#include "datamembersliderderived.hpp"
#include "datamembersBool.h"
#include "datamembersEnumDerived.h"
#include "drvToolInclude/application.h"
#include "actionNodeView.hpp"

#include "allActionTree.h"
#include "generated/actiontree.h"

#include <qvariant.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qslider.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qpushbutton.h>
#include <qmessagebox.h>
#include <qapplication.h>
#include <qwidgetlist.h>

#include <math.h>

//----------------------------------------------------------------------
// DataMemberIntSlider
//----------------------------------------------------------------------

DataMemberIntSlider::DataMemberIntSlider
(
	Application* app,
	QWidget* parent, 
	const char* name, 
	WFlags fl
)
:
DataMemberSlider( parent, name, fl ),
ApplicationDataMember(app),
m_IIntDataMember(0), 
m_dataMembersInstance(0) 
{

}

DataMemberIntSlider::~DataMemberIntSlider()
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
}

void DataMemberIntSlider::SetDataMember( IDataMembers * dataMember, IIntDataMember * dataMemberInterface )
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }

	m_dataMembersInstance = dataMember;
	m_IIntDataMember =  dataMemberInterface;

	QString displayName(m_IIntDataMember->GetName()->GetString());	
	DataMemberName->setText(displayName);	

    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Attach(this);

		int min = 0;
		int max = 0;
		if( m_IIntDataMember->GetMinMaxInt(m_dataMembersInstance, min, max) )
		{
			QString str;
			str.sprintf(" (Min=%d, Max=%d)", min, max);
			str.prepend(displayName);
			QToolTip::add(this, str);
		}
	}
	syncronize();
}

void DataMemberIntSlider::setValue()
{
    QString value = lineEdit->text();
    bool isInt;
    int intValue = value.toInt( &isInt, 10 );     // dec == 0, ok == FALSE

    if(isInt)// && lineEdit->edited() == true)
    {
        m_IIntDataMember->SetInt(m_dataMembersInstance, intValue);
    }
	else
	{
		syncronize();
	}

    lineEdit->setEdited(false); // this is connected to a lost focus or a return
}

/*
void DataMemberIntSlider::setValue( const QString & value )
{
	bool isInt;
	int intValue = value.toInt( &isInt, 10 );     // dec == 0, ok == FALSE
	
	if(isInt && lineEdit->edited() == true)
	{
		m_IIntDataMember->SetInt(m_dataMembersInstance, intValue);
    }
	else
	{
		syncronize();
	}
}
*/

void DataMemberIntSlider::Update(CSubject* theChangedCSubject)
{
    syncronize();
}
void DataMemberIntSlider::UpdateDetach(CSubject* theChangedSubject)
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
    m_dataMembersInstance = NULL;
}

void DataMemberIntSlider::syncronize(void)
{
    if(m_IIntDataMember && m_dataMembersInstance)
    {
        //
        // Now update the sliders and text field
        //
        int actualValue = m_IIntDataMember->GetInt(m_dataMembersInstance);

        QString stringValue;

        stringValue.setNum(actualValue);

        lineEdit->setText( stringValue );
    }
}

//----------------------------------------------------------------------
// DataMemberFloatSlider
//----------------------------------------------------------------------

DataMemberFloatSlider::DataMemberFloatSlider
(
	Application* app,
	QWidget* parent, 
	const char* name, 
	WFlags fl
)
:
DataMemberSlider( parent, name, fl ),
ApplicationDataMember(app),
m_IFloatDataMember(0), 
m_dataMembersInstance(0) 
{

}

DataMemberFloatSlider::~DataMemberFloatSlider()
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
}

void DataMemberFloatSlider::Update(CSubject* theChangedCSubject)
{
    syncronize();
}
void DataMemberFloatSlider::UpdateDetach(CSubject* theChangedSubject)
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
    m_dataMembersInstance = NULL;
}

void DataMemberFloatSlider::SetDataMember( IDataMembers * dataMember, IFloatDataMember * dataMemberInterface )
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }

	m_dataMembersInstance = dataMember;
	m_IFloatDataMember =  dataMemberInterface;
	
	QString displayName(m_IFloatDataMember->GetName()->GetString());	
	DataMemberName->setText(displayName);	

    syncronize();

    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Attach(this);

		float min = 0.0f;
		float max = 0.0f;
		if( m_IFloatDataMember->GetMinMaxFloat(m_dataMembersInstance, min, max) )
		{
			QString str;
			str.sprintf(" (Min=%f, Max=%f)", min, max);
			str.prepend(displayName);
			QToolTip::add(this, str);
		}
	}
}


void DataMemberFloatSlider::setValue()
{
    QString value = lineEdit->text();
    bool isFloat;
    float floatDisplayValue = value.toFloat( &isFloat);     // dec == 0, ok == FALSE

    if(isFloat)// && lineEdit->edited() == true)
    {
        setValue(floatDisplayValue, true, false);
    }
	else
	{
		syncronize();
	}

    lineEdit->setEdited(false); // this is connected to a lost focus or a return
}

void DataMemberFloatSlider::setValue( float displayValue, bool updateSlider, bool updateText)
{
    Units*  unitsConverter = m_application->GetUnits();

	//
	// Converts a display Value to the raw value and sets the member
	//
    unitsConverter->SetConverted(m_IFloatDataMember, m_dataMembersInstance, displayValue);
}

void DataMemberFloatSlider::syncronize(void)
{
	Units*  unitsConverter = m_application->GetUnits();

	QString stringValue;
	// Convert back to the display units
	float displayValue = unitsConverter->GetConverted(m_IFloatDataMember, m_dataMembersInstance);
	stringValue.setNum(displayValue, 'f');
	// Set the display value:
	lineEdit->setText( stringValue );

	// Set the units string:
	const char * displayUnits = unitsConverter->GetUnitsString(m_IFloatDataMember);
	DataMemberUnits->setText(displayUnits);
}



//----------------------------------------------------------------------
// DataMemberVector2Slider
//----------------------------------------------------------------------

DataMemberVector2Slider::DataMemberVector2Slider(Application* app,
	QWidget* parent, const char* name, WFlags fl)
: DataMemberVector3( parent, name, fl )
, ApplicationDataMember(app)
, m_IVector2DataMember(0)
, m_dataMembersInstance(0)
{
	// Disable the Z edit box:
	lineEdit3->setEnabled(false);
	lineEdit3->setFrame(false);
	// Gets rid of crappy white line
	lineEdit3->setMaximumHeight(10);
	lineEdit3->setMinimumHeight(10);
}

DataMemberVector2Slider::~DataMemberVector2Slider()
{
	if(m_dataMembersInstance)
	{
		m_dataMembersInstance->Dettach(this);
	}
}

void DataMemberVector2Slider::SetDataMember( IDataMembers * dataMember, IVector2DataMember * dataMemberInterface )
{
	if(m_dataMembersInstance)
	{
		m_dataMembersInstance->Dettach(this);
	}

	m_dataMembersInstance = dataMember;
	m_IVector2DataMember =  dataMemberInterface;

	QString displayName(m_IVector2DataMember->GetName()->GetString());	
	DataMemberName->setText(displayName);	

	syncronize();

	if(m_dataMembersInstance)
	{
		m_dataMembersInstance->Attach(this);
	}
}

void DataMemberVector2Slider::Update(CSubject* theChangedCSubject)
{
	syncronize();
}

void DataMemberVector2Slider::UpdateDetach(CSubject* theChangedSubject)
{
	if(m_dataMembersInstance)
	{
		m_dataMembersInstance->Dettach(this);
	}
	m_dataMembersInstance = NULL;
}

void DataMemberVector2Slider::setValueX()
{
	QString value = lineEdit1->text();
	bool isFloat;
	float floatDisplayValue = value.toFloat(&isFloat);     // dec == 0, ok == FALSE

	if(isFloat)
	{
		setValueX(floatDisplayValue, true);
	}
	else
	{
		synchronizeX();
	}
	lineEdit1->setEdited(false); // this is connected to a lost focus or a return
}

void DataMemberVector2Slider::setValueY()
{
	QString value = lineEdit2->text();
	bool isFloat;
	float floatDisplayValue = value.toFloat(&isFloat);     // dec == 0, ok == FALSE

	if(isFloat)
	{
		setValueY(floatDisplayValue, true);
	}
	else
	{
		synchronizeY();
	}
	lineEdit2->setEdited(false); // this is connected to a lost focus or a return
}

void DataMemberVector2Slider::setValueX(float displayValue, bool updateText)
{
	// TODO: Use the units converter to handle different types of data.
	rage::Vector2 vTmp(0.0f, 0.0f);
	m_IVector2DataMember->GetVector2(m_dataMembersInstance, vTmp);
	vTmp.x = displayValue;
	m_IVector2DataMember->SetVector2(m_dataMembersInstance, vTmp);
}

void DataMemberVector2Slider::setValueY(float displayValue, bool updateText)
{
	// TODO: Use the units converter to handle different types of data.
	rage::Vector2 vTmp(0.0f, 0.0f);
	m_IVector2DataMember->GetVector2(m_dataMembersInstance, vTmp);
	vTmp.y = displayValue;
	m_IVector2DataMember->SetVector2(m_dataMembersInstance, vTmp);
}

void DataMemberVector2Slider::syncronize()
{
	// TODO: Use the units converter to handle different types of data.
	//	Units*  unitsConverter = m_application->GetUnits();

	//
	// Convert back to the display units
	//
	// float displayValue = unitsConverter->GetConverted(m_IFloatDataMember, m_dataMembersInstance);

	synchronizeX();
	synchronizeY();
}

void DataMemberVector2Slider::synchronizeX()
{
	rage::Vector2 vTmp(0.0f, 0.0f);
	m_IVector2DataMember->GetVector2(m_dataMembersInstance, vTmp);

	QString stringValue;
	stringValue.setNum(vTmp.x, 'f');
	lineEdit1->setText( stringValue );
}

void DataMemberVector2Slider::synchronizeY()
{
	rage::Vector2 vTmp(0.0f, 0.0f);
	m_IVector2DataMember->GetVector2(m_dataMembersInstance, vTmp);

	QString stringValue;
	stringValue.setNum(vTmp.y, 'f');
	lineEdit2->setText( stringValue );
}

//----------------------------------------------------------------------
// DataMemberVector3Slider
//----------------------------------------------------------------------

DataMemberVector3Slider::DataMemberVector3Slider(Application* app,
	QWidget* parent, const char* name, WFlags fl)
: DataMemberVector3( parent, name, fl )
, ApplicationDataMember(app)
, m_IVector3DataMember(0)
, m_dataMembersInstance(0)
{}

DataMemberVector3Slider::~DataMemberVector3Slider()
{
	if(m_dataMembersInstance)
	{
		m_dataMembersInstance->Dettach(this);
	}
}

void DataMemberVector3Slider::SetDataMember( IDataMembers * dataMember, IVector3DataMember * dataMemberInterface )
{
	if(m_dataMembersInstance)
	{
		m_dataMembersInstance->Dettach(this);
	}

	m_dataMembersInstance = dataMember;
	m_IVector3DataMember =  dataMemberInterface;

	QString displayName(m_IVector3DataMember->GetName()->GetString());	
	DataMemberName->setText(displayName);	

	syncronize();

	if(m_dataMembersInstance)
	{
		m_dataMembersInstance->Attach(this);
	}
}

void DataMemberVector3Slider::Update(CSubject* theChangedCSubject)
{
	syncronize();
}

void DataMemberVector3Slider::UpdateDetach(CSubject* theChangedSubject)
{
	if(m_dataMembersInstance)
	{
		m_dataMembersInstance->Dettach(this);
	}
	m_dataMembersInstance = NULL;
}

void DataMemberVector3Slider::setValueX()
{
	QString value = lineEdit1->text();
	bool isFloat;
	float floatDisplayValue = value.toFloat(&isFloat);     // dec == 0, ok == FALSE

	if(isFloat)
	{
		setValueX(floatDisplayValue, true);
	}
	else
	{
		synchronizeX();
	}
	lineEdit1->setEdited(false); // this is connected to a lost focus or a return
}

void DataMemberVector3Slider::setValueY()
{
	QString value = lineEdit2->text();
	bool isFloat;
	float floatDisplayValue = value.toFloat(&isFloat);     // dec == 0, ok == FALSE

	if(isFloat)
	{
		setValueY(floatDisplayValue, true);
	}
	else
	{
		synchronizeY();
	}
	lineEdit2->setEdited(false); // this is connected to a lost focus or a return
}

void DataMemberVector3Slider::setValueZ()
{
	QString value = lineEdit3->text();
	bool isFloat;
	float floatDisplayValue = value.toFloat(&isFloat);     // dec == 0, ok == FALSE

	if(isFloat)
	{
		setValueZ(floatDisplayValue, true);
	}
	else
	{
		synchronizeZ();
	}
	lineEdit3->setEdited(false); // this is connected to a lost focus or a return
}

void DataMemberVector3Slider::setValueX(float displayValue, bool updateText)
{
	// TODO: Use the units converter to handle different types of data.
	rage::Vector3 vTmp(0.0f, 0.0f, 0.0f);
	m_IVector3DataMember->GetVector3(m_dataMembersInstance, vTmp);
	vTmp.x = displayValue;
	m_IVector3DataMember->SetVector3(m_dataMembersInstance, vTmp);
}

void DataMemberVector3Slider::setValueY(float displayValue, bool updateText)
{
	// TODO: Use the units converter to handle different types of data.
	rage::Vector3 vTmp(0.0f, 0.0f, 0.0f);
	m_IVector3DataMember->GetVector3(m_dataMembersInstance, vTmp);
	vTmp.y = displayValue;
	m_IVector3DataMember->SetVector3(m_dataMembersInstance, vTmp);
}

void DataMemberVector3Slider::setValueZ(float displayValue, bool updateText)
{
	// TODO: Use the units converter to handle different types of data.
	rage::Vector3 vTmp(0.0f, 0.0f, 0.0f);
	m_IVector3DataMember->GetVector3(m_dataMembersInstance, vTmp);
	vTmp.z = displayValue;
	m_IVector3DataMember->SetVector3(m_dataMembersInstance, vTmp);
}

void DataMemberVector3Slider::syncronize()
{
	// TODO: Use the units converter to handle different types of data.
//	Units*  unitsConverter = m_application->GetUnits();

	//
	// Convert back to the display units
	//
	// float displayValue = unitsConverter->GetConverted(m_IFloatDataMember, m_dataMembersInstance);

	synchronizeX();
	synchronizeY();
	synchronizeZ();
}

void DataMemberVector3Slider::synchronizeX()
{
	rage::Vector3 vTmp(0.0f, 0.0f, 0.0f);
	m_IVector3DataMember->GetVector3(m_dataMembersInstance, vTmp);

	QString stringValue;
	stringValue.setNum(vTmp.x, 'f');
	lineEdit1->setText( stringValue );
}

void DataMemberVector3Slider::synchronizeY()
{
	rage::Vector3 vTmp(0.0f, 0.0f, 0.0f);
	m_IVector3DataMember->GetVector3(m_dataMembersInstance, vTmp);

	QString stringValue;
	stringValue.setNum(vTmp.y, 'f');
	lineEdit2->setText( stringValue );
}

void DataMemberVector3Slider::synchronizeZ()
{
	rage::Vector3 vTmp(0.0f, 0.0f, 0.0f);
	m_IVector3DataMember->GetVector3(m_dataMembersInstance, vTmp);

	QString stringValue;
	stringValue.setNum(vTmp.z, 'f');
	lineEdit3->setText( stringValue );
}

//----------------------------------------------------------------------
// DataMemberStringImp
//----------------------------------------------------------------------

DataMemberStringImp::DataMemberStringImp
(
	Application* app,
	QWidget* parent, 
	const char* name, 
	WFlags fl
)
:
DataMemberString(parent, name, fl),
ApplicationDataMember(app),
m_dataMemberInterface(0),
m_dataMembersInstance(0)
{
    //
    // signals and slots connections
    //
    connect( lineEdit, SIGNAL( returnPressed() ),               this, SLOT( SetValue() ) );
    connect( lineEdit, SIGNAL( lostFocus() ),                   this, SLOT( SetValue() ) );
    connect( lineEdit, SIGNAL( textChanged(const QString&) ),   this, SLOT( textChanged(const QString&) ) );

    //
    // Disable the Button... hide it...
    //
    pushButton->setEnabled( false );
    pushButton->setFlat( true );

	defaultColor = lineEdit->paletteForegroundColor();
}

DataMemberStringImp::~DataMemberStringImp()
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
}

void DataMemberStringImp::Update(CSubject* theChangedCSubject)
{
    syncronize();
}
void DataMemberStringImp::UpdateDetach(CSubject* theChangedSubject)
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
    m_dataMembersInstance = NULL;
}

void DataMemberStringImp::SetDataMember( IDataMembers * dataMember, IStringDataMember * dataMemberInterface )
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }

    m_dataMemberInterface = dataMemberInterface;
    m_dataMembersInstance = dataMember;

	DataMemberName->setText(QString(dataMemberInterface->GetName()->GetString()));
    syncronize();

    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Attach(this);
    }
}

void DataMemberStringImp::SetValue( const QString & value )
{
	lineEdit->setPaletteForegroundColor(defaultColor);//QColor(0,0,0));
    m_dataMemberInterface->SetString(m_dataMembersInstance, value.ascii());
}

void DataMemberStringImp::SetValue( void )
{
    QString lineEditText = lineEdit->text();
    const char* lineEditTextAscii = lineEditText.ascii();
    SetValue(lineEditText);
    lineEdit->setEdited(false); // this is connected to a lost focus or a return
}

void DataMemberStringImp::textChanged( const QString & value )
{
    //
    // Only care about this when the text is being edited
    //
    if(lineEdit->edited())
    {
        SetValue(value);
    }
}

void DataMemberStringImp::syncronize(void)
{
    bool isBeingEdited = lineEdit->edited();
    QString lineEditText = lineEdit->text();
    QString dataBaseString(m_dataMemberInterface->GetString(m_dataMembersInstance));

    //
    // if the Text is being edited... dont stomp it...
    // also... if the text is the same... dont change it...
    // This is an optimization and it helps prevent cycles... 
    // since I can't set the text in the lineEdit and tell it NOT
    // to send a signal... I dont want to have to set a flag in this class
    // to prevent it if I dont have too...
    //
    const char* dataBaseStringAscii = dataBaseString.ascii();
    const char* lineEditTextAscii   = lineEditText.ascii();

    if(!isBeingEdited && dataBaseString != lineEditText)
    {
        lineEdit->setText(dataBaseString);
    }
}

//----------------------------------------------------------------------
// DataMemberNodeRef
//----------------------------------------------------------------------

DataMemberNodeRef::DataMemberNodeRef
(
	Application* app,
	QWidget* parent, 
	const char* name, 
	WFlags fl
)
:
DataMemberString(parent, name, fl),
ApplicationDataMember(app),
m_dataMemberInterface(0),
m_dataMembersInstance(0)
{
    //
    // signals and slots connections
    // If the user hits return or just losses focus... try to resolve the connection
    //
    connect( lineEdit, SIGNAL( returnPressed() ),   this, SLOT( SetValue() ) );
    connect( lineEdit, SIGNAL( lostFocus() ),       this, SLOT( SetValue() ) );
    
    //
    // When the user is editing the text... dont try to resolve until enter is hit... or lost focus
    //
    connect( lineEdit, SIGNAL( textChanged(const QString&) ), this, SLOT( textChanged(const QString&) ) );

	static const QPixmap nextIcon = QPixmap::fromMimeSource( "next.png");
    //
    // Now put the ICON
    //
    pushButton->setPixmap(nextIcon);

	defaultColor = lineEdit->paletteForegroundColor();
}

DataMemberNodeRef::~DataMemberNodeRef()
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
}

void DataMemberNodeRef::Update(CSubject* theChangedCSubject)
{
    syncronize();
}
void DataMemberNodeRef::UpdateDetach(CSubject* theChangedSubject)
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
    m_dataMembersInstance = NULL;
}

void DataMemberNodeRef::OnButtonAction( void )
{
    //
    // Notify buttons to update
    //
 //   if(qApp->focusWidget())
 //   {
 //       qApp->focusWidget()->clearFocus();
 //   }

    if(m_dataMembersInstance && m_dataMemberInterface->IsResolved(m_dataMembersInstance))
    {
		// Previously, this method was casting m_dataMembersInstance to a 
		// NodeReferenceTrack*, and calling GetActionNode().  The drawback
		// with that method is that a track can only have one node reference
		// data member.
		//
		// Using ActionNode::Find is probably slower, but it allows us to
		// have multiple node references in a single track.  -CE
		// 
		ActionNode* actualNode = ActionNode::Find(m_dataMemberInterface->GetFullPath(m_dataMembersInstance));

        if(actualNode)
        {
            //
            // This is a hack in abscense of a nice way to connect differnt views
            //
            QWidgetList* list = QApplication::topLevelWidgets();
            if(list)
            {
                QWidgetListIt it( *list );         // iterate over the widgets
                QWidget * w;
                ActionTree* actionTree = NULL;
                bool foundActionTree = false;
                while( (w=it.current()) != 0 && foundActionTree == false) 
                {  
                    ++it;
                    const char* widgetName = w->name();
                    if(widgetName && strcmp(widgetName, "ActionTree") == 0)
                    {
                        foundActionTree = false;
                        actionTree = dynamic_cast<ActionTree*>(w);
                    }
                }
                delete list;

                if(actionTree)
                {
                    actionTree->SelectActionNode(actualNode);
                }
            }
        }
    }
}

void DataMemberNodeRef::SetDataMember( IDataMembers * dataMember, INodeReferenceDataMember * dataMemberInterface )
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }

    m_dataMemberInterface = dataMemberInterface;
    m_dataMembersInstance = dataMember;

	DataMemberName->setText(QString(m_dataMemberInterface->GetName()->GetString()));
	syncronize();
    //SetValue(QString(m_dataMemberInterface->GetString(m_dataMembersInstance)));

    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Attach(this);
    }
}

void DataMemberNodeRef::SetValue( const QString & value )
{
	m_dataMemberInterface->SetString(m_dataMembersInstance, value.ascii());
/*
    bool isResolved = m_dataMemberInterface->IsResolved(m_dataMembersInstance);
	
	if(isResolved)
	{
		lineEdit->setPaletteForegroundColor(QColor(0,0,0));
	    lineEdit->setText(QString(m_dataMemberInterface->GetFullPath(m_dataMembersInstance)));
    }
	else
	{
		lineEdit->setPaletteForegroundColor(QColor(255,0,0));
	    lineEdit->setText(QString(m_dataMemberInterface->GetString(m_dataMembersInstance)));
	}
*/
}

void DataMemberNodeRef::SetValue( void )
{
    SetValue(lineEdit->text());
}

void DataMemberNodeRef::textChanged( const QString & value )
{
    if(lineEdit->edited())
    {
	    lineEdit->setPaletteForegroundColor(QColor(255,0,0));
    }
}

void DataMemberNodeRef::syncronize()
{
    bool isResolved = m_dataMemberInterface->IsResolved(m_dataMembersInstance);

    if(isResolved)
    {
        lineEdit->setPaletteForegroundColor(defaultColor);//QColor(0,0,0));
        lineEdit->setText(QString(m_dataMemberInterface->GetFullPath(m_dataMembersInstance)));
    }
    else
    {
        lineEdit->setPaletteForegroundColor(QColor(255,0,0));
        lineEdit->setText(QString(m_dataMemberInterface->GetString(m_dataMembersInstance)));
    }
}

//----------------------------------------------------------------------
// DataMemberCommand
//----------------------------------------------------------------------
DataMemberCommand::DataMemberCommand
(
	Application* app,
	QWidget* parent, 
	const char* name, 
	WFlags fl
)
:
DataMemberString(parent, name, fl),
ApplicationDataMember(app),
m_dataMemberInterface(0),
m_dataMembersInstance(0)
{
    //
    // signals and slots connections
    // If the user hits return or just losses focus... try to resolve the connection
    //
    connect( lineEdit, SIGNAL( returnPressed() ),       this, SLOT( SetValue() ) );
    connect( lineEdit, SIGNAL( lostFocus() ),           this, SLOT( SetValue() ) );
    
    //
    // When the user is editing the text... dont try to resolve until enter is hit... or lost focus
    //
    connect( lineEdit, SIGNAL( textChanged(const QString&) ), this, SLOT( textChanged(const QString&) ) );

    //
    // Now put the ICON
    //
//    pushButton->setPixmap(QPixmap::fromMimeSource( "next.png"));
}

DataMemberCommand::~DataMemberCommand()
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
}

void DataMemberCommand::Update(CSubject* theChangedCSubject)
{
    syncronize();
}
void DataMemberCommand::UpdateDetach(CSubject* theChangedSubject)
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
    m_dataMembersInstance = NULL;
}

void AddTrackReferenceMembersToDataMembersView
(
    DataMembersView* view,
    TrackReference* trackRef
)
{
    int numMembers = trackRef->GetNumMembers();
	for(int i=0;i<numMembers;i++)
	{
		DataMember* dataMember = trackRef->GetMember(i);
        if(dataMember)
        {
            QWidget* newItemWidget = view->CreateDataMemberWidget(trackRef->GetTrack(), dataMember);

		    if(newItemWidget)
		    {
			    newItemWidget->show();
			    view->addChild(newItemWidget);
		    }
        }
	}
}


void DataMemberCommand::OnButtonAction( void )
{
    if(m_dataMembersInstance)
    {
        m_dataMemberInterface->Execute(m_dataMembersInstance);

        //
        // This is a hack in abscense of a nice way to connect differnt views
        //
        QWidgetList* list = QApplication::topLevelWidgets();
        if(list)
        {
            QWidgetListIt it( *list );         // iterate over the widgets
            QWidget * w;
            DataMembersView* findQueryResultsView = NULL;
            bool foundActionTree = false;
            while( (w=it.current()) != 0 && foundActionTree == false) 
            {  
                ++it;
                const char* widgetName = w->name();
                if(widgetName && strcmp(widgetName, "FindQueryResults") == 0)
                {
                    foundActionTree = true;
                    findQueryResultsView = dynamic_cast<DataMembersView*>(w);
                }
            }
            delete list;

            FindQuery* findQuery = m_application->GetFindQuery();
            if(findQueryResultsView && findQuery)
            {
                findQueryResultsView->clear();

                //
                // Add all the widgets from the search
                //
                int foundItems = findQuery->GetFoundItemNum();
                for(int i=0;i<foundItems;i++)
                {
                    NodeReferenceFilterTrack* track = findQuery->GetFoundItem(i);
                    findQueryResultsView->AddDataMembers( track );
                    
                    if(findQuery->GetQueryEditPriority())
                    {
                        ActionNode* refActionNode = track->GetActionNode();
                        if(refActionNode)
                        {
                            ActionNodePlayable* playable = dynamic_cast<ActionNodePlayable*>(refActionNode);
                            if(playable)
                            {
                                DataMember* dataMember = playable->GetMember(0);
                                if(dataMember)
                                {
                                    QWidget* newItemWidget = findQueryResultsView->CreateDataMemberWidget(playable, dataMember);

                                    if(newItemWidget)
                                    {
                                        newItemWidget->show();
                                        findQueryResultsView->addChild(newItemWidget);
                                    }
                                }
                            }
                        }
                    }
                    //
                    // AddConditionElements
                    //
                    int conditionCount = track->GetConditionCount();
                    for(int conditionID=0;conditionID<conditionCount;conditionID++)
                    {
                        AddTrackReferenceMembersToDataMembersView(findQueryResultsView, track->GetConditionTrackReference(conditionID));
                    }
                    //
                    // AddTrackElements
                    //
                    int trackCount = track->GetTrackCount();
                    for(int trackID=0;trackID<trackCount;trackID++)
                    {
                        AddTrackReferenceMembersToDataMembersView(findQueryResultsView, track->GetTrackReference(trackID));
                    }                    
                }
                
                findQueryResultsView->ActiveLayout();
            }
        }
    }
}

void DataMemberCommand::SetDataMember( IDataMembers * dataMember, ICommandDataMember * dataMemberInterface )
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }

    m_dataMemberInterface = dataMemberInterface;
    m_dataMembersInstance = dataMember;

	DataMemberName->setText(QString(m_dataMemberInterface->GetName()->GetString()));
    const char* stringValue = m_dataMemberInterface->GetString(m_dataMembersInstance);
    lineEdit->setText(QString(stringValue));


    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Attach(this);
    }
}

void DataMemberCommand::SetValue( const QString & value )
{
	m_dataMemberInterface->SetString(m_dataMembersInstance, value.ascii());
}

void DataMemberCommand::SetValue( void )
{
    SetValue(lineEdit->text());
}

void DataMemberCommand::textChanged( const QString & value )
{
/*
    if(lineEdit->edited())
    {
	    lineEdit->setPaletteForegroundColor(QColor(255,0,0));
    }
*/
}

void DataMemberCommand::syncronize()
{
    // right now this does nothing...
}

//----------------------------------------------------------------------
// DataMembersView
//----------------------------------------------------------------------
DataMembersView::DataMembersView(QWidget* parent, const char* name, WFlags fl)
:
ScrollViewDialog(parent, name, fl),
m_Instance(NULL),
m_ParentDataMembersView(NULL)
{
    //
    // Here we'll add the custom ToolBar stuff we want
    //
    QHBoxLayout* hbox = new QHBoxLayout(m_ToolBarFrame);

    m_ResetButton = new QPushButton(m_ToolBarFrame, "ResetTrack");
    m_ResetButton->setMinimumSize(0, 20);
    m_ResetButton->setMaximumSize(100, 20);
    m_ResetButton->setText("Reset");
    
    m_DataMemberName = new QLabel(m_ToolBarFrame, "TrackName");
    m_DataMemberName->setFrameShape(QFrame::PopupPanel);
    m_DataMemberName->setFrameShadow(QFrame::Sunken);
    m_DataMemberName->setMinimumSize(20, 20);
    m_DataMemberName->setMaximumSize(32767, 20);

    hbox->addWidget(m_ResetButton);
	hbox->addWidget(m_DataMemberName);
	hbox->activate();

    connect( m_ResetButton, SIGNAL(clicked()), this, SLOT(Reset()) );

    m_ViewProperties = NULL;
}

void DataMembersView::Reset(void)
{
	if(m_Instance)
	{

        int choice = QMessageBox::warning( this,    "Reset Track",
                                                    "Are you sure you want to reset \nand loose your values for this Track?\n",                                                   
                                                    "Ok",
                                                    "Cancel", 0, 1 );

        switch(choice)
        {
            case 0: 
                {
                    //
                    // Ok to reset
                    //
                    m_Instance->Reset();

                    //
                    // This refresh here should only refresh the contents of the 
                    // DataMembers not the widets themselves... 
                    // causes a Flashing of the widgets and is less efficiant
                    //
                    refresh(m_Instance);
                }
                break;
            case 1: 
                {
                    // The user clicked the Quit or pressed Escape
                    // exit
                }
                break;
        }
    }
}

QWidget* DataMembersView::CreateDataMemberWidget(IDataMembers * instance, DataMember* dataMember)
{
	QWidget* newItemWidget = 0;

    QWidget* parentWidget = viewport();;

	switch(dataMember->GetType())
	{
		case DataMember::DM_BOOL:
			{
				IBoolDataMember* boolDataMember = dynamic_cast<IBoolDataMember*>(dataMember);
				if(boolDataMember)
				{
					DataMemberBoolCheckBox* dataCheckBox = new DataMemberBoolCheckBox(parentWidget);
					dataCheckBox->SetDataMember(instance, boolDataMember);
					newItemWidget = static_cast<QWidget*>(dataCheckBox);
				}
			}
			break;

		case DataMember::DM_INT:
			{
				IIntDataMember* intDataMember = dynamic_cast<IIntDataMember*>(dataMember);
				if(intDataMember)
				{
					DataMemberIntSlider* dataSlider = new DataMemberIntSlider(Application::GetInstance(), parentWidget);											
					dataSlider->SetDataMember(instance, intDataMember);
					newItemWidget = static_cast<QWidget*>(dataSlider);
				}
			}
			break;
		
        case DataMember::DM_FLOAT:
        case DataMember::DM_TIME:
        case DataMember::DM_TRACK_TIME:
        case DataMember::DM_LENGTH:
        case DataMember::DM_VELOCITY:
        case DataMember::DM_ACCELERATION:
        case DataMember::DM_ANGLE:
			{
				IFloatDataMember* floatDataMember = dynamic_cast<IFloatDataMember*>(dataMember);
				if(floatDataMember)
				{
					DataMemberFloatSlider* dataSlider = new DataMemberFloatSlider(Application::GetInstance(), parentWidget);
					dataSlider->SetDataMember(instance, floatDataMember);
					newItemWidget = static_cast<QWidget*>(dataSlider);
				}
			}
			break;

		case DataMember::DM_VECTOR3:
			{
				IVector3DataMember* v3DataMember = dynamic_cast<IVector3DataMember*>(dataMember);
				if(v3DataMember)
				{
					DataMemberVector3Slider* dataSlider = new DataMemberVector3Slider(Application::GetInstance(), parentWidget);
					dataSlider->SetDataMember(instance, v3DataMember);
					newItemWidget = static_cast<QWidget*>(dataSlider);
				}
			}

		case DataMember::DM_VECTOR2:
			{
				IVector2DataMember* v2DataMember = dynamic_cast<IVector2DataMember*>(dataMember);
				if(v2DataMember)
				{
					DataMemberVector2Slider* dataSlider = new DataMemberVector2Slider(Application::GetInstance(), parentWidget);
					dataSlider->SetDataMember(instance, v2DataMember);
					newItemWidget = static_cast<QWidget*>(dataSlider);
				}
			}

		case DataMember::DM_ENUM:
			{
				IEnumDataMember* enumDataMember = dynamic_cast<IEnumDataMember*>(dataMember);
				if(enumDataMember)
				{
					DataMemberEnumComboBox* comboBox = new DataMemberEnumComboBox(parentWidget);
					comboBox->SetDataMember(instance, enumDataMember);
					newItemWidget = static_cast<QWidget*>(comboBox);
                    connect( comboBox, SIGNAL( updated(void) ), this, SLOT( refresh(void) ) );
				}
			}
			break;

		case DataMember::DM_STRING:
			{
				IStringDataMember* nodeDataMember = dynamic_cast<IStringDataMember*>(dataMember);
				if(nodeDataMember)
				{
					DataMemberStringImp* dataString = new DataMemberStringImp(Application::GetInstance(), parentWidget);
					dataString->SetDataMember(instance, nodeDataMember);
					newItemWidget = static_cast<QWidget*>(dataString);
				}

			}
			break;

		case DataMember::DM_NODEREF:
			{

				INodeReferenceDataMember* nodeDataMember = dynamic_cast<INodeReferenceDataMember*>(dataMember);
				if(nodeDataMember)
				{
					DataMemberNodeRef* dataString = new DataMemberNodeRef(Application::GetInstance(), parentWidget);
					dataString->SetDataMember(instance, nodeDataMember);
					newItemWidget = static_cast<QWidget*>(dataString);
				}
			}
			break;
        case DataMember::DM_COMMAND:    
            {
				ICommandDataMember* commandDataMember = dynamic_cast<ICommandDataMember*>(dataMember);
				if(commandDataMember)
				{
					DataMemberCommand* commandWidget = new DataMemberCommand(Application::GetInstance(), parentWidget);
					commandWidget->SetDataMember(instance, commandDataMember);
					newItemWidget = static_cast<QWidget*>(commandWidget);
				}
			}
			break;
		default:
			break;
	}

    return(newItemWidget);
}

void DataMembersView::AddDataMembers( IDataMembers * instance )
{
	if(instance)
	{
        //
        // This is only here for the reset method... which is sortof a hack
        //
        m_Instance = NULL;
       
        int numMembers = instance->GetNumMembers();
		for(int i=0;i<numMembers;i++)
		{

			DataMember* dataMember = instance->GetMember(i);

            QWidget* newItemWidget = CreateDataMemberWidget(instance, dataMember);

			if(newItemWidget)
			{
				newItemWidget->show();
				addChild(newItemWidget);
			}
		}
    }
}


void DataMembersView::refresh( IDataMembers * instance )
{
	clear();

    //
    // Hide and show after adding reduces flicker... even tho it's still shit
    //
    m_scrollView->viewport()->hide();
    m_Frame->hide();

    //
    // This is for the 
    //
    DataMembersView* dataMembersView = NULL;
    ConditionListDataMemberView* groupDataMemberView = NULL;
    
	if(instance)
	{
        //
        // This is only here for the reset method... which is sortof a hack
        //
        m_Instance = instance;
        
        //
        // This is shitty
        //
        Track* track = dynamic_cast<Track*>(m_Instance);
        if(track)
        {
            m_DataMemberName->setText(track->GetName()->GetString());
        }

		QWidget* parentWidget = viewport();

        int numMembers = instance->GetNumMembers();
		for(int i=0;i<numMembers;i++)
		{

			DataMember* dataMember = instance->GetMember(i);

            QWidget* newItemWidget = CreateDataMemberWidget(instance, dataMember);

            //
            // This here is still a hack since I have to find a nice way to have this
            // happen herachical update of the view work
            //
            if(!newItemWidget && dataMember->GetType() == DataMember::DM_GROUP_DATAMEMBER)
            {
				IGroupDataMember* groupDataMember = dynamic_cast<IGroupDataMember*>(dataMember);
				if(groupDataMember)
				{
					groupDataMemberView = new ConditionListDataMemberView(parentWidget);
					groupDataMemberView->SetDataMember(instance, groupDataMember);
					newItemWidget = static_cast<QWidget*>(groupDataMemberView);
                    
                    dataMembersView = new DataMembersView(parentWidget);
                    connect( groupDataMemberView, SIGNAL(updatedDataMembers(IDataMembers*)), dataMembersView, SLOT(refresh( IDataMembers * )) );

                    dataMembersView->SetParentDataMembersView(this);
				}
			}

			if(newItemWidget)
			{
				newItemWidget->show();
				addChild(newItemWidget);
			}
		}
	}
    else
    {
        m_Instance = NULL;
		m_DataMemberName->setText("");
    }

    //
    // The other half of adding the widgets... helps to reducd flicker
    //
    m_scrollView->viewport()->show();
    m_Frame->show();

	//
    // Update the Layout so the newly added widgets can resize to fit proper
    //
    ActiveLayout();

    //
    // SUPER HACK!!!
    //
    if(dataMembersView && groupDataMemberView)
	{
		dataMembersView->show();
		addChild(dataMembersView);
		IDataMembers* subDataMembers = groupDataMemberView->GetCurrentSelectedData();
		if(subDataMembers)
		{
			dataMembersView->refresh(subDataMembers);
		}
    }

	DataMembersView* dataMembersViewParent = GetParentDataMembersView();
    if(dataMembersViewParent)
    {
        dataMembersViewParent->ActiveLayout();
    }

//    m_scrollView->viewport()->adjustSize();
//    m_FrameLayout->activate(); // this is KEY!!!! after the internals are drawn!!!!
  //  qApp->processEvents();

 //   qApp->processEvents();

/*    
    
    qApp->processEvents();
    
    m_Frame->adjustSize();
    qApp->processEvents();
*/    

/*
    resize(width()+1, height()+1);    
    qApp->processEvents();
    
    resize(width()-1, height()-1);
    qApp->processEvents();
*/

//    m_Frame->updateGeometry();
//    m_scrollView->viewport()->updateGeometry();

    //
    // Hide 
    //
}

void DataMembersView::ActiveLayout(void)
{
	QWidget * pViewport = m_scrollView->viewport();
	pViewport->adjustSize();
    m_FrameLayout->activate(); // this is KEY!!!! after the internals are drawn!!!!
}

void DataMembersView::refresh( void )
{
    updated(); // signal
}

extern void SyncWithViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);
extern void SaveViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);

void DataMembersView::SyncWithViewProperies(void)
{
    SyncWithViewProperiesUtil(this, m_ViewProperties);
}

void DataMembersView::SaveViewProperies(void)
{
    SaveViewProperiesUtil(this, m_ViewProperties);
}

//----------------------------------------------------------------------
// DataMemberTrackTimingSliding
//----------------------------------------------------------------------
void TimeDataMembersView::refreshItems()
{
   	clear();

    //
    // Hide 
    //
    m_scrollView->viewport()->hide();
    m_Frame->hide();


    if(m_ActionNode)
    {
        int numTracks = m_ActionNode->GetTrackCount();
        
        for(int trackId = 0;trackId<numTracks;trackId++)
        {
            Track* track = m_ActionNode->GetTrack(trackId);

            if(track)
            {
                //
                // One Widget per Track
                //
	            QWidget* parentWidget = viewport();
                DataMemberTrackTimingSliding* timeSlider = new DataMemberTrackTimingSliding(Application::GetInstance(), parentWidget);
				timeSlider->SetDataMember(track);

                int numMembers = track->GetNumMembers();
	            for(int member=0;member<numMembers;member++)
	            {

		            QWidget* newItemWidget = 0;

		            DataMember* dataMember = track->GetMember(member);

		            switch(dataMember->GetType())
		            {	
			            case DataMember::DM_TRACK_TIME:
				            {
					            IFloatDataMember* floatDataMember = dynamic_cast<IFloatDataMember*>(dataMember);
					            if(floatDataMember)
					            {
                                    timeSlider->AddTimeMember( floatDataMember );
                                }
				            }
				            break;

			            default:
				            break;
		            }
	            }

                if(timeSlider)
		        {
			        timeSlider->show();
			        addChild(timeSlider);   
		        }

            }
        }
    }

    m_scrollView->viewport()->show();
    m_Frame->show();

    m_scrollView->viewport()->adjustSize();
    m_FrameLayout->activate(); // this is KEY!!!! after the internals are drawn!!!!
}

//----------------------------------------------------------------------
// DataMemberTrackTimingSliding
//----------------------------------------------------------------------

static float s_timeToTicks = 2.0f*30.0f; // 30fps and 2 ticks perframe

DataMemberTrackTimingSliding::DataMemberTrackTimingSliding
(
	Application* app,
	QWidget* parent, 
	const char* name, 
	WFlags fl
)
:
TrackTimingSliding( parent, name, fl ),
ApplicationDataMember(app),
m_dataMembersInstance(0),
m_MaxTimeLine(120) // ticks... actual Value to be scaled... right now 1/2frames
{
    m_NumTimeDataMembers = 0;
    for(int i=0;i<MAX_TIME_DATAMEMBERS;i++)
    {
        m_IFloatDataMember[i] = NULL;
    }

    TimeSlider->setMinValue(0);
    TimeSlider->setMaxValue(m_MaxTimeLine);
}

DataMemberTrackTimingSliding::~DataMemberTrackTimingSliding()
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
}

void DataMemberTrackTimingSliding::Update(CSubject* theChangedCSubject)
{
    syncronize();
}
void DataMemberTrackTimingSliding::UpdateDetach(CSubject* theChangedSubject)
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
    m_dataMembersInstance = NULL;
}

void DataMemberTrackTimingSliding::AddTimeMember( IFloatDataMember * dataMemberInterface )
{
    if(m_NumTimeDataMembers<MAX_TIME_DATAMEMBERS)
    {
        m_IFloatDataMember[m_NumTimeDataMembers] = dataMemberInterface;
        m_NumTimeDataMembers++;
    }
    else
    {
        printf("Out of Time Slots Line:%d\n", __LINE__);
    }

    update();
}

void DataMemberTrackTimingSliding::SetDataMember( IDataMembers * dataMember )
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }

	m_dataMembersInstance = dataMember;

    Track* track = dynamic_cast<Track*>(dataMember);
    if(track)
    {
	    QString displayName(track->GetName()->GetString());
        TrackName->setText(displayName);
    }

    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Attach(this);
    }
}

void DataMemberTrackTimingSliding::update(void)
{
    float minTimeValue = 10000.0f;
    float maxTimeValue = -10000.0f;

    for(int i=0;i<m_NumTimeDataMembers;i++)
    {
        float timeValue = m_IFloatDataMember[i]->GetFloat(m_dataMembersInstance);

        minTimeValue = (minTimeValue < timeValue)?minTimeValue:timeValue;
        maxTimeValue = (maxTimeValue > timeValue)?maxTimeValue:timeValue;
    }

    if(minTimeValue<0.0f)
    {
 
    }
    else
    {
        //
        // This is weak
        //
        if(minTimeValue == 10000.0f)
        {
            minTimeValue = 0.0f;
        }
        if(maxTimeValue == -10000.0f)
        {
            maxTimeValue = 0.0f;
        }

        int minTimeTick = (int)floor(s_timeToTicks * minTimeValue);
        int maxTimeTick = (int)floor(s_timeToTicks * maxTimeValue);
        
        int range = maxTimeTick - minTimeTick;
        int totalRange = m_MaxTimeLine - range;

        int valueAvg = (minTimeTick+maxTimeTick)/2;
        int value    = (valueAvg*range)/m_MaxTimeLine;

        TimeSlider->setSteps( 0, totalRange);        
        TimeSlider->setSteps( 1, range );
        TimeSlider->setValue( value );

/*
        TimeSlider->setSteps( 0, m_MaxTimeLine - range );        
        TimeSlider->setSteps( 1, range );
        TimeSlider->setValue((minTimeTick+maxTimeTick)/2);
*/
    }
}

void DataMemberTrackTimingSliding::syncronize()
{
    // not implemented
}

/*
void DataMemberFloatSlider::increment( int percent )
{
	float percentFloatSeconds = (float)(percent)/30.0f;

	//
	// Increments are frameBased so 1 tick is 1/30seconds
	//
	float value = percentFloatSeconds;
	if(m_IFloatDataMember->GetType() == DataMember::DM_TIME)
	{
		value = m_application->GetSecondToCurrentTimeUnit(percentFloatSeconds);
	}

	setValue(value, false, true );
}

void DataMemberFloatSlider::setValue( const QString & value )
{
	bool isFloat;
	float floatValueRaw = value.toFloat( &isFloat);     // dec == 0, ok == FALSE
	
	if(isFloat)
	{
		setValue(floatValueRaw, true, false);
    }
}


void DataMemberFloatSlider::setValue( float rawValue, bool updateSlider, bool updateText)
{

	float actualValue = rawValue;
	float actualValueSeconds = rawValue;
	if(m_IFloatDataMember->GetType() == DataMember::DM_TIME)
	{
		float floatValueSeconds = m_application->GetCurrentTimeUnitToSecond(rawValue);

		//
		// Floats are stored in seconds
		//
		m_IFloatDataMember->SetFloat(m_dataMembersInstance, floatValueSeconds);

		//
		// Now update the sliders and text field
		//
		actualValueSeconds = m_IFloatDataMember->GetFloat(m_dataMembersInstance);

		//
		// Convert back to the views units
		//
		actualValue = m_application->GetSecondToCurrentTimeUnit(actualValueSeconds);
	}
	else
	{
		//
		// Here we are all RAW
		//
		m_IFloatDataMember->SetFloat(m_dataMembersInstance, rawValue);
		actualValue = m_IFloatDataMember->GetFloat(m_dataMembersInstance);
		actualValueSeconds = actualValue;
	}

	if(updateText)
	{
		QString stringValue;
		stringValue.setNum(actualValue);
		lineEdit->setText( stringValue );
	}

	//
	// Sliders assume 1/30 seconds for 1 tick
	//
	if(updateSlider)
	{
		float toFrames = actualValueSeconds*30.0f;
		int frames = (int)(toFrames); //???
		slider->setValue( frames );
	}
}
	

class DataMemberTrackTimingSliding : public TrackTimingSliding, public ApplicationDataMember
{
    Q_OBJECT
public:
	DataMemberTrackTimingSliding(Application* app, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
	~DataMemberTrackTimingSliding(){}

    void SetDataMember( IDataMembers * dataMember )
    void SetTime0( IFloatDataMember * dataMemberInterface0 );
    void SetTime1( IFloatDataMember * dataMemberInterface1 );

public slots:
//	virtual void increment( int percent );
//    virtual void setValue( const QString & value );

private:
	void setValue( float rawValue , bool updateSlider , bool updateText);

    IFloatDataMember* m_IFloatDataMember0;
    IFloatDataMember* m_IFloatDataMember1;
    IDataMembers* m_dataMembersInstance;
};
*/
