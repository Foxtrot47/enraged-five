#include "actionTree/datamember.h"
#include "generated/datamemberenum.h"
#include "drvToolInclude/application.h"

class DataMemberEnumComboBox : public DataMemberEnum, public IObserver
{
	Q_OBJECT

public:
	DataMemberEnumComboBox( QWidget* parent = 0, const char* name = 0 );
	~DataMemberEnumComboBox();

    void SetDataMember( IDataMembers * dataMember, IEnumDataMember* dataMemberInterface );
    virtual void Update(CSubject* theChangedCSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
    virtual void SetValue( int value );

private:
    void syncronize(void);
    IEnumDataMember*  m_dataMemberInterface;
    IDataMembers*	  m_dataMembersInstance;
};
