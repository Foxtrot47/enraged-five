#include "remote.h"

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <winsock2.h>
#include "deci2.h"
#include "netmp.h"
#include "assert.h"

// strange sony defines... needed in dcmp
typedef unsigned short half;			// 16bit
typedef unsigned int word;				// 32bit
#include "dcmp.h"


#define MAXHOSTNAMELEN 256
#define DECI2_DEFAULT_PORT		"8510"		// default port number
#define DECI2_DEFAULT_TIMEOUT	10
#define PROTO_AMBMGR	0xe000				// protocol number, this sample uses
#define NODE_EE		'E'	/* (0x45) EE */
#define NODE_HOST		'H'	/* (0x48) Host */

class NetworkMgr
{
public:
	static int hSocket;
	static char Buffer[DECI2_MAX_LEN];
	static DECI2_HDR SendHdr;

// Network stuff	
	static int ShowError(char *mes);
	static void MakeDeci2Hdr(DECI2_HDR *hdr,u_short l,u_short p,u_char s,u_char d);
	static int PacketComing(int fd, struct timeval *timeout);
	static int SendPacket(int fd, char *buf, int len);
	static int RecvPacket(int fd, char *buf);
	static void ProcessNetmp(char *buf);
	static void ProcessDcmp(char *buf);
	static void ProcessSample(int fd, char *buf);	
    static void ProcessSampleActionTree(int fd, char *buf);
	static int ProcessPacket(int fd, char *buf);
	static int SendNetmpConnect(int fd);
	static int ConnectNet(const char *arg);
};

class ThreadType
{ 
protected:
	bool Active;
	bool InThread;
	unsigned long ThreadID;
	HANDLE ThreadHandle;
	void (*GoFunc)();
public: 
	ThreadType();
	~ThreadType();

	bool IsInThread() {  return InThread;  }
	void Go();
	void Start(void (*goFunc)());
	void Stop();
};

// Can't be in the class?
static unsigned long WINAPI ThreadTypeGo(void *Dummy);

int NetworkMgr::hSocket= -1;
char NetworkMgr::Buffer[DECI2_MAX_LEN];
DECI2_HDR NetworkMgr::SendHdr;

static ThreadType RecvThread;
static ThreadType SendThread;
static void RecvFunc();
static void SendFunc();

int NetworkMgr::ShowError(char *mes) 
{
	printf("%s\n", mes); return -1;
}


void NetworkMgr::MakeDeci2Hdr(DECI2_HDR *hdr,u_short l,u_short p,u_char s,u_char d) 
{
	hdr->length      = l;
	hdr->reserved    = 0;
	hdr->protocol    = p;
	hdr->source      = s;
	hdr->destination = d;
}


// check packet is comming or not
// return 0: timeout
//        1: packet comes
int NetworkMgr::PacketComing(int fd, struct timeval *timeout) 
{
	int n;
	fd_set rfds;

	FD_ZERO(&rfds);
	FD_SET(fd, &rfds);
	if ((n = select(FD_SETSIZE, &rfds, NULL, NULL, timeout)) < 0) {
		if (errno == EINTR) {
			printf("EINTR\n");
			return 0;
		}
		return ShowError("select() faild.");
	}
    if (!n) return 0;
    if (!FD_ISSET(fd, &rfds)) return ShowError("select() unknown fd.");
	return 1;
}


int NetworkMgr::SendPacket(int fd, char *buf, int len) 
{
	return send(fd, buf, len, 0);
}


int NetworkMgr::RecvPacket(int fd, char *buf) 
{
    int n;
	int len = sizeof(DECI2_HDR);
    DECI2_HDR *hdr = (DECI2_HDR *)buf;

	// read deci2 header first
	if ((n = recv(fd, buf, len, 0)) < 0) return ShowError("read() failed");
	if (n != len) return ShowError("read() failed.");

	// and then read rest of data
	if ((n = recv(fd, buf+len, hdr->length-len, 0)) < 0)
		return ShowError("read() failed");
	if (n != hdr->length-len) return ShowError("read() failed.");

	return hdr->length;
}


void NetworkMgr::ProcessNetmp(char *buf) 
{				// handle NETMP packets
	NETMP_HDR *hdr = (NETMP_HDR *)buf;

	switch (hdr->code) {
		case NETMP_CODE_RESETR:
			printf("target resetting.\n");
	        // what should this do?
			break;
		case NETMP_CODE_CONNECTR:					// connection established
			if(hdr->result != NETMP_ERR_OK) {
				printf("cannot connect to dsnetm.\n");
				exit(1);
			}
//			waitingNetmpConnectr = 0;
			printf("Connected.\n");
			break;
		default:
			printf("unknown netmp packet received.\n");
			break;
	}
}

void NetworkMgr::ProcessDcmp(char *buf) 
{				// handle DCMP packets
	DCMP_HDR *hdr = (DCMP_HDR *)buf;
	DCMP_STATUS_DATA *id;

	switch (hdr->type) {
		case DCMP_TYPE_STATUS:
			printf("DCMP_TYPE_STATUS: ");
			id = (DCMP_STATUS_DATA *)(hdr + 1);
			switch (hdr->code) {
				case DCMP_CODE_CONNECTED: printf("CONNECTED"); break;
				case DCMP_CODE_PROTO:     printf("PROTO");     break;
				case DCMP_CODE_UNLOCKED:  printf("UNLOCKED");  break;
				case DCMP_CODE_SPACE:     printf("SPACE");     break;
				default:                  printf("UNKNOWN");   break;
			}
			printf(" proto/node=0x%x", id->proto);
			break;
		case DCMP_TYPE_ERROR:
			printf("DCMP_TYPE_ERROR: ");
			switch (hdr->code) {
				case DCMP_CODE_NOPROTO:   printf("NOPROTO");   break;
				case DCMP_CODE_LOCKED:    printf("LOCKED");    break;
				case DCMP_CODE_NOSPACE:   printf("NOSPACE");   break;
				case DCMP_CODE_INVALHEAD: printf("INVALHEAD"); break;
				case DCMP_CODE_NOCONNECT: printf("NOCONNECT"); break;
				default:                  printf("UNKNOWN");   break;
			}
			break;
		default:
			printf("Unknown DCMP message");
			break;
	}
	printf(" Received.\n");
}

void NetworkMgr::ProcessSampleActionTree(int fd, char *buf)
{
    char *data;
    u_int len = 0;
    DECI2_HDR *hdr = (DECI2_HDR *)buf;

    data = (char*)(hdr+1);	// pointer for data
    data +=2; // shift
//    data++;

    printf("Received Command: %d\n", *data);

    char* datachar = (char*)data;
    const char* helloMsg = datachar; //&(datachar[2]);
    if(helloMsg && strcmp(helloMsg, "{pc}hello pc")==0)
    {
        RemoteMgr::SendMsg("{pc}hello ps2");
        return;
    }
}


// distribute packet to proper handler
int NetworkMgr::ProcessPacket(int fd, char *buf) 
{
	DECI2_HDR *pkt = (DECI2_HDR *)buf;

	switch (pkt->protocol) {
		case DECI2_PROTO_NETMP:
			ProcessNetmp((char *)(pkt + 1));
			break;
		case DECI2_PROTO_DCMP:
			ProcessDcmp((char *)(pkt + 1));
			break;
		case PROTO_AMBMGR:
			ProcessSampleActionTree(fd, buf);
			break;
		default:
			break;
	}

	return 0;
}


// send NETMP connect packet
int NetworkMgr::SendNetmpConnect(int fd)
{
    struct {
        DECI2_HDR deci2;
        NETMP_HDR netmp;
        NETMP_PROTOS protos;
    } pkt;

    MakeDeci2Hdr(&pkt.deci2, sizeof(pkt), DECI2_PROTO_NETMP,
							DECI2_NODE_HOST, DECI2_NODE_HOST);
    pkt.netmp.code = NETMP_CODE_CONNECT;
    pkt.netmp.result = 0;
	pkt.protos.pri = NETMP_PRI_DEFAULT;
	pkt.protos.reserved = 0;
	pkt.protos.proto = PROTO_AMBMGR;

    return SendPacket(fd, (char *) &pkt, sizeof(pkt));
}


// make socket and connect to server
int NetworkMgr::ConnectNet(const char *arg) 
{
	int fd;
    char *pos;
    char hostname[MAXHOSTNAMELEN + 1];
    char *strport = DECI2_DEFAULT_PORT;
    u_long addr, port;
    struct hostent *hp;
    struct sockaddr_in server;

	if (arg == NULL || !*arg) {					// arg indicates "host:port"
		if (gethostname(hostname, sizeof(hostname)) < 0)
			return ShowError("gethostname() fail");
	} else if ((pos = strchr(arg, ':')) != NULL) {
		strncpy(hostname, arg, pos - arg);
		if (*(pos + 1)) strport = pos + 1;
	} else {
		if (strlen(arg) > sizeof(hostname))
			return ShowError("argument larger than expect");
		strcpy(hostname, arg);
	}

//    bzero((char *)&server, sizeof(server));
    memset(&server, 0, sizeof(server));
	if ((addr = inet_addr(hostname)) == -1) {
		if ((hp = gethostbyname(hostname)) == NULL)
			return ShowError("gethostbyname() fail");
		addr = *(u_long *)hp->h_addr;
		server.sin_family = hp->h_addrtype;
	} else {
		server.sin_family = AF_INET;
	}

    if (sscanf(strport, "%d", &port) != 1 || !port)	// get port number
		return ShowError("invalid port number");

    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)	// create socket
	{
		switch (WSAGetLastError())
		{
			case WSANOTINITIALISED: printf("A successful WSAStartup must occur before using this function.\n"); break;
			case WSAENETDOWN: printf(" The network subsystem or the associated service provider has failed. \n"); break;
			case WSAEAFNOSUPPORT: printf(" The specified address family is not supported. \n"); break;
			case WSAEINPROGRESS: printf(" A blocking Windows Sockets 1.1 call is in progress, or the service provider is still processing a callback function. \n"); break;
			case WSAEMFILE: printf(" No more socket descriptors are available. \n"); break;
			case WSAENOBUFS: printf(" No buffer space is available. The socket cannot be created. \n"); break;
			case WSAEPROTONOSUPPORT: printf(" The specified protocol is not supported. \n"); break;
			case WSAEPROTOTYPE: printf(" The specified protocol is the wrong type for this socket. \n"); break;
			case WSAESOCKTNOSUPPORT: printf("The specified socket type is not supported in this address family. \n"); break;
		}

		return ShowError("socket() fail");
	}

    server.sin_addr.s_addr = addr;
    server.sin_port = htons((u_short) port);
	// connect to host
    if (connect(fd, (struct sockaddr *)&server, sizeof(server)) < 0)
		return ShowError("connect() fail");

	return fd;
}

void RecvFunc()
{
	static struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	if (NetworkMgr::PacketComing(NetworkMgr::hSocket, &timeout) == 1) 
	{
// Packet coming, receive and process it
		if (NetworkMgr::RecvPacket(NetworkMgr::hSocket, NetworkMgr::Buffer) > 0)
		{
			NetworkMgr::ProcessPacket(NetworkMgr::hSocket, NetworkMgr::Buffer);
		}
	}

	Sleep(10);
}

void SendFunc()
{
// What was i gonna use this func for??  Probably don't need it.
	Sleep(10);
}

//---------------------------------------------------------
// ThreadType class
//---------------------------------------------------------
ThreadType::ThreadType()
{
	Active= false;
}


ThreadType::~ThreadType()
{
}

void ThreadType::Start(void (*goFunc)())
{
	if (!Active)
	{
		GoFunc= goFunc;
		Active= true;

		ThreadHandle= CreateThread(NULL, 0, ThreadTypeGo, this, 0, &ThreadID);
		SetThreadPriority(ThreadHandle, THREAD_PRIORITY_HIGHEST);
	}
}

void ThreadType::Stop()
{
	if (Active)
	{
		Active= false;
		GoFunc= NULL;
	}
}

void ThreadType::Go()
{
	InThread= true;

// Loop for the life of the thread
	while (Active)
	{
		(*GoFunc)();
	}

	CloseHandle(ThreadHandle);

	InThread= false;
}

unsigned long WINAPI ThreadTypeGo(void *Dummy)
{
	((ThreadType *) Dummy)->Go();
	return 0;
}

//------------------------------------------------------------------------------------
// RemoteMgr
//------------------------------------------------------------------------------------
bool RemoteMgr::Connect(const char *hostname)
{
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    wVersionRequested = MAKEWORD( 2, 2 );

    // Initialize winsock
    err = WSAStartup( wVersionRequested, &wsaData );
    if ( err != 0 ) 
    {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
        return false;
    }

    if (LOBYTE( wsaData.wVersion ) != 2 ||
        HIBYTE( wsaData.wVersion ) != 2 ) 
    {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
        return false;
    }

    // Connect to host
    if ((NetworkMgr::hSocket = NetworkMgr::ConnectNet(hostname)) < 0) 
    {
        printf("Cannot connect to host %s\n", hostname);
        return false;
    }

    // Send NETMP connect
    if (NetworkMgr::SendNetmpConnect(NetworkMgr::hSocket) < 0) 
    {
        printf("%Failed to send connect packet to %s\n", hostname);
        return false;
    }

    // Start the recv and send threads
    RecvThread.Start(RecvFunc);
    SendThread.Start(SendFunc);

    return true;
}

bool RemoteMgr::Disconnect()
{
    RecvThread.Stop();
    SendThread.Stop();

    while ((RecvThread.IsInThread()) || (SendThread.IsInThread()))
        Sleep(5); // Wait until thread is completely done before deleting anything it might use.

    WSACleanup();

    return true;
}

void RemoteMgr::SendMsg(const char* msg)
{
    char *data;
    u_int len = 0;
    DECI2_HDR *hdr = (DECI2_HDR *) NetworkMgr::Buffer;
    int deci2HrdLen = sizeof(DECI2_HDR);

    data = (char*)(hdr+1);	// pointer for data
    data++; data[0] = '\0';
    data++; data[0] = '\0';

    // Copy command to send buffer
    char* msgBuff = (char*)data;
    strncpy(msgBuff, msg, DECI2_MAX_LEN-deci2HrdLen);
    int msgLen = strlen(msg);
    msgBuff[msgLen] = '\0';

    printf("Sending %s\n", msgBuff);

    len = msgLen+1+sizeof(u_short); //u_short is datablocksize
    len += deci2HrdLen;

    // Make send header
    NetworkMgr::MakeDeci2Hdr(hdr, (u_short) len, PROTO_AMBMGR, NODE_HOST, NODE_EE);

    // Send packet
    NetworkMgr::SendPacket(NetworkMgr::hSocket, NetworkMgr::Buffer, len);
}

