#include "datamembersBool.h"
#include <qlabel.h>
#include <qcheckbox.h>

//----------------------------------------------------------------------
// DataMemberBoolCheckBox
//----------------------------------------------------------------------

DataMemberBoolCheckBox::DataMemberBoolCheckBox(QWidget* parent, const char* name)
: DataMemberCheckbox( parent, name )
, m_dataMemberInterface(0)
, m_dataMembersInstance(0)
{
	connect( DataMemberCheckbox::checkBox, SIGNAL( toggled(bool) ), this, SLOT( setValue(bool) ) );
}

DataMemberBoolCheckBox::~DataMemberBoolCheckBox()
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
}

void DataMemberBoolCheckBox::Update(CSubject* theChangedCSubject)
{
    syncronize();
}

void DataMemberBoolCheckBox::UpdateDetach(CSubject* theChangedSubject)
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
    m_dataMembersInstance = NULL;
}

void DataMemberBoolCheckBox::SetDataMember( IDataMembers * dataMember, IBoolDataMember * dataMemberInterface )
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }

    m_dataMembersInstance = dataMember;
	m_dataMemberInterface = dataMemberInterface;

	QString displayName(m_dataMemberInterface->GetName()->GetString());
	DataMemberCheckbox::DataMemberName->setText(displayName);	

    syncronize();

    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Attach(this);
    }
}

void DataMemberBoolCheckBox::setValue ( bool tf )
{
	//
	// Documentation says we need to do this since the toggled signal 
	// cant be trusted for Tristated checkboxes
	//
    if(m_dataMemberInterface && m_dataMembersInstance)
    {
		bool checked = DataMemberCheckbox::checkBox->isChecked();
        bool dataMemberValue = m_dataMemberInterface->GetBool(m_dataMembersInstance);
    
        //
        // This is a bit of a hack to prevent an unnecessary update
        //
        if(checked != dataMemberValue)
        {
            m_dataMemberInterface->SetBool(m_dataMembersInstance, checked);
        }
    }
}

void DataMemberBoolCheckBox::syncronize()
{
    bool boolSet = m_dataMemberInterface->GetBool(m_dataMembersInstance);
	DataMemberCheckbox::checkBox->setChecked(boolSet);
}

void DataMemberBoolCheckBox::onPressed()
{
	// Set the focus when the checkbox is first pressed.  This forces whatever
	// widget is currently active to lose focus, which in turn pushes its
	// value into the underlying datamember.
	// Without this, DataMemberBoolCheckBox::setValue causes the parent
	// observer to refresh all the datamembers, losing any values that were in
	// the process of being edited.
	//
	// From RDR2 bug 22972:
	// 
	// Repro steps
	//  1. Open Dr. V
	//  2. Click on Global then Create to create the default Track (execute)
	//  3. Click in any field and start typing (numerical field or text field)
	//  4. click on a check box
	// 
	// Results: The changes in the text field are gone.
	//
	// Expected Results: The changes should be saved after clicking a checkbox
	// like how it is done if you click into another text field.

	setFocus();
}
