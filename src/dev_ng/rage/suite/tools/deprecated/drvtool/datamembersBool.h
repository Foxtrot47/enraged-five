#include "actionTree/datamember.h"
#include "drvToolInclude/application.h"
#include "generated/datamembercheckbox.h"

class DataMemberBoolCheckBox : public DataMemberCheckbox, public IObserver
{
	Q_OBJECT

public:
	DataMemberBoolCheckBox( QWidget* parent = 0, const char* name = 0 );
	~DataMemberBoolCheckBox();

    void SetDataMember( IDataMembers * dataMember, IBoolDataMember * dataMemberInterface );
    virtual void Update(CSubject* theChangedCSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
    void setValue ( bool check );
	virtual void onPressed();

private:
    void syncronize(void);
    IBoolDataMember* m_dataMemberInterface;
    IDataMembers*	 m_dataMembersInstance;
};
