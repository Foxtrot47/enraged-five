#include "mainQt.h"
#include "application.hpp"

int main( int argc, char ** argv )
{
    Application* drvAppDataBase = Application::CreateInstance();
    int returnValue = mainQT( drvAppDataBase, argc, argv );
    return(returnValue);
}

