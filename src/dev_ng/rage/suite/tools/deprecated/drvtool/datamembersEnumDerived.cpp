#include "datamembersEnumDerived.h"
#include <qcombobox.h>
#include <qlabel.h>

//----------------------------------------------------------------------
// Widget for Enum DataMembers
//----------------------------------------------------------------------


DataMemberEnumComboBox::DataMemberEnumComboBox( QWidget* parent, const char* name)
:
DataMemberEnum( parent, name ),
m_dataMemberInterface(0),
m_dataMembersInstance(0)
{
	//
	// Not allowed to edit this shit
	//
    comboBoxEnum->setEditable( false );
	//comboBoxEnum->setEditable( true );

	//
	// Set the enum whenever a new item in the widget a newly activate value 
	//
	//QComboBox* comboBoxPtr = (QComboBox*)this;
	connect( comboBoxEnum, SIGNAL( activated ( int ) ), this, SLOT( SetValue(int) ) );
}

DataMemberEnumComboBox::~DataMemberEnumComboBox()
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
}

void DataMemberEnumComboBox::Update(CSubject* theChangedCSubject)
{
    syncronize();
}
void DataMemberEnumComboBox::UpdateDetach(CSubject* theChangedSubject)
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }
    m_dataMembersInstance = NULL;
}

void DataMemberEnumComboBox::SetDataMember( IDataMembers * dataMember, IEnumDataMember * dataMemberInterface )
{
    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Dettach(this);
    }

    m_dataMemberInterface = dataMemberInterface;
    m_dataMembersInstance = dataMember;

    QString displayName(m_dataMemberInterface->GetName()->GetString());
	DataMemberName->setText(displayName);	

	//
	// Need to populate this this ComboBox with all the enum values
	//
	int numEnums = m_dataMemberInterface->GetNumEnums();
	for(int i=0;i<numEnums;i++)
	{
		comboBoxEnum->insertItem( QString(m_dataMemberInterface->GetEnumName(i) ) );
	}
	
    syncronize();

    if(m_dataMembersInstance)
    {
        m_dataMembersInstance->Attach(this);
    }
}

void DataMemberEnumComboBox::SetValue( int value )
{
    if(m_dataMembersInstance && m_dataMemberInterface)
    {
	    m_dataMemberInterface->SetEnum(m_dataMembersInstance, value);
    }
    updated(); //signal
}

void DataMemberEnumComboBox::syncronize()
{
    int currentEnum = m_dataMemberInterface->GetEnum(m_dataMembersInstance);
    comboBoxEnum->setCurrentItem( currentEnum );
}
