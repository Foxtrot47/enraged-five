/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

void ListViewItemCreator::init()
{
    //
    // Popup Views and menus
    //
    m_PopUpMenu = new QPopupMenu(this, "ConditionViewPopup");
    m_PopUpMenu->insertItem( "&Delete",	this, SLOT(deleteItem()));
    m_PopUpMenu->insertItem( "&Copy"  ,	this, SLOT(copyItem()));
	m_PopUpMenu->insertItem( "Cut"    , this, SLOT(cutItem()));
    m_PopUpMenu->insertItem( "&Paste",	this, SLOT(pasteItem()));
	m_PopUpMenu->insertItem( "Covert To Spa&wn", this, SLOT(convertToSpawn()));
}

void ListViewItemCreator::newItemFromItemDisplay()
{
    QString newItemName = itemDisplay->currentText();
    newItem( newItemName );
}


void ListViewItemCreator::deleteItem()
{

}


void ListViewItemCreator::copyItem()
{

}


void ListViewItemCreator::cutItem()
{
	copyItem();
	deleteItem();
}


void ListViewItemCreator::pasteItem()
{

}


void ListViewItemCreator::moveItem()
{

}


void ListViewItemCreator::activatePopUp( QListViewItem * /*item*/, const QPoint & position )
{
	m_PopUpMenu->move( position );
	m_PopUpMenu->show();
}


void ListViewItemCreator::newItem( QString & itemName )
{

}

void ListViewItemCreator::refreshItems()
{

}

void ListViewItemCreator::convertToSpawn()
{

}
