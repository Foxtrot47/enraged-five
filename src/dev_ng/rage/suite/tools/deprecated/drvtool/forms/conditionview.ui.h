/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/


void conditionView::scroll( int value )
{
	QWidget* parentWidget = conditionViewLayout->mainWidget();
	QRect parentSize(parentWidget->geometry());
	
	int scrollAmmount = parentSize.height()*value/99;
	
	parentWidget->move ( 0, -scrollAmmount );
	parentWidget->updateGeometry();
}


void conditionView::addWidget( QWidget * widget )
{
	QWidget* parentWidget = conditionViewLayout->mainWidget();
	
	QRect newparentSize(parentWidget->geometry());
	QRect widgetSize(widget->geometry());
	
	parentWidget->resize(newparentSize.width(), newparentSize.height() + widgetSize.height() );
	widget->resize(newparentSize.width(),  widgetSize.height() );
	
	conditionViewLayout->add(widget);
}


QWidget* conditionView::getAttachParent()
{
	return(conditionViewLayout->mainWidget());
}


QVBoxLayout* conditionView::Layout()
{
	return(conditionViewLayout);
}


void conditionView::clear()
{
	QWidget* layoutParent = conditionViewLayout->mainWidget();

	delete layoutParent;
//	delete conditionViewLayout;

	QWidget* privateLayoutWidget = new QWidget( this, "conditionViewLayout" );
    privateLayoutWidget->setGeometry( QRect( 10, 0, 330, 30 ) );
    conditionViewLayout = new QVBoxLayout( privateLayoutWidget, 0, 0, "conditionViewLayout"); 
    languageChange();
    resize( QSize(445, 373).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
	privateLayoutWidget->show();

/*
    QWidget* privateLayoutWidget_2 = new QWidget( this, "conditionViewLayout" );
    privateLayoutWidget_2->setGeometry( QRect(  10, 0, 330, 30  ) );
    conditionViewLayout = new QVBoxLayout( privateLayoutWidget_2, 0, 0, "conditionViewLayout");
	privateLayoutWidget_2->show();
*/
}
