

void DataMemberVector3::onLostFocusX()
{
    setValueX();
}


void DataMemberVector3::onReturnPressedX()
{
    setValueX();
}


void DataMemberVector3::onLostFocusY()
{
    setValueY();
}


void DataMemberVector3::onReturnPressedY()
{
    setValueY();
}


void DataMemberVector3::onLostFocusZ()
{
    setValueZ();
}


void DataMemberVector3::onReturnPressedZ()
{
    setValueZ();
}

