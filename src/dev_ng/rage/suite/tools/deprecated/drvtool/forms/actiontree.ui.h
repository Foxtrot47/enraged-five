/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "datamemberslider.h"
#include "datamemberstring.h"
#include "datamembercreateview.h"
#include "qvbox.h"

//
// These are all code... no Designer tool used for these
//
#include "../datamembersEnumDerived.h"
#include "../datamembersBool.h"
#include "../datamembersliderderived.hpp"
#include "../ScrollViewDialog.h"
#include "../actionNodeView.hpp"
#include "../SpewWindow.hpp"
#include "../ActionControllerView.hpp"
#include "../MotionTreeView.hpp"

//
// This is going to be the new database for the application...
// Slowly replacing the "ActionTree"'s data... the "ActionTree"
// will act as simply another "View" to the applications data...
//
#include "../../drvToolInclude/application.h"
#include "../allActionTree.h"

#include <qtimer.h>
#include <qapplication.h>
#include <qclipboard.h>
#include <qmessagebox.h>
#include <qstringlist.h>

//
// Styles
//
#include <qcdestyle.h> 
#include <qplatinumstyle.h> 
#include <qsgistyle.h> 

#include <string>

const float g_UpdateTimeSeconds = 0.015f; // seconds
const float g_UpdateTimeMilliSeconds = g_UpdateTimeSeconds*1000.0f; // seconds

const char* g_scriptPath = 0;

ActionNodeView* g_testNodeView;
DataMembersView* g_UnitsView;
DataMembersView* g_FindQueryView;
DataMembersView* g_FindQueryResultsView;
DataMembersView* g_FilePropertiesView;
ActionNodeBankTreeView* g_GlobalBank;
SpewWindow* g_SpewWindow;
extern Application* g_theApp;
ViewProperties* g_ActionTreeViewProperties;
ActionControllerView* g_ActionControllerView;
ActionControllerOwnerView* g_ActionControllerOwnerView;
MotionTreeView* g_MotionTreeView;
//MotionTreeEditView* g_MotionTreeEditView;

ActionTree* g_ActionTree = 0;
extern Application* g_theApp;

void RefreshDataMemberTimeingView( IDataMembers * instance, ScrollViewDialog * parent );

void ActionTree::fileNew()
{
    QString fileName;
    bool bFileExists = false;

    do 
    {
        fileName = QFileDialog::getSaveFileName(
            g_theApp->GetFileProperties()->GetScriptPath(),
            "Action File (*.act)",
            this,
            "New file dialog",
            "Choose a fileName and Path" );

            FILE *fileExists = NULL;
        
            if(fileName.ascii())
            {
                fileExists = fopen( fileName.ascii(), "r" );

                if(fileExists)
                {   
                    fclose(fileExists);
                    bFileExists = true;

                    QString caption = fileName + " File Already Exists";
                    QString text = caption + " please pick another filename\n";

                    QMessageBox::warning( this, caption, text, QMessageBox::Ok, QMessageBox::NoButton);
                }
                else
                {
                    bFileExists = false;
                }
            }

    } while(fileName.ascii() && bFileExists);


    //
    // Here you should have a valid FileName that doesn't already exist
    //
    if(fileName.ascii())
    {
        ArchiveFileRecord* archiveFile = new ArchiveFileRecord(fileName.ascii());
        g_theApp->GetFileProperties()->SetScriptPath(archiveFile->GetPathName()->GetString());
        g_theApp->GetFileProperties()->Notify();
        ActionNodeBank* rootActionNode = new ActionNodeBank("UNNAMED_BANK");
        archiveFile->SetArchiveObject(rootActionNode);
        if(rootActionNode)
        {
            ActionNode::AddRoot(rootActionNode);
            //
            // Add a View for the FileName();
            //
            ArchiveFileRecordTreeView* fileView = new ArchiveFileRecordTreeView(archiveFile, g_GlobalBank, NULL, rootActionNode->GetPriority());

            //
            // Recurse the rest of the view
            //
            newNodeView( fileView );  
        }
    }
}


void ActionTree::fileOpen()
{

     QStringList files = QFileDialog::getOpenFileNames(
						"Action File (*.act)",
                        g_theApp->GetFileProperties()->GetScriptPath(),
						this,
						"open file dialog"
						"Choose a file" );

    QStringList list = files;

//    QStringList::Iterator it = list.begin();
//   while( it != list.end() ) 

    int fileTotal = files.size();
    for(int i=0;i<fileTotal;i++)
    {
        QString fileName = files[i];
        if(fileName.ascii())
	    {
            fileOpen(fileName.ascii());
	    }

        //
        // Next file
        //
//        ++it;
    }

}

void ActionTree::fileSave()
{
	int numFiles = g_theApp->GetFileProperties()->GetArchiveFileRecordsSize();
    
    bool skipWarning = false;

    if(numFiles)
    {
        QString message = "<font color=green><b>SAVING......:</b></font>";
        g_SpewWindow->appendPostTime(message);
    }

	for(int i=0;i<numFiles;i++)
	{
		ArchiveFileRecord* archiveFileRecord = g_theApp->GetFileProperties()->GetArchiveFileRecords(i);
        bool OkToWrite = true;
        
        if(!skipWarning && archiveFileRecord->IsFileWriteAble() == false)
        {
            QString fileName(archiveFileRecord->GetFileName()->GetString());
            QString caption = fileName + " File Not Writeable";
            QString youDontHaveCheckedOut = " you probably dont have " + fileName + " checked out";
            //QString text = youDontHaveCheckedOut + "\n Would you like to skip all warnings this save?";

            //int warningReturn = QMessageBox::warning( this, caption, text, QMessageBox::No, QMessageBox::Ok);
            //if(warningReturn == QMessageBox::Ok)
            {
                //skipWarning = true;
            }

            QString message;
            QString warningString = "<font color=red><b>NOT SAVED:</b></font>";
            message = warningString + caption + youDontHaveCheckedOut;
            g_SpewWindow->appendPostTime(message);

            OkToWrite = false;
        }

        //
        // Even if this write operation will fail... it'll still write to a temp file
        // this is handled internally... it should be brought out and handled here
        // Instead!!!
        //
        if(OkToWrite && archiveFileRecord->IsFileChangedOnDisk())
        {
            //
            // Disallow write of this file
            // This is here to prevent people from checking out a file 
            // after making edits in an Open DrV... overwriteing whats on disk
            //
            OkToWrite = false;
            QString fileName(archiveFileRecord->GetFileName()->GetString());
            QString caption = fileName + " File Changed On Disk";
            QString text = fileName + " has changed on Disk.\n Do you want to save the file with a different fileName?";

            int warningReturn = QMessageBox::warning( this, caption, text, QMessageBox::No, QMessageBox::Yes);
            if(warningReturn == QMessageBox::Yes)
            {
                changeFileName(archiveFileRecord);
                OkToWrite = true;
            }
        }

        if(OkToWrite)
        {
            archiveFileRecord->WriteToFile();
			QString fileName(archiveFileRecord->GetFileName()->GetString());
            QString saved = "<font color=green><b>SAVED:</b></font>";
            QString message = saved + fileName;
            g_SpewWindow->appendPostTime(message);

			if (g_theApp->GetFileProperties()->GetTrySaveToGameFolders())
			{
				// New stuff, save to game dirs if it can
				std::string filename(archiveFileRecord->GetFullPathName()->GetString());
				int posData = filename.find("Data");
				if (posData>=0) // If we found Data in the path, we're saving game data and should try Game dirs
				{
					std::string firstHalf = filename.substr(0,posData);
					std::string secondHalf = filename.substr(posData + 4,filename.length());
			
					std::string gameDirs[3];
					gameDirs[0] = "Game\\Xenon";
					gameDirs[1] = "Game\\Win32";
					gameDirs[2] = "Game\\PS3";

					for (int i = 0; i < 3; ++i)
					{
						std::string saveTo = firstHalf + gameDirs[i] + secondHalf;
						if (archiveFileRecord->WriteToFile(saveTo.c_str()))
							g_SpewWindow->appendPostTime(QString(gameDirs[i].c_str()));
					}        
				}
			}
        }
	}
}




void ActionTree::fileExit()
{
    close(false);
}

void ActionTree::editCut()
{
    editCopy();
    deleteNodeFromSelection();
}

void ActionTree::editCopy()
{
	if(m_SelectedItem)
	{
		ActionNode* actionNode = m_SelectedItem->GetActionNode();

		TextBuffer textBuffer(64096);
		if(actionNode)
		{
			actionNode->Write(textBuffer);
			
			QClipboard *clipBoard = QApplication::clipboard();

			//
			// Copy text into the clipboard
			//
			clipBoard->setText( textBuffer.GetBuffer() );
		}
	}
}


void ActionTree::editPaste()
{

	if(m_SelectedItem)
	{
		ActionNode* actionNode = m_SelectedItem->GetActionNode();
		QClipboard *clipBoard = QApplication::clipboard();

		//
		// Get the Shit from copy text from the clipboard (paste)
		// 
		QString text = clipBoard->text();
		if( !text.isNull() )
		{
			qDebug( "The clipboard contains: " + text );


			TextParser textParser(text.ascii(), " ,\t\r\n" );
			ActionNode* subTree = ActionNode::Create(textParser);

			if(subTree)
			{

				//
				// Now we need to parent the tree to the selectedNode...
				// And resolve references
				//               
                actionNode->AddChildActionNode(subTree);
				ActionNode* motionTree = subTree->FindRoot();
				motionTree->ResolveTrackReferences();


				//
				// Now it's part of the ActionTree... update the Gui
				//
                int numChildren = actionNode->GetChildCount();
				TreeNodeView*  subTreeView = 0;
				if(ActionNodePlayable* node = dynamic_cast<ActionNodePlayable*>(subTree))
				{
					ActionNodePlayableTreeView* actionNodePlayableView = new ActionNodePlayableTreeView(node, m_SelectedItem, NULL, numChildren);
                    subTreeView = actionNodePlayableView;
				}
				else 
				if(ActionNodeBank* bank = dynamic_cast<ActionNodeBank*>(subTree))
				{
					ActionNodeBankTreeView* actionNodeBankView = new ActionNodeBankTreeView(bank, m_SelectedItem, NULL, numChildren);	
					subTreeView = actionNodeBankView;
				}
				else 
				if(ActionNodeFileReference* fileRefNode = dynamic_cast<ActionNodeFileReference*>(subTree))
				{
                    ActionNodeFileReferenceTreeView* actionNodeFileReferenceView = new ActionNodeFileReferenceTreeView(fileRefNode, m_SelectedItem, NULL, numChildren);
					subTreeView = actionNodeFileReferenceView;
				}

				if(subTreeView)
				{
					//
					// Recurse the rest of the view
					//
					newNodeView( subTreeView );
				}
			}
		}
	}
}


void ActionTree::editFind()
{
    g_FindQueryView->showNormal();
    g_FindQueryView->ActiveLayout();
    g_FindQueryResultsView->showNormal();
    g_FindQueryResultsView->ActiveLayout();
}

void ActionTree::helpAbout()
{
    QString caption = "DrV ActionTree";
    QString text = "Originally created by Liberty Walker at Rockstar Vancouver.";
	text += "\n\nCurrently maintained by:";
	text += "\n  Charles Eubanks (charles@rockstarsd.com)";
	text += "\n  Bryan Musson (musson@rockstarsd.com)";
	text += "\n  Warren Johnson (wjohnson@rockstarvancouver.com)";
	text += "\n  Duncan Frostick (dfrostick@rockstarvancouver.com)";
	text += "\n  Mark Tennant (mtennant@rockstarvancouver.com)";
	text += "\n\nNamed after the character Doctor V in the video game Tobal 2";
	text += "\nhttp://en.wikipedia.org/wiki/Tobal_2";

    QMessageBox::about(this, caption, text);
}


void ActionTree::RemoveBreakpointsFromConditionGroupR(ConditionGroupCondition *pConditionGroup)
{
	int iConditionCount = pConditionGroup->GetConditionGroup()->GetConditionCount();

	for (int i = 0; i < iConditionCount; i++)
	{
		Condition *pCondition = pConditionGroup->GetConditionGroup()->GetCondition(i);
		pCondition->SetDebugBreakPoint(false);

		ConditionGroupCondition *pChildConditionGroup = dynamic_cast<ConditionGroupCondition *>(pCondition);
		if (pChildConditionGroup)
		{
			RemoveBreakpointsFromConditionGroupR(pChildConditionGroup);
		}

	}
}


void ActionTree::RemoveBreakpointsR(ActionNode *pActionNode)
{
	int iTrackCount = pActionNode->GetTrackCount();
	for (int i = 0; i < iTrackCount; i++)
	{
		Track *pTrack = pActionNode->GetTrack(i);
		pTrack->SetDebugBreakPoint(false);

	} // for i

	int iConditionCount = pActionNode->GetConditionCount();
	for (int i = 0; i < iConditionCount; i++)
	{
		Condition *pCondition = pActionNode->GetCondition(i);
		pCondition->SetDebugBreakPoint(false);

		ConditionGroupCondition *pConditionGroup = dynamic_cast<ConditionGroupCondition *>(pCondition);
		if (pConditionGroup)
		{
			RemoveBreakpointsFromConditionGroupR(pConditionGroup);
		}

	} // for i

	int iNumChildren = pActionNode->GetChildCount();

	for (int i = 0; i < iNumChildren; i++)
	{
		ActionNode *pChildActionNode = pActionNode->GetChild(i);
		RemoveBreakpointsR(pChildActionNode);
		
	} // for i
	
} // ActionTree::RemoveBreakpointR


void ActionTree::toolsRemoveAllBreakpoints()
{
	ActionNode *pActionNode = g_GlobalBank->GetActionNode();

	RemoveBreakpointsR(pActionNode);

}

//
// My additions
//

void ActionTree::rightButtonClicked( QListViewItem *item, const QPoint &position, int id)
{
	clicked( item, position, id);

    QPoint popupPosition = position;

    float popupPositionHeight = popupPosition.y();
    float popUpHeight =  m_TreeViewPopup->frameRect().height();
    float deskTopHeight = QApplication::desktop()->height();
    static float screenFudge = 100;
    if(popupPositionHeight+popUpHeight+screenFudge > deskTopHeight)
    {
        //
        // Just position it from the bottom
        //
        popupPosition.setY(popupPositionHeight-popUpHeight);
    }

	m_TreeViewPopup->move( popupPosition );
	m_TreeViewPopup->show();
}


void ActionTree::clicked( QListViewItem *item, const QPoint &, int )
{
	m_SelectedItem = dynamic_cast<TreeNodeView*>(item);

	if(m_SelectedItem)
	{
        ActionNode* actionNode = m_SelectedItem->GetActionNode();
		g_testNodeView->SetActionNode(actionNode);
		//m_tracksTimingViewNew->SetActionNode(actionNode);
        //ActionTreeView->startDrag();
	}
}

void ActionTree::newNodeFromSelected()
{
	if(m_SelectedItem)
	{
		newNode( m_SelectedItem );
	}

	m_TreeViewPopup->hide();
	
}

void ActionTree::newBankFromSelected()
{
	if(m_SelectedItem)
	{
		newBank( m_SelectedItem );
	}

	m_TreeViewPopup->hide();
}

void ActionTree::newFileReferenceBankFromSelected()
{
	if(m_SelectedItem)
	{
		newFileReferenceBank( m_SelectedItem );
	}

	m_TreeViewPopup->hide();
}


void ActionTree::newNode( QListViewItem *parent )
{
	TreeNodeView* parentNodeView = dynamic_cast<TreeNodeView*>(parent);	
	if(parentNodeView)
	{
		newNode(parentNodeView);
	}
}


void ActionTree::newNode( TreeNodeView *parent )
{

	ActionNode* parentNode = parent->GetActionNode();
	
	if(parentNode == NULL)
	{
		return;
	}

	QString childName(parent->text(0));
	QString child("_Child");

	childName = childName + child;

	//
	// Now Actually Build the structure
	//
	ActionNodePlayable* actionNode = new ActionNodePlayable(childName.ascii());
	parentNode->AddChildActionNode(actionNode);

    //
    // Now Build the View
    //
	int sortOrder =	parentNode->GetChildCount();
    ActionNodePlayableTreeView* actionNodeView = new ActionNodePlayableTreeView(actionNode, parent, NULL, sortOrder);	
}

void ActionTree::newBank( TreeNodeView * parent )
{
	ActionNode* parentNode = parent->GetActionNode();
	
	if(parentNode == NULL)
	{
		return;
	}

	QString childName(parent->text(0));
	QString child("_Child");

	childName = childName + child;

	//
	// Now Actually Build the structure
	//
	ActionNodeBank* actionNode = new ActionNodeBank(childName.ascii());
	parentNode->AddChildActionNode(actionNode);

    //
    // Now Build the View
    //
	int sortOrder =	parentNode->GetChildCount();
	new ActionNodeBankTreeView(actionNode, parent, NULL, sortOrder);
}

void ActionTree::newFileReferenceBank( TreeNodeView * parent )
{
	ActionNode* parentNode = parent->GetActionNode();
	
	if(parentNode == NULL)
	{
		return;
	}

    QString fileName = QFileDialog::getOpenFileName(
        g_theApp->GetFileProperties()->GetScriptPath(),
        "Action File (*.act)",
        this,
        "New file Reference dialog",
        "Choose a File to reference" );

    if(fileName.ascii())
    {
	    //
	    // Now Actually Build the structure... the name will come from the file Reference
	    //
	    ActionNodeFileReference* actionNode = new ActionNodeFileReference(NULL);
	    parentNode->AddChildActionNode(actionNode);
        
        //
        // Pass on the fileName attributes
        //
        actionNode->SetFullPathName(fileName.ascii());

        //
        // Now Build the View
        //
	    int sortOrder =	parentNode->GetChildCount();
        ActionNodeFileReferenceTreeView* nodeRefView = new ActionNodeFileReferenceTreeView(actionNode, parent, NULL, sortOrder);

        //
        // Somehow this seems weird to me... 
        // the resolve could be through the ActionNodeFileReference...
        //
        nodeRefView->Resolve();
        newNodeView( nodeRefView );
    }
}


void ActionTree::deleteNodeFromSelection()
{
	if(m_SelectedItem && m_SelectedItem!=g_GlobalBank)
	{
		ActionNode* actionNode = m_SelectedItem->GetActionNode();
		ActionNode* parentNode = actionNode->GetParent();
        
		// Don't allow deletion of the root node
		if(!parentNode)
			return;

        //
        // So the view can be released from the node to be deleted 
        //
        g_testNodeView->SetActionNode(NULL);

		if(parentNode)
		{
			//
			// need to break this branch
			//
			parentNode->RemoveChildActionNode(actionNode);

		}
		else
		{
			rootActionTree = NULL;
			delete actionNode;
		}

		//
		// Now delete the whole view
		//
		delete m_SelectedItem;
		m_SelectedItem = NULL;

		// Now select the current item, for keyboard power users:
		QListViewItem *nextItem = ActionTreeView->currentItem();
		ActionNode* nextActionNode = nextItem ? dynamic_cast<TreeNodeView*>(nextItem)->GetActionNode() : NULL;
		SelectActionNode(nextActionNode);
	}
}

template <class viewType>  
viewType* CreateView
(
    QWidget* parent,
    const char* name,
    const char* caption
)
{
    viewType* newDataMembersView;
    newDataMembersView = new viewType(parent, name, Qt::WType_TopLevel);
    newDataMembersView->setCaption(QString(caption));
    newDataMembersView->m_ViewProperties = g_theApp->GetCreateViewProperties(newDataMembersView->name());
    newDataMembersView->SyncWithViewProperies();
    return(newDataMembersView);
}

template <class viewType>  
void ConnectActivationAction
(
    viewType* view,
    QAction* activationAction
)
{
    if(activationAction)
    {
        if(!view->isHidden() && !view->isMinimized())
        {
            activationAction->toggle();
        }
        QObject::connect( view, SIGNAL(closedSignal( bool)), activationAction, SLOT(toggle()) );
    }
}

void ActionTree::playNodeFromSelected()
{
	m_TreeViewPopup->hide();
}

void ActionTree::init()
{    
    g_ActionTree = this;

	//
	// Initialize the Tracks and conditions and ActionNOdes
	//
	ActionNode::Init();

    //
    // Show the Global BANK
    //
    ActionNodeBank* globalNode = ActionNode::GetRoot();

    if(globalNode)
    {
	    g_GlobalBank  = new ActionNodeBankTreeView(globalNode, ActionTreeView, NULL, 0);
    }

	//
	// Nothing selected... or the default firstIndex for tracks and conditions
	//
	m_SelectedItem = 0;
	m_conditionViewIndex = 0;
	m_taskViewIndex = 0;

	//
	// Disable node sorting
	//
	//ActionTreeView->setSorting(-1);

	//
	// All popup menus
	//
	m_TreeViewPopup = new QPopupMenu(this, "TreeViewPopup");
	m_TreeViewPopup->insertItem( "New &ActionNode",	        this, SLOT(newNodeFromSelected()),              CTRL+Key_A			);
	m_TreeViewPopup->insertItem( "New &Bank",	            this, SLOT(newBankFromSelected()),              CTRL+Key_B			);
	m_TreeViewPopup->insertItem( "New &File Reference",	    this, SLOT(newFileReferenceBankFromSelected()), CTRL+Key_F			);
	m_TreeViewPopup->insertItem( "Resolve File Reference",  this, SLOT(resolveFileReferenceBank())								);
	m_TreeViewPopup->insertItem( "Rename Node"  ,	        this, SLOT(renameSelected()),					Key_F2				);
	m_TreeViewPopup->insertItem( "Copy Node Path"  ,	    this, SLOT(copySelectedNodeFullPath()),			CTRL+SHIFT+Key_C	);
	m_TreeViewPopup->insertItem( "Cu&t"  ,		            this, SLOT(editCut()),							CTRL+Key_X			);
	m_TreeViewPopup->insertItem( "&Copy"  ,		            this, SLOT(editCopy()),							CTRL+Key_C			);
	m_TreeViewPopup->insertItem( "&Paste",		            this, SLOT(editPaste()),						CTRL+Key_V			);
	m_TreeViewPopup->insertItem( "&Delete",		            this, SLOT(deleteNodeFromSelection()),			Key_Delete			);
	m_TreeViewPopup->insertItem( "Delete",					this, SLOT(deleteNodeFromSelection()),			CTRL+Key_D			);
	m_TreeViewPopup->insertItem( "Move &Up",				this, SLOT(incrementSelectedPriority()),		CTRL+Key_Up			);
	m_TreeViewPopup->insertItem( "Move Dow&n",				this, SLOT(decrementSelectedPriority()),		CTRL+Key_Down		);

    //
    // Hack... view Properties for the ActionTreeView
    //
    m_ViewProperties = g_theApp->GetCreateViewProperties(name());
    this->SyncWithViewProperies();
    this->setDockEnabled( Qt::DockBottom, true ); 

	//
	// Set up the Sub component Views
	// 
    g_testNodeView          = CreateView<ActionNodeView> ( this, "ActionNodeView",      "ActionNode View");
    m_ConditionViewNew      = CreateView<DataMembersView>( this, "ConditionView",       "Condition View");
    m_TracksViewNew         = CreateView<DataMembersView>( this, "TracksView",          "Tracks View");
    g_UnitsView             = CreateView<DataMembersView>( this, "Units",               "Units");  
    g_FindQueryView         = CreateView<DataMembersView>( this, "FindQuery",           "Find Query");
    g_FindQueryResultsView  = CreateView<DataMembersView>( this, "FindQueryResults",    "Find Query Results");
    g_FilePropertiesView    = CreateView<DataMembersView>( this, "FilePropertiesView",  "File Edit Properties");
    g_SpewWindow            = CreateView<SpewWindow>     ( this, "SpewWindow",          "SpewWindow");
    //m_tracksTimingViewNew   = CreateView<TimeDataMembersView>( this, "TracksTimingView","Track Timing View");

	//
	// Remote watching of running actionControllers AND motionTrees
	//
    g_ActionControllerView  = CreateView<ActionControllerView>( this, "ActionControllerView", "ActionControllerView");
    g_ActionControllerOwnerView = CreateView<ActionControllerOwnerView>( this, "ActionControllerOwnerView", "ActionControllerOwnerView");

	//
	// To remote watch MotionTrees
	//
	g_MotionTreeView = CreateView<MotionTreeView>( this, "MotionTreeView", "MotionTreeView");
//	g_MotionTreeEditView = CreateView<MotionTreeEditView>( this, "MotionTreeEditView", "MotionTreeEditView");

    //
    // to recieve notification of messages
    //
    g_theApp->Attach(g_ActionControllerOwnerView);
//	g_theApp->Attach(g_MotionTreeView);

    g_UnitsView->refresh(g_theApp->GetUnits());
    g_FindQueryView->refresh(g_theApp->GetFindQuery());
    g_FilePropertiesView->refresh(g_theApp->GetFileProperties());

    //
    // This is just so they can fill out with some colour in the pain
    //
    g_FindQueryResultsView->ActiveLayout();
    m_ConditionViewNew->ActiveLayout();
    m_TracksViewNew->ActiveLayout();
    g_FilePropertiesView->ActiveLayout();
    //m_tracksTimingViewNew->ActiveLayout();

    //
    // For hooking up the menu actions state
    //
    ConnectActivationAction<ActionNodeView>(g_testNodeView,         viewShowActionNodeView);
    ConnectActivationAction<DataMembersView>(m_ConditionViewNew,    viewShowConditionView);
    ConnectActivationAction<DataMembersView>(m_TracksViewNew,       viewShowTracksView);
    ConnectActivationAction<DataMembersView>(g_UnitsView,           viewShowUnitsView);
    ConnectActivationAction<DataMembersView>(g_FindQueryView,       viewShowFindQuery);    
    ConnectActivationAction<DataMembersView>(g_FindQueryResultsView,viewShowFindQueryResults);
    ConnectActivationAction<DataMembersView>(g_FilePropertiesView,  viewShowFilePropertiesView);
    ConnectActivationAction<SpewWindow>     (g_SpewWindow,          viewShowSpewView);
    ConnectActivationAction<ActionControllerView>       (g_ActionControllerView,          viewShowActionControllerView);
    ConnectActivationAction<ActionControllerOwnerView>  (g_ActionControllerOwnerView,viewShowActionControllerOwnerView);
	ConnectActivationAction<MotionTreeView>				(g_MotionTreeView,viewShowMotionTreeView);
//	ConnectActivationAction<MotionTreeEditView>			(g_MotionTreeEditView,viewShowMotionTreeView);
	
//    ConnectActivationAction<TimeDataMembersView>(m_tracksTimingViewNew, viewShowTrackTimingView);


    //
    // Connect interview signal slots for updateing
    //
    connect( g_UnitsView, SIGNAL(updated(void)), g_testNodeView, SLOT(refresh(void)) );
    connect( g_testNodeView->m_conditionView, SIGNAL(updatedDataMembers(IDataMembers*)), m_ConditionViewNew, SLOT(refresh( IDataMembers * )) );
    connect( g_testNodeView->m_tracksView	, SIGNAL(updatedDataMembers(IDataMembers*)), m_TracksViewNew,    SLOT(refresh( IDataMembers * )) );

    //
    // Now process the commandline if any
    //
    int argc = qApp->argc();
    char **argv = qApp->argv();
    for ( int i = 1; i < argc; i++ )  // a.argc() == argc
    {
        fileOpen(argv[i]); 
    }

    //
    // Simulate a Running Game
    //
    QTimer *timer = new QTimer( this );
    connect( timer, SIGNAL(timeout()), this, SLOT(Update()) );
    timer->start( g_UpdateTimeMilliSeconds ); // 16ms single-shot timer

}

void ActionTree::showActionNode( bool on )
{
	if(on)
	{
		g_testNodeView->showNormal();
 	}
	else
	{
		g_testNodeView->hide();
	}
}

void ActionTree::showCondition(bool on)
{
	if(on)
	{
		m_ConditionViewNew->showNormal();
        m_ConditionViewNew->ActiveLayout();
	}
	else
	{
		m_ConditionViewNew->hide();
	}
}

void ActionTree::showTracks( bool on )
{
	if(on)
	{
		m_TracksViewNew->showNormal();
        m_TracksViewNew->ActiveLayout();
	}
	else
	{
		m_TracksViewNew->hide();
	}
}


void ActionTree::showTracksTiming( bool on )
{
/*
	if(on)
	{
		m_tracksTimingViewNew->showNormal();
	}
	else
	{
		m_tracksTimingViewNew->hide();
	}
*/
}

void ActionTree::showUnits( bool on )
{
	if(on)
	{
		g_UnitsView->showNormal();
        g_UnitsView->ActiveLayout();
	}
	else
	{
		g_UnitsView->hide();
	}
}

void ActionTree::showFindQuery( bool on )
{
	if(on)
	{
		g_FindQueryView->showNormal();
        g_FindQueryView->ActiveLayout();
	}
	else
	{
		g_FindQueryView->hide();
	}
}


void ActionTree::showFindQueryResults( bool on )
{
	if(on)
	{
		g_FindQueryResultsView->showNormal();
        g_FindQueryResultsView->ActiveLayout();
	}
	else
	{
		g_FindQueryResultsView->hide();
	}
}

void ActionTree::showFileProperties( bool on )
{
    if(on)
    {
        g_FilePropertiesView->showNormal();
        g_FilePropertiesView->ActiveLayout();
    }
    else
    {
        g_FilePropertiesView->hide();
    }
}

void ActionTree::showSpew( bool on )
{
    if(on)
    {
        g_SpewWindow->showNormal();
        g_SpewWindow->repaint();
    }
    else
    {
        g_SpewWindow->hide();
    }
}

void ActionTree::nodeRenamed( QListViewItem * item, int col )
{
	QString newName = item->text(col);

	TreeNodeView* actionNodeView = dynamic_cast<TreeNodeView*>(item);	
	if(actionNodeView)
	{
        actionNodeView->SetName(newName.ascii(), col);
	}
}

void ActionTree::newNodeView( TreeNodeView * parent )
{
	ActionNode* parentNode = parent->GetActionNode();
	
	if(parentNode == NULL)
	{
		return;
	}

	int childCount = parentNode->GetChildCount();

	for(int i=0;i<childCount;i++)
	{
		ActionNode* childNode = parentNode->GetChild(i);

		TreeNodeView* actionNodeView = NULL;

		//
		// Construct the correct view
		//
		if(ActionNodePlayable* node = dynamic_cast<ActionNodePlayable*>(childNode))
		{
			ActionNodePlayableTreeView* actionNodePlayableView = new ActionNodePlayableTreeView(node, parent, NULL, i);	
			actionNodeView = actionNodePlayableView;
		}
		else 
		if(ActionNodeBank* bank = dynamic_cast<ActionNodeBank*>(childNode))
		{
			ActionNodeBankTreeView* actionNodeBankView = new ActionNodeBankTreeView(bank, parent, NULL, i);	
			actionNodeView = actionNodeBankView;
		}
		else
		if(ActionNodeFileReference* fileRef = dynamic_cast<ActionNodeFileReference*>(childNode))
		{
            ActionNodeFileReferenceTreeView* archiveFileRecordTreeView = new ActionNodeFileReferenceTreeView(fileRef, parent, NULL, i);	
			actionNodeView = archiveFileRecordTreeView;
		}
		//
		// Now recurse for decendants
		//
		newNodeView(actionNodeView);
	}
}

#include <qstatusbar.h> 
void ActionTree::Update()
{
    static QTime resetTime = QTime::currentTime();
    static QTime time = QTime::currentTime();
    static int maxMS = 0;

    //
    // Not sure if I need this
    //
    QWidget* fucker = QWidget::keyboardGrabber();

    g_theApp->Update(g_UpdateTimeSeconds);

    g_ActionControllerOwnerView->Update();
    g_ActionControllerView->Update();
	g_MotionTreeView->Update();

    int ms = time.elapsed();
    maxMS = (maxMS>ms)?maxMS:ms;

    QStatusBar* status = statusBar();
    if(status)
    {
        QString timerMessage("Update::");       
        timerMessage = timerMessage + QString::number(ms) + QString("/") + QString::number(maxMS);       
        status->message( timerMessage );
    }
    
    static int resetThreshold = 30000; // 30 seconds
    if(resetTime.elapsed()>resetThreshold)
    {
        resetTime.restart();
        maxMS = 0;
    }

    time.restart();
}


void ActionTree::activateTreeViewPopUp( QListViewItem * item, const QPoint & position, int id )
{

	clicked( item, position, id);

	m_TreeViewPopup->move( position );
	m_TreeViewPopup->show();

}

void ActionTree::decrementSelectedPriority()
{
    changeSelectedPriority( false );
}
void ActionTree::incrementSelectedPriority()
{
    changeSelectedPriority( true );
}

bool ActionTree::close( bool alsoDelete )
{
    this->SaveViewProperies();
    m_ConditionViewNew->SaveViewProperies();
    m_TracksViewNew->SaveViewProperies();
    g_testNodeView->SaveViewProperies();
    g_UnitsView->SaveViewProperies();
    g_FindQueryView->SaveViewProperies();
    g_FindQueryResultsView->SaveViewProperies();
//    m_tracksTimingViewNew->SaveViewProperies();
    g_FilePropertiesView->SaveViewProperies();
    g_SpewWindow->SaveViewProperies();
    g_ActionControllerView->SaveViewProperies();
    g_ActionControllerOwnerView->SaveViewProperies();
	g_MotionTreeView->SaveViewProperies();
//	g_MotionTreeEditView->SaveViewProperies();

    QStyle& currentStyle = QApplication::style(); 

    if(dynamic_cast<QCDEStyle*>(&currentStyle))
    {
        g_theApp->GetApplicationPreferences()->SetStringViewStyle("QCDEStyle");
    }
    else
    if(dynamic_cast<QWindowsStyle*>(&currentStyle))
    {
        g_theApp->GetApplicationPreferences()->SetStringViewStyle("QWindowsStyle");
    }
    else
    if(dynamic_cast<QPlatinumStyle*>(&currentStyle))
    {
        g_theApp->GetApplicationPreferences()->SetStringViewStyle("QPlatinumStyle");
    }
    else
    {
        g_theApp->GetApplicationPreferences()->SetStringViewStyle("QSGIStyle");
    }

    g_theApp->WriteConfig();
    g_theApp->DeInit();

    m_ConditionViewNew->close(false);
    m_TracksViewNew->close(false);
    g_testNodeView->close(false);
    g_UnitsView->close(false);
    g_FindQueryView->close(false);
    g_FindQueryResultsView->close(false);
 //   m_tracksTimingViewNew->close(false);
    g_FilePropertiesView->close(false);
    g_SpewWindow->close(false);
    g_ActionControllerView->close(false);
    g_ActionControllerOwnerView->close(false);
	g_MotionTreeView->close(false);
//	g_MotionTreeEditView->close(false);

    //
    // Releases internal Widgets... 
    // some of these have pointers to IDataMembers 
    // so remove the widgets before the ActionNodes get deleted
    // Better to have reference counting... it'll happen
    //
    m_ConditionViewNew->clear();
    m_TracksViewNew->clear();
//    g_testNodeView->clear();
    g_UnitsView->clear();
    g_FindQueryView->clear();
    g_FindQueryResultsView->clear();
//    m_tracksTimingViewNew->clear();
    g_FilePropertiesView->clear();

    return(QMainWindow::close(alsoDelete));
}



void ActionTree::changeSelectedPriority( bool increase )
{
    if(m_SelectedItem)
	{
        int priority = -1;
		ActionNode* actionNode = m_SelectedItem->GetActionNode();
        if(actionNode)
        {
            priority = actionNode->ChangePriority(increase);
            if(priority>=0)
            {

                QListViewItem* parentItem = m_SelectedItem->parent();
                if(parentItem)
                {
                    parentItem->sort();
                }
            }
        }
    }
}

void openToItem(QListViewItem * item)
{
    QListViewItem* currentItem = item->parent();

    while(currentItem)
    {
        currentItem->setOpen(true);
        currentItem = currentItem->parent();
    }
}

void ActionTree::selectedNode( QListViewItem * item )
{
	m_SelectedItem = dynamic_cast<TreeNodeView*>(item);	

	if(m_SelectedItem)
	{
        openToItem( item ); //ActionTreeView->setOpen(item, true );
        ActionNode* actionNode = m_SelectedItem->GetActionNode();
		g_testNodeView->SetActionNode(actionNode);
//		m_tracksTimingViewNew->SetActionNode(actionNode);
	}
}


bool ActionTree::SelectActionNode( ActionNode * actionNode )
{
    bool found = false;
    if(actionNode)
    {
        QListViewItemIterator it( ActionTreeView );
        TreeNodeView* currentTreeNodeItem = NULL;

        while( it.current() && found == false) 
        {
            if( currentTreeNodeItem = dynamic_cast<TreeNodeView*>(it.current()) )
            {
                if(currentTreeNodeItem->GetActionNode() == actionNode)
                {
                    //
                    // Now Select it!!! and stop the search
                    //
                    ActionTreeView->setSelected(it.current(), true );
                    ActionTreeView->ensureItemVisible ( it.current() );
                    found = true;
                } 
            }

            //
            // Next
            //
            ++it;
        }
    }

    return(found);
}

void ActionTree::copySelectedNodeFullPath()
{
	if(m_SelectedItem)
	{
		ActionNode* actionNode = m_SelectedItem->GetActionNode();

		if(actionNode)
		{			
			QClipboard *clipBoard = QApplication::clipboard();

			//
			// Copy text into the clipboard
			//
            char fullPathName[1024];
            actionNode->GetNameFullPath(fullPathName, 1024);
			clipBoard->setText( fullPathName );
		}
	}
}


void ActionTree::resolveFileReferenceBank()
{
	if(m_SelectedItem)
	{
		ActionNodeFileRecordTreeView* fileRefNodeView = dynamic_cast<ActionNodeFileRecordTreeView*>(m_SelectedItem);

		if(fileRefNodeView)
		{	
            fileRefNodeView->Resolve();
            ActionNode* newBranch = fileRefNodeView->GetActionNode();
            if(newBranch)
            {
               newNodeView( m_SelectedItem );
            }
        }
    }
}

void ActionTree::renameSelected()
{
	if(m_SelectedItem)
	{
        m_SelectedItem->startRename(0);
    }
}


QDragObject* ActionTree::dragObject()
{
    if(m_SelectedItem)
	{
        //QDragObject*

    }
    return(NULL);
}


void ActionTree::handleDropped( QDropEvent * e )
{

}
#include <direct.h>
extern	bool BuildFullPath(const char* path_from, const char*  relative_path, char* absolutePathBuffer);
void ActionTree::fileOpen( const char * filename )
{
    ArchiveFileRecord* archiveFile = new ArchiveFileRecord(filename);

    //
    // Hack to Remember the Path
    //
    g_theApp->GetFileProperties()->SetScriptPath(archiveFile->GetPathName()->GetString());
    g_theApp->GetFileProperties()->Notify();

    const CBasicString* fullPathName = archiveFile->GetFullPathName();  
    QString fileNameQString(fullPathName->GetString());

    QString message;
    QString loading = "<font color=green><b>LOADING......:</b></font>";
    message = loading + fileNameQString;
    g_SpewWindow->appendPostTime(message);

    //
    // Load the Data
    //
    archiveFile->ReadFromFile();

    //
    // Used to be done inside ReadFromFile
    //
    ActionNodeBank* rootActionNode = dynamic_cast<ActionNodeBank*>(archiveFile->GetArchiveObject());
    if(rootActionNode)
    {
        ActionNode::AddRoot(rootActionNode);
        rootActionNode->ResolveTrackReferences();

        //
        // Add a View for the FileName();
        //
        ArchiveFileRecordTreeView* fileView = new ArchiveFileRecordTreeView(archiveFile, g_GlobalBank, NULL, rootActionNode->GetPriority());

        //
        // Recurse the rest of the view
        //
        newNodeView( fileView );  
        
        QString message;
        QString loaded = "<font color=green><b>LOADED:</b></font>";
        message = loaded + fileNameQString;
        g_SpewWindow->appendPostTime(message);
    }
    else
    {
        QString message;
        QString loadedFailed = "<font color=red><b>LOAD FAILED</b></font>";
        message = loadedFailed + fileNameQString;
        g_SpewWindow->appendPostTime(message);
    }
}

void ActionTree::SyncWithViewProperies( void )
{
    if(m_ViewProperties)
    {
        move( m_ViewProperties->GetXposition(),  m_ViewProperties->GetYposition());
        resize(m_ViewProperties->GetWidth(), m_ViewProperties->GetHeight());
    }
}

void ActionTree::SaveViewProperies( void )
{
    if(m_ViewProperties)
    {
        m_ViewProperties->SetXposition(x());
        m_ViewProperties->SetYposition(y());

        m_ViewProperties->SetHeight(height());
        m_ViewProperties->SetWidth(width());
    }
}

void ActionTree::fileSaveAs()
{

}


void ActionTree::fileSaveAs( QListViewItem * item, const QPoint & position, int id )
{

}


void ActionTree::changeFileName( ArchiveFileRecord * fileRecord )
{
    QString fileName;
    bool bFileExists = false;

    do 
    {
        fileName = QFileDialog::getSaveFileName(
            g_theApp->GetFileProperties()->GetScriptPath(),
            "Action File (*.act)",
            this,
            "File Save As dialog",
            "Choose a fileName and Path" );

        FILE *fileExists = NULL;

        if(fileName.ascii())
        {
            fileExists = fopen( fileName.ascii(), "r" );

            if(fileExists)
            {   
                fclose(fileExists);
                bFileExists = true;

                QString caption = fileName + " File Already Exists";
                QString text = caption + " please pick another filename\n";

                QMessageBox::warning( this, caption, text, QMessageBox::Ok, QMessageBox::NoButton);
            }
            else
            {
                bFileExists = false;
            }
        }

    } while(fileName.ascii() && bFileExists);


    //
    // Here you should have a valid FileName that doesn't already exist
    //
    if(fileName.ascii())
    {
        fileRecord->SetFullPathName(fileName.ascii());

        //
        // Shitty Part is now I have to find the fucking view to update it
        //
        QListViewItemIterator it( ActionTreeView );
        ActionNodeFileRecordTreeView* currentTreeNodeItem = NULL;
        bool found = false;

        while( it.current() && found == false) 
        {
            if( currentTreeNodeItem = dynamic_cast<ActionNodeFileRecordTreeView*>(it.current()) )
            {
                if(currentTreeNodeItem->GetArchiveFileRecord() == fileRecord)
                {
                    found = true;
                    //
                    // Now update the fucking View
                    //
                    currentTreeNodeItem->SyncronizeWithNode();
                } 
            }

            //
            // Next
            //
            ++it;
        }
    }
}

void ActionTree::ActivateAllLayouts()
{
    m_ConditionViewNew->ActiveLayout();
    m_TracksViewNew->ActiveLayout();
//    g_testNodeView->ActiveLayout();
    g_UnitsView->ActiveLayout();
    g_FindQueryView->ActiveLayout();
    g_FindQueryResultsView->ActiveLayout();
//    m_tracksTimingViewNew->ActiveLayout();
    g_FilePropertiesView->ActiveLayout();
 
    g_SpewWindow->updateContents();
    g_SpewWindow->repaintContents(); 
}

void ActionTree::SetStyleWinDoze()
{
    bool useHighlightCols = false;
    QStyle* style = new QWindowsStyle;
    if(style)
    {
        QApplication::setStyle( style );
        ActivateAllLayouts();
    }
}


void ActionTree::SetStyleSGI()
{
    bool useHighlightCols = false;
    QStyle* style = new QSGIStyle(useHighlightCols);
    if(style)
    {
        QApplication::setStyle( style );
        ActivateAllLayouts();
    }
}


void ActionTree::SetStyleCDE()
{
    bool useHighlightCols = false;
    QStyle* style = new QCDEStyle(useHighlightCols);
    if(style)
    {
        QApplication::setStyle( style );
        ActivateAllLayouts();
    }
}


void ActionTree::SetStylePlatinum()
{
    bool useHighlightCols = false;
    QStyle* style = new QPlatinumStyle;
    if(style)
    {
        QApplication::setStyle( style );
        ActivateAllLayouts();
    }
}

void ActionTree::showActionController( bool on )
{
    if(on)
    {
        g_ActionControllerView->showNormal();
    }
    else
    {
        g_ActionControllerView->hide();
    }
}


void ActionTree::showActionControllerOwner( bool on )
{
    if(on)
    {
        g_ActionControllerOwnerView->showNormal();
    }
    else
    {
        g_ActionControllerOwnerView->hide();
    }
}

/*
void ActionTree::toggleReferenceInclude()
{
    if(m_SelectedItem)
    {
        ActionNodeFileReferenceTreeView* fileRef = dynamic_cast<ActionNodeFileReferenceTreeView*>(m_SelectedItem);
        if(fileRef)
        {
            fileRef->ToggleRefInclude();
        }
    }
}
*/


void ActionTree::showMotionTree( bool on )
{
    if(on)
    {
        g_MotionTreeView->showNormal();
//		g_MotionTreeEditView->showNormal();
    }
    else
    {
        g_MotionTreeView->hide();
//		g_MotionTreeEditView->hide();
    }
}
