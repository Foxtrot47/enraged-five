unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}
FORMS	= tracktimingsliding.ui \
	actiontree.ui \
	conditionview.ui \
	datamembercheckbox.ui \
	datamembercreateview.ui \
	datamemberenum.ui \
	datamemberslider.ui \
	datamemberstring.ui \
	datamembervector3.ui \
	keyposetracktiming.ui \
	testappdialogbase.ui \
	trackslistcreateview.ui
IMAGES	= ../DrVart/DrVIcon.png \
	../DrVart/filenew.png \
	../DrVart/fileopen.png \
	../DrVart/filesave.png \
	../DrVart/next.png \
	
TEMPLATE	=app
CONFIG	+= qt warn_on release
LANGUAGE	= C++
