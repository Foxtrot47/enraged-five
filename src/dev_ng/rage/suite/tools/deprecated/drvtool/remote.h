#ifndef REMOTE_CPP
#define REMOTE_CPP

class RemoteMgr
{
public:
	static bool Connect(const char *hostname);
	static bool Disconnect();
    static void SendMsg(const char* msg);
};

#endif 
