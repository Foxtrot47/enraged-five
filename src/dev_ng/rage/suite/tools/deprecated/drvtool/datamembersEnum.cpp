#include "datamembersEnum.h"

//----------------------------------------------------------------------
// Widget for Enum DataMembers
//----------------------------------------------------------------------


DataMemberEnumComboBox::DataMemberEnumComboBox( QWidget* parent, const char* name)
:
QComboBox(parent, name),
m_dataMemberInterface(0),
m_dataMembersInstance(0)
{
	//
	// Not allowed to edit this shit
	//
	setEditable( false );

	//
	// Set the enum whenever a new item in the widget a newly activate value 
	//
	//QComboBox* comboBoxPtr = (QComboBox*)this;
	connect( this, SIGNAL( activated ( int ) ), this, SLOT( SetValue(int) ) );

}

void DataMemberEnumComboBox::SetDataMember( IDataMembers * dataMember, IEnumDataMember * dataMemberInterface )
{
    m_dataMemberInterface = dataMemberInterface;
    m_dataMembersInstance = dataMember;

	//
	// Need to populate this this ComboBox with all the enum values
	//
	int numEnums = m_dataMemberInterface->GetNumEnums(); //(void){return(sizeof(m_enumName)/sizeof(char*));}

	for(int i=0;i<numEnums;i++)
	{
		insertItem( QString(m_dataMemberInterface->GetEnumName(i) ) );
	}
	
	int currentEnum = m_dataMemberInterface->GetEnum(m_dataMembersInstance);

	setCurrentItem( currentEnum );
}

void DataMemberEnumComboBox::SetValue( int value )
{
	m_dataMemberInterface->SetEnum(m_dataMembersInstance, value);
}

