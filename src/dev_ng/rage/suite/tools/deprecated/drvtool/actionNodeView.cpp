#include "actionNodeView.hpp"
#include "allActionTree.h"
#include "drvToolInclude/application.h"
#include "datamembersliderderived.hpp"

#include <qpixmap.h>
#include <qlineedit.h>
#include <qlayout.h>
#include <qlistbox.h>
#include <qcombobox.h>
#include <qpushbutton.h>
#include <qpopupmenu.h> 
#include <qsplitter.h> 
#include <qlabel.h> 
#include <qapplication.h>
#include <qclipboard.h>
#include <qbitmap.h> 
#include <qregexp.h>

//#include "remote.h"

static const char* folder_closed_xpm[]={
    "16 16 9 1",
    "g c #808080",
    "b c #c0c000",
    "e c #c0c0c0",
    "# c #000000",
    "c c #ffff00",
    ". c None",
    "a c #585858",
    "f c #a0a0a4",
    "d c #ffffff",
    "..###...........",
    ".#abc##.........",
    ".#daabc#####....",
    ".#ddeaabbccc#...",
    ".#dedeeabbbba...",
    ".#edeeeeaaaab#..",
    ".#deeeeeeefe#ba.",
    ".#eeeeeeefef#ba.",
    ".#eeeeeefeff#ba.",
    ".#eeeeefefff#ba.",
    ".##geefeffff#ba.",
    "...##gefffff#ba.",
    ".....##fffff#ba.",
    ".......##fff#b##",
    ".........##f#b##",
    "...........####."};

static const char* folder_open_xpm[]={
    "16 16 11 1",
    "# c #000000",
    "g c #c0c0c0",
    "e c #303030",
    "a c #ffa858",
    "b c #808080",
    "d c #a0a0a4",
    "f c #585858",
    "c c #ffdca8",
    "h c #dcdcdc",
    "i c #ffffff",
    ". c None",
    "....###.........",
    "....#ab##.......",
    "....#acab####...",
    "###.#acccccca#..",
    "#ddefaaaccccca#.",
    "#bdddbaaaacccab#",
    ".eddddbbaaaacab#",
    ".#bddggdbbaaaab#",
    "..edgdggggbbaab#",
    "..#bgggghghdaab#",
    "...ebhggghicfab#",
    "....#edhhiiidab#",
    "......#egiiicfb#",
    "........#egiibb#",
    "..........#egib#",
    "............#ee#"};

static const char * folder_locked[]={
    "16 16 10 1",
    "h c #808080",
    "b c #ffa858",
    "f c #c0c0c0",
    "e c #c05800",
    "# c #000000",
    "c c #ffdca8",
    ". c None",
    "a c #585858",
    "g c #a0a0a4",
    "d c #ffffff",
    "..#a#...........",
    ".#abc####.......",
    ".#daa#eee#......",
    ".#ddf#e##b#.....",
    ".#dfd#e#bcb##...",
    ".#fdccc#daaab#..",
    ".#dfbbbccgfg#ba.",
    ".#ffb#ebbfgg#ba.",
    ".#ffbbe#bggg#ba.",
    ".#fffbbebggg#ba.",
    ".##hf#ebbggg#ba.",
    "...###e#gggg#ba.",
    ".....#e#gggg#ba.",
    "......###ggg#b##",
    ".........##g#b##",
    "...........####."};

/* XPM */
char *s_red_exclamation[]={
    "16 16 39 1",
    ". c None",
    "D c #000000",
    "z c #000000",
    "B c #000000",
    "v c #000000",
    "# c #000000",
    "C c #000000",
    "o c #000000",
    "k c #000000",
    "d c #000000",
    "A c #451212",
    "c c #531616",
    "a c #564747",
    "y c #6e1d1d",
    "F c #832323",
    "w c #892828",
    "E c #897171",
    "u c #a02b2b",
    "q c #a22b2b",
    "n c #ab2d2d",
    "b c #c17070",
    "t c #c65c5c",
    "p c #c66c6c",
    "r c #c67171",
    "j c #d33838",
    "l c #d46464",
    "K c #d63939",
    "g c #e53e3e",
    "I c #ea3f3f",
    "x c #f34141",
    "J c #f64242",
    "m c #fb4343",
    "i c #fe4444",
    "s c #ff4444",
    "H c #ff6d6d",
    "f c #ff8787",
    "h c #ff8a8a",
    "G c #ffb1b1",
    "e c #ffc7c7",
    "......#abc#.....",
    "......defgd.....",
    "......dhijd.....",
    "......klmnk.....",
    "......opiqo.....",
    "......orsqo.....",
    "......orsqo.....",
    "......otmuo.....",
    "......vwxyv.....",
    "......zcnAz.....",
    ".......BCB......",
    "......DvCvD.....",
    "......#EbF#.....",
    "......dGHId.....",
    "......dJgKd.....",
    "......#ddd#....."};

static const char * pix_file []={
    "16 16 7 1",
    "# c #000000",
    "b c #ffffff",
    "e c #000000",
    "d c #404000",
    "c c #c0c000",
    "a c #ffffc0",
    ". c None",
    "................",
    ".........#......",
    "......#.#a##....",
    ".....#b#bbba##..",
    "....#b#bbbabbb#.",
    "...#b#bba##bb#..",
    "..#b#abb#bb##...",
    ".#a#aab#bbbab##.",
    "#a#aaa#bcbbbbbb#",
    "#ccdc#bcbbcbbb#.",
    ".##c#bcbbcabb#..",
    "...#acbacbbbe...",
    "..#aaaacaba#....",
    "...##aaaaa#.....",
    ".....##aa#......",
    ".......##......."};

/* XPM */
const static char *reference_xpm[]={
"16 16 3 1",
"a c #000000",
"# c #6fb504",
". c None",
"................",
"................",
"................",
"................",
"................",
"................",
"................",
".........#......",
".........###....",
"##############..",
"################",
"##############aa",
"aaaaaaaaa###aa..",
".........#aa....",
".........a......",
"................"};


QPixmap stackPix(const QPixmap* base, QPixmap* layer)
{
    QPixmap canvis(base->width(), base->height(), base->depth());
    canvis.fill(); // fill white
    bitBlt( &canvis, QPoint(0,0), base, base->rect());
    bitBlt( &canvis, QPoint(0,0), layer, layer->rect());
    canvis.setMask( canvis.createHeuristicMask() );

    return(canvis);
}

extern Application* g_theApp;

//------------------------------------------------------------------------------
// TrackTreeNode
//------------------------------------------------------------------------------

TrackTreeNode::TrackTreeNode(TrackOrCondition * pTrack, QListView * pParent, QListViewItem * pInsertAfter)
: QListViewItem(pParent, pInsertAfter)
, m_pTrack(pTrack)
{
	Init();
}

TrackTreeNode::TrackTreeNode(TrackOrCondition * pTrack, QListViewItem * pParent, QListViewItem * pInsertAfter)
: QListViewItem(pParent, pInsertAfter)
, m_pTrack(pTrack)
{
	Init();
}

void TrackTreeNode::Init()
{
	setRenameEnabled(0, false);
	setOpen(true);
	QString trackName(m_pTrack->GetName()->GetString());
	setText(0, trackName);

	ConditionGroupCondition * pCGC = dynamic_cast<ConditionGroupCondition*>(m_pTrack);
	if( pCGC )
	{
		int iNumChildren = pCGC->GetConditionCount();
		TrackTreeNode * pPrevNode = 0;
		for( int i=0; i<iNumChildren; ++i )
		{
			Condition * pCondition = dynamic_cast<Condition*>(pCGC->GetCondition(i));
			if( pCondition )
				pPrevNode = new TrackTreeNode(pCondition, this, pPrevNode);
		}
	}
}

//------------------------------------------------------------------------------
// TreeNodeView
//------------------------------------------------------------------------------

TreeNodeView::TreeNodeView(QListView * parent, QListViewItem * after, int sortOrder, int nodecol) 
: 
QListViewItem ( parent, after ),
m_sortOrder(sortOrder),
m_NodeRefColumn(nodecol)
{
}

TreeNodeView::TreeNodeView(QListViewItem * parent, QListViewItem * after, int sortOrder, int nodecol)
: 
QListViewItem ( parent, after ),
m_sortOrder(sortOrder),
m_NodeRefColumn(nodecol)
{
}

int  TreeNodeView::GetSortOrder() const
{
    const ActionNode* actionNode = GetActionNode(); 
    if(actionNode)
    {
        return(actionNode->GetPriority());
    }
    else
    {
	    return( m_sortOrder );
    }
}

QString TreeNodeView::key( int column, bool ascending ) const
{
    const ActionNode* actionNode = GetActionNode(); 
    if(actionNode)
    {
        int sortOrder = actionNode->GetPriority();
        return( QString::number(sortOrder) );
    }
    else
    {
	    return( QString::number(m_sortOrder) );
    }
}

int TreeNodeView::compare ( QListViewItem * i, int col, bool ascending ) const
{
    int thisSortOrder = GetSortOrder();
    int iSortOrder    = thisSortOrder;
    
    TreeNodeView* iTreeNodeView = dynamic_cast<TreeNodeView*>(i);
    if(iTreeNodeView)
    {
        iSortOrder = iTreeNodeView->GetSortOrder();
    }
    
    int sortReturn = 0;

    if(thisSortOrder<iSortOrder)
    {
        sortReturn = -1;
    }
    else
    if(thisSortOrder>iSortOrder)
    {
        sortReturn = 1;
    }
    
    if(!ascending)
    {
        sortReturn *= -1;
    }

    return(sortReturn);
}


//--------------------------------------------------------------------------
// ActionNodeBankTreeView
//--------------------------------------------------------------------------

ActionNodeBankTreeView::ActionNodeBankTreeView
(
    ActionNodeBank* actionNodeBank,
    QListView * parent,  
	QListViewItem * after,
    int sortOrder,
	int nodecol
) 
: 
TreeNodeView ( parent, after, sortOrder, nodecol ),
m_ReferenceNode(actionNodeBank)
{
	init();
    setDragEnabled ( true );
    setDropEnabled ( true );
}

ActionNodeBankTreeView::ActionNodeBankTreeView
(
    ActionNodeBank* actionNodeBank,
    QListViewItem * parent, 
	QListViewItem * after,
    int sortOrder,
	int nodecol
) 
: 
TreeNodeView ( parent, after, sortOrder , nodecol ),
m_ReferenceNode(actionNodeBank)
{
    init();
    setDragEnabled ( true );
    setDropEnabled ( true );
}

void ActionNodeBankTreeView::init(void)
{
	setRenameEnabled( m_NodeRefColumn, true );
	
	if(isOpen())
	{
		static QPixmap folderOpen((const char **)folder_open_xpm);
		setPixmap(m_NodeRefColumn, folderOpen);
	}
	else
	{
		static QPixmap folderClosed((const char **)folder_closed_xpm);
        setPixmap(m_NodeRefColumn, folderClosed);        
	}

	QString bankName(m_ReferenceNode->GetName()->GetString());
	setText(m_NodeRefColumn, bankName);
}

// Check reference node to see if we should be expanded or not
bool ActionNodeBankTreeView::LocalIsOpen()
{
	#if __TOOL
	if (m_ReferenceNode)
		return m_ReferenceNode->m_bViewExpanded;
	#endif
	return false;
}

// Override setOpen so we save value in reference node
void ActionNodeBankTreeView::setOpen(bool o)
{
	#if __TOOL
	if (m_ReferenceNode)
	{
		m_ReferenceNode->m_bViewExpanded = o;
	}
	#endif

	if(o)
	{
		static QPixmap folderOpen((const char **)folder_open_xpm);
		setPixmap(m_NodeRefColumn, folderOpen);
	}
	else
	{
		static QPixmap folderClosed((const char **)folder_closed_xpm);
        setPixmap(m_NodeRefColumn, folderClosed);        
	}

	TreeNodeView::setOpen(o);
}

//--------------------------------------------------------------------------
// ActionNodePlayableTreeView
//--------------------------------------------------------------------------
ActionNodePlayableTreeView::ActionNodePlayableTreeView
(
    ActionNodePlayable* actionNodePlayable, 
    QListView * parent,  
	QListViewItem * after,
    int sortOrder,
	int nodecol
) 
: 
TreeNodeView ( parent, after, sortOrder , nodecol ),
m_ReferenceNode(actionNodePlayable)
{
    init();
}

ActionNodePlayableTreeView::ActionNodePlayableTreeView
(
    ActionNodePlayable* actionNodePlayable,
    QListViewItem * parent,  
	QListViewItem * after,
    int sortOrder,
	int nodecol
) 
: 
TreeNodeView ( parent, after, sortOrder , nodecol ),
m_ReferenceNode(actionNodePlayable)
{
    init();
}

// Override setOpen so we save value in reference node
void ActionNodePlayableTreeView::setOpen(bool bOpen)
{		
	#if __TOOL	
	if (m_ReferenceNode)
	{
		m_ReferenceNode->m_bViewExpanded = bOpen;
	}
	#endif

	TreeNodeView::setOpen(bOpen);
};

// Check reference node to see if we should be expanded or not
bool ActionNodePlayableTreeView::LocalIsOpen()
{
	#if __TOOL
	return m_ReferenceNode->m_bViewExpanded;
	#else
	return false;
	#endif
};

void ActionNodePlayableTreeView::init(void)
{
	setRenameEnabled( m_NodeRefColumn, true );
	setPixmap(m_NodeRefColumn, GetIcon());   
	if(m_ReferenceNode)
	{
		QString nodeName(m_ReferenceNode->GetName()->GetString());
		setText(m_NodeRefColumn, nodeName);
	}
}
const QPixmap& ActionNodePlayableTreeView::GetIcon(void)
{
	static QPixmap playablePixmap = QPixmap::fromMimeSource( "ActionNodePlayable.PNG" );
    return playablePixmap;
}

//--------------------------------------------------------------------------
// ActionNodeFileRecordTreeView
//--------------------------------------------------------------------------
ActionNodeFileRecordTreeView::ActionNodeFileRecordTreeView
(
    QListView * parent,  
	QListViewItem * after,
    int sortOrder,
	int nodecol
)
: 
TreeNodeView ( parent, after, sortOrder , nodecol )
{
}

ActionNodeFileRecordTreeView::ActionNodeFileRecordTreeView
(   
    QListViewItem * parent,  
	QListViewItem * after,
    int sortOrder,
	int nodecol
)
: 
TreeNodeView ( parent, after, sortOrder , nodecol )
{
}

void ActionNodeFileRecordTreeView::init(void)
{
	setRenameEnabled( 0, true );
	setRenameEnabled( 1, true );
	setRenameEnabled( 2, true );

    //
    // This is the File IconImage
    //
	static QPixmap fileIcon((const char **)pix_file);  
    //static QPixmap fileIcon(QPixmap::fromMimeSource( "fileNew.PNG" ));    
	setPixmap(1, fileIcon);

    setOpen(false);

    ArchiveFileRecord*  archiveFileRecord = GetArchiveFileRecord();
    ActionNode*         referenceNode = GetActionNode();
    
    if(referenceNode)
    {
        QString referenceNodeName(referenceNode->GetName()->GetString());
        setText(0, referenceNodeName);
    }

    //
    // Now set the file and path Names
    //
    if(archiveFileRecord)
    {
        g_theApp->GetFileProperties()->AddFileRecord( archiveFileRecord );
 
        QString fileNameView(archiveFileRecord->GetFileName()->GetString());
	    QString pathNameView(archiveFileRecord->GetPathName()->GetString());

	    setText(1, fileNameView);
	    setText(2, pathNameView);
    }
}

void ActionNodeFileRecordTreeView::removeArchiveFileRecordFromApp(void)
{
    //
    // Global HACK
    //
    ArchiveFileRecord*  archiveFileRecord = GetArchiveFileRecord();
    if(archiveFileRecord)
    {
        g_theApp->GetFileProperties()->RemoveFileRecord( archiveFileRecord );
    }
}

void ActionNodeFileRecordTreeView::SyncronizeWithNode(void)
{
    ArchiveFileRecord*  archiveFileRecord = GetArchiveFileRecord();
    if(archiveFileRecord)
    {
        QString fileNameView(archiveFileRecord->GetFileName()->GetString());
        QString pathNameView(archiveFileRecord->GetPathName()->GetString());

        setText(1, fileNameView);
        setText(2, pathNameView);    
    }
}

void ActionNodeFileRecordTreeView::SetName( const char* name, int col )
{
    if(col == 0)
    {
        ActionNode* referenceNode = GetActionNode();
        if(referenceNode)
        {
            referenceNode->GetName()->SetString(name);
        }
    }
	else if(col == 1)
	{
		GetArchiveFileRecord()->SetFileName(name);
	}
	else if(col == 2)
	{
		GetArchiveFileRecord()->SetPathName(name);
	}
}

void ActionNodeFileRecordTreeView::setOpen( bool o)
{
	QListViewItem::setOpen(o);
    updeteIcon();
}

void ActionNodeFileRecordTreeView::updeteIcon(void)
{
    bool o = isOpen();
    if(o)
    {
        static QPixmap folderOpen((const char **)folder_open_xpm);
        setPixmap(0, folderOpen);
    }
    else
    {
        static QPixmap folderClosed((const char **)folder_closed_xpm);
        setPixmap(0, folderClosed);
    }
}

//--------------------------------------------------------------------------
// ArchiveFileRecordTreeView
//--------------------------------------------------------------------------
ArchiveFileRecordTreeView::ArchiveFileRecordTreeView
(
    ArchiveFileRecord* archiveFileRecord, 
    QListView* parent, 
	QListViewItem * after,
    int sortOrder,
	int nodecol
)
:
ActionNodeFileRecordTreeView(parent, after, sortOrder, nodecol ),
m_ArchiveFileRecord(archiveFileRecord)
{
    init();
}
ArchiveFileRecordTreeView::ArchiveFileRecordTreeView
(
    ArchiveFileRecord* archiveFileRecord, 
    QListViewItem* parent, 
	QListViewItem * after,
    int sortOrder,
	int nodecol
)
:
ActionNodeFileRecordTreeView(parent, after, sortOrder, nodecol ),
m_ArchiveFileRecord(archiveFileRecord)
{
    init();
}

ArchiveFileRecordTreeView::~ArchiveFileRecordTreeView()
{
    removeArchiveFileRecordFromApp();
    delete m_ArchiveFileRecord;
}

ActionNodeFileReferenceTreeView::ActionNodeFileReferenceTreeView
(
    ActionNodeFileReference* actionNodeFileReference,   
    QListView* parent, 
	QListViewItem * after,
    int sortOrder,
	int nodecol
)
:
ActionNodeFileRecordTreeView(parent, after, sortOrder, nodecol ),
m_ActionNodeFileReference(actionNodeFileReference)
{
    init();
}

ActionNodeFileReferenceTreeView::ActionNodeFileReferenceTreeView
(
    ActionNodeFileReference* actionNodeFileReference, 
    QListViewItem* parent, 
	QListViewItem * after,
    int sortOrder,
	int nodecol
)
:
ActionNodeFileRecordTreeView(parent, after, sortOrder, nodecol ),
m_ActionNodeFileReference(actionNodeFileReference)
{
    init();
}

ActionNodeFileReferenceTreeView::~ActionNodeFileReferenceTreeView()
{
   removeArchiveFileRecordFromApp();
}

void ActionNodeFileReferenceTreeView::Resolve(void)
{
    //
    // Should support a re-resolve!!!
    //
    ActionNode* referenceNode = m_ActionNodeFileReference->GetActionNode();
    if(!referenceNode)
    {
        m_ActionNodeFileReference->ReadFile();
    }

    referenceNode = m_ActionNodeFileReference->GetActionNode();
    if(referenceNode)
    {
        QString referenceNodeName(referenceNode->GetName()->GetString());
        setText(0, referenceNodeName);
    }
}

void ActionNodeFileReferenceTreeView::updeteIcon(void)
{
    ActionNodeFileRecordTreeView::updeteIcon();

    static QPixmap referencePix((const char **)reference_xpm);
    static QPixmap folder = stackPix(pixmap(0), &referencePix);
    static QPixmap redExclamation((const char **)s_red_exclamation);
    static QPixmap folderInclude = stackPix(&folder, &redExclamation);

	if(!m_ActionNodeFileReference || !m_ActionNodeFileReference->GetActionNode())
    {
		setPixmap(0, folderInclude);
    }
	else
	{
		setPixmap(0, folder);
	}
}

//--------------------------------------------------------------------------
// TracksListViewBase
//--------------------------------------------------------------------------
TextBuffer* TracksListViewBase::s_MsgBuff = NULL;
TracksListViewBase::TracksListViewBase( QWidget * parent, const char * name, WFlags f)
: ListViewItemCreator(parent, name, f)
, m_ReOrderTrack(0)
, m_ActionNode(0)
, m_iLastSelectedItemIndex(0)
{
    //
    // Try to Trap key event
    //
    listView->installEventFilter( this );
	listView->setSorting(-1);

	if(s_MsgBuff == NULL)
    {
        s_MsgBuff = new TextBuffer(1024);
    }
}

void TracksListViewBase::newItem( QString & itemName )
{
	IDataMembers * pDM = AddActionNodeItem(itemName.ascii());
	if( pDM!=NULL )
	{
		TrackOrCondition * pTrack = dynamic_cast<TrackOrCondition*>(pDM);
		new TrackTreeNode(pTrack, listView, GetLastToplevelNode());
	}
}

TrackTreeNode * TracksListViewBase::GetCurrSelectedNode()
{
	return dynamic_cast<TrackTreeNode*>(listView->selectedItem());
}

IDataMembers * TracksListViewBase::GetCurrentSelectedData()
{
	TrackTreeNode * pNode = GetCurrSelectedNode();
	if( pNode )
		return pNode->GetTrack();
	return NULL;
}

QListViewItem * TracksListViewBase::GetLastToplevelNode()
{
	QListViewItem * pNext = 0;
	QListViewItem * pLast = listView->firstChild();
	while(pLast && (pNext = pLast->nextSibling())!=0 )
		pLast = pNext;

	return pLast;
}

QListViewItem * TracksListViewBase::GetLastChild(QListViewItem * pItem)
{
	QListViewItem * pCurr = pItem ? pItem->firstChild() : 0;
	while( pCurr && pCurr->nextSibling() )
		pCurr = pCurr->nextSibling();
	return pCurr;
}

int TracksListViewBase::GetItemIndex(const QListViewItem * pItem) const
{
	int index = 0;
	QListViewItem * pSibling = 0;
	QListViewItem * pParent = pItem->parent();
	if( pParent )
		pSibling = pParent->firstChild();
	else
		pSibling = listView->firstChild();
	while( pSibling && pSibling!=pItem )
	{
		pSibling = pSibling->nextSibling();
		++index;
	}
	return pSibling ? index : -1;
}

QListViewItem * TracksListViewBase::GetPreviousSibling(const QListViewItem * pItem) const
{
	QListViewItem * pSibling = 0;
	QListViewItem * pParent = pItem->parent();
	if( pParent )
		pSibling = pParent->firstChild();
	else
		pSibling = listView->firstChild();
	while( pSibling && pSibling!=pItem )
	{
		QListViewItem * pNext = pSibling->nextSibling();
		if( pNext==pItem )
			return pSibling;
		pSibling = pNext;
	}
	return pSibling==pItem ? 0 : pSibling;
}

QListViewItem * TracksListViewBase::GetNestedItem(int index)
{
	int i = index;
	QListViewItem * pNode = listView->firstChild();
	while( pNode && i<index && pNode->itemBelow() )
	{
		pNode = pNode->itemBelow();
		++i;
	}
	return pNode;
}

int TracksListViewBase::GetNestedIndex(QListViewItem * pItem) const
{
	int i = 0;
	QListViewItem * pNode = listView->firstChild();
	while( pNode && pNode!=pItem && pNode->itemBelow() )
	{
		pNode = pNode->itemBelow();
		++i;
	}
	return pNode ? i : -1;
}

void TracksListViewBase::deleteItem()
{
	TrackTreeNode *pTTN = GetCurrSelectedNode();
	if( pTTN && DeleteActionNodeItem(pTTN->GetTrack()) )
	{
		delete pTTN;
	}
}

void TracksListViewBase::copyItem()
{
	IDataMembers * pDM = NULL;
	TrackTreeNode *pTTN = GetCurrSelectedNode();
	if( pTTN )
		pDM = pTTN->GetTrack();
	if(pDM)
	{			
		QClipboard *clipBoard = QApplication::clipboard();

		//
		// Copy text into the clipboard
		//
		TextBuffer textBuffer(131072);
		pDM->Write(textBuffer);

		clipBoard->setText( textBuffer.GetBuffer() );
	}
}

void TracksListViewBase::pasteItem()
{
	QClipboard *clipBoard = QApplication::clipboard();

	//
	// Get the Shit from copy text from the clipboard (paste)
	// 
	QString text = clipBoard->text();
	if( !text.isNull() )
	{
		qDebug( "The clipboard contains: " + text );

		TextParser textParser(text.ascii(), " ,\t\r\n" );

		IDataMembers * pNewItem = AddActionNodeItem(textParser);
		if( pNewItem!=NULL )
		{
			IDataMembers * pNewItem = AddActionNodeItem(textParser);
			if( pNewItem!=NULL )
			{
				// Insert the item into the listView
				TrackOrCondition * pNewTrack = dynamic_cast<TrackOrCondition*>(pNewItem);
				if( pNewTrack )
					new TrackTreeNode(pNewTrack, listView, GetLastToplevelNode());
			}
			textParser.GetNextToken();
		}
	}
}

void TracksListViewBase::itemSelected(QListViewItem * pItem)
{
	IDataMembers * pDM = 0;
	if( pItem )
	{
		TrackTreeNode *pTTN = dynamic_cast<TrackTreeNode*>(pItem);
		if( pTTN )
			pDM = pTTN->GetTrack();
	}
	updatedDataMembers(pDM);
	m_iLastSelectedItemIndex = GetNestedIndex(pItem);
}

void TracksListViewBase::updateSelected()
{
	IDataMembers * pDM = GetCurrentSelectedData();

	// NOTE: We need to send this signal even if the track is null.
	// This makes sure the Reset button text is cleared.
	updatedDataMembers(pDM); // SIGNAL
}

void TracksListViewBase::resetItems()
{
	IDataMembers * pDM = GetCurrentSelectedData();
	if(m_ActionNode && pDM)
	{
		pDM->Reset();
		refreshItems();
	}
}

void TracksListViewBase::refreshItems()
{
	RefreshListView();
	updateSelected();
}

void TracksListViewBase::RefreshListView()
{
	listView->clear();

	int itemCount = GetActionNodeItemCount();
	for(int i=0;i<itemCount;i++)
	{
		// Add it to the tree view:
		TrackOrCondition * pTrack = dynamic_cast<TrackOrCondition*>(GetActionNodeItem(i));
		if( pTrack )
			new TrackTreeNode(pTrack, listView, GetLastToplevelNode());
	}

	// find the item that is at the same index or before the last selected
	//	item index
	int iItemToSelectIndex = 0;
	QListViewItem *pItemToSelect = listView->firstChild();
	while (pItemToSelect && pItemToSelect->itemBelow())
	{
		if (iItemToSelectIndex == m_iLastSelectedItemIndex)
		{
			break;
		}
		pItemToSelect = pItemToSelect->itemBelow();
		++iItemToSelectIndex;
	}

	// now save the last selected item index and restore it once we've set the
	//	currently selected item index.
	iItemToSelectIndex = m_iLastSelectedItemIndex;
	if (pItemToSelect == NULL)
	{
		listView->setSelected(listView->firstChild(), true);
	}
	else
	{
		listView->setSelected(pItemToSelect, true);
	}
	m_iLastSelectedItemIndex = iItemToSelectIndex;

}

void TracksListViewBase::convertToSpawn()
{
	TrackTreeNode *pTTN = GetCurrSelectedNode();

	if(pTTN)
	{
		TrackOrCondition *pSelectedTrack = pTTN->GetTrack();
		if (pSelectedTrack)
		{
			NodeReferenceTrack *pNodeRefTrack = dynamic_cast<NodeReferenceTrack *>(pSelectedTrack);
			if (pNodeRefTrack)
			{
				const char *pNodeRefPath = pNodeRefTrack->GetActionNodeReference();

				SpawnTrack *pNewSpawnTrack = static_cast<SpawnTrack *>(m_ActionNode->AddTrack("Spawn"));
				pNewSpawnTrack->SetMaster(true);
				pNewSpawnTrack->SetUpdateOnBegin(true);
				pNewSpawnTrack->SetActionNodeReference(pNodeRefPath);

				deleteItem();

				// Insert the item into the listView
				new TrackTreeNode(pNewSpawnTrack, listView, GetLastToplevelNode());

			}
		}
	}
}


bool TracksListViewBase::eventFilter( QObject *obj, QEvent *e )
{
    if(listView == obj)
    {
        if ( e->type() == QEvent::KeyPress ) 
        {
            QKeyEvent* keyEvent = static_cast<QKeyEvent*>(e);
            if(keyEvent)
            {
                if(keyEvent->key() == Qt::Key_Control || keyEvent->key() == Qt::Key_Delete || m_ReOrderTrack == 1)
                {
                    keyPressEvent(keyEvent);
                    return FALSE;
                }
            }
        }

        return parent()->eventFilter( obj, e );
    } 
    else 
    {
        //
        // pass the event on to the parent class
        //
        return parent()->eventFilter( obj, e );
    }
}

void TracksListViewBase::keyPressEvent( QKeyEvent * e )
{   
    switch(e->key())
    {

    case Qt::Key_Control:
        {
            m_ReOrderTrack = 1;
            grabKeyboard();
            e->accept();
        }
        break;
    case Qt::Key_Up:
        {
            if(m_ReOrderTrack)
            {
                incrementSelectedPriority();
                e->accept();
            }
            else
            {
                e->ignore();
            }
        }
        break;
    case Qt::Key_Down:
        {
            if(m_ReOrderTrack)
            {
                decrementSelectedPriority();
                e->accept();
            }
            else
            {
                e->ignore();
            }
        }
        break;
	case Qt::Key_C:
		{
			if (m_ReOrderTrack)
			{
				copyItem();
				e->accept();
			}
			else
			{
				e->ignore();
			}
		}
		break;
	case Qt::Key_V:
		{
			if (m_ReOrderTrack)
			{
				pasteItem();
				e->accept();
			}
			else
			{
				e->ignore();
			}
		}
		break;
	case Qt::Key_X:
		{
			if (m_ReOrderTrack)
			{
				cutItem();
				e->accept();
			}
			else
			{
				e->ignore();
			}
		}
		break;
	case Qt::Key_Delete:
	case Qt::Key_D:
		{
			if (e->key() == Qt::Key_Delete ||
				m_ReOrderTrack)
			{
				deleteItem();
				e->accept();
			}
			else
			{
				e->ignore();
			}
		}
		break;
	case Qt::Key_W:
		{
			if (m_ReOrderTrack)
			{
				convertToSpawn();
				e->accept();
			}
			else
			{
				e->ignore();
			}
		}
		break;
    default:
        e->ignore();
        break;
    }
}


void TracksListViewBase::keyReleaseEvent( QKeyEvent * e )
{
    switch(e->key())
    {

    case Qt::Key_Control:
        {
            m_ReOrderTrack = 0;
            releaseKeyboard();
            e->accept();
        }
        break;

    default:
        e->ignore();
        break;
    }  
}

void TracksListViewBase::decrementSelectedPriority()
{
	QListViewItem * pLVI = listView->selectedItem();
	// Bail if nothing is selected:
	if( !pLVI )
		return;
	// Bail if this item can't move down any more:
	if( pLVI->nextSibling()==NULL )
		return;

	// Is this a root-level item?
	if( pLVI->parent()==NULL )
	{
		int index = GetItemIndex(pLVI);
		if( index>=0 && ChangeActionNodeItemPriority(index, false) )
		{
			pLVI->moveItem(pLVI->nextSibling());
		}
	}
	else
	{
		TrackTreeNode * pParentTTN = dynamic_cast<TrackTreeNode*>(pLVI->parent());
		if( !pParentTTN )
			return;
		ConditionGroupCondition * pCGC = dynamic_cast<ConditionGroupCondition*>(pParentTTN->GetTrack());
		if( !pCGC )
			return;
		int index = GetItemIndex(pLVI);
		if( index>=0 )
		{
			pCGC->ChangeConditionPriority(index, false);
			pLVI->moveItem(pLVI->nextSibling());
		}
	}
}

void TracksListViewBase::incrementSelectedPriority()
{
	QListViewItem * pLVI = listView->selectedItem();
	// Bail if nothing is selected:
	if( !pLVI )
		return;

	QListViewItem * pPrevSibling = GetPreviousSibling(pLVI);
	// Bail if this is the first item
	if( !pPrevSibling )
		return;

	// Is this a root-level item?
	if( pLVI->parent()==NULL )
	{
		int index = GetItemIndex(pLVI);
		if( index>=0 && ChangeActionNodeItemPriority(index, true) )
		{
			pPrevSibling->moveItem(pLVI);
		}
	}
	else
	{
		TrackTreeNode * pParentTTN = dynamic_cast<TrackTreeNode*>(pLVI->parent());
		if( !pParentTTN )
			return;
		ConditionGroupCondition * pCGC = dynamic_cast<ConditionGroupCondition*>(pParentTTN->GetTrack());
		if( !pCGC )
			return;
		int index = GetItemIndex(pLVI);
		if( index>=0 )
		{
			pCGC->ChangeConditionPriority(index, true);
			pPrevSibling->moveItem(pLVI);
		}
	}
}

void TracksListViewBase::Update(CSubject* theChangedSubject)
{   
	QListViewItem * pLVI = listView->selectedItem();
	if (!pLVI)
	{
		return;
	}

	int iSelectedItemIndex = GetItemIndex(pLVI);

    if(theChangedSubject && iSelectedItemIndex >= 0)
    {
        if(m_ActionNode)
        {
            IDataMembers* track = dynamic_cast<IDataMembers*>(theChangedSubject);

            if(track)
            {
				if(track->m_ChangedMember)
				{
					TextBuffer msgBuff(1024);
	                
					msgBuff.Write("[act]EDIT");
					msgBuff.Write(remoteEditName());
					msgBuff.Write(" ");
					char fullPathName[1024];
					m_ActionNode->GetNameFullPath(fullPathName, 1024);
					msgBuff.Write(fullPathName);
					msgBuff.Write(" ");
					msgBuff.Write(iSelectedItemIndex);
					msgBuff.Write(" ");
	             
					msgBuff.Write(" { ");   
					msgBuff.Write(track->m_ChangedMember->GetName()->GetString());
					msgBuff.Write(" ");
					track->m_ChangedMember->Write(track, msgBuff);
					msgBuff.Write(" }");   
	                
					g_theApp->PostMessageToRemoteHost(msgBuff.GetBuffer());
				}
				else
				{
					Assert(0);
				}
            }
        }
    }
}

void TracksListViewBase::UpdateDetach(CSubject* theChangedSubject)
{
    if(theChangedSubject)
    {
        theChangedSubject->Dettach(this);
    }
}

void TracksListViewBase::SetActionNode(ActionNode* actionNode)
{
    dettachObservers();
    m_ActionNode = actionNode;
    attachObservers();

    refreshItems();
}

void TracksListViewBase::notifyNewTrack(Track* newTrack)
{
    //
    // Remote shit
    //
    s_MsgBuff->Reset();
    s_MsgBuff->Write("[act]ADD");
    s_MsgBuff->Write(remoteEditName());
    s_MsgBuff->Write(" ");
    char fullPathName[1024];
    m_ActionNode->GetNameFullPath(fullPathName, 1024);
    s_MsgBuff->Write(fullPathName);
    s_MsgBuff->Write(" ");
    s_MsgBuff->Write(newTrack->GetName()->GetString());

    g_theApp->PostMessageToRemoteHost(s_MsgBuff->GetBuffer());
}

void TracksListViewBase::notifyDeleteTrack(int TrackID)
{
    //
    // Remote shit
    //
    s_MsgBuff->Reset();
    s_MsgBuff->Write("[act]DELETE");
    s_MsgBuff->Write(remoteEditName());
    s_MsgBuff->Write(" ");
    char fullPathName[1024];
    m_ActionNode->GetNameFullPath(fullPathName, 1024);
    s_MsgBuff->Write(fullPathName);
    s_MsgBuff->Write(" ");
    s_MsgBuff->Write(TrackID);

    g_theApp->PostMessageToRemoteHost(s_MsgBuff->GetBuffer());
}

void TracksListViewBase::notifyChangePriority(int TrackID, bool increase)
{
    //
    // Remote shit
    //
    s_MsgBuff->Reset();
    s_MsgBuff->Write("[act]CHANGEPRI");
    s_MsgBuff->Write(remoteEditName());
    s_MsgBuff->Write(" ");
    char fullPathName[1024];
    m_ActionNode->GetNameFullPath(fullPathName, 1024);
    s_MsgBuff->Write(fullPathName);
    s_MsgBuff->Write(" ");
    s_MsgBuff->Write(TrackID);
    if(increase)
    {
        s_MsgBuff->Write(" 1");
    }
    else
    {
        s_MsgBuff->Write(" 0");
    }
    
    g_theApp->PostMessageToRemoteHost(s_MsgBuff->GetBuffer());
}


//--------------------------------------------------------------------------
// TracksListView
//--------------------------------------------------------------------------
TracksListView::TracksListView( QWidget * parent, const char * name, WFlags f)
:
TracksListViewBase(parent, name, f)
{
	//
	// Populate the Track/Condition create list boxes
	//
	int registeredTrackCount = Track::GetNumRegistered();
	for(int i=0;i<registeredTrackCount;i++)
	{
		itemDisplay->insertItem( QString(Track::GetRegistered(i)->GetString()) );
	}
}

TracksListView::~TracksListView()
{
}

void TracksListView::attachObservers()
{
    if(m_ActionNode)
    {
        int trackCount = m_ActionNode->GetTrackCount();
        for(int i=0;i<trackCount;i++)
        {
            Track* track = m_ActionNode->GetTrack(i);
            if(track)
            {
                track->Attach(this);
            }
        }
    }
}

void TracksListView::dettachObservers()
{
    if(m_ActionNode)
    {
        int trackCount = m_ActionNode->GetTrackCount();
        for(int i=0;i<trackCount;i++)
        {
            Track* track = m_ActionNode->GetTrack(i);
            if(track)
            {
                track->Dettach(this);
            }
        }
    }
}

int TracksListView::GetActionNodeItemCount() const
{
	return m_ActionNode ? m_ActionNode->GetTrackCount() : 0;
}

const char * TracksListView::GetActionNodeItemStr(int idx) const
{
	return m_ActionNode ? m_ActionNode->GetTrackFormat(idx) : "";
}

IDataMembers * TracksListView::GetActionNodeItem(int idx)
{
	return m_ActionNode ? m_ActionNode->GetTrack(idx) : 0;
}

IDataMembers * TracksListView::AddActionNodeItem(const char * pName)
{
	if( m_ActionNode==0 )
		return NULL;
	return m_ActionNode->AddTrack(pName);
}

IDataMembers * TracksListView::AddActionNodeItem(TextParser & textParser)
{
	if( m_ActionNode==0 )
		return NULL;
	return m_ActionNode->AddTrack(textParser);
}

bool TracksListView::DeleteActionNodeItem(int idx)
{
	int iCurrentCount = GetActionNodeItemCount();
	if( idx<0 || idx>=iCurrentCount )
		return false;
	m_ActionNode->DeleteTrack(idx);
	// Make sure it deleted properly:
	return (GetActionNodeItemCount() != iCurrentCount);
}

bool TracksListView::DeleteActionNodeItem(IDataMembers * pDM)
{
	Track * pTrack = dynamic_cast<Track*>(pDM);
	if( pTrack )
		return m_ActionNode->DeleteTrack(pTrack);
	return false;
}

bool TracksListView::ChangeActionNodeItemPriority(int idx, bool increase)
{
	if( !m_ActionNode )
		return false;
	m_ActionNode->ChangeTrackPriority(idx, increase);
	return true;
}


//--------------------------------------------------------------------------
// ConditionListView
//--------------------------------------------------------------------------
ConditionListView::ConditionListView( QWidget * parent, const char * name, WFlags f)
:
TracksListViewBase(parent, name, f)
{
	//
	// Populate the Track/Condition create list boxes
	//
	int registeredConditionCount = Condition::GetNumRegistered();
	int i;
	for(i=0;i<registeredConditionCount;i++)
	{
		itemDisplay->insertItem( QString(Condition::GetRegistered(i)->GetString()) );
	}
}

ConditionListView::~ConditionListView()
{
}

void ConditionListView::attachObservers()
{
    if(m_ActionNode)
    {
        int trackCount = m_ActionNode->GetConditionCount();
        for(int i=0;i<trackCount;i++)
        {
            Condition* track = m_ActionNode->GetCondition(i);
            if(track)
            {
                track->Attach(this);
            }
        }
    }
}

void ConditionListView::dettachObservers()
{
    if(m_ActionNode)
    {
        int trackCount = m_ActionNode->GetConditionCount();
        for(int i=0;i<trackCount;i++)
        {
            Condition* track = m_ActionNode->GetCondition(i);
            if(track)
            {
                track->Dettach(this);
            }
        }
    }
}

int ConditionListView::GetActionNodeItemCount() const
{
	return m_ActionNode ? m_ActionNode->GetConditionCount() : 0;
}

const char * ConditionListView::GetActionNodeItemStr(int idx) const
{
	return m_ActionNode ? m_ActionNode->GetCondtionFormat(idx) : "";
}

IDataMembers * ConditionListView::GetActionNodeItem(int idx)
{
	return m_ActionNode ? m_ActionNode->GetCondition(idx) : 0;
}

IDataMembers * ConditionListView::AddActionNodeItem(const char * pName)
{
	TrackTreeNode *pTTN = GetCurrSelectedNode();
	ConditionGroupCondition * pCGC = (pTTN ? pTTN->GetConditionGroupCondition() : 0);
	if( pTTN && pCGC )
	{
		IDataMembers * pNewCond = pCGC->AddCondition(pName);
		TrackOrCondition * pNewCondTrack = dynamic_cast<TrackOrCondition*>(pNewCond);
		if( pNewCondTrack )
			new TrackTreeNode(pNewCondTrack, pTTN, GetLastChild(pTTN));
		return 0;
	}
	else
	{
		if( m_ActionNode==0 )
			return NULL;
		return m_ActionNode->AddCondition(pName);
	}
}

IDataMembers * ConditionListView::AddActionNodeItem(TextParser & textParser)
{
	TrackTreeNode *pTTN = GetCurrSelectedNode();
	ConditionGroupCondition * pCGC = (pTTN ? pTTN->GetConditionGroupCondition() : 0);
	if( pTTN && pCGC )
	{
		IDataMembers * pNewCond = pCGC->AddCondition(textParser);
		TrackOrCondition * pNewCondTrack = dynamic_cast<TrackOrCondition*>(pNewCond);
		if( pNewCondTrack )
			new TrackTreeNode(pNewCondTrack, pTTN, GetLastChild(pTTN));
		return 0;
	}
	else
	{
		if( m_ActionNode==0 )
			return NULL;
		return m_ActionNode->AddCondition(textParser);
	}
}

bool ConditionListView::DeleteActionNodeItem(int idx)
{
	int iCurrentCount = GetActionNodeItemCount();
	if( idx<0 || idx>=iCurrentCount )
		return false;
	m_ActionNode->DeleteCondition(idx);
	// Make sure it deleted properly:
	return (GetActionNodeItemCount() != iCurrentCount);
}

bool ConditionListView::DeleteActionNodeItem(IDataMembers * pDM)
{
	Condition * pCondition = dynamic_cast<Condition*>(pDM);
	if( pCondition )
		return m_ActionNode->DeleteCondition(pCondition);
	return false;
}

bool ConditionListView::ChangeActionNodeItemPriority(int idx, bool increase)
{
	if( !m_ActionNode )
		return false;
	m_ActionNode->ChangeConditionPriority(idx, increase);
	return true;
}


//--------------------------------------------------------------------------
// ConditionListDataMemberView
//--------------------------------------------------------------------------
ConditionListDataMemberView::ConditionListDataMemberView( QWidget * parent, const char * name, WFlags f)
:
TracksListViewBase(parent, name, f)
{
	//
	// Populate the Track/Condition create list boxes
	//
    //
    // #### replace this interface!!!!
    //
	int registeredConditionCount = Condition::GetNumRegistered(); 
	int i;
	for(i=0;i<registeredConditionCount;i++)
	{
		itemDisplay->insertItem( QString(Condition::GetRegistered(i)->GetString()) );
	}
}

void ConditionListDataMemberView::SetDataMember
( 
    IDataMembers * dataMember, 
    IGroupDataMember* dataMemberInterface 
)
{
    m_dataMembersInstance = dataMember;
	m_dataMemberInterface =  dataMemberInterface;
    refreshItems();
}

ConditionListDataMemberView::~ConditionListDataMemberView()
{
}

IDataMembers* ConditionListDataMemberView::GetCurrentSelectedData(void)
{
	return TracksListViewBase::GetCurrentSelectedData();
}

int ConditionListDataMemberView::GetActionNodeItemCount() const
{
	return m_dataMemberInterface ? m_dataMemberInterface->GetCount(m_dataMembersInstance) : 0;
}

const char * ConditionListDataMemberView::GetActionNodeItemStr(int idx) const
{
	return m_dataMemberInterface ? m_dataMemberInterface->GetDataMemberFormat(m_dataMembersInstance, idx) : "";
}

IDataMembers * ConditionListDataMemberView::GetActionNodeItem(int idx)
{
	return m_dataMemberInterface ? m_dataMemberInterface->GetDataMember(m_dataMembersInstance, idx) : 0;
}

IDataMembers * ConditionListDataMemberView::AddActionNodeItem(const char * pName)
{
	TrackTreeNode *pTTN = GetCurrSelectedNode();
	ConditionGroupCondition * pCGC = (pTTN ? pTTN->GetConditionGroupCondition() : 0);
	if( pTTN && pCGC )
	{
		IDataMembers * pNewCond = pCGC->AddCondition(pName);
		TrackOrCondition * pNewCondTrack = dynamic_cast<TrackOrCondition*>(pNewCond);
		if( pNewCondTrack )
			new TrackTreeNode(pNewCondTrack, pTTN, GetLastChild(pTTN));
		return 0;
	}
	else
	{
		if( m_dataMemberInterface==0 )
			return NULL;
		return m_dataMemberInterface->Add(m_dataMembersInstance, pName);
	}
}

IDataMembers * ConditionListDataMemberView::AddActionNodeItem(TextParser & textParser)
{
	TrackTreeNode *pTTN = GetCurrSelectedNode();
	ConditionGroupCondition * pCGC = (pTTN ? pTTN->GetConditionGroupCondition() : 0);
	if( pTTN && pCGC )
	{
		IDataMembers * pNewCond = pCGC->AddCondition(textParser);
		TrackOrCondition * pNewCondTrack = dynamic_cast<TrackOrCondition*>(pNewCond);
		if( pNewCondTrack )
			new TrackTreeNode(pNewCondTrack, pTTN, GetLastChild(pTTN));
		return 0;
	}
	else
	{
		if( m_dataMemberInterface==0 )
			return NULL;
		return m_dataMemberInterface->Add(m_dataMembersInstance, textParser);
	}
}

bool ConditionListDataMemberView::DeleteActionNodeItem(int idx)
{
	int iCurrentCount = GetActionNodeItemCount();
	if( idx<0 || idx>=iCurrentCount )
		return false;
	m_dataMemberInterface->Delete(m_dataMembersInstance, idx);
	// Make sure it deleted properly:
	return (GetActionNodeItemCount() != iCurrentCount);
}

bool ConditionListDataMemberView::DeleteActionNodeItem(IDataMembers * pConditionToDelete)
{
	return m_dataMemberInterface->Delete(m_dataMembersInstance, pConditionToDelete);
}

bool ConditionListDataMemberView::ChangeActionNodeItemPriority(int idx, bool increase)
{
	if( !m_dataMemberInterface )
		return false;
	m_dataMemberInterface->ChangePriority(m_dataMembersInstance, idx, increase);
	return true;
}


void ConditionListDataMemberView::itemSelected( QListViewItem * pItem )
{
	// TODO: Figure out why TracksListViewBase::itemSelected(QListViewItem*)
	// causes bad things to happen.  Specifically, the listView sometimes
	// doesn't refresh, so it appears as though multiple items are selected
	// (especially if you click/drag in the listView).

	TracksListViewBase::itemSelected(pItem);
}



//--------------------------------------------------------------------------
// ActionNodeView
//--------------------------------------------------------------------------
char ActionNodeView::s_msgBuff[1024];
ActionNodeView::ActionNodeView( QWidget * parent, const char * name, WFlags f)
:
QWidget( parent, name, f )
{
	m_ActionNode = NULL;
	m_conditionViewIndex = 0;
	m_tracksViewIndex = 0;

	QSplitter *split = new QSplitter( Vertical, this, "splitter" );
    split->setOpaqueResize( TRUE );

	setCaption(QString("Action Node View"));

	m_NodeName = new QLineEdit( split, "Node Name" );
	m_NodeName->setEnabled(false);
	m_NodeName->setMinimumHeight(20);
	m_NodeName->setMaximumHeight(20);

    QHBox* scrollViewVbox = new QHBox(split); 
    m_PlayButton = new QPushButton(QString("Play"), scrollViewVbox);
    m_Pause      = new QPushButton(QString("Pause"), scrollViewVbox);
    m_Stop       = new QPushButton(QString("Stop"), scrollViewVbox);
    m_Forward    = new QPushButton(QString("Forward"), scrollViewVbox);
    m_SimRate    = new QSpinBox( scrollViewVbox, "SimRate" );
    //m_SimRate->setMinimumWidth(10);
    //m_SimRate->setMaximumWidth(20);
    m_SimRate->setLineStep(10);
    m_SimRate->setMaxValue(300);
    m_SimRate->setValue(100); // default simRate

//    m_EditPriorityView = new DataMemberIntSlider(Application::GetInstance(), split);

	m_conditionView = new ConditionListView(split);
	m_conditionView->setCaption(QString("Condition List"));
    m_conditionView->groupName->setText("Condition List");
	m_conditionView->show();

	m_tracksView = new TracksListView(split);
	m_tracksView->setCaption(QString("Tracks List"));
	m_tracksView->groupName->setText("Tracks List");
    m_tracksView->show();

	QVBox *commentArea = new QVBox(split);
	QLabel *commentTitle = new QLabel( "Comment", commentArea );
	m_Comment = new QTextEdit(commentArea);

	//
	// Now lets lay it all out
	//
    m_ActionNodeViewLayout = new QGridLayout( this, 1, 1, 2, 0, "ActionNodeViewLayout");

	m_ActionNodeViewLayout->addWidget( split, 0, 0 );

    m_ViewProperties = NULL;

    // signals and slots connections
    connect( m_PlayButton, SIGNAL( clicked() ), this, SLOT( play() ) );
    connect( m_Stop,        SIGNAL( clicked() ), this, SLOT( stop() ) );
    connect( m_Forward,     SIGNAL( clicked() ), this, SLOT( forward() ) );
    connect( m_Pause,       SIGNAL( toggled ( bool ) ), this,    SLOT( pause(bool) ) );
    m_Pause->setToggleButton(true);
    connect( m_SimRate, SIGNAL( valueChanged(const QString&) ),   this, SLOT( setSimRate(const QString&) ) );
	connect( m_Comment, SIGNAL( textChanged() ) , this, SLOT( setComment() ) ); 
}

ActionNodeView::~ActionNodeView()
{


}

void SyncWithViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties)
{
    if(viewProperties)
    {
        widget->move( viewProperties->GetXposition(),  viewProperties->GetYposition());
        widget->resize(viewProperties->GetWidth(), viewProperties->GetHeight());

        switch(viewProperties->GetOpen())
        {
            case ViewProperties::SM_HIDDEN:
                {
                    widget->hide();
                }
                break;
            case ViewProperties::SM_MINIMIZED:
                {
                    widget->showMinimized();
                }
                break;
            case ViewProperties::SM_MAXIMIZED:
                {
                    widget->showMaximized();
                }
                break;
            case ViewProperties::SM_NORMAL:
            default:
                {
                    widget->showNormal();
                }
                break;
        }
    }
}

void SaveViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties)
{
    if(viewProperties)
    {
        viewProperties->SetXposition(widget->x());
        viewProperties->SetYposition(widget->y());

        viewProperties->SetHeight(widget->height());
        viewProperties->SetWidth(widget->width());

        if(widget->isMinimized())
        {
            viewProperties->SetOpen(ViewProperties::SM_MINIMIZED); 
        }
        else
        if(widget->isMaximized())
        {
            viewProperties->SetOpen(ViewProperties::SM_MAXIMIZED);
        }
        else
        if(widget->isHidden())
        {
            viewProperties->SetOpen(ViewProperties::SM_HIDDEN);
        }
        else
        {
            viewProperties->SetOpen(ViewProperties::SM_NORMAL);
        }
    }
}

void ActionNodeView::SyncWithViewProperies(void)
{
    SyncWithViewProperiesUtil(this, m_ViewProperties);
}

void ActionNodeView::SaveViewProperies(void)
{
    SaveViewProperiesUtil(this, m_ViewProperties);
}

void ActionNodeView::SetActionNode(ActionNode* actionNode)
{

	m_ActionNode = actionNode;

	if(m_ActionNode)
	{
		m_NodeName->setText(QString(m_ActionNode->GetName()->GetString()));
		QString commentText = QString(m_ActionNode->GetComment()).replace(QRegExp("''"), "\"");
		m_Comment->setText(commentText);
	}
    
    /*ActionNodePlayable* playable = dynamic_cast<ActionNodePlayable*>(actionNode);
    if(playable)
    {
        IIntDataMember* dataMemberInterface = dynamic_cast<IIntDataMember*>(playable->GetMember(0));
        if(dataMemberInterface)
        {
            m_EditPriorityView->setEnabled(true);
            m_EditPriorityView->SetDataMember(playable, dataMemberInterface);
        }
        else
        {
            m_EditPriorityView->setEnabled(false);
        }
    }
    else
    {
        m_EditPriorityView->setEnabled(false);
    }*/

	m_conditionView->SetActionNode(m_ActionNode);
	m_tracksView->SetActionNode(m_ActionNode);

}

void ActionNodeView::refresh()
{
//    m_EditPriorityView->Update(NULL);

	//
	// These could be connected
	//
	m_conditionView->refreshItems();
	m_tracksView->refreshItems();
}

void ActionNodeView::play()
{
    if(m_ActionNode)
    {
        strcpy(s_msgBuff, "[act]Play ");
        int commandLength = strlen(s_msgBuff);
        m_ActionNode->GetNameFullPath(&s_msgBuff[commandLength], 1024-commandLength);
        g_theApp->PostMessageToRemoteHost(s_msgBuff);
    }
}

void ActionNodeView::stop()
{
    strcpy(s_msgBuff, "[act]Stop");
    g_theApp->PostMessageToRemoteHost(s_msgBuff);
}

void ActionNodeView::forward()
{
    strcpy(s_msgBuff, "[act]Forward");
    g_theApp->PostMessageToRemoteHost(s_msgBuff);
}

void ActionNodeView::pause(bool on)
{
    sprintf(s_msgBuff, "[act]Pause %s", on?"on":"off");
    g_theApp->PostMessageToRemoteHost(s_msgBuff);
}

void ActionNodeView::setSimRate(const QString& simRate)
{
    sprintf(s_msgBuff, "[act]Sim %s", simRate.ascii());
    g_theApp->PostMessageToRemoteHost(s_msgBuff);
}

void ActionNodeView::setComment()
{
	if(m_ActionNode)
	{
		m_ActionNode->SetComment(m_Comment->text().replace(QRegExp("\""), "''"));
		//m_ActionNode->SetHasUnsavedChanges();
	}
}

//--------------------------------------------------------------------------
// ActionNodeListView
//--------------------------------------------------------------------------
#include "actiontree.h"
extern ActionTree* g_ActionTree;
ActionNodeListView::ActionNodeListView
(
    QWidget* parent, 
    const char* name, 
    WFlags fl
)
:QListView(parent,name,fl)
{
    connect( this, SIGNAL( pressed(QListViewItem*,const QPoint&,int) ), 
        this, SLOT( clickedHandler(QListViewItem*,const QPoint&,int) ) );
    connect( this, SIGNAL( clicked(QListViewItem*,const QPoint&,int) ), 
        this, SLOT( clickedHandler(QListViewItem*,const QPoint&,int) ) );
    //
    // Disable Sorting
    //
    setSorting(-1);
}

void ActionNodeListView::Update(CSubject* theChangedSubject)
{
    /*
    ActionTreeLog* actionTreeLog = dynamic_cast<ActionTreeLog*>(theChangedSubject);

    if(actionTreeLog)
    {   
    QString message(actionTreeLog->GetMessage());
    append(message);
    repaintContents();
    }
    */
}

void ActionNodeListView::UpdateDetach(CSubject* theChangedSubject)
{
    theChangedSubject->Dettach(this);
}

void ActionNodeListView::clickedHandler 
( 
 QListViewItem *item, 
 const QPoint& coords, 
 int column
 )
{
    if(item)
    {
        TreeNodeView* actionNodeView = dynamic_cast<TreeNodeView*>(item);
        if(actionNodeView)
        {
            ActionNode* actionNode = actionNodeView->GetActionNode();
            if(actionNode)
            {
                g_ActionTree->SelectActionNode(actionNode);
            }
        }
    }
}
