#include "SpewWindow.hpp"
#include <qdatetime.h> 
#include "../allActionTree.h"

SpewWindow::SpewWindow(QWidget* parent, const char* name, WFlags fl)
:
QTextEdit(NULL, name)
{

    if(parent)
    {
        QPoint p(0,0);
        reparent(parent, fl, p);
    }

    setReadOnly(TRUE);
    setTextFormat( Qt::LogText );

    ActionTreeLog::GetInstance()->Attach(this);

	setVScrollBarMode(QScrollView::AlwaysOn);
}

SpewWindow::~SpewWindow()
{
    ActionTreeLog::GetInstance()->Dettach(this);
}

void SpewWindow::initStyle(void)
{
    QStyleSheetItem * warningItem = new QStyleSheetItem( this->styleSheet(), "warn" );
    warningItem->setColor( "red" );
    warningItem->setFontWeight( QFont::Bold );
    warningItem->setFontUnderline( TRUE );

    QStyleSheetItem * savedItem = new QStyleSheetItem( this->styleSheet(), "good" );
    savedItem->setColor( "blue" );
    savedItem->setFontWeight( QFont::Bold );
    savedItem->setFontUnderline( TRUE );

}

void SpewWindow::appendPostTime(const QString& message)
{
	static bool s_bScrolledToBottom = false;

    QTime time = QTime::currentTime();
    QString timeString = time.toString(QString("h:mm:ss.zzz ap"));
    
    append(timeString + ": " + message);

	if (!s_bScrolledToBottom && contentsHeight() >= visibleHeight())
	{
		scrollToBottom();
		s_bScrolledToBottom = true;
	}

    updateContents();
	repaintContents();
}

void SpewWindow::Update(CSubject* theChangedSubject)
{
    ActionTreeLog* actionTreeLog = dynamic_cast<ActionTreeLog*>(theChangedSubject);

    if(actionTreeLog)
    {   
        QString message(actionTreeLog->GetMessage());
        append(message);
		scrollToBottom();

        updateContents();
		repaintContents();
    }
}

void SpewWindow::UpdateDetach(CSubject* theChangedSubject)
{
    theChangedSubject->Dettach(this);
}

extern void SyncWithViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);
extern void SaveViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);

void SpewWindow::SyncWithViewProperies(void)
{
    SyncWithViewProperiesUtil(this, m_ViewProperties);
}

void SpewWindow::SaveViewProperies(void)
{
    SaveViewProperiesUtil(this, m_ViewProperties);
}
