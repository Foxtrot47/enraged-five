#ifndef ACTION_NODE_VIEW_HPP
#define ACTION_NODE_VIEW_HPP

#include <qvariant.h>
#include <qdialog.h>
#include <qlistview.h>
#include <qwidget.h>
#include <qdockwindow.h> 
#include <qspinbox.h> 
#include <qtextedit.h>

#include "generated/trackslistcreateview.h"
#include "allActionTree.h"

class ActionNode;
class ActionNodeBank;
class ActionNodePlayable;
class ArchiveFileRecord;

class ViewProperties;
class DataMemberIntSlider;

class QGridLayout;
class QVBoxLayout;
class QPopupMenu;
class QListBoxItem;
class QPushButton;
class QTextEdit;

//------------------------------------------------------------------------------

class TrackTreeNode : public QListViewItem
{
public:
	TrackTreeNode(TrackOrCondition * pTrack, QListView * pParent, QListViewItem * pInsertAfter);
	TrackTreeNode(TrackOrCondition * pTrack, QListViewItem * pParent, QListViewItem * pInsertAfter);

	TrackOrCondition * GetTrack() { return m_pTrack; }
	Condition * GetCondition() { return dynamic_cast<Condition*>(m_pTrack); }
	ConditionGroupCondition * GetConditionGroupCondition() { return dynamic_cast<ConditionGroupCondition*>(m_pTrack); }

protected:
	void Init();

protected:
	TrackOrCondition * m_pTrack;
};

//------------------------------------------------------------------------------

class TreeNodeView : public QListViewItem
{
public:

	TreeNodeView(QListView*		parent, QListViewItem *after, int sortOrder, int nodecol = 0);  
	TreeNodeView(QListViewItem*	parent, QListViewItem *after, int sortOrder, int nodecol = 0); 

	virtual QString key ( int column, bool ascending ) const;
    virtual int compare ( QListViewItem * i, int col, bool ascending ) const; 

	int  GetSortOrder() const;//{return(m_sortOrder);}
	void SetSortOrder(int sortOrder){m_sortOrder = sortOrder;}

	virtual ActionNode* GetActionNode(void) const = 0;

    virtual void SetName( const char* name, int col ) = 0;

    virtual void SyncronizeWithNode(void){}
	
	void SetNodeRefColumn(int col){m_NodeRefColumn = col;}
	int  GetNodeRefColumn(void){return(m_NodeRefColumn);}

protected:

	int m_sortOrder;
	int m_NodeRefColumn;
};

//------------------------------------------------------------------------------

class ActionNodeBankTreeView : public TreeNodeView
{
public:
	
	ActionNodeBankTreeView(ActionNodeBank* actionNodeBank, QListView*		parent, QListViewItem * after, int sortOrder, int nodecol = 0);  
	ActionNodeBankTreeView(ActionNodeBank* actionNodeBank, QListViewItem*	parent, QListViewItem * after, int sortOrder, int nodecol = 0);   

	virtual ActionNode* GetActionNode(void) const {return((ActionNode*)m_ReferenceNode);}
	
    virtual void SetName( const char* name, int col )
    {
        if(col == m_NodeRefColumn)
        {
            m_ReferenceNode->GetName()->SetString(name);
        }
    }

    virtual void setOpen( bool );
	 bool LocalIsOpen();

private:
    void init(void);
   	ActionNodeBank* m_ReferenceNode;

};

//------------------------------------------------------------------------------

class ActionNodePlayableTreeView : public TreeNodeView
{
public:
	
	ActionNodePlayableTreeView(ActionNodePlayable* actionNodePlayable, QListView*       parent, QListViewItem * after, int sortOrder, int nodecol = 0);   
	ActionNodePlayableTreeView(ActionNodePlayable* actionNodePlayable, QListViewItem*	parent, QListViewItem * after, int sortOrder, int nodecol = 0);  

	virtual ActionNode* GetActionNode(void) const {return((ActionNode*)m_ReferenceNode);}

    virtual void SetName( const char* name, int col )
    {
        if(col == m_NodeRefColumn)
        {
            m_ReferenceNode->GetName()->SetString(name);
        }
    }
    static const QPixmap& GetIcon(void);

	virtual void setOpen(bool bOpen);
	bool LocalIsOpen();
private:
    void init(void);
	ActionNodePlayable* m_ReferenceNode;
};

//------------------------------------------------------------------------------

class ActionNodeFileRecordTreeView : public TreeNodeView
{
public:
	
	ActionNodeFileRecordTreeView(QListView*     parent, QListViewItem * after, int sortOrder, int nodecol = 0);  
	ActionNodeFileRecordTreeView(QListViewItem*	parent, QListViewItem * after, int sortOrder, int nodecol = 0);  

	virtual ActionNode* GetActionNode(void) const = 0;
    virtual ArchiveFileRecord* GetArchiveFileRecord() = 0;
    virtual void Resolve(void){};

    virtual void SetName( const char* name, int col );
    virtual void setOpen( bool );

    virtual void SyncronizeWithNode(void);

protected:
    virtual void updeteIcon(void);
    void init(void);
    void removeArchiveFileRecordFromApp(void);
};

//------------------------------------------------------------------------------

class ArchiveFileRecordTreeView : public ActionNodeFileRecordTreeView
{
public:
	
	ArchiveFileRecordTreeView(ArchiveFileRecord* archiveFileRecord, QListView* parent, QListViewItem * after, int sortOrder, int nodecol = 0);  
	ArchiveFileRecordTreeView(ArchiveFileRecord* archiveFileRecord, QListViewItem*	parent, QListViewItem * after, int sortOrder, int nodecol = 0);  
    ~ArchiveFileRecordTreeView();

	virtual ActionNode* GetActionNode(void) const 
    {
        IArchive* object = m_ArchiveFileRecord->GetArchiveObject();

        return(dynamic_cast<ActionNode*>(object));
    }
    ArchiveFileRecord* GetArchiveFileRecord(){return(m_ArchiveFileRecord);}

private:
    ArchiveFileRecord* m_ArchiveFileRecord;
};

//------------------------------------------------------------------------------

class ActionNodeFileReferenceTreeView : public ActionNodeFileRecordTreeView
{
public:
	
	ActionNodeFileReferenceTreeView(ActionNodeFileReference* archiveFileRecord, QListView* parent, QListViewItem * after, int sortOrder, int nodecol = 0);  
	ActionNodeFileReferenceTreeView(ActionNodeFileReference* archiveFileRecord, QListViewItem*	parent, QListViewItem * after, int sortOrder, int nodecol = 0);  
    ~ActionNodeFileReferenceTreeView();

    virtual void Resolve(void);

	virtual ActionNode* GetActionNode(void) const {return(m_ActionNodeFileReference);}
    ArchiveFileRecord* GetArchiveFileRecord(){return(m_ActionNodeFileReference->GetArchiveFileRecord());}

private:
    virtual void updeteIcon(void);
    ActionNodeFileReference* m_ActionNodeFileReference;
};

//------------------------------------------------------------------------------

class TracksListViewBase : public ListViewItemCreator , public IObserver
{
	Q_OBJECT
public:
	TracksListViewBase( QWidget * parent = 0, const char * name = 0, WFlags f = 0 );
	~TracksListViewBase(){}

	virtual void SetActionNode(ActionNode* actionNode);
    virtual void resetItems();
    
    //
    // Observer Interface
    //
    virtual void Update(CSubject* theChangedSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

	virtual void newItem( QString & itemName );

public slots:
	virtual void deleteItem();
	virtual void copyItem();
	virtual void pasteItem();
	virtual void moveItem() {}
	virtual void refreshItems();
	virtual void itemSelected( QListViewItem * );
	virtual void updateSelected();
	virtual void convertToSpawn();

signals:
	void updatedDataMembers( IDataMembers * instance );

protected:

    static TextBuffer* s_MsgBuff;

	virtual bool eventFilter( QObject *obj, QEvent *ev );
    virtual void keyPressEvent ( QKeyEvent * e );
    virtual void keyReleaseEvent ( QKeyEvent * e );
    int m_ReOrderTrack;
    
    virtual void incrementSelectedPriority();
    virtual void decrementSelectedPriority();

	void RefreshListView();

	TrackTreeNode * GetCurrSelectedNode();
	IDataMembers * GetCurrentSelectedData();

	QListViewItem * GetLastToplevelNode();
	QListViewItem * GetLastChild(QListViewItem *);
	int GetItemIndex(const QListViewItem*) const;
	QListViewItem * GetPreviousSibling(const QListViewItem*) const;
	QListViewItem * GetNestedItem(int index);
	int GetNestedIndex(QListViewItem * pItem) const;

    ActionNode* m_ActionNode;

	int m_iLastSelectedItemIndex;

	// ActionNode accessors
	virtual int GetActionNodeItemCount() const = 0;
	virtual const char * GetActionNodeItemStr(int idx) const = 0;
	virtual IDataMembers * GetActionNodeItem(int idx) = 0;
	virtual IDataMembers * AddActionNodeItem(const char * pName) = 0;
	virtual IDataMembers * AddActionNodeItem(TextParser & textParser) = 0;
	virtual bool DeleteActionNodeItem(int idx) = 0;
	virtual bool DeleteActionNodeItem(IDataMembers *) = 0;
	virtual bool ChangeActionNodeItemPriority(int idx, bool increase) = 0;

    //
    // Observer stuff
    //
    virtual const char* remoteEditName() = 0;
    virtual void attachObservers() = 0;
    virtual void dettachObservers() = 0;

    void notifyNewTrack(Track* newTrack);
    void notifyDeleteTrack(int TrackID);
    void notifyChangePriority(int TrackID, bool increase);
};

//------------------------------------------------------------------------------

class TracksListView : public TracksListViewBase
{
	Q_OBJECT
public:
	TracksListView( QWidget * parent = 0, const char * name = 0, WFlags f = 0 );
	~TracksListView();

protected:
	// ActionNode accessors
	virtual int GetActionNodeItemCount() const;
	virtual const char * GetActionNodeItemStr(int idx) const;
	virtual IDataMembers * GetActionNodeItem(int idx);
	virtual IDataMembers * AddActionNodeItem(const char * pName);
	virtual IDataMembers * AddActionNodeItem(TextParser & textParser);
	virtual bool DeleteActionNodeItem(int idx);
	virtual bool DeleteActionNodeItem(IDataMembers *);
	virtual bool ChangeActionNodeItemPriority(int idx, bool increase);

	//
    // Observer stuff
    //
    virtual const char* remoteEditName(){return("TRACK");}
    virtual void attachObservers();
    virtual void dettachObservers();
};

//------------------------------------------------------------------------------

class ConditionListView : public TracksListViewBase
{
	Q_OBJECT
public:
	ConditionListView( QWidget * parent = 0, const char * name = 0, WFlags f = 0 );
	~ConditionListView();

protected:
	// ActionNode accessors
	virtual int GetActionNodeItemCount() const;
	virtual const char * GetActionNodeItemStr(int idx) const;
	virtual IDataMembers * GetActionNodeItem(int idx);
	virtual IDataMembers * AddActionNodeItem(const char * pName);
	virtual IDataMembers * AddActionNodeItem(TextParser & textParser);
	virtual bool DeleteActionNodeItem(int idx);
	virtual bool DeleteActionNodeItem(IDataMembers *);
	virtual bool ChangeActionNodeItemPriority(int idx, bool increase);

	//
    // Observer stuff
    //
    virtual const char* remoteEditName(){return("CONDITION");}
    virtual void attachObservers();
    virtual void dettachObservers();
};

//------------------------------------------------------------------------------

class ConditionListDataMemberView : public TracksListViewBase
{
	Q_OBJECT
public:
	ConditionListDataMemberView( QWidget * parent = 0, const char * name = 0, WFlags f = 0 );
	~ConditionListDataMemberView();

    void SetDataMember( IDataMembers * dataMember, IGroupDataMember* dataMemberInterface );

    IDataMembers* GetCurrentSelectedData(void);

private:
    IGroupDataMember*  m_dataMemberInterface;
    IDataMembers*	  m_dataMembersInstance;

	// ActionNode accessors
	virtual int GetActionNodeItemCount() const;
	virtual const char * GetActionNodeItemStr(int idx) const;
	virtual IDataMembers * GetActionNodeItem(int idx);
	virtual IDataMembers * AddActionNodeItem(const char * pName);
	virtual IDataMembers * AddActionNodeItem(TextParser & textParser);
	virtual bool DeleteActionNodeItem(int idx);
	virtual bool DeleteActionNodeItem(IDataMembers *);
	virtual bool ChangeActionNodeItemPriority(int idx, bool increase);

	virtual void itemSelected( QListViewItem * );

	//
    // This wont work
    //
    virtual const char* remoteEditName(){return("CONDITIONGROUP");}
    virtual void attachObservers(){}
    virtual void dettachObservers(){}
};

//------------------------------------------------------------------------------

class ActionNodeView : public QWidget
{
	
	Q_OBJECT
public:

	ActionNodeView( QWidget * parent = 0, const char * name = 0, WFlags f = 0 );
	~ActionNodeView();

	void SetActionNode(ActionNode* actionNode);

    //DataMemberIntSlider* m_EditPriorityView;
	ConditionListView*	m_conditionView;
	TracksListView*		m_tracksView;
   
    ViewProperties* m_ViewProperties;
    void SyncWithViewProperies(void);
    void SaveViewProperies(void);

    virtual bool close( bool alsoDelete ); 

public slots:
	virtual void refresh();
    virtual void play();
    virtual void stop();
    virtual void forward();
    virtual void pause(bool on);
    virtual void setSimRate(const QString&);
	virtual void setComment();

signals:
    void closedSignal(bool on_off);
private:

	void refreshConditionListView(void);
	void refreshTracksListView(void);

	ActionNode* m_ActionNode;

	int m_conditionViewIndex;
	int m_tracksViewIndex;

	QPopupMenu* m_ConditionViewPopup;
	QPopupMenu* m_TracksViewPopup;

    QGridLayout* m_ActionNodeViewLayout;

	//
	// View Widgets
	//
	QLineEdit*			m_NodeName;
    QSpinBox*			m_SimRate;
    QPushButton*        m_PlayButton;
    QPushButton*        m_Pause;
    QPushButton*        m_Stop;
    QPushButton*        m_Forward;
	QTextEdit*          m_Comment;

    static char s_msgBuff[1024];
};

//------------------------------------------------------------------------------

inline bool ActionNodeView::close( bool alsoDelete )
{
    closedSignal(false); //signal
    return(QWidget::close(alsoDelete));
}

//------------------------------------------------------------------------------

class ActionNodeListView : public QListView , public IObserver
{
    Q_OBJECT
public:
    ActionNodeListView(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );

    virtual void Update(CSubject* theChangedSubject);
    virtual void UpdateDetach(CSubject* theChangedSubject);

public slots:
    void clickedHandler( QListViewItem* item, const QPoint& selectionPoint, int column);

};
#endif
