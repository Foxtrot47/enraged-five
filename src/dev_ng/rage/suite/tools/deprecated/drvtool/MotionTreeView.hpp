#ifndef MOTIONTREEVEIW_H
#define MOTIONTREEVEIW_H

#include "allActionTree.h"
#include "../../drvToolInclude/application.h"
#include "ActionNodeView.hpp"
#include <qlistview.h>

#include "ActionControllerView.hpp"
class QPopupMenu;

//---------------------------------------------------------------------------
// MotionTreeView... 
// Since the ActionControllerOwnerView would get unrully if it displayed
// MotionTree info in the same view it gets dispatched to this view
//---------------------------------------------------------------------------
class MotionTreeView : public ObjectParameterHistoryView
{
    Q_OBJECT

public:
	enum ColumnTypes
	{
		COL_NODE,
		COL_WEIGHT,
		COL_DIRECTION,
		COL_RATE,
		COL_TIME,
		COL_ANIMATION,
		NUM_COL_TYPES,
	};

public:

    MotionTreeView(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~MotionTreeView();

    void initStyle(void);
    void Update(void);
	void ClearAllViews();

	virtual bool DoesItemExist(QString& guidNumber, QString& controllerName);

    virtual void UpdateDetach(CSubject* theChangedSubject);
    virtual bool HandleMessage(CBasicString* msg);

    //
    // This is just to have a connection to some Properties data
    //
    ViewProperties* m_ViewProperties;
    void SyncWithViewProperies(void);
    void SaveViewProperies(void);

    void OnUpdate(QListViewItem* objectClassName, TextParser& message);

    //
    // This can be called by multiple Threads
    //
    void RecieveMessage(CBasicString* msg);
    void handleMessage(CBasicString* msg);

	//
	// This is just to Add and remove a reciever point for
	// a motionTree update
	//
    virtual void AddFullPathCopy(QListViewItem* objectParameter);
    virtual void RemoveFullPathCopy(QListViewItem* objectParameter);

	//
	// Control history viewing
	//
	virtual void SetHistoryLevel(int count);
	virtual int  GetHistoryLevel(void);

	virtual void setminwidth(int w);

public slots:
    void rightButtonPressedHandler( QListViewItem* item, const QPoint& selectionPoint, int column);
    void clickedHandler( QListViewItem* item, const QPoint& selectionPoint, int column);

private:

	QHBoxLayout* m_gridLayout;

    QPopupMenu* m_ObjectClassPopupMenu;
    QPopupMenu* m_ObjectPopupMenu;

    bool m_sem;
    std::vector<CBasicString*>m_MessageBuff;
    bool m_Updated;

	std::vector<QListView*>m_PlayHistoryView;
};


//---------------------------------------------------------------------------
// QListViewItemMTNode... 
// it Really is a motionTree View
//---------------------------------------------------------------------------
class QListViewItemMTNode : public QListViewItem
{
public:
    QListViewItemMTNode(QListViewItem* parent, QListViewItem* after, int refPointer);    
    ~QListViewItemMTNode();

	static QListViewItemMTNode* CreateNode(QListViewItem* parent, QListViewItem* after, TextParser& message, int inGamePointer, char* nodeType, int& childCount);
	
	virtual int GetChildCount(void)=0;

public:
	std::vector<float> m_Weights; // this should be in the parentView
	std::vector<float> m_WeightRates; // this should be in the parentView

	int m_RefPointer; // this is the in-game pointer...
};

//---------------------------------------------------------------------------
// crmtNodeParentView... 
//---------------------------------------------------------------------------
class crmtNodeParentView: public QListViewItemMTNode
{
public:
	crmtNodeParentView(QListViewItem* parent, QListViewItem* after, int refPointer):QListViewItemMTNode ( parent, after, refPointer ),m_ChildCount(0){}
    ~crmtNodeParentView(){}

	virtual void Read(TextParser& constructInfo);
	virtual int GetChildCount(void){return(m_ChildCount);}

protected:
	int m_ChildCount;
};

//---------------------------------------------------------------------------
// crmtBlendNNodeView... 
//---------------------------------------------------------------------------
class crmtBlendNNodeView: public crmtNodeParentView
{
public:
    crmtBlendNNodeView(QListViewItem* parent, QListViewItem* after, int refPointer);
    ~crmtBlendNNodeView(){}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);

};

//---------------------------------------------------------------------------
// crmtBlendNodeView... 
//---------------------------------------------------------------------------
class crmtBlendNodeView: public crmtNodeParentView
{
public:
    crmtBlendNodeView(QListViewItem* parent, QListViewItem* after, int refPointer);
    ~crmtBlendNodeView(){}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);

};

//---------------------------------------------------------------------------
// crmtBlendRateNodeView... 
//---------------------------------------------------------------------------
class crmtBlendRateNodeView: public crmtBlendNodeView
{
public:
    crmtBlendRateNodeView(QListViewItem* parent, QListViewItem* after, int refPointer);
    ~crmtBlendRateNodeView(){}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);
};

//---------------------------------------------------------------------------
// crmtBlendNodeDirectionView... 
//---------------------------------------------------------------------------
class crmtBlendNodeDirectionView: public crmtBlendNNodeView
{
public:
    crmtBlendNodeDirectionView(QListViewItem* parent, QListViewItem* after, int refPointer);
    ~crmtBlendNodeDirectionView(){}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);

};

//---------------------------------------------------------------------------
// crmtNodeAddSubtract... 
//---------------------------------------------------------------------------
class crmtNodeAddSubtractView: public crmtNodeParentView
{
public:
    crmtNodeAddSubtractView(QListViewItem* parent, QListViewItem* after, int refPointer);
    ~crmtNodeAddSubtractView(){}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);
};

//---------------------------------------------------------------------------
// crmtAnimNodeView... 
//---------------------------------------------------------------------------
class crmtAnimNodeView: public QListViewItemMTNode
{
public:
    crmtAnimNodeView(QListViewItem* parent, QListViewItem* after, int refPointer);
    ~crmtAnimNodeView(){}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);

	virtual int GetChildCount(void){return(0);}
};

//------------------------------------------------------------------------------
// crmtNodeFilterView
//------------------------------------------------------------------------------
class crmtNodeFilterView: public crmtNodeParentView
{
public:
    crmtNodeFilterView(QListViewItem* parent, QListViewItem* after, int refPointer);
    virtual ~crmtNodeFilterView() {}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);

}; // class crmtNodeFilterView

//------------------------------------------------------------------------------
// crmtNodeMoverView
//------------------------------------------------------------------------------
class crmtNodeMoverView: public crmtNodeParentView
{
public:
    crmtNodeMoverView(QListViewItem* parent, QListViewItem* after, int refPointer);
    virtual ~crmtNodeMoverView() {}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);

}; // class crmtNodeMoverView

//------------------------------------------------------------------------------
// crmtNodeRagdollView
//------------------------------------------------------------------------------
class crmtNodeRagdollView: public crmtNodeParentView
{
public:
    crmtNodeRagdollView(QListViewItem* parent, QListViewItem* after, int refPointer);
    virtual ~crmtNodeRagdollView() {}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);

}; // class crmtNodeRagdollView

//------------------------------------------------------------------------------
// crmtNodeExpressionView
//------------------------------------------------------------------------------
class crmtNodeExpressionView: public crmtNodeParentView
{
public:
	crmtNodeExpressionView(QListViewItem* parent, QListViewItem* after, int refPointer);
	virtual ~crmtNodeExpressionView() {}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);

}; // class crmtNodeExpressionView

//------------------------------------------------------------------------------
// crmtNodePoseView
//------------------------------------------------------------------------------
class crmtNodePoseView: public QListViewItemMTNode
{
public:
	crmtNodePoseView(QListViewItem* parent, QListViewItem* after, int refPointer);
	virtual ~crmtNodePoseView() {}

	static QListViewItemMTNode* Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer);

	virtual void Read(TextParser& constructInfo);

	virtual int GetChildCount() { return 0; }

}; // class crmtNodePoseView

//---------------------------------------------------------------------------
// MotionTreeEditView... 
// A simple MotionTreeEditView
//---------------------------------------------------------------------------
class MotionTreeEditView : public QListView
{
    Q_OBJECT

public:
	enum ColumnTypes
	{
		COL_NODE,
		COL_WEIGHT,
		COL_DIRECTION,
		COL_TIME,
		COL_ANIMATION,
		NUM_COL_TYPES,
	};

public:

    MotionTreeEditView(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~MotionTreeEditView();

    //
    // This is just to have a connection to some Properties data
    //
    ViewProperties* m_ViewProperties;
    void SyncWithViewProperies(void);
    void SaveViewProperies(void);

    virtual bool close( bool alsoDelete ); 
	
	void createnode(void);
signals:
    void closedSignal(bool on_off);

public slots:
    void rightButtonPressedHandler( QListViewItem* item, const QPoint& selectionPoint, int column);
    void clickedHandler( QListViewItem* item, const QPoint& selectionPoint, int column);

private:
    QPopupMenu* m_ObjectClassPopupMenu;
    QPopupMenu* m_ObjectPopupMenu;

};

inline bool MotionTreeEditView::close( bool alsoDelete )
{
    closedSignal(false); //signal
    return(QWidget::close(alsoDelete));
}

#endif //MOTIONTREEVEIW_H