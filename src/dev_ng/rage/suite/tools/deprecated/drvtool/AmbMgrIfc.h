#ifndef AMBMGRIFC_CPP
#define AMBMGRIFC_CPP



#define MAX_COMMAND_QUEUE_SIZE 100

// strange sony defines
typedef unsigned short half;			// 16bit
typedef unsigned int word;				// 32bit

typedef enum
{
// Receiving events
	CMD_Init= 0,
	CMD_Kill,

// Send events
	CMD_ShowTrueRadii,
	CMD_SelectAmbience,
	CMD_CubeHasSource,
	CMD_MoveAmbience,
	CMD_ResizeAmbience,
	CMD_SetCubeSourcePosition,
	CMD_ReloadAmbienceFile,
	CMD_TransformAmbience,
	CMD_LinkCameras,
	
// Send and receive events
	CMD_SetCameraPosition,

	CMD_NULL,
} CommandListEnum;



typedef struct
{
	short WhichAmb;
} CMD_SelectAmbienceType;

typedef struct
{
	float x;
	float y;
	float z;
} StandardVector;

typedef struct
{
	StandardVector Look;
	StandardVector Pos;
} CMD_SetCameraPositionType;

typedef struct 
{
	StandardVector WorldPos;
	StandardVector Dimensions;
	StandardVector SourcePos;
	unsigned short RotXY;
} CMD_TransformAmbienceType;

typedef union
{
	bool ShowTrueRadii;
	bool CubeHasSource;
	bool LinkCameras;
	CMD_SelectAmbienceType SelectAmbience;
	StandardVector MoveAmbience;
	StandardVector ResizeAmbience;
	StandardVector SetCubeSourcePosition;
	CMD_SetCameraPositionType SetCameraPosition;
	CMD_TransformAmbienceType TransformAmbience;
} CommandUnionType;



struct CommandDataType
{
	CommandDataType();

	CommandListEnum Type;
	bool Received;
	CommandUnionType Data;
};



class AmbMgrIfc
{
public:
	static bool Connect(const char *hostname);
	static bool Disconnect();
	static void CommandReceived(CommandListEnum type, void *data);
	static void CommandToSend(CommandListEnum type, CommandUnionType *data);
	static void Update();

	static bool GetCameraPos(StandardVector &pos, StandardVector &look);
	static void SetShowTrueRadii(bool on);
	static void SetLinkCameras(bool on);
	static void SelectAmbience(long index);
	static void ReloadAmbienceFile();
	static void TransformAmbience(StandardVector *WorldPos,	StandardVector *Dimensions,	StandardVector *SourcePos, unsigned short *RotXY);

    static void SendMsg(const char* msg);

private:
	static bool AmbienceMgrInited;

	static short NumCommandsQueued;
	static CommandDataType CommandList[MAX_COMMAND_QUEUE_SIZE];

	static void SendCommand(short commandNum);
};



#endif 
