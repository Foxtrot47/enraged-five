
#include <qpopupmenu.h>
#include <qsplitter.h> 

#include "MotionTreeView.hpp"
#include "ActionNodeView.hpp"
#include "actiontree.h"

extern Application* g_theApp;
extern ActionTree* g_ActionTree;
extern MotionTreeView* g_MotionTreeView;

static int s_playHistory = 10;

//---------------------------------------------------------------------------
// Generic util funktions
//---------------------------------------------------------------------------
extern QListViewItem* findChild(QListViewItem* parent, QString& text, int textID);
extern void ClearChildren(QListViewItem* parent);
extern QListViewItem* findChildView(QListView* parent, QString& text, int textID);

extern char *s_red_exclamation[];
static char s_nodeNamePath[1024];

char animNameBuff[128];

//---------------------------------------------------------------------------
// Parent helper function
//---------------------------------------------------------------------------
QListViewItemMTNode::QListViewItemMTNode(QListViewItem* parent, QListViewItem* after, int refPointer)
:
QListViewItem ( parent, after ),
m_RefPointer(refPointer)
{
	//
	// base 16 for Hex
	//
	setText(MotionTreeView::COL_NODE, QString::number(m_RefPointer, 16));

	//
	// add parent weights
	//
	QListViewItemMTNode *pParent = dynamic_cast<QListViewItemMTNode *>(parent);
	if (pParent)
	{
		if (pParent->m_Weights.size())
		{
			std::vector<float>::iterator it(pParent->m_Weights.end());
			it--;
			setText(MotionTreeView::COL_WEIGHT, QString::number(*it));
			pParent->m_Weights.pop_back();
		}
		if (pParent->m_WeightRates.size())
		{
			std::vector<float>::iterator it(pParent->m_WeightRates.end());
			it--;
			setText(MotionTreeView::COL_RATE, QString::number(*it));
			pParent->m_WeightRates.pop_back();
		}
	}
}

QListViewItemMTNode::~QListViewItemMTNode()
{

}

//---------------------------------------------------------------------------
// Parent helper function
//---------------------------------------------------------------------------
void crmtNodeParentView::Read(TextParser& constructInfo)
{
	// if this is a node Parent
	if(constructInfo.MatchCurrentToken("num"))
	{
		constructInfo.GetNextToken(); // num
		constructInfo.GetNextToken(); // children			
		m_ChildCount = constructInfo.GetTokenAsInt();
		constructInfo.GetNextToken(); // value
		return;
	}
	
	Assert(0);
}

//---------------------------------------------------------------------------
// Classed Per Nodetype
//---------------------------------------------------------------------------

crmtBlendNNodeView::crmtBlendNNodeView(QListViewItem* parent, QListViewItem* after, int refPointer)
:
crmtNodeParentView ( parent, after, refPointer)
{
	static QPixmap icon = QPixmap::fromMimeSource( "crmtNodeBlendN.PNG" );
	setPixmap(MotionTreeView::COL_NODE, icon);
}

QListViewItemMTNode* crmtBlendNNodeView::Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer)
{
	crmtBlendNNodeView*	newNode = new crmtBlendNNodeView(parent, after, refPointer);
	newNode->Read(constructInfo);
	return(newNode);
}

void crmtBlendNNodeView::Read(TextParser& constructInfo)
{
	crmtNodeParentView::Read(constructInfo);

	m_Weights.resize(m_ChildCount);

	// nodeblendn has a list of blend weights and rates after 'first child <address>'
	while (constructInfo.MatchCurrentToken("child") == false)
	{
		constructInfo.GetNextToken();
		if(constructInfo.EndOfText())
			break;
	}
	constructInfo.GetNextToken(); // child
	constructInfo.GetNextToken(); // <address>

	for (int i = 0; i < m_ChildCount; i++)
	{
		constructInfo.GetNextToken(); // [<x>] weight %s
		constructInfo.GetNextToken();
		const float fBlendWeight = constructInfo.GetTokenAsFloat();

		m_Weights[(m_ChildCount - 1) - i] = fBlendWeight;
		constructInfo.GetNextToken();

		// rate <x>
		constructInfo.GetNextToken();
		constructInfo.GetNextToken();
		const float fBlendRate = constructInfo.GetTokenAsFloat();

	} // for
}

//---------------------------------------------------------------------------
// crmtBlendNodeView... 
// it Really is a motionTree View
//---------------------------------------------------------------------------
crmtBlendNodeView::crmtBlendNodeView(QListViewItem* parent, QListViewItem* after, int refPointer)
:
crmtNodeParentView ( parent, after, refPointer)
{
	static QPixmap icon = QPixmap::fromMimeSource( "crmtNodeBlend.PNG" );
	setPixmap(MotionTreeView::COL_NODE, icon);
}

QListViewItemMTNode* crmtBlendNodeView::Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer)
{
	crmtBlendNodeView*	newNode = new crmtBlendNodeView(parent, after, refPointer);
	newNode->Read(constructInfo);
	return(newNode);
}

void crmtBlendNodeView::Read(TextParser& constructInfo)
{
	crmtNodeParentView::Read(constructInfo);

	m_Weights.resize(2);
	m_WeightRates.resize(2);

	// nodeblendn has a list of blend weights and rates after 'first child <address>'
	while (constructInfo.MatchCurrentToken("weight") == false)
	{
		constructInfo.GetNextToken();
		if(constructInfo.EndOfText())
			break;
	}
	constructInfo.GetNextToken(); // weightValue

	const float fBlendWeight = constructInfo.GetTokenAsFloat();
	m_Weights[0] = fBlendWeight;
	m_Weights[1] = 1.0f - fBlendWeight;

	constructInfo.GetNextToken(); // rate
	constructInfo.GetNextToken(); // rate Value
	const float fBlendWeightRate = constructInfo.GetTokenAsFloat();
	setText(MotionTreeView::COL_RATE, QString::number(fBlendWeightRate));

	constructInfo.GetNextToken(); // filter
	constructInfo.GetNextToken(); // filter value

	constructInfo.GetNextToken(); // modifier
	constructInfo.GetNextToken(); // modifier value

	constructInfo.GetNextToken(); // minweight
	constructInfo.GetNextToken(); // minweight value

	constructInfo.GetNextToken(); // maxweight
	constructInfo.GetNextToken(); // maxweight value

	constructInfo.GetNextToken(); // destroy
	constructInfo.GetNextToken(); // destroy value

	const int destroyOnCompleat = constructInfo.GetTokenAsInt();
	setText(MotionTreeView::COL_DIRECTION, QString::number(destroyOnCompleat));

//	constructInfo.GetNextToken(); // weightset
//	const char* weightSet = constructInfo.GetTokenString();
//	setText(MotionTreeView::COL_ANIMATION, weightSet);

}

//---------------------------------------------------------------------------
// crmtBlendNodeView... 
// it Really is a motionTree View
//---------------------------------------------------------------------------
crmtBlendRateNodeView::crmtBlendRateNodeView(QListViewItem* parent, QListViewItem* after, int refPointer)
:
crmtBlendNodeView ( parent , after, refPointer)
{
	static QPixmap icon = QPixmap::fromMimeSource( "crmtNodeBlendRate.PNG" );
	setPixmap(MotionTreeView::COL_NODE, icon);
}

QListViewItemMTNode* crmtBlendRateNodeView::Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer)
{
	crmtBlendRateNodeView*	newNode = new crmtBlendRateNodeView(parent, after, refPointer);
	newNode->Read(constructInfo);
	return(newNode);
}

//---------------------------------------------------------------------------
// crmtBlendNodeDirectionView... 
// it Really is a motionTree View
//---------------------------------------------------------------------------
crmtBlendNodeDirectionView::crmtBlendNodeDirectionView(QListViewItem* parent, QListViewItem* after, int refPointer)
:
crmtBlendNNodeView ( parent , after, refPointer)
{
	static QPixmap icon = QPixmap::fromMimeSource( "crmtNodeBlendDirection.PNG" );
	setPixmap(MotionTreeView::COL_NODE, icon);
}

QListViewItemMTNode* crmtBlendNodeDirectionView::Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer)
{
	crmtBlendNodeDirectionView*	newNode = new crmtBlendNodeDirectionView(parent, after, refPointer);
	newNode->Read(constructInfo);
	return(newNode);
}

void crmtBlendNodeDirectionView::Read(TextParser& constructInfo)
{
	crmtBlendNNodeView::Read(constructInfo);

	// active direction %f
	constructInfo.GetNextToken();
	constructInfo.GetNextToken();
	const float fActiveDirection = constructInfo.GetTokenAsFloat();

	setText(MotionTreeView::COL_DIRECTION, QString::number(fActiveDirection));
}

//---------------------------------------------------------------------------
// crmtNodeAddSubtractView... 
// it Really is a motionTree View
//---------------------------------------------------------------------------
crmtNodeAddSubtractView::crmtNodeAddSubtractView(QListViewItem* parent, QListViewItem* after, int refPointer)
:
crmtNodeParentView ( parent , after, refPointer)
{
	static QPixmap icon = QPixmap::fromMimeSource( "crmtNodeAddSubtract.PNG" );
	setPixmap(MotionTreeView::COL_NODE, icon);
}

QListViewItemMTNode* crmtNodeAddSubtractView::Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer)
{
	crmtNodeAddSubtractView* newNode = new crmtNodeAddSubtractView(parent, after, refPointer);
	newNode->Read(constructInfo);
	return(newNode);
}

void crmtNodeAddSubtractView::Read(TextParser& constructInfo)
{
	crmtNodeParentView::Read(constructInfo);

	m_Weights.resize(2);
	m_WeightRates.resize(2);

	// nodeblendn has a list of blend weights and rates after 'first child <address>'
	while (constructInfo.MatchCurrentToken("weight") == false)
	{
		constructInfo.GetNextToken();
		if(constructInfo.EndOfText())
			break;
	}
	constructInfo.GetNextToken(); // weightValue

	const float fBlendWeight = constructInfo.GetTokenAsFloat();
	m_Weights[0] = fBlendWeight;
	m_Weights[1] = 1.0f;

	constructInfo.GetNextToken(); // rate
	constructInfo.GetNextToken(); // rate Value
	const float fBlendWeightRate = constructInfo.GetTokenAsFloat();
	setText(MotionTreeView::COL_RATE, QString::number(fBlendWeightRate));

	constructInfo.GetNextToken(); // destroy
	constructInfo.GetNextToken(); // destroyValue
	const int destroyOnCompleat = constructInfo.GetTokenAsInt();
	setText(MotionTreeView::COL_DIRECTION, QString::number(destroyOnCompleat));

//	constructInfo.GetNextToken(); // weightset
//	const char* weightSet = constructInfo.GetTokenString();
//	setText(MotionTreeView::COL_ANIMATION, weightSet);

}


//---------------------------------------------------------------------------
// crmtAnimNodeView... 
// it Really is a motionTree View
//---------------------------------------------------------------------------
crmtAnimNodeView::crmtAnimNodeView(QListViewItem* parent, QListViewItem* after, int refPointer)
:
QListViewItemMTNode ( parent, after, refPointer )
{
	static QPixmap icon = QPixmap::fromMimeSource( "crmtNodeAnimation.PNG" );
	setPixmap(MotionTreeView::COL_NODE, icon);
}

QListViewItemMTNode* crmtAnimNodeView::Create(QListViewItem* parent, QListViewItem* after, TextParser& constructInfo, int refPointer)
{
	crmtAnimNodeView* newNode = new crmtAnimNodeView(parent, after, refPointer);
	newNode->Read(constructInfo);
	return(newNode);
}

void crmtAnimNodeView::Read(TextParser& constructInfo)
{
	if(constructInfo.MatchCurrentToken("anim")) 
	{
		constructInfo.GetNextToken(); // the animation Name
		constructInfo.GetTokenString(animNameBuff, 128);
		setText(MotionTreeView::COL_ANIMATION, animNameBuff);
		
		// Time
		constructInfo.GetNextToken();// time animation Name
		constructInfo.GetNextToken();// the timeValue
		float playTime = constructInfo.GetTokenAsFloat();
		// Duration
		constructInfo.GetNextToken();// duration Name
		constructInfo.GetNextToken();// the timeValue
		float durationTime = constructInfo.GetTokenAsFloat();
		// looping
		constructInfo.GetNextToken();// looping Name
		constructInfo.GetNextToken();// looping Value
		// rate
		constructInfo.GetNextToken();// rate Name
		constructInfo.GetNextToken();// rate Value
		float rate = constructInfo.GetTokenAsFloat();
		setText(MotionTreeView::COL_RATE, QString::number(rate));
		float normalizedTime = playTime/durationTime;
		static const QString sep("/");
		QString displayTime = QString::number(playTime) + sep + QString::number(durationTime) + sep + QString::number(normalizedTime);
		//setText(MotionTreeView::COL_TIME, QString::number(playTime));			
		setText(MotionTreeView::COL_TIME, displayTime);			
	}

	//Assert(0);
}

//------------------------------------------------------------------------------
// crmtNodeFilterView
//------------------------------------------------------------------------------
crmtNodeFilterView::crmtNodeFilterView(QListViewItem *pParent, QListViewItem *pAfter, int iRefPointer) :
	crmtNodeParentView(pParent, pAfter, iRefPointer)
{
	static QPixmap s_Icon = QPixmap::fromMimeSource("crmtNodeFilter.png");
	setPixmap(MotionTreeView::COL_NODE, s_Icon);

} // crmtNodeFilterView::crmtNodeFilterView

QListViewItemMTNode* crmtNodeFilterView::Create(QListViewItem *pParent, QListViewItem *pAfter, TextParser &theTextParser, int iRefPointer)
{
	crmtNodeFilterView *pNewNode = new crmtNodeFilterView(pParent, pAfter, iRefPointer);
	pNewNode->Read(theTextParser);
	return pNewNode;

} // crmtNodeFilterView::Create

void crmtNodeFilterView::Read(TextParser &theTextParser)
{
	crmtNodeParentView::Read(theTextParser);

} // crmtNodeFilterView::Read


//------------------------------------------------------------------------------
// crmtNodeMoverView
//------------------------------------------------------------------------------
crmtNodeMoverView::crmtNodeMoverView(QListViewItem *pParent, QListViewItem *pAfter, int iRefPointer) :
	crmtNodeParentView(pParent, pAfter, iRefPointer)
{
	static QPixmap s_Icon = QPixmap::fromMimeSource("crmtNodeMover.png");
	setPixmap(MotionTreeView::COL_NODE, s_Icon);

} // crmtNodeMoverView::crmtNodeMoverView

QListViewItemMTNode* crmtNodeMoverView::Create(QListViewItem *pParent, QListViewItem *pAfter, TextParser &theTextParser, int iRefPointer)
{
	crmtNodeMoverView *pNewNode = new crmtNodeMoverView(pParent, pAfter, iRefPointer);
	pNewNode->Read(theTextParser);
	return pNewNode;

} // crmtNodeMoverView::Create

void crmtNodeMoverView::Read(TextParser &theTextParser)
{
	crmtNodeParentView::Read(theTextParser);

} // crmtNodeMoverView::Read


//------------------------------------------------------------------------------
// crmtNodeRagdollView
//------------------------------------------------------------------------------
crmtNodeRagdollView::crmtNodeRagdollView(QListViewItem *pParent, QListViewItem *pAfter, int iRefPointer) :
	crmtNodeParentView(pParent, pAfter, iRefPointer)
{
	static QPixmap s_Icon = QPixmap::fromMimeSource("crmtNodeRagdoll.png");
	setPixmap(MotionTreeView::COL_NODE, s_Icon);

} // crmtNodeRagdollView::crmtNodeRagdollView

QListViewItemMTNode* crmtNodeRagdollView::Create(QListViewItem *pParent, QListViewItem *pAfter, TextParser &theTextParser, int iRefPointer)
{
	crmtNodeRagdollView *pNewNode = new crmtNodeRagdollView(pParent, pAfter, iRefPointer);
	pNewNode->Read(theTextParser);
	return pNewNode;

} // crmtNodeRagdollView::Create

void crmtNodeRagdollView::Read(TextParser &theTextParser)
{
	crmtNodeParentView::Read(theTextParser);

} // crmtNodeRagdollView::Read


//------------------------------------------------------------------------------
// crmtNodeExpressionView
//------------------------------------------------------------------------------
crmtNodeExpressionView::crmtNodeExpressionView(QListViewItem *pParent, QListViewItem *pAfter, int iRefPointer) :
crmtNodeParentView(pParent, pAfter, iRefPointer)
{
	static QPixmap s_Icon = QPixmap::fromMimeSource("crmtNodeExpression.png");
	setPixmap(MotionTreeView::COL_NODE, s_Icon);

} // crmtNodeExpressionView::crmtNodeExpressionView

QListViewItemMTNode* crmtNodeExpressionView::Create(QListViewItem *pParent, QListViewItem *pAfter, TextParser &theTextParser, int iRefPointer)
{
	crmtNodeExpressionView *pNewNode = new crmtNodeExpressionView(pParent, pAfter, iRefPointer);
	pNewNode->Read(theTextParser);
	return pNewNode;

} // crmtNodeExpressionView::Create

void crmtNodeExpressionView::Read(TextParser &theTextParser)
{
	crmtNodeParentView::Read(theTextParser);

	while (theTextParser.MatchCurrentToken("weight") == false)
	{
		theTextParser.GetNextToken();
		if (theTextParser.EndOfText())
			break;
	}
	theTextParser.GetNextToken(); // weight value

	const float fExpressionsWeight = theTextParser.GetTokenAsFloat();
	setText(MotionTreeView::COL_WEIGHT, QString::number(fExpressionsWeight));

} // crmtNodeExpressionView::Read


//------------------------------------------------------------------------------
// crmtNodePoseView
//------------------------------------------------------------------------------
crmtNodePoseView::crmtNodePoseView(QListViewItem *pParent, QListViewItem *pAfter, int iRefPointer) :
QListViewItemMTNode(pParent, pAfter, iRefPointer)
{
	static QPixmap s_Icon = QPixmap::fromMimeSource("crmtNodePose.png");
	setPixmap(MotionTreeView::COL_NODE, s_Icon);

} // crmtNodePoseView::crmtNodePoseView

QListViewItemMTNode* crmtNodePoseView::Create(QListViewItem *pParent, QListViewItem *pAfter, TextParser &theTextParser, int iRefPointer)
{
	crmtNodePoseView *pNewNode = new crmtNodePoseView(pParent, pAfter, iRefPointer);
	pNewNode->Read(theTextParser);
	return pNewNode;

} // crmtNodePoseView::Create

void crmtNodePoseView::Read(TextParser &theTextParser)
{
} // crmtNodeExpressionView::Read


//----------------------------------------------------------------------------------
typedef QListViewItemMTNode* (*CreateCommand)(QListViewItem*, QListViewItem*, TextParser&, int);
struct CreateCommandMTNode
{
	const char* m_nodeName;
    CreateCommand m_CommandFunction;
};

CreateCommandMTNode g_NodeTypeCreateCommands[] = 
{    
	{"crmtNodeAnimation",		&crmtAnimNodeView::Create}, 
	{"crmtNodeEventAnimation",	&crmtAnimNodeView::Create}, 
	{"crmtNodeAddSubtract",		&crmtNodeAddSubtractView::Create},   
	{"crmtNodeBlendDirection",	&crmtBlendNodeDirectionView::Create},   
	{"crmtNodeBlend",			&crmtBlendNodeView::Create},   
	{"crmtNodeBlendN",			&crmtBlendNNodeView::Create},
	{"crmtNodeBlendRate",		&crmtBlendRateNodeView::Create},
	{"crmtNodeBlendNCapture",	&crmtBlendNNodeView::Create},
	{"crmtNodeFilter",			&crmtNodeFilterView::Create},
	{"crmtNodeMover",			&crmtNodeMoverView::Create},
	{"crmtNodeRagdoll",			&crmtNodeRagdollView::Create},
	{"crmtNodeExpression",		&crmtNodeExpressionView::Create},
	{"crmtNodePose",			&crmtNodePoseView::Create},
};
const int g_NodeTypeCreateCommandsCount = sizeof(g_NodeTypeCreateCommands) / sizeof(CreateCommandMTNode);

//---------------------------------------------------------------------------
// QListViewItemMTNode... 
// it Really is a motionTree View
//---------------------------------------------------------------------------
QListViewItemMTNode* QListViewItemMTNode::CreateNode
(
	QListViewItem* parent,
	QListViewItem* after,
	TextParser& playinfo, 
	int inGamePointer,
	char* nodeType, 
	int& childCount
)
{
	QListViewItemMTNode* animNode = NULL;

	for(int i=0;i<g_NodeTypeCreateCommandsCount && animNode==NULL;i++)
	{
		CreateCommandMTNode regCommand = g_NodeTypeCreateCommands[i];	       		
        if(strcmp(nodeType, regCommand.m_nodeName) == 0)
        {
			animNode = (regCommand.m_CommandFunction)(parent, after, playinfo, inGamePointer);
        }
    }

	if (animNode)
	{
		childCount = animNode->GetChildCount();	
	}
	else
	{
		AssertMsg(false, ("AnimNode Not Found for NodeType: %s", nodeType));
	}
	return(animNode);
}

char pointerBuff[64];
char nodeTypeBuff[64];
char parentPointerBuff[64];
char nodeSiblingBuff[64];

//scanf("node %s type '%s' refs %d",pointerBuff, nodeTypeBuff, &refcount);
//scanf("parent %s sibling %p", parentPointerBuff, m_SiblingNode);
// Fix this... This code should be moved into the QListViewItemMTNode class
void buildDebutMT(QListViewItem* mtRoot, TextParser& message)
{
	TextParser playinfo(message.GetCurrentReadPosition(), " ,\t\r\n|+'");

	QListViewItem *pLastParentItem = mtRoot;
	QListViewItem *pLastChildItem = NULL;

	std::vector<QListViewItem*> parentStack;
	parentStack.reserve(20);	    
	parentStack.push_back(mtRoot);

	while(!playinfo.EndOfText())
	{
		while(playinfo.MatchCurrentToken("node") == false)
		{
			playinfo.GetNextToken();
			if(playinfo.EndOfText())
				return;
		}
		
		int refs;

		// node %s
		playinfo.GetNextToken();
		playinfo.GetTokenString(pointerBuff, 64);
		int inGamePointer = 0;
		sscanf(pointerBuff, "%x", &inGamePointer);
		playinfo.GetNextToken();

		// type %s
		playinfo.GetNextToken();
		playinfo.GetTokenString(nodeTypeBuff, 64);
		playinfo.GetNextToken();

		// refs %s
		playinfo.GetNextToken();
		refs = playinfo.GetTokenAsInt();
		playinfo.GetNextToken();

		// parent %s
		playinfo.GetNextToken();
		playinfo.GetTokenString(parentPointerBuff, 64);
		playinfo.GetNextToken();

		// sibling %s
		playinfo.GetNextToken();
		playinfo.GetTokenString(parentPointerBuff, 64);
		playinfo.GetNextToken();

 		int childCount = 0;

		//
		// Current Parent
		//
		QListViewItem* currentparent = NULL;
		if(parentStack.size() > 0)
		{
			std::vector<QListViewItem*>::iterator it(parentStack.end());
			it--;
			currentparent = *it;
			parentStack.pop_back();
		}
		else
		{
			Assert(0);
			return;
		}

		// if the last parent item is not the same as the current parent item,
		//	then we've moved to a different parent, in which case we want
		//	the last child item to be the last parent in order to add new
		//	children to the same root.
		if (pLastParentItem != currentparent)
		{
			pLastChildItem = pLastParentItem;
		}

		pLastParentItem = currentparent;

		QListViewItem* animNode = QListViewItemMTNode::CreateNode( currentparent, pLastChildItem, playinfo, inGamePointer, nodeTypeBuff, childCount);
		if (animNode)
		{
			pLastChildItem = animNode;

			animNode->setOpen(true);
		
			if(childCount)
			{
				for(int i=0;i<childCount;i++)
				{
					parentStack.push_back(animNode);
				}
			}
		}
	}

	Assert(parentStack.size() == 0);

}

//---------------------------------------------------------------------------
// View for MotionTreeView
//---------------------------------------------------------------------------
#include <qlayout.h>
MotionTreeView::MotionTreeView(QWidget* parent, const char* name, WFlags fl)
:
ObjectParameterHistoryView(parent, name, fl)
{
	m_PlayHistoryView.reserve(10);
	SetHistoryLevel(1);

    m_sem = 0;
    m_Updated = false;

}

MotionTreeView::~MotionTreeView()
{

}

void MotionTreeView::rightButtonPressedHandler 
( 
    QListViewItem *item, 
    const QPoint& coords, 
    int column
)
{
   if(item)
   {
       m_ObjectClassPopupMenu->move( coords );
       m_ObjectClassPopupMenu->show();
   }
}

//---------------------------------------------------------------------------
// On mouse click for the Owner View
//---------------------------------------------------------------------------
void MotionTreeView::clickedHandler 
( 
 QListViewItem *item, 
 const QPoint& coords, 
 int column
 )
{
    if(item)
    {
        QCheckListItem* objectParameter = dynamic_cast<QCheckListItem*>(item);
        if(objectParameter)
        {
            int isChecked = objectParameter->isOn()?1:0;

            QListViewItem* ObjectInstance = objectParameter->parent();
            if(ObjectInstance)
            {
                QListViewItem* ObjectClass = ObjectInstance->parent();
                if(ObjectClass)
                {
  
                }
            }
        }
    }
}

void MotionTreeView::RecieveMessage(CBasicString* message)
{

}

//
// note: ActionControllerView and MotionTreeView are basically the same...
// this funktion only differs by calling:UpdateMT(); instead of Update();
// Consider Uniting the Fucking Godamn thing ;)
//
void MotionTreeView::Update(void)
{
	if (!m_Updated) return;

	const int historyLevel = m_PlayHistoryView.size();

	// for all views
	QListViewItemIterator it( m_PlayHistoryView[0] );
	while ( it.current() ) 
	{
		QListViewItem *item = it.current();
		++it;

		//
		// Dynamic_cast to find a current runnining parameterStore
		//
		QListViewItemParameterStore* itemParameterStoreCurrent = dynamic_cast<QListViewItemParameterStore*>(item);
		if ( itemParameterStoreCurrent )
		{
			itemParameterStoreCurrent->UpdateMT();

			//
			// Now find subquent QListViewItemParameterStore* items in the history view...
			//				
			int history = 1;
			QListViewItemParameterStore* nextHistory = itemParameterStoreCurrent->m_NextHistoryItem;

			//
			// Yes I'm seeting the string pointer in the 
			//
			CBasicString* playInfo = itemParameterStoreCurrent->GetHistory(history);
			while(nextHistory && playInfo)
			{
				CBasicString* playRecord = new CBasicString(playInfo->GetString());
				nextHistory->Playing(playRecord);
				nextHistory->UpdateMT();
				nextHistory->clearHistory(); // we're not 'storing' updateInfos in this view

				//
				// Next
				//
				history++;
				playInfo = itemParameterStoreCurrent->GetHistory(history);
				nextHistory = nextHistory->m_NextHistoryItem;
			}
		}
	}

	m_Updated = false;
}

// deprecate
bool MotionTreeView::HandleMessage(CBasicString* msg)
{
    return(false);
}

// deprecate
void MotionTreeView::handleMessage(CBasicString* msg)
{

}

//
// OnUpdate should be setup as a message reciever like the HandleMessage 
// used to above
//
void MotionTreeView::OnUpdate
(
    QListViewItem* objectClassName, 
    TextParser& message
)
{
    char charBuff[64];
    message.GetTokenString(charBuff, 64);
    QString objectHandle(charBuff);

    QListViewItem* objectInstance = m_PlayHistoryView[0]->findItem( objectHandle, ActionControllerOwnerView::COL_POINTER);
    
	if(objectInstance == NULL)
	{
		// Create One 
		
	}
	
	if(objectInstance)
    {
        message.GetNextToken();
        message.GetTokenString(charBuff, 64);
        QString objectParameterName(charBuff);
        QListViewItem* objectParameter = findChild(objectInstance, objectParameterName, 0);
        QListViewItemParameterStore* itemParameterStore = dynamic_cast<QListViewItemParameterStore*>(objectParameter);
        if(itemParameterStore)
        {
            const char* messageRaw = message.GetCurrentReadPosition();
            CBasicString* playRecord = new CBasicString(messageRaw);
            itemParameterStore->Playing(playRecord);
			m_Updated = true;
        }
    }
}

void MotionTreeView::UpdateDetach(CSubject* theChangedSubject)
{
  //  theChangedSubject->Dettach(this);
}

extern void SyncWithViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);
extern void SaveViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);

void MotionTreeView::SyncWithViewProperies(void)
{
    SyncWithViewProperiesUtil(this, m_ViewProperties);
}

void MotionTreeView::SaveViewProperies(void)
{
    SaveViewProperiesUtil(this, m_ViewProperties);
}

//
// This adds and inserts a copy of the tree branch that leads to the objectParameter
// This practically is the actionController instance for a particular charcter 
// this is used to represent another view of the Running actionController
//
void MotionTreeView::AddFullPathCopy(QListViewItem* objectParameter)
{

	const int historyLevel = m_PlayHistoryView.size();

	// objectParameter is coming from ActionControllerOwnerView, so the text
	//	columns are relative to that view (so should use the COL_* enum to
	//	find the text.)

	//
	// This is just to connect the ViewObjects together for faster updating of history
	//
	QListViewItemParameterStore* lastParamStore = NULL;

	// for all views
	for(int i=0;i<historyLevel;i++)
	{
		QListView* this_ = m_PlayHistoryView[i];

		std::vector<QListViewItem*> parentStack;
		parentStack.reserve(20);
	    
		QListViewItem* currentItem = objectParameter;
		do
		{
			parentStack.push_back(currentItem);
			currentItem = currentItem->parent();

		} while(currentItem);

		std::vector<QListViewItem*>::iterator copyItemIT;
	    
		QListViewItem* currentParent = NULL;

		if(parentStack.size())
		{
			copyItemIT = parentStack.end();
			copyItemIT--;
			QListViewItem* currentCopyItem = *copyItemIT;
			currentParent = findChildView(this_, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE), ActionControllerOwnerView::COL_TYPE);
			if(currentParent == 0)
			{
				//
				// Seed the root (like: PED clase etc.)
				//
				currentParent = new QListViewItem(this_, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE));
				currentParent->setOpen(true);
			}
	        
			parentStack.pop_back();
		}

		if(parentStack.size())
		{
			copyItemIT = parentStack.end();
			copyItemIT--;
			QListViewItem* currentCopyItem = *copyItemIT;
			QListViewItem* itemFound = findChild(currentParent, currentCopyItem->text(ActionControllerOwnerView::COL_POINTER), ActionControllerOwnerView::COL_POINTER);
			if(itemFound == 0)
			{
				//
				// Make an Entity Object
				//
				currentParent = new EntityObjectInstanceQListViewItem(currentParent, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE));
				currentParent->setText(ActionControllerOwnerView::COL_POINTER, currentCopyItem->text(ActionControllerOwnerView::COL_POINTER));
				currentParent->setText(ActionControllerOwnerView::COL_MODEL, currentCopyItem->text(ActionControllerOwnerView::COL_MODEL));
				currentParent->setOpen(true);
			}
			else
			{
				currentParent = itemFound;
			}
	        
			parentStack.pop_back();
		}

		if(parentStack.size())
		{
			copyItemIT = parentStack.end();
			copyItemIT--;
			QListViewItem* currentCopyItem = *copyItemIT;
			QListViewItem* itemFound = findChild(currentParent, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE), ActionControllerOwnerView::COL_TYPE);
			if(itemFound == 0)
			{
				//
				// this makes the Object that receives acitoncontroller updates
				//
				QListViewItemParameterStore* paramStore = new QListViewItemParameterStore(currentParent, currentCopyItem->text(ActionControllerOwnerView::COL_TYPE));				

				if(lastParamStore)
				{
					lastParamStore->m_NextHistoryItem = paramStore;
				}

				lastParamStore = paramStore;
			}
	        
			parentStack.pop_back();
		}
	}
}

void MotionTreeView::RemoveFullPathCopy(QListViewItem* objectParameter)
{
    //
    // This will delete the entire branch if it's a vine otherwise just remove the
	// objectParameter that matches the input objectParameter
    //
	// for all views
	const int viewCount = m_PlayHistoryView.size();

	for(int i=0;i<viewCount;i++)
	{
		QListView* this_ = m_PlayHistoryView[i];

		QListViewItem* currentItem =  this_->findItem( objectParameter->text(ActionControllerOwnerView::COL_TYPE), ActionControllerOwnerView::COL_TYPE);
		while(currentItem)
		{
			QListViewItem* parent = currentItem->parent();
			delete currentItem;
			currentItem = NULL;

			if(parent && parent->childCount() == 0)
			{
				currentItem = parent;
			}
		}
	}
}

//---------------------------------------------------------------------------
// View for MotionTreeEditView
//---------------------------------------------------------------------------
MotionTreeEditView::MotionTreeEditView(QWidget* parent, const char* name, WFlags fl)
:
QListView(parent, name, fl)
{
    addColumn( tr( "Node" ) );
	addColumn( tr( "Weight" ) );
	addColumn( tr( "Direction" ) );
    addColumn( tr( "Time" ) );
    addColumn( tr( "Animation" ) );
    //header()->setClickEnabled( FALSE, header()->count() - 1 );
    setAcceptDrops( TRUE );
    setRootIsDecorated( FALSE );
    setDefaultRenameAction( QListView::Accept );

    connect( this, SIGNAL( rightButtonPressed(QListViewItem*,const QPoint&,int) ), 
             this, SLOT( rightButtonPressedHandler(QListViewItem*,const QPoint&,int) ) );
    connect( this, SIGNAL( clicked(QListViewItem*,const QPoint&,int) ), 
        this, SLOT( clickedHandler(QListViewItem*,const QPoint&,int) ) );

    //
    // All popup menus
    //
    m_ObjectClassPopupMenu = new QPopupMenu(this, "ObjectClassPopup");
    m_ObjectClassPopupMenu->insertItem( "CreateNode", this, SLOT(createnode()));
 
	setSorting(-1);

}

MotionTreeEditView::~MotionTreeEditView()
{

}

void MotionTreeEditView::rightButtonPressedHandler 
( 
    QListViewItem *item, 
    const QPoint& coords, 
    int column
)
{
   if(item)
   {
       m_ObjectClassPopupMenu->move( coords );
       m_ObjectClassPopupMenu->show();
   }
}

//---------------------------------------------------------------------------
// create node
//---------------------------------------------------------------------------
void MotionTreeEditView::createnode(void)
{
/*
    m_ObjectClassPopupMenu->hide();
    QListViewItem* selected = selectedItem();
    if(selected)
    {
		QListViewItemMTNode* animNode = QListViewItemMTNode::CreateNode( selected, playinfo, nodeTypeBuff, childCount);
		animNode->setOpen(true);		
    }
*/
}

//---------------------------------------------------------------------------
// On mouse click for the Owner View
//---------------------------------------------------------------------------
void MotionTreeEditView::clickedHandler 
( 
 QListViewItem *item, 
 const QPoint& coords, 
 int column
 )
{
    if(item)
    {
        QCheckListItem* objectParameter = dynamic_cast<QCheckListItem*>(item);
        if(objectParameter)
        {
            int isChecked = objectParameter->isOn()?1:0;

            QListViewItem* ObjectInstance = objectParameter->parent();
            if(ObjectInstance)
            {
                QListViewItem* ObjectClass = ObjectInstance->parent();
                if(ObjectClass)
                {
  
                }
            }
        }
    }
}

extern void SyncWithViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);
extern void SaveViewProperiesUtil(QWidget* widget, ViewProperties* viewProperties);

void MotionTreeEditView::SyncWithViewProperies(void)
{
    SyncWithViewProperiesUtil(this, m_ViewProperties);
}

void MotionTreeEditView::SaveViewProperies(void)
{
    SaveViewProperiesUtil(this, m_ViewProperties);
}


static void disconnectHistory(QListViewItem* parent)
{
	//
	// Disconnect the history pointers ;)
	//
	QListViewItem* myChild = parent->firstChild();
	while( myChild ) 
	{
		//
		// disconnect the history
		//
		QListViewItemParameterStore* paramStore = dynamic_cast<QListViewItemParameterStore*>(myChild);
		if(paramStore)
		{
			paramStore->m_NextHistoryItem = NULL;
		}
		
		//
		// Recurse children
		//
		disconnectHistory(myChild);

		//
		// next sibling
		//
		myChild = myChild->nextSibling();
	}
}

void MotionTreeView::SetHistoryLevel(int count)
{

	//
	// This clears ALL 
	//
	//m_PlayHistoryView.erase(m_PlayHistoryView.begin(), m_PlayHistoryView.end());
	//clear();

	int currentCount = m_PlayHistoryView.size();
	if(currentCount>count)
	{
		for(int i=count;i<currentCount;i++)
		{
			remomeChild(m_PlayHistoryView[i]);
			delete m_PlayHistoryView[i];
		}
		m_PlayHistoryView.erase(m_PlayHistoryView.begin()+count, m_PlayHistoryView.end());

		QListViewItem* root = m_PlayHistoryView[count-1]->firstChild();
		if(root)
		{
			disconnectHistory(root);
		}
	}

//    QHBoxLayout* gridLayout = new QHBoxLayout( this );
//	QSplitter *split = new QSplitter( Horizontal, this, "splitter" );

	currentCount = m_PlayHistoryView.size();
	for(int i=currentCount;i<count;i++)
	{

		QListView* m_PlayView = new QListView(viewport(),"slave");


		m_PlayHistoryView.push_back(m_PlayView);

		//
		// Column def could be Abstracted
		//
		m_PlayView->addColumn( tr( "Node" ) );
		m_PlayView->addColumn( tr( "Weight" ) );
		m_PlayView->addColumn( tr( "Direction" ) );
		m_PlayView->addColumn( tr( "Rate" ) );		
		m_PlayView->addColumn( tr( "Time" ) );
		m_PlayView->addColumn( tr( "Animation" ) );

		m_PlayView->setAcceptDrops( TRUE );
		m_PlayView->setRootIsDecorated( FALSE );
		m_PlayView->setDefaultRenameAction( QListView::Accept );

		m_PlayView->setColumnWidthMode( 0, QListView::Manual );
		m_PlayView->setColumnAlignment( 0, Qt::AlignJustify );
		static int s_defaultWidth = 250;
		m_PlayView->setColumnWidth( 0, s_defaultWidth );
		m_PlayView->setSorting(-1);

		m_PlayView->setMinimumSize( QSize( m_MinWidth, 0 ) );
		m_PlayView->setMaximumSize( QSize( 32767, 32767 ) );
		
		m_PlayView->show();
		addChild(m_PlayView);
	}
	
	m_scrollView->viewport()->adjustSize();
    m_FrameLayout->activate(); // this is KEY!!!! after the internals are drawn!!!!

}

int  MotionTreeView::GetHistoryLevel(void)
{
	return(m_PlayHistoryView.size());
}

void MotionTreeView::setminwidth(int w)
{
	ObjectParameterHistoryView::setminwidth(w);
	
	int count = m_PlayHistoryView.size();
	
	for(int i=0;i<count;i++)
	{
		m_PlayHistoryView[i]->setMinimumSize( QSize( m_MinWidth, 0 ) );
	}

	m_scrollView->viewport()->adjustSize();
	m_FrameLayout->activate(); // this is KEY!!!! after the internals are drawn!!!!
}

void MotionTreeView::ClearAllViews()
{
	int iNumPlayHistories = m_PlayHistoryView.size();
	
	for (int i = 0; i < iNumPlayHistories; i++)
	{
		m_PlayHistoryView[i]->clear();

	} // for i

} // MotionTreeView::ClearAllViews

bool MotionTreeView::DoesItemExist(QString& guidNumber, QString& controllerName)
{
	QListViewItem *pObjectInstance = m_PlayHistoryView[0]->findItem(guidNumber, ActionControllerOwnerView::COL_POINTER);
	if (pObjectInstance)
	{
		QListViewItem *pObjectParameter = findChild(pObjectInstance, controllerName, ActionControllerOwnerView::COL_TYPE);
		QListViewItemParameterStore *pObjectParameterStore = dynamic_cast<QListViewItemParameterStore*>(pObjectParameter);
		if (pObjectParameterStore)
		{
			return true;
		}
	}

	return false;

} // MotionTreeView::DoesItemExist
