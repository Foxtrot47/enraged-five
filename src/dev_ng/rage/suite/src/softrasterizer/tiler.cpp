// 
// softrasterizer/tiler.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "tiler.h"
#include "grcore/viewport.h"
#include "vectormath/legacyconvert.h"
#include "profile/profiler.h"
#include "system/cache.h"
#include "tsklib.h"
#include "boxoccluder.h"
#include "occludeModel.h"

#if __WIN32
//#include "softrasterizer/scan_frag.frag"
#include "scan.h"
#else
#include "system/codefrag_spu.h"
#endif
#if !__SPU
DECLARE_FRAG_INTERFACE(scan_frag)
#endif

#define USE_SQUARES 0

// TODO -- i'm not convinced these are necessary - try attaching a rag toggle to the following functions:
// - ClipTriangles
// - FullTransform2V
// - TriSetup
// - rstGenerateTris
// - rstFastTiler::AddHPTTri

// TODO -- use Vec::V4InvertPrecise
// e.g.
// RAST_INLINE ScalarV_Out InvertPrecise( ScalarV_In val ) { return ScalarV( Vec::V4InvertPrecise( val.GetIntrin128() ) ); }
// RAST_INLINE Vec2V_Out InvertPrecise( Vec2V_In val ) { return Vec2V( Vec::V4InvertPrecise( val.GetIntrin128() ) ); }
// RAST_INLINE Vec3V_Out InvertPrecise( Vec3V_In val ) { return Vec3V( Vec::V4InvertPrecise( val.GetIntrin128() ) ); }
// RAST_INLINE Vec4V_Out InvertPrecise( Vec4V_In val ) { return Vec4V( Vec::V4InvertPrecise( val.GetIntrin128() ) ); }

RAST_INLINE ScalarV_Out InvertExact( ScalarV_In val )
{
	ScalarV recip = Invert(val);
	// extra newton raphson iteration
	return AddScaled(recip, recip, SubtractScaled(ScalarV(V_ONE), val, recip));
}
RAST_INLINE Vec3V_Out InvertExact( Vec3V_In val )
{
	Vec3V recip = Invert(val);
	// extra newton-raphson iteration
	return AddScaled(recip, recip, SubtractScaled(Vec3V(V_ONE), val, recip));
}
RAST_INLINE Vec4V_Out InvertExact( Vec4V_In val )
{
	Vec4V recip = Invert(val);
	// extra newton-raphson iteration
	return AddScaled(recip, recip, SubtractScaled(Vec4V(V_ONE), val, recip));
}
RAST_INLINE Vec3V_Out InvertScaleExact( Vec3V_In val, ScalarV_In invScale )
{
	return val * InvertExact(  invScale);
}
RAST_INLINE ScalarV_Out InvertScaleExact( ScalarV_In val, ScalarV_In invScale )
{
	return val * InvertExact(  invScale);
}
RAST_INLINE Vec3V_Out InvertScaleExact( Vec3V_In val, Vec3V_In invScale )
{
	return val * InvertExact(  invScale);
}
RAST_INLINE Vec4V_Out InvertScaleExact( Vec4V_In val, Vec4V_In invScale )
{
	return val * InvertExact(  invScale);
}

RAST_INLINE Vec3V_Out InvertScaleExactTransform( Vec3V_In val, ScalarV_In invScale, ScalarV_In zscale )
{
	Vec3V a = InvertScaleExact( val,invScale);
	a.SetZ( val.GetZ()*zscale);
	return a;
}

/*
BoolV_Out BehindPoly( Vec3V_In cpos,  Vec3V_In v0,  Vec3V_In v1,  Vec3V_In v2)
{
	Vec3V normal = Cross(v1-v0,v0-v2);
	return Dot( normal,  v0-cpos ) <= ScalarV(V_ZERO);
}
bool IsInsideConvexVolume( Vec3V_In cameraPos, const Vec3V* verts, const u8* indices, int, int  numIndices)
{
	BoolV isInside(V_T_T_T_T);
	for (int i=0; i<numIndices; i+=3)
	{
		isInside = isInside & BehindPoly( cameraPos, verts[ indices[i+0]], verts[indices[i+1]], verts[ indices[i+2]]);
	}
	return isInside.Getb();
}
*/

namespace rage {

// matches enum { CLIP_PLANE_LEFT, CLIP_PLANE_RIGHT, CLIP_PLANE_TOP, CLIP_PLANE_BOTTOM, CLIP_PLANE_NEAR, CLIP_PLANE_FAR };

RAST_INLINE int BackFaceCullHomo( Vec4V_In v0, Vec4V_In v1, Vec4V_In v2)
{
	ScalarV a = v0.GetX();
	ScalarV b = v1.GetX();
	ScalarV c = v2.GetX();
	ScalarV d = v0.GetY();
	ScalarV e = v1.GetY();
	ScalarV f = v2.GetY();
	ScalarV g = v0.GetW();
	ScalarV h = v1.GetW();
	ScalarV i = v2.GetW();

	ScalarV determinant = a*e*i + b*f*g + c*d*h - c*e*g - b*d*i - a*f*h;
	/*

	// TODO : do a bunch of these at a pop.
	v0X*v1Y*v2W+ v1X*v2Y*v0W + v2X*v0Y*v1W  - ( v2X*v1Y*v0W - v1X*v0Y*v2W - v0X*v2Y*v1W)

	 v0(XWY)*v1(YXW)*v2(WYX) - v0(WYX)*v1(YXW)*v2*(XWY)
	Vec3V determinant=v0.Get<Vec::X,Vec::W,Vec::Y>()*v1.Get<Vec::Y,Vec::X,Vec::W>()*v2.Get<Vec::W,Vec::Y,Vec::X>()
	  - v0.Get<Vec::W,Vec::Y,Vec::X>()*v1.Get<Vec::Y,Vec::X,Vec::W>()*v2.Get<Vec::X,Vec::W,Vec::Y>();
	return IsGreaterThanOrEqualAll(Dot(determinant, Vec3V(V_ONE)), ScalarV(V_ZERO));
	*/
	
	return IsLessThanAll(determinant, ScalarV(V_ZERO));
}


template<bool checkZ>
RAST_INLINE int AddTri( Vec4V_In v0, Vec4V_In v1, Vec4V_In v2, Vec4V* out, int outcount )
{
	// change TO assert verify
	Assert( outcount+3 < MAX_GENERATED_VERTS);
	if ( outcount+3 >= MAX_GENERATED_VERTS )
		return outcount;

#if DEBUG_RASTERIZER
	if ( checkZ)
	{
		Assert( IsGreaterThanAll(Min(v0, v1, v2).GetZ(), ScalarV(V_ZERO)) );
	}
#endif
	out[outcount+0]=v0;
	out[outcount+1]=v1;
	out[outcount+2]=v2;
	return outcount +3;
}

RAST_INLINE int AddTri3( Vec3V_In v0, Vec3V_In v1, Vec3V_In v2, Vec3V* out, int outcount )
{
	out[outcount+0]=v0;
	out[outcount+1]=v1;
	out[outcount+2]=v2;
	Assert( outcount+3 < MAX_GENERATED_VERTS);
	return outcount +3;
}

template<int Plane>
int ClipTriangles( const Vec4V* verts, int amtVerts, Vec4V* out, Vec3V_In ClipMin, Vec3V_In ClipMax )
{
	int outcount=0;
	for (int i=0;i<amtVerts;i+=3)
	{

		Vec4V vi = verts[i+0];
		Vec4V vj = verts[i+1];
		Vec4V vk = verts[i+2];

		Vec3V codeDist;
		Vec3V triW = Vec3V(vi.GetW(),vj.GetW(),vk.GetW());
		switch( Plane ){
			case grcViewport::CLIP_PLANE_LEFT	:  codeDist= SubtractScaled(Vec3V(vi.GetX(),vj.GetX(),vk.GetX()), triW, ClipMin.GetX()); break;
			case grcViewport::CLIP_PLANE_RIGHT  :  codeDist=-SubtractScaled(Vec3V(vi.GetX(),vj.GetX(),vk.GetX()), triW, ClipMax.GetX()); break;
			case grcViewport::CLIP_PLANE_TOP	:  codeDist= SubtractScaled(Vec3V(vi.GetY(),vj.GetY(),vk.GetY()), triW, ClipMin.GetY()); break;
			case grcViewport::CLIP_PLANE_BOTTOM :  codeDist=-SubtractScaled(Vec3V(vi.GetY(),vj.GetY(),vk.GetY()), triW, ClipMax.GetY()); break;
			case grcViewport::CLIP_PLANE_NEAR	:  codeDist=               (Vec3V(vi.GetZ(),vj.GetZ(),vk.GetZ()) -Vec3V(ClipMin.GetZ())); break;
		}

		u32 clipMask = MoveSignMask( codeDist);
		if (Likely(clipMask == 7) ) // all outside
		{
			continue;
		}

		if (Likely( clipMask == 0)) // all inside this plane
		{
			outcount=AddTri<true>( vi, vj,vk, out, outcount);	
			continue;
		}

		Vec3V denom =  codeDist - codeDist.Get<Vec::Y, Vec::Z, Vec::X>();
		Vec3V bl = InvertScaleExact( codeDist, denom);

		Vec4V a = Lerp( bl.GetX(), vi,vj);
		Vec4V b = Lerp( bl.GetY(), vj,vk);
		Vec4V c = Lerp( bl.GetZ(), vk,vi);
		
		switch( clipMask )
		{
		case 1: // P outside, Q and R inside		
			outcount = AddTri<true>(vj,c,a, out, outcount);	
			outcount = AddTri<true>(vj,vk,c, out, outcount);	
			break;
		case 2:
			outcount = AddTri<true>(vi,a,b, out, outcount);	
			outcount = AddTri<true>(vi,b,vk, out, outcount);	
			break;
		case 3:
			outcount = AddTri<true>(vk, c,b, out, outcount);	
			break;
		case 4:
			outcount = AddTri<true>(vi,vj,b, out, outcount);	
			outcount = AddTri<true>(vi, b,c, out, outcount);	
			break;
		case 5:
			outcount = AddTri<true>(vj,b, a, out, outcount);	
			break;
		case 6:
			outcount = AddTri<true>(vi,a,c, out, outcount);	
			break;
		}
	}
	return outcount;
}

void rstFastTiler::SetClippingMemory( Vec4V* clip0, Vec4V* clip1 )
{
	m_clipInBuffer =clip0;
	m_clipOutBuffer = clip1;
}

int rstFastTiler::AddIndexedHTriListWithClipping(  const Vec4V* hverts, const u8* indices, int numIndices, int numVerts,
								PreTri2* outTris,
								int startIdx)
{
	Vec3V ClipMax( RAST_SCREEN_WIDTH+1.f, RAST_SCREEN_HEIGHT+1.f, 0.f);
	Vec3V ClipMinTest( -1.f, -1.f, 0.0001f);
	Vec3V ClipMin( -1.f, -1.f, 0.001f);
	int clipInCount=0;
	int bcnt = 0;
	u32 modelClipMask =0;
	
	Vec4V* clipIn = m_clipInBuffer;
	{
		ScalarV clipMaskBuffer[rstFastTiler::MaxIndexedVerts];
		
		Vec3V ClipMaxMask = Vec3V( Vec3VConstant< 1<<grcViewport::CLIP_PLANE_RIGHT, 1<<grcViewport::CLIP_PLANE_BOTTOM,0>());
		Vec3V ClipMinMask = Vec3V( Vec3VConstant< 1<<grcViewport::CLIP_PLANE_LEFT, 1<<grcViewport::CLIP_PLANE_TOP, 1<<grcViewport::CLIP_PLANE_NEAR>());
		ClipMaxMask.SetWZero();
		ClipMinMask.SetWZero();

		for (int i=0; i< numVerts; i++)
		{				
			Vec3V clipW(hverts[i].GetW(),hverts[i].GetW(), ScalarV(V_ONE) );
			Vec4V maskMax = Vec4V( hverts[i].GetXYZ() >= (ClipMax * hverts[i].GetW()) ) & Vec4V(ClipMaxMask);				
			Vec4V maskMin = Vec4V( hverts[i].GetXYZ() <= (ClipMinTest * clipW)) & Vec4V(ClipMinMask);				
			Vec4V mask = maskMax | maskMin;

			clipMaskBuffer[i]=mask.GetX() | mask.GetY() | mask.GetZ(); // TODO -- consider using si_orx on SPU
		}
		u32* clipMasks = (u32*)clipMaskBuffer;
		
		for (int i =0;i< numIndices; i+=3 )
		{
			int i0 = indices[i+0];
			int i1 = indices[i+1];
			int i2 = indices[i+2];
			Assert( i0<numVerts);
			Assert( i1<numVerts);
			Assert( i2<numVerts);
			u32 acceptMask = clipMasks[i0*4] | clipMasks[i1*4] | clipMasks[i2*4];
			u32 rejectMask = clipMasks[i0*4] & clipMasks[i1*4] & clipMasks[i2*4];
			
			if ( Likely( rejectMask != 0) )  // completely culled by a plane
				continue;

			if ( BackFaceCullHomo(  hverts[i0], hverts[i1], hverts[i2]) == 0)
				continue;

			if (acceptMask==0 )  // doesn't overlap near plane
			{
				bcnt += AddHPTTri( hverts[i0], hverts[i1], hverts[i2], outTris, bcnt, startIdx);
				continue;
			}
			clipInCount = AddTri<false>(hverts[i0],hverts[i1],hverts[i2], clipIn, clipInCount);	
			modelClipMask |= acceptMask;
		}
	}

	if (!clipInCount)  // all visible or clipped
	{
		return bcnt;
	}
	// now clip to each plane if require
	Vec4V* clipOut = m_clipOutBuffer;

	int clipOutCount = 0;
	if ( modelClipMask & ( 1<<grcViewport::CLIP_PLANE_NEAR))
	{
		clipOutCount= ClipTriangles<grcViewport::CLIP_PLANE_NEAR>( clipIn, clipInCount, clipOut, ClipMin,ClipMax);
		std::swap(clipIn, clipOut);
		std::swap(clipInCount, clipOutCount);
	}
	if ( modelClipMask & ( 1<<grcViewport::CLIP_PLANE_LEFT))
	{
		clipOutCount= ClipTriangles<grcViewport::CLIP_PLANE_LEFT>( clipIn, clipInCount, clipOut, ClipMin,ClipMax);
		std::swap(clipIn, clipOut);
		std::swap(clipInCount, clipOutCount);
	}
	if ( modelClipMask & ( 1<<grcViewport::CLIP_PLANE_RIGHT))
	{
		clipOutCount= ClipTriangles<grcViewport::CLIP_PLANE_RIGHT>( clipIn, clipInCount, clipOut, ClipMin,ClipMax);
		std::swap(clipIn, clipOut);
		std::swap(clipInCount, clipOutCount);
	}
	if ( modelClipMask & ( 1<<grcViewport::CLIP_PLANE_TOP))
	{
		clipOutCount= ClipTriangles<grcViewport::CLIP_PLANE_TOP>( clipIn, clipInCount, clipOut, ClipMin,ClipMax);
		std::swap(clipIn, clipOut);
		std::swap(clipInCount, clipOutCount);
	}
	if ( modelClipMask & ( 1<<grcViewport::CLIP_PLANE_BOTTOM))
	{
		clipOutCount= ClipTriangles<grcViewport::CLIP_PLANE_BOTTOM>( clipIn, clipInCount, clipOut, ClipMin,ClipMax);
		std::swap(clipIn, clipOut);
		std::swap(clipInCount, clipOutCount);
	}

	for ( int i = 0; i < clipInCount; i+=3)
	{
		bcnt += AddHPTTri( clipIn[i+0], clipIn[i+1], clipIn[i+2], outTris, bcnt, startIdx );
	}
	return bcnt;
}


#if __RASTERSTATS
namespace RasterStats
{
	EXT_PF_TIMER(AddTriList);
	EXT_PF_TIMER(Rasterize);
	EXT_PF_TIMER(Transform);
	EXT_PF_TIMER(GenEdges);
	EXT_PF_TIMER(GenerateStencil);
	EXT_PF_TIMER(GenerateWaterTris);
	EXT_PF_TIMER(StencilCompareAndGenerate);
}

using namespace RasterStats;
#endif // __RASTERSTAT

RAST_INLINE Vec4V_Out VecIntAdd( Vec4V_In a, Vec4V_In b )
{
#if __XENON
	return Vec4V( __vaddsws(a.GetIntrin128(), b.GetIntrin128())); // why does this require saturation only on 360?
#else
	return AddInt( a, b );
#endif
}

RAST_INLINE Vec3V_Out FullTransform2V( Mat44V_In mtx, Vec3V_In vin, ScalarV_In rescaleDepth)
{
	Vec4V vout = rstFullTransformHomo( mtx, vin);
	return InvertScaleExactTransform( vout.GetXYZ(), vout.GetW(), rescaleDepth );
}

// NOTE : 4 at a pop would be a lot quicker
template<int RoundDown>
RAST_INLINE int TriSetup( Vec3V_In v1, Vec3V_In v2, Vec3V_In v3, PreTri2& pTri, ScalarV_In sizeThreshold, int& issmall, Vec4V_InOut setFirstBit )
{
	Vec3V vx,vy,vz;
	Transpose3x3( vx,vy,vz, v1, v2, v3 );
	//Assert( IsGreaterThanAll( vz, Vec3V(V_ZERO)));
	vz = InvertExact(vz);

	Vec3V DX = vx - vx.Get<Vec::Y, Vec::Z, Vec::X>();
	Vec3V DY = vy - vy.Get<Vec::Y, Vec::Z, Vec::X>();
	Vec3V DZ = vz - vz.Get<Vec::Y, Vec::Z, Vec::X>();

	//Assert( IsGreaterThanOrEqualAll( vz, Vec3V(V_ZERO)));

	// back face cull
	ScalarV DYX = DY.GetX();
	ScalarV DYZ = DY.GetZ();
	ScalarV DXX = DX.GetX();
	ScalarV DXZ = DX.GetZ();
	ScalarV DZX = DZ.GetX();
	ScalarV DZZ = DZ.GetZ();

	ScalarV area = AddScaled( -DXZ * DYX, DXX, DYZ); // C

	ScalarV a = AddScaled( -DZX * DYZ, DYX, DZZ); // A
	ScalarV b = AddScaled( -DXX * DZZ, DZX, DXZ); // B

	/// const ScalarV inv_area = Invert(area);  //const float d = -(a * x1 + b* y1 + area * z1);

	a = -InvertScaleExact( a, area);//inv_area;
	b = -InvertScaleExact( b, area);//inv_area;
	ScalarV d = SubtractScaled(SubtractScaled(vz.GetX(), a, vx.GetX()), b, vy.GetX());  // D
	Vec3V abd(a,b,d);
	
	// Constant part of half-edge functions
	Vec3V C = SubtractScaled(DY * vx, DX, vy);
	Vec3V  minV = Min( v1,v2);
	minV = Min( minV,v3);

	if ( RoundDown)
		minV = RoundToNearestIntNegInf(minV);	// round to floor value so that we allow for centrein

	Vec3V  maxV = Max( v1,v2);
	maxV = Max( maxV,v3) ;

	Vec4V minMaxV = MergeXY( minV, maxV );

	// calculate size threshold
	Vec3V dims = maxV - minV;
	ScalarV maxDim = Max( dims.GetX(), dims.GetY());

	setFirstBit = SelectFT( sizeThreshold >= maxDim, Vec4VConstant<0x1, 0x1, 0x1, 0x1>() , Vec4VConstant<~0x0U, ~0x0U, ~0x0U, ~0x0U>());
	setFirstBit = setFirstBit & Vec4V(area > ScalarV(V_ZERO));

	issmall = IsGreaterThanOrEqualAll( sizeThreshold, maxDim);

	// Correct for fill convention  - disable for now
	//	Vec3V zero3(V_ZERO);
	//	VecBoolV IsTopEdge = ( DY < zero3 ) | ( (DY == zero3 )&(DX > zero3));
	//	C += Vec3V(V_ONE) & Vec3V(IsTopEdge);

	// need to remove any that are sub pixel size
	pTri.DX = Vec4V(DX, abd.GetY());
	pTri.DY = Vec4V(DY, -abd.GetX());
	pTri.C = Vec4V(C, abd.GetZ());
	pTri.minMax = minMaxV;
	int backFacing = IsLessThanOrEqualAll( area, ScalarV(V_ZERO));
	//Assert( backFacing || FPIsFinite(pTri.DX.GetWf()));
	//Assert( backFacing || FPIsFinite(pTri.DY.GetWf()));
	//Assert( backFacing || FPIsFinite(pTri.C.GetWf()));

#if DEBUG_RASTERIZER
	Assert( minMaxV.GetXf() <= minMaxV.GetYf());
	Assert( minMaxV.GetZf() <= minMaxV.GetWf());
#endif

	return 1 - backFacing;
}

void rstTransformVerts( const rstTransformToScreenSpace& transform, const Vec3V* verts, Vec3V* /*__restrict*/ postTransformVerts, int numVerts)
{
	PF_FUNC(Transform);
	ScalarV rescaleDepth =transform.GetRescaleDepth(); 
	
	for ( int i=0; i < numVerts; i++)
	{
		postTransformVerts[i]= FullTransform2V(  transform.m_comMtx, verts[i], rescaleDepth );
	}
}

void rstTransformVertsHomo( const rstTransformToScreenSpace& transform, const Vec3V* verts, Vec4V* __restrict postTransformVerts, int numVerts)
{
	PF_FUNC(Transform);

	Assert( IsTrueAll(IsNotNan(transform.m_comMtx.GetCol0())));
	Assert( IsTrueAll(IsNotNan(transform.m_comMtx.GetCol1())));
	Assert( IsTrueAll(IsNotNan(transform.m_comMtx.GetCol2())));
	Assert( IsTrueAll(IsNotNan(transform.m_comMtx.GetCol3())));

	for (int i = 0; i < numVerts; i++)
	{
		Assert( IsTrueXYZ(IsNotNan(verts[i])));
		postTransformVerts[i]= rstFullTransformHomo(  transform.m_comMtx, verts[i] );
		Assert( IsTrueAll(IsNotNan(postTransformVerts[i])));
	}
}

#define MAX_NUM_QUERY_VERTS 32
int rstGenerateTris(  Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth, PreTri2*	Tris, 
				ScalarV* minZ, Vec3V* verts, const u8* indices, int numVerts, int numIndices, bool flip, ScalarV_In zbias)
{
	Assert( numVerts < MAX_NUM_QUERY_VERTS);
	Vec3V scVerts[MAX_NUM_QUERY_VERTS];
	BoolV gmask(V_F_F_F_F);
	//BoolV infrontMask(V_T_T_T_T);
	ScalarV thresholdH(0.005f+0.001f );

	for (int i =0; i < numVerts; i++)  // TODO : unroll this boyo 
	{
		Vec4V vout = rstFullTransformHomo( worldToScreenMtx, verts[i]);
		ScalarV vw = vout.GetZ();
		gmask = gmask |   (vw <= thresholdH);	
		scVerts[i]=InvertScaleExactTransform( vout.GetXYZ(), vout.GetW(), rescaleDepth );
		scVerts[i].SetZ( scVerts[i].GetZ()- zbias);
		//infrontMask =  infrontMask & mask;		
	}


	bool clipped = false;
	if (IsTrue(gmask))
	{
		return -1; // set to cover full screen for now	
	}
		
#if __ASSERT
	for (int i =0; i < numVerts; i++)  // TODO : unroll this boyo 
	{			
		Assert( IsTrue(scVerts[i].GetZ() >= ScalarV(V_ZERO)));
	}
#endif
	// could transform AOS and then convert
	int amt=0;
	for (int i=0; i< numIndices/3; i++)
	{
		int issmall;
		Vec4V setFirstBit;
		int i0 = indices[i*3 +0];
		int i1 = indices[i*3 +1];
		int i2 = indices[i*3 +2];
		if ( flip){
			std::swap(i1,i2);
		}
		if (clipped)
		{
			i0 = i*3 + 0;
			i1 = i*3 + 1;
			i2 = i*3 + 2;		
		}
		minZ[amt] = Min( scVerts[i0], scVerts[i1], scVerts[i2] ).GetZ();
		int frontFace = TriSetup<false>( scVerts[i0], scVerts[i2], scVerts[i1],
							Tris[amt], ScalarV(V_ZERO), issmall, setFirstBit);
		amt += frontFace;
	}
	return amt;
}



RAST_INLINE void EdgeListSetup( Vec3V_In v1, Vec3V_In v2, Vec3V_In v3, Vec3V_In v4, EdgeList& edgeList )
{
	Vec4V vx,vy,vz;
	Transpose3x4to4x3( vx,vy,vz, v1, v2, v3, v4 );
	vz = InvertExact(vz);

	Vec3V DX = vx.GetXYZ() - vx.Get<Vec::Y, Vec::Z, Vec::W>();
	Vec3V DY = vy.GetXYZ() - vy.Get<Vec::Y, Vec::Z, Vec::W>();

	// NOTE : to fold in projected area Calc
	// doubleArea = DX * ( vy.GetXYZ()+vy.Get<Vec::Y, Vec::Z, Vec::W>());

	// Constant part of half-edge functions
	Vec3V C = SubtractScaled(DY * vx.GetXYZ(), DX, vy.GetXYZ());

	edgeList.DX = DX;
	edgeList.DY = DY;
	edgeList.C = C;

	
}

// Hull vertex table lookup for Exact Occlusion test from
// http://data.icg.tugraz.at/~dieter/publications/Schmalstieg_031.pdf
// First element in each row is the number of vertices
// Rest of the elements in the row is the index into the Box vertex
// Following is the order of vertices for the box

u8 g_hullVertexTable[43][7] = 
{
	//Number of side - Indices
	/*0*/	{0, 0, 0, 0, 0, 0, 0},	//inside
	/*1*/	{4, 0, 4, 7, 3, 0, 0},	//left
	/*2*/	{4, 1, 2, 6, 5, 0, 0},	//right
	/*3*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*4*/	{4, 0, 1, 5, 4, 0, 0},	//bottom
	/*5*/	{6, 0, 1, 5, 4, 7, 3},	//bottom, left
	/*6*/	{6, 0, 1, 2, 6, 5, 4},	//bottom, right
	/*7*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*8*/	{4, 2, 3, 7, 6, 0, 0},	//top
	/*9*/	{6, 4, 7, 6, 2, 3, 0},	//top, left
	/*10*/	{6, 2, 3, 7, 6, 5, 1},	//top, right
	/*11*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*12*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*13*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*14*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*15*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*16*/	{4, 0, 3, 2, 1, 0, 0},	//front
	/*17*/	{6, 0, 4, 7, 3, 2, 1},	//front, left
	/*18*/	{6, 0, 3, 2, 6, 5, 1},	//front, right
	/*19*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*20*/	{6, 0, 3, 2, 1, 5, 4},	//front, bottom
	/*21*/	{6, 2, 1, 5, 4, 7, 3},	//front, bottom, left
	/*22*/	{6, 0, 3, 2, 6, 5, 4},	//front, bottom, right
	/*23*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*24*/	{6, 0, 3, 7, 6, 2, 1},	//front, top
	/*25*/	{6, 0, 4, 7, 6, 2, 1},	//front, top, left
	/*26*/	{6, 0, 3, 7, 6, 5, 1},	//front, top, right
	/*27*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*28*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*29*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*30*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*31*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*32*/	{4, 4, 5, 6, 7, 0, 0},	//back
	/*33*/	{6, 4, 5, 6, 7, 3, 0},	//back, left
	/*34*/	{6, 1, 2, 6, 7, 4, 5},	//back, right
	/*35*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*36*/	{6, 0, 1, 5, 6, 7, 4},	//back, bottom
	/*37*/	{6, 0, 1, 5, 6, 7, 3},	//back, bottom, left
	/*38*/	{6, 0, 1, 2, 6, 7, 4},	//back, bottom, right
	/*39*/	{0, 0, 0, 0, 0, 0, 0},	//invalid
	/*40*/	{6, 2, 3, 7, 4, 5, 6},	//back, top
	/*41*/	{6, 0, 4, 5, 6, 2, 3},	//back, top, left
	/*42*/	{6, 1, 2, 3, 7, 4, 5},	//back, top, right
};

//Function to fetch row from hull vertex table
// http://data.icg.tugraz.at/~dieter/publications/Schmalstieg_031.pdf
__forceinline u8 rstAABBExactClassification(Vec3V_In eye, Vec3V_In bbMin, Vec3V_In bbMax, u8** indices)
{
	*indices = NULL;

	Vec4V bitMask = ( Vec4V(IsLessThan(eye, bbMin)) & Vec4VConstant<1,4,16,0>() ) |( Vec4V(IsGreaterThan(eye, bbMax)) & Vec4VConstant<2,8,32,0>() ); 

	ScalarV res = bitMask.GetX() | bitMask.GetY() | bitMask.GetZ(); 
	const int tableIdx = res.Geti();
	
	if(tableIdx > 42 || g_hullVertexTable[tableIdx][0] == 0)
	{
		return 0; //all cases above 42 is invalid
	}
	*indices = &(g_hullVertexTable[tableIdx][1]);
	return g_hullVertexTable[tableIdx][0];
}

__forceinline  u8 rstAABBExactClassificationOrtho(Vec3V_In direction, u8** indices)
{
	Vec3V threshold(ScalarV(0.001f));
	Vec4V bitMask = ( Vec4V(IsLessThan(direction, -threshold)) & Vec4VConstant<2,8,32,0>() ) |( Vec4V(IsGreaterThan(direction, threshold)) & Vec4VConstant<1,4,16,0>() ); 
	ScalarV res = bitMask.GetX() | bitMask.GetY() | bitMask.GetZ(); 
	const int tableIdx = res.Geti();

	if(tableIdx > 42 || g_hullVertexTable[tableIdx][0] == 0)
	{
		return 0; //all cases above 42 is invalid
	}
	*indices = &(g_hullVertexTable[tableIdx][1]);
	return g_hullVertexTable[tableIdx][0];
}

#if __BANK
ScalarV g_SimpleTestThresholdPixels = THRESHOLD_PIXELS_SIMPLE_TEST;
#else
const ScalarV g_SimpleTestThresholdPixels = THRESHOLD_PIXELS_SIMPLE_TEST;
#endif


#if __BANK
bool g_DebugRenderBoxExactHull = false;
void DrawBoxExactHullDebug(const Vec3V *verts, const u8* hullIndices, const u8 numVerts, Color32 color, const Mat34V* mtx = NULL) 
{	
	if ( mtx)
		grcWorldMtx(MAT34V_TO_MATRIX34(*mtx));
	else {
		Matrix34 mtx;
		mtx.Identity();
		grcWorldMtx(mtx);
	}

	grcBegin(drawLines,numVerts * 2);
	grcColor(color);

	for(int i=0; i<(numVerts - 1); i++)
	{
		grcVertex3f(verts[hullIndices[i]]);	grcVertex3f(verts[hullIndices[i+1]]);
	}

	grcVertex3f(verts[hullIndices[numVerts-1]]);	grcVertex3f(verts[hullIndices[0]]);

	grcEnd();
}
#endif

int rstGetBoxHull(  Vec3V_In eye, Vec3V_In bminI, Vec3V_In bmaxI, u8*& hullIndices,Vec3V aabbVerts[8])
{
	//Order should be this way in order to preserve the hull vertex lookup format
	ScalarV Eplision = ScalarVConstant<FLOAT_TO_INT(0.001f)>();//0.001f or  1 millimetre
	Vec3V bmin = bminI;
	Vec3V bmax = Max(bmaxI, bminI+Vec3V(Eplision));  // get rid of degenerate edges

	aabbVerts[0]=bmin;
	aabbVerts[1]=GetFromTwo<Vec::X2,Vec::Y1,Vec::Z1>(bmin,bmax);
	aabbVerts[2]=GetFromTwo<Vec::X2,Vec::Y2,Vec::Z1>(bmin,bmax);
	aabbVerts[3]=GetFromTwo<Vec::X1,Vec::Y2,Vec::Z1>(bmin,bmax);

	aabbVerts[4]=GetFromTwo<Vec::X1,Vec::Y1,Vec::Z2>(bmin,bmax);
	aabbVerts[5]=GetFromTwo<Vec::X2,Vec::Y1,Vec::Z2>(bmin,bmax);
	aabbVerts[6]=bmax;
	aabbVerts[7]=GetFromTwo<Vec::X1,Vec::Y2,Vec::Z2>(bmin,bmax);
	u8 numVerts = rstAABBExactClassification(eye, bmin, bmax, &hullIndices);

	if(hullIndices == NULL){
		return 0;
	}
	return numVerts;
}

// http://data.icg.tugraz.at/~dieter/publications/Schmalstieg_031.pdf
bool CheckProjectedAreaCloseToSquare( const Vec3V* dst, int num, Vec3V_In minV, Vec3V_In maxV, ScalarV_In threshold)
{
	// Calculates area of convex hull using simple contour integral approach
	ScalarV areaHull = (dst[num-1].GetX() - dst[0].GetX()) * (dst[num-1].GetY() + dst[0].GetY() );
	for ( int i=0; i<num-1; i++)
		areaHull += (dst[i].GetX() - dst[i+1].GetX()) * (dst[i].GetY() + dst[i+1].GetY());

	areaHull *=-ScalarV(V_HALF);  // need to flip area due to ordering difference with rasterization

	//Assert( areaHull.Getf() >= 0.f);				// gtaV data has some degenerate polys in it. But we aren't changing it now.

	ScalarV areaSquare = (maxV.GetX()-minV.GetX()) * ( maxV.GetY()-minV.GetY());
	static const ScalarV smallHullAreaThreshold = ScalarVConstant<FLOAT_TO_INT(4.f)>();
	static const ScalarV smallDimensionThreshold = ScalarV(V_ONE); //we could try ScalarV(V_HALF) to be more tigher)
	//Displayf("Area Square %f Hull %f", areaSquare.Getf(), areaHull.Getf());
	return IsLessThanAll( areaSquare-areaHull, threshold) != 0 || IsLessThanAll( areaHull,  smallHullAreaThreshold) || IsLessThanAll(maxV.GetX() - minV.GetX(), smallDimensionThreshold) || IsLessThanAll(maxV.GetY() - minV.GetY(), smallDimensionThreshold);
}

template<int Plane>
__forceinline ScalarV_Out fwPolyHomoDistanceFromClipPlane(Vec4V_In point, Vec2V_In screenRes)
{
	const Vec3V ClipMax( screenRes, ScalarV(V_ZERO));	
	static const Vec3V ClipMin( 0.0f, 0.0f, 0.001f);

	ScalarV dist = ScalarV(V_ZERO);
	switch(Plane)
	{
	case grcViewport::CLIP_PLANE_LEFT	:  
		dist= SubtractScaled(point.GetX(), point.GetW(), ClipMin.GetX()); 
		break;
	case grcViewport::CLIP_PLANE_RIGHT  :  
		dist=-SubtractScaled(point.GetX(), point.GetW(), ClipMax.GetX()); 
		break;
	case grcViewport::CLIP_PLANE_TOP	: 
		dist= SubtractScaled(point.GetY(), point.GetW(), ClipMin.GetY()); 
		break;
	case grcViewport::CLIP_PLANE_BOTTOM : 
		dist=-SubtractScaled(point.GetY(), point.GetW(), ClipMax.GetY()); 
		break;
	case grcViewport::CLIP_PLANE_NEAR	: 
		dist=               (point.GetZ() -ClipMin.GetZ()); 
		break;

	}
	return dist;
}

template<int Plane>
static int fwPolyHomoClip(Vec4V* dst, int dstCountMax, const Vec4V* src, int srcCount, Vec2V_In screenRes)
{
	int dstCount = 0;

	Vec4V   p0 = src[srcCount - 1];
	ScalarV d0 = fwPolyHomoDistanceFromClipPlane<Plane>(p0, screenRes);
	u32     g0 = IsGreaterThanOrEqualAll(d0, ScalarV(V_ZERO));

	for (int i = 0; i < srcCount; i++)
	{
		const Vec4V   p1 = src[i];
		const ScalarV d1 = fwPolyHomoDistanceFromClipPlane<Plane>(p1, screenRes);
		const u32     g1 = IsGreaterThanOrEqualAll(d1, ScalarV(V_ZERO));

		if (dstCount < dstCountMax && g1 != g0) { dst[dstCount++] = (p0*d1 - p1*d0)/(d1 - d0); }
		if (dstCount < dstCountMax && g1      ) { dst[dstCount++] = p1; }

		p0 = p1;
		d0 = d1;
		g0 = g1;
	}

	return dstCount;
}

bool rstTransformHullAndGetMinZ(Mat44V_In worldToScreenMtx, Vec2V_In screenRes, Vec3V* aabbVerts, const u8* hullIndices, int hullVertCount, ScalarV_In rescaleDepth, Vec3V_InOut scMinV, Vec3V_InOut scMaxV, ScalarV_InOut minZ, Vec3V* ssHullVerts, int& ssHullVertCount)
{
	scMinV = Vec3V(V_FLT_MAX);
	scMaxV = Vec3V(V_NEG_FLT_MAX);	
	if(hullVertCount == 0 || hullIndices == NULL)
	{
		//we're probably inside the bounding box
		minZ = ScalarV(V_ZERO);
		return true;
	}

	//settings the clipping boundary constants
	const Vec3V ClipMax( screenRes, ScalarV(V_ZERO));
	static const Vec3V ClipMinTest(V_ZERO);
	static const Vec3V ClipMin( V_ZERO);

	Vec3V ClipMaxMask = Vec3V( Vec3VConstant< BIT(grcViewport::CLIP_PLANE_RIGHT), BIT(grcViewport::CLIP_PLANE_BOTTOM), 0>());
	Vec3V ClipMinMask = Vec3V( Vec3VConstant< BIT(grcViewport::CLIP_PLANE_LEFT), BIT(grcViewport::CLIP_PLANE_TOP), BIT(grcViewport::CLIP_PLANE_NEAR)>());
	ClipMaxMask.SetWZero();
	ClipMinMask.SetWZero();

	ScalarV clipMaskBuffer[8];
	u32* clipMasks = (u32*)clipMaskBuffer;
	//these masks will only use the corresponding plane test
	static const u32 clipLRTBPlaneMask = BIT(grcViewport::CLIP_PLANE_LEFT) + BIT(grcViewport::CLIP_PLANE_RIGHT) + BIT(grcViewport::CLIP_PLANE_BOTTOM) + BIT(grcViewport::CLIP_PLANE_TOP);
	static const u32 clipNearPlaneMask = BIT(grcViewport::CLIP_PLANE_NEAR);
	Vec4V clipSpaceVerts[8];
	bool bPerformMinZClip = false;
	Vec4V minZVert = Vec4V(V_FLT_MAX);
	int minZVertIndex = -1;
	ssHullVertCount = hullVertCount;
	//convert all points to homogenous clip space
	//Check if the point closest to the camera is withing the viewport
	for(int i=0; i<8; i++)
	{
		clipSpaceVerts[i] = rstFullTransformHomo( worldToScreenMtx, aabbVerts[i]);

		Vec3V vScreenSpace = InvertScaleExactTransform( clipSpaceVerts[i].GetXYZ(), Abs(clipSpaceVerts[i].GetW()), rescaleDepth );
		scMinV = Min(scMinV, vScreenSpace);
		scMaxV = Max(scMaxV, vScreenSpace);

		//Mark which of frustum planes is the vert out of
		Vec3V clipW(clipSpaceVerts[i].GetW(),clipSpaceVerts[i].GetW(), ScalarV(V_ONE) );
		Vec4V maskMax = Vec4V( clipSpaceVerts[i].GetXYZ() >= (ClipMax * clipSpaceVerts[i].GetW()) ) & Vec4V(ClipMaxMask);				
		Vec4V maskMin = Vec4V( clipSpaceVerts[i].GetXYZ() <= (ClipMinTest * clipW)) & Vec4V(ClipMinMask);				
		Vec4V mask = maskMax | maskMin;
		clipMaskBuffer[i] = mask.GetX() | mask.GetY() | mask.GetZ();

		//Check out which vert has the least Z. Also keep track of if that vert is in or out of the LRTB frustum planes.
		// If there are 2 verts with the same least Z, keep track of the visible one (within LRTB frustum)
		if(IsGreaterThanAll(minZVert.GetZ(), clipSpaceVerts[i].GetZ()) || (IsEqualAll(minZVert.GetZ(),clipSpaceVerts[i].GetZ()) && bPerformMinZClip))
		{			
			bPerformMinZClip = ((clipMasks[i * 4] & clipLRTBPlaneMask) != 0);
			minZVertIndex = i;
			minZVert = clipSpaceVerts[i];
		}
	}


	const int kMaxPoints = 10;
	Vec4V buf[kMaxPoints];
	Vec4V clippedHullVerts[kMaxPoints];
	{
		u32 acceptNearMask = 0;
		u32 rejectNearMask = 0xffffffff;
		u32 rejectLRTBMask = 0xffffffff;
		//Let's clip the hull
		for(int i=0; i<hullVertCount; i++)
		{
			buf[i] = clipSpaceVerts[hullIndices[i]];
			acceptNearMask |= (clipMasks[hullIndices[i] * 4] & clipNearPlaneMask);
			rejectNearMask &= (clipMasks[hullIndices[i] * 4] & clipNearPlaneMask);

			rejectLRTBMask &= (clipMasks[hullIndices[i] * 4] & clipLRTBPlaneMask);
		}
		//reject if all hull verts are clipped against near plane (occurs when inside the bounding box)
		// still a valid hull
		if(rejectNearMask != 0)	{ ssHullVertCount = 0;	minZ = ScalarV(V_ZERO); return true; }
		//reject if all hull verts are out of LRTB frustum. This is an invalid hull
		if(rejectLRTBMask != 0) { ssHullVertCount = 0; minZ = ScalarV(V_FLT_MAX); return false;}

		if(acceptNearMask == 0)
		{
			// No need to clip if all verts pass the near plane test
			for(int i=0; i<ssHullVertCount; i++)
			{
				//Divide by w
				ssHullVerts[i] = InvertScaleExactTransform(buf[i].GetXYZ(), buf[i].GetW(), rescaleDepth);
			}

		}
		else
		{
			//clipping only with near plane because negative w will screw us up
			ssHullVertCount = fwPolyHomoClip<grcViewport::CLIP_PLANE_NEAR>(clippedHullVerts, kMaxPoints, buf, ssHullVertCount, screenRes); 	if (ssHullVertCount < 3)	{ ssHullVertCount = 0;	minZ = ScalarV(V_ZERO); return true; }
			//Disabling this assert as it comes on for degenerate cases which doesnt affect the game. In case if you see any weird popping issues, please enable this assert and see if we hit it. 
			//Assertf(ssHullVertCount <= 8, "Number of clipped hull verts is greater than 8. Count = %d", ssHullVertCount);
			for(int i=0; i<ssHullVertCount; i++)
			{
				//Divide by w
				ssHullVerts[i] = InvertScaleExactTransform(clippedHullVerts[i].GetXYZ(), clippedHullVerts[i].GetW(), rescaleDepth);
			}
		}
	}


	//Let's find the vert with the least Z
	//all these are ordered in anticlockwise order
	static const u32 facePoints[6][4] =
	{
		{0,	1, 2, 3},	//0
		{1, 5, 6, 2},	//1
		{3, 2, 6, 7},	//2
		{4, 0, 3, 7},	//3
		{7, 6, 5, 4},	//4
		{4, 5, 1, 0}	//5                                
	};

	if(!bPerformMinZClip)
	{
		//If so, lets return that Z
		Vec3V vScreenSpaceVert = InvertScaleExactTransform( minZVert.GetXYZ(), Abs(minZVert.GetW()), rescaleDepth );
		minZ = vScreenSpaceVert.GetZ();
		//valid hull
		return true;
	}
	
	scMinV = Vec3V(V_FLT_MAX);
	scMaxV = Vec3V(V_NEG_FLT_MAX);	
	minZ = ScalarV(V_FLT_MAX);
	Vec4V clippedFaceVerts[kMaxPoints];
	bool isAllFacesBackCulled = true;
	//If not lets clip against the LRTB frustum
	for(int i=0; i<6; i++)
	{	
		//Check if the face is facing the camera. Skip if it faces away
		if ( BackFaceCullHomo(  clipSpaceVerts[facePoints[i][0]], clipSpaceVerts[facePoints[i][2]], clipSpaceVerts[facePoints[i][1]]) == 0)
			continue;

		isAllFacesBackCulled = false;
		//Let's clip this face
		Vec4V faceVerts[4];
		u32 acceptMask = 0;
		u32 rejectMask = 0xffffffff;
		for(int j=0; j<4; j++)
		{
			faceVerts[j] = clipSpaceVerts[facePoints[i][j]];
			acceptMask |= (clipMasks[facePoints[i][j] * 4] & clipLRTBPlaneMask);
			rejectMask &= (clipMasks[facePoints[i][j] * 4] & clipLRTBPlaneMask);
		}
		//Reject if whole face is completely out of one of the LRTB planes
		if(rejectMask != 0)	{ continue;	}

		int numClippedFaceVerts = 4;
		//If plane is completely in the LRTB frustum then just do the test with the face verts without clipping
		if(acceptMask == 0)
		{
			for(int j=0; j<numClippedFaceVerts; j++)
			{
				Vec3V vScreenSpace = InvertScaleExactTransform( faceVerts[j].GetXYZ(), Abs(faceVerts[j].GetW()), rescaleDepth );
				scMinV = Min(scMinV, vScreenSpace);
				scMaxV = Max(scMaxV, vScreenSpace);
				minZ = Min(minZ, vScreenSpace.GetZ());
			}
		}
		else
		{
			//Looks like there's an overlap with atleast one of the plane.
			//Clip against LRTB planes
			numClippedFaceVerts = fwPolyHomoClip<grcViewport::CLIP_PLANE_LEFT>	(buf,					kMaxPoints,		faceVerts,			numClippedFaceVerts, screenRes);	if (numClippedFaceVerts < 3) { continue; }
			numClippedFaceVerts = fwPolyHomoClip<grcViewport::CLIP_PLANE_RIGHT>	(clippedFaceVerts,		kMaxPoints,		buf,				numClippedFaceVerts, screenRes);	if (numClippedFaceVerts < 3) { continue; }
			numClippedFaceVerts = fwPolyHomoClip<grcViewport::CLIP_PLANE_BOTTOM>(buf,					kMaxPoints,		clippedFaceVerts,	numClippedFaceVerts, screenRes);	if (numClippedFaceVerts < 3) { continue; }
			numClippedFaceVerts = fwPolyHomoClip<grcViewport::CLIP_PLANE_TOP>	(clippedFaceVerts,		kMaxPoints,		buf,				numClippedFaceVerts, screenRes);	if (numClippedFaceVerts < 3) { continue; }


			for(int j=0; j<numClippedFaceVerts; j++)
			{
				Vec3V vScreenSpace = InvertScaleExactTransform( clippedFaceVerts[j].GetXYZ(), Abs(clippedFaceVerts[j].GetW()), rescaleDepth );
				scMinV = Min(scMinV, vScreenSpace);
				scMaxV = Max(scMaxV, vScreenSpace);
				minZ = Min(minZ, vScreenSpace.GetZ());
			}

		}
	}

	//This means we're actually inside the bounding box. An edge case where we get a hull even though we're actually inside
	if(isAllFacesBackCulled)
	{
		//in this case the minZ should be set to 0, so it will be trivially accepted
		minZ = ScalarV(V_ZERO);
	}
	//it's a valid hull
	return true;

}


ScalarV_Out rstTestEdgeListCoverage(  Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth, bool isOrtho,
									const Vec4V* occlusionBuffer, Vec3V* verts, const u8* indices, ScalarV_In zbias,int numVerts, Vec4V_In minMaxBounds, ScalarV_In minZBuffer
									DEV_ONLY(, u32* trivialAcceptActiveFrustum, u32* trivialAcceptMinZ, u32* trivialAcceptVisiblePixel)
									BANK_ONLY(, ScalarV_In simpleTestThresholdPixels, bool useTrivialAcceptTest, bool useTrivialAcceptVisiblePixelTest, bool useMinZClipping)
							)
{
	Assert( numVerts>3);
	Assert( numVerts <= 6);
	const int kMaxPoints = 10;
	Vec3V scVerts[kMaxPoints];

	ScalarV thresholdH(0.005f+0.001f );
	BoolV gmask(V_F_F_F_F);
	
	Vec3V minV(V_FLT_MAX);
	Vec3V maxV(V_NEG_FLT_MAX);
	ScalarV minZ(V_FLT_MAX);
	static const Vec2V rastRes((float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT);
	if(!isOrtho BANK_ONLY( && useMinZClipping))
	{
		int scNumHullVerts = 0;
		bool isValidHull = rstTransformHullAndGetMinZ(worldToScreenMtx, rastRes, verts, indices, numVerts, rescaleDepth, minV, maxV, minZ, scVerts, scNumHullVerts);
		if(!isValidHull)
		{
			//if hull is outside LRTB frustum then it is occluded (or outside the viewport)
			return ScalarV(V_ZERO);
		}
		//if all the hull verts are clipped from the near plane, assume that its not occluded
		if(scNumHullVerts == 0)
		{
			DEV_ONLY(sysInterlockedIncrement(trivialAcceptMinZ);)
			return ScalarV(V_FLT_MAX);
		}
		numVerts = scNumHullVerts;

	}
	else
	{
		Vec3V scVertsBBX[8];

		for (int i =0; i < 8; i++)
		{		
			Vec4V vout = rstFullTransformHomo( worldToScreenMtx, verts[i]);
			ScalarV vw = vout.GetZ();
			gmask = gmask |   (vw <= thresholdH);
			scVertsBBX[i] = InvertScaleExactTransform( vout.GetXYZ(), vout.GetW(), rescaleDepth );	
			minV = Min(scVertsBBX[i], minV);
			maxV = Max(scVertsBBX[i], maxV);
		}
		for (int i =0; i < numVerts; i++)
		{
			scVerts[i] =scVertsBBX[indices[i]];
		}
		minZ = minV.GetZ();
	}

	//Do Trivial accept test
	BANK_ONLY(if(useTrivialAcceptTest))
	{
		if(rstTestAABBTrivialAccept(minV, maxV, minMaxBounds, minZBuffer, minZ
			DEV_ONLY(,trivialAcceptActiveFrustum, trivialAcceptMinZ)
			))
		{
			return ScalarV(V_ONE);
		}
	}

	
	// OR should we assume already tested with viewport culling?

	Vec4V minMaxV = MergeXY( minV, maxV );

	if ((isOrtho BANK_ONLY( || !useMinZClipping)) && IsTrue(gmask))
	{
		const ScalarV fullScreenArea = ScalarVFromF32(RAST_HIZ_WIDTH * RAST_HIZ_HEIGHT);
		return fullScreenArea;
	}

	// NOTE : might be faster to convert scVerts to SOA form here
	// to speed up CheckProjectedArea Calc
#if __BANK
	const ScalarV thresholdPixels = simpleTestThresholdPixels;
#else
	const ScalarV thresholdPixels = THRESHOLD_PIXELS_SIMPLE_TEST;
#endif
	//doing a max on FLT_MIN as we do an invert on this for the occlusion test
 	//this would avoid the divide-by-zero in rstScanQueryHiZLarge
	ScalarV minZQuery =Max( minZ - zbias, ScalarV(V_FLT_MIN));
	if ( CheckProjectedAreaCloseToSquare( scVerts, numVerts, minV,maxV, thresholdPixels)  )
	{
		return !IsFalseAll(rstScanQueryHiZLarge( occlusionBuffer, minV, maxV,minZQuery))?ScalarV(V_FLT_MAX):ScalarV(V_ZERO);
	}

	// TODO: could just fold this into the indices table for getting the shillotee edges
	//
	static const int edgeTable[6][10]={
		{0,1,2,0,1,2,0,1,2,0},
		{0,1,2,3,0,1,2,3,1,0},
		{0,1,2,3,4,0,1,2,3,4},
		{0,1,2,3,4,5,0,1,2,3},
		{0,1,2,3,4,5,6,0,3,4},
		{0,1,2,3,4,5,0,5,6,7}
	};
	Assertf((numVerts) <= 8, "Number of verts is greater than 8. numVert = %d", numVerts);
	const int* ei=edgeTable[Min(numVerts, 8)-3];

	EdgeList e0;
	EdgeList e1;
	EdgeList e2;

	EdgeListSetup(scVerts[ei[0]], scVerts[ei[1]], scVerts[ei[2]],scVerts[ei[3]], e0 );
	EdgeListSetup(scVerts[ei[3]], scVerts[ei[4]], scVerts[ei[5]],scVerts[ei[6]], e1 );
	if(numVerts <= 6)
	{
		return rstTest2EdgeListsHiZ( occlusionBuffer, e0,e1, minZQuery, minMaxV
			DEV_ONLY(, trivialAcceptVisiblePixel) BANK_ONLY(, useTrivialAcceptVisiblePixelTest));

	}
	else
	{
		EdgeListSetup(scVerts[ei[6]], scVerts[ei[7]], scVerts[ei[8]],scVerts[ei[9]], e2 );
		return rstTest3EdgeListsHiZ( occlusionBuffer, e0,e1,e2, minZQuery, minMaxV
			DEV_ONLY(, trivialAcceptVisiblePixel) BANK_ONLY(, useTrivialAcceptVisiblePixelTest));

	}


}

ScalarV_Out rstTestAABBExactEdgeList( bool isOrtho,  Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth,   
									const Vec4V* occlusionBuffer,Vec3V_In eye, Vec3V_In bminI, Vec3V_In bmaxI, ScalarV_In zbias, Vec4V_In minMaxBounds, ScalarV_In minZBuffer 
									DEV_ONLY(, u32* trivialAcceptActiveFrustum, u32* trivialAcceptMinZ, u32* trivialAcceptVisiblePixel)
									BANK_ONLY(, ScalarV_In simpleTestThresholdPixels, bool useTrivialAcceptTest, bool useTrivialAcceptVisiblePixelTest, bool useMinZClipping)
									 )
{

	const int NUM_AABB_VERTS = 8;
	Vec3V aabbVerts[NUM_AABB_VERTS];
	//Order should be this way in order to preserve the hull vertex lookup format
	ScalarV Eplision = ScalarVConstant<FLOAT_TO_INT(0.001f)>();//0.001f or  1 millimetre
	Vec3V bmin = bminI;
	Vec3V bmax = Max(bmaxI, bminI+Vec3V(Eplision));  // get rid of degenerate edges
	
	aabbVerts[0]=bmin;
	aabbVerts[1]=GetFromTwo<Vec::X2,Vec::Y1,Vec::Z1>(bmin,bmax);
	aabbVerts[2]=GetFromTwo<Vec::X2,Vec::Y2,Vec::Z1>(bmin,bmax);
	aabbVerts[3]=GetFromTwo<Vec::X1,Vec::Y2,Vec::Z1>(bmin,bmax);

	aabbVerts[4]=GetFromTwo<Vec::X1,Vec::Y1,Vec::Z2>(bmin,bmax);
	aabbVerts[5]=GetFromTwo<Vec::X2,Vec::Y1,Vec::Z2>(bmin,bmax);
	aabbVerts[6]=bmax;
	aabbVerts[7]=GetFromTwo<Vec::X1,Vec::Y2,Vec::Z2>(bmin,bmax);

	u8 *pHullIndices = NULL;
	u8 numVerts = isOrtho ?  rstAABBExactClassificationOrtho( eye, &pHullIndices) :  rstAABBExactClassification(eye, bmin, bmax, &pHullIndices);//

	
	if(numVerts == 0 || pHullIndices == NULL)
	{
		return ScalarV(V_ONE);
	}

#if __BANK && 0 //to be used only for rage sample
	if(g_DebugRenderBoxExactHull)
	{
		static const Color32 colHull(255,255,0);
		DrawBoxExactHullDebug(aabbVerts, pHullIndices, numVerts, colHull );
	}
#endif

	ScalarV res = rstTestEdgeListCoverage(  worldToScreenMtx,rescaleDepth, isOrtho,
		occlusionBuffer, aabbVerts, pHullIndices, zbias, numVerts, minMaxBounds, minZBuffer
		DEV_ONLY(,trivialAcceptActiveFrustum, trivialAcceptMinZ, trivialAcceptVisiblePixel)
		BANK_ONLY( , simpleTestThresholdPixels, useTrivialAcceptTest, useTrivialAcceptVisiblePixelTest, useMinZClipping)
		);
	return res;
}

ScalarV_Out rstTestOOBBExactEdgeList( Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth,   
	const Vec4V* occlusionBuffer,Vec3V_In eye, Mat34V_In OOBBToWorldMtx, Vec3V_In bminI, Vec3V_In bmaxI, ScalarV_In zbias, Vec4V_In minMaxBounds, ScalarV_In minZBuffer 
	DEV_ONLY(, u32* trivialAcceptActiveFrustum, u32* trivialAcceptMinZ, u32* trivialAcceptVisiblePixel)
	BANK_ONLY(, ScalarV_In simpleTestThresholdPixels, bool useTrivialAcceptTest, bool useTrivialAcceptVisiblePixelTest, bool useMinZClipping)
	)
{

	Vec3V aabbVerts[8];
	Vec3V eyeOOBBSpace =UnTransformOrtho( OOBBToWorldMtx, eye); // transform eye into OOBB space

	u8 *hullIndices = NULL;
	u8 numVerts = (u8)rstGetBoxHull( eyeOOBBSpace,bminI, bmaxI, hullIndices,aabbVerts);
	if(numVerts == 0 || hullIndices == NULL)
	{
		return ScalarV(V_ONE);
	}
#if __BANK && 0 
	static const Color32 colHull(255,255,0);
	DrawBoxExactHullDebug(aabbVerts, hullIndices, numVerts, colHull , &OOBBToWorldMtx );
#endif

	Mat44V OOBToScreenMtx;
	Mat44V	OOBBToWorldMtx44(Vec4V(OOBBToWorldMtx.GetCol0(), ScalarV(V_ZERO)),
								Vec4V(OOBBToWorldMtx.GetCol1(), ScalarV(V_ZERO)),
								Vec4V(OOBBToWorldMtx.GetCol2(), ScalarV(V_ZERO)),
								Vec4V(OOBBToWorldMtx.GetCol3(), ScalarV(V_ONE)));

	/*for(int i=0;i<8;i++){
		aabbVerts[i]=Transform( OOBBToWorldMtx, aabbVerts[i]);
	}*/

	Multiply( OOBToScreenMtx, worldToScreenMtx, OOBBToWorldMtx44);
	ScalarV res = rstTestEdgeListCoverage(  OOBToScreenMtx,rescaleDepth, false,
		occlusionBuffer, aabbVerts, hullIndices, zbias, numVerts, minMaxBounds, minZBuffer
		DEV_ONLY(,trivialAcceptActiveFrustum, trivialAcceptMinZ, trivialAcceptVisiblePixel)
		BANK_ONLY( , simpleTestThresholdPixels, useTrivialAcceptTest, useTrivialAcceptVisiblePixelTest, useMinZClipping)
		);
	return res;
}

int rstGenerateAABBTris(  Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth, PreTri2 Tris[RST_MAX_AABB_TRIS], ScalarV* minZ, 
								Vec3V_In bmin, Vec3V_In bmax, ScalarV_In zbias )
{
	const int NUM_AABB_VERTS = 8;
	const u8 AABBIndices[]  =
	{
		2,3,1, 0,2,1, // 2,3,1,0,
		1,3,7, 5,1,7, // 1,3,7,5,
		2,6,7, 3,2,7, // 2,6,7,3,
		6,2,0, 4,6,0, // 6,2,0,4,
		4,5,7, 6,4,7, // 4,5,7,6,
		0,1,5, 4,0,5, // 0,1,5,4,			
	};

	Vec3V aabbVerts[NUM_AABB_VERTS];
	aabbVerts[0]=bmin;
	aabbVerts[1]=GetFromTwo<Vec::X2,Vec::Y1,Vec::Z1>(bmin,bmax);
	aabbVerts[2]=GetFromTwo<Vec::X1,Vec::Y2,Vec::Z1>(bmin,bmax);
	aabbVerts[3]=GetFromTwo<Vec::X2,Vec::Y2,Vec::Z1>(bmin,bmax);

	aabbVerts[4]=GetFromTwo<Vec::X1,Vec::Y1,Vec::Z2>(bmin,bmax);
	aabbVerts[5]=GetFromTwo<Vec::X2,Vec::Y1,Vec::Z2>(bmin,bmax);
	aabbVerts[6]=GetFromTwo<Vec::X1,Vec::Y2,Vec::Z2>(bmin,bmax);
	aabbVerts[7]=bmax;

	int amt =  rstGenerateTris( worldToScreenMtx, rescaleDepth, Tris, minZ, 
						aabbVerts, AABBIndices, NUM_AABB_VERTS, NELEM(AABBIndices), true, zbias );
	Assert( amt <= RST_MAX_AABB_TRIS);
	return amt;
}

#if __BANK && !__SPU
void rstFastTiler::AddWidgets( bkBank& bk )
{
	bk.PushGroup("Tiler");
	bk.AddSlider("m_smallThreshold",&m_smallThreshold, 0, 16,1);
	bk.AddText("m_numBackFaced", &m_numBackFaced);
	bk.AddText("m_maxNumTris",&m_maxNumTris);
	bk.AddText("m_numSmall", &m_numSmall);
	bk.AddText("m_numTris", &m_numTris);
	bk.AddToggle("m_enableSmall", &m_enableSmall);
	bk.PushGroup("Tile Amounts");
	bk.AddText("tile 0", &m_cntTable[0]);
	bk.AddText("tile 1", &m_cntTable[1]);
	bk.AddText("tile 2", &m_cntTable[2]);
	bk.AddText("tile 3", &m_cntTable[3]);
	bk.AddText("tile 4", &m_cntTable[4]);
	bk.AddText("tile 5", &m_cntTable[5]);
	bk.AddText("tile 6", &m_cntTable[6]);
	bk.AddText("tile 7", &m_cntTable[7]);
	bk.PopGroup();
	bk.PopGroup();
}
#endif

RAST_INLINE void rstFastTiler::FindTiles( const PreTri2& p, Vec4V_In SetFirstBit, Vec4V_InOut c1, Vec4V_InOut c2
									RAST_TILES_12_ONLY(	, Vec4V_InOut c3 )		) const
{
	// do min max checks
	Vec4V minX = Vec4V(p.minMax.GetX());
	Vec4V minY = Vec4V(p.minMax.GetZ());
	Vec4V maxX = Vec4V(p.minMax.GetY());
	Vec4V maxY = Vec4V(p.minMax.GetW()); 
	// TODO - use u8's ?
	Vec4V colOverlap = Vec4V((minX < m_MaxTileX) & ( maxX > m_MinTileX));
	Vec4V rowOverlap = Vec4V((minY < m_MaxTileY) & ( maxY > m_MinTileY));

	Vec4V r1 = colOverlap & Vec4V(rowOverlap.GetX());
	Vec4V r2 = colOverlap & Vec4V(rowOverlap.GetY());

	Vec3V C = p.C.GetXYZ();
	Vec4V tDX = -p.DY;
	Vec3V tDY = p.DX.GetXYZ() * m_TileHeight;

	Vec3V EdgeReject = p.GetTrivalReject( m_TileWidth, m_TileHeight);

	Vec4V C0X = AddScaled( Vec4V(C.GetX()), Vec4V(tDX.GetX()), m_MinTileX);
	Vec4V C0Y = AddScaled( Vec4V(C.GetY()), Vec4V(tDX.GetY()), m_MinTileX);
	Vec4V C0Z = AddScaled( Vec4V(C.GetZ()), Vec4V(tDX.GetZ()), m_MinTileX);

	Vec3V EdgeReject0 = EdgeReject;
	Vec3V EdgeReject1 = EdgeReject - tDY;

	// 4x4 case 
	// Evaluate half-space functions store in 4x4 lists and repeat.

	Vec4V er1 = Vec4V(Vec4V(EdgeReject0.GetX()) < C0X);
	Vec4V er2 = Vec4V(Vec4V(EdgeReject1.GetX()) < C0X);

	er1 = er1 & Vec4V(Vec4V(EdgeReject0.GetY()) < C0Y);
	er2 = er2 & Vec4V(Vec4V(EdgeReject1.GetY()) < C0Y);

	er1 = er1 & Vec4V(Vec4V(EdgeReject0.GetZ()) < C0Z);
	er2 = er2 & Vec4V(Vec4V(EdgeReject1.GetZ()) < C0Z);

	er1 = er1 & r1;
	er2 = er2 & r2;

	c1 = er1 & SetFirstBit;
	c2 = er2 & SetFirstBit;

#if RAST_NUM_TILES == 12
	Vec4V r3 = colOverlap & Vec4V(rowOverlap.GetZ());
	Vec3V EdgeReject2 = EdgeReject1 - tDY;
	Vec4V er3 = Vec4V(Vec4V(EdgeReject2.GetX()) < C0X);
	er3 = er3 & Vec4V(Vec4V(EdgeReject2.GetY()) < C0Y);	
	er3 = er3 & Vec4V(Vec4V(EdgeReject2.GetZ()) < C0Z);
	er3 = er3 & r3;
	c3 = er3 & SetFirstBit;
#endif
}

RAST_INLINE void rstFastTiler::ApplyIndexChange( u16 index, Vec4V_In c1, Vec4V_In c2, Vec4V_In RAST_TILES_12_ONLY(c3), int issmall )
{
	int* cnt = m_cntTable + issmall *  RAST_NUM_TILES;
#if __ASSERT
	for (int i=0;i<RAST_NUM_TILES;i++)
		Assert( cnt[i]<MaxTris );
#endif
	Vec4V*  res = (Vec4V*  )cnt;
	m_tiles[0][cnt[0]] = index;
	m_tiles[1][cnt[1]] = index;
	m_tiles[2][cnt[2]] = index;
	m_tiles[3][cnt[3]] = index;
	m_tiles[4][cnt[4]] = index;
	m_tiles[5][cnt[5]] = index;
	m_tiles[6][cnt[6]] = index;
	m_tiles[7][cnt[7]] = index;

#if RAST_NUM_TILES == 12
	m_tiles[8][cnt[8]] = index;
	m_tiles[9][cnt[9]] = index;
	m_tiles[10][cnt[10]] = index;
	m_tiles[11][cnt[11]] = index;
#endif
	res[0] = VecIntAdd(res[0], c1);
	res[1] = VecIntAdd(res[1], c2);

	RAST_TILES_12_ONLY( res[2] = VecIntAdd(res[2], c3) );
#if __ASSERT
	for (int i=0;i<RAST_NUM_TILES;i++)
		Assert( cnt[i]<=MaxTris );
#endif
}

void rstFastTiler::DuplicateLastIndices()
{
	for (int i = 0; i < RAST_NUM_TILES; i++)
	{
		m_tiles[i][m_cntTable[0 * RAST_NUM_TILES + i]] = m_tiles[i][Max(m_cntTable[0 * RAST_NUM_TILES + i]-1,0)];	
		int smallIdx =m_cntTable[1 * RAST_NUM_TILES + i];
		m_tiles[i][smallIdx] = m_tiles[i][Min(smallIdx+1, MaxSmall )];	
	}
}

void rstFastTiler::Setup()
{
	BANK_ONLY(m_maxNumTris = 0;)
	m_numTris =0;
	CompileTimeAssert( MaxTris <=( 1<<16));
	
	m_smallThreshold = 4;
	Reset();

	m_TileWidth = ScalarVFromF32((float)RAST_TILE_WIDTH);
	m_TileHeight =  ScalarVFromF32((float)RAST_TILE_HEIGHT);
	m_MinTileX = Vec4VConstant<U32_ZERO, U32_ONE, U32_TWO,U32_THREE>() * m_TileWidth;
	m_MaxTileX = Vec4VConstant<U32_ONE, U32_TWO, U32_THREE,U32_FOUR>() * m_TileWidth;

	m_MinTileY = Vec4VConstant<U32_ZERO, U32_ONE, U32_TWO,U32_THREE>() * m_TileHeight;
	m_MaxTileY = Vec4VConstant<U32_ONE, U32_TWO, U32_THREE,U32_FOUR>() * m_TileHeight;

#if __BANK
	m_enableSmall = 1;
#endif
}

rstFastTiler::rstFastTiler()
{
	Setup();

	m_pixelOffset = Vec3V(V_ZERO);
#if __BANK
	m_stencilUserData = NULL;
#endif // __BANK
}

void rstFastTiler::GenerateTileMem()
{
	Reset();
}

void rstFastTiler::Reset()
{
#if __BANK
	m_maxNumTris = Max( m_numTris,m_maxNumTris );
#endif

	for (int i = 0; i < MaxNumTiles; i++)
	{
		m_cntTable[0 * MaxNumTiles + i]=0;
		m_cntTable[1 * MaxNumTiles + i] = MaxSmall;
	}
	
	m_numTris=0;
	m_sizeThreshold = ScalarVFromF32((float)m_smallThreshold);
#if __BANK
	m_numBackFaced = 0;
	m_numSmall = 0;

	m_enableSmallMask = m_enableSmall ? Vec4V(V_T_T_T_T) :  Vec4V(V_INT_1);
#endif
	m_clipInBuffer = 0;
	m_clipOutBuffer = 0;
}

ScanTileInfo rstFastTiler::GetInfo()
{
	ScanTileInfo t;
	t._indices = m_tiles[0];
	t.tris = m_tris;
	t.m_cnt = m_cntTable;
	t.pad=0;
	t.m_squares.reset();
	return t;
}

void rstFastTiler::SetViewport( const rstTransformToScreenSpace& screenTransform )
{
	m_transform= screenTransform;
}

int rstFastTiler::AddPTTri(Vec3V_In ptV0, Vec3V_In ptV1,Vec3V_In ptV2, PreTri2* __restrict outTris, int idx, int startIdx )
{
	PrefetchDC(outTris+idx+2);
	PreTri2 tri0;
	int issmall0;
	Vec4V setFirstBit0;
	int added = TriSetup<true>( ptV0, ptV1, ptV2,tri0, m_sizeThreshold, issmall0, setFirstBit0  );

#if __BANK
	issmall0 &= m_enableSmall;
	setFirstBit0 &= m_enableSmallMask;
#endif
	Vec4V c00,c01,c02;
	FindTiles( tri0, setFirstBit0, c00, c01 RAST_TILES_12_ONLY( ,c02 ) );
	
	added = added & ~IsEqualIntAll(c00|c01  RAST_TILES_12_ONLY(|c02 ), Vec4V(V_ZERO));

	if ( added && (idx + startIdx) < MaxTris)  // need check due to clipping adding tris
	{
		outTris[idx] = tri0;
		Assert( (idx + startIdx)< MaxTris );
		ApplyIndexChange( (u16)(idx + startIdx), c00, c01, c02, issmall0 );
	}

#if __BANK
	m_numBackFaced += (1-added);
	m_numSmall += issmall0&added;
#endif
		
	return added;
}

int rstFastTiler::AddHPTTri(Vec4V_In hptV0, Vec4V_In hptV1,Vec4V_In hptV2, PreTri2* __restrict outTris, int idx, int startIdx)
{
	Vec3V ptV0 = InvertScaleExactTransform( hptV0.GetXYZ(), hptV0.GetW(), m_transform.GetRescaleDepth() );
	Vec3V ptV1 = InvertScaleExactTransform( hptV1.GetXYZ(), hptV1.GetW(), m_transform.GetRescaleDepth() );
	Vec3V ptV2 = InvertScaleExactTransform( hptV2.GetXYZ(), hptV2.GetW(), m_transform.GetRescaleDepth() );

	ptV0 += m_pixelOffset;
	ptV1 += m_pixelOffset;
	ptV2 += m_pixelOffset;

#if STENCIL_WRITER_DEBUG
	if (m_stencilUserData)
	{
		const Vec3V debugPixelOffset(m_stencilUserData->debugPixelOffsetX, m_stencilUserData->debugPixelOffsetY, 0.0f);

		ptV0 += debugPixelOffset;
		ptV1 += debugPixelOffset;
		ptV2 += debugPixelOffset;
	}
#endif // STENCIL_WRITER_DEBUG

	return AddPTTri( ptV0, ptV1, ptV2, outTris, idx, startIdx );
}

int rstFastTiler::AddIndexedTriListWithClipping( 
	Vec3V*__restrict ptVerts,
	const Vec3V*	verts, 
	const u8*	Indices, 
	int  numIndices,
	int numVerts,
	PreTri2* outTris,
	int startIdx)
{
	// and add to tiler
	rstTransformVertsHomo( m_transform, verts, (Vec4V*) ptVerts, numVerts);

	return AddIndexedHTriListWithClipping(  (Vec4V*)ptVerts, Indices, numIndices, numVerts,
		outTris,startIdx );
}


	// TODO -- this code is copied from vectorutil.cpp
#if STENCIL_WRITER_DEBUG
static __forceinline ScalarV_Out fwPlaneDistanceTo(Vec4V_In plane, Vec3V_In p)
{
	//return Dot(plane, Vec4V(p, ScalarV(V_ONE)); // either way works ..
	return Dot(plane.GetXYZ(), p) + plane.GetW();
}

static int fwPolyClip(Vec3V* dst, int dstCountMax, const Vec3V* src, int srcCount, Vec4V_In plane)
{
	int dstCount = 0;

	Vec3V   p0 = src[srcCount - 1];
	ScalarV d0 = fwPlaneDistanceTo(plane, p0);
	u32     g0 = IsGreaterThanOrEqualAll(d0, ScalarV(V_ZERO));

	for (int i = 0; i < srcCount; i++)
	{
		const Vec3V   p1 = src[i];
		const ScalarV d1 = fwPlaneDistanceTo(plane, p1);
		const u32     g1 = IsGreaterThanOrEqualAll(d1, ScalarV(V_ZERO));

		if (dstCount < dstCountMax && g1 != g0) { dst[dstCount++] = (p0*d1 - p1*d0)/(d1 - d0); }
		if (dstCount < dstCountMax && g1      ) { dst[dstCount++] = p1; }

		p0 = p1;
		d0 = d1;
		g0 = g1;
	}

	return dstCount;
}

static int fwPolyClipToFrustum(Vec3V* dst, const Vec3V* src, int srcCount, Mat44V_In viewProj)
{
	const int kMaxPoints = 32;
	Vec3V buf[kMaxPoints];

	const Vec4V tmp0 = MergeXY(viewProj.a(), viewProj.c());
	const Vec4V tmp1 = MergeXY(viewProj.b(), viewProj.d());
	const Vec4V tmp2 = MergeZW(viewProj.a(), viewProj.c());
	const Vec4V tmp3 = MergeZW(viewProj.b(), viewProj.d());
	const Vec4V row0 = MergeXY(tmp0, tmp1);
	const Vec4V row1 = MergeZW(tmp0, tmp1);
//	const Vec4V row2 = MergeXY(tmp2, tmp3);
	const Vec4V row3 = MergeZW(tmp2, tmp3);

	srcCount = fwPolyClip(buf, kMaxPoints, src, srcCount, row3 + row0); if (srcCount < 3) { return 0; }
	srcCount = fwPolyClip(dst, kMaxPoints, buf, srcCount, row3 - row0); if (srcCount < 3) { return 0; }
	srcCount = fwPolyClip(buf, kMaxPoints, dst, srcCount, row3 + row1); if (srcCount < 3) { return 0; }
	srcCount = fwPolyClip(dst, kMaxPoints, buf, srcCount, row3 - row1); if (srcCount < 3) { return 0; }
//	srcCount = fwPolyClip(buf, kMaxPoints, dst, srcCount,        row2); if (srcCount < 3) { return 0; }
//	srcCount = fwPolyClip(dst, kMaxPoints, buf, srcCount, row3 - row2);

	return srcCount;
}
#endif // STENCIL_WRITER_DEBUG

void rstFastTiler::AddIndexedTriList(
	const Vec3V*	verts, 
	const u8*	Indices, 
	int  numIndices,
	int numVerts,
	bool requiresClipping)
{
	Vec3V  postTransformVerts[MaxIndexedVerts];
	int bcnt = 0;
	int startIdx =  m_numTris;

	if ( Unlikely( m_numTris >= (MaxTris-2) ))
	{
		// DND DLH - TODO
		// Considering increasing max tris as we are relatively frequently hitting this overflow
		// downtown
		//Displayf("OVerflow %i -> %i (%i )",numIndices/3, (MaxTris-m_numTris),m_numTris);
		return;
	}
	// first loop transforms verts
	Assertf(  numVerts <= MaxIndexedVerts, "Too many occlusion verts in model" );

	// DND DLH - move tri address access after the early out to prevent out of bounds
	// ASAN buffer addressing violation
	PreTri2* outTris = m_tris+m_numTris;

#if STENCIL_WRITER_DEBUG
	if (m_stencilUserData &&
		m_stencilUserData->debugSimpleClipper)
	{
		PF_FUNC(GenEdges);
		for (int t = 0; t< numIndices; t+=3)
		{
			Vec3V tri[] =
			{
				verts[Indices[t+0]],
				verts[Indices[t+1]],
				verts[Indices[t+2]],
			};

			Vec3V clipped[NELEM(tri) + 6];
			const int numClipped = fwPolyClipToFrustum(clipped, tri, NELEM(tri), m_transform.m_comMtx);

			rstTransformVerts( m_transform, clipped, postTransformVerts, numClipped);

			for (int i = 2; i < numClipped; i++)
			{
				bcnt += AddPTTri(postTransformVerts[0], postTransformVerts[i - 1], postTransformVerts[i], outTris, bcnt, startIdx);
			}
		}

		m_numTris += bcnt;
		return;
	}
#endif // STENCIL_WRITER_DEBUG

	if (Likely(!requiresClipping)) // all outside
	{
#if __ASSERT
		for (int i =0; i < numVerts; i++) 
		{
			ScalarV d = Dot(Vec4V(verts[i],ScalarV(V_ONE)), m_transform.m_cplane); 
			Assertf( IsGreaterThanOrEqualAll(  d, ScalarV(V_NEGONE) ), 
						"Occlusion Model requires clipping but says it doesnt ( dist %f )", d.Getf());
		}
#endif
		rstTransformVerts( m_transform, verts, postTransformVerts, numVerts);
		{
			PF_FUNC(GenEdges);
			for (int t = 0; t< numIndices; t+=3)
			{
				Vec3V r01= postTransformVerts[Indices[t+0]];
				Vec3V r02= postTransformVerts[Indices[t+1]];
				Vec3V r03= postTransformVerts[Indices[t+2]];

				bcnt += AddPTTri(r01, r02, r03, outTris, bcnt, startIdx);
			}
		}
		m_numTris += bcnt;
		return;
	}
	m_numTris += AddIndexedTriListWithClipping( postTransformVerts,verts, Indices, numIndices,numVerts,
											outTris, startIdx );
}

int rstFastTiler::AddTriListBatch( const Vec3V* __restrict tris, PreTri2* outTris, int amt, int startIdx, int /*maxT*/ )
{
	int endamt = (amt/3)*3;
	endamt = Min( endamt,  (MaxSmall)-m_numTris-3 );
	amt = Min( amt, (MaxSmall)-m_numTris-3 );
	int bcnt = 0;
	ScalarV rescaleDepth = m_transform.GetRescaleDepth();
	for (int t = 0; t< endamt; t+=3)
	{
		Vec3V r01= FullTransform2V(  m_transform.m_comMtx, tris[t*3+0], rescaleDepth );
		Vec3V r02= FullTransform2V(  m_transform.m_comMtx, tris[t*3+1], rescaleDepth );
		Vec3V r03= FullTransform2V(  m_transform.m_comMtx, tris[t*3+2], rescaleDepth );

		Vec3V r11= FullTransform2V(  m_transform.m_comMtx, tris[t*3+3], rescaleDepth );
		Vec3V r12= FullTransform2V(  m_transform.m_comMtx, tris[t*3+4], rescaleDepth );
		Vec3V r13= FullTransform2V(  m_transform.m_comMtx, tris[t*3+5], rescaleDepth );

		Vec3V r21= FullTransform2V(  m_transform.m_comMtx, tris[t*3+6], rescaleDepth );
		Vec3V r22= FullTransform2V(  m_transform.m_comMtx, tris[t*3+7], rescaleDepth );
		Vec3V r23= FullTransform2V(  m_transform.m_comMtx, tris[t*3+8], rescaleDepth );

		PreTri2 tri0,tri1,tri2;
		int issmall0,issmall1,issmall2;
		Vec4V setFirstBit0,setFirstBit1,setFirstBit2;
		int added0 = TriSetup<true>( r01, r02, r03 ,tri0, m_sizeThreshold, issmall0, setFirstBit0 );
		int added1 = TriSetup<true>( r11, r12, r13 ,tri1, m_sizeThreshold, issmall1, setFirstBit1 );
		int added2 = TriSetup<true>( r21, r22, r23 ,tri2, m_sizeThreshold, issmall2, setFirstBit2 );

		int addedresult = added0 + added1 + added2;
		if ( !addedresult)
		{
#if __BANK
			m_numBackFaced+=3;
#endif
			continue;
		}
		int idx0 = bcnt;
		int idx1 = bcnt + added0;
		int idx2 = idx1 + added1;
		bcnt = bcnt + addedresult;
#if __BANK
		issmall0 &= m_enableSmall;
		setFirstBit0 &= m_enableSmallMask;
		issmall1 &= m_enableSmall;
		setFirstBit1 &= m_enableSmallMask;
		issmall2 &= m_enableSmall;
		setFirstBit2 &= m_enableSmallMask;
#endif
		Vec4V c00,c01,c10, c11,c20,c21;
		Vec4V c02,c12,c22;
		FindTiles( tri0, setFirstBit0, c00, c01 RAST_TILES_12_ONLY(,c02 ) );
		FindTiles( tri1, setFirstBit1, c10, c11 RAST_TILES_12_ONLY(,c12 ));
		FindTiles( tri2, setFirstBit2, c20, c21 RAST_TILES_12_ONLY(,c22  ));

		outTris[idx0] = tri0;
		outTris[idx1] = tri1;
		outTris[idx2] = tri2;

		ApplyIndexChange( (u16)(idx0 + startIdx), c00, c01, c02,issmall0 );
		ApplyIndexChange( (u16)(idx1 + startIdx), c10, c11, c12, issmall1 );
		ApplyIndexChange( (u16)(idx2 + startIdx), c20, c21, c22, issmall2 );
#if __BANK
		m_numBackFaced += (1-added0);
		m_numSmall += issmall0&added0;
		m_numBackFaced += (1-added1);
		m_numSmall += issmall1&added1;
		m_numBackFaced += (1-added2);
		m_numSmall += issmall2&added2;
#endif
	}

	for (int t = endamt; t< amt; t++)
	{
		Vec3V r01= FullTransform2V(  m_transform.m_comMtx, tris[t*3+0], rescaleDepth );
		Vec3V r02= FullTransform2V(  m_transform.m_comMtx, tris[t*3+1], rescaleDepth );
		Vec3V r03= FullTransform2V(  m_transform.m_comMtx, tris[t*3+2], rescaleDepth );

		PreTri2 tri0;
		int issmall0;
		Vec4V setFirstBit0;
		int added0 = TriSetup<true>( r01, r02, r03 ,tri0, m_sizeThreshold, issmall0, setFirstBit0 );
		
		int idx0 = bcnt;
		bcnt = bcnt + added0;

#if __BANK
		issmall0 &= m_enableSmall;
		setFirstBit0 &= m_enableSmallMask;
#endif
		Vec4V c00,c01,c10, c11,c20,c21;
		Vec4V c02;
		FindTiles( tri0, setFirstBit0, c00, c01  RAST_TILES_12_ONLY(,c02 ) );
		outTris[idx0] = tri0;
		ApplyIndexChange( (u16)(idx0 + startIdx), c00, c01, c02, issmall0 );
		
#if __BANK
		m_numBackFaced += (1-added0);
		m_numSmall += issmall0&added0;
#endif
	}

	return bcnt;
}

enum Tags
{
	TAG_LOADVERTS_0 =1,
	TAG_WRITETRIS_0=2
};

void rstFastTiler::AddTriList( sysTaskBuffer& sm, const Vec3V* __restrict tris, int amt )
{
	// calculate batches
	const int BatchSize = 256;

	Reset();

	for ( int b = 0; b < amt; b += BatchSize)
	{
		ScratchMemStackPoint ck(sm);
		int batchAmt = Min( amt - b, BatchSize);
		const Vec3V* inTris = taskLoadBuffer( "inTris", sm,  tris + b*3, batchAmt*3, TAG_LOADVERTS_0 );
		taskWait( TAG_LOADVERTS_0);

		PreTri2* outTris = taskAllocBuffer<PreTri2,16>( sm, batchAmt);

		int amtTris = AddTriListBatch( inTris, outTris, batchAmt, m_numTris, MaxTris -1 - b);

		taskWriteBuffer( "outTris",outTris, m_tris + m_numTris, amtTris, TAG_WRITETRIS_0 );
		taskWait( TAG_WRITETRIS_0);

		m_numTris+=amtTris;
	}

	taskWait( TAG_WRITETRIS_0);
}
void rstFastTiler::AddTriList( const Vec3V* __restrict tris, int amt )
{
	amt = Min(amt, MaxTris - m_numTris);
	m_numTris += AddTriListBatch( tris, m_tris+m_numTris, amt, m_numTris,  MaxTris -1);
	
	// duplicate last indices
	DuplicateLastIndices();
}

void rstFastTiler::Assign( const PreTri2& p, u16 index, int issmall, Vec4V_In SetFirstBit )
{
	const VecBoolV allTrue(V_T_T_T_T);

	// Vec3V two3 = Vec3V(V_TWO);
	// Vec3V three3 = Vec3V(V_THREE);

	int* cnt = m_cntTable + issmall *RAST_NUM_TILES;

	Vec4V* __restrict res = (Vec4V*  __restrict)cnt;
	m_tiles[0][cnt[0]] = index;
	m_tiles[1][cnt[1]] = index;
	m_tiles[2][cnt[2]] = index;
	m_tiles[3][cnt[3]] = index;
	m_tiles[4][cnt[4]] = index;
	m_tiles[5][cnt[5]] = index;
	m_tiles[6][cnt[6]] = index;
	m_tiles[7][cnt[7]] = index;

	// do min max checks
	Vec4V minX = Vec4V(p.minMax.GetX());
	Vec4V minY = Vec4V(p.minMax.GetZ());
	Vec4V maxX = Vec4V(p.minMax.GetY());
	Vec4V maxY = Vec4V(p.minMax.GetW());

	Vec4V colOverlap = Vec4V((minX < m_MaxTileX) & ( maxX > m_MinTileX));
	Vec4V rowOverlap = Vec4V((minY < m_MaxTileY) & ( maxY > m_MinTileY));

	Vec4V r1 = colOverlap & Vec4V(rowOverlap.GetX());
	Vec4V r2 = colOverlap & Vec4V(rowOverlap.GetY());

	Vec3V C = p.C.GetXYZ();
	Vec4V tDX = -p.DY;
	Vec3V tDY = p.DX.GetXYZ() * m_TileHeight;
	Vec3V EdgeReject = p.GetTrivalReject( m_TileWidth, m_TileHeight);

	Vec4V C0X = AddScaled(Vec4V(C.GetX()), m_MinTileX, tDX.GetX());
	Vec4V C0Y = AddScaled(Vec4V(C.GetY()), m_MinTileX, tDX.GetY());
	Vec4V C0Z = AddScaled(Vec4V(C.GetZ()), m_MinTileX, tDX.GetZ());

	Vec3V EdgeReject0 = EdgeReject;
	Vec3V EdgeReject1 = EdgeReject - tDY;

	// 4x4 case 
	// Evaluate half-space functions store in 4x4 lists and repeat.

	Vec4V er1 = Vec4V(Vec4V(EdgeReject0.GetX()) < C0X);
	Vec4V er2 = Vec4V(Vec4V(EdgeReject1.GetX()) < C0X);

	er1 = er1 &	Vec4V(Vec4V(EdgeReject0.GetY()) < C0Y);
	er2 = er2 & Vec4V(Vec4V(EdgeReject1.GetY()) < C0Y);

	er1 = er1 & Vec4V(Vec4V(EdgeReject0.GetZ()) < C0Z);
	er2 = er2 & Vec4V(Vec4V(EdgeReject1.GetZ()) < C0Z);

	er1 = er1 & r1;
	er2 = er2 & r2;

	er1 = er1 & SetFirstBit;
	er2 = er2 & SetFirstBit;

	res[0] = VecIntAdd(res[0], er1);
	res[1] = VecIntAdd(res[1], er2);
}
	
#if !__SPU
void rstCreateScanTileTasks(  rstFastTiler** tiler,
	Vec4V* zbuffer,
	Vec4V* hiZ,
	int    hiZStride,
	bool useSimple,
	bool useSmall,
	bool drawBlock,
	DependantTaskList* dtask,
	const ShadowSquares& squares,
	ScanTileInfo stInfo[RAST_NUM_TILERS_ACTIVE],
	u32*		 nearZTiles,
	Vec4V*		 minMaxBounds)
{
	ScanUserData		ud;
	sysTaskParameters params;

	params.Scratch.Size = 32 * 1024;
	ud.useSimple = useSimple;
	ud.useSmall = useSmall;
	ud.drawBlock = drawBlock;
	Assign( ud.HiZStride, hiZStride);

	// Tiler Job could write this out
	for ( int k =0;k < RAST_NUM_TILERS_ACTIVE; k++)
	{
		stInfo[k]= tiler[k]->GetInfo();
		stInfo[k].m_squares=squares;
	}
	params.Input.Data = stInfo;
	params.Input.Size = sizeof(ScanTileInfo)*RAST_NUM_TILERS_ACTIVE;

	for (int i =RAST_NUM_TILES_HIGH-1; i >= 0; i--)
	{
		for (int j = 0; j < RAST_NUM_TILES_WIDE; j++)
		{
			Assign( ud.index ,i*RAST_NUM_TILES_WIDE + j);
			ud.locationx = (float)(RAST_TILE_WIDTH * j);
			ud.locationy = (float)(RAST_TILE_HEIGHT * i);
			ud.HiZTile = hiZ ? hiZ +  ( i *RAST_TILE_HEIGHT *hiZStride/2 ) + j * RAST_TILE_WIDTH/2 : 0;
			ud.nearZVal = nearZTiles + i *RAST_NUM_TILES_WIDE + j;
			ud.minMaxBounds = minMaxBounds + i * RAST_NUM_TILES_WIDE + j;
			if ( zbuffer != 0 ){
				Vec4V* zbufferTile = zbuffer + ( i *RAST_NUM_TILES_WIDE + j ) * RAST_TILE_WIDTH*RAST_TILE_HEIGHT;  // 64*72 pixel blocks
				params.Output.Data = zbufferTile;
				params.Output.Size = RAST_TILE_SIZE_BYTES;
			}

			dtask->Add( TASK_INTERFACE(scan), params, ud);
		}
	}
}

void rstCreateScanTileTasks( rstFastTiler** tiler,
	Vec4V* zbuffer,
	Vec4V* hiZ,
	int    hiZStride,
	bool useSimple,
	bool useSmall,
	bool drawBlock,
	sysDependency dtask[RAST_NUM_TILES],
	const ShadowSquares& squares,
	ScanTileInfo stInfo[RAST_NUM_TILERS_ACTIVE],
	u32* nearZTiles,
	Vec4V*		 minMaxBounds) 
{
	int c=0;

	ScanUserData		ud;
	sysTaskParameters params;

	sysDependency proto;
	tskSetScratch( proto, 32*1024);
	proto.Init( FRAG_SPU_CODE(scan_frag), 0);
	proto.m_Priority = sysDependency::kPriorityMed;
	ud.useSimple = useSimple;
	ud.useSmall=useSmall;
	ud.drawBlock=drawBlock;
	Assign( ud.HiZStride, hiZStride);

	// Tiler Job could write this out
	for ( int k =0;k < RAST_NUM_TILERS_ACTIVE; k++)
	{
		stInfo[k]= tiler[k]->GetInfo();
		stInfo[k].m_squares=squares;
	}
	tskSetInput( proto, 1,  stInfo, RAST_NUM_TILERS_ACTIVE);

	for (int i =0; i < RAST_NUM_TILES_HIGH; i++)
	{
		for (int j = 0; j < RAST_NUM_TILES_WIDE; j++)
		{
			Assign( ud.index ,c++);
			ud.locationx = (float)(RAST_TILE_WIDTH * j);
			ud.locationy = (float)(RAST_TILE_HEIGHT * i);
			ud.HiZTile = hiZ ? hiZ +  ( i *RAST_TILE_HEIGHT *hiZStride/2 ) + j * RAST_TILE_WIDTH/2 : 0;
			ud.nearZVal = nearZTiles + i *RAST_TILE_HEIGHT + j;
			ud.minMaxBounds = minMaxBounds + i * RAST_TILE_HEIGHT + j;

			dtask[i]=proto;
			tskSetUserData( dtask[i], ud);
			if ( zbuffer != 0 ){
				Vec4V* zbufferTile = zbuffer + ( i *RAST_NUM_TILES_WIDE + j ) * RAST_TILE_WIDTH*RAST_TILE_HEIGHT;  // 64*72 pixel blocks
				tskSetOutput( dtask[i],2, zbufferTile, RAST_TILE_SIZE_BYTES);
			}
		}
	}
}

#endif
#if !__SPU
rstTransformToScreenSpace::rstTransformToScreenSpace( const grcViewport* vp, bool isHiZ /*= false*/ NV_SUPPORT_ONLY(,bool useStereoFrustum) )
{
	m_cplane = vp->GetFrustumClipPlane( grcViewport::CLIP_PLANE_NEAR);
	float ClipEarlyThreshold =  isHiZ ? 0.005f : TRANSFORM_EXTRA_CLIP_THRESHOLD-2.f;
	m_cplane.SetWf( m_cplane.GetWf() - ClipEarlyThreshold);
	m_campos = vp->GetCameraPosition();
	int imgRatio = isHiZ ? 2: 1;
	grcViewport view(*vp);
	view.SetWindow( 0,0, RAST_SCREEN_WIDTH/imgRatio, RAST_SCREEN_HEIGHT/imgRatio);

	// The shear got re-calculated by now, and the original value is lost. Recovering...
	const Vector2 vShear = view.GetPerspectiveShear() + vp->GetPerspectiveShear();
	view.SetPerspectiveShear( vShear.x, vShear.y );

#if NV_SUPPORT
	if(useStereoFrustum)
	{
		m_comMtx = view.GetCullFullCompositeMtx();
	}
	else
#endif
	m_comMtx = view.GetFullCompositeMtx();

	m_scrExtents = Vec3V((float)(RAST_SCREEN_WIDTH/imgRatio), (float)(RAST_SCREEN_HEIGHT/imgRatio),1. );
	if( vp->IsPerspective())
		m_scrExtents.SetW( ScalarVFromF32( 1.0f/(view.GetFarClip()-  view.GetNearClip())));
	else
		m_scrExtents.SetW(ScalarV(V_ONE));

}
#endif

ScalarV_Out rstTestModelExact(  Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth,  const Vec4V* occlusionBuffer, Vec3V* verts, const u8* indices, int numVerts,
	int numIndices, ScalarV_In  /*smallThreshold*/ )
{
	// Get screen space area, 

	// half are backing but worst case clipping could nearly double amount of tris.
	PreTri2	Tris[RST_MAX_MODEL_TRIS];	
	ScalarV	MinZ[RST_MAX_MODEL_TRIS];	
	int numActiveTris= rstGenerateTris( worldToScreenMtx, rescaleDepth, Tris, MinZ, verts, indices, numVerts, numIndices, false, ScalarV(V_ZERO) );
	Assert( numActiveTris < RST_MAX_MODEL_TRIS);
	return rstTestHiZ( occlusionBuffer, Tris, MinZ, numActiveTris);
}

ScalarV_Out rstTestAABBExact(  Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth,  const Vec4V* occlusionBuffer,  Vec3V_In bmin, Vec3V_In bmax, ScalarV_In  zBias )
{
	// half are backing but worst case clipping could nearly double amount of tris.
	PreTri2	Tris[RST_MAX_AABB_TRIS];
	ScalarV	MinZ[RST_MAX_AABB_TRIS];	

	int numActiveTris= rstGenerateAABBTris( worldToScreenMtx, rescaleDepth, Tris, MinZ, bmin, bmax, zBias );
	return rstTestHiZ( occlusionBuffer, Tris, MinZ,numActiveTris);
}

} // namespace rage


#ifndef FRAG_TSKLIB_ONLY

u8* GenerateIndexBuffer( sysTaskBuffer& sm )
{
	int idx=0;

	int MaxNumIndices = (rstFastTiler::MaxIndexedVerts*6)/4;
	u8* indices = taskAllocBuffer<u8,128>(sm, MaxNumIndices);

	for (int vi=0;vi< rstFastTiler::MaxIndexedVerts; vi+=4)
	{
		indices[ idx+0] = (u8) vi;
		indices[ idx+1] = (u8)(vi+1);
		indices[ idx+2] = (u8)(vi+2);

		indices[ idx+3] = (u8)(vi+2);
		indices[ idx+4] = (u8)(vi+3);
		indices[ idx+5] = (u8) vi;
		idx+=6;
	}

	return indices;
}

void generatewatertris( rage::sysTaskParameters& p)
{
	static const int TAG_WD_GET = 10;
	static const int TAG_WD_WRITE = 11;
#if __RASTERSTATS
	PF_FUNC(GenerateWaterTris);
#endif

	ScratchMemStackPoint cm( p.Scratch);

	rstFastTiler* tiler = (rage::rstFastTiler*)p.Input.Data;
	// Don't reset tiler or set viewport - this was handled in boxoccluder

	GenerateWaterTrisUserData ud = tskGetUserData<GenerateWaterTrisUserData>(p);

	int numTotalVerts = taskGetU32( ud.numVertsEA, TAG_WD_GET);
	taskWait(TAG_WD_GET);

	const Vec3V* verts = taskLoadBuffer("verts",  p.Scratch, ud.verts, numTotalVerts,  TAG_WD_GET);
	taskWait(TAG_WD_GET);
	Vec4V* clipInBuffer= taskAllocBuffer<Vec4V,16>( p.Scratch, MAX_GENERATED_VERTS);
	Vec4V* clipOutBuffer= taskAllocBuffer<Vec4V,16>( p.Scratch, MAX_GENERATED_VERTS);
	tiler->SetClippingMemory( clipInBuffer, clipOutBuffer);

	u8* indices = GenerateIndexBuffer( p.Scratch );

	int numQuads = numTotalVerts / 4;
	int numQuadsPerJob = numQuads / RAST_NUM_TILERS_ACTIVE;
	int iStartIndex = 4 * numQuadsPerJob * ud.jobIndex;
	int iEndIndex = (ud.jobIndex == RAST_NUM_TILERS_ACTIVE-1) ? numTotalVerts : 4 * numQuadsPerJob * (ud.jobIndex+1);

	for (int i=iStartIndex;i<iEndIndex;i++){
		Assert( IsTrueXYZ(IsNotNan(verts[i])));
	}

	int stepSize=rstFastTiler::MaxIndexedVerts;
	// loop over every batch of 256 or lest verts and add with static index buffer
	for (int i=iStartIndex;i<iEndIndex; i+=stepSize)
	{
		int numVerts = Min( stepSize, iEndIndex - i);
		int numIndices  =(numVerts*6)/4;
		tiler->AddIndexedTriList( verts+i, indices, numIndices, numVerts, true);
	}

	// write out explicitly to output
	taskWrite( "Tiler Output",tiler, (rstFastTiler* )ud.tilerEA, TAG_WD_WRITE );
	taskWait( TAG_WD_WRITE ); 

	DependantTaskList::UpdateDependancies( p, TAG_WD_WRITE);
}

// todo cull out with bbox test
bool TestPtInPolyXY( ScalarV_In px, ScalarV_In py, const u8* indices, int numIndices, const Vec3V* verts){

	int i=0;

	ScalarV offset(V_ZERO);
	for (;i<numIndices-(3*3);i+=3)  // does 4 tris at a pop
	{
		// Compute vectors  
		Vec3V p0=verts[indices[i+0]]; Vec3V p1=verts[indices[i+1]];  Vec3V p2=verts[indices[i+2]]; i+=3;
		Vec3V p01=verts[indices[i+0]]; Vec3V p11=verts[indices[i+1]];  Vec3V p21=verts[indices[i+2]]; i+=3;
		Vec3V p02=verts[indices[i+0]]; Vec3V p12=verts[indices[i+1]];  Vec3V p22=verts[indices[i+2]]; i+=3;
		Vec3V p03=verts[indices[i+0]]; Vec3V p13=verts[indices[i+1]];  Vec3V p23=verts[indices[i+2]]; 

		Vec4V p0x,p0y,p0z,p1x,p1y,p1z,p2x,p2y,p2z;
		Transpose3x4to4x3(p0x, p0y, p0z, p0,p01,p02,p03);
		Transpose3x4to4x3(p1x, p1y, p1z, p1,p11,p12,p13);
		Transpose3x4to4x3(p2x, p2y, p2z, p2,p21,p22,p23);

		Vec4V invdA =Invert(-p1y*p2x + p0y*(-p1x + p2x) + p0x*(p1y - p2y) + p1x*p2y);

		Vec4V u = invdA*(p0y*p2x - p0x*p2y + (p2y - p0y)*px + (p0x - p2x)*py);
		Vec4V v = invdA*(p0x*p1y - p0y*p1x + (p0y - p1y)*px + (p1x - p0x)*py);

		// Check if point is in triangle
		VecBoolV hit = (u >= Vec4V(V_ZERO)) & (v >= Vec4V(V_ZERO)) & (u + v < Vec4V(V_ONE));
		if (Unlikely(!IsFalseAll(hit)))		
				return true;		
	}	
	
	for (;i<numIndices;i+=3)
	{
		// Compute vectors  
		int i0=indices[i]; int i1=indices[i+1]; int i2=indices[i+2];
		ScalarV p0x = verts[i0].GetX();
		ScalarV p0y = verts[i0].GetY();
		ScalarV p1x = verts[i1].GetX();
		ScalarV p1y = verts[i1].GetY();
		ScalarV p2x = verts[i2].GetX();
		ScalarV p2y = verts[i2].GetY();			

		ScalarV invdA =Invert(-p1y*p2x + p0y*(-p1x + p2x) + p0x*(p1y - p2y) + p1x*p2y);

		ScalarV u = invdA*(p0y*p2x - p0x*p2y + (p2y - p0y)*px + (p0x - p2x)*py);
		ScalarV v = invdA*(p0x*p1y - p0y*p1x + (p0y - p1y)*px + (p1x - p0x)*py);
		
		// Check if point is in triangle
		if (Unlikely(IsTrue((u >= ScalarV(V_ZERO)) & (v >= ScalarV(V_ZERO)) & (u + v < ScalarV(V_ONE)))))
				return true;
	}	
	return false;
}
void stencilwriter( rage::sysTaskParameters& p )
{
	static const int TAG_SW_LOAD =4;
	static const int TAG_SW_AMOUNT =2;

	using namespace rage;
#if __RASTERSTATS
	PF_FUNC(GenerateStencil);
#endif	
	ScratchMemStackPoint cm( p.Scratch);

	StencilWriterUserData ud = tskGetUserData<StencilWriterUserData>(p);
	bool writeTempZBuffer = ud.writeZBuffer;	 // rename to writeTempHiZBuffer

	rstFastTiler* tiler;
	int rdcnt=0;
	if ( !writeTempZBuffer)
		tiler= tskGetReadOnly<rage::rstFastTiler>(p, rdcnt++);
	else
		tiler = (rage::rstFastTiler*)p.Input.Data;

	const rstTransformToScreenSpace* clipAndTransform = tskGetReadOnly<rstTransformToScreenSpace>(p,rdcnt++);
	tiler->SetViewport(*clipAndTransform);
	tiler->Reset();

	
	tiler->m_pixelOffset = Vec3V(-Vec2V(V_QUARTER), ScalarV(V_ZERO));
#if __BANK
	tiler->m_stencilUserData = &ud;
#endif // __BANK
	
	int numTotalVerts = taskGetU32( ud.numVertsEA, TAG_SW_AMOUNT);
	
	Vec4V* HiZBuffer;	
	if (writeTempZBuffer)
	{
		HiZBuffer = (Vec4V*)p.Output.Data;
		sysMemSet( HiZBuffer, 0, sizeof(Vec4V)*RAST_HIZ_SIZE_VEC);
	}
	else{
		HiZBuffer =  (Vec4V*)p.Input.Data;
	}
	taskWait(TAG_SW_AMOUNT);

	bool cameraIsUnderWater=false;
	Vec3V cameraPosition = clipAndTransform->m_campos;
	{
		ScratchMemStackPoint cm( p.Scratch);

		const Vec3V* verts = taskLoadBuffer("verts",  p.Scratch, ud.verts, numTotalVerts,  TAG_SW_LOAD);
		taskWait(TAG_SW_LOAD);
		Vec4V* clipInBuffer= taskAllocBuffer<Vec4V,16>( p.Scratch, MAX_GENERATED_VERTS);
		Vec4V* clipOutBuffer= taskAllocBuffer<Vec4V,16>( p.Scratch, MAX_GENERATED_VERTS);
		tiler->SetClippingMemory( clipInBuffer, clipOutBuffer);

		u8* indices = GenerateIndexBuffer( p.Scratch );

		for (int i=0;i<numTotalVerts;i++){
			Assert( IsTrueXYZ(IsNotNan(verts[i])));
		}

		int stepSize=rstFastTiler::MaxIndexedVerts;
		// loop over every batch of 256 or lest verts and add with static index buffer
		for (int i=0;i< numTotalVerts; i+=stepSize)
		{
			tiler->Reset();
			tiler->SetClippingMemory( clipInBuffer, clipOutBuffer);
			int numVerts = Min( stepSize, numTotalVerts - i);
			int numIndices  =(numVerts*6)/4;
			tiler->AddIndexedTriList( verts+i, indices, numIndices, numVerts, true);

			if ( ud.clearToAllVisible && TestPtInPolyXY( cameraPosition.GetX(), cameraPosition.GetY(), indices, numIndices, verts+i ) )
				cameraIsUnderWater=true;
			
			// need to split this up further to save memory above
			rstStencilHiZ(HiZBuffer, tiler->m_tris, tiler->m_numTris, ud, writeTempZBuffer);
		}
	}

#if STENCIL_WRITER_DEBUG
	if (ud.debugEnableWaterStencil)
#endif // STENCIL_WRITER_DEBUG
	{
		// write out explicitly to output
		u32 waterOccluderCount = taskGetU32(ud.waterOccluderCountEA, TAG_SW_AMOUNT);
		taskWait(TAG_SW_AMOUNT);
				
		if( waterOccluderCount >0 )
		{
			ScratchMemStackPoint cm(p.Scratch);
			// double buffer the dma's of the model geometry	
			u8* buff[2]={0,0};	
#if __PS3
			buff[0] = taskAllocBuffer<u8,16>(p.Scratch, OccludeModel::GetMaxModelSize());
			buff[1] = taskAllocBuffer<u8,16>(p.Scratch, OccludeModel::GetMaxModelSize());
#endif


			Vec3V* verts = taskAllocBuffer<Vec3V,16>(p.Scratch, rstFastTiler::MaxIndexedVerts);
			Vec4V* clipInBuffer = taskAllocBuffer<Vec4V,16>(p.Scratch, MAX_GENERATED_VERTS);
			Vec4V* clipOutBuffer = taskAllocBuffer<Vec4V,16>(p.Scratch, MAX_GENERATED_VERTS);

			ScalarV bitPrecision = ScalarVFromF32(p.UserData[0].asFloat);
			u8* isClipped = taskLoadBuffer( "resWaterClipped", p.Scratch, ud.outputWaterClipped, TSK_ROUNDUP16(waterOccluderCount), TAG_SW_LOAD );
			OccludeModel* occList = taskLoadBuffer( "resWaterOccluders",p.Scratch, ud.outputWaterOccluders, waterOccluderCount, TAG_SW_LOAD );
			taskWait(TAG_SW_LOAD);
			int bId=0;

			occList[0].LoadGeometryLocally(buff[bId],TAG_SW_LOAD );
			for (u32 i=0; i<waterOccluderCount; i++)
			{
				taskWait(TAG_SW_LOAD);
				if ( Likely(i!=(waterOccluderCount-1)))
				{
					bId = !bId;
					occList[i+1].LoadGeometryLocally(buff[bId],TAG_SW_LOAD );
				}	

				Assert( !occList[i].IsEmpty() );
				Assert( occList[i].AreFlagsSet(OccludeModel::FLAG_WATER_ONLY));
				if ( occList[i].IsEmpty())
					continue;
					
				XENON_ONLY(Assert(occList[i].IsValid()));
				occList[i].UnPackVertices(verts, rstFastTiler::MaxIndexedVerts, bitPrecision);

				tiler->Reset();
				tiler->SetClippingMemory(clipInBuffer, clipOutBuffer);
				if (tiler->IsFull(occList[i].GetNumIndices()))
					continue;
				
				VecBoolV ptInXY = (cameraPosition> occList[i].GetMin() ) &( cameraPosition< occList[i].GetMax()) |VecBoolV(V_F_F_T_T);
				
				if ( ud.clearToAllVisible && IsTrueAll(ptInXY) && TestPtInPolyXY( cameraPosition.GetX(), cameraPosition.GetY(), occList[i].GetIndices(), occList[i].GetNumIndices(), verts) )
					cameraIsUnderWater=true;
				
				tiler->AddIndexedTriList(verts, occList[i].GetIndices(), occList[i].GetNumIndices(), occList[i].GetNumVerts(), isClipped[i]!=0);

				// need to split this up further to save memory above
				rstStencilHiZ(HiZBuffer, tiler->m_tris, tiler->m_numTris, ud, writeTempZBuffer);
			}
		}	

	}
	if ( cameraIsUnderWater && writeTempZBuffer ){  // clear to all visible
		// clear the zBuffer
		Vec4V nearVal(ScalarV(1.f/0.00001f));
		for(int i=0;i<RAST_HIZ_SIZE_VEC;i++)
			HiZBuffer[i]=nearVal;
	}
	// now render it
	if ( !writeTempZBuffer)
	{
		Vec4V* stencilBuffer = (Vec4V*)p.Output.Data;
		rstSetStencilBits( HiZBuffer, stencilBuffer );
	}
	// else output is already setup
	DependantTaskList::UpdateDependancies( p, TAG_SW_LOAD);
}
void stencilcompareandgenerate( rage::sysTaskParameters& p  )
{
#if __RASTERSTATS
	PF_FUNC(StencilCompareAndGenerate);
#endif
	size_t amt;
	Vec4V* hiZBuffer = tskGetInput<Vec4V>(p, amt);
	Assert( amt == RAST_HIZ_SIZE_VEC);

	Vec4V* stencil = (Vec4V*) p.Output.Data;
	const Vec4V* tempHiZBuffer = tskGetReadOnlyBuffer<Vec4V>(p,0,RAST_HIZ_SIZE_VEC);

	rstCompareToStencilBits( hiZBuffer, tempHiZBuffer, stencil);
}

void triangleocclusiontester( rage::sysTaskParameters& p )
{
	using namespace rage;
#if __RASTERSTATS
//	PF_FUNC(OccludeTriangles);
#endif	

	const rstTransformToScreenSpace* transform = (rstTransformToScreenSpace*) p.ReadOnly[0].Data;
	TriangleOcclusionUserData ud = tskGetUserData<TriangleOcclusionUserData>(p);

	Vec4V* hiZBuffer = (Vec4V*)p.Input.Data;
	const Vec3V* verts = (Vec3V*) p.ReadOnly[1].Data;
	const u16* indices = (u16*) p.ReadOnly[2].Data;
	const Vec3V* normals = (Vec3V*) p.ReadOnly[3].Data;

	// calculate points shifted along normal
	Vec3V* postTransformShiftedVerts = taskAllocBuffer<Vec3V,16>(p.Scratch, ud.numVerts);
	{
		ScratchMemStackPoint ckpoint(p.Scratch);
		Vec3V* shiftedVerts = taskAllocBuffer<Vec3V,16>(p.Scratch, ud.numVerts);

		ScalarV shiftAmt= ScalarVFromF32( ud.shiftAmt);
		for (int i = 0; i < ud.numVerts; i++)
		{
			shiftedVerts[i]=AddScaled( verts[i], normals[i], shiftAmt);
		}
		rstTransformVerts( *transform, shiftedVerts, postTransformShiftedVerts, ud.numVerts);
	}
	Vec3V* postTransformVerts = taskAllocBuffer<Vec3V,16>(p.Scratch, ud.numVerts);	
	rstTransformVerts( *transform, postTransformVerts, postTransformShiftedVerts, ud.numVerts);

	u16* rindices =(u16*)p.Output.Data;
	int cnt=0;
	rstTransformVerts( *transform, verts, postTransformVerts, ud.numVerts);
	for (int t = 0; t< ud.numIndices; t+=3)
	{
		Vec3V r01= postTransformVerts[indices[t+0]];
		Vec3V r02= postTransformVerts[indices[t+1]];
		Vec3V r03= postTransformVerts[indices[t+2]];

		Vec3V rs01= postTransformShiftedVerts[indices[t+0]];
		Vec3V rs02= postTransformShiftedVerts[indices[t+1]];
		Vec3V rs03= postTransformShiftedVerts[indices[t+2]];

		Vec3V min01 = Min( r01, rs01);
		Vec3V min02 = Min( r02, rs02);
		Vec3V min03 = Min( r03, rs03);

		Vec3V max01 = Max( r01, rs01);
		Vec3V max02 = Max( r02, rs02);
		Vec3V max03 = Max( r03, rs03);

		Vec3V scmin = Min( min01,min02);
		Vec3V scmax = Max( max01,max02);
		scmin = Min( scmin, min03);
		scmax = Max( scmax, max03);

		if ( IsTrueAll(rstScanQueryHiZLarge( hiZBuffer, scmin, scmax,scmin.GetZ())) )
		{
			rindices[cnt+0]=indices[t+0];
			rindices[cnt+1]=indices[t+1];
			rindices[cnt+2]=indices[t+2];
			cnt+=3;
		}
	}
	taskSetU32( ud.finalCntEA,cnt, 2 ); 
	DependantTaskList::UpdateDependancies( p, 1);
}

#endif // FRAG_TSKLIB_ONLY
