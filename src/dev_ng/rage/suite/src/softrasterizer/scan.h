// 
// softrasterizer/scan.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SOFTRASTERIZER_SCAN_H 
#define SOFTRASTERIZER_SCAN_H 

// Tasks should only depend on taskheader.h, not task.h (which is the dispatching system)

#ifndef FRAG_TSKLIB_ONLY
#include "../../../../rage/base/src/system/taskheader.h"

DECLARE_TASK_INTERFACE(scan);
//rstScanTileWrapper);
#include "system/task.h"
#endif

#include "atl/array.h"
#include "vectormath/classes.h"

#include "softrasterizer/scansetup.h"
#include "softrasterizer/tsklib.h"

#define DEBUG_RASTERIZER 0

namespace rage 
{


struct PreTri2
{
	Vec4V C;
	Vec4V DX;
	Vec4V DY;
	Vec4V minMax;

	PreTri2(){}
	PreTri2( Vec3V_In iC, Vec3V_In iDX, Vec3V_In iDY):C(iC),DX(iDX),DY(iDY)	{}

	//RAST_INLINE Vec3V_Out GetTrivalReject() const
	//{
	//	Vec3V tRejectCX = SelectFT( -DY.GetXYZ() > Vec3V(V_ZERO), Vec3V(V_ZERO), Vec3V(V_ONE) );
	//	Vec3V tRejectCY = SelectFT( -DX.GetXYZ() > Vec3V(V_ZERO),  Vec3V(V_ONE),Vec3V(V_ZERO) );
	//	return -(DX.GetXYZ()* tRejectCY  - DY.GetXYZ() * tRejectCX );
	//}
	RAST_INLINE Vec3V_Out GetTrivalReject( ScalarV_In w, ScalarV_In h ) const
	{
		Vec4V zero(V_ZERO);
		Vec4V tRejectCX = SelectFT( DY < zero, zero, Vec4V(w) );
		Vec4V tRejectCY = SelectFT( DX < zero,  Vec4V(h),zero);
		return (-DX* tRejectCY  + DY * tRejectCX).GetXYZ();
	}
};

struct EdgeList
{
	Vec3V DX;
	Vec3V DY;
	Vec3V C;
};

struct ShadowSquares
{
	Vec4V m_spheresX;
	Vec4V m_spheresY;
	Vec4V m_spheresR;
	Vec4V m_axisXY;
	bool enabled;
	void Default(){
		m_spheresX=Vec4V(10.f,30.f,40.f,50.f);
		m_spheresY=Vec4V(10.f,30.f,40.f,50.f);
		m_spheresR=Vec4V(ScalarV(10.f));
		m_axisXY=Vec4V(3.f,3.f,3.f,-3.f);
		enabled=true;
	}
	void reset(){
		enabled=false;
		m_spheresX=Vec4V(V_ZERO);
		m_spheresY=Vec4V(V_ZERO);
		m_spheresR=Vec4V(V_ZERO);
		m_axisXY=Vec4V(V_ZERO);
	}
};

struct ScanTileInfo
{
	const u16*			_indices;
	const PreTri2*		tris;
	int*				m_cnt;	
	u32					pad;
	
	ShadowSquares		m_squares;

	ScanTileInfo() : m_cnt(0)	{}
	void GetAndCalcInfo( int dmaTag, int index, const u16*& smallIndices, int& smallAmt, const u16*& indices, int& amt )
	{
		int		cntTableBuffer[RAST_NUM_TILES*3];
		const int* cntTable = taskLoadBuffer( "cnt table",cntTableBuffer,m_cnt,RAST_NUM_TILES*2,dmaTag);
		taskWait(dmaTag);
		amt = cntTable[ index];
		int sa = cntTable[ index + RAST_NUM_TILES];
		indices = _indices + MAX_TILER_TRIS*index;
		smallIndices = indices + sa+1;
		smallAmt = (MAX_TILER_TRIS-1) -sa;
		

		int maxAmt=MAX_TILER_TRIS;

		amt=amt > 0 ? amt : 0;
		amt=amt <maxAmt ? amt :maxAmt;
		
		smallAmt=smallAmt > 0 ? smallAmt : 0;
		smallAmt=smallAmt < maxAmt ? smallAmt :maxAmt;
	}
};
CompileTimeAssert( (sizeof(ScanTileInfo)&15)==0);

#if __BANK
extern bool g_ShowBoundsInHiZ;
#endif
struct ScanUserData
{
	
	float	 locationx;
	float	 locationy;

	Vec4V*	HiZTile;
	u32*	nearZVal;
	Vec4V*	minMaxBounds;
	u16		HiZStride;
	u16		index;
	bool	useSimple:1;
	bool	useSmall:1;
	bool	drawBlock:1;

};
void rstScanTile( const ScanTileInfo* listTileInfo, ScanUserData& ud, Vec4V* zbufferOut, size_t zTileSize, sysTaskBuffer& sm );

//VecBoolV_Out rstScanQueryHiZLarge( const Vec4V* __restrict HiZBuffer, Vec3V_In scmin, Vec3V_In scmax);

__forceinline VecBoolV_Out rstScanQueryHiZLargeApprox( const Vec4V* __restrict HiZBuffer, Vec3V_In scmin, Vec3V_In scmax)
{
	Assert( HiZBuffer !=0 );
	Assert16( HiZBuffer );

	///const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	/// const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	const Vec4V signMask(V_7FFFFFFF);
	//const Vec4V bottomMask = Vec4VConstant<~0x1, ~0x1, ~0x1,~0x1>();
	const Vec4V one(V_ONE);
	const Vec4V two(V_TWO);
	const Vec4V three(V_THREE);
	const Vec4V four(V_FOUR);
	const Vec3V half(V_HALF);
	const Vec3V two3(V_TWO);
	const Vec4V zero(V_ZERO);
	const Vec4V HiZSizeV( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, (float)RAST_HIZ_HEIGHT);
	const Vec3V StartRound = Vec3VConstant<U32_HALF, U32_HALF, U32_ONE>();
	const Vec3V StartScale = Vec3VConstant<U32_TWO, U32_TWO, U32_ONE>();

	Vec4V ztest = Vec4V(Invert(scmin.GetZ()));
	Assert(IsTrueAll(scmin<=scmax));
	// Check Screen space loop extents
	Vec3V startV = RoundToNearestIntZero(scmin*StartRound)*StartScale; 
	Vec3V endV = RoundToNearestIntPosInf(scmax);
	Vec4V extents = MergeXY( startV, endV );
	extents = Max( extents, zero);
	extents = Min( extents, HiZSizeV);

	Vec4V extentsInt = FloatToIntRaw<0>(extents);
	//extentsInt &=bottomMask;
	const Vec4V t = extentsInt;
	int* ext = (int*)&t;
	int sx = ext[0];
	int ex = ext[1];
	int sy = ext[2];
	int ey = ext[3];

	// TODO do branchless
	Assert( (sx >= RAST_HIZ_WIDTH  && ex >=RAST_HIZ_WIDTH) || sx <= (RAST_HIZ_WIDTH-2));	
	Assert( (sy >= RAST_HIZ_HEIGHT && ey >=RAST_HIZ_HEIGHT)|| sy <= (RAST_HIZ_HEIGHT-2));		
	Assert( (sx &0x1)==0);
	Assert( (sy &0x1)==0);
	Assert( (sx >= 0 && ex >=0));	
	Assert( (sy >= 0 && ey >=0));		
	
	VecBoolV res0(V_F_F_F_F);
	VecBoolV res1(V_F_F_F_F);
	VecBoolV res2(V_F_F_F_F);
	VecBoolV res3(V_F_F_F_F);


	const Vec4V*  __restrict hiZScanLine = HiZBuffer + sy * RAST_HIZ_WIDTH + sx;

	Assert( ex <=RAST_HIZ_WIDTH);	
	Assert( ey <=RAST_HIZ_HEIGHT);		
	//Assert(  sx!=ex);	
	//Assert(  sy!=ey);	
	for (int y = sy;  y < ey; y+=2 )
	{
		const Vec4V* __restrict hiZ = hiZScanLine;
		// branch??
		// -- super slow on 360
		// 8/15/12 - cthomas - Although the branch on the Xbox 360 may be expensive in absolute terms, 
		// it is still a very large performance improvement to test and use this early out. Analysis  
		// of the Visibility/Scan system using this version of the HiZQuery with this early out branch 
		// yielded a ~35-45% performance improvement over this same function without the early out. The 
		// TestOcclusion method in ScanEntities ran between 0.31-0.61% with the early out, versus 
		// 0.53 - 0.84% without the early out.
		if ( Unlikely(!IsFalseAll(res0|res1|res2|res3)) ) 
		{
			return res0|res1|res2|res3;
		}

		for (int x = sx; x < ex; x+=2 )
		{			
			Vec4V hiZ0 = hiZ[0];
			Vec4V hiZ1 = hiZ[1];
			Vec4V hiZ4 = hiZ[RAST_HIZ_WIDTH+0];
			Vec4V hiZ5 = hiZ[RAST_HIZ_WIDTH+1];

	
			VecBoolV test0 = (ztest > hiZ0);
			VecBoolV test1 = (ztest > hiZ1);
			VecBoolV test2 = (ztest > hiZ4);
			VecBoolV test3 = (ztest > hiZ5);

			res0 = res0 | test0; 
			res1 = res1 | test1; 
			res2 = res2 | test2; 
			res3 = res3 | test3; 

			hiZ+=2;
		}
		hiZScanLine += RAST_HIZ_WIDTH*2;
	}
	res3 = res3 | res2;
	VecBoolV res = res0 | res1;
	return res | res3;
}


__forceinline VecBoolV_Out rstScanQueryHiZLarge( const Vec4V* __restrict HiZBuffer, Vec3V_In scmin, Vec3V_In scmax, ScalarV_In minZQuery )
{
	Assert( HiZBuffer !=0 );
	Assert16( HiZBuffer );

	const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	const Vec4V signMask(V_7FFFFFFF);
	//const Vec4V bottomMask = Vec4VConstant<~0x1, ~0x1, ~0x1,~0x1>();
	const Vec4V one(V_ONE);
	const Vec4V two(V_TWO);
	const Vec4V three(V_THREE);
	const Vec4V four(V_FOUR);
	const Vec3V half(V_HALF);
	const Vec3V two3(V_TWO);
	const Vec4V zero(V_ZERO);
	const Vec4V HiZSizeV( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, (float)RAST_HIZ_HEIGHT);
	const Vec3V StartRound = Vec3VConstant<U32_HALF, U32_HALF, U32_ONE>();
	const Vec3V StartScale = Vec3VConstant<U32_TWO, U32_TWO, U32_ONE>();

	Vec4V ztest = Vec4V(Invert(minZQuery));	   
	// Check Screen space loop extents
	Vec3V startV = RoundToNearestIntZero(scmin*StartRound)*StartScale; 
	Vec3V endV = RoundToNearestIntPosInf(scmax);
	Vec4V extents = MergeXY( startV, endV );
	extents = Max( extents, zero);
	extents = Min( extents, HiZSizeV);

	Vec4V extentsInt = FloatToIntRaw<0>(extents);
	//extentsInt &=bottomMask;
	const Vec4V t = extentsInt;
	int* ext = (int*)&t;
	int sx = ext[0];
	int ex = ext[1];
	int sy = ext[2];
	int ey = ext[3];
    
	// TODO do branchless
	Assert( (sx >= RAST_HIZ_WIDTH && ex >=RAST_HIZ_WIDTH) || sx <= (RAST_HIZ_WIDTH-2));	
	Assert( (sy >= RAST_HIZ_HEIGHT && ey >=RAST_HIZ_HEIGHT)|| sy <= (RAST_HIZ_HEIGHT-2));		
	Assert( (sx &0x1)==0);
	Assert( (sy &0x1)==0);
	Assert( (sx >= 0 && ex >=0));	
	Assert( (sy >= 0 && ey >=0));		

	Vec3V scmin2 = Max( scmin,Vec3V(V_ZERO));
	scmin2 = Min( scmin2, HiZSizeV.Get<Vec::X, Vec::Z, Vec::Y>());

	Vec3V scmax2 = Max( scmax, Vec3V(V_ZERO));
	scmax2 = Min( scmax2, HiZSizeV.Get<Vec::Y, Vec::Z, Vec::Y>());

	// round to sub pixel boundaries and find 2D box for testing
	ScalarV eplislon(0.25f);
	Vec3V ppStartV = RoundToNearestIntZero(scmin2 * two3-Vec3V(eplislon)) * half;
	Vec3V ppEndV = RoundToNearestIntPosInf(scmax2 * two3+Vec3V(eplislon)) * half;


	
	VecBoolV res0(V_F_F_F_F);
	VecBoolV res1(V_F_F_F_F);
	VecBoolV res2(V_F_F_F_F);
	VecBoolV res3(V_F_F_F_F);

	const Vec4V*  __restrict hiZScanLine = HiZBuffer + sy * RAST_HIZ_WIDTH + sx;

	Vec3V midP = ( ppStartV + ppEndV ) * half;
	Vec3V bounds = ( ppEndV - ppStartV ) * half;

	Vec4V boundY = Vec4V(bounds.GetY()); 
	Vec4V boundX = Vec4V(bounds.GetX());

	Vec4V sxV =  Vec4V(extents.GetX()) + offsetX - Vec4V(midP.GetX());
	Vec4V posY =  Vec4V(extents.GetZ()) + offsetY - Vec4V(midP.GetY());

	Vec4V sposX0 = sxV;
	Vec4V sposX1 = sxV + one;

	Assert( ex <= RAST_HIZ_WIDTH);	
	Assert( ey <= RAST_HIZ_HEIGHT);		

	// TODO : unroll so works well with small boxes as well as large
	ey-=sy;
	ex-=sx;	
	for (int y = 0;  y < ey; y+=2 )
	{
		const Vec4V* __restrict hiZ = hiZScanLine;
		Vec4V absPosY = posY &signMask;

		VecBoolV inRangeY =  absPosY <= boundY;
		VecBoolV inRangeY2 =  ((posY+one) &signMask) <= boundY;

		Vec4V bounds0 = boundX & Vec4V(inRangeY);
		Vec4V bounds1 = boundX & Vec4V(inRangeY2);

		Vec4V posX0 = sposX0;
		Vec4V posX1 = sposX1;

		for (int x = 0; x < ex; x+=2 )
		{			
			Vec4V pX0 = posX0 & signMask;
			Vec4V pX1 = posX1 & signMask;

			Vec4V hiZ0 = hiZ[0];
			Vec4V hiZ1 = hiZ[1];
			Vec4V hiZ4 = hiZ[RAST_HIZ_WIDTH+0];
			Vec4V hiZ5 = hiZ[RAST_HIZ_WIDTH+1];
			
			VecBoolV inbounds0 = pX0 <= bounds0;
			VecBoolV inbounds1 = pX1 <= bounds0;

			VecBoolV test0 = (ztest > hiZ0);
			VecBoolV test1 = (ztest > hiZ1);

			VecBoolV inbounds4 = pX0 <= bounds1;
			VecBoolV inbounds5 = pX1 <= bounds1;

			inbounds0 = inbounds0& inRangeY;
			inbounds1 = inbounds1& inRangeY;

			VecBoolV test4 = (ztest > hiZ4);
			VecBoolV test5 = (ztest > hiZ5);

			test0 = test0 & inbounds0;
			test1 = test1 & inbounds1;

			test4 = test4 & inbounds4;
			test5 = test5 & inbounds5;

			res0 = res0 | test0; 
			res1 = res1 | test1; 
			res2 = res2 | test4; 
			res3 = res3 | test5; 

			posX0 += two;
			posX1 += two;

			hiZ+=2;
		}
		posY += two;

		hiZScanLine += RAST_HIZ_WIDTH*2;
	}
	res3 = res3 | res2;
	VecBoolV res = res0 | res1;
	return res | res3;
}

inline ScalarV_Out rstScanQueryCountHiZLarge( const Vec4V* __restrict HiZBuffer, Vec3V_In scmin, Vec3V_In scmax)
{
	const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	const Vec4V signMask(V_7FFFFFFF);
	const Vec4V one(V_ONE);
	const Vec4V two(V_TWO);
	const Vec4V three(V_THREE);
	const Vec4V four(V_FOUR);
	const Vec3V half(V_HALF);
	const Vec3V two3(V_TWO);
	static const Vec4V HiZSizeV( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, (float)RAST_HIZ_HEIGHT);

	Vec4V ztest = Vec4V(Invert(scmin.GetZ()));

	// Check Screen space loop extents
	Vec3V startV = RoundToNearestIntZero(scmin); 
	Vec3V endV = RoundToNearestIntPosInf(scmax);
	Vec4V extents = MergeXY( startV, endV );
	extents = Max( extents, Vec4V(V_ZERO));
	extents = Min( extents, HiZSizeV);
	Vec4V extentsInt = FloatToIntRaw<0>(extents);
	const Vec4V t = extentsInt;
	int* ext = (int*)&t;
	int sx = ext[0];
	int ex = ext[1];
	int sy = ext[2];
	int ey = ext[3];

	Vec3V scmin2 = Max( scmin, Vec3V(V_ZERO));
	scmin2 = Min( scmin2, HiZSizeV.GetXYZ());

	Vec3V scmax2 = Max( scmax, Vec3V(V_ZERO));
	scmax2 = Min( scmax2, HiZSizeV.GetXYZ());

	// round to sub pixel boundaries and find 2D box for testing
	Vec3V ppStartV = RoundToNearestIntZero(scmin2 * two3) * half;
	Vec3V ppEndV = RoundToNearestIntPosInf(scmax2 * two3) * half;

	Vec3V midP = ( ppStartV + ppEndV ) * half;
	Vec3V bounds = ( ppEndV - ppStartV ) * half;

	Vec4V boundY = Vec4V(bounds.GetY()); 
	Vec4V boundX = Vec4V(bounds.GetX());

	Vec4V sxV =   Vec4V(extents.GetX()) + offsetX - Vec4V(midP.GetX());
	Vec4V posY =  Vec4V(extents.GetZ()) + offsetY - Vec4V(midP.GetY());
	Vec4V res0(V_ZERO);
	Vec4V res1(V_ZERO);
	Vec4V res2(V_ZERO);
	Vec4V res3(V_ZERO);

	const Vec4V*  __restrict hiZScanLine = HiZBuffer + sy * RAST_HIZ_WIDTH + sx;

	Vec4V inRangeX[RAST_HIZ_WIDTH+4];
	Vec4V posX0 = sxV;
	int tx=ex-sx;
	for (int x = 0; x <tx+4 ; x++)  // could do first and last bounds check outside loop?
	{
		Vec4V pX0 = Abs(posX0);
		posX0 += Vec4V(V_ONE);
		inRangeX[x]=Vec4V(pX0 <= boundX) &one;
	}

	for (int y = sy;  y < ey; y+=2 )
	{
		const Vec4V* __restrict hiZ = hiZScanLine;

		Vec4V absPosY = posY &signMask;

		VecBoolV inRangeY =  absPosY <= boundY;
		VecBoolV inRangeY2 =  ((posY+one) &signMask) <= boundY;

		Vec4V ztestY = ztest& Vec4V(inRangeY);  // remove values that aren't on valid scanlines.
		Vec4V ztestY2 = ztest& Vec4V(inRangeY2);

		
		for (int x = 0; x < tx; x+=4 )
		{			
			Vec4V x0 =inRangeX[x]; Vec4V x1=inRangeX[x+1];
			Vec4V x2 =inRangeX[x+2]; Vec4V x3=inRangeX[x+3];

			Vec4V hiZ0 = hiZ[0];
			Vec4V hiZ1 = hiZ[1];
			Vec4V hiZ2 = hiZ[2];
			Vec4V hiZ3 = hiZ[3];
			Vec4V hiZ4 = hiZ[RAST_HIZ_WIDTH+0];
			Vec4V hiZ5 = hiZ[RAST_HIZ_WIDTH+1];
			Vec4V hiZ6 = hiZ[RAST_HIZ_WIDTH+2];
			Vec4V hiZ7 = hiZ[RAST_HIZ_WIDTH+3];
						
			Vec4V test0 = Vec4V(ztestY > hiZ0) & x0;
			Vec4V test1 = Vec4V(ztestY > hiZ1) & x1;
			Vec4V test2 = Vec4V(ztestY > hiZ2) & x2;
			Vec4V test3 = Vec4V(ztestY > hiZ3) & x3;
			Vec4V test4 = Vec4V(ztestY2 > hiZ4) & x0;
			Vec4V test5 = Vec4V(ztestY2 > hiZ5) & x1;
			Vec4V test6 = Vec4V(ztestY2 > hiZ6) & x2;
			Vec4V test7 = Vec4V(ztestY2 > hiZ7) & x3;

			res0 = res0 + test0;
			res1 = res1 + test1; 
			res2 = res2 + test2; 
			res3 = res3 + test3; 
			res0 = res0 + test4; 
			res1 = res1 + test5; 
			res2 = res2 + test6; 
			res3 = res3 + test7; 
			hiZ+=4;
		}
		posY += two;
		hiZScanLine += RAST_HIZ_WIDTH*2;
	}
	res3 = res3+ res2;
	Vec4V res = res0 + res1;
	res = res + res3;
	return Dot(res, Vec4V(V_QUARTER));
}


void rstSetStencilBits( Vec4V* HiZBuffer, Vec4V* stencilBuffer );
void rstClearStencilBuffer( Vec4V*  stencilBuffer);
void rstCompareToStencilBits( const Vec4V* __restrict HiZBuffer,const  Vec4V* __restrict stencilHiZBuffer, Vec4V* __restrict stencilBuffer );

void rstStencilHiZ(Vec4V* __restrict HiZBuffer, const PreTri2* tris, int amt, const struct StencilWriterUserData& ud, bool writeZ );


ScalarV_Out rstTestHiZ( const Vec4V* __restrict HiZBuffer, const PreTri2* tris, ScalarV* minZ, int amt );
ScalarV_Out rstTest2EdgeListsHiZ( const Vec4V* __restrict HiZBuffer, const EdgeList& edge0,const EdgeList& edge1, ScalarV_In minZ, Vec4V_In minMaxV	
							   DEV_ONLY(, u32* trivialAcceptVisiblePixel) BANK_ONLY(, bool useTrivialAcceptVisiblePixelTest));
ScalarV_Out rstTest3EdgeListsHiZ( const Vec4V* __restrict HiZBuffer, const EdgeList& edge0,const EdgeList& edge1, const EdgeList& edge2, ScalarV_In minZ, Vec4V_In minMaxV	
							   DEV_ONLY(, u32* trivialAcceptVisiblePixel) BANK_ONLY(, bool useTrivialAcceptVisiblePixelTest));



}
#ifndef FRAG_TSKLIB_ONLY
// namespace rage
void scan( rage::sysTaskParameters& params);
#endif

SPUFRAG_DECL(bool, scan_frag, rage::sysDependency&);


#endif // SOFTRASTERIZER_SCAN_H 
