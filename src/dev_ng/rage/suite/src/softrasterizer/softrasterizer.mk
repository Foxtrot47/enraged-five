PROJECT = softrasterizer

INCLUDES = -I .. -I ..\..\..\base\src -I ..\..\..\suite\src
DEFINES = \

OBJS = \
	$(INTDIR)/scan.job.obj	\
	$(INTDIR)/scan.obj	\
	$(INTDIR)/tiler.obj	\
	$(INTDIR)/softrasterizer.obj	\


include ..\..\..\..\rage\build\Makefile.template

$(INTDIR)/scan.job.obj: ./scan.job
	call ..\..\..\..\rage\build\make_spurs_job_pb $(subst /,\,$(abspath $(subst \,/,.))) $(INTDIR) $(basename scan.job) "$(INTDIR)"

$(INTDIR)/scan.obj: ./scan.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/tiler.obj: ./tiler.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(INTDIR)/softrasterizer.obj: ./softrasterizer.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

HEADERS: $(HEADERS)
