// 
// softrasterizer/softrasterizer.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
#include "tiler.h"
#include "softrasterizer.h"
#include "profile/profiler.h"
#include "system/cache.h"
#include "grcore/im.h"
#include "grcore/light.h"
#if __BANK
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/quads.h"
#endif
#include "vectormath/legacyconvert.h"
#include "softrasterizer/boxOccluder.h"
#include "system/new.h"

#include "system/dependencyscheduler.h"
#if !RSG_PS3

SPUFRAG_DECL(bool, addTasks_frag, sysDependency&);

#include "softrasterizer/addTasks_frag.frag"
SPUFRAG_DECL(bool, boxoccluder_frag, sysDependency&);
SPUFRAG_DECL(bool, boxselector_frag, sysDependency&);
SPUFRAG_DECL(bool, modelselector_frag, sysDependency&);

#endif
#if !__SPU
DECLARE_FRAG_INTERFACE(addTasks_frag);
DECLARE_FRAG_INTERFACE(boxoccluder_frag);
DECLARE_FRAG_INTERFACE(boxselector_frag);
DECLARE_FRAG_INTERFACE(modelselector_frag);
DECLARE_FRAG_INTERFACE(stencilwriter_frag);
DECLARE_FRAG_INTERFACE(generatewatertris_frag);
#endif
#define ALLOC_ZBUFFER 0
//__ASSERT

namespace rage {

#if __RASTERSTATS
namespace RasterStats
{
	EXT_PF_TIMER(Resolve);
	EXT_PF_TIMER(AddTriList);
	EXT_PF_TIMER(Queries);
	EXT_PF_TIMER(Rasterize);
};
using namespace RasterStats;
#endif // __RASTERSTAT

RAST_INLINE void CalculateScreenSpaceBoundSlow(	Mat44V_In mtx, ScalarV_In rescaleDepth,
	Vec3V_In bmin, Vec3V_In bmax,
	Vec3V_InOut scbmin, Vec3V_InOut scbmax )
{

	Vec3V r0 = bmin;
	Vec3V r1 = GetFromTwo<Vec::X1, Vec::Y1, Vec::Z2>(bmin, bmax);
	Vec3V r2 = GetFromTwo<Vec::X1, Vec::Y2, Vec::Z2>(bmin, bmax);
	Vec3V r3 = GetFromTwo<Vec::X1, Vec::Y2, Vec::Z1>(bmin, bmax);
	Vec3V r4 = GetFromTwo<Vec::X2, Vec::Y1, Vec::Z1>(bmin, bmax);
	Vec3V r5 = GetFromTwo<Vec::X2, Vec::Y1, Vec::Z2>(bmin, bmax);
	Vec3V r6 = GetFromTwo<Vec::X2, Vec::Y2, Vec::Z1>(bmin, bmax);
	Vec3V r7 = bmax;

	Vec3V pt0 = FullTransform2V( mtx, r0, rescaleDepth);
	Vec3V pt1 = FullTransform2V( mtx, r1, rescaleDepth);
	Vec3V pt2 = FullTransform2V( mtx, r2, rescaleDepth);
	Vec3V pt3 = FullTransform2V( mtx, r3, rescaleDepth);
	Vec3V pt4 = FullTransform2V( mtx, r4, rescaleDepth);
	Vec3V pt5 = FullTransform2V( mtx, r5, rescaleDepth);
	Vec3V pt6 = FullTransform2V( mtx, r6, rescaleDepth);
	Vec3V pt7 = FullTransform2V( mtx, r7, rescaleDepth);

	Vec3V ptMin0 = Min(pt0, pt1);
	Vec3V ptMin1 = Min(pt2, pt3);
	Vec3V ptMin2 = Min(pt4, pt5);
	Vec3V ptMin3 = Min(pt6, pt7);

	Vec3V ptMax0 = Max(pt0, pt1);
	Vec3V ptMax1 = Max(pt2, pt3);
	Vec3V ptMax2 = Max(pt4, pt5);
	Vec3V ptMax3 = Max(pt6, pt7);

	ptMin0 = Min(ptMin0, ptMin1);
	ptMin1 = Min(ptMin2, ptMin3);

	ptMax0 = Min(ptMax0, ptMax1);
	ptMax1 = Min(ptMax2, ptMax3);

	scbmin = Min(ptMin0, ptMin1);
	scbmax = Max(ptMax0, ptMax1);
}

ScalarV_Out rstCreateCurrentSplatMat44V( const grcViewport* vp, rstSplatMat44V& splatMtx, Vec3V_InOut scrExtents )
{
	rstTransformToScreenSpace clipAndTransform( vp, true);
	return rstCreateCurrentSplatMat44V( clipAndTransform,splatMtx,scrExtents );
}

void rstCalculateOcclusion( const grcViewport* vp,  int start, int end, 
						const Vec4V* HiZ,
						const Vec3V* __restrict bmin, const Vec3V* __restrict bmax, 
						Vec3V* __restrict scmin,Vec3V* __restrict scmax,
						u32* __restrict results )
{
#if __RASTERSTATS
	PF_FUNC(Queries);
#endif
	rstSplatMat44V splatMtx;
	Vec3V scExtents;
	ScalarV rescaleDepth = rstCreateCurrentSplatMat44V( vp, splatMtx, scExtents);
	Vec3V scbmin;
	Vec3V scbmax;
	ScalarV zero(V_ZERO);
	const Vec3V extents( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, 0.f);

	for (int i = start; i < end; i++)
	{
		if ( rstCalculateScreenSpaceBoundIsVis(	splatMtx, rescaleDepth,bmin[i],bmax[i],scbmin, scbmax,extents)){  // pipeline these two
			results[i] = !IsTrueAll(rstScanQueryHiZLarge( HiZ, scbmin, scbmax, scbmin.GetZ()) );
		}
		else
			results[i]=false;

		scmin[i] = scbmin;
		scmax[i] = scbmax;
	}
}

void rstCalculateOcclusionAABBExactEdgeList( const grcViewport* vp,  int start, int end, 
						   const Vec4V* HiZ, Vec4V_In minMaxBounds, ScalarV_In minZBuffer,
						   const Vec3V* __restrict bmin, const Vec3V* __restrict bmax, 
						   u32* __restrict results 
						   DEV_ONLY(, u32* trivialAcceptActiveFrustum, u32* trivialAcceptMinZ, u32* trivialAcceptVisiblePixel)
#if __BANK
						   ,Vec3V* __restrict scmin,Vec3V* __restrict scmax
						   , ScalarV_In simpleTestThresholdPixels, bool useTrivialAcceptVisiblePixelTest
#endif						  
						   )
{
#if __RASTERSTATS
	PF_FUNC(Queries);
#endif
	rstTransformToScreenSpace transform( vp, true);
	ScalarV zero(V_ZERO);
	const Vec3V extents( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, 0.f);
#if __BANK
	Vec3V scbmin;
	Vec3V scbmax;
	rstSplatMat44V splatMtx;
	Vec3V scExtents;
	ScalarV rescaleDepth = rstCreateCurrentSplatMat44V( vp, splatMtx, scExtents);
#endif
	for (int i = start; i < end; i++)
	{
		results[i] = IsZeroAll(rstTestAABBExactEdgeList( false, transform.m_comMtx, transform.GetRescaleDepth(), HiZ, vp->GetCameraPosition(), bmin[i], bmax[i], ScalarV(V_ZERO), minMaxBounds, minZBuffer
			DEV_ONLY(, trivialAcceptActiveFrustum, trivialAcceptMinZ, trivialAcceptVisiblePixel)
			BANK_ONLY(, simpleTestThresholdPixels, true, useTrivialAcceptVisiblePixelTest)
			));

#if __BANK
		rstCalculateScreenSpaceBoundIsVis(	splatMtx, rescaleDepth,Vec3V(V_ZERO), scbmin, scbmax, scbmax,extents);
		scmin[i] = scbmin;
		scmax[i] = scbmax;
#endif
	}
}

//--------------------- Clipping code 

/*
// http://pmaillot.chez.com/tstrips.pdf
template<class TriFunc>
RAST_INLINE void Clipper( Vec3V_In i, Vec3V_In j, Vec3V_In k, Vec4V_In clipPlane, TriFunc& AddTri )
{
	ScalarV one(V_ONE);

	ScalarV iDist = Dot(Vec4V(i,one), clipPlane); 
	ScalarV jDist = Dot(Vec4V(j,one), clipPlane); 
	ScalarV kDist = Dot(Vec4V(k,one), clipPlane);

	Vec3V codeDist( iDist, jDist, kDist);

	int codeV = MoveSignMask( codeDist);
	Assert( codeV >=0 && codeV <= 7);

	Vec3V denom = codeDist - codeDist.Get<Vec::Y, Vec::Z, Vec::X>();
	denom = Invert(denom);
	Vec3V t = codeDist * denom;  

	Vec3V a = Lerp( t.GetX(), i,j);
	Vec3V b = Lerp( t.GetY(), j,k);
	Vec3V c = Lerp( t.GetZ(), k,i);

	switch( codeV )
	{
	case 0: // all inside
		AddTri(i,j,k);
		break;
	case 1: // P outside, Q and R inside
		
		AddTri(c,a,j);
		AddTri(j,k,c);
		break;
	case 2:
		AddTri(i,a,b);
		AddTri(b,k,i);
		break;
	case 3:
		AddTri(c,b,k);
		break;
	case 4:
		AddTri(i,j,b);
		AddTri(b,c,i);
		break;
	case 5:
		AddTri(a,j,b);
		break;
	case 6:
		AddTri(i,a,c);
		break;
	case 7:
		break;
	}
}

struct ClippedResults
{
	atArray<Vec3V,128,u32>& m_triList;

	ClippedResults(atArray<Vec3V,128,u32>& results) : m_triList(results)
	{
		m_triList.ResetCount();
	}
	RAST_INLINE void operator()(Vec3V_In p1, Vec3V_In p2,Vec3V_In p3)
	{
		m_triList.Append()=p1;
		m_triList.Append()=p2;
		m_triList.Append()=p3;
	}
};
*/

SoftRasterizer::SoftRasterizer(rstTileList* tilers, bool createWaterOccluderList)
:	m_selectorTask(true),
	m_tilerTask(true),
	m_stencilTask(true),
	m_scanTask(true),
	m_largeTriangleOcclusion(true),
	m_generateWaterTrisTask(true),
	m_modelSelectorTask(true),
	m_compareAndGenerateStencilTask(true),
	m_transform( rage_new rstTransformToScreenSpace() ),
	m_stencilTransform( rage_new rstTransformToScreenSpace() ), 
	m_modelWorkCount( rage_new u32() )
{
	// allow for sharing tiles
	m_tiler = tilers;
	// combine stencil and hi-z in one block for fast dma'ing.
	m_HiZBuffer = rage_new Vec4V[ RAST_HIZ_AND_STENCIL_SIZE_VEC];
	m_StencilBuffer = m_HiZBuffer + RAST_HIZ_SIZE_VEC;

	// note could reclaim this somehow by overlapping with HiZBuffer
	m_tileBoxPackets = rage_aligned_new(16) TileDataPacket<BoxOccluder>[ RAST_NUM_TILERS_ACTIVE ];
	m_tileModelPackets = rage_aligned_new(16) TileDataPacket<OccludeModel>[ RAST_NUM_TILERS_ACTIVE ];

	for ( int i = 0; i < RAST_NUM_TILERS_ACTIVE; i++)
	{
		m_tileBoxPackets[i].amt=0;
		m_tileModelPackets[i].amt=0;
	}	

	m_parentRasterizer = 0;
	m_isParent=false;
	Assert16(m_transform);
	Assert16(m_stencilTransform);

#if ALLOC_ZBUFFER
	m_ZBuffer = rage_new Vec4V[ RAST_SCREEN_SIZE];
#else
	m_ZBuffer = 0;
#endif

#if __BANK
	grcImage *result = grcImage::Create(RAST_SCREEN_WIDTH_PIXELS, RAST_SCREEN_HEIGHT_PIXELS,1, grcImage::L8, grcImage::STANDARD,0,0);

	Assert( result );
	grcTextureFactory::TextureCreateParams params(grcTextureFactory::TextureCreateParams::SYSTEM, grcTextureFactory::TextureCreateParams::LINEAR );
	BANK_ONLY(grcTexture::SetCustomLoadName("DebugDrawTexture");)
	m_DebugDrawTexture = rage::grcTextureFactory::GetInstance().Create(result, &params);
	BANK_ONLY(grcTexture::SetCustomLoadName(NULL);)
	Assert( m_DebugDrawTexture );
	LastSafeRelease( result );
#endif
	m_useSimple = false;
	m_useSmall = true;
#if __BANK
	m_viewScale = 2.0f;
	m_showHiZ = false;
	m_depthStart = 0.0f;
	m_depthEnd = 0.08f;
	m_depthAlpha = 0.75f;
	m_depthFullscreen = false;
	m_depthOverlapShadowTarget = false;
	m_debugDraw = false;
	m_showStencil=false;
	m_showWaterZBuffer=false;
	m_dumpTaskGraph = false;
#endif
	if ( createWaterOccluderList )
	{
		m_outputWaterOccluders = rage_aligned_new(16) OccludeModel[ OccludeModel::MaxWaterOccluders ];
		m_outputWaterClipped = rage_aligned_new(16) u8[ OccludeModel::MaxWaterOccluders ];		
	}
	else 
	{
		m_outputWaterOccluders = NULL;
		m_outputWaterClipped = NULL;
	}
	m_waterOccluderCount = rage_aligned_new(16) u32();
	m_tempHiZBuffer = NULL;

	m_useSysDependencyTasks= false;
	m_hasStencilDependancy=false;
	m_hasWaterTriDependancy=false;
	m_useTempHiZBufferForStencil = true;
	m_shadowSquares.reset();
}

SoftRasterizer::~SoftRasterizer()
{
	delete [] m_HiZBuffer;
	delete [] m_ZBuffer;

	delete m_transform;
	delete m_modelWorkCount;

	delete m_tileBoxPackets;
	delete m_tileModelPackets;
#if __BANK
	m_DebugDrawTexture->Release();
#endif
}

void SoftRasterizer::BeginSubmit( const grcViewport* vp NV_SUPPORT_ONLY(, bool useStereoFrustum) )
{
	rstTransformToScreenSpace clipAndTransform(vp,false NV_SUPPORT_ONLY(,useStereoFrustum));  // move to start function
	*m_transform = clipAndTransform;
	for ( int i = 0; i < RAST_NUM_TILERS_ACTIVE; i++)
	{
		m_tileBoxPackets[i].amt=0;
		m_tileModelPackets[i].amt=0;
	}	
}

void SoftRasterizer::RemoveWaterTriDependancy()
{
	if (!m_useSysDependencyTasks )
	{
		m_hasWaterTriDependancy=false;
		m_generateWaterTrisTask.RemoveDependancy(false);
	}	
}

void SoftRasterizer::AddWaterTriDependancy()
{
	Assert( m_useSysDependencyTasks == false);
	m_hasWaterTriDependancy=true;
	m_generateWaterTrisTask.DependsOn(1);
}

void SoftRasterizer::RemoveStencilDependancy()
{
	if (!m_useSysDependencyTasks )
	{
		m_hasStencilDependancy=false;
		m_stencilTask.RemoveDependancy(false);
	}	
}

void SoftRasterizer::AddStencilDependancy()
{
	Assert( m_useSysDependencyTasks == false);
	m_hasStencilDependancy=true;
	m_stencilTask.DependsOn(1);
}

DependantTaskList* SoftRasterizer::PrepareScanTasks()
{
	rstCreateScanTileTasks(   m_tiler->tiles.begin(), 
		GetZBuffer(), 
		GetHiZBuffer(),
		RAST_HIZ_WIDTH,
		m_useSimple,
		m_useSmall,
		false,
		&m_scanTask,
		m_shadowSquares,
		stInfo,
		nearZTiles,
		minMaxBounds);
	return &m_scanTask;
}

void SoftRasterizer::PrepareScanTasks( sysDependency scanTasks[RAST_NUM_TILES])
{
	rstCreateScanTileTasks(   m_tiler->tiles.begin(), 
		GetZBuffer(), 
		GetHiZBuffer(),
		RAST_HIZ_WIDTH,
		m_useSimple,
		m_useSmall,
		false,
		scanTasks,
		m_shadowSquares,
		stInfo,
		nearZTiles,
		minMaxBounds
		);	

	//tskSetInputOutput( addTaskSortTasks, 0, tasks, NUM_FRAGMENTS);

	// fire off this tasks as one unit
	m_postScanTask.Init( FRAG_SPU_CODE(addTasks_frag), 0);
	m_postScanTask.m_Priority = sysDependency::kPriorityMed;
	for (int i=0; i<RAST_NUM_TILES;i++)
	{		
		scanTasks[i].AddChild( m_postScanTask);
	}
}

bool SoftRasterizer::EndSubmitWithSySDep()
{
	bool jobsStarted = false;
	bool isParentActive = m_parentRasterizer && ( m_parentRasterizer->m_selectorTask.IsSetup() || m_parentRasterizer->m_modelSelectorTask.IsSetup() );

	PrepareSubmitTilersDependency();

	if (isParentActive){
		// is sharing tiler so wait until parent down
		for (int i=0; i<RAST_NUM_TILERS_ACTIVE; i++)
			m_tilerDepTask[i].AddChild( m_parentRasterizer->m_postScanTask);
	}

	if ( isParentActive )
	{
		for (int i=0; i<RAST_NUM_TILERS_ACTIVE; i++)
		{
			m_selectorDepTask.AddChild( m_parentRasterizer->m_tilerDepTask[i] );
			m_modelSelectorDepTask.AddChild( m_parentRasterizer->m_tilerDepTask[i] );
		}			
	}
	for (int i=0; i<RAST_NUM_TILERS_ACTIVE; i++)
	{
		m_tilerDepTask[i].AddChild( m_selectorDepTask );
		m_tilerDepTask[i].AddWeakChild( m_modelSelectorDepTask );
	}

	PrepareScanTasks( m_scanDepTask);
	for (int i=0; i<RAST_NUM_TILES; i++){
		m_scanDepTask[i].AddChild( m_postTilerTask );			
	}
	if ( !m_isParent && !isParentActive)
	{
		sysDependencyScheduler::Insert( &m_selectorDepTask);			
		sysDependencyScheduler::Insert( &m_modelSelectorDepTask);
		jobsStarted = true;
	}
	else if (isParentActive)
	{
		sysDependencyScheduler::Insert( &m_parentRasterizer->m_selectorDepTask);			
		sysDependencyScheduler::Insert( &m_parentRasterizer->m_modelSelectorDepTask);
		jobsStarted = true;
	}	
	return jobsStarted;
}

bool SoftRasterizer::EndSubmit( DependantTaskList* toMakeDependant, bool perfWait )
{
	if ( m_useSysDependencyTasks)
	{
		return EndSubmitWithSySDep();
	}
	bool jobsStarted = false;
	bool isParentActive = m_parentRasterizer && ( m_parentRasterizer->m_selectorTask.IsSetup() || m_parentRasterizer->m_modelSelectorTask.IsSetup() );

	if ( m_selectorTask.IsSetup() || m_modelSelectorTask.IsSetup() )
	{
		DependantTaskList* tiler = PrepareSubmitBoxOccluders();

		if (isParentActive){
			// is sharing tiler so wait until parent down
			Assertf( m_parentRasterizer->m_scanTask.IsSetup(),"Chaining occlusion viewports but first one not setup");
			tiler->DependsOn( m_parentRasterizer->m_scanTask);		
		}

		if ( m_selectorTask.IsSetup()){				
			tiler->DependsOn( m_selectorTask);
		}

		if (m_modelSelectorTask.IsSetup()){			
				tiler->DependsOn( m_modelSelectorTask );
		}

		DependantTaskList* scanTasks = PrepareScanTasks();
		scanTasks->DependsOn( *tiler);
		if ( toMakeDependant )
			toMakeDependant->DependsOn( *scanTasks );

		if ( m_generateWaterTrisTask.IsSetup()){
			// we need to wait until the boxoccluder jobs are complete
			m_generateWaterTrisTask.DependsOn(*tiler);
			scanTasks->DependsOn(m_generateWaterTrisTask);
		}

		if ( m_stencilTask.IsSetup()){	
			if ( !m_useTempHiZBufferForStencil )
				m_stencilTask.DependsOn( *scanTasks);
			else
			{			
				Assert( m_compareAndGenerateStencilTask.IsSetup());
				m_stencilTask.DependsOn( m_modelSelectorTask);
				m_compareAndGenerateStencilTask.DependsOn(m_stencilTask);
				m_compareAndGenerateStencilTask.DependsOn( *scanTasks);
			}

			if ( toMakeDependant )
				toMakeDependant->DependsOn( m_stencilTask );
		}

#if __BANK
		if ( m_dumpTaskGraph && m_parentRasterizer)
		{
			const DependantTaskList* rootNodes[16];
			rootNodes[0]=&m_parentRasterizer->m_selectorTask;
			rootNodes[1]=&m_parentRasterizer->m_modelSelectorTask;
			DependantTaskList::taskDumpDependancyGraph("c:\\rasterizerGraph.dot",rootNodes,2 );
			m_dumpTaskGraph=false;
		}
#endif
		if ( m_isParent )
		{
			m_scanTask.DependsOn(1);  // parent scan tasks will wait until child is fired off			
		}
		if (!isParentActive)// fire off parent straight away
		{
			m_selectorTask.StartRoot();
			m_modelSelectorTask.StartRoot();
			jobsStarted = true;
		}			
	}
	if (isParentActive)
	{
		m_modelSelectorTask.StartRoot();
		m_selectorTask.StartRoot();

		m_parentRasterizer->RemoveChildDependancy();  // remove dependancy so won't fire until ready
		jobsStarted = true;
	}
	// back up for simpler cases
	if ( !m_scanTask.IsSetup())  
	{
		rstCreateScanTileTasks(   m_tiler->tiles.begin(), 
			GetZBuffer(), 
			GetHiZBuffer(),
			RAST_HIZ_WIDTH,
			m_useSimple,
			m_useSmall,
			false,
			&m_scanTask,
			m_shadowSquares,
			stInfo,
			nearZTiles,
			minMaxBounds);
		
		if ( toMakeDependant )
			toMakeDependant->DependsOn( m_scanTask );

		m_scanTask.Start();
		if (  perfWait)
		{
			m_scanTask.Wait();
		}
		jobsStarted = true;
	}

	return jobsStarted;
}

void SoftRasterizer::RemoveChildDependancy()
{
	m_scanTask.RemoveDependancy(false);  // remove dependancy so won't fire until ready
}

void SoftRasterizer::RasterizeModel(
					   const Vec3V*	verts, 
					   const u8*	indices, 
				   int  numIndices,
				   int  numVerts,
				   bool requiresClipping )
{
	m_tiler->tiles[0]->AddIndexedTriList( verts, indices, numIndices, numVerts, requiresClipping);	
}

float SoftRasterizer::GetCurrentRastTransform( Mat44V_InOut mtx)
{
	mtx = m_transform->m_comMtx;
	return m_transform->GetRescaleDepth().Getf();
}

rstTransformToScreenSpace SoftRasterizer::GetCurrentRastTransform()
{
	return *m_transform;
}

void SoftRasterizer::RasterizeTris( const atArray<Vec3V,128,u32>& /*sceneTris*/ )
{
	// TODO : convert to build a mesh 
	/*
	grcWorldIdentity();
	atArray<Vec3V,128,u32>			m_ClippedSceneTris; 
	if ( m_ClippedSceneTris.GetCapacity() != (sceneTris.GetCount()*2) )
	{
		m_ClippedSceneTris.Reset();
		m_ClippedSceneTris.Reserve(sceneTris.GetCount()*2);
	}
	rstTransformToScreenSpace clipAndTransform( grcViewport::GetCurrent(), false);
	int numTris = sceneTris.GetCount();

	ClippedResults	clippedResults(m_ClippedSceneTris);

	int amt = numTris/3-4;
	int i = 0;
	for (; i < amt; i+=4)
	{
		PrefetchDC( sceneTris.begin() + i *3 + 12);
		Clipper(sceneTris[i*3+0],sceneTris[i*3+1],sceneTris[i*3+2], clipAndTransform.m_cplane, clippedResults);
		Clipper(sceneTris[i*3+3],sceneTris[i*3+4],sceneTris[i*3+5], clipAndTransform.m_cplane, clippedResults);
		Clipper(sceneTris[i*3+6],sceneTris[i*3+7],sceneTris[i*3+8], clipAndTransform.m_cplane, clippedResults);
		Clipper(sceneTris[i*3+9],sceneTris[i*3+10],sceneTris[i*3+11], clipAndTransform.m_cplane, clippedResults);
	}
	for (; i < numTris/3; i++)
	{
		Clipper(sceneTris[i*3+0],sceneTris[i*3+1],sceneTris[i*3+2], clipAndTransform.m_cplane, clippedResults);
	}

#if __RASTERSTATS
	PF_FUNC(AddTriList);
#endif

	if ( m_singleThreadTransform && !m_chainTasks )
	{
		//m_tiler->tiles[0]->SetViewport(clipAndTransform);
		m_tiler->tiles[0]->AddTriList( m_ClippedSceneTris.begin(), m_ClippedSceneTris.GetCount()/3);

#if __RASTERSTATS
		PF_FUNC(Rasterize);
#endif
		rstCreateScanTileTasks(   m_tiler->tiles.begin(), 
			GetZBuffer(), 
			GetHiZBuffer(),
			RAST_HIZ_WIDTH,
			m_useSimple,
			m_useSmall,
			BANK_ONLY(m_drawBlock ||) false,
			&m_scanTask);
		m_scanTask.Start();
		m_scanTask.Wait();		
	}*/
}

void SoftRasterizer::Wait()
{
	if ( m_useSysDependencyTasks)
	{
		//Need to do this once we're done with the tasks so that we have the active frustum for the
		//whole buffer
		CombineResults();
		return;
	}

	if ( m_hasWaterTriDependancy != false){
		RemoveWaterTriDependancy(); // not fired off water tri task even though setup.
	}
	if ( m_hasStencilDependancy != false){
		RemoveStencilDependancy(); // not fired off stencil task even though setup.
	}
	m_stencilTask.Wait();
	m_generateWaterTrisTask.Wait();

	m_tilerTask.Wait();
	m_scanTask.Wait();
	m_selectorTask.Wait();
	m_modelSelectorTask.Wait();

	m_compareAndGenerateStencilTask.Wait();


	//Need to do this once we're done with the tasks so that we have the active frustum for the
	//whole buffer
	CombineResults();
}

void SoftRasterizer::WaitForScanTask()
{
	m_scanTask.Wait();
}

void SoftRasterizer::GenerateCompareStencilTask()
{
	sysTaskParameters p;
	tskSetInput( p,  GetHiZBuffer(), RAST_HIZ_SIZE_VEC );
	tskSetReadOnlyBuffer(p, m_tempHiZBuffer, RAST_HIZ_SIZE_VEC );
	p.Output.Data =  GetHiStencilBuffer();
	p.Output.Size = RAST_STENCIL_SIZE_BYTES;
	m_compareAndGenerateStencilTask.Add( TASK_INTERFACE(stencilcompareandgenerate), p);
}

void SoftRasterizer::SelectAndRasterizeModelOccluders(int numBuckets, const COccludeModelBucket* pBuckets, u8* isClipped,  float threshold, OccludeModel* resOccluders)
{
	if ( m_useSysDependencyTasks )
	{
		sysDependency& dep = m_modelSelectorDepTask;
		dep.Init(FRAG_SPU_CODE(modelselector_frag), 0 );
		dep.m_Priority = sysDependency::kPriorityMed;
		tskSetScratch( dep, 32*1024);
		tskSetInput( dep, 1, pBuckets, numBuckets );
		tskSetInput(dep, 2,  m_transform );

		ModelSelectorUserData ud;
		ud.threshold = threshold;
		ud.NumTilers = NumActiveTilers();
		ud.outputOccluders = resOccluders;
		ud.packets = m_tileModelPackets;
		ud.outputClipped = isClipped;
		tskSetUserData( dep, ud );
		return;
	}
	sysTaskParameters p;
	tskSetInput( p, pBuckets, numBuckets );
	tskSetReadOnly(p, m_transform );

#if __PS3
	// EJ: Increasing this because we are using more memory due to the new DMA calls
	p.Scratch.Size=48*1024;
#else
	p.Scratch.Size=32*1024;
#endif

	ModelSelectorUserData userData;
	userData.threshold = threshold;
	userData.NumTilers = NumActiveTilers();
	userData.outputOccluders = resOccluders;

	if ( m_waterOccluderCount != NULL)
		*m_waterOccluderCount=0;

	userData.outputWaterClipped=m_outputWaterClipped;
	userData.outputWaterOccluders=m_outputWaterOccluders;
	userData.waterOccluderCountEA = m_waterOccluderCount;

	userData.packets = m_tileModelPackets;
	userData.outputClipped = isClipped;
	m_modelSelectorTask.Add( TASK_INTERFACE(modelselector), p, userData);
}

//-- task dispatchers
void SoftRasterizer::SelectAndRasterizeBoxOccluders(int numBuckets, const CBoxOccluderBucket* pBuckets, u8* isClipped, int /*amtResBoxOccluders*/, float threshold, BoxOccluder* resBoxOccluders)
{
	if ( m_useSysDependencyTasks )
	{
		sysDependency& dep = m_selectorDepTask;
		dep.Init(FRAG_SPU_CODE(boxselector_frag), 0 );
		dep.m_Priority = sysDependency::kPriorityMed;
		tskSetScratch( dep, 32*1024);
		tskSetInput( dep, 1, pBuckets, numBuckets );
		tskSetInput(dep, 2,  m_transform );

		BoxSelectorUserData userData;
		userData.threshold = threshold;
		userData.NumTilers = NumActiveTilers();
		userData.outputOccluders = resBoxOccluders;
		userData.packets = m_tileBoxPackets;
		userData.outputClipped = isClipped;
		tskSetUserData( dep, userData );
		return;
	}

	sysTaskParameters p;
	tskSetInput( p, pBuckets, numBuckets );
	tskSetReadOnly(p, m_transform );

#if __PS3
	// EJ: Increasing this because we are using more memory due to the new DMA calls
	p.Scratch.Size=48*1024;
#else
	p.Scratch.Size=32*1024;
#endif

	BoxSelectorUserData userData;
	userData.threshold = threshold;
	userData.NumTilers = NumActiveTilers();
	userData.outputOccluders = resBoxOccluders;
	userData.packets = m_tileBoxPackets;
	userData.outputClipped = isClipped;
	m_selectorTask.Add( TASK_INTERFACE(boxselector), p, userData);
}

void SoftRasterizer::PrepareSubmitTilersDependency()
{
	int NumTilers = NumActiveTilers();

	// fire off this tasks as one unit
	m_postTilerTask.Init( FRAG_SPU_CODE(addTasks_frag), 0);
	m_postTilerTask.m_Priority = sysDependency::kPriorityMed;

	for (int i = 0; i < NumTilers; i++)
	{	
		sysDependency& dep=m_tilerDepTask[i];
		dep.Init(FRAG_SPU_CODE(boxoccluder_frag), 0 );
		dep.m_Priority = sysDependency::kPriorityMed;
		tskSetScratch( dep, 16*1024);
		tskSetInputOutput( dep, 1, m_tiler->tiles[i]);

		tskSetInput(dep, 2, m_tileBoxPackets[i]);
		tskSetInput( dep, 3, m_tileModelPackets[i]);
		tskSetInput( dep, 4,  m_transform );

		BoxOccluderUserData ud;
		ud.bitPrecision = OCCLUDE_MODEL_DEFAULT_ERROR;
		ud.tilerOutput = m_tiler->tiles[i];
		tskSetUserData( dep, ud );

		m_postTilerTask.AddChild( dep);
	}
}

DependantTaskList* SoftRasterizer::PrepareSubmitBoxOccluders() 
{	
	int NumTilers = NumActiveTilers();
	for (int i = 0; i < NumTilers; i++)
	{
		sysTaskParameters p;
#if __PS3
		p.Scratch.Size=48*1024;
#else
		p.Scratch.Size=32*1024;
#endif
		p.SpuStackSize=32*1024;

		p.Input.Data = m_tiler->tiles[i];
		p.Input.Size=sizeof(rstFastTiler);
		
		tskSetReadOnly( p, m_tileBoxPackets[i]);
		tskSetReadOnly( p, m_tileModelPackets[i]);
		tskSetReadOnly( p, m_transform );
				
		p.UserData[UD_TILER_EA].asPtr=p.Input.Data; // store effective address;
		p.UserData[UD_BITPRECISION_EA].asFloat = OCCLUDE_MODEL_DEFAULT_ERROR;
		p.UserData[UD_MODELWORK_CNT_EA].asPtr =m_modelWorkCount;
		*m_modelWorkCount=0;
		p.UserDataCount = UA_TOTAL;			
		m_tilerTask.Add(TASK_INTERFACE(boxoccluder), p );
	}
	return &m_tilerTask;
}

void SoftRasterizer::PrepareSubmitGenerateWaterTris(
	Vec3V* verts,
	u32* numVerts)
{
	Assert( !m_useSysDependencyTasks );
	Assert( !m_generateWaterTrisTask.IsSetup());

	int NumTilers = NumActiveTilers();
	for (int i = 0; i < NumTilers; i++)
	{
		sysTaskParameters  p;
		p.Input.Data = m_tiler->tiles[i];
		p.Input.Size = sizeof(rstFastTiler);

		GenerateWaterTrisUserData ud( i, verts, numVerts );
		ud.tilerEA = p.Input.Data;

#if __PS3
		p.Scratch.Size=(48+16)*1024;
#else
		p.Scratch.Size=32*1024;
#endif
		p.SpuStackSize = (48)*1024;

		m_generateWaterTrisTask.Add( TASK_INTERFACE(generatewatertris), p, ud);
	}

	AddWaterTriDependancy();
}

void SoftRasterizer::RasterizeStencilModel(
	const grcViewport& view,
	Vec3V*	verts, 	
	u32*  numVerts,
	bool /*requiresClipping*/,
	bool clearToAllVisible
#if STENCIL_WRITER_DEBUG
	, bool debugEnableWaterStencil
	, bool debugSimpleClipper
	, int debugBoundsExpandX0
	, int debugBoundsExpandY0
	, int debugBoundsExpandX1
	, int debugBoundsExpandY1
	, float debugPixelOffsetX
	, float debugPixelOffsetY
#endif // STENCIL_WRITER_DEBUG
	)
{
	Assert( !m_useSysDependencyTasks );
	Assert( !m_stencilTask.IsSetup());
	*m_stencilTransform =rstTransformToScreenSpace( &view, true);
	
	sysTaskParameters  p;
	
	if ( m_useTempHiZBufferForStencil ){
		p.Input.Data = m_tiler->m_waterTiler;
		p.Input.Size = sizeof(rstFastTiler);
		p.Output.Data = m_tempHiZBuffer;
		p.Output.Size = RAST_HIZ_SIZE_BYTES;
	}
	else
	{
		p.Input.Data = m_HiZBuffer;
		p.Input.Size = RAST_HIZ_SIZE_BYTES;
		p.Output.Data = m_StencilBuffer;
		p.Output.Size = RAST_STENCIL_SIZE_BYTES;
		tskSetReadOnly( p, m_tiler->m_waterTiler);
	}
	
	tskSetReadOnly( p, *m_stencilTransform);
	StencilWriterUserData ud( verts, numVerts, OCCLUDE_MODEL_DEFAULT_ERROR, m_tileModelPackets,m_useTempHiZBufferForStencil, clearToAllVisible );

#if STENCIL_WRITER_DEBUG
	ud.debugEnableWaterStencil = debugEnableWaterStencil;
	ud.debugSimpleClipper = debugSimpleClipper;
	ud.debugBoundsExpandX0 = debugBoundsExpandX0;
	ud.debugBoundsExpandY0 = debugBoundsExpandY0;
	ud.debugBoundsExpandX1 = debugBoundsExpandX1;
	ud.debugBoundsExpandY1 = debugBoundsExpandY1;
	ud.debugPixelOffsetX = debugPixelOffsetX;
	ud.debugPixelOffsetY = debugPixelOffsetY;
#endif // STENCIL_WRITER_DEBUG

#if __PS3
	p.Scratch.Size=(48+32)*1024;
#else
	p.Scratch.Size=32*1024;
#endif

	ud.outputWaterClipped=m_outputWaterClipped;
	ud.outputWaterOccluders=m_outputWaterOccluders;
	ud.waterOccluderCountEA = m_waterOccluderCount;

	p.SpuStackSize = (24)*1024;
	m_stencilTask.Add( TASK_INTERFACE(stencilwriter), p, ud);
	
	if ( m_useTempHiZBufferForStencil)
		GenerateCompareStencilTask();

	AddStencilDependancy();
}

#if __BANK
void ResolveZBuffer( u8* res, bool convertToColor, float s, float e, 
					bool showHiZ, const Vec4V* hiZbuffer, const Vec4V* zBuffer )
{
#if __RASTERSTATS
	PF_FUNC(Resolve);
#endif

	ScalarV sv = ScalarVFromF32(s);
	ScalarV ev = ScalarVFromF32(e);
	ScalarV scalev = Invert(ev - sv);

	int step =2;
	int sw = RAST_SCREEN_WIDTH;
	int sh = RAST_SCREEN_HEIGHT;

	if ( showHiZ || !zBuffer )
	{
		int stride = sw*2;
		int vstride = stride/4;
		for (int y =0; y < sh; y+=step)
		{
			int cnt = y * 2 * stride;
			//Vec4V* vres = (Vec4V*)(res + cnt);
			u32* vres = (u32*)(res + cnt);
			for ( int x = 0; x < sw; x+=step)
			{

				Vec4V v = hiZbuffer[ y * sw/(step*step) + x/step];
				v = Invert(v);

				v = Saturate( (v-Vec4V(sv)) * Vec4V(scalev) );
				v = Vec4V(V_ONE) - v;
				v = FloatToIntRaw<8>(v);
#if __XENON || __PS3		
				v =  Vec4V( Vec::V4BytePermute<
					3,3,7,7,
					11,11,15,15,
					3,7,11,15,
					3,7,11,15>(v.GetIntrin128()));
				u32 r1 =  v.GetXi();
				u32 r2 =  v.GetYi();
#else
				u32 v0 = v.GetXi() | v.GetXi()<<8;
				u32 v1 = v.GetYi() | v.GetYi()<<8;
				u32 v2 = v.GetZi() | v.GetZi()<<8;
				u32 v3 = v.GetWi() | v.GetWi()<<8;

				u32 r1 = v0 | (v1<<16);
				u32 r2 = v2 | (v3<<16);										
#endif

				vres[vstride*0] = r1;
				vres[vstride*1] = r1;
				vres[vstride*2] = r2;
				vres[vstride*3] = r2;


				vres++;			
			}
		}
		return;
	}

	int tileWidthPow2 = 5;
	int tileWidth = 1<< tileWidthPow2;
	int tileWidthMask = tileWidth-1;
	int tileAcross = 4;

	int cnt =0;
	for (int y =0; y < RAST_SCREEN_HEIGHT; y++)
	{
		int tty = (int)(floorf((float)(y)/((float)RAST_TILE_HEIGHT)));
		int ity = y - tty*RAST_TILE_HEIGHT;

		for ( int x = 0; x < RAST_SCREEN_WIDTH; x++)
		{
			Vec4V v;

			int ttx = (x>>tileWidthPow2);
			int ttile = ttx + tty * tileAcross;
			int itx = x&tileWidthMask;

			int index = ity * tileWidth + itx;
			v = *(zBuffer+index + ttile * tileWidth*RAST_TILE_HEIGHT);

			if ( convertToColor )
			{
				v = Invert(v);
				v = Saturate( (v-Vec4V(sv)) * Vec4V(scalev) );
				v = Vec4V(V_ONE) - v;
				v = FloatToIntRaw<8>(v);
#if __XENON || __PS3
				v =  Vec4V( Vec::V4BytePermute<
					3,3,3,3,
					7,7,7,7,
					11,11,11,11,
					15,15,15,15>(v.GetIntrin128Ref()) );
#endif
			}

			res[cnt*2] = (u8)v.GetXi();
			res[cnt*2+1] = (u8)v.GetYi();

			res[cnt*2 + RAST_SCREEN_WIDTH*2] = (u8)v.GetZi();
			res[cnt*2 + RAST_SCREEN_WIDTH*2+1] = (u8)v.GetWi();
			cnt++;
		}
		cnt += RAST_SCREEN_WIDTH;
	}
}

void ResolveStencil( u8* res,  const Vec4V* stencilBuffer)
{
#if __RASTERSTATS
	PF_FUNC(Resolve);
#endif

	int cnt =0;		
	for (int y =0; y < RAST_SCREEN_HEIGHT; y++)
	{
	    u32* row =(u32*) &stencilBuffer[y];
		for ( int x = 0; x < RAST_SCREEN_WIDTH; x++)
		{
			Assert( x <= 128);
			int val = row[ x>>5];
			val = (val>>(31-(x&31)))&1;

			val = val >0 ? 255 : 0;
			
			res[cnt*2] = (u8)val;
			res[cnt*2+1] = (u8)val;

			res[cnt*2 + RAST_SCREEN_WIDTH*2] = (u8)val;
			res[cnt*2 + RAST_SCREEN_WIDTH*2+1] = (u8)val;
			cnt++;
		}
		cnt += RAST_SCREEN_WIDTH;
	}
}

void SoftRasterizer::CopyToTexture(bool /*popZ*/ )
{
	grcTextureLock lock;
	m_DebugDrawTexture->LockRect( 0, 0, lock );
	Assert( lock.Pitch == RAST_SCREEN_WIDTH_PIXELS * 1);
	Assert( lock.BitsPerPixel == 8 );
	Assert( lock.Width == RAST_SCREEN_WIDTH_PIXELS );
	Assert( lock.Height == RAST_SCREEN_HEIGHT_PIXELS);

	if ( m_showStencil ){
		if ( m_showWaterZBuffer && m_tempHiZBuffer != NULL)
			ResolveZBuffer( (u8*)lock.Base, true, m_depthStart, m_depthEnd, m_showHiZ, m_tempHiZBuffer, GetZBuffer() );
		else
			ResolveStencil(  (u8*)lock.Base, m_StencilBuffer);
	}
	else
		ResolveZBuffer( (u8*)lock.Base, true, m_depthStart, m_depthEnd, m_showHiZ, GetHiZBuffer(), GetZBuffer() );

	m_DebugDrawTexture->UnlockRect(lock);
}
void SoftRasterizer::GetDebugExtents( float& sX, float& sY, float& eX, float& eY )
{
	float startX=0;
	float startY=0;
	float endY=(float)RAST_SCREEN_HEIGHT_PIXELS*m_viewScale;
	float endX=(float)RAST_SCREEN_WIDTH_PIXELS*m_viewScale;

	if ( m_depthFullscreen)
	{
		endY = (float)GRCDEVICE.GetHeight();
		endX = (float)GRCDEVICE.GetWidth();
	}
	else if ( m_depthOverlapShadowTarget )
	{
		// Note: these should match, respectively, the offsets defined in CCSMDebugGlobals::DebugDraw
		// and the cascade sizes defined in cascadeshadows_common.fxh
		const int cascadeSize = RSG_PS3? 640 : 512;
		startX = 50;
		startY = 32;
		endX = startX + cascadeSize;
		endY = startY + cascadeSize;
	}
	sX = startX;
	sY = startY;
	eX = endX;
	eY = endY;
}

void SoftRasterizer::DisplayStats() const
{
	Displayf("Mem Usuage ");
	Displayf("___________");
	int tilerSize= 0;
	int alltilerSize = 0;
	int zbuffSize = m_ZBuffer ? sizeof(Vec4V)*RAST_SCREEN_SIZE : 0;	
	int HiZbuffSize = sizeof(Vec4V)*RAST_HIZ_SIZE_VEC;
	int debugTexMem = m_DebugDrawTexture->GetWidth()* m_DebugDrawTexture->GetHeight()*(m_DebugDrawTexture->GetBitsPerPixel()/8);

	char fValue[128], fValue2[128];

	if (m_tiler)
	{
		tilerSize= rstFastTiler::GetTileMemSizeBytes();
		alltilerSize = tilerSize *RAST_NUM_TILERS_ACTIVE;
		Displayf("Tiler Size %s Num Tilers %i Overall Tiler Size %s", 
			prettyprinter(fValue2, sizeof(fValue2), (s64)tilerSize) , RAST_NUM_TILERS_ACTIVE, 
			prettyprinter(fValue, sizeof(fValue), (s64) alltilerSize));
	}

	if (m_ZBuffer)
	{
		Displayf("Z Buffer Width  %i Height %i Size in Bytes %s ", RAST_SCREEN_WIDTH_PIXELS, RAST_SCREEN_HEIGHT_PIXELS,
							prettyprinter(fValue, sizeof(fValue), (s64) zbuffSize) );
	}
	Displayf("Hi Z Buffer Width  %i Height %i Size in Bytes %s ", RAST_HIZ_WIDTH*2, RAST_HIZ_HEIGHT*2, 
						prettyprinter(fValue, sizeof(fValue), (s64)HiZbuffSize ));
	Displayf("General Crap Bytes %" SIZETFMT "d ", sizeof(*this) );
	Displayf("Debug Tex mem %s", prettyprinter(fValue, sizeof(fValue), (s64)debugTexMem));
	Displayf("Total Mem Usage %s (Bank Builds)", 
						 prettyprinter(fValue, sizeof(fValue), debugTexMem +sizeof(*this) + alltilerSize +  zbuffSize + HiZbuffSize));

	Displayf("Total Mem Usage %s (Assert builds)", 
		prettyprinter(fValue, sizeof(fValue), sizeof(*this) + alltilerSize +  zbuffSize + HiZbuffSize));

	Displayf("Total Mem Usage %s ", 
		prettyprinter(fValue, sizeof(fValue), sizeof(*this) + alltilerSize +   HiZbuffSize));
}

void SoftRasterizer::DebugDraw(bool popZ )
{
	if ( !m_debugDraw)
	{
		return;
	}
	CopyToTexture(popZ);

	static grcBlendStateHandle hOverlayBlendState = grcStateBlock::BS_Invalid;
	static grcBlendStateHandle hStencilBlendState = grcStateBlock::BS_Invalid;

	if(hOverlayBlendState == grcStateBlock::BS_Invalid)
	{
		grcBlendStateDesc bsDesc;		
		bsDesc.BlendRTDesc[0].BlendEnable=true;
		bsDesc.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVBLENDFACTOR;
		bsDesc.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_BLENDFACTOR;
		hOverlayBlendState = grcStateBlock::CreateBlendState(bsDesc);
		Assert(hOverlayBlendState != grcStateBlock::BS_Invalid);
	}

	if(hStencilBlendState == grcStateBlock::BS_Invalid)
	{
		grcBlendStateDesc bsDesc;		
		bsDesc.BlendRTDesc[0].BlendEnable=true;
		bsDesc.BlendRTDesc[0].DestBlend = grcRSV::BLEND_ONE;
		bsDesc.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_BLENDFACTOR;
		hStencilBlendState = grcStateBlock::CreateBlendState(bsDesc);
		Assert(hStencilBlendState != grcStateBlock::BS_Invalid);
	}

	Color32 blendFactor=Color32(m_depthAlpha,m_depthAlpha,m_depthAlpha,m_depthAlpha);
	grcStateBlock::SetBlendState( m_showStencil && !m_showWaterZBuffer ? hStencilBlendState : hOverlayBlendState, blendFactor.GetColor(), grcStateBlock::ActiveSampleMask);
	grcStateBlock::SetRasterizerState( grcStateBlock::RS_NoBackfaceCull);
	grcStateBlock::SetDepthStencilState(grcStateBlock::DSS_IgnoreDepth);
	grcWorldIdentity();
	grcLightState::SetEnabled(false);

	float startX, startY, endX, endY;
	GetDebugExtents( startX, startY, endX, endY );
	
	grcBindTexture(m_DebugDrawTexture);


	PUSH_DEFAULT_SCREEN();
	grcDrawSingleQuadf(startX,startY,endX,endY,0.0f,0.0f,0.0f,1.0f,1.0f,Color32(255,255,255));
	POP_DEFAULT_SCREEN();
	grcBindTexture(0);

	grcStateBlock::SetBlendState( grcStateBlock::BS_Default);
}

void SoftRasterizer::DebugDrawActiveFrustum()
{
	PUSH_DEFAULT_SCREEN();
	grcWorldIdentity();

	float startX, startY, endX, endY;
	GetDebugExtents( startX, startY, endX, endY );
	//static bool bUseTileScale = false;
	Vec3V	rescale(endX/(float)(RAST_HIZ_WIDTH),endY/(float)(RAST_HIZ_HEIGHT),-1.0f);//( 1.0/256.0f,1.0f/144.0f, 1.0f);
	rstDrawScBounds(Vec3V(minMaxBoundCombined.GetXY(), ScalarV(V_FLT_SMALL_1)) * rescale, 
		Vec3V(minMaxBoundCombined.GetZW(), ScalarV(V_FLT_SMALL_1)) * rescale, 
		Color32(255,255,0)
		);

	POP_DEFAULT_SCREEN();
}

void SoftRasterizer::AddWidgets( bkBank& bank )
{
	bank.PushGroup("Rasterizer");

	bank.PushGroup("System");
	bank.AddButton("Dump Info", datCallback(MFA(SoftRasterizer::DisplayStats),this));
	bank.AddToggle("m_useSimple",&m_useSimple);
	bank.AddToggle("m_useSmall",&m_useSmall);
	bank.AddToggle("dumpTaskGraph", &m_dumpTaskGraph);
	bank.AddToggle("use sys dependencies", &m_useSysDependencyTasks);
	bank.AddToggle("use temp HI Z buffer for stencil", &m_useTempHiZBufferForStencil);
	bank.PopGroup();

	bank.PushGroup("Draw Frame Buffer");
	bank.AddToggle("Debug Draw", &m_debugDraw);
	bank.AddSlider("View Scale", &m_viewScale, 0.0f,8.0f,0.1f);

	bank.AddToggle("m_showStencil",&m_showStencil);
	bank.AddToggle("m_showWaterZBuffer", &m_showWaterZBuffer);
#if ALLOC_ZBUFFER
	bank.AddToggle("m_showHiZ", &m_showHiZ);
#endif
	bank.AddSlider("depth Start", &m_depthStart, 0.0f,1.0f,0.0001f);
	bank.AddSlider("depth End", &m_depthEnd, 0.0f,1.0f,0.0001f);
	bank.AddSlider("alpha", &m_depthAlpha, 0.0f,1.0f,0.0001f);
	bank.AddToggle("overlay", &m_depthFullscreen);
	bank.AddToggle("overlay shadow target", &m_depthOverlapShadowTarget);
	
	bank.PopGroup();

	if (m_tiler)
		m_tiler->AddWidgets(bank);

	bank.PopGroup();
}

void rstDrawScBounds( Vec3V_In bmin, Vec3V_In bmax, Color32 col )
{
	grcBegin(drawLines,8);

	Vector3 c1 = VEC3V_TO_VECTOR3(bmin);
	Vector3 c3 = VEC3V_TO_VECTOR3(bmax);
	Vector3 c2 = Vector3(c1.x, c3.y, c1.z);
	Vector3 c4 = Vector3(c3.x, c1.y, c1.z);

	grcColor(col);
	grcVertex3f(c1);	
	grcVertex3f(c2);

	grcVertex3f(c2);
	grcVertex3f(c3);	

	grcVertex3f(c3);	
	grcVertex3f(c4);

	grcVertex3f(c4);
	grcVertex3f(c1);
	grcEnd();
}


void SoftRasterizer::DrawWaterOccluderMeshes( bool  depthOnly)
{
	if ( !m_outputWaterOccluders)
			return;

	int amount = *m_waterOccluderCount;
	for (int i=0; i< amount; i++)
	{
		m_outputWaterOccluders[i].Draw( OCCLUDE_MODEL_DEFAULT_ERROR, false, !depthOnly );
	}
	
}

void SoftRasterizer::DumpWaterOccluderMeshes()
{
	if ( !m_outputWaterOccluders)
		return;

	int amt = *m_waterOccluderCount;
	Displayf("Num of Water Occluder Models %i ", amt);
	int numVerts = 0;
	int numTris=0;
	OccludeModel* models=m_outputWaterOccluders;
	for(int i=0; i<amt; i++){
		numVerts += models[i].GetNumVerts();
		numTris += models[i].GetNumTris();
	}
	Displayf("Total Tris %i Total Verts %i", numTris, numVerts );
}

#endif // __BANK

void SoftRasterizer::Reset()
{
	m_tiler->Reset();
}

void SoftRasterizer::CombineResults()
{
	minZ=FLT_MAX;
	float* nearZFloat = (float*)nearZTiles;
	Vec2V minBounds(V_FLT_MAX);
	Vec2V maxBounds(V_NEG_FLT_MAX);
	for(int i=0; i<RAST_NUM_TILES; i++)
	{
		minBounds = Min(minMaxBounds[i].GetXY(), minBounds);
		maxBounds = Max(minMaxBounds[i].GetZW(), maxBounds);
		minZ = Min( nearZFloat[i], minZ);
	}

	//Making sure we get correct results and so changing the conditions slightly.
	//If anything is out of bounds then result will get set to -1
	VecBoolV minBoundsCondition = And(IsLessThan(minBounds, Vec2V(V_FLT_LARGE_8)), IsGreaterThanOrEqual(minBounds, Vec2V(V_ZERO)));
	VecBoolV maxBoundsCondition = And(IsGreaterThan(maxBounds, Vec2V(V_NEGTWO)), IsLessThan(maxBounds, Vec2V(V_FLT_LARGE_8)));
	//Slightly increase the bounds to take care of precision errors
	Vec2V newMinBounds = SelectFT(minBoundsCondition, Vec2V(V_NEGONE), minBounds) - Vec2V(V_FLT_SMALL_4);
	Vec2V newMaxBounds = SelectFT(maxBoundsCondition, Vec2V(V_NEGONE), maxBounds) + Vec2V(V_FLT_SMALL_4);
	minMaxBoundCombined = Vec4V(newMinBounds, newMaxBounds);
	//Displayf("Min Bounds: %f %f ------- Max Bounds: %f %f", newMinBounds.GetXf(), newMinBounds.GetYf(), newMaxBounds.GetXf(), newMaxBounds.GetYf()  );
}

void SoftRasterizer::SetWaterOccluderResources( OccludeModel* waterOccluders, u8* waterClipped )
{
	m_outputWaterOccluders = waterOccluders;
	m_outputWaterClipped = waterClipped;	
}

} // namespace rage

