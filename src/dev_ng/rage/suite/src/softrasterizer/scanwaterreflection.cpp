// ======================================
// softrasterizer/scanwaterreflection.cpp
// (c) 2013 RockstarNorth
// ======================================

#include "softrasterizer/scanwaterreflection.h"

#include "math/amath.h"
#include "math/vecshift.h" // for BIT64
#include "system/memops.h"
#include "system/nelem.h"

#include "fwutil/xmacro.h"

namespace rage {

void ScanWaterReflection(rstScanWaterReflectionParamsOut& paramsOut, const rstScanWaterReflectionParamsIn& paramsIn)
{
	const int w = 128; // width of stencil buffer
	const int h = 72; // height of stencil buffer

	const Vec4V* stencil = paramsIn.m_stencil;
	spdkDOP2D_8& visBounds = paramsOut.m_visBounds;

	sysMemSet(&paramsOut, 0, sizeof(paramsOut));

#if __BANK
	if (paramsIn.m_debugFlags & SCAN_WATER_REFLECTION_DEBUG_REMOVE_ISOLATED_STENCIL)
	{
		static Vec4V stencil2[h];

		for (int j = 0; j < h; j++)
		{
			stencil2[j] = stencil[j] & (stencil[Max<int>(0, j - 1)] | stencil[Min<int>(j + 1, h - 1)]);
		}

		stencil = stencil2;
	}
#endif // __BANK

	const int mode = BANK_SWITCH(paramsIn.m_mode, WRVB_DEFAULT);

	if (mode == WRVB_NONE)
	{
		visBounds.SetIdentity();
	}
	else if (mode == WRVB_WATER_QUADS || mode == WRVB_WATER_QUADS_USE_OCCLUSION)
	{
		// handled in UpdateViewport
	}
	else if (mode == WRVB_STENCIL)
	{
		visBounds.SetEmpty();

		const u32* stencilBits = (const u32*)stencil;

		if (stencilBits)
		{
			const int stencilStride = (w + 31)/32;
			bool bIsEmpty = true;

			for (int j = 0; j < h; j++)
			{
				const u32* stencilRow = (u32*) &stencilBits[j*stencilStride];

				for (int i = 0; i < w; i++)
				{
					if (stencilRow[i/32] & BIT(31 - i%32))
					{
						visBounds.AddPoint(Vec2V((float)i, (float)j));
						bIsEmpty = false;
					}
				}
			}

			if (!bIsEmpty) // expand by one pixel in the +x and +y directions
			{
				visBounds.AddToAxis(Vec4V(Vec2V(V_ONE), Vec2V(V_ZERO)));
				visBounds.AddToDiag(Vec4V(ScalarV(V_TWO), Vec2V(V_ONE), ScalarV(V_ZERO)));
			}
		}
	}
	else if (mode == WRVB_STENCIL_FAST)
	{
		visBounds.SetEmpty();

		if (stencil)
		{
			Vec4V mask = Vec4V(V_ZERO);
			int j0 = -1;
			int j1 = -1;

			for (int j = 0; j < h; j++)
			{
				const Vec4V row = stencil[j];

				if (!IsEqualIntAll(row, Vec4V(V_ZERO)))
				{
					mask |= row;

					if (j0 == -1)
					{
						j0 = j;
					}

					j1 = j;
				}
			}

			if (j0 != -1)
			{
				const u32* stencilRow = (const u32*)&mask;
				int i0 = -1;
				int i1 = -1;

				for (int i = 0; i < w; i++)
				{
					if (stencilRow[i/32] & BIT64(31 - i%32))
					{
						if (i0 == -1)
						{
							i0 = i;
						}

						i1 = i;
					}
				}

				visBounds.AddPoint(Vec2V((float)i0, (float)j0));
				visBounds.AddPoint(Vec2V((float)i1, (float)j0));
				visBounds.AddPoint(Vec2V((float)i0, (float)j1));
				visBounds.AddPoint(Vec2V((float)i1, (float)j1));

				// expand by one pixel in the +x and +y directions
				visBounds.AddToAxis(Vec4V(Vec2V(V_ONE), Vec2V(V_ZERO)));
				visBounds.AddToDiag(Vec4V(ScalarV(V_TWO), Vec2V(V_ONE), ScalarV(V_ZERO)));
			}
		}
	}
	else if (mode == WRVB_STENCIL_VECTORISED)
	{
		visBounds.SetEmpty();

		int j0 = -1;
		int j1 = -1;

		for (int j = 0; j < h; j++) // find top row
		{
			if (!IsEqualIntAll(stencil[j], Vec4V(V_ZERO)))
			{
				j0 = j;
				break;
			}
		}

		for (int j = h - 1; j >= 0; j--) // find bottom row
		{
			if (!IsEqualIntAll(stencil[j], Vec4V(V_ZERO)))
			{
				j1 = j;
				break;
			}
		}

		if (j0 != -1)
		{
			Vec4V i0_top_mask = stencil[j0];
			Vec4V i1_top_mask = i0_top_mask;
			Vec4V mid_mask    = i0_top_mask;
			Vec4V i0_bot_mask = stencil[j1];
			Vec4V i1_bot_mask = i0_bot_mask;
#if RSG_CPU_PPC
			const Vec::Vector_4V one = ScalarVConstant<0x01010101>().GetIntrin128();
			Vec::Vector_4V shift;
#else
			const u32 one = 1;
			u32 shift;
#endif
			shift = one;

			for (int j = j0 + 1; j <= j1; j++)
			{
				const Vec::Vector_4V s = stencil[j].GetIntrin128();

				i0_top_mask |= Vec4V(Vec::V4ShiftRightBits128(s, shift));//j - j0));
				i1_top_mask |= Vec4V(Vec::V4ShiftLeftBits128(s, shift));//j - j0));
				mid_mask    |= Vec4V(s);
#if RSG_CPU_PPC
				shift = __altivec_vaddubm(shift, one);
#else
				shift += one;
#endif
			}

			shift = one;

			for (int j = j1 - 1; j >= j0; j--)
			{
				const Vec::Vector_4V s = stencil[j].GetIntrin128();

				i0_bot_mask |= Vec4V(Vec::V4ShiftRightBits128(s, shift));//j - j0));
				i1_bot_mask |= Vec4V(Vec::V4ShiftLeftBits128(s, shift));//j - j0));
#if RSG_CPU_PPC
				shift = __altivec_vaddubm(shift, one);
#else
				shift += one;
#endif
			}

			const u32 i0_top =       CountLeadingZeros128 (i0_top_mask);
			const u32 i1_top = 127 - CountTrailingZeros128(i1_top_mask);
			const u32 i0_mid =       CountLeadingZeros128 (mid_mask);
			const u32 i1_mid = 127 - CountTrailingZeros128(mid_mask);
			const u32 i0_bot =       CountLeadingZeros128 (i0_bot_mask);
			const u32 i1_bot = 127 - CountTrailingZeros128(i1_bot_mask);

			const float x0_top = (float)(i0_top + 0);
			const float x1_top = (float)(i1_top + 1);
			const float x0_mid = (float)(i0_mid + 0);
			const float x1_mid = (float)(i1_mid + 1);
			const float x0_bot = (float)(i0_bot + 0);
			const float x1_bot = (float)(i1_bot + 1);
			const float y0     = (float)(j0     + 0);
			const float y1     = (float)(j1     + 1);

			const float px   = +x1_mid;
			const float py   = +y1;
			const float nx   = -x0_mid;
			const float ny   = -y0;
			const float pxpy = +x1_bot + y1;
			const float nxpy = -x0_bot + y1;
			const float pxny = +x1_top - y0;
			const float nxny = -x0_top - y0;

			const Vec4V temp[] = { Vec4V(px,py,nx,ny), Vec4V(pxpy,nxpy,pxny,nxny) };

			sysMemCpy(&visBounds, temp, sizeof(temp));
		}
	}

	// ============================================================================================

#if __PS3
	for (int i = 0; i < NELEM(paramsOut.m_edgeClipPlanes); i++)
	{
		paramsOut.m_edgeClipPlanes[i] = Vec4V(V_ZERO);
	}

	paramsOut.m_edgeClipPlaneCount = 0;
#endif // __PS3

#if __BANK
	paramsOut.m_debugWaterPlaneIndex = -1;
	paramsOut.m_debugNearPlaneIndex = -1;
#endif // __BANK

	// TODO -- once we get the vis bounds cull planes working, incorporate the farClipPlane .. probably replace the smallest plane
	// (this is only important if the far clip is set to something other than zero however, so far i have not seen this to be the case)
	const Vec2V pointScale = Vec2V(2.0f/(float)w, -2.0f/(float)h);
	const Vec2V pointBias = Vec2V(-1.0f, 1.0f);
	Vec2V points[8];
	int numPoints = visBounds.GetUniquePoints(points, ScalarV(V_FLT_SMALL_4));

	if (numPoints >= 3)
	{
		Vec3V worldPoints[NELEM(points)];
		Mat44V invCompMat;
		InvertFull(invCompMat, paramsIn.m_gameVPCompositeMtx);

		for (int i = 0; i < numPoints; i++)
		{
			const Vec2V point = AddScaled(pointBias, pointScale, points[i]); // -> [-1..1]

			//Assert(MaxElement(Abs(points[i] - points[(i + 1)%numPoints])).Getf() > 0.0001f);
			worldPoints[i] = rstTransformProjective(invCompMat, Vec3V(point, ScalarV(V_ONE)));
		}

		for (int i = 0; i < numPoints; i++)
		{
			const Vec3V worldEdge1 = paramsIn.m_cameraPos - worldPoints[i];
			const Vec3V worldEdge0 = paramsIn.m_cameraPos - worldPoints[(i + 1)%numPoints];

			const Vec3V p = rstPlaneReflect      (paramsIn.m_waterPlane, paramsIn.m_cameraPos);
			const Vec3V n = rstPlaneReflectVector(paramsIn.m_waterPlane, Normalize(Cross(worldEdge0, worldEdge1)));

			paramsOut.m_reflectedPlanes[i] = rstBuildPlane(p, n);
#if __PS3
			if (IsGreaterThanAll(MinElement(Abs(points[i] - points[(i + 1)%numPoints])), ScalarV(V_ZERO))) // diagonal plane?
			{
				if (AssertVerify(paramsOut.m_edgeClipPlaneCount < NELEM(paramsOut.m_edgeClipPlanes)))
				{
					paramsOut.m_edgeClipPlanes[paramsOut.m_edgeClipPlaneCount++] = paramsOut.m_reflectedPlanes[i];
				}
			}
#endif // __PS3
		}

		if (numPoints < NELEM(paramsOut.m_reflectedPlanes) && (paramsIn.m_controlFlags & SCAN_WATER_REFLECTION_CONTROL_EXTRA_CULL_PLANE_WATER)) // add the near plane if we can, why not
		{
#if __BANK
			paramsOut.m_debugWaterPlaneIndex = numPoints;
#endif // __BANK
			paramsOut.m_reflectedPlanes[numPoints++] = -paramsIn.m_waterPlane; // water plane is pointed upwards, but we want to cull objects underneath it
#if __PS3
			if (paramsOut.m_edgeClipPlaneCount < NELEM(paramsOut.m_edgeClipPlanes))
			{
				paramsOut.m_edgeClipPlanes[paramsOut.m_edgeClipPlaneCount++] = paramsOut.m_reflectedPlanes[numPoints - 1];
			}
#endif // __PS3
		}

		if (numPoints < NELEM(paramsOut.m_reflectedPlanes) && (paramsIn.m_controlFlags & SCAN_WATER_REFLECTION_CONTROL_EXTRA_CULL_PLANE_NEAR)) // add the near plane if we can, why not
		{
#if __BANK
			paramsOut.m_debugNearPlaneIndex = numPoints;
#endif // __BANK
			paramsOut.m_reflectedPlanes[numPoints++] = paramsIn.m_nearPlane;
#if __PS3
			if (paramsOut.m_edgeClipPlaneCount < NELEM(paramsOut.m_edgeClipPlanes))
			{
				paramsOut.m_edgeClipPlanes[paramsOut.m_edgeClipPlaneCount++] = paramsOut.m_reflectedPlanes[numPoints - 1];
			}
#endif // __PS3
		}

		for (int i = numPoints; i < NELEM(paramsOut.m_reflectedPlanes); i++)
		{
			paramsOut.m_reflectedPlanes[i] = Vec4V(V_ZERO);
		}
	}
	else
	{
		// one plane way up above the clouds pointing towards space culls everything
		paramsOut.m_reflectedPlanes[0] = Vec4V(Vec3V(V_Z_AXIS_WONE), ScalarV(V_FLT_LARGE_8));

		for (int i = 1; i < NELEM(paramsOut.m_reflectedPlanes); i++)
		{
			paramsOut.m_reflectedPlanes[i] = Vec4V(V_ZERO);
		}

		// force water reflection to render sky-only
		paramsOut.m_outputFlags |= SCAN_WATER_REFLECTION_OUT_NO_STENCIL;
	}
}

} // namespace rage
