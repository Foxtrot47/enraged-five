// 
// softrasterizer/scan.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "scan.h"
#include "tiler.h" // for StencilWriterUserData
#include "profile/profiler.h"
#include "tsklib.h"
#include "grprofile/pix.h"
#include "system/dependency.h"

#ifndef RST_QUERY_ONLY
#define RST_QUERY_ONLY 0
#endif

#define SCAN_SPEW_CRAZY	0

namespace rage
{


enum DMA_TAGS
{
	TAG_LOADINDICES = 2,
	TAG_LOADTRI_0=3,
	TAG_LOADTRI_1=4,
	TAG_LOADTRI_2=5,
	TAG_LOADTRI_3=6,
	TAG_LOADTRI_4=7,
	TAG_LOADTRI_5=8,
	TAG_LOADTRI_6=9,
	TAG_LOADTRI_7=10,
	TAG_LOADCNTTABLE_8=11,

	TAG_WRITEHIZ_0=12,
	TAG_WRITEHIZ_1=13,
};




#if __RASTERSTATS
	namespace RasterStats
	{
		PF_PAGE(RastPage,"Raster Stats");

		PF_GROUP(RastGeneric);
		PF_LINK(RastPage, RastGeneric);
		PF_TIMER(Rasterize, RastGeneric);
		PF_TIMER(Resolve, RastGeneric);
		PF_TIMER(AddTriList, RastGeneric);
		PF_TIMER(Queries, RastGeneric);
		PF_TIMER(SelectOccluders, RastGeneric);
		PF_TIMER(SelectModelOccluders, RastGeneric);
		PF_TIMER(Transform, RastGeneric);
		PF_TIMER(GenEdges, RastGeneric);
		PF_TIMER(GenerateStencil, RastGeneric);
		PF_TIMER(GenerateWaterTris, RastGeneric);
		PF_TIMER(StencilCompareAndGenerate, RastGeneric);
	};
	using namespace RasterStats;
#endif // __RASTERSTAT

void Overlay( Vec4V* __restrict dest )
{
	Vec4V step(ScalarVFromF32(-0.1f));
	Vec4V minV(ScalarVFromF32(0.0001f));

	Vec4V mv =  Max( Invert(*dest) + step , minV);
	*dest = Invert(mv);
}

int GetCornerOffset( int x, int y)
{
	return ( x * RAST_BLOCK_WIDTH) + y * RAST_BLOCK_HEIGHT*RAST_TILE_WIDTH;
}

void ScanBlockTest( Vec4V* __restrict dest )
{
	for (int i = 0; i < RAST_BLOCK_HEIGHT; i++)
	{
		for(int j =0; j < RAST_BLOCK_WIDTH; j++)
		{
			Overlay(dest + i * RAST_TILE_WIDTH + j );
		}
	}
}

RAST_INLINE void ScanBlockEdgeFunctions4x4( Vec4V* __restrict dest,
	Vec4V_In Of1in, Vec4V_In Of2in, Vec4V_In Of3in,   Vec4V_In OfZin,  
	Vec4V_In C, Vec4V_In DX, Vec4V_In DY)
{
	// Vec4V DY2 = DY * Vec4V(V_TWO);
	Vec4V C0 = C;
	Vec4V C1 = C + DX;
	Vec4V C2 = AddScaled( C, DX, Vec4V(V_TWO));
	Vec4V C3 = AddScaled( C, DX, Vec4V(V_THREE));

	Vec4V DYX = -Vec4V(DY.GetX());
	Vec4V DYY = -Vec4V(DY.GetY());
	Vec4V DYZ = -Vec4V(DY.GetZ());
	Vec4V DYW = -Vec4V(DY.GetW());

	Vec4V C0X = Vec4V(C0.GetX());
	Vec4V C1X = Vec4V(C1.GetX());
	Vec4V C2X = Vec4V(C2.GetX());
	Vec4V C3X = Vec4V(C3.GetX());

	Vec4V C0Y = Vec4V(C0.GetY());
	Vec4V C1Y = Vec4V(C1.GetY());
	Vec4V C2Y = Vec4V(C2.GetY());
	Vec4V C3Y = Vec4V(C3.GetY());

	Vec4V C0Z = Vec4V(C0.GetZ());
	Vec4V C1Z = Vec4V(C1.GetZ());
	Vec4V C2Z = Vec4V(C2.GetZ());
	Vec4V C3Z = Vec4V(C3.GetZ());

	Vec4V C0W = Vec4V(C0.GetW());
	Vec4V C1W = Vec4V(C1.GetW());
	Vec4V C2W = Vec4V(C2.GetW());
	Vec4V C3W = Vec4V(C3.GetW());

	Vec4V Of1 = Of1in;
	Vec4V Of2 = Of2in;
	Vec4V Of3 = Of3in;
	Vec4V OfZ = OfZin;
	//edge masks = 
	// does 4*4 pixel blocks ( 256 pixels at a pop)
	for(int y=0; y < 2 ; y++)
	{
		Vec4V Of11 = Of1 + DYX;
		Vec4V Of12 = Of2 + DYY;
		Vec4V Of13 = Of3 + DYZ;
		Vec4V Of1Z = OfZ + DYW;

		Vec4V currentZ0 = dest[0];
		Vec4V currentZ1 = dest[1];
		Vec4V currentZ2 = dest[2];
		Vec4V currentZ3 = dest[3];
		Vec4V currentZ4 = dest[0+ RAST_TILE_WIDTH];
		Vec4V currentZ5 = dest[1+ RAST_TILE_WIDTH];
		Vec4V currentZ6 = dest[2+ RAST_TILE_WIDTH];
		Vec4V currentZ7 = dest[3+ RAST_TILE_WIDTH];

		VecBoolV mask0 = C0X > Of1;
		VecBoolV mask1 = C1X > Of1;
		VecBoolV mask2 = C2X > Of1;
		VecBoolV mask3 = C3X > Of1;
		VecBoolV mask4 = C0X > Of11;
		VecBoolV mask5 = C1X > Of11;
		VecBoolV mask6 = C2X > Of11;
		VecBoolV mask7 = C3X > Of11;

		mask0 = mask0 & (C0Y > Of2);
		mask1 = mask1 & (C1Y > Of2);
		mask2 = mask2 & (C2Y > Of2);
		mask3 = mask3 & (C3Y > Of2);
		mask4 = mask4 & (C0Y > Of12);
		mask5 = mask5 & (C1Y > Of12);
		mask6 = mask6 & (C2Y > Of12);
		mask7 = mask7 & (C3Y > Of12);

		mask0 = mask0 & (C0Z > Of3);
		mask1 = mask1 & (C1Z > Of3);
		mask2 = mask2 & (C2Z > Of3);
		mask3 = mask3 & (C3Z > Of3);
		mask4 = mask4 & (C0Z > Of13);
		mask5 = mask5 & (C1Z > Of13);
		mask6 = mask6 & (C2Z > Of13);
		mask7 = mask7 & (C3Z > Of13);

		Vec4V nZ0 =  C0W - OfZ;
		Vec4V nZ1 =  C1W - OfZ;
		Vec4V nZ2 =  C2W - OfZ;
		Vec4V nZ3 =  C3W - OfZ;
		Vec4V nZ4 =  C0W - Of1Z;
		Vec4V nZ5 =  C1W - Of1Z;
		Vec4V nZ6 =  C2W - Of1Z;
		Vec4V nZ7 =  C3W - Of1Z;

		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask0) & nZ0, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask1) & nZ1, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask2) & nZ2, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask3) & nZ3, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask4) & nZ4, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask5) & nZ5, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask6) & nZ6, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask7) & nZ7, Vec4V(V_NEGONE)));

#if 0 // TODO -- we can do this only if we can guarantee that z>=0
		Vec4V r0= Max(currentZ0 & Vec4V(mask0), nZ0);
		Vec4V r1= Max(currentZ1 & Vec4V(mask1), nZ1);
		Vec4V r2= Max(currentZ2 & Vec4V(mask2), nZ2);
		Vec4V r3= Max(currentZ3 & Vec4V(mask3), nZ3);
		Vec4V r4= Max(currentZ4 & Vec4V(mask4), nZ4);
		Vec4V r5= Max(currentZ5 & Vec4V(mask5), nZ5);
		Vec4V r6= Max(currentZ6 & Vec4V(mask6), nZ6);
		Vec4V r7= Max(currentZ7 & Vec4V(mask7), nZ7);
#else
		mask0 = mask0 & (nZ0 > currentZ0);
		mask1 = mask1 & (nZ1 > currentZ1);
		mask2 = mask2 & (nZ2 > currentZ2);
		mask3 = mask3 & (nZ3 > currentZ3);
		mask4 = mask4 & (nZ4 > currentZ4);
		mask5 = mask5 & (nZ5 > currentZ5);
		mask6 = mask6 & (nZ6 > currentZ6);
		mask7 = mask7 & (nZ7 > currentZ7);

		Vec4V r0= SelectFT( mask0, currentZ0, nZ0);
		Vec4V r1= SelectFT( mask1, currentZ1, nZ1);
		Vec4V r2= SelectFT( mask2, currentZ2, nZ2);
		Vec4V r3= SelectFT( mask3, currentZ3, nZ3);
		Vec4V r4= SelectFT( mask4, currentZ4, nZ4);
		Vec4V r5= SelectFT( mask5, currentZ5, nZ5);
		Vec4V r6= SelectFT( mask6, currentZ6, nZ6);
		Vec4V r7= SelectFT( mask7, currentZ7, nZ7);
#endif

		dest[0]=r0;
		dest[1]=r1;
		dest[2]=r2;
		dest[3]=r3;
		dest[0+ RAST_TILE_WIDTH]=r4;
		dest[1+ RAST_TILE_WIDTH]=r5;
		dest[2+ RAST_TILE_WIDTH]=r6;
		dest[3+ RAST_TILE_WIDTH]=r7;
		dest +=RAST_TILE_WIDTH*2;

		Of1 = Of11 + DYX;
		Of2 = Of12 + DYY;
		Of3 = Of13 + DYZ;
		OfZ = Of1Z + DYW;
	}
}

RAST_INLINE void ScanBlockEdgeFunctions16x16( Vec4V* __restrict dest,
	Vec4V_In Of1in, Vec4V_In Of2in,   Vec4V_In Of3in,   Vec4V_In OfZin,  
	Vec4V_In C, Vec4V_In DX, Vec4V_In DY )
{
	Vec4V C0 = C;
	Vec4V C1 = C + DX;
	Vec4V C2 = AddScaled( C, DX, Vec4V(V_TWO));
	Vec4V C3 = AddScaled( C, DX, Vec4V(V_THREE));
	Vec4V C4 = AddScaled( C, DX, Vec4V(V_FOUR));
	Vec4V C5 = AddScaled( C, DX, Vec4V(V_FIVE));
	Vec4V C6 = AddScaled( C, DX, Vec4V(V_SIX));
	Vec4V C7 = AddScaled( C, DX, Vec4V(V_SEVEN));

	Vec4V negDY = -DY;
	Vec4V DYX = Vec4V(negDY.GetX());
	Vec4V DYY = Vec4V(negDY.GetY());
	Vec4V DYZ = Vec4V(negDY.GetZ());
	Vec4V DYW = Vec4V(negDY.GetW());

	Vec4V C0X = Vec4V(C0.GetX());
	Vec4V C1X = Vec4V(C1.GetX());
	Vec4V C2X = Vec4V(C2.GetX());
	Vec4V C3X = Vec4V(C3.GetX());
	Vec4V C4X = Vec4V(C4.GetX());
	Vec4V C5X = Vec4V(C5.GetX());
	Vec4V C6X = Vec4V(C6.GetX());
	Vec4V C7X = Vec4V(C7.GetX());

	Vec4V C0Y = Vec4V(C0.GetY());
	Vec4V C1Y = Vec4V(C1.GetY());
	Vec4V C2Y = Vec4V(C2.GetY());
	Vec4V C3Y = Vec4V(C3.GetY());
	Vec4V C4Y = Vec4V(C4.GetY());
	Vec4V C5Y = Vec4V(C5.GetY());
	Vec4V C6Y = Vec4V(C6.GetY());
	Vec4V C7Y = Vec4V(C7.GetY());

	Vec4V C0Z = Vec4V(C0.GetZ());
	Vec4V C1Z = Vec4V(C1.GetZ());
	Vec4V C2Z = Vec4V(C2.GetZ());
	Vec4V C3Z = Vec4V(C3.GetZ());
	Vec4V C4Z = Vec4V(C4.GetZ());
	Vec4V C5Z = Vec4V(C5.GetZ());
	Vec4V C6Z = Vec4V(C6.GetZ());
	Vec4V C7Z = Vec4V(C7.GetZ());

	Vec4V C0W = Vec4V(C0.GetW());
	Vec4V C1W = Vec4V(C1.GetW());
	Vec4V C2W = Vec4V(C2.GetW());
	Vec4V C3W = Vec4V(C3.GetW());
	Vec4V C4W = Vec4V(C4.GetW());
	Vec4V C5W = Vec4V(C5.GetW());
	Vec4V C6W = Vec4V(C6.GetW());
	Vec4V C7W = Vec4V(C7.GetW());

	Vec4V Of1 = Of1in;
	Vec4V Of2 = Of2in;
	Vec4V Of3 = Of3in;
	Vec4V OfZ = OfZin;
	// does 16*16 pixel blocks ( 256 pixels at a pop)
	for(int y=0; y < RAST_BLOCK_HEIGHT ; y++)
	{
		Vec4V currentZ0 = dest[0];
		Vec4V currentZ1 = dest[1];
		Vec4V currentZ2 = dest[2];
		Vec4V currentZ3 = dest[3];
		Vec4V currentZ4 = dest[4];
		Vec4V currentZ5 = dest[5];
		Vec4V currentZ6 = dest[6];
		Vec4V currentZ7 = dest[7];

		VecBoolV mask0 = C0X > Of1;
		VecBoolV mask1 = C1X > Of1;
		VecBoolV mask2 = C2X > Of1;
		VecBoolV mask3 = C3X > Of1;
		VecBoolV mask4 = C4X > Of1;
		VecBoolV mask5 = C5X > Of1;
		VecBoolV mask6 = C6X > Of1;
		VecBoolV mask7 = C7X > Of1;

		mask0 = mask0 & (C0Y > Of2);
		mask1 = mask1 & (C1Y > Of2);
		mask2 = mask2 & (C2Y > Of2);
		mask3 = mask3 & (C3Y > Of2);
		mask4 = mask4 & (C4Y > Of2);
		mask5 = mask5 & (C5Y > Of2);
		mask6 = mask6 & (C6Y > Of2);
		mask7 = mask7 & (C7Y > Of2);

		mask0 = mask0 & (C0Z > Of3);
		mask1 = mask1 & (C1Z > Of3);
		mask2 = mask2 & (C2Z > Of3);
		mask3 = mask3 & (C3Z > Of3);
		mask4 = mask4 & (C4Z > Of3);
		mask5 = mask5 & (C5Z > Of3);
		mask6 = mask6 & (C6Z > Of3);
		mask7 = mask7 & (C7Z > Of3);
	
		Vec4V nZ0 =  C0W - OfZ;
		Vec4V nZ1 =  C1W - OfZ;
		Vec4V nZ2 =  C2W - OfZ;
		Vec4V nZ3 =  C3W - OfZ;
		Vec4V nZ4 =  C4W - OfZ;
		Vec4V nZ5 =  C5W - OfZ;
		Vec4V nZ6 =  C6W - OfZ;
		Vec4V nZ7 =  C7W - OfZ;

#if DEBUG_RASTERIZER
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask0) & nZ0, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask1) & nZ1, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask2) & nZ2, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask3) & nZ3, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask4) & nZ4, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask5) & nZ5, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask6) & nZ6, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll(  Vec4V(mask7) & nZ7, Vec4V(V_NEGONE)));
#endif

#if 0 // TODO -- we can do this only if we can guarantee that z>=0
		Vec4V r0= Max(currentZ0 & Vec4V(mask0), nZ0);
		Vec4V r1= Max(currentZ1 & Vec4V(mask1), nZ1);
		Vec4V r2= Max(currentZ2 & Vec4V(mask2), nZ2);
		Vec4V r3= Max(currentZ3 & Vec4V(mask3), nZ3);
		Vec4V r4= Max(currentZ4 & Vec4V(mask4), nZ4);
		Vec4V r5= Max(currentZ5 & Vec4V(mask5), nZ5);
		Vec4V r6= Max(currentZ6 & Vec4V(mask6), nZ6);
		Vec4V r7= Max(currentZ7 & Vec4V(mask7), nZ7);
#else
		mask0 = mask0 & (nZ0 > currentZ0);
		mask1 = mask1 & (nZ1 > currentZ1);
		mask2 = mask2 & (nZ2 > currentZ2);
		mask3 = mask3 & (nZ3 > currentZ3);
		mask4 = mask4 & (nZ4 > currentZ4);
		mask5 = mask5 & (nZ5 > currentZ5);
		mask6 = mask6 & (nZ6 > currentZ6);
		mask7 = mask7 & (nZ7 > currentZ7);

		Vec4V r0= SelectFT( mask0, currentZ0, nZ0);
		Vec4V r1= SelectFT( mask1, currentZ1, nZ1);
		Vec4V r2= SelectFT( mask2, currentZ2, nZ2);
		Vec4V r3= SelectFT( mask3, currentZ3, nZ3);
		Vec4V r4= SelectFT( mask4, currentZ4, nZ4);
		Vec4V r5= SelectFT( mask5, currentZ5, nZ5);
		Vec4V r6= SelectFT( mask6, currentZ6, nZ6);
		Vec4V r7= SelectFT( mask7, currentZ7, nZ7);
#endif

		dest[0]=r0;
		dest[1]=r1;
		dest[2]=r2;
		dest[3]=r3;
		dest[4]=r4;
		dest[5]=r5;
		dest[6]=r6;
		dest[7]=r7;
		dest +=RAST_TILE_WIDTH;

		Of1 += DYX;
		Of2 += DYY;
		Of3 += DYZ;
		OfZ += DYW;
	}
}

RAST_INLINE void ScanBlockEdgeFunctions16x16ZOnly( Vec4V* __restrict dest,
											 Vec4V_In OfZin,  Vec4V_In C, Vec4V_In DX, Vec4V_In DY )
{
	Vec4V C0 = C;
	Vec4V C1 = C + DX;
	Vec4V C2 = AddScaled( C, DX, Vec4V(V_TWO));
	Vec4V C3 = AddScaled( C, DX, Vec4V(V_THREE));
	Vec4V C4 = AddScaled( C, DX, Vec4V(V_FOUR));
	Vec4V C5 = AddScaled( C, DX, Vec4V(V_FIVE));
	Vec4V C6 = AddScaled( C, DX, Vec4V(V_SIX));
	Vec4V C7 = AddScaled( C, DX, Vec4V(V_SEVEN));

	Vec4V negDY = -DY;
	Vec4V DYW = Vec4V(negDY.GetW());

	Vec4V C0W = Vec4V(C0.GetW());
	Vec4V C1W = Vec4V(C1.GetW());
	Vec4V C2W = Vec4V(C2.GetW());
	Vec4V C3W = Vec4V(C3.GetW());
	Vec4V C4W = Vec4V(C4.GetW());
	Vec4V C5W = Vec4V(C5.GetW());
	Vec4V C6W = Vec4V(C6.GetW());
	Vec4V C7W = Vec4V(C7.GetW());

	Vec4V OfZ = OfZin;
	// does 16*16 pixel blocks ( 256 pixels at a pop)
	for(int y=0; y < RAST_BLOCK_HEIGHT ; y++)
	{
		Vec4V currentZ0 = dest[0];
		Vec4V currentZ1 = dest[1];
		Vec4V currentZ2 = dest[2];
		Vec4V currentZ3 = dest[3];
		Vec4V currentZ4 = dest[4];
		Vec4V currentZ5 = dest[5];
		Vec4V currentZ6 = dest[6];
		Vec4V currentZ7 = dest[7];

		Vec4V nZ0 =  C0W - OfZ;
		Vec4V nZ1 =  C1W - OfZ;
		Vec4V nZ2 =  C2W - OfZ;
		Vec4V nZ3 =  C3W - OfZ;
		Vec4V nZ4 =  C4W - OfZ;
		Vec4V nZ5 =  C5W - OfZ;
		Vec4V nZ6 =  C6W - OfZ;
		Vec4V nZ7 =  C7W - OfZ;

#if DEBUG_RASTERIZER
		Assert( IsGreaterThanOrEqualAll( nZ0, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll( nZ1, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll( nZ2, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll( nZ3, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll( nZ4, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll( nZ5, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll( nZ6, Vec4V(V_NEGONE)));
		Assert( IsGreaterThanOrEqualAll( nZ7, Vec4V(V_NEGONE)));
#endif

		Vec4V r0= Max( currentZ0, nZ0);
		Vec4V r1= Max( currentZ1, nZ1);
		Vec4V r2= Max( currentZ2, nZ2);
		Vec4V r3= Max( currentZ3, nZ3);
		Vec4V r4= Max( currentZ4, nZ4);
		Vec4V r5= Max( currentZ5, nZ5);
		Vec4V r6= Max( currentZ6, nZ6);
		Vec4V r7= Max( currentZ7, nZ7);

		dest[0]=r0;
		dest[1]=r1;
		dest[2]=r2;
		dest[3]=r3;
		dest[4]=r4;
		dest[5]=r5;
		dest[6]=r6;
		dest[7]=r7;
		dest +=RAST_TILE_WIDTH;

		OfZ += DYW;
	}
}

/*RAST_INLINE VecBoolV_Out InBetweenPosNegBounds( Vec4V_In p, Vec4V_In bounds, Vec4V_In signMask )
{
	return ( (p&signMask) < bounds );
}*/

RAST_INLINE VecBoolV_Out ScanQueryHiZLarge( const Vec4V* __restrict HiZBuffer, Vec3V_In scmin, Vec3V_In scmax)
{
	const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	const Vec4V signMask(V_7FFFFFFF);
	const Vec4V WayOffScreen(V_NEG_FLT_MAX);
	const Vec4V one(V_ONE);
	const Vec4V two(V_TWO);
	const Vec4V three(V_THREE);
	const Vec4V four(V_THREE);
	const Vec3V half(V_HALF);
	const Vec3V two3(V_TWO);
	static const Vec4V HiZSizeV( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, (float)RAST_HIZ_HEIGHT);

	Vec4V ztest = Vec4V(Invert(scmin.GetZ()));

	// Check Screen space loop extents
	Vec3V startV = RoundToNearestIntZero(scmin); 
	Vec3V endV = RoundToNearestIntPosInf(scmax);
	Vec4V extents = MergeXY( startV, endV );
	extents = Max( extents, Vec4V(V_ZERO));
	extents = Min( extents, HiZSizeV);
	Vec4V extentsInt = FloatToIntRaw<0>(extents);
	const Vec4V t = extentsInt;
	int* ext = (int*)&t;
	int sx = ext[0];
	int ex = ext[1];
	int sy = ext[2];
	int ey = ext[3];

	// round to sub pixel boundaries and find 2D box for testing
	Vec3V ppStartV = RoundToNearestIntZero(scmin * two3) * half;
	Vec3V ppEndV = RoundToNearestIntPosInf(scmax * two3) * half;

	Vec3V midP = ( ppStartV + ppEndV ) * half;
	Vec3V bounds = ( ppEndV- ppStartV ) * half;

	Vec4V boundY = Vec4V(bounds.GetY()); 
	Vec4V boundX = Vec4V(bounds.GetX());
	Vec4V boundYX2 = boundY - one;


	Vec4V sxV =  Vec4V(extents.GetX()) + offsetX - Vec4V(midP.GetX());
	Vec4V posY =  Vec4V(extents.GetZ()) + offsetY - Vec4V(midP.GetY());
	VecBoolV res0(V_F_F_F_F);
	VecBoolV res1(V_F_F_F_F);
	VecBoolV res2(V_F_F_F_F);
	VecBoolV res3(V_F_F_F_F);


	const Vec4V*  __restrict hiZScanLine = HiZBuffer + sy * RAST_HIZ_WIDTH + sx;

	Vec4V sposX0 = sxV;
	Vec4V sposX1 = sxV + one;
	Vec4V sposX2 = sxV + two;
	Vec4V sposX3 = sxV + three;


	for (int y = sy;  y < ey; y+=2 )
	{
		const Vec4V* __restrict hiZ = hiZScanLine;

		Vec4V absPosY = posY &signMask;
		// TODO only doing two range checks instead of four here.
		VecBoolV inRangeY =  absPosY <= boundY;
		VecBoolV inRangeY2 =  ((posY+one) &signMask) <= boundYX2;

		Vec4V bounds0 = boundX & Vec4V(inRangeY);
		Vec4V bounds1 = boundX & Vec4V(inRangeY2);

		Vec4V posX0 = sposX0;
		Vec4V posX1 = sposX1;
		Vec4V posX2 = sposX2;
		Vec4V posX3 = sposX3;
		for (int x = sx; x < ex; x+=4 )
		{
			Vec4V pX0 = posX0 & signMask;
			Vec4V pX1 = posX1 & signMask;
			Vec4V pX2 = posX2 & signMask;
			Vec4V pX3 = posX3 & signMask;

			Vec4V hiZ0 = hiZ[0];
			Vec4V hiZ1 = hiZ[1];
			Vec4V hiZ2 = hiZ[2];
			Vec4V hiZ3 = hiZ[3];
			Vec4V hiZ4 = hiZ[RAST_HIZ_WIDTH+0];
			Vec4V hiZ5 = hiZ[RAST_HIZ_WIDTH+1];
			Vec4V hiZ6 = hiZ[RAST_HIZ_WIDTH+2];
			Vec4V hiZ7 = hiZ[RAST_HIZ_WIDTH+3];


			VecBoolV inbounds0 = pX0 <= bounds0;
			VecBoolV inbounds1 = pX1 <= bounds0;
			VecBoolV inbounds2 = pX2 <= bounds0;
			VecBoolV inbounds3 = pX3 <= bounds0;
			VecBoolV inbounds4 = pX0 <= bounds1;
			VecBoolV inbounds5 = pX1 <= bounds1;
			VecBoolV inbounds6 = pX2 <= bounds1;
			VecBoolV inbounds7 = pX3 <= bounds1;

			VecBoolV test0 = (ztest > hiZ0);
			VecBoolV test1 = (ztest > hiZ1);
			VecBoolV test2 = (ztest > hiZ2);
			VecBoolV test3 = (ztest > hiZ3);
			VecBoolV test4 = (ztest > hiZ4);
			VecBoolV test5 = (ztest > hiZ5);
			VecBoolV test6 = (ztest > hiZ6);
			VecBoolV test7 = (ztest > hiZ7);


			test0 = test0 & inbounds0;
			test1 = test1 & inbounds1;
			test2 = test2 & inbounds2;
			test3 = test3 & inbounds3;
			test4 = test4 & inbounds4;
			test5 = test5 & inbounds5;
			test6 = test6 & inbounds6;
			test7 = test7 & inbounds7;

			res0 = res0 | test0; 
			res1 = res1 | test1; 
			res2 = res2 | test2; 
			res3 = res3 | test3; 
			res0 = res0 | test4; 
			res1 = res1 | test5; 
			res2 = res2 | test6; 
			res3 = res3 | test7; 

			hiZ+=4;
			posX0 += four;
			posX1 += four;
			posX2 += four;
			posX3 += four;
		}
		posY += two;
		hiZScanLine += RAST_HIZ_WIDTH*2;
	}
	res3 = res3 | res2;
	VecBoolV res = res0 | res1;
	return res | res3;
}

int ScanQueryHiZSimple(  const Vec4V* __restrict HiZBuffer, Vec3V_In scmin, Vec3V_In scmax)
{
	Vec4V HiZSizeV( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, (float)RAST_HIZ_HEIGHT);
	Vec4V ztest = Vec4V(Invert(scmin.GetZ()));

	Vec3V startV = RoundToNearestIntZero(scmin);
	Vec3V endV = RoundToNearestIntPosInf(scmax);

	Vec4V extents = MergeXY( startV, endV ); // startX, endX, startY, endY

	extents = Max( extents, Vec4V(V_ZERO));
	extents = Min( extents, HiZSizeV);

	extents = FloatToIntRaw<0>(extents);
	const Vec4V t = extents;
	int* ext = (int*)&t;
	int sx = ext[0];
	int ex = ext[1];
	int sy = ext[2];
	int ey = ext[3];

	Vec4V res(V_ZERO);
	for (int y = sy;  y < ey; y++ )
	{
		const Vec4V*  __restrict hiZ = HiZBuffer + y * RAST_HIZ_WIDTH + sx;
		for (int x = sx; x < ex; x++ )
		{
			res = res | Vec4V(ztest > *hiZ); 
			hiZ++;
		}
	}
	return IsEqualIntAll( res, Vec4V(V_ZERO));
}

Vec4V GenerateHiZTile( const Vec4V* zbufferTile, float indexX, float indexY, Vec4V* __restrict HiZTile, int /*HiZStride*/, Vec4V *minMaxBoundsEA)
{
#if TASK_LOCALACCESS_ONLY
	Vec4V			hiZLocalLine[2][RAST_HIZ_WIDTH];
#endif
	const Vec4V*		zbuffScanLine = zbufferTile;
	Vec4V* __restrict	hiZScanLine = HiZTile;


	Vec4V tMin0(V_NEG_FLT_MAX);
	Vec4V tMin1(V_NEG_FLT_MAX);
	Vec4V tMin2(V_NEG_FLT_MAX);
	Vec4V tMin3(V_NEG_FLT_MAX);

	Vec4V minPosX(V_FLT_MAX);
	Vec4V maxPosX(V_NEG_FLT_MAX);
	Vec4V minPosY(V_FLT_MAX);
	Vec4V maxPosY(V_NEG_FLT_MAX);
	const Vec4V startPosY = Vec4V(indexY, indexY, indexY, indexY);
	Vec4V incrementPosY = startPosY;//Vec4VConstant<0,0,0,0>();
	const Vec4V startPosX = Vec4V(indexX, indexX + 1.0f, indexX + 2.0f, indexX + 3.0f);
	//bool foundPos = false;
	for (int y = 0; y < RAST_TILE_HEIGHT; y+=2)
	{
		const Vec4V*		zbuff = zbuffScanLine;
		Vec4V incrementPosX = startPosX;//Vec4VConstant<0,0,0,0>();
#if TASK_LOCALACCESS_ONLY
		int buffIdx = ((y>>1)&1);
		Vec4V* __restrict	hiZ = hiZLocalLine[buffIdx];
#else
		Vec4V* __restrict	hiZ = hiZScanLine;
#endif
		for (int x = 0; x < RAST_TILE_WIDTH/8; x++ )
		{
			Vec4V v1  = zbuff[ 0 ];
			Vec4V v2  = zbuff[ 1 ];
			Vec4V v11 = zbuff[ 2 ];
			Vec4V v21 = zbuff[ 3 ];
			Vec4V v12 = zbuff[ 4 ];
			Vec4V v22 = zbuff[ 5 ];
			Vec4V v13 = zbuff[ 6 ];
			Vec4V v23 = zbuff[ 7 ];

			Vec4V v3  = zbuff[ RAST_TILE_WIDTH];
			Vec4V v4  = zbuff[ RAST_TILE_WIDTH + 1];
			Vec4V v31 = zbuff[ RAST_TILE_WIDTH + 2];
			Vec4V v41 = zbuff[ RAST_TILE_WIDTH + 3];
			Vec4V v32 = zbuff[ RAST_TILE_WIDTH + 4];
			Vec4V v42 = zbuff[ RAST_TILE_WIDTH + 5];
			Vec4V v33 = zbuff[ RAST_TILE_WIDTH + 6];
			Vec4V v43 = zbuff[ RAST_TILE_WIDTH + 7];

			Vec4V t1,t2,t3,t4;
			Transpose4x4(t1,t2,t3,t4, v1,v2,v3,v4);
			Vec4V t11,t21,t31,t41;
			Transpose4x4(t11,t21,t31,t41, v11,v21,v31,v41);
			Vec4V t12,t22,t32,t42;
			Transpose4x4(t12,t22,t32,t42, v12,v22,v32,v42);
			Vec4V t13,t23,t33,t43;
			Transpose4x4(t13,t23,t33,t43, v13,v23,v33,v43);

			Vec4V HiZ0 = Min(Min(t1,t2), Min(t3,t4) );
			Vec4V HiZ1 = Min(Min(t11,t21), Min(t31,t41) );
			Vec4V HiZ2 = Min(Min(t12,t22), Min(t32,t42) );
			Vec4V HiZ3 = Min(Min(t13,t23), Min(t33,t43) );


			Vec4V extentsValidZ0 = Vec4V(IsGreaterThan(HiZ0, Vec4V(V_ONE)));
			Vec4V extentsValidZ1 = Vec4V(IsGreaterThan(HiZ1, Vec4V(V_ONE)));
			Vec4V extentsValidZ2 = Vec4V(IsGreaterThan(HiZ2, Vec4V(V_ONE)));
			Vec4V extentsValidZ3 = Vec4V(IsGreaterThan(HiZ3, Vec4V(V_ONE)));

			Vec4V extendsValidt0,extendsValidt1,extendsValidt2,extendsValidt3;
			Transpose4x4(extendsValidt0,extendsValidt1,extendsValidt2,extendsValidt3, extentsValidZ0,extentsValidZ1,extentsValidZ2,extentsValidZ3);
			Vec4V extendsValidBlocks = extendsValidt0 | extendsValidt1 | extendsValidt2 | extendsValidt3;
			//const VecBoolV extendsValidBlockCondition = IsGreaterThan(extendsValidBlocks, Vec4V(V_ZERO)); //VecBoolV(extendsValidBlocks);
			const VecBoolV extendsValidBlockCondition(extendsValidBlocks.GetIntrin128());
			
			minPosX = Min(SelectFT(extendsValidBlockCondition, Vec4V(V_FLT_MAX), incrementPosX), minPosX);
			maxPosX = Max(SelectFT(extendsValidBlockCondition, Vec4V(V_NEG_FLT_MAX), incrementPosX), maxPosX);
			minPosY = Min(SelectFT(extendsValidBlockCondition, Vec4V(V_FLT_MAX), incrementPosY), minPosY);
			maxPosY = Max(SelectFT(extendsValidBlockCondition, Vec4V(V_NEG_FLT_MAX), incrementPosY), maxPosY);

			// NOTE - use dbct?? - could pipeline due to dependency at end
			hiZ[0]= HiZ0;
			hiZ[1]= HiZ1;
			hiZ[2]= HiZ2;
			hiZ[3]= HiZ3;

			tMin0 = Max(tMin0, HiZ0);
			tMin1 = Max(tMin1, HiZ1);
			tMin2 = Max(tMin2, HiZ2);
			tMin3 = Max(tMin3, HiZ3);

			zbuff += 8;
			hiZ +=4;
			incrementPosX += Vec4V(V_FOUR);
		}
#if TASK_LOCALACCESS_ONLY
		taskWait(TAG_WRITEHIZ_0 + !buffIdx);  // double buffer the waits
		taskWriteBuffer( "HiZ Result",hiZLocalLine[buffIdx], hiZScanLine, RAST_HIZ_TILE_WIDTH, TAG_WRITEHIZ_0 + buffIdx );
#endif
		zbuffScanLine += RAST_TILE_WIDTH*2;
		hiZScanLine += RAST_HIZ_WIDTH;
		incrementPosY += Vec4V(V_ONE);
	}
	Vec4V totalMin=Max(Max(tMin0,tMin1 ), Max(tMin2, tMin3));
	
	ScalarV totalMinX = Min(Min(minPosX.GetX(), minPosX.GetY()), Min(minPosX.GetZ(), minPosX.GetW()));
	ScalarV totalMinY = Min(Min(minPosY.GetX(), minPosY.GetY()), Min(minPosY.GetZ(), minPosY.GetW()));
	ScalarV totalMaxX = Max(Max(maxPosX.GetX(), maxPosX.GetY()), Max(maxPosX.GetZ(), maxPosX.GetW())) + ScalarV(V_ONE);
	ScalarV totalMaxY = Max(Max(maxPosY.GetX(), maxPosY.GetY()), Max(maxPosY.GetZ(), maxPosY.GetW())) + ScalarV(V_ONE);

	Vec4V minMaxBounds(totalMinX, totalMinY, totalMaxX, totalMaxY);

	taskSetVec4V( minMaxBoundsEA, minMaxBounds, TAG_WRITEHIZ_0);
	//Displayf("Address %u  ------- Index %f %f\n", minMaxBoundsEA, indexX, indexY);
#if TASK_LOCALACCESS_ONLY
	int buffIdx = (((RAST_TILE_HEIGHT-1)>>1)&1);
	taskWait(TAG_WRITEHIZ_0 + buffIdx);  // double buffer the waits	
#endif

	return totalMin;
}


void ScanSmallBlockTest( Vec4V* zBuffer )
{
	// draw 4x4 block
	for (int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			Overlay(zBuffer + j + i*RAST_TILE_WIDTH);
			Overlay(zBuffer + j + i*RAST_TILE_WIDTH);
			Overlay(zBuffer + j + i*RAST_TILE_WIDTH);
		}
	}
}

template<bool DrawBlock>
void ScanTileSmall( const u16* __restrict indices, int amt, 
	const PreTri2* tris, Vec4V* zBuffer, Vec2V_In location )
{
	Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	// Vec4V roundOffset = Vec4V( Vec2V(V_NEGONE), Vec2V(V_ONE) );

	Vec4V zero(V_ZERO);
	Vec4V edge(Vec2V(ScalarVFromF32(RAST_TILE_WIDTH-4)), Vec2V(ScalarVFromF32(RAST_TILE_HEIGHT-4)));
	Vec4V loc4( MergeXY(location, location));
	zero += loc4;
	edge += loc4;

#if TASK_LOCALACCESS_ONLY

	taskWait( TAG_LOADINDICES);

	const int CacheLineSize = 8;
	const int CacheLineMask = CacheLineSize-1;
	const int CacheSize = 8;
	const int CacheSizeMask = CacheSize-1;
	PreTri2 np[CacheSize][CacheLineSize];
	
	// prefetch
	const int PreFetchDist = 8;
	const int PreFetchDistMask = PreFetchDist-1;
	const int PreFetchStep = PreFetchDistMask;

	int doWaitPrev[PreFetchDist];
	PreTri2* prevTri[PreFetchDist];
	int		prevCacheLine = -1;
	int buff = 0;

	int prefetchAmt = Min( PreFetchStep,amt );
	for (int i =0; i < prefetchAmt;i++ )
	{
		int fidx = i&PreFetchDistMask;

		int idx = indices[i];
		int cacheLine = idx&~CacheLineMask;
		int offset= idx&CacheLineMask;

		
		doWaitPrev[fidx]= 0;
		if ( Unlikely( cacheLine != prevCacheLine))
		{
			buff = (buff + 1) & CacheSizeMask;
			taskLoadBuffer("tri",&np[buff][0], tris + cacheLine, CacheLineSize,  TAG_LOADTRI_0 + buff);
			doWaitPrev[fidx]= TAG_LOADTRI_0 + buff;
			prevCacheLine = cacheLine;
		}
		prevTri[fidx] = np[buff] + offset;
		
	}
#else
	const int PreFetchStep = 0;
#endif
	
	for (int t = 0; t < amt-PreFetchStep; t++)
	{
#if TASK_LOCALACCESS_ONLY
		int fidx = (t+PreFetchStep)&PreFetchDistMask;
		int fidx2 = t & PreFetchDistMask;

		int idx = indices[t + PreFetchStep];
		int cacheLine = idx&~CacheLineMask;
		int offset = idx&CacheLineMask;
		
		doWaitPrev[fidx]= 0;
		if ( Unlikely( cacheLine != prevCacheLine))
		{
			buff = (buff + 1) & CacheSizeMask;
			taskLoadBuffer("tri",&np[buff][0], tris + cacheLine, CacheLineSize,  TAG_LOADTRI_0 + buff);
			doWaitPrev[fidx]= TAG_LOADTRI_0 + buff;
			prevCacheLine = cacheLine;
		}
		prevTri[fidx] = np[buff] + offset;

		if ( Unlikely( doWaitPrev[fidx2]))
		{
			taskWait(doWaitPrev[fidx2]);
		}

		const PreTri2* p =prevTri[fidx2];
#else
		const PreTri2* p =tris + indices[t];
#endif

		Vec4V DX = -p->DY;
		Vec4V DY = p->DX;
		Vec4V C = p->C;
		Vec4V intMinMaxV =  p->minMax;

		Vec4V Of1 = -AddScaled(DY.GetX() * offsetY, offsetX, DX.GetX());
		Vec4V Of2 = -AddScaled(DY.GetY() * offsetY, offsetX, DX.GetY());
		Vec4V Of3 = -AddScaled(DY.GetZ() * offsetY, offsetX, DX.GetZ());
		Vec4V OfZ = -AddScaled(DY.GetW() * offsetY, offsetX, DX.GetW());
		
		// if overlaps edges pull in so never overwrites.
		intMinMaxV = Max( intMinMaxV, zero );
		intMinMaxV = Min( intMinMaxV, edge);

		Vec4V pos = intMinMaxV;
		intMinMaxV -= loc4;

		Vec4V actIntMin = FloatToIntRaw<0>(intMinMaxV);
		int* loc = (int*)&actIntMin;

		Vec4V* __restrict dest = zBuffer + loc[0] + loc[2]*RAST_TILE_WIDTH;
		if ( DrawBlock)
		{
			ScanSmallBlockTest( dest);
		}
		else
		{
			Vec4V Cs = AddScaled(AddScaled(C, DY, pos.GetZ()), DX, pos.GetX());
			ScanBlockEdgeFunctions4x4( dest, Of1, Of2,  Of3,  OfZ, Cs, DX, DY );
		}
	}
#if TASK_LOCALACCESS_ONLY
	int start = Max( amt-PreFetchStep,0 );
	for (int t =start; t < amt; t++)
	{
		int fidx = t&PreFetchDistMask;
		if ( Unlikely( doWaitPrev[fidx]))
		{
			taskWait(doWaitPrev[fidx]);
		}
		const PreTri2* p =prevTri[fidx];
		Vec4V DX = -p->DY;
		Vec4V DY = p->DX;
		Vec4V C = p->C;
		Vec4V intMinMaxV =  p->minMax;

		Vec4V Of1 = -AddScaled(DY.GetX() * offsetY, offsetX, DX.GetX());
		Vec4V Of2 = -AddScaled(DY.GetY() * offsetY, offsetX, DX.GetY());
		Vec4V Of3 = -AddScaled(DY.GetZ() * offsetY, offsetX, DX.GetZ());
		Vec4V OfZ = -AddScaled(DY.GetW() * offsetY, offsetX, DX.GetW());

		// if overlaps edges pull in so never overwrites.
		intMinMaxV = Max( intMinMaxV, zero );
		intMinMaxV = Min( intMinMaxV, edge);

		Vec4V pos = intMinMaxV;
		intMinMaxV -= loc4;

		Vec4V actIntMin = FloatToIntRaw<0>(intMinMaxV);
		int* loc = (int*)&actIntMin;

		Vec4V* __restrict dest = zBuffer + loc[0] + loc[2]*RAST_TILE_WIDTH;
		if ( DrawBlock)
		{
			ScanSmallBlockTest( dest);
		}
		else
		{
			Vec4V Cs = AddScaled(AddScaled(C, DY, pos.GetZ()), DX, pos.GetX());
			ScanBlockEdgeFunctions4x4( dest, Of1, Of2,  Of3,  OfZ, Cs, DX, DY );
		}
	}
#endif
}

// A tile is 4x4 blocks
template<bool DrawBlock>
void ScanTileSimple( const u16* __restrict indices, int amt, 
	const PreTri2* tris, Vec4V* zBuffer, Vec2V_In location )
{
	Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();

	offsetX += Vec4V(location.GetX());
	offsetY += Vec4V(location.GetY());
	taskWait( TAG_LOADINDICES);
	PreTri2 p;
	for (int t = 0; t < amt; t++)
	{
		taskLoad(p, tris + indices[t],  TAG_LOADTRI_0 );
		taskWait( TAG_LOADTRI_0);

		Vec4V Of1 = SubtractScaled(p.DY.GetX() * offsetX, offsetY, p.DX.GetX());
		Vec4V Of2 = SubtractScaled(p.DY.GetY() * offsetX, offsetY, p.DX.GetY());
		Vec4V Of3 = SubtractScaled(p.DY.GetZ() * offsetX, offsetY, p.DX.GetZ());
		Vec4V OfZ = SubtractScaled(p.DY.GetW() * offsetX, offsetY, p.DX.GetW());

		Vec4V DX = -p.DY;
		Vec4V DY = p.DX;

		for (int j = 0; j < 4; j++)
		{
			for (int i = 0; i < 4; i++)
			{
				Vec4V* __restrict dest = zBuffer + GetCornerOffset(i,j);
				if ( DrawBlock)
				{
					ScanBlockTest( dest );
				}
				else
				{
					Vec4V C = AddScaled(AddScaled(p.C, DY, ScalarVFromF32( (float)RAST_BLOCK_HEIGHT * (float)j )), DX, ScalarVFromF32( (float)RAST_BLOCK_WIDTH* (float)i )); // TODO -- fix LHS
					ScanBlockEdgeFunctions16x16( dest, Of1, Of2,  Of3,  OfZ, C, DX, DY );
				}
			}
		}
	}
}

template<bool DrawBlock>
void ScanTile2( const u16* __restrict indices, int amt, 
	const PreTri2* tris, Vec4V* zBuffer, Vec2V_In location )
{
	static const ScalarV TileWidth = ScalarV((float)RAST_BLOCK_WIDTH);
	static const ScalarV TileHeight = ScalarV((float)RAST_BLOCK_HEIGHT);

	// calculate corners ( could even be static )
	Vec4V* corner[16];
	Vec2V offsets[16];
	int c = 0;
	for (int j = 0; j < 4; j++)
	{
		for (int i = 0; i < 4; i++)
		{
			corner[c] = zBuffer + GetCornerOffset(i,j);
			offsets[c] = Vec2V( (float)RAST_BLOCK_WIDTH * (float)i,  (float)RAST_BLOCK_HEIGHT* (float)j ); // TODO -- fix LHS
			c++;
		}
	}

	const VecBoolV allTrue(V_T_T_T_T);
	Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();

	Vec4V lx = Vec4V(location.GetX());
	Vec4V ly = Vec4V(location.GetY());

	offsetX += lx;
	offsetY += ly;

	// Vec4V two = Vec4V(V_TWO);
	// Vec4V three = Vec4V(V_THREE);

	Vec3V two3 = Vec3V(V_TWO);
	Vec3V three3 = Vec3V(V_THREE);

	Vec4V ExtentMinOffsetsX = Vec4VConstant<U32_ZERO, U32_ONE, U32_TWO, U32_THREE>() * TileWidth;
	Vec4V ExtentMaxOffsetsX = Vec4VConstant<U32_ONE, U32_TWO, U32_THREE, U32_FOUR>() * TileWidth;

	Vec4V ExtentMinOffsetsY = Vec4VConstant<U32_ZERO, U32_ONE, U32_TWO, U32_THREE>() * TileHeight;
	Vec4V ExtentMaxOffsetsY = Vec4VConstant<U32_ONE, U32_TWO, U32_THREE, U32_FOUR>() * TileHeight;

	Vec4V SetFirstBit(V_INT_1);
	Vec4V MaxBlockX =  lx + ExtentMaxOffsetsX;
	Vec4V MaxBlockY =  ly + ExtentMaxOffsetsY;

	Vec4V MinBlockX =  lx + ExtentMinOffsetsX;
	Vec4V MinBlockY =  ly + ExtentMinOffsetsY; 

	Vec4V TileOffsets = ExtentMinOffsetsX;

	Vec4V res[4];
	int tileIdx[16];

	int maxAmt = amt;

#if TASK_LOCALACCESS_ONLY

	taskWait( TAG_LOADINDICES);
	PreTri2 np[2];
	PreTri2 p;
	int buff = 0;
	taskLoad(np[buff], tris + indices[0],  TAG_LOADTRI_0 + buff);
	maxAmt = maxAmt-1;
#endif
	 
	for (int t = 0; t < maxAmt; t++)
	{
#if TASK_LOCALACCESS_ONLY
		int obuff = !buff;
		taskLoad(np[obuff], tris + indices[t+1],  TAG_LOADTRI_0 + obuff);
		taskWait( TAG_LOADTRI_0 + buff);
		p =np[buff];
		buff = obuff;
#else
		const PreTri2& p = tris[indices[t]];
#endif

		Vec4V Of1 = SubtractScaled(p.DY.GetX() * offsetX, offsetY, p.DX.GetX());
		Vec4V Of2 = SubtractScaled(p.DY.GetY() * offsetX, offsetY, p.DX.GetY());
		Vec4V Of3 = SubtractScaled(p.DY.GetZ() * offsetX, offsetY, p.DX.GetZ());
		Vec4V OfZ = SubtractScaled(p.DY.GetW() * offsetX, offsetY, p.DX.GetW());

		Vec4V minX = Vec4V(p.minMax.GetX());
		Vec4V minY = Vec4V(p.minMax.GetZ());
		Vec4V maxX = Vec4V(p.minMax.GetY());
		Vec4V maxY = Vec4V(p.minMax.GetW());

		// do check for tr
		Vec4V DX = -p.DY;
		Vec4V DY = p.DX;
		Vec4V C = p.C;
		Vec4V tDY = p.DX * TileHeight;
		Vec3V EdgeReject = p.GetTrivalReject(TileWidth, TileHeight);

		EdgeReject -= AddScaled(lx * DX, ly, DY).GetXYZ();

		Vec4V C0X = AddScaled(Vec4V(C.GetX()), TileOffsets, DX.GetX());
		Vec4V C0Y = AddScaled(Vec4V(C.GetY()), TileOffsets, DX.GetY());
		Vec4V C0Z = AddScaled(Vec4V(C.GetZ()), TileOffsets, DX.GetZ());

		Vec3V tDY3 =  tDY.GetXYZ();
		Vec3V EdgeReject0 = EdgeReject;
		Vec3V EdgeReject1 = EdgeReject - tDY3;
		Vec3V EdgeReject2 = AddScaled( EdgeReject, -tDY3, two3);
		Vec3V EdgeReject3 = AddScaled( EdgeReject, -tDY3, three3);

		// do min max checks
		Vec4V colOverlap = Vec4V((minX < MaxBlockX) & ( maxX > MinBlockX));
		Vec4V rowOverlap = Vec4V((minY < MaxBlockY) & ( maxY > MinBlockY));

		Vec4V r1 = colOverlap & Vec4V(rowOverlap.GetX());
		Vec4V r2 = colOverlap & Vec4V(rowOverlap.GetY());
		Vec4V r3 = colOverlap & Vec4V(rowOverlap.GetZ());
		Vec4V r4 = colOverlap & Vec4V(rowOverlap.GetW());
		// 4x4 case 
		// Evaluate half-space functions store in 4x4 lists and repeat.

		Vec4V er1 = Vec4V(Vec4V(EdgeReject0.GetX()) < C0X);
		Vec4V er2 = Vec4V(Vec4V(EdgeReject1.GetX()) < C0X);
		Vec4V er3 = Vec4V(Vec4V(EdgeReject2.GetX()) < C0X);
		Vec4V er4 = Vec4V(Vec4V(EdgeReject3.GetX()) < C0X);

		er1 = er1 & Vec4V(Vec4V(EdgeReject0.GetY()) < C0Y);
		er2 = er2 & Vec4V(Vec4V(EdgeReject1.GetY()) < C0Y);
		er3 = er3 & Vec4V(Vec4V(EdgeReject2.GetY()) < C0Y);
		er4 = er4 & Vec4V(Vec4V(EdgeReject3.GetY()) < C0Y);

		er1 = er1 & Vec4V(Vec4V(EdgeReject0.GetZ()) < C0Z);
		er2 = er2 & Vec4V(Vec4V(EdgeReject1.GetZ()) < C0Z);
		er3 = er3 & Vec4V(Vec4V(EdgeReject2.GetZ()) < C0Z);
		er4 = er4 & Vec4V(Vec4V(EdgeReject3.GetZ()) < C0Z);

		er1 = er1 & r1;
		er2 = er2 & r2;
		er3 = er3 & r3;
		er4 = er4 & r4;

		er1 = er1 & SetFirstBit;
		er2 = er2 & SetFirstBit;
		er3 = er3 & SetFirstBit;
		er4 = er4 & SetFirstBit;

		// convert from 4 into ints some how.
		res[0] = er1;
		res[1] = er2;
		res[2] = er3;
		res[3] = er4;

		int* cr = (int*)res;

		// note could do two at once to reduce dependencies here
		int cnt = 0;
		tileIdx[cnt] = 0; cnt+=cr[0];
		tileIdx[cnt] = 1; cnt+=cr[1];
		tileIdx[cnt] = 2; cnt+=cr[2];
		tileIdx[cnt] = 3; cnt+=cr[3];

		tileIdx[cnt] = 4; cnt+=cr[4];
		tileIdx[cnt] = 5; cnt+=cr[5];
		tileIdx[cnt] = 6; cnt+=cr[6];
		tileIdx[cnt] = 7; cnt+=cr[7];

		tileIdx[cnt] = 8; cnt+=cr[8];
		tileIdx[cnt] = 9; cnt+=cr[9];
		tileIdx[cnt] = 10; cnt+=cr[10];
		tileIdx[cnt] = 11; cnt+=cr[11];

		tileIdx[cnt] = 12; cnt+=cr[12];
		tileIdx[cnt] = 13; cnt+=cr[13];
		tileIdx[cnt] = 14; cnt+=cr[14];
		tileIdx[cnt] = 15; cnt+=cr[15];

		//Assert( cnt != 0);
		// Due to precision issues assert above can fire in some cases.
		// descend into each block
		for (int i =0; i < cnt; i++ )
		{
			int tv = tileIdx[i];
			Vec4V* dest = corner[tv];
			Vec4V Cs = SubtractScaled(AddScaled(p.C, p.DX, offsets[tv].GetY()), p.DY, offsets[tv].GetX());

			// Need to find if overlaps tile completely
			//static bool zOnly = true;
			//ScanBlockEdgeFunctions16x16ZOnly( dest, OfZ, Cs, DX, DY );
			
			ScanBlockEdgeFunctions16x16(  dest, Of1, Of2,  Of3,  OfZ, Cs, DX, DY );
		}
	}
#if TASK_LOCALACCESS_ONLY
	for (int t = amt-1; t < amt; t++)
	{
		taskWait( TAG_LOADTRI_0 + buff);
		p =np[buff];

		Vec4V Of1 = SubtractScaled(p.DY.GetX() * offsetX, offsetY, p.DX.GetX());
		Vec4V Of2 = SubtractScaled(p.DY.GetY() * offsetX, offsetY, p.DX.GetY());
		Vec4V Of3 = SubtractScaled(p.DY.GetZ() * offsetX, offsetY, p.DX.GetZ());
		Vec4V OfZ = SubtractScaled(p.DY.GetW() * offsetX, offsetY, p.DX.GetW());

		Vec4V minX = Vec4V(p.minMax.GetX());
		Vec4V minY = Vec4V(p.minMax.GetZ());
		Vec4V maxX = Vec4V(p.minMax.GetY());
		Vec4V maxY = Vec4V(p.minMax.GetW());

		Vec4V DX = -p.DY;
		Vec4V DY = p.DX;
		Vec4V C = p.C;
		Vec4V tDY = p.DX * TileHeight;
		Vec3V EdgeReject = p.GetTrivalReject(TileWidth, TileHeight);

		EdgeReject -= AddScaled(lx * DX, ly, DY).GetXYZ();

		Vec4V C0X = AddScaled(Vec4V(C.GetX()), TileOffsets, DX.GetX());
		Vec4V C0Y = AddScaled(Vec4V(C.GetY()), TileOffsets, DX.GetY());
		Vec4V C0Z = AddScaled(Vec4V(C.GetZ()), TileOffsets, DX.GetZ());

		Vec3V tDY3 =  tDY.GetXYZ();
		Vec3V EdgeReject0 = EdgeReject;
		Vec3V EdgeReject1 = EdgeReject - tDY3;
		Vec3V EdgeReject2 = AddScaled( EdgeReject, -tDY3, two3);
		Vec3V EdgeReject3 = AddScaled( EdgeReject, -tDY3, three3);

		// do min max checks
		Vec4V colOverlap = Vec4V((minX < MaxBlockX) & ( maxX > MinBlockX));
		Vec4V rowOverlap = Vec4V((minY < MaxBlockY) & ( maxY > MinBlockY));

		Vec4V r1 = colOverlap & Vec4V(rowOverlap.GetX());
		Vec4V r2 = colOverlap & Vec4V(rowOverlap.GetY());
		Vec4V r3 = colOverlap & Vec4V(rowOverlap.GetZ());
		Vec4V r4 = colOverlap & Vec4V(rowOverlap.GetW());
		// 4x4 case 
		// Evaluate half-space functions store in 4x4 lists and repeat.

		Vec4V er1 = Vec4V(Vec4V(EdgeReject0.GetX()) < C0X);
		Vec4V er2 = Vec4V(Vec4V(EdgeReject1.GetX()) < C0X);
		Vec4V er3 = Vec4V(Vec4V(EdgeReject2.GetX()) < C0X);
		Vec4V er4 = Vec4V(Vec4V(EdgeReject3.GetX()) < C0X);

		er1 = er1 & Vec4V(Vec4V(EdgeReject0.GetY()) < C0Y);
		er2 = er2 & Vec4V(Vec4V(EdgeReject1.GetY()) < C0Y);
		er3 = er3 & Vec4V(Vec4V(EdgeReject2.GetY()) < C0Y);
		er4 = er4 & Vec4V(Vec4V(EdgeReject3.GetY()) < C0Y);

		er1 = er1 & Vec4V(Vec4V(EdgeReject0.GetZ()) < C0Z);
		er2 = er2 & Vec4V(Vec4V(EdgeReject1.GetZ()) < C0Z);
		er3 = er3 & Vec4V(Vec4V(EdgeReject2.GetZ()) < C0Z);
		er4 = er4 & Vec4V(Vec4V(EdgeReject3.GetZ()) < C0Z);

		er1 = er1 & r1;
		er2 = er2 & r2;
		er3 = er3 & r3;
		er4 = er4 & r4;

		er1 = er1 & SetFirstBit;
		er2 = er2 & SetFirstBit;
		er3 = er3 & SetFirstBit;
		er4 = er4 & SetFirstBit;

		// convert from 4 into ints some how.
		res[0] = er1;
		res[1] = er2;
		res[2] = er3;
		res[3] = er4;

		int* cr = (int*)res;

		// note could do two at once to reduce dependencies here
		int cnt = 0;
		tileIdx[cnt] = 0; cnt+=cr[0];
		tileIdx[cnt] = 1; cnt+=cr[1];
		tileIdx[cnt] = 2; cnt+=cr[2];
		tileIdx[cnt] = 3; cnt+=cr[3];

		tileIdx[cnt] = 4; cnt+=cr[4];
		tileIdx[cnt] = 5; cnt+=cr[5];
		tileIdx[cnt] = 6; cnt+=cr[6];
		tileIdx[cnt] = 7; cnt+=cr[7];

		tileIdx[cnt] = 8; cnt+=cr[8];
		tileIdx[cnt] = 9; cnt+=cr[9];
		tileIdx[cnt] = 10; cnt+=cr[10];
		tileIdx[cnt] = 11; cnt+=cr[11];

		tileIdx[cnt] = 12; cnt+=cr[12];
		tileIdx[cnt] = 13; cnt+=cr[13];
		tileIdx[cnt] = 14; cnt+=cr[14];
		tileIdx[cnt] = 15; cnt+=cr[15];

		// descend into each block
		for (int i =0; i < cnt; i++ )
		{
			int t = tileIdx[i];
			Vec4V* dest = corner[t];
			Vec4V C = SubtractScaled(AddScaled(p.C, p.DX, offsets[t].GetY()), p.DY, offsets[t].GetX());
			ScanBlockEdgeFunctions16x16(  dest, Of1, Of2,  Of3,  OfZ, C, DX, DY );			
		}
	}

#endif
}

#define ZERO_CACHE_LINE __XENON 

void ClearTile( Vec4V*__restrict p, Vec4V_In v )
{
#if SCAN_SPEW_CRAZY
	Displayf("Clearing Tile ");
#endif

	int end = RAST_TILE_WIDTH * RAST_TILE_HEIGHT;
#if ZERO_CACHE_LINE
	Vec4V*__restrict np = p;
	Assert( ((int)p &31)==0);

	for ( int i =0; i < 4; i++, np+=8)  // get 4 cache lines in flight
		__dcbz128( 0,(void*)np );

	end -= 8*4;
#endif
	for (int i =0; i < end; i+=8)
	{
		p[0]=v;
		p[1]=v;
		p[2]=v;
		p[3]=v;
		p[4]=v;
		p[5]=v;
		p[6]=v;
		p[7]=v;
		p+=8;
#if ZERO_CACHE_LINE
		__dcbz128( 0,(void*)np );
		np+=8;
#endif
	}
#if ZERO_CACHE_LINE
	for (int i =0; i < 8*4; i+=8)
	{
		p[0]=v;
		p[1]=v;
		p[2]=v;
		p[3]=v;
		p[4]=v;
		p[5]=v;
		p[6]=v;
		p[7]=v;
		p+=8;
	}
#endif
}

void ClearShadowTiles(Vec4V* zbuffer, const ShadowSquares& squares, Vec4V_In clearV, Vec4V_In nearV, Vec2V_In tileOffset)
{
	const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	for (int y = 0; y < RAST_TILE_HEIGHT; y++ )
	{	
		Vec4V posY=Vec4V(ScalarV((float)y))+offsetY+Vec4V(tileOffset.GetY());
		for (int x = 0; x < RAST_TILE_WIDTH; x++ )
		{	
			// calculate position;
			Vec4V posX = Vec4V(ScalarV((float)x))+offsetX+Vec4V(tileOffset.GetX());
			
			const Vec4V svx0 = Vec4V(squares.m_spheresX.GetX()) - posX;
			const Vec4V svy0 = Vec4V(squares.m_spheresY.GetX()) - posY;
			const Vec4V svx1 = Vec4V(squares.m_spheresX.GetY()) - posX;
			const Vec4V svy1 = Vec4V(squares.m_spheresY.GetY()) - posY;
			const Vec4V svx2 = Vec4V(squares.m_spheresX.GetZ()) - posX;
			const Vec4V svy2 = Vec4V(squares.m_spheresY.GetZ()) - posY;
			const Vec4V svx3 = Vec4V(squares.m_spheresX.GetW()) - posX;
			const Vec4V svy3 = Vec4V(squares.m_spheresY.GetW()) - posY;

			// transform into shadow box
			const Vec4V vx0 = AddScaled(svx0*squares.m_axisXY.GetX(), svy0, squares.m_axisXY.GetY());
			const Vec4V vy0 = AddScaled(svx0*squares.m_axisXY.GetZ(), svy0, squares.m_axisXY.GetW());
			const Vec4V vx1 = AddScaled(svx1*squares.m_axisXY.GetX(), svy1, squares.m_axisXY.GetY());
			const Vec4V vy1 = AddScaled(svx1*squares.m_axisXY.GetZ(), svy1, squares.m_axisXY.GetW());
			const Vec4V vx2 = AddScaled(svx2*squares.m_axisXY.GetX(), svy2, squares.m_axisXY.GetY());
			const Vec4V vy2 = AddScaled(svx2*squares.m_axisXY.GetZ(), svy2, squares.m_axisXY.GetW());
			const Vec4V vx3 = AddScaled(svx3*squares.m_axisXY.GetX(), svy3, squares.m_axisXY.GetY());
			const Vec4V vy3 = AddScaled(svx3*squares.m_axisXY.GetZ(), svy3, squares.m_axisXY.GetW());

			const Vec4V maxd0=Max(Abs(vx0),Abs(vy0));
			const Vec4V maxd1=Max(Abs(vx1),Abs(vy1));
			const Vec4V maxd2=Max(Abs(vx2),Abs(vy2));
			const Vec4V maxd3=Max(Abs(vx3),Abs(vy3));

			// check if inside			
			VecBoolV inside=  maxd0 < Vec4V(squares.m_spheresR.GetX())
							| maxd1 < Vec4V(squares.m_spheresR.GetY())
							| maxd2 < Vec4V(squares.m_spheresR.GetZ())
							| maxd3 < Vec4V(squares.m_spheresR.GetW());

			zbuffer[y*RAST_TILE_WIDTH+x]=SelectFT( inside, nearV, clearV );
		}
	}
}
void rstScanTile( const ScanTileInfo* listTileInfo, ScanUserData& ud, Vec4V* zbufferOut, size_t zTileSize, sysTaskBuffer& sm )
{
	ScratchMemStackPoint gck(sm);
	Vec4V* zbufferTile = taskAllocBuffer<Vec4V,128>( sm, RAST_TILE_WIDTH * RAST_TILE_HEIGHT);
	CompileTimeAssert( (sizeof(Vec4V) *RAST_TILE_WIDTH * RAST_TILE_HEIGHT)<20*1024);


	if ( listTileInfo->m_squares.enabled)
	{
		ClearShadowTiles(zbufferTile, listTileInfo->m_squares, Vec4V(V_ONE), Vec4V(ScalarV(100000.f)),Vec2V(ud.locationx, ud.locationy));
	}
	else
	{
		ClearTile( zbufferTile, Vec4V(V_ONE));
	}
	for (int k = 0; k < RAST_NUM_TILERS_ACTIVE; k++ )
	{
		ScanTileInfo tInfo = listTileInfo[k];		
		const u16* smallIndicesEA;
		const u16* indicesEA;
		int smallAmt=0;
		int amt=0;
		tInfo.GetAndCalcInfo(TAG_LOADCNTTABLE_8, ud.index, smallIndicesEA, smallAmt,indicesEA, amt);

#if SCAN_SPEW_CRAZY
		Displayf("Tile Info smallindices %0x smallAmt %i indices %0x amt %i", 
			(u32)tInfo.smallIndices, tInfo.smallAmt, (u32)tInfo.indices, tInfo.amt);
#endif

		if ( ud.useSmall && smallAmt > 0 )
		{
			// LoadInto buffer the indices completely
			ScratchMemStackPoint ck(sm);
			// note could preload the first ones of this
			const u16* smallIndices = taskLoadBuffer( "Load Indices",sm, smallIndicesEA, smallAmt , TAG_LOADINDICES);
#if ( __BANK )
			if ( Unlikely(ud.drawBlock))
			{
				ScanTileSmall<true>( smallIndices, smallAmt, tInfo.tris, zbufferTile, Vec2V(ud.locationx, ud.locationy) );
			}
			else
#endif
			{
				ScanTileSmall<false>( smallIndices, smallAmt, tInfo.tris, zbufferTile, Vec2V(ud.locationx, ud.locationy)  );
			}
		}
		if ( Likely( amt> 0))
		{
			ScratchMemStackPoint ck(sm);

			const u16* indices = taskLoadBuffer("Load Indices", sm, indicesEA, amt, TAG_LOADINDICES);
#if ( __BANK  )
			if ( Unlikely(ud.drawBlock) )
			{
				ScanTile2<true>( indices, amt, tInfo.tris, zbufferTile, Vec2V(ud.locationx, ud.locationy)  );
			}
			else
#endif
			{
				ScanTile2<false>(indices, amt, tInfo.tris, zbufferTile, Vec2V(ud.locationx, ud.locationy)  );
			}
		}
		
	}

	if ( ud.HiZTile){

		float hiZTileIndexX = (ud.locationx * (float)RAST_HIZ_TILE_WIDTH / (float)RAST_TILE_WIDTH);
		float hiZTileIndexY = (ud.locationy * (float)RAST_HIZ_TILE_HEIGHT / (float)RAST_TILE_HEIGHT);
		Vec4V tileMin = GenerateHiZTile( zbufferTile, hiZTileIndexX, hiZTileIndexY, ud.HiZTile,ud.HiZStride, ud.minMaxBounds );
		ScalarV tmin= Max( Max(tileMin.GetX(), tileMin.GetY()), Max(tileMin.GetZ(), tileMin.GetW()));
		tmin = Invert( tmin);
		u32 tminI = tmin.Geti();
		taskSetU32( ud.nearZVal, tminI, TAG_WRITEHIZ_0);
	}

	if ( zTileSize > (64*16))
	{
		for (int i = 0; i < RAST_TILE_WIDTH * RAST_TILE_HEIGHT; i++)
		{
			zbufferOut[i]=zbufferTile[i];
		}
	}
}


template<bool writeZ>
RAST_INLINE void StencilEdgeFunctions1x1( Vec4V* __restrict dest,
	Vec4V_In Of1in, Vec4V_In Of2in, Vec4V_In Of3in,   Vec4V_In OfZin,  
	Vec4V_In C, Vec4V_In DX, Vec4V_In DY, int sx, int sy )
{
	Vec4V signMask(V_80000000);
	
	Vec4V C0 = C;

	Vec4V C0X = Vec4V(C0.GetX());
	Vec4V C0Y = Vec4V(C0.GetY());
	Vec4V C0Z = Vec4V(C0.GetZ());
	Vec4V C0W = Vec4V(C0.GetW());

	Vec4V SOf1 = Of1in;
	Vec4V SOf2 = Of2in;
	Vec4V SOf3 = Of3in;
	Vec4V SOfZ = OfZin;
	Vec4V* scandest = dest;

	for(int y=0; y < sy ; y++)
	{
		Vec4V Of1 = SOf1;
		Vec4V Of2 = SOf2;
		Vec4V Of3 = SOf3;
		Vec4V OfZ = SOfZ;
		for (int x = 0; x < sx; x++)
		{
			Vec4V currentZ0 = dest[0];

			VecBoolV mask0 = C0X > Of1;			
			mask0 = mask0 & (C0Y > Of2);
			mask0 = mask0 & (C0Z > Of3);
			Vec4V nZ0 =  C0W - OfZ;

			Of1 -= Vec4V(DX.GetX());
			Of2 -= Vec4V(DX.GetY());
			Of3 -= Vec4V(DX.GetZ());
			OfZ -= Vec4V(DX.GetW());

			mask0 = mask0 & (nZ0 > currentZ0);
				
			if (writeZ) 
				dest[0]=SelectFT( mask0 , currentZ0, nZ0);
			else
				dest[0]=currentZ0 | (Vec4V(mask0) & signMask);							
			dest++;
		}
		scandest +=RAST_HIZ_WIDTH;
		dest = scandest;

		SOf1 -= Vec4V(DY.GetX());
		SOf2 -= Vec4V(DY.GetY());
		SOf3 -= Vec4V(DY.GetZ());
		SOfZ -= Vec4V(DY.GetW());
	}
}

/*
RAST_INLINE void StencilEdgeFunctions4x4( Vec4V* __restrict dest,
	Vec4V_In Of1in, Vec4V_In Of2in, Vec4V_In Of3in,   Vec4V_In OfZin,  
	Vec4V_In C, Vec4V_In DX, Vec4V_In DY, int sx, int sy )
{
	Vec4V signMask(V_80000000);
	Vec4V DY2 = DY * Vec4V(V_TWO);
	Vec4V C0 = C;
	Vec4V C1 = C + DX;
	Vec4V C2 = AddScaled( C, DX, Vec4V(V_TWO));
	Vec4V C3 = AddScaled( C, DX, Vec4V(V_THREE));
	Vec4V DX_X4 = -DX * Vec4V(V_FOUR);

	Vec4V DYX = -Vec4V(DY.GetX());
	Vec4V DYY = -Vec4V(DY.GetY());
	Vec4V DYZ = -Vec4V(DY.GetZ());
	Vec4V DYW = -Vec4V(DY.GetW());

	Vec4V DYX_X2 = DYX+DYX;
	Vec4V DYY_X2 = DYY+DYY;
	Vec4V DYZ_X2 = DYZ+DYZ;
	Vec4V DYW_X2 = DYW+DYW;

	Vec4V C0X = Vec4V(C0.GetX());
	Vec4V C1X = Vec4V(C1.GetX());
	Vec4V C2X = Vec4V(C2.GetX());
	Vec4V C3X = Vec4V(C3.GetX());

	Vec4V C0Y = Vec4V(C0.GetY());
	Vec4V C1Y = Vec4V(C1.GetY());
	Vec4V C2Y = Vec4V(C2.GetY());
	Vec4V C3Y = Vec4V(C3.GetY());

	Vec4V C0Z = Vec4V(C0.GetZ());
	Vec4V C1Z = Vec4V(C1.GetZ());
	Vec4V C2Z = Vec4V(C2.GetZ());
	Vec4V C3Z = Vec4V(C3.GetZ());

	Vec4V C0W = Vec4V(C0.GetW());
	Vec4V C1W = Vec4V(C1.GetW());
	Vec4V C2W = Vec4V(C2.GetW());
	Vec4V C3W = Vec4V(C3.GetW());

	Vec4V SOf1 = Of1in;
	Vec4V SOf2 = Of2in;
	Vec4V SOf3 = Of3in;
	Vec4V SOfZ = OfZin;

	Vec4V* scandest = dest;
	
	// does 4*4 pixel blocks ( 256 pixels at a pop)
	for(int y=0; y < sy ; y++)
	{
		Vec4V Of1 = SOf1;
		Vec4V Of2 = SOf2;
		Vec4V Of3 = SOf3;
		Vec4V OfZ = SOfZ;

		Vec4V Of11 = SOf1 + DYX;
		Vec4V Of12 = SOf2 + DYY;
		Vec4V Of13 = SOf3 + DYZ;
		Vec4V Of1Z = SOfZ + DYW;

		for (int x = 0; x < sx; x++)
		{
			Vec4V currentZ0 = dest[0];
			Vec4V currentZ1 = dest[1];
			Vec4V currentZ2 = dest[2];
			Vec4V currentZ3 = dest[3];
			Vec4V currentZ4 = dest[0+ RAST_HIZ_WIDTH];
			Vec4V currentZ5 = dest[1+ RAST_HIZ_WIDTH];
			Vec4V currentZ6 = dest[2+ RAST_HIZ_WIDTH];
			Vec4V currentZ7 = dest[3+ RAST_HIZ_WIDTH];

			VecBoolV mask0 = C0X > Of1;
			VecBoolV mask1 = C1X > Of1;
			VecBoolV mask2 = C2X > Of1;
			VecBoolV mask3 = C3X > Of1;
			VecBoolV mask4 = C0X > Of11;
			VecBoolV mask5 = C1X > Of11;
			VecBoolV mask6 = C2X > Of11;
			VecBoolV mask7 = C3X > Of11;

			mask0 = mask0 & (C0Y > Of2);
			mask1 = mask1 & (C1Y > Of2);
			mask2 = mask2 & (C2Y > Of2);
			mask3 = mask3 & (C3Y > Of2);
			mask4 = mask4 & (C0Y > Of12);
			mask5 = mask5 & (C1Y > Of12);
			mask6 = mask6 & (C2Y > Of12);
			mask7 = mask7 & (C3Y > Of12);

			mask0 = mask0 & (C0Z > Of3);
			mask1 = mask1 & (C1Z > Of3);
			mask2 = mask2 & (C2Z > Of3);
			mask3 = mask3 & (C3Z > Of3);
			mask4 = mask4 & (C0Z > Of13);
			mask5 = mask5 & (C1Z > Of13);
			mask6 = mask6 & (C2Z > Of13);
			mask7 = mask7 & (C3Z > Of13);

			Vec4V nZ0 =  C0W - OfZ;
			Vec4V nZ1 =  C1W - OfZ;
			Vec4V nZ2 =  C2W - OfZ;
			Vec4V nZ3 =  C3W - OfZ;
			Vec4V nZ4 =  C0W - Of1Z;
			Vec4V nZ5 =  C1W - Of1Z;
			Vec4V nZ6 =  C2W - Of1Z;
			Vec4V nZ7 =  C3W - Of1Z;

			Of1 += Vec4V(DX_X4.GetX());
			Of2 += Vec4V(DX_X4.GetY());
			Of3 += Vec4V(DX_X4.GetZ());
			OfZ += Vec4V(DX_X4.GetW());

			Of11 += Vec4V(DX_X4.GetX());
			Of12 += Vec4V(DX_X4.GetY());
			Of13 += Vec4V(DX_X4.GetZ());
			Of1Z += Vec4V(DX_X4.GetW());

#if !STENCIL_IGNORE_DEPTH
			mask0 = mask0 & (nZ0 > currentZ0);
			mask1 = mask1 & (nZ1 > currentZ1);
			mask2 = mask2 & (nZ2 > currentZ2);
			mask3 = mask3 & (nZ3 > currentZ3);
			mask4 = mask4 & (nZ4 > currentZ4);
			mask5 = mask5 & (nZ5 > currentZ5);
			mask6 = mask6 & (nZ6 > currentZ6);
			mask7 = mask7 & (nZ7 > currentZ7);
#endif // !STENCIL_IGNORE_DEPTH

			Vec4V r0= currentZ0 | (Vec4V(mask0) & signMask);
			Vec4V r1= currentZ1 | (Vec4V(mask1) & signMask);
			Vec4V r2= currentZ2 | (Vec4V(mask2) & signMask);
			Vec4V r3= currentZ3 | (Vec4V(mask3) & signMask);
			Vec4V r4= currentZ4 | (Vec4V(mask4) & signMask);
			Vec4V r5= currentZ5 | (Vec4V(mask5) & signMask);
			Vec4V r6= currentZ6 | (Vec4V(mask6) & signMask);
			Vec4V r7= currentZ7 | (Vec4V(mask7) & signMask);

			dest[0]=r0;
			dest[1]=r1;
			dest[2]=r2;
			dest[3]=r3;
			dest[0+ RAST_HIZ_WIDTH]=r4;
			dest[1+ RAST_HIZ_WIDTH]=r5;
			dest[2+ RAST_HIZ_WIDTH]=r6;
			dest[3+ RAST_HIZ_WIDTH]=r7;
			dest +=4;
		}
		scandest +=RAST_HIZ_WIDTH*2;
		dest = scandest;

		SOf1 += DYX_X2;
		SOf2 += DYY_X2;
		SOf3 += DYZ_X2;
		SOfZ += DYW_X2;
	}
}

void rstClearStencilBuffer( Vec4V* __restrict stencilBuffer )
{
	for (int i = 0; i < RAST_STENCIL_SIZE_VEC; i++)
		stencilBuffer[i]=Vec4V(V_ZERO);
}
*/

void rstSetStencilBits( Vec4V* __restrict HiZBuffer, Vec4V* __restrict stencilBuffer )
{	
	const Vec4V upperNibble = Vec4VConstant<0x80,0x40,0x20,0x10>();
	const Vec4V lowerNibble = Vec4VConstant<0x8,0x4,0x2,0x1>();

	// convert to bit mask
	
	for (int i =0; i < RAST_HIZ_HEIGHT*2;i+=2)
	{
		u8* bitStencil0 =(u8*) &stencilBuffer[i];
		u8* bitStencil1 =(u8*) &stencilBuffer[i+1];

		for (int j =0; j < RAST_HIZ_WIDTH;j+=4)
		{
			int idx=i/2*RAST_HIZ_WIDTH+j;
			Vec4V z0=HiZBuffer[idx];
			Vec4V z1=HiZBuffer[idx+1];
			Vec4V z2=HiZBuffer[idx+2];
			Vec4V z3=HiZBuffer[idx+3];

			HiZBuffer[idx]=Abs(z0);
			HiZBuffer[idx+1]=Abs(z1);
			HiZBuffer[idx+2]=Abs(z2);
			HiZBuffer[idx+3]=Abs(z3);

			Vec4V sb0=GetFromTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>( z0,z1);
			Vec4V sb1=GetFromTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>( z0,z1);
			sb0 = Vec4V( sb0 < Vec4V(V_ZERO) );	
			sb1 = Vec4V( sb1 < Vec4V(V_ZERO) );	
			sb0 = sb0 &upperNibble;
			sb1 = sb1 &upperNibble;

			Vec4V sb2=GetFromTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>( z2,z3);
			Vec4V sb3=GetFromTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>( z2,z3);
			sb2 = Vec4V( sb2 < Vec4V(V_ZERO) );	
			sb3 = Vec4V( sb3 < Vec4V(V_ZERO) );	 

			sb2 = sb2 &lowerNibble;
			sb3 = sb3 &lowerNibble;

			Vec4V tr = sb0 | sb2;
			Vec4V br = sb1 | sb3;
			tr = tr | tr.Get<Vec::Z,Vec::W,Vec::X,Vec::Y>();
			br = br | br.Get<Vec::Z,Vec::W,Vec::X,Vec::Y>();

			tr = tr | Vec4V(tr.GetY());
			br = br | Vec4V(br.GetY());

			bitStencil0[j/4]=(u8)tr.GetXi();
			bitStencil1[j/4]=(u8)br.GetXi();
		}
	}
}


void rstCompareToStencilBits( const Vec4V* __restrict HiZBuffer, const Vec4V* __restrict stencilHiZBuffer, Vec4V* __restrict stencilBuffer )
{
	const Vec4V upperNibble = Vec4VConstant<0x80,0x40,0x20,0x10>();
	const Vec4V lowerNibble = Vec4VConstant<0x8,0x4,0x2,0x1>();

	// convert to bit mask
	for (int i =0; i < RAST_HIZ_HEIGHT*2;i+=2)
	{
		u8* bitStencil0 =(u8*) &stencilBuffer[i];
		u8* bitStencil1 =(u8*) &stencilBuffer[i+1];

		for (int j =0; j < RAST_HIZ_WIDTH;j+=4)
		{
			int idx=i/2*RAST_HIZ_WIDTH+j;
			Vec4V z0=HiZBuffer[idx];
			Vec4V z1=HiZBuffer[idx+1];
			Vec4V z2=HiZBuffer[idx+2];
			Vec4V z3=HiZBuffer[idx+3];

			Vec4V sz0=stencilHiZBuffer[idx];
			Vec4V sz1=stencilHiZBuffer[idx+1];
			Vec4V sz2=stencilHiZBuffer[idx+2];
			Vec4V sz3=stencilHiZBuffer[idx+3];
			
			Vec4V s0 = Vec4V( z0 < sz0);
			Vec4V s1 = Vec4V( z1 < sz1);
			Vec4V s2 = Vec4V( z2 < sz2);
			Vec4V s3 = Vec4V( z3 < sz3);

			
			Vec4V sb0=GetFromTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>( s0,s1);
			Vec4V sb1=GetFromTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>( s0,s1);
			sb0 = sb0 &upperNibble;
			sb1 = sb1 &upperNibble;

			Vec4V sb2=GetFromTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>( s2,s3);
			Vec4V sb3=GetFromTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>( s2,s3);		
			sb2 = sb2 &lowerNibble;
			sb3 = sb3 &lowerNibble;

			Vec4V tr = sb0 | sb2;
			Vec4V br = sb1 | sb3;
			tr = tr | tr.Get<Vec::Z,Vec::W,Vec::X,Vec::Y>();
			br = br | br.Get<Vec::Z,Vec::W,Vec::X,Vec::Y>();

			tr = tr | Vec4V(tr.GetY());
			br = br | Vec4V(br.GetY());

			int ix=j/4;
#if RSG_PC || RSG_DURANGO || RSG_ORBIS
			ix = (ix/4)*4 + 3-(ix&3);  // byte swap
#endif
			bitStencil0[ix]=(u8)tr.GetXi();
			bitStencil1[ix]=(u8)br.GetXi();
		}
	}
}


void rstStencilHiZ(Vec4V* __restrict HiZBuffer, const PreTri2* tris, int amt, const struct StencilWriterUserData&
#if STENCIL_WRITER_DEBUG
				   ud
#endif // STENCIL_WRITER_DEBUG
				 , bool writeZ  )
{
	const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	Vec4V edge(Vec2V(ScalarVFromF32(RAST_HIZ_WIDTH)), Vec2V(ScalarVFromF32(RAST_HIZ_HEIGHT)));

#if STENCIL_WRITER_DEBUG
	const Vec4V debugBoundsExpand = Vec4V(
		-(float)ud.debugBoundsExpandX0,
		+(float)ud.debugBoundsExpandX1,
		-(float)ud.debugBoundsExpandY0,
		+(float)ud.debugBoundsExpandY1
	);
#endif // STENCIL_WRITER_DEBUG

	for (int i =0; i < amt; i++)
	{
		const PreTri2& p = tris[i];
		Vec4V DX = -p.DY;
		Vec4V DY = p.DX;
		Vec4V C = p.C;
		Vec4V intMinMaxV =  p.minMax;

		intMinMaxV += Vec4V(V_Y_AXIS_WONE); // add {0,1,0,1} to prevent cracks
#if STENCIL_WRITER_DEBUG
		intMinMaxV += debugBoundsExpand;
#endif // STENCIL_WRITER_DEBUG

		Vec4V Of1 = -AddScaled(DY.GetX() * offsetY, offsetX, DX.GetX());
		Vec4V Of2 = -AddScaled(DY.GetY() * offsetY, offsetX, DX.GetY());
		Vec4V Of3 = -AddScaled(DY.GetZ() * offsetY, offsetX, DX.GetZ());
		Vec4V OfZ = -AddScaled(DY.GetW() * offsetY, offsetX, DX.GetW());


		// if overlaps edges pull in so never overwrites.
		intMinMaxV = Max( intMinMaxV, Vec4V(V_ZERO) );
		intMinMaxV = Min( intMinMaxV, edge);

		Vec4V pos = intMinMaxV;

		Vec4V actIntMin = FloatToIntRaw<0>(intMinMaxV);
		int* loc = (int*)&actIntMin;
		
		Vec4V* __restrict dest = HiZBuffer + loc[0] + loc[2]*RAST_HIZ_WIDTH;
		Vec4V Cs = AddScaled(AddScaled(C, DY, pos.GetZ()), DX, pos.GetX());
		int sx = (loc[1]-loc[0]);
		int sy = (loc[3]-loc[2]);
		if ( writeZ)
			StencilEdgeFunctions1x1<true>( dest, Of1, Of2,  Of3,  OfZ, Cs, DX, DY, sx,sy );
		else
			StencilEdgeFunctions1x1<false>( dest, Of1, Of2,  Of3,  OfZ, Cs, DX, DY, sx,sy );
	}
}
#if __BANK
bool g_ShowBoundsInHiZ = false;
#endif

RAST_INLINE bool Test2EdgeListsSimple(  const Vec4V* __restrict src,
											  Vec4V_In Of1in, Vec4V_In Of2in, Vec4V_In Of3in,   Vec4V_In Of4in,   Vec4V_In Of5in,  Vec4V_In Of6in, 
											  Vec3V_In C0v, Vec3V_In DX0, Vec3V_In DY0,Vec3V_In C1v, Vec3V_In DX1, Vec3V_In DY1, int sx, int sy, Vec4V_In minmax,
											  Vec4V_In extents, ScalarV_In vminZ 
											  DEV_ONLY(, u32* trivialAcceptVisiblePixel) BANK_ONLY(, bool useTrivialAcceptVisiblePixelTest))
{
	VecBoolV  countMask(Vec4V(V_ONE).GetIntrin128());
	const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	const Vec4V quater = Vec4V(V_QUARTER);

	Vec4V C1 = Vec4V(C0v.GetX());	
	Vec4V C2 = Vec4V(C0v.GetY());
	Vec4V C3 = Vec4V(C0v.GetZ());
	Vec4V C4 = Vec4V(C1v.GetX());
	Vec4V C5 = Vec4V(C1v.GetY());
	Vec4V C6 = Vec4V(C1v.GetZ());

	Vec4V SOf1 = Of1in;
	Vec4V SOf2 = Of2in;
	Vec4V SOf3 = Of3in;
	Vec4V SOf4 = Of4in;
	Vec4V SOf5 = Of5in;
	Vec4V SOf6 = Of6in;
	
	const Vec4V*  __restrict scansrc = src;
	const Vec4V*  __restrict srcptr = src;
	//Vec4V c0(V_ZERO);
	VecBoolV result(V_F_F_F_F);

	// 2D extents calculation annoying
	Vec2V ppStartV = RoundToNearestIntZero(minmax.Get<Vec::X, Vec::Z>() * Vec2V(V_TWO)) * Vec2V(V_HALF);
	Vec2V ppEndV = RoundToNearestIntPosInf(minmax.Get<Vec::Y, Vec::W>() * Vec2V(V_TWO)) * Vec2V(V_HALF);

	Vec2V midP = ( ppStartV + ppEndV )* Vec2V(V_HALF);
	Vec2V bounds = ( ppEndV - ppStartV ) * Vec2V(V_HALF);

	Vec4V boundY = Vec4V(bounds.GetY()); 
	Vec4V boundX = Vec4V(bounds.GetX());

	Vec4V sxV =  Vec4V(extents.GetX()) + quater + offsetX - Vec4V(midP.GetX());
	Vec4V posY =  Vec4V(extents.GetZ()) + quater + offsetY - Vec4V(midP.GetY());

	Vec4V minZ = Vec4V(Invert(vminZ));
	VecBoolV inRangeX[RAST_HIZ_WIDTH];
	Vec4V posX0 = sxV;
	for (int x = 0; x < sx; x++)  // could do first and last bounds check outside loop?
	{
		Vec4V pX0 = Abs(posX0);
		posX0 += Vec4V(V_ONE);
		inRangeX[x]=pX0 <= boundX;
	}

	// does single pixel blocks ( 4 pixels at a pop)
	for(int y=0; y < sy ; y++)
	{
		if(BANK_ONLY(useTrivialAcceptVisiblePixelTest && )!IsFalseAll(result))
		{
#if __DEV
			sysInterlockedIncrement(trivialAcceptVisiblePixel);
#endif	
			return true;
		}
		Vec4V Of1 = SOf1;
		Vec4V Of2 = SOf2;
		Vec4V Of3 = SOf3;
		Vec4V Of4 = SOf4;
		Vec4V Of5 = SOf5;
		Vec4V Of6 = SOf6;

		Vec4V absPosY = Abs(posY);
		VecBoolV inRangeY =  absPosY <= boundY;

		for (int x = 0; x < sx; x++)  // could do first and last bounds check outside loop?
		{
			const Vec4V currentZ0 = *srcptr;	

			VecBoolV cp1 = C1 > Of1;	
			VecBoolV cp2 = C2 > Of2;
			VecBoolV cp3 = C3 > Of3;
			VecBoolV cp4 = C4 > Of4;	
			VecBoolV cp5 = C5 > Of5;
			VecBoolV cp6 = C6 > Of6;

			Of1 -= Vec4V(DX0.GetX());
			Of2 -= Vec4V(DX0.GetY());
			Of3 -= Vec4V(DX0.GetZ());
			Of4 -= Vec4V(DX1.GetX());
			Of5 -= Vec4V(DX1.GetY());
			Of6 -= Vec4V(DX1.GetZ());

			VecBoolV inbounds0 = inRangeX[x]&inRangeY;  // test 2D extents			
			VecBoolV inPoly = (inbounds0 & cp1 & cp2 & cp3 & cp4 & cp5 & cp6 );
			VecBoolV inPolyAndDepth = inPoly & (minZ > currentZ0);
			result = result | inPolyAndDepth;			

#if __BANK && 0//to be used only in debug 		
			//if(g_ShowBoundsInHiZ)
			static dev_bool showBounds = false;
			if( showBounds )
			{	
				static bool applybounds = false;
				static bool showrange = false;
				if (applybounds)
					inPolyAndDepth = inbounds0;
				if (showrange)
					inPolyAndDepth= VecBoolV(V_T_T_T_T);

				Vec4V *src2 = const_cast<Vec4V*>(srcptr);
				*src2 =SelectFT( inPolyAndDepth, srcptr[0], Vec4V(ScalarV(10000.f)));		
			}		
#endif 
			
			srcptr++;
		}
		scansrc +=RAST_HIZ_WIDTH;
		srcptr = scansrc;
		posY += Vec4V(V_ONE);

		SOf1 -= Vec4V(DY0.GetX());
		SOf2 -= Vec4V(DY0.GetY());
		SOf3 -= Vec4V(DY0.GetZ());
		SOf4 -= Vec4V(DY1.GetX());
		SOf5 -= Vec4V(DY1.GetY());
		SOf6 -= Vec4V(DY1.GetZ());
	}
	return (!IsFalseAll(result));
}

RAST_INLINE bool Test3EdgeListsSimple(  const Vec4V* __restrict src,
									  Vec4V_In Of1in, Vec4V_In Of2in, Vec4V_In Of3in,   Vec4V_In Of4in,   Vec4V_In Of5in,  Vec4V_In Of6in, Vec4V_In Of7in,   Vec4V_In Of8in,  Vec4V_In Of9in, 
									  Vec3V_In C0v, Vec3V_In DX0, Vec3V_In DY0,Vec3V_In C1v, Vec3V_In DX1, Vec3V_In DY1,Vec3V_In C2v, Vec3V_In DX2, Vec3V_In DY2, int sx, int sy, Vec4V_In minmax,
									  Vec4V_In extents, ScalarV_In vminZ 
									  DEV_ONLY(, u32* trivialAcceptVisiblePixel) BANK_ONLY(, bool useTrivialAcceptVisiblePixelTest))
{
	VecBoolV  countMask(Vec4V(V_ONE).GetIntrin128());
	const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	const Vec4V quater = Vec4V(V_QUARTER);

	Vec4V C1 = Vec4V(C0v.GetX());	
	Vec4V C2 = Vec4V(C0v.GetY());
	Vec4V C3 = Vec4V(C0v.GetZ());
	Vec4V C4 = Vec4V(C1v.GetX());
	Vec4V C5 = Vec4V(C1v.GetY());
	Vec4V C6 = Vec4V(C1v.GetZ());
	Vec4V C7 = Vec4V(C2v.GetX());
	Vec4V C8 = Vec4V(C2v.GetY());
	Vec4V C9 = Vec4V(C2v.GetZ());

	Vec4V SOf1 = Of1in;
	Vec4V SOf2 = Of2in;
	Vec4V SOf3 = Of3in;
	Vec4V SOf4 = Of4in;
	Vec4V SOf5 = Of5in;
	Vec4V SOf6 = Of6in;
	Vec4V SOf7 = Of7in;
	Vec4V SOf8 = Of8in;
	Vec4V SOf9 = Of9in;

	const Vec4V*  __restrict scansrc = src;
	const Vec4V*  __restrict srcptr = src;
	//Vec4V c0(V_ZERO);
	VecBoolV result(V_F_F_F_F);

	// 2D extents calculation annoying
	Vec2V ppStartV = RoundToNearestIntZero(minmax.Get<Vec::X, Vec::Z>() * Vec2V(V_TWO)) * Vec2V(V_HALF);
	Vec2V ppEndV = RoundToNearestIntPosInf(minmax.Get<Vec::Y, Vec::W>() * Vec2V(V_TWO)) * Vec2V(V_HALF);

	Vec2V midP = ( ppStartV + ppEndV )* Vec2V(V_HALF);
	Vec2V bounds = ( ppEndV - ppStartV ) * Vec2V(V_HALF);

	Vec4V boundY = Vec4V(bounds.GetY()); 
	Vec4V boundX = Vec4V(bounds.GetX());

	Vec4V sxV =  Vec4V(extents.GetX()) + quater + offsetX - Vec4V(midP.GetX());
	Vec4V posY =  Vec4V(extents.GetZ()) + quater + offsetY - Vec4V(midP.GetY());

	Vec4V minZ = Vec4V(Invert(vminZ));
	VecBoolV inRangeX[RAST_HIZ_WIDTH];
	Vec4V posX0 = sxV;
	for (int x = 0; x < sx; x++)  // could do first and last bounds check outside loop?
	{
		Vec4V pX0 = Abs(posX0);
		posX0 += Vec4V(V_ONE);
		inRangeX[x]=pX0 <= boundX;
	}

	// does single pixel blocks ( 4 pixels at a pop)
	for(int y=0; y < sy ; y++)
	{
		if(BANK_ONLY(useTrivialAcceptVisiblePixelTest && )!IsFalseAll(result))
		{
#if __DEV
			sysInterlockedIncrement(trivialAcceptVisiblePixel);
#endif	
			return true;
		}
		Vec4V Of1 = SOf1;
		Vec4V Of2 = SOf2;
		Vec4V Of3 = SOf3;
		Vec4V Of4 = SOf4;
		Vec4V Of5 = SOf5;
		Vec4V Of6 = SOf6;
		Vec4V Of7 = SOf7;
		Vec4V Of8 = SOf8;
		Vec4V Of9 = SOf9;

		Vec4V absPosY = Abs(posY);
		VecBoolV inRangeY =  absPosY <= boundY;

		for (int x = 0; x < sx; x++)  // could do first and last bounds check outside loop?
		{
			const Vec4V currentZ0 = *srcptr;	

			VecBoolV cp1 = C1 > Of1;	
			VecBoolV cp2 = C2 > Of2;
			VecBoolV cp3 = C3 > Of3;
			VecBoolV cp4 = C4 > Of4;	
			VecBoolV cp5 = C5 > Of5;
			VecBoolV cp6 = C6 > Of6;
			VecBoolV cp7 = C7 > Of7;	
			VecBoolV cp8 = C8 > Of8;
			VecBoolV cp9 = C9 > Of9;

			Of1 -= Vec4V(DX0.GetX());
			Of2 -= Vec4V(DX0.GetY());
			Of3 -= Vec4V(DX0.GetZ());
			Of4 -= Vec4V(DX1.GetX());
			Of5 -= Vec4V(DX1.GetY());
			Of6 -= Vec4V(DX1.GetZ());
			Of7 -= Vec4V(DX2.GetX());
			Of8 -= Vec4V(DX2.GetY());
			Of9 -= Vec4V(DX2.GetZ());

			VecBoolV inbounds0 = inRangeX[x]&inRangeY;  // test 2D extents			
			VecBoolV inPoly = (inbounds0 & cp1 & cp2 & cp3 & cp4 & cp5 & cp6 & cp7 & cp8 & cp9 );
			VecBoolV inPolyAndDepth = inPoly & (minZ > currentZ0);
			result = result | inPolyAndDepth;			

#if __BANK && 0//to be used only in debug 		
			//if(g_ShowBoundsInHiZ)
			static dev_bool showBounds = false;
			if( showBounds )
			{	
				static bool applybounds = false;
				static bool showrange = false;
				if (applybounds)
					inPolyAndDepth = inbounds0;
				if (showrange)
					inPolyAndDepth= VecBoolV(V_T_T_T_T);

				Vec4V *src2 = const_cast<Vec4V*>(srcptr);
				*src2 =SelectFT( inPolyAndDepth, srcptr[0], Vec4V(ScalarV(10000.f)));		
			}		
#endif 

			srcptr++;
		}
		scansrc +=RAST_HIZ_WIDTH;
		srcptr = scansrc;
		posY += Vec4V(V_ONE);

		SOf1 -= Vec4V(DY0.GetX());
		SOf2 -= Vec4V(DY0.GetY());
		SOf3 -= Vec4V(DY0.GetZ());
		SOf4 -= Vec4V(DY1.GetX());
		SOf5 -= Vec4V(DY1.GetY());
		SOf6 -= Vec4V(DY1.GetZ());
		SOf7 -= Vec4V(DY2.GetX());
		SOf8 -= Vec4V(DY2.GetY());
		SOf9 -= Vec4V(DY2.GetZ());
	}
	return (!IsFalseAll(result));
}

// Test Pixel Count
RAST_INLINE Vec4V_Out TestEdgeFunctionsSimple(  const Vec4V* __restrict src,
										   Vec4V_In Of1in, Vec4V_In Of2in, Vec4V_In Of3in,   Vec4V_In OfZin,  
										   Vec4V_In C, Vec4V_In DX, Vec4V_In DY, int sx, int sy, Vec4V_In minmax,
										   Vec4V_In extents, ScalarV_In vminZ )
{
	VecBoolV  countMask(Vec4V(V_ONE).GetIntrin128());
	const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	const Vec4V quater = Vec4V(V_QUARTER);
	Vec4V DX_X4 = -DX;

	Vec4V C0X = Vec4V(C.GetX());	
	Vec4V C0Y = Vec4V(C.GetY());
	Vec4V C0Z = Vec4V(C.GetZ());
	Vec4V C0W = Vec4V(C.GetW());
	
	Vec4V C1 = C - DX_X4;
	Vec4V C1X = Vec4V(C1.GetX());
	Vec4V C1Y = Vec4V(C1.GetY());
	Vec4V C1Z = Vec4V(C1.GetZ());
	Vec4V C1W = Vec4V(C1.GetW());

	DX_X4 *= Vec4V(V_TWO);

	Vec4V SOf1 = Of1in;
	Vec4V SOf2 = Of2in;
	Vec4V SOf3 = Of3in;
	Vec4V SOfZ = OfZin;

	const Vec4V*  __restrict scansrc = src;
	const Vec4V*  __restrict srcptr = src;
	Vec4V c0(V_ZERO);
	Vec4V c1(V_ZERO);

	Vec2V ppStartV = RoundToNearestIntZero(minmax.Get<Vec::X, Vec::Z>() * Vec2V(V_TWO)) * Vec2V(V_HALF);
	Vec2V ppEndV = RoundToNearestIntPosInf(minmax.Get<Vec::Y, Vec::W>() * Vec2V(V_TWO)) * Vec2V(V_HALF);

	Vec2V midP = ( ppStartV + ppEndV )* Vec2V(V_HALF);
	Vec2V bounds = ( ppEndV - ppStartV ) * Vec2V(V_HALF);

	Vec4V boundY = Vec4V(bounds.GetY()); 
	Vec4V boundX = Vec4V(bounds.GetX());

	Vec4V sxV =  Vec4V(extents.GetX()) + quater + offsetX - Vec4V(midP.GetX());
	Vec4V posY =  Vec4V(extents.GetZ()) + quater + offsetY - Vec4V(midP.GetY());
	
	ScalarV minZ = Invert(vminZ);


	// does single pixel blocks ( 4 pixels at a pop)
	for(int y=0; y < sy ; y++)
	{
		Vec4V Of1 = SOf1;
		Vec4V Of2 = SOf2;
		Vec4V Of3 = SOf3;
		Vec4V OfZ = SOfZ;

		Vec4V posX0 = sxV;
		Vec4V posX1 = sxV+ Vec4V(V_ONE);
		Vec4V absPosY = Abs(posY);
		VecBoolV inRangeY =  absPosY <= boundY;

		Vec4V bounds0 = SelectFT( inRangeY , Vec4V(V_NEGONE), boundX);
		
		// -- pipeline comparisons
		for (int x = 0; x < sx; x+=2)  // could do first and last bounds check outside loop?
		{
		
			// pipeline this boyo
			Vec4V nZ0 =  C0W - OfZ;	
			Vec4V nZ1 =  C1W - OfZ;

			Vec4V pX0 = Abs(posX0);
			Vec4V pX1 = Abs(posX1);
			const Vec4V currentZ0 = srcptr[0];	
			const Vec4V currentZ1 = srcptr[1];	

			VecBoolV cp0_x = C0X > Of1;	
			VecBoolV cp1_x = C1X > Of1;			

			VecBoolV cp0_y = C0Y > Of2;
			VecBoolV cp1_y = C1Y > Of2;

			VecBoolV cp0_z = C0Z > Of3;
			VecBoolV cp1_z = C1Z > Of3;
			

			// dep
			nZ0 = Min( Vec4V(minZ), nZ0);
			nZ1 = Min( Vec4V(minZ), nZ1);

			Of1 += Vec4V(DX_X4.GetX());
			Of2 += Vec4V(DX_X4.GetY());
			Of3 += Vec4V(DX_X4.GetZ());
			OfZ += Vec4V(DX_X4.GetW());
			

			VecBoolV inbounds0 = pX0 <= bounds0;
			VecBoolV inbounds1 = pX1 <= bounds0;
			posX0 += Vec4V(V_TWO);
			posX1 += Vec4V(V_TWO);

			// dep
			VecBoolV cp0_zv = (nZ0 > currentZ0);	
			VecBoolV cp1_zv = (nZ1 > currentZ1);	

			VecBoolV mk_0 = (inbounds0 & cp0_x);
			VecBoolV mk_1  = (inbounds1 & cp1_x);	
			VecBoolV mk_0_yz = (cp0_y & cp0_z);
			VecBoolV mk_1_yz = (cp1_y & cp1_z);
			
			
			VecBoolV mask0 = mk_0 & cp0_zv & mk_0_yz;	
			VecBoolV mask1 = mk_1 & cp1_zv & mk_1_yz;	

			// dep
			c0 += Vec4V( (mask0&countMask).GetIntrin128() );	
			c1 += Vec4V( (mask1&countMask).GetIntrin128() );

#if DEBUG_RASTERIZER			
			static bool applybounds = true;
			static bool showrange = false;
			if (applybounds)
				mask0 = mask0 & inbounds0;
			if (showrange)
				mask0= VecBoolV(V_T_T_T_T);

#endif 
			//Vec4V *src2 = const_cast<Vec4V*>(srcptr);
			//src2[0] =srcptr[0]& Vec4V((!mask0).GetIntrin128() );	
			//src2[1] =srcptr[1]& Vec4V((!mask1).GetIntrin128() );	
			srcptr+=2;
			
		}

		scansrc +=RAST_HIZ_WIDTH;
		srcptr = scansrc;
		posY += Vec4V(V_ONE);

		SOf1 -= Vec4V(DY.GetX());
		SOf2 -= Vec4V(DY.GetY());
		SOf3 -= Vec4V(DY.GetZ());
		SOfZ -= Vec4V(DY.GetW());
	}
	return c0+c1;
}

RAST_INLINE Vec4V_Out TestEdgeFunctions4x4( const Vec4V* __restrict src,
	Vec4V_In Of1in, Vec4V_In Of2in, Vec4V_In Of3in,   Vec4V_In OfZin,  
	Vec4V_In C, Vec4V_In DX, Vec4V_In DY, int sx, int sy )
{
	VecBoolV  countMask(Vec4V(V_ONE).GetIntrin128());

	// Vec4V DY2 = DY * Vec4V(V_TWO);
	Vec4V C0 = C;
	Vec4V C1 = C + DX;
	Vec4V C2 = AddScaled( C, DX, Vec4V(V_TWO));
	Vec4V C3 = AddScaled( C, DX, Vec4V(V_THREE));
	Vec4V DX_X4 = -DX * Vec4V(V_FOUR);

	Vec4V DYX = -Vec4V(DY.GetX());
	Vec4V DYY = -Vec4V(DY.GetY());
	Vec4V DYZ = -Vec4V(DY.GetZ());
	Vec4V DYW = -Vec4V(DY.GetW());

	Vec4V DYX_X2 = DYX + DYX;
	Vec4V DYY_X2 = DYY + DYY;
	Vec4V DYZ_X2 = DYZ + DYZ;
	Vec4V DYW_X2 = DYW + DYW;

	Vec4V C0X = Vec4V(C0.GetX());
	Vec4V C1X = Vec4V(C1.GetX());
	Vec4V C2X = Vec4V(C2.GetX());
	Vec4V C3X = Vec4V(C3.GetX());

	Vec4V C0Y = Vec4V(C0.GetY());
	Vec4V C1Y = Vec4V(C1.GetY());
	Vec4V C2Y = Vec4V(C2.GetY());
	Vec4V C3Y = Vec4V(C3.GetY());

	Vec4V C0Z = Vec4V(C0.GetZ());
	Vec4V C1Z = Vec4V(C1.GetZ());
	Vec4V C2Z = Vec4V(C2.GetZ());
	Vec4V C3Z = Vec4V(C3.GetZ());

	Vec4V C0W = Vec4V(C0.GetW());
	Vec4V C1W = Vec4V(C1.GetW());
	Vec4V C2W = Vec4V(C2.GetW());
	Vec4V C3W = Vec4V(C3.GetW());

	Vec4V SOf1 = Of1in;
	Vec4V SOf2 = Of2in;
	Vec4V SOf3 = Of3in;
	Vec4V SOfZ = OfZin;

	const Vec4V*  __restrict scansrc = src;

	Vec4V c0(V_ZERO);
	Vec4V c1(V_ZERO);
	Vec4V c2(V_ZERO);
	Vec4V c3(V_ZERO);
	Vec4V c4(V_ZERO);
	Vec4V c5(V_ZERO);
	Vec4V c6(V_ZERO);
	Vec4V c7(V_ZERO);
	
	// does 4*4 pixel blocks ( 256 pixels at a pop)
	for(int y=0; y < sy ; y++)
	{
		Vec4V Of1 = SOf1;
		Vec4V Of2 = SOf2;
		Vec4V Of3 = SOf3;
		Vec4V OfZ = SOfZ;

		Vec4V Of11 = SOf1  + DYX;
		Vec4V Of12 = SOf2 + DYY;
		Vec4V Of13 = SOf3 + DYZ;
		Vec4V Of1Z = SOfZ + DYW;

		for (int x = 0; x < sx; x++)
		{
			const Vec4V currentZ0 = src[0];
			const Vec4V currentZ1 = src[1];
			const Vec4V currentZ2 = src[2];
			const Vec4V currentZ3 = src[3];
			const Vec4V currentZ4 = src[0+ RAST_HIZ_WIDTH];
			const Vec4V currentZ5 = src[1+ RAST_HIZ_WIDTH];
			const Vec4V currentZ6 = src[2+ RAST_HIZ_WIDTH];
			const Vec4V currentZ7 = src[3+ RAST_HIZ_WIDTH];

			VecBoolV mask0 = C0X > Of1;
			VecBoolV mask1 = C1X > Of1;
			VecBoolV mask2 = C2X > Of1;
			VecBoolV mask3 = C3X > Of1;
			VecBoolV mask4 = C0X > Of11;
			VecBoolV mask5 = C1X > Of11;
			VecBoolV mask6 = C2X > Of11;
			VecBoolV mask7 = C3X > Of11;

			mask0 = mask0 & (C0Y > Of2);
			mask1 = mask1 & (C1Y > Of2);
			mask2 = mask2 & (C2Y > Of2);
			mask3 = mask3 & (C3Y > Of2);
			mask4 = mask4 & (C0Y > Of12);
			mask5 = mask5 & (C1Y > Of12);
			mask6 = mask6 & (C2Y > Of12);
			mask7 = mask7 & (C3Y > Of12);

			mask0 = mask0 & (C0Z > Of3);
			mask1 = mask1 & (C1Z > Of3);
			mask2 = mask2 & (C2Z > Of3);
			mask3 = mask3 & (C3Z > Of3);
			mask4 = mask4 & (C0Z > Of13);
			mask5 = mask5 & (C1Z > Of13);
			mask6 = mask6 & (C2Z > Of13);
			mask7 = mask7 & (C3Z > Of13);

			const Vec4V nZ0 =  C0W - OfZ;
			const Vec4V nZ1 =  C1W - OfZ;
			const Vec4V nZ2 =  C2W - OfZ;
			const Vec4V nZ3 =  C3W - OfZ;
			const Vec4V nZ4 =  C0W - Of1Z;
			const Vec4V nZ5 =  C1W - Of1Z;
			const Vec4V nZ6 =  C2W - Of1Z;
			const Vec4V nZ7 =  C3W - Of1Z;

			Of1 += Vec4V(DX_X4.GetX());
			Of2 += Vec4V(DX_X4.GetY());
			Of3 += Vec4V(DX_X4.GetZ());
			OfZ += Vec4V(DX_X4.GetW());

			Of11 += Vec4V(DX_X4.GetX());
			Of12 += Vec4V(DX_X4.GetY());
			Of13 += Vec4V(DX_X4.GetZ());
			Of1Z += Vec4V(DX_X4.GetW());

			mask0 = mask0 & (nZ0 > currentZ0);
			mask1 = mask1 & (nZ1 > currentZ1);
			mask2 = mask2 & (nZ2 > currentZ2);
			mask3 = mask3 & (nZ3 > currentZ3);
			mask4 = mask4 & (nZ4 > currentZ4);
			mask5 = mask5 & (nZ5 > currentZ5);
			mask6 = mask6 & (nZ6 > currentZ6);
			mask7 = mask7 & (nZ7 > currentZ7);
			
			c0 += Vec4V( (mask0&countMask).GetIntrin128() );
			c1 += Vec4V( (mask1&countMask).GetIntrin128() );
			c2 += Vec4V( (mask2&countMask).GetIntrin128() );
			c3 += Vec4V( (mask3&countMask).GetIntrin128() );
			c4 += Vec4V( (mask4&countMask).GetIntrin128() );
			c5 += Vec4V( (mask5&countMask).GetIntrin128() );
			c6 += Vec4V( (mask6&countMask).GetIntrin128() );
			c7 += Vec4V( (mask7&countMask).GetIntrin128() );
			
			src +=4;
		}
		scansrc +=RAST_HIZ_WIDTH*2;
		src = scansrc;

		SOf1 += DYX_X2;
		SOf2 += DYY_X2;
		SOf3 += DYZ_X2;
		SOfZ += DYW_X2;
	}
	c0 += c1;
	c2 += c3;
	c4 += c5;
	c6 += c7;

	c0 += c2;
	c4 += c6;

	return c0 + c4;
}


 ScalarV_Out rstTestHiZ( const Vec4V* __restrict HiZBuffer, const PreTri2* tris, ScalarV* minZ, int amt)
{
	const Vec4V edge(Vec2V(ScalarVFromF32(RAST_HIZ_WIDTH)), Vec2V(ScalarVFromF32(RAST_HIZ_HEIGHT)));

	if ( amt == -1)
	{
		return edge.GetX()*edge.GetZ();
	}
	const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();
	
	Vec4V sum(V_ZERO);
	for (int i =0; i < amt; i++)
	{

		const PreTri2& p = tris[i];
		Vec4V DX = -p.DY;
		Vec4V DY = p.DX;
		Vec4V C = p.C;
		Vec4V intMinMaxV =  p.minMax;
		Vec4V currentMinMaxV =  p.minMax;
	 
		Vec4V Of1 = -AddScaled(DY.GetX() * offsetY, offsetX, DX.GetX());
		Vec4V Of2 = -AddScaled(DY.GetY() * offsetY, offsetX, DX.GetY());
		Vec4V Of3 = -AddScaled(DY.GetZ() * offsetY, offsetX, DX.GetZ());
		Vec4V OfZ = -AddScaled(DY.GetW() * offsetY, offsetX, DX.GetW());

		intMinMaxV *= Vec4VConstant<U32_HALF, U32_HALF, U32_ONE, U32_ONE>();
		// if overlaps edges pull in so never overwrites.
		intMinMaxV = Vec4V(	RoundToNearestIntNegInf(intMinMaxV.Get<Vec::X, Vec::Z>()),
							RoundToNearestIntPosInf(intMinMaxV.Get<Vec::Y, Vec::W>()));

		intMinMaxV *= Vec4VConstant<U32_TWO, U32_ONE, U32_TWO, U32_ONE>();
		intMinMaxV = intMinMaxV.Get<Vec::X, Vec::Z, Vec::Y, Vec::W>();

		intMinMaxV = Clamp( intMinMaxV, Vec4V(V_ZERO), edge);		

		Vec4V pos = intMinMaxV;
		Vec4V actIntMin = FloatToIntRaw<0>(intMinMaxV);
		int* loc = (int*)&actIntMin;

		Vec4V conservativeRange = Vec4V(p.GetTrivalReject( ScalarV(V_HALF), ScalarV(V_HALF)), ScalarV(V_ZERO));

		
		const Vec4V* __restrict src = HiZBuffer + loc[0] + loc[2]*RAST_HIZ_WIDTH;
		Vec4V Cs = AddScaled(AddScaled(C, DY, pos.GetZ()), DX, pos.GetX()) - conservativeRange;
		const bool useSimple = true;
		if ( useSimple)
		{
			// round up to for 2 for loop unrolling
			Assert( loc[1] <= RAST_HIZ_WIDTH);

			int sx = loc[1]-loc[0];			
			int sy = loc[3]-loc[2];
			Assert( (sx&0x1)==0);
			sum += TestEdgeFunctionsSimple( src, Of1, Of2,  Of3,  OfZ, Cs, DX, DY, sx,sy ,currentMinMaxV, pos, minZ[i]);		
		}
		else
		{
			int sx = ((loc[1]-loc[0])>>2)+1;
			int sy = ((loc[3]-loc[2])>>1)+1;
			sum += TestEdgeFunctions4x4( src, Of1, Of2,  Of3,  OfZ, Cs, DX, DY, sx,sy );
		}		
	}
	return Dot(sum, Vec4V(V_ONE));

}

// does up to six edges
 ScalarV_Out rstTest2EdgeListsHiZ( const Vec4V* __restrict HiZBuffer, const EdgeList& edge0,const EdgeList& edge1, ScalarV_In minZ, Vec4V_In minMaxV
	 DEV_ONLY(, u32* trivialAcceptVisiblePixel) BANK_ONLY(, bool useTrivialAcceptVisiblePixelTest))
 {
	 const Vec4V edge(Vec2V(ScalarVFromF32(RAST_HIZ_WIDTH)), Vec2V(ScalarVFromF32(RAST_HIZ_HEIGHT)));
	 const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	 const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();

	 Vec3V DX0 = -edge0.DY;
	 Vec3V DY0 = edge0.DX;
	 Vec3V C0 = edge0.C;
	 Vec3V DX1 = -edge1.DY;
	 Vec3V DY1 = edge1.DX;
	 Vec3V C1 = edge1.C;

	// calculate edge offsets
	 Vec4V Of1 = -AddScaled(DY0.GetX() * offsetY, offsetX, DX0.GetX());
	 Vec4V Of2 = -AddScaled(DY0.GetY() * offsetY, offsetX, DX0.GetY());
	 Vec4V Of3 = -AddScaled(DY0.GetZ() * offsetY, offsetX, DX0.GetZ());

	 Vec4V Of4 = -AddScaled(DY1.GetX() * offsetY, offsetX, DX1.GetX());
	 Vec4V Of5 = -AddScaled(DY1.GetY() * offsetY, offsetX, DX1.GetY());
	 Vec4V Of6 = -AddScaled(DY1.GetZ() * offsetY, offsetX, DX1.GetZ());
	 
	 Vec4V intMinMaxV = minMaxV;

	 // if overlaps edges pull in so never overwrites.
	 intMinMaxV = Vec4V(	RoundToNearestIntNegInf(intMinMaxV.Get<Vec::X, Vec::Z>()),
							RoundToNearestIntPosInf(intMinMaxV.Get<Vec::Y, Vec::W>()));

	 intMinMaxV = intMinMaxV.Get<Vec::X, Vec::Z, Vec::Y, Vec::W>();

	 intMinMaxV = Clamp( intMinMaxV, Vec4V(V_ZERO), edge);		

	 Vec4V pos = intMinMaxV;
	 Vec4V actIntMin = FloatToIntRaw<0>(intMinMaxV);
	 int* loc = (int*)&actIntMin;

	 // Shift to half texel so conservative tests.
	Vec3V zero(V_ZERO);
	Vec3V tRejectCX0 = SelectFT( DY0 < zero, zero, Vec3V(V_HALF) );
	Vec3V tRejectCY0 = SelectFT( DX0 < zero,  Vec3V(V_HALF) ,zero);
	Vec3V conservativeRange0 =  (-DX0* tRejectCY0  + DY0 * tRejectCX0);
	
	Vec3V tRejectCX1 = SelectFT( DY1 < zero, zero, Vec3V(V_HALF) );
	Vec3V tRejectCY1 = SelectFT( DX1 < zero,  Vec3V(V_HALF) ,zero);
	Vec3V conservativeRange1 =  (-DX1* tRejectCY1  + DY1 * tRejectCX1);

	 const Vec4V* __restrict src = HiZBuffer + loc[0] + loc[2]*RAST_HIZ_WIDTH;
	 Vec3V Cs0 = AddScaled(AddScaled(C0, DY0, pos.GetZ()), DX0, pos.GetX()) - conservativeRange0;
	 Vec3V Cs1 = AddScaled(AddScaled(C1, DY1, pos.GetZ()), DX1, pos.GetX()) - conservativeRange1;
	 
	 Assert( loc[1] <= RAST_HIZ_WIDTH);

	 int sx = loc[1]-loc[0];			
	 int sy = loc[3]-loc[2];
	 bool result = Test2EdgeListsSimple( src, Of1, Of2,  Of3,  Of4, Of5, Of6, Cs0, DX0, DY0, Cs1, DX1, DY1, sx,sy ,minMaxV, pos, minZ
		 DEV_ONLY(, trivialAcceptVisiblePixel) BANK_ONLY(, useTrivialAcceptVisiblePixelTest));		
	 //return Dot(sum, Vec4V(V_QUARTER)); // reduce to 25% as it counts sub pixels
	 return (result?ScalarV(V_FLT_MAX):ScalarV(V_ZERO));
 }


 // does up to six edges
 ScalarV_Out rstTest3EdgeListsHiZ( const Vec4V* __restrict HiZBuffer, const EdgeList& edge0,const EdgeList& edge1,const EdgeList& edge2, ScalarV_In minZ, Vec4V_In minMaxV
	 DEV_ONLY(, u32* trivialAcceptVisiblePixel) BANK_ONLY(, bool useTrivialAcceptVisiblePixelTest))
 {
	 const Vec4V edge(Vec2V(ScalarVFromF32(RAST_HIZ_WIDTH)), Vec2V(ScalarVFromF32(RAST_HIZ_HEIGHT)));
	 const Vec4V offsetX = Vec4VConstant<U32_ZERO, U32_HALF, U32_ZERO, U32_HALF>();
	 const Vec4V offsetY = Vec4VConstant<U32_ZERO, U32_ZERO, U32_HALF, U32_HALF>();

	 Vec3V DX0 = -edge0.DY;
	 Vec3V DY0 = edge0.DX;
	 Vec3V C0 = edge0.C;
	 Vec3V DX1 = -edge1.DY;
	 Vec3V DY1 = edge1.DX;
	 Vec3V C1 = edge1.C;
	 Vec3V DX2 = -edge2.DY;
	 Vec3V DY2 = edge2.DX;
	 Vec3V C2 = edge2.C;

	 // calculate edge offsets
	 Vec4V Of1 = -AddScaled(DY0.GetX() * offsetY, offsetX, DX0.GetX());
	 Vec4V Of2 = -AddScaled(DY0.GetY() * offsetY, offsetX, DX0.GetY());
	 Vec4V Of3 = -AddScaled(DY0.GetZ() * offsetY, offsetX, DX0.GetZ());

	 Vec4V Of4 = -AddScaled(DY1.GetX() * offsetY, offsetX, DX1.GetX());
	 Vec4V Of5 = -AddScaled(DY1.GetY() * offsetY, offsetX, DX1.GetY());
	 Vec4V Of6 = -AddScaled(DY1.GetZ() * offsetY, offsetX, DX1.GetZ());

	 Vec4V Of7 = -AddScaled(DY2.GetX() * offsetY, offsetX, DX2.GetX());
	 Vec4V Of8 = -AddScaled(DY2.GetY() * offsetY, offsetX, DX2.GetY());
	 Vec4V Of9 = -AddScaled(DY2.GetZ() * offsetY, offsetX, DX2.GetZ());

	 Vec4V intMinMaxV = minMaxV;

	 // if overlaps edges pull in so never overwrites.
	 intMinMaxV = Vec4V(	RoundToNearestIntNegInf(intMinMaxV.Get<Vec::X, Vec::Z>()),
		 RoundToNearestIntPosInf(intMinMaxV.Get<Vec::Y, Vec::W>()));

	 intMinMaxV = intMinMaxV.Get<Vec::X, Vec::Z, Vec::Y, Vec::W>();

	 intMinMaxV = Clamp( intMinMaxV, Vec4V(V_ZERO), edge);		

	 Vec4V pos = intMinMaxV;
	 Vec4V actIntMin = FloatToIntRaw<0>(intMinMaxV);
	 int* loc = (int*)&actIntMin;

	 // Shift to half texel so conservative tests.
	 Vec3V zero(V_ZERO);
	 Vec3V tRejectCX0 = SelectFT( DY0 < zero, zero, Vec3V(V_HALF) );
	 Vec3V tRejectCY0 = SelectFT( DX0 < zero,  Vec3V(V_HALF) ,zero);
	 Vec3V conservativeRange0 =  (-DX0* tRejectCY0  + DY0 * tRejectCX0);

	 Vec3V tRejectCX1 = SelectFT( DY1 < zero, zero, Vec3V(V_HALF) );
	 Vec3V tRejectCY1 = SelectFT( DX1 < zero,  Vec3V(V_HALF) ,zero);
	 Vec3V conservativeRange1 =  (-DX1* tRejectCY1  + DY1 * tRejectCX1);

	 Vec3V tRejectCX2 = SelectFT( DY2 < zero, zero, Vec3V(V_HALF) );
	 Vec3V tRejectCY2 = SelectFT( DX2 < zero,  Vec3V(V_HALF) ,zero);
	 Vec3V conservativeRange2 =  (-DX2* tRejectCY2  + DY2 * tRejectCX2);

	 const Vec4V* __restrict src = HiZBuffer + loc[0] + loc[2]*RAST_HIZ_WIDTH;
	 Vec3V Cs0 = AddScaled(AddScaled(C0, DY0, pos.GetZ()), DX0, pos.GetX()) - conservativeRange0;
	 Vec3V Cs1 = AddScaled(AddScaled(C1, DY1, pos.GetZ()), DX1, pos.GetX()) - conservativeRange1;
	 Vec3V Cs2 = AddScaled(AddScaled(C2, DY2, pos.GetZ()), DX2, pos.GetX()) - conservativeRange2;

	 Assert( loc[1] <= RAST_HIZ_WIDTH);

	 int sx = loc[1]-loc[0];			
	 int sy = loc[3]-loc[2];
	 bool result = Test3EdgeListsSimple( src, Of1, Of2,  Of3,  Of4, Of5, Of6, Of7, Of8, Of9, Cs0, DX0, DY0, Cs1, DX1, DY1, Cs2, DX2, DY2, sx,sy ,minMaxV, pos, minZ
		 DEV_ONLY(, trivialAcceptVisiblePixel) BANK_ONLY(, useTrivialAcceptVisiblePixelTest));		
	 //return Dot(sum, Vec4V(V_QUARTER)); // reduce to 25% as it counts sub pixels
	 return (result?ScalarV(V_FLT_MAX):ScalarV(V_ZERO));
 }


// Queries
VecBoolV_Out rstScanQueryHiZVLarge( const Vec4V* __restrict HiZBuffer, Vec3V_In scmin, Vec3V_In scmax)
{
	static const Vec4V HiZSizeV( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, (float)RAST_HIZ_HEIGHT);
	Vec4V ztest = Vec4V(Invert(scmin.GetZ()));

	// Check Screen space loop extents
	Vec3V startV = RoundToNearestIntZero(scmin); 
	Vec3V endV = RoundToNearestIntPosInf(scmax);
	Vec4V extents = MergeXY( startV, endV );

	Assert( extents.GetZf() < extents.GetWf());
	Assert(  extents.GetXf() < extents.GetYf());

	extents = Max( extents, Vec4V(V_ZERO));
	extents = Min( extents, HiZSizeV);
	Vec4V extentsInt = FloatToIntRaw<0>(extents);
	const Vec4V t = extentsInt;
	int* ext = ( int* __restrict)&t;
	int sx = ext[0];
	int ex = ext[1];
	int sy = ext[2];
	int ey = ext[3];

	// round to sub pixel boundaries and find 2D box for testing	
	VecBoolV res0(V_F_F_F_F);
	VecBoolV res1(V_F_F_F_F);
	VecBoolV res2(V_F_F_F_F);
	VecBoolV res3(V_F_F_F_F);


	const Vec4V*  __restrict hiZScanLine = HiZBuffer + sy * RAST_HIZ_WIDTH + sx;

	for (int y = sy;  y < ey; y+=2 )
	{
		const Vec4V* __restrict hiZ = hiZScanLine;
		for (int x = sx; x < ex; x+=4 )
		{
			Vec4V hiZ0 = hiZ[0];
			Vec4V hiZ1 = hiZ[1];
			Vec4V hiZ2 = hiZ[2];
			Vec4V hiZ3 = hiZ[3];
			Vec4V hiZ4 = hiZ[RAST_HIZ_WIDTH+0];
			Vec4V hiZ5 = hiZ[RAST_HIZ_WIDTH+1];
			Vec4V hiZ6 = hiZ[RAST_HIZ_WIDTH+2];
			Vec4V hiZ7 = hiZ[RAST_HIZ_WIDTH+3];

			VecBoolV test0 = (ztest > hiZ0);
			VecBoolV test1 = (ztest > hiZ1);
			VecBoolV test2 = (ztest > hiZ2);
			VecBoolV test3 = (ztest > hiZ3);
			VecBoolV test4 = (ztest > hiZ4);
			VecBoolV test5 = (ztest > hiZ5);
			VecBoolV test6 = (ztest > hiZ6);
			VecBoolV test7 = (ztest > hiZ7);

			res0 = res0 | test0; 
			res1 = res1 | test1; 
			res2 = res2 | test2; 
			res3 = res3 | test3; 
			res0 = res0 | test4; 
			res1 = res1 | test5; 
			res2 = res2 | test6; 
			res3 = res3 | test7; 

			hiZ+=4;
		}
		hiZScanLine += RAST_HIZ_WIDTH*2;
	}
	res3 = res3 | res2;
	VecBoolV res = res0 | res1;
	return res | res3;
}





} // namespace rage
#ifndef FRAG_TSKLIB_ONLY

void scan( rage::sysTaskParameters& p)
{
	using namespace rage;
#if __RASTERSTATS
	PF_FUNC(Rasterize);
#endif
#if SCAN_SPEW_CRAZY
	Displayf("Entering SPU job Scan tile Wrapper");
#endif
#if TASK_EMULATE_SPU
	char scratchBuffer[32*1024];
	params.Scratch.Data = scratchBuffer;
	params.Scratch.Size = sizeof( scratchBuffer);
#endif
	const ScanTileInfo* tInfo = (ScanTileInfo*)p.Input.Data;
	ScanUserData ud = tskGetUserData<ScanUserData>(p);
	rstScanTile( tInfo, ud, (Vec4V*)p.Output.Data, p.Output.Size, p.Scratch );

	DependantTaskList::UpdateDependancies( p, TAG_LOADINDICES);
#if SCAN_SPEW_CRAZY && __SPU
	Displayf("Exiting SPU job Scan tile Wrapper");
#endif
}
#endif

#if !RST_QUERY_ONLY
SPUFRAG_IMPL(bool, scan_frag, rage::sysDependency& task)
{
	using namespace rage;
#if __RASTERSTATS
	PF_FUNC(Rasterize);
#endif
#if SCAN_SPEW_CRAZY
	Displayf("Entering SPU job Scan tile Wrapper");
#endif
	const ScanTileInfo* tInfo = tskGetInput<ScanTileInfo>(task,1);
	ScanUserData ud = tskGetUserData<ScanUserData>(task);
	int outSize;
	Vec4V* outTile = tskGetOutput<Vec4V>( task, 2,outSize);
	sysTaskBuffer sm = tskGetScratch( task);
	rstScanTile( tInfo, ud, (Vec4V*)outTile, outSize,sm );

#if SCAN_SPEW_CRAZY && __SPU
	Displayf("Exiting SPU job Scan tile Wrapper");
#endif
	return true;
}
#endif

