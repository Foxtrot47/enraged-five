#include "system/new.h"
#include "tsklib.h"


#if !__SPU && __BANK
#include "file/stream.h"
#include "system/memory.h"
#include <string>
#include "system/Exec.h"
#endif

namespace rage
{

#if !__SPU && __BANK
void DependantTaskList::taskDumpDependancyGraphRecurse( fiStream* out, const DependantTaskList* dtask )
{
	const char* nom =dtask->GetName();
	for (int i =0; i <  dtask->m_numDependants; i++ )
	{
		const DependantTaskList* dep=dtask->dependants[i];

		fprintf( out,"\t%s0 -> %s0", nom, dep->GetName());
		char brac = '[';
		if ( dtask->NumTasks() > 1)
		{
			fprintf( out," [ltail=cluster%s %c ",nom, dep->NumTasks() > 1 ?',' : ']');
			brac=' ';
		}
		if ( dep->NumTasks() > 1)
		{
			fprintf( out," %clhead=cluster%s] ",brac, dep->GetName());
		}
		fprintf( out,";\n");
		taskDumpDependancyGraphRecurse( out, dep );
	} 
	if ( dtask->m_amt == 1)
		fprintf( out,"\t%s0[ color=gold, label=\"%s\"];\n",nom, nom);
	else
	{
		fprintf( out,"\tsubgraph cluster%s {\n",nom);
		for (int j=0; j < dtask->m_amt; j++)
		{	
			fprintf( out,"\t\t%s%i[ label=\"%s %i\"];\n", nom, j, nom, j);
		}
		fprintf( out,"\t}\n");
	}
	
}


void DependantTaskList::taskDumpDependancyGraph( const char* fname, const DependantTaskList** rootNodes, int amt )
{
	fiStream* out= fiStream::Create(fname);

	fprintf( out,"digraph G {\n\tsize=\"8,24\";\n");
	fprintf( out,"\tcompound=true;\n");
	fprintf( out,"\tnode [color=lightblue2, style=filled,fontname=arial];\n");
	for (int i =0; i < amt; i++)
	{
		if ( rootNodes[i]->IsSetup())
		{
			taskDumpDependancyGraphRecurse( out, rootNodes[i]);
		}
	}
	fprintf( out,"}\n");
	out->Close();

	std::string graphVizRoot = "\"c:\\NewComputer\\codelibs\\Graphviz2.16\\bin\\";
	std::string command = graphVizRoot + "dot.exe\" -Tpng " + std::string(fname) + " -o "+ std::string("c:\\graph.png");
	int ret = sysExec(command.c_str() );
	if (ret)
	{
		Displayf( "ERROR with GraphViz generation ");
		Displayf( "%s", command.c_str() );
	}
	else
	{	
		command ="start c:\\graph.png";
		 sysExec(command.c_str() );
	}
}
#endif

#if !__SPU
u32									DependantTaskList::m_OutputDummy[4];

void rage::DependantTaskList::Add( TASK_INTERFACE_PARAMS, sysTaskParameters &p )
{
#if !__NO_OUTPUT
	if(p.ReadOnlyCount > TASK_MAX_READ_ONLY)
		Quitf( "Read only count for dep task is too high: %zu, max is %d", p.ReadOnlyCount, TASK_MAX_READ_ONLY);
#endif

	if ( m_amt ==0 )
	{
		*m_refCount=0;
		m_numDependants=0;
	}
	if (p.ReadOnlyCount==0 || p.ReadOnly[p.ReadOnlyCount-1].Data != this )
	{
	#if !__NO_OUTPUT
		if(p.ReadOnlyCount == TASK_MAX_READ_ONLY)
			Quitf("Cannot append dep tasklist readonly entry, all slots used");
	#endif

		p.ReadOnly[p.ReadOnlyCount].Data=this;
		p.ReadOnly[p.ReadOnlyCount++].Size= sizeof(DependantTaskList);	
	}
	if ( p.Output.Data == NULL && p.Output.Size == 0 )
	{
		p.Output.Data = &m_OutputDummy;
		p.Output.Size = sizeof( m_OutputDummy);
	}
	hdles[m_amt++] = sysTaskManager::Prepare( TASK_INTERFACE_PARAMS_PASS, p);
}

void rage::DependantTaskList::Wait()
{
	for (int i =0; i < m_amt; i++)
	{
		if ( hdles[i].hdle){
			sysTaskManager::Wait( hdles[i].hdle);
			hdles[i].hdle=0;	
		}
	}
	m_amt=0;
}

#endif
}