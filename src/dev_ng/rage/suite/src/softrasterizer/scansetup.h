// 
// softrasterizer/scansetup.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SOFTRASTERIZER_SCANSETUP_H 
#define SOFTRASTERIZER_SCANSETUP_H 

namespace rage {


#define __RASTERSTATS (!__FINAL && !__SPU)

#define RAST_NUM_TILERS_ACTIVE 4

	//3
#if RAST_NUM_TILERS_ACTIVE == 3
#define MAX_TILER_TRIS	(512+128)
#else	
#define MAX_TILER_TRIS	(384+64)
#endif

#if !__DEV
#define RAST_INLINE __forceinline
#else
#define RAST_INLINE 
#endif


#define RAST_NUM_TILES_WIDE				4
#define RAST_NUM_TILES_HIGH				3

#define RAST_BLOCK_WIDTH				8

#if RAST_NUM_TILES_HIGH == 3 
#define RAST_BLOCK_HEIGHT				6
#else
#define RAST_BLOCK_HEIGHT				9
#endif

#define RAST_NUM_TILES					(RAST_NUM_TILES_WIDE * RAST_NUM_TILES_HIGH)		// 4*2 = 8



#define RAST_TILE_WIDTH					(RAST_BLOCK_WIDTH * 4)							// 8*4 = 32
#define RAST_TILE_HEIGHT				(RAST_BLOCK_HEIGHT * 4)							// 9*4 = 36
#define RAST_TILE_SIZE_BYTES			(RAST_TILE_WIDTH * RAST_TILE_HEIGHT * 16)		// 32*36*16 = 18432

#define RAST_SCREEN_WIDTH				(RAST_TILE_WIDTH * RAST_NUM_TILES_WIDE)			// 32*4 = 128
#define RAST_SCREEN_HEIGHT				(RAST_TILE_HEIGHT * RAST_NUM_TILES_HIGH)		// 36*2 = 72
#define RAST_SCREEN_SIZE				(RAST_SCREEN_WIDTH * RAST_SCREEN_HEIGHT)		// 128*72 = 9216

#define RAST_SCREEN_WIDTH_PIXELS		(RAST_SCREEN_WIDTH * 2)							// 128*2 = 256
#define RAST_SCREEN_HEIGHT_PIXELS		(RAST_SCREEN_HEIGHT * 2)						// 72*2 = 144

#define RAST_HIZ_WIDTH					(RAST_SCREEN_WIDTH / 2)							// 128/2 = 64
#define RAST_HIZ_HEIGHT					(RAST_SCREEN_HEIGHT / 2)						// 72/2 = 36
#define RAST_HIZ_SIZE_VEC				(RAST_HIZ_WIDTH * RAST_HIZ_HEIGHT)				// 64*36 = 2304
#define RAST_HIZ_SIZE_BYTES				(RAST_HIZ_SIZE_VEC * 16)						// 2304*16 = 36864

#define RAST_HIZ_TILE_WIDTH				(RAST_TILE_WIDTH / 2)							// 32/2 = 16
#define RAST_HIZ_TILE_HEIGHT			(RAST_TILE_HEIGHT / 2)							// 36/2 = 18

#define RAST_STENCIL_SIZE_VEC			(RAST_SCREEN_HEIGHT + 4)						// 72+4 = 76
#define RAST_STENCIL_SIZE_BYTES			(RAST_STENCIL_SIZE_VEC * 16)					// 76*16 = 1216

#define RAST_HIZ_AND_STENCIL_SIZE_VEC	(RAST_HIZ_SIZE_VEC + RAST_STENCIL_SIZE_VEC)		// 2304+76 = 2380
#define RAST_HIZ_AND_STENCIL_SIZE_BYTES	(RAST_HIZ_AND_STENCIL_SIZE_VEC * 16)			// 2380*16 = 38080

CompileTimeAssert(RAST_SCREEN_HEIGHT == 72);
CompileTimeAssert(RAST_SCREEN_WIDTH == 128);
} // namespace rage

#define _USE_SYSDEPENDANCY 1

#endif // SOFTRASTERIZER_SCANSETUP_H 
