
#include "vectormath/classes.h"

using namespace rage::Vec;

#include "softrasterizer/BoxOccluder.h"
#include "math/amath.h"
#include "profile/profiler.h"
#include "softrasterizer/softrasterizer.h"
#include "scan.h"
#include "grprofile/pix.h"
#include "tsklib.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grmodel/shader.h"
#include "occludeModel.h"

#define BOCCLUDER_SPEW_CRAZY 0
#define INVERT_ROTATION_HANDEDNESS 1


using namespace rage;
#if !__SPU
#include "softrasterizer/occluders_parser.h"
#endif


#define NUM_BOX_OCCLUDER_INDICES 36

namespace rage
{
#define MAX_IN_TRIS NUM_BOX_OCCLUDER_INDICES


#if __RASTERSTATS
	namespace RasterStats
	{
		EXT_PF_TIMER(AddTriList);
		EXT_PF_TIMER(SelectOccluders);
		EXT_PF_TIMER(SelectModelOccluders);
	}
	using namespace RasterStats;
#endif // __RASTERSTAT
	
	void BoxOccluder::SetSize( float Length, float Width, float Height, float RotZ )
	{
		iLength = Max((u16)1, (u16)(Length * 4.0f));
		iWidth = Max((u16)1, (u16)(Width * 4.0f));
		iHeight = Max((u16)1, (u16)(Height * 4.0f));
		iCosZ= (s16)((Cosf(RotZ)) * 16384.0f);	
		iSinZ = (s16)((Sinf(RotZ)) * 16384.0f);	
	}
	RAST_INLINE Vec4V_Out BoxOccluder::Decompress( Vec4V_InOut Size) const
	{
		//const Vec4V g_ScaleSinCos(.5f/4.0f, .5f/4.0f, .5f/4.0f, 1.0f/16384.0f);
		const Vec4V g_ScaleSinCos = Vec4VConstant<0x3E000000,0x3E000000,0x3E000000,0x38800000>();
		Assert16(&iCenterX);
		const Vec4V* vinm = (const Vec4V*)&iCenterX;
		const Vec4V vin = *vinm;
		Size = IntToFloatRaw<0>(Vec4V(V4UnpackHighSignedShort( vin.GetIntrin128())))*g_ScaleSinCos;
		return IntToFloatRaw<2>(Vec4V(V4UnpackLowSignedShort( vin.GetIntrin128())));
	}
	RAST_INLINE void BoxOccluder::CalculateVerts( Vec3V* RESTRICT verts ) const
	{
		const ScalarV g_ScaleCos = ScalarVConstant<0x39800000>(); // u32 version of 4.0f/16384.0f
		const ScalarV g_OneEighth = ScalarVConstant<0x3E000000>();   // u32 version of 1./8.f
		Vec4V Size;
		Vec4V Sphere =Decompress( Size);

		ScalarV Length = Size.GetX()-g_OneEighth;
		ScalarV Width = Size.GetY()-g_OneEighth;
		ScalarV Height = Size.GetZ()-g_OneEighth;
		Vec3V VecZ = Vec3V(V_Z_AXIS_WZERO)* Height; // 0 0 h 0
		ScalarV SinZ = Size.GetW();
		ScalarV CosZ = Sphere.GetW()*g_ScaleCos;

#if INVERT_ROTATION_HANDEDNESS
		SinZ = Negate( SinZ );
#endif

		Vec3V VecX =Width * Vec3V(  CosZ,SinZ, ScalarV(V_ZERO)); // c s 0 0 
		Vec3V VecY =Length * Vec3V(-SinZ, CosZ, ScalarV(V_ZERO)); // -s c 0 0

		Vec3V Center = Sphere.GetXYZ();
		Vec3V Top = Center + VecZ;
		Vec3V Bot = Center - VecZ;	
		Vec3V A = VecX + VecY;
		Vec3V B = VecX - VecY;

		Vec3V v0 = Top + B;
		Vec3V v1 = Top - A;
		Vec3V v2 = Top + A;
		Vec3V v3 = Top - B;

		Vec3V v4 = Bot + A;
		Vec3V v5 = Bot - B;
		Vec3V v6 = Bot + B;
		Vec3V v7 = Bot - A;

		// TODO - use Z up
		verts[0] = v0;
		verts[1] = v1;
		verts[2] = v2;
		verts[3] = v3;
		verts[4] = v4;
		verts[5] = v5;
		verts[6] = v6;
		verts[7] = v7;

		// could use points to create screenspace bound and test for visibility and area.
	}

	VECTOR_ALIGN  static u8 BoxOccluderIndices[]  =
	{
		2,3,1,	0,2,1, // 2,3,1,0,
		4,5,3,	2,4,3, // 4,5,3,2,
		6,7,5,	4,6,5, // 6,7,5,4,
		0,1,7,	6,0,7, // 0,1,7,6,
		3,5,7,	1,3,7, // 3,5,7,1,
		4,2,0,	6,4,0, // 4,2,0,6,
		0,0,0,0,0,0,0,0,0,0,0,0		// pad 16 byte aligned for SPU dmas
	} ;
	int BoxOccluder::GetNumOccluderIndices()
	{
		return  NUM_BOX_OCCLUDER_INDICES;
	}
	u8* BoxOccluder::GetOccluderIndices()
	{
		return BoxOccluderIndices;
	}
	void BoxOccluder::DebugDraw() const
	{
		Vec3V verts[8];
		CalculateVerts(verts);
		
			grcBegin(drawTris,GetNumOccluderIndices());

			for (int i = 0; i < GetNumOccluderIndices(); i++)
			{		
				grcVertex3f( VEC3V_TO_VECTOR3(verts[GetOccluderIndices()[i]] ));	
			}

			grcEnd();
		
	}


	int rstSelectBoxOccluders( const rstTransformToScreenSpace& screen, const CBoxOccluderBucket* __restrict pBuckets,  
		BoxOccluder* __restrict rocclist, u8* isClipped, size_t amt, 
		int maxOccluders, ScalarV_In threshold, u32* packedArea SPU_ONLY(, BoxOccluder* pElements))
	{
		rstSplatMat44V splatMtx;
		Vec3V maxBounds;
		Vec3V minBounds(0.f,0.f,-100.f);
		ScalarV rescaleDepth = rstCreateCurrentSplatMat44V( screen, splatMtx, maxBounds);
		ScalarV maxArea = maxBounds.GetX()*maxBounds.GetY();
		Vec3V cameraPos=screen.m_campos;
		int cnt =  0;
		float eps = TRANSFORM_EXTRA_CLIP_THRESHOLD;
		ScalarV clipPlane = ScalarVFromF32(eps)*rescaleDepth;
		ScalarV zero(V_ZERO);

		for (size_t i = 0; i < amt; i++)
		{
			const CBoxOccluderBucket& bucket = pBuckets[i];
#if __SPU
			// EJ: Just to be 100% safe since the SPU hates me
			if (0 == bucket.count || NULL == bucket.pElements)
				continue;

			const int dataSize = bucket.count * sizeof(BoxOccluder);
			sysDmaLargeGetAndWait((const void*)pElements, reinterpret_cast<u64>(pBuckets[i].pElements), dataSize, 0);
#else
			BoxOccluder* pElements = bucket.pElements;
#endif
			Assert(pElements);

			for (int j = 0; j < bucket.count; ++j)
			{
				const BoxOccluder& occ = pElements[j];

				Vec3V oobb[8];
				occ.CalculateVerts(oobb);

				Vec3V scbmin;
				Vec3V scbmax;

				Vec3V worldMin;
				Vec3V worldMax;
				rstCalculateScreenSpaceOOBBBound(splatMtx, rescaleDepth,oobb, scbmin,scbmax, worldMin, worldMax );

				Vec4V inCamera = Vec4V(cameraPos >worldMin & cameraPos < worldMax);
				ScalarV inCam=(inCamera.GetX()& inCamera.GetY()&inCamera.GetZ());

				// clip to screen first
				scbmax = Clamp( scbmax, minBounds, maxBounds);
				scbmin = Clamp( scbmin, minBounds, maxBounds);

				// calculate selection threshold
				Vec3V diff = scbmax - scbmin;

				BoolV clipNear = scbmin.GetZ() < clipPlane;
				BoolV inView = scbmax.GetZ() > zero;

				isClipped[cnt] =(u8) IsTrue(clipNear);
#if __ASSERT
				if ( !isClipped[cnt]){
					for (int i =0; i < 8; i++) 
					{
						ScalarV d = Dot(Vec4V(oobb[i],ScalarV(V_ONE)), screen.m_cplane); 
						Assertf( IsGreaterThanOrEqualAll(  d, ScalarV(V_NEGONE) ), 
							"Occlusion Model requires clipping but says it doesnt ( dist %f )", d.Getf());
					}
				}		
#endif				
				ScalarV area = diff.GetX() * diff.GetY();
				area = SelectFT( BoolV(inCam.GetIntrin128())|clipNear, area, maxArea);	
				rocclist[cnt] = occ;
				int parea=Vec4V(FloatToIntRaw<0>(area)).GetXi();
				Assert( parea>=0 && parea < (1<<16));
				packedArea[cnt]= parea<<16 | cnt;

				cnt +=  (u8)IsTrue( (area > threshold)& inView );	
			
				if ( Unlikely( cnt == maxOccluders))
				{
					return cnt;
				}				
			}
		}

		return cnt;
	}
}

#ifndef FRAG_TSKLIB_ONLY

#include "softrasterizer/tsklib.h"
#include "softrasterizer/tiler.h"
using namespace rage;





#if __WIN32 || RSG_ORBIS
#include "boxoccluder_frag.frag"
#include "boxselector_frag.frag"
#include "modelselector_frag.frag"
#endif

//#else

void boxoccluder( rage::sysTaskParameters& p  )
{
#if __RASTERSTATS
	PF_FUNC(AddTriList);
#endif	

#if TASK_EMULATE_SPU
	char scratchBuffer[16*1024];
	p.Scratch.Data = scratchBuffer;
	p.Scratch.Size = sizeof( scratchBuffer);
#endif
	rage::sysTaskBuffer sm = p.Scratch;

	using namespace rage;
	rstFastTiler* tiler = (rage::rstFastTiler*)p.Input.Data;
	tiler->Reset();
	tiler->SetViewport( *tskGetReadOnly<rstTransformToScreenSpace>(p, 2));

	ScratchMemStackPoint gck(sm);

	Vec4V* clipInBuffer= taskAllocBuffer<Vec4V,16>( sm, MAX_GENERATED_VERTS);
	Vec4V* clipOutBuffer= taskAllocBuffer<Vec4V,16>( sm, MAX_GENERATED_VERTS);


	tiler->SetClippingMemory( clipInBuffer, clipOutBuffer);
		
	// model amt are higher priority
	TileDataPacket<OccludeModel>* modelPacket = tskGetReadOnly<TileDataPacket<OccludeModel> >(p, 1);
	if ( modelPacket->amt )
	{
		ScratchMemStackPoint ck(sm);
		OccludeModel* occList = taskLoadBuffer( "resOccluderModels", sm, modelPacket->occluders, modelPacket->amt , TAG_LOAD);
		u8* isClipped = taskLoadBuffer( "clippedFlags",sm, modelPacket->clippedFlags, TSK_ROUNDUP16(modelPacket->amt), TAG_LOAD);
		taskWait( TAG_LOAD);

		Vec3V* verts = taskAllocBuffer<Vec3V,16>( sm,rstFastTiler::MaxIndexedVerts);
		
		
		// double buffer the dma's of the model geometry
		u8* buff[2]={0,0};	
#if __PS3
		buff[0] = taskAllocBuffer<u8,16>( sm,OccludeModel::GetMaxModelSize());
		buff[1] = taskAllocBuffer<u8,16>( sm,OccludeModel::GetMaxModelSize());
#endif
		int bId = 0;

		ScalarV bitPrecision = ScalarVFromF32(p.UserData[UD_BITPRECISION_EA].asFloat) ;

		occList[0].LoadGeometryLocally(buff[bId],TAG_LOAD );

		for (int i =0; i < modelPacket->amt; i++)	
		{					
			taskWait( TAG_LOAD);
			if ( occList[i].IsEmpty())
				continue;

			if ( Likely(i!=(modelPacket->amt-1)))
			{
				bId = !bId;
				occList[i+1].LoadGeometryLocally(buff[bId],TAG_LOAD );
			}		

			Assert(!occList[i].AreFlagsSet(OccludeModel::FLAG_WATER_ONLY));
			// skip water only occluders, these are written to stencil
			if (occList[i].AreFlagsSet(OccludeModel::FLAG_WATER_ONLY))
				continue;

			occList[i].UnPackVertices( verts, rstFastTiler::MaxIndexedVerts, bitPrecision);

			if ( tiler->IsFull( occList[i].GetNumIndices() ) )
				continue;

			tiler->AddIndexedTriList(  verts,
				occList[i].GetIndices(),
				occList[i].GetNumIndices(),
				occList[i].GetNumVerts(),
				isClipped[i]!=0);
		}			
	}

	TileDataPacket<BoxOccluder>* packet = tskGetReadOnly<TileDataPacket<BoxOccluder> >(p, 0);
	if (packet->amt)
	{
		ScratchMemStackPoint ck(sm);

		BoxOccluder* occList = taskLoadBuffer( "occluders", sm, packet->occluders, packet->amt , TAG_LOAD);
		u8* isClipped = taskLoadBuffer( "clippedFlags",sm, packet->clippedFlags, TSK_ROUNDUP16(packet->amt) , TAG_LOAD);		
		taskWait( TAG_LOAD);

		Vec3V verts[8];
		for (int i =0; i < packet->amt; i++)
		{
			occList[i].CalculateVerts( verts);

			tiler->AddIndexedTriList( 
				verts, 
				BoxOccluder::GetOccluderIndices(), 
				BoxOccluder::GetNumOccluderIndices(),
				8,
				isClipped[i]!=0);	
		}
	}

	// write out explicitly to output
	taskWrite( "Tiler Output",tiler, (rstFastTiler* )p.UserData[UD_TILER_EA].asPtr, TAG_WRITETILER_0 );
	taskWait( TAG_WRITETILER_0); 

	DependantTaskList::UpdateDependancies( p, TAG_WRITETILER_0);
}


void boxselector( rage::sysTaskParameters& p )
{
#if __RASTERSTATS
	PF_FUNC(SelectOccluders);
#endif	

	using namespace rage;
	size_t numInBuckets;
	const CBoxOccluderBucket* pBuckets = tskGetInput<CBoxOccluderBucket>(p, numInBuckets);
	
	ScratchMemStackPoint cm(p.Scratch);
	u8*  isClipped = taskAllocBuffer<u8,16>(p.Scratch, 1024); 
	BoxOccluder* resboxes =  taskAllocBuffer<BoxOccluder,16>(p.Scratch,512);
	rstTransformToScreenSpace* clipAndTransform = (rstTransformToScreenSpace*) p.ReadOnly[0].Data;
	BoxSelectorUserData userData = tskGetUserData<BoxSelectorUserData>(p);

#if __SPU
	// EJ: Ugly, but the most memory-efficient way of handling this
	int maxOccluders = 0;
	for (int i = 0; i < (int) numInBuckets; ++i)
	{
		if (pBuckets[i].count > maxOccluders)
			maxOccluders = pBuckets[i].count;
	}

	BoxOccluder* pElements = taskAllocBuffer<BoxOccluder, 16>(p.Scratch, maxOccluders);
#endif

	u32* parea=taskAllocBuffer<u32,16>(p.Scratch, 512); 
	int amt = rstSelectBoxOccluders( *clipAndTransform, pBuckets, resboxes, isClipped,	numInBuckets, MAX_NUM_BOX_OCCLUDERS_RASTERIZED, ScalarVFromF32(userData.threshold), parea SPU_ONLY(, pElements));
	std::sort( parea, parea+amt,std::greater<int>());

	// fire off child tasks.
	int NumTilers = userData.NumTilers;
	int stepSize = (amt+(NumTilers))/ NumTilers;
	
	int c = 0;
	TileDataPacket<BoxOccluder>	packets[RAST_NUM_TILERS_ACTIVE] ;

	// round up to the number of tilers
	// so we can place the sorted ones easily

	u8*  isClippedFinal = taskAllocBuffer<u8,16>(p.Scratch, MAX_NUM_BOX_OCCLUDERS_RASTERIZED+16); 
	BoxOccluder* resBoxesFinal =  taskAllocBuffer<BoxOccluder,16>(p.Scratch,MAX_NUM_BOX_OCCLUDERS_RASTERIZED+16);


	int roundedAmt = stepSize*NumTilers;
	for (int i = 0; i < NumTilers; i++, c+= stepSize)
	{	
		int end =  Min( roundedAmt, c +stepSize);
		int size=  Max(end-c,0);

		packets[i].clippedFlags = userData.outputClipped + c;
		packets[i].occluders = userData.outputOccluders+ c;
		packets[i].amt = size;	
		// copy in sorted occluders for each tiler
		for (int j=0;j<size;j++){
			int idx= i +(j*NumTilers);
			if ( idx < amt){
				int sidx = parea[idx]&0xFFFF;  // bottom 16 bits gives index
				Assert( sidx <amt);
				isClippedFinal[c+j]=isClipped[sidx];
				resBoxesFinal[c+j]=resboxes[sidx];
			}
			else
			{
				isClippedFinal[c+j]=1;
				resBoxesFinal[c+j]=BoxOccluder::Empty();
			}
		}
	}
	amt = roundedAmt;



	taskWriteBuffer( "clipFlags",isClippedFinal, userData.outputClipped, TSK_ROUNDUP16(amt), TAG_WRITETILER_0 );
	taskWriteBuffer( "resOccluders",resBoxesFinal, userData.outputOccluders, amt, TAG_WRITETILER_0 );

	Assert( c >= amt);
	taskWriteBuffer( "TilerPackets",packets, userData.packets, NumTilers, TAG_WRITETILER_0 );
	taskWait( TAG_WRITETILER_0);

	DependantTaskList::UpdateDependancies( p, TAG_WRITETILER_0);
}
void modelselector( rage::sysTaskParameters& p )
{
#if __RASTERSTATS
	PF_FUNC(SelectModelOccluders);
#endif	

	using namespace rage;
	size_t numInBuckets;
	const COccludeModelBucket* pBuckets = tskGetInput<COccludeModelBucket>(p, numInBuckets);

	ScratchMemStackPoint cm(p.Scratch);
	const size_t MaxResultOccluders = 256 >> __64BIT;
	u8*  isClipped = taskAllocBuffer<u8,16>(p.Scratch, MaxResultOccluders); 
	OccludeModel* resmodels =  taskAllocBuffer<OccludeModel,16>(p.Scratch,MaxResultOccluders);
	rstTransformToScreenSpace* clipAndTransform = (rstTransformToScreenSpace*) p.ReadOnly[0].Data;
	ModelSelectorUserData userData = tskGetUserData<ModelSelectorUserData>(p);

#if __SPU
	OccludeModel* pElements = taskAllocBuffer<OccludeModel, 16>(p.Scratch, MAX_NUM_OCCLUDE_MODELS); 	
#endif

	u32* parea=taskAllocBuffer<u32,16>(p.Scratch, MaxResultOccluders); 
	int amt = rstSelectModelOccluders( *clipAndTransform, pBuckets, resmodels, isClipped, numInBuckets, MaxResultOccluders, ScalarVFromF32(  userData.threshold) , parea SPU_ONLY(, pElements));

	if ( userData.outputWaterClipped != NULL)
	{
		ScratchMemStackPoint watercm(p.Scratch);
		int waterOccluderCount=0;
		OccludeModel* resWaterOccluders =  taskAllocBuffer<OccludeModel,16>(p.Scratch, OccludeModel::MaxWaterOccluders);
		u8*  resWaterClipped = taskAllocBuffer<u8,16>(p.Scratch, OccludeModel::MaxWaterOccluders); 
		// strip out water occluders into their own list.
		for (int i=0;i<amt;){
			if ( resmodels[i].AreFlagsSet(OccludeModel::FLAG_WATER_ONLY)){
				XENON_ONLY(Assert(resmodels[i].IsValid()));
				resWaterOccluders[waterOccluderCount]=resmodels[i];
				resWaterClipped[waterOccluderCount++]=isClipped[i];

				// swap with end
				amt--;
				resmodels[i]=resmodels[amt];
				isClipped[i]=isClipped[amt];
				parea[i]=parea[amt];
			}
			else{
				i++;
			}
		}		
		taskSetU32( userData.waterOccluderCountEA, waterOccluderCount, TAG_WRITETILER_0);
		// write out explicitly to output
		if ( waterOccluderCount){
			taskWriteBuffer( "resWaterClipped",resWaterClipped, userData.outputWaterClipped, TSK_ROUNDUP16(waterOccluderCount), TAG_WRITETILER_0 );
			taskWriteBuffer( "resWaterOccluders",resWaterOccluders, userData.outputWaterOccluders, waterOccluderCount, TAG_WRITETILER_0 );			
		}
		taskWait(TAG_WRITETILER_0);
	}
	else {
		// strip out water occluders 
		for (int i=0;i<amt;){
			if ( resmodels[i].AreFlagsSet(OccludeModel::FLAG_WATER_ONLY)){
				// swap with end
				amt--;
				resmodels[i]=resmodels[amt];
				isClipped[i]=isClipped[amt];
				parea[i]=parea[amt];
			}
			else 
				i++;
			
		}
	}
	// add in the idex here due to water occluder sorting.
	for(int i=0;i<amt;i++)
		parea[i]|=i;

	std::sort( parea, parea+amt,std::greater<int>());
	
	// fire off child tasks.
	int NumTilers = userData.NumTilers;
	int stepSize = (amt+NumTilers)/ NumTilers;
	
	int c = 0;
	TileDataPacket<OccludeModel>	packets[RAST_NUM_TILERS_ACTIVE] ;

	u8*  isClippedFinal = taskAllocBuffer<u8,16>(p.Scratch, MaxResultOccluders+16); 
	OccludeModel* resmodelsFinal =  taskAllocBuffer<OccludeModel,16>(p.Scratch,MaxResultOccluders);

	// round up to the number of tilers
	// so we can place the sorted ones easily
	int roundedAmt = stepSize*NumTilers;
	for (int i = 0; i < NumTilers; i++, c+= stepSize)
	{	
		int end =  Min( roundedAmt, c +stepSize);
		int size=  Max(end-c,0);

		packets[i].clippedFlags = userData.outputClipped + c;
		packets[i].occluders = userData.outputOccluders+ c;
		packets[i].amt = size;	
		// copy in sorted occluders for each tiler
		for (int j=0;j<size;j++){
			int idx= i +(j*NumTilers);
			if ( idx < amt){
				int sidx = parea[idx]&0xFFFF;  // bottom 16 bits gives index
				Assert( sidx <amt);
				isClippedFinal[c+j]=isClipped[sidx];
				resmodelsFinal[c+j]=resmodels[sidx];
			}
			else
			{
				isClippedFinal[c+j]=0;
				resmodelsFinal[c+j]=OccludeModel::Empty();
			}
		}
	}
	amt = roundedAmt;
	// write out explicitly to output
	taskWriteBuffer( "clipFlags",isClippedFinal, userData.outputClipped, TSK_ROUNDUP16(amt), TAG_WRITETILER_0 );
	taskWriteBuffer( "resOccluders",resmodelsFinal, userData.outputOccluders, amt, TAG_WRITETILER_0 );

	Assert( c >= amt);
	taskWriteBuffer( "TilerPackets",packets, userData.packets, NumTilers, TAG_WRITETILER_0 );
	taskWait( TAG_WRITETILER_0);

	DependantTaskList::UpdateDependancies( p, TAG_WRITETILER_0);
}
#endif
