#ifndef _BOX_OCCLUDER_H_
#define _BOX_OCCLUDER_H_

#include "atl/array.h"
#include "parser/macros.h"
#include "vectormath/classes.h"

#include "system/codefrag_spu.h"
#include "system/dependency.h"

#define TAG_LOAD 3
#define TAG_WRITETILER_0 4

// EJ: Box Occluder Optimization
#define MAX_NUM_BOX_OCCLUDERS (4000)
#define MAX_NUM_BOX_OCCLUDERS_RASTERIZED (512)

namespace rage
{
	class OccludeModel;
	class rstFastTiler;
	struct rstTransformToScreenSpace;

	// EJ: Box Occluder Optimization
	class BoxOccluder;
	typedef atArray<BoxOccluder> CBoxOccluderArray;

	struct ALIGNAS(16) CBoxOccluderBucket
	{
		CBoxOccluderArray* pArray;
		BoxOccluder* pElements;
		int count;
		int	mapDataSlotIndex;
	} ;
	
	CompileTimeAssert(0 == (sizeof(CBoxOccluderBucket) % 16));

	// PURPOSE:
	// Box occluders are a compressed oriented bounding box that can be 
	//  used to place approximate occluders into the level. Each box occluder only takes 16 bytes.
	//
	class ALIGNAS(16) BoxOccluder
	{
		s16		iCenterX;		// interleave to allow for fast decompression
		s16		iCenterY; 
		s16		iCenterZ;
		s16		iCosZ;

		s16		iLength;
		s16		iWidth;
		s16		iHeight;	
		s16		iSinZ;
	public:

		float		GetCenterX() const { return iCenterX * 0.25f; }
		float		GetCenterY() const { return iCenterY * 0.25f; }
		float		GetCenterZ() const { return iCenterZ * 0.25f; }
		void		SetCenterX(float Val) { iCenterX = (s16)((Val) * 4.0f); }
		void		SetCenterY(float Val) { iCenterY = (s16)((Val) * 4.0f); }
		void		SetCenterZ(float Val) { iCenterZ = (s16)((Val) * 4.0f); }

		void		SetSize(float Length, float Width, float Height, float RotZ);

		float		GetLength() const { return iLength * 0.25f; }
		float		GetWidth() const { return iWidth * 0.25f; }
		float		GetHeight() const { return iHeight * 0.25f; }

		void		CalculateVerts(Vec3V* RESTRICT verts)  const;
	

		Vec4V_Out   Decompress( Vec4V_InOut Size) const;
		void DebugDraw() const;

		static  int GetNumOccluderIndices();
		static u8* GetOccluderIndices();

		static BoxOccluder Empty(){
			BoxOccluder occ;
			occ.SetSize(10.f,10.f,10.f,0.f);
			occ.SetCenterX(0.f);
			occ.SetCenterY(0.f);
			occ.SetCenterZ(-1000.f);
			//memset(&occ,0,sizeof(BoxOccluder));
			return occ;
		}
		PAR_SIMPLE_PARSABLE;
	}  ;


	int rstSelectBoxOccluders( const rstTransformToScreenSpace& screen, const CBoxOccluderBucket* __restrict pBuckets, BoxOccluder* __restrict rocclist, u8* isClipped, int amt, 
		int maxOccluders, ScalarV_In threshold, u32* packedArea SPU_ONLY(, BoxOccluder* pElements));


	enum BoxOccUserData
	{
		UD_TILER_EA,
		UD_BITPRECISION_EA,
		UD_MODELWORK_CNT_EA,
		UA_TOTAL
	};

	struct BoxOccluderUserData
	{
		float		bitPrecision;
		rstFastTiler* tilerOutput;
	};

	template<class T>
	struct TileDataPacket
	{
		u8*				clippedFlags;
		T*				occluders;
		int				amt;
#if __64BIT
		u32				pad[3];
#else
		u32				pad;
#endif

		TileDataPacket(): amt(0),occluders(0) {}
	};
	struct BoxSelectorUserData
	{
		float							threshold;
		BoxOccluder*					outputOccluders;
		u8*								outputClipped;
		int								NumTilers;
		TileDataPacket<BoxOccluder>*	packets;	
	};
	struct ModelSelectorUserData
	{
		float							threshold;
		OccludeModel*					outputOccluders;
		u8*								outputClipped;

		OccludeModel*					outputWaterOccluders;
		u8*								outputWaterClipped;
		u32*							waterOccluderCountEA;

		int								NumTilers;
		TileDataPacket<OccludeModel>*	packets;	
	};
	
}


#ifndef FRAG_TSKLIB_ONLY
#include "system/task.h"
DECLARE_TASK_INTERFACE(boxoccluder);
DECLARE_TASK_INTERFACE(boxselector);
DECLARE_TASK_INTERFACE(modelselector);

void boxoccluder( rage::sysTaskParameters& p );
void boxselector( rage::sysTaskParameters& p );
void modelselector( rage::sysTaskParameters& p );

#endif

#endif