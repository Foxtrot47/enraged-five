// Runs the scan conversion and rasterization of a tile task

#ifndef _BOXOCCLUDER_FRAG_H
#define _BOXOCCLUDER_FRAG_H

#define FRAG_TSKLIB_ONLY

#include "system/dependency.h"
#include "tsklib.h"
#if !__WIN32 && !RSG_ORBIS
#include "boxoccluder.h"
#include "boxoccluder.cpp"
#include "tiler.cpp"
#include "occludeModel.cpp"
#endif


using namespace rage;


SPUFRAG_IMPL(bool, boxoccluder_frag, sysDependency& task)
{

	rage::sysTaskBuffer sm = tskGetScratch(task);
	
	BoxOccluderUserData ud = tskGetUserData<BoxOccluderUserData>( task);

	rstFastTiler* tiler = tskGetInputOutput<rage::rstFastTiler>(task,1);
	tiler->Reset();
	tiler->SetViewport( *tskGetInput<rstTransformToScreenSpace>( task, 3));

	// model amt are higher priority
	const TileDataPacket<OccludeModel>* modelPacket = tskGetInput<TileDataPacket<OccludeModel> >( task, 2);
	if ( modelPacket->amt )
	{
		ScratchMemStackPoint ck(sm);
		OccludeModel* occList = taskLoadBuffer( "resOccluderModels", sm, modelPacket->occluders, modelPacket->amt , TAG_LOAD);
		u8* isClipped = taskLoadBuffer( "clippedFlags",sm, modelPacket->clippedFlags, modelPacket->amt , TAG_LOAD);
		taskWait( TAG_LOAD);

		Vec3V* verts = taskAllocBuffer<Vec3V,16>( sm,rstFastTiler::MaxIndexedVerts);
		
		
		// double buffer the dma's of the model geometry
		u8* buff[2];		
		buff[0] = taskAllocBuffer<u8,16>( sm,OccludeModel::GetMaxModelSize());
		buff[1] = taskAllocBuffer<u8,16>( sm,OccludeModel::GetMaxModelSize());

		int bId = 0;

		ScalarV bitPrecision = ScalarVFromF32(ud.bitPrecision) ;

		occList[0].LoadGeometryLocally(buff[bId],TAG_LOAD );

		for (int i =0; i < modelPacket->amt; i++)	
		{					
			taskWait( TAG_LOAD);
			if ( occList[i].IsEmpty())
				continue;

			if ( Likely(i!=(modelPacket->amt-1)))
			{
				bId = !bId;
				occList[i+1].LoadGeometryLocally(buff[bId],TAG_LOAD );
			}		

			occList[i].UnPackVertices( verts, rstFastTiler::MaxIndexedVerts, bitPrecision);

			if ( tiler->IsFull( occList[i].GetNumIndices() ) )
				continue;

			tiler->AddIndexedTriList(  verts,
				occList[i].GetIndices(),
				occList[i].GetNumIndices(),
				occList[i].GetNumVerts(),
				isClipped[i]!=0);
		}			
	}

	const TileDataPacket<BoxOccluder>* packet = tskGetInput<TileDataPacket<BoxOccluder> >(task, 0);
	if (packet->amt)
	{
		ScratchMemStackPoint ck(sm);

		BoxOccluder* occList = taskLoadBuffer( "occluders", sm, packet->occluders, packet->amt , TAG_LOAD);
		u8* isClipped = taskLoadBuffer( "clippedFlags",sm, packet->clippedFlags, packet->amt , TAG_LOAD);
		taskWait( TAG_LOAD);

		Vec3V verts[8];
		for (int i =0; i < packet->amt; i++)
		{
			occList[i].CalculateVerts( verts);

			tiler->AddIndexedTriList( 
				verts, 
				BoxOccluder::GetOccluderIndices(), 
				BoxOccluder::GetNumOccluderIndices(),
				8,
				isClipped[i]!=0);	
		}
	}

	// write out explicitly to output
	//taskWrite( "Tiler Output",tiler, ud.tilerOutput, TAG_WRITETILER_0 );
	//taskWait( TAG_WRITETILER_0); 
	return true;
}

#endif