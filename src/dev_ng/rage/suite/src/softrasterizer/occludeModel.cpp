#include "softrasterizer/occludeModel.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "softrasterizer/tiler.h"
#include "softrasterizer/softrasterizer.h"
#include "scan.h"
#include "grcore/viewport.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"


#include "vectormath/classes.h"
#if __BANK
#include "grcore/im.h"
#endif


	

#define _USE_U32FORMAT 1

namespace rage
{	  

#if SUPPORT_OCCLUDER_MESH
grcVertexDeclaration* CheapShadowMesh::sm_vdecl=0;
#endif


OccludeModel::VertexFormat OccludeModel::sm_CurrentVertexFormat = OccludeModel::VFMT_F32;
grcRasterizerStateHandle OccludeModel::sm_hFrontFaceCullState;

// General function to convert from 3 floats in an array to Vec3V's

void ConvertFloatx3ToVec3V( Vec3V* __restrict verts, const float* packedVerts, int amt)
{
	int pv =(amt&~3); 

	const Vec4V* packedInputVerts = (Vec4V*)packedVerts;	
	for (int i=0;i<pv;i+=4)  // use simple pipelining
	{
		// load
	Vec4V a=packedInputVerts[0];
	Vec4V b= packedInputVerts[1];
	Vec4V c= packedInputVerts[2];
		//convert
	Vec3V p0 = a.GetXYZ();
	Vec3V p1 = GetFromTwo<Vec::W1,Vec::X2,Vec::Y2>(a,b);
	Vec3V p2 = GetFromTwo<Vec::Z1,Vec::W1,Vec::X2>(b,c);
	Vec3V p3 = c.Get<Vec::Y,Vec::Z,Vec::W>();

		// store
		verts[i+0]=p0;
		verts[i+1]=p1;
		verts[i+2]=p2;
		verts[i+3]=p3;

		packedInputVerts+=3;
	}

	float *out = (float*) verts;
	for (int i=pv; i<amt; i++) {
		out[i*4+0] = packedVerts[i*3+0];
		out[i*4+1] = packedVerts[i*3+1];
		out[i*4+2] = packedVerts[i*3+2];
		out[i*4+3] = 0;
	}
}



	int GetMaxVertSize( int numVerts)
	{
		return numVerts * sizeof(float)*3;
	}

	int OccludeModel::CalcVertSize( int numVerts ) const
	{
#if _USE_U32FORMAT
		return (IsFloatVertexFormat()) ?  numVerts * sizeof(float)*3 : numVerts * sizeof(u32);
#else
		return numVerts * sizeof(u32)*2;
#endif
	}
	Vec3V_Out Snap( Vec3V_In v, ScalarV_In inD, ScalarV_In Dim )
	{
		return RoundToNearestInt( v * inD)* Dim;
	}
#if _USE_U32FORMAT
	u32 PackVector111110( Vec3V_In v )
	{
		Assert( v.GetXi() >= 0);
		Assert( v.GetYi()>= 0);
		Assert( v.GetZi() >= 0);
		Assert( v.GetXi() < (1<<11));
		Assertf( v.GetYi() < (1<<11), " %i >= %i ", v.GetYi(), (1<<11) );
		Assert( v.GetZi() < (1<<10));

		return ( v.GetXi()<< 21|
			v.GetYi()<< 10|
			v.GetZi());
	}

	void OccludeModel::PackVertices( const Vec3V* inverts, int numVerts, ScalarV_In errorSize )
	{
		if ( sm_CurrentVertexFormat == VFMT_F32)
		{
			Assert( m_numTris & BIT_IS_VFMT_F32);
			float* packVerts = (float*)m_verts;
			for (int i=0;i<numVerts;i++)
			{
				for (int j=0;j<3; j++)
				{
					packVerts[i*3+j]=inverts[i][j];
				}
			}
			return;
		}
		ScalarV invErrorSize = Invert(errorSize);
		// quantize centre
		Vec3V centre = Snap( m_bmin, invErrorSize, errorSize );
		Vec3V quantScale = Vec3V(invErrorSize);
		Vec3V offset = centre * -quantScale;
		u32* packVerts = (u32*)m_verts;
		for (int i =0; i < numVerts; i++ )
		{	
			Vec3V p0 = AddScaled( offset, inverts[i] , quantScale);		
			packVerts[i]= PackVector111110( FloatToIntRaw<0>(p0));
		}	

	}
	void OccludeModel::UnPackVertices( Vec3V* verts, int ASSERT_ONLY(arraySize), ScalarV_In errorSize ) const 
	{

		if ( IsFloatVertexFormat())
		{
			ConvertFloatx3ToVec3V(verts, (float*)m_verts, GetNumVerts());
			return;
		}

		ScalarV invErrorSize = Invert(errorSize);
		Vec3V centre = Snap( m_bmin, invErrorSize, errorSize );

		// quantize centre
		int pv =( (GetNumVerts()+3)&~3) -4; 

		Assert(arraySize >= pv);

		Vec4V* packVerts = (Vec4V*)m_verts;
		Vec4V mask = Vec4V(ScalarVFromU32((1<<11)-1));
		Vec4V maskZ = Vec4V(ScalarVFromU32((1<<10)-1));
		Vec4V quantScale = Vec4V(errorSize);

		Vec4V CX = Vec4V(centre.GetX());
		Vec4V CY = Vec4V(centre.GetY());
		Vec4V CZ = Vec4V(centre.GetZ());


		Vec4V vin0 = *packVerts++;

		Vec4V z0 = vin0 & maskZ;
		Vec4V x0 = Vec4V(Vec::V4ShiftRight<21>(vin0.GetIntrin128()));
		Vec4V y0 = Vec4V(Vec::V4ShiftRight<10>(vin0.GetIntrin128()));

		z0 = IntToFloatRaw<0>(z0);
		x0 &= mask;
		y0 &= mask;

		x0 = IntToFloatRaw<0>(x0);
		y0 = IntToFloatRaw<0>(y0);

		for (int i =0; i < pv; i+=4 )  // pipeline to cover 12 cycle latency instructions
		{			
			Vec4V vin0I = *packVerts++;

			Vec4V rz0= AddScaled( CZ, z0, quantScale);
			Vec4V rx0= AddScaled( CX, x0, quantScale);
			Vec4V ry0= AddScaled( CY, y0, quantScale);

			Vec4V x = Vec4V(Vec::V4ShiftRight<21>(vin0.GetIntrin128()));
			Vec4V y = Vec4V(Vec::V4ShiftRight<10>(vin0.GetIntrin128()));
			z0 = vin0I & maskZ;

			x0 = x &  mask;
			y0 = y &  mask;

			z0 = IntToFloatRaw<0>(z0);  
			x0 = IntToFloatRaw<0>(x0);
			y0 = IntToFloatRaw<0>(y0);

			Transpose4x3to3x4( verts[i+0], verts[i+1], verts[i+2], verts[i+3], rx0, ry0, rz0 );
		}

		Vec4V rz0= AddScaled( CZ, z0, quantScale);
		Vec4V rx0= AddScaled( CX, x0, quantScale);
		Vec4V ry0= AddScaled( CY, y0, quantScale);
		Transpose4x3to3x4( verts[pv+0], verts[pv+1], verts[pv+2], verts[pv+3], rx0, ry0, rz0 );
	}

#else
	// Note Vertices waste 30% of storage
	// need to pack XXXX,YYY,ZZZ for better compression
	// XXYYZZ__
	void OccludeModel::PackVertices( const Vec3V* inverts, int numVerts, ScalarV_In errorSize )
	{
		ScalarV invErrorSize = Invert(errorSize);
		Vec3V centre = (m_bmin + m_bmax ) *Vec3V(V_HALF);
		// quantize centre
		centre = Snap( centre, invErrorSize, errorSize );

		//Assert( IsTrue( ( m_bmax - m_bmin) < ( errorSize*(1<<15)) ) ))
		int pv = (numVerts+1)&~1;
		Vec3V quantScale = Vec3V(invErrorSize);
		Vec3V offset = centre * -quantScale;
		Vec4V* packVerts = (Vec4V*)m_verts;
		for (int i =0; i < pv; i+=2 )
		{	
			int i2 = Min(i+1 , numVerts-1);
			Vec3V p0 = AddScaled( offset, inverts[i] , quantScale);		
			Vec3V p1 = AddScaled( offset, inverts[i2], quantScale);
			p0 =  FloatToIntRaw<0>(p0);
			p1 =  FloatToIntRaw<0>(p1);

			packVerts[i/2]=Vec4V(Vec::V4PackSignedIntToSignedShort( p0.GetIntrin128(), p1.GetIntrin128()));
		}	

	}
	void OccludeModel::UnPackVertices( Vec3V* verts, int /*arraySize*/, ScalarV_In errorSize ) const 
	{
		ScalarV invErrorSize = Invert(errorSize);
		Vec3V centre = (m_bmin + m_bmax ) *Vec3V(V_HALF);
		centre = Snap( centre, invErrorSize, errorSize );

		// quantize centre
		int pv = (m_numVerts+1)&~1;
		Vec3V invQuantScale = Vec3V(errorSize);
		Vec4V* packVerts = (Vec4V*)m_verts;
		for (int i =0; i < pv; i+=2 )
		{	
			Vec4V vin = packVerts[i/2];
			Vec4V p0 =Vec4V(Vec::V4UnpackLowSignedShort( vin.GetIntrin128()));
			Vec4V p1 =Vec4V(Vec::V4UnpackHighSignedShort( vin.GetIntrin128()));

			p0 = IntToFloatRaw<0>(p0);
			p1 = IntToFloatRaw<0>(p1);
			verts[i]=  AddScaled( centre, p0.GetXYZ(), invQuantScale);
			verts[i+1]= AddScaled( centre,p1.GetXYZ(), invQuantScale);
		}
	}

#endif
	void OccludeModel::InitClass()
	{
#if __BANK
		grcRasterizerStateDesc rsDesc;
		rsDesc.CullMode = grcRSV::CULL_FRONT;

		sm_hFrontFaceCullState = grcStateBlock::CreateRasterizerState(rsDesc);
		Assert(sm_hFrontFaceCullState != grcStateBlock::RS_Invalid);
#endif
	}


	OccludeModel::OccludeModel( int numVerts, Vec3V* verts, int numIndices, int* indices, ScalarV_In invErrorSize  )
	{
		m_flags=0; // clear 
		Assign(m_numTris,numIndices/3);

		if ( sm_CurrentVertexFormat == VFMT_F32)
		{
			m_numTris |=BIT_IS_VFMT_F32;
			Assign(m_numVertsInBytes,numVerts*sizeof(float)*3);
		}
		else
		{
			Assign(m_numVertsInBytes,numVerts*4);
		}
		Assert( GetNumVerts() == numVerts );
		Assert( GetNumIndices() == numIndices );


		// compute bounding box
		Vec3V bmin= Vec3V( V_FLT_MAX );
		Vec3V bmax= Vec3V( V_NEG_FLT_MAX );
		for (int i = 0; i < numVerts; i++)
		{
			bmin = Min( bmin, verts[i]);
			bmax = Max( bmax, verts[i]);
		}
		m_bmin = bmin;
		m_bmax = bmax;

		m_dataSize = GetGeoBlockSize();
		m_verts = rage_aligned_new(16) u8[m_dataSize];
		PackVertices( verts, GetNumVerts(), invErrorSize );

		u8* rindices = GetIndices();
		for (int i =0; i < numIndices; i++)
		{
			Assign(rindices[i],indices[i]);
		}


		Displayf("Generate Occ Model Verts( %i ) Indices (%i) Vert Size (%i) Size (%i) ",GetNumVerts(), GetNumIndices(), CalcVertSize(GetNumVerts()), GetMemorySize() );

		if ( GetNumVerts()<16){
			Warningf("Generated a Very Small Model, this is inefficient");
		}
	}


	int OccludeModel::GetMaxModelSize()
	{
		return GetMaxVertSize(rstFastTiler::MaxIndexedVerts) + sizeof(u8)*rstFastTiler::MaxIndexedVerts*6;
	}

	void OccludeModel::LoadGeometryLocally( u8* buffer, int DMATag )
	{
		if ( IsEmpty())
			return;

		//#if __SPU
		int blockSize = GetGeoBlockSize();
		Assertf( blockSize < 8*1024,"Big Occ Model Verts( %i ) Indices (%i) Vert Size (%i) Size (%i) ",
			GetNumVerts(), GetNumIndices(), CalcVertSize(GetNumVerts()), GetMemorySize() );
		if ( blockSize >= 8*1024) // corrupt data
		{
			m_verts=0; // make it empty
			return;
		}

#		if !__SPU
			u8* data = taskLoadBuffer( "occludeModelGeom", buffer, 
				m_verts, blockSize, DMATag);
#		else
			u32 vertsEa = (u32)m_verts;
			u32 dmaBegin = vertsEa & ~15;
			u32 dmaEnd = (vertsEa + blockSize + 15) & ~15;
			u8* data = taskLoadBuffer( "occludeModelGeom", buffer, 
				(u8*)dmaBegin, dmaEnd-dmaBegin, DMATag) + (vertsEa&15);
#		endif

		m_verts=data;
		//#endif
	}

	OccludeModel::OccludeModel( datResource &rsc )
	{
		rsc.PointerFixup(m_verts);
	}


#if __BANK
	bool OccludeModel::IsValid()
	{
		float* v = (float*)m_verts;
		Vec3V eps(ScalarV(0.001f));
		for(int i=0;i<GetNumVerts();i++)
		{
			Vec3V p( v[i*3],v[i*3+1],v[i*3+2]);
			if ( !IsTrueXYZ( p>= (m_bmin- eps)) ||!IsTrueXYZ( p<= (m_bmax+eps))){
				Assertf(false,"Invalid Position in occluder model");
				return false;
			}

		}
		return true;
	}
#endif
	bool OccludeModel::ByteSwapOccludeModel(OccludeModel& in)
	{
		Assert16(in.m_verts);
		// NOTE that the incoming data is always little endan since it's coming from a .cmap file!
		if (sysEndian::IsBig())
		{
			u32* srcVerts = (u32*) in.m_verts;
			u32* destVerts = (u32*) in.m_verts;
			const int vertCount = in.GetNumVerts();

			if (in.GetVertexFormat()==VFMT_F32 )
			{
				for (int x = 0; x < (vertCount * 3); x++)
					*(destVerts++) = sysEndian::Swap(*(srcVerts++));
			}
			else
			{
				for (int x = 0; x < vertCount; x++)
					*(destVerts++) = sysEndian::Swap(*(srcVerts++));
			}
		}
#if __BANK
		return in.IsValid();
#else
		return true;
#endif
	}

	int rstSelectModelOccluders(  const rstTransformToScreenSpace& screen, const COccludeModelBucket* __restrict pBuckets,  OccludeModel* __restrict occlist, u8* isClipped, size_t amt, 
		size_t maxOccluders, ScalarV_In threshold, u32* packedArea SPU_ONLY(, OccludeModel* pElements))
	{
		rstSplatMat44V splatMtx;

		Vec3V maxBounds;
		Vec3V minBounds(0.f,0.f,-100.f);
		ScalarV rescaleDepth = rstCreateCurrentSplatMat44V( screen, splatMtx, maxBounds);
		ScalarV maxArea = maxBounds.GetX()*maxBounds.GetY();
		int cnt =  0;
		float eps = TRANSFORM_EXTRA_CLIP_THRESHOLD;
		ScalarV clipPlane = ScalarVFromF32(eps)*rescaleDepth;

		Vec3V cameraPos=screen.m_campos;

		for (size_t i = 0; i < amt; i++)
		{
			const COccludeModelBucket& bucket = pBuckets[i];
#if __SPU
			// EJ: Just to be 100% safe since the SPU hates me
			if (0 == bucket.count || NULL == bucket.pElements)
				continue;

			const int dataSize = bucket.count * sizeof(OccludeModel);
			sysDmaLargeGetAndWait((const void*)pElements, reinterpret_cast<u64>(pBuckets[i].pElements), dataSize, 0);			
#else
			OccludeModel* pElements = bucket.pElements;
#endif
			Assert(pElements);

			for (int j = 0; j < bucket.count; ++j)
			{
				const OccludeModel& occ = pElements[j];

				Vec3V scbmin;
				Vec3V scbmax;
				if (!rstCalculateScreenSpaceBoundIsVis(splatMtx, rescaleDepth,occ.GetMin(), occ.GetMax(),scbmin, scbmax, maxBounds))
				{
					continue;
				}

				// pipeline these two

				// clip to screen first
				scbmax = Clamp( scbmax, minBounds, maxBounds);
				scbmin = Clamp( scbmin, minBounds, maxBounds);

				// could quickly sort on screen area

				// calculate lod
				Vec3V diff = scbmax - scbmin;
				isClipped[cnt] = (u8)IsLessThanOrEqualAll( scbmin.GetZ(), clipPlane);
				ScalarV area = diff.GetX() * diff.GetY();

				VecBoolV inCamera = cameraPos > occ.GetMin() & cameraPos < occ.GetMax();
				BoolV inCam=inCamera.GetX()& inCamera.GetY()&inCamera.GetZ();

				area = SelectFT( inCam, area, maxArea);	
				occlist[cnt] = occ;
				int parea=Vec4V(FloatToIntRaw<0>(area)).GetXi();
				Assert( parea>=0 && parea < (1<<16));
				packedArea[cnt]= parea<<16 ;
				cnt += IsTrue( (area > threshold) & (scbmax.GetZ() > ScalarV(V_ZERO)) );		
				cnt = Min( cnt, (int)maxOccluders-1);	
			}				
		}
		return cnt;
	}
#if MESH_LIBRARY

	struct Tri
	{
		Vec3V p[3];

		Vec3V_Out GetCentre() const
		{
			return (p[0]+p[1]+p[2])*ScalarVFromF32(1.f/3.f);
		}
	};
	Tri* CreateTriListFromMesh( mshMesh* srcMesh, int& amt )
	{
		amt=srcMesh->GetTriangleCount();
		Tri* tris = rage_new Tri[srcMesh->GetTriangleCount()];
		int cnt=0;
		for (int i=0; i<srcMesh->GetMtlCount(); i++) 
		{
			mshMaterial& mat = srcMesh->GetMtl(i);				
			for (mshMaterial::TriangleIterator ti = mat.BeginTriangles(); 
				ti != mat.EndTriangles(); ++ti)
			{
				int i0,i1,i2;
				ti.GetVertIndices( i0, i1, i2);
				tris[cnt].p[0]=VECTOR3_TO_VEC3V( mat.GetVertex(i0).Pos);
				tris[cnt].p[1]=VECTOR3_TO_VEC3V( mat.GetVertex(i1).Pos);
				tris[cnt++].p[2]=VECTOR3_TO_VEC3V( mat.GetVertex(i2).Pos);
			}
		}
		Assert( cnt == srcMesh->GetTriangleCount());
		return tris;
	}
	mshMesh* CreateMeshFromTriList( Tri* start,Tri* end )
	{
		mshMesh* srcMesh = rage_new mshMesh();

		mshMaterial& mat = srcMesh->NewMtl();	
		mat.Prim.Resize(1);
		mshPrimitive& P = mat.Prim[0];
		int amt = ptrdiff_t_to_int(end-start);
		P.Type=mshTRIANGLES;
		P.Priority=1;
		P.Idx.Resize( amt*3);
		for (int i=0; i < amt;i++)
			for ( int j=0; j < 3; j++)
				P.Idx[ i*3+j]=i*3+j;


		for ( Tri* t=start; t!=end; ++t)
		{
			for (int i=0;i<3;i++){
				mshVertex v0;
				v0.Pos=VEC3V_TO_VECTOR3(t->p[i]);
				mat.AddVertex(v0);
			}
		}
		srcMesh->CleanModel();
		return srcMesh;
	}

	bool Split( Tri* start, Tri* end, Vec3V_In boundSize, ScalarV_In errorSize )
	{
		Vec3V maxBoxSize( (float)(1<<11),(float)(1<<11), (float)(1<<10));
		maxBoxSize *=errorSize;

		int smallEnough = ( OccludeModel::GetCurrentVertexFormat() == OccludeModel::VFMT_F32) || IsLessThanAll(boundSize, maxBoxSize);
		int amt=ptrdiff_t_to_int(end-start)*3;
		// note should split if boundSize is greater than compression
		return amt > (rstFastTiler::MaxIndexedVerts*4) || smallEnough==0;
	}
	struct OnLeftSide
	{
		float centre;
		int  ax;
		OnLeftSide( int Axis, float c ) : centre( c), ax( Axis ) {}

		bool operator()( const Tri& a )		{
			return a.GetCentre()[ax] < centre;
		}
	};
	OccludeModel GenerateOcclusionModel( mshMesh* srcMesh, ScalarV_In errorSize )
	{
		// create occluder Mesh
		mshMaterial& mat = srcMesh->GetMtl(0);
		int numVerts=mat.GetVertexCount();
		int numIndices=mat.GetTriangleCount()*3;

		Vec3V* verts = rage_new Vec3V[ numVerts];
		int*	  indices = rage_new int[numIndices];
		

		for (int j = 0; j < numVerts;j++)
		{
			verts[j]= VECTOR3_TO_VEC3V(mat.GetVertex(j).Pos);
		}

		int ic=0;
		for (mshMaterial::TriangleIterator ti = mat.BeginTriangles(); 
			ti != mat.EndTriangles(); ++ti, ic+=3)
		{
			ti.GetVertIndices( indices[ic+0], indices[ic+1], indices[ic+2]);
		}

		sysMemEndTemp();
		OccludeModel model( numVerts, verts,numIndices, indices, errorSize );
		sysMemStartTemp();

		delete[] verts;
		delete[] indices;

		return model;
	}
	void BuildOccluderMeshesRecurse( Tri* start, Tri* end , ScalarV_In errorSize,  atArray<OccludeModel>& models )
	{
		Vec3V bmin( V_FLT_MAX);
		Vec3V bmax( V_NEG_FLT_MAX);
		for ( Tri* t = start; t!=end; ++t)
		{
			for (int i=0;i<3;i++)
			{
				bmin = Min( bmin, t->p[i]);
				bmax = Max( bmax, t->p[i]);
			}
		}

		Vec3V boundSize	= bmax-bmin;
		if (!Split( start, end, boundSize , errorSize ))
		{
			mshMesh* srcMesh=CreateMeshFromTriList( start, end );
			mshMaterial& mat = srcMesh->GetMtl(0);
			int numVerts=mat.GetVertexCount();
			if ( numVerts< 256)
			{
				models.Grow()= GenerateOcclusionModel(srcMesh, errorSize ) ;
				delete srcMesh;
				return;
			}
			delete srcMesh;
		}
		int Axis =2;
		if ( boundSize.GetXf() > boundSize.GetZf())
			if ( boundSize.GetXf() > boundSize.GetYf())
				Axis=0;
			else
				Axis=1;
		else
			if ( boundSize.GetYf() > boundSize.GetZf())
				Axis=1;

		// should we use median?
		Tri* mid=start;
		int doRetry=0;
		do 
		{
		
			float centre = ( bmin[ Axis ]+bmax[ Axis ] ) * 0.5f;
			mid = std::partition( start, end, OnLeftSide( Axis, centre ));
			if ( mid== start || mid == end ){
				Axis= (Axis+1)%3;
				doRetry++;
			}
			else 
				doRetry=0;
		}
		while( doRetry && doRetry<4);

		if ( mid == start )
		{
			++mid;
		}
		else if  ( mid == end )
		{
			--mid;
		}
		FastAssert( mid != start );
		FastAssert( mid != end );

		BuildOccluderMeshesRecurse( start, mid , errorSize, models);
		BuildOccluderMeshesRecurse( mid, end, errorSize, models);			
	}


	static int occModelPacketize(const mshMesh&/*mesh*/,const mshMaterial&/*mtl*/,void* /*closure*/) {
		return rstFastTiler::MaxIndexedVerts;
	}
	void DumpMemUsage( atArray<OccludeModel>& models )
	{

		int totalMem=0;
		int totalVerts = 0;
		for (int i =0; i < models.GetCount();i++)
		{
			totalVerts+= models[i].GetNumVerts();
			totalMem += models[i].GetMemorySize();
		}
		Displayf(" Total Amount of Memory %ik (%i)  Verts (%i) Models (%i) ", totalMem>>10,totalMem, totalVerts,models.GetCount());
	}
	void LoadOccludeModel( atArray<OccludeModel>& models,const char* meshFileName, ScalarV_In errorSize )
	{
		sysMemStartTemp();

		mshMesh* srcMesh = rage_new mshMesh;
		if ( SerializeFromFile(meshFileName, *srcMesh, "mesh") == false ) 
		{
			Errorf("Can't load mesh file %s", meshFileName);
			return;
		}


		bool useSpatialSplit=true;
		if ( useSpatialSplit )
		{
			int amtTris;
			Tri* triList=CreateTriListFromMesh( srcMesh, amtTris);
			BuildOccluderMeshesRecurse(   triList, triList+amtTris , errorSize, models );
			delete []triList;

			DumpMemUsage( models);	

			delete srcMesh;
			sysMemEndTemp();
			// to get around fragmentation issues
			atArray<OccludeModel> actmodels(models);
			models=actmodels;
			return;
		}

		srcMesh->ClearVtxAttributes();
		srcMesh->MergeSameMaterials();

		mshMesh* srcMesh2 = rage_new mshMesh;
		srcMesh2->AddMtl(srcMesh->GetMtl(0));
		for (int i = 1; i < srcMesh->GetMtlCount(); i++)
		{
			srcMesh2->GetMtl(0).Merge(srcMesh->GetMtl(i));
		}
		delete srcMesh;
		srcMesh = srcMesh2;
		srcMesh->CleanModel();
		// compute single block of memory for Verts and Indices

	

		Vec3V* verts = rage_new Vec3V[ rstFastTiler::MaxIndexedVerts];
		int*	  indices = rage_new int[rstFastTiler::MaxIndexedVerts*6];

		int vc=0;
		int ic=0;

		// if number of verts > 256 need to split model up into smaller batch sizes 
		srcMesh->Packetize(occModelPacketize, 0, 1024);
		sysMemEndTemp();

		int cntNumModels=0;
		for (int i=0; i<srcMesh->GetMtlCount(); i++) 
		{
			mshMaterial& mat = srcMesh->GetMtl(i);
			if ( (mat.GetVertexCount() + vc) > rstFastTiler::MaxIndexedVerts )
			{
				vc = 0;
				cntNumModels++;
			}
			vc+=mat.GetVertexCount();
		}
		if ( vc> 0 )
			cntNumModels++;

		models.Resize(cntNumModels);

		int numModels=0;
		vc=0;

		for (int i=0; i<srcMesh->GetMtlCount(); i++) 
		{

			mshMaterial& mat = srcMesh->GetMtl(i);
			if ( (mat.GetVertexCount() + vc) > rstFastTiler::MaxIndexedVerts )
			{
				OccludeModel model( vc, verts, ic, indices, errorSize );
				models[numModels++]=model;
				vc = 0;
				ic = 0;
			}
			int ib =vc;
			for (int j = 0; j < mat.GetVertexCount();j++)
			{
				const Vector3& v = mat.GetVertex(j).Pos;
				verts[vc++]= Vec3V( v.x, v.y, v.z);
			}

			for (mshMaterial::TriangleIterator ti = mat.BeginTriangles(); 
				ti != mat.EndTriangles(); ++ti)
			{
				ti.GetVertIndices( indices[ic+0], indices[ic+1], indices[ic+2]);
				indices[ic+0] +=ib;
				indices[ic+1] +=ib;
				indices[ic+2] +=ib;

				Assert( indices[ic+0] < rstFastTiler::MaxIndexedVerts);
				Assert( indices[ic+1] < rstFastTiler::MaxIndexedVerts);
				Assert( indices[ic+2] < rstFastTiler::MaxIndexedVerts);

				ic+=3;
			}
		}

		if ( vc> 0 )
		{
			OccludeModel model( vc, verts, ic, indices, errorSize );
			models[numModels++]=model;
			vc = 0;
			ic = 0;
		}
		Assert( cntNumModels == numModels);


		sysMemStartTemp();
		delete [] verts;
		delete [] indices;

		delete  srcMesh;
		sysMemEndTemp();


		DumpMemUsage( models);
	}

#endif


	// TODO : resourcing needs to be finished.

	IMPLEMENT_PLACE(OccludeModelList);
	IMPLEMENT_PLACE(OccludeModel);
	////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
	void OccludeModelList::DeclareStruct(class datTypeStruct &s) 
	{
		pgBase::DeclareStruct(s);
		SSTRUCT_BEGIN_BASE(OccludeModelList, pgBase)
			SSTRUCT_FIELD(OccludeModelList, m_models)	
			SSTRUCT_END(OccludeModelList)
	}
	void OccludeModel::DeclareStruct(class datTypeStruct &s) 
	{
#if __RESOURCECOMPILER
		if (g_ByteSwap)
		{
			u32* vel = (u32*)m_verts;
			for (int i =0; i < GetNumVerts(); i++)
			{
				datSwapper( vel[i]);
			}
		}  // don't need to swap indices
#endif // __RESOURCECOMPILER
		SSTRUCT_BEGIN(OccludeModel)
			SSTRUCT_FIELD(OccludeModel, m_bmin)
			SSTRUCT_FIELD(OccludeModel, m_bmax)
			SSTRUCT_FIELD(OccludeModel, m_dataSize)
			SSTRUCT_DYNAMIC_ARRAY(OccludeModel,m_verts,m_numVertsInBytes)
			SSTRUCT_FIELD(OccludeModel, m_numVertsInBytes)
			SSTRUCT_FIELD(OccludeModel, m_numTris)
			SSTRUCT_FIELD(OccludeModel, m_flags)
			SSTRUCT_END(OccludeModel)
	}

#endif // __DECLARESTRUCT

#if __BANK
	void OccludeModel::Draw( float bitPrecisionf, bool showBounds, bool doubleSided ) const
	{
		if ( IsEmpty())
			return;
		
		ScalarV bitPrecision = ScalarVFromF32(bitPrecisionf) ;
		Vec3V verts[rstFastTiler::MaxIndexedVerts];
		UnPackVertices( verts, rstFastTiler::MaxIndexedVerts, bitPrecision);
		u8* indices= GetIndices();
		int numIndices = GetNumIndices();


		//grcColor3f(0.5f,1.f,0.5f);

		Vector3		frontfaceColors[3] = { Vector3(1.0f,0.0f,0.0f), Vector3(0.0f,1.0f,0.0f), Vector3(0.0f,0.0f,1.0f) };
		Vector3		backfaceColors[3] = { Vector3(0.4f,0.0f,0.0f), Vector3(0.0f,0.4f,0.0f), Vector3(0.0f,0.0f,0.4f) };

		int passCount = doubleSided ? 2 : 1;

		for (int pass=0; pass<passCount; pass++)
		{
			Vector3 *colors = (pass == 1) ? backfaceColors : frontfaceColors;
			grcStateBlock::SetRasterizerState( (pass == 1) ? sm_hFrontFaceCullState 
													: grcStateBlock::RS_Default );
			int start =0;
			while( start < numIndices)
			{
				int end =  Min(start + grcBeginMax3, numIndices);
				grcBegin(drawTris,end-start);
				for (int i = start; i < end; i++)
				{
					grcColor3f( colors[i%3].GetX(), colors[i%3].GetY(), colors[i%3].GetZ() );
					grcVertex3f( VEC3V_TO_VECTOR3(verts[indices[i]] ));
				}
				start  = end;
				grcEnd();
			}
		}	

		if ( showBounds )
		{
			grcDrawBox( VEC3V_TO_VECTOR3( m_bmin), VEC3V_TO_VECTOR3( m_bmax), Color32(1.f,0.5f,0.5f) );
		}
	}
#endif

	OccludeModelList::OccludeModelList(datResource &rsc) 
		: m_models(rsc,1)
	{}

#if SUPPORT_OCCLUDER_MESH

	void CheapShadowMesh::Init()
	{
		// setup vertex declaration
		grcVertexElement element( 0, grcVertexElement::grcvetPosition, 0, grcFvf::GetDataSizeFromType(grcFvf::grcdsFloat3),grcFvf::grcdsFloat3);
		sm_vdecl = AssertRetVal( GRCDEVICE.CreateVertexDeclaration(&element,1 ));		
	}
	   
	void CheapShadowMesh::Destroy()
	{
		delete sm_vdecl;
	}

	bool CheapShadowMesh::AreBuffersValid() const
	{
#if __XENON
		grcVertexBufferD3D*		d3dVerts = static_cast< grcVertexBufferD3D* >( m_verts );
		grcIndexBufferD3D*		d3dIndices = static_cast< grcIndexBufferD3D* >( m_indices );

		return m_vertexBufferMemory  && m_indexBufferMemory && d3dVerts->GetD3DBuffer() && d3dIndices->GetD3DBuffer();
#else
		return m_vertexBufferMemory  && m_indexBufferMemory;
#endif
	}

	PS3_ONLY(extern u32 g_AllowVertexBufferVramLocks;)

#if !__SPU 
	CheapShadowMesh::CheapShadowMesh(  const OccludeModel* modelList, int amt, MeshAllocator* allocator )
	{
		int totalIndices = 0;
		int totalVerts=0;
		Vec3V tbmin(V_FLT_MAX);
		Vec3V tbmax(V_NEG_FLT_MAX);

		// Calculate totals
		for (int i=0; i< amt;i++)
		{
			const OccludeModel& mod = modelList[i];
			Assert(!mod.IsEmpty());

			totalIndices += mod.GetNumIndices();
			totalVerts +=  mod.GetNumVerts();
			tbmin = Min( tbmin, mod.GetMin());
			tbmax = Max( tbmax, mod.GetMax());
		}
		m_bound = spdAABB( tbmin, tbmax);

		// generate the vertex buffer
		grcFvf fvf0;
		fvf0.SetPosChannel(true, grcFvf::grcdsFloat3);

		int vertexBufferSize = totalVerts * sizeof(float)*3;
		int indexBufferSize = totalIndices * sizeof(u16);
	
		m_vertexBufferMemory = allocator->Allocate( vertexBufferSize);
		m_indexBufferMemory =  allocator->Allocate( indexBufferSize);
		m_verts = 0;
		m_indices = 0;
		if ( !m_vertexBufferMemory || !m_indexBufferMemory)
		{				
			Warningf("Streaming Buffers allocation for CheapShadowMesh failed, probably because out of memory" );
			return;
		}
		if ( m_vertexBufferMemory && m_indexBufferMemory)
		{
			const bool bReadWrite = true;
			const bool bDynamic = allocator->IsDynamic();
			m_verts = grcVertexBuffer::Create(totalVerts, fvf0, bReadWrite, bDynamic, m_vertexBufferMemory);
			m_indices = grcIndexBuffer::Create(totalIndices, bDynamic, m_indexBufferMemory);
		}
		

		if ( !AreBuffersValid() )
		{
			Warningf("Buffers allocation for CheapShadowMesh failed, probably because out of memory" );
			return;
		}

		{
			// Index buffers must be locked before vertex buffers
			u16*   outIndices = m_indices->LockRW();

			// not resourcing this currently
			PS3_ONLY(g_AllowVertexBufferVramLocks++;)

			grcVertexBuffer::LockHelper lock(m_verts);

			float* outVerts = (float*) lock.GetLockPtr();

			ASSERT_ONLY( float* startVerts = outVerts);
			ASSERT_ONLY( u16* startIndices = outIndices);

			int    cIdx=0;
			
			for (int i=0;i< amt;i++)
			{
				Vec3V inVerts[rstFastTiler::MaxIndexedVerts];
				const OccludeModel& mod = modelList[i];

				// get verts
				mod.UnPackVertices( inVerts, rstFastTiler::MaxIndexedVerts, ScalarV(OCCLUDE_MODEL_DEFAULT_ERROR ));
				int numverts = mod.GetNumVerts();	 
				for (int j=0;j< numverts ;j++ , outVerts +=3)
				{	 										 
					// can't do a full memcpy as inVerts are Vec3V.
					memcpy(outVerts, (void*)&inVerts[j], 12);					
				}

				// get indices
				const u8* indices =  mod.GetIndices();				
				for (int j=0;j< mod.GetNumIndices() ;j++){
					u16 idx;
					Assign(idx, (int)indices[j]+cIdx);
					*outIndices++=idx;
				}
				cIdx += numverts;
			}

			Assert( (outVerts - startVerts) == m_verts->GetVertexCount()*3);
			Assert( m_verts->GetVertexStride() == 12);
			Assert( (outIndices - startIndices) ==  m_indices->GetIndexCount());

			m_indices->UnlockRW();
			
			PS3_ONLY(g_AllowVertexBufferVramLocks--;)
		}

		if (!allocator->IsDynamic())
		{
			m_verts->MakeReadOnly();
		}
	}
#endif // !__SPU

	void CheapShadowMesh::Release( MeshAllocator* allocator )
	{
		allocator->Free(m_indexBufferMemory);
		allocator->Free(m_vertexBufferMemory);	
		m_indexBufferMemory = 0;
		m_vertexBufferMemory = 0;
	}
	CheapShadowMesh::~CheapShadowMesh()
	{
		delete m_indices;
		delete m_verts;
	}

	void CheapShadowMesh::Render() const
	{
		Assert( AreBuffersValid() );

		// assume shader is set outside
		GRCDEVICE.SetVertexDeclaration(sm_vdecl);	 // could be outside main loop
		GRCDEVICE.SetStreamSource(0, *m_verts, 0, m_verts->GetVertexStride());
		GRCDEVICE.SetIndices( *m_indices);
		GRCDEVICE.DrawIndexedPrimitive( drawTris, 0, m_indices->GetIndexCount());
		GRCDEVICE.ClearStreamSource(0);
	}

#endif // SUPPORT_OCCLUDER_MESH

};

