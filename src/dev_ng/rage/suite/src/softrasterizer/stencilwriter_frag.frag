
#ifndef STENCILWRITER_FRAG
#define STENCILWRITER_FRAG 

#include "system/dependency.h"

using namespace rage;
SPUFRAG_IMPL(bool, stencilwriter_frag, sysDependency& dep)
{
	// noop for now
	return true;
}

#endif // STENCILWRITER_FRAG

