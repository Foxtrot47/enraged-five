
#ifndef _BOXSELECTOR_FRAG_H
#define _BOXSELECTOR_FRAG_H

#define FRAG_TSKLIB_ONLY

#include "softrasterizer/scansetup.h"
#include "system/codefrag_spu.h"
#include "system/dependency.h"
#include "vectormath/classes.h"
#if !__WIN32 && !RSG_ORBIS
#include "boxoccluder.cpp"
#endif
#include "softrasterizer/tiler.h"
#include "softrasterizer/softrasterizer.h"

using namespace rage;
	
int rstSelectBoxOccludersFrag( const rstTransformToScreenSpace& screen, const CBoxOccluderBucket* __restrict pBuckets,  
							BoxOccluder* __restrict rocclist, u8* isClipped, int amt, 
							int maxOccluders, ScalarV_In threshold SPU_ONLY(, BoxOccluder* pElements))
{
	rstSplatMat44V splatMtx;
	Vec3V maxBounds;
	Vec3V minBounds(0.f,0.f,-100.f);
	ScalarV rescaleDepth = rstCreateCurrentSplatMat44V( screen, splatMtx, maxBounds);
	int cnt =  0;
	float eps = 0.001f;
	ScalarV clipPlane = ScalarVFromF32(eps);
	ScalarV zero(V_ZERO);

	for (int i = 0; i < amt; i++)
	{
		const CBoxOccluderBucket& bucket = pBuckets[i];
#if __SPU
		// EJ: Just to be 100% safe since the SPU hates me
		if (0 == bucket.count || NULL == bucket.pElements)
			continue;

		const int dataSize = bucket.count * sizeof(BoxOccluder);
		sysDmaLargeGetAndWait((const void*)pElements, reinterpret_cast<u64>(pBuckets[i].pElements), dataSize, 0);
#else
		BoxOccluder* pElements = bucket.pElements;
#endif
		Assert(pElements);

		for (int j = 0; j < bucket.count; ++j)
		{
			const BoxOccluder& occ = pElements[j];
			
			Vec3V scbmin;
			Vec3V scbmax;

			Vec3V oobb[8];
			occ.CalculateVerts(oobb);
			Vec3V wmin,wmax;
			rstCalculateScreenSpaceOOBBBound(splatMtx, rescaleDepth,oobb, scbmin,scbmax,wmin,wmax);

			// clip to screen first
			scbmax = Clamp( scbmax, minBounds, maxBounds);
			scbmin = Clamp( scbmin, minBounds, maxBounds);

			// calculate selection threshold
			Vec3V diff = scbmax - scbmin;
			isClipped[cnt] =(u8) IsLessThanOrEqualAll( scbmin.GetZ(), clipPlane);
			ScalarV area = diff.GetX() * diff.GetY();
			rocclist[cnt] = occ;

			cnt +=  IsTrue( (area > threshold)& (scbmax.GetZ() > zero) );	
			cnt = Min( cnt, maxOccluders);
		}
	}
	return cnt;
}
	
SPUFRAG_IMPL(bool, boxselector_frag, sysDependency& task)
{

	int numInBuckets;
	const CBoxOccluderBucket* pBuckets = tskGetInput<CBoxOccluderBucket>(task, 1, numInBuckets);

	sysTaskBuffer Scratch = tskGetScratch( task);
	ScratchMemStackPoint cm( Scratch);
	u8*  isClipped = taskAllocBuffer<u8,16>(Scratch, 1024); 
	BoxOccluder* resboxes =  taskAllocBuffer<BoxOccluder,16>(Scratch,512);
	const rstTransformToScreenSpace* clipAndTransform = tskGetInput<rstTransformToScreenSpace>( task,2);
	BoxSelectorUserData userData = tskGetUserData<BoxSelectorUserData>( task);

#if __SPU
	// EJ: Ugly, but the most memory-efficient way of handling this
	int maxOccluders = 0;
	for (int i = 0; i < numInBuckets; ++i)
	{
		if (pBuckets[i].count > maxOccluders)
			maxOccluders = pBuckets[i].count;
	}
	
	BoxOccluder* pElements = taskAllocBuffer<BoxOccluder, 16>(Scratch, maxOccluders);
#endif

	int amt = rstSelectBoxOccludersFrag( *clipAndTransform, pBuckets, resboxes, isClipped,	numInBuckets, 512, ScalarVFromF32(  userData.threshold) SPU_ONLY(, pElements));

	// write out explicitly to output
	taskWriteBuffer( "clipFlags",isClipped, userData.outputClipped, TSK_ROUNDUP16(amt), TAG_WRITETILER_0 );
	taskWriteBuffer( "resOccluders",resboxes, userData.outputOccluders, amt, TAG_WRITETILER_0 );

	// fire off child tasks.
	int NumTilers = userData.NumTilers;
	int stepSize = amt/ NumTilers;
	int err = amt - stepSize*NumTilers;
	int cerr=err;

	int c = 0;
	BEGIN_ALIGNED(16) TileDataPacket<BoxOccluder>	packets[RAST_NUM_TILERS_ACTIVE] ALIGNED(16);

	for (int i = 0; i < NumTilers; i++, c+= stepSize)
	{
		int ci=0;
		if ( cerr >= NumTilers){
			ci=1;
			cerr-=NumTilers;
		}
		cerr += err;

		int end =  Min( amt, (c +stepSize+ci));
		int size=  Max(end-c,0);

		packets[i].clippedFlags = userData.outputClipped + c;
		packets[i].occluders = userData.outputOccluders+ c;
		packets[i].amt = size;		
		c+=ci;
	}
	Assert( c >= amt);
	taskWriteBuffer( "TilerPackets",packets, userData.packets, NumTilers, TAG_WRITETILER_0 );
	taskWait( TAG_WRITETILER_0);
	return true;
}


#endif //_BOXSELECTOR_FRAG_H