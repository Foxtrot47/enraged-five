// 
// crmotiontree/dependency_filter.frag
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef ADD_TASKS_FRAG
#define ADD_TASKS_FRAG 

#include "system/dependency.h"
#include "system/dependencyscheduler.h"
#include "softrasterizer/tsklib.h"

#if __SPU
#include "system/dependencyscheduler.cpp"
#endif

using namespace rage;

SPUFRAG_IMPL(bool, addTasks_frag, sysDependency& dep)
{
	int numParents=0;
	sysDependency* parents = tskGetInputOutput<sysDependency>( dep, 0, numParents);

	for (int i=0;i<numParents;i++)
	{
		sysDependencyScheduler::Insert(parents + i);
	}

	return true;
}

#endif // ADD_TASKS_FRAG