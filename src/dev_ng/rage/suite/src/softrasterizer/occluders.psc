<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">
							
	<structdef type="rage::BoxOccluder" name="BoxOccluder"  simple="true">
		<s16 name="iCenterX"/>
		<s16 name="iCenterY"/>
		<s16 name="iCenterZ"/>
		<s16 name="iCosZ"/>
		<s16 name="iLength"/>
		<s16 name="iWidth"/>
		<s16 name="iHeight"/>
		<s16 name="iSinZ"/>
	</structdef>
	
	<structdef type="rage::OccludeModel" name="OccludeModel"  simple="true">
		<Vec3V name="m_bmin"/>
		<Vec3V name="m_bmax"/>
		<u32 name="m_dataSize"/>
		<array name="m_verts" type="pointer" sizeVar="m_dataSize" align="16">
			<u8/>
		</array>
		<u16 name="m_numVertsInBytes"/>
		<u16 name="m_numTris"/>
		<u32 name="m_flags"/>
	</structdef>

</ParserSchema>
