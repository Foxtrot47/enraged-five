#ifndef _OCCLUDE_MODEL_H_
#define _OCCLUDE_MODEL_H_


#include "parser/macros.h"
#include "vectormath/legacyconvert.h"
#include "atl/array.h"
#include "data/base.h"
#include "grcore/stateblock.h"
#include "paging/base.h"
#include "mesh/mesh_config.h"
#include "spatialdata/aabb.h"

// EJ: Occlude Model Optimization
#define MAX_NUM_OCCLUDE_MODELS (384+32) 
//(384)
#define SUPPORT_OCCLUDER_MESH 0

namespace rage
{
	struct sysTaskBuffer;


//	PURPOSE
//		Stores vertices and indices for a model for use with the rage rasterizer
//
//  DESCRIPTION
//		An occlusion model is stored in the OccludeModel class. To allow for fast rendering of this model 
//		and easy usage on the SPU, each model can only store 256 vertices. The internal storage is 8 bytes per vertex and 3 bytes per triangle. This keeps memory consumption low, and further compression could be achieved if required. 
//		The compression of vertices uses 16 bits per channel. Each vertex stores the offset from the 
//		center of the bounding volume of the model. A global precision value is used to quantize the 
//		positions first before offsetting. This allows for the vertices on different models to align and
//		for no cracks to appear. ( further description is in this paper [http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.172.7485&rep=rep1&type=pdf] under local vertex quantization with global snapping ).
//		The model consists of a 32 byte header and a separate block for vertex and index information. This allows for loading in the occlude models in a single DMA for 
//		culling and selection followed by DMA in the vertex and index information if required.
//		To load a mesh and convert it to a series of OccludeModel's call.
//
//
//		void LoadOccludeModel( atArray<OccludeModel>& models,const char* meshFileName , ScalarV_In errorSize);
//
//		Error size is the size of a single bit after quantization ( for example 12.5 cm is a nice size for general occlusion ). This generates a series of occlude models due to the limitation of 256 verts per model.
//		An example of this is shown in sample rasterizer.
//		Since this uses general rage meshes, this allows for the normal export toolchain to be used to export models for occlusion. Then a resourcing tool can load them and resource the final models.
//

#define OCCLUDE_MODEL_DEFAULT_ERROR (1.f/2.f)
//#define OCCLUDE_MODEL_DEFAULT_ERROR 1.
	
	// EJ: Occlude Model Optimization
	class OccludeModel;
	typedef atArray<OccludeModel> COccludeModelArray;

	struct ALIGNAS(16) COccludeModelBucket
	{
		COccludeModelArray* pArray;
		OccludeModel* pElements;
		int count;
		int	mapDataSlotIndex;
	} ;

	CompileTimeAssert(0 == (sizeof(COccludeModelBucket) % 16));

	class OccludeModel
	{
	// TODO pack into 32 bytes
	// rearrange into float maxx,minx,maxy,miny,maxz,minz, 
	public:
		static const int									RORC_VERSION = 1;
		static const int									MaxIndexedVerts=256;
		static const int									MaxWaterOccluders=64;

		enum VertexFormat
		{
			VFMT_11_11_10=0,			// U32 format with 4 bytes per vertex
			VFMT_F32					// F32 per channel so 12 bytes per vertex
		};

		enum Flags
		{
			FLAG_WATER_ONLY = BIT(0),
		};
	
		static void SetCurrentVertexFormat( VertexFormat vt )
		{
			sm_CurrentVertexFormat = vt;
		}
		static VertexFormat GetCurrentVertexFormat()
		{
			return sm_CurrentVertexFormat;
		}
		static void InitClass();
		
		OccludeModel() {}
		OccludeModel(datResource &rsc);
		OccludeModel( int numVerts, Vec3V* verts, int numIndices, int* indices, ScalarV_In invErrorSize  );

		static OccludeModel Empty(){
			OccludeModel occ;
			memset(&occ,0,sizeof(OccludeModel));
			return occ;
		}
		bool IsEmpty() const {
			return m_verts==0;
		}

		u8*		GetIndices() const
		{
			return (u8*)m_verts + CalcVertSize(GetNumVerts());
		}
		int			GetNumIndices() const { return GetNumTris() *3; }
		int			GetNumTris() const  { return (m_numTris&~BIT_IS_VFMT_F32); }

		Vec3V_Out GetMin() const { return m_bmin; }
		Vec3V_Out GetMax() const { return m_bmax; }

		
		void Destroy()
		{
			delete m_verts;
		}
		int GetGeoBlockSize() const
		{
			return CalcVertSize(GetNumVerts()) + GetNumIndices()*sizeof(u8);
		}
		VertexFormat GetVertexFormat() const { return IsFloatVertexFormat() ? VFMT_F32 : VFMT_11_11_10; }
		
		int GetNumVerts() const
		{
			return IsFloatVertexFormat() ? m_numVertsInBytes/12 : (m_numVertsInBytes>>2);
		}
		int GetMemorySize() const
		{
			return GetGeoBlockSize() + sizeof(OccludeModel);
		}
		int CalcVertSize( int numVerts ) const ;
		static int GetMaxModelSize();

		void LoadGeometryLocally( u8* buffer, int dmaTag );
		
		void UnPackVertices( Vec3V* verts, int arraySize, ScalarV_In errorSize ) const ;
		void PackVertices( const Vec3V* verts, int numVerts, ScalarV_In errorSize );

		void FreeCopy()
		{
			delete m_verts;
		}
#if __BANK
		void Draw(float bitPrecision, bool showBounds, bool doubleSided =true) const;
#endif

		DECLARE_PLACE	(OccludeModel);

#if	__DECLARESTRUCT
		void				DeclareStruct	(datTypeStruct &s);
#endif

		bool AreFlagsSet(u32 flags) const { return (m_flags & flags) == flags; }
		u32 GetFlags() const { return m_flags; }

		static 	bool ByteSwapOccludeModel( OccludeModel& in );
		BANK_ONLY(bool	IsValid() );


		PAR_SIMPLE_PARSABLE;

	private:

		Vec3V								m_bmin;  // TODO - could pack these
		Vec3V								m_bmax;

		u32									m_dataSize;
		u8*									m_verts;

		u16									m_numVertsInBytes;
		u16									m_numTris;

		u32									m_flags;


		static VertexFormat				sm_CurrentVertexFormat;
		static const  u16				BIT_IS_VFMT_F32 =0x8000;
		static grcRasterizerStateHandle sm_hFrontFaceCullState;

		bool IsFloatVertexFormat() const { return (m_numTris &BIT_IS_VFMT_F32)!=0;  }

	};

	// make this possible to be resourced.
	class OccludeModelList : public pgBase
	{
	public:
		static const int									RORC_VERSION=OccludeModel::RORC_VERSION;
		atArray<OccludeModel> m_models;
		OccludeModelList(datResource &rsc);
		OccludeModelList() {}
		~OccludeModelList()
		{
			for (int i=0;i<m_models.GetCount();i++)
				m_models[i].Destroy();
		}
		DECLARE_PLACE	(OccludeModelList);
#if	__DECLARESTRUCT
		void				DeclareStruct	(datTypeStruct &s);
#endif
	};

	
#if MESH_LIBRARY
	void LoadOccludeModel( atArray<OccludeModel>& models,const char* meshFileName , ScalarV_In errorSize);
#endif
	struct rstTransformToScreenSpace;

	int rstSelectModelOccluders( const rstTransformToScreenSpace& screen, const COccludeModelBucket* __restrict pBuckets,  OccludeModel* __restrict occlist, u8* isClipped, size_t amt, 
		size_t maxOccluders, ScalarV_In threshold , u32* parea SPU_ONLY(, OccludeModel* pElements));

#if SUPPORT_OCCLUDER_MESH
	// fwd references
	class grcIndexBuffer;
	class grcVertexBuffer;
	struct grcVertexDeclaration;

	// CheapShadowModel
	// PURPOSE: uses occluders to build a mesh for distance shadows.
	struct CheapShadowMesh
	{
		grcIndexBuffer*					m_indices;
		grcVertexBuffer*				m_verts; 
		static grcVertexDeclaration*	sm_vdecl;
		void* m_indexBufferMemory;
		void* m_vertexBufferMemory;

	public:

		class MeshAllocator
		{
		public:
			virtual void* Allocate(int size )=0;
			virtual void Free( void* address )=0;		 
			virtual bool IsDynamic()=0;		 
			virtual  ~MeshAllocator() {}
		};

		spdAABB							m_bound;					
	
		static void Init();
		static void Destroy();

		CheapShadowMesh( const OccludeModel* modelList, int amt, MeshAllocator* allocator);
		~CheapShadowMesh();

		void Render() const;
		void Release( MeshAllocator* allocator );

		bool IsValid() const { return AreBuffersValid(); }
	private:
		bool AreBuffersValid() const;
	};
#endif
};



#endif