// 
// softrasterizer/tiler.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SOFTRASTERIZER_TILER_H 
#define SOFTRASTERIZER_TILER_H 

//#include "atl/array.h"
#include "vectormath/classes.h"
#include "scan.h"
#include "softrasterizer/scansetup.h"
#include "system/interlocked.h"
#include "grcore/device.h"

namespace rage {

class OccludeModel;
class grcViewport;
BANK_ONLY(class bkBank;)

#define TRANSFORM_EXTRA_CLIP_THRESHOLD 5.f
#define MAX_CLIP_IN_VERTS 152
#define MAX_CLIP_MULTIPLIER 5
#define MAX_GENERATED_VERTS (MAX_CLIP_MULTIPLIER * MAX_CLIP_IN_VERTS)

#define THRESHOLD_PIXELS_SIMPLE_TEST (ScalarV(V_THREE)) //(3.0f)
#define STENCIL_WRITER_DEBUG (__BANK && !__64BIT) // disabled for x64 because pointers in StencilWriterUserData cause struct to be too large for tskGetUserData

#if __BANK
extern bool g_DebugRenderBoxExactHull;
#endif
struct rstTransformToScreenSpace
{
	Vec4V	m_cplane;
	Mat44V	m_comMtx;
	Vec3V	m_campos;
	Vec3V	m_scrExtents;

	__forceinline ScalarV_Out GetRescaleDepth() const { return m_scrExtents.GetW(); }

	rstTransformToScreenSpace() {}
#if !__SPU
	rstTransformToScreenSpace( const grcViewport* , bool isHiZ NV_SUPPORT_ONLY(,bool useStereoFrustum=false) );
#endif
};


#if RAST_NUM_TILES == 12
#define RAST_TILES_12_ONLY(...)  __VA_ARGS__
#else
#define RAST_TILES_12_ONLY(...)	
#endif


class rstFastTiler
{
	static const int MaxTris = MAX_TILER_TRIS;
public:
	static const int MaxIndexedVerts=256;
	static const int MaxSmall = MaxTris-1;
	rstFastTiler();

	void GenerateTileMem();

	void			Reset();
	ScanTileInfo	GetInfo();

	void SetViewport( const rstTransformToScreenSpace& screenView );

	bool IsFull( int numIndices ) const
	{
		return (m_numTris + numIndices/3 )>= MaxTris;
	}

//	void AddTri(  Vec3V_In v1, Vec3V_In v2, Vec3V_In v3);

	void AddTriList( sysTaskBuffer& sm, const Vec3V* __restrict tris, int amt );
	void AddTriList(  const Vec3V* __restrict tris, int amt );
	void AddIndexedTriList(
		const Vec3V*	occludeVerts, 
		const u8*	indices, 
		int  numIndices,
		int  numVerts,
		bool requiresClipping);

	void Assign( const PreTri2& p, u16 index, int issmall, Vec4V_In SetFirstBit);
#if __BANK
	void AddWidgets( bkBank& bk );
#endif
	int GetTileMemSize() const { return MaxNumTiles * MaxTris; }
	static int GetTileMemSizeBytes() 
	{
		int bsize = 0;
		bsize +=  sizeof(PreTri2)*MaxTris;
		bsize += sizeof(u16)* (MaxNumTiles *MaxTris);
		bsize += sizeof(int)*( RAST_NUM_TILES*2);
		return bsize;
	}

	static const int MaxNumTiles = RAST_NUM_TILES;
	int		m_cntTable[MaxNumTiles*2]  ALIGNED(128);

	RAST_INLINE void DuplicateLastIndices();

	void SetClippingMemory( Vec4V* clip0, Vec4V* clip1 );

private:

	rstTransformToScreenSpace	m_transform;

	void Setup();
	int AddTriListBatch( const Vec3V* __restrict intris, PreTri2* outTris, 
							int amt, int startIdx, int maxTris);
	int AddHPTTri(Vec4V_In hptV0, Vec4V_In hptV1,Vec4V_In hptV2, PreTri2* __restrict outTris, int idx, int startIdx);
	int AddPTTri(Vec3V_In ptV0, Vec3V_In ptV1,Vec3V_In ptV2, PreTri2* __restrict outTris, int idx, int startIdx );
//	int AddPTTriClipped(Vec3V_In ptV0, Vec3V_In ptV1,Vec3V_In ptV2, Vec3V_In zPlane, PreTri2* __restrict outTris, int idx, int startIdx );

	void TransformVerts( const Vec3V*	verts, Vec3V* __restrict postTransformVerts, int numVerts) const;

	int AddIndexedTriListWithClipping(
		 Vec3V*__restrict pVerts,
		const Vec3V*	postTransVerts,
		const u8*	Indices, 
		int  numIndices,
		int numVerts,
		PreTri2* outTris,
		int startIdx);

	int AddIndexedHTriListWithClipping(  const Vec4V* hverts, const u8* indices, int numIndices, int numVerts,
		PreTri2* outTris, int startIdx );
	
	RAST_INLINE void ApplyIndexChange( u16 index, Vec4V_In c1, Vec4V_In c2, Vec4V_In c3, int issmall );



	RAST_INLINE void FindTiles( const PreTri2& p, Vec4V_In SetFirstBit,
		Vec4V_InOut c1, Vec4V_InOut c2 RAST_TILES_12_ONLY( , Vec4V_InOut c3  )	) const;

public:
	int		m_numTris;
	ScalarV	m_sizeThreshold;
	int		m_smallThreshold;

	u16		m_tiles[MaxNumTiles][MaxTris] ALIGNED(128);

	ScalarV	m_TileWidth;
	ScalarV	m_TileHeight;
	Vec4V	m_MaxTileX;
	Vec4V	m_MinTileX;
	Vec4V	m_MaxTileY;
	Vec4V	m_MinTileY;

	PreTri2	m_tris[MaxTris] ALIGNED(128);

	Vec4V*	m_clipInBuffer;
	Vec4V*	m_clipOutBuffer;

	Vec3V	m_pixelOffset;

private:

#if __BANK
	int		m_enableSmall;
	int		m_numBackFaced;
	int		m_numSmall;
	Vec4V	m_enableSmallMask;
	int		m_maxNumTris;
public:
	struct StencilWriterUserData* m_stencilUserData;
#endif
};


__forceinline Vec4V_Out rstFullTransformHomo( Mat44V_In mtx, Vec3V_In vin)
{
	Vec4V x = Vec4V(vin.GetX());
	Vec4V y = Vec4V(vin.GetY());
	Vec4V z = Vec4V(vin.GetZ());

	Vec4V vout = AddScaled( mtx.GetCol3(), x, mtx.GetCol0() );
	vout = AddScaled( vout, y, mtx.GetCol1() );
	vout = AddScaled( vout, z, mtx.GetCol2() );
	return vout;
}

Vec3V_Out FullTransform2V( Mat44V_In mtx, Vec3V_In vin, ScalarV_In rescaleDepth );

enum { RST_MAX_AABB_TRIS = 12, RST_MAX_MODEL_TRIS = 20};  // Allow for half being backfaced
int rstGenerateAABBTris( const rstTransformToScreenSpace& transform, PreTri2	Tris[RST_MAX_AABB_TRIS], 
							ScalarV* minZ, Vec3V_In bmin, Vec3V_In bmax, ScalarV_In zbias );

void rstCreateScanTileTasks( rstFastTiler** tiler,
	Vec4V* zbuffer,
	Vec4V* hiZ,
	int    hiZStride,
	bool useSimple,
	bool useSmall,
	bool drawBlock,
	sysDependency dtask[RAST_NUM_TILES] ,
	const ShadowSquares& squares,
	ScanTileInfo stInfo[RAST_NUM_TILERS_ACTIVE],
	u32* nearZTiles,
	Vec4V* minMaxBounds ) ;

#ifndef FRAG_TSKLIB_ONLY
// Task management
// 
void rstCreateScanTileTasks( rstFastTiler** tiler,
	Vec4V* zbuffer,
	Vec4V* hiZ,
	int    hiZStride,
	bool useSimple,
	bool useSmall,
	bool drawBlock,
	DependantTaskList* dtask,
	const ShadowSquares& squares,
	ScanTileInfo stInfo[RAST_NUM_TILERS_ACTIVE] ,
	u32* nearZTiles,
	Vec4V* minMaxBounds);
#endif

struct TilerTaskInfo
{
	rstFastTiler	tiler;
	const Vec3V*	tris;
	int				amt;

	TilerTaskInfo( rstFastTiler* tile, const Vec3V* t, int a )
		: tiler(*tile), tris(t), amt(a){}
	TilerTaskInfo() {}
};

struct TriangleOcclusionUserData
{
	int numVerts;
	int numIndices;
	rage::u32* finalCntEA;
	float shiftAmt;
};

struct GenerateWaterTrisUserData
{
	Vec3V*	verts;
	u32*	numVertsEA;
	void*	tilerEA;
	u32		jobIndex;

	GenerateWaterTrisUserData(){}
	GenerateWaterTrisUserData( u32 _jobIndex, Vec3V* _verts, u32* _numVertsEA)
		: jobIndex(_jobIndex), verts(_verts), numVertsEA(_numVertsEA)
	{}
};

struct StencilWriterUserData
{	
	Vec3V*	verts;
	u32*	numVertsEA;
	void*	tileModelPacketsEa;

	u32*			waterOccluderCountEA;
	OccludeModel*	outputWaterOccluders;
	u8*				outputWaterClipped;

	float	precision;
	bool	writeZBuffer;
	bool	clearToAllVisible;
#if STENCIL_WRITER_DEBUG
	bool	debugEnableWaterStencil;
	bool	debugSimpleClipper;
	int		debugBoundsExpandX0;
	int		debugBoundsExpandY0;
	int		debugBoundsExpandX1;
	int		debugBoundsExpandY1;
	float	debugPixelOffsetX;
	float	debugPixelOffsetY;
#endif // STENCIL_WRITER_DEBUG

	StencilWriterUserData(){}
	StencilWriterUserData( 	Vec3V* _verts, u32* _numVertsEA, float _precision, void* _tileModelPacketsEa, bool writeZ, bool _clearToAllVisible )
		:  verts(_verts), numVertsEA(_numVertsEA), precision(_precision),
		tileModelPacketsEa(_tileModelPacketsEa), writeZBuffer(writeZ), clearToAllVisible( _clearToAllVisible )
#if STENCIL_WRITER_DEBUG
		, debugEnableWaterStencil(true)
		, debugSimpleClipper(false)
		, debugBoundsExpandX0(0)
		, debugBoundsExpandY0(0)
		, debugBoundsExpandX1(0)
		, debugBoundsExpandY1(0)
		, debugPixelOffsetX(0.0f)
		, debugPixelOffsetY(0.0f)
#endif // STENCIL_WRITER_DEBUG
	{}
};

int ClipTriangles( const Vec3V* verts, const u8* indices, int amtVerts, Vec3V* out, Vec4V_In clipPlane );

//void ClipViewportGeneratePlanes( Vec4V planes[4], const grcViewport* vp, float guardband);
//int ClipToViewport( Vec4V planes[4], Vec3V* tris, Vec3V* outTris, int amtVerts );

ScalarV_Out rstTestModelExact( Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth, const Vec4V* occlusionBuffer, Vec3V* verts, const u8* indices, int numVerts,
	int numIndices, ScalarV_In  /*smallThreshold*/ );
ScalarV_Out rstTestAABBExact( Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth,  const Vec4V* occlusionBuffer,  Vec3V_In bmin, Vec3V_In bmax, ScalarV_In zBias );

ScalarV_Out rstTestAABBExactEdgeList( bool isOrtho, Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth,   
										const Vec4V* occlusionBuffer,Vec3V_In eye, Vec3V_In bmin, Vec3V_In bmax, ScalarV_In zbias , Vec4V_In minMaxBounds, ScalarV_In minZBuffer
										DEV_ONLY(, u32* trivialAcceptActiveFrustum, u32* trivialAcceptMinZ, u32* trivialAcceptVisiblePixel)
#if __BANK
									 , ScalarV_In simpleTestThresholdPixels = THRESHOLD_PIXELS_SIMPLE_TEST
									 , bool useTrivialAcceptTest = true, bool useTrivialAcceptVisiblePixelTest = true, bool useMinZClipping = true
#endif
									 );
ScalarV_Out rstTestOOBBExactEdgeList( Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth,   
									 const Vec4V* occlusionBuffer,Vec3V_In eye, Mat34V_In OOBBToWorldMtx, Vec3V_In bminI, Vec3V_In bmaxI, ScalarV_In zbias, Vec4V_In minMaxBounds, ScalarV_In minZBuffer 
									 DEV_ONLY(, u32* trivialAcceptActiveFrustum, u32* trivialAcceptMinZ, u32* trivialAcceptVisiblePixel)
#if __BANK
									 , ScalarV_In simpleTestThresholdPixels = THRESHOLD_PIXELS_SIMPLE_TEST
									 , bool useTrivialAcceptTest = true, bool useTrivialAcceptVisiblePixelTest = true, bool useMinZClipping = true
#endif
									 );

ScalarV_Out rstTestEdgeListCoverage(  Mat44V_In worldToScreenMtx, ScalarV_In rescaleDepth, bool isOrtho, 
									const Vec4V* occlusionBuffer, Vec3V* verts, const u8* indices, ScalarV_In zbias,int numVerts, Vec4V_In minMaxBounds, ScalarV_In minZBuffer
									DEV_ONLY(, u32* trivialAcceptActiveFrustum, u32* trivialAcceptMinZ, u32* trivialAcceptVisiblePixel)
									BANK_ONLY(, ScalarV_In simpleTestThresholdPixels = THRESHOLD_PIXELS_SIMPLE_TEST, bool useTrivialAcceptTest = true, bool useTrivialAcceptVisiblePixelTest = true, bool useMinZClipping = true)
									);

__forceinline bool rstTestAABBTrivialAccept(Vec3V_In scMin, Vec3V_In scMax, Vec4V_In minMaxBounds, ScalarV_In minZBuffer, ScalarV_In minZ
							  DEV_ONLY(, u32* trivialAcceptActiveFrustum, u32* trivialAcceptMinZ)
							  )
{

	//Displayf("min Bounds = %f %f , Max Bounds = %f %f", minMaxBounds.GetXf(), minMaxBounds.GetYf(), minMaxBounds.GetZf(), minMaxBounds.GetWf());
	static const Vec2V maxClamp((float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT);
	static const Vec2V minClamp(V_ZERO);
	const Vec2V clampedSCMin = Clamp(scMin.GetXY(), minClamp, maxClamp);
	const Vec2V clampedSCMax = Clamp(scMax.GetXY(), minClamp, maxClamp);

	VecBoolV inrange =  ((clampedSCMin < minMaxBounds.GetXY()) |  (clampedSCMax > minMaxBounds.GetZW())) & VecBoolV(V_T_T_F_F);
	if(!IsFalseAll(inrange))
	{
#if __DEV
		sysInterlockedIncrement(trivialAcceptActiveFrustum);
#endif
		return true;

	}

	if( IsLessThanAll(minZ,minZBuffer))
	{
#if __DEV
		sysInterlockedIncrement(trivialAcceptMinZ);
#endif
		return true;
	}

	return false;
}
int rstGetBoxHull(  Vec3V_In eye, Vec3V_In bminI, Vec3V_In bmaxI, u8*& hullIndices,Vec3V aabbVerts[8]);
bool rstTransformHullAndGetMinZ (Mat44V_In worldToScreenMtx, Vec2V_In screenRes, Vec3V* aabbVerts, const u8* hullIndices, int hullVertCount, ScalarV_In rescaleDepth, Vec3V_InOut scMinV, Vec3V_InOut scMaxV, ScalarV_InOut minZ, Vec3V* ssHullVerts, int& ssHullVertCount);

// see 'GetSignMask' in vectorutil.h
inline int MoveSignMask( Vec3V_In v )
{
#if __WIN32PC || RSG_DURANGO || RSG_ORBIS
	return 7 & _mm_movemask_ps( v.GetIntrin128());
#elif 0 && __SPU // TODO -- test this
	const qword perm = (qword)(vector int){0x80808080, 0x08090a0b, 0x04050607, 0x00010203}; // [0,Z,Y,X]
	const qword vin = (qword)v.GetIntrin128();
	return spu_extract((vector int)si_gb(si_rotmi(si_shufb(vin, vin, perm), -31)), 0);
#else
	const Vec3V mask = Vec3V( Vec3VConstant< 1<<0, 1<<1, 1 <<2>());
	Vec3V vsigns = Vec3V( v < Vec3V(V_ZERO));
	Vec3V vweights = mask & vsigns;
	vweights = vweights | vweights.Get<Vec::Y, Vec::Z, Vec::Z>();
	vweights = vweights | vweights.Get<Vec::Z, Vec::Z, Vec::X>();

	return  vweights.GetXi();
#endif
}

} // namespace rage

#ifndef FRAG_TSKLIB_ONLY

DECLARE_TASK_INTERFACE(stencilwriter);
// PURPOSE: task which creates a single bit stencil image marked out with the given geometry.
void stencilwriter( rage::sysTaskParameters& p );

DECLARE_TASK_INTERFACE(generatewatertris);
// PURPOSE: task which generates tris for the water quads
void generatewatertris( rage::sysTaskParameters& p );

// PURPOSE: task which tests hi-zbuffer agaisnt water hi-z buffer to see if it's visible.
DECLARE_TASK_INTERFACE(stencilcompareandgenerate);
void stencilcompareandgenerate( rage::sysTaskParameters& p );

// PURPOSE: task which tests extruded triangles for visibility ,useful for polygons with geometry
// generated on them.
void triangleocclusiontester( rage::sysTaskParameters& p );

#endif

#endif // SOFTRASTERIZER_TILER_H 
