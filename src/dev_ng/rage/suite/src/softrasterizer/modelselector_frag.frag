#ifndef _MODELSELECTOR_FRAG_H
#define _MODELSELECTOR_FRAG_H

#define FRAG_TSKLIB_ONLY
#if !__WIN32 && !RSG_ORBIS
#include "boxoccluder.h"
#include "boxoccluder.cpp"
#include "occludeModel.cpp"
#endif

SPUFRAG_IMPL(bool, modelselector_frag, sysDependency& task)
{
#if __RASTERSTATS
	PF_FUNC(SelectModelOccluders);
#endif	

	using namespace rage;
	int numInBuckets;
	const COccludeModelBucket* pBuckets = tskGetInput<COccludeModelBucket>(task, 1, numInBuckets);
	sysTaskBuffer Scratch = tskGetScratch( task);
	ScratchMemStackPoint cm(Scratch);

	const int MaxResultOccluders = 256;
	u8*  isClipped = taskAllocBuffer<u8,16>(Scratch, MaxResultOccluders); 
	OccludeModel* resmodels =  taskAllocBuffer<OccludeModel,16>(Scratch,MaxResultOccluders);
	const rstTransformToScreenSpace* clipAndTransform = tskGetInput<rstTransformToScreenSpace>( task,2);
	ModelSelectorUserData userData = tskGetUserData<ModelSelectorUserData>(task);

#if __SPU
	OccludeModel* pElements = taskAllocBuffer<OccludeModel, 16>(Scratch, MAX_NUM_OCCLUDE_MODELS); 	
#endif

	u32* parea=taskAllocBuffer<u32,16>(Scratch, MaxResultOccluders); 
	int amt= rstSelectModelOccluders( *clipAndTransform, pBuckets, resmodels, isClipped, numInBuckets, MaxResultOccluders, ScalarVFromF32(  userData.threshold) , parea SPU_ONLY(, pElements));

	std::sort( parea, parea+amt,std::greater<int>());


	// fire off child tasks.
	int NumTilers = userData.NumTilers;
	int stepSize = (amt+NumTilers)/ NumTilers;

	int c = 0;
	BEGIN_ALIGNED(16) TileDataPacket<OccludeModel>	packets[RAST_NUM_TILERS_ACTIVE] ALIGNED(16);

	u8*  isClippedFinal = taskAllocBuffer<u8,16>(Scratch, MaxResultOccluders); 
	OccludeModel* resmodelsFinal =  taskAllocBuffer<OccludeModel,16>(Scratch,MaxResultOccluders);

	// round up to the number of tilers
	// so we can place the sorted ones easily
	int roundedAmt = stepSize*NumTilers;
	for (int i = 0; i < NumTilers; i++, c+= stepSize)
	{	
		int end =  Min( roundedAmt, c +stepSize);
		int size=  Max(end-c,0);

		packets[i].clippedFlags = userData.outputClipped + c;
		packets[i].occluders = userData.outputOccluders+ c;
		packets[i].amt = size;	
		// copy in sorted occluders for each tiler
		for (int j=0;j<size;j++){
			int idx= i +(j*NumTilers);
			if ( idx < amt){
				int sidx = parea[idx]&0xFFFF;  // bottom 16 bits gives index
				Assert( sidx <amt);
				isClippedFinal[c+j]=isClipped[sidx];
				resmodelsFinal[c+j]=resmodels[sidx];
			}
			else
			{
				isClippedFinal[c+j]=0;
				resmodelsFinal[c+j]=OccludeModel::Empty();
			}
		}
	}
	amt = roundedAmt;
	// write out explicitly to output
	taskWriteBuffer( "clipFlags",isClippedFinal, userData.outputClipped, TSK_ROUNDUP16(amt), TAG_WRITETILER_0 );
	taskWriteBuffer( "resOccluders",resmodelsFinal, userData.outputOccluders, amt, TAG_WRITETILER_0 );

	Assert( c >= amt);
	taskWriteBuffer( "TilerPackets",packets, userData.packets, NumTilers, TAG_WRITETILER_0 );
	taskWait( TAG_WRITETILER_0);
	return true;
}


#endif //_MODELSELECTOR_FRAG_H