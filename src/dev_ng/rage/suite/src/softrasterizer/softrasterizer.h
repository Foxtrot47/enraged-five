// 
// softrasterizer/softrasterizer.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SOFTRASTERIZER_SOFTRASTERIZER_H 
#define SOFTRASTERIZER_SOFTRASTERIZER_H 

#ifndef FRAG_TSKLIB_ONLY
#include "system/taskheader.h"
#endif
#include "softrasterizer/scansetup.h"
#include "softrasterizer/tiler.h"
#include "softrasterizer/tsklib.h"
#include "data/base.h"

#include "softrasterizer/boxoccluder.h"
#include "softrasterizer/occludemodel.h"
#include "softrasterizer/tiler.h"

#if __BANK
#include "vector\color32.h"
#endif
namespace rage {

class BoxOccluder;
class OccludeModel;
class rstFastTiler;
class grcTexture;
struct rstTransformToScreenSpace;

struct rstTileList
{
	atRangeArray<rstFastTiler*,RAST_NUM_TILERS_ACTIVE>					tiles;
	atRangeArray<rstFastTiler,RAST_NUM_TILERS_ACTIVE>					tileMem;
	rstFastTiler*														m_waterTiler;
	rstFastTiler														m_waterTilerMem;
	rstTileList(){
		for (int i = 0; i < RAST_NUM_TILERS_ACTIVE; i++)
		{
			tiles[i] = &tileMem[i];
			tiles[i]->GenerateTileMem();
		}
		m_waterTiler = &m_waterTilerMem;
		m_waterTiler->GenerateTileMem();
	}
	void Reset(){
		for ( int i =0;i < RAST_NUM_TILERS_ACTIVE; i++)
			tiles[i]->Reset();
	}
#if __BANK
	void AddWidgets(bkBank& bank ) {
		for (int i = 0; i < RAST_NUM_TILERS_ACTIVE; i++)
			tiles[i]->AddWidgets(bank);
	}
#endif
};


// PURPOSE : simple per frame allocator that deals with alignment and 
// allocating classes and arrays of classes.
//
template<class Allocator>
class ScratchPadAllocator
{
	u8* m_dataBlock;
	u8* m_cptr;
	const u8* m_end;

	BANK_ONLY(	size_t m_maxAddress );
	BANK_ONLY( bool m_dumpAllocs );

	Allocator	m_alloc;

	void ApplyAlignment( size_t originalAlignment){		
		size_t alignment = Max((size_t)16, (size_t)originalAlignment); // align to struct size		
		m_cptr = (u8*)(((size_t)m_cptr+(alignment-1))&~(alignment-1));
		Assert( ((size_t)m_cptr&15)==0);		
	}
	void Increment(size_t s ){
		m_cptr += s;
		BANK_ONLY(m_maxAddress=Max( (size_t)m_cptr, (size_t)m_maxAddress) );
	}

public:
	ScratchPadAllocator() { m_dataBlock=NULL; m_end=NULL;m_cptr=NULL;}

	ScratchPadAllocator( bool BANK_ONLY(dumpAllocations)) 
	{ 
		m_dataBlock = m_cptr = m_alloc.Alloc(); 
		int size = m_alloc.GetSize();
		m_end = m_dataBlock+size;
		BANK_ONLY(m_maxAddress = (size_t)m_cptr);				
		BANK_ONLY(m_dumpAllocs = dumpAllocations);
	}
	~ScratchPadAllocator()
	{
		m_alloc.Free(m_dataBlock);
	}
	void Clear(){
		m_alloc.setMaxUsed((int)(m_cptr-m_dataBlock));
		m_cptr=m_dataBlock;
	}
	template<class T>
	T* Alloc(){
		ApplyAlignment((size_t)__alignof(T));
		T* result = rage_placement_new(m_cptr) T();
		Increment(sizeof(T));
		BANK_ONLY( if ( m_dumpAllocs) Displayf(" Alloc Size %" SIZETFMT "d %s ", sizeof(T), typeid(T).name()) );
		return AssertVerify(m_cptr<= m_end ) ?  result : NULL;
	}
	template<class T>
	T* AllocArray(int amt = 1){
		ApplyAlignment((size_t)__alignof(T));
		T* result = rage_placement_new (m_cptr) T[amt];
		Increment(sizeof(T) * amt);

		BANK_ONLY(  if ( m_dumpAllocs) Displayf(" Alloc Size %" SIZETFMT "d Amt %d %s ", sizeof(T)* amt, amt, typeid(T).name()) );
		return AssertVerify( m_cptr<= m_end ) ?  result : NULL ;
	}

	void End(){
#if __BANK
		if ( m_dumpAllocs) 
		{
			Displayf("Max Size %" SIZETFMT "d Current Size %" SIZETFMT "d ", m_maxAddress-(size_t)m_dataBlock, m_end-m_dataBlock);
			if ( (size_t)(m_end-m_dataBlock) < (m_maxAddress-(size_t)m_dataBlock))
				Errorf("Overflow in temp allocator");
		}
#endif
		Clear();
	}

	template<class T>
	bool In( const T* p ) const  { return ((u8*)p>=m_dataBlock && (u8*)p<=m_end ); }


	// Interface to allow for allocations that may have to become static for debug reasons
	// ptr should be initialized to null before use
	template<class T>
	void AllocScratchPad( T*& ptr, bool BANK_ONLY(requireStaticAllocation))
	{
#if __BANK
		if(ptr ==0){  // need to create static version
			if ( requireStaticAllocation ){
				ptr = rage_new T();
				return;
			}
		}
		else
		{
			if ( !requireStaticAllocation)
			{
				Assert(!In(ptr) );
				delete ptr;
			}
			else
				return;
		}
#endif
		ptr = Alloc<T>();
	}
	template<class T>
	void FreeScratchPad(T*& ptr )
	{
#if __BANK
		Assert(ptr != 0);
		if (!In(ptr) )
			return;	
#endif
		ptr=0;
	}
	// Interface to allow for allocations that may have to become static for debug reasons
	// ptr should be initialized to null before use
	template<class T>
	void AllocScratchPadArray( T*& ptr, int amt, bool BANK_ONLY(requireStaticAllocation))
	{
#if __BANK
		if(ptr ==0){  // need to create static version
			if ( requireStaticAllocation ){
				ptr = rage_new T[amt]();
				return;
			}
		}
		else
		{
			if ( !requireStaticAllocation)
			{
				Assert(!In(ptr) );
				delete [] ptr;
			}
			else
				return;
		}
#endif
		ptr = AllocArray<T>(amt);
	}
	template<class T>
	void FreeScratchPadArray(T*& ptr )
	{
		FreeScratchPad(ptr);
	}
	//-------------------------
};

struct rstSplatMat44V 
{
	Vec4V _m00;
	Vec4V _m01;
	Vec4V _m02;
	Vec4V _m03;
	Vec4V _m10;
	Vec4V _m11;
	Vec4V _m12;
	Vec4V _m13;
	Vec4V _m20;
	Vec4V _m21;
	Vec4V _m22;
	Vec4V _m23;
	Vec4V _m30;
	Vec4V _m31;
	Vec4V _m32;
	Vec4V _m33;
};

inline void rstCreateSplatMat44V( Mat44V_In mtx, rstSplatMat44V& splatMtx )
{
	splatMtx._m00 = Vec4V(mtx.GetCol0().GetX());
	splatMtx._m01 = Vec4V(mtx.GetCol0().GetY());
	splatMtx._m02 = Vec4V(mtx.GetCol0().GetZ());
	splatMtx._m03 = Vec4V(mtx.GetCol0().GetW());

	splatMtx._m10 = Vec4V(mtx.GetCol1().GetX());
	splatMtx._m11 = Vec4V(mtx.GetCol1().GetY());
	splatMtx._m12 = Vec4V(mtx.GetCol1().GetZ());
	splatMtx._m13 = Vec4V(mtx.GetCol1().GetW());

	splatMtx._m20 = Vec4V(mtx.GetCol2().GetX());
	splatMtx._m21 = Vec4V(mtx.GetCol2().GetY());
	splatMtx._m22 = Vec4V(mtx.GetCol2().GetZ());
	splatMtx._m23 = Vec4V(mtx.GetCol2().GetW());

	splatMtx._m30 = Vec4V(mtx.GetCol3().GetX());
	splatMtx._m31 = Vec4V(mtx.GetCol3().GetY());
	splatMtx._m32 = Vec4V(mtx.GetCol3().GetZ());
	splatMtx._m33 = Vec4V(mtx.GetCol3().GetW());
}


__forceinline ScalarV_Out rstCreateCurrentSplatMat44V( const rstTransformToScreenSpace& clipAndTransform, rstSplatMat44V& splatMtx, Vec3V_InOut scrExtents )
{
	scrExtents = clipAndTransform.m_scrExtents;

	rstCreateSplatMat44V(clipAndTransform.m_comMtx, splatMtx);
	return clipAndTransform.GetRescaleDepth();
}

ScalarV_Out rstCreateCurrentSplatMat44V( const grcViewport* vp, rstSplatMat44V& splatMtx, Vec3V_InOut scrExtents );


__forceinline void FullTransform2V4( const rstSplatMat44V& mtx, Vec4V* __restrict points, Vec4V_In x, Vec4V_In y, Vec4V_In z)
{
	Vec4V c0 = AddScaled( mtx._m30, mtx._m20, z );
	Vec4V c1 = AddScaled( mtx._m31, mtx._m21, z );
	Vec4V c2 = AddScaled( mtx._m32, mtx._m22, z );
	Vec4V c3 = AddScaled( mtx._m33, mtx._m23, z );

	c0 = AddScaled(  c0, mtx._m10, y );
	c1 = AddScaled(  c1, mtx._m11, y );
	c2 = AddScaled(  c2, mtx._m12, y );
	c3 = AddScaled(  c3, mtx._m13, y );

	points[0] = AddScaled(  c0, mtx._m00, x );
	points[1] = AddScaled(  c1, mtx._m01, x );
	points[2] = AddScaled(  c2, mtx._m02, x );
	points[3] = AddScaled(  c3, mtx._m03, x );
}

inline bool rstCalculateScreenSpaceBoundIsVis(	const rstSplatMat44V& splatMtx, ScalarV_In rescaleDepth,
										 Vec3V_In bmin, Vec3V_In bmax,
										 Vec3V_InOut scbmin, Vec3V_InOut scbmax , Vec3V_In exmax )
{

	// get aabb points (SoA)
	Vec4V minmax_x = GetFromTwo<Vec::X1, Vec::X2, Vec::X1, Vec::X2>(bmin, bmax); // x X x X
	Vec4V minmax_y = GetFromTwo<Vec::Y1, Vec::Y1, Vec::Y2, Vec::Y2>(bmin, bmax); // y y Y Y
	Vec4V minmax_z_0 = Vec4V(bmin.GetZ()); // z z z z
	Vec4V minmax_z_1 = Vec4V(bmax.GetZ()); // Z Z Z Z

	// transform points to world space
	Vec4V points0[4];
	Vec4V points1[4];

	FullTransform2V4(splatMtx, points0, minmax_x, minmax_y, minmax_z_0 );
	FullTransform2V4(splatMtx, points1, minmax_x, minmax_y, minmax_z_1 );

	// apply homogenous clipping
// now test points are visible

	VecBoolV clipMaskMinX = (points0[0] <= Vec4V(V_ZERO)) &( points1[0] <= Vec4V(V_ZERO));
	VecBoolV clipMaskMinY = (points0[1] <= Vec4V(V_ZERO)) &( points1[1] <= Vec4V(V_ZERO));
	VecBoolV clipMaskMinZ = (points0[2] <= Vec4V(V_ZERO)) &( points1[2] <= Vec4V(V_ZERO));

	VecBoolV clipMaskMaxX = (points0[0] > (exmax.GetX()*points0[3])) & (points1[0] > (exmax.GetX()*points1[3]));
	VecBoolV clipMaskMaxY = (points0[1] > (exmax.GetY()*points0[3])) & (points1[1] > (exmax.GetY()*points1[3]));


	if (IsTrueAll(clipMaskMinX)|| IsTrueAll(clipMaskMinY) || IsTrueAll(clipMaskMinZ)
		|| IsTrueAll(clipMaskMaxY)  || IsTrueAll(clipMaskMaxX)){
			scbmin=scbmax=Vec3V(V_ZERO);
			return false;
	}
	
	// bring into perspective space with divide by w

	Vec4V invW0 = Invert( points0[3]);
	Vec4V invW1 = Invert( points1[3]);

	Vec4V z0 = points0[2] * rescaleDepth;
	Vec4V z1 = points1[2] * rescaleDepth;
	Vec4V minz = Min( z0, z1);
	Vec4V maxz = Max( z0, z1);

	// negative w component so it's hard to get corret bounds.
	if ( !IsTrueAll( points0[3]>Vec4V(V_ZERO)) || !IsTrueAll( points1[3]>Vec4V(V_ZERO))){

		ScalarV minv = Min( Min( minz.GetX(),  minz.GetY()), Min(  minz.GetZ(),  minz.GetW()) );
		ScalarV maxv = Max( Max( maxz.GetX(), maxz.GetY()), Max( maxz.GetZ(), maxz.GetW()) );

		scbmin=Vec3V(V_ZERO);
		scbmin.SetZ(minv);
		scbmax=exmax;
		scbmax.SetZ(maxv);
		return true;
	}
	Vec4V x0 = points0[0] * invW0;
	Vec4V y0 = points0[1] * invW0;
	Vec4V x1 = points1[0] * invW1;
	Vec4V y1 = points1[1] * invW1;

	// now get minimum / maximum of 8 points
	Vec4V minx = Min( x0, x1);
	Vec4V miny = Min( y0, y1);

	Vec4V maxx = Max( x0, x1);
	Vec4V maxy = Max( y0, y1);


	Vec3V minv0,minv1,minv2,minv3;
	Transpose4x3to3x4( minv0,minv1,minv2,minv3, minx,miny,minz );
	Vec3V minv = Min( Min( minv0, minv1), Min( minv2, minv3) );

	Vec3V maxv0,maxv1,maxv2,maxv3;
	Transpose4x3to3x4( maxv0,maxv1,maxv2,maxv3, maxx,maxy,maxz );
	Vec3V maxv = Max( Max( maxv0, maxv1), Max( maxv2, maxv3) );

	scbmin = minv;
	scbmax = maxv;
	return true;
}


//http://zeuxcg.blogspot.com/2009/03/view-frustum-culling-optimization.html
inline void rstCalculateScreenSpaceBound(	const rstSplatMat44V& splatMtx, ScalarV_In rescaleDepth,
	Vec3V_In bmin, Vec3V_In bmax,
	Vec3V_InOut scbmin, Vec3V_InOut scbmax )
{

	// get aabb points (SoA)
	Vec4V minmax_x = GetFromTwo<Vec::X1, Vec::X2, Vec::X1, Vec::X2>(bmin, bmax); // x X x X
	Vec4V minmax_y = GetFromTwo<Vec::Y1, Vec::Y1, Vec::Y2, Vec::Y2>(bmin, bmax); // y y Y Y
	Vec4V minmax_z_0 = Vec4V(bmin.GetZ()); // z z z z
	Vec4V minmax_z_1 = Vec4V(bmax.GetZ()); // Z Z Z Z

	// transform points to world space
	Vec4V points0[4];
	Vec4V points1[4];

	FullTransform2V4(splatMtx, points0, minmax_x, minmax_y, minmax_z_0 );
	FullTransform2V4(splatMtx, points1, minmax_x, minmax_y, minmax_z_1 );


	// bring into perspective space with divide by w
	Vec4V invW0 = Abs(Invert( points0[3]));
	Vec4V invW1 = Abs(Invert( points1[3]));

	Vec4V z0 = points0[2] * rescaleDepth;
	Vec4V z1 = points1[2] * rescaleDepth;
	Vec4V minz = Min( z0, z1);
	Vec4V maxz = Max( z0, z1);

	Vec4V x0 = points0[0] * invW0;
	Vec4V y0 = points0[1] * invW0;
	Vec4V x1 = points1[0] * invW1;
	Vec4V y1 = points1[1] * invW1;

	// now get minimum / maximum of 8 points
	Vec4V minx = Min( x0, x1);
	Vec4V miny = Min( y0, y1);

	Vec4V maxx = Max( x0, x1);
	Vec4V maxy = Max( y0, y1);


	Vec3V minv0,minv1,minv2,minv3;
	Transpose4x3to3x4( minv0,minv1,minv2,minv3, minx,miny,minz );
	Vec3V minv = Min( Min( minv0, minv1), Min( minv2, minv3) );

	Vec3V maxv0,maxv1,maxv2,maxv3;
	Transpose4x3to3x4( maxv0,maxv1,maxv2,maxv3, maxx,maxy,maxz );
	Vec3V maxv = Max( Max( maxv0, maxv1), Max( maxv2, maxv3) );

	scbmin = minv;
	scbmax = maxv;
}

__forceinline void rstCalculateScreenSpaceConvexVolume(	const rstSplatMat44V& splatMtx, ScalarV_In rescaleDepth,
	const Vec3V* __restrict oobb,
	Vec3V_InOut scbmin, Vec3V_InOut scbmax,
	Vec3V_InOut worldMin, Vec3V_InOut worldMax)
{

	Vec4V px0,py0,pz0;
	Vec4V px1,py1,pz1;
	Vec4V px2,py2,pz2;

	Transpose3x4to4x3( px0, py0, pz0, oobb[0], oobb[1], oobb[2], oobb[3] );
	Transpose3x4to4x3( px1, py1, pz1, oobb[4], oobb[5], oobb[6], oobb[7] );
	Transpose3x4to4x3( px2, py2, pz2, oobb[8], oobb[9], oobb[10], oobb[11] );

	// transform points to world space
	Vec4V points0[4];
	Vec4V points1[4];
	Vec4V points2[4];

	worldMin=oobb[0];
	worldMax=oobb[0];
	for (int i =1;i<12;i++)
	{
		worldMin = Min(worldMin,oobb[i]);
		worldMax = Max(worldMax,oobb[i]);
	}

	FullTransform2V4(splatMtx, points0, px0, py0, pz0 );
	FullTransform2V4(splatMtx, points1, px1, py1, pz1 );
	FullTransform2V4(splatMtx, points2, px2, py2, pz2 );

	// bring into perspective space with divide by w
	Vec4V invW0 = Invert( points0[3]);
	Vec4V invW1 = Invert( points1[3]);
	Vec4V invW2 = Invert( points2[3]);

	Vec4V z0 = points0[2] * rescaleDepth;
	Vec4V z1 = points1[2] * rescaleDepth;
	Vec4V z2 = points2[2] * rescaleDepth;
	Vec4V minz = Min(Min( z0, z1),z2);
	Vec4V maxz = Max(Max( z0, z1),z2);

	Vec4V x0 = points0[0] * invW0;
	Vec4V y0 = points0[1] * invW0;
	Vec4V x1 = points1[0] * invW1;
	Vec4V y1 = points1[1] * invW1;
	Vec4V x2 = points2[0] * invW2;
	Vec4V y2 = points2[1] * invW2;

	// now get minimum / maximum of 8 points
	Vec4V minx = Min(Min( x0, x1),x2);
	Vec4V miny = Min(Min( y0, y1),y2);

	Vec4V maxx = Max(Max( x0, x1),x2);
	Vec4V maxy = Max(Max( y0, y1),y2);


	Vec3V minv0,minv1,minv2,minv3;
	Transpose4x3to3x4( minv0,minv1,minv2,minv3, minx,miny,minz );
	Vec3V minv = Min( Min( minv0, minv1), Min( minv2, minv3) );

	Vec3V maxv0,maxv1,maxv2,maxv3;
	Transpose4x3to3x4( maxv0,maxv1,maxv2,maxv3, maxx,maxy,maxz );
	Vec3V maxv = Max( Max( maxv0, maxv1), Max( maxv2, maxv3) );

	scbmin = minv;
	scbmax = maxv;
}

__forceinline void rstCalculateScreenSpaceOOBBBound(	const rstSplatMat44V& splatMtx, ScalarV_In rescaleDepth,
	const Vec3V* __restrict oobb,
	Vec3V_InOut scbmin, Vec3V_InOut scbmax,
	Vec3V_InOut worldMin, Vec3V_InOut worldMax)
{

	Vec4V px0,py0,pz0;
	Vec4V px1,py1,pz1;

	Transpose3x4to4x3( px0, py0, pz0, oobb[0], oobb[1], oobb[2], oobb[3] );
	Transpose3x4to4x3( px1, py1, pz1, oobb[4], oobb[5], oobb[6], oobb[7] );

	// transform points to world space
	Vec4V points0[4];
	Vec4V points1[4];

	// less dependences this way and should be able to schedule with the rest
	worldMin = Min( Min( Min( oobb[0], oobb[1]), Min(oobb[2],oobb[3]) ), Min( Min( oobb[4], oobb[5]), Min( oobb[6], oobb[7])) );
	worldMax = Max( Max( Max( oobb[0], oobb[1]), Max(oobb[2],oobb[3]) ), Max( Max( oobb[4], oobb[5]), Max( oobb[6], oobb[7])) );

	FullTransform2V4(splatMtx, points0, px0, py0, pz0 );
	FullTransform2V4(splatMtx, points1, px1, py1, pz1 );

	// bring into perspective space with divide by w
	Vec4V invW0 = Invert( points0[3]);
	Vec4V invW1 = Invert( points1[3]);

	Vec4V z0 = points0[2] * rescaleDepth;
	Vec4V z1 = points1[2] * rescaleDepth;
	Vec4V minz = Min( z0, z1);
	Vec4V maxz = Max( z0, z1);

	Vec4V x0 = points0[0] * invW0;
	Vec4V y0 = points0[1] * invW0;
	Vec4V x1 = points1[0] * invW1;
	Vec4V y1 = points1[1] * invW1;

	// now get minimum / maximum of 8 points
	Vec4V minx = Min( x0, x1);
	Vec4V miny = Min( y0, y1);

	Vec4V maxx = Max( x0, x1);
	Vec4V maxy = Max( y0, y1);


	Vec3V minv0,minv1,minv2,minv3;
	Transpose4x3to3x4( minv0,minv1,minv2,minv3, minx,miny,minz );
	Vec3V minv = Min( Min( minv0, minv1), Min( minv2, minv3) );

	Vec3V maxv0,maxv1,maxv2,maxv3;
	Transpose4x3to3x4( maxv0,maxv1,maxv2,maxv3, maxx,maxy,maxz );
	Vec3V maxv = Max( Max( maxv0, maxv1), Max( maxv2, maxv3) );

	scbmin = minv;
	scbmax = maxv;
}
#ifndef FRAG_TSKLIB_ONLY
	
// PURPOSE:
// Controls the rasterizer to all for rendering triangles on the CPU
// DESCRIPTION:
// The rage software rasterizer consists of three main components.
// Tiling  ( softrasterizer/tiler.cpp)''' 
//	Transforming vertices, clipping with the near plane and  building edge equations and triangles 
//  and selecting which tiles a triangle overlaps. If triangle is small, sorts it into small list
//  Scan conversion (softrasterizer/scan.cpp)''' 
//	For each tile, clears zbuffer,  scan converts all triangles for that tile. Finds
//  block overlaps for large triangles and renders each block. If triangle is small just renders. 
//  After all triangles are rendered, converts the zbuffer to a hi-zbuffer.
//  Queries (softrasterizer/scan.h)
//	These are a series of functions that query the results of the hi-z buffer to check
//  if objects are occluded, or other game specific tasks.
//	Softrasterizer.cpp is a layer above the tiling and scan conversion and adds debug functionality.
class SoftRasterizer : public datBase
{		
public:		
	SoftRasterizer(rstTileList* tilers, bool createWaterOccluders=false);
	virtual ~SoftRasterizer();

	Vec4V* GetZBuffer() { return m_ZBuffer; }
	Vec4V* GetHiZBuffer() { return m_HiZBuffer; }
	Vec4V* GetHiStencilBuffer() { return m_StencilBuffer; }

	void Reset();
	void RasterizeTris( const atArray<Vec3V,128,u32>& sceneTris );

	void RasterizeModel(
		const Vec3V*	verts, 
		const u8*	indices, 
		int  numIndices,
		int  numVerts,
		bool requiresClipping );
		
	void RasterizeStencilModel(
		const grcViewport& view,
		Vec3V*	verts, 
		u32*  numVerts,
		bool requiresClipping,
		bool clearAllToVisible = false
#if STENCIL_WRITER_DEBUG
		, bool debugEnableWaterStencil = false
		, bool debugSimpleClipper = false
		, int debugBoundsExpandX0 = 0
		, int debugBoundsExpandY0 = 0
		, int debugBoundsExpandX1 = 0
		, int debugBoundsExpandY1 = 0
		, float debugPixelOffsetX = 0.0f
		, float debugPixelOffsetY = 0.0f
#endif // STENCIL_WRITER_DEBUG
		);
	void GenerateCompareStencilTask();
	void Wait();
	void WaitForScanTask();

	DependantTaskList* PrepareScanTasks();
	DependantTaskList* PrepareSubmitBoxOccluders();

	void PrepareScanTasks( sysDependency scanTasks[RAST_NUM_TILES]);
	void PrepareSubmitTilersDependency();
	void PrepareSubmitGenerateWaterTris(Vec3V* verts, u32* numVerts);

	float GetCurrentRastTransform( Mat44V_InOut mtx );
	rstTransformToScreenSpace GetCurrentRastTransform();
#if __BANK
	void CopyToTexture(bool popZ=false);
	void DisplayStats() const ;
	bool IsDebugDrawEnabled() const { return m_debugDraw; }
	void DebugDraw(bool popZ=false);
	void DebugDrawActiveFrustum();
	void AddWidgets( bkBank& bank);
	void EnableDebugDraw(bool debugDraw, bool fullScreen) {m_debugDraw = debugDraw; m_depthFullscreen = fullScreen;}
	void EnableDebugDraw(bool debugDraw, bool fullScreen, bool showStencil) {m_debugDraw = debugDraw; m_depthFullscreen = fullScreen; m_showStencil = showStencil;}
	void GetDebugExtents( float& sX, float& sY, float& eX, float& eY );
	void DrawWaterOccluderMeshes( bool  depthOnly);
	void DumpWaterOccluderMeshes();
#endif
	void BeginSubmit( const grcViewport* vp NV_SUPPORT_ONLY(,bool useStereoFrustum=false) );
	bool EndSubmit( DependantTaskList* toMakeDependant = NULL, bool perfWait = true);
	bool EndSubmitWithSySDep();

	int GetTileModelCount(int tile) const	{ return m_tileModelPackets[tile].amt; }
	int GetTileBoxCount(int tile) const		{ return m_tileBoxPackets[tile].amt; }

	void DependsOn( SoftRasterizer* parent){
		if ( parent == NULL ){
			if ( m_parentRasterizer )
				m_parentRasterizer->m_isParent = false;
			m_parentRasterizer = NULL;
			return;
		}
		m_parentRasterizer = parent;
		m_parentRasterizer->m_isParent = true;
	}
	int NumActiveTilers() {
		return  RAST_NUM_TILERS_ACTIVE;
	}

	float GetMinimumZ()					{ return minZ; }
	Vec4V_Out GetMinMaxBounds()			{ return minMaxBoundCombined; }
	// EJ: Occluder Optimization
	void SelectAndRasterizeBoxOccluders(int numBuckets, const CBoxOccluderBucket* pBuckets, u8* isClipped, int amtResBoxOccluders, float threshold, BoxOccluder* resBoxOccluders);
	void SelectAndRasterizeModelOccluders(int numBuckets, const COccludeModelBucket* pBuckets, u8* isClipped, float threshold, OccludeModel* resOccluders);

	void RemoveWaterTriDependancy();
	void AddWaterTriDependancy();
	void RemoveStencilDependancy();
	void AddStencilDependancy();
	void RemoveChildDependancy();


	void setShadowSquares( const ShadowSquares& squares) { m_shadowSquares=squares; }
	void SetTileList( rstTileList* tiler) { Assert( m_tiler == NULL || tiler == NULL); m_tiler = tiler; }

	void SetWaterOccluderResources( OccludeModel* waterOccluders, u8* waterClipped);
	void SetTempHiZBuffer( Vec4V* mem ) { m_tempHiZBuffer = mem;}
	BANK_ONLY( bool ShowTempHiZBuffer() { return m_showStencil && m_showWaterZBuffer ;} )

private:
	u32								m_pad[3];// pad for alignement

	sysDependency					m_modelSelectorDepTask;
	sysDependency					m_selectorDepTask;			
	sysDependency					m_tilerDepTask[RAST_NUM_TILERS_ACTIVE ];	
	sysDependency					m_stencilDepTask;													
	sysDependency					m_scanDepTask[RAST_NUM_TILES];
	sysDependency					m_postTilerTask;
	sysDependency					m_postScanTask;
	sysDependency					m_largeTriangleOcclusionDepTask;
	sysDependency					m_generateWaterTrisDepTask;

	DependantTaskList				m_modelSelectorTask;
	DependantTaskList				m_selectorTask;	
	DependantTaskList				m_tilerTask;	
	DependantTaskList				m_stencilTask;	
	DependantTaskList				m_compareAndGenerateStencilTask;	
	DependantTaskList				m_scanTask;
	DependantTaskList				m_largeTriangleOcclusion;
	DependantTaskList				m_generateWaterTrisTask;

	
	u32								nearZTiles[RAST_NUM_TILES];
	Vec4V							minMaxBounds[RAST_NUM_TILES];
	Vec4V							minMaxBoundCombined;
	float							minZ;

	bool							m_useSysDependencyTasks;
	u32*							m_modelWorkCount;
	rstTileList*					m_tiler;

	SoftRasterizer*					m_parentRasterizer;
	bool							m_isParent;

	void CreateTilerTasks( const rstTransformToScreenSpace& clipAndTransform, const Vec3V* tris, int amt  );
	
	Vec4V*							m_HiZBuffer;
	Vec4V*							m_ZBuffer;
	Vec4V*							m_StencilBuffer;
	Vec4V*							m_tempHiZBuffer;


	rstTransformToScreenSpace*		m_transform;
	rstTransformToScreenSpace*		m_stencilTransform;

	TileDataPacket<BoxOccluder>*	m_tileBoxPackets;
	TileDataPacket<OccludeModel>*	m_tileModelPackets;
	ShadowSquares					m_shadowSquares;
	ScanTileInfo					stInfo[RAST_NUM_TILERS_ACTIVE];

	OccludeModel*		m_outputWaterOccluders;
	u8*					m_outputWaterClipped;
	u32*				m_waterOccluderCount;
	
	int								m_numActiveScan;

	bool							m_useSimple;
	bool							m_useSmall;
	bool							m_preparedTasks;


	bool							m_useTempHiZBufferForStencil;

	bool							m_hasStencilDependancy;
	bool							m_hasWaterTriDependancy;

	void CombineResults();
#if __BANK
	bool							m_debugDraw;
	float							m_viewScale;
	bool							m_showHiZ;
	bool							m_showStencil;
	bool							m_showWaterZBuffer;
	float							m_depthStart;
	float							m_depthEnd;
	float							m_depthAlpha;
	bool							m_depthFullscreen;
	bool							m_depthOverlapShadowTarget;
	bool							m_dumpTaskGraph;
	grcTexture*						m_DebugDrawTexture;	
#endif


};

void rstCalculateOcclusion( const grcViewport* vp, int start, int end, 
	const Vec4V* HiZ, 
	const Vec3V* __restrict bmin, const Vec3V* __restrict bmax, 
	Vec3V* __restrict scbmin,Vec3V* __restrict scbmax,
	u32* __restrict results );

void rstCalculateOcclusionAABBExactEdgeList( const grcViewport* vp, int start, int end, 
						   const Vec4V* HiZ, Vec4V_In minMaxBounds, ScalarV_In minZBuffer, 
						   const Vec3V* __restrict bmin, const Vec3V* __restrict bmax, 
						   u32* __restrict results 
						   DEV_ONLY(, u32* trivialAcceptActiveFrustum, u32* trivialAcceptMinZ, u32* trivialAcceptVisiblePixel)
#if __BANK
						   ,Vec3V* __restrict scmin,Vec3V* __restrict scmax
						   , ScalarV_In simpleTestThresholdPixels = THRESHOLD_PIXELS_SIMPLE_TEST, bool useTrivialAcceptVisiblePixelTest = true
#endif						  
						   );

#if __BANK
void rstDrawScBounds( Vec3V_In bmin, Vec3V_In bmax, Color32 col  );
#endif

#endif // FRAG_TSKLIB_ONLY

} // namespace rage

#endif // SOFTRASTERIZER_SOFTRASTERIZER_H 
