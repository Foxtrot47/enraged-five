// ====================================
// softrasterizer/scanwaterreflection.h
// (c) 2013 RockstarNorth
// ====================================

#ifndef _SOFTRASTERIZER_SCANWATERREFLECTION_H_
#define _SOFTRASTERIZER_SCANWATERREFLECTION_H_

#include "spatialdata/kdop2d.h"
#include "system/bit.h"
#include "vectormath/mat44v.h"

namespace rage {

enum eWaterReflectionVisibilityBounds
{
	WRVB_NONE,
	WRVB_WATER_QUADS,
	WRVB_WATER_QUADS_USE_OCCLUSION,
	WRVB_STENCIL,
	WRVB_STENCIL_VECTORISED,
	WRVB_STENCIL_FAST,
	WRVB_COUNT,

	WRVB_DEFAULT = WRVB_STENCIL // TODO SHIP -- this should be WRVB_STENCIL_VECTORISED, but there's a bug in the code
};

enum // control flags
{
	SCAN_WATER_REFLECTION_CONTROL_SET_CULL_PLANES        = BIT(0),
	SCAN_WATER_REFLECTION_CONTROL_EXTRA_CULL_PLANE_WATER = BIT(1),
	SCAN_WATER_REFLECTION_CONTROL_EXTRA_CULL_PLANE_NEAR  = BIT(2),
};

#if __BANK
enum // debug flags
{
	SCAN_WATER_REFLECTION_DEBUG_REMOVE_ISOLATED_STENCIL = BIT(0),
};
#endif // __BANK

enum // output flags
{
	SCAN_WATER_REFLECTION_OUT_NO_STENCIL = BIT(0),
};

class rstScanWaterReflectionParamsIn
{
public:
	Mat44V m_gameVPCompositeMtx;
	Vec3V m_cameraPos;
	Vec4V m_nearPlane;
	Vec4V m_waterPlane;
	const Vec4V* m_stencil; // 128x72 bits
	u32 m_controlFlags;
#if __BANK
	int m_mode; // eWaterReflectionVisibilityBounds
	u32 m_debugFlags;
#endif // __BANK
};

class rstScanWaterReflectionParamsOut
{
public:
	spdkDOP2D_8 m_visBounds;
	Vec4V m_reflectedPlanes[8];
#if __PS3
	Vec4V m_edgeClipPlanes[4];
	int m_edgeClipPlaneCount;
#endif // __PS3
	u32 m_outputFlags;
#if __BANK
	int m_debugWaterPlaneIndex;
	int m_debugNearPlaneIndex;
#endif // __BANK
};

void ScanWaterReflection(rstScanWaterReflectionParamsOut& paramsOut, const rstScanWaterReflectionParamsIn& paramsIn);

// ================================================================================================
// utilities (from fwmaths/vectorutil.h)

__forceinline Vec4V_Out rstBuildPlane(Vec3V_In p, Vec3V_In n)
{
	return Vec4V(n, -Dot(p, n));
}

__forceinline ScalarV_Out rstPlaneDistanceTo(Vec4V_In plane, Vec3V_In p)
{
#if __XENON // dot products are fast
	return Dot(plane, Vec4V(p, ScalarV(V_ONE)));
#else
	return Dot(plane.GetXYZ(), p) + plane.GetW();
#endif
}

__forceinline Vec3V_Out rstPlaneReflect(Vec4V_In plane, Vec3V_In p)
{
	return SubtractScaled(p, plane.GetXYZ()*ScalarV(V_TWO), rstPlaneDistanceTo(plane, p));
}

__forceinline Vec3V_Out rstPlaneReflectVector(Vec4V_In plane, Vec3V_In v)
{
	return SubtractScaled(v, plane.GetXYZ()*ScalarV(V_TWO), Dot(plane.GetXYZ(), v));
}

__forceinline Vec3V_Out rstTransformProjective(Mat44V_In mat, Vec3V_In v)
{
	const Vec4V h = Multiply(mat, Vec4V(v, ScalarV(V_ONE)));
	return h.GetXYZ()/h.GetW();
}

} // namespace rage

#if __SPU
#include "softrasterizer/scanwaterreflection.cpp"
#endif // __SPU

#endif // _SOFTRASTERIZER_SCANWATERREFLECTION_H_
