#ifndef TASKLIB_H
#define TASKLIB_H



#ifndef FRAG_TSKLIB_ONLY
#include "system/task.h"
#endif

#include "system/cellsyncmutex.h"
#include "system/dma.h"
#include "system/dependency.h"
#include "system/param.h"
#include "data/safestruct.h" 
#include "vectormath/vec4v.h"

//__SPU
#define TASK_EMULATE_SPU  0
//__XENON || __PPU
#define TASK_LOCALACCESS_ONLY   TASK_EMULATE_SPU || __SPU


#if !__SPU
#include "system/memops.h"
#endif

XPARAM(singleThreadedTaskManager);

namespace rage 
{

#ifdef FRAG_TSKLIB_ONLY
	struct sysTaskBuffer {	// matches order of DMA descriptors on PS3
		size_t Size;		// in bytes
		void *Data;			// pointer to data
	};
#endif
#define TSK_ROUNDUP16(x)	(((x)+15)&~15)



	struct ScratchMemStackPoint
	{
		sysTaskBuffer	 m_storedScratchMem;
		sysTaskBuffer*	 m_smVal;
		ScratchMemStackPoint( sysTaskBuffer& scratchMem ) : m_storedScratchMem(scratchMem), m_smVal(&scratchMem)
		{}
		~ScratchMemStackPoint()
		{
			*m_smVal = m_storedScratchMem;
		}
	};

	__forceinline void IsValidLocalPtr( sysTaskBuffer& /*sm*/, void* /*p*/ )
	{
		//#if TASK_LOCALACCESS_ONLY
		//		Assert( sysIpcIsStackAddress(cp) || InScratchMemory( sm));
		//#endif
	}


	template<class T, int alignment>
	T* taskAllocBuffer( sysTaskBuffer& sm, size_t amt )
	{
		size_t bsize = (amt * sizeof(T) + (16-1) ) & ~(16-1); // always 16 byte aligned
		size_t alignOffset = (  ((size_t)sm.Data + (alignment-1))  & ~(alignment-1) )- (size_t)sm.Data ;

		T* scratchAddress = (T*)((char*)sm.Data + alignOffset );

		bsize += alignOffset;

		size_t MaxSize = sm.Size;
		Assert( bsize <= MaxSize );
		bsize = bsize >  MaxSize ? MaxSize : bsize;
		sm.Size -= bsize;
		sm.Data =(void*)((char*)sm.Data + bsize);
		return scratchAddress;
	}
	__forceinline void* spuBuffer( sysTaskBuffer& sm, size_t bsize, const void* ppuAddress,  int SPU_ONLY(tag) )
	{
		Assert( (size_t)ppuAddress >= 256*1024);
#if !__WIN32
		Assert( (size_t)ppuAddress < 0xCDCDCD00);
#endif
		size_t offset = ((size_t)ppuAddress & 15);
		char* readAddress =(char*) ((size_t)ppuAddress& ~15);
		size_t ppuSize = (bsize+offset+15) & ~15;
		size_t MaxSize = sm.Size;
		Assert( ppuSize <= MaxSize );
		ppuSize = ppuSize >  MaxSize ? MaxSize : ppuSize;

		void* dataAddress = sm.Data;
#if __SPU
		if ( bsize> 16*1024){
			int toffset=0;
			size_t tSize = ppuSize;
			while( tSize > 16*1024){
				sysDmaGet((void*)((char*)dataAddress+toffset),( uint64_t)(readAddress+toffset), 16*1024, tag+1);
				tSize -= 16*1024;
				toffset +=16*1024;
			}
			sysDmaGet((void*)((char*)dataAddress+toffset),( uint64_t)(readAddress+toffset), tSize, tag+1);
		}
		else
			sysDmaGet(dataAddress,( uint64_t)readAddress, ppuSize, tag+1);
#else
		sysMemCpy( dataAddress, readAddress, ppuSize);
#endif

		char* scratchAddress = (char*)sm.Data;
		sm.Data = ( scratchAddress + ppuSize);
		sm.Size -= ppuSize;

		return (char*)dataAddress+offset;
	}

	__forceinline void* spuBufferFast( void* dataAddress, size_t bsize, const void* ppuAddress,  int SPU_ONLY(tag) )
	{
		Assert( (size_t)ppuAddress >= 256*1024);
		Assert( ((size_t)dataAddress&15)==0);
		Assert( ((size_t)bsize&15)==0);
		Assert( bsize< 16*1024);
#if __PS3
		Assert( (size_t)ppuAddress < 0xCDCDCD00);
#endif
#if __SPU
		sysDmaGet(dataAddress,( uint64_t)ppuAddress, bsize, tag+1);
#else
		sysMemCpy( dataAddress, ppuAddress, bsize);
#endif
		return (char*)dataAddress;
	}
	template<class T>
	void taskWriteBuffer(  const char* SPU_ONLY(str),const T* src, T* dscEA, size_t amt, int SPU_ONLY(tag) )
	{
		SPU_ONLY(sysDmaSetContext(str));
		size_t bsize = amt * sizeof(T);
		Assert( (size_t)dscEA >= 256*1024);
		Assert( ((size_t)src&15)==0);
		Assert( ((size_t)bsize&15)==0);
#if __PS3
		Assert( (size_t)dscEA < 0xCDCDCD00);
#endif
#if __SPU
		if ( sizeof(T) < 16*1024)
		{
			sysDmaPut(src,( uint64_t)dscEA, bsize, tag+1);
		}
		else{
			sysDmaLargePut(src,( uint64_t)dscEA, bsize, tag+1);
		}
#else
		if  ( dscEA  != src ){
			sysMemCpy( dscEA, src, bsize);
		}
#endif
	}
	template<class T>
	void taskWrite( const char* str, const T* src, T* dscEA, int tag){
		taskWriteBuffer( str,src, dscEA, 1, tag );
	}

	template<class T>
	__forceinline void taskLoad( T& v, const T* dataEA, int
#if __SPU || TASK_EMULATE_SPU
		tag  
#endif
		)
	{
#if __SPU || TASK_EMULATE_SPU
		spuBufferFast( &v, sizeof(T), dataEA, tag );
#else
		v = *dataEA;
#endif
	}
	template<class T>
	__forceinline  T* taskLoadBuffer( 
		const char* SPU_ONLY(str),
#if __SPU || TASK_EMULATE_SPU
		sysTaskBuffer& sm, T* dataEA, int amt, int tag 
#else
		sysTaskBuffer& /*sm*/, T* dataEA, int /*amt*/, int /*tag*/ 
#endif
		)
	{
		SPU_ONLY(sysDmaSetContext(str));
#if __SPU || TASK_EMULATE_SPU
		return (T*)spuBuffer( sm, amt* sizeof(T), dataEA, tag );
#else
		return dataEA;
#endif
	}

	template<class T>
	__forceinline  T* taskLoadBuffer(  const char* SPU_ONLY(str),
#if __SPU || TASK_EMULATE_SPU
		T* localBuffer,  T* dataEA, int amt, int tag 
#else
		T* /*localBuffer*/,  T* dataEA, int /*amt*/, int /*tag*/ 
#endif
		)
	{
		SPU_ONLY(sysDmaSetContext(str));

#if __SPU || TASK_EMULATE_SPU
		return (T*)spuBufferFast( localBuffer, amt* sizeof(T), dataEA, tag );
#else
		return dataEA;
#endif
	}
	template<class T>
	__forceinline  const T* taskLoadBuffer(  const char* SPU_ONLY(str),
#if __SPU || TASK_EMULATE_SPU
		T* localBuffer, const T* dataEA, int amt, int tag 
#else
		T* /*localBuffer*/,const  T* dataEA, int /*amt*/, int /*tag*/ 
#endif
		)
	{
		SPU_ONLY(sysDmaSetContext(str));

#if __SPU || TASK_EMULATE_SPU
		return (T*)spuBufferFast( localBuffer, amt* sizeof(T), dataEA, tag );
#else
		return dataEA;
#endif
	}

	__forceinline void taskWait( int SPU_ONLY(tag))
	{
#if __SPU
		sysDmaWait( 1<<(tag+1));
#endif
	}

	__forceinline u32 taskGetU32( u32* EA, int SPU_ONLY(dmaTAG))
	{
#if __SPU
		return sysDmaGetUInt32((uint64_t)EA, DMA_TAG(dmaTAG));
#else
		return *EA;
#endif
	}
	__forceinline void taskSetU32( u32* EA, u32 val, int SPU_ONLY(dmaTAG))
	{		
#if __SPU
		return sysDmaPutUInt32(val, (uint64_t)EA, DMA_TAG(dmaTAG));
#else
		*EA=val;
#endif
	}
	__forceinline void taskSetVec4V( Vec4V* EA, Vec4V_In val, int SPU_ONLY(dmaTAG))
	{
#if __SPU
		return sysDmaPutAndWait(&val, (uint32_t)EA, sizeof(Vec4V), DMA_TAG(dmaTAG));
#else
		*EA=val;
#endif
	}


	//----------------------------- sysDependency Interface functions

	template<class T>
	void tskSetBuffer(sysDependency& dep, int index , const T* in, int amt) 
	{
		//CompileTimeAssert( (sizeof(T)&0xF)==0);	
		dep.m_Params[index].m_AsPtr = (void*)in;
		dep.m_DataSizes[index]=amt*sizeof(T);
	}
	template<class T>
	T* tskGetBuffer(const sysDependency& dep, int index ,int& amt)
	{
		//	CompileTimeAssert( (sizeof(T)&0xF)==0);
		Assert( (dep.m_DataSizes[index]%sizeof(T))==0);
		amt =dep.m_DataSizes[index] / sizeof(T);
		return (T*)dep.m_Params[index].m_AsPtr;
	}


	__forceinline u32 OutMask( int v)	  { return 1<<(v+7); }
	__forceinline u32 InMask( int v)	  { return 1<<v; }
	__forceinline u32 InOutMask(int v ) { return InMask(v) |  OutMask(v); }




#define tsk_UserDataParmStart 4

	__forceinline bool tskBufferIsNotSet( u32 val, int index )
	{
		Assertf( index < (tsk_UserDataParmStart+1), "Overflow with number of params");
		return ( val & InOutMask(index)) ==0;
	}
	template<class T>
	void tskSetInput(sysDependency& dep , int index, const T* in, int amt = 1) 
	{
		Assert( tskBufferIsNotSet( dep.m_Flags,index ));
		dep.m_Flags |=InMask(index);
		tskSetBuffer( dep, index, in, amt);
	}
	template<class T>
	void tskSetInput(sysDependency& dep , int index, const T& in) 
	{
		Assert( tskBufferIsNotSet( dep.m_Flags,index ));
		dep.m_Flags |=InMask(index);
		tskSetBuffer( dep, index, &in, 1);
	}
	template<class T>
	const T* tskGetInput(const sysDependency& dep , int index,int& amt)
	{
		Assert( dep.m_Flags &InMask(index) );
		return tskGetBuffer<T>(dep,index,amt);
	}
	template<class T>
	void tskSetOutput(sysDependency& dep ,int index ,const T* in, int amt = 1) 
	{
		Assert( tskBufferIsNotSet( dep.m_Flags,index ));
		dep.m_Flags |=OutMask(index);
		tskSetBuffer( dep, index, in, amt);
	}
	template<class T>
	T* tskGetOutput(const sysDependency& dep, int index ,int& amt)
	{
		Assert( dep.m_Flags & OutMask(index));
		return tskGetBuffer<T>(dep, index, amt);
	}

	template<class T>
	void tskSetInputOutput(sysDependency& dep ,int index ,const T* in, int amt = 1) 
	{
		Assertf( tskBufferIsNotSet( dep.m_Flags,index ),"buffer is already set");
		dep.m_Flags |=InOutMask(index);
		tskSetBuffer( dep, index, in, amt);
	}
	template<class T>
	T* tskGetInputOutput(const sysDependency& dep, int index ,int& amt)
	{
		Assertf( ( dep.m_Flags & InOutMask(index) ) == InOutMask(index), "tried to access buffer which isn't a inputOutput buffer");
		return tskGetBuffer<T>(dep, index, amt);
	}

	template<class T>	T*			tskGetInputOutput(const sysDependency& dep, int index)	{ int amt; return tskGetInputOutput<T>(dep, index, amt);}
	template<class T>	const T*	tskGetInput(const sysDependency& dep, int index)		{ int amt; return tskGetInput<T>(dep, index, amt);}
	template<class T>	T*			tskGetOutput(const sysDependency& dep, int index)		{ int amt; return tskGetOutput<T>(dep, index, amt);}

	__forceinline int MinTskVal( int a, int b) { return a <b ? a : b; }


	template<int T>
	struct TSKLIB_USERDATA_IS_TOO_LARGE;

#define DEPTASK_PARAM_NUM 9
	template<class T>
	T tskGetUserData( const sysDependency& dep )
	{
		//T result;
		const int userDataStart = DEPTASK_PARAM_NUM - int((sizeof(T)+3)/sizeof(float));
		const int tskStartParam = MinTskVal(userDataStart,  7-1);
#if !__PS3 && !__PSP2 && !__64BIT
		STATIC_ASSERTION_FAILURE< ((DEPTASK_PARAM_NUM-userDataStart)* sizeof(float)) >= sizeof(T),
			TSKLIB_USERDATA_IS_TOO_LARGE<userDataStart> >();
#endif
		// Calculate alignment offset
		// memcpy gets right of alignment bugs

#if !__SPU
		Assertf(dep.m_DataSizes[tskStartParam] == sizeof(T), "Invalid Type for UserData %s",  typeid(T).name());
#endif
	//	memcpy( &result, &dep.m_Params[tskStartParam], sizeof( T) );
		return *((T*) &dep.m_Params[tskStartParam] );
	}


	__forceinline sysTaskBuffer tskGetScratch( const sysDependency& sys)
	{
		sysTaskBuffer res;
		Assert( sys.m_Flags & sysDepFlag::ALLOC0);
		res.Size = sys.m_DataSizes[0];
		res.Data = sys.m_Params[0].m_AsPtr;
		return res;
	}


#if !__SPU
	template<class T>
	void  tskSetUserData(sysDependency& dep, const T& value)
	{	
		const int userDataStart = DEPTASK_PARAM_NUM - int((sizeof(T)+3)/sizeof(float));
		const int tskStartParam = MinTskVal(userDataStart,  7-1);
#if !__PS3 && !__PSP2 && !__64BIT
		STATIC_ASSERTION_FAILURE< ((DEPTASK_PARAM_NUM-userDataStart)* sizeof(float)) >= sizeof(T),
									TSKLIB_USERDATA_IS_TOO_LARGE<userDataStart> >();
#endif
		// memcpy gets rid of alignment bugs
		memcpy( &dep.m_Params[tskStartParam], &value, sizeof( T) );
		ASSERT_ONLY(dep.m_DataSizes[tskStartParam] = sizeof(T));
	}

	__forceinline void tskSetScratch( sysDependency& sys, u32 scratchSize = 64*1024)
	{
		sys.m_Flags |= sysDepFlag::ALLOC0;
		sys.m_Params[0].m_AsPtr=NULL;
		sys.m_DataSizes[0]=scratchSize;
	}

#endif
};

	//--------------------------------------------------------------------------------


#ifndef FRAG_TSKLIB_ONLY



#if TASK_ALLOW_SPU_ADD_JOB


#include "system/interlocked.h"

using namespace rage;
#if __SPU

#define DEBUG_DISPATCH 0

#include <cell/sync/mutex.h>
#include <cell/spurs.h>
#include "system/dma.h"
#include "system/task_spu_config.h"
#define DMA_TAG(xfer) (xfer + 1)
#define DMA_MASK(xfer) (1 << DMA_TAG(xfer))


	// would be nice to have a payload here as well?
	inline void sysTaskDispatchTask( const sysPreparedTaskHandle& prepHdle , bool fromTasks=true, int dmaTAG  =0)
	{
		if ( prepHdle.hdle == NULL)
		{
			Displayf("Invalid Handle , cannot fire off job");
			return;
		}
		u32 listLockAddress= (u32)prepHdle.JobChainCommandListLockEAddress;
		u64	listAddress = (u64)prepHdle.JobChainCommandListEAddress;
		rageCellSyncMutexLock( listLockAddress );

		u32 cur_index = sysDmaGetUInt32((uint64_t)prepHdle.JobChainCommandListIndexEAddress, DMA_TAG(dmaTAG));

#if DEBUG_DISPATCH
		Displayf("cur_index = %d", cur_index);
#endif

		const int MaxTasks = TASK_MAX_TASKS_SPU;
		const int MaxCommands = MaxTasks + 8;

		u32 nextCommand = cur_index+1;
		nextCommand = (nextCommand == MaxCommands-1) ? 0 : nextCommand;

		u32 endCommand = nextCommand+1;
		endCommand = (endCommand == MaxCommands-1) ? 0 : endCommand;

		// compute end command address
		u64 sync_command_address = listAddress + sizeof(uint64_t)*cur_index;
		u64 end_command_address = listAddress + sizeof(uint64_t)*endCommand;
		u64 job_command_address = listAddress + sizeof(uint64_t)*nextCommand;

#if DEBUG_DISPATCH
		Displayf("end_command_address = %x", endCommand);
		Displayf("job_command_address = %x", nextCommand);
#endif
		sysDmaPutUInt64(CELL_SPURS_JOB_COMMAND_END, (uint64_t)end_command_address, DMA_TAG(dmaTAG));
		// we should not need a sync as the DMA should be finishing it.

		sysDmaPutUInt64(CELL_SPURS_JOB_COMMAND_SYNC, (uint64_t)sync_command_address, DMA_TAG(dmaTAG));



		uint64_t job_command = CELL_SPURS_JOB_COMMAND_JOB( (CellSpursJob256*)prepHdle.jobToAdd);
		sysDmaPutUInt64(job_command, (uint64_t)job_command_address, DMA_TAG(dmaTAG));
#if DEBUG_DISPATCH
		Displayf("job_command = %llx", job_command);
#endif

		cur_index = endCommand;
		sysDmaPutUInt32(cur_index, (uint64_t)prepHdle.JobChainCommandListIndexEAddress, DMA_TAG(dmaTAG));

		rageCellSyncMutexUnlock( listLockAddress );
		//	We might not need to call this if this SPU job is spawning a job
		//	on the same job chain. 	
		cellSpursRunJobChain((uint64_t)prepHdle.JobChainEAddress);
	}

	inline void sysTaskDispatchTasks( const sysPreparedTaskHandle *hdles , int amt, bool fromTasks=true, int dmaTAG  =0)
	{
		if ( amt == 0){
			return;
		}
		u32 listLockAddress= (u32)hdles[0].JobChainCommandListLockEAddress;
		u64 listAddress = (u64)hdles[0].JobChainCommandListEAddress;
		rageCellSyncMutexLock( listLockAddress );

		u32 cur_index = sysDmaGetUInt32((uint64_t)hdles[0].JobChainCommandListIndexEAddress, DMA_TAG(dmaTAG));

		const int MaxTasks = TASK_MAX_TASKS_SPU;
		const int MaxCommands = MaxTasks + 8;
		u32 start_index = cur_index;

		for (int i =0; i < amt; i++ ){
			u32 nextCommand = cur_index+1;
			nextCommand = (nextCommand == MaxCommands-1) ? 0 : nextCommand;

			u64 job_command_address = listAddress + sizeof(uint64_t)*nextCommand;
			uint64_t job_command = CELL_SPURS_JOB_COMMAND_JOB( (CellSpursJob256*)hdles[i].jobToAdd);
			sysDmaPutUInt64(job_command, (uint64_t)job_command_address, DMA_TAG(dmaTAG));

#if DEBUG_DISPATCH
			Displayf("job_command_address = %x", nextCommand);
			Displayf("job_command = %llx", job_command);
#endif
			cur_index = nextCommand;		
		}
		u32 endCommand = cur_index+1;
		endCommand = (endCommand == MaxCommands-1) ? 0 : endCommand;
		u64 sync_command_address = listAddress + sizeof(uint64_t)*start_index;
		u64 end_command_address = listAddress + sizeof(uint64_t)*endCommand;

		sysDmaPutUInt32(endCommand, (uint64_t)hdles[0].JobChainCommandListIndexEAddress, DMA_TAG(dmaTAG));

		sysDmaPutUInt64(CELL_SPURS_JOB_COMMAND_END, (uint64_t)end_command_address, DMA_TAG(dmaTAG));

		// we should not need a sync as the DMA should be finishing it.
		sysDmaPutUInt64(CELL_SPURS_JOB_COMMAND_SYNC, (uint64_t)sync_command_address, DMA_TAG(dmaTAG));

		rageCellSyncMutexUnlock( listLockAddress );

		//	We might not need to call this if this SPU job is spawning a job
		//	on the same job chain. 	
		cellSpursRunJobChain((uint64_t)hdles[0].JobChainEAddress);

	}
#else

	inline void sysTaskDispatchTask( const sysPreparedTaskHandle& prepHdle , 
		bool 
#if __WIN32 && TASK_USE_CONTINUATION_TASK
			fromTasks=true 
#endif
			,int /*dmaTAG*/ =0 
		)
	{

		if(PARAM_singleThreadedTaskManager.Get())
		{
			//SetEvent(taskInfo.Completed);
			////		PIXEnd();
			sysTaskManager::DispatchLocally( prepHdle);
		}
		else
		{
	#if __WIN32 && TASK_USE_CONTINUATION_TASK
			// PURPOSE: Sets the next task to be executed on the same thread.
			// can be useful to chain tasks together.
			if ( !fromTasks || !sysTaskManager::SetContinuationTask( prepHdle.hdle) )
	#endif

			sysTaskManager::Dispatch( prepHdle);
		}
	}
	inline void sysTaskDispatchTasks( const sysPreparedTaskHandle *hdles , int amt, bool fromTasks=true, int dmaTAG =0)
	{
		for (int i = 0; i < amt; i++)
			sysTaskDispatchTask( hdles[i], fromTasks,dmaTAG);
	}

#endif //__SPU

#endif //TASK_ALLOW_SPU_ADD_JOB


namespace rage
{


#define TSK_USER_DATA_START  1

	template<class T>
	T tskGetUserData( const sysTaskParameters& p )
	{
		T result;
		CompileTimeAssert( ((TASK_MAX_USER_DATA-TSK_USER_DATA_START)* sizeof(float)) >= sizeof(T));
		// Calculate alignment offset
		// memcpy gets right of alignment bugs
#if !__SPU
		Assertf( p.UserData[0].asPtr == typeid(T).name(), "Invalid Type for UserData %s",  typeid(T).name());
#endif
		Assert( p.UserDataCount == ((sizeof(T)+3)>>2) + TSK_USER_DATA_START );
		memcpy( &result, &p.UserData[TSK_USER_DATA_START], sizeof( T) );
		return result;
	}
	template<class T>
	void  tskSetUserData(sysTaskParameters& p , const T& value)
	{	
		CompileTimeAssert( ( (TASK_MAX_USER_DATA-TSK_USER_DATA_START)* sizeof(float)) >= sizeof(T));
		// memcpy gets rid of alignment bugs
#if !__SPU
		p.UserData[0].asPtr = (void*)typeid(T).name();
#endif
		memcpy( &p.UserData[TSK_USER_DATA_START], &value, sizeof( T) );
		p.UserDataCount = ((sizeof(T)+3)>>2) + TSK_USER_DATA_START;
	}
	template<class T>
	void tskSetInput(rage::sysTaskParameters& p , const T* in, int amt) 
	{
		CompileTimeAssert( (sizeof(T)&0xF)==0);
		Assert( (p.Input.Size%sizeof(T))==0);

		p.Input.Data = (void*)in;
		p.Input.Size = sizeof(T)*amt;
	}
	template<class T>
	T* tskGetInput(const rage::sysTaskParameters& p ,size_t& amt)
	{
		CompileTimeAssert( (sizeof(T)&0xF)==0);
		Assert( (p.Input.Size%sizeof(T))==0);
		amt =p.Input.Size / sizeof(T);
		return (T*)p.Input.Data;
	}
	template<class T>
	void tskSetReadOnly(rage::sysTaskParameters& p , T* val) 
	{
		CompileTimeAssert( (sizeof(T)&0xF)==0);
		Assert16(val);
		p.ReadOnly[p.ReadOnlyCount].Data=(void*)val;
		p.ReadOnly[p.ReadOnlyCount++].Size=sizeof(T);
		Assert( p.ReadOnlyCount <= (TASK_MAX_READ_ONLY-1));
	}
	
	template<class T>
	void tskSetReadOnlyBuffer(rage::sysTaskParameters& p , const T* val, int size) 
	{
		Assert(val);
		CompileTimeAssert( (sizeof(T)&0xF)==0);
		Assert16(val);
		p.ReadOnly[p.ReadOnlyCount].Data=(void*)val;
		p.ReadOnly[p.ReadOnlyCount++].Size=sizeof(T)*size;
		Assert( p.ReadOnlyCount <= (TASK_MAX_READ_ONLY-1));
	}

	template<class T>
	T* tskGetReadOnlyBuffer(rage::sysTaskParameters& p , int index, int ASSERT_ONLY(size)) 
	{
		CompileTimeAssert( (sizeof(T)&0xF)==0 );
		Assert( p.ReadOnly[index].Size == sizeof(T)*size);
		return (T*)p.ReadOnly[index].Data;
	}
	template<class T>
	void tskSetReadOnly(rage::sysTaskParameters& p , const T& val) 
	{
		CompileTimeAssert( (sizeof(T)&0xF)==0);
		Assert16(&val);
		p.ReadOnly[p.ReadOnlyCount].Data=(void*)&val;
		p.ReadOnly[p.ReadOnlyCount++].Size=sizeof(T);
		Assert( p.ReadOnlyCount <= (TASK_MAX_READ_ONLY-1));
	}

	template<class T>
	T* tskGetReadOnly(rage::sysTaskParameters& p , int index) 
	{
		CompileTimeAssert( (sizeof(T)&0xF)==0 );
		Assert( p.ReadOnly[index].Size == sizeof(T));
		return (T*)p.ReadOnly[index].Data;
	}
	// Task Dependency Control
	class fiStream; 
	class ALIGNAS(16) DependantTaskList
	{
		static const int MaxTasksInList=12;
		static const int MaxDependantsInList=3;

		int								m_numDependants;  // in theory would be the same as refCnt
		DependantTaskList*				dependants[MaxDependantsInList];

		// tasklist

		u32*							m_refCount;		
		int								m_amt;
		u32								pad3;
		u32								pad4;

		// handles to my tasks.
		sysPreparedTaskHandle			hdles[MaxTasksInList];


		DependantTaskList( DependantTaskList& other);
		DependantTaskList operator=( DependantTaskList& other);

		static u32									m_OutputDummy[4] ;
		DependantTaskList() { m_refCount =0;}
	public:
#if !__SPU

		DependantTaskList(bool) : 
			m_amt(0), m_refCount( rage_new u32() ),m_numDependants(0){
				Assert16(this);
			}
#endif

		~DependantTaskList()
		{
#if !__SPU
#if !__WIN32PC
			Assert( m_refCount == 0);
#endif
			delete m_refCount;		
#endif
		}
		int	NumTasks() const { return m_amt; }

#if !__SPU
#if __BANK
		const char* GetName() const { return sysTaskManager::GetTaskName(hdles[0].hdle); }
#endif
		

		template<class T>
		void Add( TASK_INTERFACE_PARAMS, sysTaskParameters &p , T uv)
		{
			tskSetUserData(p,uv);
			Add( TASK_INTERFACE_PARAMS_PASS, p);
		}
		void Add( TASK_INTERFACE_PARAMS, sysTaskParameters &p );
#endif
		void AddResourceDependancy()
		{
			sysInterlockedIncrement(m_refCount);
		}
		__forceinline int GetRefCount()
		{
			return *m_refCount;
		}
		void RemoveDependancy( bool fromTasks, int DMATag = 1)
		{
			if ( !IsSetup() )
				return;
#if !__SPU
			Assertf( *m_refCount>0, "Ref Count is zero (%i)", *m_refCount);
#endif
			if ( sysInterlockedDecrement(m_refCount) == 0) // can execute
			{
				// once we are all done fire off child tasks
				sysTaskDispatchTasks(hdles, m_amt, fromTasks,DMATag);
			}
		}
		void DependsOn(int amt=1){
			Assert( IsSetup());
			sysInterlockedAdd(m_refCount,amt );
		}

		void DependsOn(DependantTaskList& p){
			Assert( IsSetup());
			if ( p.IsSetup())
			{
				sysInterlockedAdd(m_refCount, p.m_amt );
				p.dependants[p.m_numDependants++]=this;
			}
		}
		// Call when the task should be the root of the dependancy tree. 
		// it checks to make sure.
		void StartRoot(){
			if ( m_amt==0)
				return;
			Assert( *m_refCount == 0);
			Start();
		}
		void Start( int DMATag=1)
		{			
			sysTaskDispatchTasks(hdles, m_amt, false,DMATag);
		}
#if !__SPU
		void Reset()
		{
			m_amt =0;
			*m_refCount=0;
		}
		void Wait();
		void StartAndWait()
		{
			Start();
			Wait();
		}
#endif
		bool IsSetup() const
		{
			return m_amt !=0;
		}
#if !__SPU && __BANK
		static void taskDumpDependancyGraphRecurse( fiStream* out, const DependantTaskList* dtask );
		static void taskDumpDependancyGraph( const char* fname, const DependantTaskList** rootNodes, int amt );
#endif

		static void UpdateDependancies( const sysTaskParameters& p, int DMATag =1 )
		{
			const DependantTaskList*	dtask = (DependantTaskList*)p.ReadOnly[p.ReadOnlyCount-1].Data;
			DependantTaskList	childTasksBuf[MaxDependantsInList];
			DependantTaskList*  childTasks[MaxDependantsInList];
			for (int i = 0; i < dtask->m_numDependants; i++)
			{
				childTasks[i] = taskLoadBuffer(  "childTasks",&childTasksBuf[i], dtask->dependants[i], 1,  DMATag);
			}
			taskWait( DMATag );
			for (int i = 0; i < dtask->m_numDependants; i++)
			{
				childTasks[i]->RemoveDependancy(true,DMATag);	
			}
		}
	} ;
	CompileTimeAssert((sizeof(DependantTaskList)&15)==0);
	

};
#endif



#endif
