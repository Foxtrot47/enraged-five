//
// pedsafezone.h
// 
// Copyright (C) 2014 Rockstar Games.  All Rights Reserved.
//
#ifndef PED_SAFE_ZONE_H
#define PED_SAFE_ZONE_H

#include <vector>
#include "vector/vector3.h"
#include "spatialdata/aabb.h"

namespace rage
{
	//If you alter these, you need to rebuild all the vehicles.rpf data
#define	GTA_VEHICLE_DAMAGE_DELTA_SCALE			     (0.5f)
#define VEHICLE_DEFORMATION_PROPORTIONAL			 (0) 

	// The rate at which larger vehicles have their damage emphasized more due to their size, Tailgater = 0 (min), Titan = 1 (max)
	// The overall damage multiplier is thus = 1 (base) + [0->1 based on relative size) * VEHICLE_DEFORMATION_PROPORTIONAL_SCALE_SLOPE
#define VEHICLE_DEFORMATION_PROPORTIONAL_SCALE_SLOPE (4.0f)

	//It is possible to patch the vertices' cpv.g values on XENON and PS3 using GetVehicleDamage()->RecalculatePedSafeZone() at runtime, since it's too late to change the data
	//For now just leave those off and let ragebuilder do the work for Next Gen only. In BANK we can recalculate on the fly for tuning purposes, prior to a fresh data rebuild.
#define ADD_PED_SAFE_ZONE_TO_VEHICLES ((RSG_ORBIS * 1) + (RSG_DURANGO * 1) + (RSG_PC * 1) + (RSG_XENON * 0) + (RSG_PS3 * 0))

	//Make the unsafe zone a little farther away from the safe zone, to protect the wheels a little
#define PED_SAFE_PROTECT_WHEELS (ADD_PED_SAFE_ZONE_TO_VEHICLES * 1)

#if ADD_PED_SAFE_ZONE_TO_VEHICLES

	class crSkeletonData;
	#define PED_SEAT_MAX 18
	#define WHEELS_PED_SAFE_MAX 10

//////////////////////////////////////////////////////////////////////////
// PURPOSE
//	Computes the damage scale per vertex, or at a specific model space position, so that we can protect the ped seating area. This is set into the Green diffuse channel of the vertices in Rage builder,
//  and the same calculations must also be used in-game so that bone coordinates such as wheels and doors and windows match the damage scale at that particular position in model space.
//
class CPedSafeZone
{
public:
	CPedSafeZone();
	CPedSafeZone(const crSkeletonData* pSkeletonData, const Vector3& boundingBoxMin, const Vector3& boundingBoxMax);
	void ReComputePedSafeArea(const crSkeletonData* pSkeletonData, const Vector3& boundingBoxMin, const Vector3& boundingBoxMax);
	void ReComputePedSafeArea(const crSkeletonData* pSkeletonData, float fMaxDamageRangeInMeters);

	const Vector3& GetPedSafeAreaMin() const { return m_PedSafeAreaBB.GetMinVector3(); }
	const Vector3& GetPedSafeAreaMax() const { return m_PedSafeAreaBB.GetMaxVector3(); }
	const Vector3& GetPedUnSafeAreaMin() const { return m_PedUnSafeAreaBB.GetMinVector3(); }
	const Vector3& GetPedUnSafeAreaMax() const { return m_PedUnSafeAreaBB.GetMaxVector3(); }

	static float CalculateDamageMultiplier(const Vector3& boundingBoxMin, const Vector3& boundingBoxMax, float slope);
	
	void GetPedSafeAreaMinMax(Vector3& safeMin, Vector3& safeMax, Vector3& unSafeMin, Vector3& unSafeMax) const;

	//returns 0 -> 1 range to multiply damage based on whether we are in the safe area (0), just around it in the buffer zone (0 -> 1), or outside, (1)
	ScalarV_Out GetPedSafeAreaMultiplier(Vec3V_In vecOffset) const;

	static bank_float m_sfPedSafeClearanceForSides;
	static bank_float m_sfPedSafeClearanceSpaceForFeet;
	static bank_float m_sfPedSafeClearanceUnderneath;
	static bank_float m_sfPedSafeClearanceSpaceForHeadAndHats;
	static bank_float m_sfPedSafeClearanceSpaceForBack;

#if PED_SAFE_PROTECT_WHEELS
	static bank_float m_sfProtectWheelsMult;
	static bank_float m_sfProtectWheelsRange;
	static bank_float m_sfProtectWheelsClamp;
	static bank_bool m_sbProtectWheels;
#endif

private:
	void ClearPedSafeArea();

	s32 m_PedBoneIDArray[PED_SEAT_MAX];
	spdAABB m_PedSafeAreaBB;
	spdAABB m_PedUnSafeAreaBB;
	float m_fMaxDamageRangeInMeters;

#if PED_SAFE_PROTECT_WHEELS
	s32 m_WheelIDArray[WHEELS_PED_SAFE_MAX];
	float m_fMaxWheelDistanceToPedSafe;
#endif
	
};

#endif // ADD_PED_SAFE_ZONE_TO_VEHICLES

}
#endif // PED_SAFE_ZONE_H
