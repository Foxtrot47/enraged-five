// 
// pedsafezone.cpp
// 
// Copyright (C) 2014 Rockstar Games.  All Rights Reserved. 
// 

#include "pedsafezone.h"
#include "crskeleton/skeletondata.h"
#include "vectormath/legacyconvert.h"

namespace rage
{
#if ADD_PED_SAFE_ZONE_TO_VEHICLES

bank_float CPedSafeZone::m_sfPedSafeClearanceForSides				= 0.29f;
bank_float CPedSafeZone::m_sfPedSafeClearanceSpaceForFeet			= 0.93f;
bank_float CPedSafeZone::m_sfPedSafeClearanceUnderneath				= 0.40f;
bank_float CPedSafeZone::m_sfPedSafeClearanceSpaceForHeadAndHats	= 0.86f;
bank_float CPedSafeZone::m_sfPedSafeClearanceSpaceForBack			= 0.64f;

//There should always be at least some damage influence, even inside the cabin, otherwise it will look weird at the threshold between ped safe zone and unsafe zone.
#define VEHICLE_DEFORMATION_MINIMUM (0.05f)
#define VEHICLE_DEFORMATION_PROPORTIONAL_SCALE_SLOPE (4.0f)
const float MIN_DAMAGE_RANGE = 4.975f; // tailgater = 0, min damage size multiplier
const float MAX_DAMAGE_RANGE = 68.8f - MIN_DAMAGE_RANGE; // jet = 1.0, max damage size multiplier

static const char * s_PedSeatBoneNames[PED_SEAT_MAX]	=
{
	"seat_dside_f",
	"seat_dside_r",
	"seat_dside_r1",
	"seat_dside_r2",
	"seat_dside_r3",
	"seat_dside_r4",
	"seat_dside_r5",
	"seat_dside_r6",
	"seat_dside_r7",
	"seat_pside_f",
	"seat_pside_r",
	"seat_pside_r1",
	"seat_pside_r2",
	"seat_pside_r3",
	"seat_pside_r4",
	"seat_pside_r5",
	"seat_pside_r6",
	"seat_pside_r7",
};

#if PED_SAFE_PROTECT_WHEELS
bank_float CPedSafeZone::m_sfProtectWheelsMult  = 0.9f;
bank_float CPedSafeZone::m_sfProtectWheelsRange = 0.25f;
bank_float CPedSafeZone::m_sfProtectWheelsClamp = 1.5f;
bank_bool  CPedSafeZone::m_sbProtectWheels	    = true;

static const char * s_WheelBoneNames[WHEELS_PED_SAFE_MAX]	=
{
	"wheel_lf",
	"wheel_rf",
	"wheel_lm1",
	"wheel_rm1",
	"wheel_lm2",
	"wheel_rm2",
	"wheel_lm3",
	"wheel_rm3",
	"wheel_lr",
	"wheel_rr",
};

#endif


CPedSafeZone::CPedSafeZone()
{
}

CPedSafeZone::CPedSafeZone(const crSkeletonData* pSkeletonData, const Vector3& boundingBoxMin, const Vector3& boundingBoxMax)
{
	ReComputePedSafeArea(pSkeletonData, boundingBoxMin, boundingBoxMax);
}

void CPedSafeZone::ReComputePedSafeArea(const crSkeletonData* pSkeletonData, const Vector3& boundingBoxMin, const Vector3& boundingBoxMax)
{
	ReComputePedSafeArea(pSkeletonData, CalculateDamageMultiplier(boundingBoxMin, boundingBoxMax, VEHICLE_DEFORMATION_PROPORTIONAL_SCALE_SLOPE) * GTA_VEHICLE_DAMAGE_DELTA_SCALE);
}

float CPedSafeZone::CalculateDamageMultiplier(const Vector3& boundingBoxMin, const Vector3& boundingBoxMax, float slope)
{
#if VEHICLE_DEFORMATION_PROPORTIONAL
	float vehicleLength = boundingBoxMax.y - boundingBoxMin.y;
	float sizeMultiplier = Abs(vehicleLength - MIN_DAMAGE_RANGE) / MAX_DAMAGE_RANGE;

	if (sizeMultiplier < 1.0f)
	{
		return 1.0f;
	}

	float extraSize = sizeMultiplier - 1.0f;
	extraSize *= slope;
	return (1.0f + extraSize);
#else
	(void)boundingBoxMin;
	(void)boundingBoxMax;
	(void)slope;
	return 1.0f;
#endif
}

void CPedSafeZone::ClearPedSafeArea()
{
	Vec3V vZero(V_ZERO);
	m_PedSafeAreaBB.Set(vZero, vZero);
	m_PedUnSafeAreaBB.Set(vZero, vZero);
	m_fMaxDamageRangeInMeters = 0.0f;

#if PED_SAFE_PROTECT_WHEELS
	m_fMaxWheelDistanceToPedSafe = 0.0f;
#endif
}

void CPedSafeZone::ReComputePedSafeArea(const crSkeletonData* pSkeletonData, float fMaxDamageRangeInMeters)
{
	ClearPedSafeArea();

	if (!pSkeletonData || (fMaxDamageRangeInMeters <= 0.0f))
	{
		return;
	}

	m_fMaxDamageRangeInMeters = fMaxDamageRangeInMeters;

	for(int i=0; i < PED_SEAT_MAX; ++i)
	{
		const crBoneData *pBone = pSkeletonData->FindBoneData(s_PedSeatBoneNames[i]);
		if( pBone )
		{
			m_PedBoneIDArray[i] = pBone->GetIndex();
		}
		else
		{
			m_PedBoneIDArray[i] = -1;
		}
	}

	Vector3 safeBoxMin = Vector3(+FLT_MAX, +FLT_MAX, +FLT_MAX);
	Vector3 safeBoxMax = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	//Get the seat mins and maxes
	for(int i=0; i < PED_SEAT_MAX; ++i)
	{
		const s32 boneIdx = m_PedBoneIDArray[i];

		if (boneIdx != -1 && (pSkeletonData->GetBoneData(boneIdx)))
		{
			// get the original position of the seat
			Vector3 seatPosition = VEC3V_TO_VECTOR3(pSkeletonData->GetBoneData(boneIdx)->GetDefaultTranslation());

			safeBoxMin.x = Min<float>(seatPosition.x, safeBoxMin.x);
			safeBoxMin.y = Min<float>(seatPosition.y, safeBoxMin.y);
			safeBoxMin.z = Min<float>(seatPosition.z, safeBoxMin.z);

			safeBoxMax.x = Max<float>(seatPosition.x, safeBoxMax.x);
			safeBoxMax.y = Max<float>(seatPosition.y, safeBoxMax.y);
			safeBoxMax.z = Max<float>(seatPosition.z, safeBoxMax.z);
		}
	}

	Vector3 extraClearanceMin = Vector3(-m_sfPedSafeClearanceForSides, -m_sfPedSafeClearanceSpaceForBack, -m_sfPedSafeClearanceUnderneath);
	Vector3 extraClearanceMax = Vector3(m_sfPedSafeClearanceForSides, m_sfPedSafeClearanceSpaceForFeet, m_sfPedSafeClearanceSpaceForHeadAndHats);

	m_PedSafeAreaBB.SetMin(VECTOR3_TO_VEC3V(safeBoxMin + extraClearanceMin));
	m_PedSafeAreaBB.SetMax(VECTOR3_TO_VEC3V(safeBoxMax + extraClearanceMax));

	//The max damage is one meter in any direction, so we reduce the damage in the buffer zone as we get closer to the safe zone.
	Vector3 bufferZone = Vector3(m_fMaxDamageRangeInMeters, m_fMaxDamageRangeInMeters, m_fMaxDamageRangeInMeters);
	m_PedUnSafeAreaBB.SetMin(VECTOR3_TO_VEC3V(m_PedSafeAreaBB.GetMinVector3() - bufferZone));
	m_PedUnSafeAreaBB.SetMax(VECTOR3_TO_VEC3V(m_PedSafeAreaBB.GetMaxVector3() + bufferZone));


#if PED_SAFE_PROTECT_WHEELS

	for(int i=0; i < WHEELS_PED_SAFE_MAX && m_sbProtectWheels; ++i)
	{
		const crBoneData *pBone = pSkeletonData->FindBoneData(s_WheelBoneNames[i]);
		if( pBone )
		{
			m_WheelIDArray[i] = pBone->GetIndex();
		}
		else
		{
			m_WheelIDArray[i] = -1;
		}
	}

	float maxWheelDistance = 0.0f;

	//Get the distance from each wheel to the safe zone, and find the max, this will be used to scale up the size of the buffer zone
	for(int i=0; i < WHEELS_PED_SAFE_MAX && m_sbProtectWheels; ++i)
	{
		const s32 boneIdx = m_WheelIDArray[i];

		if (boneIdx != -1 && (pSkeletonData->GetBoneData(boneIdx)))
		{
			// get the original position of the seat
			Vector3 wheelPosition = VEC3V_TO_VECTOR3(pSkeletonData->GetBoneData(boneIdx)->GetDefaultTranslation());

			ScalarV distanceToPoint = m_PedSafeAreaBB.DistanceToPoint(VECTOR3_TO_VEC3V(wheelPosition));
			maxWheelDistance = Max<float>(distanceToPoint.Getf(), maxWheelDistance);
		}
	}

	if (m_sbProtectWheels && (maxWheelDistance > 0.0f))
	{
		maxWheelDistance += m_sfProtectWheelsRange;

		//Increase the size of the damage shield a bit, to make the gradient of damage scales in 3D space less steep. This will make the wheel wells more safe, as well as the rest of the car generally.
		float extraSizeMult = Min<float>(maxWheelDistance / m_fMaxDamageRangeInMeters, m_sfProtectWheelsClamp);
		extraSizeMult = Max<float>(extraSizeMult * m_sfProtectWheelsMult, 1.0f);

		m_fMaxDamageRangeInMeters *= extraSizeMult;

		//Re-calculate the unsafe area, so that subsequent calculations will have a larger range.
		bufferZone = Vector3(m_fMaxDamageRangeInMeters, m_fMaxDamageRangeInMeters, m_fMaxDamageRangeInMeters);
		m_PedUnSafeAreaBB.SetMin(VECTOR3_TO_VEC3V(m_PedSafeAreaBB.GetMinVector3() - bufferZone));
		m_PedUnSafeAreaBB.SetMax(VECTOR3_TO_VEC3V(m_PedSafeAreaBB.GetMaxVector3() + bufferZone));
	}
#endif
}

void CPedSafeZone::GetPedSafeAreaMinMax(Vector3& safeMin, Vector3& safeMax, Vector3& unSafeMin, Vector3& unSafeMax) const
{
	safeMin = GetPedSafeAreaMin();
	safeMax = GetPedSafeAreaMax();
	unSafeMin = GetPedUnSafeAreaMin();
	unSafeMax = GetPedUnSafeAreaMax();
}

	
ScalarV_Out CPedSafeZone::GetPedSafeAreaMultiplier(Vec3V_In vecOffset) const
{
	if ((m_fMaxDamageRangeInMeters == 0.0f) || !m_PedSafeAreaBB.IsValid() || !m_PedUnSafeAreaBB.IsValid())
	{
		return ScalarV(V_ONE);
	}

	//Early return for trivial cases.
	if (m_PedSafeAreaBB.ContainsPoint(vecOffset))
	{
		return ScalarVFromF32(VEHICLE_DEFORMATION_MINIMUM);
	}
	else if (!m_PedUnSafeAreaBB.ContainsPoint(vecOffset))
	{
		return ScalarV(V_ONE);
	}
	
	ScalarV distanceToPoint = m_PedSafeAreaBB.DistanceToPoint(vecOffset);
	float multiplier = Max(VEHICLE_DEFORMATION_MINIMUM, (distanceToPoint.Getf() / m_fMaxDamageRangeInMeters));
	return ScalarVFromF32(multiplier);
}

#endif // ADD_PED_SAFE_ZONE_TO_VEHICLES

} // namespace rage




