//// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//// PARTICULAR PURPOSE.
////
//// Copyright (c) Microsoft Corporation. All rights reserved

#if RSG_DURANGO

#include "ChatIntegrationLayer_durango.winrt.h"

#include "intsafe.h"
#include "mmreg.h"
#include "robuffer.h"

#include "math/amath.h"
#include "system/memory.h"

#if RSG_DEV
#include "system/new_winrt.h"
#else
#include "system/new.h"
#endif // RSG_DEV

#define ENABLE_GAMECHAT_LIB_OUTPUT 0

extern __THREAD int RAGE_LOG_DISABLE;

static float gs_MicLoudness = 0.0f;
static float gs_MicLoudnessMean = 0.0f;

static std::shared_ptr<ChatIntegrationLayer> chatIntegrationLayerInstance = nullptr;

rage::sysCriticalSectionToken ChatIntegrationLayer::m_chatUserUpdateCriticalSectionToken;
rage::sysCriticalSectionToken ChatIntegrationLayer::m_chatPacketVectorCriticalSectionToken;
rage::sysCriticalSectionToken ChatIntegrationLayer::m_userPacketVectorCriticalSectionToken;

namespace Microsoft {
namespace Xbox {
namespace GameChat {

Microsoft::Xbox::GameChat::ChatUserUpdateType ChatUserUpdate::UpdateType::get()
{
	return m_updateType;
}

void ChatUserUpdate::UpdateType::set(_In_ Microsoft::Xbox::GameChat::ChatUserUpdateType value)
{
	m_updateType = value;
}

uint8 ChatUserUpdate::ChannelIndex::get()
{
	return m_channelIndex;
}

void ChatUserUpdate::ChannelIndex::set(_In_ uint8 value)
{
	m_channelIndex = value;
}

Platform::Object^ ChatUserUpdate::UniqueRemoteConsoleIdentifier::get()
{
	return m_uniqueRemoteConsoleIdentifier;
}

void ChatUserUpdate::UniqueRemoteConsoleIdentifier::set(_In_ Platform::Object^ value)
{
	m_uniqueRemoteConsoleIdentifier = value;
}

Windows::Xbox::System::IUser^ ChatUserUpdate::User::get()
{
	return m_user;
}

void ChatUserUpdate::User::set(_In_ Windows::Xbox::System::IUser^ value)
{
	m_user = value;
}

} // namespace GameChat
} // namespace Xbox
} // namespace Microsoft

bool ChatIntegrationLayerUtils::IsStringEqualCaseInsenstive( Platform::String^ val1, Platform::String^ val2 )
{
	return ( _wcsicmp(val1->Data(), val2->Data()) == 0 );
}

void ChatIntegrationLayerUtils::GetBufferBytes( __in Windows::Storage::Streams::IBuffer^ buffer, __out byte** ppOut )
{
	if ( ppOut == nullptr || buffer == nullptr )
	{
		throw ref new Platform::InvalidArgumentException();
	}

	*ppOut = nullptr;

	Microsoft::WRL::ComPtr<IInspectable> srcBufferInspectable(reinterpret_cast<IInspectable*>( buffer ));
	Microsoft::WRL::ComPtr<Windows::Storage::Streams::IBufferByteAccess> srcBufferByteAccess;
	srcBufferInspectable.As(&srcBufferByteAccess);
	srcBufferByteAccess->Buffer(ppOut);
}

// To integrate the Chat DLL in your game, you can use this ChatIntegrationLayer class with modifications, 
// or create your own design your own class using the code in this file a guide.
std::shared_ptr<ChatIntegrationLayer> GetChatIntegrationLayer()
{
    if (chatIntegrationLayerInstance == nullptr)
    {
        chatIntegrationLayerInstance.reset( rage_new ChatIntegrationLayer() );
    }

    return chatIntegrationLayerInstance;
}

ChatIntegrationLayer::ChatIntegrationLayer() 
{

}

void ChatIntegrationLayer::InitializeChatManager( 
    __in bool combineCaptureBuffersIntoSinglePacket,
    __in bool useKinectAsCaptureSource,
    __in bool applySoundEffectsToCapturedAudio,
    __in bool applySoundEffectsToChatRenderedAudio
    )
{
    m_chatManager = ref new Microsoft::Xbox::GameChat::ChatManager();
	m_isChatManagerValid = true;

	m_OnUserAudioDeviceAddedCB = NULL;
	m_OnUserAudioDeviceRemovedCB = NULL;

	{
		rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);
		m_isProcessingChatUserUpdate = false;
	}

	++RAGE_LOG_DISABLE;
	m_pendingChatUserUpdates = ref new Platform::Collections::Vector<Microsoft::Xbox::GameChat::ChatUserUpdate^>();
	m_activeChatUserAsyncOperations = ref new Platform::Collections::Vector<Windows::Foundation::IAsyncAction^>();
	m_pendingOutgoingChatPackets = ref new Platform::Collections::Vector<Microsoft::Xbox::GameChat::ChatPacketEventArgs^>();
	m_pendingOutgoingUserPackets = ref new Platform::Collections::Vector<Microsoft::Xbox::GameChat::ChatPacketEventArgs^>();
	--RAGE_LOG_DISABLE;

    m_chatManager->ChatSettings->CombineCaptureBuffersIntoSinglePacket = combineCaptureBuffersIntoSinglePacket; // if unset, it defaults to TRUE
    m_chatManager->ChatSettings->UseKinectAsCaptureSource = useKinectAsCaptureSource; // if unset, it defaults to FALSE
    m_chatManager->ChatSettings->PreEncodeCallbackEnabled = applySoundEffectsToCapturedAudio; // if unset, it defaults to FALSE
    m_chatManager->ChatSettings->PostDecodeCallbackEnabled = applySoundEffectsToChatRenderedAudio; // if unset, it defaults to FALSE

    std::weak_ptr<ChatIntegrationLayer> weakPtrToThis = shared_from_this();

    if( applySoundEffectsToCapturedAudio )
    {
        m_tokenOnPreEncodeAudioBuffer = m_chatManager->OnPreEncodeAudioBuffer += ref new Microsoft::Xbox::GameChat::ProcessAudioBufferHandler( [ weakPtrToThis ](
            __in Windows::Storage::Streams::IBuffer^ preEncodedRawBuffer, 
            __in Windows::Xbox::Chat::IFormat^ audioFormat,
            __in Windows::Foundation::Collections::IVectorView<Microsoft::Xbox::GameChat::ChatUser^>^ chatUsers
            )
        {            
            Windows::Storage::Streams::IBuffer^ rawBufferWithAudioEffects;

            // Using a std::weak_ptr instead of 'this' to avoid dangling pointer if caller class is released.
            // Simply unregistering the callback in the destructor isn't enough to prevent a dangling pointer
            std::shared_ptr<ChatIntegrationLayer> sharedPtrToThis(weakPtrToThis.lock());
            if( sharedPtrToThis != nullptr )
            {
                rawBufferWithAudioEffects = sharedPtrToThis->ApplyPreEncodeChatSoundEffects(
                    preEncodedRawBuffer, 
                    audioFormat, 
                    chatUsers
                    );
            }
            return rawBufferWithAudioEffects;
        });
    }

    if( applySoundEffectsToChatRenderedAudio )
    {
        m_tokenOnPostDecodeAudioBuffer = m_chatManager->OnPostDecodeAudioBuffer += ref new Microsoft::Xbox::GameChat::ProcessAudioBufferHandler( [ weakPtrToThis ](
            __in Windows::Storage::Streams::IBuffer^ postDecodedRawBuffer, 
            __in Windows::Xbox::Chat::IFormat^ audioFormat,
            __in Windows::Foundation::Collections::IVectorView<Microsoft::Xbox::GameChat::ChatUser^>^ chatUsers
            )
        { 
            Windows::Storage::Streams::IBuffer^ rawBufferWithAudioEffects;

            // Using a std::weak_ptr instead of 'this' to avoid dangling pointer if caller class is released.
            // Simply unregistering the callback in the destructor isn't enough to prevent a dangling pointer
            std::shared_ptr<ChatIntegrationLayer> sharedPtrToThis(weakPtrToThis.lock());
            if( sharedPtrToThis != nullptr )
            {
                rawBufferWithAudioEffects = sharedPtrToThis->ApplyPostDecodeChatSoundEffects(
                    postDecodedRawBuffer, 
                    audioFormat, 
                    chatUsers
                    );
            }
            return rawBufferWithAudioEffects;
        });
    }

#ifdef PROFILE
    m_chatManager->ChatSettings->PerformanceCountersEnabled = true;
#endif

    // Upon enter constrained mode, mute everyone.  
    // Upon leaving constrained mode, unmute everyone who was previously muted.
    m_tokenResourceAvailabilityChanged = Windows::ApplicationModel::Core::CoreApplication::ResourceAvailabilityChanged += 
        ref new EventHandler< Platform::Object^ >( [weakPtrToThis] (Platform::Object^, Platform::Object^ )
    {
        // Using a std::weak_ptr instead of 'this' to avoid dangling pointer if caller class is released.
        // Simply unregistering the callback in the destructor isn't enough to prevent a dangling pointer
        std::shared_ptr<ChatIntegrationLayer> sharedPtrToThis(weakPtrToThis.lock());
        if( sharedPtrToThis != nullptr )
        {
            if (Windows::ApplicationModel::Core::CoreApplication::ResourceAvailability == Windows::ApplicationModel::Core::ResourceAvailability::Constrained)
            {
                if( sharedPtrToThis->m_chatManager != nullptr )
                {
                    sharedPtrToThis->m_chatManager->MuteAllUsersFromAllChannels();
                }
            }
            else if(Windows::ApplicationModel::Core::CoreApplication::ResourceAvailability == Windows::ApplicationModel::Core::ResourceAvailability::Full)
            {
                if( sharedPtrToThis->m_chatManager != nullptr )
                {
                    sharedPtrToThis->m_chatManager->UnmuteAllUsersFromAllChannels();

                    // The title should remember who was muted so when the Resume even occurs
                    // to avoid unmuting users who has been previously muted.  Simply re-mute them here
                }
            }
        }
    });

    m_tokenOnDebugMessage = m_chatManager->OnDebugMessage += 
        ref new Windows::Foundation::EventHandler<Microsoft::Xbox::GameChat::DebugMessageEventArgs^>(
            [weakPtrToThis] ( Platform::Object^, Microsoft::Xbox::GameChat::DebugMessageEventArgs^ args )
    {
        // Using a std::weak_ptr instead of 'this' to avoid dangling pointer if caller class is released.
        // Simply unregistering the callback in the destructor isn't enough to prevent a dangling pointer
        std::shared_ptr<ChatIntegrationLayer> sharedPtrToThis(weakPtrToThis.lock());
        if( sharedPtrToThis != nullptr )
        {
            sharedPtrToThis->OnDebugMessageReceived(args);
        }
    });

    m_tokenOnOutgoingChatPacketReady = m_chatManager->OnOutgoingChatPacketReady += 
        ref new Windows::Foundation::EventHandler<Microsoft::Xbox::GameChat::ChatPacketEventArgs^>( 
            [weakPtrToThis] ( Platform::Object^, Microsoft::Xbox::GameChat::ChatPacketEventArgs^ args )
    {
        // Using a std::weak_ptr instead of 'this' to avoid dangling pointer if caller class is released.
        // Simply unregistering the callback in the destructor isn't enough to prevent a dangling pointer
        std::shared_ptr<ChatIntegrationLayer> sharedPtrToThis(weakPtrToThis.lock());
        if( sharedPtrToThis != nullptr )
        {
            sharedPtrToThis->OnOutgoingChatPacketReady(args);
        }
    });

    m_tokenOnCompareUniqueConsoleIdentifiers = m_chatManager->OnCompareUniqueConsoleIdentifiers += 
        ref new Microsoft::Xbox::GameChat::CompareUniqueConsoleIdentifiersHandler( 
            [weakPtrToThis] ( Platform::Object^ obj1, Platform::Object^ obj2 ) 
    { 
        // Using a std::weak_ptr instead of 'this' to avoid dangling pointer if caller class is released.
        // Simply unregistering the callback in the destructor isn't enough to prevent a dangling pointer
        std::shared_ptr<ChatIntegrationLayer> sharedPtrToThis(weakPtrToThis.lock());
        if( sharedPtrToThis != nullptr )
        {
            return sharedPtrToThis->CompareUniqueConsoleIdentifiers(obj1, obj2); 
        }
        else
        {
            return false;
        }
    });

	m_tokenAudioDeviceAdded = User::AudioDeviceAdded += 
		ref new Windows::Foundation::EventHandler<Windows::Xbox::System::AudioDeviceAddedEventArgs^>( 
		[weakPtrToThis] ( Platform::Object^, Windows::Xbox::System::AudioDeviceAddedEventArgs^ args )
	{
		// Using a std::weak_ptr instead of 'this' to avoid dangling pointer if caller class is released.
		// Simply unregistering the callback in the destructor isn't enough to prevent a dangling pointer
		std::shared_ptr<ChatIntegrationLayer> sharedPtrToThis(weakPtrToThis.lock());
		if( sharedPtrToThis != nullptr )
		{
			sharedPtrToThis->OnUserAudioDeviceAdded(args);
		}
	});

	m_tokenAudioDeviceRemoved = User::AudioDeviceRemoved += 
		ref new Windows::Foundation::EventHandler<Windows::Xbox::System::AudioDeviceRemovedEventArgs^>( 
		[weakPtrToThis] ( Platform::Object^, Windows::Xbox::System::AudioDeviceRemovedEventArgs^ args )
	{
		// Using a std::weak_ptr instead of 'this' to avoid dangling pointer if caller class is released.
		// Simply unregistering the callback in the destructor isn't enough to prevent a dangling pointer
		std::shared_ptr<ChatIntegrationLayer> sharedPtrToThis(weakPtrToThis.lock());
		if( sharedPtrToThis != nullptr )
		{
			sharedPtrToThis->OnUserAudioDeviceRemoved(args);
		}
	});

	m_tokenUserMuteStateChanged = Windows::Xbox::Chat::ChatMuteList::UserMuteStateChanged +=
		ref new Windows::Foundation::EventHandler<Windows::Xbox::Chat::UserMuteStateChangedEventArgs^>(
		[weakPtrToThis] (Platform::Object^, Windows::Xbox::Chat::UserMuteStateChangedEventArgs^ args)
	{
		// Using a std::weak_ptr instead of 'this' to avoid dangling pointer if caller class is released.
		// Simply unregistering the callback in the destructor isn't enough to prevent a dangling pointer
		std::shared_ptr<ChatIntegrationLayer> sharedPtrToThis(weakPtrToThis.lock());
		if( sharedPtrToThis != nullptr )
		{
			sharedPtrToThis->OnUserMuteStateChanged(args);
		}
	});
}


void ChatIntegrationLayer::Shutdown()
{
	m_isChatManagerValid = false;
    if( m_chatManager != nullptr )
    {
        m_chatManager->OnDebugMessage -= m_tokenOnDebugMessage;
        m_chatManager->OnOutgoingChatPacketReady -= m_tokenOnOutgoingChatPacketReady;
        m_chatManager->OnCompareUniqueConsoleIdentifiers -= m_tokenOnCompareUniqueConsoleIdentifiers;
        Windows::ApplicationModel::Core::CoreApplication::ResourceAvailabilityChanged -= m_tokenResourceAvailabilityChanged;
        if( m_chatManager->ChatSettings->PreEncodeCallbackEnabled )
        {
            m_chatManager->OnPreEncodeAudioBuffer -= m_tokenOnPreEncodeAudioBuffer;
        }
        if( m_chatManager->ChatSettings->PostDecodeCallbackEnabled )
        {
            m_chatManager->OnPostDecodeAudioBuffer -= m_tokenOnPostDecodeAudioBuffer;
        }
        User::AudioDeviceAdded -= m_tokenAudioDeviceAdded;
        User::AudioDeviceRemoved -= m_tokenAudioDeviceRemoved;
		Windows::Xbox::Chat::ChatMuteList::UserMuteStateChanged -= m_tokenUserMuteStateChanged;
        m_chatManager = nullptr;
    }

	{
		rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);
		if( m_pendingChatUserUpdates != nullptr )
		{
			m_pendingChatUserUpdates->Clear();
			m_pendingChatUserUpdates = nullptr;
		}

		if( m_activeChatUserAsyncOperations != nullptr )
		{
			// Cancel active async operations
			for( unsigned int i = 0; i < m_activeChatUserAsyncOperations->Size; ++i )
			{
				if( m_activeChatUserAsyncOperations->GetAt(i)->Status == Windows::Foundation::AsyncStatus::Started )
				{
					m_activeChatUserAsyncOperations->GetAt(i)->Cancel();
				}
			}

			m_activeChatUserAsyncOperations->Clear();
			m_activeChatUserAsyncOperations = nullptr;
		}
	}

	{
		rage::sysCriticalSection cs(m_chatPacketVectorCriticalSectionToken);
		if( m_pendingOutgoingChatPackets != nullptr )
		{
			m_pendingOutgoingChatPackets->Clear();
			m_pendingOutgoingChatPackets = nullptr;
		}
	}

	{
		rage::sysCriticalSection cs(m_userPacketVectorCriticalSectionToken);
		if( m_pendingOutgoingUserPackets != nullptr )
		{
			m_pendingOutgoingUserPackets->Clear();
			m_pendingOutgoingUserPackets = nullptr;
		}
	}
}

void ChatIntegrationLayer::OnDebugMessageReceived( 
    __in Microsoft::Xbox::GameChat::DebugMessageEventArgs^ args 
    )
{
	args;
#if ENABLE_GAMECHAT_LIB_OUTPUT
	OutputDebugStringW( args->Message->Data() );
	OutputDebugStringW( L"\r\n" );
#endif // ENABLE_GAMECHAT_LIB_OUTPUT
}

void ChatIntegrationLayer::OnOutgoingChatPacketReady( 
    __in Microsoft::Xbox::GameChat::ChatPacketEventArgs^ args 
    )
{
	// Save it to the appropriate vector for later
	
	if (args->ChatMessageType == Microsoft::Xbox::GameChat::ChatMessageType::ChatVoiceDataMessage)
	{
		rage::sysCriticalSection cs(m_chatPacketVectorCriticalSectionToken);
		m_pendingOutgoingChatPackets->Append(args);
	}
	else if (args->ChatMessageType == Microsoft::Xbox::GameChat::ChatMessageType::UserAddedMessage)
	{
		rage::sysCriticalSection cs(m_userPacketVectorCriticalSectionToken);
		m_pendingOutgoingUserPackets->Append(args);
	}
	else if (args->ChatMessageType == Microsoft::Xbox::GameChat::ChatMessageType::UserRemovedMessage)
	{
		rage::sysCriticalSection cs(m_userPacketVectorCriticalSectionToken);
		m_pendingOutgoingUserPackets->Append(args);
	}
}

Windows::Foundation::Collections::IVector<Microsoft::Xbox::GameChat::ChatPacketEventArgs^>^ ChatIntegrationLayer::GetPendingOutgoingChatPackets()
{
	return m_pendingOutgoingChatPackets;
}

Windows::Foundation::Collections::IVector<Microsoft::Xbox::GameChat::ChatPacketEventArgs^>^ ChatIntegrationLayer::GetPendingOutgoingUserPackets()
{
	return m_pendingOutgoingUserPackets;
}

void ChatIntegrationLayer::OnIncomingChatMessage( 
    __in Windows::Storage::Streams::IBuffer^ chatMessage,
    Platform::Object^ uniqueRemoteConsoleIdentifier
    )
{
    if( m_chatManager != nullptr )
    {
        m_chatManager->ProcessIncomingChatMessage(chatMessage, uniqueRemoteConsoleIdentifier);
    }
}

// Only add people who intend to play.
void ChatIntegrationLayer::AddAllLocallySignedInUsersToChatClient(
    __in uint8 channelIndex,
    __in Windows::Foundation::Collections::IVectorView<Windows::Xbox::System::User^>^ locallySignedInUsers
    )
{
    // To integrate the Chat DLL in your game, 
    // add all locally signed in users to the chat client
    for each( Windows::Xbox::System::User^ user in locallySignedInUsers )
    {
        if( user != nullptr )
        {
            AddLocalUserToChatChannel( channelIndex, user );
        }
    }
}

void ChatIntegrationLayer::AddLocalUserToChatChannel(
    __in uint8 channelIndex,
    __in Windows::Xbox::System::IUser^ user
    )
{
	rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);
	if( m_isProcessingChatUserUpdate || (m_pendingChatUserUpdates->Size > 0) )
	{
		if( m_chatManager != nullptr )
		{
			// Queue for later since we are processing
			Microsoft::Xbox::GameChat::ChatUserUpdate^ chatUserUpdate = ref new Microsoft::Xbox::GameChat::ChatUserUpdate();
			chatUserUpdate->UpdateType = Microsoft::Xbox::GameChat::ChatUserUpdateType::CHAT_LOCAL_USER_ADDED;
			chatUserUpdate->ChannelIndex = channelIndex;
			chatUserUpdate->User = user;

			m_pendingChatUserUpdates->Append(chatUserUpdate);
		}
		return;
	}
	
	AddLocalUserToChatChannelInternal(channelIndex, user);
}

void ChatIntegrationLayer::RemoveRemoteConsole( 
    Platform::Object^ uniqueRemoteConsoleIdentifier
    )
{
	rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);
	if( m_isProcessingChatUserUpdate || (m_pendingChatUserUpdates->Size > 0) )
	{
		if( m_chatManager != nullptr )
		{
			// Queue for later since we are processing
			Microsoft::Xbox::GameChat::ChatUserUpdate^ chatUserUpdate = ref new Microsoft::Xbox::GameChat::ChatUserUpdate();
			chatUserUpdate->UpdateType = Microsoft::Xbox::GameChat::ChatUserUpdateType::CHAT_REMOTE_CONSOLE_REMOVED;
			chatUserUpdate->UniqueRemoteConsoleIdentifier = uniqueRemoteConsoleIdentifier;

			m_pendingChatUserUpdates->Append(chatUserUpdate);
		}
		return;
	}

	RemoveRemoteConsoleInternal(uniqueRemoteConsoleIdentifier);
}

void ChatIntegrationLayer::RemoveUserFromChatChannel( 
    __in uint8 channelIndex,
    __in Windows::Xbox::System::IUser^ user 
    )
{
	rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);
	if( m_isProcessingChatUserUpdate || (m_pendingChatUserUpdates->Size > 0) )
	{
		if( m_chatManager != nullptr )
		{
			// Queue for later since we are processing
			Microsoft::Xbox::GameChat::ChatUserUpdate^ chatUserUpdate = ref new Microsoft::Xbox::GameChat::ChatUserUpdate();
			chatUserUpdate->UpdateType = Microsoft::Xbox::GameChat::ChatUserUpdateType::CHAT_LOCAL_USER_REMOVED;
			chatUserUpdate->ChannelIndex = channelIndex;
			chatUserUpdate->User = user;

			m_pendingChatUserUpdates->Append(chatUserUpdate);
		}
		return;
	}

	RemoveUserFromChatChannelInternal(channelIndex, user);
}

void ChatIntegrationLayer::AddLocalUserToChatChannelInternal(
	__in uint8 channelIndex,
	__in Windows::Xbox::System::IUser^ user
	)
{
	// Adds a local user to a specific channel.  

	if( m_chatManager != nullptr )
	{
		auto asyncOp = m_chatManager->AddLocalUserToChatChannelAsync( channelIndex, user );
		m_activeChatUserAsyncOperations->Append(asyncOp);

		m_isProcessingChatUserUpdate = true;

		rage::sysMemAllocator& oldAllocator = rage::sysMemAllocator::GetCurrent();
		rage::sysMemAllocator::SetCurrent(rage::sysMemAllocator::GetMaster());
		concurrency::create_task( asyncOp ).then( [this](concurrency::task<void> t)
		{
			{
				rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);
				Assertf(m_isProcessingChatUserUpdate, "%s", __FUNCTION__);
				if( !concurrency::is_task_cancellation_requested() )
				{
					m_isProcessingChatUserUpdate = false;
				}
			}

			// Error handling
			try
			{
				t.get();                
			}
			catch ( Platform::Exception^ ex )
			{
#if ENABLE_GAMECHAT_LIB_OUTPUT
				OutputDebugStringW( L"ChatIntegrationLayer::AddLocalUserToChatChannelAsync() - Exception\r\n" );
#endif // ENABLE_GAMECHAT_LIB_OUTPUT
			}
		})
		//.wait()
		;
		rage::sysMemAllocator::SetCurrent(oldAllocator);
	}
}

void ChatIntegrationLayer::RemoveRemoteConsoleInternal( 
	Platform::Object^ uniqueRemoteConsoleIdentifier
	)
{
	// uniqueConsoleIdentifier is a Platform::Object^ and can be cast or unboxed to most types. 
	// What exactly you use doesn't matter, but optimally it would be something that uniquely identifies a console on in the session. 
	// A Windows::Xbox::Networking::SecureDeviceAssociation^ is perfect to use if you have access to it.

	// This is how you would convert from an int to a Platform::Object^
	// Platform::Object obj = IntToPlatformObject(5);

	if( m_chatManager != nullptr )
	{
		auto asyncOp = m_chatManager->RemoveRemoteConsoleAsync( uniqueRemoteConsoleIdentifier );
		m_activeChatUserAsyncOperations->Append(asyncOp);

		m_isProcessingChatUserUpdate = true;

		rage::sysMemAllocator& oldAllocator = rage::sysMemAllocator::GetCurrent();
		rage::sysMemAllocator::SetCurrent(rage::sysMemAllocator::GetMaster());
		concurrency::create_task( asyncOp ).then( [this](concurrency::task<void> t)
		{
			{
				rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);
				Assertf(m_isProcessingChatUserUpdate, "%s", __FUNCTION__);
				if( !concurrency::is_task_cancellation_requested() )
				{
					m_isProcessingChatUserUpdate = false;
				}
			}

			// Error handling
			try
			{
				t.get();                
			}
			catch ( Platform::Exception^ ex )
			{
#if ENABLE_GAMECHAT_LIB_OUTPUT
				OutputDebugStringW( L"ChatIntegrationLayer::RemoveRemoteConsoleAsync() - Exception\r\n" );
#endif // ENABLE_GAMECHAT_LIB_OUTPUT
			}
		})
		//.wait()
		;
		rage::sysMemAllocator::SetCurrent(oldAllocator);
	}
}

void ChatIntegrationLayer::RemoveUserFromChatChannelInternal( 
	__in uint8 channelIndex,
	__in Windows::Xbox::System::IUser^ user 
	)
{
	if( m_chatManager != nullptr )
	{
		auto asyncOp = m_chatManager->RemoveLocalUserFromChatChannelAsync( channelIndex, user );
		m_activeChatUserAsyncOperations->Append(asyncOp);

		m_isProcessingChatUserUpdate = true;

		rage::sysMemAllocator& oldAllocator = rage::sysMemAllocator::GetCurrent();
		rage::sysMemAllocator::SetCurrent(rage::sysMemAllocator::GetMaster());
		concurrency::create_task( asyncOp ).then( [this](concurrency::task<void> t)
		{
			{
				rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);
				Assertf(m_isProcessingChatUserUpdate, "%s", __FUNCTION__);
				if( !concurrency::is_task_cancellation_requested() )
				{
					m_isProcessingChatUserUpdate = false;
				}
			}

			// Error handling
			try
			{
				t.get();
			}
			catch ( Platform::Exception^ ex )
			{
#if ENABLE_GAMECHAT_LIB_OUTPUT
				OutputDebugStringW( L"ChatIntegrationLayer::RemoveLocalUserFromChatChannelAsync() - Exception\r\n" );
#endif // ENABLE_GAMECHAT_LIB_OUTPUT
			}
		})
		//.wait()
		;
		rage::sysMemAllocator::SetCurrent(oldAllocator);
	}
}

void ChatIntegrationLayer::ProcessPendingChatUserUpdates()
{
	rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);
	
	if( m_isProcessingChatUserUpdate || (m_pendingChatUserUpdates->Size == 0) )
	{
		return;
	}

	Microsoft::Xbox::GameChat::ChatUserUpdate^ chatUserUpdate = m_pendingChatUserUpdates->GetAt(0);

	switch(chatUserUpdate->UpdateType)
	{
	case Microsoft::Xbox::GameChat::ChatUserUpdateType::CHAT_LOCAL_USER_ADDED:
		AddLocalUserToChatChannelInternal(chatUserUpdate->ChannelIndex, chatUserUpdate->User);
		break;

	case Microsoft::Xbox::GameChat::ChatUserUpdateType::CHAT_LOCAL_USER_REMOVED:
		RemoveUserFromChatChannelInternal(chatUserUpdate->ChannelIndex, chatUserUpdate->User);
		break;

	case Microsoft::Xbox::GameChat::ChatUserUpdateType::CHAT_REMOTE_CONSOLE_REMOVED:
		RemoveRemoteConsoleInternal(chatUserUpdate->UniqueRemoteConsoleIdentifier);
		break;

	default:
		break;
	}

	m_pendingChatUserUpdates->RemoveAt(0);
}

void ChatIntegrationLayer::ProcessActiveAsyncOperations()
{
	rage::sysCriticalSection cs(m_chatUserUpdateCriticalSectionToken);

	for( unsigned int i = 0; i < m_activeChatUserAsyncOperations->Size; )
	{
		if( m_activeChatUserAsyncOperations->GetAt(i)->Status != Windows::Foundation::AsyncStatus::Started )
		{
			m_activeChatUserAsyncOperations->RemoveAt(i);
			continue;
		}

		++i;
	}
}

void ChatIntegrationLayer::OnSecureDeviceAssocationConnectionEstablished( 
    __in Platform::Object^ uniqueConsoleIdentifier 
    )
{
    /// Call this when a new console connects.  
    /// This adds this console to the chat layer
    if( m_chatManager != nullptr )
    {
        m_chatManager->HandleNewRemoteConsole(uniqueConsoleIdentifier );
    }
}

Windows::Foundation::Collections::IVectorView<Microsoft::Xbox::GameChat::ChatUser^>^ ChatIntegrationLayer::GetChatUsers()
{
	if( !m_isChatManagerValid )
	{
		// NOTE: From bugs like B*2002064, it looks like it may be possible for the caller to call this and have the chat
		//        users be destroyed while the caller is processing them. If that's the case, then we may need critical
		//        sections to protect access.
		Assertf( m_isChatManagerValid, "ChatIntegrationLayer::GetChatUsers() - Chat Manager Invalid.");
		return nullptr;
	}

    if( m_chatManager != nullptr )
    {
        return m_chatManager->GetChatUsers();
    }

    return nullptr;
}

bool ChatIntegrationLayer::HasMicFocus()
{
    if( m_chatManager != nullptr )
    {
        return m_chatManager->HasMicFocus;
    }

    return false;
}

float ChatIntegrationLayer::GetMicLoudness()
{
	if( m_chatManager != nullptr )
	{
		return gs_MicLoudness;
	}

	return false;
}

Platform::Object^ ChatIntegrationLayer::IntToPlatformObject( 
    __in int val 
    )
{
    return (Platform::Object^)val;

    // You can also do the same using a PropertyValue.
    //return Windows::Foundation::PropertyValue::CreateInt32(val);
}

int ChatIntegrationLayer::PlatformObjectToInt( 
    __in Platform::Object^ uniqueRemoteConsoleIdentifier 
    )
{
    return safe_cast<int>( uniqueRemoteConsoleIdentifier ); 

    // You can also do the same using a PropertyValue.
    //return safe_cast<Windows::Foundation::IPropertyValue^>(uniqueRemoteConsoleIdentifier)->GetInt32();
}

void ChatIntegrationLayer::HandleChatChannelChanged( 
    __in uint8 oldChatChannelIndex, 
    __in uint8 newChatChannelIndex, 
    __in Microsoft::Xbox::GameChat::ChatUser^ chatUser 
    )
{
    // We remember if the local user was currently muted from all channels. And when we switch channels, 
    // we ensure that the state persists. For remote users, title should implement this themselves
    // based on title game design if they want to persist the muting state.

    bool wasUserMuted = false;
    IUser^ userBeingRemoved = nullptr;

    if (chatUser != nullptr && chatUser->IsLocal)
    {
        wasUserMuted = chatUser->IsMuted;
        userBeingRemoved = chatUser->User;
        if (userBeingRemoved != nullptr)
        {
            RemoveUserFromChatChannel(oldChatChannelIndex, userBeingRemoved);
            AddLocalUserToChatChannel(newChatChannelIndex, userBeingRemoved);
        }
    }

    // If the local user was muted earlier, get the latest chat users and mute him again on the newly added channel.
    if (wasUserMuted && userBeingRemoved != nullptr)
    {
        auto chatUsers = GetChatUsers();
        if (chatUsers != nullptr )
        {
            for (UINT chatUserIndex = 0; chatUserIndex < chatUsers->Size; chatUserIndex++)
            {
                Microsoft::Xbox::GameChat::ChatUser^ chatUser = chatUsers->GetAt(chatUserIndex);
                if( chatUser != nullptr  &&
                    ChatIntegrationLayerUtils::IsStringEqualCaseInsenstive(chatUser->XboxUserId, userBeingRemoved->XboxUserId))
                {
                    m_chatManager->MuteUserFromAllChannels(chatUser);
                    break;
                }
            }
        }
    }
}

void ChatIntegrationLayer::ChangeChatUserMuteState( 
    __in Microsoft::Xbox::GameChat::ChatUser^ chatUser 
    )
{
    /// Helper function to swap the mute state of a specific chat user
    if( m_chatManager != nullptr && chatUser != nullptr)
    {
        if (chatUser->IsMuted)
        {
            m_chatManager->UnmuteUserFromAllChannels(chatUser);
        }
        else
        {
            m_chatManager->MuteUserFromAllChannels(chatUser);
        }
    }
}

Microsoft::Xbox::GameChat::ChatUser^ ChatIntegrationLayer::GetChatUserByXboxUserId( 
    __in Platform::String^ xboxUserId 
    )
{
    Windows::Foundation::Collections::IVectorView<Microsoft::Xbox::GameChat::ChatUser^>^ chatUsers = GetChatUsers();
    for each (Microsoft::Xbox::GameChat::ChatUser^ chatUser in chatUsers)
    {
        if (chatUser != nullptr && 
            ChatIntegrationLayerUtils::IsStringEqualCaseInsenstive(chatUser->XboxUserId, xboxUserId) )
        {
            return chatUser;
        }
    }
    
    return nullptr;
}

bool ChatIntegrationLayer::CompareUniqueConsoleIdentifiers( 
    __in Platform::Object^ uniqueRemoteConsoleIdentifier1, 
    __in Platform::Object^ uniqueRemoteConsoleIdentifier2 
    )
{
    if (uniqueRemoteConsoleIdentifier1 == nullptr || uniqueRemoteConsoleIdentifier2 == nullptr)
    {
        return false;
    }

	unsigned long long uniqueId1 = safe_cast<Windows::Foundation::IPropertyValue^>(uniqueRemoteConsoleIdentifier1)->GetUInt64();
	unsigned long long uniqueId2 = safe_cast<Windows::Foundation::IPropertyValue^>(uniqueRemoteConsoleIdentifier2)->GetUInt64();
	return uniqueId1 == uniqueId2;
}

bool ChatIntegrationLayer::AreSecureDeviceAddressesEqual( 
    Windows::Xbox::Networking::SecureDeviceAssociation^ secureDeviceAssociation1, 
    Windows::Xbox::Networking::SecureDeviceAssociation^ secureDeviceAssociation2
    )
{
    if( secureDeviceAssociation1 != nullptr && 
        secureDeviceAssociation2 != nullptr && 
        secureDeviceAssociation1->RemoteSecureDeviceAddress != nullptr &&
        secureDeviceAssociation2->RemoteSecureDeviceAddress != nullptr &&
        secureDeviceAssociation1->RemoteSecureDeviceAddress->Compare(secureDeviceAssociation2->RemoteSecureDeviceAddress) == 0 )
    {
        return true;
    }

    return false;
}

Windows::Storage::Streams::IBuffer^ ChatIntegrationLayer::ApplyPreEncodeChatSoundEffects(
    __in Windows::Storage::Streams::IBuffer^ preEncodedRawBuffer, 
    __in Windows::Xbox::Chat::IFormat^ audioFormat,
    __in Windows::Foundation::Collections::IVectorView<Microsoft::Xbox::GameChat::ChatUser^>^ chatUsers
    )
{
    // audioFormat->Subtype will be either KSDATAFORMAT_SUBTYPE_PCM (samples are int) or KSDATAFORMAT_SUBTYPE_IEEE_FLOAT (samples are float)
    Assert( audioFormat->Subtype == Platform::Guid(KSDATAFORMAT_SUBTYPE_IEEE_FLOAT) );
    Assert( audioFormat->BitsPerSample == 32 ); 
    Assert( audioFormat->ChannelCount == 1 ); 

	// 100ms @ 24kHz
	enum { rmsWindowLength = 2400 };
	const float oneOverRmsWindowLength = 1.f / rmsWindowLength;

    byte* srcAudioBufferBytes = nullptr;
    ChatIntegrationLayerUtils::GetBufferBytes(preEncodedRawBuffer, &srcAudioBufferBytes);

	const float *samples = reinterpret_cast<const float*>(srcAudioBufferBytes); // each sample is a 32-bit float in [0.0f, 1.0f]
	const unsigned numSamples = preEncodedRawBuffer->Length >> 2; // 32 bits (4 bytes) per sample 

    for(unsigned i = 0; i < numSamples; i++)
    {
		const float sampleVal = (float)(*samples++);
		const float sampleSquared = sampleVal * sampleVal;
		gs_MicLoudnessMean = (((rmsWindowLength-1) * gs_MicLoudnessMean) + sampleSquared) * oneOverRmsWindowLength;
    }

	// [0, 1] metric of the sustained volume coming through the microphone
	gs_MicLoudness = rage::Min(1.0f, 5.f * rage::Sqrtf(gs_MicLoudnessMean));

	// No processing needed
    return preEncodedRawBuffer;
}

Windows::Storage::Streams::IBuffer^ ChatIntegrationLayer::ApplyPostDecodeChatSoundEffects( 
    __in Windows::Storage::Streams::IBuffer^ postDecodedRawBuffer, 
    __in Windows::Xbox::Chat::IFormat^ audioFormat, 
    __in Windows::Foundation::Collections::IVectorView<Microsoft::Xbox::GameChat::ChatUser^>^ chatUsers
    )
{
    // TODO: something useful with postDecodedRawBuffer based on audioFormat and chatUsers
    // This is an trivial sound effect to test the ChatSettings->PostDecodeCallbackEnabled feature.  
    // This sound effect amplifies the volume and causes it to clip.  It does not sound good nor is it high performance.
    
    // audioFormat->Subtype will be either KSDATAFORMAT_SUBTYPE_PCM (samples are int) or KSDATAFORMAT_SUBTYPE_IEEE_FLOAT (samples are float)
    Assert( audioFormat->Subtype == Platform::Guid(KSDATAFORMAT_SUBTYPE_PCM) );
    Assert( audioFormat->BitsPerSample == 16 ); 
    Assert( audioFormat->ChannelCount == 1 ); 

    // For better performance, use a pre-allocated buffer or operate on the incoming buffer and return it.
    Windows::Storage::Streams::IBuffer^ rawBufferWithAudioEffects = ref new Windows::Storage::Streams::Buffer(postDecodedRawBuffer->Length);
    rawBufferWithAudioEffects->Length = postDecodedRawBuffer->Length;

    byte* destAudioBufferBytes = nullptr;
    ChatIntegrationLayerUtils::GetBufferBytes( rawBufferWithAudioEffects, &destAudioBufferBytes );
    byte* srcAudioBufferBytes = nullptr;
    ChatIntegrationLayerUtils::GetBufferBytes( postDecodedRawBuffer, &srcAudioBufferBytes );

    USHORT* srcAudioSampleArray = (USHORT*)srcAudioBufferBytes;
    USHORT* destAudioSampleArray = (USHORT*)destAudioBufferBytes;
    int numberOfSamples = postDecodedRawBuffer->Length / sizeof(USHORT);
    for( int i=0; i<numberOfSamples; i++ )
    {
        USHORT originalAudioSample = *srcAudioSampleArray;
        float newAudioSampleFloat = (float)(originalAudioSample * 1.5f); // avoid casting for better performance
        USHORT newAudioSample = (USHORT)min(newAudioSampleFloat, USHORT_MAX);
        *destAudioSampleArray = newAudioSample;
        srcAudioSampleArray++;
        destAudioSampleArray++;
    }

    return rawBufferWithAudioEffects;
}

void ChatIntegrationLayer::OnUserAudioDeviceAdded( 
    AudioDeviceAddedEventArgs^ eventArgs 
    )
{
    Assert(eventArgs != nullptr);

    if(m_OnUserAudioDeviceAddedCB)
		m_OnUserAudioDeviceAddedCB(eventArgs);
}

void ChatIntegrationLayer::OnUserAudioDeviceRemoved( 
	AudioDeviceRemovedEventArgs ^ eventArgs 
	)
{
	Assert(eventArgs != nullptr);

	if(m_OnUserAudioDeviceRemovedCB)
		m_OnUserAudioDeviceRemovedCB(eventArgs);
}

void ChatIntegrationLayer::OnUserMuteStateChanged( 
	Windows::Xbox::Chat::UserMuteStateChangedEventArgs ^ eventArgs 
	)
{
	Assert(eventArgs != nullptr);
	Displayf("ChatIntegrationLayer::OnUserMuteStateChanged");

	if(m_OnUserMuteStateChangedCB)
		m_OnUserMuteStateChangedCB(eventArgs);
}

void ChatIntegrationLayer::OnUserRemoved(
    UserRemovedEventArgs^ eventArgs
    )
{
    Assert(eventArgs != nullptr);

    User^ user = eventArgs->User;
    if( user != nullptr )
    {
        RemoveUserFromChatChannel( 
			0, //g_sampleInstance->GetUISelectionChatChannelIndex(),
            user 
            );
    }
}

void ChatIntegrationLayer::OnControllerPairingChanged( Windows::Xbox::Input::ControllerPairingChangedEventArgs^ args )
{
	auto controller = args->Controller;
	if ( controller )
	{
		if ( controller->Type == L"Windows.Xbox.Input.Gamepad" )
		{
			// Either add the user or sign one in
			User^ user = args->User;
			if ( user != nullptr )
			{
				AddLocalUserToChatChannel(
					0, //g_sampleInstance->GetUISelectionChatChannelIndex(),
					user
					);
			}
		}
	}

}

#endif //RSG_DURANGO
