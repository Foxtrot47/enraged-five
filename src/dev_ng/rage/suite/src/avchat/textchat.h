// 
// textchat.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNET_TEXTCHAT_H
#define SNET_TEXTCHAT_H

#if __WIN32PC

#include "atl/delegate.h"
#include "file/file_config.h"
#include "net/connectionmanager.h"
#include "rline/rlgamerinfo.h"
#include "net/message.h"

/*
    This module provides network text chat features.

    To use text chat initialize an instance of TextChat with
    the expected number of local and remote typers, an instance
    of a connection manager that will be used for network communication,
    and the id of the network channel to use for text communication.
*/

namespace rage
{

static const u32 MAX_TEXT_MESSAGE_LEN = 256;

struct CMsgTextMessage
{
	NET_MESSAGE_DECL(CMsgTextMessage, CMSG_TEXT_MESSAGE);

	CMsgTextMessage()
	{
		m_Text[0] = '\0';
		m_TeamOnly = false;
	}

	CMsgTextMessage(const rlGamerId& gamerId, const char* text, bool teamOnly)
	{
		strncpy(m_Text, text, sizeof(m_Text));
		m_Text[MAX_TEXT_MESSAGE_LEN - 1] = '\0';
		Assert(strlen(m_Text) > 0);
		m_GamerId = gamerId;
		m_TeamOnly = teamOnly;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerStr(msg.m_Text, MAX_TEXT_MESSAGE_LEN) && bb.SerUser(msg.m_GamerId) && bb.SerBool(msg.m_TeamOnly);
	}

	char m_Text[MAX_TEXT_MESSAGE_LEN];
	rlGamerId m_GamerId;
	bool m_TeamOnly;
};

struct CMsgPlayerIsTyping
{
	NET_MESSAGE_DECL(CMsgPlayerIsTyping, CMSG_PLAYER_IS_TYPING);

	CMsgPlayerIsTyping()
	{

	}

	CMsgPlayerIsTyping(const rlGamerId& gamerId)
	{
		m_GamerId = gamerId;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerUser(msg.m_GamerId);
	}

	rlGamerId m_GamerId;
};

class TextChat
{
	typedef atDelegator<void (TextChat* textChat, const rlGamerHandle& gamerHandle, const char* text, bool teamOnly)> Delegator;
public:

    enum
    {
        MAX_LOCAL_TYPERS   = RL_MAX_LOCAL_GAMERS,
        MAX_REMOTE_TYPERS  = 31
    };

	enum
	{
		SEND_TO_TEAM = -1,
		SEND_TO_EVERYONE = -2
	};

	enum
	{
		MAX_TYPERS = MAX_LOCAL_TYPERS + MAX_REMOTE_TYPERS,
	};

	//PURPOSE
	//  Text chat delegate type.
	//  The signature for event handlers is:
	//  void OnEvent(TextChat* textChat, const rlGamerHandle& gamerHandle, const char* text)
	//
	typedef Delegator::Delegate Delegate;

    TextChat();

    ~TextChat();

    //PURPOSE
    //  Initialize the text chat system.
    //PARAMS
    //  maxLocalTypers     - Maximum number of local typers.
    //  maxRemoteTypers    - Maximum number of remote typers.
    //  cxnMgr              - Connection manager instance.
    //  channelId           - Id of network channel used for text.
	//	transitionChannelId	- Id of network channel used for text during transitions.
    bool Init(const unsigned maxLocalTypers,
			  const unsigned maxRemoteTypers,
			  netConnectionManager* cxnMgr,
			  const unsigned channelId,
			  const unsigned transitionChannelId);

    //PURPOSE
    //  Shuts down the text chat system.
    void Shutdown();

    //PURPOSE
    //  Returns true if text chat is initialized.
    bool IsInitialized() const;

	//PURPOSE
	//  Adds a delegate that will be called when text messages are received.
	void AddDelegate(Delegate* dlgt);

	//PURPOSE
	//  Removes a delegate.
	void RemoveDelegate(Delegate* dlgt);

    //PURPOSE
    //  Adds a typer to the current group of typers.
    //PARAMS
    //  gamerInfo   - Gamer info for new typer.  The gamer info
    //                can represent either a local or a remote gamer.
    //  endpointId  - EndpointId of the gamer.  Ignored
    //                if the gamer is local.
    bool AddTyper(const rlGamerInfo& gamerInfo,
				  const EndpointId endpointId);

	//PURPOSE
	//  Sets whether the specified remote typer is on the same team as the local typer.
	//PARAMS
	//  endpointId  - EndpointId of the gamer.  Ignored
	//                if the gamer is local.
	void SetOnSameTeam(const EndpointId endpointId,
					   bool onSameTeem);

    //PURPOSE
    //  Removes a typer from the current group of typers.
    bool RemoveTyper(const rlGamerId& gamerId);

	//PURPOSE
	//  Removes all remote typers.
	void RemoveAllRemoteTypers();

    //PURPOSE
    //  Removes all typers.
    void RemoveAllTypers();

    //PURPOSE
    //  Returns true if the given gamer is among the current group
    //  of typers.
    bool HaveTyper(const rlGamerId& gamerId) const;
	bool HaveTyper(const EndpointId endpointId) const;

	//PURPOSE
	//  Sets the recipients of text chat messages
	void SetTextChatRecipients(int recipients);

	//PURPOSE
    //  Returns true if the given gamer is currently typing.
    bool IsTyping(const rlGamerId& gamerId) const;

	//PURPOSE
	//  Tells the text chat system that the specified local gamer is typing.
	bool SetLocalPlayerIsTyping(const rlGamerId& gamerId);

    //PURPOSE
    //  Returns true if any gamer is currently typing.
    bool IsAnyTyping() const;

    //PURPOSE
    //  (PC Only) Returns true if the typer with the given gamer handle is muted
    //  (blocked) for the local gamer.
    //PARAMS
    //  remoteGamerHandle   - Handle of gamer for whom the mute state is
    //                        desired.
    //NOTES
    //  This function uses the gamer handle instead of the gamer id so
    //  it can be used to check the mute state for gamers that are not
    //  registered with text chat.
    bool IsMuted(const rlGamerHandle& remoteGamerHandle) const;

    //PURPOSE
    //  Returns true if the gamer at the local index has chat
    //  privileges.
    //PARAMS
    //  localGamerIndex     - Index of local gamer doing the query.
    bool HasTextChatPrivileges(const int localGamerIndex) const;
	
	//PURPOSE
	//  Returns true if the local gamer is prohibited from sending or receiving text communication
	//PARAMS
	//  localGamerIndex     - Index of local gamer doing the query.
	bool IsTextChatProhibited(const int localGamerIndex) const;

	//PURPOSE
    //  Returns true if the local gamer has a keyboard.
    //NOTES
    //  Can be used to check for the existence of a keyboard for
	//  local gamers that are not registered with text chat.
    bool HasKeyboard(const int localGamerIndex) const;
	
	//PURPOSE
	//  Set whether text chat is in a network transition state or not.
	void SetInTransition(bool inTransition);

	//PURPOSE
	//  Queues a single string of text to be sent from a local player
	bool SubmitText(const rlGamerId& gamerId,
					const char* text,
					bool teamOnly);

    //PURPOSE
    //  Updates the text chat system.
    void Update();

private:

    struct Typer;
    struct Peer;

    unsigned RemoteTyperCount() const;

    int GetTyperIndex(const rlGamerId& gamerId) const;
    Typer* FindTyperByGamerId(const rlGamerId& gamerId);
    const Typer* FindTyperByGamerId(const rlGamerId& gamerId) const;

    int GetPeerIndex(const EndpointId endpointId) const;
    Peer* FindPeerByEndpointId(const EndpointId endpointId);
    const Peer* FindPeerByEndpointId(const EndpointId endpointId) const;

    //Network event handler.
    void OnNetEvent(netConnectionManager* cxnMgr, const netEvent* evt);

	void SetRemotePlayerIsTyping(const netEventFrameReceived* fr);
    void ReceiveText(const netEventFrameReceived* fr);
    void SendMessages();

	Delegator m_Delegator;

    netConnectionManager::Delegate m_CxnMgrDlgt;
	netConnectionManager::Delegate m_CxnMgrTransitionDlgt;

	char m_TextToSend[MAX_TEXT_MESSAGE_LEN];
	int m_TextChatRecipients;

	//Typer represents both local and remote typers.
    struct Typer
    {
        Typer()
            : m_Peer(NULL)
            , m_Next(NULL)
            , m_IsActive(false)
			, m_IsTypingCountdown(0)
        {
        }

        void Init(const rlGamerInfo& gamerInfo);
        void Shutdown();

        rlGamerInfo m_GamerInfo;

        Peer* m_Peer;

        Typer* m_Next;

        //If positive, indicates the typer is typing.
        int m_IsTypingCountdown;
        bool m_IsActive : 1;
    };

    //Peer represents remote peers.  Each peer can have one or more typers.
    struct Peer
    {
        Peer()
            : m_NumTypers(0)
            , m_Next(NULL)
			, m_ConnectionId(-1)
			, m_EndpointId(NET_INVALID_ENDPOINT_ID)
            , m_HaveTextChatPrivs(true)
            , m_Muted(false)
			, m_SentIsTypingMessageCountdown(0)
			, m_OnSameTeamAsLocalPlayer(false)
        {
            for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
            {
                m_Typers[i] = NULL;
            }
        }

        void Init(const EndpointId endpointId)
        {
            Assert(!m_Next);
            Assert(!m_NumTypers);

#if __ASSERT
            for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
            {
                Assert(!m_Typers[i]);
            }
#endif  //__ASSERT
			
			m_ConnectionId = -1;
            m_EndpointId = endpointId;
            m_HaveTextChatPrivs = true;
            m_Muted = false;
			m_SentIsTypingMessageCountdown = 0;
			m_OnSameTeamAsLocalPlayer = false;
        }

        void Shutdown()
        {
            Assert(!m_Next);
            Assert(!m_NumTypers);

#if __ASSERT
            for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
            {
                Assert(!m_Typers[i]);
            }
#endif  //__ASSERT

			m_ConnectionId = -1;
			m_EndpointId = NET_INVALID_ENDPOINT_ID;
            m_HaveTextChatPrivs = true;
            m_Muted = false;
			m_SentIsTypingMessageCountdown = 0;
			m_OnSameTeamAsLocalPlayer = false;
        }

        void AddTyper(Typer* typer)
        {
            Assert(!typer->m_Peer);

            if(AssertVerify(m_NumTypers < RL_MAX_LOCAL_GAMERS))
            {
                m_Typers[m_NumTypers++] = typer;
                typer->m_Peer = this;
            }
        }

        void RemoveTyper(Typer* typer)
        {
            Assert(this == typer->m_Peer);
            Assert(m_NumTypers > 0);

            for(int i = 0; i < (int) m_NumTypers; ++i)
            {
                if(typer == m_Typers[i])
                {
                    m_Typers[i] = m_Typers[m_NumTypers-1];
                    m_Typers[m_NumTypers-1] = NULL;
                    --m_NumTypers;
                    typer->m_Peer = NULL;
                    break;
                }
            }
        }

		void SetOnSameTeam(bool onSameTeam)
		{
			m_OnSameTeamAsLocalPlayer = onSameTeam;
		}

		int m_ConnectionId;
		EndpointId m_EndpointId;
        Typer* m_Typers[RL_MAX_LOCAL_GAMERS];
        unsigned m_NumTypers;
		int m_SentIsTypingMessageCountdown;
		bool m_OnSameTeamAsLocalPlayer;
		Peer* m_Next;

        //True if all local gamers have privileges to text chat with
        //all remote gamers on the peer.
        bool m_HaveTextChatPrivs    : 1;

        //True if any local gamer has muted any remote gamer
        //that is local to the peer.
        bool m_Muted            : 1;
    };

    Typer m_TyperPile[MAX_TYPERS];
    Typer* m_TyperPool;
    Typer* m_Typers[MAX_TYPERS];
    unsigned m_NumTypers;

    //Local typers indexed by local gamer index.
    Typer* m_LocalTypers[RL_MAX_LOCAL_GAMERS];

    Peer m_PeerPile[MAX_TYPERS];
    Peer* m_PeerPool;
    Peer* m_Peers[MAX_TYPERS];
    unsigned m_NumPeers;

    unsigned m_MaxLocalTypers;
    unsigned m_MaxRemoteTypers;
    unsigned m_NumLocalTypers;

    netConnectionManager* m_CxnMgr;
    unsigned m_ChannelId;
	unsigned m_TransitionChanelId;
	unsigned m_LastUpdateTime;
	sysCriticalSectionToken m_CsTypers;

	bool m_InTransition : 1;
	bool m_Initialized  : 1;
};

}   //namespace rage

#endif // __WIN32PC
#endif // SNET_TEXTCHAT_H