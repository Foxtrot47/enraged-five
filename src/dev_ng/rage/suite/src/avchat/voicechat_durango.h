// 
// avchat/voicechat_durango.h 
// 
// Copyright (C) 2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNET_AVCHAT_DURANGO_H
#define SNET_AVCHAT_DURANGO_H

#if RSG_DURANGO

#include <xdk.h>

#include "voicechattypes.h"
#include "rline/rl.h"
#include "rline/durango/rlxbl_interface.h"
#include "atl/map.h"
#include "atl/singleton.h"

namespace rage
{
	class netConnectionManager;
	class netEventFrameReceived;
	class rlGamerInfo;

	enum eChatEventType
	{
		RLXBL_CHAT_EVENT_USER_MUTE_STATE_CHANGED = 0,
	};

#define RLXBL_CHAT_EVENT_COMMON_DECL(name)\
	static unsigned EVENT_ID() {return name::GetAutoId();}\
	virtual unsigned GetId() const {return name::GetAutoId();}

#define RLXBL_CHAT_EVENT_DECL(name, id)\
	AUTOID_DECL_ID(name, rlXblChatEvent, id)\
	RLXBL_EVENT_COMMON_DECL(name)

	class rlXblChatEvent
	{
	public:
		AUTOID_DECL_ROOT(rlXblChatEvent);
		RLXBL_CHAT_EVENT_COMMON_DECL(rlXblChatEvent);

		rlXblChatEvent() {}
		virtual ~rlXblChatEvent() {}
	};

	typedef atDelegator<void (const class rlXblChatEvent*)> rlXblChatEventDelegator;
	typedef rlXblChatEventDelegator::Delegate rlXblChatEventDelegate;

	//PURPOSE
	//  Dispatched when the mute state of a user changes
	class rlXblChatEventUserMuteStateChanged : public rlXblChatEvent
	{
	public:
		RLXBL_CHAT_EVENT_DECL(rlXblChatEventUserMuteStateChanged, RLXBL_CHAT_EVENT_USER_MUTE_STATE_CHANGED);

		rlXblChatEventUserMuteStateChanged(int localGamerIndex, bool bIsMuted, const u64 recipientXUID)
			: m_LocalGamerIndex(localGamerIndex)
			, m_bIsMuted(bIsMuted)
			, m_RecipientXUID(recipientXUID)
		{
		}

		int GetLocalGamerIndex() const { return m_LocalGamerIndex; }
		bool IsMuted() const { return m_bIsMuted; }
		u64 GetRecipientXUID() const { return m_RecipientXUID; }

	private:
		int m_LocalGamerIndex;
		bool m_bIsMuted : 1;
		u64 m_RecipientXUID;
	};

	class VoiceChatDurango
	{
	public:

		~VoiceChatDurango();

		bool Init();
		void Shutdown();

		void RestartChatManager();

		void Update();

		void RefreshHeadsetState();
		void FlagHeadsetRefresh(int localIndex);

		bool AddLocalChatUser(int localIndex);
		void RemoveLocalChatUser(int localIndex); 

		bool AddRemoteChatUser(const rlGamerInfo& gamerInfo, const EndpointId endpointId, bool shouldHandleNewConnection);
		bool RemoveRemoteChatUser(const rlGamerHandle& gamerHandle);
		bool SetChatUserAsPendingRemove(const rlGamerHandle& gamerHandle);
		void RemoveAllChatUsers();

		bool MicHasFocus() const;
		float GetMicLoudness() const;
		bool HasHeadset(int localIndex) const { return m_bHasHeadset[localIndex]; }
		bool HasSharedAudioDevice(int localIndex) const { return m_bHasSharedAudioDevice[localIndex]; }

		void SetHasHeadset(int localIndex, bool hasHeadset);
		void SetHasSharedAudioDevice(int localIndex, bool hasSharedAudio);

		bool IsTalking(u64 uniqueId) const;
		bool IsBlocked(u64 uniqueId) const;
		bool IsMuted(u64 uniqueId) const;

		void SendUserPackets(u64 userId);

		bool PackChat(u64 uniqueTalkerId, void* dst, unsigned* numBytes); 
		void SubmitChat(u64 remoteXuid, const void* data, const unsigned sizeOfData);

		void ReceiveMessage(const netEventFrameReceived* fr);

		void SetConnectionManager(netConnectionManager* cxnMgr) { m_CxnMgr = cxnMgr; }
		netConnectionManager* GetConnectionManager() const { return m_CxnMgr; }

		void AddDelegate(rlXblChatEventDelegate* dlgt);
		void RemoveDelegate(rlXblChatEventDelegate* dlgt);
		void DispatchEvent(const rlXblChatEvent* e);

	private:

		struct ChatUserInfo
		{
			friend class VoiceChatDurango;

			static const u64 INVALID_UNIQUE_ID = 0;

		public:
			ChatUserInfo()						{ Clear(); }
			bool IsValid() const				{ return m_UniqueId != INVALID_UNIQUE_ID; }
			bool IsUniqueIdMatch(u64 id) const	{ return IsValid() && (m_UniqueId == id); }
			void Set(u64 id)					{ m_UniqueId = id; }
			void Clear()										
			{
				m_UniqueId = INVALID_UNIQUE_ID; 
				m_IsTalking = false;
				m_IsMuted = false;
				m_IsBlocked = false;
#if !__NO_OUTPUT
				m_MuteTrack = false;
				m_RestrictionTrack = 0xFFFFFFFF;
#endif
			}

			const u64 GetUniqueId() const						{ return m_UniqueId; }

		private:
			
			u64 m_UniqueId;
			bool m_IsTalking;
			bool m_IsMuted;
			bool m_IsBlocked;
#if !__NO_OUTPUT
			bool m_MuteTrack;
			unsigned m_RestrictionTrack;
#endif
		};

		struct RemoteChatUserInfo : public ChatUserInfo
		{
			friend class VoiceChatDurango;

		public:
			RemoteChatUserInfo() : ChatUserInfo()				{ Clear(); }
			void Set(const EndpointId endpointId, u64 id)		{ m_EndpointId = endpointId; ChatUserInfo::Set(id); }
			const EndpointId GetEndpointId() const				{ return m_EndpointId; }
			void Clear()										
			{
				ChatUserInfo::Clear();
				m_EndpointId = NET_INVALID_ENDPOINT_ID; 
				m_PendingRemove = false;
			}

			void SetPendingRemove(bool bPendingRemove)			{ m_PendingRemove = bPendingRemove; }
			bool IsPendingRemove() const						{ return m_PendingRemove; }

		private:
			EndpointId m_EndpointId;
			bool m_PendingRemove;
		};

		bool AddLocalParticipant(int localIndex);
		bool AddRemoteParticipant(u64 uniqueId);
		void RemoveLocalParticipant(int localIndex);
		void RemoveRemoteParticipant(u64 uniqueId);

		static void OnXblEvent(rlXbl* xbl, const rlXblEvent* evt);

		void RefreshHeadset(int localIndex);
		bool UpdateChatState(ChatUserInfo* userInfo);

		bool m_bHasHeadset[RL_MAX_LOCAL_GAMERS];
		bool m_bHasSharedAudioDevice[RL_MAX_LOCAL_GAMERS];
		unsigned m_HeadsetRefreshMask;

		ChatUserInfo m_LocalInfo; 
		static const unsigned MAX_REMOTE_GAMERS = (RL_MAX_GAMERS_PER_SESSION - 1) + (RL_MAX_GAMERS_PER_SESSION - 1);
		RemoteChatUserInfo m_RemoteChatUserInfos[MAX_REMOTE_GAMERS];

		netConnectionManager* m_CxnMgr;

		rlXblChatEventDelegator m_Delegator;
		mutable sysCriticalSectionToken m_Cs;

		rlXblEventDelegator::Delegate m_XblDlgt;
		
		atMap<u64, u32> m_InitialUserPacketSendAttemptTime;
	};

	typedef atSingleton<VoiceChatDurango> VoiceChatDurangoSingleton;
}   //namespace rage

#endif //RSG_DURANGO

#endif  //SNET_AVCHAT_DURANGO_H
