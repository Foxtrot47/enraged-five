// 
// avchat/voicechattypes.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNET_AVCHAT_TYPES_H
#define SNET_AVCHAT_TYPES_H

#include "rline/rl.h"

namespace rage
{

class VoiceChatTypes
{
public:

    enum
    {
        MAX_LOCAL_TALKERS   = RL_MAX_LOCAL_GAMERS,
        MAX_REMOTE_TALKERS  = RL_MAX_GAMERS_PER_SESSION - 1
    };

    enum
    {
        MAX_TALKERS = MAX_LOCAL_TALKERS + MAX_REMOTE_TALKERS,
    };
};

}   //namespace rage

#endif  //SNET_AVCHAT_TYPES_H
