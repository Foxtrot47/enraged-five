// 
// sample_snet/textchat.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if __WIN32PC

#include "textchat.h"

#include "diag/seh.h"
#include "net/connectionmanager.h"
#include "net/message.h"
#include "rline/rlpc.h"
#include "rline/rlpresence.h"
#include "system/param.h"
#include "system/timer.h"

namespace rage
{

#if !__NO_OUTPUT

RAGE_DEFINE_SUBCHANNEL(ragenet, textchat)

#define tcDebug(fmt, ...)   RAGE_DEBUGF1(ragenet_textchat, fmt, ##__VA_ARGS__)
#define tcWarning(fmt, ...) RAGE_WARNINGF(ragenet_textchat, fmt, ##__VA_ARGS__)
#define tcError(fmt, ...)   RAGE_ERRORF(ragenet_textchat, fmt, ##__VA_ARGS__)

#else

#define tcDebug(a, ...)
#define tcWarning(a, ...)
#define tcError(a, ...)

#endif  //__NO_OUTPUT

///////////////////////////////////////////////////////////////////////////////
//  TextChat
///////////////////////////////////////////////////////////////////////////////

NET_MESSAGE_IMPL(CMsgTextMessage);
NET_MESSAGE_IMPL(CMsgPlayerIsTyping);

static const int IS_TYPING_COUNTDOWN = 2000;
static const int IS_TYPING_MSG_FREQ = 1000;

TextChat::TextChat()
: m_TyperPool(NULL)
, m_NumTypers(0)
, m_PeerPool(NULL)
, m_NumPeers(0)
, m_MaxLocalTypers(0)
, m_MaxRemoteTypers(0)
, m_NumLocalTypers(0)
, m_CxnMgr(0)
, m_ChannelId(NET_INVALID_CHANNEL_ID)
, m_TransitionChanelId(NET_INVALID_CHANNEL_ID)
, m_LastUpdateTime(0)
, m_InTransition(false)
, m_Initialized(false)
, m_TextChatRecipients(SEND_TO_EVERYONE)
{
    sysMemSet(m_LocalTypers, 0, sizeof(m_LocalTypers));

    m_CxnMgrDlgt.Bind(this, &TextChat::OnNetEvent);
	m_CxnMgrTransitionDlgt.Bind(this, &TextChat::OnNetEvent);

    //Initialize the pool of typers and peers
    for(int i = 0; i < MAX_TYPERS; ++i)
    {
        m_TyperPile[i].m_Next = m_TyperPool;
        m_TyperPool = &m_TyperPile[i];
        m_Typers[i] = NULL;

        m_PeerPile[i].m_Next = m_PeerPool;
        m_PeerPool = &m_PeerPile[i];
        m_Peers[i] = NULL;
    }
}

TextChat::~TextChat()
{
    this->Shutdown();
}

bool
TextChat::Init(const unsigned maxLocalTypers,
                const unsigned maxRemoteTypers,
                netConnectionManager* cxnMgr,
                const unsigned channelId,
				const unsigned transitionChannelId)
{
    Assert(maxLocalTypers <= MAX_LOCAL_TYPERS);
    Assert(maxRemoteTypers <= MAX_REMOTE_TYPERS);

    bool success = false;

    if(AssertVerify(!m_Initialized))
    {
        Assert(!m_NumTypers);
        Assert(!m_NumPeers);
        Assert(!m_NumLocalTypers);

        m_MaxLocalTypers = maxLocalTypers;
        m_MaxRemoteTypers = maxRemoteTypers;
        m_CxnMgr = cxnMgr;
        m_ChannelId = channelId;
		m_TransitionChanelId = transitionChannelId;
		m_LastUpdateTime = 0;
		m_TextChatRecipients = SEND_TO_EVERYONE;

#if __ASSERT
		for(int i = 0; i < MAX_TYPERS; ++i)
        {
            Assert(!m_Typers[i]);
            Assert(!m_Peers[i]);
        }
#endif  //__ASSERT

        //Start listening for network events.
        m_CxnMgr->AddChannelDelegate(&m_CxnMgrDlgt, m_ChannelId);
		m_CxnMgr->AddChannelDelegate(&m_CxnMgrTransitionDlgt, m_TransitionChanelId);

        success = m_Initialized = true;
    }

    return success;
}

void
TextChat::Shutdown()
{
    if(m_Initialized)
    {
		m_Delegator.Clear();
		m_CxnMgr->RemoveChannelDelegate(&m_CxnMgrTransitionDlgt);
		m_CxnMgr->RemoveChannelDelegate(&m_CxnMgrDlgt);

        //Remove any remaining typers.
        this->RemoveAllTypers();

        Assert(0 == m_NumTypers);
        Assert(0 == m_NumLocalTypers);

        m_CxnMgr = NULL;
        m_ChannelId = NET_INVALID_CHANNEL_ID;
		m_TransitionChanelId = NET_INVALID_CHANNEL_ID;
		m_LastUpdateTime = 0;
		m_TextChatRecipients = SEND_TO_EVERYONE;
        m_MaxLocalTypers = m_MaxRemoteTypers = 0;

#if __ASSERT
        for(int i = 0; i < MAX_TYPERS; ++i)
        {
            Assert(!m_Typers[i]);
            Assert(!m_Peers[i]);
        }
#endif  //__ASSERT

		m_InTransition = false;
        m_Initialized = false;
    }
}

bool
TextChat::IsInitialized() const
{
    return m_Initialized;
}

void
TextChat::AddDelegate(Delegate* dlgt)
{
	m_Delegator.AddDelegate(dlgt);
}

void
TextChat::RemoveDelegate(Delegate* dlgt)
{
	m_Delegator.RemoveDelegate(dlgt);
}

bool
TextChat::AddTyper(const rlGamerInfo& gamerInfo,
                    const EndpointId endpointId)
{
    SYS_CS_SYNC(m_CsTypers);

    Assert(m_Initialized);
    Assert(!this->HaveTyper(gamerInfo.GetGamerId()));

    bool success = false;

    tcDebug("Adding %s typer:\"%s\"...",
            gamerInfo.IsLocal() ? "local" : "remote",
            gamerInfo.GetName());

    if(AssertVerify((gamerInfo.IsLocal() && m_NumLocalTypers < m_MaxLocalTypers)
                    || (gamerInfo.IsRemote() && this->RemoteTyperCount() < m_MaxRemoteTypers)))
    {
        Assert(m_TyperPool);

        //Grab a typer from the pool and put him in our various containers.
        Typer* typer = m_TyperPool;
        m_TyperPool = typer->m_Next;
        typer->m_Next = NULL;

        typer->Init(gamerInfo);

        if(gamerInfo.IsLocal())
        {
            Assert(!m_LocalTypers[gamerInfo.GetLocalIndex()]);

            m_LocalTypers[gamerInfo.GetLocalIndex()] = typer;
            ++m_NumLocalTypers;
        }
        else
        {
            Assert(NET_IS_VALID_ENDPOINT_ID(endpointId));

            Peer* peer = this->FindPeerByEndpointId(endpointId);

            if(!peer)
            {
                Assert(m_PeerPool);

                peer = m_PeerPool;
                m_PeerPool = peer->m_Next;
                peer->m_Next = NULL;

                peer->Init(endpointId);

                m_Peers[m_NumPeers++] = peer;
            }

            peer->AddTyper(typer);
        }

        m_Typers[m_NumTypers++] = typer;

        success = true;
    }

    return success;
}

void
TextChat::SetOnSameTeam(const EndpointId endpointId, bool onSameTeem)
{
	Assert(m_Initialized);
	Peer* peer = this->FindPeerByEndpointId(endpointId);
	if(AssertVerify(peer))
	{
		peer->SetOnSameTeam(onSameTeem);
	}
}

bool
TextChat::RemoveTyper(const rlGamerId& gamerId)
{
    SYS_CS_SYNC(m_CsTypers);

    Assert(m_Initialized);

    const int typerIdx = this->GetTyperIndex(gamerId);

    if(typerIdx >= 0)
    {
        Assert(m_NumTypers > 0);

        Typer* typer = m_Typers[typerIdx];

        tcDebug("Removing %s typer:\"%s\"...",
                typer->m_GamerInfo.IsLocal() ? "local" : "remote",
                typer->m_GamerInfo.GetName());

        m_Typers[typerIdx] = m_Typers[m_NumTypers-1];
        m_Typers[m_NumTypers-1] = NULL;
        --m_NumTypers;

        if(typer->m_GamerInfo.IsLocal())
        {
            Assert(m_NumLocalTypers > 0);
            Assert(typer == m_LocalTypers[typer->m_GamerInfo.GetLocalIndex()]);
            m_LocalTypers[typer->m_GamerInfo.GetLocalIndex()] = NULL;
            --m_NumLocalTypers;
        }
        else
        {
            Peer* peer = typer->m_Peer;
			if(peer)
			{
				peer->RemoveTyper(typer);

				if(0 == peer->m_NumTypers)
				{
					const int index = this->GetPeerIndex(peer->m_EndpointId);
					Assert(index >= 0);

					m_Peers[index] = m_Peers[m_NumPeers-1];
					m_Peers[m_NumPeers-1] = NULL;
					--m_NumPeers;

					peer->Shutdown();
					peer->m_Next = m_PeerPool;
					m_PeerPool = peer;
				}
			}
        }

        typer->Shutdown();
        typer->m_Next = m_TyperPool;
        m_TyperPool = typer;
		return true;
    }
	return false;
}

void
TextChat::RemoveAllRemoteTypers()
{
	unsigned int typerIndex = 0;

	while(m_NumTypers > m_NumLocalTypers)
	{
		if(m_Typers[typerIndex]->m_GamerInfo.IsLocal())
		{
			++typerIndex;
		}
		else
		{
			this->RemoveTyper(m_Typers[typerIndex]->m_GamerInfo.GetGamerId());
		}
	}

	Assert(m_NumLocalTypers == m_NumTypers);
}

void
TextChat::RemoveAllTypers()
{
    while(m_NumTypers)
    {
        this->RemoveTyper(m_Typers[0]->m_GamerInfo.GetGamerId());
    }

    Assert(0 == m_NumTypers);
    Assert(0 == m_NumLocalTypers);
}

bool TextChat::HaveTyper(const EndpointId endpointId) const
{
	return FindPeerByEndpointId(endpointId) != nullptr;
}

bool
TextChat::HaveTyper(const rlGamerId& gamerId) const
{
    return NULL != this->FindTyperByGamerId(gamerId);
}

void
TextChat::SetTextChatRecipients(int recipients)
{
	if(AssertVerify(recipients == SEND_TO_TEAM || recipients == SEND_TO_EVERYONE))
	{
		m_TextChatRecipients = recipients;
	}
}

bool
TextChat::IsTyping(const rlGamerId& gamerId) const
{
    const Typer* typer = this->FindTyperByGamerId(gamerId);

    return typer && typer->m_IsTypingCountdown > 0;
}

bool
TextChat::SetLocalPlayerIsTyping(const rlGamerId& gamerId)
{
	Typer* typer = this->FindTyperByGamerId(gamerId);

	if(typer && AssertVerify(typer->m_GamerInfo.IsLocal()))
	{
		typer->m_IsTypingCountdown = IS_TYPING_COUNTDOWN;
	}
	return true;
}

void
TextChat::SetRemotePlayerIsTyping(const netEventFrameReceived* fr)
{
	CMsgPlayerIsTyping msg;

	tcDebug("Receiving CMsgPlayerIsTyping");

	AssertVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload));

	rlGamerId gamerId = msg.m_GamerId;

	Typer* typer = this->FindTyperByGamerId(gamerId);

	if(typer)
	{
		Assert(typer->m_GamerInfo.IsRemote());
		typer->m_IsTypingCountdown = IS_TYPING_COUNTDOWN;
	}
	else
	{
		tcDebug("invalid gamer id in SetRemotePlayerIsTyping()");
	}
}

bool
TextChat::IsAnyTyping() const
{
    bool anyTyping = false;

    for(int i = 0; i < (int) m_NumTypers; ++i)
    {
        if(this->IsTyping(m_Typers[i]->m_GamerInfo.GetGamerId()))
        {
            anyTyping = true;
            break;
        }
    }

    return anyTyping;
}

bool
TextChat::IsMuted(const rlGamerHandle& WIN32PC_ONLY(hGamer)) const
{
#if RSG_PC
	return g_rlPc.GetPlayerManager()->IsBlocked(hGamer.GetRockstarId());
#else
    return false;
#endif // RSG_PC
}

bool
TextChat::HasTextChatPrivileges(const int localGamerIndex) const
{
    return rlPresence::IsSignedIn(localGamerIndex);
}

bool
TextChat::IsTextChatProhibited(const int localGamerIndex) const
{
	if(rlPresence::IsSignedIn(localGamerIndex))
	{
		return false;
	}

	return true;
}

bool
TextChat::HasKeyboard(const int UNUSED_PARAM(localGamerIndex)) const
{
#if __WIN32PC
	// I'm assuming we always have a keyboard on the PC
	return true;
#else
    return false;
#endif 
}

void
TextChat::SetInTransition(bool inTransition)
{
	m_InTransition = inTransition;
}

void
TextChat::Update()
{
    Assert(m_CxnMgr);

    const unsigned curTime = sysTimer::GetSystemMsTime() | 0x01;

    const unsigned timeStep =
        m_LastUpdateTime
            ? (curTime - m_LastUpdateTime)
            : 0;

    m_LastUpdateTime = curTime;

    for(int i = 0; i < (int) m_NumTypers; ++i)
    {
        Typer* typer = m_Typers[i];

        if(typer->m_IsTypingCountdown > 0)
        {
            typer->m_IsTypingCountdown -= (int) timeStep;

            if(typer->m_IsTypingCountdown < 0)
            {
                typer->m_IsTypingCountdown = 0;
            }
        }
    }

    for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
    {
        const Typer* localTyper = m_LocalTypers[i];
        if(!localTyper){continue;}

        for(int j = 0; j < (int) m_NumPeers; ++j)
        {
            Peer* peer = m_Peers[j];
            peer->m_Muted = false;

			if(peer->m_SentIsTypingMessageCountdown > 0)
			{
				peer->m_SentIsTypingMessageCountdown -= (int) timeStep;

				if(peer->m_SentIsTypingMessageCountdown < 0)
				{
					peer->m_SentIsTypingMessageCountdown = 0;
				}
			}

            for(int k = 0; k < (int) peer->m_NumTypers && !peer->m_Muted; ++k)
            {
                const Typer* remoteTyper = peer->m_Typers[k];

                peer->m_Muted = this->IsMuted(remoteTyper->m_GamerInfo.GetGamerHandle());

            }
        }
    }

    this->SendMessages();
}

//private:

unsigned
TextChat::RemoteTyperCount() const
{
    Assert(m_NumTypers >= m_NumLocalTypers);
    return m_NumTypers - m_NumLocalTypers;
}

int
TextChat::GetTyperIndex(const rlGamerId& gamerId) const
{
    int index = -1;

    for(int i = 0; i < (int) m_NumTypers; ++i)
    {
        if(m_Typers[i]->m_GamerInfo.GetGamerId() == gamerId)
        {
            index = i;
            break;
        }
    }

    return index;
}

TextChat::Typer*
TextChat::FindTyperByGamerId(const rlGamerId& gamerId)
{
    const int index = this->GetTyperIndex(gamerId);

    return index >= 0 ? m_Typers[index] : NULL;
}

const TextChat::Typer*
TextChat::FindTyperByGamerId(const rlGamerId& gamerId) const
{
    return const_cast<TextChat*>(this)->FindTyperByGamerId(gamerId);
}

int
TextChat::GetPeerIndex(const EndpointId endpointId) const
{
    int index = -1;

    for(int i = 0; i < (int) m_NumPeers; ++i)
    {
        if(endpointId == m_Peers[i]->m_EndpointId)
        {
            index = i;
            break;
        }
    }

    return index;
}

TextChat::Peer*
TextChat::FindPeerByEndpointId(const EndpointId endpointId)
{
    const int index = this->GetPeerIndex(endpointId);

    return index >= 0 ? m_Peers[index] : NULL;
}

const TextChat::Peer*
TextChat::FindPeerByEndpointId(const EndpointId endpointId) const
{
    return const_cast<TextChat*>(this)->FindPeerByEndpointId(endpointId);
}

void
TextChat::OnNetEvent(netConnectionManager* /*cxnMgr*/, const netEvent* evt)
{
	const unsigned evtId = evt->GetId();

	if(NET_EVENT_FRAME_RECEIVED == evtId)
	{
		const netEventFrameReceived* fr = evt->m_FrameReceived;
		unsigned msgId;

		if(netMessage::GetId(&msgId, fr->m_Payload, fr->m_SizeofPayload))
		{
			if(msgId == CMsgTextMessage::MSG_ID())
			{
				this->ReceiveText(fr);
			}
			else if(msgId == CMsgPlayerIsTyping::MSG_ID())
			{
				this->SetRemotePlayerIsTyping(fr);
			}
		}
	}
}

void
TextChat::ReceiveText(const netEventFrameReceived* fr)
{
	CMsgTextMessage msg;

	AssertVerify(msg.Import(fr->m_Payload, fr->m_SizeofPayload));

	rlGamerId gamerId = msg.m_GamerId;

	Typer* typer = this->FindTyperByGamerId(gamerId);
	const Peer* peer = this->FindPeerByEndpointId(fr->GetEndpointId());

	// validate that our typer is found and that their peer matches the peer found by the endpoint lookup
	// the latter prevents a peer from embedding a different gamerId in the message to masquerade as someone else
	if(typer && typer->m_Peer == peer)
	{
		Assert(typer->m_GamerInfo.IsRemote());
		typer->m_IsTypingCountdown = 0;
		tcDebug("Received text message: %s", msg.m_Text);
		m_Delegator.Dispatch(this, typer->m_GamerInfo.GetGamerHandle(), msg.m_Text, msg.m_TeamOnly);
	}
	else
	{
		tcDebug("Ignoring text message because sending typer is not in our list: %s", msg.m_Text);
	}
}

bool
TextChat::SubmitText(const rlGamerId& gamerId, const char* text, bool teamOnly)
{
	bool success = false;

	rtry
	{
		if(teamOnly)
		{
			m_TextChatRecipients = SEND_TO_TEAM;
		}
		else
		{
			m_TextChatRecipients = SEND_TO_EVERYONE;
		}

		rverify(m_Initialized, catchall, );
		rverify(this->HaveTyper(gamerId), catchall, );
		rverify(text, catchall, );
		rverify(text[0] != '\0', catchall, );

		Typer* typer = this->FindTyperByGamerId(gamerId);
		rverify(typer, catchall, );
		rverify(typer->m_GamerInfo.IsLocal(), catchall, );
		rverify(m_TextToSend[0] == '\0', catchall, );

		tcDebug("Queuing local text message to be sent: %s", text);
		safecpy(m_TextToSend, text);

		success = true;
	}
	rcatchall
	{

	}

	return success;
}

void
TextChat::SendMessages()
{
	for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
	{
		Typer* typer = m_LocalTypers[i];

		if(!typer)
		{
			continue;
		}

		if((m_TextToSend[0] == '\0') && IsTyping(typer->m_GamerInfo.GetGamerId()))
		{
			const rlGamerId& gamerId = typer->m_GamerInfo.GetGamerId();
			CMsgPlayerIsTyping msg(gamerId);

			for(int i = 0; i < (int) m_NumPeers; ++i)
			{
				Peer* peer = m_Peers[i];

				// we only send the PlayerIsTyping message at most every 1000ms to each peer
				if(peer->m_Muted || !peer->m_HaveTextChatPrivs || peer->m_SentIsTypingMessageCountdown > 0)
				{
					continue;
				}

				if(m_TextChatRecipients == SEND_TO_TEAM && peer->m_OnSameTeamAsLocalPlayer == false)
				{
					// this message is meant for team members only and this peer is not on our team so don't send to him
					tcDebug("Not sending Istyping to the peer because he's not in the same team: %s", m_TextToSend);
					continue;
				}

				if(peer->m_ConnectionId < 0)
				{
					peer->m_ConnectionId = m_CxnMgr->GetConnectionId(peer->m_EndpointId, m_InTransition ? m_TransitionChanelId : m_ChannelId);
				}
				if(AssertVerify(peer->m_ConnectionId >= 0))
				{
					tcDebug("Sending CMsgPlayerIsTyping");

					m_CxnMgr->Send(peer->m_ConnectionId,
								   msg,
								   NET_SEND_RELIABLE,
								   NULL);
				}

				peer->m_SentIsTypingMessageCountdown = IS_TYPING_MSG_FREQ;
			}
		}
		else if(m_TextToSend[0] != '\0')
		{
			const rlGamerId& gamerId = typer->m_GamerInfo.GetGamerId();
			CMsgTextMessage msg(gamerId, m_TextToSend, m_TextChatRecipients == SEND_TO_TEAM);

			for(int i = 0; i < (int) m_NumPeers; ++i)
			{
				Peer* peer = m_Peers[i];

				if(peer->m_Muted || !peer->m_HaveTextChatPrivs)
				{
					tcDebug("Not sending text message because peer is muted or doesn't have text chat privs: %s", m_TextToSend);
					continue;
				}

				if(m_TextChatRecipients == SEND_TO_TEAM && peer->m_OnSameTeamAsLocalPlayer == false)
				{
					// this message is meant for team members only and this peer is not on our team so don't send to him
					tcDebug("Not sending text message to the peer because he's not in the same team: %s", m_TextToSend);
					continue;
				}

				if(peer->m_ConnectionId < 0)
				{
					peer->m_ConnectionId = m_CxnMgr->GetConnectionId(peer->m_EndpointId, m_InTransition ? m_TransitionChanelId : m_ChannelId);
				}
				if(AssertVerify(peer->m_ConnectionId >= 0))
				{
					tcDebug("Sending text message: %s", m_TextToSend);

					m_CxnMgr->Send(peer->m_ConnectionId,
								   msg,
								   NET_SEND_RELIABLE,
								   NULL);
				}
			}

			typer->m_IsTypingCountdown = 0;
			m_TextToSend[0] = '\0';
		}
	}
}

void TextChat::Typer::Init(const rlGamerInfo& gamerInfo)
{
    Assert(!m_Peer);

    m_GamerInfo = gamerInfo;
	m_IsTypingCountdown = 0;
    m_IsActive = true;
}

void TextChat::Typer::Shutdown()
{
    Assert(!m_Peer);

	m_GamerInfo.Clear();
	m_IsTypingCountdown = 0;
    m_IsActive = false;
}

}   //namespace rage

#endif // __WIN32PC
