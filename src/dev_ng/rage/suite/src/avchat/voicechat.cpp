// 
// avchat/voicechat.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "voicechat.h"

#include "atl/functor.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "net/connectionmanager.h"
#include "net/message.h"
#include "net/netdiag.h"
#include "rline/rlpresence.h"
#include "system/timer.h"
#include "system/param.h"
#include "fwsys/timer.h"

#if __BANK
#include "bank/bkmgr.h"
#include "bank/slider.h"
#include "bank/bank.h"
#endif  //__BANK

#ifndef PROCESS_RAW_DATA
#if RSG_ORBIS
#define PROCESS_RAW_DATA 1
#else
#define PROCESS_RAW_DATA 0
#endif
#endif

#if PROCESS_RAW_DATA
#define PROCESS_RAW_DATA_ONLY(x)	x
#else
#define PROCESS_RAW_DATA_ONLY(x)
#endif

#define VOICECHAT_REMOTE_TALKER_FX 0

#if RSG_DURANGO
#include <xdk.h>
#include "voicechat_durango.h"

#elif RSG_ORBIS
#pragma comment(lib,"SceVoice_stub_weak")

#define USE_SPEEX_ONLY(x)
#include <audioin.h>
#include <audioout.h>
#include <libsysmodule.h>
#include <voice.h>
#include <voice/voice_types.h>

// map PS3 API names to PS4 API names
#define cellSysmoduleIsLoaded sceSysmoduleIsLoaded
#define cellVoiceInit sceVoiceInit
#define cellVoiceReadFromOPort sceVoiceReadFromOPort
#define cellVoiceWriteToIPort sceVoiceWriteToIPort
#define CellVoiceBasePortInfo SceVoiceBasePortInfo
#define cellVoiceGetPortInfo sceVoiceGetPortInfo
#define cellVoiceCreatePort sceVoiceCreatePort
#define cellVoiceConnectIPortToOPort sceVoiceConnectIPortToOPort
#define cellVoiceStart sceVoiceStart
#define cellVoiceGetBitRate sceVoiceGetBitRate
#define cellVoiceStop sceVoiceStop
#define cellVoiceDisconnectIPortFromOPort sceVoiceDisconnectIPortFromOPort
#define cellVoiceDeletePort sceVoiceDeletePort
#define cellVoiceEnd sceVoiceEnd

#define CellVoicePortType SceVoicePortType
#define CellVoiceInitParam SceVoiceInitParam
#define CellVoiceEventType SceVoiceEventType
#define CellVoicePortParam SceVoicePortParam

#define CELL_SYSMODULE_VOICE SCE_SYSMODULE_VOICE
#define CELL_OK SCE_OK

#define CELLVOICE_PORTSTATE_NULL SCE_VOICE_PORTSTATE_NULL
#define CELLVOICE_PORTSTATE_IDLE SCE_VOICE_PORTSTATE_IDLE
#define CELLVOICE_PORTSTATE_READY SCE_VOICE_PORTSTATE_READY
#define CELLVOICE_PORTSTATE_BUFFERING SCE_VOICE_PORTSTATE_BUFFERING
#define CELLVOICE_PORTSTATE_RUNNING SCE_VOICE_PORTSTATE_RUNNING

#define CELLVOICE_PORTTYPE_IN_MIC SCE_VOICE_PORTTYPE_IN_DEVICE
#define CELLVOICE_PORTTYPE_OUT_SECONDARY SCE_VOICE_PORTTYPE_OUT_DEVICE
#define CELLVOICE_PORTTYPE_OUT_VOICE SCE_VOICE_PORTTYPE_OUT_VOICE
#define CELLVOICE_PORTTYPE_OUT_PCMAUDIO SCE_VOICE_PORTTYPE_OUT_PCMAUDIO
#define CELLVOICE_PORTTYPE_IN_VOICE SCE_VOICE_PORTTYPE_IN_VOICE

#define CELLVOICE_BITRATE_3850 SCE_VOICE_BITRATE_3850
#define CELLVOICE_PCM_SHORT SCE_VOICE_PCM_SHORT_LITTLE_ENDIAN
#define CELLVOICE_SAMPLINGRATE_16000 SCE_VOICE_SAMPLINGRATE_16000

#elif __WIN32PC
#include "rline/rlpc.h"
#include "voice/rv.h"
#include "voice/rvHardwareDevice.h"
#include "voice/rvDirectSound.h"
#include "string/stringhash.h"
#endif // RSG_DURANGO

#if USE_SPEEX || __WIN32PC
CompileTimeAssert(OVERRIDE_SPEEX_ERROR);
CompileTimeAssert(OVERRIDE_SPEEX_WARNING);
CompileTimeAssert(OVERRIDE_SPEEX_WARNING_INT);

extern "C"
{

    //Override speex_error so it doesn't call exit().
    void speex_error(const char *str)
    {
        Assertf(false, "Fatal Speex error: %s\n", str);
        Quitf(rage::ERR_GEN_CODEC_1,"Fatal Speex error: %s\n", str);
    }

    //Override speex_warning so it doesn't write to stderr.
    void speex_warning(const char * OUTPUT_ONLY(str))
    {
        Warningf("Speex warning: %s", str);
    }

    //Override speex_warning so it doesn't write to stderr.
    void speex_warning_int(const char * OUTPUT_ONLY(str), int OUTPUT_ONLY(val))
    {
        Warningf("Speex warning: %s %d", str, val);
    }

}	// extern "C"
#endif	// USE_SPEEX

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, voicechat)

#define vcDebug(fmt, ...)				RAGE_DEBUGF1(ragenet_voicechat, fmt, ##__VA_ARGS__)
#define vcWarning(fmt, ...)				RAGE_WARNINGF(ragenet_voicechat, fmt, ##__VA_ARGS__)
#define vcError(fmt, ...)				RAGE_ERRORF(ragenet_voicechat, fmt, ##__VA_ARGS__)
#define vcVerify(cond)					RAGE_VERIFY(ragenet_voicechat, cond)
#define vcVerifyf(cond, fmt, ...)		RAGE_VERIFYF(ragenet_voicechat, cond, fmt, ##__VA_ARGS__)
#define vcAssert(cond)					RAGE_ASSERT(ragenet_voicechat, cond)
#define vcAssertf(cond, fmt, ...)		RAGE_ASSERTF(ragenet_voicechat, cond, fmt, ##__VA_ARGS__)

//How long we sleep between updates of the chat thread.
static const unsigned CHAT_THREAD_SLEEP_INTERVAL = 30;

//Max ms between receiving a voice packet and when we'll hear it.
static const int ABOUT_TO_START_TALKING_TIME    = 1000;

//Number of bytes used to represent the size of a voice packet.
static const unsigned VPKT_SIZEOF_SIZE      = sizeof(u16);

//Number of bytes used to send a voice packet timestamp
#if USE_SPEEX
static const unsigned VPKT_SIZEOF_TIMESTAMP = sizeof(u32);
#else
static const unsigned VPKT_SIZEOF_TIMESTAMP = 0;
#endif

//Number of bytes in a voice packet header.
static const unsigned VPKT_SIZEOF_HEADER = sizeof(rlGamerId) + VPKT_SIZEOF_TIMESTAMP + VPKT_SIZEOF_SIZE;

//Maximum size of pending voice data (per local player)
static const unsigned MAX_DATA_SIZE = VoiceChat::MAX_PACKET_SIZE - VPKT_SIZEOF_HEADER;

//Buffers to hold pending voice data waiting to be sent
static u8 sm_VoiceData[RL_MAX_LOCAL_GAMERS][VoiceChat::MAX_PACKET_SIZE];
static unsigned sm_DataPendingSize[RL_MAX_LOCAL_GAMERS];

#if RSG_ORBIS
extern SceUserServiceUserId g_initialUserId;
#endif

#if USE_SPEEX

static const unsigned MIC_SAMPLE_RATE   = 8000;
static const unsigned AUD_SAMPLE_RATE   = 48000;
static const int DUP_MIC_SAMPLES        = ((AUD_SAMPLE_RATE / MIC_SAMPLE_RATE) * CELL_AUDIO_PORT_2CH);
static const unsigned NUM_AUD_BLOCKS    = CELL_AUDIO_BLOCK_32;
static const unsigned SIZEOF_AUD_BLOCK  = (CELL_AUDIO_PORT_2CH * CELL_AUDIO_BLOCK_SAMPLES);
static const unsigned SIZEOF_AUD_BUFFER = (SIZEOF_AUD_BLOCK * NUM_AUD_BLOCKS);

//Duration of audio buffer in milliseconds.
static const unsigned AUD_BUF_DURATION_MS   =
    1000*NUM_AUD_BLOCKS*CELL_AUDIO_BLOCK_SAMPLES/AUD_SAMPLE_RATE;

//Value from 0 to 10
static const int AVC_SPEEX_QUALITY  = 2;

//Voice activity detection.  On (1) or off (0).
static const int AVC_SPEEX_USE_VAD  = 0;

//See PS3 docs on cellMicGetSignalState()
static const unsigned AVC_CELL_VPD_CHECK_INTERVAL   = 24;   //milliseconds

//Suggested in Speex FAQ.  Actually, the FAQ suggests 8000, but power
//of 2 scale gives better float precision.
static const float AVC_SPEEX_UP_SCALE   = 8192;
static const float AVC_SPEEX_DOWN_SCALE = 1 / AVC_SPEEX_UP_SCALE;

#endif  //USE_SPEEX

#if __WIN32PC
static RVHardwareDevice* m_CaptureDevice;
static RVHardwareDevice* m_PlaybackDevice;
static bool m_CaptureDeviceUnplugged = false;
static bool m_CaptureDeviceUnpluggedPending = false;
static bool m_PlaybackDeviceUnplugged = false;
static bool m_PlaybackDeviceUnpluggedPending = false;

bool IsCaptureDeviceAvailable()
{
	return (m_CaptureDevice) && !(m_CaptureDeviceUnplugged || m_CaptureDeviceUnpluggedPending);
}

bool IsPlaybackDeviceAvailable()
{
	return (m_PlaybackDevice) && !(m_PlaybackDeviceUnplugged || m_PlaybackDeviceUnpluggedPending);
}

// time in ms to check for reconnected devices after a device disconnect has been detected
static const int CHECK_FOR_RECONNECTS_TIME_MS = 10000;
static int m_CheckForReconnectsTimer;
#endif

#if PROCESS_RAW_DATA
static float gs_Loudness[RL_MAX_LOCAL_GAMERS];
static float gs_LoudnessMean[RL_MAX_LOCAL_GAMERS];

#if RSG_XENON
void 
HandleRawData(const unsigned localPlayer, void* data, const unsigned size)
{
	// 100ms @ 16kHz
	enum {rmsWindowLength = 1600};
	const float oneOverRmsWindowLength = 1.f / rmsWindowLength;

	const s16 *samples = reinterpret_cast<const s16 *>(data);
	const u32 numSamples = size >> 1;

	for(u32 i = 0; i < numSamples; i++)
	{
		const float sampleVal = (*samples++ / 32767.f);
		const float sampleSquared = sampleVal * sampleVal;

		// Running Average: (((n-1) * average) + newSample) / n
		gs_LoudnessMean[localPlayer] = (((rmsWindowLength-1) * gs_LoudnessMean[localPlayer]) + sampleSquared) * oneOverRmsWindowLength;
	}

	// [0,1] metric of the sustained volume coming through the microphone
	gs_Loudness[localPlayer] = Min(1.f, 6.f * Sqrtf(gs_LoudnessMean[localPlayer]));

	if(localPlayer == 0 && VoiceChat::GetAudioProvider())
	{
		VoiceChat::GetAudioProvider()->LocalPlayerAudioReceived(reinterpret_cast<const s16*>(data), numSamples);
	}
}
#elif RSG_ORBIS
void
CalculateLoudness(const unsigned UNUSED_PARAM(localPlayer), void* data, const unsigned size)
{
	// 100ms @ 16kHz
	enum {rmsWindowLength = 1600};
	const float oneOverRmsWindowLength = 1.f / rmsWindowLength;

	const s16 *samples = reinterpret_cast<const s16 *>(data);
	const u32 numSamples = size >> 1;

	for(u32 i = 0; i < numSamples; i++)
	{
		const float sampleVal = (*samples++ / 32767.f);
		const float sampleSquared = sampleVal * sampleVal;

		// Running Average: (((n-1) * average) + newSample) / n
		float loudnessMean = (((rmsWindowLength-1) * gs_LoudnessMean[0]) + sampleSquared) * oneOverRmsWindowLength;

		// TODO: Set value for all local players for now since there is only support for one local player on PS4 at this time
		for(int playerIndex = 0; playerIndex < RL_MAX_LOCAL_GAMERS; ++playerIndex)
		{
			gs_LoudnessMean[playerIndex] = loudnessMean;
		}
	}

	// [0,1] metric of the sustained volume coming through the microphone
	float loudness = Min(1.f, 6.f * Sqrtf(gs_LoudnessMean[0]));
	
	// TODO: Set value for all local players for now since there is only support for one local player on PS4 at this time
	for(int playerIndex = 0; playerIndex < RL_MAX_LOCAL_GAMERS; ++playerIndex)
	{
		gs_Loudness[playerIndex] = loudness;
	}
}
#endif // RSG_XENON
#endif // PROCESS_RAW_DATA

///////////////////////////////////////////////////////////////////////////////
//  datRingBuffer
///////////////////////////////////////////////////////////////////////////////

#if USE_SPEEX
datRingBuffer::datRingBuffer()
    : m_Buf(NULL)
    , m_Capacity(0)
    , m_Buflen(0)
    , m_WritePos(0)
    , m_ReadPos(0)
{
}

void
datRingBuffer::Init(void* buf, const unsigned sizeofBuf)
{
    m_Buf = (u8*) buf;
    m_Capacity = sizeofBuf;
    m_Buflen = m_WritePos = m_ReadPos = 0;
}

void
datRingBuffer::Shutdown()
{
    m_Buf = NULL;
    m_Capacity = m_Buflen = m_WritePos = m_ReadPos = 0;
}

void
datRingBuffer::Clear()
{
    m_Buflen = m_WritePos = m_ReadPos = 0;
}

unsigned
datRingBuffer::Read(void* dst, const unsigned len)
{
    const unsigned bufLen = m_Buflen;
    const int count = int((bufLen > len) ? len : bufLen);

    if(count > 0)
    {
        //Make sure we get fresh data out of L2, otherwise we might
        //get stale data out of L1
        sys_lwsync();

        if((m_ReadPos + count) > m_Capacity)
        {
            const unsigned tmp = m_Capacity - m_ReadPos;

            sysMemCpy(dst, &m_Buf[m_ReadPos], tmp);
            sysMemCpy(&((u8*)dst)[tmp], m_Buf, count - tmp);

            m_ReadPos = (m_ReadPos + count) - m_Capacity;
        }
        else
        {
            sysMemCpy(dst, &m_Buf[m_ReadPos], count);
            m_ReadPos += count;

            if(m_ReadPos == m_Capacity){m_ReadPos = 0;}
        }

        vcAssert(m_ReadPos < m_Capacity);

        //Make sure we finish copying the data out of the buffer before
        //"freeing" the memory.  Otherwise the write could read the new
        //value of m_Buflen and overwrite the mem as it's still being
        //copied by the read thread.
        sys_lwsync();

        sysInterlockedAdd(&m_Buflen, -count);
    }

    return count;
}

unsigned
datRingBuffer::Write(const void* src, const unsigned len)
{
    const unsigned freeLen = m_Capacity - m_Buflen;
    const unsigned count = (len > freeLen) ? freeLen : len;

    if(count)
    {
        if((m_WritePos + count) > m_Capacity)
        {
            const unsigned tmp = m_Capacity - m_WritePos;

            sysMemCpy(&m_Buf[m_WritePos], src, tmp);
            sysMemCpy(m_Buf, &((const u8*)src)[tmp], count - tmp);

            m_WritePos = (m_WritePos + count) - m_Capacity;
        }
        else
        {
            sysMemCpy(&m_Buf[m_WritePos], src, count);
            m_WritePos += count;

            if(m_WritePos == m_Capacity){m_WritePos = 0;}
        }

        vcAssert(m_WritePos < m_Capacity);

        //Make sure the data gets to the buffer before the buffer
        //length gets increased
        sys_lwsync();

        sysInterlockedAdd(&m_Buflen, count);

        vcAssert(m_Buflen <= m_Capacity);
    }

    return count;
}

unsigned
datRingBuffer::GetCapacity() const
{
    return m_Capacity;
}

unsigned
datRingBuffer::GetSize() const
{
    return m_Buflen;
}
#endif	// USE_SPEEX

///////////////////////////////////////////////////////////////////////////////
//  VoiceChat
///////////////////////////////////////////////////////////////////////////////
bool VoiceChat::sm_Initialized = false;
bool VoiceChat::sm_InitializedNetwork = false;
VoiceChatAudioProvider *VoiceChat::sm_AudioProvider = NULL;

//Bitmask with a one bit for each user that has chat privileges.
//We cache chat privs because sometimes XUserCheckPrivilege(),
//which is used to check chat privs, stalls for 5-6ms.
static unsigned s_HasChatPrivsMask = 0;

static void RefreshChatPrivsMask()
{
	vcDebug("RefreshChatPrivsMask");

    s_HasChatPrivsMask = 0;
    for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
    {
		if(rlPresence::IsSignedIn(i) && rlGamerInfo::HasChatPrivileges(i))
		{
			vcDebug("RefreshChatPrivsMask :: User %d has chat privileges", i);
			s_HasChatPrivsMask |= (1<<i);
		}
	}
}

//Some presence events will require a privilege refresh
static void OnPresenceEvent(const rlPresenceEvent* evt)
{
	switch(evt->GetId())
	{
	case PRESENCE_EVENT_SIGNIN_STATUS_CHANGED:
#if RSG_DURANGO
	VoiceChatDurangoSingleton::GetInstance().RefreshHeadsetState();
	// intentional fall-through
#endif
	case PRESENCE_EVENT_PROFILE_CHANGED:
	case PRESENCE_EVENT_ONLINE_PERMISSIONS_CHANGED:
        RefreshChatPrivsMask();
		break;
	default:
		break;
    }
}

//Listens for presence events.
static rlPresence::Delegate s_PresenceDelegate(&OnPresenceEvent);

VoiceChat::VoiceChat()
: m_TalkerPool(NULL)
, m_NumTalkers(0)
, m_NumTalkerInfo(0)
, m_PeerPool(NULL)
, m_NumPeers(0)
, m_MaxLocalTalkers(0)
, m_MaxRemoteTalkers(0)
, m_NumLocalTalkers(0)
, m_CxnMgr(0)
, m_ChannelId(NET_INVALID_CHANNEL_ID)
, m_LastUpdateTime(0)
, m_LastSendTime(0)
, m_MinSendInterval(DEFAULT_MIN_SEND_INTERVAL)
, m_PktCursor(m_VoicePacket)
, m_EncBytesPerFrame(0)
, m_ThreadHandle(sysIpcThreadIdInvalid)
, m_ThreadId(sysIpcCurrentThreadIdInvalid)
, m_Sema(NULL)
, m_VpdThreshold(DEFAULT_VPD_THRESHOLD)
, m_VpdThresholdPTT(DEFAULT_VPD_THRESHOLD)
, m_CaptureHoldTimeMs(DEFAULT_VPD_HOLD_TIME_MS)
, m_LocalOnly(false)
, m_StopChatThread(false)
{
	CompileTimeAssert(MAX_DATA_SIZE < MAX_PACKET_SIZE);

	sysMemSet(m_LocalTalkers, 0, sizeof(m_LocalTalkers));

    //Initialize the pool of talkers and peers
    for(int i = 0; i < VoiceChatTypes::MAX_TALKERS; ++i)
    {
        m_TalkerPile[i].m_Next = m_TalkerPool;
        m_TalkerPool = &m_TalkerPile[i];
        m_Talkers[i] = NULL;

        m_PeerPile[i].m_Next = m_PeerPool;
        m_PeerPool = &m_PeerPile[i];
        m_Peers[i] = NULL;
    }

#if RSG_DURANGO
	VoiceChatDurangoSingleton::Instantiate();

#elif RSG_NP
    m_FrameDurationMs = 0;
    m_State = STATE_INVALID;

#if USE_SPEEX

    m_AudPortNum = ~0u;

    m_MicDevNum = -1;
    CompileTimeAssert(AUD_BUF_DURATION_MS > DEFAULT_MIN_SEND_INTERVAL);

    m_AudBlockAddr = NULL;
    m_AudDeviceBuffer = NULL;

    m_Vpd = 0;
    m_VpdCountdown = 0;

    m_SpeexEncState = NULL;

    CompileTimeAssert(sizeof(SpeexBits) == sizeof(m_SpeexBitsEncMem));
    m_SpeexBitsEnc = (SpeexBits*) m_SpeexBitsEncMem;

    m_NumSamples = 0;
    m_VoiceActivityDuringCurrentFrame = false;
    m_VoiceStreamTimestamp = 0;

    m_WaitSema = 0;
    m_WorkerThreadId = sysIpcThreadIdInvalid;

    m_StopWorker = false;
#elif USE_LIBVOICE

    m_MicPort = m_HeadsetPort = m_VoiceInPort = m_VoiceOutPort = m_MicPcmPort = LIBVOICE_PORT_INVALID;

#if RSG_ORBIS
	PortManagerSingleton::Instantiate();
#endif // RSG_ORBIS
#endif  //USE_SPEEX
#elif RSG_PC
	m_CaptureMode = CAPTURE_MODE_PUSH_TO_TALK;
	m_LocalTalkerSpeakingVolume = 0.0f;
	m_PackFrameStamp = true;
#endif // RSG_DURANGO

#if __BANK
    m_BankVpdSlider = NULL;
	m_BankVpdSliderPTT = NULL;
	m_BankSliderVpdHoldTime = NULL;
    m_BankVpdThreshold = (float) m_VpdThreshold;
	m_BankVpdThresholdPTT = (float) m_VpdThresholdPTT;
	m_BankVpdCaptureHoldTimeMs = (u32) m_CaptureHoldTimeMs;
#endif  //__BANK;

}

VoiceChat::~VoiceChat()
{
#if RSG_DURANGO
	VoiceChatDurangoSingleton::Destroy();
#endif // RSG_DURANGO

    this->Shutdown();

#if RSG_ORBIS
	PortManagerSingleton::Destroy();
#endif // RSG_ORBIS
}

void VoiceChat::AddDelegate( Delegate* dlgt )
{
	if(vcVerify(sm_InitializedNetwork))
	{
		m_Delegator.AddDelegate(dlgt);
	}
}

void VoiceChat::RemoveDelegate( Delegate* dlgt )
{
	if(vcVerify(sm_InitializedNetwork))
	{
		m_Delegator.RemoveDelegate(dlgt);
	}
}

bool
VoiceChat::Init(const unsigned maxLocalTalkers, 
                const unsigned maxRemoteTalkers, 
                void* audioDevice, 
                const int cpuAffinity)
{
	vcAssert(maxLocalTalkers <= VoiceChatTypes::MAX_LOCAL_TALKERS);
	vcAssert(maxRemoteTalkers <= VoiceChatTypes::MAX_REMOTE_TALKERS);
	
	bool success = false;

	vcDebug("Init :: CpuAffinity: %d, MaxLocalTalkers: %u, MaxRemoteTalkers: %u",
			cpuAffinity,
			maxLocalTalkers,
			maxRemoteTalkers);

	rtry
	{
		rverify(!sm_Initialized, catchall, vcError("Init :: Already initialized"));

		rverify(this->NativeInit(maxLocalTalkers,
								 maxRemoteTalkers,
								 audioDevice,
								 cpuAffinity),
				catchall,
				vcError("Init :: NativeInit failed, Check Audio is Initialised"));

		vcAssert(!m_NumTalkers);
		vcAssert(!m_NumPeers);
		vcAssert(!m_NumLocalTalkers);

		m_MaxLocalTalkers = maxLocalTalkers;
		m_MaxRemoteTalkers = maxRemoteTalkers;

		m_LocalOnly = true;

		success = sm_Initialized = true;
	}
	rcatchall
	{
		this->Shutdown();
	}

	return success;
}

bool
VoiceChat::InitNetwork(netConnectionManager* cxnMgr,
                       const unsigned channelId)
{
    vcAssert(cxnMgr);

    bool success = false;

	vcDebug("InitNetwork :: ChannelId: %u...", channelId);

    rtry
    {
		rverify(sm_Initialized, catchall, vcError("InitNetwork :: Core not initialized"));
		rverify(!sm_InitializedNetwork, catchall, vcError("InitNetwork :: Network already initialized"));

        vcAssert(m_PktCursor == m_VoicePacket);
        
        m_ChannelId = channelId;
        m_LastUpdateTime = 0;
        m_LastSendTime = 0;
        m_MinSendInterval = DEFAULT_MIN_SEND_INTERVAL;
		m_PktCursor = m_VoicePacket;
		m_LocalOnly = false;

#if __ASSERT
        for(int i = 0; i < VoiceChatTypes::MAX_TALKERS; ++i)
        {
            vcAssert(!m_Talkers[i]);
            vcAssert(!m_Peers[i]);
        }
#endif  //__ASSERT

        //Start listening for network events.
        if(cxnMgr)
        {
            cxnMgr->Subscribe(&m_EventSubscriber);
        }

		SetConnectionManager(cxnMgr);

		for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
		{
			sm_DataPendingSize[i] = 0;
			memset(sm_VoiceData[i], 0, MAX_DATA_SIZE);
		}

        success = sm_InitializedNetwork = true;

        RefreshChatPrivsMask();

#if RSG_DURANGO
		VoiceChatDurangoSingleton::GetInstance().RefreshHeadsetState();
#endif
    }
    rcatchall
    {
        this->Shutdown();
    }

    return success;
}

bool 
VoiceChat::InitWorkerThread(const int cpuAffinity)
{
	bool success = false;

	rtry
	{
		vcAssert(sysIpcThreadIdInvalid == m_ThreadHandle);
		vcAssert(sysIpcCurrentThreadIdInvalid == m_ThreadId);

		m_Sema = sysIpcCreateSema(0);

		rverify(m_Sema, catchall, vcError("Error creating semaphore"));

		m_StopChatThread = false;

		m_ThreadHandle =
			sysIpcCreateThread(&UpdateChatThread,
			this,
			sysIpcMinThreadStackSize,
			PRIO_NORMAL,
			"[RAGE] VoiceChat Worker",
			cpuAffinity, 
			"RageVoiceChatWorker");

		rverify(sysIpcThreadIdInvalid != m_ThreadHandle,
			catchall,
			vcError("Error creating voice chat thread"));

		//Wait for the thread to start.
		sysIpcWaitSema(m_Sema);

		success = true; 
	}
	rcatchall
	{
		this->Shutdown();
	}

	return success;
}

void
VoiceChat::Shutdown()
{
	vcDebug("Shutdown");

	this->NativeShutdown();

	m_Delegator.Clear();
	
	m_MaxLocalTalkers = m_MaxRemoteTalkers = 0;
	m_EncBytesPerFrame = 0;
	m_VpdThreshold = DEFAULT_VPD_THRESHOLD;
	
	sm_Initialized = false;
}

void
VoiceChat::ShutdownNetwork()
{
    vcDebug("ShutdownNetwork");

    //Stop listening for presence events.
    if(s_PresenceDelegate.IsRegistered() && rlPresence::IsInitialized())
    {
        rlPresence::RemoveDelegate(&s_PresenceDelegate);
    }

    if(m_CxnMgr)
    {
        m_CxnMgr->Unsubscribe(&m_EventSubscriber);
    }

    //Remove any remaining talkers.
    this->RemoveAllTalkers();

	m_Delegator.Clear();

    vcAssert(0 == m_NumTalkers);
    vcAssert(0 == m_NumLocalTalkers);
    vcAssert(0 == m_NumPeers);
   
    SetConnectionManager(NULL);

    m_ChannelId = NET_INVALID_CHANNEL_ID;
    m_LastUpdateTime = 0;
	m_LastSendTime = 0;
	m_MinSendInterval = DEFAULT_MIN_SEND_INTERVAL;

	m_PktCursor = m_VoicePacket;
	
#if __ASSERT
    for(int i = 0; i < VoiceChatTypes::MAX_TALKERS; ++i)
    {
        vcAssert(!m_Talkers[i]);
        vcAssert(!m_Peers[i]);
    }
#endif  //__ASSERT

    s_HasChatPrivsMask = 0;

    m_LocalOnly = true;
    sm_InitializedNetwork = false;
}

void
VoiceChat::ShutdownWorkerThread()
{
	if(sysIpcThreadIdInvalid != m_ThreadHandle)
	{
		m_StopChatThread = true;
		//Wait for the thread to complete.
		sysIpcWaitSema(m_Sema);
		sysIpcWaitThreadExit(m_ThreadHandle);
		sysIpcDeleteSema(m_Sema);

		m_ThreadHandle = sysIpcThreadIdInvalid;
		m_ThreadId = sysIpcCurrentThreadIdInvalid;
		m_Sema = NULL;
	}
}

#if __BANK

void 
VoiceChat::InitWidgets()
{
	m_BankVpdThreshold = (float) m_VpdThreshold;
	m_BankVpdThresholdPTT = (float) m_VpdThresholdPTT;
	m_BankVpdCaptureHoldTimeMs = (u32) m_CaptureHoldTimeMs;

	bkBank* bank = BANKMGR.FindBank("Voice Chat");

	if(!bank)
	{
		bank = &BANKMGR.CreateBank("Voice Chat");
	}

	m_BankVpdSlider =
		bank->AddSlider("Voice Proximity Threshold",
		&m_BankVpdThreshold,
		0.0f,
		MAX_VPD_THRESHOLD,
		1.0f);

	m_BankVpdSliderPTT =
		bank->AddSlider("Push-To-Talk VPD Threshold",
		&m_BankVpdThresholdPTT,
		0.0f,
		MAX_VPD_THRESHOLD,
		1.0f);

	m_BankSliderVpdHoldTime =
		bank->AddSlider("VPD Hold Time",
		&m_BankVpdCaptureHoldTimeMs,
		0,
		MAX_VPD_HOLD_TIME_MS,
		100);
}

void 
VoiceChat::ShutdownWidgets()
{
	if(m_BankVpdSlider)
	{
		bkBank* bank = BANKMGR.FindBank("Voice Chat");
		if(bank)
		{
			bank->Remove(*m_BankVpdSlider);
			m_BankVpdSlider = NULL;
		}
		else
		{
			vcAssert(false);
		}
	}

	if (m_BankVpdSliderPTT)
	{
		bkBank* bank = BANKMGR.FindBank("Voice Chat");
		if(bank)
		{
			bank->Remove(*m_BankVpdSliderPTT);
			m_BankVpdSliderPTT = NULL;
		}
		else
		{
			vcAssert(false);
		}
	}

	if (m_BankSliderVpdHoldTime)
	{
		bkBank* bank = BANKMGR.FindBank("Voice Chat");
		if(bank)
		{
			bank->Remove(*m_BankSliderVpdHoldTime);
			m_BankSliderVpdHoldTime = NULL;
		}
		else
		{
			vcAssert(false);
		}
	}
}

#endif //__BANK

bool
VoiceChat::IsInitialized() const
{
	return sm_Initialized;
}

bool
VoiceChat::IsInitializedNetwork() const
{
    return sm_InitializedNetwork;
}

void
VoiceChat::SetSendInterval(const unsigned interval)
{
#if USE_SPEEX
    vcAssertf(interval < AUD_BUF_DURATION_MS,
                "Send interval must be less than %d milliseconds", AUD_BUF_DURATION_MS);
#endif  //__USE_SPEEX
    m_MinSendInterval = interval;
}

unsigned
VoiceChat::GetSendInterval() const
{
    return m_MinSendInterval;
}

void
VoiceChat::SetVpdThreshold(const unsigned threshold)
{
    SYS_CS_SYNC(m_Cs);

    if(!IsInitialized())
    {
        vcError("SetVpdThreshold :: Not initialized");
    }
    else
    {
        vcAssert(threshold <= MAX_VPD_THRESHOLD);
        m_VpdThreshold = (int) threshold;

#if __WIN32PC
        vcDebug("SetVpdThreshold :: %u", m_VpdThreshold);

        if(IsCaptureDeviceAvailable())
        {
            RVoice::SetCaptureThreshold(m_CaptureDevice, m_VpdThreshold / (float)MAX_VPD_THRESHOLD);
        }
#elif RSG_DURANGO
		// TODO: Durango implementation.
#elif USE_LIBVOICE
#if RSG_ORBIS
		// there is no way to set the threshold on PS4. libVoice handles it internally.
#endif
#endif // __WIN32PC

#if __BANK
        m_BankVpdThreshold = (float) m_VpdThreshold;
        if(m_BankVpdSlider)
        {
            m_BankVpdSlider->Changed();
        }
#endif  //__BANK
    }
}

void VoiceChat::SetVpdThresholdPTT(const unsigned threshold)
{
	SYS_CS_SYNC(m_Cs);

	if(!IsInitialized())
	{
		vcError("SetVpdThresholdPTT :: Not initialized");
	}
	else
	{
		vcAssert(threshold <= MAX_VPD_THRESHOLD);
		m_VpdThresholdPTT = (int) threshold;

#if RSG_PC
		vcDebug("SetVpdThresholdPTT :: %u", m_VpdThresholdPTT);

		if(IsCaptureDeviceAvailable())
		{			
			RVoice::SetPushToTalkCaptureThreshold(m_CaptureDevice, m_VpdThresholdPTT / (float)MAX_VPD_THRESHOLD);
		}

#if __BANK
		m_BankVpdThresholdPTT = (float) m_VpdThresholdPTT;
		if (m_BankVpdSliderPTT)
		{
			m_BankVpdSliderPTT->Changed();
		}
#endif  //__BANK
#endif // RSG_PC
	}
}


void VoiceChat::SetCaptureHoldTimeMs(const u32 holdTimeMs)
{
	SYS_CS_SYNC(m_Cs);

	if(!IsInitialized())
	{
		vcError("SetCaptureHoldTimeMs :: Not initialized");
	}
	else
	{
		vcAssert(holdTimeMs <= MAX_VPD_HOLD_TIME_MS);
		m_CaptureHoldTimeMs = holdTimeMs;

#if RSG_PC
		vcDebug("SetCaptureHoldTimeMs :: %u", m_CaptureHoldTimeMs);

		if(IsCaptureDeviceAvailable())
		{
			RVoice::SetCaptureThreshold(m_CaptureDevice, m_CaptureHoldTimeMs);
		}

#if __BANK
		m_BankVpdCaptureHoldTimeMs = m_CaptureHoldTimeMs;
		if (m_BankSliderVpdHoldTime)
		{
			m_BankSliderVpdHoldTime->Changed();
		}
#endif  //__BANK
#endif // RSG_PC
	}
}

unsigned
VoiceChat::GetVpdThreshold() const
{
    return (unsigned) m_VpdThreshold;
}


unsigned 
VoiceChat::GetVpdThresholdPTT() const
{
	return (unsigned) m_VpdThresholdPTT;
}

//PURPOSE
// Returns the capture hold time 
u32 VoiceChat::GetCaptureHoldTimeMs() const
{
	return m_CaptureHoldTimeMs;
}

bool
VoiceChat::AddTalker(const rlGamerInfo& gamerInfo,
                    const EndpointId endpointId,
                    const unsigned talkerFlags)
{
    SYS_CS_SYNC(m_Cs);

    vcDebug("AddTalker :: %s (%s)...",
            gamerInfo.GetName(),
			gamerInfo.IsLocal() ? "Local" : "Remote");

    bool success = false;

    rtry
    {
        rcheck(IsInitialized(),
                catchall,
                vcError("AddTalker :: Not initialized"));

        rverify(gamerInfo.IsLocal() || (NET_IS_VALID_ENDPOINT_ID(endpointId)),
                catchall,
                vcError("AddTalker :: Invalid network endpoint"));                

        rverify(!this->HaveTalker(gamerInfo.GetGamerId()),
                catchall,
                vcError("AddTalker :: \"%s\" is already a registered talker", gamerInfo.GetName()));

        rverify(gamerInfo.IsRemote()
                || (gamerInfo.IsLocal() && m_NumLocalTalkers < m_MaxLocalTalkers),
                catchall,
                vcError("AddTalker :: Can't add any more local talkers"));

        rverify(gamerInfo.IsLocal()
                || (gamerInfo.IsRemote() && this->RemoteTalkerCount() < m_MaxRemoteTalkers),
                catchall,
                vcError("AddTalker :: 't add any more remote talkers"));

        Talker* talker = m_TalkerPool;
        rverify(talker,catchall,vcError("AddTalker :: Talker pool exhausted"));

        m_TalkerPool = talker->m_Next;
        talker->m_Next = NULL;
        m_Talkers[m_NumTalkers++] = talker;
        talker->Init(gamerInfo, talkerFlags);

        rcheck(this->NativeAddTalker(talker), FreeTalker,);

        if(gamerInfo.IsLocal())
        {
            rverify(!m_LocalTalkers[gamerInfo.GetLocalIndex()],
                    NativeRemove,
                    vcError("AddTalker :: Already have a local talker in slot %d",
                            gamerInfo.GetLocalIndex()));

            m_LocalTalkers[gamerInfo.GetLocalIndex()] = talker;
            ++m_NumLocalTalkers;

#if PROCESS_RAW_DATA
			//Reset loudness tracking
			gs_Loudness[gamerInfo.GetLocalIndex()] = 0.0f;
			gs_LoudnessMean[gamerInfo.GetLocalIndex()] = 0.0f;
#endif
		}
        else
        {
            Peer* peer = this->FindPeerByEndpointId(endpointId);

#if RL_SUPPORT_NETWORK_BOTS
            // if we are supporting bots we always want to add a new peer, this is so we can
            // use bots to test upstream bandwidth usage by the voice chat system
            if(gamerInfo.GetGamerHandle().IsBot())
            {
                peer = 0;
            }
#endif //RL_SUPPORT_NETWORK_BOTS

            if(peer)
            {
                rverify(peer->AddTalker(talker),
                        NativeRemove,
                        vcError("AddTalker :: Error adding talker to peer"));
            }
            else
            {
                peer = m_PeerPool;
                rverify(peer,NativeRemove,vcError("AddTalker :: Peer pool exhausted"));

                m_PeerPool = peer->m_Next;
                peer->m_Next = NULL;
                m_Peers[m_NumPeers++] = peer;
                peer->Init(endpointId);

                rverify(peer->AddTalker(talker),
                        FreePeer,
                        vcError("AddTalker :: Error adding talker to peer"));
            }

			if(VoiceChat::GetAudioProvider())
			{
				VoiceChat::GetAudioProvider()->NotifyRemotePlayerAdded(talker->m_GamerInfo.GetGamerId());
			}
        }

		TalkerEventTalkerAdded e(gamerInfo);
		m_Delegator.Dispatch(&e);

        success = true;
    }
    rcatch(FreePeer)
    {
        Peer* peer = m_Peers[m_NumPeers-1];
        m_Peers[m_NumPeers-1] = NULL;
        --m_NumPeers;
        peer->Shutdown();
        peer->m_Next = m_PeerPool;
        m_PeerPool = peer;

        rthrow(NativeRemove,);
    }
    rcatch(NativeRemove)
    {
        Talker* talker = m_Talkers[m_NumTalkers-1];

        this->NativeRemoveTalker(talker);

        rthrow(FreeTalker,);
    }
    rcatch(FreeTalker)
    {
        Talker* talker = m_Talkers[m_NumTalkers-1];
        m_Talkers[m_NumTalkers-1] = NULL;
        --m_NumTalkers;

        talker->Shutdown();
        talker->m_Next = m_TalkerPool;
        m_TalkerPool = talker;

        rthrow(catchall,);
    }
    rcatchall
    {
    }

    return success;
}

bool
VoiceChat::AddTalker(const rlGamerInfo& gamerInfo,
                    const EndpointId endpointId)
{
    return this->AddTalker(gamerInfo, endpointId, TALKER_FLAG_SEND_RECEIVE_VOICE);
}

int
VoiceChat::GetTalkerIndex(const rlGamerId& gamerId) const
{
	SYS_CS_SYNC(m_Cs);

	int index = -1;

	for(int i = 0; i < (int) m_NumTalkers; ++i)
	{
		if(m_Talkers[i]->m_GamerInfo.GetGamerId() == gamerId)
		{
			index = i;
			break;
		}
	}

	return index;
}

int
VoiceChat::GetTalkerIndex(const rlGamerHandle& gamerHandle) const
{
	SYS_CS_SYNC(m_Cs);

	int index = -1;

	for(int i = 0; i < (int) m_NumTalkers; ++i)
	{
		if(m_Talkers[i]->m_GamerInfo.GetGamerHandle() == gamerHandle)
		{
			index = i;
			break;
		}
	}

	return index;
}

int
VoiceChat::GetTalkerInfoIndex(const rlGamerId& gamerId) const
{
	int index = -1;

	for(int i = 0; i < (int) m_NumTalkerInfo; ++i)
	{
		if(m_TalkerInfo[i].m_GamerId == gamerId)
		{
			index = i;
			break;
		}
	}

	return index;
}

#if RSG_DURANGO
int
VoiceChat::GetTalkerInfoIndex(const rlGamerHandle& gamerHandle) const
{
	int index = -1;

	for(int i = 0; i < (int) m_NumTalkerInfo; ++i)
	{
		if(m_TalkerInfo[i].m_hGamer == gamerHandle)
		{
			index = i;
			break;
		}
	}

	return index;
}
#endif

bool
VoiceChat::HaveTalker(const rlGamerId& gamerId) const
{
	return NULL != this->FindTalkerByGamerId(gamerId);
}

bool
VoiceChat::RemoveTalker(const int talkerIdx)
{
    SYS_CS_SYNC(m_Cs);

    if(talkerIdx >= 0)
    {
        vcAssert(m_NumTalkers > 0);

        Talker* talker = m_Talkers[talkerIdx];

		vcDebug("RemoveTalker :: %s (%s)...",
				talker->m_GamerInfo.GetName(),
                talker->m_GamerInfo.IsLocal() ? "Local" : "Remote");

        this->NativeRemoveTalker(talker);

        m_Talkers[talkerIdx] = m_Talkers[m_NumTalkers-1];
        m_Talkers[m_NumTalkers-1] = NULL;
        --m_NumTalkers;

        if(talker->m_GamerInfo.IsLocal())
        {
            vcAssert(m_NumLocalTalkers > 0);
            vcAssert(talker == m_LocalTalkers[talker->m_GamerInfo.GetLocalIndex()]);

#if PROCESS_RAW_DATA
			//Reset loudness tracking
			gs_Loudness[talker->m_GamerInfo.GetLocalIndex()] = 0.0f;
			gs_LoudnessMean[talker->m_GamerInfo.GetLocalIndex()] = 0.0f;
#endif
            m_LocalTalkers[talker->m_GamerInfo.GetLocalIndex()] = NULL;
            --m_NumLocalTalkers;
        }
        else
        {
            Peer* peer = talker->m_Peer;
            peer->RemoveTalker(talker);

            if(0 == peer->m_NumTalkers)
            {
                const int index = this->GetPeerIndex(peer->m_EndpointId);
                vcAssert(index >= 0);

                m_Peers[index] = m_Peers[m_NumPeers-1];
                m_Peers[m_NumPeers-1] = NULL;
                --m_NumPeers;

                peer->Shutdown();
                peer->m_Next = m_PeerPool;
                m_PeerPool = peer;
            }

			if(VoiceChat::GetAudioProvider())
			{
				VoiceChat::GetAudioProvider()->NotifyRemotePlayerRemoved(talker->m_GamerInfo.GetGamerId());
			}
        }

		TalkerEventTalkerRemoved e(talker->m_GamerInfo);
		m_Delegator.Dispatch(&e);

        talker->Shutdown();
        talker->m_Next = m_TalkerPool;
        m_TalkerPool = talker;

		return true;
    }

	return false;
}

bool 
VoiceChat::RemoveTalker(const rlGamerId& gamerId)
{
	SYS_CS_SYNC(m_Cs);

	int talkerIdx = GetTalkerIndex(gamerId);

	return RemoveTalker(talkerIdx);
}

void
VoiceChat::RemoveAllTalkers()
{
	vcDebug("RemoveAllTalkers");

    while(m_NumTalkers)
    {
        this->RemoveTalker(0);
    }

    vcAssert(0 == m_NumTalkers);
    vcAssert(0 == m_NumLocalTalkers);
}

#if RSG_DURANGO
bool 
VoiceChat::AddLocalChatUser(const int localIndex)
{
	return VoiceChatDurangoSingleton::GetInstance().AddLocalChatUser(localIndex);
}

void 
VoiceChat::RemoveLocalChatUser(const int localIndex)
{
	VoiceChatDurangoSingleton::GetInstance().RemoveLocalChatUser(localIndex);
}

bool
VoiceChat::AddRemoteChatUser(const rlGamerInfo& gamerInfo, const EndpointId endpointId)
{
	if(!vcVerify(gamerInfo.IsRemote()))
	{
		vcError("AddRemoteChatUser :: Local user should be added with AddLocalChatUser!");
		return false;
	}

	// If this is a remote user being added, we only want one of either the local or remote user
	//  to handle the new connection. Currently we will check to see which one has the higher XUID
	//  and let that one handle the new connection. This can break when we have more than one talker
	//  on a console.
	// TODO: Fix this when we add the ability to have multiple talkers on one console.
	bool shouldHandleNewConnection = true;

	int localTalkerIndex = -1;

	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
	{
		if (m_LocalTalkers[i])
		{
			localTalkerIndex = i;
			break;
		}
	}

	if (localTalkerIndex >= 0)
	{
		u64 localXuid = m_LocalTalkers[localTalkerIndex]->m_GamerInfo.GetGamerHandle().GetXuid();
		const u64 remoteUniqueId = gamerInfo.GetGamerHandle().GetXuid();

		if (localXuid < remoteUniqueId)
		{
			shouldHandleNewConnection = false;
		}
	}
	else
	{
		vcAssertf(false, "VoiceChat::AddChatUser - Can't find local xuid");
		return false;
	}

	return VoiceChatDurangoSingleton::GetInstance().AddRemoteChatUser(gamerInfo, endpointId, shouldHandleNewConnection);
}

bool
VoiceChat::RemoveRemoteChatUser(const rlGamerHandle& gamerHandle)
{
	return VoiceChatDurangoSingleton::GetInstance().RemoveRemoteChatUser(gamerHandle);
}

bool
VoiceChat::SetChatUserAsPendingRemove(const rlGamerHandle& gamerHandle)
{
	return VoiceChatDurangoSingleton::GetInstance().SetChatUserAsPendingRemove(gamerHandle);
}

void
VoiceChat::RemoveAllChatUsers()
{
	vcDebug("RemoveAllChatUsers");
	VoiceChatDurangoSingleton::GetInstance().RemoveAllChatUsers();
}
#endif // RSG_DURANGO

bool
VoiceChat::IsTalking(const int talkerIdx) const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

	if(talkerIdx >= 0)
	{	
		return m_TalkerInfo[talkerIdx].m_IsTalking;
	}

	return false;
}

bool 
VoiceChat::IsTalking(const rlGamerId& gamerId) const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

	int talkerIdx = GetTalkerInfoIndex(gamerId);

	return IsTalking(talkerIdx);
}

bool
VoiceChat::IsAnyTalking() const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

	bool anyTalking = false;

    for(int i = 0; i < (int) m_NumTalkerInfo; ++i)
    {
        if(m_TalkerInfo[i].m_IsTalking)
        {
            anyTalking = true;
            break;
        }
    }

    return anyTalking;
}

bool
VoiceChat::IsAnyLocalTalking() const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

    bool anyTalking = false;

    for(int i = 0; i < (int) m_NumTalkerInfo; ++i)
    {
        if(m_TalkerInfo[i].m_IsLocal && m_TalkerInfo[i].m_IsTalking)
        {
            anyTalking = true;
            break;
        }
    }

    return anyTalking;
}

bool
VoiceChat::IsAnyRemoteTalking() const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

	bool anyTalking = false;

    for(int i = 0; i < (int) m_NumTalkerInfo; ++i)
    {
		if(!m_TalkerInfo[i].m_IsLocal && m_TalkerInfo[i].m_IsTalking)
        {
            anyTalking = true;
            break;
        }
    }

    return anyTalking;
}

bool
VoiceChat::IsAboutToTalk(const int talkerIdx) const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

	if(talkerIdx >= 0)
	{
		return m_TalkerInfo[talkerIdx].m_IsAboutToTalk;
	}

	return false;
}

bool 
VoiceChat::IsAboutToTalk(const rlGamerId& gamerId) const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

	int talkerIdx = GetTalkerInfoIndex(gamerId);

	return IsAboutToTalk(talkerIdx);
}

bool
VoiceChat::IsAnyAboutToTalk() const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

	bool anyAbout = false;

    for(int i = 0; i < (int) m_NumTalkerInfo; ++i)
    {
        if(m_TalkerInfo[i].m_IsAboutToTalk)
        {
            anyAbout = true;
            break;
        }
    }

    return anyAbout;
}

bool
VoiceChat::IsAnyLocalAboutToTalk() const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

	bool anyAbout = false;

    for(int i = 0; i < (int) m_NumTalkerInfo; ++i)
    {
		if(m_TalkerInfo[i].m_IsLocal && m_TalkerInfo[i].m_IsAboutToTalk)
		{
            anyAbout = true;
            break;
        }
    }

    return anyAbout;
}

bool
VoiceChat::IsAnyRemoteAboutToTalk() const
{
	//Note that this function is using m_TalkerInfo (not m_Talkers)
	//for safe access across threads (and to avoid deadlock)

	bool anyAbout = false;

    for(int i = 0; i < (int) m_NumTalkerInfo; ++i)
    {
		if(!m_TalkerInfo[i].m_IsLocal && m_TalkerInfo[i].m_IsAboutToTalk)
		{
            anyAbout = true;
            break;
        }
    }

    return anyAbout;
}

bool 
VoiceChat::MicHasFocus()
{
#if !RSG_DURANGO
	return true;
#else
	return VoiceChatDurangoSingleton::GetInstance().MicHasFocus();
#endif // !RSG_DURANGO
}

bool
VoiceChat::IsMuted(const rlGamerHandle& WIN32PC_ONLY(hGamer) DURANGO_ONLY(hGamer))
{
#if RSG_PC
    return g_rlPc.GetPlayerManager()->IsBlocked(hGamer.GetRockstarId());
#elif RSG_DURANGO
	int talkerIdx = GetTalkerInfoIndex(hGamer);
	if(talkerIdx >= 0)
	{
		return m_TalkerInfo[talkerIdx].m_IsMuted;
	}
	return false;
#else
    return false;
#endif // RSG_PC
}

bool
VoiceChat::HasChatPrivileges(const int localGamerIndex,
                             const rlGamerHandle& ORBIS_ONLY(hGamer) DURANGO_ONLY(hGamer))
{
    bool hasPriv = false;

    if(rlPresence::IsSignedIn(localGamerIndex))
    {
        hasPriv = true;
#if RSG_DURANGO
		int talkerIdx = GetTalkerInfoIndex(hGamer);
		if(talkerIdx >= 0)
		{
			hasPriv = !m_TalkerInfo[talkerIdx].m_IsBlocked;
		}
#elif RSG_NP
        if(rlPresence::IsOnline(localGamerIndex))
        {
			hasPriv = ((s_HasChatPrivsMask & (1 << localGamerIndex)) != 0);
            if (hasPriv && g_rlNp.GetBasic().IsPlayerOnBlockList(hGamer))
            {
                hasPriv = false;
            }
        }
        else
        {
            hasPriv = true;
        }
#endif // RSG_DURANGO
    }

    return hasPriv;
}

bool
VoiceChat::IsVoiceOverSpeakers() const
{
	if(!IsInitialized())
    {
        vcError("VoiceChat not initialized");
    }
    else
    {
#if RSG_PC
        return false;
#elif USE_SPEEX
        return true;
#elif USE_LIBVOICE
        return true;
#endif // RSG_PC
    }

    return false;
}

bool
VoiceChat::HasHeadset(const int DURANGO_ONLY(localGamerIndex)) const
{
	if(!IsInitialized())
    {
        vcError("VoiceChat not initialized");
    }
    
#if RSG_PC
    return this->NativeHasHeadset(0);
#elif USE_SPEEX
    return m_MicDevNum >= 0;
#elif USE_LIBVOICE
	int value = 0; 
	int ret = sceVoiceGetPortAttr(m_MicPort, SCE_VOICE_ATTR_AUDIOINPUT_SILENT_STATE, &value, sizeof(value));
	return (ret == SCE_OK) && (value & SCE_AUDIO_IN_SILENT_STATE_DEVICE_NONE) == 0;
#elif RSG_DURANGO
	return VoiceChatDurangoSingleton::GetInstance().HasHeadset(localGamerIndex) || VoiceChatDurangoSingleton::GetInstance().HasSharedAudioDevice(localGamerIndex);
#else
	return false;
#endif // RSG_PC
}

unsigned 
VoiceChat::GetTalkerFlags(const int talkerIdx) const
{
	SYS_CS_SYNC(m_Cs);

	unsigned flags = 0;
	
	if(talkerIdx >= 0)
	{
		flags = m_Talkers[talkerIdx]->m_Flags;
	}

	return flags;
}

unsigned 
VoiceChat::GetTalkerFlags(const rlGamerId& gamerId) const
{
	SYS_CS_SYNC(m_Cs);

	int talkerIdx = GetTalkerIndex(gamerId);

	return GetTalkerFlags(talkerIdx);
}

void 
VoiceChat::SetTalkerFlags(const int talkerIdx, const unsigned flags)
{
    SYS_CS_SYNC(m_Cs);

    if(talkerIdx >= 0)
	{
		m_Talkers[talkerIdx]->m_Flags = flags;
	}
}

void 
VoiceChat::SetTalkerFlags(const rlGamerId& gamerId, const unsigned flags)
{
	SYS_CS_SYNC(m_Cs);

	int talkerIdx = GetTalkerIndex(gamerId);

	SetTalkerFlags(talkerIdx, flags);
}

float 
VoiceChat::GetLoudness(const int PROCESS_RAW_DATA_ONLY(localGamerIndex)) const
{
	float loudness = 0.0f;

#if PROCESS_RAW_DATA
	loudness = gs_Loudness[localGamerIndex];
#else

#if RSG_DURANGO
	loudness = VoiceChatDurangoSingleton::GetInstance().GetMicLoudness();
#elif RSG_PC
	loudness = GetLocalTalkerSpeakingVolume();
#endif // RSG_DURANGO

#endif // PROCESS_RAW_DATA

	return loudness;
}

unsigned 
VoiceChat::GetTalkerLastSendTime(const int talkerIdx) const
{
	SYS_CS_SYNC(m_Cs);

	unsigned lastSendTime = 0;
	
	if(talkerIdx >= 0)
	{
		lastSendTime = m_Talkers[talkerIdx]->m_LastSend;
	}

	return lastSendTime;
}

unsigned 
VoiceChat::GetTalkerLastSendTime(const rlGamerId& gamerId) const
{
	SYS_CS_SYNC(m_Cs);

	int talkerIdx = GetTalkerIndex(gamerId);

	return GetTalkerLastSendTime(talkerIdx);
}

unsigned 
VoiceChat::GetTalkerLastReceiveTime(const int talkerIdx) const
{
	SYS_CS_SYNC(m_Cs);

	unsigned lastReceiveTime = 0;

	if(talkerIdx >= 0)
	{
		lastReceiveTime = m_Talkers[talkerIdx]->m_LastReceive;
	}

	return lastReceiveTime;
}

unsigned 
VoiceChat::GetTalkerLastReceiveTime(const rlGamerId& gamerId) const
{
	SYS_CS_SYNC(m_Cs);

	int talkerIdx = GetTalkerIndex(gamerId);

	return GetTalkerLastReceiveTime(talkerIdx);
}

void 
VoiceChat::SetTalkerAudioParam(const int talkerIdx, const u32 paramNameHash, const float paramValue)
{
	SYS_CS_SYNC(m_Cs);

	if(talkerIdx >= 0)
	{
		NativeSetAudioParam(m_Talkers[talkerIdx], paramNameHash, paramValue);
	}
}

void 
VoiceChat::SetTalkerAudioParam(const rlGamerId& gamerId, const u32 paramNameHash, const float paramValue)
{
	SYS_CS_SYNC(m_Cs);

	int talkerIdx = GetTalkerIndex(gamerId);

	return SetTalkerAudioParam(talkerIdx, paramNameHash, paramValue);
}

void
VoiceChat::Update()
{
    SYS_CS_SYNC(m_Cs);

    if(IsInitialized())
    {
        vcAssert(m_LocalOnly || m_CxnMgr);

        //When rlPresence is initialized start listening for presence events.
        if(!s_PresenceDelegate.IsRegistered() && rlPresence::IsInitialized())
        {
            rlPresence::AddDelegate(&s_PresenceDelegate);
            RefreshChatPrivsMask();

#if RSG_DURANGO
			VoiceChatDurangoSingleton::GetInstance().RefreshHeadsetState();
#endif
        }

        //Run Update() only from the worker thread.
        //Unless there is no worker thread, then always run it.
        if(sysIpcCurrentThreadIdInvalid != m_ThreadId
            && sysIpcGetCurrentThreadId() != m_ThreadId)
        {
            // copy talker info into thread safe structures
			for(int i = 0; i < (int) m_NumTalkers; ++i)
			{
				m_TalkerInfo[i].m_GamerId = m_Talkers[i]->m_GamerInfo.GetGamerId();
				m_TalkerInfo[i].m_IsTalking = m_Talkers[i]->m_IsTalking;
				m_TalkerInfo[i].m_IsAboutToTalk = m_Talkers[i]->m_AboutToTalkTimer > 0;
				m_TalkerInfo[i].m_IsLocal = m_Talkers[i]->m_GamerInfo.IsLocal();
#if RSG_DURANGO
				m_TalkerInfo[i].m_hGamer = m_Talkers[i]->m_GamerInfo.GetGamerHandle();
				m_TalkerInfo[i].m_IsMuted = m_Talkers[i]->m_IsMuted;
				m_TalkerInfo[i].m_IsBlocked = m_Talkers[i]->m_IsBlocked;
#endif
			}
			m_NumTalkerInfo = static_cast<u8>(m_NumTalkers);

			// skip the rest of the update call
			return;
        }

        if(m_CxnMgr && m_EventSubscriber.IsSubscribed())
        {
            for(netEvent* evt = m_EventSubscriber.NextEvent(); evt; evt = m_EventSubscriber.NextEvent())
            {
                if(NET_EVENT_FRAME_RECEIVED == evt->GetId()
                    && evt->m_ChannelId == m_ChannelId)
                {
                    const netEventFrameReceived* fr = evt->m_FrameReceived;
					this->ReceiveChat(fr->m_Payload, fr->m_SizeofPayload);
				}
#if RSG_DURANGO
				else if(NET_EVENT_FRAME_RECEIVED == evt->GetId())
				{
					const netEventFrameReceived* fr = evt->m_FrameReceived;

					if(netMessage::IsMessage(fr->m_Payload, fr->m_SizeofPayload))
					{
						ReceiveMesssage(fr);
					}
				}
#endif // RSG_DURANGO
            }

#if RSG_DURANGO
			SendUserPackets();
#endif // RSG_DURANGO
        }

        const unsigned curTime = sysTimer::GetSystemMsTime() | 0x01;

        const unsigned timeStep =
            m_LastUpdateTime
                ? (curTime - m_LastUpdateTime)
                : 0;

        m_LastUpdateTime = curTime;

		for(int i = 0; i < (int) m_NumTalkers; ++i)
        {
            Talker* talker = m_Talkers[i];

#if RSG_DURANGO
			const u64 uniqueId = talker->m_GamerInfo.GetGamerHandle().GetXuid();
			talker->m_IsTalking = VoiceChatDurangoSingleton::GetInstance().IsTalking(uniqueId);
			talker->m_IsMuted = VoiceChatDurangoSingleton::GetInstance().IsMuted(uniqueId);
			talker->m_IsBlocked = VoiceChatDurangoSingleton::GetInstance().IsBlocked(uniqueId);
#else
			talker->m_IsTalking = this->NativeIsTalking(talker);
#endif

            if(talker->m_AboutToTalkTimer > 0)
            {
                if(talker->m_IsTalking)
                {
                    //No longer about to talk - he *is* talking.
                    talker->m_AboutToTalkTimer = 0;
                }
                else
                {
					talker->m_AboutToTalkTimer -= (int) timeStep;
					
                    if(talker->m_AboutToTalkTimer < 0)
                    {
                        talker->m_AboutToTalkTimer = 0;
                    }
                }
            }
        }

        for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
        {
            const Talker* localTalker = m_LocalTalkers[i];
            if(!localTalker){continue;}

            for(int j = 0; j < (int) m_NumPeers; ++j)
            {
                Peer* peer = m_Peers[j];
                peer->m_Muted = false;

                for(int k = 0; k < (int) peer->m_NumTalkers && !peer->m_Muted; ++k)
                {
                    const Talker* remoteTalker = peer->m_Talkers[k];

                    peer->m_Muted = this->IsMuted(remoteTalker->m_GamerInfo.GetGamerHandle());
                }
            }
        }

        this->NativeUpdate(timeStep);

#if USE_SPEEX
        // flag if we have detected voice activity above the specified threshold while gathering the current batch of voice data
        if(m_Vpd >= m_VpdThreshold)
        {
            m_VoiceActivityDuringCurrentFrame = true;
        }
#endif

		// pack local voice data
		this->PackChat();

		//Should we send voice - Assume false
		bool sendPacket = false;
		for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
		{
			//If any pending buffer has less than one frame of data remaining
			if((MAX_DATA_SIZE - sm_DataPendingSize[i]) < m_EncBytesPerFrame)
			{
				sendPacket = true;
				break;
			}

			//If we have any data and the minimum time interval has elapsed
			if(sm_DataPendingSize[i] > 0 && ((m_LastUpdateTime - m_LastSendTime) > m_MinSendInterval))
			{
				sendPacket = true; 
				break;
			}
		}

		if(sendPacket)
		{
			//Build and send packet
			bool allSent = this->BuildPacket();
#if RSG_PC
			if(!m_MuteChatBecauseFocusLost)
			{
				this->SendFrame();
			}
			else
			{
				vcDebug("Update :: Not sending voice data due to focus being lost.");
				m_PktCursor = m_VoicePacket;
			}
#else
			this->SendFrame();			
#endif // RSG_PC

			//If all available data was sent, mark last sent time
			if(allSent)
			{
				m_LastSendTime = m_LastUpdateTime;
			}
#if USE_SPEEX
			m_VoiceActivityDuringCurrentFrame = false;
#endif
		}

#if __BANK
		static unsigned const BANK_UPDATE_INTERVAL = 1000;
		static unsigned s_LastBankUpdate = 0;
		if(m_LastUpdateTime - s_LastBankUpdate > BANK_UPDATE_INTERVAL)
		{
			s_LastBankUpdate = m_LastUpdateTime;
			if(this->GetVpdThreshold() != (unsigned) m_BankVpdThreshold)
			{
				this->SetVpdThreshold((unsigned) m_BankVpdThreshold);
			}
		}
#endif  //__BANK
    }
}

#if !__NO_OUTPUT

void
VoiceChat::LogContents()
{
	vcDebug("Channel ID: %d", m_ChannelId);
	vcDebug("Min Send Interval %d", m_MinSendInterval);
	vcDebug("VPD Threshold %d", m_VpdThreshold);
	vcDebug("Encoded Bytes Per Frame %d", m_EncBytesPerFrame);
#if RSG_NP
	vcDebug("Frame Duration MS %d", m_FrameDurationMs);
#endif
	vcDebug("Max Local Talkers %d", m_MaxLocalTalkers);
	vcDebug("Max Remote Talkers %d", m_MaxRemoteTalkers);
	vcDebug("Num Local Talkers %d", m_NumLocalTalkers);
	vcDebug("Num Remote Talkers %d", m_NumTalkers - m_NumLocalTalkers);
	vcDebug("Num Peers %d", m_NumPeers);
	for(int i = 0; i < (int) m_NumPeers; ++i)
	{
		const Peer* peer = m_Peers[i];

		vcDebug("   Peer %d: Has Chat Privileges %s", i, peer->m_HaveChatPrivs ? "True" : "False");
		vcDebug("   Peer %d: Is Muted %s", i, peer->m_Muted ? "True" : "False");
		vcDebug("   Peer %d: Num Talkers %d", i, peer->m_NumTalkers);

		for(int j = 0; j < (int) peer->m_NumTalkers; j++)
		{
			vcDebug("      Talker %d: Name: %s", j, peer->m_Talkers[j]->m_GamerInfo.GetName());
			vcDebug("      Talker %d: Flags: %d", j, peer->m_Talkers[j]->m_Flags);
			vcDebug("      Talker %d: Attempted Send: %s", j, peer->m_Talkers[j]->m_AttemptedSend ? "True" : "False");
			vcDebug("      Talker %d: Last Sent: %d", j, peer->m_Talkers[j]->m_LastSend);
			vcDebug("      Talker %d: Total Sent: %d", j, peer->m_Talkers[j]->m_TotalSent);
			vcDebug("      Talker %d: Last Received: %d", j, peer->m_Talkers[j]->m_LastReceive);
			vcDebug("      Talker %d: Total Received: %d", j, peer->m_Talkers[j]->m_TotalReceived);
		}
	}
}

#endif

//private:

void
VoiceChat::UpdateChatThread(void* arg)
{
    VoiceChat* vc = (VoiceChat*) arg;

    vc->m_ThreadId = sysIpcGetCurrentThreadId();

    //Signal the main thread that we've started
    sysIpcSignalSema(vc->m_Sema);

    while(!vc->m_StopChatThread)
    {
        vc->Update();
        sysIpcSleep(CHAT_THREAD_SLEEP_INTERVAL);
    }

    //Signal the main thread we've stopped.
    sysIpcSignalSema(vc->m_Sema);
}

unsigned
VoiceChat::RemoteTalkerCount() const
{
    const int count = m_NumTalkers - m_NumLocalTalkers;
    return vcVerify(count >= 0) ? count : 0;
}

VoiceChat::Talker*
VoiceChat::FindTalkerByGamerId(const rlGamerId& gamerId)
{
    SYS_CS_SYNC(m_Cs);

    const int index = this->GetTalkerIndex(gamerId);

    return index >= 0 ? m_Talkers[index] : NULL;
}

const VoiceChat::Talker*
VoiceChat::FindTalkerByGamerId(const rlGamerId& gamerId) const
{
    return const_cast<VoiceChat*>(this)->FindTalkerByGamerId(gamerId);
}

bool
VoiceChat::IsLocalTalker(const Talker* talker) const
{
    SYS_CS_SYNC(m_Cs);

    return talker >= (const Talker*)&m_LocalTalkers[0]
            && talker < (const Talker*)&m_LocalTalkers[RL_MAX_LOCAL_GAMERS];
}

int
VoiceChat::GetPeerIndex(const EndpointId endpointId) const
{
    SYS_CS_SYNC(m_Cs);

    int index = -1;

    for(int i = 0; i < (int) m_NumPeers; ++i)
    {
        if(endpointId == m_Peers[i]->m_EndpointId)
        {
            index = i;
            break;
        }
    }

    return index;
}

VoiceChat::Peer*
VoiceChat::FindPeerByEndpointId(const EndpointId endpointId)
{
    SYS_CS_SYNC(m_Cs);

    const int index = this->GetPeerIndex(endpointId);

    return index >= 0 ? m_Peers[index] : NULL;
}

const VoiceChat::Peer*
VoiceChat::FindPeerByEndpointId(const EndpointId endpointId) const
{
    return const_cast<VoiceChat*>(this)->FindPeerByEndpointId(endpointId);
}

void
VoiceChat::PackChat()
{
    SYS_CS_SYNC(m_Cs);

    vcAssert(IsInitialized());

	for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
    {
        Talker* talker = m_LocalTalkers[i];

        if(!talker)
        {
            //Drain the voice samples.
            unsigned size = MAX_DATA_SIZE;
            while(this->NativePackChat(i, sm_VoiceData[i], &size) && size)
            {
            }
            continue;
        }

        bool finished = false;
        unsigned bytesLeft = MAX_DATA_SIZE - sm_DataPendingSize[i];
        while(!finished && bytesLeft >= (int)m_EncBytesPerFrame)
        {
			u8* pDataCursor = sm_VoiceData[i] + sm_DataPendingSize[i];

            unsigned size = bytesLeft;
            if(this->NativePackChat(i, pDataCursor, &size) && size)
            {
                vcAssert(size <= (unsigned) bytesLeft);

                pDataCursor += size;
                bytesLeft -= size;
                sm_DataPendingSize[i] += size;
            }
            else
            {
                finished = true;
            }
        }
    }
}

bool 
VoiceChat::BuildPacket()
{
	bool sentAll = true; 

	for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
	{
		Talker* talker = m_LocalTalkers[i];

		//Ignore inactive local talkers
		if(!talker)
		{
			continue;
		}

		int bytesLeft = ptrdiff_t_to_int(&m_VoicePacket[sizeof(m_VoicePacket)] - m_PktCursor);
		int bytesRequired = VPKT_SIZEOF_HEADER + sm_DataPendingSize[i];

		//Check that we have enough space to send this data
		if(bytesLeft < bytesRequired)
		{
			//Indicate that further data is pending
			sentAll = false;
			break; 
		}

		u8* pktHdr = m_PktCursor;
		unsigned sizeofGamerId;

		const rlGamerId& gamerId = talker->m_GamerInfo.GetGamerId();

		//Write the packet header, consisting of the gamer id,
		//timestamp and the size of the packet.
		vcVerify(gamerId.Export(pktHdr, bytesLeft, &sizeofGamerId));

#if USE_SPEEX
		// write the timestamp
		u8 *pktTimestamp = &pktHdr[sizeofGamerId];
		u32 timestamp = m_VoiceStreamTimestamp;
		datBitBuffer bb;
		bb.SetReadWriteBytes(pktTimestamp, bytesLeft);
		bb.WriteUns(timestamp, sizeof(timestamp)<<3);

		// write the size of the voice data
		u8* pktSize = pktTimestamp + sizeof(timestamp);
#else
		u8* pktSize = &pktHdr[sizeofGamerId];
#endif

#if RSG_PC
		m_LocalTalkerSpeakingVolume = 0.0f;
		m_PackFrameStamp = true;
#endif // RSG_PC

		m_PktCursor = pktSize + VPKT_SIZEOF_SIZE;

		vcAssert(VPKT_SIZEOF_HEADER == unsigned(m_PktCursor - pktHdr));

		bytesLeft -= VPKT_SIZEOF_HEADER;

		//Copy in voice data and manage running counters
		memcpy(m_PktCursor, sm_VoiceData[i], sm_DataPendingSize[i]);
		m_PktCursor += sm_DataPendingSize[i];
		bytesLeft -= sm_DataPendingSize[i];

		//Update packet size
		CompileTimeAssert(2 == VPKT_SIZEOF_SIZE);
		pktSize[0] = u8(sm_DataPendingSize[i] & 0xFF);
		pktSize[1] = u8((sm_DataPendingSize[i] >> 8) & 0xFF);

		//Flush pending size
		sm_DataPendingSize[i] = 0;
		memset(sm_VoiceData[i], 0, MAX_DATA_SIZE);
	}

	return sentAll;
}

void
VoiceChat::ReceiveChat(const void* data, const unsigned sizeofData)
{
    SYS_CS_SYNC(m_Cs);

    vcAssert(IsInitialized());

    if(m_LocalOnly)
    {
        return;
    }

	rlGamerId gamerId;
    unsigned sizeofId;

    const u8* p = (const u8*) data;
    const u8* eod = p + sizeofData;
    unsigned bytesLeft = sizeofData;

    while(p < eod)
    {
        // read the gamer ID
        if(!vcVerify(gamerId.Import(p, bytesLeft, &sizeofId))
            || !vcVerify(bytesLeft >= (sizeofId + VPKT_SIZEOF_TIMESTAMP + VPKT_SIZEOF_SIZE + m_EncBytesPerFrame)))
        {
            break;
        }

        p += sizeofId;

        // read the timestamp
        u32 timestamp = 0;
#if USE_SPEEX
        datBitBuffer bb;
        bb.SetReadOnlyBytes(p, sizeof(timestamp));
        const bool success = bb.ReadUns(timestamp, sizeof(timestamp)<<3);

        if(!vcVerify(success))
        {
            break;
        }

        p += sizeof(timestamp);
#endif

        // read the voice data and submit it
        CompileTimeAssert(2 == VPKT_SIZEOF_SIZE);
        const unsigned pktSize = unsigned(p[0]) | (p[1] << 8);
        vcAssert(pktSize >= m_EncBytesPerFrame);

        p += VPKT_SIZEOF_SIZE;

        Talker* talker = this->FindTalkerByGamerId(gamerId);

        if(talker)
        {
            vcAssert(talker->m_GamerInfo.IsRemote());

			if((TALKER_FLAG_RECEIVE_VOICE & talker->m_Flags))
			{
				talker->m_AboutToTalkTimer = ABOUT_TO_START_TALKING_TIME;
			
				this->NativeSubmitChat(talker, p, pktSize, timestamp);

	#if !__NO_OUTPUT
				talker->m_TotalReceived += pktSize;

				static unsigned totalSize = 0;
				totalSize += pktSize;
				vcDebug("ReceiveChat :: From: %s, Received: %db. Total: %db, Interval: %d", talker->m_GamerInfo.GetName(), pktSize, talker->m_TotalReceived, m_LastUpdateTime - talker->m_LastReceive);
	#endif // !__NO_OUTPUT
			}
			else
			{
				vcDebug("ReceiveChat :: From: %s. Received: %db. Receive Flag Disabled", talker->m_GamerInfo.GetName(), pktSize);
			}

			talker->m_LastReceive = m_LastUpdateTime;
        }
		else
		{
#if !__NO_OUTPUT
			vcDebug("ReceiveChat :: Unknown GamerId: %" I64FMT "u. Received: %db", gamerId.GetId(), pktSize);
#endif // !__NO_OUTPUT
		}

        p += pktSize;
        vcAssert(p <= eod);
    }
}

#if RSG_DURANGO
void
VoiceChat::ReceiveMesssage(const netEventFrameReceived* fr)
{
	VoiceChatDurangoSingleton::GetInstance().ReceiveMessage(fr);
}

void
VoiceChat::SendUserPackets()
{
	int localTalkerIndex = -1;

	for (int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
	{
		if (m_LocalTalkers[i])
		{
			localTalkerIndex = i;
			break;
		}
	}

	if (localTalkerIndex >= 0)
	{
		u64 localXuid = m_LocalTalkers[localTalkerIndex]->m_GamerInfo.GetGamerHandle().GetXuid();
		VoiceChatDurangoSingleton::GetInstance().SendUserPackets(localXuid);
	}
}
#endif // RSG_DURANGO

void
VoiceChat::SendFrame()
{
#if __ASSERT
	if(m_CxnMgr)
	{
		netChannelPolicies policies;
		// Done outside of the mutex to avoid a deadlock
		m_CxnMgr->GetChannelPolicies(m_ChannelId, &policies);
		vcAssert(!policies.m_CompressionEnabled);
	}
#endif  //__ASSERT

	u8 voicePacket[MAX_PACKET_SIZE];
	EndpointId endpoints[VoiceChatTypes::MAX_TALKERS];
	unsigned numEndpoints = 0;
	unsigned size = 0;
	const unsigned channelId = m_ChannelId;

#if !__NO_OUTPUT		
	bool sentVoice = false;
	unsigned sends = 0;
#endif

	{
		SYS_CS_SYNC(m_Cs);

		vcAssert(IsInitialized());

		if(m_PktCursor > m_VoicePacket)
		{
#if RSG_PC
			Talker* localTalker = m_LocalTalkers[0];
			if(localTalker && (m_LocalEchoEnabled || m_IsVoiceTestMode))
			{
				NativeSubmitChat(localTalker, m_VoicePacket + VPKT_SIZEOF_HEADER,
					ptrdiff_t_to_int(m_PktCursor - m_VoicePacket - VPKT_SIZEOF_HEADER), 0);
			}
#endif

			if(m_LocalOnly == false WIN32PC_ONLY(&& !m_IsVoiceTestMode))
			{
				size = ptrdiff_t_to_int(m_PktCursor - m_VoicePacket);

				for(int i = 0; i < (int) m_NumPeers; ++i)
				{
					const Peer* peer = m_Peers[i];

					if(peer->m_Muted || !peer->m_HaveChatPrivs){continue;}

					bool sendIt = false;

					for(int j = 0; j < (int) peer->m_NumTalkers; ++j)
					{
						if(TALKER_FLAG_SEND_VOICE & peer->m_Talkers[j]->m_Flags)
						{
							sendIt = true;
						
							// update tracking values
							peer->m_Talkers[j]->m_LastSend = m_LastUpdateTime;
#if !__NO_OUTPUT
							peer->m_Talkers[j]->m_AttemptedSend = true; 
							peer->m_Talkers[j]->m_TotalSent += size;
#endif
						}
					}

					if(sendIt && m_CxnMgr)
					{
						if (numEndpoints == 0)
						{
							// At least one person gets the voice data so we copy it into the temporary buffer
							memcpy(voicePacket, m_VoicePacket, size);
						}

						if (netVerify(numEndpoints < COUNTOF(endpoints)))
						{
							endpoints[numEndpoints] = peer->m_EndpointId;
							++numEndpoints;

#if !__NO_OUTPUT
							// use talker 0 to identify this peer (temporary for url:bugstar:936323)
							const Talker* talker = peer->m_Talkers[0];
							vcDebug("SendFrame :: To: %s, Sending: %db, Total: %db", talker->m_GamerInfo.GetName(), size, talker->m_TotalSent);
#endif
						}
					}
				}

			}

			m_PktCursor = m_VoicePacket;
		}
	}

	for (unsigned i = 0; i < numEndpoints; ++i)
	{
		// SendOutOfBand has to be done outside of the mutex to avoid deadlocks when other threads access VoiceChat methods
		OUTPUT_ONLY(bool success = ) m_CxnMgr->SendOutOfBand(endpoints[i],
			channelId,
			voicePacket,
			size,
			0);

	#if !__NO_OUTPUT
		if (success)
		{
			sentVoice = true;
			sends++;
		}
		else
		{
			vcDebug("SendFrame :: Failed to send voice to endpoint %u.", endpoints[i]);
		}
	#endif //!__NO_OUTPUT
	}

#if !__NO_OUTPUT
	if(sentVoice)
	{
		static unsigned totalSize = 0;
		totalSize += (size * sends);
		vcDebug("SendFrame :: Summary - Sent: %db. Total: %db. Interval: %d", (size * sends), totalSize, m_LastUpdateTime - m_LastSendTime);
	}
#endif
}

void VoiceChat::SetConnectionManager(netConnectionManager* cxnMgr)
{
	m_CxnMgr = cxnMgr;

#if RSG_DURANGO
	VoiceChatDurangoSingleton::GetInstance().SetConnectionManager(cxnMgr);
#endif // RSG_DURANGO
}

#if RSG_DURANGO
bool
VoiceChat::NativeInit(const int UNUSED_PARAM(maxLocalTalkers),
					  const int UNUSED_PARAM(maxRemoteTalkers),
					  void* UNUSED_PARAM(audioDevice),
					  const int UNUSED_PARAM(cpuAffinity))
{
	return VoiceChatDurangoSingleton::GetInstance().Init();
}

void
VoiceChat::NativeShutdown()
{
	VoiceChatDurangoSingleton::GetInstance().Shutdown();
}

bool
VoiceChat::NativeAddTalker(Talker* talker)
{
	talker;
	return true;
}

void
VoiceChat::NativeRemoveTalker(Talker* talker)
{
	talker;
}

bool
VoiceChat::NativeIsTalking(const Talker* talker) const
{
	const u64 uniqueId = talker->m_GamerInfo.GetGamerHandle().GetXuid();
	return VoiceChatDurangoSingleton::GetInstance().IsTalking(uniqueId);
}

void
VoiceChat::NativeUpdate(const unsigned UNUSED_PARAM(timeStep))
{
	VoiceChatDurangoSingleton::GetInstance().Update();
}

bool
VoiceChat::NativePackChat(const int localTalkerIndex,
						  void* dst,
						  unsigned* numBytes)
{
	if (!m_LocalTalkers[localTalkerIndex])
	{
		return false;
	}

	return VoiceChatDurangoSingleton::GetInstance().PackChat(m_LocalTalkers[localTalkerIndex]->m_GamerInfo.GetGamerHandle().GetXuid(), dst, numBytes);
}

void
VoiceChat::NativeSubmitChat(Talker* talker,
							const void* data,
							const unsigned sizeofData,
							const u32 UNUSED_PARAM(timestamp))
{
	u64 remoteXuid = talker->m_GamerInfo.GetGamerHandle().GetXuid();
	VoiceChatDurangoSingleton::GetInstance().SubmitChat(remoteXuid,
														data,
														sizeofData);
}

void
VoiceChat::NativeSetAudioParam(Talker* UNUSED_PARAM(talker),
							   const u32 UNUSED_PARAM(paramNameHash),
							   const float UNUSED_PARAM(paramValue))
{

}

#elif RSG_NP

#if USE_LIBVOICE
static int 
CreatePort_Device(unsigned &port,CellVoicePortType portType,int playerId)
{
    CellVoicePortParam      PortArgs;
    PortArgs.portType                  = portType;
    PortArgs.bMute                     = false;
    PortArgs.threshold                 = 100;                   //*** WARNING! Unused at the moment.
    PortArgs.volume                    = 1.0f;

// 	SceUserServiceUserId userId;/**< User ID. see <c>user_service.h</c> */	
// 	int32_t type;				/**< Device type. <c>SCE_AUDIO_IN_TYPE_VOICE</c> for #SCE_VOICE_PORTTYPE_IN_DEVICE; <c>SCE_AUDIO_OUT_PORT_TYPE_VOICE</c> for #SCE_VOICE_PORTTYPE_OUT_DEVICE. */
// 	int32_t index;				/**< Device index. See the document <i>User Management Overview</i>. */
	PortArgs.device.userId           = playerId;
	PortArgs.device.index            = 0; // TODO: NS - not used? must be 0?

	if(portType == SCE_VOICE_PORTTYPE_IN_DEVICE)
	{
		PortArgs.device.type		 =  SCE_AUDIO_IN_TYPE_VOICE;
	}
	else if(vcVerify(portType == SCE_VOICE_PORTTYPE_OUT_DEVICE))
	{
		PortArgs.device.type		 =  SCE_AUDIO_OUT_PORT_TYPE_VOICE;
	}

    return cellVoiceCreatePort(&port, &PortArgs );
}

static int 
CreatePort_Voice(unsigned &port,CellVoicePortType portType)
{
    CellVoicePortParam      PortArgs;
    PortArgs.portType                  = portType;
    PortArgs.bMute                     = false;
    PortArgs.threshold                 = 100;
    PortArgs.volume                    = 1.0f;

#if RSG_ORBIS
	PortArgs.voice.bitrate             = SCE_VOICE_BITRATE_7300;
#else
	PortArgs.voice.bitrate             = CELLVOICE_BITRATE_3850;
#endif

    return cellVoiceCreatePort(&port, &PortArgs);
}

static int 
CreatePort_PCM(unsigned &port,CellVoicePortType portType)
{
	CellVoicePortParam      PortArgs;
	PortArgs.portType                  = portType;
	PortArgs.bMute                     = false;
	// For threshold, documentation states maximum is:
	//16-bit integer: (PCM-buffer-size/512*16) ms  
	PortArgs.threshold                 = 0;
	PortArgs.volume                    = 1.0f;
	PortArgs.pcmaudio.bufSize		   = 2048;
	PortArgs.pcmaudio.format.dataType = CELLVOICE_PCM_SHORT;
	PortArgs.pcmaudio.format.sampleRate = CELLVOICE_SAMPLINGRATE_16000;

	return cellVoiceCreatePort(&port, &PortArgs);
}

// Returns number of bytes in the shared buffer.  Whatever the hell that is.
/*
static int
GetPortSize(unsigned port)
{
    CellVoiceBasePortInfo   portInfo;
    memset(&portInfo,0,sizeof(portInfo));
    int ret = cellVoiceGetPortInfo(port, &portInfo);
    if (ret < 0)
        return ret;
    else
        return portInfo.numByte;
} */

static int
GetFrameSize(unsigned port)
{
    CellVoiceBasePortInfo   portInfo;
    memset(&portInfo,0,sizeof(portInfo));
    int ret = cellVoiceGetPortInfo(port, &portInfo);
    if (ret < 0)
        return ret;
    else
        return portInfo.frameSize;
}

#endif // USE_LIBVOICE

bool
VoiceChat::NativeInit(const int /*maxLocalTalkers*/,
                      const int /*maxRemoteTalkers*/,
                      void* /*audioDevice*/,
                      const int USE_SPEEX_ONLY(cpuAffinity))
{
    bool success = false;

    m_State = STATE_INVALID;

    rtry
    {
        m_VpdThreshold = DEFAULT_VPD_THRESHOLD;

#if USE_SPEEX
        int err;

        vcAssert(!m_WaitSema);

        vcDebug("NativeInit :: Loading CELL_SYSMODULE_MIC...");

        //Make sure the libmic module is available already.
        err = cellSysmoduleIsLoaded(CELL_SYSMODULE_MIC);
        rcheck(CELL_OK == err,
                catchall,
                vcError("Error:0x%08x. CELL_SYSMODULE_MIC wasn't loaded", err));

        ++m_State;

        vcDebug("NativeInit :: Initializing libmic...");

        //Initialize libmic.
        err = cellMicInit();
        rcheck(CELL_OK == err,
                catchall,
                vcError("NativeInit :: cellMicInit Failed: 0x%08x", err));

        ++m_State;

        mthRandom rng(sysTimer::GetSystemMsTime());

        vcDebug("NativeInit :: Creating mic event queue...");

        //create event queue to recv mic events
        const uint64_t equeue_key_base = 0x0000000072110700UL;
        sys_event_queue_attribute_t mic_queue_attr = {SYS_SYNC_FIFO, SYS_PPU_QUEUE, ""};
        m_MicQueueId = equeue_key_base | (rng.GetInt() & 0xFFFF);

        for(int tryCount = 0; tryCount < 100; ++tryCount)
        {
            err = sys_event_queue_create(&m_MicCallbackQueue,
                                        &mic_queue_attr,
                                        m_MicQueueId,
                                        32);
            if(CELL_OK == err){ break; }
            m_MicQueueId =  equeue_key_base | (rng.GetInt() & 0xFFFF);
        }

        rcheck(CELL_OK == err,
			catchall,
			vcError("NativeInit :: sys_event_queue_create Failed: 0x%08x", err));

        vcDebug("NativeInit :: MicCallbackQueue: %u", m_MicCallbackQueue);

        ++m_State;

        vcDebug("NativeInit :: Registering mic event queue...");

        //Register mic event queue.
        err = cellMicSetNotifyEventQueue(m_MicQueueId);
        rcheck(CELL_OK == err,
			catchall,
			vcError("NativeInit :: cellMicSetNotifyEventQueue Failed: 0x%08x", err));

        ++m_State;

        vcDebug("NativeInit :: Creating audio callback queue...");

        //create event queue to recv audio events
        sys_event_queue_attribute_t aud_queue_attr = {SYS_SYNC_FIFO, SYS_PPU_QUEUE, ""};
        m_AudioQueueId = equeue_key_base | (rng.GetInt() & 0xFFFF);

        for(int tryCount = 0; tryCount < 100; ++tryCount)
        {
            err = sys_event_queue_create(&m_AudioCallbackQueue,
                                        &aud_queue_attr,
                                        m_AudioQueueId,
                                        32);
            if(CELL_OK == err){ break; }
            m_AudioQueueId =  equeue_key_base | (rng.GetInt() & 0xFFFF);
        }

        rcheck(CELL_OK == err,
			catchall,
			vcError("NativeInit :: sys_event_queue_create Failed: 0x%08x", err));

        vcDebug("NativeInit :: AudioCallbackQueue: %u", m_AudioCallbackQueue);

        ++m_State;

        vcDebug("NativeInit :: Registering audio callback queue...");

        //Register audio event queue.
        err = cellAudioSetNotifyEventQueue(m_AudioQueueId);
        rcheck(CELL_OK == err,
			catchall,
			vcError("NativeInit :: cellAudioSetNotifyEventQueue Failed: 0x%08x", err));

        ++m_State;

        vcDebug("NativeInit :: Opening secondary audio port (headphones)...");

        //Open the audio port corresponding to the headphones in
        //a headset (i.e. CELL_AUDIO_PORTATTR_OUT_SECONDARY).
        CellAudioPortParam audPortParm = {0};
        audPortParm.nChannel    = CELL_AUDIO_PORT_2CH;
        audPortParm.nBlock      = NUM_AUD_BLOCKS;
        audPortParm.attr        = CELL_AUDIO_PORTATTR_OUT_SECONDARY;// | CELL_AUDIO_PORTATTR_INITLEVEL;
        audPortParm.level       = 0;//0.8f; // 0.0 = -0dB  0.5 = -6dB
        err = cellAudioPortOpen(&audPortParm, &m_AudPortNum);
        rcheck(CELL_OK == err,
			catchall,
			vcError("NativeInit :: cellAudioPortOpen Failed: 0x%08x", err));

        ++m_State;

        m_AudBlockAddr = NULL;
        m_AudDeviceBuffer = NULL;

        vcDebug("NativeInit :: Initializing Speex...");

        //Initialize speex codec.
        speex_bits_init(m_SpeexBitsEnc);
        m_SpeexEncState = speex_encoder_init(&speex_nb_mode);

        int samplesPerFrame, sampleRate, bitRate, quality, vad;

        quality = AVC_SPEEX_QUALITY;
        vad = AVC_SPEEX_USE_VAD;

        speex_encoder_ctl(m_SpeexEncState, SPEEX_SET_QUALITY, &quality);
        speex_encoder_ctl(m_SpeexEncState, SPEEX_GET_SAMPLING_RATE, &sampleRate);
        speex_encoder_ctl(m_SpeexEncState, SPEEX_GET_FRAME_SIZE, &samplesPerFrame);
        speex_encoder_ctl(m_SpeexEncState, SPEEX_GET_BITRATE, &bitRate);
        speex_encoder_ctl(m_SpeexEncState, SPEEX_SET_VAD, &vad);

        vcAssert(AVC_SPEEX_SAMPLE_RATE == sampleRate);
        vcAssert(AVC_SPEEX_SAMPLES_PER_FRAME == samplesPerFrame);

        m_EncBytesPerFrame = AVC_SPEEX_SAMPLE_DURATION * ((bitRate+7) >> 3) / 1000;
        m_FrameDurationMs = AVC_SPEEX_SAMPLE_DURATION;

        //Speex adds an extra byte;
        m_EncBytesPerFrame += 1;

        m_NumSamples = 0;
        m_VoiceActivityDuringCurrentFrame = false;

        vcDebug("NativeInit :: Creating audio submission thread...");

        //Create the thread that writes to the audio device buffer.
        m_WaitSema = sysIpcCreateSema(0);
        m_StopWorker = false;

        rverify(m_WaitSema,
                catchall,
                vcError("NativeInit :: Error creating semaphores"));

        m_WorkerThreadId = sysIpcCreateThread(&VoiceChat::Worker,
                                                this,
                                                sysIpcMinThreadStackSize,
                                                PRIO_NORMAL,
                                                "[RAGE] VoiceChat Worker",
                                                cpuAffinity);

        rverify(sysIpcThreadIdInvalid != m_WorkerThreadId,
                catchall,
                vcError("NativeInit :: Error creating worker thread"));

        //Wait for the thread to start.
        sysIpcWaitSema(m_WaitSema);

        m_Vpd = 0;
        m_VpdCountdown = 0;

#elif USE_LIBVOICE

#if RSG_ORBIS
		rverify(CELL_OK == sceSysmoduleLoadModule(CELL_SYSMODULE_VOICE),catchall,vcError("CELL_SYSMODULE_VOICE wasn't loaded."));
#endif

        rverify(CELL_OK == cellSysmoduleIsLoaded(CELL_SYSMODULE_VOICE),catchall,vcError("CELL_SYSMODULE_VOICE wasn't loaded."));

        CellVoiceInitParam Params;

		// TODO: NS - using initial user here - if we support multiple local users, we need to create ports for each user involved in voice chat
		int playerId = g_initialUserId;

		Params.appType = SCE_VOICE_APPTYPE_GAME;
		Params.onEvent = NULL;
		int ret = cellVoiceInit(&Params, SCE_VOICE_VERSION_100);

        rverify(ret == CELL_OK,catchall,vcError("cellVoiceInit returned:0x%08x",ret));
        m_State = STATE_CELLVOICE_INIT;

        ret = CreatePort_Device(m_MicPort,CELLVOICE_PORTTYPE_IN_MIC, playerId);
        rverify(ret == CELL_OK,catchall,vcError("CreatePort(IN_MIC) returned:0x%08x",ret));

        ret = CreatePort_Voice(m_VoiceOutPort,CELLVOICE_PORTTYPE_OUT_VOICE);
        rverify(ret == CELL_OK,catchall,vcError("CreatePort(OUT_VOICE) returned:0x%08x",ret));

        ret = cellVoiceConnectIPortToOPort(m_MicPort, m_VoiceOutPort);
        rverify(ret == CELL_OK,catchall,vcError("Connect(Mic->VoiceOut) returned:0x%08x",ret));

        ret = CreatePort_Device(m_HeadsetPort,CELLVOICE_PORTTYPE_OUT_SECONDARY, playerId);
        rverify(ret == CELL_OK,catchall,vcError("CreatePort(OUT_SECONDARY) returned:0x%08x",ret));

		// Create a PCM output port to give us access to the local mic data
		ret = CreatePort_PCM(m_MicPcmPort, CELLVOICE_PORTTYPE_OUT_PCMAUDIO);
		rverify(ret == CELL_OK,catchall,vcError("CreatePort(OUT_PCMAUDIO) returned:0x%08x",ret));
		// Route the local mic port into this PCM port
		ret = cellVoiceConnectIPortToOPort(m_MicPort, m_MicPcmPort);
		rverify(ret == CELL_OK,catchall,vcError("Connect(Mic->PCM) returned:0x%08x",ret));

#if RSG_ORBIS
		PortManagerSingleton::GetInstance().Init(m_HeadsetPort);
#endif // RSG_ORBIS

		// there is no way to set the threshold on PS4. libVoice handles it internally.

		const unsigned VOICE_MEMORY_CONTAINER_SIZE = SCE_VOICE_MEMORY_CONTAINER_SIZE;
		SceVoiceStartParam arg;
		memset(&arg, 0, sizeof(arg));
		arg.memSize = VOICE_MEMORY_CONTAINER_SIZE;
		arg.container = rage_new u8[VOICE_MEMORY_CONTAINER_SIZE]; // TODO: NS - from which pool?
		rverify(arg.container != NULL,catchall,vcError("Error allocating voice chat memory"));

		/*
			As mentioned on the Sony forums, sceVoiceStart() creates threads using the same affinity as the calling thread,
			unlike other Sony functions that don't set a specific CPU. Disable this 'feature' by changing the affinity of
			the calling thread to all CPUs, then calling sceVoiceStart() then restoring the calling thread's affinity back
			to its original value. This reduces the amount of crackling heard over voice chat.
		*/

		SceKernelCpumask oldAffinity;
		scePthreadGetaffinity((ScePthread)sysIpcGetCurrentThreadId(), &oldAffinity);
		scePthreadSetaffinity((ScePthread)sysIpcGetCurrentThreadId(), (1 << 0) + (1 << 1) + (1 << 2) + (1 << 3) + (1 << 4) + (1 << 5));
		ret = sceVoiceStart(&arg);
		scePthreadSetaffinity((ScePthread)sysIpcGetCurrentThreadId(), oldAffinity);

        rverify(ret == CELL_OK,catchall,vcError("NativeInit :: cellVoiceStart failed: 0x%08x", ret));
        m_State = STATE_CELLVOICE_START;

        // Must do this after the service is started... and wait for it 
        m_EncBytesPerFrame = GetFrameSize(m_VoiceOutPort);
        if (m_EncBytesPerFrame & 0x80000000) {
            vcWarning(("NativeInit :: GetFrameSize indicates stack isn't ready yet, will poll..."));
            do {
                sysIpcSleep(100);
                m_EncBytesPerFrame = GetFrameSize(m_VoiceOutPort);
            }	while (m_EncBytesPerFrame & 0x80000000);
        }

        uint32_t bitRate;
        ret = cellVoiceGetBitRate(m_VoiceOutPort, &bitRate);
        rverify(ret >= 0, catchall, vcError("cellVoiceGetBitRate failed: 0x%08x", ret));
        m_FrameDurationMs = (8000 * m_EncBytesPerFrame) / bitRate;

        vcDebug("NativeInit :: FrameSize(OUT_VOICE) = %u",m_EncBytesPerFrame);
        vcDebug("NativeInit :: FrameDuration(OUT_VOICE) = %u",m_FrameDurationMs);

#endif // USE_LIBVOICE

        success = true;
    }
    rcatchall
    {
        this->NativeShutdown();
    }

    return success;
}

void
VoiceChat::NativeShutdown()
{
#if USE_SPEEX
    if(sysIpcThreadIdInvalid != m_WorkerThreadId)
    {
        m_StopWorker = true;

        //Wait for the thread to end.
        sysIpcWaitSema(m_WaitSema);
        sysIpcWaitThreadExit(m_WorkerThreadId);
        m_WorkerThreadId = sysIpcThreadIdInvalid;
    }

    if(m_WaitSema)
    {
        sysIpcDeleteSema(m_WaitSema);
        m_WaitSema = 0;
    }

    if(m_State >= STATE_LOADED_MODULE)
    {
        if(m_State >= STATE_INITIALIZED_MIC)
        {
            if(m_State >= STATE_CREATED_MIC_QUEUE)
            {
                if(m_State >= STATE_REGISTERED_MIC_QUEUE)
                {
                    if(m_State >= STATE_CREATED_AUD_QUEUE)
                    {
                        if(m_State >= STATE_REGISTERED_AUD_QUEUE)
                        {
                            if(m_State >= STATE_OPENED_AUD_PORT)
                            {
                                cellAudioPortClose(m_AudPortNum);

                                m_AudBlockAddr = NULL;
                                m_AudDeviceBuffer = NULL;

                                speex_bits_destroy(m_SpeexBitsEnc);
                                speex_encoder_destroy(m_SpeexEncState);
                                m_SpeexEncState = NULL;
                            }

                            cellMicRemoveNotifyEventQueue(m_AudioQueueId);
                        }

                        sys_event_queue_destroy(m_AudioCallbackQueue, SYS_EVENT_QUEUE_DESTROY_FORCE);
                    }

                    cellMicRemoveNotifyEventQueue(m_MicQueueId);
                }

                sys_event_queue_destroy(m_MicCallbackQueue, SYS_EVENT_QUEUE_DESTROY_FORCE);
            }

            if(m_MicDevNum > -1)
            {
                cellMicClose(m_MicDevNum);

                m_MicDevNum = -1;
            }

            cellMicEnd();
        }
    }

    m_State = STATE_INVALID;

    m_AudPortNum = ~0;

    m_MicDevNum = -1;

    m_NumSamples = 0;
    m_VoiceActivityDuringCurrentFrame = false;
    m_Vpd = 0;
    m_VpdCountdown = 0;

#elif USE_LIBVOICE

    if (m_State >= STATE_CELLVOICE_START)
	{
		int ret = cellVoiceStop();
        if(ret != CELL_OK)
		{
			vcError("cellVoiceStop returned:0x%08x", ret);
		}
	}

    if (m_MicPort != LIBVOICE_PORT_INVALID && m_VoiceOutPort != LIBVOICE_PORT_INVALID) 
	{
        int ret = cellVoiceDisconnectIPortFromOPort(m_MicPort, m_VoiceOutPort);
		if(ret != CELL_OK)
		{
			vcError("cellVoiceDisconnectIPortFromOPort returned:0x%08x", ret);
		}
	}

	if(m_MicPort != LIBVOICE_PORT_INVALID && m_MicPcmPort != LIBVOICE_PORT_INVALID)
	{
		int ret = cellVoiceDisconnectIPortFromOPort(m_MicPort, m_MicPcmPort);
		if(ret != CELL_OK)
		{
			vcError("cellVoiceDisconnectIPortFromOPort returned:0x%08x", ret);
		}
	}

#if RSG_ORBIS
	PortManagerSingleton::GetInstance().Shutdown();
#endif // RSG_ORBIS

    if (m_MicPort != LIBVOICE_PORT_INVALID)
    {
		int ret = cellVoiceDeletePort(m_MicPort);
		if(ret != CELL_OK)
		{
			vcError("cellVoiceDeletePort returned:0x%08x", ret);
		}
        m_MicPort = LIBVOICE_PORT_INVALID;
    }
    if (m_VoiceOutPort != LIBVOICE_PORT_INVALID)
    {
        int ret = cellVoiceDeletePort(m_VoiceOutPort);
		if(ret != CELL_OK)
		{
			vcError("cellVoiceDeletePort returned:0x%08x", ret);
		}
		m_VoiceOutPort = LIBVOICE_PORT_INVALID;
    }
	if (m_HeadsetPort != LIBVOICE_PORT_INVALID)
	{
		int ret = cellVoiceDeletePort(m_HeadsetPort);
		if(ret != CELL_OK)
		{
			vcError("cellVoiceDeletePort returned:0x%08x", ret);
		}
		m_HeadsetPort = LIBVOICE_PORT_INVALID;
	}
	if (m_MicPcmPort != LIBVOICE_PORT_INVALID)
	{
		int ret = cellVoiceDeletePort(m_MicPcmPort);
		if(ret != CELL_OK)
		{
			vcError("cellVoiceDeletePort returned:0x%08x", ret);
		}
		m_MicPcmPort = LIBVOICE_PORT_INVALID;
	}
    if (m_State >= STATE_CELLVOICE_INIT)
	{
		int ret = cellVoiceEnd();
		if(ret != CELL_OK)
		{
			vcError("cellVoiceEnd returned:0x%08x", ret);
		}
	}

    m_State = STATE_INVALID;
#endif // USE_SPEEX

    m_FrameDurationMs = 0;
}

bool
VoiceChat::NativeAddTalker(Talker* talker)
{
    vcAssert(talker);
    vcAssert(talker->m_GamerInfo.IsValid());

    bool success = false;

#if USE_SPEEX
    talker->m_RingBuffer.Clear();
    talker->m_ProcessVoiceData = false;
    talker->m_SpeexJitterBuffer = jitter_buffer_init(AVC_SPEEX_SAMPLE_DURATION);
    talker->m_AccumulatedTimeStep = 0;

    CompileTimeAssert(sizeof(SpeexBits) == sizeof(talker->m_SpeexBitsDecMem));
    talker->m_SpeexBitsDec = (SpeexBits*) talker->m_SpeexBitsDecMem;

    //Initialize speex codec.
    speex_bits_init(talker->m_SpeexBitsDec);
    talker->m_SpeexDecState = speex_decoder_init(&speex_nb_mode);

    success = true;

#elif USE_LIBVOICE

#if RSG_ORBIS
	success = true;
#else
    OUTPUT_ONLY(int ret;)
    
    if(CELL_OK != (OUTPUT_ONLY(ret =) CreatePort_Voice(talker->m_InPort,CELLVOICE_PORTTYPE_IN_VOICE)))
    {
        vcError("CreatePort(CELLVOICE_PORTTYPE_IN_VOICE) returned:0x%08x",ret);
    }
    else if(CELL_OK !=
            (OUTPUT_ONLY(ret =) cellVoiceConnectIPortToOPort(talker->m_InPort, this->GetHeadsetPort())))
    {
        vcError("cellVoiceConnectIPortToOPort returned:0x%08x",ret);
    }
	if(CELL_OK != (OUTPUT_ONLY(ret =) CreatePort_PCM(talker->m_PcmPort, CELLVOICE_PORTTYPE_OUT_PCMAUDIO)))
	{
		vcError("CreatePort(CELLVOICE_PORTTYPE_OUT_PCM) returned:0x%08x",ret);
	}
	else if(CELL_OK !=
		(OUTPUT_ONLY(ret =) cellVoiceConnectIPortToOPort(talker->m_InPort, talker->m_PcmPort)))
	{
		vcError("cellVoiceConnectIPortToOPort (PCM) returned:0x%08x",ret);
	}
    else
    {
        success = true;
    }
#endif // RSG_ORBIS
#else
    success = true;
#endif // USE_SPEEX

    return success;
}

void
VoiceChat::NativeRemoveTalker(Talker* talker)
{
    vcAssert(talker);
    vcAssert(talker->m_GamerInfo.IsValid());

#if USE_SPEEX
    jitter_buffer_destroy(talker->m_SpeexJitterBuffer);
    talker->m_SpeexJitterBuffer = 0;
    talker->m_ProcessVoiceData = false;
    talker->m_AccumulatedTimeStep = 0;
    speex_bits_destroy(talker->m_SpeexBitsDec);
    talker->m_SpeexBitsDec = NULL;
    speex_decoder_destroy(talker->m_SpeexDecState);
    talker->m_SpeexDecState = NULL;
#elif USE_LIBVOICE
#if RSG_ORBIS
	if(talker->m_InPort != LIBVOICE_PORT_INVALID)
	{
		vcDebug("NativeRemoveTalker :: Releasing Port %u for %s", talker->m_InPort, talker->m_GamerInfo.GetName());
		PortManagerSingleton::GetInstance().ReleaseVoiceInputPortId(talker->m_InPort);
	}

	talker->m_InPort = LIBVOICE_PORT_INVALID;
	talker->m_PcmPort = LIBVOICE_PORT_INVALID;
#else
    if (talker->m_InPort != LIBVOICE_PORT_INVALID && this->GetHeadsetPort() != LIBVOICE_PORT_INVALID)
        cellVoiceDisconnectIPortFromOPort(talker->m_InPort, this->GetHeadsetPort());

	if (talker->m_InPort != LIBVOICE_PORT_INVALID && talker->m_PcmPort != LIBVOICE_PORT_INVALID)
		cellVoiceDisconnectIPortFromOPort(talker->m_InPort, talker->m_PcmPort);
    	
	if (talker->m_InPort != LIBVOICE_PORT_INVALID)
    {
        cellVoiceDeletePort(talker->m_InPort);
        talker->m_InPort = LIBVOICE_PORT_INVALID;
    }

	if (talker->m_PcmPort != LIBVOICE_PORT_INVALID)
	{
		cellVoiceDeletePort(talker->m_PcmPort);
		talker->m_PcmPort = LIBVOICE_PORT_INVALID;
	}
#endif // RSG_ORBIS
#endif // USE_SPEEX
}

bool
VoiceChat::NativeIsTalking(const Talker* talker) const
{
    vcAssert(talker);
    vcAssert(talker->m_GamerInfo.IsValid());

    bool isTalking = false;

//#if USE_SPEEX
    isTalking = (talker->m_IsTalkingCountdown > 0);

    //The following is unreliable for remote talkers.
    //Sometimes the port state for remote talkers gets stuck in
    //PORTSTATE_RUNNING.  Should be investigated.

/*#elif USE_LIBVOICE

    CellVoiceBasePortInfo portInfo;
    memset(&portInfo, 0, sizeof(portInfo));

    int err;

    if(talker->m_GamerInfo.IsLocal())
    {
        err = cellVoiceGetPortInfo(m_MicPort, &portInfo);
    }
    else
    {
        err = cellVoiceGetPortInfo(talker->m_InPort, &portInfo);
    }

    if(err < 0)
    {
        vcError("cellVoiceGetPortInfo returned:0x%08x for %s talker:\"%s\"",
                    err,
                    talker->m_GamerInfo.IsLocal() ? "local" : "remote",
                    talker->m_GamerInfo.GetName());
    }
    else
    {
        isTalking = (CELLVOICE_PORTSTATE_RUNNING == portInfo.state);
    }
#endif*/

    return isTalking;
}

void
VoiceChat::NativeUpdate(const unsigned timeStep)
{
#if USE_SPEEX
    sys_event_t callbackEvt;
    int numEvents = 0;

    int err = sys_event_queue_tryreceive(m_MicCallbackQueue,
                                        &callbackEvt,
                                        1,
                                        &numEvents);

    vcAssert(0 == numEvents || 1 == numEvents);

    if(CELL_OK == err && numEvents)
    {
        const int msg = (int) callbackEvt.data1;
        const int devNum = (int) callbackEvt.data2;

        switch (msg)
        {
        case CELLMIC_ATTACH:
            vcDebug("NativeUpdate :: CELLMIC_ATTACH");

            if(-1 == m_MicDevNum || devNum == m_MicDevNum)
            {
                int state = 0;

                int err;
                if(CELL_OK == (err = cellMicOpen(devNum, MIC_SAMPLE_RATE)))
                {
                    ++state;    //1

                    if(CELL_OK == (err = cellMicStart(devNum)))
                    {
                        ++state;    //2

                        if(CELL_OK == (err = cellAudioPortStart(m_AudPortNum)))
                        {
                            ++state;    //3

                            CellAudioPortConfig audPortCfg;
                            if(CELL_OK == (err = cellAudioGetPortConfig(m_AudPortNum, &audPortCfg)))
                            {
                                SYS_CS_SYNC(m_CsChatDevice);

                                m_MicDevNum = devNum;

                                //Pointer to index of block that the audio system is
                                //currently reading.
                                m_AudBlockAddr = (uint64_t*) audPortCfg.readIndexAddr;
                                m_AudDeviceBuffer = (float*) audPortCfg.portAddr;
                            }
                            else
                            {
                                vcError("NativeUpdate :: Error:0x%08x calling cellAudioGetPortConfig()", err);
                            }
                        }
                        else
                        {
                            vcError("NativeUpdate :: Error:0x%08x calling cellAudioPortStart()", err);
                        }
                    }
                    else
                    {
                        vcError("NativeUpdate :: Error:0x%08x calling cellMicStart()", err);
                    }
                }
                else
                {
                    vcError("NativeUpdate :: Error:0x%08x calling cellMicOpen()", err);
                }

                vcAssert(state <= 3);

                if(CELL_OK != err)
                {
                    if(state > 0)
                    {
                        if(state > 1)
                        {
                            if(state > 2)
                            {
                                cellAudioPortStop(m_AudPortNum);
                            }

                            cellMicStop(devNum);
                        }

                        cellMicClose(devNum);
                    }

                    m_MicDevNum = -1;
                    m_AudBlockAddr = NULL;
                    m_AudDeviceBuffer = NULL;
                }
            }
            break;
        case CELLMIC_DETACH:
            vcDebug("NativeUpdate :: CELLMIC_DETACH");

            if(devNum == m_MicDevNum)
            {
                this->DisposeChatDevice();
            }
            break;
        /*case CELLMIC_DATA:
            if(devNum == m_MicDevNum)
            {
                float samples[256] __attribute__ ((aligned(128)));
                for(int len = cellMicRead(m_MicDevNum, &samples, (int) sizeof(samples));
                    len > 0;
                    len = cellMicRead(m_MicDevNum, &samples, (int) sizeof(samples)))
                {
                    this->NativeSubmitChat(rlGamerInfo(), samples, len);
                }
            }
            break;*/
        default:
            break;
        }
    }

    m_VpdCountdown -= (int) timeStep;

    if(m_VpdCountdown < 0)
    {
        if(m_MicDevNum >= 0)
        {
            //Voice proximity detection.  Value is 0 to 10.
            int32_t vpd;
            const int err =
                cellMicGetSignalState(m_MicDevNum, CELLMIC_SIGSTATE_LOCTALK, &vpd);

            if(CELL_OK == err)
            {
                m_Vpd = vpd;
            }
            else if(CELL_MICIN_ERROR_DEVICE_NOT_FOUND == err)
            {
                this->DisposeChatDevice();
            }
            else
            {
                m_Vpd = 0;
                vcError("NativeUpdate :: Error:0x%08x calling cellMicGetSignalState()", err);
            }
        }

        m_VpdCountdown = AVC_CELL_VPD_CHECK_INTERVAL;
    }

    //Update the talkers
    for(int i = 0; i < (int) m_NumTalkers; ++i)
    {
        Talker* talker = m_Talkers[i];
        if(!talker)
        {
            continue;
        }

        //Update the jitter buffers for each remote talker
        if(talker->m_GamerInfo.IsRemote() && talker->m_ProcessVoiceData)
        {
            talker->m_AccumulatedTimeStep += timeStep;

            while(talker->m_AccumulatedTimeStep >= AVC_SPEEX_SAMPLE_DURATION)
            {
                JitterBufferPacket packet;
                char data[2048];
                packet.data = data;
                int error = jitter_buffer_get(talker->m_SpeexJitterBuffer, &packet, NULL);

                if(error == JITTER_BUFFER_OK)
                {
                    talker->m_RingBuffer.Write(packet.data, packet.len);
                    talker->m_IsTalkingCountdown += AVC_SPEEX_SAMPLE_DURATION;
                }

                jitter_buffer_tick(talker->m_SpeexJitterBuffer);

                talker->m_AccumulatedTimeStep -= AVC_SPEEX_SAMPLE_DURATION;
            }
        }
    }

#endif

	s16 sampleBuffer[1024];

	const bool sampleAudio = (VoiceChat::GetAudioProvider() != NULL);

    //Update the is talking countdown
    for(int i = 0; i < (int) m_NumTalkers; ++i)
    {
        Talker* talker = m_Talkers[i];
        if(talker && talker->m_IsTalkingCountdown > 0)
        {
            talker->m_IsTalkingCountdown -= timeStep;
            if(talker->m_IsTalkingCountdown < 0)
            {
                talker->m_IsTalkingCountdown = 0;
            }
        }

		if(sampleAudio && talker->m_PcmPort != LIBVOICE_PORT_INVALID)
		{
			u32 size = static_cast<u32>(sizeof(sampleBuffer));
			cellVoiceReadFromOPort(talker->m_PcmPort, &sampleBuffer, &size);
			VoiceChat::GetAudioProvider()->RemotePlayerAudioReceived(sampleBuffer, size>>1, talker->m_GamerInfo.GetGamerId());
		}
		   
#if RSG_ORBIS
		if(talker->m_InPort != LIBVOICE_PORT_INVALID && (talker->m_IsTalkingCountdown <= 0))
		{
			// unassign the port when he's done talking, freeing up the port for someone else
			vcDebug("NativeUpdate :: Voice Chat Releasing Port %u for %s", talker->m_InPort, talker->m_GamerInfo.GetName());
			PortManagerSingleton::GetInstance().ReleaseVoiceInputPortId(talker->m_InPort);
			talker->m_InPort = LIBVOICE_PORT_INVALID;
			talker->m_PcmPort = LIBVOICE_PORT_INVALID;
		}
#endif // RSG_ORBIS
    }

	//Check if the local mic port is ready
	CellVoiceBasePortInfo portInfo;
	u32 size = static_cast<u32>(sizeof(sampleBuffer));
	const bool portValid = (cellVoiceGetPortInfo(m_MicPort, &portInfo) == CELL_OK);

	if(portValid)
	{
		cellVoiceReadFromOPort(m_MicPcmPort, &sampleBuffer, &size);
#if PROCESS_RAW_DATA
		CalculateLoudness(0, sampleBuffer, size);
#endif // PROCESS_RAW_DATA
	}

	if(sampleAudio)
	{
		VoiceChat::GetAudioProvider()->LocalPlayerAudioReceived(sampleBuffer, size>>1);	
	}
}

void VoiceChat::NativeSetAudioParam(Talker *UNUSED_PARAM(talker), const u32 UNUSED_PARAM(paramNameHash), const float UNUSED_PARAM(paramValue))
{

}

bool
VoiceChat::NextFrame(void* frame, const unsigned USE_SPEEX_ONLY(maxSize))
{
    bool success = false;

#if USE_SPEEX
    if(m_MicDevNum >= 0 && maxSize >= m_EncBytesPerFrame)
    {
        vcAssert(m_NumSamples < AVC_SPEEX_SAMPLES_PER_FRAME);

        int len = AVC_SPEEX_SAMPLES_PER_FRAME - m_NumSamples;
        len = cellMicRead(m_MicDevNum,
                            &m_RawFrame[m_NumSamples],
                            int(len * sizeof(float)));

        if(len > 0)
        {
            //Len must be a multiple of 4 because we're reading floats.
            vcAssert(!(len & 0x03));
            CompileTimeAssert(4 == sizeof(float));

            m_NumSamples += len >> 2;

            vcAssert(m_NumSamples <= AVC_SPEEX_SAMPLES_PER_FRAME);

            if(AVC_SPEEX_SAMPLES_PER_FRAME == m_NumSamples)
            {
                m_NumSamples = 0;
                m_VoiceStreamTimestamp += AVC_SPEEX_SAMPLE_DURATION;

                //We only return the frame data for packing and sending if there was any
                //voice activity above the specified threshold while gathering the frame data
                if(m_VoiceActivityDuringCurrentFrame == false)
                {
                    float tmp[(MIC_SAMPLE_RATE+AVC_SPEEX_SAMPLE_DURATION-1)/AVC_SPEEX_SAMPLE_DURATION];

                    //No voice activity detected - empty the input buffer.
                    for(unsigned i = 0;
                        i < 1000 / AVC_SPEEX_SAMPLE_DURATION;
                        i += AVC_SPEEX_SAMPLE_DURATION)
                    {
                        len = cellMicRead(m_MicDevNum, tmp, sizeof(tmp));
                        if(0 >= len)
                        {
                            break;
                        }
                        else
                        {
                            unsigned numSamples = len >> 2;

                            float sampleTime = (((float)numSamples / AVC_SPEEX_SAMPLES_PER_FRAME) * AVC_SPEEX_SAMPLE_DURATION);
                            m_VoiceStreamTimestamp += static_cast<unsigned>(sampleTime);
                        }
                    }
                }
                else
                {
                    for(unsigned i = 0; i < AVC_SPEEX_SAMPLES_PER_FRAME; ++i)
                    {
                        m_RawFrame[i] *= AVC_SPEEX_UP_SCALE;
                    }

                    speex_bits_reset(m_SpeexBitsEnc);

                    if(speex_encode(m_SpeexEncState, m_RawFrame, m_SpeexBitsEnc))
                    {
                        const unsigned encLen =
                            speex_bits_write(m_SpeexBitsEnc, (char*)frame, maxSize);

                        //6 bytes for Comfort Noise Generation (CNG) when
                        //VAD (Voice Activity Detection) is enabled.
                        vcAssert(encLen == m_EncBytesPerFrame || 6 == encLen);

                        success = (encLen == m_EncBytesPerFrame);
                    }
                }
            }
        }
    }
#elif USE_LIBVOICE
    CellVoiceBasePortInfo portInfo;
    int err = cellVoiceGetPortInfo(m_MicPort, &portInfo);
    if(err < 0)
    {
        vcError("cellVoiceGetPortInfo returned:0x%08x", err);
    }
    else 
    {
		//Set to requested frame size
		uint32_t amtRead = m_EncBytesPerFrame;

		if(CELLVOICE_PORTSTATE_RUNNING == portInfo.state)
		{
			err = cellVoiceReadFromOPort(m_VoiceOutPort, frame, &amtRead);
			if (err < 0)
			{
				vcError("VoiceReadFromOPort returned:0x%08x", err);
			}
			else
			{
				success = amtRead == m_EncBytesPerFrame;
			}
		}
    }
#endif

    return success;
}

void
VoiceChat::DisposeChatDevice()
{
    vcDebug("DisposeChatDevice");

#if USE_SPEEX
    if(m_MicDevNum >= 0)
    {
        vcAssert(m_AudBlockAddr);
        vcAssert(m_AudDeviceBuffer);

        SYS_CS_SYNC(m_CsChatDevice);

        cellAudioPortStop(m_AudPortNum);
        cellMicStop(m_MicDevNum);
        cellMicClose(m_MicDevNum);
        m_MicDevNum = -1;
        m_AudBlockAddr = NULL;
        m_AudDeviceBuffer = NULL;
        m_Vpd = 0;
    }
#endif
}

bool
VoiceChat::NativePackChat(const int localTalkerIndex,
                            void* dst,
                            unsigned* numBytes)
{
    vcAssert(*numBytes >= m_EncBytesPerFrame);

    bool success = false;

    if(vcVerify(localTalkerIndex >= 0 && localTalkerIndex < RL_MAX_LOCAL_GAMERS))
    {
        Talker* localTalker = m_LocalTalkers[localTalkerIndex];

        const unsigned sizeofFrame = m_EncBytesPerFrame;
        const unsigned maxBytes = *numBytes;
        const unsigned maxFrames = maxBytes / sizeofFrame;
        int duration = 0;

        *numBytes = 0;

#if USE_SPEEX
        if(m_MicDevNum >= 0 && maxFrames > 0)
#elif USE_LIBVOICE
        if(maxFrames > 0)
#endif
        {
            u8* p = (u8*) dst;

            for(unsigned fc = 0;
                fc < maxFrames && this->NextFrame(p, maxBytes - *numBytes);
                ++fc)
            {
                p += sizeofFrame;
                *numBytes += sizeofFrame;

                duration += m_FrameDurationMs;
            }

            success = true;
        }

        if(localTalker)
        {
            localTalker->m_IsTalkingCountdown += duration;
        }
    }

    return success;
}

void
VoiceChat::NativeSubmitChat(Talker* talker,
                            const void* data,
                            const unsigned sizeofData,
                            const u32 USE_SPEEX_ONLY(timestamp))
{
    vcAssert(sizeofData >= m_EncBytesPerFrame);
    vcAssert(talker);

#if USE_SPEEX

    const unsigned sizeofFrame = m_EncBytesPerFrame;
    const unsigned numFrames = sizeofData / sizeofFrame;
    vcAssert((numFrames * sizeofFrame) == sizeofData);
    const char* p = (const char*) data;

    u32 frameTimestamp = timestamp;
    for(unsigned fc = 0; fc < numFrames; ++fc, p += sizeofFrame)
    {
        talker->m_ProcessVoiceData = true;

        // store decoded packets in the jitter buffer
        float decFrame[AVC_SPEEX_SAMPLES_PER_FRAME];
        speex_bits_read_from(talker->m_SpeexBitsDec, (char*) p, sizeofFrame);

        int err = speex_decode(talker->m_SpeexDecState, talker->m_SpeexBitsDec, decFrame);

        if(!vcVerify(0 == err))
        {
            vcError("NativeSubmitChat :: Error calling speex_decode:0x%08x", err);
        }
        else
        {
            JitterBufferPacket packet;
            packet.data = (char *)decFrame;
            packet.len = sizeof(decFrame);
            packet.span = AVC_SPEEX_SAMPLE_DURATION;
            packet.timestamp = frameTimestamp;
            jitter_buffer_put(talker->m_SpeexJitterBuffer, &packet);
        }

        frameTimestamp += AVC_SPEEX_SAMPLE_DURATION;
    }
#elif USE_LIBVOICE
    uint32_t amtWritten = sizeofData;

#if RSG_ORBIS
	if (talker->m_InPort == LIBVOICE_PORT_INVALID)
	{
		vcDebug("NativeSubmitChat :: Voice Chat Acquiring Input Port for %s", talker->m_GamerInfo.GetName());
		if (PortManagerSingleton::GetInstance().GetAvailableVoiceInputPortId(talker->m_InPort))
		{
			vcDebug("NativeSubmitChat :: Voice Chat Acquiring PCM Port for %s, InputPort: %u", talker->m_GamerInfo.GetName(), talker->m_InPort);
			PortManagerSingleton::GetInstance().GetConnectedPcmOutputPortId(talker->m_InPort, talker->m_PcmPort);
		}
	}

	if (talker->m_InPort != LIBVOICE_PORT_INVALID && talker->m_PcmPort != LIBVOICE_PORT_INVALID)
	{
		// TODO: NS - frameGaps?
		int16_t frameGaps = 0;
		int err = cellVoiceWriteToIPort(talker->m_InPort,data,&amtWritten,frameGaps);
		if (err < 0)
		{
			vcError("NativeSubmitChat :: ellVoiceWriteToIPort(%d) returned: 0x%08x for talker: %s",sizeofData,err,talker->m_GamerInfo.GetName());
		}
		else
		{
			talker->m_IsTalkingCountdown += (amtWritten / m_EncBytesPerFrame) * m_FrameDurationMs;
		}
	}
	else
	{
#if !__NO_OUTPUT
		if (talker->m_InPort == LIBVOICE_PORT_INVALID)
		{
			vcWarning("NativeSubmitChat :: No Input Ports available for talker: %s", talker->m_GamerInfo.GetName());
		}

		if (talker->m_PcmPort == LIBVOICE_PORT_INVALID)
		{
			vcWarning("NativeSubmitChat :: No PCM Output Ports available for talker: %s", talker->m_GamerInfo.GetName());
		}
#endif // !__NO_OUTPUT
	}
#else
	int err = cellVoiceWriteToIPort(talker->m_InPort,data,&amtWritten);
	if (err < 0)
	{
		vcError("NativeSubmitChat :: cellVoiceWriteToIPort(%d) returned: 0x%08x for talker: %s",sizeofData,err,talker->m_GamerInfo.GetName());
	}
	else
	{
		talker->m_IsTalkingCountdown += (amtWritten / m_EncBytesPerFrame) * m_FrameDurationMs;
	}
#endif // RSG_ORBIS
#endif // USE_SPEEX
}

#if USE_SPEEX
void
VoiceChat::Worker(void* arg)
{
    VoiceChat* vc = (VoiceChat*) arg;

    sysIpcSignalSema(vc->m_WaitSema);

    CompileTimeAssert(NUM_AUD_BLOCKS == 8
                        || NUM_AUD_BLOCKS == 16
                        || NUM_AUD_BLOCKS == 32);

    int readBlock = -1;
    int writeBlock = -1;
    float* dst = NULL;
    int audOffset = -1;

    while(!vc->m_StopWorker)
    {
        sys_event_t callbackEvt;

        static const unsigned AQ_TIMEOUT = 20; //milliseconds

        int err = sys_event_queue_receive(vc->m_AudioCallbackQueue,
                                            &callbackEvt,
                                            AQ_TIMEOUT * 1000);

        if(vc->m_StopWorker)
        {
            continue;
        }

        {
            SYS_CS_SYNC(vc->m_CsChatDevice);

            if(vc->m_MicDevNum < 0)
            {
                readBlock = writeBlock = audOffset = -1;
                dst = NULL;
                continue;
            }
            else if(-1 == readBlock)
            {
                vcAssert(vc->m_AudBlockAddr);
                vcAssert(vc->m_AudDeviceBuffer);

                readBlock = writeBlock = vc->m_AudBlockAddr ? *vc->m_AudBlockAddr : 0;
                dst = vc->m_AudDeviceBuffer;
                audOffset = writeBlock * SIZEOF_AUD_BLOCK;

                //Back up by one block because a few lines down we advance
                //by one block.
                readBlock = (readBlock + NUM_AUD_BLOCKS + 1) & (NUM_AUD_BLOCKS - 1);
            }
        }

        if(err < 0)
        {
            vcError("Error:0x%08x calling sys_event_queue_receive()", err);
            continue;
        }

        readBlock = (readBlock + 1) & (NUM_AUD_BLOCKS - 1);
        if(readBlock == writeBlock)
        {
            writeBlock = (readBlock + 1) & (NUM_AUD_BLOCKS - 1);
            audOffset = writeBlock * SIZEOF_AUD_BLOCK;
        }
        else
        {
            continue;
        }

        SYS_CS_SYNC(vc->m_Cs);

        for(unsigned tIdx = 0; tIdx < vc->m_NumTalkers; ++tIdx)
        {
            Talker* t = vc->m_Talkers[tIdx];
            vcAssert(t->m_IsActive);

            if(!t->m_RingBuffer.GetSize())
            {
                continue;
            }

            float samples[CELL_AUDIO_BLOCK_SAMPLES];

            unsigned offset = audOffset;

            for(unsigned nb = t->m_RingBuffer.Read(samples, sizeof(samples));
                nb;
                nb = t->m_RingBuffer.Read(samples, sizeof(samples)))
            {
                const unsigned numSamples = nb / sizeof(float);

                //Make sure we're reading whole floats.
                vcAssert(numSamples * sizeof(float) == nb);

                //Will the audio buffer wrap while writing these samples?
                if(offset + (numSamples * DUP_MIC_SAMPLES) >= SIZEOF_AUD_BUFFER)
                {
                    //Writing these samples will cause a wrap to the beginning
                    //of the audio buffer.

                    for(unsigned i = 0; i < numSamples; ++i)
                    {
                        float sample = samples[i] * AVC_SPEEX_DOWN_SCALE;

                        if(offset + DUP_MIC_SAMPLES >= SIZEOF_AUD_BUFFER)
                        {
                            if(offset < SIZEOF_AUD_BUFFER)
                            {
                                sample += dst[offset];
                            }
                            else
                            {
                                sample += dst[0];
                            }

                            if(sample < -1){sample = -1;}
                            else if(sample > 1){sample = 1;}

                            for(int j = 0; j < DUP_MIC_SAMPLES; ++j, ++offset)
                            {
                                if(offset >= SIZEOF_AUD_BUFFER)
                                {
                                    offset = 0;
                                }

                                dst[offset] = sample;
                            }
                        }
                        else
                        {
                            float* pf = &dst[offset];
                            sample += *pf;
                            if(sample < -1){sample = -1;}
                            else if(sample > 1){sample = 1;}

                            CompileTimeAssert(12 == DUP_MIC_SAMPLES);
                            pf[0] = sample;
                            pf[1] = sample;
                            pf[2] = sample;
                            pf[3] = sample;
                            pf[4] = sample;
                            pf[5] = sample;
                            pf[6] = sample;
                            pf[7] = sample;
                            pf[8] = sample;
                            pf[9] = sample;
                            pf[10] = sample;
                            pf[11] = sample;

                            offset += DUP_MIC_SAMPLES;
                        }
                    }
                }
                else
                {
                    //Writing these samples will not cause a wrap so we can
                    //avoid the checks for wrap.

                    for(unsigned i = 0; i < numSamples; ++i)
                    {
                        float sample = samples[i] * AVC_SPEEX_DOWN_SCALE;

                        float* pf = &dst[offset];
                        sample += *pf;
                        if(sample < -1){sample = -1;}
                        else if(sample > 1){sample = 1;}

                        CompileTimeAssert(12 == DUP_MIC_SAMPLES);
                        pf[0] = sample;
                        pf[1] = sample;
                        pf[2] = sample;
                        pf[3] = sample;
                        pf[4] = sample;
                        pf[5] = sample;
                        pf[6] = sample;
                        pf[7] = sample;
                        pf[8] = sample;
                        pf[9] = sample;
                        pf[10] = sample;
                        pf[11] = sample;

                        offset += DUP_MIC_SAMPLES;
                    }
                }

                CompileTimeAssert(NUM_AUD_BLOCKS == 8
                                    || NUM_AUD_BLOCKS == 16
                                    || NUM_AUD_BLOCKS == 32);

                writeBlock = (offset / SIZEOF_AUD_BLOCK) & (NUM_AUD_BLOCKS - 1);
            }
        }
    }

    sysIpcSignalSema(vc->m_WaitSema);
}
#endif	// USE_SPEEX

#elif RSG_PC

// we don't support multiple local gamers on PC
CompileTimeAssert(RL_MAX_LOCAL_GAMERS == 1);

bool
VoiceChat::InitLocalOnly(const unsigned maxLocalTalkers,
                         const int cpuAffinity)
{
    SYS_CS_SYNC(m_Cs);

    m_LocalOnly = true;
    bool success = Init(maxLocalTalkers, 0, NULL, cpuAffinity);
	if(success)
	{
		success = InitNetwork(NULL, 0);
	}
	SetLocalEcho(true);
    return success;
}

bool
VoiceChat::SetLocalOnly(bool localOnly)
{
    SYS_CS_SYNC(m_Cs);

    vcAssert(sm_Initialized);

    m_LocalOnly = localOnly;
    SetLocalEcho(localOnly);

    return true;
}

VoiceChat::Device::Device(GUID id, const wchar_t *name, bool capture, bool playback, bool isDefaultCaptureDevice, bool isDefaultPlaybackDevice)
{
    memset(&m_Id, 0, sizeof(m_Id));
    wcsncpy(m_Name, name, RV_DEVICE_NAME_LEN);
    m_Id = id;
    m_Id32 = atDataHash((unsigned int*)&m_Id, sizeof(m_Id));
    m_Capture = capture;
    m_Playback = playback;
    m_IsDefaultCaptureDevice = isDefaultCaptureDevice;
    m_IsDefaultPlabackDevice = isDefaultPlaybackDevice;
}

void 
VoiceChat::SetLocalEcho(bool enabled)
{
    SYS_CS_SYNC(m_Cs);

    m_LocalEchoEnabled = enabled;
}

void
VoiceChat::SetMuteChatBecauseFocusLost(bool shouldMute)
{
	if (m_MuteChatBecauseFocusLost != shouldMute)
	{
		vcDebug("SetMuteChatBecauseFocusLost :: %s -> %s", m_MuteChatBecauseFocusLost ? "ON" : "OFF", shouldMute ? "ON" : "OFF");
		m_MuteChatBecauseFocusLost = shouldMute;
	}
}

void 
VoiceChat::EnumerateDevices(DeviceList &list)
{
    list.Clear();

    vcDebug("EnumerateDevices :: Checking for devices...");
    RVDeviceInfo devices[MAX_DEVICES];
    int num = RVoice::ListDevices(devices, MAX_DEVICES, RV_CAPTURE_AND_PLAYBACK);
    vcDebug("EnumerateDevices :: Found %d voice playback/capture devices", num);

    for(int i = 0; i < num; i++)
    {
        RVDeviceInfo* info = &devices[i];
        bool pd = (info->m_deviceType & RV_PLAYBACK) != 0;
        bool cd = (info->m_deviceType & RV_CAPTURE) != 0;
        bool dpd = (info->m_defaultDevice & RV_PLAYBACK) != 0;
        bool dcd = (info->m_defaultDevice & RV_CAPTURE) != 0;

        VoiceChat::Device device((GUID)(info->m_id), info->m_name, cd, pd, dcd, dpd);

        list.AddDevice(device);
    }
}

bool 
VoiceChat::SetCaptureDevice(const Device &device)
{
    SYS_CS_SYNC(m_Cs);

    bool success = false;

    vcDebug("SetCaptureDevice :: Checking for devices...");
    RVDeviceInfo devices[MAX_DEVICES];
    int num = RVoice::ListDevices(devices, MAX_DEVICES, RV_CAPTURE);
    vcDebug("SetCaptureDevice :: Found %d voice capture devices", num);

    for(int i = 0; i < num; i++)
    {
        RVDeviceInfo* info = &devices[i];
        bool cd = (info->m_deviceType & RV_CAPTURE) != 0;

        if(cd && (info->m_id == device.GetId()) && (_wcsicmp(info->m_name, device.GetName()) == 0))
        {
            success = SetupDevice(info, RV_CAPTURE);
            Talker* talker = m_LocalTalkers[0];
            if(talker && success && m_CaptureDevice)
            {
                if(RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == false)
                {
                    if(RVoice::StartDevice(m_CaptureDevice, RV_CAPTURE))
                    {
                        vcDebug("SetCaptureDevice :: Started capture device successfully");
                        DrainCaptureDevice();
                    }
                    else
                    {
                        vcDebug("SetCaptureDevice :: Failed to start capture device");
                    }
                }
                break;
            }
        }
    }

    if(success)
    {
        vcDebug("SetCaptureDevice :: Setting capture device to %ls", device.GetName());
    }
    else
    {
        vcWarning("SetCaptureDevice :: Failed to set capture device to %ls", device.GetName());
    }

    return success;
}

void 
VoiceChat::SetCaptureDeviceVolume(float volume)
{
    SYS_CS_SYNC(m_Cs);

    float clampedVolume = Clamp(volume, 0.0f, 1.0f);
    vcAssert(clampedVolume == volume);
    vcDebug("SetCaptureDeviceVolume :: Setting capture device volume to %f", volume);

    m_CaptureDeviceVolume = clampedVolume;

    if(IsCaptureDeviceAvailable())
    {
        RVoice::SetDeviceVolume(m_CaptureDevice, RV_CAPTURE, clampedVolume);
    }
}

float 
VoiceChat::GetCaptureDeviceVolume()
{
    return m_CaptureDeviceVolume;
}

bool 
VoiceChat::SetPlaybackDevice(const Device &device)
{
    SYS_CS_SYNC(m_Cs);

    bool success = false;

    vcDebug("SetPlaybackDevice :: Checking for devices...");
    RVDeviceInfo devices[MAX_DEVICES];
    int num = RVoice::ListDevices(devices, MAX_DEVICES, RV_PLAYBACK);
    vcDebug("SetPlaybackDevice :: Found %d voice playback devices", num);

    for(int i = 0; i < num; i++)
    {
        RVDeviceInfo* info = &devices[i];
        bool cd = (info->m_deviceType & RV_PLAYBACK) != 0;

        if(cd && (info->m_id == device.GetId()) && (_wcsicmp(info->m_name, device.GetName()) == 0))
        {
            success = SetupDevice(info, RV_PLAYBACK);
            Talker* talker = m_LocalTalkers[0];
            if(talker && success && m_PlaybackDevice)
            {
                if(RVoice::IsDeviceStarted(m_PlaybackDevice, RV_PLAYBACK) == false)
                {
                    if(RVoice::StartDevice(m_PlaybackDevice, RV_PLAYBACK))
                    {
                        vcDebug("SetPlaybackDevice :: Started playback device successfully");
                    }
                    else
                    {
                        vcDebug("SetPlaybackDevice :: Failed to start playback device");
                    }
                }
                break;
            }
        }
    }

    if(success)
    {
        vcDebug("SetPlaybackDevice :: Setting playback device to %ls", device.GetName());
    }
    else
    {
        vcWarning("SetPlaybackDevice :: Failed to set playback device to %ls", device.GetName());
    }

    return success;
}

void 
VoiceChat::SetPlaybackDeviceVolume(float volume)
{
    SYS_CS_SYNC(m_Cs);

    float clampedVolume = Clamp(volume, 0.0f, 1.0f);
    vcAssert(clampedVolume == volume);
    vcDebug("SetPlaybackDeviceVolume :: %.2f -> %.2f", m_PlaybackDeviceVolume, volume);

    m_PlaybackDeviceVolume = clampedVolume;

    if(IsPlaybackDeviceAvailable())
    {
        RVoice::SetDeviceVolume(m_PlaybackDevice, RV_PLAYBACK, clampedVolume);
    }
}

float 
VoiceChat::GetPlaybackDeviceVolume()
{
    return m_PlaybackDeviceVolume;
}

bool
VoiceChat::SetCaptureDeviceById32(u32 id32)
{
    SYS_CS_SYNC(m_Cs);

    bool success = false;
    DeviceList deviceList;
    VoiceChat::EnumerateDevices(deviceList);

    const VoiceChat::Device* captureDevice = NULL;

    for(u8 deviceIndex = 0; deviceIndex < deviceList.GetNumDevices(); deviceIndex++)
    {
        const VoiceChat::Device &device = deviceList.GetDevice(deviceIndex);

        if((id32 == DEFAULT_DEVICE_ID) && device.IsDefaultCaptureDevice())
        {
            captureDevice = &device;
            break;
        }
        else if(device.GetId32() == id32)
        {
            if(vcVerify(device.IsCaptureDevice()))
            {
                captureDevice = &device;
                break;
            }
        }
    }

    if(captureDevice)
    {
        success = SetCaptureDevice(*captureDevice);
    }
    else
    {
        FreeCaptureDevice();
    }

    return success;
}

bool
VoiceChat::SetPlaybackDeviceById32(u32 id32)
{
    SYS_CS_SYNC(m_Cs);

    bool success = false;
    DeviceList deviceList;
    VoiceChat::EnumerateDevices(deviceList);

    const VoiceChat::Device* playbackDevice = NULL;

    for(u8 deviceIndex = 0; deviceIndex < deviceList.GetNumDevices(); deviceIndex++)
    {
        const VoiceChat::Device &device = deviceList.GetDevice(deviceIndex);
    
        if((id32 == DEFAULT_DEVICE_ID) && device.IsDefaultPlaybackDevice())
        {
            playbackDevice = &device;
            break;
        }
        else if(device.GetId32() == id32)
        {
            if(vcVerify(device.IsPlaybackDevice()))
            {
                playbackDevice = &device;
                break;
            }
        }
    }

    if(playbackDevice)
    {
        success = SetPlaybackDevice(*playbackDevice);
    }
    else
    {
        FreePlaybackDevice();
    }

    return success;
}

void 
VoiceChat::SetCaptureMode(CaptureMode mode)
{
    SYS_CS_SYNC(m_Cs);

    m_CaptureMode = mode;

    Talker* talker = m_LocalTalkers[0];
    if(talker)
    {
        if(m_CaptureMode == CAPTURE_MODE_VOICE_ACTIVATED)
        {
            if(IsCaptureDeviceAvailable() && (RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == false))
            {
                if(RVoice::StartDevice(m_CaptureDevice, RV_CAPTURE))
                {
                    vcDebug("SetCaptureMode :: Started capture device successfully");
                    DrainCaptureDevice();
                }
                else
                {
                    vcDebug("SetCaptureMode :: Failed to start capture device");
                }
            }
        }
        else
        {
            // push-to-talk
            if(IsCaptureDeviceAvailable() && RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == true)
            {
                DrainCaptureDevice();
                RVoice::StopDevice(m_CaptureDevice, RV_CAPTURE);
            }
        }
    }
}

VoiceChat::CaptureMode
VoiceChat::GetCaptureMode() const
{
	return m_CaptureMode;
}

void 
VoiceChat::SetPushToTalk(bool captureEnabled)
{
    SYS_CS_SYNC(m_Cs);

    if(vcVerify(m_CaptureMode == CAPTURE_MODE_PUSH_TO_TALK))
    {
        if(captureEnabled)
        {
            if(IsCaptureDeviceAvailable() && (RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == false))
            {
                if(RVoice::StartDevice(m_CaptureDevice, RV_CAPTURE))
                {
                    vcDebug("SetPushToTalk :: Started capture device successfully");
                    DrainCaptureDevice();
                }
                else
                {
                    vcDebug("SetPushToTalk :: Failed to start capture device");
                }
            }
        }
        else
        {
            if(IsCaptureDeviceAvailable() && RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE == true))
            {
                DrainCaptureDevice();
                RVoice::StopDevice(m_CaptureDevice, RV_CAPTURE);
            }
        }
    }
}

bool 
VoiceChat::GetPushToTalk()
{
    SYS_CS_SYNC(m_Cs);

    return vcVerify(m_CaptureMode == CAPTURE_MODE_PUSH_TO_TALK) &&
           IsCaptureDeviceAvailable() &&
           (RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == true);
}

float 
VoiceChat::GetLocalTalkerSpeakingVolume() const
{
    SYS_CS_SYNC(m_Cs);

    Talker* talker = m_LocalTalkers[0];

    if(talker)
    {
        if(IsTalking(talker->m_GamerInfo.GetGamerId()))
        {
            return m_LocalTalkerSpeakingVolume;
        }
    }

    return 0.0f;
}

void
VoiceChat::CaptureDeviceUnpluggedCallback(RVHardwareDevice* /*device*/)
{
	SYS_CS_SYNC(m_Cs);

	// TODO: NS - send event up to app?
	vcDebug("CaptureDeviceUnpluggedCallback :: Voice capture device unplugged");
	m_CaptureDeviceUnpluggedPending = true;
}

void 
VoiceChat::PlaybackDeviceUnpluggedCallback(RVHardwareDevice* /*device*/)
{
	SYS_CS_SYNC(m_Cs);

	// TODO: NS - send event up to app?
	vcDebug("PlaybackDeviceUnpluggedCallback :: Voice playback device unplugged");
	m_PlaybackDeviceUnpluggedPending = true;
}

void 
VoiceChat::ListChannels(RVHardwareDevice* OUTPUT_ONLY(device), RVDeviceType /*OUTPUT_ONLY(type)*/)
{
#if !__NO_OUTPUT
    wchar_t name[RV_CHANNEL_NAME_LEN];
    int num;
    int i;

    num = RVoice::GetNumChannels(device);
    if(num < 2)
        return;

    vcDebug("ListChannels:");

    for(i = 0 ; i < num ; i++)
    {
        RVoice::GetChannelName(device, i, name);
        vcDebug(("\t\t%d: %s"), i, name);
    }
#endif
}

void
VoiceChat::DrainCaptureDevice()
{
    SYS_CS_SYNC(m_Cs);

    if(IsCaptureDeviceAvailable() && RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == true)
    {
        u8 buffer[1024];
        int sizeOfBuffer = sizeof(buffer);
        u16 frameStamp;
        double volume;

        bool capturedData;
        const int MAX_LOOP_ITERATIONS = 3;
        int loopCount = 0; // ensure finite loop
        do 
        {
            capturedData = RVoice::CapturePacket(m_CaptureDevice, buffer, &sizeOfBuffer, &frameStamp, &volume) == true;
        } while (capturedData && (++loopCount < MAX_LOOP_ITERATIONS));
    }
}

void
VoiceChat::FreeCaptureDevice()
{
    SYS_CS_SYNC(m_Cs);

    if(m_CaptureDevice != NULL)
    {
        if(IsCaptureDeviceAvailable() && RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE))
        {
            RVoice::StopDevice(m_CaptureDevice, RV_CAPTURE);
        }

        RVoice::FreeDevice(m_CaptureDevice);
    }

    m_CaptureDevice = NULL;
    sysMemSet(m_CaptureDeviceName, 0, sizeof(m_CaptureDeviceName));
    sysMemSet((void*)&m_CaptureDeviceId, 0, sizeof(m_CaptureDeviceId));
    m_CaptureDeviceUnplugged = false;
}

void
VoiceChat::FreePlaybackDevice()
{
    SYS_CS_SYNC(m_Cs);

    if(m_PlaybackDevice != NULL)
    {
        if(IsPlaybackDeviceAvailable() && RVoice::IsDeviceStarted(m_PlaybackDevice, RV_PLAYBACK))
        {
            RVoice::StopDevice(m_PlaybackDevice, RV_PLAYBACK);
        }

        RVoice::FreeDevice(m_PlaybackDevice);
    }

    m_PlaybackDevice = NULL;
    sysMemSet(m_PlaybackDeviceName, 0, sizeof(m_PlaybackDeviceName));
    sysMemSet((void*)&m_PlaybackDeviceId, 0, sizeof(m_PlaybackDeviceId));

    m_PlaybackDeviceUnplugged = false;
}

bool 
VoiceChat::SetupDevice(const void* deviceInfo, const RVDeviceType types)
{
    SYS_CS_SYNC(m_Cs);

    bool success = false;
    const RVDeviceInfo* info = (RVDeviceInfo*)deviceInfo;

    rtry
    {
        rverify((info->m_deviceType & types) == types, catchall, );

        RVHardwareDevice* captureDevice = NULL;
        RVHardwareDevice* playbackDevice = NULL;

        if(types & RV_CAPTURE)
        {
            FreeCaptureDevice();
            rverify(m_CaptureDevice == NULL, catchall, );

            captureDevice = RVoice::NewDevice(info->m_id, RV_CAPTURE);
            vcDebug("\tCreation: %s", captureDevice?"succeeded":"failed");
            rverify(captureDevice, catchall, );
        }

        if(types & RV_PLAYBACK)
        {
            FreePlaybackDevice();
            rverify(m_PlaybackDevice == NULL, catchall, );

            playbackDevice = RVoice::NewDevice(info->m_id, RV_PLAYBACK);
            vcDebug("\tCreation: %s", playbackDevice?"succeeded":"failed");
            rverify(playbackDevice, catchall, );
        }

        if(types & RV_CAPTURE)
        {
            RVoice::SetCaptureThreshold(captureDevice, m_VpdThreshold / (float)MAX_VPD_THRESHOLD);
			RVoice::SetPushToTalkCaptureThreshold(captureDevice, m_VpdThresholdPTT / (float)MAX_VPD_THRESHOLD);
			RVoice::SetCaptureHoldTimeMs(captureDevice, m_CaptureHoldTimeMs);
			RVoice::SetDeviceVolume(captureDevice, RV_CAPTURE, m_CaptureDeviceVolume);

			DeviceUnpluggedFunctor unpluggedCB;
			unpluggedCB.Bind(this, &VoiceChat::CaptureDeviceUnpluggedCallback);
			RVoice::SetUnpluggedCallback(captureDevice, unpluggedCB);

            ListChannels(captureDevice, RV_CAPTURE);

            wcsncpy(m_CaptureDeviceName, info->m_name, RV_DEVICE_NAME_LEN);
            m_CaptureDeviceId = info->m_id;
            m_CaptureDeviceUnplugged = false;
			m_CaptureDeviceUnpluggedPending = false;
            m_CaptureDevice = captureDevice;
        }

        if(types & RV_PLAYBACK)
        {
			DeviceUnpluggedFunctor unpluggedCB;
			unpluggedCB.Bind(this, &VoiceChat::PlaybackDeviceUnpluggedCallback);
			RVoice::SetUnpluggedCallback(playbackDevice, unpluggedCB);

            RVoice::SetDeviceVolume(playbackDevice, RV_PLAYBACK, m_PlaybackDeviceVolume);
            ListChannels(playbackDevice, RV_PLAYBACK);

            wcsncpy(m_PlaybackDeviceName, info->m_name, RV_DEVICE_NAME_LEN);
            m_PlaybackDeviceId = info->m_id;
            m_PlaybackDeviceUnplugged = false;
			m_PlaybackDeviceUnpluggedPending = false;
            m_PlaybackDevice = playbackDevice;
        }

        success = true;
    }
    rcatchall
    {

    }

    return success;
}

void 
VoiceChat::CheckForReconnectedCaptureDevice()
{
    SYS_CS_SYNC(m_Cs);

    vcAssert(m_CaptureDevice == NULL);
    
    vcDebug("CheckForReconnectedCaptureDevice :: Checking for devices...");
    RVDeviceInfo devices[MAX_DEVICES];
    int num = RVoice::ListDevices(devices, MAX_DEVICES, RV_CAPTURE);
    vcDebug("CheckForReconnectedCaptureDevice :: Found %d voice capture devices", num);

    for(int i = 0; i < num; i++)
    {
        RVDeviceInfo* info = &devices[i];
        bool cd = (info->m_deviceType & RV_CAPTURE) != 0;

        if(cd && (m_CaptureDeviceId == info->m_id) && (_wcsicmp(info->m_name, m_CaptureDeviceName) == 0))
        {
            SetupDevice(info, RV_CAPTURE);
        }

        if(m_CaptureDevice)
        {
            if(RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == false)
            {
                if(RVoice::StartDevice(m_CaptureDevice, RV_CAPTURE))
                {
                    vcDebug("CheckForReconnectedCaptureDevice :: Started capture device successfully");
                    DrainCaptureDevice();
                }
                else
                {
                    vcDebug("CheckForReconnectedCaptureDevice :: Failed to start capture device");
                }
            }
            break;
        }
    }
}

void 
VoiceChat::CheckForReconnectedPlaybackDevice()
{
    SYS_CS_SYNC(m_Cs);

    vcAssert(m_PlaybackDevice == NULL);

    vcDebug("CheckForReconnectedPlaybackDevice :: Checking for devices...");
    RVDeviceInfo devices[MAX_DEVICES];
    int num = RVoice::ListDevices(devices, MAX_DEVICES, RV_PLAYBACK);
    vcDebug("CheckForReconnectedPlaybackDevice :: Found %d voice playback devices", num);

    for(int i = 0; i < num; i++)
    {
        RVDeviceInfo* info = &devices[i];
        bool pd = (info->m_deviceType & RV_PLAYBACK) != 0;

        if(pd && (m_PlaybackDeviceId == info->m_id) && (_wcsicmp(info->m_name, m_PlaybackDeviceName) == 0))
        {
            SetupDevice(info, RV_PLAYBACK);
        }

        if(m_PlaybackDevice)
        {
            if(RVoice::IsDeviceStarted(m_PlaybackDevice, RV_PLAYBACK) == false)
            {
                if(RVoice::StartDevice(m_PlaybackDevice, RV_PLAYBACK))
                {
                    vcDebug("CheckForReconnectedPlaybackDevice :: Started playback device successfully");
                }
                else
                {
                    vcDebug("CheckForReconnectedPlaybackDevice :: Failed to start playback device");
                }
            }
            break;
        }
    }
}

bool 
VoiceChat::SelectDefaultDevices()
{
    SYS_CS_SYNC(m_Cs);

    vcDebug("SelectDefaultDevices :: Checking for devices...");
    RVDeviceInfo devices[MAX_DEVICES];
    int num = RVoice::ListDevices(devices, MAX_DEVICES, RV_CAPTURE_AND_PLAYBACK);
    vcDebug("SelectDefaultDevices :: Found %d voice playback/capture devices", num);

    for(int i = 0; i < num; i++)
    {
        RVDeviceInfo* info = &devices[i];
        bool dpd = (info->m_defaultDevice & RV_PLAYBACK) != 0;
        bool dcd = (info->m_defaultDevice & RV_CAPTURE) != 0;

#if !__NO_OUTPUT
        bool pd = (info->m_deviceType & RV_PLAYBACK) != 0;
        bool cd = (info->m_deviceType & RV_CAPTURE) != 0;

        const char * hardwareType;

        if(info->m_hardwareType == RVHardwareDirectSound)
            hardwareType = "DirectSound";
        else
            hardwareType = "Unknown";

        vcDebug(("%d: %s"), i, info->m_name);
        vcDebug(" (%s)\tPlayback: %s\tCapture: %s",
            hardwareType,
            pd?(dpd?"Default":"Yes"):"No",
            cd?(dcd?"Default":"Yes"):"No");
#endif

        if(dpd || dcd)
        { 
            RVDeviceType types = 0;
            if(dpd && !m_PlaybackDevice)
                types |= RV_PLAYBACK;
            if(dcd && !m_CaptureDevice)
                types |= RV_CAPTURE;

#if !__NO_OUTPUT
            const char * typesString;

            if(types == RV_CAPTURE)
                typesString = "capture";
            else if(types == RV_PLAYBACK)
                typesString = "playback";
            else
                typesString = "capture & playback";

            vcDebug("\tUsing Types: %s", typesString);
#endif

            SetupDevice(info, types);
        }

        if(m_PlaybackDevice && m_CaptureDevice)
        {
            break;
        }
    }

    return true;
}

void
VoiceChat::SetVoiceTestMode(bool bEnabled)
{
	if (bEnabled != m_IsVoiceTestMode)
	{
		vcDebug("SetVoiceTestMode :: %s -> %s", m_IsVoiceTestMode ? "Enabled" : "Disabled", bEnabled ? "Enabled" : "Disabled");
		m_IsVoiceTestMode = bEnabled;
	}
}

bool
VoiceChat::NativeInit(const int /*maxLocalTalkers*/,
                    const int /*maxRemoteTalkers*/,
                    void* /*audioDevice*/,
                    const int /*cpuAffinity*/)
{
    SYS_CS_SYNC(m_Cs);

    bool result;

    m_LocalEchoEnabled = false;
	m_MuteChatBecauseFocusLost = false;

    m_CaptureDevice = NULL;
    sysMemSet(m_CaptureDeviceName, 0, sizeof(m_CaptureDeviceName));
    sysMemSet((void*)&m_CaptureDeviceId, 0, sizeof(m_CaptureDeviceId));
    m_CaptureDeviceUnplugged = false;
    m_CaptureDeviceVolume = 1.0f;

    m_PlaybackDevice = NULL;
    sysMemSet(m_PlaybackDeviceName, 0, sizeof(m_PlaybackDeviceName));
    sysMemSet((void*)&m_PlaybackDeviceId, 0, sizeof(m_PlaybackDeviceId));
    m_PlaybackDeviceUnplugged = false;
    m_PlaybackDeviceVolume = 1.0f;

    m_CheckForReconnectsTimer = 0;

	m_IsVoiceTestMode = false;

    result = RVoice::Init(NULL) == true;

    if(!result)
    {
        vcDebug("NativeInit :: Startup failed");
        return false;
    }
    vcDebug("NativeInit :: Started");

    RVoice::SetSampleRate(RVRATE_16KHz);

    // At 16KHz sample rate, RVCodecAboveAverage has a bitrate of 16kbps
    if(!RVoice::SetCodec(RVCodecAboveAverageQuality))
    {
        vcDebug("NativeInit :: Failed to set the codec");
        return false;
    }

    RVoice::GetCodecInfo(&m_SamplesPerFrame, (int*)&m_EncBytesPerFrame, &m_BitsPerSecond);
    m_BytesPerFrame = (m_SamplesPerFrame * RV_BYTES_PER_SAMPLE);

    vcDebug("NativeInit :: SamplesPerFrame: %d, EncodedFrameSize: %d, BitsPerSecond: %d",
        m_SamplesPerFrame, m_EncBytesPerFrame, m_BitsPerSecond);

    return true;
}

void
VoiceChat::NativeShutdown()
{
    FreeCaptureDevice();
    FreePlaybackDevice();
    RVoice::Shutdown();
}

bool
VoiceChat::NativeAddTalker(Talker* talker)
{
	SYS_CS_SYNC(m_Cs);

    if(talker->m_GamerInfo.IsLocal())
    {
        if(m_CaptureMode == CAPTURE_MODE_VOICE_ACTIVATED)
        {
            if(IsCaptureDeviceAvailable() && (RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == false))
            {
                if(RVoice::StartDevice(m_CaptureDevice, RV_CAPTURE))
                {
                    vcDebug("NativeAddTalker :: Started capture device successfully");
                    DrainCaptureDevice();
                }
                else
                {
                    vcDebug("NativeAddTalker :: Failed to start capture device");
                }
            }
        }
        else
        {
            // push-to-talk
            if((m_CaptureDevice != NULL) && RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == true)
            {
                DrainCaptureDevice();
                RVoice::StopDevice(m_CaptureDevice, RV_CAPTURE);
            }
        }

        if(IsPlaybackDeviceAvailable() && (RVoice::IsDeviceStarted(m_PlaybackDevice, RV_PLAYBACK) == false))
        {
            if(m_PlaybackDevice->Start(RV_PLAYBACK))
            {
                vcDebug("NativeAddTalker :: Started playback device successfully");
            }
            else
            {
                vcDebug("NativeAddTalker :: Failed to start playback device");
            }
        }
    }

    return true;
}

void
VoiceChat::NativeRemoveTalker(Talker* talker)
{
	SYS_CS_SYNC(m_Cs);

    vcAssert(talker);
    vcAssert(talker->m_GamerInfo.IsValid());

    if(talker->m_GamerInfo.IsLocal())
    {
        if((m_CaptureDevice != NULL) && RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE))
        {
            DrainCaptureDevice();
            RVoice::StopDevice(m_CaptureDevice, RV_CAPTURE);
        }

        if(IsPlaybackDeviceAvailable() && RVoice::IsDeviceStarted(m_PlaybackDevice, RV_PLAYBACK))
        {
            RVoice::StopDevice(m_PlaybackDevice, RV_PLAYBACK);
        }
    }
}

bool
VoiceChat::NativeHasHeadset(const int /*localGamerIndex*/) const
{
	SYS_CS_SYNC(m_Cs);

    return IsCaptureDeviceAvailable() &&
           ((RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == true) ||
           m_CaptureMode == CAPTURE_MODE_PUSH_TO_TALK);
}

bool
VoiceChat::IsPlaybackDeviceStarted()
{
	SYS_CS_SYNC(m_Cs);

    return IsPlaybackDeviceAvailable() && (RVoice::IsDeviceStarted(m_PlaybackDevice, RV_PLAYBACK) == true);
}

bool
VoiceChat::NativeIsTalking(const Talker* talker) const
{
	SYS_CS_SYNC(m_Cs);

	bool isTalking = false;

    if(IsPlaybackDeviceAvailable())
    {
        if(talker->m_GamerInfo.IsLocal())
        {
            isTalking = (talker->m_IsTalkingCountdown > 0);
        }
        else
        {
            RockstarId source = talker->m_GamerInfo.GetGamerHandle().GetRockstarId();
            isTalking = RVoice::IsSourceTalking(m_PlaybackDevice, source) == true;
        }
    }

    return isTalking;
}

void
VoiceChat::NativeUpdate(const unsigned timeStep)
{
	if(m_CaptureDeviceUnpluggedPending)
	{
		vcDebug("NativeUpdate :: Processing capture device unplugged");
		m_CaptureDeviceUnplugged = true;
		if(m_CaptureDevice != NULL)
		{
			// rvoice frees the device immediately after calling our callback
			//gvFreeDevice(m_CaptureDevice);
			m_CaptureDevice = NULL;
		}

		if(m_CheckForReconnectsTimer == 0)
		{
			m_CheckForReconnectsTimer = CHECK_FOR_RECONNECTS_TIME_MS;
		}
		m_CaptureDeviceUnpluggedPending = false;
	}

	if(m_PlaybackDeviceUnpluggedPending)
	{
		vcDebug("NativeUpdate :: Processing playback device unplugged");
		m_PlaybackDeviceUnplugged = true;
		if(m_PlaybackDevice != NULL)
		{
			// rvoice frees the device immediately after calling our callback
			//gvFreeDevice(m_PlaybackDevice);
			m_PlaybackDevice = NULL;
		}

		if(m_CheckForReconnectsTimer == 0)
		{
			m_CheckForReconnectsTimer = CHECK_FOR_RECONNECTS_TIME_MS;
		}
		m_PlaybackDeviceUnpluggedPending = false;
	}

    if(m_NumTalkers > 0)
    {
        // update the local isTalking countdown
        Talker* talker = m_LocalTalkers[0];

        if(talker && talker->m_IsTalkingCountdown > 0)
        {
            talker->m_IsTalkingCountdown -= timeStep;
            if(talker->m_IsTalkingCountdown < 0)
            {
                talker->m_IsTalkingCountdown = 0;
            }
        }

        // check for device reconnects
        if(m_CheckForReconnectsTimer > 0)
        {
            m_CheckForReconnectsTimer -= timeStep;
            if(m_CheckForReconnectsTimer < 0)
            {
                m_CheckForReconnectsTimer = 0;

				if(m_CaptureDeviceUnplugged)
                {
                    vcAssert(m_CaptureDevice == NULL);
                    CheckForReconnectedCaptureDevice();
                }

                if(m_PlaybackDeviceUnplugged)
                {
                    vcAssert(m_PlaybackDevice == NULL);
                    CheckForReconnectedPlaybackDevice();
                }

                if(m_CaptureDeviceUnplugged || m_PlaybackDeviceUnplugged)
                {
                    m_CheckForReconnectsTimer = CHECK_FOR_RECONNECTS_TIME_MS;
                }
            }
        }

        // RAGE voice update
        RVoice::Update();
    }
}

bool
VoiceChat::NativePackChat(const int localTalkerIndex,
                            void* dst,
                            unsigned* numBytes)
{
    bool success = false;
    int duration = 0;
    vcAssert(numBytes);

    if(vcVerify(localTalkerIndex >= 0 && localTalkerIndex < RL_MAX_LOCAL_GAMERS))
    {
        unsigned sizeOfHeader = m_PackFrameStamp ? sizeof(u16) : 0;

        bool enoughSpace = *numBytes >= (sizeOfHeader + m_EncBytesPerFrame);

        if(enoughSpace && m_CaptureDevice && (RVoice::IsDeviceStarted(m_CaptureDevice, RV_CAPTURE) == true))
        {
            u16 frameStamp;
            double volume;

            // reserve space for the framestamp at the beginning of the packet
            int numAudioBytes = *numBytes - sizeOfHeader;
            success = RVoice::CapturePacket(m_CaptureDevice, (u8*)dst + sizeOfHeader, &numAudioBytes, &frameStamp, &volume) == true;
            success = success && (numAudioBytes > 0);

            // Note: I'm discarding audio with volume <= 0.1 for two reasons:
            // 1. bandwidth optimization, this saves about 10% of voice bandwidth
            // 2. PC audio drivers often report that there is a microphone attached
            //    when there isn't, and sends low volume audio continuously through
            //    the voice chat system (no idea why), wasting tons of bandwidth
    // 		if(success && (volume <= 0.1))
    // 		{
    // 			static unsigned totalBytesDiscarded = 0;
    // 			totalBytesDiscarded += numAudioBytes;
    // 			vcDebug("discarding %d bytes of low volume voice chat data (%f) to save bandwidth (total bytes discarded: %d)", numAudioBytes, volume, totalBytesDiscarded);
    // 			success = false;
    // 			numAudioBytes = 0;
    // 		}
            
            if(success)
            {
                vcAssert((numAudioBytes % m_EncBytesPerFrame) == 0);

                int numFrames = numAudioBytes / m_EncBytesPerFrame;
                int numSamples = numFrames * m_SamplesPerFrame;
                duration = (numSamples * 1000) / RVoice::GetSampleRate();

                vcAssert(duration > 0);

                if((float)volume > m_LocalTalkerSpeakingVolume)
                {
                    m_LocalTalkerSpeakingVolume = (float)volume;
                }

#if !__NO_OUTPUT
                if(m_PackFrameStamp)
                {
                    vcDebug("NativePackChat :: Framestamp: %d", frameStamp);
                }

                vcDebug("NativePackChat :: numAudioBytes:%d, numFrames:%d, numSamples:%d, duration:%d ms, frameStamp:%d, volume:%f",
                         numAudioBytes, numFrames, numSamples, duration, frameStamp, volume);
#endif

                if(m_PackFrameStamp)
                {
                    // insert the framestamp into the front of the packet
                    m_PackFrameStamp = false;
                    frameStamp = htons(frameStamp);
                    
                    vcAssert(sizeOfHeader == sizeof(u16));

                    memcpy(dst, (void*)&frameStamp, sizeof(u16));
                }

                unsigned packetLen = numAudioBytes + sizeOfHeader;
                vcAssert(*numBytes >= packetLen);
                *numBytes = packetLen;

                Talker* localTalker = m_LocalTalkers[localTalkerIndex];
                if(localTalker)
                {
                    localTalker->m_IsTalkingCountdown += duration;
                }
            }
            else
            {
                *numBytes = 0;
            }
        }
    }

    return success;
}

void
VoiceChat::NativeSubmitChat(Talker* talker,
                            const void* data,
                            const unsigned sizeofData,
                            const u32 /*timestamp*/)
{
    vcAssert(sizeofData >= m_EncBytesPerFrame);
    vcAssert((talker && talker->m_GamerInfo.IsValid()) &&
          (talker->m_GamerInfo.IsRemote() ||
          (talker->m_GamerInfo.IsLocal() && (m_LocalEchoEnabled WIN32PC_ONLY(|| m_IsVoiceTestMode)))));

    vcAssert(data && sizeofData);

    if(!IsPlaybackDeviceAvailable())
    {
        return;
    }

	if(m_MuteChatBecauseFocusLost)
	{
		vcDebug("NativeSubmitChat :: Not playing voice audio due to focus being lost.");
		return;
	}

    int len = (int)sizeofData;

    if(!netVerifyf((len % m_EncBytesPerFrame) == sizeof(u16), "NativeSubmitChat :: Invalid len (%d)", len))
    {
        return;
    }

    u8* packet = (u8*)data;

    // extract the frameStamp
    u16 frameStamp = 0;
    memcpy(&frameStamp, packet, sizeof(u16));
    frameStamp = ntohs(frameStamp);
    len -= sizeof(u16);
    packet += sizeof(u16);

#if !__NO_OUTPUT
    unsigned numAudioBytes = len;
    int numFrames = numAudioBytes / m_EncBytesPerFrame;
    int numSamples = numFrames * m_SamplesPerFrame;
    int duration = (numSamples * 1000) / RVoice::GetSampleRate();

    vcAssert(duration > 0);

    vcDebug("NativeSubmitChat :: NumAudioBytes: %d, NumFrames: %d, NumSamples: %d, Duration: %dms, FrameStamp: %d", numAudioBytes, numFrames, numSamples, duration, frameStamp);
#endif

    // queue it
    RockstarId source = talker->m_GamerInfo.GetGamerHandle().GetRockstarId();
    RVoice::PlayPacket(m_PlaybackDevice, packet, len, source, frameStamp, 0);
}

void VoiceChat::NativeSetAudioParam(Talker *UNUSED_PARAM(talker), const u32 UNUSED_PARAM(paramNameHash), const float UNUSED_PARAM(paramValue))
{

}

#else

bool
VoiceChat::NativeInit(const int /*maxLocalTalkers*/,
                    const int /*maxRemoteTalkers*/,
                    void* /*audioDevice*/,
                    const int /*cpuAffinity*/)
{
    //Stubbed to allow minimal applications to run
    return true;
}

void
VoiceChat::NativeShutdown()
{
}

bool
VoiceChat::NativeAddTalker(Talker* /*talker*/)
{
    //Stubbed to allow minimal applications to run
    return true;
}

void
VoiceChat::NativeRemoveTalker(Talker* /*talker*/)
{
}

bool
VoiceChat::NativeIsTalking(const Talker* /*talker*/) const
{
    return false;
}

void
VoiceChat::NativeUpdate(const unsigned /*timeStep*/)
{
}

bool
VoiceChat::NativePackChat(const int /*localTalkerIndex*/,
                            void* /*dst*/,
                            unsigned* /*numBytes*/)
{
    return false;
}

void
VoiceChat::NativeSubmitChat(Talker* /*that*/,
                            const void* /*data*/,
                            const unsigned /*sizeofData*/,
                            const u32 /*timestamp*/)
{
}

void VoiceChat::NativeSetAudioParam(Talker *UNUSED_PARAM(talker), const u32 UNUSED_PARAM(paramNameHash), const float UNUSED_PARAM(paramValue))
{

}

#endif

void VoiceChat::Talker::Init(const rlGamerInfo& gamerInfo,
                             const unsigned flags)
{
    vcAssert(!m_Peer);

#if USE_SPEEX
    vcAssert(!m_ProcessVoiceData);
    vcAssert(!m_SpeexJitterBuffer);
    vcAssert(!m_AccumulatedTimeStep);
    vcAssert(!m_SpeexBitsDec);
    vcAssert(!m_SpeexDecState);
#elif USE_LIBVOICE
    vcAssert(LIBVOICE_PORT_INVALID == m_InPort);
#endif  //USE_SPEEX

    m_GamerInfo = gamerInfo;
    m_Flags = flags;
    m_AboutToTalkTimer = 0;
	m_LastSend = 0;
	m_LastReceive = 0;
#if !__NO_OUTPUT
	m_AttemptedSend = false;
	m_TotalSent = 0;
	m_TotalReceived = 0;
#endif
    m_IsActive = true;
#if USE_SPEEX || USE_LIBVOICE || RSG_PC
    m_IsTalkingCountdown = 0;
#endif
	m_IsTalking = false;
#if RSG_DURANGO
	m_IsMuted = false;
	m_IsBlocked = false;
#endif
}

void VoiceChat::Talker::Shutdown()
{
    vcAssert(!m_Peer);

#if USE_SPEEX
    vcAssert(!m_ProcessVoiceData);
    vcAssert(!m_SpeexJitterBuffer);
    vcAssert(!m_AccumulatedTimeStep);
    vcAssert(!m_SpeexBitsDec);
    vcAssert(!m_SpeexDecState);
#elif USE_LIBVOICE
    vcAssert(LIBVOICE_PORT_INVALID == m_InPort);
#endif  //USE_SPEEX

    m_GamerInfo.Clear();
    m_Flags = 0;
    m_AboutToTalkTimer = 0;
	m_LastSend = 0;
	m_LastReceive = 0;
#if !__NO_OUTPUT
	m_AttemptedSend = false;
	m_TotalSent = 0;
	m_TotalReceived = 0;
#endif
    m_IsActive = false;
	m_IsTalking = false;
#if RSG_DURANGO
	m_IsMuted = false;
	m_IsBlocked = false;
#endif
}

#if RSG_ORBIS
VoiceChat::PortManager::PortManager() :
	m_HeadsetPortId(LIBVOICE_PORT_INVALID)
{

}

VoiceChat::PortManager::~PortManager()
{

}

void
VoiceChat::PortManager::Init(unsigned headsetPortId)
{
	m_HeadsetPortId = headsetPortId;
	OUTPUT_ONLY(int ret;)
	vcDebug("VoiceChat::PortManager::Init");

	for (int i = 0; i < MAX_VOICE_INPUT_PORTS; ++i)
	{
		bool isInitSuccessful = false;

		if(CELL_OK != (OUTPUT_ONLY(ret =) CreatePort_Voice(m_VoiceInputPortInfos[i].m_VoiceInputPortId, CELLVOICE_PORTTYPE_IN_VOICE)))
		{
			vcError("CreatePort(CELLVOICE_PORTTYPE_IN_VOICE) returned:0x%08x",ret);
		}
		else if(CELL_OK != (OUTPUT_ONLY(ret =) cellVoiceConnectIPortToOPort(m_VoiceInputPortInfos[i].m_VoiceInputPortId, m_HeadsetPortId)))
		{
			vcError("cellVoiceConnectIPortToOPort returned:0x%08x",ret);
		}

		if(CELL_OK != (OUTPUT_ONLY(ret =) CreatePort_PCM(m_VoiceInputPortInfos[i].m_ConnectedPcmOutputPortId, CELLVOICE_PORTTYPE_OUT_PCMAUDIO)))
		{
			vcError("CreatePort(CELLVOICE_PORTTYPE_OUT_PCM) returned:0x%08x",ret);
		}
		else if(CELL_OK != (OUTPUT_ONLY(ret =) cellVoiceConnectIPortToOPort(m_VoiceInputPortInfos[i].m_VoiceInputPortId, m_VoiceInputPortInfos[i].m_ConnectedPcmOutputPortId)))
		{
			vcError("cellVoiceConnectIPortToOPort (PCM) returned:0x%08x",ret);
		}
		else
		{
			isInitSuccessful = true;
			m_VoiceInputPortInfos[i].m_IsInitialized = true;
			m_VoiceInputPortInfos[i].m_IsAvailable = true;
		}

		vcAssertf(isInitSuccessful, "VoiceChat::PortManager::Init() - Port creation failure on index %d", i);
	}
}

void
VoiceChat::PortManager::Shutdown()
{
	OUTPUT_ONLY(int ret;)
	vcDebug("VoiceChat::PortManager::Shutdown");

	for (int i = 0; i < MAX_VOICE_INPUT_PORTS; ++i)
	{
		if (m_VoiceInputPortInfos[i].m_IsInitialized)
		{
			if (m_VoiceInputPortInfos[i].m_VoiceInputPortId != LIBVOICE_PORT_INVALID)
			{
				if (m_HeadsetPortId != LIBVOICE_PORT_INVALID)
				{
					if(CELL_OK != (OUTPUT_ONLY(ret =) cellVoiceDisconnectIPortFromOPort(m_VoiceInputPortInfos[i].m_VoiceInputPortId, m_HeadsetPortId)))
					{
						vcError("cellVoiceDisconnectIPortFromOPort returned:0x%08x", ret);
					}
				}

				if (m_VoiceInputPortInfos[i].m_ConnectedPcmOutputPortId != LIBVOICE_PORT_INVALID)
				{
					if(CELL_OK != (OUTPUT_ONLY(ret =) cellVoiceDisconnectIPortFromOPort(m_VoiceInputPortInfos[i].m_VoiceInputPortId, m_VoiceInputPortInfos[i].m_ConnectedPcmOutputPortId)))
					{
						vcError("cellVoiceDisconnectIPortFromOPort returned:0x%08x", ret);
					}
					else if(CELL_OK != (OUTPUT_ONLY(ret =) cellVoiceDeletePort(m_VoiceInputPortInfos[i].m_ConnectedPcmOutputPortId)))
					{
						vcError("cellVoiceDeletePort returned:0x%08x", ret);
					}
				}

				if(CELL_OK != (OUTPUT_ONLY(ret =) cellVoiceDeletePort(m_VoiceInputPortInfos[i].m_VoiceInputPortId)))
				{
					vcError("cellVoiceDeletePort returned:0x%08x", ret);
				}
			}
		}
		m_VoiceInputPortInfos[i].Reset();
	}

	m_HeadsetPortId = LIBVOICE_PORT_INVALID;
}

bool
VoiceChat::PortManager::GetAvailableVoiceInputPortId(unsigned &voiceInputPortId)
{
	bool isSuccess = false;

	for (int i = 0; i < MAX_VOICE_INPUT_PORTS && !isSuccess; ++i)
	{
		if (m_VoiceInputPortInfos[i].m_IsAvailable && m_VoiceInputPortInfos[i].m_IsInitialized)
		{
			vcDebug("GetAvailableVoiceInputPortId :: Acquired Port #%d", i);
			voiceInputPortId = m_VoiceInputPortInfos[i].m_VoiceInputPortId;
			m_VoiceInputPortInfos[i].m_IsAvailable = false;
			isSuccess = true;
		}
	}

	return isSuccess;
}

bool
VoiceChat::PortManager::GetConnectedPcmOutputPortId(const unsigned voiceInputPortId, unsigned &pcmOutputPortId)
{
	bool isSuccess = false;

	for (int i = 0; i < MAX_VOICE_INPUT_PORTS && !isSuccess; ++i)
	{
		if (m_VoiceInputPortInfos[i].m_VoiceInputPortId == voiceInputPortId)
		{
			pcmOutputPortId = m_VoiceInputPortInfos[i].m_ConnectedPcmOutputPortId;
			vcDebug("GetConnectedPcmOutputPortId :: Acquired PCM Port #%d", pcmOutputPortId);
			isSuccess = true;
		}
	}

	return isSuccess;
}

void
VoiceChat::PortManager::ReleaseVoiceInputPortId(const unsigned voiceInputPortId)
{
	for (int i = 0; i < MAX_VOICE_INPUT_PORTS; ++i)
	{
		if (m_VoiceInputPortInfos[i].m_VoiceInputPortId == voiceInputPortId)
		{
			vcDebug("ReleaseVoiceInputPortId :: Released Port # %d", i);
			m_VoiceInputPortInfos[i].m_IsAvailable = true;
			break;
		}
	}
}
#endif // RSG_ORBIS

}   //namespace rage
