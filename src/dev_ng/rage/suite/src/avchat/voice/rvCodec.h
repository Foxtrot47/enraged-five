#if RSG_PC
// 
// voice/rvcodec.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef RV_CODEC_H
#define RV_CODEC_H

#include "rv.h"

namespace rage
{
	//	Information to define a custom codec.
	struct RVCustomCodecInfo
	{
		int m_samplesPerFrame;  // Number of samples in an unencoded frame.
		int m_encodedFrameSize; // Number of bytes in an encoded frame.

		Functor1Ret<bool, RVDecoderData*> NewDecoderCB;			// Used to allocate a new decoder instance for each incoming source.
		Functor1<RVDecoderData> FreeDecoderCB;					// Used to free decoder data allocated through m_newDecoderCallback.
		Functor2<u8*, const s16*> EncoderCB;					// Used to encode data.
		Functor3<s16*, const u8*, RVDecoderData> DecodeAddCB;	// Called to decode data. Decode must add to, not set the output.
		Functor3<s16*, const u8*, RVDecoderData> DecodeSetCB;	// sets output (optional)
	};

	class RVCodec
	{
	public:

		static void Init();
		static void Shutdown();
		static void CleanupCodec();

		static bool SetCodec(RVCodecQuality codec);
		static void SetCustomCodec(RVCustomCodecInfo * info);

		static bool SetInternalCodec(RVCodecQuality codec);

		static void RawEncode(u8 * out, const s16 * in);
		static void RawDecodeAdd(s16 * out, const u8 * in, RVDecoderData /*data*/);
		static void RawDecodeSet(s16 * out, const u8 * in, RVDecoderData /*data*/);
		static void SetRawCodec(void);

		static void SetSampleRate(int sampleRate);
		static int GetSampleRate() { return m_SampleRate; }

		static bool NewDecoder(RVDecoderData * data);
		static void FreeDecoder(RVDecoderData data);

		static void Encode(u8 * out, const s16 * in);
		static void DecodeAdd(s16 * out, const u8 * in, RVDecoderData data);
		static void DecodeSet(s16 * out, const u8 * in, RVDecoderData data);

		static void ResetEncoder(void);

		static int GetBytesPerFrame() { return m_BytesPerFrame; }
		static int GetSamplesPerFrame() { return m_SamplesPerFrame; }
		static int GetEncodedFrameSize() { return m_EncodedFrameSize; }
		static int GetBytesPerSecond() { return m_BytesPerSecond; }

	private:

		static RVCustomCodecInfo RVICodecInfo;
		static bool RVICleanupInternalCodec;
		static bool RVIRawCodec;

		static int m_SamplesPerFrame;
		static int m_BytesPerFrame;
		static int m_EncodedFrameSize;
		static int m_BytesPerSecond;

		static int m_SampleRate;			//In samples per second.
	};

} // namespace rage

#endif // RV_CODEC_H

#endif // RSG_PC