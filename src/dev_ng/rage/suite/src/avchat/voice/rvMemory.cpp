#if RSG_PC
// 
// voice/rvMemory.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#include "rvMemory.h"
#include "rvDebug.h"

#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/new.h"

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rvoice, memory)
	#undef __rage_channel
	#define __rage_channel rvoice_memory

#if RV_DEBUG_MEM
	static int s_NumAllocs = 0;
	static int s_MaxAllocs = 0;
	static s64 s_BytesAllocated = 0;
	static s64 s_MaxBytesAllocated = 0;
#endif

	static sysCriticalSectionToken s_RvDebugMemCs;
	static sysCriticalSectionToken s_RvDebugSpewCs;

	void* RVMemory::Allocate(size_t size)
	{
#if RV_USE_RL_ALLOCATOR
		Assert(g_rlAllocator);
		u8* new_p = (u8*)RL_ALLOCATE(RVMemory, size);

		//if we don't have enough available mem in rline's heap
		//then fall back to the global heap.
		if(!new_p)
		{
			RL_RAGE_TRACK(RVMemory);
			new_p = rage_new u8[size];

#if RV_DEBUG_MEM
			rvDebug3("Allocated %u bytes from global heap (rline's largest block is only %u bytes)", size, largestBlock);
#endif
		}

#else
		RL_RAGE_TRACK(RVMemory);
		u8* new_p = rage_new u8[size];
#endif

#if RV_DEBUG_MEM
		if(new_p)
		{
			SYS_CS_SYNC(s_RvDebugMemCs);

			++s_NumAllocs;

			if (s_NumAllocs > s_MaxAllocs)
			{
				s_MaxAllocs = s_NumAllocs;
			}

#if RV_USE_RL_ALLOCATOR
			size_t s = rlGetAllocSize(new_p);
#else
			size_t s = sysMemAllocator::GetCurrent().GetSize(new_p);
#endif
			Assert(s);

			s_BytesAllocated += s;

			if(s_BytesAllocated > s_MaxBytesAllocated)
			{
				s_MaxBytesAllocated = s_BytesAllocated;
			}

			rvDebug3("Allocated %u bytes (allocs=%d  maxallocs=%d  totalbytes=%" I64FMT "d  maxbytes=%" I64FMT "d)", 
				s, s_NumAllocs, s_MaxAllocs, s_BytesAllocated, s_MaxBytesAllocated);
		}
#endif

		return new_p;
	}

	void RVMemory::Free(void* ptr)
	{
#if RV_DEBUG_MEM
		SYS_CS_SYNC(s_RvDebugMemCs);

		Assert(s_NumAllocs > 0);
		--s_NumAllocs;

		size_t s;

#if RV_USE_RL_ALLOCATOR
		if(g_rlAllocator->IsValidPointer(ptr))
		{
			s = rlGetAllocSize(ptr);
		}
		else
		{
			s = sysMemAllocator::GetCurrent().GetSize(ptr);
		}
#else
		s = sysMemAllocator::GetCurrent().GetSize(ptr);
#endif
		Assert(s);

		Assert(s_BytesAllocated > 0);      
		s_BytesAllocated -= s;
		Assert(s_BytesAllocated >= 0);

		rvDebug3("Freed %u bytes (allocs=%d  maxallocs=%d  totalbytes=%" I64FMT "d  maxbytes=%" I64FMT "d)", 
			s, s_NumAllocs, s_MaxAllocs, s_BytesAllocated, s_MaxBytesAllocated);
#endif

#if RV_USE_RL_ALLOCATOR
		if(g_rlAllocator->IsValidPointer(ptr))
		{
			rlFree(ptr);
		}
		else
		{
			delete [] (u8*)ptr;
		}
#else
		delete [] (u8*)ptr;
#endif
	}

	void* RVMemory::ReAllocate(void* ptr, size_t size)
	{
		u8* new_p = size ? (u8*)Allocate(size) : 0;

		if(ptr)
		{
			if(new_p)
			{
#if RV_USE_RL_ALLOCATOR
				size_t s = rlGetAllocSize(ptr);
#else
				size_t s = sysMemAllocator::GetCurrent().GetSize(ptr);
#endif
				s = s > size ? size : s;

				sysMemCpy(new_p, ptr, s);
			}

			Free(ptr);
		}

		return new_p;
	}


} // namespace rage

#endif // RSG_PC