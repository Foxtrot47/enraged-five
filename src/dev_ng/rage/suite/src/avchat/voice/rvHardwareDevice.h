#if RSG_PC
// 
// voice/rvHardwareDevice.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef RV_HARDWARE_DEVICE_H
#define RV_HARDWARE_DEVICE_H

#include "rvSource.h"

// rage headers
#include "atl/array.h"
#include "atl/delegate.h"
#include "system/criticalsection.h"

#ifndef WINXP
#define WINXP NTDDI_WINXP
#endif

#ifndef NTDDI_WINLH
#define NTDDI_WINLH NTDDI_LONGHORN
#endif

#ifndef NTDDI_WINS03
#define NTDDI_WINS03 NTDDI_WS03
#endif

#ifndef NTDDI_WIN2K3
#define NTDDI_WIN2K3 NTDDI_WINXP
#endif

#ifndef NTDDI_WXP
#define NTDDI_WXP NTDDI_WINXP
#endif

#ifndef NTDDI_XPSP2
#define NTDDI_XPSP2 NTDDI_WINXPSP2
#endif

#ifndef _WIN32_IE
#define _WIN32_IE 0x0500
#endif

#ifndef _NT_TARGET_VERSION_WIN7
#define _NT_TARGET_VERSION_WIN7 0x501
#endif

#define DIRECTSOUND_VERSION 0x0900

#pragma warning(disable:4201)
#include <mmsystem.h>
#include <dsound.h>
#if (DIRECTSOUND_VERSION == 0x0800)
#include <dvoice.h>
#endif

// amount of time to keep capturing even after voice drops below the capture threshold
// makes sure that speech that trails off, or slightly dips in volume, is still captured
#define RVI_HOLD_THRESHOLD_FRAMES       20

// buffer sizes for capture and playback
#define RVI_PLAYBACK_BUFFER_MILLISECONDS     200
#define RVI_CAPTURE_BUFFER_MILLISECONDS      1000

namespace rage
{
	// Device callbacks
	typedef atDelegate<void (RVHardwareDevice*)> DeviceUnpluggedFunctor;
	typedef atDelegate<void (RVHardwareDevice*, s16*, u16)> DeviceFilterFunctor;

	struct RVIHardwareData
	{
		bool m_playing;
		LPDIRECTSOUND m_playback;
		LPDIRECTSOUNDBUFFER m_playbackBuffer;
		DWORD m_playbackBufferSize;
		DWORD m_playbackBufferHalfSize;
		DWORD m_playbackBufferPosition;
		u16 m_playbackClock;
		RVISource m_playbackSources[RVSourceMgr::MAX_SOURCES];
		u32 m_playbackLastThink;

		bool m_capturing;
		LPDIRECTSOUNDCAPTURE m_capture;
		LPDIRECTSOUNDCAPTUREBUFFER m_captureBuffer;
		DWORD m_captureBufferSize;
		DWORD m_captureBufferPosition;
		double m_captureVolume;
		u16 m_captureClock;
		double m_captureThreshold;
		double m_pushToTalkCaptureThreshold;
		u32 m_captureLastCrossedThresholdTime;
		u32 m_captureHoldTimeMs;

		RVIHardwareData()
		{
			sysMemSet(this, 0, sizeof(RVIHardwareData));
			m_captureHoldTimeMs = 2000; // 2 second default hold time
			m_captureLastCrossedThresholdTime = 0;
		}

		void ClearSourceList()
		{
			for (int i = 0; i < RVSourceMgr::MAX_SOURCES; i++)
			{
				RVSourceMgr::FreeSource(&m_playbackSources[i]);
			}
		}
	};

	class RVHardwareDevice
	{
	public:

		RVHardwareDevice();
		bool Init(RVDeviceID deviceID, RVHardwareType hardwareType, RVDeviceType type);
		void Shutdown();
		bool PlaybackUpdate();

		bool Start(RVDeviceType type);
		void Stop(RVDeviceType type);
		bool IsStarted(RVDeviceType type);

		void SetDeviceVolume(double threshold, RVDeviceType type);
		double GetDeviceVolume(RVDeviceType type);

		void SetCaptureThreshold(double threshold);
		double GetCaptureThreshold();

		void SetPushToTalkCaptureThreshold(double threshold);
		double GetCapturePushToTalkThreshold();

		void RVHardwareDevice::SetCaptureHoldTimeMs(u32 holdTimeMs);
		u32 RVHardwareDevice::GetCaptureHoldTimeMs();

		int GetAvailableCaptureBytes();
		bool CapturePacket(u8 * packet, int * len, u16 * frameStamp, double * volume);

		void PlayPacket(const u8 * packet, int len, RVSource source, u16 frameStamp, bool mute);

		bool IsSourceTalking(RVSource source);
		int ListTalkingSources(RVSource sources[], int maxSources);

		int GetNumChannels();
		void GetChannelName(int channel, wchar_t[RV_CHANNEL_NAME_LEN]);

		void SetChannel(int channel);
		int GetChannel();

		void SetUnpluggedCallback(DeviceUnpluggedFunctor cb) { m_UnpluggedCB = cb; }
		bool HasUnpluggedCB();
		void Unplugged();
		bool IsUnplugged() { return m_bUnplugged; }

		void CaptureFilter(s16 * audio, u16 frameStamp);
		void SetCaptureFilter(DeviceFilterFunctor cb) { m_CaptureFilterCB = cb; }
		bool HasCaptureFilter();

		void PlaybackFilter(s16 * audio, u16 frameStamp);
		void SetPlaybackFilter(DeviceFilterFunctor cb) { m_PlaybackFilterCB = cb; }
		bool HasPlaybackFilter();

		void SetSavedCaptureThreshold(double threshold) { m_savedCaptureThreshold = threshold; }
		double GetSavedCaptureThreshold() { return m_savedCaptureThreshold; }

		RVCaptureMode GetCaptureMode() { return m_captureMode; }
		void SetCaptureMode(RVCaptureMode mode) { m_captureMode = mode; }

		RVDeviceType GetDeviceTypes() { return m_Types; }

#if !__NO_OUTPUT
		const char * GetDeviceIdStr();
#endif

	private:

		double GetCurrentThreshold();

		RVIHardwareData m_Data;
		RVDeviceID m_DeviceID;

#if !__NO_OUTPUT
		char m_DeviceIdStr[38];
#endif

		RVHardwareType m_HardwareType;
		RVDeviceType m_Types;
		RVCaptureMode m_captureMode;
		double m_savedCaptureThreshold;

		bool m_bUnplugged;

		DeviceUnpluggedFunctor m_UnpluggedCB;
		DeviceFilterFunctor m_CaptureFilterCB;
		DeviceFilterFunctor m_PlaybackFilterCB;

		sysCriticalSectionToken m_Cs;
	};

} // namespace rage

#endif // RV_HARDWARE_DEVICE_H

#endif // RSG_PC
