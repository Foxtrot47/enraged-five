#if RSG_PC
// 
// voice/rv.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#include "rv.h"
#include "rvCodec.h"
#include "rvDebug.h"
#include "rvSource.h"
#include "rvFrame.h"
#include "rvDirectSound.h"
#include "rvHardwareDevice.h"

namespace rage
{
	const float RVoice::MIN_THRESHOLD = 0.05f;

	RAGE_DEFINE_CHANNEL(rvoice)
	RAGE_DEFINE_SUBCHANNEL(rvoice, rv)
	#undef __rage_channel
	#define __rage_channel rvoice_rv

	bool RVoice::Init(HWND hWnd)
	{
		// init codecs
		RVCodec::Init();

		RVIGlobalMute = false;

		// startup the devices
		if(!RVDirectSound::Init(hWnd))
		{
			rvDebug3("RVDirectSound failed initialization");
			return false;
		}

		return true;
	}

	void RVoice::Shutdown()
	{
		RVDirectSound::Shutdown();
		RVCodec::Shutdown();
		RVFrameMgr::Shutdown();
	}

	void RVoice::Update()
	{
		RVDirectSound::Update();
	}

	/**********
	** CODEC **
	**********/
	bool RVoice::SetCodec(RVCodecQuality codec)
	{
		return RVCodec::SetCodec(codec);
	}

	void RVoice::GetCodecInfo(int * samplesPerFrame, int * encodedFrameSize, int * bitsPerSecond)
	{
		if(samplesPerFrame)
		{
			*samplesPerFrame = RVCodec::GetSamplesPerFrame();
		}
		if(encodedFrameSize)
		{
			*encodedFrameSize = RVCodec::GetEncodedFrameSize();
		}
		if(bitsPerSecond)
		{
			*bitsPerSecond = (int)(8 * RVCodec::GetBytesPerSecond() * RVCodec::GetEncodedFrameSize() / RVCodec::GetBytesPerFrame());
		}
	}

	/****************
	** Sample Rate **
	****************/
	void RVoice::SetSampleRate(int sampleRate)
	{
		RVCodec::SetSampleRate(sampleRate);
	}

	int RVoice::GetSampleRate(void)
	{
		return RVCodec::GetSampleRate();
	}

	/************
	** DEVICES **
	************/
	int RVoice::ListDevices(RVDeviceInfo devices[], int maxDevices, RVDeviceType types)
	{
		return RVDirectSound::HardwareListDevices(devices, maxDevices, types);
	}

	RVHardwareDevice* RVoice::NewDevice(RVDeviceID deviceID, RVDeviceType type)
	{
		return RVDirectSound::HardwareNewDevice(deviceID, type);
	}

	void RVoice::FreeDevice(RVHardwareDevice* device)
	{
		RVDirectSound::DeleteDeviceFromList(device);
	}

	bool RVoice::StartDevice(RVHardwareDevice* device, RVDeviceType type)
	{
		return device->Start(type);
	}

	void RVoice::StopDevice(RVHardwareDevice* device, RVDeviceType type)
	{
		device->Stop(type);
	}

	bool RVoice::IsDeviceStarted(RVHardwareDevice* device, RVDeviceType type)
	{
		return device->IsStarted(type);
	}

	void RVoice::SetDeviceVolume(RVHardwareDevice* device, RVDeviceType type, double volume)
	{
		volume = max(volume, 0.0);
		volume = min(volume, 1.0);
		RVHardwareDevice* dev = (RVHardwareDevice*)device;
		if (dev)
		{
			dev->SetDeviceVolume(volume, type);
		}
	}

	double RVoice::GetDeviceVolume(RVHardwareDevice* device, RVDeviceType type)
	{
		return device->GetDeviceVolume(type);
	}

	void RVoice::SetUnpluggedCallback(RVHardwareDevice* device, DeviceUnpluggedFunctor unpluggedCallback)
	{
		device->SetUnpluggedCallback(unpluggedCallback);
	}

	void RVoice::SetFilterCallback(RVHardwareDevice* device, RVDeviceType type, DeviceFilterFunctor callback)
	{
		if (type & RV_CAPTURE)
		{
			device->SetCaptureFilter(callback);
		}
		if (type & RV_PLAYBACK)
		{
			device->SetPlaybackFilter(callback);
		}
	}

	/************
	** CAPTURE **
	************/
	bool RVoice::CapturePacket(RVHardwareDevice* device, u8 * packet, int * len, u16 * frameStamp, double * volume)
	{
		bool bResult = device->CapturePacket(packet, len, frameStamp, volume);
		
		if (device->IsUnplugged())
		{
			RVDirectSound::DeleteDeviceFromList(device);
		}

		return bResult;
	}

	int RVoice::GetAvailableCaptureBytes(RVHardwareDevice* device)
	{
		return device->GetAvailableCaptureBytes();
	}

	void RVoice::SetCaptureThreshold(RVHardwareDevice* device, double threshold)
	{
		// Note: on PC, don't allow a threshold < MIN_THRESHOLD (0.05):
		// PC audio drivers often report that there is a microphone attached
		// when there isn't, and sends low volume audio continuously through
		// the voice chat system (fan noise?), wasting tons of bandwidth.
		if (threshold < MIN_THRESHOLD)
		{
			rvDebug3("Capture threshold (%f) below minimum threshold (%f)", threshold, MIN_THRESHOLD);
			threshold = MIN_THRESHOLD;
		}

		device->SetCaptureThreshold(threshold);
	}

	void RVoice::SetPushToTalkCaptureThreshold(RVHardwareDevice* device, double threshold)
	{
		device->SetPushToTalkCaptureThreshold(threshold);
	}

	double RVoice::GetCaptureThreshold(RVHardwareDevice* device)
	{
		return device->GetCaptureThreshold();
	}

	void RVoice::SetCaptureHoldTimeMs(RVHardwareDevice* device, u32 holdTimeMs)
	{
		device->SetCaptureHoldTimeMs(holdTimeMs);
	}

	double RVoice::GetCaptureHoldTimeMs(RVHardwareDevice* device)
	{
		return device->GetCaptureHoldTimeMs();
	}

	void RVoice::SetCaptureMode(RVHardwareDevice* device, RVCaptureMode captureMode)
	{
		//This only works with Capture devices.
		rvAssertf(device->GetDeviceTypes() & RV_CAPTURE, "SetCaptureMode called for non-capture device");

		//See if we are switching from Threshold to PTT.
		if ((device->GetCaptureThreshold() == RVCaptureModeThreshold) && (captureMode == RVCaptureModePushToTalk))
		{
			double savedThreshold = GetCaptureThreshold(device);
			device->SetSavedCaptureThreshold(savedThreshold);
			
			// set to 0
			SetCaptureThreshold(device, 0.0f);

			//Stop the capture device.
			if (IsDeviceStarted(device, RV_CAPTURE))
			{
				StopDevice(device, RV_CAPTURE);
			}
		}
		//See if we are switching from PTT to Threshold.
		if ((device->GetCaptureMode() == RVCaptureModePushToTalk) && (captureMode == RVCaptureModeThreshold))
		{
			SetCaptureThreshold(device, device->GetSavedCaptureThreshold());

			//Turn on the capture device.
			device->Start(RV_CAPTURE);
		}

		device->SetCaptureMode(captureMode);
	}

	void RVoice::SetPushToTalk(RVHardwareDevice* device, bool talkOn)
	{
		if (talkOn)
		{
			StartDevice(device, RV_CAPTURE);
		}
		else
		{
			StopDevice(device, RV_CAPTURE);
		}
	}

	bool RVoice::GetPushToTalk(RVHardwareDevice* device)
	{
		return IsDeviceStarted(device, RV_CAPTURE);
	}

	/*************
	** PLAYBACK **
	*************/
	void RVoice::PlayPacket(RVHardwareDevice* device, const u8 * data, int len, RVSource source, u16 frameStamp, bool mute)
	{
		device->PlayPacket(data, len, source, frameStamp, mute);
	}

	bool RVoice::IsSourceTalking(RVHardwareDevice* device, RVSource source)
	{
		return device->IsSourceTalking(source);
	}

	void RVoice::SetGlobalMute(bool mute)
	{
		RVSourceMgr::SetGlobalMute(mute);
	}

	bool RVoice::GetGlobalMute(void)
	{
		return RVSourceMgr::GetGlobalMute();
	}

	/*************
	** CHANNELS **
	*************/
	int RVoice::GetNumChannels(RVHardwareDevice* device)
	{
		return device->GetNumChannels();
	}

	void RVoice::GetChannelName(RVHardwareDevice* device, int channel, wchar_t name[RV_CHANNEL_NAME_LEN])
	{
		if (device->GetNumChannels() > 0)
		{
			device->GetChannelName(channel, name);
		}
	}

	void RVoice::SetChannel(RVHardwareDevice* device,  int channel)
	{
		device->SetChannel(channel);
	}

	int RVoice::GetChannel(RVHardwareDevice* device)
	{
		return device->GetChannel();
	}

} // namespace rage

#endif // RSG_PC
