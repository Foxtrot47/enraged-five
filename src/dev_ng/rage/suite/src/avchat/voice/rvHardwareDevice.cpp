#if RSG_PC
// 
// voice/rvHardwareDevice.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#include "rvHardwareDevice.h"
#include "rvSource.h"
#include "rvCodec.h"
#include "rvUtil.h"
#include "rvDirectSound.h"

#include <stdio.h>
#include <math.h>

#pragma warning(disable:4201)
#include <mmsystem.h>
#include <dsound.h>
#if (DIRECTSOUND_VERSION == 0x0800)
#include <dvoice.h>
#endif

// rage includes
#include "system/timer.h"

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rvoice, hardwaredevice)
	#undef __rage_channel
	#define __rage_channel rvoice_hardwaredevice

	RVHardwareDevice::RVHardwareDevice()
		: m_Types(0)
		, m_captureMode((RVCaptureMode)0)
		, m_bUnplugged(false)
		, m_UnpluggedCB(nullptr)
		, m_CaptureFilterCB(nullptr)
		, m_PlaybackFilterCB(nullptr)
		, m_savedCaptureThreshold(0)
	{
		sysMemSet(&m_DeviceID, 0, sizeof(m_DeviceID));

#if !__NO_OUTPUT
		m_DeviceIdStr[0] = '\0';
#endif
	}

	bool RVHardwareDevice::Init(RVDeviceID deviceID, RVHardwareType hardwareType, RVDeviceType type)
	{
		m_DeviceID = deviceID;
		m_HardwareType = hardwareType;
		m_Types = type;

#if !__NO_OUTPUT
		formatf(m_DeviceIdStr,
			"%08lx-%04hx-%04hx-%02x%02x-%02x%02x%02x%02x%02x%02x",
			m_DeviceID.Data1, m_DeviceID.Data2, m_DeviceID.Data3, m_DeviceID.Data4[0],
			m_DeviceID.Data4[1], m_DeviceID.Data4[2], m_DeviceID.Data4[3], m_DeviceID.Data4[4],
			m_DeviceID.Data4[5], m_DeviceID.Data4[6], m_DeviceID.Data4[7]);

		rvDebug3("RVHardwareDevice::Init [%s]", GetDeviceIdStr());
#endif

		HRESULT result;

		// check if they want to init playback
		if(type == RV_PLAYBACK)
		{
			RVSourceMgr::Init();

			// create the interface
			result = DirectSoundCreate(&m_DeviceID, &m_Data.m_playback, NULL);
			if(FAILED(result))
			{
				rvError("DirectSoundCreated failed.");
				return false;
			}

			// setup the playback device
			if(!RVDirectSound::InitPlaybackDevice(&m_Data))
			{
				rvError("InitPlaybackDevice failed.");
				IDirectSound_Release(m_Data.m_playback);
				m_Data.m_playback = NULL;
				return false;
			}
		}
		// check if they want to init capture
		else if(type == RV_CAPTURE)
		{
			// create the interface
			result = DirectSoundCaptureCreate(&m_DeviceID, &m_Data.m_capture, NULL);
			if(FAILED(result))
			{
				rvError("DirectSoundCaptureCreate failed.");
				return false;
			}

			// setup the capture device
			if(!RVDirectSound::InitCaptureDevice(&m_Data))
			{
				rvError("InitCaptureDevice failed.");
				IDirectSoundCapture_Release(m_Data.m_capture);
				m_Data.m_capture = NULL;
				return false;
			}

			// set some data vars
			m_Data.m_captureVolume = 1.0;
			m_Data.m_captureClock = 0;
			m_Data.m_captureLastCrossedThresholdTime = (m_Data.m_captureClock - RVI_HOLD_THRESHOLD_FRAMES - 1);
		}

		return true;
	}

	void RVHardwareDevice::Shutdown()
	{
		rvDebug3("RVHardwareDevice::Shutdown [%s]", GetDeviceIdStr());

		if(m_Types == RV_PLAYBACK)
		{
			for (int i = 0; i < RVSourceMgr::MAX_SOURCES; i++)
			{
				RVSourceMgr::FreeSource(&m_Data.m_playbackSources[i]);
			}

			if(m_Data.m_playbackBuffer)
			{
				IDirectSoundBuffer_Release(m_Data.m_playbackBuffer);
				m_Data.m_playbackBuffer = NULL;
			}
			if(m_Data.m_playback)
			{
				IDirectSound_Release(m_Data.m_playback);
				m_Data.m_playback = NULL;
			}
		}
		
		if (m_Types == RV_CAPTURE)
		{
			if(m_Data.m_captureBuffer)
			{
				IDirectSoundCaptureBuffer_Stop(m_Data.m_captureBuffer);
				IDirectSoundCaptureBuffer_Release(m_Data.m_captureBuffer);
				m_Data.m_captureBuffer = NULL;
			}
			if(m_Data.m_capture)
			{
				IDirectSoundCapture_Release(m_Data.m_capture);
				m_Data.m_capture = NULL;
			}
		}
	}

	bool RVHardwareDevice::PlaybackUpdate()
	{
		SYS_CS_SYNC(m_Cs);

		HRESULT result;
		DWORD lockPosition;
		LPVOID audioPtr1, audioPtr2;
		DWORD audioLen1, audioLen2;
		bool wroteToBuffer;
		DWORD newBytes;
		DWORD rawPlayCursor;
		DWORD playCursor;
		DWORD writeCursor;
		u32 now;
		int diff;
		int numFrames;
		int i;

		// don't do anything if we're not playing
		if(!m_Data.m_playing)
		{
			return true;
		}

		// get the current position
		result = IDirectSoundBuffer_GetCurrentPosition(m_Data.m_playbackBuffer, &rawPlayCursor, &writeCursor);
		if(FAILED(result))
		{
			return false;
		}

		// we only want to deal with whole frames
		playCursor = (DWORD)RVUtil::RoundDownToNearestMultiple((int)rawPlayCursor, RVCodec::GetBytesPerFrame());

		// get the number of new bytes
		newBytes = (((playCursor + m_Data.m_playbackBufferSize) - m_Data.m_playbackBufferPosition) % m_Data.m_playbackBufferSize);

		// before we store the new position, save the old one
		lockPosition = m_Data.m_playbackBufferPosition;

		// store the new position
		m_Data.m_playbackBufferPosition = playCursor;

		// figure out how long it has been since out last think
		now = sysTimer::GetSystemMsTime();
		diff = (int)(now - m_Data.m_playbackLastThink);
		m_Data.m_playbackLastThink = now;

		// adjust the time based on the number of new samples we know about
		diff -= RVUtil::DivideByBytesPerMillisecond(newBytes);

		// adjust again based on the raw play cursor
		// because newBytes is based on an adjusted play cursor
		diff -= (rawPlayCursor - playCursor);

		// diff should now be approximately 0
		// if it is closer to a multiple of the buffer length (in ms),
		// that is because we have missed at least one loop through the buffer
		// we use half of the buffer size because it is half way between
		// what we expect and what we are checking for
		if(diff >= RVUtil::DivideByBytesPerMillisecond((int)m_Data.m_playbackBufferHalfSize))
		{
			int numMissedLoops;
			int numMissedFrames;
			int msPerBuffer = RVUtil::DivideByBytesPerMillisecond((int)m_Data.m_playbackBufferSize);

			// estimate how many loops we missed
			numMissedLoops = (RVUtil::RoundToNearestMultiple(diff, msPerBuffer) / msPerBuffer);

			// convert to frames
			numMissedFrames = (numMissedLoops * (m_Data.m_playbackBufferSize / RVCodec::GetBytesPerFrame()));

			// adjust the clock
			// Expanded to remove warnings in VS2K5
			m_Data.m_playbackClock = m_Data.m_playbackClock + (u16)(numMissedFrames);
		}

		// if we don't have any new bytes, there's nothing to do
		if(newBytes == 0)
		{
			return true;
		}

		// lock the appropriate half of the playback buffer
		result = IDirectSoundBuffer_Lock(m_Data.m_playbackBuffer, lockPosition, newBytes, &audioPtr1, &audioLen1, &audioPtr2, &audioLen2, 0);
		if(FAILED(result))
		{
			return false;
		}

		// fill it
		numFrames = (audioLen1 / RVCodec::GetBytesPerFrame());
		wroteToBuffer = RVSourceMgr::WriteSourcesToBuffer(m_Data.m_playbackSources, m_Data.m_playbackClock, (s16 *)audioPtr1, numFrames);
		if(!wroteToBuffer)
		{
			sysMemSet(audioPtr1, 0, audioLen1);
		}

		// filter
		if(HasPlaybackFilter())
		{
			for(i = 0 ; i < numFrames ; i++)
			{
				PlaybackFilter((s16 *)audioPtr1 + (RVCodec::GetSamplesPerFrame() * i), (u16)(m_Data.m_playbackClock + i));
			}
		}

		// update the clock
		// Expanded to remove warnings in VS2K5
		m_Data.m_playbackClock = m_Data.m_playbackClock + (u16)numFrames;

		// do the same for the second pointer, if it is set
		if(audioPtr2)
		{
			// fill it
			numFrames = (audioLen2 / RVCodec::GetBytesPerFrame());
			wroteToBuffer = RVSourceMgr::WriteSourcesToBuffer(m_Data.m_playbackSources, m_Data.m_playbackClock, (s16 *)audioPtr2, numFrames);
			if(!wroteToBuffer)
			{
				sysMemSet(audioPtr2, 0, audioLen2);
			}

			// filter
			if(HasPlaybackFilter())
			{
				for(i = 0 ; i < numFrames ; i++)
				{
					PlaybackFilter((s16 *)audioPtr2 + (RVCodec::GetSamplesPerFrame() * i), (u16)(m_Data.m_playbackClock + i));
				}
			}

			// update the clock
			// Expanded to remove warnings in VS2K5
			m_Data.m_playbackClock = m_Data.m_playbackClock + (u16)numFrames;
		}

		// unlock the buffer
		result = IDirectSoundBuffer_Unlock(m_Data.m_playbackBuffer, audioPtr1, audioLen1, audioPtr2, audioLen2);
		if(FAILED(result))
		{
			rvError("IDirectSoundBuffer_Unlock failed (Result: %d)", result);
			return false;
		}

		return true;
	}

	bool RVHardwareDevice::Start(RVDeviceType type)
	{
		SYS_CS_SYNC(m_Cs);

		// start playback device
		if(type == RV_PLAYBACK)
		{
			return RVDirectSound::StartPlaybackDevice(&m_Data);
		}
		else
		{
			// otherwise, start capture device
			return RVDirectSound::StartCaptureDevice(&m_Data);
		}
	}

	void RVHardwareDevice::Stop(RVDeviceType type)
	{
		SYS_CS_SYNC(m_Cs);

		if(type == RV_PLAYBACK)
		{
			// stop playback device
			RVDirectSound::StopPlaybackDevice(&m_Data);
		}
		else
		{
			// otherwise, stop capture device
			RVDirectSound::StopCaptureDevice(&m_Data);
		} 
	}

	bool RVHardwareDevice::IsStarted(RVDeviceType type)
	{
		if(type == RV_PLAYBACK)
		{
			return RVDirectSound::IsPlaybackDeviceStarted(&m_Data);
		}

		return RVDirectSound::IsCaptureDeviceStarted(&m_Data);
	}

	void RVHardwareDevice::SetDeviceVolume(double threshold, RVDeviceType type)
	{
		if(type == RV_PLAYBACK)
		{
			RVDirectSound::SetPlaybackDeviceVolume(&m_Data, threshold);
		}
		else
		{
			RVDirectSound::SetCaptureDeviceVolume(&m_Data, threshold);
		}
	}

	double RVHardwareDevice::GetDeviceVolume(RVDeviceType type)
	{
		if(type == RV_PLAYBACK)
		{
			return RVDirectSound::GetPlaybackDeviceVolume(&m_Data);
		}

		return RVDirectSound::GetCaptureDeviceVolume(&m_Data);
	}

	void RVHardwareDevice::SetCaptureThreshold(double threshold)
	{
		m_Data.m_captureThreshold = threshold;
	}

	double RVHardwareDevice::GetCaptureThreshold()
	{
		return m_Data.m_captureThreshold;
	}

	void RVHardwareDevice::SetPushToTalkCaptureThreshold(double threshold)
	{
		m_Data.m_pushToTalkCaptureThreshold = threshold;
	}

	double RVHardwareDevice::GetCapturePushToTalkThreshold()
	{
		return m_Data.m_pushToTalkCaptureThreshold;
	}

	void RVHardwareDevice::SetCaptureHoldTimeMs(u32 holdTimeMs)
	{
		m_Data.m_captureHoldTimeMs = holdTimeMs;
	}

	u32 RVHardwareDevice::GetCaptureHoldTimeMs()
	{
		return m_Data.m_captureHoldTimeMs;
	}

	int RVHardwareDevice::GetAvailableCaptureBytes()
	{
		HRESULT result;
		DWORD readPosition;
		DWORD newBytes;
		int numFrames;

		// get the current position
		result = IDirectSoundCaptureBuffer_GetCurrentPosition(m_Data.m_captureBuffer, NULL, &readPosition);
		if(FAILED(result))
		{
			Unplugged();
			return 0;
		}

		// get the number of new bytes
		newBytes = (((readPosition + m_Data.m_captureBufferSize) - m_Data.m_captureBufferPosition) % m_Data.m_captureBufferSize);

		// figure out how many frames this is
		numFrames = (newBytes / RVCodec::GetBytesPerFrame());

		// calculate how many bytes this is once encoded
		newBytes = (numFrames * RVCodec::GetEncodedFrameSize());

		return newBytes;
	}

	bool RVHardwareDevice::CapturePacket(u8 * packet, int * len, u16 * frameStamp, double * volume)
	{
		HRESULT result;
		DWORD readPosition;
		DWORD newBytes;
		LPVOID audioPtr1, audioPtr2;
		s16 * audioPtr;
		DWORD audioLen, audioLen1, audioLen2;
		bool overThreshold = false;
		int numFrames;
		int i, j;
		int lenAvailable;
		int framesAvailable;

		// figure out how many encoded bytes they can handle
		lenAvailable = *len;

		// clear the len and volume
		*len = 0;
		if(volume)
		{
			*volume = 0;
		}

		// get the current position
		result = IDirectSoundCaptureBuffer_GetCurrentPosition(m_Data.m_captureBuffer, NULL, &readPosition);
		if(FAILED(result))
		{
			Unplugged();
			return false;
		}

		// get the number of new bytes
		newBytes = (((readPosition + m_Data.m_captureBufferSize) - m_Data.m_captureBufferPosition) % m_Data.m_captureBufferSize);

		// figure out how many frames this is
		numFrames = (newBytes / RVCodec::GetBytesPerFrame());

		// figure out how many frames they can handle
		framesAvailable = (lenAvailable / RVCodec::GetEncodedFrameSize());

		if (numFrames > framesAvailable)
		{
			rvDebug3("%d frames, %d available, %d too many", numFrames, framesAvailable, numFrames - framesAvailable);
		}

		// don't give them more frames than they can handle
		numFrames = min(numFrames, framesAvailable);
		if(!numFrames)
		{
			return false;
		}

		// round off the bytes to a frame boundary
		newBytes = (numFrames * RVCodec::GetBytesPerFrame());

		// only do this part if we are capturing
		if(m_Data.m_capturing)
		{
			// lock the buffer
			result = IDirectSoundCaptureBuffer_Lock(m_Data.m_captureBuffer, m_Data.m_captureBufferPosition, newBytes, &audioPtr1, &audioLen1, &audioPtr2, &audioLen2, 0);
			if(FAILED(result))
			{
				Unplugged();
				return false;
			}

			// get the volume if requested
			if(volume)
			{
				double vol1 = RVUtil::GetSamplesVolume((s16*)audioPtr1, audioLen1 / RV_BYTES_PER_SAMPLE);
				double vol2 = RVUtil::GetSamplesVolume((s16*)audioPtr2, audioLen2 / RV_BYTES_PER_SAMPLE);
				*volume = max(vol1, vol2);
			}

			// check against the threshold
			if(volume)
			{
				// we already got the volume, so use that to check
				overThreshold = (*volume >= GetCurrentThreshold());
			}
			else
			{
				// we didn't get a volume, so check the samples directly
				overThreshold = RVUtil::IsOverThreshold((s16*)audioPtr1, audioLen1 / RV_BYTES_PER_SAMPLE, GetCurrentThreshold());

				// if not over threshold, and there is a second portion of audio, check it
				if(!overThreshold && audioPtr2)
				{
					overThreshold = RVUtil::IsOverThreshold((s16*)audioPtr2, audioLen2 / RV_BYTES_PER_SAMPLE, GetCurrentThreshold());
				}
			}

			// did the audio cross the threshold?
			if(overThreshold)
			{
				// update the time at which we crossed
				m_Data.m_captureLastCrossedThresholdTime = sysTimer::GetSystemMsTime();
			}
			else
			{
				// check if we are still within the hold time
				u32 timeSinceLastThreshold = (sysTimer::GetSystemMsTime() - m_Data.m_captureLastCrossedThresholdTime);
				overThreshold = timeSinceLastThreshold < m_Data.m_captureHoldTimeMs;
			}

			if(overThreshold)
			{
				// store the frameStamp
				*frameStamp = m_Data.m_captureClock;

				// setup the starting audio pointer and len
				audioPtr = (s16*)audioPtr1;
				audioLen = audioLen1;

				// handle the data one frame at a time
				for(i = 0 ; i < numFrames ; i++)
				{
					// scale the data
					if(m_Data.m_captureVolume < 1.0)
					{
						for(j = 0 ; j < RVCodec::GetSamplesPerFrame() ; j++)
						{
							audioPtr[j] = (s16)(audioPtr[j] * m_Data.m_captureVolume);
						}
					}

					// filter
					CaptureFilter(audioPtr, (u16)(m_Data.m_captureClock + i));

					// encode the buffer into the packet
					RVCodec::Encode(packet + (RVCodec::GetEncodedFrameSize() * i), audioPtr);

					// update the loop info as needed
					if(audioLen > (DWORD)RVCodec::GetBytesPerFrame())
					{
						audioPtr += RVCodec::GetSamplesPerFrame();
						audioLen -= RVCodec::GetBytesPerFrame();
					}
					else
					{
						audioPtr = (s16*)audioPtr2;
						audioLen = audioLen2;
					}
				}
			}

			// unlock the buffer
			result = IDirectSoundCaptureBuffer_Unlock(m_Data.m_captureBuffer, audioPtr1, audioLen1, audioPtr2, audioLen2);
			if(FAILED(result))
			{
				Unplugged();
				return false;
			}
		}

		// set the new position
		m_Data.m_captureBufferPosition += newBytes;
		m_Data.m_captureBufferPosition %= m_Data.m_captureBufferSize;

		// increment the clock
		// Expanded to remove warnings in VS2K5
		m_Data.m_captureClock = m_Data.m_captureClock + (u16)numFrames;

		// set the len
		*len = (numFrames * RVCodec::GetEncodedFrameSize());

		// return false if we didn't get a packet
		if(!overThreshold)
		{
			return false;
		}

		return true;
	}

	void RVHardwareDevice::PlayPacket(const u8 * packet, int len, RVSource source, u16 frameStamp, bool mute)
	{
		// don't do anything if we're not playing
		if(!m_Data.m_playing)
		{
			return;
		}

		//add it
		RVSourceMgr::AddPacketToSourceList(m_Data.m_playbackSources, packet, len, source, frameStamp, mute, m_Data.m_playbackClock);
	}

	bool RVHardwareDevice::IsSourceTalking(RVSource source)
	{
		// don't do anything if we're not playing
		if(!m_Data.m_playing)
		{
			return false;
		}

		return RVSourceMgr::IsSourceTalking(m_Data.m_playbackSources, source);
	}

	int RVHardwareDevice::ListTalkingSources(RVSource sources[], int maxSources)
	{
		// don't do anything if we're not playing
		if(!m_Data.m_playing)
		{
			return false;
		}

		return RVSourceMgr::ListTalkingSources(m_Data.m_playbackSources, sources, maxSources);
	}


	void RVHardwareDevice::Unplugged()
	{
		if (m_UnpluggedCB.IsBound())
		{
			m_UnpluggedCB(this);
		}

		m_bUnplugged = true;
	}

	void RVHardwareDevice::CaptureFilter(s16 * audio, u16 frameStamp)
	{
		if (m_CaptureFilterCB.IsBound())
		{
			m_CaptureFilterCB(this, audio, frameStamp);
		}
	}

	void RVHardwareDevice::PlaybackFilter(s16 * audio, u16 frameStamp)
	{
		if (m_PlaybackFilterCB.IsBound())
		{
			m_PlaybackFilterCB(this, audio, frameStamp);
		}
	}

	bool RVHardwareDevice::HasUnpluggedCB()
	{
		if (m_UnpluggedCB.IsBound())
		{
			return true;
		}

		return false;
	}

	bool RVHardwareDevice::HasPlaybackFilter()
	{
		if (m_PlaybackFilterCB.IsBound())
		{
			return true;
		}

		return false;
	}

	bool RVHardwareDevice::HasCaptureFilter()
	{
		if (m_CaptureFilterCB.IsBound())
		{
			return true;
		}

		return false;
	}

	int RVHardwareDevice::GetNumChannels()
	{
		return 0;
	}

	void RVHardwareDevice::GetChannelName(int /*channel*/, wchar_t[RV_CHANNEL_NAME_LEN])
	{
		Assert(false);
	}

	void RVHardwareDevice::SetChannel(int /*channel*/)
	{
		Assert(false);
	}

	int RVHardwareDevice::GetChannel()
	{
		Assert(false);
		return 0;
	}

	double RVHardwareDevice::GetCurrentThreshold()
	{
		if (m_captureMode == RVCaptureModePushToTalk)
		{
			return m_Data.m_pushToTalkCaptureThreshold;
		}
		else
		{
			return m_Data.m_captureThreshold;
		}
	}

#if !__NO_OUTPUT
	const char * RVHardwareDevice::GetDeviceIdStr()
	{
		return m_DeviceIdStr;
	}
#endif

} // namespace rage

#endif // RSG_PC

