#if RSG_PC
// 
// voice/rvUtil.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef RV_UTIL_H
#define RV_UTIL_H

#include "rv.h"
#include "rvCodec.h"

namespace rage
{
	class RVUtil
	{
	public:
		// gets the volume for a set of samples
		static double GetSamplesVolume(const s16 * samplesPtr, int numSamples);

		// checks if any samples in the set are above the given threshold
		static bool IsOverThreshold(const s16 * samplesPtr, int numSamples, double threshold);

		// returns the lowest multiple of base that is >= value
		static int RoundUpToNearestMultiple(int value, int base);

		// returns the highest multiple of base that is <= value
		static int RoundDownToNearestMultiple(int value, int base);

		// rounds the multiple of base that is closest to value
		static int RoundToNearestMultiple(int value, int base);

		// multiply or divide by bytes per millisecond
		static int MultiplyByBytesPerMillisecond(int value);
		static int DivideByBytesPerMillisecond(int value);

	};
}

#endif // RV_UTIL_H
#endif // RSG_PC
