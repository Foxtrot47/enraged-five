#if RSG_PC
// 
// voice/rvUtil.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#include "rvUtil.h"
#include <math.h>
#include <limits.h>

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rvoice, util)
	#undef __rage_channel
	#define __rage_channel rvoice_util

	double RVUtil::GetSamplesVolume(const s16 * samplesPtr, int numSamples)
	{
		s16 value;
		s16 max = 0;
		int i;

		for(i = 0 ; i < numSamples ; i++)
		{
			value = samplesPtr[i];
			if(value < 0)
			{
				value = (s16)(-value);
			}
			if(value > max)
			{
				max = value;
			}
		}

		return ((double)max / -SHRT_MIN);
	}

	bool RVUtil::IsOverThreshold(const s16 * samplesPtr, int numSamples, double threshold)
	{
		int i;

		if(threshold == (double)0.0)
		{
			return true;
		}

		for(i = 0 ; i < numSamples ; i++)
		{
			if(labs(samplesPtr[i]) > (threshold * SHRT_MAX))
			{
				return true;
			}
		}

		return false;
	}

	int RVUtil::RoundUpToNearestMultiple(int value, int base)
	{
		int remainder;

		remainder = (value % base);
		if(remainder)
		{
			value += (base - remainder);
		}

		return value;
	}

	int RVUtil::RoundDownToNearestMultiple(int value, int base)
	{
		value -= (value % base);
		return value;
	}

	int RVUtil::RoundToNearestMultiple(int value, int base)
	{
		int remainder;

		remainder = (value % base);
		if(remainder < (base / 2))
		{
			value -= remainder;
		}
		else
		{
			value += (base - remainder);
		}

		return value;
	}

	int RVUtil::MultiplyByBytesPerMillisecond(int value)
	{
		return (int)(value * RVCodec::GetSampleRate() * RV_BYTES_PER_SAMPLE / 1000);
	}

	int RVUtil::DivideByBytesPerMillisecond(int value)
	{
		return (int)(value * 1000 / (RVCodec::GetSampleRate() * RV_BYTES_PER_SAMPLE));
	}

} // namespace rage

#endif // RSG_PC
