#if RSG_PC
// 
// voice/rv.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef RV_H
#define RV_H

#include "rvDebug.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <wchar.h>

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

//R* CHANGE: Different includes
#include "file/file_config.h"   //For online service defines
#include "system/xtl.h"         //Replaces several windows headers

// rage includes
#include "atl/delegate.h"
#include "atl/functor.h"

#include <winsock.h>
#include <ctype.h>
#include <assert.h>

namespace rage
{

//DEFINES
/////////
#define RV_BYTES_PER_SAMPLE          (sizeof(s16) / sizeof(u8))
#define RV_BITS_PER_SAMPLE           (RV_BYTES_PER_SAMPLE * 8)

#define RV_DEVICE_NAME_LEN   64
#define RV_CHANNEL_NAME_LEN  64

#define RV_CAPTURE   1
#define RV_PLAYBACK  2
#define RV_CAPTURE_AND_PLAYBACK  (RV_CAPTURE|RV_PLAYBACK)

#define RVRATE_8KHz   8000
#define RVRATE_11KHz 11025
#define RVRATE_16KHz 16000

//	Identifies each of the default codecs available. The codecs are arranged 
//	in order of descending quality and bandwidth. 
enum RVCodecQuality
{
	RVCodecRaw,
	RVCodecSuperHighQuality,
	RVCodecHighQuality,
	RVCodecAboveAverageQuality,
	RVCodecAverage,
	RVCodecLowBandwidth,
	RVCodecSuperLowBandwidth
};

// Hardware type of a device.
enum RVHardwareType
{
	RVHardwareDirectSound,   // Win32
};

// RVCaptureMode
enum RVCaptureMode
{
	RVCaptureModeThreshold,	// mode captures speech based on the current threshold value.
	RVCaptureModePushToTalk	// mode captures speech when SetPushToTalk is turned on.
};

// Typedefs
typedef void *                 RVDecoderData;
typedef int                    RVDeviceType;
typedef s64                    RVSource; // R* CHANGE for RockstarID
typedef GUID                   RVDeviceID;

// GLOBALS
extern const RVDeviceID RVDefaultCaptureDeviceID;
extern const RVDeviceID RVDefaultPlaybackDeviceID;

// Information for an audio device.
struct RVDeviceInfo
{
	GUID m_id;								// Used if you initialize this device with gvNewDevice.
	wchar_t m_name[RV_DEVICE_NAME_LEN];		// // A user-readable name for the device.
	RVDeviceType m_deviceType;				// Indicates if this device is for capture, playback, or both capture and playback.
	RVDeviceType m_defaultDevice;			// Indicates if this device is the capture device, playback device, both, or neither (0)
	RVHardwareType m_hardwareType;			// More information about the device's actual hardware. 
} ;

// Fwd Declares
class RVHardwareDevice;

// Device callbacks
typedef atDelegate<void (RVHardwareDevice*)> DeviceUnpluggedFunctor;
typedef atDelegate<void (RVHardwareDevice*, s16*, u16)> DeviceFilterFunctor;

class RVoice
{
public:
	//PURPOSE
	//	Initializes RageVoice
	//PARAMS
	//	hWnd - Window Handle
	static bool Init(HWND hWnd);

	//PURPOSE
	//	Shuts down RageVoice
	static void Shutdown();

	//PURPOSE
	//	Updates RageVoice
	static void Update();

	//PURPOSE
	//	Set the codec to be used by RageVoice
	//PARAMS
	//	code : the codec identifier
	static bool SetCodec(RVCodecQuality codec);

	//PURPOSE
	//	Obtain the particular stats for the codec
	//PARAMS
	//	samplesperFrame: the samples per frame (out)
	//	encodedFrameSize: the encoded frame size in bytes (out)
	//  bitsPerSecond:	the bits per second (out)
	static void GetCodecInfo(int * samplesPerFrame, int * encodedFrameSize, int * bitsPerSecond);

	//PURPOSE
	//	Set the codec sample rate
	//PARAMS
	//	sampleRate: the sample rate
	static void SetSampleRate(int sampleRate);

	//PURPOSE
	//	Get the codec sample rate
	static int GetSampleRate();

	//PURPOSE
	//	Get a list of devices available on the system
	//PARAMS
	//	devices: The list of device details to be filled by the function (out)
	//	maxDevices:	The number of elements in the devices array
	//	types:	The types of devices to survey
	static int ListDevices(RVDeviceInfo devices[], int maxDevices, RVDeviceType types);

	//PURPOSE
	//	Interacts with the user to set up the capture and playback devices
	//PARAMS
	//	captureDeviceId: The Id of the capture device to set up
	//	playbackDeviceId:	The id of the playback device to set up
	static bool RunSetupWizard(RVDeviceID captureDeviceID, RVDeviceID playbackDeviceID);

	//PURPOSE
	//	Determines if the registry has information on the specified device pair
	//PARAMS
	//	captureDeviceId: Reference to the device used to capture audio
	//	playbackDeviceId: Reference to the device used to playback audio
	static bool AreDevicesSetup(RVDeviceID captureDeviceID, RVDeviceID playbackDeviceID);

	//PURPOSE
	//	Initializes a device
	//PARAMS
	//	deviceId: The ID for the device to be initialized
	//	type:	Specifies whether the device handles capture, playback or both
	static RVHardwareDevice* NewDevice(RVDeviceID deviceID, RVDeviceType type);

	//PURPOSE
	//	Frees a device and cleans it up
	//PARAMS
	//	device:	The handle to the device to be freed
	static void FreeDevice(RVHardwareDevice* device);

	//PURPOSE
	//	Start a device capturing and/or playing audio
	// PARAMS
	//	device: the handle to the device
	//	type: Specifies capture, playback, or both
	static bool StartDevice(RVHardwareDevice* device, RVDeviceType type);

	//PURPOSE
	//	Stop a device from capturing and/or playing audio
	// PARAMS
	//	device: the handle to the device
	//	type: Specifies capture, playback, or both
	static void StopDevice(RVHardwareDevice* device, RVDeviceType type);

	//PURPOSE
	//	Checks to see if a device has been started as the given device type
	// PARAMS
	//	device: the handle to the device
	//	type: Specifies capture, playback, or both
	static bool IsDeviceStarted(RVHardwareDevice* device, RVDeviceType type);

	//PURPOSE
	//	Sets a device's volume
	// PARAMS
	//	device: the handle to the device
	//	type: Specifies capture, playback, or both
	//	volume: the volume ranging from 0.0 to 1.0
	static void SetDeviceVolume(RVHardwareDevice* device, RVDeviceType type, double volume);

	//PURPOSE
	//	Gets the volume from a capture or playback device
	// PARAMS
	//	device: the handle to the device
	//	type: Specifies capture, playback, or both
	static double GetDeviceVolume(RVHardwareDevice* device, RVDeviceType type);

	//PURPOSE
	// Takes captured audio data out of the internal capture buffer, storing it in the provided memory block
	// PARAMS
	//	device: the handle to the device
	//	packet: the block of memory to receive the data (out)
	//	len: The maximum number of bytes to be moved to the out buffer (ref)
	//	frameStamp: The frame stamp for the captured packet (out)
	//	volume: The peak volume for the capture packet (out)
	static bool CapturePacket(RVHardwareDevice* device, u8 * packet, int * len, u16 * frameStamp, double * volume);

	//PURPOSE
	//	Returns how many bytes are currently available for capture on the given device
	//PARAMS
	//	device: the handle to the device
	static int GetAvailableCaptureBytes(RVHardwareDevice* device);

	//PURPOSE
	//	Sets the capture mode for the device
	//PARAMS
	//	device: the handle to the device
	//	captureMode: the new capture mode
	static void SetCaptureMode(RVHardwareDevice* device, RVCaptureMode captureMode);

	//PURPOSE
	//	Sets the capture threshold for the device
	//PARAMS
	//	device: the handle to the device
	//	threshold: the new threshold to set
	static void SetCaptureThreshold(RVHardwareDevice* device, double threshold);

	//PURPOSE
	//	Sets the push to talk threshold for the device
	//PARAMS
	//	device: the handle to the device
	//	threshold: the new PTT threshold to set
	static void SetPushToTalkCaptureThreshold(RVHardwareDevice* device, double threshold);
	static double GetCaptureThreshold(RVHardwareDevice* device);
	
	//PURPOSE
	//	Sets the capture hold time for the device in MS. The capture hold time specifies how long
	//	to transmit data after the threshold has been reached. Default: 2000ms
	//PARAMS
	//	device: the handle to the device
	//	holdTimeMs: the capture hold time in milliseconds
	static void SetCaptureHoldTimeMs(RVHardwareDevice* device, u32 holdTimeMs);

	//PURPOSE
	//	Gets the capture hold time for the device in MS
	//PARAMS
	//	device: the handle to the device
	static double GetCaptureHoldTimeMs(RVHardwareDevice* device);

	//PURPOSE
	//	Used to turn on or off capturing the device. Must be in RVCaptureModePushToTalk mode.
	//PARAMS
	//	device: the handle to the device
	//	talkOn: true to start device capture, false to turn off capture
	static void SetPushToTalk(RVHardwareDevice* device, bool talkOn);

	//PURPOSE
	//	Returns true if PushToTalk is currently on, false if it is off
	//PARAMS
	//	device: the handle to the device
	static bool GetPushToTalk(RVHardwareDevice* device);

	//PURPOSE
	//	Plays a packet retrieved from the capture buffer
	//PARAMS
	//	device: the handle to the device
	//	packet: the packet with audio data
	//	len:	the packet's length
	//	source: the source that originated the audio
	//	frameStamp:	the packet's frame stamp
	//	mute:	Mutes the packet, allows having a player muted but still acknowledging their data
	static void PlayPacket(RVHardwareDevice* device, const u8 * packet, int len, RVSource source, u16 frameStamp, bool mute);

	//PURPOSE
	//	Determines if the particular source is talking on the specified device
	//PARAMS
	//	device: the handle to the device
	//	source: the source identifier
	static bool IsSourceTalking(RVHardwareDevice* device, RVSource source);

	//PURPOSE
	//	Gets a list of all sources currently talking on the device
	//PARAMS
	//	device: the handle to the device
	// sources: an array to receive the talking sources (out)
	//	maxSources: the number of elements in the sources array
	static int ListTalkingSources(RVHardwareDevice* device, RVSource sources[], int maxSources);

	//PURPOSE
	//	Sets the callback for when a device is unplugged
	//PARAMS
	//	device: the handle to the device
	//	unpluggedCallback: the functor to call when the device is unplugged
	static void SetUnpluggedCallback(RVHardwareDevice* device, DeviceUnpluggedFunctor unpluggedCallback);

	//PURPOSE
	//	Sets the filter for when a device is plugged
	//PARAMS
	//	device: the handle to the device
	//	type: set the filter to playback, capture, or both
	//	callback: the functor to call when filtering
	static void SetFilterCallback(RVHardwareDevice* device, RVDeviceType type, DeviceFilterFunctor callback);

	//PURPOSE
	//	Sets the global mute value - defaults to false
	//PARAMS
	//	mute: set to true to globally mute all play packets
	static void SetGlobalMute(bool mute);

	//PURPOSE
	//	Gets the global mute value
	static bool GetGlobalMute(void);

	//PURPOSE
	//	Get the number of channels set for the device
	//PARAMS
	//	device: the handle to the device
	static int GetNumChannels(RVHardwareDevice* device);

	//PURPOSE
	//	Get a channel name by index
	//PARAMS
	//	device: the handle to the device
	//	channel: the channel to retrieve
	//	name: the name of the given channel index (out)
	static void GetChannelName(RVHardwareDevice* device, int channel, wchar_t name[RV_CHANNEL_NAME_LEN]);

	//PURPOSE
	//	Set a channel name by index
	//PARAMS
	//	device: the handle to the device
	//	channel: the channel to set
	static void SetChannel(RVHardwareDevice* device, int channel);

	//PURPOSE
	//	Gets the default channel of the device
	//PARAMS
	//	device: The device chanenl
	static int GetChannel(RVHardwareDevice* device);

private:

	static const float MIN_THRESHOLD;
};

} // namespace rage

#endif // RV_H
#endif // RSG_PC
