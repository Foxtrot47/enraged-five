#if RSG_PC
// 
// voice/rvSource.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef RV_SOURCE_H
#define RV_SOURCE_H

#include "rv.h"
#include "rvFrame.h"

namespace rage
{

	extern bool RVIGlobalMute;

	struct RVISource
	{
		bool			m_inUse;
		RVSource        m_source;
		bool			m_isTalking;
		u16				m_finishedTalkingTime;
		u16				m_clockOffset;
		float           m_clockOffsetAverage;
		RVDecoderData   m_decoderData;
		RVIPendingFrame m_frameHead;

	};

	class RVSourceMgr
	{
	public:
		static void Init();

		static void AddPacketToSource(RVISource* source, u16 frameStamp, const u8 * packet, int len, bool mute);
		static void AddPacketToSourceList(RVISource* sourceList, const u8 * packet, int len, RVSource source, u16 frameStamp, bool mute, u16 currentPlayClock);

		static RVISource* AddSourceToList(RVISource* sourceList, RVSource source);
		static RVISource* FindSourceInList(RVISource* sourceList, RVSource source);
	
		static void FreeSource(RVISource * source);

		static bool IsSourceTalking(RVISource* sourceList, RVSource source);
		static int ListTalkingSources(RVISource* sourceList, RVSource sources[], int maxSources);

		static void SetGlobalMute(bool mute);
		static bool GetGlobalMute();

		static bool WriteSourcesToBuffer(RVISource* sourceList, u16 startTime, s16 * sampleBuffer, int numFrames);


		static const int MAX_SOURCES = 8;
	};

}

#endif // RV_SOURCE_H

#endif // RSG_PC