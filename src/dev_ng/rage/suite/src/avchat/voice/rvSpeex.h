#if RSG_PC
// 
// voice/rvSpeex.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef RV_SPEEX_H
#define RV_SPEEX_H

#include "rv.h"
#include "speex/speex.h"

	/*
	8000kHz
	quality: samplesPerFrame encodedFrameSize bitsPerSecond
	0:             160               6             2150
	1:             160              10             3950
	2:             160              15             5950
	3:             160              20             8000
	4:             160              20             8000
	5:             160              28            11000
	6:             160              28            11000
	7:             160              38            15000
	8:             160              38            15000
	9:             160              46            18200
	10:            160              62            24600

	16000kHz
	quality: samplesPerFrame encodedFrameSize bitsPerSecond
	0:             320              10             3950
	1:             320              15             5750
	2:             320              20             7750
	3:             320              25             9800
	4:             320              32            12800
	5:             320              42            16800
	6:             320              52            20600
	7:             320              60            23800
	8:             320              70            27800
	9:             320              86            34200
	10:            320             106            42200
	*/

namespace rage
{
	class RVSpeex
	{
	public:
		static bool SpeexInitialize(int quality, int sampleRate);
		static void SpeexCleanup(void);

		static int SpeexGetSamplesPerFrame(void);
		static int SpeexGetEncodedFrameSize(void);

		static bool SpeexNewDecoder(RVDecoderData * data);
		static void SpeexFreeDecoder(RVDecoderData data);

		static void SpeexEncode(u8 * out, const s16 * in);
		static void SpeexDecodeAdd(s16 * out, const u8 * in, RVDecoderData data);
		static void SpeexDecodeSet(s16 * out, const u8 * in, RVDecoderData data);

		static void SpeexResetEncoder(void);

	private:
		static bool RVSpeex::RVISpeexInitialized;
		static void * RVSpeex::RVISpeexEncoderState;
		static SpeexBits RVSpeex::RVISpeexBits;
		static int RVSpeex::RVISpeexEncodedFrameSize;
		static int RVSpeex::RVISpeexSamplesPerFrame;
	};
}

#endif // RV_SPEEX_H

#endif // RSG_PC