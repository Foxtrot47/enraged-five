#if RSG_PC
// 
// voice/rvcodec.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#include "rvCodec.h"
#include "rvFrame.h"
#include "rvSpeex.h"

#include "system/memops.h"

namespace rage
{

	RAGE_DEFINE_SUBCHANNEL(rvoice, codec)
	#undef __rage_channel
	#define __rage_channel rvoice_codec

	#define RVI_RAW_BASE_SAMPLES_PER_FRAME 160

	RVCustomCodecInfo RVCodec::RVICodecInfo;
	bool RVCodec::RVICleanupInternalCodec;
	bool RVCodec::RVIRawCodec;

	int RVCodec::m_SamplesPerFrame;
	int RVCodec::m_BytesPerFrame;
	int RVCodec::m_EncodedFrameSize;
	int RVCodec::m_SampleRate;
	int RVCodec::m_BytesPerSecond;

	void RVCodec::CleanupCodec()
	{
		RVSpeex::SpeexCleanup();
	}

	void RVCodec::Init()
	{
		//Set a default sample rate.
		SetSampleRate(RVRATE_8KHz);
		RVICleanupInternalCodec = false;
	}

	void RVCodec::Shutdown()
	{
		if(RVICleanupInternalCodec)
		{
			CleanupCodec();
			RVICleanupInternalCodec = false;
		}
	}

	bool RVCodec::SetInternalCodec(RVCodecQuality codec)
	{
		RVCustomCodecInfo info;
		int quality;

		// figure out the quality
		// goto rvSpeex.h to see what the quality values mean
		if(codec == RVCodecSuperHighQuality)
		{
			quality = 10;
		}
		else if(codec == RVCodecHighQuality)
		{
			quality = 7;
		}
		else if(codec == RVCodecAboveAverageQuality)
		{
			quality = 5;
		}
		else if(codec == RVCodecAverage)
		{
			quality = 4;
		}
		else if(codec == RVCodecLowBandwidth)
		{
			quality = 2;
		}
		else if(codec == RVCodecSuperLowBandwidth)
		{
			quality = 1;
		}
		else
		{
			return false;
		}

		rvDebug3("Set codec quality to %d", (int)codec);

		// init speex
		if(!RVSpeex::SpeexInitialize(quality, m_SampleRate))
		{
			rvError("RVSpeex failed initialization");
			return false;
		}

		// setup the info
		info.m_samplesPerFrame = RVSpeex::SpeexGetSamplesPerFrame();
		info.m_encodedFrameSize = RVSpeex::SpeexGetEncodedFrameSize();
		info.NewDecoderCB = MakeFunctorRet(RVSpeex::SpeexNewDecoder);
		info.FreeDecoderCB = MakeFunctor(RVSpeex::SpeexFreeDecoder);
		info.EncoderCB = MakeFunctor(RVSpeex::SpeexEncode);
		info.DecodeAddCB = MakeFunctor(RVSpeex::SpeexDecodeAdd);
		info.DecodeSetCB = MakeFunctor(RVSpeex::SpeexDecodeSet);

		// set it
		SetCustomCodec(&info);

		return true;
	}

	void RVCodec::RawEncode(u8 * out, const s16 * in)
	{
		s16 * sampleOut = (s16 *)out;

		for(int i = 0 ; i < m_SamplesPerFrame; i++)
		{
			sampleOut[i] = htons(in[i]);
		}
	}

	void RVCodec::RawDecodeAdd(s16 * out, const u8 * in, RVDecoderData /*data*/)
	{
		const s16 * sampleIn = (const s16 *)in;
		int i;

		for(i = 0 ; i < m_SamplesPerFrame; i++)
		{
			// Expanded to remove warnings in VS2K5
			out[i] = out[i] + ntohs(sampleIn[i]);
		}
	}

	void RVCodec::RawDecodeSet(s16 * out, const u8 * in, RVDecoderData /*data*/)
	{
		const s16 * sampleIn = (const s16 *)in;
		int i;

		for(i = 0 ; i < m_SamplesPerFrame; i++)
		{
			out[i] = ntohs(sampleIn[i]);
		}
	}

	void RVCodec::SetRawCodec(void)
	{
		RVCustomCodecInfo info;
		sysMemSet(&info, 0, sizeof(info));
		// setup the info
		if (m_SampleRate == RVRATE_8KHz)
		{
			info.m_samplesPerFrame = RVI_RAW_BASE_SAMPLES_PER_FRAME;
		}
		// need to double the frame size since data has shorter wavelength
		else if (m_SampleRate == RVRATE_16KHz)
		{
			info.m_samplesPerFrame = RVI_RAW_BASE_SAMPLES_PER_FRAME * 2;
		}

		// the samples per frame should be set above 
		info.m_encodedFrameSize = (info.m_samplesPerFrame * RV_BYTES_PER_SAMPLE);
		info.NewDecoderCB = NULL;
		info.FreeDecoderCB = NULL;
		info.EncoderCB = MakeFunctor(&RawEncode);
		info.DecodeAddCB = MakeFunctor(RawDecodeAdd);
		info.DecodeSetCB = MakeFunctor(RawDecodeSet);

		// set it
		SetCustomCodec(&info);
	}

	bool RVCodec::SetCodec(RVCodecQuality codec)
	{
	#if !defined(GV_NO_DEFAULT_CODEC)
		// cleanup if we need to
		if(RVICleanupInternalCodec)
		{
			CleanupCodec();
			RVICleanupInternalCodec = false;
		}
	#endif

		// raw codec is handled specially
		if(codec == RVCodecRaw)
		{
			SetRawCodec();
			RVIRawCodec = true;
			return true;
		}
		else
		{
			RVIRawCodec = false;
		}

		if(!SetInternalCodec(codec))
		{
			return false;
		}

		// clean this up at some point
		RVICleanupInternalCodec = true;

		return true;
	}

	void RVCodec::SetCustomCodec(RVCustomCodecInfo * info)
	{
		// store the info
		sysMemCpy(&RVICodecInfo, info, sizeof(RVCustomCodecInfo));
		m_SamplesPerFrame = info->m_samplesPerFrame;
		m_EncodedFrameSize = info->m_encodedFrameSize;

		// extra info
		m_BytesPerFrame = (m_SamplesPerFrame * (int)RV_BYTES_PER_SAMPLE);

		// frames needs to be initialized with codec info
		RVFrameMgr::Init();
	}

	void RVCodec::SetSampleRate(int sampleRate)
	{
		//Save the sample rate.
		m_SampleRate     = sampleRate;
		m_BytesPerSecond = m_SampleRate * RV_BYTES_PER_SAMPLE;
	}

	bool RVCodec::NewDecoder(RVDecoderData * data)
	{
		if(!RVICodecInfo.NewDecoderCB)
		{
			*data = NULL;
			return true;
		}

		return RVICodecInfo.NewDecoderCB(data);
	}

	void RVCodec::FreeDecoder(RVDecoderData data)
	{
		if(RVICodecInfo.FreeDecoderCB)
		{
			RVICodecInfo.FreeDecoderCB(data);
		}
	}

	void RVCodec::Encode(u8 * out, const s16 * in)
	{
		RVICodecInfo.EncoderCB(out, in);
	}

	void RVCodec::DecodeAdd(s16 * out, const u8 * in, RVDecoderData data)
	{
		RVICodecInfo.DecodeAddCB(out, in, data);
	}

	void RVCodec::DecodeSet(s16 * out, const u8 * in, RVDecoderData data)
	{
		if(RVICodecInfo.DecodeSetCB)
		{
			RVICodecInfo.DecodeSetCB(out, in, data);
		}
		else
		{
			sysMemSet(out, 0, m_BytesPerFrame);
			RVICodecInfo.DecodeAddCB(out, in, data);
		}
	}

	void RVCodec::ResetEncoder(void)
	{
		//If we are using the RawCodec, we have never set up Speex.
		if (!RVIRawCodec)
		{
			RVSpeex::SpeexResetEncoder();
		}
	}

} // namespace rage

#endif // RSG_PC