#if RSG_PC
// 
// voice/rvSpeex.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//


#include "rvSpeex.h"
#include "rvCodec.h"
#include "rvMemory.h"
#include "system/new.h"

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rvoice, speex)
	#undef __rage_channel
	#define __rage_channel rvoice_speex

	bool RVSpeex::RVISpeexInitialized = false;
	void * RVSpeex::RVISpeexEncoderState;
	SpeexBits RVSpeex::RVISpeexBits;
	int RVSpeex::RVISpeexEncodedFrameSize;
	int RVSpeex::RVISpeexSamplesPerFrame;

	//Encode/Decode buffer.
	static float * RVISpeexBuffer;

	bool RVSpeex::SpeexInitialize(int quality, int sampleRate)
	{
		int rate;
		int bitsPerFrame;
		int samplesPerSecond;
	
		// we shouldn't already be initialized
		if(RVISpeexInitialized)
		{
			return false;
		}

		// create a new encoder state
		if (sampleRate == RVRATE_8KHz)
		{
			RVISpeexEncoderState = speex_encoder_init(&speex_nb_mode);
		}
		else if (sampleRate == RVRATE_16KHz)
		{
			RVISpeexEncoderState = speex_encoder_init(&speex_wb_mode);
		}
		else
		{
			return false;
		}

		if(!RVISpeexEncoderState)
		{
			return false;
		}

		// set the sampling rate
		samplesPerSecond = sampleRate;
		speex_encoder_ctl(RVISpeexEncoderState, SPEEX_SET_SAMPLING_RATE, &samplesPerSecond);

		// Get the samples per frame setting.
		speex_encoder_ctl(RVISpeexEncoderState, SPEEX_GET_FRAME_SIZE, &RVISpeexSamplesPerFrame);

		// set the quality
		speex_encoder_ctl(RVISpeexEncoderState, SPEEX_SET_QUALITY, &quality);

		// initialize the bits struct
		speex_bits_init(&RVISpeexBits);

		// get the bitrate
		speex_encoder_ctl(RVISpeexEncoderState, SPEEX_GET_BITRATE, &rate);

		// convert to bits per frame
		bitsPerFrame = (rate / (sampleRate / RVISpeexSamplesPerFrame));

		// convert to bytes per frame and store, round up to allocate more space than needed.
		RVISpeexEncodedFrameSize = (bitsPerFrame / 8);
		if (bitsPerFrame % 8)
		{
			RVISpeexEncodedFrameSize++;
		}

		// create our encoding and decoding buffer.
		RVISpeexBuffer = (float *)RVMemory::Allocate(RVISpeexSamplesPerFrame * sizeof(float));
		sysMemSet(RVISpeexBuffer, 0, RVISpeexSamplesPerFrame * sizeof(float));

		// we're now initialized
		RVISpeexInitialized = true;

		return true;
	}

	void RVSpeex::SpeexCleanup(void)
	{
		// make sure there is something to cleanup
		if(!RVISpeexInitialized)
		{
			return;
		}

		// free up encoding and decoding buffer.
		RVMemory::Free(RVISpeexBuffer);

		// destroy the encoder state
		speex_encoder_destroy(RVISpeexEncoderState);
		RVISpeexEncoderState = NULL;

		// destroy the bits struct
		speex_bits_destroy(&RVISpeexBits);

		// no longer initialized
		RVISpeexInitialized = false;
	}

	int RVSpeex::SpeexGetSamplesPerFrame(void)
	{
		return RVISpeexSamplesPerFrame;
	}

	int RVSpeex::SpeexGetEncodedFrameSize(void)
	{
		return RVISpeexEncodedFrameSize;
	}

	bool RVSpeex::SpeexNewDecoder(RVDecoderData * data)
	{
		void * decoder;
		int perceptualEnhancement = 1;
	
		// create a new decoder state
		if (RVCodec::GetSampleRate() == RVRATE_8KHz)
		{
			decoder = speex_decoder_init(&speex_nb_mode);
		}
		else if (RVCodec::GetSampleRate() == RVRATE_16KHz)
		{
			decoder = speex_decoder_init(&speex_wb_mode);
		}
		else
		{
			return false;
		}

		if(!decoder)
		{
			return false;
		}

		// turn on the perceptual enhancement
		speex_decoder_ctl(decoder, SPEEX_SET_ENH, &perceptualEnhancement);

		*data = decoder;
		return true;
	}

	void RVSpeex::SpeexFreeDecoder(RVDecoderData data)
	{
		// destory the decoder state
		speex_decoder_destroy((void *)data);
	}

	void RVSpeex::SpeexEncode(u8 * out, const s16 * in)
	{
		int bytesWritten;
		int i;

		// convert the input to floats for encoding
		for(i = 0 ; i < RVISpeexSamplesPerFrame ; i++)
		{
			RVISpeexBuffer[i] = in[i];
		}

		// flush the bits
		speex_bits_reset(&RVISpeexBits);

		// encode the frame
		speex_encode(RVISpeexEncoderState, RVISpeexBuffer, &RVISpeexBits);

		// write the bits to the output
		bytesWritten = speex_bits_write(&RVISpeexBits, (char *)out, RVISpeexEncodedFrameSize);
		Assert(bytesWritten == RVISpeexEncodedFrameSize);
	}

	void RVSpeex::SpeexDecodeAdd(s16 * out, const u8 * in, RVDecoderData data)
	{
		int rcode;
		int i;

		// read the data into the bits
		speex_bits_read_from(&RVISpeexBits, (char *)in, RVISpeexEncodedFrameSize);

		// decode it
		rcode = speex_decode((void *)data, &RVISpeexBits, RVISpeexBuffer);
		Assert(rcode == 0);

		// convert the output from floats
		for(i = 0 ; i < RVISpeexSamplesPerFrame ; i++)
		{
			// Expanded to remove warnings in VS2K5
			out[i] = out[i] + (s16)RVISpeexBuffer[i];
		}
	}

	void RVSpeex::SpeexDecodeSet(s16 * out, const u8 * in, RVDecoderData data)
	{
		int rcode;
		int i;

		// read the data into the bits
		speex_bits_read_from(&RVISpeexBits, (char *)in, RVISpeexEncodedFrameSize);

		// decode it
		rcode = speex_decode((void *)data, &RVISpeexBits, RVISpeexBuffer);
		Assert(rcode == 0);

		// convert the output from floats
		for(i = 0 ; i < RVISpeexSamplesPerFrame ; i++)
		{
			out[i] = (s16)RVISpeexBuffer[i];
		}
	}

	void RVSpeex::SpeexResetEncoder(void)
	{
		// reset the encoder's state
		speex_encoder_ctl(RVISpeexEncoderState, SPEEX_RESET_STATE, NULL);
	}

} // namespace rage

#endif // RSG_PC