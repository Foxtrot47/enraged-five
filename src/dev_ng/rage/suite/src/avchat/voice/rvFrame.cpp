#if RSG_PC
// 
// voice/rvFrame.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#include "rvFrame.h"
#include "rvCodec.h"
#include "rvMemory.h"

#include "system/new.h"


namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rvoice, frame)
	#undef __rage_channel
	#define __rage_channel rvoice_frame

	// packets are only accepted if they are frameStamped to be played
	// within this many frames from the current play clock
	#define RVI_INCOMING_PACKET_TIMEFRAME_FRAMES      (RVI_FRAMESTAMP_MAX / 2)
	#define RVI_PREALLOCATED_FRAMES                   200
	#define RVI_DYNAMICALLY_ALLOCATE_FRAMES           1

	// list of available frames
	static RVIPendingFrame * RVIAvailableFrames;

	bool RVFrameMgr::IsFrameStampGT(u16 a, u16 b)
	{
		return ((u16)(b - a) > RVI_INCOMING_PACKET_TIMEFRAME_FRAMES);
	}

	bool RVFrameMgr::IsFrameStampGTE(u16 a, u16 b)
	{
		return ((u16)(b - a - 1) > RVI_INCOMING_PACKET_TIMEFRAME_FRAMES);
	}

	void RVFrameMgr::FreePendingFrame(RVIPendingFrame * frame)
	{
		RVMemory::Free(frame);
	}

	RVIPendingFrame* RVFrameMgr::NewPendingFrame()
	{
		RVIPendingFrame * frame;

		// allocate a new frame
		frame =  (RVIPendingFrame*)RVMemory::Allocate(sizeof(RVIPendingFrame) + RVCodec::GetEncodedFrameSize() - 1);

		// return it
		return frame;
	}

	void RVFrameMgr::PutPendingFrame(RVIPendingFrame * frame)
	{
		// put the frame back in the available frames list
		frame->m_next = RVIAvailableFrames;
		RVIAvailableFrames = frame;
	}

	RVIPendingFrame* RVFrameMgr::GetPendingFrame(void)
	{
		RVIPendingFrame * frame;

		// check the available frames list
		if(RVIAvailableFrames)
		{
			frame = RVIAvailableFrames;
			RVIAvailableFrames = frame->m_next;
			return frame;
		}

	#if RVI_DYNAMICALLY_ALLOCATE_FRAMES
		// allocate a new frame
		return NewPendingFrame();
	#else
		// we can't dynamically allocate frames
		return NULL;
	#endif
	}

	void RVFrameMgr::Init(void)
	{
		RVIPendingFrame * frame;
		int i;

		if(RVIAvailableFrames)
		{
			Shutdown();
		}

		RVIAvailableFrames = NULL;

		for(i = 0 ; i < RVI_PREALLOCATED_FRAMES ; i++)
		{
			frame = NewPendingFrame();
			if(!frame)
			{
				return;
			}
			frame->m_next = RVIAvailableFrames;
			RVIAvailableFrames = frame;
		}
	}

	void RVFrameMgr::Shutdown(void)
	{
		RVIPendingFrame * next;

		while(RVIAvailableFrames)
		{
			next = RVIAvailableFrames->m_next;
			FreePendingFrame(RVIAvailableFrames);
			RVIAvailableFrames = next;
		}
	}

} // namespace rage

#endif // RSG_PC