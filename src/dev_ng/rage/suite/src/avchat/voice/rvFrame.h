#if RSG_PC
// 
// voice/rvFrame.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef RV_FRAME_H
#define RV_FRAME_H

#include "rv.h"

namespace rage
{
	// max value for a framestamp
	#define RVI_FRAMESTAMP_MAX   0xFFFF
	#define RVI_PRE_DECODE 0

	// when allocated, enough memory is allocated to fit an entire frame into the m_frame array
	typedef struct RVIPendingFrame
	{
		u16 m_frameStamp;
		struct RVIPendingFrame * m_next;

		// m_frame must be the last member of this struct
		u8 m_frame[1];
	} RVIPendingFrame;

	class RVFrameMgr
	{
	public:
		static void Init(void);
		static void Shutdown(void);

		// Pending Frames
		static RVIPendingFrame* NewPendingFrame();
		static RVIPendingFrame * GetPendingFrame(void);
		static void PutPendingFrame(RVIPendingFrame * frame);
		static void FreePendingFrame(RVIPendingFrame * frame);

		// Comparisons
		static bool IsFrameStampGT(u16 a, u16 b);
		static bool IsFrameStampGTE(u16 a, u16 b);
	};

} // namespace rage

#endif // RV_FRAME_H

#endif // RSG_PC