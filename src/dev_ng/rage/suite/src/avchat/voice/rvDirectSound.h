#if RSG_PC
// 
// voice/rvDirectSound.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef RV_DIRECT_SOUND_H
#define RV_DIRECT_SOUND_H

#include "rv.h"

// rage headers
#include "atl/array.h"
#include "system/criticalsection.h"

namespace rage 
{
	// fwd decl
	struct RVIHardwareData; 

	class RVDirectSound
	{
	public:

		static bool Init(HWND hWnd);
		static void Shutdown();
		
		static void Update();
		static bool PlaybackDeviceUpdate(RVHardwareDevice * device);
		static void FreeDevice(RVHardwareDevice* device);

		static RVHardwareDevice* HardwareNewDevice(GUID deviceID, RVDeviceType type);
		static int HardwareListDevices(RVDeviceInfo devices[], int maxDevices, RVDeviceType types);
		static void AppendDeviceToList(RVHardwareDevice* device);
		static void DeleteDeviceFromList(RVHardwareDevice* device, int indexHelper = -1);

		static bool StartPlaybackDevice(RVIHardwareData * data);
		static void StopPlaybackDevice(RVIHardwareData * data);

		static bool StartCaptureDevice(RVIHardwareData * data);
		static void StopCaptureDevice(RVIHardwareData * data);

		static bool IsPlaybackDeviceStarted(RVIHardwareData * data);
		static bool IsCaptureDeviceStarted(RVIHardwareData * data);

		static void SetPlaybackDeviceVolume(RVIHardwareData * data, double volume);
		static double GetPlaybackDeviceVolume(RVIHardwareData * data);

		static void SetCaptureDeviceVolume(RVIHardwareData * data, double volume);
		static double GetCaptureDeviceVolume(RVIHardwareData * data);		

		static bool InitPlaybackDevice(RVIHardwareData * data);
		static bool InitCaptureDevice(RVIHardwareData * data);

	private:
		static HWND m_Hwnd;
		static atArray<RVHardwareDevice*> m_RVHardwareDevices;
		static bool m_bRVICleanupCOM;

		static sysCriticalSectionToken m_Cs;
	};

} // namespace rage

#endif // RV_DIRECT_SOUND_H

#endif // RSG_PC