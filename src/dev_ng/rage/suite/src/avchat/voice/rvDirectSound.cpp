#if RSG_PC
// 
// voice/rvDirectSound.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef WINXP
#define WINXP NTDDI_WINXP
#endif

#ifndef NTDDI_WINLH
#define NTDDI_WINLH NTDDI_LONGHORN
#endif

#ifndef NTDDI_WINS03
#define NTDDI_WINS03 NTDDI_WS03
#endif

#ifndef NTDDI_WIN2K3
#define NTDDI_WIN2K3 NTDDI_WINXP
#endif

#ifndef NTDDI_WXP
#define NTDDI_WXP NTDDI_WINXP
#endif

#ifndef NTDDI_XPSP2
#define NTDDI_XPSP2 NTDDI_WINXPSP2
#endif

#ifndef _WIN32_IE
#define _WIN32_IE 0x0500
#endif

#ifndef _NT_TARGET_VERSION_WIN7
#define _NT_TARGET_VERSION_WIN7 0x501
#endif

#define DIRECTSOUND_VERSION 0x0900

#include "rvDirectSound.h"

#include "rvCodec.h"
#include "rvHardwareDevice.h"
#include "rvMemory.h"
#include "rvSource.h"
#include "rvUtil.h"

#include <stdio.h>
#include <math.h>

#pragma warning(disable:4201)
#include <audiodefs.h>
#include <mmsystem.h>
#include <dsound.h>
#if (DIRECTSOUND_VERSION == 0x0800)
#include <dvoice.h>
#endif

#pragma comment(lib,"dsound.lib")

// rage includes
#include "system/timer.h"

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rvoice, directsound)
	#undef __rage_channel
	#define __rage_channel rvoice_directsound

	static WAVEFORMATEX m_RVIWaveFormat;
	HWND RVDirectSound::m_Hwnd;
	atArray<RVHardwareDevice*> RVDirectSound::m_RVHardwareDevices;
	bool RVDirectSound::m_bRVICleanupCOM;
	sysCriticalSectionToken RVDirectSound::m_Cs;

	/************
	** DEFINES **
	************/
	// GUID utility macros
	#define RVI_GUID_COPY(dest, src)          sysMemCpy((dest), (src), sizeof(GUID))

	// these were copied from dsound.h
	const GUID RVDefaultPlaybackDeviceID = {0xdef00002, 0x9c6d, 0x47ed, 0xaa, 0xf1, 0x4d, 0xda, 0x8f, 0x2b, 0x5c, 0x03};
	const GUID RVDefaultCaptureDeviceID  = {0xdef00003, 0x9c6d, 0x47ed, 0xaa, 0xf1, 0x4d, 0xda, 0x8f, 0x2b, 0x5c, 0x03};

	/**********
	** TYPES **
	**********/


	typedef struct
	{
		RVDeviceInfo * m_devices;
		int m_len;
		int m_num;
		bool m_enumeratingCapture;
	} RVIEnumDevicesInfo;

	/**************
	** FUNCTIONS **
	**************/
	bool RVDirectSound::Init(HWND hWnd)
	{
		HRESULT result;

		// check the hwnd
		if(!hWnd)
		{
			// Use the desktop window as a failsafe. Do not use the foreground window
			//  because it could belong to another application that exits while we still need
			//  access to it.
			hWnd = GetDesktopWindow();
		}

		// store it
		m_Hwnd = hWnd;
		rvAssertf(m_Hwnd != NULL, "RVDirectSound::Init() - NULL window handle.");

		// create the devices array
		m_RVHardwareDevices.clear();

		// init COM
		Assert(m_bRVICleanupCOM == false);
		result = CoInitialize(NULL);
		if(SUCCEEDED(result))
		{
			m_bRVICleanupCOM = true;
		}

		return true;
	}

	void RVDirectSound::Shutdown()
	{
		// cleanup the devices
		m_RVHardwareDevices.clear();

		// cleanup COM
		if(m_bRVICleanupCOM == true)
		{
			CoUninitialize();
			m_bRVICleanupCOM = false;
		}
	}

	bool RVDirectSound::PlaybackDeviceUpdate(RVHardwareDevice * device)
	{
		return device->PlaybackUpdate();
	}

	void RVDirectSound::Update(void)
	{
		SYS_CS_SYNC(m_Cs);

		RVHardwareDevice * device;
		bool rcode;
		int num;

		// loop through backwards so that we can remove devices as we go
		num = m_RVHardwareDevices.GetCount();
		for(int i = (num - 1) ; i >= 0 ; i--)
		{
			// get the device
			device = m_RVHardwareDevices[i];

			// check if this is a playback device
			if(device->GetDeviceTypes() == RV_PLAYBACK)
			{
				// let it think
				rcode = PlaybackDeviceUpdate(device);

				// check if the device was unplugged
				if(!rcode)
				{
					device->Unplugged();
					DeleteDeviceFromList(device, i);
				}
			}
		}
	}

	static BOOL CALLBACK RVIEnumDevicesCallback(LPGUID lpGuid, LPCWSTR desc, LPCWSTR /*module*/, LPVOID lpContext)
	{
		if (lpGuid)
		{
			RVIEnumDevicesInfo * info = (RVIEnumDevicesInfo *)lpContext;
			RVDeviceInfo * device = NULL;
			int i;

			// first check if it is already in the list
			for(i = 0 ; i < info->m_num ; i++)
			{
				if(IsEqualGUID(info->m_devices[i].m_id, *lpGuid))
				{
					device = &info->m_devices[i];
				}
			}

			if(!device)
			{
				// if not, add it
				device = &info->m_devices[info->m_num++];

				// clear it
				sysMemSet(device, 0, sizeof(RVDeviceInfo));

				// store the ID and name
				RVI_GUID_COPY(&device->m_id, lpGuid);
				safecpy(device->m_name, desc, RV_DEVICE_NAME_LEN);
				device->m_name[RV_DEVICE_NAME_LEN - 1] = '\0';
			}

			// store if it is a cap or playback device
			if(info->m_enumeratingCapture)
			{
				device->m_deviceType = RV_CAPTURE;
			}
			else
			{
				device->m_deviceType = RV_PLAYBACK;
			}

			// directsound device
			device->m_hardwareType = RVHardwareDirectSound;

			// check for full list
			if(info->m_num == info->m_len)
			{
				return false;
			}
		}

		return true;
	}

	int RVDirectSound::HardwareListDevices(RVDeviceInfo devices[], int maxDevices, RVDeviceType types)
	{
	#if DIRECTSOUND_VERSION >= 0x0800
		GUID defaultCaptureDevice;
		GUID defaultPlaybackDevice;
	#endif
		RVIEnumDevicesInfo info;
		int i;

		// make sure there is space for at least one device
		if(maxDevices < 1)
			return 0;

		// enumerate capture and playback devices
		info.m_devices = devices;
		info.m_len = maxDevices;
		info.m_num = 0;
		if(types & RV_CAPTURE)
		{
			info.m_enumeratingCapture = true;
			DirectSoundCaptureEnumerateW(RVIEnumDevicesCallback, &info);
		}
		if(types & RV_PLAYBACK)
		{
			info.m_enumeratingCapture = false;
			DirectSoundEnumerateW(RVIEnumDevicesCallback, &info);
		}

	#if DIRECTSOUND_VERSION >= 0x0800
		// check for the default capture and playback devices
		GetDeviceID(&DSDEVID_DefaultVoiceCapture, &defaultCaptureDevice);
		GetDeviceID(&DSDEVID_DefaultVoicePlayback, &defaultPlaybackDevice);
		for(i = 0 ; i < info.m_num ; i++)
		{
			if(IsEqualGUID((GUID)(devices[i].m_id), defaultCaptureDevice))
			{
				devices[i].m_defaultDevice = RV_CAPTURE;
			}
			else if(IsEqualGUID((GUID)(devices[i].m_id), defaultPlaybackDevice))
			{
				devices[i].m_defaultDevice = RV_PLAYBACK;
			}
		}
	#else
		for(i = 0 ; i < info.m_num ; i++)
		{
			devices[i].m_defaultDevice = (RVDeviceType)0;
		}
	#endif

		return info.m_num;
	}

	bool RVDirectSound::StartPlaybackDevice(RVIHardwareData * data)
	{
		HRESULT result;
		LPVOID audioPtr1, audioPtr2;
		DWORD audioLen1, audioLen2;

		// make sure the device is stopped
		IDirectSoundBuffer_Stop(data->m_playbackBuffer);

		// lock the buffer
		result = IDirectSoundBuffer_Lock(data->m_playbackBuffer, 0, 0, &audioPtr1, &audioLen1, &audioPtr2, &audioLen2, DSBLOCK_ENTIREBUFFER);
		if(FAILED(result))
		{
			rvError("RVDirectSound::StartPlaybackDevice() - Failed IDirectSoundBuffer_Lock with HRESULT 0x%08X", result);
			return false;
		}

		// clear it
		sysMemSet(audioPtr1, 0, audioLen1);

		// unlock the buffer
		result = IDirectSoundBuffer_Unlock(data->m_playbackBuffer, audioPtr1, audioLen1, audioPtr2, audioLen2);
		if(FAILED(result))
		{
			rvError("RVDirectSound::StartPlaybackDevice() - Failed IDirectSoundBuffer_Unlock with HRESULT 0x%08X", result);
			return false;
		}

		// reset the position
		result = IDirectSoundBuffer_SetCurrentPosition(data->m_playbackBuffer, 0);
		if(FAILED(result))
		{
			rvError("RVDirectSound::StartPlaybackDevice() - Failed IDirectSoundBuffer_SetCurrentPosition with HRESULT 0x%08X", result);
			return false;
		}

		// start the buffer
		result = IDirectSoundBuffer_Play(data->m_playbackBuffer, 0, 0, DSBPLAY_LOOPING);
		if(FAILED(result))
		{
			rvError("RVDirectSound::StartPlaybackDevice() - Failed IDirectSoundBuffer_Play with HRESULT 0x%08X", result);
			return false;
		}

		// clear the playback clocks
		data->m_playbackClock = 0;

		// init the think time
		data->m_playbackLastThink = sysTimer::GetSystemMsTime();

		// started playing
		data->m_playing = true;

		return true;
	}

	bool RVDirectSound::StartCaptureDevice(RVIHardwareData * data)
	{
		// started capturing
		data->m_capturing = true;

		return true;
	}

	void RVDirectSound::StopPlaybackDevice(RVIHardwareData * data)
	{
		// stop the playback buffer
		IDirectSoundBuffer_Stop(data->m_playbackBuffer);

		// stopped playing
		data->m_playing = false;

		// clear any pending sources & buffers
		data->ClearSourceList();
	}

	void RVDirectSound::StopCaptureDevice(RVIHardwareData * data)
	{
		// stopped capturing
		data->m_capturing = false;

		// reset the encoder's state
		RVCodec::ResetEncoder();
	}

	bool RVDirectSound::IsPlaybackDeviceStarted(RVIHardwareData * data)
	{
		HRESULT result;
		DWORD status;

		result = IDirectSoundBuffer_GetStatus(data->m_playbackBuffer, &status);
		if(FAILED(result))
		{
			rvError("RVDirectSound::IsPlaybackDeviceStarted() - Failed IDirectSoundBuffer_GetStatus with HRESULT 0x%08X", result);
			return false;
		}

		if(status & DSBSTATUS_PLAYING)
			return true;
		return false;
	}

	bool RVDirectSound::IsCaptureDeviceStarted(RVIHardwareData * data)
	{
		return data->m_capturing;
	}

	void RVDirectSound::SetPlaybackDeviceVolume(RVIHardwareData * data, double volume)
	{
		LONG vol;

		// convert from our scale to DS's scale
		if(volume == 0.0)
			vol = -10000;
		else
			vol = (LONG)(log(volume) * 2000);

		// set the volume
		IDirectSoundBuffer_SetVolume(data->m_playbackBuffer, vol);
	}

	void RVDirectSound::SetCaptureDeviceVolume(RVIHardwareData * data, double volume)
	{
		data->m_captureVolume = volume;
	}

	double RVDirectSound::GetPlaybackDeviceVolume(RVIHardwareData * data)
	{
		HRESULT result;
		LONG vol;

		// get the volume
		result = IDirectSoundBuffer_GetVolume(data->m_playbackBuffer, &vol);
		if(FAILED(result))
		{
			rvError("RVDirectSound::GetPlaybackDeviceVolume() - Failed IDirectSoundBuffer_GetVolume with HRESULT 0x%08X", result);
			return (double)0;
		}

		// convert it to our format
		return (double)(exp(vol / 2000.0));
	}

	double RVDirectSound::GetCaptureDeviceVolume(RVIHardwareData * data)
	{
		return data->m_captureVolume;
	}

	bool RVDirectSound::InitPlaybackDevice(RVIHardwareData * data)
	{
		HRESULT result;
		DSBUFFERDESC bufferDescriptor;

		// set the cooperative level
		result = IDirectSound_SetCooperativeLevel(data->m_playback, m_Hwnd, DSSCL_PRIORITY);
		if(FAILED(result))
		{
			rvError("RVDirectSound::InitPlaybackDevice() - Failed IDirectSound_SetCooperativeLevel with HRESULT 0x%08X", result);
			return false;
		}

		// setup the buffer size
		data->m_playbackBufferSize = RVUtil::MultiplyByBytesPerMillisecond(RVI_PLAYBACK_BUFFER_MILLISECONDS);

		// make sure it is a multiple of twice the raw frame size
		// it needs to be twice the size because it will be split in two
		data->m_playbackBufferSize = RVUtil::RoundUpToNearestMultiple(data->m_playbackBufferSize, RVCodec::GetBytesPerFrame() * 2);

		// we use this a lot, so calc it here
		data->m_playbackBufferHalfSize = (data->m_playbackBufferSize / 2);

		// set up the buffer descriptor
		sysMemSet(&bufferDescriptor, 0, sizeof(DSBUFFERDESC));
		bufferDescriptor.dwSize = sizeof(DSBUFFERDESC);
		bufferDescriptor.dwFlags = DSBCAPS_CTRLVOLUME|DSBCAPS_GETCURRENTPOSITION2;
		bufferDescriptor.dwFlags |= DSBCAPS_GLOBALFOCUS;
		bufferDescriptor.dwBufferBytes = (DWORD)data->m_playbackBufferSize;
		bufferDescriptor.lpwfxFormat = &m_RVIWaveFormat;

		// create the buffer
		result = IDirectSound_CreateSoundBuffer(data->m_playback, &bufferDescriptor, &data->m_playbackBuffer, NULL);
		if(FAILED(result))
		{
			rvError("RVDirectSound::InitPlaybackDevice() - Failed IDirectSound_CreateSoundBuffer with HRESULT 0x%08X", result);
			return false;
		}

		return true;
	}

	bool RVDirectSound::InitCaptureDevice(RVIHardwareData * data)
	{
		HRESULT result;
		DSCBUFFERDESC bufferDescriptor;

		// setup the buffer size
		data->m_captureBufferSize = RVUtil::MultiplyByBytesPerMillisecond(RVI_CAPTURE_BUFFER_MILLISECONDS);

		// make sure it is a multiple of the raw frame size
		data->m_captureBufferSize = RVUtil::RoundUpToNearestMultiple(data->m_captureBufferSize, RVCodec::GetBytesPerFrame());

		// setup the buffer descriptor
		sysMemSet(&bufferDescriptor, 0, sizeof(bufferDescriptor));
		bufferDescriptor.dwSize = sizeof(DSCBUFFERDESC);
		bufferDescriptor.dwBufferBytes = (DWORD)data->m_captureBufferSize;
		bufferDescriptor.lpwfxFormat = &m_RVIWaveFormat;

		// create the buffer
		result = IDirectSoundCapture_CreateCaptureBuffer(data->m_capture, &bufferDescriptor, &data->m_captureBuffer, NULL);
		if(FAILED(result))
		{
			rvError("RVDirectSound::InitCaptureDevice() - Failed IDirectSoundCapture_CreateCaptureBuffer with HRESULT 0x%08X", result);
			return false;
		}

		// start the buffer
		result = IDirectSoundCaptureBuffer_Start(data->m_captureBuffer, DSCBSTART_LOOPING);
		if(FAILED(result))
		{
			rvError("RVDirectSound::InitCaptureDevice() - Failed IDirectSoundCaptureBuffer_Start with HRESULT 0x%08X", result);
			IDirectSoundCaptureBuffer_Release(data->m_captureBuffer);
			data->m_captureBuffer = NULL;
			return false;
		}

		return true;
	}

	RVHardwareDevice* RVDirectSound::HardwareNewDevice(RVDeviceID deviceID, RVDeviceType type)
	{
		RVHardwareDevice * device;

		// DS can only handle one type or the other
		if((type != RV_CAPTURE) && (type != RV_PLAYBACK))
		{
			return NULL;
		}

		//setup the wave format
		sysMemSet(&m_RVIWaveFormat, 0, sizeof(WAVEFORMATEX));
		m_RVIWaveFormat.wFormatTag = WAVE_FORMAT_PCM;
		m_RVIWaveFormat.nChannels = 1;
		m_RVIWaveFormat.nSamplesPerSec = RVCodec::GetSampleRate();
		m_RVIWaveFormat.wBitsPerSample = RV_BITS_PER_SAMPLE;
		m_RVIWaveFormat.nBlockAlign = ((m_RVIWaveFormat.nChannels * m_RVIWaveFormat.wBitsPerSample) / 8); 
		m_RVIWaveFormat.nAvgBytesPerSec = (m_RVIWaveFormat.nSamplesPerSec * m_RVIWaveFormat.nBlockAlign);

		// create a new device
		device = RVMemory::New<RVHardwareDevice>();

		if(!device)
		{
			rvError("Could not create Hardware device");
			return NULL;
		}

		if (!device->Init(deviceID, RVHardwareDirectSound, type))
		{
			rvError("Could not initialize Hardware device");
			device->Shutdown();
			RVMemory::Delete(device);
			return NULL;
		}

		// add it to the list
		AppendDeviceToList(device);
		return device;
	}

	void RVDirectSound::FreeDevice(RVHardwareDevice* device)
	{
		rvDebug3("RVDirectSound::FreeDevice [%s]", device->GetDeviceIdStr());

		// shutdown the device
		device->Shutdown();

		// free memory for device
		RVMemory::Delete(device);
	}

	void RVDirectSound::AppendDeviceToList(RVHardwareDevice* device)
	{
		SYS_CS_SYNC(m_Cs);
		m_RVHardwareDevices.PushAndGrow(device);
	}

	void RVDirectSound::DeleteDeviceFromList(RVHardwareDevice* device, int indexHelper)
	{
		SYS_CS_SYNC(m_Cs);
		rvDebug3("RVDirectSound::DeleteDeviceFromList [%s]", device->GetDeviceIdStr());

		// remove pointer from array
		if (indexHelper >= 0)
		{
			m_RVHardwareDevices.Delete(indexHelper);
		}
		else
		{
			m_RVHardwareDevices.DeleteMatches(device);
		}

		// Free the device's memory
		FreeDevice(device);
	}

} // namespace rage

#endif // RSG_PC