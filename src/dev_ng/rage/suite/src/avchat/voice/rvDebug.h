#if RSG_PC
// 
// voice/rvDebug.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RV_DEBUG_H
#define RV_DEBUG_H

// rage headers
#include "diag/channel.h"

namespace rage
{

RAGE_DECLARE_CHANNEL(rvoice)

#define rvAssertf(cond,fmt,...)				RAGE_ASSERTF(rvoice,cond,fmt,##__VA_ARGS__)
#define rvVerifyf(cond,fmt,...)				RAGE_VERIFYF(rvoice,cond,fmt,##__VA_ARGS__)
#define rvFatal(fmt,...)					RAGE_FATALF(rvoice,fmt,##__VA_ARGS__)
#define rvError(fmt,...)					RAGE_ERRORF(rvoice,fmt,##__VA_ARGS__)
#define rvWarning(fmt,...)					RAGE_WARNINGF(rvoice,fmt,##__VA_ARGS__)
#define rvDisplay(fmt,...)					RAGE_DISPLAYF(rvoice,fmt,##__VA_ARGS__)
#define rvDebug1(fmt,...)					RAGE_DEBUGF1(rvoice,fmt,##__VA_ARGS__)
#define rvDebug2(fmt,...)					RAGE_DEBUGF2(rvoice,fmt,##__VA_ARGS__)
#define rvDebug3(fmt,...)					RAGE_DEBUGF3(rvoice,fmt,##__VA_ARGS__)
#define rvLogf(severity,fmt,...)			RAGE_LOGF(rvoice,severity,fmt,##__VA_ARGS__)
#define rvCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,rvoice,severity,fmt,##__VA_ARGS__)

} // namespace rage


#endif // RV_DEBUG_H

#endif // RSG_PC