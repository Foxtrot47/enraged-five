#if RSG_PC
// 
// voice/rvMemory.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#ifndef RV_MEMORY_H
#define RV_MEMORY_H

//When 1, this tracks number of allocations and sizes, including highwater marks.
//This generates a huge amount of log output, though.
#define RV_DEBUG_MEM				0

//Makes rvoice allocations go through the allocator passed to rlInit().
//When 0, it uses the global heap allocator.
#define RV_USE_RL_ALLOCATOR	1

#include "rline/rl.h"
#include "system/memory.h"

namespace rage
{
#if RV_USE_RL_ALLOCATOR
	extern sysMemAllocator* g_rlAllocator;
#endif

	class RVMemory
	{
	public:
		template<class T> 
		static T* New()
		{
			T* ptr = NULL;

#if RV_USE_RL_ALLOCATOR
			ptr = RL_NEW(RVMemory, T);
			if (!ptr)
			{
				ptr = rage_new T();
			}
#else
			ptr = rage_new T();
#endif

			return ptr;
		}

		template<class T> 
		static void Delete(T* ptr)
		{
#if RV_USE_RL_ALLOCATOR
			if(g_rlAllocator->IsValidPointer(ptr))
			{
				RL_DELETE(ptr);
			}
			else
			{
				delete ptr;
			}
#else
			delete ptr;
#endif
		}

		static void* Allocate(size_t size);
		static void Free(void* ptr);
		static void* ReAllocate(void* ptr, size_t size);
	};
}

#endif // RV_MEMORY_H

#endif // RSG_PC