#if RSG_PC
// 
// voice/rvSource.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// Based off GameSpyVoice: https://github.com/gamespy-tech/GameSpyOpenSource
//

#include "rvSource.h"
#include "rvCodec.h"
#include "rvFrame.h"
#include "rvUtil.h"
#include "rv.h"

#include <stdio.h>

#include "system/memops.h"

namespace rage
{
	RAGE_DEFINE_SUBCHANNEL(rvoice, source)
	#undef __rage_channel
	#define __rage_channel rvoice_source

	#define RVI_SYNCHRONIZATION_DELAY                 200

	#define RVI_CLOCK_OFFSET_AVERAGING_FACTOR         0.96875  //(31/32)

	#define RVI_DIVERGENCE_HIGH_WATERMARK             10
	#define RVI_DIVERGENCE_LOW_WATERMARK              ((u16)-5)

	static int RVISynchronizationDelayFrames;
	bool     RVIGlobalMute;

	void RVSourceMgr::FreeSource(RVISource * source)
	{
		RVIPendingFrame * frame = source->m_frameHead.m_next;
		RVIPendingFrame * next;

		// make sure it is in use
		if(!source->m_inUse)
		{
			return;
		}

		// put all of the pending frames back into the list
		while(frame)
		{
			next = frame->m_next;
			RVFrameMgr::PutPendingFrame(frame);
			frame = next;
		}

		// free the decoder data
		RVCodec::FreeDecoder(source->m_decoderData);

		// it is no longer in use
		source->m_inUse = false;
	}

	void RVSourceMgr::Init()
	{
		if(!RVISynchronizationDelayFrames)
		{
			int delay;
		
			delay = RVUtil::MultiplyByBytesPerMillisecond(RVI_SYNCHRONIZATION_DELAY);
			delay = RVUtil::RoundUpToNearestMultiple(delay, RVCodec::GetBytesPerFrame());

			RVISynchronizationDelayFrames = (delay / RVCodec::GetBytesPerFrame());
		}
	}

	RVISource* RVSourceMgr::FindSourceInList(RVISource* sourceList, RVSource source)
	{
		RVISource * rviSource;
		int i;

		// check if this source is in the list
		for(i = 0 ; i < MAX_SOURCES ; i++)
		{
			rviSource = &sourceList[i];
			if(rviSource->m_inUse)
			{
				if(memcmp(&rviSource->m_source, &source, sizeof(RVSource)) == 0)
				{
					return rviSource;
				}
			}
		}

		return NULL;
	}

	bool RVSourceMgr::IsSourceTalking(RVISource* sourceList, RVSource source)
	{
		RVISource * RVISource = FindSourceInList(sourceList, source);
		if(!RVISource)
		{
			return false;
		}

		return RVISource->m_isTalking;
	}

	int RVSourceMgr::ListTalkingSources(RVISource* sourceList, RVSource sources[], int maxSources)
	{
		RVISource * RVISource;
		int numTalking = 0;
		int i;

		// loop through the sources
		for(i = 0 ; i < MAX_SOURCES ; i++)
		{
			RVISource = &sourceList[i];

			// check if the source is in use and talking
			if(RVISource->m_inUse && RVISource->m_isTalking)
			{
				// add it to the list
				sysMemCpy(&sources[numTalking], &RVISource->m_source, sizeof(RVSource));

				// one more talker
				numTalking++;

				// check for the max
				if(numTalking == maxSources)
				{
					break;
				}
			}
		}

		return numTalking;
	}

	void RVSourceMgr::SetGlobalMute(bool mute)

	{
		RVIGlobalMute = mute;
	}

	bool RVSourceMgr::GetGlobalMute(void)
	{
		return RVIGlobalMute;
	}

	RVISource * RVSourceMgr::AddSourceToList(RVISource* sourceList, RVSource source)
	{
		bool result;
		RVISource * rviSource = NULL;
		int i;

		// loop through the sources
		for(i = 0 ; i < MAX_SOURCES ; i++)
		{
			// check if this source is available
			if(!sourceList[i].m_inUse)
			{
				rviSource = &sourceList[i];
				break;
			}

			// also look for a source without frames
			// if we don't find a totally free one, we can take this one over
			if(!rviSource && !sourceList[i].m_frameHead.m_next)
			{
				rviSource = &sourceList[i];
			}
		}

		// check if we didn't find anything
		if(!rviSource)
		{
			return NULL;
		}

		// if this source is already in use, free it first
		if(rviSource->m_inUse)
		{
			FreeSource(rviSource);
		}

		// initialize source
		sysMemSet(rviSource, 0, sizeof(RVISource));

		// make sure we can get a new decoder before moving on
		result = RVCodec::NewDecoder(&rviSource->m_decoderData);
		if(!result)
		{
			rvError("Error allocating new Decoder");
			return NULL;
		}

		sysMemSet(rviSource, 0, sizeof(rviSource));

		// mark as in use
		rviSource->m_inUse = true;

		// set the rest of the info
		sysMemCpy(&rviSource->m_source, &source, sizeof(RVSource));
		rviSource->m_clockOffset = 0;
		rviSource->m_clockOffsetAverage = 0;
		rviSource->m_isTalking = false;
		rviSource->m_finishedTalkingTime = 0;
		rviSource->m_frameHead.m_next = NULL;

		return rviSource;
	}

	void RVSourceMgr::AddPacketToSource(RVISource * source, u16 frameStamp, const u8 * packet, int len, bool mute)
	{
		RVIPendingFrame * frame;
		RVIPendingFrame * nextFrame;
		RVIPendingFrame * newFrame;
		u16 packetFinishedTime;
		int numFrames;
		int i;

		// the packet len should be a multiple of the encoded frame size
		Assert(!(len % RVCodec::GetEncodedFrameSize()));

		// calculate the number of frames in this packet
		numFrames = (len / RVCodec::GetEncodedFrameSize());

		// use the clock offset to adjust the frameStamp
		// Expanded to remove warnings in VS2K5
		frameStamp = frameStamp + (u16)source->m_clockOffset;

		// figure out when this packet will be finished
		packetFinishedTime = (u16)(frameStamp + numFrames);

		// update the time at which this source is done talking
		if(RVFrameMgr::IsFrameStampGT(packetFinishedTime, source->m_finishedTalkingTime))
		{
			source->m_finishedTalkingTime = packetFinishedTime;
		}

		// if muted, don't add.
		if(mute || RVIGlobalMute)
		{
			//Flag that they are currently talking because we skip this step in the hardware.
			source->m_isTalking = true;
			return;
		}

		// find where to add it
		// it must be in chronological order
		for(frame = &source->m_frameHead ; frame->m_next ; frame = frame->m_next)
		{
			// store a pointer to the next frame
			// this is the frame we will compare to this time through the loop
			nextFrame = frame->m_next;

			// check if the framestamp is the same (a repeated packet)
			if(nextFrame->m_frameStamp == frameStamp)
			{
				return;
			}

			// check if the new frame should be placed in front of the next frame
			if(RVFrameMgr::IsFrameStampGT(nextFrame->m_frameStamp, frameStamp))
			{
				// check that the packet's finish time doesn't cross over the next frame
				Assert(!RVFrameMgr::IsFrameStampGT(packetFinishedTime, nextFrame->m_frameStamp));

				// everything is good, break out so we can insert the new frames
				break;
			}
		}

		// loop through the frames in the packet
		for(i = 0 ; i < numFrames ; i++)
		{
			// get a new frame
			newFrame = RVFrameMgr::GetPendingFrame();
			if(!newFrame)
			{
				return;
			}

			// fill it in
			newFrame->m_frameStamp = frameStamp;
	#if RVI_PRE_DECODE
			RVIDecodeSet(newFrame->m_frame, packet, source->m_decoderData);
	#else
			sysMemCpy(newFrame->m_frame, packet, (unsigned int)RVCodec::GetEncodedFrameSize());
	#endif
			newFrame->m_next = frame->m_next;

			// setup the previous frame's next pointer to point to this one
			frame->m_next = newFrame;

			// adjust vars
			frameStamp++;
			packet += RVCodec::GetEncodedFrameSize();
			frame = newFrame;
		}
	}

	void RVSourceMgr::AddPacketToSourceList(RVISource* sourceList,
								  const u8 * packet, int len, RVSource source, u16 frameStamp, bool mute,
								  u16 currentPlayClock)
	{
		u16 playTime;
		RVISource * rviSource;
		u16 clockOffset;
		u16 divergence;
		int unwrappedClockOffset;

		// add the sync delay to the clock to get the play time
		playTime = (u16)(currentPlayClock + RVISynchronizationDelayFrames);

		// calculate the clock offset
		clockOffset = (u16)(playTime - frameStamp);

		// check if this source already exists
		rviSource = FindSourceInList(sourceList, source);
		if(!rviSource)
		{
			// it isn't, so add it
			rviSource = AddSourceToList(sourceList, source);

			// drop the packet if it couldn't be added
			if(!rviSource)
			{
				rvDebug3("Packet could not be added to source list");
				return;
			}

			// store the clock offset
			rviSource->m_clockOffset = clockOffset;
			rviSource->m_clockOffsetAverage = (float)clockOffset;

			// init the finished talking time
			rviSource->m_finishedTalkingTime = (u16)(frameStamp + rviSource->m_clockOffset);
		}
		else
		{
			// unwrap the clock offset if needed
			if((clockOffset < rviSource->m_clockOffsetAverage) && RVFrameMgr::IsFrameStampGT(clockOffset, (u16)rviSource->m_clockOffsetAverage))
			{
				unwrappedClockOffset = (clockOffset + RVI_FRAMESTAMP_MAX);
			}
			else if((rviSource->m_clockOffsetAverage < clockOffset) && RVFrameMgr::IsFrameStampGT((u16)rviSource->m_clockOffsetAverage, clockOffset))
			{
				unwrappedClockOffset = (clockOffset - RVI_FRAMESTAMP_MAX);
			}
			else
			{
				unwrappedClockOffset = clockOffset;
			}

			// update the running average of the clock offset
			rviSource->m_clockOffsetAverage =
				(float)((rviSource->m_clockOffsetAverage * RVI_CLOCK_OFFSET_AVERAGING_FACTOR) +
				(unwrappedClockOffset * (1.0 - RVI_CLOCK_OFFSET_AVERAGING_FACTOR)));

			if(rviSource->m_clockOffsetAverage < 0)
			{
				rviSource->m_clockOffsetAverage += RVI_FRAMESTAMP_MAX;
			}
			else if(rviSource->m_clockOffsetAverage >= RVI_FRAMESTAMP_MAX)
			{
				rviSource->m_clockOffsetAverage -= RVI_FRAMESTAMP_MAX;
			}

			// calculate the divergence
			divergence = (u16)(rviSource->m_clockOffset - (u16)rviSource->m_clockOffsetAverage);

			// check against the high-water mark
			if(RVFrameMgr::IsFrameStampGT(divergence, RVI_DIVERGENCE_HIGH_WATERMARK))
			{
				// update the clock offset
				rviSource->m_clockOffset--;

				// if this is a one frame packet, just drop it
				if(len == RVCodec::GetEncodedFrameSize())
				{
					return;
				}

				// otherwise update the params
				packet += RVCodec::GetEncodedFrameSize();
				len -= RVCodec::GetEncodedFrameSize();
			}
			// check against the low-water mark
			else if(RVFrameMgr::IsFrameStampGT(RVI_DIVERGENCE_LOW_WATERMARK, divergence))
			{
				// update the clock offset
				// this will basically add a frame of silence
				rviSource->m_clockOffset++;
			}

			// check if this packet is too old or too far ahead to play
			if(RVFrameMgr::IsFrameStampGT(currentPlayClock, (u16)(frameStamp + rviSource->m_clockOffset)))
			{
				return;
			}
		}

		// add the packet to the source
		AddPacketToSource(rviSource, frameStamp, packet, len, mute);
	}

	bool RVSourceMgr::WriteSourcesToBuffer(RVISource* sourceList, u16 startTime,
								   s16 * sampleBuffer, int numFrames)
	{
		RVISource * source;
		u16 timeSliceEnd;
		RVIPendingFrame * frame;
		bool result = false;

		// calculate the end of the time slice
		timeSliceEnd = (u16)(startTime + numFrames);

		// clear the sample buffer
		sysMemSet(sampleBuffer, 0, (unsigned int)numFrames * RVCodec::GetBytesPerFrame());

		// loop through the sources
		for(int i = 0 ; i < MAX_SOURCES ; i++)
		{
			// get the next source
			source = &sourceList[i];

			// check if it is in use
			if(!source->m_inUse)
			{
				continue;
			}

			// keep going while there are pending frames for this source
			while(source->m_frameHead.m_next)
			{
				// cache a pointer to the first frame
				frame = source->m_frameHead.m_next;

				// check if this frame is too far ahead
				if(RVFrameMgr::IsFrameStampGTE(frame->m_frameStamp, timeSliceEnd))
				{
					break;
				}

				// make sure this buffer's timeslice hasn't already elapsed
				if(RVFrameMgr::IsFrameStampGTE(frame->m_frameStamp, startTime))
				{
					// add the frame to the buffer
					s16 * writePtr = (sampleBuffer + ((u16)(frame->m_frameStamp - startTime) * RVCodec::GetSamplesPerFrame()));
	#if RVI_PRE_DECODE
					int j;
					for(j = 0 ; j < m_SamplesPerFrame ; j++)
						writePtr[j] += frame->m_frame[j];
	#else
					RVCodec::DecodeAdd(writePtr, frame->m_frame, source->m_decoderData);
	#endif

					// this source is talking
					source->m_isTalking = true;

					// set the return value to indicate that decoding took place
					result = true;
				}

				// update the source to point to the next frame
				source->m_frameHead.m_next = frame->m_next;

				// free the frame we just used
				RVFrameMgr::PutPendingFrame(frame);
			}

			// remove the source if it has no more frames and is marked as finished
			if(!source->m_frameHead.m_next && RVFrameMgr::IsFrameStampGTE(timeSliceEnd, source->m_finishedTalkingTime))
			{
				FreeSource(source);
			}
		}

		return result;
	}

} // namespace rage
 
#endif // RSG_PC