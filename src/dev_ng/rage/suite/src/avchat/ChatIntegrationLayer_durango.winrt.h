//// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//// PARTICULAR PURPOSE.
////
//// Copyright (c) Microsoft Corporation. All rights reserved
#pragma once

#if RSG_DURANGO

#pragma warning(push)
#pragma warning(disable: 4668)
#include <xdk.h>
#include <collection.h>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable: 4062)
#include "ppltasks.h"
#pragma warning(pop)

#include "system/criticalsection.h"

using namespace Windows::Xbox::System;
using namespace Windows::Storage::Streams;

using namespace Windows::Foundation;

enum ChatPacketType
{
    IncomingPacket = 0,
    OutgoingPacket = 1
};

namespace Microsoft {
namespace Xbox {
namespace GameChat {

public enum class ChatUserUpdateType
{
	CHAT_UPDATE_INVALID		= -1,
	CHAT_LOCAL_USER_ADDED	= 0,
	CHAT_LOCAL_USER_REMOVED,
	CHAT_REMOTE_CONSOLE_REMOVED
};

public ref class ChatUserUpdate sealed
{
public:
	property Microsoft::Xbox::GameChat::ChatUserUpdateType UpdateType
	{
		Microsoft::Xbox::GameChat::ChatUserUpdateType get();
		void set(_In_ Microsoft::Xbox::GameChat::ChatUserUpdateType value);
	}

	property uint8 ChannelIndex
	{
		uint8 get();
		void set(_In_ uint8 value);
	}

	property Platform::Object^ UniqueRemoteConsoleIdentifier
	{
		Platform::Object^ get();
		void set(_In_ Platform::Object^ value);
	}

	property Windows::Xbox::System::IUser^ User
	{
		Windows::Xbox::System::IUser^ get();
		void set(_In_ Windows::Xbox::System::IUser^ value);
	}

internal:
	ChatUserUpdate() :
		m_updateType(ChatUserUpdateType::CHAT_UPDATE_INVALID),
		m_channelIndex(0),
		m_uniqueRemoteConsoleIdentifier(nullptr),
		m_user(nullptr)
	{

	}

private:
	ChatUserUpdateType m_updateType;
	uint8 m_channelIndex;
	Platform::Object^ m_uniqueRemoteConsoleIdentifier;
	Windows::Xbox::System::IUser^ m_user;	
};

} // namespace GameChat
} // namespace Xbox
} // namespace Microsoft

class ChatIntegrationLayer 
    : public std::enable_shared_from_this<ChatIntegrationLayer> // shared_from_this is needed to use a weak ref to 'this' when handling delegate callbacks
{
public:
    ChatIntegrationLayer();

    /// <summary>
    /// Initializes the chat manager
    /// </summary>
    void InitializeChatManager( 
        __in bool combineCaptureBuffersIntoSinglePacketEnabled,
        __in bool useKinectAsCaptureSource,
        __in bool applySoundEffectToCapture,
        __in bool applySoundEffectToRender
        );

    /// <summary>
    /// Shuts down the chat manager
    /// </summary>
    void Shutdown();

    /// <summary>
    /// Adds a local user to a specific channel
    /// This is helper function waits for the task to complete so shouldn't be called from the UI thread 
    /// </summary>
    /// <param name="channelIndex">The channel to add the user to</param>
    /// <param name="user">The local user to add</param>
    void AddLocalUserToChatChannel( 
        __in uint8 channelIndex,
        __in Windows::Xbox::System::IUser^ user
        ); 

    /// <summary>
    /// Removes a local user from a specific channel
    /// This is helper function waits for the task to complete so shouldn't be called from the UI thread 
    /// </summary>
    /// <param name="channelIndex">The channel to remove the user from</param>
    /// <param name="user">The local user to remove</param>
    void RemoveUserFromChatChannel(
        __in uint8 channelIndex,
        __in Windows::Xbox::System::IUser^ user
        );

    /// <summary>
    /// Removes a remote console from chat
    /// This is helper function waits for the task to complete so shouldn't be called from the UI thread 
    /// </summary>
    /// <param name="uniqueRemoteConsoleIdentifier">A unique ID for the remote console</param>
    void RemoveRemoteConsole( 
        Platform::Object^ uniqueRemoteConsoleIdentifier
        );

    /// <summary>
    /// Handles incoming chat messages from the game's network layer
    /// </summary>
    /// <param name="chatPacket">A buffer containing the chat message</param>
    /// <param name="uniqueRemoteConsoleIdentifier">A unique ID for the remote console</param>
    void OnIncomingChatMessage( 
        Windows::Storage::Streams::IBuffer^ chatPacket,
        Platform::Object^ uniqueRemoteConsoleIdentifier
        );

	/// <summary>
	/// Processes pending ChatUserUpdate
	/// </summary>
	void ProcessPendingChatUserUpdates();

	/// <summary>
	/// Processes active async operations
	/// </summary>
	void ProcessActiveAsyncOperations();

    /// <summary>
    /// Returns a list of chat users in the chat session
    /// </summary>
    Windows::Foundation::Collections::IVectorView<Microsoft::Xbox::GameChat::ChatUser^>^ GetChatUsers();

    /// <summary>
    /// Returns true if the game has mic focus.  Otherwise another app has mic focus
    /// </summary>
    bool HasMicFocus();

	/// <summary>
	/// Returns loudness of local player mic
	/// </summary>
	float GetMicLoudness();

    /// <summary>
    /// Helper function to swap the mute state of a specific chat user
    /// </summary>
    void ChangeChatUserMuteState(
        __in Microsoft::Xbox::GameChat::ChatUser^ chatUser 
        );

    /// <summary>
    /// Helper function to change the channel of a specific chat user
    /// </summary>
    void HandleChatChannelChanged(
        __in uint8 oldChatChannelIndex,
        __in uint8 newChatChannelIndex,
        __in Microsoft::Xbox::GameChat::ChatUser^ chatUser 
        );

    /// <summary>
    /// Call this when a new console connects.  
    /// This adds this console to the chat layer
    /// </summary>
    void OnSecureDeviceAssocationConnectionEstablished( 
        __in Platform::Object^ uniqueConsoleIdentifier 
        );

    /// <summary>
    /// Adds a list of locally signed in users that have intent to play to the chat session on a specific channel index.
    /// Avoid adding any user who is signed in that doesn't have intent to play otherwise users who are biometrically 
    /// signed in automatically will be added to the chat session
    /// </summary>
    /// <param name="channelIndex">The channel to add the users to</param>
    /// <param name="locallySignedInUsers">A list of locally signed in users that have intent to play</param>
    void AddAllLocallySignedInUsersToChatClient(
        __in uint8 channelIndex,
        __in Windows::Foundation::Collections::IVectorView<Windows::Xbox::System::User^>^ locallySignedInUsers
        );

    /// <summary>
    /// Handles when a debug message is received.  Send this to the UI and OutputDebugString.  Games should integrate with their existing log system.
    /// </summary>
    /// <param name="args">Contains the debug message to log</param>
    void OnDebugMessageReceived( 
        __in Microsoft::Xbox::GameChat::DebugMessageEventArgs^ args 
        );

    /// <summary>
    /// Send the chat packet to all connected consoles
    ///
    /// To integrate the Chat DLL in your game, change the following code to use your game's network layer.
    /// You will need to isolate chat messages to be unique from the rest of you game's other message types.
    /// When args->SendPacketToAllConnectedConsoles is true, your game should send the chat message to each connected console using the game's network layer.
    /// It should send the chat message with Reliable UDP if args->SendReliable is true.
    /// It should send the chat message in order (if that feature is available) if args->SendInOrder is true
    /// </summary>
    /// <param name="args">Describes the packet to send</param>
    void OnOutgoingChatPacketReady( 
        __in Microsoft::Xbox::GameChat::ChatPacketEventArgs^ args 
        );

	/// Get ready chat packets for the game to send over the network.
	Windows::Foundation::Collections::IVector<Microsoft::Xbox::GameChat::ChatPacketEventArgs^>^ GetPendingOutgoingChatPackets();

	/// Get ready user packets for the game to send over the network.
	Windows::Foundation::Collections::IVector<Microsoft::Xbox::GameChat::ChatPacketEventArgs^>^ GetPendingOutgoingUserPackets();

    /// <summary>
    /// Example of how to cast an int to a Platform::Object^
    /// </summary>
    Platform::Object^ IntToPlatformObject(
        __in int val
        );

    /// <summary>
    /// Example of how to cast an Platform::Object^ to an int
    /// </summary>
    int PlatformObjectToInt(
        __in Platform::Object^ obj
        );

    /// <summary>
    /// Helper function to get specific ChatUser by xboxUserId
    /// </summary>
    Microsoft::Xbox::GameChat::ChatUser^ GetChatUserByXboxUserId(
        __in Platform::String^ xboxUserId
        );

    /// <summary>
    /// Helper function to get specific ChatUser by xboxUserId
    /// </summary>
    bool ChatIntegrationLayer::CompareUniqueConsoleIdentifiers( 
        __in Platform::Object^ uniqueRemoteConsoleIdentifier1, 
        __in Platform::Object^ uniqueRemoteConsoleIdentifier2 
        );

    /// <summary>
    /// uniqueRemoteConsoleIdentifier is a Platform::Object^ and can be cast or unboxed to most types. 
    /// What exactly you use doesn't matter, but optimally it would be something that uniquely identifies a console on in the session. 
    /// A Windows::Xbox::Networking::SecureDeviceAssociation^ is perfect to use if you have access to it.
    ///
    /// This is how you would convert a Platform::Object to an int 
    /// int consoleId = PlatformOjectToInt(args->TargetUniqueConsoleIdentifier);
    /// And then compare as normal
    ///
    /// In this example, the uniqueRemoteConsoleIdentifier is a Windows::Xbox::Networking::SecureDeviceAssociation, 
    /// so we compare using the RemoteSecureDeviceAddress using the helper function AreSecureDeviceAddressesEqual
    /// </summary>
    bool AreSecureDeviceAddressesEqual( 
        Windows::Xbox::Networking::SecureDeviceAssociation^ secureDeviceAssociation1, 
        Windows::Xbox::Networking::SecureDeviceAssociation^ secureDeviceAssociation2
        );

    /// <summary>
    /// This delegate handler is called prior to encoding captured audio buffer.
    /// This allows titles to apply sound effects to the capture stream
    /// </summary>
    Windows::Storage::Streams::IBuffer^ ApplyPreEncodeChatSoundEffects(
        __in Windows::Storage::Streams::IBuffer^ preEncodedRawBuffer, 
        __in Windows::Xbox::Chat::IFormat^ audioFormat,
        __in Windows::Foundation::Collections::IVectorView<Microsoft::Xbox::GameChat::ChatUser^>^ chatUsers
        );

    /// <summary>
    /// This delegate handler is called after decoding a remote audio buffer.
    /// This allows titles to apply sound effects to a remote user's audio mix
    /// </summary>
    Windows::Storage::Streams::IBuffer^ ApplyPostDecodeChatSoundEffects(
        __in Windows::Storage::Streams::IBuffer^ postDecodedRawBuffer, 
        __in Windows::Xbox::Chat::IFormat^ audioFormat,
        __in Windows::Foundation::Collections::IVectorView<Microsoft::Xbox::GameChat::ChatUser^>^ chatUsers
        );

    void OnUserAudioDeviceAdded(
        __in Windows::Xbox::System::AudioDeviceAddedEventArgs^ eventArgs
        );

	void OnUserAudioDeviceRemoved(
		__in Windows::Xbox::System::AudioDeviceRemovedEventArgs ^ eventArgs
		);

	void OnUserMuteStateChanged(
		__in Windows::Xbox::Chat::UserMuteStateChangedEventArgs ^ eventArgs
		);

    void OnUserRemoved(
        __in Windows::Xbox::System::UserRemovedEventArgs^ eventArgs
        );
	
	void OnControllerPairingChanged( 
		__in Windows::Xbox::Input::ControllerPairingChangedEventArgs^ args 
		);

	rage::sysCriticalSectionToken& GetChatPacketVectorCSToken() { return m_chatPacketVectorCriticalSectionToken; }
	rage::sysCriticalSectionToken& GetUserPacketVectorCSToken() { return m_userPacketVectorCriticalSectionToken; }

	typedef void (*OnUserAudioDeviceAddedCB )(__in Windows::Xbox::System::AudioDeviceAddedEventArgs^);
	typedef void (*OnUserAudioDeviceRemovedCB )(__in Windows::Xbox::System::AudioDeviceRemovedEventArgs^);
	typedef void (*OnUserMuteStateChangedCB )(__in Windows::Xbox::Chat::UserMuteStateChangedEventArgs^);

	OnUserAudioDeviceAddedCB m_OnUserAudioDeviceAddedCB;
	OnUserAudioDeviceRemovedCB m_OnUserAudioDeviceRemovedCB;
	OnUserMuteStateChangedCB m_OnUserMuteStateChangedCB;

private:
	/// <summary>
	/// Adds a local user to a specific channel
	/// </summary>
	/// <param name="channelIndex">The channel to add the user to</param>
	/// <param name="user">The local user to add</param>
	void AddLocalUserToChatChannelInternal( 
		__in uint8 channelIndex,
		__in Windows::Xbox::System::IUser^ user
		); 

	/// <summary>
	/// Removes a local user from a specific channel
	/// </summary>
	/// <param name="channelIndex">The channel to remove the user from</param>
	/// <param name="user">The local user to remove</param>
	void RemoveUserFromChatChannelInternal(
		__in uint8 channelIndex,
		__in Windows::Xbox::System::IUser^ user
		);

	/// <summary>
	/// Removes a remote console from chat
	/// </summary>
	/// <param name="uniqueRemoteConsoleIdentifier">A unique ID for the remote console</param>
	void RemoveRemoteConsoleInternal( 
		Platform::Object^ uniqueRemoteConsoleIdentifier
		);

    Microsoft::Xbox::GameChat::ChatManager^ m_chatManager;
	volatile bool m_isChatManagerValid;
    Windows::Foundation::EventRegistrationToken m_tokenOnDebugMessage;
    Windows::Foundation::EventRegistrationToken m_tokenOnOutgoingChatPacketReady;
    Windows::Foundation::EventRegistrationToken m_tokenOnCompareUniqueConsoleIdentifiers;
    Windows::Foundation::EventRegistrationToken m_tokenResourceAvailabilityChanged;
    Windows::Foundation::EventRegistrationToken m_tokenOnPreEncodeAudioBuffer;
    Windows::Foundation::EventRegistrationToken m_tokenOnPostDecodeAudioBuffer;
	Windows::Foundation::EventRegistrationToken m_tokenAudioDeviceAdded;
	Windows::Foundation::EventRegistrationToken m_tokenAudioDeviceRemoved;
	Windows::Foundation::EventRegistrationToken m_tokenUserMuteStateChanged;

	Windows::Foundation::Collections::IVector<Microsoft::Xbox::GameChat::ChatUserUpdate^>^ m_pendingChatUserUpdates;
	Windows::Foundation::Collections::IVector<Windows::Foundation::IAsyncAction^>^ m_activeChatUserAsyncOperations;
	static rage::sysCriticalSectionToken m_chatUserUpdateCriticalSectionToken;
	volatile bool m_isProcessingChatUserUpdate;

	Windows::Foundation::Collections::IVector<Microsoft::Xbox::GameChat::ChatPacketEventArgs^>^ m_pendingOutgoingChatPackets;
	static rage::sysCriticalSectionToken m_chatPacketVectorCriticalSectionToken;

	Windows::Foundation::Collections::IVector<Microsoft::Xbox::GameChat::ChatPacketEventArgs^>^ m_pendingOutgoingUserPackets;
	static rage::sysCriticalSectionToken m_userPacketVectorCriticalSectionToken;
};

class ChatIntegrationLayerUtils
{
public:
	static bool IsStringEqualCaseInsenstive( Platform::String^ xboxUserId1, Platform::String^ xboxUserId2 );

	static void GetBufferBytes( __in Windows::Storage::Streams::IBuffer^ buffer, __out byte** ppOut );
};

std::shared_ptr<ChatIntegrationLayer> GetChatIntegrationLayer();

#endif // RSG_DURANGO
