// 
// avchat/voicechat_durango.winrt.cpp
// 
// Copyright (C) 2014 Rockstar Games.  All Rights Reserved. 
// 

#include "voicechat_durango.h"

#if RSG_DURANGO

#include "ChatIntegrationLayer_durango.winrt.h"

#include "atl/vector.h"
#include "diag/channel.h"
#include "net/connectionmanager.h"
#include "net/event.h"
#include "net/message.h"
#include "net/netdiag.h"
#include "rline/durango/private/rlxblcommon.h"
#include "rline/durango/private/rlxblinternal.h"
#include "system/timer.h"

// This code uses 'ref new' which uses global new. This include is to fix url:bugstar:1743182 (even though its
// a different callstack, it appears to affect the other file). This must be before the WinRT includes .
#include "system/new_winrt.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <xdk.h>
#include <collection.h>
#pragma warning(pop)

#include "robuffer.h"
#using <windows.winmd>

using namespace Microsoft::Xbox::GameChat;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Storage::Streams;
using namespace Windows::Xbox::Chat;

#define DISPLAY_DEBUG_VOICE_CHAT 0 // Do not enable when submitting

#define ENABLE_USER_CHECKS_WHEN_PACKING_CHAT 0

extern __THREAD int RAGE_LOG_DISABLE;

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(ragenet, voicechat_durango)

#define vcDebug(fmt, ...)			RAGE_DEBUGF1(ragenet_voicechat_durango, fmt, ##__VA_ARGS__)
#define vcWarning(fmt, ...)			RAGE_WARNINGF(ragenet_voicechat_durango, fmt, ##__VA_ARGS__)
#define vcError(fmt, ...)			RAGE_ERRORF(ragenet_voicechat_durango, fmt, ##__VA_ARGS__)
#define vcVerify(cond)				RAGE_VERIFY(ragenet_voicechat_durango, cond)
#define vcAssert(cond, fmt, ...)	RAGE_ASSERTF(ragenet_voicechat_durango, cond, fmt, ##__VA_ARGS__)

#if __ASSERT
#define VC_EXCEPTION(ex)										\
	vcAssert(false, "Exception 0x%08x, Msg: %ls",				\
	ex->HResult, 												\
	ex->Message != nullptr ? ex->Message->Data() : L"NULL");		
#else
#define VC_EXCEPTION(ex)										\
	vcError("Exception 0x%08x, Msg: %ls",						\
	ex->HResult, 												\
	ex->Message != nullptr ? ex->Message->Data() : L"NULL");	
#endif

typedef u32 PacketGuardType;
typedef u16 PacketPayloadLengthType;
static const u32 PACKET_GUARD_SIZE = sizeof(PacketGuardType);
static const u32 PACKET_PAYLOAD_LENGTH_SIZE = sizeof(PacketPayloadLengthType);
static const PacketGuardType PACKET_GUARD_VALUE = 0xFFFFFFFF;

#if DISPLAY_DEBUG_VOICE_CHAT
typedef u32 PacketSequenceNumberType;
typedef u32 PacketNumberConcatenatedType;
static const u32 PACKET_SEQUENCE_NUMBER_SIZE = sizeof(PacketSequenceNumberType);
static const u32 PACKET_NUMBER_CONCATENATED_SIZE = sizeof(PacketNumberConcatenatedType);
#endif // DISPLAY_DEBUG_VOICE_CHAT

static const u8 DEFAULT_CHAT_CHANNEL = 0;

// Hang on to ChatIntegrationLayer shared pointer to make sure that the ref count doesn't
//   decrement to zero before VoiceChatDurango is done with it.
static std::shared_ptr<ChatIntegrationLayer> chatIntegrationLayerPtr = nullptr;

struct VoiceChatUserMsg
{
	NET_MESSAGE_DECL(VoiceChatUserMsg, VOICE_CHAT_USER_MSG);

	VoiceChatUserMsg()
	{

	}

	VoiceChatUserMsg(u64 uniqueUserId, u8* bufferBytes, u32 bufferSize)
	{
		m_UniqueUserId = uniqueUserId;
		if(AssertVerify(bufferSize <= sizeof(m_BufferBytes)))
		{
			sysMemCpy(m_BufferBytes, bufferBytes, bufferSize);
		}
		m_BufferSize = bufferSize;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		bool result = bb.SerUns(msg.m_UniqueUserId, sizeof(msg.m_UniqueUserId) << 3)
				   && bb.SerUns(msg.m_BufferSize, sizeof(msg.m_BufferSize) << 3)
				   && vcVerify(msg.m_BufferSize <= static_cast<unsigned>(sizeof(msg.m_BufferBytes)))
				   && bb.SerBytes(msg.m_BufferBytes, msg.m_BufferSize);
		vcAssert(result, "VoiceChatUserMsg - Serialization error");
		return result;
	}

	u64 m_UniqueUserId;
	u8 m_BufferBytes[1024];
	u32 m_BufferSize;
};

NET_MESSAGE_IMPL(VoiceChatUserMsg);

// event declarations
AUTOID_IMPL(rlXblChatEvent);
AUTOID_IMPL(rlXblChatEventUserMuteStateChanged);

void OnUserAudioDeviceAdded(AudioDeviceAddedEventArgs^ eventArgs)
{
	Assertf(eventArgs != nullptr, "OnUserAudioDeviceAdded - Invalid eventArgs");
	if (eventArgs && eventArgs->User)
	{
		try
		{
			chatIntegrationLayerPtr->AddLocalUserToChatChannel(DEFAULT_CHAT_CHANNEL, eventArgs->User);
		}
		catch (Platform::COMException^ e)
		{
			vcError("VoiceChatDurango::OnUserAudioDeviceAdded() - Failed to add user.");
			RL_EXCEPTION(e);
		}
		catch (...)
		{
			vcError("VoiceChatDurango::OnUserAudioDeviceAdded() - Failed to add user.");
		}

		if(eventArgs->User == nullptr)
		{
			vcDebug("VoiceChatDurango::OnUserAudioDeviceAdded - Invalid user!");
			return;
		}

		int localGamerIndex = rlXblInternal::GetInstance()->_GetPresenceManager()->GetLocalIndex(eventArgs->User);
		if(localGamerIndex < 0)
		{
			vcDebug("VoiceChatDurango::OnUserAudioDeviceAdded - Invalid local user for %ls!", eventArgs->User->XboxUserId->Data());
			return;
		}

		VoiceChatDurango& voiceChatDurango = VoiceChatDurangoSingleton::GetInstance();
		vcDebug("VoiceChatDurango::OnUserAudioDeviceAdded :: XUID: %ls, Index: %d", eventArgs->User->XboxUserId->Data(), localGamerIndex);

		// if we already have these set to TRUE, we don't care
		bool hasHeadset = voiceChatDurango.HasHeadset(localGamerIndex);
		bool hasSharedAudio = voiceChatDurango.HasSharedAudioDevice(localGamerIndex);
		if(hasHeadset && hasSharedAudio)
		{
			vcDebug("VoiceChatDurango::OnUserAudioDeviceAdded - Already have headset and shared audio device!");
			return;
		}

		// otherwise, check device
		if(eventArgs->AudioDevice != nullptr)
		{
			vcDebug("VoiceChatDurango::OnUserAudioDeviceAdded - ID: %ls, Category: %u, Type: %u, Sharing: %u", eventArgs->AudioDevice->Id->Data(), eventArgs->AudioDevice->DeviceCategory, eventArgs->AudioDevice->DeviceType, eventArgs->AudioDevice->Sharing);
			
			if(eventArgs->AudioDevice->DeviceCategory == Windows::Xbox::System::AudioDeviceCategory::Communications)
			{
				bool isHeadset = (eventArgs->AudioDevice->Sharing != Windows::Xbox::System::AudioDeviceSharing::Shared);
				bool isSharedAudio = (eventArgs->AudioDevice->Sharing == Windows::Xbox::System::AudioDeviceSharing::Shared);

				vcDebug("\tHeadset: %s, SharedAudio: %s", isHeadset ? "True" : "False", isSharedAudio ? "True" : "False");

				hasHeadset |= isHeadset;
				hasSharedAudio |= isSharedAudio;
			}
		}
		else
		{
			vcDebug("VoiceChatDurango::OnUserAudioDeviceAdded - Invalid device!");
		}

		voiceChatDurango.SetHasHeadset(localGamerIndex, hasHeadset);
		voiceChatDurango.SetHasSharedAudioDevice(localGamerIndex, hasSharedAudio);
	}
}

void OnUserAudioDeviceRemoved(AudioDeviceRemovedEventArgs ^ eventArgs)
{
	Assertf(eventArgs != nullptr, "OnUserAudioDeviceRemoved - Invalid eventArgs");
	if (eventArgs && eventArgs->User)
	{
		//try
		//{
		//	chatIntegrationLayerPtr->RemoveUserFromChatChannel(DEFAULT_CHAT_CHANNEL, eventArgs->User);
		//}
		//catch (Platform::COMException^ e)
		//{
		//	vcError("VoiceChatDurango::OnUserAudioDeviceRemoved() - Failed to remove user.");
		//	RL_EXCEPTION(e);
		//}
		//catch (...)
		//{
		//	vcError("VoiceChatDurango::OnUserAudioDeviceRemoved() - Failed to remove user.");
		//}

		if(eventArgs->User == nullptr)
		{
			vcDebug("VoiceChatDurango::OnUserAudioDeviceRemoved - Invalid user!");
			return;
		}

		int localGamerIndex = rlXblInternal::GetInstance()->_GetPresenceManager()->GetLocalIndex(eventArgs->User);
		if(localGamerIndex < 0)
		{
			vcDebug("VoiceChatDurango::OnUserAudioDeviceRemoved - Invalid local user for %ls!", eventArgs->User->XboxUserId->Data());
			return;
		}

		VoiceChatDurango& voiceChatDurango = VoiceChatDurangoSingleton::GetInstance();
		vcDebug("VoiceChatDurango::OnUserAudioDeviceRemoved :: XUID: %ls, Index: %d", eventArgs->User->XboxUserId->Data(), localGamerIndex);

#if !__NO_OUTPUT
		// log device details
		if(eventArgs->AudioDevice != nullptr)
		{
			vcDebug("VoiceChatDurango::OnUserAudioDeviceRemoved - ID: %ls, Category: %u, Type: %u, Sharing: %u", eventArgs->AudioDevice->Id->Data(), eventArgs->AudioDevice->DeviceCategory, eventArgs->AudioDevice->DeviceType, eventArgs->AudioDevice->Sharing);
		}
		else
		{
			vcDebug("VoiceChatDurango::OnUserAudioDeviceRemoved - Invalid device!");
		}
#endif

		// if we already have these set to false, we don't care
		if(!voiceChatDurango.HasHeadset(localGamerIndex) && !voiceChatDurango.HasSharedAudioDevice(localGamerIndex))
			return;

		// flag user for refresh
		voiceChatDurango.FlagHeadsetRefresh(localGamerIndex);
	}
}

void OnUserMuteStateChanged(Windows::Xbox::Chat::UserMuteStateChangedEventArgs ^ eventArgs)
{
	vcDebug("OnUserMuteStateChanged");

	Assertf(eventArgs != nullptr, "OnUserMuteStateChanged - Invalid eventArgs");
	if(eventArgs && eventArgs->Owner)
	{
		int localGamerIndex = -1;
		bool bIsMuted = false;
		u64 xuid = 0;

		try
		{
			// extract event parameters in try / catch
			int localGamerIndex = rlXblInternal::GetInstance()->_GetPresenceManager()->GetLocalIndex(eventArgs->Owner);
			if(localGamerIndex < 0)
			{
				vcWarning("OnUserMuteStateChanged - Could not find local owner");
				return;
			}

			bIsMuted = eventArgs->Muted;
			swscanf_s(eventArgs->RecipientXboxUserId->Data(), L"%" LI64FMT L"u", &xuid);
		}
		catch (Platform::Exception^ e)
		{
			vcError("OnUserMuteStateChanged - Failed to remove user.");
			RL_EXCEPTION(e);
		}

		vcDebug("OnUserMuteStateChanged :: Muted: %s, XUID: %llu", bIsMuted ? "True" : "False", xuid);

		// fire event 
		rlXblChatEventUserMuteStateChanged e(localGamerIndex, bIsMuted, xuid);
		VoiceChatDurangoSingleton::GetInstance().DispatchEvent(&e);
	}
}

static EventRegistrationToken s_KinectAvailabilityToken;

VoiceChatDurango::~VoiceChatDurango()
{
	m_Delegator.Clear();
}

bool
VoiceChatDurango::Init()
{
	try
	{
		chatIntegrationLayerPtr = GetChatIntegrationLayer();
		chatIntegrationLayerPtr->InitializeChatManager(false, true, true, false);
		vcDebug("VoiceChatDurango::Init() - Chat Manager Initialized");

		// callbacks
		chatIntegrationLayerPtr->m_OnUserAudioDeviceAddedCB = OnUserAudioDeviceAdded;
		chatIntegrationLayerPtr->m_OnUserAudioDeviceRemovedCB = OnUserAudioDeviceRemoved;
		chatIntegrationLayerPtr->m_OnUserMuteStateChangedCB = OnUserMuteStateChanged;

		// register for Kinect availability token
		Windows::Kinect::KinectSensor^ rKinect = Windows::Kinect::KinectSensor::GetDefault();
		if(rKinect != nullptr)
		{
			rKinect->Open();
			s_KinectAvailabilityToken = rKinect->IsAvailableChanged += 
				ref new TypedEventHandler<Windows::Kinect::KinectSensor^, Windows::Kinect::IsAvailableChangedEventArgs^>
				([=] (Windows::Kinect::KinectSensor^, Windows::Kinect::IsAvailableChangedEventArgs^)
			{
				vcDebug("Kinect Availablility Changed :: Flagging Refresh on all users");
				m_HeadsetRefreshMask = 0xffffffff;
			});
		}
	}
	catch (Platform::COMException^ e)
	{
		VC_EXCEPTION(e);
	}

	g_rlXbl.AddDelegate(&m_XblDlgt);
	m_XblDlgt.Bind(&VoiceChatDurango::OnXblEvent);

	m_HeadsetRefreshMask = 0;

	for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		m_bHasHeadset[i] = false;
		m_bHasSharedAudioDevice[i] = false;
	}

	m_LocalInfo.Clear();
	for(int i = 0; i < MAX_REMOTE_GAMERS; i++)
	{
		m_RemoteChatUserInfos[i].Clear();
	}
	
	m_InitialUserPacketSendAttemptTime.Reset();

	return true;
}

void
VoiceChatDurango::Shutdown()
{
	m_InitialUserPacketSendAttemptTime.Reset();

	g_rlXbl.RemoveDelegate(&m_XblDlgt);

	m_HeadsetRefreshMask = 0;

	for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
	{
		m_bHasHeadset[i] = false;
		m_bHasSharedAudioDevice[i] = false;
	}

	m_LocalInfo.Clear();
	for(int i = 0; i < MAX_REMOTE_GAMERS; i++)
	{
		m_RemoteChatUserInfos[i].Clear();
	}

	try
	{
		Windows::Kinect::KinectSensor^ rKinect = Windows::Kinect::KinectSensor::GetDefault();
		if(rKinect != nullptr)
		{
			rKinect->IsAvailableChanged -= s_KinectAvailabilityToken;
		}
		rKinect->Close();

		// device callbacks
		chatIntegrationLayerPtr->m_OnUserMuteStateChangedCB = nullptr;
		chatIntegrationLayerPtr->m_OnUserAudioDeviceAddedCB = nullptr;
		chatIntegrationLayerPtr->m_OnUserAudioDeviceRemovedCB = nullptr;

		chatIntegrationLayerPtr->Shutdown();
		chatIntegrationLayerPtr = nullptr;
		vcDebug("VoiceChatDurango::Shutdown() - Chat Manager Shutdown");
	}
	catch (Platform::COMException^ e)
	{
		VC_EXCEPTION(e);
	}
}

void
VoiceChatDurango::AddDelegate(rlXblChatEventDelegate* dlgt)
{
	SYS_CS_SYNC(m_Cs);
	m_Delegator.AddDelegate(dlgt);
}

void
VoiceChatDurango::RemoveDelegate(rlXblChatEventDelegate* dlgt)
{
	SYS_CS_SYNC(m_Cs);
	m_Delegator.RemoveDelegate(dlgt);
}

void
VoiceChatDurango::DispatchEvent(const rlXblChatEvent *e)
{
	m_Delegator.Dispatch(e);
}

void
VoiceChatDurango::RestartChatManager()
{
	Shutdown();
	Init();
}

bool
VoiceChatDurango::AddLocalChatUser(int localIndex)
{
	vcDebug("VoiceChatDurango::AddLocalChatUser - Index: %d", localIndex);

	try
	{
		// setup local chat user tracking
		m_LocalInfo.Clear();
		m_LocalInfo.Set(rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUserId(localIndex));
	}
	catch (Platform::COMException^ e)
	{
		VC_EXCEPTION(e);
	}

	return AddLocalParticipant(localIndex);
}

bool
VoiceChatDurango::AddRemoteChatUser(const rlGamerInfo& gamerInfo, const EndpointId endpointId, bool shouldHandleNewConnection)
{
	bool isAdded = false;

	// make sure we haven't already added this player
	for(int i = 0; i < MAX_REMOTE_GAMERS; ++i)
	{
		if(m_RemoteChatUserInfos[i].IsValid() && m_RemoteChatUserInfos[i].IsUniqueIdMatch(gamerInfo.GetGamerHandle().GetXuid()))
		{
			vcDebug("VoiceChatDurango::AddRemoteChatUser - Existing Index: %d, Name: %s, XUID: %llu, endpointId: %u", i, gamerInfo.GetName(), gamerInfo.GetGamerHandle().GetXuid(), endpointId);
			m_RemoteChatUserInfos[i].SetPendingRemove(false);
			isAdded = true;
			break;
		}
	}

	if(!isAdded)
	{
		for(int i = 0; i < MAX_REMOTE_GAMERS; ++i)
		{
			if(!m_RemoteChatUserInfos[i].IsValid())
			{
				vcDebug("VoiceChatDurango::AddRemoteChatUser - Index: %d, Name: %s, XUID: %llu, endpointId: %u", i, gamerInfo.GetName(), gamerInfo.GetGamerHandle().GetXuid(), endpointId);
				m_RemoteChatUserInfos[i].Set(endpointId, gamerInfo.GetGamerHandle().GetXuid());
				isAdded = true;
				break;
			}
		}
	}

	if(isAdded)
	{
		if (shouldHandleNewConnection)
		{
			vcDebug("VoiceChatDurango::AddRemoteChatUser - Adding Remote Participant. Name: %s, XUID: %llu", gamerInfo.GetName(), gamerInfo.GetGamerHandle().GetXuid());
			const u64 uniqueId = gamerInfo.GetGamerHandle().GetXuid();
			return AddRemoteParticipant(uniqueId);
		}
		else
		{
			vcDebug("VoiceChatDurango::AddRemoteChatUser - Skipping Remote Participant. Name: %s, XUID: %llu", gamerInfo.GetName(), gamerInfo.GetGamerHandle().GetXuid());
			return true;
		}
	}
	else
	{
		vcAssert(false, "VoiceChatDurango::AddRemoteChatUser - Remote chat user info list is too full. This should not happen. Name: %s, XUID: %llu", gamerInfo.GetName(), gamerInfo.GetGamerHandle().GetXuid());
		return false;
	}
}

void 
VoiceChatDurango::RemoveLocalChatUser(int localIndex)
{
	vcDebug("VoiceChatDurango::RemoveLocalChatUser - Index: %d", localIndex);
	RemoveLocalParticipant(localIndex);
}

bool
VoiceChatDurango::RemoveRemoteChatUser(const rlGamerHandle& gamerHandle)
{
	if(gamerHandle.IsValid())
	{
		bool isRemoved = false;

		for(int i = 0; i < MAX_REMOTE_GAMERS; ++i)
		{
			if(m_RemoteChatUserInfos[i].IsUniqueIdMatch(gamerHandle.GetXuid()))
			{
				vcDebug("VoiceChatDurango::RemoveRemoteChatUser - Index: %d, XUID: %llu", i, gamerHandle.GetXuid());
				m_RemoteChatUserInfos[i].Clear();
				isRemoved = true;
				break;
			}
		}

		if(isRemoved)
		{
			vcDebug("VoiceChatDurango::RemoveRemoteChatUser - Removing Remote Participant - XUID: %llu", gamerHandle.GetXuid());
			const u64 uniqueId = gamerHandle.GetXuid();
			VoiceChatDurangoSingleton::GetInstance().RemoveRemoteParticipant(uniqueId);
		}
		else
		{
			vcAssert(false, "VoiceChatDurango::RemoveChatUser - Unique user ID not found.");
#if !__NO_OUTPUT
			vcDebug("VoiceChatDurango::RemoveChatUser - Searching for XUID: %llu", gamerHandle.GetXuid());
			for(int i = 0; i < MAX_REMOTE_GAMERS; ++i)
			{
				vcDebug("VoiceChatDurango::RemoveChatUser - Full List Index: %d, XUID: %llu", i, m_RemoteChatUserInfos[i].GetUniqueId());
			}
#endif // !__NO_OUTPUT
			return false;
		}
	}
	else
	{
		vcDebug("VoiceChatDurango::RemoveRemoteChatUser - Unhandled.");
	}

	return true;
}

bool 
VoiceChatDurango::SetChatUserAsPendingRemove(const rlGamerHandle& gamerHandle)
{
	if(gamerHandle.IsValid())
	{
		for(int i = 0; i < MAX_REMOTE_GAMERS; ++i)
		{
			if(m_RemoteChatUserInfos[i].IsUniqueIdMatch(gamerHandle.GetXuid()))
			{
				vcDebug("VoiceChatDurango::SetChatUserAsPendingRemove - Setting chat user as pending remove - XUID: %llu", gamerHandle.GetXuid());
				m_RemoteChatUserInfos[i].SetPendingRemove(true);
				return true;
			}
		}
	}

	return false;
}

void
VoiceChatDurango::RemoveAllChatUsers()
{
	RestartChatManager();

	for(int i = 0; i < MAX_REMOTE_GAMERS; ++i)
	{
		if(m_RemoteChatUserInfos[i].IsValid())
		{
			m_RemoteChatUserInfos[i].Clear();
		}
	}

	return;
}

bool
VoiceChatDurango::AddLocalParticipant(int localIndex)
{
	try
	{
		User^ localXboxUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(localIndex);
		if(localXboxUser != nullptr)
		{
			chatIntegrationLayerPtr->AddLocalUserToChatChannel(DEFAULT_CHAT_CHANNEL, localXboxUser);
			return true;
		}
	}
	catch (Platform::COMException^ e)
	{
		VC_EXCEPTION(e);
	}

	return false;
}

bool
VoiceChatDurango::AddRemoteParticipant(u64 uniqueId)
{
	try
	{
		Platform::Object^ uniqueRemoteUserIdToAdd = Windows::Foundation::PropertyValue::CreateUInt64(uniqueId);
		chatIntegrationLayerPtr->OnSecureDeviceAssocationConnectionEstablished(uniqueRemoteUserIdToAdd);
	}
	catch (Platform::COMException^ e)
	{
		VC_EXCEPTION(e);
	}

	return true;
}

void
VoiceChatDurango::RemoveLocalParticipant(int localIndex)
{
	try
	{
		User^ localXboxUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(localIndex);
		if ((localXboxUser != nullptr) && localXboxUser->IsSignedIn)
		{
			chatIntegrationLayerPtr->RemoveUserFromChatChannel(DEFAULT_CHAT_CHANNEL, localXboxUser);
		}
	}
	catch (Platform::COMException^ e)
	{
		VC_EXCEPTION(e);
	}
}

void
VoiceChatDurango::RemoveRemoteParticipant(u64 uniqueId)
{
	try
	{
		Platform::Object^ uniqueRemoteRequestingUserIdToRemove = Windows::Foundation::PropertyValue::CreateUInt64(uniqueId);
		chatIntegrationLayerPtr->RemoveRemoteConsole(uniqueRemoteRequestingUserIdToRemove);
	}
	catch (Platform::COMException^ e)
	{
		VC_EXCEPTION(e);
	}
}

bool
VoiceChatDurango::UpdateChatState(ChatUserInfo* userInfo)
{
	if(!userInfo || !userInfo->IsValid())
		return false;

	try
	{
		String^ winRTXuid = rlXblCommon::XuidToWinRTString(userInfo->GetUniqueId());
		ChatUser^ chatUser = chatIntegrationLayerPtr->GetChatUserByXboxUserId(winRTXuid);

		if (chatUser)
		{
#if !__NO_OUTPUT
			bool bWasMuted = userInfo->m_IsMuted;
			bool bWasBlocked = userInfo->m_IsBlocked;
#endif

			userInfo->m_IsTalking	= (chatUser->TalkingMode == ChatUserTalkingMode::TalkingOverHeadset 
									|| chatUser->TalkingMode == ChatUserTalkingMode::TalkingOverKinect);


			userInfo->m_IsMuted		= (chatUser->IsMuted
									|| chatUser->RestrictionMode == Windows::Xbox::Chat::ChatRestriction::Muted);

			userInfo->m_IsBlocked	= (chatUser->RestrictionMode == Windows::Xbox::Chat::ChatRestriction::NotInSameChannel
									|| chatUser->RestrictionMode == Windows::Xbox::Chat::ChatRestriction::NotAllowed
									|| chatUser->RestrictionMode == Windows::Xbox::Chat::ChatRestriction::UnknownRelationship);

#if !__NO_OUTPUT
			if(userInfo->m_MuteTrack != chatUser->IsMuted)
			{
				vcDebug("UpdateChatState :: ChatUser->IsMuted Changed for %" I64FMT "u. Was: %s, Now: %s", 
					    userInfo->GetUniqueId(), 
						userInfo->m_MuteTrack ? "Muted" : "Not Muted", 
						chatUser->IsMuted ? "Muted" : "Not Muted");
				userInfo->m_MuteTrack = chatUser->IsMuted;
			}

			unsigned restrictionMode = static_cast<unsigned>(chatUser->RestrictionMode);
			if(userInfo->m_RestrictionTrack != restrictionMode)
			{
				vcDebug("UpdateChatState :: ChatUser->RestrictionMode Changed for %" I64FMT "u. Was: %u, Now: %u", 
					    userInfo->GetUniqueId(), 
						userInfo->m_RestrictionTrack, 
						restrictionMode);
				userInfo->m_RestrictionTrack = restrictionMode;
			}

			if(userInfo->m_IsMuted != bWasMuted)
			{
				vcDebug("UpdateChatState :: Muted Changed for %" I64FMT "u. Was: %s, Now: %s", 
					    userInfo->GetUniqueId(), 
						bWasMuted ? "Muted" : "Not Muted", 
						userInfo->m_IsMuted ? "Muted" : "Not Muted");
			}

			if(userInfo->m_IsBlocked != bWasBlocked)
			{
				vcDebug("UpdateChatState :: Blocked Changed for %" I64FMT "u. Was: %s, Now: %s", 
					    userInfo->GetUniqueId(), 
						bWasBlocked ? "Blocked" : "Not Blocked", 
						userInfo->m_IsBlocked ? "Blocked" : "Not Blocked");
			}
#endif
		}

		return false;
	}
	catch (Platform::Exception^ e)
	{
		VC_EXCEPTION(e);	
	}

	return false;
}

bool
VoiceChatDurango::IsTalking(u64 uniqueId) const
{
	// check local user first
	if(m_LocalInfo.IsUniqueIdMatch(uniqueId))
		return m_LocalInfo.m_IsTalking;

	// run through all remote users
	for(int j = 0; j < MAX_REMOTE_GAMERS; ++j)
	{
		if(m_RemoteChatUserInfos[j].IsValid() && m_RemoteChatUserInfos[j].GetUniqueId() == uniqueId)
		{
			return m_RemoteChatUserInfos[j].m_IsTalking;
		}
	}
	return false;
}

bool
VoiceChatDurango::IsMuted(u64 uniqueId) const
{
	// check local user first
	if(m_LocalInfo.IsUniqueIdMatch(uniqueId))
		return m_LocalInfo.m_IsMuted;

	// run through all remote users
	for(int j = 0; j < MAX_REMOTE_GAMERS; ++j)
	{
		if(m_RemoteChatUserInfos[j].IsValid() && m_RemoteChatUserInfos[j].GetUniqueId() == uniqueId)
		{
			return m_RemoteChatUserInfos[j].m_IsMuted;
		}
	}
	return false;
}

bool
VoiceChatDurango::IsBlocked(u64 uniqueId) const
{
	// check local user first
	if(m_LocalInfo.IsUniqueIdMatch(uniqueId))
		return m_LocalInfo.m_IsBlocked;

	// run through all remote users
	for(int j = 0; j < MAX_REMOTE_GAMERS; ++j)
	{
		if(m_RemoteChatUserInfos[j].IsValid() && m_RemoteChatUserInfos[j].GetUniqueId() == uniqueId)
		{
			return m_RemoteChatUserInfos[j].m_IsBlocked;
		}
	}
	return false;
}

bool
VoiceChatDurango::MicHasFocus() const
{
	try
	{
		return chatIntegrationLayerPtr->HasMicFocus();
	}
	catch (Platform::Exception^ e)
	{
		VC_EXCEPTION(e);
	}
	return false;
}

float
VoiceChatDurango::GetMicLoudness() const
{
	try
	{
		return m_LocalInfo.m_IsTalking ? chatIntegrationLayerPtr->GetMicLoudness() : 0.0f;
	}
	catch (Platform::Exception^ e)
	{
		VC_EXCEPTION(e);
	}
	return false;
}

void
VoiceChatDurango::OnXblEvent(rlXbl* /*xbl*/, const rlXblEvent* evt)
{
	const unsigned evtId = evt->GetId();

	if(RLXBL_PRESENCE_EVENT_SIGNIN_STATUS_CHANGED == evtId)
	{
		vcDebug("VoiceChatDurango::OnXblEvent(RLXBL_PRESENCE_EVENT_SIGNIN_STATUS_CHANGED)");

		rlXblPresenceEventSigninStatusChanged* msg = (rlXblPresenceEventSigninStatusChanged*)evt;

		if(msg->IsSignedIn())
		{
			vcDebug("\t Signed In");
			VoiceChatDurangoSingleton::GetInstance().FlagHeadsetRefresh(msg->GetLocalGamerIndex());
		}
	}
}

void 
VoiceChatDurango::RefreshHeadsetState()
{
	// run through all local users
	for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
		RefreshHeadset(i);
	
	// hard reset
	m_HeadsetRefreshMask = 0;
}

void 
VoiceChatDurango::FlagHeadsetRefresh(int localIndex)
{
	vcDebug("FlagHeadsetRefresh :: For local index %d", localIndex);
	m_HeadsetRefreshMask |= (1 << localIndex);
}

void 
VoiceChatDurango::RefreshHeadset(int localIndex)
{
	vcDebug("RefreshHeadset :: For local index %d", localIndex);
	
	// reset user mask
	m_HeadsetRefreshMask &= ~(1 << localIndex);

	// mark as false
	bool bHasHeadset = false;
	bool bHasSharedAudioDevice = false;
	
	User^ localXboxUser = rlXblInternal::GetInstance()->_GetPresenceManager()->GetXboxUser(localIndex);
	if(localXboxUser == nullptr)
		return;

	try
	{
		IVectorView<IAudioDeviceInfo^>^ audioDevices = localXboxUser->AudioDevices;

		vcDebug("RefreshHeadset :: Found %u devices", audioDevices->Size);

		for (unsigned i = 0; i < audioDevices->Size; i++)
		{
			Windows::Xbox::System::IAudioDeviceInfo^ device = audioDevices->GetAt(i);
			if(device != nullptr)
			{
				vcDebug("RefreshHeadset - ID: %ls, Category: %u, Type: %u, Sharing: %u", device->Id->Data(), device->DeviceCategory, device->DeviceType, device->Sharing);

				if(device->DeviceCategory == Windows::Xbox::System::AudioDeviceCategory::Communications)
				{
					bool isHeadset = (device->Sharing != Windows::Xbox::System::AudioDeviceSharing::Shared);
					bool isSharedAudio = (device->Sharing == Windows::Xbox::System::AudioDeviceSharing::Shared);

					vcDebug("\tHeadset: %s, SharedAudio: %s", isHeadset ? "True" : "False", isSharedAudio ? "True" : "False");

					bHasHeadset |= isHeadset;
					bHasSharedAudioDevice |= isSharedAudio;

#if __NO_OUTPUT
					// we can drop out early if we already have both set (keep looping for output builds)
					if(bHasHeadset && bHasSharedAudioDevice)
						break;
#endif
				}
			}
		}
	}
	catch (Platform::COMException^ e)
	{
		VC_EXCEPTION(e);
	}

	// update settings
	SetHasHeadset(localIndex, bHasHeadset);
	SetHasSharedAudioDevice(localIndex, bHasSharedAudioDevice);
}

void 
VoiceChatDurango::SetHasHeadset(int localIndex, bool hasHeadset) 
{ 
	if(hasHeadset != m_bHasHeadset[localIndex])
	{
		vcDebug("SetHasHeadset :: Headset %s", hasHeadset ? "Found" : "Not Found");
		m_bHasHeadset[localIndex] = hasHeadset; 
	}
}

void 
VoiceChatDurango::SetHasSharedAudioDevice(int localIndex, bool hasSharedAudio) 
{ 
	bool bCanUseKinect = false;
	try
	{
		Windows::Kinect::KinectSensor^ rKinect = Windows::Kinect::KinectSensor::GetDefault();
		bool bGamechatCapable = (static_cast<unsigned>(rKinect->KinectCapabilities) & static_cast<unsigned>(Windows::Kinect::KinectCapabilities::Gamechat)) != 0;
		bCanUseKinect = rKinect->IsAvailable && bGamechatCapable;

		vcDebug("SetHasSharedAudioDevice :: Shared Audio %s, Kinect Available: %s, Kinect Open: %s, Kinect Capabilities: %u, Gamechat: %s, CanUseKinect: %s", 
				hasSharedAudio ? "Found" : "Not Found",
				rKinect->IsAvailable ? "True" : "False",
				rKinect->IsOpen ? "True" : "False",
				rKinect->KinectCapabilities,
				bGamechatCapable ? "True" : "False",
				bCanUseKinect ? "True" : "False");
	}
	catch (Platform::Exception^ e)
	{
		vcError("VoiceChatDurango::SetHasSharedAudioDevice - Exception thrown");
		RL_EXCEPTION(e);
	}

	// check if we have a Kinect attached
	if(hasSharedAudio && bCanUseKinect)
	{
		vcDebug("SetHasSharedAudioDevice :: Kinect Available");
		m_bHasSharedAudioDevice[localIndex] = hasSharedAudio; 
	}
	else
	{
		vcDebug("SetHasSharedAudioDevice :: Kinect Not Available. Ignoring shared audio.");
		m_bHasSharedAudioDevice[localIndex] = false; 
	}
}

bool
VoiceChatDurango::PackChat(u64 uniqueTalkerId, void* dst, unsigned* numBytes)
{
	// TODO: Test multiple signed in users on one machine submitting packets in the same frame.

	bool success = false;

#if DISPLAY_DEBUG_VOICE_CHAT
	static int packetCounter = 0;
	int numConcatenatedPackets = 0;
#endif // DISPLAY_DEBUG_VOICE_CHAT

	sysCriticalSection cs(chatIntegrationLayerPtr->GetChatPacketVectorCSToken());
	IVector<ChatPacketEventArgs^>^ pendingOutgoingChatPackets = chatIntegrationLayerPtr->GetPendingOutgoingChatPackets();

	bool isSignedIn = false;
	for(int i = 0; i < RL_MAX_LOCAL_GAMERS && !isSignedIn; ++i)
	{
		isSignedIn = rlPresence::IsSignedIn(i);
	}

	if ((isSignedIn == false) || !pendingOutgoingChatPackets || pendingOutgoingChatPackets->Size == 0)
	{
		return false;
	}

	unsigned int dstSize = *numBytes;
	*numBytes = 0;

	u8* dstBytes = (u8*)dst;

	// Loop through packets and add size info to header for valid packets
	for (unsigned int i = 0; i < pendingOutgoingChatPackets->Size; ++i)
	{
		ChatPacketEventArgs^ currentChatPacketEventArgs = pendingOutgoingChatPackets->GetAt(i);
		
		u64 currentPacketUserId = 0;
		if (currentChatPacketEventArgs && currentChatPacketEventArgs->ChatUser)
		{
			swscanf_s(currentChatPacketEventArgs->ChatUser->XboxUserId->Begin(), L"%llu", &currentPacketUserId);
		}

		if (currentChatPacketEventArgs->ChatMessageType == Microsoft::Xbox::GameChat::ChatMessageType::UserAddedMessage)
		{
			vcAssert(false, "VoiceChatDurango::PackChat() - Packet index %d is adding a user.", i);
		}
		else if (currentChatPacketEventArgs->ChatMessageType == Microsoft::Xbox::GameChat::ChatMessageType::UserRemovedMessage)
		{
			vcAssert(false, "VoiceChatDurango::PackChat() - Packet index %d is removing a user.", i);
		}
		else if (currentChatPacketEventArgs->ChatMessageType == Microsoft::Xbox::GameChat::ChatMessageType::InvalidMessage)
		{
			vcDebug("VoiceChatDurango::PackChat() - Packet index %d is invalid.", i);
			continue;
		}

#if ENABLE_USER_CHECKS_WHEN_PACKING_CHAT
		if (currentPacketUserId == uniqueTalkerId)
#else
		uniqueTalkerId;	// avoid compiler error for unused var
#endif // ENABLE_USER_CHECKS_WHEN_PACKING_CHAT
		{
			unsigned int sourceBufferLength = currentChatPacketEventArgs->PacketBuffer->Length;

			if (sourceBufferLength == 0)
			{
				// Ignore empty packets
				//vcDebug("VoiceChatDurango::PackChat() - Empty packet ignored.");
			}
			else if ((sourceBufferLength + PACKET_PAYLOAD_LENGTH_SIZE) < dstSize)
			{
				// Add 2 bytes to the header to specify the size of each packet.
				*((PacketPayloadLengthType*)dstBytes) = static_cast<PacketPayloadLengthType>(sourceBufferLength);
				dstBytes += PACKET_PAYLOAD_LENGTH_SIZE;
				dstSize -= PACKET_PAYLOAD_LENGTH_SIZE;
				(*numBytes) += PACKET_PAYLOAD_LENGTH_SIZE;
			}
			else
			{
				vcWarning("VoiceChatDurango::PackChat() - Not enough space for header info.");
			}
		}
	}

	if (*numBytes == 0)
	{
		// Early out if we have no data to send
		pendingOutgoingChatPackets->Clear();
		return false;
	}

	// Add a guard to separate the header and payload
	if (PACKET_GUARD_SIZE < dstSize)
	{
		// Add a guard to specify the end of the header.
		*((PacketGuardType*)dstBytes) = PACKET_GUARD_VALUE;
		dstBytes += sizeof(PACKET_GUARD_SIZE);
		dstSize -= sizeof(PACKET_GUARD_SIZE);
		(*numBytes) += sizeof(PACKET_GUARD_SIZE);
	}
	else
	{
		// Early out if we can't fit the guard in
		vcWarning("VoiceChatDurango::PackChat() - Not enough space for guard");
		pendingOutgoingChatPackets->Clear();
		return false;
	}

#if DISPLAY_DEBUG_VOICE_CHAT
	if (PACKET_NUMBER_CONCATENATED_SIZE < dstSize)
	{
		// Add an int specifying the number of packets being concatenated.
		numConcatenatedPackets = (*numBytes - PACKET_GUARD_SIZE) / PACKET_PAYLOAD_LENGTH_SIZE;
		*((PacketNumberConcatenatedType*)dstBytes) = numConcatenatedPackets;
		dstBytes += PACKET_NUMBER_CONCATENATED_SIZE;
		dstSize -= PACKET_NUMBER_CONCATENATED_SIZE;
		(*numBytes) += PACKET_NUMBER_CONCATENATED_SIZE;	
		Displayf("Num Concatenated Packets: %d", numConcatenatedPackets);
	}
	else
	{
		Assertf(false, "VoiceChatDurango::PackChat() - Not enough space for number of concatenated packets");
	}
#endif // DISPLAY_DEBUG_VOICE_CHAT

	// Loop through valid packets and add payload data
	for (unsigned int i = 0; i < pendingOutgoingChatPackets->Size; ++i)
	{
		ChatPacketEventArgs^ currentChatPacketEventArgs = pendingOutgoingChatPackets->GetAt(i);

		u64 currentPacketUserId = 0;
		if (currentChatPacketEventArgs->ChatUser)
		{
			swscanf_s(currentChatPacketEventArgs->ChatUser->XboxUserId->Begin(), L"%llu", &currentPacketUserId);
		}

#if ENABLE_USER_CHECKS_WHEN_PACKING_CHAT
		if (currentPacketUserId == uniqueTalkerId)
#endif // ENABLE_USER_CHECKS_WHEN_PACKING_CHAT
		{
			if (currentChatPacketEventArgs->ChatMessageType == Microsoft::Xbox::GameChat::ChatMessageType::InvalidMessage)
			{
				continue;
			}

			byte* packetBufferBytes = nullptr;
			unsigned int sourceBufferLength = currentChatPacketEventArgs->PacketBuffer->Length;
			//Assert(dstSize > currentChatPacketEventArgs->PacketBuffer->Length);

			if (sourceBufferLength == 0)
			{
				// Ignore empty packets
			}
			else if (sourceBufferLength < dstSize)
			{
#if DISPLAY_DEBUG_VOICE_CHAT
				if (PACKET_SEQUENCE_NUMBER_SIZE < dstSize)
				{
					// Add a sequence # to make sure we are receiving packets in the correct order.
					*((PacketSequenceNumberType*)dstBytes) = packetCounter;
					dstBytes += PACKET_SEQUENCE_NUMBER_SIZE;
					dstSize -= PACKET_SEQUENCE_NUMBER_SIZE;
					(*numBytes) += PACKET_SEQUENCE_NUMBER_SIZE;
				}
				else
				{
					Assertf(false, "VoiceChatDurango::PackChat() - Not enough space for sequence number");
				}
#endif // DISPLAY_DEBUG_VOICE_CHAT
				
				ChatIntegrationLayerUtils::GetBufferBytes(currentChatPacketEventArgs->PacketBuffer, &packetBufferBytes);
				errno_t err = memcpy_s( dstBytes, dstSize, packetBufferBytes, sourceBufferLength );

				if (err != 0)
				{
					Assertf( false, "VoiceChatDurango::PackChat() - memcpy_s failure");
				}

#if DISPLAY_DEBUG_VOICE_CHAT
				{
					static char debugSendPacketString[4096];
					static char debugTempString[1024];
					memset(debugSendPacketString, 0, 4096);
					memset(debugTempString, 0, 1024);

					for(unsigned int i = 0; i < sourceBufferLength; ++i)
					{
						_itoa(dstBytes[i], debugTempString, 10);
						strcat(debugSendPacketString, debugTempString);
						strcat(debugSendPacketString, ".");
					}

					Displayf("Send Packet: %d: %s", packetCounter, debugSendPacketString);
					++packetCounter;
				}
#endif // DISPLAY_DEBUG_VOICE_CHAT

				dstBytes += sourceBufferLength;
				dstSize -= sourceBufferLength;
				(*numBytes) = (*numBytes) + sourceBufferLength;

				success = true;
			}
			else
			{
				vcWarning("VoiceChatDurango::PackChat() - Not enough space for payload. Stopping at index %d", i);
				success = false;
				break;
			}
		}
	}

	pendingOutgoingChatPackets->Clear();

	return success;
}

void
VoiceChatDurango::SubmitChat(u64 remoteXuid, const void* data, const unsigned sizeOfData)
{
	u8* pCurrentHeaderPosition = (u8*)data;
	u8* pHeaderEndPosition = NULL;

	u8* pPayloadStartPosition = (u8*)data;
	u8* pCurrentPayloadPosition = NULL;
	u32 packetSequenceNumber = 0;

	// Split concatenated buffers and handle them one at a time
	// 1) First loop handles full packets concatenated due to the send delay threshold in voicechat.cpp not being reached.
	// 2) Second loop handles sub packets concatenated due to having multiple packets packed in the same update.
	do 
	{
		// Move past the packet size header data
		while (*((PacketGuardType*)pPayloadStartPosition) != PACKET_GUARD_VALUE &&
			   (pPayloadStartPosition - (u8*)data) < sizeOfData)
		{
			pPayloadStartPosition += PACKET_PAYLOAD_LENGTH_SIZE;
		}

		if ((pPayloadStartPosition - (u8*)data) >= sizeOfData)
		{
			Assertf(false, "VoiceChatDurango::SubmitChat() - Bad packet");
			return;
		}
		else if (*((PacketGuardType*)pPayloadStartPosition) == PACKET_GUARD_VALUE)
		{
			pHeaderEndPosition = pPayloadStartPosition;
		
			// Move past the guard
			pPayloadStartPosition += PACKET_GUARD_SIZE;

#if DISPLAY_DEBUG_VOICE_CHAT
			// Move past the concatenated packets number
			int numConcatenatedPackets = *((u32*)pPayloadStartPosition);
			pPayloadStartPosition += sizeof(u32);
			Displayf("Num Concatenated Packets: %d", numConcatenatedPackets);
#endif // DISPLAY_DEBUG_VOICE_CHAT
		}
		else
		{
			Assertf(false, "VoiceChatDurango::SubmitChat() - Unhandled error");
			return;
		}

		pCurrentPayloadPosition = pPayloadStartPosition;
		Platform::Object^ uniqueId = Windows::Foundation::PropertyValue::CreateUInt64(remoteXuid);
		 
		while (pCurrentHeaderPosition < pHeaderEndPosition)
		{
			u32 packetSize = *((PacketPayloadLengthType*)pCurrentHeaderPosition);

#if DISPLAY_DEBUG_VOICE_CHAT
			// Store sequence number and move past it
			packetSequenceNumber = *((PacketSequenceNumberType*)pCurrentPayloadPosition);
			pCurrentPayloadPosition += PACKET_SEQUENCE_NUMBER_SIZE;
#endif // DISPLAY_DEBUG_VOICE_CHAT

			// Handle duplicate messages to add/remove remote users, as well as voice messages from remote users that have not been added yet.
			ChatMessageType messageType = (ChatMessageType)(*((u8*)pCurrentPayloadPosition));
			bool shouldKeepMessage = true;

			if (messageType != ChatMessageType::ChatVoiceDataMessage)
			{
				vcWarning("VoiceChatDurango::SubmitChat - Received invalid message");
			}

			Windows::Storage::Streams::IBuffer^ destBuffer = ref new Windows::Storage::Streams::Buffer( packetSize );
			byte* destBufferBytes = nullptr;
			ChatIntegrationLayerUtils::GetBufferBytes( destBuffer, &destBufferBytes );
			Assert(destBuffer->Capacity >= packetSize);
			errno_t err = memcpy_s( destBufferBytes, destBuffer->Capacity, pCurrentPayloadPosition, packetSize );
			if (err != 0)
			{
				Assertf(false, "VoiceChatDurango::SubmitChat() - memcpy_s failed");
			}

			destBuffer->Length = packetSize;

#if DISPLAY_DEBUG_VOICE_CHAT
			{
				static char debugReceivePacketString[4096];
				static char debugTempString[1024];		
				memset(debugReceivePacketString, 0, 4096);
				memset(debugTempString, 0, 1024);

				for(unsigned int i = 0; i < packetSize; ++i)
				{
					_itoa(pCurrentPayloadPosition[i], debugTempString, 10);
					strcat(debugReceivePacketString, debugTempString);
					strcat(debugReceivePacketString, ".");
				}
				Displayf("Receive Packet: %d: %s", packetSequenceNumber, debugReceivePacketString);
			}
#endif // DISPLAY_DEBUG_VOICE_CHAT

			if (packetSize == 0)
			{
				vcDebug("VoiceChatDurango::SubmitChat() - Empty packet received.");
				shouldKeepMessage = false;
			}

			if (shouldKeepMessage)
			{
				chatIntegrationLayerPtr->OnIncomingChatMessage(destBuffer, uniqueId);
			}

			pCurrentHeaderPosition += PACKET_PAYLOAD_LENGTH_SIZE;
			pCurrentPayloadPosition += packetSize;
		}

		pCurrentHeaderPosition = pCurrentPayloadPosition;
		pHeaderEndPosition = NULL;

		pPayloadStartPosition = pCurrentPayloadPosition;
		packetSequenceNumber = 0;
	}
	while((pCurrentPayloadPosition - (u8*)data) < sizeOfData);
}

void
VoiceChatDurango::ReceiveMessage(const netEventFrameReceived* fr)
{
	unsigned msgId;
	if (netMessage::GetId(&msgId, fr->m_Payload, fr->m_SizeofPayload))
	{
		if (msgId == VoiceChatUserMsg::MSG_ID())
		{
			VoiceChatUserMsg msg;

			if (msg.Import(fr->m_Payload, fr->m_SizeofPayload))
			{
				u64 userId = msg.m_UniqueUserId;
				Platform::Object^ uniqueRemoteUserId = Windows::Foundation::PropertyValue::CreateUInt64(userId);

				byte* bufferBytes = msg.m_BufferBytes;
				u32 bufferSize = msg.m_BufferSize;

				if (bufferSize > 0)
				{
					Windows::Storage::Streams::IBuffer^ destBuffer = ref new Windows::Storage::Streams::Buffer( bufferSize );
					byte* destBufferBytes = nullptr;
					ChatIntegrationLayerUtils::GetBufferBytes( destBuffer, &destBufferBytes );
					Assert(destBuffer->Capacity >= bufferSize);
					errno_t err = memcpy_s( destBufferBytes, destBuffer->Capacity, bufferBytes, bufferSize );
					if (err != 0)
					{
						Assertf(false, "VoiceChatDurango::ReceiveMessage() - memcpy_s failed");
					}

					destBuffer->Length = bufferSize;

					vcDebug("VoiceChatDurango::ReceiveMessage - Message from userId: %llu", userId);
					chatIntegrationLayerPtr->OnIncomingChatMessage(destBuffer, uniqueRemoteUserId);
				}
			}
		}
	}
}

void
VoiceChatDurango::Update()
{
	// run through all local users
	for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
	{
		if((m_HeadsetRefreshMask & (1 << i)) != 0)
			RefreshHeadset(i);

		m_HeadsetRefreshMask = 0;
	}
	
	// update local user and then run through all remote users
	UpdateChatState(&m_LocalInfo);
	for(int j = 0; j < MAX_REMOTE_GAMERS; ++j)
	{
		UpdateChatState(&m_RemoteChatUserInfos[j]);
	}

	if(vcVerify(chatIntegrationLayerPtr != nullptr))
	{
		chatIntegrationLayerPtr->ProcessPendingChatUserUpdates();
		chatIntegrationLayerPtr->ProcessActiveAsyncOperations();
	}
}

#if !__NO_OUTPUT
const char* GetUserMessageTypeAsString(const Microsoft::Xbox::GameChat::ChatMessageType messageType)
{
	switch(messageType)
	{
	case Microsoft::Xbox::GameChat::ChatMessageType::ChatVoiceDataMessage: return "ChatVoiceDataMessage";
	case Microsoft::Xbox::GameChat::ChatMessageType::UserAddedMessage: return "UserAddedMessage";
	case Microsoft::Xbox::GameChat::ChatMessageType::UserRemovedMessage: return "UserRemovedMessage";
	case Microsoft::Xbox::GameChat::ChatMessageType::InvalidMessage: return "InvalidMessage";
	default: return "NotSupported";
	}
}
#endif

void 
VoiceChatDurango::SendUserPackets(u64 userId)
{
	sysCriticalSection cs(chatIntegrationLayerPtr->GetUserPacketVectorCSToken());

	IVector<ChatPacketEventArgs^>^ pendingOutgoingUserPackets = chatIntegrationLayerPtr->GetPendingOutgoingUserPackets();

	bool isSignedIn = false;
	for(int i = 0; i < RL_MAX_LOCAL_GAMERS && !isSignedIn; ++i)
	{
		isSignedIn = rlPresence::IsSignedIn(i);
	}

	if ((isSignedIn == false) || !pendingOutgoingUserPackets || pendingOutgoingUserPackets->Size == 0)
	{
		return;
	}

	// Note: The size of the vector can change each iteration, which is why we query for the size each time
	//        and why we don't increment i on each iteration.
	for (unsigned int i = 0; i < pendingOutgoingUserPackets->Size; )
	{
		bool sendResult = true;
		ChatPacketEventArgs^ currentChatPacketEventArgs = pendingOutgoingUserPackets->GetAt(i);

		// Extract buffer bytes

		byte* packetBufferBytes = nullptr;
		unsigned int packetBufferLength = currentChatPacketEventArgs->PacketBuffer->Length;

		if (packetBufferLength == 0)
		{
			pendingOutgoingUserPackets->RemoveAt(i);
			continue;
		}

		u64 targetUniqueId = 0;
		if (currentChatPacketEventArgs->UniqueTargetConsoleIdentifier != nullptr)
		{
			targetUniqueId = safe_cast<Windows::Foundation::IPropertyValue^>(currentChatPacketEventArgs->UniqueTargetConsoleIdentifier)->GetUInt64();
		}

		if (m_CxnMgr)
		{
			ChatIntegrationLayerUtils::GetBufferBytes(currentChatPacketEventArgs->PacketBuffer, &packetBufferBytes);
			VoiceChatUserMsg msg(userId, packetBufferBytes, packetBufferLength);

			int numPlayerSendAttempts = 0;

			for(int j = 0; j < MAX_REMOTE_GAMERS; ++j)
			{
				if(!m_RemoteChatUserInfos[j].IsValid())
				{
					continue;
				}

				if(m_RemoteChatUserInfos[j].IsPendingRemove())
				{
					continue;
				}

				int connectionId = m_CxnMgr->GetAnyConnectionWithEndpointId(m_RemoteChatUserInfos[j].GetEndpointId());
				if (connectionId < 0)
				{
					vcWarning("VoiceChatDurango::SendUserPackets - FAILED to send %s to: XUID: %llu, endpointId: %u (connection is not established)", GetUserMessageTypeAsString(currentChatPacketEventArgs->ChatMessageType), m_RemoteChatUserInfos[j].GetUniqueId(), m_RemoteChatUserInfos[j].GetEndpointId());
					continue; 
				}

				bool shouldSend = currentChatPacketEventArgs->SendPacketToAllConnectedConsoles;
				if (!shouldSend)
				{				
					if (m_RemoteChatUserInfos[j].IsUniqueIdMatch(targetUniqueId))
					{
						shouldSend = true;
					}
				}

				if (shouldSend)
				{
					bool isSuccess = m_CxnMgr->Send(connectionId,
													msg,
													NET_SEND_RELIABLE,
													NULL);

					++numPlayerSendAttempts;
#if !__NO_OUTPUT
					if (isSuccess)
					{
						vcDebug("VoiceChatDurango::SendUserPackets - Sent %s to: XUID: %llu, endpointId: %u", GetUserMessageTypeAsString(currentChatPacketEventArgs->ChatMessageType), m_RemoteChatUserInfos[j].GetUniqueId(), m_RemoteChatUserInfos[j].GetEndpointId());
					}
					else
					{
						vcDebug("VoiceChatDurango::SendUserPackets - FAILED to send %s to: XUID: %llu, endpointId: %u", GetUserMessageTypeAsString(currentChatPacketEventArgs->ChatMessageType), m_RemoteChatUserInfos[j].GetUniqueId(), m_RemoteChatUserInfos[j].GetEndpointId());
					}
#endif // !__NO_OUTPUT

					sendResult = sendResult && isSuccess;
				}
			}

			if (numPlayerSendAttempts == 0)
			{
				if (currentChatPacketEventArgs->ChatMessageType == Microsoft::Xbox::GameChat::ChatMessageType::UserAddedMessage)
				{
					if (currentChatPacketEventArgs->SendPacketToAllConnectedConsoles)
					{
						vcDebug("VoiceChatDurango::SendUserPackets - Could not find anyone to broadcast %s packet to.", GetUserMessageTypeAsString(currentChatPacketEventArgs->ChatMessageType));
					}
					else
					{
						u32* timeOfInitialAttempt = m_InitialUserPacketSendAttemptTime.Access(targetUniqueId);

						if (timeOfInitialAttempt != NULL)
						{
							bool isPendingRemove = false;
							for(int j = 0; j < MAX_REMOTE_GAMERS; ++j)
							{
								if(m_RemoteChatUserInfos[j].IsValid() && m_RemoteChatUserInfos[j].IsUniqueIdMatch(targetUniqueId) && m_RemoteChatUserInfos[j].IsPendingRemove())
								{
									// we do not want to remove this for anyone pending remove - these players might be added back in within this
									// time limit and will not receive a UserAddedPacket if this is not kept around
									// if they are removed and then added, we'll generate a new packet when they are added again
									isPendingRemove = true; 
									sendResult = false;
									break;
								}
							}

							// if not pending remove, use timeout
							if(!isPendingRemove)
							{
								const u32 MAX_ATTEMPT_MS_TIME = 10000;
								if (rage::sysTimer::GetSystemMsTime() - *timeOfInitialAttempt < MAX_ATTEMPT_MS_TIME)
								{
									vcDebug("VoiceChatDurango::SendUserPackets - Removing pending %s packet to %llu.", GetUserMessageTypeAsString(currentChatPacketEventArgs->ChatMessageType), targetUniqueId);
									sendResult = false;
								}
							}
						}
						else
						{
							vcDebug("VoiceChatDurango::SendUserPackets - Could not find target %llu to send %s packet to.", targetUniqueId, GetUserMessageTypeAsString(currentChatPacketEventArgs->ChatMessageType));
							m_InitialUserPacketSendAttemptTime.Insert(targetUniqueId, rage::sysTimer::GetSystemMsTime());
							sendResult = false;
						}
					}
				}
			}

			if (sendResult)
			{
				if (m_InitialUserPacketSendAttemptTime.Access(targetUniqueId))
				{
					m_InitialUserPacketSendAttemptTime.Delete(targetUniqueId);
				}

				pendingOutgoingUserPackets->RemoveAt(i);
				continue;
			}
		}

		++i;
	}
}

}   //namespace rage

#endif //RSG_DURANGO
