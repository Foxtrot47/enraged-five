// 
// avchat/voicechat.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNET_AVCHAT_H
#define SNET_AVCHAT_H

#include "voicechattypes.h"
#include "file/file_config.h"
#include "net/connectionmanager.h"
#include "rline/rlgamerinfo.h"
#include "atl/delegate.h"

// avchat/makefile.txt must match this.
#define USE_SPEEX			(0 && RSG_NP)
#define USE_LIBVOICE		(1 && RSG_NP)

#if RSG_NP
#if RSG_ORBIS
#include "atl/singleton.h"
#endif // RSG_ORBIS

#if USE_SPEEX
extern "C"{
    struct SpeexBits;
    struct SpeexPreprocessState;
    struct JitterBuffer_;

    typedef struct JitterBuffer_ JitterBuffer;
}
#endif
#elif RSG_PC
#include "atl/array.h"
#include "voice/rv.h"
#endif // RSG_NP

/*
    This module provides network voice chat features.

    To use voice chat initialize an instance of VoiceChat with
    the expected number of local and remote talkers, an instance
    of a connection manager that will be used for network communication,
    and the id of the network channel to use for voice communication.

    By default when a local talker speaks into a mic the resulting
    voice data is broadcast to all remote talkers.

    By default when a voice packet is received from a remote talker the
    voice is played for all local talkers, except for those who have
    the remote talker muted.

    On Xbox XAudio2 must be initialized before attempting to use voice chat.
*/

namespace rage
{
#if __BANK
class bkSlider;
#endif  //__BANK

class VoiceChatAudioProvider
{
public:
	virtual ~VoiceChatAudioProvider(){}

	virtual void LocalPlayerAudioReceived(const s16 *data, const u32 lengthSamples) = 0;
	virtual void NotifyRemotePlayerAdded(const rlGamerId &gamerId) = 0;
	virtual void NotifyRemotePlayerRemoved(const rlGamerId &gamerId) = 0;
	virtual void RemotePlayerAudioReceived(const s16 *data, const u32 lengthSamples, const rlGamerId &gamerId) = 0;
	virtual void RemoveAllRemotePlayers() = 0;
};


#if USE_SPEEX
//PURPOSE
//  A ring buffer that is thread safe for single reader, single writer
//  situations, i.e. where one thread is a reader and another thread is the
//  writer.
class datRingBuffer
{
public:

    datRingBuffer();

    //PURPOSE
    //  Initialize the ring buffer.
    //PARAMS
    //  buf         - The buffer.
    //  sizeofBuf   - Size of the buffer in bytes.
    void Init(void* buf, const unsigned sizeofBuf);

    //PURPOSE
    //  Shuts down the ring buffer.
    void Shutdown();

    //PURPOSE
    //  Clears the ring buffer.
    //NOTES
    //  This operation is *not* threadsafe.  Do not call this function
    //  if there are other threads currently reading/writing the buffer.
    void Clear();

    //PURPOSE
    //  Reads from the ring buffer.
    //PARAMS
    //  dst     - Destination buffer.
    //  len     - Number of bytes to read.
    //RETURNS
    //  Number of bytes actually read, which can be less than the
    //  number requested.
    unsigned Read(void* dst, const unsigned len);

    //PURPOSE
    //  Writes to the ring buffer.
    //PARAMS
    //  src     - Destination buffer.
    //  len     - Number of bytes to write.
    //RETURNS
    //  Number of bytes actually written, which can be less than the
    //  number requested.
    unsigned Write(const void* src, const unsigned len);

    //PURPOSE
    //  Returns the total capacity of the ring buffer.
    unsigned GetCapacity() const;

    //PURPOSE
    //  Returns the number bytes that can be read.
    unsigned GetSize() const;

private:

    u8* m_Buf;
    unsigned m_Capacity;
    volatile unsigned m_Buflen;
    unsigned m_WritePos;
    unsigned m_ReadPos;
};
#endif

class VoiceChat
{
public:

    enum
    {
        //We send voice data at most once every DEFAULT_MIN_SEND_INTERVAL ms.
        DEFAULT_MIN_SEND_INTERVAL = 100
    };

    enum
    {
        //Default threshold for voice proximity detection (VPD).
        //Values are in the range 0-10, 10 means the speaker must be
        //very close to the mic.
        DEFAULT_VPD_THRESHOLD   = 8,
        MAX_VPD_THRESHOLD       = 10,
		DEFAULT_VPD_HOLD_TIME_MS = 1000,
		MAX_VPD_HOLD_TIME_MS = 5000
    };

	enum 
	{
		MAX_PACKET_SIZE = netFrame::MAX_BYTE_SIZEOF_PAYLOAD,
	};

    //Talker flags are combined in the flags parameter passed to AddTalker().
    enum TalkerFlags
    {
        //Send/Receive flags allow one-way chat capabilities.

		TALKER_FLAG_NONE			= 0x0,
        //Indicates we will send voice packets to the talker.
        TALKER_FLAG_SEND_VOICE      = 0x01,
        //Indicates we will receive packets from the talker.
        TALKER_FLAG_RECEIVE_VOICE   = 0x02,
        //Indicates we will send and receive packets from the talker.
        TALKER_FLAG_SEND_RECEIVE_VOICE  =
            TALKER_FLAG_SEND_VOICE | TALKER_FLAG_RECEIVE_VOICE
    };

    VoiceChat();

    ~VoiceChat();

	//PURPOSE
	//  Initialize the core voice chat systems. Called at game start.
    //  The core system typically involves initializing the microphone
    //  and speakers.
    //  No networking dependencies occur at this point.
    //  After calling Init() InitNetwork() must be called to initialize a
    //  fully functioning voice chat system.
	//PARAMS
	//  maxLocalTalkers     - Maximum number of local talkers.
	//  maxRemoteTalkers    - Maximum number of remote talkers.
	//  audioDevice         - Audio device required for voice chat.
	//                        On Xbox this is a IXAudio2 object.
	//  cpuAffinity         - CPU on which worker thread will run.
	bool Init(const unsigned maxLocalTalkers,
              const unsigned maxRemoteTalkers,
              void* audioDevice,
              const int cpuAffinity);

	//PURPOSE
	//  Shuts down the core voice chat systems. Called on game shutdown.
	void Shutdown();

	//PURPOSE
	//  Initialize the voice chat worker thread
	//PARAMS
	//  cpuAffinity - CPU on which worker thread will run.
	bool InitWorkerThread(const int cpuAffinity); 

	//PURPOSE
	//  Shuts down the voice chat worker thread
	void ShutdownWorkerThread(); 

    //PURPOSE
    //  Initialize the voice chat system for networking.
    //  InitCore() *must* be called prior to calling InitNetwork().
    //PARAMS
    //  cxnMgr			- Connection manager instance.
    //  channelId		- Id of network channel used for voice.
    bool InitNetwork(netConnectionManager* cxnMgr,
                     const unsigned channelId);

    //PURPOSE
    //  Shuts down the voice chat system for networking.
    void ShutdownNetwork();

#if __BANK
	//PURPOSE
	//  Initialise and shutdown bank widgets
	void InitWidgets();
	void ShutdownWidgets();
#endif

	//PURPOSE
	//  Returns true if voice chat is initialized.
	bool IsInitialized() const;

    //PURPOSE
    //  Returns true if voice chat is initialized for networking.
    bool IsInitializedNetwork() const;

    //PURPOSE
    //  Sets the millisecond interval with which we collect and
    //  send voice from local talkers to remote talkers.
    //  This should be tuned to minimize bandwidth while maintaining
    //  quality.
    //PARAMS
    //  interval    - Millisecond interval on which we collect
    //                and send voice from local talkers.
    void SetSendInterval(const unsigned interval);

    //PURPOSE
    //  Returns the millisecond interval on which we collect
    //  and send voice from local talkers.
    unsigned GetSendInterval() const;

    //PURPOSE
    //  Sets the threshold value for voice proximity detection (VPD),
    //  or how close a talker must be to the mic in order to register.
    //PARAMS
    //  threshold   - Value from 0 to 10.
    //                0 will pick up voices that are far from the mic,
    //                10 will only pick up voices that are very near the mic.
    //NOTES
    //  Has not effect on Xenon.
    void SetVpdThreshold(const unsigned threshold);

	//PURPOSE
	// Sets the threshold value for VPD when push-to-talk is enabled
	// Generally this value should be lower than the standard Vpd
	// value as the user is making a conscious decision to PTT
	// PARAMS
	//  threshold   - Value from 0 to 10.
	//                0 will pick up voices that are far from the mic,
	//                10 will only pick up voices that are very near the mic.
    void SetVpdThresholdPTT(const unsigned threshold);

    //PURPOSE
    //  Returns the threshold value for voice proximity detection (VPD).
    unsigned GetVpdThreshold() const;

	//PURPOSE
	//  Returns the threshold value for voice proximity detection (VPD) in PushToTalk mode
	unsigned GetVpdThresholdPTT() const;

	//PURPOSE
	// Sets the capture hold time for VPD capture
	void SetCaptureHoldTimeMs(const u32 holdTimeMs);

	//PURPOSE
	// Returns the capture hold time 
	u32 GetCaptureHoldTimeMs() const;

    //PURPOSE
    //  Adds a talker to the current group of talkers.
    //PARAMS
    //  gamerInfo   - Gamer info for new talker.  The gamer info
    //                can represent either a local or a remote gamer.
    //  endpointId  - EndpointId for the gamer.  Ignored
    //                if the gamer is local.
    //flags         - Combination of TalkerFlags bits.  Must not be zero!
    //NOTES
    //  The flags parameter can be used to specify one-way voice communication.
    //  If TALKER_FLAG_SEND_VOICE is not set we will not send voice to that
    //  talker.
    //  If TALKER_FLAG_RECEIVE_VOICE is not set we will not receive voice from
    //  that talker.
    //  If both flags are set we will send and receive voice.
    bool AddTalker(const rlGamerInfo& gamerInfo,
                    const EndpointId endpointId,
                    const unsigned flags);

    //PURPOSE
    //  Convenience function to add a talker with send/receive flags set.
    //PARAMS
    //  gamerInfo   - Gamer info for new talker.  The gamer info
    //                can represent either a local or a remote gamer.
	//  endpointId  - EndpointId for the gamer.  Ignored
	//                if the gamer is local.
    bool AddTalker(const rlGamerInfo& gamerInfo,
					const EndpointId endpointId);

	//PURPOSE
	//  Returns talker index. -1 if not found.
	int GetTalkerIndex(const rlGamerId& gamerId) const;
	int GetTalkerIndex(const rlGamerHandle& gamerHandle) const;

	//PURPOSE
	//  Returns talker index. -1 if not found.
	int GetTalkerInfoIndex(const rlGamerId& gamerId) const;
#if RSG_DURANGO
	int GetTalkerInfoIndex(const rlGamerHandle& gamerHandle) const;
#endif

	//PURPOSE
	//  Returns true if the given gamer is among the current group
	//  of talkers.
	bool HaveTalker(const rlGamerId& gamerId) const;

    //PURPOSE
    //  Removes a talker from the current group of talkers.
    bool RemoveTalker(const int talkerIdx);
	bool RemoveTalker(const rlGamerId& gamerId);

    //PURPOSE
    //  Removes all talkers.
    void RemoveAllTalkers();

#if RSG_DURANGO
	//PURPOSE
	//  Adds a chat user to the current group of chat users.
	//PARAMS
	//  gamerInfo   - Gamer info for new chat user.  The gamer info
	//                can represent either a local or a remote gamer.
	//	endpointId	- endpointId of new chat user.
	bool AddLocalChatUser(const int localIndex);
	bool AddRemoteChatUser(const rlGamerInfo& gamerInfo, const EndpointId endpointId);

	//PURPOSE
	//  Removes a chat user from the current group of chat users.
	//PARAMS
	//  gamerInfo   - Gamer info for chat user to be removed.
	//	addr		- address of chat user to be removed.
	void RemoveLocalChatUser(const int localIndex);
	bool RemoveRemoteChatUser(const rlGamerHandle& gamerHandle);
	bool SetChatUserAsPendingRemove(const rlGamerHandle& gamerHandle);

	//PURPOSE
	//  Removes all chat users.
	void RemoveAllChatUsers();
#endif // RSG_DURANGO

    //PURPOSE
    //  Returns true if the given gamer is currently talking.
    bool IsTalking(const int talkerIdx) const;
	bool IsTalking(const rlGamerId& gamerId) const;

    //PURPOSE
    //  Returns true if any gamer is currently talking.
    bool IsAnyTalking() const;

    //PURPOSE
    //  Returns true if any local gamer is currently talking.
    bool IsAnyLocalTalking() const;

    //PURPOSE
    //  Returns true if any remote gamer is currently talking.
    bool IsAnyRemoteTalking() const;

    //PURPOSE
    //  Returns true if the given gamer is about to start talking.
    //NOTES
    //  For remote gamers there is a delay between when a voice packet
    //  is received and when the audio is played.  During this delay
    //  IsAboutToTalk() will return true.
    bool IsAboutToTalk(const int talkerIdx) const;
	bool IsAboutToTalk(const rlGamerId& gamerId) const;

    //PURPOSE
    //  Returns true if any gamer is about to start talking.
    //NOTES
    //  For remote gamers there is a delay between when a voice packet
    //  is received and when the audio is played.  During this delay
    //  IsAnyAboutToTalk() will return true.
    bool IsAnyAboutToTalk() const;

    //PURPOSE
    //  Returns true if any local gamer is about to start talking.
    bool IsAnyLocalAboutToTalk() const;

    //PURPOSE
    //  Returns true if any remote gamer is about to start talking.
    //NOTES
    //  For remote gamers there is a delay between when a voice packet
    //  is received and when the audio is played.  During this delay
    //  IsAnyRemoteAboutToTalk() will return true.
    bool IsAnyRemoteAboutToTalk() const;

    //PURPOSE
    //  Returns true if microphone has focus
    //NOTES
	//
    static bool MicHasFocus();

    //PURPOSE
    //  Returns true if the talker with the given gamer handle is muted
    //  for the local gamer.
    //PARAMS
    //  remoteGamerHandle   - Handle of gamer for whom the mute state is
    //                        desired.
    //NOTES
    //  This function uses the gamer handle instead of the gamer id so
    //  it can be used to check the mute state for gamers that are not
    //  registered with voice chat.
	bool IsMuted(const rlGamerHandle& remoteGamerHandle);

    //PURPOSE
    //  Returns true if the gamer at the local index has chat
    //  privileges with the gamer with the given gamer handle.
    //PARAMS
    //  localGamerIndex     - Index of local gamer doing the query.
    //  gamerHandle         - Handle of gamer for whom the privilege state is
    //                        desired.
    //NOTES
    //  This function uses the gamer handle instead of the gamer id so
    //  it can be used to check chat privileges for gamers that are not
    //  registered with voice chat.
    bool HasChatPrivileges(const int localGamerIndex,
                           const rlGamerHandle& gamerHandle);

    //PURPOSE
    //  Returns true if the local gamer has a headset.
    //NOTES
    //  Can be used to check headsets for local gamers that are not
    //  registered with voice chat.
    bool HasHeadset(const int localGamerIndex) const;

	//PURPOSE
	//  Returns true if the gamer has the setting to have voice output through speakers
	bool IsVoiceOverSpeakers() const;

    //PURPOSE
    //  Retrieves the talker flags of an existing talker (see TalkerFlags).
    //NOTES
    //  Returns 0 if talker doesn't exist.
    unsigned GetTalkerFlags(const int talkerIdx) const;
	unsigned GetTalkerFlags(const rlGamerId& gamerId) const;

    //PURPOSE
    //  Modifies the talker flags of an existing talker (see TalkerFlags).
    //PARAMS
    //	gamerId				- Id of gamer whose talker flags will be modified.
    //  flags				- New flags (see TalkerFlags enum), at least one
    //						  of the flags must be set.
    void SetTalkerFlags(const int talkerIdx, const unsigned flags);
	void SetTalkerFlags(const rlGamerId& gamerId, const unsigned flags);

	//PURPOSE
	//  Returns the local gamers loudness 
	//  [0,1] Metric of the sustained volume coming through the microphone
	float GetLoudness(const int localGamerIndex) const;

	//PURPOSE
	//  Retrieves the last time we sent anything to this talker
	unsigned GetTalkerLastSendTime(const int talkerIdx) const;
	unsigned GetTalkerLastSendTime(const rlGamerId& gamerId) const;

	//PURPOSE
	//  Retrieves the last time we received anything from this talker
	unsigned GetTalkerLastReceiveTime(const int talkerIdx) const;
	unsigned GetTalkerLastReceiveTime(const rlGamerId& gamerId) const;

	//PURPOSE
	//  Sets the audio parameter values for this talker
	void SetTalkerAudioParam(const int talkerIdx, const u32 paramNameHash, const float paramValue);
	void SetTalkerAudioParam(const rlGamerId& gamerId, const u32 paramNameHash, const float paramValue);

	// PURPOSE
	//	Allows the game to specify a custom provider to handle audio streams
	static void SetAudioProvider(VoiceChatAudioProvider *provider) { sm_AudioProvider = provider; }	
	static VoiceChatAudioProvider *GetAudioProvider() { return sm_AudioProvider; }

    //PURPOSE
    //  Updates the voice chat system.  Should be called at least once
    //  ever 100 ms.
    void Update();

	unsigned GetTalkerCount() const { return m_NumTalkers; }

#if !__NO_OUTPUT
	void LogContents();
#endif

private:

    struct Talker;
    struct Peer;
	
#if RSG_ORBIS
	class PortManager;
#endif // RSG_ORBIS

    static void UpdateChatThread(void* arg);

    unsigned RemoteTalkerCount() const;

    Talker* FindTalkerByGamerId(const rlGamerId& gamerId);
    const Talker* FindTalkerByGamerId(const rlGamerId& gamerId) const;
    bool IsLocalTalker(const Talker* talker) const;

    int GetPeerIndex(const EndpointId endpointId) const;
    Peer* FindPeerByEndpointId(const EndpointId endpointId);
    const Peer* FindPeerByEndpointId(const EndpointId endpointId) const;

	void PackChat();
	bool BuildPacket(); 

	void ReceiveChat(const void* data,
                const unsigned sizeofData);

#if RSG_DURANGO
	void ReceiveMesssage(const netEventFrameReceived* fr);
	void SendUserPackets();
#endif // RSG_DURANGO

    //Sends a frame containing voice packets to all remote talkers.
    void SendFrame();

	void SetConnectionManager(netConnectionManager* cxnMgr);

    bool NativeInit(const int maxLocalTalkers,
                    const int maxRemoteTalkers,
                    void* audioDevice,
                    const int cpuAffinity);

    void NativeShutdown();

    bool NativeAddTalker(Talker* talker);

    void NativeRemoveTalker(Talker* talker);

    bool NativeIsTalking(const Talker* talker) const;

    void NativeUpdate(const unsigned timeStep);

    //localTalkerIndex is the index into m_LocalTalkers.
    bool NativePackChat(const int localTalkerIndex,
                        void* dst,
                        unsigned* numBytes); 

    void NativeSubmitChat(Talker* talker,
                            const void* data,
                            const unsigned sizeofData,
                            const u32 timestamp);

	void NativeSetAudioParam(Talker *talker,
							const u32 paramNameHash,
							const float paramValue);

#if USE_SPEEX
    enum
    {
        AVC_SPEEX_SAMPLE_RATE       = 8000,
        AVC_SPEEX_SAMPLE_DURATION   = 20,   //milliseconds
        AVC_SPEEX_SAMPLES_PER_FRAME = AVC_SPEEX_SAMPLE_DURATION * AVC_SPEEX_SAMPLE_RATE / 1000,
    };
#endif  //USE_SPEEX

    //Talker represents both local and remote talkers.
    struct Talker
    {
        Talker()
            : m_Flags(0)
            , m_AboutToTalkTimer(0)
			, m_LastSend(0)
			, m_LastReceive(0)
#if !__NO_OUTPUT
			, m_AttemptedSend(false)
			, m_TotalSent(0)
			, m_TotalReceived(0)
#endif
            , m_Peer(NULL)
            , m_Next(NULL)
			, m_IsTalking(false)
            , m_IsActive(false)
#if RSG_DURANGO
			, m_IsMuted(false)
			, m_IsBlocked(false)
#endif
        {

#if USE_SPEEX || USE_LIBVOICE || RSG_PC
            m_IsTalkingCountdown = 0;
#endif
#if USE_SPEEX
            m_RingBuffer.Init(m_RingBufferBuf, SIZEOF_RING_BUFFER);
#elif USE_LIBVOICE
            // Remote talkers have their data come into this port
            // (and it's eventually connected to m_HeadsetPort so we can hear it)
            m_InPort = LIBVOICE_PORT_INVALID;
			m_PcmPort = LIBVOICE_PORT_INVALID;
#endif  // USE_SPEEX
        }

        void Init(const rlGamerInfo& gamerInfo,
                const unsigned flags);
        void Shutdown();

        rlGamerInfo m_GamerInfo;

        unsigned m_Flags;

#if USE_SPEEX
        //Jitter buffer for the talker - this improves the speech quality
        //over bad connections by dealing with out of order packets and lag
        JitterBuffer* m_SpeexJitterBuffer;
        unsigned      m_AccumulatedTimeStep;
#elif USE_LIBVOICE
        unsigned      m_InPort;
		unsigned      m_PcmPort;
#endif

        //Timer started when a voice packet is received to account for
        //the delay between receiving a voice packet when the audio plays.
        int m_AboutToTalkTimer;

		//Track interaction with this talker
		unsigned m_LastSend;
		unsigned m_LastReceive;

#if !__NO_OUTPUT
		bool m_AttemptedSend; 
		unsigned m_TotalSent;
		unsigned m_TotalReceived;
#endif

        Peer* m_Peer;

        Talker* m_Next;

#if USE_SPEEX || USE_LIBVOICE || RSG_PC
        //If positive, indicates the talker is talking.
        int m_IsTalkingCountdown;
#endif

		bool m_IsTalking : 1;

#if	RSG_DURANGO
		bool m_IsMuted : 1;
		bool m_IsBlocked : 1;
#endif

#if USE_SPEEX
        enum
        {
            SIZEOF_RING_BUFFER  = sizeof(float) * AVC_SPEEX_SAMPLE_RATE / 8
        };

        // ring buffer for passing data for mixing to the voice chat thread
        datRingBuffer m_RingBuffer;
        u8 m_RingBufferBuf[SIZEOF_RING_BUFFER];

        //This flag is set when we have received enough data to start reading from the jitter buffer
        bool m_ProcessVoiceData;
        u8                  m_SpeexBitsDecMem[36];
        SpeexBits*          m_SpeexBitsDec;
        void*               m_SpeexDecState;
#endif  //USE_SPEEX

        bool m_IsActive : 1;
	};

    //Peer represents remote peers.  Each peer can have one or more talkers.
    struct Peer
    {
        Peer()
            : m_EndpointId(NET_INVALID_ENDPOINT_ID)
			, m_NumTalkers(0)
            , m_Next(NULL)
            , m_HaveChatPrivs(true)
            , m_Muted(false)
        {
            for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
            {
                m_Talkers[i] = NULL;
            }
        }

        void Init(const EndpointId endpointId)
        {
            Assert(!m_Next);
            Assert(!m_NumTalkers);
            Assert(NET_IS_VALID_ENDPOINT_ID(endpointId));

#if __ASSERT
            for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
            {
                Assert(!m_Talkers[i]);
            }
#endif  //__ASSERT

            m_EndpointId = endpointId;
            m_HaveChatPrivs = true;
            m_Muted = false;
        }

        void Shutdown()
        {
            Assert(!m_Next);
            Assert(!m_NumTalkers);

#if __ASSERT
            for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
            {
                Assert(!m_Talkers[i]);
            }
#endif  //__ASSERT

            m_EndpointId = NET_INVALID_ENDPOINT_ID;
            m_HaveChatPrivs = true;
            m_Muted = false;
        }

        bool AddTalker(Talker* talker)
        {
            Assert(!talker->m_Peer);

            bool success = false;

            if(AssertVerify(m_NumTalkers < RL_MAX_LOCAL_GAMERS)
                && AssertVerify(!this->HaveTalker(talker)))
            {
                m_Talkers[m_NumTalkers++] = talker;
                talker->m_Peer = this;
                success = true;
            }

            return success;
        }

        void RemoveTalker(Talker* talker)
        {
            Assert(this == talker->m_Peer);
            Assert(m_NumTalkers > 0);

            for(int i = 0; i < (int) m_NumTalkers; ++i)
            {
                if(talker == m_Talkers[i])
                {
                    m_Talkers[i] = m_Talkers[m_NumTalkers-1];
                    m_Talkers[m_NumTalkers-1] = NULL;
                    --m_NumTalkers;
                    talker->m_Peer = NULL;
                    break;
                }
            }
        }

        bool HaveTalker(const Talker* talker) const
        {
            bool have = false;
            for(int i = 0; i < (int) m_NumTalkers; ++i)
            {
                if(m_Talkers[i] == talker)
                {
                    have = true;
                    break;
                }
            }

            return have;
        }

        EndpointId m_EndpointId;
        Talker* m_Talkers[RL_MAX_LOCAL_GAMERS];
        unsigned m_NumTalkers;
        Peer* m_Next;

        //True if all local gamers have privileges to chat with
        //all remote gamers on the peer.
        bool m_HaveChatPrivs    : 1;

        //True if any local gamer has muted any remote gamer
        //that is local to the peer.
        bool m_Muted            : 1;
    };

#if RSG_ORBIS
	class PortManager
	{
	public:
		PortManager();
		~PortManager();

		void Init(unsigned headsetPortId);
		void Shutdown();

		bool GetAvailableVoiceInputPortId(unsigned &voiceInputPortId);
		bool GetConnectedPcmOutputPortId(const unsigned voiceInputPortId, unsigned &pcmOutputPortId);

		void ReleaseVoiceInputPortId(const unsigned voiceInputPortId);

	private:
		static const u32 MAX_VOICE_INPUT_PORTS = 7; // This is the maximum number that PS4 can create as mentioned in the SDK docs.

		struct VoiceInputPortInfo
		{
			VoiceInputPortInfo() { Reset(); }
			~VoiceInputPortInfo() {}

			void Reset()
			{
				m_VoiceInputPortId = LIBVOICE_PORT_INVALID;
				m_ConnectedPcmOutputPortId = LIBVOICE_PORT_INVALID;
				m_IsInitialized = false;
				m_IsAvailable = true;
			}

			unsigned m_VoiceInputPortId;
			unsigned m_ConnectedPcmOutputPortId;

			bool m_IsInitialized;
			bool m_IsAvailable;
		};

		VoiceInputPortInfo m_VoiceInputPortInfos[MAX_VOICE_INPUT_PORTS];
		unsigned m_HeadsetPortId;
	};

	typedef atSingleton<PortManager> PortManagerSingleton;
#endif // RSG_ORBIS

    Talker m_TalkerPile[VoiceChatTypes::MAX_TALKERS];
    Talker* m_TalkerPool;
    Talker* m_Talkers[VoiceChatTypes::MAX_TALKERS];
    unsigned m_NumTalkers;

	// bit flags 
	struct TalkerInfo
	{
		TalkerInfo() : m_IsTalking(0), m_IsAboutToTalk(0), m_IsLocal(0) DURANGO_ONLY(, m_IsMuted(0), m_IsBlocked(0)) { }
		rlGamerId m_GamerId;
		unsigned m_IsTalking : 1;
		unsigned m_IsAboutToTalk : 1; 
		unsigned m_IsLocal : 1;
#if RSG_DURANGO
		rlGamerHandle m_hGamer;
		unsigned m_IsMuted : 1;
		unsigned m_IsBlocked : 1;
#endif
	};
	u8 m_NumTalkerInfo;
	TalkerInfo m_TalkerInfo[VoiceChatTypes::MAX_TALKERS];

    //Local talkers indexed by local gamer index.
    Talker* m_LocalTalkers[RL_MAX_LOCAL_GAMERS];

    Peer m_PeerPile[VoiceChatTypes::MAX_TALKERS];
    Peer* m_PeerPool;
    Peer* m_Peers[VoiceChatTypes::MAX_TALKERS];
    unsigned m_NumPeers;
	
    unsigned m_MaxLocalTalkers;
    unsigned m_MaxRemoteTalkers;
    unsigned m_NumLocalTalkers;

    netConnectionManager* m_CxnMgr;
    unsigned m_ChannelId;

    unsigned m_LastUpdateTime;

    unsigned m_LastSendTime;
    unsigned m_MinSendInterval;

	u8 m_VoicePacket[MAX_PACKET_SIZE];
	//Pointer to the cursor location in an outbound voice packet.
	u8* m_PktCursor;

    //Number of encoded bytes per frame.
    unsigned m_EncBytesPerFrame;

    //Stuff for the background processing thread.
    mutable sysCriticalSectionToken m_Cs;
    sysIpcThreadId m_ThreadHandle;
    sysIpcCurrentThreadId m_ThreadId;
    sysIpcSema m_Sema;

    //Threshold value of VPD to indicate someone is talking into
    //the mic.  Values are 0 to 10, with 10 being most sensitive.
    //(See cellMicGetSignalState()).
    int m_VpdThreshold;

	// Push-to-talk VPD threshold (0 - hear breathing, 10 - hear jack shit)
	int m_VpdThresholdPTT;

	// When voice capture is detected, transmit all audio for the hold time (i.e. 1 second) 
	// This prevents users from cutting out while their volume dips below the threshold.
	u32 m_CaptureHoldTimeMs;

    //Subscribes to netEvents from the connection manager.
    netEventConsumer<netEvent> m_EventSubscriber;

#if RSG_NP

    //Reads the next frame of voice from the mic.
    bool NextFrame(void* frame,
                    const unsigned maxSize);

    void DisposeChatDevice();

    //Number of milliseconds per frame of voice data.
    unsigned m_FrameDurationMs;

#if USE_SPEEX
    enum State
    {
        STATE_INVALID       = -1,
        STATE_LOADED_MODULE,
        STATE_INITIALIZED_MIC,
        STATE_CREATED_MIC_QUEUE,
        STATE_REGISTERED_MIC_QUEUE,
        STATE_CREATED_AUD_QUEUE,
        STATE_REGISTERED_AUD_QUEUE,
        STATE_OPENED_AUD_PORT,
    };
#else
    enum State
    {
        STATE_INVALID = -1,
        STATE_CELLVOICE_INIT,
        STATE_CELLVOICE_START
    };
#endif

    int m_State;

#if USE_SPEEX
    //Microphone event queue
    uint64_t            m_MicQueueId;
    sys_event_queue_t   m_MicCallbackQueue;

    //Audio device event queue
    uint64_t            m_AudioQueueId;
    sys_event_queue_t   m_AudioCallbackQueue;

    //Audio device port num
    unsigned            m_AudPortNum;

    sysCriticalSectionToken m_CsChatDevice;

    //Microphone device num
    int                 m_MicDevNum;

    //Address of index of current audio device read block
    u64*                m_AudBlockAddr;
    //Pointer to audio device ring buffer.
    float*              m_AudDeviceBuffer;

    //Voice proximity detection.
    int                 m_Vpd;
    //Used to check VPD every so often.
    int                 m_VpdCountdown;

    //These are declared to be the same size as SpeexBits.  This
    //is compile-time asserted in voicechat.cpp.  The pointers
    //m_SpeexBitsEnc and m_SpeexBitsDec will be initialized to
    //point to this mem.
    u8                  m_SpeexBitsEncMem[36];

    SpeexBits*              m_SpeexBitsEnc;
    SpeexPreprocessState*   m_SpeexPreprocessState;
    void*                   m_SpeexEncState;

    //Samples from the device.
    float m_RawFrame[AVC_SPEEX_SAMPLES_PER_FRAME];

    //Current number of samples in the raw frame buf.
    unsigned m_NumSamples;

    //This flag is set if voice activity was detected during the current voice frame
    bool m_VoiceActivityDuringCurrentFrame;

    //Current timestamp of voice stream
    u32 m_VoiceStreamTimestamp;

    //Worker that writes data to the audio device.
    static void Worker(void* arg);
    sysIpcSema m_WaitSema;      //Used to wait on worker startup/shutdown
    sysIpcThreadId m_WorkerThreadId;

    bool    m_StopWorker    : 1;
#elif USE_LIBVOICE
    enum { LIBVOICE_PORT_INVALID = 0xFFFFFFFF };
    unsigned		m_MicPort, m_HeadsetPort, m_VoiceInPort, m_VoiceOutPort;
	unsigned		m_MicPcmPort;
public:
    unsigned GetHeadsetPort() const { return m_HeadsetPort; }
#endif

#elif RSG_PC
public:
	static const int MAX_DEVICES = 64;
	static const unsigned int DEFAULT_DEVICE_ID = 0;

	//PURPOSE
	//  Initialize the voice chat system for local talker use only.
	//	This is so the UI can set up voice chat options without
	//  requiring the network to be open.
	//PARAMS
	//  maxLocalTalkers     - Maximum number of local talkers.
	//  cpuAffinity         - CPU on which worker thread will run.
	bool InitLocalOnly(const unsigned maxLocalTalkers,
					   const int cpuAffinity);

	//PURPOSE
	//  Sats an *already initialized* voice chat system to local talker use only.
	//	This is so the UI can set up voice chat options when the voice chat system
	//  is already active.
	//PARAMS
	//  localOnly - true to set to local-only mode, false to reset non-local-only mode.
	bool SetLocalOnly(bool localOnly);

    class Device
    {
    public:
		Device() {}
		Device(GUID id, const wchar_t *name, bool capture, bool playback, bool isDefaultCaptureDevice, bool isDefaultPlaybackDevice);

		const wchar_t* GetName() const {return m_Name;}
		GUID GetId() const {return m_Id;}
		u32 GetId32() const {return m_Id32;}
		bool IsCaptureDevice() const {return m_Capture;}
		bool IsPlaybackDevice() const {return m_Playback;}
		bool IsDefaultCaptureDevice() const {return m_IsDefaultCaptureDevice;}
		bool IsDefaultPlaybackDevice() const {return m_IsDefaultPlabackDevice;}

    private:
        wchar_t m_Name[64]; // RV_DEVICE_NAME_LEN
        GUID m_Id;
		u32 m_Id32;
        bool m_Capture : 1;
        bool m_Playback : 1;
        bool m_IsDefaultCaptureDevice : 1;
        bool m_IsDefaultPlabackDevice : 1;
    };

    class DeviceList
    {
        friend class VoiceChat;
    public:
		unsigned GetNumDevices() const {return (unsigned)m_Devices.GetCount();}
		const Device& GetDevice(u8 index) const {return m_Devices[index];}
    private:
        void Clear() {m_Devices.Reset();}
        void AddDevice(Device device) {m_Devices.Append() = device;}
        atFixedArray<Device, MAX_DEVICES> m_Devices;
    };

	void SetLocalEcho(bool enabled);
	void SetMuteChatBecauseFocusLost(bool shouldMute);
	void EnumerateDevices(DeviceList &list);
	bool SetCaptureDeviceById32(u32 id32);
	void SetCaptureDeviceVolume(float volume);

	enum CaptureMode
	{
		CAPTURE_MODE_VOICE_ACTIVATED,
		CAPTURE_MODE_PUSH_TO_TALK,
		NUM_CAPTURE_MODES
	};

	void SetCaptureMode(CaptureMode mode);
	CaptureMode GetCaptureMode() const;
	void SetPushToTalk(bool captureEnabled);
	bool GetPushToTalk();

	void FreeCaptureDevice();
	void FreePlaybackDevice();
	float GetCaptureDeviceVolume();
    float GetLocalTalkerSpeakingVolume() const;

	bool SetPlaybackDeviceById32(u32 id32);
	void SetPlaybackDeviceVolume(float volume);
	float GetPlaybackDeviceVolume();

	bool IsPlaybackDeviceStarted();

	void SetVoiceTestMode(bool bEnabled);

private:
    void CaptureDeviceUnpluggedCallback(RVHardwareDevice* device);
    void PlaybackDeviceUnpluggedCallback(RVHardwareDevice* device);
	bool SetCaptureDevice(const Device &device);
	bool SetPlaybackDevice(const Device &device);
    void ListChannels(RVHardwareDevice* device, RVDeviceType type);
    void CheckForReconnectedCaptureDevice();
    void CheckForReconnectedPlaybackDevice();
    bool SetupDevice(const void* deviceInfo, const RVDeviceType types);
    bool SelectDefaultDevices();
    bool NativeHasHeadset(const int localTalkerIndex) const;
	void DrainCaptureDevice();

    int m_SamplesPerFrame;
    int m_BytesPerFrame;
    int m_BitsPerSecond;
    float m_LocalTalkerSpeakingVolume;
    bool m_PackFrameStamp;
	volatile bool m_IsVoiceTestMode;

	bool m_LocalEchoEnabled;
	bool m_MuteChatBecauseFocusLost;
	GUID m_CaptureDeviceId;
	GUID m_PlaybackDeviceId;
	CaptureMode m_CaptureMode;
	wchar_t m_CaptureDeviceName[64];	// RV_DEVICE_NAME_LEN
	wchar_t m_PlaybackDeviceName[64]; // RV_DEVICE_NAME_LEN
	float m_CaptureDeviceVolume;
	float m_PlaybackDeviceVolume;	
#endif

#if __BANK
    bkSlider* m_BankVpdSlider;
	bkSlider* m_BankVpdSliderPTT;
	bkSlider* m_BankSliderVpdHoldTime;
    float m_BankVpdThreshold;
	float m_BankVpdThresholdPTT;
	u32 m_BankVpdCaptureHoldTimeMs;
#endif  //__BANK

	bool m_LocalOnly	    : 1;
    bool m_StopChatThread   : 1;
    static bool sm_Initialized;
	static bool sm_InitializedNetwork;
	// Optional pointer to allow local player voice data to be routed through the audio engine
	static VoiceChatAudioProvider *sm_AudioProvider;

	//Stuff for delegates.
public:
	
	//Base Class for talker event
	class TalkerEvent
	{
	public:
		enum TalkerEventType
		{
			TALKER_ADDED,
			TALKER_REMOVED,
		};

		TalkerEventType GetEventType() const { return m_EventType; }
		const rlGamerInfo& GetGamerInfo() const { return m_GamerInfo; }
	protected:
		TalkerEvent(TalkerEventType eventType, const rlGamerInfo& rGamerInfo) 
			: m_EventType(eventType)
			, m_GamerInfo(rGamerInfo)
		{
		}
		TalkerEventType m_EventType;
		rlGamerInfo m_GamerInfo;

	private:
		TalkerEvent() {}
	};

	class TalkerEventTalkerAdded : public TalkerEvent
	{
	public:
		TalkerEventTalkerAdded( const rlGamerInfo& rGamerInfo) : TalkerEvent(TALKER_ADDED, rGamerInfo) {}
	};

	class TalkerEventTalkerRemoved : public TalkerEvent
	{
	public:
		TalkerEventTalkerRemoved( const rlGamerInfo& rGamerInfo) : TalkerEvent(TALKER_REMOVED, rGamerInfo) {}
	};

private:
	typedef atDelegator<void (const TalkerEvent* evt)> Delegator;
	Delegator m_Delegator;
public:

	//PURPOSE
	//  Event delegate type.
	//  The signature for event handlers is:
	//
	//  void OnEvent(const voicechat::TalkerEvent* evt)
	//
	typedef Delegator::Delegate Delegate;


	//PURPOSE
	//  Adds a delegate that will be called with event notifications.
	void AddDelegate(Delegate* dlgt);

	//PURPOSE
	//  Removes a delegate.
	void RemoveDelegate(Delegate* dlgt);
};

}   //namespace rage

#endif  //SNET_AVCHAT_H
