// 
// snet/session.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNET_SESSION_H
#define SNET_SESSION_H

#include "atl/inlist.h"
#include "tasks.h"
#include "net/connectionmanager.h"
#include "rline/rlpeerinfo.h"
#include "rline/rlsession.h"
#include "rline/scpresence/rlscpresence.h"
#include "snet.h"

/*
    Although much of this code handles multiple locally signed in
    gamers, snSession was written in the context of games that permit
    only a single signed in gamer per peer.  Because of this there is
    probably a non-trivial amount of work to be done to support multiple
    locally signed in gamers.
*/

//FIXME (KB) - remove events that request data from the app.  This
//             is error prone, as more than one handler might attempt
//             to answer.  This is better handled by a callback that
//             can only be registered once.

namespace rage
{
class snSession;
class snPeer;

class snGamer
{
    friend class snHostSessionTask;
    friend class snJoinSessionTask;
    friend class snHandleJoinRequestTask;
    friend class snJoinGamersToRlineTask;
	friend class snJoinLocalSessionBotTask;
	friend class snNotifyAddGamerTask;
	friend class snAddRemoteGamerTask;
	friend class snDropGamersTask;
	friend class snSession;
	friend class snPeer;

public:

    snGamer();

    void Clear();

    const rlGamerInfo& GetGamerInfo() const;
    const rlGamerHandle& GetGamerHandle() const;
    const rlGamerId& GetGamerId() const;

    snPeer* GetPeer();
    const snPeer* GetPeer() const;

private:

    rlGamerInfo m_GamerInfo;

    rlSlotType m_SlotType;

    snPeer* m_Peer;

    inlist_node<snGamer> m_ListLink;

    bool m_FullyJoined  : 1;
	bool m_JoinedToRline : 1;
	bool m_Established : 1;
	bool m_IsPendingDrop : 1;

	int m_BailReason;
};

class snPeer
{
    friend class snSession;

public:

    snPeer();

    ~snPeer();

    void InitLocal(const int localGamerIndex);

    void InitRemote(const int cxnId,
					const EndpointId endpointId,
                    const rlPeerInfo& peerInfo);

    void Shutdown();

    bool IsConnected() const;
    bool IsLocal() const;
    bool IsRemote() const;

    bool HasGamer(const rlGamerId& gamerId) const;
    bool HasGamer(const rlGamerHandle& gamerHandle) const;

    unsigned GetGamerCount() const;

    //PURPOSE
    //  Gathers gamers hailing from the peer.
    //RETURNS
    //  The number of gamers gathered.  If the gamers array
    //  is too small this value may be less than the number of gamers
    //  actually owned by the peer.
    unsigned GetGamers(rlGamerInfo gamers[], const unsigned maxGamers) const;
    unsigned GetGamers(rlGamerId gamerIds[], const unsigned maxGamers) const;
    unsigned GetGamers(rlGamerHandle gamerHandles[], const unsigned maxGamers) const;

	snGamer* GetGamer(const rlGamerId& gamerId);
	const snGamer* GetGamer(const rlGamerId& gamerId) const;
	snGamer* GetGamer(const rlGamerHandle& gamerHandle);
	const snGamer* GetGamer(const rlGamerHandle& gamerHandle) const;

    const rlPeerInfo& GetPeerInfo() const;

    u64 GetPeerId() const;

    int GetCxnId() const;
	EndpointId GetEndpointId() const;

	bool AllowMigration() const;

private:

#if RL_SUPPORT_NETWORK_BOTS
    static const unsigned MAX_GAMERS_PER_PEER = RL_MAX_LOCAL_GAMERS + RL_MAX_NETWORK_BOTS;
#else
    static const unsigned MAX_GAMERS_PER_PEER = RL_MAX_LOCAL_GAMERS;
#endif // RL_SUPPORT_NETWORK_BOTS

    bool AddGamer(snGamer* gamer);

#if RL_SUPPORT_NETWORK_BOTS
    bool AddBotGamer(snGamer* gamer);
#endif // RL_SUPPORT_NETWORK_BOTS

    bool RemoveGamer(const rlGamerId& gamerId);
    bool RemoveGamer(const rlGamerHandle& gamerHandle);

    int m_CxnId;
    rlPeerInfo m_PeerInfo;
	EndpointId m_EndpointId;
	snGamer* m_Gamers[RL_MAX_LOCAL_GAMERS];
    unsigned m_NumGamers;
#if RL_SUPPORT_NETWORK_BOTS
    snGamer* m_BotGamers[RL_MAX_NETWORK_BOTS];
    unsigned m_NumBotGamers;
#endif // RL_SUPPORT_NETWORK_BOTS
    inlist_node<snPeer> m_ListLink;
    bool m_IsLocal : 1;
	bool m_AllowMigrate : 1;
};

//Snet event ids.
enum
{
    //Session was hosted.
    SNET_EVENT_SESSION_HOSTED,
    //Remotely hosted session was joined.
    SNET_EVENT_SESSION_JOINED,
    //Network bot joined the session
    SNET_EVENT_SESSION_BOT_JOINED,
    //Request to join remotely hosted session failed.
    SNET_EVENT_JOIN_FAILED,
    //Request to join a network bot into a remotely hosted session failed.
    SNET_EVENT_SESSION_BOT_JOIN_FAILED,
	// A gamer is being asynchronously added to the session
	SNET_EVENT_ADDING_GAMER,
    //A gamer was added to the session.
    SNET_EVENT_ADDED_GAMER,
    //A gamer was removed from the session.
    SNET_EVENT_REMOVED_GAMER,
    //Session was started.
    SNET_EVENT_SESSION_ESTABLISHED,
    //Dispatched when a session migration begins.
    SNET_EVENT_SESSION_MIGRATE_START,
    //The session is now owned by a new host.
    //A series of SNET_EVENT_REMOVED_GAMER events may follow, especially
    //for gamers local to the dropped host.
    SNET_EVENT_SESSION_MIGRATE_END,
    //The session attributes were changed.
    //Access snSession::GetConfig().m_Attrs to get the new attributes.
    SNET_EVENT_SESSION_ATTRS_CHANGED,
    //Dispatched when the session has been destroyed.
    SNET_EVENT_SESSION_DESTROYED,
	//The session has retrieved a display name
	SNET_EVENT_DISPLAY_NAME_RETRIEVED,
	//Removed from session
	SNET_EVENT_REMOVED_FROM_SESSION,
	//Removed from session due to stall
	SNET_EVENT_REMOVED_DUE_TO_STALL,
};

#define SNET_EVENT_COMMON_DECL( name )\
    static unsigned EVENT_ID() { return name::GetAutoId(); }\
    virtual unsigned GetId() const { return name::GetAutoId(); }

#define SNET_EVENT_DECL( name, id )\
    AUTOID_DECL_ID( name, snEvent, id )\
    SNET_EVENT_COMMON_DECL( name )

class snEventSessionHosted;
class snEventSessionJoined;
class snEventSessionBotJoined;
class snEventJoinFailed;
class snEventSessionBotJoinFailed;
class snEventAddingGamer;
class snEventAddedGamer;
class snEventRemovedGamer;
class snEventSessionStarted;
class snEventSessionEnded;
class snEventSessionMigrateStart;
class snEventSessionMigrateEnd;
class snEventSessionAttrsChanged;
class snEventSessionDestroyed;
class snEventSessionDisplayNameRetrieved;
class snEventRemovedFromSession;
class snEventRemovedDueToStall;

//PURPOSE
//  Base class for all snet session event classes.
class snEvent
{
    friend class snSession;

public:

    AUTOID_DECL_ROOT(snEvent);
    SNET_EVENT_COMMON_DECL(snEvent);

    snEvent()
    {
        m_Event = this;
    }

    virtual ~snEvent() {}

    //Easy access to specific event type.  Use the event id
    //to determine which pointer to use.
    union
    {
        snEvent*                     m_Event;
        snEventSessionHosted*        m_SessionHosted;
        snEventSessionJoined*        m_SessionJoined;
        snEventSessionBotJoined*     m_SessionBotJoined;
        snEventJoinFailed*           m_JoinFailed;
        snEventSessionBotJoinFailed* m_SessionBotJoinFailed;
		snEventAddingGamer*			 m_AddingGamer;
        snEventAddedGamer*           m_AddedGamer;
        snEventRemovedGamer*         m_RemovedGamer;
        snEventSessionStarted*       m_SessionStarted;
        snEventSessionEnded*         m_SessionEnded;
        snEventSessionMigrateStart*  m_SessionMigrateStart;
        snEventSessionMigrateEnd*    m_SessionMigrateEnd;
        snEventSessionAttrsChanged*  m_SessionAttrsChanged;
        snEventSessionDestroyed*     m_SessionDestroyed;
		snEventSessionDisplayNameRetrieved* m_SessionDisplayNameRetrieved;
		snEventRemovedFromSession*	 m_RemovedFromSession;
		snEventRemovedDueToStall*	 m_RemovedDueToStall;
	};

private:

    inlist_node<snEvent> m_ListLink;
};

//PURPOSE
//  Dispatched when a session is hosted.
class snEventSessionHosted: public snEvent
{
public:

    SNET_EVENT_DECL(snEventSessionHosted, SNET_EVENT_SESSION_HOSTED);
};

//PURPOSE
//  Dispatched when a remotely hosted session is joined.
class snEventSessionJoined: public snEvent
{
public:

    SNET_EVENT_DECL(snEventSessionJoined, SNET_EVENT_SESSION_JOINED);

    snEventSessionJoined(const EndpointId hostEndpointId,
                        const void* data,
                        const unsigned sizeofData)
        : m_HostEndpointId(hostEndpointId)
        , m_Data(data)
        , m_SizeofData(sizeofData)
    {
    }

    EndpointId m_HostEndpointId;

    //App-specific data provided by the host.
    const void* m_Data;
    unsigned m_SizeofData;
};

//PURPOSE
//  Dispatched when a network bot joins a network session
class snEventSessionBotJoined: public snEvent
{
public:

    SNET_EVENT_DECL(snEventSessionBotJoined, SNET_EVENT_SESSION_BOT_JOINED);

    snEventSessionBotJoined() {}
};

//PURPOSE
//  Dispatched when a request to join a remotely hosted session fails.
class snEventJoinFailed: public snEvent
{
public:

    SNET_EVENT_DECL(snEventJoinFailed, SNET_EVENT_JOIN_FAILED);

    snEventJoinFailed(const rlSessionInfo& sessionInfo,
                        snJoinResponseCode responseCode,
                        const void* response,
                        const unsigned sizeofResponse)
        : m_SessionInfo(sessionInfo)
        , m_ResponseCode(responseCode)
        , m_Response(response)
        , m_SizeofResponse(sizeofResponse)
    {
        snAssert(SNET_JOIN_ACCEPTED != responseCode);
    }

    rlSessionInfo m_SessionInfo;
    snJoinResponseCode m_ResponseCode;

    //App-specific data provided by the host.
    const void* m_Response;
    unsigned m_SizeofResponse;
};

//PURPOSE
//  Dispatched when a request to join a network bot into a remotely hosted session fails.
class snEventSessionBotJoinFailed : public snEvent
{
public:

    SNET_EVENT_DECL(snEventSessionBotJoinFailed, SNET_EVENT_SESSION_BOT_JOIN_FAILED);

    snEventSessionBotJoinFailed(const rlSessionInfo& sessionInfo,
                        snJoinResponseCode responseCode,
                        const void* response,
                        const unsigned sizeofResponse)
        : m_SessionInfo(sessionInfo)
        , m_ResponseCode(responseCode)
        , m_Response(response)
        , m_SizeofResponse(sizeofResponse)
    {
        snAssert(SNET_JOIN_ACCEPTED != responseCode);
    }

    rlSessionInfo m_SessionInfo;
    snJoinResponseCode m_ResponseCode;

    //App-specific data provided by the host.
    const void* m_Response;
    unsigned m_SizeofResponse;
};

//PURPOSE
//  Dispatched when a gamer is added to the session.
//NOTES
//  For local gamers m_Data will be NULL and m_SizeofData will be zero.
class snEventAddedGamer : public snEvent
{
public:

    SNET_EVENT_DECL(snEventAddedGamer, SNET_EVENT_ADDED_GAMER);

    snEventAddedGamer(const rlGamerInfo& gamerInfo,
                        const EndpointId endpointId,
						const netAddress& relayAddr,
                        const rlSlotType slotType,
                        const void* data,
                        const unsigned sizeofData)
        : m_GamerInfo(gamerInfo)
        , m_EndpointId(endpointId)
		, m_RelayAddr(relayAddr)
        , m_SlotType(slotType)
        , m_Data(data)
        , m_SizeofData(sizeofData)
    {
    }

    rlGamerInfo m_GamerInfo;
    EndpointId m_EndpointId;
	netAddress m_RelayAddr;
    rlSlotType m_SlotType;
    const void* m_Data;
    unsigned m_SizeofData;
};

//PURPOSE
//  Dispatched when a gamer is about to be added to the session (asynchronously)
//NOTES
//  For local gamers m_Data will be NULL and m_SizeofData will be zero.
class snEventAddingGamer : public snEvent
{
public:

	SNET_EVENT_DECL(snEventAddingGamer, SNET_EVENT_ADDING_GAMER);

	snEventAddingGamer(const rlGamerInfo& gamerInfo, const EndpointId& endpointId, const netAddress& relayAddr, const rlSlotType slotType, const void* data, const unsigned sizeofData)
		: m_GamerInfo(gamerInfo)
		, m_EndpointId(endpointId)
		, m_RelayAddr(relayAddr)
		, m_SlotType(slotType)
		, m_Data(data)
		, m_SizeofData(sizeofData)
	{
	}

	rlGamerInfo m_GamerInfo;
	EndpointId m_EndpointId;
	netAddress m_RelayAddr;
	rlSlotType m_SlotType;
	const void* m_Data;
	unsigned m_SizeofData;
};

//PURPOSE
//  Dispatched when a gamer is removed from the session.
class snEventRemovedGamer : public snEvent
{
public:

    SNET_EVENT_DECL(snEventRemovedGamer, SNET_EVENT_REMOVED_GAMER);

    explicit snEventRemovedGamer(const rlGamerInfo& gamer, const EndpointId endpointId)
        : m_GamerInfo(gamer)
		, m_EndpointId(endpointId)
		, m_RemoveReason(-1)
    {
    }

    rlGamerInfo m_GamerInfo;
	EndpointId m_EndpointId;
	int m_RemoveReason;
};

//PURPOSE
//  Dispatched when the session has started.
class snEventSessionEstablished : public snEvent
{
public:

    SNET_EVENT_DECL(snEventSessionEstablished, SNET_EVENT_SESSION_ESTABLISHED);
};

//PURPOSE
//  Dispatched when a session migration begins.
class snEventSessionMigrateStart : public snEvent
{
public:

    SNET_EVENT_DECL(snEventSessionMigrateStart, SNET_EVENT_SESSION_MIGRATE_START);

    snEventSessionMigrateStart()
    {
    }
};

//PURPOSE
//  Dispatched when the session has migrated to a new host,
//  or has failed.
class snEventSessionMigrateEnd : public snEvent
{
public:

    SNET_EVENT_DECL(snEventSessionMigrateEnd, SNET_EVENT_SESSION_MIGRATE_END);

    snEventSessionMigrateEnd(const rlPeerInfo& newHost, const bool succeeded, const int numCandidates, const int candidateIdx)
        : m_NewHost(newHost)
        , m_Succeeded(succeeded)
		, m_NumCandidates(numCandidates)
		, m_CandidateIdx(candidateIdx)
    {
    }

    rlPeerInfo m_NewHost;
    bool m_Succeeded;
	int m_NumCandidates;
	int m_CandidateIdx;
};

//PURPOSE
//  Dispatched when the session attributes have changed.
//  Access snSession::GetConfig().m_Attrs for the new attributes.
class snEventSessionAttrsChanged : public snEvent
{
public:

    SNET_EVENT_DECL(snEventSessionAttrsChanged, SNET_EVENT_SESSION_ATTRS_CHANGED);

    snEventSessionAttrsChanged()
    {
    }
};

//PURPOSE
//  Dispatched when the session has been destroyed.
class snEventSessionDestroyed : public snEvent
{
public:

    SNET_EVENT_DECL(snEventSessionDestroyed, SNET_EVENT_SESSION_DESTROYED);

    snEventSessionDestroyed()
    {
    }
};

//PURPOSE
//	Dispatched when the session has retrieved a display name for a previously failed gamer
class snEventSessionDisplayNameRetrieved : public snEvent
{
public:

	SNET_EVENT_DECL(snEventSessionDisplayNameRetrieved, SNET_EVENT_DISPLAY_NAME_RETRIEVED);

	snEventSessionDisplayNameRetrieved(const rlGamerInfo& gamerInfo)
		: m_GamerInfo(gamerInfo)
	{
	}

	rlGamerInfo m_GamerInfo;
};

//PURPOSE
//	Dispatched when we are forcibly removed / kicked from a session
class snEventRemovedFromSession : public snEvent
{
public:

	SNET_EVENT_DECL(snEventRemovedFromSession, SNET_EVENT_REMOVED_FROM_SESSION);

	snEventRemovedFromSession(const unsigned numGamers)
		: m_NumGamers(numGamers)
	{
	}

	unsigned m_NumGamers;
};

//PURPOSE
//	Dispatched when we forcibly remove ourselves due to local stall
class snEventRemovedDueToStall : public snEvent
{
public:

	SNET_EVENT_DECL(snEventRemovedDueToStall, SNET_EVENT_REMOVED_DUE_TO_STALL);

	snEventRemovedDueToStall()
	{
	}
};

//PURPOSE
//  Dispatched on the host via the snSessionOwner::GetGamerData callback when
//  the session requires application-specific gamer data.
//
//NOTES
//  The application should copy app-specific gamer data to the m_Data
//  buffer.
class snGetGamerData
{
public:

    snGetGamerData()
        : m_SizeofData(0)
    {
    }

    //Buffer to which app should copy data.      
    u8 m_Data[SNET_MAX_SIZEOF_GAMER_DATA];
    unsigned m_SizeofData;
};

//PURPOSE
//  Contains join request data sent from a join requester to the session
//  host.  Dispatched on the host via the snSessionOwner::RequestJoin callback when
//  a join request is received.
//
//NOTES
//  m_RequestData contains application-specific data from the requester.
//
//  The owner implementation of RequestJoin should return true to accept
//  the join request and false to deny it.
//
//  The owner implementation can copy app data to the m_ResponseData
//  array and set the m_SizeofResponseData member to the size of the data.
//  This data will be delivered to the requester with the response.
//
//  Note that even if the owner implementation returns true the join
//  request might still be denied if there aren't enough empty slots to
//  fill the request.
//
//  Application code should not assume that a new player has successfully
//  joined the session until it receives the SNET_EVENT_ADDED_GAMER event.
//
//  By default all join requests are rejected.
class snJoinRequest
{
public:

    snJoinRequest(const netAddress& addr,
                    const rlSlotType slotType,
                    const void* requestData,
                    const unsigned sizeofRequestData,
                    const unsigned groupSize)
        : m_Addr(addr)
        , m_SlotType(slotType)
        , m_RequestData(requestData)
        , m_SizeofRequestData(sizeofRequestData)
        , m_SizeofResponseData(0)
        , m_GroupSize(groupSize)
    {
    }

    netAddress m_Addr;         //Gamer's network address
    rlSlotType m_SlotType;          //Type of slot in which gamer wishes to join

    const void* m_RequestData;      //App-specific data sent by the requester.
    unsigned m_SizeofRequestData;   //Number of bytes in the data.

    u8 m_ResponseData[SNET_MAX_SIZEOF_JOIN_RESPONSE_DATA];
    unsigned m_SizeofResponseData;

    unsigned m_GroupSize;
};

//PURPOSE
//  A collection of delegates (callbacks) passed to snSession::Init().
//  Each callback will be called in response to events that require
//  authoritative action.
class snSessionOwner
{
public:

	snSessionOwner()
	{
		GetGamerData.Reset();
		HandleJoinRequest.Reset();
		ShouldMigrate.Reset();
		GetHostMigrationCandidates.Reset();
	}

    //PURPOSE
    //  Called by the session to retrieve application-specific data
    //  about a gamer.
    //PARAMS
    //  session         - Session in which the gamer is a member.
    //  gamerInfo       - The gamer.
    //  gamerData       - Object to which app should copy data.
    //NOTES
    //  This function is typically called on the host prior to
    //  broadcasting gamer data to other peers.
    //
    //  If no callback is bound to this delegate the default action
    //  is to broadcast no application-specific data.
    atDelegate<void (snSession*, const rlGamerInfo&, snGetGamerData*)> GetGamerData;

    //PURPOSE
    //  Called when a join request is received.  This gives the session
    //  owner (the application) the opportunity to investigate the join
    //  request and decide whether to accept or deny.
    //PARAMS
    //  session         - Session to which the request pertains.
    //  gamerInfo       - Gamer requesting to join.
    //  joinRequest     - The join request.
    //RETURNS
    //  True if the gamer should be accepted.
    //NOTES
    //  If no callback is bound to this delegate the default action is
    //  to deny all join requests.
    atDelegate<bool (snSession*, const rlGamerInfo&, snJoinRequest*)> HandleJoinRequest;

	//PURPOSE
	//  Called prior to starting a host migration to check if the application
	//  wants to migrate the session
	//PARAMS
	//RETURNS
	//  True if the session should migrate
	//NOTES
	//  If no callback is bound to this delegate the default action is
	//  to always migrate
	atDelegate<bool ()> ShouldMigrate;

    //PURPOSE
    //  Called prior to starting a host migration.
    //  The array of peer infos should be populated with host migration
    //  candidates in the order in which they will be attempted.
    //  All peers must generate the same array in the same order for
    //  a given host migration.
    //
    //  Be sure to include the local peer (yourself) in the array.
    //PARAMS
    //  session         - Session to which the request pertains.
    //  candidates      - Array of peer info host candidates.
    //  maxCandidates   - Maximum number of candidates.
    //RETURNS
    //  Number of candidates put into the array.
    //NOTES
    //  If no callback is bound to this delegate the default action is
    //  to generate a candidate array in ordered by peer id.
    atDelegate<unsigned (snSession*, rlPeerInfo*, const unsigned)> GetHostMigrationCandidates;
};

class snSession
{
    friend class snHostSessionTask;

    //These tasks are performed in sequence when a peer tries to join a session.
    //The indentation indicates subtasks of tasks.
    friend class snJoinSessionTask;
        friend class snEstablishSessionTask;
            friend class snQueryHostConfigTask;
    
    friend class snJoinRemoteSessionBotTask;
    friend class snJoinLocalSessionBotTask;

    //From snJoinSessionTask, the new gamer sends the host a snMsgJoinRequest.
    //The host receives the request and decides to accept or deny the request.
    //If accepted the host adds the gamer to the session, generates a
    //snEventAddedGamer event, sends a snMsgAddGamerCmd for the joiner to
    //existing peers, and sends snMsgAddGamerCmds for each existing peer to
    //the joiner.
    //The host then starts a snHandleJoinRequestTask task, which joins the
    //gamer to the underlying rlSession.
    //
    //If the joiner requests a public slot and none are available the join
    //request will be denied.  If the joiner requests a private slot and
    //none are available an attempt will be made to place the joiner in a
    //public slot.
    friend class snHandleJoinRequestTask;

    friend class snConnectToPeerTask;
    
    //When a new gamer joins the .
    friend class snAddRemoteGamerTask;

    friend class snDropGamersTask;

    //This task assumes that the gamers have been (or will be) added
    //at the snSession level and only adds them at the rlSession level.
    friend class snJoinGamersToRlineTask;
	friend class snJoinCompleteTask; 

    //This task assumes that the gamers have been (or will be) removed
    //at the snSession level, and only removes them at the rlSession
    //level.
    friend class snLeaveGamersFromRlineTask;

    friend class snMigrateSessionTask;

    friend class snDestroyTask;

#if RSG_DURANGO
	friend class snPrepareToHandleJoinRequestTask;
#endif

	friend class snEnableMatchmakingAdvertiseTask;
	friend class snChangeAttributesTask;

    friend class snSendInvitesTask;
    friend class snSendPartyInvitesTask;

    //TODO: This sounds way too similar to snQueryHostConfigTask.  Basically, it
    //      just combines a NAT negotiate with getting the config.  Need to think
    //      about this more.
    friend class snQueryConfigTask;

    friend class snModifyPresenceFlagsTask;

	friend class snNotifyAddGamerTask;

    typedef atDelegator<void (snSession*, const snEvent*)> Delegator;

public:

	// PURPOSE
	//Time to live for slot reservations.
	static const unsigned SLOT_RESERVATION_TTL_MS   = 60*1000;

	// PURPOSE
	//	In order to not spam 
	static const unsigned TIME_BETWEEN_RESERVATION_UPDATES_MS = 30*1000;

    //PURPOSE
    //  Parameter to Kick() that determines how gamers are kicked.
    enum KickType
    {
        KICK_INFORM_PEERS,      //Remove the gamer and inform all peers
        KICK_DONT_INFORM_PEERS  //Remove the gamer and don't inform peers
    };

    //PURPOSE
    //  Declare the delegate type.
    //NOTES
    //  The signature for a delegate is:
    //
    //  void OnEvent( snSession* session, snEvent* event );
    //
    //  session - The session dispatching the event.
    //  event   - The event.
    typedef Delegator::Delegate Delegate;

    //PURPOSE
    //  Returns true if we can host a session of the given type.s
    static bool CanHost(const rlNetworkMode netMode);

    //PURPOSE
    //  Returns true if we can join a session of the given type.
    static bool CanJoin(const rlNetworkMode netMode);

	//PURPOSE
	//   
	static void SetSelfRemoveStallTime(const unsigned nSelfRemoveStallTime); 
	static void SetConnectToPeerChecks(const bool connectToPeerChecks);
	static bool AllowConnectToPeerChecks();

    snSession();

    ~snSession();

    //PURPOSE
    //  Initializes the session.
    //PARAMS
    //  allocator       - Used to allocate tasks and events.
    //  owner           - Collection of callbacks implemented by
    //                    the session owner.
    //  cxnMgr          - Used to send/receive messages.
    //  channelId       - Network channel on which communication will occur.
    //                    This is typically a unique channel set aside for
    //                    for session communication.
    //  name            - Optional.  Session name used in debug output.
    //NOTES
    //  For PS3 LAN sessions be sure to disable crypto on the socket
    //  used by the connection manager.
    bool Init(sysMemAllocator* allocator,
                const snSessionOwner& owner,
                netConnectionManager* cxnMgr,
                const unsigned channelId,
                const char* name);

    void Shutdown(bool isSudden, int bailReasonIfSudden);

	//PURPOSE
	//  Returns true if the session is initialized.
	bool IsInitialized() const;

    //PURPOSE
    //  Returns true if the session exists, i.e. has been created.
    bool Exists() const;

	//PURPOSE
	//  Returns true if the session has been established. This means that all 
	//  pre-existing players have been added to the session locally.
	bool IsEstablished() const;

    //PURPOSE
    //  Returns true if the session is an online session.
    bool IsOnline() const;

    //PURPOSE
    //  Returns true if the session is a lan session.
    bool IsLan() const;

    //PURPOSE
    //  Returns true if the session is a network session.
    bool IsNetwork() const;

    //PURPOSE
    //  Returns true if the session is an offline session.
    bool IsOffline() const;

    //PURPOSE
    //  Returns true if the session is currently migrating to a new host.
    bool IsMigrating() const;

    //PURPOSE
    //  Returns the channel ID passed to Init().
    unsigned GetChannelId() const;

    //PURPOSE
    //  Applies a new channel ID for this session.
    //  Temporary workaround for marking one session type as another
    //  This will be removed as soon as that functionality is removed.
    void SwapChannelId(unsigned channelId);

	//PURPOSE
	//  Returns the rlSession for this session
	const rlSession* GetRlSession() const;

    //PURPOSE
    //  Returns the session info object describing this session.
    const rlSessionInfo& GetSessionInfo() const;

	//PURPOSE
	//  Returns the platform-specific session handle.
	const void* GetSessionHandle() const;


	//PURPOSE
    //  Retrieves the peer info of the session host.
    //RETURNS
    //  True on success.  False if there is no current host.
    bool GetHostPeerInfo(rlPeerInfo* peerInfo) const;

    //PURPOSE
    //  Retrieves the peer id of the session host.
    //RETURNS
    //  True on success.  False if there is no current host.
    bool GetHostPeerId(u64* peerId) const;

	//PURPOSE
	//  Retrieves the peer id of the session host.
	//RETURNS
	//  True on success.  False if there is no current host.
	bool IsHostPeerId(u64 peerId) const;

	//PURPOSE
    //  Retrieves the peer address of the session host.
    //RETURNS
    //  True on success.  False if there is no current host.
    bool GetHostPeerAddress(rlPeerAddress* peerAddr) const;

    //PURPOSE
    //  Retrieves the network endpointId of the session host.
    //RETURNS
    //  True on success.  False if there is no current host.
    bool GetHostEndpointId(EndpointId* endpointId) const;
	bool IsHostEndpointId(const EndpointId endpointId) const;
	
    //PURPOSE
    //  Returns the peer info of the local peer.
    const rlPeerInfo& GetLocalPeerInfo() const;

    //PURPOSE
    //  Returns the peer id of the local peer.
    u64 GetLocalPeerId() const;

    //PURPOSE
    //  Returns the owner of the local session instance.
    const rlGamerInfo& GetOwner() const;

    //PURPOSE
    //  Returns the globally unique session id.
    //  This ID *must not* be saved beyond the lifetime of
    //  the session, as session IDs can be reused.
    u64 GetSessionId() const;

	//PURPOSE
	//  Returns the session token
	const rlSessionToken GetSessionToken() const;

    //PURPOSE
    //  Returns true if the local peer is hosting the session.
    bool IsHost() const;

    //PURPOSE
    //  Returns the connection manager instance used by the session
    //  to communicate with remote peers.
    netConnectionManager* GetCxnMgr();
    const netConnectionManager* GetCxnMgr() const;

    //PURPOSE
    //  Allows the owner information to be changed
    void SetOwner(const snSessionOwner& owner);

	//PURPOSE
	//  Set presence attributes to be set by the game session
	void SetPresenceAttributeNames(const char* attrSessionToken,
		const char* attrSessionId,
		const char* attrSessionInfo,
		const char* attrIsHost,
		const char* attrIsJoinable,
		const bool updateAttributes);

	//PURPOSE
	//  Clear presence attributes to be set by the game session
	void ClearPresenceAttributeNames();

    //PURPOSE
    //  Hosts a session.
    //PARAMS
    //  localGamerIndex - Local index of session owner.
    //  netMode         - Networking mode (ONLINE, LAN, OFFLINE)
    //  maxPubSlots     - Maximum number of public slots.
    //  maxPrivSlots    - Maximum number of private slots.
    //  attrs           - Matching attributes.
    //  createFlags     - Session creation flags (rlSession::CreateFlags)
    //  ownerData       - Gamer data to be replicated to other peers in
    //                    the session.
    //  sizeofOwnerData - Size of gamer data to be replicated to other peers
    //                    in the session.
    //  status          - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //  For PS3 LAN sessions be sure to disable crypto on the socket
    //  used by the connection manager passed to Init().
    //
    //  PS3 LAN sessions must use the NET_PROTO_UDP socket protocol.
    //  PS3 ONLINE sessions must use the NET_PROTO_UDPP2P socket protocol.
    //
    //  Sessions with no public slots are considered private sessions and
    //  will not be advertised on matchmaking.  Private sessions thus
    //  require no server resources.  Consider making sessions private when
    //  possible.  For example, invite-only sessions, such as those used
    //  for parties, should be private.
    //
    //  Matching attributes cannot be nil (unset) for sessions with public
    //  slots.  Private sessions are permitted to have nil attributes, but
    //  they must still specify a valid game mode and game type.
    //
    //  This initiates an asynchronous operation that is not complete until
    //  the Pending() function of the status object returns false.
    bool Host(const int localGamerIndex,
            const rlNetworkMode netMode,
            const int maxPubSlots,
            const int maxPrivSlots,
            const rlMatchingAttributes& attrs,
            const unsigned createFlags,
            const void* ownerData,
            const unsigned sizeofOwnerData,
            netStatus* status);

    //PURPOSE
    //  Joins a session hosted by a remote peer.
    //PARAMS
    //  localGamerIndex - Local index of gamer joining the session.
    //  sessionInfo     - Session info identifying the session.
    //  netMode         - Network mode.
    //  slotType        - SLOT_PUBLIC or SLOT_PRIVATE.
    //                    The type of slot to which the gamer is eventually
    //                    joined is up to the host.
    //  createFlags     - From rlSession::CreateFlags.
    //  joinerData      - Application data sent to the host in the join request
    //                    (in the snMsgJoinRequest message).
    //  sizeofJoinerData- Size of application data.
    //  groupMembers    - Array of group members
    //  numGroupMembers - Number of members in the group
    //  status          - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //  For PS3 LAN sessions be sure to disable crypto on the socket
    //  used by the connection manager passed to Init().
    //
    //  The groupMembers array must contain at least one entry, which would be
    //  the gamer ID of the caller.
    //
    //  This initiates an asynchronous operation that is not complete until
    //  the Pending() function of the status object returns false.
    bool Join(const int localGamerIndex,
                const rlSessionInfo& sessionInfo,
                rlNetworkMode netMode,
                const rlSlotType slotType,
                const unsigned createFlags,
                const void* joinerData,
                const unsigned sizeofJoinerData,
                const rlGamerHandle* group,
                const unsigned numGroupMembers,
                netStatus* status);

	//PURPOSE
	//  Returns whether we are currently joining or not
	bool IsJoining();

    //PURPOSE
    //  Joins a network bot to a session hosted by a remote peer.
    //PARAMS
    //  joiner          - Local network bot joining the session.
    //  sessionInfo     - Session info identifying the session.
    //  netMode         - Network mode.
    //  slotType        - SLOT_PUBLIC or SLOT_PRIVATE.
    //                    The type of slot to which the gamer is eventually
    //                    joined is up to the host.
    //  joinerData      - Application data sent to the host in the join request
    //                    (in the snMsgJoinRequest message).
    //  sizeofJoinerData    - Size of application data.
    //  status          - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //  For PS3 LAN sessions be sure to disable crypto on the socket
    //  used by the connection manager passed to Init().
    //
    //  This initiates an asynchronous operation that is not complete until
    //  the Pending() function of the status object returns false.
    bool JoinRemoteBot(const rlGamerInfo& joiner,
					   const rlSessionInfo& sessionInfo,
					   rlNetworkMode netMode,
					   const rlSlotType slotType,
					   const void* joinerData,
					   const unsigned sizeofJoinerData,
					   netStatus* status);

    //PURPOSE
    //  Joins a network bot to a session hosted by a local peer.
    //PARAMS
    //  joiner          - Local gamer joining the session.
    //  sessionInfo     - Session info identifying the session.
    //  netMode         - Network mode.
    //  slotType        - SLOT_PUBLIC or SLOT_PRIVATE.
    //                    The type of slot to which the gamer is eventually
    //                    joined is up to the host.
    //  createFlags     - From rlSession::CreateFlags.
    //  joinerData      - Application data sent to the host in the join request
    //                    (in the snMsgJoinRequest message).
    //  sizeofJoinerData    - Size of application data.
    //  status          - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //  For PS3 LAN sessions be sure to disable crypto on the socket
    //  used by the connection manager passed to Init().
    //
    //  This initiates an asynchronous operation that is not complete until
    //  the Pending() function of the status object returns false.
    bool JoinLocalBot(const rlGamerInfo& joiner,
					  const rlSessionInfo& sessionInfo,
					  rlNetworkMode netMode,
					  const rlSlotType slotType,
					  const void* joinerData,
					  const unsigned sizeofJoinerData,
					  netStatus* status);

    //PURPOSE
    //  Destroys the local instance of the session.
    //PARAMS
    //  status  - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //  This initiates an asynchronous operation that is not complete until
    //  the Pending() function of the status object returns false.
    bool Destroy(netStatus* status);

    //PURPOSE
    //  Removes a local gamer from the session.
    //PARAMS
    //  localGamerIndex - Index of gamer to leave.
    //  status          - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //
    //  This initiates an asynchronous operation that is not complete until
    //  the Pending() function of the status object returns false.
    bool Leave(const int localGamerIndex, netStatus* status);

    //PURPOSE
    //  Removes a locally created network bot from the session.
    //PARAMS
    //  botGamerHandle - Gamer handle of the bot to remove from the session
    //  status         - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //
    //  This initiates an asynchronous operation that is not complete until
    //  the Pending() function of the status object returns false.
    bool LeaveLocalBot(const rlGamerHandle &botGamerHandle, netStatus* status);

    //PURPOSE
    //  Removes remote gamers from the session.  Callable only by the host.
    //PARAMS
    //  kickees     - Remote gamers to kick.
    //  numKickees  - Number of remote gamers to kick.
    //  kickType    - One of the KickType enumerants.
    //  status      - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //
    //  This initiates an asynchronous operation that is not complete until
    //  the Pending() function of the status object returns false.
    bool Kick(const rlGamerHandle* kickees,
                const unsigned numKickees,
                const KickType kickType,
                netStatus* status);

	//PURPOSE
	//  Returns whether we are in the process of creating a session as host
	bool IsHostPending();

	//PURPOSE
	//  Has pending joining gamers
	bool HasJoinersPending();

	//PURPOSE
	//  Enables matchmaking
	//  Sessions can be started with RL_SESSION_CREATE_FLAG_MATCHMAKING_ADVERTISE_DISABLED
	//  This allows advertising to be enabled / started after the session has been activated
	bool EnableMatchmakingAdvertising(netStatus* status);

    //PURPOSE
    //  Changes the attributes of the session.
    //NOTES
    //  Only attributes contained in attrs will be changed
    bool ChangeAttributes(const rlMatchingAttributes& newAttrs, netStatus* status);

	//PURPOSE
	//  Enables/disables presence flags.  See the rlSessionPresenceFlags enum.
	bool ModifyPresenceFlags(const unsigned presenceFlags, netStatus* status);
	unsigned GetPresenceFlags() const;
	bool SetPresenceEnabled(const bool enabled, netStatus* status);
	bool IsPresenceEnabled() const;

    //PURPOSE
    //  Returns true if invites are enabled.
    bool IsInvitable() const;

	//PURPOSE
	//  Returns true if another player can join this session via presence.
	bool IsJoinableViaPresence() const;

    //PURPOSE
    //  Sends invites to remote gamers to join the session.
    //PARAMS
    //  gamerHandles    - Array of gamer handles representing
    //                    remote gamers.
    //  numGamerInfos   - Length of gamer handle array.
    //  subject         - Subject line of invite message.  PS3 only.
    //  salutation      - String to display with invitation.  Pass NULL
    //                    for a default salutation.
    //  status          - Optional status object that can be polled for
    //                    completion.
    //NOTES
    //  Per invocation, the number of gamer handles is limited to
    //  RL_MAX_GAMERS_PER_SESSION - 1.  To send more invites call this
    //  function multiple times.
    bool SendInvites(const rlGamerHandle* gamerHandles,
                    const unsigned numGamers,
                    const char* subject,
                    const char* salutation,
                    netStatus* status);

    //PURPOSE
    //  Sends invites to all members of platforms party to join this session.
    //PARAMS
    //  localGamerIndex - Local gamer sending invites
    //  status          - Optional status object that can be polled for
    //                    completion.
    bool SendPartyInvites(const int localGamerIndex,
                          netStatus* status);
	
    //PURPOSE
    //  Migrate the session to a new host
    bool Migrate();

    //PURPOSE
    //  Cancel any active or queued migration tasks
    void CancelMigrationTasks();
	void MarkAsNotMigrating(const u64 peerId);
	bool DoesAllowMigration(const u64 peerId);

	//PURPOSE
	//  Cancel all running tasks
	void CancelAllSerialTasks();
	void CancelAllConcurrentTasks();

	//PURPOSE
	//  Access to gamer established state
	void MarkAsEstablished(const rlGamerHandle& hGamer);
	bool IsGamerEstablished(const rlGamerHandle& hGamer);

	//PURPOSE
	//  Reserve slots for the gamers to join.
	bool ReserveSlots(const rlGamerHandle* gamerHandles,
                      const unsigned numGamerHandles,
                      const rlSlotType slotType,
					  const unsigned reservationTimeout,
					  bool bIsJoinRequest,
					  unsigned userParam = 0);

	//PURPOSE
	//  Returns true if the gamer has a slot reservation.
	bool HasSlotReservation(const rlGamerHandle& gamerHandle) const;

	//PURPOSE
	//  Returns numbers of slot reservations with matching param
	unsigned GetNumSlotsWithParam(unsigned userParam);
	
	//PURPOSE
	//  Frees reserved slots for gamers
	void FreeSlots(const rlGamerHandle* gamerHandles,
				   const unsigned numGamerHandles);

    //PURPOSE
    //  Updates the state of the session.  This function should be called on a
    //  regular interval not to exceed 100 milliseconds.
    //PARAMS
    //  curTime - Current time in milliseconds.
    void Update(const unsigned curTime);

    //PURPOSE
    //  Returns the session configuration.
    //NOTES
    //  Calling this function prior to completing session creation will
    //  result in undefined behavior.
    const rlSessionConfig& GetConfig() const;

    //PURPOSE
    //  Retrieves a gamer info given a gamer id.
    //RETURNS
    //  True on success.
    bool GetGamerInfo(const rlGamerId& gamerId, rlGamerInfo* gamerInfo) const;
    bool GetGamerInfo(const rlGamerHandle& gamerHandle, rlGamerInfo* gamerInfo) const;

    //PURPOSE
    //  Gathers session members owned by the given peer.
    //RETURNS
    //  The number of gamers gathered.  If the gamers array
    //  is too small this value may be less than the number of gamers
    //  actually owned by the peer.
    unsigned GetGamers(const u64 peerId, rlGamerInfo gamers[], const unsigned maxGamers) const;
    unsigned GetGamers(const u64 peerId, rlGamerId gamerIds[], const unsigned maxGamers) const;
    unsigned GetGamers(const u64 peerId, rlGamerHandle gamerHandle[], const unsigned maxGamers) const;

	//PURPOSE
	//  Gathers session members owned by the given peer with this endpoint
	//RETURNS
	//  The number of gamers gathered.  If the gamers array
	//  is too small this value may be less than the number of gamers
	//  actually owned by the peer.
	unsigned GetGamers(const EndpointId endpointId, rlGamerInfo gamers[], const unsigned maxGamers) const;
	unsigned GetGamers(const EndpointId endpointId, rlGamerId gamerIds[], const unsigned maxGamers) const;
	unsigned GetGamers(const EndpointId endpointId, rlGamerHandle gamerHandle[], const unsigned maxGamers) const;

    //PURPOSE
    //  Gathers current session members.
    //RETURNS
    //  The number of gamers gathered.  If the gamers array
    //  is too small this value may be less than the number of gamers
    //  actually in the session.
    unsigned GetGamers(rlGamerInfo gamers[], const unsigned maxGamers) const;
    unsigned GetGamers(rlGamerId gamerIds[], const unsigned maxGamers) const;
    unsigned GetGamers(rlGamerHandle gamerHandles[], const unsigned maxGamers) const;

    //PURPOSE
    //  Given an array of gamer IDs, populate an array of gamer handles.
    //RETURNS
    //  Number of gamer handles populated.
    unsigned GetGamerHandles(
		const rlGamerId gamerIds[],
		const unsigned numGamerIds,
		rlGamerHandle gamerHandles[],
		const unsigned maxGamers) const;

    //PURPOSE
    //  Given an array of gamer handles, populate an array of gamer IDs.
    //RETURNS
    //  Number of gamer IDs populated.
    unsigned GetGamerIds(
		const rlGamerHandle gamerHandles[],
		const unsigned numGamerHandles,
		rlGamerId gamerIds[],
		const unsigned maxGamers) const;

    //PURPOSE
    //  Returns number of session members owned by the given peer.
    unsigned GetGamerCount(const u64 peerId) const;

    //PURPOSE
    //  Returns number of session members.
    unsigned GetGamerCount() const;

    //PURPOSE
    //  Returns number of session members owned by the local peer.
    unsigned GetLocalGamerCount() const;

	//PURPOSE
	//  Returns the maximum number of slots of the given type.
	unsigned GetMaxSlots(const rlSlotType slotType) const;

	//PURPOSE
	//  Returns the number of empty slots of the given type.
    unsigned GetEmptySlots(const rlSlotType slotType) const;

    //PURPOSE
    //  Returns the number of available slots of the given type.
    //  A slot can be empty but unavailable if it's been reserved.
    unsigned GetAvailableSlots(const rlSlotType slotType) const;

	//PURPOSE
	//  Returns the slot type of a gamer
	rlSlotType GetSlotType(const rlGamerId& gamerId) const;
	rlSlotType GetSlotType(const rlGamerHandle& gamerHandle) const;

    //PURPOSE
    //  Returns number of remote peers in the session.
    unsigned GetPeerCount() const;

    //PURPOSE
    //  Retrieves the connection Id of the peer.
    int GetPeerCxn(const u64 peerId) const;

	bool GetPeerEndpointId(const u64 peerId, EndpointId* endpointId) const;
	EndpointId GetPeerEndpointId(const u64 peerId) const;
	u64 GetPeerIdByEndpointId(const EndpointId epId) const;

    //PURPOSE
    //  Whether we can send to this peer
    bool CanSendToPeer(const u64 peerId) const;

    //PURPOSE
    //  Returns true if the gamer is a member of the session.
    bool IsMember(const rlGamerHandle& gamerHandle) const;

    //PURPOSE
    //  Returns true if the gamer is a member of the session.
    bool IsMember(const rlGamerId& gamerId) const;

    //PURPOSE
    //  Adds a delegate that will be called with event notifications.
    void AddDelegate(Delegate* dlgt);

    //PURPOSE
    //  Removes a delegate.
    void RemoveDelegate(Delegate* dlgt);

    //PURPOSE
    //  Reliably sends a message to a specific peer on the channel used
    //  for session communication.
    //PARAMS
    //  peerId      - Peer id of the recipient.
    //  msg         - The message.
    //  sendFlags   - Bit mask composed of netSendFlags enum.
    template<typename T>
    void SendMsgReliable(const u64 peerId, const T& msg, const unsigned sendFlags) const
    {
        SendMsg(peerId, msg, (sendFlags | NET_SEND_RELIABLE));
    }

	template<typename T>
	void SendMsg(const u64 peerId, const T& msg, const unsigned sendFlags) const
	{
		const snPeer* peer = this->FindPeerById(peerId);

		if(peer)
		{
			RAGE_CONDLOGF(peer->IsLocal(),
				snet,
				DIAG_SEVERITY_WARNING,
				"Why am I sending a message to myself?");

            if(peer->GetCxnId() > 0)
			{
				m_CxnMgr->Send(peer->m_CxnId, msg, sendFlags, NULL);
			}
		}
	}

    //PURPOSE
    //  Reliably broadcasts a message to all peers on the channel used
    //  for session communication.
    //PARAMS
    //  msg         - The message
    //  sendFlags   - Bit mask composed of netSendFlags enum.
    template<typename T>
    void BroadcastMsgReliable(const T& msg, const unsigned sendFlags) const
    {
		BroadcastMsg(msg, (sendFlags | NET_SEND_RELIABLE));
    }

	//PURPOSE
	//  Broadcasts a message to all peers on the channel used
	//  for session communication.
	//PARAMS
	//  msg         - The message
	//  sendFlags   - Bit mask composed of netSendFlags enum.
	template<typename T>
	void BroadcastMsg(const T& msg, const unsigned sendFlags) const
	{
		for(int i = 0; i < (int) m_NumPeers; ++i)
		{
			const snPeer* peer = m_Peers[i];

			if(peer->GetCxnId() < 0)
			{
				continue;
			}

			if(!(m_CxnMgr->IsOpen(peer->GetCxnId()) || m_CxnMgr->IsPendingOpen(peer->GetCxnId())))
			{
				continue;
			}

			m_CxnMgr->Send(peer->GetCxnId(), msg, sendFlags, NULL);
		}
	}

    //PURPOSE
    //  Set app-specific data returned to QoS queries.  This can be set at any
    //  time.  To reset, call with a null data pointer.
    //PARAMS
    //  data        - Pointer to data
    //  size        - Size of user data (max is RL_MAX_QOS_USER_DATA_SIZE)
    //RETURNS
    //  True in success, false on failure.
    bool SetQosUserData(const u8* data,
                        const u16 size);

	//PURPOSE
	//  Set app-specific data returned to config queries.  This can be set at any
	//  time.  To reset, call with a null data pointer.
	//PARAMS
	//  data        - Pointer to data
	//  size        - Size of user data (max is RL_MAX_SESSION_USER_DATA_SIZE)
	//RETURNS
	//  True in success, false on failure.
	bool SetSessionUserData(const u8* data,
                            const u16 size);

#if RSG_ORBIS
	// PURPOSE
	// Returns the PS4 Web API SessionId
	const rlSceNpSessionId& GetWebApiSessionId() const;

	// PURPOSE
	// Set the PS4 Web API Session Id. This can be set at any time.
	void SetWebApiSessionId(const rlSceNpSessionId& sessionId);
#endif

	//PURPOSE
	//   Returns true if we're accepting join requests.
	bool IsAcceptingJoinRequests() const;

	//PURPOSE
	//  Returns a peer ID given a connection ID.
	u64 GetPeerIdByCxn(int cxnId) const;
	
    //PURPOSE
    //  Cancels the operation associated with the status object.
    //WARNING
    //  The only cancelable operation is QueryConfig().  All other operations
    //  will fail to cancel.
    void Cancel(netStatus* status);

	//PURPOSE
	// Access to in-flight display name notification tasks
	bool HasInFlightDisplayNameTasks();
	void CancelDisplayNameTask(const rlGamerHandle& handle);
	void CancelDisplayNameTasks();
	void NotifyDisplayNameTasksAddedPlayer(); 

	//PURPOSE
	//  Returns the number of reserved slots of the given type.
	unsigned NumReservedSlots(const rlSlotType slotType) const;

	// PURPOSE
	//	Returns the number of reserved slots
	unsigned TotalReservedSlots() const { return m_NumReservedSlots; }

	//PURPOSE
	//  Removes the local player from the session
	void RemoveLocalFromSession();


private:

    //PURPOSE
    //  Start listening for network events.
    void EnableNetwork();

    //PURPOSE
    //  Stop listening for network events.
    void DisableNetwork();

    //PURPOSE
    //  Enables/disables acceptance of join requests.
    void SetAcceptingJoinRequests(const bool accepting);

    //PURPOSE
    //  Retrieves an array of candidates for hosting the session.  Used
    //  during host migration.
    unsigned GetHostCandidates(rlPeerInfo (&candidates)[RL_MAX_GAMERS_PER_SESSION]);

    snPeer* AddPeer(const int cxnId,
					const EndpointId endpointId,
                    const rlPeerInfo& peerInfo);

    //PURPOSE
    //  Removes a peer from the session.
    //  The peer must contain no gamers at the time of removal.
    void RemovePeer(snPeer* peer);
    void RemovePeer(const u64 peerId);

    //PURPOSE
    //  Removes a peer (and all gamers local to the peer) from the session
    //  and the service (XBL) session.
    //PARAMS
    //  peerInfo    - Peer to drop.
    //  task        - 
    //  status      - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //  If this is called on the host all peers are notified of the drop.
    bool DropPeer(const u64 peerId,
                    snLeaveGamersFromRlineTask* task,
                    netStatus* status);

    //PURPOSE
    //  Closes a peer's network connection, but leaves the peer attached
    //  to the session.
    void DisconnectPeer(snPeer* peer);
    void DisconnectPeer(const u64 peerId);

    bool HasPeer(const u64 peerId) const;

    bool IsConnectedToPeer(const u64 peerId) const;

    //PURPOSE
    //  Processes join requests. Called automatically by 
	//  PrepareToHandleJoinRequest() or it's async task.
    void HandleJoinRequest(const snMsgJoinRequest& rqst,
                            const netAddress& sender,
                            const netSequence seq);


    //PURPOSE
    //  Attempts to accept a join request for a single joiner.
    //  This function does not process group joins.
    //  Group joins are handled in HandleJoinRequest where slots
    //  are reserved for the group.  Each individual from the
    //  group must then request to join, at which point they will
    //  pass through AcceptJoinRequest.
    void AcceptJoinRequest(const snMsgJoinRequest& rqst,
                            const netAddress& sender,
                            const netSequence seq,
                            const void* responseData,
                            const unsigned sizeofResponseData);

    //PURPOSE
    //  Denies join requests.
    void DenyJoinRequest(const netAddress& sender,
                        const snJoinResponseCode responseCode,
                        const void* responseData,
                        const unsigned sizeofResponseData);

    //PURPOSE
    //  Handles the host leaving while joining commands.
    void HandleHostLeftWhileJoiningCmd(const netRequest& rqst,
                                       const snMsgHostLeftWhilstJoiningCmd& cmd);

    //PURPOSE
    //  Handles RemoveGamer commands.
    void HandleRemoveGamerCmd(const snPeer* requestingPeer,
                                const snMsgRemoveGamersFromSessionCmd& cmd);

    //PURPOSE
    //  Process gamer info requests.
    void HandleGamerInfoRequest(const snMsgRequestGamerInfo& rqst,
                                const int cxnId);

    //PURPOSE
    //  Inform the joiner of existing peers.
    void InformJoinerOfPeers(const rlGamerInfo& joiner, const unsigned numReservedSlots, ReservedSlot* reservedSlots);

    //PURPOSE
    //  Inform existing peers of the joiner.
	void InformPeersOfJoiner(const rlGamerInfo& joiner,const rlGamerHandle* reserveMembers = NULL, const unsigned reserveSize = 0);

    //PURPOSE
    //  Sends a snMsgAddGamerToSessionCmd.
    bool SendAddGamerCommandToPeer(const rlGamerHandle& gamerHandle, const int cxnId, ReservedSlot* reservedSlots = NULL, const unsigned numReservedSlots = 0);

    snGamer* FindGamerById(const rlGamerId& gamerId);
    const snGamer* FindGamerById(const rlGamerId& gamerId) const;

    snGamer* FindGamerByHandle(const rlGamerHandle& gamerHandle);
    const snGamer* FindGamerByHandle(const rlGamerHandle& gamerHandle) const;

    snPeer* FindPeerById(const u64 peerId);
    const snPeer* FindPeerById(const u64 peerId) const;

    snPeer* FindPeerByCxn(const int cxnId);
    const snPeer* FindPeerByCxn(const int cxnId) const;

    snPeer* FindPeerByEndpointId(const EndpointId endpointId);
    const snPeer* FindPeerByEndpointId(const EndpointId endpointId) const;

	// PURPOSE
	//	Returns the reserved slots
	ReservedSlot* GetReservedSlots() { return &m_ReservedSlots[0]; }

    //PURPOSE
    //  Returns a pointer to the reserved slot, or NULL.
    ReservedSlot* FindSlotReservation(const rlGamerHandle& gamerHandle);
    const ReservedSlot* FindSlotReservation(const rlGamerHandle& gamerHandle) const;

    //PURPOSE
    //  Fills a slot reservation.
    bool FillSlotReservation(const rlGamerHandle gamerHandle);

    //PURPOSE
    //  Process reserved slots, timing out the ones that have expired.
    void UpdateReservedSlots();

#if RSG_DURANGO
    //PURPOSE
	//	Process users without display names, retrieve them as necessary
	void UpdateDisplayNames();
#endif

	//PURPOSE
	//  Removes the local player from the session
	//void RemoveLocalFromSession();

 	//PURPOSE
	//  Returns an existing, pending snJoinGamersToRlineTask or creates
	//	a new snJoinGamersToRlineTask. 
	//  existingJoinTask is set based on which
	snJoinGamersToRlineTask* GetJoinGamersToRlineTask(bool& existingJoinTask);

    //PURPOSE
    //  Adds a gamer to the session.
    //NOTES
    //  This does not add the gamer to the service (XBL) session.
    bool AddGamer(const rlGamerInfo& gamerInfo,
                    const rlSlotType slotType,
                    const void* data,
                    const unsigned sizeofData);

    //PURPOSE
    //  Removes a gamer from the session.
    //NOTES
    //  This does not remove the gamer from the service (XBL) session.
    bool RemoveGamer(const rlGamerId& gamerId, const int contextParameter);
    bool RemoveGamer(const rlGamerHandle& gamerHandle, const int contextParameter);

	//PURPOSE
	//  Returns whether we are currently dropping this gamer
	bool IsDropping(const rlGamerHandle& gamerHandle);

    //PURPOSE
    //  Removes gamers from the session and the service (XBL) session,
    //  and notifies all peers of the operation.
    //PARAMS
    //  requestingPeer  - Peer requesting the drop.
    //  gamerHandles    - Gamers to remove.
    //  numGamers       - Number of gamers to remove.
    //  task            - 
    //  status          - Optional status object that can be polled for completion.
    //RETURNS
    //  True if the asynchronous operation was initiated.
    //NOTES
    //  If this is called on the host all peers are notified of the drop.
    bool DropGamers(const snPeer* requestingPeer,
                    const rlGamerHandle* gamerHandles,
                    const int numGamers,
                    const KickType kickType,
                    snLeaveGamersFromRlineTask* task,
					const int removeReason,
                    netStatus* status);

    //PURPOSE
    //  After receiving snMsgSessionMemberIds from the host
    //  ReconcileMembersWithHost() locally removes gamers that are no
    //  longer members, and requests the host to send a
    //  snMsgAddGamerToSessionCmd for each gamer that is a member
    //  but not present locally.
    //PARAMS
    //  members     - Current session members, sent from the host.
    //  numMembers  - Number of members in the array.
    void ReconcileMembersWithHost(const rlGamerId* members,
                                const unsigned numMembers);

    template<typename T>
    bool ValidateRqst(const netRequest* rqst,
                        T* msg) const;

    template<typename T>
    bool ValidateMsg(const netEventFrameReceived* frame,
                    T* msg) const;

    void OnNetEvent(netConnectionManager* cxnMgr,
                    const netEvent* evt);

    void OnRequest(netTransactor* transactor,
                    netRequestHandler* handler,
                    const netRequest* rqst);

    snPeer* ConnectToPeer(const rlPeerInfo& peerInfo,
							const int cxnId,
                            const EndpointId endpointId,
                            const void* bytes,
                            const unsigned numBytes,
                            netStatus* status);

    template<typename T>
    void BroadcastCommand(const T& msg,
                        const snPeer* except)
    {
        for(unsigned i = 0; i < m_NumPeers; ++i)
        {
            const snPeer* peer = m_Peers[i];

            if(!m_CxnMgr->IsOpen(peer->GetCxnId()))
            {
                continue;
            }

            if(except && peer->m_PeerInfo == except->m_PeerInfo)
            {
                continue;
            }

            m_Transactor.SendRequest(peer->m_CxnId, NULL, msg, 0, NULL);
        }
    }

    void AddTask(snTask* task);
    void AddTaskLifo(snTask* task);
    void RemoveTask(snTask* task);
	void AddConcurrentTask(snTask* task);

    void UpdateTasks(const unsigned timeStep);

    void Clear();

    //PURPOSE
    //  Update the presence attributes for the game session
    void UpdatePresenceAttributes(const int localGamerIndex,
                                  const bool leftGame,
                                  const bool isHost);

    //Used in debug builds to print the session members.
    void PrintMembers() const;

	//Used to dispatch events
	void DispatchEvent(snEvent* pEvent);

    sysMemAllocator* m_Allocator;
    snSessionOwner m_Owner;
    netConnectionManager* m_CxnMgr;
    netConnectionManager::Delegate m_CxnMgrDlgt;
    rlSession m_RlSession;
    snPeer m_LocalPeer;
    u64 m_HostPeerId;

    netRequestHandler m_RqstHandler;
    netTransactor m_Transactor;

    typedef inlist<snPeer, &snPeer::m_ListLink> PeerList;
    typedef inlist<snGamer, &snGamer::m_ListLink> GamerList;

    snGamer m_GamerPile[RL_MAX_GAMERS_PER_SESSION];
    GamerList m_GamerPool;
    snGamer* m_Gamers[RL_MAX_GAMERS_PER_SESSION];
    unsigned m_NumGamers;

    snPeer m_PeerPile[RL_MAX_GAMERS_PER_SESSION];
    PeerList m_PeerPool;
    snPeer* m_Peers[RL_MAX_GAMERS_PER_SESSION];
    unsigned m_NumPeers;

    unsigned m_NumOccupiedPrivSlots;

	ReservedSlot m_ReservedSlots[RL_MAX_GAMERS_PER_SESSION];
    unsigned m_NumReservedSlots;
	bool m_bNeedToUpdateReservations;
	u32 m_uTimeOfLastReservationUpdate;
	netStatus m_UpdateReservationStatus;

	// Display names
#if RSG_DURANGO
	bool m_DirtyDisplayNames;
	netStatus m_DisplayNameStatus;
	rlGamerHandle m_GamerHandles[RL_MAX_GAMERS_PER_SESSION];
	rlDisplayName m_DisplayNames[RL_MAX_GAMERS_PER_SESSION];
	unsigned m_PendingDisplayNames;
	unsigned m_DisplayNameRetrievalTime;
	unsigned m_CurrentDisplayNameBackoffTimer;
	static const int RETRY_TIMERS = 4;
	static const int DISPLAYNAME_FAILURE_RETRY_TIMER[RETRY_TIMERS];
#endif

    //Delegates session events.
    Delegator m_Delegator;

    typedef inlist<snTask, &snTask::m_ListLink> TaskQ;

    TaskQ m_TaskQ;

    TaskQ m_ConcurrentTasks;

	bool m_AllowRemoveTask;

    unsigned m_LastTime;
	static unsigned sm_SelfRemoveStallTime; 
	static bool sm_ConnectToPeerChecks;

    unsigned m_ChannelId;

    OUTPUT_ONLY(char m_Name[64]);

    bool m_NetworkEnabled   : 1;
    bool m_AcceptingJoinRequests    : 1;
    bool m_PresenceEnabled  : 1;
    bool m_Migrating        : 1;
    bool m_IsInitialized    : 1;
	bool m_IsEstablished	: 1;

	int m_LocalOwnerIndex;

    //Presence attributes for this session
	char m_AttrSessionToken[RLSC_PRESENCE_ATTR_NAME_MAX_SIZE];
	char m_AttrSessionId[RLSC_PRESENCE_ATTR_NAME_MAX_SIZE];
	char m_AttrSessionInfo[RLSC_PRESENCE_ATTR_NAME_MAX_SIZE];
	char m_AttrIsHost[RLSC_PRESENCE_ATTR_NAME_MAX_SIZE];
	char m_AttrIsJoinable[RLSC_PRESENCE_ATTR_NAME_MAX_SIZE];
};

}   //namespace rage;

#endif  //SNET_SESSION_H
