// 
// snet/tasks.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNET_TASKS_H
#define SNET_TASKS_H

#include "atl/inlist.h"
#include "messages.h"
#include "net/connectionmanager.h"
#include "net/transaction.h"
#include "net/status.h"
#include "net/tunneler.h"
#include "rline/rlsession.h"
#include "rline/rltask.h"
#include "snet.h"

//
//  Tasks are essentially little state machines that carry out
//  asynchronous operations over a period of time.
//  After calling Start(), a task's Update() function must be called
//  every frame with the number of milliseconds elapsed since the last
//  call to Update(), until calling Finish().
//

namespace rage
{

class snSession;

RAGE_DECLARE_SUBCHANNEL(snet, tasks)

#define SN_TASK_DECL(name, channel)\
    RL_TASK_DECL(name)\
    RL_TASK_USE_CHANNEL(channel)\
    static const void* TASK_TYPE_ID(){static int MY_TYPE_ID; return &MY_TYPE_ID;}\
    virtual const void* TaskTypeId() const{return TASK_TYPE_ID();}\
    static bool ISA(const snTask* t){return t->TaskTypeId() == TASK_TYPE_ID();}

//PURPOSE
//  Base class for all tasks
class snTask : public rlTask<snSession>
{
public:

    snTask()
        : m_DeleteWhenFinished(true)
    {
		m_TokenPrefix[0] = '\0';
    }

	bool ConfigureContext(rlTask<snSession>* pTask, snSession* pSession);
	void RefreshLogContext();

    virtual void Finish(const FinishType finishType, const int resultCode = 0)
    {
        this->rlTask<snSession>::Finish(finishType, resultCode);
    }

    virtual const void* TaskTypeId() const
    {
        return NULL;
    }

#if !__NO_OUTPUT
	virtual const char* GetTaskLogContext() const
	{
		return m_TokenPrefix;
	}
#endif

    inlist_node<snTask> m_ListLink;

    bool m_DeleteWhenFinished : 1;
	char m_TokenPrefix[32];
};

//PURPOSE
//  Queries the host for the session configuration.
class snQueryHostConfigTask : public rlTaskBase
{

public:

    enum
    {
        DEFAULT_TIMEOUT_MS      = 1250,
        DEFAULT_MAX_ATTEMPTS    = 10,
    };

public:

	RL_TASK_USE_CHANNEL(snet_tasks);
	RL_TASK_DECL(snQueryHostConfigTask);

    snQueryHostConfigTask();
    ~snQueryHostConfigTask();

    bool Configure(netConnectionManager* cxnMgr,
                    const unsigned channelId,
                    const EndpointId hostEndpointId,
                    const rlSessionInfo& sessionInfo,
                    rlSessionConfig* sessionConfig,
                    rlPeerInfo* hostPeerInfo);

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

    virtual bool IsCancelable() const
    {
        return true;
    }

    static void SetRetryPolicy(const unsigned timeoutMs, const unsigned maxAttempts);

private:

    virtual void DoCancel();

    bool SendQuery();

    void OnResponse(netTransactor* transactor,
                    netResponseHandler* handler,
                    const netResponse* resp);

    netTransactor m_Transactor;
    unsigned m_ChannelId;
    rlSessionInfo m_SessionInfo;
    netResponseHandler m_RespHandler;
    unsigned m_TxId;
    unsigned m_QueryCount;
    EndpointId m_HostEndpointId;

    rlSessionConfig* m_SessionConfig;
    rlPeerInfo* m_HostPeerInfo;

    static unsigned sm_TimeoutMs;
    static unsigned sm_MaxAttempts;
};

//PURPOSE
//  Joins gamers to the the rlSession.
class snJoinGamersToRlineTask : public snTask
{
public:

    SN_TASK_DECL(snJoinGamersToRlineTask, snet_tasks);

    snJoinGamersToRlineTask();

    ~snJoinGamersToRlineTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    bool Configure(snSession* ctx);

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    //  gamerHandles    - Gamers to remove.
    //  numGamers       - Number of gamers to remove.
    bool Configure(snSession* ctx,
                   const rlGamerHandle* gamerHandles,
				   const rlGamerId* gamerIds,
                   const unsigned numGamers);

    bool AddGamer(const rlGamerHandle& gamerHandle,
				  const rlGamerId& gamerId,
				  const rlSlotType slot,
				  const bool OUTPUT_ONLY(isJoinRequest),
                  netStatus** status);

	void RemoveDroppedGamers();
    void DropGamers(const rlGamerId gamerIds[], const unsigned int numGamerIds);
	void DropGamers();

	bool IsWaitingForAddGamers();
	bool IsAdding(const rlGamerId gamerIds[], const unsigned int numGamerIds);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:

    enum
    {
        STATE_INVALID = -1,
        STATE_WAITING_FOR_ADD_GAMER_TASKS,
        STATE_JOINING,
    };

    int m_State;
    netStatus m_MyStatus;
    rlGamerHandle m_GamerHandles[RL_MAX_GAMERS_PER_SESSION];
	rlGamerId m_GamerIds[RL_MAX_GAMERS_PER_SESSION];
	rlSlotType m_Slots[RL_MAX_GAMERS_PER_SESSION];
    netStatus m_AddGamerStatus[RL_MAX_GAMERS_PER_SESSION];
	bool m_Dropped[RL_MAX_GAMERS_PER_SESSION];

	unsigned m_NumGamers;
};

class snNotifyAddGamerTask;

//PURPOSE
//  Indicates the end of a join, when all existing remote players
//  have been added to the session
class snJoinCompleteTask : public snTask
{
public:
	SN_TASK_DECL(snJoinCompleteTask, snet_tasks);

	snJoinCompleteTask();
	~snJoinCompleteTask();

	bool Configure(snSession* ctx);
	void Finish(const FinishType finishType, const int resultCode = 0);
	void Update(const unsigned timeStep);

#if RSG_DURANGO
	void AddDependency(netStatus** status, snNotifyAddGamerTask* task);
#endif
	
private:

#if RSG_DURANGO
	unsigned m_NumDependencies;
	netStatus m_DependencyStatus[RL_MAX_GAMERS_PER_SESSION];
	snNotifyAddGamerTask* m_DependencyTasks[RL_MAX_GAMERS_PER_SESSION];
#endif
};

//PURPOSE
class snLeaveGamersFromRlineTask : public snTask
{
public:

    SN_TASK_DECL(snLeaveGamersFromRlineTask, snet_tasks);

    snLeaveGamersFromRlineTask();
    ~snLeaveGamersFromRlineTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    //  gamerHandles    - Gamers to remove.
    //  numGamers       - Number of gamers to remove.
    bool Configure(snSession* ctx,
                    const rlGamerHandle* gamerHandles,
                    const unsigned numGamers);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

	virtual rlTaskBase::ShutdownBehaviour GetShutdownBehaviour() { return rlTaskBase::EXECUTE_ON_SHUTDOWN; }

private:

    void LeaveSession();

    enum
    {
        STATE_INVALID = -1,
        STATE_LEAVING_MATCH,
        STATE_LEAVING_SESSION,
    };

    int m_State;

    netStatus m_MyStatus;
    rlGamerHandle m_GamerHandles[RL_MAX_GAMERS_PER_SESSION];
    unsigned m_NumGamers;
};

//PURPOSE
//  Handles join requests from remote peers.  This task is
//  run only on the host.  If the joiner requests a private
//  slot and none are available then put him in a public slot.
class snHandleJoinRequestTask : public snTask
{

public:

    enum
    {
        DEFAULT_TIMEOUT_MS = 10000,
    };

public:
 
    SN_TASK_DECL(snHandleJoinRequestTask, snet_tasks);

     snHandleJoinRequestTask();
    ~snHandleJoinRequestTask();

    //PURPOSE
    //  Configures the task to handle a join request.
    //PARAMS
    //  ctx     - The session.
    //  rqst    - The request from the remote peer.
    bool Configure(snSession* ctx,
                    const snMsgJoinRequest& rqst,
                    const bool peerAlreadyExists);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:

    snMsgJoinRequest m_Rqst;
    bool m_PeerAlreadyExists;
};

//PURPOSE
//  Attempts to connect to a peer
class snConnectToPeerTask : public snTask
{
public:

    SN_TASK_DECL(snConnectToPeerTask, snet_tasks);

    snConnectToPeerTask();

    ~snConnectToPeerTask();

    //PURPOSE
    //  Configures the task to connect to a peer.
    //PARAMS
    //  ctx         - The session.
    //  peerAddr    - Peer address to which a connection will be made.
    //  relayAddr   - Relay address for peer.
    //  netMode     - RL_NETMODE_LAN or RL_NETMODE_ONLINE.
    bool Configure(snSession* ctx,
                    const rlPeerAddress& peerAddr,
                    const rlNetworkMode netMode);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

    EndpointId GetEndpointId() const;

	void CloseConnection();

    int GetCxnId() const;

private:

    enum
    {
        STATE_INVALID = -1,
        STATE_OPENING_TUNNEL,
        STATE_CONNECT_TO_PEER,
    };

    int m_State;

    rlPeerAddress m_PeerAddr;
    netTunnelRequest m_TunnelRqst;
    EndpointId m_EndpointId;

    int m_CxnId;

    netStatus m_MyStatus;
};

//PURPOSE
//  Adds a remote gamer to the session.
class snAddRemoteGamerTask : public snTask
{
public:

    SN_TASK_DECL(snAddRemoteGamerTask, snet_tasks);

    snAddRemoteGamerTask();
    ~snAddRemoteGamerTask();

    //PURPOSE
    //  Configures the task to add a remote gamer to the session.
    //PARAMS
    //  ctx     - The session.
    //  agCmd   - The command from the host.
    bool Configure(snSession* ctx,
                    const snMsgAddGamerToSessionCmd& agCmd);

    virtual void Start();
    virtual void Finish(const FinishType finishType, const int resultCode = 0);
    virtual void Update(const unsigned timeStep);
    virtual void Drop(const rlGamerId gamerIds[], const unsigned int numGamerIds);

private:

    enum
    {
        STATE_INVALID = -1,
        STATE_CONNECTING_TO_PEER,
		STATE_WAIT_FOR_JOIN,
        STATE_ADD_GAMER,
    };

    bool m_GamerWasDroppedWhileAdding;

    int m_State;

    snMsgAddGamerToSessionCmd m_AgCmd;
	
    netStatus m_MyStatus;

    snConnectToPeerTask m_ConnectToPeerTask;
};

//PURPOSE
//  Drops gamers from the session.
class snDropGamersTask : public snTask
{
public:

    SN_TASK_DECL(snDropGamersTask, snet_tasks);

    snDropGamersTask();

    ~snDropGamersTask();

    //PURPOSE
    //  Configures the task to drop gamers from the session.
    //PARAMS
    //  ctx                 - The session.
    //  requestingPeerId    - Id of peer requesting the drop.
    //  gamerIds            - Gamers to drop.
    //  numGamers           - Number of gamers to drop.
    bool Configure(snSession* ctx,
                    const u64 requestingPeerId,
                    const rlGamerId* gamerIds,
                    const unsigned numGamers,
					const int removeReason);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:

    u64 m_RequestingPeerId;
    rlGamerId m_GamerIds[RL_MAX_GAMERS_PER_SESSION];
    rlGamerHandle m_GamerHandles[RL_MAX_GAMERS_PER_SESSION];
    unsigned m_NumGamers;
	int m_RemoveReason;
};

//PURPOSE
//  Destroys a session.
class snDestroyTask : public snTask
{
public:

    SN_TASK_DECL(snDestroyTask, snet_tasks);

    snDestroyTask();

    ~snDestroyTask();

    bool Configure(snSession* ctx);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:

    netStatus m_MyStatus;
};

//  Enables/disables presence settings
class snModifyPresenceFlagsTask : public snTask
{
public:

    SN_TASK_DECL(snModifyPresenceFlagsTask, snet_tasks);

    snModifyPresenceFlagsTask();

	//PURPOSE
	//  Configures the task
	//PARAMS
	//  ctx             - The session.
	//  presenceFlags   - Presence Flags
	bool Configure(snSession* ctx,
                    unsigned presenceFlags);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:

    netStatus m_MyStatus;
    unsigned m_PresenceFlags;
	//Set to true if we need to broadcast the change to peers.
    bool m_BroadcastCmd         : 1;
};

//PURPOSE
//  Establishes the local instance of a session hosted by a remote peer.
class snEstablishSessionTask : public snTask
{
public:

    SN_TASK_DECL(snEstablishSessionTask, snet_tasks);

    snEstablishSessionTask();

    ~snEstablishSessionTask();

    //PURPOSE
    //  Configures the task to establish the session.
    //PARAMS
    //  ctx             - The session.
    //  localGamerIndex - Index of local owner of the session instance.
    //  sessionInfo     - Session info identifying the session.
    //  netMode         - Network mode.
    //  createFlags     - Create flags.
    //  slotType        - Slot type into which the local owner will join
    bool Configure(snSession* ctx,
                    const int localGamerIndex,
                    const rlSessionInfo& sessionInfo,
                    const rlNetworkMode netMode,
                    const unsigned createFlags,
                    const rlSlotType slotType);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

    const rlPeerInfo& GetHostPeerInfo() const;

    EndpointId GetHostEndpointId() const;

private:

    enum
    {
        STATE_INVALID = -1,
        STATE_QUERY_DETAILS,
        STATE_QUERYING_DETAILS,
        STATE_ACTIVATE_SESSION,
        STATE_ACTIVATING_SESSION,
        STATE_ESTABLISH_SESSION,
        STATE_ESTABLISHING_SESSION,
        STATE_OPEN_TUNNEL,
        STATE_OPENING_TUNNEL,
        STATE_DESTROY,
        STATE_DESTROYING
    };

    int m_State;
    netStatus m_MyStatus;
    int m_LocalGamerIndex;
    rlSessionInfo m_SessionInfo;
    rlSessionDetail m_SessionDetail;
    unsigned m_NumResults;
    rlNetworkMode m_NetMode;
	unsigned m_CreateFlags; 
	rlSlotType m_SlotType;
	netTunnelRequest m_TunnelRqst;
};

//PURPOSE
//  Creates a session as the host.
class snHostSessionTask : public snTask
{
public:

    SN_TASK_DECL(snHostSessionTask, snet_tasks);

    snHostSessionTask();

    ~snHostSessionTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    //  localGamerIndex - Index of local gamer that's hosting the session.
    //  netMode         - Network mode (offline, online, syslink).
    //  maxPubSlots     - Maximum number public slots.
    //  maxPrivSlots    - Maximum number of private slots.
    //  attrs           - Session attributes.
    //  createFlags     - Session creation flags.
    //  ownerData       - Gamer data to be replicated to other peers in
    //                    the session.
    //  sizeofOwnerData - Size of gamer data to be replicated to other peers
    //                    in the session.
    bool Configure(snSession* ctx,
                    const int localGamerIndex,
                    const rlNetworkMode netMode,
                    const int maxPubSlots,
                    const int maxPrivSlots,
                    const rlMatchingAttributes& attrs,
                    const unsigned createFlags,
                    const void* ownerData,
                    const unsigned sizeofOwnerData);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:

    enum
    {
        STATE_INVALID = -1,
        STATE_CREATE,
        STATE_CREATING,
        STATE_JOIN,
        STATE_JOINING,
        STATE_DESTROY,
        STATE_DESTROYING,
    };

    int m_State;
    netStatus m_MyStatus;
    int m_LocalGamerIndex;
    rlSlotType m_SlotType;
    rlNetworkMode m_NetMode;
    int m_MaxPubSlots;
    int m_MaxPrivSlots;
    rlMatchingAttributes m_Attrs;
    unsigned m_CreateFlags;
    unsigned m_SizeofOwnerData;
    u8 m_OwnerData[SNET_MAX_SIZEOF_GAMER_DATA];
};

//PURPOSE
//  Joins a session as a guest.
class snJoinSessionTask : public snTask
{
public:

    SN_TASK_DECL(snJoinSessionTask, snet_tasks);

    snJoinSessionTask();
    ~snJoinSessionTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    //  localGamerIndex - Local gamer that is joining the session.
    //  sessionInfo     - Session info identifying the session.
    //  netMode         - Network mode.
    //  slotType        - SLOT_PUBLIC or SLOT_PRIVATE.
    //                    The type of slot to which the gamer is eventually
    //                    joined is up to the host.
    //  createFlags     - From rlSession::CreateFlags.
    //  joinerData      - Application data sent in the join request.
    //  sizeofJoinerData    - Size of application data.
    //  groupMembers    - Array of group members
    //  groupSize       - Number of members in the group
    bool Configure(snSession* ctx,
                    const int localGamerIndex,
                    const rlSessionInfo& sessionInfo,
                    const rlNetworkMode netMode,
                    const rlSlotType slotType,
                    const unsigned createFlags,
                    const void* joinerData,
                    const unsigned sizeofJoinerData,
                    const rlGamerHandle* groupMembers,
                    const unsigned groupSize);

    virtual void Start();
    virtual void Finish(const FinishType finishType, const int resultCode = 0);
    virtual void Update(const unsigned timeStep);

private:

    enum
    {
        STATE_INVALID = -1,
        STATE_ESTABLISH_SESSION,
        STATE_ESTABLISHING_SESSION,
        STATE_REQUEST_JOIN,
        STATE_AWAITING_RESPONSE,
        STATE_JOIN_LOCALLY,
        STATE_JOINING_LOCALLY,
        STATE_DESTROY,
        STATE_DESTROYING
    };

    void OnNetEvent(netConnectionManager* cxnMgr,
                    const netEvent* evt);

    int m_State;
    int m_LocalGamerIndex;
    rlGamerInfo m_JoinerInfo;
    rlSessionInfo m_SessionInfo;
    rlNetworkMode m_NetMode;
    unsigned m_CreateFlags;
    rlSlotType m_SlotType;
    u8 m_JoinerData[SNET_MAX_SIZEOF_GAMER_DATA];
    unsigned m_SizeofJoinerData;

    int m_HostCxnId;
    EndpointId m_HostEndpointId;
    snJoinResponseCode m_ResponseCode;
    u8 m_ResponseData[SNET_MAX_SIZEOF_JOIN_RESPONSE_DATA];
    unsigned m_SizeofResponseData;
    netConnectionManager::Delegate m_NetEvtDlgt;

    rlGamerHandle m_GroupMembers[RL_MAX_GAMERS_PER_SESSION];
    unsigned m_GroupSize;

    netStatus m_MyStatus;

    snEstablishSessionTask m_EstablishSessionTask;
};

//PURPOSE
//  Joins a network bot to a remotely hosted network session as a guest.
class snJoinRemoteSessionBotTask : public snTask
{
public:

    SN_TASK_DECL(snJoinRemoteSessionBotTask, snet_tasks);

    snJoinRemoteSessionBotTask();
    ~snJoinRemoteSessionBotTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    //  joiner          - Gamer info for the network bot that is joining the session.
    //  sessionInfo     - Session info identifying the session.
    //  netMode         - Network mode.
    //  slotType        - SLOT_PUBLIC or SLOT_PRIVATE.
    //                    The type of slot to which the gamer is eventually
    //                    joined is up to the host.
    //  createFlags     - From rlSession::CreateFlags.
    //  joinerData       - Application data sent in the join request.
    //  sizeofJoinerData    - Size of application data.
    bool Configure(snSession* ctx,
                    const rlGamerInfo& joiner,
                    const rlSessionInfo& sessionInfo,
                    const rlNetworkMode netMode,
                    const rlSlotType slotType,
                    const void* joinerData,
                    const unsigned sizeofJoinerData);

    virtual void Start();
    virtual void Finish(const FinishType finishType, const int resultCode = 0);
    virtual void Update(const unsigned timeStep);

private:

    enum
    {
        STATE_INVALID = -1,
        STATE_REQUEST_JOIN,
        STATE_AWAITING_RESPONSE,
        STATE_JOIN_LOCALLY,
        STATE_JOINING_LOCALLY,
        STATE_DESTROY,
        STATE_DESTROYING
    };

    void OnNetEvent(netConnectionManager* cxnMgr,
                    const netEvent* evt);

    rlGamerInfo m_Joiner;
    rlSessionInfo m_SessionInfo;
    rlSlotType m_SlotType;
    u8 m_JoinerData[SNET_MAX_SIZEOF_GAMER_DATA];
    unsigned m_SizeofJoinerData;

    int m_State;
    netStatus m_MyStatus;
    int m_HostCxnId;
    EndpointId m_HostEndpointId;
    snJoinResponseCode m_ResponseCode;
    u8 m_ResponseData[SNET_MAX_SIZEOF_JOIN_RESPONSE_DATA];
    unsigned m_SizeofResponseData;
    netConnectionManager::Delegate m_NetEvtDlgt;
};

//PURPOSE
//  Joins a network bot to a locally hosted network session as a guest.
class snJoinLocalSessionBotTask : public snTask
{
public:

    SN_TASK_DECL(snJoinLocalSessionBotTask, snet_tasks);

    snJoinLocalSessionBotTask();
    ~snJoinLocalSessionBotTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    //  joiner          - Local gamer that is joining the session.
    //  sessionInfo     - Session info identifying the session.
    //  slotType        - SLOT_PUBLIC or SLOT_PRIVATE.
    //                    The type of slot to which the gamer is eventually
    //                    joined is up to the host.
    //  joinerData      - Application data sent to the host in the join request
    //                    (in the snMsgJoinRequest message).
    //  sizeofJoinerData    - Size of application data.
    bool Configure(snSession* ctx,
                    const rlGamerInfo& joiner,
                    const rlSessionInfo& sessionInfo,
                    const rlSlotType slotType,
                    const void* joinerData,
                    const unsigned sizeofJoinerData);

    virtual void Start();
    virtual void Finish(const FinishType finishType, const int resultCode = 0);
    virtual void Update(const unsigned timeStep);

private:

    enum
    {
        STATE_INVALID = -1,
        STATE_JOIN_LOCALLY,
        STATE_JOINING_LOCALLY,
        STATE_DESTROY,
        STATE_DESTROYING
    };

    rlGamerInfo m_Joiner;
    rlSessionInfo m_SessionInfo;
    rlSlotType m_SlotType;
    u8 m_JoinerData[SNET_MAX_SIZEOF_GAMER_DATA];
    unsigned m_SizeofJoinerData;

    int m_State;
    netStatus m_MyStatus;
};

//PURPOSE
//  Migrates the session to a new host.
class snMigrateSessionTask : public snTask
{
public:

    SN_TASK_DECL(snMigrateSessionTask, snet_tasks);

    enum
    {
        //Maximum number of new host candidates.
        MAX_CANDIDATES = RL_MAX_GAMERS_PER_SESSION,

		//Grace period of 1.5 times the given timeout to allow for all parties to 
		//discover the disconnection
		DEFAULT_TIMEOUT_MS = (netConnectionPolicies::DEFAULT_TIMEOUT_INTERVAL * 3) / 2,
		DEFAULT_REQUEST_INTERVAL = 2000,
	};

    snMigrateSessionTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    bool Configure(snSession* ctx);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

	void GetCandidates(); 

	virtual bool RemoveCandidate(const u64 peerId); 

	static void SetTimeoutPolicy(const unsigned timeoutMs);
	static void SetMigrateRequestInterval(const unsigned requestInterval);
	static void RefreshNumMigrateRequestAttempts(); 

private:

    enum
    {
        STATE_INVALID = -1,
        STATE_MIGRATE,
        STATE_MIGRATING,
        STATE_AWAITING_MIGRATE_RESPONSES,
        STATE_WAITING_FOR_NEW_HOST,
    };

    bool Commence();

    bool Migrate(const rlPeerInfo& newHost,
                const rlSessionInfo* sessionInfo);

    void SendMigrateCommands();

    bool DropDeadbeats();

    //Handles migrate command from new host.
    void OnRequest(netTransactor* transactor,
                    netRequestHandler* handler,
                    const netRequest* rqst);

    //Handles responses to migrate command we send.
    void OnResponse(netTransactor* transactor,
                    netResponseHandler* handler,
                    const netResponse* resp);

    struct RespHandler : public netResponseHandler
    {
        RespHandler()
            : m_PeerId(0)
            , m_Attempts(0)
            , m_Succeeded(false)
        {
        }

        void Reset(const u64 peerId)
        {
            m_PeerId = peerId;
            m_Attempts = 0;
            m_Succeeded = false;
        }

        u64 m_PeerId;
        unsigned m_Attempts;
        bool m_Succeeded    : 1;
    };

    int m_State;
    netStatus m_MyStatus;
    //Add one to permit addition of ourselves to the list of candidates.
    rlPeerInfo m_Candidates[MAX_CANDIDATES + 1];
	bool m_CandidateRemoved[MAX_CANDIDATES + 1];
    int m_NumCandidates;
    int m_CurCandidate;
    rlSessionInfo m_NewSessionInfo;
    RespHandler m_RespHandlers[RL_MAX_GAMERS_PER_SESSION];
    int m_NumRequestsSent;
    int m_NumPendingResponses;
    int m_NumSucceeded;
    netRequestHandler m_RqstHandler;
    int m_WaitTimeout;

	static unsigned sm_TimeoutMs;
	static unsigned sm_MigrateRequestInterval;
	static unsigned sm_MigrateRequestNumAttempts; 
};

//PURPOSE
//  Enables matchmaking advertising
class snEnableMatchmakingAdvertiseTask : public snTask
{
public:

	SN_TASK_DECL(snEnableMatchmakingAdvertiseTask, snet_tasks);

	snEnableMatchmakingAdvertiseTask();

	//PURPOSE
	//  Configures the task
	//PARAMS
	//  ctx             - The session.
	//  newAttrs        - New attributes
	bool Configure(snSession* ctx);

	virtual void Start();

	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	virtual void Update(const unsigned timeStep);

private:

	netStatus m_MyStatus;
};

//PURPOSE
//  Changes session attributes.
class snChangeAttributesTask : public snTask
{
public:

    SN_TASK_DECL(snChangeAttributesTask, snet_tasks);

    snChangeAttributesTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    //  newAttrs        - New attributes
    bool Configure(snSession* ctx,
                    const rlMatchingAttributes& newAttrs);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:

    rlMatchingAttributes m_NewAttrs;
    netStatus m_MyStatus;
};

//PURPOSE
//  Sends game invitations to a collection of remote gamers.
class snSendInvitesTask : public snTask
{
public:

    SN_TASK_DECL(snSendInvitesTask, snet_tasks);

    snSendInvitesTask();

    ~snSendInvitesTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    //  gamerHandles    - Gamers to invite.
    //  numGamers       - Number of gamers to invite.
    //  subject         - Subject line of invite message.  PS3 only.
    //  salutation      - Text attached to invitation.
    bool Configure(snSession* ctx,
                    const rlGamerHandle* gamerHandles,
                    const unsigned numGamers,
                    const char* subject,
                    const char* salutation);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:

    netStatus m_MyStatus;
    rlGamerHandle* m_GamerHandles;
    unsigned m_NumGamers;
    char m_Subject[RL_MAX_INVITE_SUBJECT_CHARS];
    char m_Salutation[RL_MAX_INVITE_SALUTATION_CHARS];
};

//PURPOSE
//  Sends game invitations to all members of the platform party.
class snSendPartyInvitesTask : public snTask
{
public:

    SN_TASK_DECL(snSendPartyInvitesTask, snet_tasks);

    snSendPartyInvitesTask();
    ~snSendPartyInvitesTask();

    //PURPOSE
    //  Configures the task
    //PARAMS
    //  ctx             - The session.
    //  localGamerIndex - Index of local gamer sending invites.
    bool Configure(snSession* ctx,
        const int localGamerIndex);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:
    netStatus m_MyStatus;
    int m_LocalGamerIndex;
};

class snEnableXblPresenceTask : public snTask
{
    //Dummy class - not needed for non-Live platforms
};

#if RSG_DURANGO

//PURPOSE
//  Drops gamers from the session.
class snNotifyAddGamerTask : public snTask
{
public:

	SN_TASK_DECL(snNotifyAddGamerTask, snet_tasks);

	enum 
	{
		DEFAULT_TIMEOUT_MS = 2500,
	};

	snNotifyAddGamerTask();
	~snNotifyAddGamerTask();

	//PURPOSE
	//  Configures the task to look up a display name and send an add gamer event
	//PARAMS
	//  gamerInfo	-	the gamerInfo to add
	//	slotType	-	the slot type to add to (public or private)
	//	data		-	the gamer data
	//	sizeOfData	-	size of the gamer Data
	bool Configure(snSession* ctx, const rlGamerInfo& gamerInfo, const rlSlotType slotType, const void* data, const unsigned sizeofData);

	//PURPOSE
	//	Finishes the task and fires the add gamer event
	virtual void Finish(const FinishType finishType, const int resultCode = 0);

	//PURPOSE
	// Starts the display name task
	bool GetDisplayName();

	//PURPOSE
	// Get gamer handle that we're working on
	const rlGamerHandle& GetGamerHandle();

	//PURPOSE
	//	Update the task
	virtual void Update(const unsigned timeStep);
	
	//PURPOSE
	//	Cancel the task
	virtual bool IsCancelable() const {return true;}
	virtual void DoCancel();

	//PURPOSE
	//	Disable adding the player when the task completes
	virtual void DoNotAddPlayer();

	//PURPOSE
	//	Time at which the task will time out. If this is reached, we will
	//	cancel the display name request and add the gamer
	static void SetTimeoutPolicy(const unsigned timeoutMs);

private:

	rlGamerInfo m_GamerInfo;
	rlSlotType m_SlotType;
	u8 m_Data[SNET_MAX_SIZEOF_GAMER_DATA];
	unsigned m_SizeOfData;
	u32 m_SuccessTime;

	enum State
	{
		STATE_GET_DISPLAYNAME,
		STATE_GETTING_DISPLAYNAME
	};

	State m_State;

	u64 m_Xuid;
	rlDisplayName m_DisplayName;
	netStatus m_DisplayNameStatus;
	bool m_bAddPlayer;

	unsigned m_TimeStarted; 
	static unsigned sm_TimeoutMs;
};

#endif // RSG_DURANGO

}   //namespace rage

#endif  //SNET_TASKS_H
