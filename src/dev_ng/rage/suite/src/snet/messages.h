// 
// snet/messages.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNET_MESSAGES_H
#define SNET_MESSAGES_H

#include "data/bitbuffer.h"
#include "net/message.h"
#include "rline/rlpeerinfo.h"
#include "rline/rlsession.h"
#include "rline/rlsessionfinder.h"
#include "snet.h"
#include "string/string.h"
#include "system/nelem.h"
#include "system/memory.h"

namespace rage
{

//PURPOSE
// Identifies a reserved player slot in a session by slot type.
// Sent when Players in parties are added to the session.
struct ReservedSlot
{
	ReservedSlot()
	{
		Clear();
	}

	void Reset(const rlGamerHandle& gamerHandle,
			   const rlSlotType slotType,
			   const unsigned expiry,
			   const unsigned userParam)
	{
		m_GamerHandle = gamerHandle;
		m_SlotType = slotType;
		m_ExpiryMs = expiry;
		m_UserParam = userParam;
	}

	void Clear()
	{
		m_GamerHandle.Clear();
		m_ExpiryMs = 0;
		m_UserParam = 0;
	}

	rlGamerHandle m_GamerHandle;
	rlSlotType m_SlotType;
	unsigned m_ExpiryMs;
	unsigned m_UserParam;
};

//PURPOSE
//  Sent to a session host to retrieve session config
//NOTES
//  H->P:       NO
//  P->Host:    YES
//  P->H:       NO
struct snMsgConfigRequest
{
    NET_MESSAGE_DECL(snMsgConfigRequest, SN_MSG_CONFIG_REQUEST);

    void Reset(const rlSessionInfo& sessionInfo)
    {
        m_SessionInfo = sessionInfo;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerUser(msg.m_SessionInfo);
    }

    rlSessionInfo m_SessionInfo;
};

//PURPOSE
//  Response to a session config request.
//NOTES
//  H->P:       YES
//  P->Host:    NO
//  P->H:       NO
struct snMsgConfigResponse
{
    NET_MESSAGE_DECL(snMsgConfigResponse, SN_MSG_CONFIG_RESPONSE);

    void Reset(const rlPeerInfo& hostPeerInfo,
                const rlSessionInfo& sessionInfo,
                const rlSessionConfig& config)
    {
        m_HostPeerInfo = hostPeerInfo;
        m_SessionInfo = sessionInfo;
        m_Config = config;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerUser(msg.m_HostPeerInfo)
                && snVerify(msg.m_HostPeerInfo.IsValid())
                && bb.SerUser(msg.m_SessionInfo)
                && snVerify(msg.m_SessionInfo.IsValid())
                && bb.SerUser(msg.m_Config);
    }

    rlPeerInfo m_HostPeerInfo;
    //The session info that the caller should attempt to join.
    //In the case of a party session on xbox it will be different from the
    //session that's being queried because we query the party's "presence"
    //session, but we join the party session.
    rlSessionInfo m_SessionInfo;
    rlSessionConfig m_Config;
};

//PURPOSE
//  Base class for session messages.  Contains the session id in order
//  to distinguish messages belonging to different sessions.
struct snMsgBase
{
    NET_MESSAGE_DECL(snMsgBase, SN_MSG_BASE);

    snMsgBase()
        : m_SessionId(rlSession::INVALID_SESSION_ID)
    {
    }

    void Reset(const u64 sessionId)
    {
        m_SessionId = sessionId;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerUns(msg.m_SessionId, 64)
                && snVerify(msg.m_SessionId != rlSession::INVALID_SESSION_ID);
    }

    u64 m_SessionId;
};

//PURPOSE
//  Base class for AddGamer messages
struct snMsgAddGamerToSessionBase : public snMsgBase
{
    snMsgAddGamerToSessionBase()
        : m_SlotType(RL_SLOT_INVALID)
        , m_SizeofData(0)
    {
    }

    void Reset(const u64 sessionId,
                const rlGamerInfo& gamerInfo,
                const netAddress& relayAddr,
                const rlSlotType slotType,
                const void* data,
                const unsigned sizeofData)
    {
        snAssert(sizeofData <= sizeof(m_Data));
        snAssert(RL_SLOT_PUBLIC == slotType
                || RL_SLOT_PRIVATE == slotType);

        this->snMsgBase::Reset(sessionId);
        m_GamerInfo = gamerInfo;
        m_RelayAddr = relayAddr;

#if USE_NET_ASSERTS
		if(relayAddr.IsValid())
		{
			// we should have the real relay address
			snAssert(!relayAddr.GetRelayToken().IsValid());
			snAssert(relayAddr.GetTargetAddress().IsValid());
		}
#endif
		
		m_SizeofData = sizeofData;
        if(sizeofData)
        {
            snAssert(data);
            sysMemCpy(m_Data, data, sizeofData);
        }
        m_SlotType = slotType;
    }

    NET_MESSAGE_SER(bb, msg)
    {
		const bool success = snMsgBase::SerMsgPayload(bb, msg)
                && bb.SerUser(msg.m_GamerInfo)
                && bb.SerUser(msg.m_RelayAddr)
                && bb.SerUns(msg.m_SlotType, datBitsNeeded<RL_SLOT_NUM_TYPES>::COUNT)
                && bb.SerUns(msg.m_SizeofData, datBitsNeeded<sizeof(msg.m_Data)>::COUNT)
                && snVerifyf(msg.m_SizeofData <= sizeof(msg.m_Data), "m_SizeofData[%u] overflow", msg.m_SizeofData)
                && (msg.m_SizeofData ? bb.SerBytes(msg.m_Data, msg.m_SizeofData) : true);

#if USE_NET_ASSERTS
		if(msg.m_RelayAddr.IsValid())
		{
			// we should have the real relay address
			snAssert(!msg.m_RelayAddr.GetRelayToken().IsValid());
			snAssert(msg.m_RelayAddr.GetTargetAddress().IsValid());
		}
#endif

		return success;
    }

    rlGamerInfo m_GamerInfo;
    netAddress m_RelayAddr;
    rlSlotType m_SlotType;
    unsigned m_SizeofData;
    u8 m_Data[SNET_MAX_SIZEOF_GAMER_DATA];
};

//PURPOSE
//  Sent to a host to request joining the session.
//  The game session host will reserve slots for the group
//  for a short period of time.
//  If the join request is for a group, the caller will be joined
//  to the session, but the rest of the group will only have slots
//  reserved for them.  They must still request to join.
//  The group array must contain at least one entry; that of the
//  caller.
//NOTES
//  H->P:       NO
//  P->Host:    YES
//  P->H:       NO
struct snMsgJoinRequest : public snMsgAddGamerToSessionBase
{
    NET_MESSAGE_DECL(snMsgJoinRequest, SN_MSG_JOIN_REQUEST);

    snMsgJoinRequest()
        : m_GroupSize(0)
    {
    }

    void Reset(const u64 sessionId,
                const rlGamerInfo& gamerInfo,
                const netAddress& relayAddr,
                const rlSlotType slotType,
                const void* data,
                const unsigned sizeofData,
                const rlGamerHandle* groupMembers,
                const unsigned groupSize)
    {
        snMsgAddGamerToSessionBase::Reset(sessionId, gamerInfo, relayAddr, slotType, data, sizeofData);
        if(rlVerify(groupSize > 0)
            && rlVerify(groupSize <= COUNTOF(m_GroupMembers)))
        {
            m_GroupSize = groupSize;
            for(int i = 0; i < (int)groupSize; ++i)
            {
                m_GroupMembers[i] = groupMembers[i];
            }
        }
        else
        {
            m_GroupSize = 1;
            m_GroupMembers[0] = gamerInfo.GetGamerHandle();
        }
    }

#if RSG_ORBIS
	// on PS4, the max export size of the m_GroupMembers array is larger than the 
	// max size of a network frame. Instead of exporting the entire rlGamerHandle, we
	// only export the string and reconstruct the gamerhandle on import.
	NET_MESSAGE_EXPORT(bb, msg)
	{
		if(snMsgAddGamerToSessionBase::SerMsgPayload(bb, msg)
			&& bb.SerUns(msg.m_GroupSize, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT))
		{
			for(int i = 0; i < (int)msg.m_GroupSize; ++i)
			{
				char buf[RL_MAX_GAMER_HANDLE_CHARS] = {0};
				if(!snVerify((msg.m_GroupMembers[i].ToString(buf) != NULL) && (buf[0] != '\0')))
				{
					return false;
				}

				if(!snVerify(bb.SerStr(buf, sizeof(buf))))
				{
					return false;
				}
			}

			return true;
		}

		return false;
	}

	NET_MESSAGE_IMPORT(bb, msg)
	{
		if(snMsgAddGamerToSessionBase::SerMsgPayload(bb, msg)
			&& bb.SerUns(msg.m_GroupSize, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT))
		{
			for(int i = 0; i < (int)msg.m_GroupSize; ++i)
			{
				char buf[RL_MAX_GAMER_HANDLE_CHARS] = {0};

				if(!snVerify(bb.SerStr(buf, sizeof(buf))))
				{
					return false;
				}

				if(!snVerify(buf[0] != '\0'))
				{
					return false;
				}

				if(!snVerify(msg.m_GroupMembers[i].FromString(buf)))
				{
					return false;
				}
			}

			return true;
		}

		return false;
	}
#else
    NET_MESSAGE_SER(bb, msg)
    {
        if(snMsgAddGamerToSessionBase::SerMsgPayload(bb, msg)
            && bb.SerUns(msg.m_GroupSize, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT))
        {
            if (!snVerifyf(msg.m_GroupSize <= RL_MAX_GAMERS_PER_SESSION, "m_GroupSize[%u] overflow", msg.m_GroupSize))
            {
                return false;
            }

            for(int i = 0; i < (int)msg.m_GroupSize; ++i)
            {
                if(!bb.SerUser(msg.m_GroupMembers[i]))
                {
                    return false;
                }
            }

            return true;
        }

        return false;
    }
#endif

    unsigned m_GroupSize;   //Includes the group leader
    rlGamerHandle m_GroupMembers[RL_MAX_GAMERS_PER_SESSION];
};

//PURPOSE
//  Response to a request to add a gamer to a session.
//NOTES
//  H->P:       YES
//  P->Host:    NO
//  P->H:       NO
struct snMsgJoinResponse : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgJoinResponse, SN_MSG_JOIN_RESPONSE);

    snMsgJoinResponse()
        : m_SlotType(RL_SLOT_INVALID)
        , m_ResponseCode(SNET_JOIN_DENIED_NOT_JOINABLE)
        , m_SizeofGamerData(0)
        , m_SizeofResponseData(0)
    {
    }

    void Reset(const u64 sessionId,
                const snJoinResponseCode responseCode,
                const rlSlotType slotType,
                const void* gamerData,
                const unsigned sizeofGamerData,
                const void* responseData,
                const unsigned sizeofResponseData)
    {
        snAssert(RL_SLOT_PUBLIC == slotType
                || RL_SLOT_PRIVATE == slotType);
        snAssert(sizeofGamerData <= sizeof(m_GamerData));
        snAssert(sizeofResponseData <= sizeof(m_ResponseData));

        this->snMsgBase::Reset(sessionId);

        m_ResponseCode = responseCode;
        m_SlotType = slotType;

        if(SNET_JOIN_ACCEPTED == responseCode)
        {
            m_SizeofGamerData = sizeofGamerData;
            if(sizeofGamerData)
            {
                snAssert(gamerData);
                sysMemCpy(m_GamerData, gamerData, sizeofGamerData);
            }
        }
        else
        {
            m_SizeofGamerData = 0;
        }

        m_SizeofResponseData = sizeofResponseData;
        if(sizeofResponseData)
        {
            snAssert(responseData);
            sysMemCpy(m_ResponseData, responseData, sizeofResponseData);
        }
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return snMsgBase::SerMsgPayload(bb, msg)
                && bb.SerUns(msg.m_SlotType, datBitsNeeded<RL_SLOT_NUM_TYPES>::COUNT)
                && bb.SerUns(msg.m_ResponseCode, datBitsNeeded<SNET_JOIN_NUM_CODES>::COUNT)
                && bb.SerUns(msg.m_SizeofGamerData, datBitsNeeded<sizeof(msg.m_GamerData)>::COUNT)
                && snVerifyf(msg.m_SizeofGamerData <= static_cast<unsigned>(sizeof(msg.m_GamerData)), "m_SizeofGamerData[%u] overflow", msg.m_SizeofGamerData)
                && bb.SerUns(msg.m_SizeofResponseData, datBitsNeeded<sizeof(msg.m_ResponseData)>::COUNT)
                && snVerifyf(msg.m_SizeofResponseData <= static_cast<unsigned>(sizeof(msg.m_ResponseData)), "m_SizeofResponseData[%u] overflow", msg.m_SizeofResponseData)
                && (msg.m_SizeofGamerData ? bb.SerBytes(msg.m_GamerData, msg.m_SizeofGamerData) : true)
                && (msg.m_SizeofResponseData ? bb.SerBytes(msg.m_ResponseData, msg.m_SizeofResponseData) : true);
    }

    rlSlotType m_SlotType;
    unsigned m_ResponseCode;
    unsigned m_SizeofGamerData;
    unsigned m_SizeofResponseData;
    u8 m_GamerData[SNET_MAX_SIZEOF_GAMER_DATA];
    u8 m_ResponseData[SNET_MAX_SIZEOF_JOIN_RESPONSE_DATA];
};

//PURPOSE
//  Sent from host to guest to notify it add a gamer to its local
//  instance of a session.
//NOTES
//  H->P:       YES
//  P->Host:    NO
//  P->H:       NO
struct snMsgAddGamerToSessionCmd : public snMsgAddGamerToSessionBase
{
    NET_MESSAGE_DECL(snMsgAddGamerToSessionCmd, SN_MSG_ADD_GAMER_TO_SESSION_CMD);

    void Reset(const u64 sessionId,
                const rlGamerInfo& gamerInfo,
                const netAddress& relayAddr,
                const rlSlotType slotType,
                const void* data,
                const unsigned sizeofData,
				const rlGamerHandle* reservedMembers,
				const unsigned reservedSize)
    {
        this->snMsgAddGamerToSessionBase::Reset(sessionId, gamerInfo, relayAddr, slotType, data, sizeofData);
		m_ReservedSize = reservedSize;
		for(int i = 0; i < (int)reservedSize; ++i)
		{
			m_ReservedMembers[i] = reservedMembers[i];
		}
    }

	void Reset(const u64 sessionId,
		const rlGamerInfo& gamerInfo,
		const netAddress& relayAddr,
		const rlSlotType slotType,
		const void* data,
		const unsigned sizeofData,
		const ReservedSlot* reservedSlots,
		const unsigned reservedSize)
	{
		this->snMsgAddGamerToSessionBase::Reset(sessionId, gamerInfo, relayAddr, slotType, data, sizeofData);
		m_ReservedSize = reservedSize;
		for(int i = 0; i < (int)reservedSize; ++i)
		{
			m_ReservedMembers[i] = reservedSlots[i].m_GamerHandle;
		}
	}

#if RSG_ORBIS
	// on PS4, the max export size of the m_ReservedMembers array is larger than the 
	// max size of a network frame. Instead of exporting the entire rlGamerHandle, we
	// only export the string and reconstruct the gamerhandle on import.
	NET_MESSAGE_EXPORT(bb, msg)
	{
		if(snMsgAddGamerToSessionBase::SerMsgPayload(bb, msg)
			&& bb.SerUns(msg.m_ReservedSize, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT))
		{
			for(int i = 0; i < (int)msg.m_ReservedSize; ++i)
			{
				char buf[RL_MAX_GAMER_HANDLE_CHARS] = {0};
				if(!snVerify((msg.m_ReservedMembers[i].ToString(buf) != NULL) && (buf[0] != '\0')))
				{
					return false;
				}

				if(!snVerify(bb.SerStr(buf, sizeof(buf))))
				{
					return false;
				}
			}

			return true;
		}

		return false;
	}

	NET_MESSAGE_IMPORT(bb, msg)
	{
		if(snMsgAddGamerToSessionBase::SerMsgPayload(bb, msg)
			&& bb.SerUns(msg.m_ReservedSize, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT))
		{
			for(int i = 0; i < (int)msg.m_ReservedSize; ++i)
			{
				char buf[RL_MAX_GAMER_HANDLE_CHARS] = {0};

				if(!snVerify(bb.SerStr(buf, sizeof(buf))))
				{
					return false;
				}

				if(!snVerify(buf[0] != '\0'))
				{
					return false;
				}

				if(!snVerify(msg.m_ReservedMembers[i].FromString(buf)))
				{
					return false;
				}
			}

			return true;
		}

		return false;
	}
#else
    NET_MESSAGE_SER(bb, msg)
    {
		if(snMsgAddGamerToSessionBase::SerMsgPayload(bb, msg)
			&& bb.SerUns(msg.m_ReservedSize, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT))
		{
			if (!snVerifyf(msg.m_ReservedSize <= RL_MAX_GAMERS_PER_SESSION, "m_GroupSize[%u] overflow", msg.m_ReservedSize))
			{
				return false;
			}

			for(int i = 0; i < (int)msg.m_ReservedSize; ++i)
			{
				if(!bb.SerUser(msg.m_ReservedMembers[i]))
				{
					return false;
				}
			}

			return true;
		}

		return false;
    }
#endif

	unsigned m_ReservedSize;   //Includes the joiner
	rlGamerHandle m_ReservedMembers[RL_MAX_GAMERS_PER_SESSION];
};

//PURPOSE
//  Sent from host to guest to notify that the host left during the join process.
//NOTES
//  H->P:       YES
//  P->Host:    NO
//  P->H:       NO
struct snMsgHostLeftWhilstJoiningCmd : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgHostLeftWhilstJoiningCmd, SN_MSG_HOST_LEFT_WHILST_JOINING_CMD);

    void Reset(const u64 sessionId)
    {
        this->snMsgBase::Reset(sessionId);
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return snMsgBase::SerMsgPayload(bb, msg);
    }
};

//PURPOSE
//  Sent to notify a peer to remove a gamer from its local instance of a session.
//NOTES
//  H->P:       YES
//  P->Host:    YES
//  P->H:       NO
struct snMsgRemoveGamersFromSessionCmd : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgRemoveGamersFromSessionCmd, SN_MSG_REMOVE_GAMERS_FROM_SESSION_CMD);

    void Reset(const u64 sessionId,
               const rlGamerId* gamerIds,
               const unsigned numGamers,
			   const int removeReason)
    {
        snAssert(numGamers <= RL_MAX_GAMERS_PER_SESSION);

        this->snMsgBase::Reset(sessionId);
        
		for(unsigned i = 0; i < numGamers; ++i)
        {
            m_GamerIds[i] = gamerIds[i];
        }

		m_RemoveReason = removeReason;

        m_NumGamers = numGamers;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        bool success = false;
        if(snMsgBase::SerMsgPayload(bb, msg)
            && bb.SerUns(msg.m_NumGamers, datBitsNeeded<RL_MAX_GAMERS_PER_SESSION>::COUNT))
        {
            if (!snVerifyf(msg.m_NumGamers <= RL_MAX_GAMERS_PER_SESSION, "m_NumGamers[%u] overflow", msg.m_NumGamers))
            {
                return false;
            }

            success = true;
            for(unsigned i = 0; i < msg.m_NumGamers; ++i)
            {
                if(!bb.SerUser(msg.m_GamerIds[i]))
                {
                    success = false;
                    break;
                }
            }

			if(!bb.SerInt(msg.m_RemoveReason, 16))
			{
				success = false;
			}
        }

        return success;
    }

    rlGamerId m_GamerIds[RL_MAX_GAMERS_PER_SESSION];
	int m_RemoveReason;
    unsigned m_NumGamers;
};

//PURPOSE
//  Sent from a peer to remote peers to indicate that we should not be considered
//  for host migration
struct snMsgNotMigrating : public snMsgBase
{
	NET_MESSAGE_DECL(snMsgNotMigrating, SN_MSG_NOT_MIGRATING);

	void Reset(const u64 sessionId)
	{
		this->snMsgBase::Reset(sessionId);
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return snMsgBase::SerMsgPayload(bb, msg);
	}
};

//PURPOSE
//  Sent from a new host to remote peers to notify them of a successful
//  host migration.
//NOTES
//  H->P:       YES
//  P->Host:    NO
//  P->H:       NO
struct snMsgMigrateHostRequest : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgMigrateHostRequest, SN_MSG_MIGRATE_HOST_REQUEST);

    void Reset(const u64 sessionId,
                const rlSessionInfo& sessionInfo)
    {
        this->snMsgBase::Reset(sessionId);
        m_SessionInfo = sessionInfo;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return snMsgBase::SerMsgPayload(bb, msg)
                && bb.SerUser(msg.m_SessionInfo);
    }

    rlSessionInfo m_SessionInfo;
};

//PURPOSE
//  Sent from a new host to remote peers to notify them of a successful
//  host migration.
struct snMsgMigrateHostResponse : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgMigrateHostResponse, SN_MSG_MIGRATE_HOST_RESPONSE);

    snMsgMigrateHostResponse()
        : m_NumMembers(-1)
        , m_Accept(false)
    {
    }

    void Reset(const u64 sessionId,
                const bool accept)
    {
        this->snMsgBase::Reset(sessionId);
        m_Accept = accept;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        bool success = snMsgBase::SerMsgPayload(bb, msg)
                        && bb.SerBool(msg.m_Accept);

        if(success && msg.m_Accept)
        {
            success = bb.SerUns(msg.m_NumMembers,
                                datBitsNeeded<RL_MAX_LOCAL_GAMERS>::COUNT);

            if (!snVerifyf(msg.m_NumMembers <= RL_MAX_LOCAL_GAMERS, "m_NumMembers[%u] overflow", msg.m_NumMembers))
            {
                return false;
            }

            for(int i = 0; success && i < msg.m_NumMembers; ++i)
            {
                success = bb.SerUser(msg.m_MemberIds[i]);
            }
        }

        return success;
    }

    //If the peer accepts the new host this array will contain the peer's
    //view of which of his local gamers are currently members of the session.
    //The host will compare his view to that of the peer to determine if any
    //of the peer's local gamers have left the session.
    rlGamerId m_MemberIds[RL_MAX_LOCAL_GAMERS];

    int m_NumMembers;

    //True if the peer accepts the new host.
    bool m_Accept;
};

//PURPOSE
//  Sent from the host to a client to tell the client to start
//  the match.
struct snMsgStartMatchCmd : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgStartMatchCmd, SN_MSG_START_MATCH_CMD);

    snMsgStartMatchCmd()
    {
    }

    void Reset(const u64 sessionId)
    {
        this->snMsgBase::Reset(sessionId);
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return snMsgBase::SerMsgPayload(bb, msg);
    }
};

//PURPOSE
//  Sent from host when the session attributes change.
struct snMsgChangeSessionAttributesCmd : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgChangeSessionAttributesCmd, SN_MSG_CHANGE_SESSION_ATTRIBUTES_CMD);

    snMsgChangeSessionAttributesCmd()
    {
    }

    void Reset(const u64 sessionId,
                const rlMatchingAttributes& attrs)
    {
        this->snMsgBase::Reset(sessionId);
        m_Attrs = attrs;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return snMsgBase::SerMsgPayload(bb, msg)
                && bb.SerUser(msg.m_Attrs);
    }

    rlMatchingAttributes m_Attrs;
};

//PURPOSE
//  Sent from host when the invitable setting changes.
struct snMsgSetInvitableCmd : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgSetInvitableCmd, SN_MSG_SET_INVITABLE_CMD);

    snMsgSetInvitableCmd()
        : m_Invitable(false)
    {
    }

    void Reset(const u64 sessionId,
                const bool invitable)
    {
        this->snMsgBase::Reset(sessionId);
        m_Invitable = invitable;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return snMsgBase::SerMsgPayload(bb, msg)
                && bb.SerBool(msg.m_Invitable);
    }

    bool m_Invitable;
};

//PURPOSE
//  Sent from host to verify current session membership.
//  In response peers must locally remove gamers that are not in the session,
//  and add gamers that are in the session but not present locally.
//  Peers must request from the host a snMsgAddGamerToSessionCmd for gamers
//  they don't have locally.
struct snMsgSessionMemberIds : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgSessionMemberIds, SN_MSG_SESSION_MEMBER_IDS);

    snMsgSessionMemberIds()
        : m_Count(0)
    {
    }

    void Reset(const u64 sessionId,
                const rlGamerId* members,
                const unsigned memberCount)
    {
        snAssert(memberCount > 0);
        snAssert(memberCount <= COUNTOF(m_GamerIds));

        this->snMsgBase::Reset(sessionId);

        for(int i = 0; i < (int) memberCount && i < COUNTOF(m_GamerIds); ++i)
        {
            m_GamerIds[i] = members[i];
        }

        m_Count = memberCount;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        bool success = false;
        if(snMsgBase::SerMsgPayload(bb, msg)
            && bb.SerUns(msg.m_Count, datBitsNeeded<COUNTOF(msg.m_GamerIds)>::COUNT))
        {
            success = true;

            if (!snVerifyf(msg.m_Count <= RL_MAX_GAMERS_PER_SESSION, "m_Count[%u] overflow", msg.m_Count))
            {
                return false;
            }

            for(int i = 0; i < (int) msg.m_Count; ++i)
            {
                if(!bb.SerUser(msg.m_GamerIds[i]))
                {
                    success = false;
                    break;
                }
            }
        }

        return success;
    }

    unsigned m_Count;
    rlGamerId m_GamerIds[RL_MAX_GAMERS_PER_SESSION];
};

//PURPOSE
//  Sent to host to request that the host send us the add gamer command
//  for an individual gamer.
//  This is sent when a peer receives a snMsgSessionMemberIds message
//  and realizes it doesn't locally have all members of the session.
//NOTES
//  H->P:       NO
//  P->H:       YES
struct snMsgRequestGamerInfo : public snMsgBase
{
    NET_MESSAGE_DECL(snMsgRequestGamerInfo, SN_MSG_REQUEST_GAMER_INFO);

    void Reset(const u64 sessionId,
                const rlGamerId& gamerId)
    {
        this->snMsgBase::Reset(sessionId);

        m_GamerId = gamerId;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        return snMsgBase::SerMsgPayload(bb, msg)
                && bb.SerUser(msg.m_GamerId);
    }

    rlGamerId m_GamerId;
};

}   //namespace rage

#endif  //SNET_MESSAGES_H
