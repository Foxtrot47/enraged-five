// 
// snet/tasks.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tasks.h"

#include "diag/output.h"
#include "diag/seh.h"
#include "messages.h"
#include "rline/rlpresence.h"
#include "session.h"
#include "snet.h"

#include "system/nelem.h"
#include "system/param.h"

#if RSG_DURANGO
#include "system/timer.h"
#endif

//FIXME (KB) - handle failures to join in a private slot.  Should the app handle this?

//FIXME (KB) - perhaps handle match registration handled concurrently so
//             the host won't time out waiting if a peer has a bunch
//             of other tasks queued before the registration.  Gotta be
//             careful about this though, because we don't want to
//             process match registration, for example, at the same time
//             we're leaving the session.

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(snet, tasks)
#undef __rage_channel
#define __rage_channel snet_tasks

enum KeyExchangeTimeouts
{ 
    SNET_KEY_EXCHANGE_REQUEST_TIMEOUT_MS            = 1000,
    SNET_KEY_EXCHANGE_INITIATE_REQUEST_TIMEOUT_MS   = 1000,
    SNET_KEY_EXCHANGE_MAX_REQUEST_ATTEMPTS          = 10
};

enum HostMigrationTimeouts
{
    //Minimum host migration timeout.  Used only if connection timeouts
    //are too low or set to infinity.
    //This is the time we'll wait for the new host to notify us of a
    //successful host migration, or the time we'll wait for a peer to
    //acknowledge us as the host, before we assume that host/peer is dead.
	SNET_HOST_MIGRATION_MIN_TIMEOUT = netConnectionPolicies::DEFAULT_TIMEOUT_INTERVAL,
};

///////////////////////////////////////////////////////////////////////////////
// snTask
///////////////////////////////////////////////////////////////////////////////
bool snTask::ConfigureContext(rlTask<snSession>* pTask, snSession* pSession)
{
	rlTaskBase::ConfigureContext(pTask, pSession);
	RefreshLogContext();
	return true; 
}

void snTask::RefreshLogContext()
{
	formatf(m_TokenPrefix, " 0x%016" I64FMT "x", m_Ctx ? (m_Ctx->Exists() ? m_Ctx->GetSessionInfo().GetToken().GetValue() : 0) : 0);
}

///////////////////////////////////////////////////////////////////////////////
// snQueryHostConfigTask
///////////////////////////////////////////////////////////////////////////////
unsigned snQueryHostConfigTask::sm_TimeoutMs = snQueryHostConfigTask::DEFAULT_TIMEOUT_MS;
unsigned snQueryHostConfigTask::sm_MaxAttempts = snQueryHostConfigTask::DEFAULT_MAX_ATTEMPTS;

snQueryHostConfigTask::snQueryHostConfigTask()
: m_ChannelId(NET_INVALID_CHANNEL_ID)
, m_TxId(NET_INVALID_TRANSACTION_ID)
, m_QueryCount(0)
, m_SessionConfig(NULL)
, m_HostPeerInfo(NULL)
{
    m_RespHandler.Bind(this, &snQueryHostConfigTask::OnResponse);
}

snQueryHostConfigTask::~snQueryHostConfigTask()
{
}

bool
snQueryHostConfigTask::Configure(netConnectionManager* cxnMgr,
                                const unsigned channelId,
                                const EndpointId hostEndpointId,
                                const rlSessionInfo& sessionInfo,
                                rlSessionConfig* sessionConfig,
                                rlPeerInfo* hostPeerInfo)
{
    m_Transactor.Init(cxnMgr);
    m_ChannelId = channelId;
    m_SessionInfo = sessionInfo;
    m_HostEndpointId = hostEndpointId;
    m_TxId = NET_INVALID_TRANSACTION_ID;
    m_QueryCount = 0;
    m_SessionConfig = sessionConfig;
    m_SessionConfig->Clear();
    m_HostPeerInfo = hostPeerInfo;
    m_HostPeerInfo->Clear();
    return true;
}

void
snQueryHostConfigTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Querying host for session config %s", FinishString(finishType));
    
    if(m_RespHandler.Pending())
    {
        m_RespHandler.Cancel();
    }

    this->rlTaskBase::Finish(finishType, resultCode);
}

void
snQueryHostConfigTask::Update(const unsigned timeStep)
{
    this->rlTaskBase::Update(timeStep);

    if(WasCanceled())
    {
        this->Finish(FINISH_CANCELED);
    }
    else if(!m_RespHandler.Pending())
    {
        if(m_QueryCount < sm_MaxAttempts)
        {
            if(!this->SendQuery())
            {
                this->Finish(FINISH_FAILED);
            }
        }
        else
        {
            this->Finish(FINISH_FAILED);
        }
    }
}

//private:

void
snQueryHostConfigTask::DoCancel()
{
    if(m_RespHandler.Pending())
    {
        m_Transactor.CancelRequest(&m_RespHandler);
    }
}

bool
snQueryHostConfigTask::SendQuery()
{
    //Request the session config from the host.
    rlTaskDebug("Querying host (endpointId %u) for session config - attempt:%u", 
				m_HostEndpointId, m_QueryCount);

    snMsgConfigRequest configReq;
    configReq.Reset(m_SessionInfo);

    bool success;

    if(m_QueryCount)
    {
        success = m_Transactor.ResendRequestOutOfBand(m_HostEndpointId,
													m_ChannelId,
													m_TxId,
													configReq,
													sm_TimeoutMs,
													&m_RespHandler);
    }
    else
    {
        success = m_Transactor.SendRequestOutOfBand(m_HostEndpointId,
													m_ChannelId,
													&m_TxId,
													configReq,
													sm_TimeoutMs,
													&m_RespHandler);
    }
    ++m_QueryCount;

    return success;
}

void
snQueryHostConfigTask::OnResponse(netTransactor* /*transactor*/,
                                    netResponseHandler* /*handler*/,
                                    const netResponse* resp)

{
    if(resp->Complete() && !WasCanceled())
    {
        if(resp->Answered())
        {
            //Get the host's response
            snMsgConfigResponse configResp;

            if(!snVerify(configResp.Import(resp->m_Data, resp->m_SizeofData)))
            {
                rlTaskError("Failed to import %s", configResp.GetMsgName());
            }
            else
            {
                *m_SessionConfig = configResp.m_Config;
                *m_HostPeerInfo = configResp.m_HostPeerInfo;
                snAssert(m_SessionInfo == configResp.m_SessionInfo);

                this->Finish(FINISH_SUCCEEDED);
            }
        }
        else
        {
            rlTaskDebug("Query config request:" NET_ADDR_FMT " timed out or failed",
                        NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr));
        }
    }
}

void
snQueryHostConfigTask::SetRetryPolicy(const unsigned timeoutMs, 
                                      const unsigned maxAttempts)
{
    snAssert(timeoutMs > 0);
    snAssert(maxAttempts > 0);

	if(sm_TimeoutMs != timeoutMs)
	{
		snDebug("Setting snQueryHostConfigTask timeout to %u", timeoutMs);
		sm_TimeoutMs = timeoutMs;
	}

	if(sm_MaxAttempts != maxAttempts)
	{
		snDebug("Setting snQueryHostConfigTask max attempts to %u", maxAttempts);
		sm_MaxAttempts = maxAttempts;
	}
}

///////////////////////////////////////////////////////////////////////////////
// snHandleJoinRequestTask
///////////////////////////////////////////////////////////////////////////////

snHandleJoinRequestTask::snHandleJoinRequestTask()
{

}

snHandleJoinRequestTask::~snHandleJoinRequestTask()
{
}

bool
snHandleJoinRequestTask::Configure(snSession* /*ctx*/,
                                   const snMsgJoinRequest& rqst,
                                   const bool peerAlreadyExists)
{
    snAssert(rqst.m_GamerInfo.IsRemote());
    m_Rqst = rqst;
    m_PeerAlreadyExists = peerAlreadyExists;
    return true;
}

void
snHandleJoinRequestTask::Start()
{
    rlTaskDebug("Handling \"%s\"'s request to join the session in a %s slot...",
				m_Rqst.m_GamerInfo.GetName(),
				RL_SLOT_PUBLIC == m_Rqst.m_SlotType ? "public" : "private");

    this->snTask::Start();

    rtry
    {
        rcheck(m_Ctx->Exists(), 
               catchall, 
               rlTaskError("snSession does not exist"));

        //At this point the gamer should have already been added to the snSession,
        //though he may have been removed due to a dropped connection.
        rcheck(m_Ctx->IsMember(m_Rqst.m_GamerInfo.GetGamerId()),
               catchall,
               rlTaskError("Gamer:\"%s\" is not a member of session",
							m_Rqst.m_GamerInfo.GetName()));

        rverify(m_Ctx->HasPeer(m_Rqst.m_GamerInfo.GetPeerInfo().GetPeerId()),
                catchall,
                rlTaskError("Cannot handle join request from non-existent peer"));

        snAssert(m_Ctx->m_RlSession.GetEmptySlots(m_Rqst.m_SlotType));
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snHandleJoinRequestTask::Finish(const FinishType finishType, const int /*resultCode*/)
{
    rlTaskDebug("Joining gamer:\"%s\" %s", m_Rqst.m_GamerInfo.GetName(), FinishString(finishType));

    //Between the time this task started and the time it finished
    //the peer could have dropped so check for that.

    if(FinishSucceeded(finishType))
    {
        if(m_Ctx->IsMember(m_Rqst.m_GamerInfo.GetGamerId()))
        {
            const snPeer* peer = m_Ctx->FindPeerById(m_Rqst.m_GamerInfo.GetPeerInfo().GetPeerId());

            snGamer* gamer = m_Ctx->FindGamerById(m_Rqst.m_GamerInfo.GetGamerId());
            snAssert(gamer);
            snAssert(!gamer->m_FullyJoined);

            gamer->m_FullyJoined = true;

            if(!m_PeerAlreadyExists)
            {
                m_Ctx->InformJoinerOfPeers(m_Rqst.m_GamerInfo, m_Ctx->TotalReservedSlots(), m_Ctx->GetReservedSlots());
            }

            m_Ctx->InformPeersOfJoiner(m_Rqst.m_GamerInfo, m_Rqst.m_GroupMembers, m_Rqst.m_GroupSize);

            //If we're in progress send a start command to the new peer.
            if(snVerify(peer) && !m_PeerAlreadyExists)
            {
                snMsgStartMatchCmd startCmd;
                startCmd.Reset(m_Ctx->GetSessionId());

                rlTaskDebug("Sending %s to \"%s\"...", startCmd.GetMsgName(), m_Rqst.m_GamerInfo.GetName());

                //FIXME (KB) - doesn't need to be a transaction.
                m_Ctx->m_Transactor.SendRequest(peer->GetCxnId(), NULL, startCmd, 0, NULL);
            }
        }
        else
        {
            rlTaskDebug("Join succeeded, but player left or was disconnected");
        }

        this->snTask::Finish(FINISH_SUCCEEDED);
    }
    else
    {
        this->snTask::Finish(FINISH_FAILED);
    }
}

void
snHandleJoinRequestTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);
	this->Finish(FINISH_SUCCEEDED);
}

///////////////////////////////////////////////////////////////////////////////
// snConnectToPeerTask
///////////////////////////////////////////////////////////////////////////////

snConnectToPeerTask::snConnectToPeerTask()
: m_State(STATE_INVALID)
, m_CxnId(-1)
{
}

snConnectToPeerTask::~snConnectToPeerTask()
{
    if(m_TunnelRqst.Pending())
    {
        m_TunnelRqst.Cancel();
    }
}

bool
snConnectToPeerTask::Configure(snSession* /*ctx*/,
                                const rlPeerAddress& peerAddr,
                                const rlNetworkMode netMode)
{
    snAssert(peerAddr.IsValid());
    snAssert(!m_Ctx->IsHost());

    bool success = false;

    m_PeerAddr = peerAddr;
	m_EndpointId = NET_INVALID_ENDPOINT_ID;
    m_CxnId = -1;

    const netTunnelType tunnelType =
        (RL_NETMODE_LAN == netMode)
            ? NET_TUNNELTYPE_LAN
            : NET_TUNNELTYPE_ONLINE;

	const bool isHost = peerAddr.GetPeerId() == m_Ctx->GetSessionInfo().GetHostPeerAddress().GetPeerId();
	const netTunnelDesc tunnelDesc(tunnelType, NET_TUNNEL_REASON_P2P, m_Ctx->GetSessionInfo().GetToken().GetValue(), !isHost);

    if(snVerify(m_Ctx->m_CxnMgr &&
				m_Ctx->m_CxnMgr->OpenTunnel(m_PeerAddr,
											tunnelDesc,
                                            &m_TunnelRqst,
                                            &m_MyStatus)))
    {
        snAssert(m_MyStatus.Pending() || m_MyStatus.Succeeded());   //Trying to catch a bug

        m_State = STATE_OPENING_TUNNEL;
        success = true;
    }
    else
    {
        rlTaskError("Failed to open tunnel with peer; possible cause is cookie already in use");
    }

    return success;
}

void
snConnectToPeerTask::Start()
{
    rlTaskDebug("Connecting to peer...");

    snAssert(-1 == m_CxnId);
	snAssert(!NET_IS_VALID_ENDPOINT_ID(m_EndpointId));

    this->snTask::Start();

    if(!m_Ctx->Exists())
    {
       rlTaskError("snSession does not exist");
       this->Finish(FINISH_FAILED);
    }
}

void
snConnectToPeerTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Connecting to peer %s", FinishString(finishType));

    if(!FinishSucceeded(finishType))
    {
		if(m_Ctx->m_CxnMgr)
		{
			m_Ctx->m_CxnMgr->CancelTunnelRequest(&m_TunnelRqst);
		}
		netAssert(!m_MyStatus.Pending());

		CloseConnection();
    }

    this->snTask::Finish(finishType, resultCode);
}

void
snConnectToPeerTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    do
    {
        switch(m_State)
        {
        case STATE_OPENING_TUNNEL:
            if(m_MyStatus.Succeeded())
            {
				rlTaskDebug("Opened Tunnel. Connecting to peer...");
				m_EndpointId = m_TunnelRqst.GetEndpointId();
                m_State = STATE_CONNECT_TO_PEER;
            }
            else if(!m_MyStatus.Pending())
            {
                rlTaskError("Failed to negotiate NAT");
                rlTaskError("  Tunnel request status: %d", m_MyStatus.GetStatus());

                m_State = STATE_CONNECT_TO_PEER;
            }
            break;

        case STATE_CONNECT_TO_PEER:
			{
				if(NET_IS_VALID_ENDPOINT_ID(m_EndpointId))
				{
					rlTaskDebug("Connecting to peer:" NET_ADDR_FMT "",
								NET_ADDR_FOR_PRINTF(m_TunnelRqst.GetNetAddress()));

					m_CxnId = m_Ctx->m_CxnMgr->OpenConnection(m_EndpointId,
															  m_Ctx->m_ChannelId,
															  NULL,
															  0,
															  NULL);
				}
				else
				{
					//NOTE: GTA V requires the connection to be created even if the tunnel request fails.
					// See comments in snAddRemoteGamerTask::Update():
					//		Always attempt to create the peer if it doesn't exist, even
					//		if connecting to him failed.

					//Generate fake IPs in the range 127.0.0.16 to 127.255.255.255
					static unsigned s_NextFakeIp = 0;
					const unsigned fakeIp = 0x7F000010 | (s_NextFakeIp & 0x00FFFFFF);
					++s_NextFakeIp;
					netAddress fakeAddr(netIpAddress(netIpV4Address(fakeIp)), 0xFFFF);

					rlTaskDebug("Connecting to peer with fake address:" NET_ADDR_FMT "",
								NET_ADDR_FOR_PRINTF(fakeAddr));

					m_CxnId = m_Ctx->m_CxnMgr->OpenConnection(m_PeerAddr,
															  fakeAddr,
															  m_Ctx->m_ChannelId,
															  NULL,
															  0,
															  NULL);

					if(m_CxnId >= 0)
					{
						m_EndpointId = m_Ctx->m_CxnMgr->GetEndpointId(m_CxnId);
					}
				}

				if(m_CxnId >= 0)
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				else
				{
					this->Finish(FINISH_FAILED);
				}
			}
			
            break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

void
snConnectToPeerTask::CloseConnection()
{
    if(m_CxnId >= 0)
    {
		if(m_Ctx->m_CxnMgr)
		{
			m_Ctx->m_CxnMgr->CloseConnection(m_CxnId, NET_CLOSE_IMMEDIATELY);
		}
		m_CxnId = -1;
    }
    
	m_EndpointId = NET_INVALID_ENDPOINT_ID;
}

int
snConnectToPeerTask::GetCxnId() const
{
    snAssert(this->IsFinished());
    return m_CxnId;
}

EndpointId
snConnectToPeerTask::GetEndpointId() const
{
    snAssert(this->IsFinished());
    return m_EndpointId;
}

///////////////////////////////////////////////////////////////////////////////
// snAddRemoteGamerTask
///////////////////////////////////////////////////////////////////////////////

snAddRemoteGamerTask::snAddRemoteGamerTask()
: m_State(STATE_INVALID)
, m_GamerWasDroppedWhileAdding(false)
{
    //We're going to put this task on the session's concurrent
    //task list, but we don't want it to be deleted when it's finished.
    m_ConnectToPeerTask.m_DeleteWhenFinished = false;
}

snAddRemoteGamerTask::~snAddRemoteGamerTask()
{

}

bool
snAddRemoteGamerTask::Configure(snSession* /*ctx*/,
                                const snMsgAddGamerToSessionCmd& agCmd)
{
    snAssert(agCmd.m_GamerInfo.IsRemote());
    snAssert(!m_Ctx->IsHost());
    m_AgCmd = agCmd;

    bool success = false;

	const u64 peerId = m_AgCmd.m_GamerInfo.GetPeerInfo().GetPeerId();
	const snPeer* peer = m_Ctx->FindPeerById(peerId);
	if(peer)
    {
        rlTaskDebug("Already have the peer - no need to NAT");

		// check if the relay address should be updated
		if(agCmd.m_RelayAddr.IsRelayServerAddr() && (m_Ctx->m_CxnMgr->GetRelayAddress(peer->GetEndpointId()) != agCmd.m_RelayAddr))
		{
			m_Ctx->m_CxnMgr->UpdateEndpointAddress(peer->GetPeerInfo().GetPeerAddress().GetPeerId(),
													m_Ctx->m_CxnMgr->GetAddress(peer->GetEndpointId()),
													agCmd.m_RelayAddr);
		}
		m_State = STATE_ADD_GAMER;
        success = true;
    }
    else if(rlTaskBase::Configure(&m_ConnectToPeerTask,
                                    m_Ctx,
                                    agCmd.m_GamerInfo.GetPeerInfo().GetPeerAddress(),
                                    m_Ctx->GetConfig().m_NetworkMode,
                                    &m_MyStatus))
    {
        m_ConnectToPeerTask.Start();
        m_Ctx->AddConcurrentTask(&m_ConnectToPeerTask);
        m_State = STATE_CONNECTING_TO_PEER;
        success = true;
    }

    return success;
}

void
snAddRemoteGamerTask::Start()
{
    rlTaskDebug("Adding remote gamer:\"%s\"...", m_AgCmd.m_GamerInfo.GetName());

    this->snTask::Start();

    rtry
    {
        rcheck(m_Ctx->Exists(), 
               catchall, 
               rlTaskError("snSession does not exist"));

        //We may have become the host between the time this task
        //was queued and the time we started processing it.
        //This task is intended to be run only by non-hosts, so we
        //kick the guy and abort it.

        //TMS REMOVED: 	We were getting situations where connections during migrations
        //				were failing totally.
        //if(m_Ctx->IsHost())
        //{
        //    //Tell everyone else to kick the guy.

        //    snMsgRemoveGamersFromSessionCmd rgCmd;
        //    rgCmd.Reset(m_Ctx->GetSessionId(), &m_AgCmd.m_GamerInfo.GetGamerId(), 1);
        //    m_Ctx->BroadcastCommand(rgCmd, NULL);

        //    rthrow(catchall,
        //            rlTaskError("Ignoring %s - we're now the host", this->GetTaskName()));
        //}
        //else
        {
            rcheck(!m_Ctx->IsMember(m_AgCmd.m_GamerInfo.GetGamerId()),
                   catchall,
                   rlTaskError("Gamer is already member of session"));
        }
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snAddRemoteGamerTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Adding remote gamer:\"%s\" %s",
            m_AgCmd.m_GamerInfo.GetName(),
            FinishString(finishType));

    if(FinishSucceeded(finishType))
	{
		snGamer* gamer = m_Ctx->FindGamerById(m_AgCmd.m_GamerInfo.GetGamerId());
		if(rlVerify(gamer && !gamer->m_FullyJoined))
		{
			rlTaskDebug("Marking as fully joined");
			m_Ctx->FillSlotReservation(gamer->GetGamerHandle());
			gamer->m_FullyJoined = true;
		}
	}
	else
    {
        if(m_ConnectToPeerTask.IsActive())
        {
            m_ConnectToPeerTask.Finish(FINISH_FAILED);
            m_Ctx->m_ConcurrentTasks.erase(&m_ConnectToPeerTask);
		}

		// housekeeping
		if(m_State == STATE_WAIT_FOR_JOIN && m_MyStatus.Pending())
		{
			m_MyStatus.SetFailed(resultCode);
		}

        const u64 peerId = m_AgCmd.m_GamerInfo.GetPeerInfo().GetPeerId();
        const snPeer* peer = m_Ctx->FindPeerById(peerId);
        if(peer)
        {
            if(0 == peer->GetGamerCount())
            {
                rlTaskDebug("Dropping peer that we added before add gamer failed");
                if(m_Ctx->IsHost())
                {
                    m_Ctx->DropPeer(peerId, NULL, NULL);
                }
                else
                {
                    m_Ctx->RemovePeer(peerId);
                }
            }
#if !__NO_OUTPUT
			else
			{
				rlTaskDebug("Cannot drop peer, has %u gamers", peer->GetGamerCount());

				// make sure that all players are pending drop
				rlGamerId gamers[RL_MAX_GAMERS_PER_SESSION];
				unsigned numGamers = peer->GetGamers(gamers, RL_MAX_GAMERS_PER_SESSION);

				for(unsigned i = 0; i < numGamers; i++)
				{
					snGamer* gamer = m_Ctx->FindGamerById(gamers[i]);
					rlTaskDebug("Gamer %s is %s", gamer->GetGamerInfo().GetName(), gamer->m_IsPendingDrop ? "Pending Drop" : "Not Pending Drop");
				}
			}
#endif
        }
        else if(m_ConnectToPeerTask.IsFinished() && m_ConnectToPeerTask.GetCxnId() >= 0 && m_Ctx->m_CxnMgr)
        {
            rlTaskDebug("Closing the connection that was opened before the add gamer failed");
            m_Ctx->m_CxnMgr->CloseConnection(m_ConnectToPeerTask.GetCxnId(),
                                            NET_CLOSE_IMMEDIATELY);
        }

        peer = m_Ctx->FindPeerById(peerId);
        if(peer && m_GamerWasDroppedWhileAdding)
        {
			if(0 == peer->GetGamerCount())
			{
				if(m_Ctx->IsHost())
				{
					m_Ctx->DropPeer(peerId, NULL, NULL);
				}
				else
				{
					m_Ctx->RemovePeer(peerId);
				}
			}
#if !__NO_OUTPUT
			else
			{
				rlTaskDebug("Cannot drop peer, has %u gamers", peer->GetGamerCount());
				
				// make sure that all players are pending drop
				rlGamerId gamers[RL_MAX_GAMERS_PER_SESSION];
				unsigned numGamers = peer->GetGamers(gamers, RL_MAX_GAMERS_PER_SESSION);

				for(unsigned i = 0; i < numGamers; i++)
				{
					snGamer* gamer = m_Ctx->FindGamerById(gamers[i]);
					rlTaskDebug("Gamer %s is %s", gamer->GetGamerInfo().GetName(), gamer->m_IsPendingDrop ? "Pending Drop" : "Not Pending Drop");
				}
			}
#endif
        }
    }

    this->snTask::Finish(finishType, resultCode);
}

void
snAddRemoteGamerTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

	if(snSession::AllowConnectToPeerChecks())
	{
		if(!snVerify(m_Ctx->m_IsInitialized))
		{
			rlTaskError("Owning session is not initialized.");
			this->Finish(FINISH_FAILED);
			return;
		}
	}

    do
    {
        switch(m_State)
        {
        case STATE_CONNECTING_TO_PEER:
            if(m_MyStatus.Pending())
            {
                if(m_GamerWasDroppedWhileAdding)
                {
                    this->Finish(FINISH_FAILED);
                }
            }
            else
            {
                bool hasPeer = m_Ctx->FindPeerById(m_AgCmd.m_GamerInfo.GetPeerInfo().GetPeerId()) != 0;

                //Always attempt to create the peer if it doesn't exist, even
                //if connecting to him failed.
                if(!hasPeer && !m_GamerWasDroppedWhileAdding)
                {
                    rlTaskDebug("Connecting to remote peer:" NET_ADDR_FMT "...", NET_ADDR_FOR_PRINTF(m_Ctx->GetCxnMgr()->GetAddress(m_ConnectToPeerTask.GetEndpointId())));

                    //Connect to the peer on the negotiated address.
                    const snPeer* peer =
                        m_Ctx->ConnectToPeer(m_AgCmd.m_GamerInfo.GetPeerInfo(),
											 m_ConnectToPeerTask.GetCxnId(),
                                             m_ConnectToPeerTask.GetEndpointId(),
                                             NULL,
                                             0,
                                             NULL);

                    hasPeer = (NULL != peer);
                }

                if(hasPeer && !m_GamerWasDroppedWhileAdding)
                {
					m_State = STATE_ADD_GAMER;
                }
                else
                {
                    rlTaskError("Failed to connect to peer");
                    this->Finish(FINISH_FAILED);
                }
            }
            break;

		case STATE_WAIT_FOR_JOIN:
			{
				if(!m_Ctx->IsJoining())
				{
					rlTaskDebug("Join Complete");
					m_State = STATE_ADD_GAMER;
					m_MyStatus.SetSucceeded();
				}
			}
			break;

        case STATE_ADD_GAMER:

			// wait for the join to complete.
			if(m_Ctx->IsJoining())
			{
				rlTaskDebug("Joining session. Waiting...");
				m_State = STATE_WAIT_FOR_JOIN;
				m_MyStatus.SetPending();
			}
			else
			{
				rlTaskDebug("Adding gamer...");

				if(!m_GamerWasDroppedWhileAdding &&
					snVerify(m_Ctx->AddGamer(m_AgCmd.m_GamerInfo,
												 m_AgCmd.m_SlotType,
												 m_AgCmd.m_SizeofData ? m_AgCmd.m_Data : NULL,
												 m_AgCmd.m_SizeofData)))
				{
					this->Finish(FINISH_SUCCEEDED);
				}
				else
				{
					rlTaskError("Failed to add gamer");
					this->Finish(FINISH_FAILED);
				}
			}
            break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

void 
snAddRemoteGamerTask::Drop(const rlGamerId gamerIds[], const unsigned int numGamerIds)
{
    m_GamerWasDroppedWhileAdding = false;

    for(unsigned int i = 0; i < (int)numGamerIds; ++i)
    {
        if(m_AgCmd.m_GamerInfo.GetGamerId() == gamerIds[i])
        {
            m_GamerWasDroppedWhileAdding = true;

            // if we configured the task but haven't started yet, stop the task here
            if(this->IsPending() && !this->IsActive())
            {
                rlTaskDebug("Dropping peer that we were asked to remove before the add task had started.");
                this->Finish(FINISH_FAILED);
            }
            else
            {
                rlTaskDebug("Dropping peer that we were asked to remove while we were in the middle of adding it.");
            }
            return;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
// snJoinGamersToRlineTask
///////////////////////////////////////////////////////////////////////////////

snJoinGamersToRlineTask::snJoinGamersToRlineTask()
    : m_State(STATE_INVALID)
    , m_NumGamers(0)
{
}

snJoinGamersToRlineTask::~snJoinGamersToRlineTask()
{
}

bool
snJoinGamersToRlineTask::Configure(snSession* /*ctx*/)
{
    m_State = -1;
    m_NumGamers = 0;
    return true;
}

bool
snJoinGamersToRlineTask::Configure(snSession* /*ctx*/,
                                     const rlGamerHandle* gamerHandles,
									 const rlGamerId* gamerIds,
                                     const unsigned numGamers)
{
    snAssert(numGamers > 0);
    snAssert(numGamers <= RL_MAX_GAMERS_PER_SESSION);

    bool success = false;
    m_NumGamers = 0;

    for(int i = 0; i < (int) numGamers; ++i)
    {
        m_GamerHandles[m_NumGamers] = gamerHandles[i];
		m_GamerIds[m_NumGamers] = gamerIds[i];
		m_Dropped[m_NumGamers] = false;
		m_NumGamers++;
    }

    success = true;

    return success;
}

bool
snJoinGamersToRlineTask::AddGamer(const rlGamerHandle& gamerHandle,
								  const rlGamerId& gamerId,
								  const rlSlotType slot,
								  const bool OUTPUT_ONLY(isJoinRequest),
                                  netStatus** status)
{
	bool success = false;

    if(snVerify(m_NumGamers < COUNTOF(m_GamerHandles)))
    {
        m_GamerHandles[m_NumGamers] = gamerHandle;
		m_GamerIds[m_NumGamers] = gamerId;
		m_Dropped[m_NumGamers] = false;
		m_Slots[m_NumGamers] = slot;
        *status = &m_AddGamerStatus[m_NumGamers];
        ++m_NumGamers;

        OUTPUT_ONLY(char ghBuf[RL_MAX_GAMER_HANDLE_CHARS]);
        rlTaskDebug("Adding gamer %s - %" I64FMT "u - via %s", gamerHandle.ToString(ghBuf), gamerId.GetId(), isJoinRequest ? "Join Request" : "Add Gamer Command");

        success = true;
    }

    return success;
}

bool
snJoinGamersToRlineTask::IsWaitingForAddGamers()
{
    return (m_State <= STATE_WAITING_FOR_ADD_GAMER_TASKS);
}

bool 
snJoinGamersToRlineTask::IsAdding(const rlGamerId gamerIds[], const unsigned int numGamerIds)
{
	for(int i = 0; i < (int) numGamerIds; ++i)
	{
		for(int j = 0; j < (int) m_NumGamers; ++j)
		{	
			if(gamerIds[i] == m_GamerIds[j])
			{
				if(m_AddGamerStatus[j].Pending())
				{
					return true;
				}
			}
		}
	}
	return false;
}

void
snJoinGamersToRlineTask::Start()
{
    rlTaskDebug("Adding: %u gamers to rlSession...", m_NumGamers);

    m_State = STATE_WAITING_FOR_ADD_GAMER_TASKS;

    this->snTask::Start();
}

void
snJoinGamersToRlineTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Adding: %u gamers %s", m_NumGamers, FinishString(finishType));

    if(FINISH_SUCCEEDED == finishType)
    {
        for(int i = 0; i < (int) m_NumGamers; ++i)
        {
            snGamer* gamer = m_Ctx->FindGamerByHandle(m_GamerHandles[i]);

            if(!m_Dropped[i])
            {
				if(gamer)
				{
					snAssert(!gamer->m_JoinedToRline);
					gamer->m_JoinedToRline = true;
				}
				else
				{
					OUTPUT_ONLY(char ghBuf[RL_MAX_GAMER_HANDLE_CHARS]);
					rlTaskDebug("Gamer %s - %" I64FMT "u - was dropped", m_GamerHandles[i].ToString(ghBuf), m_GamerIds[i].GetId());
				}
            }
        }
    }
    else
    {
        //Need to kill any in-flight add gamer tasks
        for(int i = 0; i < (int) m_NumGamers; ++i)
        {
            if(m_AddGamerStatus[i].Pending())
            {
                rlTaskBase* task = rlGetTaskManager()->FindTask(&m_AddGamerStatus[i]);
                if(task)
                {
					snTask* sntask = (snTask*)task;
					if(sntask)
					{
						if(snAddRemoteGamerTask::ISA(sntask))
						{
							snAddRemoteGamerTask* addGamersTask = (snAddRemoteGamerTask*)task;
							addGamersTask->Finish(finishType);
						}
						else if(snHandleJoinRequestTask::ISA(sntask))
						{
							snHandleJoinRequestTask* handleJoinRequestTask = (snHandleJoinRequestTask*)task;
							handleJoinRequestTask->Finish(finishType);
						}
					}
                }
            }
        }
    }

	// cancel rl command if in flight
	if(m_MyStatus.Pending())
	{
		snAssert(!FinishSucceeded(finishType));
		m_Ctx->m_RlSession.Cancel(&m_MyStatus);
	}

    this->snTask::Finish(finishType, resultCode);
}

void
snJoinGamersToRlineTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    do
    {
        switch(m_State)
        {
        case STATE_WAITING_FOR_ADD_GAMER_TASKS:
            {
                bool finishedAddingGamers = true;

                //Check the status objects
                for(int i = 0; i < (int) m_NumGamers; ++i)
                {
                    if(m_AddGamerStatus[i].Pending())
                    {
                        finishedAddingGamers = false;
                        break;
                    }
                }

                if(finishedAddingGamers)
                {
                    if(!m_Ctx->Exists())
                    {
                        rlTaskError("Session no longer exists!");
                        this->Finish(FINISH_FAILED);
                    }
                    else
                    {
						rlTaskDebug("All pending tasks completed");
						
						RemoveDroppedGamers();

                        if(m_NumGamers > 0)
                        {
                            if(!m_Ctx->m_RlSession.Join(m_GamerHandles, m_Slots, m_NumGamers, &m_MyStatus))
                            {
                                rlTaskError("Error adding gamers to rlSession");
                                this->Finish(FINISH_FAILED);
                            }
                            else
                            {
                                m_State = STATE_JOINING;
                            }
                        }
                        else
                        {
                            rlTaskDebug("No gamers left to add to rlSession");
                            this->Finish(FINISH_SUCCEEDED);
                        }
                    }
                }
            }
            break;

        case STATE_JOINING:
            if(!m_MyStatus.Pending())
            {
				this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
			}
            break;
        }
    }
    while((!m_MyStatus.Pending() && this->IsActive()) && m_State != STATE_WAITING_FOR_ADD_GAMER_TASKS);
}

void
snJoinGamersToRlineTask::RemoveDroppedGamers()
{
	//Make sure the gamers are still members of the session.
	for(int i = 0; i < (int) m_NumGamers; ++i)
	{
		if(!m_Ctx->IsMember(m_GamerHandles[i]) || m_Dropped[i])
		{
			OUTPUT_ONLY(char ghBuf[RL_MAX_GAMER_HANDLE_CHARS]);
			rlTaskDebug("%s is no longer a member - not adding to rlSession",
				m_GamerHandles[i].ToString(ghBuf));

			m_GamerHandles[i] = m_GamerHandles[m_NumGamers-1];
			m_GamerIds[i] = m_GamerIds[m_NumGamers-1];
			m_Dropped[i] = m_Dropped[m_NumGamers-1];
			m_Slots[i] = m_Slots[m_NumGamers-1];
			--m_NumGamers;
			--i;
		}
	}
}

void 
snJoinGamersToRlineTask::DropGamers(const rlGamerId gamerIds[], const unsigned int numGamerIds)
{
	//This is only for client path (in response to host dropping players)
	if(m_Ctx->IsHost())
	{
		return;
	}

    for(int i = 0; i < (int) numGamerIds; ++i)
    {
        rlTaskDebug("Checking if gamer %" I64FMT "u should be dropped", gamerIds[i].GetId());

        for(int j = 0; j < (int) m_NumGamers; ++j)
        {	
	        if(gamerIds[i] == m_GamerIds[j])
	        {
		        if(m_AddGamerStatus[j].Pending())
		        {
					rlTaskBase* task = rlGetTaskManager()->FindTask(&m_AddGamerStatus[j]);
					if(task)
					{
						snTask* sntask = (snTask*)task;
						if(sntask && snAddRemoteGamerTask::ISA(sntask))
						{
							snAddRemoteGamerTask* addGamerTask = (snAddRemoteGamerTask*)task;
							addGamerTask->Drop(&m_GamerIds[j], 1);
						}
					}
		        }

                OUTPUT_ONLY(char ghBuf[RL_MAX_GAMER_HANDLE_CHARS]);
                rlTaskDebug("Dropping gamer %s - %" I64FMT "u", m_GamerHandles[j].ToString(ghBuf), m_GamerIds[j].GetId());

		        // mark as dropped - we cannot shuffle as the m_AddGamerStatus are looked up by address
		        m_Dropped[j] = true; 
                break;
	        }
        }
	}
}

void 
snJoinGamersToRlineTask::DropGamers()
{
	for(int j = 0; j < (int) m_NumGamers; ++j)
	{	
		if(m_AddGamerStatus[j].Pending())
		{
			rlTaskBase* task = rlGetTaskManager()->FindTask(&m_AddGamerStatus[j]);
			if(task)
			{
				snAddRemoteGamerTask* addGamerTask = (snAddRemoteGamerTask*)task;
				addGamerTask->Drop(&m_GamerIds[j], 1);
			}
		}

		OUTPUT_ONLY(char ghBuf[RL_MAX_GAMER_HANDLE_CHARS]);
		rlTaskDebug("Dropping gamer %s - %" I64FMT "u", m_GamerHandles[j].ToString(ghBuf), m_GamerIds[j].GetId());

		m_Dropped[j] = true; 
	}
}

///////////////////////////////////////////////////////////////////////////////
// snJoinCompleteTask
///////////////////////////////////////////////////////////////////////////////

snJoinCompleteTask::snJoinCompleteTask()
#if RSG_DURANGO
	: m_NumDependencies(0)
#endif
{

}

snJoinCompleteTask::~snJoinCompleteTask()
{

}

bool
snJoinCompleteTask::Configure(snSession* /*ctx*/)
{
	// nothing to do here
	return true;
}

void
snJoinCompleteTask::Finish(const FinishType finishType, const int resultCode)
{
	if(finishType == FINISH_SUCCEEDED)
	{
		// mark session as established
		m_Ctx->m_IsEstablished = true; 
	}

	//Save the pointer before calling finish.
	snSession* ctx = m_Ctx;

	//Call finish before dispatching the event because the event
	//can cause the task to be deleted, and calling Finish() after
	//that would be fatal.
	this->snTask::Finish(finishType, resultCode);

	if(finishType == FINISH_SUCCEEDED)
	{
		// notify the app
		snEventSessionEstablished e;
		ctx->DispatchEvent(&e);
	}
#if RSG_DURANGO
	else
	{
		// need to kill any in-flight dependencies
		for(int i = 0; i < (int) m_NumDependencies; ++i)
		{
			if(m_DependencyTasks[i] && m_DependencyStatus[i].Pending())
			{
				m_DependencyTasks[i]->Finish(finishType);
			}
		}
	}
#endif
}

void
snJoinCompleteTask::Update(const unsigned timeStep)
{
	this->snTask::Update(timeStep);

#if RSG_DURANGO
	// assume true, which will complete the task
	bool finishedDependencies = true;

	// if we have dependencies
	if(m_NumDependencies > 0)
	{
		// check all status objects
		for(unsigned i = 0; i < m_NumDependencies; ++i)
		{
			if(m_DependencyStatus[i].Pending())
			{
				finishedDependencies = false;
				break;
			}
		}
	}

	if(finishedDependencies)
	{
		this->Finish(FINISH_SUCCEEDED);
	}
#else
	this->Finish(FINISH_SUCCEEDED);
#endif
}

#if RSG_DURANGO
void 
snJoinCompleteTask::AddDependency(netStatus** status, snNotifyAddGamerTask* task)
{
	*status = &m_DependencyStatus[m_NumDependencies];
	m_DependencyTasks[m_NumDependencies] = task;
	++m_NumDependencies;
}
#endif

///////////////////////////////////////////////////////////////////////////////
// snLeaveGamersFromRlineTask
///////////////////////////////////////////////////////////////////////////////

snLeaveGamersFromRlineTask::snLeaveGamersFromRlineTask()
    : m_State(STATE_INVALID)
    , m_NumGamers(0)
{
}

snLeaveGamersFromRlineTask::~snLeaveGamersFromRlineTask()
{
}

bool
snLeaveGamersFromRlineTask::Configure(snSession* /*ctx*/,
                                         const rlGamerHandle* gamerHandles,
                                         const unsigned numGamers)
{
    snAssert(numGamers > 0);
    snAssert(numGamers <= RL_MAX_GAMERS_PER_SESSION);

    bool success = false;
    m_NumGamers = 0;

    for(int i = 0; i < (int) numGamers; ++i)
    {
		m_GamerHandles[m_NumGamers++] = gamerHandles[i];
    }

    success = true;

    return success;
}

void
snLeaveGamersFromRlineTask::Start()
{
    rlTaskDebug("Removing:%d gamers from rlSession/rlMatch...",
            m_NumGamers);

    this->snTask::Start();

    rtry
    {
        rcheck(m_Ctx->Exists(), 
               catchall, 
               rlTaskError("snSession does not exist"));

        this->LeaveSession();
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snLeaveGamersFromRlineTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Removing gamers from rlSession %s", FinishString(finishType));

	if(m_MyStatus.Pending())
	{
		snAssert(!FinishSucceeded(finishType));
		m_Ctx->m_RlSession.Cancel(&m_MyStatus);
	}

    this->snTask::Finish(finishType, resultCode);
}

void
snLeaveGamersFromRlineTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    do
    {
        switch(m_State)
        {
        case STATE_LEAVING_SESSION:
            if(!m_MyStatus.Pending())
            {
                this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
            }
            break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}
void
snLeaveGamersFromRlineTask::LeaveSession()
{
    unsigned numGamers = 0;
    rlGamerHandle gamerHandles[RL_MAX_GAMERS_PER_SESSION];

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        if(m_Ctx->m_RlSession.IsMember(m_GamerHandles[i]))
        {
            gamerHandles[numGamers++] = m_GamerHandles[i];
        }
        else
        {
            rlTaskError("Gamer is not a member of the session");
        }
    }

    if(numGamers)
    {
        if(!snVerify(m_Ctx->m_RlSession.Leave(gamerHandles, numGamers, &m_MyStatus)))
        {
            rlTaskError("Error leaving gamers from session");
            m_MyStatus.ForceSucceeded();
        }
    }
    else
    {
        m_MyStatus.ForceSucceeded();
    }

    m_State = STATE_LEAVING_SESSION;
}

///////////////////////////////////////////////////////////////////////////////
// snDropGamersTask
///////////////////////////////////////////////////////////////////////////////

snDropGamersTask::snDropGamersTask()
: m_RequestingPeerId(RL_INVALID_PEER_ID)
, m_NumGamers(0)
{
}

snDropGamersTask::~snDropGamersTask()
{
}

bool
snDropGamersTask::Configure(snSession* ctx,
                            const u64 requestingPeerId,
                            const rlGamerId* gamerIds,
                            const unsigned numGamers,
							const int removeReason)
{
    snAssert(numGamers <= COUNTOF(m_GamerIds));
    snAssert(!m_Ctx->IsHost());

    m_RequestingPeerId = requestingPeerId;
    m_NumGamers = 0;
	m_RemoveReason = removeReason;

    for(int i = 0; i < (int) numGamers && i < COUNTOF(m_GamerIds); ++i)
    {
        m_GamerIds[m_NumGamers++] = gamerIds[i];

		// tag this gamer as dropping
		snGamer* gamer = ctx->FindGamerById(gamerIds[i]);
		if(gamer)
		{
			gamer->m_IsPendingDrop = true;
		}
    }

    return true;
}

void
snDropGamersTask::Start()
{
    rlTaskDebug("Dropping gamers...");

    this->snTask::Start();

    rtry
    {
        rcheck(m_Ctx->Exists(), 
               catchall, 
               rlTaskError("snSession does not exist"));

        //We may have become the host between the time this task
        //was queued and the time we started processing it.  Because
        //this task is only run on non-hosts, if we're the host we just
        //abort it.
        rcheck(!m_Ctx->IsHost(),
                catchall,
                rlTaskError("Ignoring %s - we're now the host",
                        this->GetTaskName()));

        const unsigned numGamers = m_NumGamers;
        m_NumGamers = 0;

        for(int i = 0; i < (int) numGamers; ++i)
        {
            const snGamer* gamer = m_Ctx->FindGamerById(m_GamerIds[i]);

            //It's possible that we don't have this gamer as a member if
            //a host migration occurred just after the old host told us to
            //remove him.  The new host might also tell us to remove him,
            //but he's already gone.

            if(gamer)
            {
                snAssert(gamer->GetGamerInfo().IsRemote());
                m_GamerHandles[m_NumGamers++] = gamer->GetGamerHandle();
            }
        }

        if(0 == m_NumGamers)
        {
            //We ended up with no gamers to drop because they've already
            //been dropped.  This will occur when a host tells us to drop
            //gamers, then the host migrates, then the new host tells us
            //to drop the same gamers.
            this->Finish(FINISH_SUCCEEDED);
        }
        else if(!m_Ctx->FindPeerById(m_RequestingPeerId))
        {
            //At some point between queuing this task and running it
            //we dropped the peer that requested the drop.
            this->Finish(FINISH_SUCCEEDED);
        }
        else
        {
            rcheck(m_Ctx->DropGamers(m_Ctx->FindPeerById(m_RequestingPeerId),
                                    m_GamerHandles,
                                    m_NumGamers,
                                    snSession::KICK_INFORM_PEERS,
                                    NULL,
									m_RemoveReason,
                                    NULL),
                    catchall,);

            this->Finish(FINISH_SUCCEEDED);
        }
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snDropGamersTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Dropping gamers %s", FinishString(finishType));

    this->snTask::Finish(finishType, resultCode);
}

void
snDropGamersTask::Update(const unsigned /*timeStep*/)
{
}

///////////////////////////////////////////////////////////////////////////////
// snDestroyTask
///////////////////////////////////////////////////////////////////////////////

snDestroyTask::snDestroyTask()
{
}

snDestroyTask::~snDestroyTask()
{
}

bool
snDestroyTask::Configure(snSession* /*ctx*/)
{
    return true;
}

void
snDestroyTask::Start()
{
    rlTaskDebug("Destroying session...");

    this->snTask::Start();

    if(m_Ctx->Exists())
    {
        if(!m_Ctx->m_RlSession.Destroy(&m_MyStatus))
        {
            this->Finish(FINISH_FAILED);
        }
    }
    else
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snDestroyTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Session destruction %s", FinishString(finishType));

	if(m_MyStatus.Pending())
	{
		snAssert(!FinishSucceeded(finishType));
		m_Ctx->m_RlSession.Cancel(&m_MyStatus);
	}

    //Save the pointer before calling finish.
    snSession* ctx = m_Ctx;

    //Call finish before dispatching the event because the event
    //can cause the task to be deleted, and calling Finish() after
    //that would be fatal.	
    this->snTask::Finish(finishType, resultCode);

	ctx->Clear();

    snEventSessionDestroyed e;
	ctx->DispatchEvent(&e);
}

void
snDestroyTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
    }
}

///////////////////////////////////////////////////////////////////////////////
// snModifyPresenceFlagsTask
///////////////////////////////////////////////////////////////////////////////

snModifyPresenceFlagsTask::snModifyPresenceFlagsTask()
: m_PresenceFlags(0)
, m_BroadcastCmd(true)
{
}

bool
snModifyPresenceFlagsTask::Configure(snSession* /*ctx*/, const unsigned presenceFlags)
{
    m_PresenceFlags = presenceFlags;
    m_BroadcastCmd = true;
    return true;
}

void
snModifyPresenceFlagsTask::Start()
{
    this->snTask::Start();

    rtry
    {
        rcheck(m_Ctx->Exists(), catchall,);

		rlTaskDebug("  Presence:          %s",
			(m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE) ? "true" : "false");

		rlTaskDebug("  Join Via Presence: %s",
			(m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_JOIN_VIA_PRESENCE) ? "true" : "false");

		bool invitable = (m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE);
		if(invitable != m_Ctx->IsInvitable())
		{
			//Don't broadcast the command if the invitable setting hasn't changed.
			m_BroadcastCmd = false;
		}

        if(m_PresenceFlags != m_Ctx->m_RlSession.GetPresenceFlags())
        {
            m_Ctx->m_RlSession.ModifyPresenceFlags(m_PresenceFlags, &m_MyStatus);
        }
        else
        {
            this->Finish(FINISH_SUCCEEDED);
        }
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snModifyPresenceFlagsTask::Finish(const FinishType finishType, const int resultCode)
{
	if(FinishSucceeded(finishType) && m_Ctx->Exists())
    {
        if(m_Ctx->IsHost() && m_BroadcastCmd)
        {
            //Tell everyone about the new invitable setting.
            snMsgSetInvitableCmd cmd;
            cmd.Reset(m_Ctx->GetSessionId(), (m_PresenceFlags & RL_SESSION_PRESENCE_FLAG_INVITABLE));

            m_Ctx->BroadcastCommand(cmd, NULL);
        }
    }
	else if(m_MyStatus.Pending())
	{
		m_Ctx->m_RlSession.Cancel(&m_MyStatus);
	}

    this->snTask::Finish(finishType, resultCode);
}

void
snModifyPresenceFlagsTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    if(m_MyStatus.Succeeded())
    {
        this->Finish(FINISH_SUCCEEDED);
    }
    else if(!m_MyStatus.Pending())
    {
        this->Finish(FINISH_FAILED);
    }
}

///////////////////////////////////////////////////////////////////////////////
// snEstablishSessionTask
///////////////////////////////////////////////////////////////////////////////

snEstablishSessionTask::snEstablishSessionTask()
: m_State(STATE_INVALID)
, m_LocalGamerIndex(-1)
, m_NumResults(0)
, m_NetMode(RL_NETMODE_INVALID)
, m_CreateFlags(0)
, m_SlotType(RL_SLOT_INVALID)
{
}

snEstablishSessionTask::~snEstablishSessionTask()
{
    snAssert(this->IsFinished() || !this->IsStarted());
}

bool
snEstablishSessionTask::Configure(snSession* /*ctx*/,
                                const int localGamerIndex,
                                const rlSessionInfo& sessionInfo,
                                const rlNetworkMode netMode,
                                const unsigned createFlags,
                                const rlSlotType slotType)
{
    snAssert(RL_NETMODE_LAN == netMode
            || RL_NETMODE_ONLINE == netMode);

    m_LocalGamerIndex = localGamerIndex;
    m_NetMode = netMode;
	m_CreateFlags = createFlags;
    m_SlotType = slotType;
    m_SessionInfo = sessionInfo;

    return true;
}

void
snEstablishSessionTask::Start()
{
    rlTaskDebug("Establishing session...");

    this->snTask::Start();

    m_State = STATE_QUERY_DETAILS;
}

void
snEstablishSessionTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Establishing session %s", FinishString(finishType));

    if(m_MyStatus.Pending())
    {
        switch(m_State)
        {
        case STATE_QUERYING_DETAILS:
            rlSessionManager::Cancel(&m_MyStatus);
            break;

        case STATE_OPENING_TUNNEL:
            m_Ctx->m_CxnMgr->CancelTunnelRequest(&m_TunnelRqst);
            break;

        case STATE_ACTIVATING_SESSION:
        case STATE_ESTABLISHING_SESSION:
        case STATE_DESTROYING:
            m_Ctx->m_RlSession.Cancel(&m_MyStatus);
            break;

        default:
            rlTaskError("Status pending in invalid state!");
            break;
        }
    }

    if(!FinishSucceeded(finishType))
    {
        const u64 peerId = m_SessionDetail.m_HostPeerInfo.GetPeerId();
        if(m_Ctx->HasPeer(peerId))
        {
            m_Ctx->RemovePeer(peerId);
        }

        m_Ctx->DisableNetwork();
    }

    this->snTask::Finish(finishType, resultCode);
}

void
snEstablishSessionTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    switch(m_State)
    {
    case STATE_QUERY_DETAILS:
        if(!snVerify(0 == m_Ctx->GetGamerCount()))
        {
			rlTaskError("No gamers in session");
            m_State = STATE_DESTROY;
        }
        else if(rlSessionManager::QueryDetail(m_NetMode,
                                            m_Ctx->m_ChannelId,
											0,
											0,
											0,
											true,
                                            &m_SessionInfo,
                                            1,
                                            &m_SessionDetail,
                                            &m_NumResults,
                                            &m_MyStatus))
        {
			rlTaskDebug("Querying session details...");
			m_Ctx->EnableNetwork();
            m_State = STATE_QUERYING_DETAILS;
        }
        else
        {
			rlTaskError("Failed to query session details");
			m_State = STATE_DESTROY;
        }
        break;

    case STATE_QUERYING_DETAILS:
        if(m_MyStatus.Succeeded())
        {
			if(m_NumResults == 1)
			{
				rlTaskDebug("Querying session details succeeded. Activating session...");
				m_SessionDetail.m_SessionConfig.m_CreateFlags |= (m_CreateFlags & RL_SESSION_CREATE_FLAG_LOCAL_MASK);
				m_State = STATE_ACTIVATE_SESSION;
			}
			else
			{
				rlTaskError("Failed to query session details. Results: %d", m_NumResults);
				m_State = STATE_DESTROY;
			}
        }
        else if(!m_MyStatus.Pending())
        {
			rlTaskError("Failed to query session details");
			m_State = STATE_DESTROY;
        }
        break;

    case STATE_ACTIVATE_SESSION:

		if(!m_Ctx->m_RlSession.Activate(m_LocalGamerIndex,
											m_SessionDetail.m_HostPeerInfo.GetPeerId(),
                                            m_SessionInfo,
                                            m_NetMode,
                                            m_SessionDetail.m_SessionConfig.m_CreateFlags | (m_CreateFlags & RL_SESSION_CREATE_FLAG_LOCAL_MASK),
                                            m_SessionDetail.m_SessionConfig.m_PresenceFlags,
                                            m_SessionDetail.m_SessionConfig.m_MaxPublicSlots,
                                            m_SessionDetail.m_SessionConfig.m_MaxPrivateSlots,
                                            m_SlotType,
                                            &m_MyStatus))
        {
            rlTaskError("Failed to activate session");
            m_State = STATE_DESTROY;
        }
        else
        {
			rlTaskDebug("Activating session...");
			m_State = STATE_ACTIVATING_SESSION;
        }
        break;

    case STATE_ACTIVATING_SESSION:
        if(m_MyStatus.Succeeded())
        {
			rlTaskDebug("Activating session succeeded. Establishing session...");
			m_State = STATE_ESTABLISH_SESSION;
        }
        else if(!m_MyStatus.Pending())
        {
			rlTaskError("Failed to activate session");
			m_State = STATE_DESTROY;
        }
        break;

    case STATE_ESTABLISH_SESSION:
        rlTaskDebug("Establishing local instance of session...");
        if(m_Ctx->m_RlSession.Establish(m_SessionDetail.m_SessionConfig, &m_MyStatus))
        {
			rlTaskDebug("Establishing session...");
			m_State = STATE_ESTABLISHING_SESSION;
        }
        else
        {
			rlTaskError("Failed to establish session");
			m_State = STATE_DESTROY;
        }
        break;

    case STATE_ESTABLISHING_SESSION:
        if(m_MyStatus.Succeeded())
        {
			rlTaskDebug("Establishing session succeeded. Opening tunnel...");
			m_State = STATE_OPEN_TUNNEL;
        }
        else if(!m_MyStatus.Pending())
        {
			rlTaskError("Failed to establish session");
			m_State = STATE_DESTROY;
        }
        break;

    case STATE_OPEN_TUNNEL:
        {
            netTunnelType tunnelType;

            if(RL_NETMODE_ONLINE == m_NetMode)
            {
                tunnelType = NET_TUNNELTYPE_ONLINE;
            }
            else
            {
                snAssert(RL_NETMODE_LAN == m_NetMode);
                tunnelType = NET_TUNNELTYPE_LAN;
            }

			snAssert(m_Ctx->GetSessionInfo() == m_SessionInfo);
			
			const netTunnelDesc tunnelDesc(tunnelType, NET_TUNNEL_REASON_ESTABLISH, m_SessionInfo.GetToken().GetValue(), false);

			if(snVerify(m_Ctx->m_CxnMgr->OpenTunnel(m_SessionInfo.GetHostPeerAddress(),
													tunnelDesc,
													&m_TunnelRqst,
													&m_MyStatus)))
            {
				rlTaskDebug("Opening tunnel to host....");
				m_State = STATE_OPENING_TUNNEL;
            }
            else
            {
                rlTaskError("Failed to open tunnel to session host");
                m_State = STATE_DESTROY;
            }
        }
        break;

    case STATE_OPENING_TUNNEL:
        if(m_MyStatus.Succeeded())
        {
			rlTaskDebug("Opening tunnel to succeeded");
			this->Finish(FINISH_SUCCEEDED);
        }
        else if(!m_MyStatus.Pending())
        {
			rlTaskError("Failed to open tunnel to session host");
			m_State = STATE_DESTROY;
        }
        break;

    case STATE_DESTROY:
        if(m_Ctx->m_RlSession.IsActivated())
		{
            if(m_Ctx->m_RlSession.Destroy(&m_MyStatus))
			{
				rlTaskDebug("Destroying session...");
				m_State = STATE_DESTROYING;
			}
			else
			{
				rlTaskError("Failed to destroy session");
				this->Finish(FINISH_FAILED);
			}
		}
		else
		{
			rlTaskError("Session does not need destroyed");
			this->Finish(FINISH_FAILED);
		}
		break;

    case STATE_DESTROYING:
        if(!m_MyStatus.Pending())
        {
			rlTaskDebug("Destroying session %s", m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
			this->Finish(FINISH_FAILED);
        }
        break;
    }
}

const rlPeerInfo&
snEstablishSessionTask::GetHostPeerInfo() const
{
    return m_SessionDetail.m_HostPeerInfo;
}

EndpointId
snEstablishSessionTask::GetHostEndpointId() const
{
    return m_TunnelRqst.GetEndpointId();
}

///////////////////////////////////////////////////////////////////////////////
// snHostSessionTask
///////////////////////////////////////////////////////////////////////////////

snHostSessionTask::snHostSessionTask()
: m_State(STATE_INVALID)
, m_LocalGamerIndex(-1)
, m_SlotType(RL_SLOT_INVALID)
, m_NetMode(RL_NETMODE_INVALID)
, m_MaxPubSlots(0)
, m_MaxPrivSlots(0)
, m_CreateFlags(0)
, m_SizeofOwnerData(0)
{
}

snHostSessionTask::~snHostSessionTask()
{
}

bool
snHostSessionTask::Configure(snSession* /*ctx*/,
                            const int localGamerIndex,
                            const rlNetworkMode netMode,
                            const int maxPubSlots,
                            const int maxPrivSlots,
                            const rlMatchingAttributes& attrs,
                            const unsigned createFlags,
                            const void* ownerData,
                            const unsigned sizeofOwnerData)
{
    m_LocalGamerIndex = localGamerIndex;
    m_NetMode = netMode;

    if(RL_NETMODE_OFFLINE == m_NetMode)
    {
        //In offline sessions, force all slots to be public, since 
        //rlSession::HostOffline() makes no distinction.
        m_MaxPubSlots = maxPubSlots + maxPrivSlots;
        m_MaxPrivSlots = 0;

        snAssert(0 == createFlags);
    }
    else
    {
        m_MaxPubSlots = maxPubSlots;
        m_MaxPrivSlots = maxPrivSlots;
    }

	//Validate session slots
	if(!snVerify((m_MaxPubSlots + m_MaxPrivSlots) <= RL_MAX_GAMERS_PER_SESSION))
	{
		snError("Invalid number of slots - Pub: %d, Priv: %d, Max: %d", m_MaxPubSlots, m_MaxPrivSlots, RL_MAX_GAMERS_PER_SESSION);

		// try removing public slots first
		int overspill = RL_MAX_GAMERS_PER_SESSION - (m_MaxPubSlots + m_MaxPrivSlots);
		m_MaxPubSlots = Max(m_MaxPubSlots - overspill, 0);

		// and then private slots
		if((m_MaxPubSlots + m_MaxPrivSlots) > RL_MAX_GAMERS_PER_SESSION)
		{
			m_MaxPrivSlots = Max(m_MaxPrivSlots - overspill, 0);
		}

		// snAssert that we're under again
		snAssert((m_MaxPubSlots + m_MaxPrivSlots) <= RL_MAX_GAMERS_PER_SESSION);
	}

    //The host always joins in a public slot if available.
    if(m_MaxPubSlots > 0)
    {
        m_SlotType = RL_SLOT_PUBLIC;
    }
    else
    {
        m_SlotType = RL_SLOT_PRIVATE;
    }

    m_Attrs = attrs;
    m_CreateFlags = createFlags;
    m_SizeofOwnerData = sizeofOwnerData;
    if(m_SizeofOwnerData)
    {
        snAssert(sizeofOwnerData <= sizeof(m_OwnerData));
        sysMemCpy(m_OwnerData, ownerData, sizeofOwnerData);
    }
    return true;
}

void
snHostSessionTask::Start()
{
    rlTaskDebug("Creating session as host...");

    this->snTask::Start();

    if(snVerify(!m_Ctx->Exists()))
    {
        m_State = STATE_CREATE;
    }
    else
    {
		rlTaskError("Session already exists!");
        this->Finish(FINISH_FAILED);
    }
}

void
snHostSessionTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Creating session as host %s", FinishString(finishType));

	if(m_Ctx->Exists())
    {
        if(FinishSucceeded(finishType))
        {
			m_Ctx->m_HostPeerId = m_Ctx->m_LocalPeer.GetPeerId();

            if(RL_NETMODE_OFFLINE == m_NetMode)
            {
                m_Ctx->SetAcceptingJoinRequests(false);
            }
            else
            {
                m_Ctx->SetAcceptingJoinRequests(true);
            }

			//Save the pointer before calling finish.
			snSession* ctx = m_Ctx;

			//Call finish before dispatching the event because the event
			//can cause the task to be deleted, and calling Finish() after
			//that would be fatal.
			this->snTask::Finish(finishType, resultCode);

            //Notify the app.
			{
				snEventSessionHosted e;
				ctx->DispatchEvent(&e);
			}

			//Establish here and notify the app
			{
				ctx->m_IsEstablished = true; 
				snEventSessionEstablished e;
				ctx->DispatchEvent(&e);
			}
			
            rlGamerInfo ginfo;
            snVerify(rlPresence::GetGamerInfo(m_LocalGamerIndex, &ginfo));

			// clear local reservation if we have one
			ctx->FillSlotReservation(ginfo.GetGamerHandle());

            snVerify(ctx->AddGamer(ginfo,
                                   m_SlotType,
                                   m_SizeofOwnerData ? m_OwnerData : NULL,
                                   m_SizeofOwnerData));

            snGamer* gamer = ctx->FindGamerById(ginfo.GetGamerId());
            gamer->m_FullyJoined = true;
			gamer->m_JoinedToRline = true;
		}
        else
        {
			if(m_MyStatus.Pending())
			{
				m_Ctx->m_RlSession.Cancel(&m_MyStatus);
			}

            m_Ctx->DisableNetwork();
			this->snTask::Finish(finishType, resultCode);
        }
    }
	else
	{
		this->snTask::Finish(finishType, resultCode);
	}
}

void
snHostSessionTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    do
    {
        switch(m_State)
        {
            case STATE_CREATE:
                m_Ctx->m_LocalPeer.InitLocal(m_LocalGamerIndex);
                m_Ctx->EnableNetwork();
                if(RL_NETMODE_OFFLINE == m_NetMode)
                {
                    if(m_Ctx->m_RlSession.HostOffline(m_LocalGamerIndex,
                                                         m_MaxPubSlots,
                                                         m_Attrs,
                                                         &m_MyStatus))
                    {
						rlTaskDebug("Creating offline session...");
						m_State = STATE_CREATING;
                    }
                    else
                    {
						rlTaskError("Failed to host session!");
						this->Finish(FINISH_FAILED);
                    }
                }
                else
                {
                    if(m_Ctx->m_RlSession.Host(m_LocalGamerIndex,
                                              m_NetMode,
                                              m_MaxPubSlots,
                                              m_MaxPrivSlots,
                                              m_Attrs,
                                              m_CreateFlags,
                                              &m_MyStatus))
                    {
						rlTaskDebug("Creating session...");
						m_State = STATE_CREATING;
                    }
                    else
                    {
						rlTaskError("Failed to host session!");
						this->Finish(FINISH_FAILED);
                    }
                }
                break;

            case STATE_CREATING:
                if(m_MyStatus.Succeeded())
                {
					rlTaskDebug("Created session");
					snTask::RefreshLogContext();
					m_State = STATE_JOIN;
                }
                else if(!m_MyStatus.Pending())
                {
					rlTaskError("Failed to create session!");
					this->Finish(FINISH_FAILED);
                }
                break;

            case STATE_JOIN:
                {
                    rlGamerHandle gamerHandle;
                    if(!snVerify(rlPresence::GetGamerHandle(m_LocalGamerIndex, &gamerHandle)))
                    {
                        rlTaskError("Failed to get gamer info for gamer at index: %d", m_LocalGamerIndex);
                        m_State = STATE_DESTROY;
                    }
                    else if(snVerify(m_Ctx->m_RlSession.Join(&gamerHandle, &m_SlotType, 1, &m_MyStatus)))
                    {
						rlTaskDebug("Joining session...");
						m_State = STATE_JOINING;
                    }
                    else
                    {
						rlTaskError("Failed to join session!");
						m_State = STATE_DESTROY;
                    }
                }
                break;

            case STATE_JOINING:
                if(m_MyStatus.Succeeded())
                {
					rlTaskDebug("Joined session");
					this->Finish(FINISH_SUCCEEDED);
                }
                else if(!m_MyStatus.Pending())
                {
					rlTaskError("Failed to join session!");
					m_State = STATE_DESTROY;
                }
                break;

            case STATE_DESTROY:
                if(m_Ctx->m_RlSession.Destroy(&m_MyStatus))
                {
					rlTaskDebug("Destroying session...");
					m_State = STATE_DESTROYING;
                }
                else
                {
					rlTaskError("Failed to destroy session!");
					this->Finish(FINISH_FAILED);
                }
                break;

            case STATE_DESTROYING:
                if(!m_MyStatus.Pending())
                {
					rlTaskDebug("Destroy session %s", m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
					this->Finish(FINISH_FAILED);
                }
                break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

///////////////////////////////////////////////////////////////////////////////
// snJoinSessionTask
///////////////////////////////////////////////////////////////////////////////

snJoinSessionTask::snJoinSessionTask()
: m_State(STATE_INVALID)
, m_LocalGamerIndex(-1)
, m_NetMode(RL_NETMODE_INVALID)
, m_CreateFlags(0)
, m_SlotType(RL_SLOT_INVALID)
, m_SizeofJoinerData(0)
, m_HostCxnId(-1)
, m_HostEndpointId(NET_INVALID_ENDPOINT_ID)
, m_ResponseCode(SNET_JOIN_DENIED_NOT_JOINABLE)
, m_SizeofResponseData(0)
, m_GroupSize(0)
{
    m_NetEvtDlgt.Bind(this, &snJoinSessionTask::OnNetEvent);
}

snJoinSessionTask::~snJoinSessionTask()
{
}

bool
snJoinSessionTask::Configure(snSession* /*ctx*/,
                            const int localGamerIndex,
                            const rlSessionInfo& sessionInfo,
                            const rlNetworkMode netMode,
                            const rlSlotType slotType,
                            const unsigned createFlags,
                            const void* joinerData,
                            const unsigned sizeofJoinerData,
                            const rlGamerHandle* groupMembers,
                            const unsigned groupSize)
{
    snAssert(RL_NETMODE_LAN == netMode
            || RL_NETMODE_ONLINE == netMode);

    if(!snVerifyf(rlPresence::GetGamerInfo(localGamerIndex, &m_JoinerInfo),
                    "Error retrieving gamer info for gamer:%d", localGamerIndex))
    {
        return false;
    }

    if(!snVerifyf(groupSize <= COUNTOF(m_GroupMembers),
                "Group size is too big - max is %d", COUNTOF(m_GroupMembers))
        || !snVerifyf(groupSize > 0, "Group size is zero"))
    {
        return false;
    }

    m_LocalGamerIndex = localGamerIndex;
    m_SessionInfo = sessionInfo;
    m_NetMode = netMode;
    m_SlotType = slotType;
    m_CreateFlags = createFlags;
    m_SizeofJoinerData = sizeofJoinerData;
    if(m_SizeofJoinerData)
    {
        snAssert(sizeofJoinerData <= sizeof(m_JoinerData));
        sysMemCpy(m_JoinerData, joinerData, sizeofJoinerData);
    }

	m_HostCxnId = -1;
    m_HostEndpointId = NET_INVALID_ENDPOINT_ID;
    m_ResponseCode = SNET_JOIN_DENIED_NOT_JOINABLE;

    bool joinerInGroup = false;
    for(int i = 0; i < (int)groupSize; ++i)
    {
        m_GroupMembers[i] = groupMembers[i];

        if(groupMembers[i] == m_JoinerInfo.GetGamerHandle())
        {
            joinerInGroup = true;
        }
    }

    if(!snVerifyf(joinerInGroup, "Joiner is not in the group"))
    {
        return false;
    }

    m_GroupSize = groupSize;

    return true;
}

void
snJoinSessionTask::Start()
{
    rlTaskDebug("Joining session as guest... Flags: %u", m_CreateFlags);

    this->snTask::Start();

    m_SizeofResponseData = 0;

    if(snVerify(!m_Ctx->Exists()))
    {
        m_State = STATE_ESTABLISH_SESSION;
    }
    else
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snJoinSessionTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Joining session as guest %s", FinishString(finishType));

    if(m_NetEvtDlgt.IsRegistered())
    {
        m_Ctx->m_CxnMgr->RemoveChannelDelegate(&m_NetEvtDlgt);
    }

    if(!FinishSucceeded(finishType))
    {
		if(m_State > STATE_ESTABLISHING_SESSION)
        {
            const rlPeerInfo& hostPeerInfo =
                m_EstablishSessionTask.GetHostPeerInfo();

            if(m_Ctx->HasPeer(hostPeerInfo.GetPeerId()))
            {
                m_Ctx->RemovePeer(hostPeerInfo.GetPeerId());
            }
        }

		m_Ctx->DisableNetwork();

		if(m_EstablishSessionTask.IsActive())
		{
			m_EstablishSessionTask.Finish(finishType);
		}

        if(m_MyStatus.Pending())
        {
            if(STATE_AWAITING_RESPONSE == m_State)
			{
				m_MyStatus.SetFailed();
			}
			else if((STATE_JOIN_LOCALLY == m_State) || (STATE_DESTROYING == m_State))
			{
				m_Ctx->m_RlSession.Cancel(&m_MyStatus);
			}
			else
			{
				snAssertf(0, "Invalid state");
				m_MyStatus.SetFailed();
			}
        }

        //Notify the app.
        if(SNET_JOIN_ACCEPTED == m_ResponseCode)
        {
            //The host accepted our join request, but we were still
            //unable to join, probably due to some error in the
            //platform level (NP, XBL, etc.) join process
            m_ResponseCode = SNET_JOIN_DENIED_NOT_JOINABLE;
            m_SizeofResponseData = 0;
        }

		//Save the pointer before calling finish.
		snSession* ctx = m_Ctx;

		//Call finish before dispatching the event because the event
		//can cause the task to be deleted, and calling Finish() after
		//that would be fatal.
		this->snTask::Finish(finishType, resultCode);

        //Notify the app.
        snEventJoinFailed e(m_SessionInfo,
                            m_ResponseCode,
                            m_SizeofResponseData ? m_ResponseData : NULL,
                            m_SizeofResponseData);

        ctx->DispatchEvent(&e);
    }
    else
    {
		//Save the pointer before calling finish.
		snSession* ctx = m_Ctx;

		//Call finish before dispatching the event because the event
		//can cause the task to be deleted, and calling Finish() after
		//that would be fatal.
		this->snTask::Finish(finishType, resultCode);

        //Notify the app.
        snEventSessionJoined e(m_EstablishSessionTask.GetHostEndpointId(),
                                m_SizeofResponseData ? m_ResponseData : NULL,
                                m_SizeofResponseData);
        ctx->DispatchEvent(&e);

        snVerify(ctx->AddGamer(m_JoinerInfo,
                               m_SlotType,
                               m_SizeofJoinerData ? m_JoinerData : NULL,
                               m_SizeofJoinerData));

        snGamer* gamer = ctx->FindGamerById(m_JoinerInfo.GetGamerId());

        if(snVerify(gamer))
        {
            snAssert(!gamer->m_FullyJoined);
            gamer->m_FullyJoined = true;
			gamer->m_JoinedToRline = true;
		}
    }
}

void
snJoinSessionTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    do
    {
        switch(m_State)
        {
            case STATE_ESTABLISH_SESSION:
                m_Ctx->m_LocalPeer.InitLocal(m_LocalGamerIndex);
                m_Ctx->EnableNetwork();
                if(snVerify(rlTaskBase::Configure(&m_EstablishSessionTask,
                                                    m_Ctx,
                                                    m_LocalGamerIndex,
                                                    m_SessionInfo,
                                                    m_NetMode,
                                                    m_CreateFlags,
                                                    m_SlotType,
                                                    &m_MyStatus)))
                {
					rlTaskDebug("Establishing session...");
                    m_EstablishSessionTask.Start();
                    m_State = STATE_ESTABLISHING_SESSION;
                }
                else
                {
                    rlTaskError("Error configuring EstablishSessionTask");
                    this->Finish(FINISH_FAILED);
                }
                break;

            case STATE_ESTABLISHING_SESSION:
                if(m_MyStatus.Pending())
                {
                    m_EstablishSessionTask.Update(timeStep);
                }
                else if(m_MyStatus.Succeeded())
                {
					rlTaskDebug("Established session. Requesting join...");
					m_State = STATE_REQUEST_JOIN;
                }
                else
                {
					rlTaskError("Establishing session failed");
					m_ResponseCode = SNET_JOIN_DENIED_FAILED_TO_ESTABLISH;
					this->Finish(FINISH_FAILED);
                }
                break;

            case STATE_REQUEST_JOIN:
                {
                    //Send a join request to the host.
                    snMsgJoinRequest joinRqst;

                    snAssert(m_GroupSize > 0);

                    joinRqst.Reset(m_Ctx->GetSessionId(),
                                    m_JoinerInfo,
                                    netRelay::GetMyRelayAddress(),
                                    m_SlotType,
                                    m_JoinerData,
                                    m_SizeofJoinerData,
                                    m_GroupMembers,
                                    m_GroupSize);

                    u8 msgBuf[sizeof(snMsgJoinRequest)];
                    unsigned sizeofMsg;
                    const rlPeerInfo& hostPeerInfo = m_EstablishSessionTask.GetHostPeerInfo();

                    m_Ctx->m_CxnMgr->AddChannelDelegate(&m_NetEvtDlgt, m_Ctx->m_ChannelId);

                    if(snVerify(joinRqst.Export(msgBuf, sizeof(msgBuf), &sizeofMsg))
                        && snVerify(m_Ctx->ConnectToPeer(hostPeerInfo,
															 -1,
                                                             m_EstablishSessionTask.GetHostEndpointId(),
															 msgBuf,
                                                             sizeofMsg,
                                                             NULL)))
                    {
						rlTaskDebug("Requesting to join the session...");

						const snPeer* peer = m_Ctx->FindPeerById(hostPeerInfo.GetPeerId());
                        snAssert(peer);
						m_HostCxnId = peer->GetCxnId();
                        m_HostEndpointId = peer->GetEndpointId();

                        m_MyStatus.SetPending();
                        m_State = STATE_AWAITING_RESPONSE;
                    }
                    else
                    {
						rlTaskError("Failed to request to join");
						m_State = STATE_DESTROY;
                    }
                }
                break;

            case STATE_AWAITING_RESPONSE:
                if(m_MyStatus.Succeeded())
                {
					rlTaskDebug("Request to join succeeded. Joining locally...");
                    m_State = STATE_JOIN_LOCALLY;
                }
                else if(!m_MyStatus.Pending())
                {
					rlTaskError("Request to join failed");
					m_State = STATE_DESTROY;
                }
                break;

            case STATE_JOIN_LOCALLY:
                
                if(snVerify(m_Ctx->m_RlSession.Join(&m_JoinerInfo.GetGamerHandle(),
                                                        &m_SlotType,
                                                        1,
                                                        &m_MyStatus)))
                {
					rlTaskDebug("Joining session locally...");
					m_State = STATE_JOINING_LOCALLY;
                }
                else
                {
					rlTaskError("Failed to join locally");
					m_State = STATE_DESTROY;
                }
                break;

            case STATE_JOINING_LOCALLY:
                if(m_MyStatus.Succeeded())
                {
					rlTaskDebug("Joined session locally");
					this->Finish(FINISH_SUCCEEDED);
                }
                else if(!m_MyStatus.Pending())
                {
					rlTaskError("Failed to join locally");
					m_State = STATE_DESTROY;
                }
                break;

            case STATE_DESTROY:
                if(m_Ctx->m_RlSession.Destroy(&m_MyStatus))
                {
					rlTaskDebug("Destroying session...");
					m_State = STATE_DESTROYING;
                }
                else
                {
					rlTaskError("Failed to destroy session");
					this->Finish(FINISH_FAILED);
                }
                break;

            case STATE_DESTROYING:
                if(!m_MyStatus.Pending())
                {
					rlTaskDebug("Destroying session %s", m_MyStatus.Succeeded() ? "Succeeded" : "Failed");
					this->Finish(FINISH_FAILED);
                }
                break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

void
snJoinSessionTask::OnNetEvent(netConnectionManager* /*cxnMgr*/,
                            const netEvent* evt)
{
    unsigned msgId;

    if(NET_EVENT_FRAME_RECEIVED == evt->GetId())
    {
        if(netMessage::GetId(&msgId, evt->m_FrameReceived->m_Payload, evt->m_FrameReceived->m_SizeofPayload))
		{
            if(snMsgJoinResponse::MSG_ID() == msgId)
			{
				if(evt->m_CxnId == m_HostCxnId)
				{
					m_Ctx->m_CxnMgr->RemoveChannelDelegate(&m_NetEvtDlgt);

					snMsgJoinResponse joinResp;
					bool success = false;

					if(!snVerify(joinResp.Import(evt->m_FrameReceived->m_Payload,
						evt->m_FrameReceived->m_SizeofPayload)))
					{
						rlTaskError("Failed to import %s", joinResp.GetMsgName());
					}
					else
					{
						m_ResponseCode = snJoinResponseCode(joinResp.m_ResponseCode);

						rlTaskDebug("Gamer:\"%s\" was %s into the session",
							m_JoinerInfo.GetName(),
							(SNET_JOIN_ACCEPTED == m_ResponseCode)
							? "accepted"
							: "not accepted");

						m_SizeofResponseData = joinResp.m_SizeofResponseData;
						if(m_SizeofResponseData
							&& snVerify(m_SizeofResponseData <= sizeof(m_ResponseData)))
						{
							sysMemCpy(m_ResponseData, joinResp.m_ResponseData, joinResp.m_SizeofResponseData);
						}

						if(SNET_JOIN_ACCEPTED == m_ResponseCode)
						{
							m_SlotType = joinResp.m_SlotType;

							m_SizeofJoinerData = joinResp.m_SizeofGamerData;
							if(m_SizeofJoinerData
								&& snVerify(m_SizeofJoinerData <= sizeof(m_JoinerData)))
							{
								sysMemCpy(m_JoinerData, joinResp.m_GamerData, joinResp.m_SizeofGamerData);
							}
						}

						success = (SNET_JOIN_ACCEPTED == m_ResponseCode);
					}

					success ? m_MyStatus.SetSucceeded() : m_MyStatus.SetFailed();
				}
				else
				{
					rlTaskError("Received snMsgJoinResponse from non-host gamer! CxnId: %08x, HostCxnId: %08x", evt->m_CxnId, m_HostCxnId);
				}
			}
        }
    }
    else if(NET_EVENT_CONNECTION_ERROR == evt->GetId()
            && evt->m_CxnId == m_HostCxnId)
    {
        m_Ctx->m_CxnMgr->RemoveChannelDelegate(&m_NetEvtDlgt);

        snMsgJoinResponse joinResp;

        if(joinResp.Import(evt->m_CxnError->m_Data,
                            evt->m_CxnError->m_SizeofData))
        {
            m_ResponseCode = snJoinResponseCode(joinResp.m_ResponseCode);
            snAssert(SNET_JOIN_ACCEPTED != m_ResponseCode);

            rlTaskDebug("Gamer:\"%s\" was not accepted into the session",
                        m_JoinerInfo.GetName());

            m_SizeofResponseData = joinResp.m_SizeofResponseData;
            if(m_SizeofResponseData
                && snVerify(m_SizeofResponseData <= sizeof(m_ResponseData)))
            {
                sysMemCpy(m_ResponseData, joinResp.m_ResponseData, joinResp.m_SizeofResponseData);
            }
        }
        else
        {
            rlTaskError("Failed to import %s", joinResp.GetMsgName());
        }

         m_MyStatus.SetFailed();
    }
}

///////////////////////////////////////////////////////////////////////////////
// snJoinRemoteSessionBotTask
///////////////////////////////////////////////////////////////////////////////

snJoinRemoteSessionBotTask::snJoinRemoteSessionBotTask()
: m_State(STATE_INVALID)
, m_HostCxnId(-1)
, m_HostEndpointId(NET_INVALID_ENDPOINT_ID)
, m_SizeofJoinerData(0)
, m_SizeofResponseData(0)
, m_ResponseCode(SNET_JOIN_DENIED_NOT_JOINABLE)
, m_SlotType(RL_SLOT_INVALID)
{
    m_NetEvtDlgt.Bind(this, &snJoinRemoteSessionBotTask::OnNetEvent);
}

snJoinRemoteSessionBotTask::~snJoinRemoteSessionBotTask()
{
}

bool
snJoinRemoteSessionBotTask::Configure(snSession* /*ctx*/,
                                      const rlGamerInfo& joiner,
                                      const rlSessionInfo& sessionInfo,
                                      const rlNetworkMode NET_ASSERTS_ONLY(netMode),
                                      const rlSlotType slotType,
                                      const void* joinerData,
                                      const unsigned sizeofJoinerData)
{
    snAssert(RL_NETMODE_LAN == netMode
            || RL_NETMODE_ONLINE == netMode);

    m_Joiner = joiner;
    m_SessionInfo = sessionInfo;
    m_SlotType = slotType;
    m_SizeofJoinerData = sizeofJoinerData;
    if(m_SizeofJoinerData)
    {
        snAssert(sizeofJoinerData <= sizeof(m_JoinerData));
        sysMemCpy(m_JoinerData, joinerData, sizeofJoinerData);
    }

    m_HostCxnId = -1;
    m_HostEndpointId = NET_INVALID_ENDPOINT_ID;
    m_ResponseCode = SNET_JOIN_DENIED_NOT_JOINABLE;

    return true;
}

void
snJoinRemoteSessionBotTask::Start()
{
    rlTaskDebug("Joining a remote network bot into the session as a guest...");

    this->snTask::Start();

    m_SizeofResponseData = 0;

    if(snVerify(m_Ctx->Exists()))
    {
        m_State = STATE_REQUEST_JOIN;
    }
    else
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snJoinRemoteSessionBotTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Joining a remote network bot into the session as a guest %s", FinishString(finishType));

    if(m_NetEvtDlgt.IsRegistered())
    {
        m_Ctx->m_CxnMgr->RemoveChannelDelegate(&m_NetEvtDlgt);
    }

    if(!FinishSucceeded(finishType))
    {
        if(m_MyStatus.Pending())
        {
			if(STATE_AWAITING_RESPONSE == m_State)
			{
				m_MyStatus.SetFailed();
			}
			else if((STATE_JOIN_LOCALLY == m_State) || (STATE_DESTROYING == m_State))
			{
				m_Ctx->m_RlSession.Cancel(&m_MyStatus);
			}
			else
			{
				snAssertf(0, "Invalid state");
				m_MyStatus.SetFailed();
			}
        }

        //Notify the app.
        if(SNET_JOIN_ACCEPTED == m_ResponseCode)
        {
            //The host accepted our join request, but we were still
            //unable to join, probably due to some error in the
            //platform level (NP, XBL, etc.) join process
            m_ResponseCode = SNET_JOIN_DENIED_NOT_JOINABLE;
            m_SizeofResponseData = 0;
        }

        snEventSessionBotJoinFailed e(m_SessionInfo,
                                      m_ResponseCode,
                                      m_SizeofResponseData ? m_ResponseData : NULL,
                                      m_SizeofResponseData);
        m_Ctx->DispatchEvent(&e);
    }
    else
    {
        //Notify the app.
        snEventSessionBotJoined e;
        m_Ctx->DispatchEvent(&e);

        snVerify(m_Ctx->AddGamer(m_Joiner,
                                    m_SlotType,
                                    m_SizeofJoinerData ? m_JoinerData : NULL,
                                    m_SizeofJoinerData));
    }
    this->snTask::Finish(finishType, resultCode);
}

void
snJoinRemoteSessionBotTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    do
    {
        switch(m_State)
        {
            case STATE_REQUEST_JOIN:
                {
                    rlTaskDebug("Requesting to join network bot into the session...");

                    //Send a join request to the host.
                    snMsgJoinRequest joinRqst;
                    joinRqst.Reset(m_Ctx->GetSessionId(),
                                    m_Joiner,
                                    netRelay::GetMyRelayAddress(),
                                    m_SlotType,
                                    m_JoinerData,
                                    m_SizeofJoinerData,
                                    &m_Joiner.GetGamerHandle(),
                                    1);

                    u64 peerId;
                    if(snVerify(m_Ctx->GetHostPeerId(&peerId)))
                    {
                        m_Ctx->SendMsgReliable(peerId, joinRqst, 0);

                        m_Ctx->m_CxnMgr->AddChannelDelegate(&m_NetEvtDlgt, m_Ctx->m_ChannelId);

                        const snPeer* peer = m_Ctx->FindPeerById(peerId);
                        snAssert(peer);
                        m_HostCxnId = peer->GetCxnId();
                        m_HostEndpointId = peer->GetEndpointId();

                        m_MyStatus.SetPending();
                        m_State = STATE_AWAITING_RESPONSE;
                    }
                }
                break;

            case STATE_AWAITING_RESPONSE:
                if(m_MyStatus.Succeeded())
                {
                    m_State = STATE_JOIN_LOCALLY;
                }
                else if(!m_MyStatus.Pending())
                {
                    m_State = STATE_DESTROY;
                }
                break;

            case STATE_JOIN_LOCALLY:
                rlTaskDebug("Joining session locally...");

                if(snVerify(m_Ctx->m_RlSession.Join(&m_Joiner.GetGamerHandle(),
                                                        &m_SlotType,
                                                        1,
                                                        &m_MyStatus)))
                {
                    m_State = STATE_JOINING_LOCALLY;
                }
                else
                {
                    m_State = STATE_DESTROY;
                }
                break;

            case STATE_JOINING_LOCALLY:
                if(m_MyStatus.Succeeded())
                {
                    this->Finish(FINISH_SUCCEEDED);
                }
                else if(!m_MyStatus.Pending())
                {
                    m_State = STATE_DESTROY;
                }
                break;

            case STATE_DESTROY:
                if(m_Ctx->m_RlSession.Destroy(&m_MyStatus))
                {
                    m_State = STATE_DESTROYING;
                }
                else
                {
                    this->Finish(FINISH_FAILED);
                }
                break;

            case STATE_DESTROYING:
                if(!m_MyStatus.Pending())
                {
                    this->Finish(FINISH_FAILED);
                }
                break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

void
snJoinRemoteSessionBotTask::OnNetEvent(netConnectionManager* /*cxnMgr*/,
                            const netEvent* evt)
{
    unsigned msgId;

    if(NET_EVENT_FRAME_RECEIVED == evt->GetId())
    {
        if(evt->m_CxnId == m_HostCxnId
            && netMessage::GetId(&msgId,
                                  evt->m_FrameReceived->m_Payload,
                                  evt->m_FrameReceived->m_SizeofPayload)
            && snMsgJoinResponse::MSG_ID() == msgId)
        {
            m_Ctx->m_CxnMgr->RemoveChannelDelegate(&m_NetEvtDlgt);

            snMsgJoinResponse joinResp;
            bool success = false;

            if(!snVerify(joinResp.Import(evt->m_FrameReceived->m_Payload,
                                            evt->m_FrameReceived->m_SizeofPayload)))
            {
                rlTaskError("Failed to import %s", joinResp.GetMsgName());
            }
            else
            {
                m_ResponseCode = snJoinResponseCode(joinResp.m_ResponseCode);

                rlTaskDebug("Gamer:\"%s\" was %s into the session",
                            m_Joiner.GetName(),
                            (SNET_JOIN_ACCEPTED == m_ResponseCode)
                                ? "accepted"
                                : "not accepted");

                m_SizeofResponseData = joinResp.m_SizeofResponseData;
                if(m_SizeofResponseData
                    && snVerify(m_SizeofResponseData <= sizeof(m_ResponseData)))
                {
                    sysMemCpy(m_ResponseData, joinResp.m_ResponseData, joinResp.m_SizeofResponseData);
                }

                if(SNET_JOIN_ACCEPTED == m_ResponseCode)
                {
                    m_SlotType = joinResp.m_SlotType;

                    m_SizeofJoinerData = joinResp.m_SizeofGamerData;
                    if(m_SizeofJoinerData
                        && snVerify(m_SizeofJoinerData <= sizeof(m_JoinerData)))
                    {
                        sysMemCpy(m_JoinerData, joinResp.m_GamerData, joinResp.m_SizeofGamerData);
                    }
                }

                success = (SNET_JOIN_ACCEPTED == m_ResponseCode);
            }

            success ? m_MyStatus.SetSucceeded() : m_MyStatus.SetFailed();
        }
    }
    else if(NET_EVENT_CONNECTION_ERROR == evt->GetId()
            && evt->m_CxnId == m_HostCxnId)
    {
        m_Ctx->m_CxnMgr->RemoveChannelDelegate(&m_NetEvtDlgt);

        snMsgJoinResponse joinResp;

        if(joinResp.Import(evt->m_CxnError->m_Data,
                            evt->m_CxnError->m_SizeofData))
        {
            m_ResponseCode = snJoinResponseCode(joinResp.m_ResponseCode);
            snAssert(SNET_JOIN_ACCEPTED != m_ResponseCode);

            rlTaskDebug("Gamer:\"%s\" was not accepted into the session",
                        m_Joiner.GetName());

            m_SizeofResponseData = joinResp.m_SizeofResponseData;
            if(m_SizeofResponseData
                && snVerify(m_SizeofResponseData <= sizeof(m_ResponseData)))
            {
                sysMemCpy(m_ResponseData, joinResp.m_ResponseData, joinResp.m_SizeofResponseData);
            }
        }
        else
        {
            rlTaskError("Failed to import %s", joinResp.GetMsgName());
        }

         m_MyStatus.SetFailed();
    }
}

///////////////////////////////////////////////////////////////////////////////
// snJoinLocalSessionBotTask
///////////////////////////////////////////////////////////////////////////////

snJoinLocalSessionBotTask::snJoinLocalSessionBotTask()
: m_State(STATE_INVALID)
, m_SlotType(RL_SLOT_INVALID)
{
}

snJoinLocalSessionBotTask::~snJoinLocalSessionBotTask()
{
}

bool
snJoinLocalSessionBotTask::Configure(snSession* /*ctx*/,
                            const rlGamerInfo& joiner,
                            const rlSessionInfo& sessionInfo,
                            const rlSlotType slotType,
                            const void* joinerData,
                            const unsigned sizeofJoinerData)
{
    m_Joiner = joiner;
    m_SessionInfo = sessionInfo;
    m_SlotType = slotType;
    m_SizeofJoinerData = sizeofJoinerData;
    if(m_SizeofJoinerData)
    {
        snAssert(sizeofJoinerData <= sizeof(m_JoinerData));
        sysMemCpy(m_JoinerData, joinerData, sizeofJoinerData);
    }

    return true;
}

void
snJoinLocalSessionBotTask::Start()
{
    rlTaskDebug2("Joining a network bot into the session as a guest...");

    this->snTask::Start();

    m_State = STATE_JOIN_LOCALLY;
}

void
snJoinLocalSessionBotTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug2("Joining a network bot into the session as a guest %s", FinishString(finishType));

    if(!FinishSucceeded(finishType))
    {
		if(m_MyStatus.Pending())
		{
			if((STATE_JOIN_LOCALLY == m_State) || (STATE_DESTROYING == m_State))
			{
				m_Ctx->m_RlSession.Cancel(&m_MyStatus);
			}
			else
			{
				snAssertf(0, "Invalid state");
				m_MyStatus.SetFailed();
			}
		}
        //Notify the app.
        snEventJoinFailed e(m_SessionInfo,
                            SNET_JOIN_DENIED_NOT_JOINABLE,
                            NULL,
                            0);
		m_Ctx->DispatchEvent(&e);
    }
    else
    {
        //Notify the app.
        snEventSessionBotJoined e;
        m_Ctx->DispatchEvent(&e);

        snVerify(m_Ctx->AddGamer(m_Joiner,
                              m_SlotType,
                              m_JoinerData,
                              m_SizeofJoinerData));

        snGamer* gamer = m_Ctx->FindGamerById(m_Joiner.GetGamerId());

        if(snVerify(gamer))
        {
            snAssert(!gamer->m_FullyJoined);
            gamer->m_FullyJoined = true;
			gamer->m_JoinedToRline = true;
		}

        m_Ctx->InformPeersOfJoiner(m_Joiner);
    }
    this->snTask::Finish(finishType, resultCode);
}

void
snJoinLocalSessionBotTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    do
    {
        switch(m_State)
        {
            case STATE_JOIN_LOCALLY:
                rlTaskDebug2("Joining session locally...");

                if(snVerify(m_Ctx->m_RlSession.Join(&m_Joiner.GetGamerHandle(),
                                                        &m_SlotType,
                                                        1,
                                                        &m_MyStatus)))
                {
                    m_State = STATE_JOINING_LOCALLY;
                }
                else
                {
                    m_State = STATE_DESTROY;
                }
                break;

            case STATE_JOINING_LOCALLY:
                if(m_MyStatus.Succeeded())
                {
                    this->Finish(FINISH_SUCCEEDED);
                }
                else if(!m_MyStatus.Pending())
                {
                    m_State = STATE_DESTROY;
                }
                break;

            case STATE_DESTROY:
                if(m_Ctx->m_RlSession.Destroy(&m_MyStatus))
                {
                    m_State = STATE_DESTROYING;
                }
                else
                {
                    this->Finish(FINISH_FAILED);
                }
                break;

            case STATE_DESTROYING:
                if(!m_MyStatus.Pending())
                {
                    this->Finish(FINISH_FAILED);
                }
                break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

///////////////////////////////////////////////////////////////////////////////
// snMigrateSessionTask
///////////////////////////////////////////////////////////////////////////////
unsigned snMigrateSessionTask::sm_TimeoutMs = snMigrateSessionTask::DEFAULT_TIMEOUT_MS;
unsigned snMigrateSessionTask::sm_MigrateRequestInterval = snMigrateSessionTask::DEFAULT_REQUEST_INTERVAL;
unsigned snMigrateSessionTask::sm_MigrateRequestNumAttempts = (snMigrateSessionTask::sm_TimeoutMs / snMigrateSessionTask::sm_MigrateRequestInterval) + 
															  (((snMigrateSessionTask::sm_TimeoutMs % snMigrateSessionTask::sm_MigrateRequestInterval) == 0) ? 0 : 1);

snMigrateSessionTask::snMigrateSessionTask()
    : m_State(STATE_INVALID)
    , m_NumCandidates(0)
    , m_CurCandidate(-1)
    , m_NumRequestsSent(0)
    , m_NumPendingResponses(0)
    , m_NumSucceeded(0)
    , m_WaitTimeout(0)
{
    m_RqstHandler.Bind(this, &snMigrateSessionTask::OnRequest);

    for(int i = 0; i < RL_MAX_GAMERS_PER_SESSION; ++i)
    {
        m_RespHandlers[i].Bind(this, &snMigrateSessionTask::OnResponse);
    }
}

bool
snMigrateSessionTask::Configure(snSession* /*ctx*/)
{
    m_CurCandidate = -1;

    return true;
}

void
snMigrateSessionTask::GetCandidates()
{
	// get candidates from session
	rlPeerInfo hostCandidates[RL_MAX_GAMERS_PER_SESSION];
	unsigned numHostCandidates = m_Ctx->GetHostCandidates(hostCandidates);

	//Make sure each candidate is viable and that
	//we don't have too many candidates...

	rlPeerInfo actualCandidates[snMigrateSessionTask::MAX_CANDIDATES];
	unsigned actualCount = 0;

	for(int i = 0; i < (int) numHostCandidates && actualCount < snMigrateSessionTask::MAX_CANDIDATES; ++i)
	{
		const snPeer* peer = m_Ctx->FindPeerById(hostCandidates[i].GetPeerId());

		if(peer && (peer->GetCxnId() >= 0 || peer->IsLocal()))
		{
			actualCandidates[actualCount] = hostCandidates[i];
			++actualCount;
		}
	}

	if(0 == actualCount)
	{
		//If there are no viable candidates then add ourselves.
		actualCandidates[0] = m_Ctx->GetLocalPeerInfo();
		actualCount = 1;
	}

	snAssert(actualCount <= MAX_CANDIDATES);

	m_NumCandidates = 0;
	m_NumRequestsSent = m_NumPendingResponses = m_NumSucceeded = 0;

	bool amICandidate = false;

	const rlPeerInfo& myPeerInfo = m_Ctx->GetLocalPeerInfo();

	for(unsigned i = 0; i < actualCount && i < MAX_CANDIDATES; ++i)
	{
		snPeer* peer = m_Ctx->FindPeerById(actualCandidates[i].GetPeerId());
		if(peer && peer->IsRemote() && !peer->AllowMigration())
		{
			rlTaskDebug("Not adding candidate :: PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "] - Migration Blocked", peer->GetPeerId(), NET_ADDR_FOR_PRINTF(m_Ctx->GetCxnMgr()->GetAddress(peer->GetEndpointId())));
			continue;
		}

#if !__NO_OUTPUT
		const bool isLocal = peer->GetPeerId() == myPeerInfo.GetPeerId();

		if(isLocal)
		{
			rlTaskDebug("Adding candidate :: PeerID: 0x%" I64FMT "x [Local]", peer->GetPeerId());
		}
		else
		{
			rlTaskDebug("Adding candidate :: PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "]", peer->GetPeerId(), NET_ADDR_FOR_PRINTF(m_Ctx->GetCxnMgr()->GetAddress(peer->GetEndpointId())));
		}
#endif

		m_Candidates[m_NumCandidates] = actualCandidates[i];
		m_CandidateRemoved[m_NumCandidates] = false;

		if(!amICandidate
			&& actualCandidates[i].GetPeerId() == myPeerInfo.GetPeerId())
		{
			amICandidate = true;
		}

		++m_NumCandidates;
	}

	//Make sure we're always one of the candidates
	if(!amICandidate)
	{
		//Make sure there's room to add ourselves.
		CompileTimeAssert(COUNTOF(m_Candidates) > MAX_CANDIDATES);

		rlTaskDebug("Local not found in candidates :: Adding as post");
		m_Candidates[m_NumCandidates] = myPeerInfo;
		++m_NumCandidates;
	}

	rlTaskDebug("Have %d candidates", m_NumCandidates);
}

void
snMigrateSessionTask::Start()
{
    this->snTask::Start();

    rtry
    {
        rcheck(m_Ctx->Exists(), catchall,);

        rlTaskDebug("Migrating session:0x%016" I64FMT "x...", m_Ctx->GetSessionId());

		GetCandidates();

        m_WaitTimeout = 0;
        m_CurCandidate = -1;

        m_State = STATE_MIGRATE;
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snMigrateSessionTask::Finish(const FinishType finishType, const int resultCode)
{
    m_Ctx->m_Transactor.RemoveRequestHandler(&m_RqstHandler);

    for(int i = 0; i < m_NumRequestsSent; ++i)
    {
        if(m_RespHandlers[i].Pending())
        {
            m_RespHandlers[i].Cancel();
        }
    }

    if(m_Ctx->Exists())
    {
        rlTaskDebug("Migrating session:0x%016" I64FMT "x %s",
                m_Ctx->GetSessionId(),
                FinishString(finishType));

        if(m_Ctx->IsHost())
        {
            snAssert(RL_INVALID_PEER_ID != m_Ctx->m_HostPeerId);

            //Sanity check - we should have disabled this
            //when the host migration started (in snSession.cpp).
            snAssert(!m_Ctx->IsAcceptingJoinRequests());
            m_Ctx->SetAcceptingJoinRequests(true);
        }
        else
        {
            //The host id will be invalid if the native host migration failed,
            //which will occur if the local gamer signs out or if we
            //lose our connection to the backend service (XBL).
            snAssert(RL_INVALID_PEER_ID != m_Ctx->m_HostPeerId || !FinishSucceeded(finishType));
        }
    }

    if(m_MyStatus.Pending())
    {
        snAssert(!FinishSucceeded(finishType));

		if(finishType == FINISH_CANCELED)
		{
            if(m_State == STATE_MIGRATING)
            {
                m_Ctx->m_RlSession.Cancel(&m_MyStatus);
            }
            else
            {
                m_MyStatus.SetCanceled();
            }
		}
		else
		{
			snAssert(STATE_AWAITING_MIGRATE_RESPONSES == m_State
					|| STATE_WAITING_FOR_NEW_HOST == m_State);

			//If the status object is left in the pending state when this object
			//is destroyed it will snAssert.  This only occurs if the task is
			//destroyed before it's finished.
			m_MyStatus.SetFailed();
		}
    }

    snAssert(m_Ctx->m_Migrating || !FinishSucceeded(finishType));
    m_Ctx->m_Migrating = false;

    //Save the pointer before calling finish.
    snSession* ctx = m_Ctx;

    //Call finish before dispatching the event because the event
    //can cause the task to be deleted, and calling Finish() after
    //that would be fatal.
    this->snTask::Finish(finishType, resultCode);

    if(FinishSucceeded(finishType))
    {
        snEventSessionMigrateEnd e(m_Candidates[m_CurCandidate], true, m_NumCandidates, m_CurCandidate);
        ctx->DispatchEvent(&e);
    }
    else
    {
        snEventSessionMigrateEnd e(rlPeerInfo(), false, m_NumCandidates, m_CurCandidate);
        ctx->DispatchEvent(&e);
    }
}

void
snMigrateSessionTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    do
    {
        switch(m_State)
        {
            case STATE_MIGRATE:
                if(!this->Commence() && m_CurCandidate >= m_NumCandidates)
                {
                    this->Finish(FINISH_FAILED);
                }
                break;

            case STATE_MIGRATING:
                if(m_MyStatus.Succeeded())
                {
                    if(!m_Ctx->IsHost())
                    {
                        //Make sure we're still connected to the new host
                        if(m_Ctx->IsConnectedToPeer(m_Candidates[m_CurCandidate].GetPeerId()) && !m_CandidateRemoved[m_CurCandidate])
                        {
                            this->Finish(FINISH_SUCCEEDED);
                        }
                        else
                        {
                            //Not connected - try again.
                            m_State = STATE_MIGRATE;
                        }
                    }
                    else if(m_Ctx->GetPeerCount())
                    {
                        //We're the new host.
                        //Tell other peers to migrate to us.
                        m_State = STATE_AWAITING_MIGRATE_RESPONSES;
                        this->SendMigrateCommands();
                    }
                    else
                    {
                        this->Finish(FINISH_SUCCEEDED);
                    }
                }
                else if(!m_MyStatus.Pending())
                {
					//Try again.
					m_State = STATE_MIGRATE;
                }
                break;

            case STATE_AWAITING_MIGRATE_RESPONSES:
                if(!m_MyStatus.Pending())
                {
                    snAssert(!m_NumPendingResponses);

                    //Drop gamers that have not responded.
                    this->DropDeadbeats();
                    this->Finish(FINISH_SUCCEEDED);
                }
                break;

            case STATE_WAITING_FOR_NEW_HOST:
                if(m_MyStatus.Succeeded())
                {
                    if(this->Migrate(m_Candidates[m_CurCandidate],
                                    &m_NewSessionInfo))
                    {
                        m_State = STATE_MIGRATING;
                    }
                    else
                    {
                        this->Finish(FINISH_FAILED);
                    }
                }
                else if(m_MyStatus.Pending())
                {
                    //Are we still connected to the candidate?
                    if(!m_Ctx->IsConnectedToPeer(m_Candidates[m_CurCandidate].GetPeerId()) || m_CandidateRemoved[m_CurCandidate])
                    {
                        rlTaskDebug("Dropped connection to host candidate");

                        m_MyStatus.SetCanceled();

                        m_State = STATE_MIGRATE;
                    }
                    else
                    {
                        //Did we time out?
                        m_WaitTimeout -= timeStep;
                        if(m_WaitTimeout <= 0)
                        {
                            rlTaskDebug("Timed out waiting for host candidate");
                            m_MyStatus.SetCanceled();
                            m_State = STATE_MIGRATE;
                        }
                    }
                }
                else
                {
                    this->Finish(FINISH_FAILED);
                }
                break;
        }
    }
    while(!m_MyStatus.Pending() && this->IsActive());
}

//private:

bool
snMigrateSessionTask::Commence()
{
    bool success = false;

    m_Ctx->m_HostPeerId = RL_INVALID_PEER_ID;

    const int oldCandidate = m_CurCandidate;
    ++m_CurCandidate;

    rtry
    {
        rcheck(m_Ctx->Exists(), catchall,);

        if(oldCandidate >= 0)
        {
            rlTaskDebug("Host migration failed - trying again...");

            const rlPeerInfo& candidate = m_Candidates[oldCandidate];

            if(!candidate.IsLocal())
            {
                //Disconnect the peer that failed to become the host.
                snPeer* peer = m_Ctx->FindPeerById(candidate.GetPeerId());
                if(peer && peer->GetCxnId() >= 0)
                {
                    m_Ctx->DisconnectPeer(peer);
                }
            }
        }

        if(m_CurCandidate >= m_NumCandidates)
        {
            rlTaskDebug("Can't commence host migration - no more candidates.");
        }
        else
        {
            rlTaskDebug("Commencing host migration...");

            m_Ctx->m_Transactor.RemoveRequestHandler(&m_RqstHandler);

            const rlPeerInfo& newHost = m_Candidates[m_CurCandidate];

            if(newHost.IsLocal())
            {
                //We're the new host.

                rlTaskDebug("Migrating session:0x%016" I64FMT "x to ourselves...",
                            m_Ctx->GetSessionId());

                if(m_Ctx->IsHost())
                {
                    //If we're already the host then just finish.
                    //This can occur if we were the host and were asked to
                    //grant session ownership to another peer, which then
                    //failed.
                    m_MyStatus.ForceSucceeded();
                }
                else
                {
                    rverify(this->Migrate(newHost, NULL),
                            catchall,
                            rlTaskError("Error migrating host"));
                }

                m_State = STATE_MIGRATING;
            }
            else
            {
                //Check that we're still connected to the new host.
                rcheck(m_Ctx->IsConnectedToPeer(newHost.GetPeerId()),
                        catchall,
                        rlTaskDebug("We are no longer connected to the host candidate"));
				
				//Check that we're still connected to the new host.
				rcheck(!m_CandidateRemoved[m_CurCandidate],
						catchall,
						rlTaskDebug("Candidate removed"));

                //Wait for the migrate command from the new host.

#if __ASSERT || !__NO_OUTPUT
                const snPeer* hostPeer = m_Ctx->FindPeerById(newHost.GetPeerId());
#endif
                snAssert(hostPeer && hostPeer->GetCxnId() >= 0);

                rlTaskDebug("Migrating session:0x%016" I64FMT "x to remote peer: PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "]...",
                            m_Ctx->GetSessionId(),
                            hostPeer->GetPeerId(),
                            NET_ADDR_FOR_PRINTF(m_Ctx->GetCxnMgr()->GetAddress(hostPeer->GetEndpointId())));

                m_WaitTimeout = static_cast<int>(sm_TimeoutMs);
                rlTaskDebug("Waiting for host candidate - Timeout: %d", m_WaitTimeout);

                m_State = STATE_WAITING_FOR_NEW_HOST;
                m_Ctx->m_Transactor.AddRequestHandler(&m_RqstHandler,
                                                    m_Ctx->m_ChannelId);
                m_MyStatus.SetPending();
            }

            m_Ctx->m_HostPeerId = newHost.GetPeerId();

            success = true;
        }
    }
    rcatchall
    {
    }

    return success;
}

bool
snMigrateSessionTask::Migrate(const rlPeerInfo& newHost,
                            const rlSessionInfo* sessionInfo)
{
    bool success = false;

    //If we're the new host make sure we're not the current host.
    snAssert(!newHost.IsLocal() || !m_Ctx->IsHost());

    if(newHost.IsLocal())
    {
        snAssert(!sessionInfo);
        success = m_Ctx->m_RlSession.MigrateHost(newHost, NULL, &m_MyStatus);
    }
    else
    {
        snAssert(sessionInfo);
        success = m_Ctx->m_RlSession.MigrateHost(newHost, sessionInfo, &m_MyStatus);
    }

    return success;
}

void
snMigrateSessionTask::SendMigrateCommands()
{
    snAssert(m_Ctx->IsHost());
    snAssert(m_Ctx->GetPeerCount() > 0);
    
    rlTaskDebug("Sending migrate commands to peers...");

    m_NumRequestsSent = m_NumPendingResponses = m_NumSucceeded = 0;

    snMsgMigrateHostRequest mhRqst;

    mhRqst.Reset(m_Ctx->GetSessionId(), m_Ctx->GetSessionInfo());

    //We might not actually send requests to all these peers
    //(e.g. if their connection is invalid), but we want to keep
    //track of how many peers we have so we can later remove
    //peers that failed to set us as the host.
    m_NumRequestsSent = m_Ctx->m_NumPeers;

    for(int i = 0; i < m_NumRequestsSent; ++i)
    {
        const snPeer* peer = m_Ctx->m_Peers[i];

        m_RespHandlers[i].Reset(peer->GetPeerId());

        if(peer->GetCxnId() < 0)
        {
            continue;
        }
		else if(m_Ctx->m_CxnMgr->IsClosed(peer->GetCxnId()))
		{
			// the connection closed event might be pending
			continue;
		}
		else if(!peer->AllowMigration())
		{
			continue;
		}

        if(m_Ctx->m_Transactor.SendRequest(peer->GetCxnId(),
											NULL,
                                            mhRqst,
                                            sm_MigrateRequestInterval,
                                            &m_RespHandlers[i]))
        {
            ++m_NumPendingResponses;
        }
    }

    m_MyStatus.SetPending();

    if(!m_NumPendingResponses)
    {
        m_MyStatus.SetSucceeded();
    }
}

bool
snMigrateSessionTask::DropDeadbeats()
{
    snAssert(m_Ctx->IsHost());

    rlTaskDebug("Dropping deadbeat gamers...");

    //For each deadbeat peer collect the gamers local to that peer.

    rlGamerId deadbeats[RL_MAX_GAMERS_PER_SESSION];
    unsigned numDeadbeats = 0;

    for(int i = 0; i < m_NumRequestsSent; ++i)
    {
        if(!m_RespHandlers[i].m_Succeeded)
        {
            const u64 peerId = m_RespHandlers[i].m_PeerId;
            const unsigned numGamers =
                m_Ctx->GetGamers(peerId,
                                &deadbeats[numDeadbeats],
                                COUNTOF(deadbeats) - numDeadbeats);

            numDeadbeats += numGamers;

            snVerify(m_Ctx->DropPeer(peerId, NULL, NULL));
        }
    }

    rlTaskDebug("Synching session membership with peers...");

    //Build the current membership list that we will send
    //to peers to ensure everyone has the same view of the session.

    rlGamerId gamerIds[RL_MAX_GAMERS_PER_SESSION];
    unsigned numGamers = m_Ctx->GetGamers(gamerIds, COUNTOF(gamerIds));

    //If any of the current membership are going to be dropped
    //then remove them from the list.
    for(int i = 0; i < (int) numDeadbeats; ++i)
    {
        for(int j = 0; j < (int) numGamers; ++j)
        {
            if(gamerIds[j] == deadbeats[i])
            {
                gamerIds[j] = gamerIds[numGamers-1];
                --numGamers;
                break;
            }
        }
    }

    if(numGamers > 1)
    {
        snMsgSessionMemberIds msg;

        msg.Reset(m_Ctx->GetSessionId(), gamerIds, numGamers);

#if !__NO_OUTPUT
        rlTaskDebug("%d gamer ids will be synched", numGamers);
        for(int i = 0; i < (int) msg.m_Count; ++i)
        {
            char buf[64];
            rlTaskDebug("  %s", gamerIds[i].ToHexString(buf));
        }
#endif  //__NO_OUTPUT

        m_Ctx->BroadcastMsg(msg, NET_SEND_RELIABLE);
    }

    return true;
}

void
snMigrateSessionTask::OnRequest(netTransactor* transactor,
                                netRequestHandler* handler,
                                const netRequest* rqst)
{
    snAssert(&m_RqstHandler == handler);

    snMsgMigrateHostRequest mhRqst;

    if(STATE_WAITING_FOR_NEW_HOST == m_State
        && m_MyStatus.Pending()
        && mhRqst.Import(rqst->m_Data, rqst->m_SizeofData)
        && mhRqst.m_SessionId == m_Ctx->GetSessionId())
    {
        const snPeer* peer = m_Ctx->FindPeerByCxn(rqst->m_TxInfo.m_CxnId);

        const rlPeerInfo& newHost = m_Candidates[m_CurCandidate];

        snMsgMigrateHostResponse mhResp;

        //The peer pointer can be null if the peer is in the midst
        //of a host migration, but is also leaving the game.  The
        //leave command would have been sent to us before the
        //snMsgMigrateHostResponse was sent.
        if(peer)
        {
			if(newHost.GetPeerId() == peer->GetPeerId())
			{
				rlTaskDebug("OnRequest :: PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "]", peer->GetPeerId(), NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

				//Give the host our view of which local gamers are still
				//in the session.
				mhResp.Reset(m_Ctx->GetSessionId(), true);
				rlGamerInfo gamers[RL_MAX_LOCAL_GAMERS];
				snAssert(m_Ctx->m_LocalPeer.GetGamerCount() <= (int)COUNTOF(gamers));

				mhResp.m_NumMembers =
					m_Ctx->m_LocalPeer.GetGamers(gamers, COUNTOF(gamers));

				for(int i = 0; i < mhResp.m_NumMembers; ++i)
				{
					mhResp.m_MemberIds[i] = gamers[i].GetGamerId();
				}

				transactor->RemoveRequestHandler(handler);
				m_NewSessionInfo = mhRqst.m_SessionInfo;
				m_MyStatus.SetSucceeded();
			}
			else
			{
				rlTaskDebug("OnRequest :: Unexpected Peer - PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "]", peer->GetPeerId(), NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));
				mhResp.Reset(m_Ctx->GetSessionId(), false);
			}
        }
        else
        {
			rlTaskDebug("OnRequest :: Unknown peer [" NET_ADDR_FMT "]",
						NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

            mhResp.Reset(m_Ctx->GetSessionId(), false);
        }

        snVerify(transactor->SendResponse(rqst->m_TxInfo, mhResp));
    }
}

void
snMigrateSessionTask::OnResponse(netTransactor* /*transactor*/,
                                netResponseHandler* handler,
                                const netResponse* resp)
{
    if(resp->Complete())
    {
        RespHandler* respHandler =
            static_cast<RespHandler*>(handler);

        snAssert(m_NumPendingResponses > 0);
        --m_NumPendingResponses;

        respHandler->m_Succeeded = false;

        bool retry = false;

        const snPeer* peer = m_Ctx->FindPeerById(respHandler->m_PeerId);

        //The peer pointer can be null if the peer is in the midst
        //of a host migration, but is also leaving the game.  The
        //leave command would have been sent to us before the
        //snMsgMigrateHostResponse was sent, causing the peer
        //to be removed from our local session instance.

        if(resp->TimedOut())
        {
            //This situation occurs when the peer is in the wrong state
            //at the time we sent the first request, so we send another.

			rlTaskDebug("OnResponse :: TimedOut - PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "]", peer ? peer->GetPeerId() : 0, NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr));
            retry = true;
        }
        else if(resp->Answered())
        {
            snMsgMigrateHostResponse mhResp;

            if(!mhResp.Import(resp->m_Data, resp->m_SizeofData))
            {
                rlTaskError("Failed to import %s from:" NET_ADDR_FMT "",
                        mhResp.GetMsgName(),
                        NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr));
            }
            else
            {
				rlTaskDebug("OnResponse :: Received %s with %s - PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "]", 
                            mhResp.GetMsgName(),
							mhResp.m_Accept ? "ACCEPT" : "REJECT",
							peer ? peer->GetPeerId() : 0, 
							NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr));

                if(!mhResp.m_Accept)
                {
                    //This situation occurs when the peer is waiting for
                    //another peer to become the host at the time we sent
                    //the first request, so we send another.

                    retry = true;
                }
                //Make sure we still have a valid peer pointer.  It
                //could be invalid if the peer left while processing the
                //host migration.
                else if(peer)
                {
                    respHandler->m_Succeeded = true;

                    //Compare our view of the peer's local gamers with
                    //his view.  Remove any gamers that the peer's view
                    //does not contain.
                    rlGamerInfo gamers[RL_MAX_LOCAL_GAMERS];
                    snAssert((int)COUNTOF(gamers) >= peer->GetGamerCount());

                    const int numGamers = peer->GetGamers(gamers, COUNTOF(gamers));
                    rlGamerHandle notHave[RL_MAX_LOCAL_GAMERS];
                    int numNotHave = 0;

                    for(int i = 0; i < numGamers; ++i)
                    {
                        bool have = false;
                        for(int j = 0; j < mhResp.m_NumMembers; ++j)
                        {
                            if(gamers[i].GetGamerId() == mhResp.m_MemberIds[j])
                            {
                                have = true;
                                break;
                            }
                        }

                        if(!have)
                        {
                            notHave[numNotHave++] = gamers[i].GetGamerHandle();
                        }
                    }

                    if(numNotHave)
                    {
                        m_Ctx->DropGamers(&m_Ctx->m_LocalPeer,
                                            notHave,
                                            numNotHave,
                                            snSession::KICK_INFORM_PEERS,
                                            NULL,
											-1,
                                            NULL);
                    }
                }
            }
        }

        bool retried = false;

        if(retry && peer && peer->GetCxnId() >= 0)
        {
            snAssert(!respHandler->m_Succeeded);
            
            ++respHandler->m_Attempts;

            if(respHandler->m_Attempts < sm_MigrateRequestNumAttempts)
            {
                snMsgMigrateHostRequest mhRqst;
                mhRqst.Reset(m_Ctx->GetSessionId(), m_Ctx->GetSessionInfo());

                rlTaskDebug("Attempt %d to send migrate command to: PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "]...",
                            respHandler->m_Attempts,
							peer ? peer->GetPeerId() : 0,
                            NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr));

                if(m_Ctx->m_Transactor.SendRequest(peer->GetCxnId(),
													NULL,
                                                    mhRqst,
                                                    sm_MigrateRequestInterval,
                                                    respHandler))
                {
                    ++m_NumPendingResponses;
                    retried = true;
                }
            }
        }

        if(respHandler->m_Succeeded)
        {
			rlTaskDebug("Migrate host request: PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "] succeeded (%d pending)",
						peer ? peer->GetPeerId() : 0,
                        NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr),
                        m_NumPendingResponses);
        }
        else if(retried)
        {
			rlTaskDebug("Retrying migrate host request: PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "] (%d pending)...", 
						peer ? peer->GetPeerId() : 0,
                        NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr),
                        m_NumPendingResponses);
        }
        else
        {
			rlTaskDebug("Migrate host request: PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "] timed out or failed (%d pending)",
						peer ? peer->GetPeerId() : 0,
                        NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr),
                        m_NumPendingResponses);
        }

        if(0 == m_NumPendingResponses)
        {
            m_MyStatus.SetSucceeded();
        }
    }
}

void
snMigrateSessionTask::SetTimeoutPolicy(const unsigned timeoutMs)
{
	if(sm_TimeoutMs != timeoutMs)
	{
		sm_TimeoutMs = timeoutMs;
		if(sm_TimeoutMs < SNET_HOST_MIGRATION_MIN_TIMEOUT)
		{
			sm_TimeoutMs = SNET_HOST_MIGRATION_MIN_TIMEOUT;
		}

		snDebug("snMigrateSessionTask :: Setting Timeout: %u", sm_TimeoutMs);
		RefreshNumMigrateRequestAttempts();
	}
}

void
snMigrateSessionTask::SetMigrateRequestInterval(const unsigned requestInterval)
{
	if(sm_MigrateRequestInterval > sm_TimeoutMs)
	{
		snDebug("snMigrateSessionTask :: Requested RequestInterval %u smaller than ", requestInterval);
		return;
	}

	if(sm_MigrateRequestInterval != requestInterval)
	{
		snDebug("snMigrateSessionTask :: Setting RequestInterval: %u", requestInterval);
		sm_MigrateRequestInterval = requestInterval;
		RefreshNumMigrateRequestAttempts();
	}
}

void 
snMigrateSessionTask::RefreshNumMigrateRequestAttempts()
{
	// calculate how many requests we'll send
	unsigned nMigrateRequestNumAttempts = (sm_TimeoutMs / sm_MigrateRequestInterval) + (((sm_TimeoutMs % sm_MigrateRequestInterval) == 0) ? 0 : 1);
	if(sm_MigrateRequestNumAttempts != nMigrateRequestNumAttempts)
	{
		snDebug("snMigrateSessionTask :: Setting NumMigrateRequests: %u", nMigrateRequestNumAttempts);
		sm_MigrateRequestNumAttempts = nMigrateRequestNumAttempts;
	}
}

bool
snMigrateSessionTask::RemoveCandidate(const u64 peerId)
{
	for(int i = 0; i < m_NumCandidates; ++i)
	{
		if((m_Candidates[i].GetPeerId() == peerId) && !m_CandidateRemoved[i])
		{
			rlTaskDebug("RemoveCandidate :: Removing [%d] :: PeerID: 0x%" I64FMT "x", i, peerId);
			m_CandidateRemoved[i] = true; 
			return true;
		}
	}

	rlTaskDebug("RemoveCandidate :: Could not find PeerID: 0x%" I64FMT "x", peerId);

	return false;
}

///////////////////////////////////////////////////////////////////////////////
// snEnableMatchmakingAdvertiseTask
///////////////////////////////////////////////////////////////////////////////

snEnableMatchmakingAdvertiseTask::snEnableMatchmakingAdvertiseTask()
{
}

bool
snEnableMatchmakingAdvertiseTask::Configure(snSession* /*ctx*/)
{
	return true;
}

void
snEnableMatchmakingAdvertiseTask::Start()
{
	this->snTask::Start();

	rtry
	{
		rcheck(m_Ctx->Exists(), catchall,);

		rlTaskDebug("Enabling matchmaking advertising on: 0x%016" I64FMT "x...", m_Ctx->GetSessionId());

		m_Ctx->m_RlSession.EnableMatchmakingAdvertise(&m_MyStatus);
	}
	rcatchall
	{
		this->Finish(FINISH_FAILED);
	}
}

void
snEnableMatchmakingAdvertiseTask::Finish(const FinishType finishType, const int resultCode)
{
	rlTaskDebug("Enabling matchmaking advertising %s", FinishString(finishType));

	if(m_MyStatus.Pending())
	{
		snAssert(!FinishSucceeded(finishType));
		m_Ctx->m_RlSession.Cancel(&m_MyStatus);
	}

	this->snTask::Finish(finishType, resultCode);
}

void
snEnableMatchmakingAdvertiseTask::Update(const unsigned timeStep)
{
	this->snTask::Update(timeStep);

	if(m_MyStatus.Succeeded())
	{
		this->Finish(FINISH_SUCCEEDED);
	}
	else if(!m_MyStatus.Pending())
	{
		this->Finish(FINISH_FAILED);
	}
}

///////////////////////////////////////////////////////////////////////////////
// snChangeAttributesTask
///////////////////////////////////////////////////////////////////////////////

snChangeAttributesTask::snChangeAttributesTask()
{
}

bool
snChangeAttributesTask::Configure(snSession* /*ctx*/,
                                const rlMatchingAttributes& newAttrs)
{
    m_NewAttrs = newAttrs;
    return true;
}

void
snChangeAttributesTask::Start()
{
    this->snTask::Start();

    rtry
    {
        rcheck(m_Ctx->Exists(), catchall,);

        rlTaskDebug("Changing attributes on session:0x%016" I64FMT "x...",
                m_Ctx->GetSessionId());

        m_Ctx->m_RlSession.ChangeAttributes(m_NewAttrs, &m_MyStatus);
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snChangeAttributesTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Changing attributes %s", FinishString(finishType));

    if(FinishSucceeded(finishType) && m_Ctx->Exists())
    {
        if(m_Ctx->IsHost())
        {
            //Tell everyone about the new attributes.
            snMsgChangeSessionAttributesCmd cmd;
            cmd.Reset(m_Ctx->GetSessionId(), m_Ctx->GetConfig().m_Attrs);

            m_Ctx->BroadcastCommand(cmd, NULL);
        }
    }
	else
	{
		if(m_MyStatus.Pending())
		{
			m_Ctx->m_RlSession.Cancel(&m_MyStatus);
		}
	}

	//Save the pointer before calling finish.
	snSession* ctx = m_Ctx;

	//Call finish before dispatching the event because the event
	//can cause the task to be deleted, and calling Finish() after
	//that would be fatal.
	this->snTask::Finish(finishType, resultCode);

	if(FinishSucceeded(finishType) && ctx->Exists())
	{
		snEventSessionAttrsChanged e;
		ctx->DispatchEvent(&e);
	}
}

void
snChangeAttributesTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    if(m_MyStatus.Succeeded())
    {
        this->Finish(FINISH_SUCCEEDED);
    }
    else if(!m_MyStatus.Pending())
    {
        this->Finish(FINISH_FAILED);
    }
}

///////////////////////////////////////////////////////////////////////////////
// snSendInvitesTask
///////////////////////////////////////////////////////////////////////////////

snSendInvitesTask::snSendInvitesTask()
    : m_GamerHandles(NULL)
    , m_NumGamers(0)
{
}

snSendInvitesTask::~snSendInvitesTask()
{
    snAssert(!m_GamerHandles);

    if(m_GamerHandles)
    {
        Delete(m_GamerHandles, m_NumGamers, *m_Ctx->m_Allocator);
    }
}

bool
snSendInvitesTask::Configure(snSession* /*ctx*/,
                             const rlGamerHandle* gamerHandles,
                             const unsigned numGamers,
                             const char* subject,
                             const char* salutation)
{
    snAssert(numGamers < RL_MAX_GAMERS_PER_SESSION);
    snAssert(!m_GamerHandles);
    snAssert(!subject
            || strlen(subject) < RL_MAX_INVITE_SUBJECT_CHARS);
    snAssert(!salutation
            || strlen(salutation) < RL_MAX_INVITE_SALUTATION_CHARS);

    bool success = false;

    m_NumGamers = 0;
    m_GamerHandles =
        (rlGamerHandle*) m_Ctx->m_Allocator->RAGE_LOG_ALLOCATE(sizeof(rlGamerHandle) * numGamers, 0);

    if(snVerify(m_GamerHandles))
    {
        new(m_GamerHandles) rlGamerHandle[numGamers];

        for(int i = 0; i < (int) numGamers; ++i)
        {
            m_GamerHandles[i] = gamerHandles[i];
        }

        m_NumGamers = numGamers;

        safecpy(m_Subject, subject ? subject : "", COUNTOF(m_Subject));
        safecpy(m_Salutation, salutation ? salutation : "", COUNTOF(m_Salutation));
        success = true;
    }

    return success;
}

void
snSendInvitesTask::Start()
{
    rlTaskDebug("Inviting:%d gamers to session...", m_NumGamers);

    this->snTask::Start();

    rtry
    {
        rcheck(m_Ctx->Exists(), 
               catchall, 
               rlTaskError("snSession does not exist"));

        rcheck(m_Ctx->m_RlSession.SendInvites(m_GamerHandles,
                                            m_NumGamers,
                                            m_Subject,
                                            m_Salutation,
                                            &m_MyStatus),
                catchall,
                rlTaskError("Error sending invitations"));
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snSendInvitesTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Inviting gamers to session %s", FinishString(finishType));

    if(m_GamerHandles)
    {
        Delete(m_GamerHandles, m_NumGamers, *m_Ctx->m_Allocator);
        m_GamerHandles = NULL;
    }

	if(m_MyStatus.Pending())
	{
		snAssert(!FinishSucceeded(finishType));
		m_Ctx->m_RlSession.Cancel(&m_MyStatus);
	}

    this->snTask::Finish(finishType, resultCode);
}

void
snSendInvitesTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
    }
}

///////////////////////////////////////////////////////////////////////////////
// snSendPartyInvitesTask
///////////////////////////////////////////////////////////////////////////////

snSendPartyInvitesTask::snSendPartyInvitesTask()
{
}

snSendPartyInvitesTask::~snSendPartyInvitesTask()
{
}

bool
snSendPartyInvitesTask::Configure(snSession* /*ctx*/,
                             const int localGamerIndex)
{
    m_LocalGamerIndex = localGamerIndex;

    return true;
}

void
snSendPartyInvitesTask::Start()
{
    rlTaskDebug("Inviting party members to session...");

    this->snTask::Start();

    rtry
    {
        rcheck(m_Ctx->Exists(), 
            catchall, 
            rlTaskError("snSession does not exist"));

        rcheck(m_Ctx->m_RlSession.SendPartyInvites(m_LocalGamerIndex, &m_MyStatus),
            catchall,
            rlTaskError("Error sending invitations"));
    }
    rcatchall
    {
        this->Finish(FINISH_FAILED);
    }
}

void
snSendPartyInvitesTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Inviting party members to session %s", FinishString(finishType));

	if(m_MyStatus.Pending())
	{
		snAssert(!FinishSucceeded(finishType));
		m_Ctx->m_RlSession.Cancel(&m_MyStatus);
	}

    this->snTask::Finish(finishType, resultCode);
}

void
snSendPartyInvitesTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        this->Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
    }
}

#if 0
/*
This is a big hack that we thought we might need for GTA4PC because
they were attempting to write too much data to arbitrated leaderboards.
The hack was to split gamers up into multiple sessions so the leaderboard
writes for each session wouldn't exceed XBL limits.

We never finished it because GTA4PC decided to simply change the
affected leaderboards to non-arbitrated and that appeared to have
fixed the problem.

The hack actually works, but it was never tested with a number of
players that would exceed XBL limits (20 players in the case of
GTA4PC) so there's still the question of whether it will work in
that case.

Also, I never got around to writing relative score.  The plan was
to use the original session (not the split sessions) to write
relative score so that TrueSkill calculations would remain
consistent.

I'm leaving this code here in case it's ever needed again.

Other required changes include modifying snEndTask to call this code
before calling rlMatch::End(), modifying rlMatch to give access
to the inner class LbUpdate, and modifying rlMatch::End() to not write
arbitrated leaderboards, but still write non-arbitrated leaderboards
and relative score (both arb and non-arb).
*/

///////////////////////////////////////////////////////////////////////////////
// snGta4PcHackStatsTask
///////////////////////////////////////////////////////////////////////////////

class snGta4PcHackStatsTask : public snTask
{
public:
    SN_DECL_TASK(snGta4PcHackStatsTask);

    snGta4PcHackStatsTask();

    virtual ~snGta4PcHackStatsTask();

    bool Configure(snSession* /*ctx*/,
                    const unsigned gameMode,
                    const rlGamerInfo* gamers,
                    const unsigned numGamers);

    virtual void Start();

    virtual void Finish(const FinishType finishType, const int resultCode = 0);

    virtual void Update(const unsigned timeStep);

private:
    enum
    {
        MY_TIMEOUT  = 10000,

        MAX_GAMERS_PER_HACK     = 8,
    };

    enum
    {
        STATE_INVALID = -1,
        STATE_WAITING_FOR_SESSION_INFO,
        STATE_COMMIT_SESSION_ATTRS,
        STATE_COMMITTING_SESSION_ATTRS,
        STATE_CREATE_SESSION,
        STATE_CREATING_SESSION,
        STATE_ADD_MEMBERS,
        STATE_ADDING_MEMBERS,
        STATE_WAITING_FOR_CLIENTS_TO_REGISTER,
        STATE_REGISTER,
        STATE_REGISTERING,
        STATE_WAITING_FOR_START_COMMAND,
        STATE_START,
        STATE_STARTING,
        STATE_REPORT_STATS,
        STATE_REPORTING_STATS,
        STATE_END,
        STATE_ENDING,
        STATE_DESTROY_SESSION,
        STATE_DESTROYING_SESSION,
    };

    bool CommitSessionAttrs();

    bool CreateSession();

    bool AddMembers();

    void SendSessionInfo();

    void SendRegisterCompleteMsg();

    void SendStartCommand();

    bool Register();

    bool StartSession();

    bool ReportStats();

    bool EndSession();

    bool DestroySession();

    void OnNetEvent(netConnectionManager* cxnMgr,
                    const netEvent* evt);

    struct CompareGamerInfos
    {
        bool operator()(const rlGamerInfo* a, const rlGamerInfo* b) const
        {
            return a->GetGamerId() < b->GetGamerId();
        }
    };

    int m_State;
    netStatus m_MyStatus[2];
    rlXovStatus m_XovStatus[2];
    HANDLE m_hSession;
    unsigned m_GameMode;
    u64 m_ArbToken;
    XSESSION_INFO m_XSessionInfo;
    XSESSION_REGISTRATION_RESULTS* m_RegResults;
    DWORD m_LocalJoinerIndices[RL_MAX_LOCAL_GAMERS];
    BOOL m_LocalJoinerPrivSlots[RL_MAX_LOCAL_GAMERS];
    XUID m_RemoteJoinerXuids[RL_MAX_GAMERS_PER_SESSION];
    BOOL m_RemoteJoinerPrivSlots[RL_MAX_GAMERS_PER_SESSION];

    netConnectionManager::Delegate m_NetEvtDlgt;

    struct GamerData
    {
        GamerData()
            : m_LbUpdates(NULL)
            , m_Views(NULL)
            , m_Properties(NULL)
            , m_NumViews(0)
            , m_NumProps(0)
        {
        }

        ~GamerData()
        {
            snAssert(!m_LbUpdates);
            snAssert(!m_Views);
            snAssert(!m_Properties);
        }

        void Clear()
        {
            if(!snVerify(!m_XovStatus.Pending()))
            {
                m_XovStatus.Cancel();
            }

            snAssert(!m_MyStatus.Pending());

            if(m_Views)
            {
                delete [] m_Views;
            }

            if(m_Properties)
            {
                delete [] m_Properties;
            }

            m_GamerInfo.Clear();
            m_LbUpdates = NULL;
            m_Views = NULL;
            m_Properties = NULL;

            m_NumViews = m_NumProps = 0;
        }

        rlGamerInfo m_GamerInfo;
        const rlMatch::LbUpdate* m_LbUpdates;
        XSESSION_VIEW_PROPERTIES* m_Views;
        XUSER_PROPERTY* m_Properties;
        unsigned m_NumViews;
        unsigned m_NumProps;
        rlXovStatus m_XovStatus;
        netStatus m_MyStatus;
    };

    GamerData m_GamerDatas[RL_MAX_GAMERS_PER_SESSION];
    unsigned m_NumGamers;
    unsigned m_NumYetToRegister;

    int m_Timer;

    bool m_IAmHost  : 1;
    bool m_Failed   : 1;
};

///////////////////////////////////////////////////////////////////////////////
// snGta4PcHackStatsTask
///////////////////////////////////////////////////////////////////////////////

snGta4PcHackStatsTask::snGta4PcHackStatsTask()
: m_State(STATE_INVALID)
, m_hSession(NULL)
, m_GameMode(~0u)
, m_ArbToken(0)
, m_RegResults(NULL)
, m_NumGamers(0)
, m_NumYetToRegister(~0u)
, m_IAmHost(false)
, m_Failed(false)
{
    m_NetEvtDlgt.Bind(this, &snGta4PcHackStatsTask::OnNetEvent);
}

snGta4PcHackStatsTask::~snGta4PcHackStatsTask()
{
    if(m_RegResults)
    {
        delete [] (u8*) m_RegResults;
        m_RegResults = NULL;
    }

    for(int i = 0; i < COUNTOF(m_GamerDatas); ++i)
    {
        m_GamerDatas[i].Clear();
    }
}

bool
snGta4PcHackStatsTask::Configure(snSession* /*ctx*/,
                                const unsigned gameMode,
                                const rlGamerInfo* gamers,
                                const unsigned numGamers)
{
    snAssert(numGamers <= COUNTOF(m_GamerDatas));
    snAssert(ctx->IsArbitrated());

    const rlGamerInfo* sortedGamers[COUNTOF(m_GamerDatas)];
    for(int i = 0; i < (int) numGamers; ++i)
    {
        sortedGamers[i] = &gamers[i];
    }

    std::sort(&sortedGamers[0], &sortedGamers[numGamers], CompareGamerInfos());

    m_IAmHost = (sortedGamers[0]->GetGamerId() == ctx->GetOwner().GetGamerId());

    rlTaskDebug("%s will be the host of the stats session",
            sortedGamers[0]->IsLocal() ? "I" : sortedGamers[0]->GetName());

    m_GameMode = gameMode;

    for(int i = 0; i < (int) numGamers; ++i)
    {
        snAssert(!m_GamerDatas[i].m_LbUpdates);
        snAssert(!m_GamerDatas[i].m_NumProps);
        snAssert(!m_GamerDatas[i].m_NumViews);
        snAssert(!m_GamerDatas[i].m_Views);
        snAssert(!m_GamerDatas[i].m_Properties);

        m_GamerDatas[i].Clear();
        m_GamerDatas[i].m_GamerInfo = *sortedGamers[i];
    }

    m_NumGamers = numGamers;

    return true;
}

void
snGta4PcHackStatsTask::Start()
{
    rlTaskDebug("Hacking stats for Gta4 PC...");

    snAssert(m_Ctx->IsArbitrated());
    snAssert(!m_RegResults);

    m_Ctx->GetCxnMgr()->AddDelegate(&m_NetEvtDlgt, m_Ctx->m_ChannelId);

    m_Failed = false;

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        snAssert(!m_GamerDatas[i].m_LbUpdates);
        snAssert(!m_GamerDatas[i].m_NumProps);
        snAssert(!m_GamerDatas[i].m_NumViews);
        snAssert(!m_GamerDatas[i].m_Views);
        snAssert(!m_GamerDatas[i].m_Properties);

        m_GamerDatas[i].m_LbUpdates =
            m_Ctx->m_RlMatch.GetLbUpdates(m_GamerDatas[i].m_GamerInfo.GetGamerId());
    }

    if(m_IAmHost)
    {
        m_State = STATE_COMMIT_SESSION_ATTRS;
    }
    else
    {
        m_ArbToken = 0;
        m_Timer = MY_TIMEOUT;
        m_State = STATE_WAITING_FOR_SESSION_INFO;
    }

    this->snTask::Start();
}

void
snGta4PcHackStatsTask::Finish(const FinishType finishType, const int resultCode)
{
    rlTaskDebug("Hacking stats for Gta4 PC %s", FinishString(finishType));

    m_Ctx->GetCxnMgr()->RemoveDelegate(&m_NetEvtDlgt);

    if(m_RegResults)
    {
        delete [] (u8*) m_RegResults;
        m_RegResults = NULL;
    }

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        m_GamerDatas[i].Clear();
    }

    this->snTask::Finish(finishType, resultCode);
}

void
snGta4PcHackStatsTask::Update(const unsigned timeStep)
{
    this->snTask::Update(timeStep);

    m_XovStatus[0].Update();
    m_XovStatus[1].Update();

    switch(m_State)
    {
    case STATE_WAITING_FOR_SESSION_INFO:
        if(0 != m_ArbToken)
        {
            m_State = STATE_COMMIT_SESSION_ATTRS;
        }
        else
        {
            m_Timer -= (int) timeStep;
            if(m_Timer <= 0)
            {
                rlTaskDebug("Timed out waiting for session info");
                this->Finish(FINISH_FAILED);
            }
        }
        break;
    case STATE_COMMIT_SESSION_ATTRS:
        if(this->CommitSessionAttrs())
        {
            m_State = STATE_COMMITTING_SESSION_ATTRS;
        }
        else
        {
            this->Finish(FINISH_FAILED);
        }
        break;
    case STATE_COMMITTING_SESSION_ATTRS:
        if(m_MyStatus[0].Succeeded() && m_MyStatus[1].Succeeded())
        {
            m_State = STATE_CREATE_SESSION;
        }
        else if(!m_MyStatus[0].Pending() && !m_MyStatus[1].Pending())
        {
            this->Finish(FINISH_FAILED);
        }
        break;
    case STATE_CREATE_SESSION:
        if(this->CreateSession())
        {
            m_State = STATE_CREATING_SESSION;
        }
        else
        {
            this->Finish(FINISH_FAILED);
        }
        break;
    case STATE_CREATING_SESSION:
        if(m_MyStatus[0].Succeeded())
        {
            m_State = STATE_ADD_MEMBERS;
        }
        else if(!m_MyStatus[0].Pending())
        {
            this->Finish(FINISH_FAILED);
        }
        break;
    case STATE_ADD_MEMBERS:
        if(this->AddMembers())
        {
            m_State = STATE_ADDING_MEMBERS;
        }
        else
        {
            m_Failed = true;
            m_State = STATE_DESTROY_SESSION;
        }
        break;
    case STATE_ADDING_MEMBERS:
        if(m_MyStatus[0].Succeeded() && m_MyStatus[1].Succeeded())
        {
            if(m_IAmHost)
            {
                m_NumYetToRegister = m_NumGamers - 1;
                this->SendSessionInfo();
                m_Timer = MY_TIMEOUT;
                m_State = STATE_WAITING_FOR_CLIENTS_TO_REGISTER;
            }
            else
            {
                m_State = STATE_REGISTER;
            }
        }
        else if(!m_MyStatus[0].Pending() && !m_MyStatus[1].Pending())
        {
            m_Failed = true;
            m_State = STATE_DESTROY_SESSION;
        }
        break;
    case STATE_WAITING_FOR_CLIENTS_TO_REGISTER:
        if(0 == m_NumYetToRegister)
        {
            m_State = STATE_REGISTER;
        }
        else
        {
            m_Timer -= (int) timeStep;
            if(m_Timer <= 0)
            {
                rlTaskDebug("Timed out waiting for clients to register");
                rlTaskDebug("Fuck em");
                m_State = STATE_REGISTER;
            }
        }
        break;
    case STATE_REGISTER:
        if(this->Register())
        {
            m_State = STATE_REGISTERING;
        }
        else
        {
            m_Failed = true;
            m_State = STATE_DESTROY_SESSION;
        }
        break;
    case STATE_REGISTERING:
        if(m_MyStatus[0].Succeeded())
        {
            if(m_IAmHost)
            {
                this->SendStartCommand();
                m_State = STATE_START;
            }
            else
            {
                this->SendRegisterCompleteMsg();
                m_Timer = MY_TIMEOUT;
                m_State = STATE_WAITING_FOR_START_COMMAND;
            }
        }
        else if(!m_MyStatus[0].Pending())
        {
            m_Failed = true;
            m_State = STATE_DESTROY_SESSION;
        }
        break;
    case STATE_WAITING_FOR_START_COMMAND:
        m_Timer -= (int) timeStep;
        if(m_Timer <= 0)
        {
            rlTaskDebug("Timed out waiting for start command");
            this->Finish(FINISH_FAILED);
        }
        break;
    case STATE_START:
        if(this->StartSession())
        {
            m_State = STATE_STARTING;
        }
        else
        {
            m_Failed = true;
            m_State = STATE_DESTROY_SESSION;
        }
        break;
    case STATE_STARTING:
        if(m_MyStatus[0].Succeeded())
        {
            m_State = STATE_REPORT_STATS;
        }
        else if(!m_MyStatus[0].Pending())
        {
            m_Failed = true;
            m_State = STATE_DESTROY_SESSION;
        }
        break;
    case STATE_REPORT_STATS:
        if(this->ReportStats())
        {
            m_State = STATE_REPORTING_STATS;
        }
        else
        {
            m_Failed = true;
            m_State = STATE_DESTROY_SESSION;
        }
        break;
    case STATE_REPORTING_STATS:
        {
            unsigned numPending = 0;
            for(int i = 0; i < (int) m_NumGamers; ++i)
            {
                if(m_GamerDatas[i].m_MyStatus.Pending())
                {
                    ++numPending;
                    m_GamerDatas[i].m_XovStatus.Update();
                }
            }

            if(0 == numPending)
            {
                m_State = STATE_END;
            }
        }
        break;
    case STATE_END:
        if(this->EndSession())
        {
            m_State = STATE_ENDING;
        }
        else
        {
            m_Failed = true;
            m_State = STATE_DESTROY_SESSION;
        }
        break;
    case STATE_ENDING:
        if(m_MyStatus[0].Succeeded())
        {
            m_State = STATE_DESTROY_SESSION;
        }
        else if(!m_MyStatus[0].Pending())
        {
            m_Failed = true;
            m_State = STATE_DESTROY_SESSION;
        }
        break;
    case STATE_DESTROY_SESSION:
        if(this->DestroySession())
        {
            m_State = STATE_DESTROYING_SESSION;
        }
        else
        {
            this->Finish(FINISH_FAILED);
        }
        break;
    case STATE_DESTROYING_SESSION:
        if(!m_MyStatus[0].Pending())
        {
            this->Finish(!m_Failed && m_MyStatus[0].Succeeded());
        }
        break;
    }
}

bool
snGta4PcHackStatsTask::CommitSessionAttrs()
{
    rlTaskDebug("Committing session attributes...");

    bool success = true;

    m_XovStatus[0].Reset("XUserSetContextEx", &m_MyStatus[0]);
    m_XovStatus[1].Reset("XUserSetContextEx", &m_MyStatus[1]);

    const int localIndex = m_Ctx->GetOwner().GetLocalIndex();
    const unsigned gameType = X_CONTEXT_GAME_TYPE_STANDARD;

    DWORD dw0 = XUserSetContextEx(localIndex,
                                X_CONTEXT_GAME_TYPE,
                                gameType,
                                m_XovStatus[0].ToXov());

    DWORD dw1 = XUserSetContextEx(localIndex,
                                X_CONTEXT_GAME_MODE,
                                m_GameMode,
                                m_XovStatus[1].ToXov());

    if(ERROR_IO_PENDING != dw0)
    {
        rlXovError("XUserSetContextEx", dw0, m_XovStatus[0].ToXov());
        success = false;
    }

    if(ERROR_IO_PENDING != dw1)
    {
        rlXovError("XUserSetContextEx", dw1, m_XovStatus[1].ToXov());
        success = false;
    }

    return success;
}

bool
snGta4PcHackStatsTask::CreateSession()
{
    rlTaskDebug("Creating session as %s with %d slots...",
        m_IAmHost ? "host" : "client",
        m_NumGamers);

    bool success = true;

    m_XovStatus[0].Reset("XSessionCreate", &m_MyStatus[0]);

    unsigned flags = XSESSION_CREATE_USES_STATS
                    | XSESSION_CREATE_USES_ARBITRATION
                    | XSESSION_CREATE_USES_PEER_NETWORK;

    if(m_IAmHost)
    {
        flags |= XSESSION_CREATE_HOST;
    }

    const int localIndex = m_Ctx->GetOwner().GetLocalIndex();

    DWORD dw = XSessionCreate(flags,
                                localIndex,
                                0,              //pub slots
                                m_NumGamers,    //priv slots
                                &m_ArbToken,
                                &m_XSessionInfo,
                                m_XovStatus[0].ToXov(),
                                &m_hSession);

    if(ERROR_IO_PENDING != dw)
    {
        rlXovError("XSessionCreate", dw, m_XovStatus[0].ToXov());
        success = false;
    }

    return success;
}

bool
snGta4PcHackStatsTask::AddMembers()
{
    rlTaskDebug("Adding members to session:0x%016" I64FMT "x...",
            m_XSessionInfo.sessionID);

    bool success = true;

    unsigned numLocalGamers = 0;
    unsigned numRemoteGamers = 0;

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        const rlGamerInfo& gamerInfo = m_GamerDatas[i].m_GamerInfo;

        rlTaskDebug("  Adding %s to session:0x%016" I64FMT "x...",
                gamerInfo.GetName(),
                m_XSessionInfo.sessionID);

        if(gamerInfo.IsLocal())
        {
            m_LocalJoinerIndices[numLocalGamers] = gamerInfo.GetLocalIndex();
            m_LocalJoinerPrivSlots[numLocalGamers] = TRUE;
            ++numLocalGamers;
        }
        else
        {
            m_RemoteJoinerXuids[numRemoteGamers] = gamerInfo.GetGamerHandle().GetXuid();
            m_RemoteJoinerPrivSlots[numRemoteGamers] = TRUE;
            ++numRemoteGamers;
        }
    }

    snAssert(numLocalGamers);
    snAssert(numRemoteGamers);

    m_XovStatus[0].Reset("XSessionJoinLocal", &m_MyStatus[0]);

    DWORD dw = XSessionJoinLocal(m_hSession,
                                numLocalGamers,
                                m_LocalJoinerIndices,
                                m_LocalJoinerPrivSlots,
                                m_XovStatus[0].ToXov());

    if(ERROR_IO_PENDING != dw)
    {
        rlXovError("XSessionJoinLocal", dw, m_XovStatus[0].ToXov());
        success = false;
    }
    else
    {
        m_XovStatus[1].Reset("XSessionJoinRemote", &m_MyStatus[1]);

        DWORD dw = XSessionJoinRemote(m_hSession,
                                    numRemoteGamers,
                                    m_RemoteJoinerXuids,
                                    m_RemoteJoinerPrivSlots,
                                    m_XovStatus[1].ToXov());

        if(ERROR_IO_PENDING != dw)
        {
            rlXovError("XSessionJoinRemote", dw, m_XovStatus[1].ToXov());
            m_XovStatus[0].Cancel();
            success = false;
        }
    }

    return success;
}

struct SessionInfoMsg
{
    NET_MESSAGE_DECL(SessionInfoMsg, SESSION_INFO_MSG);

    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerBytes(&msg.m_XSessionInfo, sizeof(msg.m_XSessionInfo))
                && bb.SerUns(msg.m_ArbToken, 64);
    }

    XSESSION_INFO m_XSessionInfo;
    u64 m_ArbToken;
};

struct RegisterCompleteMsg
{
    NET_MESSAGE_DECL(RegisterCompleteMsg, REGISTER_COMPLETE_MSG);

    NET_MESSAGE_SER(/*bb*/, /*msg*/)
    {
        return true;
    }
};

struct StartCommandMsg
{
    NET_MESSAGE_DECL(StartCommandMsg, START_COMMAND_MSG);

    NET_MESSAGE_SER(/*bb*/, /*msg*/)
    {
        return true;
    }
};

NET_MESSAGE_IMPL(SessionInfoMsg);
NET_MESSAGE_IMPL(RegisterCompleteMsg);
NET_MESSAGE_IMPL(StartCommandMsg);

void
snGta4PcHackStatsTask::SendSessionInfo()
{
    SessionInfoMsg msg;
    msg.m_XSessionInfo = m_XSessionInfo;
    msg.m_ArbToken = m_ArbToken;

    //Don't send to ourselves.
    for(int i = 1; i < (int) m_NumGamers; ++i)
    {
        const rlGamerInfo& gamerInfo = m_GamerDatas[i].m_GamerInfo;

        rlTaskDebug("Sending %s for session:0x%016" I64FMT "x to %s",
                msg.GetMsgName(),
                m_XSessionInfo.sessionID,
                gamerInfo.GetName());

        m_Ctx->SendMsg(gamerInfo.GetPeerInfo().GetPeerId(), msg, NET_SEND_RELIABLE);
    }
}

void
snGta4PcHackStatsTask::SendRegisterCompleteMsg()
{
    RegisterCompleteMsg msg;

    //First gamer in the array is the host.
    const rlGamerInfo& gamerInfo = m_GamerDatas[0].m_GamerInfo;

    rlTaskDebug("Sending %s for session:0x%016" I64FMT "x to %s",
            msg.GetMsgName(),
            m_XSessionInfo.sessionID,
            gamerInfo.GetName());

    m_Ctx->SendMsg(gamerInfo.GetPeerInfo().GetPeerId(), msg, NET_SEND_RELIABLE);
}

void
snGta4PcHackStatsTask::SendStartCommand()
{
    StartCommandMsg msg;

    //Don't send to ourselves.
    for(int i = 1; i < (int) m_NumGamers; ++i)
    {
        const rlGamerInfo& gamerInfo = m_GamerDatas[i].m_GamerInfo;

        rlTaskDebug("Sending %s for session:0x%016" I64FMT "x to %s",
                msg.GetMsgName(),
                m_XSessionInfo.sessionID,
                gamerInfo.GetName());

        m_Ctx->SendMsg(gamerInfo.GetPeerInfo().GetPeerId(), msg, NET_SEND_RELIABLE);
    }
}

bool
snGta4PcHackStatsTask::Register()
{
    rlTaskDebug("Registering for arbitration on session:0x%016" I64FMT "x...",
            m_XSessionInfo.sessionID);

    bool success = false;

    DWORD dwSizeofBuffer = 0;
    DWORD dw = XSessionArbitrationRegister(m_hSession,
                                            0,
                                            m_ArbToken,
                                            &dwSizeofBuffer,
                                            NULL,
                                            NULL);

    if(ERROR_INSUFFICIENT_BUFFER == dw && dwSizeofBuffer > 0)
    {
        success = true;

        m_RegResults =
            (XSESSION_REGISTRATION_RESULTS*) rage_new u8[dwSizeofBuffer];

        m_XovStatus[0].Reset("XSessionArbitrationRegister", &m_MyStatus[0]);

        dw = XSessionArbitrationRegister(m_hSession,
                                        0,
                                        m_ArbToken,
                                        &dwSizeofBuffer,
                                        m_RegResults,
                                        m_XovStatus[0].ToXov());

        if(ERROR_IO_PENDING != dw)
        {
            rlXovError("XSessionArbitrationRegister", dw, m_XovStatus[0].ToXov());
            success = false;
        }
    }

    return success;
}

bool
snGta4PcHackStatsTask::StartSession()
{
    rlTaskDebug("Starting session:0x%016" I64FMT "x...",
            m_XSessionInfo.sessionID);

    bool success = true;

    m_XovStatus[0].Reset("XSessionStart", &m_MyStatus[0]);

    DWORD dw = XSessionStart(m_hSession,
                            0,
                            m_XovStatus[0].ToXov());

    if(ERROR_IO_PENDING != dw)
    {
        rlXovError("XSessionStart", dw, m_XovStatus[0].ToXov());
        success = false;
    }

    return success;
}

bool
snGta4PcHackStatsTask::ReportStats()
{
    rlTaskDebug("Reporting stats for session:0x%016" I64FMT "x...",
            m_XSessionInfo.sessionID);

    bool success = false;

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        GamerData* gd = &m_GamerDatas[i];

        rlTaskDebug("  Reporting stats for %s in session:0x%016" I64FMT "x...",
                gd->m_GamerInfo.GetName(),
                m_XSessionInfo.sessionID);

        //Count the number of views and properties.

        snAssert(!gd->m_Views);
        snAssert(!gd->m_Properties);

        gd->m_NumViews = 0;
        gd->m_NumProps = 0;

        for(const rlMatch::LbUpdate* lbu = gd->m_LbUpdates; lbu; lbu = lbu->m_Next)
        {
            ++gd->m_NumViews;
            gd->m_NumProps += lbu->m_UpdateSchema.GetFieldCount();
        }

        if(gd->m_NumViews < 1)
        {
            //At least one view is needed to report score.
            gd->m_NumViews = 1;
        }

        if(gd->m_NumProps < 2)
        {
            //At least two properties are needed to report score.
            gd->m_NumProps = 2;
        }

        gd->m_Views = rage_new XSESSION_VIEW_PROPERTIES[gd->m_NumViews];
        gd->m_Properties = rage_new XUSER_PROPERTY[gd->m_NumProps];

        if(!snVerify(gd->m_Views))
        {
            rlTaskError("Error allocating XSESSION_VIEW_PROPERTIES");
        }
        else if(!snVerify(gd->m_Properties))
        {
            rlTaskError("Error allocating XUSER_PROPERTY");
        }
        else
        {
            XSESSION_VIEW_PROPERTIES* view = gd->m_Views;
            XUSER_PROPERTY* prop = gd->m_Properties;

            for(const rlMatch::LbUpdate* lbu = gd->m_LbUpdates; lbu; lbu = lbu->m_Next)
            {
                rlTaskDebug("Reporting stats for gamer:%s view:0x%08x",
                        gd->m_GamerInfo.GetName(),
                        lbu->m_LbInfo->GetId());

                view->dwViewId = lbu->m_LbInfo->GetId();
                view->dwNumProperties = 0;
                view->pProperties = prop;
                const rlSchema& schema = lbu->m_UpdateSchema;
                const int numFields = schema.GetFieldCount();

                for(int i = 0; i < numFields; ++i)
                {
                    snAssertf(!schema.IsNil(i),
                                "Schema field: %d is nil", i);

                    ++view->dwNumProperties;

                    prop->dwPropertyId = schema.GetFieldId(i);
                    prop->value.type = (BYTE) XPROPERTYTYPEFROMID(prop->dwPropertyId);

                    snVerify(schema.GetFieldData(i,
                                                    &prop->value.i64Data,
                                                    sizeof(prop->value.i64Data)));

                    ++prop;
                }

                snAssert(unsigned(prop - view->pProperties) == view->dwNumProperties);

                if(view->dwNumProperties > 0)
                {
                    ++view;
                }
            }

            const unsigned numViews = unsigned(view - gd->m_Views);

            if(numViews)
            {
                gd->m_XovStatus.Reset("XSessionWriteStats", &gd->m_MyStatus);

                //Write the stats.  Note this only does a local write.  Stats
                //are only reported to XBL during a call to XSessionEnd().
                DWORD dwStatus =
                    XSessionWriteStats(m_hSession,
                                       gd->m_GamerInfo.GetGamerHandle().GetXuid(),
                                       numViews,
                                       gd->m_Views,
                                       gd->m_XovStatus.ToXov());

                if(ERROR_IO_PENDING != dwStatus && ERROR_SUCCESS != dwStatus)
                {
                    rlXovError("XSessionWriteStats", dwStatus, gd->m_XovStatus.ToXov());
                }
                else
                {
                    success = true;
                }
            }
            else
            {
                success = true;

                gd->m_MyStatus.ForceSucceeded();
            }
        }
    }

    return success;
}

bool
snGta4PcHackStatsTask::EndSession()
{
    rlTaskDebug("Ending session:0x%016" I64FMT "x...",
            m_XSessionInfo.sessionID);

    bool success = true;

    m_XovStatus[0].Reset("XSessionEnd", &m_MyStatus[0]);

    DWORD dw = XSessionEnd(m_hSession, m_XovStatus[0].ToXov());

    if(ERROR_IO_PENDING != dw)
    {
        rlXovError("XSessionEnd", dw, m_XovStatus[0].ToXov());
        success = false;
    }

    return success;
}

bool
snGta4PcHackStatsTask::DestroySession()
{
    rlTaskDebug("Destroying session:0x%016" I64FMT "x...",
            m_XSessionInfo.sessionID);

    bool success = true;

    m_XovStatus[0].Reset("XSessionDelete", &m_MyStatus[0]);

    DWORD dw = XSessionDelete(m_hSession, m_XovStatus[0].ToXov());

    if(ERROR_IO_PENDING != dw)
    {
        rlXovError("XSessionDelete", dw, m_XovStatus[0].ToXov());
        success = false;
    }

    return success;
}

void
snGta4PcHackStatsTask::OnNetEvent(netConnectionManager* /*cxnMgr*/, const netEvent* evt)
{
    unsigned msgId;

    if(NET_EVENT_FRAME_RECEIVED == evt->GetId()
        && netMessage::GetId(&msgId,
                            evt->m_FrameReceived->m_Payload,
                            evt->m_FrameReceived->m_SizeofPayload))
    {
        if(SessionInfoMsg::MSG_ID() == msgId)
        {
            if(STATE_WAITING_FOR_SESSION_INFO == m_State
                && 0 == m_ArbToken)
            {
                SessionInfoMsg msg;

                if(msg.Import(evt->m_FrameReceived->m_Payload,
                            evt->m_FrameReceived->m_SizeofPayload))
                {
                    m_XSessionInfo = msg.m_XSessionInfo;
                    m_ArbToken = msg.m_ArbToken;
                }
            }
        }
        else if(RegisterCompleteMsg::MSG_ID() == msgId)
        {
            if(STATE_WAITING_FOR_CLIENTS_TO_REGISTER == m_State
                && m_NumYetToRegister > 0)
            {
                --m_NumYetToRegister;
            }
        }
        else if(StartCommandMsg::MSG_ID() == msgId)
        {
            if(STATE_WAITING_FOR_START_COMMAND == m_State)
            {
                m_State = STATE_START;
            }
        }
    }
}

#endif  //0

#if RSG_DURANGO

unsigned snNotifyAddGamerTask::sm_TimeoutMs = snNotifyAddGamerTask::DEFAULT_TIMEOUT_MS;

snNotifyAddGamerTask::snNotifyAddGamerTask()
	: m_SizeOfData(0)
	, m_bAddPlayer(true)
	, m_TimeStarted(0)
{
	sysMemSet(m_Data, 0, sizeof(m_Data));
}

snNotifyAddGamerTask::~snNotifyAddGamerTask()
{

}

bool snNotifyAddGamerTask::Configure(snSession* , const rlGamerInfo& gamerInfo, const rlSlotType slotType, const void* data, const unsigned sizeofData)
{
	m_GamerInfo = gamerInfo;
	m_SlotType = slotType;
	m_bAddPlayer = true; 

	if (sizeofData > 0)
	{
		sysMemCpy(m_Data, data, sizeofData);
		m_SizeOfData = sizeofData;
	}
	
	m_State = STATE_GET_DISPLAYNAME;
	return true;
}

void snNotifyAddGamerTask::Finish(const FinishType finishType, const int resultCode)
{
	// fail-safe
	if (m_DisplayNameStatus.Pending())
	{
		g_rlXbl.GetProfileManager()->CancelPlayerNameRequest(&m_DisplayNameStatus);
	}

	if (!WasCanceled())
	{
		// if display name as read successfully, copy it to gamer info
		if (finishType == FINISH_SUCCEEDED)
		{
			m_GamerInfo.SetDisplayName(&m_DisplayName);

			// set the session gamer display name
			snGamer* g = m_Ctx->FindGamerByHandle(m_GamerInfo.GetGamerHandle());
			if (g)
			{
				g->m_GamerInfo.SetDisplayName(&m_DisplayName);
			}
		}
		else
		{
			m_Ctx->m_DirtyDisplayNames = true;
			rlTaskError("Unable to retrieve display name, asserts are expected. Throwing ADDED_GAMER event anyway.")
		}

		//Save the pointer before calling finish.
		snSession* ctx = m_Ctx;

		//Call finish before dispatching the event because the event
		//can cause the task to be deleted, and calling Finish() after
		//that would be fatal.
		this->snTask::Finish(finishType, resultCode);

		if(!m_bAddPlayer)
		{
			rlTaskDebug("Gamer - %s - flagged not to add", m_GamerInfo.GetDisplayName());

			// We'll still need to notify about the display name - use the snEventSessionDisplayNameRetrieved for this
			snEventSessionDisplayNameRetrieved evt(m_GamerInfo);
			ctx->m_Delegator.Dispatch(ctx, &evt);
		}
		else if(ctx->IsMember(m_GamerInfo.GetGamerId()))
		{
			const EndpointId endpointId = ctx->GetPeerEndpointId(m_GamerInfo.GetPeerInfo().GetPeerId());
			const netAddress relayAddr = NET_IS_VALID_ENDPOINT_ID(endpointId) ? ctx->GetCxnMgr()->GetRelayAddress(endpointId) : netAddress::INVALID_ADDRESS;

			// Do not fail the add if the display name is unavailable, fallback to gamer tag
			snEventAddedGamer ag(m_GamerInfo, endpointId, relayAddr, m_SlotType, m_Data, m_SizeOfData);
			ctx->m_Delegator.Dispatch(ctx, &ag);
		}
		else
		{
			// this can happen if the gamer is removed while we are getting the display name
			rlTaskWarning("Gamer - %s - is no longer a member", m_GamerInfo.GetDisplayName());
		}
	}
	else
	{
		this->snTask::Finish(finishType, resultCode);
	}
}

bool snNotifyAddGamerTask::GetDisplayName()
{
	rlTaskDebug3("Requesting Display name for %" I64FMT "u", m_GamerInfo.GetGamerHandle().GetXuid());
	return g_rlXbl.GetProfileManager()->GetPlayerNames(m_Ctx->GetOwner().GetLocalIndex(), &m_GamerInfo.GetGamerHandle(), 1, &m_DisplayName, NULL, &m_DisplayNameStatus);
}

const rlGamerHandle& snNotifyAddGamerTask::GetGamerHandle()
{
	return m_GamerInfo.GetGamerHandle();
}

void snNotifyAddGamerTask::DoCancel()
{
	if (m_DisplayNameStatus.Pending())
	{
		g_rlXbl.GetProfileManager()->CancelPlayerNameRequest(&m_DisplayNameStatus);
	}
}

void snNotifyAddGamerTask::Update(const unsigned timeStep)
{
	this->snTask::Update(timeStep);

	if (WasCanceled())
	{
		Finish(FINISH_CANCELED);
		return;
	}

	switch(m_State)
	{
	case STATE_GET_DISPLAYNAME:
		{
			if (GetDisplayName())
			{
				m_TimeStarted = sysTimer::GetSystemMsTime();
				m_State = STATE_GETTING_DISPLAYNAME;
			}
			else
			{
				rlTaskDebug("GetDisplayName failed.");
				Finish(FINISH_FAILED);
			}
		}
		break;
	case STATE_GETTING_DISPLAYNAME:
		if(m_DisplayNameStatus.Succeeded())
		{
			Finish(FINISH_SUCCEEDED);
		}
		else if(!m_DisplayNameStatus.Pending())
		{
			Finish(FINISH_FAILED);
		}
		// check for timeout, only execute if our timeout time is non-zero
		else if((sm_TimeoutMs > 0) && ((sysTimer::GetSystemMsTime() - m_TimeStarted) > sm_TimeoutMs))
		{				
			// fail here so that we dirty the display names and pull this information again
			rlTaskDebug("Getting display name timed out (Timeout: %ums", sm_TimeoutMs);
			Finish(FINISH_FAILED);
			g_rlXbl.GetProfileManager()->CancelPlayerNameRequest(&m_DisplayNameStatus);
		}
		break;
	}
}

void snNotifyAddGamerTask::DoNotAddPlayer()
{
	if(m_bAddPlayer)
	{
		rlTaskDebug("DoNotAddPlayer");
		m_bAddPlayer = false;
	}
}

void snNotifyAddGamerTask::SetTimeoutPolicy(const unsigned timeoutMs)
{
	if(sm_TimeoutMs != timeoutMs)
	{
		sm_TimeoutMs = timeoutMs;
		snDebug("snNotifyAddGamerTask :: Setting Timeout: %u", sm_TimeoutMs);
	}
}

#endif // RSG_DURANGO

}   //namespace rage
