// 
// snet/snet.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNET_SNET_H
#define SNET_SNET_H

#include "diag/channel.h"
#include "rline/rl.h"
#include "system/stack.h"

namespace rage
{

enum
{
    SNET_MAX_SIZEOF_GAMER_DATA          = 512,

    //Size of data that can be sent to a joiner when responding
    //to a join request.
    SNET_MAX_SIZEOF_JOIN_RESPONSE_DATA  = 512,
};

//PURPOSE
//  Code sent in join responses.
enum snJoinResponseCode
{
    //Request was denied because the session is not joinable.
    SNET_JOIN_DENIED_NOT_JOINABLE,
	//Request was denied because we could not establish a connection to the session
	SNET_JOIN_DENIED_FAILED_TO_ESTABLISH,
	//Request was denied due to no empty slots.
    SNET_JOIN_DENIED_NO_EMPTY_SLOTS,
    //Joiner is already a member.
    SNET_JOIN_DENIED_ALREADY_JOINED,
	//Request was denied due to being invite / private only
	SNET_JOIN_DENIED_PRIVATE,
	//Request was denied by the app.
    SNET_JOIN_DENIED_APP_SAID_NO,
    //Request was accepted.
    SNET_JOIN_ACCEPTED,

    SNET_JOIN_NUM_CODES
};

RAGE_DECLARE_CHANNEL(snet)

#define snDebug(fmt, ...)						RAGE_DEBUGF1(__rage_channel, fmt, ##__VA_ARGS__)
#define snDebug1(fmt, ...)						RAGE_DEBUGF1(__rage_channel, fmt, ##__VA_ARGS__)
#define snDebug2(fmt, ...)						RAGE_DEBUGF2(__rage_channel, fmt, ##__VA_ARGS__)
#define snDebug3(fmt, ...)						RAGE_DEBUGF3(__rage_channel, fmt, ##__VA_ARGS__)
#define snDisplay(fmt, ...)						RAGE_DISPLAYF(__rage_channel, fmt, ##__VA_ARGS__)
#define snWarning(fmt, ...)						RAGE_WARNINGF(__rage_channel, fmt, ##__VA_ARGS__)
#define snError(fmt, ...)						RAGE_ERRORF(__rage_channel, fmt, ##__VA_ARGS__)
#define snCondLogf(cond, severity, fmt, ...)	RAGE_CONDLOGF(__rage_channel, cond, severity, fmt, ##__VA_ARGS__)

#if USE_NET_ASSERTS
#if __ASSERT
#define snVerify(cond)							RAGE_VERIFY(__rage_channel, cond)
#define snVerifyf(cond, fmt, ...)				RAGE_VERIFYF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#else
#define snVerify(cond)							( Likely(cond) || _NetworkVerifyf(RAGE_CAT2(Channel_,__rage_channel), #cond, __FILE__, __LINE__, "") )
#define snVerifyf(cond, fmt, ...)				( Likely(cond) || _NetworkVerifyf(RAGE_CAT2(Channel_,__rage_channel), #cond, __FILE__, __LINE__, fmt, ##__VA_ARGS__) )
#endif  // __ASSERT

#if __ASSERT
#define snAssert(cond)							RAGE_ASSERT(__rage_channel, cond)
#define snAssertf(cond, fmt, ...)				RAGE_ASSERTF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#else
#define snAssert(cond)			do { static bool __FILE__l__LINE__ =false;  \
	if( !Likely(cond) && !(__FILE__l__LINE__) ) { sysStack::PrintStackTrace(); \
	RAGE_ERRORF(__rage_channel, "NetworkAssertf(%s) FAILED", #cond); __FILE__l__LINE__=true;  } } while(false)
#define snAssertf(cond, fmt, ...)	do { static bool __FILE__l__LINE__ =false;  \
	if( !Likely(cond) && !(__FILE__l__LINE__) ) { sysStack::PrintStackTrace(); char formatted[1024]; formatf(formatted, fmt, ##__VA_ARGS__); \
	RAGE_ERRORF(__rage_channel, "NetworkAssertf(%s) FAILED: %s", #cond, formatted); __FILE__l__LINE__=true;  } } while(false)
#endif  // __ASSERT

#else
#define snVerify(cond)							RAGE_VERIFY(__rage_channel, cond)
#define snVerifyf(cond, fmt, ...)				RAGE_VERIFYF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#define snAssert(cond) 							RAGE_ASSERT(__rage_channel, cond)
#define snAssertf(cond, fmt, ...)  				RAGE_ASSERTF(__rage_channel, cond, fmt, ##__VA_ARGS__)
#endif // USE_NET_ASSERTS

}   //namespace rage

#endif  //SNET_SNET_H

//This is deliberately outside of the #include guards
//in order to mitigate problems with unity builds.
#undef __rage_channel
#define __rage_channel snet
