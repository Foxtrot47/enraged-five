// 
// snet/messages.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "messages.h"
  
namespace rage
{

NET_MESSAGE_IMPL(snMsgConfigRequest);
NET_MESSAGE_IMPL(snMsgConfigResponse);
NET_MESSAGE_IMPL(snMsgBase);
NET_MESSAGE_IMPL(snMsgJoinRequest);
NET_MESSAGE_IMPL(snMsgJoinResponse);
NET_MESSAGE_IMPL(snMsgAddGamerToSessionCmd);
NET_MESSAGE_IMPL(snMsgHostLeftWhilstJoiningCmd);
NET_MESSAGE_IMPL(snMsgRemoveGamersFromSessionCmd);
NET_MESSAGE_IMPL(snMsgStartMatchCmd);
NET_MESSAGE_IMPL(snMsgNotMigrating);
NET_MESSAGE_IMPL(snMsgMigrateHostRequest);
NET_MESSAGE_IMPL(snMsgMigrateHostResponse);
NET_MESSAGE_IMPL(snMsgChangeSessionAttributesCmd);
NET_MESSAGE_IMPL(snMsgSetInvitableCmd);
NET_MESSAGE_IMPL(snMsgSessionMemberIds);
NET_MESSAGE_IMPL(snMsgRequestGamerInfo);

}   //namespace rage
