// 
// snet/session.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "session.h"

#include "diag/output.h"
#include "diag/seh.h"
#include "messages.h"
#include "net/nethardware.h"
#include "net/task.h"
#include "net/transaction.h"
#include "rline/rlpresence.h"
#include "snet.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"

#include "rline/rlqos.h"
#include "rline/rlxbl.h"

#if RSG_PC
#include "rline/rlpc.h"
#endif

#include <algorithm>

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(snet, session)
#undef __rage_channel
#define __rage_channel snet_session

#if RSG_DURANGO
const int snSession::DISPLAYNAME_FAILURE_RETRY_TIMER[RETRY_TIMERS] = { 0, 10, 30, 120 };
#endif

AUTOID_IMPL(snEvent);
AUTOID_IMPL(snEventSessionHosted);
AUTOID_IMPL(snEventSessionJoined);
AUTOID_IMPL(snEventSessionBotJoined);
AUTOID_IMPL(snEventJoinFailed);
AUTOID_IMPL(snEventSessionBotJoinFailed);
AUTOID_IMPL(snEventAddingGamer);
AUTOID_IMPL(snEventAddedGamer);
AUTOID_IMPL(snEventRemovedGamer);
AUTOID_IMPL(snEventSessionEstablished);
AUTOID_IMPL(snEventSessionMigrateStart);
AUTOID_IMPL(snEventSessionMigrateEnd);
AUTOID_IMPL(snEventSessionAttrsChanged);
AUTOID_IMPL(snEventSessionDestroyed);
AUTOID_IMPL(snEventSessionDisplayNameRetrieved);
AUTOID_IMPL(snEventRemovedFromSession);
AUTOID_IMPL(snEventRemovedDueToStall);

//Allocate memory for an object of type T from the given allocator
// and call the default constructor.
template<typename T>
static T* snNew(sysMemAllocator* allocator, T*& t)
{
    t = (T*) allocator->RAGE_LOG_ALLOCATE(sizeof(T), 0);
    if(t){new(t) T();}

    return t;
}

//Used for sorting rlPeerInfos.
struct PeerPred
{
    bool operator()(const rlPeerInfo& a, const rlPeerInfo& b) const
    {
        return a.GetPeerId() < b.GetPeerId();
    }
};

///////////////////////////////////////////////////////////////////////////////
//  snGamer
///////////////////////////////////////////////////////////////////////////////

snGamer::snGamer()
: m_Peer(NULL)
{
    this->Clear();
}

void
snGamer::Clear()
{
    snAssert(!m_Peer);
    m_GamerInfo.Clear();
    m_SlotType = RL_SLOT_INVALID;
    m_Peer = NULL;
    m_FullyJoined = false;
	m_JoinedToRline = false;
	m_Established = false;
	m_IsPendingDrop = false;
}

const rlGamerInfo&
snGamer::GetGamerInfo() const
{
    return m_GamerInfo;
}

const rlGamerHandle&
snGamer::GetGamerHandle() const
{
    return m_GamerInfo.GetGamerHandle();
}

const rlGamerId&
snGamer::GetGamerId() const
{
    return m_GamerInfo.GetGamerId();
}

snPeer*
snGamer::GetPeer()
{
    return m_Peer;
}

const snPeer*
snGamer::GetPeer() const
{
    return m_Peer;
}

///////////////////////////////////////////////////////////////////////////////
//  snPeer
///////////////////////////////////////////////////////////////////////////////

snPeer::snPeer()
    : m_CxnId(-1)
	, m_EndpointId(NET_INVALID_ENDPOINT_ID)
    , m_NumGamers(0)
#if RL_SUPPORT_NETWORK_BOTS
    , m_NumBotGamers(0)
#endif // RL_SUPPORT_NETWORK_BOTS
    , m_IsLocal(false)
	, m_AllowMigrate(true)
{
}

snPeer::~snPeer()
{
    this->Shutdown();
}

void
snPeer::InitLocal(const int localGamerIndex)
{
    snAssert(m_CxnId < 0);
	snAssert(!NET_IS_VALID_ENDPOINT_ID(m_EndpointId));
    snAssert(0 == m_NumGamers);
#if RL_SUPPORT_NETWORK_BOTS
    snAssert(0 == m_NumBotGamers);
#endif // RL_SUPPORT_NETWORK_BOTS
    snVerify(rlPeerInfo::GetLocalPeerInfo(&m_PeerInfo, localGamerIndex));
    m_IsLocal = true;
}

void
snPeer::InitRemote(const int cxnId,
					const EndpointId endpointId,
                    const rlPeerInfo& peerInfo)
{
    snAssert(m_CxnId < 0);
	snAssert(!NET_IS_VALID_ENDPOINT_ID(m_EndpointId));
    snAssert(0 == m_NumGamers);
#if RL_SUPPORT_NETWORK_BOTS
    snAssert(0 == m_NumBotGamers);
#endif // RL_SUPPORT_NETWORK_BOTS
    m_CxnId = cxnId;
	m_EndpointId = endpointId;
    m_PeerInfo = peerInfo;
    m_IsLocal = false;
}

void
snPeer::Shutdown()
{
    snAssert(0 == m_NumGamers);
#if RL_SUPPORT_NETWORK_BOTS
    snAssert(0 == m_NumBotGamers);
#endif // RL_SUPPORT_NETWORK_BOTS
    m_CxnId = -1;
	m_EndpointId = NET_INVALID_ENDPOINT_ID;
    m_PeerInfo.Clear();
    m_IsLocal = false;
	m_AllowMigrate = true;
}

bool
snPeer::IsConnected() const
{
    return m_CxnId >= 0;
}

bool
snPeer::IsLocal() const
{
    return m_IsLocal;
}

bool
snPeer::IsRemote() const
{
    return !this->IsLocal();
}

bool
snPeer::HasGamer(const rlGamerId& gamerId) const
{
    return 0 != this->GetGamer(gamerId);
}

bool
snPeer::HasGamer(const rlGamerHandle& gamerHandle) const
{
    return 0 != this->GetGamer(gamerHandle);
}

unsigned
snPeer::GetGamerCount() const
{
    unsigned numGamers = m_NumGamers;

#if RL_SUPPORT_NETWORK_BOTS
    numGamers += m_NumBotGamers;
#endif // RL_SUPPORT_NETWORK_BOTS

    return numGamers;
}

unsigned
snPeer::GetGamers(rlGamerInfo gamerInfoss[], const unsigned maxGamers) const
{
    unsigned count = 0;
    for(int i = 0; i < (int) m_NumGamers && count < maxGamers; ++i, ++count)
    {
        snAssert(m_Gamers[i]);
        gamerInfoss[count] = m_Gamers[i]->m_GamerInfo;
    }

#if RL_SUPPORT_NETWORK_BOTS
    for(int i = 0; i < (int) m_NumBotGamers && count < maxGamers; ++i, ++count)
    {
        snAssert(m_BotGamers[i]);
        gamerInfoss[count] = m_BotGamers[i]->m_GamerInfo;
    }
#endif // RL_SUPPORT_NETWORK_BOTS

    return count;
}

unsigned
snPeer::GetGamers(rlGamerId gamerIds[], const unsigned maxGamers) const
{
    unsigned count = 0;
    for(int i = 0; i < (int) m_NumGamers && count < maxGamers; ++i, ++count)
    {
        snAssert(m_Gamers[i]);
        gamerIds[count] = m_Gamers[i]->m_GamerInfo.GetGamerId();
    }

#if RL_SUPPORT_NETWORK_BOTS
    for(int i = 0; i < (int) m_NumBotGamers && count < maxGamers; ++i, ++count)
    {
        snAssert(m_BotGamers[i]);
        gamerIds[count] = m_BotGamers[i]->m_GamerInfo.GetGamerId();
    }
#endif // RL_SUPPORT_NETWORK_BOTS

    return count;
}

unsigned
snPeer::GetGamers(rlGamerHandle gamerHandles[], const unsigned maxGamers) const
{
    unsigned count = 0;
    for(int i = 0; i < (int) m_NumGamers && count < maxGamers; ++i, ++count)
    {
        snAssert(m_Gamers[i]);
        gamerHandles[count] = m_Gamers[i]->m_GamerInfo.GetGamerHandle();
    }

#if RL_SUPPORT_NETWORK_BOTS
    for(int i = 0; i < (int) m_NumBotGamers && count < maxGamers; ++i, ++count)
    {
        snAssert(m_BotGamers[i]);
        gamerHandles[count] = m_BotGamers[i]->m_GamerInfo.GetGamerHandle();
    }
#endif // RL_SUPPORT_NETWORK_BOTS

    return count;
}

const rlPeerInfo&
snPeer::GetPeerInfo() const
{
    return m_PeerInfo;
}

u64
snPeer::GetPeerId() const
{
    return m_PeerInfo.GetPeerId();
}

int
snPeer::GetCxnId() const
{
    return m_CxnId;
}

EndpointId
snPeer::GetEndpointId() const
{
    return m_EndpointId;
}

bool 
snPeer::AllowMigration() const
{
	return IsLocal() || m_AllowMigrate;
}

//private:

bool
snPeer::AddGamer(snGamer* gamer)
{
    bool success = false;

    if(snVerify(!this->HasGamer(gamer->m_GamerInfo.GetGamerId()))
        && snVerify(!gamer->m_Peer)
        && snVerify(gamer->m_GamerInfo.GetPeerInfo() == m_PeerInfo)
        && snVerify(m_NumGamers < RL_MAX_LOCAL_GAMERS)
        && snVerify(RL_SLOT_PUBLIC == gamer->m_SlotType
                        || RL_SLOT_PRIVATE == gamer->m_SlotType))
    {
        m_Gamers[m_NumGamers++] = gamer;
        gamer->m_Peer = this;
        success = true;
    }

    return success;
}

#if RL_SUPPORT_NETWORK_BOTS

bool
snPeer::AddBotGamer(snGamer* gamer)
{
    bool success = false;

    if(snVerify(!this->HasGamer(gamer->m_GamerInfo.GetGamerId()))
        && snVerify(!gamer->m_Peer)
        && snVerify(gamer->m_GamerInfo.GetPeerInfo() == m_PeerInfo)
        && snVerify(m_NumBotGamers < RL_MAX_NETWORK_BOTS)
        && snVerify(RL_SLOT_PUBLIC == gamer->m_SlotType
                        || RL_SLOT_PRIVATE == gamer->m_SlotType))
    {
        m_BotGamers[m_NumBotGamers++] = gamer;
        gamer->m_Peer = this;
        success = true;
    }

    return success;
}

#endif // RL_SUPPORT_NETWORK_BOTS

bool
snPeer::RemoveGamer(const rlGamerId& gamerId)
{
    const snGamer* gamer = this->GetGamer(gamerId);
    return gamer && this->RemoveGamer(gamer->GetGamerHandle());
}

bool
snPeer::RemoveGamer(const rlGamerHandle& gamerHandle)
{
    bool success = false;

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        snAssert(m_Gamers[i]);

        if(m_Gamers[i]->m_GamerInfo.GetGamerHandle() == gamerHandle)
        {
            snAssert(this == m_Gamers[i]->m_Peer);
            m_Gamers[i]->m_Peer = NULL;
            m_Gamers[i] = m_Gamers[m_NumGamers-1];
            m_Gamers[m_NumGamers-1] = NULL;
            --m_NumGamers;
            success = true;
            break;
        }
    }

#if RL_SUPPORT_NETWORK_BOTS

    for(int i = 0; i < (int) m_NumBotGamers && !success; ++i)
    {
        snAssert(m_BotGamers[i]);

        if(m_BotGamers[i]->m_GamerInfo.GetGamerHandle() == gamerHandle)
        {
            snAssert(this == m_BotGamers[i]->m_Peer);
            m_BotGamers[i]->m_Peer = NULL;
            m_BotGamers[i] = m_BotGamers[m_NumBotGamers-1];
            m_BotGamers[m_NumBotGamers-1] = NULL;
            --m_NumBotGamers;
            success = true;
            break;
        }
    }

#endif // RL_SUPPORT_NETWORK_BOTS

    snAssert(success);

    return success;
}

snGamer*
snPeer::GetGamer(const rlGamerId& gamerId)
{
    snGamer* gamer = 0;

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        snAssert(m_Gamers[i]);

        if(m_Gamers[i]->m_GamerInfo.GetGamerId() == gamerId)
        {
            gamer = m_Gamers[i];
            break;
        }
    }

#if RL_SUPPORT_NETWORK_BOTS

    for(int i = 0; i < (int) m_NumBotGamers && !gamer; ++i)
    {
        snAssert(m_BotGamers[i]);

        if(m_BotGamers[i]->m_GamerInfo.GetGamerId() == gamerId)
        {
            gamer = m_BotGamers[i];
            break;
        }
    }

#endif // RL_SUPPORT_NETWORK_BOTS

    return gamer;
}

const snGamer*
snPeer::GetGamer(const rlGamerId& gamerId) const
{
    return const_cast<snPeer*>(this)->GetGamer(gamerId);
}

snGamer*
snPeer::GetGamer(const rlGamerHandle& gamerHandle)
{
    snGamer* gamer = 0;

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        snAssert(m_Gamers[i]);

        if(m_Gamers[i]->m_GamerInfo.GetGamerHandle() == gamerHandle)
        {
            gamer = m_Gamers[i];
            break;
        }
    }

#if RL_SUPPORT_NETWORK_BOTS

    for(int i = 0; i < (int) m_NumBotGamers && !gamer; ++i)
    {
        snAssert(m_BotGamers[i]);

        if(m_BotGamers[i]->m_GamerInfo.GetGamerHandle() == gamerHandle)
        {
            gamer = m_BotGamers[i];
            break;
        }
    }

#endif // RL_SUPPORT_NETWORK_BOTS

    return gamer;
}

const snGamer*
snPeer::GetGamer(const rlGamerHandle& gamerHandle) const
{
    return const_cast<snPeer*>(this)->GetGamer(gamerHandle);
}

///////////////////////////////////////////////////////////////////////////////
//  snSession
///////////////////////////////////////////////////////////////////////////////

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT(fmt) "%s(0x%016" I64FMT "x/0x%016" I64FMT "x) " fmt, m_Name, this->GetSessionId(), GetSessionToken().m_Value

bool
snSession::CanHost(const rlNetworkMode netMode)
{
    return RL_NETMODE_OFFLINE == netMode
            || (RL_NETMODE_LAN == netMode
                && rlPeerAddress::HasNetwork())
            || (RL_NETMODE_ONLINE == netMode
                && netHardware::IsOnline());
}

bool
snSession::CanJoin(const rlNetworkMode netMode)
{
    return snSession::CanHost(netMode);
}

unsigned snSession::sm_SelfRemoveStallTime = 0;
bool snSession::sm_ConnectToPeerChecks = true;

void 
snSession::SetSelfRemoveStallTime(const unsigned nSelfRemoveStallTime)
{
	sm_SelfRemoveStallTime = nSelfRemoveStallTime;
}

void 
snSession::SetConnectToPeerChecks(const bool connectToPeerChecks)
{
	sm_ConnectToPeerChecks = connectToPeerChecks;
}

bool
snSession::AllowConnectToPeerChecks()
{
	return sm_ConnectToPeerChecks;
}

snSession::snSession()
    : m_Allocator(NULL)
    , m_CxnMgr(NULL)
    , m_HostPeerId(RL_INVALID_PEER_ID)
    , m_NumGamers(0)
    , m_NumPeers(0)
    , m_NumOccupiedPrivSlots(0)
    , m_NumReservedSlots(0)
	, m_bNeedToUpdateReservations(false)
	, m_uTimeOfLastReservationUpdate(0)
    , m_LastTime(0)
    , m_ChannelId(NET_INVALID_CHANNEL_ID)
    , m_NetworkEnabled(false)
    , m_AcceptingJoinRequests(false)
    , m_PresenceEnabled(false)
    , m_Migrating(false)
    , m_IsInitialized(false)
	, m_IsEstablished(false)
	, m_AllowRemoveTask(false)
#if RSG_DURANGO
	, m_DisplayNameRetrievalTime(0)
	, m_CurrentDisplayNameBackoffTimer(0)
	, m_PendingDisplayNames(0)
	, m_DirtyDisplayNames(false)
#endif
{
    sysMemSet(m_Gamers, 0, sizeof(m_Gamers));
    sysMemSet(m_Peers, 0, sizeof(m_Peers));

    m_RqstHandler.Bind(this, &snSession::OnRequest);
    m_CxnMgrDlgt.Bind(this, &snSession::OnNetEvent);

    OUTPUT_ONLY(m_Name[0] = '\0');
}

snSession::~snSession()
{
    this->Shutdown(false, -1);
}

bool
snSession::Init(sysMemAllocator* allocator,
                const snSessionOwner& owner,
                netConnectionManager* cxnMgr,
                const unsigned channelId,
                const char* OUTPUT_ONLY(name))
{
    snAssert(allocator);
    snAssert(cxnMgr);
    snAssert(owner.GetGamerData.IsBound());
    snAssert(owner.HandleJoinRequest.IsBound());

    bool success = false;

    if(snVerify(!m_IsInitialized))
    {
        rtry
        {
            snAssert(!m_Allocator);
            snAssert(!m_CxnMgr);
            snAssert(channelId <= NET_MAX_CHANNEL_ID);
            snAssert(m_TaskQ.empty());
            snAssert(rlIsInitialized());
            snAssert(!m_NumGamers);
            snAssert(!m_NumPeers);
            snAssert(!m_NumOccupiedPrivSlots);
            snAssert(RL_INVALID_PEER_ID == m_HostPeerId);
            snAssert(!m_LastTime);
            snAssert(NET_INVALID_CHANNEL_ID == m_ChannelId);
            snAssert(!m_NetworkEnabled);
            snAssert(!m_AcceptingJoinRequests);
            snAssert(!m_PresenceEnabled);
            snAssert(!m_Migrating);

            m_Allocator = allocator;
            m_Owner = owner;
            m_CxnMgr = cxnMgr;
            m_ChannelId = channelId;

            rverify(m_RlSession.Init(),
                    catchall,
                    snError("Init :: Error initializing rlSession"));

            for(int i = 0; i < RL_MAX_GAMERS_PER_SESSION; ++i)
            {
                m_GamerPool.push_back(&m_GamerPile[i]);
                m_PeerPool.push_back(&m_PeerPile[i]);
            }

#if !__NO_OUTPUT
            if(name)
            {
                safecpy(m_Name, name);
            }
            else
            {
                m_Name[0] = '\0';
            }
#endif

            m_NumReservedSlots = 0;
			m_bNeedToUpdateReservations = false;

			m_LocalOwnerIndex = -1;

            ClearPresenceAttributeNames();

            m_IsInitialized = success = true;

            snDebug("Init :: Succeeded");
        }
        rcatchall
        {
            snDebug("Init :: Failed");
        }
    }

    return success;
}

void
snSession::Shutdown(bool isSudden, int bailReasonIfSudden)
{
    if(m_IsInitialized)
    {
        snDebug("Shutdown :: Sudden: %s, Exists: %s, Bail Reason (If Sudden): %d", isSudden ? "True" : "False", this->Exists() ? "True" : "False", bailReasonIfSudden);

		// try to notify players remaining in the session
		if(isSudden && this->Exists() && RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalOwnerIndex))
		{
			rlGamerHandle localGamerHandle;
			rlPresence::GetGamerHandle(m_LocalOwnerIndex, &localGamerHandle);

			if(this->IsMember(localGamerHandle) && GetGamerCount() > 1)
			{
				rlGamerId localGamerId;
				// numGamerIds should be 1, or 0 if GetGamerIds fails
				const unsigned numGamerIds = this->GetGamerIds(&localGamerHandle, 1, &localGamerId, 1);

				snMsgRemoveGamersFromSessionCmd rgCmd;
				rgCmd.Reset(this->GetSessionId(), &localGamerId, numGamerIds, bailReasonIfSudden);

				if(m_CxnMgr)
				{
					rlGamerInfo gamerInfo[RL_MAX_GAMERS_PER_SESSION];
					unsigned numGamers = GetGamers(gamerInfo, RL_MAX_GAMERS_PER_SESSION);

					for(unsigned i = 0; i < numGamers; i++)
					{
						if(gamerInfo[i].IsRemote())
						{
							int cxnId = this->GetPeerCxn(gamerInfo[i].GetPeerInfo().GetPeerId());
							EndpointId endpointId = this->GetPeerEndpointId(gamerInfo[i].GetPeerInfo().GetPeerId());

							// if not valid, we'll still be establishing a connection to this peer
							if(cxnId >= 0 && NET_IS_VALID_ENDPOINT_ID(endpointId))
							{
								snDebug("Shutdown :: Sending remove command to %s", gamerInfo->GetName());
								m_CxnMgr->SendOutOfBand(endpointId, GetChannelId(), rgCmd, NET_SEND_IMMEDIATE);
							}
							else
							{
								snDebug("Shutdown :: Cannot send remove command to %s, Invalid EndpointId", gamerInfo->GetName());
							}
						}
					}
				}
			}
		}

        m_Delegator.Clear();

        m_RlSession.Shutdown(isSudden);

        while(!m_TaskQ.empty())
        {
            snTask* task = m_TaskQ.front();
            if(!task->IsFinished())
            {
                task->Finish(snTask::FINISH_CANCELED);
            }

			m_AllowRemoveTask = true;
            this->RemoveTask(task);
			m_AllowRemoveTask = false;
        }

        while(!m_ConcurrentTasks.empty())
        {
            snTask* task = m_ConcurrentTasks.front();
            if(!task->IsFinished())
            {
                task->Finish(snTask::FINISH_CANCELED);
            }

            m_ConcurrentTasks.pop_front();

            if(task->m_DeleteWhenFinished)
            {
                Delete(task, *m_Allocator);
            }
        }

        this->Clear();

        m_GamerPool.clear();
        m_PeerPool.clear();

        m_Allocator = 0;
        m_Owner = snSessionOwner();
        m_CxnMgr = 0;
        m_HostPeerId = RL_INVALID_PEER_ID;
        snAssert(0 == m_NumOccupiedPrivSlots);
        m_LastTime = 0;
        m_ChannelId = NET_INVALID_CHANNEL_ID;
        m_NumReservedSlots = 0;
		m_bNeedToUpdateReservations = false;
        m_NetworkEnabled = false;
        m_AcceptingJoinRequests = false;
        m_PresenceEnabled = false;
        m_Migrating = false;
		m_IsEstablished = false;

		m_LocalOwnerIndex = -1;

        ClearPresenceAttributeNames();

		for(unsigned i = 0; i < RL_MAX_GAMERS_PER_SESSION; i++)
		{
			m_ReservedSlots[i].Clear();
		}
		m_NumReservedSlots = 0;

        m_IsInitialized = false;

        snDebug("Shutdown");
    }
}

bool
snSession::IsInitialized() const
{
	return m_IsInitialized;
}

bool
snSession::Exists() const
{
    return m_RlSession.IsEstablished();
}

bool 
snSession::IsEstablished() const
{
	return Exists() && m_IsEstablished;
}

bool
snSession::IsOnline() const
{
    return m_RlSession.IsOnline();
}

bool
snSession::IsLan() const
{
    return m_RlSession.IsLan();
}

bool
snSession::IsNetwork() const
{
    return this->IsLan() || this->IsOnline();
}

bool
snSession::IsOffline() const
{
    return m_RlSession.IsOffline();
}

bool
snSession::IsMigrating() const
{
    return m_Migrating;
}

unsigned
snSession::GetChannelId() const
{
    return m_ChannelId;
}

void 
snSession::SwapChannelId(unsigned channelId)
{
    //Can only be called when we already have a valid channel ID
    snAssert(channelId <= NET_MAX_CHANNEL_ID);
    snAssert(NET_INVALID_CHANNEL_ID != m_ChannelId);

    //Disable the network and join requests
    DisableNetwork();
    SetAcceptingJoinRequests(false);

    //Now swap channel and enable network and join requests
    m_ChannelId = channelId;

    EnableNetwork();
    SetAcceptingJoinRequests(true);
}

const rlSession* 
snSession::GetRlSession() const
{
	//Required for session query callback
	return &m_RlSession;
}

const rlSessionInfo&
snSession::GetSessionInfo() const
{
    snAssert(this->Exists());
    return m_RlSession.GetSessionInfo();
}

const void* 
snSession::GetSessionHandle() const
{
	snAssert(this->Exists());
	return m_RlSession.GetSessionHandle();
}

bool
snSession::GetHostPeerId(u64* peerId) const
{
    bool success = false;
    *peerId = RL_INVALID_PEER_ID;

    if(this->Exists())
    {
        if(RL_INVALID_PEER_ID != m_HostPeerId)
        {
            *peerId = m_HostPeerId;
            success = true;
        }
    }

    return success;
}

bool 
snSession::IsHostPeerId(u64 peerId) const
{
	// make sure that comparing two RL_INVALID_PEER_ID values doesn't succeed
	if(RL_INVALID_PEER_ID == m_HostPeerId)
	{
		return false;
	}

	return (peerId == m_HostPeerId);
}

bool
snSession::GetHostPeerInfo(rlPeerInfo* peerInfo) const
{
    bool success = false;

    peerInfo->Clear();

    if(this->Exists())
    {
        const snPeer* peer = this->FindPeerById(m_HostPeerId);

        if(peer)
        {
            *peerInfo = peer->m_PeerInfo;
            success = true;
        }
    }

    return success;
}

bool
snSession::GetHostPeerAddress(rlPeerAddress* peerAddr) const
{
    bool success = false;

    rlPeerInfo peerInfo;
    if(this->GetHostPeerInfo(&peerInfo))
    {
        *peerAddr = peerInfo.GetPeerAddress();
        success = true;
    }

    return success;
}

bool
snSession::GetHostEndpointId(EndpointId* endpointId) const
{
    bool success = false;

    *endpointId = NET_INVALID_ENDPOINT_ID;

    if(this->Exists())
    {
        const snPeer* peer = this->FindPeerById(m_HostPeerId);

        if(peer)
        {
            *endpointId = peer->m_EndpointId;
            success = true;
        }
    }

    return success;
}

bool
snSession::IsHostEndpointId(const EndpointId endpointId) const
{
	if(endpointId == NET_INVALID_ENDPOINT_ID)
		return false;

	EndpointId hostEndpointId;
	GetHostEndpointId(&hostEndpointId);
	
	return (endpointId == hostEndpointId);
}

const rlPeerInfo&
snSession::GetLocalPeerInfo() const
{
    snAssert(this->Exists());
    return m_LocalPeer.m_PeerInfo;
}

u64
snSession::GetLocalPeerId() const
{
    snAssert(this->Exists());
    return m_LocalPeer.GetPeerId();
}

const rlGamerInfo&
snSession::GetOwner() const
{
    return m_RlSession.GetOwner();
}

u64
snSession::GetSessionId() const
{
    return m_RlSession.GetId();
}

const rlSessionToken
snSession::GetSessionToken() const
{
	if(Exists())
	{
		return m_RlSession.GetSessionInfo().GetToken();
	}

	return rlSessionToken();
}

bool
snSession::IsHost() const
{
    return this->Exists()
            && (m_LocalPeer.GetPeerId() == m_HostPeerId);
}

netConnectionManager*
snSession::GetCxnMgr()
{
    return m_CxnMgr;
}

const netConnectionManager*
snSession::GetCxnMgr() const
{
    return m_CxnMgr;
}

void 
snSession::SetOwner(const snSessionOwner& owner)
{
    m_Owner = owner;
}

void 
snSession::SetPresenceAttributeNames(const char* attrSessionToken,
									 const char* attrSessionId,
									 const char* attrSessionInfo,
									 const char* attrIsHost,
									 const char* attrIsJoinable,
									 const bool updateAttributes)
{
	if(attrSessionToken)
	{
		safecpy(m_AttrSessionToken, attrSessionToken);
	}
	else
	{
		m_AttrSessionToken[0] = '\0';
	}

	if(attrSessionId)
	{
		safecpy(m_AttrSessionId, attrSessionId);
	}
	else
	{
		m_AttrSessionId[0] = '\0';
	}

    if(attrSessionInfo)
    {
        safecpy(m_AttrSessionInfo, attrSessionInfo);
    }
    else
    {
        m_AttrSessionInfo[0] = '\0';
    }

    if(attrIsHost)
    {
        safecpy(m_AttrIsHost, attrIsHost);
    }
    else
    {
        m_AttrIsHost[0] = '\0';
    }

    if(attrIsJoinable)
    {
        safecpy(m_AttrIsJoinable, attrIsJoinable);
    }
    else
    {
        m_AttrIsJoinable[0] = '\0';
    }

    if(updateAttributes)
    {
        UpdatePresenceAttributes(m_LocalOwnerIndex, !Exists(), IsHost());
    }
}

void
snSession::ClearPresenceAttributeNames()
{
#if RSG_PC
	// Clear the additional session info on shutdown
	if (g_rlPc.IsInitialized())
	{
		if (m_AttrSessionInfo[0] != '\0' && g_rlPc.GetAdditionalSessionAttr()[0] != '\0' && strcmp(m_AttrSessionInfo, g_rlPc.GetAdditionalSessionAttr()) == 0)
		{
			g_rlPc.GetPresenceManager()->SetAdditionalSessionInfo("");
		}
	}
#endif

	m_AttrSessionToken[0] = '\0';
	m_AttrSessionId[0] = '\0';
	m_AttrSessionInfo[0] = '\0';
	m_AttrIsHost[0] = '\0';
	m_AttrIsJoinable[0] = '\0';
}

bool
snSession::Host(const int localGamerIndex,
                const rlNetworkMode netMode,
                const int maxPubSlots,
                const int maxPrivSlots,
                const rlMatchingAttributes& attrs,
                const unsigned createFlags,
                const void* ownerData,
                const unsigned sizeofOwnerData,
                netStatus* status)
{
    bool success = false;

    snHostSessionTask* task = NULL;

    rtry
    {
        rverify(snSession::CanHost(netMode), catchall,);

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task,
                                    this,
                                    localGamerIndex,
                                    netMode,
                                    maxPubSlots,
                                    maxPrivSlots,
                                    attrs,
                                    createFlags,
                                    ownerData,
                                    sizeofOwnerData,
                                    status), catchall,);

        this->AddTask(task);

        m_PresenceEnabled = (0 == (createFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED));

        success = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }


    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

bool
snSession::Join(const int localGamerIndex,
                const rlSessionInfo& sessionInfo,
                rlNetworkMode netMode,
                const rlSlotType slotType,
                const unsigned createFlags,
                const void* joinerData,
                const unsigned sizeofJoinerData,
                const rlGamerHandle* groupMembers,
                const unsigned numGroupMembers,
                netStatus* status)
{
    snAssert(RL_NETMODE_LAN == netMode
            || RL_NETMODE_ONLINE == netMode);

    bool success = false;

    snJoinSessionTask* task = NULL;

    rtry
    {
        rlGamerInfo ginfo;
        rverify(rlPresence::GetGamerInfo(localGamerIndex, &ginfo),catchall, snError("No gamer info!"));
        rverify(!this->IsMember(ginfo.GetGamerId()), catchall, snError("Already a member of this session!"));

        rverify(snSession::CanJoin(RL_NETMODE_LAN)
                || snSession::CanJoin(RL_NETMODE_ONLINE), catchall, snError("Cannot join!"));

        // build join group
		rlGamerHandle group[RL_MAX_GAMERS_PER_SESSION];
		unsigned groupSize;

		if(numGroupMembers > 0)
		{
			rverify(numGroupMembers <= RL_MAX_GAMERS_PER_SESSION,
				catchall,
				snError("Too many group members: %d", numGroupMembers));

			groupSize = numGroupMembers;
			for(int i = 0; i < (int)numGroupMembers; i++)
			{
				group[i] = groupMembers[i];
			}

			bool joinerInGroup = false;
			for(int i = 0; i < (int)groupSize; ++i)
			{
				if(ginfo.GetGamerHandle() == groupMembers[i])
				{
					joinerInGroup = true;
					break;
				}
			}

			rverify(joinerInGroup,
				catchall,
				snError("Joiner must be in the group"));
		}
		else
		{
			rverify(rlPresence::GetGamerHandle(localGamerIndex, &group[0]),
				catchall,
				snError("No gamer signed in at index: %d", localGamerIndex));
			groupSize = 1;
		}

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task,
                                        this,
                                        localGamerIndex,
                                        sessionInfo,
                                        netMode,
                                        slotType,
                                        createFlags,
                                        joinerData,
                                        sizeofJoinerData,
                                        group,
                                        groupSize,
                                        status), catchall,);

        this->AddTask(task);

        this->SetAcceptingJoinRequests(false);

        m_PresenceEnabled = (0 == (createFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED));

        success = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

bool 
snSession::IsJoining()
{
	// this is crude, but simple
	TaskQ::iterator it = m_TaskQ.begin();
	TaskQ::iterator stop = m_TaskQ.end();

	for(; stop != it; ++it)
	{
		if(snJoinSessionTask::ISA(*it))
		{
			return true;
		}
	}

	return false;
}

bool
snSession::JoinRemoteBot(const rlGamerInfo& joiner,
                         const rlSessionInfo& sessionInfo,
                         rlNetworkMode netMode,
                         const rlSlotType slotType,
                         const void* joinerData,
                         const unsigned sizeofJoinerData,
                         netStatus* status)
{
    snAssert(RL_NETMODE_LAN == netMode
            || RL_NETMODE_ONLINE == netMode);

    bool success = false;

    snJoinRemoteSessionBotTask* task = NULL;

    rtry
    {
        rverify(!this->IsMember(joiner.GetGamerId()), catchall,);

        rverify(snSession::CanJoin(RL_NETMODE_LAN)
                || snSession::CanJoin(RL_NETMODE_ONLINE), catchall,);

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task,
                                        this,
                                        joiner,
                                        sessionInfo,
                                        netMode,
                                        slotType,
                                        joinerData,
                                        sizeofJoinerData,
                                        status), catchall,);

        this->AddTask(task);

        this->SetAcceptingJoinRequests(false);

        success = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

bool
snSession::JoinLocalBot(const rlGamerInfo& joiner,
                     const rlSessionInfo& sessionInfo,
                     rlNetworkMode netMode,
                     const rlSlotType slotType,
                     const void* joinerData,
                     const unsigned sizeofJoinerData,
                     netStatus* status)
{
    bool success = false;

    snJoinLocalSessionBotTask* task = NULL;

    rtry
    {
        rverify(!this->IsMember(joiner.GetGamerId()), catchall,);

        rverify(snSession::CanJoin(netMode)
                , catchall,);

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task,
                                        this,
                                        joiner,
                                        sessionInfo,
                                        slotType,
                                        joinerData,
                                        sizeofJoinerData,
                                        status), catchall,);

        this->AddTask(task);

        success = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    return success;
}

bool
snSession::Destroy(netStatus* status)
{
    bool success = false;

    snDestroyTask* task = NULL;

    rtry
    {
        rverify(this->Exists(), catchall,);

        this->SetAcceptingJoinRequests(false);

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task, this, status), catchall,);

        this->AddTask(task);

        success = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

bool
snSession::Leave(const int localGamerIndex, netStatus* status)
{
	snDebug("Leave");

    rlGamerHandle gh;
    const bool success =
        snVerify(this->Exists())
        && snVerify(rlPresence::GetGamerHandle(localGamerIndex, &gh))
        && snVerify(this->IsMember(gh))
        && this->DropGamers(&m_LocalPeer,
                            &gh,
                            1,
                            KICK_INFORM_PEERS,
                            NULL,
							-1,
                            status);

    if(!success && status){status->SetPending(); status->SetFailed();}

    if(gh == this->GetOwner().GetGamerHandle())
    {
        this->SetAcceptingJoinRequests(false);
    }

    return success;
}

bool
snSession::LeaveLocalBot(const rlGamerHandle &botGamerHandle, netStatus* status)
{
    const bool success =
        snVerify(this->Exists())
        && snVerify(this->IsMember(botGamerHandle))
        && this->DropGamers(&m_LocalPeer,
                            &botGamerHandle,
                            1,
                            KICK_INFORM_PEERS,
                            NULL,
							-1,
                            status);

    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

bool
snSession::Kick(const rlGamerHandle* kickees,
                const unsigned numKickees,
                const KickType kickType,
                netStatus* status)
{
    snAssert(numKickees <= RL_MAX_GAMERS_PER_SESSION);

    bool success = false;

    if(snVerify(this->Exists())
        && snVerify(this->IsHost()))
    {
        rlGamerHandle tmp[RL_MAX_GAMERS_PER_SESSION];
        unsigned count = 0;

        for(int i = 0; i < (int) numKickees && i < COUNTOF(tmp); ++i)
        {
            if(!kickees[i].IsLocal())
            {
                tmp[count++] = kickees[i];
            }
        }

        if(count > 0)
        {
            success = this->DropGamers(&m_LocalPeer,
                                        tmp,
                                        count,
                                        kickType,
                                        NULL,
										-1,
                                        status);
        }
        else
        {
            success = true;
            if(status){status->SetPending(); status->SetSucceeded();}
        }
    }

    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

bool
snSession::ChangeAttributes(const rlMatchingAttributes& newAttrs, netStatus* status)
{
    bool success = false;

    snChangeAttributesTask* task = NULL;

    rtry
    {
        //Only the host can call ChangeAttributes() - the host will then
        //send a command to all peers.
        rverify(this->Exists(), catchall,);
        rverify(this->IsHost(), catchall,);

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task, this, newAttrs, status), catchall,);

        this->AddTask(task);
        success = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

bool 
snSession::IsHostPending()
{
	// this is crude, but simple
	TaskQ::iterator it = m_TaskQ.begin();
	TaskQ::iterator stop = m_TaskQ.end();

	for(; stop != it; ++it)
	{
		if(snHostSessionTask::ISA(*it))
		{
			return true;
		}
	}

	return false;
}

bool 
snSession::HasJoinersPending()
{
	// this is crude, but simple
	TaskQ::iterator it = m_TaskQ.begin();
	TaskQ::iterator stop = m_TaskQ.end();

	for(; stop != it; ++it)
	{
		if(snJoinGamersToRlineTask::ISA(*it))
		{
			snJoinGamersToRlineTask* pTempTask = static_cast<snJoinGamersToRlineTask*>(*it);
			if(!pTempTask->IsWaitingForAddGamers())
			{
				return true; 
			}
		}
	}

	return false;
}

bool
snSession::EnableMatchmakingAdvertising(netStatus* status)
{
	bool success = false;

	snEnableMatchmakingAdvertiseTask* task = NULL;

	rtry
	{
		rverify(this->Exists(), catchall,);

		task = snNew(m_Allocator, task);

		rverify(task, catchall,);

		rverify(rlTaskBase::Configure(task, this, status), catchall,);

		this->AddTask(task);
		success = true;
	}
	rcatchall
	{
		if(task){Delete(task, *m_Allocator);}
		task = NULL;
	}

	if(!success && status){status->SetPending(); status->SetFailed();}

	return success;
}

bool
snSession::SetPresenceEnabled(const bool enabled, netStatus* status)
{
    bool success = false;

    snEnableXblPresenceTask* task = NULL;

    rtry
    {
        rverify(!(this->GetConfig().m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED),
                catchall,
                snError("Session was created with no presence.  Therefore presence can't be enabled/disabled"));

        rverify(enabled || this->GetConfig().m_MaxPublicSlots == 0,
                catchall,
                snError("Disabling presence is supported only for private sessions"));

        m_PresenceEnabled = enabled;
        if(status){status->ForceSucceeded();}
        success = true;

    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    if(!success && status){status->ForceFailed();}

    return success;
}

bool
snSession::IsPresenceEnabled() const
{
    return m_PresenceEnabled;
}

bool
snSession::ModifyPresenceFlags(unsigned presenceFlags, netStatus* status)
{
    bool success = false;

    if(m_RlSession.GetPresenceFlags() == presenceFlags)
    {
        if(status){status->SetPending(); status->SetSucceeded();}
        success = true;
    }
    else
    {
        snModifyPresenceFlagsTask* task = NULL;

        rtry
        {
            rverify(this->Exists(), catchall,);
            rverify(!(this->GetConfig().m_CreateFlags & RL_SESSION_CREATE_FLAG_PRESENCE_DISABLED),
                    catchall,
                    snError("Can't change the presence flags on a session created with the CREATE_NO_PRESENCE flag"));
            rverify(this->IsHost(),
                    catchall,
                    snError("Only the host can set presence flags"));
            rverify(!this->IsEstablished()
                    || !(this->GetConfig().m_CreateFlags & RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED),
                    catchall,
                    snError("Can't change the presence settings on a JIP-disabled session if the match is in progress"));

            task = snNew(m_Allocator, task);

            rverify(task, catchall,);

            rverify(rlTaskBase::Configure(task, this, presenceFlags, status), catchall,);

            this->AddTask(task);
            success = true;
        }
        rcatchall
        {
            if(task){Delete(task, *m_Allocator);}
            task = NULL;
        }

        if(!success && status){status->SetPending(); status->SetFailed();}
    }

    return success;
}

unsigned
snSession::GetPresenceFlags() const
{
	return m_RlSession.GetPresenceFlags();
}

bool
snSession::IsInvitable() const
{
    return m_RlSession.IsInvitable()
            //Can't send invites unless presence is enabled.
            && this->IsPresenceEnabled()
            //If we're the host, be sure we're accepting join requests.
            && (!this->IsHost() || this->IsAcceptingJoinRequests())
            //If JIP is disabled be sure we're not in progress.
            && !((m_RlSession.GetCreateFlags() & RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED)
                    && this->IsEstablished())
            //Make sure we have empty slots.
            && (this->GetEmptySlots(RL_SLOT_PUBLIC) > 0
                || this->GetEmptySlots(RL_SLOT_PRIVATE) > 0);
}

bool
snSession::IsJoinableViaPresence() const
{
    // very similar to IsInvitable() but we need to have open public slots
    return m_RlSession.IsJoinableViaPresence()
            //Can't JVP unless presence is enabled.
            && this->IsPresenceEnabled()
            //If we're the host, be sure we're accepting join requests.
            && (!this->IsHost() || this->IsAcceptingJoinRequests())
            //If JIP is disabled be sure we're not in progress.
            && !((m_RlSession.GetCreateFlags() & RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED)
                    && this->IsEstablished())
            //Make sure we have empty slots.
            && (this->GetEmptySlots(RL_SLOT_PUBLIC) > 0);
}

bool
snSession::SendInvites(const rlGamerHandle* gamerHandles,
                        const unsigned numGamers,
                        const char* subject,
                        const char* salutation,
                        netStatus* status)
{
    bool success = false;

    snSendInvitesTask* task = NULL;

    rtry
    {
        rcheck(numGamers > 0,
                catchall,
                snWarning("Invalid attempt to invite zero gamers"));

        rverify(numGamers < RL_MAX_GAMERS_PER_SESSION,
                catchall,
                snError("Invalid attempt to invite too many (%d) gamers",
                            numGamers));

        rcheck(this->IsInvitable(),
                catchall,
                snWarning("Not sending invites - session is not invitable"));

        rlGamerHandle tmpHandles[RL_MAX_GAMERS_PER_SESSION];
        unsigned tmpCount = 0;

        //Make sure we're not inviting someone who is already in the session.
        for(int i = 0; i < (int) numGamers; ++i)
        {
            bool haveit = false;
            for(int j = 0; j < (int) m_NumGamers; ++j)
            {
                if(gamerHandles[i] == m_Gamers[j]->GetGamerHandle())
                {
                    haveit = true;
                    break;
                }
            }

            if(!haveit)
            {
                tmpHandles[tmpCount++] = gamerHandles[i];
            }
        }

        rcheck(tmpCount > 0,
                catchall,
                snWarning("Not sending invites - all gamers are already in the session"));

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task, this, tmpHandles, tmpCount, subject, salutation, status),
                catchall,);

        this->AddTask(task);
        success = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

bool
snSession::SendPartyInvites(const int localGamerIndex,
                       netStatus* status)
{
    bool success = false;

    snSendPartyInvitesTask* task = NULL;

    rtry
    {
        rverify(RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex),catchall,);

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task, this, localGamerIndex, status),
            catchall,);

        this->AddTask(task);
        success = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

bool
snSession::Migrate()
{
    snAssert(!m_Migrating);

    bool success = false;

    rlFireAndForgetTask<snMigrateSessionTask>* task = NULL;

    rtry
    {
        rverify(this->Exists(), catchall,);
        rverify(!m_Migrating, catchall,);

        m_HostPeerId = RL_INVALID_PEER_ID;

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task,
                                    this,
                                    &task->m_Status),
                                    catchall,);

        //Notify the app that a session migration has started.
        snEventSessionMigrateStart e;
        DispatchEvent(&e);

        this->AddTaskLifo(task);

        //Don't accept join requests while migrating.
        this->SetAcceptingJoinRequests(false);

        success = m_Migrating = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    return success;
}

void
snSession::CancelMigrationTasks()
{
    //If we have a pending snMigrateSessionTask in the task queue, cancel it
    bool foundMigrateTask = false;
    do
    {
        foundMigrateTask = false; 

        TaskQ::iterator it = m_TaskQ.begin();
        TaskQ::iterator stop = m_TaskQ.end();

        for(; stop != it; ++it)
        {
            if(snMigrateSessionTask::ISA(*it))
            {
                if(!(*it)->IsFinished())
                {
                    (*it)->Finish(snTask::FINISH_CANCELED);
                    
                    //Flag that we should look for more snMigrateSessionTask entries
                    foundMigrateTask = true; 
                    break;
                }
            }
        }
    }
    while(foundMigrateTask);
}

void 
snSession::MarkAsNotMigrating(const u64 peerId)
{
	if(peerId == RL_INVALID_PEER_ID)
	{
		return;
	}

	// prevent this peer from being considered in migration tasks
	snPeer* peer = this->FindPeerById(peerId);
	if(peer)
	{
		// do not migrate this peer
		if(peer->m_AllowMigrate)
		{
			snDebug("MarkAsNotMigrating :: Blocking migrate to PeerID: 0x%" I64FMT "x [" NET_ADDR_FMT "]",
					peer->GetPeerId(),
					NET_IS_VALID_ENDPOINT_ID(peer->GetEndpointId()) ? NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(peer->GetEndpointId())) : "");
			peer->m_AllowMigrate = false;
		}
	}

	// remove as a candidate from any active tasks
	TaskQ::iterator it = m_TaskQ.begin();
	TaskQ::iterator stop = m_TaskQ.end();
	for(; stop != it; ++it)
	{
		if(snMigrateSessionTask::ISA(*it))
		{
			if(!(*it)->IsFinished())
			{
				snMigrateSessionTask* migrateTask = static_cast<snMigrateSessionTask*>(*it);
				migrateTask->RemoveCandidate(peerId);
			}
		}
	}
}

bool 
snSession::DoesAllowMigration(const u64 peerId)
{
	snPeer* peer = this->FindPeerById(peerId);
	return peer && peer->m_AllowMigrate;
}

void
snSession::CancelAllSerialTasks()
{
	snDebug("CancelAllSerialTasks");

	snTask* task;

	TaskQ::iterator it = m_TaskQ.begin();
	TaskQ::iterator stop = m_TaskQ.end();

	for(; stop != it; ++it)
	{
		task = *it;
		if(!task->IsFinished())
		{
			task->Finish(snTask::FINISH_CANCELED);
		}
	}
}

void
snSession::CancelAllConcurrentTasks()
{
	snDebug("CancelAllConcurrentTasks");

	while(!m_ConcurrentTasks.empty())
	{
		snTask* task = m_ConcurrentTasks.front();
		if(!task->IsFinished())
		{
			task->Finish(snTask::FINISH_CANCELED);
		}

		m_ConcurrentTasks.pop_front();

		if(task->m_DeleteWhenFinished)
		{
			Delete(task, *m_Allocator);
		}
	}
}

void 
snSession::MarkAsEstablished(const rlGamerHandle& hGamer)
{
	snGamer* gamer = this->FindGamerByHandle(hGamer);
	if(gamer)
	{
		// do not migrate this peer
		if(!gamer->m_Established)
		{
			snDebug("MarkAsEstablished :: %s", gamer->m_GamerInfo.GetName());
			gamer->m_Established = true;
		}
	}
}

bool 
snSession::IsGamerEstablished(const rlGamerHandle& hGamer)
{
	snGamer* gamer = this->FindGamerByHandle(hGamer);
	return gamer && gamer->m_Established;
}

bool
snSession::ReserveSlots(const rlGamerHandle* gamerHandles,
						const unsigned numGamerHandles,
						const rlSlotType slotType,
						const unsigned reservationTimeout,
						bool bIsJoinRequest,
						unsigned userParam)
{
	//Check for joiners who already have a slot reservation
	unsigned numSlotsRequired = 0;
	for(int i = 0; i < (int)numGamerHandles; ++i)
	{
		if(!this->HasSlotReservation(gamerHandles[i]) && !IsMember(gamerHandles[i]))
		{
			++numSlotsRequired;
		}
	}

	snDebug2("ReserveSlots :: NumGamers: %u, NumSlotsRequired: %u Type: %s", numGamerHandles, numSlotsRequired, slotType == RL_SLOT_PUBLIC ? "PUBLIC" : "PRIVATE");

	const unsigned expiry = sysTimer::GetSystemMsTime() + reservationTimeout;
	
	if(0 == numSlotsRequired)
	{
		//Renew expiration for existing reservations
		for(int i = 0; i < (int)numGamerHandles; ++i)
		{
			//Do we already have a reserved slot for this guy?
			ReservedSlot* rs = FindSlotReservation(gamerHandles[i]);
			if(rs)
			{
				rs->m_ExpiryMs = expiry;
				if(userParam != 0)
				{
					rs->m_UserParam = userParam;
				}
			}
		}

		//Everyone already has a reserved slot.
		return true;
	}
	else if(GetAvailableSlots(slotType) >= numSlotsRequired)
	{
		for(int i = 0; i < (int)numGamerHandles; ++i)
		{
			//Do we already have a reserved slot for this guy?
			ReservedSlot* rs = FindSlotReservation(gamerHandles[i]);
			if(rs)
			{
				//Renew the expiration
				rs->m_ExpiryMs = expiry;
				rs->m_UserParam = userParam;
			}
			else
			{
				if(snVerify(m_NumReservedSlots < RL_MAX_GAMERS_PER_SESSION))
				{
					OUTPUT_ONLY(char ghStr[32]);
					snDebug2("ReserveSlots :: For %s", gamerHandles[i].ToString(ghStr,32));
					m_ReservedSlots[m_NumReservedSlots++].Reset(gamerHandles[i], slotType, expiry, userParam);
				}
				else
				{
					snError("ReserveSlots :: No free slots! NumReserved: %u! MaxPub: %u, EmptyPub: %u, ReservedPub: %u, MaxPriv: %u, EmptyPriv: %u, ReservedPriv: %u", 
                        m_NumReservedSlots,
                        m_RlSession.GetMaxSlots(RL_SLOT_PUBLIC),
                        GetEmptySlots(RL_SLOT_PUBLIC),
                        NumReservedSlots(RL_SLOT_PUBLIC),
                        m_RlSession.GetMaxSlots(RL_SLOT_PRIVATE),
                        GetEmptySlots(RL_SLOT_PRIVATE),
                        NumReservedSlots(RL_SLOT_PRIVATE));
				}
			}
		}

		// Join requests already handle updating the SC advertisement
		if((IsHost() || IsHostPending()) && !bIsJoinRequest)
		{
			m_bNeedToUpdateReservations = true;
		}

		snDebug2("ReserveSlots :: Successful, NumReservedSlots: %u, NeedToUpdateReservations: %s", m_NumReservedSlots, m_bNeedToUpdateReservations ? "True" : "False");
		return true;
	}

	snDebug("ReserveSlots :: Failed - Reserved %u slots (%u required). Max: %u, Empty: %u, Reserved: %u, OccupiedPriv: %u", m_NumReservedSlots, numSlotsRequired, GetMaxSlots(slotType), GetEmptySlots(slotType), NumReservedSlots(slotType), m_NumOccupiedPrivSlots);
	return false;
}

bool
snSession::HasSlotReservation(const rlGamerHandle& gamerHandle) const
{
	return (NULL != FindSlotReservation(gamerHandle));
}

unsigned 
snSession::GetNumSlotsWithParam(unsigned userParam)
{
	unsigned count = 0;
	for(int i = 0; i < (int)m_NumReservedSlots; ++i)
	{
		if(m_ReservedSlots[i].m_UserParam == userParam)
		{
			count++;
		}
	}
	return count;
}

void 
snSession::FreeSlots(const rlGamerHandle* gamerHandles,
					 const unsigned numGamerHandles)
{
	for(int i = 0; i < (int)m_NumReservedSlots; ++i)
	{
		for(int j = 0; j < (int)numGamerHandles; ++j)
		{
			if(m_ReservedSlots[i].m_GamerHandle == gamerHandles[j])
			{
				OUTPUT_ONLY(char ghStr[32]);
				snDebug2("Freeing slot reservation for gamer:%s",
					m_ReservedSlots[i].m_GamerHandle.ToString(ghStr, 32));
				m_ReservedSlots[i] = m_ReservedSlots[m_NumReservedSlots-1];
				--m_NumReservedSlots;
				--i;
				m_bNeedToUpdateReservations = true;
			}
		}
	}
}

void
snSession::Update(const unsigned curTime)
{
    if(m_IsInitialized)
    {
        m_RlSession.Update();

		if(this->Exists() && sm_SelfRemoveStallTime > 0 && ((curTime - m_LastTime) > sm_SelfRemoveStallTime))
		{
			// notify the app
			snEventRemovedDueToStall evt;
			m_Delegator.Dispatch(this, &evt);

			// remove from session
			this->RemoveLocalFromSession();
		}
       
        unsigned timeStep = 0;
        if(m_LastTime && curTime > m_LastTime)
        {
            timeStep = curTime - m_LastTime;
        }
        m_LastTime = curTime | 0x01;

        if(m_NetworkEnabled)
        {
            m_Transactor.Update(timeStep);
        }

        this->UpdateTasks(timeStep);

        this->UpdateReservedSlots();

		// avoid lots of debug spew by setting the "joinable" attribute only if it differs from the current value
		if((m_AttrIsJoinable[0] != '\0') && Exists())
		{
			int localGamerIndex = GetOwner().GetLocalIndex();

			bool isJoinable;
			if(rlPresence::IsOnline(localGamerIndex) &&
				(!rlPresenceAttributeHelper::GetBool(localGamerIndex, isJoinable, m_AttrIsJoinable) || IsJoinableViaPresence() != isJoinable))
			{
				rlPresenceAttributeHelper::SetBool(localGamerIndex, IsJoinableViaPresence(), m_AttrIsJoinable);
			}
		}

#if RSG_PC
		if(g_rlPc.IsInitialized())
		{
			if (strcmp(m_AttrIsJoinable, rlScAttributeId::IsGameJoinable.Name) == 0)
			{
				g_rlPc.GetPresenceManager()->AllowGameInvites(IsInvitable());
			}
			if (m_AttrIsJoinable[0] != '\0' && strcmp(m_AttrIsJoinable, g_rlPc.GetAdditionalJoinAttr()) == 0)
			{
				g_rlPc.GetPresenceManager()->AllowAdditionalSessionGameInvites(IsInvitable());
			}
		}
#endif

#if RSG_DURANGO
		this->UpdateDisplayNames();
#endif
    }
}

const rlSessionConfig&
snSession::GetConfig() const
{
    return m_RlSession.GetConfig();
}

bool
snSession::GetGamerInfo(const rlGamerId& gamerId,
                        rlGamerInfo* gamerInfo) const
{
    bool success = false;

    gamerInfo->Clear();
    const snGamer* gamer = this->FindGamerById(gamerId);

    if(gamer)
    {
        *gamerInfo = gamer->GetGamerInfo();
        success = true;
    }

    return success;
}

bool
snSession::GetGamerInfo(const rlGamerHandle& gamerHandle,
                        rlGamerInfo* gamerInfo) const
{
    bool success = false;

    gamerInfo->Clear();
    const snGamer* gamer = this->FindGamerByHandle(gamerHandle);

    if(gamer)
    {
        *gamerInfo = gamer->GetGamerInfo();
        success = true;
    }

    return success;
}

unsigned
snSession::GetGamers(const u64 peerId,
                    rlGamerInfo gamers[],
                    const unsigned maxGamers) const
{
    const snPeer* peer = this->FindPeerById(peerId);
    return peer ? peer->GetGamers(gamers, maxGamers) : 0;
}

unsigned
snSession::GetGamers(const u64 peerId,
                    rlGamerId gamerIds[],
                    const unsigned maxGamers) const
{
    const snPeer* peer = this->FindPeerById(peerId);
    return peer ? peer->GetGamers(gamerIds, maxGamers) : 0;
}

unsigned
snSession::GetGamers(const u64 peerId,
                    rlGamerHandle gamerHandles[],
                    const unsigned maxGamers) const
{
    const snPeer* peer = this->FindPeerById(peerId);
    return peer ? peer->GetGamers(gamerHandles, maxGamers) : 0;
}

unsigned
snSession::GetGamers(const EndpointId endpointId,
	rlGamerInfo gamers[],
	const unsigned maxGamers) const
{
	const snPeer* peer = this->FindPeerByEndpointId(endpointId);
	return peer ? peer->GetGamers(gamers, maxGamers) : 0;
}

unsigned
snSession::GetGamers(const EndpointId endpointId,
	rlGamerId gamerIds[],
	const unsigned maxGamers) const
{
	const snPeer* peer = this->FindPeerByEndpointId(endpointId);
	return peer ? peer->GetGamers(gamerIds, maxGamers) : 0;
}

unsigned
snSession::GetGamers(const EndpointId endpointId,
	rlGamerHandle gamerHandles[],
	const unsigned maxGamers) const
{
	const snPeer* peer = this->FindPeerByEndpointId(endpointId);
	return peer ? peer->GetGamers(gamerHandles, maxGamers) : 0;
}

unsigned
snSession::GetGamers(rlGamerInfo gamers[], const unsigned maxGamers) const
{
    unsigned count;

    for(count = 0; count < m_NumGamers && count < maxGamers; ++count)
    {
        gamers[count] = m_Gamers[count]->m_GamerInfo;
    }

    snAssert(this->GetGamerCount() > maxGamers
            || this->GetGamerCount() == count);

    return count;
}

unsigned
snSession::GetGamers(rlGamerId gamerIds[], const unsigned maxGamers) const
{
    unsigned count;

    for(count = 0; count < m_NumGamers && count < maxGamers; ++count)
    {
        gamerIds[count] = m_Gamers[count]->m_GamerInfo.GetGamerId();
    }

    snAssert(this->GetGamerCount() > maxGamers
            || this->GetGamerCount() == count);

    return count;
}

unsigned
snSession::GetGamers(rlGamerHandle gamerHandles[], const unsigned maxGamers) const
{
    unsigned count;

    for(count = 0; count < m_NumGamers && count < maxGamers; ++count)
    {
        gamerHandles[count] = m_Gamers[count]->m_GamerInfo.GetGamerHandle();
    }

    snAssert(this->GetGamerCount() > maxGamers
            || this->GetGamerCount() == count);

    return count;
}

unsigned
snSession::GetGamerHandles(const rlGamerId gamerIds[],
                            const unsigned numGamerIds,
                            rlGamerHandle gamerHandles[],
                            const unsigned maxGamers) const
{
    unsigned numGamerHandles = 0;
    for(int i = 0; i < (int)numGamerIds && i < (int)maxGamers; ++i)
    {
        const snGamer* gamer = this->FindGamerById(gamerIds[i]);
        if(gamer)
        {
            gamerHandles[numGamerHandles++] = gamer->GetGamerHandle();
        }
    }

    return numGamerHandles;
}

unsigned
snSession::GetGamerIds(const rlGamerHandle gamerHandles[],
                        const unsigned numGamerHandles,
                        rlGamerId gamerIds[],
                        const unsigned maxGamers) const
{
    unsigned numGamerIds = 0;
    for(int i = 0; i < (int)numGamerHandles && i < (int)maxGamers; ++i)
    {
        const snGamer* gamer = this->FindGamerByHandle(gamerHandles[i]);
        if(gamer)
        {
            gamerIds[numGamerIds++] = gamer->GetGamerId();
        }
    }

    return numGamerIds;
}

unsigned
snSession::GetGamerCount(const u64 peerId) const
{
    const snPeer* peer = this->FindPeerById(peerId);
    return peer ? peer->GetGamerCount() : 0;
}

unsigned
snSession::GetGamerCount() const
{
    return m_NumGamers;
}

unsigned
snSession::GetLocalGamerCount() const
{
    return m_LocalPeer.GetGamerCount();
}

unsigned
snSession::GetMaxSlots(const rlSlotType slotType) const
{
    return m_RlSession.GetMaxSlots(slotType);
}

unsigned
snSession::GetEmptySlots(const rlSlotType slotType) const
{
    unsigned avail = 0;

    const unsigned numPrivSlots = m_RlSession.GetMaxSlots(RL_SLOT_PRIVATE);
    const unsigned numSlots = m_RlSession.GetMaxSlots();

    snAssert(numPrivSlots >= m_NumOccupiedPrivSlots);

    const unsigned numPrivAvail = numPrivSlots - m_NumOccupiedPrivSlots;

    if(RL_SLOT_PUBLIC == slotType)
    {
        const unsigned numAvail = numSlots - this->GetGamerCount();
        avail = numAvail - numPrivAvail;
    }
    else
    {
        avail = numPrivAvail;
    }

    return avail;
}

unsigned
snSession::GetAvailableSlots(const rlSlotType slotType) const
{
    snAssert(GetEmptySlots(slotType) >= NumReservedSlots(slotType));

    return GetEmptySlots(slotType) - NumReservedSlots(slotType);
}

rlSlotType
snSession::GetSlotType(const rlGamerId& gamerId) const
{
    const snGamer* gamer = this->FindGamerById(gamerId);
    return gamer ? gamer->m_SlotType : RL_SLOT_INVALID;
}

rlSlotType
snSession::GetSlotType(const rlGamerHandle& gamerHandle) const
{
    const snGamer* gamer = this->FindGamerByHandle(gamerHandle);
    return gamer ? gamer->m_SlotType : RL_SLOT_INVALID;
}

unsigned
snSession::GetPeerCount() const
{
    return m_NumPeers;
}

bool
snSession::GetPeerEndpointId(const u64 peerId, EndpointId* endpointId) const
{
    bool success = false;

	*endpointId = NET_INVALID_ENDPOINT_ID;

	const snPeer* peer = this->FindPeerById(peerId);
	if(peer)
	{
		*endpointId = peer->GetEndpointId();
		success = true;
	}

	return success;
}

int
snSession::GetPeerCxn(const u64 peerId) const
{
    const snPeer* peer = this->FindPeerById(peerId);

    return NULL != peer ? peer->m_CxnId : NET_INVALID_CXN_ID;
}

EndpointId
snSession::GetPeerEndpointId(const u64 peerId) const
{
	const snPeer* peer = this->FindPeerById(peerId);
	return (peer != NULL) ? peer->m_EndpointId : NET_INVALID_ENDPOINT_ID;
}

bool
snSession::CanSendToPeer(const u64 peerId) const
{
    const snPeer* peer = this->FindPeerById(peerId);

    if(peer)
    {
        return m_CxnMgr->IsOpen(peer->m_CxnId) || m_CxnMgr->IsPendingOpen(peer->m_CxnId);
    }

    return false;
}

bool
snSession::IsMember(const rlGamerHandle& gamerHandle) const
{
    return 0 != this->FindGamerByHandle(gamerHandle);
}

bool
snSession::IsMember(const rlGamerId& gamerId) const
{
    return 0 != this->FindGamerById(gamerId);
}

void
snSession::AddDelegate(Delegate* dlgt)
{
    m_Delegator.AddDelegate(dlgt);
}

void
snSession::RemoveDelegate(Delegate* dlgt)
{
    m_Delegator.RemoveDelegate(dlgt);
}

bool
snSession::SetQosUserData(const u8* data,
                          const u16 size)
{
    return m_RlSession.SetQosUserData(data, size);
}

bool
snSession::SetSessionUserData(const u8* data,
						      const u16 size)
{
	return m_RlSession.SetSessionUserData(data, size);
}

#if RSG_ORBIS
const rlSceNpSessionId& snSession::GetWebApiSessionId() const
{
	return m_RlSession.GetSessionInfo().GetWebApiSessionId();
}

void snSession::SetWebApiSessionId(const rlSceNpSessionId& sessionId)
{
	m_RlSession.SetWebApiSessionId(sessionId);
}

#endif

u64 snSession::GetPeerIdByCxn(int cxnId) const
{
    const snPeer *pPeer = FindPeerByCxn(cxnId);
    if (pPeer)
        return pPeer->GetPeerId();

    return RL_INVALID_PEER_ID;
}

u64 snSession::GetPeerIdByEndpointId(const EndpointId endpointId) const
{
    const snPeer *pPeer = FindPeerByEndpointId(endpointId);
    if (pPeer)
	{
		return pPeer->GetPeerId();
	}

    return RL_INVALID_PEER_ID;
}

void
snSession::Cancel(netStatus* status)
{
    rlGetTaskManager()->CancelTask(status);
}

bool
snSession::HasInFlightDisplayNameTasks()
{
#if RSG_DURANGO
	TaskQ::iterator it = m_ConcurrentTasks.begin();
	TaskQ::iterator stop = m_ConcurrentTasks.end();
	for(; stop != it; ++it)
	{
		if(snNotifyAddGamerTask::ISA(*it))
		{
			return true;
		}
	}
#endif
	return false;
}

void
snSession::CancelDisplayNameTask(const rlGamerHandle& DURANGO_ONLY(handle))
{
#if RSG_DURANGO
	// if we're in the process of adding this gamer, we need to cancel that 
	TaskQ::iterator it = m_ConcurrentTasks.begin();
	TaskQ::iterator stop = m_ConcurrentTasks.end();
	for(; stop != it; ++it)
	{
		if(snNotifyAddGamerTask::ISA(*it))
		{
			snNotifyAddGamerTask* pNotifyTask = static_cast<snNotifyAddGamerTask*>(*it);
			if(pNotifyTask->GetGamerHandle() == handle)
			{
				pNotifyTask->Cancel();
			}
		}
	}
#endif
}

void 
snSession::CancelDisplayNameTasks()
{
#if RSG_DURANGO
	// Cancel all snNotifyAddGamerTask tasks
	TaskQ::iterator it = m_ConcurrentTasks.begin();
	TaskQ::iterator stop = m_ConcurrentTasks.end();
	for(; stop != it; ++it)
	{
		if(snNotifyAddGamerTask::ISA(*it))
		{
			snNotifyAddGamerTask* pNotifyTask = static_cast<snNotifyAddGamerTask*>(*it);
			pNotifyTask->Cancel();
		}
	}
#endif
}

void 
snSession::NotifyDisplayNameTasksAddedPlayer()
{
#if RSG_DURANGO
	// Cancel all snNotifyAddGamerTask tasks
	TaskQ::iterator it = m_ConcurrentTasks.begin();
	TaskQ::iterator stop = m_ConcurrentTasks.end();
	for(; stop != it; ++it)
	{
		if(snNotifyAddGamerTask::ISA(*it))
		{
			snNotifyAddGamerTask* pNotifyTask = static_cast<snNotifyAddGamerTask*>(*it);
			pNotifyTask->DoNotAddPlayer();
		}
	}
#endif
}

//private:

void
snSession::EnableNetwork()
{
    if(!m_NetworkEnabled)
    {
        snDebug("EnableNetwork");
        m_Transactor.Init(m_CxnMgr);
        m_Transactor.AddRequestHandler(&m_RqstHandler, m_ChannelId);
        m_CxnMgr->AddChannelDelegate(&m_CxnMgrDlgt, m_ChannelId);
        m_NetworkEnabled = true;
    }
}

void
snSession::DisableNetwork()
{
    if(m_NetworkEnabled)
    {
        snDebug("DisableNetwork");
        m_Transactor.Shutdown();
        m_CxnMgr->RemoveChannelDelegate(&m_CxnMgrDlgt);
        m_NetworkEnabled = false;
    }
}

void
snSession::SetAcceptingJoinRequests(const bool accepting)
{
    if(this->Exists() && this->IsHost())
    {
        if(accepting)
        {
            m_CxnMgr->StartListening(m_ChannelId);
        }
        else
        {
            //Don't call StopListening().  It globally disables
            //this channel for anyone else who might be listening.
            //m_CxnMgr->StopListening(m_ChannelId);
        }

        m_AcceptingJoinRequests = accepting;
    }
    else
    {
        m_AcceptingJoinRequests = false;
    }
}

bool
snSession::IsAcceptingJoinRequests() const
{
    //Check that the session owner is still a member because
    //he might be in the process of leaving.
    bool accepting =
        this->Exists()
        && this->IsHost()
        && this->IsNetwork()
        && this->IsMember(m_RlSession.GetOwner().GetGamerId())
        && !this->IsMigrating()
        //No joining if JIP is disabled and the match has started.
        && !((m_RlSession.GetCreateFlags() & RL_SESSION_CREATE_FLAG_JOIN_IN_PROGRESS_DISABLED)
                && this->IsEstablished())
        && m_AcceptingJoinRequests;

    return accepting;
}

unsigned
snSession::GetHostCandidates(rlPeerInfo (&candidates)[RL_MAX_GAMERS_PER_SESSION])
{
    //Collect an array of candidates for new host.

    int numCandidates = 0;

    snAssert(m_NumPeers < RL_MAX_GAMERS_PER_SESSION);

    if(m_Owner.GetHostMigrationCandidates.IsBound())
    {
        numCandidates =
            m_Owner.GetHostMigrationCandidates(this,candidates, COUNTOF(candidates));
        if(!snVerify(numCandidates <= COUNTOF(candidates)))
        {
            numCandidates = 0;
        }
    }

    if(0 == numCandidates)
    {
        //Make sure we're a candidate.
        candidates[numCandidates++] = this->GetLocalPeerInfo();

        for(int i = 0; i < (int) m_NumPeers && numCandidates < RL_MAX_GAMERS_PER_SESSION; ++i)
        {
            const snPeer* peer = m_Peers[i];

            if(peer->GetCxnId() >= 0)
            {
				// the connection closed event might be pending
				if(!m_CxnMgr->IsClosed(peer->GetCxnId()))
				{
					candidates[numCandidates++] = peer->m_PeerInfo;
				}
            }
        }

        //Sort candidates by peer id.
        if(numCandidates > 1)
        {
            std::sort(&candidates[0], &candidates[numCandidates], PeerPred());
        }
    }

    return numCandidates;
}

snPeer*
snSession::AddPeer(const int cxnId,
					const EndpointId endpointId,
                    const rlPeerInfo& peerInfo)
{
    snPeer* peer = NULL;

    if(snVerify(cxnId >= 0)
        && snVerify(!m_CxnMgr->IsClosed(cxnId)))
    {
        snDebug("Adding peer:" NET_ADDR_FMT "...", NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(endpointId)));

        if(snVerify(!this->HasPeer(peerInfo.GetPeerId()))
            && !m_PeerPool.empty())
        {
            snAssert(!this->FindPeerByCxn(cxnId));

            peer = *m_PeerPool.begin();
            m_PeerPool.pop_front();

            peer->InitRemote(cxnId, endpointId, peerInfo);
            m_Peers[m_NumPeers++] = peer;
        }
    }

    return peer;
}

void
snSession::RemovePeer(snPeer* peer)
{
    snAssert(0 == peer->GetGamerCount());
    snAssert(this->HasPeer(peer->m_PeerInfo.GetPeerId()));

    if(&m_LocalPeer != peer)
    {
		if(NET_IS_VALID_ENDPOINT_ID(peer->GetEndpointId()))
		{
			snDebug("Removing peer:" NET_ADDR_FMT "...",
				NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(peer->GetEndpointId())));
		}
		else
		{
			snDebug("Removing peer with invalid endpointId...");
		}

        this->DisconnectPeer(peer);

        snAssert(peer->GetCxnId() < 0);

        OUTPUT_ONLY(bool removed = false);

        for(int i = 0; i < (int) m_NumPeers; ++i)
        {
            if(m_Peers[i] == peer)
            {
                m_Peers[i] = m_Peers[m_NumPeers-1];
                m_Peers[m_NumPeers-1] = NULL;
                --m_NumPeers;
                OUTPUT_ONLY(removed = true);
                break;
            }
        }

        snAssert(removed);

        peer->Shutdown();

        m_PeerPool.push_back(peer);
    }
    else
    {
        snDebug("Removing peer:local...");
    }
}

void
snSession::RemovePeer(const u64 peerId)
{
    snPeer* peer = this->FindPeerById(peerId);

    if(peer)
    {
        this->RemovePeer(peer);
    }
}

bool
snSession::DropPeer(const u64 peerId,
                    snLeaveGamersFromRlineTask* task,
                    netStatus* status)
{
    bool success = false;

    rtry
    {
        rverify(this->Exists(), catchall,);
        rverify(this->IsHost(), catchall,);

        snPeer* peer = this->FindPeerById(peerId);

        rcheck(peer,
                DoesNotExist,
                snError("DropPeer :: Could not find peer. This may be a peer whose join request was rejected and subsequently disconnected, and can be ignored."));

        this->DisconnectPeer(peer);

        rlGamerHandle gamerHandles[snPeer::MAX_GAMERS_PER_PEER];
        const unsigned numGamers = peer->GetGamers(gamerHandles, snPeer::MAX_GAMERS_PER_PEER);

        if(numGamers)
        {
            rcheck(this->DropGamers(&m_LocalPeer,
                                    gamerHandles,
                                    numGamers,
                                    KICK_INFORM_PEERS,
                                    task,
									-1,
                                    status),
                    catchall,);
        }
        else
        {
            this->RemovePeer(peer);

            if(status){status->ForceSucceeded();}
        }

        //Should no longer have this peer.
        snAssert(!this->HasPeer(peerId));

        success = true;
    }
    rcatch(DoesNotExist)
    {
        //The peer doesn't exist - that's ok.  There are situations where
        //a peer list is obtained and shortly after the peer is dropped.
        //Then the stale peer list iterated over and used to call DropPeer().
        success = true;
    }
    rcatchall
    {
        if(status)
        {
            status->SetPending(); 
            status->SetFailed();
        }
    }

    return success;
}

void
snSession::DisconnectPeer(snPeer* peer)
{
    snAssert(&m_LocalPeer != peer);
    snAssert(this->HasPeer(peer->m_PeerInfo.GetPeerId()));
	
#if !__NO_OUTPUT
	if(NET_IS_VALID_ENDPOINT_ID(peer->GetEndpointId()))
	{
		snDebug("DisconnectPeer :: Peer:" NET_ADDR_FMT "...", NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(peer->GetEndpointId())));
	}
	else
	{
		snDebug("DisconnectPeer :: Peer with invalid endpointId...");
	}

    for(int i = 0; i < (int) peer->m_NumGamers; ++i)
    {
        snDebug("\tDisconnecting gamer:\"%s\" (%" I64FMT "u)",
                    peer->m_Gamers[i]->m_GamerInfo.GetName(),
                    peer->m_Gamers[i]->m_GamerInfo.GetGamerId().GetId());
    }
#endif  //!__NO_OUTPUT

    const int peerCxnId = peer->GetCxnId();

    if(peerCxnId >= 0)
    {
        m_CxnMgr->CloseConnection(peerCxnId, NET_CLOSE_GRACEFULLY);
        peer->m_CxnId = NET_INVALID_CXN_ID;
		peer->m_EndpointId = NET_INVALID_ENDPOINT_ID;

        if(peer->GetPeerId() == m_HostPeerId && !m_Migrating)
        {
            m_HostPeerId = RL_INVALID_PEER_ID;

            if(this->Exists()
                //The case where we disconnect the host and we're not
                //a member occurs when we're joining and there's an error
                //or the host rejects us.  In that case we don't want
                //to do a host migration.
                && this->IsMember(this->GetOwner().GetGamerId()))
            {
				bool skipMigrate = false;
				if(m_Owner.ShouldMigrate.IsBound())
				{
					if(!m_Owner.ShouldMigrate())
					{
						//App does not want to migrate.

						snDebug("DisconnectPeer :: Host peer disconnected. Not migrating, app said no");
						skipMigrate = true; 
					}
				}
				
				if(!skipMigrate)
				{
					//FIXME (KB) - handle failure here.
                    snVerify(this->Migrate());
				}
				else
				{
					snDebug("DisconnectPeer :: Not Migrating - Informing remote peers");
					snMsgNotMigrating cmd;
					cmd.Reset(this->GetSessionId());
					this->BroadcastCommand(cmd, &m_LocalPeer);
				}
            }
        }
    }
}

void
snSession::DisconnectPeer(const u64 peerId)
{
    snPeer* peer = this->FindPeerById(peerId);

    if(peer)
    {
        this->DisconnectPeer(peer);
    }
}

bool
snSession::HasPeer(const u64 peerId) const
{
    return NULL != this->FindPeerById(peerId);
}

bool
snSession::IsConnectedToPeer(const u64 peerId) const
{
    return this->GetPeerCxn(peerId) >= 0;
}

void
snSession::HandleJoinRequest(const snMsgJoinRequest& rqst,
                             const netAddress& sender,
                             const netSequence seq)
{
#if !__NO_OUTPUT
	snDebug("HandleJoinRequest :: Group Members: %u", rqst.m_GroupSize);
	for(unsigned i = 0; i < rqst.m_GroupSize; ++i)
	{
		char buf[RL_MAX_GAMER_HANDLE_CHARS] = {0};
		snDebug("\t%s", rqst.m_GroupMembers[i].ToString(buf));
	}
#endif

    if(!snVerify(this->IsHost()))
    {
    }
    //If we received a request for another session then just
    //ignore it.  This could occur if we're running multiple
    //sessions.
    else if(this->GetSessionId() == rqst.m_SessionId)
    {
		unsigned numJoiners = rqst.m_GroupSize;
		// Subtract reserved joiners already in the session
		if (numJoiners > 1)
		{
			for (unsigned int i = 0; i < rqst.m_GroupSize; i++)
			{
				if (IsMember(rqst.m_GroupMembers[i]))
				{
					numJoiners--;
				}
			}
		}

        //m_GroupSize includes the current joiner.
        if(numJoiners > 1
            && this->GetAvailableSlots(rqst.m_SlotType) < numJoiners
            && this->GetAvailableSlots(RL_SLOT_PUBLIC) < numJoiners)
        {
            snDebug("HandleJoinRequest :: Denying group join request from:" NET_ADDR_FMT " - no available slots",
                        NET_ADDR_FOR_PRINTF(sender));

            this->DenyJoinRequest(sender,
                                    SNET_JOIN_DENIED_NO_EMPTY_SLOTS,
                                    NULL,
                                    0);
        }
        else if(1 == numJoiners
                && this->GetAvailableSlots(rqst.m_SlotType) < numJoiners
                && !this->HasSlotReservation(rqst.m_GamerInfo.GetGamerHandle())
                //If the requested slot type is private and no private slots
                //are available then we'll try to put the joiner in a public slot
                && this->GetAvailableSlots(RL_SLOT_PUBLIC) < numJoiners)
        {
            //If this is a session with available private slots, and the request is for a public slot...
			bool privateOnly = (this->GetAvailableSlots(RL_SLOT_PRIVATE) > 0) && (rqst.m_SlotType == RL_SLOT_PUBLIC);

			snDebug("HandleJoinRequest :: Denying join request from:" NET_ADDR_FMT " - %s",
				NET_ADDR_FOR_PRINTF(sender), privateOnly ? "private only" : "no available slots");

            this->DenyJoinRequest(sender,
                                    privateOnly ? SNET_JOIN_DENIED_PRIVATE : SNET_JOIN_DENIED_NO_EMPTY_SLOTS,
                                    NULL,
                                    0);
        }
        else if(!this->IsAcceptingJoinRequests())
        {
            snDebug("HandleJoinRequest :: Denying join request from:" NET_ADDR_FMT " - not accepting peers",
                        NET_ADDR_FOR_PRINTF(sender));

            this->DenyJoinRequest(sender,
                                    SNET_JOIN_DENIED_NOT_JOINABLE,
                                    NULL,
                                    0);
        }
        else if(this->IsMember(rqst.m_GamerInfo.GetGamerId()))
        {
            snDebug("HandleJoinRequest :: Denying join request from:" NET_ADDR_FMT " - %s (id) is already in the session",
					NET_ADDR_FOR_PRINTF(sender),
					rqst.m_GamerInfo.GetName());

            this->DenyJoinRequest(sender,
								  SNET_JOIN_DENIED_ALREADY_JOINED,
								  NULL,
								  0);
        }
		else if(this->IsMember(rqst.m_GamerInfo.GetGamerHandle()))
		{
			snDebug("HandleJoinRequest :: Denying join request from:" NET_ADDR_FMT " - %s (handle) is already in the session",
					NET_ADDR_FOR_PRINTF(sender),
					rqst.m_GamerInfo.GetName());

			this->DenyJoinRequest(sender,
								  SNET_JOIN_DENIED_ALREADY_JOINED,
								  NULL,
								  0);
		}
        else
        {
            snAssert(rqst.m_GroupSize > 0);

            snJoinRequest jr(sender,
                            rqst.m_SlotType,
                            rqst.m_Data,
                            rqst.m_SizeofData,
                            rqst.m_GroupSize);

			bool appSaysNo = false;
            if(m_Owner.HandleJoinRequest.IsBound())
			{
				if(!m_Owner.HandleJoinRequest(this, rqst.m_GamerInfo, &jr))
				{
					appSaysNo = true;
				}
			}

			if(appSaysNo)
            {
                snDebug("HandleJoinRequest :: Denying join request from:" NET_ADDR_FMT " - app said no",
                            NET_ADDR_FOR_PRINTF(sender));

                this->DenyJoinRequest(sender,
                                    SNET_JOIN_DENIED_APP_SAID_NO,
                                    (jr.m_SizeofResponseData ? jr.m_ResponseData : NULL),
                                    jr.m_SizeofResponseData);
            }
            else if(!this->ReserveSlots(rqst.m_GroupMembers, rqst.m_GroupSize, rqst.m_SlotType, SLOT_RESERVATION_TTL_MS, true)
                    //If we fail to reserve the requested slot type then try to
                    //reserve public slots.
                    && !this->ReserveSlots(rqst.m_GroupMembers, rqst.m_GroupSize, RL_SLOT_PUBLIC, SLOT_RESERVATION_TTL_MS, true))
            {
                snDebug("HandleJoinRequest :: Denying join request from:" NET_ADDR_FMT " - failed to reserve slots",
                            NET_ADDR_FOR_PRINTF(sender));

                this->DenyJoinRequest(sender,
                                        SNET_JOIN_DENIED_NO_EMPTY_SLOTS,
                                        NULL,
                                        0);
            }
            else
            {
                snDebug("HandleJoinRequest :: Accepting join request from:" NET_ADDR_FMT ", relay: " NET_ADDR_FMT "",
                            NET_ADDR_FOR_PRINTF(sender),
							NET_ADDR_FOR_PRINTF(rqst.m_RelayAddr));

                this->AcceptJoinRequest(rqst,
                                        sender,
                                        seq,
                                        jr.m_ResponseData,
                                        jr.m_SizeofResponseData);
            }
        }
    }
}

void
snSession::AcceptJoinRequest(const snMsgJoinRequest& rqst,
                            const netAddress& sender,
                            const netSequence seq,
                            const void* responseData,
                            const unsigned sizeofResponseData)
{
    snAssert(this->Exists() && this->IsHost());

    snDebug("AcceptJoinRequest :: Received %s for: \"%s\" from:" NET_ADDR_FMT "",
            rqst.GetMsgName(),
            rqst.m_GamerInfo.GetName(),
            NET_ADDR_FOR_PRINTF(sender));

    snJoinResponseCode denyCode = SNET_JOIN_DENIED_NOT_JOINABLE;

    int cxnId = NET_INVALID_CXN_ID;
    snPeer* peer = NULL;
    bool peerAlreadyExists = false;

	bool existingJoinTask = false;
	snHandleJoinRequestTask* task = NULL;
	snJoinGamersToRlineTask* joinGamersTask = NULL;
	netStatus* status = NULL;

    rtry
    {
        rcheck(this->GetSessionId() == rqst.m_SessionId, catchall,);

		// Get or create snJoinGamersToRlineTask 
		joinGamersTask = GetJoinGamersToRlineTask(existingJoinTask);
		rverify(joinGamersTask, catchall,);

        snMsgJoinRequest tmpRqst = rqst;

        //If the joiner reserved a slot then use the
        //slot type from the reservation.
        const ReservedSlot* rs = this->FindSlotReservation(tmpRqst.m_GamerInfo.GetGamerHandle());
        if(rs)
        {
            tmpRqst.m_SlotType = rs->m_SlotType;
        }
        else if(!this->GetAvailableSlots(tmpRqst.m_SlotType))
        {
            //No slots of the requested type available.
            //If the joiner requested a private slot then try
            //to put him in a public slot.
            if(RL_SLOT_PUBLIC == tmpRqst.m_SlotType)
            {
                denyCode = SNET_JOIN_DENIED_NO_EMPTY_SLOTS;

                rthrow(catchall,
                        snDebug("AcceptJoinRequest :: Rejecting %s - no public slots available",
                                    rqst.GetMsgName()));
            }
            //Requested slot type was PRIVATE, but no private slots are
            //available, so check for public slots.
            else if(!this->GetAvailableSlots(RL_SLOT_PUBLIC))
            {
                denyCode = SNET_JOIN_DENIED_NO_EMPTY_SLOTS;

                rthrow(catchall,
                        snDebug("AcceptJoinRequest :: Rejecting %s - no public or private slots available",
                                    rqst.GetMsgName()));
            }

            tmpRqst.m_SlotType = RL_SLOT_PUBLIC;
        }

        snAssert(this->GetEmptySlots(tmpRqst.m_SlotType));

        // check if this is a join request from a new peer or for a new gamer from an existing peer
        const rlPeerInfo& peerInfo = rqst.m_GamerInfo.GetPeerInfo();

        if(!this->HasPeer(peerInfo.GetPeerId()))
        {
            cxnId = m_CxnMgr->AcceptConnection(peerInfo.GetPeerAddress(), sender, m_ChannelId, seq, NULL);

            rcheck(cxnId >= 0,
                    catchall,
                    snError("AcceptJoinRequest :: Rejecting %s from peer:0x%016" I64FMT "x - no more connections",
                                rqst.GetMsgName(),
                                peerInfo.GetPeerId()));

            //Create the peer object
            peer = this->AddPeer(cxnId, m_CxnMgr->GetEndpointId(cxnId), peerInfo);

            rcheck(peer,
                    catchall,
                    snError("AcceptJoinRequest :: Error allocating peer instance for peer:0x%016" I64FMT "x",
                            peerInfo.GetPeerId()));
        }
        else
        {
            peerAlreadyExists = true;
            cxnId = this->GetPeerCxn(peerInfo.GetPeerId());

            rcheck(cxnId >= 0,
                    catchall,
                    snError("AcceptJoinRequest :: Invalid connection ID retrieved for peer:0x%016" I64FMT "x",
                            peerInfo.GetPeerId()));
        }

		//The status member is provided by the join task
		rverify(joinGamersTask->AddGamer(tmpRqst.m_GamerInfo.GetGamerHandle(),
									     tmpRqst.m_GamerInfo.GetGamerId(),
									     tmpRqst.m_SlotType,
										 true,
									     &status),
				catchall,);

		rcheck(status, catchall, );

        task = snNew(m_Allocator, task);

        rverify(task, catchall,);

        rverify(rlTaskBase::Configure(task, this, tmpRqst, peerAlreadyExists, status), catchall,);

        snVerifyf(this->FillSlotReservation(tmpRqst.m_GamerInfo.GetGamerHandle()),
                "AcceptJoinRequest :: No slot reservation");

        rverify(this->AddGamer(tmpRqst.m_GamerInfo,
                                tmpRqst.m_SlotType,
                                tmpRqst.m_SizeofData ? tmpRqst.m_Data : NULL,
                                tmpRqst.m_SizeofData), catchall,);

        //Send the join response.

        //Ask the host for the host's version of the gamer's data
        //and send it back to the gamer.  This provides for cases
        //where the host is responsible for setting some portion
        //of the gamer's data.
        snGetGamerData ggd;
        if(m_Owner.GetGamerData.IsBound())
        {
            m_Owner.GetGamerData(this, tmpRqst.m_GamerInfo, &ggd);
        }

        snMsgJoinResponse resp;

        resp.Reset(this->GetSessionId(),
                    SNET_JOIN_ACCEPTED,
                    tmpRqst.m_SlotType,
                    ggd.m_Data,
                    ggd.m_SizeofData,
                    responseData,
                    sizeofResponseData);

        m_CxnMgr->Send(cxnId, resp, NET_SEND_RELIABLE, NULL);

        this->AddConcurrentTask(task);

		if(status->Pending() && !existingJoinTask)
		{
			//Add to serial queue. Waits for snHandleJoinRequestTask to complete
			this->AddTask(joinGamersTask);
		}
    }
    rcatchall
    {
        this->DenyJoinRequest(sender, denyCode, NULL, 0);

        if(!peerAlreadyExists)
        {
            if(cxnId >= 0)
            {
                m_CxnMgr->CloseConnection(cxnId, NET_CLOSE_IMMEDIATELY);
                cxnId = NET_INVALID_CXN_ID;
            }

            if(peer)
            {
                this->RemovePeer(peer);
            }
        }

        if(task)
        {
            Delete(task, *m_Allocator);
            task = NULL;
        }

		if(joinGamersTask)
		{
			if(!existingJoinTask)
			{
				Delete(joinGamersTask, *m_Allocator);
				joinGamersTask = NULL;
			}
			else if(status)
			{
				status->SetPending();
				status->SetFailed();
			}
    }
}
}

void
snSession::DenyJoinRequest(const netAddress& sender,
                            const snJoinResponseCode responseCode,
                            const void* responseData,
                            const unsigned sizeofResponseData)
{
    snDebug("DenyJoinRequest :: Denying join request from:" NET_ADDR_FMT "",
                NET_ADDR_FOR_PRINTF(sender));

    const int cxnId = m_CxnMgr->GetConnectionId(sender, m_ChannelId);

    snMsgJoinResponse resp;

    resp.Reset(this->GetSessionId(),
                responseCode,
                RL_SLOT_PUBLIC,
                NULL,
                0,
                responseData,
                sizeofResponseData);

    if(cxnId < 0)
    {
        u8 msgBuf[256];
        unsigned sizeofMsg = 0;
        if(snVerify(resp.Export(msgBuf, sizeof(msgBuf), &sizeofMsg)))
        {
            m_CxnMgr->DenyConnection(sender, m_ChannelId, msgBuf, sizeofMsg);
        }
    }
    else
    {
        m_CxnMgr->Send(cxnId, resp, NET_SEND_RELIABLE, NULL);
    }
}

void 
snSession::HandleHostLeftWhileJoiningCmd(const netRequest&,
                                         const snMsgHostLeftWhilstJoiningCmd&)
{
    if(snVerify(m_HostPeerId != RL_INVALID_PEER_ID))
    {
        snPeer* peer = FindPeerById(m_HostPeerId);
        if(snVerify(peer))
        {
            snDebug("HandleHostLeftWhileJoiningCmd :: Host peer:" NET_ADDR_FMT " left whilst joining ...",
                     NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(peer->GetEndpointId())));

            //We might get a remove gamer command, but we won't have the gamer
            //to disconnect since the host was never added. Handle this directly. 
            DisconnectPeer(peer);
        }
    }
    else
    {
        //Catch this with logging
        snError("Host peer left whilst joining but m_HostPeerId == RL_INVALID_PEER_ID");
    }
}

void
snSession::HandleRemoveGamerCmd(const snPeer* requestingPeer,
                                const snMsgRemoveGamersFromSessionCmd& cmd)
{
	snDebug("HandleRemoveGamerCmd :: Remove reason: %d", cmd.m_RemoveReason);

#if !__NO_OUTPUT
    for(int i = 0; i < (int) cmd.m_NumGamers; ++i)
    {
		snGamer* gamer = FindGamerById(cmd.m_GamerIds[i]);
        snDebug("HandleRemoveGamerCmd :: Removing gamer: Name: %s, Id: %" I64FMT "u...", gamer ? gamer->GetGamerInfo().GetName() : "Invalid", cmd.m_GamerIds[i].GetId());
    }
#endif

    if(requestingPeer)
    {
		
#if !__NO_OUTPUT
		// retrieve gamers associated with peer
		rlGamerInfo info[RL_MAX_GAMERS_PER_SESSION];
		unsigned nGamers = GetGamers(requestingPeer->GetPeerId(), info, RL_MAX_GAMERS_PER_SESSION);

		// log gamers associated with peer
		for(unsigned i = 0; i < nGamers; ++i)
		{
			snDebug("HandleRemoveGamerCmd :: Requesting Peer: %s", info[i].GetName());
		}

		if(requestingPeer->GetPeerId() == m_HostPeerId)
		{
			snDebug("HandleRemoveGamerCmd :: Requesting peer is host"); 
		}
#endif

		if(this->IsHost())
        {
			unsigned numOwned = 0;

            //Make sure the requesting peer owns the gamers.
            for(int i = 0; i < (int) cmd.m_NumGamers; ++i)
            {
				const snGamer* gamer = requestingPeer->GetGamer(cmd.m_GamerIds[i]);
                if(gamer)
                {
					snWarning("HandleRemoveGamerCmd :: Gamer %s owned by peer", gamer->GetGamerInfo().GetName());
					++numOwned;
                }
                else
                {
                    snWarning("HandleRemoveGamerCmd :: Gamer %" I64FMT "u not owned by peer!", cmd.m_GamerIds[i].GetId());
                }
            }

            if(cmd.m_NumGamers == numOwned)
            {
				rlGamerHandle gamerHandles[RL_MAX_GAMERS_PER_SESSION];
                const unsigned numGamers = this->GetGamerHandles(cmd.m_GamerIds, cmd.m_NumGamers, gamerHandles, COUNTOF(gamerHandles));

                this->DropGamers(requestingPeer,
                                gamerHandles,
                                numGamers,
                                KICK_INFORM_PEERS,
                                NULL,
								cmd.m_RemoveReason,
                                NULL);
            }
            else
            {
                snAssert(m_Migrating);

                //This typically occurs during a host migration where
                //two peers are competing for host duties.  The competing
                //host is trying to tell us to remove gamers that are no
                //longer part of the session.  The session will end up split,
                //us hosting and the competing peer also hosting.
                snError("HandleRemoveGamerCmd :: Not removing gamers - requesting peer doesn't own at least one of the gamers.");
            }
        }
		else if(requestingPeer->GetPeerId() == m_HostPeerId)
        {
            //If any gamers are local that means they were kicked.
            //Instead of removing the local gamers, remove all non-local
            //gamers, leaving us in our own session.

			bool anyLocal = false;
			bool anyFound = false;
            for(int i = 0; i < (int) cmd.m_NumGamers; ++i)
            {
                const snGamer* gamer = this->FindGamerById(cmd.m_GamerIds[i]);

                if(gamer)
                {
					anyFound = true;
					if(gamer->GetGamerInfo().IsLocal())
					{
						anyLocal = true;
					}
                    break;
                }
            }

			// also check players that we're in the process of adding
			if(!anyFound)
			{
				TaskQ::iterator it = m_TaskQ.begin();
				TaskQ::iterator stop = m_TaskQ.end();

				for(; stop != it && !anyFound; ++it)
				{
					if(!(*it)->IsFinished() && snJoinGamersToRlineTask::ISA(*it))
					{
						snJoinGamersToRlineTask* joinGamersTask = (snJoinGamersToRlineTask*)*it;
						anyFound = joinGamersTask->IsAdding(cmd.m_GamerIds, cmd.m_NumGamers);
					}
				}
			}

            if(anyLocal)
            {
				snDebug("HandleRemoveGamerCmd :: Gamer is local. Removing everyone else instead");

                rlGamerHandle gamerHandles[RL_MAX_GAMERS_PER_SESSION];
                const unsigned numGamers =
                    this->GetGamers(gamerHandles, COUNTOF(gamerHandles));
                unsigned numDrops = 0;

				// notify the app
				snEventRemovedFromSession evt(numGamers);
				m_Delegator.Dispatch(this, &evt);

                for(int i = 0; i < (int) numGamers; ++i)
                {
                    if(!gamerHandles[i].IsLocal())
                    {
                        gamerHandles[numDrops++] = gamerHandles[i];
                    }
                }

				//Drop any gamers we are adding, we've been kicked
				TaskQ::iterator it = m_TaskQ.begin();
				TaskQ::iterator stop = m_TaskQ.end();

				for(; stop != it; ++it)
				{
					if(!(*it)->IsFinished() && snJoinGamersToRlineTask::ISA(*it))
					{
						snJoinGamersToRlineTask* joinGamersTask = (snJoinGamersToRlineTask*)*it;
						joinGamersTask->DropGamers();
					}
				}

                if(numDrops)
                {
                    this->DropGamers(requestingPeer,
                                    gamerHandles,
                                    numDrops,
                                    KICK_INFORM_PEERS,
                                    NULL,
									-1,
                                    NULL);
                }
            }
            else if(anyFound)
            {
				//If we have a queued snJoinGamersToRlineTask task with
				//the gamers we're trying to remove then abort adding
				//them.
				TaskQ::iterator it = m_TaskQ.begin();
				TaskQ::iterator stop = m_TaskQ.end();

				for(; stop != it; ++it)
				{
					if(!(*it)->IsFinished() && snJoinGamersToRlineTask::ISA(*it))
					{
						//Abort adding the gamers we're about to remove.
						snJoinGamersToRlineTask* joinGamersTask = (snJoinGamersToRlineTask*)*it;
						joinGamersTask->DropGamers(cmd.m_GamerIds, cmd.m_NumGamers);
					}
				}

                rlFireAndForgetTask<snDropGamersTask>* task = NULL;
                rlGetTaskManager()->CreateTask(&task);

                if(snVerify(task))
                {
					snDebug("HandleRemoveGamerCmd :: Configuring snDropGamersTask...");

                    if(snVerify(rlTaskBase::Configure(task,
                                                        this,
                                                        requestingPeer->GetPeerId(),
                                                        cmd.m_GamerIds,
                                                        cmd.m_NumGamers,
														cmd.m_RemoveReason,
                                                        &task->m_Status)))
                    {
                        snVerify(rlGetTaskManager()->AddParallelTask(task));
                    }
                    else
                    {
                        rlGetTaskManager()->DestroyTask(task);
                        task = NULL;
                    }
                }
            }
        }
		// requesting peer is another peer
		else
		{
			// check that at least one of the dropped gamers is owned by the peer
			bool requesterOwnsDroppedGamers = false;
			for(int i = 0; i < (int) cmd.m_NumGamers; ++i)
			{
				if(requestingPeer->HasGamer(cmd.m_GamerIds[i]))
				{
					requesterOwnsDroppedGamers = true;
					break;
				}
			}

			if(requesterOwnsDroppedGamers)
			{
				snDebug("HandleRemoveGamerCmd :: Received from peer. Blocking migration");
				MarkAsNotMigrating(requestingPeer->GetPeerId());
			}
			else
			{
				snDebug("HandleRemoveGamerCmd :: Received from peer who is not dropping themselves. Presumably a new host.");
			}
		}
    }
	else
	{
		snDebug("HandleRemoveGamerCmd :: Requesting peer not found"); 
	}
}

void
snSession::HandleGamerInfoRequest(const snMsgRequestGamerInfo& rqst,
                                const int cxnId)
{
    if(!snVerify(this->IsHost()))
    {
    }
    //If we received a request for another session then just
    //ignore it.  This could occur if we're running multiple
    //sessions.
    else if(this->GetSessionId() == rqst.m_SessionId)
    {
        const snGamer* gamer = this->FindGamerById(rqst.m_GamerId);
        if(gamer)
        {
            this->SendAddGamerCommandToPeer(gamer->GetGamerHandle(), cxnId, NULL, 0);
        }
    }
}

void
snSession::InformJoinerOfPeers(const rlGamerInfo& joiner, const unsigned numReservedSlots, ReservedSlot* reservedSlots)
{
    snAssert(this->IsMember(joiner.GetGamerId()));
    snAssert(this->FindGamerById(joiner.GetGamerId())->m_FullyJoined);

    //Inform the joiner of existing peers.

    const snPeer* joinerPeer =
        this->FindPeerById(joiner.GetPeerInfo().GetPeerId());
    snAssert(joinerPeer);

    //Send the host (ourselves) first as the joiner expects
    //the first gamer added to be the host.

    const rlGamerInfo& ownerInfo = m_RlSession.GetOwner();

    snDebug("InformJoinerOfPeers :: Sending %s to:\"%s\" for the host (%s)...",
                snMsgAddGamerToSessionCmd::MSG_NAME(),
                joiner.GetName(),
                ownerInfo.GetName());

	bool hostCmdFailed = false;
    if(this->IsMember(ownerInfo.GetGamerId()))
    {
        if(!this->SendAddGamerCommandToPeer(ownerInfo.GetGamerHandle(), joinerPeer->GetCxnId(), reservedSlots, numReservedSlots))
		{
			snError("InformJoinerOfPeers :: Failed to send add gamer command for host %s!", ownerInfo.GetName()); 
			hostCmdFailed = true; 
		}
    }
    else
    {
        snError("InformJoinerOfPeers :: The host left the game while \"%s\" was joining!", joiner.GetName());

        //Inform the peer that the host has left
        snMsgHostLeftWhilstJoiningCmd cmd;
        cmd.Reset(this->GetSessionId());

        this->m_Transactor.SendRequest(joinerPeer->GetCxnId(), NULL, cmd, 0, NULL);
    }

    snDebug("InformJoinerOfPeers :: Sending %s to:\"%s\" for existing peers...",
                snMsgAddGamerToSessionCmd::MSG_NAME(),
                joiner.GetName());

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        const rlGamerInfo& gamerInfo = m_Gamers[i]->GetGamerInfo();

        //Don't send the host info again (unless it failed above, in which case, try again)
        if(gamerInfo == ownerInfo && !hostCmdFailed)
        {
            continue;
        }

        //Don't send the peer's own gamers back to him.
        if(gamerInfo.GetPeerInfo().GetPeerId() == joiner.GetPeerInfo().GetPeerId())
        {
            continue;
        }

        if(!m_Gamers[i]->m_FullyJoined)
        {
            //This gamer is still in the process of being joined to the session.
            //When he's fully joined we will call InformPeersOfJoiner() and
            //everyone will know about him.
            continue;
        }

        if(!this->SendAddGamerCommandToPeer(gamerInfo.GetGamerHandle(), joinerPeer->GetCxnId()))
		{
			snError("InformJoinerOfPeers :: Failed to send add gamer command for peer %s!", gamerInfo.GetName()); 
		}
    }
}

void
snSession::InformPeersOfJoiner(const rlGamerInfo& joiner, const rlGamerHandle* reserveMembers, const unsigned reserveSize)
{
    snAssert(this->IsMember(joiner.GetGamerId()));
    snAssert(this->FindGamerById(joiner.GetGamerId())->m_FullyJoined);

    //Inform existing peers of the joiner
    //(except the peer owning the joiner).

    const snPeer* joinerPeer =
        this->FindPeerById(joiner.GetPeerInfo().GetPeerId());
    snAssert(joinerPeer);

    //Ask the host for the host's version of the gamer's data.
    //This provides for cases where the host is responsible for
    //setting some portion of the gamer's data.
    snGetGamerData ggd;
    if(m_Owner.GetGamerData.IsBound())
    {
        m_Owner.GetGamerData(this, joiner, &ggd);
    }

    snMsgAddGamerToSessionCmd agCmd;
	
	// get the updated relay address in case it has changed since the peer joined the session
	netAddress relayAddr = m_CxnMgr->GetRelayAddress(this->GetPeerCxn(joiner.GetPeerInfo().GetPeerId()));

    agCmd.Reset(this->GetSessionId(),
                joiner,
                relayAddr,
                this->GetSlotType(joiner.GetGamerId()),
                ggd.m_SizeofData ? ggd.m_Data : NULL,
                ggd.m_SizeofData,
				reserveMembers,
				reserveSize);

    snDebug("InformPeersOfJoiner :: Sending %s for: \"%s\" with %d reserved slots...",
                agCmd.GetMsgName(),
                joiner.GetName(),
				reserveSize);

    for(int i = 0; i < (int) m_NumPeers; ++i)
    {
        const snPeer* peer = m_Peers[i];

        if(peer == joinerPeer)
        {
            //Don't send the peer's own gamers back to him.
            continue;
        }

        //If at least one of the peer's gamers is fully joined then
        //send the command.
        for(int j = 0; j < (int) peer->m_NumGamers; ++j)
        {
            if(peer->m_Gamers[j]->m_FullyJoined)
            {
                //FIXME (KB) - don't need a transaction here.
                m_Transactor.SendRequest(peer->GetCxnId(), NULL, agCmd, 0, NULL);
                break;
            }
        }
    }
}

bool
snSession::SendAddGamerCommandToPeer(const rlGamerHandle& gamerHandle, const int cxnId, ReservedSlot* reservedSlots, const unsigned numReservedSlots)
{
    bool success = false;

	rtry
	{
		rverify(gamerHandle.IsValid(), catchall, snError("SendAddGamerCommandToPeer :: Invalid gamer handle!"));

		snGamer* gamer = this->FindGamerByHandle(gamerHandle);

#if !__NO_OUTPUT
		char handleString[RL_MAX_GAMER_HANDLE_CHARS];
		gamerHandle.ToString(handleString);
#endif
		rverify(gamer, catchall, snError("SendAddGamerCommandToPeer :: Cannot find gamer with handle %s", handleString));

		snPeer* peer = gamer ? gamer->GetPeer() : NULL;
		rverify(gamer, catchall, snError("SendAddGamerCommandToPeer :: No peer for %s", gamer->GetGamerInfo().GetName()));

		snMsgAddGamerToSessionCmd cmd;

		snGetGamerData ggd;
		if(m_Owner.GetGamerData.IsBound())
		{
			m_Owner.GetGamerData(this, gamer->GetGamerInfo(), &ggd);
		}

		// get the updated relay address in case it has changed since the peer joined the session
		netAddress relayAddr;
		if(peer->IsLocal())
		{
			relayAddr = netRelay::GetMyRelayAddress();
		}
		else
		{
			relayAddr = m_CxnMgr->GetRelayAddress(this->GetPeerCxn(peer->GetPeerInfo().GetPeerId()));
		}

		cmd.Reset(this->GetSessionId(),
				  gamer->GetGamerInfo(),
				  relayAddr,
				  gamer->m_SlotType,
				  ggd.m_Data,
				  ggd.m_SizeofData,
				  reservedSlots,
				  numReservedSlots);

		//FIXME (KB) - don't need a transaction here.
		success = m_Transactor.SendRequest(cxnId, NULL, cmd, 0, NULL);
	}
	rcatchall
	{
	}

    return success;
}

snGamer*
snSession::FindGamerById(const rlGamerId& gamerId)
{
    snGamer* gamer = NULL;

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        if(m_Gamers[i]->m_GamerInfo.GetGamerId() == gamerId)
        {
            gamer = m_Gamers[i];
            break;
        }
    }

    return gamer;
}

const snGamer*
snSession::FindGamerById(const rlGamerId& gamerId) const
{
    return const_cast<snSession*>(this)->FindGamerById(gamerId);
}

snGamer*
snSession::FindGamerByHandle(const rlGamerHandle& gamerHandle)
{
    snGamer* gamer = NULL;

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        if(m_Gamers[i]->m_GamerInfo.GetGamerHandle() == gamerHandle)
        {
            gamer = m_Gamers[i];
            break;
        }
    }

    return gamer;
}

const snGamer*
snSession::FindGamerByHandle(const rlGamerHandle& gamerHandle) const
{
    return const_cast<snSession*>(this)->FindGamerByHandle(gamerHandle);
}

snPeer*
snSession::FindPeerById(const u64 peerId)
{
    snPeer* peer = NULL;
    if(peerId == m_LocalPeer.m_PeerInfo.GetPeerId())
    {
        peer = &m_LocalPeer;
    }
    else
    {
        for(int i = 0; i < (int) m_NumPeers; ++i)
        {
            if(m_Peers[i]->m_PeerInfo.GetPeerId() == peerId)
            {
                peer = m_Peers[i];
                break;
            }
        }
    }

    return peer;
}

const snPeer*
snSession::FindPeerById(const u64 peerId) const
{
    return const_cast<snSession*>(this)->FindPeerById(peerId);
}

snPeer*
snSession::FindPeerByCxn(const int cxnId)
{
    snPeer* peer = NULL;
    if(snVerify(cxnId >= 0))
    {
        for(int i = 0; i < (int) m_NumPeers; ++i)
        {
            if(m_Peers[i]->GetCxnId() == cxnId)
            {
                peer = m_Peers[i];
                break;
            }
        }
    }

    return peer;
}

const snPeer*
snSession::FindPeerByCxn(const int cxnId) const
{
    return const_cast<snSession*>(this)->FindPeerByCxn(cxnId);
}

snPeer*
snSession::FindPeerByEndpointId(const EndpointId endpointId)
{
    snPeer* peer = NULL;
    if(snVerify(NET_IS_VALID_ENDPOINT_ID(endpointId)))
    {
        for(int i = 0; i < (int) m_NumPeers; ++i)
        {
            if(m_Peers[i]->GetEndpointId() == endpointId)
            {
                peer = m_Peers[i];
                break;
            }
        }
    }

    return peer;
}

const snPeer*
snSession::FindPeerByEndpointId(const EndpointId endpointId) const
{
    return const_cast<snSession*>(this)->FindPeerByEndpointId(endpointId);
}

unsigned
snSession::NumReservedSlots(const rlSlotType slotType) const
{
    unsigned count = 0;
    for(int i = 0; i < (int)m_NumReservedSlots; ++i)
    {
        if(slotType == m_ReservedSlots[i].m_SlotType)
        {
            ++count;
        }
    }

    return count;
}

ReservedSlot*
snSession::FindSlotReservation(const rlGamerHandle& gamerHandle)
{
    for(int i = 0; i < (int)m_NumReservedSlots; ++i)
    {
        if(gamerHandle == m_ReservedSlots[i].m_GamerHandle)
        {
            return &m_ReservedSlots[i];
        }
    }

    return NULL;
}

const ReservedSlot*
snSession::FindSlotReservation(const rlGamerHandle& gamerHandle) const
{
    return const_cast<snSession*>(this)->FindSlotReservation(gamerHandle);
}

bool
snSession::FillSlotReservation(const rlGamerHandle gamerHandle)
{
    bool filled = false;

    for(int i = 0; i < (int)m_NumReservedSlots; ++i)
    {
        if(gamerHandle == m_ReservedSlots[i].m_GamerHandle)
        {
            OUTPUT_ONLY(char ghStr[32]);
            snDebug2("Filling slot reservation for gamer:%s",
                        m_ReservedSlots[i].m_GamerHandle.ToString(ghStr,32));

            m_ReservedSlots[i] = m_ReservedSlots[m_NumReservedSlots-1];
            --m_NumReservedSlots;
            filled = true;
            break;
        }
    }

    return filled;
}

void
snSession::UpdateReservedSlots()
{
    const unsigned curTime = sysTimer::GetSystemMsTime();
    for(int i = 0; i < (int)m_NumReservedSlots; ++i)
    {
        if(int(m_ReservedSlots[i].m_ExpiryMs - curTime) <= 0)
        {
            OUTPUT_ONLY(char ghStr[32]);
            snDebug2("Expired slot reservation for gamer:%s after %u ms",
                        m_ReservedSlots[i].m_GamerHandle.ToString(ghStr, 32),
                        m_ReservedSlots[i].m_ExpiryMs);
            m_ReservedSlots[i] = m_ReservedSlots[m_NumReservedSlots-1];
            --m_NumReservedSlots;
            --i;
			m_bNeedToUpdateReservations = true;
        }
    }

	// Update the number of reserved slots in the rlsession
	if (this->Exists())
	{
		m_RlSession.SetNumReservedSlots(m_NumReservedSlots);
	}

	// If we need to update SCS matchmaking with the reservations, and
	// the minimum time between updates (default 30s) has elapsed
	if (IsHost() && IsOnline() && m_RlSession.HasScMatchId() && m_bNeedToUpdateReservations && !m_UpdateReservationStatus.Pending() && !m_Migrating &&
		sysTimer::GetSystemMsTime() - m_uTimeOfLastReservationUpdate > TIME_BETWEEN_RESERVATION_UPDATES_MS)
	{
		m_bNeedToUpdateReservations = false;
		if (!m_RlSession.UpdateAvailableSlots(&m_UpdateReservationStatus))
		{
			snError("Failed to update the rlsession's available slots.");
		}
	}
}

#if RSG_DURANGO
void
snSession::UpdateDisplayNames()
{
	// If we aren't in progress
	if (!m_DisplayNameStatus.Pending())
	{
		// The request has succeeded, process the results
		if (m_DisplayNameStatus.Succeeded())
		{
			// Update display names
			for (unsigned int i = 0; i < m_PendingDisplayNames; i++)
			{
				snGamer* g = FindGamerByHandle(m_GamerHandles[i]);
				if (g)
				{
					snDebug("Retrieved gamer name %s for %"I64FMT"u, updating snGamer.", m_DisplayNames[i], m_GamerHandles[i].GetXuid());
					g->m_GamerInfo.SetDisplayName(&m_DisplayNames[i]);

					snEventSessionDisplayNameRetrieved e(g->m_GamerInfo);
					m_Delegator.Dispatch(this, &e);
				}
				else
				{
					snDebug("Retrieved gamer name %s for %"I64FMT"u, but matching snGamer could not be found", m_DisplayNames[i], m_GamerHandles[i].GetXuid());
				}
			}

			// Reset backoff timer, set pending count to 0, reset status
			m_CurrentDisplayNameBackoffTimer = 0;
			m_PendingDisplayNames = 0;
			m_DisplayNameStatus.Reset();
		}
		// If the current request has failed
		else if (m_DisplayNameStatus.Failed())
		{
			// Increment the backoff timer
			if (m_CurrentDisplayNameBackoffTimer < RETRY_TIMERS-1)
			{
				m_CurrentDisplayNameBackoffTimer++;
			}

			snDebug("Display name retrieval failed, upping backoff to %d seconds.", DISPLAYNAME_FAILURE_RETRY_TIMER[m_CurrentDisplayNameBackoffTimer]);
			m_PendingDisplayNames = 0;
			m_DisplayNameStatus.Reset();
			m_DirtyDisplayNames = true;
		}
		// If not a success or failure, we can start the next request. (Check the dirty flag)
		else if (m_DirtyDisplayNames)
		{
			// Calculate the backoff timer
			unsigned currentRetryTimer = DISPLAYNAME_FAILURE_RETRY_TIMER[m_CurrentDisplayNameBackoffTimer];

			// Calculate the time since the last request, check if we can start a new request
			if ((sysTimer::GetSystemMsTime() - m_DisplayNameRetrievalTime) > (currentRetryTimer * 1000)) // ms
			{
				// reset pending display names
				m_PendingDisplayNames = 0;

				// iterate the gamer list
				for(int i = 0; i < (int) m_NumGamers; ++i)
				{
					// get the gamer at the current index
					const snGamer* gamer = m_Gamers[i];

					// remote gamers only
					if (gamer && !gamer->GetGamerInfo().IsLocal())
					{
						// Get its gamer info, see if it has a display name
						const rlGamerInfo info = gamer->GetGamerInfo();
						if (!info.HasDisplayName())
						{
							// Add it to the list 
							snDebug("Gamer at index %d (%"I64FMT"u) has no name, adding to request list!");
							m_GamerHandles[m_PendingDisplayNames] = info.GetGamerHandle();
							m_PendingDisplayNames++;
						}
					}
				}

				// If we found a display name to get (someone could have left, and 0 is completely valid)
				if (m_PendingDisplayNames > 0)
				{
					// Set the retrieval time to now.
					m_DisplayNameRetrievalTime = sysTimer::GetSystemMsTime();
					snDebug("Requesting %u display names at time: %u", m_PendingDisplayNames, m_DisplayNameRetrievalTime);

					// Get the player names
					snVerify(g_rlXbl.GetProfileManager()->GetPlayerNames(GetOwner().GetLocalIndex(), m_GamerHandles, m_PendingDisplayNames, m_DisplayNames, NULL, &m_DisplayNameStatus));
				}

				m_DirtyDisplayNames = false;
			}
		}
	}
}
#endif // RSG_DURANGO

void 
snSession::RemoveLocalFromSession()
{
	snDebug("RemoveLocalFromSession");

	// we need to simulate a leave
	rlGamerHandle gh;
	rlPresence::GetGamerHandle(rlPresence::GetActingUserIndex(), &gh);

	// broadcast to everyone that we're leaving
	rlGamerId gamerIds[RL_MAX_GAMERS_PER_SESSION];
	const unsigned numGamerIds = this->GetGamerIds(&gh, 1, gamerIds, COUNTOF(gamerIds));
	snMsgRemoveGamersFromSessionCmd rgCmd;
	rgCmd.Reset(this->GetSessionId(), gamerIds, numGamerIds, -1);
	this->BroadcastCommand(rgCmd, &m_LocalPeer);

	// remove everyone else
	snPeer* hostPeer = FindPeerById(m_HostPeerId);
	if(hostPeer)
	{
		rlGamerHandle gamerHandles[RL_MAX_GAMERS_PER_SESSION];
		const unsigned numGamers = this->GetGamers(gamerHandles, COUNTOF(gamerHandles));
		unsigned numDrops = 0;

		for(int i = 0; i < (int) numGamers; ++i) if(!gamerHandles[i].IsLocal())
		{
			gamerHandles[numDrops++] = gamerHandles[i];
		}

		// drop any gamers we are adding, we've been kicked
		TaskQ::iterator it = m_TaskQ.begin();
		TaskQ::iterator stop = m_TaskQ.end();

		for(; stop != it; ++it)
		{
			if(!(*it)->IsFinished() && snJoinGamersToRlineTask::ISA(*it))
			{
				snJoinGamersToRlineTask* joinGamersTask = (snJoinGamersToRlineTask*)*it;
				joinGamersTask->DropGamers();
			}
		}

		if(numDrops)
		{
			// don't inform peers, just do this locally
			this->DropGamers(hostPeer,
							 gamerHandles,
							 numDrops,
							 KICK_DONT_INFORM_PEERS,
							 NULL,
							 -1,
							 NULL);
		}
	}
}

snJoinGamersToRlineTask*
snSession::GetJoinGamersToRlineTask(bool& bExistingJoinTask)
{
	// if we have a queued snJoinGamersToRlineTask task then add gamers to that.
	snJoinGamersToRlineTask* pJoinGamersTask = NULL;
	bExistingJoinTask = false;

	// only do this when adding our initial batch of gamers
	if(!IsEstablished())
	{
		// find a snJoinGamersToRlineTask in front of a snJoinCompleteTask
		for(TaskQ::iterator it = m_TaskQ.begin(); it != m_TaskQ.end(); ++it)
		{
			if(snJoinGamersToRlineTask::ISA(*it))
			{
				snJoinGamersToRlineTask* pTempTask = static_cast<snJoinGamersToRlineTask*>(*it);

				if(!pTempTask)
				{
					snDebug("GetJoinGamersToRlineTask - Invalid snJoinGamersToRlineTask");
					continue; 
				}

				if(pJoinGamersTask != NULL)
				{
					snDebug("GetJoinGamersToRlineTask - Found [%u], but have existing task", pTempTask->GetTaskId());
					continue; 
				}

				if(pTempTask->IsFinished())
				{
					snDebug("GetJoinGamersToRlineTask - Found [%u], but it has finished", pTempTask->GetTaskId());
					continue; 
				}

				if(!pTempTask->IsWaitingForAddGamers())
				{
					snDebug("GetJoinGamersToRlineTask - Found [%u], but it has already added all gamers", pTempTask->GetTaskId());
					continue; 
				}

				// all tests passed, use this task
				pJoinGamersTask = pTempTask;
			}
			else if(snJoinCompleteTask::ISA(*it) && pJoinGamersTask)
			{
				snDebug("GetJoinGamersToRlineTask - Found [%u], but we have a queued snJoinCompleteTask", pJoinGamersTask->GetTaskId());
				pJoinGamersTask = NULL;
				break;
			}
		}
	}
	
	// if we don't have one (or the one that's there is not valid), create a new snJoinGamersToRlineTask
	if(!pJoinGamersTask)
	{
		snDebug("GetJoinGamersToRlineTask - Creating new snJoinGamersToRlineTask");
		
		rlFireAndForgetTask<snJoinGamersToRlineTask>* jtask;

		jtask = snNew(m_Allocator, jtask);

		if(snVerify(jtask))
		{
			rlTaskBase::Configure(jtask, this, &jtask->m_Status);
			pJoinGamersTask = jtask;
		}
	}
	else
	{
		snDebug("GetJoinGamersToRlineTask - Found existing task [%u]", pJoinGamersTask->GetTaskId());
		bExistingJoinTask = true; 
	}

	return pJoinGamersTask;
}

#if !__NO_OUTPUT
static unsigned s_AddRemoveCount;
#endif  //__NO_OUTPUT

bool
snSession::AddGamer(const rlGamerInfo& gamerInfo,
                    const rlSlotType slotType,
                    const void* data,
                    const unsigned sizeofData)
{
    snAssert(!data || sizeofData > 0);
    snAssert(RL_SLOT_PUBLIC == slotType
            || RL_SLOT_PRIVATE == slotType);

    bool success = false;

    snDebug("Adding gamer:\"%s\"...", gamerInfo.GetName());

    snPeer* peer = this->FindPeerById(gamerInfo.GetPeerInfo().GetPeerId());

    if(snVerify(peer) && snVerify(!m_GamerPool.empty()))
    {
        snGamer* gamer = *m_GamerPool.begin();
        m_GamerPool.pop_front();

        gamer->Clear();
        gamer->m_GamerInfo = gamerInfo;
        gamer->m_SlotType = slotType;

#if RL_SUPPORT_NETWORK_BOTS
        // if the peer already has a gamer associated with it this must be a network bot
        bool addedGamer = (peer->GetGamerCount() == 0) ? peer->AddGamer(gamer) : peer->AddBotGamer(gamer);
#else
        bool addedGamer = peer->AddGamer(gamer);
#endif // RL_SUPPORT_NETWORK_BOTS

        if(!snVerify(addedGamer))
        {
            m_GamerPool.push_back(gamer);
        }
        else
        {
            m_Gamers[m_NumGamers++] = gamer;

            if(RL_SLOT_PRIVATE == gamer->m_SlotType)
            {
                ++m_NumOccupiedPrivSlots;
                snAssert(m_NumOccupiedPrivSlots <= this->GetMaxSlots(RL_SLOT_PRIVATE));
            }

			bool bFireEvent = true;

#if RSG_DURANGO
			if (!gamerInfo.IsLocal() && ((m_RlSession.GetCreateFlags() & RL_SESSION_CREATE_FLAG_DISPLAY_NAMES_NOT_REQUIRED) == 0))
			{
				snNotifyAddGamerTask* task = NULL;
				netStatus* status = NULL;

				// if we have a pending join complete task, this gamer pre-existed our join add a dependency for the notify task 
				// so that we only consider the join to be fully complete if we've "fully" added the gamer
				snJoinCompleteTask* pJoinCompleteTask = NULL;
				for(TaskQ::iterator it = m_TaskQ.begin(); it != m_TaskQ.end(); ++it)
				{
					if(snJoinCompleteTask::ISA(*it) && !(*it)->IsFinished())
					{
						pJoinCompleteTask = static_cast<snJoinCompleteTask*>(*it);
						break;
					}
				}

				// if we have a join dependency task, 
				if(pJoinCompleteTask)
				{
					snNotifyAddGamerTask* notifyTask = NULL;
					notifyTask = snNew(m_Allocator, notifyTask);
					pJoinCompleteTask->AddDependency(&status, notifyTask);
					task = notifyTask; 
				}
				else
				{
					// prepare a task to notify the app
					rlFireAndForgetTask<snNotifyAddGamerTask>* notifyTask = NULL;
					notifyTask = snNew(m_Allocator, notifyTask);
					status = &notifyTask->m_Status;
					task = notifyTask; 
				}
				
				if(snVerify(task) && snVerify(status))
				{
					if(snVerify(rlTaskBase::Configure(task, this, gamerInfo, slotType, sizeofData ? data : NULL, sizeofData, status)))
					{
						this->AddConcurrentTask(task);
						bFireEvent = false;

						// notify the app of the incoming gamer
						const netAddress relayAddr = NET_IS_VALID_ENDPOINT_ID(peer->GetEndpointId()) ? m_CxnMgr->GetRelayAddress(peer->GetEndpointId()) : netAddress::INVALID_ADDRESS;
						snEventAddingGamer ag(gamerInfo, peer->GetEndpointId(), relayAddr, slotType, sizeofData ? data : NULL, sizeofData);
						m_Delegator.Dispatch(this, &ag);
					}
					else
					{
						Delete(task, *m_Allocator);
						task = NULL;

						// fail the status to prevent the complete task from stalling
						status->SetPending();
						status->SetFailed();
					}
				}
			}
#endif

			if (bFireEvent)
			{
				snEventAddedGamer e(gamerInfo,
									peer->GetEndpointId(),
									NET_IS_VALID_ENDPOINT_ID(peer->GetEndpointId()) ? m_CxnMgr->GetRelayAddress(peer->GetEndpointId()) : netAddress::INVALID_ADDRESS,
									slotType,
									sizeofData ? data : NULL,
									sizeofData);
				DispatchEvent(&e);
			}

#if !__NO_OUTPUT
            snDebug("AddRemoveCount(AddGamer):%d", s_AddRemoveCount);
            ++s_AddRemoveCount;
            this->PrintMembers();
#endif  //__NO_OUTPUT

            success = true;
        }
    }

    return success;
}

bool
snSession::RemoveGamer(const rlGamerId& gamerId, const int removeReason)
{
    const snGamer* gamer = this->FindGamerById(gamerId);

    return gamer && this->RemoveGamer(gamer->GetGamerHandle(), removeReason);
}

bool
snSession::RemoveGamer(const rlGamerHandle& gamerHandle, const int removeReason)
{
    bool success = false;

    snGamer* gamer = NULL;
    unsigned gamerIdx;
    for(gamerIdx = 0; gamerIdx < m_NumGamers; ++gamerIdx)
    {
        if(m_Gamers[gamerIdx]->m_GamerInfo.GetGamerHandle() == gamerHandle)
        {
            gamer = m_Gamers[gamerIdx];
            break;
        }
    }

#if RSG_DURANGO
	CancelDisplayNameTask(gamerHandle);
#endif // RSG_DURANGO

    if(snVerify(gamer))
    {
        snDebug("Removing gamer:\"%s\", remove reason: %d...", gamer->m_GamerInfo.GetName(), removeReason);

        m_Gamers[gamerIdx] = m_Gamers[m_NumGamers-1];
        m_Gamers[m_NumGamers-1] = NULL;
        --m_NumGamers;

        if(RL_SLOT_PRIVATE == gamer->m_SlotType)
        {
            snAssert(m_NumOccupiedPrivSlots > 0);
            --m_NumOccupiedPrivSlots;
        }

		// find the peer by Id instead of using the peer pointer owned by the player
		// in case the underlying peer has been removed
		snPeer* peer = FindPeerById(gamer->m_GamerInfo.GetPeerInfo().GetPeerId());
		if(snVerify(peer))
		{
			// check if this peer is the same as the one owned by our ped
			snAssert(peer == gamer->GetPeer());

			// remove the gamer from the peer
			snVerify(peer->RemoveGamer(gamerHandle));

			// remove peer if we've removed all gamers
			if(peer->GetGamerCount() == 0)
			{
				this->RemovePeer(peer);
			}
		}

		// Notify app that gamer was removed
		snEventRemovedGamer e(gamer->m_GamerInfo, peer->GetEndpointId());
		e.m_RemoveReason = removeReason;
		DispatchEvent(&e);

		gamer->Clear();

		m_GamerPool.push_back(gamer);

        success = true;
    }

    return success;
}

bool
snSession::DropGamers(const snPeer* requestingPeer,
                    const rlGamerHandle* gamerHandles,
                    const int numGamers,
                    const KickType kickType,
                    snLeaveGamersFromRlineTask* lgTask,
                    const int removeReason,
					netStatus* status)
{
    snAssert(gamerHandles && numGamers);
    snAssert(numGamers <= RL_MAX_GAMERS_PER_SESSION);
    snAssert(requestingPeer);

	snDebug("DropGamers :: removeReason: %d", removeReason);

    bool success = false;

    snLeaveGamersFromRlineTask* task = NULL;

    rtry
    {
        rcheck(numGamers > 0, catchall,);

        const snPeer* hostPeer = this->FindPeerById(m_HostPeerId);

        const bool requesterIsHost = requestingPeer && (requestingPeer == hostPeer);
        const bool requesterIsMe = (requestingPeer == &m_LocalPeer);

        //One of the following must be true:
        //1.  We're the requester.
        //2.  The requester is the host.
        //3.  We're the host.

        //Note that it's possible for all to be false if we receive a
        //DropGamers command from a peer who migrated the session to
        //himself, but whom we were unable to set as the new host.
        //This will occur in the following situation:
        //A, B, C, D are in a session hosted by A.
        //A and B drop, C begins waiting for B to become the host, and
        //a short time later D begins waiting for B to become the host.
        //C times out waiting for B, then becomes the host.  C tells D
        //he's the host, but D is still waiting for B and rejects C's
        //request.  Upon receiving D's rejection, C drops D.  D receives
        //the drop command from C but, according to D, C is not the host.
        rcheck(this->IsHost()
                || requesterIsMe
                || requesterIsHost, catchall,);

        if(lgTask)
        {
            rverify(rlTaskBase::Configure(lgTask, this, gamerHandles, numGamers, status), catchall,);
        }
        else
        {
            task = snNew(m_Allocator, task);

            rverify(task, catchall,);

            rverify(rlTaskBase::Configure(task, this, gamerHandles, numGamers, status), catchall,);
            this->AddTask(task);
        }

        if(this->IsHost())
        {
            if(KICK_INFORM_PEERS == kickType)
            {
                rlGamerId gamerIds[RL_MAX_GAMERS_PER_SESSION];
                const unsigned numGamerIds = this->GetGamerIds(gamerHandles, numGamers, gamerIds, COUNTOF(gamerIds));
                snMsgRemoveGamersFromSessionCmd rgCmd;
                rgCmd.Reset(this->GetSessionId(), gamerIds, numGamerIds, removeReason);

                this->BroadcastCommand(rgCmd, requestingPeer);
            }
        }
        else if(requesterIsMe)
        {
			// let everyone know we're leaving
			rlGamerId gamerIds[RL_MAX_GAMERS_PER_SESSION];
			const unsigned numGamerIds = this->GetGamerIds(gamerHandles, numGamers, gamerIds, COUNTOF(gamerIds));
			snMsgRemoveGamersFromSessionCmd rgCmd;
			rgCmd.Reset(this->GetSessionId(), gamerIds, numGamerIds, removeReason);

			this->BroadcastCommand(rgCmd, requestingPeer);
        }

        for(int i = 0; i < numGamers; ++i)
        {
            const snGamer* gamer = this->FindGamerByHandle(gamerHandles[i]);

            if(!snVerify(gamer))
            {
                continue;
            }
			
            snAssert(this->HasPeer(gamer->m_GamerInfo.GetPeerInfo().GetPeerId()));

            if(this->IsHost() || requesterIsHost)
            {
				snDebug1("%s :: Removing gamer \"%s\" because I'm the host, or the requester is the host...", __FUNCTION__, gamer->GetGamerInfo().GetDisplayName());
                this->RemoveGamer(gamerHandles[i], removeReason);
            }
            else if(requesterIsMe && snVerify(gamer->m_GamerInfo.IsLocal()))
            {
				snDebug1("%s :: Removing gamer \"%s\" because I'm the requester...", __FUNCTION__, gamer->GetGamerInfo().GetDisplayName());
                this->RemoveGamer(gamerHandles[i], removeReason);
            }
        }

#if !__NO_OUTPUT
        snDebug("AddRemoveCount(DropGamers):%d", s_AddRemoveCount);
        ++s_AddRemoveCount;
        this->PrintMembers();
#endif  //__NO_OUTPUT

        success = true;
    }
    rcatchall
    {
        if(task){Delete(task, *m_Allocator);}
        task = NULL;
    }

    if(!success && status){status->SetPending(); status->SetFailed();}

    return success;
}

void
snSession::ReconcileMembersWithHost(const rlGamerId* members,
                                    const unsigned numMembers)
{
    snDebug("Reconciling session membership with the host...");

    rtry
    {
        rverify(numMembers > 0, catchall,);

        rcheck(!this->IsHost(),
                catchall,
                snError("We're the host - no need to reconcile membership"));

        u64 hostPeerId;
        rverify(this->GetHostPeerId(&hostPeerId),
                catchall,
                snError("Error retrieving host peer id."));

        //Remove members we have locally but are no longer members
        rlGamerId gamerIds[RL_MAX_GAMERS_PER_SESSION];
        const unsigned numGamers = this->GetGamers(gamerIds, COUNTOF(gamerIds));
        unsigned numToRemove = 0;

        for(int i = 0; i < (int) numGamers; ++i)
        {
            bool remove = true;

            for(int j = 0; j < (int) numMembers; ++j)
            {
                if(members[j] == gamerIds[i])
                {
                    remove = false;
                    break;
                }
            }

            if(remove)
            {
                gamerIds[numToRemove] = gamerIds[i];
                ++numToRemove;
            }
        }

        if(numToRemove)
        {
            snDebug("Removing %d gamers that are no longer members...",
                    numToRemove);

            rlFireAndForgetTask<snDropGamersTask>* task = NULL;
            rlGetTaskManager()->CreateTask(&task);

            if(snVerify(task))
            {
                if(snVerify(rlTaskBase::Configure(task,
                                                      this,
                                                      hostPeerId,
                                                      gamerIds,
                                                      numToRemove,
													  -1,
                                                      &task->m_Status)))
                {
                    snVerify(rlGetTaskManager()->AddParallelTask(task));
                }
                else
                {
                    rlGetTaskManager()->DestroyTask(task);
                    task = NULL;
                }
            }
        }

        //For members we don't have locally request that the
        //host send us an add gamer command.
        snMsgRequestGamerInfo msg;
        for(int i = 0; i < (int) numMembers; ++i)
        {
            if(!this->IsMember(members[i]))
            {
#if !__NO_OUTPUT
                char gamerIdStr[64];
                snDebug("Member:%s is not present locally - requesting from host.",
                        members[i].ToHexString(gamerIdStr));
#endif  //__NO_OUTPUT

                msg.Reset(this->GetSessionId(), members[i]);
                this->SendMsg(hostPeerId, msg, NET_SEND_RELIABLE);
            }
        }
    }
    rcatchall
    {
    }
}

//Macro that makes it much easier to debug what condition in a request failed.
//It checks the condition, and if it fails it prints out a useful debug string.
#define VALIDATE_RQST_COND(rqst, cond)          \
    if (!(cond))                              \
    {                                           \
        snDebug("Validating " #rqst" failed: "#cond);     \
        return false;                           \
    }

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
                        rlMsgQosProbeRequest* msg) const
{
    VALIDATE_RQST_COND(rlMsgQosProbeRequest, this->Exists());
    VALIDATE_RQST_COND(rlMsgQosProbeRequest, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
    VALIDATE_RQST_COND(rlMsgQosProbeRequest, this->GetSessionInfo() == msg->m_SessionInfo);

    return true;
}

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
                        snMsgConfigRequest* msg) const
{
    VALIDATE_RQST_COND(snMsgConfigRequest, this->Exists());
    VALIDATE_RQST_COND(snMsgConfigRequest, this->IsHost());
    VALIDATE_RQST_COND(snMsgConfigRequest, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
    VALIDATE_RQST_COND(snMsgConfigRequest, this->GetSessionInfo() == msg->m_SessionInfo);

    return true;
}

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
                        snMsgJoinRequest* msg) const
{
    VALIDATE_RQST_COND(snMsgJoinRequest, this->Exists());
    VALIDATE_RQST_COND(snMsgJoinRequest, this->IsHost());
    VALIDATE_RQST_COND(snMsgJoinRequest, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
    VALIDATE_RQST_COND(snMsgJoinRequest, this->GetSessionId() == msg->m_SessionId);

    return true;
}

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
                        snMsgAddGamerToSessionCmd* msg) const
{
    VALIDATE_RQST_COND(snMsgAddGamerToSessionCmd, this->Exists());
    //Verify the command came from the host.
    VALIDATE_RQST_COND(snMsgAddGamerToSessionCmd, rqst->m_TxInfo.m_CxnId >= 0);
    VALIDATE_RQST_COND(snMsgAddGamerToSessionCmd, this->GetPeerCxn(m_HostPeerId) == rqst->m_TxInfo.m_CxnId);
    VALIDATE_RQST_COND(snMsgAddGamerToSessionCmd, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
	VALIDATE_RQST_COND(snMsgAddGamerToSessionCmd, snVerify(msg->m_GamerInfo.IsValid()));
    VALIDATE_RQST_COND(snMsgAddGamerToSessionCmd, this->GetSessionId() == msg->m_SessionId);

    return true;
}

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
                        snMsgHostLeftWhilstJoiningCmd* msg) const
{
    VALIDATE_RQST_COND(snMsgHostLeftWhilstJoiningCmd, this->Exists());
    //Verify the command came from the host.
    VALIDATE_RQST_COND(snMsgHostLeftWhilstJoiningCmd, rqst->m_TxInfo.m_CxnId >= 0);
    VALIDATE_RQST_COND(snMsgHostLeftWhilstJoiningCmd, this->GetPeerCxn(m_HostPeerId) == rqst->m_TxInfo.m_CxnId);
    VALIDATE_RQST_COND(snMsgHostLeftWhilstJoiningCmd, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
    VALIDATE_RQST_COND(snMsgHostLeftWhilstJoiningCmd, this->GetSessionId() == msg->m_SessionId);

    return true;
}

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
						snMsgNotMigrating* msg) const
{
	VALIDATE_RQST_COND(snMsgNotMigrating, this->Exists());
	VALIDATE_RQST_COND(snMsgNotMigrating, rqst->m_TxInfo.m_CxnId >= 0);
	VALIDATE_RQST_COND(snMsgNotMigrating, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
	VALIDATE_RQST_COND(snMsgNotMigrating, this->GetSessionId() == msg->m_SessionId);

	return true;
}

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
                        snMsgRemoveGamersFromSessionCmd* msg) const
{
    VALIDATE_RQST_COND(snMsgRemoveGamersFromSessionCmd, this->Exists());
    VALIDATE_RQST_COND(snMsgRemoveGamersFromSessionCmd, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
    VALIDATE_RQST_COND(snMsgRemoveGamersFromSessionCmd, this->GetSessionId() == msg->m_SessionId);
    VALIDATE_RQST_COND(snMsgRemoveGamersFromSessionCmd, rqst->m_TxInfo.m_CxnId >= 0);

    return true;
}

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
                        snMsgStartMatchCmd* msg) const
{
    VALIDATE_RQST_COND(snMsgStartMatchCmd, this->Exists());
    //Verify the command came from the host.
    VALIDATE_RQST_COND(snMsgStartMatchCmd, rqst->m_TxInfo.m_CxnId >= 0);
    VALIDATE_RQST_COND(snMsgStartMatchCmd, this->GetPeerCxn(m_HostPeerId) == rqst->m_TxInfo.m_CxnId);
    VALIDATE_RQST_COND(snMsgStartMatchCmd, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
    VALIDATE_RQST_COND(snMsgStartMatchCmd, this->GetSessionId() == msg->m_SessionId);

    return true;
}

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
                        snMsgChangeSessionAttributesCmd* msg) const
{
    VALIDATE_RQST_COND(snMsgChangeSessionAttributesCmd, this->Exists());
    //Verify the command came from the host.
    VALIDATE_RQST_COND(snMsgChangeSessionAttributesCmd, rqst->m_TxInfo.m_CxnId >= 0);
    VALIDATE_RQST_COND(snMsgChangeSessionAttributesCmd, this->GetPeerCxn(m_HostPeerId) == rqst->m_TxInfo.m_CxnId);
    VALIDATE_RQST_COND(snMsgChangeSessionAttributesCmd, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
    VALIDATE_RQST_COND(snMsgChangeSessionAttributesCmd, this->GetSessionId() == msg->m_SessionId);

    return true;
}

template<>
bool
snSession::ValidateRqst(const netRequest* rqst,
                        snMsgSetInvitableCmd* msg) const
{
    VALIDATE_RQST_COND(snMsgSetInvitableCmd, this->Exists());
    //Verify the command came from the host.
    VALIDATE_RQST_COND(snMsgSetInvitableCmd, rqst->m_TxInfo.m_CxnId >= 0);
    VALIDATE_RQST_COND(snMsgSetInvitableCmd, this->GetPeerCxn(m_HostPeerId) == rqst->m_TxInfo.m_CxnId);
    VALIDATE_RQST_COND(snMsgSetInvitableCmd, snVerify(msg->Import(rqst->m_Data, rqst->m_SizeofData)));
    VALIDATE_RQST_COND(snMsgSetInvitableCmd, this->GetSessionId() == msg->m_SessionId);

    return true;
}

template<>
bool
snSession::ValidateMsg(const netEventFrameReceived* frame,
                        snMsgSessionMemberIds* msg) const
{
    VALIDATE_RQST_COND(snMsgSessionMemberIds, this->Exists());
    //Verify the command came from the host.
    VALIDATE_RQST_COND(snMsgSessionMemberIds, frame->m_CxnId >= 0);
    VALIDATE_RQST_COND(snMsgSessionMemberIds, this->GetPeerCxn(m_HostPeerId) == frame->m_CxnId);
    VALIDATE_RQST_COND(snMsgSessionMemberIds, snVerify(msg->Import(frame->m_Payload, frame->m_SizeofPayload)));
    VALIDATE_RQST_COND(snMsgSessionMemberIds, this->GetSessionId() == msg->m_SessionId);

    return true;
}

void
snSession::OnNetEvent(netConnectionManager* NET_ASSERTS_ONLY(cxnMgr),
                        const netEvent* evt)
{
    snAssert(cxnMgr == m_CxnMgr);

    const unsigned evtId = evt->GetId();

    if(NET_EVENT_CONNECTION_REQUESTED == evtId)
    {
        if(this->Exists() && this->IsHost())
        {
            //A connection request should contain a join
            //request, or nothing.

            unsigned msgId;
            if(netMessage::GetId(&msgId,
                                evt->m_CxnRequested->m_Payload,
                                evt->m_CxnRequested->m_SizeofPayload))
            {
                if(snMsgJoinRequest::MSG_ID() == msgId)
                {
                    snMsgJoinRequest rqst;
                    if(rqst.Import(evt->m_CxnRequested->m_Payload,
                                    evt->m_CxnRequested->m_SizeofPayload))
                    {
                        snDebug("Received %s from:" NET_ADDR_FMT "",
                                rqst.GetMsgName(),
                                NET_ADDR_FOR_PRINTF(evt->m_CxnRequested->m_Sender));

                        this->HandleJoinRequest(rqst,
											    evt->m_CxnRequested->m_Sender,
												evt->m_CxnRequested->m_Sequence);
                    }
                }
            }
        }
    }
    else if(NET_EVENT_FRAME_RECEIVED == evtId)
    {
        unsigned msgId;

        if(!this->Exists())
        {
        }
		else if(netMessage::GetId(&msgId,
				evt->m_FrameReceived->m_Payload,
				evt->m_FrameReceived->m_SizeofPayload))
		{
			// we can receive this OOB when a player leaves suddenly
			if(snMsgRemoveGamersFromSessionCmd::MSG_ID() == msgId)
			{
				snMsgRemoveGamersFromSessionCmd rgCmd;
				if(rgCmd.Import(evt->m_FrameReceived->m_Payload, evt->m_FrameReceived->m_SizeofPayload)
					&& this->GetSessionId() == rgCmd.m_SessionId
					&& evt->m_CxnId == NET_INVALID_CXN_ID)
				{
					snDebug("Received out of band %s from:" NET_ADDR_FMT "",
						rgCmd.GetMsgName(),
						NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));

					rlGamerInfo gamerInfo;
					if(GetGamerInfo(rgCmd.m_GamerIds[0], &gamerInfo))
					{
						const snPeer* requestingPeer = FindPeerById(gamerInfo.GetPeerInfo().GetPeerId());
						if(requestingPeer)
						{
							snDebug("Removing %s, bail reason %d", gamerInfo.GetName(), rgCmd.m_RemoveReason);
							this->HandleRemoveGamerCmd(requestingPeer, rgCmd);
						}
					}
				}
			}
			// messages only for host
			else if(this->IsHost())
			{
				if(snMsgJoinRequest::MSG_ID() == msgId)
				{
					//Handle join requests.

					snMsgJoinRequest rqst;
					if(rqst.Import(evt->m_FrameReceived->m_Payload,
						evt->m_FrameReceived->m_SizeofPayload))
					{
						snDebug("Received %s from:" NET_ADDR_FMT "",
							rqst.GetMsgName(),
							NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));

						this->HandleJoinRequest(rqst,
							evt->m_FrameReceived->m_Sender,
							evt->m_FrameReceived->m_Sequence);
					}
				}
				else if(snMsgRequestGamerInfo::MSG_ID() == msgId)
				{
					//Handle gamer info requests.

					snMsgRequestGamerInfo msg;
					if(msg.Import(evt->m_FrameReceived->m_Payload,
						evt->m_FrameReceived->m_SizeofPayload))
					{
						snDebug("Received %s from:" NET_ADDR_FMT "",
							msg.GetMsgName(),
							NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));

						this->HandleGamerInfoRequest(msg, evt->m_CxnId);
					}
				}
			}
			// messages only for non-host
			else
			{
				if(snMsgSessionMemberIds::MSG_ID() == msgId)
				{
					snMsgSessionMemberIds memberIds;
					if(this->ValidateMsg(evt->m_FrameReceived, &memberIds))
					{
						snDebug("Received %s from:" NET_ADDR_FMT "",
							memberIds.GetMsgName(),
							NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));

						this->ReconcileMembersWithHost(memberIds.m_GamerIds,
							memberIds.m_Count);
					}
				}
			}
		}
    }
    else if(NET_EVENT_CONNECTION_ESTABLISHED == evtId)
    {
    }
    else if(NET_EVENT_CONNECTION_ERROR == evtId)
    {
		// if we are running a self kick stall removal timer and we detect a stall, skip this
		if(!(sm_SelfRemoveStallTime > 0 && ((sysTimer::GetSystemMsTime() - m_LastTime) > sm_SelfRemoveStallTime)))
		{
			snDebug("Dropped connection %08x.%d, due to: %s (%d)",
				evt->m_CxnId, evt->m_ChannelId,
				netEventConnectionError::CXNERR_SEND_ERROR == evt->m_CxnError->m_ErrorCode
				? "CXNERR_SEND_ERROR"
				: netEventConnectionError::CXNERR_TERMINATED == evt->m_CxnError->m_ErrorCode
				? "CXNERR_TERMINATED"
				: netEventConnectionError::CXNERR_TIMED_OUT == evt->m_CxnError->m_ErrorCode
				? "CXNERR_TIMED_OUT"
				: "UNKNOWN ERROR",
				evt->m_CxnError->m_ErrorCode);

			snPeer* peer = this->FindPeerByCxn(evt->m_CxnId);

			if(peer)
			{
				if(this->Exists() && this->IsHost())
				{
					snVerify(this->DropPeer(peer->m_PeerInfo.GetPeerId(), NULL, NULL));
				}
				else
				{
					this->DisconnectPeer(peer);
				}
			}
		}
		else
		{
			snDebug("Ignoring dropped connection, local stall");
		}
    }
    else if(NET_EVENT_CONNECTION_CLOSED == evtId)
    {
		// if we are running a self kick stall removal timer and we detect a stall, skip this
		if(!(sm_SelfRemoveStallTime > 0 && ((sysTimer::GetSystemMsTime() - m_LastTime) > sm_SelfRemoveStallTime)))
		{
			snDebug("Connection %08x.%d closed",
				evt->m_CxnId, evt->m_ChannelId);

			snPeer* peer = this->FindPeerByCxn(evt->m_CxnId);

			if(peer)
			{
				if(this->Exists() && this->IsHost())
				{
					snVerify(this->DropPeer(peer->m_PeerInfo.GetPeerId(), NULL, NULL));
				}
				else
				{
					this->DisconnectPeer(peer);
				}
			}
		}
		else
		{
			snDebug("Ignoring closed connection, local stall");
		}
    }
}

void
snSession::OnRequest(netTransactor* transactor,
                    netRequestHandler* /*handler*/,
                    const netRequest* rqst)
{
    snAssert(&m_Transactor == transactor);

    rtry
    {
        unsigned msgId;
        rcheck(netMessage::GetId(&msgId,
                                 rqst->m_Data,
                                 rqst->m_SizeofData), catchall,);

        if(rlMsgQosProbeRequest::MSG_ID() == msgId)
        {
            rlMsgQosProbeRequest probeRqst;
            if(this->ValidateRqst(rqst, &probeRqst))
            {
                snDebug("Received %s from:" NET_ADDR_FMT "",
                            probeRqst.GetMsgName(),
                            NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

                rlMsgQosProbeResponse probeResp;

                if(0 != (probeResp.m_UserDataSize = m_RlSession.GetQosUserDataSize()))
                {
                    sysMemCpy(probeResp.m_UserData, m_RlSession.GetQosUserData(), probeResp.m_UserDataSize);
                }

                transactor->SendResponse(rqst->m_TxInfo, probeResp);
            }
        }
        else if(snMsgConfigRequest::MSG_ID() == msgId)
        {
            snMsgConfigRequest cfgRqst;
            if(this->ValidateRqst(rqst, &cfgRqst))
            {
                snDebug("Received %s from:" NET_ADDR_FMT "",
                            cfgRqst.GetMsgName(),
                            NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

                //FIXME (KB) - check if we can handle additional guests.
                rlPeerInfo localPeerInfo;
                if(snVerify(rlPeerInfo::GetLocalPeerInfo(&localPeerInfo, m_RlSession.GetOwner().GetLocalIndex())))
                {
                    snMsgConfigResponse cfgResp;
                    cfgResp.Reset(localPeerInfo,
                                    m_RlSession.GetSessionInfo(),
                                    m_RlSession.GetConfig());
                    transactor->SendResponse(rqst->m_TxInfo, cfgResp);
                }
            }
        }
        else if(snMsgAddGamerToSessionCmd::MSG_ID() == msgId)
        {
            snMsgAddGamerToSessionCmd agCmd;
            if(this->ValidateRqst(rqst, &agCmd))
            {
                snDebug("Received %s for: \"%s\" with relay: " NET_ADDR_FMT " from:" NET_ADDR_FMT " with %u reserved slots",
                        agCmd.GetMsgName(),
                        agCmd.m_GamerInfo.GetName(),
						NET_ADDR_FOR_PRINTF(agCmd.m_RelayAddr),
						NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr),
						agCmd.m_ReservedSize);

				// Reserved players from joiner
				if(agCmd.m_ReservedSize > 0)
				{
					ReserveSlots(agCmd.m_ReservedMembers, agCmd.m_ReservedSize, agCmd.m_SlotType, SLOT_RESERVATION_TTL_MS, false);
				}
				
				// Get or create snJoinGamersToRlineTask 
                bool existingJoinTask = false;
				snJoinGamersToRlineTask* joinGamersTask = GetJoinGamersToRlineTask(existingJoinTask);

                if(joinGamersTask)
                {
                    //The status member is provided by the join task
                    netStatus* status = NULL;
                    snVerify(joinGamersTask->AddGamer(agCmd.m_GamerInfo.GetGamerHandle(),
														  agCmd.m_GamerInfo.GetGamerId(),
                                                          agCmd.m_SlotType,
														  false,
                                                          &status));
                    if(status)
                    {
                        //Start an snAddRemoteGamerTask to handle this. It will open connections as necessary 
						//(including doing NAT negotiation) and send our response to the request.
                        snAddRemoteGamerTask* task = rlGetTaskManager()->CreateTask<snAddRemoteGamerTask>();
                        if(snVerify(task))
                        {
                            if(snVerify(rlTaskBase::Configure(task, this, agCmd, status)))
                            {
                                snVerify(rlGetTaskManager()->AddParallelTask(task));
                            }
                            else
                            {
                                rlGetTaskManager()->DestroyTask(task);
                                task = NULL;
								status->SetPending();
								status->SetFailed();
                            }
                        }
                    }

                    //If not an existing task and we successfully added the snAddRemoteGamerTask
                    if(!existingJoinTask)
                    {
                        if(status && status->Pending())
                        {
                            //Add to serial queue. Waits for snAddRemoteGamerTasks to complete
                            this->AddTask(joinGamersTask);
                        }
                        else
                        {
                            Delete(joinGamersTask, *m_Allocator);
                            joinGamersTask = NULL;
                        }
                    }
                }
            }
        }
        else if(snMsgHostLeftWhilstJoiningCmd::MSG_ID() == msgId)
        {
            snMsgHostLeftWhilstJoiningCmd rgCmd;

            if(this->ValidateRqst(rqst, &rgCmd))
            {
                snDebug("Received %s from:" NET_ADDR_FMT "",
                    rgCmd.GetMsgName(),
                    NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

                this->HandleHostLeftWhileJoiningCmd(*rqst, rgCmd);
            }
        }
		else if(snMsgNotMigrating::MSG_ID() == msgId)
		{
			snMsgNotMigrating rgCmd;

			if(this->ValidateRqst(rqst, &rgCmd))
			{
				snDebug("Received %s from:" NET_ADDR_FMT "",
					rgCmd.GetMsgName(),
					NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

				const snPeer* requestingPeer =
					this->FindPeerByCxn(rqst->m_TxInfo.m_CxnId);

				if(requestingPeer)
				{
					this->MarkAsNotMigrating(requestingPeer->GetPeerId());
				}
			}
		}
        else if(snMsgRemoveGamersFromSessionCmd::MSG_ID() == msgId)
        {
            snMsgRemoveGamersFromSessionCmd rgCmd;

            if(this->ValidateRqst(rqst, &rgCmd))
            {
                snDebug("Received %s from:" NET_ADDR_FMT "",
                        rgCmd.GetMsgName(),
                        NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

				const snPeer* requestingPeer =
					this->FindPeerByCxn(rqst->m_TxInfo.m_CxnId);

                this->HandleRemoveGamerCmd(requestingPeer, rgCmd);
            }
        }
        else if(snMsgStartMatchCmd::MSG_ID() == msgId)
        {
            snMsgStartMatchCmd startCmd;
            if(this->ValidateRqst(rqst, &startCmd))
            {
                snDebug("Received %s from:" NET_ADDR_FMT "",
                        startCmd.GetMsgName(),
                        NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

				rlFireAndForgetTask<snJoinCompleteTask>* task = NULL;

				task = snNew(m_Allocator, task);

				if(snVerify(task))
				{
					if(snVerify(rlTaskBase::Configure(task, this, &task->m_Status)))
					{
						this->AddTask(task);
					}
					else
					{
						Delete(task, *m_Allocator);
						task = NULL;
					}
				}
            }
        }
        else if(snMsgChangeSessionAttributesCmd::MSG_ID() == msgId)
        {
            snMsgChangeSessionAttributesCmd changeAttrCmd;
            if(this->ValidateRqst(rqst, &changeAttrCmd))
            {
                snDebug("Received %s from:" NET_ADDR_FMT "",
                        changeAttrCmd.GetMsgName(),
                        NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

                rlFireAndForgetTask<snChangeAttributesTask>* task = NULL;

                task = snNew(m_Allocator, task);

                if(snVerify(task))
                {
                    if(snVerify(rlTaskBase::Configure(task,
                                                        this,
                                                        changeAttrCmd.m_Attrs,
                                                        &task->m_Status)))
                    {
                        this->AddTask(task);
                    }
                    else
                    {
                        Delete(task, *m_Allocator);
                        task = NULL;
                    }
                }
            }
        }
        else if(snMsgSetInvitableCmd::MSG_ID() == msgId)
        {
            snMsgSetInvitableCmd setInvitableCmd;
            if(this->ValidateRqst(rqst, &setInvitableCmd))
            {
                snDebug("Received %s from:" NET_ADDR_FMT "",
                        setInvitableCmd.GetMsgName(),
                        NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

                rlFireAndForgetTask<snModifyPresenceFlagsTask>* task = NULL;

                task = snNew(m_Allocator, task);

                if(snVerify(task))
                {
					unsigned presenceFlags = this->m_RlSession.GetPresenceFlags();
					if(setInvitableCmd.m_Invitable)
					{
						presenceFlags |= RL_SESSION_PRESENCE_FLAG_INVITABLE;
					}
					else
					{
						presenceFlags &= ~RL_SESSION_PRESENCE_FLAG_INVITABLE;
					}

                    if(snVerify(rlTaskBase::Configure(task,
                                                            this,
                                                            presenceFlags,
                                                            &task->m_Status)))
                    {
                        this->AddTask(task);
                    }
                    else
                    {
                        Delete(task, *m_Allocator);
                        task = NULL;
                    }
                }
            }
        }
#if !__NO_OUTPUT
        else
        {
            const char* msgName = netMessage::GetName(rqst->m_Data, rqst->m_SizeofData);
            if(msgName)
            {
                snDebug("Ignoring %s from:" NET_ADDR_FMT "",
                            msgName,
                            NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));
            }
        }
#endif  //!__NO_OUTPUT
    }
    rcatchall
    {
    }
}

snPeer*
snSession::ConnectToPeer(const rlPeerInfo& peerInfo,
						const int cxnId,
						const EndpointId endpointId,
                        const void* bytes,
                        const unsigned numBytes,
                        netStatus* status)
{
    snAssert(this->Exists());

    // bool success = false;

    snPeer* newPeer = NULL;
    int newCxnId = -1;
    int useCxnId = cxnId;

    rtry
    {
		if(sm_ConnectToPeerChecks)
		{
			rverifyall(this->m_IsInitialized);
			rverifyall(NET_IS_VALID_ENDPOINT_ID(endpointId));
			rverifyall(m_CxnMgr && m_CxnMgr->IsInitialized());
		}

        snDebug("Connecting to peer:" NET_ADDR_FMT "",
                    NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(endpointId)));

        rverify(!this->FindPeerById(peerInfo.GetPeerId()),
                catchall,
                snError("We're already connected to:" NET_ADDR_FMT "",
                        NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(endpointId))));

        if(useCxnId < 0)
        {
			useCxnId = newCxnId = m_CxnMgr->OpenConnection(endpointId,
														   m_ChannelId,
														   bytes,
														   numBytes,
														   status);
        }

        rverify(useCxnId >= 0, catchall,);

        newPeer = this->AddPeer(useCxnId, endpointId, peerInfo);

        rverify(newPeer, catchall,);

        if(!this->IsHost() && !m_Migrating && m_NumPeers == 1)
        {
            //Assume the first peer to which we connect is the host.
            snAssert(RL_INVALID_PEER_ID == m_HostPeerId);

            snDebug("Peer:" NET_ADDR_FMT " is host", NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(endpointId)));
            m_HostPeerId = peerInfo.GetPeerId();
        }

        // success = true;
    }
    rcatchall
    {
        if(newCxnId >= 0)
        {
            m_CxnMgr->CloseConnection(newCxnId, NET_CLOSE_IMMEDIATELY);
        }

        if(newPeer)
        {
            this->RemovePeer(newPeer);
            newPeer = NULL;
        }

        if(status)
        {
            status->SetPending(); status->SetFailed();
        }
    }

    return newPeer;
}

void
snSession::AddTask(snTask* task)
{
    snAssert(task->IsPending());
    snAssert(!task->IsStarted());

	rlTaskDebug1Ptr(task, "Adding serial task [%p]", task);

    m_TaskQ.push_back(task);

    if(1 == m_TaskQ.size())
    {
        task->Start();
    }
}

void
snSession::AddConcurrentTask(snTask* task)
{
	m_ConcurrentTasks.push_back(task);
}

void
snSession::AddTaskLifo(snTask* task)
{
    snAssert(task->IsPending());
    snAssert(!task->IsStarted());

    //Insert the task after all tasks that have already started.
    TaskQ::iterator it = m_TaskQ.begin();
    TaskQ::const_iterator stop = m_TaskQ.end();
    for(; stop != it; ++it)
    {
        if(!(*it)->IsActive())
        {
            break;
        }
    }

	rlTaskDebug1Ptr(task, "Adding serial task LIFO [%p]", task);

    m_TaskQ.insert(it, task);

    if(1 == m_TaskQ.size())
    {
        task->Start();
    }
}

void
snSession::RemoveTask(snTask* task)
{
    if(!snVerifyf(m_AllowRemoveTask, "RemoveTask :: Not allowed to call!"))
	{
		snError("RemoveTask :: Not allowed to call!");
		return;
	}

	snAssert(!this->Exists() || !task->IsPending());

	rlTaskDebug1Ptr(task, "Removing serial task [%p]", task);

    m_TaskQ.erase(task);

    if(task->m_DeleteWhenFinished)
    {
        Delete(task, *m_Allocator);
    }
}

void
snSession::UpdateTasks(const unsigned timeStep)
{
	m_AllowRemoveTask = true;

	snTask* task;
    TaskQ::iterator it = m_ConcurrentTasks.begin();
    TaskQ::iterator next = it;
    TaskQ::const_iterator stop = m_ConcurrentTasks.end();

    for(++next; stop != it; it = next, ++next)
    {
        task = *it;

        if(!task->IsStarted())
        {
            task->Start();
        }

        if(task->IsActive())
        {
            task->Update(timeStep);
        }

        if(task->IsFinished())
        {
            m_ConcurrentTasks.erase(task);

            if(task->m_DeleteWhenFinished)
            {
                Delete(task, *m_Allocator);
            }
        }
        else
        {
            snAssert(task->IsActive());
#if !__NO_OUTPUT
			if(!task->IsActive())
			{
				rlTaskErrorPtr(task, "Inactive concurrent task!");
			}
#endif
        }
    }

    task = m_TaskQ.empty() ? NULL : m_TaskQ.front();

    if(task)
    {
        do
        {
            if(!task->IsStarted())
            {
                task->Start();
            }

            if(task->IsActive())
            {
                task->Update(timeStep);
            }

            if(task->IsFinished())
            {
				this->RemoveTask(task);
				
                task = m_TaskQ.empty() ? NULL : m_TaskQ.front();

                if(task && task->IsPending() && !task->IsStarted())
                {
                    task->Start();
                }
            }
            else
            {
                snAssert(task->IsActive());
#if !__NO_OUTPUT
				if(!task->IsActive())
				{
					rlTaskErrorPtr(task, "Inactive serial task!");
				}
#endif
            }
        }
        while(task && !task->IsActive());
    }
	
	m_AllowRemoveTask = false;
}

void
snSession::Clear()
{
    //It's important that this is set before calling any other
    //functions, so those functions won't think we're the host.
    m_HostPeerId = RL_INVALID_PEER_ID;

    this->SetAcceptingJoinRequests(false);
    this->DisableNetwork();

	// avoid an infinite loop if RemoveGamer() doesn't decrement m_NumGamers
	// (this was exploited by injecting an invalid gamer handle into snMsgAddGamerToSessionCmd)
	unsigned numGamers = m_NumGamers;
    while(numGamers)
    {
        this->RemoveGamer(m_Gamers[0]->m_GamerInfo.GetGamerId(), -1);
		--numGamers;
    }

    snAssert(this->GetGamerCount() == 0);

	// avoid an infinite loop if RemovePeer() doesn't decrement m_NumPeers
	unsigned numPeers = m_NumPeers;
    while(numPeers)
    {
        this->RemovePeer(m_Peers[0]);
		--numPeers;
    }

#if RSG_DURANGO
	if (m_DisplayNameStatus.Pending())
	{
		netTask::Cancel(&m_DisplayNameStatus);
		m_DisplayNameStatus.Reset();
	}
	m_DirtyDisplayNames = false;
	m_PendingDisplayNames = 0;
	m_DisplayNameRetrievalTime = 0;
	m_CurrentDisplayNameBackoffTimer = 0;
#endif

    snAssert(this->GetPeerCount() == 0);

    snAssert(m_GamerPool.size() == COUNTOF(m_GamerPile));
    snAssert(m_PeerPool.size() == COUNTOF(m_PeerPile));

    m_LocalPeer.Shutdown();
    m_NumOccupiedPrivSlots = 0;
    m_NumReservedSlots = 0;
	m_bNeedToUpdateReservations = false;
    m_PresenceEnabled = false;
    m_Migrating = false;
	m_IsEstablished = false;
}

void
snSession::PrintMembers() const
{
#if !__NO_OUTPUT

    snDebug("Session members: %d", m_NumGamers);

    for(int i = 0; i < (int) m_NumGamers; ++i)
    {
        const snGamer* gamer = m_Gamers[i];
        const u64 peerId = gamer->GetGamerInfo().GetPeerInfo().GetPeerId();
		EndpointId endpointId;
        snVerify(this->GetPeerEndpointId(peerId, &endpointId));
        const char* slotStr;

        if(RL_SLOT_PRIVATE == gamer->m_SlotType)
        {
            slotStr = "prv";
        }
        else if(RL_SLOT_PUBLIC == gamer->m_SlotType)
        {
            slotStr = "pub";
        }
        else
        {
            slotStr = "***";
        }

        const bool isHost = (peerId == m_HostPeerId);

        snDebug("    (0x%016" I64FMT "x) %-20s (%s) [" NET_ADDR_FMT "] %s",
                peerId,
                gamer->GetGamerInfo().GetName(),
                slotStr,
                NET_IS_VALID_ENDPOINT_ID(endpointId) ? NET_ADDR_FOR_PRINTF(m_CxnMgr->GetAddress(endpointId)) : "",
                isHost ? "(HOST)" : "");
    }

#endif  //!__NO_OUTPUT
}

void 
snSession::DispatchEvent(snEvent* pEvent)
{
	int localGamerIndex = -1;
	bool leftGame = false;
	bool isHost = false;
	if(SNET_EVENT_ADDED_GAMER == pEvent->GetId())
	{
		const snEventAddedGamer* ag = (const snEventAddedGamer*) pEvent;
#if HACK_GTA4
		if(ag->m_GamerInfo.IsLocal() && !ag->m_GamerInfo.GetGamerHandle().IsBot())
#else
		if(ag->m_GamerInfo.IsLocal())
#endif
		{
			//We joined a session.  Set our GameSessionId and IsHost attributes.
			snAssert(m_LocalOwnerIndex < 0);
			localGamerIndex = m_LocalOwnerIndex = ag->m_GamerInfo.GetLocalIndex();
			isHost = IsHost();
		}
	}
	else if(SNET_EVENT_REMOVED_GAMER == pEvent->GetId())
	{
		const snEventRemovedGamer* rg = (const snEventRemovedGamer*) pEvent;
#if HACK_GTA4
		if(rg->m_GamerInfo.IsLocal() && !rg->m_GamerInfo.GetGamerHandle().IsBot())
#else
		if(rg->m_GamerInfo.IsLocal())
#endif
		{
			//We left a session.  Set our GameSessionId and IsHost attributes.
			localGamerIndex = rg->m_GamerInfo.GetLocalIndex();
			leftGame = true;

			snAssert(m_LocalOwnerIndex == localGamerIndex);
		}
	}
	else if(SNET_EVENT_SESSION_DESTROYED == pEvent->GetId())
	{
		//We left a session.  Set our GameSessionId and IsHost attributes.
		localGamerIndex = m_LocalOwnerIndex;
		m_LocalOwnerIndex = -1;
		leftGame = true;
	}
	else if(SNET_EVENT_SESSION_MIGRATE_END == pEvent->GetId())
	{
		//Host migrated, possibly to us.  Update our GameSessionid and IsHost attributes.
		snAssert(m_LocalOwnerIndex >= 0);
		localGamerIndex = m_LocalOwnerIndex;
		isHost = IsHost();
	}

	UpdatePresenceAttributes(localGamerIndex, leftGame, isHost);

	// dispatch to delegate
	m_Delegator.Dispatch(this, pEvent);
}

void
snSession::UpdatePresenceAttributes(const int localGamerIndex, const bool leftGame, const bool isHost)
{
    if(localGamerIndex >= 0
        //Signing out could have caused REMOVED_GAMER or
        //a SESSION_DESTROYED event to occur
        && rlPresence::IsSignedIn(localGamerIndex))
    {
        //Do not advertise single player private sessions
        bool isPrivate = false;
        if(Exists() && GetMaxSlots(RL_SLOT_PRIVATE) == 1 && GetMaxSlots(RL_SLOT_PUBLIC) == 0)
        {
            isPrivate = true; 
        }

        rlSessionInfo sinfo;
        if(!leftGame && !isPrivate && Exists())
        {
            sinfo = GetSessionInfo();
        }

        if(this->IsOnline() && sinfo.IsValid())
        {
            //Set presence attributes
            if(m_AttrSessionToken[0] != '\0')
            {
                rlPresenceAttributeHelper::SetSessionToken(localGamerIndex, sinfo.GetToken(), m_AttrSessionToken);
            }

			if(m_AttrSessionId[0] != '\0')
			{
				rlPresenceAttributeHelper::SetSessionId(localGamerIndex, GetSessionId(), m_AttrSessionId);
			}

            if(m_AttrSessionInfo[0] != '\0')
            {
                rlPresenceAttributeHelper::SetSessionInfo(localGamerIndex, sinfo, m_AttrSessionInfo);
            }

            if(m_AttrIsHost[0] != '\0')
            {
                rlPresenceAttributeHelper::SetBool(localGamerIndex, isHost, m_AttrIsHost);
            }

            if(m_AttrIsJoinable[0] != '\0')
            {
                rlPresenceAttributeHelper::SetBool(localGamerIndex, sinfo.IsValid() && IsJoinableViaPresence(), m_AttrIsJoinable);
            }
        }
        else
        {
            //also need to keep these attributes up to date if we leave a match or invalidate them if we host a private session, etc.
            if(m_AttrSessionToken[0] != '\0')
            {
                rlSessionToken emptyToken;
                rlPresenceAttributeHelper::SetSessionToken(localGamerIndex, emptyToken, m_AttrSessionToken);
            }

			if(m_AttrSessionId[0] != '\0')
			{
				u64 invalidSessionId = 0;
				rlPresenceAttributeHelper::SetSessionId(localGamerIndex, invalidSessionId, m_AttrSessionId);
			}

            if(m_AttrSessionInfo[0] != '\0')
            {
                rlSessionInfo emptySession;
                rlPresenceAttributeHelper::SetSessionInfo(localGamerIndex, emptySession, m_AttrSessionInfo);
            }

            if(m_AttrIsHost[0] != '\0')
            {
                rlPresenceAttributeHelper::SetBool(localGamerIndex, false, m_AttrIsHost);
            }

            if(m_AttrIsJoinable[0] != '\0')
            {
                rlPresenceAttributeHelper::SetBool(localGamerIndex, false, m_AttrIsJoinable);
            }
        }

#if RSG_PC
		if(g_rlPc.IsInitialized())
		{
			// Only update the SCUI with the game session, ignoring transition sessions etc.
			// If the m_AttrSessionInfo is not set, we'll allow it as well.
			if (m_AttrSessionInfo[0] == '\0' || strcmp(m_AttrSessionInfo, rlScAttributeId::GameSessionInfo.Name) == 0)
			{
				bool isGameInvitable = sinfo.IsValid() && IsInvitable();
				char sinfoBuf[rlSessionInfo::TO_STRING_BUFFER_SIZE] = {0};

				if(isGameInvitable)
				{
					snVerifyf(sinfo.ToString(sinfoBuf),
						"Error serializing game session info");
				}

				g_rlPc.GetPresenceManager()->AllowGameInvites(isGameInvitable);
				g_rlPc.GetPresenceManager()->SetGameSessionInfo(sinfoBuf);
				snDebug3("UpdatePresenceAttributes - Allow Game Invites (%d), Game Session Info: (%s)", isGameInvitable, sinfoBuf);
			}
			else if (m_AttrSessionInfo[0] != '\0' && g_rlPc.GetAdditionalSessionAttr()[0] != '\0' &&
					strcmp(m_AttrSessionInfo, g_rlPc.GetAdditionalSessionAttr()) == 0)
			{
				bool isGameInvitable = sinfo.IsValid() && IsInvitable();
				char sinfoBuf[rlSessionInfo::TO_STRING_BUFFER_SIZE] = {0};

				if(isGameInvitable)
				{
					snVerifyf(sinfo.ToString(sinfoBuf),
						"Error serializing game session info");
				}
				
				g_rlPc.GetPresenceManager()->AllowAdditionalSessionGameInvites(isGameInvitable);
				g_rlPc.GetPresenceManager()->SetAdditionalSessionInfo(sinfoBuf);
				snDebug3("UpdatePresenceAttributes - Allow Additional Session Game Invites (%d), Game Session Info: (%s)", isGameInvitable, sinfoBuf);
			}
		}
#endif
    }
    else
    {
        if(m_AttrIsJoinable[0] != '\0' && !Exists())
        {
            // make sure we're not advertising a joinable state when we're not in a session.
            // this is necessary because if the player failed to join a session, it would set
            // the isJoinable flag to true and never set it to false after failing to join.
            for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
            {
                if(rlPresence::IsSignedIn(i))
                {
                    //Avoid lots of debug spew by setting the "joinable" attribute only
                    //if it differs from the current value.
                    bool isJoinable;
                    if(rlPresenceAttributeHelper::GetBool(i, isJoinable, m_AttrIsJoinable)
                        && isJoinable == true)
                    {
                        rlPresenceAttributeHelper::SetBool(i, false, m_AttrIsJoinable);
                    }
                }
            }
        }
    }
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT RAGE_LOG_FMT_DEFAULT

}   //namespace rage
