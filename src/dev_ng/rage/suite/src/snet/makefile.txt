Project snet

IncludePath $(RAGE_DIR)\base\src

Files {
	messages.cpp
	messages.h
	party.cpp
	party.h
	session.cpp
	session.h
	snet.cpp
	snet.h
	tasks.cpp
	tasks.h
}
