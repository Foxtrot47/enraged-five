// 
// snet/snet.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "snet.h"
#include "system/param.h"

namespace rage
{
RAGE_DEFINE_CHANNEL(snet,
					DIAG_SEVERITY_DEBUG1,
					DIAG_SEVERITY_DISPLAY, 
					DIAG_SEVERITY_ASSERT,
					CHANNEL_POSIX_ON);

}   //namespace rage
