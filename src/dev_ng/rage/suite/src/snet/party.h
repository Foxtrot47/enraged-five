//snet/session.h 
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNET_PARTY_H
#define SNET_PARTY_H

#include "session.h"

namespace rage
{

//Party event ids.
enum
{
	//A member was added to the party.
	SNPARTY_EVENT_ADDED_MEMBER,
	//A member was removed from the party.
	SNPARTY_EVENT_REMOVED_MEMBER,
	//The party leader changed.
	SNPARTY_EVENT_LEADER_CHANGED,
	//Leader has notified us that we're entering a game.
	SNPARTY_EVENT_ENTER_GAME,
	//Leader notified us that we're leaving a game.
	SNPARTY_EVENT_LEAVE_GAME,
	//Our attempt to join a party failed.
	SNPARTY_EVENT_JOIN_FAILED
};

#define SNPARTY_EVENT_COMMON_DECL(name)\
	static unsigned EVENT_ID() {return name::GetAutoId();}\
	virtual unsigned GetId() const {return name::GetAutoId();}

#define SNPARTY_EVENT_DECL(name, id)\
	AUTOID_DECL_ID(name, snPartyEvent, id)\
	SNPARTY_EVENT_COMMON_DECL(name)

class snParty;
class snPartyEventAddedMember;
class snPartyEventRemovedMember;
class snPartyEventLeaderChanged;
class snPartyEventEnterGame;
class snPartyEventLeaveGame;
class snPartyEventJoinFailed;

//PURPOSE
//  Base class for all snet session event classes.
class snPartyEvent
{
	friend class snParty;

public:

	AUTOID_DECL_ROOT(snPartyEvent);
	SNPARTY_EVENT_COMMON_DECL(snPartyEvent);

	snPartyEvent()
	{
		m_Event = this;
	}

	virtual ~snPartyEvent() {}

	//Easy access to specific event type.  Use the event id
	//to determine which pointer to use.
	union
	{
		snPartyEvent*               m_Event;
		snPartyEventAddedMember*    m_AddedMember;
		snPartyEventRemovedMember*  m_RemovedMember;
		snPartyEventLeaderChanged*  m_LeaderChanged;
		snPartyEventEnterGame*      m_EnterGame;
		snPartyEventLeaveGame*      m_LeaveGame;
		snPartyEventJoinFailed*     m_JoinFailed;
	};

private:

	inlist_node<snEvent> m_ListLink;
};

//PURPOSE
//  Dispatched when a member is added to the party.
class snPartyEventAddedMember : public snPartyEvent
{
public:

	SNPARTY_EVENT_DECL(snPartyEventAddedMember, SNPARTY_EVENT_ADDED_MEMBER);

	snPartyEventAddedMember(const rlGamerInfo& gamer,
			                const EndpointId endpointId,
			                const rlSlotType slotType,
			                const void* data,
			                const unsigned sizeofData)
		: m_GamerInfo(gamer)
		, m_EndpointId(endpointId)
		, m_SlotType(slotType)
		, m_Data(data)
		, m_SizeofData(sizeofData)
	{
	}

	//Gamer that was added
	rlGamerInfo m_GamerInfo;
	EndpointId m_EndpointId;
	rlSlotType m_SlotType;
	const void* m_Data;
	unsigned m_SizeofData;
};

//PURPOSE
//  Dispatched when a member is removed from the party.
class snPartyEventRemovedMember : public snPartyEvent
{
public:

	SNPARTY_EVENT_DECL(snPartyEventRemovedMember, SNPARTY_EVENT_REMOVED_MEMBER);

	snPartyEventRemovedMember(const rlGamerInfo& gamer)
		: m_GamerInfo(gamer)
	{
	}

	//Gamer that was removed
	rlGamerInfo m_GamerInfo;
};

//PURPOSE
//  Dispatched when there's a new leader of the party.
class snPartyEventLeaderChanged : public snPartyEvent
{
public:

	SNPARTY_EVENT_DECL(snPartyEventLeaderChanged, SNPARTY_EVENT_LEADER_CHANGED);

	snPartyEventLeaderChanged(const rlGamerInfo& newLeader)
		: m_NewLeader(newLeader)
	{
	}

	//The new leader
	rlGamerInfo m_NewLeader;
};

//PURPOSE
//  Dispatched when a party leader is joining a game session.
class snPartyEventEnterGame : public snPartyEvent
{
public:

	SNPARTY_EVENT_DECL(snPartyEventEnterGame, SNPARTY_EVENT_ENTER_GAME);

	snPartyEventEnterGame(const rlSessionInfo& sessionInfo)
		: m_SessionInfo(sessionInfo)
	{
	}

	//Session info for game to enter
	rlSessionInfo m_SessionInfo;
};

//PURPOSE
//  Dispatched when a party leader left a game session.
class snPartyEventLeaveGame : public snPartyEvent
{
public:

	SNPARTY_EVENT_DECL(snPartyEventLeaveGame, SNPARTY_EVENT_LEAVE_GAME);

	snPartyEventLeaveGame()
	{
	}
};

//PURPOSE
//  Dispatched when a request to join a party fails.
class snPartyEventJoinFailed: public snPartyEvent
{
public:

	SNPARTY_EVENT_DECL(snPartyEventJoinFailed, SNPARTY_EVENT_JOIN_FAILED);

	snPartyEventJoinFailed(const rlSessionInfo& sessionInfo,
						   snJoinResponseCode responseCode,
						   const void* response,
						   const unsigned sizeofResponse)
		: m_SessionInfo(sessionInfo)
		, m_ResponseCode(responseCode)
		, m_Response(response)
		, m_SizeofResponse(sizeofResponse)
	{
		Assert(SNET_JOIN_ACCEPTED != responseCode);
	}

	rlSessionInfo m_SessionInfo;
	snJoinResponseCode m_ResponseCode;

	//App-specific data provided by the host.
	const void* m_Response;
	unsigned m_SizeofResponse;
};

class sysMemAllocator;
class netConnectionManager;
class LeavePartyTask;

class snParty
{
	friend class LeavePartyTask;

	typedef atDelegator<void (snParty*, const snPartyEvent*)> Delegator;

public:

	typedef Delegator::Delegate Delegate;

	snParty();

	~snParty();

	//PURPOSE
	//  Initialize the party instance.
	//PARAMS
	//  allocator   - Used to allocate memory.
	//  sessionCb   - Collection of callbacks implemented by
	//                the session owner.
	//  cxnMgr      - Connection manager - used to send network messages.
	//  channelId   - Channel on which party network traffic is sent.
	bool Init(sysMemAllocator* allocator,
			  snSessionOwner sessionCb,
			  netConnectionManager* cxnMgr,
			  const unsigned channelId);

	//PURPOSE
	//  Shuts down the party instance.
	void Shutdown();

	//PURPOSE
	//  Adds a delegate that will be called with event notifications.
	void AddDelegate(Delegate* dlgt);

	//PURPOSE
	//  Removes a delegate.
	void RemoveDelegate(Delegate* dlgt);

	//PURPOSE
	//  Returns true if the party exists.
	bool Exists() const;

	//PURPOSE
	//  Hosts a new party
	//PARAMS
	//  localGamerIndex     - Local index of gamer hosting the party
	//  attrs               - Matchmaking attributes associated with the party.
	//  create flags        - From rlSessionCreateFlags.
	//  numSlots            - Number of slots in the party.
	//NOTES
	//  A party is not actually advertised on matchmaking, but it can still have
	//  matchmaking attributes associated with it.
	bool Host(const int localGamerIndex,
			  const rlMatchingAttributes attrs,
			  const int numSlots,
			  netStatus* status);

	//PURPOSE
	//  Joins a party
	//PARAMS
	//  localGamerIndex - Local index of gamer hosting the party
	//  sessionInfo     - Session info of the party to join
	//  joinerData      - Application data sent to the host in the join request
	//                    (in the snMsgJoinRequest message).
	//  sizeofJoinerData    - Size of application data.
	bool Join(const int localGamerIndex,
			  const rlSessionInfo& sessionInfo,
			  const void* joinerData,
			  const unsigned sizeofJoinerData,
			  netStatus* status);

	//PURPOSE
	//  Leaves a party
	bool Leave(netStatus* status);

	//PURPOSE
	//  Kicks players from the party.  Only the party leader can kick.
	//PARAMS
	//  kickees     - Remote gamers to kick.
	//  numKickees  - Number of remote gamers to kick.
	//  kickType    - One of the KickType enumerants.
	//  status      - Optional status object that can be polled for completion.
	//RETURNS
	//  True if the asynchronous operation was initiated.
	bool Kick(const rlGamerHandle* kickees,
			  const unsigned numKickees,
			  const snSession::KickType kickType,
			  netStatus* status);

	//PURPOSE
	//  Called by the party leader to notify party members that the
	//  party is joining a game session.
	//PARAMS
	//  sessionInfo     - Session info for the game session to join.
	bool NotifyEnteringGame(const rlSessionInfo& sessionInfo);

	//PURPOSE
	//  Called by the party leader to notify party members that the
	//  party is leaving a game session.
	bool NotifyLeavingGame();

	//PURPOSE
	//  Returns true if invites can be sent.
	bool IsInvitable() const;

	//PURPOSE
	//  Returns true if another player can join this session via presence.
	bool IsJoinableViaPresence() const;

	//PURPOSE
	//  Destroys the party
	bool Destroy(netStatus* status);

	//PURPOSE
	//  Sends game invites to the given players
	//PARAMS
	//  gamerHandles    - Handles for gamers to invite
	//  numGamers       - Number of gamer handles
	//  salutation      - String to display with invitation.
	bool SendGameInvites(const rlGamerHandle* gamerHandles,
			             const unsigned numGamers,
			             const char* subject, 
			             const char* salutation,
			             netStatus* status);

	//PURPOSE
	//  Sends invites to the party to the given players
	//PARAMS
	//	localGamerIndex	- Local gamer index of the inviter
	//  gamerHandles    - Handles for gamers to invite
	//  numGamers       - Number of gamer handles
	//  salutation      - String to display with invitation.
	bool SendPartyInvites(const int localGamerIndex,
		                  const rlGamerHandle* gamerHandles,
		                  const unsigned numGamers,
		                  const char* salutation,
		                  netStatus* status);

	//PURPOSE
	//  Called once per frame to update the state of the party
	void Update();

	//PURPOSE
	//  Returns true if the local gamer is the party leader
	bool IsLeader() const;

	//PURPOSE
	//  Returns number of session members.
	unsigned GetGamerCount() const;

	//PURPOSE
	//  Returns true if the gamer is a member of the session.
	bool IsMember(const rlGamerHandle& gamerHandle) const;

	//PURPOSE
	//  Returns true if the gamer is a member of the session.
	bool IsMember(const rlGamerId& gamerId) const;

	//PURPOSE
	//  Retrieves the members of the party.
	//PARAMS
	//  members     - Array of members to be populated
	//  maxMembers  - Maximum size of the array
	//RETURNS
	//  Number of members populated.
	unsigned GetMembers(rlGamerInfo* members,
			            const unsigned maxMembers) const;
	unsigned GetMembers(rlGamerId* members,
			            const unsigned maxMembers) const;
	unsigned GetMembers(rlGamerHandle* members,
			            const unsigned maxMembers) const;

	//PURPOSE
	//  Retrieves the network endpoint id of the peer.
	//PARAMS
	//  peerId      - Id of the peer.
	//  endpointId  - On return contains the endpointId.
	//RETURNS
	//  True on success;
	bool GetPeerEndpointId(const u64 peerId, 
			               EndpointId* endpointId) const;

	//PURPOSE
	//  Retrieves the party leader.
	bool GetLeader(rlGamerInfo* leader) const;

	//PURPOSE
	//  Sends a message to a party member
	//PARAMS
	//  peerId      - Peer id of the recipient.
	//  msg         - The message.
	//  sendFlags   - Bit mask composed of netSendFlags enum.
	template<typename T>
	void SendMsg(const u64 peerId,
			     const T& msg,
			     const unsigned sendFlags) const
	{
		m_Session.SendMsg(peerId, msg, sendFlags);
	}

	//PURPOSE
	//  Broadcasts a message to all party members.
	template<typename T>
	void BroadcastMsg(const T& msg, const unsigned sendFlags) const
	{
		m_Session.BroadcastMsg(msg, sendFlags);
	}

	//PURPOSE
	//  Returns the owner of the local session instance.
	const rlGamerInfo& GetOwner() const;

	//PURPOSE
	//  Returns the globally unique session id.
	//  This ID *must not* be saved beyond the lifetime of
	//  the session, as session IDs can be reused.
	u64 GetSessionId() const;

	//PURPOSE
	//  Returns the session info for the party
	const rlSessionInfo& GetSessionInfo() const;

private:

	template<typename T>
	bool ValidateMsg(const netEventFrameReceived* frame,
			         T* msg) const;

	void OnSessionEvent(snSession* session,
			            const snEvent* event);

	void OnNetEvent(netConnectionManager* cxnMgr,
			        const netEvent* evt);

	snSession m_Session;

	Delegator m_Delegator;

	netTimeStep m_TimeStep;

	int m_LocalGamerIndex;

	snSession::Delegate m_SessionEventDelegate;
	netConnectionManager::Delegate m_CxnMgrDelegate;

	bool m_IsInitialized : 1;
};

}   //namespace rage;

#endif  //SNET_PARTY_H
