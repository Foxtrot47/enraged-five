// 
// snet/session.cpp 
// 
// Copyright (C) Forever Rockstar Games.  All Rights Reserved. 
// 

#include "party.h"

#include "diag/output.h"
#include "diag/seh.h"
#include "rline/rlpresence.h"
#include "rline/scpresence/rlscpresencemessage.h"
#include "system/param.h"
#include "system/timer.h"

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(snet, party)
#undef __rage_channel
#define __rage_channel snet_party

//#undef RAGE_LOG_FMT
//#define RAGE_LOG_FMT(fmt) "0x%016" I64FMT "x: "fmt, this->GetId()

struct msgPartyEnterGame
{
	NET_MESSAGE_DECL(msgPartyEnterGame, MSG_PARTY_ENTER_GAME);

	void Reset(const u64 sessionId,
		       const rlSessionInfo& sessionInfo)
	{
		m_SessionId = sessionId;
		m_SessionInfo = sessionInfo;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerUns(msg.m_SessionId, 64)
			&& snVerify(msg.m_SessionId != rlSession::INVALID_SESSION_ID)
			&& bb.SerUser(msg.m_SessionInfo);
	}

	u64 m_SessionId;
	rlSessionInfo m_SessionInfo;
};

struct msgPartyLeaveGame
{
	NET_MESSAGE_DECL(msgPartyLeaveGame, MSG_PARTY_LEAVE_GAME);

	void Reset(const u64 sessionId)
	{
		m_SessionId = sessionId;
	}

	NET_MESSAGE_SER(bb, msg)
	{
		return bb.SerUns(msg.m_SessionId, 64)
			&& snVerify(msg.m_SessionId != rlSession::INVALID_SESSION_ID);
	}

	u64 m_SessionId;
};

NET_MESSAGE_IMPL(msgPartyEnterGame);
NET_MESSAGE_IMPL(msgPartyLeaveGame);

AUTOID_IMPL(snPartyEvent);
AUTOID_IMPL(snPartyEventAddedMember);
AUTOID_IMPL(snPartyEventRemovedMember);
AUTOID_IMPL(snPartyEventLeaderChanged);
AUTOID_IMPL(snPartyEventEnterGame);
AUTOID_IMPL(snPartyEventLeaveGame);
AUTOID_IMPL(snPartyEventJoinFailed);

//////////////////////////////////////////////////////////////////////////
//  LeavePartyTask
//////////////////////////////////////////////////////////////////////////
class LeavePartyTask : public rlTaskBase
{
public:
	SN_TASK_DECL(LeavePartyTask, snet_party);

	LeavePartyTask();

	bool Configure(snParty* party,
		           const int localGamerIndex);

	virtual void Update(const unsigned timeStep);

	virtual bool IsCancelable() const
	{
		return true;
	}

private:

	void DoCancel();

	enum State
	{
		STATE_LEAVE_PARTY,
		STATE_LEAVING_PARTY,
		STATE_DESTROY,
		STATE_DESTROYING,
	};

	State m_State;
	snParty* m_Party;
	int m_LocalGamerIndex;

	netStatus m_MyStatus;;
};

LeavePartyTask::LeavePartyTask()
	: m_State(STATE_LEAVE_PARTY)
	, m_Party(NULL)
	, m_LocalGamerIndex(-1)
{
}

bool
LeavePartyTask::Configure(snParty* party, const int localGamerIndex)
{
	m_Party = party;
	m_LocalGamerIndex = localGamerIndex;

	return true;
}

void
LeavePartyTask::DoCancel()
{
	//Cancel tasks we depend on
	rlGetTaskManager()->CancelTask(&m_MyStatus);
}

void
LeavePartyTask::Update(const unsigned timeStep)
{
	this->rlTaskBase::Update(timeStep);

	if(this->WasCanceled())
	{
		//Wait for dependencies to finish
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_CANCELED);
		}

		return;
	}

	switch(m_State)
	{
	case STATE_LEAVE_PARTY:
		rlTaskDebug("Leaving party session...");

		if(m_Party->m_Session.Leave(m_LocalGamerIndex, &m_MyStatus))
		{
			m_State = STATE_LEAVING_PARTY;
		}
		else
		{
			rlTaskError("Error leaving party session");
			m_State = STATE_DESTROY;
		}
		break;
	case STATE_LEAVING_PARTY:
		if(m_MyStatus.Succeeded())
		{
			m_State = STATE_DESTROY;
		}
		else if(!m_MyStatus.Pending())
		{
			rlTaskError("Error leaving party session");
			m_State = STATE_DESTROY;
		}
		break;
	case STATE_DESTROY:
		if(m_Party->m_Session.Destroy(&m_MyStatus))
		{
			m_State = STATE_DESTROYING;
		}
		else
		{
			rlTaskError("Error destroying party");
			//We still consider the "leave" operation a success.
			this->Finish(FINISH_SUCCEEDED);
		}
		break;
	case STATE_DESTROYING:
		if(!m_MyStatus.Pending())
		{
			this->Finish(FINISH_SUCCEEDED);
		}
		break;
	}
}

//////////////////////////////////////////////////////////////////////////
//  snParty
//////////////////////////////////////////////////////////////////////////
snParty::snParty()
	: m_LocalGamerIndex(-1)
	, m_IsInitialized(false)
{
	m_SessionEventDelegate.Bind(this, &snParty::OnSessionEvent);
	m_CxnMgrDelegate.Bind(this, &snParty::OnNetEvent);
}

snParty::~snParty()
{
	this->Shutdown();
}

bool
snParty::Init(sysMemAllocator* allocator,
			  snSessionOwner sessionCb,
			  netConnectionManager* cxnMgr,
			  const unsigned channelId)
{
	Assert(allocator);
	Assert(cxnMgr);

	bool success = false;

	if(AssertVerify(!m_IsInitialized))
	{
		rtry
		{
			rverify(m_Session.Init(allocator,
				                   sessionCb,
				                   cxnMgr,
				                   channelId,
				                   "PARTY"),catchall,);

			m_Session.AddDelegate(&m_SessionEventDelegate);

            cxnMgr->AddChannelDelegate(&m_CxnMgrDelegate, channelId);

			m_LocalGamerIndex = -1;

			m_IsInitialized = success = true;
		}
		rcatchall
		{
		}
	}

	return success;
}

void
snParty::Shutdown()
{
	m_CxnMgrDelegate.Unregister();
	m_Session.Shutdown(false, -1);
	m_LocalGamerIndex = -1;
	m_IsInitialized = false;
}

void
snParty::AddDelegate(Delegate* dlgt)
{
	m_Delegator.AddDelegate(dlgt);
}

void
snParty::RemoveDelegate(Delegate* dlgt)
{
	m_Delegator.RemoveDelegate(dlgt);
}

bool
snParty::Exists() const
{
	return m_Session.Exists();
}

bool
snParty::Host(const int localGamerIndex,
			  const rlMatchingAttributes attrs,
			  const int numSlots,
			  netStatus* status)
{
	snDebug("Local gamer: %d is hosting a party...", localGamerIndex);

	if(snVerify(!this->Exists()))
	{
		if(m_Session.Host(localGamerIndex,
						  RL_NETMODE_ONLINE,
						  0,        //public slots
						  numSlots, //private slots
						  attrs,
						  0,        //create flags
						  NULL,     //owner data
						  0,        //size of owner data
						  status))
		{
			m_LocalGamerIndex = localGamerIndex;
			return true;
		}
	}

	return false;
}

bool
snParty::Join(const int localGamerIndex, 
              const rlSessionInfo& sessionInfo, 
			  const void* joinerData, 
			  const unsigned sizeofJoinerData, 
			  netStatus* status)
{
	snDebug("Local gamer: %d is joining a party...", localGamerIndex);

	rlGamerHandle gamerHandle;
	if(snVerify(!this->Exists())
		&& snVerify(rlPresence::GetGamerHandle(localGamerIndex, &gamerHandle)))
	{
		if(m_Session.Join(localGamerIndex,
						  sessionInfo,
						  RL_NETMODE_ONLINE,
						  RL_SLOT_PRIVATE,
						  0,      //create flags
						  sizeofJoinerData ? joinerData : NULL,
						  sizeofJoinerData,
						  &gamerHandle,
						  1,
						  status))
		{
			m_LocalGamerIndex = localGamerIndex;
			return true;
		}
	}

	return false;
}

bool
snParty::Leave(netStatus* status)
{
	bool success = false;

	snDebug("Local gamer: %d is leaving party...", m_LocalGamerIndex);

	LeavePartyTask* task = NULL;

	rtry
	{
		rverify(m_IsInitialized, catchall,);
		rverify(this->Exists(),catchall,snError("Party doesn't exist"));
		rverify(rlGetTaskManager()->CreateTask(&task),catchall,);
		rverify(rlTaskBase::Configure(task,
			                          this,
			                          m_LocalGamerIndex,
			                          status),catchall,);
		rverify(rlGetTaskManager()->AppendSerialTask((size_t) this, task), catchall,);

		success = true;
	}
	rcatchall
	{
		if(task)
		{
			rlGetTaskManager()->DestroyTask(task);
		}
	}

	return success;
}

bool
snParty::Kick(const rlGamerHandle* kickees,
	          const unsigned numKickees,
	          const snSession::KickType kickType,
	          netStatus* status)
{
	return rlVerifyf(this->IsLeader(), "Only the party leader can call Kick()")
		&& m_Session.Kick(kickees, numKickees, kickType, status);
}

bool
snParty::NotifyEnteringGame(const rlSessionInfo& gameSessionInfo)
{
	bool success = false;

	if(snVerify(this->IsLeader()))
	{
		msgPartyEnterGame msg;
		msg.Reset(m_Session.GetSessionId(), gameSessionInfo);
		m_Session.BroadcastMsg(msg, NET_SEND_RELIABLE);

		success = true;
	}

	return success;
}

bool
snParty::NotifyLeavingGame()
{
	bool success = false;

	if(snVerify(this->IsLeader()))
	{
		msgPartyLeaveGame msg;
		msg.Reset(m_Session.GetSessionId());
		m_Session.BroadcastMsg(msg, NET_SEND_RELIABLE);

		success = true;
	}

	return success;
}

bool
snParty::IsInvitable() const
{
	return m_Session.IsInvitable();
}

bool
snParty::IsJoinableViaPresence() const
{
	return m_Session.IsJoinableViaPresence();
}

bool
snParty::Destroy(netStatus* status)
{
	return m_Session.Destroy(status);
}

bool 
snParty::SendGameInvites(const rlGamerHandle* gamerHandles, 
                         const unsigned numGamers, 
						 const char* subject, 
						 const char* salutation, 
						 netStatus* status)
{
	snDebug("Sending invites...");

	if(this->IsInvitable())
	{
		if(m_Session.SendInvites(gamerHandles, numGamers, subject, salutation, status))
		{
			return true;
		}
		else
		{
			snError("Error sending invites");
			return false;
		}
	}
	else
	{
		snError("Invites are disabled");
		return false;
	}
}

bool
snParty::SendPartyInvites(const int localGamerIndex, 
                          const rlGamerHandle* gamerHandles, 
						  const unsigned numGamers, 
						  const char* salutation, 
						  netStatus* status)
{
	snDebug("Sending party invites...");

	bool success = false;

	rtry
	{
		if(!salutation)
		{
			salutation = "";
		}

		rverify(strlen(salutation) < RL_MAX_INVITE_SALUTATION_CHARS, catchall, );

		rverify(this->IsInvitable(), catchall, snError("Invites are disabled"));

		rlScPresenceMessageMpPartyInvite partyInvite;
		rverify(rlPresence::GetGamerHandle(localGamerIndex, &partyInvite.m_InviterGamerHandle), catchall, );
		rverify(rlPresence::GetName(localGamerIndex, partyInvite.m_InviterName), catchall, );
		partyInvite.m_SessionInfo = m_Session.GetSessionInfo();
		safecpy(partyInvite.m_Salutation, salutation);

		// cap the number of recipients we send to at RLSC_PRESENCE_MAX_MESSAGE_RECIPIENTS
		int numRecipientsToSendTo = numGamers;
		if(!rlVerify(numRecipientsToSendTo <= RLSC_PRESENCE_MAX_MESSAGE_RECIPIENTS))
		{
			rlDebug("SendPartyInvites :: Too many recipients: %d, Capping at: %d", numGamers, numRecipientsToSendTo);
			numRecipientsToSendTo = RLSC_PRESENCE_MAX_MESSAGE_RECIPIENTS;
		}

		rverify(rlPresence::PostMessage(localGamerIndex, gamerHandles, numRecipientsToSendTo, partyInvite, 0), catchall, );

		if (status)
		{
			status->SetPending();
			status->SetSucceeded();
		}

		success = true;
	}
	rcatchall
	{
		if (status)
		{
			status->SetPending();
			status->SetFailed();
		}
	}

	return success;
}

void
snParty::Update()
{
	m_TimeStep.SetTime(sysTimer::GetSystemMsTime());
	m_Session.Update(m_TimeStep.GetCurrent());
	//m_Transactor.Update(m_TimeStep.GetTimeStep());
}

bool
snParty::IsLeader() const
{
	return this->Exists() && m_Session.IsHost();
}

unsigned
snParty::GetGamerCount() const
{
	return m_Session.GetGamerCount();
}

bool
snParty::IsMember(const rlGamerHandle& gamerHandle) const
{
	return m_Session.IsMember(gamerHandle);
}

bool
snParty::IsMember(const rlGamerId& gamerId) const
{
	return m_Session.IsMember(gamerId);
}

unsigned
snParty::GetMembers(rlGamerInfo* members,
	                const unsigned maxMembers) const
{
	return m_Session.GetGamers(members, maxMembers);
}

unsigned
snParty::GetMembers(rlGamerId* members,
	                const unsigned maxMembers) const
{
	return m_Session.GetGamers(members, maxMembers);
}

unsigned 
snParty::GetMembers(rlGamerHandle* members,
				    const unsigned maxMembers) const
{
	return m_Session.GetGamers(members, maxMembers);
}

bool 
snParty::GetPeerEndpointId(const u64 peerId, 
                           EndpointId* endpointId) const
{
	EndpointId epId = m_Session.GetPeerEndpointId(peerId);
	if(NET_IS_VALID_ENDPOINT_ID(epId))
	{
		*endpointId = epId;
		return true;
	}
	return false;
}

bool
snParty::GetLeader(rlGamerInfo* leader) const
{
	bool success = false;
	u64 peerId;
	if(m_Session.GetHostPeerId(&peerId))
	{
		rlGamerInfo gi[RL_MAX_LOCAL_GAMERS];
		const unsigned numGamers =
			m_Session.GetGamers(peerId, gi, RL_MAX_LOCAL_GAMERS);
		if(1 == numGamers)
		{
			*leader = gi[0];
			success = true;
		}
		else
		{
			//We haven't yet added the leader to our local copy of the session.
			Assert(0 == numGamers);
		}
	}

	return success;
}

const rlGamerInfo&
snParty::GetOwner() const
{
	return m_Session.GetOwner();
}

u64
snParty::GetSessionId() const
{
	return snVerify(m_Session.Exists()) ? m_Session.GetSessionId() : 0;
}

const rlSessionInfo&
snParty::GetSessionInfo() const
{
	Assert(m_Session.Exists());
	return m_Session.GetSessionInfo();
}

//private:

#define VALIDATE_RQST_COND(rqst, cond)          \
if (!(cond))                              \
{                                           \
snDebug("Validating " #rqst" failed: "#cond);     \
return false;                           \
}

template<>
bool
snParty::ValidateMsg(const netEventFrameReceived* frame,
			         msgPartyEnterGame* msg) const
{
    //Verify the message came from the host.
    EndpointId leaderEndpointId;
    VALIDATE_RQST_COND(msgPartyEnterGame, frame->m_CxnId >= 0);
    VALIDATE_RQST_COND(msgPartyEnterGame, m_Session.GetHostEndpointId(&leaderEndpointId));
    VALIDATE_RQST_COND(msgPartyEnterGame, leaderEndpointId == frame->GetEndpointId());
    VALIDATE_RQST_COND(msgPartyEnterGame, AssertVerify(msg->Import(frame->m_Payload, frame->m_SizeofPayload)));
    VALIDATE_RQST_COND(msgPartyEnterGame, m_Session.GetSessionId() == msg->m_SessionId);

	return true;
}

template<>
bool
snParty::ValidateMsg(const netEventFrameReceived* frame,
                     msgPartyLeaveGame* msg) const
{
    //Verify the message came from the host.
    EndpointId leaderEndpointId;
    VALIDATE_RQST_COND(msgPartyLeaveGame, frame->m_CxnId >= 0);
    VALIDATE_RQST_COND(msgPartyLeaveGame, m_Session.GetHostEndpointId(&leaderEndpointId));
    VALIDATE_RQST_COND(msgPartyLeaveGame, leaderEndpointId == frame->GetEndpointId());
    VALIDATE_RQST_COND(msgPartyLeaveGame, AssertVerify(msg->Import(frame->m_Payload, frame->m_SizeofPayload)));
    VALIDATE_RQST_COND(msgPartyLeaveGame, m_Session.GetSessionId() == msg->m_SessionId);

	return true;
}

void
snParty::OnSessionEvent(snSession* /*session*/, const snEvent* evt)
{
	switch(evt->GetId())
	{
	case SNET_EVENT_ADDED_GAMER:
		{
			snPartyEventAddedMember e(evt->m_AddedGamer->m_GamerInfo, evt->m_AddedGamer->m_EndpointId, evt->m_AddedGamer->m_SlotType, evt->m_AddedGamer->m_Data, evt->m_AddedGamer->m_SizeofData);
			m_Delegator.Dispatch(this, &e);
		}
		break;
	case SNET_EVENT_REMOVED_GAMER:
		{
			snPartyEventRemovedMember e(evt->m_RemovedGamer->m_GamerInfo);
			m_Delegator.Dispatch(this, &e);
		}
		break;
	case SNET_EVENT_JOIN_FAILED:
		{
			snPartyEventJoinFailed e(evt->m_JoinFailed->m_SessionInfo, evt->m_JoinFailed->m_ResponseCode, evt->m_JoinFailed->m_Response, evt->m_JoinFailed->m_SizeofResponse);
			m_Delegator.Dispatch(this, &e);
		}
		break;
	case SNET_EVENT_SESSION_MIGRATE_END:
		{
			rlGamerInfo gi[RL_MAX_LOCAL_GAMERS];
			const unsigned numGamers = m_Session.GetGamers(evt->m_SessionMigrateEnd->m_NewHost.GetPeerId(), gi, RL_MAX_LOCAL_GAMERS);
			if(snVerify(1 == numGamers))
			{
				snPartyEventLeaderChanged e(gi[0]);
				m_Delegator.Dispatch(this, &e);
			}
		}
		break;
	}
}

void
snParty::OnNetEvent(netConnectionManager* /*cxnMgr*/, const netEvent* evt)
{
	const unsigned evtId = evt->GetId();

	if(NET_EVENT_FRAME_RECEIVED == evtId)
	{
		unsigned msgId;

		if(!this->Exists())
		{
		}
		else if(this->IsLeader())
		{
		}
		else if(netMessage::GetId(&msgId, evt->m_FrameReceived->m_Payload, evt->m_FrameReceived->m_SizeofPayload))
		{
			//We're not the leader.

			if(msgPartyEnterGame::MSG_ID() == msgId)
			{
				msgPartyEnterGame msg;
				if(this->ValidateMsg(evt->m_FrameReceived, &msg))
				{
					snDebug("Received %s from:" NET_ADDR_FMT "", msg.GetMsgName(), NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));

					snPartyEventEnterGame event(msg.m_SessionInfo);
					m_Delegator.Dispatch(this, &event);
				}
			}
			else if(msgPartyLeaveGame::MSG_ID() == msgId)
			{
				msgPartyLeaveGame msg;
				if(this->ValidateMsg(evt->m_FrameReceived, &msg))
				{
					snDebug("Received %s from:" NET_ADDR_FMT "", msg.GetMsgName(), NET_ADDR_FOR_PRINTF(evt->m_FrameReceived->m_Sender));

					snPartyEventLeaveGame event;
					m_Delegator.Dispatch(this, &event);
				}
			}
		}
	}
}

#undef RAGE_LOG_FMT
#define RAGE_LOG_FMT    RAGE_LOG_FMT_DEFAULT

}   //namespace rage
