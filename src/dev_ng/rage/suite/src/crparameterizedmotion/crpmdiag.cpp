//
// crparameterizedmotion/crpmdiag.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "crpmdiag.h"

#include "system/param.h"

namespace rage
{

PARAM( crpmDebug, "Debug level for parameterized motion lib" );

#if __DEV
static const int CRPM_DEFAULT_DEBUG = 2;
#else //__DEV
static const int CRPM_DEFAULT_DEBUG = 0;
#endif //__DEV

int crpmDebugLevel()
{
	static int LEVEL = -1;

	if( LEVEL < 0 )
	{
		if( !PARAM_crpmDebug.Get( LEVEL ) )
		{
			LEVEL = CRPM_DEFAULT_DEBUG;
		}
	}

	return LEVEL;
}

}   //namespace rage

