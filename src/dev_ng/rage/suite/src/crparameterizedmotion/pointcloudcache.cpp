//
// crparameterizedmotion/pointcloudcache.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "pointcloudcache.h"

#include "cranimation/frame.h"
#include "cranimation/framefilters.h"
#include "crclip/clip.h"
#include "crskeleton/skeleton.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmPointCloudCache::crpmPointCloudCache()
: m_Clip(NULL)
, m_WeightSet(NULL)
, m_FilterWeightSet(NULL)
, m_FilterMover(false)
, m_WindowSize(1)
, m_SampleRate(1.f/30.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmPointCloudCache::crpmPointCloudCache(const crClip& clip, const crSkeletonData& skelData, u32 windowSize, float sampleRate, const crWeightSet* weightSet, const crWeightSet* filterWeightSet, bool filterMover)
: m_Clip(NULL)
, m_WeightSet(NULL)
, m_FilterWeightSet(NULL)
, m_FilterMover(false)
, m_WindowSize(1)
, m_SampleRate(1.f/30.f)
{
	Init(clip, skelData, windowSize, sampleRate, weightSet, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

crpmPointCloudCache::~crpmPointCloudCache()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::Init(const crClip& clip, const crSkeletonData& skelData, u32 windowSize, float sampleRate, const crWeightSet* weightSet, const crWeightSet* filterWeightSet, bool filterMover)
{
	SetClip(&clip);
	SetWindowSize(windowSize);
	SetSampleRate(sampleRate);
	SetWeightSet(weightSet);
	SetFilterWeightSet(filterWeightSet);
	SetFilterMover(filterMover);

	Build(skelData);
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::SetClip(const crClip* clip)
{
	if(clip)
	{
		clip->AddRef();
	}
	if(m_Clip)
	{
		m_Clip->Release();
	}
	m_Clip = clip;
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::SetWindowSize(u32 windowSize)
{
	// Assert(windowSize >= 0);
	m_WindowSize = windowSize;
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::SetSampleRate(float sampleRate)
{
	Assert(sampleRate > 0.f);
	m_SampleRate = sampleRate;
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::SetWeightSet(const crWeightSet* weightSet)
{
	// TODO --- ref count?
	m_WeightSet = weightSet;
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::SetFilterWeightSet(const crWeightSet* filterWeightSet)
{
	// TODO --- ref count?
	m_FilterWeightSet = filterWeightSet;
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::SetFilterMover(bool filterMover)
{
	m_FilterMover = filterMover;
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::Shutdown()
{
	Flush();

	SetClip(NULL);
	SetWeightSet(NULL);
	SetFilterWeightSet(NULL);
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::Build(const crSkeletonData& skelData)
{
	crSkeleton skel;
	skel.Init(skelData);

	Mat34V mtx(V_ZERO);
	skel.SetParentMtx(&mtx);

	crFrame frame;
	frame.InitCreateBoneAndMoverDofs(skelData);

	Build(skel, frame);
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::Build(crSkeleton& skel, crFrame& frame)
{
	Assert(m_Clip);
	Flush();

	const u16 moverId = 0;

	int numFrames = int((m_Clip->GetDuration() / m_SampleRate) + 0.5f) + 1;
	m_Cache.Resize(numFrames+1);

	crFrameFilterBoneBasic filter(skel.GetSkeletonData());
	if(m_FilterWeightSet)
	{
		filter.AddWeightSet(*m_FilterWeightSet);
	}
	else
	{
		filter.AddBoneId(0, true, 1.f);
	}
	filter.SetNonBoneDofsAllowed(!m_FilterMover);

	for(int frameIdx=0; frameIdx<numFrames; ++frameIdx)
	{
		int startFrame = frameIdx - m_WindowSize;
		int endFrame = frameIdx + m_WindowSize;

		for(int i=startFrame; i<=endFrame; ++i)
		{
			float time = Clamp(i*m_SampleRate, 0.f, m_Clip->GetDuration());

			m_Clip->Composite(frame, time, time);

			frame.Pose(skel, false, &filter);

			Mat34V* parentMtx = const_cast<Mat34V*>(skel.GetParentMtx());
			if(parentMtx)
			{
				frame.GetMoverMatrix(moverId, *parentMtx);
			}
			skel.Update();

			m_Cache[frameIdx].AddPose(skel, m_WeightSet);
		}

		m_Bound.Adjust(m_Cache[frameIdx].GetBound().m_MinPoint);
		m_Bound.Adjust(m_Cache[frameIdx].GetBound().m_MaxPoint);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudCache::Flush()
{
	m_Cache.clear();
	m_Bound.Reset();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
