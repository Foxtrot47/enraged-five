//
// crparameterizedmotion/parametertranslator.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERTRANSLATOR_H
#define CRPARAMETERIZEDMOTION_PARAMETERTRANSLATOR_H

#if 0  // COMPOSITE PM'S REMOVED TEMPORARILY





#include "parameterizedmotion.h"
#include "parameter.h"

namespace rage
{

// forward decls
class crFrame;
class datSerialize;
class crpmRegistrationCurveData;
class crpmBlendDatabase;
class crpmParameterization;
class crpmParameterSampler;
class crpmParameter;
class crpmParameterizedMotion;
class crpmParameterInterpolator;
class crpmParameterTranslator;


class crpmParameterTranslator
{
public:
	virtual ~crpmParameterTranslator() { }

    virtual void translate(const crpmParameter& in, std::vector<crpmParameterizedMotion*>&, crpmParameter& out, int& selectedChildOut) const
    {
        out = in;
        selectedChildOut = 0;
    }

    // PURPOSE: return the # of dimensions for parameters to be translated
    virtual unsigned int getInParameterDimension(const std::vector<crpmParameterizedMotion*>& inChilds) const
    {
        return inChilds[0]->getParameterDimension();
    }

    // PURPOSE: return the # of dimensions for parameters being unsed in the child PMS
    virtual unsigned int getOutParameterDimension(const std::vector<crpmParameterizedMotion*>& inChilds) const
    {
        return inChilds[0]->getParameterDimension();
    }

    virtual unsigned int getWeightDimension(const std::vector<crpmParameterizedMotion*>& inChilds) const
    {
        return inChilds[0]->getWeightDimension();
    }

    virtual void getParameterBoundaries(const std::vector<crpmParameterizedMotion*>& inChilds, crpmParameter& min, crpmParameter& max) const
    {
        return inChilds[0]->getParameterBoundaries(min, max);
    }
};

const crpmParameterTranslator& getNullParameterTranslator();
/*
class mfAddDummyDimensionParameterTranslator : public crpmParameterTranslator
{
public:
    mfAddDummyDimensionParameterTranslator(int dimensionsToAdd)
        : mDimensionsToAdd(dimensionsToAdd)
    {

    }

    virtual void translate(const crpmParameter& in, std::vector<crpmParameterizedMotion*>&, crpmParameter& out, int& selectedChildOut) const
    {
        std::copy(in.begin(), in.begin() + in.size() - mDimensionsToAdd, out.begin());
        selectedChildOut = 0;
    }

    // PURPOSE: return the # of dimensions for parameters to be translated
    virtual unsigned int getInParameterDimension(const std::vector<crpmParameterizedMotion*>& inChilds) const
    {
        return inChilds[0]->getParameterDimension() + mDimensionsToAdd;
    }

    // PURPOSE: return the # of dimensions for parameters being unsed in the child PMS
    virtual unsigned int getOutParameterDimension(const std::vector<crpmParameterizedMotion*>& inChilds) const
    {
        return inChilds[0]->getParameterDimension();
    }

    virtual unsigned int getWeightDimension(const std::vector<crpmParameterizedMotion*>& inChilds) const
    {
        return inChilds[0]->getWeightDimension();
    }

    virtual void getParameterBoundaries(const std::vector<crpmParameterizedMotion*>& inChilds, crpmParameter& min, crpmParameter& max) const
    {
        crpmParameter tempMin(inChilds[0]->getParameterDimension());
        crpmParameter tempMax(inChilds[0]->getParameterDimension());

        inChilds[0]->getParameterBoundaries(tempMin, tempMax);

        min = crpmParameter(getInParameterDimension(inChilds));
        max = crpmParameter(getInParameterDimension(inChilds));
        std::copy(tempMin.begin(), tempMin.end(), min.begin());
        std::copy(tempMax.begin(), tempMax.end(), max.begin());
    }
private:
    int mDimensionsToAdd;
};


class mfStrafeSelectionParameterTranslator : public crpmParameterTranslator
{
public:
    virtual void translate(const crpmParameter& in, std::vector<crpmParameterizedMotion*>& ASSERT_ONLY(inChilds), crpmParameter& out, int& selectedChildOut) const
    {
        AssertMsg(in.size() == 4, "we expect 4 dimensions (in-dir,out-dir,in-feet,out-feet)");
        AssertMsg(inChilds.size() == 4, "we need PMs for all 4 quadrants");

        // we expect the order of inChilds to be:
        // Fwd-Fwd
        // Fwd-Bwd
        // Bwd-Fwd
        // Bwd-Bwd

        // ~1 means facing forward (like in Lucas's paper)
        bool bInForward = (in[2] > 0.5f);
        bool bOutForward = (in[3] > 0.5f);

        if (bInForward)
        {
            selectedChildOut = (bOutForward ? 0 : 1);
        }
        else
        {
            selectedChildOut = (bOutForward ? 2 : 3);
        }

        // translate parameters
        out[0] = (bInForward ? in[0] : convertFwdToBwdParameter(in[0]));
        out[1] = (bOutForward ? in[1] : convertFwdToBwdParameter(in[1]));
    }

    // PURPOSE: return the # of dimensions for parameters to be translated
    virtual unsigned int getInParameterDimension(const std::vector<crpmParameterizedMotion*>& ) const
    {
        return 4;
    }

    // PURPOSE: return the # of dimensions for parameters being unsed in the child PMS
    virtual unsigned int getOutParameterDimension(const std::vector<crpmParameterizedMotion*>& ) const
    {
        return 2;
    }

    virtual unsigned int getWeightDimension(const std::vector<crpmParameterizedMotion*>& inChilds) const
    {
        return inChilds[0]->getWeightDimension();
    }

    virtual void getParameterBoundaries(const std::vector<crpmParameterizedMotion*>& inChilds, crpmParameter& min, crpmParameter& max) const
    {
        //TODO: FastAssert that this is the case in the data
        //NOTE: this could also be calculated

        min = crpmParameter(getInParameterDimension(inChilds));
        max = crpmParameter(getInParameterDimension(inChilds));

        min[0] = -180.0f;
        min[1] = -180.0f;
        min[2] = 0.0f;
        min[3] = 0.0f;

        max[0] = 180.0f;
        max[1] = 180.0f;
        max[2] = 1.0f;
        max[3] = 1.0f;
    }

private:
    float convertFwdToBwdParameter(float value) const
    {
        if (value < 0.0f)
        {
            return (value + 180.0f);
        }
        else
        {
            return (value - 180.0f);
        }
    }

    float convertBwdToFwdParameter(float value) const
    {
        return convertFwdToBwdParameter(value);
    }
};


class mfStrafeLoopSelectionParameterTranslator : public crpmParameterTranslator
{
    virtual unsigned int getWeightDimension(const std::vector<crpmParameterizedMotion*>& inChilds) const
    {
        return inChilds[0]->getWeightDimension();
    }

protected:
    float convertFwdToBwdParameter(float value) const
    {
        if (value < 0.0f)
        {
            return (value + 180.0f);
        }
        else
        {
            return (value - 180.0f);
        }
    }

    float convertBwdToFwdParameter(float value) const
    {
        return convertFwdToBwdParameter(value);
    }
};

class mfStrafeAimLoopSelectionParameterTranslator : public mfStrafeLoopSelectionParameterTranslator
{
public:
    virtual void translate(const crpmParameter& in, std::vector<crpmParameterizedMotion*>& ASSERT_ONLY(inChilds), crpmParameter& out, int& selectedChildOut) const
    {
        // IN: in-travel, out-travel, in-feet, out-feet, aim-V, aim-H
        // OUT: travel, aim-V, aim-H

        AssertMsg(in.size() == 6, "we expect 6 dimensions (in-travel-angle, out-travel-angle, in-feet, out-feet, aim-angle-vertical, aim-angle-horizontal)");
        AssertMsg(inChilds.size() == 2, "we need PMs for all 2 quadrants");

        // we expect the order of inChilds to be:
        // Fwd-Fwd
        // Bwd-Bwd

        // ~1 means facing forward (like in Lucas's paper)
        //bool bInForward = (in[3] > 0.5f);
        bool bInForward = abs(in[1]) <= 90.0f;

        // translate parameters
        out[0] = (bInForward ? in[1] : convertFwdToBwdParameter(in[1]));
        out[1] = in[4];
        out[2] = 0.0f;//in[5];
        selectedChildOut = (bInForward ? 0 : 1);
    }

    // PURPOSE: return the # of dimensions for parameters to be translated
    virtual unsigned int getInParameterDimension(const std::vector<crpmParameterizedMotion*>& ) const
    {
        return 6;
    }

    // PURPOSE: return the # of dimensions for parameters being used in the child PMS
    virtual unsigned int getOutParameterDimension(const std::vector<crpmParameterizedMotion*>& ) const
    {
        return 3;
    }

    virtual unsigned int getWeightDimension(const std::vector<crpmParameterizedMotion*>& inChilds) const
    {
        return inChilds[0]->getWeightDimension();
    }

    virtual void getParameterBoundaries(const std::vector<crpmParameterizedMotion*>& inChilds, crpmParameter& min, crpmParameter& max) const
    {
        //TODO: FastAssert that this is the case in the data
        //NOTE: this could also be calculated

        min = crpmParameter(getInParameterDimension(inChilds));
        max = crpmParameter(getInParameterDimension(inChilds));

        min[0] = -180.0f;
        min[1] = -180.0f;
        min[2] =   0.0f;
        min[3] =   0.0f;
        min[4] = -90.0f;
        min[5] = -90.0f;

        max[0] = 180.0f;
        max[1] = 180.0f;
        max[2] =   1.0f;
        max[3] =   1.0f;
        max[4] =  90.0f;
        max[5] =  90.0f;
    }
};

class mfStrafeNoAimLoopSelectionParameterTranslator : public mfStrafeLoopSelectionParameterTranslator
{
public:
    virtual void translate(const crpmParameter& in, std::vector<crpmParameterizedMotion*>& ASSERT_ONLY(inChilds), crpmParameter& out, int& selectedChildOut) const
    {
        // IN: in-travel, out-travel, in-feet, out-feet, aim-H
        // OUT: travel, (aim-H)

        AssertMsg(in.size() == 5, "we expect 5 dimensions (in-travel-angle, out-travel-angle, in-feet, out-feet, aim-angle-horizontal)");
        AssertMsg(inChilds.size() == 2, "we need PMs for all 2 quadrants");

        // we expect the order of inChilds to be:
        // Fwd-Fwd
        // Bwd-Bwd

        // ~1 means facing forward (like in Lucas's paper)
        //bool bInForward = (in[3] > 0.5f);
        bool bInForward = abs(in[1]) <= 90.0f;

        // translate parameters
        out[0] = (bInForward ? in[1] : convertFwdToBwdParameter(in[1]));
        //out[1] = in[4];
        selectedChildOut = (bInForward ? 0 : 1);
    }

    // PURPOSE: return the # of dimensions for parameters to be translated
    virtual unsigned int getInParameterDimension(const std::vector<crpmParameterizedMotion*>& ) const
    {
        return 5;
    }

    // PURPOSE: return the # of dimensions for parameters being used in the child PMS
    virtual unsigned int getOutParameterDimension(const std::vector<crpmParameterizedMotion*>& ) const
    {
        //return 2;
        return 1;
    }

    virtual void getParameterBoundaries(const std::vector<crpmParameterizedMotion*>& inChilds, crpmParameter& min, crpmParameter& max) const
    {
        //TODO: FastAssert that this is the case in the data
        //NOTE: this could also be calculated

        min = crpmParameter(getInParameterDimension(inChilds));
        max = crpmParameter(getInParameterDimension(inChilds));

        min[0] = -180.0f;
        min[1] = -180.0f;
        min[2] =   0.0f;
        min[3] =   0.0f;
        min[4] = -90.0f;

        max[0] = 180.0f;
        max[1] = 180.0f;
        max[2] =   1.0f;
        max[3] =   1.0f;
        max[4] =  90.0f;
    }
};




class mfShootDodgeSelectionParameterTranslator : public crpmParameterTranslator
{
public:
    virtual void translate(const crpmParameter& in, std::vector<crpmParameterizedMotion*>& ASSERT_ONLY(inChilds), crpmParameter& out, int& selectedChildOut) const
    {
        AssertMsg(in.size() == 2, "we expect 2 in dimensions");
        AssertMsg(out.size() == 2, "we expect 2 out dimensions");
        AssertMsg(inChilds.size() == 4, "we need 4 children");

        float travel = in[0];
        // NOTE: The order here has to match the order in the composite pm-file!
        if ((travel <=   90.0f) && (travel >=    0.0f)) selectedChildOut = 0; // FwdLft
        if ((travel <     0.0f) && (travel >=  -90.0f)) selectedChildOut = 1; // FwdRgt
        if ((travel <   -90.0f) && (travel >= -180.0f)) selectedChildOut = 2; // BwdRgt
        if ((travel <= -180.0f) && (travel >= -270.0f)) selectedChildOut = 3; // BwdLft
        out[0] = in[0];
        out[1] = in[1];
    }

    // PURPOSE: return the # of dimensions for parameters to be translated
    virtual unsigned int getInParameterDimension(const std::vector<crpmParameterizedMotion*>& ) const
    {
        return 2;
    }

    // PURPOSE: return the # of dimensions for parameters being used in the child PMS
    virtual unsigned int getOutParameterDimension(const std::vector<crpmParameterizedMotion*>& ) const
    {
        return 2;
    }

    virtual void getParameterBoundaries(const std::vector<crpmParameterizedMotion*>& inChilds, crpmParameter& min, crpmParameter& max) const
    {
        //TODO: FastAssert that this is the case in the data
        //NOTE: this could also be calculated

        min = crpmParameter(getInParameterDimension(inChilds));
        max = crpmParameter(getInParameterDimension(inChilds));

        min[0] = -270.0f;
        min[1] = -60.0f;

        max[0] = 90.0f;
        max[1] = 60.0f;
    }
};
*/

} // namespace rage


#endif // 0  // COMPOSITE PM'S REMOVED TEMPORARILY

#endif // CRPARAMETERIZEDMOTION_PARAMETERTRANSLATOR_H
