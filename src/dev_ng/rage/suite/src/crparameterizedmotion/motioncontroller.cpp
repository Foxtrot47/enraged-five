//
// crparameterizedmotion/motioncontroller.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "motioncontroller.h"

#include "registrationcurve.h"
#include "registrationcurveplayer.h"

#include "cranimation/animation.h"
#include "cranimation/framefilters.h"
#include "crclip/clip.h"
#include "crclip/clips.h"
#include "crclip/clipplayer.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmMotionController::crpmMotionController()
: m_MoverMtx(V_IDENTITY)
, m_FilterWeightSet(NULL)
, m_FilterMover(false)
, m_Filter(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmMotionController::~crpmMotionController()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionController::Shutdown()
{
	m_Skeleton.Shutdown();
	m_Frame.Shutdown();
	m_FilterWeightSet = NULL;
	if(m_Filter)
	{
		delete m_Filter;
		m_Filter = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionController::Reset(Mat34V_In mtx)
{
	m_MoverMtx = mtx;
	m_Skeleton.Reset();
	m_Skeleton.Update();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionController::InitBase(const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	m_Skeleton.Init(skelData, &m_MoverMtx);
	m_Frame.InitCreateBoneAndMoverDofs(skelData);

	m_FilterWeightSet = filterWeightSet;
	m_FilterMover = filterMover;
	
	crFrameFilterBoneBasic* filter = rage_new crFrameFilterBoneBasic(skelData);
	if(m_FilterWeightSet)
	{
		filter->AddWeightSet(*m_FilterWeightSet);
	}
	else
	{
		filter->AddBoneId(0, true, 1.f);
	}
	filter->SetNonBoneDofsAllowed(!m_FilterMover);
	m_Filter = filter;

	Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionController::Pose()
{
	m_Frame.Pose(m_Skeleton, false, m_Filter);

	const u16 moverId = 0;

	Mat34V deltaMtx;
	if(m_Frame.GetMoverMatrix(moverId, deltaMtx))
	{
		Transform(m_MoverMtx, m_MoverMtx, deltaMtx);
	}

	m_Skeleton.Update();
}

////////////////////////////////////////////////////////////////////////////////

crpmMotionControllerClip::crpmMotionControllerClip()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmMotionControllerClip::crpmMotionControllerClip(const crClip& clip, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	Init(clip, skelData, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

crpmMotionControllerClip::~crpmMotionControllerClip()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerClip::Init(const crClip& clip, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	m_Player.SetClip(&clip);

	InitBase(skelData, filterWeightSet, filterMover);

	m_Player.Composite(m_Frame);

	Pose();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerClip::Shutdown()
{
	m_Player.Shutdown();

	crpmMotionController::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerClip::Reset(Mat34V_In mtx)
{
	m_Player.SetTime(0.f);

	crpmMotionController::Reset(mtx);

	m_Player.Composite(m_Frame);

	Pose();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerClip::Advance(float deltaTime)
{
	m_Player.Update(deltaTime);

	m_Player.Composite(m_Frame);

	Pose();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerClip::SetLooped(bool looped)
{
	m_Player.SetLooped(true, looped);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmMotionControllerClip::IsFinished() const
{
	return m_Player.GetTime() == GetDuration();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerClip::SetU(float u)
{
	m_Player.SetTime(GetDuration() * u);
}

////////////////////////////////////////////////////////////////////////////////

float crpmMotionControllerClip::GetU() const
{
	return (GetDuration()>0.f) ? (GetTime() / GetDuration()) : 0.f;
}

////////////////////////////////////////////////////////////////////////////////

float crpmMotionControllerClip::GetDuration() const
{
	return m_Player.GetClip() ? m_Player.GetClip()->GetDuration() : 0.f;
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerClip::SetTime(float time)
{
	m_Player.SetTime(time);
}

////////////////////////////////////////////////////////////////////////////////

float crpmMotionControllerClip::GetTime() const
{
	return m_Player.GetTime();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerClip::InitBase(const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	crpmMotionController::InitBase(skelData, filterWeightSet, filterMover);

	if(m_Player.GetClip())
	{
		crFrameData* frameData = rage_new crFrameData;
		m_Player.GetClip()->InitDofs(*frameData);
		m_Frame.Init(*frameData);
		frameData->Release();
	}

	Reset();
}

////////////////////////////////////////////////////////////////////////////////

crpmMotionControllerRc::crpmMotionControllerRc()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmMotionControllerRc::crpmMotionControllerRc(const crpmRegistrationCurveData& rcData, const crClips& clips, const crpmWeights* weights, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	Init(rcData, clips, weights, skelData, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

crpmMotionControllerRc::~crpmMotionControllerRc()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerRc::Init(const crpmRegistrationCurveData& rcData, const crClips& clips, const crpmWeights* weights, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	if(weights)
	{
		Assert(clips.GetNumClips() == u32(weights->GetNumWeights()));
		m_Weights.Init(*weights);
	}
	else
	{
		m_Weights.Init(clips.GetNumClips());
		m_Weights.SetWeight(rcData.GetReferenceClipIndex(), 1.f);
	}

	m_Player.Init(rcData, clips, m_Weights);

	crpmMotionController::InitBase(skelData, filterWeightSet, filterMover);

	m_Player.Composite(m_Frame);

	Pose();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerRc::Shutdown()
{
	m_Player.Shutdown();

	crpmMotionController::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerRc::Reset(Mat34V_In mtx)
{
	m_Player.Reset(m_Weights);

	crpmMotionController::Reset(mtx);

	m_Player.Composite(m_Frame);

	Pose();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerRc::Advance(float deltaTime)
{
	m_Player.Update(deltaTime, m_Weights);

	m_Player.Composite(m_Frame);

	Pose();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerRc::SetLooped(bool looped)
{
	m_Player.SetLooped(looped);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmMotionControllerRc::IsFinished() const
{
	return m_Player.IsFinished();
}

////////////////////////////////////////////////////////////////////////////////

float crpmMotionControllerRc::GetDuration() const
{
	return m_Player.GetDuration();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerRc::SetWeights(const crpmWeights& weights)
{
	m_Weights.SetWeights(weights);
}

////////////////////////////////////////////////////////////////////////////////

const crpmWeights& crpmMotionControllerRc::GetWeights() const
{
	return m_Weights;
}

////////////////////////////////////////////////////////////////////////////////

float crpmMotionControllerRc::GetU() const
{
	return m_Player.GetU();
}

////////////////////////////////////////////////////////////////////////////////

void crpmMotionControllerRc::SetU(float u)
{
	m_Player.SetU(u, m_Weights);
}

////////////////////////////////////////////////////////////////////////////////

float crpmMotionControllerRc::GetRemainingTime() const
{
	return m_Player.GetRemainingTime();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


