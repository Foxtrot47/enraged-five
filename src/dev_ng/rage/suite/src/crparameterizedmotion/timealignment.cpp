//
// crparameterizedmotion/timealignment.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "timealignment.h"

#define STRICTLY_INCREASING (1)

////////////////////////////////////////////////////////////////////////////////

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmTimeAlignment::crpmTimeAlignment()
: m_ClipX(NULL)
, m_ClipY(NULL)
, m_SampleRateX(1.f/30.f)
, m_SampleRateY(1.f/30.f)
, m_CyclesX(1)
, m_CyclesY(1)
, m_RepeatX(1)
, m_RepeatY(1)
, m_DistanceAverage(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmTimeAlignment::crpmTimeAlignment(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& distanceGrid)
: m_ClipX(NULL)
, m_ClipY(NULL)
, m_SampleRateX(1.f/30.f)
, m_SampleRateY(1.f/30.f)
, m_CyclesX(1)
, m_CyclesY(1)
, m_RepeatX(1)
, m_RepeatY(1)
, m_DistanceAverage(0.f)
{
	Calculate(path, distanceGrid);
}

////////////////////////////////////////////////////////////////////////////////

crpmTimeAlignment::~crpmTimeAlignment()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmTimeAlignment::Shutdown()
{
	m_TemporalAlignmentSeries.Shutdown();
	m_TransformAlignmentSeries.Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmTimeAlignment::Calculate(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& grid)
{
	Shutdown();

	m_ClipX = grid.GetClipX();
	m_ClipY = grid.GetClipY();

	m_SampleRateX = grid.GetSampleRateX();
	m_SampleRateY = grid.GetSampleRateY();

	m_CyclesX = grid.GetCyclesX();
	m_CyclesY = grid.GetCyclesY();

	m_RepeatX = grid.GetRepeatX();
	m_RepeatY = grid.GetRepeatY();

	m_DistanceAverage = path.GetDistanceAverage();

	// fit to optimal path calculated
	CreateTemporalAlignments(path, grid);

	// fit through the transformations on the optimal path
	CreateTransformAlignments(path, grid);
}

////////////////////////////////////////////////////////////////////////////////

Vec2V_Out crpmTimeAlignment::GetAlignmentCells(float u) const
{
	Assert(InRange(u, 0.f, 1.f));
	return m_TemporalAlignmentSeries.GetPosition(u);
}

////////////////////////////////////////////////////////////////////////////////

crpmPointCloudTransform crpmTimeAlignment::GetAlignmentTransform(float u) const
{
	Assert(InRange(u, 0.f, 1.f));
	Vec3V v = m_TransformAlignmentSeries.GetPosition(u);
	return crpmPointCloudTransform(v[0], v[1], v[2]);
}

////////////////////////////////////////////////////////////////////////////////

void crpmTimeAlignment::ConvertCellXToCellY(float cellX, float& outCellY, float& outU) const
{
	outU = ConvertCellToU(cellX, 0);
	outCellY = m_TemporalAlignmentSeries.GetPosition(outU, 1);
}

////////////////////////////////////////////////////////////////////////////////

void crpmTimeAlignment::ConvertCellYToCellX(float cellY, float& outCellX, float& outU) const
{
	outU = ConvertCellToU(cellY, 1);
	outCellX = m_TemporalAlignmentSeries.GetPosition(outU, 0);
}

////////////////////////////////////////////////////////////////////////////////

float crpmTimeAlignment::ConvertCellToU(float cell, int clipIdx) const
{
	return m_TemporalAlignmentSeries.GetInversePosition(cell, clipIdx);
}

////////////////////////////////////////////////////////////////////////////////

void crpmTimeAlignment::CreateTemporalAlignments(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& UNUSED_PARAM(grid))
{
    // create an array of Vector2's out of path cells
    atArray<Vec2V> pathCoordinates;
    pathCoordinates.Reserve(path.GetLength());

    for(crpmDistanceGrid::Path::const_iterator it=path.begin(); it!=path.end(); ++it)
    {
		// TODO --- convert into time?  distance grid knows sample frequencies
        pathCoordinates.PushAndGrow(Vec2V(float(it->x), float(it->y)));
    }

#if STRICTLY_INCREASING
	// experimental strictly increasing code
	const float bias = 1.f/3.f;

	for(atArray<Vec2V>::iterator it=(pathCoordinates.begin()+1); it!=(pathCoordinates.end()-1); ++it)
	{
		if(it->GetXf() >= (it+1)->GetXf())
		{
			it->SetXf(Lerp(bias, (it+1)->GetXf(), (it-1)->GetXf()));
		}
		if(it->GetYf() >= (it+1)->GetYf())
		{
			it->SetYf(Lerp(bias, (it+1)->GetYf(), (it-1)->GetYf()));
		}
	}
	for(atArray<Vec2V>::iterator it=(pathCoordinates.end()-1); it!=pathCoordinates.begin(); --it)
	{
		if((it->GetXf() <= (it-1)->GetXf()))
		{
			it->SetXf(Lerp(bias, (it-1)->GetXf(), (it+1)->GetXf()));
		}
		if((it->GetYf() <= (it-1)->GetYf()))
		{
			it->SetYf(Lerp(bias, (it-1)->GetYf(), (it+1)->GetYf()));
		}
	}
#endif // STRICTLY_INCREASING

	m_TemporalAlignmentSeries.Init(pathCoordinates.GetCount(), &pathCoordinates[0]);
}

////////////////////////////////////////////////////////////////////////////////

void crpmTimeAlignment::CreateTransformAlignments(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& grid)
{
	// create an array of Vector3's from path cell transformations
    atArray<Vec3V> transformations;
    transformations.Reserve(path.GetLength());

    crpmAngleContinuityPreserver acp;
    for(crpmDistanceGrid::Path::const_iterator it=path.begin(); it!=path.end(); ++it)
    {
        const crpmPointCloudTransform& transform = grid.GetCellTransform(it->x, it->y);

        // fill a 3d vector with the relevant information from the transformation
		Vec3V v(acp.Get(transform.GetAngle()), transform.GetTranslation().GetXf(), transform.GetTranslation().GetZf());

		if(transformations.size() > 0 && fabsf(transformations.back().GetXf() - v.GetXf()) > (PI*0.25f))
		{
//			Warningf("angle differences are discontinuous: %f - increase window-size", transformations.back().x - v.x);
		}

        // add vector as a control point for the spline
        transformations.PushAndGrow(v);
    }

	m_TransformAlignmentSeries.Init((int)transformations.size(), &transformations[0]);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

