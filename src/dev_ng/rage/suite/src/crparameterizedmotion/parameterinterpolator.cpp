//
// crparameterizedmotion/parameterinterpolator.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "parameterinterpolator.h"

#include "math/amath.h"
#include "math/simplemath.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterInterpolator::crpmParameterInterpolator()
: m_Parameterization(NULL)
, m_IsFinished(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterInterpolator::crpmParameterInterpolator(int worstNumParameterDimensions)
: m_Parameterization(NULL)
, m_IsFinished(false)
{
	PreInit(worstNumParameterDimensions);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterInterpolator::crpmParameterInterpolator(const crpmParameter& parameter, const crpmParameterization* parameterization)
: m_Parameterization(NULL)
, m_IsFinished(false)
{
	Init(parameter, parameterization);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterInterpolator::~crpmParameterInterpolator()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterInterpolator::PreInit(int worstNumParameterDimensions)
{
	m_Current.PreInit(worstNumParameterDimensions);
	m_Target.PreInit(worstNumParameterDimensions);

	m_Rate.PreInit(worstNumParameterDimensions);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterInterpolator::Init(const crpmParameter& parameter, const crpmParameterization* parameterization)
{
	m_Parameterization = parameterization;

	m_Rate.Init(parameter.GetNumDimensions(), FLT_MAX);

	Reset(parameter);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterInterpolator::Clear()
{
	m_Parameterization = NULL;
	m_IsFinished = false;

	m_Current.Clear();
	m_Target.Clear();
	m_Rate.Clear();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterInterpolator::Shutdown()
{
	m_Parameterization = NULL;

	m_Current.Shutdown();
	m_Target.Shutdown();
	m_Rate.Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterInterpolator::Reset(const crpmParameter& parameter)
{
	m_Current.Init(parameter);
	m_Target.Init(parameter);

	m_IsFinished = true;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterInterpolator::Update(float deltaTime)
{
	Assert(deltaTime >= 0.f);

	if(!m_IsFinished)
	{
		m_Current.Approach(m_Current, m_Target, m_Rate, deltaTime, m_Parameterization);
		if(m_Current.IsClose(m_Target, NULL, m_Parameterization))
		{
			m_Current.Set(m_Target);
			m_IsFinished = true;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterInterpolator::SetTarget(const crpmParameter& target)
{
	if(!m_Target.IsClose(target, NULL, m_Parameterization))
	{
		m_Target.Set(target);
		m_IsFinished = false;
	}
}

////////////////////////////////////////////////////////////////////////////////

};

