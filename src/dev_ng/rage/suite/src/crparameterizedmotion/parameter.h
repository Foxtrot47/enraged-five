//
// crparameterizedmotion/parameter.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETER_H
#define CRPARAMETERIZEDMOTION_PARAMETER_H

#include <algorithm>
#include <functional>

#include "serializationhelper.h"

#include "atl/array.h"

namespace rage
{

class atString;
class bkBank;
class crpmParameterization;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Represents a parameter vector
class crpmParameter
{
public:

	// PURPOSE: Default constructor
	crpmParameter();

	// PURPOSE: Resource constructor
	crpmParameter(datResource&);

	// PURPOSE: Pre-initializing constructor
//	crpmParameter(int worstNumDimensions);

	// PURPOSE: Initializing constructor
	// PARAMS:
	// numDimensions - number of dimensions in parameter
	// value - default value for each dimension (default 0.0)
    crpmParameter(int numDimensions, float value=0.f);

	// PURPOSE: Copy constructor
	// PARAMS: parameter - parameter to copy dimensions and values from
    crpmParameter(const crpmParameter& parameter);

	// PURPOSE: Destructor
	~crpmParameter();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmParameter);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Pre-initialize
	// NOTES: Reserves worst case storage needed for later initialization
	void PreInit(int worstNumDimensions);

	// PURPOSE: Initializer
	// PARAMS:
	// numDimensions - number of dimensions in parameter
	// value - default value for each dimension (default 0.0)
	void Init(int numDimensions, float value=0.f);

	// PURPOSE: Initializer from copy
	// PARAMS: parameter - parameter to copy dimensions and values from
	void Init(const crpmParameter& parameter);

	// PURPOSE: Clear
	void Clear();

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Grow, append an new dimension
	void Grow(float value=0.f);

	// PURPOSE: Get number of dimensions in parameter
	int GetNumDimensions() const;

	// PURPOSE: Get value by dimension index
	float GetValue(int dimension) const;

	// PURPOSE: Set by copy
	// PARAMS: parameter - parameter to copy values from (must have matching number of dimensions)
	void Set(const crpmParameter& parameter);

	// PURPOSE: Set value by dimension index
	void SetValue(int dimension, float value);

	// PURPOSE: Set all values to new value
	void SetValues(float value);

	// PURPOSE: Element access by dimension index (non-const)
	float& operator[](const int index);

	// PURPOSE: Element access by dimension index (const)
	const float& operator[](const int index) const;

	// PURPOSE: Equality operator
	bool operator==(const crpmParameter& parameter) const;
	
	// PURPOSE: Inequality operator
	bool operator!=(const crpmParameter& parameter) const;

	// PURPOSE: Assignment operator
	// NOTES: Dimensionality of all parameters must match.
	const crpmParameter& operator=(const crpmParameter& parameter);

	// PURPOSE: Subtraction operator
	// NOTES: Dimensionality of all parameters must match.
    const crpmParameter& operator-=(const crpmParameter& parameter);

	// PURPOSE: Addition operator
	// NOTES: Dimensionality of all parameters must match.
    const crpmParameter& operator+=(const crpmParameter& parameter);

	// PURPOSE: Division operator
	// NOTES: Dimensionality of all parameters must match.
	const crpmParameter& operator/=(const crpmParameter& parameter);

	// PURPOSE: Division operator (on float array)
	// NOTES: Dimensionality of all parameters must match.
	const crpmParameter& operator/=(const atArray<float>& values);

	// PURPOSE: Multiplication operator
	// NOTES: Dimensionality of all parameters must match.
	const crpmParameter& operator*=(const crpmParameter& parameter);

	// PURPOSE: Multiplication operator (on float array)
	// NOTES: Dimensionality of all parameters must match.
	const crpmParameter& operator*=(const atArray<float>& values);

	// PURPOSE: Interpolate between two parameters
	// PARAMS:
	// t - interpolation weight [0..1]
	// parameter0, parameter1 - parameters to interpolate between
	// parameterization - optional parameterization (see notes)
	// NOTES: Dimensionality of all parameters must match.
	// If parameterization is provided then can intelligently interpolate the values for each dimension,
	// using meta data in dimension info - this allows for wrapping, non-linear interpolation etc.
	// If parameterization not provided, will just linearly interpolate each dimension.
	void Lerp(float t, const crpmParameter& parameter0, const crpmParameter& parameter1, const crpmParameterization* parameterization);
	
	// PURPOSE: Approach parameter, with maximum change
	// PARAMS:
	// source, target - current and target parameters
	// change - maximum change parameter
	// changeScalar - optional scalar for change parameter (scales all dimensions)
	// parameterization - optional parameterization (see notes)
	// NOTES: Dimensionality of all parameters must match.
	// If parameterization is provided then can intelligently approach the value for each dimension,
	// using meta data in dimension info - this allows for wrapping, non-linear approach etc.
	// If parameterization not provided, will just linearly interpolate each dimension.
	void Approach(const crpmParameter& source, const crpmParameter& target, const crpmParameter& change, float changeScalar=1.f, const crpmParameterization* parameterization=NULL);

	// PURPOSE: Clamp between two parameters
	// PARAMS:
	// minParameter, maxParameter - parameters to clamp between
	// NOTES: Dimensionality of all parameters must match.
	void Clamp(const crpmParameter& minParameter, const crpmParameter& maxParameter);

	// PURPOSE: Near equality test
	// PARAMS:
	// parameter - comparison parameter
	// tolerance - maximum deviation
	// parameterization - optional parameterization (see notes)
	// NOTES: Dimensionality of all parameters must match.
	// Tolerance is optional, SMALL_FLOAT will be used for all dimensions if not provided.
	// If parameterization is provided then can intelligently test the values for each dimension,
	// using meta data in dimension info - this allows for wrapping etc.
	bool IsClose(const crpmParameter& parameter, const crpmParameter* tolerance=NULL, const crpmParameterization* parameterization=NULL) const;

	// PURPOSE: STL iterator types
	typedef atArray<float>::iterator iterator;
	typedef atArray<float>::const_iterator const_iterator;
	typedef atArray<float>::size_type size_type;

	// PURPOSE: STL container interface
    const_iterator begin() const;

	// PURPOSE: STL container interface
	const_iterator end() const;

	// PURPOSE: STL container interface
	iterator begin();

	// PURPOSE: STL container interface
	iterator end();

	// PURPOSE: STL container interface
    size_type size() const;

	// PURPOSE: Serialize
	void Serialize(datSerialize&);

	// PURPOSE: Dump output
	void Dump(atString&) const;

	// PURPOSE: Debug widgets
#if __BANK
	void AddWidgets(bkBank&, const crpmParameter* minParameter=NULL, const crpmParameter* maxParameter=NULL);
#endif // __BANK

protected:
    atArray<float> m_Values;
};


////////////////////////////////////////////////////////////////////////////////

inline int crpmParameter::GetNumDimensions() const
{
	return int(m_Values.size());
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmParameter::GetValue(int dimension) const
{
	return m_Values[dimension];
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameter::Set(const crpmParameter& parameter)
{
	Assert(m_Values.size() == parameter.m_Values.size());
	m_Values = parameter.m_Values;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameter::SetValue(int dimension, float value)
{
	m_Values[dimension] = value;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameter::SetValues(float value)
{
	std::fill(m_Values.begin(), m_Values.end(), value);
}

////////////////////////////////////////////////////////////////////////////////

inline float& crpmParameter::operator[](const int index)
{
	return m_Values[index];
}

////////////////////////////////////////////////////////////////////////////////

inline const float& crpmParameter::operator[](const int index) const
{
	return m_Values[index];
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmParameter::operator==(const crpmParameter& parameter) const
{
	if(m_Values.GetCount() == parameter.m_Values.GetCount())
	{
		for(int i=0; i<m_Values.GetCount(); ++i)
		{
			if(m_Values[i] != parameter.m_Values.GetCount())
			{
				return false;
			}
		}
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmParameter::operator!=(const crpmParameter& parameter) const
{
	return !(*this == parameter);
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameter::operator=(const crpmParameter& parameter)
{
//	Assert(m_Values.size() == parameter.m_Values.size());
	m_Values = parameter.m_Values;
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameter::operator-=(const crpmParameter& parameter)
{
	Assert(m_Values.size() == parameter.m_Values.size());
	std::transform(m_Values.begin(), m_Values.end(), parameter.begin(), m_Values.begin(), std::minus<float>());
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameter::operator+=(const crpmParameter& parameter)
{
	Assert(m_Values.size() == parameter.m_Values.size());
	std::transform(m_Values.begin(), m_Values.end(), parameter.begin(), m_Values.begin(), std::plus<float>());
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameter::operator/=(const crpmParameter& parameter)
{
	Assert(m_Values.size() == parameter.m_Values.size());
	std::transform(m_Values.begin(), m_Values.end(), parameter.begin(), m_Values.begin(), std::divides<float>());
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameter::operator/=(const atArray<float>& values)
{
	Assert(m_Values.size() == values.size());
	std::transform(m_Values.begin(), m_Values.end(), values.begin(), m_Values.begin(), std::divides<float>());
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameter::operator*=(const crpmParameter& parameter)
{
	Assert(m_Values.size() == parameter.m_Values.size());
	std::transform(m_Values.begin(), m_Values.end(), parameter.begin(), m_Values.begin(), std::multiplies<float>());
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameter::operator*=(const atArray<float>& values)
{
	Assert(m_Values.size() == values.size());
	std::transform(m_Values.begin(), m_Values.end(), values.begin(), m_Values.begin(), std::multiplies<float>());
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameter::const_iterator crpmParameter::begin() const
{
	return m_Values.begin();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameter::const_iterator crpmParameter::end() const
{
	return m_Values.end();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameter::iterator crpmParameter::begin()
{
	return m_Values.begin();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameter::iterator crpmParameter::end()
{
	return m_Values.end();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameter::size_type crpmParameter::size() const
{
	return m_Values.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameter::Serialize(datSerialize& s)
{
	SerializeAtlArrayAsStlArray(s, m_Values);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


#endif // CRPARAMETERIZEDMOTION_PARAMETER_H
