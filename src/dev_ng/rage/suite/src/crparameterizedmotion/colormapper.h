//
// crparameterizedmotion/colormapper.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_COLORMAPPER_H
#define CRPARAMETERIZEDMOTION_COLORMAPPER_H

namespace rage
{

// forward declarations
class Color32;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract color mapper, maps range of values of pixel colors
// Used to generate pixel colors for debug images.
class crpmColorMapper
{
public:

	// PURPOSE: Constructor
	// PARAMS: minValue, maxValue - minimum and maximum values that will be mapped
	// colorCount - number of colors to map to (calculated by derived class)
    crpmColorMapper(float minValue, float maxValue, u32 colorCount);

	// PURPOSE: Destructor
	virtual ~crpmColorMapper();

	// PURPOSE: Map value to color
	// (low values = low color index, high values = high color index)
	// PARAMS: value - value to map (will be clamped to minimum/maximum range)
	// RETURNS: mapped color
    Color32 Map(float value) const;

    // PURPOSE: Map value to negative (or reverse) color
	// (low values = high color index, high values = low color index)
	// PARAMS: value - value to map (will be clamped to minimum/maximum range)
	// RETURNS: mapped color
    Color32 NegativeMap(float value) const;

protected:

	// PURPOSE: Internal function that allows derived concrete classes to
	// perform different color mapping
	// PARAMS: colorIdx - color index
	// RETURNS: mapped color
    virtual Color32 MapColorIndexToColor(u32 colorIdx) const = 0;

private:

	// PURPOSE: Define the assignment operator as private to avoid a compiler warning
	crpmColorMapper& operator= (const crpmColorMapper&);

protected:
    float m_MinValue;
    float m_MaxValue;

    u32 m_ColorCount;

	float m_InvValuePerColorIdx;
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Heat color mapper, maps values to "heat" colors
class crpmColorMapperHeat : public crpmColorMapper
{
public:

	// PURPOSE: Constructor
	// PARAMS: minValue, maxValue - minimum and maximum values that will be mapped
    crpmColorMapperHeat(float minValue, float maxValue);

protected:

	// PURPOSE: Internal function, override color mapping
    Color32 MapColorIndexToColor(u32 colorIdx) const;

private:

	// PURPOSE: Define the assignment operator as private to avoid a compiler warning
    crpmColorMapperHeat& operator= (const crpmColorMapperHeat&);
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Gray-scale color mapper, maps values to gray-scale colors
class crpmColorMapperGrayscale : public crpmColorMapper
{
public:

	// PURPOSE: Constructor
	// PARAMS: minValue, maxValue - minimum and maximum values that will be mapped
    crpmColorMapperGrayscale(float minValue, float maxValue);

protected:

	// PURPOSE: Internal function, override color mapping
	Color32 MapColorIndexToColor(u32 colorIdx) const;

private:

	// PURPOSE: Define the assignment operator as private to avoid a compiler warning
    crpmColorMapperGrayscale& operator= (const crpmColorMapperGrayscale&);
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
#endif // CRPARAMETERIZEDMOTION_COLORMAPPER_H
