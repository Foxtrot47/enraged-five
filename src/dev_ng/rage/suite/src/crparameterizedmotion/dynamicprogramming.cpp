//
// crparameterizedmotion/dynamicprogramming.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "dynamicprogramming.h"

#include "colormapper.h"
#include "image.h"

#include <algorithm>

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmDynamicProgramming::crpmDynamicProgramming()
: m_Grid(NULL)
, m_SlopeLimit(0)
, m_MinX(0)
, m_MinY(0)
, m_MaxX(0)
, m_MaxY(0)
, m_MinSum(FLT_MAX)
, m_MaxSum(-FLT_MAX)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmDynamicProgramming::crpmDynamicProgramming(const crpmDistanceGrid& grid, int slopeLimit, int minX, int minY, int maxX, int maxY)
: m_Grid(NULL)
, m_SlopeLimit(0)
, m_MinX(0)
, m_MinY(0)
, m_MaxX(0)
, m_MaxY(0)
, m_MinSum(FLT_MAX)
, m_MaxSum(-FLT_MAX)
{
	Init(grid, slopeLimit, minX, minY, maxX, maxY);
}

////////////////////////////////////////////////////////////////////////////////

void crpmDynamicProgramming::Init(const crpmDistanceGrid& grid, int slopeLimit, int minX, int minY, int maxX, int maxY)
{
	m_Grid = &grid;
	m_SlopeLimit = slopeLimit;
	m_MinX = minX;
	m_MinY = minY;
	m_MaxX = (maxX<0)?grid.GetSizeX():maxX;
	m_MaxY = (maxY<0)?grid.GetSizeY():maxY;

	AssertMsg((m_MinX >= 0) && (m_MinY >= 0) , "invalid bounds");
	AssertMsg((m_MinX <= m_MaxX) && (m_MinY <= m_MaxY) , "invalid bounds");
	AssertMsg(m_MaxX <= grid.GetSizeX() && m_MaxY <= grid.GetSizeY() , "invalid bounds");

	// ensure we are inside the bounds of the distance-grid
	m_MaxX = Min(grid.GetSizeX(), m_MaxX);
	m_MaxY = Min(grid.GetSizeY(), m_MaxY);

	m_Cells.Resize((m_MaxY-m_MinY)*(m_MaxX-m_MinX));

	for(atArray<Cell>::iterator it=m_Cells.begin(); it!=m_Cells.end(); ++it)
	{
		it->Reset();
	}

	m_MinSum = FLT_MAX;
	m_MaxSum = -FLT_MAX;

	// slope limit must exceed slope required to traverse grid
	float slope = Max(float(m_MaxX - m_MinX) / float(m_MaxY - m_MinY), float(m_MaxY - m_MinY) / float(m_MaxX - m_MinX));
	m_SlopeLimit = Max<int>(int(ceilf(slope)), m_SlopeLimit);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDynamicProgramming::FindBestPath(const crpmDistanceGrid::Path& srcPath, const crpmDistanceGrid::Path& dstPath)
{
	Assert(m_Grid);

	m_AreaMinX = m_MinX;
	m_AreaMinY = m_MinY;
	m_AreaMaxX = m_MaxX;
	m_AreaMaxY = m_MaxY;

	for(crpmDistanceGrid::Path::const_iterator it=srcPath.begin(); it!=srcPath.end(); ++it)
	{
		m_AreaMinX = Max(it->x, m_AreaMinX);
		m_AreaMinY = Max(it->y, m_AreaMinY);
	}

	CalcCellValues(dstPath);

	BuildBridgeFrom(srcPath);

	return (m_BestPath.GetLength() != 0);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDynamicProgramming::FindBestBridge(const crpmDistanceGrid::Path& srcPath, const crpmDistanceGrid::Path& dstPath)
{
	if(!FindBestPath(srcPath, dstPath))
	{
		return false;
	}

	int index = 0;

	// find common cells in bridge and dst
	crpmDistanceGrid::Path::Coordinate endPoint;
	do
	{
		++index;
		endPoint = *(m_BestPath.end()-index);
	}
	while(dstPath.Contains(endPoint.x, endPoint.y));

	// no cells in common, building bridge failed
	if(index == 1)
	{
		return false;
	}

	// remove redundant cells from the bridge (those in dst)
	int size = int(m_BestPath.end() - m_BestPath.begin());
	m_BestPath.RemoveRange(size-index+2,size);

	// remove similar cells from start of src and bridge
	index = 0;
	while((index < m_BestPath.GetLength()) && (srcPath.Contains(m_BestPath[index].x, m_BestPath[index].y)))
	{
		++index;
	}
	m_BestPath.RemoveRange(0, index-1);

	if(m_BestPath.GetLength() == 0)
	{
		return false;
	}

	AssertMsg(srcPath.Contains(m_BestPath.begin()->x, m_BestPath.begin()->y) , "pruned bridge does not connect to source path");
	AssertMsg(!srcPath.Contains((m_BestPath.begin()+1)->x, (m_BestPath.begin()+1)->y) , "pruned bridge has second point in common with source");

	AssertMsg(m_BestPath.IsContinuous() , "bridge is not continuous");

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crpmDynamicProgramming::FindBestBridge()
{
	Assert(m_Grid);

    AssertMsg((float(m_MaxX - m_MinX) / float(m_MaxY - m_MinY)) <= m_SlopeLimit , "can't fit path with current slope constraints");
    AssertMsg((float(m_MaxY - m_MinY) / float(m_MaxX - m_MinX)) <= m_SlopeLimit , "can't fit path with current slope constraints");

    crpmDistanceGrid::Path topRight;
    topRight.Add(m_MaxX-1, m_MaxY-1, 0.f);

    m_AreaMinX = m_MinX;
    m_AreaMinY = m_MinY;
    m_AreaMaxX = m_MaxX;
    m_AreaMaxY = m_MaxY;

    CalcCellValues(topRight);

    BuildBridgeFrom(m_MinX, m_MinY, m_BestPath);

    AssertMsg((m_BestPath.GetStartX() == m_MinX) && (m_BestPath.GetStartY() == m_MinY) , "bridge doesn't start at correct place");
    AssertMsg((m_BestPath.GetEndX() == (m_MaxX-1)) && (m_BestPath.GetEndY() == (m_MaxY-1)) , "bridge doesn't end at correct place");
}

////////////////////////////////////////////////////////////////////////////////

void crpmDynamicProgramming::MakeDynamicProgrammingImage(crpmImage& outImage) const
{
	outImage.Clear();

	crpmColorMapperHeat heatMapper(m_MinSum, m_MaxSum);
	crpmColorMapperGrayscale grayMapper(m_MinSum, m_MaxSum);

	for (int y=m_MinY; y<m_MaxY; ++y)
	{
		for (int x=m_MinX; x<m_MaxX; ++x)
		{
			float sum = GetCellSum(x, y);
			Color32 col;
			if(sum == FLT_MAX)
			{
				col = Color32(0, 0, 0, 0);
			}
			else
			{
				col = heatMapper.Map(sum);
			}

			if(!IsCellValid(x, y))
			{
				col = grayMapper.Map(sum);
				col.SetRed(128 + (col.GetRed()>>1));
				col.SetGreen(128 + (col.GetGreen()>>1));
				col.SetBlue(128 + (col.GetBlue()>>1));
			}

			outImage.SetPixel(x, y, col);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

float crpmDynamicProgramming::Cell::GetSum() const
{
	float minSum = FLT_MAX;
	for(atArray<Pair>::const_iterator it=m_Pairs.begin(); it!=m_Pairs.end(); ++it)
	{
		minSum = Min(it->m_Sum, minSum);
	}

	return minSum;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDynamicProgramming::Cell::TryAddSumSlopeCounterPair(float candidateSum, const crpmSlope& candidateSlopeCounter)
{
	// Each candidate sum and slope counter pair competes against the others stored.  The candidate can be
	// better in two ways: it can either be less constrained than the best slope counter, or have a better sum.
	// In either of these cases, we should continue the iteration, as cells further down the path may be
	// updated too.

	atArray<Pair>::iterator it = m_Pairs.begin();
	bool found = false;
	bool proceed = true;

	while(it != m_Pairs.end())
	{
		Pair& p = *it;

		// this iterator matches the candidate, update the sum if it is better
		if (p.m_SlopeCounter == candidateSlopeCounter)
		{
			found = true;
			if (candidateSum < p.m_Sum)
			{
				p.m_Sum = candidateSum;
			}
		}
		// this iterator has less freedom than the candidate
		else if (p.m_SlopeCounter.IsStrictlyMoreConstrained(candidateSlopeCounter))
		{
			// and its sum is bigger, so delete it!
			if (candidateSum < p.m_Sum)
			{
				AssertMsg(p.m_SlopeCounter.Get() != 0 , "trying to remove the least constrained iterator");
				it = m_Pairs.erase(it);

				continue; // don't increment it
				// candidate either gets added later, or is made redundant by a better iterator
			}
			else
			{
				// the candidate has a better slope constraint, but its sum is worse...
				// discard prematurely for optimization purposes
				proceed = false;
			}
		}
		// this iterator has more freedom than the candidate, so we're competing with it for the right to continue
		else if (p.m_SlopeCounter.IsStrictlyLessConstrained(candidateSlopeCounter))
		{
			// it has a better sum than the candidate too, so don't bother going any further
			if (candidateSum >= p.m_Sum)
			{
				proceed = false;
				found = true;
			}
		}

		++it;
	}

	if (!found)
	{
		m_Pairs.PushAndGrow(Pair(candidateSum, candidateSlopeCounter));
	}

	return proceed;
}

////////////////////////////////////////////////////////////////////////////////

void crpmDynamicProgramming::Cell::Reset()
{
	m_Pairs.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crpmDynamicProgramming::CalcCellValues(const crpmDistanceGrid::Path& dstPath)
{
	//
	// This algorithm is a new improved version of the dynamic programming solution
	// described in Kovar's paper.  It actually enforces the constraints of the paths
	// while percolating(filtering) the values from the target locations -- which is more
	// accurate.  Since we take into account full minima chains in one go, it is
	// also more efficient than dealing with cells individually (but slower
	// otherwise).
	//
	// Instead of scanning along X and Y in the grid, the process involves scanning
	// all legal paths from each of the cells in the destination chain, and updating
	// the cell values if they can be improved.
	//

	atArray<PathIterator> pathIter;

	// load up empty paths from each cell in the destination chain
	crpmDistanceGrid::Path::const_iterator it;
	for (it=dstPath.begin(); it != dstPath.end(); ++it)
	{
		// do not consider starting paths that are outside the DP area
		if((it->x < m_MinX) || (it->x >= m_MaxX) || (it->y < m_MinY) || (it->y >= m_MaxY))
		{
			continue;
		}

		pathIter.PushAndGrow(PathIterator(it->x, it->y, crpmSlope(m_SlopeLimit), GetCellDistance(it->x, it->y), 1));

		m_AreaMaxX = Min(it->x, m_AreaMaxX);
		m_AreaMaxY = Min(it->y, m_AreaMaxY);
	}

	// sort the iterators by the length of the path, so the shortest come first
	std::make_heap(pathIter.begin(), pathIter.end());

	int iterationCount = 0;

	// main loop, efficient recursion with a stack
	while(!pathIter.empty())
	{
		++iterationCount;

		// make sure the best iterator is at the back of the array
		std::pop_heap(pathIter.begin(), pathIter.end());

		PathIterator pi = pathIter.back();
		pathIter.Pop();

		// check validity criteria for current path
		if(!pi.m_SlopeCounter.IsInsideLimit(m_SlopeLimit) || (!IsCellValid(pi.m_X, pi.m_Y)))
		{
			// don't recurse any more from this iterator
			continue;
		}

		// current iterator is valid so try to improve the cell value
		if(!UpdateCellSum(pi.m_X, pi.m_Y, pi.GetValue(), pi.m_SlopeCounter))
		{
			// skip this path, iterator was worse than previous one
			continue;
		}

		// push neighbors onto the stack to be checked further
		if(pi.m_X > m_MinX)
		{
			PathIterator leftNeighbour(pi.m_X-1, pi.m_Y,
				crpmSlope(pi.m_SlopeCounter).StepLeft(),
				pi.m_Sum + GetCellDistance(pi.m_X-1, pi.m_Y),
				pi.m_Length + 1);

			pathIter.PushAndGrow(leftNeighbour);
			std::push_heap(pathIter.begin(), pathIter.end());
		}

		if(pi.m_Y > m_MinY)
		{
			PathIterator lowerNeighbour(pi.m_X, pi.m_Y-1,
				crpmSlope(pi.m_SlopeCounter).StepDown(),
				pi.m_Sum + GetCellDistance(pi.m_X, pi.m_Y-1),
				pi.m_Length + 1);

			pathIter.PushAndGrow(lowerNeighbour);
			std::push_heap(pathIter.begin(), pathIter.end());
		}

		if((pi.m_X > m_MinX) && (pi.m_Y > m_MinY))
		{
			PathIterator lowerLeftNeighbour(pi.m_X-1, pi.m_Y-1,
				crpmSlope(pi.m_SlopeCounter).StepDiagonal(),
				pi.m_Sum + GetCellDistance(pi.m_X-1, pi.m_Y-1),
				pi.m_Length + 1);

			pathIter.PushAndGrow(lowerLeftNeighbour);
			std::push_heap(pathIter.begin(), pathIter.end());
		}
	}

	m_Destination = dstPath;
}

////////////////////////////////////////////////////////////////////////////////

void crpmDynamicProgramming::BuildBridgeFrom(const crpmDistanceGrid::Path& srcPath)
{
	m_BestPath.Clear();

	for(crpmDistanceGrid::Path::const_iterator it=srcPath.begin(); it!=srcPath.end(); ++it)
	{
		// the window is set by the user, usually to match the dst path
		if (it->x >= m_MaxX || it->y >= m_MaxY)
		{
			continue;
		}

		crpmDistanceGrid::Path bridge;
		BuildBridgeFrom(it->x, it->y, bridge);

		bool srcContainsPath = srcPath.Contains(bridge.GetStartX(), bridge.GetStartY());
		bool dstContainsPath = m_Destination.Contains(bridge.GetEndX(), bridge.GetEndY());
		if(srcContainsPath && dstContainsPath)
		{
			if((m_BestPath.GetLength() == 0)
				|| (sm_UseAverageAsValue && bridge.GetDistanceAverage() < m_BestPath.GetDistanceAverage())
				|| (!sm_UseAverageAsValue && bridge.GetDistanceSum() < m_BestPath.GetDistanceSum()))
			{
				m_BestPath = bridge;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDynamicProgramming::BuildBridgeFrom(int x, int y, crpmDistanceGrid::Path& outBridge)
{
    crpmSlope slopeCounter(m_SlopeLimit);
    int nextX = x;
	int nextY = y;

    outBridge.Clear();

    do
    {
        outBridge.AddSafe(nextX, nextY, GetCellDistance(nextX, nextY));
    }
	while(FindBestNeighbor(nextX, nextY, slopeCounter));
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDynamicProgramming::FindBestNeighbor(int& x, int& y, crpmSlope& slopeCounter)
{
    const float invalid = FLT_MAX;

    Assert((x<m_MaxX) && (y<m_MaxY));

    bool stepUpValid = (y+1<m_MaxY);
    bool stepRightValid = (x+1<m_MaxX);

    bool upperNeigbourValid = slopeCounter.CanStepUp(m_SlopeLimit) && stepUpValid;
    bool rightNeighbourValid = slopeCounter.CanStepRight(m_SlopeLimit) && stepRightValid;
    bool upperRightNeighbourValid = stepUpValid && stepRightValid;

    float upperNeighbourValue = upperNeigbourValid ? GetCellSum(x,y+1) : invalid;
    float rightNeighbourValue = rightNeighbourValid ? GetCellSum(x+1,y) : invalid;
    float upperRightNeighbourValue = upperRightNeighbourValid ? GetCellSum(x+1,y+1) : invalid;

    if ((upperRightNeighbourValue <= upperNeighbourValue)
        && (upperRightNeighbourValue <= rightNeighbourValue)
        && (upperRightNeighbourValue != invalid)
        )
    {
        slopeCounter.StepDiagonal();
        ++x; ++y;
        return true;
    }
    else if ((upperNeighbourValue <= upperRightNeighbourValue)
        && (upperNeighbourValue <= rightNeighbourValue)
        && (upperNeighbourValue != invalid))
    {
        slopeCounter.StepUp();
        ++y;
        return true;
    }
    else if ((rightNeighbourValue <= upperRightNeighbourValue)
        && (rightNeighbourValue <= upperNeighbourValue)
        && (rightNeighbourValue != invalid))
    {
        slopeCounter.StepRight();
        ++x;
        return true;
    }

    return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDynamicProgramming::IsCellValid(int x, int y) const
{
    const float slope1a = float(x - m_AreaMinX+1) / float(y - m_AreaMinY+1);
    const float slope1b = float(x - m_MinX+1) / float(y - m_MinY+1);
    const float slope2a = float(m_AreaMaxX - x+1) / float(m_AreaMaxY - y+1);
    const float slope2b = float(m_MaxX - x+1) / float(m_MaxY - y+1);

    // TODO - add further optimization like in the thesis..
    if(((slope1a > 0.f) && (slope1a < 1.f/m_SlopeLimit))
		|| (slope1b > m_SlopeLimit)
		|| ((slope2a > 0.f) && (slope2a < 1.f/m_SlopeLimit))
		|| (slope2b > m_SlopeLimit))
    {
        return false;
    }

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDynamicProgramming::UpdateCellSum(int x, int y, float sum, const crpmSlope& slopeCounter)
{
    // keep track of minima and maxima for visualization purposes
	m_MinSum = Min(sum, m_MinSum);
	m_MaxSum = Max(sum, m_MaxSum);

    // update the cell if the value is lower, prune this branch of the search if possible
	Cell& cell = GetCell(x,y);
	return cell.TryAddSumSlopeCounterPair(sum, slopeCounter);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDynamicProgramming::sm_UseAverageAsValue = false;

////////////////////////////////////////////////////////////////////////////////


} // namespace rage
