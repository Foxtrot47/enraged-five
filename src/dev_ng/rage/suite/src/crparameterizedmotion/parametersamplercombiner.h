//
// crparameterizedmotion/parametersamplercombiner.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERSAMPLERCOMBINER_H
#define CRPARAMETERIZEDMOTION_PARAMETERSAMPLERCOMBINER_H

#include "parametersampler.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Parameter sampler combiner class
// Combine multiples samplers with a specific mathematics operation
class crpmParameterSamplerCombiner : public crpmParameterSamplerContainer
{
public:

	// PURPOSE: Default constructor
	crpmParameterSamplerCombiner();

	// PURPOSE: Destructor
	virtual ~crpmParameterSamplerCombiner();

	// PURPOSE:
	CRPM_DECLARE_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerCombiner);

	// PURPOSE: Add additional sampler to the collection
	virtual void AppendSampler(crpmParameterSampler& sampler);

protected:

	// PURPOSE: Calculate parameter for given set of weights
	virtual void CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const;

	// PURPOSE: Internal function, override sampler property configuration
	virtual bool ConfigProperty(const char* token, ConfigParser& parser);

private:
	enum eCombinerOperation
	{
		kCombinerOperationMinimum,
		kCombinerOperationMaximum,
		kCombinerOperationMean,
		kCombinerOperationSum,
		kCombinerOperationNum
	};
	eCombinerOperation m_Operation;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERSAMPLERCOMBINER_H
