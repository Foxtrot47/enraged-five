//
// crparameterizedmotion/parameterizedmotion.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "parameterizedmotion.h"

#include "parameter.h"
#include "parameterization.h"
#include "parameterizedmotionsimple.h"
#include "parameterizedmotionvariable.h"
#include "parametersampler.h"

#include "crclip/framecompositor.h"
#include "crmetadata/tags.h"
#include "crmetadata/dumpoutput.h"
#include "data/safestruct.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionData::crpmParameterizedMotionData(eParameterizedMotionDataType pmDataType)
: m_RefCount(0)
, m_Type(kPmDataTypeNone)
{
	Assert(pmDataType != kPmDataTypeNone);
	
	AddRef();

	m_Type = pmDataType;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionData::crpmParameterizedMotionData(datResource& rsc)
: m_Name(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionData::~crpmParameterizedMotionData()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionData::Place(void* that, datResource& rsc)
{
	Assert(that);
	switch(((crpmParameterizedMotionData*)that)->GetType())
	{
	case kPmDataTypeSimple:
		crpmParameterizedMotionSimpleData::Place((crpmParameterizedMotionSimpleData*)that, rsc);
		break;

	case kPmDataTypeVariable:
		crpmParameterizedMotionVariableData::Place((crpmParameterizedMotionVariableData*)that, rsc);
		break;

	default:
		Assert(0);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionData::VirtualConstructFromPtr(datResource& rsc, crpmParameterizedMotionData* base)
{
	Assert(base);
	base->Place(base, rsc);
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crpmParameterizedMotionData::eParameterizedMotionDataType);

void crpmParameterizedMotionData::DeclareStruct(datTypeStruct& s)
{
	pgBase::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crpmParameterizedMotionData, pgBase)
	SSTRUCT_FIELD(crpmParameterizedMotionData, m_RefCount)
	SSTRUCT_FIELD(crpmParameterizedMotionData, m_Type)
	SSTRUCT_FIELD(crpmParameterizedMotionData, m_Name)
	SSTRUCT_END(crpmParameterizedMotionData)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionData::Shutdown()
{
	m_Type = kPmDataTypeNone;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
crpmParameterizedMotionData* crpmParameterizedMotionData::AllocateAndLoad(const char* filename, crpmParameterizedMotionLoader* UNUSED_PARAM(pmLoader), crClipLoader* clipLoader, crAnimLoader* animLoader)
{
	const char* extension = ASSET.FindExtensionInPath(filename);
	if(!stricmp(extension, ".spm"))
	{
		return crpmParameterizedMotionSimpleData::AllocateAndLoad(filename, clipLoader, animLoader);
	}
	else if(!stricmp(extension, ".vpm"))
	{
		return crpmParameterizedMotionVariableData::AllocateAndLoad(filename, clipLoader, animLoader);
	}
	else if(!stricmp(extension, ".cpm"))
	{
		// TODO --- load composite pm data
		return NULL;
	}
	else
	{
		return NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionData::Dump(atString& inoutText) const
{
	crDumpOutputString output;
	Dump(output);
	inoutText += output.GetString();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionData::Dump(crDumpOutput& output) const
{
	output.Outputf(0, "filename", "%s", m_Name.c_str());
	output.Outputf(0, "pmtype", "(%d)", int(m_Type));
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterizedMotionData::PushParameterizedMotionFolder() const
{
	char pmFolderBuf[RAGE_MAX_PATH];
	safecpy(pmFolderBuf, GetName());

	fiAssetManager::RemoveNameFromPath(pmFolderBuf, RAGE_MAX_PATH, pmFolderBuf);

	if(pmFolderBuf[0] && strcmp(pmFolderBuf, GetName()))
	{
		ASSET.PushFolder(pmFolderBuf);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotion::crpmParameterizedMotion(eParameterizedMotionType pmType)
: m_Type(kPmTypeNone)
, m_Data(NULL)
{
	Assert(pmType != kPmTypeNone);
	m_Type = pmType;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotion::~crpmParameterizedMotion()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotion::Clear()
{
	if(m_Data)
	{
		m_Data->Release();
		m_Data = NULL;
	}

	m_TempParameter.Clear();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotion::Shutdown()
{
	m_Type = kPmTypeNone;

	if(m_Data)
	{
		m_Data->Release();
		m_Data = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotion* crpmParameterizedMotion::Allocate(eParameterizedMotionType type)
{
	switch(type)
	{
	case crpmParameterizedMotionData::kPmDataTypeSimple:
		return rage_new crpmParameterizedMotionSimple;

	case crpmParameterizedMotionData::kPmDataTypeVariable:
		return rage_new crpmParameterizedMotionVariable;

	default:
		return NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotion* crpmParameterizedMotion::AllocateFromData(const crpmParameterizedMotionData& data)
{
	eParameterizedMotionType type = GetTypeFromData(data);

	crpmParameterizedMotion* pm = Allocate(type);
	if(pm)
	{
		switch(pm->GetType())
		{
		case crpmParameterizedMotion::kPmTypeSimple:
			static_cast<crpmParameterizedMotionSimple*>(pm)->Init(static_cast<const crpmParameterizedMotionSimpleData&>(data));
			break;

		case crpmParameterizedMotion::kPmTypeVariable:
			static_cast<crpmParameterizedMotionVariable*>(pm)->Init(static_cast<const crpmParameterizedMotionVariableData&>(data));
			break;

		default:
			break;
		}
	}

	return pm;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotion::eParameterizedMotionType crpmParameterizedMotion::GetTypeFromData(const crpmParameterizedMotionData& data)
{
	switch(data.GetType())
	{
	case crpmParameterizedMotionData::kPmDataTypeSimple:
		return kPmTypeSimple;

	case crpmParameterizedMotionData::kPmDataTypeVariable:
		return kPmTypeVariable;

	default:
		return kPmTypeNone;
	}
}

////////////////////////////////////////////////////////////////////////////////

#if USE_PM_COMPOSITOR
void crpmParameterizedMotion::Composite(crFrame& outFrame)
{
	crFrameCompositorFrameLazy compositor(outFrame);
	Composite(compositor);
}
#endif // USE_PM_COMPOSITOR

////////////////////////////////////////////////////////////////////////////////

int crpmParameterizedMotion::GetNumParameterDimensions() const
{
	return GetParameterization().GetNumDimensions();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotion::GetParameter(crpmParameter& outParameter) const
{
	outParameter = GetParameter();
}

////////////////////////////////////////////////////////////////////////////////

float crpmParameterizedMotion::GetParameterValue(int dim) const
{
	GetParameter(m_TempParameter);
	return m_TempParameter.GetValue(dim);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotion::SetParameterValue(int dim, float value, bool force)
{
	GetParameter(m_TempParameter);
	m_TempParameter.SetValue(dim, value);
	SetParameter(m_TempParameter, force);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotion::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		crpmParameterSampler::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotion::ShutdownClass()
{
	crpmParameterSampler::ShutdownClass();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotion::InternalPreInit(int worstNumParameterDimensions)
{
	m_TempParameter.PreInit(worstNumParameterDimensions);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotion::InternalInit(const crpmParameterizedMotionData& data)
{
	Clear();

	Assert(!m_Data);
	m_Data = &data;
	m_Data->AddRef();

	m_TempParameter.Init(GetNumParameterDimensions());
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterizedMotion::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
