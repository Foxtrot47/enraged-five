//
// crparameterizedmotion/colormapper.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "colormapper.h"

#include "math/amath.h"
#include "vector/color32.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmColorMapper::crpmColorMapper(float minValue, float maxValue, u32 colorCount)
: m_MinValue(minValue)
, m_MaxValue(maxValue)
, m_ColorCount(colorCount)
{
	float valuePerColorIdx = (m_MaxValue - m_MinValue) / static_cast<float>(m_ColorCount);
    m_InvValuePerColorIdx = 1.f / valuePerColorIdx;
}

////////////////////////////////////////////////////////////////////////////////

crpmColorMapper::~crpmColorMapper()
{
}

////////////////////////////////////////////////////////////////////////////////

Color32 crpmColorMapper::Map(float value) const
{
	u32 colorIdx = static_cast<int>((value - m_MinValue) * m_InvValuePerColorIdx);
	colorIdx = Clamp(colorIdx, 0u, m_ColorCount);

    return MapColorIndexToColor(colorIdx);
}

////////////////////////////////////////////////////////////////////////////////

Color32 crpmColorMapper::NegativeMap(float value) const
{
	u32 colorIdx = static_cast<int>((m_MaxValue - value) * m_InvValuePerColorIdx);
	colorIdx = Clamp(colorIdx, 0u, m_ColorCount);

   return MapColorIndexToColor(colorIdx);
}

////////////////////////////////////////////////////////////////////////////////

crpmColorMapperHeat::crpmColorMapperHeat(float minValue, float maxValue)
: crpmColorMapper(minValue, maxValue, 255*2)
{
}

////////////////////////////////////////////////////////////////////////////////

Color32 crpmColorMapperHeat::MapColorIndexToColor(u32 colorIndex) const
{
    // calculate RGB elements
	Color32 color(0, 0, 0);

    color.SetRed((colorIndex >= 255) ? (colorIndex - 255) : 0 );
    color.SetGreen((colorIndex <= 255) ? colorIndex : (m_ColorCount - colorIndex) );
    color.SetBlue((colorIndex <= 255) ? (255 - colorIndex) : 0 );

	int diff = 255 - Max(color.GetRed(), Max(color.GetGreen(), color.GetBlue()));

    if (color.GetRed() != 0)
    {
        color.SetRed(color.GetRed() + diff);
    }

    if (color.GetGreen() != 0)
    {
        color.SetGreen(color.GetGreen() + diff);
    }

    if (color.GetBlue() != 0)
    {
        color.SetBlue(color.GetBlue() + diff);
    }

	return color;
}

////////////////////////////////////////////////////////////////////////////////

crpmColorMapperGrayscale::crpmColorMapperGrayscale(float minValue, float maxValue)
: crpmColorMapper(minValue, maxValue, 256)
{
}

////////////////////////////////////////////////////////////////////////////////

Color32 crpmColorMapperGrayscale::MapColorIndexToColor(u32 colorIndex) const
{
	return Color32((int)colorIndex, (int)colorIndex, (int)colorIndex);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
