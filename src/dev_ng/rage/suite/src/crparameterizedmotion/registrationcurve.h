//
// crparameterizedmotion/registrationcurve.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_REGISTRATIONCURVE_H
#define CRPARAMETERIZEDMOTION_REGISTRATIONCURVE_H

#include "pointcloudtransform.h"
#include "spline.h"

#include "crclip/clip.h"
#include "crclip/clips.h"

namespace rage
{

class crDumpOutput;
class crpmRegistrationCurveCalculator;
class crpmWeights;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE:  Registration curve non-instance data storage class
class crpmRegistrationCurveData
{
	friend class crpmBlendDatabaseCalculator;
	friend class crpmRegistrationCurveCalculator;

public:
	
	// PURPOSE: Constructor
	crpmRegistrationCurveData();

	// PURPOSE: Resource constructor
	crpmRegistrationCurveData(datResource&);

	// PURPOSE: Destructor
    ~crpmRegistrationCurveData();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmRegistrationCurveData);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Get number of clips in registration curve
	u16 GetNumClips() const;
	
	// PURPOSE: Get index of reference clip
	u16 GetReferenceClipIndex() const;

	// PURPOSE: Is registration curve looped
	bool IsLooped() const;

	// PURPOSE: Map registration curve phase to phase in a particular clip
	float MapUToClipU(float u, int clipIdx) const;

	// PURPOSE: Map registration curve phase to time in a particular clip
	float MapUToTime(float u, int clipIdx, const crClips& clips) const;

	// PURPOSE: Map phase in a particular clip to registration curve phase
	float MapClipUToU(float clipU, int clipIdx) const;

	// PURPOSE: Map time in a particular clip to registration curve phase
	float MapTimeToU(float time, int clipIdx, const crClips& clips) const;

	// PURPOSE: Get max registration curve phase for a particular clip (as integer)
	u32 GetMaxU(int clipIdx) const;

	// PURPOSE: Get max number of loops
	u32 GetMaxNumLoops() const;

#if CR_DEV
	// PURPOSE: Serialize
	void Serialize(datSerialize& s, float* rescale=NULL);

	// PURPOSE: Dump output
	void Dump(crDumpOutput&) const;
#endif // CR_DEV

protected:

	atArray<datOwner<crpmSeries<float> > > m_AlignmentCurves;

	u16 m_NumClips;
	u16 m_ReferenceClipIndex;
	u16 m_MaxNumLoops;
	bool m_IsLooped;
	datPadding<1> m_Padding;
};

////////////////////////////////////////////////////////////////////////////////

inline u16 crpmRegistrationCurveData::GetNumClips() const
{
	return m_NumClips;
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crpmRegistrationCurveData::GetReferenceClipIndex() const
{
	return m_ReferenceClipIndex;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmRegistrationCurveData::IsLooped() const
{
	return m_IsLooped;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmRegistrationCurveData::MapUToTime(float u, int clipIdx, const crClips& clips) const
{
	return clips.GetClip(clipIdx)->ConvertPhaseToTime(MapUToClipU(u, clipIdx));
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmRegistrationCurveData::MapTimeToU(float time, int clipIdx, const crClips& clips) const
{
	return MapClipUToU(clips.GetClip(clipIdx)->ConvertTimeToPhase(time), clipIdx);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crpmRegistrationCurveData::GetMaxU(int clipIdx) const
{
	return u32(MapClipUToU(1.f, clipIdx)+0.5f);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crpmRegistrationCurveData::GetMaxNumLoops() const
{
	return u32(m_MaxNumLoops);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRPARAMETERIZEDMOTION_REGISTRATIONCURVE_H
