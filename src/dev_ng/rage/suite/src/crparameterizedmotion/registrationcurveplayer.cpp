//
// crparameterizedmotion/registrationcurveplayer.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "registrationcurveplayer.h"

#include "registrationcurve.h"

#include "cranimation/frame.h"
#include "crclip/clip.h"
#include "crclip/clips.h"
#include "crclip/framecompositor.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurvePlayer::crpmRegistrationCurvePlayer()
: m_RcData(NULL)
, m_Clips(NULL)
, m_U(0.f)
, m_DeltaU(0.f)
, m_PostDeltaU(0.f)
, m_LoopCount(0)
, m_IsLooped(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurvePlayer::crpmRegistrationCurvePlayer(int worstNumClips)
: m_RcData(NULL)
, m_Clips(NULL)
, m_U(0.f)
, m_DeltaU(0.f)
, m_PostDeltaU(0.f)
, m_LoopCount(0)
, m_IsLooped(false)
{
	PreInit(worstNumClips);
}

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurvePlayer::crpmRegistrationCurvePlayer(const crpmRegistrationCurveData& rcData, const crClips& clips, const crpmWeights& weights)
: m_RcData(NULL)
, m_Clips(NULL)
, m_U(0.f)
, m_DeltaU(0.f)
, m_PostDeltaU(0.f)
, m_LoopCount(0)
, m_IsLooped(false)
{
	Init(rcData, clips, weights);
}

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurvePlayer::~crpmRegistrationCurvePlayer()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::PreInit(int worstNumClips)
{
	m_Weights.PreInit(worstNumClips);
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::Init(const crpmRegistrationCurveData& rcData, const crClips& clips, const crpmWeights& weights)
{
	Assert(rcData.GetNumClips() == clips.GetNumClips());
	Assert(rcData.GetNumClips() == weights.GetNumWeights());

	m_RcData = &rcData;
	m_Clips = &clips;
	m_Weights.Init(weights);
	m_U = 0.f;
	m_DeltaU = 0.f;
	m_PostDeltaU = 0.f;
	m_LoopCount = 0;
	m_IsLooped = m_RcData->IsLooped();
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::Clear()
{
	m_RcData = NULL;
	m_Clips = NULL;
	m_U = 0.f;
	m_DeltaU = 0.f;
	m_PostDeltaU = 0.f;
	m_LoopCount = 0;
	m_IsLooped = false;

	m_Weights.Clear();
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::Shutdown()
{
	m_RcData = NULL;
	m_Clips = NULL;
	m_U = 0.f;
	m_DeltaU = 0.f;
	m_PostDeltaU = 0.f;
	m_LoopCount = 0;
	m_IsLooped = false;

	m_Weights.Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::Reset()
{
	Reset(m_Weights);
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::Reset(const crpmWeights& weights)
{
	m_U = 0.f;
	m_DeltaU = 0.f;
	m_PostDeltaU = 0.f;
	m_LoopCount = 0;
	UpdateInternal(weights);
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::Update(float deltaTime, const crpmWeights& weights)
{
	Assert(deltaTime >= 0.f);

	float elapsedU = 0.f;
	while(deltaTime > SMALL_FLOAT)
	{
		float timeRemaining = CalcDeltaTime(1.f-m_U, weights);
		if(timeRemaining <= deltaTime)
		{
			deltaTime -= timeRemaining;
			elapsedU += 1.f-m_U;
		
			if(m_IsLooped)
			{
				m_U = 0.f;
				m_LoopCount++;
			}
			else
			{
				m_U = 1.f;
				break;
			}
		}
		else
		{
			float deltaU = CalcDeltaU(deltaTime, weights);
			m_U = Min(m_U + deltaU, 1.f);

			deltaTime = 0.f;
			elapsedU += deltaU;
			break;
		}
	}

	m_DeltaU = elapsedU;

	m_DeltaU += m_PostDeltaU;
	m_PostDeltaU = 0.f;
	
	m_LoopCount %= m_RcData->GetMaxNumLoops();

	UpdateInternal(weights);
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::UpdateU(float deltaU, const crpmWeights& weights)
{
	Assert(deltaU >= 0.f);

	float elapsedU = 0.f;
	if(deltaU > SMALL_FLOAT)
	{
		if(IsLooped())
		{
			elapsedU = deltaU;
			float floorU = floorf(m_U+deltaU);
			m_U = Min(m_U+deltaU - floorU, 1.f);
			m_LoopCount += u32(floorU);
		}
		else
		{
			elapsedU = Min(deltaU, 1.f-m_U);
			m_U = Min(m_U+deltaU, 1.f);
		}
	}

	m_DeltaU = elapsedU;

	m_DeltaU += m_PostDeltaU;
	m_PostDeltaU = 0.f;

	m_LoopCount %= m_RcData->GetMaxNumLoops();

	UpdateInternal(weights);
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::SetU(float u, const crpmWeights& weights)
{
	m_U = 0.f;
	UpdateU(u, weights);
	m_DeltaU = 0.f;  // ???
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::SetDeltaU(float deltaU)
{
	Assert(deltaU >= 0.f);
	m_DeltaU = deltaU;
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurvePlayer::GetDeltaU() const
{
	return m_DeltaU;
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::SetPostDeltaU(float deltaU)
{
	Assert(deltaU >= 0.f);
	m_PostDeltaU = deltaU;
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurvePlayer::GetPostDeltaU() const
{
	return m_PostDeltaU;
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::Composite(crFrameCompositor& inoutCompositor) const
{
	Assert(m_Clips);

	const int numClips = m_Clips->GetNumClips();
	float sum = 0.f;

	static bool output = false;

	if(output)
	{
		Printf("u %f du %f\n", m_U, m_DeltaU);
	}

	bool first = true;
	for(int i=0; i<numClips; ++i)
	{
		float weight = m_Weights.GetNormalizedWeight(i);
		if(!IsNearZero(weight))
		{
			float corrected = 1.f;
			sum += weight;
			if(!IsClose(weight, sum))
			{
				corrected = weight / sum;
			}

			u32 imaxU = m_RcData->GetMaxU(i);
			float maxU = float(imaxU);
			float loopU = float(m_LoopCount%imaxU);

			const crClip* clip = m_Clips->GetClip(i);
			float time = m_RcData->MapUToTime(loopU+m_U, i, *m_Clips);

			float deltaTime = 0.f;
			if(m_DeltaU > 0.f)
			{
				float prevU = m_U-m_DeltaU;
				deltaTime = time - m_RcData->MapUToTime(fmodf(prevU+loopU+ceilf(m_DeltaU/maxU)*maxU, maxU), i, *m_Clips) - clip->GetDuration() * floor((prevU+loopU) / maxU);
			}

			if(output)
			{
				Printf("[%d] w %f d %f t %f dt %f\n", i, weight, clip->GetDuration(), time, deltaTime);
			}

			clip->Composite(inoutCompositor, time, deltaTime);
			if(!first)
			{
				inoutCompositor.Blend(corrected);
			}
			first = false;

			// TODO --- transform?
		}
	}	
}

////////////////////////////////////////////////////////////////////////////////

// BEGIN TEMP - GET USE_PM_COMPOSITOR
} // namespace rage
#include "parameterizedmotion.h"
namespace rage {
// END TEMP - GET USE_PM_COMPOSITOR

void crpmRegistrationCurvePlayer::Composite(crFrame& outFrame) const
{
#if USE_PM_COMPOSITOR
	crFrameCompositorFrameLazy compositor(outFrame);
	Composite(compositor);
#else // USE_PM_COMPOSTIOR
	Assert(m_Clips);

	const int numClips = m_Clips->GetNumClips();
	float sum = 0.f;

	static bool output = false;

	if(output)
		Printf("u %f du %f\n", m_U, m_DeltaU);

	for(int i=0; i<numClips; ++i)
	{
		float weight = m_Weights.GetNormalizedWeight(i);
		if(!IsNearZero(weight))
		{
			float corrected = 1.f;
			sum += weight;
			if(!IsClose(weight, sum))
			{
				corrected = weight / sum;
			}

			u32 imaxU = m_RcData->GetMaxU(i);
			float maxU = float(imaxU);
			float loopU = float(m_LoopCount%imaxU);

			const crClip* clip = m_Clips->GetClip(i);
			float time = m_RcData->MapUToTime(loopU+m_U, i, *m_Clips);

			float deltaTime = 0.f;
			if(m_DeltaU > 0.f)
			{
				float prevU = m_U-m_DeltaU;
				deltaTime = time - m_RcData->MapUToTime(fmodf(prevU+loopU+ceilf(m_DeltaU/maxU)*maxU, maxU), i, *m_Clips) - clip->GetDuration() * floor((prevU+loopU) / maxU);
			}

			if(output)
			{
				Printf("[%d] w %f d %f t %f dt %f\n", i, weight, clip->GetDuration(), time, deltaTime);
			}

			// TODO --- composite blend?
			crFrame frame;
			frame.Init(outFrame);
			clip->Composite(frame, time, deltaTime);
			outFrame.Blend(corrected, frame);

			// TODO --- transform?
		}
	}
#endif // USE_PM_COMPOSITOR
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurvePlayer::GetRemainingTime() const
{
	return CalcDeltaTime(1.f-m_U, m_Weights);
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurvePlayer::GetDuration() const
{
	return CalcTotalTime(m_Weights);
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurvePlayer::GetMaxDuration() const
{
	Assert(m_Clips);

	float maxDuration = 0.f;

	const int numClips = m_Clips->GetNumClips();
	for(int i=0; i<numClips; ++i)
	{
		maxDuration = Max(m_Clips->GetClip(i)->GetDuration(), maxDuration);
	}

	return maxDuration;
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurvePlayer::CalcDeltaU(float deltaTime, const crpmWeights& weights) const
{
	return CalcDeltaU(m_U, deltaTime, weights, *m_Clips);
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurvePlayer::CalcDeltaTime(float deltaU, const crpmWeights& weights) const
{
	Assert(m_RcData);
	Assert(deltaU >= 0.f);

	if(deltaU > 0.f)
	{
		float u = Min(m_U+deltaU, 1.f);

		float deltaTime = 0.f;

		const int numClips = m_RcData->GetNumClips();
		for(int i=0; i<numClips; ++i)
		{
			float w = weights.GetNormalizedWeight(i);
			if(w > SMALL_FLOAT)
			{
				u32 imaxU = m_RcData->GetMaxU(i);
				float loopU = float(m_LoopCount%imaxU);
				float startTime = m_RcData->MapUToTime(m_U+loopU, i, *m_Clips);
				float endTime = m_RcData->MapUToTime(u+loopU, i, *m_Clips);

				deltaTime += (endTime-startTime)*w;
			}
		}

		return deltaTime;
	}
	
	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurvePlayer::CalcTotalTime(const crpmWeights& weights) const
{
	Assert(m_Clips);

	float totalTime = 0.f;

	const int numClips = m_Clips->GetNumClips();
	for(int i=0; i<numClips; ++i)
	{
		float w = weights.GetNormalizedWeight(i);
		if(w > SMALL_FLOAT)
		{
			totalTime += m_Clips->GetClip(i)->GetDuration()*w/m_RcData->GetMaxU(i);
		}
	}

	return totalTime;
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurvePlayer::UpdateInternal(const crpmWeights& weights)
{
	m_Weights = weights;
	// TODO --- NOW THERE'S NO GLOBAL ALIGNMENTS, ALIGNMENTS OR TIMES, NOT SURE WHAT THE PURPOSE OF THIS IS...
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurvePlayer::CalcDeltaU(float u, float deltaTime, const crpmWeights& weights, const crClips& clips) const
{
	Assert(m_RcData);

	Assert(InRange(u, 0.f, 1.f));
	Assert(deltaTime >= 0.f);

	if(deltaTime > 0.f)
	{
		float deltaUWeightedSum = 0.f;

		const int numClips = m_RcData->GetNumClips();
		for(int i=0; i<numClips; ++i)
		{
			float normalizedWeight = weights.GetNormalizedWeight(i);
			if(IsNearZero(normalizedWeight))
			{
				continue;
			}

			float clipDeltaU = clips.GetClip(i)->ConvertTimeToPhase(deltaTime);
			float clipU = m_RcData->MapUToClipU(u, i);

			float nextClipU = Min(clipU + clipDeltaU, 1.f);
			float nextU = m_RcData->MapClipUToU(nextClipU, i);

			float deltaU = nextU-u;
			if(deltaU < 0.f)
			{
				Warningf("crpmRegistrationCurvePlayer::CalcDeltaU - u must strictly increase");
				deltaU = 0.f;
			}

			deltaUWeightedSum += deltaU * normalizedWeight;
		}

		return deltaUWeightedSum;
	}

	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

