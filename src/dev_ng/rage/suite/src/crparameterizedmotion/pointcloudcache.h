//
// crparameterizedmotion/pointcloudcache.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_POINTCLOUDCACHE_H
#define CRPARAMETERIZEDMOTION_POINTCLOUDCACHE_H

#include "pointcloud.h"

#include "atl/array.h"

namespace rage
{

class crClip;
class crWeightSet;


// PURPOSE: Calculates and caches a set of point clouds
class crpmPointCloudCache
{
public:

	// PURPOSE: Default constructor
	crpmPointCloudCache();

	// PURPOSE: Initializing constructor
	// PARAMS:
	// clip - clip to use for building point clouds
	// skelData - skeleton data (not stored, but used to build temporary intermediate structures)
	// windowSize - number of adjacent "frames" to consider when building a point cloud
	// sampleRate - frequency at which to sample "frames" at
	// weightSet - weights to use for designating/biasing particular bones participation in cloud
	// filterWeightSet - weights to use for eliminating particular bones from update
	// filterMover - eliminate mover from update (default false)
	// NOTES: automatically builds cache with values supplied
	crpmPointCloudCache(const crClip& clip, const crSkeletonData& skelData, u32 windowSize=1, float sampleRate=1.f/30.f, const crWeightSet* weightSet=NULL, const crWeightSet* filterWeightSet=NULL, bool filterMover=false);

	// PURPOSE: Destructor
	~crpmPointCloudCache();


	// PURPOSE: Initialize the point cloud cache
	// PARAMS:
	// clip - clip to use for building point clouds
	// skelData - skeleton data (not stored, but used to build temporary intermediate structures)
	// windowSize - number of adjacent "frames" to consider when building a point cloud
	// sampleRate - frequency at which to sample "frames" at
	// weightSet - weights to use for designating/biasing particular bones participation in cloud
	// filterWeightSet - weights to use for eliminating particular bones from update
	// filterMover - eliminate mover from update (default false)
	// NOTES: automatically builds cache with values supplied
	void Init(const crClip& clip, const crSkeletonData& skelData, u32 windowSize=1, float sampleRate=1.f/30.f, const crWeightSet* weightSet=NULL, const crWeightSet* filterWeightSet=NULL, bool filterMover=false);

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();


	// PURPOSE: Set the clip to use
	// PARAMS: clip - clip to use for building point clouds
	void SetClip(const crClip* clip);

	// PURPOSE: Set the window size
	// PARAMS: windowSize - number of adjacent "frames" to consider when building a point cloud
	// NOTES: A window size of zero means only the current frame is used to build
	// the point cloud, while a window size of one means the previous, current and next
	// frames are used etc.
	void SetWindowSize(u32 windowSize);

	// PURPOSE: Set the sample rate
	// PARAMS: sampleRate - frequency at which to sample "frames" at
	void SetSampleRate(float sampleRate);

	// PURPOSE: Set the alignment weight set
	// PARAMS: weightSet - weights to use for designating/biasing particular bones participation in cloud
	void SetWeightSet(const crWeightSet* weightSet);

	// PURPOSE: Set the partial weight set
	// PARAMS: partialWeightSet - weights to use for eliminating particular bones from update
	void SetFilterWeightSet(const crWeightSet* filterWeightSet);

	// PURPOSE: Filter mover from update
	// PARAMS: filterMover - true to eliminate mover from update, false to include
	void SetFilterMover(bool filterMover);


	// PURPOSE: Build the point clouds using current settings
	// PARAMS: skelData - skeleton data (used to construct temporary skeleton and frame)
	// NOTES: Constructs temporary skeleton and frame, then invokes true build function
	void Build(const crSkeletonData& skelData);

	// PURPOSE: Build the point clouds using current settings
	// PARAMS:
	// skeleton - temporary skeleton instance (used during calculation, contents will be overwritten)
	// frame - temporary frame (used during calculation, contents will be overwritten)
	void Build(crSkeleton& skel, crFrame& frame);

	// PURPOSE: Flush cached point clouds
	void Flush();


	// PURPOSE: Get the number of point clouds held in cache
	// RETURNS: number of point clouds (zero if not yet built, or just flushed)
	u32 GetNumPointClouds();

	// PURPOSE: Retrieve a point cloud
	// PARAMS: idx - "frame" index [0..numPointClounds-1]
	// RETURNS: Reference to point cloud (will assert on illegal index)
	const crpmPointCloud& GetPointCloud(u32 idx);

	// PURPOSE: Retrieve point cloud bound
	// RETURNS: Reference to bound containing the point clouds
	const crpmPointCloud::PointCloudBound& GetPointCloudBound() const;


private:
	const crClip* m_Clip;
	const crWeightSet* m_WeightSet;
	const crWeightSet* m_FilterWeightSet;
	bool m_FilterMover;

	u32 m_WindowSize;
	float m_SampleRate;

	crpmPointCloud::PointCloudBound m_Bound;
	atArray<crpmPointCloud> m_Cache;
};

// inline functions

////////////////////////////////////////////////////////////////////////////////

inline u32 crpmPointCloudCache::GetNumPointClouds()
{
	return (u32)m_Cache.size();
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmPointCloud& crpmPointCloudCache::GetPointCloud(u32 idx)
{
	return m_Cache[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmPointCloud::PointCloudBound& crpmPointCloudCache::GetPointCloudBound() const
{
	return m_Bound;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
#endif // CRPARAMETERIZEDMOTION_POINTCLOUDCACHE_H

