//
// crparameterizedmotion/parameterizedmotionplayer.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONPLAYER_H
#define CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONPLAYER_H

#include "parameter.h"
#include "parameterizedmotion.h"

#include "atl/functor.h"

namespace rage
{

class crFrame;
class crFrameCompositor;
class crTag;
class crpmParameterizedMotion;

////////////////////////////////////////////////////////////////////////////////

class crpmParameterizedMotionPlayer
{
public:

	// PURPOSE: Default constructor
	crpmParameterizedMotionPlayer();

	// PURPOSE: Destructor
	~crpmParameterizedMotionPlayer();

	// PURPOSE: Initialize
	// PARAMS: pm - pointer to parameterized motion instance (can be NULL)
	// NOTE: This call changes the parameterized motion being played,
	// and also initializes the parameter dimension/values and boundaries.
	// See SetParameterizedMotion() for a call that just switches the
	// parameterized motion without performing these other steps.
	void Init(crpmParameterizedMotion* pm);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Update parameterized motion
	// PARAM: deltaTime - elapsed time (in seconds)
	void Update(float deltaTime);
		
	// PURPOSE: Type definition of functor to call when update triggers a tag enter/exit
	// First parameter is the tag that was triggered,
	// Second parameter is true for enter/exit event, false for a tick,
	// Third parameter is true for an enter event, false for an exit event.
	typedef Functor3<const crTag&, bool, bool> TagTriggerFunctor;

	// PURPOSE: Update parameterized motion
	// PARAM:
	// deltaTime - elapsed time (in seconds)
	// parameterTime - elapsed time for parameter changes (in seconds)
	// outLoopedOrEnded - did the parameterized motion loop or end during update
	// tagTriggerFunctor - optional pointer to functor to call if delta causes triggering of any tag enter/exit
	void Update(float deltaTime, float parameterTime, bool& outLoopedOrEnded, const TagTriggerFunctor* tagTriggerFunctor=NULL);

	// PURPOSE: Composite parameterized motion, using frame compositor
	// PARAMS: inoutCompositor - frame compositor
	void Composite(crFrameCompositor& inoutCompositor) const;

	// PURPOSE: Composite parameterized motion into frame
	// PARAMS: outFrame - frame to composite
	void Composite(crFrame& outFrame) const;


	// PURPOSE: Get the current parameterized motion instance (non-const)
	// RETURNS: Pointer to parameterized motion instance (can be NULL)
	const crpmParameterizedMotion* GetParameterizedMotion() const;

	// PURPOSE: Get the current parameterized motion instance
	// RETURNS: Non-const pointer to parameterized motion instance (can be NULL)
	crpmParameterizedMotion* GetParameterizedMotion();

	// PURPOSE: Set the parameterized motion instance
	// PARAMS: pm - pointer to parameterized motion instance (can be NULL)
	// NOTE: This call changes the parameterized motion being played,
	// without initializing the parameter dimension/values or boundaries.
	// See Init() for a call which initializes these during a change.
	void SetParameterizedMotion(crpmParameterizedMotion* pm);


	// PURPOSE: Get playback rate
	// RETURNS: Current playback rate
	float GetRate() const;

	// PURPOSE: Set playback rate
	// PARAMS: rate - playback rate (must be >= 0.0, negative rates not possible)
	void SetRate(float rate);

protected:

	// PURPOSE: Internal function, triggers tags encountered during update
	void TriggerTags(const crTags& tags, float startU, float endU, const TagTriggerFunctor* tagTriggerFunctor) const;

private:

	crpmParameterizedMotion* m_Pm;
	float m_Rate;
};

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotionPlayer::Update(float deltaTime)
{
	bool junkLoopedOrEnded;
	Update(deltaTime, deltaTime, junkLoopedOrEnded);
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotionPlayer::Composite(crFrameCompositor& inoutCompositor) const
{
	if(m_Pm)
	{
		m_Pm->Composite(inoutCompositor);
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotionPlayer::Composite(crFrame& outFrame) const
{
	if(m_Pm)
	{
		m_Pm->Composite(outFrame);
	}
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterizedMotion* crpmParameterizedMotionPlayer::GetParameterizedMotion() const
{
	return m_Pm;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotion* crpmParameterizedMotionPlayer::GetParameterizedMotion()
{
	return m_Pm;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotionPlayer::SetParameterizedMotion(crpmParameterizedMotion* pm)
{
	m_Pm = pm;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmParameterizedMotionPlayer::GetRate() const
{
	return m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotionPlayer::SetRate(float rate)
{
	m_Rate = rate;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONPLAYER_H
