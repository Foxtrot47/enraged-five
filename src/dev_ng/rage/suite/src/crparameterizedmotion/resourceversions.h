//
// crparameterizedmotion/resourceversions.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_RESOURCEVERSIONS_H
#define CRPARAMETERIZEDMOTION_RESOURCEVERSIONS_H

#include "cranimation/resourceversions.h"
/*
PURPOSE
	Use this to determine when you need to regenerate any module resources.
	It is incremented whenever any internal changes happen to any structures
	that will invalidate any existing resources.
	Add this value to any private resource version counters in higher level code
	that includes any module structures.
NOTE
	The parameterized motion module is dependent on cr modules, the cr module
	resource version counter is already added into the module counter.
*/
const int crpmResourceBaseVersion = crResourceBaseVersion + 2;

#endif // CRPARAMETERIZEDMOTION_RESOURCEVERSIONS_H
