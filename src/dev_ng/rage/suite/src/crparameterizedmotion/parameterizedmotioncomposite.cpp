//
// crparameterizedmotion/parameterizedmotioncomposite.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "parameterizedmotioncomposite.h"

#if 0 // COMPOSITE PM'S REMOVED TEMPORARILY




#include "parametertranslator.h"

#include <sstream>

namespace rage
{


//------------------------------------------------------------------------------
crpmParameterizedMotionComposite::crpmParameterizedMotionComposite(
    const std::string& name,
    std::vector<crpmParameterizedMotion*>& childs,
    const crpmParameterTranslator& translator)
//------------------------------------------------------------------------------
    : mChilds(childs)
    , mTranslator(translator)
    , mOutParameter(mTranslator.getOutParameterDimension(childs))
    , mSelectedChild(-1)
    , mName(name)
{
    if (!checkAllHaveSameProperties())
    {
        Warningf("[RSV] Warning: some properties of composite MF %s differ!", getName().c_str());
    }
}

//------------------------------------------------------------------------------
bool crpmParameterizedMotionComposite::checkAllHaveSameProperties() const
//------------------------------------------------------------------------------
{
    bool loopFlagReference = mChilds[0]->isLooping();

    for (unsigned int childIndex = 1; childIndex < mChilds.size(); ++childIndex)
    {
        if (mChilds[childIndex]->isLooping() != loopFlagReference)
        {
            return false;
        }
    }

    return true;
}

//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::translateParameter(const crpmParameter& in)
//------------------------------------------------------------------------------
{
    int oldSelected = mSelectedChild;
    mTranslator.translate(in, mChilds, mOutParameter, mSelectedChild);

#ifdef DEBUG_OUTPUT
    std::stringstream paramStr;
    std::copy(mOutParameter.begin(), mOutParameter.end(), std::ostream_iterator<float>(paramStr, " "));

    std::stringstream ss;
    ss << "CompositePM(" << getName().c_str() << "): " << "child " << getCurrentChild()->getName().c_str() << "(" << mSelectedChild << ") ";
    if (oldSelected != -1)
    {
        ss << "- was " << mChilds[oldSelected]->getName().c_str() << "(" << oldSelected << ") ";
    }
    ss << "- parameters " << paramStr.str();

    Displayf(ss.str().c_str());
#endif

    if (oldSelected != mSelectedChild)
    {
        if (oldSelected != -1)
        {
            // cut transition to other child PM
//            Mat34V moverMatrix = /*projectToGround*/(mChilds[oldSelected]->getCurrentMoverFrame());
//            getCurrentChild()->continueFrom(moverMatrix);
        }

        //TODO: [CLEMENS] for now we completely reset the parameter interpolation
        // when changing between different sub-PMs
        getCurrentChild()->resetParameterInterpolation();
    }
}


//------------------------------------------------------------------------------
crpmParameterizedMotionComposite::~crpmParameterizedMotionComposite()
//------------------------------------------------------------------------------
{
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::continueFrom(Mat34V_In pos)
//------------------------------------------------------------------------------
{
    for (unsigned int childIndex = 0; childIndex < mChilds.size(); ++childIndex)
    {
        mChilds[childIndex]->continueFrom(pos);
    }
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::continueFromAdvanceTime(Mat34V_In pos, const float desiredTime)
//------------------------------------------------------------------------------
{
    for (unsigned int childIndex = 0; childIndex < mChilds.size(); ++childIndex)
    {
        mChilds[childIndex]->continueFromAdvanceTime(pos, desiredTime);
    }
}

/*
//------------------------------------------------------------------------------
Mat34V_Out crpmParameterizedMotionComposite::getCurrentRootFrame() const
//------------------------------------------------------------------------------
{
    return getCurrentChild()->getCurrentRootFrame();
}


//------------------------------------------------------------------------------
Mat34V_Out crpmParameterizedMotionComposite::getCurrentMoverFrame() const
//------------------------------------------------------------------------------
{
    return getCurrentChild()->getCurrentMoverFrame();
}
*/

//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::setParameter(const crpmParameter& parameter)
//------------------------------------------------------------------------------
{
    translateParameter(parameter);
    getCurrentChild()->setParameter(mOutParameter);
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::setParameterInterpolated(const crpmParameter& parameter, const float weight /*= 0.1f*/)
//------------------------------------------------------------------------------
{
    translateParameter(parameter);
    getCurrentChild()->setParameterInterpolated(mOutParameter, weight);
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::setTargetParameter(const crpmParameter& targetParameter, const float blendSeconds)
//------------------------------------------------------------------------------
{
    translateParameter(targetParameter);
    getCurrentChild()->setTargetParameter(mOutParameter, blendSeconds);
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::setParameterWithFilteredWeights(const crpmParameter& parameter, float filter)
//------------------------------------------------------------------------------
{
    translateParameter(parameter);
    getCurrentChild()->setParameterWithFilteredWeights(mOutParameter, filter);
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::update(const float animDeltaTime, const float parameterDeltaTime)
//------------------------------------------------------------------------------
{
    for (unsigned int childIndex = 0; childIndex < mChilds.size(); ++childIndex)
    {
        mChilds[childIndex]->update(animDeltaTime, parameterDeltaTime);
    }
}

//------------------------------------------------------------------------------
bool crpmParameterizedMotionComposite::isFinished()
//------------------------------------------------------------------------------
{
    return getCurrentChild()->isFinished();
}

//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::reset()
//------------------------------------------------------------------------------
{
    for (unsigned int childIndex = 0; childIndex < mChilds.size(); ++childIndex)
    {
        mChilds[childIndex]->reset();
    }
    mSelectedChild = -1;
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::setHandleLooping(bool isLooping)
//------------------------------------------------------------------------------
{
    for (unsigned int childIndex = 0; childIndex < mChilds.size(); ++childIndex)
    {
        mChilds[childIndex]->setHandleLooping(isLooping);
    }
}


//------------------------------------------------------------------------------
crpmParameterizedMotion* crpmParameterizedMotionComposite::getCurrentChild() const
//------------------------------------------------------------------------------
{
    Assert(mSelectedChild != -1);
    return mChilds[mSelectedChild];
}


void crpmParameterizedMotionComposite::Composite(crFrame& frame) const
{
	getCurrentChild()->Composite(frame);
}

/*
//------------------------------------------------------------------------------
const crFrame& crpmParameterizedMotionComposite::getAnimFrame() const
//------------------------------------------------------------------------------
{
    return getCurrentChild()->getAnimFrame();
}
*/

//------------------------------------------------------------------------------
const crpmWeights& crpmParameterizedMotionComposite::getCurrentWeights() const
//------------------------------------------------------------------------------
{
    return getCurrentChild()->getCurrentWeights();
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::setWeightsDirectly(const crpmWeights& weights)
//------------------------------------------------------------------------------
{
    getCurrentChild()->setWeightsDirectly(weights);
}


//------------------------------------------------------------------------------
int crpmParameterizedMotionComposite::getWeightDimension() const
//------------------------------------------------------------------------------
{
    return mTranslator.getWeightDimension(mChilds);
}


//------------------------------------------------------------------------------
int crpmParameterizedMotionComposite::getParameterDimension() const
//------------------------------------------------------------------------------
{
    return mTranslator.getInParameterDimension(mChilds);
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::getParameterBoundaries(crpmParameter& min, crpmParameter& max) const
//------------------------------------------------------------------------------
{
    mTranslator.getParameterBoundaries(mChilds, min, max);
}

//------------------------------------------------------------------------------
float crpmParameterizedMotionComposite::getRemainingPlayTime() const
//------------------------------------------------------------------------------
{
    return getCurrentChild()->getRemainingPlayTime();
}


//------------------------------------------------------------------------------
float crpmParameterizedMotionComposite::getMaxDuration() const
//------------------------------------------------------------------------------
{
    return getCurrentChild()->getMaxDuration();
}

/*
//------------------------------------------------------------------------------
bool crpmParameterizedMotionComposite::hasLooped() const
//------------------------------------------------------------------------------
{
    return getCurrentChild()->hasLooped();
}
*/

//------------------------------------------------------------------------------
const crpmRegistrationCurvePlayer& crpmParameterizedMotionComposite::getPlayback() const
//------------------------------------------------------------------------------
{
    return getCurrentChild()->getPlayback();
}


//------------------------------------------------------------------------------
const crClip* crpmParameterizedMotionComposite::getReferenceClip() const
//------------------------------------------------------------------------------
{
    return getCurrentChild()->getReferenceClip();
}


//------------------------------------------------------------------------------
bool crpmParameterizedMotionComposite::isLooping()
//------------------------------------------------------------------------------
{
    return getCurrentChild()->isLooping();
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::enableDirectParameterMapping(bool enable)
//------------------------------------------------------------------------------
{
    for (unsigned int childIndex = 0; childIndex < mChilds.size(); ++childIndex)
    {
        mChilds[childIndex]->enableDirectParameterMapping(enable);
    }
}


//------------------------------------------------------------------------------
void crpmParameterizedMotionComposite::resetParameterInterpolation()
//------------------------------------------------------------------------------
{
    for (unsigned int childIndex = 0; childIndex < mChilds.size(); ++childIndex)
    {
        mChilds[childIndex]->resetParameterInterpolation();
    }
}


//------------------------------------------------------------------------------
float crpmParameterizedMotionComposite::getCurrentU() const
//------------------------------------------------------------------------------
{
	return getCurrentChild()->getCurrentU();
}


} // namespace rage

#endif // 0 COMPOSITE PM'S REMOVED TEMPORARILY



