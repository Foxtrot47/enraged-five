//
// crparameterizedmotion/parametersamplerspatial.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "parametersamplerspatial.h"

#include "motioncontroller.h"
#include "parameter.h"
#include "registrationcurve.h"

#include "crclip/clip.h"
#include "crclip/clips.h"
#include "crmetadata/tags.h"
#include "crskeleton/skeletondata.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerSpatial::crpmParameterSamplerSpatial()
: crpmParameterSamplerPhysical(kSamplerTypeSpatial)
, m_BoneIdx(-1)
, m_MajorAxis(1)
, m_Minimal(true)
, m_TagKey(0)
, m_Axes("xyz")
, m_Tolerance(SMALL_FLOAT)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerSpatial::crpmParameterSamplerSpatial(u16 boneId, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
: crpmParameterSamplerPhysical(kSamplerTypeSpatial)
, m_BoneIdx(-1)
, m_MajorAxis(1)
, m_Minimal(true)
, m_TagKey(0)
, m_Axes("xyz")
, m_Tolerance(SMALL_FLOAT)
{
	Init(boneId, rcData, clips, skelData, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerSpatial::~crpmParameterSamplerSpatial()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRPM_IMPLEMENT_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerSpatial, crpmParameterSampler::kSamplerTypeSpatial);

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerSpatial::Init(u16 boneId, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	int boneIdx;
	if(skelData.ConvertBoneIdToIndex(boneId, boneIdx))
	{
		m_BoneIdx = boneIdx;
	}

	PreInit(rcData, clips, skelData, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerSpatial::Shutdown()
{

	crpmParameterSampler::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerSpatial::CalcParameterDerivedWithMotionController(crpmMotionController& mc, crpmParameter& outParameter) const
{
	Vec3V sample(V_ZERO);
	Vec3V discriminator(V_ZERO);
	int numSamples = 0;

	if(!m_TagKey && m_MajorAxis >= 0)
	{
		discriminator = Vec3V(V_FLT_MAX);
		if(!m_Minimal)
		{
			discriminator = -discriminator;
		}
	}

	do
	{
		mc.Advance(m_SampleRate);

		Mat34V mtx;
		if(m_BoneIdx >= 0)
		{
			mc.GetSkeleton().GetGlobalMtx(m_BoneIdx, mtx);
		}
		else
		{
			mtx = *mc.GetSkeleton().GetParentMtx();
		}

		Vec3V pos = mtx.GetCol3();

		if(m_TagKey)
		{
			const u32 numClips = m_Clips->GetNumClips();
			for(u32 i=0; i<numClips; ++i)
			{
				const crClip* clip = m_Clips->GetClip(i);
				float clipTime = m_RcData->MapUToTime(mc.GetU(), i, *m_Clips);

				if(clip && clip->GetTags())
				{
					bool contained = false;
					const crTags* tags = clip->GetTags();
					const int numTags = tags->GetNumTags();
					for(int t=0; t<numTags; ++t)
					{
						const crTag* tag = tags->GetTag(t);
						if(tag->GetKey() == m_TagKey && tag->Contains(clipTime))
						{
							contained = true;
							break;
						}
					}

					if(contained)
					{
						sample += pos;
						numSamples++;
						break;
					}
				}
			}
		}
		else if(m_MajorAxis >= 0)
		{
			if(m_Minimal)
			{
				if(discriminator[m_MajorAxis] > pos[m_MajorAxis])
				{
					if(!IsNearZero(discriminator[m_MajorAxis]-pos[m_MajorAxis], m_Tolerance))
					{
						sample = discriminator = pos;
						numSamples = 1;
					}
				}
			}
			else
			{
				if(discriminator[m_MajorAxis] < pos[m_MajorAxis])
				{
					if(!IsNearZero(discriminator[m_MajorAxis]-pos[m_MajorAxis], m_Tolerance))
					{
						sample = discriminator = pos;
						numSamples = 1;
					}
				}
			}
		}
	}
	while(!mc.IsFinished());

	if(numSamples > 0)
	{
		sample /= ScalarVFromF32(float(numSamples));
	}

	for(int i=0; i<outParameter.GetNumDimensions(); ++i)
	{
		switch(m_Parameterization.GetDimensionInfo(i).GetDimensionMeaning())
		{
		case crpmParameterization::DimensionInfo::kDimensionMeaningSpatialX:
			outParameter.SetValue(i, sample.GetXf());
			break;
		case crpmParameterization::DimensionInfo::kDimensionMeaningSpatialY:
			outParameter.SetValue(i, sample.GetYf());
			break;
		case crpmParameterization::DimensionInfo::kDimensionMeaningSpatialZ:
			outParameter.SetValue(i, sample.GetZf());
			break;
		default:
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerSpatial::DebugMapParameterDerived(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const
{
	v = Vec3V(V_ZERO);

	for(int i=0; i<parameter.GetNumDimensions(); ++i)
	{
		switch(m_Parameterization.GetDimensionInfo(i).GetDimensionMeaning())
		{
		case crpmParameterization::DimensionInfo::kDimensionMeaningSpatialX:
			v.SetXf(parameter.GetValue(i));
			break;
		case crpmParameterization::DimensionInfo::kDimensionMeaningSpatialY:
			v.SetYf(parameter.GetValue(i));
			break;
		case crpmParameterization::DimensionInfo::kDimensionMeaningSpatialZ:
			v.SetZf(parameter.GetValue(i));
			break;
		default:
			break;
		}
	}

	if(skel.GetParentMtx())
	{
		v = Transform(*skel.GetParentMtx(), v);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerSpatial::PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	crpmParameterSamplerPhysical::PreInit(rcData, clips, skelData, filterWeightSet, filterMover);

	InitParameterization();
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerSpatial::ConfigProperty(const char* token, ConfigParser& parser)
{
	if(!stricmp(token, "BoneName"))
	{
		atString boneName;
		if(parser.StripStringProperty(boneName))
		{
			const crBoneData* bd = parser.m_SkelData->FindBoneData(boneName);
			if(bd)
			{
				m_BoneIdx = u16(bd->GetIndex());
				return true;
			}
		}
		return false;
	}
	else if(!stricmp(token, "BoneId"))
	{
		int boneId;
		if(parser.StripIntProperty(boneId))
		{
			int boneIdx;
			if(parser.m_SkelData->ConvertBoneIdToIndex(u16(boneId), boneIdx))
			{
				m_BoneIdx = boneIdx;
				return true;
			}
		}
		return false;
	}
	else if(!stricmp(token, "BoneIdx"))
	{
		int boneIdx;
		if(parser.StripIntProperty(boneIdx))
		{
			m_BoneIdx = u16(boneIdx);
			return true;
		}
		return false;
	}
	else if(!stricmp(token, "MajorAxis"))
	{
		atString axis;
		if(parser.StripStringProperty(axis))
		{
			if(axis[0] >= 'x' && axis[0] <= 'z')
			{
				m_MajorAxis = axis[0]-'x';
				return true;
			}
			else if(axis[0] >= 'X' && axis[0] <= 'Z')
			{
				m_MajorAxis = axis[0]-'X';
				return true;
			}
		}
		return false;
	}
	else if(!stricmp(token, "Minimal"))
	{
		return parser.StripBoolProperty(m_Minimal);
	}
	else if(!stricmp(token, "Axes"))
	{
		if(parser.StripStringProperty(m_Axes))
		{
			InitParameterization();
			return true;
		}
		return false;
	}
	else if(!stricmp(token, "TagKey"))
	{
		int tagKey;
		if(parser.StripIntProperty(tagKey))
		{
			m_TagKey = crTag::Key(tagKey);
			return true;
		}
		return false;
	}
	else if(!stricmp(token, "Tolerance"))
	{
		float tolerance;
		if(parser.StripFloatProperty(tolerance))
		{
			m_Tolerance = tolerance;
			return true;
		}
		return false;
	}

	return crpmParameterSamplerPhysical::ConfigProperty(token, parser);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerSpatial::InitParameterization()
{
	int numAxes = m_Axes.GetLength();
	m_Parameterization.Init(numAxes);

	for(int i=0; i<numAxes; ++i)
	{
		crpmParameterization::DimensionInfo& dim = m_Parameterization.GetDimensionInfo(i);
		dim.SetDimensionType(crpmParameterization::DimensionInfo::kDimensionTypeScalar);

		if(m_Axes[i] == 'x' || m_Axes[i] == 'X')
		{
			dim.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningSpatialX);
		}
		else if(m_Axes[i] == 'y' || m_Axes[i] == 'Y')
		{
			dim.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningSpatialY);
		}
		else if(m_Axes[i] == 'z' || m_Axes[i] == 'Z')
		{
			dim.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningSpatialZ);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
