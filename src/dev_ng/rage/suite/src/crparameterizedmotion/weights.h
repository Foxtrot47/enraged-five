//
// crparameterizedmotion/weights.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_WEIGHTS_H
#define CRPARAMETERIZEDMOTION_WEIGHTS_H

#include "atl/array.h"
#include "data/safestruct.h"
#include "math/simplemath.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Class that encapsulates a set of weights, providing efficient sum and normalization
class crpmWeights
{
public:

	// PURPOSE: Default constructor
	crpmWeights();

	// PURPOSE: Resource constructor
	crpmWeights(datResource&);

	// PURPOSE: Pre-initializing constructor
//	crpmWeights(int worstNumWeights);

	// PURPOSE: Initializing constructor
	// PARAMS:
	// numWeights - number of weights to store
	// weight - initial value for all the weights (default 0.0)
	crpmWeights(int numWeights, float weight=0.f);

	// PURPOSE: Copy constructor
	crpmWeights(const crpmWeights& weights);

	// PURPOSE: Destructor
	~crpmWeights();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmWeights);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT


	// PURPOSE: Pre-initialize
	// NOTES: Reserves worst case storage needed for later initialization
	void PreInit(int worstNumWeights);

	// PURPOSE: Initializer
	// PARAMS:
	// numWeights - number of weights to store
	// weight - initial value for all the weights (default 0.0)
	void Init(int numWeights, float weight=0.f);

	// PURPOSE: Initializer from copy
	void Init(const crpmWeights& weights);

	// PURPOSE: Clear
	void Clear();

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Clear all the weights to an initial value
	// weight - value for all the weights (default 0.0)
	void ClearWeights(float weight=0.f);

	// PURPOSE: Get number of weights stored
	int GetNumWeights() const;

	// PURPOSE: Get weight by index
	float GetWeight(int idx) const;

	// PURPOSE: Set weight by index
	void SetWeight(int idx, float weight);

	// PURPOSE: Set weights from another (identically sized) collection of weights
	void SetWeights(const crpmWeights& weights);

	// PURPOSE: Set weights from array of floats
	// NOTES: Caution, relies on caller validating sufficient valid weights are provided
	void SetWeights(float* weights);

	// PURPOSE: Get sum of all the weights contained
	// NOTES: Efficient, not calculated by this call
	float GetWeightSum() const;

	// PURPOSE: Get largest weight contained
	float GetWeightMax() const;

	// PURPOSE: Get inverse of sum of all the weights contained
	// NOTES: Efficient, only calculated if needed on first call
	float GetInvWeightSum() const;

	// PURPOSE: Get normalized weight by index
	// NOTES: Efficient, simple multiply for all but first call
	float GetNormalizedWeight(int idx) const;

	// PURPOSE: Lerp between (identically sized) collections of weights
	void Lerp(float blend, const crpmWeights& weights0, const crpmWeights& weights1);

	// PURPOSE: Normalize weights
	void Normalize();

	// PURPOSE: Serialize
	void Serialize(datSerialize&);

private:

	atArray<float> m_Weights;
	float m_Sum;
	mutable float m_InvSum;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Class that packs a set of weights for efficient storage
class crpmWeightsPacked
{
public:

	// PURPOSE: Default constructor
	crpmWeightsPacked();

	// PURPOSE: Resource constructor
	crpmWeightsPacked(datResource&);

	// PURPOSE: Initializing constructor
	// PARAMS:
	// weights - unpacked weights
	crpmWeightsPacked(const crpmWeights& weights);

	// PURPOSE: Copy constructor
	crpmWeightsPacked(const crpmWeightsPacked& weights);

	// PURPOSE: Destructor
	~crpmWeightsPacked();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmWeightsPacked);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initializer
	// PARAMS:
	// weights - unpacked weights
	void Init(const crpmWeights& weights);

	// PURPOSE: Initializer from copy
	void Init(const crpmWeightsPacked& weights);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Pack weights
	void Pack(const crpmWeights& weights);

	// PURPOSE: Unpack weights
	void Unpack(crpmWeights& weights) const;

	// PURPOSE: Serialize
	void Serialize(datSerialize&);

private:

	atArray<u32> m_WeightsIndices;
};

////////////////////////////////////////////////////////////////////////////////

inline crpmWeights::crpmWeights()
: m_Sum(0.f)
, m_InvSum(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmWeights::crpmWeights(int numWeights, float weight)
: m_Sum(0.f)
, m_InvSum(0.f)
{
	Init(numWeights, weight);
}

////////////////////////////////////////////////////////////////////////////////

inline crpmWeights::crpmWeights(const crpmWeights& weights)
: m_Sum(0.f)
, m_InvSum(0.f)
{
	Init(weights);
}

////////////////////////////////////////////////////////////////////////////////

inline crpmWeights::~crpmWeights()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmWeights::Init(int numWeights, float weight)
{
	Assert(numWeights >= 0);

	m_Weights.Reset();
	m_Weights.Resize(numWeights);

	ClearWeights(weight);
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmWeights::Init(const crpmWeights& weights)
{
	Init(weights.m_Weights.GetCount());
	SetWeights(weights);
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmWeights::Shutdown()
{
	m_Weights.Reset();
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmWeights::ClearWeights(float weight)
{
	const int numWeights = m_Weights.GetCount();
	for(int i=0; i<numWeights; ++i)
	{
		m_Weights[i] = weight;
	}

	m_Sum = weight * float(numWeights);
	m_InvSum = 0.f;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmWeights::GetNumWeights() const
{
	return m_Weights.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmWeights::GetWeight(int idx) const
{
	return m_Weights[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmWeights::SetWeight(int idx, float weight)
{
	float& w = m_Weights[idx];

	m_Sum += weight - w;
	m_InvSum = 0.f;

	w = weight;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmWeights::SetWeights(const crpmWeights& weights)
{
	Assert(m_Weights.GetCount() == weights.m_Weights.GetCount());

	const int numWeights = m_Weights.GetCount();
	for(int i=0; i<numWeights; ++i)
	{
		m_Weights[i] = weights.m_Weights[i];
	}

	m_Sum = weights.m_Sum;
	m_InvSum = weights.m_InvSum;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmWeights::SetWeights(float* weights)
{
	ClearWeights(0.f);

	const int numWeights = m_Weights.GetCount();
	for(int i=0; i<numWeights; ++i)
	{
		SetWeight(i, weights[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmWeights::GetWeightSum() const
{
	return m_Sum;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmWeights::GetInvWeightSum() const
{
	if(m_InvSum == 0.f)
	{
		if(!IsNearZero(m_Sum))
		{
			m_InvSum = 1.f/m_Sum;
		}
	}

	return m_InvSum;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmWeights::GetNormalizedWeight(int idx) const
{
	float invSum = GetInvWeightSum();
	return m_Weights[idx] * invSum;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_WEIGHTS_H
