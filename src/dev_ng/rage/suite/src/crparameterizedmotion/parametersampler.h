//
// crparameterizedmotion/parametersampler.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERSAMPLER_H
#define CRPARAMETERIZEDMOTION_PARAMETERSAMPLER_H

#include "parameterization.h"

#include "atl/array.h"
#include "atl/string.h"
#include "vectormath/vec3v.h"

namespace rage
{

class crClips;
class crSkeleton;
class crSkeletonData;
class crWeightSet;
class crpmMotionController;
class crpmMotionControllerRc;
class crpmParameter;
class crpmRegistrationCurveData;
class crpmWeights;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base parameter sampler class
// Parameter samplers are used during the building of blend databases to
// sample the result of various weight combinations, and covert these into parameters.
class crpmParameterSampler
{
public:

	// PURPOSE: Enumeration of built in sampler types
	enum eSamplerTypes
	{
		kSamplerTypeNone,

		kSamplerTypeContainer,

		kSamplerTypeClipName,
		kSamplerTypeClipProperty,

		kSamplerTypeDirectional,
		kSamplerTypeSpatial,
		kSamplerTypeLocomotion,

		kSamplerTypeCombiner,

		// this must be last in enumeration of built in sampler types
		kSamplerTypeNum,

		// custom defined samplers must use enumerations >= kSamplerTypeCustom
		kSamplerTypeCustom = 0x80,
	};


	// PURPOSE: Default constructor
	crpmParameterSampler(eSamplerTypes samplerType=kSamplerTypeNone);

	// PURPOSE: Destructor
	virtual ~crpmParameterSampler();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Reset
	virtual void Reset() const;

	// PURPOSE: Factory, create sampler using sampler type
	static crpmParameterSampler* CreateSampler(eSamplerTypes samplerType);

	// PURPOSE: Factory, create sampler using sampler name
	static crpmParameterSampler* CreateSampler(const char* samplerName);

	// PURPOSE: Build sampler(s) based on configuration string
	static crpmParameterSampler* BuildSampler(const char* samplerConfig, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);


	// PURPOSE: Calculate parameter for given set of weights
	void CalcParameter(const crpmWeights& weights, crpmParameter& outParameter) const;

	// PURPOSE: Get parameterization object, describes parameter dimensions
	const crpmParameterization& GetParameterization() const;

	// PURPOSE: Get the sampler type
	eSamplerTypes GetSamplerType() const;

	// PURPOSE: Get number of weight dimensions
	int GetNumWeightDimensions() const;

	// PURPOSE: Get number of parameter dimensions
	int GetNumParameterDimensions() const;

	// PURPOSE: Optional debug mapping of parameter to world space for visualization
	bool DebugMapParameter(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const;


	// PURPOSE: Initialize parameter sampler registry
	static void InitClass();

	// PURPOSE: Shutdown the parameter sampler registry
	static void ShutdownClass();

protected:

	// PURPOSE: Calculate parameter for given set of weights (override and implement in derived class)
	virtual void CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const = 0;

	// PURPOSE: Optional debug mapping of parameter to world space for visualization (override and implement in derived class)
	virtual bool DebugMapParameterDerived(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const;

	// PURPOSE: Internal function, initialize base classes
	virtual void PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE:
	struct ConfigParser
	{
		// PURPOSE:
		ConfigParser(const char* configString, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

		// PURPOSE:
		void StripWhitespace();

		// PURPOSE:
		bool StripToken(char* token, int tokenMaxSize);

		// PURPOSE:
		bool MatchChar(char ch);

		// PURPOSE:
		bool StripFloatProperty(float& outFloat);

		// PURPOSE:
		bool StripStringProperty(atString& outString);

		// PURPOSE:
		bool StripIntProperty(int& outInt);

		// PURPOSE:
		bool StripBoolProperty(bool& outBool);

		// PURPOSE:
		bool StripFloatArrayProperty(int& inoutCount, float* inoutFloats);

		// PURPOSE:
		bool StripVector3Property(Vec3V_InOut inoutVector3);

		const char* m_ConfigString;

		const crpmRegistrationCurveData* m_RcData;
		const crClips* m_Clips;
		const crSkeletonData* m_SkelData;
		const crWeightSet* m_FilterWeightSet;
		bool m_FilterMover;
	};

	// PURPOSE: Internal function, configures a sampler
	bool ConfigSampler(ConfigParser& parser);

	// PURPOSE: Internal function, configures a sampler property
	// NOTES: Override to implement, but chain to base if token unknown
	virtual bool ConfigProperty(const char* token, ConfigParser& parser);


	// PURPOSE: Definition of sampler create call
	typedef crpmParameterSampler* SamplerCreateFn();

	// PURPOSE: Sampler type info, used to register sampler types
	class SamplerTypeInfo
	{
	public:

		// PURPOSE: Constructor
		// PARAMS: samplerType - sampler type identifier (see crpmParameterSampler::eSamplerTypes enumeration)
		// samplerName - sampler name string
		// createFn - sampler creation call
		// size - sizeof sampler
		SamplerTypeInfo(eSamplerTypes samplerType, const char* samplerName, SamplerCreateFn* createFn, size_t size);

		// PURPOSE: Destructor
		~SamplerTypeInfo();

		// PURPOSE: Registration call
		void Register() const;

		// PURPOSE: Get sampler type identifier
		// RETURNS: Sampler type identifier (see crpmParameterSampler::eSamplerTypes enumeration)
		eSamplerTypes GetSamplerType() const;

		// PURPOSE: Get sampler name string
		// RETURNS: String of sampler's type name
		const char* GetSamplerName() const;

		// PURPOSE: Get sampler creation function
		// RETURNS: Sampler creation function pointer
		SamplerCreateFn* GetCreateFn() const;

		// PURPOSE: Get sampler placement function
		// PURPOSE: Get sampler size (as returned by sizeof)
		// RETURNS: sizeof sampler
		size_t GetSize() const;

	private:
		eSamplerTypes m_SamplerType;
		const char* m_Name;
		SamplerCreateFn* m_CreateFn;
		size_t m_Size;
	};

	// PURPOSE: Get info about a sampler type
	// PARAMS: samplerType - sampler type identifier (see crpmParameterSampler::eSamplerTypes enumeration)
	// RETURNS: const pointer to sampler type info structure (may be NULL if type unknown)
	static const SamplerTypeInfo* FindSamplerTypeInfo(eSamplerTypes samplerType);

	// PURPOSE: Get info about a sampler type (by name)
	// PARAMS: samplerName - sampler class name (can be truncated, missing the crpmParamterSampler prefix)
	// RETURNS: const pointer to sampler type info structure (may be NULL if not found)
	static const crpmParameterSampler::SamplerTypeInfo* FindSamplerTypeInfo(const char* samplerName);

	// PURPOSE: Get info about this sampler
	// RETURNS: const reference to sampler type info structure about this sampler
	virtual const SamplerTypeInfo& GetSamplerTypeInfo() const = 0;


	// PURPOSE: Declare functions necessary to register an sampler type
	// Registers sampler in central sampler type registry
	#define CRPM_DECLARE_PARAMETER_SAMPLER_TYPE(__typename) \
		static crpmParameterSampler* AllocateSampler(); \
		static void InitClass(); \
		virtual crpmParameterSampler* Clone() const; \
		virtual const rage::crpmParameterSampler::SamplerTypeInfo& GetSamplerTypeInfo() const; \
		static const rage::crpmParameterSampler::SamplerTypeInfo sm_SamplerTypeInfo;

	// PURPOSE: Implement functions necessary to register an sampler type
	#define CRPM_IMPLEMENT_PARAMETER_SAMPLER_TYPE(__typename, __typeid) \
		rage::crpmParameterSampler* __typename::AllocateSampler() \
		{ \
			__typename* sampler = rage_new __typename; \
			return sampler; \
		} \
		void __typename::InitClass() \
		{ \
			sm_SamplerTypeInfo.Register(); \
		} \
		rage::crpmParameterSampler* __typename::Clone() const \
		{ \
			__typename* sampler = rage_new __typename(*this); \
			return sampler; \
		} \
		const rage::crpmParameterSampler::SamplerTypeInfo& __typename::GetSamplerTypeInfo() const \
		{ \
			return sm_SamplerTypeInfo; \
		} \
		const rage::crpmParameterSampler::SamplerTypeInfo __typename::sm_SamplerTypeInfo(rage::crpmParameterSampler::eSamplerTypes(__typeid), #__typename, AllocateSampler, sizeof(__typename));


private:

	// PURPOSE: Register a new sampler type (only call from SamplerTypeInfo::Register)
	// PARAMS: sampler type info (must be global/class static, persist for entire execution)
	static void RegisterSamplerTypeInfo(const SamplerTypeInfo& info);

	// PURPOSE: Sampler type info registry
	static atArray<const SamplerTypeInfo*> sm_SamplerTypeInfos;

	// PURPOSE: Class initialization
	static bool sm_InitClassCalled;

protected:

	eSamplerTypes m_SamplerType;

	crpmParameterization m_Parameterization;

	const crpmRegistrationCurveData* m_RcData;
	const crClips* m_Clips;
	const crSkeletonData* m_SkelData;
	const crWeightSet* m_FilterWeightSet;
	bool m_FilterMover;

	atArray<float> m_ParameterScale;
	atArray<float> m_ParameterOffset;

	friend class crpmParameterSamplerContainer;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract physical parameter sampler class
// Initializes and holds a motion controller to allow sampling of physical
// attributes from underlying clips/registration curves
class crpmParameterSamplerPhysical : public crpmParameterSampler
{
public:

	// PURPOSE: Constructor
	crpmParameterSamplerPhysical(eSamplerTypes samplerType=kSamplerTypeNone);

	// PURPOSE: Destructor
	virtual ~crpmParameterSamplerPhysical();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Reset
	virtual void Reset() const;

protected:

	// PURPOSE: Calculate parameter for given set of weights override
	virtual void CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const;

	// PURPOSE: Calculate parameter for given motion controller
	virtual void CalcParameterDerivedWithMotionController(crpmMotionController& mc, crpmParameter& outParameter) const = 0;

	// PURPOSE: Internal function, call from derived class to initialize physical sampler
	virtual void PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Internal function, override sampler property configuration
	virtual bool ConfigProperty(const char* token, ConfigParser& parser);


private:

	mutable crpmMotionControllerRc* m_MotionControllerRc;

protected:

	float m_SampleRate;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Aggregates multiple sub samplers together into a single sampler
class crpmParameterSamplerContainer : public crpmParameterSampler
{
public:

	// PURPOSE: Default constructor
	crpmParameterSamplerContainer(eSamplerTypes samplerType=kSamplerTypeContainer);

	// PURPOSE: Destructor
	virtual ~crpmParameterSamplerContainer();

	// PURPOSE:
	CRPM_DECLARE_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerContainer);

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Add additional sampler to the collection
	virtual void AppendSampler(crpmParameterSampler& sampler);

protected:

	// PURPOSE: Calculate parameter for given set of weights
	virtual void CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const;

	// PURPOSE: Debug mapping of parameter to world space for visualization
	virtual bool DebugMapParameterDerived(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const;

	// PURPOSE: Internal function, override sampler property configuration
	virtual bool ConfigProperty(const char* token, ConfigParser& parser);

	atArray<crpmParameterSampler*> m_Samplers;
	mutable atArray<crpmParameter> m_Parameters;
};

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterSampler::eSamplerTypes crpmParameterSampler::SamplerTypeInfo::GetSamplerType() const
{
	return m_SamplerType;
}

////////////////////////////////////////////////////////////////////////////////

inline const char* crpmParameterSampler::SamplerTypeInfo::GetSamplerName() const
{
	return m_Name;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterSampler::SamplerCreateFn* crpmParameterSampler::SamplerTypeInfo::GetCreateFn() const
{
	return m_CreateFn;
}

////////////////////////////////////////////////////////////////////////////////

inline size_t crpmParameterSampler::SamplerTypeInfo::GetSize() const
{
	return m_Size;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERSAMPLER_H
