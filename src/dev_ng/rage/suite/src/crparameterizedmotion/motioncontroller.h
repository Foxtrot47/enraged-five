//
// crparameterizedmotion/motioncontroller.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_MOTIONCONTROLLER_H
#define CRPARAMETERIZEDMOTION_MOTIONCONTROLLER_H

#include "registrationcurveplayer.h"

#include "cranimation/frame.h"
#include "crclip/clipplayer.h"
#include "crskeleton/skeleton.h"


namespace rage
{

class crFrameFilter;

////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Abstract base class that defines interface for wrapper which can advance
// though different kinds of "motions".
class crpmMotionController
{
public:

	// PURPOSE: Default constructor
	crpmMotionController();

	// PURPOSE: Destructor
	virtual ~crpmMotionController();

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Reset the current motion time to zero.
	virtual void Reset(Mat34V_In mtx=Mat34V(V_IDENTITY));

	// PURPOSE: Advance the current motion time by the amount specified.
	// PARAMS: deltaTime - the amount of time to advance by (in seconds)
	virtual void Advance(float deltaTime) = 0;

	// TODO --- isLooped?

	// PURPOSE: Force looping to be enabled or disabled
	// PARAMS: looped - force looping to be enabled or disabled
	virtual void SetLooped(bool looped) = 0;

	// PURPOSE: Has playback finished (only meaningful on non-looped motion)
	virtual bool IsFinished() const = 0;

	// PURPOSE: Get duration
	virtual float GetDuration() const = 0;

	// PURPOSE: Set phase
	virtual void SetU(float u) = 0;

	// PURPOSE: Get phase
	virtual float GetU() const = 0;

	// PURPOSE: Get the current composited frame
	const crFrame& GetFrame() const;

	// PURPOSE: Get the current posed skeleton
	const crSkeleton& GetSkeleton() const;

protected:

	// PURPOSE: Base initialization, call from derived class
	virtual void InitBase(const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Pose the skeleton and mover (using the updated frame)
	void Pose();


	Mat34V m_MoverMtx;
	crFrame m_Frame;
	crSkeleton m_Skeleton;
	const crWeightSet* m_FilterWeightSet;
	bool m_FilterMover;
	crFrameFilter* m_Filter;
};


////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Implements motion advancing wrapper for a clip
class crpmMotionControllerClip : public crpmMotionController
{
public:

	// PURPOSE: Default constructor
	crpmMotionControllerClip();

	// PURPOSE: Initializing constructor
	crpmMotionControllerClip(const crClip& clip, const crSkeletonData& skelData, const crWeightSet* filterWeightSet=NULL, bool filterMover=false);

	// PURPOSE: Destructor
	virtual ~crpmMotionControllerClip();

	// PURPOSE: Initializer
	void Init(const crClip& clip, const crSkeletonData& skelData, const crWeightSet* filterWeightSet=NULL, bool filterMover=false);

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Implement reset
	virtual void Reset(Mat34V_In mtx=Mat34V(V_IDENTITY));

	// PURPOSE: Implement advance
	virtual void Advance(float deltaTime);

	// PURPOSE: Implement looping
	virtual void SetLooped(bool looped);

	// PURPOSE: Implement is finished
	virtual bool IsFinished() const;

	// PURPOSE: Implement get duration
	virtual float GetDuration() const;

	// PURPOSE: Set phase
	virtual void SetU(float u);

	// PURPOSE: Get phase
	virtual float GetU() const;


	// PURPOSE: Implement set time
	void SetTime(float time);

	// PURPOSE: Implement get time
	float GetTime() const;


protected:

	// PURPOSE: Override base initialization
	virtual void InitBase(const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

private:
	crClipPlayer m_Player;
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Implements motion advancing wrapper for a registration curve player
class crpmMotionControllerRc : public crpmMotionController
{
public:

	// PURPOSE: Default constructor
	crpmMotionControllerRc();

	// PURPOSE: Initializing constructor
	crpmMotionControllerRc(const crpmRegistrationCurveData& rcData, const crClips& clips, const crpmWeights* weights, const crSkeletonData& skelData, const crWeightSet* filterWeightSet=NULL, bool filterMover=false);

	// PURPOSE: Destructor
	virtual ~crpmMotionControllerRc();

	// PURPOSE: Initializer
	void Init(const crpmRegistrationCurveData& rcData, const crClips& clips, const crpmWeights* weights, const crSkeletonData& skelData, const crWeightSet* filterWeightSet=NULL, bool filterMover=false);

	// PURPOSE: Shutdown
	virtual void Shutdown();
	
	// PURPOSE: Implement reset
	virtual void Reset(Mat34V_In mtx=Mat34V(V_IDENTITY));

	// PURPOSE: Implement advance
	virtual void Advance(float deltaTime);

	// PURPOSE: Implement looping
	virtual void SetLooped(bool looped);

	// PURPOSE: Implement is finished
	virtual bool IsFinished() const;

	// PURPOSE: Get duration (given current weights)
	virtual float GetDuration() const;

	// PURPOSE: Set phase
	virtual void SetU(float u);

	// PURPOSE: Get phase
	virtual float GetU() const;


	// PURPOSE: Set weights
	void SetWeights(const crpmWeights& weights);

	// PURPOSE: Get weights
	const crpmWeights& GetWeights() const;


	// PURPOSE: Get time remaining (given current weights)
	float GetRemainingTime() const;



private:
	crpmRegistrationCurvePlayer m_Player;
	crpmWeights m_Weights;
};

////////////////////////////////////////////////////////////////////////////////

inline const crFrame& crpmMotionController::GetFrame() const
{
	return m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

inline const crSkeleton& crpmMotionController::GetSkeleton() const
{
	return m_Skeleton;
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage
#endif // CRPARAMETERIZEDMOTION_MOTIONCONTROLLER_H



