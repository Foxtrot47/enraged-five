//
// crparameterizedmotion/blenddatabase.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "blenddatabase.h"

#include "parameterization.h"

#include "atl/array_struct.h"
#include "atl/string.h"
#include "crmetadata/dumpoutput.h"
#include "data/safestruct.h"
#include "system/memory.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

extern __THREAD int s_PmSimpleSerializeVersion;

////////////////////////////////////////////////////////////////////////////////

crpmBlendDatabase::crpmBlendDatabase()
: m_NumWeightDimensions(0)
, m_SortDimension(-1)
, m_AveMinDistance(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmBlendDatabase::crpmBlendDatabase(datResource& rsc)
: m_Parameterization(rsc)
, m_MinCorner(rsc)
, m_MaxCorner(rsc)
, m_MinCornerExtended(rsc)
, m_MaxCornerExtended(rsc)
, m_Extent(rsc)
, m_InvExtent(rsc)
, m_Entries(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmBlendDatabase::crpmBlendDatabase(const crpmParameterization& parameterization, int numWeightDimensions)
: m_NumWeightDimensions(0)
, m_SortDimension(-1)
, m_AveMinDistance(0.f)
{
	Init(parameterization, numWeightDimensions);
}

////////////////////////////////////////////////////////////////////////////////

crpmBlendDatabase::~crpmBlendDatabase()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmBlendDatabase);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmBlendDatabase::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmBlendDatabase)
	SSTRUCT_FIELD(crpmBlendDatabase, m_Parameterization)
	SSTRUCT_FIELD(crpmBlendDatabase, m_NumWeightDimensions)
	SSTRUCT_FIELD(crpmBlendDatabase, m_MinCorner)
	SSTRUCT_FIELD(crpmBlendDatabase, m_MaxCorner)
	SSTRUCT_FIELD(crpmBlendDatabase, m_MinCornerExtended)
	SSTRUCT_FIELD(crpmBlendDatabase, m_MaxCornerExtended)
	SSTRUCT_FIELD(crpmBlendDatabase, m_Extent)
	SSTRUCT_FIELD(crpmBlendDatabase, m_InvExtent)
	SSTRUCT_FIELD(crpmBlendDatabase, m_Entries)
	SSTRUCT_FIELD(crpmBlendDatabase, m_SortDimension)
	SSTRUCT_FIELD(crpmBlendDatabase, m_AveMinDistance)
	SSTRUCT_END(crpmBlendDatabase)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabase::Init(const crpmParameterization& parameterization, int numWeightDimensions)
{
	Assert(numWeightDimensions > 0);

	m_Parameterization = parameterization;
	m_NumWeightDimensions = numWeightDimensions;

	const int parameterDim = parameterization.GetNumDimensions();

	m_MinCorner.Init(parameterDim);
	m_MaxCorner.Init(parameterDim);

	m_MinCornerExtended.Init(parameterDim);
	m_MaxCornerExtended.Init(parameterDim);

	m_Extent.Init(parameterDim);
	m_InvExtent.Init(parameterDim);
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabase::Shutdown()
{
	m_Parameterization.Shutdown();

	const int numEntries = m_Entries.GetCount();
	for(int i=0; i<numEntries; ++i)
	{
		delete m_Entries[i];
		m_Entries[i] = NULL;
	}
	m_Entries.Reset();
}

////////////////////////////////////////////////////////////////////////////////

bool crpmBlendDatabase::IsParameterInsideExtendedBoundaries(const crpmParameter& parameter) const
{
	const int numParameterDims = GetNumParameterDimensions();
	for(int d=0; d<numParameterDims; ++d)
	{
		if(NotInRange(parameter[d], m_MinCornerExtended[d], m_MaxCornerExtended[d]))
		{
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

float crpmBlendDatabase::FindMinDistanceToParameter(const crpmParameter& parameter, crpmVariations variations) const
{
	Assert(GetNumParameterDimensions() == parameter.GetNumDimensions());

	float minDistSqr = FLT_MAX;
	const int numEntries = m_Entries.size();
	for(int i=0; i<numEntries; ++i)
	{
		const Entry& e = *m_Entries[i];
		if((e.GetVariations() & variations) == e.GetVariations())
		{
			float distSqr = CalcParameterDistanceSqr(parameter, e.GetParameter());

			minDistSqr = Min(distSqr, minDistSqr);
		}
	}

	return sqrt(minDistSqr);
}

////////////////////////////////////////////////////////////////////////////////

#define USE_SQR_FALLOFF (1)

bool crpmBlendDatabase::MapParameterToWeights(const crpmParameter& parameter, crpmVariations variations, crpmWeights& outWeights, int k) const
{
	Assert(m_NumWeightDimensions == outWeights.GetNumWeights());
	Assert(m_Entries.size() > 0);

	const int pd = GetNumParameterDimensions();

	if(k < 0)
	{
		const int kd = 3;
		k = kd;
		for(int i=1; i<pd; ++i)
		{
			k *= 3;
		}
	}
	Assert(k > 0);

	k = Min(GetNumEntries(), k);

	int n=0;
	float* resultWeights = Alloca(float, m_NumWeightDimensions);
	for(int i=0; i<m_NumWeightDimensions; ++i)
	{
		resultWeights[i] = 0.f;
	}

	const float d = float(pd);
	const float dInv = 1.f/d;

	const float desired = float(k)*powf(2.f, d);
	const float factor = powf(desired, dInv);

	const float threshold = m_AveMinDistance * factor * 0.5f;
	const float thresholdSqr = square(threshold);
#if USE_SQR_FALLOFF
	const float thresholdSqrInv = 1.f/threshold;
#else // USE_SQR_FALLOFF
	const float thresholdInv = 1.f/threshold;
#endif // USE_SQR_FALLOFF

	int first, last;
	CalcEntryRangeThresholdSqr(parameter, thresholdSqr, first, last);

	for(int i=first; i<=last; ++i)
	{
		const Entry& e = *m_Entries[i];
		if((e.GetVariations() & variations) == e.GetVariations())
		{
			float distSqr = CalcParameterDistanceSqr(parameter, e.GetParameter(), thresholdSqr);
			if(distSqr < thresholdSqr)
			{
				float dist = sqrt(distSqr);
#if USE_SQR_FALLOFF
				float iw = Max(square(threshold - dist), 0.f) * thresholdSqrInv;
//				float iw = Max(square(square(threshold - dist)), 0.f) * square(thresholdSqrInv);
#else // USE_SQR_FALLOFF
				float iw = Max(threshold - dist, 0.f) * thresholdInv;
#endif // USE_SQR_FALLOFF

				e.GetWeights(outWeights);

				for(int j=0; j<m_NumWeightDimensions; ++j)
				{
					resultWeights[j] += outWeights.GetWeight(j)*iw;
				}

				++n;
			}
		}
	}

	// if found k or more samples within simple threshold, then return
	if(n >= k)
	{
		outWeights.SetWeights(resultWeights);
		outWeights.Normalize();

		return true;
	}

	// otherwise, find the k nearest neighbors
	NearestResult* nearest = Alloca(NearestResult, k+1);
	if(FindKNearestToParameter(k, parameter, variations, nearest))
	{
		if(k > 1)
		{
			const float threshold = nearest[k-1].second;
#if USE_SQR_FALLOFF
			const float thresholdSqrInv = 1.f/square(threshold);
#else // USE_SQR_FALLOFF
			const float thresholdInv = 1.f/threshold;
#endif // USE_SQR_FALLOFF

			for(int i=0; i<m_NumWeightDimensions; ++i)
			{
				resultWeights[i] = 0.f;
			}

			for(int i=0; i<k; ++i)
			{
				const Entry& e = *nearest[i].first;
				float dist = nearest[i].second;
#if USE_SQR_FALLOFF
				float iw = Max(square(threshold - dist), 0.f) * thresholdSqrInv;
//				float iw = Max(square(square(threshold - dist)), 0.f) * square(thresholdSqrInv);
#else // USE_SQR_FALLOFF
				float iw = Max(threshold - dist, 0.f) * thresholdInv;
#endif // USE_SQR_FALLOFF

				e.GetWeights(outWeights);

				for(int j=0; j<m_NumWeightDimensions; ++j)
				{
					resultWeights[j] += outWeights.GetWeight(j)*iw;
				}
			}

			outWeights.SetWeights(resultWeights);
			outWeights.Normalize();
		}
		else
		{
			const Entry& e = *nearest[0].first;
			e.GetWeights(outWeights);
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crpmBlendDatabase::Serialize(datSerialize& s)
{
	s << m_Parameterization;

	s << m_NumWeightDimensions;

	s << m_MinCorner;
	s << m_MaxCorner;
	s << m_MinCornerExtended;
	s << m_MaxCornerExtended;
	s << m_Extent;
	s << m_InvExtent;

	int numEntries = m_Entries.GetCount();
	s << numEntries;
	if(s.IsRead())
	{
		m_Entries.Resize(numEntries);
	}

	for(int i=0; i<numEntries; ++i)
	{
		if(s.IsRead())
		{
			m_Entries[i] = rage_new Entry;
		}

		s << *m_Entries[i];
	}

	s << m_AveMinDistance;

	if(s_PmSimpleSerializeVersion >= 6)
	{
		s << m_SortDimension;
	}
}

////////////////////////////////////////////////////////////////////////////////

void DumpParameter(int verbosity, const char* name, const crpmParameter& parameter, crDumpOutput& output)
{
	atString text;
	parameter.Dump(text);
	output.Outputf(verbosity, name, "%s", text.c_str());
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabase::Dump(crDumpOutput& output) const
{
	DumpParameter(0, "mincorner", m_MinCorner, output);
	DumpParameter(0, "maxcorner", m_MaxCorner, output);
	DumpParameter(1, "mincornerext", m_MinCornerExtended, output);
	DumpParameter(1, "maxcornerext", m_MaxCornerExtended, output);
	DumpParameter(1, "extent", m_Extent, output);
	DumpParameter(1, "invextent", m_InvExtent, output);
	output.Outputf(1, "avemindist", "%f", m_AveMinDistance);

	m_Parameterization.Dump(output);

	const int numEntries = m_Entries.GetCount();
	output.Outputf(0, "numentries", "%d", numEntries);

	const int numWeights = GetNumWeightDimensions();
	crpmWeights weights(GetNumWeightDimensions(), 0.f);

	for(int i=0; i<numEntries; ++i)
	{
		const Entry& e = *m_Entries[i];
		
		DumpParameter(2, "entryparameter", e.GetParameter(), output);

		e.GetWeights(weights);
		for(int n=0; n<numWeights; ++n)
		{
			float w = weights.GetWeight(n);
			if(!IsNearZero(w))			
			{
				output.Outputf(2, "entryweight", i, n, "%f", w);
			}
		}
	}

	output.Outputf(2, "sortdimension", "%d", m_SortDimension);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crpmBlendDatabase::Entry::Entry(datResource& rsc)
: m_Parameter(rsc)
, m_Weights(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmBlendDatabase::Entry::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmBlendDatabase::Entry)
	SSTRUCT_FIELD(crpmBlendDatabase::Entry, m_Parameter)
	SSTRUCT_FIELD(crpmBlendDatabase::Entry, m_Weights)
	SSTRUCT_FIELD(crpmBlendDatabase::Entry, m_Variations)
	SSTRUCT_END(crpmBlendDatabase::Entry)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

float crpmBlendDatabase::CalcParameterDistanceSqr(const crpmParameter& parameter0, const crpmParameter& parameter1) const
{
	Assert(GetNumParameterDimensions() == parameter0.GetNumDimensions());
	Assert(GetNumParameterDimensions() == parameter1.GetNumDimensions());

	float dist = 0.f;

	const int numParameterDims = GetNumParameterDimensions();
	for(int i=0; i<numParameterDims; ++i)
	{
		float spaceScale = m_Parameterization.GetDimensionInfo(i).GetSpaceScale();
		dist += square((parameter1[i] - parameter0[i]) * spaceScale * m_InvExtent[i]);
	}

	return dist;
}

////////////////////////////////////////////////////////////////////////////////

float crpmBlendDatabase::CalcParameterDistanceSqr(const crpmParameter& parameter0, const crpmParameter& parameter1, float thresholdSqr) const
{
	Assert(GetNumParameterDimensions() == parameter0.GetNumDimensions());
	Assert(GetNumParameterDimensions() == parameter1.GetNumDimensions());

	float dist = 0.f;

	const int numParameterDims = GetNumParameterDimensions();
	for(int i=0; i<numParameterDims && dist<=thresholdSqr; ++i)
	{
		float spaceScale = m_Parameterization.GetDimensionInfo(i).GetSpaceScale();
		dist += square((parameter1[i] - parameter0[i]) * spaceScale * m_InvExtent[i]);
	}

	return dist;
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabase::CalcEntryRangeThresholdSqr(const crpmParameter& parameter, float thresholdSqr, int& first, int& last) const
{
	const int numEntries = m_Entries.GetCount();
	if(m_SortDimension >= 0)
	{
		const float val = parameter.GetValue(m_SortDimension);
		const float spaceScale = m_Parameterization.GetDimensionInfo(m_SortDimension).GetSpaceScale();
		const float invExtent = m_InvExtent[m_SortDimension];

		int low = 0;
		int high = numEntries-1;
		while(low <= high)
		{
			int mid = (low+high) >> 1;

			float vi = m_Entries[mid]->GetParameter().GetValue(m_SortDimension);
			if(vi < val && square((vi-val) * spaceScale * invExtent) > thresholdSqr)
			{
				low = mid+1;				
			}
			else
			{
				high = mid-1;
			}
		}

		first = Max(low-1, 0);

		low = first;
		high = numEntries-1;
		while(low <= high)
		{
			int mid = (low+high) >> 1;

			float vi = m_Entries[mid]->GetParameter().GetValue(m_SortDimension);
			if(vi > val && square((vi-val) * spaceScale * invExtent) > thresholdSqr)
			{
				high = mid-1;				
			}
			else
			{
				low = mid+1;
			}
		}

		last = Min(high+1, numEntries-1);
	}
	else
	{
		first = 0;
		last = numEntries-1;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmBlendDatabase::FindKNearestToParameter(int k, const crpmParameter& parameter, crpmVariations variations, NearestResult* nearest) const
{
	Assert(k <= GetNumEntries());

	const int numEntries = GetNumEntries();

	int i=0;
	for(i=0; i<numEntries; ++i)
	{
		const Entry& e = *m_Entries[i];
		if((e.GetVariations() & variations) == e.GetVariations())
		{
			nearest[0].first = &e;
			break;
		}
	}
	
	if(i == numEntries)
	{
		return false;
	}

	nearest[0].second = CalcParameterDistanceSqr(parameter, m_Entries[i]->GetParameter());

	int n = 1;
	for(; i<numEntries; ++i)
	{
		int finish = Min(n, k);
		
		const Entry& e = *m_Entries[i];
		if((e.GetVariations() & variations) == e.GetVariations())
		{
			++n;

			nearest[finish].first = &e;
			nearest[finish].second = CalcParameterDistanceSqr(parameter, e.GetParameter(), nearest[finish-1].second);

			for(int j=finish; j>0; --j)
			{
				if(nearest[j].second < nearest[j-1].second)
				{
					SwapEm(nearest[j], nearest[j-1]);
				}
				else
				{
					break;
				}
			}
		}
	}

	if(n < k)
	{
		return false;
	}

	for(int i=0; i<k; ++i)
	{
		nearest[i].second = sqrt(nearest[i].second);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crpmBlendDatabase::Entry::Serialize(datSerialize& s)
{
	s << m_Parameter;
	if(s_PmSimpleSerializeVersion < 2)
	{
		sysMemStartTemp();
		crpmWeights weights;
		s << weights;

		sysMemEndTemp();
		m_Weights.Init(weights);
		sysMemStartTemp();

		weights.Shutdown();
		sysMemEndTemp();
	}
	else
	{
		s << m_Weights;
	}

	if(s_PmSimpleSerializeVersion >= 7)
	{
		s << m_Variations;
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

