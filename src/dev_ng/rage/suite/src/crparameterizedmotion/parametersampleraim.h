//
// crparameterizedmotion/parametersampleraim.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERSAMPLERAIM_H
#define CRPARAMETERIZEDMOTION_PARAMETERSAMPLERAIM_H

#include "parametersamplerdirectional.h"

namespace rage
{

// DEPRECATED, use crpmParameterSamplerAim instead
typedef crpmParameterSamplerDirectional crpmParameterSamplerAim;

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERSAMPLERAIM_H
