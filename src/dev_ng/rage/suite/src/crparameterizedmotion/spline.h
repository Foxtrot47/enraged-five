//
// crparameterizedmotion/spline.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_SPLINE_H
#define CRPARAMETERIZEDMOTION_SPLINE_H

#include "serializationhelper.h"

#include "curve/curve.h"
#include "data/safestruct.h"
#include "vector/vectorn.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Wrapper for vector of points
// Provides continuity by interpolating between adjacent points
template<class _Vector> class crpmSeries
{
public:

	// PURPOSE: Constructor
	crpmSeries();

	// PURPOSE: Resource constructor
	crpmSeries(datResource& rsc);

	// PURPOSE: Initializing constructor
	// PARAMS:
	// numPoints - number of points in series
	// points - pointer to a vector of points (must contain numPoints elements)
	crpmSeries(int numPoints, _Vector* points);

	// PURPOSE: Destructor
	~crpmSeries();

	// PURPOSE: Placement
	IMPLEMENT_PLACE_INLINE(crpmSeries);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif // __DECLARESTRUCT

	// PURPOSE: Initializer
	// PARAMS:
	// numPoints - number of points in series
	// points - pointer to a vector of points (must contain numPoints elements)
	void Init(int numPoints, _Vector* points);

	// PURPOSE: Shutdown, free dynamic allocation
	void Shutdown();

	// PURPOSE: Get interpolated point, given phase
	// PARAMS: u - phase [0..1]
	// RETUENS: interpolated point value at that phase
	_Vector GetPosition(float u) const;

	// PURPOSE: Get dimension of interpolated point, given phase
	// PARAMS: u - phase [0..1]
	// dimension - index into point dimensions
	// RETURNS: interpolated point dimension value at that phase
	float GetPosition(float u, int dimension) const;

	// PURPOSE: Get phase, given value of particular dimension
	// PARMAS:
	// v - value
	// dimension - point dimension
	// RETURNS: phase at that value
	// NOTES: Works only with series values that are strictly increasing
	float GetInversePosition(float v, int dimension) const;

	// PURPOSE: Get number of points in series
	int GetNumPoints() const;

	// PURPOSE: Serialize
	void Serialize(datSerialize& s);

protected:

	atArray<_Vector> m_Points;
};

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline crpmSeries<_Vector>::crpmSeries()
{
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline crpmSeries<float>::crpmSeries(datResource& rsc)
: m_Points(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline crpmSeries<_Vector>::crpmSeries(datResource& rsc)
: m_Points(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline crpmSeries<_Vector>::crpmSeries(int numPoints, _Vector* points)
{
	Init(numPoints, points);
}

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline crpmSeries<_Vector>::~crpmSeries()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
template<class _Vector>
inline void crpmSeries<_Vector>::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmSeries)
	SSTRUCT_FIELD(crpmSeries, m_Points)
	SSTRUCT_END(crpmSeries)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline void crpmSeries<_Vector>::Init(int numPoints, _Vector* points)
{
	Assert(numPoints > 1);

	m_Points.Reset();
	m_Points.Reserve(numPoints);

	for(int i=0; i<numPoints; ++i)
	{
		m_Points.PushAndGrow(points[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline void crpmSeries<_Vector>::Shutdown()
{
	m_Points.Reset();
}

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline _Vector crpmSeries<_Vector>::GetPosition(float u) const
{
	Assert(InRange(u, 0.f, 1.f));

	float numSpaces = float(m_Points.size()-1);

	int point = Clamp(int(floor(u*numSpaces)), 0, int(m_Points.size()-2));
	float fraction = Clamp((u - float(point)/numSpaces) * numSpaces, 0.f, 1.f);

	return Lerp(ScalarVFromF32(fraction), m_Points[point], m_Points[point+1]);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline float crpmSeries<float>::GetPosition(float u) const
{
	Assert(InRange(u, 0.f, 1.f));

	float numSpaces = float(m_Points.size()-1);

	int point = Clamp(int(floor(u*numSpaces)), 0, int(m_Points.size()-2));
	float fraction = Clamp((u - float(point)/numSpaces) * numSpaces, 0.f, 1.f);

	return Lerp(fraction, m_Points[point], m_Points[point+1]);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline float crpmSeries<float>::GetPosition(float u, int) const
{
	Assert(InRange(u, 0.f, 1.f));

	float numSpaces = float(m_Points.size()-1);

	int point = Clamp(int(floor(u*numSpaces)), 0, int(m_Points.size()-2));
	float fraction = Clamp((u - float(point)/numSpaces) * numSpaces, 0.f, 1.f);

	return Lerp(fraction, m_Points[point], m_Points[point+1]);
}

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline float crpmSeries<_Vector>::GetPosition(float u, int dimension) const
{
	Assert(InRange(u, 0.f, 1.f));

	float numSpaces = float(m_Points.size()-1);

	int point = Clamp(int(floor(u*numSpaces)), 0, int(m_Points.size()-2));
	float fraction = Clamp((u - float(point)/numSpaces) * numSpaces, 0.f, 1.f);

	return Lerp(fraction, m_Points[point][dimension], m_Points[point+1][dimension]);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline float crpmSeries<float>::GetInversePosition(float v, int) const
{
	const int numSpaces = (int)m_Points.size()-1;

	if(v <= m_Points[0])
	{
		return 0.f;
	}
	else if(v >= m_Points[numSpaces])
	{
		return 1.f;
	}

	// TODO --- replace with binary search
	int i;
	for(i=1; i<numSpaces; ++i)
	{
		if(m_Points[i] > v)
		{
			break;
		}
	}

	Assert(m_Points[i-1] < m_Points[i]);
	float fraction = Clamp((v - m_Points[i-1]) / (m_Points[i] - m_Points[i-1]), 0.f, 1.f);

	return (float(i-1)+fraction)/float(numSpaces);
}

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline float crpmSeries<_Vector>::GetInversePosition(float v, int dimension) const
{
#if 1
	const int numSpaces = (int)m_Points.size()-1;

	if(v <= m_Points[0][dimension])
	{
		return 0.f;
	}
	else if(v >= m_Points[numSpaces][dimension])
	{
		return 1.f;
	}

	// TODO --- replace with binary search
	int i;
	for(i=1; i<numSpaces; ++i)
	{
		if(m_Points[i][dimension] > v)
		{
			break;
		}
	}

	Assert(m_Points[i-1][dimension] < m_Points[i][dimension]);
	float fraction = Clamp((v - m_Points[i-1][dimension]) / (m_Points[i][dimension] - m_Points[i-1][dimension]), 0.f, 1.f);

	return (float(i-1)+fraction)/float(numSpaces);
#else
	// TODO --- much quicker way of doing this now, binary search for points surrounding
	// then simple linear calculation to determine u

	const int numSamples = 13;  // 2^13 == 8192 (ie close to the 10K sample points)
	u32 b = 0;
	float uFraction = 0.5f;
	for(int i=0; i<numSamples; i++)
	{
		float frame = GetPosition(uFraction * float(b|1), dimension);
		if(frame < v)
		{
			b |= 1;
		}
		b <<= 1;
		uFraction *= 0.5f;
	}

	float frameLow  = GetPosition(uFraction * float(b), dimension);
	float frameHigh = GetPosition(uFraction * float(b|1), dimension);
	float u = (frameLow+frameHigh) * 0.5f;

	return u;
#endif
}

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline int crpmSeries<_Vector>::GetNumPoints() const
{
	return (int)m_Points.size();
}

////////////////////////////////////////////////////////////////////////////////

template<class _Vector>
inline void crpmSeries<_Vector>::Serialize(datSerialize& s)
{
	SerializeAtlArrayAsStlArray(s, m_Points);
}

////////////////////////////////////////////////////////////////////////////////

#if ENABLE_UNUSED_CURVE_CODE

// PURPOSE: Wrapper for spline interface, a vector of splines.
// Underneath parameterized motion currently uses NURBS from the curve module.
template<class _Vector> class crpmSpline : public cvCurveNurbs<_Vector>
{
public:
	typedef cvCurveNurbs<_Vector> _Base;

	// PURPOSE: Default constructor
	crpmSpline()
	{
	}

	// PURPOSE: Resource constructor
	crpmSpline(datResource& rsc) : cvCurveNurbs<_Vector>(rsc)
	{
	}

	// PURPOSE: Initializing constructor
	// PARAMS:
	crpmSpline(int numControlPoints, _Vector* controlPoints,
		int degree, bool ASSERT_ONLY(looping), bool ASSERT_ONLY(open))
	{
		FastAssert(!looping);
		FastAssert(open);
		if(degree==2)
		{
			FastAssert(numControlPoints==3);

			// Create spline with second vertex duplicated
			_Base::AllocateVertices(4);
			_Base::SetNumVertices(4);

			_Base::GetVertex(0) = controlPoints[0];
			_Base::GetVertex(1) = controlPoints[1];
			_Base::GetVertex(2) = controlPoints[1];
			_Base::GetVertex(3) = controlPoints[2];
			_Base::PostInit();

			// Solve for 0.33 and 0.66 to find spread out placement for the two middle vertices.
			_Vector v1, v2;
			_Base::SolveSegment(v1,0,1.0f/3.0f);
			_Base::SolveSegment(v2,0,2.0f/3.0f);

			// Create final spline.
			_Base::GetVertex(1) = v1;
			_Base::GetVertex(2) = v2;

			_Base::PostInit();
		}
		else
		{
			FastAssert(degree==3);
			_Base::AllocateVertices(numControlPoints);
			_Base::SetNumVertices(numControlPoints);
			for(int i=0; i<numControlPoints; i++)
			{
				_Vector & v = _Base::GetVertex(i);
				v = controlPoints[i];
			}
			_Base::PostInit();
		}
	}

	// PURPOSE: Get positions on all splines simultaneously
	_Vector GetPosition(float u)
	{
		_Vector vOut;
		_Base::SolveSegment(vOut,0,u);
		return vOut;
	}

	// PURPOSE: Get position on single spline
	float GetPosition(float u, int dimension)
	{
		float out;
		_Base::SolveSegmentForOneDimension(out, dimension, u);
		return out;
	}

	// PURPOSE: Get the number of control points
	int GetNumCtrlPoints() const
	{
		return _Base::GetNumVertices();
	}
};

#endif // ENABLE_UNUSED_CURVE_CODE


////////////////////////////////////////////////////////////////////////////////

inline void GetHalfKernel(int halfWidth, atArray<float>& result)
{
	float sum = 0.f;

	for(int i=0; i<halfWidth; i++)
	{
		// TODO - expose 4.5 as a parameter
		float filterExponent = -4.5f * float(i*i) / float(halfWidth*halfWidth);
		result[i] = expf(filterExponent);
		sum += result[i];
		if (i != 0)
		{
			sum += result[i];
		}
	}

	// normalize kernel
	for(int i=0; i<halfWidth; i++)
	{
		result[i] /= sum;
	}
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
inline const T GetTwoValuesFromArray(const atArray<T>& array, int index, int k)
{
	int lowerArrayIndex = (index - k);
	int upperArrayIndex = (index + k);

	// clamp at the boundaries
	if(lowerArrayIndex < 0)
	{
		lowerArrayIndex = 0;
	}
	if(upperArrayIndex > int(array.size()-1))
	{
		upperArrayIndex = int(array.size()-1);
	}

	return array[lowerArrayIndex] + array[upperArrayIndex];
}

////////////////////////////////////////////////////////////////////////////////

template <typename T>
void SnapFilteredToOriginalSignal(atArray<T>& values, atArray<T>& filtered)
{
	// NOTE - ramp in the difference, so that original samples and filtered samples at the start/end are the same

	T offsetStart = filtered[0] - values[0];
	T offsetEnd = values.Top() - filtered.Top();

	T diff = offsetEnd + offsetStart;
	diff /= float(filtered.GetCount()-1);

	// offset each sample by offsetStart and ramp in diff
	for (int i=0; i<filtered.GetCount(); ++i)
	{
		filtered[i] -= offsetStart;
		filtered[i] += diff * float(i);
	}

	// to make sure we snap the last sample
	// filtered.back() = values.back();
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
void ApplyKernelFilter(atArray<T>& values, bool snapToOriginalSignal=true)
{
#define USE_FILTERING
#ifdef USE_FILTERING
	atArray<T> filtered(values.size());

	const int half_kernel_size = 3;
	atArray<float> half_kernel(half_kernel_size);
	GetHalfKernel(half_kernel_size, half_kernel);

	for(unsigned int i=0; i<values.GetCount(); ++i)
	{
		T value = values[i] * half_kernel[0];

		for(int k=1; k<half_kernel_size; ++k)
		{
			value += GetTwoValuesFromArray(values, i, k) * half_kernel[k];
		}

		filtered[i] = value;
	}

	if(snapToOriginalSignal)
	{
		SnapFilteredToOriginalSignal(values, filtered);
	}

	values = filtered;
#else  // USE_FILTERING
	values;
#endif  // USE_FILTERING
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_SPLINE_H
