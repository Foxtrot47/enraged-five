//
// crparameterizedmotion/parameterinterpolator.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERINTERPOLATOR_H
#define CRPARAMETERIZEDMOTION_PARAMETERINTERPOLATOR_H

#include "parameter.h"


namespace rage
{

class crpmParameterization;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Manages interpolation towards an end parameter in a given duration of time.
class crpmParameterInterpolator
{
public:

	// PURPOSE: Default constructor
	crpmParameterInterpolator();

	// PURPOSE: Pre-initializing constructor
	crpmParameterInterpolator(int worstNumParameterDimensions);

	// PURPOSE: Initializing constructor
	// PARAMS:
	// parameter - starting parameter
	// parameterization - optional parameterization, improves interpolation if provided
	crpmParameterInterpolator(const crpmParameter& parameter, const crpmParameterization* parameterization);

	// PURPOSE: Destructor
	~crpmParameterInterpolator();

	// PURPOSE: Pre-initialize
	// NOTES: Reserves worst case storage needed for later initialization
	void PreInit(int worstNumParameterDimensions);

	// PURPOSE: Initializer
	// PARAMS:
	// parameter - starting parameter
	// parameterization - optional parameterization, improves interpolation if provided
	void Init(const crpmParameter& parameter, const crpmParameterization* parameterization);

	// PURPOSE: Clear
	void Clear();

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Reset interpolation
	// PARAMS: parameterization - new starting parameter
	void Reset(const crpmParameter& parameter);

	// PURPOSE: Update interpolation
	// PARAMS: deltaTime - time elapsed since last update in seconds (must be >= 0.0)
    void Update(float deltaTime);

	// PURPOSE: Get current parameter
	// RETURNS: current result of interpolation (or target if interpolation finished)
	const crpmParameter& GetCurrent() const;

	// PURPOSE: Get target parameter
	// RETURNS: interpolation target
	const crpmParameter& GetTarget() const;

	// PURPOSE: Set new end target parameter
	// PARAMS:
	// target - new end target parameter
	void SetTarget(const crpmParameter& target);

	// PURPOSE: Is interpolation finished (ie current == target)
	bool IsFinished() const;

	// PURPOSE: Get parameter rates
	// RETURNS: current maximum rate of change per second for each parameter
	const crpmParameter& GetRate() const;

	// PURPOSE: Set parameter rates
	// PARAMS:
	// rate - new maximum rate of change per second for each parameter
	void SetRate(const crpmParameter& rate);
	

private:
	const crpmParameterization* m_Parameterization;

    crpmParameter m_Current;
    crpmParameter m_Target;
	
	crpmParameter m_Rate;

	bool m_IsFinished;
};

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameterInterpolator::GetCurrent() const
{
	return m_Current;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameterInterpolator::GetTarget() const
{
	return m_Target;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmParameterInterpolator::IsFinished() const
{
	return m_IsFinished;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameterInterpolator::GetRate() const
{
	return m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterInterpolator::SetRate(const crpmParameter& rate)
{
	m_Rate.Set(rate);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERINTERPOLATOR_H

