//
// crparameterizedmotion/pointcloudtranform.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_POINTCLOUDTRANSFORM_H
#define CRPARAMETERIZEDMOTION_POINTCLOUDTRANSFORM_H

#include "math/simplemath.h"
#include "vectormath/classes.h"

#include "angle.h"


namespace rage
{

// forward declarations
class crpmPointCloud;

// PURPOSE: Calculates and represents the transformation between point clouds
class crpmPointCloudTransform
{
public:

	// PURPOSE: Default constructor
    crpmPointCloudTransform();

	// PURPOSE: Resource constructor
	crpmPointCloudTransform(datResource&);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initializing constructor
	// PARAMS: angle - transform angle (about the up axis)
	// u,v - translation in horizontal plane
    crpmPointCloudTransform(float angle, float u, float v);

	// PURPOSE: Initializing constructor
	// PARAMS: angle - transform angle (about the up axis)
	// v - translation in three-dimensions
	crpmPointCloudTransform(float angle, Vec3V_In v);

	// PURPOSE: Initializing constructor
	// PARAMS: matrix - transformation in matrix form
    crpmPointCloudTransform(Mat34V_In matrix);


	// PURPOSE: Get transform angle (about the up axis)
	// RETURNS: current transformation angle (about up axis) in radians
	float GetAngle() const;

	// PURPOSE: Get translation
	// RETURNS: current translation
	Vec3V_Out GetTranslation() const;


	// PURPOSE: Set transformation from matrix
	// PARAMS: matrix - new transformation in matrix form
	void FromMatrix(Mat34V_In matrix);

	// PURPOSE: Get current transformation in matrix form
	// RETURNS: matrix of current transformation
    Mat34V_Out ToMatrix() const;


	// PURPOSE: Set transformation from vector
	// PARAMS: vector - new transformation packed into vector3 form
	void FromVector3(Vec3V_In vector);

	// PURPOSE: Get current transformation as vector
	// RETURNS: packed vector of current transform
	Vec3V_Out ToVector3() const;


	// PURPOSE: Equality operator
	// PARAMS: transform - other transform to compare against
    bool operator==(const crpmPointCloudTransform& transform) const;

	// PURPOSE: Is the transform identity?
	// RETURNS: true - if transform is identity
	// NOTES: Doesn't test vertical component of translation!
	bool IsIdentity() const;


	// PURPOSE: Calculate a point cloud transform from two point clouds
	// PARAMS: p1, p2 - point clouds
    void Calculate(const crpmPointCloud& p1, const crpmPointCloud& p2);


	// PURPOSE: Identity transform
	static const crpmPointCloudTransform IDENTITY;

protected:
    Vec3V m_Translation;
	crpmAngleRadians m_Angle;
};

// inline functions

////////////////////////////////////////////////////////////////////////////////

inline crpmPointCloudTransform::crpmPointCloudTransform()
: m_Translation(V_ZERO)
, m_Angle(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmPointCloudTransform::crpmPointCloudTransform(float angle, Vec3V_In v)
: m_Translation(v)
, m_Angle(angle)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmPointCloudTransform::crpmPointCloudTransform(Mat34V_In matrix)
{
	FromMatrix(matrix);
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmPointCloudTransform::GetAngle() const
{
	return m_Angle;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec3V_Out crpmPointCloudTransform::GetTranslation() const
{
	return m_Translation;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmPointCloudTransform::operator==(const crpmPointCloudTransform& transform) const
{
	return IsEqualAll(m_Translation, transform.m_Translation)!=0 && IsClose(m_Angle, transform.m_Angle);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmPointCloudTransform::IsIdentity() const
{
	return m_Angle == 0.f && IsZeroAll(m_Translation);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
#endif // CRPARAMETERIZEDMOTION_POINTCLOUDTRANSFORM_H
