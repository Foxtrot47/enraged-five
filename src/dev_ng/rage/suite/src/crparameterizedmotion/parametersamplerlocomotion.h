//
// crparameterizedmotion/parametersamplerlocomotion.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERSAMPLERLOCOMOTION_H
#define CRPARAMETERIZEDMOTION_PARAMETERSAMPLERLOCOMOTION_H

#include "parametersampler.h"
#include "vectormath/vec3v.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Locomotion parameter sampler class
// Samples locomotion properties; turn rate, speed, etc
class crpmParameterSamplerLocomotion : public crpmParameterSamplerPhysical
{
public:

	// PURPOSE:
	crpmParameterSamplerLocomotion();

	// PURPOSE:
	crpmParameterSamplerLocomotion(bool sampleSpeed, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE:
	virtual ~crpmParameterSamplerLocomotion();

	// PURPOSE:
	CRPM_DECLARE_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerLocomotion);

	// PURPOSE:
	void Init(bool sampleSpeed, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE:
	virtual void Shutdown();

protected:

	// PURPOSE:
	virtual void CalcParameterDerivedWithMotionController(crpmMotionController& mc, crpmParameter& outParameter) const;

	// PURPOSE: Debug mapping of parameter to world space for visualization
	virtual bool DebugMapParameterDerived(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const;

	// PURPOSE: Internal function, initialize base classes
	virtual void PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Internal function, override sampler property configuration
	virtual bool ConfigProperty(const char* token, ConfigParser& parser);

	// PURPOSE: Internal funciton, initialize parameterization
	void InitParameterization();

protected:

	bool m_SampleSpeed;
	bool m_SampleDirection;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERSAMPLERLOCOMOTION_H
