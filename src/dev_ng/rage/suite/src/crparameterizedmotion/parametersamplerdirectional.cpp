//
// crparameterizedmotion/parametersamplerdirectional.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "parametersamplerdirectional.h"

#include "motioncontroller.h"
#include "parameter.h"

#include "crskeleton/skeletondata.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerDirectional::crpmParameterSamplerDirectional()
: crpmParameterSamplerPhysical(kSamplerTypeDirectional)
, m_AimBoneIdx(-1)
, m_BaseBoneIdx(-1)
, m_AimBoneOffset(V_Y_AXIS_WZERO)
, m_Wrap(180.f*DtoR)
, m_Negate(false)
, m_Eulers("yp")
, m_Phase(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerDirectional::crpmParameterSamplerDirectional(u16 aimBoneId, u16 baseBoneId, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
: crpmParameterSamplerPhysical(kSamplerTypeDirectional)
, m_AimBoneIdx(-1)
, m_BaseBoneIdx(-1)
, m_AimBoneOffset(V_Y_AXIS_WZERO)
, m_Wrap(180.f*DtoR)
, m_Negate(false)
, m_Eulers("yp")
{
	Init(aimBoneId, baseBoneId, rcData, clips, skelData, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerDirectional::~crpmParameterSamplerDirectional()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRPM_IMPLEMENT_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerDirectional, crpmParameterSampler::kSamplerTypeDirectional);

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerDirectional::Init(u16 aimBoneId, u16 baseBoneId, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	PreInit(rcData, clips, skelData, filterWeightSet, filterMover);

	int aimBoneIdx, baseBoneIdx;
	if(skelData.ConvertBoneIdToIndex(aimBoneId, aimBoneIdx))
	{
		m_AimBoneIdx = aimBoneIdx;
	}
	if(skelData.ConvertBoneIdToIndex(baseBoneId, baseBoneIdx))
	{
		m_BaseBoneIdx = baseBoneIdx;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerDirectional::Shutdown()
{

	crpmParameterSampler::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerDirectional::CalcParameterDerivedWithMotionController(crpmMotionController& mc, crpmParameter& outParameter) const
{
	if(m_Phase > 0.f)
	{
		mc.SetU(m_Phase);
	}

	// TEMP FIX - advance after phase set (won't get exact phase requested + really bad on loops) but without it pose isn't updated by sampler yet.
	mc.Advance(1.f/120.f);  // TODO --- unnecessary?

	Assert(m_AimBoneIdx >= 0);
	Mat34V aimMtx;
	mc.GetSkeleton().GetGlobalMtx(m_AimBoneIdx, aimMtx);

	Vec3V dir;
	if(m_BaseBoneIdx >= 0)
	{
		Mat34V baseMtx;
		mc.GetSkeleton().GetGlobalMtx(m_BaseBoneIdx, baseMtx);

		dir = Normalize(aimMtx.GetCol3() - baseMtx.GetCol3());
	}
	else
	{
		dir = Transform3x3(aimMtx, m_AimBoneOffset);
	}

	float incline = Angle(dir, GetUpAxis()).Getf();
		
	dir = Cross(dir, GetUpAxis());
	dir = Cross(dir, GetUpAxis());

	Vec3V axis = IsYUpAxis()? Vec3V(V_X_AXIS_WONE) : Vec3V(V_Y_AXIS_WONE);
	if(m_Negate)
	{
		axis = -axis;
	}

	float yaw = Angle(dir, axis).Getf()*((dir[IsYUpAxis()?2:0]>0.f)?-1.f:1.f)*(m_Negate?-1.f:1.f);

	if(m_Wrap>0.f)
	{
		if(yaw>m_Wrap)
		{
			yaw-=360.f*DtoR;
		}
	}
	else
	{
		if(yaw<m_Wrap)
		{
			yaw+=360.f*DtoR;
		}
	}

	incline -= 90.f*DtoR;

	for(int i=0; i<outParameter.GetNumDimensions(); ++i)
	{
		switch(m_Parameterization.GetDimensionInfo(i).GetDimensionMeaning())
		{
		case crpmParameterization::DimensionInfo::kDimensionMeaningPitch:
			outParameter.SetValue(i, incline);
			break;
		case crpmParameterization::DimensionInfo::kDimensionMeaningYaw:
			outParameter.SetValue(i, yaw);
			break;
		case crpmParameterization::DimensionInfo::kDimensionMeaningRoll:
			Assert(0);  // TODO - not supported yet
			outParameter.SetValue(i, 0.f);
			break;
		default:
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerDirectional::DebugMapParameterDerived(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const
{
	Vec3V e(V_ZERO);

	for(int i=0; i<parameter.GetNumDimensions(); ++i)
	{
		switch(m_Parameterization.GetDimensionInfo(i).GetDimensionMeaning())
		{
		case crpmParameterization::DimensionInfo::kDimensionMeaningPitch:
			e.SetXf(parameter.GetValue(i));
			break;
		case crpmParameterization::DimensionInfo::kDimensionMeaningYaw:
			e.SetZf(parameter.GetValue(i));
			break;
		case crpmParameterization::DimensionInfo::kDimensionMeaningRoll:
			// TODO --- something with roll
			break;
		default:
			break;
		}
	}


	Mat33V rotX, rotZ;
	Mat33VFromXAxisAngle(rotX, e.GetX());
	Mat33VFromZAxisAngle(rotZ, ScalarVFromF32(e.GetZf() + (m_Negate?(DtoR*180.f):0.f)));

	Mat33V rot;
	Multiply(rot, rotZ, rotX);

	Mat34V globalMtx;
	skel.GetGlobalMtx((m_BaseBoneIdx>=0)?m_BaseBoneIdx:m_AimBoneIdx, globalMtx);

	Mat34V mtx(rot, globalMtx.GetCol3());
	v = Transform(mtx, -Vec3V(V_Y_AXIS_WONE));

	if(skel.GetParentMtx())
	{
		v = Transform3x3(*skel.GetParentMtx(), v);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerDirectional::PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	crpmParameterSamplerPhysical::PreInit(rcData, clips, skelData, filterWeightSet, filterMover);

	InitParameterization();
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerDirectional::ConfigProperty(const char* token, ConfigParser& parser)
{
	if(!stricmp(token, "BaseBoneName") || !stricmp(token, "AimBoneName"))
	{
		atString boneName;
		if(parser.StripStringProperty(boneName))
		{
			const crBoneData* bd = parser.m_SkelData->FindBoneData(boneName);
			if(bd)
			{
				if(!stricmp(token, "BaseBoneName"))
				{
					m_BaseBoneIdx = u16(bd->GetIndex());
				}
				else
				{
					m_AimBoneIdx = u16(bd->GetIndex());
				}
				return true;
			}
		}
		return false;
	}
	else if(!stricmp(token, "BaseBoneId") || !stricmp(token, "AimBoneId"))
	{
		int boneId;
		if(parser.StripIntProperty(boneId))
		{
			int boneIdx;
			if(parser.m_SkelData->ConvertBoneIdToIndex(u16(boneId), boneIdx))
			{
				if(!stricmp(token, "BaseBoneId"))
				{
					m_BaseBoneIdx = boneIdx;
				}
				else
				{
					m_AimBoneIdx = boneIdx;
				}
				return true;
			}
		}
		return false;
	}
	else if(!stricmp(token, "BaseBoneIdx") || !stricmp(token, "AimBoneIdx"))
	{
		int boneIdx;
		if(parser.StripIntProperty(boneIdx))
		{
			if(!stricmp(token, "BaseBoneIdx"))
			{
				m_BaseBoneIdx = u16(boneIdx);
			}
			else
			{
				m_AimBoneIdx = u16(boneIdx);
			}
			return true;
		}
		return false;
	}
	else if(!stricmp(token, "Wrap"))
	{
		if(parser.StripFloatProperty(m_Wrap))
		{
			m_Wrap *= DtoR;
			return true;
		}
		return false;
	}
	else if(!stricmp(token, "Negate"))
	{
		return parser.StripBoolProperty(m_Negate);
	}
	else if(!stricmp(token, "Eulers"))
	{
		if(parser.StripStringProperty(m_Eulers))
		{
			InitParameterization();
			return true;
		}
		return false;
	}
	else if(!stricmp(token, "AimBoneOffset"))
	{
		if(parser.StripVector3Property(m_AimBoneOffset))
		{
			return true;
		}
		return false;
	}
	else if(!stricmp(token, "Phase"))
	{
		if(parser.StripFloatProperty(m_Phase))
		{
			return true;
		}
		return false;
	}

	return crpmParameterSamplerPhysical::ConfigProperty(token, parser);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerDirectional::InitParameterization()
{
	int numEulers = m_Eulers.GetLength();
	m_Parameterization.Init(numEulers);

	for(int i=0; i<numEulers; ++i)
	{
		crpmParameterization::DimensionInfo& dim = m_Parameterization.GetDimensionInfo(i);
		dim.SetDimensionType(crpmParameterization::DimensionInfo::kDimensionTypeRadiansMinusPiToPi);

		if(m_Eulers[i] == 'p' || m_Eulers[i] == 'P')
		{
			dim.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningPitch);
		}
		else if(m_Eulers[i] == 'y' || m_Eulers[i] == 'Y')
		{
			dim.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningYaw);
		}
		else if(m_Eulers[i] == 'r' || m_Eulers[i] == 'R')
		{
			dim.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningRoll);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage
