//
// crparameterizedmotion/blenddatabasecalculator.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_BLENDDATABASECALCULATOR_H
#define CRPARAMETERIZEDMOTION_BLENDDATABASECALCULATOR_H

#include "blenddatabase.h"

#include "atl/string.h"
#include "math/random.h"

#include <map>
#include <vector>

namespace rage
{

class crClips;
class crpmBlendDatabase;
class crpmParameterSampler;
class crpmRegistrationCurveData;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Populates a blend database with entries
class crpmBlendDatabaseCalculator
{
public:

	// PURPOSE: Default constructor
	crpmBlendDatabaseCalculator();

	// PURPOSE: Initializing constructor
	// PARAMS:
	// maxNumEntries - maximum number of entries to insert into the blend database
	// distanceThreshold - minimum (parameter space) distance threshold between entries
	// sourceDistanceThreshold - minimum (parameter space) distance threshold between source entries
	// parameterExtension - percentage to bias minimum and maximum parameter space boundaries
	// stochasticPopulate - use random method to populate database, instead of barycentric search (default true)
	// debugPath - folder to output debug images into (default NULL, no debug images generated)
	crpmBlendDatabaseCalculator(int maxNumEntries, float distanceThreshold=0.01f, float sourceDistanceThreshold=0.05f, float parameterExtensionFactor=0.2f, bool stochasticPopulate=true, const char* debugPath=NULL);

	// PURPOSE: Initializer
	// maxNumEntries - maximum number of entries to insert into the blend database
	// distanceThreshold - minimum (parameter space) distance threshold between entries
	// sourceDistanceThreshold - minimum (parameter space) distance threshold between source entries
	// parameterExtension - percentage to bias minimum and maximum parameter space boundaries
	// stochasticPopulate - use random method to populate database, instead of barycentric search (default true)
	// debugPath - folder to output debug images into (default NULL, no debug images generated)
	void Init(int maxNumEntries, float distanceThreshold=0.01f, float sourceDistanceThreshold=0.05f, float parameterExtensionFactor=0.2f, bool stochasticPopulate=true, const char* debugPath=NULL);

	// PURPOSE: Type definition for supplying valid combinations of source weights
	typedef std::vector<std::vector<int> > ValidCombinations;

	// PURPOSE: Type definition for supplying variations
	struct Variations : public std::pair<std::string, std::vector< std::pair<std::vector<int>, std::vector<Variations> > > > {};

	// PURPOSE: Calculate the blend database, populate it with entries
	// PARAMS: 
	// inoutDatabase - database to populate (must have already been initialized with parameterization)
	// sampler - sampler to use to translate weights into parameters
	// clips - optional pointer to set of clips used 
	// rcData - optional pointer to registration curves
	// validCombinations - optionally specify subsets of valid combinations of source weights (default all combinations valid)
	// NOTES: clips and rcData can be optionally provided so that source entries can be removed
	// from them if they are found to be too close in parameter space. If not provided, 
	// the source entries won't be removed, but also won't be used by the blend database.
	bool Calculate(crpmBlendDatabase& inoutDatabase, const crpmParameterSampler& sampler, crClips* clips=NULL, crpmRegistrationCurveData* rcData=NULL, const Variations* variations=NULL, const ValidCombinations* validCombinations=NULL);


protected:

	// PURPOSE: Internal function, calculates entries for source weights
	void CalcSourceEntries(atArray<crpmBlendDatabase::Entry>& outSourceEntries, const crpmParameterSampler& sampler) const;

	// PURPOSE: Internal function, initializes parameter space
	void InitParameterSpace(crpmBlendDatabase& bd, const atArray<crpmBlendDatabase::Entry>& sourceEntries) const;

	// PURPOSE: Internal function, simplify source entries (remove ones that are too close)
	void SimplifySourceEntries(crpmBlendDatabase& bd, atArray<crpmBlendDatabase::Entry>& inoutSourceEntries, crClips* clips, crpmRegistrationCurveData* rcData) const;

	// PURPOSE: Internal function, populates database with entries
	void PopulateDatabase(crpmBlendDatabase& bd, const atArray<crpmBlendDatabase::Entry>& sourceEntries, const crpmParameterSampler& sampler, const ValidCombinations& validCombinations, const Variations* variations) const;

	// PURPPOSE: Internal function, recursively searches for database entry candidates
	int RecursiveBarycentricInsert(crpmBlendDatabase& bd, const crpmParameterSampler& sampler, const Variations* variations, const std::vector<int>& vc, crpmWeights& weights, int dimension, int fails) const;

	// PURPOSE: Internal function, attempts to insert candidate entry into database
	bool InsertIntoDatabase(crpmBlendDatabase& bd, const crpmParameter& parameter, const crpmWeights& weights, const Variations* variations, float distanceThreshold) const;

	// PURPOSE: Internal function, calculate variations from weights
	int CalcVariationsFromWeights(const Variations& variations, const crpmWeights& weights, u32& outVariations, bool top) const;

	// PURPOSE: Internal function, generates a legal random set of weights
	void GenerateRandomWeights(const crpmBlendDatabase& bd, crpmWeights& outWeights, const std::vector<int>& validCombination) const;

	// PURPOSE: Internal function, generates combination array
	void GenerateValidCombinations(const crpmBlendDatabase& bd, atArray<crpmBlendDatabase::Entry>& sourceEntries, const Variations* variations, ValidCombinations& validCombinations) const;

	// PURPOSE: Internal function, recursively generate valid combination
	void GenerateCombinations(int weightIdx, int numWeightDimensions, const Variations* variations, std::vector<int>& weightIndices, std::vector<std::pair<std::vector<int>, float> >& combinations) const;

	// PURPOSE: Internal function, checks a combination conforms with variations
	int CheckCombinationVariations(const std::vector<int>& weightIndices, const Variations& variations) const;

	// PURPOSE: Internal function, sort blend database entries
	void SortDatabase(crpmBlendDatabase& bd, int sortDimension) const;

	// PURPOSE: Internal function, calculate average minimum distance database entries
	float CalcAverageMinDistanceToParameter(crpmBlendDatabase& inoutBlendDatabase) const;

	// PURPOSE: Internal function, estimate hypersphere volume for parameter combination
	float CalcHypersphereVolume(const crpmBlendDatabase& bd, const std::vector<int>& combination) const;

	// PURPOSE: Internal function, calculate number of entries contained within hypersphere volume
	int CalcNumHypersphereEntries(const crpmBlendDatabase& bd, const std::vector<int>& combination) const;

	bool m_StochasticPopulate;
	int m_MaxNumEntries;
	float m_DistanceThreshold;
	float m_SourceDistanceThreshold;
	float m_ParameterExtensionFactor;

	atString m_DebugPath;

	mutable mthRandom m_Rand;
};

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CRPARAMETERIZEDMOTION_BLENDDATABASECALCULATOR_H

