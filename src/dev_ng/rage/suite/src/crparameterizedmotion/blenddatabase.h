//
// crparameterizedmotion/blenddatabase.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_BLENDDATABASE_H
#define CRPARAMETERIZEDMOTION_BLENDDATABASE_H

#include "parameter.h"
#include "parameterization.h"
#include "weights.h"

#include "cranimation/animation_config.h"

namespace rage
{

class crDumpOutput;
class crpmParameterization;
class crpmBlendDatabaseCalculator;

typedef u32 crpmVariations;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Database of parameter to weight entries, searched at runtime using k-nearest neighbor
// algorithm to find weights necessary to achieve arbitrary input parameters.
class crpmBlendDatabase
{
	friend class crpmBlendDatabaseCalculator;

public:

	// PURPOSE: Default constructor
	crpmBlendDatabase();

	// PURPOSE: Resource constructor
	crpmBlendDatabase(datResource&);

	// PURPOSE: Initializing constructor
	// PARAMS:
	// parameterization - describes parameter dimensions (blend database will take copy)
	// numWeightDimensions - number of weight dimensions (ie number of clips in registration curve)
	crpmBlendDatabase(const crpmParameterization& parameterization, int numWeightDimensions);

	// PURPOSE: Destructor
	~crpmBlendDatabase();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmBlendDatabase);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT


	// PURPOSE: Initializer
	// PARAMS:
	// parameterization - describes parameter dimensions (blend database will take copy)
	// numWeightDimensions - number of weight dimensions (ie number of clips in registration curve)
	// NOTES: Call this (or initializing constructor) before passing blend database to calculator
	void Init(const crpmParameterization& parameterization, int numWeightDimensions);

	// PURPOSE: Shutdown
	void Shutdown();


	// PURPOSE: Get parameterization
	const crpmParameterization& GetParameterization() const;

	// PURPOSE: Get number of parameter dimensions
	int GetNumParameterDimensions() const;

	// PURPOSE: Get number of weight dimensions
	int GetNumWeightDimensions() const;

	// PURPOSE: Get boundaries of parameter space
	void GetParameterBoundaries(crpmParameter& outMinCorner, crpmParameter& outMaxCorner) const;

	// PURPOSE: Get extended boundaries of parameter space
	void GetExtendedParameterBoundaries(crpmParameter& outMinCorner, crpmParameter& outMaxCorner) const;

	// PURPOSE: Get extent of parameter space (ie distance between corners in each dimension)
	void GetParameterExtent(crpmParameter& outExtent) const;

	// PURPOSE: Verify parameter lies within extended boundaries
	bool IsParameterInsideExtendedBoundaries(const crpmParameter& parameter) const;

	// PURPOSE: Get number of entries in database
	int GetNumEntries() const;


	// PURPOSE: Get entry by index
	void GetEntry(int idx, crpmParameter& outParameter, crpmWeights& outWeights, crpmVariations& outVariations) const;


	// PURPOSE: Find the minimum distance from parameter to entry in database
	float FindMinDistanceToParameter(const crpmParameter& parameter, crpmVariations variations) const;

	// PURPOSE: Map an arbitrary parameter to a set of weight values (interpolates between k nearest neighbors)
	// PARAMS:
	// parameter - parameter to translate into set of weights
	// variations - define which variations to use
	// outWeights - resulting interpolated weights (initialize to match database weight dimensions)
	// k - number of neighboring entries in database to use when interpolating (default -1, select automatically)
	// RETURNS: true - successful, false - failed to perform mapping (outWeights is undefined)
	bool MapParameterToWeights(const crpmParameter& parameter, crpmVariations variations, crpmWeights& outWeights, int k=-1) const;

#if CR_DEV
	// PURPOSE: Serialize
	void Serialize(datSerialize&);

	// PURPOSE: Dump output
	void Dump(crDumpOutput&) const;
#endif // CR_DEV

private:

	// PURPOSE: Entry in the blend database
	class Entry
	{
	public:

		// PURPOSE: Default constructor
		Entry();

		// PURPOSE: Resource constructor
		Entry(datResource&);

		// PURPOSE: Initializing constructor
		Entry(const crpmParameter& parameter, const crpmWeights& weights, crpmVariations variations);

		// PURPOSE: Destructor
		~Entry();

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(Entry);

		// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		// PURPOSE: Get parameter
		const crpmParameter& GetParameter() const;

		// PURPOSE: Get weights
		void GetWeights(crpmWeights& weights) const;

		// PURPOSE: Get variations
		crpmVariations GetVariations() const;

#if CR_DEV
		// PURPOSE: Serialize
		void Serialize(datSerialize&);
#endif // CR_DEV

	private:

		crpmParameter m_Parameter;
		crpmWeightsPacked m_Weights;
		crpmVariations m_Variations;
	};


	// PURPOSE: Internal function, calculate squared distance between two parameters
	float CalcParameterDistanceSqr(const crpmParameter& parameter0, const crpmParameter& parameter1) const;

	// PURPOSE: Internal function, calculate squared distance between two parameters
	float CalcParameterDistanceSqr(const crpmParameter& parameter0, const crpmParameter& parameter1, float thresholdSqr) const;

	// PURPOSE: Internal function, calculate range of entries that are possibly within threshold
	void CalcEntryRangeThresholdSqr(const crpmParameter& parameter, float thresholdSqr, int& first, int& last) const;

	// PURPOSE: Nearest result type
	struct NearestResult
	{
		const Entry* first;
		float second;
	};

	// PURPOSE: Internal function, find k nearest entries to a parameter
	bool FindKNearestToParameter(int k, const crpmParameter& parameter, crpmVariations variations, NearestResult* nearest) const;

private:

    crpmParameterization m_Parameterization;
	int m_NumWeightDimensions;

    crpmParameter m_MinCorner;
    crpmParameter m_MaxCorner;
    crpmParameter m_MinCornerExtended;
    crpmParameter m_MaxCornerExtended;
	crpmParameter m_Extent;
	crpmParameter m_InvExtent;

	atArray< datOwner<Entry> > m_Entries;
	int m_SortDimension;

	float m_AveMinDistance;
};


////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterization& crpmBlendDatabase::GetParameterization() const
{
	return m_Parameterization;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmBlendDatabase::GetNumParameterDimensions() const
{
	return m_Parameterization.GetNumDimensions();
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmBlendDatabase::GetNumWeightDimensions() const
{
	return m_NumWeightDimensions;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmBlendDatabase::GetParameterBoundaries(crpmParameter& outMinCorner, crpmParameter& outMaxCorner) const
{
	outMinCorner = m_MinCorner;
	outMaxCorner = m_MaxCorner;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmBlendDatabase::GetExtendedParameterBoundaries(crpmParameter& outMinCorner, crpmParameter& outMaxCorner) const
{
	outMinCorner = m_MinCornerExtended;
	outMaxCorner = m_MaxCornerExtended;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmBlendDatabase::GetParameterExtent(crpmParameter& outExtent) const
{
	outExtent = m_Extent;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmBlendDatabase::GetNumEntries() const
{
	return int(m_Entries.size());
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmBlendDatabase::GetEntry(int idx, crpmParameter& outParameter, crpmWeights& outWeights, crpmVariations& outVariations) const
{
	const Entry& e = *m_Entries[idx];
	outParameter = e.GetParameter();
	e.GetWeights(outWeights);
	outVariations = e.GetVariations();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmBlendDatabase::Entry::Entry()
: m_Variations(0)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmBlendDatabase::Entry::Entry(const crpmParameter& parameter, const crpmWeights& weights, crpmVariations variations)
: m_Parameter(parameter)
, m_Weights(weights)
, m_Variations(variations)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmBlendDatabase::Entry::~Entry()
{
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmBlendDatabase::Entry::GetParameter() const
{
	return m_Parameter;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmBlendDatabase::Entry::GetWeights(crpmWeights& weights) const
{
	m_Weights.Unpack(weights);
}

////////////////////////////////////////////////////////////////////////////////

inline crpmVariations crpmBlendDatabase::Entry::GetVariations() const
{
	return m_Variations;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


#endif // CRPARAMETERIZEDMOTION_BLENDDATABASE_H
