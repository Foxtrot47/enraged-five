//
// crparameterizedmotion/pointcloudtranform.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "pointcloudtransform.h"

#include "pointcloud.h"


namespace rage
{

const crpmPointCloudTransform crpmPointCloudTransform::IDENTITY;

////////////////////////////////////////////////////////////////////////////////

crpmPointCloudTransform::crpmPointCloudTransform(float angle, float u, float v)
: m_Translation(V_ZERO)
, m_Angle(angle)
{
	SetTranslationHorizontal(m_Translation, u, v);
}

////////////////////////////////////////////////////////////////////////////////

crpmPointCloudTransform::crpmPointCloudTransform(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmPointCloudTransform::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crpmPointCloudTransform);
	STRUCT_FIELD(m_Translation);
	STRUCT_FIELD(m_Angle);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudTransform::FromMatrix(Mat34V_In matrix)
{
	m_Translation = matrix.GetCol3();
	m_Angle.FromMatrix(matrix);
}

////////////////////////////////////////////////////////////////////////////////

Mat34V_Out crpmPointCloudTransform::ToMatrix() const
{
	Mat34V matrix;
	m_Angle.ToMatrix(matrix);
	matrix.SetCol3(m_Translation);
	return matrix;
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudTransform::FromVector3(Vec3V_In v)
{
	m_Angle.SetRadians(v[0]);

	SetTranslationHorizontal(m_Translation, v[1], v[2]);
	SetTranslationVertical(m_Translation, 0.f);
}

////////////////////////////////////////////////////////////////////////////////

Vec3V_Out crpmPointCloudTransform::ToVector3() const
{
	Vec3V v;
	v[0] = m_Angle.GetRadians();
	GetTranslationHorizontal(m_Translation, v[1], v[2]);
	return v;
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloudTransform::Calculate(const crpmPointCloud& p1, const crpmPointCloud& p2)
{
    // Calculate the transformation between two point clouds. The formula can
    // be found in the "Registration Curves"-Paper, on page 4

	AssertMsg(p1.size()==p2.size(), "crpmPointCloudTransform::Calculate - point clouds must be of equal size");
    AssertMsg(p1.size()>1, "crpmPointCloudTransform::Calculate - point clouds with only a single point don't work");

    // Calculate the weighted averages of the X and Z coordinates of all
    // points in both clouds
    crpmPointType mean1 = p1.ComputeMeanPoint();
    crpmPointType mean2 = p2.ComputeMeanPoint();

	float mean1u, mean1v, mean2u, mean2v;
	GetTranslationHorizontal(mean1, mean1u, mean1v);
	GetTranslationHorizontal(mean2, mean2u, mean2v);

    // Calculate the minimum rotation angle
    float sum1 = 0.f;
    float sum2 = 0.f;

	AssertMsg(IsClose(p1.GetTotalPointWeights(), p2.GetTotalPointWeights()) , "crpmPointCloudTransform::Calculate - total point weights must be identical");
    float invTotalWeight = 1.f/p1.GetTotalPointWeights();

	for(int i=0; i<static_cast<int>(p1.size()); ++i)
	{
		float u1, u2, v1, v2;
		GetTranslationHorizontal(p1.GetPoint(i), u1, v1);
		GetTranslationHorizontal(p2.GetPoint(i), u2, v2);

		AssertMsg(IsClose(p1.GetPointWeight(i), p2.GetPointWeight(i)) , "crpmPointCloudTransform::Calculate - individual point weights must be identical");
		float weight = p1.GetPointWeight(i);

		sum1 += (weight * invTotalWeight) * ((u1*v2) - (u2*v1));
		sum2 += (weight * invTotalWeight) * ((u1*u2) + (v1*v2));
	}

	float prod1 = mean1u*mean2v;
	float prod2 = mean2u*mean1v;

	float prod3 = mean1u*mean2u;
	float prod4 = mean1v*mean2v;

	float diff1 = prod1-prod2;
	float sum3 = prod3+prod4;

	float sinA = sum1-diff1;
	float cosA = sum2-sum3;

	float angle = atan2(sinA, cosA);
	if(IsYUpAxis())
	{
		m_Angle.SetRadians(angle);
	}
	else
	{
		m_Angle.SetRadians(-angle);
	}

	float uCosP2 = mean2u*cos(angle);
	float vSinP2 = mean2v*sin(angle);

	float uSinP2 = mean2u*sin(angle);
	float vCosP2 = mean2v*cos(angle);

	float u0 = mean1u - uCosP2 - vSinP2;
	float v0 = mean1v + uSinP2 - vCosP2;

	m_Translation = Vec3V(V_ZERO);
	SetTranslationHorizontal(m_Translation, u0, v0);

	float diff;
	GetTranslationVertical(mean1-mean2, diff);
	SetTranslationVertical(m_Translation, diff);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
