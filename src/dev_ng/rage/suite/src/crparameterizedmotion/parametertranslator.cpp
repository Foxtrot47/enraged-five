//
// crparameterizedmotion/parametertranslator.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "parametertranslator.h"

#if 0  // COMPOSITE PM'S REMOVED TEMPORARILY




namespace rage
{

//------------------------------------------------------------------------------
const crpmParameterTranslator& getNullParameterTranslator()
//------------------------------------------------------------------------------
{
    static crpmParameterTranslator translator;
    return translator;
}

} // namespace rage

#endif // 0  // COMPOSITE PM'S REMOVED TEMPORARILY

