//
// crparameterizedmotion/registrationcurvecalculator.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_REGISTRATIONCURVECALCULATOR_H
#define CRPARAMETERIZEDMOTION_REGISTRATIONCURVECALCULATOR_H

#include "distancegrid.h"
#include "registrationcurve.h"

#include "atl/map.h"
#include "atl/string.h"

namespace rage
{

class crClip;
class crClips;
class crSkeletonData;
class crWeightSet;
class crpmDynamicProgramming;
class crpmTimeAlignment;


////////////////////////////////////////////////////////////////////////////////

class crpmRegistrationCurveCalculator
{
public:

	// PURPOSE: Default constructor
	crpmRegistrationCurveCalculator();

	// PURPOSE: Initializing constructor
	// PARAMS:
	// skelData - skeleton data
	// weightSet - optional alignment weight set (to describe and bias point clouds)
	// filterWeightSet - optional filter weight set (to filter out partial body)
	// filterMover - filter mover (default false)
	// sampleRate - rate at which to sample clips in intermediate calculations (default 30Hz or 1/30)
	// slopeLimit - maximum slope permitted during grid path searching
	// windowSize - sample window size used when calculating point clouds
	// debugPath - folder to output debug images into (default NULL, no debug images generated)
	crpmRegistrationCurveCalculator(const crSkeletonData& skelData, const crWeightSet* weightSet=NULL, const crWeightSet* filterWeightSet=NULL, bool filterMover=false, float sampleRate=1.f/30.f, int slopeLimit=2, int windowSize=0, const char* debugPath=NULL);

	// PURPOSE: Destructor
	~crpmRegistrationCurveCalculator();

	// PURPOSE: Initialize calculator
	// skelData - skeleton data
	// weightSet - optional alignment weight set (to describe and bias point clouds)
	// filterWeightSet - optional filter weight set (to filter out partial body)
	// filterMover - filter mover (default false)
	// sampleRate - rate at which to sample clips in intermediate calculations (default 30Hz or 1/30)
	// slopeLimit - maximum slope permitted during grid path searching
	// windowSize - sample window size used when calculating point clouds
	// debugPath - folder to output debug images into (default NULL, no debug images generated)
	void Init(const crSkeletonData& skelData, const crWeightSet* weightSet=NULL, const crWeightSet* filterWeightSet=NULL, bool filterMover=false, float sampleRate=1.f/30.f, int slopeLimit=2, int windowSize=0, const char* debugPath=NULL);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Calculate registration curve data from clips provided, using time alignment method
	// PARAMS:
	// outRc - output registration curve (only valid if return value is true)
	// clips - ordered list of clips to generate registration curve for
	// looped - does the registration curve loop (are the clips all looped)
	// refClipIdx - force particular clip to be reference clip (default, select best automatically)
	bool CalculateByTimeAlignments(crpmRegistrationCurveData& outRc, const crClips& clips, bool looped=false, int refClipIdx=-1);

	// PURPOSE: Calculate registration curve data from clips provided, using frame correspondence method
//	bool CalculateByFrameCorrespondences()
//	atArray<crpmNDimensionalVectorType>& frameCorrespondences,


protected:

	// PURPOSE: Internal function, initializes the registration curve
	bool InitRegistrationCurve(crpmRegistrationCurveData& outRc, const crClips& clips, bool looped);

	// PURPOSE: Internal function, calculates time and spatial alignments
	bool CreateTimeAlignments(const crClips& clips);

	// PURPOSE: Internal function, adds a clip pair, calculating all the intermediate data
	bool AddClips(const crClip& clip, const crClip& otherClip);

	// PURPOSE: Internal function, calculates the best path across the grid
	bool CalcBestPath(crpmDistanceGrid::Path& outPath, const crpmDistanceGrid& grid) const;

	// PURPOSE: Internal function, select best reference clip (if override provided isn't valid)
	int ChooseReferenceClip(const crClips& clips, int refClip=-1);

	// PURPOSE: Internal function, calculate time and alignment control points
	bool CalcTimeAndSpatialAlignmentControlPoints(atArray<atArray<float> >& outTimeControlPoints, atArray<atArray<Vec3V> >& outSpatialControlPoints, int numControlPoints, const crClips& clips, int refClipIdx);

	// PURPOSE: Internal function, uses time and alignment control points to build registration curves
	void CreateTimeAndSpatialAlignmentCurves(crpmRegistrationCurveData& outRc, const atArray<atArray<float> >& timeControlPoints, const atArray<atArray<Vec3V> >& spatialControlPoints);

	// PURPOSE: Create graphical images of internal intermediate structures for debugging
	void GenerateDistanceGridImage(const crpmDistanceGrid& grid) const;

	// PURPOSE: Create graphical images of internal intermediate structures for debugging
	void GeneratePathImage(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& grid) const;

	// PURPOSE: Create graphical images of internal intermediate structures for debugging
	void GenerateDynamicProgrammingImage(const crpmDynamicProgramming& dp, const crpmDistanceGrid& grid) const;

	// PURPOSE: Dump diagnostic information for debugging
	void Dump();

protected:

	const crSkeletonData* m_SkeletonData;
	const crWeightSet* m_WeightSet;
	const crWeightSet* m_FilterWeightSet;
	bool m_FilterMover;
	float m_SampleRate;
	int m_WindowSize;
	int m_SlopeLimit;
	atString m_DebugPath;

	atArray<crpmTimeAlignment*> m_TimeAlignments;

	typedef atMap<const crClip*, atArray<const crpmTimeAlignment*> > TimeAlignmentMap;
	TimeAlignmentMap m_TimeAlignmentMap;
};

} // namespace rage


#endif // CRPARAMETERIZEDMOTION_REGISTRATIONCURVECALCULATOR_H
