//
// crparameterizedmotion/parameterizedmotiondictionary.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "parameterizedmotiondictionary.h"

#include "parameterizedmotionsimple.h"

#include "atl/map_struct.h"
#include "file/stream.h"
#include "file/token.h"
#include "paging/rscbuilder.h"
#include "system/memory.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionDictionary::crpmParameterizedMotionDictionary()
: pgBaseRefCounted(0)
, m_ClipDictionary(NULL)
, m_ClipDictionaryOwner(true)
, m_BaseNameKeys(false)
{
	if(m_ClipDictionaryOwner)
	{
		m_ClipDictionary = rage_new crClipDictionary;
	}
}

////////////////////////////////////////////////////////////////////////////////

void FixupDataFunc(datResource &rsc, datOwner<crpmParameterizedMotionData>& data)
{
    data.Place(&data, rsc);
}

crpmParameterizedMotionDictionary::crpmParameterizedMotionDictionary(datResource& rsc)
: pgBaseRefCounted(rsc)
, m_ClipDictionary(rsc)
, m_ParameterizedMotions(rsc, NULL, &FixupDataFunc)
{
	if(m_ClipDictionaryOwner)
	{
		crClipDictionary::Place(m_ClipDictionary, rsc);
	}
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionDictionary::~crpmParameterizedMotionDictionary()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

bool ReleasePmFunc(crpmParameterizedMotionData* pmData, crpmParameterizedMotionDictionary::PmKey, void*)
{
	if(pmData)
	{
		pmData->Release();
	}
	return true;
}

void crpmParameterizedMotionDictionary::Shutdown()
{
	if(m_ClipDictionaryOwner)
	{
		delete m_ClipDictionary;
		m_ClipDictionary = NULL;
	}

	ForAll(&ReleasePmFunc, NULL);

	m_ParameterizedMotions.Kill();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmParameterizedMotionDictionary);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmParameterizedMotionDictionary::DeclareStruct(datTypeStruct& s)
{
	pgBaseRefCounted::DeclareStruct(s);
	STRUCT_BEGIN(crpmParameterizedMotionDictionary);
	if(m_ClipDictionaryOwner)
	{
		STRUCT_FIELD_OWNED_PTR(m_ClipDictionary);
	}
	else
	{
		STRUCT_FIELD_VP(m_ClipDictionary);
	}
	STRUCT_FIELD(m_ClipDictionaryOwner);
	STRUCT_FIELD(m_BaseNameKeys);
	STRUCT_IGNORE(m_Padding);
	STRUCT_FIELD(m_ParameterizedMotions);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
crpmParameterizedMotionDictionary* crpmParameterizedMotionDictionary::AllocateAndLoadParameterizedMotions(const char* listFilename, crpmParameterizedMotionLoader* loader, crClipLoader* subLoader, crAnimLoader* subSubLoader, bool baseNameKeys)
{
	crpmParameterizedMotionDictionary* dictionary = rage_new crpmParameterizedMotionDictionary;
	if(dictionary->LoadParameterizedMotions(listFilename, loader, subLoader, subSubLoader, baseNameKeys))
	{
		return dictionary;
	}
	delete dictionary;
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionDictionary* crpmParameterizedMotionDictionary::AllocateAndLoadParameterizedMotions(const atArray<atString>& pmFilenames, crpmParameterizedMotionLoader* loader, crClipLoader* subLoader, crAnimLoader* subSubLoader, bool baseNameKeys)
{
	crpmParameterizedMotionDictionary* dictionary = rage_new crpmParameterizedMotionDictionary;
	if(dictionary->LoadParameterizedMotions(pmFilenames, loader, subLoader, subSubLoader, baseNameKeys))
	{
		return dictionary;
	}
	delete dictionary;
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterizedMotionDictionary::LoadParameterizedMotions(const char* listFilename, crpmParameterizedMotionLoader* loader, crClipLoader* subLoader, crAnimLoader* subSubLoader, bool baseNameKeys)
{
	fiSafeStream f(ASSET.Open(listFilename, "pmlist"));
	if(f)
	{
		fiTokenizer T(listFilename, f);	

		sysMemStartTemp();
		atArray<atString> pmFilenames;

		const int maxBufSize = RAGE_MAX_PATH;
		char buf[maxBufSize];
		while(T.GetLine(buf, maxBufSize) > 0)
		{
			pmFilenames.Grow() = buf;
		}
		sysMemEndTemp();

		bool success = LoadParameterizedMotions(pmFilenames, loader, subLoader, subSubLoader, baseNameKeys);

		sysMemStartTemp();
		pmFilenames.Reset();
		sysMemEndTemp();

		return success;
	}
	else
	{
		Errorf("crpmParameterizedMotionDictionary::LoadParameterizedMotions - failed to open list file '%s'", listFilename);
		return false;
	}
}


////////////////////////////////////////////////////////////////////////////////

class ClipLoaderDictionaryBuilder : public crClipLoader
{
public:

	ClipLoaderDictionaryBuilder(crClipLoader& clipLoader)
		: m_ClipLoader(&clipLoader)
	{
	}

	virtual ~ClipLoaderDictionaryBuilder()
	{
		Shutdown();
	}
	
	void Shutdown()
	{
		m_ClipNames.Reset();
	}

	virtual crClip* AllocateAndLoadClip(const char* clipFilename, crAnimLoader* animLoader)
	{
		crClip* clip = m_ClipLoader->AllocateAndLoadClip(clipFilename, animLoader);
		if(clip)
		{
			const int numClips = m_ClipNames.GetCount();
			for(int i=0; i<numClips; ++i)
			{
				if(!stricmp((const char*)m_ClipNames[i], clip->GetName()))
				{
					return clip;
				}
			}

			m_ClipNames.Grow() = clip->GetName();
		}
		else
		{
			Errorf("ClipLoaderDictionaryBuilder::AllocateAndLoadClip - failed to load clip '%s'", clipFilename);
		}

		return clip;
	}

	void BuildDictionary(crClipDictionary& dictionary, crAnimLoader* animLoader, bool baseNameKeys)
	{
		dictionary.LoadClips(m_ClipNames, m_ClipLoader, animLoader, baseNameKeys);
	}

	atArray<atString> m_ClipNames;
	crClipLoader* m_ClipLoader;
};

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterizedMotionDictionary::LoadParameterizedMotions(const atArray<atString>& pmFilenames, crpmParameterizedMotionLoader* loader, crClipLoader* subLoader, crAnimLoader* subSubLoader, bool baseNameKeys)
{
	crpmParameterizedMotionLoaderBasic defaultLoader;
	if(!loader)
	{
		loader = &defaultLoader;
	}

	crClipLoaderBasic defaultSubLoader;
	if(!subLoader)
	{
		subLoader = &defaultSubLoader;
	}

	crAnimLoaderBasic defaultSubSubLoader;
	if(!subSubLoader)
	{
		subSubLoader = &defaultSubSubLoader;
	}

	ClipLoaderDictionaryBuilder builderLoader(*subLoader);
	
	const int numPms = pmFilenames.GetCount();

	bool success = true;
	for(int i=0; i<numPms; ++i)
	{
		const char* pmFilename = (const char*)pmFilenames[i];

		sysMemStartTemp();
		crpmParameterizedMotionData* pm = loader->AllocateAndLoadParameterizedMotion(pmFilename, &builderLoader, subSubLoader);
		sysMemEndTemp();

		if(!pm)
		{
			Errorf("crpmParameterizedMotionDictionary::LoadParameterizedMotions - failed to load pm '%s'", pmFilename);
			success = false;
			continue;
		}

		sysMemStartTemp();
		delete pm;
		sysMemEndTemp();
	}

	Assert(m_ClipDictionary);
	builderLoader.BuildDictionary(*m_ClipDictionary, subSubLoader, baseNameKeys);

	sysMemStartTemp();
	builderLoader.Shutdown();
	sysMemEndTemp();

	crClipLoaderDictionary dictionaryLoader(m_ClipDictionary);

	m_ParameterizedMotions.Create(u16(numPms));
	m_BaseNameKeys = baseNameKeys;

	for(int i=0; i<numPms; ++i)
	{
		const char* pmFilename = (const char*)pmFilenames[i];

		PmKey key = ConvertNameToKey(pmFilename);
		const crpmParameterizedMotionData* existingPm = GetParameterizedMotion(key);
		if(existingPm)
		{
			if(!stricmp(existingPm->GetName(), pmFilename))
			{
				// NOTE - with composite parameterized motions this is more legitimate
				Warningf("crpmParameterizedMotionDictionary::LoadParameterizedMotions - attempted multiple addition of same pm '%s'", pmFilename);
				continue;
			}
			else
			{
				Errorf("crpmParameterizedMotionDictionary::LoadParameterizedMotions - hash clash '%s' == '%s' (%d) base name keys %d", pmFilename, (const char*)existingPm->GetName(), u32(key), baseNameKeys);
				success = false;
				continue;
			}
		}

		crpmParameterizedMotionData* pm = loader->AllocateAndLoadParameterizedMotion(pmFilename, &dictionaryLoader, NULL);
		if(!pm)
		{
			Errorf("crpmParameterizedMotionDictionary::LoadParameterizedMotions - failed to load pm '%s'", pmFilename);
			success = false;
			continue;
		}

		m_ParameterizedMotions.Insert(key, pm);
	}

	return success;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionLoaderBasic::crpmParameterizedMotionLoaderBasic()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionLoaderBasic::~crpmParameterizedMotionLoaderBasic()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionData* crpmParameterizedMotionLoaderBasic::AllocateAndLoadParameterizedMotion(const char* pmFilename, crClipLoader* clipLoader, crAnimLoader* animLoader)
{
	return crpmParameterizedMotionData::AllocateAndLoad(pmFilename, this, clipLoader, animLoader);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionDictionary* crpmParameterizedMotionDictionary::LoadResource(const char* dictionaryFilename)
{
	crpmParameterizedMotionDictionary *pdt = NULL;

	pgRscBuilder::Load(pdt, dictionaryFilename, "#pdt", crpmParameterizedMotionDictionary::RORC_VERSION);

	return pdt;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionLoader::crpmParameterizedMotionLoader()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionLoader::~crpmParameterizedMotionLoader()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionLoaderDictionary::crpmParameterizedMotionLoaderDictionary()
: m_Dictionary(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionLoaderDictionary::crpmParameterizedMotionLoaderDictionary(const crpmParameterizedMotionDictionary* dictionary)
: m_Dictionary(NULL)
{
	Init(dictionary);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionLoaderDictionary::~crpmParameterizedMotionLoaderDictionary()
{
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionLoaderDictionary::Init(const crpmParameterizedMotionDictionary* dictionary)
{
	m_Dictionary = dictionary;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionData* crpmParameterizedMotionLoaderDictionary::AllocateAndLoadParameterizedMotion(const char* pmFilename, crClipLoader*, crAnimLoader*)
{
	if(m_Dictionary)
	{
		crpmParameterizedMotionData* pm = const_cast<crpmParameterizedMotionData*>(m_Dictionary->GetParameterizedMotion(pmFilename));
		if(pm)
		{
			pm->AddRef();
			return pm;
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
