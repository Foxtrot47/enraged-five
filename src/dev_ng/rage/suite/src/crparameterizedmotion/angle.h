//
// crparameterizedmotion/angle.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_ANGLE_H
#define CRPARAMETERIZEDMOTION_ANGLE_H

#include "data/resource.h"
#include "data/struct.h"
#include "math/amath.h"
#include "math/angmath.h"
#include "vector/vector3.h"
#include "vectormath/legacyconvert.h"
#include "vectormath/vec3v.h"
#include "vectormath/mat34v.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Represents an angle, and variety of angle based operations.
class crpmAngle
{
public:

	// PURPOSE: Default constructor
	crpmAngle();

	// PURPOSE: Resource constructor
	crpmAngle(datResource&);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crpmAngle();

	// PURPOSE: Set angle from another
	// angle - new angle
	void Set(const crpmAngle& angle);

	// PURPOSE: Set angle using radians
	// radians - new angle in radians
	void SetRadians(const float radians);

	// PURPOSE: Set angle using degrees
	// degrees - new angle in degrees
	void SetDegrees(const float degrees);

	// PURPOSE: Get angle in radians
	// RETURNS: raw angle in radians (no range limits)
	float GetRadians() const;

	// PURPOSE: Get angle in degrees
	// RETURNS: raw angle in degrees (no range limits)
	float GetDegrees() const;


	// PURPOSE: Get equivalent angle in radians, between -PI and PI
	// RETURNS: equivalent angle in radians (>= -PI and <= PI)
	float GetRadiansMinusPiToPi() const;

	// PURPOSE: Get equivalent angle in radians, between -PI and PI
	// RETURNS: equivalent angle in radians (>= -PI and < PI)
	float GetRadiansMinusPiToPiX() const;

	// PURPOSE: Get equivalent angle in radians, between 0 and 2*PI
	// RETURNS: equivalent angle in radians (>= 0 and <= 2*PI)
	float GetRadians0To2Pi() const;

	// PURPOSE: Get equivalent angle in degrees, between -180 and 180
	// RETURNS: equivalent angle in degrees (>= -180 and <= 180)
	float GetDegreesMinus180To180() const;

	// PURPOSE: Get equivalent angle in degrees, between -180 and 180
	// RETURNS: equivalent angle in degrees (>= -180 and < 180)
	float GetDegreesMinus180To180X() const;

	// PURPOSE: Get equivalent angle in degrees, between 0 and 360
	// RETURNS: equivalent angle in degrees (>= 0 and <= 360)
	float GetDegrees0To360() const;

	// PURPOSE: Get equivalent angle in degrees, between -270 and 90
	// RETURNS: equivalent angle in degrees (>= -270 and <= 90)
	float GetDegreesMinus270To90() const;

	// PURPOSE: Get equivalent angle in degrees, between -270 and 90
	// RETURNS: equivalent angle in degrees (>= -270 and < 90)
	float GetDegreesMinus270To90X() const;

	// PURPOSE: Get equivalent angle in degrees, between -270 and -90
	// RETURNS: equivalent angle in degrees (>= -270 and <= -90)
	float GetDegreesMinus270ToMinus90() const;

	// PURPOSE: Get equivalent angle in degrees, between -270 and -90
	// RETURNS: equivalent angle in degrees (>= -270 and < -90)
	float GetDegreesMinus270ToMinus90X() const;


	// PURPOSE: += operator
	void operator += (const crpmAngle& a);

	// PURPOSE: -= operator
	void operator -= (const crpmAngle& a);

	// PURPOSE: assignment operator
	virtual void operator = (const crpmAngle& a);

	// PURPOSE: equality operator
	bool operator == (const crpmAngle& a) const;

	// PURPOSE: Make the angle positive, produces equivalent angle >= 0
	void MakePositive();


	// PURPOSE: Interpolation the two input angles using parameter provided
	// PARAMS: t - interpolation parameter
	// a1 - input angle 1
	// a2 - input angle 2
	void Lerp(float t, const crpmAngle& a1, const crpmAngle& a2);

	// PURPOSE: Approach the goal angle at given rate
	// PARAMS: goal - goal angle
	// radiansPerSecond - approach rate in radians per second
	// deltaTime - elapsed time
	void Approach(const crpmAngle& goal, float radiansPerSecond, float deltaTime);

	
	// PURPOSE: Convert matrix into angle (by sampling the rotation about the up axis)
	void FromMatrix(Mat34V_In mtx);

	// PURPOSE: Convert angle into a matrix (by setting the rotation about the up axis)
	void ToMatrix(Mat34V_InOut outMtx) const;


private:

	// PURPOSE: Internal function for calculating equivalent angles within a range
	template<bool greaterEqual, bool lessEqual>
	static float ConvertRadians(float radians, float min, float max);
	
protected:

	float m_Radians;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Represents an angle that implicitly converts to radians
class crpmAngleRadians : public crpmAngle
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS: radians - default angle in radians (default 0.0)
	crpmAngleRadians(float radians=0.f);

	// PURPOSE: Implicit conversion to float
	// RETURNS: angle in radians (>= -PI and < PI)
	operator float() const;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Represents an angle that implicitly converts to degrees
class crpmAngleDegrees : public crpmAngle
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS: radians - default angle in degrees (default 0.0)
	crpmAngleDegrees(float degrees=0.f);

	// PURPOSE: Implicit conversion to float
	// RETURNS: angle in degrees (>= -180 and < 180)
	operator float() const;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Helps make a series of angles continuous (useful if being represented by spline)
class crpmAngleContinuityPreserver
{
public:

	// PURPOSE: Default constructor
	crpmAngleContinuityPreserver();

	// PURPOSE: Initializing constructor
	crpmAngleContinuityPreserver(float angle);

	// PURPOSE: Initialize preserver with first angle
	void Init(float angle);

	// PURPOSE: Make angle continuous
	float Get(float angle);

private:
	float m_Angle;
	bool m_Initialized;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Get the global up axis (ultimately from g_UpAxis)
Vec3V_Out GetUpAxis();

// PURPOSE: Is the +Y axis up?
bool IsYUpAxis();

// PURPOSE: Is the +Z axis up?
bool IsZUpAxis();

// PURPOSE: Split a vector into the horizontal components (as defined by the up axis)
void GetTranslationHorizontal(Vec3V_In vec, float& u, float& v);

// PURPOSE: Set the horizontal components in a vector (as defined by the up axis)
void SetTranslationHorizontal(Vec3V_InOut vec, float u, float v);

// PURPOSE: Split a vector into the vertical component (as defined by the up axis)
void GetTranslationVertical(Vec3V_In vec, float& h);

// PURPOSE: Set the vertical component in a vector (as defined by the up axis)
void SetTranslationVertical(Vec3V_InOut vec, float h);

// PURPOSE: Get vertical rotation component (as defined by the up axis)
void GetRotationVertical(Mat34V_In mtx, float& h);

// PURPOSE: Set vertical rotation component (as defined by the up axis)
void SetRotationVertical(Mat34V_InOut mtx, float h);

// PURPOSE: Set the horizontal rotation components (as defined by the up axis)
void GetRotationHorizontal(Mat34V_In mtx, float& u, float& v);

// PURPOSE: Set the horizontal rotation components (as defined by the up axis)
void SetRotationHorizontal(Mat34V_InOut mtx, float u, float v);

////////////////////////////////////////////////////////////////////////////////

inline crpmAngle::crpmAngle()
: m_Radians(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmAngle::~crpmAngle()
{
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmAngle::Set(const crpmAngle& angle)
{
	m_Radians = angle.GetRadians();
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmAngle::SetRadians(const float radians)
{
	m_Radians = radians;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmAngle::SetDegrees(const float degrees)
{
	m_Radians = degrees * DtoR;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetRadians() const
{
	return m_Radians;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetDegrees() const
{
	return m_Radians * RtoD;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetRadiansMinusPiToPi() const
{
	return ConvertRadians<true, true>(m_Radians, -PI, PI);
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetRadiansMinusPiToPiX() const
{
	return ConvertRadians<true, false>(m_Radians, -PI, PI);
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetRadians0To2Pi() const
{
	return ConvertRadians<true, true>(m_Radians, 0, 2.f*PI);
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetDegreesMinus180To180() const
{
	return GetRadiansMinusPiToPi() * RtoD;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetDegreesMinus180To180X() const
{
	return GetRadiansMinusPiToPiX() * RtoD;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetDegrees0To360() const
{
	return GetRadians0To2Pi() * RtoD;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetDegreesMinus270To90() const
{
	return ConvertRadians<true, true>(m_Radians, -1.5f*PI, 0.5f*PI) * RtoD;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetDegreesMinus270To90X() const
{
	return ConvertRadians<true, false>(m_Radians, -1.5f*PI, 0.5f*PI) * RtoD;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetDegreesMinus270ToMinus90() const
{
	return ConvertRadians<true, true>(m_Radians, -1.5f*PI, -0.5f*PI) * RtoD;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngle::GetDegreesMinus270ToMinus90X() const
{
	return ConvertRadians<true, false>(m_Radians, -1.5f*PI, -0.5f*PI) * RtoD;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmAngle::operator += (const crpmAngle& a)
{
	m_Radians += a.GetRadians();
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmAngle::operator -= (const crpmAngle& a)
{
	m_Radians -= a.GetRadians();
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmAngle::operator = (const crpmAngle& a)
{
	m_Radians = a.GetRadians();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmAngle::operator == (const crpmAngle& a) const
{
	return m_Radians == a.GetRadians();
}

////////////////////////////////////////////////////////////////////////////////

template<bool greaterEqual, bool lessEqual>
__forceinline float crpmAngle::ConvertRadians(float radians, float min, float max)
{
	Assert(min < max);

	if(greaterEqual?(radians<min):(radians<=min))
	{
		radians = CanonicalizeAngle(radians);
		float cmin = CanonicalizeAngle(min);
		if(greaterEqual?(radians<cmin):(radians<=cmin))
		{
			radians += 2.f*PI;
		}
		radians -= cmin-min;

		if(lessEqual?(radians<=max):(radians<max))
		{
			return radians;
		}
	}
	else if(lessEqual?(radians>max):(radians<=min))
	{
		radians = CanonicalizeAngle(radians);
		float cmax = CanonicalizeAngle(max);
		if(lessEqual?(radians>cmax):(radians>=cmax))
		{
			radians -= 2.f*PI;
		}
		radians -= cmax-max;

		if(greaterEqual?(radians>=min):(radians>min))
		{
			return radians;
		}
	}
	else
	{
		return radians;
	}

	// bad, limits are probably unreasonable (or possibly floating point math problem)
	return radians;
	//	return IsCloserToAngle(radians, min, max) ? min : max;  // better?
}

////////////////////////////////////////////////////////////////////////////////

inline crpmAngleRadians::crpmAngleRadians(float radians)
{
	SetRadians(radians);
}

////////////////////////////////////////////////////////////////////////////////

inline crpmAngleRadians::operator float() const
{
	return GetRadiansMinusPiToPiX();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmAngleDegrees::crpmAngleDegrees(float degrees)
{
	SetDegrees(degrees);
}

////////////////////////////////////////////////////////////////////////////////

inline crpmAngleDegrees::operator float() const
{
	return GetDegreesMinus180To180X();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmAngleContinuityPreserver::crpmAngleContinuityPreserver()
: m_Angle(0.f)
, m_Initialized(false)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmAngleContinuityPreserver::crpmAngleContinuityPreserver(float angle)
: m_Angle(0.f)
, m_Initialized(false)
{
	Init(angle);
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmAngleContinuityPreserver::Init(float angle)
{
	m_Angle = angle;
	m_Initialized = true;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmAngleContinuityPreserver::Get(float angle)
{
	if(m_Initialized)
	{
		while ((angle - m_Angle) > PI)
		{
			angle -= 2.f*PI;
		}
		while ((angle - m_Angle) < -PI)
		{
			angle += 2.f*PI;
		}
	}

	Init(angle);
	return angle;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec3V_Out GetUpAxis()
{
	Assert(g_UnitUp == YAXIS || g_UnitUp == ZAXIS);
	return RCC_VEC3V(g_UnitUp);
}

////////////////////////////////////////////////////////////////////////////////

inline bool IsYUpAxis()
{
	Assert(g_UnitUp == YAXIS || g_UnitUp == ZAXIS);
	return g_UnitUp.y > 0.f;
}

////////////////////////////////////////////////////////////////////////////////

inline bool IsZUpAxis()
{
	Assert(g_UnitUp == YAXIS || g_UnitUp == ZAXIS);
	return g_UnitUp.z > 0.f;
}

////////////////////////////////////////////////////////////////////////////////

inline void GetTranslationHorizontal(Vec3V_In vec, float& u, float& v)
{
	u = vec.GetXf();
	if(IsYUpAxis())
	{
		v = vec.GetZf();
	}
	else
	{
		v = vec.GetYf();
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void SetTranslationHorizontal(Vec3V_InOut vec, float u, float v)
{
	vec.SetXf(u);
	if(IsYUpAxis())
	{
		vec.SetZf(v);
	}
	else
	{
		vec.SetYf(v);
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void GetTranslationVertical(Vec3V_In vec, float& h)
{
	if(IsYUpAxis())
	{
		h = vec.GetYf();
	}
	else
	{
		h = vec.GetZf();
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void SetTranslationVertical(Vec3V_InOut vec, float h)
{
	if(IsYUpAxis())
	{
		vec.SetYf(h);
	}
	else
	{
		vec.SetZf(h);
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void GetRotationVertical(Mat34V_In mtx, float& h)
{
	Vec3V e;
	if(IsYUpAxis())
	{
		e = Mat33VToEulersYXZ(mtx.GetMat33());
		h = e.GetYf();
	}
	else
	{
		e = Mat33VToEulersZXY(mtx.GetMat33());
		h = e.GetZf();
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void SetRotationVertical(Mat34V_InOut mtx, float h)
{
	Vec3V e;
	if(IsYUpAxis())
	{
		e = Mat33VToEulersYXZ(mtx.GetMat33());
		e.SetYf(h);
		Mat33VFromEulersYXZ(mtx.GetMat33Ref(), e);
	}
	else
	{
		e = Mat33VToEulersZXY(mtx.GetMat33());
		e.SetZf(h);
		Mat33VFromEulersZXY(mtx.GetMat33Ref(), e);
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void GetRotationHorizontal(Mat34V_In mtx, float& u, float& v)
{
	Vec3V e;
	if(IsYUpAxis())
	{
		e = Mat33VToEulersYXZ(mtx.GetMat33());
		u = e.GetXf();
		v = e.GetZf();
	}
	else
	{
		e = Mat33VToEulersZXY(mtx.GetMat33());
		u = e.GetXf();
		v = e.GetYf();
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void SetRotationHorizontal(Mat34V_InOut mtx, float u, float v)
{
	Vec3V e;
	if(IsYUpAxis())
	{
		e = Mat33VToEulersYXZ(mtx.GetMat33());
		e.SetXf(u);
		e.SetZf(v);
		Mat33VFromEulersYXZ(mtx.GetMat33Ref(), e);
	}
	else
	{
		e = Mat33VToEulersZXY(mtx.GetMat33());
		e.SetXf(u);
		e.SetYf(v);
		Mat33VFromEulersZXY(mtx.GetMat33Ref(), e);
	}
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CRPARAMETERIZEDMOTION_ANGLE_H
