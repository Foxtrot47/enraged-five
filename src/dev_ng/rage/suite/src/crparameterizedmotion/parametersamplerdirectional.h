//
// crparameterizedmotion/parametersamplerdirectional.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERSAMPLERDIRECTIONAL_H
#define CRPARAMETERIZEDMOTION_PARAMETERSAMPLERDIRECTIONAL_H

#include "parametersampler.h"
#include "parameterization.h"
#include "vectormath/vec3v.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Directional parameter sampler class
// Samples direction of designated bone(s)
class crpmParameterSamplerDirectional : public crpmParameterSamplerPhysical
{
public:

	// PURPOSE:
	crpmParameterSamplerDirectional();

	// PURPOSE:
	crpmParameterSamplerDirectional(u16 aimBoneId, u16 baseBoneId, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE:
	virtual ~crpmParameterSamplerDirectional();

	// PURPOSE:
	CRPM_DECLARE_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerDirectional);

	// PURPOSE:
	void Init(u16 aimBoneId, u16 baseBoneId, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE:
	virtual void Shutdown();

protected:

	// PURPOSE:
	virtual void CalcParameterDerivedWithMotionController(crpmMotionController& mc, crpmParameter& outParameter) const;

	// PURPOSE: Debug mapping of parameter to world space for visualization
	virtual bool DebugMapParameterDerived(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const;

	// PURPOSE: Internal function, initialize base classes
	virtual void PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Internal function, override sampler property configuration
	virtual bool ConfigProperty(const char* token, ConfigParser& parser);

	// PURPOSE: Internal function, setup parameterization
	void InitParameterization();

protected:

	int m_AimBoneIdx;
	int m_BaseBoneIdx;
	Vec3V m_AimBoneOffset;
	float m_Wrap;
	bool m_Negate;
	atString m_Eulers;
	float m_Phase;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERSAMPLERDIRECTIONAL_H
