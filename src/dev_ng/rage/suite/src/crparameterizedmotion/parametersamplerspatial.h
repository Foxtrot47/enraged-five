//
// crparameterizedmotion/parametersamplerspatial.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERSAMPLERSPATIAL_H
#define CRPARAMETERIZEDMOTION_PARAMETERSAMPLERSPATIAL_H

#include "parametersampler.h"
#include "parameterization.h"

#include "crmetadata/tag.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Spatial parameter sampler class
// Samples spatial position of designated bone
class crpmParameterSamplerSpatial : public crpmParameterSamplerPhysical
{
public:

	// PURPOSE: Default constructor
	crpmParameterSamplerSpatial();

	// PURPOSE: Initializing constructor
	crpmParameterSamplerSpatial(u16 boneId, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Destructor
	virtual ~crpmParameterSamplerSpatial();

	// PURPOSE: Sampler registration
	CRPM_DECLARE_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerSpatial);

	// PURPOSE: Initialization
	void Init(u16 boneId, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Shutdown
	virtual void Shutdown();

protected:

	// PURPOSE: Calculate parameter override
	virtual void CalcParameterDerivedWithMotionController(crpmMotionController& mc, crpmParameter& outParameter) const;

	// PURPOSE: Debug mapping of parameter to world space for visualization
	virtual bool DebugMapParameterDerived(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const;

	// PURPOSE: Internal function, initialize base classes
	virtual void PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Internal function, override sampler property configuration
	virtual bool ConfigProperty(const char* token, ConfigParser& parser);

	// PURPOSE: Internal function, setup parameterization
	void InitParameterization();

protected:

	int m_BoneIdx;
	s8 m_MajorAxis;
	bool m_Minimal;
	crTag::Key m_TagKey;
	atString m_Axes;
	float m_Tolerance;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERSAMPLERSPATIAL_H
