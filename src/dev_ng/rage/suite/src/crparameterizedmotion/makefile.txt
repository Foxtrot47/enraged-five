Project crparameterizedmotion

IncludePath $(RAGE_DIR)\base\src

Files {
	Folder Common {
		crpmdiag.cpp
		crpmdiag.h
		resourceversions.h
		colormapper.cpp
		colormapper.h
		angle.cpp
		angle.h
		weights.cpp
		weights.h
		serializationhelper.h
		image.h
		spline.h
		motioncontroller.cpp
		motioncontroller.h
	}
	Folder Runtime {
		pointcloud.cpp
		pointcloud.h
		pointcloudtransform.cpp
		pointcloudtransform.h
		registrationcurve.cpp
		registrationcurve.h
		registrationcurveplayer.cpp
		registrationcurveplayer.h
		blenddatabase.cpp
		blenddatabase.h
		parameter.cpp
		parameter.h
		parameterization.cpp
		parameterization.h
		parameterinterpolator.cpp
		parameterinterpolator.h
	}
	Folder ParameterizedMotion {
		parameterizedmotion.cpp
		parameterizedmotion.h
		parameterizedmotionsimple.cpp
		parameterizedmotionsimple.h
		parameterizedmotionvariable.cpp
		parameterizedmotionvariable.h
		parameterizedmotiondictionary.cpp
		parameterizedmotiondictionary.h
		parameterizedmotionplayer.cpp
		parameterizedmotionplayer.h
	}
	Folder Samplers {
		parametersampler.cpp
		parametersampler.h
		parametersampleraim.cpp
		parametersampleraim.h
		parametersamplerclip.cpp
		parametersamplerclip.h
		parametersamplerdirectional.cpp
		parametersamplerdirectional.h
		parametersamplerspatial.cpp
		parametersamplerspatial.h
		parametersamplerlocomotion.cpp
		parametersamplerlocomotion.h
		parametersamplercombiner.cpp
		parametersamplercombiner.h
	}
	Folder Factory {
		distancegrid.cpp
		distancegrid.h
		pointcloudcache.cpp
		pointcloudcache.h
		slope.h
		timealignment.cpp
		timealignment.h
		dynamicprogramming.cpp
		dynamicprogramming.h
		registrationcurvecalculator.cpp
		registrationcurvecalculator.h
		blenddatabasecalculator.cpp
		blenddatabasecalculator.h
	}
	Folder Refactor {
		parametertranslator.cpp
		parametertranslator.h
		parameterizedmotioncomposite.cpp
		parameterizedmotioncomposite.h
	}
}
