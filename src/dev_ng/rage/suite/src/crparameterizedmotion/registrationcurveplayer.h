//
// crparameterizedmotion/registrationcurveplayer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRPARAMETERIZEDMOTION_REGISTRATIONCURVEPLAYER_H
#define CRPARAMETERIZEDMOTION_REGISTRATIONCURVEPLAYER_H

#include "pointcloudtransform.h"
#include "weights.h"

namespace rage
{

class crClips;
class crFrame;
class crFrameCompositor;
class crpmRegistrationCurveData;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: This class is responsible for playing a registration curve and its associated set of clips
class crpmRegistrationCurvePlayer
{
public:

	// PURPOSE: Default constructor
	crpmRegistrationCurvePlayer();

	// PURPOSE: Pre-initializing constructor
	crpmRegistrationCurvePlayer(int worstNumClips);

	// PURPOSE: Initializing constructor
	crpmRegistrationCurvePlayer(const crpmRegistrationCurveData& rcData, const crClips& clips, const crpmWeights& weights);

	// PURPOSE: Destructor
	~crpmRegistrationCurvePlayer();

	// PURPOSE: Pre-initialize
	// NOTES: Reserves worst case storage needed for later initialization
	void PreInit(int worstNumClips);

	// PURPOSE: Initializer
	void Init(const crpmRegistrationCurveData& rcData, const crClips& clips, const crpmWeights& weights);

	// PURPOSE: Clear
	void Clear();

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Reset
	void Reset();

	// PURPOSE: Reset with weights
	void Reset(const crpmWeights& weights);

	// PURPOSE: Update using delta time
	void Update(float deltaTime, const crpmWeights& weights);

	// PURPOSE: Update using delta registration curve phase
	void UpdateU(float deltaU, const crpmWeights& weights);
	
	// PURPOSE: Set registration curve phase
	void SetU(float u, const crpmWeights& weights);

	// PURPOSE: Get registration curve phase
	float GetU() const;

	// PURPOSE: Set registration curve delta phase
	void SetDeltaU(float deltaU);

	// PURPOSE: Get registration curve delta phase
	float GetDeltaU() const;

	// PURPOSE: Set registration curve post delta phase
	void SetPostDeltaU(float deltaU);

	// PURPOSE: Get registration curve post delta phase
	float GetPostDeltaU() const;

	// PURPOSE: Composite current pose using a compositor
	void Composite(crFrameCompositor& inoutCompositor) const;

	// PURPOSE: Composite a frame with the current pose
	void Composite(crFrame& outFrame) const;

	// PURPOSE: Has the player finished (reached the end of a non-looped registration curve)
	bool IsFinished() const;

	// PURPOSE: Set looped status (overrides default value in registration curve data)
	void SetLooped(bool looped);

	// PURPOSE: Is registration curve player looped?
	bool IsLooped() const;

	// PURPOSE: Get remaining time
	float GetRemainingTime() const;

	// PURPOSE: Get total duration
	float GetDuration() const;

	// PURPOSE: Get maximum clip duration
	float GetMaxDuration() const;

	// PURPOSE: Calculate delta registration curve phase from delta time
	float CalcDeltaU(float deltaTime, const crpmWeights& weights) const;

	// PURPOSE: Calculate delta time from delta registration curve phase
	float CalcDeltaTime(float deltaU, const crpmWeights& weights) const;

	// PURPOSE: Calculate total time for set of weights
	float CalcTotalTime(const crpmWeights& weights) const;

	// PURPOSE: Get registration curve data
	const crpmRegistrationCurveData* GetRegistrationCurve() const;

	// PURPOSE: Get clips
	const crClips* GetClips() const;

protected:

	// PURPOSE: NOT SURE?
	void UpdateInternal(const crpmWeights& weights);

	// PURPOSE: Calculate a delta in registration curve phase, given a delta time and weights
	// NOTES: Calculates a time in each clip, advances it by the supplied delta time,
	// then converts it back into a new registration curve phase.
	// A weighted mean is performed on all the new phases, to determine the final delta phase
	float CalcDeltaU(float u, float deltaTime, const crpmWeights& weights, const crClips& clips) const;


	const crpmRegistrationCurveData* m_RcData;
	const crClips* m_Clips;
	crpmWeights m_Weights;
	float m_U;
	float m_DeltaU;
	float m_PostDeltaU;
	u32 m_LoopCount;

	bool m_IsLooped;
};

////////////////////////////////////////////////////////////////////////////////

inline float crpmRegistrationCurvePlayer::GetU() const
{
	return m_U;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmRegistrationCurvePlayer::IsFinished() const
{
	return !m_IsLooped && (m_U == 1.f);
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmRegistrationCurvePlayer::SetLooped(bool looped)
{
	m_IsLooped = looped;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmRegistrationCurvePlayer::IsLooped() const
{
	return m_IsLooped;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmRegistrationCurveData* crpmRegistrationCurvePlayer::GetRegistrationCurve() const
{
	return m_RcData;
}

////////////////////////////////////////////////////////////////////////////////

inline const crClips* crpmRegistrationCurvePlayer::GetClips() const
{
	return m_Clips;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_REGISTRATIONCURVEPLAYER_H


