//
// crparameterizedmotion/pointcloud.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "pointcloud.h"

#include "pointcloudtransform.h"

#include "atl/array_struct.h"
#include "cranimation/frame.h"
#include "cranimation/weightset.h"
#include "crskeleton/bonedata.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"

#if CR_DEV
#include "crmetadata/dumpoutput.h"
#endif // CR_DEV

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmPointCloud::crpmPointCloud()
: m_TotalWeight(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmPointCloud::crpmPointCloud(datResource& rsc)
: m_Points(rsc, true)
, m_Weights(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmPointCloud::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crpmPointCloud);
	STRUCT_FIELD(m_Points);
	STRUCT_FIELD(m_Weights);
	STRUCT_FIELD(m_Bound);
	STRUCT_FIELD(m_TotalWeight);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmPointCloud);

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloud::AddPose(const crSkeleton& skeleton, const crWeightSet* weightSet)
{
	const int numBones = skeleton.GetSkeletonData().GetNumBones();
	for(int i=0; i<numBones; ++i)
	{
		float weight = 1.f;
		if(weightSet)
		{
			weight = weightSet->GetAnimWeight(i);
		}

		if(weight > 0.f)
		{
			Mat34V globalMtx;
			skeleton.GetGlobalMtx(i, globalMtx);
			AddPoint(globalMtx.GetCol3(), weight);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloud::AddPose(const crSkeleton& skeleton, const f32* weights)
{
	const int numBones = skeleton.GetSkeletonData().GetNumBones();
	for(int i=0; i<numBones; ++i)
	{
		float weight = weights[i];
		if(weight > 0.f)
		{
			Mat34V globalMtx;
			skeleton.GetGlobalMtx(i, globalMtx);
			AddPoint(globalMtx.GetCol3(), weight);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloud::AddFrame(const crFrame& frame, const crSkeletonData& skelData, const crWeightSet* weightSet)
{
	crSkeleton skel;
	skel.Init(skelData);

	frame.Pose(skel);

	skel.Update();

	AddPose(skel, weightSet);
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloud::Append(const crpmPointCloud& other)
{
	for(int i=0; i<other.m_Points.GetCount(); ++i)
	{
		AddPoint(other.m_Points[i], other.m_Weights[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloud::Transform(Mat34V_In transform)
{
	m_Bound.Reset();

	for(int i=0; i<m_Points.GetCount(); ++i)
	{
		crpmPointType& point = m_Points[i];
		point = rage::Transform(transform, point);

		m_Bound.Adjust(point);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmPointCloud::IsClose(const crpmPointCloud& other, float tolerance) const
{
	AssertMsg(m_Points.size() == other.m_Points.size() , "crpmPointCloud::IsClose - point clouds have to be the same size");

	for(int i=0; i<m_Points.GetCount(); ++i)
	{
		const crpmPointType& thisPoint = m_Points[i];
		const crpmPointType& otherPoint = other.m_Points[i];

		if(!rage::IsCloseAll(thisPoint, otherPoint, ScalarVFromF32(tolerance)))
		{
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

float crpmPointCloud::ComputeDistance(const crpmPointCloud& other, crpmPointCloudTransform& distanceTransform, float earlyOutDistance) const
{
	AssertMsg(m_Points.size() == other.m_Points.size() , "crpmPointCloud::ComputeDistance - point clouds must have equal size");
	AssertMsg(!m_Points.empty() , "crpmPointCloud::ComputeDistance - point clouds can not be empty");
	AssertMsg(rage::IsClose(m_TotalWeight, other.m_TotalWeight) , "crpmPointCloud::ComputeDistance - point clouds must have identical weights");

	// get the transform needed to transform the other PC into the same "space" as this PC
	distanceTransform.Calculate(*this, other);

	return ComputeDistanceGivenTransform(other, distanceTransform, earlyOutDistance);
}

////////////////////////////////////////////////////////////////////////////////

float crpmPointCloud::ComputeDistanceGivenTransform(const crpmPointCloud& other, const crpmPointCloudTransform& transform, float earlyOutDistance) const
{
	AssertMsg(m_Points.size() == other.m_Points.size() , "crpmPointCloud::ComputeDistanceGivenTransform - pointclouds must have equal size");
	AssertMsg(!m_Points.empty() , "crpmPointCloud::ComputeDistanceGivenTransform - point clouds can not be empty");
	AssertMsg(rage::IsClose(m_TotalWeight, other.m_TotalWeight) , "crpmPointCloud::ComputeDistanceGivenTransform - point clouds must have identical total weights");

	Assert(m_TotalWeight > 0.f);
	float invTotalWeight = 1.f/m_TotalWeight;

	const bool skipTransform = transform.IsIdentity();
	const bool isYUpAxis = IsYUpAxis();

	ScalarV angle = ScalarVFromF32(transform.GetAngle());

	float distSum = 0.f;
	for (int i=0; i<m_Points.GetCount() && distSum < earlyOutDistance; i++)
	{
		AssertMsg(rage::IsClose(m_Weights[i], other.m_Weights[i]) , "crpmPointCloud::ComputeDistanceGivenTransform - point clouds must have identical individual weights");

		const crpmPointType& thisPoint = m_Points[i];
		crpmPointType otherPoint = other.m_Points[i];

		if(!skipTransform)
		{
			if(isYUpAxis)
			{
				otherPoint = RotateAboutYAxis( otherPoint, angle );
			}
			else
			{
				otherPoint = RotateAboutZAxis( otherPoint, angle );
			}
			otherPoint += transform.GetTranslation();
		}

		crpmPointType diff = thisPoint - otherPoint;

		float len2 = MagSquared(diff).Getf();

		distSum += m_Weights[i] * invTotalWeight * len2;
	}

	return distSum;
}

////////////////////////////////////////////////////////////////////////////////

float crpmPointCloud::ComputeDistanceUntransformed(const crpmPointCloud& other, float earlyOutDistance) const
{
	return ComputeDistanceGivenTransform(other, crpmPointCloudTransform::IDENTITY, earlyOutDistance);
}

////////////////////////////////////////////////////////////////////////////////

crpmPointType crpmPointCloud::ComputeMeanPoint() const
{
	crpmPointType mean(V_ZERO);

	if(!m_Points.empty())
	{
		float invTotalWeight = 1.f/m_TotalWeight;

		for(int i=0; i<m_Points.GetCount(); ++i)
		{
			mean += m_Points[i] * ScalarVFromF32(m_Weights[i] * invTotalWeight);
		}
	}

	return mean;
}

////////////////////////////////////////////////////////////////////////////////

float crpmPointCloud::ComputeLargestDistance(const crpmPointCloud& other, int& outMaxIndex) const
{
	float maxDistance = -FLT_MAX;

	for (int i = 0; i < m_Points.GetCount(); i++)
	{
		crpmPointType thisPoint = m_Points[i];
		crpmPointType otherPoint = other.m_Points[i];

		crpmPointType diff = thisPoint - otherPoint;

		float len = Mag(diff).Getf();

		if (len > maxDistance)
		{
			maxDistance = len;
			outMaxIndex = (int)i;
		}
	}

	return maxDistance;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crpmPointCloud::Dump(const char* header) const
{
	Displayf("%s\n", header);
	for(int i=0; i<m_Points.GetCount(); ++i)
	{
		DumpPoint(i);
	}
}

void crpmPointCloud::Dump(crDumpOutput& output) const
{
	int numPoints = int(m_Points.size());
	output.Outputf(0, "num points", "%d", numPoints);

	output.PushLevel(NULL);

	for (int i=0; i<numPoints; ++i)
	{
		const crpmPointType& p = m_Points[i];
		output.Outputf(0, "point", "%4d:\t%e\t%e\t%e", i, p[0], p[1], p[2]);
	}

	output.PopLevel();

	int numWeights = int(m_Weights.size());
	output.Outputf(0, "num weights", "%d", numWeights);

	output.PushLevel(NULL);

	for (int i=0; i<numWeights; ++i)
	{
		output.Outputf(0, "weight", "%f", m_Weights[i]);
	}
	
	output.PopLevel();

	const crpmPointType& minPoint = m_Bound.m_MinPoint;
	output.Outputf(0, "min bound", "%e\t%e\t%e", minPoint[0], minPoint[1], minPoint[2]);

	const crpmPointType& maxPoint = m_Bound.m_MaxPoint;
	output.Outputf(0, "max bound", "%e\t%e\t%e", maxPoint[0], maxPoint[1], maxPoint[2]);

	output.Outputf(0, "total weight", "%f", m_TotalWeight);
}

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloud::DumpPoint(size_t index) const
{
	AssertMsg(int(index)<int(size()) , "crpmPointCloud::DumpPoint - index out of range");

	const crpmPointType& p = m_Points[int(index)];
	Displayf("%4d:\t%e\t%e\t%e\n", int(index), p[0], p[1], p[2]);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloud::Serialize(datSerialize& s)
{
	int numPoints = (int) m_Points.size();
	s << numPoints;

	if (s.IsRead())
	{
		m_Points.Resize(numPoints);
	}

	for (int i=0; i<numPoints; ++i)
	{
		s << m_Points[i];
	}

	int numWeights = (int) m_Weights.size();
	s << numWeights;

	if (s.IsRead())
	{
		m_Weights.Resize(numWeights);
	}

	for (int i=0; i<numWeights; ++i)
	{
		s << m_Weights[i];
	}

	s << m_Bound;
	s << m_TotalWeight;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmPointCloud::PointCloudBound::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(PointCloudBound);
	STRUCT_FIELD(m_MinPoint);
	STRUCT_FIELD(m_MinPoint);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmPointCloud::PointCloudBound::Serialize(datSerialize& s)
{
	s << m_MinPoint;
	s << m_MaxPoint;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


