//
// crparameterizedmotion/angle.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "angle.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmAngle::crpmAngle(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmAngle::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crpmAngle);
	STRUCT_FIELD(m_Radians);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmAngle::MakePositive()
{
	if(m_Radians < 0.f)
	{
		float angle = CanonicalizeAngle(m_Radians);
		m_Radians = Selectf( angle, angle, angle + 2.f*PI );
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmAngle::Lerp(float t, const crpmAngle& a1, const crpmAngle& a2)
{
	float rad1 = a1.GetRadiansMinusPiToPiX();
	float rad2 = a2.GetRadiansMinusPiToPiX();

	float diffRad = rad2 - rad1;
	diffRad = ConvertRadians<true, true>(diffRad, -PI, PI);

	m_Radians = rage::Lerp(t, rad1, rad2 + diffRad);
}

////////////////////////////////////////////////////////////////////////////////

void crpmAngle::Approach(const crpmAngle& goal, float radiansPerSecond, float deltaTime)
{
	float currRad = GetRadiansMinusPiToPiX();
	float goalRad = goal.GetRadiansMinusPiToPiX();

	float diffRad = ConvertRadians<true, true>(goalRad - currRad, -PI, PI);

	rage::Approach(currRad, currRad + diffRad, radiansPerSecond, deltaTime);

	m_Radians = currRad;
}

////////////////////////////////////////////////////////////////////////////////

void crpmAngle::FromMatrix(Mat34V_In mtx)
{
	Vec3V eulers;

	if(IsYUpAxis())
	{
		eulers = Mat33VToEulersYZX(mtx.GetMat33());
		SetRadians(eulers[1]);
	}
	else
	{
		eulers = Mat33VToEulersZYX(mtx.GetMat33());
		SetRadians(eulers[2]);
	}

	// convert negative angles to positive ones
	MakePositive();

	AssertMsg((GetRadians()>=0.0f) && (GetRadians()<=(2.0f*PI)), "crpmAngle::FromMatrix - gives invalid angle");
}

////////////////////////////////////////////////////////////////////////////////

void crpmAngle::ToMatrix(Mat34V_InOut outMtx) const
{
	if(IsYUpAxis())
	{
		Mat34VFromYAxisAngle(outMtx, ScalarVFromF32(GetRadians()), outMtx.GetCol3());
	}
	else
	{
		Mat34VFromZAxisAngle(outMtx, ScalarVFromF32(GetRadians()), outMtx.GetCol3());
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
