//
// crparameterizedmotion/parameterizedmotionvariable.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONVARIABLE_H
#define CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONVARIABLE_H

#include "parameterizedmotionsimple.h"

#include "atl/map.h"
#include "data/struct.h"
#include "string/stringhash.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Variable parameterized motion data
class crpmParameterizedMotionVariableData : public crpmParameterizedMotionSimpleData
{
public:

	// PURPOSE: Default constructor
	crpmParameterizedMotionVariableData();

	// PURPOSE: Resource constructor
	crpmParameterizedMotionVariableData(datResource&);

	// PURPOSE: Initializing constructor
	// NOTES: Parameterized motion will take ownership of clips, registration curve and blend database
	// It will destroy these objects on shutdown/destruction of the parameterized motion data
	crpmParameterizedMotionVariableData(const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags=NULL, const crProperties* properties=NULL);

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotionVariableData();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmParameterizedMotionVariableData);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initializer, used to create variable data
	// NOTES: Parameterized motion will take ownership of clips, registration curve and blend database
	// It will destroy these objects on shutdown/destruction of the parameterized motion data
	void Init(const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags=NULL, const crProperties* properties=NULL);

	// PURPOSE: Shutdown
	void Shutdown();

#if CR_DEV
	// PURPOSE: Load from file
	// PARAMS:
	// filename - .vpm filename
	// clipLoader - clip loader
	// animLoader - animation loader
	// RETURNS: true - load successful, false - load failed
	virtual bool Load(const char* filename, crClipLoader* clipLoader=NULL, crAnimLoader* animLoader=NULL);

	// PURPOSE: Allocate and load from file
	// PARAMS:
	// filename - .vpm filename
	// clipLoader - clip loader
	// animLoader - animation loader
	// RETURNS: If successful returns pointer to newly allocated and loaded variable data, otherwise NULL.
	static crpmParameterizedMotionVariableData* AllocateAndLoad(const char* filename, crClipLoader* clipLoader=NULL, crAnimLoader* animLoader=NULL);

	// PURPOSE: Save to file
	// PARAMS: filename - .vpm filename
	// RETURNS: true - save successful, false - save failed
	virtual bool Save(const char* filename) const;

	// PURPOSE: Serialize
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump debug output
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Add a new variation
	int AddVariation(const char* name, int numOptions, int parentVariation, int parentOption);

	// PURPOSE: Add a clip to a variation option
	bool AddVariationClip(int clipIdx, int variation, int option);


	// PURPOSE: Variation key (hash key used to retrieve variations)
	typedef u32 VariationKey;

	// PURPOSE: Calculate variation key from string
	static VariationKey CalcVariationKey(const char* name);


	// PURPOSE: Get number of variations
	int GetNumVariations() const;

	// PURPOSE: Get number of options
	int GetNumOptions(int variation) const;

	// PURPOSE: Find variation index using a name
	int GetIndexByName(const char* name) const;

	// PURPOSE: Find variation index using a hash key
	int GetIndexByKey(VariationKey key) const;


protected:
	
	// PURPOSE: Variation
	struct Variation
	{
		// PURPOSE: Default constructor
		Variation();

		// PURPOSE: Resource placement
		Variation(datResource&);

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(crpmParameterizedMotionVariableData::Variation);

		// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		// PURPOSE: Shutdown
		void Shutdown();

		// PURPOSE: Serialization
		void Serialize(datSerialize& s, atArray<Variation>& variations);

		atString m_Name;
		int m_Index;

		struct Option
		{
			// PURPOSE: Default constructor
			Option();

			// PURPOSE: Resource placement
			Option(datResource&);

			// PURPOSE: Placement
			IMPLEMENT_PLACE_INLINE(crpmParameterizedMotionVariableData::Variation::Option);

			// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
			void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

			// PURPOSE: Shutdown
			void Shutdown();

			// PURPOSE: Serialization
			void Serialize(datSerialize& s, atArray<Variation>& variations);

			atArray<int> m_Clips;
			atArray<datRef<Variation> > m_Variations;
		};

		atArray<Option> m_Options;
	};

	// PURPOSE: Internal function, fixup resource data
	static void FixupDataFunc(datResource &rsc, datRef<Variation>& data);

	atArray<Variation> m_Variations;
	atMap<VariationKey, datRef<Variation> > m_VariationsMap;

	static const u8 sm_FileVersions[];
};

////////////////////////////////////////////////////////////////////////////////

extern __THREAD int s_PmVariableSerializeVersion;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Variable parameterized motion instance
class crpmParameterizedMotionVariable : public crpmParameterizedMotionSimple
{
public:

	// PURPOSE: Default constructor
	crpmParameterizedMotionVariable();

	// PURPOSE: Pre-initializing constructor
	crpmParameterizedMotionVariable(int worstNumClips, int worstNumParameterDimensions, int worstNumVariations);

	// PURPOSE: Initializing constructor
	crpmParameterizedMotionVariable(const crpmParameterizedMotionVariableData& pmVariableData);

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotionVariable();

	// PURPOSE: Pre-initialize
	// NOTES: Reserves worst case storage needed for later initialization
	void PreInit(int worstNumClips, int worstNumParameterDimensions, int worstNumVariations);

	// PURPOSE: Initializer
	void Init(const crpmParameterizedMotionVariableData& pmVariableData);

	// PURPOSE: Clear
	// NOTES: Detaches instance from data, but doesn't shutdown dynamic storage
	void Clear();

	// PURPOSE: Shutdown
	virtual void Shutdown();


	// PURPOSE: Update override
	virtual void Update(float rcDeltaTime, float parameterDeltaTime);


	// PURPOSE: Get variation option by index, name or key
	int GetVariationOptionByIndex(int variationIdx);
	int GetVariationOptionByName(const char* variationName);
	int GetVariationOptionByKey(crpmParameterizedMotionVariableData::VariationKey variationKey);

	// PURPOSE: Set variation
	void SetVariationOptionByIndex(int variationIdx, int newOption);
	void SetVariationOptionByName(const char* variationName, int newOption);
	void SetVariationOptionByKey(crpmParameterizedMotionVariableData::VariationKey variationKey, int newOption);


	// PURPOSE: Get variable parameterized motion type data
	const crpmParameterizedMotionVariableData* GetVariableData() const;

protected:

	// PURPOSE: Internal function, calculate variation field
	void CalcVariations(crpmVariations& outVariations) const;

	// PURPOSE: Internal function, calculate weights from current parameters
	virtual void CalcWeights(crpmWeights& outWeights) const;

private:

	atArray<int> m_VariationOptions;
	crpmVariations m_Variations;
};

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotionVariableData::VariationKey crpmParameterizedMotionVariableData::CalcVariationKey(const char* name)
{
	return VariationKey(atStringHash(name));
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmParameterizedMotionVariableData::GetNumVariations() const
{
	return m_Variations.GetCount()-1;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmParameterizedMotionVariableData::GetNumOptions(int variation) const
{
	return m_Variations[variation+1].m_Options.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmParameterizedMotionVariableData::GetIndexByName(const char* name) const
{
	return GetIndexByKey(CalcVariationKey(name));
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmParameterizedMotionVariableData::GetIndexByKey(VariationKey key) const
{
	Variation* variation = *m_VariationsMap.Access(key);
	if(variation)
	{
		return variation->m_Index-1;
	}
	return -1;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmParameterizedMotionVariable::GetVariationOptionByIndex(int variationIdx)
{
	return m_VariationOptions[variationIdx];
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmParameterizedMotionVariable::GetVariationOptionByName(const char* variationName)
{
	const int variationIdx = GetVariableData()->GetIndexByName(variationName);
	if(variationIdx >=0)
	{
		return m_VariationOptions[variationIdx];
	}
	return -1;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmParameterizedMotionVariable::GetVariationOptionByKey(crpmParameterizedMotionVariableData::VariationKey variationKey)
{
	const int variationIdx = GetVariableData()->GetIndexByKey(variationKey);
	if(variationIdx >=0)
	{
		return m_VariationOptions[variationIdx];
	}
	return -1;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotionVariable::SetVariationOptionByIndex(int variationIdx, int newOption)
{
	m_VariationOptions[variationIdx] = newOption;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotionVariable::SetVariationOptionByName(const char* variationName, int newOption)
{
	const int variationIdx = GetVariableData()->GetIndexByName(variationName);
	if(variationIdx >=0)
	{
		m_VariationOptions[variationIdx] = newOption;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotionVariable::SetVariationOptionByKey(crpmParameterizedMotionVariableData::VariationKey variationKey, int newOption)
{
	const int variationIdx = GetVariableData()->GetIndexByKey(variationKey);
	if(variationIdx >=0)
	{
		m_VariationOptions[variationIdx] = newOption;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterizedMotionVariableData* crpmParameterizedMotionVariable::GetVariableData() const
{
	return static_cast<const crpmParameterizedMotionVariableData*>((const crpmParameterizedMotionData*)m_Data);
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONVARIABLE_H
