//
// crparameterizedmotion/pointcloud.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_POINTCLOUD_H
#define CRPARAMETERIZEDMOTION_POINTCLOUD_H

#include "atl/array.h"
#include "cranimation/animation_config.h"
#include "vectormath/classes.h"

#include <float.h>		// for FLT_EPSILON

namespace rage
{

// forward declarations
class crFrame;
class crWeightSet;
class crBoneData;
class crSkeleton;
class crSkeletonData;
class crpmPointCloudTransform;

#if CR_DEV
class crDumpOutput;
#endif // CR_DEV

// PURPOSE: Type definition for point within a cloud
typedef Vec3V crpmPointType;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:  Represents a "cloud" of points in 3D.
class crpmPointCloud
{
public:

	// PURPOSE: Default constructor
	crpmPointCloud();

	// PURPOSE: Resource constructor
	crpmPointCloud(datResource&);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

	// PURPOSE: Placement
	DECLARE_PLACE(crpmPointCloud);

	// PURPOSE:  Add point to point cloud
	// PARAMS: point - point to add
	// weight - weight to add this point with
	void AddPoint(const crpmPointType& point, float weight=1.f);

	// PURPOSE: Add pose from skeleton to the point cloud
	// PARAMS: skeleton - posed skeleton (make sure Update has been called to set global matrices)
	// weightSet - optionally used to filter/bias particular bones (default NULL)
	void AddPose(const crSkeleton& skeleton, const crWeightSet* weightSet=NULL);

	// PURPOSE: Add pose from array of weights
	void AddPose(const crSkeleton& skeleton, const f32* weights);

	// PURPOSE: Add frame to the point cloud
	// PARAMS: frame - animation frame to add
	// skelData - skeleton data to use as template for constructing temporary skeleton to pose
	// weightSet - optionally used to filter/bias particular bones (default NULL)
	// NOTES: Internally will construct a skeleton, pose and update it, then call AddPose.
	// Not very efficient for repeated use (lots of wasted effort in skeleton construction/destruction)
	// Better to externally construct skeleton once, and call AddPose directly instead.
	void AddFrame(const crFrame& frame, const crSkeletonData& skelData, const crWeightSet* weightSet=NULL);

	// PURPOSE:  Add all the points from another point cloud
	void Append(const crpmPointCloud& other);


	// PURPOSE: Transform all points in the point cloud by the supplied transform
	// PARAMS: transform - transformation matrix
	void Transform(Mat34V_In transform);


	// PURPOSE: Get a point from the point cloud by index.
	// NOTES: This version returns a const point
	const crpmPointType& GetPoint(int index) const;

	// PURPOSE: Get a point from the point cloud by index.
	// NOTES: This version returns a modifiable point (not const)
	crpmPointType& GetPoint(int index);

	// PURPOSE: Get weight of a particular point
	// RETURNS: point weight (>= 0.0)
	float GetPointWeight(int index) const;

	// PURPOSE: Get accumulation of all point weights
	// RETURNS: total weight of all points
	// NOTES: Cached, fast to retrieve
	float GetTotalPointWeights() const;


	// PURPOSE:  Does another point cloud have approximately the same points in it?
	// PARAMS: other - other point cloud
	// tolerance - comparison tolerance (default FLT_EPSILON)
	// NOTES: Will assert if point clouds are of unequal size
	bool IsClose(const crpmPointCloud& other, float tolerance=FLT_EPSILON) const;


	// PURPOSE: Compute the minimum average squared-distance between corresponding points given
	// that the other point cloud can be translated along the ground plane and rotated about the
	// up axis.
	float ComputeDistance(const crpmPointCloud& other, crpmPointCloudTransform& distanceTransform, float earlyOutDistance=FLT_MAX) const;

	// PURPOSE: Compute the average squared-distance between corresponding points given
	// that points in the other point cloud are transformed by the supplied transform
	float ComputeDistanceGivenTransform(const crpmPointCloud& other, const crpmPointCloudTransform& transform, float earlyOutDistance=FLT_MAX) const;

	// PURPOSE: Compute the average squared-distance between corresponding points.
	float ComputeDistanceUntransformed(const crpmPointCloud& other, float earlyOutDistance=FLT_MAX) const;

	// PURPOSE: Calculate the average of the coordinates of all points in this point cloud
	crpmPointType ComputeMeanPoint() const;

	// PURPOSE: Given another point cloud, calculate the largest distance between
	// corresponding points
	float ComputeLargestDistance(const crpmPointCloud& other, int& outMaxIndex) const;

#if CR_DEV
	// PURPOSE: Output information about this point cloud for debugging
	// PARAMS: header - string to prefix to debugging output
	void Dump(const char* header) const;

	// PURPOSE: Output information about this point cloud for debugging
	// PARAMS: output - dump object
	void Dump(crDumpOutput& output) const;

	// PURPOSE: Output information about a specific point for debugging
	// PARAMS:  index - index of point
	void DumpPoint(::size_t index) const;
#endif // CR_DEV

	// PURPOSE: STL vector compatibility
	typedef atArray<crpmPointType>::size_type size_type;

	// PURPOSE: STL vector compatibility
	typedef atArray<crpmPointType>::const_iterator const_iterator;

	// PURPOSE: STL vector compatibility
	const_iterator begin() const;

	// PURPOSE: STL vector compatibility
	const_iterator end() const;

	// PURPOSE: Returns number of points contained within point cloud (STL vector compatibility)
	size_type size() const;

	// PURPOSE: STL vector compatibility
	void reserve(size_type _Count);

	// PURPOSE: Serialize
	void Serialize(datSerialize& s);

	// PURPOSE: Axis aligned bounding box, used to contain all points within point cloud
	struct PointCloudBound
	{
		// PURPOSE: Construct an empty bounding box
		PointCloudBound();

		// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif //__DECLARESTRUCT

		// PURPOSE:  If necessary, adjust bounding box to include point
		// PARAMS:  point - point to include
		void Adjust(const crpmPointType& point);

		// PURPOSE:  Empty the bounding box
		void Reset();

		// PURPOSE:  Equality operator
		bool operator==(const PointCloudBound& other) const;

		// PURPOSE:  Not Equal Operator
		bool operator!=(const PointCloudBound& other) const;

		// PURPOSE:  Does the bounding box contain a point?
		// PURPOSE: true - if not empty, false - if empty
		bool IsSet() const;

		// PURPOSE: Serialize
		void Serialize(datSerialize& s);
	
		// PUROSE: Bounding box dimensions, inclusive
		crpmPointType m_MinPoint;
		crpmPointType m_MaxPoint;
	};

	// PURPOSE: Get point cloud bounding box
	const PointCloudBound& GetBound() const;


protected:

	// PURPOSE: Points in the point cloud
	atArray< crpmPointType > m_Points;

	// PURPOSE: Point weights
	atArray< float > m_Weights;

	// PURPOSE: Bounding box of all points in the point cloud
    PointCloudBound m_Bound;

	// PURPOSE: Accumulation of all point weights
	float m_TotalWeight;
};


// inline functions

////////////////////////////////////////////////////////////////////////////////

inline void crpmPointCloud::AddPoint(const crpmPointType& point, float weight)
{
	FastAssert(IsFiniteAll(point));
	FastAssert(weight >= 0.f);

	m_Bound.Adjust(point);
	m_TotalWeight += weight;

	m_Points.PushAndGrow(point);
	m_Weights.PushAndGrow(weight);
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmPointType& crpmPointCloud::GetPoint(int index) const
{
	return m_Points[index];
}

////////////////////////////////////////////////////////////////////////////////

inline crpmPointType& crpmPointCloud::GetPoint(int index)
{
	return m_Points[index];
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmPointCloud::GetPointWeight(int index) const
{
	return m_Weights[index];
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmPointCloud::GetTotalPointWeights() const
{
	return m_TotalWeight;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmPointCloud::const_iterator crpmPointCloud::begin() const
{
	return m_Points.begin();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmPointCloud::const_iterator crpmPointCloud::end() const
{
	return m_Points.end();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmPointCloud::size_type crpmPointCloud::size() const
{
	return m_Points.size();
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmPointCloud::reserve(crpmPointCloud::size_type _Count)
{
	m_Points.Reserve(_Count);
	m_Weights.Reserve(_Count);
}

////////////////////////////////////////////////////////////////////////////////

inline crpmPointCloud::PointCloudBound::PointCloudBound()
{
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmPointCloud::PointCloudBound::Adjust(const crpmPointType& point)
{
	m_MinPoint = Min(m_MinPoint, point);
	m_MaxPoint = Max(m_MaxPoint, point);
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmPointCloud::PointCloudBound::Reset()
{
	m_MinPoint = Vec3V(V_FLT_MAX);
	m_MaxPoint = Vec3V(V_NEG_FLT_MAX);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmPointCloud::PointCloudBound::operator==(const PointCloudBound& other) const
{
	return IsEqualAll(m_MinPoint, other.m_MinPoint) && IsEqualAll(m_MaxPoint, other.m_MaxPoint);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmPointCloud::PointCloudBound::operator!=(const PointCloudBound& other) const
{
	return (!operator==(other));
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmPointCloud::PointCloudBound::IsSet() const
{
	return IsLessThanOrEqualAll(m_MinPoint, m_MaxPoint)!=0;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmPointCloud::PointCloudBound& crpmPointCloud::GetBound() const
{
	return m_Bound;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_POINTCLOUD_H
