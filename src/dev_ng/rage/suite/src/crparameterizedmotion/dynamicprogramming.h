//
// crparameterizedmotion/dynamicprogramming.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_DYNAMICPROGRAMMING_H
#define CRPARAMETERIZEDMOTION_DYNAMICPROGRAMMING_H

#include "distancegrid.h"
#include "slope.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Dynamic programming is used to search distance grid for optimal bridges (paths)
class crpmDynamicProgramming
{
public:

	// PURPOSE: Default constructor
    crpmDynamicProgramming();

	// PURPOSE: Initializing constructor
	// grid - distance grid to search
	// slopeLimit - slope limit (will be automatically adjusted if impossible to satisfy)
	// minX, minY, maxX, maxY - search a subsection of the grid (default values search entire grid)
	crpmDynamicProgramming(const crpmDistanceGrid& grid, int slopeLimit, int minX=0, int minY=0, int maxX=-1, int maxY=-1);

	// PURPOSE: Initializer
	// PARAMS:
	// grid - distance grid to search
	// slopeLimit - slope limit (will be automatically adjusted if impossible to satisfy)
	// minX, minY, maxX, maxY - search a subsection of the grid (default values search entire grid)
	void Init(const crpmDistanceGrid& grid, int slopeLimit, int minX=0, int minY=0, int maxX=-1, int maxY=-1);


    // PURPOSE: Find best path from any point on source path, to any point on destination path
	// (without removing any overlaps with source or destination paths)
	// PARAMS: srcPath, dstPath - source and destination paths
	// RETURNS: true - successful (use GetBestPath to retrieve path), false - failed to find valid path
    bool FindBestPath(const crpmDistanceGrid::Path& srcPath, const crpmDistanceGrid::Path& dstPath);

    // PURPOSE: Find best bridge from any point on source path, to any point on destination path
	// (with no overlap with source or destination paths)
	// PARAMS: srcPath, dstPath - source and destination paths
	// RETURNS: true - successful (use GetBestPath to retrieve path), false - failed to find valid path
    bool FindBestBridge(const crpmDistanceGrid::Path& srcPath, const crpmDistanceGrid::Path& dstPath);

    // PURPOSE: Find best bridge from lower left to upper right corners of grid
	// NOTES: Always succeeds, use GetBestPath to retrieve path
    void FindBestBridge();


	// PURPOSE: Get the best path
	// RETURNS: Reference to best path (or bridge)
	// NOTES: Undefined if FindBestBridge/Path not called, or returned false.
    const crpmDistanceGrid::Path& GetBestPath() const;


	// PURPOSE: Get extent of between min and max limits
	int GetSizeX() const;

	// PURPOSE: Get extent of between min and max limits
	int GetSizeY() const;


	// PURPOSE: Render image for debugging
	void MakeDynamicProgrammingImage(crpmImage& outImage) const;

private:

	// PURPOSE: Internal path iterator, used for updating cell values in the DP algorithm
	class PathIterator
	{
	public:

		// PURPSOE: Default constructor
		PathIterator() {}

		// PURPOSE: Constructor
		PathIterator(int x, int y, const crpmSlope& slopeCounter, float sum, int length);

		// PURPSOE: Get path value
		float GetValue() const;

		// PURPOSE: For sorting cheapest paths first
		bool operator<(const PathIterator& other) const;

		int m_X;
		int m_Y;
		crpmSlope m_SlopeCounter; // monotonicity counter to check for degenerate slopes
		float m_Sum; // current total value of the path
		int m_Length; // length of the path
	};
	
	// PURPOSE: Internal representation of a grid cell, used to store DP algorithm intermediate state
	class Cell
	{
	public:

		// PURPOSE: Get the current lowest sum
		float GetSum() const;

		// PURPOSE: Try and add a new sum slope counter pair
		bool TryAddSumSlopeCounterPair(float candidateSum, const crpmSlope& candidateSlopeCounter);

		// PURPOSE: Reset cell
		void Reset();

	private:

		// PURPOSE: Internal representation of a sum slope counter pair
		class Pair
		{
		public:

			// PURPOSE: Default constructor
			Pair();

			// PURPOSE Initializing constructor
			Pair(float sum, const crpmSlope& slopeCounter);

			float m_Sum;
			crpmSlope m_SlopeCounter;
		};

		atArray<Pair> m_Pairs;
	};

	// PURPOSE: Calculate cell values, given destination path
    void CalcCellValues(const crpmDistanceGrid::Path& dstPath);

	// PURPOSE: Build bridge from source path, based on already calculated cell values
    void BuildBridgeFrom(const crpmDistanceGrid::Path& srcPath);

	// PURPOSE: Build bridge from point, based on already calculated cell values
    void BuildBridgeFrom(int x, int y, crpmDistanceGrid::Path& bridge);

	// PURPOSE: Find best neighboring cell to current cell
    bool FindBestNeighbor(int& x, int& y, crpmSlope& counter);

	// PURPOSE: Is cell valid (within optimization slopes)
	bool IsCellValid(int x, int y) const;

	// PURPOSE: Try to update the cell sum
    bool UpdateCellSum(int x, int y, float sum, const crpmSlope& slopeCounter);

	// PURPOSE: Get the cell sum
	float GetCellSum(int x, int y) const;

	// PURPOSE: Get the cell distance
    float GetCellDistance(int x, int y);

	// PURPOSE: Calculate cell offset (index into cell array)
	int GetCellOffset(int x, int y) const;

	// PURPOSE: Get cell, as an index into the min/max range
	const Cell& GetCell(int x, int y) const;

	// PURPOSE:
	Cell& GetCell(int x, int y);


    atArray<Cell> m_Cells;

    const crpmDistanceGrid* m_Grid;
    int m_SlopeLimit;

    int m_MinX;
    int m_MinY;
    int m_MaxX;
    int m_MaxY;

    // PURPOSE: Defines area that lies between the source and destination paths (but not including them)
    int m_AreaMinX;
    int m_AreaMinY;
    int m_AreaMaxX;
    int m_AreaMaxY;

	crpmDistanceGrid::Path m_BestPath;
    crpmDistanceGrid::Path m_Destination;

    float m_MinSum;
    float m_MaxSum;

	static bool sm_UseAverageAsValue;
};


////////////////////////////////////////////////////////////////////////////////

inline const crpmDistanceGrid::Path& crpmDynamicProgramming::GetBestPath() const
{
	return m_BestPath;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDynamicProgramming::GetSizeX() const
{
	return m_MaxX-m_MinX;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDynamicProgramming::GetSizeY() const
{
	return m_MaxY-m_MinY;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDynamicProgramming::PathIterator::PathIterator(int x, int y, const crpmSlope& slopeCounter, float sum, int length)
: m_X(x)
, m_Y(y)
, m_SlopeCounter(slopeCounter)
, m_Sum(sum)
, m_Length(length)
{
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmDynamicProgramming::PathIterator::GetValue() const
{
	if(crpmDynamicProgramming::sm_UseAverageAsValue)
	{
		return m_Sum / float(m_Length);
	}
	else
	{
		return m_Sum;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmDynamicProgramming::PathIterator::operator<(const PathIterator& other) const
{
	return GetValue() > other.GetValue();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDynamicProgramming::Cell::Pair::Pair()
: m_Sum(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDynamicProgramming::Cell::Pair::Pair(float sum, const crpmSlope& slopeCounter)
: m_Sum(sum)
, m_SlopeCounter(slopeCounter)
{
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmDynamicProgramming::GetCellSum(int x, int y) const
{
	return GetCell(x, y).GetSum();
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmDynamicProgramming::GetCellDistance(int x, int y)
{
	FastAssert(m_Grid);
	return square(1.f + m_Grid->GetCellDistance(x, y)) - 1.f;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDynamicProgramming::GetCellOffset(int x, int y) const
{
	AssertMsg(x>=m_MinX && x<m_MaxX && y>=m_MinY && y<m_MaxY , "coordinates outside of DP window");

	const int offset = (y-m_MinY)*(m_MaxX-m_MinX)+(x-m_MinX);
	AssertMsg(offset < int(m_Cells.size()) , "cell offset is outside of array");

	return offset;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmDynamicProgramming::Cell& crpmDynamicProgramming::GetCell(int x, int y) const
{
	const int offset = GetCellOffset(x, y);
	return m_Cells[offset];
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDynamicProgramming::Cell& crpmDynamicProgramming::GetCell(int x, int y)
{
	const int offset = GetCellOffset(x, y);
	return m_Cells[offset];
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_DYNAMICPROGRAMMING_H
