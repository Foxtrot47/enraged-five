//
// crparameterizedmotion/slope.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_SLOPE_H
#define CRPARAMETERIZEDMOTION_SLOPE_H

#include "math/amath.h"

namespace rage
{

// PURPOSE: Encapsulates slope maintenance
class crpmSlope
{
public:

	// PURPOSE: Constructor
	// PARAMS: slopeLimit - slope limit
    crpmSlope(int slopeLimit=-1);

	// PURPOSE: Copy constructor
    crpmSlope(const crpmSlope& slope);


	// PURPOSE: Get the current slope counter
	// RETURNS: current slope counter
	int Get() const;

	// PURPOSE: Set the slope counter
	// slopeCounter - new slope counter
	void Set(int slopeCounter);

	// PURPOSE: Reset the slope counter
	void Reset();


	// PURPOSE: Step diagonal
	crpmSlope& StepDiagonal();

	// PURPOSE: Step right
	crpmSlope& StepRight();

	// PURPOSE: Step up
	crpmSlope& StepUp();

	// PURPOSE: Step left
	crpmSlope& StepLeft();

	// PURPOSE: Step down
	crpmSlope& StepDown();


	// PURPOSE: Is inside slope limit
	// PARAMS: slopeLimit - slope limit
	// NOTES: if no slope limit specified, will use default provided on construction, if any.
	bool IsInsideLimit(int slopeLimit=-1) const;

	// PURPOSE: Can step right [within slope limit]
	// PARAMS: slopeLimit - slope limit
	// NOTES: if no slope limit specified, will use default provided on construction, if any.
	bool CanStepRight(int slopeLimit=-1) const;

	// PURPOSE: Can step up [within slope limit]
	// PARAMS: slopeLimit - slope limit
	// NOTES: if no slope limit specified, will use default provided on construction, if any.
	bool CanStepUp(int slopeLimit=-1) const;


	// PURPOSE: Is this slope more constrained than provided one?
	// PARAMS: slope - other slope
    bool IsMoreConstrained(const crpmSlope& slope) const;

	// PURPOSE: Is this slope strictly less constrained than provided one?
	// PARAMS: slope - other slope
	bool IsStrictlyLessConstrained(const crpmSlope& slope) const;

	// PURPOSE: Is this slope strictly more constrained than provided one?
	// PARAMS: slope - other slope
	bool IsStrictlyMoreConstrained(const crpmSlope& slope) const;

	// PURPOSE: Equality operator, compares slope counters only.
	// PARAMS: slope - other slope
    bool operator==(const crpmSlope& slope);

private:
    int m_SlopeCounter;
	int m_SlopeLimit;
};


// inlines

////////////////////////////////////////////////////////////////////////////////

inline crpmSlope::crpmSlope(int slopeLimit)
: m_SlopeCounter(0)
, m_SlopeLimit(slopeLimit)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmSlope::crpmSlope(const crpmSlope& slope)
: m_SlopeCounter(slope.m_SlopeCounter)
, m_SlopeLimit(slope.m_SlopeLimit)
{
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmSlope::Get() const
{
	return m_SlopeCounter;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmSlope::Set(int slopeCounter)
{
	m_SlopeCounter = slopeCounter;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmSlope::Reset()
{
	Set(0);
}

////////////////////////////////////////////////////////////////////////////////

inline crpmSlope& crpmSlope::StepDiagonal()
{
	// diagonal step, reset slope counter
	m_SlopeCounter = 0;

	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmSlope& crpmSlope::StepRight()
{
	// if step in opposite direction, reset slope counter
	m_SlopeCounter = Min(0, m_SlopeCounter);

	//one side step further, decrement slope counter
	--m_SlopeCounter;

	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmSlope& crpmSlope::StepUp()
{
	// if step in opposite direction, reset slope counter
	m_SlopeCounter = Max(0, m_SlopeCounter);

	//one further step up, increment slope counter
	++m_SlopeCounter;

	return *this;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmSlope& crpmSlope::StepLeft()
{
	return StepUp();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmSlope& crpmSlope::StepDown()
{
	return StepRight();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmSlope::IsInsideLimit(int slopeLimit) const
{
	slopeLimit = (slopeLimit < 0) ? m_SlopeLimit : slopeLimit;
	FastAssert(slopeLimit >= 0);

	return m_SlopeCounter < slopeLimit && -m_SlopeCounter < slopeLimit;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmSlope::CanStepRight(int slopeLimit) const
{
	slopeLimit = (slopeLimit < 0) ? m_SlopeLimit : slopeLimit;
	FastAssert(slopeLimit >= 0);

	return -(m_SlopeCounter-1) < slopeLimit;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmSlope::CanStepUp(int slopeLimit) const
{
	slopeLimit = (slopeLimit < 0) ? m_SlopeLimit : slopeLimit;
	FastAssert(slopeLimit >= 0);

	return (m_SlopeCounter+1) < slopeLimit;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmSlope::IsMoreConstrained(const crpmSlope& slope) const
{
	const bool sameSlopeDirection = ((m_SlopeCounter * slope.m_SlopeCounter) >= 0);
	return (abs(m_SlopeCounter) >= abs(slope.m_SlopeCounter)) && sameSlopeDirection;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmSlope::IsStrictlyLessConstrained(const crpmSlope& slope) const
{
	// return whether this slope is less constrained than the other
	const bool sameSlopeDirection = ((m_SlopeCounter * slope.m_SlopeCounter) >= 0);
	return (abs(m_SlopeCounter) < abs(slope.m_SlopeCounter)) && sameSlopeDirection;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmSlope::IsStrictlyMoreConstrained(const crpmSlope& slope) const
{
	// return whether this slope is more constrained than the other
	const bool sameSlopeDirection = ((m_SlopeCounter * slope.m_SlopeCounter) >= 0);
	return (abs(m_SlopeCounter) > abs(slope.m_SlopeCounter)) && sameSlopeDirection;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmSlope::operator==(const crpmSlope& slope)
{
	return m_SlopeCounter == slope.m_SlopeCounter;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
#endif // CRPARAMETERIZEDMOTION_SLOPE_H
