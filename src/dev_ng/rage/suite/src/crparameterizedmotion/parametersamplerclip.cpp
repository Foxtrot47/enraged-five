//
// crparameterizedmotion/parametersamplerclip.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "parametersamplerclip.h"

#include "parameter.h"
#include "parameterization.h"
#include "weights.h"

#include "crclip/clip.h"
#include "crclip/clips.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "string/string.h"
#include "string/stringutil.h"


namespace rage
{


////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerClipProperty::crpmParameterSamplerClipProperty()
: crpmParameterSampler(kSamplerTypeClipProperty)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerClipProperty::crpmParameterSamplerClipProperty(const char* propertyName, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
: crpmParameterSampler(kSamplerTypeClipProperty)
{
	Init(propertyName, rcData, clips, skelData, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerClipProperty::~crpmParameterSamplerClipProperty()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRPM_IMPLEMENT_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerClipProperty, crpmParameterSampler::kSamplerTypeClipProperty);

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerClipProperty::Init(const char* propertyName, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	PreInit(rcData, clips, skelData, filterWeightSet, filterMover);

	m_PropertyName = propertyName;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerClipProperty::Shutdown()
{
	m_PropertyName.Reset();

	crpmParameterSampler::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerClipProperty::CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const
{
	Assert(m_Clips);

	float propertySum = 0.f;

	const int numClips = m_Clips->GetNumClips();
	for(int i=0; i<numClips; ++i)
	{
		const float weight = weights.GetWeight(i);
		if(!IsNearZero(weight))
		{
			const crProperty* prop = m_Clips->GetClip(i)->GetProperties()?m_Clips->GetClip(i)->GetProperties()->FindProperty(m_PropertyName):NULL;
			if(prop)
			{
				const crPropertyAttribute* propAttribute = prop->GetAttribute(m_PropertyAttribute);
				if(propAttribute)
				{
					switch(propAttribute->GetType())
					{
					case crPropertyAttribute::kTypeFloat:
						{
							const crPropertyAttributeFloat* propAttributeFloat = static_cast<const crPropertyAttributeFloat*>(propAttribute);
							propertySum += propAttributeFloat->GetFloat() * weight;
						}
						break;

					default:
						break;
					}
				}
			}
		}
	}

	outParameter.SetValue(0, propertySum);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerClipProperty::PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	crpmParameterSampler::PreInit(rcData, clips, skelData, filterWeightSet, filterMover);

	m_Parameterization.Init(1);

	crpmParameterization::DimensionInfo& dim0 = m_Parameterization.GetDimensionInfo(0);
	dim0.SetDimensionType(crpmParameterization::DimensionInfo::kDimensionTypeScalar);
	dim0.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningUnknown);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerClipProperty::ConfigProperty(const char* token, ConfigParser& parser)
{
	if(!stricmp(token, "PropertyName"))
	{
		return parser.StripStringProperty(m_PropertyName);
	}
	else if(!stricmp(token, "PropertyAttribute"))
	{
		return parser.StripStringProperty(m_PropertyAttribute);
	}

	return crpmParameterSampler::ConfigProperty(token, parser);
}


////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerClipName::crpmParameterSamplerClipName()
: crpmParameterSampler(kSamplerTypeClipName)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerClipName::crpmParameterSamplerClipName(const char* propertyName, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
: crpmParameterSampler(kSamplerTypeClipName)
{
	Init(propertyName, rcData, clips, skelData, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerClipName::~crpmParameterSamplerClipName()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRPM_IMPLEMENT_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerClipName, crpmParameterSampler::kSamplerTypeClipName);

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerClipName::Init(const char* clipName, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	PreInit(rcData, clips, skelData, filterWeightSet, filterMover);

	m_ClipName = clipName;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerClipName::Shutdown()
{
	m_ClipName.Reset();

	crpmParameterSampler::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerClipName::CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const
{
	Assert(m_Clips);

	float sum = 0.f;

	const int numClips = m_Clips->GetNumClips();
	for(int i=0; i<numClips; ++i)
	{
		const float weight = weights.GetWeight(i);
		if(!IsNearZero(weight))
		{
			if(!StringWildcardCompare(m_ClipName.c_str(), m_Clips->GetClip(i)->GetName(), true))
			{
				sum += weight;
			}
		}
	}

	outParameter.SetValue(0, sum);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerClipName::PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	crpmParameterSampler::PreInit(rcData, clips, skelData, filterWeightSet, filterMover);

	m_Parameterization.Init(1);

	crpmParameterization::DimensionInfo& dim0 = m_Parameterization.GetDimensionInfo(0);
	dim0.SetDimensionType(crpmParameterization::DimensionInfo::kDimensionTypeScalar);
	dim0.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningUnknown);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerClipName::ConfigProperty(const char* token, ConfigParser& parser)
{
	if(!stricmp(token, "ClipName"))
	{
		return parser.StripStringProperty(m_ClipName);
	}

	return crpmParameterSampler::ConfigProperty(token, parser);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

