//
// crparameterizedmotion/parameterization.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "parameterization.h"

#include "angle.h"
#include "parameterizedmotionsimple.h"

#include "atl/array_struct.h"
#include "crmetadata/dumpoutput.h"
#include "data/safestruct.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterization::crpmParameterization()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterization::crpmParameterization(datResource& rsc)
: m_Dimensions(rsc, true)
, m_DefaultParameter(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterization::crpmParameterization(int numDimensions)
{
	Init(numDimensions);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterization::~crpmParameterization()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmParameterization);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmParameterization::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmParameterization)
	SSTRUCT_FIELD(crpmParameterization, m_Dimensions)
	SSTRUCT_FIELD(crpmParameterization, m_DefaultParameter)
	SSTRUCT_END(crpmParameterization)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmParameterization::Init(int numDimensions)
{
	m_Dimensions.Reset();
	m_Dimensions.Resize(numDimensions);
	m_DefaultParameter.Init(numDimensions);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterization::Shutdown()
{
	m_Dimensions.Reset();
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterization::DimensionInfo::DimensionInfo(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crpmParameterization::DimensionInfo::eDimensionType);
datSwapper_ENUM(crpmParameterization::DimensionInfo::eDimensionMeaning);

void crpmParameterization::DimensionInfo::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmParameterization::DimensionInfo)
	SSTRUCT_FIELD(crpmParameterization::DimensionInfo, m_DimensionType)
	SSTRUCT_FIELD(crpmParameterization::DimensionInfo, m_DimensionMeaning)
	SSTRUCT_FIELD(crpmParameterization::DimensionInfo, m_SpaceScale)
	SSTRUCT_END(crpmParameterization::DimensionInfo)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

float crpmParameterization::DimensionInfo::Lerp(float t, float f0, float f1) const
{
	switch(m_DimensionType)
	{
	case kDimensionTypeBinary:
		return (t>0.f)?f1:f0;

	case kDimensionTypeDegreesMinus270To90:
		{
			crpmAngleDegrees angle;
			angle.Lerp(t, crpmAngleDegrees(f0), crpmAngleDegrees(f1));
			return angle.GetDegreesMinus270To90();
		}

	default:
	case kDimensionTypeScalar:
		return rage::Lerp(t, f0, f1);
	}
}

////////////////////////////////////////////////////////////////////////////////

float crpmParameterization::DimensionInfo::Approach(float f0, float f1, float maxChange) const
{
	Assert(maxChange >= 0.f);

	switch(m_DimensionType)
	{
		// TODO --- support wrapping angles here.

	default:
	case kDimensionTypeScalar:
		return f0 + rage::Clamp(f1-f0, -maxChange, maxChange);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterization::DimensionInfo::IsClose(float f0, float f1, float tolerance) const
{
	Assert(tolerance >= 0.f);

	switch(m_DimensionType)
	{
		// TODO --- support wrapping angles here.

	default:
	case kDimensionTypeScalar:
		return rage::IsClose(f0, f1, tolerance);
	}
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crpmParameterization::DimensionInfo::Serialize(datSerialize& s)
{
	int dimensionType = int(m_DimensionType);
	s << dimensionType;
	if(s.IsRead())
	{
		m_DimensionType = eDimensionType(dimensionType);
	}

	int dimensionMeaning = int(m_DimensionMeaning);
	s << dimensionMeaning;
	if(s.IsRead())
	{
		m_DimensionMeaning = eDimensionMeaning(dimensionMeaning);
	}

	s << m_SpaceScale;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterization::Serialize(datSerialize& s)
{
	s << m_Dimensions;
	if(s_PmSimpleSerializeVersion >= 3)
	{
		s << m_DefaultParameter;
	}
	else if(s.IsRead())
	{
		m_DefaultParameter.Init(m_Dimensions.GetCount(), 0.f);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterization::Dump(crDumpOutput& output) const
{
	for(int i=0; i<GetNumDimensions(); ++i)
	{
		const DimensionInfo& info = m_Dimensions[i];

		output.Outputf(2, "dimensiontype", i, "(%d)", int(info.GetDimensionType()));
		output.Outputf(2, "dimensionmeaning", i, "(%d)", int(info.GetDimensionMeaning()));
		output.Outputf(2, "spacescale", "%f", i, info.GetSpaceScale());
	}

	atString defaultText;
	m_DefaultParameter.Dump(defaultText);
	output.Outputf(1, "defaultparameter", "%s", defaultText.c_str());
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crpmParameterization::DimensionInfo& crpmParameterization::AppendDimensionInfo()
{
	m_DefaultParameter.Grow();
	return m_Dimensions.Grow();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
