//
// crparameterizedmotion/parametersamplercombiner.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "parametersamplercombiner.h"


namespace rage
{


////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerCombiner::crpmParameterSamplerCombiner()
: crpmParameterSamplerContainer(kSamplerTypeCombiner)
, m_Operation(kCombinerOperationMean)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerCombiner::~crpmParameterSamplerCombiner()
{
	crpmParameterSamplerContainer::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRPM_IMPLEMENT_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerCombiner, crpmParameterSampler::kSamplerTypeCombiner);

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerCombiner::ConfigProperty(const char* token, ConfigParser& parser)
{
	if(!stricmp(token, "Operation"))
	{
		atString operationStr;
		parser.StripStringProperty(operationStr);

		static const char* operationNames[kCombinerOperationNum] = {"minimum", "maximum", "mean", "sum"};

		int i=0;
		for(; i<kCombinerOperationNum; i++)
		{
			if(stricmp(operationStr.c_str(), operationNames[i]))
			{
				m_Operation = static_cast<eCombinerOperation>(i);
				break;
			}
		}
		if(i==kCombinerOperationNum)
		{
			// TODO --- unknown combiner operation
			return false;
		}
	}

	return crpmParameterSamplerContainer::ConfigProperty(token, parser);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerCombiner::AppendSampler(crpmParameterSampler& sampler)
{
	if(m_Samplers.empty())
	{
		m_Samplers.Grow() = &sampler;
		m_Parameters.Grow().Init(sampler.GetNumParameterDimensions());

		 // The parameterization dimensions will be based on the first sampler
		const int numParameters = sampler.GetNumParameterDimensions();
		for(int i=0; i<numParameters; ++i)
		{
			m_Parameterization.AppendDimensionInfo() = sampler.GetParameterization().GetDimensionInfo(i);
		}
	}
	else if(sampler.GetNumParameterDimensions() == m_Parameterization.GetNumDimensions())
	{
		m_Samplers.Grow() = &sampler;
		m_Parameters.Grow().Init(sampler.GetNumParameterDimensions());
	}
	else
	{
		AssertMsg(false, "crpmParameterSamplerCombiner::AppendSampler - trying to add sampler with an invalid number of dimension.");
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerCombiner::CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const
{
	const int numSamplers = m_Samplers.GetCount();
	if(numSamplers == 0)
	{
		return;
	}

	// Calculate the parameters of all samplers
	for(int i=0; i<numSamplers; ++i)
	{
		m_Samplers[i]->CalcParameter(weights, m_Parameters[i]);
	}

	// Combine result from all parameters
	const int numDimension = m_Parameterization.GetNumDimensions();
	for(int d=0; d<numDimension; ++d)
	{
		float result = 0.f;
		switch(m_Operation)
		{
		case kCombinerOperationMinimum:
			{
				result = m_Parameters[0].GetValue(d);
				for(int i=1; i<numSamplers; ++i)
				{
					result = FPMin(result, m_Parameters[i].GetValue(d));
				}
			}
			break;

		case kCombinerOperationMaximum:
			{
				result = m_Parameters[0].GetValue(d);
				for(int i=1; i<numSamplers; ++i)
				{
					result = FPMax(result, m_Parameters[i].GetValue(d));
				}
			}
			break;

		case kCombinerOperationMean:
			{
				for(int i=0; i<numSamplers; ++i)
				{
					result += m_Parameters[i].GetValue(d);
				}
				result /= numSamplers;
			}
			break;

		case kCombinerOperationSum:
			{
				for(int i=0; i<numSamplers; ++i)
				{
					result += m_Parameters[i].GetValue(d);
				}
			}
			break;

		default:
			{
				AssertMsg(false, "crpmParameterSamplerCombiner::CalcParameterDerived - invalid combiner operation.");
			}
			break;
		}
		outParameter.SetValue(d, result);
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
