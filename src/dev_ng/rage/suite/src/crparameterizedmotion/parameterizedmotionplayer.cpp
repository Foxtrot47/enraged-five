//
// crparameterizedmotion/parameterizedmotionplayer.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "parameterizedmotionplayer.h"

#include "crmetadata/tags.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionPlayer::crpmParameterizedMotionPlayer()
: m_Pm(NULL)
, m_Rate(1.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionPlayer::~crpmParameterizedMotionPlayer()
{
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionPlayer::Init(crpmParameterizedMotion* pm)
{
	SetParameterizedMotion(pm);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionPlayer::Shutdown()
{
	m_Pm = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionPlayer::Update(float deltaTime, float parameterTime, bool& outLoopedOrEnded, const TagTriggerFunctor* tagTriggerFunctor)
{
	outLoopedOrEnded = false;
	if(m_Pm)
	{
		float startU = m_Pm->GetU();
		bool finished = m_Pm->IsFinished();

		m_Pm->Update(deltaTime*m_Rate, parameterTime*m_Rate);

		float endU = m_Pm->GetU();

		outLoopedOrEnded = (endU < startU) || (m_Pm->IsFinished() && !finished);

		if(tagTriggerFunctor != NULL)
		{
			const crTags* tags = m_Pm->GetTags();
			if(tags != NULL)
			{
				if(endU >= startU)
				{
					TriggerTags(*tags, startU, endU, tagTriggerFunctor);
				}
				else
				{
					TriggerTags(*tags, startU, 1.f, tagTriggerFunctor);
					TriggerTags(*tags, 0.f, endU, tagTriggerFunctor);
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionPlayer::TriggerTags(const crTags& tags, float startU, float endU, const TagTriggerFunctor* tagTriggerFunctor) const
{
	const int numTags = tags.GetNumTags();
	for(int i=0; i<numTags; ++i)
	{
		const crTag* tag = tags.GetTag(i);
		Assert(tag);

		const float startTag = tag->GetStart();
		const float endTag = tag->GetEnd();
		if(startTag > endU)
		{
			break;
		}

		if(startTag >= startU && startTag < endU)
		{
			(*tagTriggerFunctor)(*tag, true, true);
		}
		if(endTag >= startU && startTag < endU)
		{
			(*tagTriggerFunctor)(*tag, false, true);
		}
		if(endTag >= startU && endTag < endU)
		{
			(*tagTriggerFunctor)(*tag, true, false);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
