//
// crparameterizedmotion/weights.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "weights.h"

#include "atl/array_struct.h"
#include "vector/quantize.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmWeights::crpmWeights(datResource& rsc)
: m_Weights(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

//crpmWeights::crpmWeights(int worstNumWeights)
//{
//	PreInit(worstNumWeights);
//}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmWeights);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmWeights::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crpmWeights);
	STRUCT_FIELD(m_Weights);
	STRUCT_FIELD(m_Sum);
	STRUCT_FIELD(m_InvSum);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmWeights::PreInit(int worstNumWeights)
{
	m_Weights.Reserve(worstNumWeights);
}

////////////////////////////////////////////////////////////////////////////////

void crpmWeights::Clear()
{
	m_Weights.Resize(0);

	m_Sum = 0.f;
	m_InvSum = 0.f;
}

////////////////////////////////////////////////////////////////////////////////

float crpmWeights::GetWeightMax() const
{
	float maxWeight = 0.f;

	const int numWeights = m_Weights.GetCount();
	for(int i=0; i<numWeights; ++i)
	{
		maxWeight = Max(m_Weights[i], maxWeight);
	}

	return maxWeight;
}

////////////////////////////////////////////////////////////////////////////////

void crpmWeights::Lerp(float blend, const crpmWeights& weights0, const crpmWeights& weights1)
{
	Assert(m_Weights.GetCount() == weights0.m_Weights.GetCount());
	Assert(m_Weights.GetCount() == weights1.m_Weights.GetCount());

	m_Sum = 0.f;
	m_InvSum = 0.f;

	const int numWeights = m_Weights.GetCount();
	for(int i=0; i<numWeights; ++i)
	{
		m_Weights[i] = weights0.m_Weights[i] * (1.f-blend) + weights1.m_Weights[i] * blend;
		m_Sum += m_Weights[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmWeights::Normalize()
{
	float invSum = GetInvWeightSum();

	const int numWeights = m_Weights.GetCount();
	for(int i=0; i<numWeights; ++i)
	{
		m_Weights[i] *= invSum;
	}

	if(invSum == 0.f)
	{
		m_Sum = 0.f;
		m_InvSum = 0.f;
	}
	else
	{
		m_Sum = 1.f;
		m_InvSum = 1.f;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmWeights::Serialize(datSerialize& s)
{
	s << m_Weights;
	s << m_Sum;
	s << m_InvSum;
}

////////////////////////////////////////////////////////////////////////////////

crpmWeightsPacked::crpmWeightsPacked()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmWeightsPacked::crpmWeightsPacked(datResource& rsc)
: m_WeightsIndices(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmWeightsPacked::crpmWeightsPacked(const crpmWeights& weights)
{
	Init(weights);
}

////////////////////////////////////////////////////////////////////////////////

crpmWeightsPacked::crpmWeightsPacked(const crpmWeightsPacked& weights)
{
	Init(weights);
}

////////////////////////////////////////////////////////////////////////////////

crpmWeightsPacked::~crpmWeightsPacked()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmWeightsPacked)

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmWeightsPacked::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmWeightsPacked)
	SSTRUCT_FIELD(crpmWeightsPacked, m_WeightsIndices)
	SSTRUCT_END(crpmWeightsPacked)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmWeightsPacked::Init(const crpmWeights& weights)
{
	Pack(weights);
}

////////////////////////////////////////////////////////////////////////////////

void crpmWeightsPacked::Init(const crpmWeightsPacked& weights)
{
	m_WeightsIndices = weights.m_WeightsIndices;
}

////////////////////////////////////////////////////////////////////////////////

void crpmWeightsPacked::Shutdown()
{
	m_WeightsIndices.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crpmWeightsPacked::Pack(const crpmWeights& weights)
{
	const int numWeights = weights.GetNumWeights();
	int numNonZeroWeights = 0;
	for(int i=0; i<numWeights; ++i)
	{
		if(!IsNearZero(weights.GetWeight(i)))
		{
			++numNonZeroWeights;
		}
	}

	if(numNonZeroWeights > 0)
	{
		m_WeightsIndices.Reset();
		m_WeightsIndices.Resize(numNonZeroWeights);

		int n=0;
		for(int i=0; i<numWeights; ++i)
		{
			float w = weights.GetWeight(i);
			if(!IsNearZero(w))
			{
				m_WeightsIndices[n++] = (Pack16_0to1(w)<<16) | u16(i);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmWeightsPacked::Unpack(crpmWeights& weights) const
{
	weights.ClearWeights(0.f);

	const int numIndices = m_WeightsIndices.GetCount();
	if(numIndices > 0)
	{
		Assert(weights.GetNumWeights() > int(m_WeightsIndices[numIndices-1]&0xffff));

		for(int i=0; i<numIndices; ++i)
		{
			int idx = int(m_WeightsIndices[i]&0xffff);
			float w = Unpack16_0to1(u16(m_WeightsIndices[i]>>16));

			weights.SetWeight(idx, w);
		}

//		weights.Normalize();  // TODO --- normalize to correct for rounding errors?
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmWeightsPacked::Serialize(datSerialize& s)
{
	s << m_WeightsIndices;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
