//
// crparameterizedmotion/timealignment.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_TIMEALIGNMENT_H
#define CRPARAMETERIZEDMOTION_TIMEALIGNMENT_H

#include "distancegrid.h"
#include "spline.h"

namespace rage
{

class crClip;
class crpmPointCloudTransform;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Stores alignment curves between two clips
class crpmTimeAlignment
{
public:

	// PURPOSE: Default constructor
    crpmTimeAlignment();

	// PURPOSE: Calculating constructor
	// PARAMS:
	// path - path representing alignment
	// grid - path's distance grid
	crpmTimeAlignment(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& grid);

	// PURPOSE: Destructor
	~crpmTimeAlignment();

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Calculate the alignment
	// Calculates temporal and transformation alignment curves for path provided
	// PARAMS:
	// path - path representing alignment
	// grid - path's distance grid
	void Calculate(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& grid);

	// PURPOSE: Get cells for clips, given alignment phase
	// PARAMS: u - alignment phase
	// RETURNS: vector2 containing clip x and y cells
    Vec2V_Out GetAlignmentCells(float u) const;

	// PURPOSE: Get transformation for clips, given alignment phase
	// PARAMS: u - alignment phase
	// RETURNS: point cloud transformation
    crpmPointCloudTransform GetAlignmentTransform(float u) const;

	// PURPOSE: Convert cell in clip x into cell in clip y (plus alignment phase)
	// PARAMS:
	// cellX - input cell index in clip x
	// outCellY - output cell index in clip y
	// outU - output alignment phase
    void ConvertCellXToCellY(float cellX, float& outCellY, float& outU) const;

	// PURPOSE: Convert cell in clip y into time in clip x (plus alignment phase)
	// PARAMS:
	// cellY - input cell index in clip y
	// outTimeX - output cell index in clip x
	// outU - output alignment phase
	void ConvertCellYToCellX(float cellY, float& outCellX, float& outU) const;

	// PURPOSE: Get clip x
	// RETURNS: pointer to clip x
    const crClip* GetClipX() const;
	
	// PURPOSE: Get clip y
	// RETURNS: pointer to clip y
	const crClip* GetClipY() const;

	// PURPOSE: Get clip x sample rate
	float GetSampleRateX() const;

	// PURPOSE: Get clip y sample rate
	float GetSampleRateY() const;

	// PURPOSE: Get clip x cycles
	int GetCyclesX() const;

	// PURPOSE: Get clip y cycles
	int GetCyclesY() const;

	// PURPOSE: Get clip x repeat
	int GetRepeatX() const;

	// PURPOSE: Get clip y repeat
	int GetRepeatY() const;

	// PURPOSE: Get average distance of path
	// RETURNS: average distance of path
	float GetDistanceAverage() const;


private:

	// PURPOSE: Internal function, converts cell index on clip into alignment phase
	float ConvertCellToU(float cell, int clipIdx) const;

	// PURPOSE: Internal function, creates the temporal alignments
	void CreateTemporalAlignments(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& grid);

	// PURPOSE: Internal function, creates the transformation alignments
	void CreateTransformAlignments(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& grid);

	crpmSeries<Vec2V> m_TemporalAlignmentSeries;
	crpmSeries<Vec3V> m_TransformAlignmentSeries;

    const crClip* m_ClipX;
	const crClip* m_ClipY;

	float m_SampleRateX;
	float m_SampleRateY;

	int m_CyclesX;
	int m_CyclesY;

	int m_RepeatX;
	int m_RepeatY;

    float m_DistanceAverage;
};

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crpmTimeAlignment::GetClipX() const
{
	return m_ClipX;
}

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crpmTimeAlignment::GetClipY() const
{
	return m_ClipY;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmTimeAlignment::GetSampleRateX() const
{
	return m_SampleRateX;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmTimeAlignment::GetSampleRateY() const
{
	return m_SampleRateY;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmTimeAlignment::GetCyclesX() const
{
	return m_CyclesX;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmTimeAlignment::GetCyclesY() const
{
	return m_CyclesY;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmTimeAlignment::GetRepeatX() const
{
	return m_RepeatX;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmTimeAlignment::GetRepeatY() const
{
	return m_RepeatY;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmTimeAlignment::GetDistanceAverage() const
{
	return m_DistanceAverage;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRPARAMETERIZEDMOTION_TIMEALIGNMENT_H



