//
// crparameterizedmotion/parametersamplerlocomotion.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "parametersamplerlocomotion.h"

#include "motioncontroller.h"
#include "parameter.h"

#include "crskeleton/skeletondata.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerLocomotion::crpmParameterSamplerLocomotion()
: crpmParameterSamplerPhysical(kSamplerTypeLocomotion)
, m_SampleSpeed(true)
, m_SampleDirection(true)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerLocomotion::crpmParameterSamplerLocomotion(bool sampleSpeed, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
: crpmParameterSamplerPhysical(kSamplerTypeLocomotion)
, m_SampleSpeed(true)
, m_SampleDirection(true)
{
	Init(sampleSpeed, rcData, clips, skelData, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerLocomotion::~crpmParameterSamplerLocomotion()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRPM_IMPLEMENT_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerLocomotion, crpmParameterSampler::kSamplerTypeLocomotion);

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerLocomotion::Init(bool sampleSpeed, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	m_SampleSpeed = sampleSpeed;

	PreInit(rcData, clips, skelData, filterWeightSet, filterMover);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerLocomotion::Shutdown()
{

	crpmParameterSampler::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerLocomotion::CalcParameterDerivedWithMotionController(crpmMotionController& mc, crpmParameter& outParameter) const
{
	float duration = mc.GetDuration();

	float displacement = 0.f;
	float rotation = 0.f;

	Mat34V lastMtx;
	mc.GetSkeleton().GetGlobalMtx(0, lastMtx);
	do
	{
		mc.Advance(m_SampleRate);

		Mat34V currMtx;
		mc.GetSkeleton().GetGlobalMtx(0, currMtx);
		
		Mat34V deltaMtx;
		UnTransformOrtho(deltaMtx, lastMtx, currMtx);

		displacement += Mag(deltaMtx.GetCol3()).Getf();

		float rot;
		GetRotationVertical(deltaMtx, rot);

		rotation += rot;

//		Displayf("dT %f T %f dR %f R %f", deltaMtx.d.Mag(), displacement, rot, rotation);

		lastMtx = currMtx;
	}
	while(!mc.IsFinished());

	int numParameters = 0;
	if(m_SampleDirection)
	{
		outParameter.SetValue(numParameters++, rotation / duration);
	}

	if(m_SampleSpeed)
	{
		outParameter.SetValue(numParameters++, displacement / duration);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerLocomotion::DebugMapParameterDerived(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const
{
#if HACK_GTA4
	v = Vec3V(V_Y_AXIS_WONE);
#else //HACK_GTA4
	v = Vec3V(V_X_AXIS_WONE);
#endif //HACK_GTA4

	if(m_SampleSpeed)
	{
		const float len = parameter.GetValue(m_SampleDirection?1:0);
		v *= ScalarVFromF32(len);
	}

	if(m_SampleDirection)
	{
		v = RotateAboutZAxis(v, ScalarVFromF32(parameter.GetValue(0)));
	}

	if(skel.GetParentMtx())
	{
		Mat34V mtx = *skel.GetParentMtx();
		Vec3V e = Mat33VToEulersZXY(mtx.GetMat33Ref());
		e[0] = e[1] = 0.f;
		Mat33VFromEulersZXY(mtx.GetMat33Ref(), e);
		v = Transform(mtx, v);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerLocomotion::PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	crpmParameterSamplerPhysical::PreInit(rcData, clips, skelData, filterWeightSet, filterMover);

	InitParameterization();
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerLocomotion::ConfigProperty(const char* token, ConfigParser& parser)
{
	if(!stricmp(token, "SampleSpeed"))
	{
		if(parser.StripBoolProperty(m_SampleSpeed))
		{
			InitParameterization();
			return true;
		}
		return false;
	}
	if(!stricmp(token, "SampleDirection"))
	{
		if(parser.StripBoolProperty(m_SampleDirection))
		{
			InitParameterization();
			return true;
		}
		return false;
	}

	return crpmParameterSamplerPhysical::ConfigProperty(token, parser);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerLocomotion::InitParameterization()
{
	Assert(m_SampleDirection || m_SampleSpeed);
	m_Parameterization.Init((m_SampleDirection&&m_SampleSpeed) ? 2 : 1);

	int numParameters = 0;
	if(m_SampleDirection)
	{
		crpmParameterization::DimensionInfo& dim = m_Parameterization.GetDimensionInfo(numParameters++);
		dim.SetDimensionType(crpmParameterization::DimensionInfo::kDimensionTypeRadians);
		dim.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningHeading);
	}
	if(m_SampleSpeed)
	{
		crpmParameterization::DimensionInfo& dim = m_Parameterization.GetDimensionInfo(numParameters++);
		dim.SetDimensionType(crpmParameterization::DimensionInfo::kDimensionTypeScalar);
		dim.SetDimensionMeaning(crpmParameterization::DimensionInfo::kDimensionMeaningSpeed);
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


