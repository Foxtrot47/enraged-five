//
// crparameterizedmotion/parametersamplerclip.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERSAMPLERCLIP_H
#define CRPARAMETERIZEDMOTION_PARAMETERSAMPLERCLIP_H

#include "parametersampler.h"

#include "atl/string.h"

namespace rage
{


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Clip property sampler
class crpmParameterSamplerClipProperty : public crpmParameterSampler
{
public:

	// PURPOSE: Default constructor
	crpmParameterSamplerClipProperty();

	// PURPOSE: Initializing constructor
	crpmParameterSamplerClipProperty(const char* propertyName, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Destructor
	virtual ~crpmParameterSamplerClipProperty();

	// PURPOSE:
	CRPM_DECLARE_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerClipProperty);

	// PURPOSE: Initialization
	void Init(const char* propertyName, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Shutdown
	virtual void Shutdown();

protected:

	// PURPOSE: Calculate parameter for given set of weights
	virtual void CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const;

	// PURPOSE: Internal function, initialize base classes
	virtual void PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Internal function, override sampler property configuration
	virtual bool ConfigProperty(const char* token, ConfigParser& parser);

public:

	atString m_PropertyName;
	atString m_PropertyAttribute;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Clip filename sampler
class crpmParameterSamplerClipName : public crpmParameterSampler
{
public:

	// PURPOSE: Default constructor
	crpmParameterSamplerClipName();

	// PURPOSE: Initializing constructor
	crpmParameterSamplerClipName(const char* propertyName, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Destructor
	virtual ~crpmParameterSamplerClipName();

	// PURPOSE:
	CRPM_DECLARE_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerClipName);

	// PURPOSE: Initialization
	void Init(const char* clipName, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Shutdown
	virtual void Shutdown();

protected:

	// PURPOSE: Calculate parameter for given set of weights
	virtual void CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const;

	// PURPOSE: Internal function, initialize base classes
	virtual void PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover);

	// PURPOSE: Internal function, override sampler property configuration
	virtual bool ConfigProperty(const char* token, ConfigParser& parser);

public:

	atString m_ClipName;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERSAMPLERCLIP_H
