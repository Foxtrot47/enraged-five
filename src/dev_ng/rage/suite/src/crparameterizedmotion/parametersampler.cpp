//
// crparameterizedmotion/parametersampler.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//


#include "parametersampler.h"

#include "motioncontroller.h"
#include "parameter.h"
#include "parameterization.h"
#include "parametersamplerclip.h"
#include "parametersamplercombiner.h"
#include "parametersamplerdirectional.h"
#include "parametersamplerlocomotion.h"
#include "parametersamplerspatial.h"

#include "crclip/clips.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterSampler::crpmParameterSampler(eSamplerTypes samplerType)
: m_SamplerType(samplerType)
, m_RcData(NULL)
, m_Clips(NULL)
, m_SkelData(NULL)
, m_FilterWeightSet(NULL)
, m_FilterMover(false)
{
	Assert(samplerType != kSamplerTypeNone);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSampler::~crpmParameterSampler()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSampler::Shutdown()
{
	m_SamplerType = kSamplerTypeNone;

	m_Parameterization.Shutdown();

	m_RcData = NULL;
	m_Clips = NULL;
	m_SkelData = NULL;
	m_FilterWeightSet = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSampler::Reset() const
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSampler* crpmParameterSampler::CreateSampler(eSamplerTypes samplerType)
{
	const SamplerTypeInfo* samplerTypeInfo = FindSamplerTypeInfo(samplerType);
	if(samplerTypeInfo)
	{
		SamplerCreateFn* createFn = samplerTypeInfo->GetCreateFn();
		return createFn();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSampler* crpmParameterSampler::CreateSampler(const char* samplerName)
{
	const SamplerTypeInfo* samplerTypeInfo = FindSamplerTypeInfo(samplerName);
	if(!samplerTypeInfo)
	{
		// failed to find, try prefixing base class string
		char buf[255];
		formatf(buf, "crpmParameterSampler%s", samplerName);
		samplerTypeInfo = FindSamplerTypeInfo(buf);

		// special fall back for old aim sampler
		if(!samplerTypeInfo)
		{
			if(!stricmp(samplerName, "aim"))
			{
				samplerTypeInfo = FindSamplerTypeInfo("crpmParameterSamplerDirectional");
			}
		}
	}

	if(samplerTypeInfo)
	{
		SamplerCreateFn* createFn = samplerTypeInfo->GetCreateFn();
		return createFn();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSampler* crpmParameterSampler::BuildSampler(const char* samplerConfig, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	if(samplerConfig && samplerConfig[0])
	{
		ConfigParser parser(samplerConfig, rcData, clips, skelData, filterWeightSet, filterMover);

		parser.StripWhitespace();

		const int tokenMaxSize = 256;
		char tokenBuf[tokenMaxSize];
		if(parser.StripToken(tokenBuf, tokenMaxSize))
		{
			crpmParameterSampler* sampler = CreateSampler(tokenBuf);
			if(sampler)
			{
				sampler->PreInit(rcData, clips, skelData, filterWeightSet, filterMover);
				if(!sampler->ConfigSampler(parser))
				{
					// TODO --- error, failed to config sampler
				}

				return sampler;
			}
			else
			{
				// TODO --- report failed to create sampler
			}
		}
		else
		{
			// TODO --- bad string, no sampler?
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSampler::CalcParameter(const crpmWeights& weights, crpmParameter& outParameter) const
{
	CalcParameterDerived(weights, outParameter);

	const int numDimensions = GetNumParameterDimensions();
	for(int i=0; i<numDimensions; ++i)
	{
		const float scale = m_ParameterScale.GetCount()?m_ParameterScale[i]:1.f;
		const float offset = m_ParameterOffset.GetCount()?m_ParameterOffset[i]:0.f;

		outParameter.SetValue(i, (outParameter.GetValue(i)*scale)+offset);
	}
}

////////////////////////////////////////////////////////////////////////////////

const crpmParameterization& crpmParameterSampler::GetParameterization() const
{
	return m_Parameterization;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSampler::eSamplerTypes crpmParameterSampler::GetSamplerType() const
{
	return m_SamplerType;
}

////////////////////////////////////////////////////////////////////////////////

int crpmParameterSampler::GetNumWeightDimensions() const
{
	return m_Clips->GetNumClips();
}

////////////////////////////////////////////////////////////////////////////////

int crpmParameterSampler::GetNumParameterDimensions() const
{
	return GetParameterization().GetNumDimensions();
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::DebugMapParameter(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const
{
	const int numDimensions = GetNumParameterDimensions();

	crpmParameter rawParameter(numDimensions, 0.f);
	for(int i=0; i<numDimensions; ++i)
	{
		const float scale = m_ParameterScale.GetCount()?m_ParameterScale[i]:1.f;
		const float offset = m_ParameterOffset.GetCount()?m_ParameterOffset[i]:0.f;

		rawParameter.SetValue(i, (parameter.GetValue(i)-offset)/scale);
	}

	return DebugMapParameterDerived(rawParameter, skel, v);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSampler::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		sm_SamplerTypeInfos.Resize(kSamplerTypeNum);
		for(int i=0; i<sm_SamplerTypeInfos.GetCount(); ++i)
		{
			sm_SamplerTypeInfos[i] = NULL;
		}

		// automatically register expression ops
		crpmParameterSamplerContainer::InitClass();
		crpmParameterSamplerDirectional::InitClass();
		crpmParameterSamplerClipName::InitClass();
		crpmParameterSamplerClipProperty::InitClass();
		crpmParameterSamplerLocomotion::InitClass();
		crpmParameterSamplerSpatial::InitClass();
		crpmParameterSamplerCombiner::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSampler::ShutdownClass()
{
	sm_SamplerTypeInfos.Reset();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::DebugMapParameterDerived(const crpmParameter&, const crSkeleton&, Vec3V_InOut) const
{
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSampler::PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	m_RcData = rcData;
	m_Clips = &clips;
	m_SkelData = &skelData;
	m_FilterWeightSet = filterWeightSet;
	m_FilterMover = filterMover;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSampler::ConfigParser::ConfigParser(const char* configString, const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
: m_ConfigString(configString)
, m_RcData(rcData)
, m_Clips(&clips)
, m_SkelData(&skelData)
, m_FilterWeightSet(filterWeightSet)
, m_FilterMover(filterMover)
{
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSampler::ConfigParser::StripWhitespace()
{
	if(m_ConfigString)
	{
		while(*m_ConfigString == ' ' || *m_ConfigString == '\t')
		{
			m_ConfigString++;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigParser::StripToken(char* tokenBuf, int tokenBufSize)
{
	if(m_ConfigString)
	{
		tokenBuf[tokenBufSize-1] = '\0';

		for(int i=0; i<(tokenBufSize-1); ++i)
		{
			if((*m_ConfigString>='a' && *m_ConfigString<='z') || (*m_ConfigString>='A' && *m_ConfigString<='Z') || (*m_ConfigString>='0' && *m_ConfigString<='9') || (*m_ConfigString=='_') || (*m_ConfigString=='+') || (*m_ConfigString=='-') || (*m_ConfigString=='.') || (*m_ConfigString=='*') || (*m_ConfigString=='?'))
			{
				tokenBuf[i] = *m_ConfigString++;
			}
			else
			{
				tokenBuf[i] = '\0';
				return (i > 0);
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigParser::MatchChar(char ch)
{
	if(m_ConfigString && ch == *m_ConfigString)
	{
		++m_ConfigString;
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigParser::StripFloatProperty(float& outFloat)
{
	if(m_ConfigString)
	{
		StripWhitespace();
		if(MatchChar('('))
		{
			StripWhitespace();

			const int tokenMaxSize = 256;
			char tokenBuf[tokenMaxSize];
			if(StripToken(tokenBuf, tokenMaxSize))
			{
				outFloat = float(atof(tokenBuf));
				StripWhitespace();
				return MatchChar(')');
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigParser::StripStringProperty(atString& outString)
{
	if(m_ConfigString)
	{
		StripWhitespace();
		if(MatchChar('('))
		{
			StripWhitespace();

			const int tokenMaxSize = 256;
			char tokenBuf[tokenMaxSize];
			if(StripToken(tokenBuf, tokenMaxSize))
			{
				outString = tokenBuf;
				StripWhitespace();
				return MatchChar(')');
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigParser::StripIntProperty(int& outInt)
{
	if(m_ConfigString)
	{
		StripWhitespace();
		if(MatchChar('('))
		{
			StripWhitespace();

			const int tokenMaxSize = 256;
			char tokenBuf[tokenMaxSize];
			if(StripToken(tokenBuf, tokenMaxSize))
			{
				outInt = atoi(tokenBuf);
				StripWhitespace();
				return MatchChar(')');
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigParser::StripBoolProperty(bool& outBool)
{
	if(m_ConfigString)
	{
		StripWhitespace();
		if(MatchChar('('))
		{
			StripWhitespace();

			const int tokenMaxSize = 256;
			char tokenBuf[tokenMaxSize];
			if(StripToken(tokenBuf, tokenMaxSize))
			{
				if(!stricmp(tokenBuf, "false") || !stricmp(tokenBuf, "0"))
				{
					outBool = false;
				}
				else if(!stricmp(tokenBuf, "true") || !stricmp(tokenBuf, "1"))
				{
					outBool = true;
				}
				else
				{
					return false;
				}

				StripWhitespace();
				return MatchChar(')');
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigParser::StripFloatArrayProperty(int& inoutCount, float* inoutFloats)
{
	if(m_ConfigString)
	{
		StripWhitespace();
		if(MatchChar('('))
		{
			for(int i=0; i<inoutCount; ++i)
			{
				StripWhitespace();

				const int tokenMaxSize = 256;
				char tokenBuf[tokenMaxSize];
				if(StripToken(tokenBuf, tokenMaxSize))
				{
					inoutFloats[i] = float(atof(tokenBuf));
					StripWhitespace();
					if(MatchChar(')'))
					{
						inoutCount = i+1;
						return true;
					}
					else if(!MatchChar(','))
					{
						return false;
					}
				}
			}
			
			return false;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigParser::StripVector3Property(Vec3V_InOut inoutVector3)
{
	int count = 3;
	return StripFloatArrayProperty(count, &inoutVector3[0]) && (count == 3);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigSampler(ConfigParser& parser)
{
	parser.StripWhitespace();
	if(parser.MatchChar('('))
	{
		const int tokenMaxSize = 256;
		char tokenBuf[tokenMaxSize];

		do
		{
			parser.StripWhitespace();
			if(parser.StripToken(tokenBuf, tokenMaxSize))
			{
				if(!ConfigProperty(tokenBuf, parser))
				{
					// TODO --- error failed to use token
				}
			}
			parser.StripWhitespace();

			if(parser.MatchChar(')'))
			{
				break;
			}
			else if(!parser.MatchChar(','))
			{
				// TODO --- error
				return false;
			}
		}
		while(parser.m_ConfigString);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::ConfigProperty(const char* token, ConfigParser& parser)
{
	if(!stricmp(token, "SpaceScale"))
	{
		int numDimensions = m_Parameterization.GetNumDimensions();
		float* spaceScale = Alloca(float, numDimensions);

		if(parser.StripFloatArrayProperty(numDimensions, spaceScale))
		{
			for(int i=0; i<numDimensions; ++i)
			{
				m_Parameterization.GetDimensionInfo(i).SetSpaceScale(spaceScale[i]);
			}
			return true;
		}

		return false;
	}
	else if(!stricmp(token, "ParameterScale"))
	{
		int numDimensions = m_Parameterization.GetNumDimensions();
		float* parameterScale = Alloca(float, numDimensions);

		if(parser.StripFloatArrayProperty(numDimensions, parameterScale))
		{
			m_ParameterScale.Reset();
			m_ParameterScale.Resize(numDimensions);

			for(int i=0; i<numDimensions; ++i)
			{
				m_ParameterScale[i] = parameterScale[i];
			}
			return true;
		}

		return false;
	}
	else if(!stricmp(token, "ParameterOffset"))
	{
		int numDimensions = m_Parameterization.GetNumDimensions();
		float* parameterOffset = Alloca(float, numDimensions);

		if(parser.StripFloatArrayProperty(numDimensions, parameterOffset))
		{
			m_ParameterOffset.Reset();
			m_ParameterOffset.Resize(numDimensions);

			for(int i=0; i<numDimensions; ++i)
			{
				m_ParameterOffset[i] = parameterOffset[i];
			}
			return true;
		}

		return false;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSampler::SamplerTypeInfo::SamplerTypeInfo(crpmParameterSampler::eSamplerTypes samplerType, const char* name, SamplerCreateFn* createFn, size_t size)
: m_SamplerType(samplerType)
, m_Name(name)
, m_CreateFn(createFn)
, m_Size(size)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSampler::SamplerTypeInfo::~SamplerTypeInfo()
{
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSampler::SamplerTypeInfo::Register() const
{
	crpmParameterSampler::RegisterSamplerTypeInfo(*this);
}

////////////////////////////////////////////////////////////////////////////////

const crpmParameterSampler::SamplerTypeInfo* crpmParameterSampler::FindSamplerTypeInfo(crpmParameterSampler::eSamplerTypes samplerType)
{
	// if build in sampler type, retrieve from first kSamplerTypeNum entries
	if(samplerType < kSamplerTypeNum)
	{
		AssertMsg(samplerType < sm_SamplerTypeInfos.GetCount() , "crpmParameterSampler::FindSamplerTypeInfo - built in sampler types not registered, missing crpmParameterSampler::InitClass call?");
		return sm_SamplerTypeInfos[samplerType];
	}
	else
	{
		// non-built in (custom) sampler type, search any remaining entries
		for(int i=kSamplerTypeNum; i<sm_SamplerTypeInfos.GetCount(); ++i)
		{
			if(sm_SamplerTypeInfos[i] && sm_SamplerTypeInfos[i]->GetSamplerType() == samplerType)
			{
				return sm_SamplerTypeInfos[i];
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

const crpmParameterSampler::SamplerTypeInfo* crpmParameterSampler::FindSamplerTypeInfo(const char* samplerName)
{
	for(int i=0; i<sm_SamplerTypeInfos.GetCount(); ++i)
	{
		if(sm_SamplerTypeInfos[i] && !stricmp(sm_SamplerTypeInfos[i]->GetSamplerName(), samplerName))
		{
			return sm_SamplerTypeInfos[i];
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSampler::RegisterSamplerTypeInfo(const crpmParameterSampler::SamplerTypeInfo& info)
{
	eSamplerTypes samplerType = info.GetSamplerType();

	// if built in type, store in first kSamplerTypeNum entries (which should already be allocated)
	if(samplerType < kSamplerTypeNum)
	{
		AssertMsg(sm_SamplerTypeInfos.GetCount() >= kSamplerTypeNum , "crpmParameterSampler::RegisterSamplerTypeInfo - build in sampler type attempting to register before call to crpmParameterSampler::InitClass");
		AssertMsg(!sm_SamplerTypeInfos[samplerType] , "crpmParameterSampler::RegisterSamplerTypeInfo - built in sampler type registered twice");

		sm_SamplerTypeInfos[samplerType] = &info;
	}
	else
	{
		// non-built in (custom) sampler type, grow array and append
		AssertMsg(sm_SamplerTypeInfos.GetCount() >= kSamplerTypeNum , "crpmParameterSampler::RegisterNodeTypeInfo - custom sampler type attempting to register before call to crpmParameterSampler::InitClass");
		AssertMsg(!FindSamplerTypeInfo(samplerType) , "crpmParameterSampler::RegisterSamplerTypeInfo - custom sampler type registered twice");

		sm_SamplerTypeInfos.Grow() = &info;
	}
}

////////////////////////////////////////////////////////////////////////////////

atArray<const crpmParameterSampler::SamplerTypeInfo*> crpmParameterSampler::sm_SamplerTypeInfos;

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSampler::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerPhysical::crpmParameterSamplerPhysical(eSamplerTypes samplerType)
: crpmParameterSampler(samplerType)
, m_MotionControllerRc(NULL)
, m_SampleRate(1.f/30.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerPhysical::~crpmParameterSamplerPhysical()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerPhysical::Shutdown()
{
	if(m_MotionControllerRc)
	{
		delete m_MotionControllerRc;
		m_MotionControllerRc = NULL;
	}

	crpmParameterSampler::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerPhysical::Reset() const
{
	crpmParameterSampler::Reset();

	if(m_MotionControllerRc)
	{
		delete m_MotionControllerRc;
		m_MotionControllerRc = NULL;

		if(m_RcData != NULL)
		{
			m_MotionControllerRc = rage_new crpmMotionControllerRc(*m_RcData, *m_Clips, NULL, *m_SkelData, m_FilterWeightSet, m_FilterMover);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerPhysical::CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const
{
	if(m_MotionControllerRc)
	{
		m_MotionControllerRc->SetLooped(false);
		m_MotionControllerRc->SetWeights(weights);
		m_MotionControllerRc->Reset();

		CalcParameterDerivedWithMotionController(*m_MotionControllerRc, outParameter);
	}
	else
	{
		const int numWeights = weights.GetNumWeights();
		Assert(numWeights > 0);

		int primaryWeightIdx = -1;
		for(int i=0; i<numWeights; ++i)
		{	
			if(IsClose(weights.GetNormalizedWeight(i), 1.f))
			{
				primaryWeightIdx = i;
			}
		}
		Assert(primaryWeightIdx >= 0);

		crpmMotionControllerClip mcClip(*m_Clips->GetClip(primaryWeightIdx), *m_SkelData, m_FilterWeightSet, m_FilterMover);
		
		CalcParameterDerivedWithMotionController(mcClip, outParameter);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerPhysical::PreInit(const crpmRegistrationCurveData* rcData, const crClips& clips, const crSkeletonData& skelData, const crWeightSet* filterWeightSet, bool filterMover)
{
	crpmParameterSampler::PreInit(rcData, clips, skelData, filterWeightSet, filterMover);

	if(rcData != NULL)
	{
		m_MotionControllerRc = rage_new crpmMotionControllerRc(*rcData, clips, NULL, skelData, filterWeightSet, filterMover);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerPhysical::ConfigProperty(const char* token, ConfigParser& parser)
{
	if(!stricmp(token, "SampleRate"))
	{
		return parser.StripFloatProperty(m_SampleRate);
	}

	return crpmParameterSampler::ConfigProperty(token, parser);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerContainer::crpmParameterSamplerContainer(eSamplerTypes samplerType)
: crpmParameterSampler(samplerType)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterSamplerContainer::~crpmParameterSamplerContainer()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRPM_IMPLEMENT_PARAMETER_SAMPLER_TYPE(crpmParameterSamplerContainer, crpmParameterSampler::kSamplerTypeContainer);

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerContainer::Shutdown()
{
	const int numSamplers = m_Samplers.GetCount();
	for(int i=0; i<numSamplers; ++i)
	{
		m_Samplers[i]->Shutdown();
		delete m_Samplers[i];
	}

	m_Samplers.Reset();
	m_Parameters.Reset();

	crpmParameterSampler::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerContainer::AppendSampler(crpmParameterSampler& sampler)
{
	m_Samplers.Grow() = &sampler;
	m_Parameters.Grow().Init(sampler.GetNumParameterDimensions());

	const int numParameters = sampler.GetNumParameterDimensions();
	for(int i=0; i<numParameters; ++i)
	{
		m_Parameterization.AppendDimensionInfo() = sampler.GetParameterization().GetDimensionInfo(i);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterSamplerContainer::CalcParameterDerived(const crpmWeights& weights, crpmParameter& outParameter) const
{
	const int numSamplers = m_Samplers.GetCount();
	int parameterCount = 0;
	for(int i=0; i<numSamplers; ++i)
	{
		m_Samplers[i]->CalcParameter(weights, m_Parameters[i]);

		const int numParameters = m_Samplers[i]->GetNumParameterDimensions();
		for(int j=0; j<numParameters; ++j)
		{
			outParameter.SetValue(j+parameterCount, m_Parameters[i].GetValue(j));
		}

		parameterCount += numParameters;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerContainer::DebugMapParameterDerived(const crpmParameter& parameter, const crSkeleton& skel, Vec3V_InOut v) const
{
	if(m_Samplers.GetCount() > 0)
	{
		return m_Samplers[0]->DebugMapParameter(parameter, skel, v);
	}
	
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterSamplerContainer::ConfigProperty(const char* token, ConfigParser& parser)
{
	crpmParameterSampler* sampler = crpmParameterSampler::CreateSampler(token);
	if(sampler)
	{
		sampler->PreInit(parser.m_RcData, *parser.m_Clips, *parser.m_SkelData, parser.m_FilterWeightSet, parser.m_FilterMover);
		if(sampler->ConfigSampler(parser))
		{
			AppendSampler(*sampler);
			return true;
		}
		delete sampler;
		return false;
	}

	return crpmParameterSampler::ConfigProperty(token, parser);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
