//
// crparameterizedmotion/blenddatabasecalculator.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "blenddatabasecalculator.h"

#include "blenddatabase.h"
#include "parameterization.h"
#include "parametersampler.h"
#include "registrationcurve.h"

#include "crclip/clip.h"
#include "crclip/clips.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmBlendDatabaseCalculator::crpmBlendDatabaseCalculator()
: m_MaxNumEntries(0)
, m_DistanceThreshold(0.f)
, m_SourceDistanceThreshold(0.f)
, m_ParameterExtensionFactor(0.f)
, m_StochasticPopulate(true)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmBlendDatabaseCalculator::crpmBlendDatabaseCalculator(int maxNumEntries, float distanceThreshold, float sourceDistanceThreshold, float parameterExtensionFactor, bool stochasticPopulate, const char* debugPath)
: m_MaxNumEntries(0)
, m_DistanceThreshold(0.f)
, m_SourceDistanceThreshold(0.f)
, m_ParameterExtensionFactor(0.f)
, m_StochasticPopulate(true)
{
	Init(maxNumEntries, distanceThreshold, sourceDistanceThreshold, parameterExtensionFactor, stochasticPopulate, debugPath);
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabaseCalculator::Init(int maxNumEntries, float distanceThreshold, float sourceDistanceThreshold, float parameterExtensionFactor, bool stochasticPopulate, const char* debugPath)
{
	Assert(maxNumEntries > 1);
	Assert(distanceThreshold > 0.f);
	Assert(sourceDistanceThreshold >= 0.f);
	Assert(parameterExtensionFactor >= 0.f);

	m_MaxNumEntries = maxNumEntries;
	m_DistanceThreshold = distanceThreshold;
	m_SourceDistanceThreshold = sourceDistanceThreshold;
	m_ParameterExtensionFactor = parameterExtensionFactor;
	m_StochasticPopulate = stochasticPopulate;

	m_DebugPath = debugPath;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmBlendDatabaseCalculator::Calculate(crpmBlendDatabase& inoutBlendDatabase, const crpmParameterSampler& sampler, crClips* clips, crpmRegistrationCurveData* rcData, const Variations* variations, const std::vector<std::vector<int> >* validCombinations)
{
	Assert(inoutBlendDatabase.GetNumParameterDimensions());
	Assert(clips || !rcData);
	Assert(!(variations && validCombinations));

	atArray<crpmBlendDatabase::Entry> sourceEntries;
	CalcSourceEntries(sourceEntries, sampler);

	InitParameterSpace(inoutBlendDatabase, sourceEntries);
	
	if(!variations)
	{
		SimplifySourceEntries(inoutBlendDatabase, sourceEntries, clips, rcData);
		sampler.Reset();
	}

	std::vector<std::vector<int> > autoValidCombinations;
	if(!validCombinations)
	{
		GenerateValidCombinations(inoutBlendDatabase, sourceEntries, variations, autoValidCombinations);
		validCombinations = &autoValidCombinations;
	}

	PopulateDatabase(inoutBlendDatabase, sourceEntries, sampler, *validCombinations, variations);

	SortDatabase(inoutBlendDatabase, -1);

	inoutBlendDatabase.m_AveMinDistance = CalcAverageMinDistanceToParameter(inoutBlendDatabase);

	if(m_DebugPath.GetLength())
	{

	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabaseCalculator::CalcSourceEntries(atArray<crpmBlendDatabase::Entry>& outSourceEntries, const crpmParameterSampler& sampler) const
{
	const int numSources = sampler.GetNumWeightDimensions();
	const int numParameterDims = sampler.GetNumParameterDimensions();

	outSourceEntries.Reserve(numSources);

	crpmWeights weights(numSources, 0.f);
	for(int i=0; i<numSources; ++i)
	{
		weights.ClearWeights(0.f);
		weights.SetWeight(i, 1.f);

		crpmParameter parameter(numParameterDims);
		sampler.CalcParameter(weights, parameter);

		Printf("source entry[%d] parameters", i);
		for(int d=0; d<numParameterDims; ++d)
		{
			Printf(" %f", parameter.GetValue(d));
		}
		Printf("\n");

		outSourceEntries.Grow() = crpmBlendDatabase::Entry(parameter, weights, 0);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabaseCalculator::SimplifySourceEntries(crpmBlendDatabase& bd, atArray<crpmBlendDatabase::Entry>& inoutSourceEntries, crClips* clips, crpmRegistrationCurveData* rcData) const
{
	if(m_SourceDistanceThreshold > 0.f)
	{
		const float minDistSqr = square(m_SourceDistanceThreshold);
		for(int i=0; i<inoutSourceEntries.size(); ++i)
		{
			for(int j=i+1; j<inoutSourceEntries.size(); ++j)
			{
				const int refClipIdx = rcData?rcData->m_ReferenceClipIndex:0;
				int ri = (refClipIdx+i)%inoutSourceEntries.size();
				int rj = (refClipIdx+j)%inoutSourceEntries.size();

				float distSqr = bd.CalcParameterDistanceSqr(inoutSourceEntries[ri].GetParameter(), inoutSourceEntries[rj].GetParameter());
				if(distSqr < minDistSqr)
				{
					inoutSourceEntries.Delete(rj);
					if(clips)
					{
						Displayf("Removing source entry '%s'", clips->GetClip(rj)->GetName());
						clips->RemoveClip(rj);
					}
					if(rcData)
					{
						if(rcData->m_AlignmentCurves[rj])
						{
							delete rcData->m_AlignmentCurves[rj];
						}
						rcData->m_AlignmentCurves.Delete(rj);
						rcData->m_NumClips--;

						if(j < rcData->m_ReferenceClipIndex)
						{
							rcData->m_ReferenceClipIndex--;
						}
					}
					--j;
				}
			}
		}

		crpmWeights weights(inoutSourceEntries.size());
		for(int i=0; i<inoutSourceEntries.size(); ++i)
		{
			weights.ClearWeights(0.f);
			weights.SetWeight(i, 1.f);

			inoutSourceEntries[i] = crpmBlendDatabase::Entry(inoutSourceEntries[i].GetParameter(), weights, 0);
		}

		bd.m_NumWeightDimensions = inoutSourceEntries.size();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabaseCalculator::InitParameterSpace(crpmBlendDatabase& bd, const atArray<crpmBlendDatabase::Entry>& sourceEntries) const
{
	bd.m_MinCorner.SetValues(FLT_MAX);
	bd.m_MaxCorner.SetValues(-FLT_MAX);

	const int numDim = bd.GetNumParameterDimensions();

	// find the min and max corners
	const int numSourceEntries = sourceEntries.size();
	for(int i=0; i<numSourceEntries; ++i)
	{
		const crpmParameter& parameter = sourceEntries[i].GetParameter();

		for(int d=0; d<numDim; ++d)
		{
			bd.m_MinCorner[d] = Min(parameter[d], bd.m_MinCorner[d]);
			bd.m_MaxCorner[d] = Max(parameter[d], bd.m_MaxCorner[d]);
		}
	}

	bd.m_Extent = bd.m_MaxCorner;
	bd.m_Extent -= bd.m_MinCorner;

	// now extend each dimension by a factor
	for(int d=0; d<numDim; ++d)
	{
		float span = bd.m_MaxCorner[d] - bd.m_MinCorner[d];

		// extend in both directions by half extension factor
		float extension = span*(m_ParameterExtensionFactor*0.5f);

		bd.m_MinCornerExtended[d] = bd.m_MinCorner[d] - extension;
		bd.m_MaxCornerExtended[d] = bd.m_MaxCorner[d] + extension;

		// set inverse extent for this dimension
		if(bd.m_Extent[d] > SMALL_FLOAT)
		{
			bd.m_InvExtent[d] = 1.f / bd.m_Extent[d];
		}
		else
		{
			bd.m_InvExtent[d] = 0.f;
			Warningf("crpmBlendDatabaseCalculator::InitParameterSpace - parameter dimension[%d] has minimal extent %f.  Either omit this parameter, or add additional samples with more variation in this dimension.", d, bd.m_Extent[d]);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabaseCalculator::PopulateDatabase(crpmBlendDatabase& bd, const atArray<crpmBlendDatabase::Entry>& sourceEntries, const crpmParameterSampler& sampler, const ValidCombinations& validCombinations, const Variations* variations) const
{
	// populate with source entries first
	const int numSourceEntries = sourceEntries.size();
	if(numSourceEntries < (bd.GetNumParameterDimensions()+1))
	{
		Warningf("crpmBlendDatabaseCalculator::PopulateDatabase - not enough source entries for parameter dimensions");
	}

	bd.m_Entries.Reserve(Clamp(m_MaxNumEntries, numSourceEntries, 65534));

	for(int i=0; i<numSourceEntries; ++i)
	{
		crpmWeights weights(bd.GetNumWeightDimensions());
		sourceEntries[i].GetWeights(weights);

		if(!InsertIntoDatabase(bd, sourceEntries[i].GetParameter(), weights, variations, m_SourceDistanceThreshold))
		{
			Warningf("crpmBlendDatabaseCalculator::PopulateDatabase - source entry[%d] rejected, too close to other source entires", i);
		}
	}

	const int numValidCombinations = (int)validCombinations.size();
	for(int i=0; i<numValidCombinations; ++i)
	{
		if(m_StochasticPopulate)
		{
			const float maxConfidence = 0.95f;
			const int minFailedInserts = 10;

			const int maxPossibleInserts = Clamp(int(CalcHypersphereVolume(bd, validCombinations[i]) * float(m_MaxNumEntries)), 1, m_MaxNumEntries);
			const int existingInserts = Max(CalcNumHypersphereEntries(bd, validCombinations[i]), bd.GetNumParameterDimensions());

//			Printf("EXISTING INSERTS %d MAX POSSIBLE %d\n", existingInserts, maxPossibleInserts);

			float successRate = 1.f - Min(float(existingInserts) / float(maxPossibleInserts), 0.99f);
			int maxFailedInserts = Max(int(log(1.f-maxConfidence)/log(1.f-successRate)), minFailedInserts);

			Printf("combination %d/%d\n", i, numValidCombinations);

//			Printf("INITIAL SUCCESS RATE %f INSERTS %d\n", successRate, maxFailedInserts);

			int numInserts = 0;
			int numFailedInserts = 0;
			int totalFailedInserts = 0;

			while(bd.GetNumEntries() < m_MaxNumEntries)
			{
				crpmWeights weights;
				GenerateRandomWeights(bd, weights, validCombinations[i]);
				
				crpmParameter parameter(bd.GetNumParameterDimensions());
				sampler.CalcParameter(weights, parameter);

				if(InsertIntoDatabase(bd, parameter, weights, variations, m_DistanceThreshold))
				{
					successRate = Min(1.f/float(numFailedInserts+1), successRate);
					maxFailedInserts = Max(int(log(1.f-maxConfidence)/log(1.f-successRate)), minFailedInserts);

//					Printf("REVISED SUCCESS RATE %f INSERTS %d\n", successRate, maxFailedInserts);

					totalFailedInserts += numFailedInserts;
					numFailedInserts = 0;
					numInserts++;
				}
				else if(++numFailedInserts > maxFailedInserts)
				{
					Printf("inserts %d fails %d repeated fails %d total inserts %d\n", numInserts, totalFailedInserts+numFailedInserts, numFailedInserts, bd.GetNumEntries());
					break;
				}
			}
		}
		else
		{
			Printf("combination %d/%d\n", i, numValidCombinations);

			crpmWeights weights;
			weights.Init(bd.GetNumWeightDimensions(), 0.f);

//			Printf("BARYCENTRIC SEARCH\n");
			const int fails = 8;
			OUTPUT_ONLY(int numInserts =) RecursiveBarycentricInsert(bd, sampler, variations, validCombinations[i], weights, 0, fails);

			OUTPUT_ONLY(Printf("inserts %d total inserts %d\n", numInserts, bd.GetNumEntries()));
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

int crpmBlendDatabaseCalculator::RecursiveBarycentricInsert(crpmBlendDatabase& bd, const crpmParameterSampler& sampler, const Variations* variations, const std::vector<int>& vc, crpmWeights& weights, int dimension, int fails) const
{
	const int numDimensions = (int)vc.size();
	int vcd = vc[dimension];

	int inserts = 0;

	if(dimension < (numDimensions-2) || (dimension < (numDimensions-1) && weights.GetWeightSum() > 0.f))
	{	
		weights.SetWeight(vcd, 0.f);
		inserts = RecursiveBarycentricInsert(bd, sampler, variations, vc, weights, dimension+1, fails);
	}
	if(dimension < (numDimensions-1))
	{
		const int passLimit = 8;
		for(int p=1; p<passLimit; p++)
		{
			int inst = 0;
			const int n = 1<<p;
			for(int i=1; i<n; ++i)
			{
				if(i&1)
				{
					weights.SetWeight(vcd, 0.f);

					float w = float(i)/float(n);
					if((weights.GetWeightSum() + w - SMALL_FLOAT) < 1.f)
					{
						weights.SetWeight(vcd, w);
						inst += RecursiveBarycentricInsert(bd, sampler, variations, vc, weights, dimension+1, fails);
					}
					else
					{
						break;
					}
				}
			}

			if(!inst)
			{
				if(!fails)
				{
					break;
				}
				fails--;
			}
			inserts += inst;
		}
	}
	else
	{
		weights.SetWeight(vcd, 1.f-weights.GetWeightSum());

//		for(int i=0; i<numDimensions; ++i)
//		{
//			Printf("%.3f ", weights.GetWeight(vc[i]));
//		}

		crpmParameter parameter;
		parameter.Init(bd.GetNumParameterDimensions());
		sampler.CalcParameter(weights, parameter);

		if(InsertIntoDatabase(bd, parameter, weights, variations, m_DistanceThreshold))
		{
//			Printf("INSERT");
			inserts++;
		}
//		Printf("\n");
	}

	weights.SetWeight(vcd, 0.f);
	return inserts;

}

////////////////////////////////////////////////////////////////////////////////

bool crpmBlendDatabaseCalculator::InsertIntoDatabase(crpmBlendDatabase& bd, const crpmParameter& parameter, const crpmWeights& weights, const Variations* variations, float distanceThreshold) const
{
	if(!bd.IsParameterInsideExtendedBoundaries(parameter))
	{
		return false;
	}

	u32 v = 0;
	if(variations)
	{
		CalcVariationsFromWeights(*variations, weights, v, true);
	}

	float distance = bd.FindMinDistanceToParameter(parameter, v);
	if(distance >= distanceThreshold)
	{
		if(m_DebugPath.GetLength())
		{
			Printf("%d", bd.m_Entries.size());
			for(int i=0; i<parameter.GetNumDimensions(); ++i)
			{
				Printf(",%f", parameter.GetValue(i));
			}
			for(int i=0; i<weights.GetNumWeights(); ++i)
			{
				Printf(",%f", weights.GetWeight(i));
			}
			Printf("\n");
		}

		bd.m_Entries.Grow() = rage_new crpmBlendDatabase::Entry(parameter, weights, v);
		return true;
	}
	else
	{
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////

int crpmBlendDatabaseCalculator::CalcVariationsFromWeights(const Variations& variations, const crpmWeights& weights, u32& outVariations, bool top) const
{
	int nv = (int) variations.second.size();
	bool found = false;
	for(u32 i=0; i<variations.second.size(); ++i)
	{
		for(u32 j=0; j<variations.second[i].first.size(); ++j)
		{
			if(weights.GetWeight(variations.second[i].first[j]) > 0.f)
			{
				outVariations |= 1<<i;
				found = true;
				break;
			}
		}

		for(u32 j=0; j<variations.second[i].second.size(); ++j)
		{
			u32 var = 0;
			int nvj = CalcVariationsFromWeights(variations.second[i].second[j], weights, var, false);
			if(var != u32((1<<nvj)-1))
			{
				outVariations |= 1<<i;
				found = true;
			}
			outVariations |= var<<nv;
			nv += nvj;
		}
	}
	if(!found)
	{
		outVariations |= (1<<variations.second.size())-1;
	}
	if(top)
	{
		outVariations >>= 1;
	}
	return nv;
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabaseCalculator::GenerateRandomWeights(const crpmBlendDatabase& bd, crpmWeights& outWeights, const std::vector<int>& validCombination) const
{
#if 1
	Assert(validCombination.size() > 0);

	// initialize weights to zero
	const int numWeightDims = bd.GetNumWeightDimensions();
	outWeights.Init(numWeightDims, 0.f);

	// max weights is parameter dimension plus one - minimum to form volume in parameter space
	const int parameterDim = bd.GetNumParameterDimensions();
	int maxWeights = Min(parameterDim+1, int(validCombination.size()));

	// build random remap list
	int* remap = Alloca(int, maxWeights);
	for(int i=0; i<maxWeights; ++i)
	{
		while(1)
		{
			remap[i] = m_Rand.GetRanged(0, (int)validCombination.size()-1);

			int j=0;
			for(j=0; j<i; j++)
			{
				if(remap[j] == remap[i])
				{
					break;
				}
			}
			if(j==i)
			{
				break;
			}
		}
	}

	// assign weights
	for(int i=0; i<maxWeights; ++i)
	{
		int weightIdx = validCombination[remap[i]];

		// set weight, either a random fraction of what remains, or all if last
		if(i < (maxWeights-1))
		{
			outWeights.SetWeight(weightIdx, m_Rand.GetRanged(0.f, 1.f-outWeights.GetWeightSum()));
		}
		else
		{
			outWeights.SetWeight(weightIdx, 1.f-outWeights.GetWeightSum());
		}
	}
#else
	// initialize weights to zero
	const int numWeightDims = bd.GetNumWeightDimensions();
	weights.Init(numWeightDims, 0.f);

	mthRandom& rand = g_ReplayRand;

	// select a random valid combination set (or set all bits if no valid combinations)
	u64 vcBits = (1<<numWeightDims)-1;
	if(validCombinations)
	{
		int vcIdx = rand.GetRanged(0, validCombinations->size()-1);
		Assert((*validCombinations)[vcIdx].size() > 0);
		for(int i=0; i<int((*validCombinations)[vcIdx].size()); ++i)
		{
			Assert((*validCombinations)[vcIdx][i] < numWeightDims);
			vcBits |= 1ULL<<u64((*validCombinations)[vcIdx][i]);
		}
	}

	// max bits is parameter dimension plus one - minimum to form volume in parameter space
	const int parameterDim = bd.GetNumParameterDimensions();
	int maxBits = parameterDim+1;

	// given blend group, randomly select bits until optimum bits selected (or blend group exhausted)
	int numBits = 0;
	u64 bits = 0;
	while((numBits < maxBits) && (bits != vcBits))
	{
		u64 b = 1<<(rand.GetRanged(0, numWeightDims-1));
		if((bits & b) == 0)
		{
			bits |= b;
			numBits++;
		}
	}

	// assign weights
	while(numBits > 0)
	{
		Assert(bits != 0);

		// randomly select bit from remaining subset of blend group
		int bit = rand.GetRanged(0, numBits-1);
		int weightIdx=0;
		for(u64 w=1; weightIdx<numWeightDims; ++weightIdx,w<<=1)
		{
			if(bits & w)
			{
				if(!bit--)
				{
					bits &= ~w;
					--numBits;
					break;
				}
			}
		}
		Assert(weightIdx < numWeightDims);

		// set weight, either a random fraction of what remains, or all if last
		if(numBits > 0)
		{
			weights.SetWeight(weightIdx, rand.GetRanged(0.f, 1.f-weights.GetWeightSum()));
		}
		else
		{
			weights.SetWeight(weightIdx, 1.f-weights.GetWeightSum());
		}
	}
#endif
}

////////////////////////////////////////////////////////////////////////////////

bool SortPairsOnSecond(const std::pair<std::vector<int>, float>& lhs, const std::pair<std::vector<int>, float>& rhs)
{
	return lhs.second < rhs.second;
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabaseCalculator::GenerateValidCombinations(const crpmBlendDatabase& bd, atArray<crpmBlendDatabase::Entry>& sourceEntries, const Variations* variations, ValidCombinations& validCombinations) const
{
	// max weights ideally parameter dimension plus one - minimum to form volume in parameter space
	const int parameterDim = bd.GetNumParameterDimensions();
	int numWeights = Min(parameterDim+1, bd.GetNumWeightDimensions());

	std::vector<int> weightIndices;
	weightIndices.resize(numWeights);
	std::vector<std::pair<std::vector<int>, float> > combinations;
	GenerateCombinations(0, bd.GetNumWeightDimensions(), variations, weightIndices, combinations);

	// now sort the combinations based on max parameter distance
	const int numCombinations = (int)combinations.size();
	if(numCombinations > 1)
	{
//		const float maxDistance = sqrt(float(bd.GetNumParameterDimensions()));
		for(int i=0; i<numCombinations; ++i)
		{
			float distance = -FLT_MAX;
		
			for(int j=0; j<(numWeights-1); ++j)
			{
				const crpmParameter& pj = sourceEntries[combinations[i].first[j]].GetParameter();
				for(int k=j+1; k<numWeights; ++k)
				{
					const crpmParameter& pk = sourceEntries[combinations[i].first[k]].GetParameter();
					
					float d = bd.CalcParameterDistanceSqr(pj, pk);
					distance = Max(d, distance);
				}
			}

			combinations[i].second = distance;
//			combinations[i].second = (maxDistance-distance) + combinations[i].second*maxDistance;
		}

		std::sort(combinations.begin(), combinations.end(), SortPairsOnSecond);
	}

	// populate valid combination array with sorted combinations
	validCombinations.reserve(numCombinations);
	for(int i=0; i<numCombinations; ++i)
	{
		validCombinations.push_back(combinations[i].first);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabaseCalculator::GenerateCombinations(int weightIdx, int numWeightDimensions, const Variations* variations, std::vector<int>& weightIndices, std::vector<std::pair<std::vector<int>, float> >& combinations) const
{
	if(weightIdx < int(weightIndices.size()))
	{
		int prevWeight = -1;
		if(weightIdx > 0)
		{
			prevWeight = weightIndices[weightIdx-1];
		}

		for(int i=prevWeight+1; i<numWeightDimensions; ++i)
		{
			weightIndices[weightIdx] = i;
			GenerateCombinations(weightIdx+1, numWeightDimensions, variations, weightIndices, combinations);
		}
	}
	else
	{
		int numVariations = 0;
		if(variations)
		{
			if((numVariations = CheckCombinationVariations(weightIndices, *variations)) < 0)
			{
				return;
			}
			numVariations--;
		}
		combinations.push_back(std::pair<std::vector<int>, float>(weightIndices, float(numVariations)));
	}
}

////////////////////////////////////////////////////////////////////////////////

int crpmBlendDatabaseCalculator::CheckCombinationVariations(const std::vector<int>& weightIndices, const Variations& variations) const
{
	int found = 0;
	for(u32 i=0; i<variations.second.size(); ++i)
	{
		int fi=0;
		for(u32 j=0; j<variations.second[i].first.size() && !fi; ++j)
		{
			for(u32 wi=0; wi<weightIndices.size(); ++wi)
			{
				if(variations.second[i].first[j] == weightIndices[wi])
				{
					++fi;
					break;
				}
			}
		}

		for(u32 j=0; j<variations.second[i].second.size(); ++j)
		{
			int fj = CheckCombinationVariations(weightIndices, variations.second[i].second[j]);
			if(fj<0)
			{
				return -1;
			}
			fi += fj;
		}

		if(found>0 && fi>0)
		{
			return -1;
		}
		found += fi;
	}
	return found;
}

////////////////////////////////////////////////////////////////////////////////

void crpmBlendDatabaseCalculator::SortDatabase(crpmBlendDatabase& bd, int sortDimension) const
{
	if(sortDimension < 0)
	{
		const int numDimensions = bd.GetNumParameterDimensions();

		crpmParameter extent(numDimensions);
		bd.GetParameterExtent(extent);

		sortDimension = 0;
		for(int d=1; d<numDimensions; ++d)
		{
			if(extent.GetValue(d) > extent.GetValue(sortDimension))
			{
				sortDimension = d;
			}
		}
	}

	Assert(InRange(sortDimension, 0, bd.GetNumParameterDimensions()-1));

	const int numEntries = bd.GetNumEntries();
	for(int i=numEntries-1; i>=0; --i)
	{
		bool swap = false;
		for(int j=0; j<i; ++j)
		{
			crpmBlendDatabase::Entry** ej0 = &bd.m_Entries[j].ptr;
			crpmBlendDatabase::Entry** ej1 = &bd.m_Entries[j+1].ptr;

			const float val0 = (*ej0)->GetParameter().GetValue(sortDimension);
			const float val1 = (*ej1)->GetParameter().GetValue(sortDimension);

			if(val1 < val0)
			{
				SwapEm(*ej0, *ej1);
				swap = true;
			}
		}

		if(!swap)
		{
			break;
		}
	}

	bd.m_SortDimension = sortDimension;
}

////////////////////////////////////////////////////////////////////////////////

float crpmBlendDatabaseCalculator::CalcAverageMinDistanceToParameter(crpmBlendDatabase& bd) const
{
	Assert(bd.GetNumEntries() > 1);

	float distanceSum = 0.f;
	int n = 0;

	const int numEntries = bd.GetNumEntries();
	for(int i=0; i<numEntries; ++i)
	{
		const crpmBlendDatabase::Entry& ei = *bd.m_Entries[i];

		float minDistSqr = FLT_MAX;
		for(int j=0; j<numEntries; ++j)
		{
			const crpmBlendDatabase::Entry& ej = *bd.m_Entries[j];

			if((i != j) && (ei.GetVariations() == ej.GetVariations()))
			{
				float distSqr = bd.CalcParameterDistanceSqr(ei.GetParameter(), ej.GetParameter());
				minDistSqr = Min(distSqr, minDistSqr);
			}
		}

		if(minDistSqr < FLT_MAX)
		{
			distanceSum += sqrt(minDistSqr);
			n++;
		}
	}

	return distanceSum / float(n);
}

////////////////////////////////////////////////////////////////////////////////

int Factorial(int n)
{
	int r = 1;
	for(int i=n; i>1; --i)
	{
		r *= i;
	}
	return r;
}

////////////////////////////////////////////////////////////////////////////////

float crpmBlendDatabaseCalculator::CalcHypersphereVolume(const crpmBlendDatabase& bd, const std::vector<int>& combination) const
{
	float maxDistance = 0.f;

	const int numEntryDims = (int)combination.size();
	for(int i=0; i<numEntryDims; ++i)
	{
		const crpmBlendDatabase::Entry& ei = *bd.m_Entries[combination[i]];
		for(int j=1; j<numEntryDims; ++j)
		{
			const crpmBlendDatabase::Entry& ej = *bd.m_Entries[combination[j]];
			maxDistance = Max(bd.CalcParameterDistanceSqr(ei.GetParameter(), ej.GetParameter()), maxDistance);
		}
	}
	maxDistance = sqrt(maxDistance);

	float r = maxDistance*0.5f;
	int n = bd.GetNumParameterDimensions();
	float c = (n&1) ? ((pow(2.f, float(n)) * float(Factorial((n-1)/2)) * pow(PI, float((n-1)/2))) / float(Factorial(n)) ) : (pow(PI, float(n/2)) / float(Factorial(n/2)));

//	Printf("HYPERSPHERE VOLUME %f\n", c * pow(r, float(n)));

	return c * pow(r, float(n));
}

////////////////////////////////////////////////////////////////////////////////

int crpmBlendDatabaseCalculator::CalcNumHypersphereEntries(const crpmBlendDatabase& bd, const std::vector<int>& combination) const
{
	crpmParameter center(bd.GetNumParameterDimensions());
	float maxDistanceSqr = 0.f;

	const int numEntryDims = (int)combination.size();
	for(int i=0; i<numEntryDims; ++i)
	{
		const crpmBlendDatabase::Entry& ei = *bd.m_Entries[combination[i]];
		for(int j=1; j<numEntryDims; ++j)
		{
			const crpmBlendDatabase::Entry& ej = *bd.m_Entries[combination[j]];
			float distanceSqr = bd.CalcParameterDistanceSqr(ei.GetParameter(), ej.GetParameter());
			if(distanceSqr > maxDistanceSqr)
			{
				center.Lerp(0.5f, ei.GetParameter(), ej.GetParameter(), &bd.GetParameterization());
				maxDistanceSqr = distanceSqr;
			}
		}
	}

	maxDistanceSqr /= 4.f;

	int n=0;
	const int numEntries = bd.GetNumEntries();
	for(int i=0; i<numEntries; ++i)
	{
		const crpmBlendDatabase::Entry& e = *bd.m_Entries[i];
		float distanceSqr = bd.CalcParameterDistanceSqr(e.GetParameter(), center);
		if(distanceSqr <= maxDistanceSqr)
		{
			n++;
		}
	}

	return n;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
