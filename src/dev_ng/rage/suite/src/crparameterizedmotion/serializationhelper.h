//
// crparameterizedmotion/serializationhelper.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_SERIALIZATIONHELPER_H
#define CRPARAMETERIZEDMOTION_SERIALIZATIONHELPER_H

#include "atl/array.h"
#include "file/serialize.h"

namespace rage
{

// PURPOSE: Serialize ATL array (which was previously an STL vector)
template <class T>
void SerializeAtlArrayAsStlArray(datSerialize& s, atArray<T>& array)
{
	// PROBLEM: STL serialize used 32-bit size counter, ATL uses 16-bit
	unsigned int size = (unsigned int) array.size();
	s << size << datNewLine;
	if (s.IsRead())
	{
		array.Resize(size);
	}

	for (unsigned int i = 0; i < size; ++i)
	{
		s << array[i];
	}
}

} // namespace rage
#endif // CRPARAMETERIZEDMOTION_SERIALIZATIONHELPER_H
