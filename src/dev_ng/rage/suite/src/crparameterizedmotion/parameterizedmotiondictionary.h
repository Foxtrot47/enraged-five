//
// crparameterizedmotion/parameterizedmotiondictionary.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONDICTIONARY_H
#define CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONDICTIONARY_H

#include "parameterizedmotion.h"

#include "crclip/clipdictionary.h"

namespace rage
{

class crpmParameterizedMotionLoader;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Simple parameterized motion container class
class crpmParameterizedMotionDictionary : public pgBaseRefCounted
{
public:

	// PURPOSE: Default constructor
	crpmParameterizedMotionDictionary();

	// PURPOSE: Resource constructor
	crpmParameterizedMotionDictionary(datResource&);

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotionDictionary();

	// PURPOSE: Shutdown, free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmParameterizedMotionDictionary);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Off-line resourcing version
	static const int RORC_VERSION = 4 + crpmParameterizedMotionData::RORC_VERSION + crClipDictionary::RORC_VERSION;

#if CR_DEV
	// PURPOSE: Allocate and load dictionary, from parameterized motion data contained in list file
	// PARAMS:
	// listFilename - filename of a text file, containing a list of parameterized motion filenames (one per line)
	// loader - optional derived parameterized motion loader class, to control loading of parameterized motion (default NULL)
	// subLoader - optional derived clip loader class, to control loading of any clips within parameterized motion (default NULL)
	// subSubLoader - optional derived animation loader class, to control loading of any animations within clips (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: Newly allocated clip dictionary, or NULL if error encountered
	static crpmParameterizedMotionDictionary* AllocateAndLoadParameterizedMotions(const char* listFilename, crpmParameterizedMotionLoader* loader=NULL, crClipLoader* subLoader=NULL, crAnimLoader* subSubLoader=NULL, bool baseNameKeys=false);

	// PURPOSE: Allocate and load dictionary, from parameterized motion file names provided
	// PARAMS:
	// pmFilenames - array of parameterized motion filenames
	// loader - optional derived parameterized motion loader class, to control loading of parameterized motion (default NULL)
	// subLoader - optional derived clip loader class, to control loading of any clips within parameterized motion (default NULL)
	// subSubLoader - optional derived animation loader class, to control loading of any animations within clips (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: Newly allocated clip dictionary, or NULL if error encountered
	static crpmParameterizedMotionDictionary* AllocateAndLoadParameterizedMotions(const atArray<atString>& pmFilenames, crpmParameterizedMotionLoader* loader=NULL, crClipLoader* subLoader=NULL, crAnimLoader* subSubLoader=NULL, bool baseNameKeys=false);

	// PURPOSE: Load dictionary, from parameterized motion data contained in list file
	// PARAMS:
	// listFilename - filename of a text file, containing a list of parameterized motion filenames (one per line)
	// loader - optional derived parameterized motion loader class, to control loading of parameterized motion (default NULL)
	// subLoader - optional derived clip loader class, to control loading of any clips within parameterized motion (default NULL)
	// subSubLoader - optional derived animation loader class, to control loading of any animations within clips (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: true - load successful, false - error encountered
	bool LoadParameterizedMotions(const char* listFilename, crpmParameterizedMotionLoader* loader=NULL, crClipLoader* subLoader=NULL, crAnimLoader* subSubLoader=NULL, bool baseNameKeys=false);

	// PURPOSE: Load dictionary, from parameterized motion file names provided
	// PARAMS:
	// pmFilenames - array of parameterized motion filenames
	// loader - optional derived parameterized motion loader class, to control loading of parameterized motion (default NULL)
	// subLoader - optional derived clip loader class, to control loading of any clips within parameterized motion (default NULL)
	// subSubLoader - optional derived animation loader class, to control loading of any animations within clips (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: true - load successful, false - error encountered
	bool LoadParameterizedMotions(const atArray<atString>& pmFilenames, crpmParameterizedMotionLoader* loader=NULL, crClipLoader* subLoader=NULL, crAnimLoader* subSubLoader=NULL, bool baseNameKeys=false);
#endif // CR_DEV

    // PURPOSE:	Loads a clip dictionary saved out as a resource.
	// The resource is created by rage/base/tools/srorc.
    // PARAMS:	
	// dictionaryFilename - Name of the dictionary; must not have an extension,
	// and "_pdt" is automatically appended to it.
    // RETURNS:	Pointer to clip dictionary object, if load was successful.
    static crpmParameterizedMotionDictionary* LoadResource(const char* dictionaryFilename);

	// PURPOSE: Dictionary key
	typedef u32 PmKey;


	// PURPOSE: Retrieve a parameterized motion using a filename
	// PARAMS: filename - parameterized motion filename
	// RETURNS: pointer to parameterized motion, if found, or NULL if not
	// NOTES: Fast, hashes filename into key then uses map to retrieve
	const crpmParameterizedMotionData* GetParameterizedMotion(const char* filename) const;
	DEPRECATED const crpmParameterizedMotionData* FindParameterizedMotion(const char* filename) const { return GetParameterizedMotion(filename); }

	// PURPOSE: Retrieve a parameterized motion using a filename (non-const)
	// PARAMS: filename - parameterized motion filename
	// RETURNS: Non-const pointer to parameterized motion, if found, or NULL if not
	// NOTES: Fast, hashes filename into key then uses map to retrieve
	crpmParameterizedMotionData* GetParameterizedMotion(const char* filename);
	DEPRECATED crpmParameterizedMotionData* FindParameterizedMotion(const char* filename) { return GetParameterizedMotion(filename); }

	// PURPOSE: Retrieve a parameterized motion using a hash key
	// PARAMS: key - hash key
	// RETURNS: pointer to parameterized motion, if found, or NULL if not
	// NOTES: Very fast, using key to retrieve clip from map
	const crpmParameterizedMotionData* GetParameterizedMotion(PmKey key) const;
	DEPRECATED const crpmParameterizedMotionData* FindParameterizedMotion(PmKey key) const { return GetParameterizedMotion(key); }

	// PURPOSE: Retrieve a parameterized motion using a hash key (non-const)
	// PARAMS: key - hash key
	// RETURNS: Non-const pointer to parameterized motion, if found, or NULL if not
	// NOTES: Very fast, using key to retrieve clip from map
	crpmParameterizedMotionData* GetParameterizedMotion(PmKey key);
	DEPRECATED crpmParameterizedMotionData* FindParameterizedMotion(PmKey key) { return GetParameterizedMotion(key); }

	// PURPOSE: Convert parameterized motion filename into hash key
	// PARAMS: filename - clip filename
	// RETURNS: Hash key
	PmKey ConvertNameToKey(const char* filename) const;

	// PURPOSE: Get number of parameterized motions in dictionary
	// RETURNS: Total number of parameterized motions stored in dictionary
	u32 GetNumParameterizedMotions() const;

	// PURPOSE: Get parameterized motion using index
	// PARAMS: idx - parameterized motion index [0..numParameterizedMotions-1]
	// RETURNS: Pointer to clip
	// NOTES: Very slow, iterates through parameterized motions to find index
	// Use ForAll instead if enumerating all the parameterized motions for better performance
	const crpmParameterizedMotionData* FindParameterizedMotionByIndex(u32 idx) const;

	// PURPOSE: Get parameterized motion using index (non-const)
	// PARAMS: idx - parameterized motion index [0..numParameterizedMotions-1]
	// RETURNS: Non-const pointer to clip
	// NOTES: Very slow, iterates through parameterized motions to find index
	// Use ForAll instead if enumerating all the parameterized motions for better performance
	crpmParameterizedMotionData* FindParameterizedMotionByIndex(u32 idx);

	// PURPOSE: Get parameterized motion hash key using index
	// PARAMS: idx - parameterized motion index [0..numParameterizedMotions-1]
	// RETURNS: Hash key
	// NOTES: Very slow, iterates through parameterized motions to find index
	// Use ForAll instead if enumerating all the parameterized motions for better performance
	PmKey FindKeyByIndex(u32 idx) const;


	// PURPOSE: Const callback typedef
	typedef bool (*ConstPmDictionaryCallback)(const crpmParameterizedMotionData* pmData, PmKey key, void* data);

	// PURPOSE:	Execute a specified callback on every item in this dictionary
	// PARAMS:
	// cb - const callback function
	// data - user-specified callback data
	// RETURNS:	true - all callbacks succeeded (or dictionary was empty), false - callback failed
	bool ForAll(ConstPmDictionaryCallback cb, void* data) const;


	// PURPOSE: Non-const callback typedef
	typedef bool (*PmDictionaryCallback)(crpmParameterizedMotionData* pmData, PmKey key, void* data);

	// PURPOSE:	Execute a specified callback on every item in this dictionary
	// PARAMS:
	// cb - non-const callback function
	// data - user-specified callback data
	// RETURNS:	true - all callbacks succeeded (or dictionary was empty), false - callback failed
	bool ForAll(PmDictionaryCallback cb, void* data);


	// PURPOSE: Get direct access to internal clip dictionary
	// RETURNS: pointer to clip dictionary
	const crClipDictionary* GetClipDictionary() const;

	// PURPOSE: Get direct access to internal clip dictionary (non-const)
	// RETURNS: Non-const pointer to clip dictionary
	crClipDictionary* GetClipDictionary();

protected:

	// PURPOSE: Internal function for hashing filename to key
	// PARAMS: name - name to hash
	// RETURNS: Hash key
	// NOTES: Uses atStringHash by default, override to modify behavior
	virtual PmKey CalcKey(const char* name) const;


	datRef<crClipDictionary> m_ClipDictionary;
	bool m_ClipDictionaryOwner;

	bool m_BaseNameKeys;

	datPadding<2> m_Padding;

	atMap<PmKey, datOwner<crpmParameterizedMotionData> >  m_ParameterizedMotions;
};

////////////////////////////////////////////////////////////////////////////////

/*
class crpmParameterizedMotionDictionaryIdMap
{
	crpmParameterizedMotionDictionaryIdMap();

	crpmParameterizedMotionDictionaryIdMap(crParameterizedMotionDictionary&, crParameterizedMotionDictionaryMap*);

	virtual ~crpmParameterizedMotionDictionaryIdMap();

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmParameterizedMotionDictionaryIdMap);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	void Init(const crParameterizedMotionDictionary&, crParameterizedMotionDictionaryMap*);

	const crParameterizedMotionDictionary& GetDictionary() const;

	const crParameterizedMotionDictionaryMap* GetParent() const;

	typedef u32 ParameterizedMotionGameKey;

	bool AddEntry(ParameterizedMotionGameKey gameKey, const char* ParameterizedMotionFilename);

	bool AddEntry(ParameterizedMotionGameKey gameKey, crParameterizedMotionDictionary::ParameterizedMotionKey key);

	const crParameterizedMotion* FindEntry(ParameterizedMotionGameKey gameKey, bool searchParents=true) const;

	u32 GetNumEntries() const;

	void GetEntry(u32 idx, ParameterizedMotionGameKey& gameKey, crParameterizedMotionDictionary::ParameterizedMotionKey& key) const;
};
*/
////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base parameterized motion loader class
class crpmParameterizedMotionLoader
{
public:

	// PURPOSE: Constructor
	crpmParameterizedMotionLoader();

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotionLoader();

	// PURPOSE: Allocate and load parameterized motion - override this to implement in derived class
	virtual crpmParameterizedMotionData* AllocateAndLoadParameterizedMotion(const char* pmFilename, crClipLoader* clipLoader, crAnimLoader* animLoader) = 0;
};

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
// PURPOSE: Basic parameterized motion loader class
class crpmParameterizedMotionLoaderBasic : public crpmParameterizedMotionLoader
{
public:

	// PURPOSE: Constructor
	crpmParameterizedMotionLoaderBasic();

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotionLoaderBasic();

	// PURPOSE: Override allocate and load parameterized motion function
	virtual crpmParameterizedMotionData* AllocateAndLoadParameterizedMotion(const char* pmFilename, crClipLoader* clipLoader, crAnimLoader* animLoader);
};
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Dictionary parameterized motion loader - retrieves from dictionary
class crpmParameterizedMotionLoaderDictionary : public crpmParameterizedMotionLoader
{
public:

	// PURPOSE: Constructor
	crpmParameterizedMotionLoaderDictionary();

	// PURPOSE: Initializing constructor
	// PARAMS: dictionary - pointer to dictionary
	crpmParameterizedMotionLoaderDictionary(const crpmParameterizedMotionDictionary* dictionary);

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotionLoaderDictionary();

	// PURPOSE: Initializer
	// PARAMS: dictionary - pointer to dictionary
	void Init(const crpmParameterizedMotionDictionary* dictionary);

	// PURPOSE: Override allocate and load parameterized motion function
	virtual crpmParameterizedMotionData* AllocateAndLoadParameterizedMotion(const char* pmFilename, crClipLoader* clipLoader, crAnimLoader* animLoader);

protected:
	const crpmParameterizedMotionDictionary* m_Dictionary;
};

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterizedMotionData* crpmParameterizedMotionDictionary::GetParameterizedMotion(const char* pmName) const
{
	return GetParameterizedMotion(ConvertNameToKey(pmName));
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotionData* crpmParameterizedMotionDictionary::GetParameterizedMotion(const char* pmName)
{
	return GetParameterizedMotion(ConvertNameToKey(pmName));
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterizedMotionData* crpmParameterizedMotionDictionary::GetParameterizedMotion(PmKey key) const
{
	const datOwner<crpmParameterizedMotionData>* result = m_ParameterizedMotions.Access(key);
	return result?(*result):NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline  crpmParameterizedMotionData* crpmParameterizedMotionDictionary::GetParameterizedMotion(PmKey key)
{
	datOwner<crpmParameterizedMotionData>* result = m_ParameterizedMotions.Access(key);
	return result?(*result):NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotionDictionary::PmKey crpmParameterizedMotionDictionary::ConvertNameToKey(const char* pmName) const
{
	if(m_BaseNameKeys)
	{
		char baseName[RAGE_MAX_PATH];
		ASSET.BaseName(baseName, RAGE_MAX_PATH, ASSET.FileName(pmName));

		return CalcKey(baseName);
	}

	return CalcKey(pmName);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crpmParameterizedMotionDictionary::GetNumParameterizedMotions() const
{
	return u32(m_ParameterizedMotions.GetNumUsed());
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterizedMotionData* crpmParameterizedMotionDictionary::FindParameterizedMotionByIndex(u32 idx) const
{
	atMap<PmKey, datOwner<crpmParameterizedMotionData> >::ConstIterator it = m_ParameterizedMotions.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
    return it.GetData();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotionData* crpmParameterizedMotionDictionary::FindParameterizedMotionByIndex(u32 idx)
{
	atMap<PmKey, datOwner<crpmParameterizedMotionData> >::Iterator it = m_ParameterizedMotions.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
	return it.GetData();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotionDictionary::PmKey crpmParameterizedMotionDictionary::FindKeyByIndex(u32 idx) const
{
	atMap<PmKey, datOwner<crpmParameterizedMotionData> >::ConstIterator it = m_ParameterizedMotions.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
	return it.GetKey();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmParameterizedMotionDictionary::ForAll(ConstPmDictionaryCallback cb, void* data) const
{
	bool result = true;

	atMap<PmKey, datOwner<crpmParameterizedMotionData> >::ConstIterator it = m_ParameterizedMotions.CreateIterator();
	while(it)
	{
		if((result = cb(it.GetData(), it.GetKey(), data)) == false)
		{
			break;
		}
		it.Next();
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmParameterizedMotionDictionary::ForAll(PmDictionaryCallback cb, void* data)
{
	bool result = true;

	atMap<PmKey, datOwner<crpmParameterizedMotionData> >::Iterator it = m_ParameterizedMotions.CreateIterator();
	while(it)
	{
		if((result = cb(it.GetData(), it.GetKey(), data)) == false)
		{
			break;
		}
		it.Next();
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////

inline const crClipDictionary* crpmParameterizedMotionDictionary::GetClipDictionary() const
{
	return m_ClipDictionary;
}

////////////////////////////////////////////////////////////////////////////////

inline crClipDictionary* crpmParameterizedMotionDictionary::GetClipDictionary()
{
	return m_ClipDictionary;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotionDictionary::PmKey crpmParameterizedMotionDictionary::CalcKey(const char* name) const
{
	return PmKey(atStringHash(name));
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONDICTIONARY_H
