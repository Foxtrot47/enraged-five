//
// crparameterizedmotion/parameterizedmotionsimple.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONSIMPLE_H
#define CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONSIMPLE_H

#include "blenddatabase.h"
#include "parameter.h"
#include "parameterinterpolator.h"
#include "parameterizedmotion.h"
#include "registrationcurveplayer.h"
#include "weights.h"

#include "cranimation/animation_config.h"

namespace rage
{

class crAnimLoader;
class crClips;
class crClipLoader;
class crpmBlendDatabase;
class crpmRegistrationCurveData;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Simple parameterized motion data
class crpmParameterizedMotionSimpleData : public crpmParameterizedMotionData
{
protected:

	// PURPOSE: Derived constructor
	crpmParameterizedMotionSimpleData(eParameterizedMotionDataType pmDataType);

public:

	// PURPOSE: Default constructor
	crpmParameterizedMotionSimpleData();

	// PURPOSE: Resource constructor
	crpmParameterizedMotionSimpleData(datResource&);

	// PURPOSE: Initializing constructor
	// NOTES: Parameterized motion will take ownership of clips, registration curve and blend database
	// It will destroy these objects on shutdown/destruction of the parameterized motion data
	crpmParameterizedMotionSimpleData(const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags=NULL, const crProperties* properties=NULL);

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotionSimpleData();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmParameterizedMotionSimpleData);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initializer, used to create simple data
	// NOTES: Parameterized motion will take ownership of clips, registration curve and blend database
	// It will destroy these objects on shutdown/destruction of the parameterized motion data
	void Init(const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags=NULL, const crProperties* properties=NULL);

	// PURPOSE: Shutdown
	void Shutdown();

#if CR_DEV
	// PURPOSE: Load from file
	// PARAMS:
	// filename - .spm filename
	// clipLoader - clip loader
	// animLoader - animation loader
	// RETURNS: true - load successful, false - load failed
	virtual bool Load(const char* filename, crClipLoader* clipLoader=NULL, crAnimLoader* animLoader=NULL);

	// PURPOSE: Allocate and load from file
	// PARAMS:
	// filename - .spm filename
	// clipLoader - clip loader
	// animLoader - animation loader
	// RETURNS: If successful returns pointer to newly allocated and loaded simple data, otherwise NULL.
	static crpmParameterizedMotionSimpleData* AllocateAndLoad(const char* filename, crClipLoader* clipLoader=NULL, crAnimLoader* animLoader=NULL);

	// PURPOSE: Save to file
	// PARAMS: filename - .spm filename
	// RETURNS: true - save successful, false - save failed
	virtual bool Save(const char* filename) const;

	// PURPOSE: Serialize
	virtual void Serialize(datSerialize&);

	// PURPOSE: Dump debug output
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Get clips
	const crClips* GetClips() const;

	// PURPOSE: Get registration curve
	// NOTES: Can be NULL on legitimately initialized data
	// Indicates a pose based parameterized motion, where no time elapses
	const crpmRegistrationCurveData* GetRegistrationCurveData() const;

	// PURPOSE: Get blend database
	const crpmBlendDatabase* GetBlendDatabase() const;


	// PURPOSE: Get tags
	const crTags* GetTags() const;

	// PURPOSE: Get tags (non-const)
	crTags* GetTags();

	// PURPOSE: Get properties
	const crProperties* GetProperties() const;

	// PURPOSE: Get properties (non-const)
	crProperties* GetProperties();

protected:
	datOwner<crClips> m_Clips;
	datOwner<crpmRegistrationCurveData> m_RcData;
	datOwner<crpmBlendDatabase> m_BlendDatabase;

	datOwner<crTags> m_Tags;
	datOwner<crProperties> m_Properties;

	static const u8 sm_FileVersions[];
};

////////////////////////////////////////////////////////////////////////////////

extern __THREAD int s_PmSimpleSerializeVersion;
extern __THREAD crClipLoader* s_PmSimpleClipLoader;
extern __THREAD crAnimLoader* s_PmSimpleAnimLoader;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Simple parameterized motion instance
class crpmParameterizedMotionSimple : public crpmParameterizedMotion
{
protected:

	// PURPOSE: Derived constructor
	crpmParameterizedMotionSimple(eParameterizedMotionType pmType);

public:
	
	// PURPOSE: Default constructor
	crpmParameterizedMotionSimple();
	
	// PURPOSE: Pre-initializing constructor
	crpmParameterizedMotionSimple(int worstNumClips, int worstNumParameterDimensions);

	// PURPOSE: Initializing constructor
	crpmParameterizedMotionSimple(const crpmParameterizedMotionSimpleData& pmSimpleData);

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotionSimple();

	// PURPOSE: Pre-initialize
	// NOTES: Reserves worst case storage needed for later initialization
	void PreInit(int worstNumClips, int worstNumParameterDimensions);

	// PURPOSE: Initializer
	void Init(const crpmParameterizedMotionSimpleData& pmSimpleData);

	// PURPOSE: Clear
	// NOTES: Detaches instance from data, but doesn't shutdown dynamic storage
	void Clear();

	// PURPOSE: Shutdown
	virtual void Shutdown();


	// PURPOSE: Update override
	virtual void Update(float rcDeltaTime, float parameterDeltaTime);

	// PURPOSE: Composite override
	virtual void Composite(crFrameCompositor& inoutComposite);

#if !USE_PM_COMPOSITOR
	// PURPOSE: Composite override
	virtual void Composite(crFrame& outFrame);
#endif // !USE_PM_COMPOSITOR

	// TODO - RESET?


	// PURPOSE: Get current parameter override
	virtual const crpmParameter& GetParameter() const;

	// PURPOSE: Set new parameter override
	virtual void SetParameter(const crpmParameter& parameter, bool force=false);

	// PURPOSE: Set parameter value override
	virtual void SetParameterValue(int dim, float value, bool force=false);

	// PURPOSE: Get parameterization override
	virtual const crpmParameterization& GetParameterization() const;

	// PURPOSE: Get parameter limits override
	virtual void GetParameterLimits(crpmParameter& outMinLimit, crpmParameter& outMaxLimit) const;

	// PURPOSE: Set parameter limits override
	virtual void SetParameterLimits(const crpmParameter& minLimit, const crpmParameter& maxLimit);

	// PURPOSE: Get parameter boundaries override
	virtual void GetParameterBoundaries(crpmParameter& outMinBoundary, crpmParameter& outMaxBoundary) const;

	// PURPOSE: Get parameter rates of change override
	virtual void GetParameterRates(crpmParameter& outRates) const;

	// PURPOSE: Set parameter rates of change override
	virtual void SetParameterRates(const crpmParameter& rates);


	// PURPOSE: Get phase override
	virtual float GetU() const;

	// PURPOSE: Set phase override
	virtual void SetU(float u);

	// PURPOSE: Get delta override
	virtual float GetDeltaU() const;

	// PURPOSE: Set delta override
	virtual void SetDeltaU(float u);

	// PURPOSE: Get post delta override
	virtual float GetPostDeltaU() const;

	// PURPOSE: Set post delta override
	virtual void SetPostDeltaU(float u);


	// PURPOSE: Is looped override
	virtual bool IsLooped() const;

	// PURPOSE: Set looped override
	virtual void SetLooped(bool looped);

	// PURPOSE: Is finished override
	virtual bool IsFinished() const;

	// PURPOSE: Remaining time override
	virtual float GetRemainingTime() const;

	// PURPOSE: Current duration override
	virtual float GetDuration() const;

	// PURPOSE: Maximum duration override
	virtual float GetMaxDuration() const;



	// PURPOSE: Get tags manager override
	virtual const crTags* GetTags() const;

	// PURPOSE: Get property manager override
	virtual const crProperties* GetProperties() const;


	// PURPOSE: Get simple parameterized motion type data
	const crpmParameterizedMotionSimpleData* GetSimpleData() const;
	

protected:

	// PURPOSE: Internal function, calculate weights from current parameters
	virtual void CalcWeights(crpmWeights& outWeights) const;


protected:

	crpmRegistrationCurvePlayer m_Player;

	crpmWeights m_Weights;

	crpmParameter m_ParameterMin;
	crpmParameter m_ParameterMax;

	crpmParameterInterpolator m_Interpolator;
};

////////////////////////////////////////////////////////////////////////////////

inline const crClips* crpmParameterizedMotionSimpleData::GetClips() const
{
	return m_Clips;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmRegistrationCurveData* crpmParameterizedMotionSimpleData::GetRegistrationCurveData() const
{
	return m_RcData;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmBlendDatabase* crpmParameterizedMotionSimpleData::GetBlendDatabase() const
{
	return m_BlendDatabase;
}

////////////////////////////////////////////////////////////////////////////////

inline const crTags* crpmParameterizedMotionSimpleData::GetTags() const
{
	return m_Tags;
}

////////////////////////////////////////////////////////////////////////////////

inline crTags* crpmParameterizedMotionSimpleData::GetTags()
{
	return m_Tags;
}

////////////////////////////////////////////////////////////////////////////////

inline const crProperties* crpmParameterizedMotionSimpleData::GetProperties() const
{
	return m_Properties;
}

////////////////////////////////////////////////////////////////////////////////

inline crProperties* crpmParameterizedMotionSimpleData::GetProperties()
{
	return m_Properties;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterizedMotionSimpleData* crpmParameterizedMotionSimple::GetSimpleData() const
{
	return static_cast<const crpmParameterizedMotionSimpleData*>((const crpmParameterizedMotionData*)m_Data);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONSIMPLE_H

