//
// crparameterizedmotion/parameter.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "parameter.h"

#include "parameterization.h"

#include "atl/array_struct.h"
#include "atl/string.h"
#include "bank/bank.h"
#include "bank/slider.h"
#include "data/safestruct.h"
#include "math/amath.h"
#include "math/simplemath.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameter::crpmParameter()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameter::crpmParameter(datResource& rsc)
: m_Values(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

//crpmParameter::crpmParameter(int worstNumDimensions)
//{
//	PreInit(worstNumDimensions);
//}

////////////////////////////////////////////////////////////////////////////////

crpmParameter::crpmParameter(int numDimensions, float value)
{
	Init(numDimensions, value);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameter::crpmParameter(const crpmParameter& parameter)
{
	Init(parameter);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameter::~crpmParameter()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmParameter);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmParameter::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmParameter)
	SSTRUCT_FIELD(crpmParameter, m_Values)
	SSTRUCT_END(crpmParameter)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::PreInit(int worstNumDimensions)
{
	m_Values.Reserve(worstNumDimensions);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::Init(int numDimensions, float value)
{
	m_Values.Resize(numDimensions);
	for(int i=0; i<numDimensions; ++i)
	{
		m_Values[i] = value;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::Init(const crpmParameter& parameter)
{
	m_Values = parameter.m_Values;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::Clear()
{
	m_Values.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::Shutdown()
{
	m_Values.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::Grow(float value)
{
	m_Values.PushAndGrow(value);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::Lerp(float t, const crpmParameter& parameter0, const crpmParameter& parameter1, const crpmParameterization* parameterization)
{
	Assert(m_Values.size() == parameter0.m_Values.size());
	Assert(m_Values.size() == parameter1.m_Values.size());

	const int numValues = (int)m_Values.size();
	if(parameterization)
	{
		for(int i=0; i<numValues; ++i)
		{
			m_Values[i] = parameterization->GetDimensionInfo(i).Lerp(t, parameter0.m_Values[i], parameter1.m_Values[i]);
		}
	}
	else
	{
		for(int i=0; i<numValues; ++i)
		{
			m_Values[i] = rage::Lerp(t, parameter0.m_Values[i], parameter1.m_Values[i]);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::Approach(const crpmParameter& source, const crpmParameter& target, const crpmParameter& change, float changeScalar, const crpmParameterization* parameterization)
{
	Assert(m_Values.size() == source.m_Values.size());
	Assert(m_Values.size() == target.m_Values.size());
	Assert(m_Values.size() == change.m_Values.size());
	Assert(changeScalar >= 0.f);

	const int numValues = (int)m_Values.size();
	if(parameterization)
	{
		for(int i=0; i<numValues; ++i)
		{
			if(rage::IsClose(change.m_Values[i], FLT_MAX))
			{
				m_Values[i] = target.m_Values[i];
			}
			else
			{
				m_Values[i] = parameterization->GetDimensionInfo(i).Approach(source.m_Values[i], target.m_Values[i], change.m_Values[i]*changeScalar);
			}
		}
	}
	else
	{
		for(int i=0; i<numValues; ++i)
		{
			if(rage::IsClose(change.m_Values[i], FLT_MAX))
			{
				m_Values[i] = target.m_Values[i];
			}
			else
			{
				Assert(change.m_Values[i] >= 0.f);
				m_Values[i] = source.m_Values[i] + rage::Clamp(target.m_Values[i]-source.m_Values[i], -change.m_Values[i]*changeScalar, change.m_Values[i]*changeScalar);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::Clamp(const crpmParameter& minParameter, const crpmParameter& maxParameter)
{
	Assert(m_Values.size() == minParameter.m_Values.size());
	Assert(m_Values.size() == maxParameter.m_Values.size());

	const int numValues = (int)m_Values.size();
	for(int i=0; i<numValues; ++i)
	{
		m_Values[i] = rage::Clamp(m_Values[i], minParameter.m_Values[i], maxParameter.m_Values[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameter::IsClose(const crpmParameter& parameter, const crpmParameter* tolerance, const crpmParameterization* parameterization) const
{
	Assert(m_Values.size() == parameter.m_Values.size());
	Assert(!tolerance || (m_Values.size() == tolerance->m_Values.size()));

	const int numValues = (int)m_Values.size();
	if(parameterization)
	{
		for(int i=0; i<numValues; ++i)
		{
			if(!parameterization->GetDimensionInfo(i).IsClose(m_Values[i], parameter.m_Values[i], tolerance?tolerance->m_Values[i]:SMALL_FLOAT))
			{
				return false;
			}
		}
	}
	else
	{
		for(int i=0; i<numValues; ++i)
		{
			if(!rage::IsClose(m_Values[i], parameter.m_Values[i], tolerance?tolerance->m_Values[i]:SMALL_FLOAT))
			{
				return false;
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameter::Dump(atString& text) const
{
	for(int i=0; i<GetNumDimensions(); ++i)
	{
		char buf[64];
		formatf(buf, "%f ", m_Values[i]);

		text += buf;
	}
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void crpmParameter::AddWidgets(bkBank& bk, const crpmParameter* minParameter, const crpmParameter* maxParameter)
{
	Assert(!minParameter || GetNumDimensions() == minParameter->GetNumDimensions());
	Assert(!maxParameter || GetNumDimensions() == maxParameter->GetNumDimensions());

	bk.PushGroup("Parameterized Motion");
	for(int i=0; i<GetNumDimensions(); ++i)
	{
		float minValue = bkSlider::FLOAT_MIN_VALUE;
		float maxValue = bkSlider::FLOAT_MAX_VALUE;
		float units = 0.001f;

		if(minParameter && maxParameter)
		{
			minValue = minParameter->GetValue(i);
			maxValue = maxParameter->GetValue(i);

			float range = maxValue-minValue;
			if(range > 0.f)
			{
				units = rage::Clamp(pow(10.f, floorf(log10f(range / 1000.f))), 0.f, 1.f);
			}
		}

		char buf[64];
		formatf(buf, "Parameter[%d]", i);
		
		bk.AddSlider(buf, &m_Values[i], minValue, maxValue, units);
	}
	bk.PopGroup();
	
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
