//
// crparameterizedmotion/image.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_IMAGE_H
#define CRPARAMETERIZEDMOTION_IMAGE_H

#include "grcore/image.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Thin wrapper for rage grcImage class
// Images are used in motion families to assist with debugging.
class crpmImage
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS: x, y - image width and height in pixels
    crpmImage(u32 x, u32 y);

	// PURPOSE: Destructor
	virtual ~crpmImage();

	// PURPOSE: Clear the image
	// PARAMS: rgba - color to clear to (default: black)
    void Clear(const Color32& rgba = Color32(0));

	// PURPOSE: Set a pixel in the image
	// PARAMS: x, y - pixel coordinates
	// rgba - new pixel color
    virtual void SetPixel(u32 x, u32 y, const Color32& rgba);

	// PURPOSE: Get a pixel in the image
	// PARAMS: x, y - pixel coordinates
	// RETURNS: pixel color
    virtual Color32 GetPixel(u32 x, u32 y) const;

	// PURPOSE: Get the image width
	// RETURNS: Image width, in pixels
    u32 GetWidth() const;

	// PURPOSE: Get the image height
	// RETURNS: Image height, in pixels
    u32 GetHeight() const;

	// PURPOSE: Save the image as a DDS file
	// PARAMS: filename - image filename
    void SaveDDS(const char* filename) const;


protected:
    grcImage* m_Image;
};


////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Wrapper for an image flipped in the y axis, derived off of crpmImage
class crpmImageFlippedY : public crpmImage
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS: x, y - image width and height in pixels
	crpmImageFlippedY(int x, int y);

	// PURPOSE: Set a pixel in the image
	// PARAMS: x, y - pixel coordinates
	// rgba - new pixel color
    virtual void SetPixel(u32 x, u32 y, const Color32& rgba);

	// PURPOSE: Get a pixel in the image
	// PARAMS: x, y - pixel coordinates
	// RETURNS: pixel color
	virtual Color32 GetPixel(u32 x, u32 y) const;

};

////////////////////////////////////////////////////////////////////////////////

inline crpmImage::crpmImage(u32 x, u32 y)
{
	m_Image = grcImage::Create(x, y, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);
}

////////////////////////////////////////////////////////////////////////////////

inline crpmImage::~crpmImage()
{
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmImage::Clear(const Color32& rgba)
{
	m_Image->Clear(rgba.GetColor());
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmImage::SetPixel(u32 x, u32 y, const Color32& rgba)
{
	m_Image->SetPixel(x, y, rgba.GetColor());
}

////////////////////////////////////////////////////////////////////////////////

inline Color32 crpmImage::GetPixel(u32 x, u32 y) const
{
	return Color32(m_Image->GetPixel(x, y));
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crpmImage::GetWidth() const
{
	return m_Image->GetWidth();
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crpmImage::GetHeight() const
{
	return m_Image->GetHeight();
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmImage::SaveDDS(const char* filename) const
{
	m_Image->SaveDDS(filename);
}

////////////////////////////////////////////////////////////////////////////////

inline crpmImageFlippedY::crpmImageFlippedY(int x, int y)
: crpmImage(x, y)
{
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmImageFlippedY::SetPixel(u32 x, u32 y, const Color32& rgba)
{
	if(x < GetWidth() && y < GetHeight())
	{
		crpmImage::SetPixel(x, (GetHeight()-1) - y, rgba);
	}
}

////////////////////////////////////////////////////////////////////////////////

inline Color32 crpmImageFlippedY::GetPixel(u32 x, u32 y) const
{
	if(x < GetWidth() && y < GetHeight())
	{
		return crpmImage::GetPixel(x, (GetHeight()-1) - y);
	}

	return Color32(0);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
#endif // CRPARAMETERIZEDMOTION_IMAGE_H

