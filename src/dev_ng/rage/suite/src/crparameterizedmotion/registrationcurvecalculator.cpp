//
// crparameterizedmotion/registrationcurvecalculator.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "registrationcurvecalculator.h"

#include "dynamicprogramming.h"
#include "image.h"
#include "timealignment.h"

#include "crclip/clip.h"
#include "crclip/clips.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurveCalculator::crpmRegistrationCurveCalculator()
: m_SkeletonData(NULL)
, m_WeightSet(NULL)
, m_FilterWeightSet(NULL)
, m_FilterMover(false)
, m_SampleRate(1.f/30.f)
, m_WindowSize(0)
, m_SlopeLimit(2)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurveCalculator::crpmRegistrationCurveCalculator(const crSkeletonData& skelData, const crWeightSet* weightSet, const crWeightSet* filterWeightSet, bool filterMover, float sampleRate, int slopeLimit, int windowSize, const char* debugPath)
: m_SkeletonData(NULL)
, m_WeightSet(NULL)
, m_FilterWeightSet(NULL)
, m_FilterMover(false)
, m_SampleRate(1.f/30.f)
, m_WindowSize(0)
, m_SlopeLimit(2)
{
	Init(skelData, weightSet, filterWeightSet, filterMover, sampleRate, slopeLimit, windowSize, debugPath);
}

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurveCalculator::~crpmRegistrationCurveCalculator()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurveCalculator::Init(const crSkeletonData& skelData, const crWeightSet* weightSet, const crWeightSet* filterWeightSet, bool filterMover, float sampleRate, int slopeLimit, int windowSize, const char* debugPath)
{
	m_SkeletonData = &skelData;
	m_WeightSet = weightSet;
	m_FilterWeightSet = filterWeightSet;
	m_FilterMover = filterMover;
	m_SampleRate = sampleRate;
	m_SlopeLimit = slopeLimit;
	m_WindowSize = windowSize;
	m_DebugPath = debugPath;
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurveCalculator::Shutdown()
{
	m_SkeletonData = NULL;
	m_WeightSet = NULL;
	m_FilterWeightSet = NULL;

	for(atArray<crpmTimeAlignment*>::const_iterator it=m_TimeAlignments.begin(); it!=m_TimeAlignments.end(); ++it)
	{
		delete *it;
	}
	m_TimeAlignments.Reset();

	m_TimeAlignmentMap.Reset();

	m_DebugPath.Reset();
}

////////////////////////////////////////////////////////////////////////////////

bool crpmRegistrationCurveCalculator::CalculateByTimeAlignments(crpmRegistrationCurveData& outRc, const crClips& clips, bool looped, int refClipIdx)
{
	Assert(m_SkeletonData);

	if(InitRegistrationCurve(outRc, clips, looped))
	{
		if(CreateTimeAlignments(clips))
		{
			refClipIdx = ChooseReferenceClip(clips, refClipIdx);
			if(refClipIdx >= 0)
			{
				outRc.m_ReferenceClipIndex = u16(refClipIdx);
				
				atArray<atArray<float> > timeControlPoints;
				atArray<atArray<Vec3V> > alignmentControlPoints;
				if(CalcTimeAndSpatialAlignmentControlPoints(timeControlPoints, alignmentControlPoints, -1, clips, refClipIdx))
				{
					CreateTimeAndSpatialAlignmentCurves(outRc, timeControlPoints, alignmentControlPoints);

					return true;
				}
			}
		}
	}

	Errorf("crpmRegistrationCurveCalculator::CalculateByTimeAlignments - failed to calculate registration curve data");
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmRegistrationCurveCalculator::InitRegistrationCurve(crpmRegistrationCurveData& outRc, const crClips& clips, bool looped)
{
	outRc.m_IsLooped = looped;
	outRc.m_NumClips = u16(clips.GetNumClips());

	if(clips.GetNumClips() < 2)
	{
		Errorf("crpmRegistrationCurveCalculator::InitRegistrationCurve - must have at least 2 clips to form registration curve");
		return false;
	}

	int numLoops = 1;

	const int numClips = clips.GetNumClips();
	for(int i=0; i<numClips; ++i)
	{
		numLoops = LeastCommonMultiple(numLoops, GetCycles(*clips.GetClip(i)));
	}

	outRc.m_MaxNumLoops = u16(numLoops);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmRegistrationCurveCalculator::CreateTimeAlignments(const crClips& clips)
{
	const int numClips = clips.GetNumClips();
	for(int i=0; i<numClips; ++i)
	{
		const crClip* clip0 = clips.GetClip(i);
		if(clip0)
		{
			for(int j=i+1; j<numClips; ++j)
			{
				const crClip* clip1 = clips.GetClip(j);
				if(clip1)
				{
					if(clip0 != clip1)
					{
						// add a time alignment for both clip combinations
						if (!AddClips(*clip0, *clip1) || !AddClips(*clip1, *clip0))
						{
							Errorf("crpmRegistrationCurveCalculator::CalcTimeAlignments - failed to generate data for clip '%s' vs '%s'", (const char*)clip0->GetName(), (const char*)clip1->GetName());
							return false;
						}
					}
				}
				else
				{
					Errorf("crpmRegistrationCurveCalculator::CalcTimeAlignments - clip %d is missing", j);
					return false;
				}
			}
		}
		else
		{
			Errorf("crpmRegistrationCurveCalculator::CalcTimeAlignments - clip %d is missing", i);
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmRegistrationCurveCalculator::AddClips(const crClip& clip0, const crClip& clip1)
{
	crpmDistanceGrid grid;
	grid.Calculate(clip0, clip1, *m_SkeletonData, m_WindowSize, m_SampleRate, FLT_MAX, m_WeightSet, m_FilterWeightSet, m_FilterMover);

	if(m_DebugPath.GetLength())
	{
		GenerateDistanceGridImage(grid);
	}

//	m_DistanceGrids.push_back(grid);

	crpmDistanceGrid::Path bestPath;
	if(!CalcBestPath(bestPath, grid))
	{
		Errorf("crpmRegistrationCurveCalculator::AddClips - failed to calculate a best path");
		return false;
	}

	crpmTimeAlignment* timeAlignment = rage_new crpmTimeAlignment(bestPath, grid);
	m_TimeAlignments.PushAndGrow(timeAlignment);

	m_TimeAlignmentMap[grid.GetClipY()].PushAndGrow(timeAlignment);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmRegistrationCurveCalculator::CalcBestPath(crpmDistanceGrid::Path& outPath, const crpmDistanceGrid& grid) const
{
	crpmDynamicProgramming dp;
	dp.Init(grid, m_SlopeLimit);

	dp.FindBestBridge();
	outPath = dp.GetBestPath();

	if(m_DebugPath.GetLength())
	{
		GenerateDynamicProgrammingImage(dp, grid);
		GeneratePathImage(outPath, grid);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

int crpmRegistrationCurveCalculator::ChooseReferenceClip(const crClips& clips, int refClip)
{
	const int numClips = clips.GetNumClips();
	if(!InRange(refClip, 0, numClips-1))
	{
		// choose the reference motion based on the sum of averages for all its TAs
		float minSum = FLT_MAX;
		for(int i=0; i<numClips; ++i)	
		{
			atArray<const crpmTimeAlignment*>& timeAlignments = m_TimeAlignmentMap[clips.GetClip(i)];

			// only motions with a single cycle can be reference motions
			if((*timeAlignments.begin())->GetCyclesY() == 1)
			{
				float sum = 0.f;
				for(atArray<const crpmTimeAlignment*>::const_iterator it=timeAlignments.begin(); it!=timeAlignments.end(); ++it)
				{
					sum += (*it)->GetDistanceAverage();
				}

				if(sum < minSum)
				{
					minSum = sum;
					refClip = i;
				}
			}
		}

		AssertMsg(InRange(refClip, 0, numClips-1), "crpmRegistrationCurveCalculator::ChooseReferenceClip - failed to find reference clip (at least one clip must have just single cycle)");
	}

	return refClip;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmRegistrationCurveCalculator::CalcTimeAndSpatialAlignmentControlPoints(atArray<atArray<float> >& outTimeControlPoints, atArray<atArray<Vec3V> >& outSpatialControlPoints, int numControlPoints, const crClips& clips, int refClipIdx)
{
	const int numClips = clips.GetNumClips();
	Assert(InRange(refClipIdx, 0, numClips-1));

	Assert(clips.GetClip(refClipIdx));
	const crClip& refClip = *clips.GetClip(refClipIdx);

	if(numControlPoints < 0)
	{
		// if not specified, generate two control points per sample rate frame (plus one for the end)
		numControlPoints = int((2.f*refClip.GetDuration()/m_SampleRate)+0.5f)+1;
	}

	outTimeControlPoints.Resize(numControlPoints);
	outSpatialControlPoints.Resize(numControlPoints);

	atArray<crpmAngleContinuityPreserver> acps(numClips, numClips);

	for(int i=0; i<numControlPoints; ++i)
	{
		bool last = i==(numControlPoints-1);
		float fract = float(i) / float(numControlPoints-1);
		float refTime = refClip.GetDuration() * fract;

		atArray<float>& timeControlPoint = outTimeControlPoints[i];
		atArray<Vec3V>& spatialControlPoint = outSpatialControlPoints[i];

		timeControlPoint.Resize(numClips);
		spatialControlPoint.Resize(numClips);

		timeControlPoint[refClipIdx] = refClip.ConvertTimeToPhase(refTime);
		spatialControlPoint[refClipIdx] = Vec3V(V_ZERO);

		// for each time alignment where reference clip is clip0/y
		atArray<const crpmTimeAlignment*>& timeAlignments = *m_TimeAlignmentMap.Access(&refClip);
//		AssertMsg(its != m_TimeAlignmentMap.end() , "missing time alignments for reference motion?");

		for(atArray<const crpmTimeAlignment*>::const_iterator it=timeAlignments.begin(); it!=timeAlignments.end(); ++it)
		{
			const crpmTimeAlignment& ta = **it;

			Assert(ta.GetCyclesY() == 1);
			Assert(ta.GetRepeatX() == 1);

			float time;
			crpmPointCloudTransform transform;

			if(last)
			{
				time = float(ta.GetRepeatY());
				transform = ta.GetAlignmentTransform(1.f);
			}
			else
			{
				float u = 0.f;
				float clipTime = ta.GetClipX()->GetDuration() * fract;
				ta.ConvertCellXToCellY(clipTime / ta.GetSampleRateX(), time, u);
				time = ta.GetClipY()->ConvertTimeToPhase(time * ta.GetSampleRateY());
				transform = ta.GetAlignmentTransform(Clamp(u, 0.f, 1.f));
			}

			u32 clipIdx = 0;
			AssertVerify(clips.FindClip(ta.GetClipX(), clipIdx));
			
			timeControlPoint[clipIdx] = time;

			Vec3V v = transform.ToVector3();
			v.SetXf(acps[clipIdx].Get(v.GetXf()));
			spatialControlPoint[clipIdx] = v;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurveCalculator::CreateTimeAndSpatialAlignmentCurves(crpmRegistrationCurveData& outRc, const atArray<atArray<float> >& timeControlPoints, const atArray<atArray<Vec3V> >& ASSERT_ONLY(spatialControlPoints))
{
	Assert(timeControlPoints.size() > 0);
	Assert(spatialControlPoints.size() > 0);
	Assert(timeControlPoints.size() == spatialControlPoints.size());
	Assert(timeControlPoints[0].size() >= 2);

	const int numControlPoints = (int)timeControlPoints.size();
	const int numClips = (int)timeControlPoints[0].size();

	outRc.m_AlignmentCurves.Resize(numClips);

	for(int i=0; i<numClips; ++i)
	{	
		// TODO --- might want to make entry NULL for ref clip?

		atArray<float> alignments;
		alignments.Reserve(numControlPoints);

		for(int j=0; j<numControlPoints; ++j)
		{
			Assert(timeControlPoints[j].GetCount() == int(numClips));
			Assert(spatialControlPoints[j].GetCount() == int(numClips));

			alignments.PushAndGrow(timeControlPoints[j][i]);
		}

		outRc.m_AlignmentCurves[i] = rage_new crpmSeries<float>(numControlPoints, &alignments[0]);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurveCalculator::GenerateDistanceGridImage(const crpmDistanceGrid& grid) const
{
	crpmImage image(grid.GetSizeX(), grid.GetSizeY());
	grid.MakeDistanceGridImage(image);

	char clipY[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(clipY, RAGE_MAX_PATH, ASSET.FileName((const char*)grid.GetClipY()->GetName()));

	char clipX[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(clipX, RAGE_MAX_PATH, ASSET.FileName((const char*)grid.GetClipX()->GetName()));

	char filename[RAGE_MAX_PATH];
	formatf(filename, "%s/grid___%s___%s.dds", (const char*)m_DebugPath, clipY, clipX);

	image.SaveDDS(filename);
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurveCalculator::GeneratePathImage(const crpmDistanceGrid::Path& path, const crpmDistanceGrid& grid) const
{
	crpmImage image(grid.GetSizeX(), grid.GetSizeY());
	grid.MakeDistanceGridImage(image);
	path.MakePathImage(image);

	char clipY[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(clipY, RAGE_MAX_PATH, ASSET.FileName((const char*)grid.GetClipY()->GetName()));

	char clipX[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(clipX, RAGE_MAX_PATH, ASSET.FileName((const char*)grid.GetClipX()->GetName()));

	char filename[RAGE_MAX_PATH];
	formatf(filename, "%s/path___%s___%s.dds", (const char*)m_DebugPath, clipY, clipX);

	image.SaveDDS(filename);
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurveCalculator::GenerateDynamicProgrammingImage(const crpmDynamicProgramming& dp, const crpmDistanceGrid& grid) const
{
	crpmImage image(dp.GetSizeX(), dp.GetSizeY());
	dp.MakeDynamicProgrammingImage(image);

	char clipY[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(clipY, RAGE_MAX_PATH, ASSET.FileName((const char*)grid.GetClipY()->GetName()));

	char clipX[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(clipX, RAGE_MAX_PATH, ASSET.FileName((const char*)grid.GetClipX()->GetName()));

	char filename[RAGE_MAX_PATH];
	formatf(filename, "%s/dp___%s___%s.dds", (const char*)m_DebugPath, clipY, clipX);

	image.SaveDDS(filename);
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurveCalculator::Dump()
{
//	for(int i=0; i<numTimeAlignments)
	// TODO --- DUMP OUTPUT FROM TIMEALIGNMENTS AND DISTANCE GRIDS?
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage



