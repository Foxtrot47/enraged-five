//
// crparameterizedmotion/crpmdiag.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_CRMFDIAG_H
#define CRPARAMETERIZEDMOTION_CRMFDIAG_H

namespace rage
{

// PURPOSE: Returns the current debug level for the parameterized motion module
int crpmDebugLevel();

#define crpmDebug1			SpewDebug( 1, ::rage::crpmDebugLevel(), "crpm" )
#define crpmDebug2			SpewDebug( 2, ::rage::crpmDebugLevel(), "crpm" )
#define crpmDebug3			SpewDebug( 3, ::rage::crpmDebugLevel(), "crpm" )

#define crpmWarning			SpewWarning( "crpm" )
#define crpmError			SpewError( "crpm" )

#define crpmAssert			FastAssert
#define crpmAssertf			Assertf
#define crpmAssertMsg		AssertMsg
#define crpmAssertVerify	AssertVerify
#define crpmDebugAssert		DebugAssert

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_CRMFDIAG_H
