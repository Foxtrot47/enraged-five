//
// crparameterizedmotion/distancegrid.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_DISTANCEGRID_H
#define CRPARAMETERIZEDMOTION_DISTANCEGRID_H

#include "pointcloudtransform.h"

#include "atl/array.h"

namespace rage
{

class Color32;
class crClip;
class crSkeletonData;
class crWeightSet;
class crpmImage;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Distance grid, two-dimensional set of cells representing distance between two clips
class crpmDistanceGrid
{
public:

	// PURPOSE: Default constructor
	crpmDistanceGrid();

	// PURPOSE: Calculate a distance grid between two clips
	// PARAMS:
	// clipY, clipX - two clips (can be the same for symmetric grid)
	// skelData - skeleton data
	// windowSize - window size to use when calculating point cloud transforms
	// sampleRate - rate at which to sample clips (default 1.0/30.0)
	// maxDistance - clamp maximum permitted distance
	// weightSet - weight set to use when calculating points within cloud transforms
	// filterWeightSet - weight set to use when generating partial updates
	// filterMover - exclude mover when generating partial updates
	void Calculate(const crClip& clipY, const crClip& clipX, const crSkeletonData& skelData, int windowSize, float sampleRate=1.f/30.f, float maxDistance=FLT_MAX, const crWeightSet* weightSet=NULL, const crWeightSet* filterWeightSet=NULL, bool filterMover=false);

	// PURPOSE: Get clip x
	const crClip* GetClipX() const;

	// PURPOSE: Get clip  y
	const crClip* GetClipY() const;

	// PURPOSE: Get clip x sample rate
	float GetSampleRateX() const;

	// PURPOSE: Get clip y sample rate
	float GetSampleRateY() const;

	// PURPOSE: Get clip x cycles
	int GetCyclesX() const;

	// PURPOSE: Get clip y cycles
	int GetCyclesY() const;

	// PURPOSE: Get clip x repeats
	int GetRepeatX() const;

	// PURPOSE: Get clip y repeats
	int GetRepeatY() const;

	// PURPOSE: Get x size of grid (in cells)
	int GetSizeX() const;

	// PURPOSE: Get y size of grid (in cells)
	int GetSizeY() const;

	// PURPOSE: Is the coordinate supplied inside the grid
	bool IsCellInGrid(int x, int y) const;

	// PURPOSE: Get distance at coordinate supplied
	float GetCellDistance(int x, int y) const;

	// PURPOSE: Is cell identified by coordinate a local minimum
	bool IsCellLocalMinimum(int x, int y) const;

	// PURPOSE: Get transform of cell at coordinate supplied
	const crpmPointCloudTransform& GetCellTransform(int x, int y) const;

	// PURPOSE: Get minimum distance of any cell in grid
	float GetMinDistance() const;

	// PURPOSE: Get maximum distance of any cell in grid
	float GetMaxDistance() const;


	// PURPOSE: Represents a path in the grid
	class Path
	{
	public:

		// PURPOSE: Default constructor
		Path();

		// PURPOSE: Add coordinate to path
		void Add(int x, int y, float distance);

		// PURPOSE: Add coordinate to path, making sure there are no redundant cells when adding
		void AddSafe(int x, int y, float distance);

		// PURPOSE: Clear path
		void Clear();

		// PURPOSE: Get length of path (number of coordinates)
		int GetLength() const;

		// PURPOSE: Get sum of all coordinate distances on path
		float GetDistanceSum() const;

		// PURPOSE: Get average of all coordinate distances on path
		float GetDistanceAverage() const;

		// PURPOSE: Is path continuous
		bool IsContinuous() const;

		// PURPOSE: Get x coordinate of start of path
		int GetStartX() const;

		// PURPOSE: Get y coordinate of start of path
		int GetStartY() const;

		// PURPOSE: Get x coordinate of end of path
		int GetEndX() const;

		// PURPOSE: Get y coordinate of end of path
		int GetEndY() const;

		// PURPOSE: Does path contain specified coordinate
		bool Contains(int x, int y) const;

		// PURPOSE: Remove a section of the path
		void RemoveRange(int a, int b);

		// PURPOSE: Internal path coordinate representation
		struct Coordinate
		{
			// PURPOSE: Default constructor
			Coordinate();

			// PURPOSE: Initializing constructor
			Coordinate(int _x, int _y, float _distance=0.f);

			// PURPOSE: Equality operator
			bool operator==(const Coordinate& other) const;

			// PURPOSE: Is specified coordinate in range
			bool IsInRange(const Coordinate& c) const;

			int x;
			int y;
			float distance;
		};

		// PURPOSE: Coordinate container type
		typedef atArray<Coordinate> CoordinateContainer;

		// PURPOSE: Coordinate container iterator
		typedef CoordinateContainer::const_iterator const_iterator;

		// PURPOSE: STL iterator interface
		const_iterator begin() const;

		// PURPOSE: STL iterator interface
		const_iterator end() const;

		// PURPOSE: STL iterator interface
		const Coordinate& operator[](int i) const;

		// PURPOSE: Dump out path (for debugging)
		void Dump() const;

		// PURPOSE: Render out image of path
		void MakePathImage(crpmImage& outImage, Color32* col=NULL) const;

	protected:

		CoordinateContainer m_Path;
	};


	// PURPOSE: Dump output (for debugging)
	void Dump() const;

	// PURPOSE: Generate a image of the distance grid (for debugging)
	void MakeDistanceGridImage(crpmImage& outImage, bool heatMap=false) const;

	// PURPOSE: Generate a image of the valid regions in grid (for debugging)
	void MakeValidRegionImage(crpmImage& outImage) const;


protected:

	// PURPOSE: Internal cell representation
	class Cell
	{
	public:

		// PURPOSE: Default constructor
		Cell();

		// PURPOSE: Initializing constructor
		Cell(float distance, const crpmPointCloudTransform& transform);

		// PURPOSE: Get point cloud transform
		const crpmPointCloudTransform& GetTransform() const;

		// PURPOSE: Set point cloud transform
		void SetTransform(const crpmPointCloudTransform&);

		// PURPOSE: Get distance
		float GetDistance() const;

		// PURPOSE: Set distance
		void SetDistance(float);

		// PURPOSE: Is cell local minimum
		bool IsLocalMinimum() const;

		// PURPOSE: Set cell as local minimum
		void SetLocalMinimum(const bool b);

		// PURPOSE: Is cell in valid region
		bool IsValidRegion() const;

		// PURPOSE: Set cell in valid region
		void SetValidRegion(const bool b);


	private:

		crpmPointCloudTransform m_Transform;
		float m_Distance;
		bool m_LocalMinimum;
		bool m_ValidRegion;
	};


	// PURPOSE: Is grid symmetrical (are the two clips identical)
	bool IsSymmetrical() const;

	// PURPOSE: Get cell (taking symmetry into account)
	const Cell& GetCell(int x, int y) const;

	// PURPOSE: Create grid of empty cells
    void CreateEmptyCells(int maxX, int maxY);

	// PURPOSE: Calculate valid region around local minimum
    void CalcValidRegion(float threshold);

	// PURPOSE: Is cell horizontal minimum
    bool IsCellHorizontalMinimum(int x, int y);

	// PURPOSE: Is cell vertical minimum
	bool IsCellVerticalMinimum(int x, int y);

	// PURPOSE: Fill valid region horizontally
    void FillValidRegionHorizontally(int x, int y, int direction, float threshold);

	// PURPOSE: Fill valid region vertically
    void FillValidRegionVertically(int x, int y, int direction, float threshold);


private:

	const crClip* m_ClipX;
    const crClip* m_ClipY;

	int m_CyclesX;
	int m_CyclesY;

	int m_RepeatX;
	int m_RepeatY;

    int m_SizeX;
    int m_SizeY;

    float m_MinDistance;
    float m_MaxDistance;

	float m_SampleRateX;
	float m_SampleRateY;

    atArray< atArray<Cell> > m_Cells;
};

////////////////////////////////////////////////////////////////////////////////

extern int GetCycles(const crClip& clip);

////////////////////////////////////////////////////////////////////////////////

extern int LeastCommonMultiple(int a, int b);

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crpmDistanceGrid::GetClipX() const
{
	return m_ClipX;
}

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crpmDistanceGrid::GetClipY() const
{
	return m_ClipY;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::GetCyclesX() const
{
	return m_CyclesX;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::GetCyclesY() const
{
	return m_CyclesY;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::GetRepeatX() const
{
	return m_RepeatX;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::GetRepeatY() const
{
	return m_RepeatY;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmDistanceGrid::GetSampleRateX() const
{
	return m_SampleRateX;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmDistanceGrid::GetSampleRateY() const
{
	return m_SampleRateY;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::GetSizeX() const
{
	return m_SizeX;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::GetSizeY() const
{
	return m_SizeY;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmDistanceGrid::IsCellInGrid(int x, int y) const
{
	return (x >= 0) && (y >= 0) && (x < m_SizeX) && (y < m_SizeY);
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmDistanceGrid::GetCellDistance(int x, int y) const
{
	return GetCell(x, y).GetDistance();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmDistanceGrid::IsCellLocalMinimum(int x, int y) const
{
	return GetCell(x, y).IsLocalMinimum();
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmPointCloudTransform& crpmDistanceGrid::GetCellTransform(int x, int y) const
{
	return GetCell(x, y).GetTransform();
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmDistanceGrid::GetMinDistance() const
{
	return m_MinDistance;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmDistanceGrid::GetMaxDistance() const
{
	return m_MaxDistance;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::Path::GetStartX() const
{
	return m_Path.begin()->x;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::Path::GetStartY() const
{
	return m_Path.begin()->y;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::Path::GetEndX() const
{
	return m_Path.rbegin()->x;
}

////////////////////////////////////////////////////////////////////////////////

inline int crpmDistanceGrid::Path::GetEndY() const
{
	return m_Path.rbegin()->y;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDistanceGrid::Path::Coordinate::Coordinate()
: x(0)
, y(0)
, distance(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDistanceGrid::Path::Coordinate::Coordinate(int _x, int _y, float _distance)
: x(_x)
, y(_y)
, distance(_distance)
{
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmDistanceGrid::Path::Coordinate::operator==(const Coordinate& other) const
{
	return (x == other.x) && (y == other.y);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmDistanceGrid::Path::Coordinate::IsInRange(const Coordinate& c) const
{
	int dx = x - c.x;
	int dy = y - c.y;
	int dist = dx + dy;
	if (dist < 1  ||  dist > 2  ||  dx > 1  ||  dy > 1)
	{
		return false;
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDistanceGrid::Path::const_iterator crpmDistanceGrid::Path::begin() const
{
	return m_Path.begin();
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDistanceGrid::Path::const_iterator crpmDistanceGrid::Path::end() const
{
	return m_Path.end();
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmDistanceGrid::Path::Coordinate& crpmDistanceGrid::Path::operator[](int i) const
{
	return m_Path[i];
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDistanceGrid::Cell::Cell()
: m_Transform(0.f, 0.f, 0.f)
, m_Distance(0.f)
, m_LocalMinimum(false)
, m_ValidRegion(false)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmDistanceGrid::Cell::Cell(float distance, const crpmPointCloudTransform& transform)
: m_Transform(0.f, 0.f, 0.f)
, m_Distance(0.f)
, m_LocalMinimum(false)
, m_ValidRegion(false)
{
	SetDistance(distance);
	SetTransform(transform);
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmPointCloudTransform& crpmDistanceGrid::Cell::GetTransform() const
{
	return m_Transform;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmDistanceGrid::Cell::SetTransform(const crpmPointCloudTransform& transform)
{
	m_Transform = transform;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmDistanceGrid::Cell::GetDistance() const
{
	return m_Distance;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmDistanceGrid::Cell::SetDistance(float distance)
{
	m_Distance = distance;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmDistanceGrid::Cell::IsLocalMinimum() const
{
	return m_LocalMinimum;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmDistanceGrid::Cell::SetLocalMinimum(const bool minimum)
{
	m_LocalMinimum = minimum;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmDistanceGrid::Cell::IsValidRegion() const
{
	return m_ValidRegion || m_LocalMinimum;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmDistanceGrid::Cell::SetValidRegion(const bool valid)
{
	m_ValidRegion = valid;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmDistanceGrid::IsSymmetrical() const
{
	return (m_ClipY == m_ClipX);
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmDistanceGrid::Cell& crpmDistanceGrid::GetCell(int x, int y) const
{
	Assert(IsCellInGrid(x, y));

	if(IsSymmetrical() && (y > x))
	{
		return m_Cells[y][x];
	}
	else
	{
		return m_Cells[x][y];
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


#endif // CRPARAMETERIZEDMOTION_DISTANCEGRID_H
