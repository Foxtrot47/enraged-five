//
// crparameterizedmotion/parameterizedmotion.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTION_H
#define CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTION_H

#include "atl/referencecounter.h"
#include "atl/string.h"
#include "crmetadata/property.h"

#include "parameter.h"
#include "cranimation/animation_config.h"
#include "paging/ref.h"

#define USE_PM_COMPOSITOR (0)  // NO crFrame::CompositeWithDeltaBlend yet
#define PM_ATOMIC_REFCOUNT (1)

namespace rage
{

class bkBank;
class crAnimLoader;
class crClipLoader;
class crDumpOutput;
class crFrame;
class crFrameCompositor;
class crProperties;
class crTags;
class crpmParameter;
class crpmParameterization;
class crpmParameterizedMotionLoader;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base class for all parameterized motion data classes
class crpmParameterizedMotionData : public pgBase
{
public:

	// PURPOSE: Enumeration of types of parameterized motion data
	// NOTES: Do not assume this is parallel with types of parameterized motion instances
	enum eParameterizedMotionDataType
	{
		kPmDataTypeNone,
		kPmDataTypeSimple,
		kPmDataTypeVariable,
//		kPmDataTypeComposite,

		// must be last in list
		kPmDataTypeNum
	};

protected:

	// PURPOSE: Internal constructor - base cannot be directly constructed
	crpmParameterizedMotionData(eParameterizedMotionDataType pmDataType=kPmDataTypeNone);

public:

	// PURPOSE: Resource constructor
	crpmParameterizedMotionData(datResource&);

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotionData();


	// PURPOSE: Placement
	static void Place(void*, datResource&);

	// PURPOSE: Fixes up derived type from pointer to base class only
	static void VirtualConstructFromPtr(datResource&, crpmParameterizedMotionData* base);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Off-line resource version
	static const int RORC_VERSION = 13;

	// PURPOSE: Shutdown
	virtual void Shutdown();

#if CR_DEV
	// PURPOSE: Allocate and load from file
	// PARAMS:
	// filename - parameterized motion filename
	// pmLoader - parameterized motion loader
	// clipLoader - clip loader
	// animLoader - animation loader
	// RETURNS: If successful returns pointer to newly allocated and loaded simple data, otherwise NULL.
	// NOTES: Determines parameterized motion data type by examining filename extension
	static crpmParameterizedMotionData* AllocateAndLoad(const char* filename, crpmParameterizedMotionLoader* pmLoader=NULL, crClipLoader* clipLoader=NULL, crAnimLoader* animLoader=NULL);

	// PURPOSE: Dump debug output to string
	void Dump(atString& inoutText) const;

	// PURPOSE: Dump debug output
	virtual void Dump(crDumpOutput& inoutDumpOuput) const;
#endif // CR_DEV

	// PURPOSE: Add reference count
	void AddRef() const;

	// PURPOSE: Release reference count, destroy if count reaches zero
	// RETURNS: New reference count
	u32 Release() const;

	// PURPOSE: Get current reference count
	u32 GetRef() const;


	// PURPOSE: Get parameterized motion data type
	eParameterizedMotionDataType GetType() const;

	// PURPOSE: Get name (from filename when loaded)
	const char* GetName() const;

protected:

	// PURPOSE: Push relative folder (used to make referenced files relative to parent asset)
	bool PushParameterizedMotionFolder() const;


	mutable u32 m_RefCount;
	eParameterizedMotionDataType m_Type;
	atString m_Name;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base class for all parameterized motion instances
class crpmParameterizedMotion
{
public:

	// PURPOSE: Enumeration of types of parameterized motion instances
	// NOTES: Do not assume this is parallel with types of parameterized motion data
	enum eParameterizedMotionType
	{
		kPmTypeNone,
		kPmTypeSimple,
		kPmTypeVariable,
//		kPmTypeComposite,

		// must be last in list
		kPmTypeNum
	};

protected:

	// PURPOSE: Internal constructor - base cannot be directly constructed
	crpmParameterizedMotion(eParameterizedMotionType pmType=kPmTypeNone);

public:

	// PURPOSE: Destructor
	virtual ~crpmParameterizedMotion();

	// PURPOSE: Clear
	// NOTES: Like shutdown, clear detaches from data and resets interval state,
	// but unlike shutdown it does not free dynamic allocation - so if instance has been
	// preinitialized to worst case it can be re-used by subsequent init call without
	// triggering more dynamic allocation.
	virtual void Clear();

	// PURPOSE: Shutdown
	virtual void Shutdown();


	// PURPOSE: Allocate based on type
	// PARAMS: type - parameterized motion type to allocate
	// RETURNS: Newly allocated parameterized motion instance
	static crpmParameterizedMotion* Allocate(eParameterizedMotionType type);

	// PURPOSE: Allocate based on data
	// PARAMS: data - parameterized motion data to allocate instance for
	// RETURNS: Newly allocated and initialized parameterized motion instance
	// NOTES: This also initializes the parameterized motion instance
	static crpmParameterizedMotion* AllocateFromData(const crpmParameterizedMotionData& data);

	
	// PURPOSE: Get parameterized motion instance type
	eParameterizedMotionType GetType() const;

	// PURPOSE: Get parameterized motion instance type from data
	// PARAM: data - parameterized motion data to find instance type for
	// RETURNS: Suitable instance type for this parameterized motion data
	static eParameterizedMotionType GetTypeFromData(const crpmParameterizedMotionData& data);


	// PURPOSE: Update parameterized motion
	// PARAMS: deltaTime - delta time in seconds (must be >= 0.0)
	void Update(float deltaTime);

	// PURPOSE: Update parameterized motion (override to implement)
	// PARAMS:
	// rcDeltaTime - delta time in seconds to update registration curve (must be >= 0.0)
	// parameterDeltaTime - delta time in seconds to update parameter interpolation (must be >= 0.0)
	virtual void Update(float rcDeltaTime, float parameterDeltaTime) = 0;

	// PURPOSE: Composite frame using compositor (override to implement)
	// PARAMS: inoutCompositor - frame compositor
	virtual void Composite(crFrameCompositor& inoutCompositor) = 0;

	// PURPOSE: Composite frame
	// PARAMS: outFrame - frame with already populated DOFs, values will be set with current pose
#if USE_PM_COMPOSITOR
	void Composite(crFrame& outFrame);
#else // USE_PM_COMPOSITOR
	virtual void Composite(crFrame& outFrame) = 0;
#endif // USE_PM_COMPOSITOR


	// TODO - RESET?


	// PURPOSE: Get number of parameter dimensions
	// RETURNS: Number of parameter dimensions (will be >= 1)
	int GetNumParameterDimensions() const;

	// PURPOSE: Get current parameter value
	// PARAMS: outParameter - parameter (with correct number of dimensions), values will be set
	void GetParameter(crpmParameter& outParameter) const;

	// PURPOSE: Get current parameter value by reference (override to implement)
	virtual const crpmParameter& GetParameter() const = 0;

	// PURPOSE: Get current parameter value by index
	// PARAMS: dim - parameter dimension index [0..numDimensions-1]
	// RETURNS: value of parameter on given dimension
	float GetParameterValue(int dim) const;

	// PURPOSE: Set new parameter value (override to implement)
	// PARAMS:
	// parameter - new parameter values (with correct number of dimensions)
	// force - override any rate of change interpolation and immediately change parameter
	virtual void SetParameter(const crpmParameter& parameter, bool force=false) = 0;
	
	// PURPOSE: Get current parameter value by index
	// PARAMS:
	// dim - parameter dimension index [0..numDimensions-1]
	// value - new value for parameter (for given dimension)
	// force - override any rate of change interpolation and immediately change parameter
	// NOTE: Setting force to true when using setting by value may force all dimensions to
	// change immediately to target values
	virtual void SetParameterValue(int dim, float value, bool force=false) = 0;

	// PURPOSE: Get parameterization - a class that describes the different parameter dimensions (override to implement)
	virtual const crpmParameterization& GetParameterization() const = 0;
	
	
	// PURPOSE: Get parameter limits (override to implement)
	// PARAMS: outMinLimit, outMaxLimit - min and max parameter values for each dimension
	// NOTES: Boundaries are min and max possible values achievable by the source data,
	// while limits are min and max values permitted as defined by the runtime.
	// By default limits are initialized to be the same as the boundary - but they can be varied.
	virtual void GetParameterLimits(crpmParameter& outMinLimit, crpmParameter& outMaxLimit) const = 0;

	// PURPOSE: Set parameter limits (override to implement)
	// PARAMS: minLimit, maxLimit - min and max parameter values for each dimension
	// NOTES: Boundaries are min and max possible values achievable by the source data,
	// while limits are min and max values permitted as defined by the runtime.
	// By default limits are initialized to be the same as the boundary - but they can be varied.
	virtual void SetParameterLimits(const crpmParameter& minLimit, const crpmParameter& maxLimit) = 0;

	// PURPOSE: Get parameter boundaries (override to implement)
	// PARAMS: outMinBoundary, outMaxBoundary - min and max parameter boundaries for each dimension
	// NOTES: Boundaries are min and max possible values achievable by the source data,
	// while limits are min and max values permitted as defined by the runtime.
	// By default limits are initialized to be the same as the boundary - but they can be varied.
	virtual void GetParameterBoundaries(crpmParameter& outMinBoundary, crpmParameter& outMaxBoundary) const = 0;

	// PURPOSE: Get parameter rates of change (override to implement)
	// PARAMS: outRates - maximum rate of change for parameter values for each dimension
	virtual void GetParameterRates(crpmParameter& outRates) const = 0;

	// PURPOSE: Set parameter rates of change (override to implement)
	// PARAMS: rates - maximum rate of change for parameter values for each dimension
	virtual void SetParameterRates(const crpmParameter& rates) = 0;


	// PURPOSE: Get current phase of parameterized motion (override to implement)
	// RETURNS: Current phase [0..1]
	virtual float GetU() const = 0;

	// PURPOSE: Set new phase of parameterized motion (override to implement)
	// PARAMS: u - new phase [0..1]
	virtual void SetU(float u) = 0;


	// PURPOSE: Get current delta phase of parameterized motion (override to implement)
	// RETURNS: Current delta phase [>=0]
	virtual float GetDeltaU() const = 0;

	// PURPOSE: Set new delta phase of parameterized motion (override to implement)
	// PARAMS: deltaU - new delta phase [>=0]
	virtual void SetDeltaU(float deltaU) = 0;


	// PURPOSE: Get current post delta phase of parameterized motion (override to implement)
	// RETURNS: Current post delta phase [>=0]
	virtual float GetPostDeltaU() const = 0;

	// PURPOSE: Set new post delta phase of parameterized motion (override to implement)
	// PARAMS: deltaU - new post delta phase [>=0]
	virtual void SetPostDeltaU(float deltaU) = 0;

	
	// PURPOSE: Is the parameterized motion looping?  (override to implement)
	// RETURNS: true - looping, false - non-looping
	virtual bool IsLooped() const = 0;

	// PURPOSE: Set the parameterized motion looping behavior (override to implement)
	// PARAMS: looped - set true to force looping, false to
	virtual void SetLooped(bool looped) = 0;

	// PURPOSE: Has the non-looping parameterized motion finished?  (override to implement)
	// RETURNS: true - if non-looping
	virtual bool IsFinished() const = 0;

	// PURPOSE: Get remaining time left to play (to loop, or to finish if non-looping)
	// RETURNS: time remaining (in seconds)
	// NOTES: Dynamic, can vary as parameters are changed (as well as when motion progresses)
	virtual float GetRemainingTime() const = 0;

	// PURPOSE: Get total duration
	// RETURNS: estimated total duration (in seconds)
	// NOTES: Dynamic, can vary as parameters are changed
	virtual float GetDuration() const = 0;

	// PURPOSE: Get maximum duration
	// RETURNS: Maximum duration (in seconds)
	virtual float GetMaxDuration() const = 0;


	// PURPOSE: Does this parameterized motion have a tag manager
	// RETURNS: true - has a tag manager, false - no tag manager
	bool HasTags() const;

	// PURPOSE: Get tags manager (override to implement)
	// RETURNS: Const pointer to tags object (NULL if none exists)
	virtual const crTags* GetTags() const = 0;


	// PURPOSE: Does this parameterized motion have a property manager
	// RETURNS: true - has a property manager, false - no property manager
	bool HasProperties() const;

	// PURPOSE: Get property manager (override to implement)
	// RETURNS: Const pointer to property object (NULL if none exists)
	virtual const crProperties* GetProperties() const = 0;


	// PURPOSE: Get parameterized motion type data
	const crpmParameterizedMotionData* GetData() const;


	// PURPOSE: Initialize parameterized motion classes
	static void InitClass();

	// PURPOSE: Shutdown parameterized motion classes
	static void ShutdownClass();


	// TODO - JamesM - call m_Data->AddRef here, or something.
	void AddRef() { }

	// TODO - JamesM - call m_Data->Release here, or something.
	void Release() { }

protected:

	// PURPOSE: Internal pre-initialization, call from derived pre-initialization calls
	void InternalPreInit(int worstNumParameterDimensions);

	// PURPOSE: Internal initialization, call from derived initialization calls
	void InternalInit(const crpmParameterizedMotionData& data);


protected:

	eParameterizedMotionType m_Type;
	pgRef<const crpmParameterizedMotionData> m_Data;

	mutable crpmParameter m_TempParameter;

	static bool sm_InitClassCalled;
};


////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotionData::AddRef() const
{
#if PM_ATOMIC_REFCOUNT
	sysInterlockedIncrement(&m_RefCount);
#else // PM_ATOMIC_REFCOUNT
	++m_RefCount;
#endif // PM_ATOMIC_REFCOUNT
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crpmParameterizedMotionData::Release() const
{
#if PM_ATOMIC_REFCOUNT
	int refCount = int(sysInterlockedDecrement(&m_RefCount));
	if(!refCount)
	{
		delete this;
		return 0;
	}
	else
	{
		return refCount;
	}
#else // PM_ATOMIC_REFCOUNT
	if(!--m_RefCount)
	{
		delete this;
		return 0;
	}
	else
	{
		return m_RefCount;
	}
#endif // PM_ATOMIC_REFCOUNT
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crpmParameterizedMotionData::GetRef() const
{
	return m_RefCount;	
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotionData::eParameterizedMotionDataType crpmParameterizedMotionData::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

inline const char* crpmParameterizedMotionData::GetName() const
{
	return m_Name;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotion::eParameterizedMotionType crpmParameterizedMotion::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterizedMotion::Update(float deltaTime)
{
	Update(deltaTime, deltaTime);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmParameterizedMotion::HasTags() const
{
	return GetTags() != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crpmParameterizedMotion::HasProperties() const
{
	return GetProperties() != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterizedMotionData* crpmParameterizedMotion::GetData() const
{
	return m_Data;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTION_H
