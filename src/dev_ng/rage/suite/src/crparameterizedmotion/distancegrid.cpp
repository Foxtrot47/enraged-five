//
// crparameterizedmotion/distancegrid.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "distancegrid.h"

#include "colormapper.h"
#include "image.h"
#include "pointcloudcache.h"

#include "crclip/clip.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmDistanceGrid::crpmDistanceGrid()
: m_ClipX(NULL)
, m_ClipY(NULL)
, m_CyclesX(1)
, m_CyclesY(1)
, m_RepeatX(1)
, m_RepeatY(1)
, m_SizeX(0)
, m_SizeY(0)
, m_MinDistance(FLT_MAX)
, m_MaxDistance(-FLT_MAX)
, m_SampleRateX(1.f/30.f)
, m_SampleRateY(1.f/30.f)
{
}

////////////////////////////////////////////////////////////////////////////////

int GetCycles(const crClip& clip)
{
	if(clip.GetProperties())
	{
		const crProperty* prop = clip.GetProperties()->FindProperty("cycles");
		if(prop)
		{
			const crPropertyAttribute* propAttribute = prop->GetAttribute("cycles");
			if(propAttribute)
			{
				if(propAttribute->GetType() == crPropertyAttribute::kTypeInt)
				{
					const crPropertyAttributeInt* propAttributeInt = static_cast<const crPropertyAttributeInt*>(propAttribute);
					return propAttributeInt->GetInt();
				}
			}
		}
	}
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

int GreatestCommonDivisor(int a, int b)
{
	while(b)
	{
		int t = b;
		b = a%b;
		a = t;
	}
	return a;
}

////////////////////////////////////////////////////////////////////////////////

int LeastCommonMultiple(int a, int b)
{
	return a * b / GreatestCommonDivisor(Max(a,b), Min(a,b));
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::Calculate(const crClip& clipY, const crClip& clipX, const crSkeletonData& skelData, int windowSize, float sampleRate, float maxDistance, const crWeightSet* weightSet, const crWeightSet* filterWeightSet, bool filterMover)
{
	m_ClipX = &clipX;
	m_ClipY = &clipY;

	m_SampleRateX = sampleRate;
	m_SampleRateY = sampleRate;

	m_CyclesX = GetCycles(clipX);
	m_CyclesY = GetCycles(clipY);

	int lcm = LeastCommonMultiple(m_CyclesX, m_CyclesY);
	m_RepeatX = lcm / m_CyclesX;
	m_RepeatY = lcm / m_CyclesY;

	int framesX = int((clipX.GetDuration()/m_SampleRateX)+0.5f)+1;
	int framesY = int((clipY.GetDuration()/m_SampleRateY)+0.5f)+1;

	int sizeX = (framesX-1)*m_RepeatX+1;
	int sizeY = (framesY-1)*m_RepeatY+1;

	CreateEmptyCells(sizeX, sizeY);

	crpmPointCloudCache cacheX(clipX, skelData, windowSize, m_SampleRateX, weightSet, filterWeightSet, filterMover);
	crpmPointCloudCache cacheY(clipY, skelData, windowSize, m_SampleRateY, weightSet, filterWeightSet, filterMover);

	m_MinDistance = FLT_MAX;
	m_MaxDistance = -FLT_MAX;

	const bool symmetrical = IsSymmetrical();

    for(int y=0; y<m_SizeY; ++y)
    {
		int iy = (m_RepeatY>1)?(y%framesY):y;
		const crpmPointCloud& pcy = cacheY.GetPointCloud(iy);

		int topX = symmetrical ? (y+1) : m_SizeX;
        for(int x=0; x<topX; ++x)
        {
			int ix = (m_RepeatX>1)?(x%framesX):x;
			const crpmPointCloud& pcx = cacheX.GetPointCloud(ix);

            float distance = 0.f;
            crpmPointCloudTransform distanceTransform;

			distance = pcy.ComputeDistance(pcx, distanceTransform);
            distance = Min(distance, maxDistance);

            m_Cells[x][y] = Cell(distance, distanceTransform);

            m_MinDistance = Min(m_MinDistance, distance);
            m_MaxDistance = Max(m_MaxDistance, distance);
        }
    }

    // valid region is at most 15% larger than local minimum value
    const float threshold = 0.15f;
	CalcValidRegion(threshold);
}

////////////////////////////////////////////////////////////////////////////////

crpmDistanceGrid::Path::Path()
{
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::Path::Add(int x, int y, float distance)
{
	m_Path.PushAndGrow(Coordinate(x, y, distance));
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::Path::AddSafe(int x, int y, float distance)
{
	Coordinate c(x, y, distance);
	if(GetLength() > 1 && c.IsInRange(*(m_Path.rbegin()+1)))
	{
		m_Path.back() = c;
	}
	else
	{
		Add(x, y, distance);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::Path::Clear()
{
	m_Path.Reset();
}

////////////////////////////////////////////////////////////////////////////////

int crpmDistanceGrid::Path::GetLength() const
{
	return int(m_Path.GetCount());
}

////////////////////////////////////////////////////////////////////////////////

float crpmDistanceGrid::Path::GetDistanceSum() const
{
	float sum = 0.f;
	for(const_iterator it=m_Path.begin(); it!=m_Path.end(); ++it)
	{
		sum += it->distance;
	}

	return sum;
}

////////////////////////////////////////////////////////////////////////////////

float crpmDistanceGrid::Path::GetDistanceAverage() const
{
	if(m_Path.empty())
	{
		return 0.f;
	}
	else
	{
		return GetDistanceSum() / float(GetLength());
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDistanceGrid::Path::IsContinuous() const
{
	if(m_Path.size() < 2)
	{
		return true;
	}

	for(const_iterator it=m_Path.begin()+1; it!=m_Path.end(); ++it)
	{
		if(!it->IsInRange(*(it-1)))
		{
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDistanceGrid::Path::Contains(int x, int y) const
{
	for(const_iterator it=m_Path.begin(); it!=m_Path.end(); ++it)
	{
		if((x == it->x) && (y == it->y))
		{
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::Path::RemoveRange(int a, int b)
{
	AssertMsg(a <= int(m_Path.size()) && b <= int(m_Path.size()), "crpmDistanceGrid::Path::RemoveRange - removing items out of range");
	AssertMsg(b >= a, "crpmDistanceGrid::Path::RemoveRange - negative range");

	if (a < b)
	{
		m_Path.erase(m_Path.begin()+a, m_Path.begin()+b);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::Path::Dump() const
{
	for(const_iterator it=m_Path.begin(); it!=m_Path.end(); ++it)
	{
		Printf("(%i,%i) ", it->x, it->y);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::Path::MakePathImage(crpmImage& outImage, Color32* col) const
{
	for(const_iterator it=begin(); it!=end(); ++it)
	{
		int x = it->x;
		int y = it->y;

		Color32 rgba(0, 192, 192, 255);

		if (col)
		{
			rgba = *col;
		}

		if ((x >= 0) && (y >= 0))
		{
			outImage.SetPixel(x, y, rgba);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::Dump() const
{
	for(int y=m_SizeY-1; y>=0; --y)
	{
		for(int x=0; x<m_SizeX; ++x)
		{
			Printf("%10.4f\t", GetCellDistance(x, y));
		}

		Printf("\n");
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::MakeDistanceGridImage(crpmImage& outImage, bool heatMap) const
{
	const float power = 0.3f;

	float minValue = pow(GetMinDistance(), power);
	float maxValue = pow(GetMaxDistance(), power);

	crpmColorMapperGrayscale grayScaleMapper(minValue, maxValue);
	crpmColorMapperHeat heatMapper(minValue, maxValue);

	crpmColorMapper* colorMapper;
	if(heatMap)
	{
		colorMapper = &heatMapper;
	}
	else
	{
		colorMapper = &grayScaleMapper;
	}

	for(int y=0; y<m_SizeY; ++y)
	{
		for(int x=0; x<m_SizeX; ++x)
		{
			Color32 rgba;
			rgba = colorMapper->NegativeMap(pow(GetCellDistance(x, y), power));
			outImage.SetPixel(x, y, rgba);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::MakeValidRegionImage(crpmImage& image) const
{
	for(int y=0; y<GetSizeY(); ++y)
	{
		for(int x=0; x<GetSizeX(); ++x)
		{
			Color32 rgba;
			if(IsCellLocalMinimum(x, y))
			{
				rgba.Set(255, 0, 255, 0);
			}
			else if(GetCell(x, y).IsValidRegion())
			{
				rgba.Set(255, 255, 0, 0);
			}
			else
			{
				rgba.Set(64, 64, 64, 64);
			}
			image.SetPixel(x, y, rgba);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::CreateEmptyCells(int sizeX, int sizeY)
{
	m_SizeX = sizeX;
	m_SizeY = sizeY;

	m_Cells.Reset();
	m_Cells.Reserve(sizeX);

	for(int x=0; x<m_SizeX; ++x)
	{
		atArray<Cell> columnVector(sizeY, sizeY);
		m_Cells.PushAndGrow(columnVector);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::CalcValidRegion(float threshold)
{
	const bool symmetrical = IsSymmetrical();

    for(int y=0; y<m_SizeY; ++y)
    {
		int topX = symmetrical ? (y+1) : m_SizeX;
		for(int x=0; x<topX; ++x)
        {
			if(IsCellHorizontalMinimum(x, y))
			{
				m_Cells[x][y].SetLocalMinimum(true);
				FillValidRegionHorizontally(x, y, +1, threshold);
				FillValidRegionHorizontally(x, y, -1, threshold);
			}

			if (IsCellVerticalMinimum(x, y))
			{
				m_Cells[x][y].SetLocalMinimum(true);
				FillValidRegionVertically(x, y, +1, threshold);
				FillValidRegionVertically(x, y, -1, threshold);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDistanceGrid::IsCellVerticalMinimum(int x, int y)
{
    float distance = GetCellDistance(x, y);
    float distanceDown = (y == 0) ? FLT_MAX : GetCellDistance(x, y-1);
    float distanceUp = (y == (m_SizeY-1)) ? FLT_MAX : GetCellDistance(x, y+1);

	return (distance < distanceDown) && (distance < distanceUp);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmDistanceGrid::IsCellHorizontalMinimum(int x, int y)
{
    float distance = GetCellDistance(x, y);
    float distanceDown = (x == 0) ? FLT_MAX : GetCellDistance(x-1, y);
    float distanceUp = (x == (m_SizeX-1)) ? FLT_MAX : GetCellDistance(x+1, y);

    return (distance < distanceDown) && (distance < distanceUp);
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::FillValidRegionHorizontally(int x, int y, int direction, float threshold)
{
	int n = x + direction;
	while((n < m_SizeX) && (n >= 0) && (fabsf(GetCellDistance(n, y) / GetCellDistance(x,y) - 1.0f) < threshold))
	{
		m_Cells[n][y].SetValidRegion(true);
		n += direction;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmDistanceGrid::FillValidRegionVertically(int x, int y, int direction, float threshold)
{
	int n = y + direction;
	while((n < m_SizeY) && (n >= 0) && (fabsf(GetCellDistance(x, n) / GetCellDistance(x, y) - 1.0f) < threshold))
	{
		m_Cells[x][n].SetValidRegion(true);
		n += direction;
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
