//
// crparameterizedmotion/registrationcurve.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "registrationcurve.h"

#include "weights.h"

#include "atl/array_struct.h"
#include "crclip/clip.h"
#include "crclip/clips.h"
#include "crmetadata/dumpoutput.h"
#include "data/safestruct.h"
#include "system/memory.h"

namespace rage
{

extern __THREAD int s_PmSimpleSerializeVersion;

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurveData::crpmRegistrationCurveData()
: m_NumClips(0)
, m_ReferenceClipIndex(0)
, m_MaxNumLoops(1)
, m_IsLooped(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurveData::crpmRegistrationCurveData(datResource& rsc)
: m_AlignmentCurves(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmRegistrationCurveData::~crpmRegistrationCurveData()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmRegistrationCurveData);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmRegistrationCurveData::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmRegistrationCurveData)
	SSTRUCT_FIELD(crpmRegistrationCurveData, m_AlignmentCurves)
	SSTRUCT_FIELD(crpmRegistrationCurveData, m_NumClips)
	SSTRUCT_FIELD(crpmRegistrationCurveData, m_ReferenceClipIndex)
	SSTRUCT_FIELD(crpmRegistrationCurveData, m_MaxNumLoops)
	SSTRUCT_FIELD(crpmRegistrationCurveData, m_IsLooped)
	SSTRUCT_IGNORE(crpmRegistrationCurveData, m_Padding)
	SSTRUCT_END(crpmRegistrationCurveData)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurveData::Shutdown()
{
	for(u16 i=0; i<m_NumClips; ++i)
	{
		delete m_AlignmentCurves[i];
		m_AlignmentCurves[i] = NULL;
	}
	m_AlignmentCurves.Reset();
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurveData::MapUToClipU(float u, int clipIdx) const
{
	Assert(InRange(clipIdx, 0, m_NumClips-1));

	return m_AlignmentCurves[clipIdx]->GetInversePosition(u, 3);
//	return m_AlignmentCurves[clipIdx]->GetPosition(u);
}

////////////////////////////////////////////////////////////////////////////////

float crpmRegistrationCurveData::MapClipUToU(float clipU, int clipIdx) const
{
	Assert(InRange(clipIdx, 0, m_NumClips-1));

	return m_AlignmentCurves[clipIdx]->GetPosition(clipU);
//	return m_AlignmentCurves[clipIdx]->GetInversePosition(clipU, 3);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crpmRegistrationCurveData::Serialize(datSerialize& s, float* rescale)
{
	s << m_NumClips;
	s << m_ReferenceClipIndex;
	s << m_IsLooped;

	if(s_PmSimpleSerializeVersion >= 9)
	{
		s << m_MaxNumLoops;
	}

	if(s.IsRead())
	{
		m_AlignmentCurves.Resize(m_NumClips);
		for(u16 i=0; i<m_NumClips; ++i)
		{
			m_AlignmentCurves[i] = rage_new crpmSeries<float>;
		}
	}

	if(s_PmSimpleSerializeVersion < 2)
	{
		for(u16 i=0; i<m_NumClips; ++i)
		{
			sysMemStartTemp();
			{
				atArray<Vector4> points;
				SerializeAtlArrayAsStlArray(s, points);
				
				atArray<float> timeAlignment;
				timeAlignment.Reserve(points.size());

				for(int p=0; p<points.GetCount(); ++p)
				{
					float rs = rescale?rescale[i]:1.f;
					timeAlignment.PushAndGrow(points[p].w*rs);
				}

				if(timeAlignment.size())
				{
					crpmSeries<float> reverse;
					reverse.Init( (int) timeAlignment.size(), &timeAlignment[0]);

					for(int p=0; p<timeAlignment.GetCount(); ++p)
					{
						timeAlignment[p] = reverse.GetInversePosition(float(p)/float(timeAlignment.size()-1), 0);
					}
					timeAlignment[0] = 0.f;
					timeAlignment[timeAlignment.size()-1] = 1.f;
				}

				sysMemEndTemp();
				m_AlignmentCurves[i]->Init((int)points.size(), &timeAlignment[0]);
				sysMemStartTemp();
			}
			sysMemEndTemp();
		}
	}
	else
	{
		bool noSpatialAlignment = true;
		s << noSpatialAlignment;

		for(u16 i=0; i<m_NumClips; ++i)
		{
			if(s_PmSimpleSerializeVersion < 9)
			{
				sysMemStartTemp();
				{
					atArray<float> timeAlignment;
					SerializeAtlArrayAsStlArray(s, timeAlignment);

					for(int p=0; p<timeAlignment.GetCount(); ++p)
					{
						timeAlignment[p] = Clamp(timeAlignment[p]*(rescale?rescale[i]:1.f), 0.f, 1.f);
					}
					if(timeAlignment.size())
					{
						crpmSeries<float> reverse;
						reverse.Init( (int) timeAlignment.size(), &timeAlignment[0]);

						for(int p=0; p<timeAlignment.GetCount(); ++p)
						{
							timeAlignment[p] = reverse.GetInversePosition(float(p)/float(timeAlignment.size()-1), 0);
						}
						timeAlignment[0] = 0.f;
						timeAlignment[timeAlignment.size()-1] = 1.f;

						sysMemEndTemp();
						m_AlignmentCurves[i]->Init( (int) timeAlignment.size(), &timeAlignment[0]);
						sysMemStartTemp();
					}
				}
				sysMemEndTemp();
			}
			else
			{
				s << *m_AlignmentCurves[i];
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmRegistrationCurveData::Dump(crDumpOutput& output) const
{
	output.Outputf(0, "islooped", "%d", m_IsLooped);
	output.Outputf(1, "refclipidx", "%d", m_ReferenceClipIndex);

	output.Outputf(1, "numalignments", "%d", m_NumClips);
	for(u16 i=0; i<m_NumClips; ++i)
	{
		const int num = m_AlignmentCurves[i]->GetNumPoints();
		for(int n=0; n<num; ++n)
		{
			output.Outputf(2, "alignment", i, n, "%f", m_AlignmentCurves[i]->GetPosition(float(n)/float(num-1)));
		}
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

