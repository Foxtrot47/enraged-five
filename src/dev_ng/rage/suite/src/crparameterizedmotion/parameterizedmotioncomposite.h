//
// crparameterizedmotion/parameterizedmotioncomposite.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONCOMPOSITE_H
#define CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONCOMPOSITE_H

#if 0 // COMPOSITE PM'S REMOVED TEMPORARILY





#include "system/noncopyable.h"
#include "parameterizedmotion.h"
#include "parameter.h"

namespace rage
{

// forward decls
class crFrame;
class datSerialize;
class crpmRegistrationCurveData;
class crpmBlendDatabase;
class crpmParameterization;
class crpmParameterSampler;
class crpmParameter;
class crpmParameterizedMotion;
class crpmParameterInterpolator;
class crpmParameterTranslator;

// PURPOSE:  A composite parameterized motion acts like a regular parameterized motion but
//           it's purpose is to redistribute parameters to subparameterized motions, producing
//           a single composite motion from the overall results.
// NOTES:  [rheck] This composite parameterized motion class was in the early stages of development.  At the moment
//         it can only act as a "switch" between sub parameterized motions.  It uses the incoming
//         parameters to chose a sub parameterized motion to use and then translates the parameters
//         into the appropriate form needed for by that sub parameterized motion.  This special case is a
//         very useful one but, as is, this class fails to meet its potential.
//         Two other possible uses for composite parameterized motions that I can think of off the top of my head are:
//           (1)  Splicing upper-bodies onto lower-bodies, where  upper-bodies are produced by one parameterized
//                motion and lower-bodies are produced by another parameterized motion
//           (2)  Combining a parameterized motion with a parameterized displacement map (i.e. additive motion)
class crpmParameterizedMotionComposite : public sysNonCopyable, public crpmParameterizedMotion
{  // TODO --- THIS IS A SERIAL COMPOSITE PM, THERE WILL BE A PARALLEL ONE TOO
private:

    crpmParameterizedMotionComposite(const std::string& name, std::vector<crpmParameterizedMotion*>& , const crpmParameterTranslator& );
    ~crpmParameterizedMotionComposite();

    void translateParameter(const crpmParameter& in);

public:

    // continue from a position with timewarp
    void continueFromAdvanceTime(Mat34V_In pos, const float desiredTime);

    // spatial data access/modification
    void continueFrom(Mat34V_In pos);
//    Mat34V_Out getCurrentRootFrame() const;
    Mat34V_Out getCurrentMoverFrame() const;
//
    // setting parameter
    void setParameter(const crpmParameter& parameter);
    void setParameterInterpolated(const crpmParameter& parameter, const float weight = 0.1f);
    void setTargetParameter(const crpmParameter& targetParameter, const float blendSeconds);
    void resetParameterInterpolation();
    void setParameterWithFilteredWeights(const crpmParameter& parameter, float filter);

    //TODO: remove this from the interface?
    void setHandleLooping(bool);

	// playback access
	float getCurrentU() const;

    bool isLooping();

    // playback
    void update(const float animDeltaTime, const float parameterDeltaTime);
    void reset();
	virtual void Composite(crFrame&) const;
//    const crFrame& getAnimFrame() const;
    bool isFinished();
    void timeAlignFrom(const crpmParameterizedMotion&);
    void enableDirectParameterMapping(bool enable);

    // tag access
    bool hasTag(const std::string& tag) const;
    bool hasTagAtCurrentU(const std::string& tag) const;
    bool hasTagWithinDeltaSeconds(const std::string& tag, float deltaSeconds = 0.0f) const;

    // name access
    const std::string& getName() const { return mName; }

    // debug access
    const crpmWeights& getCurrentWeights() const;
    void setWeightsDirectly(const crpmWeights& weights);

    // data access
    int getWeightDimension() const;
    int getParameterDimension() const;
    void getParameterBoundaries(crpmParameter& min, crpmParameter& max) const;
    float getRemainingPlayTime() const;
    float getMaxDuration() const;
//    bool hasLooped() const;
    const crpmRegistrationCurvePlayer& getPlayback() const;
    const crClip* getReferenceClip() const;

    // identity access
    bool isSimple() { return false; }

    crpmParameterizedMotion* getCurrentChild() const;

private:
    bool checkAllHaveSameProperties() const;

private:
    std::vector<crpmParameterizedMotion*> mChilds;
    const crpmParameterTranslator& mTranslator;
    crpmParameter mOutParameter;
    int mSelectedChild;
    std::string mName;
};


} // namespace rage

#endif // 0 COMPOSITE PM'S REMOVED TEMPORARILY

#endif // CRPARAMETERIZEDMOTION_PARAMETERIZEDMOTIONCOMPOSITE_H
