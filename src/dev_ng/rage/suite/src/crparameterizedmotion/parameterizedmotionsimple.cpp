//
// crparameterizedmotion/parameterizedmotionsimple.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "parameterizedmotionsimple.h"

#include "blenddatabase.h"
#include "registrationcurve.h"
#include "registrationcurveplayer.h"
#include "weights.h"

#include "bank/bank.h"
#include "cranimation/animdictionary.h"
#include "crclip/clipdictionary.h"
#include "crclip/clips.h"
#include "crmetadata/dumpoutput.h"
#include "crmetadata/properties.h"
#include "crmetadata/tags.h"
#include "data/safestruct.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimpleData::crpmParameterizedMotionSimpleData(eParameterizedMotionDataType pmDataType)
: crpmParameterizedMotionData(pmDataType)
, m_Clips(NULL)
, m_RcData(NULL)
, m_BlendDatabase(NULL)
, m_Tags(NULL)
, m_Properties(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimpleData::crpmParameterizedMotionSimpleData()
: crpmParameterizedMotionData(kPmDataTypeSimple)
, m_Clips(NULL)
, m_RcData(NULL)
, m_BlendDatabase(NULL)
, m_Tags(NULL)
, m_Properties(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimpleData::crpmParameterizedMotionSimpleData(datResource& rsc)
: crpmParameterizedMotionData(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimpleData::crpmParameterizedMotionSimpleData(const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags, const crProperties* properties)
: crpmParameterizedMotionData(kPmDataTypeSimple)
, m_Clips(NULL)
, m_RcData(NULL)
, m_BlendDatabase(NULL)
, m_Tags(NULL)
, m_Properties(NULL)
{
	Init(clips, rcData, bd, tags, properties);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimpleData::~crpmParameterizedMotionSimpleData()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmParameterizedMotionSimpleData);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmParameterizedMotionSimpleData::DeclareStruct(datTypeStruct& s)
{
	crpmParameterizedMotionData::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crpmParameterizedMotionSimpleData, crpmParameterizedMotionData)
	SSTRUCT_FIELD(crpmParameterizedMotionSimpleData, m_Clips)
	SSTRUCT_FIELD(crpmParameterizedMotionSimpleData, m_RcData)
	SSTRUCT_FIELD(crpmParameterizedMotionSimpleData, m_BlendDatabase)
	SSTRUCT_FIELD(crpmParameterizedMotionSimpleData, m_Tags)
	SSTRUCT_FIELD(crpmParameterizedMotionSimpleData, m_Properties)
	SSTRUCT_END(crpmParameterizedMotionSimpleData)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimpleData::Init(const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags, const crProperties* properties)
{
	Assert(clips.GetNumClips() == u16(bd.GetNumWeightDimensions()));
	Assert(!rcData || clips.GetNumClips() == rcData->GetNumClips());

	m_Clips = const_cast<crClips*>(&clips);
	m_RcData = const_cast<crpmRegistrationCurveData*>(rcData);
	m_BlendDatabase = const_cast<crpmBlendDatabase*>(&bd);
	m_Tags = const_cast<crTags*>(tags);
	m_Properties = const_cast<crProperties*>(properties);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimpleData::Shutdown()
{
	if(m_Clips)
	{
		delete m_Clips;
		m_Clips = NULL;
	}

	if(m_RcData)
	{
		delete m_RcData;
		m_RcData = NULL;
	}

	if(m_BlendDatabase)
	{
		delete m_BlendDatabase;
		m_BlendDatabase = NULL;
	}

	if(m_Tags)
	{
		delete m_Tags;
		m_Tags = NULL;
	}

	if(m_Properties)
	{
		delete m_Properties;
		m_Properties = NULL;
	}

	crpmParameterizedMotionData::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
bool crpmParameterizedMotionSimpleData::Load(const char* filename, crClipLoader* clipLoader, crAnimLoader* animLoader)
{
	s_PmSimpleClipLoader = clipLoader;
	s_PmSimpleAnimLoader = animLoader;

	fiStream* f = ASSET.Open(filename, "spm", false, true);
	if(!f)
	{
		Errorf("crpmParameterizedMotionSimpleData::Load - failed to open file '%s' for reading", filename);
		return false;
	}

	int magic = 0;
	f->ReadInt(&magic, 1);

	if(magic != 'DSMP')
	{
		Errorf("crpmParameterizedMotionSimpleData::Load - file is not a simple pm file");
		f->Close();
		return false;
	}

	fiSerialize s(f, true, false);

	u8 version = 0;
	s << version;
	
	const u8 latestVersion = sm_FileVersions[0];
	if(version > latestVersion)
	{
		Errorf("crpmParameterizedMotionSimpleData::Load - attempting to load incompatiable simple pm file version '%d' (only up to '%d' supported)", version, latestVersion);
		return false;
	}

	s_PmSimpleSerializeVersion = version;

	m_Name = filename;
	if(fiSerializeFrom(f, *this))
	{
		f->Close();
		return true;
	}

	Errorf("crpmParameterizedMotionSimpleData::Load - failed to load file '%s'", filename);
	f->Close();

	return false;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimpleData* crpmParameterizedMotionSimpleData::AllocateAndLoad(const char* filename, crClipLoader* clipLoader, crAnimLoader* animLoader)
{
	crpmParameterizedMotionSimpleData* data = rage_new crpmParameterizedMotionSimpleData;
	if(data->Load(filename, clipLoader, animLoader))
	{
		return data;
	}
	else
	{
		delete data;
		return NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterizedMotionSimpleData::Save(const char* filename) const
{
	fiStream* f = ASSET.Create(filename, "spm", false);
	if(!f)
	{
		Errorf("crpmParameterizedMotionSimpleData - failed to create file '%s'", filename);
		return false;
	}

	int magic = 'DSMP';
	f->WriteInt(&magic, 1);

	const bool binary = true;
	fiSerialize s(f, false, binary);

	u8 version = sm_FileVersions[0];
	s << datLabel("PmVersion:") << version << datNewLine;

	s_PmSimpleSerializeVersion = version;

	if(fiSerializeTo(f, const_cast<crpmParameterizedMotionSimpleData&>(*this), binary))
	{
		f->Close();
		return true;
	}

	Errorf("crpmParameterizedMotionSimpleData - failed to save file '%s'", filename);
	f->Close();

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimpleData::Serialize(datSerialize& s)
{
	bool hasRcData = (m_RcData != NULL);
	if(s_PmSimpleSerializeVersion >= 5)
	{
		s << hasRcData;
	}
	else
	{
		hasRcData = true;
	}

	if(s.IsRead())
	{
		m_Clips = rage_new crClips;
		if(hasRcData)
		{
			m_RcData = rage_new crpmRegistrationCurveData;
		}
		m_BlendDatabase = rage_new crpmBlendDatabase;
	}

	bool pushFolder = PushParameterizedMotionFolder();
	m_Clips->Serialize(s, s_PmSimpleClipLoader, s_PmSimpleAnimLoader);
	if(pushFolder)
	{
		ASSET.PopFolder();
	}
	if(hasRcData)
	{
		if(s.IsRead() && s_PmSimpleSerializeVersion < 8)
		{
			float* rescale = Alloca(float, m_Clips->GetNumClips());
			for(u32 i=0; i<m_Clips->GetNumClips(); ++i)
			{
				rescale[i] = 1.f / m_Clips->GetClip(i)->GetDuration();
			}

			const_cast<crpmRegistrationCurveData&>(*m_RcData).Serialize(s, rescale);
		}
		else
		{
			s << const_cast<crpmRegistrationCurveData&>(*m_RcData);
		}
	}
	s << const_cast<crpmBlendDatabase&>(*m_BlendDatabase);

	if(s_PmSimpleSerializeVersion >= 4)
	{
		bool hasTags = (m_Tags!=NULL);
		s << hasTags;
		if(hasTags)
		{
			if(s.IsRead())
			{
				m_Tags = rage_new crTags;
			}
			s << *m_Tags;
		}

		bool hasProperties = (m_Properties!=NULL);
		s << hasProperties;
		if(hasProperties)
		{
			if(s.IsRead())
			{
				m_Properties = rage_new crProperties;
			}
			s << *m_Properties;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimpleData::Dump(crDumpOutput& output) const
{
	crpmParameterizedMotionData::Dump(output);

	output.Outputf(1, "hasproperties", "%d", int(m_Properties!=NULL));
	if(m_Properties)
	{
		m_Properties->Dump(output);
	}

	output.Outputf(1, "hastags", "%d", int(m_Tags!=NULL));
	if(m_Tags)
	{
		m_Tags->Dump(output);
	}

	if(m_Clips)
	{
		m_Clips->Dump(output);
	}

	if(m_RcData)
	{
		m_RcData->Dump(output);
	}

	if(m_BlendDatabase)
	{
		m_BlendDatabase->Dump(output);
	}
}

////////////////////////////////////////////////////////////////////////////////

const u8 crpmParameterizedMotionSimpleData::sm_FileVersions[] =
{
	10,  // 24-AUG-10 - new style properties/tags
	9, // 29-OCT-09 - inverted time alignment series
	8, // 15-OCT-09 - converted registration time alignment from time -> phase values
	7, // 02-OCT-09 - added variation field to blend database entries
	6, // 29-MAY-09 - added sort dimension to blend database
	5, // 21-MAY-09 - made rcData optional
	4, // 280109 - added tags and properties
	3, // 080109 - added default parameter to parameterization
	2, // 281008 - packed blend database weights, removed spatial alignments from registration curves
	1, // 280708 - first version
	0,
};

////////////////////////////////////////////////////////////////////////////////

__THREAD int s_PmSimpleSerializeVersion = 0;
__THREAD crClipLoader* s_PmSimpleClipLoader = NULL;
__THREAD crAnimLoader* s_PmSimpleAnimLoader = NULL;
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimple::crpmParameterizedMotionSimple(eParameterizedMotionType pmType)
: crpmParameterizedMotion(pmType)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimple::crpmParameterizedMotionSimple()
: crpmParameterizedMotion(kPmTypeSimple)
{	
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimple::crpmParameterizedMotionSimple(int worstNumClips, int worstNumParameterDimensions)
: crpmParameterizedMotion(kPmTypeSimple)
{
	PreInit(worstNumClips, worstNumParameterDimensions);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimple::crpmParameterizedMotionSimple(const crpmParameterizedMotionSimpleData& pmSimpleData)
: crpmParameterizedMotion(kPmTypeSimple)
{
	Init(pmSimpleData);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionSimple::~crpmParameterizedMotionSimple()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::PreInit(int worstNumClips, int worstNumParameterDimensions)
{
	InternalPreInit(worstNumParameterDimensions);

	m_Player.PreInit(worstNumClips);

	m_Weights.PreInit(worstNumClips);

	m_ParameterMin.PreInit(worstNumParameterDimensions);
	m_ParameterMax.PreInit(worstNumParameterDimensions);

	m_Interpolator.PreInit(worstNumParameterDimensions);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::Init(const crpmParameterizedMotionSimpleData& pmSimpleData)
{
	InternalInit(pmSimpleData);

	m_Weights.Init(GetSimpleData()->GetClips()->GetNumClips());
	m_Weights.SetWeight(GetSimpleData()->GetRegistrationCurveData()->GetReferenceClipIndex(), 1.f);

	m_Player.Init(*GetSimpleData()->GetRegistrationCurveData(), *GetSimpleData()->GetClips(), m_Weights);

	m_Interpolator.Init(GetSimpleData()->GetBlendDatabase()->GetParameterization().GetDefaultParameter(), &GetSimpleData()->GetBlendDatabase()->GetParameterization());

	GetParameterBoundaries(m_ParameterMin, m_ParameterMax);

	CalcWeights(m_Weights);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::Clear()
{
	m_Player.Clear();

	m_Weights.Clear();

	m_ParameterMin.Clear();
	m_ParameterMax.Clear();

	m_Interpolator.Clear();

	crpmParameterizedMotion::Clear();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::Shutdown()
{

	crpmParameterizedMotion::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::Update(float rcDeltaTime, float parameterDeltaTime)
{
	if(!m_Interpolator.IsFinished())
	{
		m_Interpolator.Update(parameterDeltaTime);

		CalcWeights(m_Weights);
	}

	m_Player.Update(rcDeltaTime, m_Weights);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::Composite(crFrameCompositor& inoutCompositor)
{
	m_Player.Composite(inoutCompositor);
}

////////////////////////////////////////////////////////////////////////////////

#if !USE_PM_COMPOSITOR
void crpmParameterizedMotionSimple::Composite(crFrame& outFrame)
{
	m_Player.Composite(outFrame);
}
#endif // !USE_PM_COMPOSITOR

////////////////////////////////////////////////////////////////////////////////

const crpmParameter& crpmParameterizedMotionSimple::GetParameter() const
{
	return m_Interpolator.GetCurrent();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::SetParameter(const crpmParameter& parameter, bool force)
{
	m_TempParameter.Set(parameter);
	m_TempParameter.Clamp(m_ParameterMin, m_ParameterMax);

	if(force)
	{
		m_Interpolator.Reset(m_TempParameter);

		CalcWeights(m_Weights);
	}
	else
	{
		m_Interpolator.SetTarget(m_TempParameter);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::SetParameterValue(int dim, float value, bool force)
{
	m_TempParameter.Set(m_Interpolator.GetTarget());
	m_TempParameter.SetValue(dim, value);

	SetParameter(m_TempParameter, force);
}

////////////////////////////////////////////////////////////////////////////////

const crpmParameterization& crpmParameterizedMotionSimple::GetParameterization() const
{
	return GetSimpleData()->GetBlendDatabase()->GetParameterization();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::GetParameterLimits(crpmParameter& outMinLimit, crpmParameter& outMaxLimit) const
{
	outMinLimit.Init(m_ParameterMin);
	outMaxLimit.Init(m_ParameterMax);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::SetParameterLimits(const crpmParameter& minLimit, const crpmParameter& maxLimit)
{
	m_ParameterMin.Set(minLimit);
	m_ParameterMax.Set(maxLimit);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::GetParameterBoundaries(crpmParameter& outMinBoundary, crpmParameter& outMaxBoundary) const
{
	GetSimpleData()->GetBlendDatabase()->GetParameterBoundaries(outMinBoundary, outMaxBoundary);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::GetParameterRates(crpmParameter& outRates) const
{
	outRates.Set(m_Interpolator.GetRate());
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::SetParameterRates(const crpmParameter& rates)
{
	m_Interpolator.SetRate(rates);
}

////////////////////////////////////////////////////////////////////////////////

float crpmParameterizedMotionSimple::GetU() const
{
	return m_Player.GetU();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::SetU(float u)
{
	m_Player.SetU(u, m_Weights);
}

////////////////////////////////////////////////////////////////////////////////

float crpmParameterizedMotionSimple::GetDeltaU() const
{
	return m_Player.GetDeltaU();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::SetDeltaU(float deltaU)
{
	m_Player.SetDeltaU(deltaU);
}

////////////////////////////////////////////////////////////////////////////////

float crpmParameterizedMotionSimple::GetPostDeltaU() const
{
	return m_Player.GetPostDeltaU();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::SetPostDeltaU(float deltaU)
{
	m_Player.SetPostDeltaU(deltaU);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterizedMotionSimple::IsLooped() const
{
	return m_Player.IsLooped();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::SetLooped(bool looped)
{
	m_Player.SetLooped(looped);
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterizedMotionSimple::IsFinished() const
{
	return m_Player.IsFinished();
}

////////////////////////////////////////////////////////////////////////////////

float crpmParameterizedMotionSimple::GetRemainingTime() const
{
	return m_Player.GetRemainingTime();
}

////////////////////////////////////////////////////////////////////////////////

float crpmParameterizedMotionSimple::GetDuration() const
{
	return m_Player.GetDuration();
}

////////////////////////////////////////////////////////////////////////////////

float crpmParameterizedMotionSimple::GetMaxDuration() const
{
	return m_Player.GetMaxDuration();
}

////////////////////////////////////////////////////////////////////////////////

const crTags* crpmParameterizedMotionSimple::GetTags() const
{
	return GetSimpleData()?GetSimpleData()->GetTags():NULL;
}

////////////////////////////////////////////////////////////////////////////////

const crProperties* crpmParameterizedMotionSimple::GetProperties() const
{
	return GetSimpleData()?GetSimpleData()->GetProperties():NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionSimple::CalcWeights(crpmWeights& outWeights) const
{
	if(!GetSimpleData()->GetBlendDatabase()->MapParameterToWeights(m_Interpolator.GetCurrent(), 0, outWeights))
	{
		// TODO --- very bad fall back
		outWeights.ClearWeights();
		outWeights.SetWeight(GetSimpleData()->GetRegistrationCurveData()->GetReferenceClipIndex(), 1.f);
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

