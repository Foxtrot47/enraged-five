//
// crparameterizedmotion/parameterization.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRPARAMETERIZEDMOTION_PARAMETERIZATION_H
#define CRPARAMETERIZEDMOTION_PARAMETERIZATION_H

#include "parameter.h"

#include "atl/array.h"
#include "cranimation/animation_config.h"

namespace rage
{

class crDumpOutput;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Describes parameterization space, individual dimensions
class crpmParameterization
{
public:

	// PURPOSE: Default constructor
	crpmParameterization();

	// PURPOSE: Resource constructor
	crpmParameterization(datResource&);

	// PURPOSE: Initializing constructor
	// PARAMS:
	// numDimensions - number of dimensions in parameter being described
	crpmParameterization(int numDimensions);

	// PURPOSE: Destructor
	~crpmParameterization();

	// PURPOSE: Placement
	DECLARE_PLACE(crpmParameterization);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initializer
	// PARAMS:
	// numDimensions - number of dimensions in parameter being described
	void Init(int numDimensions);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Get number of dimensions in parameter being described
	int GetNumDimensions() const;

//	int GetNumContinuousDimensions() const;
//	int GetTimeDimensionIndex() const;

	// PURPOSE: Get default parameter values
	// RETURNS: default parameter values for each dimension
	const crpmParameter& GetDefaultParameter() const;

	// PURPOSE: Get default parameter values (non const)
	// RETURNS: default parameter values for each dimension
	crpmParameter& GetDefaultParameter();

	// PURPOSE: Get space scale content ("volume" across arbitrary dimensions)
	float CalcSpaceScaleContent() const;

	// PURPOSE: Describes an individual dimension in the parameter
	class DimensionInfo
	{
	public:

		// PURPOSE: Default constructor
		DimensionInfo();

		// PURPOSE: Resource constructor
		DimensionInfo(datResource&);

		// PURPOSE: Destructor
		~DimensionInfo();

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(DimensionInfo);

		// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT


		// PURPOSE: Dimension unit type enumeration
		enum eDimensionType
		{
			kDimensionTypeNone,
			kDimensionTypeScalar,
			kDimensionTypeBinary,
			kDimensionTypeRadians,
			kDimensionTypeRadiansMinusPiToPi,
			kDimensionTypeDegreesMinus180To180,
			kDimensionTypeDegreesMinus270To90,
			// TODO other degree/radian angle ranges, time?
		};

		// PURPOSE: Set dimension type
		void SetDimensionType(eDimensionType dimensionType);

		// PURPOSE: Get dimension type
		eDimensionType GetDimensionType() const;

		// PURPOSE: Dimension meaning enumeration
		enum eDimensionMeaning
		{
			kDimensionMeaningNone,
			kDimensionMeaningUnknown,
			kDimensionMeaningSpatialX,
			kDimensionMeaningSpatialY,
			kDimensionMeaningSpatialZ,
			kDimensionMeaningPitch,
			kDimensionMeaningYaw,
			kDimensionMeaningRoll,
			kDimensionMeaningSpeed,
			kDimensionMeaningHeading,
			kDimensionMeaningGradient,
			kDimensionMeaningBank,
			kDimensionMeaningIntensity,			
		};
		
		// PURPOSE: Set dimension type
		void SetDimensionMeaning(eDimensionMeaning dimensionMeaning);

		// PURPOSE: Get dimension type
		eDimensionMeaning GetDimensionMeaning() const;

		// PURPOSE: Set dimension space scale
		// NOTES: optionally bias fidelity in particular parameter dimension
		void SetSpaceScale(float scale);

		// PURPOSE: Get dimension space scale
		// NOTES: optionally bias fidelity in particular parameter dimension
		float GetSpaceScale() const;


		// PURPOSE: Interpolate between two values within dimension
		// PARAMS:
		// t - interpolation weight [0..1]
		// f0, f1 - values to interpolate
		// RETURNS: interpolation result
		// NOTES: Uses information about dimension to perform more intelligent interpolation
		float Lerp(float t, float f0, float f1) const;

		// PURPOSE: Approach value within dimension, with limiting maximum change
		// f0, f1 - current and target values
		// maxChange - change limit
		// RETURNS: approach result
		// NOTES: Uses information about dimension to perform more intelligent approach
		float Approach(float f0, float f1, float maxChange) const;
			
		// PURPOSE: Near equality test
		// f0, f1 - two values to compare
		// tolerance - max error
		// RETURNS: true - within tolerance, false - beyond tolerance
		// NOTES: Uses information about dimension to perform more intelligent approach
		bool IsClose(float f0, float f1, float tolerance) const;

#if CR_DEV
		// PURPOSE: Serialize
		void Serialize(datSerialize&);
#endif // CR_DEV

	private:
		eDimensionType m_DimensionType;
		eDimensionMeaning m_DimensionMeaning;
		float m_SpaceScale;
	};

	// PURPOSE: Get information about dimension, by dimension index (const)
	const DimensionInfo& GetDimensionInfo(int idx) const;

	// PURPOSE: Get information about dimension, by dimension index (non-const)
	DimensionInfo& GetDimensionInfo(int idx);

	// PURPOSE: Append dimension info
	DimensionInfo& AppendDimensionInfo();

#if CR_DEV
	// PURPOSE: Serialize
	void Serialize(datSerialize&);

	// PURPOSE: Dump output
	void Dump(crDumpOutput&) const;
#endif // CR_DEV

private:

	atArray<DimensionInfo> m_Dimensions;

	crpmParameter m_DefaultParameter;
};

////////////////////////////////////////////////////////////////////////////////

inline int crpmParameterization::GetNumDimensions() const
{
	return m_Dimensions.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameter& crpmParameterization::GetDefaultParameter() const
{
	return m_DefaultParameter;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameter& crpmParameterization::GetDefaultParameter()
{
	return m_DefaultParameter;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmParameterization::CalcSpaceScaleContent() const
{
	float content = 1.f;

	const int numDimensions = m_Dimensions.GetCount();
	for(int i=0; i<numDimensions; ++i)
	{
		content *= m_Dimensions[i].GetSpaceScale();
	}

	return 1.f;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterization::DimensionInfo::DimensionInfo()
: m_DimensionType(kDimensionTypeScalar)
, m_DimensionMeaning(kDimensionMeaningUnknown)
, m_SpaceScale(1.f)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterization::DimensionInfo::~DimensionInfo()
{
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterization::DimensionInfo::SetDimensionType(eDimensionType dimensionType)
{
	m_DimensionType = dimensionType;	
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterization::DimensionInfo::eDimensionType crpmParameterization::DimensionInfo::GetDimensionType() const
{
	return m_DimensionType;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterization::DimensionInfo::SetDimensionMeaning(eDimensionMeaning dimensionMeaning)
{
	m_DimensionMeaning = dimensionMeaning;	
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterization::DimensionInfo::eDimensionMeaning crpmParameterization::DimensionInfo::GetDimensionMeaning() const
{
	return m_DimensionMeaning;
}

////////////////////////////////////////////////////////////////////////////////

inline void crpmParameterization::DimensionInfo::SetSpaceScale(float scale)
{
	m_SpaceScale = scale;
}

////////////////////////////////////////////////////////////////////////////////

inline float crpmParameterization::DimensionInfo::GetSpaceScale() const
{
	return m_SpaceScale;
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterization::DimensionInfo& crpmParameterization::GetDimensionInfo(int idx) const
{
	return m_Dimensions[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterization::DimensionInfo& crpmParameterization::GetDimensionInfo(int idx)
{
	return m_Dimensions[idx];
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


#endif // CRPARAMETERIZEDMOTION_PARAMETERIZATION_H
