//
// crparameterizedmotion/parameterizedmotionvariable.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "parameterizedmotionvariable.h"

#include "registrationcurve.h"

#include "atl/array_struct.h"
#include "atl/map_struct.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariableData::crpmParameterizedMotionVariableData()
: crpmParameterizedMotionSimpleData(kPmDataTypeVariable)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariableData::crpmParameterizedMotionVariableData(datResource& rsc)
: crpmParameterizedMotionSimpleData(rsc)
, m_Variations(rsc, true)
, m_VariationsMap(rsc, NULL, &FixupDataFunc)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariableData::crpmParameterizedMotionVariableData(const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags, const crProperties* properties)
: crpmParameterizedMotionSimpleData(kPmDataTypeSimple)
{
	Init(clips, rcData, bd, tags, properties);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariableData::~crpmParameterizedMotionVariableData()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crpmParameterizedMotionVariableData);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmParameterizedMotionVariableData::DeclareStruct(datTypeStruct& s)
{
	crpmParameterizedMotionSimpleData::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(crpmParameterizedMotionVariableData, crpmParameterizedMotionSimpleData)
	SSTRUCT_FIELD(crpmParameterizedMotionVariableData, m_Variations)
	SSTRUCT_FIELD(crpmParameterizedMotionVariableData, m_VariationsMap)
	SSTRUCT_END(crpmParameterizedMotionVariableData)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariableData::Init(const crClips& clips, const crpmRegistrationCurveData* rcData, const crpmBlendDatabase& bd, const crTags* tags, const crProperties* properties)
{
	crpmParameterizedMotionSimpleData::Init(clips, rcData, bd, tags, properties);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariableData::Shutdown()
{
	const int numVariations = m_Variations.GetCount();
	for(int i=0; i<numVariations; ++i)
	{
		m_Variations[i].Shutdown();
	}
	m_Variations.Reset();
	m_VariationsMap.Kill();

	crpmParameterizedMotionSimpleData::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
bool crpmParameterizedMotionVariableData::Load(const char* filename, crClipLoader* clipLoader, crAnimLoader* animLoader)
{
	s_PmSimpleClipLoader = clipLoader;
	s_PmSimpleAnimLoader = animLoader;

	fiStream* f = ASSET.Open(filename, "vpm", false, true);
	if(!f)
	{
		Errorf("crpmParameterizedMotionVariableData::Load - failed to open file '%s' for reading", filename);
		return false;
	}

	int magic = 0;
	f->ReadInt(&magic, 1);

	if(magic != 'DVMP')
	{
		Errorf("crpmParameterizedMotionVaraibleData::Load - file is not a variable pm file");
		f->Close();
		return false;
	}

	fiSerialize s(f, true, false);

	u8 version = 0;
	s << version;
	
	const u8 latestVersion = sm_FileVersions[0];
	if(version > latestVersion)
	{
		Errorf("crpmParameterizedMotionVaraibleData::Load - attempting to load incompatiable variable pm file version '%d' (only up to '%d' supported)", version, latestVersion);
		return false;
	}

	s_PmVariableSerializeVersion = version;

	m_Name = filename;
	if(fiSerializeFrom(f, *this))
	{
		f->Close();
		return true;
	}

	Errorf("crpmParameterizedMotionVariableData::Load - failed to load file '%s'", filename);
	f->Close();

	return false;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariableData* crpmParameterizedMotionVariableData::AllocateAndLoad(const char* filename, crClipLoader* clipLoader, crAnimLoader* animLoader)
{
	crpmParameterizedMotionVariableData* data = rage_new crpmParameterizedMotionVariableData;
	if(data->Load(filename, clipLoader, animLoader))
	{
		return data;
	}
	else
	{
		delete data;
		return NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterizedMotionVariableData::Save(const char* filename) const
{
	fiStream* f = ASSET.Create(filename, "vpm", false);
	if(!f)
	{
		Errorf("crpmParameterizedMotionVariableData - failed to create file '%s'", filename);
		return false;
	}

	int magic = 'DVMP';
	f->WriteInt(&magic, 1);

	const bool binary = true;
	fiSerialize s(f, false, binary);

	u8 version = sm_FileVersions[0];
	s << datLabel("VariablePmVersion:") << version << datNewLine;

	s_PmVariableSerializeVersion = version;

	if(fiSerializeTo(f, const_cast<crpmParameterizedMotionVariableData&>(*this), binary))
	{
		f->Close();
		return true;
	}

	Errorf("crpmParameterizedMotionVariableData - failed to save file '%s'", filename);
	f->Close();

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariableData::Serialize(datSerialize& s)
{
	int numVariations = m_Variations.GetCount();
	s << datLabel("NumVariations:") << numVariations << datNewLine;

	if(s.IsRead())
	{
		m_Variations.Resize(numVariations);
	}
	for(int i=0; i<numVariations; ++i)
	{
		m_Variations[i].m_Index = i;
		m_Variations[i].Serialize(s, m_Variations);
	}

	if(s.IsRead())
	{
		m_VariationsMap.Create(u16(numVariations));
		for(int i=1; i<numVariations; ++i)
		{
			m_VariationsMap.Insert(CalcVariationKey(m_Variations[i].m_Name), &m_Variations[i]);
		}
	}

	u8 versionSimple = crpmParameterizedMotionSimpleData::sm_FileVersions[0];
	s << datLabel("SimplePmVersion:") << versionSimple << datNewLine;

	s_PmSimpleSerializeVersion = versionSimple;

	crpmParameterizedMotionSimpleData::Serialize(s);

	s_PmSimpleSerializeVersion = 0;
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariableData::Dump(crDumpOutput& output) const
{
	crpmParameterizedMotionData::Dump(output);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

int crpmParameterizedMotionVariableData::AddVariation(const char* name, int numOptions, int parentVariation, int parentOption)
{
	int nv = m_Variations.GetCount();

	Variation& v = m_Variations.Grow();
	v.m_Index = nv;
	v.m_Name = name;
	v.m_Options.Resize(numOptions);

	if(nv > 0)
	{
		Assert(name);
		m_VariationsMap.Insert(CalcVariationKey(name), &v);
	}

	if(parentVariation >= 0)
	{
		Variation& pv = m_Variations[parentVariation];
		Variation::Option& pvo = pv.m_Options[parentOption];
		pvo.m_Variations.Grow() = &v;
	}

	return nv-1;
}

////////////////////////////////////////////////////////////////////////////////

bool crpmParameterizedMotionVariableData::AddVariationClip(int clipIdx, int variation, int option)
{
	Variation& v = m_Variations[variation+1];
	Variation::Option& vo = v.m_Options[option];

	vo.m_Clips.Grow() = clipIdx;

	return vo.m_Clips.GetCount() > 0;
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariableData::Variation::Variation()
: m_Index(-1)
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariableData::Variation::Variation(datResource& rsc)
: m_Name(rsc)
, m_Options(rsc, true)
{	
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmParameterizedMotionVariableData::Variation::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmParameterizedMotionVariableData::Variation)
	SSTRUCT_FIELD(crpmParameterizedMotionVariableData::Variation, m_Name)
	SSTRUCT_FIELD(crpmParameterizedMotionVariableData::Variation, m_Index)
	SSTRUCT_FIELD(crpmParameterizedMotionVariableData::Variation, m_Options)
	SSTRUCT_END(crpmParameterizedMotionVariableData::Variation)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariableData::Variation::Shutdown()
{
	const int numOptions = m_Options.GetCount();
	for(int i=0; i<numOptions; ++i)
	{
		m_Options[i].Shutdown();
	}
	m_Options.Reset();
	m_Name.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariableData::Variation::Serialize(datSerialize& s, atArray<Variation>& variations)
{
	s << m_Name;
	s << m_Index;

	int numOptions = m_Options.GetCount();
	s << numOptions;
	if(s.IsRead())
	{
		m_Options.Resize(numOptions);
	}
	for(int i=0; i<numOptions; ++i)
	{
		m_Options[i].Serialize(s, variations);
	}
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariableData::Variation::Option::Option()
{
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariableData::Variation::Option::Option(datResource& rsc)
: m_Clips(rsc)
, m_Variations(rsc, true)
{	
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crpmParameterizedMotionVariableData::Variation::Option::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crpmParameterizedMotionVariableData::Variation::Option)
	SSTRUCT_FIELD(crpmParameterizedMotionVariableData::Variation::Option, m_Clips)
	SSTRUCT_FIELD(crpmParameterizedMotionVariableData::Variation::Option, m_Variations)
	SSTRUCT_END(crpmParameterizedMotionVariableData::Variation::Option)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariableData::Variation::Option::Shutdown()
{
	m_Clips.Reset();
	m_Variations.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariableData::Variation::Option::Serialize(datSerialize& s, atArray<Variation>& variations)
{
	int numClips = m_Clips.GetCount();
	s << numClips;
	if(s.IsRead())
	{
		m_Clips.Resize(numClips);
	}
	for(int i=0; i<numClips; ++i)
	{
		s << m_Clips[i];
	}

	int numVariations = m_Variations.GetCount();
	s << numVariations;
	if(s.IsRead())
	{
		m_Variations.Resize(numVariations);
	}
	for(int i=0; i<numVariations; ++i)
	{
		int variation = -1;
		if(!s.IsRead())
		{
			variation = variations[i].m_Index;
		}
		s << variation;
		if(s.IsRead())
		{
			m_Variations[i] = &(variations[variation]);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariableData::FixupDataFunc(datResource &rsc, datRef<crpmParameterizedMotionVariableData::Variation>& data)
{
	data.Place(&data, rsc);
}

////////////////////////////////////////////////////////////////////////////////

const u8 crpmParameterizedMotionVariableData::sm_FileVersions[] =
{
	2, // 24-AUG-10 - new style properties/tags
	1, // 30-SEP-09 - first version
	0,
};

////////////////////////////////////////////////////////////////////////////////

__THREAD int s_PmVariableSerializeVersion = 0;

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariable::crpmParameterizedMotionVariable()
: crpmParameterizedMotionSimple(kPmTypeVariable)
{	
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariable::crpmParameterizedMotionVariable(int worstNumClips, int worstNumParameterDimensions, int worstNumVariations)
: crpmParameterizedMotionSimple(kPmTypeVariable)
{
	PreInit(worstNumClips, worstNumParameterDimensions, worstNumVariations);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariable::crpmParameterizedMotionVariable(const crpmParameterizedMotionVariableData& pmVariableData)
: crpmParameterizedMotionSimple(kPmTypeVariable)
{
	Init(pmVariableData);
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotionVariable::~crpmParameterizedMotionVariable()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariable::PreInit(int worstNumClips, int worstNumParameterDimensions, int worstNumVariations)
{
	crpmParameterizedMotionSimple::PreInit(worstNumClips, worstNumParameterDimensions);

	m_VariationOptions.Reserve(worstNumVariations);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariable::Init(const crpmParameterizedMotionVariableData& pmVariableData)
{
	crpmParameterizedMotionSimple::Init(pmVariableData);

	m_VariationOptions.Resize(pmVariableData.GetNumVariations());

	const int numVariationOptions = m_VariationOptions.GetCount();
	for(int i=0; i<numVariationOptions; ++i)
	{
		m_VariationOptions[i] = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariable::Clear()
{
	const int numVariationOptions = m_VariationOptions.GetCount();
	for(int i=0; i<numVariationOptions; ++i)
	{
		m_VariationOptions[i] = 0;
	}

	crpmParameterizedMotionSimple::Clear();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariable::Shutdown()
{
	m_VariationOptions.Reset();

	crpmParameterizedMotionSimple::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariable::Update(float rcDeltaTime, float parameterDeltaTime)
{
	crpmVariations oldVariations = m_Variations;
	CalcVariations(m_Variations);

	if(oldVariations != m_Variations)
	{
		CalcWeights(m_Weights);
	}

	crpmParameterizedMotionSimple::Update(rcDeltaTime, parameterDeltaTime);
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariable::CalcVariations(crpmVariations& outVariations) const
{
	outVariations = 0;

	const crpmParameterizedMotionVariableData* pmvd = GetVariableData();
	int shift = 0;

	const int numVariations = pmvd->GetNumVariations();
	for(int i=0; i<numVariations; ++i)
	{
		outVariations |= (1<<(m_VariationOptions[i]+shift));

		int numOptions = pmvd->GetNumOptions(i);
		shift += numOptions;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crpmParameterizedMotionVariable::CalcWeights(crpmWeights& outWeights) const
{
	if(!GetSimpleData()->GetBlendDatabase()->MapParameterToWeights(m_Interpolator.GetCurrent(), m_Variations, outWeights))
	{
		// TODO --- very bad fall back
		outWeights.ClearWeights();
		outWeights.SetWeight(GetSimpleData()->GetRegistrationCurveData()->GetReferenceClipIndex(), 1.f);
	}
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage
