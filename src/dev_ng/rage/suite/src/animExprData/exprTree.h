// /exprTree.h

#ifndef _EXPR_TREE_H_
#define _EXPR_TREE_H_

#include "parser/manager.h"

#include <string>

using namespace std;

namespace rage {

//-----------------------------------------------------------------------------

struct AeDrivenControllerInput;

//-----------------------------------------------------------------------------

class ExprNodeBase
{
public:
	enum eNodeSuperType
	{
		kNone = 0,
		kUnary,
		kBinary,
		kTernary,
		kOperand
	};

public:
	ExprNodeBase() 
		: m_pParent(NULL)
		{};

	virtual ~ExprNodeBase() {};

	virtual eNodeSuperType	GetSuperType() const { return kNone; }

	ExprNodeBase*			GetParent() const { return m_pParent; }
	void					SetParent(ExprNodeBase* pParent) { m_pParent = pParent; }

	virtual unsigned int	GetChildCount() const { return 0; }
	virtual ExprNodeBase*	GetChild(unsigned int) const { return NULL; }

	PAR_PARSABLE;

private:
	ExprNodeBase*	m_pParent;
};

//-----------------------------------------------------------------------------

class ExprNodeTernaryOperator : public ExprNodeBase
{
public:
	enum eTernaryOpType
	{
		kNone = 0,
		kIfThenElse,
		kClamp
	};

public:
	ExprNodeTernaryOperator()
		: m_opType(kNone)
		, m_pLeft(NULL)
		, m_pMiddle(NULL)
		, m_pRight(NULL)
	{};

	ExprNodeTernaryOperator(eTernaryOpType type)
		: m_opType(type)
		, m_pLeft(NULL)
		, m_pMiddle(NULL)
		, m_pRight(NULL)
	{};

	virtual ~ExprNodeTernaryOperator()
	{
		if(m_pLeft) { delete m_pLeft; m_pLeft = NULL; }
		if(m_pMiddle) { delete m_pMiddle; m_pMiddle = NULL; }
		if(m_pRight) { delete m_pRight; m_pRight = NULL; }
	}

	virtual ExprNodeBase::eNodeSuperType	GetSuperType() const { return ExprNodeBase::kTernary; }

	virtual unsigned int	GetChildCount() const { return 3; }
	virtual ExprNodeBase*	GetChild(unsigned int idx) const
	{
		Assert(idx < 3);
		if(idx == 0)
			return m_pLeft;
		else if(idx == 1)
			return m_pMiddle;
		else
			return m_pRight;
	}

	eTernaryOpType	GetOperatorType() const { return m_opType; }

	ExprNodeBase*	m_pLeft;
	ExprNodeBase*	m_pMiddle;
	ExprNodeBase*	m_pRight;

	PAR_PARSABLE;

private:
	eTernaryOpType	m_opType;
};

//-----------------------------------------------------------------------------

class ExprNodeBinaryOperator : public ExprNodeBase
{
public:
	enum eBinaryOpType
	{
		kNone = 0,
		kAssign,
		kAdd,
		kSubtract,
		kMultiply,
		kDivide,
		kGreaterThan,
		kGreaterThanEqualTo,
		kLessThan,
		kLessThanEqualTo,
		kEqualTo,
		kNotEqualTo,
		kIfThen,
		kMax,
		kMin,
		kMod,
		kPow
	};

public:
	ExprNodeBinaryOperator()
		: m_opType(kNone)
		, m_pLeft(NULL)
		, m_pRight(NULL)
	{};

	ExprNodeBinaryOperator(eBinaryOpType type) 
		: m_opType(type)
		, m_pLeft(NULL)
		, m_pRight(NULL)
	{};

	virtual ~ExprNodeBinaryOperator() 
	{
		if(m_pLeft) { delete m_pLeft; m_pLeft = NULL; }
		if(m_pRight) { delete m_pRight; m_pRight = NULL; }
	};

	virtual ExprNodeBase::eNodeSuperType	GetSuperType() const { return ExprNodeBase::kBinary; }

	virtual unsigned int	GetChildCount() const { return 2; }
	virtual ExprNodeBase*	GetChild(unsigned int idx) const
	{
		Assert(idx < 2);
		if(idx == 0)
			return m_pLeft;
		else
			return m_pRight;
	}

	eBinaryOpType	GetOperatorType() const { return m_opType; }

	ExprNodeBase*	m_pLeft;
	ExprNodeBase*	m_pRight;

	PAR_PARSABLE;

private:
	eBinaryOpType	m_opType;
};


//-----------------------------------------------------------------------------

class ExprNodeUnaryOperator : public ExprNodeBase
{
public:
	enum eUnaryOpType
	{
		kNone = 0,
		kNegate,
		kAbs,
		kAcos,
		kAsin,
		kATan,
		kCeil,
		kCos,
		kCosH,
		kDToR,
		kExp,
		kFloor,
		kLn,
		kLog,
		kRToD,
		kSin,
		kSinH,
		kSqrt,
		kTan,
		kTanH,
		kCosD,
		kSinD,
		kTanD,
		kAcosD,
		kAsinD,
		kAtanD
	};

	ExprNodeUnaryOperator()
		: m_opType(kNone)
		, m_pChild(NULL)
	{};

	ExprNodeUnaryOperator(eUnaryOpType type)
		: m_opType(type)
		, m_pChild(NULL)
	{};

	virtual ~ExprNodeUnaryOperator()
	{
		if(m_pChild) { delete m_pChild; m_pChild = NULL; }
	}

	virtual ExprNodeBase::eNodeSuperType	GetSuperType() const { return ExprNodeBase::kUnary; }

	virtual unsigned int	GetChildCount() const { return 1; }
	virtual ExprNodeBase*	GetChild(unsigned int ASSERT_ONLY(idx)) const { Assert(idx == 0); return m_pChild; }

	eUnaryOpType	GetOperatorType() const { return m_opType; }

	ExprNodeBase*	m_pChild;

	PAR_PARSABLE;

private:
	eUnaryOpType	m_opType;
};

//-----------------------------------------------------------------------------

class ExprNodeOperand : public ExprNodeBase
{
public:
	enum eOperandType
	{
		kNone = 0,
		kConstant,
		kNameAttrPair,
		kTime
	};

public:
	ExprNodeOperand() {};
	virtual ~ExprNodeOperand() {};

	virtual ExprNodeBase::eNodeSuperType	GetSuperType() const { return ExprNodeBase::kOperand; }
	virtual eOperandType					GetOperandType() const { return kNone; }	

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

class ExprNodeNameAttrPair : public ExprNodeOperand
{
public:
	ExprNodeNameAttrPair() {};

	ExprNodeNameAttrPair(const char* szName, const char* szAttr)
		: m_name(szName)
		, m_attr(szAttr)
		, m_pInput(NULL)
	{};

	virtual ~ExprNodeNameAttrPair() {};

	virtual ExprNodeOperand::eOperandType	GetOperandType() const { return ExprNodeOperand::kNameAttrPair; }

	atString	m_name;
	atString	m_attr;

	AeDrivenControllerInput* m_pInput;	//Not serialized, fixed up on load

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

class ExprNodeConstant : public ExprNodeOperand
{
public:
	ExprNodeConstant() {};

	ExprNodeConstant(float value) 
		: m_value(value)
		{};

	virtual ~ExprNodeConstant() {};

	virtual ExprNodeOperand::eOperandType	GetOperandType() const { return ExprNodeOperand::kConstant; }

	float	m_value;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

class ExprNodeTime : public ExprNodeOperand
{
public:
	enum eTimeUnits
	{
		kSeconds,
		kFrame30,
		kFrame60
	};

public:
	ExprNodeTime() 
		: m_units(kSeconds)
	{};
	
	ExprNodeTime(eTimeUnits units)
		: m_units(units)
	{};

	virtual ~ExprNodeTime() {};

	virtual ExprNodeOperand::eOperandType	GetOperandType() const { return ExprNodeOperand::kTime; }
	
	eTimeUnits m_units;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

class ExprTree
{
public:
	ExprTree() {};
	virtual ~ExprTree()
	{
		if(m_pRoot)
		{
			delete m_pRoot;
			m_pRoot = NULL;
		}
	}

	ExprNodeBase* m_pRoot;

	PAR_PARSABLE;

private:
	void	PostLoad();

	void	FixupParentPointers(ExprNodeBase* pNode);
};

//-----------------------------------------------------------------------------

} // End namespace rage

#endif //_EXPR_TREE_H_

