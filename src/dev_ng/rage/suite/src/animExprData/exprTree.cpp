// /exprTree.cpp

#include "exprTree.h"
#include "exprTree_parser.h"

namespace rage {

//-----------------------------------------------------------------------------

void ExprTree::FixupParentPointers(ExprNodeBase* pNode)
{
	unsigned int childCount = pNode->GetChildCount();
	for(unsigned int i=0; i < childCount; i++)
	{
		pNode->GetChild(i)->SetParent(pNode);
		FixupParentPointers(pNode->GetChild(i));
	}
}

//-----------------------------------------------------------------------------

void ExprTree::PostLoad()
{
	if(!m_pRoot)
		return;

	FixupParentPointers(m_pRoot);
}

//-----------------------------------------------------------------------------

}//End namespace rage

