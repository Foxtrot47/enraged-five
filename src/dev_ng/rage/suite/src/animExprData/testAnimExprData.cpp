// 
// /testAnimExprData.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "diag/output.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "parser/tree.h"
#include "system/main.h"
#include "system/param.h"

#include "animExprData/animExprData.h"

using namespace rage;

REQ_PARAM(file, "[testAnimExprData] Input animation expression xml file");

//-----------------------------------------------------------------------------

int TestAeFileLoad()
{
	AeFile	theFile;

	const char* szFile;
	PARAM_file.Get(szFile);

	if(!PARSER.LoadObject<AeFile>(szFile, "", theFile))
	{
		Errorf("Failed to load expression file '%s'", szFile);
		return 1;
	}

	return 0;
}

//-----------------------------------------------------------------------------

int Main()
{
	INIT_PARSER;
	
	int iRes= TestAeFileLoad();

	SHUTDOWN_PARSER;

	return iRes;
}

//-----------------------------------------------------------------------------

