// 
// animExprData/animExprData.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include <queue>
#include <sstream>

#include "parser/tree.h"
#include "parser/treenode.h"

#include "vector/vector3.h"
#include "vector/vector4.h"

#include "exprTree.h"
#include "animExprData.h"
#include "animExprData_parser.h"

using namespace std;

namespace rage {


//----------------------------------------------------------------------------

void AeControllerContainer::GetContainedControllers(vector<AeController*>& outControllers)
{
	outControllers.clear();
}

//----------------------------------------------------------------------------

void AeXYZController::GetContainedControllers(vector<AeController*>& outControllers)
{
	outControllers.clear();

	outControllers.push_back(m_pXController);
	outControllers.push_back(m_pYController);
	outControllers.push_back(m_pZController);
}

//----------------------------------------------------------------------------

void AeBlendController::GetContainedControllers(vector<AeController*>& outControllers)
{
	outControllers.clear();

	for (atArray<AeBlendControllerItem*>::iterator it = m_inputs.begin(); it != m_inputs.end(); ++it)
	{
		AeBlendControllerItem* pItem = (*it);
		outControllers.push_back(pItem->m_pController);
	}
}

//----------------------------------------------------------------------------

void AeLookAtController::GetContainedControllers(vector<AeController*>& outControllers)
{
	outControllers.clear();

	outControllers.push_back(m_pBlendController);
	outControllers.push_back(m_pBoneOSController);
	outControllers.push_back(m_pParentOSController);
	outControllers.push_back(m_pUpnodeOSController);
}

//-----------------------------------------------------------------------------

AeDriverComponent* AeDriver::FindComponentById(const atString& id)
{
	for(atArray< AeDriverComponent >::iterator cmpIt = m_components.begin();
		cmpIt != m_components.end();
		++cmpIt)
	{
		if((*cmpIt).m_id == id)
			return &(*cmpIt);
	}
	return NULL;
}

//-----------------------------------------------------------------------------

bool AeDrivenController::AddDriver(AeDriver* pDriver, AeDriverComponent* pDriverComponent, float unitConvFactor)
{
	if(!HasDriver(pDriver->m_id.c_str(), pDriverComponent->m_id.c_str()))
	{
		AeDrivenControllerInput* pInput = rage_new AeDrivenControllerInput();
		
		pInput->m_unitConversionFactor = unitConvFactor;
		pInput->m_pDriver = pDriver;
		pInput->m_pDriverComp = pDriverComponent;
		pInput->m_driverId = pDriver->m_id;
		pInput->m_driverCompId = pDriverComponent->m_id;

		m_inputs.PushAndGrow(pInput);

		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------

bool AeDrivenController::HasDriver(const char* szDriverId, const char* szDriverComponentId)
{
	for(atArray< AeDrivenControllerInput* >::iterator it = m_inputs.begin();
			it != m_inputs.end();
			++it)
		{
			AeDrivenControllerInput* pInput = (*it);
			if( (strcmpi(pInput->m_driverId.c_str(), szDriverId) == 0) &&
				(strcmpi(pInput->m_driverCompId.c_str(), szDriverComponentId) == 0) )
			{
				return true;
			}
		}
	return false;
}

//-----------------------------------------------------------------------------

AeFile::AeFile()
	: m_globalTranslateScaleFactor(1.0f)
{
}

//-----------------------------------------------------------------------------

 AeFile::~AeFile()
 {
	 for(atArray< AeDriver* >::iterator it = m_driverLibrary.begin();
		 it != m_driverLibrary.end();
		 ++it)
	 {
		 delete (*it);
	 }

	 for(atArray< AeController* >::iterator it = m_controllerLibrary.begin();
		 it != m_controllerLibrary.end();
		 ++it)
	 {
		 delete (*it);
	 }

	 for(atArray< AeBone* >::iterator it = m_boneLibrary.begin();
		 it != m_boneLibrary.end();
		 ++it)
	 {
		 delete (*it);
	 }

	 for(atArray< AeMesh* >::iterator it = m_meshLibrary.begin();
		 it != m_meshLibrary.end();
		 ++it)
	 {
		delete (*it);
	 }

	 for(atArray< AeMotion* >::iterator it = m_motionLibrary.begin();
		 it != m_motionLibrary.end();
		 ++it)
	 {
		 delete (*it);
	 }
 }

//-----------------------------------------------------------------------------

AeController* AeFile::FindControllerById(const atString& id)
{
	for(atArray< AeController* >::iterator ctrlIt = m_controllerLibrary.begin();
		ctrlIt != m_controllerLibrary.end();
		++ctrlIt)
	{
		if((*ctrlIt)->m_id == id)
			return (*ctrlIt);
	}
	return NULL;
}

//-----------------------------------------------------------------------------

AeDriver* AeFile::FindDriverById(const atString& id)
{
	for(atArray< AeDriver* >::iterator drvIt = m_driverLibrary.begin();
		drvIt != m_driverLibrary.end();
		++drvIt)
	{
		if((*drvIt)->m_id == id)
			return (*drvIt);
	}
	return NULL;
}

//-----------------------------------------------------------------------------

void AeFile::FixupExpressionControllerPtrs(AeExpressionController* pExprCtrl)
{
	//Fixup all the nameAttrPair node type driver pointers 
	queue<ExprNodeBase*> exprNodeQueue;

	exprNodeQueue.push(pExprCtrl->m_pExprTree->m_pRoot);

	while(!exprNodeQueue.empty())
	{
		ExprNodeBase* pNode = exprNodeQueue.front();
		exprNodeQueue.pop();

		//We only care about expression operands that are name/attr pairs.
		if(pNode->GetSuperType() == ExprNodeBase::kOperand)
		{
			ExprNodeOperand* pOperand = static_cast<ExprNodeOperand*>(pNode);
			if(pOperand->GetOperandType() == ExprNodeOperand::kNameAttrPair)
			{
				ExprNodeNameAttrPair* pNameAttrNode = static_cast<ExprNodeNameAttrPair*>(pOperand);
				
				//Look through the controller inputs and try to match up the input
				//referenced by the expression tree node
				AeDrivenControllerInput* pInput = NULL;

				for( unsigned int i = 0; i < pExprCtrl->GetInputCount(); i++)
				{
					AeDrivenControllerInput* pCurrent = pExprCtrl->GetInput(i);
					if( (pCurrent->m_driverId == pNameAttrNode->m_name) &&
						(pCurrent->m_driverCompId == pNameAttrNode->m_attr) )
					{
						pInput = pCurrent;
						break;
					}
				}

				if(!pInput)
				{
					//The only time we shouldn't find our input listed in the controllers input list is if this node
					//represents the output and an assignment operation
					ExprNodeBase* pParent = pNameAttrNode->GetParent();
					if(!pParent)
					{
						Quitf("Failed to locate driver input '%s.%s' referenced in the expression tree of controller '%s'",
							pNameAttrNode->m_name.c_str(), pNameAttrNode->m_attr.c_str(), pExprCtrl->m_id.c_str());
					}

					if(pParent->GetSuperType() == ExprNodeBase::kBinary)
					{
						ExprNodeBinaryOperator* pBinOperator = static_cast<ExprNodeBinaryOperator*>(pParent);
						if(pBinOperator->GetOperatorType() == ExprNodeBinaryOperator::kAssign)
						{
							if( pBinOperator->GetChild(0) != pNameAttrNode )
							{
								Errorf("Failed to locate driver input '%s.%s' referenced in the expression tree of controller '%s'",
									pNameAttrNode->m_name.c_str(), pNameAttrNode->m_attr.c_str(), pExprCtrl->m_id.c_str());
							}
						}
					}
				}
				
				pNameAttrNode->m_pInput = pInput;
			}
		}

		//Add the children to the traversal queue
		for(unsigned int i = 0; i < pNode->GetChildCount(); i++)
		{
			exprNodeQueue.push(pNode->GetChild(i));
		}
	}
}

//-----------------------------------------------------------------------------

void AeFile::FixupDrivenControllerPtrs(AeDrivenController* pDrvCtrl)
{
	for(atArray< AeDrivenControllerInput* >::iterator it = pDrvCtrl->m_inputs.begin();
		it != pDrvCtrl->m_inputs.end();
		++it)
	{
		AeDrivenControllerInput* pInput = (*it);

		AeDriver* pDriver = FindDriverById(pInput->m_driverId);
		Assert(pDriver);
		AeDriverComponent* pDriverCmp = pDriver->FindComponentById(pInput->m_driverCompId);
		Assert(pDriverCmp);

		pInput->m_pDriver = pDriver;
		pInput->m_pDriverComp = pDriverCmp;
	}

	AeExpressionController* pExpressionCtrl = dynamic_cast<AeExpressionController*>(pDrvCtrl);
	if(pExpressionCtrl)
	{
		FixupExpressionControllerPtrs(pExpressionCtrl);
		return;
	}
}

//-----------------------------------------------------------------------------

void AeFile::FixupControllerPtrs(AeController* pController)
{
	AeDrivenController* pDrvCtrl = dynamic_cast<AeDrivenController*>(pController);
	if(pDrvCtrl)
	{
		FixupDrivenControllerPtrs(pDrvCtrl);
		return;
	}

	AeControllerContainer* pControllerContainer = dynamic_cast<AeControllerContainer*>(pController);
	if (pControllerContainer)
	{
		vector<AeController*> controllers;
		pControllerContainer->GetContainedControllers(controllers);

		for (vector<AeController*>::iterator it = controllers.begin();
			 it != controllers.end(); ++it)
		{
			FixupControllerPtrs(*it);
		}

		return;
	}
}

//-----------------------------------------------------------------------------

void AeFile::PostLoad()
{
	//Fixup the bone transform component controller pointers
	for(atArray< AeBone* >::iterator boneIt = m_boneLibrary.begin();
		boneIt != m_boneLibrary.end();
		++boneIt)
	{
		AeBone* pBone = (*boneIt);
		for(atArray< AeBoneTransformComponent* >::iterator compIt = pBone->m_transform.begin();
			compIt != pBone->m_transform.end();
			++compIt)
		{
			AeController* pCtrl = FindControllerById((*compIt)->m_controllerId);
			Assertf(pCtrl, "Failed to find controller id '%s', referenced by component '%s' on bone '%s'", 
				(*compIt)->m_controllerId.c_str(), (*compIt)->m_id.c_str(), pBone->m_id.c_str());
			(*compIt)->m_pController = pCtrl;
		}
	}

	//Fixup the controller driver and driver component pointers
	for(atArray< AeController* >::iterator ctrlIt = m_controllerLibrary.begin();
		ctrlIt != m_controllerLibrary.end();
		++ctrlIt)
	{
		FixupControllerPtrs(*ctrlIt);
	}

	//Fixup the mesh item controllers
	for(atArray<AeMesh*>::iterator meshIt = m_meshLibrary.begin();
		meshIt != m_meshLibrary.end();
		++meshIt)
	{
		AeMesh* pMesh = (*meshIt);

		//Fixup the blend target controllers
		for(atArray< AeMeshBlendTarget* >::iterator blendTgtIt = pMesh->m_blendTargets.begin();
			blendTgtIt != pMesh->m_blendTargets.end();
			++blendTgtIt)
		{
			AeController* pCtrl = FindControllerById((*blendTgtIt)->m_controllerId);
			Assertf(pCtrl, "Failed to find controller id '%s', referenced by blend target '%s', of mesh '%s'",
				(*blendTgtIt)->m_controllerId.c_str(), (*blendTgtIt)->m_id.c_str(), pMesh->m_id.c_str());
			(*blendTgtIt)->m_pController = pCtrl;
		}

		//Fixup the animated normal map target controllers
		for(atArray< AeMeshAnimNormalMapMult* >::iterator animNrmMultIt = pMesh->m_animNrmMults.begin();
			animNrmMultIt != pMesh->m_animNrmMults.end();
			++animNrmMultIt)
		{
			AeController* pCtrl = FindControllerById((*animNrmMultIt)->m_controllerId);
			Assertf(pCtrl, "Failed to find controller id '%s', referenced by animated normal map multipler '%s', of mesh '%s'",
				(*animNrmMultIt)->m_controllerId.c_str(), (*animNrmMultIt)->m_id.c_str(), pMesh->m_id.c_str());
			(*animNrmMultIt)->m_pController = pCtrl;
		}
	}
}

//-----------------------------------------------------------------------------
//BEGIN TEST CODE.....
class AeDrivenElementDepNode
{
public:
	AeDrivenElementDepNode()
		: m_pParent(NULL)
		, m_pDrivenElement(NULL)
	{};

	~AeDrivenElementDepNode() {};

	string	m_nodeId;
	string	m_componentId;

	AeDrivenElement*					m_pDrivenElement;

	AeDrivenElementDepNode*				m_pParent;
	vector< AeDrivenElementDepNode*>	m_dependents;
};

//-----------------------------------------------------------------------------

int CollectDrivenControllerInputs(AeFile& theFile, AeController* pController, vector<AeDrivenControllerInput*>& retInputs)
{
	AeDrivenController* pDrivenController = dynamic_cast<AeDrivenController*>(pController);
	if(pDrivenController)
	{
		int nInputs = pDrivenController->GetInputCount();
		for(int inputIdx = 0; inputIdx < nInputs; inputIdx++)
		{
			AeDrivenControllerInput* pInput = pDrivenController->GetInput(inputIdx);
			retInputs.push_back(pInput);
		}
	}
	else
	{
		//It's not a driven controller, so it must be a blend controller
		AeBlendController* pBlendController = dynamic_cast<AeBlendController*>(pController);
		Assert(pBlendController);

		int nItems = pBlendController->GetBlendItemCount();
		for(int itemIdx = 0; itemIdx < nItems; itemIdx++)
		{
			AeBlendControllerItem* pItem = pBlendController->GetBlendItem(itemIdx);
			Assert(pItem);

			CollectDrivenControllerInputs(theFile, pItem->m_pController, retInputs);
		}
	}

	return (int)retInputs.size();
}

//-----------------------------------------------------------------------------

AeDrivenElementDepNode* FindDepNode(AeDrivenElementDepNode* pRoot, const char* szNodeId, const char* szComponentId)
{
	if( (strcmp(pRoot->m_nodeId.c_str(),szNodeId) == 0) &&
		(strcmp(pRoot->m_componentId.c_str(),szComponentId) == 0) )
	{
		return pRoot;
	}
	else
	{
		int nDeps = (int)pRoot->m_dependents.size();
		
		if(!nDeps)
		{
			return NULL;
		}
		else
		{
			for(int depIdx = 0; depIdx < nDeps; depIdx++)
			{
				AeDrivenElementDepNode* pNode = FindDepNode(pRoot->m_dependents[depIdx], szNodeId, szComponentId);
				if(pNode)
				{
					return pNode;
				}
			}
		}
	}

	return NULL;
}

//-----------------------------------------------------------------------------

void PrintDependencyGraph(AeDrivenElementDepNode* pRoot, int indent = 0)
{
	stringstream ss;

	for(int i=0; i<indent; i++)
		ss << "\t";

	ss << "Node : " << pRoot->m_nodeId << " Component : " << pRoot->m_componentId;

	Displayf("%s", ss.str().c_str());

	int nDeps = (int)pRoot->m_dependents.size();
	for(int i=0; i<nDeps; i++)
	{
		PrintDependencyGraph(pRoot->m_dependents[i], indent + 1);
	}
}

//-----------------------------------------------------------------------------

AeBoneTransformComponent* FindBoneTransformComponent(AeFile& theFile, const char* szBoneId, const char* szComponentId)
{
	int nBones = theFile.GetBoneCount();
	for(int boneIdx = 0; boneIdx < nBones; boneIdx++)
	{
		AeBone* pBone = theFile.GetBone(boneIdx);
		Assert(pBone);

		if(strcmp(pBone->m_id.c_str(), szBoneId) == 0)
		{
			int nTransformComponents = pBone->GetTransformComponentCount();
			for(int compIdx = 0; compIdx < nTransformComponents; compIdx++)
			{
				AeBoneTransformComponent* pComp = pBone->GetTransformComponent(compIdx);
				Assert(pComp);

				if(strcmp(pComp->m_id.c_str(), szComponentId) == 0)
				{
					return pComp;
				}
			}
		}
	}
	return NULL;
}

//-----------------------------------------------------------------------------

void AeFile::PrioritizeEvaluationOrdering()
{
	vector<AeDrivenElementDepNode*> masterDepNodeArray;

	AeDrivenElementDepNode* pRootDepNode = rage_new AeDrivenElementDepNode();
	pRootDepNode->m_nodeId = "depRoot";
	pRootDepNode->m_componentId = "depRootComp";
	masterDepNodeArray.push_back(pRootDepNode);
	
	int nBones = GetBoneCount();
	for(int boneIdx = 0; boneIdx < nBones; boneIdx++)
	{
		AeBone* pBone = GetBone(boneIdx);
		Assert(pBone);

		int nTransformComponents = pBone->GetTransformComponentCount();
		for(int compIdx = 0; compIdx < nTransformComponents; compIdx++)
		{
			AeBoneTransformComponent* pComp = pBone->GetTransformComponent(compIdx);
			Assert(pComp);

			const char* szBoneName = pBone->m_id.c_str();
			const char* szCompName = pComp->m_id.c_str();

			//Check to see if a dependency node has already been created for this driven component
			AeDrivenElementDepNode* pDrivenDepNode = FindDepNode(pRootDepNode, szBoneName, szCompName);
			if(!pDrivenDepNode)
			{
				pDrivenDepNode = rage_new AeDrivenElementDepNode();
				pDrivenDepNode->m_pDrivenElement = pComp;
				pDrivenDepNode->m_nodeId = szBoneName;
				pDrivenDepNode->m_componentId = szCompName;
				masterDepNodeArray.push_back(pDrivenDepNode);

				pRootDepNode->m_dependents.push_back(pDrivenDepNode); //TODO : I don't know if this is correct...
			}

			//Find all of the inputs this node depends on
			AeController* pController = FindControllerById(pComp->m_controllerId);
			Assert(pController);

			vector<AeDrivenControllerInput*> inputArray;
			int nInputs = CollectDrivenControllerInputs(*this, pController, inputArray);

			for(int inputIdx = 0; inputIdx < nInputs; inputIdx++)
			{
				AeDrivenControllerInput* pInput = inputArray[inputIdx];

				const char* szDriver = pInput->m_driverId.c_str();
				const char* szDriverCompId = pInput->m_driverCompId.c_str();

				//Determine if the "driver" is also being "driven" by the expression system
				AeBoneTransformComponent* pDriverComp = FindBoneTransformComponent(*this, szDriver, szDriverCompId);
				if(pDriverComp)
				{
					//See if there is already a dependency node
					AeDrivenElementDepNode* pDriverDepNode = FindDepNode(pRootDepNode, szDriver, szDriverCompId);
					if(pDriverDepNode)
					{
						pDrivenDepNode->m_dependents.push_back(pDriverDepNode);
					}
					else
					{
						pDriverDepNode = rage_new AeDrivenElementDepNode();
						pDriverDepNode->m_pDrivenElement = pDriverComp;
						pDriverDepNode->m_nodeId = szDriver;
						pDriverDepNode->m_componentId = szDriverCompId;
						masterDepNodeArray.push_back(pDriverDepNode);

						pDrivenDepNode->m_dependents.push_back(pDriverDepNode);
					}
				}
			}//End for(int inputIdx...
		}//End for(int compIdx...
	}//End for(int boneIdx...

	PrintDependencyGraph(pRootDepNode);

	for(vector<AeDrivenElementDepNode*>::iterator it = masterDepNodeArray.begin();
		it != masterDepNodeArray.end();
		++it)
	{
		delete (*it);
	}
}

//-----------------------------------------------------------------------------

} // namespace rage
