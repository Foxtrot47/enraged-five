// 
// animExprData/animExprData.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef ANIMEXPRDATA_ANIMEXPRDATA_H 
#define ANIMEXPRDATA_ANIMEXPRDATA_H 

#include <string>
#include <vector>

#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"
#include "parser/manager.h"
#include "exprTree.h"

using namespace std;

namespace rage {

class AeFile;

//-----------------------------------------------------------------------------

enum eComponentType
{
	kFloat = 0,
	kVector,
	kQuat
};

enum eComponentRotationOrder
{
	kXYZ = 0,
	kYZX,
	kZXY,
	kXZY,
	kYXZ,
	kZYX
};

enum eComponentAxis
{
	kXAxis = 0,
	kYAxis,
	kZAxis,
	kNegXAxis,
	kNegYAxis,
	kNegZAxis,
	kAxisNone,
	kAxisAll
};

enum eEvaluationSpace
{
	kDefault = 0,
	kZeroed,
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a particular property of a driver, e.g. the X translation component
//of a bone.
class AeDriverComponent
{
public:
	AeDriverComponent() 
		: m_trackId(-1)
		, m_type(kFloat)
		, m_axis(kAxisNone)
		, m_rotOrder(kXYZ)
		, m_evalSpace(kDefault)
	{}

	virtual ~AeDriverComponent() {}

	atString					m_id;			//Component id
	int							m_trackId;		//Animation track id.  Not sure this is necessary but may be useful for the runtime?
	eComponentType				m_type;			//Component type, float, vector, quat, etc..
	eComponentAxis				m_axis;			//Axis represented by this component
	eComponentRotationOrder		m_rotOrder;		//Rotation order for the component (if rotation type)
	eEvaluationSpace			m_evalSpace;	//Evaluation space of the driver (determines how the runtime pre/post tranforms the
												//joint when retrieving the value of a driver component).
	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE : Represents a "driver" in the expression system, which is an element
//which contains a list of components whose values feed into the expression system.
class AeDriver
{
public:
	AeDriver() 
		: m_boneId(-1)
	{}

	virtual ~AeDriver() {}

	AeDriverComponent* FindComponentById(const atString& id);

	atString	m_id;			//Driver id
	int			m_boneId;		//Bone id of the driver, this may need to be made more generic as it will refer to an
							//indentifier for any "input" value 

	atArray< AeDriverComponent >	m_components;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE : Base class of all controller types
class AeController
{
public:
	AeController() 
		: m_outputUnitConversionFactor(1.0f)
	{}

	virtual ~AeController() {}

	atString	m_id;							//Controller identifier
	float		m_outputUnitConversionFactor;	//Scale factor to be applied to the controllers output

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Helper structure to hold information about an input to a driven controller
struct AeDrivenControllerInput
{
	AeDrivenControllerInput()
		: m_unitConversionFactor(1.0f)
		, m_pDriver(NULL)
		, m_pDriverComp(NULL)
	{}

	virtual ~AeDrivenControllerInput() {}

	atString	m_driverId;						//Identifier of the driver
	atString	m_driverCompId;					//Identifier of the driver component that provides
												//the input to the controller

	float	m_unitConversionFactor;			//Unit conversion factor to apply to the incoming driver data

	AeDriver*				m_pDriver;		//Not Serialized - Fixup on post-load
	AeDriverComponent*		m_pDriverComp;	//Not Serialized - Fixup on post-load

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller which requires an input value from an 
//external driver
class AeDrivenController : public AeController
{
	friend class AeFile;

public:
	AeDrivenController() {}
	virtual ~AeDrivenController() 
	{
		for(atArray< AeDrivenControllerInput* >::iterator it = m_inputs.begin();
			it != m_inputs.end();
			++it)
		{
			delete (*it);
		}
	}

	unsigned int				GetInputCount() const { return (unsigned int)m_inputs.size(); }
	AeDrivenControllerInput*	GetInput(int idx) const { Assert(idx < m_inputs.size()); return m_inputs[idx]; }

	bool	AddDriver(AeDriver* pDriver, AeDriverComponent* pDriverComponent, float unitConvFactor = 1.0f);
	bool	HasDriver(const char* szDriverId, const char* szDriverComponentId);

	PAR_PARSABLE;

private:
	atArray< AeDrivenControllerInput* >	m_inputs;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller which acts as a container for sub-controllers
class AeControllerContainer : public AeController
{
public:
	AeControllerContainer() {}
	virtual ~AeControllerContainer() {}

	virtual void GetContainedControllers(vector<AeController*>& outControllers);

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller whose output is exactly a constant float value
class AeFloatConstantController : public AeController
{
public:
	AeFloatConstantController()
		: m_value(0.0f)
	{}

	virtual ~AeFloatConstantController() {}

	float	m_value;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a keyframe on a curve
class AeCurveKey
{
public:
	AeCurveKey() 
		: m_in(0.0f)
		, m_out(0.0f)
	{}

	virtual ~AeCurveKey() {}

	float	m_in;		//Input value
	float	m_out;		//Output value

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller whose output is defined by evaulating a linear
//function defined by its keys. 
// TODO : May need to expand this to support specifying key frame tangents and
//		  different interpolation types
class AeFloatCurveController : public AeDrivenController
{
public:
	AeFloatCurveController() {}
	virtual ~AeFloatCurveController() {}

	atArray< AeCurveKey >	m_keys;		//Array of key values
	
	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller whose output is defined by evaluating
//a scripted expression.  
// TODO : This controller will likely need to be revamped to accomodate a tree
//		  of controllers which will represent the expression (as opposed to
//		  the script string it currently contains)
class AeExpressionController : public AeDrivenController
{
public:
	AeExpressionController()
		: m_pExprTree(NULL)
	{}

	virtual ~AeExpressionController() 
	{
		delete m_pExprTree;
	}

	ExprTree*	m_pExprTree;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller whose output is driven by the object space
//translation or rotation of an object
class AeObjectSpaceController : public AeDrivenController
{
public:
	AeObjectSpaceController() {}
	virtual ~AeObjectSpaceController() {}

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller whose output is the same as the inputs.
//This essentially serves as a pass-through controller which doesn't modify
//any of the input values
class AeDirectController : public AeDrivenController
{
public:
	AeDirectController()
		: m_scale(1.0f)
	{}

	virtual ~AeDirectController() {}

	float	m_scale;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller whose output is defined by three sub-controllers
//for X, Y, and Z components
class AeXYZController : public AeControllerContainer
{
public:
	AeXYZController()
	:	m_type(kVector),
		m_pXController(NULL),
		m_pYController(NULL),
		m_pZController(NULL)
	{}

	virtual ~AeXYZController()
	{
		delete m_pXController;
		delete m_pYController;
		delete m_pZController;
	}

	void GetContainedControllers(vector<AeController*>& outControllers);

	eComponentType	m_type;
	AeController*	m_pXController;
	AeController*	m_pYController;
	AeController*	m_pZController;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Container class for a sub-controller of a blend controller
class AeBlendControllerItem
{
public:
	AeBlendControllerItem()
		: m_weight(1.0f)
	{}

	virtual ~AeBlendControllerItem() 
	{
		delete m_pController;
	}

	float			m_weight;			//Weight to be applied to the controller
	AeController*	m_pController;		//Controller 

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller whose output is defined by a weighted sum of its
//sub-controllers
class AeBlendController : public AeControllerContainer
{
public:
	AeBlendController() 
		: m_type ( kFloat )
	{}

	virtual ~AeBlendController() 
	{
		for(atArray< AeBlendControllerItem* >::iterator it = m_inputs.begin(); 
			it != m_inputs.end();
			++it)
		{
			delete (*it);
		}
	}

	void GetContainedControllers(vector<AeController*>& outControllers);

	int							GetBlendItemCount() const { return (int)m_inputs.size(); }
	AeBlendControllerItem*		GetBlendItem(int idx) const { Assert(idx<(int)m_inputs.size()); return m_inputs[idx]; }

	eComponentType						m_type;		//Type of data being blended
	atArray< AeBlendControllerItem* >	m_inputs;	//Array of sub-controller inputs

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller whose output is defined by a weighted sum of its
//sub-controllers - this is an object space version for constraints
class AeObjectSpaceBlendController : public AeBlendController
{
public:

	int		m_hashId;							//Controller identifier
	int		m_trackId;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE: Represents a controller whose output is defined by looking at a
//weighted list of targets
class AeLookAtController : public AeControllerContainer
{
public:
	AeLookAtController()
	:	m_pBlendController(NULL),
		m_pBoneOSController(NULL),
		m_pParentOSController(NULL),
		m_pUpnodeOSController(NULL),
		m_boneAxis(kAxisNone),
		m_boneUpAxis(kAxisNone),
		m_upnodeUpAxis(kAxisNone),
		m_offset(V_ONE)
	{
	}
	virtual ~AeLookAtController() 
	{
		delete m_pBlendController;
		delete m_pBoneOSController;
		delete m_pParentOSController;
		delete m_pUpnodeOSController;
	}

	void GetContainedControllers(vector<AeController*>& outControllers);

	AeBlendController*			m_pBlendController;		//Controller containing weighted look at target inputs
	AeObjectSpaceController*	m_pBoneOSController;	//Controller driven by the bone's translation
	AeObjectSpaceController*	m_pParentOSController;	//Controller driven by the bone's parent's translation
	AeObjectSpaceController*	m_pUpnodeOSController;	//Controller driven by the upnode's rotation
	eComponentAxis				m_boneAxis;				//The bone's axis that is aligned with the look at
	eComponentAxis				m_boneUpAxis;			//The bone's up axis
	eComponentAxis				m_upnodeUpAxis;			//The upnode's up axis
	Vec4V						m_offset;				//The rotational offset from the look at rotation

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

class AeDrivenElement
{
public:
	AeDrivenElement() 
		: m_priority(0)
	{}

	virtual ~AeDrivenElement() {}

	int		m_priority;

	atString		m_controllerId;		//Id of controller driving this component
	AeController*	m_pController;		//Not Serialized - Fixup on post-load

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE : Represents a component of a bone transform
class AeBoneTransformComponent : public AeDrivenElement
{
public:
	AeBoneTransformComponent() 
		: m_unitConversionFactor(1.0f)
		, m_trackId(-1)
	{}

	virtual ~AeBoneTransformComponent() {}

	atString	m_id;						//Component identifier
	int			m_trackId;					//Track identifier 
	float		m_unitConversionFactor;		//Scale factor to be applied to incoming from a controller

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE : Represents a bone that is being driven by an expression
class AeBone
{
public:
	AeBone() 
		: m_hashId(-1)
		, m_vParentScale(0.f,0.f,0.f)
	{}

	virtual ~AeBone() 
	{
		for(atArray< AeBoneTransformComponent* >::iterator it = m_transform.begin();
			it != m_transform.end();
			++it)
		{
			delete (*it);
		}
	}

	int							GetTransformComponentCount() const { return (int)m_transform.size(); }
	AeBoneTransformComponent*	GetTransformComponent(int idx) const { Assert(idx < (int)m_transform.size()); return m_transform[idx]; }

	atString	m_id;			//Bone name
	int			m_hashId;		//Runtime bone id

	atArray< AeBoneTransformComponent* >	m_transform;	//Array of transform components

	Vec3V		m_vParentOffset;
	Vec3V		m_vParentScale;
	Vec4V		m_vParentOrient;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE : Represents a blend target whose weight is driven by an expression
class AeMeshBlendTarget : public AeDrivenElement
{
public:
	AeMeshBlendTarget() {}
	
	virtual ~AeMeshBlendTarget() {}

	atString	m_id;			//Blend target name
	int			m_hashId;		//Hashed blend target name

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE : Represents an animated normal map multiplier which is driven by an expression
class AeMeshAnimNormalMapMult : public AeDrivenElement
{
public:
	AeMeshAnimNormalMapMult() {}

	virtual ~AeMeshAnimNormalMapMult() {}

	atString	m_id;			//Multipler name
	int			m_hashId;		//Hashed multiplier name

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE : Represents a mesh utilizing expressions to control some aspect of the
//			mesh data, be it blend targets, or shader parameters
class AeMesh
{
public:
	AeMesh() {}

	virtual ~AeMesh() 
	{
		for(atArray< AeMeshBlendTarget* >::iterator it = m_blendTargets.begin();
			it != m_blendTargets.end();
			++it)
		{
			delete (*it);
		}
		
		for(atArray< AeMeshAnimNormalMapMult* >::iterator it = m_animNrmMults.begin();
			it != m_animNrmMults.end();
			++it)
		{
			delete (*it);
		}
	}

	atString	m_id;	//Mesh name	

	atArray< AeMeshBlendTarget* >		m_blendTargets;			//Array of blend targets
	atArray< AeMeshAnimNormalMapMult* >	m_animNrmMults;			//Array of animated normal map multipliers

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE : Represents a secondary motion, containing two springs for linear and angular motion.
class AeMotion
{
public:
	AeMotion() {}

	virtual ~AeMotion() {}

	Vec3V m_strengthLinear, m_strengthAngular;
	Vec3V m_dampingLinear, m_dampingAngular;
	Vec3V m_minConstraintLinear, m_minConstraintAngular;
	Vec3V m_maxConstraintLinear, m_maxConstraintAngular;
	Vec3V m_gravity;
	Vec3V m_direction;
	int m_boneId;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------
//PURPOSE : Represents the main expression data file
class AeFile
{
public:
	AeFile();
		
	virtual ~AeFile();

	AeController*	FindControllerById(const atString& id);
	AeDriver*		FindDriverById(const atString& id);

	int				GetDriverCount() const { return (int)m_driverLibrary.size(); }
	AeDriver*		GetDriver(int idx) const { Assert(idx < (int)m_driverLibrary.size()); return m_driverLibrary[idx]; }

	int				GetControllerCount() const { return (int)m_controllerLibrary.size(); }
	AeController*	GetController(int idx) const { Assert(idx < (int)m_controllerLibrary.size()); return m_controllerLibrary[idx]; }

	int				GetBoneCount() const { return (int)m_boneLibrary.size(); }
	AeBone*			GetBone(int idx) const { Assert(idx < (int)m_boneLibrary.size()); return m_boneLibrary[idx]; }

	int				GetMeshCount() const { return (int)m_meshLibrary.size(); }
	AeMesh*			GetMesh(int idx) const { Assert(idx < (int)m_meshLibrary.size()); return m_meshLibrary[idx]; }

	float			GetGlobalTranslateScaleFactor() const { return m_globalTranslateScaleFactor; }
	void			SetGlobalTranslateScaleFactor(float sf) { m_globalTranslateScaleFactor = sf; }

	void			PrioritizeEvaluationOrdering();

	float						m_globalTranslateScaleFactor;
	
	atArray< AeDriver* >		m_driverLibrary;
	atArray< AeController* >	m_controllerLibrary;

	atArray< AeBone* >			m_boneLibrary;
	atArray< AeMesh* >			m_meshLibrary;
	atArray< AeMotion* >		m_motionLibrary;

	PAR_PARSABLE;

private:
	void		PostLoad();

	void		FixupControllerPtrs(AeController* pController);
	void		FixupDrivenControllerPtrs(AeDrivenController* pDrvCtrl);
	void		FixupExpressionControllerPtrs(AeExpressionController* pExprCtrl);
};

//-----------------------------------------------------------------------------

} // namespace rage

#endif // ANIMEXPRDATA_ANIMEXPRDATA_H 
