<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<structdef type="::rage::ExprNodeBase">
</structdef>

<enumdef type="::rage::ExprNodeTernaryOperator::eTernaryOpType">
	<enumval name="kNone"/>
	<enumval name="kIfThenElse"/>
	<enumval name="kClamp"/>
</enumdef>

<structdef base="::rage::ExprNodeBase" type="::rage::ExprNodeTernaryOperator">
	<pointer name="m_pLeft" policy="owner" type="::rage::ExprNodeBase"/>
	<pointer name="m_pMiddle" policy="owner" type="::rage::ExprNodeBase"/>
	<pointer name="m_pRight" policy="owner" type="::rage::ExprNodeBase"/>
	<enum name="m_opType" size="32" type="::rage::ExprNodeTernaryOperator::eTernaryOpType"/>
</structdef>

<enumdef type="::rage::ExprNodeBinaryOperator::eBinaryOpType">
	<enumval name="kNone"/>
	<enumval name="kAssign"/>
	<enumval name="kAdd"/>
	<enumval name="kSubtract"/>
	<enumval name="kMultiply"/>
	<enumval name="kDivide"/>
	<enumval name="kGreaterThan"/>
	<enumval name="kGreaterThanEqualTo"/>
	<enumval name="kLessThan"/>
	<enumval name="kLessThanEqualTo"/>
	<enumval name="kEqualTo"/>
	<enumval name="kNotEqualTo"/>
	<enumval name="kIfThen"/>
	<enumval name="kMax"/>
	<enumval name="kMin"/>
	<enumval name="kMod"/>
	<enumval name="kPow"/>
</enumdef>

<structdef base="::rage::ExprNodeBase" type="::rage::ExprNodeBinaryOperator">
	<pointer name="m_pLeft" policy="owner" type="::rage::ExprNodeBase"/>
	<pointer name="m_pRight" policy="owner" type="::rage::ExprNodeBase"/>
	<enum name="m_opType" size="32" type="::rage::ExprNodeBinaryOperator::eBinaryOpType"/>
</structdef>

<enumdef type="::rage::ExprNodeUnaryOperator::eUnaryOpType">
	<enumval name="kNone"/>
	<enumval name="kNegate"/>
	<enumval name="kAbs"/>
	<enumval name="kAcos"/>
	<enumval name="kAsin"/>
	<enumval name="kATan"/>
	<enumval name="kCeil"/>
	<enumval name="kCos"/>
	<enumval name="kCosH"/>
	<enumval name="kDToR"/>
	<enumval name="kExp"/>
	<enumval name="kFloor"/>
	<enumval name="kLn"/>
	<enumval name="kLog"/>
	<enumval name="kRToD"/>
	<enumval name="kSin"/>
	<enumval name="kSinH"/>
	<enumval name="kSqrt"/>
	<enumval name="kTan"/>
	<enumval name="kTanH"/>
	<enumval name="kCosD"/>
	<enumval name="kSinD"/>
	<enumval name="kTanD"/>
	<enumval name="kAcosD"/>
	<enumval name="kAsinD"/>
	<enumval name="kAtanD"/>
</enumdef>

<structdef base="::rage::ExprNodeBase" type="::rage::ExprNodeUnaryOperator">
	<pointer name="m_pChild" policy="owner" type="::rage::ExprNodeBase"/>
	<enum name="m_opType" size="32" type="::rage::ExprNodeUnaryOperator::eUnaryOpType"/>
</structdef>

<structdef base="::rage::ExprNodeBase" type="::rage::ExprNodeOperand">
</structdef>

<structdef base="::rage::ExprNodeOperand" type="::rage::ExprNodeNameAttrPair">
	<string name="m_name" noInit="true" type="atString"/>
	<string name="m_attr" noInit="true" type="atString"/>
</structdef>

<structdef base="::rage::ExprNodeOperand" type="::rage::ExprNodeConstant">
	<float init="0.0" name="m_value"/>
</structdef>

<enumdef type="::rage::ExprNodeTime::eTimeUnits">
	<enumval name="kSeconds"/>
	<enumval name="kFrame30"/>
	<enumval name="kFrame60"/>
</enumdef>

<structdef base="::rage::ExprNodeOperand" type="::rage::ExprNodeTime">
	<enum name="m_units" size="32" type="::rage::ExprNodeTime::eTimeUnits"/>
</structdef>

<structdef onPostLoad="PostLoad" type="::rage::ExprTree">
	<pointer name="m_pRoot" policy="owner" type="::rage::ExprNodeBase"/>
</structdef>

</ParserSchema>