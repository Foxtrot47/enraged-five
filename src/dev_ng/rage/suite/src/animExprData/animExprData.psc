<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>

<enumdef type="::rage::eComponentType">
	<enumval name="kFloat"/>
	<enumval name="kVector"/>
	<enumval name="kQuat"/>
</enumdef>

<enumdef type="::rage::eComponentRotationOrder">
	<enumval name="kXYZ"/>
	<enumval name="kYZX"/>
	<enumval name="kZXY"/>
	<enumval name="kXZY"/>
	<enumval name="kYXZ"/>
	<enumval name="kZYX"/>
</enumdef>

<enumdef type="::rage::eComponentAxis">
	<enumval name="kXAxis"/>
	<enumval name="kYAxis"/>
	<enumval name="kZAxis"/>
	<enumval name="kNegXAxis"/>
	<enumval name="kNegYAxis"/>
	<enumval name="kNegZAxis"/>
	<enumval name="kAxisNone"/>
	<enumval name="kAxisAll"/>
</enumdef>

<enumdef type="::rage::eEvaluationSpace">
	<enumval name="kDefault"/>
	<enumval name="kZeroed"/>
</enumdef>

<structdef type="::rage::AeDriverComponent">
	<string name="m_id" noInit="true" type="atString"/>
	<int init="-1" name="m_trackId" noInit="false" type="int"/>
	<enum name="m_type" size="32" type="::rage::eComponentType"/>
	<enum name="m_axis" size="32" type="::rage::eComponentAxis"/>
	<enum name="m_rotOrder" size="32" type="::rage::eComponentRotationOrder"/>
	<enum name="m_evalSpace" size="32" type="::rage::eEvaluationSpace"/>
</structdef>

<structdef type="::rage::AeDriver">
	<string name="m_id" noInit="true" type="atString"/>
	<int init="-1" name="m_boneId" noInit="false" type="int"/>
	<array name="m_components" type="atArray">
		<struct type="::rage::AeDriverComponent"/>
	</array>
</structdef>

<structdef type="::rage::AeController">
	<float name="m_outputUnitConversionFactor"/>
	<string name="m_id" noInit="true" type="atString"/>
</structdef>

<structdef type="::rage::AeDrivenControllerInput">
	<string name="m_driverId" noInit="true" type="atString"/>
	<string name="m_driverCompId" noInit="true" type="atString"/>
	<float name="m_unitConversionFactor" noInit="true"/>
</structdef>

<structdef base="::rage::AeController" type="::rage::AeDrivenController">
	<array name="m_inputs" type="atArray">
		<pointer name="m_inputs" policy="owner" type="::rage::AeDrivenControllerInput"/>
	</array>
</structdef>

<structdef base="::rage::AeController" type="::rage::AeControllerContainer">
</structdef>

<structdef base="::rage::AeController" type="::rage::AeFloatConstantController">
	<float init="0.0" name="m_value" noInit="false"/>
</structdef>

<structdef type="::rage::AeCurveKey">
	<float init="0.0" name="m_in" noInit="false"/>
	<float init="0.0" name="m_out" noInit="false"/>
</structdef>

<structdef base="::rage::AeDrivenController" type="::rage::AeFloatCurveController">
	<array name="m_keys" type="atArray">
		<struct type="::rage::AeCurveKey"/>
	</array>
</structdef>

<structdef base="::rage::AeDrivenController" type="::rage::AeExpressionController">
	<pointer name="m_pExprTree" policy="owner" type="::rage::ExprTree"/>
</structdef>

<structdef base="::rage::AeDrivenController" type="::rage::AeObjectSpaceController">
</structdef>

<structdef base="::rage::AeDrivenController" type="::rage::AeDirectController">
	<float init="1.0" name="m_scale" noInit="false"/>
</structdef>

<structdef base="::rage::AeControllerContainer" type="::rage::AeXYZController">
	<enum name="m_type" size="32" type="::rage::eComponentType"/>
	<pointer name="m_pXController" policy="owner" type="::rage::AeController"/>
	<pointer name="m_pYController" policy="owner" type="::rage::AeController"/>
	<pointer name="m_pZController" policy="owner" type="::rage::AeController"/>
</structdef>

<structdef type="::rage::AeBlendControllerItem">
	<float init="1.0" name="m_weight" noInit="false"/>
	<pointer name="m_pController" policy="owner" type="::rage::AeController"/>
</structdef>

<structdef base="::rage::AeControllerContainer" type="::rage::AeBlendController">
	<enum name="m_type" size="32" type="::rage::eComponentType"/>
	<array name="m_inputs" type="atArray">
		<pointer policy="owner" type="::rage::AeBlendControllerItem"/>
	</array>
</structdef>

<structdef base="::rage::AeBlendController" type="::rage::AeObjectSpaceBlendController">
	<int init="-1" name="m_hashId" noInit="false" type="int"/>
	<int init="-1" name="m_trackId" noInit="false" type="int"/>
</structdef>

<structdef base="::rage::AeControllerContainer" type="::rage::AeLookAtController">
	<pointer name="m_pBlendController" policy="owner" type="::rage::AeBlendController"/>
	<pointer name="m_pBoneOSController" policy="owner" type="::rage::AeObjectSpaceController"/>
	<pointer name="m_pParentOSController" policy="owner" type="::rage::AeObjectSpaceController"/>
	<pointer name="m_pUpnodeOSController" policy="owner" type="::rage::AeObjectSpaceController"/>
	<enum name="m_boneAxis" size="32" type="::rage::eComponentAxis"/>
	<enum name="m_boneUpAxis" size="32" type="::rage::eComponentAxis"/>
	<enum name="m_upnodeUpAxis" size="32" type="::rage::eComponentAxis"/>
	<Vector4 name="m_offset"/>
</structdef>

<structdef type="::rage::AeDrivenElement">
	<int init="0" name="m_priority"/>
	<string name="m_controllerId" type="atString"/>
</structdef>

<structdef base="::rage::AeDrivenElement" type="::rage::AeBoneTransformComponent">
	<string name="m_id" noInit="true" type="atString"/>
	<int init="-1" name="m_trackId" noInit="false" type="int"/>
	<float name="m_unitConversionFactor" noInit="true"/>
</structdef>

<structdef type="::rage::AeBone">
	<string name="m_id" noInit="true" type="atString"/>
	<int init="-1" name="m_hashId" noInit="false"/>
	<array name="m_transform" type="atArray">
		<pointer policy="owner" type="::rage::AeBoneTransformComponent"/>
	</array>
	<Vector3 name="m_vParentOffset"/>
	<Vector4 name="m_vParentOrient"/>
</structdef>

<structdef base="::rage::AeDrivenElement" type="::rage::AeMeshBlendTarget">
	<string name="m_id" noInit="true" type="atString"/>
	<int init="-1" name="m_hashId" noInit="false"/>
</structdef>

<structdef base="::rage::AeDrivenElement" type="::rage::AeMeshAnimNormalMapMult">
	<string name="m_id" noInit="true" type="atString"/>
	<int init="-1" name="m_hashId" noInit="false"/>
</structdef>

<structdef type="::rage::AeMesh">
	<string name="m_id" noInit="true" type="atString"/>
	<array name="m_blendTargets" type="atArray">
		<pointer policy="owner" type="::rage::AeMeshBlendTarget"/>
	</array>
	<array name="m_animNrmMults" type="atArray">
		<pointer policy="owner" type="::rage::AeMeshAnimNormalMapMult"/>
	</array>
</structdef>

<structdef onPostLoad="PostLoad" type="::rage::AeFile">

	<float name="m_globalTranslateScaleFactor"/>
	
	<array name="m_driverLibrary" type="atArray">
		<pointer policy="owner" type="::rage::AeDriver"/>
	</array>

	<array name="m_controllerLibrary" type="atArray">
		<pointer policy="owner" type="::rage::AeController"/>
	</array>

	<array name="m_boneLibrary" type="atArray">
		<pointer policy="owner" type="::rage::AeBone"/>
	</array>

	<array name="m_meshLibrary" type="atArray">
		<pointer policy="owner" type="::rage::AeMesh"/>
	</array>

	<array name="m_motionLibrary" type="atArray">
		<pointer policy="owner" type="::rage::AeMotion"/>
	</array>
</structdef>

<structdef type="::rage::AeMotion">
	<Vector3 name="m_strengthLinear"/>
	<Vector3 name="m_strengthAngular"/>
	<Vector3 name="m_dampingLinear"/>
	<Vector3 name="m_dampingAngular"/>
	<Vector3 name="m_minConstraintLinear"/>
	<Vector3 name="m_minConstraintAngular"/>
	<Vector3 name="m_maxConstraintLinear"/>
	<Vector3 name="m_maxConstraintAngular"/>
	<Vector3 init="0.0" name="m_gravity" noInit="false"/>
	<Vector3 init="0.0" name="m_direction" noInit="false"/>
	<int name="m_boneId"/>
</structdef>

</ParserSchema>