// 
// cutscene/cutsaudioentity.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutsaudioentity.h"

#include "cutsoptimisations.h"
#include "cutschannel.h"
#include "cutsevent.h"
#include "cutsmanager.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "system/param.h"

CUTS_OPTIMISATIONS()

namespace rage {

XPARAM(noaudio);

//##############################################################################

cutsAudioEntity::cutsAudioEntity()
: cutsUniqueEntity()
#if __BANK
, m_bPlayingForward(true)
#endif // __BANK
, m_iCurrentBuffer(0)
, m_iLastBuffer(-1)
, m_iCurrentConcatSection(0)
, m_iLoadConcatSection(-1)
, m_iLoadBuffer(-1)
, m_bWasPaused(false)
, m_bWasResuming(false)
{

}

cutsAudioEntity::cutsAudioEntity( const cutfObject* pObject )
: cutsUniqueEntity(pObject)
#if __BANK
, m_bPlayingForward(true)
#endif // __BANK
, m_iCurrentBuffer(0)
, m_iLastBuffer(-1)
, m_iCurrentConcatSection(0)
, m_iLoadConcatSection(-1)
, m_iLoadBuffer(-1)
, m_bWasPaused(false)
, m_bWasResuming(false)
{

}

cutsAudioEntity::~cutsAudioEntity()
{

}

void cutsAudioEntity::DispatchEvent( cutsManager *pManager, const cutfObject *pObject, s32 iEventId, const cutfEventArgs* pEventArgs,  const float UNUSED_PARAM(ftime), const u32 UNUSED_PARAM(StickyId) )
{
    switch ( iEventId )
    {
    case CUTSCENE_RESTART_EVENT:
        {
            m_bWasPaused = false;
            m_bWasResuming = false;

            UnloadAllAudio();
        }
        // fallthru
    case CUTSCENE_LOAD_AUDIO_EVENT:
        {
            m_iCurrentConcatSection = pManager->GetStartingConcatSection();
            m_iCurrentBuffer = 0;

            const cutfAudioObject *pAudioObject = dynamic_cast<const cutfAudioObject *>( pObject );

            char cAudioName[CUTSCENE_OBJNAMELEN];
            float fStartOffset = 0.0f;

            // If section is 0, favor the manager's audio start time in case it's different than what's stored in the cutfAudioObject.
            if (m_iCurrentConcatSection == 0)    
            {
                if ( (pEventArgs != NULL) && (pEventArgs->GetType() == CUTSCENE_FINAL_NAME_EVENT_ARGS_TYPE) )
                {
                    const cutfFinalNameEventArgs *pNameEventArgs = dynamic_cast<const cutfFinalNameEventArgs *>( pEventArgs );
                    safecpy( cAudioName, pNameEventArgs->GetName(), sizeof(cAudioName) );
                }
                else
                {
                    safecpy( cAudioName, pAudioObject->GetDisplayName().c_str(), sizeof(cAudioName) );
                }

                fStartOffset = pManager->GetAudioStartTime();
            }

            if ( !LoadAudio( pManager, pObject->GetObjectId(), cAudioName, fStartOffset, m_iCurrentBuffer ) && !ContinueWithoutAudio() )
            {
                pManager->Unload();
            }

            m_bWasPaused = false;
            m_bWasResuming = false;
        }
        break;
    case CUTSCENE_UNLOAD_AUDIO_EVENT:
        {
            // just to make sure
            UnloadAllAudio(); 
            
            m_bWasPaused = false;
            m_bWasResuming = false;
        }
        break;
    case CUTSCENE_PLAY_AUDIO_EVENT:
        {
            if ( m_audioPrepareData[m_iCurrentBuffer].prepareState == AUD_PREPARED )
            {
                Play( m_iCurrentBuffer );
            }
        }
        break;
    case CUTSCENE_STOP_AUDIO_EVENT:
    case CUTSCENE_CANCEL_LOAD_EVENT:
        {
            UnloadAudio( m_iCurrentBuffer );

            // just in case
            pManager->SetIsLoading( pObject->GetObjectId(), false );
        }
        break;
    case CUTSCENE_UPDATE_LOADING_EVENT:
        {
            if ( !UpdateLoading( pManager, pObject ) && !ContinueWithoutAudio() )
            {
                pManager->Unload();
            }
        }
        break;
    case CUTSCENE_UPDATE_EVENT:
        {
            // if the buffer changed last time, we need to unload the last one and load up a new one
            if ( m_iCurrentBuffer != m_iLastBuffer )
            {
#if __BANK
                int iLoadBuffer = m_iLastBuffer;
                int iLoadConcatSection = -1;

                // last buffer of -1 indicates that we just started the scene
                if ( m_bPlayingForward )
                {
                    if ( m_iLastBuffer == -1 )
                    {
                        // We just started the scene
                        iLoadBuffer = 1;
                    }
                    else
                    {
                        // go back one more buffer
                        --iLoadBuffer;
                        if ( iLoadBuffer < 0 )
                        {
                            iLoadBuffer = c_numBuffers - 1;
                        }
                    }

                    iLoadConcatSection = pManager->GetNextConcatSection( m_iCurrentConcatSection );
                }
                else
                {
                    // go forward one more buffer
                    ++iLoadBuffer;
                    if ( iLoadBuffer == c_numBuffers )
                    {
                        iLoadBuffer = 0;
                    }

                    iLoadConcatSection = pManager->GetPreviousConcatSection( m_iCurrentConcatSection );
                }
#else // __BANK
                int iLoadBuffer = m_iLastBuffer;

                // We just started the scene
                if ( iLoadBuffer == -1 )
                {
                    iLoadBuffer = 1;
                }

                int iLoadConcatSection = pManager->GetNextConcatSection( m_iCurrentConcatSection );
#endif // __BANK

                if ( m_iLastBuffer != -1 )
                {
                    UnloadAudio( iLoadBuffer );
                }

                if ( iLoadConcatSection != -1 )
                {
                    // Deferred loading
                    m_iLoadConcatSection = iLoadConcatSection;
                    m_iLoadBuffer = iLoadBuffer;
                }
            }

            m_iLastBuffer = m_iCurrentBuffer;

            // see if we need to change the buffer
            int iCurrentConcatSection = pManager->GetCurrentConcatSection();
            if ( iCurrentConcatSection > m_iCurrentConcatSection )
            {                   
                ++m_iCurrentBuffer;
                if ( m_iCurrentBuffer == c_numBuffers )
                {
                    m_iCurrentBuffer = 0;
                }

                if ( m_audioPrepareData[m_iCurrentBuffer].prepareState == AUD_PREPARED )
                {
                    Play( m_iCurrentBuffer );
                }

#if __BANK
                m_bPlayingForward = true;
#endif // __BANK
            }
#if __BANK
            else if ( iCurrentConcatSection < m_iCurrentConcatSection )
            {
                --m_iCurrentBuffer;
                if ( m_iCurrentBuffer == -1 )
                {
                    m_iCurrentBuffer = c_numBuffers - 1;
                }

                m_bPlayingForward = false;
            }
#endif // __BANK
            
            if ( iCurrentConcatSection != m_iCurrentConcatSection )
            {
                m_iCurrentConcatSection = iCurrentConcatSection;
            }

            if ( !UpdateLoading( pManager, pObject ) && !ContinueWithoutAudio() )
            {
                pManager->Unload();
            }
        }
        break;
    case CUTSCENE_PLAY_EVENT:
        {
            if ( m_bWasPaused )
            {
                // A little redundant, but makes sure we're ready to re-prepare the audio
                UnloadAllAudio();

                m_iCurrentConcatSection = pManager->GetConcatSectionForTime( pManager->GetTime() );
                m_iCurrentBuffer = 0;

                const cutfAudioObject *pAudioObject = dynamic_cast<const cutfAudioObject *>( pObject );

                char cAudioName[CUTSCENE_OBJNAMELEN];
                safecpy( cAudioName, pAudioObject->GetDisplayName().c_str(), sizeof(cAudioName) );

                float fStartOffset = ConvertAudioSecondsToAudioOffset( pManager, pObject, pManager->GetAudioSeconds(), pManager->GetTime() );

                if ( LoadAudio( pManager, pObject->GetObjectId(), cAudioName, fStartOffset, m_iCurrentBuffer ) )
                {
                    // Tell the manager to wait for us
                    pManager->NeedToLoadBeforeResuming();
                    
                    m_bWasResuming = true;
                }
                else if ( !ContinueWithoutAudio() )
                {
                    pManager->Unload();
                }

                m_bWasPaused = false;
            }
            else if ( m_bWasResuming )
            {
                if ( m_audioPrepareData[m_iCurrentBuffer].prepareState == AUD_PREPARED )
                {
                    Play( m_iCurrentBuffer );
                }

                m_bWasResuming = false;
            }
        }
        break;
    case CUTSCENE_PAUSE_EVENT:
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
    case CUTSCENE_FAST_FORWARD_EVENT:
        {
            // We can't pause or scrub, so just stop the audio.  We'll re-buffer it later.
            UnloadAllAudio();

            m_bWasPaused = true;
            m_bWasResuming = false;
        }
        break;
#if __BANK
    case CUTSCENE_MUTE_AUDIO_EVENT:
        {
            Mute( m_iCurrentBuffer );
        }
        break;
    case CUTSCENE_UNMUTE_AUDIO_EVENT:
        {
            Unmute( m_iCurrentBuffer );
        }
        break;
#endif // __BANK
    default:
        break;
    }
}

bool cutsAudioEntity::ContinueWithoutAudio() const
{
    return PARAM_noaudio.Get();
}

float cutsAudioEntity::GetAudioTime( cutsManager *pManager, const cutfObject* pObject )
{
    float fAudioOffset = ConvertAudioSecondsToAudioOffset( pManager, pObject, pManager->GetAudioSeconds(), pManager->GetTime() );
    
    float fAudioTime = GetPlayTime( fAudioOffset, m_iCurrentBuffer );
    if ( fAudioTime < 0.0f )
    {
        return -1.0f;
    }

    float fAudioSeconds = ConvertAudioOffsetToAudioSeconds( pManager, pObject, fAudioTime, pManager->GetTime() );

    return fAudioSeconds;
}

#if __BANK

void cutsAudioEntity::Mute( int /*iBuffer*/ )
{

}

void cutsAudioEntity::Unmute( int /*iBuffer*/ )
{

}

#endif // __BANK

float cutsAudioEntity::ConvertAudioSecondsToAudioOffset( cutsManager *pManager, const cutfObject* UNUSED_PARAM(pObject), float fAudioSeconds, float fSceneSeconds ) const
{
    float fAudioOffset = fAudioSeconds;

    int iConcatSection = pManager->GetConcatSectionForTime( fSceneSeconds );
    if ( iConcatSection > 0 )
    {        
        const atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> &concatDataList = pManager->GetConcatDataList();
        if ( (concatDataList.GetCount() > 1) && (iConcatSection < concatDataList.GetCount()) )
        {
            // adjust for this being a new section of concatenated audio, taking into account sectioned vs. unsectioned animations
            fAudioOffset -= (concatDataList[iConcatSection].fStartTime + (fAudioSeconds - fSceneSeconds));
        }
    }

    return fAudioOffset;
}

float cutsAudioEntity::ConvertAudioOffsetToAudioSeconds( cutsManager *pManager, const cutfObject* UNUSED_PARAM(pObject), float fAudioOffset, float fSceneSeconds ) const
{
    float fAudioSeconds = fAudioOffset;

    int iConcatSection = pManager->GetConcatSectionForTime( fSceneSeconds );
    if ( iConcatSection > 0 )
    {        
        const atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> &concatDataList = pManager->GetConcatDataList();

        if ( (concatDataList.GetCount() > 1) && (iConcatSection < concatDataList.GetCount()) )
        {
            // adjust for this being a new section of concatenated audio
            fAudioSeconds += concatDataList[iConcatSection].fStartTime;
        }
    }

    return fAudioSeconds;
}

bool cutsAudioEntity::LoadAudio( cutsManager *pManager, s32 iObjectId, const char* pName, float fStartOffset, int iBuffer )
{
    safecpy( m_audioPrepareData[iBuffer].audioData.cAudioName, pName, sizeof(m_audioPrepareData[iBuffer].audioData.cAudioName) );
    m_audioPrepareData[iBuffer].audioData.fStartOffset = fStartOffset;

    cutsDebugf1( "%s Buffering audio '%s' to start at %f.", pManager->GetDisplayTime(), pName, fStartOffset );

    m_audioPrepareData[iBuffer].prepareState 
        = Prepare( m_audioPrepareData[iBuffer].audioData.cAudioName, m_audioPrepareData[iBuffer].audioData.fStartOffset, iBuffer );
    switch ( m_audioPrepareData[iBuffer].prepareState )
    {
    case AUD_PREPARING:
        {
            pManager->SetIsLoading( iObjectId, true );
        }
        break;
    case AUD_PREPARED:
        {
            pManager->SetIsLoading( iObjectId, true );
            pManager->SetIsLoaded( iObjectId, true );
        }
        break;
    case AUD_PREPARE_FAILED:
        {
            cutsErrorf( "%s Preparation of audio track '%s' failed.", pManager->GetDisplayTime(), pName );            
        }
        return false;
    default:
        break;
    }                

    return true;
}

void cutsAudioEntity::UnloadAudio( int iBuffer )
{
    Stop( iBuffer );

    m_audioPrepareData[iBuffer].prepareState = -1;
}

void cutsAudioEntity::UnloadAllAudio()
{   
    for ( int i = 0; i < c_numBuffers; ++i )
    {
        UnloadAudio( i );
    }

    m_iLastBuffer = -1;
    m_iCurrentConcatSection = -1;
    m_iCurrentBuffer = 0;

    m_iLoadBuffer = -1;
    m_iLoadConcatSection = -1;
}

bool cutsAudioEntity::UpdateLoading( cutsManager *pManager, const cutfObject *pObject )
{
    for ( int i = 0; i < c_numBuffers; ++i )
    {
        if ( m_audioPrepareData[i].prepareState == AUD_PREPARING )
        {
            m_audioPrepareData[i].prepareState 
                = Prepare( m_audioPrepareData[i].audioData.cAudioName, m_audioPrepareData[i].audioData.fStartOffset, i );
            switch ( m_audioPrepareData[i].prepareState )
            {
            case AUD_PREPARING:
                break;
            case AUD_PREPARED:
                {
                    pManager->SetIsLoaded( pObject->GetObjectId(), true );
                }
                break;
            case AUD_PREPARE_FAILED:
                {
                    cutsErrorf( "%s Preparation of audio track '%s' failed.", pManager->GetDisplayTime(), 
                        m_audioPrepareData[i].audioData.cAudioName );
                }
                return false;
            }
        }
    }

    return true;
}

//##############################################################################

} // namespace rage
