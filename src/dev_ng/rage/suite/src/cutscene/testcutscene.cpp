// 
// /testcutscene.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cutsentity.h"
#include "cutsevent.h"
#include "cutsmanager.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfobject.h"
#include "parser/macros.h"
#include "system/param.h"

using namespace rage;

PARAM( cutfile, "[testcutscene] The cut file to simulate playing" );

//##############################################################################

class TestCutsceneEntity : public cutsSingletonEntity
{
public:
    TestCutsceneEntity()
    {

    }

    ~TestCutsceneEntity()
    {

    }

    void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* /*pEventArgs*/ )
    {
        if ( iEventId != CUTSCENE_UPDATE_EVENT )
        {
            Displayf( "[%f] %s -> (%d) %s", pManager->GetSeconds(), cutfEvent::GetDisplayName( iEventId ), 
                pObject->GetObjectId(), pObject->GetDisplayName().c_str() );
        }
    }
};

class TestCutsceneManager : public cutsManager
{
public:
    TestCutsceneManager()
    {

    }

    ~TestCutsceneManager()
    {

    }

protected:
    cutsEntity* ReserveEntity( const cutfObject* pObject )
    {
        Displayf( "[%f] Reserved Entity for %s", GetSeconds(), pObject->GetDisplayName().c_str() );
        m_testCutsceneEntity.AddObject( pObject );
        return &m_testCutsceneEntity;
    }

    void ReleaseEntity( cutsEntity* /*pEntity*/, const cutfObject* pObject )
    {
        m_testCutsceneEntity.RemoveObject( pObject );
        Displayf( "[%f] Released Entity for %s", GetSeconds(), pObject->GetDisplayName().c_str() );

        if ( m_testCutsceneEntity.GetObjectList().GetCount() == 0 )
        {
            Displayf( "\tAll entities released.  Can dispose of TestCutsceneEntity now." );
        }
    }

#if __BANK
    SEditCutfObjectLocationInfo* GetHiddenObjectInfo()
    {
        return NULL;
    }

    SEditCutfObjectLocationInfo* GetFixupObjectInfo()
    {
        return NULL;
    }
#endif // __BANK

private:
    TestCutsceneEntity m_testCutsceneEntity;
};

//##############################################################################

int Main()
{
    const char* pCutfile;
    if ( !PARAM_cutfile.Get( pCutfile ) )
    {
        sysParam::Help( "=testcutscene" );
        return 1;
    }

    INIT_PARSER;
    cutsManager::InitClass();
    
    TestCutsceneManager mgr;
    mgr.Load( pCutfile );

    do 
    {
        mgr.PreSceneUpdate();
        mgr.PostSceneUpdate( 1.0f / 30.0f );
    } while( mgr.IsRunning() );

    cutsManager::ShutdownClass();
    SHUTDOWN_PARSER;
    
    return 0;
}
