// 
// cutscene/cutsboundsentity.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTSCENE_CUTSBOUNDSENTITY_H 
#define CUTSCENE_CUTSBOUNDSENTITY_H 

#include "cutsentity.h"
#include "vector/matrix34.h"
#include "vector/vector3.h"

namespace rage {

//#############################################################################

class cutsBoundsEntity : public cutsUniqueEntity
{
public:
    cutsBoundsEntity();
    cutsBoundsEntity( const cutfObject* pObject );
    virtual ~cutsBoundsEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL, const float fTime = 0.0f, const u32 StickyId = 0 );

    // PURPOSE: Returns true when this bounds is active.
    // RETURNS: true or false
    bool IsActive() const;

protected:
    bool m_bIsActive;

#if !__FINAL
public:
    // PURPOSE: Draws debug text and/or objects for this entity.
    // NOTES:  The cutsManager will call this function when GetDebugDraw returns true.
    virtual void DebugDraw() const;

protected:
    Matrix34 m_sceneOrientation;
    Vector3 m_vertices[12][2];
#endif // !__FINAL
};

inline bool cutsBoundsEntity::IsActive() const
{
    return m_bIsActive;
}

//#############################################################################

} // namespace rage

#endif // CUTSCENE_CUTSBOUNDSENTITY_H 
