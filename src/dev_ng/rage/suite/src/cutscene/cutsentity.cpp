// 
// cutscene/cutsentity.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutsentity.h"

#include "cutsoptimisations.h"
#include "cutsevent.h"
#include "cutfile/cutfobject.h"

CUTS_OPTIMISATIONS()

namespace rage {

cutsEntity::cutsEntity()
#if !__FINAL
: m_bDebugDraw(false)
#endif // !__FINAL
{

}

void cutsEntity::DispatchEvent( cutsManager* /*pManager*/, const cutfObject* /*pObject*/, s32 NOTFINAL_ONLY(iEventId), 
                               const cutfEventArgs* /*pEventArgs*/, const float /*fTime*/, const u32 )
{
#if !__FINAL
    switch ( iEventId )
    {
    case CUTSCENE_SHOW_DEBUG_LINES_EVENT:
        {
            m_bDebugDraw = true;
        }
        break;
    case CUTSCENE_HIDE_DEBUG_LINES_EVENT:
    case CUTSCENE_END_OF_SCENE_EVENT:
        {
            m_bDebugDraw = false;
        }
        break;
    default:
        break;
    }
#endif // !__FINAL
}

//##############################################################################

cutsUniqueEntity::cutsUniqueEntity()
: m_pObject( NULL )
{

}

cutsUniqueEntity::cutsUniqueEntity( const cutfObject* pObject )
: m_pObject( pObject )
{

}

//##############################################################################

cutsSingletonEntity::cutsSingletonEntity()
{

}

cutsSingletonEntity::cutsSingletonEntity( atArray<const cutfObject*>& pObjectList )
{
    m_pObjectList.Assume( pObjectList );
}

void cutsSingletonEntity::AddObject( const cutfObject *pObject )
{
    m_pObjectList.Grow() = pObject;
}

void cutsSingletonEntity::RemoveObject( const cutfObject *pObject )
{
    for ( int i = 0; i < m_pObjectList.GetCount(); ++i )
    {
        if ( m_pObjectList[i]->GetObjectId() == pObject->GetObjectId() )
        {
            m_pObjectList.Delete( i );
            break;
        }
    }
}

const cutfObject* cutsSingletonEntity::GetObjectById( s32 iObjectId )
{
    for ( int i = 0; i < m_pObjectList.GetCount(); ++i )
    {
        if ( m_pObjectList[i]->GetObjectId() == iObjectId )
        {
            return m_pObjectList[i];
        }
    }

    return NULL;
}

//##############################################################################

} // namespace rage
