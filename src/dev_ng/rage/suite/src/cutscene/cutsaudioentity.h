// 
// cutscene/cutsaudioentity.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTSCENE_CUTSAUDIOENTITY_H 
#define CUTSCENE_CUTSAUDIOENTITY_H 

#include "cutsentity.h"

#include "audiosoundtypes/soundcontrol.h"
#include "cutfile/cutfdefines.h"
#include "cutfile/cutfobject.h"

namespace rage {

//##############################################################################

class cutsAudioEntity : public cutsUniqueEntity
{
public:
    cutsAudioEntity();
    cutsAudioEntity( const cutfObject* pObject );
    virtual ~cutsAudioEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL, const float fTime = 0.0f, const u32 StickyId = 0);

    // PURPOSE: Retrieves the name of the current audio stream.   
    // RETURNS: The audio stream name
    const char* GetAudioName() const;

    // PURPOSE: Retrieves the start time for the current audio stream.
    // RETURNS: The start time in seconds
    float GetStartOffset() const;

    // PURPOSE: Retrieves the current audio time in seconds, adjusted for the current concat section (when we've concatenated
    //    cutscenes and Use One Audio is false).
    // PARAMS:
    //    pManager - the cutscene manager
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    fSeconds - seconds that have elapsed since playback of the entire scene began
    // RETURNS: the time in seconds, or -1.0f on error or if no audio is playing
    float GetAudioTime( cutsManager *pManager, const cutfObject* pObject );

    // PURPOSE: Retrieves the status of Was Paused.
    // RETURNS: true or false
    bool WasPaused() const;

protected:
    // PURPOSE: Indicates when we should *not* cancel the cutscene if we fail to prepare the audio.
    // RETURNS: True to continue without audio
    virtual bool ContinueWithoutAudio() const;

    // PURPOSE:  Returns the current audio time in seconds
    // PARAMS:
    //    fSeconds - the seconds that have elapsed since Prepare was called.
    //    iBuffer - the buffer to look at
    // RETURNS: the play time in seconds, or -1.0f on error or if no audio is playing
    virtual float GetPlayTime( float fSeconds, int iBuffer ) = 0;

    // PURPOSE:  Asynchronous call to request the slot acquisition and preloading of a cutscene audio asset.
    // PARAMS:
    //    pName - the name of the cut scene/audio stream
    //    fStartOffset - the offset, in seconds, at which to begin playback
    //    iBuffer - the buffer to prepare
    // RETURNS: The prepare state.
    virtual audPrepareState Prepare( const char *pName, float fStartOffset, int iBuffer ) = 0;
    
    // PURPOSE:  Start the audio
    // PARAMS:
    //    iBuffer - the buffer to play
    // NOTES:  Calling code must ensure that the asset is prepared (ie Prepare() returns AUD_PREPARED)
    virtual void Play( int iBuffer ) = 0;
    
    // PURPOSE:  Stop the audio and free resources.
    // PARAMS:
    //    iBuffer - the buffer to stop
    virtual void Stop( int iBuffer ) = 0;

#if __BANK
    // PURPOSE: Mute the audio
    // PARAMS:
    //    iBuffer - the buffer to mute
    virtual void Mute( int iBuffer );

    // PURPOSE: Unmute the audio
    // PARAMS:
    //    iBuffer - the buffer to unmute
    virtual void Unmute( int iBuffer );
#endif // __BANK
    
    float ConvertAudioSecondsToAudioOffset( cutsManager *pManager, const cutfObject* pObject, float fAudioSeconds, float fSceneSeconds ) const;

    float ConvertAudioOffsetToAudioSeconds( cutsManager *pManager, const cutfObject* pObject, float fAudioOffset, float fSceneSeconds ) const;

    bool LoadAudio( cutsManager *pManager, s32 iObjectId, const char* pName, float fStartOffset, int iBuffer );

    void UnloadAudio( int iBuffer );

    void UnloadAllAudio();

    bool UpdateLoading( cutsManager *pManager, const cutfObject *pObject );

    static const int c_numBuffers = 2;  // current and next

    struct SAudioPrepareData
    {
        SAudioPrepareData()
            : audioData()
            , prepareState(-1)
        {

        }

        SAudioPrepareData( const cutfAudioObject::SAudioData &data )
            : audioData(data)
            , prepareState(-1)
        {

        }

        cutfAudioObject::SAudioData audioData;
        int prepareState;
    };

    SAudioPrepareData m_audioPrepareData[c_numBuffers];

#if __BANK
    bool m_bPlayingForward;
#endif
    int m_iCurrentBuffer;
    int m_iLastBuffer;
    int m_iCurrentConcatSection;
    int m_iLoadConcatSection;
    int m_iLoadBuffer;

    bool m_bWasPaused;
    bool m_bWasResuming;
};

inline const char* cutsAudioEntity::GetAudioName() const
{
    if ( m_iCurrentConcatSection != -1 )
    {
        return m_audioPrepareData[m_iCurrentConcatSection].audioData.cAudioName;
    }

    return NULL;
}

inline float cutsAudioEntity::GetStartOffset() const
{
    if ( m_iCurrentConcatSection != -1 )
    {
        return m_audioPrepareData[m_iCurrentConcatSection].audioData.fStartOffset;
    }

    return -1.0f;
}

inline bool cutsAudioEntity::WasPaused() const
{
    return m_bWasPaused;
}

//##############################################################################

} // namespace rage

#endif // CUTSCENE_CUTSAUDIOENTITY_H 
