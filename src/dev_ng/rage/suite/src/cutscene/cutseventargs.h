// 
// cutscene/cutseventargs.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTSCENE_CUTSEVENTARGS_H 
#define CUTSCENE_CUTSEVENTARGS_H 

#include "cutfile\cutfeventargs.h"

namespace rage {

class crAnimation;
class crClip;
class crClipDictionary;

//##############################################################################

enum ECutscenePlaybackEventArgsType
{
    CUTSCENE_UPDATE_EVENT_ARGS_TYPE = CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY,
    CUTSCENE_ANIMATION_EVENT_ARGS_TYPE,
    CUTSCENE_DUAL_ANIMATION_EVENT_ARGS_TYPE,
    CUTSCENE_CLIP_EVENT_ARGS_TYPE,
    CUTSCENE_DUAL_CLIP_EVENT_ARGS_TYPE,
	CUTSCENE_DICTIONARY_LOADED_EVENT_ARGS_TYPE,
    CUTSCENE_LAST_PLAYBACK_EVENT_ARGS_TYPE,
    CUTSCENE_DUAL_CLIP_ANIM_EVENT_ARGS_TYPE = CUTSCENE_LAST_PLAYBACK_EVENT_ARGS_TYPE
};

//##############################################################################

class cutsEventArgs : public cutfEventArgs
{
public:
    static void InitClass();
    static void ShutdownClass();

private:
    // PURPOSE: Retrieves the name of the event args.
    // PARAMS:
    //    iEventArgsType - the event args type to get the display name of
    // RETURNS: The display name of the given event args type.
    // NOTES: This will be used when editing the events in various tools.
    static const char* GetPlaybackTypeName( s32 iEventArgsType );

    static const char* c_cutscenePlaybackEventArgsTypeNames[]; // for the static GetTypeName function
};

//##############################################################################

class cutsUpdateEventArgs : public cutsEventArgs
{
public:
    cutsUpdateEventArgs( float fTime, float fDeltaTime, float fSectionStartTime, float fSectionDuration );
    ~cutsUpdateEventArgs();

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Returns the current time of the cut scene.
    // RETURNS: The time in seconds
    float GetSeconds() const;

    // PURPOSE: Returns the time since the last update
    // RETURNS: The time in seconds
    float GetDeltaSeconds() const;

    // PURPOSE: Returns the current time of the cut scene relative to the starting time of the current section.
    // RETURNS: The time in seconds
    float GetSectionSeconds() const;

    // PURPOSE: Returns the start time of the current section
    // RETURNS: The time in seconds
    float GetSectionStartSeconds() const;

    // PURPOSE: Returns the duration of the current section
    // RETURNS: The time in seconds
    float GetSectionDurationSections() const;

    // PURPOSE: Returns the time that the current section ends
    // RETURNS: The time in seconds
    float GetSectionEndSeconds() const;

    // PURPOSE: Returns the percentage progress through the current section
    // RETURNS: A value between 0.0f and 1.0f
    float GetSectionPhase() const;

protected:
    float m_fTime;
    float m_fDeltaTime;
    float m_fSectionStartTime;
    float m_fSectionDuration;
};

inline s32 cutsUpdateEventArgs::GetType() const
{
    return CUTSCENE_UPDATE_EVENT_ARGS_TYPE;
}

inline float cutsUpdateEventArgs::GetSeconds() const
{
    return m_fTime;
}

inline float cutsUpdateEventArgs::GetDeltaSeconds() const
{
    return m_fDeltaTime;
}

inline float cutsUpdateEventArgs::GetSectionSeconds() const
{
    return m_fTime - m_fSectionStartTime;
}

inline float cutsUpdateEventArgs::GetSectionStartSeconds() const
{
    return m_fSectionStartTime;
}

inline float cutsUpdateEventArgs::GetSectionDurationSections() const
{
    return m_fSectionDuration;
}

inline float cutsUpdateEventArgs::GetSectionEndSeconds() const
{
    return m_fSectionStartTime + m_fSectionDuration;
}

inline float cutsUpdateEventArgs::GetSectionPhase() const
{
    return m_fTime / GetSectionEndSeconds();
}

//##############################################################################

// PURPOSE: A class for providing an animation to play and at what time in the
//  animation.
class cutsAnimationEventArgs : public cutsEventArgs
{
public:
    cutsAnimationEventArgs( float fTime, const crAnimation *pAnim );
    ~cutsAnimationEventArgs();

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the time in the animation to play
    // RETURNS: The time in seconds.
    float GetTime() const;

    // PURPOSE: Retrieves the animation to play
    // RETURNS: The animation.
    const crAnimation* GetAnimation() const;

protected:
    float m_fTime;
    const crAnimation *m_pAnimation;
};

inline s32 cutsAnimationEventArgs::GetType() const
{
    return CUTSCENE_ANIMATION_EVENT_ARGS_TYPE;
}

inline float cutsAnimationEventArgs::GetTime() const
{
    return m_fTime;
}

inline const crAnimation* cutsAnimationEventArgs::GetAnimation() const
{
    return m_pAnimation;
}

//##############################################################################

// PURPOSE: A class for providing two animations to play and at what time in the
//  animation.  Typically used to play both a body and a face animation.
class cutsDualAnimationEventArgs : public cutsAnimationEventArgs
{
public:
    cutsDualAnimationEventArgs( float fTime, const crAnimation *pAnim1, const crAnimation *pAnim2 );
    ~cutsDualAnimationEventArgs();

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the secondary animation to play
    // RETURNS: The secondary animation.
    const crAnimation* GetAnimation2() const;

protected:
    const crAnimation *m_pAnimation2;
};

inline s32 cutsDualAnimationEventArgs::GetType() const
{
    return CUTSCENE_DUAL_ANIMATION_EVENT_ARGS_TYPE;
}

inline const crAnimation* cutsDualAnimationEventArgs::GetAnimation2() const
{
    return m_pAnimation2;
}

//##############################################################################

// PURPOSE: A class for providing a clip to play and at what time in the
//  clip.
class cutsClipEventArgs : public cutsEventArgs
{
public:
#if HACK_GTA4
	cutsClipEventArgs( float fTime, const crClip *pClip, const char* pAnimDict );
#else
    cutsClipEventArgs( float fTime, const crClip *pClip );
#endif    
	~cutsClipEventArgs();

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the time in the clip to play
    // RETURNS: The time in seconds.
    float GetTime() const;

    // PURPOSE: Retrieves the clip to play
    // RETURNS: The clip.
    const crClip* GetClip() const;
#if HACK_GTA4
	// PURPOSE: Returns the name of the dictionary
	// RETURNS: The clip.
	const char* GetAnimDict() const;
#endif

protected:
    float m_fTime;
    const crClip *m_pClip;
#if HACK_GTA4
	const char* m_pAnimDict;
#endif
};

inline s32 cutsClipEventArgs::GetType() const
{
    return CUTSCENE_CLIP_EVENT_ARGS_TYPE;
}

inline float cutsClipEventArgs::GetTime() const
{
    return m_fTime;
}

inline const crClip* cutsClipEventArgs::GetClip() const
{
    return m_pClip;
}
#if HACK_GTA4
inline const char* cutsClipEventArgs::GetAnimDict() const 
{
	return m_pAnimDict;
}
#endif
//##############################################################################

// PURPOSE: A class for providing two clips to play and at what time in the
//  clip.  Typically used to play both a body and a face clip animation.
class cutsDualClipEventArgs : public cutsClipEventArgs
{
public:

#if HACK_GTA4
	cutsDualClipEventArgs( float fTime, const crClip *pClip1, const crClip *pClip2, const char* pAnimDict );
#else
    cutsDualClipEventArgs( float fTime, const crClip *pClip1, const crClip *pClip2 );
#endif   
	~cutsDualClipEventArgs();

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the secondary clip to play
    // RETURNS: The secondary clip.
    const crClip* GetClip2() const;

protected:
    const crClip *m_pClip2;
	
};

inline s32 cutsDualClipEventArgs::GetType() const
{
    return CUTSCENE_DUAL_CLIP_EVENT_ARGS_TYPE;
}

inline const crClip* cutsDualClipEventArgs::GetClip2() const
{
    return m_pClip2;
}

//##############################################################################

// PURPOSE: A class for providing a clip and an animation to play and at what 
//    time.  Typically used to play both a body clip and a face animation.
class cutsDualClipAnimEventArgs : public cutsClipEventArgs
{
public:

#if HACK_GTA4
	cutsDualClipAnimEventArgs( float fTime, const crClip *pClip, const crAnimation *pAnimation, const char* pAnimDict );
#else
    cutsDualClipAnimEventArgs( float fTime, const crClip *pClip, const crAnimation *pAnimation );
#endif   
	~cutsDualClipAnimEventArgs();

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the animation to play
    // RETURNS: The animation.
    const crAnimation* GetAnimation() const;

protected:
    const crAnimation *m_pAnimation;
};

inline s32 cutsDualClipAnimEventArgs::GetType() const
{
    return CUTSCENE_DUAL_CLIP_ANIM_EVENT_ARGS_TYPE;
}

inline const crAnimation* cutsDualClipAnimEventArgs::GetAnimation() const
{
    return m_pAnimation;
}

//##############################################################################

// PURPOSE: A class for providing an animation to play and at what time in the
//  animation.
class cutsDictionaryLoadedEventArgs : public cutsEventArgs
{
public:
	cutsDictionaryLoadedEventArgs( crClipDictionary* pDict, s32 iSection );
	~cutsDictionaryLoadedEventArgs();

	// PURPOSE: Retrieves the type of this event args for easy casting
	// RETURNS: An id representing the type
	virtual s32 GetType() const;

	// PURPOSE: Retrieves the dictionary
	// RETURNS: The dictionary .
	crClipDictionary* GetDictionary() const;

	// PURPOSE: Retrieves the anim section the dictionary corresponds to
	// RETURNS: The anim section index
	s32 GetSection() const;

protected:
	crClipDictionary *m_pDict;
	s32 m_iSection;
};

inline s32 cutsDictionaryLoadedEventArgs::GetType() const
{
	return CUTSCENE_DICTIONARY_LOADED_EVENT_ARGS_TYPE;
}

inline crClipDictionary * cutsDictionaryLoadedEventArgs::GetDictionary() const
{
	return m_pDict;
}

inline s32 cutsDictionaryLoadedEventArgs::GetSection() const
{
	return m_iSection;
}

} // namespace rage

#endif // CUTSCENE_CUTSEVENTARGS_H 
