//
// cutscene/cutsoptimisations.h
//
// Copyright (C) 2012 Rockstar Games.  All Rights Reserved.
//`

#ifndef CUTSCENE_CUTSOPTIMISATIONS_H
#define CUTSCENE_CUTSOPTIMISATIONS_H

#define CUTS_OPTIMISATIONS_OFF	0

#if CUTS_OPTIMISATIONS_OFF
#define CUTS_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else
#define CUTS_OPTIMISATIONS()
#endif // CUTS_OPTIMISATIONS_OFF

#endif // CUTSCENE_CUTSOPTIMISATIONS_H
