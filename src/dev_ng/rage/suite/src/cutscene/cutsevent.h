// 
// cutscene/cutsevent.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTSCENE_CUTSEVENT_H 
#define CUTSCENE_CUTSEVENT_H 

#include "cutfile/cutfevent.h"

namespace rage {

//##############################################################################

enum ECutscenePlaybackEvent
{
    CUTSCENE_START_OF_SCENE_EVENT = CUTSCENE_EVENT_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY, // Dispatched at the beginning of the cut scene when playback first begins.
    CUTSCENE_END_OF_SCENE_EVENT,                            // Dispatched at the end of the cut scene when playback is stopped and we are about to unload.
    
    CUTSCENE_PLAY_EVENT,                                    // Start playing the cut scene.
    CUTSCENE_UPDATE_FADING_OUT_AT_BEGINNING_EVENT,          // Update the screen fade entity during the fading out state at the beginning.  cutsUpdateEventArgs
    CUTSCENE_UPDATE_LOADING_EVENT,                          // Update the asset and animation managers and the audio entity during the loading state.
    CUTSCENE_UPDATE_EVENT,                                  // Update the every entity with the current scene time, section time, etc.  cutsUpdateEventArgs
    CUTSCENE_UPDATE_UNLOADING_EVENT,                        // Update the asset and animation managers and the audio entity during the unloading state.
    CUTSCENE_UNLOADED_EVENT,                                // Last event dispatched to the asset and animation managers, the audio entity before releasing them.
    CUTSCENE_PAUSE_EVENT,                                   // Pause the cut scene.
    CUTSCENE_STOP_EVENT,                                    // Stop the cut scene    
    CUTSCENE_CANCEL_LOAD_EVENT,                             // Skip the cut scene during one of the loading states.

    CUTSCENE_SET_ANIMATION_EVENT,                           // cutsAnimationEventArgs
    CUTSCENE_SET_DUAL_ANIMATION_EVENT,                      // cutsDualAnimationEventArgs
    CUTSCENE_SET_CLIP_EVENT,                                // cutsClipEventArgs
    CUTSCENE_SET_DUAL_CLIP_EVENT,                           // cutsDualClipEventArgs
    CUTSCENE_SET_DUAL_CLIP_ANIM_EVENT,                      // cutsDualClipAnimEventArgs

	CUTSCENE_DICTIONARY_LOADED_EVENT,						// A dictionary has just loaded in
	CUTSCENE_SKIPPED_EVENT,									// The player has skipped the cutscene

    // START DEBUG SECTION
    CUTSCENE_STEP_FORWARD_EVENT,                            // Dispatched when the user step's forward
    CUTSCENE_STEP_BACKWARD_EVENT,                           // Dispatched when the user step's backward
    CUTSCENE_FAST_FORWARD_EVENT,                            // Dispatched when the user fast forwards.  cutfFloatValueEventArgs
    CUTSCENE_REWIND_EVENT,                                  // Dispatched when the user rewinds.  cutfFloatValueEventArgs
    CUTSCENE_RESTART_EVENT,                                 // Dispatched when the scene is about to restart (we could be looping).
    CUTSCENE_SET_FACE_ZOOM_EVENT,                           // Tell the camera to zoom in on a particular Ped Model's face.  cutfObjectIdEventArgs
    CUTSCENE_CLEAR_FACE_ZOOM_EVENT,                         // Tell the camera to end the face zoom (and resume playing its animation).  cutfObjectIdEventArgs
    CUTSCENE_SET_FACE_ZOOM_DISTANCE_EVENT,                  // Tell the camera what distance to calculate the face zoom matrix at.  cutfFloatEventArgs
    CUTSCENE_SET_FACE_ZOOM_NEAR_DRAW_DISTANCE_EVENT,        // Tell the camera what the near draw distance to use.  cutfFloatEventArgs
    CUTSCENE_SET_FACE_ZOOM_FAR_DRAW_DISTANCE_EVENT,         // Tell the camera what the far draw distance to use.  cutfFloatEventArgs
    CUTSCENE_SHOW_DEBUG_LINES_EVENT,                        // Tell the visible object to draw debug lines.
    CUTSCENE_HIDE_DEBUG_LINES_EVENT,                        // Tell the visible object to stop drawing debug lines.
    CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT,               // Tell the visible object that the scene offset and/or rotation changed so they can adjust accordingly.
    CUTSCENE_MUTE_AUDIO_EVENT,                              // Tell the audio entity to mute itself
    CUTSCENE_UNMUTE_AUDIO_EVENT,                            // Tell the audio entity to unmute itself
    CUTSCENE_ENABLE_DEPTH_OF_FIELD_EVENT,                   // Tell the camera to enable its depth of field
    CUTSCENE_LAST_PLAYBACK_EVENT,
    CUTSCENE_DISABLE_DEPTH_OF_FIELD_EVENT,						// Tell the camera to disable its depth of field
	CUTSCENE_PLAY_BACKWARDS_EVENT = CUTSCENE_LAST_PLAYBACK_EVENT
    // END DEBUG SECTION
};

//##############################################################################

class cutsEvent : public cutfEvent
{
public:
    static void InitClass();
    static void ShutdownClass();

private:
    // PURPOSE: Determines the display name for the given event id.  This function should be passed into 
    //    cutfEvent::SetEventIdDisplayNameFunc(..).
    // PARAMS:
    //    iEventId - the event id
    // RETURNS: The event id's name.  If the event id is not found, returns "(unknown)".
    static const char* GetPlaybackEventDisplayName( s32 iEventId );

    static const char* c_cutscenePlaybackEventIdDisplayNames[]; // for the static GetDisplayName function
};

//##############################################################################

} // namespace rage

#endif // CUTSCENE_CUTSEVENT_H 
