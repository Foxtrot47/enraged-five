// 
// cutscene/cutsmanager.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTSCENE_CUTSMANAGER_H 
#define CUTSCENE_CUTSMANAGER_H 

#include "cutsentity.h"

#include "atl/bitset.h"
#include "atl/map.h"
#include "cutfile/cutfile2.h"
#include "file/asset.h"
#include "data/base.h"
#include "vector/matrix34.h"

namespace rage {

class bkBank;
class bkText;
class bkCombo;
class bkGroup;
class bkSlider;
class cutfEvent;
class cutfObject;
class cutfCameraObject;

//##############################################################################

#if !__NO_OUTPUT
#define cutsManagerDebugf(fmt,...) if ( (Channel_Cutscene.TtyLevel == DIAG_SEVERITY_DEBUG3) && GetCutfObject() ) { char debugStr[256]; cutsManager::CommonDebugStr(debugStr); cutsDebugf1("%s" fmt, debugStr,##__VA_ARGS__); }
#else
#define cutsManagerDebugf(fmt,...) do {} while(false)
#endif //!__NO_OUTPUT

#if __BANK

struct SEditCutfLightInfo
{
    SEditCutfLightInfo()
        : pObject( NULL )
    {
        Reset();
    }

    void Reset()
    {
        fOriginalIntensity = 100.0f;
        bOverrideIntensity = false;
		bHaveEditedIntensity = false; 
        fIntensity = 100.0f;
        
        fOriginalFallOff = 0.0f;
        bOverrideFallOff = false;
		bHaveEditedFalloff = false; 
        fFallOff = 0.0f;
        
        fOriginalConeAngle = 45.0f;
        bOverrideConeAngle = false;
		bHaveEditedConeAngle = false; 
        fConeAngle = 45.0f;
        
        vOriginalColor.Set( 1.0f, 1.0f, 1.0f );
        bOverrideColor = false;
		bHaveEditedColor = false; 
        vColor.Set( 1.0f, 1.0f, 1.0f );

        vOriginalPosition.Zero();
        bOverridePosition = false;
		bHaveEditedPosition = false; 
        vPosition.Zero();
       
        vOriginalDirection.Set( 0.0f, -1.0f, 0.0f );
        bOverrideDirection = false;
		bHaveEditedDirection = false; 
        vDirection.Set( 0.0f, -1.0f, 0.0f );

		OriginalLightFlags = 0; 
		bOverrideLightFlags = false; 
		bHaveEditedFlags = false; 
		LightFlags = 0; 
	
		OriginalTimeFlags = 16777215; 
		bOverrideTimeFlags = false; 
		bHaveEditedTimeFlags = false; 
		TimeFlags = 16777215; 
		
		OriginalVolumeIntensity = 1.0f; 
		VolumeIntensity = 1.0f; 
		bHaveEditedVolumeIntensity = false; 
		bOverrideVolumeIntensity = false;

		VolumeSizeScale = 1.0f; 
		OriginalVolumeSizeScale = 1.0f; 
		bHaveEditedVolumeSizeScale = false; 
		bOverrideVolumeSizeScale = false; 
		
		OriginalCoronaIntensity = 1.0f; 
		CoronaIntensity = 1.0f; 
		bHaveEditedCoronaIntensity = false; 
		bOverrideCoronaIntensity = false;

		CoronaSize = 1.0f; 
		OriginalCoronaSize = 1.0f; 
		bHaveEditedCoronaSize = false; 
		bOverrideCoronaSize = false; 

		CoronaZbias = 1.0f; 
		OriginalCoronaZbias = 1.0f; 
		bHaveEditedCoronaZBias = false;
		bOverrideCoronaZbias = false; 
		
		ExpoFallOff = 0.0f; 
		OriginalExpoFallOff = 0.0f; 
		bOverrideExpoFallOff = false;
		bHaveEditedExpoFallOff= false;

		InnerConeAngle = 0.0f; 
		OriginalInnerConeAngle = 0.0f ; 
		bOverrideInnerConeAngle = false; 
		bHaveEditedInnerConeAngle= false;

		OuterColourAndIntensity.Set(1.0f); 
		OriginalOuterColourAndIntensity.Set(1.0f); 
		bOverrideOuterColourAndIntensity = false; 
		bHaveEditedOuterColourAndIntensity= false;

		ShadowBlur = 0.0f; 
		OriginalShadowBlur = 0.0f;
		bOverrideShadowBlur = false; 
		bHaveEditedShadowBlur = false; 

		IsActive = false; 

		AuthoringQuat.Identity(); 
		OrginalAuthoringQuat.Identity(); 
		AuthoringPos = VEC3_ZERO; 
		OriginaAuthoringPos = VEC3_ZERO;

		IsAttached = false; 

		WasCreatedInGame = false; 

		HasBeenRenamed = false; 

		CamCutName.Clear(); 

		markedForDelete = false; 
    }

    cutfObject *pObject;
	
	bool IsActive; 
	
    float fOriginalIntensity;
    bool bOverrideIntensity;
    float fIntensity;
	bool bHaveEditedIntensity; 
    
	float fOriginalFallOff;
    bool bOverrideFallOff;
    float fFallOff;
	bool bHaveEditedFalloff; 

    float fOriginalConeAngle;
    bool bOverrideConeAngle;
    float fConeAngle;
	bool bHaveEditedConeAngle; 

    Vector3 vOriginalColor;
    bool bOverrideColor;
    Vector3 vColor;
	bool bHaveEditedColor;

    Vector3 vOriginalPosition;
    bool bOverridePosition;
    Vector3 vPosition;
	bool bHaveEditedPosition; 

    Vector3 vOriginalDirection;
    bool bOverrideDirection;
    Vector3 vDirection;
	bool bHaveEditedDirection; 

	u32 OriginalLightFlags; 
	bool bOverrideLightFlags; 
	u32 LightFlags; 
	bool bHaveEditedFlags; 

	u32 OriginalTimeFlags; 
	bool bOverrideTimeFlags; 
	u32 TimeFlags; 
	bool bHaveEditedTimeFlags; 

	float OriginalVolumeIntensity; 
	float VolumeIntensity; 
	bool bOverrideVolumeIntensity; 
	bool bHaveEditedVolumeIntensity; 

	float VolumeSizeScale; 
	float OriginalVolumeSizeScale; 
	bool  bOverrideVolumeSizeScale; 
	bool bHaveEditedVolumeSizeScale; 

	Vector4 OuterColourAndIntensity; 
	Vector4 OriginalOuterColourAndIntensity; 
	bool bOverrideOuterColourAndIntensity; 
	bool bHaveEditedOuterColourAndIntensity; 

	float OriginalCoronaIntensity; 
	float CoronaIntensity; 
	bool bOverrideCoronaIntensity;
	bool bHaveEditedCoronaIntensity; 

	float CoronaSize; 
	float OriginalCoronaSize ; 
	bool bOverrideCoronaSize; 
	bool bHaveEditedCoronaSize; 

	float CoronaZbias; 
	float OriginalCoronaZbias; 
	bool bOverrideCoronaZbias; 
	bool bHaveEditedCoronaZBias; 

	float ExpoFallOff; 
	float OriginalExpoFallOff; 
	bool bOverrideExpoFallOff; 
	bool bHaveEditedExpoFallOff; 

	float InnerConeAngle; 
	float OriginalInnerConeAngle; 
	bool bOverrideInnerConeAngle; 
	bool bHaveEditedInnerConeAngle; 
	
	float ShadowBlur; 
	float OriginalShadowBlur; 
	bool bOverrideShadowBlur; 
	bool bHaveEditedShadowBlur; 

	atHashString OriginalLightName; 
	bool HasBeenRenamed; 

	Quaternion AuthoringQuat;
	Quaternion OrginalAuthoringQuat;

	Vector3 AuthoringPos; 
	Vector3 OriginaAuthoringPos; 

	bool IsAttached; 
	bool WasCreatedInGame; 

	atHashString CamCutName; 
	bool markedForDelete; 
};

struct SEditCutfDrawDistanceInfo
{
    SEditCutfDrawDistanceInfo()
        : pEvent( NULL )
        , fOriginalNearClip( 0.0f )
        , bOverrideNearClip( false )
        , fNearClip( CUTSCENE_DEFAULT_NEAR_CLIP )
        , fOriginalFarClip( 0.0f )
        , bOverrideFarClip( false )
        , fFarClip( CUTSCENE_DEFAULT_FAR_CLIP )
    {

    }

    const cutfEvent *pEvent;

    float fOriginalNearClip;
    bool bOverrideNearClip;
    float fNearClip;

    float fOriginalFarClip;
    bool bOverrideFarClip;
    float fFarClip;
};

struct SEditCutfObjectLocationInfo
{
    SEditCutfObjectLocationInfo()
        : pObject(NULL)
        , fRadius(1.0f)
    {
        cName[0] = 0;
        vPosition.Zero();
    }

    const cutfObject *pObject;
    char cName[CUTSCENE_OBJNAMELEN];
    Vector3 vPosition;
    float fRadius;
};

//structure for adding a blocking bounds
struct sEditCutfBlockingBoundsInfo
{
	sEditCutfBlockingBoundsInfo()
		: pObject(NULL)
	{	
		fHeight = 0.0f;
		for (int i=0; i<4; i++)
		{
			vCorners[i] = VEC3_ZERO; 
		}
		LocateMat.Identity();
	}
	const cutfObject *pObject;
	char cName[CUTSCENE_OBJNAMELEN];
	float fHeight; 
	Vector3 vCorners[4];

	//Used for editing
	Matrix34 LocateMat; 
	Vector3 vMin; 
	Vector3 vMax; 

};

#endif // __BANK

//##############################################################################
#define SETUP_CUTSCENE_WIDGET_FOR_CONTENT_CONTROLLED_BUILD  0
#define SETUP_CUTSCENE_STREAMING_FOR_CONTENT_CONTROLLED_BUILD 0
#define CUTSCENE_AUTHORIZED_FOR_PLAYBACK (!__FINAL)
#define ALLOW_APPROVAL_RENDER (1) 


// PURPOSE: Base class for a managing a cut scene.  Derived classes need to
//  implement CreateEntity(...).
class cutsManager : public datBase
{
public:
    cutsManager();
    virtual ~cutsManager();

    static void InitClass();

    static void ShutdownClass();
	
	enum ePlayingState
	{
		PLAY_STATE_FORWARDS_NORMAL_SPEED,
		PLAY_STATE_FORWARDS_SCALED, 
		PLAY_STATE_BACKWARDS,
		PLAY_STATE_PAUSED,
		PLAY_STATE_SKIPPING
	};
	
	
	// Keep in sync with c_cutsceneScreenFadeOverrideDisplayNames in cutsManager.cpp
    enum EScreenFadeOverride
    {
        DEFAULT_FADE,   // Use whatever fade was specified in the cut file.
        FORCE_FADE,     // Force the fade
        SKIP_FADE,      // Skip the fade
    };

	// Keep in sync with c_cutsceneStateNames in cutsManager.cpp
	enum ECutsceneState
	{
		CUTSCENE_IDLE_STATE = 0,        // no cut file, scene, models, anims, or audio is loaded                      
		CUTSCENE_LOAD_CUTFILE_STATE,    // start streaming in the cut file
		CUTSCENE_LOADING_CUTFILE_STATE, // wait for the cut file to stream
		CUTSCENE_LOADED_CUTFILE_STATE,  // we've loaded the cutfile
		CUTSCENE_FADING_OUT_STATE,      // wait for the screen to fade out
		CUTSCENE_LOAD_STATE,            // execute the load events
		CUTSCENE_LOADING_STATE,         // wait for the load events to indicate readiness
		CUTSCENE_LOADING_BEFORE_RESUMING_STATE, // wait for assets to load when resuming after the pause state
		CUTSCENE_PLAY_STATE,            // playing the audio and updating the anims and fx
		CUTSCENE_PAUSED_STATE,          // paused
		CUTSCENE_STOPPED_STATE,         // stopped (playing will start from the beginning)
		CUTSCENE_CANCELLING_STATE,      // Cancelled during load, we need to wait for cleanup operations to complete
		CUTSCENE_UNLOAD_STATE,          // release the entities that are no longer needed, tell the remaining entities to unload
#if HACK_GTA4
		CUTSCENE_SKIPPING_STATE,
#endif    
		CUTSCENE_UNLOADING_STATE,       // wait for everything to finish unloading, releasing the remaining entities, then unload the cutfile

#if CUTSCENE_AUTHORIZED_FOR_PLAYBACK
		CUTSCENE_AUTHORIZED_STATE,
#endif
		CUTSCENE_NUM_STATES
	};

    // PURPOSE: Prepares the manager to play the cut scene given by the cut file.
    // PARAMS:
    //    pFilename - the name of the file to load.
    //    pExtension - the file extension, if not already included in pFilename.  If no extension is provided in either place, 
    //      the most up-to-date file between the cutxml and cutbin will be loaded.
    //    bPlayNow - whether to begin playing right away
    //    fadeOutGameAtBeginning - used to override the fade out at the beginning
    //    fadeInCutsceneAtBeginning - used to override the fade in at the beginning
    //    fadeOutCutsceneAtEnd - used to override the fade out at the end
    //    fadeInGameAtEnd - used to override the fade in at the end
    //    bJustCutfile - when true, only the cutfile will be loaded and IsLoaded() will return true.  Call ResumeLoad() to continue.
    // NOTES: If a cut scene is already playing, Unload() will be called
    //    before the new cut scene begins processing.
    virtual void Load( const char* pFilename, const char* pExtension=NULL, bool bPlayNow=true, 
        EScreenFadeOverride fadeOutGameAtBeginning=DEFAULT_FADE, EScreenFadeOverride fadeInCutsceneAtBeginning=DEFAULT_FADE,
        EScreenFadeOverride fadeOutCutsceneAtEnd=DEFAULT_FADE, EScreenFadeOverride fadeInGameAtEnd=DEFAULT_FADE, bool bJustCutfile=false );

    // PURPOSE: Resumes loading the scene after the cutfile has been loaded.  Does nothing if bJustCutfile=false in the call to Load(...).
    virtual void ResumeLoad();

#if !__FINAL
	virtual bool RequiresPreviousAnimDictOnSkip() { return false; }
#endif // !__FINAL

    // PURPOSE: Unloads the current cut scene and its assets to prepare the manager
    //    for playing another.
    // NOTES:  If a cut scene is currently playing or is paused, Stop(...) will be called (with the values for the end 
    //    of scene fades that were passed into Load(...)) before unloading anything.  For finer control over how the scene
    //    is unloaded, call Stop(...).
    virtual void Unload();

    // PURPOSE: If paused or stopped, will start or continue playback.  Dispatches
    //    the CUTSCENE_PLAY_EVENT.
    void Play();

    // PURPOSE: If playing, will pause playback.  Call Play() to resume.  Dispatches
    //    the CUTSCENE_PAUSE_EVENT.
    void Pause();

#if !__FINAL
	
	// PURPOSE: A function that can be used when the calling callback function is activated.
	void PlayBackward(); 
	
		// PURPOSE: Pause if playing, resume playing if paused. Convenience function called by
	// BankPauseResumeCallback() or externally.
	void PauseResume();

    // PURPOSE: If playing or paused, will step forward one frame.  Dispatches
    //    the CUTSCENE_STEP_FORWARD_EVENT.
    void StepForward();

    // PURPOSE: If playing or paused, will step backward one frame.  Dispatches
    //    the CUTSCENE_STEP_BACKWARD_EVENT.
    void StepBackward();

    // PURPOSE: If playing or paused, will cycle to the next fast forward speed.  Dispatches
    //    the CUTSCENE_FAST_FORWARD_EVENT.
    void FastForward();

    // PURPOSE: If playing or paused, will cycle to the next rewind speed.  Dispatches
    //    the CUTSCENE_REWIND_EVENT.
    void Rewind();

    // PURPOSE: If playing or paused, will restart the scene from the beginning.  Dispatches
    //    the CUTSCENE_RESTART_EVENT.
    void Restart();

    // PURPOSE: If playing or paused, will set the Camera Face Zoom to the previous face.  Dispatches
    //    the CUTSCENE_SET_FACE_ZOOM_EVENT and CUTSCENE_CLEAR_FACE_ZOOM_EVENT.
    void PreviousFace();

    // PURPOSE: If playing or paused, will set the Camera Face Zoom to the next face.  Dispatches
    //    the CUTSCENE_SET_FACE_ZOOM_EVENT and CUTSCENE_CLEAR_FACE_ZOOM_EVENT.
    void NextFace();

    // PURPOSE:  Retrieves the number of Ped Models in the current scene.
    // RETURNS: The number of Ped Models
    int GetNumFaces() const;

    // PURPOSE:  Sets the Camera Face Zoom to the Ped Model
    // PARAMS:
    //    index - Any values outside of the range [0, GetNumFaces()) will turn off the Camera Face Zoom
    void SetFace( int index );

    // PURPOSE: Draws the debug text on screen.
    virtual void DrawDebugText();

    // PURPOSE: Draws the scene orientation axis (if enabled) and calls all of the entity's DebugDraw functions (if enabled).
    virtual void DebugDraw();

    // PURPOSE: Indicates when are in the process of restarting the cut scene.
    // RETURNS: true if we are restarting
    bool IsRestarting() const;

	// PURPOSE: Get the current playback rate 
	float GetPlayBackRate() const { return m_fPlaybackRate; }
	
	ePlayingState GetPlayBackState() const { return m_PlayBackState; }

#endif // !__FINAL

    // PURPOSE: If playing or paused, will stop playback and unload the data using the screen fade settings provided.
    //    Dispatches the CUTSCENE_STOP_EVENT.
    // PARAMS: 
    //    fadeOutCutscene - used to override the fade out at the end
    //    fadeInGame - used to override the fade in at the end
    void Stop( EScreenFadeOverride fadeOutCutscene=DEFAULT_FADE, EScreenFadeOverride fadeInGame=DEFAULT_FADE );

    // PURPOSE: Fade to black by dispatching an event to the Screen Fade entity.
    // PARAMS:
    //    color - the color to fade out to
    //    fDuration - the length in seconds
    // RETURNS: true if the event was dispatched, otherwise false.
    virtual bool FadeOut( const Color32 &color, float fDuration );

    // PURPOSE: Fade from black by dispatching an event to the Screen Fade entity.
    // PARAMS:
    //    color - the color to fade out to
    //    fDuration - the length in seconds
    // RETURNS: true if the event was dispatched, otherwise false.
    bool FadeIn( const Color32 &color, float fDuration );

    // PURPOSE: Called from somewhere in the game's main update loop before
    //    PostSceneUpdate().
    virtual void PreSceneUpdate();

    // PURPOSE: Called from somewhere in the game's main update loop after 
    //    PreSceneUpdate().
    // PARAMS:
    //    fDeltaTime - the delta time in seconds
    virtual void PostSceneUpdate( float fDeltaTime );
	
	// PURPOSE: Update the section updates called within an update function
	// PARAMS:
	//    
	void UpdateSectionInfo(); 


    // PURPOSE: Returns the cut file that was last loaded
    // RETURNS: The cut file
    const char* GetCutsceneFilename() const;

    // PURPOSE: Returns the name of the scene that was last loaded
    // RETURNS: The scene name
    const char* GetCutsceneName() const;

    // PURPOSE: Returns the directory where the faces are located.  This is stored in the cut file itself
    // RETURNS: The face directory.
    virtual const char* GetFaceDirectory() const;

    // PURPOSE: Retrieves the current time of the scene
    // RETURNS: The time in seconds
    float GetTime() const;

	float GetInternalTime() const { return m_fInternalTime; }

	// PURPOSE: Retrieves the time of cut scene including any frame range
	// RETURNS: The time in seconds
	float GetCutSceneTimeWithRangeOffset() { return m_fTime + (m_iStartFrame / CUTSCENE_FPS); } 

    // PURPOSE: Retrieves the current time for the audio.
    // RETURNS: The time in seconds
    // NOTES: This may be different than GetSeconds() when the anims have been sectioned.
    float GetAudioSeconds() const;

	// PURPOSE: Set the current time step for the cut scene system
	// RETURNS: The time step in seconds
	void SetDeltaTime(float fTimeStep) { m_fDeltaTime = fTimeStep; } 

	// PURPOSE: Retrieves the current time step of the scene
	// RETURNS: The time step in seconds
	float GetDeltaTime() const;

    // PURPOSE: Returns a string with the current playback time, in terms of the selected status display units.
    // RETURNS: The time as a string
    // NOTES: Recommended usage is for Displayfs.
    const char* GetDisplayTime() const;

    // PURPOSE: Returns the total length of the cutscene.  This will be different, depending if the cutscene anims have been sectioned.
    // RETURNS: The time in seconds.
    float GetTotalSeconds() const;

    // PURPOSE: Indicates that the cut scene manager is not idle
    // RETURNS: true if a cut scene is running
    bool IsRunning() const;

    // PURPOSE: Indicates that a cut scene is playing
    // RETURNS: true if a cut scene is playing
    bool IsPlaying() const;

    // PURPOSE: Indicates that a cut scene is paused
    // RETURNS: true if a cut scene is paused
    bool IsPaused() const;

    // PURPOSE: Returns that a cutscene is fade out at the beginning, before we start loading the assets.
    // RETURNS: true if a cut scene is fading out at the beginning
    bool IsFadingOutAtBeginning() const;

    // PURPOSE: Returns that a cutscene is fade out at the end, before we unloading the assets.
    // RETURNS: true if a cut scene is fading out at the end
    bool IsFadingOutAtEnd() const;

    // PURPOSE: Indicates when a cutfile is loading.
    // RETURNS: true if a cutfile is loading
    bool IsCutfileLoading() const;

    // PURPOSE: Indicates when a cutfile has finished loading.
    // RETURNS: true if a cutfile is loaded
    bool IsCutfileLoaded() const;
    
    // PURPOSE: Indicates that a cut scene is loading
    // RETURNS: true if a cut scene is loading
    virtual bool IsLoading() const;

    // PURPOSE: Indicates when a cut scene has finished loading
    // RETURNS: true if a cut scene is loaded
    bool IsLoaded() const;

    // PURPOSE: Indicates that an object is loading
    // PARAMS:
    //    iObjectId - the object id
    // RETURNS: true if the object is loading
    bool IsLoading( s32 iObjectId ) const;

    // PURPOSE: Informs the cut scene manager that a particular object is loading and 
    //    that we should wait for it to finish before proceeding.
    // PARAMS:
    //    iObjectId - the object id that is ready.
    //    bIsLoading - true or false
    void SetIsLoading( s32 iObjectId, bool bIsLoading );

    // PURPOSE: Indicates when an object has finished loading
    // PARAMS:
    //    iObjectId - the object id.
    // RETURNS: true if a cut scene is loaded
    bool IsLoaded( s32 iObjectId ) const;

    // PURPOSE: Informs the cut scene manager that a particular object is loaded and 
    //    ready to play.
    // PARAMS:
    //    iObjectId - the object id that is ready.
    //    bIsLoaded - true or false
    void SetIsLoaded( s32 iObjectId, bool bIsLoaded );

    // PURPOSE: Indicates that a cut scene is unloading
    // RETURNS: true if a cut scene is unloading
    bool IsUnloading() const;

    // PURPOSE: Indicates that an object is unloading
    // PARAMS:
    //    iObjectId - the object id
    // RETURNS: true if the object is unloading
    bool IsUnloading( s32 iObjectId ) const;

    // PURPOSE: Informs the cut scene manager that a particular object is unloading and 
    //    that we should wait for it to finish before proceeding.
    // PARAMS:
    //    iObjectId - the object id that is ready.
    //    bIsUnloading - true or false
    void SetIsUnloading( s32 iObjectId, bool bIsUnloading );

    // PURPOSE: Indicates when an object has finished unloading
    // PARAMS:
    //    iObjectId - the object id.
    // RETURNS: true if a cut scene is unloaded
    bool IsUnloaded( s32 iObjectId ) const;

    // PURPOSE: Informs the cut scene manager that a particular object is unloaded and 
    //    ready to play.
    // PARAMS:
    //    iObjectId - the object id that is ready.
    //    bIsUnloaded - true or false
    void SetIsUnloaded( s32 iObjectId, bool bIsUnloaded );

    // PURPOSE: Indicates when we are cancelling the cutscene during load.  This will return true from the point that
    // Stop() or Unload() was called during one of the loading states until the cutscene is completely unloaded and 
    // the manager is idle again.
    // RETURNS: True if we're cancelling, otherwise false.
    bool IsCancellingLoad() const;

    // PURPOSE: Indicates the play back has finished and the unload and cleanup has started.  This will return true from
    // the point that Stop() or Unload() is externally called during pause or playback until the scene is complete unloaded and
    // the manager is idle again.
    // RETURNS: True that the scene is terminating
    bool IsTerminating() const;

	// PURPOSE: Indicates the play back is in the process of being skipped, this can be called externally to check if the playback
	// skip has been called.
	// RETURNS: True if the playback skip is in progress. 
	bool IsSkippingPlayback() const; 

    // PURPOSE: Called by an entity that needs to go into the loading state before playback can begin.
    void NeedToLoadBeforeResuming();

	// PURPOSE: Indicates that a cut scene is playing
	// RETURNS: true if a cut scene is playing
	bool IsLoadingBeforeResuming() const;

    // PURPOSE: Retrieves the object with the given id, if it exists
    // PARAMS:
    //    iObjectId - the id of the object
    // RETURNS: the object, NULL if not found
    const cutfObject* GetObjectById( s32 iObjectId ) const;

    // PURPOSE: Retrieves the entity with the given object id, if it exists
    //    iObjectId - the id of the object
    // RETURNS: the entity, NULL if not found
    cutsEntity* GetEntityByObjectId( s32 iObjectId ) const;

	float GetStartTime() const; 

    // PURPOSE: Calculates the start time based whether the cut file's animations have been sectioned
    //    and the current start frame.
    // RETURNS: Time in seconds
    float GetStartTimeFromStartFrame() const;
	u32 GetStartFrame() const;

    // PURPOSE: Retrieves the time that the audio should start at.
    // RETURNS: Time in seconds
    float GetAudioStartTime() const;

    // PURPOSE: Calculates the end time based whether the cut file has been sectioned
    //    and the current end frame.
    // RETURNS: Time in seconds
    float GetEndTime() const;
	
	float GetEndTimeFromEndFrame() const;
	u32 GetEndFrame() const;

    // PURPOSE: Retrieves the time that the audio should end at.
    // RETURNS: Time in seconds
    float GetAudioEndTime() const;

    // PURPOSE: Retrieves the offset of the scene. Physical entities (models, blocking bounds, camera, etc.)
    //    will have to initialize themselves to this position before playback begins.
    // RETURNS: The scene offset
    const Vector3& GetSceneOffset() const;

    // PURPOSE: Retrieves the scene rotation.  Physical entities (models, blocking bounds, camera, etc.)
    //    will have to initialize themselves to this rotation before playback begins.
    // RETURNS: The scene rotation in radians.
    float GetSceneRotation() const;

	float GetScenePitch() const; 

	float GetSceneRoll() const; 

    // PURPOSE: Sets m to be the orientation matrix indicated by GetSceneOffset() and GetSceneRotation().
    // PARAMS:
    //    m - the matrix to set
    void GetSceneOrientationMatrix( Matrix34 &m );

    // PURPOSE: Sets m to be the orientation matrix indicated as it would be at the specified time.  More for concatenated cutscenes than
    //    anything else.
    // PARAMS:
    //    iConcatSection - the concat section to get the orientation matrix for
    //    m - the matrix to set
    void GetSceneOrientationMatrix( int iConcatSection, Matrix34 &m );

	void GetOriginalSceneOrientationMatrix( int iConcatSection, Matrix34 &m );

	void GetOriginalSceneOrientationMatrix( Matrix34 &m );

	void StoreOriginalOrientationData(int concatSection); 

    // PURPOSE: Retrieves the extra room name.
    // RETURNS: The name of the extra room
    const char* GetExtraRoomName() const;

    // PURPOSE: Retrieves the extra room's position
    // RETURNS: The position of the extra room
    const Vector3& GetExtraRoomPosition() const;

    // PURPOSE: Retrieves whether or not Depth of Field is enabled   
    // RETURNS: true or false
    // NOTES:  The default state is false.  Enable on a per-scene basis with the Rag widgets and store in the .cuttune file.
    bool IsDepthOfFieldEnabled() const;

	// PURPOSE: Retrieves whether or not start scene event has been dispatched   
	// RETURNS: true or false
	// NOTES:  Set to false before the start scene event has been dispatched, true afterwards.
	bool IsStartSceneDispatched() const;

    // PURPOSE: Retrieves the list of start times according to the cutfile's settings
    // RETURNS: A float array
    const atArray<float>& GetSectionStartTimeList() const;

    // PURPOSE: Returns the section that we will start the scene with.
    // RETURNS: The starting section number
    int GetStartingSection() const;

    // PURPOSE: Returns the current playback section number.
    // RETURNS: The current section number.
    int GetCurrentSection() const;

    // PURPOSE: Returns the previous playback section number
    // RETURNS: The previous section number.  -1 if the current section is 0.
    int GetPreviousSection() const;

    // PURPOSE: Returns the previous playback section number
    // PARAMS:
    //    iSection - the section to get the previous of
    // RETURNS: The previous section number.  -1 if the current section is 0.
    int GetPreviousSection( int iSection ) const;

    // PURPOSE: Returns the next playback section number
    // RETURNS: The next section number.  -1 if the current section is the last section.
    int GetNextSection() const;

    // PURPOSE: Returns the next playback section number
    // PARAMS:
    //    iSection - the section to get the next of
    // RETURNS: The next section number.  -1 if the current section is the last section.
    int GetNextSection( int iSection ) const;

    // PURPOSE: Returns the section number that encapsulates the time provided.
    // PARAMS:
    //    fTime - the time in seconds
    // RETURNS: The section number.  -1 if the time is past the end of the scene.
    int GetSectionForTime( float fTime ) const;

    // PURPOSE: Returns the start time of playback section.
    // PARAMS: 
    //    iSection i the index of the section
    // RETURNS: Time in seconds.  -1 if iSection is out of range
    float GetSectionStartTime( int iSection ) const;

    // PURPOSE: Returns the start time of the current playback section.
    // RETURNS: Time in seconds
    float GetCurrentSectionStartTime() const;

    // PURPOSE: Returns the elapsed time of the current section.
    // RETURNS: Time in seconds
    float GetCurrentSectionTime() const;

    // PURPOSE: Retrieves the list of start times according to the cutfile's settings
    // RETURNS: A float array
    const atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT>& GetConcatDataList() const;
	
	// PURPOSE: Checks if the incoming data is for a valid concatenated scene.
	// RETURNS: If the concat data is valid for concatted scene
	bool IsConcatDataValid() const; 

    // PURPOSE: Returns the concat section that we will start the scene with.
    // RETURNS: The starting concat section number
    int GetStartingConcatSection() const;

    // PURPOSE: Returns the current playback concat section number.
    // RETURNS: The current concat section number.
    int GetCurrentConcatSection() const;

    // PURPOSE: Returns the previous playback concat section number
    // RETURNS: The previous concat section number.  -1 if the current concat section is 0.
    int GetPreviousConcatSection() const;

    // PURPOSE: Returns the previous playback concat section number
    // PARAMS:
    //    iConcatSection - the concat section to get the previous of
    // RETURNS: The previous concat section number.  -1 if the current concat section is 0.
    int GetPreviousConcatSection( int iConcatSection ) const;

	// PURPOSE: Returns the last valid playback concat section number. Branching means that con cat sections can be invalid.
	// RETURNS: Gets the last concat section that is valid for play back else returns -1; 
	int GetPreviousValidConcatSection(int iConcatSection) const;

    // PURPOSE: Returns the next playback concat section number
    // RETURNS: The next concat section number.  -1 if the current concat section is the last concat section.
    int GetNextConcatSection() const;

	// PURPOSE: Returns the next valid playback concat section number. Branching means that con cat sections can be invalid.
	// RETURNS: Gets the next concat section that is valid for play back else returns -1; 
	int GetNextValidConcatSection(int iConcatSection) const;

    // PURPOSE: Returns the next playback concat section number
    // PARAMS:
    //    iConcatSection - the concat section to get the next of
    // RETURNS: The next concat section number.  -1 if the current concat section is the last concat section.
    int GetNextConcatSection( int iConcatSection ) const;

    // PURPOSE: Returns the concat section number that encapsulates the time provided.
    // PARAMS:
    //    fTime - the time in seconds
    // RETURNS: The concat section number.  -1 if the time is past the end of the scene.
    int GetConcatSectionForTime( float fTime ) const;
	
	// PURPOSE: Checks that the section passed in is in the concat section list
	// PARAMS:
	//    fTime - the time in seconds
	// RETURNS: false if the section is not in the concat section list. 
	bool IsConcatSectionValid(int iConcatSection) const; 
	
	// PURPOSE: Returns start time of concat setion
	// PARAMS:
	//    iConcatSection - index of concat
	// RETURNS: -1.0 if the section is not in the concat section list. 
	float GetConcatSectionStartTime(s32 iSection) const;

	// PURPOSE: Gets the duration of the concat section.
	// PARAMS:
	//    fTime - the time in seconds
	// RETURNS: The length of the concat section is valid
	float GetConcatSectionDuration(s32 iConcatSection) const; 

	// PURPOSE: Returns true if the scene is concatted
	// PARAMS:
	// RETURNS: Returns true if the scene is concatted
	bool IsConcatted() const; 
	
	// PURPOSE: Check if this concat section is valid for playback
	// PARAMS:
	// RETURNS: Returns true if this section is valid for playback.
	bool IsConcatSectionValidForPlayBack(int iConcatSection) const; 

    // PURPOSE: Gets the cutfPedModelObjects.
    // PARAMS:
    //    pedModelObjectList - the list to add the objects to
    void GetModelObjectsOfType( atArray<const cutfObject*> &pedModelObjectList, int ObjectType ) const;

    const cutfEvent* GetFirstEvent( s32 iObjectId, s32 iEventId=-1 ) const;
    const cutfEvent* GetNextEvent( s32 iObjectId, s32 iEventId=-1 ) const;
	
	// PURPOSE: Converts the four points and a corner into an matrix and two vectors
	// PARAMS:
	void ConvertAngledAreaToMatrixAndVectors(Vector3 StoredCorners[], float fHeight, Vector3 &vMin, Vector3 &vMax, Matrix34 &LocateMat, float fCreationTime);
	
	// PURPOSE: Get const access to the cutfile outside the cut manager. 
	// PARAMS:
	const cutfCutsceneFile2* GetCutfFile() const; 

	bool IsFlagSet(u32 flags, u32 flag) const { return (flags & flag)!=0; }

	// PURPOSE: Set the cutscene position
	void SetSceneOffset(const Vector3& offset);

	// PURPOSE: Set the cutscene heading
	void SetSceneRotation(float heading);

	void SetScenePitch(float pitch);

	void SetSceneRoll(float roll); 

#if __BANK
	bool IsIsolating() const { return m_bIsolateSelectedPed; }
#endif

	static bool IsBankOpen();

protected:

    const cutfEvent* GetEventInternal( int iStartingIndex, s32 iObjectId, s32 iEventId ) const;

	void SetInternalTime(float time) { m_fInternalTime = time; } 
	
	void SetTime(float time) {m_fTime = time; }

	void IntialiseTimers(float InternalTime, float Time) {SetInternalTime(InternalTime);  SetTime(Time);  }

    // PURPOSE: Clears all of the data as both an initialization and cleanup step
    virtual void Clear();

    struct SEntityObject
    {
        SEntityObject()
            : pEntity(NULL)
            , pObject(NULL)
            , bIsLoading(false)
            , bIsLoaded(false)
            , bIsUnloading(false)
            , bIsUnloaded(false)
        {

        }
		
	
        cutsEntity* pEntity;
        const cutfObject* pObject;
        bool bIsLoading:1;
        bool bIsLoaded:1;
        bool bIsUnloading:1;
        bool bIsUnloaded:1;
    };

    // PURPOSE: Loads the cut file
    // PARAMS:
    //    pFilename - the cut file to load.  Typically, this will be m_cCutfileToLoad
    // RETURNS: true if it was loaded (or queued to load), false otherwise
    // NOTES:  The only reason you should have to override this function is if you 
    //    aren't loading the file from disc, a mounted drive, or any other device.
    virtual bool LoadCutFile( const char* pFilename );

    // PURPOSE: Process the idle state
    virtual void DoIdleState();

    // PURPOSE: Process the load cutfile state
    virtual void DoLoadCutfileState();

    // PURPOSE: Process the loading cutfile state
    virtual void DoLoadingCutfileState();

    // PURPOSE: Process the fading out state
    virtual void DoFadingOutState();

    // PURPOSE: Process the load state
    virtual void DoLoadState();

    // PURPOSE: Process the loading state
    virtual void DoLoadingState();

    // PURPOSE: Process the loading before resuming state
    virtual void DoLoadingBeforeResumingState();

    // PURPOSE: Process the play state
    virtual void DoPlayState();

    // PURPOSE: Process the paused state
    virtual void DoPausedState();

    // PURPOSE: Process the stopped state
    virtual void DoStoppedState();

    // PURPOSE: Process the cancelling state
    virtual void DoCancellingState();

    // PURPOSE: Process the unload state
    virtual void DoUnloadState();

    // PURPOSE: Process the unloading state
    virtual void DoUnloadingState();

	//PURPOSE: Special case skipping state that can return the game to any point. 
	virtual void DoSkippingState() {}; 
	
	//PURPOSE: Dispatches the playback events as well as an update event.
	virtual void  UpdatePlayBackEvents(); 	

	//PURPOSE: A state allows something specific to happen before running the scene.  
	virtual void DoAuthorizedState() {}; 

    // PURPOSE: Dispatches the event
    // PARAMS:
    //    pEvent - the event to dispatch
    virtual void DispatchEvent( const cutfEvent *pEvent );

    // PURPOSE: Dispatches the event that is opposite
    // PARAMS:
    //    pEvent - the event to dispatch
    virtual void DispatchOppositeEvent( const cutfEvent *pEvent );

    // PURPOSE: Dispatches an event, given by iEventId, to all known entities.
    // PARAMS:
    //    iEventId - the id of the event to dispatch
    //    pEventArgs - the event args to accompany the event.
    virtual void DispatchEventToAllEntities( s32 iEventId, const cutfEventArgs *pEventArgs=NULL );

    // PURPOSE: Dispatches the opposite of an event, given by iEventId, to all known entities.
    // PARAMS:
    //    iEventId - the id of the event to dispatch
    //    pEventArgs - the event args to accompany the event.
    virtual void DispatchOppositeEventToAllEntities( s32 iEventId, const cutfEventArgs *pEventArgs=NULL );

    // PURPOSE: Reserves an entity for use by the cut scene manager that corresponds 
    //    to the given cut scene object.
    // PARAMS:
    //    pObject - the object to return a cut scene entity for
    // RETURNS: A cut scene entity.
    // NOTES: Depending on your setup, you may be creating an entity here, or merely 
    //    returning the pointer to one you've already created.  In addition, a single
    //    cut scene entity can be responsible for receiving the events for more than
    //    one cut scene object.
    virtual cutsEntity* ReserveEntity( const cutfObject* pObject ) = 0;

    // PURPOSE: Releases an entity that the cut scene manager no longer needs
    // PARAMS:
    //    pEntity - the entity to release
    //    pObject - the entity's associated object.  For the case of "singleton"
    //      entities, the entity itself should never be deleted until the last 
    //      cut scene object that is owns is released.
    virtual void ReleaseEntity( cutsEntity *pEntity, const cutfObject* pObject ) = 0;

    // PURPOSE: Since we sometimes force screen fades, we may need to dispatch events
    //    to the Screen Fade object regardless if there are any events to this object
    //    in the cut file.
    // PARAMS:
    //    bCreateIfNotFound - true if it is ok to create a new screen fade object if
    //      one is not found
    // RETURNS: the object id of the Screen Fade object, -1 if not found
    s32 FindScreenFadeObjectId( bool bCreateIfNotFound );

    // PURPOSE: Returns the object id of the asset manager
    // RETURNS: The object id of the asset manager, -1 if not found
    s32 FindAssetManagerObjectId();

    // PURPOSE: Returns the object id of the audio object
    // RETURNS: The object id of the audio object, -1 if not found
    s32 FindAudioObjectId();

public:
    // PURPOSE: Returns the object id of the camera object
    // RETURNS: The object id of the camera object, -1 if not found
    s32 FindCameraObjectId();

    // PURPOSE: Retrieves the fade out duration when a FORCE_FADE out is applied.
    // RETURNS: Time in seconds
    virtual float GetDefaultFadeOutDuration() const;

    // PURPOSE: Retrieves the fade in duration when a FORCE_FADE in is applied.
    // RETURNS: Time in seconds
    virtual float GetDefaultFadeInDuration() const;

    // PURPOSE: Retrieves the fade color when a FORCE_FADE is applied.
    // PARAMS: The color
    virtual Color32 GetDefaultFadeColor() const;

    // PURPOSE: Retrieves the value to use as the default Near Draw Distance when the Create For Camera Cuts button is clicked.     
    // RETURNS: The default Near Draw Distance
    // NOTES: The Rage default is CUTSCENE_DEFAULT_NEAR_CLIP.  Override this function to provide your own game-specific default.
    virtual float GetDefaultNearDrawDistance() const;

    // PURPOSE: Retrieves the value to use as the default Far Draw Distance when the Create For Camera Cuts button is clicked.     
    // RETURNS: The default Far Draw Distance
    // NOTES: The Rage default is CUTSCENE_DEFAULT_FAR_CLIP.  Override this function to provide your own game-specific default.
    virtual float GetDefaultFarDrawDistance() const;

	atMap<u32, Vector4>* m_pOriginalSceneMat;

	bool HasFadeOutAtEndBeenCalled() const { return m_bFadedOutCutsceneAtEnd; }
	bool HasFadeInAtEndBeenCalled() const  { return m_bHasFadeInGameAtEndBeenCalled; }
protected:
    // PURPOSE: Retrieves the SEntityObject for the given cutfObject.
    // PARAMS:
    //    pObject - the object
    //    bCreateIfNotFound - when true, if no SEntityObject is found, CreateEntity is 
    //      called and a new SEntityObject is created and added to the map.
    // RETURNS: the entity object  
    SEntityObject* GetEntityObject( const cutfObject *pObject, bool bCreateIfNotFound=true );

    // PURPOSE: Deletes the SEntityObject for the given cutfObject, if it exists.  ReleaseEntity
    //    is called to release the associated entity.
    // PARAMS:
    //    pObject - the object
    void DeleteEntityObject( const cutfObject *pObject );

    // PURPOSE: Finds the object ids that will receive events in the cutfEvent list (or 
    //    not in the list, if bInvert is true).
    // PARAMS:
    //    pEventList - the event list to find the object ids for
    //    iObjectIdList - the list to put the found object ids into
    //    bInvert - when true, iObjectIdList will contain the object ids NOT in pEventList
    void GetObjectIdsInEventList( const atArray<cutfEvent*>& pEventList, atArray<s32>& iObjectIdList, bool bInvert=false ) const;

    // PURPOSE: Fades out the game at the beginning if our overrides or the cutfile data direct us to.
    // PARAMS:
    //    screenFadeOverride - the override to use
    // RETURNS: The return value of FadeOut(...) if it is called, otherwise false.
    bool FadeOutGameAtBeginning( EScreenFadeOverride screenFadeOverride );

    // PURPOSE: Fades in the cut scene at the beginning if our overrides or the cutfile data direct us to.
    // PARAMS:
    //    screenFadeOverride - the override to use
    // RETURNS: The return value of FadeIn(...) if it is called, otherwise false.
    bool FadeInCutsceneAtBeginning( EScreenFadeOverride screenFadeOverride );

    // PURPOSE: Fades out the cut scene at the end if our overrides or the cutfile data direct us to.
    // PARAMS:
    //    screenFadeOverride - the override to use
    //    bStopping - set to true when we want the fade to occur regardless of the current playback time.  Will still check the settings, though.
    // RETURNS: The return value of FadeOut(...) if it is called, otherwise false.
    bool FadeOutCutsceneAtEnd( EScreenFadeOverride screenFadeOverride, bool bStopping=false );

    // PURPOSE: Fades in the game at the end if our overrides or the cutfile data direct us to.
    // PARAMS:
    //    screenFadeOverride - the override to use
    // RETURNS: The return value of FadeIn(...) if it is called, otherwise false.
    bool FadeInGameAtEnd( EScreenFadeOverride screenFadeOverride );

    // PURPOSE: Calculates the new current time using the deltaTime and the default method for the current platform.
    // PARAMS:
    //    fDeltaTime - the game update time
    // RETURNS: The new current time
    float GetDefaultTime( float fDeltaTime );

#if !__FINAL
    // PURPOSE: Calculates the new current time using the deltaTime, the current playback rate, and for the current platform.
    // PARAMS:
    //    fDeltaTime - the game update time
    // RETURNS: The new current time
    float GetDefaultVCRTime( float fDeltaTime );
#endif // !__FINAL

#if __BANK
    // PURPOSE: Calculates the new current time using the deltaTime, the current playback rate, the timestep source, and for the current platform.
    // PARAMS:
    //    fDeltaTime - the game update time
    // RETURNS: The new current time
    float GetTimestepSourceVCRTime( float fDeltaTime );
#endif // __BANK

    // PURPOSE: Retrieves the current time of the audio stream.
    // RETURNS: -1.0f if there is no audio stream, or some another error occurred
    float GetAudioEntityTime();

	void SetSkipFrame(u32 iFrame) { m_SkipFrame = iFrame; } 

#if HACK_GTA4
	void SetCutfFile (cutfCutsceneFile2* pCutfile);
#else
	 cutfCutsceneFile2 m_cutsceneFile;
#endif	

	cutfCutsceneFile2* GetCutfFile();
	
	enum EStreamingState
	{
		STREAMING_IDLE_STATE,               // nothing is streaming
		STREAMING_CUTFILE_STATE,    // loading the cut file
		STREAMING_ASSETS_STATE,     // loading scene, models, and/or audio
	};

	enum EStatusUnits
	{
		SECONDS,
		FRAMES,
	};

	atArray<float> m_fSectionStartTimeList;
	atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> m_concatDataList;
   
	char m_cCutfileToLoad[RAGE_MAX_PATH];
	char m_cExtraRoomName[CUTSCENE_OBJNAMELEN];
	char m_cCurrentSceneName[CUTSCENE_LONG_OBJNAMELEN];
	char m_cDisplayTime[16];

    atMap<s32, SEntityObject> m_cutsceneEntityObjects;
	Vector3 m_vExtraRoomPos;
    Vector3 m_vSceneOffset;

	float m_fTime;
	float m_fInternalTime;
    float m_fFadeOutEndTime;
    float m_fDeltaTime;
    float m_fSectionStartTime;
    float m_fSectionDuration;
	float m_fStartTime; 
	float m_fEndTime; 
	float m_fSceneRotation;
	float m_fScenePitch; 
	float m_fSceneRoll; 

#if __BANK
    Vector3 m_vBankSceneOffset;
	float m_fBankSceneRotation;
	float m_fBankScenePitch; 
	float m_fBankSceneRoll; 

	int m_iSelectedIsolatePedIndex;
	bool m_bIsolateSelectedPed;
#endif //__BANK
	
	EScreenFadeOverride m_fadeOutGameAtBeginning;
	EScreenFadeOverride m_fadeInCutsceneAtBeginning;
	EScreenFadeOverride m_fadeOutCutsceneAtEnd;
	EScreenFadeOverride m_fadeInGameAtEnd;
	EScreenFadeOverride m_fadeInGameAtEndWhenStopping;
	ECutsceneState m_cutsceneState;
	ePlayingState m_PlayBackState; 
	EStreamingState m_streamingState;

#if !__FINAL
	ePlayingState m_PreviousPlayBackState; 
#endif
	
	int m_iCurrentSection;
    int m_iCurrentConcatSection;
    s32 m_iNextEventIndex;
	s32 m_iNextLoadEvent; //keep the index into the load events 
	s32 m_iNextUnloadEvent; 

    s32 m_iStartFrame;
    s32 m_iEndFrame;
	int m_SkipFrame; //The frame to be skipped too.
	int m_iStatusUnits;

	bool m_bDepthOfFieldEnabled;	
	bool m_bDispatchedStartScene;
	bool m_bForceAudioSync;
	bool m_bPlayNow;
	bool m_bJustLoadCutfile;
	bool m_bCutfileStreamed;
	bool m_bAssetsStreamed;
	bool m_bIsStoppingForEndFadeOut;
	bool m_bSkippingPlayback; 
	bool m_bIsCancellingLoad;
	bool m_bIsTerminating;
	bool m_bReachedPlayBackEnd;
	bool m_bNeedToLoadBeforeResuming;
	bool m_bResumingPlayFromPause; 
	bool m_bFadedOutCutsceneAtEnd;
    bool m_bHasFadeInGameAtEndBeenCalled; 
#if !__FINAL
    atArray<const cutfObject*> m_pedModelObjectList;

    float m_fFaceZoomDistance;
    float m_fFaceZoomNearDrawDistance;
    float m_fFaceZoomFarDrawDistance;
	float m_fStatusDisplayX;
	float m_fStatusDisplayY;
	float m_fPlaybackRate;
    
	int m_iTimestepSource;
    int m_iSelectedPedModelIndex;

    bool m_bDisplayStatus;    
    bool m_bDisplayMemoryUsage;
    bool m_bDisplayDebugLines;
    bool m_bDisplayBlockingBoundsLines;
    bool m_bDisplayRemovalBoundsLines;
    bool m_bDisplayCameraLines;
    bool m_bDisplayLightLines;
    bool m_bDisplayModelLines;
    bool m_bDisplayParticleEffectLines;
    bool m_bDisplaySceneOrigin;
	bool m_bDisplayHiddenLines; 
	bool m_bAudioSyncWhenJogging; 
	bool m_bAllowAudioSyncFromRag; 
	bool m_bGeneratingLoadAudioEventNotFromData; 
	bool m_bIsStepping;
	bool m_bStepped;
	bool m_bIsRestarting;
#endif // !__FINAL

private: 
#if HACK_GTA4	
	cutfCutsceneFile2* m_pCutSceneFile;
#endif


public:
#if !__NO_OUTPUT
	// PURPOSE: Returns the state of the cutscene manager.
	ECutsceneState GetState() { return m_cutsceneState; }

	// PURPOSE: Returns the a cutscene state enum as a string.
	const char * GetStateName(ECutsceneState state);
#endif

#if !__FINAL
	enum eScrubbingState
	{
		SCRUBBING_STEPPING_FORWARDS,
		SCRUBBING_STEPPING_BACKWARDS, 
		SCRUBBING_FAST_FORWARDING, 
		SCRUBBING_REWINDING, 
		SCRUBBING_PLAYING_BACKWARDS, 
		SCRUBBING_PAUSING,
		SCRUBBING_FROM_LOOPING,
		SCRUBBING_FORWARDS_FROM_TIME_LINE_BAR,
		SCRUBBING_BACKWARDS_FROM_TIME_LINE_BAR, 
		SCRUBBING_NONE
	};
	
	bool IsResumingPlaybackFromBeingPaused() { return m_bResumingPlayFromPause; } 

	eScrubbingState GetScrubbingState() const { return m_ScrubbingState; }

protected:
	eScrubbingState m_ScrubbingState; 
	int m_iCurrentFrameWithFrameRanges; 

#endif

	public:
#if __BANK

	public:

    // PURPOSE: Adds the widgets for controlling and debugging a cut scene.
    // PARAMS:
    //    bank - the bank to add to.
    virtual void AddWidgets( bkBank &bank );

    // PURPOSE: Removes the widgets for controlling and debugging a cut scene.
    void RemoveWidgets();

	// PURPOSE: Removes the widgets for controlling and debugging a cut scene.
	void CreateVCRWidget(bkBank * pBank);


    // PURPOSE: Retrieve the SEditCutfLightInfo for the specified object id.  You must manually override the 
    //    light animation to view the debug light settings.
    // PARAMS:
    //    iObjectId - the id of the object.
    // RETURNS: NULL if not found
    SEditCutfLightInfo* GetEditLightInfo( s32 iObjectId ) const;

	SEditCutfLightInfo* GetSelectedLightInfo() const; 

	SEditCutfLightInfo* GetActiveLightInfo() const; 

	void SetLightPosition(const Vector3& pos) { m_vLightPosition = pos; }
	void SetLightOrientation(const Vector3& rot) { m_vLightOrientation = rot; }

	// PURPOSE:  Returns the index in the cutscene light list.  
	int GetLightIndex(const char* lightName);

    // PURPOSE: Adds a hidden object to our scene.
    // PARAMS:
    //    pObjectLocationInfo - the hidden object information
    virtual bool AddHiddenObject( SEditCutfObjectLocationInfo *pObjectLocationInfo );

    // PURPOSE: Removes a hidden object from our scene.
    // PARAMS:
    //    pObjectLocationInfo - the hidden object information
    virtual void RemoveHiddenObject( SEditCutfObjectLocationInfo *pObjectLocationInfo );

    // PURPOSE: Retrieves a new Hidden Object SEditCutfObjectLocationInfo for the currently selected item in the game world.
    // RETURNS: An SEditCutfObjectLocationInfo instance, or NULL if nothing should be added.
    // NOTES: This function is used by the bank widgets.
    virtual SEditCutfObjectLocationInfo* GetHiddenObjectInfo() = 0;

    // PURPOSE: Adds a hidden object to our scene.
    // PARAMS:
    //    pObjectLocationInfo - the hidden object information
    virtual bool AddFixupObject( SEditCutfObjectLocationInfo *pObjectLocationInfo );

    // PURPOSE: Removes a hidden object from our scene.
    // PARAMS:
    //    pObjectLocationInfo - the hidden object information
    virtual void RemoveFixupObject( SEditCutfObjectLocationInfo *pObjectLocationInfo );

    // PURPOSE: Retrieves a new Fixup Object SEditCutfObjectLocationInfo for the currently selected item in the game world.
    // RETURNS: An SEditCutfObjectLocationInfo instance, or NULL if nothing should be added.
    // NOTES: This function is used by the bank widgets.
    virtual SEditCutfObjectLocationInfo* GetFixupObjectInfo() = 0;

	// PURPOSE: Adds a hidden object to our scene.
	// PARAMS:
	//    pObjectLocationInfo - the hidden object information
	virtual bool AddBlockingBoundObject( sEditCutfBlockingBoundsInfo *pObjectLocationInfo );

	// PURPOSE: Removes a hidden object from our scene.
	// PARAMS:
	//    pObjectLocationInfo - the hidden object information
	virtual void RemoveBlockingBoundObject( sEditCutfBlockingBoundsInfo *pObjectLocationInfo );

	// PURPOSE: Retrieves a new BlockingBound Object sEditCutfBlockingBoundsInfo for the currently selected item in the game world.
	// RETURNS: An sEditCutfBlockingBoundsInfo instance, or NULL if nothing should be added.
	// NOTES: This function is used by the bank widgets.
	virtual sEditCutfBlockingBoundsInfo* GetBlockingBoundObjectInfo() = 0;

	// PURPOSE: 
	// RETURNS: 
	// NOTES: This function is used by the bank widgets.
	virtual void SetScrubbingControl(u32 UNUSED_PARAM(CurrentFrame)) {};

	bool IsAudioLoadEventNotComingFromData() const { return m_bGeneratingLoadAudioEventNotFromData; }

	virtual void CreateLightCB() {};
	virtual void DuplicateLightCB() {};
	virtual void DuplicateLightFromCameraCB() {};
	virtual void DeleteLightCB() {};
	virtual void RenameLightCB() {};
	virtual void ActivateCameraTrackingCB() {};
	virtual void SnapLightToCamera() {}; 
	virtual void SnapCameraToLight() {}; 
	virtual void UpdateActiveLightSelectionHistory() {}; 

protected:
	enum ETimestepSource
	{
		DEFAULT_TIMESTEP,
		GAME_TIMESTEP,
		AUDIO_TIMESTEP
	};


	bool m_bEditSceneOrigin;

	Vector4 m_vVolumeOuterColourAndIntensity; 

	Vector3 m_vLightColor;
	Vector3 m_vLightPosition;
	Vector3 m_vLightDirection;
	Vector3 m_vLightOrientation; 
	Vector3 m_vBlockingBoundPos; 
	Vector3 m_vBlockingBoundRot;
	Vector3 m_LocateMatRot; 
    
	atArray<SEditCutfLightInfo *> m_editLightList;
	atArray<atHashString> m_deletedLightList;
	atArray<SEditCutfDrawDistanceInfo *> m_editDrawDistanceList;
	atArray<SEditCutfObjectLocationInfo *> m_editHiddenObjectList;
	atArray <cutfEvent*> m_HiddenObjectEvents;	//Array of events
	atArray<SEditCutfObjectLocationInfo *> m_editFixupObjectList;
	atArray <cutfEvent*> m_FixupObjectEvents;	//Array of events
	atArray<sEditCutfBlockingBoundsInfo *> m_editBlockingBoundObjectList;
	atArray <cutfEvent*> m_BlockingBoundObjectEvents;
	atArray<SEditCutfLightInfo *> m_activeLightList;
	
	struct SelectionIndexCamCuts
	{
		s32 selectionIndex; 
		atHashString camCut; 
	};
	
	atArray<SelectionIndexCamCuts> ActiveLightSelectionList; 



	float m_fVolumeIntensity;
	float m_fVolumeSizeScale; 
	float m_fCoronaSize; 
	float m_fCoronaIntensity; 
	float m_fCoronaZBias; 
	float m_fLightIntensity;
	float m_fLightFallOff;
	float m_fLightExponentFallOff; 
	float m_fLightConeAngle;
	float m_fInnerConeAngle;
	float m_fShadowBlur; 
	float m_fCurrentPhase; 
	float m_fDrawDistanceNearClip;
	float m_fDrawDistanceFarClip;
	float m_fBoundingBoxWidth; 
	float m_fBoundingBoxHeight; 
	float m_fBoundingBoxLength;  

	const cutfCameraObject *m_pCameraObject;
	
	bkSlider* m_pPlayBackSlider; 
	bkSlider* m_pPhasePlayBackSlider; 
	bkSlider* m_pLoopStartSlider;
	bkSlider* m_pLoopEndSlider;

	bkSlider* m_pFacePlayBackSlider; 
	bkSlider* m_pLightPlayBackSlider; 
	bkSlider* m_pCameraPlayBackSlider; 
	bkSlider* m_pHiddenObjPlayBackSlider; 
	bkSlider* m_pFixupObjPlayBackSlider; 
	bkSlider* m_pBlockingBoundPlayBackSlider; 
	bkSlider* m_pPhaseSlider; 
	bkCombo *m_pEditFaceCombo;
	bkCombo *m_pIsolatePedCombo;
	bkBank *m_pBank;
	bkGroup *m_pCurrentSceneGroup;
	bkCombo *m_pEditLightCombo;
	bkCombo *m_pActiveLightCombo; 
	bkGroup *m_pEditLightGroup;
	bkCombo *m_pEditDrawDistanceCombo;
	bkGroup *m_pEditDrawDistanceGroup;
	bkSlider *m_pEditDrawDistanceNearClipSlider;
	bkSlider *m_pEditDrawDistanceFarClipSlider; 
	bkCombo *m_pEditHiddenObjectCombo;
	bkCombo *m_pEditHiddenObjectEvents; 
	bkGroup *m_pEditHiddenObjectGroup;
	bkCombo *m_pEditFixupObjectCombo;
	bkGroup *m_pEditFixupObjectGroup;
	bkCombo* m_pEditFixupObjectEvents; 
	bkCombo *m_pEditBlockingBoundObjectCombo;
	bkGroup *m_pEditBlockingBoundObjectGroup;
	bkCombo* m_pEditBlockingBoundObjectEvents; 
	bkText* m_pEditLightStatusText; 
	bkText* m_pLightHotloadingStatusText; 

	static bool s_bIsBankOpen;

	s32 m_TextureDictID; 
	s32 m_TextureKey; 



#if __PPU
	int m_iAudioResyncTime;
#endif

	int m_iLoopStartFrame;
	int m_iLoopEndFrame;
	int m_iAudioOffset;
	int m_iSelectedLightIndex;
	int m_iSelectedDrawDistanceIndex;
	int m_iSelectedHiddenObjectIndex;
	int m_iSelectedHiddenObjectEventIndex; 
	int m_iSelectedFixupObjectIndex;
	int m_iSelectedFixupObjectEventIndex; 
	int m_iSelectedBlockingBoundObjectIndex;
	int m_iSelectedBlockingBoundObjectEventIndex; 
	int m_iActiveLightIndex; 

	char m_LightEditStatus[128]; 
	char m_LightHotLoadingStatus[256]; 

	u32 m_LightFlags; 
	u32 m_LightTimeFlags; 

	int m_createdLightTypeIndex; 
	char m_createdLightName[256];
	char m_duplicatedLightName[256]; 
	char m_LightNewName[256]; 
	bool m_LightCastStaticObjectShadowFlag; 
	bool m_LightCastDynamicObjectShadowFlag;
	bool m_LightCalcFromSunFlag; 
	bool m_LightEnableBuzzingFlag; 
	bool m_LightForceBuzzingFlag; 
	bool m_LightDrawVolumeFlag; 
	bool m_lightNoSpecularFlag; 
	bool m_LightCoronaOnlyFlag;
	bool m_LightBothIntAndExtFlag; 
	bool m_LightNotInReflection; 
	bool m_LightOnlyInReflection; 
	bool m_LightReflector;
	bool m_LightDiffuser;
	bool m_LightCalcByAmbientFlag;  
	bool m_LightFlagsDontLightAlpha; 
	bool m_LightUseAsPedLight; 
	bool m_UseIntensityAsMultiplier; 
	bool m_IsPedOnlyLight;

	bool m_bMuteAudio;
	bool m_bPauseAtEnd;
	bool m_ActivateCameraDebugging; 
	bool m_bUpdateLightWithCamera; 
	bool m_debugActiveState; 

	bool m_LightHour1;
	bool m_LightHour2;
	bool m_LightHour3;
	bool m_LightHour4;
	bool m_LightHour5;
	bool m_LightHour6;
	bool m_LightHour7;
	bool m_LightHour8;
	bool m_LightHour9;
	bool m_LightHour10;
	bool m_LightHour11;
	bool m_LightHour12;
	bool m_LightHour13;
	bool m_LightHour14;
	bool m_LightHour15;
	bool m_LightHour16;
	bool m_LightHour17;
	bool m_LightHour18;
	bool m_LightHour19;
	bool m_LightHour20;
	bool m_LightHour21;
	bool m_LightHour22;
	bool m_LightHour23;
	bool m_LightHour24; 
	bool m_bLoop;
	bool m_bActivateBlockingObjectEditing; 
	bool m_bRenderBlockingBoundObjects; 
	bool m_bScrubbingFromLooping; 
	bool m_HoldAtEnd; 
#if HACK_GTA4
	bool m_bRenderHiddenObjects;
	bool m_ActivateMapObjectEditing;
	bool m_FixupMapObject; 
#endif

	u8 m_uFlashiness; 
	



	// PURPOSE: Returns a screen fade enum as a string.  For debug use only!
	const char * GetScreenFadeOverrideName(EScreenFadeOverride screenFadeoverride);

    // PURPOSE: Creates the widgets for the current scene
    virtual void AddSceneWidgets();
    
    // PURPOSE: Builds a list of SEditCutfLightInfo structures so the lights may be edited.
    void PopulateLightList();

	void PopulateLightEditInfo(); 

	void AddLightEditInfo(cutfObject* pObject); 

	void AddLightToActiveList(SEditCutfLightInfo* pLightinfo);
	
	SEditCutfLightInfo* DuplicateLightEditInfo(cutfObject* pObject, SEditCutfLightInfo *pEditLightInfo); 

	void InialiseLightEditInfo(SEditCutfLightInfo *pEditLightInfo); 

	void UpdateLightCombo(); 

	virtual void PopulateActiveLightList(bool UNUSED_PARAM(forceRepopulateList = false)) {}; 

	void UpdateActiveLightListCombo(); 

    // PURPOSE: Creates the SEditDrawDistance objects for each event or camera cut
    void PopulateDrawDistanceList();

    // PURPOSE: Builds a list of SEditCutfObjectLocationInfo structures for our pre-existing Hidden Objects.
    void PopulateHiddenObjectList();

    // PURPOSE: Builds a list of SEditCutfObjectLocationInfo structures for our pre-existing Fixup Objects.
    void PopulateFixupObjectList();
	
	// PURPOSE: Builds a list of SEditCutfObjectLocationInfo structures for our pre-existing Fixup Objects.
	void PopulateBlockingBoundsList(); 

    // PURPOSE: Stores the current orientation in the cutfile data structure.
    void SaveSceneOrientation();

    // PURPOSE: Stores the current Depth Of Field setting in the cutfile data structure.
    void SaveDepthOfField();

    // PURPOSE: Stores the current playback range in the cutfile data structure.
    void SavePlaybackRange();

    // PURPOSE: Stores the current draw distances in the cutfile data structure.
    void SaveDrawDistances();

    // PURPOSE: Stores the current hidden objects in the cutfile data structure.
    void SaveHiddenObjects();

    // PURPOSE: Stores the current fixup objects in the cutfile data structure.
    void SaveFixupObjects();
	
	 // PURPOSE: Stores the current blocking bounds objects in data structure.
	void SaveBlockingBoundObjects(); 

    // PURPOSE: Save the current cutfile data structure to disk.  Derived classes
    //    should override this to implement custom saving.
    virtual void SaveCutfile();

	//PURPOSE: Custom function for dispatching the opposite ped variation events
	void DispatchOppositeVariationEvent(const cutfEvent *pEvent, atArray<cutfEvent*> &objectEventList); 

    void DispatchEventToObjectType( s32 iObjectType, s32 iEventId, cutfEventArgs *pEventArgs=NULL );

    void DispatchSetDrawDistanceEvent( int iIndex );
	
	virtual void SetFrameRangeForPlayBackSliders(); 

    void BankSceneOrientationChangedCallback();
    void BankSaveSceneOrientationCallback();
    void BankToggleDepthOfFieldCallback();
    void BankSaveDepthOfFieldCallback();

    void BankStartTimeChangedCallback();
    void BankEndTimeChangedCallback();
    void BankSavePlaybackRangeCallback();

    void BankPauseCallback();
    void BankPlayForwardsCallback();
    void BankPlayBackwardsCallback();
    void BankStepForwardCallback();
    void BankStepBackwardCallback();
    void BankFastForwardCallback();
    void BankRewindCallback();
    void BankRestartCallback();

    void BankLoopStartTimeChangedCallback();
    void BankLoopEndTimeChangedCallback();

	void BankFrameScrubbingCallback(); 
	void BankPhaseScrubbingCallback(); 

    void BankStatusDisplayUnitsChangedCallback();
    void BankDisplayDebugLinesChangedCallback();
    void BankDisplayBlockingBoundsLinesChangedCallback();
    void BankDisplayRemovalBoundsLinesChangedCallback();
    void BankDisplayCameraLinesChangedCallback();

	void BankDisplayLightLinesChangedCallback();
    void BankDisplayModelLinesChangedCallback();
    void BankDisplayParticleEffectLinesCallback();
	void BankDisplayHiddenObjectLinesCallback(); 
    void BankMuteAudioCallback();

    void BankFaceZoomDistanceChangedCallback();
    void BankFaceZoomNearDrawDistanceChangedCallback();
    void BankFaceZoomFarDrawDistanceChangedCallback();
    void BankFaceSelectedCallback();
	void BankIsolateSelectedCallback();
    
    virtual void BankLightSelectedCallback();
	void BankActiveLightSelectedCallBack();
    void BankLightOverrideChangedCallback();
    void BankLightValueChangedCallback();
	void BankLightDirectionChangedCallBack(); 
	void BankLightPositionChangedCallBack(); 
	void BankWorldSpaceToOriginalSceneSpace(const Matrix34& World, Matrix34& Local);
	void BankOriginalSceneSpaceToWorldSpace(const Matrix34& Local, Matrix34& World);
    void BankResetSelectedLightCallback();
	void BankUpdateLightFlagsCallback();
	void BankUpdateLightTimeFlagsCallback(); 
	void UpdateFlagsRagWidgets(bool &RagValue, u32 flags, u32 flag);
	void UpdateRagForLightTimeFlags(); 
	void UpdateRagForLightFlags(); 
	void BankLightOverrideTimeFlagsChangedCallback(); 
	void BankLightOverrideFlagsChangedCallback(); 

	void ClearFlag(u32& flags, u32 flag) { flags &= ~flag; }
	void UpdateDebugFlag(u32& LightInfoFlag, u32 CutsceneFlagToChange, bool RagBoolWidget, u32 &Flags); 
	void UpdateLightEditInfo(); 

	void CreateDuplicateLightName(const atString& lightName); 

    void BankResetAllDrawDistancesCallback();
    void BankSetToDefaultAllDrawDistancesCallback();
	void BankSetToUseTimecycleAllDrawDistancesCallback(); 
    void BankSaveDrawDistancesCallback();
    void BankDrawDistanceSelectedCallback();
    void BankDrawDistanceOverrideChangedCallback();
    void BankDrawDistanceValueChangedCallback();
    void BankResetSelectedDrawDistanceCallback();
    void BankSetToDefaultDrawDistancesCallback();
	void BankUseTimeCycleValuesCallback();
    void BankAddHiddenObjectCallback();
    void BankRemoveHiddenObjectCallback();
    void BankHiddenObjectSelectedCallback();
	void BankPopulateHiddenObjectEventList(const cutfObject* pObject);
	void BankAddHiddenShowEvent(); 
	void BankAddHiddenHideEvent(); 
	void BankHiddenRemoveEvent(); 
	
	virtual void SaveDrawDistanceFromWidget(); 

    void BankAddFixupObjectCallback();
    void BankRemoveFixupObjectCallback();
	void BankPopulateFixupObjectEventList(const cutfObject* pObject); 
    void BankFixupObjectSelectedCallback();
	void BankAddFixupEvent(); 
	void BankRemoveFixupEvent(); 

//edit blocking bounds
	void BankAddBlockingBoundObjectCallback();
	void BankRemoveBlockingBoundObjectCallback();
	void BankPopulateBlockingBoundObjectEventList(const cutfObject* pObject); 
	void BankBlockingBoundObjectSelectedCallback();
	void BankAddAddBlockingBoundEvent(); 
	void BankAddRemoveBlockingBoundEvent(); 
	void BankRemoveBlockingBoundEvent(); 
	void BankRenameBlockingBoundObjectCallback();

	void FixUpPlayBackEventIndices(bool hasChangedPlayBackDirection); 
	void FixUpLoadEventIndices(bool hasChangedPlayBackDirection);
	void FixUpUnloadEventIndices(bool hasChangedPlayBackDirection); 
	void FixUpEventIndicesForPlayBackDirectionChange(); 

#else
public:

protected:

#endif // __BANK

#if !__FINAL
    // PURPOSE: Sets the coordinates at which the status display appears.  The 
    //    memory display will appear below this.
    // PARAMS:
    //    x - the x screen coordinate
    //    y - the y screen coordinate
    void SetStatusDisplayCoords( float x, float y );

    // PURPOSE: Displays the status message at the coordinates (m_fStatusDisplayX, m_fStatusDisplayY).
    virtual void DisplayStatus();

    // PURPOSE: Displays the memory usage by all objects in the scene
    virtual void DisplayMemoryUsage();

    // PURPOSE: Draws the origin of the scene
    virtual void DrawSceneOrigin();

    // PURPOSE: Builds a list of objects that are ped models with face animations for the Face Zoom Viewer.
    void PopulatePedModelFaceList();
#endif // !__FINAL

public:
#if !__NO_OUTPUT
	void CommonDebugStr(char * debugStr);
#endif //!__NO_OUTPUT
};

inline const char* cutsManager::GetCutsceneFilename() const
{
    return m_cCutfileToLoad;
}

inline const char* cutsManager::GetCutsceneName() const
{
    return m_cCurrentSceneName;
}

inline float cutsManager::GetTime() const
{
    return m_fTime;
}

inline float cutsManager::GetDeltaTime() const
{
	return m_fDeltaTime;
}

inline const char* cutsManager::GetDisplayTime() const
{
    return m_cDisplayTime;
}

inline float cutsManager::GetTotalSeconds() const
{
	if(GetCutfFile())
	{
		return GetCutfFile()->GetTotalDuration();
	}
	return 0.0f; 
	
}

inline bool cutsManager::IsRunning() const
{
    return m_cutsceneState != CUTSCENE_IDLE_STATE;
}

inline bool cutsManager::IsPlaying() const
{
    return m_cutsceneState == CUTSCENE_PLAY_STATE;
}

inline bool cutsManager::IsPaused() const
{
    return m_cutsceneState == CUTSCENE_PAUSED_STATE;
}

inline bool cutsManager::IsFadingOutAtBeginning() const
{
    return (m_cutsceneState == CUTSCENE_FADING_OUT_STATE) && !m_bIsStoppingForEndFadeOut;
}

inline bool cutsManager::IsFadingOutAtEnd() const
{
    return (m_cutsceneState == CUTSCENE_FADING_OUT_STATE) && m_bIsStoppingForEndFadeOut;
}

inline bool cutsManager::IsCutfileLoading() const
{
    return (m_cutsceneState == CUTSCENE_LOAD_CUTFILE_STATE) 
        || ((m_cutsceneState == CUTSCENE_LOADING_CUTFILE_STATE) && (m_streamingState == STREAMING_CUTFILE_STATE));
}

inline bool cutsManager::IsCutfileLoaded() const
{
    return m_bCutfileStreamed;
}

inline bool cutsManager::IsLoaded() const
{
    return m_bAssetsStreamed;
}

inline bool cutsManager::IsCancellingLoad() const
{
    return m_bIsCancellingLoad;
}

inline bool cutsManager::IsSkippingPlayback() const
{
	return m_bSkippingPlayback; 
}

inline bool cutsManager::IsTerminating() const
{
	return m_bIsTerminating; 
}

inline void cutsManager::NeedToLoadBeforeResuming()
{
    m_bNeedToLoadBeforeResuming = true;
}

inline bool cutsManager::IsLoadingBeforeResuming() const
{
	return m_cutsceneState==CUTSCENE_LOADING_BEFORE_RESUMING_STATE;
}

inline const Vector3& cutsManager::GetSceneOffset() const
{
    return m_vSceneOffset;
}

inline float cutsManager::GetSceneRotation() const
{
    return m_fSceneRotation * DtoR;
}

inline float cutsManager::GetScenePitch() const
{
	return m_fScenePitch * DtoR;
}

inline float cutsManager::GetSceneRoll() const
{
	return m_fSceneRoll * DtoR;
}


inline const char* cutsManager::GetExtraRoomName() const
{
    return m_cExtraRoomName;
}

inline const Vector3& cutsManager::GetExtraRoomPosition() const
{
    return m_vExtraRoomPos;
}

inline bool cutsManager::IsDepthOfFieldEnabled() const
{
    return m_bDepthOfFieldEnabled;
}

inline bool cutsManager::IsStartSceneDispatched() const
{
	return m_bDispatchedStartScene;
}

inline const atArray<float>& cutsManager::GetSectionStartTimeList() const
{
    return m_fSectionStartTimeList;
}

inline int cutsManager::GetCurrentSection() const
{
    return m_iCurrentSection;
}

inline int cutsManager::GetPreviousSection() const
{
    return GetPreviousSection( m_iCurrentSection );
}

inline int cutsManager::GetNextSection() const
{
    return GetNextSection( m_iCurrentSection );
}

inline float cutsManager::GetCurrentSectionStartTime() const
{
    return m_fSectionStartTime;
}

inline float cutsManager::GetCurrentSectionTime() const
{
    return Max<float>( 0.0f, GetTime() - GetCurrentSectionStartTime() );
}

inline const atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT>& cutsManager::GetConcatDataList() const
{
    return m_concatDataList;
}

inline int cutsManager::GetCurrentConcatSection() const
{
    return m_iCurrentConcatSection;
}

inline int cutsManager::GetPreviousConcatSection() const
{
    return GetPreviousConcatSection( m_iCurrentConcatSection );
}

inline int cutsManager::GetNextConcatSection() const
{
    return GetNextConcatSection( m_iCurrentConcatSection );
}

inline const cutfEvent* cutsManager::GetFirstEvent( s32 iObjectId, s32 iEventId ) const
{
    return GetEventInternal( 0, iObjectId, iEventId );
}

inline const cutfEvent* cutsManager::GetNextEvent( s32 iObjectId, s32 iEventId ) const
{
    return GetEventInternal( m_iNextEventIndex + 1, iObjectId, iEventId );
}

inline float cutsManager::GetDefaultFadeOutDuration() const
{
    return CUTSCENE_DEFAULT_FADE_TIME;
}

inline float cutsManager::GetDefaultFadeInDuration() const
{
    return CUTSCENE_DEFAULT_FADE_TIME;
}

inline Color32 cutsManager::GetDefaultFadeColor() const
{
    return Color32( 0, 0, 0, 255 );
}

inline float cutsManager::GetDefaultNearDrawDistance() const
{
    return CUTSCENE_DEFAULT_NEAR_CLIP;
}

inline float cutsManager::GetDefaultFarDrawDistance() const
{
    return CUTSCENE_DEFAULT_FAR_CLIP;
}

#if !__FINAL

inline int cutsManager::GetNumFaces() const
{
    return m_pedModelObjectList.GetCount();
}

inline bool cutsManager::IsRestarting() const
{
    return m_bIsRestarting;
}

inline void cutsManager::SetStatusDisplayCoords( float x, float y )
{
    m_fStatusDisplayX = x;
    m_fStatusDisplayY = y;
}



#endif // !__FINAL

} // namespace rage

#endif // CUTSCENE_CUTSMANAGER_H 
