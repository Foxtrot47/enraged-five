// 
// cutscene/cutsboundsentity.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "cutsboundsentity.h"

#include "cutsoptimisations.h"
#include "cutsevent.h"
#include "cutseventargs.h"
#include "cutsmanager.h"
#include "cutfile/cutfobject.h"
#include "grcore/debugdraw.h"
#include "grcore/im.h"

CUTS_OPTIMISATIONS()

namespace rage {

//#############################################################################

cutsBoundsEntity::cutsBoundsEntity()
: cutsUniqueEntity()
, m_bIsActive(false)
{
}

cutsBoundsEntity::cutsBoundsEntity( const cutfObject* pObject )
: cutsUniqueEntity(pObject)
, m_bIsActive(false)
{
#if !__FINAL
    const cutfBlockingBoundsObject *pBoundsObject = dynamic_cast<const cutfBlockingBoundsObject *>( pObject );

    // bottom
    m_vertices[0][0] = pBoundsObject->GetCorner( 0 );
    m_vertices[0][1] = pBoundsObject->GetCorner( 1 );

    m_vertices[1][0] = pBoundsObject->GetCorner( 2 );
    m_vertices[1][1] = pBoundsObject->GetCorner( 3 );

    m_vertices[2][0] = pBoundsObject->GetCorner( 0 );
    m_vertices[2][1] = pBoundsObject->GetCorner( 2 );

    m_vertices[3][0] = pBoundsObject->GetCorner( 1 );
    m_vertices[3][1] = pBoundsObject->GetCorner( 3 );

    // top
    for ( int i = 4; i < 8; ++i )
    {
        for ( int j = 0; j < 2; ++j )
        {
            m_vertices[i][j] = m_vertices[i-4][j];
            m_vertices[i][j].SetY( m_vertices[i][j].GetY() + pBoundsObject->GetHeight() );
        }
    }

    // sides
    m_vertices[8][0] = m_vertices[0][0];
    m_vertices[8][1] = m_vertices[4][0];

    m_vertices[9][0] = m_vertices[0][1];
    m_vertices[9][1] = m_vertices[4][1];

    m_vertices[10][0] = m_vertices[1][0];
    m_vertices[10][1] = m_vertices[5][0];

    m_vertices[11][0] = m_vertices[1][1];
    m_vertices[11][1] = m_vertices[5][1];
#endif // !__FINAL
}

cutsBoundsEntity::~cutsBoundsEntity()
{

}

void cutsBoundsEntity::DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs,  const float UNUSED_PARAM(fTime), const u32 UNUSED_PARAM(StickyId) )
{
    switch ( iEventId )
    {
#if !__FINAL
    case CUTSCENE_START_OF_SCENE_EVENT:
    case CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT:
        {
            pManager->GetSceneOrientationMatrix( m_sceneOrientation );
        }
        break;
#endif // !__FINAL
    case CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT:
    case CUTSCENE_ACTIVATE_REMOVAL_BOUNDS_EVENT:
        {
            m_bIsActive = true;
        }
        break;
    case CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT:
    case CUTSCENE_DEACTIVATE_REMOVAL_BOUNDS_EVENT:
        {
            m_bIsActive = false;
        }
        break;
    default:
        break;
    }

    cutsUniqueEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

#if !__FINAL

void cutsBoundsEntity::DebugDraw() const
{
	bool bOldLighting = grcLighting( false );

    grcWorldMtx( m_sceneOrientation );

    Color32 c;
    if ( IsActive() )
    {
        c = Color32( 0.0f, 0.9f, 0.0f );
    }
    else
    {
        c = Color32( 0.9f, 0.0f, 0.0f );
    }

    for ( int i = 0; i < 12; ++i )
    {
        grcDrawLine( m_vertices[i][0], m_vertices[i][1], c );
    }

    grcColor( Color32( 0.9f, 0.9f, 0.9f ) );
    grcDrawLabel( m_vertices[0][0] + m_sceneOrientation.d, GetObject()->GetDisplayName().c_str() );

	grcLighting( bOldLighting );
}

#endif // !__FINAL

//#############################################################################

} // namespace rage
