// 
// cutscene/cutsanimentity.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutsanimentity.h"

#include "cutsoptimisations.h"
#include "cutschannel.h"
#include "cutsevent.h"
#include "cutseventargs.h"
#include "cutsmanager.h"

#include "cranimation/animation.h"
#include "cranimation/animdictionary.h"
#include "cranimation/animplayer.h"
#include "cranimation/framefilters.h"
#include "crclip/clip.h"
#include "crclip/clipdictionary.h"
#include "creature/creature.h"
#include "creature/component.h"
#include "creature/componentcamera.h"
#include "creature/componentlight.h"
#include "creature/componentptfx.h"
#include "creature/componentskeleton.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/motiontreescheduler.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/nodeclip.h"
#include "crmotiontree/observer.h"
#include "crmotiontree/requestanimation.h"
#include "crmotiontree/requestclip.h"
#include "crmotiontree/requestfilter.h"
#include "crmotiontree/requestmerge.h"
#include "crmetadata/properties.h"
#include "crmetadata/propertyattributes.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfobject.h"
#include "diag/tracker.h"
#include "file/asset.h"
#include "file/device.h"
#include "grcore/debugdraw.h"
#include "paging/rscbuilder.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxmanager.h"
#include "system/param.h"
#include "system/timemgr.h"

CUTS_OPTIMISATIONS()

namespace rage {

#if !__FINAL
PARAM( cutsIgnoreDicts, "[cutscene] Set to ignore cutscene dictionaries and load the raw clips and animations.  Good for a cutscene viewer." );
bool cutsAnimationManagerEntity::ms_KeepExtraDictionaries = false; 
#endif // !__FINAL

//##############################################################################

static int GetFileIndex( const char* pFilename )
{
    char file[RAGE_MAX_PATH];
    safecpy( file, pFilename, sizeof(file) );

    char *pExtension = const_cast<char *>( ASSET.FindExtensionInPath( file ) );
    if ( pExtension == NULL )
    {
        pExtension = &(file[strlen(file)]);
    }

    if ( pExtension != NULL )
    {
        *pExtension = 0;

        while ( (pExtension >= file) && (*pExtension != '-') )
        {
            --pExtension;
        }

        if ( (pExtension >= file) && (*pExtension == '-') )
        {
            ++pExtension;

            int len = (int) strlen( pExtension );
            for ( int i = 0; i < len; ++i )
            {
                if ( (*pExtension < '0') || (*pExtension > '9') )
                {
                    return -1;
                }
            }

            return atoi( pExtension );
        }
    }

    return -1;
}

//##############################################################################

cutsAnimationManagerEntity::cutsAnimationManagerEntity( const cutfObject* pObject )
: cutsUniqueEntity(pObject)
, m_bAnimsAreCompressed(false)
, m_iCurrentAnimSection(-1)
{
    m_cDictionaryBaseName[0] = 0;
}

cutsAnimationManagerEntity::~cutsAnimationManagerEntity()
{
	m_ObjectsToBeAnimated.Reset();   
    m_objectIdToAnimKeyMap.Kill();
    m_objectIdToClipKeyMap.Kill();
	
	for(s32 i=0; i < m_Dictionaries.GetCount(); i++)
	{
		m_Dictionaries[i].pAnimDictionary = NULL; 
		m_Dictionaries[i].pClipDictionary = NULL;
	}

	m_Dictionaries.Reset(); 
}

void cutsAnimationManagerEntity::RequestAnimDictionaryforSection(cutsManager *pManager,const cutfObject* pObject, s32 iSection )
{
	if(iSection >= 0 && iSection < m_Dictionaries.GetCount() )	
	{
		if( (!(m_Dictionaries[iSection].bAnimDictIsResource || m_Dictionaries[iSection].bClipDictIsResource)) &&m_Dictionaries[iSection].bValidForLoading )
		{
			char dictName[CUTSCENE_LONG_OBJNAMELEN];	
			if(pManager->GetCutsceneName())
			{
				if ( GetSectionedName( dictName, CUTSCENE_LONG_OBJNAMELEN, pManager->GetCutsceneName(), 
					iSection, pManager->GetSectionStartTimeList().GetCount() ) )
				{
					if ( !LoadDictionary( pManager, pObject->GetObjectId(), dictName, iSection ) )
					{
						cutsErrorf( "Unable to load second animation dictionary.  Cancelling cutscene." );

						pManager->Unload();
					}
				}
			}
		}
	}
}

bool cutsAnimationManagerEntity::IsSectionValidToLoad(s32 CurrentSection) const
{
	if(CurrentSection >= 0 && CurrentSection < m_Dictionaries.GetCount())
	{
		return m_Dictionaries[CurrentSection].bValidForLoading; 
	}

	return false; 
}

s32 cutsAnimationManagerEntity::GetNextValidSectionToLoad(s32 CurrentSection)
{
	if(CurrentSection >= 0 && CurrentSection < m_Dictionaries.GetCount() )
	{

		CurrentSection += 1; 

		if(CurrentSection < m_Dictionaries.GetCount() )	
		{
			for(int i = CurrentSection; i < m_Dictionaries.GetCount(); i++)
			{
				if(m_Dictionaries[i].bValidForLoading)
				{
					return i; 				
				}
			}
		}	
	}
	return -1; 
}

s32 cutsAnimationManagerEntity::GetPreviousValidSectionToLoad(s32 CurrentSection)
{
	if(CurrentSection >= 0 && CurrentSection < m_Dictionaries.GetCount()) //check that the section tested is valid
	{
		CurrentSection -=1; 
		
		if(CurrentSection >= 0)	
		{
			for(int i = CurrentSection; i >= 0; i--)
			{
				if(m_Dictionaries[i].bValidForLoading)
				{
					return i; 				
				}
			}
		}	
	}

	return -1; 
}

void cutsAnimationManagerEntity::ReleaseUnusedAnimDictionaries(cutsManager *pManager, int iSection, int iReserveSection )
{
	//go through all the dictionaries and unload any that are not needed	

	s32 CurrentSection = iSection; 
	s32 NextSection = GetNextValidSectionToLoad(CurrentSection);  
	s32 NextNextSection = GetNextValidSectionToLoad(NextSection);

//#if	 !__FINAL && !CUTSCENE_DEBUG_FINAL	
//	s32 PrevSection = GetPreviousValidSectionToLoad(iSection); 
//#endif

	for (int i = 0; i < m_Dictionaries.GetCount(); i++)
	{	
		// dont stream out the previous dict
#if !__FINAL && !CUTSCENE_DEBUG_FINAL				
		if(ms_KeepExtraDictionaries)					
		{
			s32 PrevSection = GetPreviousValidSectionToLoad(iSection); 

			if(i != PrevSection && i != CurrentSection && i != NextSection && i != NextNextSection && i != iReserveSection)
			{
				UnloadDictionary( pManager, i ); 
			}
		}
		else
		{
			if(i != CurrentSection && i != NextSection && i != NextNextSection && i != iReserveSection)
			{
				UnloadDictionary( pManager, i ); 
			}
	
		}
#else
			if(i != CurrentSection && i != NextSection && i != NextNextSection && i != iReserveSection)
			{
				UnloadDictionary( pManager, i ); 
			}
	
#endif
//#if	 !__FINAL && !CUTSCENE_DEBUG_FINAL
//		if(i != PrevSection && i != CurrentSection && i != NextSection && i != NextNextSection && i != iReserveSection)
//		{
//			UnloadDictionary( pManager, i ); 
//		}
//#else
	
//#endif
	}
}

#if __ASSERT
bool ShouldIgnoreObject(const cutfObject* pObject)
{
	if (pObject->GetType()==CUTSCENE_LIGHT_OBJECT_TYPE || pObject->GetType()==CUTSCENE_PARTICLE_EFFECT_OBJECT_TYPE)
	{
		return true;
	}
	else
	{
		return false;
	}
}
#endif //__ASSERT

const crClip* cutsAnimationManagerEntity::GetCameraAnimForTime(const cutfObject* pObject, float CurrentTime, cutsManager* pManager)
{
	u32 currentSection = pManager->GetSectionForTime(CurrentTime);  

	const crClip *pClip = NULL; 
	
	if(pObject->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE)
	{
		const cutfCameraObject* pCam = static_cast<const cutfCameraObject*>(pObject); 
		
		u32 AnimBase = pCam->GetAnimStreamingBase(); 

		u32 FinalHashString = 0; 

		if (GetSectionedName( AnimBase, FinalHashString, currentSection, pManager->GetSectionStartTimeList().GetCount()))	
		{
			pClip = GetClipUsingAnimSection( FinalHashString, currentSection );
		}

	}
	return pClip; 
}


void cutsAnimationManagerEntity::DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, 
    const cutfEventArgs* pEventArgs,  const float UNUSED_PARAM(fTime), const u32 UNUSED_PARAM(StickyId))
{
    RAGE_TRACK( cutsAnimationManagerEntity_DispatchEvent );

    switch ( iEventId )
    {
    case CUTSCENE_RESTART_EVENT:
        {
            if ( NOTFINAL_ONLY( !PARAM_cutsIgnoreDicts.Get() && ) (pManager->GetSectionStartTimeList().GetCount() > 1) )
            {
                ClearAllEntityAnimations( pManager );
                UnloadAllDictionaries( pManager );
            }
            else
            {
                // If we're playing raw animation and/or we have only 1 section, we don't need to unload and reload the dictionaries
                return;
            }
        }

        // fallthru
    case CUTSCENE_LOAD_ANIM_DICT_EVENT:
        {
			//This implementation streams dictionaries based on the current section, but unlike the non gta4 version
			//it creates a buffer which has the same number of sections for the scene. This allows for easy streaming of dictionaries
			//as well as giving the ability to jump backwards and forwards easily.
			//This event state has been refactored so that it can be dispatched multiple times and not just once as in the previous implementation.

			//allocate the buffer of streaming dictionaries if its empty
			
			if(m_Dictionaries.GetCount() == 0)
			{
				const atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> &concatDataList = pManager->GetConcatDataList();
				const atArray<float>& SectionTimeList = pManager->GetSectionStartTimeList();

#if __ASSERT
				cutsDebugf3("Anim Manager - Streaming Anim Section Summary");
				for (int i = 0 ; i < SectionTimeList.GetCount(); i++)
				{
					if(pManager->GetSectionStartTimeList().GetCount() > 1)
					{
						float SectionStartTime = SectionTimeList[i]; 
						float SectionEndTime; 

						if(i < SectionTimeList.GetCount() -1)
						{
							SectionEndTime = SectionTimeList[i+1];
						}
						else
						{
							SectionEndTime = pManager->GetTotalSeconds(); 
						}		
						cutsDebugf3("Anim Manager - Anim Section[%d] = (%f, %f)", i, SectionStartTime, SectionEndTime); 
					}
				}

				for ( int j = 0; j < concatDataList.GetCount();  j++ )
				{
					float ConcatSectionEndTime; 
					float ConcatSectionStartTime = concatDataList[j].fStartTime; 

					if(j == concatDataList.GetCount() - 1 )
					{
						ConcatSectionEndTime = pManager->GetTotalSeconds();
					}
					else
					{
						ConcatSectionEndTime = concatDataList[j+1].fStartTime; 
					}
					cutsDebugf3("Anim Manager - Concat Section [%d] = (%f, %f)",  j, ConcatSectionStartTime, ConcatSectionEndTime);
				}
#endif //__ASSERT

				for (int i = 0 ; i < SectionTimeList.GetCount(); i++)
				{
					SDictionaryData NewBufferEntry; 
					
					//There is only one section so we dont have any streaming issues, just load the one dictionary	
					//bool isValid = false; 
					if(pManager->GetSectionStartTimeList().GetCount() > 1)
					{
						float SectionStartTime = SectionTimeList[i]; 
						float SectionEndTime; 

						if(i < SectionTimeList.GetCount() -1)
						{
							SectionEndTime = SectionTimeList[i+1];
						}
						else
						{
							SectionEndTime = pManager->GetTotalSeconds(); 
						}
						
						if(concatDataList.GetCount() > 0)
						{
							for ( int j = concatDataList.GetCount() - 1; j >= 0; --j )
							{
								float ConcatSectionEndTime; 
								float ConcatSectionStartTime = concatDataList[j].fStartTime; 

								if(j == concatDataList.GetCount() - 1 )
								{
									ConcatSectionEndTime = pManager->GetTotalSeconds();
								}
								else
								{
									ConcatSectionEndTime = concatDataList[j+1].fStartTime; 
								}
								
								//This to make sure that anim sections are not streamed for invalid concat sections. It validates the anim sections if they 
								//match concat sections in length, if they are longer than the concat sections or shorter the concate section.
								bool SectionsComparedToConcats = ((SectionStartTime >= ConcatSectionStartTime) && (SectionStartTime < ConcatSectionEndTime))
									|| ((SectionEndTime > ConcatSectionStartTime) && (SectionEndTime < ConcatSectionEndTime)); 

								bool ConcatsComparedToSections = ((ConcatSectionStartTime >= SectionStartTime) && (ConcatSectionStartTime < SectionEndTime)) 
									||((ConcatSectionEndTime > SectionStartTime) && (ConcatSectionEndTime < SectionEndTime)); 
								
								if(SectionsComparedToConcats || ConcatsComparedToSections)
								{
									if(concatDataList[j].bValidForPlayBack)
									{
										cutsDebugf3("Anim Manager - Streaming Anim Section:%d %f, %f concat section: %d, %f, %f, isValid True", i, SectionStartTime, SectionEndTime, j, ConcatSectionStartTime, ConcatSectionEndTime); 
										NewBufferEntry.bValidForLoading = true; 
										break; 
									}	
									cutsDebugf3("Anim Manager - Not streaming Anim Section:%d %f, %f concat section: %d, %f, %f, isValid: %d ", i, SectionStartTime, SectionEndTime, j, ConcatSectionStartTime, ConcatSectionEndTime, (u32) concatDataList[j].bValidForPlayBack); 
								}				
							}
						}
						else
						{
							NewBufferEntry.bValidForLoading = true; 
						}
					}
					else
					{
						NewBufferEntry.bValidForLoading = true;
					}
					m_Dictionaries.PushAndGrow(NewBufferEntry); 
				}
			}
		
			
			//Can  if these 
			//args are present, a skip to point in scene has been called. The args represent the target section and the section its come from. 
			if(pEventArgs && pEventArgs->GetType() == CUTSCENE_TWO_FLOAT_VALUES_EVENT_ARGS_TYPE)				
			{
				const cutfTwoFloatValuesEventArgs* pSectionArgs = static_cast<const cutfTwoFloatValuesEventArgs*>(pEventArgs); 		
				s32 iSectionSkippedFrom = (s32) pSectionArgs->GetFloat1(); 	
				s32 iSectionToSkipTo = (s32) pSectionArgs->GetFloat2(); 
				
				m_iCurrentAnimSection = iSectionSkippedFrom;   

				//need to check that the section we are going to is valid ie a valid concat section or any section
				// sure i can work out from the concat section list a valid section list
				if(iSectionToSkipTo == iSectionSkippedFrom)
				{
					return;
				}

#if	 !__FINAL && !CUTSCENE_DEBUG_FINAL
				if (pManager->RequiresPreviousAnimDictOnSkip())
					RequestAnimDictionaryforSection(pManager, pObject, GetPreviousValidSectionToLoad(iSectionToSkipTo));
#endif
					
				u32 loadSection = iSectionToSkipTo; 

				RequestAnimDictionaryforSection(pManager, pObject, loadSection );

				loadSection = GetNextValidSectionToLoad(loadSection); 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection); 

				loadSection = GetNextValidSectionToLoad(loadSection); 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection);
			}
			else
			{
				//need to change this so that it listens to the to the concat list if need be 
				
				s32 iCurrentSection = pManager->GetSectionForTime(pManager->GetTime()); 
			
				//need to pre stream two dictionaries ahead from the current section
				//check that the dictionary is not already loaded
				s32 loadSection = iCurrentSection; 
			
				//sanity check that the first section that we are going to load is valid else find the first valid section
				if(!IsSectionValidToLoad(loadSection))
				{
					loadSection = GetNextValidSectionToLoad(loadSection); 
					iCurrentSection = loadSection; 
				}
				RequestAnimDictionaryforSection(pManager, pObject, loadSection );
				
				loadSection = GetNextValidSectionToLoad(loadSection); 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection); 
				
				loadSection = GetNextValidSectionToLoad(loadSection); 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection); 
			}
        }
        break;

    case CUTSCENE_UNLOAD_ANIM_DICT_EVENT:
        {
			//This now needs to be called in the post scene update so moved to a seperate event, can be added to the data if need be
			//or called explicitly by the cut scene manager in its post update
			if(pManager->IsPlaying() || pManager->IsLoadingBeforeResuming())
			{
				ReleaseUnusedAnimDictionaries(pManager, pManager->GetSectionForTime(pManager->GetTime()), -1); 
			}
			else
			{
				m_iCurrentAnimSection = -1;

				m_ObjectsToBeAnimated.Reset(); 

				UnloadAllDictionaries(pManager);
			}
        }
        break;

    case CUTSCENE_SET_ANIM_EVENT:
        {
			cutsAnimManagerDebugf("CUTSCENE_SET_ANIM_EVENT - event args:%s", pEventArgs? "yes" : "no");
			// Skip pre cutscene set anim events, they're from static light objects that pre-fire the set anim events to create the shadows
			if(pManager->IsStartSceneDispatched())
			{
				m_iCurrentAnimSection = pManager->GetSectionForTime(pManager->GetTime());            
	            
				u32 AnimPartialHash = 0; 
				s32 ObjectId = -1; 
				
				if(pEventArgs)
				{
					if(pEventArgs->GetType() == CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE ||  pEventArgs->GetType() == CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE)
					{	
						const cutfObjectIdEventArgs *pObjectIdNameEventArgs = static_cast<const cutfObjectIdEventArgs *>( pEventArgs ); 
						
						const cutfObject* pObject = pManager->GetObjectById(pObjectIdNameEventArgs->GetObjectId());
						
						ObjectId = pObjectIdNameEventArgs->GetObjectId(); 

						if(pObject)
						{
							if(pObject->GetAnimStreamingType() == CUTSCENE_NAMED_ANIMATED_OBJECT)
							{
								const cutfNamedAnimatedObject* pAnimatedObject = static_cast<const cutfNamedAnimatedObject*>(pObject); 
								AnimPartialHash = pAnimatedObject->GetAnimStreamingBase(); 
							}
							else if(pObject->GetAnimStreamingType() == CUTSCENE_NAMED_STREAMED_ANIMATED_OBJECT)
							{
								const cutfNamedAnimatedStreamedObject* pAnimatedObject = static_cast<const cutfNamedAnimatedStreamedObject*>(pObject); 
								AnimPartialHash = pAnimatedObject->GetAnimStreamingBase(); 
							}
							else if(pObject->GetAnimStreamingType() == CUTSCENE_ANIMATED_LIGHT_STREAMED_OBJECT)
							{
								const cutfAnimatedLightObject* pAnimatedObject = static_cast<const cutfAnimatedLightObject*>(pObject); 
								AnimPartialHash = pAnimatedObject->GetAnimStreamingBase(); 
							}
							else
							{
								cutsAssertf(ShouldIgnoreObject(pObject), "Trying to animate non animated object not of type: CUTSCENE_NAMED_ANIMATED_OBJECT, CUTSCENE_NAMED_STREAMED_ANIMATED_OBJECT or CUTSCENE_ANIMATED_LIGHT_STREAMED_OBJECT");
							}

							cutsAssertf(ShouldIgnoreObject(pObject) || AnimPartialHash != 0, "%s has an invalid partial hash", pObject->GetDisplayName().c_str());
						}
						else
						{
							return; 
						}

					}
					else if(pEventArgs->GetType() == CUTSCENE_OBJECT_ID_PARTIAL_HASH_EVENT_ARGS_TYPE)
					{
						const cutfObjectIdPartialHashEventArgs *pObjectIdNameEventArgs = static_cast<const cutfObjectIdPartialHashEventArgs *>( pEventArgs ); 
						AnimPartialHash = pObjectIdNameEventArgs->GetPartialHash(); 
						ObjectId = pObjectIdNameEventArgs->GetObjectId(); 
					}
				}
				else
				{
					return; 
				}

				if ( pEventArgs && ObjectId != -1 )
				{
					bool bAnimRefd = false; 
					
					for (s32 i=0; i < m_ObjectsToBeAnimated.GetCount(); i++)
					{
						if(ObjectId == m_ObjectsToBeAnimated[i].iObjectid)
						{
							m_ObjectsToBeAnimated[i].AnimRef += 1; 
							bAnimRefd = true; 
						}
					}
					
					if(!bAnimRefd)
					{
						SAnimPlayStruct NewPlayStruct; 
						
						NewPlayStruct.iObjectid = ObjectId; 
						NewPlayStruct.AnimRef = 1; 
						NewPlayStruct.PartialAnimHash = AnimPartialHash; 
						NewPlayStruct.AnimSectionAnimWasSet = -1; 
						m_ObjectsToBeAnimated.PushAndGrow(NewPlayStruct); 
					}
		

					u32 FinalAnimHash = 0; 
					
					for (s32 i=0; i < m_ObjectsToBeAnimated.GetCount(); i++)
					{
						if(m_ObjectsToBeAnimated[i].iObjectid == ObjectId)
						{
							if ( GetSectionedAnimNames( pManager, ObjectId, AnimPartialHash, FinalAnimHash))
							{
								if(SetEntityAnimation( pManager, ObjectId, pManager->GetCurrentSectionTime(), FinalAnimHash, 0))
								{
									m_ObjectsToBeAnimated[i].AnimSectionAnimWasSet = pManager->GetSectionForTime(pManager->GetTime()); 
								}
								else
								{
									// cutsErrorf( "Unable to set clip/anim on '%s' to '%s' with face anim '%s'.", pObjectIdNameEventArgs->GetName(), cAnimName, cFaceAnimName );
								}
							}
						}
					}
				}
			}
        }
        break;

    case CUTSCENE_CLEAR_ANIM_EVENT:
        {
            // just pass along the event
			if(pEventArgs && (pEventArgs->GetType() == CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE || pEventArgs->GetType() == CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE) )
			{
				const cutfObjectIdEventArgs *pObjectIdNameEventArgs = static_cast<const cutfObjectIdEventArgs *>( pEventArgs );
			
				if ( pObjectIdNameEventArgs != NULL )
				{
					for (s32 i=0; i < m_ObjectsToBeAnimated.GetCount(); i++)
					{
						if(m_ObjectsToBeAnimated[i].iObjectid == pObjectIdNameEventArgs->GetObjectId() )
						{
							m_ObjectsToBeAnimated[i].AnimRef -= 1; 

							if(m_ObjectsToBeAnimated[i].AnimRef == 0)
							{
								cutfObjectIdEventArgs* EventArgs = rage_new cutfObjectIdEventArgs(pObjectIdNameEventArgs->GetObjectId()); 
								
								ClearEntityAnimation( pManager, EventArgs );

								m_ObjectsToBeAnimated.Delete(i);
							}
						}
					}
				}
			}
        }
        break;

    case CUTSCENE_UPDATE_EVENT:
        {
			m_iCurrentAnimSection = pManager->GetSectionForTime(pManager->GetTime());    

			s32 iCurrentSection = pManager->GetSectionForTime(pManager->GetTime()); 
			
			if (m_Dictionaries[iCurrentSection].bValidForLoading)
			{
				cutsAssertf(IsDictionaryLoaded(iCurrentSection), "Anim dictionary for section (%d) concat section (%d) is not streamed in, anims will not play", iCurrentSection, pManager->GetConcatSectionForTime(pManager->GetTime())); 
				
				for (s32 i=0; i < m_ObjectsToBeAnimated.GetCount(); i++)
				{
					if(m_ObjectsToBeAnimated[i].AnimSectionAnimWasSet != iCurrentSection)
					{
						u32 AnimHash = 0; 
						m_ObjectsToBeAnimated[i].AnimSectionAnimWasSet = iCurrentSection; 

						if ( GetSectionedAnimNames( pManager, m_ObjectsToBeAnimated[i].iObjectid, m_ObjectsToBeAnimated[i].PartialAnimHash, AnimHash))
						{
							//	cutsAnimManagerDebugf("CUTSCENE_UPDATE_EVENT - Crossing anim section boundary: Setting new anim %s (face %s) on entity %s", cAnimName, cFaceAnimName, m_ObjectsToBeAnimated[i].pName);m_ObjectsToBeAnimated[i].iObjectid, pManager->GetCurrentSectionTime(), cAnimName, cFaceAnimName ) )
							if(!SetEntityAnimation( pManager, m_ObjectsToBeAnimated[i].iObjectid, pManager->GetCurrentSectionTime(), AnimHash, 0))
							{
								//	cutsAnimManagerDebugf("CUTSCENE_UPDATE_EVENT - Crossing anim section boundary: Setting new anim %s (face %s) on entity %s", cAnimName, cFaceAnimName, m_ObjectsToBeAnimated[i].pName);
								cutfObjectIdEventArgs objectIdNameEventArgs( m_ObjectsToBeAnimated[i].iObjectid);
								ClearEntityAnimation( pManager, &objectIdNameEventArgs );
							}
						}
					}
				}
			}
				//crossed a section boundary lets update the animation dictionary requests 
#if !__FINAL			
			if(pManager->GetPlayBackState() == cutsManager::PLAY_STATE_FORWARDS_NORMAL_SPEED || pManager->GetPlayBackState() == cutsManager::PLAY_STATE_FORWARDS_SCALED)
#endif					
			{
				s32 loadSection = iCurrentSection; 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection );

				loadSection = GetNextValidSectionToLoad(loadSection); 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection); 
				
				loadSection = GetNextValidSectionToLoad(loadSection); 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection); 
			}
#if !__FINAL	
			else if(pManager->GetPlayBackState() == cutsManager::PLAY_STATE_BACKWARDS)
			{
				RequestAnimDictionaryforSection(pManager, pObject, GetPreviousValidSectionToLoad(iCurrentSection));

				u32 loadSection = iCurrentSection; 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection );
				
				loadSection = GetNextValidSectionToLoad(loadSection); 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection); 
				
				loadSection = GetNextValidSectionToLoad(loadSection); 
				RequestAnimDictionaryforSection(pManager, pObject, loadSection); 
			}
#endif
		
		}
        break;


	case CUTSCENE_PLAY_BACKWARDS_EVENT:
		{
#if !__FINAL	
			if(!ms_KeepExtraDictionaries)	
			{
				s32 iCurrentSection = pManager->GetSectionForTime(pManager->GetTime()); 
				
				RequestAnimDictionaryforSection(pManager, pObject, GetPreviousValidSectionToLoad(iCurrentSection));
				
				pManager->NeedToLoadBeforeResuming();
				ms_KeepExtraDictionaries = true; 
			}
#endif
		}
		break;

    case CUTSCENE_CANCEL_LOAD_EVENT:
        {
            CancelLoads( pManager, pObject );
        }
        break;
    case CUTSCENE_STEP_BACKWARD_EVENT:
    case CUTSCENE_REWIND_EVENT:
        {

#if !__FINAL	
			if(!ms_KeepExtraDictionaries)	
			{
				s32 iCurrentSection = pManager->GetSectionForTime(pManager->GetTime()); 

				RequestAnimDictionaryforSection(pManager, pObject, GetPreviousValidSectionToLoad(iCurrentSection));

				pManager->NeedToLoadBeforeResuming();
				ms_KeepExtraDictionaries = true; 
			}
#endif

#if RMPTFX_EDITOR
            // cannot go backwards, so just pause it
		    if ( g_ptxManager::IsInstantiated() )
            {
                RMPTFXMGR.SetDeltaTimeScalar( 0.0f );
            }
#endif // RMPTFX_EDITOR
        }
        break;
    case CUTSCENE_PLAY_EVENT:
    case CUTSCENE_STEP_FORWARD_EVENT:
        {

#if RMPTFX_EDITOR
            if ( g_ptxManager::IsInstantiated() )
            {
                RMPTFXMGR.SetDeltaTimeScalar( 1.0f );
            }
#endif // RMPTFX_EDITOR
        }
        break;
    case CUTSCENE_FAST_FORWARD_EVENT:
        {
#if RMPTFX_EDITOR
			if (g_ptxManager::IsInstantiated())
			{
				const cutfFloatValueEventArgs *pFloatEventArgs = dynamic_cast<const cutfFloatValueEventArgs *>( pEventArgs );
				if ( pEventArgs != NULL )
				{
					RMPTFXMGR.SetDeltaTimeScalar( pFloatEventArgs->GetFloat1() );
				}
			}
#endif // RMPTFX_EDITOR
        }
        break;
    case CUTSCENE_PAUSE_EVENT:
        {
#if RMPTFX_EDITOR
            if ( g_ptxManager::IsInstantiated() )
			{
				RMPTFXMGR.SetDeltaTimeScalar( 0.0f );
			}
#endif // RMPTFX_EDITOR
        }
        break;
    case CUTSCENE_END_OF_SCENE_EVENT:
        {
#if !__FINAL	        
			ms_KeepExtraDictionaries = false; 
#endif	
			ClearAllEntityAnimations( pManager );

#if RMPTFX_EDITOR
            if ( g_ptxManager::IsInstantiated() )
			{
	            RMPTFXMGR.SetDeltaTimeScalar( 1.0f );
			}
#endif // RMPTFX_EDITOR

            // Reset for unloading state
            pManager->SetIsUnloading( pObject->GetObjectId(), false );
        }
        break;
    default:
        break;
    }
}

const crAnimation* cutsAnimationManagerEntity::GetAnimation( const char* pName, int iAnimBuffer ) const
{
	if ( (iAnimBuffer < 0) || (iAnimBuffer >= m_Dictionaries.GetCount()) )
	{
		cutsErrorf( "Buffer index %d out of range.", iAnimBuffer );
		return NULL;
	}

	if ( m_Dictionaries[iAnimBuffer].pAnimDictionary != NULL )
	{
		char animName[RAGE_MAX_PATH];
		sprintf( animName, "%s.anim", pName );

		return m_Dictionaries[iAnimBuffer].pAnimDictionary->GetAnimation( animName );
	}
    return NULL;
}

const crAnimation* cutsAnimationManagerEntity::GetAnimation( crAnimDictionary::AnimKey animKey, int iAnimBuffer ) const
{
	if ( (iAnimBuffer < 0) || (iAnimBuffer >= m_Dictionaries.GetCount()) )
	{
		cutsErrorf( "Buffer index %d out of range.", iAnimBuffer );
		return NULL;
	}

	if ( m_Dictionaries[iAnimBuffer].pAnimDictionary != NULL )
	{
		return m_Dictionaries[iAnimBuffer].pAnimDictionary->GetAnimation( animKey );
	}
    return NULL;
}

const crClip* cutsAnimationManagerEntity::GetClip( const char* pName, int iClipBuffer ) const
{
	if ( (iClipBuffer < 0) || (iClipBuffer >= m_Dictionaries.GetCount()) )
	{
		cutsErrorf( "Buffer index %d out of range.", iClipBuffer );
		return NULL;
	}

	if ( m_Dictionaries[iClipBuffer].pClipDictionary != NULL )
	{
		char clipName[RAGE_MAX_PATH];
		sprintf( clipName, "%s.clip", pName );

		return m_Dictionaries[iClipBuffer].pClipDictionary->GetClip( clipName );
	}
    return NULL;

}


const crClip* cutsAnimationManagerEntity::GetClip( crClipDictionary::ClipKey clipKey, int iClipBuffer ) const
{
	if ( (iClipBuffer < 0) || (iClipBuffer >= m_Dictionaries.GetCount()) )
	{
		cutsErrorf( "Buffer index %d out of range.", iClipBuffer );
		return NULL;
	}

	if ( m_Dictionaries[iClipBuffer].pClipDictionary != NULL )
	{
		return m_Dictionaries[iClipBuffer].pClipDictionary->GetClip( clipKey );
	}
        return NULL;
    }


#if !__FINAL

int cutsAnimationManagerEntity::GetMemoryUsage( int iBuffer ) const
{  
	if ( (iBuffer < 0) || (iBuffer >= m_Dictionaries.GetCount()) )
	{
		cutsErrorf( "Buffer index %d out of range.", iBuffer );
		return 0;
	}

	int iMemorySize = 0;

	if ( m_Dictionaries[iBuffer].pAnimDictionary != NULL )
	{
		iMemorySize += m_Dictionaries[iBuffer].iAnimDictionarySize;
	}

	if ( m_Dictionaries[iBuffer].pClipDictionary != NULL )
	{
		iMemorySize += m_Dictionaries[iBuffer].iClipDictionarySize;
	}

	return iMemorySize;
}

struct SAnimSearchStruct
{
	atArray<atString> animFiles;
	atArray<atString> clipFiles;
	int matchIndex;
};

static void CB_FindClipFilesWithIndex( const fiFindData &data, void *userArg )
{
	SAnimSearchStruct &animSearch = *((SAnimSearchStruct *) userArg);

	const char* pExtension = ASSET.FindExtensionInPath( data.m_Name );
	if ( (pExtension != NULL) && (stricmp( pExtension, ".clip" ) == 0) )
	{
		int iIndex = GetFileIndex( data.m_Name );
		if ( iIndex == animSearch.matchIndex )
		{
			atString filename( data.m_Name );
			filename.Lowercase();

			animSearch.clipFiles.Grow() = filename;
		}
	}
}

#endif // !__FINAL

bool cutsAnimationManagerEntity::LoadDictionary( cutsManager *NOTFINAL_ONLY(pManager), s32 NOTFINAL_ONLY(iObjectId), const char* NOTFINAL_ONLY(pName), int NOTFINAL_ONLY(iBuffer) )
{
#if !__FINAL
	RAGE_TRACK( cutsAnimationManagerEntity_LoadDictionary );

    pManager->SetIsLoading( iObjectId, true );

    char cDirectory[RAGE_MAX_PATH];
    ASSET.RemoveExtensionFromPath( cDirectory, sizeof(cDirectory), pManager->GetCutsceneFilename() );

    int len = (int) strlen( cDirectory );
    for ( int i = 0; i < len; ++i )
    {
        if ( cDirectory[i] == '\\' )
        {
            cDirectory[i] = '/';
        }
    }

    char cCutfileDirectory[RAGE_MAX_PATH];
    ASSET.RemoveNameFromPath( cCutfileDirectory, sizeof(cCutfileDirectory), cDirectory );

    // try to load the dictionaries if we are sectioned
    if ( NOTFINAL_ONLY( !PARAM_cutsIgnoreDicts.Get() && ) (pManager->GetSectionStartTimeList().GetCount() > 0) )
    {
        // construct the filename, as if it lives in the directory under the cutfile
        char cClipDictFilename[RAGE_MAX_PATH];
        sprintf( cClipDictFilename, "%s\\%s.%ccdt", cDirectory, pName, g_sysPlatform );

        cutsDisplayf( "Looking for clip dictionary '%s'.", cClipDictFilename );

        if ( !ASSET.Exists( cClipDictFilename, NULL ) )
        {        
            // construct the filename, as if it lives in the same directory as the cutfile
            sprintf( cClipDictFilename, "%s\\%s.%ccdt", cCutfileDirectory, pName, g_sysPlatform );

            cutsDisplayf( "Not found.  Looking for secondary clip dictionary '%s'.", cClipDictFilename );
        }

        if ( ASSET.Exists( cClipDictFilename, NULL ) )
        {
            m_bAnimsAreCompressed = true;

            atString clipDictResourceName;
            clipDictResourceName.Set( cClipDictFilename, (int) strlen(cClipDictFilename), 0, (int) strlen(cClipDictFilename) - 5 );

            int iMemoryAvailableBefore = (int) sysMemAllocator::GetCurrent().GetMemoryAvailable();
            m_Dictionaries[iBuffer].pClipDictionary = crClipDictionary::LoadResource( clipDictResourceName.c_str() );
            if ( m_Dictionaries[iBuffer].pClipDictionary == NULL )
            {
                return false;
            }

            int iMemoryAvailableAfter = (int) sysMemAllocator::GetCurrent().GetMemoryAvailable();
            m_Dictionaries[iBuffer].iClipDictionarySize = (iMemoryAvailableBefore - iMemoryAvailableAfter) / 1024;

            m_Dictionaries[iBuffer].bClipDictIsResource = true;

            safecpy( m_Dictionaries[iBuffer].cName, pName, sizeof(m_Dictionaries[iBuffer].cName) );

            pManager->SetIsLoaded( iObjectId, true );

            return true;
        }
        else
        {
            cutsDisplayf( "%s Not found.  Looking for raw animations.", pManager->GetDisplayTime() );
        }
    }

    cutsDisplayf( "%s Looking for raw animations.", pManager->GetDisplayTime() );
        
    m_bAnimsAreCompressed = false;

    // find the index number of the anim dict
    SAnimSearchStruct animSearch;
    animSearch.matchIndex = GetFileIndex( pName );

    // find all files in that cDirectory that match the iIndex
    ASSET.EnumFiles( cDirectory, CB_FindClipFilesWithIndex, &animSearch );

    if ( (animSearch.animFiles.GetCount() == 0) && (animSearch.clipFiles.GetCount() == 0) )
    {
        safecpy( cDirectory, cCutfileDirectory, sizeof(cDirectory) );
        ASSET.EnumFiles( cDirectory, CB_FindClipFilesWithIndex, &animSearch );

        if ( (animSearch.animFiles.GetCount() == 0) && (animSearch.clipFiles.GetCount() == 0) )
        {
            cutsWarningf( "No .clip or .anim files found." );
        }
    }

    // determine the face animation filenames
    atArray<const cutfObject *> pedModelObjectList;
    pManager->GetModelObjectsOfType( pedModelObjectList, CUTSCENE_PED_MODEL_TYPE );
    
    if ( pedModelObjectList.GetCount() > 0 )
    {
        char cFaceDirectory[RAGE_MAX_PATH];
        safecpy( cFaceDirectory, pManager->GetFaceDirectory(), sizeof(cFaceDirectory) );
        int len = (int) strlen( cFaceDirectory );
        if ( len == 0 )
        {
            char cDirectory[RAGE_MAX_PATH];
            ASSET.RemoveExtensionFromPath( cDirectory, sizeof(cDirectory), pManager->GetCutsceneFilename() );

            sprintf( cFaceDirectory, "%s/faces", cDirectory );
        }
        
        for ( int i = 0; i < pedModelObjectList.GetCount(); ++i )
        {
            char cFaceAnimFilename[RAGE_MAX_PATH];
            const cutfPedModelObject *pPedModelObject = dynamic_cast<const cutfPedModelObject *>( pedModelObjectList[i] );
			//deprecated we no longer carry file names in the cutfile, its too expensive  size wise need an alternative method          
			safecpy( cFaceAnimFilename, pPedModelObject->GetFaceAnimationFilename( cFaceDirectory ).GetCStr(), sizeof(cFaceAnimFilename) );

            if ( ASSET.Exists( cFaceAnimFilename, NULL ) )
            {
                len = (int) strlen( cFaceAnimFilename );
                for ( int i = 0; i < len; ++i )
                {
                    if ( cFaceAnimFilename[i] == '\\' )
                    {
                        cFaceAnimFilename[i] = '/';
                    }
                }

                const char *pExtension = ASSET.FindExtensionInPath( cFaceAnimFilename );
                if ( (pExtension != NULL) && (stricmp( pExtension, ".clip" ) == 0) )
                {
                    animSearch.clipFiles.Grow() = cFaceAnimFilename;
                }
                else
                {
                    animSearch.animFiles.Grow() = cFaceAnimFilename;
                }
            }
        }
    }

    ASSET.PushFolder( cDirectory );

    // load the clips into a dictionary
    if ( animSearch.clipFiles.GetCount() > 0 )
    {            
        m_Dictionaries[iBuffer].bClipDictIsResource = false;

        crAnimLoader *pAnimLoader = GetAnimLoader();

        int iMemoryAvailableBefore = (int) sysMemAllocator::GetCurrent().GetMemoryAvailable();

        crAnimLoaderBasic *pAnimLoaderBasic = dynamic_cast<crAnimLoaderBasic *>( pAnimLoader );
        if ( pAnimLoaderBasic != NULL )
        {
            // turn off pack when loading raw animations
            pAnimLoaderBasic->Init( false, false );
        }

        m_Dictionaries[iBuffer].pClipDictionary = rage_new crClipDictionary;

        if ( !m_Dictionaries[iBuffer].pClipDictionary->LoadClips( animSearch.clipFiles, GetClipLoader(), pAnimLoader ) )
        {
            for ( int i = 0; i < animSearch.clipFiles.GetCount(); ++i )
            {
                animSearch.clipFiles[i].Clear();
            }

            animSearch.clipFiles.Reset();

            for ( int i = 0; i < animSearch.animFiles.GetCount(); ++i )
            {
                animSearch.animFiles[i].Clear();
            }

            animSearch.animFiles.Reset();

            return false;
        }

        int iMemoryAvailableAfter = (int) sysMemAllocator::GetCurrent().GetMemoryAvailable();
        m_Dictionaries[iBuffer].iClipDictionarySize = (iMemoryAvailableBefore - iMemoryAvailableAfter) / 1024;
    }        

    // load the face anims into a dictionary
    if ( animSearch.animFiles.GetCount() > 0 )
    {
        m_Dictionaries[iBuffer].bAnimDictIsResource = false;

        crAnimLoader *pAnimLoader = GetAnimLoader();

        int iMemoryAvailableBefore = (int) sysMemAllocator::GetCurrent().GetMemoryAvailable();

        crAnimLoaderBasic *pAnimLoaderBasic = dynamic_cast<crAnimLoaderBasic *>( pAnimLoader );
        if ( pAnimLoaderBasic != NULL )
        {
            // turn off pack when loading raw animations
            pAnimLoaderBasic->Init( false, false );
        }

        m_Dictionaries[iBuffer].pAnimDictionary = rage_new crAnimDictionary;
        if ( !m_Dictionaries[iBuffer].pAnimDictionary->LoadAnimations( animSearch.animFiles, pAnimLoader ) )
        {
            for ( int i = 0; i < animSearch.clipFiles.GetCount(); ++i )
            {
                animSearch.clipFiles[i].Clear();
            }

            animSearch.clipFiles.Reset();

            for ( int i = 0; i < animSearch.animFiles.GetCount(); ++i )
            {
                animSearch.animFiles[i].Clear();
            }

            animSearch.animFiles.Reset();

            return false;
        }            

        int iMemoryAvailableAfter = (int) sysMemAllocator::GetCurrent().GetMemoryAvailable();
        m_Dictionaries[iBuffer].iAnimDictionarySize = (iMemoryAvailableBefore - iMemoryAvailableAfter) / 1024;
    }

    ASSET.PopFolder();

    safecpy( m_Dictionaries[iBuffer].cName, pName, sizeof(m_Dictionaries[iBuffer].cName) );

    pManager->SetIsLoaded( iObjectId, true );

    for ( int i = 0; i < animSearch.clipFiles.GetCount(); ++i )
    {
        animSearch.clipFiles[i].Clear();
    }

    animSearch.clipFiles.Reset();

    for ( int i = 0; i < animSearch.animFiles.GetCount(); ++i )
    {
        animSearch.animFiles[i].Clear();
    }

    animSearch.animFiles.Reset();
#endif // !__FINAL

    return true;

}

bool cutsAnimationManagerEntity::UnloadDictionary( cutsManager* pManager, int iBuffer )
{
	if ( (iBuffer < 0) || (iBuffer >= c_numBuffers) )
    {
        cutsErrorf( "Buffer index %d out of range.", iBuffer );
        return true;
    }

    pManager->SetIsUnloading( GetObject()->GetObjectId(), true );

    if ( m_Dictionaries[iBuffer].pAnimDictionary != NULL )
    {
        if ( m_Dictionaries[iBuffer].bAnimDictIsResource )
        {
            m_Dictionaries[iBuffer].pAnimDictionary.Release();
            m_Dictionaries[iBuffer].bAnimDictIsResource = false;
        }
        else
        {
            delete m_Dictionaries[iBuffer].pAnimDictionary;
        }

        m_Dictionaries[iBuffer].pAnimDictionary = NULL;
    }

    if ( m_Dictionaries[iBuffer].pClipDictionary != NULL )
    {
        if ( m_Dictionaries[iBuffer].bClipDictIsResource )
        {
            m_Dictionaries[iBuffer].pClipDictionary.Release();
            m_Dictionaries[iBuffer].bClipDictIsResource = false;
        }
        else
        {
            delete m_Dictionaries[iBuffer].pClipDictionary;
        }

        m_Dictionaries[iBuffer].pClipDictionary = NULL;
    }

    m_Dictionaries[iBuffer].cName[0] = 0;

    pManager->SetIsUnloaded( GetObject()->GetObjectId(), true );
    return true;
}

bool cutsAnimationManagerEntity::IsDictionaryLoaded(int UNUSED_PARAM(iBuffer))
{
	return true; 
}

void cutsAnimationManagerEntity::UnloadAllDictionaries( cutsManager *pManager )
{
	for(int i =0; i < m_Dictionaries.GetCount(); i++)
	{
		UnloadDictionary( pManager, i );
	}
    }

bool cutsAnimationManagerEntity::CancelLoads( cutsManager *pManager, const cutfObject* pObject )
{
    if ( pManager->IsLoading( pObject->GetObjectId() ) )
    {
        pManager->SetIsLoading( pObject->GetObjectId(), false );
        return true;
    }

    return false;
}

bool cutsAnimationManagerEntity::GetSectionedAnimNames( cutsManager *pManager, u32 iObjectId, const char *pBaseAnimName, 
                                                       char *pAnimName, int iAnimNameLen, char *pFaceAnimName, int iFaceAnimNameLen )
{
    const cutfObject *pObject = pManager->GetObjectById( iObjectId );
    if ( pObject == NULL )
    {
        return false;
    }

    bool bIsCombined = false;
    bool bHasFace = false;

    if ( pObject->GetType() == CUTSCENE_MODEL_OBJECT_TYPE )
    {
        const cutfModelObject *pModelObject = SafeCast( const cutfModelObject, pObject );
        if ( pModelObject->GetModelType() == CUTSCENE_PED_MODEL_TYPE )
        {
            const cutfPedModelObject *pPedModelObject = SafeCast( const cutfPedModelObject, pModelObject );

            bIsCombined = pPedModelObject->HasFaceAnimation() && pPedModelObject->FaceAndBodyAreMerged();
            bHasFace = pPedModelObject->HasFaceAnimation() && !pPedModelObject->FaceAndBodyAreMerged();
        }
    }

    char cAnimName[RAGE_MAX_PATH];
    char cFaceAnimName[RAGE_MAX_PATH];

    safecpy( cAnimName, pBaseAnimName, sizeof(cAnimName) );
    cFaceAnimName[0] = 0;

    if ( bIsCombined )
    {
        strcat( cAnimName, "_dual" );
    }
	//dont support strings in final builds the data in the cutfile will need to be stored as partial hash strings
    //else if ( bHasFace || bCanHaveFace )
    //{
    //    //// add the full path because that's what we had to do to load it
    //    //char cFaceDirectory[RAGE_MAX_PATH];
    //    //safecpy( cFaceDirectory, pManager->GetFaceDirectory(), sizeof(cFaceDirectory) );
    //    //int len = (int) strlen( cFaceDirectory );
    //    //if ( len == 0 )
    //    //{
    //    //    char cDirectory[RAGE_MAX_PATH];
    //    //    ASSET.RemoveExtensionFromPath( cDirectory, sizeof(cDirectory), pManager->GetCutsceneFilename() );

    //    //    sprintf( cFaceDirectory, "%s/faces", cDirectory );
    //    //}

    //    //const cutfPedModelObject *pPedModelObject = SafeCast( const cutfPedModelObject, pObject );
    //    //safecpy( cFaceAnimName, pPedModelObject->GetFaceAnimationFilename( cFaceDirectory ).c_str(), sizeof(cFaceAnimName) );

    //    //len = (int) strlen( cFaceAnimName );
    //    //for ( int i = 0; i < len; ++i )
    //    //{
    //    //    if ( cFaceAnimName[i] == '\\' )
    //    //    {
    //    //        cFaceAnimName[i] = '/';
    //    //    }
    //    //}

    //    //if ( bCanHaveFace )
    //    //{
    //    //    if ( ASSET.Exists( cFaceAnimName, NULL ) )
    //    //    {
    //    //        bHasFace = true;
    //    //    }
    //    //    else
    //    //    {
    //    //        cFaceAnimName[0] = 0;
    //    //    }
    //    //}

    //    //if ( bHasFace )
    //    //{
    //    //    const char *pExtension = ASSET.FindExtensionInPath( cFaceAnimName );
    //    //    if ( pExtension != NULL )
    //    //    {
    //    //        *(const_cast<char *>( pExtension )) = 0;
    //    //    }
    //    //}
    //}

    //if ( m_bAnimsAreCompressed )
    //{
    //    strcat( cAnimName, "_zip" );

    //    if ( bHasFace )
    //    {
    //        strcat( cFaceAnimName, "_zip" );
    //    }
    //}

    cutsAssertf( strlen(cAnimName) < sizeof(cAnimName), "Buffer overrun" );

    if ( !GetSectionedName( pAnimName, iAnimNameLen, cAnimName, m_iCurrentAnimSection, pManager->GetSectionStartTimeList().GetCount() ) )
    {
        return false;
    }

    if ( bHasFace )
    {
        cutsAssertf( strlen(cFaceAnimName) < sizeof(cFaceAnimName), "Buffer overrun" );

        if ( !GetSectionedName( pFaceAnimName, iFaceAnimNameLen, cFaceAnimName, m_iCurrentAnimSection, pManager->GetSectionStartTimeList().GetCount() ) )
        {
            return false;
        }
    }
    else
    {
        safecpy( pFaceAnimName, cFaceAnimName, sizeof(pFaceAnimName) );
    }

    return true;
}

bool cutsAnimationManagerEntity::GetSectionedAnimNames( cutsManager *pManager, u32 iObjectId, u32 BaseAnimHash,  u32 &AnimHash )
{
	const cutfObject *pObject = pManager->GetObjectById( iObjectId );
	if ( pObject == NULL )
	{
		return false;
	}

	bool bIsCombined = false;
	//bool bHasFace = false;
	//bool bCanHaveFace = false;

	AnimHash = 0;

	if ( pObject->GetType() == CUTSCENE_MODEL_OBJECT_TYPE )
	{
		const cutfModelObject *pModelObject = SafeCast( const cutfModelObject, pObject );
		if ( pModelObject->GetModelType() == CUTSCENE_PED_MODEL_TYPE )
		{
			const cutfPedModelObject *pPedModelObject = SafeCast( const cutfPedModelObject, pModelObject );

			bIsCombined = pPedModelObject->HasFaceAnimation() && pPedModelObject->FaceAndBodyAreMerged();
			//bHasFace = pPedModelObject->HasFaceAnimation() && !pPedModelObject->FaceAndBodyAreMerged();
			//bCanHaveFace = !bIsCombined && !bHasFace;
		}
	}

	if ( bIsCombined )
	{
		BaseAnimHash = atPartialStringHash("_dual", BaseAnimHash);
	}


	if ( !GetSectionedName(  BaseAnimHash, AnimHash, m_iCurrentAnimSection, pManager->GetSectionStartTimeList().GetCount() ) )
	{
		return false;
	}

	//if ( bHasFace )
	//{
	//	//cutsAssertf( strlen(cFaceAnimName) < sizeof(cFaceAnimName), "Buffer overrun" );

	//	if ( !GetSectionedName( BaseAnimHash, AnimHash, m_iCurrentAnimSection, pManager->GetSectionStartTimeList().GetCount() ) )
	//	{
	//		return false;
	//	}
	//}

	return true;
}

bool cutsAnimationManagerEntity::SetEntityAnimation( cutsManager *pManager, s32 iObjectId, float fSectionTime, 
                                                    const char *pAnimName1, const char* pAnimName2 )
{
    cutsEntity *pEntity = pManager->GetEntityByObjectId( iObjectId );
    if ( pEntity == NULL )
    {
        return false;
    }

    const cutfObject *pObject = pManager->GetObjectById( iObjectId );
    if ( pObject == NULL )
    {
        return false;
    }

#if HACK_GTA4
	const char* pAnimDict;
	//fix here computing the wrong section when not starting from zero
	//pAnimDict = m_Dictionaries[pManager->GetSectionForTime(fSectionTime)].cName;

	pAnimDict = m_Dictionaries[m_iCurrentAnimSection].cName;
#endif

    // always prefer clips over animations.
	const crClip *pClip1 = NULL;
	
		pClip1 = GetClip( pAnimName1 );
	
	if ( pClip1 != NULL )
    {
        if ( (pAnimName2 != NULL) && (strlen( pAnimName2 ) > 0) )
        {
            const crClip *pClip2 = GetClip( pAnimName2 );
            if ( pClip2 != NULL )
            {
#if HACK_GTA4
				cutsDualClipEventArgs dualClipEventArgs( fSectionTime, pClip1, pClip2, pAnimDict );
#else
				cutsDualClipEventArgs dualClipEventArgs( fSectionTime, pClip1, pClip2 );
#endif                
				pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_DUAL_CLIP_EVENT, &dualClipEventArgs );
            }
            else
            {
                const crAnimation *pAnim2 = GetAnimation( pAnimName2 );
                if ( pAnim2 != NULL )
                {
#if HACK_GTA4     
					cutsDualClipAnimEventArgs dualClipAnimEventArgs( fSectionTime, pClip1, pAnim2, pAnimDict );
#else
					cutsDualClipAnimEventArgs dualClipAnimEventArgs( fSectionTime, pClip1, pAnim2 );
#endif
					pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_DUAL_CLIP_ANIM_EVENT, &dualClipAnimEventArgs );
                }
                else
                {
                    // dispatch the event with only the primary clip, but return false so that we may display a warning
#if HACK_GTA4
					cutsClipEventArgs clipEventArgs( fSectionTime, pClip1, pAnimDict );
#else
					cutsClipEventArgs clipEventArgs( fSectionTime, pClip1 );
#endif     
					pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_CLIP_EVENT, &clipEventArgs );
                    return false;
                }
            }
        }
        else
        {
#if HACK_GTA4     
			cutsClipEventArgs clipEventArgs( fSectionTime, pClip1, pAnimDict );
#else
			cutsClipEventArgs clipEventArgs( fSectionTime, pClip1 );
#endif            
			pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_CLIP_EVENT, &clipEventArgs );
        }
    }
    else 
    {
        const crAnimation *pAnim1 = GetAnimation( pAnimName1 );
        if ( pAnim1 != NULL )
        {
            if ( (pAnimName2 != NULL) && (strlen( pAnimName2 ) > 0) )
            {
                const crAnimation *pAnim2 = GetAnimation( pAnimName2 );
                if ( pAnim2 != NULL )
                {
                    cutsDualAnimationEventArgs dualAnimEventArgs( fSectionTime, pAnim1, pAnim2 );
                    pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_DUAL_ANIMATION_EVENT, &dualAnimEventArgs );
                }
                else
                {
                    // dispatch the event with only the primary anim, but return false so that we may display a warning
                    cutsAnimationEventArgs animEventArgs( fSectionTime, pAnim1 );
                    pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_ANIMATION_EVENT, &animEventArgs );
                    return false;
                }
            }
            else
            {
                cutsAnimationEventArgs animEventArgs( fSectionTime, pAnim1 );
                pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_ANIMATION_EVENT, &animEventArgs );
            }
        }
        else
        {            
            return false;
        }
    }

    return true;
}

bool cutsAnimationManagerEntity::SetEntityAnimation( cutsManager *pManager, s32 iObjectId, float fSectionTime, 
													u32 Anim1Hash, u32 Anim2Hash )
{
	cutsEntity *pEntity = pManager->GetEntityByObjectId( iObjectId );
	if ( pEntity == NULL )
	{
		return false;
	}

	const cutfObject *pObject = pManager->GetObjectById( iObjectId );
	if ( pObject == NULL )
	{
		return false;
	}

#if HACK_GTA4
	const char* pAnimDict;
	//fix here computing the wrong section when not starting from zero
	//pAnimDict = m_Dictionaries[pManager->GetSectionForTime(fSectionTime)].cName;

	pAnimDict = m_Dictionaries[m_iCurrentAnimSection].cName;
#endif

	// always prefer clips over animations.
	const crClip *pClip1 = NULL;

	pClip1 = GetClip( Anim1Hash );

	if ( pClip1 != NULL )
	{
		if ( (Anim2Hash != 0))
		{
			const crClip *pClip2 = GetClip( Anim2Hash );
			if ( pClip2 != NULL )
			{
#if HACK_GTA4
				cutsDualClipEventArgs dualClipEventArgs( fSectionTime, pClip1, pClip2, pAnimDict );
#else
				cutsDualClipEventArgs dualClipEventArgs( fSectionTime, pClip1, pClip2 );
#endif                
				pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_DUAL_CLIP_EVENT, &dualClipEventArgs );
			}
			else
			{
				const crAnimation *pAnim2 = GetAnimation( Anim2Hash );
				if ( pAnim2 != NULL )
				{
#if HACK_GTA4     
					cutsDualClipAnimEventArgs dualClipAnimEventArgs( fSectionTime, pClip1, pAnim2, pAnimDict );
#else
					cutsDualClipAnimEventArgs dualClipAnimEventArgs( fSectionTime, pClip1, pAnim2 );
#endif
					pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_DUAL_CLIP_ANIM_EVENT, &dualClipAnimEventArgs );
				}
				else
				{
					// dispatch the event with only the primary clip, but return false so that we may display a warning
#if HACK_GTA4
					cutsClipEventArgs clipEventArgs( fSectionTime, pClip1, pAnimDict );
#else
					cutsClipEventArgs clipEventArgs( fSectionTime, pClip1 );
#endif     
					pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_CLIP_EVENT, &clipEventArgs );
					return false;
				}
			}
		}
		else
		{
#if HACK_GTA4     
			cutsClipEventArgs clipEventArgs( fSectionTime, pClip1, pAnimDict );
#else
			cutsClipEventArgs clipEventArgs( fSectionTime, pClip1 );
#endif            
			pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_CLIP_EVENT, &clipEventArgs );
		}
	}
	else 
	{
		const crAnimation *pAnim1 = GetAnimation( Anim1Hash );
		if ( pAnim1 != NULL )
		{
			if ( Anim2Hash != 0 )
			{
				const crAnimation *pAnim2 = GetAnimation( Anim2Hash );
				if ( pAnim2 != NULL )
				{
					cutsDualAnimationEventArgs dualAnimEventArgs( fSectionTime, pAnim1, pAnim2 );
					pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_DUAL_ANIMATION_EVENT, &dualAnimEventArgs );
				}
				else
				{
					// dispatch the event with only the primary anim, but return false so that we may display a warning
					cutsAnimationEventArgs animEventArgs( fSectionTime, pAnim1 );
					pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_ANIMATION_EVENT, &animEventArgs );
					return false;
				}
			}
			else
			{
				cutsAnimationEventArgs animEventArgs( fSectionTime, pAnim1 );
				pEntity->DispatchEvent( pManager, pObject, CUTSCENE_SET_ANIMATION_EVENT, &animEventArgs );
			}
		}
		else
		{            
			return false;
		}
	}

	return true;
}


void cutsAnimationManagerEntity::ClearAllEntityAnimations( cutsManager *pManager )
{
	for (s32 i=0; i < m_ObjectsToBeAnimated.GetCount(); i++)
	{
		cutfObjectIdEventArgs objectIdEventArgs( m_ObjectsToBeAnimated[i].iObjectid);
		ClearEntityAnimation( pManager, &objectIdEventArgs);
	}

    m_ObjectsToBeAnimated.Reset();
}

void cutsAnimationManagerEntity::ClearEntityAnimation( cutsManager *pManager, const cutfObjectIdEventArgs *pObjectIdNameEventArgs )
{
    cutsEntity *pDestEntity = pManager->GetEntityByObjectId( pObjectIdNameEventArgs->GetObjectId() );
    if ( pDestEntity != NULL )
    {
        const cutfObject *pDestObject = pManager->GetObjectById( pObjectIdNameEventArgs->GetObjectId() );
        if ( pDestObject != NULL )
        {
            pDestEntity->DispatchEvent( pManager, pDestObject, CUTSCENE_CLEAR_ANIM_EVENT, pObjectIdNameEventArgs );
        }
    }
}

bool cutsAnimationManagerEntity::GetSectionedName( char* pDest, int destLen, const char* pName, int iSectionIndex, int iNumSections )
{
    int fileIndex = GetFileIndex( pName );

    // build the name based on the sectioning
    char name[RAGE_MAX_PATH];
    if ( fileIndex == -1 )
    {
        if ( iNumSections == 0 )
        {
            safecpy( name, pName, sizeof(name) );
        }
        else if ( (iSectionIndex >= 0) && (iSectionIndex < iNumSections) )
        {
			sprintf( name, "%s-%d", pName, iSectionIndex );
		}
        else
        {
            // nothing more to load
            return false; 
        }
    }
    else
    {
        safecpy( name, pName, sizeof(name) );
    }

    safecpy( pDest, name, destLen );
    return true;
}

bool cutsAnimationManagerEntity::GetSectionedName( u32 AnimHash, u32 &FinalHashString, int iSectionIndex, int iNumSections )
{
	if ( iNumSections == 0 )
	{
		FinalHashString = atFinalizeHash(AnimHash);
	}
	else if ( (iSectionIndex >= 0) && (iSectionIndex < iNumSections) )
	{
		char indexBuff[8];
		formatf(indexBuff, 8, "-%d", iSectionIndex); 
		FinalHashString = atStringHash(indexBuff, AnimHash);
	}
	else
	{
		// nothing more to load
		return false; 
	}

	return true;
}


//##############################################################################

cutsAnimatedEntity::cutsAnimatedEntity( const cutfObject* pObject )
: cutsUniqueEntity( pObject )
{

}

cutsAnimatedEntity::~cutsAnimatedEntity()
{

}

void cutsAnimatedEntity::DispatchEvent( cutsManager *pManager, const cutfObject *pObject, s32 iEventId, 
                                         const cutfEventArgs* pEventArgs, const float UNUSED_PARAM(fTime),const u32 UNUSED_PARAM(StickyId) )
{
    switch ( iEventId )
    {
    case CUTSCENE_SET_ANIMATION_EVENT:
        {
            // set the animation and the initial playback time
            const cutsAnimationEventArgs *pAnimEventArgs = dynamic_cast<const cutsAnimationEventArgs *>( pEventArgs );
            if ( pAnimEventArgs != NULL )
            {
                SetAnimation( pAnimEventArgs->GetAnimation(), pAnimEventArgs->GetTime() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_ANIMATION_EVENT:
        {
            // set the dual animation and initial playback time
            const cutsDualAnimationEventArgs *pDualAnimEventArgs = dynamic_cast<const cutsDualAnimationEventArgs *>( pEventArgs );
            if ( pDualAnimEventArgs != NULL )
            {
                SetDualAnimation( pDualAnimEventArgs->GetAnimation(), pDualAnimEventArgs->GetAnimation2(), pDualAnimEventArgs->GetTime() );
            }
        }
        break;
    case CUTSCENE_SET_CLIP_EVENT:
        {
            // set the clip and the initial playback time
            const cutsClipEventArgs *pClipEventArgs = dynamic_cast<const cutsClipEventArgs *>( pEventArgs );
            if ( pClipEventArgs != NULL )
            {
                SetClip( pClipEventArgs->GetClip(), pClipEventArgs->GetTime() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_EVENT:
        {
            // set the dual clip and the initial playback time
            const cutsDualClipEventArgs *pDualClipEventArgs = dynamic_cast<const cutsDualClipEventArgs *>( pEventArgs );
            if ( pDualClipEventArgs != NULL )
            {
                SetDualClip( pDualClipEventArgs->GetClip(), pDualClipEventArgs->GetClip2(), pDualClipEventArgs->GetTime() );
            }
        }
        break;
    case CUTSCENE_SET_DUAL_CLIP_ANIM_EVENT:
        {
            // set the dual clip and the initial playback time
            const cutsDualClipAnimEventArgs *pDualClipAnimEventArgs = dynamic_cast<const cutsDualClipAnimEventArgs *>( pEventArgs );
            if ( pDualClipAnimEventArgs != NULL )
            {
                SetDualClipAnim( pDualClipAnimEventArgs->GetClip(), pDualClipAnimEventArgs->GetAnimation(), pDualClipAnimEventArgs->GetTime() );
            }
        }
        break;
    case CUTSCENE_CLEAR_ANIM_EVENT:
        {
            Clear();
        }
        break;
    case CUTSCENE_UPDATE_EVENT:
        {
            const cutsUpdateEventArgs *pUpdateEventArgs = dynamic_cast<const cutsUpdateEventArgs *>( pEventArgs );
            if ( pUpdateEventArgs != NULL )
            {
                SetTime( pUpdateEventArgs->GetSectionSeconds() );
            }
        }
        break;
    default:
        break;
    }

    cutsUniqueEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}


void cutsAnimatedEntity::SetAnimation( const crAnimation * /*pAnim*/, float /*fTime*/ )
{
    // not supported unless you override this function in your derived class
    Clear();
}

void cutsAnimatedEntity::SetDualAnimation( const crAnimation * /*pAnim1*/, const crAnimation * /*pAnim2*/, float /*fTime*/ )
{
    // not supported unless you override this function in your derived class
    Clear();
}

void cutsAnimatedEntity::SetClip( const crClip * /*pClip*/, float /*fTime*/ )
{
    // not supported unless you override this function in your derived class
    Clear();
}

void cutsAnimatedEntity::SetDualClip( const crClip * /*pAnim1*/, const crClip * /*pAnim2*/, float /*fTime*/ )
{
    // not supported unless you override this function in your derived class
    Clear();
}

void cutsAnimatedEntity::SetDualClipAnim( const crClip * /*pClip*/, const crAnimation * /*pAnim*/, float /*fTime*/ )
{
    // not supported unless you override this function in your derived class
    Clear();
}

//##############################################################################

cutsAnimPlayerEntity::cutsAnimPlayerEntity( const cutfObject* pObject, crAnimPlayer *pAnimPlayer )
: cutsAnimatedEntity(pObject)
{
    cutsAssertf( pAnimPlayer != NULL, "A crAnimPlayer is required." );
    SetAnimPlayer( pAnimPlayer );
}

cutsAnimPlayerEntity::~cutsAnimPlayerEntity()
{
    SetAnimPlayer( NULL );
}

void cutsAnimPlayerEntity::SetAnimation( const crAnimation *pAnim, float fTime )
{
    m_pAnimPlayer->SetAnimation( pAnim );
    m_pAnimPlayer->SetRate( 0.0f );
    m_pAnimPlayer->SetTime( fTime );
}

void cutsAnimPlayerEntity::SetDualAnimation( const crAnimation *pAnim1, const crAnimation * /*pAnim2*/, float fTime )
{
    // not fully supported, so just set for the first anim
    SetAnimation( pAnim1, fTime );
}

void cutsAnimPlayerEntity::SetDualClipAnim( const crClip * /*pClip*/, const crAnimation *pAnim, float fTime )
{
    // not fully supported, so just set for the secondary anim
    SetAnimation( pAnim, fTime );
}

void cutsAnimPlayerEntity::Clear()
{
    SetAnimation( NULL, 0.0f );
}
void cutsAnimPlayerEntity::SetTime( float fTime )
{
    m_pAnimPlayer->SetTime( fTime );
}

//##############################################################################

cutsClipPlayerEntity::cutsClipPlayerEntity( const cutfObject* pObject, crClipPlayer *pClipPlayer )
: cutsAnimatedEntity(pObject)
{
    cutsAssertf( pClipPlayer != NULL, "A crClipPlayer is required." );
    SetClipPlayer( pClipPlayer );
}

cutsClipPlayerEntity::~cutsClipPlayerEntity()
{
    SetClipPlayer( NULL );
}

void cutsClipPlayerEntity::SetClip( const crClip *pClip, float fTime )
{
    m_pClipPlayer->SetClip( pClip );
    m_pClipPlayer->SetRate( 0.0f );
    m_pClipPlayer->SetTime( fTime );
}

void cutsClipPlayerEntity::SetDualClip( const crClip *pClip1, const crClip * /*pClip2*/, float fTime )
{
    // not fully supported, so just set for the first clip
    SetClip( pClip1, fTime );
}

void cutsClipPlayerEntity::SetDualClipAnim( const crClip *pClip, const crAnimation * /*pAnim*/, float fTime )
{
    // not fully supported, so just set for the primary clip
    SetClip( pClip, fTime );
}

void cutsClipPlayerEntity::SetTime( float fTime )
{
    m_pClipPlayer->SetTime( fTime );
}

void cutsClipPlayerEntity::Clear()
{
    SetClip( NULL, 0.0f );
}

//##############################################################################

cutsMotionTreeEntity::cutsMotionTreeEntity( const cutfObject* pObject, crmtMotionTree *pMotionTree, 
                                       crmtMotionTreeScheduler *pMotionTreeScheduler )
                                       : cutsAnimatedEntity(pObject)
                                       , m_pMotionTree(NULL)
                                       , m_pMotionTreeScheduler(pMotionTreeScheduler)
                                       , m_pObserver(NULL)
{
    cutsAssertf( pMotionTree != NULL, "A crmtMotionTree is required." );
    SetMotionTree( pMotionTree );
}

cutsMotionTreeEntity::~cutsMotionTreeEntity()
{
    SetMotionTree( NULL );

    m_pMotionTreeScheduler = NULL;
}

#if !__FINAL

void cutsMotionTreeEntity::DebugDraw() const
{
    if ( m_pMotionTree != NULL )
    {
        Mat34V globalMtx;
        bool bDrawLabel = false;
        bool bDrawAxis = false;

        const crCreature *pCreature = m_pMotionTree->GetCreature();
        if ( pCreature != NULL )
        {
            pCreature->DebugDraw();

            const crCreatureComponentSkeleton *pSkeletonComponent = pCreature->FindComponent<crCreatureComponentSkeleton>();
            if ( pSkeletonComponent != NULL )
            {
                const crSkeleton *pSkeleton = m_pMotionTree->GetSkeleton();
                if ( pSkeleton != NULL )
                {
                    pSkeleton->GetGlobalMtx( 0, globalMtx );
                    bDrawLabel = true;
                    bDrawAxis = true;
                }
            }

			const crCreatureComponentParticleEffect *pParticleEffectComponent = pCreature->FindComponent<crCreatureComponentParticleEffect>();
            if ( pParticleEffectComponent != NULL )
            {
                globalMtx = pParticleEffectComponent->GetRootMatrix();
                bDrawLabel = true;
                bDrawAxis = true;
            }

			const crCreatureComponentLight *pLightComponent = pCreature->FindComponent<crCreatureComponentLight>();
            if ( pLightComponent != NULL )
            {
                globalMtx = pLightComponent->GetRootMatrix();
                bDrawLabel = true;
            }

			const crCreatureComponentCamera *pCameraComponent = pCreature->FindComponent<crCreatureComponentCamera>();
            if ( pCameraComponent != NULL )
            {
                globalMtx = pCameraComponent->GetRootMatrix();
                bDrawLabel = true;
            }
        }
        else
        {
            const crSkeleton *pSkeleton = m_pMotionTree->GetSkeleton();
            if ( pSkeleton != NULL )
            {
                pSkeleton->GetGlobalMtx( 0, globalMtx );
                bDrawLabel = true;
                bDrawAxis = true;
            }
        }

        if ( bDrawLabel || bDrawAxis )
        {
            bool bOldLighting = grcLighting( false );

            Matrix34 m = RCC_MATRIX34(globalMtx);

            if ( bDrawLabel )
            {
                grcWorldIdentity();
                grcColor3f( 0.9f, 0.9f, 0.9f );
                grcDrawLabel( m.d, GetObject()->GetDisplayName().c_str() );
            }

            if ( bDrawAxis )
            {
                Vector3 eulers = m.GetEulers();

                char buf[1024];
                formatf( buf, "Pos: (%f, %f, %f)\nDir: (%f, %f, %f)", m.d.x, m.d.y, m.d.z, eulers.x * RtoD, eulers.y * RtoD, eulers.z * RtoD );

                grcWorldIdentity();
                grcColor3f( 0.7f, 0.7f, 0.7f );
                grcDrawLabel( m.d, 0, 10, buf );
            }

            grcLighting( bOldLighting );
        }
    }
}

#endif // !__FINAL

void cutsMotionTreeEntity::SetMotionTree( crmtMotionTree *pMotionTree )
{
    m_pMotionTree = pMotionTree;

    if ( m_pMotionTree == NULL && m_pObserver != NULL )
    {
        m_pObserver->Release();
        m_pObserver = NULL;
    }
}

void cutsMotionTreeEntity::SetAnimation( const crAnimation *pAnim, float fTime )
{
    RAGE_TRACK( cutsMotionTreeEntity_SetAnimation );

    if ( (m_pObserver != NULL) && m_pObserver->IsAttached() )
    {
        m_pObserver->Detach();
        m_pObserver = NULL;
    }

    if ( pAnim != NULL )
    {
        crmtRequestAnimation reqAnim( pAnim, fTime, 0.0f );
        m_pMotionTree->Request( reqAnim );

        if ( m_pObserver == NULL )
        {
            m_pObserver = rage_new crmtObserver;
        }

        m_pObserver->Attach( reqAnim.GetObserver() );
    }
    else
    {
        crmtRequestAnimation reqAnim;
        m_pMotionTree->Request( reqAnim );
    }
}

void cutsMotionTreeEntity::SetDualAnimation( const crAnimation *pAnim1, const crAnimation *pAnim2, float fTime )
{
    RAGE_TRACK( cutsMotionTreeEntity_SetDualAnimation );

    if ( (m_pObserver != NULL) && m_pObserver->IsAttached() )
    {
        m_pObserver->Detach();
        m_pObserver = NULL;
    }

    if ( (pAnim1 != NULL) && (pAnim2 != NULL) )
    {
        crmtRequestAnimation reqAnim1( pAnim1, fTime, 0.0f );
        crmtRequestAnimation reqAnim2( pAnim2, fTime, 0.0f );

        if ( m_pMotionTree->GetSkeleton() != NULL )
        {
            const crSkeletonData &skeletonData = m_pMotionTree->GetSkeleton()->GetSkeletonData();
            crFrameFilterBoneBasic *pBodyFilter = rage_new crFrameFilterBoneBasic( skeletonData );
            crFrameFilterBoneBasic *pFaceFilter = rage_new crFrameFilterBoneBasic( skeletonData );
            
            const crBoneData *pFacialRootBoneData = skeletonData.FindBoneData( "Facial_root" );
            if ( pFacialRootBoneData != NULL )
            {
                // add the root and take away the facial root
                pBodyFilter->AddBoneId( 0, true );
                pBodyFilter->RemoveBone( pFacialRootBoneData, true );

                // add the facial root
                pFaceFilter->AddBone( pFacialRootBoneData, true );
            }

            crmtRequestFilter reqFilter1( reqAnim1, pBodyFilter );
            crmtRequestFilter reqFilter2( reqAnim2, pFaceFilter );

            crmtRequestMerge reqMerge( reqFilter1, reqFilter2 );
            m_pMotionTree->Request( reqMerge );

            if ( m_pObserver == NULL )
            {
                m_pObserver = rage_new crmtObserver;
            }

            m_pObserver->Attach( reqMerge.GetObserver() );
        }
        else
        {
            crmtRequestMerge reqMerge( reqAnim1, reqAnim2 );
            m_pMotionTree->Request( reqMerge );

            if ( m_pObserver == NULL )
            {
                m_pObserver = rage_new crmtObserver;
            }

            m_pObserver->Attach( reqMerge.GetObserver() );
        }
    }
    else if ( pAnim1 != NULL )
    {
        SetAnimation( pAnim1, fTime );
    }
    else if ( pAnim2 != NULL )
    {
        SetAnimation( pAnim2, fTime );
    }
    else
    {
        crmtRequestAnimation reqAnim;
        m_pMotionTree->Request( reqAnim );
    }
}

void cutsMotionTreeEntity::SetClip( const crClip *pClip, float fTime )
{
    RAGE_TRACK( cutsMotionTreeEntity_SetClip );

    if ( (m_pObserver != NULL) && m_pObserver->IsAttached() )
    {
        m_pObserver->Detach();
        m_pObserver = NULL;
    }

    if ( pClip != NULL )
    {
        crmtRequestClip reqClip( pClip, fTime, 0.0f );
        m_pMotionTree->Request( reqClip );

        if ( m_pObserver == NULL )
        {
            m_pObserver = rage_new crmtObserver;
        }

        m_pObserver->Attach( reqClip.GetObserver() );
    }
    else
    {
        crmtRequestClip reqClip;
        m_pMotionTree->Request( reqClip );
    }
}

void cutsMotionTreeEntity::SetDualClip( const crClip *pClip1, const crClip *pClip2, float fTime )
{
    RAGE_TRACK( cutsMotionTreeEntity_SetDualClip );

    if ( (m_pObserver != NULL) && m_pObserver->IsAttached() )
    {
        m_pObserver->Detach();
        m_pObserver = NULL;
    }

    if ( (pClip1 != NULL) && (pClip2 != NULL) )
    {
        crmtRequestClip reqClip1( pClip1, fTime, 0.0f );
        crmtRequestClip reqClip2( pClip2, fTime, 0.0f );

        if ( m_pMotionTree->GetSkeleton() != NULL )
        {
            const crSkeletonData &skeletonData = m_pMotionTree->GetSkeleton()->GetSkeletonData();
            crFrameFilterBoneBasic *pBodyFilter = rage_new crFrameFilterBoneBasic( skeletonData );
            crFrameFilterBoneBasic *pFaceFilter = rage_new crFrameFilterBoneBasic( skeletonData );

            const crBoneData *pFacialRootBoneData = skeletonData.FindBoneData( "Facial_root" );
            if ( pFacialRootBoneData != NULL )
            {
                // add the root and take away the facial root
                pBodyFilter->AddBoneId( 0, true );
                pBodyFilter->RemoveBone( pFacialRootBoneData, true );

                // add the facial root
                pFaceFilter->AddBone( pFacialRootBoneData, true );
            }

            crmtRequestFilter reqFilter1( reqClip1, pBodyFilter );
            crmtRequestFilter reqFilter2( reqClip2, pFaceFilter );

            crmtRequestMerge reqMerge( reqFilter1, reqFilter2 );
            m_pMotionTree->Request( reqMerge );

            if ( m_pObserver == NULL )
            {
                m_pObserver = rage_new crmtObserver;
            }

            m_pObserver->Attach( reqMerge.GetObserver() );
        }
        else
        {
            crmtRequestMerge reqMerge( reqClip1, reqClip2 );
            m_pMotionTree->Request( reqMerge );

            if ( m_pObserver == NULL )
            {
                m_pObserver = rage_new crmtObserver;
            }

            m_pObserver->Attach( reqMerge.GetObserver() );
        }
    }
    else if ( pClip1 != NULL )
    {
        SetClip( pClip1, fTime );
    }
    else if ( pClip2 != NULL )
    {
        SetClip( pClip2, fTime );
    }
    else
    {
        crmtRequestAnimation reqAnim;
        m_pMotionTree->Request( reqAnim );
    }
}

void cutsMotionTreeEntity::SetDualClipAnim( const crClip *pClip, const crAnimation *pAnim, float fTime )
{
    RAGE_TRACK( cutsMotionTreeEntity_SetDualClipAnim );

    if ( (m_pObserver != NULL) && m_pObserver->IsAttached() )
    {
        m_pObserver->Detach();
        m_pObserver = NULL;
    }

    if ( (pClip != NULL) && (pAnim != NULL) )
    {
        crmtRequestClip reqClip( pClip, fTime, 0.0f );
        crmtRequestAnimation reqAnim( pAnim, fTime, 0.0f );

        if ( m_pMotionTree->GetSkeleton() != NULL )
        {
            const crSkeletonData &skeletonData = m_pMotionTree->GetSkeleton()->GetSkeletonData();
            crFrameFilterBoneBasic *pBodyFilter = rage_new crFrameFilterBoneBasic( skeletonData );
            crFrameFilterBoneBasic *pFaceFilter = rage_new crFrameFilterBoneBasic( skeletonData );

            const crBoneData *pFacialRootBoneData = skeletonData.FindBoneData( "Facial_root" );
            if ( pFacialRootBoneData != NULL )
            {
                // add the root and take away the facial root
                pBodyFilter->AddBoneId( 0, true );
                pBodyFilter->RemoveBone( pFacialRootBoneData, true );

                // add the facial root
                pFaceFilter->AddBone( pFacialRootBoneData, true );
            }

            crmtRequestFilter reqFilter1( reqClip, pBodyFilter );
            crmtRequestFilter reqFilter2( reqAnim, pFaceFilter );

            crmtRequestMerge reqMerge( reqFilter1, reqFilter2 );
            m_pMotionTree->Request( reqMerge );

            if ( m_pObserver == NULL )
            {
                m_pObserver = rage_new crmtObserver;
            }

            m_pObserver->Attach( reqMerge.GetObserver() );
        }
        else
        {
            crmtRequestMerge reqMerge( reqClip, reqAnim );
            m_pMotionTree->Request( reqMerge );

            if ( m_pObserver == NULL )
            {
                m_pObserver = rage_new crmtObserver;
            }

            m_pObserver->Attach( reqMerge.GetObserver() );
        }
    }
    else if ( pClip != NULL )
    {
        SetClip( pClip, fTime );
    }
    else if ( pAnim != NULL )
    {
        SetAnimation( pAnim, fTime );
    }
    else
    {
        crmtRequestAnimation reqAnim;
        m_pMotionTree->Request( reqAnim );
    }
}

void cutsMotionTreeEntity::SetTime( float fTime )
{
    if ( (m_pObserver != NULL) && m_pObserver->IsAttached() )
    {
        SetTime( m_pObserver->GetNode(), fTime );
    }

    // If the scheduler was provided, schedule an update and wait for it
    if ( m_pMotionTreeScheduler != NULL )
    {
        m_pMotionTreeScheduler->Schedule( *m_pMotionTree, TIME.GetSeconds() );
        m_pMotionTreeScheduler->WaitOnComplete( *m_pMotionTree );
    }
}

void cutsMotionTreeEntity::Clear()
{
    // clear the animation or clip
    if ( (m_pMotionTree != NULL) && (m_pObserver != NULL) && m_pObserver->IsAttached() )
    {
        if ( m_pObserver->GetNode()->GetNodeType() == crmtNode::kNodeAnimation )
        {
            SetAnimation( NULL, 0.0f );
        }
        else if ( m_pObserver->GetNode()->GetNodeType() == crmtNode::kNodeClip )
        {
            SetClip( NULL, 0.0f );
        }
    }
}

void cutsMotionTreeEntity::SetTime( crmtNode *pNode, float fTime )
{
    if ( pNode->GetNodeType() == crmtNode::kNodeAnimation )
    {
        crmtNodeAnimation *pAnimNode = dynamic_cast<crmtNodeAnimation *>( pNode );
        pAnimNode->GetAnimPlayer().SetTime( fTime );
    }
    else if ( pNode->GetNodeType() == crmtNode::kNodeClip )
    {
        crmtNodeClip *pClipNode = dynamic_cast<crmtNodeClip *>( pNode );
        pClipNode->GetClipPlayer().SetTime( fTime );
    }

    crmtNode *pChildNode = pNode->GetFirstChild();
    while ( pChildNode != NULL )
    {
        SetTime( pChildNode, fTime );
        pChildNode = pChildNode->GetNextSibling();
    }
}

//##############################################################################

cutsAnimatedCameraEntity::cutsAnimatedCameraEntity( const cutfObject* pObject, crmtMotionTree *pMotionTree, 
                                                   crmtMotionTreeScheduler *pMotionTreeScheduler )
                                                   : cutsMotionTreeEntity( pObject, pMotionTree, pMotionTreeScheduler )
                                                   , m_pCameraComponent(NULL)
#if !__FINAL
                                                   , m_pFaceZoomSkeleton(NULL)
                                                   , m_pFacialRootBoneData(NULL)
                                                   , m_bFaceZoomHasFace(false)
                                                   , m_fFaceZoomDistance(0.5f)
                                                   , m_fFaceZoomNearDrawDistance(-1.0f)
                                                   , m_fFaceZoomFarDrawDistance(-1.0f)
#endif // !__FINAL
{
    if ( (m_pMotionTree != NULL) && (m_pMotionTree->GetCreature() != NULL) )
    {
        crCreatureComponentCamera *pCameraComponent = m_pMotionTree->GetCreature()->FindComponent<crCreatureComponentCamera>();
        if ( pCameraComponent != NULL )
        {
            m_pCameraComponent = pCameraComponent;
        }
    }

    m_cCameraCutName[0] = 0;

#if !__FINAL
    m_cFaceZoomName[0] = 0;
#endif // !__FINAL
}

cutsAnimatedCameraEntity::~cutsAnimatedCameraEntity()
{

}

void cutsAnimatedCameraEntity::DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, 
                                             const cutfEventArgs* pEventArgs,  const float UNUSED_PARAM(fTime),const u32 UNUSED_PARAM(StickyId) )
{  
    switch ( iEventId )
    {
    case CUTSCENE_START_OF_SCENE_EVENT:
    case CUTSCENE_RESTART_EVENT:
        {
            if ( m_pCameraComponent != NULL )
            {
                const cutfCameraObject *pCameraObject = dynamic_cast<const cutfCameraObject *>( pObject );

                if ( pCameraObject->GetNearDrawDistance() < 0.0f )
                {
                    m_pCameraComponent->SetNearClip( pManager->GetDefaultNearDrawDistance() );
                }
                else
                {
                    m_pCameraComponent->SetNearClip( pCameraObject->GetNearDrawDistance() );
                }

                if ( pCameraObject->GetFarDrawDistance() < 0.0f )
                {
                    m_pCameraComponent->SetFarClip( pManager->GetDefaultFarDrawDistance() );
                }
                else
                {
                    m_pCameraComponent->SetFarClip( pCameraObject->GetFarDrawDistance() );
                }
            }

            m_cCameraCutName[0] = 0;
        }
        break;
    case CUTSCENE_END_OF_SCENE_EVENT:
        {
#if !__FINAL
            // just to make sure
            SetFaceZoomSkeleton( NULL );

            m_cFaceZoomName[0] = 0;
            m_bFaceZoomHasFace = false;
#endif // !__FINAL

            m_cCameraCutName[0] = 0;
        }
        break;
    case CUTSCENE_CAMERA_CUT_EVENT:
        {
            if ( pEventArgs->GetType() == CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE )
            {
                const cutfCameraCutEventArgs *pCameraCutEventArgs = dynamic_cast<const cutfCameraCutEventArgs *>( pEventArgs );
#if !__FINAL             
				safecpy( m_cCameraCutName, pCameraCutEventArgs->GetName().GetCStr(), sizeof(m_cCameraCutName) );
#endif
                if ( m_pCameraComponent != NULL )
                {
                    if ( pCameraCutEventArgs->GetNearDrawDistance() < 0.0f )
                    {
                        m_pCameraComponent->SetNearClip( pManager->GetDefaultNearDrawDistance() );
                    }
                    else
                    {
                        m_pCameraComponent->SetNearClip( pCameraCutEventArgs->GetNearDrawDistance() );
                    }

                    if ( pCameraCutEventArgs->GetFarDrawDistance() < 0.0f )
                    {
                        m_pCameraComponent->SetFarClip( pManager->GetDefaultFarDrawDistance() );
                    }
                    else
                    {
                        m_pCameraComponent->SetFarClip( pCameraCutEventArgs->GetFarDrawDistance() );
                    }
                }
            }
            else
            {
                m_cCameraCutName[0] = 0;
            }
        }
        break;
    case CUTSCENE_SET_DRAW_DISTANCE_EVENT:
        {
            if ( m_pCameraComponent != NULL )
            {
                const cutfTwoFloatValuesEventArgs *pTwoFloatsEventArgs = dynamic_cast<const cutfTwoFloatValuesEventArgs *>( pEventArgs );
                if ( pTwoFloatsEventArgs != NULL )
                {
                    m_pCameraComponent->SetNearClip( pTwoFloatsEventArgs->GetFloat1() );
                    m_pCameraComponent->SetFarClip( pTwoFloatsEventArgs->GetFloat2() );
                }
            }
        }
        break;

#if !__FINAL
    case CUTSCENE_SET_FACE_ZOOM_EVENT:
        {
            const cutfObjectIdEventArgs* pObjectIdEventArgs = dynamic_cast<const cutfObjectIdEventArgs *>(pEventArgs);
            if ( pObjectIdEventArgs )
            {
                const cutfObject *pFaceZoomObject = pManager->GetObjectById( pObjectIdEventArgs->GetObjectId() );
                if ( pFaceZoomObject && (pFaceZoomObject->GetType() == CUTSCENE_MODEL_OBJECT_TYPE) )
                {
                    const cutfModelObject *pFaceZoomModelObject = dynamic_cast<const cutfModelObject *>( pFaceZoomObject );
                    if ( pFaceZoomModelObject->GetModelType() == CUTSCENE_PED_MODEL_TYPE )
                    {
                        const cutfPedModelObject *pFaceZoomPedModelObject = dynamic_cast<const cutfPedModelObject *>( pFaceZoomModelObject );
                        safecpy( m_cFaceZoomName, pFaceZoomPedModelObject->GetDisplayName().c_str(), sizeof(m_cFaceZoomName) );

                        if ( NOTFINAL_ONLY( !PARAM_cutsIgnoreDicts.Get() && ) (pManager->GetSectionStartTimeList().GetCount() > 0) )
                        {
                            m_bFaceZoomHasFace = pFaceZoomPedModelObject->HasFaceAnimation();
                        }
                        else
                        {
                            char cFaceDirectory[RAGE_MAX_PATH];
                            safecpy( cFaceDirectory, pManager->GetFaceDirectory(), sizeof(cFaceDirectory) );
                            int len = (int) strlen( cFaceDirectory );
                            if ( len == 0 )
                            {
                                char cDirectory[RAGE_MAX_PATH];
                                ASSET.RemoveExtensionFromPath( cDirectory, sizeof(cDirectory), pManager->GetCutsceneFilename() );

                                sprintf( cFaceDirectory, "%s/faces", cDirectory );
                            }

                            char cFaceAnimFilename[RAGE_MAX_PATH];
                            safecpy( cFaceAnimFilename, pFaceZoomPedModelObject->GetFaceAnimationFilename( cFaceDirectory ).GetCStr(), 
                                sizeof(cFaceAnimFilename) );

                            m_bFaceZoomHasFace = ASSET.Exists( cFaceAnimFilename, NULL );
                        }
                    }
                }
            }
        }
        break;
    case CUTSCENE_CLEAR_FACE_ZOOM_EVENT:
        {
            m_cFaceZoomName[0] = 0;
            m_bFaceZoomHasFace = false;
        }
        break;
    case CUTSCENE_SET_FACE_ZOOM_DISTANCE_EVENT:
        {
            const cutfFloatValueEventArgs *pFloatEventArgs = dynamic_cast<const cutfFloatValueEventArgs *>( pEventArgs );
            if ( pFloatEventArgs != NULL )
            {
                m_fFaceZoomDistance = pFloatEventArgs->GetFloat1();
            }
        }
        break;
    case CUTSCENE_SET_FACE_ZOOM_NEAR_DRAW_DISTANCE_EVENT:
        {
            const cutfFloatValueEventArgs *pFloatEventArgs = dynamic_cast<const cutfFloatValueEventArgs *>( pEventArgs );
            if ( pFloatEventArgs != NULL )
            {
                m_fFaceZoomNearDrawDistance = pFloatEventArgs->GetFloat1();
            }
        }
        break;
    case CUTSCENE_SET_FACE_ZOOM_FAR_DRAW_DISTANCE_EVENT:
        {
            const cutfFloatValueEventArgs *pFloatEventArgs = dynamic_cast<const cutfFloatValueEventArgs *>( pEventArgs );
            if ( pFloatEventArgs != NULL )
            {
                m_fFaceZoomFarDrawDistance = pFloatEventArgs->GetFloat1();
            }
        }
        break;
#endif // !__FINAL

    default:
        break;
    }

    cutsMotionTreeEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

#if !__FINAL

const Matrix34& cutsAnimatedCameraEntity::GetFaceZoomMatrix()
{
    if ( IsFaceZoomEnabled() )
    {
        Matrix34 faceMtx;
		m_pFaceZoomSkeleton->GetGlobalMtx( m_pFacialRootBoneData->GetIndex(), RC_MAT34V(faceMtx) );

        Vector3 lookFrom, lookAt;
        faceMtx.GetLookAt( &lookFrom, &lookAt, m_fFaceZoomDistance );

        m_faceZoomMatrix.Identity();
        m_faceZoomMatrix.LookAt( lookAt, lookFrom, YAXIS );
    }

    return m_faceZoomMatrix;
}

void cutsAnimatedCameraEntity::SetFaceZoomSkeleton( const crSkeleton *pSkeleton )
{
    m_pFaceZoomSkeleton = pSkeleton;

    if ( m_pFaceZoomSkeleton != NULL )
    {
        m_pFacialRootBoneData = m_pFaceZoomSkeleton->GetSkeletonData().FindBoneData( GetFaceZoomBoneName() );
    }
    else
    {
        m_pFacialRootBoneData = NULL;
    }
}

#endif // !__FINAL

//##############################################################################

cutsAnimatedParticleEffectEntity::cutsAnimatedParticleEffectEntity( ptxEffectInst *m_pEffectInst, const cutfObject* pObject, crmtMotionTree *pMotionTree, 
                                                   crmtMotionTreeScheduler *pMotionTreeScheduler )
                                                   : cutsMotionTreeEntity( pObject, pMotionTree, pMotionTreeScheduler )
                                                   , m_pEffectInst(m_pEffectInst)
{

}

cutsAnimatedParticleEffectEntity::~cutsAnimatedParticleEffectEntity()
{

}

void cutsAnimatedParticleEffectEntity::DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, 
                                             const cutfEventArgs* pEventArgs,  const float UNUSED_PARAM(fTime), const u32 UNUSED_PARAM(StickyId) )
{  
    switch ( iEventId )
    {
    case CUTSCENE_END_OF_SCENE_EVENT:
        {
            if ( m_pEffectInst != NULL )
            {
                m_pEffectInst->Finish(false);
            }
        }
        break;
    case CUTSCENE_SET_ANIMATION_EVENT:
        {
            if ( m_pEffectInst != NULL )
            {
                m_pEffectInst->Start();
            }
        }
        break;
    case CUTSCENE_SET_CLIP_EVENT:
        {
            const cutsClipEventArgs *pClipEventArgs = dynamic_cast<const cutsClipEventArgs *>( pEventArgs );
            if ( (pClipEventArgs != NULL) && (m_pEffectInst != NULL) )
            {
                InitializeEffectInst( m_pEffectInst, pClipEventArgs->GetClip() );

                m_pEffectInst->Start();
            }
        }
        break;
    case CUTSCENE_CLEAR_ANIM_EVENT:
    case CUTSCENE_STOP_EVENT:
    case CUTSCENE_RESTART_EVENT:
        {
            if ( m_pEffectInst != NULL )
            {
                m_pEffectInst->Stop();
            }
        }
        break;
    default:
        break;
    }

    cutsMotionTreeEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

void cutsAnimatedParticleEffectEntity::InitializeEffectInst( ptxEffectInst* pEffectInst, const crClip *pClip )
{
    if ( pClip->HasProperties() )
    {
        const crProperty *pProperty = pClip->GetProperties()->FindProperty( "CanBeCulled" );
        if ( pProperty != NULL )
        {
			const crPropertyAttribute* pAttrib = pProperty->GetAttribute("CanBeCulled");
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeInt)
			{
				const crPropertyAttributeInt* pIntAttrib = static_cast<const crPropertyAttributeInt*>(pAttrib);
				pEffectInst->SetCanBeCulled( pIntAttrib->GetInt() != 0 );
			}
        }

        pProperty = pClip->GetProperties()->FindProperty( "ColorAlphaTint" );
        if ( pProperty != NULL )
        {
			const crPropertyAttribute* pAttrib = pProperty->GetAttribute("ColorAlphaTint");
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeFloat)
			{
				const crPropertyAttributeFloat* pFloatAttrib = static_cast<const crPropertyAttributeFloat*>(pAttrib);

				pEffectInst->SetAlphaTint( pFloatAttrib->GetFloat() );
			}
        }

        pProperty = pClip->GetProperties()->FindProperty( "ColorTint" );
        if ( pProperty != NULL )
        {
			const crPropertyAttribute* pAttrib = pProperty->GetAttribute("ColorTint");
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeVector4)
			{
				const crPropertyAttributeVector4* pVector4Attrib = static_cast<const crPropertyAttributeVector4*>(pAttrib);
				const Vec4V &v = pVector4Attrib->GetVector4();

				pEffectInst->SetColourTint(v.GetXYZ());
			}
        }

        pProperty = pClip->GetProperties()->FindProperty( "InvertAxis" );
        if ( pProperty != NULL )
        {
			const crPropertyAttribute* pAttrib = pProperty->GetAttribute("InvertAxis");
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeVector3)
			{
				const crPropertyAttributeVector3* pVector3Attrib = static_cast<const crPropertyAttributeVector3*>(pAttrib);
				const Vec3V &v = pVector3Attrib->GetVector3();

				u8 invertFlags = 0;
				if ( v.GetXf() != 0.0f )
				{
					invertFlags |= PTXEFFECTINVERTAXIS_X;
				}

				if ( v.GetYf() != 0.0f )
				{
					invertFlags |= PTXEFFECTINVERTAXIS_Y;
				}

				if ( v.GetZf() != 0.0f )
				{
					invertFlags |= PTXEFFECTINVERTAXIS_Z;
				}

				pEffectInst->SetInvertAxes( invertFlags );
			}
        }

        pProperty = pClip->GetProperties()->FindProperty( "LODScale" );
        if ( pProperty != NULL )
        {
			const crPropertyAttribute* pAttrib = pProperty->GetAttribute("LODScale");
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeFloat)
			{
				const crPropertyAttributeFloat* pFloatAttrib = static_cast<const crPropertyAttributeFloat*>(pAttrib);

				pEffectInst->SetLODScalar( pFloatAttrib->GetFloat() );
			}
        }

        pProperty = pClip->GetProperties()->FindProperty( "OffsetRotation" );
        if ( pProperty != NULL )
        {
			const crPropertyAttribute* pAttrib = pProperty->GetAttribute("OffsetRotation");
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeVector3)
			{
				const crPropertyAttributeVector3* pVector3Attrib = static_cast<const crPropertyAttributeVector3*>(pAttrib);
				const Vec3V &v = pVector3Attrib->GetVector3();

				pEffectInst->SetOffsetRot( v );
			}
        }

        pProperty = pClip->GetProperties()->FindProperty( "OffsetTranslation" );
        if ( pProperty != NULL )
        {
			const crPropertyAttribute* pAttrib = pProperty->GetAttribute("OffsetTranslation");
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeVector3)
			{
				const crPropertyAttributeVector3* pVector3Attrib = static_cast<const crPropertyAttributeVector3*>(pAttrib);
				const Vec3V &v = pVector3Attrib->GetVector3();

				pEffectInst->SetOffsetPos( v );
			}
        }

        pProperty = pClip->GetProperties()->FindProperty( "OverrideSpawnDistance" );
        if ( pProperty != NULL )
        {
			const crPropertyAttribute* pAttrib = pProperty->GetAttribute("OverrideSpawnDistance");
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeFloat)
			{
				const crPropertyAttributeFloat* pFloatAttrib = static_cast<const crPropertyAttributeFloat*>(pAttrib);

				pEffectInst->SetOverrideDistTravelled( pFloatAttrib->GetFloat() );
			}
        }

        pProperty = pClip->GetProperties()->FindProperty( "UserZoomScale" );
        if ( pProperty == NULL )
        {
            // backwards compatibility
            pProperty = pClip->GetProperties()->FindProperty( "UseZoomScale" ); // stupid typo
        }

        if ( pProperty != NULL )
        {
			const crPropertyAttribute* pAttrib = pProperty->GetAttribute("UserZoomScale");
			if(pAttrib == NULL)
			{
				pAttrib = pProperty->GetAttribute("UseZoomScale");
			}
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeFloat)
			{
				const crPropertyAttributeFloat* pFloatAttrib = static_cast<const crPropertyAttributeFloat*>(pAttrib);

				pEffectInst->SetUserZoom( pFloatAttrib->GetFloat() );
			}
        }
    }
}

//#############################################################################

cutsAnimatedLightEntity::cutsAnimatedLightEntity( const cutfObject* pObject, crmtMotionTree *pMotionTree, 
                                                 crmtMotionTreeScheduler *pMotionTreeScheduler )
                                                 : cutsMotionTreeEntity( pObject, pMotionTree, pMotionTreeScheduler )
#if __BANK
                                                 , m_pEditLightInfo(NULL)
#endif // __BANK
{

}

cutsAnimatedLightEntity::~cutsAnimatedLightEntity()
{

}

void cutsAnimatedLightEntity::DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs,  const float UNUSED_PARAM(fTime), const u32 UNUSED_PARAM(StickyId) )
{
#if __BANK
    switch ( iEventId )
    {
    case CUTSCENE_START_OF_SCENE_EVENT:
        {
           m_pEditLightInfo = pManager->GetEditLightInfo( pObject->GetObjectId() );
        }
        break;
    case CUTSCENE_END_OF_SCENE_EVENT:
        {
            m_pEditLightInfo = NULL;
        }
        break;
    }
#endif // __BANK

    cutsMotionTreeEntity::DispatchEvent( pManager, pObject, iEventId, pEventArgs );
}

#if !__FINAL

void cutsAnimatedLightEntity::DebugDraw() const
{
    if ( (m_pMotionTree == NULL) || (m_pMotionTree->GetCreature() == NULL) )
    {
        return;
    }

    m_pMotionTree->GetCreature()->DebugDraw();

    const crCreatureComponentLight *pLightComponent = m_pMotionTree->GetCreature()->FindComponent<crCreatureComponentLight>();
    if ( pLightComponent == NULL )
    {
        return;
    }

    Vector3 pos;
#if __BANK
    if ( (m_pEditLightInfo != NULL) && m_pEditLightInfo->bOverridePosition )
    {
        pos = m_pEditLightInfo->vPosition;
    }
    else
    {
        pos = RCC_MATRIX34(pLightComponent->GetRootMatrix()).d;
    }
#else // __BANK
    pos = RCC_MATRIX34(pLightComponent->GetRootMatrix()).d;
#endif // __BANK

    Color32 clr;

#if __BANK
    if ( (m_pEditLightInfo != NULL) && m_pEditLightInfo->bOverrideColor )
    {
        clr = m_pEditLightInfo->vColor;
    }
    else
    {
        clr = pLightComponent->GetColor();
    }
#else // __BANK
    clr = pLightComponent->GetColor();
#endif // __BANK

    bool bOldLighting = grcLighting( false );

    grcWorldIdentity();
    grcColor( clr );
    grcDrawLabel( pos, 10, -10, GetObject()->GetDisplayName().c_str() );

    grcLighting( bOldLighting );
}

#endif // !__FINAL

//##############################################################################


#if !__NO_OUTPUT
void cutsAnimationManagerEntity::CommonDebugStr(char * debugStr)
{
	sprintf(debugStr, "cutsAnimationManagerEntity ");
}
#endif //!__NO_OUTPUT

} // namespace rage
