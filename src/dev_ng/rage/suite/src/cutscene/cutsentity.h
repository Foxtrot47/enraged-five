// 
// cutscene/cutsentity.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTSCENE_CUTSENTITY_H 
#define CUTSCENE_CUTSENTITY_H 

#include "atl/array.h"

namespace rage {

//##############################################################################

enum ECutsceneEntityType
{
    CUTSCENE_UNIQUE_ENTITY_TYPE,
    CUTSCENE_SINGLETON_ENTITY_TYPE,
    CUTSCENE_NUM_ENTITY_TYPES
};

class cutfEventArgs;
class cutfObject;
class cutsManager;

//##############################################################################

// PURPOSE: Base class for a cut scene entity that is capable of receiving
//  events from the cut scene manager.  Derive to create your own game-specific
//  entities for handling events for models, camera, lights, particle effects, 
//  etc.
class cutsEntity
{
public:
    cutsEntity();
    virtual ~cutsEntity() {}

    // PURPOSE: Returns the type of id of the derived class.  Useful for casting.
    // RETURNS: The type id of this class
    virtual s32 GetType() const = 0;
    
    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL, const float fTime = 0.0f, const u32 StickyId = 0 ) = 0;

#if !__FINAL
    // PURPOSE: Retrieves the value of debug draw.
    // RETURNS: true if we should call DebugDraw()
    bool GetDebugDraw() const;

    // PURPOSE: Sets the value of debug draw.
    // PARAMS:
    //    bDraw - to draw or not to draw
    virtual void SetDebugDraw( bool bDraw );

    // PURPOSE: Draws debug text and/or objects for this entity.
    // NOTES:  The cutsManager will call this function when GetDebugDraw returns true.
    virtual void DebugDraw() const;

protected:
    bool m_bDebugDraw;
#endif // !__FINAL
};

#if !__FINAL

inline bool cutsEntity::GetDebugDraw() const
{
    return m_bDebugDraw;
}

inline void cutsEntity::SetDebugDraw( bool bDraw )
{
    m_bDebugDraw = bDraw;
}

inline void cutsEntity::DebugDraw() const
{
    // do nothing in the base class
}

#endif // !__FINAL

//##############################################################################

// PURPOSE: Base class for a "unique" cut scene entity that is associated
//  with one and only one cut scene object
class cutsUniqueEntity : public cutsEntity
{
public:
    cutsUniqueEntity();
    cutsUniqueEntity( const cutfObject* pObject );
    virtual ~cutsUniqueEntity() {}

    // PURPOSE: Returns the type of id of the derived class.  Useful for casting.
    // RETURNS: The type id of this class
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the cut scene object that is associated with this entity.
    // RETURNS: The cut scene object
    const cutfObject* GetObject() const;

    // PURPOSE: Sets the cut scene object that is associated with this entity
    // PARAMS:
    //    pObject - the object to associate with this entity
    void SetObject( const cutfObject* pObject );

protected:
    const cutfObject* m_pObject;
};

inline s32 cutsUniqueEntity::GetType() const
{
    return CUTSCENE_UNIQUE_ENTITY_TYPE;
}

inline const cutfObject* cutsUniqueEntity::GetObject() const
{
    return m_pObject;
}

inline void cutsUniqueEntity::SetObject( const cutfObject* pObject )
{
    m_pObject = pObject;
}

//##############################################################################

// PURPOSE: Base class for a "singleton" cut scene entity that will receive
//  events for multiple cut scene objects.
class cutsSingletonEntity : public cutsEntity
{
public:
    cutsSingletonEntity();
    cutsSingletonEntity( atArray<const cutfObject*>& pObjectList );
    virtual ~cutsSingletonEntity() {}

    // PURPOSE: Returns the type of id of the derived class.  Useful for casting.
    // RETURNS: The type id of this class
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the list of objects that this entity is responsible for.
    // RETURNS: The list of cutscene objects.
    atArray<const cutfObject*>& GetObjectList();

    // PURPOSE: Adds an object to the list
    // PARAMS:
    //    pObject - the object to add
    void AddObject( const cutfObject *pObject );

    // PURPOSE: Removes an object from the list
    // PARAMS:
    //    pObject - the object to remove
    void RemoveObject( const cutfObject *pObject );

    // PURPOSE: Retrieves the object, given its unique id
    // PARAMS:
    //    iObjectId - the id of the object
    // RETURNS: the object with the specified id
    const cutfObject* GetObjectById( s32 iObjectId );

protected:
    atArray<const cutfObject*> m_pObjectList;
};

inline s32 cutsSingletonEntity::GetType() const
{
    return CUTSCENE_SINGLETON_ENTITY_TYPE;
}

inline atArray<const cutfObject*>& cutsSingletonEntity::GetObjectList()
{
    return m_pObjectList;
}

//##############################################################################

} // namespace rage

#endif // CUTSCENE_CUTSENTITY_H 
