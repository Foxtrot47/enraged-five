// 
// cutscene/cutseventargs.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutseventargs.h"
#include "cutsoptimisations.h"

CUTS_OPTIMISATIONS()

namespace rage {

//##############################################################################

const char* cutsEventArgs::c_cutscenePlaybackEventArgsTypeNames[] = 
{
    "Update",
    "Animation",
    "Dual Animation",
    "Clip",
    "Dual Clip",
    "Dual Clip Anim"
};

void cutsEventArgs::InitClass()
{
    cutfEventArgs::SetEventArgsTypeNameFunc( MakeFunctorRet( &GetPlaybackTypeName ), CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY );
}

void cutsEventArgs::ShutdownClass()
{
    cutfEventArgs::SetEventArgsTypeNameFunc( 0, CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY );
}

const char* cutsEventArgs::GetPlaybackTypeName( s32 iEventArgsType )
{
    if ( (iEventArgsType >= CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY) && (iEventArgsType <= CUTSCENE_LAST_PLAYBACK_EVENT_ARGS_TYPE) )
    {
        int iEventArgsTypeOffset = iEventArgsType - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY;
        return c_cutscenePlaybackEventArgsTypeNames[iEventArgsTypeOffset];
    }

    return "";
}

//##############################################################################

cutsUpdateEventArgs::cutsUpdateEventArgs( float fTime, float fDeltaTime, float fSectionStartTime, float fSectionDuration )
: m_fTime(fTime)
, m_fDeltaTime(fDeltaTime)
, m_fSectionStartTime(fSectionStartTime)
, m_fSectionDuration(fSectionDuration)
{

}

cutsUpdateEventArgs::~cutsUpdateEventArgs()
{

}

//##############################################################################

cutsAnimationEventArgs::cutsAnimationEventArgs( float fTime, const crAnimation *pAnim )
: m_fTime(fTime)
, m_pAnimation(pAnim)
{

}

cutsAnimationEventArgs::~cutsAnimationEventArgs()
{

}

//##############################################################################

cutsDualAnimationEventArgs::cutsDualAnimationEventArgs( float fTime, const crAnimation *pAnim1, const crAnimation *pAnim2 )
: cutsAnimationEventArgs(fTime,pAnim1)
, m_pAnimation2(pAnim2)
{

}

cutsDualAnimationEventArgs::~cutsDualAnimationEventArgs()
{

}

//##############################################################################
#if HACK_GTA4
cutsClipEventArgs::cutsClipEventArgs( float fTime, const crClip *pClip, const char* pAnimDict )
: m_fTime(fTime)
, m_pClip(pClip)
, m_pAnimDict(pAnimDict)
{

}

#else
cutsClipEventArgs::cutsClipEventArgs( float fTime, const crClip *pClip )
: m_fTime(fTime)
, m_pClip(pClip)
{

}
#endif
cutsClipEventArgs::~cutsClipEventArgs()
{

}

//##############################################################################
#if HACK_GTA4
cutsDualClipEventArgs::cutsDualClipEventArgs( float fTime, const crClip *pClip1, const crClip *pClip2, const char* pAnimDict )
: cutsClipEventArgs(fTime,pClip1, pAnimDict)
, m_pClip2(pClip2)
{

}

#else

cutsDualClipEventArgs::cutsDualClipEventArgs( float fTime, const crClip *pClip1, const crClip *pClip2 )
: cutsClipEventArgs(fTime,pClip1)
, m_pClip2(pClip2)
{

}
#endif

cutsDualClipEventArgs::~cutsDualClipEventArgs()
{

}

//##############################################################################
#if HACK_GTA4
cutsDualClipAnimEventArgs::cutsDualClipAnimEventArgs( float fTime, const crClip *pClip, const crAnimation *pAnimation, const char* pAnimDict )
: cutsClipEventArgs(fTime,pClip,pAnimDict)
, m_pAnimation(pAnimation)
{

}
#else
cutsDualClipAnimEventArgs::cutsDualClipAnimEventArgs( float fTime, const crClip *pClip, const crAnimation *pAnimation )
: cutsClipEventArgs(fTime,pClip)
, m_pAnimation(pAnimation)
{

}
#endif

cutsDualClipAnimEventArgs::~cutsDualClipAnimEventArgs()
{

}

//##############################################################################

cutsDictionaryLoadedEventArgs::cutsDictionaryLoadedEventArgs( crClipDictionary* pDict, s32 iSection )
: m_pDict(pDict)
, m_iSection(iSection)
{

}

cutsDictionaryLoadedEventArgs::~cutsDictionaryLoadedEventArgs()
{

}

} // namespace rage
