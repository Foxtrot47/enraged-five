// 
// cutscene/cutsanimentity.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTSCENE_CUTSANIMENTITY_H 
#define CUTSCENE_CUTSANIMENTITY_H 

#include "cutsentity.h"

#include "atl/map.h"
#include "atl/vector.h"
#include "cranimation/animdictionary.h"
#include "crclip/clipdictionary.h"
#include "cutfile/cutfdefines.h"
#include "paging/ref.h"
#include "vector/matrix34.h"

namespace rage {

class crAnimation;
class crAnimPlayer;
class crBoneData;
class crClip;
class crClipPlayer;
class crCreatureComponentCamera;
class crmtMotionTree;
class crmtMotionTreeScheduler;
class crmtNode;
class crmtObserver;
class crSkeleton;
class cutfObjectIdNameEventArgs;
class ptxEffectInst;
struct SEditCutfLightInfo;
class cutfObjectIdEventArgs;

//#############################################################################

// Turn this on if you want to debug as if you have 2 dictionary buffers, rather than 3
#define CUTSCENE_DEBUG_FINAL 0

//##############################################################################

#if !__NO_OUTPUT
#define cutsAnimManagerDebugf(fmt,...) char debugStr[256]; CommonDebugStr(debugStr); cutsDebugf1("%s" fmt, debugStr,##__VA_ARGS__);
#else
#define cutsAnimManagerDebugf(fmt,...) do {} while(false)
#endif //!__NO_OUTPUT

// PURPOSE: A class for an Animation Manager Entity.  Takes care of the streaming
//  of animation dictionaries and updating of all animated objects with the
//  correct animations and clips at the appropriate times.
class cutsAnimationManagerEntity : public cutsUniqueEntity
{
public:
    cutsAnimationManagerEntity( const cutfObject* pObject );
    virtual ~cutsAnimationManagerEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL,  const float fTime = 0.0f, const u32 StickyId = 0 );

    const crAnimation* GetAnimation( const char* pName ) const;
    const crAnimation* GetAnimation( const char* pName, int iAnimBuffer ) const;

    const crAnimation* GetAnimation( crAnimDictionary::AnimKey animKey ) const;
    const crAnimation* GetAnimation( crAnimDictionary::AnimKey animKey, int iAnimBuffer ) const;

    const crClip* GetClip( const char* pName ) const;
    const crClip* GetClip( const char* pName, int iClipBuffer ) const;

    const crClip* GetClip( crClipDictionary::ClipKey clipKey ) const;
    const crClip* GetClip( crClipDictionary::ClipKey clipKey, int iClipBuffer ) const;
    const crClip* GetClipUsingAnimSection( crClipDictionary::ClipKey clipKey, s32 animSection) const;

    // PURPOSE: Retrieves the object that is capable of loading animations.  Override this to 
    //    provide your own loader object.
    // RETURNS: The anim loader object.  If NULL, the crAnimDictionary's default loader will be used.
    virtual crAnimLoader* GetAnimLoader() const;

    // PURPOSE: Retrieves the object that is capable of loading clips.  Override this to 
    //    provide your own loader object.
    // RETURNS: The clip loader object.  If NULL, the crClipDictionary's default loader will be used.
    virtual crClipLoader* GetClipLoader() const;

#if !__FINAL
    // PURPOSE: Returns the memory usage for the current buffer.
    // RETURNS: The memory usage in kilobytes.
    int GetMemoryUsage() const;

    // PURPOSE: Returns the memory usage for the indicated buffer.
    // RETURNS: The memory usage in kilobytes.
    int GetMemoryUsage( int iBuffer ) const;
#endif // !__FINAL

	const crClip* GetCameraAnimForTime(const cutfObject* pObject, float CurrentTime, cutsManager* pManager);

	// PURPOSE: Appends the section number to the name, if needed
	// PARAMS:
	//    pDest - the destination string
	//    destLen - the number of bytes int the destination string
	//    pName - the name
	//    iSectionIndex - the section number to append (will be appended to pName like so:  _%d)
	//    iNumSections - the total number of sections.
	// RETURNS: true if pDest contains the sectioned name, otherwise false.
	// NOTES: Will return false if iSectionIndex >= iNumSections.  pDest and pName can be the same.
	bool GetSectionedName( char* pDest, int destLen, const char* pName, int iSectionIndex, int iNumSections );


	// PURPOSE: Appends the section number to the name, if needed
	// PARAMS:
	//    pDest - the destination string
	//    destLen - the number of bytes int the destination string
	//    pName - the name
	//    iSectionIndex - the section number to append (will be appended to pName like so:  _%d)
	//    iNumSections - the total number of sections.
	// RETURNS: true if pDest contains the sectioned name, otherwise false.
	// NOTES: Will return false if iSectionIndex >= iNumSections.  pDest and pName can be the same.
	bool GetSectionedName( u32 AnimHash, u32 &FinalHashString, int iSectionIndex, int iNumSections);

protected:
    // PURPOSE: Returns a value that provides a delay between when we can unload one dictionary and load up the next.
    //    This may be necessary if your animation updates ocurr on a thread other than the main thread.
    // RETURNS:  0.0f by default (no delay)
    virtual float GetLoadNextDictionaryDelay() const;

    // PURPOSE: Loads the indicated dictionary into the specified buffer
    // PARAMS:
    //    pManager - the cutsManager (provides the scene file name)
    //    iObjectId - the object that will do the loading
    //    pName - the name of the dictionary to load
    //    iBuffer - the buffer to load the dictionary into
    // RETURNS: true if we loaded (or started loading)
    // NOTES:  This uses ASSET, crAnimation, and crClip for file loading.  Override to implement
    //    your own dictionary loading scheme.
    virtual bool LoadDictionary( cutsManager *pManager, s32 iObjectId, const char* pName, int iBuffer );

    // PURPOSE: Unloads the dictionary from the specified buffer.
    // PARAMS:
    //    pManager - the cutsManager
    //    iBuffer - the buffer to unload the dictionary from
    // RETURNS: true if we unloaded (or started to unloading)
    // NOTES: Override to implement your own dictionary unloading scheme.
    virtual bool UnloadDictionary( cutsManager *pManager, int iBuffer );

	virtual bool IsDictionaryLoaded(int iBuffer); 

    // PURPOSE: Unloads all animations and clips from all buffers
    // PARAMS:
    //    pManager - the cutsManager
    void UnloadAllDictionaries( cutsManager *pManager );

    // PURPOSE: Cancels any streaming or pending dictionary loads.  
    // PARAMS:
    //    pManager - the cutsManager (provides the API for retrieving the cutsEntity and cutfObject)
    //    pObject - the cut scene object that is associated with this entity.
    // RETURNS: true if we were loading, otherwise false.
    // NOTES: Streaming is not included with this module, so users should override the function appropriately.  This
    //    function will just set the "is loaded" flag to true.
    virtual bool CancelLoads( cutsManager *pManager, const cutfObject* pObject );

    // PURPOSE: Determines the sectioned anim name(s) for the given cutfObjectIdNameEventArgs.
    // PARAMS:
    //    pManager - the cutsManager (provides the API for retrieving the cutsEntity and cutfObject)
    //    iObjectId - the object that wants the animation
    //    pBaseAnimName - the base name of the object's animation
    //    pAnimName - out parameter for the name of the primary animation
    //    iAnimNameLen - the length of the pAnimName buffer
    //    pFaceAnimName - out parameter for the name of the secondary (face) animation
    //    pFaceAnimName - the length of the pFaceAnimName buffer
    // RETURNS: true on success, otherwise false
    bool GetSectionedAnimNames( cutsManager *pManager, u32 iObjectId, const char *pBaseAnimName, 
        char *pAnimName, int iAnimNameLen, char *pFaceAnimName, int iFaceAnimNameLen );
	
	// PURPOSE: Determines the sectioned anim name(s) for the given cutfObjectIdNameEventArgs.
	// PARAMS:
	//    pManager - the cutsManager (provides the API for retrieving the cutsEntity and cutfObject)
	//    iObjectId - the object that wants the animation
	//    pBaseAnimName - the base name of the object's animation
	//    pAnimName - out parameter for the name of the primary animation
	//    iAnimNameLen - the length of the pAnimName buffer
	//    pFaceAnimName - out parameter for the name of the secondary (face) animation
	//    pFaceAnimName - the length of the pFaceAnimName buffer
	// RETURNS: true on success, otherwise false
	bool GetSectionedAnimNames( cutsManager *pManager, u32 iObjectId, u32 BaseAnimHash,  u32 &AnimHash );

    // PURPOSE: Dispatch the appropriate Set Animation event to the specified object.
    // PARAMS:
    //    pManager - the cutsManager (provides the API for retrieving the cutsEntity and cutfObject)
    //    iObjectId - the object id to dispatch the event to
    //    fSectionTime - the time in the animation
    //    pAnimName1 - the name of the primary animation
    //    pAnimName2 - the name of the secondary animation, if any
    virtual bool SetEntityAnimation( cutsManager *pManager, s32 iObjectId, float fSectionTime, const char *pAnimName1, const char* pAnimName2=NULL );
	
	// PURPOSE: Dispatch the appropriate Set Animation event to the specified object.
	// PARAMS:
	//    pManager - the cutsManager (provides the API for retrieving the cutsEntity and cutfObject)
	//    iObjectId - the object id to dispatch the event to
	//    fSectionTime - the time in the animation
	//    pAnimName1 - the name of the primary animation
	//    pAnimName2 - the name of the secondary animation, if any
	virtual bool SetEntityAnimation( cutsManager *pManager, s32 iObjectId, float fSectionTime, u32 Anim1Hash, u32 Anim2Hash );

    // PURPOSE: Dispatches the event to the object specified in the cutfObjectIdNameEventArgs.
    // PARAMS:
    //    pManager - the cutsManager (provides the API for retrieving the cutsEntity and cutfObject)
    //    pObjectIdNameEventArgs - the event args containing the object id of the entity to dispatch to
    virtual void ClearEntityAnimation( cutsManager *pManager, const cutfObjectIdEventArgs *pObjectIdNameEventArgs );

    // PURPOSE: Clears all entity animations
    // PARAMS:
    //    pManager - the cutsManager (provides the API for retrieving the cutsEntity and cutfObject)
    void ClearAllEntityAnimations( cutsManager *pManager );

	// PURPOSE: Request the anim dictionary for the given section
	// PARAMS:
	// NOTES:
	void RequestAnimDictionaryforSection(cutsManager *pManager,const cutfObject* pObject, s32 iSection );
	
	// PURPOSE: Releases dictionaries relative to the current section, except the reserved section.
	// The reserve section is used when skipping, so that the dictionary is not streamed out from underneath
	// the entities using it. 
	// PARAMS: Set ReserveSection = -1 if its not needed.
	// NOTES:
	void ReleaseUnusedAnimDictionaries(cutsManager *pManager, int iSection, int ReserveSection);
	
	// PURPOSE: Checks to see if the current anim section is valid. 
	// PARAMS: 
	// NOTES:
	bool IsSectionValidToLoad(s32 CurrentSection) const; 

	// PURPOSE: Gets the next valid section from the current section
	// NOTES:
	s32 GetNextValidSectionToLoad(s32 CurrentSection); 
	
	// PURPOSE: Gets the previous valid section from the current section
	// NOTES:
	s32 GetPreviousValidSectionToLoad(s32 CurrentSection); 

#if !__NO_OUTPUT
	void CommonDebugStr(char * debugStr);
#endif //!__NO_OUTPUT

#if !__FINAL && !CUTSCENE_DEBUG_FINAL
    static const int c_numBuffers = 4;  // last, current, next, and the one after (for scrubbing)
	static bool ms_KeepExtraDictionaries; 
#else // !__FINAL
    static const int c_numBuffers = 3;  // current, next, and the one after
#endif // !__FINAL

    struct SDictionaryData
    {
        pgRef<crAnimDictionary> pAnimDictionary;
        pgRef<crClipDictionary> pClipDictionary;
        char cName[CUTSCENE_OBJNAMELEN];
        bool bAnimDictIsResource;
        bool bClipDictIsResource;
		bool bValidForLoading; 

#if !__FINAL
        int iAnimDictionarySize;
        int iClipDictionarySize;
#endif // !__FINAL

        SDictionaryData()
            : pAnimDictionary(NULL)
            , pClipDictionary(NULL)
            , bAnimDictIsResource(false)
            , bClipDictIsResource(false)
			, bValidForLoading(false)
#if !__FINAL
            , iAnimDictionarySize(0)
            , iClipDictionarySize(0)
#endif // !__FINAL
        {
            cName[0] = 0;
        }
    };

    atVector <SDictionaryData> m_Dictionaries; //assign a buffer for the number of sections to make skipping easier

	char m_cDictionaryBaseName[CUTSCENE_OBJNAMELEN];
    bool m_bAnimsAreCompressed;
    s32 m_iCurrentAnimSection;

	struct SAnimPlayStruct
	{
		s32 iObjectid; 
		u32 AnimRef; 
		u32 PartialAnimHash;
		s32 AnimSectionAnimWasSet; 
	};
	
	atArray<SAnimPlayStruct> m_ObjectsToBeAnimated; 

  //  atMap<s32, SAnimPlayStruct> m_objectIdToAnimBaseNameMap;

    atMap<s32, crAnimDictionary::AnimKey> m_objectIdToAnimKeyMap;
    atMap<s32, crClipDictionary::ClipKey> m_objectIdToClipKeyMap;

};

inline const crAnimation* cutsAnimationManagerEntity::GetAnimation( const char* pName ) const
{
    return GetAnimation( pName, m_iCurrentAnimSection );
}

inline const crAnimation* cutsAnimationManagerEntity::GetAnimation( crAnimDictionary::AnimKey animKey ) const
{
    return GetAnimation( animKey, m_iCurrentAnimSection );
}

inline const crClip* cutsAnimationManagerEntity::GetClip( const char* pName ) const
{
    return GetClip( pName, m_iCurrentAnimSection );
}

inline const crClip* cutsAnimationManagerEntity::GetClip( crClipDictionary::ClipKey clipKey ) const
{
    return GetClip( clipKey, m_iCurrentAnimSection );
}

inline const crClip* cutsAnimationManagerEntity::GetClipUsingAnimSection( crClipDictionary::ClipKey clipKey, s32 animSection ) const
{
	return GetClip( clipKey, animSection );
}

inline crAnimLoader* cutsAnimationManagerEntity::GetAnimLoader() const
{
    return NULL;
}

inline crClipLoader* cutsAnimationManagerEntity::GetClipLoader() const
{
    return NULL;
}

#if !__FINAL

inline int cutsAnimationManagerEntity::GetMemoryUsage() const
{
    return GetMemoryUsage( m_iCurrentAnimSection );
}

#endif // !__FINAL

inline float cutsAnimationManagerEntity::GetLoadNextDictionaryDelay() const
{
    return 0.0f;
}

//##############################################################################

// PURPOSE: Base class for an entity that can play animations and/or clips.
class cutsAnimatedEntity : public cutsUniqueEntity
{
public:
    cutsAnimatedEntity( const cutfObject* pObject );
    virtual ~cutsAnimatedEntity() = 0;

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL,  const float fTime = 0.0f, const u32 StickyId = 0 );

#if !__FINAL
    // PURPOSE: Retrieves the memory usage for this object.  Typically, this is only useful
    //    for animated models.
    // RETURNS: The memory usage in kilobytes.
    virtual int GetMemoryUsage() const;
#endif // !__FINAL

protected:
    // PURPOSE: Sets up the motion tree to play the animation
    // PARAMS:
    //    pAnim - the animation to play, or NULL to clear the current animation
    //    fTime - time in seconds
    // NOTES:  Deprecated.  All animations are now contained in clips.
    virtual void SetAnimation( const crAnimation *pAnim, float fTime );

    // PURPOSE: Sets up the motion tree to play the animations
    // PARAMS:
    //    pAnim1 - the primary animation to play, or NULL to clear the current primary animation
    //    pAnim2 - the secondary animation to play, or NULL to clear the current secondary animation
    //    fTime - time in seconds
    // NOTES:  Deprecated.  All animations are now contained in clips.
    virtual void SetDualAnimation( const crAnimation *pAnim1, const crAnimation *pAnim2, float fTime );

    // PURPOSE: Sets up the motion tree to play the clip
    // PARAMS:
    //    pClip - the clip to play, or NULL to clear the current clip
    //    fTime - time in seconds
    // NOTES:  This function is called for every animated entity regardless of the animations being sectioned,
    //    except in the case of Ped Models.  When the animations are not sectioned, it will be called for a Ped 
    //    Model only when it doesn't have a face.
    virtual void SetClip( const crClip *pClip, float fTime );
    
    // PURPOSE: Sets up the motion tree to play the clips
    // PARAMS:
    //    pClip1 - the primary clip to play, or NULL to clear the current primary clip
    //    pClip2 - the secondary clip to play, or NULL to clear the current secondary clip
    //    fTime - time in seconds
    // NOTES:  This will only be called for a Ped Model when the animations are not sectioned, and it
    //    has a face animation and that face is contained in a clip.  Normally, faces are not in clips.
    virtual void SetDualClip( const crClip *pClip1, const crClip *pClip2, float fTime );

    // PURPOSE: Sets up the motion tree to play the clip and anim
    // PARAMS:
    //    pClip - the primary clip to play, or NULL to clear the current primary clip
    //    pAnim - the secondary anim to play, or NULL to clear the current secondary anim
    //    fTime - time in seconds
    // NOTES:  This will only be called for a Ped Model when the animations are not sectioned and it has
    //    a normal face animation.
    virtual void SetDualClipAnim( const crClip *pClip, const crAnimation *pAnim, float fTime );

    // PURPOSE: Sets the current motion tree animations or clips to the specified time.
    // PARAMS:
    //    fTime - time in seconds
    virtual void SetTime( float fTime ) = 0;

    // PURPOSE: Clears the animation or clip.
    virtual void Clear() = 0;
};

#if !__FINAL

inline int cutsAnimatedEntity::GetMemoryUsage() const
{
    return 0;
}

#endif // !__FINAL

//##############################################################################

class cutsAnimPlayerEntity : public cutsAnimatedEntity
{
public:
    cutsAnimPlayerEntity( const cutfObject* pObject, crAnimPlayer *pAnimPlayer );
    virtual ~cutsAnimPlayerEntity();

    // PURPOSE: Retrieves the anim player for this entity.  This will only be valid of there is no motion tree.
    // RETURNS: The anim player.
    crAnimPlayer* GetAnimPlayer() const;

    // PURPOSE: Sets the anim player for this entity.  If there's a motion tree, it will be removed.
    // PARAMS:
    //    pAnimPlayer - the anim player
    void SetAnimPlayer( crAnimPlayer *pAnimPlayer );

protected:
    // PURPOSE: Sets up the motion tree to play the animation
    // PARAMS:
    //    pAnim - the animation to play, or NULL to clear the current animation
    //    fTime - time in seconds
    // NOTES:  Deprecated.  All animations are now contained in clips.
    virtual void SetAnimation( const crAnimation *pAnim, float fTime );

    // PURPOSE: Sets up the motion tree to play the animation
    // PARAMS:
    //    pAnim1 - the primary animation to play, or NULL to clear the current primary animation
    //    pAnim2 - the secondary animation to play, or NULL to clear the current secondary animation
    //    fTime - time in seconds
    // NOTES:  Deprecated.  All animations are now contained in clips.
    virtual void SetDualAnimation( const crAnimation *pAnim1, const crAnimation *pAnim2, float fTime );

    // PURPOSE: Sets up the motion tree to play the clip and anim
    // PARAMS:
    //    pClip - the primary clip to play, or NULL to clear the current primary clip
    //    pAnim - the secondary anim to play, or NULL to clear the current secondary anim
    //    fTime - time in seconds
    // NOTES:  This will only be called for a Ped Model when the animations are not sectioned and it has
    //    a normal face animation.
    virtual void SetDualClipAnim( const crClip *pClip, const crAnimation *pAnim, float fTime );

    // PURPOSE: Clears the animation or clip.
    virtual void Clear();

    // PURPOSE: Sets the current motion tree animations or clips to the specified time.
    // PARAMS:
    //    fTime - time in seconds
    virtual void SetTime( float fTime );

    crAnimPlayer *m_pAnimPlayer;
};

inline crAnimPlayer* cutsAnimPlayerEntity::GetAnimPlayer() const
{
    return m_pAnimPlayer;
}

inline void cutsAnimPlayerEntity::SetAnimPlayer( crAnimPlayer *pAnimPlayer )
{
    m_pAnimPlayer = pAnimPlayer;
}

//##############################################################################

class cutsClipPlayerEntity : public cutsAnimatedEntity
{
public:
    cutsClipPlayerEntity( const cutfObject* pObject, crClipPlayer *pClipPlayer );
    virtual ~cutsClipPlayerEntity();

    // PURPOSE: Retrieves the clip player for this entity.  This will only be valid of there is no motion tree.
    // RETURNS: The anim player.
    crClipPlayer* GetClipPlayer() const;

    // PURPOSE: Sets the clip player for this entity.  If there's a motion tree, it will be removed.
    // PARAMS:
    //    pAnimPlayer - the anim player
    void SetClipPlayer( crClipPlayer *pClipPlayer );

protected:
    // PURPOSE: Sets up the motion tree to play the clip
    // PARAMS:
    //    pClip - the clip to play, or NULL to clear the current clip
    //    fTime - time in seconds
    // NOTES:  This function is called for every animated entity regardless of the animations being sectioned,
    //    except in the case of Ped Models.  When the animations are not sectioned, it will be called for a Ped 
    //    Model only when it doesn't have a face.
    virtual void SetClip( const crClip *pClip, float fTime );

    // PURPOSE: Sets up the motion tree to play the clips
    // PARAMS:
    //    pClip1 - the primary clip to play, or NULL to clear the current primary clip
    //    pClip2 - the secondary clip to play, or NULL to clear the current secondary clip
    //    fTime - time in seconds
    // NOTES:  This will only be called for a Ped Model when the animations are not sectioned, and it
    //    has a face animation and that face is contained in a clip.  Normally, faces are not in clips.
    virtual void SetDualClip( const crClip *pClip1, const crClip *pClip2, float fTime );

    // PURPOSE: Sets up the motion tree to play the clip and anim
    // PARAMS:
    //    pClip - the primary clip to play, or NULL to clear the current primary clip
    //    pAnim - the secondary anim to play, or NULL to clear the current secondary anim
    //    fTime - time in seconds
    // NOTES:  This will only be called for a Ped Model when the animations are not sectioned and it has
    //    a normal face animation.
virtual void SetDualClipAnim( const crClip *pClip, const crAnimation *pAnim, float fTime );

    // PURPOSE: Sets the current motion tree animations or clips to the specified time.
    // PARAMS:
    //    fTime - time in seconds
    virtual void SetTime( float fTime );

    // PURPOSE: Clears the animation or clip.
    virtual void Clear();

    crClipPlayer *m_pClipPlayer;
};

inline crClipPlayer* cutsClipPlayerEntity::GetClipPlayer() const
{
    return m_pClipPlayer;
}

inline void cutsClipPlayerEntity::SetClipPlayer( crClipPlayer *pClipPlayer )
{
    m_pClipPlayer = pClipPlayer;
}

//##############################################################################

class cutsMotionTreeEntity : public cutsAnimatedEntity
{
public:
    cutsMotionTreeEntity( const cutfObject* pObject, crmtMotionTree *pMotionTree, crmtMotionTreeScheduler *pMotionTreeScheduler=NULL );
    virtual ~cutsMotionTreeEntity();

#if !__FINAL
    // PURPOSE: Draws debug text and/or objects for this entity.
    // NOTES:  The cutsManager will call this function when GetDebugDraw returns true.
    virtual void DebugDraw() const;
#endif // !__FINAL

    // PURPOSE: Retrieves the motion tree for this entity
    // RETURNS: The motion tree.
    crmtMotionTree* GetMotionTree() const;

    // PURPOSE: Sets the motion tree for this entity.  If there's an anim player, it will be removed.
    // PARAMS:
    //    pMotionTree - the motion tree
    void SetMotionTree( crmtMotionTree *pMotionTree );

    // PURPOSE: Retrieves the motion tree scheduler for this entity
    // RETURNS: The motion tree scheduler.
    crmtMotionTreeScheduler* GetMotionTreeScheduler() const;

    // PURPOSE: Sets the motion tree scheduler for this entity
    // PARAMS:
    //    pMotionTreeScheduler - the motion tree scheduler
    void SetMotionTreeScheduler( crmtMotionTreeScheduler *pMotionTreeScheduler );

    // PURPOSE: Retrieves the observer for this entity
    // RETURNS: The observer tree, if there is one.
    crmtObserver* GetObserver() const;

protected:
    // PURPOSE: Sets up the motion tree to play the animation
    // PARAMS:
    //    pAnim - the animation to play, or NULL to clear the current animation
    //    fTime - time in seconds
    // NOTES:  Deprecated.  All animations are now contained in clips.
    virtual void SetAnimation( const crAnimation *pAnim, float fTime );

    // PURPOSE: Sets up the motion tree to play the animation
    // PARAMS:
    //    pAnim1 - the primary animation to play, or NULL to clear the current primary animation
    //    pAnim2 - the secondary animation to play, or NULL to clear the current secondary animation
    //    fTime - time in seconds
    // NOTES:  Deprecated.  All animations are now contained in clips.
    virtual void SetDualAnimation( const crAnimation *pAnim1, const crAnimation *pAnim2, float fTime );

    // PURPOSE: Sets up the motion tree to play the clip
    // PARAMS:
    //    pClip - the clip to play, or NULL to clear the current clip
    //    fTime - time in seconds
    // NOTES:  This function is called for every animated entity regardless of the animations being sectioned,
    //    except in the case of Ped Models.  When the animations are not sectioned, it will be called for a Ped 
    //    Model only when it doesn't have a face.
    virtual void SetClip( const crClip *pClip, float fTime );

    // PURPOSE: Sets up the motion tree to play the clips
    // PARAMS:
    //    pClip1 - the primary clip to play, or NULL to clear the current primary clip
    //    pClip2 - the secondary clip to play, or NULL to clear the current secondary clip
    //    fTime - time in seconds
    // NOTES:  This will only be called for a Ped Model when the animations are not sectioned, and it
    //    has a face animation and that face is contained in a clip.  Normally, faces are not in clips.
    virtual void SetDualClip( const crClip *pClip1, const crClip *pClip2, float fTime );

    // PURPOSE: Sets up the motion tree to play the clip and anim
    // PARAMS:
    //    pClip - the primary clip to play, or NULL to clear the current primary clip
    //    pAnim - the secondary anim to play, or NULL to clear the current secondary anim
    //    fTime - time in seconds
    // NOTES:  This will only be called for a Ped Model when the animations are not sectioned and it has
    //    a normal face animation.
    virtual void SetDualClipAnim( const crClip *pClip, const crAnimation *pAnim, float fTime );

    // PURPOSE: Sets the current motion tree animations or clips to the specified time.
    // PARAMS:
    //    fTime - time in seconds
    virtual void SetTime( float fTime );

    // PURPOSE: Clears the animation or clip.
    virtual void Clear();

private:
    // PURPOSE: Sets the node's animations or clips (including children) to the specified time.
    // PARAMS:
    //    fTime - time in seconds
    void SetTime( crmtNode *pNode, float fTime );

protected:
    crmtMotionTree *m_pMotionTree;
    crmtMotionTreeScheduler *m_pMotionTreeScheduler;
    crmtObserver *m_pObserver;
};

inline crmtMotionTree* cutsMotionTreeEntity::GetMotionTree() const
{
    return m_pMotionTree;
}

inline crmtMotionTreeScheduler* cutsMotionTreeEntity::GetMotionTreeScheduler() const
{
    return m_pMotionTreeScheduler;
}

inline void cutsMotionTreeEntity::SetMotionTreeScheduler( crmtMotionTreeScheduler *pMotionTreeScheduler )
{
    m_pMotionTreeScheduler = pMotionTreeScheduler;
}

inline crmtObserver* cutsMotionTreeEntity::GetObserver() const
{
    return m_pObserver;
}

//##############################################################################

class cutsAnimatedCameraEntity : public cutsMotionTreeEntity
{
public:
    cutsAnimatedCameraEntity( const cutfObject* pObject, crmtMotionTree *pMotionTree, crmtMotionTreeScheduler *pMotionTreeScheduler=NULL );
    virtual ~cutsAnimatedCameraEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL,  const float fTime = 0.0f, const u32 StickyId = 0  );

    // PURPOSE: Returns the name of the current camera cut
    // RETURNS: The name of the current camera cut
    const char* GetCameraCutName() const;

#if !__FINAL
    // PURPOSE: Indicates when face zoom is enabled
    // RETURNS: True when face zoom is enabled, otherwise false.
    bool IsFaceZoomEnabled() const;

    // PURPOSE: Returns the name of the current face zoom (i.e. the Ped Model we're looking at)
    // RETURNS: The name of the current face zoom
    const char* GetFaceZoomName() const;

    // PURPOSE: Returns if the current face zoom has a face animation
    // RETURNS: true if there is a face animation, otherwise false
    bool FaceZoomHasFace() const;

    // PURPOSE: Retrieves the current face zoom near draw distance.
    // RETURNS: The near draw distance to use for face zoom
    float GetFaceZoomNearDrawDistance() const;

    // PURPOSE: Retrieves the current face zoom far draw distance.
    // RETURNS: The far draw distance to use for face zoom
    float GetFaceZoomFarDrawDistance() const;

    // PURPOSE: Retrieves the current matrix for the face zoom.  Is only updated when face zoom is enabled.
    // RETURNS: The camera matrix for the face zoom.
    const Matrix34& GetFaceZoomMatrix();
#endif // !__FINAL

protected:

#if !__FINAL
    // PURPOSE: Sets the skeleton containing the face that we want to zoom in on
    // PARAMS:
    //    pSkeleton - the skeleton with the face
    virtual void SetFaceZoomSkeleton( const crSkeleton *pSkeleton );

    // PURPOSE: Returns the name of the bone in the skeleton where the face camera shoudl be attached.
    // RETURNS: The name of the bone.  The default is "Facial_root".
    virtual const char* GetFaceZoomBoneName() const;
#endif // !__FINAL

    crCreatureComponentCamera *m_pCameraComponent;
    char m_cCameraCutName[CUTSCENE_LONG_OBJNAMELEN];

#if !__FINAL
    const crSkeleton *m_pFaceZoomSkeleton;
    const crBoneData *m_pFacialRootBoneData;
    char m_cFaceZoomName[CUTSCENE_LONG_OBJNAMELEN];
    bool m_bFaceZoomHasFace;
    float m_fFaceZoomDistance;
    float m_fFaceZoomNearDrawDistance;
    float m_fFaceZoomFarDrawDistance;
    Matrix34 m_faceZoomMatrix;
#endif // !__FINAL
};

inline const char* cutsAnimatedCameraEntity::GetCameraCutName() const
{
#if !__FINAL
    if ( IsFaceZoomEnabled() )
    {
        return "Face Zoom";
    }
#endif // !__FINAL

    return m_cCameraCutName;
}

#if !__FINAL

inline bool cutsAnimatedCameraEntity::IsFaceZoomEnabled() const
{
    return (m_pFaceZoomSkeleton != NULL) && (m_pFacialRootBoneData != NULL);
}

inline const char* cutsAnimatedCameraEntity::GetFaceZoomName() const
{
    return m_cFaceZoomName;
}

inline bool cutsAnimatedCameraEntity::FaceZoomHasFace() const
{
    return m_bFaceZoomHasFace;
}

inline float cutsAnimatedCameraEntity::GetFaceZoomNearDrawDistance() const
{
    return m_fFaceZoomNearDrawDistance;
}

inline float cutsAnimatedCameraEntity::GetFaceZoomFarDrawDistance() const
{
    return m_fFaceZoomFarDrawDistance;
}

inline const char* cutsAnimatedCameraEntity::GetFaceZoomBoneName() const
{
    return "Facial_root";
}

#endif // !__FINAL

//##############################################################################

class cutsAnimatedParticleEffectEntity : public cutsMotionTreeEntity
{
public:
    cutsAnimatedParticleEffectEntity( ptxEffectInst *pEffectInst, const cutfObject* pObject, crmtMotionTree *pMotionTree, 
        crmtMotionTreeScheduler *pMotionTreeScheduler=NULL );
    virtual ~cutsAnimatedParticleEffectEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL,  const float fTime = 0.0f, const u32 StickyId = 0);

protected:
    // PURPOSE: Initializes the ptxEffectInst with the settings from the crClip.
    // PARAMS:
    //    pEffectInst - the effect to initialize
    //    pClip - the clip with the initialization properties
    // NOTES: Typically used by derived classes when you don't want this class's DispatchEvent to handle the ptxEffectInst directly.
    void InitializeEffectInst( ptxEffectInst* pEffectInst, const crClip *pClip );

    ptxEffectInst* m_pEffectInst;
};

//##############################################################################

class cutsAnimatedLightEntity : public cutsMotionTreeEntity
{
public:
    cutsAnimatedLightEntity( const cutfObject* pObject, crmtMotionTree *pMotionTree, crmtMotionTreeScheduler *pMotionTreeScheduler=NULL );
    virtual ~cutsAnimatedLightEntity();

    // PURPOSE: Sends the event to this entity.  The derived class should check iEventId, cast pEventArgs to the correct
    //    type, if needed, then handle the event.
    // PARAMS:
    //    pManager - The event caller (in case you have multiple instances).  This is how acknowledgments will be handled.
    //    pObject - the cut scene object that is associated with this entity.  This is provided so that it is not necessary
    //      for each derived class to store off its own copy, and also so that a single cut scene entity can handle events from 
    //      multiple cut scene objects.
    //    iEventId - the event id.
    //    pEventArgs - the event args, if any.
    virtual void DispatchEvent( cutsManager *pManager, const cutfObject* pObject, s32 iEventId, const cutfEventArgs* pEventArgs=NULL,  const float fTime = 0.0f, const u32 StickyId = 0  );

#if !__FINAL
    // PURPOSE: Draws debug text and/or objects for this entity.
    // NOTES:  The cutsManager will call this function when GetDebugDraw returns true.
    virtual void DebugDraw() const;
#endif // !__FINAL

protected:
#if __BANK
    SEditCutfLightInfo *m_pEditLightInfo;
#endif // __BANK
};

//##############################################################################

} // namespace rage

#endif // CUTSCENE_CUTSANIMENTITY_H 
