// 
// cutscene/cutsmanager.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutsmanager.h"

#if __BANK
#include "bank/widget.h"
#include "cranimation/vcrwidget.h"
#endif // __BANK
#if !__FINAL
#include "cutsanimentity.h"
#endif // !__FINAL
#include "cutsaudioentity.h"
#include "cutschannel.h"
#include "cutsevent.h"
#include "cutseventargs.h"
#include "cutsoptimisations.h"
#include "bank/bank.h"
#include "bank/bkangle.h"
#include "bank/button.h"
#include "bank/combo.h"
#include "bank/slider.h"
#include "bank/text.h"
#include "bank/toggle.h"
#include "cutfile/cutfevent.h"
#include "cutfile/cutfeventargs.h"
#include "cutfile/cutfobject.h"

#include "diag/tracker.h"
#if !__FINAL
#include "grcore/debugdraw.h"
#include "grcore/font.h"
#include "grcore/im.h"
#include "grcore/viewport.h"
#endif // !__FINAL
#include "math/angmath.h"
#include "system/param.h"

#define CUTSCENE_AUDIO_RESYNC_TIME (0.065f)

CUTS_OPTIMISATIONS()

RAGE_DEFINE_CHANNEL( Cutscene );

namespace rage {

#if !__FINAL

PARAM( cutsDisplayStatus, "[cutscene] Enable the onscreen Status Display." );
PARAM( cutsDisplayDebugLines, "[cutscene] Enable the display of debug lines." );
PARAM( cutsDisplayBlockingBoundsLines, "[cutscene] Enable the display of blocking bounds lines." );
PARAM( cutsDisplayRemovalBoundsLines, "[cutscene] Enable the display of removal bounds lines." );
PARAM( cutsDisplayCameraLines, "[cutscene] Enable the display of camera lines." );
PARAM( cutsDisplayLightLines, "[cutscene] Enable the display of light lines." );
PARAM( cutsDisplayModelLines, "[cutscene] Enable the display of model lines." );
PARAM( cutsDisplayParticleEffectLines, "[cutscene] Enable the display of particle effect lines." );
PARAM( cutsDisplaySceneOrigin, "[cutscene] Enable the display of the scene origin." );
PARAM(cutsceneaudioscrubbing, "[cutscene] Enable audio sync when jogging");

PARAM( cutsFaceZoomDist, "[cutscene] The default distance for the Camera Face Zoom." );

PARAM( cutsFaceDir, "[cutscene] Set to override the face directory in order to hot load in 'raw mode'.  Good for a cutscene viewer." );
PARAM( cutsOffset, "[cutscene] Set to override offset the XYZ of cutscenes.  Good for a cutscene viewer.  Example: -cutsoffset=0,0,0" );
PARAM( cutsRotation, "[cutscene] Set to overide the rotation of cutscenes.  Good for a cutscene viewer." );

XPARAM( cutsIgnoreDicts );


static float s_oneFramesPerSecond = 1.0f / CUTSCENE_FPS;
static float s_twoFramesPerSecond = 2.0f / CUTSCENE_FPS;

#if __BANK
// Keep in sync with EScreenFadeOverride in cutsmanager.h
static const char* c_cutsceneScreenFadeOverrideDisplayNames[] = 
{
	"Default fade",
	"Force fade",
	"Skip fade",
};
#endif

#endif // !__FINAL

#if !__NO_OUTPUT
// Keep in sync with ECutsceneState in cutsmanager.h
static const char *c_cutsceneStateNames[] = 
{
    "Idle",
    "Load Cutfile",
    "Loading Cutfile",
    "Loaded Cutfile",
    "Fading Out",
    "Load",
    "Loading",
    "Loading Before Resuming",
    "Play",
    "Paused",
    "Stopped",
    "Cancelling",
    "Unload",
#if HACK_GTA4
	"Skipping",
#endif
	"Unloading",
#if CUTSCENE_AUTHORIZED_FOR_PLAYBACK
	"Authorized"
#endif
};
#endif // !__NO_OUTPUT

//##############################################################################

#if !__FINAL
#if __BANK
bool cutsManager::s_bIsBankOpen = false;
#endif // __BANK
#endif // !__FINAL

cutsManager::cutsManager()
: m_cutsceneState(CUTSCENE_IDLE_STATE)
, m_streamingState(STREAMING_IDLE_STATE)
, m_PlayBackState(PLAY_STATE_FORWARDS_NORMAL_SPEED)
, m_bFadedOutCutsceneAtEnd(false)
, m_fadeOutGameAtBeginning(DEFAULT_FADE)
, m_fadeInCutsceneAtBeginning(DEFAULT_FADE)
, m_fadeOutCutsceneAtEnd(DEFAULT_FADE)
, m_fadeInGameAtEnd(DEFAULT_FADE)
, m_fadeInGameAtEndWhenStopping(DEFAULT_FADE)
, m_bPlayNow(false)
, m_bJustLoadCutfile(false)
, m_bCutfileStreamed(false)
, m_bAssetsStreamed(false)
, m_bIsStoppingForEndFadeOut(false)
, m_bSkippingPlayback(false)
, m_bIsCancellingLoad(false)
, m_bIsTerminating(false)
, m_bReachedPlayBackEnd(false)
, m_bNeedToLoadBeforeResuming(false)
, m_bResumingPlayFromPause(false)
, m_fTime(0.0f)
, m_fInternalTime(0.0f)
, m_fFadeOutEndTime(0.0f)
, m_fDeltaTime(0.0f)
, m_fSectionStartTime(0.0f)
, m_fSectionDuration(0.0f)
, m_iCurrentSection(0)
//, m_iLastConcatSection(0)
, m_iCurrentConcatSection(0)
, m_iNextEventIndex(0)
, m_bDispatchedStartScene(false)
, m_bForceAudioSync(true)
, m_iStartFrame(0)
, m_iEndFrame(100000)
, m_fStartTime(0.0f)
, m_fEndTime(0.0f)
, m_fSceneRotation(0.0f)
, m_fSceneRoll(0.0f)
, m_fScenePitch(0.0f)
, m_bDepthOfFieldEnabled(false)
, m_iStatusUnits(1)
, m_pOriginalSceneMat(NULL)
#if HACK_GTA4
, m_pCutSceneFile(NULL)
#endif
#if !__FINAL
, m_fPlaybackRate(1.0f)
, m_bIsStepping(false)
, m_bStepped(false)
, m_bIsRestarting(false)
, m_iSelectedPedModelIndex(0)
, m_fFaceZoomDistance(0.3f)
, m_fFaceZoomNearDrawDistance(0.05f)
, m_fFaceZoomFarDrawDistance(1000.0f)
, m_iTimestepSource(0)
, m_fStatusDisplayX(20.0f)
, m_fStatusDisplayY(30.0f)
, m_bDisplayStatus(false)
, m_bDisplayMemoryUsage(false)
, m_bDisplayDebugLines(false)
, m_bDisplayBlockingBoundsLines(false)
, m_bDisplayRemovalBoundsLines(false)
, m_bDisplayCameraLines(false)
, m_bDisplayLightLines(false)
, m_bDisplayModelLines(false)
, m_bDisplayParticleEffectLines(false)
, m_bDisplaySceneOrigin(false)
, m_bDisplayHiddenLines(false)
, m_bAllowAudioSyncFromRag(false)
, m_ScrubbingState(SCRUBBING_NONE)
, m_PreviousPlayBackState(PLAY_STATE_FORWARDS_NORMAL_SPEED)
, m_iCurrentFrameWithFrameRanges(0)
#endif // !__FINAL
#if __BANK
, m_iSelectedIsolatePedIndex(0)
, m_bIsolateSelectedPed(false)
, m_pBank(NULL)
, m_pCurrentSceneGroup(NULL)
, m_bPauseAtEnd(false)
, m_iAudioOffset(0)
, m_bMuteAudio(false)
, m_bLoop(false)
, m_iLoopStartFrame(0)
, m_iLoopEndFrame(100000)
, m_pEditFaceCombo(NULL)
, m_pIsolatePedCombo(NULL)
, m_pPlayBackSlider(NULL)
, m_pPhasePlayBackSlider(NULL)
, m_pLoopStartSlider(NULL)
, m_pLoopEndSlider(NULL)
, m_pFacePlayBackSlider(NULL) 
, m_pLightPlayBackSlider(NULL) 
, m_pCameraPlayBackSlider(NULL)
, m_pHiddenObjPlayBackSlider(NULL) 
, m_pFixupObjPlayBackSlider(NULL) 
, m_pBlockingBoundPlayBackSlider(NULL)
, m_pPhaseSlider(NULL)
, m_pEditBlockingBoundObjectCombo(NULL)
, m_pEditBlockingBoundObjectGroup(NULL)
, m_pEditBlockingBoundObjectEvents(NULL)
, m_iSelectedLightIndex(0)
, m_pEditLightCombo(NULL)
, m_pEditLightGroup(NULL)
, m_pActiveLightCombo(NULL)
, m_fLightIntensity(100.0f)
, m_fLightFallOff(0.0f)
, m_fLightExponentFallOff(8.0f)
, m_fLightConeAngle(45.0f)
, m_fInnerConeAngle(45.0f)
, m_fShadowBlur(0.0f) 
, m_fBankSceneRotation(0.0f)
, m_fBankScenePitch(0.0f)
, m_fBankSceneRoll(0.0f)
, m_pCameraObject(NULL)
, m_iSelectedDrawDistanceIndex(0)
, m_pEditDrawDistanceCombo(NULL)
, m_pEditDrawDistanceGroup(NULL)
, m_fDrawDistanceNearClip(0.0f)
, m_fDrawDistanceFarClip(0.0f)
, m_pEditDrawDistanceNearClipSlider(NULL)
, m_pEditDrawDistanceFarClipSlider(NULL)
, m_iSelectedHiddenObjectIndex(0)
, m_iSelectedHiddenObjectEventIndex(0)
, m_pEditHiddenObjectGroup(NULL)
, m_pEditHiddenObjectCombo(NULL)
, m_pEditHiddenObjectEvents(NULL)
, m_iSelectedFixupObjectIndex(0)
, m_pEditFixupObjectGroup(NULL)
, m_pEditFixupObjectCombo(NULL)
, m_pEditFixupObjectEvents(NULL)
, m_iSelectedFixupObjectEventIndex(0)
,m_iSelectedBlockingBoundObjectIndex(0)
,m_iSelectedBlockingBoundObjectEventIndex(0)
,m_iActiveLightIndex(0)
,m_LocateMatRot(VEC3_ZERO)
,m_fCurrentPhase(0.0f)
,m_LightFlags(0)
,m_LightTimeFlags((1<<24) - 1) 
,m_createdLightTypeIndex(0)
,m_bEditSceneOrigin(false)
,m_vVolumeOuterColourAndIntensity(Vector4::ZeroType)
,m_fVolumeIntensity(1.0f)
,m_fVolumeSizeScale(1.0f)
,m_fCoronaSize(1.0f)
,m_fCoronaIntensity(1.0f)
,m_fCoronaZBias(1.0f)
,m_LightCastStaticObjectShadowFlag(false)
,m_LightCastDynamicObjectShadowFlag(false)
,m_LightCalcFromSunFlag(false)
,m_LightEnableBuzzingFlag(false)
,m_LightForceBuzzingFlag(false)
,m_LightDrawVolumeFlag(false) 
,m_lightNoSpecularFlag(false) 
,m_LightCoronaOnlyFlag(false)
,m_LightBothIntAndExtFlag(false) 
,m_LightNotInReflection(false)
,m_LightOnlyInReflection(false)
,m_LightReflector(false)
,m_LightDiffuser(false)
,m_LightCalcByAmbientFlag(false)
,m_LightFlagsDontLightAlpha(false)
,m_LightUseAsPedLight(false)
,m_UseIntensityAsMultiplier(false)
,m_IsPedOnlyLight(false)
,m_ActivateCameraDebugging(false)
,m_bUpdateLightWithCamera(false)
,m_debugActiveState(false)
,m_LightHour1(true)
,m_LightHour2(true)
,m_LightHour3(true)
,m_LightHour4(true)
,m_LightHour5(true)
,m_LightHour6(true)
,m_LightHour7(true)
,m_LightHour8(true)
,m_LightHour9(true)
,m_LightHour10(true)
,m_LightHour11(true)
,m_LightHour12(true)
,m_LightHour13(true)
,m_LightHour14(true)
,m_LightHour15(true)
,m_LightHour16(true)
,m_LightHour17(true)
,m_LightHour18(true)
,m_LightHour19(true)
,m_LightHour20(true)
,m_LightHour21(true)
,m_LightHour22(true)
,m_LightHour23(true)
,m_LightHour24(true) 
,m_HoldAtEnd(false)
,m_pEditLightStatusText(NULL)
,m_pLightHotloadingStatusText(NULL)
#endif // __BANK
{
    m_vSceneOffset.Zero();
    m_cDisplayTime[0] = 0;
    m_cCurrentSceneName[0] = 0;
	

#if !__FINAL
    float fFaceZoomDist;
    if ( PARAM_cutsFaceZoomDist.Get( fFaceZoomDist ) )
    {
        m_fFaceZoomDistance = fFaceZoomDist;
    }

    m_bDisplayStatus = PARAM_cutsDisplayStatus.Get();
    m_bDisplayDebugLines = PARAM_cutsDisplayDebugLines.Get();
    m_bDisplayBlockingBoundsLines = PARAM_cutsDisplayBlockingBoundsLines.Get();
    m_bDisplayRemovalBoundsLines = PARAM_cutsDisplayRemovalBoundsLines.Get();
    m_bDisplayCameraLines = PARAM_cutsDisplayCameraLines.Get();    
    m_bDisplayLightLines = PARAM_cutsDisplayLightLines.Get();
    m_bDisplayModelLines = PARAM_cutsDisplayModelLines.Get();
    m_bDisplayParticleEffectLines = PARAM_cutsDisplayParticleEffectLines.Get();
    m_bDisplaySceneOrigin = PARAM_cutsDisplaySceneOrigin.Get();

	m_bAudioSyncWhenJogging = PARAM_cutsceneaudioscrubbing.Get();
#endif

#if __BANK
    m_vLightColor.Set( 1.0f, 1.0f, 1.0f );
    m_vLightPosition.Set( 0.0f, 0.0f, 0.0f );
    m_vLightDirection.Set( 0.0f, 0.0f, -1.0f );
	m_fBoundingBoxHeight = 10.0f;
	m_vBankSceneOffset.Zero();
	m_vLightOrientation = VEC3_ZERO; 
	m_LightEditStatus[0] = 0;
	m_LightHotLoadingStatus[0] =0;
	m_createdLightName[0] = 0;
	m_duplicatedLightName[0] = 0;
	m_LightNewName[0] = 0; 
	
#if __PPU
    m_iAudioResyncTime = (int)(CUTSCENE_AUDIO_RESYNC_TIME * 1000.0f);
#endif // __PPU
#endif // __BANK
}

cutsManager::~cutsManager()
{
    Clear();
}

void cutsManager::InitClass()
{
    cutfEvent::InitClass();
    cutsEvent::InitClass();


}

void cutsManager::ShutdownClass()
{
    cutsEvent::ShutdownClass();
    cutfEvent::ShutdownClass();
}

void cutsManager::Load( const char* pFilename, const char* pExtension, bool bPlayNow, 
          EScreenFadeOverride fadeOutGameAtBeginning, EScreenFadeOverride fadeInCutsceneAtBeginning,
          EScreenFadeOverride fadeOutCutsceneAtEnd, EScreenFadeOverride fadeInGameAtEnd, bool bJustCutfile )
{
    cutsAssertf( !IsRunning(), "A cutscene is currently running.  You must call Unload() or Stop() first." );

    ASSET.FullPath( m_cCutfileToLoad, sizeof(m_cCutfileToLoad), pFilename, pExtension );

    pExtension = ASSET.FindExtensionInPath( pFilename );
    if ( pExtension == NULL )
    {
        char cCutBinFile[RAGE_MAX_PATH];
        ASSET.FullReadPath( cCutBinFile, sizeof(cCutBinFile), pFilename, PI_CUTSCENE_BINFILE_EXT );

        char cCutXmlFile[RAGE_MAX_PATH];
        ASSET.FullReadPath( cCutXmlFile, sizeof(cCutXmlFile), pFilename, PI_CUTSCENE_XMLFILE_EXT );

        if ( fiDevice::GetDevice( cCutBinFile )->IsOutOfDate( cCutBinFile, cCutXmlFile ) && ASSET.Exists( cCutBinFile, NULL ) )
        {
            safecpy( m_cCutfileToLoad, cCutBinFile, sizeof(m_cCutfileToLoad) );
        }
        else if ( ASSET.Exists( cCutXmlFile, NULL ) )
        {
            safecpy( m_cCutfileToLoad, cCutXmlFile, sizeof(m_cCutfileToLoad) );
        }
        else if ( ASSET.Exists( cCutBinFile, NULL ) )
        {
            safecpy( m_cCutfileToLoad, cCutBinFile, sizeof(m_cCutfileToLoad) );
        }
        else
        {
            cutsErrorf( "Unable to determine an appropriate file extension for '%s'.  Cannot load cut scene.", pFilename );
            return;
        }
    }

    m_bPlayNow = bPlayNow;

    m_fadeOutGameAtBeginning = fadeOutGameAtBeginning;
    m_fadeInCutsceneAtBeginning = fadeInCutsceneAtBeginning;
    m_fadeOutCutsceneAtEnd = fadeOutCutsceneAtEnd;
    m_fadeInGameAtEnd = fadeInGameAtEnd;
    m_fadeInGameAtEndWhenStopping = fadeInGameAtEnd;

    m_bJustLoadCutfile = bJustCutfile;

    m_cutsceneState = CUTSCENE_LOAD_CUTFILE_STATE;
}

void cutsManager::ResumeLoad()
{
    if ( m_cutsceneState == CUTSCENE_LOADED_CUTFILE_STATE )
    {
        // Put us into the fading out state, if only temporarily, so that the Fade Out Entity can check IsFadingOutAtBeginning().
        m_cutsceneState = CUTSCENE_FADING_OUT_STATE;

        // see if we need to force a fade out
        if ( !FadeOutGameAtBeginning( m_fadeOutGameAtBeginning ) )
        {
            // no fade out, so go directly to the loading state
            m_cutsceneState = CUTSCENE_LOAD_STATE;
        }
    }
}

void cutsManager::Unload()
{
    if ( IsRunning() )
    {
        switch ( m_cutsceneState )
        {
        case CUTSCENE_LOADED_CUTFILE_STATE:
            {
                m_cutsceneState = CUTSCENE_IDLE_STATE;

                // we're done
                Clear();
            }
            break;
        case CUTSCENE_STOPPED_STATE:
            {
                // I don't think this will ever happen.
                m_cutsceneState = CUTSCENE_UNLOAD_STATE;
            }
            break;
        default:
            {
                if ( (m_cutsceneState != CUTSCENE_UNLOAD_STATE) && (m_cutsceneState != CUTSCENE_UNLOADING_STATE) )
                {
                    // this happens if someone else calls Unload()
                    Stop( m_fadeOutCutsceneAtEnd, m_fadeInGameAtEnd );
                }
            }
            break;
        }
    }
    else
    {
        Clear();
    }
}

void cutsManager::Clear()
{
	
	if (GetCutfFile())
	{
		GetCutfFile()->Clear();
	}
    m_cutsceneState = CUTSCENE_IDLE_STATE;
    m_streamingState = STREAMING_IDLE_STATE;
    m_cutsceneEntityObjects.Kill();
    m_bFadedOutCutsceneAtEnd = false;
	m_bHasFadeInGameAtEndBeenCalled = false; 
    m_fadeOutGameAtBeginning = DEFAULT_FADE;
    m_fadeInCutsceneAtBeginning = DEFAULT_FADE;
    m_fadeOutCutsceneAtEnd = DEFAULT_FADE;
    m_fadeInGameAtEnd = DEFAULT_FADE;
    m_fadeInGameAtEndWhenStopping = DEFAULT_FADE;
	m_bPlayNow = false;
    m_bJustLoadCutfile = false;
    m_bCutfileStreamed = false;
    m_bAssetsStreamed = false;
    m_bIsStoppingForEndFadeOut = false;
	m_bSkippingPlayback = false; 
    m_bIsCancellingLoad = false;
    m_bIsTerminating = false;
    m_bReachedPlayBackEnd = false;
    m_bNeedToLoadBeforeResuming = false;
	m_bResumingPlayFromPause = false;
    m_fTime = 0.0f;
	m_fInternalTime = 0.0f;
    m_fFadeOutEndTime = 0.0f;
    m_fDeltaTime = 0.0f;
    m_fSectionStartTime = 0.0f;
    m_fSectionDuration = 0.0f;
    m_iCurrentSection = 0;
    m_iCurrentConcatSection = 0;
    m_iNextEventIndex = 0;
    m_bDispatchedStartScene = false;
    m_bForceAudioSync = true;
    m_fSectionStartTimeList.Reset();
    m_concatDataList.Reset();
    m_iStartFrame = 0;
    m_iEndFrame = 100000;
	if(m_pOriginalSceneMat)
	{
		delete m_pOriginalSceneMat; 
		m_pOriginalSceneMat = NULL;
	}

   	SetSceneOffset(VEC3_ZERO);
    SetSceneRotation(0.0f);
	SetScenePitch(0.0f); 
	SetSceneRoll(0.0f);
    m_bDepthOfFieldEnabled = false;

#if __BANK
    m_iLoopStartFrame = 0;
    m_iLoopEndFrame = 100000;

    m_iSelectedLightIndex = 0;
	m_iActiveLightIndex = 0;

    for ( int i = 0; i < m_editLightList.GetCount(); ++i )
    {
       if(m_editLightList[i])
	   {
		delete m_editLightList[i];
	   }

		m_editLightList[i] = NULL; 
    }

	for(int i=0; i < m_activeLightList.GetCount(); ++i)
	{
		m_activeLightList[i] = NULL; 
	}

	m_activeLightList.Reset(false); 

    m_editLightList.Reset();

	m_deletedLightList.Reset();

    m_pCameraObject = NULL;
    m_iSelectedDrawDistanceIndex = 0;
    for ( int i = 0; i < m_editDrawDistanceList.GetCount(); ++i )
    {
        delete m_editDrawDistanceList[i];
    }

    m_editDrawDistanceList.Reset();

    m_iSelectedHiddenObjectIndex = 0;
    for ( int i = 0; i < m_editHiddenObjectList.GetCount(); ++i )
    {
        delete m_editHiddenObjectList[i];
    }

    m_editHiddenObjectList.Reset();

    m_iSelectedFixupObjectIndex = 0;
    for ( int i = 0; i < m_editFixupObjectList.GetCount(); ++i )
    {
        delete m_editFixupObjectList[i];
    }

    m_editFixupObjectList.Reset();

	m_iSelectedBlockingBoundObjectIndex = 0;
	
	for ( int i = 0; i < m_editBlockingBoundObjectList.GetCount(); ++i )
	{
		delete m_editBlockingBoundObjectList[i];
	}

	m_editBlockingBoundObjectList.Reset();
	
	//reset the event list 
	m_BlockingBoundObjectEvents.Reset(); 
	m_FixupObjectEvents.Reset(); 
	m_HiddenObjectEvents.Reset(); 
	
	m_iSelectedBlockingBoundObjectEventIndex = 0; 
	m_iSelectedFixupObjectEventIndex = 0; 
	m_iSelectedHiddenObjectEventIndex = 0; 

#if !SETUP_CUTSCENE_WIDGET_FOR_CONTENT_CONTROLLED_BUILD 
    // These will reset the editing combo boxes
    PopulateDrawDistanceList();
    PopulateLightList();    
    PopulateHiddenObjectList();
    PopulateFixupObjectList();
	PopulateBlockingBoundsList(); 
#endif
#endif 

#if !__FINAL
	m_ScrubbingState = SCRUBBING_NONE;
	m_PreviousPlayBackState = PLAY_STATE_FORWARDS_NORMAL_SPEED; 
    m_bStepped = false;

    m_pedModelObjectList.Reset();

    PopulatePedModelFaceList();
#endif // !__FINAL
}

void cutsManager::Play()
{
    if ( IsRunning() )
    {
		if ( m_bNeedToLoadBeforeResuming )
		{
#if !__FINAL
			if(GetPlayBackState() == PLAY_STATE_FORWARDS_NORMAL_SPEED && GetScrubbingState() == SCRUBBING_NONE) // only do this if we a re playing forward at normal speed
			{
				// trigger an audio load, needed when jogging in rag. We're not using CUTSCENE_LOAD_AUDIO_EVENT 
				//because that will use GetDisplayName to load instead of the cached name from the initial CUTSCENE_LOAD_AUDIO_EVENT
				//m_bGeneratingLoadAudioEventNotFromData = true; 
				DispatchEventToAllEntities( CUTSCENE_LOAD_AUDIO_EVENT ); 
			}
#endif
			m_cutsceneState = CUTSCENE_LOADING_BEFORE_RESUMING_STATE;
			m_streamingState = STREAMING_ASSETS_STATE;
			m_bAssetsStreamed = false;
			m_bNeedToLoadBeforeResuming = false;
		}
		else if ( IsLoading() )
		{
			// flag play now, so that as soon as we're done streaming, we'll start playing
			m_bPlayNow = true;
		}
		else if ( IsPaused() )
		{
			m_cutsceneState = CUTSCENE_PLAY_STATE;

			m_bNeedToLoadBeforeResuming = false;
			DispatchEventToAllEntities( CUTSCENE_PLAY_EVENT );
			m_bResumingPlayFromPause = false;
			//m_bGeneratingLoadAudioEventNotFromData = false; 
		}
		else if ( m_cutsceneState == CUTSCENE_LOADED_CUTFILE_STATE )
		{
			// flag play now, so that as soon as we're done streaming, we'll start playing
			m_bPlayNow = true;

			// Continue loading
			ResumeLoad();
		}
    }
}

void cutsManager::Pause()
{
    if ( IsPlaying() )
    {
		m_PlayBackState = PLAY_STATE_PAUSED;
		m_cutsceneState = CUTSCENE_PAUSED_STATE;

        DispatchEventToAllEntities( CUTSCENE_PAUSE_EVENT );
#if !__NO_OUTPUT
		cutsDisplayf("Cutscene paused!");
		if ( (Channel_Cutscene.FileLevel >= DIAG_SEVERITY_DISPLAY))
		{
			sysStack::PrintStackTrace();
		}
#endif // !__NO_OUTPUT
	}
}

#if !__FINAL

void cutsManager::PauseResume()
{
	if ( IsPlaying() )
	{
		m_ScrubbingState = SCRUBBING_PAUSING; 
		SetSkipFrame(m_iCurrentFrameWithFrameRanges); 
		Pause();
	}

#if __BANK
	FixUpEventIndicesForPlayBackDirectionChange(); 
#endif // __BANK

	m_PlayBackState = PLAY_STATE_FORWARDS_NORMAL_SPEED;
	m_fPlaybackRate = 1.0f; 

	m_ScrubbingState = SCRUBBING_NONE; 

	m_bNeedToLoadBeforeResuming = true;
	m_bResumingPlayFromPause = true; 

	Play();
}

void cutsManager::StepForward()
{
    if ( IsPlaying() || IsPaused() )
    {
        m_cutsceneState = CUTSCENE_PLAY_STATE;
		m_PlayBackState = PLAY_STATE_FORWARDS_SCALED; 

		s32 internalFrame = u32(rage::round(GetInternalTime()*CUTSCENE_FPS)); 

		internalFrame ++; 

		if(internalFrame > m_iEndFrame)
		{
			internalFrame = m_iEndFrame; 
		}

		SetSkipFrame(internalFrame); 
		DoSkippingState(); 
		
		m_ScrubbingState = SCRUBBING_STEPPING_FORWARDS; 
        DispatchEventToAllEntities( CUTSCENE_STEP_FORWARD_EVENT );
		
    }
}
void cutsManager::PlayBackward()
{
	if ( IsPlaying() || IsPaused() )
	{
		m_PlayBackState = PLAY_STATE_BACKWARDS; 	
		DispatchEventToAllEntities( CUTSCENE_PLAY_BACKWARDS_EVENT );
	}
}

void cutsManager::StepBackward()
{
    if ( IsPlaying() || IsPaused() )
    {
		 m_PlayBackState = PLAY_STATE_BACKWARDS; 

		if (cutsVerifyf((GetCutfFile() && GetCutfFile()->IsLoaded()), "You shouldn't be calling this function without a scene loaded." ))
		{
			m_cutsceneState = CUTSCENE_PLAY_STATE;
			
			s32 internalFrame = u32(rage::round(GetInternalTime()*CUTSCENE_FPS)); 

			internalFrame --;
			
			if(internalFrame < m_iStartFrame)
			{
				internalFrame = m_iStartFrame; 
			}

			SetSkipFrame(internalFrame); 
			DoSkippingState(); 
			
			m_ScrubbingState = SCRUBBING_STEPPING_BACKWARDS; 
			DispatchEventToAllEntities( CUTSCENE_STEP_BACKWARD_EVENT );
		
		}
	}
}

void cutsManager::FastForward()
{
    if ( IsPlaying() )
    {
        if ( (m_fPlaybackRate > -1.0f) && (m_fPlaybackRate < 1.0f) )
        {
            if ( m_fPlaybackRate != s_twoFramesPerSecond )
            {
                m_fPlaybackRate = s_twoFramesPerSecond;
            }
            else
            {
                m_fPlaybackRate = s_oneFramesPerSecond;
            }
        }
        else 
        {
            if ( m_fPlaybackRate == 2.0f )
            {
                m_fPlaybackRate = 4.0f;
            }
            else if ( m_fPlaybackRate == 4.0f )
            {
                m_fPlaybackRate = 8.0f;
            }
            else
            {
                m_fPlaybackRate = 2.0f;
            }
        }

        m_PlayBackState = PLAY_STATE_FORWARDS_SCALED; 
		m_ScrubbingState = SCRUBBING_FAST_FORWARDING; 
        cutfFloatValueEventArgs floatEventArgs( m_fPlaybackRate );
        DispatchEventToAllEntities( CUTSCENE_FAST_FORWARD_EVENT, &floatEventArgs );
		
    }
    else if ( IsPaused() )
    {
		 m_PlayBackState = PLAY_STATE_FORWARDS_SCALED;        
		if ( m_fPlaybackRate == s_oneFramesPerSecond )
        {
            m_fPlaybackRate = s_twoFramesPerSecond;
        }
        else
        {
            m_fPlaybackRate = s_oneFramesPerSecond;
        }

        //m_bIsStepping = false;

        m_cutsceneState = CUTSCENE_PLAY_STATE;

		m_ScrubbingState = SCRUBBING_FAST_FORWARDING; 
        cutfFloatValueEventArgs floatEventArgs( m_fPlaybackRate );
        DispatchEventToAllEntities( CUTSCENE_FAST_FORWARD_EVENT, &floatEventArgs );
    }
}

void cutsManager::Rewind()
{
    if ( IsPlaying() )
    {		
		if ( (m_fPlaybackRate > -1.0f) && (m_fPlaybackRate < 1.0f) )
        {
            if ( m_fPlaybackRate != -s_twoFramesPerSecond )
            {
                m_fPlaybackRate = -s_twoFramesPerSecond;
            }
            else
            {
                m_fPlaybackRate = -s_oneFramesPerSecond;
            }
        }
        else 
        {
            if ( m_fPlaybackRate == -2.0f )
            {
                m_fPlaybackRate = -4.0f;
            }
            else if ( m_fPlaybackRate == -4.0f )
            {
                m_fPlaybackRate = -8.0f;
            }
            else
            {
                m_fPlaybackRate = -2.0f;
            }
        }

      //  m_bIsStepping = false;

        cutfFloatValueEventArgs floatEventArgs( m_fPlaybackRate );
		m_ScrubbingState = SCRUBBING_REWINDING; 
		m_PlayBackState = PLAY_STATE_BACKWARDS; 
        DispatchEventToAllEntities( CUTSCENE_REWIND_EVENT, &floatEventArgs );
		
    }
    else if ( IsPaused() )
    {
        if ( m_fPlaybackRate == -s_oneFramesPerSecond )
        {
            m_fPlaybackRate = -s_twoFramesPerSecond;
        }
        else
        {
            m_fPlaybackRate = -s_oneFramesPerSecond;
        }

       // m_bIsStepping = false;
		if ( cutsVerifyf( (GetCutfFile() && GetCutfFile()->IsLoaded()), "You shouldn't call Rewind without a scene loaded." ))
		{	
	
			m_cutsceneState = CUTSCENE_PLAY_STATE;
			m_PlayBackState = PLAY_STATE_BACKWARDS; 
			m_ScrubbingState = SCRUBBING_REWINDING; 

			cutfFloatValueEventArgs floatEventArgs( m_fPlaybackRate );
			DispatchEventToAllEntities( CUTSCENE_REWIND_EVENT, &floatEventArgs );
	 }
	}
}

void cutsManager::Restart()
{
    if ( IsPaused() || IsPlaying() )
    {
        DispatchEventToAllEntities( CUTSCENE_RESTART_EVENT );

        m_cutsceneState = CUTSCENE_LOADING_STATE;
        m_streamingState = STREAMING_ASSETS_STATE;
        m_bAssetsStreamed = false;
        m_bIsRestarting = true;
    }
}

void cutsManager::PreviousFace()
{    
    int index = m_iSelectedPedModelIndex - 1;  // Subtract 1, because SetFace goes from [0, GetNumFaces())
    if ( m_iSelectedPedModelIndex == 0 )
    {
        // Was off, now wrap around to the end
        index = GetNumFaces() - 1;
    }
    else
    {
        --index;
    }

    SetFace( index );  
}

void cutsManager::NextFace()
{
    int index = m_iSelectedPedModelIndex - 1;  // Subtract 1, because SetFace goes from [0, GetNumFaces())
    if ( m_iSelectedPedModelIndex == 0 )
    {
        // Was off, now wrap around to the beginning
        index = 0;
    }
    else
    {
        ++index;
    }

    SetFace( index );  
}

void cutsManager::SetFace( int index )
{
    if ( IsPaused() || IsPlaying() )
    {
        s32 iObjectId = FindCameraObjectId();
        if ( iObjectId != -1 )
        {
            if ( (index >= 0) && (index < m_pedModelObjectList.GetCount()) )
            {
                cutfObjectIdEventArgs objectIdEventArgs( m_pedModelObjectList[index]->GetObjectId() );
                cutfObjectIdEvent objectIdEvent1( iObjectId, GetTime(), CUTSCENE_SET_FACE_ZOOM_EVENT, &objectIdEventArgs );
                DispatchEvent( &objectIdEvent1 );

                // Only need to update these values if we were previously off
                if ( m_iSelectedPedModelIndex == 0 )
                {
                    cutfFloatValueEventArgs floatValueEventArgs( m_fFaceZoomDistance );
                    cutfObjectIdEvent objectIdEvent2( iObjectId, GetTime(), CUTSCENE_SET_FACE_ZOOM_DISTANCE_EVENT, &floatValueEventArgs );
                    DispatchEvent( &objectIdEvent2 );

                    floatValueEventArgs.SetFloat1( m_fFaceZoomNearDrawDistance );
                    cutfObjectIdEvent objectIdEvent3( iObjectId, GetTime(), CUTSCENE_SET_FACE_ZOOM_NEAR_DRAW_DISTANCE_EVENT,
                        &floatValueEventArgs );
                    DispatchEvent( &objectIdEvent3 );

                    floatValueEventArgs.SetFloat1( m_fFaceZoomFarDrawDistance );
                    cutfObjectIdEvent objectIdEvent4( iObjectId, GetTime(), CUTSCENE_SET_FACE_ZOOM_FAR_DRAW_DISTANCE_EVENT,
                        &floatValueEventArgs );
                    DispatchEvent( &objectIdEvent4 );
                }
                
                m_iSelectedPedModelIndex = index + 1;   // Add one, because 0 is "none"
            }
            else
            {
                cutfObjectIdEvent objectIdEvent( iObjectId, GetTime(), CUTSCENE_CLEAR_FACE_ZOOM_EVENT );
                DispatchEvent( &objectIdEvent );

                m_iSelectedPedModelIndex = 0;   // 0 is "none"
            }
        }
    }
}

#endif // !__FINAL

void cutsManager::Stop( EScreenFadeOverride fadeOutCutscene, EScreenFadeOverride fadeInGame )
{
	if ( IsPlaying() || IsPaused() )
    {
        m_fadeInGameAtEndWhenStopping = fadeInGame;
		       
		if ( !IsTerminating() && !IsCancellingLoad() )
        {
            m_bIsTerminating = !m_bReachedPlayBackEnd;
        }

        if ( FadeOutCutsceneAtEnd( fadeOutCutscene, true ) )
        {
            m_bIsStoppingForEndFadeOut = true;

            m_cutsceneState = CUTSCENE_FADING_OUT_STATE;
        }
        else
        {
            m_cutsceneState = CUTSCENE_STOPPED_STATE;

            DispatchEventToAllEntities( CUTSCENE_STOP_EVENT );
        }
    }
    else if ( IsLoading() )
    {
        m_cutsceneState = CUTSCENE_CANCELLING_STATE;        
        
        m_bIsCancellingLoad = true;
        
        DispatchEventToAllEntities( CUTSCENE_CANCEL_LOAD_EVENT );
    }
}

bool cutsManager::FadeOut( const Color32 &color, float fDuration )
{
    s32 iScreenFadeObjectId = FindScreenFadeObjectId( true );
    if ( iScreenFadeObjectId != -1 )
    {
        // make sure we have the entity, create if needed
		if ( cutsVerifyf( (GetCutfFile() && GetCutfFile()->IsLoaded()), "You shouldn't call FadeOut without a scene loaded." ))
		{		
			if ( GetEntityObject( GetCutfFile()->GetObjectList()[iScreenFadeObjectId] ) != NULL )
        {
            // Make sure we don't fade past the end of the scene
            fDuration = Min<float>( fDuration, GetEndTime() - GetTime() );
            m_fFadeOutEndTime = GetTime() + fDuration;

            if ( m_fFadeOutEndTime > 0.0f )
            {
                // dispatch a fade out event to it
                cutfScreenFadeEventArgs screenFadeEventArgs( fDuration, color );
                cutfObjectIdEvent fadeEvent( iScreenFadeObjectId, GetTime(), CUTSCENE_FADE_OUT_EVENT, &screenFadeEventArgs );

                DispatchEvent( &fadeEvent );          
                return true;
            }
        }
    }
    }

    return false;
}

bool cutsManager::FadeIn( const Color32 &color, float fDuration )
{
    s32 iScreenFadeObjectId = FindScreenFadeObjectId( true );
    if ( iScreenFadeObjectId != -1 )
    {
		if ( cutsVerifyf( (GetCutfFile() && GetCutfFile()->IsLoaded()), "You shouldn't call FadeIn without a scene loaded." ))
		{	
        // make sure we have the entity, create if needed
			if ( GetEntityObject( GetCutfFile()->GetObjectList()[iScreenFadeObjectId] ) != NULL )
        {
            // dispatch a fade out event to it
            cutfScreenFadeEventArgs screenFadeEventArgs( fDuration, color );
            cutfObjectIdEvent fadeEvent( iScreenFadeObjectId, GetTime(), CUTSCENE_FADE_IN_EVENT, &screenFadeEventArgs );

            DispatchEvent( &fadeEvent );
            return true;
        }
    }
    }

    return false;
}

void cutsManager::PreSceneUpdate()
{
    if ( m_iStatusUnits == FRAMES )
    {
        sprintf( m_cDisplayTime, "(%d)", (int)(GetAudioSeconds() * CUTSCENE_FPS) );
    }
    else 
    {
        sprintf( m_cDisplayTime, "(%.03f)", GetAudioSeconds() );
    }

    switch ( m_cutsceneState )
    {
    case CUTSCENE_IDLE_STATE:
        {			
            DoIdleState();
        }
        break;
    case CUTSCENE_LOAD_CUTFILE_STATE:
        {
            DoLoadCutfileState();
        }
        break;
    case CUTSCENE_LOADING_CUTFILE_STATE:
        {
            DoLoadingCutfileState();
        }
        break;
    case CUTSCENE_FADING_OUT_STATE:
        {
            DoFadingOutState();
        }
        break;
    case CUTSCENE_LOAD_STATE:
        {
            DoLoadState();
        }
        break;
    case CUTSCENE_LOADING_STATE:
        {
            DoLoadingState();
        }
        break;
    case CUTSCENE_LOADING_BEFORE_RESUMING_STATE:
        {
            DoLoadingBeforeResumingState();
        }
        break;
#if CUTSCENE_AUTHORIZED_FOR_PLAYBACK
	case CUTSCENE_AUTHORIZED_STATE:
		{
			DoAuthorizedState(); 
		}
		break; 
#endif
    case CUTSCENE_PLAY_STATE:
        {
            DoPlayState();
        }
        break;
    case CUTSCENE_PAUSED_STATE:
        {
            DoPausedState();
        }
        break;
    case CUTSCENE_STOPPED_STATE:
        {
			DoStoppedState();
        }
        break;
    case CUTSCENE_CANCELLING_STATE:
        {
            DoCancellingState();
        }
        break;
    case CUTSCENE_UNLOAD_STATE:
        {
            DoUnloadState();
        }
        break;
	case CUTSCENE_SKIPPING_STATE:
		{
			DoSkippingState();
		}
		break;
    case CUTSCENE_UNLOADING_STATE:
        {
            DoUnloadingState();
        }
    default:
        break;
    }
}

void cutsManager::UpdateSectionInfo()
{
	m_iCurrentSection = GetSectionForTime( GetTime() );

	if(cutsVerifyf(GetCutfFile(), "PostScene update: Cutfile not loaded cannot get total duration cutscene will not work correctly" ))
	{
		if (!GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) )
		{
			m_fSectionStartTime = 0.0f;
			m_fSectionDuration = GetCutfFile()->GetTotalDuration();
		}
		else if ( m_iCurrentSection + 1 < m_fSectionStartTimeList.GetCount() )
		{
			m_fSectionStartTime = m_fSectionStartTimeList[m_iCurrentSection];
			m_fSectionDuration = m_fSectionStartTimeList[m_iCurrentSection + 1] - m_fSectionStartTime;
		}
		else
		{
			m_fSectionStartTime = m_fSectionStartTimeList[m_iCurrentSection];
			m_fSectionDuration = GetCutfFile()->GetTotalDuration() - m_fSectionStartTime;
		}
	}

	s32 ConcatSection = GetConcatSectionForTime( GetTime() );

	if(IsConcatSectionValid(ConcatSection))
	{
		m_iCurrentConcatSection = ConcatSection; 
	}

	//now update the vScene Offset with the current concat section 
	if (m_concatDataList.GetCount() > 0 && IsConcatSectionValid(m_iCurrentConcatSection))
	{
#if !__FINAL
		if(!PARAM_cutsOffset.Get())
#endif		
		{
			SetSceneOffset(m_concatDataList[m_iCurrentConcatSection].vOffset); 
			SetSceneRotation(m_concatDataList[m_iCurrentConcatSection].fRotation); 
			SetScenePitch(m_concatDataList[m_iCurrentConcatSection].fPitch); 
			SetSceneRoll(m_concatDataList[m_iCurrentConcatSection].fRoll); 
		}

	}
}

void cutsManager::PostSceneUpdate( float fDeltaTime )
{
    if ( IsFadingOutAtBeginning() )
    {
#if __BANK
        float fNewTime = GetTime() + (fDeltaTime * m_fPlaybackRate);
#else // __BANK
        float fNewTime = GetTime() + fDeltaTime;
#endif // __BANK

        m_fDeltaTime = fNewTime - GetTime();
       // m_fTime = fNewTime;

#if !__FINAL
        //if ( m_bIsStepping )
        //{
        //    m_bIsStepping = false;
        //    m_bStepped = true;
        //    m_fPlaybackRate = 1.0f;
        //}
#endif // !__FINAL
    }
    else if ( IsPlaying() || IsFadingOutAtEnd() )
    {

#if __BANK
        float fNewTime = GetTimestepSourceVCRTime( fDeltaTime );
#elif !__FINAL
        float fNewTime = GetDefaultVCRTime( fDeltaTime );
#else
        float fNewTime = GetDefaultTime( fDeltaTime );
#endif

        m_fDeltaTime = fNewTime - GetTime();
       // m_fTime = fNewTime;            

		UpdateSectionInfo(); 
    }
}

void cutsManager::ConvertAngledAreaToMatrixAndVectors(Vector3 StoredCorners[], float fHeight, Vector3 &vMin, Vector3 &vMax, Matrix34 &LocateMat, float fCreationTime)
{
	Vector3 vResult[3]; 
	float DotResults[3]; //store the results from our dot products

	//calculate 3 vectors from the square
	for(int i =1; i < 4; i++ )
	{
		vResult[i -1] = StoredCorners[i] - StoredCorners[0]; 			
	}

	DotResults[0] = Abs(vResult[0].Dot(vResult[1])); 
	DotResults[1] = Abs(vResult[0].Dot(vResult[2])); 
	DotResults[2] = Abs(vResult[1].Dot(vResult[2])); 

	int smallest = 0; 
	float fSmallestValue = DotResults[0]; 

	for(int i = 1; i <3; i++)
	{
		if (DotResults[i] < fSmallestValue)
		{
			fSmallestValue = DotResults[i]; 
			smallest = i; 
		}
	}
	//smallest = 0 StoredCorners[0] is diagonal StoredCorners[3]
	//smallest = 1 StoredCorners[0] is diagonal StoredCorners[2]
	//smallest = 2 StoredCorners[0] is diagonal StoredCorners[1]
#if __64BIT	// Avoids an internal compiler error, otherwise they should be equivalent
	Vector3 vSide1 = vResult[smallest>>1];  
	Vector3 vSide2 = vResult[2-!smallest];  
	int DiagonalIndex = 3 - smallest;
#else
	Vector3 vSide1 = VEC3_ZERO;  
	Vector3 vSide2 = VEC3_ZERO; 
	int DiagonalIndex = 0; 
	switch(smallest)
	{
	case 0:
		{
			vSide1 = vResult[0]; 
			vSide2 = vResult[1]; 
			DiagonalIndex = 3; 

		}
		break;
	case 1:
		{
			vSide1 = vResult[0]; 
			vSide2 = vResult[2]; 
			DiagonalIndex = 2; 
		}
		break; 
	case 2:
		{
			vSide1 = vResult[1]; 
			vSide2 = vResult[2]; 
			DiagonalIndex = 1; 

		}
		break; 
	}
#endif
	Vector3 vMiddle = (StoredCorners[0] + StoredCorners[DiagonalIndex]) / 2.0f; 
	vMiddle.z += fHeight / 2.0f; 

	vSide1.Normalize(); 
	vSide2.Normalize(); 
	LocateMat.Identity();
	LocateMat.a = vSide1; 
	LocateMat.b = vSide2; 
	LocateMat.c = Vector3(0.0f, 0.0f, 1.0f);
	LocateMat.d = vMiddle; 

	Vector3 LocalMax = StoredCorners[0]; 
	Vector3	LocalCentre = vMiddle;  

	LocateMat.UnTransform(LocalMax);
	LocateMat.UnTransform(LocalCentre); 

	vMax = LocalMax - LocalCentre ; 
	vMax.Abs(); 
	vMin = vMax * -1.0f; 

	//put scene 
	Matrix34 SceneMat(M34_IDENTITY);
	s32 section = GetConcatSectionForTime(fCreationTime);
	if (section<0)
		section=0;

	GetSceneOrientationMatrix(section, SceneMat);
	LocateMat.Dot(SceneMat); 
}


#if !__FINAL

void cutsManager::DrawDebugText()
{
    if ( IsRunning() )
    {
        PUSH_DEFAULT_SCREEN();
        grcFont::GetCurrent().DrawCharBegin();		// set the font texture once in the outer code

        if ( m_bDisplayStatus )
        {
            DisplayStatus();
        }

        if ( m_bDisplayMemoryUsage && (IsPaused() || IsPlaying()) )
        {
            DisplayMemoryUsage();
        }

        grcFont::GetCurrent().DrawCharEnd();		// and finally unset the font texture
        POP_DEFAULT_SCREEN();
    }
}


void cutsManager::DebugDraw()
{
    if ( IsRunning() )
    {
        if ( m_bDisplaySceneOrigin )
        {
            DrawSceneOrigin();
        }
		if(m_cutsceneEntityObjects.GetNumUsed() > 0)
		{
			atMap<s32, SEntityObject>::Iterator iter = m_cutsceneEntityObjects.CreateIterator();
			for ( iter.Start(); !iter.AtEnd(); iter.Next() )
			{
				if (iter.GetData().pEntity && iter.GetData().pEntity->GetDebugDraw() )
				{
					iter.GetData().pEntity->DebugDraw();
				}
			}
		}
    }
}

#endif // !__FINAL

const char* cutsManager::GetFaceDirectory() const
{
#if !__FINAL
    const char* pFaceDir;
    if ( PARAM_cutsFaceDir.Get( pFaceDir ) )
    {
        static char cDirectory[RAGE_MAX_PATH];
		if (cutsVerifyf(GetCutfFile(), "GetFaceDirecory: No cutfile loaded cannot get scene name"))
		{
		 sprintf( cDirectory, "%s/%s", pFaceDir, GetCutfFile()->GetSceneName() );
		}
        return cDirectory;
    }
#endif // !__FINAL
	if (cutsVerifyf(GetCutfFile(), "GetFaceDirecory: No cutfile loaded cannot get scene name"))
	{
		return GetCutfFile()->GetFaceDirectory();
	}
	else
	{
		return NULL;
	}
}

float cutsManager::GetAudioSeconds() const
{
    float fAudioSeconds = GetTime();
    if (GetCutfFile() && GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) )
    {
        fAudioSeconds += (GetCutfFile()->GetRangeStart() / CUTSCENE_FPS);  
    }

    return Min<float>( fAudioSeconds, GetAudioEndTime() );
}

bool cutsManager::IsLoading() const
{
    switch ( m_cutsceneState )
    {
    case CUTSCENE_LOAD_CUTFILE_STATE:
    case CUTSCENE_LOAD_STATE:
        return true;
    case CUTSCENE_LOADING_CUTFILE_STATE:
        return m_streamingState == STREAMING_CUTFILE_STATE;
    case CUTSCENE_LOADING_STATE:
    case CUTSCENE_LOADING_BEFORE_RESUMING_STATE:
    case CUTSCENE_CANCELLING_STATE:
        {
            if ( m_streamingState == STREAMING_ASSETS_STATE )
            {
                atMap<s32, SEntityObject>::ConstIterator entry = m_cutsceneEntityObjects.CreateIterator();
                for ( entry.Start(); !entry.AtEnd(); entry.Next() )
                {
                    if ( entry.GetData().bIsLoading && !entry.GetData().bIsLoaded )
                    {
                        return true;
                    }
                }
            }
        }
        break;
    default:
        break;
    }

    return false;
}

bool cutsManager::IsLoading( s32 iObjectId ) const
{
    const SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
    if ( pEntityObject != NULL)
    {
        return pEntityObject->bIsLoading;
    }

    return false;
}

bool cutsManager::IsUnloading() const
{
    if ( m_cutsceneState == CUTSCENE_UNLOAD_STATE )
    {
        return true;
    }
    else if ( m_cutsceneState == CUTSCENE_UNLOADING_STATE )
    {
        atMap<s32, SEntityObject>::ConstIterator entry = m_cutsceneEntityObjects.CreateIterator();
        for ( entry.Start(); !entry.AtEnd(); entry.Next() )
        {
            if ( entry.GetData().bIsUnloading && !entry.GetData().bIsUnloaded )
            {
                return true;
            }
        }
    }

    return false;
}

void cutsManager::SetIsLoading( s32 iObjectId, bool bIsLoading )
{
    SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
    if ( pEntityObject != NULL )
    {
        pEntityObject->bIsLoading = bIsLoading;
        pEntityObject->bIsLoaded = false;
    }
}

bool cutsManager::IsLoaded( s32 iObjectId ) const
{
    const SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
    if ( pEntityObject != NULL)
    {
        return pEntityObject->bIsLoaded;
    }

    return false;
}

void cutsManager::SetIsLoaded( s32 iObjectId, bool bIsLoaded )
{
    SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
    if ( pEntityObject != NULL )
    {
        pEntityObject->bIsLoading = false;
        pEntityObject->bIsLoaded = bIsLoaded;
    }
}

bool cutsManager::IsUnloading( s32 iObjectId ) const
{
    const SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
    if ( pEntityObject != NULL)
    {
        return pEntityObject->bIsUnloading;
    }

    return false;
}

void cutsManager::SetIsUnloading( s32 iObjectId, bool bIsUnloading )
{
    SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
    if ( pEntityObject != NULL )
    {
        pEntityObject->bIsUnloading = bIsUnloading;
        pEntityObject->bIsUnloaded = false;
    }
}

bool cutsManager::IsUnloaded( s32 iObjectId ) const
{
    const SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
    if ( pEntityObject != NULL)
    {
        return pEntityObject->bIsUnloaded;
    }

    return false;
}

void cutsManager::SetIsUnloaded( s32 iObjectId, bool bIsUnloaded )
{
    SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
    if ( pEntityObject != NULL )
    {
        pEntityObject->bIsUnloading = false;
        pEntityObject->bIsUnloaded = bIsUnloaded;
    }
}

const cutfObject* cutsManager::GetObjectById( s32 iObjectId ) const
{    
    if ( cutsVerifyf( (GetCutfFile() && GetCutfFile()->IsLoaded()), "You shouldn't be calling this function without a scene loaded." )
        && (iObjectId >= 0) && (iObjectId < GetCutfFile()->GetObjectList().GetCount()) )
    {
        return GetCutfFile()->GetObjectList()[iObjectId];
    }

    return NULL;
}

cutsEntity* cutsManager::GetEntityByObjectId( s32 iObjectId ) const
{
    const SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
    if ( pEntityObject != NULL )
    {
        return pEntityObject->pEntity;
    }

    return NULL;
}

float cutsManager::GetStartTimeFromStartFrame() const
{
	return 0.0f;
}

u32 cutsManager::GetStartFrame() const
{
	return 0;
}


float cutsManager::GetStartTime() const
{
	return m_fStartTime; 
}

float cutsManager::GetEndTime() const
{
	return m_fEndTime; 
}

float cutsManager::GetAudioStartTime() const
{
    float fAudioStartTime = GetStartTime();
    if ( GetCutfFile() && GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) )
    {
        fAudioStartTime += GetCutfFile()->GetRangeStart() / CUTSCENE_FPS;
    }

    return Min<float>( fAudioStartTime, GetAudioEndTime() );
}

float cutsManager::GetEndTimeFromEndFrame() const
{
	//dont need to recalculate this value just extract from the data.
	return GetTotalSeconds();

}

u32 cutsManager::GetEndFrame() const
{
	return int(GetTotalSeconds() * CUTSCENE_FPS);
}

float cutsManager::GetAudioEndTime() const
{
    float fAudioEndTime = GetEndTime();
    if ( GetCutfFile() && GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) )
    {
        fAudioEndTime += (GetCutfFile()->GetRangeStart() / CUTSCENE_FPS);
    }

	if (GetCutfFile())
	{
		return Min<float>( fAudioEndTime, GetCutfFile()->GetRangeEnd() / CUTSCENE_FPS );
}
	else
	{
		return fAudioEndTime;
	}
}

void cutsManager::StoreOriginalOrientationData(int concatsection)
{
	if(IsConcatted())
	{
		if(!m_pOriginalSceneMat)
		{
			m_pOriginalSceneMat = rage_new atMap<u32, Vector4>; 
		}

		if(m_pOriginalSceneMat && !m_pOriginalSceneMat->Access(concatsection))
		{
			Vector4 SceneData(m_concatDataList[concatsection].vOffset.x, 
				m_concatDataList[concatsection].vOffset.y, 
				m_concatDataList[concatsection].vOffset.z,
				m_concatDataList[concatsection].fRotation) ; 

			m_pOriginalSceneMat->Insert(concatsection, SceneData); 
		}
	}
	else
	{
		if(!m_pOriginalSceneMat)
		{
			m_pOriginalSceneMat = rage_new atMap<u32, Vector4>; 
		}

		if(m_pOriginalSceneMat && !m_pOriginalSceneMat->Access(0))
		{
			Vector4 SceneData(m_vSceneOffset.x, m_vSceneOffset.y, m_vSceneOffset.z, m_fSceneRotation) ; 

			m_pOriginalSceneMat->Insert(0, SceneData); 
		}
	}
}

void cutsManager::GetSceneOrientationMatrix( Matrix34 &m )
{
	GetSceneOrientationMatrix( m_iCurrentConcatSection, m );
}

void cutsManager::GetOriginalSceneOrientationMatrix(Matrix34 &m)
{
	GetOriginalSceneOrientationMatrix(m_iCurrentConcatSection, m); 
}

void cutsManager::GetOriginalSceneOrientationMatrix(int iConcatSection, Matrix34 &m)
{
	m.Identity();
	
	if (IsConcatted() == false || GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_ONE_SCENE_ORIENTATION_FLAG ) )
	{
		if(m_pOriginalSceneMat && m_pOriginalSceneMat->Access(0))
		{
			Vector4 SceneData = *m_pOriginalSceneMat->Access(0); 
			m.Translate( Vector3(SceneData.x, SceneData.y, SceneData.z));
			m.RotateLocalZ( SceneData.w * DtoR);

		}
		else
		{
			GetSceneOrientationMatrix( iConcatSection, m );
		}
	}
	else
	{
		if(m_pOriginalSceneMat && m_pOriginalSceneMat->Access(iConcatSection))
		{
			Vector4 SceneData = *m_pOriginalSceneMat->Access(iConcatSection); 
			m.Translate( Vector3(SceneData.x, SceneData.y, SceneData.z));
			m.RotateLocalZ( SceneData.w * DtoR );
		}
		else
		{
			GetSceneOrientationMatrix( iConcatSection, m );
		}
	}
}

void cutsManager::GetSceneOrientationMatrix( int iConcatSection, Matrix34 &m )
{
    m.Identity();

    if (IsConcatted() == false || GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_ONE_SCENE_ORIENTATION_FLAG ) )
    {
		m.Translate( GetSceneOffset() );
		Vector3 Rot(GetScenePitch(), GetSceneRoll(), GetSceneRotation());
		m.FromEulersXYZ(Rot); 

	}
    else if ( iConcatSection < m_concatDataList.GetCount() )
    {
#if !__FINAL
        // Override the offset
        if ( PARAM_cutsOffset.Get() )
        {
            m.Translate( GetSceneOffset() );
        }
        else
        {
            m.Translate( m_concatDataList[iConcatSection].vOffset );
        }

        // Override the rotation
        if ( PARAM_cutsRotation.Get() )
        {
			Vector3 Rot(GetScenePitch(),GetSceneRoll(), GetSceneRotation());
			m.FromEulersXYZ(Rot); 
		}
        else
        {
			 Vector3 Rot(m_concatDataList[iConcatSection].fPitch * DtoR, m_concatDataList[iConcatSection].fRoll * DtoR, m_concatDataList[iConcatSection].fRotation * DtoR);
			 m.FromEulersXYZ(Rot); 
           
        }
#else // !__FINAL
        Vector3 Rot(m_concatDataList[iConcatSection].fPitch * DtoR, m_concatDataList[iConcatSection].fRoll * DtoR, m_concatDataList[iConcatSection].fRotation * DtoR);
		m.Translate( m_concatDataList[iConcatSection].vOffset );

		m.FromEulersXYZ(Rot); 
#endif // !__FINAL
    }
}

int cutsManager::GetStartingSection() const
{
    return GetSectionForTime( GetStartTime() );
}

int cutsManager::GetPreviousSection( int iSection ) const
{
    int iStartSection = GetSectionForTime( GetStartTime() );
    if ( iSection <= iStartSection )
    {
        return -1;
    }

    return iSection - 1;
}

int cutsManager::GetNextSection( int iSection ) const
{
    int iEndSection = GetSectionForTime( GetEndTime() );
    if ( iSection >= iEndSection )
    {
        return -1;
    }

    return iSection + 1;
}

int cutsManager::GetSectionForTime( float fTime ) const
{
    // Figure out what section we're in
    if ( m_fSectionStartTimeList.GetCount() == 0 )
    {
        return 0;
    }

    for ( int i = m_fSectionStartTimeList.GetCount() - 1; i >= 0; --i )
    {
        if ( fTime >= m_fSectionStartTimeList[i] )
        {
            return i;
        }
    }

    return -1;
}

float cutsManager::GetSectionStartTime( int iSection ) const
{
    if ( m_fSectionStartTimeList.GetCount() == 0 )
    {
        return 0.0f;
    }

    if ( (iSection >= 0) && (iSection < m_fSectionStartTimeList.GetCount()) )
    {
        return m_fSectionStartTimeList[iSection];
    }

    return -1.0f;
}

bool cutsManager::IsConcatDataValid() const
{
	bool IsValid = false; 

	if(GetCutfFile())
	{
		if(GetCutfFile()->GetCutsceneFlags().IsSet(cutfCutsceneFile2::CUTSCENE_EXTERNAL_CONCAT_FLAG))
		{
			IsValid = true; 
		}
	}
	
	return IsValid; 
}

int cutsManager::GetStartingConcatSection() const
{
    return GetConcatSectionForTime( GetStartTime() );
}

int cutsManager::GetPreviousConcatSection( int iConcatSection ) const
{
    int iStartConcatSection = GetConcatSectionForTime( GetStartTime() );
    if ( iConcatSection <= iStartConcatSection  || !IsConcatSectionValid(iStartConcatSection))
    {
        return -1;
    }

    return iConcatSection - 1;
}

int cutsManager::GetNextConcatSection( int iConcatSection ) const
{
    int iEndConcatSection = GetConcatSectionForTime( GetEndTime() );
    if ( !IsConcatSectionValid (iEndConcatSection) || iConcatSection >= iEndConcatSection)
    {
        return -1;
    }

    return iConcatSection + 1;
}

int cutsManager::GetNextValidConcatSection(int iConcatSection ) const 
{
	if(IsConcatDataValid())
	{	
		for(int i = iConcatSection + 1; i < m_concatDataList.GetCount(); i++)
		{
			if(IsConcatSectionValidForPlayBack(i))
			{
				return i; 
			}
		}
	}
	return -1; 
}

int cutsManager::GetPreviousValidConcatSection(int iConcatSection ) const 
{
	if(IsConcatDataValid())
	{	
		for(int i = iConcatSection-1; i >=0; i--)
		{
			if(IsConcatSectionValidForPlayBack(i))
			{
				return i; 
			}
		}
	}
	return -1; 
}

int cutsManager::GetConcatSectionForTime( float fTime ) const
{
	if(IsConcatDataValid())
	{	
		// Figure out what concat section we're in
		if ( m_concatDataList.GetCount() == 0 )
		{
			return 0;
		}

		for ( int i = m_concatDataList.GetCount() - 1; i >= 0; --i )
		{
			if ( fTime >= m_concatDataList[i].fStartTime && fTime <= GetEndTime() )
			{
				return i;
			}
		}
	}
    return -1;
}

bool cutsManager::IsConcatSectionValid(s32 iSection) const
{
	if(IsConcatDataValid())
	{
		if (m_concatDataList.GetCount() == 0 || iSection >= m_concatDataList.GetCount() || iSection < 0 )
		{
			return false;
		}
		else
		{
			return true; 
		}
	}
	return false; 
}

float cutsManager::GetConcatSectionStartTime(s32 iSection) const
{
	if(IsConcatSectionValid(iSection))
	{
		return m_concatDataList[iSection].fStartTime; 
	}
	return -1.f; 
}

bool cutsManager::IsConcatted() const
{
	//The external concat flag indicates two different scenes have been concatted not the same scene concatted with 2 different frame ranges. 
	
	if(IsConcatDataValid())
	{
		if (m_concatDataList.GetCount() > 0 )
		{
			return true;
		}
	}
	return false; 
}

bool cutsManager::IsConcatSectionValidForPlayBack(int iConcatSection) const
{
	bool Valid = false; 
	if(IsConcatDataValid())
	{
		if (!IsConcatSectionValid(iConcatSection))
		{
			Valid = false;
		}
		else
		{
			Valid =  m_concatDataList[iConcatSection].bValidForPlayBack; 
		}
	}
	return Valid;
}

float cutsManager::GetConcatSectionDuration(int iConcatSection) const
{
	if(IsConcatDataValid())
	{	
		if(IsConcatSectionValid(iConcatSection))
		{
			if(iConcatSection == m_concatDataList.GetCount() -1 )
			{
				return GetEndTime() - m_concatDataList[iConcatSection].fStartTime; 
			}
			else
			{
				return m_concatDataList[iConcatSection+1].fStartTime  - m_concatDataList[iConcatSection].fStartTime; 
			}
		}
	}
	return 0.0f; 
}

void cutsManager::GetModelObjectsOfType( atArray<const cutfObject*> &ModelObjectList, int ObjectType ) const
{
    // find the models
    atArray<cutfObject *> modelObjects;
	if (GetCutfFile())
	{
		GetCutfFile()->FindObjectsOfType( CUTSCENE_MODEL_OBJECT_TYPE, modelObjects );
	}
	else
	{
		return;
	}
    // add each model to our list
    for ( int i = 0; i < modelObjects.GetCount(); ++i )
    {
        cutfModelObject *pModelObject = static_cast<cutfModelObject *>( modelObjects[i] );
		
		s32 modeltype = pModelObject->GetModelType(); 
		if (  modeltype == ObjectType   )
        {
            ModelObjectList.Grow() = pModelObject;
        }
    }
}

const cutfEvent* cutsManager::GetEventInternal( int iStartingIndex, s32 iObjectId, s32 iEventId ) const
{
	
	if (cutsVerifyf(GetCutfFile(), "GetNextEvent: Cutf file not loaded cannot access object list so it will not be updated"))
	{
		const atArray<cutfEvent *> &eventList = GetCutfFile()->GetEventList();
    for ( int i = iStartingIndex; i < eventList.GetCount(); ++i )
    {
        if ( eventList[i]->GetTime() > GetEndTime() )
        {
            break;
        }

        if ( eventList[i]->GetType() == CUTSCENE_OBJECT_ID_EVENT_TYPE )
        {
            const cutfObjectIdEvent *pObjectIdEvent = dynamic_cast<const cutfObjectIdEvent *>( eventList[i] );
            if ( (pObjectIdEvent->GetObjectId() == iObjectId) 
                && ((iEventId == -1) || (pObjectIdEvent->GetEventId() == iEventId)) )
            {
                return eventList[i];
            }
        }
    }
	}
    return NULL;
}

bool cutsManager::LoadCutFile( const char* pFilename )
{
	if (cutsVerifyf(GetCutfFile(),"LoadCutFile: No cutfile object exists cannot cannot parse it" ))
	{
		if ( !GetCutfFile()->LoadFile( pFilename ) )
    {
        cutsErrorf( "Unable to load cut file '%s'.", pFilename );
        m_streamingState = STREAMING_IDLE_STATE;
        return false;
    }

#if __BANK
		if ( !GetCutfFile()->LoadTuneFile() )
    {
			cutsWarningf( "Unable to load the tune file '$/tune/cutscene/%s.%s'.", GetCutfFile()->GetSceneName(), PI_CUTSCENE_TUNEFILE_EXT );
    }

		if ( !GetCutfFile()->LoadSubtitleFile() )
    {
			cutsWarningf( "Unable to load the subtitle file '$/tune/cutscene/%s.%s'.", GetCutfFile()->GetSceneName(), PI_CUTSCENE_SUBTITLEFILE_EXT );
    }
#endif // __BANK

    m_streamingState = STREAMING_IDLE_STATE;
    return true;
}
	else
	{
		return false;
	}
}

void cutsManager::DoIdleState()
{

}

void cutsManager::DoLoadCutfileState()
{
    m_streamingState = STREAMING_CUTFILE_STATE;

    // first, load the cut file
    if ( !LoadCutFile( m_cCutfileToLoad ) )
    {
        cutsErrorf( "Unable to load cut file '%s", m_cCutfileToLoad );

        // Use the fade in @ beginning flag, because it was probably paired with the fade out game flag at the beginning
        m_fadeInGameAtEndWhenStopping = m_fadeInCutsceneAtBeginning;

        m_cutsceneState = CUTSCENE_UNLOAD_STATE;
        m_streamingState = STREAMING_IDLE_STATE;
    }
    else
    {
        m_cutsceneState = CUTSCENE_LOADING_CUTFILE_STATE;
    }
}

void cutsManager::DoLoadingCutfileState()
{
    // wait for the cutfile to be loaded
    if ( !IsLoading() )
    {
        RAGE_TRACK( cutsManager_DoLoadingCutfileState );

		if (cutsVerifyf(GetCutfFile(), "DoLoadingCutfileState::No cut file has loaded"))
		{
        m_streamingState = STREAMING_IDLE_STATE;
        m_bCutfileStreamed = true;

        // Retrieve the section splits
		GetCutfFile()->GetCurrentSectionTimeList( m_fSectionStartTimeList );

        // Retrieve the concat section times
        GetCutfFile()->GetCurrentConcatDataList( m_concatDataList );

        // Retrieve the playback range
		m_fStartTime = GetStartTimeFromStartFrame(); 
		m_fEndTime = GetEndTimeFromEndFrame(); 	

		m_iStartFrame = GetStartFrame();
		m_iEndFrame = GetEndFrame();

		//SetInternalTime(m_fStartTime); 
		IntialiseTimers(m_fStartTime, m_fStartTime); 

#if __BANK
        m_iLoopStartFrame = GetCutfFile()->GetRangeStart();
        m_iLoopEndFrame = GetCutfFile()->GetRangeEnd();
#endif // __BANK


		SetSceneOffset(GetCutfFile()->GetOffset());
		SetSceneRotation(GetCutfFile()->GetRotation());
		SetScenePitch(GetCutfFile()->GetPitch());
		SetSceneRoll(GetCutfFile()->GetRoll());
		// get the scene name
			safecpy( m_cCurrentSceneName, GetCutfFile()->GetSceneName(), sizeof(m_cCurrentSceneName) );

#if !__FINAL
        // Override the offset
        float fOffset[3];
        if ( PARAM_cutsOffset.GetArray( fOffset, 3 ) )
        {
			Vector3 vSceneOffset;
            vSceneOffset.Set( fOffset[0], fOffset[1], fOffset[2] );
			SetSceneOffset(vSceneOffset);
        }

        // Override the rotation
		float sceneRotation;
        if (PARAM_cutsRotation.Get( sceneRotation ))
		{
			SetSceneRotation(sceneRotation);
		}
#endif // !__FINAL
       	
        // Retrieve the depth of field setting
        m_bDepthOfFieldEnabled = GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_ENABLE_DEPTH_OF_FIELD_FLAG );

       // m_fTime = 0.0f;

        m_cutsceneState = CUTSCENE_LOADED_CUTFILE_STATE;
        m_streamingState = STREAMING_IDLE_STATE;
        
        if ( !m_bJustLoadCutfile )
        {
            ResumeLoad();
        }
    }
		else
		{
			m_cutsceneState = CUTSCENE_UNLOAD_STATE;
			m_streamingState = STREAMING_IDLE_STATE;
		}
	}
}

void cutsManager::DoFadingOutState()
{
    // wait for the fade to complete
    if ( GetTime() >= m_fFadeOutEndTime )
    {
        if ( m_bIsStoppingForEndFadeOut )                    
        {
            m_cutsceneState = CUTSCENE_STOPPED_STATE;

            DispatchEventToAllEntities( CUTSCENE_STOP_EVENT );
        }
        else
        {
            m_cutsceneState = CUTSCENE_LOAD_STATE;
        }
    }
    else
    {
        // otherwise, keep updating
        cutsUpdateEventArgs updateEventArgs( GetTime(), m_fDeltaTime, m_fSectionStartTime, m_fSectionDuration );
        DispatchEventToAllEntities( m_bIsStoppingForEndFadeOut ? CUTSCENE_UPDATE_EVENT : CUTSCENE_UPDATE_FADING_OUT_AT_BEGINNING_EVENT, 
            &updateEventArgs );
    }
}

void cutsManager::DoLoadState()
{
	if(!cutsVerifyf(GetCutfFile(),"DoLoadState: No cutfile object loaded  " ) )
	{
		m_cutsceneState = CUTSCENE_UNLOAD_STATE;
		m_streamingState = STREAMING_IDLE_STATE;
		return;
	}
	
	//SetInternalTime(GetStartTime()); 
    m_streamingState = STREAMING_ASSETS_STATE;

    const atArray<cutfEvent*>& pLoadEventList = GetCutfFile()->GetLoadEventList();

    // create the entities we need for loading purposes
    atArray<s32> iObjectIdsToReserve;
    GetObjectIdsInEventList( pLoadEventList, iObjectIdsToReserve );

    const atArray<cutfObject*>& pObjectList = GetCutfFile()->GetObjectList();
    for ( int i = 0; i < iObjectIdsToReserve.GetCount(); ++i )
    {
        GetEntityObject( pObjectList[iObjectIdsToReserve[i]] );
    }

    // dispatch all of the load events
    for ( int i = 0; i < pLoadEventList.GetCount(); ++i )
    {
        // skip any fade outs if we are overriding them
        if ( (pLoadEventList[i]->GetEventId() == CUTSCENE_FADE_OUT_EVENT) && (m_fadeOutGameAtBeginning != DEFAULT_FADE) )
        {
            continue;
        }

        DispatchEvent( pLoadEventList[i] );
    }

    m_cutsceneState = CUTSCENE_LOADING_STATE;
}

void cutsManager::DoLoadingState()
{
	if(!cutsVerifyf(GetCutfFile(),"DoLoadingState: No cutfile object loaded  " ) )
	{
		m_cutsceneState = CUTSCENE_UNLOAD_STATE;
		m_streamingState = STREAMING_IDLE_STATE;
		return;
	}
	
	
    if ( !IsLoading() )
    {                
        m_streamingState = STREAMING_IDLE_STATE;
        m_bAssetsStreamed = true;
        m_bFadedOutCutsceneAtEnd = false;
		m_bHasFadeInGameAtEndBeenCalled = false; 

       // SetInternalTime(GetStartTime());
#if __BANK
		 m_PlayBackState = PLAY_STATE_FORWARDS_NORMAL_SPEED; 
#endif // __BANK
       // m_iNextEventIndex = 0;
        m_bDispatchedStartScene = false;

        m_bIsTerminating = false;
        m_bReachedPlayBackEnd = false;

        // create the rest of the entities (GetEntityObject knows when we've already created one)
        const atArray<cutfObject*>& pObjectList = GetCutfFile()->GetObjectList();
        for ( int i = 0; i < pObjectList.GetCount(); ++i )
        {
            GetEntityObject( pObjectList[i] );
        }

#if __BANK
        AddSceneWidgets();
#elif !__FINAL
        PopulatePedModelFaceList();
#endif // __BANK

        // regardless of m_bPlayNow, we need to be in this state in order for Play() to start the scene
        m_cutsceneState = CUTSCENE_PAUSED_STATE;

        if ( m_bPlayNow 
#if __BANK
            || (!m_bIsCancellingLoad && IsRestarting())
#endif // __BANK
            )
        {
#if __BANK
            // make sure this gets reset
            m_bIsRestarting = false;
#endif // __BANK

            m_bIsTerminating = false;
            m_bReachedPlayBackEnd = false;

            Play();
        }
    }
    else
    {
        DispatchEventToAllEntities( CUTSCENE_UPDATE_LOADING_EVENT );
    }
}

void cutsManager::DoLoadingBeforeResumingState()
{
    if ( !IsLoading() )
    {
        m_streamingState = STREAMING_IDLE_STATE;
        m_bAssetsStreamed = true;

        // we need to be in this state in order for Play() to resume the scene
		m_cutsceneState = CUTSCENE_PAUSED_STATE;

        Play();
    }
    else
    {        
		DispatchEventToAllEntities(CUTSCENE_PAUSE_EVENT); 
        DispatchEventToAllEntities( CUTSCENE_UPDATE_LOADING_EVENT );
    }
}

void cutsManager::UpdatePlayBackEvents()
{
	// look for events to dispatch
	const atArray<cutfEvent*>& pEventList = GetCutfFile()->GetEventList();

#if __BANK
	bool IsSteppingForwards = GetScrubbingState() == SCRUBBING_STEPPING_FORWARDS || GetScrubbingState()==SCRUBBING_FORWARDS_FROM_TIME_LINE_BAR; 
	bool IsSteppingBackWards = GetScrubbingState() == SCRUBBING_STEPPING_BACKWARDS || GetScrubbingState()==SCRUBBING_BACKWARDS_FROM_TIME_LINE_BAR; 

	if ( m_PlayBackState == PLAY_STATE_FORWARDS_NORMAL_SPEED BANK_ONLY(|| m_PlayBackState ==  PLAY_STATE_FORWARDS_SCALED || IsSteppingForwards))
#endif // __BANK
	{
		//Going forward
#if __BANK
		bool applyFinalEventsIdScrubbing = true;

		if(GetTime() == GetCutfFile()->GetTotalDuration())
		{
			if(IsSteppingForwards)
			{
				applyFinalEventsIdScrubbing = false; 
			}
		}

		if(applyFinalEventsIdScrubbing)
#endif
		{
			while ( (m_iNextEventIndex < pEventList.GetCount()) && pEventList[m_iNextEventIndex]->GetTime() <= GetTime() )
			{
				if ( IsConcatted() )
				{
					//s32 currentSection = GetConcatSectionForTime(GetTime());

					s32 eventSection = GetConcatSectionForTime(pEventList[m_iNextEventIndex]->GetTime());
					bool bValidEventSection = IsConcatSectionValidForPlayBack(eventSection);
					static bool bForceDispatchEvent = false;
					
					if(!bValidEventSection)
					{
						if(pEventList[m_iNextEventIndex]->GetEventId() == CUTSCENE_CLEAR_LIGHT_EVENT)
						{
							//only dispatch the clear light event if we have come from valid concat section
							if(eventSection > 0 && IsConcatSectionValidForPlayBack(eventSection - 1))
							{
								//The clear light event has been moved just beyond the start of the invalid section
								if(pEventList[m_iNextEventIndex]->GetTime() < GetConcatSectionStartTime(eventSection) + (1.0f/ CUTSCENE_FPS))
								{
									bValidEventSection = true;
								}
							}					
						}
					}

					if (bValidEventSection || bForceDispatchEvent)
					{
						//cutsManagerDebugf("DispatchEvent (eventTime:%f) (eventSection:%d) (currentTime:%f) (currentSection:%d) = %s", 
						//	pEventList[m_iNextEventIndex]->GetTime(), eventSection, GetTime(), currentSection, pEventList[m_iNextEventIndex]->GetDisplayName());

						DispatchEvent( pEventList[m_iNextEventIndex] );
					}			
					else
					{
					//cutsManagerDebugf("DO NOT DispatchEvent (eventTime:%f) (eventSection:%d) (currentTime:%f) (currentSection:%d) = %s", 
						//	pEventList[m_iNextEventIndex]->GetTime(), eventSection, GetTime(), currentSection, pEventList[m_iNextEventIndex]->GetDisplayName());
					}
				}
				else
				{
					DispatchEvent( pEventList[m_iNextEventIndex] );
				}

				++m_iNextEventIndex;			
			}
		}
		if ( !m_bFadedOutCutsceneAtEnd && FadeOutCutsceneAtEnd( m_fadeOutCutsceneAtEnd ) )
		{
			m_bFadedOutCutsceneAtEnd = true; // so that we only trigger it once
		}
	}
#if __BANK
	else if(  m_PlayBackState == PLAY_STATE_BACKWARDS BANK_ONLY(|| IsSteppingBackWards))
	{
		// play backward
		const atArray<cutfEvent*>& pEventList = GetCutfFile()->GetEventList();
		if(m_iNextEventIndex < pEventList.GetCount() )
		{
			while ( (m_iNextEventIndex >= 0) && pEventList[m_iNextEventIndex]->GetTime() > GetTime() )
			{
				DispatchOppositeEvent( pEventList[m_iNextEventIndex] );
				--m_iNextEventIndex;
			}
		}
		//reset all the play events to zero as the above does not reset to zero, will mean all the play events will get re-dispatched but that should be fine.
		if(GetTime() ==0.0f)
		{
			while(m_iNextEventIndex > 0)
			{
				DispatchOppositeEvent( pEventList[m_iNextEventIndex] );
				--m_iNextEventIndex;

				if(m_iNextEventIndex == 0)
				{
					DispatchOppositeEvent( pEventList[m_iNextEventIndex] );
				}
			}
		}
	}
#endif //__BANK

	// dispatch the update event
	cutsUpdateEventArgs updateEventArgs( GetTime(), m_fDeltaTime, m_fSectionStartTime, m_fSectionDuration );
	DispatchEventToAllEntities( CUTSCENE_UPDATE_EVENT, &updateEventArgs );
}

void cutsManager::DoPlayState()
{
	if(!cutsVerifyf(GetCutfFile(),"DoLoadingState: No cutfile object loaded  " ) )
	{
		m_cutsceneState = CUTSCENE_UNLOAD_STATE;
		m_streamingState = STREAMING_IDLE_STATE;
		return;
	}
	
    if ( !m_bDispatchedStartScene )
	{
		//SetInternalTime(GetStartTime()); 
        
		DispatchEventToAllEntities( CUTSCENE_START_OF_SCENE_EVENT );

        m_bDispatchedStartScene = true;

        FadeInCutsceneAtBeginning( m_fadeInCutsceneAtBeginning );

#if __BANK
        // turn debug drawing back on/off
        BankDisplayDebugLinesChangedCallback();
#endif // __BANK
    }
	
	UpdatePlayBackEvents(); 

	const atArray<cutfEvent*>& pEventList = GetCutfFile()->GetEventList();

#if __BANK
	bool IsSteppingForwards = GetScrubbingState() == SCRUBBING_STEPPING_FORWARDS || GetScrubbingState()==SCRUBBING_FORWARDS_FROM_TIME_LINE_BAR; 
	bool IsSteppingBackWards = GetScrubbingState() == SCRUBBING_STEPPING_BACKWARDS || GetScrubbingState()==SCRUBBING_BACKWARDS_FROM_TIME_LINE_BAR; 
	
	// grab the latest light anim data
	UpdateLightEditInfo(); 
	
    if (IsSteppingForwards || IsSteppingBackWards )
    {
		cutsDisplayf("Triggering pause from stepping forwards or backwards");
		Pause();
    }
    else if ( m_PlayBackState == PLAY_STATE_BACKWARDS )
    {
        if ( m_iNextEventIndex == 0 )
        {
            m_bStepped = false;
            Pause();
        }
        else 
        {
			//played to the start stop playing       
			float fStartTime = GetStartTime();
            if ( GetTime() <= fStartTime )
            {
                m_bStepped = false;
                m_PlayBackState = PLAY_STATE_FORWARDS_NORMAL_SPEED; // next time we click Play, we need to play forward
				cutsDisplayf("Triggering pause from playing backwards to the start");
                Pause();
            }
        }
    }
    else
    {
        if ( m_iNextEventIndex == pEventList.GetCount() )
        {
            m_bStepped = false;
            m_bReachedPlayBackEnd = true;

            // skip the fade out, we should have done it already
            Stop( SKIP_FADE, m_fadeInGameAtEnd );
        }
        else
        {
            float fEndTime = GetEndTime();
			if ( GetTime() >= fEndTime )
            {
				cutsWarningf("Stop was called (%s) but not all the events where dispatched, check the cuts data", GetCutsceneName());
                m_bStepped = false;
                m_bReachedPlayBackEnd = true;

                // skip the fade out, we should have done it already
                Stop( SKIP_FADE, m_fadeInGameAtEnd );
            }
        }
    }
#else // __BANK
    if ( m_iNextEventIndex == pEventList.GetCount() )
    {
        m_bReachedPlayBackEnd = true;

        // skip the fade out, we should have done it already
        Stop( SKIP_FADE, m_fadeInGameAtEnd );
    }
	else
	{
		float fEndTime = GetEndTime();
		if ( GetTime() >= fEndTime )
		{
			m_bReachedPlayBackEnd = true;

			// skip the fade out, we should have done it already
			Stop( SKIP_FADE, m_fadeInGameAtEnd );
		}
	}
#endif // __BANK
}

void cutsManager::DoPausedState()
{

}

void cutsManager::DoStoppedState()
{
    DispatchEventToAllEntities( CUTSCENE_END_OF_SCENE_EVENT );    

    m_cutsceneState = CUTSCENE_UNLOAD_STATE;
}

void cutsManager::DoCancellingState()
{
    if ( !IsLoading() )
    {
        // must be in play or pause to stop
        m_cutsceneState = CUTSCENE_PAUSED_STATE;

        // skip the fade out, we probably did it already
        Stop( SKIP_FADE, m_fadeInGameAtEnd );
    }
    else
    {
        DispatchEventToAllEntities( CUTSCENE_UPDATE_LOADING_EVENT );
    }
}

void cutsManager::DoUnloadState()
{
	if( !GetCutfFile() )
	{
		m_cutsceneState = CUTSCENE_UNLOAD_STATE;
		m_streamingState = STREAMING_IDLE_STATE;
		return;
	}
	
	const atArray<cutfEvent*>& pLoadEventList = GetCutfFile()->GetLoadEventList();
    const atArray<cutfObject*>& pObjectList = GetCutfFile()->GetObjectList();

    // release all entities, except for those that we need to dispatch the unload events.  That way if we unload
    // a model, for example, the entity won't still have control over it.
    atArray<s32> iObjectIdsToRelease;
    GetObjectIdsInEventList( pLoadEventList, iObjectIdsToRelease, true );

    for ( int i = 0; i < iObjectIdsToRelease.GetCount(); ++i )
    {
        if ( (pObjectList[iObjectIdsToRelease[i]]->GetType() == CUTSCENE_SCREEN_FADE_OBJECT_TYPE) 
            && (((m_fadeInGameAtEnd == DEFAULT_FADE) && GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_IN_GAME_FLAG ))
            || (m_fadeInGameAtEnd == FORCE_FADE)) )
        {
            // If we need to do a fade in, don't delete the Screen Fade Object just yet.
            continue;
        }

        DeleteEntityObject( pObjectList[iObjectIdsToRelease[i]] );
    }

    // dispatch all of the unload events
    for ( int i = 0; i < pLoadEventList.GetCount(); ++i )
    {
        if ( pLoadEventList[i]->GetEventId() == CUTSCENE_FADE_OUT_EVENT )
        {
            // Don't dispatch any opposite fade events, we'll take care of that in DoUnloadingState().
            continue;
        }

        DispatchOppositeEvent( pLoadEventList[i] );
    }

    m_cutsceneState = CUTSCENE_UNLOADING_STATE;
}

void cutsManager::DoUnloadingState()
{
	if( !GetCutfFile() )
	{
		m_cutsceneState = CUTSCENE_UNLOAD_STATE;
		m_streamingState = STREAMING_IDLE_STATE;
		return;
	}
	
    if ( !IsUnloading() )
    {
        DispatchEventToAllEntities( CUTSCENE_UNLOADED_EVENT );
        
        // Go ahead to the idle state so that the fade entity knows we're done
        m_cutsceneState = CUTSCENE_IDLE_STATE;

        FadeInGameAtEnd( m_fadeInGameAtEndWhenStopping );

        // delete the rest of the entities (DeleteEntityObject knows when we've already deleted one)
        const atArray<cutfObject*>& pObjectList = GetCutfFile()->GetObjectList();
        for ( int i = 0; i < pObjectList.GetCount(); ++i )
        {
            DeleteEntityObject( pObjectList[i] );
        }

        // we're done
        Clear();
    }
    else
    {
        // dispatch the update unloading event
        cutsUpdateEventArgs updateEventArgs( GetTime(), m_fDeltaTime, m_fSectionStartTime, m_fSectionDuration );
        DispatchEventToAllEntities( CUTSCENE_UPDATE_UNLOADING_EVENT, &updateEventArgs );
    }
}

void cutsManager::DispatchEvent( const cutfEvent *pEvent )
{
	if(!cutsVerifyf(GetCutfFile(),"DispatchEvent: No file object file resetting cutscene"))
	{
		m_cutsceneState = CUTSCENE_UNLOAD_STATE;
		m_streamingState = STREAMING_IDLE_STATE;
		return;
	}

#if __BANK
    if ( pEvent && (pEvent->GetEventId() == CUTSCENE_LOAD_SCENE_EVENT || pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT) )
    {
        const cutfObjectIdEvent *pObjectIdEvent = dynamic_cast<const cutfObjectIdEvent *>( pEvent );
        const cutfObject *pTheObject = GetCutfFile()->GetObjectList()[pObjectIdEvent->GetObjectId()];
        SEntityObject *pEntityObject = GetEntityObject( pTheObject, false );
        if ( pEntityObject != NULL )
        {
            if ( pEvent->GetEventId() == CUTSCENE_LOAD_SCENE_EVENT)
            {
				const cutfEventArgs* pEventArgs = pEvent->GetEventArgs(); 				
				if(pEventArgs && pEventArgs->GetType() == CUTSCENE_LOAD_SCENE_EVENT_ARGS_TYPE)
				{
					// Make sure the Load Scene event has up-to-date info
					const cutfLoadSceneEventArgs *pLoadSceneEventArgs = static_cast<const cutfLoadSceneEventArgs *>( pEvent->GetEventArgs() );
					cutfLoadSceneEventArgs loadSceneEventArgs( pLoadSceneEventArgs->GetName(), m_vSceneOffset, m_fSceneRotation, m_fScenePitch, m_fSceneRoll,
					pLoadSceneEventArgs->GetAttributeList() );				
					pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, pEvent->GetEventId(), &loadSceneEventArgs );
				}
			}
            else if ( pEvent->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT)
            {
				const cutfEventArgs* pEventArgs = pEvent->GetEventArgs(); 
				
				if(pEventArgs && pEventArgs->GetType() == CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE)
				{
#if __BANK						
					// Make sure the Camera Cut event has up-to-date info
					const cutfCameraCutEventArgs *pConstCameraCutEventArgs = static_cast<const cutfCameraCutEventArgs *>( pEventArgs );

					cutfCameraCutEventArgs* pCameraCutEventArgs = const_cast<cutfCameraCutEventArgs*>(pConstCameraCutEventArgs); 

					for ( int i = 0; i < m_editDrawDistanceList.GetCount(); ++i )
					{
						if ( m_editDrawDistanceList[i]->pEvent == pEvent )
						{
							pCameraCutEventArgs->SetNearDrawDistance( m_editDrawDistanceList[i]->bOverrideNearClip 
								? m_editDrawDistanceList[i]->fNearClip : m_editDrawDistanceList[i]->fOriginalNearClip );
							pCameraCutEventArgs->SetFarDrawDistance( m_editDrawDistanceList[i]->bOverrideFarClip 
								? m_editDrawDistanceList[i]->fFarClip : m_editDrawDistanceList[i]->fOriginalFarClip );
							break;
						}
					}
#endif
					pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, pEvent->GetEventId(), pEventArgs );
				}
			}
        }
        
        return;
    }
#endif // __BANK

    switch ( pEvent->GetType() )
    {
    case CUTSCENE_EVENT_TYPE:
        {
            DispatchEventToAllEntities( pEvent->GetEventId(), pEvent->GetEventArgs() );
        }
        break;

    case CUTSCENE_OBJECT_ID_EVENT_TYPE:
        {
            // locate the SEntityObject that is the destination for the event
            const cutfObjectIdEvent *pObjectIdEvent = dynamic_cast<const cutfObjectIdEvent *>( pEvent );
            const cutfObject *pTheObject = GetCutfFile()->GetObjectList()[pObjectIdEvent->GetObjectId()];
            SEntityObject *pEntityObject = GetEntityObject( pTheObject, false );
            if ( pEntityObject != NULL )
            {
	//Hack gta4: Rely on entities existing for the entire life of scene, because they now are responsible for creating, deleting,
	//recreating and so on of game objects because there life is dependent on being streamed. Future Dev:could kill entities whose life is over because 
	//the objects they managed are dead forever. This would require to look at the streaming of objects and mark the entities for deletion. 

#if !HACK_GTA4				   
			//Rely on the entity being around if we are streaming a model out but we will create it later				
				// if we're in the playback or unload(ing) state and we unload models, we need to delete their entities
                if ( ((m_cutsceneState == CUTSCENE_PLAY_STATE) || (m_cutsceneState == CUTSCENE_UNLOAD_STATE) || (m_cutsceneState == CUTSCENE_UNLOADING_STATE)) 
                    && (pEvent->GetEventId() == CUTSCENE_UNLOAD_MODELS_EVENT) )
                {
                    const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEvent->GetEventArgs() );
                    if ( pObjectIdListEventArgs != NULL )
                    {
                        const atArray<s32>& iObjectIdList = pObjectIdListEventArgs->GetObjectIdList();
                        for ( int i = 0; i < iObjectIdList.GetCount(); ++i )
                        {
                            const cutfObject* pObject = GetObjectById( iObjectIdList[i] );
                            if ( pObject != NULL )
                            {
                                DeleteEntityObject( pObject );
                            }
                            else
                            {
                                cutsWarningf( "Object %d has already been deleted.", iObjectIdList[i] );
                            }
                        }
                    }
                    else
                    {
                        cutsWarningf( "Could not dynamically cast cutfEventArgs to cutfObjectIdListEventArgs" );
                    }
                }
#endif
                pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, pEvent->GetEventId(), pEvent->GetEventArgs(), pEvent->GetTime() );
            }
            else
            {
                cutsWarningf( "No Entity for Object (%d) '%s' was created", 
                    pTheObject->GetObjectId(), pTheObject->GetDisplayName().c_str() );
            }
        }
        break;

    case CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE:
        {
            // locate the SEntityObject that is the destination for the event
            const cutfObjectIdListEvent *pObjectIdListEvent = dynamic_cast<const cutfObjectIdListEvent *>( pEvent );

            const atArray<s32>& iObjectIdList = pObjectIdListEvent->GetObjectIdList();
            const atArray<cutfEventArgs*>& pEventArgsList = pObjectIdListEvent->GetEventArgsList();

            for ( int i = 0; i < iObjectIdList.GetCount(); ++i )
            {
                const cutfObject *pTheObject = GetCutfFile()->GetObjectList()[iObjectIdList[i]];
                SEntityObject *pEntityObject = GetEntityObject( pTheObject, false );
                if ( pEntityObject != NULL )
                {
                    if ( pEvent->GetEventArgs() == NULL )
                    {
                        if ( i < pEventArgsList.GetCount() )
                        {
                            pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, pEvent->GetEventId(), pEventArgsList[i] );
                        }
                        else
                        {
                            pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, pEvent->GetEventId() );
                        }
                    }
                    else
                    {
                        pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, pEvent->GetEventId(), pEvent->GetEventArgs() );
                    }
                }
                else
                {
                    cutsWarningf( "No Entity for Object (%d) '%s' was created", 
                        pTheObject->GetObjectId(), pTheObject->GetDisplayName().c_str() );
                }
            }
        }
        break;
    }
}

void cutsManager::DispatchOppositeEvent( const cutfEvent *pEvent )
{
	if(!cutsVerifyf(GetCutfFile(),"DispatchOppositeEvent: No file object file resetting cutscene"))
	{
		m_cutsceneState = CUTSCENE_UNLOAD_STATE;
		m_streamingState = STREAMING_IDLE_STATE;
		return;
	}
	

	  s32 iOppositeEventId = pEvent->GetOppositeEventId();

    switch ( pEvent->GetType() )
    {
    case CUTSCENE_EVENT_TYPE:
        {
            if ( iOppositeEventId >= 0 )
            {
                DispatchEventToAllEntities( iOppositeEventId, pEvent->GetEventArgs() );
            }
        }
        break;

    case CUTSCENE_OBJECT_ID_EVENT_TYPE:
        {        
            const cutfObjectIdEvent *pObjectIdEvent = dynamic_cast<const cutfObjectIdEvent *>( pEvent );

            if ( iOppositeEventId == -1 )
            {
                // look for the previous instance of this event that was sent to this object
                atArray<cutfEvent *> objectEventList;
                GetCutfFile()->FindEventsForObjectIdOnly( pObjectIdEvent->GetObjectId(), GetCutfFile()->GetEventList(), objectEventList );

				switch(pEvent->GetEventId())
				{
					case CUTSCENE_SET_VARIATION_EVENT:
					{
#if __BANK
						//added this to deal with ped variation opposite events, they are not standard in the sense of looking for just the previous event type
						//this function looks for the previous event just for that component and dispatches an event if it finds one. 
						DispatchOppositeVariationEvent(pEvent, objectEventList); 
#endif
					}
					break; 
				default:
					{
						for ( int i = 0; i < objectEventList.GetCount(); ++i )
						{
							if ( objectEventList[i] == pEvent )
							{
								int index = i - 1;
								while ( (index >= 0) && (objectEventList[index]->GetEventId() != pEvent->GetEventId() ) )
								{
									--index;
								}

								if ( index >= 0 )
								{
									DispatchEvent( objectEventList[index] );
									return;
								}
								else
								{
									// FIXME: This is an unhandled case.  Need some sort of default.
									break;
								}                        
							}
						} 
					}
					break; 
				};

				return;
            }

            if ( iOppositeEventId < 0 )
            {
                break;
            }

            const cutfObject *pTheObject = GetCutfFile()->GetObjectList()[pObjectIdEvent->GetObjectId()];
            SEntityObject *pEntityObject = GetEntityObject( pTheObject, false );
            if ( pEntityObject != NULL )
            {
                // if we're in the playback or unload(int) state and we unload models, we need to delete their entities
                if ( ((m_cutsceneState == CUTSCENE_PLAY_STATE) || (m_cutsceneState == CUTSCENE_UNLOAD_STATE) || (m_cutsceneState == CUTSCENE_UNLOADING_STATE))
                    && (iOppositeEventId == CUTSCENE_UNLOAD_MODELS_EVENT) )
                {
                    const cutfObjectIdListEventArgs *pObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( pEvent->GetEventArgs() );
                    if ( pObjectIdListEventArgs != NULL )
                    {
                        const atArray<s32>& iObjectIdList = pObjectIdListEventArgs->GetObjectIdList();
                        for ( int i = 0; i < iObjectIdList.GetCount(); ++i )
                        {
                            const cutfObject* pObject = GetObjectById( iObjectIdList[i] );
                            if ( pObject != NULL )
                            {
                                DeleteEntityObject( pObject );
                            }
                            else
                            {
                                cutsWarningf( "Object %d has already been deleted.", iObjectIdList[i] );
                            }
                        }
                    }
                    else
                    {
                        cutsWarningf( "Could not dynamically cast cutfEventArgs to cutfObjectIdListEventArgs" );
                    }
                }

                pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, iOppositeEventId, pEvent->GetEventArgs() );
            }
            else
            {
                cutsWarningf( "No Entity for Object (%d) '%s' was created", 
                    pTheObject->GetObjectId(), pTheObject->GetDisplayName().c_str() );
            }
        }
        break;

    case CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE:
        {
            const cutfObjectIdListEvent *pObjectIdListEvent = dynamic_cast<const cutfObjectIdListEvent *>( pEvent );
            
            if ( iOppositeEventId == -1 )
            {
                // Look for the previous instance of this event.
                const atArray<cutfEvent *> &eventList = GetCutfFile()->GetEventList();
                for ( int i = 0; i < eventList.GetCount(); ++i )
                {
                    if ( eventList[i] == pEvent )
                    {
                        int index = i - 1;
                        while ( (index >= 0) && (eventList[index]->GetEventId() != pEvent->GetEventId() ) )
                        {
                            --index;
                        }

                        if ( index >= 0 )
                        {
                            DispatchEvent( eventList[index] );
                            return;
                        }
                        else
                        {
                            // FIXME: This is an unhandled case.  Need some sort of default.
                            break;
                        }                        
                    }
                }
            }

            if ( iOppositeEventId < 0 )
            {
                break;
            }

            const atArray<cutfEventArgs*>& pEventArgsList = pObjectIdListEvent->GetEventArgsList();

            const atArray<s32>& iObjectIdList = pObjectIdListEvent->GetObjectIdList();
            for ( int i = 0; i < iObjectIdList.GetCount(); ++i )
            {
                const cutfObject *pTheObject = GetCutfFile()->GetObjectList()[iObjectIdList[i]];
                SEntityObject *pEntityObject = GetEntityObject( pTheObject, false );
                if ( pEntityObject != NULL )
                {
                    if ( pEvent->GetEventArgs() == NULL )
                    {
                        if ( i < pEventArgsList.GetCount() )
                        {
                            pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, iOppositeEventId, pEventArgsList[i] );
                        }
                        else
                        {
                            pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, iOppositeEventId );
                        }
                    }
                    else
                    {
                        pEntityObject->pEntity->DispatchEvent( this, pEntityObject->pObject, iOppositeEventId, pEvent->GetEventArgs() );
                    }
                }
                else
                {
                    cutsWarningf( "No Entity for Object (%d) '%s' was created", 
                        pTheObject->GetObjectId(), pTheObject->GetDisplayName().c_str() );
                }
            }
        }
        break;
    }
}

void cutsManager::DispatchEventToAllEntities( s32 iEventId, const cutfEventArgs *pEventArgs )
{
    atMap<s32, SEntityObject>::Iterator entry = m_cutsceneEntityObjects.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        entry.GetData().pEntity->DispatchEvent( this, entry.GetData().pObject, iEventId, pEventArgs );
    }
}

void cutsManager::DispatchOppositeEventToAllEntities( s32 iEventId, const cutfEventArgs *pEventArgs )
{
    atMap<s32, SEntityObject>::Iterator entry = m_cutsceneEntityObjects.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        entry.GetData().pEntity->DispatchEvent( this, entry.GetData().pObject, iEventId, pEventArgs );
    }
}

s32 cutsManager::FindScreenFadeObjectId( bool bCreateIfNotFound )
{
	if (GetCutfFile())
	{
    atArray<cutfObject *> screenFadeObjectList;
		GetCutfFile()->FindObjectsOfType( CUTSCENE_SCREEN_FADE_OBJECT_TYPE, screenFadeObjectList );

    if ( screenFadeObjectList.GetCount() > 0 )
    {
        return screenFadeObjectList[0]->GetObjectId();
    }

    if ( bCreateIfNotFound )
    {
        RAGE_TRACK( cutsManager_FindScreenFadeObjectId );

        cutfScreenFadeObject *pScreenFadeObj = rage_new cutfScreenFadeObject( -1, "Screen Fade" );
			GetCutfFile()->AddObject( pScreenFadeObj );

        return pScreenFadeObj->GetObjectId();
    }
	}
    return -1;
}

s32 cutsManager::FindAssetManagerObjectId()
{
	if (GetCutfFile())
	{
    atArray<cutfObject *> assetManagerObjectList;
		GetCutfFile()->FindObjectsOfType( CUTSCENE_ASSET_MANAGER_OBJECT_TYPE, assetManagerObjectList );

    if ( assetManagerObjectList.GetCount() > 0 )
    {
        return assetManagerObjectList[0]->GetObjectId();
    }
	}
    return -1;
}

s32 cutsManager::FindAudioObjectId()
{
	if (GetCutfFile())
	{	
    atArray<cutfObject *> audioObjectList;
		GetCutfFile()->FindObjectsOfType( CUTSCENE_AUDIO_OBJECT_TYPE, audioObjectList );

    if ( audioObjectList.GetCount() > 0 )
    {
        return audioObjectList[0]->GetObjectId();
    }
	}
    return -1;
}

s32 cutsManager::FindCameraObjectId()
{
	if (GetCutfFile())
	{
    atArray<cutfObject *> audioObjectList;
		GetCutfFile()->FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, audioObjectList );

    if ( audioObjectList.GetCount() > 0 )
    {
        return audioObjectList[0]->GetObjectId();
    }
	}
    return -1;
}

cutsManager::SEntityObject* cutsManager::GetEntityObject( const cutfObject* pObject, bool bCreateIfNotFound )
{
    RAGE_TRACK( cutsManager_GetEntityObject );

    SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( pObject->GetObjectId() );
    if ( (pEntityObject == NULL) && bCreateIfNotFound )
    {
        SEntityObject entityObject;
        entityObject.pObject = pObject;
        entityObject.pEntity = ReserveEntity( pObject );

        if ( entityObject.pEntity != NULL )
        {
            m_cutsceneEntityObjects.Insert( entityObject.pObject->GetObjectId(), entityObject );
            return m_cutsceneEntityObjects.Access( entityObject.pObject->GetObjectId() );
        }
    }

    return pEntityObject;
}

void cutsManager::DeleteEntityObject( const cutfObject *pObject )
{
    SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( pObject->GetObjectId() );
    if ( pEntityObject != NULL )
    {
        ReleaseEntity( pEntityObject->pEntity, pEntityObject->pObject );
        m_cutsceneEntityObjects.Delete( pObject->GetObjectId() );
    }
}

void cutsManager::GetObjectIdsInEventList( const atArray<cutfEvent*>& pEventList, atArray<s32>& iObjectIdList, bool bInvert ) const
{
    RAGE_TRACK( cutsManager_GetObjectIdsInEventList );

    atMap<s32, s32> iObjectIdMap;
    for ( int i = 0; i < pEventList.GetCount(); ++i )
    {
        switch ( pEventList[i]->GetType() )
        {
        case CUTSCENE_EVENT_TYPE:
            break;
        case CUTSCENE_OBJECT_ID_EVENT_TYPE:
            {
                const cutfObjectIdEvent *pObjectIdEvent = dynamic_cast<const cutfObjectIdEvent*>( pEventList[i] );
                if ( pObjectIdEvent != NULL )
                {
                    iObjectIdMap[pObjectIdEvent->GetObjectId()] = pObjectIdEvent->GetObjectId();
                }
            }
            break;
        case CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE:
            {
                const cutfObjectIdListEvent *pObjectIdListEvent = dynamic_cast<const cutfObjectIdListEvent*>( pEventList[i] );
                if ( pObjectIdListEvent != NULL )
                {
                    const atArray<s32>& iObjectIdList = pObjectIdListEvent->GetObjectIdList();
                    for ( int j = 0; j < iObjectIdList.GetCount(); ++j )
                    {
                        iObjectIdMap[iObjectIdList[i]] = iObjectIdList[i];
                    }
                }
            }
            break;
        }                
    }

    if ( bInvert )
    {
		if(cutsVerifyf(GetCutfFile(),"GetObjectIdsInEventList:: Cannot update list has no cutfobject exists" ))
		{
			const atArray<cutfObject*>& pObjectList = GetCutfFile()->GetObjectList();
        s32 iCount = pObjectList.GetCount() - iObjectIdMap.GetNumUsed();
        if ( iCount > 0 )
        {
            iObjectIdList.Reserve( iCount );
            for ( int i = 0; i < pObjectList.GetCount(); ++i )
            {
                if ( iObjectIdMap.Access( pObjectList[i]->GetObjectId() ) == NULL )
                {
                    iObjectIdList.Append() = pObjectList[i]->GetObjectId();
                }
            }
        }
    }
    }
    else
    {
        iObjectIdList.Reserve( iObjectIdMap.GetNumUsed() );
        atMap<s32, s32>::Iterator entry = iObjectIdMap.CreateIterator();
        for ( entry.Start(); !entry.AtEnd(); entry.Next() )
        {
            iObjectIdList.Append() = entry.GetKey();
        }
    }
}

bool cutsManager::FadeOutGameAtBeginning( EScreenFadeOverride screenFadeOverride )
{
    switch ( screenFadeOverride )
    {
    case DEFAULT_FADE:
		if (cutsVerifyf(GetCutfFile(), "Cutfile object does not exist cannot get fade info"))
		{
			if ( GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_OUT_GAME_FLAG ) )
        {
				return FadeOut( GetCutfFile()->GetFadeAtBeginningColor(), GetCutfFile()->GetFadeOutGameAtBeginningDuration() );
			}
        }
        break;
    case FORCE_FADE:
        return FadeOut( GetDefaultFadeColor(), GetDefaultFadeOutDuration() );
    case SKIP_FADE:
        break;
    }

    return false;
}

bool cutsManager::FadeInCutsceneAtBeginning( EScreenFadeOverride screenFadeOverride )
{
    switch ( screenFadeOverride )
    {
    case DEFAULT_FADE:
		if (cutsVerifyf(GetCutfFile(), "Cutfile object does not exist cannot get fade info"))
		{	
			if ( GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_IN_FLAG ) )
        {
				return FadeIn( GetCutfFile()->GetFadeAtBeginningColor(), GetCutfFile()->GetFadeInCutsceneAtBeginningDuration() );
			}
        }
        break;
    case FORCE_FADE:
        return FadeIn( GetDefaultFadeColor(), GetDefaultFadeInDuration() );
    case SKIP_FADE:
        break;
    }

    return false;
}

bool cutsManager::FadeOutCutsceneAtEnd( EScreenFadeOverride screenFadeOverride, bool bStopping )
{
    float fEndTime = GetEndTime();

    switch ( screenFadeOverride )
    {
		case DEFAULT_FADE:
		{
			if (cutsVerifyf(GetCutfFile(), "Cutfile object does not exist cannot get fade info"))
			{
				if ( GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_OUT_FLAG ) 
					&& (bStopping || (fEndTime - GetCutfFile()->GetFadeOutCutsceneAtEndDuration() <= GetTime())) )
				{
					return FadeOut( GetCutfFile()->GetFadeAtEndColor(), GetCutfFile()->GetFadeOutCutsceneAtEndDuration() );
				}
			}
		}
		
        break;
		case FORCE_FADE:
		{
			if ( bStopping || (fEndTime - GetDefaultFadeOutDuration() <= GetTime()) )
			{
			 return FadeOut( GetDefaultFadeColor(), GetDefaultFadeOutDuration() );
			}
		}
        break;

		case SKIP_FADE:
		{
			//m_fFadeOutEndTime = GetSeconds();  //+  GetDefaultFadeOutDuration();
			
			return false; 
		}
    }

    return false;
}

bool cutsManager::FadeInGameAtEnd( EScreenFadeOverride screenFadeOverride )
{
    m_bHasFadeInGameAtEndBeenCalled = true; 

	switch ( screenFadeOverride )
    {
    case DEFAULT_FADE:
		if (cutsVerifyf(GetCutfFile(), "Cutfile object does not exist cannot get fade info"))
		{
			if ( GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_IN_GAME_FLAG ) )
        {
				return FadeIn( GetCutfFile()->GetFadeAtEndColor(), GetCutfFile()->GetFadeInGameAtEndDuration() );
			}
        }
        break;
    case FORCE_FADE:
        return FadeIn( GetDefaultFadeColor(), GetDefaultFadeInDuration() );
    case SKIP_FADE:
        break;
    }

    return false;
}

float cutsManager::GetDefaultTime( float fDeltaTime )
{
    float fNewTime = GetTime();
    float fAudioTime = GetAudioEntityTime();

#if __PPU
    if ( fAudioTime >= 0.0f )
    {
        fNewTime += fDeltaTime;

#if __BANK
        float fAudioResyncTime = (m_iAudioResyncTime / 1000.0f);
#else // __BANK
        float fAudioResyncTime = CUTSCENE_AUDIO_RESYNC_TIME;
#endif // __BANK

        if ( (fNewTime < fAudioTime - fAudioResyncTime) || (fNewTime > fAudioTime + fAudioResyncTime) || m_bForceAudioSync )
        {
            fNewTime = fAudioTime;

            m_bForceAudioSync = false;
            
            cutsDisplayf( "Resyncing cutscene time with audio (%f).", fNewTime );
        }
    }
    else
    {
        fNewTime += fDeltaTime;
    }
#else // __PPU
    if ( fAudioTime >= 0.0f )
    {
        fNewTime = fAudioTime;
    }
    else
    {
        fNewTime += fDeltaTime;
    }
#endif // __PPU

    return Max<float>( 0.0f, fNewTime );
}

#if !__FINAL

float cutsManager::GetDefaultVCRTime( float fDeltaTime )
{
    float fNewTime = GetTime();

    if ( ( m_PlayBackState == PLAY_STATE_FORWARDS_NORMAL_SPEED ) && !m_bIsStepping && !m_bStepped )
    {
        fNewTime = GetDefaultTime( fDeltaTime );
    }
    else
    {
        fNewTime += (fDeltaTime * m_fPlaybackRate);

        //if ( m_bIsStepping )
        //{
        //    m_bIsStepping = false;
        //    m_bStepped = true;
        //  //  m_fPlaybackRate = 0.0f;
        //}
    }

    return Max<float>( 0.0f, fNewTime );
}

#endif // !__FINAL

#if __BANK

float cutsManager::GetTimestepSourceVCRTime( float fDeltaTime )
{
    float fNewTime = GetTime();

    if ( m_PlayBackState == PLAY_STATE_FORWARDS_NORMAL_SPEED /*&& !m_bIsStepping && !m_bStepped*/ )
    {
        switch ( m_iTimestepSource )
        {
        case GAME_TIMESTEP:
            {
                fNewTime += fDeltaTime;
            }
            break;
        case AUDIO_TIMESTEP:
            {
                fNewTime = GetAudioEntityTime();
            }
            break;
        default:
            {
                fNewTime = GetDefaultTime( fDeltaTime );
            }
            break;
        }
    }
    else
    {
        fNewTime = GetDefaultVCRTime( fDeltaTime );
    }

    return Max<float>( 0.0f, fNewTime );
}

#endif // __BANK

float cutsManager::GetAudioEntityTime()
{
    float fAudioTime = -1.0f;

    s32 iObjectId = FindAudioObjectId();
    if ( iObjectId != -1 )
    {
        SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
        if ( pEntityObject != NULL )
        {
            cutsAudioEntity *pAudioEntity = dynamic_cast<cutsAudioEntity *>( pEntityObject->pEntity );
            if ( pAudioEntity != NULL )
            {
                float fRawAudioTime = pAudioEntity->GetAudioTime( this, pEntityObject->pObject );
                if ( fRawAudioTime != -1.0f )
                {
                    // must subtract an offset if the anims have been sectioned because the audio has not
					if (cutsVerifyf(GetCutfFile(), "Cutfile object does not exist cannot get time info"))
					{	
						if ( GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) )
                    {
							fRawAudioTime -= (GetCutfFile()->GetRangeStart() / CUTSCENE_FPS);
						}
                    }

#if __BANK
                    if ( fRawAudioTime >= 0.0f )
                    {
                        fAudioTime = fRawAudioTime + (m_iAudioOffset / 1000.0f);
                    }
                    else
                    {
                        fAudioTime = fRawAudioTime;
                    }
#else // __BANK
                    fAudioTime = fRawAudioTime;
#endif // __BANK

                    // If we have valid audio, make sure we are at least 0 to avoid stuttering
#if !__FINAL
                    if ( fAudioTime < 0.0f )
                    {
                        cutsWarningf( "Audio Time is negative.  Slamming to 0 to avoid stuttering." );
                    }
#endif // !__FINAL
                    fAudioTime = Max<float>( 0.0f, fAudioTime );
                }
            }
        }
    }

    return fAudioTime;
}
#if HACK_GTA4
void cutsManager::SetCutfFile (cutfCutsceneFile2* pCutfile) 
{
	m_pCutSceneFile = pCutfile;
}
#endif

cutfCutsceneFile2* cutsManager::GetCutfFile()
{ 
#if HACK_GTA4
	return m_pCutSceneFile; 
#else
	return &m_cutsceneFile;
#endif
}

// PURPOSE: Set the cutscene position
void cutsManager::SetSceneOffset(const Vector3& offset)
{
	m_vSceneOffset = offset;
#if __BANK
	m_vBankSceneOffset = offset;
#endif //__BANK
}

// PURPOSE: Set the cutscene heading
void cutsManager::SetSceneRotation(float heading)
{
	m_fSceneRotation = heading;
#if __BANK
	m_fBankSceneRotation = heading;
#endif //__BANK
}

void cutsManager::SetScenePitch(float pitch)
{
	m_fScenePitch = pitch;
#if __BANK
	m_fBankScenePitch = pitch;
#endif //__BANK
}

void cutsManager::SetSceneRoll(float roll)
{
	m_fSceneRoll = roll;
#if __BANK
	m_fBankSceneRoll = roll;
#endif //__BANK
}

const cutfCutsceneFile2* cutsManager::GetCutfFile() const
{
#if HACK_GTA4
	return m_pCutSceneFile; 
#else
	return &m_cutsceneFile;
#endif
}

#if !__NO_OUTPUT
const char * cutsManager::GetStateName(ECutsceneState state)
{
	CompileTimeAssert(NELEM(c_cutsceneStateNames) == CUTSCENE_NUM_STATES); 
	return c_cutsceneStateNames[state];
}
#endif // !__NO_OUTPUT

#if __BANK

void cutsManager::CreateVCRWidget(bkBank * pBank) 
{
	crVCRWidget *pVcrWidget = (crVCRWidget *)pBank->AddWidget( *(rage_new crVCRWidget( "Playback", NULL, crVCRWidget::VCR )) );
	crVCRWidget::ActionFuncType ffCB;
	ffCB.Reset<cutsManager,&cutsManager::BankFastForwardCallback>(this);
	pVcrWidget->SetFastForwardOrGoToEndCB( ffCB );

	crVCRWidget::ActionFuncType stepForwardCB;
	stepForwardCB.Reset<cutsManager,&cutsManager::BankStepForwardCallback>(this);
	pVcrWidget->SetGoToNextOrStepForwardCB( stepForwardCB );

	crVCRWidget::ActionFuncType stepBackwardCB;
	stepBackwardCB.Reset<cutsManager,&cutsManager::BankStepBackwardCallback>(this);
	pVcrWidget->SetGoToPreviousOrStepBackwardCB( stepBackwardCB );

	crVCRWidget::ActionFuncType pauseCB;
	pauseCB.Reset<cutsManager,&cutsManager::BankPauseCallback>(this);
	pVcrWidget->SetPauseCB( pauseCB );

	crVCRWidget::ActionFuncType playBackwardsCB;
	playBackwardsCB.Reset<cutsManager,&cutsManager::BankPlayBackwardsCallback>(this);
	pVcrWidget->SetPlayBackwardsCB( playBackwardsCB );

	crVCRWidget::ActionFuncType playForwardsCB;
	playForwardsCB.Reset<cutsManager,&cutsManager::BankPlayForwardsCallback>(this);
	pVcrWidget->SetPlayForwardsCB( playForwardsCB );

	crVCRWidget::ActionFuncType rewindCB;
	rewindCB.Reset<cutsManager,&cutsManager::BankRewindCallback>(this);
	pVcrWidget->SetRewindOrGoToStartCB( rewindCB );
}

void cutsManager::AddWidgets( bkBank &bank )
{
    if ( m_pBank != NULL )
    {
        RemoveWidgets();
    }

    m_pBank = &bank;

#if SETUP_CUTSCENE_WIDGET_FOR_CONTENT_CONTROLLED_BUILD 
	if ( m_pCurrentSceneGroup == NULL )
	{
		m_pCurrentSceneGroup = m_pBank->PushGroup( "Current Scene" );
		s_bIsBankOpen = true;
		{
#if!HACK_GTA4
			m_pBank->AddText( "Scene Name", m_cCurrentSceneName, CUTSCENE_OBJNAMELEN, true );
#endif          
			m_pBank->AddSeparator();
#if HACK_GTA4            
			m_pBank->PushGroup( "Play Back" );
			{
				CreateVCRWidget(m_pBank);          
				m_pPlayBackSlider = m_pBank->AddSlider( "Current Frame", &m_iCurrentFrameWithFrameRanges, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankFrameScrubbingCallback),this) );
				m_pPhasePlayBackSlider = m_pBank->AddSlider( "Current phase", &m_fCurrentPhase, 0.0f, 1.0f, 0.00001f, 
					datCallback(MFA(cutsManager::BankPhaseScrubbingCallback),this) );
				m_pBank->AddSeparator();
				m_pBank->AddToggle( "Loop (disables streaming!)", &m_bLoop );
				m_pLoopStartSlider = m_pBank->AddSlider( "Loop Start Frame", &m_iLoopStartFrame, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankLoopStartTimeChangedCallback),this) );
				m_pLoopEndSlider = m_pBank->AddSlider( "Loop End Frame", &m_iLoopEndFrame, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankLoopEndTimeChangedCallback),this) );
				m_pBank->AddButton( "Restart", datCallback(MFA(cutsManager::BankRestartCallback),this) );
				//m_pBank->AddToggle( "Pause At End", &m_bPauseAtEnd, NullCallback, "Pause the scene at the end instead of unloading it." );
				m_pBank->AddSeparator();
			}
			m_pBank->PopGroup();
#endif
			m_pBank->PushGroup( "Scene Properties" );
			{
#if HACK_GTA4			
				m_pBank->AddText( "Scene Name", m_cCurrentSceneName, CUTSCENE_OBJNAMELEN, true );
#endif

				m_pBank->AddToggle("Enable scene origin editing", &m_bEditSceneOrigin );

				m_pBank->AddVector( "Scene Offset", &m_vBankSceneOffset, -100000.0f, 100000.0f, 1.0f, 
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
				m_pBank->AddAngle( "Scene Pitch", &m_fBankScenePitch, bkAngleType::DEGREES,
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
				m_pBank->AddAngle( "Scene Roll", &m_fBankSceneRoll, bkAngleType::DEGREES,
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
				m_pBank->AddAngle( "Scene Heading", &m_fBankSceneRotation, bkAngleType::DEGREES,
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );

			
#if !HACK_GTA4             
				m_pBank->AddText( "Extra Room Name", m_cExtraRoomName, sizeof(m_cExtraRoomName), 
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
				m_pBank->AddVector( "Extra Room Position", &m_vExtraRoomPos, -100000.0f, 100000.0f, 1.0f, 
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
#endif
				m_pBank->AddButton( "Save Scene Orientation", datCallback(MFA(cutsManager::BankSaveSceneOrientationCallback),this) );
				m_pBank->AddSeparator();

			/*	m_pBank->AddSlider( "Start Frame", &m_iStartFrame, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankStartTimeChangedCallback),this) );
				m_pBank->AddSlider( "End Frame", &m_iEndFrame, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankEndTimeChangedCallback),this) );
				m_pBank->AddButton( "Save Playback Range", datCallback(MFA(cutsManager::BankSavePlaybackRangeCallback),this) );*/
			}
			m_pBank->PopGroup();

			m_pBank->PushGroup( "Face Debug" );
			{
				CreateVCRWidget(m_pBank);   
				m_pFacePlayBackSlider = m_pBank->AddSlider( "Current Frame", &m_iCurrentFrameWithFrameRanges, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankFrameScrubbingCallback),this) );
				m_pBank->AddSeparator();
				m_pBank->AddSlider( "Face Zoom Distance", &m_fFaceZoomDistance, 0.2f, 1.0f, 0.1f,
					datCallback(MFA(cutsManager::BankFaceZoomDistanceChangedCallback),this) );

				m_pBank->AddSlider( "Near Draw Distance", &m_fFaceZoomNearDrawDistance, 0.0f, 1000.0f, 0.1f,
					datCallback(MFA(cutsManager::BankFaceZoomNearDrawDistanceChangedCallback),this) );

				m_pBank->AddSlider( "Far Draw Distance", &m_fFaceZoomFarDrawDistance, 0.0f, 1000.0f, 0.1f,
					datCallback(MFA(cutsManager::BankFaceZoomFarDrawDistanceChangedCallback),this) );

				const char *faceNameList[] = { "(none)" };
				m_pEditFaceCombo = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Face Selector", &m_iSelectedPedModelIndex, 1, faceNameList ) );
			}
			m_pBank->PopGroup();
		}
		m_pBank->PopGroup();
	}


#else

    if ( m_pCurrentSceneGroup == NULL )
    {
		 m_pCurrentSceneGroup = m_pBank->PushGroup( "Current Scene" );
		 s_bIsBankOpen = true;
		 {
#if!HACK_GTA4
			m_pBank->AddText( "Scene Name", m_cCurrentSceneName, CUTSCENE_OBJNAMELEN, true );
#endif          
			m_pBank->AddSeparator();
#if HACK_GTA4            
			m_pBank->PushGroup( "Play Back" );
			{
				CreateVCRWidget(m_pBank);          
				m_pPlayBackSlider = m_pBank->AddSlider( "Current Frame", &m_iCurrentFrameWithFrameRanges, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankFrameScrubbingCallback),this) );
				m_pPhasePlayBackSlider = m_pBank->AddSlider( "Current phase", &m_fCurrentPhase, 0.0f, 1.0f, 0.00001f, 
					datCallback(MFA(cutsManager::BankPhaseScrubbingCallback),this) );
				m_pBank->AddSeparator();
				m_pBank->AddToggle( "Loop (disables streaming!)", &m_bLoop );
				m_pLoopStartSlider = m_pBank->AddSlider( "Loop Start Frame", &m_iLoopStartFrame, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankLoopStartTimeChangedCallback),this) );
				m_pLoopEndSlider = m_pBank->AddSlider( "Loop End Frame", &m_iLoopEndFrame, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankLoopEndTimeChangedCallback),this) );
				m_pBank->AddButton( "Restart", datCallback(MFA(cutsManager::BankRestartCallback),this) );
				//m_pBank->AddToggle( "Pause At End", &m_bPauseAtEnd, NullCallback, "Pause the scene at the end instead of unloading it." );
				m_pBank->AddSeparator();

				m_bIsolateSelectedPed = false;
				const char *faceNameList[] = { "(none)" };
				m_pIsolatePedCombo = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Isolate ped", &m_iSelectedIsolatePedIndex, 1, faceNameList ) );
				m_pBank->AddToggle("Enable isolate", &m_bIsolateSelectedPed);
				m_pBank->AddSeparator();
			}
			m_pBank->PopGroup();
#endif
			m_pBank->PushGroup( "Scene Properties" );
			{
#if HACK_GTA4			
				m_pBank->AddText( "Scene Name", m_cCurrentSceneName, CUTSCENE_OBJNAMELEN, true );
#endif
				m_pBank->AddToggle("Enable scene origin editing", &m_bEditSceneOrigin );
				m_pBank->AddVector( "Scene Offset", &m_vBankSceneOffset, -100000.0f, 100000.0f, 1.0f, 
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
				m_pBank->AddAngle( "Scene Pitch", &m_fBankScenePitch, bkAngleType::DEGREES,
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
				m_pBank->AddAngle( "Scene Roll", &m_fBankSceneRoll, bkAngleType::DEGREES,
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
				m_pBank->AddAngle( "Scene Heading", &m_fBankSceneRotation, bkAngleType::DEGREES,
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );

#if !HACK_GTA4             
				m_pBank->AddText( "Extra Room Name", m_cExtraRoomName, sizeof(m_cExtraRoomName), 
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
				m_pBank->AddVector( "Extra Room Position", &m_vExtraRoomPos, -100000.0f, 100000.0f, 1.0f, 
					datCallback(MFA(cutsManager::BankSceneOrientationChangedCallback),this) );
#endif
				m_pBank->AddButton( "Save Scene Orientation", datCallback(MFA(cutsManager::BankSaveSceneOrientationCallback),this) );
				m_pBank->AddSeparator();

			/*	m_pBank->AddSlider( "Start Frame", &m_iStartFrame, 0, 100000, 1, 
                datCallback(MFA(cutsManager::BankStartTimeChangedCallback),this) );
				m_pBank->AddSlider( "End Frame", &m_iEndFrame, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankEndTimeChangedCallback),this) );
				m_pBank->AddButton( "Save Playback Range", datCallback(MFA(cutsManager::BankSavePlaybackRangeCallback),this) );*/

			
       
#if !HACK_GTA4  
            m_pBank->PushGroup( "Looping" );
            {
                m_pBank->AddToggle( "Loop (disables streaming!)", &m_bLoop );
                m_pLoopStartSlider = m_pBank->AddSlider( "Loop Start Frame", &m_iLoopStartFrame, 0, 100000, 1, 
                    datCallback(MFA(cutsManager::BankLoopStartTimeChangedCallback),this) );
                m_pLoopEndSlider = m_pBank->AddSlider( "Loop End Frame", &m_iLoopEndFrame, 0, 100000, 1, 
                    datCallback(MFA(cutsManager::BankLoopEndTimeChangedCallback),this) );
            }   
            m_pBank->PopGroup();
#endif


#if !HACK_GTA4            
				CreateVCRWidget(m_pBank);         

				m_pBank->AddButton( "Restart", datCallback(MFA(cutsManager::BankRestartCallback),this) );
				//m_pBank->AddToggle( "Pause At End", &m_bPauseAtEnd, NullCallback, "Pause the scene at the end instead of unloading it." );
	#endif
				const char *timestepSources[] = { "Default", "Game", "Audio" };
				m_pBank->AddCombo( "Timestep Source", &m_iTimestepSource, 3, timestepSources, 0 );
			}
			m_pBank->PopGroup(); //scene properties

            m_pBank->PushGroup( "Debug Display" );
            {
                m_pBank->AddToggle( "Show Status", &m_bDisplayStatus );

                const char *statusUnits[] = { "Seconds", "Frames" };
                m_pBank->AddCombo( "Status Units", &m_iStatusUnits, 2, statusUnits, 
                    datCallback(MFA(cutsManager::BankStatusDisplayUnitsChangedCallback),this) );

                m_pBank->AddToggle( "Show Memory Usage", &m_bDisplayMemoryUsage );              
#if !HACK_GTA4
                m_pBank->AddToggle( "Show Debug Lines", &m_bDisplayDebugLines,
                    datCallback(MFA(cutsManager::BankDisplayDebugLinesChangedCallback),this) );                
#endif              
				m_pBank->PushGroup( "Debug Display" );
                {
                    m_pBank->AddToggle( "Blocking Bounds", &m_bDisplayBlockingBoundsLines,
                        datCallback(MFA(cutsManager::BankDisplayBlockingBoundsLinesChangedCallback),this) );
                   
					m_pBank->AddToggle( "Removal Bounds", &m_bDisplayRemovalBoundsLines,
                        datCallback(MFA(cutsManager::BankDisplayRemovalBoundsLinesChangedCallback),this) );
                   
					m_pBank->AddToggle( "Camera", &m_bDisplayCameraLines,
                        datCallback(MFA(cutsManager::BankDisplayCameraLinesChangedCallback),this) );                  
					
					m_pBank->AddToggle( "Lights", &m_bDisplayLightLines,
                        datCallback(MFA(cutsManager::BankDisplayLightLinesChangedCallback),this) );
					
					m_pBank->AddToggle( "Models Movers", &m_bDisplayModelLines,
                        datCallback(MFA(cutsManager::BankDisplayModelLinesChangedCallback),this) );
                    
					m_pBank->AddToggle( "Particle Effects", &m_bDisplayParticleEffectLines,
                        datCallback(MFA(cutsManager::BankDisplayParticleEffectLinesCallback),this) );
                    
					m_pBank->AddToggle( "Scene Origin", &m_bDisplaySceneOrigin ); 
						
					m_pBank->AddToggle( "Hidden Objects", &m_bDisplayHiddenLines,
						datCallback(MFA(cutsManager::BankDisplayHiddenObjectLinesCallback),this) );
                }
                m_pBank->PopGroup();
            }
            m_pBank->PopGroup();

            m_pBank->PushGroup( "Audio" );
            {
                m_pBank->AddToggle( "Mute Audio", &m_bMuteAudio, datCallback(MFA(cutsManager::BankMuteAudioCallback),this) );
                m_pBank->AddSlider( "Audio Offset (ms)", &m_iAudioOffset, -2000, 2000, 10 );
#if __PPU
                m_pBank->AddSlider( "Audio Resync Time (ms)", &m_iAudioResyncTime, 0, 5000, 10 );
#endif // __PPU
            }
            m_pBank->PopGroup();

            m_pBank->PushGroup( "Face Debug" );
            {
                CreateVCRWidget(m_pBank);   
				m_pFacePlayBackSlider = m_pBank->AddSlider( "Current Frame", &m_iCurrentFrameWithFrameRanges, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankFrameScrubbingCallback),this) );
				m_pBank->AddSeparator();
				m_pBank->AddSlider( "Face Zoom Distance", &m_fFaceZoomDistance, 0.2f, 1.0f, 0.1f,
                    datCallback(MFA(cutsManager::BankFaceZoomDistanceChangedCallback),this) );

                m_pBank->AddSlider( "Near Draw Distance", &m_fFaceZoomNearDrawDistance, 0.0f, 1000.0f, 0.1f,
                    datCallback(MFA(cutsManager::BankFaceZoomNearDrawDistanceChangedCallback),this) );

                m_pBank->AddSlider( "Far Draw Distance", &m_fFaceZoomFarDrawDistance, 0.0f, 1000.0f, 0.1f,
                    datCallback(MFA(cutsManager::BankFaceZoomFarDrawDistanceChangedCallback),this) );

                const char *faceNameList[] = { "(none)" };
                m_pEditFaceCombo = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Face Selector", &m_iSelectedPedModelIndex, 1, faceNameList ) );
            }
            m_pBank->PopGroup();

            m_pBank->PushGroup( "Light Editing" );
            {
				CreateVCRWidget(m_pBank);  
				m_pLightPlayBackSlider = m_pBank->AddSlider( "Current Frame", &m_iCurrentFrameWithFrameRanges, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankFrameScrubbingCallback),this) );
				m_pBank->AddSeparator();

				m_pBank->AddToggle("Enable Light Hotloading", (reinterpret_cast<u32*>(&CutfileNonParseData::m_FileTuningFlags)), CutfileNonParseData::CUTSCENE_LIGHT_TUNING_FILE);

				m_pLightHotloadingStatusText = m_pBank->AddText("Hot loading Status:",&m_LightHotLoadingStatus[0], 256, true);

				m_pEditLightStatusText = m_pBank->AddText("Light Edit Status:",&m_LightEditStatus[0], 128, true);

				const char *lightNameList[] = { "(none)" };
                m_pEditLightCombo = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Light Selector", &m_iSelectedLightIndex, 1, lightNameList ) );


				const char* activeLightNameList[] = { "(none)" };
				m_pActiveLightCombo = m_pBank->AddCombo( "Active Light Selector", &m_iActiveLightIndex, 1, activeLightNameList);

				m_pBank->PushGroup( "Light Creation" );
				{
					m_pBank->AddText("Light Name", &m_createdLightName[0], 256, false); 
				
					const char *lightTypeList[] = { "directional", "point", "spot"};
					m_pBank->AddCombo("Light Type", &m_createdLightTypeIndex, 3, lightTypeList); 
					m_pBank->AddButton("Create light", datCallback(MFA(cutsManager::CreateLightCB),this), "Create A light"); 
					m_pBank->AddButton("Delete light", datCallback(MFA(cutsManager::DeleteLightCB),this), "Delete A light"); 

					m_pBank->PushGroup( "Duplication" );
						m_pBank->AddText("Duplicated Name", &m_duplicatedLightName[0], 256, false); 
						m_pBank->AddButton("Duplicate light", datCallback(MFA(cutsManager::DuplicateLightCB),this), "Duplicate A light"); 
						m_pBank->AddButton("Duplicate light from camera", datCallback(MFA(cutsManager::DuplicateLightFromCameraCB),this), "Duplicate A light"); 
					m_pBank->PopGroup();

					m_pBank->PushGroup( "Renaming" );
					m_pBank->AddText("New Name", &m_LightNewName[0], 256, false); 
					m_pBank->AddButton("Rename light", datCallback(MFA(cutsManager::RenameLightCB),this), "Duplicate A light"); 
					m_pBank->PopGroup();
				}
				m_pBank->PopGroup(); 

				m_pBank->PushGroup("Authoring");
					m_pBank->AddButton("Snap Camera To Light", datCallback(MFA(cutsManager::SnapCameraToLight),this), "Snaps the light to the current camera");
					m_pBank->AddToggle("Track Light With Camera", &m_bUpdateLightWithCamera, datCallback(MFA(cutsManager::ActivateCameraTrackingCB), (datBase*)this));
					m_pBank->AddButton("Snap Light to Camera", datCallback(MFA(cutsManager::SnapLightToCamera),this), "Snaps the light to the current camera");
				m_pBank->PopGroup();


                m_pEditLightGroup = m_pBank->PushGroup( "Selected Light" );
                m_pBank->PopGroup();
            }
            m_pBank->PopGroup();

            m_pBank->PushGroup( "Camera Editing" );
            {
				CreateVCRWidget(m_pBank);  
				m_pCameraPlayBackSlider = m_pBank->AddSlider( "Current Frame", &m_iCurrentFrameWithFrameRanges, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankFrameScrubbingCallback),this) );
				m_pBank->AddSeparator();
				m_pBank->AddToggle( "Display Camera Debug", &m_bDisplayCameraLines,
					datCallback(MFA(cutsManager::BankDisplayCameraLinesChangedCallback),this) );      
				m_pBank->PushGroup( "Draw Distance Editing" );
				{		
					m_pBank->AddButton( "Save Draw Distances", datCallback(MFA(cutsManager::BankSaveDrawDistancesCallback),this), 
						"Saves all draw distance edits to the source cut file, plus the Depth of Field setting." );
				
                const char *drawDistNameList[] = { "(none)" };
                m_pEditDrawDistanceCombo = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Draw Distance Selector", 
                    &m_iSelectedDrawDistanceIndex, 1, drawDistNameList ) );
					m_pEditDrawDistanceGroup = m_pBank->PushGroup( "Selected Draw Distance" );
					m_pBank->PopGroup();
				}
				m_pBank->PopGroup();

				m_pBank->PushGroup( "Draw Distance Resets", false );
				{
					m_pBank->AddButton( "Reset All Draw Distances", datCallback(MFA(cutsManager::BankResetAllDrawDistancesCallback),this), 
						"Resets all overrides to their original values." );
					m_pBank->AddButton( "Set All Draw Distances to Defaults", datCallback(MFA(cutsManager::BankSetToDefaultAllDrawDistancesCallback),this), 
						"Resets all overrides to their game-specific default values." ); 
						m_pBank->AddButton( "Set All Draw Distances to use Time Cycle Values", datCallback(MFA(cutsManager::BankSetToUseTimecycleAllDrawDistancesCallback),this), 
						"Resets all overrides to -1 so the game will use the time cycle values" );
				}
				m_pBank->PopGroup();

				/*		m_pBank->PushGroup( "Depth of Field" );
				{	
				m_pBank->AddToggle( "Use Depth Of Field", &m_bDepthOfFieldEnabled, 
				datCallback(MFA(cutsManager::BankToggleDepthOfFieldCallback),this), "Enables or disables Depth Of Field for this cutscene." );
				m_pBank->AddButton( "Save Depth of Field Setting", datCallback(MFA(cutsManager::BankSaveDepthOfFieldCallback),this) );
				}
				m_pBank->PopGroup();*/

            }
            m_pBank->PopGroup();

			m_pBank->PushGroup( "Particle Effects Editing" );
			{
				CreateVCRWidget(m_pBank); 
				m_pBank->PopGroup(); 
			}

            m_pBank->PushGroup( "Hidden Object Editing" );
            {
				CreateVCRWidget(m_pBank);
				m_pHiddenObjPlayBackSlider = m_pBank->AddSlider( "Current Frame", &m_iCurrentFrameWithFrameRanges, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankFrameScrubbingCallback),this) );
				m_pBank->AddSeparator();
				m_pBank->AddToggle("Activate Map Object Editing", &m_ActivateMapObjectEditing);
				m_pBank->AddToggle("Show Hidden Objects", &m_bRenderHiddenObjects);

				m_pBank->AddButton( "Add Hidden Object", datCallback(MFA(cutsManager::BankAddHiddenObjectCallback),this) );

                m_pBank->AddButton( "Remove Hidden Object", datCallback(MFA(cutsManager::BankRemoveHiddenObjectCallback),this) );

                //m_pBank->AddButton( "Save Hidden Objects", datCallback(MFA(cutsManager::BankSaveHiddenObjectsCallback),this), 
                    //"Saves all hidden object edits to the source cut file." );

                const char *hiddenObjectNameList[] = { "(none)" };
                m_pEditHiddenObjectCombo = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Hidden Object Selector", 
                    &m_iSelectedHiddenObjectIndex, 1, hiddenObjectNameList ) );

                m_pEditHiddenObjectGroup = m_pBank->PushGroup( "Selected Hidden Object" );
                m_pBank->PopGroup();
            }
            m_pBank->PopGroup();

            m_pBank->PushGroup( "Fixup Object Editing" );
            {
                CreateVCRWidget(m_pBank);
				m_pFixupObjPlayBackSlider = m_pBank->AddSlider( "Current Frame", &m_iCurrentFrameWithFrameRanges, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankFrameScrubbingCallback),this) );
				m_pBank->AddSeparator();
				m_pBank->AddToggle("Activate Map Object Editing", &m_ActivateMapObjectEditing);
				
				m_pBank->AddToggle("Fix up map object", &m_FixupMapObject);
				
				m_pBank->AddButton( "Add Fixup Object", datCallback(MFA(cutsManager::BankAddFixupObjectCallback),this) );

                m_pBank->AddButton( "Remove Fixup Object", datCallback(MFA(cutsManager::BankRemoveFixupObjectCallback),this) );

                const char *fixupObjectNameList[] = { "(none)" };
                m_pEditFixupObjectCombo = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Fixup Object Selector", 
                    &m_iSelectedFixupObjectIndex, 1, fixupObjectNameList ) );

                m_pEditFixupObjectGroup = m_pBank->PushGroup( "Selected Fixup Object" );
                m_pBank->PopGroup();
            }
			m_pBank->PopGroup();
			
			m_pBank->PushGroup( "Blocking bound Editing" );
			{
				CreateVCRWidget(m_pBank);
				m_pBlockingBoundPlayBackSlider = m_pBank->AddSlider( "Current Frame", &m_iCurrentFrameWithFrameRanges, 0, 100000, 1, 
					datCallback(MFA(cutsManager::BankFrameScrubbingCallback),this) );
				m_pBank->AddSeparator();
				m_pBank->AddToggle("Activate Bounding Editing", &m_bActivateBlockingObjectEditing);
				m_pBank->AddToggle("Show Blocking Bounds", &m_bRenderBlockingBoundObjects);
				m_pBank->AddVector("Position", &m_vBlockingBoundPos, -10000.0f, 10000.0f, 1.0f ); 
				m_pBank->AddSlider("Width", &m_fBoundingBoxWidth, 0.0f, 100.0f, 0.5f); 
				m_pBank->AddSlider("Length", &m_fBoundingBoxLength, 0.0f, 100.0f, 0.5f); 
				m_pBank->AddSlider("Height", &m_fBoundingBoxHeight, 0.0f, 100.0f, 0.5f); 
				m_pBank->AddSlider("Rotation", &m_vBlockingBoundRot.z, 0.0f, 360.0f, 0.5f); 

				m_pBank->AddButton( "Add Blocking bound Object", datCallback(MFA(cutsManager::BankAddBlockingBoundObjectCallback),this) );
				m_pBank->AddButton( "Remove Blocking bound Object", datCallback(MFA(cutsManager::BankRemoveBlockingBoundObjectCallback),this) );
				m_pBank->AddButton( "Rename all blocking bounds", datCallback(MFA(cutsManager::BankRenameBlockingBoundObjectCallback),this) );

				const char *BlockingObjectNameList[] = { "(none)" };
				m_pEditBlockingBoundObjectCombo = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Blocking bound Object Selector", 
					&m_iSelectedBlockingBoundObjectIndex, 1, BlockingObjectNameList ) );

				m_pEditBlockingBoundObjectGroup = m_pBank->PushGroup( "Selected Blocking Bound Object" );
				m_pBank->PopGroup();
			}
			m_pBank->PopGroup();
		}
		m_pBank->PopGroup();
    }
#endif
}

void cutsManager::RemoveWidgets()
{
    if ( m_pCurrentSceneGroup != NULL )
    {
        m_pBank->Remove( *m_pCurrentSceneGroup );
        m_pCurrentSceneGroup = NULL;
		s_bIsBankOpen = false;
        m_pBank = NULL;
    }

	m_pPlayBackSlider = NULL;
	m_pPhasePlayBackSlider = NULL;
	m_pLoopStartSlider = NULL;
	m_pLoopEndSlider = NULL;
	m_pFacePlayBackSlider = NULL;
	m_pEditFaceCombo = NULL;
	m_pLightPlayBackSlider = NULL;
	m_pLightHotloadingStatusText = NULL;
	m_pEditLightStatusText = NULL;
	m_pEditLightCombo = NULL;
	m_pActiveLightCombo = NULL;
	m_pEditLightGroup = NULL;
	m_pCameraPlayBackSlider = NULL;
	m_pEditDrawDistanceCombo = NULL;
	m_pEditDrawDistanceGroup = NULL;
	m_pHiddenObjPlayBackSlider = NULL;
	m_pEditHiddenObjectCombo = NULL;
	m_pEditHiddenObjectGroup = NULL;
	m_pFixupObjPlayBackSlider = NULL;
	m_pEditFixupObjectCombo = NULL;
	m_pEditFixupObjectGroup = NULL;
	m_pBlockingBoundPlayBackSlider = NULL;
	m_pEditBlockingBoundObjectCombo = NULL;
	m_pEditBlockingBoundObjectGroup = NULL;
}

bool cutsManager::IsBankOpen()
{
	return s_bIsBankOpen;
}

SEditCutfLightInfo* cutsManager::GetEditLightInfo( s32 iObjectId ) const
{
    for ( int i = 0; i < m_editLightList.GetCount(); ++i )
    {
        if ( m_editLightList[i]->pObject->GetObjectId() == iObjectId )
        {
            return m_editLightList[i];
        }
    }

    return NULL;
}

SEditCutfLightInfo* cutsManager::GetSelectedLightInfo() const
{
	if(m_editLightList.GetCount() > 0 && m_iSelectedLightIndex > 0 && m_iSelectedLightIndex <= m_editLightList.GetCount())
	{
		return m_editLightList[m_iSelectedLightIndex - 1];
	}
	return NULL; 
}

SEditCutfLightInfo* cutsManager::GetActiveLightInfo() const
{
	if(m_activeLightList.GetCount() > 0 && m_iActiveLightIndex > 0 && m_iActiveLightIndex <= m_activeLightList.GetCount())
	{
		return m_activeLightList[m_iActiveLightIndex - 1];
	}
	return NULL; 
}


bool cutsManager::AddHiddenObject( SEditCutfObjectLocationInfo *pObjectLocationInfo )
{
    for ( int i = 0; i < m_editHiddenObjectList.GetCount(); ++i )
    {
        if ( (strcmp( m_editHiddenObjectList[i]->cName, pObjectLocationInfo->cName ) == 0)
            && (m_editHiddenObjectList[i]->vPosition == pObjectLocationInfo->vPosition) 
            && (m_editHiddenObjectList[i]->fRadius == pObjectLocationInfo->fRadius) )
        {
            cutsWarningf( "A Hidden Object with the exact same attributes has already been added (%s within %f units of (%f,%f,%f)).",
                pObjectLocationInfo->cName, pObjectLocationInfo->fRadius, pObjectLocationInfo->vPosition.GetX(),
                pObjectLocationInfo->vPosition.GetY(), pObjectLocationInfo->vPosition.GetZ() );
            return false;
        }
    }

    // add to the list
    m_editHiddenObjectList.Grow() = pObjectLocationInfo;

    // select it
    m_iSelectedHiddenObjectIndex = m_editHiddenObjectList.GetCount();

    // update the combo
    if ( m_pEditHiddenObjectCombo != NULL )
    {
        m_pEditHiddenObjectCombo->UpdateCombo( "Hidden Object Selector", &m_iSelectedHiddenObjectIndex, 
            m_editHiddenObjectList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankHiddenObjectSelectedCallback),this) );

        m_pEditHiddenObjectCombo->SetString( 0, "(none)" );
        for ( int i = 0; i < m_editHiddenObjectList.GetCount(); ++i )
        {
            m_pEditHiddenObjectCombo->SetString( i + 1, m_editHiddenObjectList[i]->cName );
        }

        BankHiddenObjectSelectedCallback();
		
    }
	return true; 
}

void cutsManager::RemoveHiddenObject( SEditCutfObjectLocationInfo *pObjectLocationInfo )
{
    int iIndex = m_editHiddenObjectList.Find( pObjectLocationInfo );
    if ( iIndex == -1 )
    {
        return;
    }

    // delete the SEditCutfObjectLocationInfo
    delete m_editHiddenObjectList[iIndex];
    m_editHiddenObjectList.Delete( iIndex );

    // adjust the index so we're not out of range
    if ( m_iSelectedHiddenObjectIndex > m_editHiddenObjectList.GetCount() )
    {
        m_iSelectedHiddenObjectIndex = m_editHiddenObjectList.GetCount();
    }

    // update the combo
    if ( m_pEditHiddenObjectCombo != NULL )
    {
        m_pEditHiddenObjectCombo->UpdateCombo( "Hidden Object Selector", &m_iSelectedHiddenObjectIndex, 
            m_editHiddenObjectList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankHiddenObjectSelectedCallback),this) );

        m_pEditHiddenObjectCombo->SetString( 0, "(none)" );
        for ( int i = 0; i < m_editHiddenObjectList.GetCount(); ++i )
        {
            m_pEditHiddenObjectCombo->SetString( i + 1, m_editHiddenObjectList[i]->cName );
        }

        BankHiddenObjectSelectedCallback();
    }
}

bool cutsManager::AddFixupObject( SEditCutfObjectLocationInfo *pObjectLocationInfo )
{
    for ( int i = 0; i < m_editFixupObjectList.GetCount(); ++i )
    {
        if ( (strcmp( m_editFixupObjectList[i]->cName, pObjectLocationInfo->cName ) == 0)
            && (m_editFixupObjectList[i]->vPosition == pObjectLocationInfo->vPosition) 
            && (m_editFixupObjectList[i]->fRadius == pObjectLocationInfo->fRadius) )
        {
            cutsWarningf( "A Fixup Object with the exact same attributes has already been added (%s within %f units of (%f,%f,%f)).",
                pObjectLocationInfo->cName, pObjectLocationInfo->fRadius, pObjectLocationInfo->vPosition.GetX(),
                pObjectLocationInfo->vPosition.GetY(), pObjectLocationInfo->vPosition.GetZ() );
            return false;
        }
    }

    // add to the list
    m_editFixupObjectList.Grow() = pObjectLocationInfo;

    // select it
    m_iSelectedFixupObjectIndex = m_editFixupObjectList.GetCount();

    // update the combo
    if ( m_pEditFixupObjectCombo != NULL )
    {
        m_pEditFixupObjectCombo->UpdateCombo( "Fixup Object Selector", &m_iSelectedFixupObjectIndex, 
            m_editFixupObjectList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankFixupObjectSelectedCallback),this) );

        m_pEditFixupObjectCombo->SetString( 0, "(none)" );
        for ( int i = 0; i < m_editFixupObjectList.GetCount(); ++i )
        {
            m_pEditFixupObjectCombo->SetString( i + 1, m_editFixupObjectList[i]->cName );
        }

        BankFixupObjectSelectedCallback();
    }
	return true; 
}

void cutsManager::RemoveFixupObject( SEditCutfObjectLocationInfo *pObjectLocationInfo )
{
    int iIndex = m_editFixupObjectList.Find( pObjectLocationInfo );
    if ( iIndex == -1 )
    {
        return;
    }

    // delete the SEditCutfObjectLocationInfo
    delete m_editFixupObjectList[iIndex];
    m_editFixupObjectList.Delete( iIndex );

    // adjust the index so we're not out of range
    if ( m_iSelectedFixupObjectIndex > m_editFixupObjectList.GetCount() )
    {
        m_iSelectedFixupObjectIndex = m_editFixupObjectList.GetCount();
    }

    // update the combo
    if ( m_pEditFixupObjectCombo != NULL )
    {
        m_pEditFixupObjectCombo->UpdateCombo( "Fixup Object Selector", &m_iSelectedFixupObjectIndex, 
            m_editFixupObjectList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankFixupObjectSelectedCallback),this) );

        m_pEditFixupObjectCombo->SetString( 0, "(none)" );
        for ( int i = 0; i < m_editFixupObjectList.GetCount(); ++i )
        {
            m_pEditFixupObjectCombo->SetString( i + 1, m_editFixupObjectList[i]->cName );
        }

        BankFixupObjectSelectedCallback();
    }
}

bool cutsManager::AddBlockingBoundObject( sEditCutfBlockingBoundsInfo *pObjectLocationInfo )
{
	for ( int i = 0; i < m_editBlockingBoundObjectList.GetCount(); ++i )
	{
		if ( (m_editBlockingBoundObjectList[i]->vCorners[0] == pObjectLocationInfo->vCorners[0]) 
			&& (m_editBlockingBoundObjectList[i]->vCorners[1] == pObjectLocationInfo->vCorners[1]) 
			&& (m_editBlockingBoundObjectList[i]->vCorners[2] == pObjectLocationInfo->vCorners[2])
			&& (m_editBlockingBoundObjectList[i]->vCorners[3] == pObjectLocationInfo->vCorners[3])	
			&& (m_editBlockingBoundObjectList[i]->fHeight == pObjectLocationInfo->fHeight)) 
		{
			cutsWarningf( "A Blocking Bound Object with the exact same attributes has already been added"); 
			return false;
		}
	}

	// add to the list
	m_editBlockingBoundObjectList.Grow() = pObjectLocationInfo;

	// select it
	m_iSelectedBlockingBoundObjectIndex = m_editBlockingBoundObjectList.GetCount();

	// update the combo
	if ( m_pEditBlockingBoundObjectCombo != NULL )
	{
		m_pEditBlockingBoundObjectCombo->UpdateCombo( "BlockingBound Object Selector", &m_iSelectedBlockingBoundObjectIndex, 
			m_editBlockingBoundObjectList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankBlockingBoundObjectSelectedCallback),this) );

		m_pEditBlockingBoundObjectCombo->SetString( 0, "(none)" );
		for ( int i = 0; i < m_editBlockingBoundObjectList.GetCount(); ++i )
		{
			m_pEditBlockingBoundObjectCombo->SetString( i + 1, m_editBlockingBoundObjectList[i]->cName );
		}

		BankBlockingBoundObjectSelectedCallback();
	}
	return true; 
}

void cutsManager::RemoveBlockingBoundObject( sEditCutfBlockingBoundsInfo *pObjectLocationInfo )
{
	int iIndex = m_editBlockingBoundObjectList.Find( pObjectLocationInfo );
	if ( iIndex == -1 )
	{
		return;
	}

	// delete the SEditCutfObjectLocationInfo
	delete m_editBlockingBoundObjectList[iIndex];
	m_editBlockingBoundObjectList.Delete( iIndex );

	// adjust the index so we're not out of range
	if ( m_iSelectedBlockingBoundObjectIndex > m_editBlockingBoundObjectList.GetCount() )
	{
		m_iSelectedBlockingBoundObjectIndex = m_editBlockingBoundObjectList.GetCount();
	}

	// update the combo
	if ( m_pEditBlockingBoundObjectCombo != NULL )
	{
		m_pEditBlockingBoundObjectCombo->UpdateCombo( "BlockingBound Object Selector", &m_iSelectedBlockingBoundObjectIndex, 
			m_editBlockingBoundObjectList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankBlockingBoundObjectSelectedCallback),this) );

		m_pEditBlockingBoundObjectCombo->SetString( 0, "(none)" );
		for ( int i = 0; i < m_editBlockingBoundObjectList.GetCount(); ++i )
		{
			m_pEditBlockingBoundObjectCombo->SetString( i + 1, m_editBlockingBoundObjectList[i]->cName );
		}

		BankBlockingBoundObjectSelectedCallback();
	}
}

const char * cutsManager::GetScreenFadeOverrideName(EScreenFadeOverride screenFadeoverride)
{
	return c_cutsceneScreenFadeOverrideDisplayNames[screenFadeoverride];
}


void cutsManager::AddSceneWidgets()
{
	// update the face selector
	PopulatePedModelFaceList();

#if !SETUP_CUTSCENE_WIDGET_FOR_CONTENT_CONTROLLED_BUILD 
    // update the draw distance selector
    PopulateDrawDistanceList();

    if ( m_pCurrentSceneGroup == NULL )
    {
        return;
    }    

    // update the light selector
    PopulateLightList();

    // update the hidden object selector
    PopulateHiddenObjectList();

    // update the fixup object selector
    PopulateFixupObjectList();
	
	// update the blocking object selector
	PopulateBlockingBoundsList(); 
#endif
}

int cutsManager::GetLightIndex(const char* lightName)
{
	if(!cutsVerifyf(GetCutfFile(), "PopulateLightList: No cutf file object loaded"))
	{
		return -1;
	}

	for(int index = 0; index < m_editLightList.GetCount(); ++index)
	{
		atString displayName = m_editLightList[index]->pObject->GetDisplayName();
		if( stricmp(displayName.c_str(), lightName) == 0 )
		{
			return index;
		}
	}

	return -1;
}

void cutsManager::PopulateLightList()
{
	if(!GetCutfFile())
	{
		return;
	}
	
    m_iSelectedLightIndex = 0;

    if ( m_pBank == NULL )
    {
        return;
    }

    RAGE_TRACK( cutsManager_PopulateLightList );

    // find the light objects
    atArray<cutfObject *> lightObjects;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_LIGHT_OBJECT_TYPE, lightObjects );
	GetCutfFile()->FindObjectsOfType( CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE, lightObjects );

    if ( lightObjects.GetCount() == 0 )
    {
        if ( m_pEditLightGroup != NULL )
        {
            while ( m_pEditLightGroup->GetChild() )
            {
                m_pBank->Remove( *(m_pEditLightGroup->GetChild()) );
            }
        }

        if ( m_pEditLightCombo != NULL )
        {
            m_iSelectedLightIndex = 0;

            m_pEditLightCombo->UpdateCombo( "Light Selector", &m_iSelectedLightIndex, 1, NULL,
                datCallback(MFA(cutsManager::BankLightSelectedCallback),this) );

            m_pEditLightCombo->SetString( 0, "(none)" );
        }

        return;
    }

    // only create them once
    if ( m_editLightList.GetCount() > 0 )
    {
        return;
    }    

    // For each light, edit/add a SEditCutfLightInfo object for it
    for ( int i = 0; i < lightObjects.GetCount(); ++i )
    {
		AddLightEditInfo(lightObjects[i]); 
    }

    cutsDisplayf( "Found %d Lights.", m_editLightList.GetCount() );

    // update the combo
	UpdateLightCombo(); 
}

void cutsManager::AddLightToActiveList(SEditCutfLightInfo* pLightinfo)
{
	if(m_pActiveLightCombo)
	{
		bool HaveNewLightInfo = false; 
		
		for ( int i = 0; i < m_editLightList.GetCount(); ++i )
		{
			if(pLightinfo == m_editLightList[i])
			{
				m_activeLightList.Grow() = m_editLightList[i];
				HaveNewLightInfo = true; 
			}
		}

		if(HaveNewLightInfo)
		{
			UpdateActiveLightListCombo(); 
			m_iActiveLightIndex = m_activeLightList.GetCount(); 
		}
		
	}
}

void cutsManager::UpdateActiveLightListCombo()
{
	if(m_pActiveLightCombo)
	{
		m_iActiveLightIndex = Clamp(m_iActiveLightIndex, 0, m_activeLightList.GetCount()); 
		
		m_pActiveLightCombo->UpdateCombo( "Active Light Selector", &m_iActiveLightIndex, m_activeLightList.GetCount() + 1, NULL,
			datCallback(MFA(cutsManager::BankActiveLightSelectedCallBack),this) );

		m_pEditLightCombo->SetString( 0, "(none)" );
		for ( int i = 0; i < m_activeLightList.GetCount(); ++i )
		{
			m_pActiveLightCombo->SetString( i + 1, m_activeLightList[i]->pObject->GetDisplayName().c_str());
		}
	}
}

void cutsManager::UpdateLightCombo()
{
	if ( m_pEditLightCombo != NULL )
	{
		m_pEditLightCombo->UpdateCombo( "Light Selector", &m_iSelectedLightIndex, m_editLightList.GetCount() + 1, NULL,
			datCallback(MFA(cutsManager::BankLightSelectedCallback),this) );

		m_pEditLightCombo->SetString( 0, "(none)" );
		for ( int i = 0; i < m_editLightList.GetCount(); ++i )
		{
			m_pEditLightCombo->SetString( i + 1, m_editLightList[i]->pObject->GetDisplayName().c_str() );
		}
	}
}

void cutsManager::AddLightEditInfo(cutfObject* pObject)
{
	SEditCutfLightInfo *pEditLightInfo = rage_new SEditCutfLightInfo;
	m_editLightList.Grow() = pEditLightInfo;
	m_iSelectedLightIndex = m_editLightList.GetCount();

	pEditLightInfo->pObject = pObject;

	InialiseLightEditInfo(pEditLightInfo); 
}

SEditCutfLightInfo* cutsManager::DuplicateLightEditInfo(cutfObject* pObject, SEditCutfLightInfo *pOrginalLightInfo)
{
	SEditCutfLightInfo *pNewEditLightInfo = rage_new SEditCutfLightInfo;
	*pNewEditLightInfo = *pOrginalLightInfo;
	pNewEditLightInfo->pObject = pObject; 
	pNewEditLightInfo->WasCreatedInGame = true; 
	pNewEditLightInfo->HasBeenRenamed = false; 

	m_editLightList.Grow() = pNewEditLightInfo;
	return pNewEditLightInfo; 
}


void cutsManager::PopulateLightEditInfo()
{
	for(int i=0; i < m_editLightList.GetCount(); i++)
	{
		SEditCutfLightInfo *pEditLightInfo = m_editLightList[i]; 

		InialiseLightEditInfo(pEditLightInfo); 
	}
}

void cutsManager::InialiseLightEditInfo(SEditCutfLightInfo *pEditLightInfo)
{
	if( pEditLightInfo && pEditLightInfo->pObject && pEditLightInfo->pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
	{
		const cutfLightObject* pLight =  static_cast<const cutfLightObject*>(pEditLightInfo->pObject);
		pEditLightInfo->fOriginalIntensity = pLight->GetLightIntensity(); 
		pEditLightInfo->fOriginalFallOff = pLight->GetLightFallOff(); 
		pEditLightInfo->fOriginalConeAngle = pLight->GetLightConeAngle(); 
		pEditLightInfo->vOriginalColor = pLight->GetLightColour(); 
		pEditLightInfo->vOriginalPosition = pLight->GetLightPosition(); 
		pEditLightInfo->vOriginalDirection = pLight->GetLightDirection(); 
		pEditLightInfo->OriginalLightFlags = pLight->GetLightFlags(); 
		pEditLightInfo->OriginalTimeFlags = pLight->GetLightHourFlags(); 
		pEditLightInfo->OriginalVolumeIntensity = pLight->GetVolumeIntensity(); 
		pEditLightInfo->OriginalVolumeSizeScale = pLight->GetVolumeSizeScale(); 
		pEditLightInfo->OriginalOuterColourAndIntensity = pLight->GetVolumeOuterColourAndIntensity(); 
		pEditLightInfo->OriginalCoronaIntensity = pLight->GetCoronaIntensity(); 
		pEditLightInfo->OriginalCoronaSize = pLight->GetCoronaSize(); 
		pEditLightInfo->OriginalCoronaZbias = pLight->GetCoronaZBias(); 
		pEditLightInfo->OriginalExpoFallOff = pLight->GetExponentialFallOff(); 
		pEditLightInfo->OriginalInnerConeAngle = pLight->GetInnerConeAngle(); 
		pEditLightInfo->OriginalShadowBlur = pLight->GetShadowBlur(); 
		pEditLightInfo->OriginalLightName = pLight->GetName(); 
	}
}

void cutsManager::PopulateDrawDistanceList()
{
    RAGE_TRACK( cutsManager_PopulateDrawDistanceList );
	if(!GetCutfFile() )
	{
		return;
	}
	
    m_iSelectedDrawDistanceIndex = 0;

    // get the draw distance events
    atArray<cutfObject *> cameraObjectList;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );
    
    if ( !IsRestarting() && (cameraObjectList.GetCount() == 0) )
    {
        if ( m_pEditDrawDistanceGroup != NULL )
        {
            while ( m_pEditDrawDistanceGroup->GetChild() )
            {
                m_pBank->Remove( *(m_pEditDrawDistanceGroup->GetChild()) );
            }
        }

        if ( m_pEditDrawDistanceCombo != NULL )
        {
            m_iSelectedDrawDistanceIndex = 0;

            m_pEditDrawDistanceCombo->UpdateCombo( "Draw Distance Selector", &m_iSelectedDrawDistanceIndex, 1, NULL, 
                datCallback(MFA(cutsManager::BankDrawDistanceSelectedCallback),this) );

            m_pEditDrawDistanceCombo->SetString( 0, "(none)" );
        }

        return;
    }

    // only create them once
    if ( m_editDrawDistanceList.GetCount() > 0 )
    {
        return;
    }    
	
	if(cameraObjectList.GetCount() == 0)
	{
		return; 
	}

    m_pCameraObject = dynamic_cast<const cutfCameraObject *>( cameraObjectList[0] );

    // Look for all of the Camera Cut Events
    atArray<cutfEvent *> cameraEventList;
    GetCutfFile()->FindEventsForObjectIdOnly( m_pCameraObject->GetObjectId(), GetCutfFile()->GetEventList(), cameraEventList );

    for ( int i = 0; i < cameraEventList.GetCount(); ++i )
    {
        if ( cameraEventList[i]->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT )
        {
            const cutfEventArgs *pEventArgs = cameraEventList[i]->GetEventArgs();
            if ( pEventArgs->GetType() == CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE )
            {
                const cutfCameraCutEventArgs *pCameraCutEventArgs = dynamic_cast<const cutfCameraCutEventArgs *>( pEventArgs );

                SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = rage_new SEditCutfDrawDistanceInfo();
                m_editDrawDistanceList.Grow() = pEditDrawDistanceInfo;

                // Set the time to the camera cut time.  Display in terms of unsectioned time
                pEditDrawDistanceInfo->pEvent = cameraEventList[i];

                // set the original and override distances
                pEditDrawDistanceInfo->bOverrideNearClip = true;
 
                 pEditDrawDistanceInfo->fOriginalNearClip = pEditDrawDistanceInfo->fNearClip = pCameraCutEventArgs->GetNearDrawDistance();
                

                pEditDrawDistanceInfo->bOverrideFarClip = true;

             
                pEditDrawDistanceInfo->fOriginalFarClip = pEditDrawDistanceInfo->fFarClip = pCameraCutEventArgs->GetFarDrawDistance();
                
            }
        }
    }

    cutsDisplayf( "Created %d Draw Distance events.", m_editDrawDistanceList.GetCount() );

    // update the combo
    if ( m_pEditDrawDistanceCombo != NULL )
    {
        m_pEditDrawDistanceCombo->UpdateCombo( "Draw Distance Selector", &m_iSelectedDrawDistanceIndex, 
            m_editDrawDistanceList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankDrawDistanceSelectedCallback),this) );

        m_pEditDrawDistanceCombo->SetString( 0, "(none)" );

        // This will populate the combo box with the values
        BankStatusDisplayUnitsChangedCallback();
    }
}

void cutsManager::PopulateHiddenObjectList()
{
	if(!GetCutfFile())
	{
		return;
	}
	
    m_iSelectedHiddenObjectIndex = 0;

    if ( m_pBank == NULL )
    {
        return;
    }

    RAGE_TRACK( cutsManager_PopulateHiddenObjectList );

    // get the draw distance events
    atArray<cutfObject *> hiddenObjectList;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE, hiddenObjectList );
    
    if ( !IsRestarting() && (hiddenObjectList.GetCount() == 0) )
    {
        if ( m_pEditHiddenObjectGroup != NULL )
        {
            while ( m_pEditHiddenObjectGroup->GetChild() )
            {
                m_pBank->Remove( *(m_pEditHiddenObjectGroup->GetChild()) );
            }
        }

        if ( m_pEditHiddenObjectCombo != NULL )
        {
            m_iSelectedHiddenObjectIndex = 0;

            m_pEditHiddenObjectCombo->UpdateCombo( "Hidden Object Selector", &m_iSelectedHiddenObjectIndex, 1, NULL, 
                datCallback(MFA(cutsManager::BankHiddenObjectSelectedCallback),this) );

            m_pEditHiddenObjectCombo->SetString( 0, "(none)" );
        }

        return;
    }

    // only create them once
    if ( m_editHiddenObjectList.GetCount() > 0 )
    {
        return;
    }

    // For each Draw Distance event, edit/add a SEditCutfDrawDistanceInfo object for it
    for ( int i = 0; i < hiddenObjectList.GetCount(); ++i )
    {
        cutfHiddenModelObject *pHiddenModelObject = dynamic_cast<cutfHiddenModelObject *>( hiddenObjectList[i] );

        SEditCutfObjectLocationInfo *pObjectLocationInfo = rage_new SEditCutfObjectLocationInfo;
        pObjectLocationInfo->pObject = pHiddenModelObject;
        safecpy( pObjectLocationInfo->cName, pHiddenModelObject->GetName().GetCStr(), sizeof(pObjectLocationInfo->cName) );
        pObjectLocationInfo->vPosition = pHiddenModelObject->GetPosition();
        pObjectLocationInfo->fRadius = pHiddenModelObject->GetRadius();

        m_editHiddenObjectList.Grow() = pObjectLocationInfo;
    }

    cutsDisplayf( "Found %d pre-existing Hidden Objects.", m_editHiddenObjectList.GetCount() );

    // update the combo
    if ( m_pEditHiddenObjectCombo != NULL )
    {
        m_pEditHiddenObjectCombo->UpdateCombo( "Hidden Object Selector", &m_iSelectedHiddenObjectIndex, 
            m_editHiddenObjectList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankHiddenObjectSelectedCallback),this) );

        m_pEditHiddenObjectCombo->SetString( 0, "(none)" );
        for ( int i = 0; i < m_editHiddenObjectList.GetCount(); ++i )
        {
            m_pEditHiddenObjectCombo->SetString( i + 1, m_editHiddenObjectList[i]->cName );
        }
    }
}

void cutsManager::PopulateFixupObjectList()
{
	if(!GetCutfFile())
	{
		return;
	}
	
    m_iSelectedFixupObjectIndex = 0;

    if ( m_pBank == NULL )
    {
        return;
    }

    RAGE_TRACK( cutsManager_PopulateFixupObjectList );

    // get the draw distance events
    atArray<cutfObject *> fixupObjectList;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_FIXUP_MODEL_OBJECT_TYPE, fixupObjectList );

    if ( !IsRestarting() && (fixupObjectList.GetCount() == 0) )
    {
        if ( m_pEditFixupObjectGroup != NULL )
        {
            while ( m_pEditFixupObjectGroup->GetChild() )
            {
                m_pBank->Remove( *(m_pEditFixupObjectGroup->GetChild()) );
            }
        }

        if ( m_pEditFixupObjectCombo != NULL )
        {
            m_iSelectedFixupObjectIndex = 0;

            m_pEditFixupObjectCombo->UpdateCombo( "Fixup Object Selector", &m_iSelectedFixupObjectIndex, 1, NULL, 
                datCallback(MFA(cutsManager::BankFixupObjectSelectedCallback),this) );

            m_pEditFixupObjectCombo->SetString( 0, "(none)" );
        }

        return;
    }

    // only create them once
    if ( m_editFixupObjectList.GetCount() > 0 )
    {
        return;
    }

    // For each Draw Distance event, edit/add a SEditCutfDrawDistanceInfo object for it
    for ( int i = 0; i < fixupObjectList.GetCount(); ++i )
    {
        cutfFixupModelObject *pFixupModelObject = dynamic_cast<cutfFixupModelObject *>( fixupObjectList[i] );

        SEditCutfObjectLocationInfo *pObjectLocationInfo = rage_new SEditCutfObjectLocationInfo;
        pObjectLocationInfo->pObject = pFixupModelObject;
        safecpy( pObjectLocationInfo->cName, pFixupModelObject->GetName().GetCStr(), sizeof(pObjectLocationInfo->cName) );
        pObjectLocationInfo->vPosition = pFixupModelObject->GetPosition(); // Add the original scene offset
        pObjectLocationInfo->fRadius = pFixupModelObject->GetRadius();

        m_editFixupObjectList.Grow() = pObjectLocationInfo;
    }

    cutsDisplayf( "Found %d pre-existing Fixup Objects.", m_editFixupObjectList.GetCount() );

    // update the combo
    if ( m_pEditFixupObjectCombo != NULL )
    {
        m_pEditFixupObjectCombo->UpdateCombo( "Fixup Object Selector", &m_iSelectedFixupObjectIndex, 
            m_editFixupObjectList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankFixupObjectSelectedCallback),this) );

        m_pEditFixupObjectCombo->SetString( 0, "(none)" );
        for ( int i = 0; i < m_editFixupObjectList.GetCount(); ++i )
        {
            m_pEditFixupObjectCombo->SetString( i + 1, m_editFixupObjectList[i]->cName );
        }
    }
}

void cutsManager::PopulateBlockingBoundsList()
{
	if(!GetCutfFile())
	{
		return;
	}

	m_iSelectedFixupObjectIndex = 0;

	if ( m_pBank == NULL )
	{
		return;
	}

	RAGE_TRACK( cutsManager_PopulateFixupObjectList );

	// get the draw distance events
	atArray<cutfObject *> BlockingBoundObjectList;
	GetCutfFile()->FindObjectsOfType( CUTSCENE_BLOCKING_BOUNDS_OBJECT_TYPE, BlockingBoundObjectList );

	if ( !IsRestarting() && (BlockingBoundObjectList.GetCount() == 0) )
	{
		if ( m_pEditBlockingBoundObjectGroup != NULL )
		{
			while ( m_pEditBlockingBoundObjectGroup->GetChild() )
			{
				m_pBank->Remove( *(m_pEditBlockingBoundObjectGroup->GetChild()) );
			}
		}

		if ( m_pEditBlockingBoundObjectCombo != NULL )
		{
			m_iSelectedBlockingBoundObjectIndex = 0;

			m_pEditBlockingBoundObjectCombo->UpdateCombo( "Blocking bound Object Selector", &m_iSelectedBlockingBoundObjectIndex, 1, NULL, 
				datCallback(MFA(cutsManager::BankBlockingBoundObjectSelectedCallback),this) );

			m_pEditBlockingBoundObjectCombo->SetString( 0, "(none)" );
		}

		return;
	}

	// only create them once
	if ( m_editBlockingBoundObjectList.GetCount() > 0 )
	{
		return;
	}

	// For each Draw Distance event, edit/add a SEditCutfDrawDistanceInfo object for it
	for ( int i = 0; i < BlockingBoundObjectList.GetCount(); ++i )
	{
		cutfBlockingBoundsObject *pBlockBoundObject = dynamic_cast<cutfBlockingBoundsObject *>( BlockingBoundObjectList[i] );

		sEditCutfBlockingBoundsInfo *pObjectBlockBoundsInfo = rage_new sEditCutfBlockingBoundsInfo;
		pObjectBlockBoundsInfo->pObject = pBlockBoundObject;

		char name[32]; 
		formatf(name, sizeof(name)-1, "%s_%d", pBlockBoundObject->GetName().GetCStr(), i); 

		safecpy( pObjectBlockBoundsInfo->cName, name, sizeof(name) );
		pObjectBlockBoundsInfo->vCorners[0] = pBlockBoundObject->GetCorner(0); // + GetCutfFile()->GetOffset(); // Add the original scene offset
		pObjectBlockBoundsInfo->vCorners[1] = pBlockBoundObject->GetCorner(1);
		pObjectBlockBoundsInfo->vCorners[2] = pBlockBoundObject->GetCorner(2);
		pObjectBlockBoundsInfo->vCorners[3] = pBlockBoundObject->GetCorner(3);
		pObjectBlockBoundsInfo->fHeight = pBlockBoundObject->GetHeight();

		m_editBlockingBoundObjectList.Grow() = pObjectBlockBoundsInfo;
	}

	cutsDisplayf( "Found %d pre-existing Blocking Bound Objects.", m_editBlockingBoundObjectList.GetCount() );

	// update the combo
	if ( m_pEditBlockingBoundObjectCombo != NULL )
	{
		m_pEditBlockingBoundObjectCombo->UpdateCombo( "Blocking Bound Object Selector", &m_iSelectedBlockingBoundObjectIndex, 
			m_editBlockingBoundObjectList.GetCount() + 1, NULL, datCallback(MFA(cutsManager::BankBlockingBoundObjectSelectedCallback),this) );

		m_pEditBlockingBoundObjectCombo->SetString( 0, "(none)" );
		for ( int i = 0; i < m_editBlockingBoundObjectList.GetCount(); ++i )
		{
			m_pEditBlockingBoundObjectCombo->SetString( i + 1, m_editBlockingBoundObjectList[i]->cName);
		}
	}
}

void cutsManager::SaveSceneOrientation()
{
    RAGE_TRACK( cutsManager_SaveSceneOrientation );
	if(!cutsVerifyf(GetCutfFile(), "SaveSceneOrientation: No cutf file object loaded cannot save"))
	{
		return;
	}
	
	GetCutfFile()->SetOffset( m_vSceneOffset );
    GetCutfFile()->SetRotation( m_fSceneRotation );
	GetCutfFile()->SetPitch(m_fScenePitch); 
	GetCutfFile()->SetRoll(m_fSceneRoll); 

    s32 iAssetManagerObjectId = FindAssetManagerObjectId();
    if ( iAssetManagerObjectId == -1 )
    {
        return;
    }

    atArray<cutfEvent *> assetMgrEventList;
    GetCutfFile()->FindEventsForObjectIdOnly( iAssetManagerObjectId, GetCutfFile()->GetLoadEventList(), assetMgrEventList );

    for ( int i = 0; i < assetMgrEventList.GetCount(); ++i )
    {
        if ( assetMgrEventList[i]->GetEventId() == CUTSCENE_LOAD_SCENE_EVENT )
        {
            cutfLoadSceneEventArgs *pLoadSceneEventArgs 
                = dynamic_cast<cutfLoadSceneEventArgs *>( const_cast<cutfEventArgs *>( assetMgrEventList[i]->GetEventArgs() ) );
            pLoadSceneEventArgs->SetOffset( m_vSceneOffset );
            pLoadSceneEventArgs->SetRotation( m_fSceneRotation );
            break;
        }
    }

	//update thee objects relative to the scene origin; 
	SaveHiddenObjects(); 
	SaveFixupObjects(); 
}

void cutsManager::SaveDepthOfField()
{
    atBitSet &cutsceneFlags = const_cast<atBitSet &>( GetCutfFile()->GetCutsceneFlags() );
    cutsceneFlags.Set( cutfCutsceneFile2::CUTSCENE_ENABLE_DEPTH_OF_FIELD_FLAG, m_bDepthOfFieldEnabled );
}

void cutsManager::SavePlaybackRange()
{
	//DEPRECATED: No longer relevant saving frame ranges from rag
	//if(!cutsVerifyf(GetCutfFile(), "SavePlaybackRange: No cutf file object loaded cannot save"))
	//{
	//	return;
	//}
	//
 //   // clear the "is sectioned" bit because we will need to re-section the animations now
 //   if ( (m_iStartFrame != GetCutfFile()->GetRangeStart()) 
 //       || (m_iEndFrame != GetCutfFile()->GetRangeEnd()) )
 //   {
 //       atBitSet &bitSet = const_cast<atBitSet &>( GetCutfFile()->GetCutsceneFlags() );
 //       bitSet.Set( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG, false );
 //   }

 //   GetCutfFile()->SetRangeStart( m_iStartFrame );
 //   GetCutfFile()->SetRangeEnd( m_iEndFrame );
}

void cutsManager::SaveDrawDistances()
{
	if(!cutsVerifyf(GetCutfFile(), "SaveDrawDistances: No cutf file object loaded cannot save"))
	{
		return;
	}
	
    if ( m_pCameraObject == NULL )
    {
        return;
    }

    RAGE_TRACK( cutsManager_SaveDrawDistances );

    atArray<cutfEvent *> cameraEventList;
    GetCutfFile()->FindEventsForObjectIdOnly( m_pCameraObject->GetObjectId(), GetCutfFile()->GetEventList(), cameraEventList );

    int iDrawDistanceIndex = 0;

    for ( int i = 0; i < cameraEventList.GetCount(); ++i )
    {
        if ( cameraEventList[i]->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT )
        {
            cutfCameraCutEventArgs *pCameraCutEventArgs;

            cutfEventArgs *pEventArgs = const_cast<cutfEventArgs *>( cameraEventList[i]->GetEventArgs() );
            if ( pEventArgs->GetType() == CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE )
            {
                pCameraCutEventArgs = dynamic_cast<cutfCameraCutEventArgs *>( pEventArgs );
            }
            else
            {
				cameraEventList[i]->SetEventArgs( NULL );
                GetCutfFile()->RemoveEventArgs( pEventArgs );

                pCameraCutEventArgs = rage_new cutfCameraCutEventArgs();

                cameraEventList[i]->SetEventArgs( pCameraCutEventArgs );
                GetCutfFile()->AddEventArgs( pCameraCutEventArgs );
            }

            float fNearDrawDistance = m_editDrawDistanceList[iDrawDistanceIndex]->bOverrideNearClip 
                ? m_editDrawDistanceList[iDrawDistanceIndex]->fNearClip : m_editDrawDistanceList[iDrawDistanceIndex]->fOriginalNearClip;
           
			if(fNearDrawDistance == -1.0f || fNearDrawDistance >=  0.0f)
			{
				pCameraCutEventArgs->SetNearDrawDistance( fNearDrawDistance );
				m_editDrawDistanceList[iDrawDistanceIndex]->bOverrideNearClip = true;
				m_editDrawDistanceList[iDrawDistanceIndex]->fOriginalNearClip = m_editDrawDistanceList[iDrawDistanceIndex]->fNearClip = fNearDrawDistance;
			}
			else
			{
				Assertf(0, "Will not save Camera Cut %d: NearDrawDistance = %f: NearDrawDistance values must be greater than 0.0 (valid draw distance) or -1.0 (use time cycle default)", i, fNearDrawDistance ); 
			}
            float fFarDrawDistance = m_editDrawDistanceList[iDrawDistanceIndex]->bOverrideFarClip 
                ? m_editDrawDistanceList[iDrawDistanceIndex]->fFarClip : m_editDrawDistanceList[iDrawDistanceIndex]->fOriginalFarClip;
			
			if(fFarDrawDistance == -1.0f || fFarDrawDistance >=  0.0f)
			{           
				pCameraCutEventArgs->SetFarDrawDistance( fFarDrawDistance );
				m_editDrawDistanceList[iDrawDistanceIndex]->bOverrideFarClip = true;
				m_editDrawDistanceList[iDrawDistanceIndex]->fOriginalFarClip = m_editDrawDistanceList[iDrawDistanceIndex]->fFarClip = fFarDrawDistance;
			}
			else
			{
				Assertf(0, "Will not save Camera Cut %d: FarDrawDistance = %f: FarDrawDistance values must be greater than 0.0 (valid draw distance) or -1.0 (use time cycle default)", i, fFarDrawDistance); 
			}
            ++iDrawDistanceIndex;
        }
    }
}

void cutsManager::SaveHiddenObjects()
{
    RAGE_TRACK( cutsManager_SaveHiddenObjects );

	if(!cutsVerifyf(GetCutfFile(), "SaveHiddenObjects: No cutf file object loaded cannot save"))
	{
		return;
	}
    // get all of the hidden objects
    atArray<cutfObject *> hiddenObjectList;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE, hiddenObjectList );

    // look for each object's SEditCutfObjectLocationInfo
    for ( int i = 0; i < hiddenObjectList.GetCount(); ++i )
    {
        SEditCutfObjectLocationInfo *pObjectLocationInfo = NULL;
        for ( int j = 0; j < m_editHiddenObjectList.GetCount(); ++j )
        {
            if ( m_editHiddenObjectList[j]->pObject == hiddenObjectList[i] )
            {
                pObjectLocationInfo = m_editHiddenObjectList[j];
                break;
            }
        }

        if ( pObjectLocationInfo != NULL )
        {
            cutfHiddenModelObject *pHiddenModelObject = dynamic_cast<cutfHiddenModelObject *>( hiddenObjectList[i] );
            
            pHiddenModelObject->SetName( pObjectLocationInfo->cName );
            pHiddenModelObject->SetPosition( pObjectLocationInfo->vPosition);    // Deal with objects in world space only
            pHiddenModelObject->SetRadius( pObjectLocationInfo->fRadius );
        }
        else
        {
            // if not found, we can remove the object
			atArray<cutfEvent*>HiddenObjectEvents; 
			GetCutfFile()->FindEventsForObjectIdOnly( hiddenObjectList[i]->GetObjectId(), GetCutfFile()->GetEventList(), HiddenObjectEvents );
			
			for(int k = 0; k<HiddenObjectEvents.GetCount(); k++ )
			{
				GetCutfFile()->RemoveEvent(HiddenObjectEvents[k]);
			}	
			
			GetCutfFile()->RemoveObject( hiddenObjectList[i] );
			SaveCutfile(); 
        }
    }

	// look for any ObjectLocationInfos without a cutfObject and add them to the cut file
	for ( int i = 0; i < m_editHiddenObjectList.GetCount(); ++i )
	{
		if ( m_editHiddenObjectList[i]->pObject == NULL )
		{
			cutfHiddenModelObject *pHiddenModelObject = rage_new cutfHiddenModelObject( -1, m_editHiddenObjectList[i]->cName, 
				m_editHiddenObjectList[i]->vPosition, m_editHiddenObjectList[i]->fRadius );

			GetCutfFile()->AddObject( pHiddenModelObject );

			m_editHiddenObjectList[i]->pObject = pHiddenModelObject;
		}
	}

	//new objects have been added lets reset this list and get them again
	hiddenObjectList.Reset(); 
	
	//list of all the hidden objects in the list
	GetCutfFile()->FindObjectsOfType( CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE, hiddenObjectList );

	
	//search each of our hidden object
	for(int i=0; i<hiddenObjectList.GetCount(); i++)
	{
		atArray<cutfEvent*>HiddenObjectEvents; 
		GetCutfFile()->FindEventsForObjectIdOnly( hiddenObjectList[i]->GetObjectId(), GetCutfFile()->GetEventList(), HiddenObjectEvents );
		
		//this hidden object has no event lets add a view event
		if(HiddenObjectEvents.GetCount() ==0 )
		{
#if	CUTSCENE_EXPORT_EVENTS_WITH_ZERO_BASE_TIME
			float EventTime = GetCutSceneTimeWithRangeOffset();  
#else
			float EventTime = GetTime(); 
#endif

			GetCutfFile()->AddEvent( rage_new cutfObjectIdEvent( hiddenObjectList[i]->GetObjectId(), EventTime,
				CUTSCENE_HIDE_OBJECTS_EVENT, rage_new cutfObjectIdEventArgs( hiddenObjectList[i]->GetObjectId() ) ) );
			GetCutfFile()->SortEvents(); 
			SaveCutfile(); 
		}
		//we have an event for this do nothing right now, just know there is one		
	}
}

void cutsManager::SaveFixupObjects()
{
	RAGE_TRACK( cutsManager_SaveFixupObjects );

	if(!cutsVerifyf(GetCutfFile(), "SaveHiddenObjects: No cutf file object loaded cannot save"))
	{
		return;
	}
	// get all of the fixup objects
	atArray<cutfObject *> ObjectList;
	GetCutfFile()->FindObjectsOfType( CUTSCENE_FIXUP_MODEL_OBJECT_TYPE, ObjectList );

	// look for each object's SEditCutfObjectLocationInfo
	for ( int i = 0; i < ObjectList.GetCount(); ++i )
	{
		SEditCutfObjectLocationInfo *pObjectLocationInfo = NULL;
		for ( int j = 0; j < m_editFixupObjectList.GetCount(); ++j )
		{
			if ( m_editFixupObjectList[j]->pObject == ObjectList[i] )
			{
				pObjectLocationInfo = m_editFixupObjectList[j];
				break;
			}
		}

		if ( pObjectLocationInfo != NULL )
		{
			cutfFixupModelObject *pModelObject = dynamic_cast<cutfFixupModelObject *>( ObjectList[i] );

			pModelObject->SetName( pObjectLocationInfo->cName );
			pModelObject->SetPosition( pObjectLocationInfo->vPosition);    // Remove the original scene offset
			pModelObject->SetRadius( pObjectLocationInfo->fRadius );
		}
		else
		{
			// if not found, we can remove the object
			atArray<cutfEvent*>ObjectEvents; 
			GetCutfFile()->FindEventsForObjectIdOnly( ObjectList[i]->GetObjectId(), GetCutfFile()->GetEventList(), ObjectEvents );

			for(int k = 0; k<ObjectEvents.GetCount(); k++ )
			{
				GetCutfFile()->RemoveEvent(ObjectEvents[k]);
			}	

			GetCutfFile()->RemoveObject( ObjectList[i] );
			SaveCutfile(); 
		}
	}

	// look for any ObjectLocationInfos without a cutfObject and add them to the cut file
	for ( int i = 0; i < m_editFixupObjectList.GetCount(); ++i )
	{
		if ( m_editFixupObjectList[i]->pObject == NULL )
		{
			// Make sure to remove the original scene offset
			cutfFixupModelObject *pModelObject = rage_new cutfFixupModelObject( -1, m_editFixupObjectList[i]->cName, 
				m_editFixupObjectList[i]->vPosition, m_editFixupObjectList[i]->fRadius );

			GetCutfFile()->AddObject( pModelObject );

			m_editFixupObjectList[i]->pObject = pModelObject;
		}
	}

	//new objects have been added lets reset this list and get them again
	ObjectList.Reset(); 

	//list of all the fixup objects in the list
	GetCutfFile()->FindObjectsOfType( CUTSCENE_FIXUP_MODEL_OBJECT_TYPE, ObjectList );


	//search each of our fixup object
	for(int i=0; i<ObjectList.GetCount(); i++)
	{
		atArray<cutfEvent*>ObjectEvents; 
		GetCutfFile()->FindEventsForObjectIdOnly( ObjectList[i]->GetObjectId(), GetCutfFile()->GetEventList(), ObjectEvents );

		//this fixup object has no event lets add a view event
		if(ObjectEvents.GetCount() ==0 )
		{
#if	CUTSCENE_EXPORT_EVENTS_WITH_ZERO_BASE_TIME
			float EventTime = GetCutSceneTimeWithRangeOffset();  
#else
			float EventTime = GetTime(); 
#endif 

			GetCutfFile()->AddEvent( rage_new cutfObjectIdEvent( ObjectList[i]->GetObjectId(), EventTime,
				CUTSCENE_FIXUP_OBJECTS_EVENT, rage_new cutfObjectIdEventArgs( ObjectList[i]->GetObjectId() ) ) );
			GetCutfFile()->SortEvents(); 
			SaveCutfile();
		}
		//we have an event for this do nothing right now, just know there is one		
	}
}

void cutsManager::SaveBlockingBoundObjects()
{
	RAGE_TRACK( cutsManager_SaveBlockingBoundObjects );

	if(!cutsVerifyf(GetCutfFile(), "SaveBlockingBoundObjects: No cutf file object loaded cannot save"))
	{
		return;
	}

	// get all of the BlockingBound objects
	atArray<cutfObject *> BlockingBoundObjectList;
	GetCutfFile()->FindObjectsOfType( CUTSCENE_BLOCKING_BOUNDS_OBJECT_TYPE, BlockingBoundObjectList );

	// look for each object's SEditCutfObjectLocationInfo
	for ( int i = 0; i < BlockingBoundObjectList.GetCount(); ++i )
	{
		sEditCutfBlockingBoundsInfo *pObjectBlockBoundInfo = NULL;
		for ( int j = 0; j < m_editBlockingBoundObjectList.GetCount(); ++j )
		{
			if ( m_editBlockingBoundObjectList[j]->pObject == BlockingBoundObjectList[i] )
			{
				pObjectBlockBoundInfo = m_editBlockingBoundObjectList[j];
				break;
			}
		}

		if ( pObjectBlockBoundInfo == NULL )
		{
			atArray<cutfEvent*>ObjectEvents; 
			GetCutfFile()->FindEventsForObjectIdOnly( BlockingBoundObjectList[i]->GetObjectId(), GetCutfFile()->GetEventList(), ObjectEvents );

			for(int k = 0; k<ObjectEvents.GetCount(); k++ )
			{
				GetCutfFile()->RemoveEvent(ObjectEvents[k]);
			}
			// if not found, we can remove the object
			GetCutfFile()->RemoveObject( BlockingBoundObjectList[i] );
			SaveCutfile(); 
		}
	}

	// look for any ObjectLocationInfos without a cutfObject and add them to the cut file
	for ( int i = 0; i < m_editBlockingBoundObjectList.GetCount(); ++i )
	{
		if ( m_editBlockingBoundObjectList[i]->pObject == NULL )
		{
			// Make sure to remove the original scene offset
			cutfBlockingBoundsObject *pBlockingBoundModelObject = rage_new cutfBlockingBoundsObject( -1, m_editBlockingBoundObjectList[i]->cName, 
				m_editBlockingBoundObjectList[i]->vCorners, m_editBlockingBoundObjectList[i]->fHeight );

			GetCutfFile()->AddObject( pBlockingBoundModelObject );

			m_editBlockingBoundObjectList[i]->pObject = pBlockingBoundModelObject;
		}
	}

	//new objects have been added lets reset this list and get them again
	BlockingBoundObjectList.Reset(); 

	//list of all the hidden objects in the list
	GetCutfFile()->FindObjectsOfType( CUTSCENE_BLOCKING_BOUNDS_OBJECT_TYPE, BlockingBoundObjectList );


	//search each of our hidden object
	for(int i=0; i<BlockingBoundObjectList.GetCount(); i++)
	{
		atArray<cutfEvent*>ObjectEvents; 
		GetCutfFile()->FindEventsForObjectIdOnly( BlockingBoundObjectList[i]->GetObjectId(), GetCutfFile()->GetEventList(), ObjectEvents );

		//this hidden object has no event lets add a view event
		if(ObjectEvents.GetCount() ==0 )
		{
#if	CUTSCENE_EXPORT_EVENTS_WITH_ZERO_BASE_TIME
			float EventTime = GetCutSceneTimeWithRangeOffset();  
#else
			float EventTime = GetTime(); 
#endif

			GetCutfFile()->AddEvent( rage_new cutfObjectIdEvent( BlockingBoundObjectList[i]->GetObjectId(), EventTime,
				CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT, rage_new cutfObjectIdEventArgs( BlockingBoundObjectList[i]->GetObjectId() ) ) );
			GetCutfFile()->SortEvents(); 
			
			SaveCutfile();
		}
		//we have an event for this do nothing right now, just know there is one		
	}

	//rename the objects in the list 
	for(int i=0; i<BlockingBoundObjectList.GetCount(); i++)
	{
		if(i == BlockingBoundObjectList.GetCount() - 1)	
		{		
			SaveCutfile();
		}
	}

}


//Dispatch the previous event for the specific component
void cutsManager::DispatchOppositeVariationEvent(const cutfEvent *pEvent,  atArray<cutfEvent*> &objectEventList)
{
	const cutfEventArgs* pDispatchedEventArgs = pEvent->GetEventArgs();
	if(!pDispatchedEventArgs)
	{
		cutfAssertf(pDispatchedEventArgs, "Cutscene content bug! CUTSCENE_SET_VARIATION_EVENT (id=%i time=%.3f) is corrupted and needs to be recreated, variation will not be applied!", pEvent->GetEventId(), pEvent->GetTime());
		return;
	}

	switch(pDispatchedEventArgs->GetType())
	{
		case CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE:
		{
			const cutfObjectVariationEventArgs* pObjectVar = static_cast<const cutfObjectVariationEventArgs*>(pDispatchedEventArgs); 
			//store the component for the previous events
			int iComponent = pObjectVar->GetComponent(); 

			for ( int i = 0; i < objectEventList.GetCount(); ++i )
			{	
				//find the current event in the list of events
				if ( objectEventList[i] == pEvent )
				{
					if(i > 0)
					{
						//count back through the list of object events
						int index = i-1; 
						for (int j = index ; j >= 0; j-- )
						{
							const cutfEventArgs* pEventArgs = objectEventList[j]->GetEventArgs(); 

							if (pEventArgs->GetType() == CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE)
							{	
								const cutfObjectVariationEventArgs* pObVar = static_cast<const cutfObjectVariationEventArgs*>(pEventArgs); 
								//check for the last event with this component and dispatch
								if(pObVar->GetComponent() == iComponent)
								{
									DispatchEvent( objectEventList[j] );
									return; 
								}
							}
						}
					}

				}
			}
		}
		break; 

		case CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE:
			{
				const cutfVehicleVariationEventArgs* pObjectVar = static_cast<const cutfVehicleVariationEventArgs*>(pDispatchedEventArgs); 
				//store the id for the previous events
				int iObjectid = pObjectVar->GetObjectId(); 

				for ( int i = 0; i < objectEventList.GetCount(); ++i )
				{	
					//find the current event in the list of events
					if ( objectEventList[i] == pEvent )
					{
						if(i > 0)
						{
							//count back through the list of object events for the previous event of this type.
							int index = i-1; 
							for (int j = index ; j >= 0; j-- )
							{
								const cutfEventArgs* pEventArgs = objectEventList[j]->GetEventArgs(); 

								if (pEventArgs->GetType() == CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE)
								{	
									const cutfVehicleVariationEventArgs* pObVar = static_cast<const cutfVehicleVariationEventArgs*>(pEventArgs); 
									//check for the last event with this component and dispatch
									if(pObVar->GetObjectId() == iObjectid)
									{
										DispatchEvent( objectEventList[j] );
										return; 
									}
								}
							}
						}

					}
				}
			}
		break; 

		case CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE:
			{
				const cutfVehicleExtraEventArgs* pObjectVar = static_cast<const cutfVehicleExtraEventArgs*>(pDispatchedEventArgs); 
				//store the component for the previous events
				int iObjectid = pObjectVar->GetObjectId(); 

				for ( int i = 0; i < objectEventList.GetCount(); ++i )
				{	
					//find the current event in the list of events
					if ( objectEventList[i] == pEvent )
					{
						if(i > 0)
						{
							//count back through the list of object events
							int index = i-1; 
							for (int j = index ; j >= 0; j-- )
							{
								const cutfEventArgs* pEventArgs = objectEventList[j]->GetEventArgs(); 

								if (pEventArgs->GetType() == CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE)
								{	
									const cutfVehicleExtraEventArgs* pObVar = static_cast<const cutfVehicleExtraEventArgs*>(pEventArgs); 
									//check for the last event with this component and dispatch
									if(pObVar->GetObjectId() == iObjectid)
									{
										DispatchEvent( objectEventList[j] );
										return; 
									}
								}
							}
						}

					}
				}
			}
			break; 

		default:
			{
			}
		break;
	}
}

void cutsManager::SaveCutfile()
{
	if(!cutsVerifyf(GetCutfFile(), "SaveFixupObjects: No cutf file object loaded cannot save"))
	{
		return;
	}   
    // always save as the tune file
    if ( GetCutfFile()->SaveTuneFile() )
    {
        cutsDisplayf( "Saved tune file '$/tune/cutscene/%s.%s'.", GetCutfFile()->GetSceneName(), PI_CUTSCENE_TUNEFILE_EXT );
    }
    else
    {
        cutsErrorf( "Unable to save tune file '$/tune/cutscene/%s.%s'.", GetCutfFile()->GetSceneName(), PI_CUTSCENE_TUNEFILE_EXT );
    }
}

void cutsManager::DispatchEventToObjectType( s32 iObjectType, s32 iEventId, cutfEventArgs *pEventArgs )
{
    RAGE_TRACK( cutsManager_DispatchEventToObjectType );

	if(!cutsVerifyf(GetCutfFile(), "DispatchEventToObjectType: No cutf file object loaded to find objects of type "))
	{
		return;
	} 
	
    atArray<cutfObject *> objectList;
    GetCutfFile()->FindObjectsOfType( iObjectType, objectList );

    cutfObjectIdEvent evt( 0, GetTime(), iEventId, pEventArgs );

    for ( int i = 0; i < objectList.GetCount(); ++i )
    {
        evt.SetObjectId( objectList[i]->GetObjectId() );
        DispatchEvent( &evt );
    }
}

void cutsManager::DispatchSetDrawDistanceEvent( int iIndex )
{
    if ( (m_pCameraObject == NULL) || (iIndex < 0) || (iIndex >= m_editDrawDistanceList.GetCount()) )
    {
        return;
    }

    SEditCutfDrawDistanceInfo *a = m_editDrawDistanceList[iIndex];

    float fTime = GetTime();

    if ( (iIndex == 0) && (fTime <= a->pEvent->GetTime()) )
    {
        cutfTwoFloatValuesEventArgs twoFloatValuesEventArgs( m_pCameraObject->GetNearDrawDistance(), m_pCameraObject->GetFarDrawDistance() );
        cutfObjectIdEvent objectIdEvent( m_pCameraObject->GetObjectId(), GetTime(), CUTSCENE_SET_DRAW_DISTANCE_EVENT, &twoFloatValuesEventArgs );
        DispatchEvent( &objectIdEvent );
    }
    else
    {        
        float fNextCameraCutTime = iIndex + 1 < m_editDrawDistanceList.GetCount() 
            ? m_editDrawDistanceList[iIndex + 1]->pEvent->GetTime() : GetEndTime();

        if ( (fTime >= a->pEvent->GetTime()) && (fTime < fNextCameraCutTime) )
        {
            cutfTwoFloatValuesEventArgs twoFloatValuesEventArgs( a->bOverrideNearClip ? a->fNearClip : a->fOriginalNearClip,
                a->bOverrideFarClip ? a->fFarClip : a->fOriginalFarClip );
            cutfObjectIdEvent objectIdEvent( m_pCameraObject->GetObjectId(), GetTime(), CUTSCENE_SET_DRAW_DISTANCE_EVENT, &twoFloatValuesEventArgs );
            DispatchEvent( &objectIdEvent );
        }
    }
}

void cutsManager::BankSceneOrientationChangedCallback()
{
	if ( (IsPlaying() || IsPaused()) && m_bEditSceneOrigin )
	{
		//XPARAM(enablecutscenelightauthoring);
		//if(PARAM_enablecutscenelightauthoring.Get())
		//{
		//	cutsAssertf(0, "cannot edit scene origin if you are authioring lights, remove -enablecutscenelightauthoring param");
		//	return; 
		//}

		if(!IsConcatted())
		{
			StoreOriginalOrientationData(0); 
		}
	
		m_vSceneOffset = m_vBankSceneOffset;
		m_fSceneRotation = m_fBankSceneRotation;
		m_fScenePitch = m_fBankScenePitch; 
		m_fSceneRoll = m_fBankSceneRoll; 

		if(!IsConcatted())
		{
			StoreOriginalOrientationData(0); 
		}

		if(IsConcatted())
		{
			s32 concatSection = GetConcatSectionForTime(GetTime()); 
			
			StoreOriginalOrientationData(concatSection); 
			
			if(IsConcatSectionValid(concatSection))
			{
				m_concatDataList[concatSection].vOffset = m_vSceneOffset; 
				m_concatDataList[concatSection].fRotation = m_fSceneRotation; 
				m_concatDataList[concatSection].fPitch = m_fScenePitch; 
				m_concatDataList[concatSection].fRoll = m_fSceneRoll; 
				StoreOriginalOrientationData(concatSection); 
			}
		 }
		
		// Only dispatch if we've affected the current concat section
		DispatchEventToAllEntities( CUTSCENE_SCENE_ORIENTATION_CHANGED_EVENT );
	}
}

void cutsManager::BankSaveSceneOrientationCallback()
{
    if ( IsPlaying() || IsPaused() )
    {
        SaveSceneOrientation();
        SaveCutfile();
    }
}

void cutsManager::BankToggleDepthOfFieldCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        DispatchEventToObjectType( CUTSCENE_CAMERA_OBJECT_TYPE, 
            m_bDepthOfFieldEnabled ? CUTSCENE_ENABLE_DEPTH_OF_FIELD_EVENT : CUTSCENE_DISABLE_DEPTH_OF_FIELD_EVENT );
    }
}

void cutsManager::BankSaveDepthOfFieldCallback()
{
    if ( IsPlaying() || IsPaused() )
    {
        SaveDepthOfField();
        SaveCutfile();
    }
}

void cutsManager::BankStartTimeChangedCallback()
{
    int iMinFrame = 0;
	if (GetCutfFile())
	{	
		if ( GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) )
    {
			iMinFrame = GetCutfFile()->GetRangeStart();
		}
    }

    if ( m_iStartFrame < iMinFrame )
    {
        m_iStartFrame = iMinFrame;
    }
    else if ( m_iStartFrame >= m_iEndFrame )
    {
        m_iStartFrame = m_iEndFrame - 1;
    }

    if ( m_iLoopStartFrame < m_iStartFrame )
    {
        m_iLoopStartFrame = m_iStartFrame;
    }
}

void cutsManager::BankEndTimeChangedCallback()
{
	if (!cutsVerifyf(GetCutfFile(), "BankEndTimeChangedCallback: No cut file obect loaded"))
	{
		return;
	}	
	if ( m_iEndFrame > GetCutfFile()->GetRangeEnd() )
    {
		m_iEndFrame = GetCutfFile()->GetRangeEnd();
    }
    else if ( m_iEndFrame <= m_iStartFrame )
    {
        m_iEndFrame = m_iStartFrame + 1;
    }

    if ( m_iLoopEndFrame > m_iEndFrame )
    {
        m_iLoopEndFrame = m_iEndFrame;
    }
}

void cutsManager::BankSavePlaybackRangeCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        SavePlaybackRange();
        SaveCutfile();
    }
}

void cutsManager::BankPauseCallback()
{
    if ( IsPlaying() )
    {
		m_ScrubbingState = SCRUBBING_PAUSING; 
		
		SetSkipFrame(m_iCurrentFrameWithFrameRanges); 
        Pause();
		
    }
}

void cutsManager::BankPlayForwardsCallback()
{
	if(m_PlayBackState == PLAY_STATE_FORWARDS_NORMAL_SPEED)
	{
		return; 
	}

    if ( IsPlaying() )
    {
        // This will trick the system into re-queuing the audio, if any, before resuming playback at normal speed
        m_cutsceneState = CUTSCENE_PAUSED_STATE;
#if __BANK
		m_bNeedToLoadBeforeResuming = true;
#endif
    }

	FixUpEventIndicesForPlayBackDirectionChange(); 

    m_PlayBackState = PLAY_STATE_FORWARDS_NORMAL_SPEED;
	m_fPlaybackRate = 1.0f; 
	m_ScrubbingState = SCRUBBING_NONE; 
	m_bNeedToLoadBeforeResuming = true;
	m_bResumingPlayFromPause = true; 

    Play();
}

void cutsManager::FixUpPlayBackEventIndices(bool hasChangedPlayBackDirection)
{
	if(hasChangedPlayBackDirection)
	{
		if(m_PlayBackState == PLAY_STATE_FORWARDS_NORMAL_SPEED || m_PlayBackState == PLAY_STATE_FORWARDS_SCALED)
		{
			if(m_iNextEventIndex > 0)
			{
				m_iNextEventIndex--; 
			}
		}

		if (m_PlayBackState == PLAY_STATE_BACKWARDS)
		{
			m_iNextEventIndex++; 

			const atArray<cutfEvent*>& pEventList = GetCutfFile()->GetEventList();

			if ( m_iNextEventIndex >= pEventList.GetCount() )
			{
				m_iNextEventIndex = pEventList.GetCount() - 1;
			}
		}
	}
}

void cutsManager::FixUpLoadEventIndices(bool hasChangedPlayBackDirection)
{
	if(hasChangedPlayBackDirection)
	{
		if(m_PlayBackState == PLAY_STATE_FORWARDS_NORMAL_SPEED || m_PlayBackState == PLAY_STATE_FORWARDS_SCALED)
		{
			if(m_iNextLoadEvent > 0)
			{
				m_iNextLoadEvent--; 
			}
		}

		if (m_PlayBackState == PLAY_STATE_BACKWARDS)
		{
			m_iNextLoadEvent++; 

			const atArray<cutfEvent*>& pEventList = GetCutfFile()->GetLoadEventList();

			if ( m_iNextLoadEvent >= pEventList.GetCount() )
			{
				m_iNextLoadEvent = pEventList.GetCount() - 1;
			}
		}
	}
}

void cutsManager::FixUpUnloadEventIndices(bool hasChangedPlayBackDirection)
{
	if(hasChangedPlayBackDirection)
	{
		if(m_PlayBackState == PLAY_STATE_FORWARDS_NORMAL_SPEED || m_PlayBackState == PLAY_STATE_FORWARDS_SCALED)
		{
			if(m_iNextUnloadEvent > 0)
			{
				m_iNextUnloadEvent--; 
			}
		}

		if (m_PlayBackState == PLAY_STATE_BACKWARDS)
		{
			m_iNextUnloadEvent++; 

			const atArray<cutfEvent*>& pEventList = GetCutfFile()->GetLoadEventList();

			if ( m_iNextUnloadEvent >= pEventList.GetCount() )
			{
				m_iNextUnloadEvent = pEventList.GetCount() - 1;
			}
		}
	}
}

void cutsManager::FixUpEventIndicesForPlayBackDirectionChange()
{
	bool hasChangedPlayBackDirection = false; 

	if(m_PreviousPlayBackState == PLAY_STATE_BACKWARDS && (m_PlayBackState == PLAY_STATE_FORWARDS_NORMAL_SPEED || m_PlayBackState == PLAY_STATE_FORWARDS_SCALED))
	{
		hasChangedPlayBackDirection = true; 
	}

	if(m_PlayBackState == PLAY_STATE_BACKWARDS && (m_PreviousPlayBackState == PLAY_STATE_FORWARDS_NORMAL_SPEED || m_PreviousPlayBackState == PLAY_STATE_FORWARDS_SCALED))
	{
		hasChangedPlayBackDirection = true; 
	}
	
	FixUpPlayBackEventIndices(hasChangedPlayBackDirection); 
	FixUpLoadEventIndices(hasChangedPlayBackDirection); 
	FixUpUnloadEventIndices(hasChangedPlayBackDirection); 
}

void cutsManager::BankPlayBackwardsCallback()
{  
	if ( IsPlaying() )
	{
		// This will trick the system into re-queuing the audio, if any, before resuming playback at normal speed
		m_cutsceneState = CUTSCENE_PAUSED_STATE;
	}
	
	//Change the next event index depending on the direction of play
	FixUpEventIndicesForPlayBackDirectionChange();

	if ( IsRunning() )
    {
		m_PlayBackState = PLAY_STATE_BACKWARDS; 
		m_ScrubbingState = SCRUBBING_PLAYING_BACKWARDS; 
		//reset this so that the fast forward and rewind will work
		m_fPlaybackRate = -1.0f; 
		PlayBackward(); 
		   
		Play();
	}
}

void cutsManager::BankStepForwardCallback()
{
	//Change the next event index depending on the direction of play
	FixUpEventIndicesForPlayBackDirectionChange();

	StepForward();
}

void cutsManager::BankStepBackwardCallback()
{
//Change the next event index depending on the direction of play
	FixUpEventIndicesForPlayBackDirectionChange();

	StepBackward();
}

void cutsManager::BankFastForwardCallback()
{
	//Change the next event index depending on the direction of play
	FixUpEventIndicesForPlayBackDirectionChange();
	
	FastForward();
}

void cutsManager::BankRewindCallback()
{
	//Change the next event index depending on the direction of play
	FixUpEventIndicesForPlayBackDirectionChange(); 

    Rewind();
}

void cutsManager::BankRestartCallback()
{
    Restart();
}

void cutsManager::BankLoopStartTimeChangedCallback()
{
    if ( m_iLoopStartFrame < m_iStartFrame )
    {
        m_iLoopStartFrame = m_iStartFrame;
    }

    if ( m_iLoopStartFrame >= m_iLoopEndFrame )
    {
        m_iLoopStartFrame = m_iLoopEndFrame - 1;

    }

	m_fStartTime = GetStartTimeFromStartFrame(); 
}

void cutsManager::BankLoopEndTimeChangedCallback()
{
    if ( m_iLoopEndFrame > m_iEndFrame )
    {
        m_iLoopEndFrame = m_iEndFrame;
    }

    if ( m_iLoopEndFrame <= m_iLoopStartFrame )
    {
        m_iLoopEndFrame = m_iStartFrame + 1;
    }

	m_fEndTime = GetEndTimeFromEndFrame(); 
}

void cutsManager::BankFrameScrubbingCallback()
{
	SetScrubbingControl(m_iCurrentFrameWithFrameRanges); 
}

void cutsManager::BankPhaseScrubbingCallback()
{

}

void cutsManager::SetFrameRangeForPlayBackSliders()
{	
	float fStart = float(m_iStartFrame); 
	float fEnd = float(m_iEndFrame); 

	if (m_pFacePlayBackSlider)
	{
		m_pFacePlayBackSlider->SetRange(fStart, fEnd); 
	}

	if (m_pLightPlayBackSlider)
	{
		m_pLightPlayBackSlider->SetRange(fStart, fEnd); 
	}

	if (m_pCameraPlayBackSlider)
	{
		m_pCameraPlayBackSlider->SetRange(fStart, fEnd); 
	}

	if (m_pHiddenObjPlayBackSlider)
	{
		m_pHiddenObjPlayBackSlider->SetRange(fStart, fEnd); 
	}

	if (m_pFixupObjPlayBackSlider)
	{
		m_pFixupObjPlayBackSlider->SetRange(fStart, fEnd); 
	}

	if (m_pBlockingBoundPlayBackSlider)
	{
		m_pBlockingBoundPlayBackSlider->SetRange(fStart, fEnd); 
	}

	if (m_pPlayBackSlider)
	{
		m_pPlayBackSlider->SetRange(fStart, fEnd); 
	}

	if (m_pLoopStartSlider)
	{
		float fMin, fMax;
		const cutfCutsceneFile2 *pCutfFile = GetCutfFile();
		if(pCutfFile)
		{
			fMin = static_cast< float >(pCutfFile->GetRangeStart());
			fMax = static_cast< float >(pCutfFile->GetRangeEnd());
		}
		else
		{
			fMin = fStart;
			fMax = fEnd;
		}
		m_pLoopStartSlider->SetRange(fMin, fMax); 
	}

	if (m_pLoopEndSlider)
	{
		float fMin, fMax;
		const cutfCutsceneFile2 *pCutfFile = GetCutfFile();
		if(pCutfFile)
		{
			fMin = static_cast< float >(pCutfFile->GetRangeStart());
			fMax = static_cast< float >(pCutfFile->GetRangeEnd());
		}
		else
		{
			fMin = fStart;
			fMax = fEnd;
		}
		m_pLoopEndSlider->SetRange(fMin, fMax); 
	}
}

void cutsManager::BankStatusDisplayUnitsChangedCallback()
{
    // Update the Draw Distance combo box
    if ( m_pEditDrawDistanceCombo != NULL )
    {
		if (!cutsVerifyf(GetCutfFile(), "BankStatusDisplayUnitsChangedCallback: No cut file obect loaded"))
		{
			return;
		}	
			
        // Apply an offset so the display is always in terms of unsectioned time.
        float fOffset = GetCutfFile()->GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) 
            ? GetCutfFile()->GetRangeStart() / CUTSCENE_FPS: 0.0f;
        
        for ( int i = 0; i < m_editDrawDistanceList.GetCount(); ++i )
        {
            float fTime = m_editDrawDistanceList[i]->pEvent->GetTime() + fOffset;

            char cDisplayTime[32];
            
			if ( m_iStatusUnits == FRAMES )
            {
				formatf(cDisplayTime, sizeof(cDisplayTime), "Camera: %d, Frame: %d", i, (int)(fTime * CUTSCENE_FPS) );
			}
            else 
            {
                formatf(cDisplayTime, sizeof(cDisplayTime), "Camera: %d, Time: (%.03f)", i, fTime);
            }

            m_pEditDrawDistanceCombo->SetString( i + 1, cDisplayTime );
        }
    }
}

void cutsManager::BankDisplayDebugLinesChangedCallback()
{
    BankDisplayBlockingBoundsLinesChangedCallback();
    BankDisplayRemovalBoundsLinesChangedCallback();
    BankDisplayCameraLinesChangedCallback();
    BankDisplayLightLinesChangedCallback();
    BankDisplayModelLinesChangedCallback();
    BankDisplayParticleEffectLinesCallback();
}

void cutsManager::BankDisplayBlockingBoundsLinesChangedCallback()
{
 #if HACK_GTA4
	DispatchEventToObjectType( CUTSCENE_BLOCKING_BOUNDS_OBJECT_TYPE, 
		(m_bDisplayBlockingBoundsLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#else
	DispatchEventToObjectType( CUTSCENE_BLOCKING_BOUNDS_OBJECT_TYPE, 
        (m_bDisplayDebugLines && m_bDisplayBlockingBoundsLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#endif
}

void cutsManager::BankDisplayRemovalBoundsLinesChangedCallback()
{
#if HACK_GTA4
	DispatchEventToObjectType( CUTSCENE_REMOVAL_BOUNDS_OBJECT_TYPE, 
		(m_bDisplayRemovalBoundsLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#else
    DispatchEventToObjectType( CUTSCENE_REMOVAL_BOUNDS_OBJECT_TYPE, 
        (m_bDisplayDebugLines && m_bDisplayRemovalBoundsLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#endif
}

void cutsManager::BankDisplayCameraLinesChangedCallback()
{
#if HACK_GTA4
	DispatchEventToObjectType( CUTSCENE_CAMERA_OBJECT_TYPE, 
		(m_bDisplayCameraLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#else
    DispatchEventToObjectType( CUTSCENE_CAMERA_OBJECT_TYPE, 
        (m_bDisplayDebugLines && m_bDisplayCameraLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#endif
}

void cutsManager::BankDisplayLightLinesChangedCallback()
{
#if HACK_GTA4
	DispatchEventToObjectType( CUTSCENE_LIGHT_OBJECT_TYPE, 
		(m_bDisplayLightLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
	DispatchEventToObjectType( CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE, 
		(m_bDisplayLightLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#else
	DispatchEventToObjectType( CUTSCENE_LIGHT_OBJECT_TYPE, 
        (m_bDisplayDebugLines && m_bDisplayLightLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
	DispatchEventToObjectType( CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE, 
		(m_bDisplayDebugLines && m_bDisplayLightLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#endif
}

void cutsManager::BankDisplayModelLinesChangedCallback()
{
#if HACK_GTA4
	DispatchEventToObjectType( CUTSCENE_MODEL_OBJECT_TYPE, 
		(m_bDisplayModelLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#else
    DispatchEventToObjectType( CUTSCENE_MODEL_OBJECT_TYPE, 
        (m_bDisplayDebugLines && m_bDisplayModelLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#endif
}

void cutsManager::BankDisplayParticleEffectLinesCallback()
{
#if HACK_GTA4
	DispatchEventToObjectType( CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE, 
		(m_bDisplayParticleEffectLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );

	DispatchEventToObjectType( CUTSCENE_DECAL_OBJECT_TYPE, 
		(m_bDisplayParticleEffectLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );

	DispatchEventToObjectType( CUTSCENE_PARTICLE_EFFECT_OBJECT_TYPE, 
						  (m_bDisplayParticleEffectLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#else
    DispatchEventToObjectType( CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE, 
        (m_bDisplayDebugLines && m_bDisplayParticleEffectLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
#endif
}

void cutsManager::BankDisplayHiddenObjectLinesCallback()
{
	DispatchEventToObjectType( CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE, 
	(m_bDisplayHiddenLines) ? CUTSCENE_SHOW_DEBUG_LINES_EVENT : CUTSCENE_HIDE_DEBUG_LINES_EVENT );
}

void cutsManager::BankMuteAudioCallback()
{
    if ( IsPlaying() || IsPaused() )
    {
        s32 iObjectId = FindAudioObjectId();
        if ( iObjectId != -1 )
        {
            cutfObjectIdEvent objectIdEvent( iObjectId, GetTime(), m_bMuteAudio ? CUTSCENE_MUTE_AUDIO_EVENT : CUTSCENE_UNMUTE_AUDIO_EVENT );
            DispatchEvent( &objectIdEvent );
        }
    }
}

void cutsManager::BankFaceZoomDistanceChangedCallback()
{
    RAGE_TRACK( cutsManager_BankFaceZoomDistanceChangedCallback );

	if (!cutsVerifyf(GetCutfFile(), "BankFaceZoomDistanceChangedCallback: No cut file obect loaded"))
	{
		return;
	}	   
    atArray<cutfObject *> cameraObjectList;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );

    if ( cameraObjectList.GetCount() > 0 )
    {
        cutfFloatValueEventArgs floatValueEventArgs( m_fFaceZoomDistance );
        cutfObjectIdEvent objectIdEvent( cameraObjectList[0]->GetObjectId(), GetTime(), CUTSCENE_SET_FACE_ZOOM_DISTANCE_EVENT, &floatValueEventArgs );
        DispatchEvent( &objectIdEvent );
    }
}

void cutsManager::BankFaceZoomNearDrawDistanceChangedCallback()
{
    RAGE_TRACK( cutsManager_BankFaceZoomNearDrawDistanceChangedCallback );

	if (!cutsVerifyf(GetCutfFile(), "BankFaceZoomNearDrawDistanceChangedCallback: No cut file obect loaded"))
	{
		return;
	}	
    atArray<cutfObject *> cameraObjectList;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );

    if ( cameraObjectList.GetCount() > 0 )
    {
        cutfFloatValueEventArgs floatValueEventArgs( m_fFaceZoomNearDrawDistance );
        cutfObjectIdEvent objectIdEvent( cameraObjectList[0]->GetObjectId(), GetTime(), CUTSCENE_SET_FACE_ZOOM_NEAR_DRAW_DISTANCE_EVENT,
            &floatValueEventArgs );
        DispatchEvent( &objectIdEvent );
    }
}

void cutsManager::BankFaceZoomFarDrawDistanceChangedCallback()
{
    RAGE_TRACK( cutsManager_BankFaceZoomFarDrawDistanceChangedCallback );

	if (!cutsVerifyf(GetCutfFile(), "BankFaceZoomFarDrawDistanceChangedCallback: No cut file obect loaded"))
	{
		return;
	}
    atArray<cutfObject *> cameraObjectList;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );

    if ( cameraObjectList.GetCount() > 0 )
    {
        cutfFloatValueEventArgs floatValueEventArgs( m_fFaceZoomFarDrawDistance );
        cutfObjectIdEvent objectIdEvent( cameraObjectList[0]->GetObjectId(), GetTime(), CUTSCENE_SET_FACE_ZOOM_FAR_DRAW_DISTANCE_EVENT,
            &floatValueEventArgs );
        DispatchEvent( &objectIdEvent );
    }
}

void cutsManager::BankFaceSelectedCallback()
{
    SetFace( m_iSelectedPedModelIndex - 1 );    // Subtract 1, because we go from [0, GetNumFaces())
}

void cutsManager::UpdateRagForLightTimeFlags()
{
	UpdateFlagsRagWidgets(m_LightHour1, m_LightTimeFlags,BIT0); 
	UpdateFlagsRagWidgets(m_LightHour2, m_LightTimeFlags,BIT1);
	UpdateFlagsRagWidgets(m_LightHour3, m_LightTimeFlags,BIT2);
	UpdateFlagsRagWidgets(m_LightHour4, m_LightTimeFlags,BIT3);
	UpdateFlagsRagWidgets(m_LightHour5, m_LightTimeFlags,BIT4);
	UpdateFlagsRagWidgets(m_LightHour6, m_LightTimeFlags,BIT5);
	UpdateFlagsRagWidgets(m_LightHour7, m_LightTimeFlags,BIT6);
	UpdateFlagsRagWidgets(m_LightHour8, m_LightTimeFlags,BIT7);
	UpdateFlagsRagWidgets(m_LightHour9, m_LightTimeFlags,BIT8);
	UpdateFlagsRagWidgets(m_LightHour10, m_LightTimeFlags,BIT9);
	UpdateFlagsRagWidgets(m_LightHour11, m_LightTimeFlags,BIT10);
	UpdateFlagsRagWidgets(m_LightHour12, m_LightTimeFlags,BIT11);
	UpdateFlagsRagWidgets(m_LightHour13, m_LightTimeFlags,BIT12);
	UpdateFlagsRagWidgets(m_LightHour14, m_LightTimeFlags,BIT13);
	UpdateFlagsRagWidgets(m_LightHour15, m_LightTimeFlags,BIT14);
	UpdateFlagsRagWidgets(m_LightHour16, m_LightTimeFlags,BIT15);
	UpdateFlagsRagWidgets(m_LightHour17, m_LightTimeFlags,BIT16);
	UpdateFlagsRagWidgets(m_LightHour18, m_LightTimeFlags,BIT17);
	UpdateFlagsRagWidgets(m_LightHour19, m_LightTimeFlags,BIT18);
	UpdateFlagsRagWidgets(m_LightHour20, m_LightTimeFlags,BIT19);
	UpdateFlagsRagWidgets(m_LightHour21, m_LightTimeFlags,BIT20);
	UpdateFlagsRagWidgets(m_LightHour22, m_LightTimeFlags,BIT21);
	UpdateFlagsRagWidgets(m_LightHour23, m_LightTimeFlags,BIT22);
	UpdateFlagsRagWidgets(m_LightHour24, m_LightTimeFlags,BIT23);
}

void cutsManager::UpdateRagForLightFlags()
{
	UpdateFlagsRagWidgets(m_LightCastStaticObjectShadowFlag,m_LightFlags, CUTSCENE_LIGHTFLAG_CAST_STATIC_GEOM_SHADOWS); 
	UpdateFlagsRagWidgets(m_LightCastDynamicObjectShadowFlag, m_LightFlags,CUTSCENE_LIGHTFLAG_CAST_DYNAMIC_GEOM_SHADOWS); 
	UpdateFlagsRagWidgets(m_LightCalcFromSunFlag, m_LightFlags,CUTSCENE_LIGHTFLAG_CALC_FROM_SUN); 
	UpdateFlagsRagWidgets(m_LightEnableBuzzingFlag,m_LightFlags, CUTSCENE_LIGHTFLAG_ENABLE_BUZZING); 
	UpdateFlagsRagWidgets(m_LightForceBuzzingFlag, m_LightFlags,CUTSCENE_LIGHTFLAG_FORCE_BUZZING); 
	UpdateFlagsRagWidgets(m_LightDrawVolumeFlag, m_LightFlags,CUTSCENE_LIGHTFLAG_DRAW_VOLUME); 
	UpdateFlagsRagWidgets(m_lightNoSpecularFlag, m_LightFlags,CUTSCENE_LIGHTFLAG_NO_SPECULAR); 
	UpdateFlagsRagWidgets(m_LightCoronaOnlyFlag, m_LightFlags,CUTSCENE_LIGHTFLAG_CORONA_ONLY); 
	UpdateFlagsRagWidgets(m_LightBothIntAndExtFlag, m_LightFlags,CUTSCENE_LIGHTFLAG_BOTH_INTERIOR_AND_EXTERIOR); 
	UpdateFlagsRagWidgets(m_LightNotInReflection, m_LightFlags,CUTSCENE_LIGHTFLAG_NOT_IN_REFLECTION); 
	UpdateFlagsRagWidgets(m_LightOnlyInReflection, m_LightFlags,CUTSCENE_LIGHTFLAG_ONLY_IN_REFLECTION); 
	UpdateFlagsRagWidgets(m_LightReflector, m_LightFlags, CUTSCENE_LIGHTFLAG_REFLECTOR); 
	UpdateFlagsRagWidgets(m_LightDiffuser, m_LightFlags, CUTSCENE_LIGHTFLAG_DIFFUSER); 
	UpdateFlagsRagWidgets(m_LightCalcByAmbientFlag, m_LightFlags, CUTSCENE_LIGHTFLAG_ADD_AMBIENT_LIGHT); 
	UpdateFlagsRagWidgets(m_LightFlagsDontLightAlpha, m_LightFlags, CUTSCENE_LIGHTFLAG_DONT_LIGHT_ALPHA); 
	UpdateFlagsRagWidgets(m_LightUseAsPedLight, m_LightFlags, CUTSCENE_LIGHTFLAG_IS_CHARACTER_LIGHT); 
	UpdateFlagsRagWidgets(m_UseIntensityAsMultiplier, m_LightFlags, CUTSCENE_LIGHTFLAG_CHARACTER_LIGHT_INTENSITY_AS_MULTIPLIER); 
	UpdateFlagsRagWidgets(m_IsPedOnlyLight, m_LightFlags, CUTSCENE_LIGHTFLAG_IS_PED_ONLY_LIGHT);
}

void cutsManager::BankActiveLightSelectedCallBack()
{
	if(m_iActiveLightIndex >0 && m_iActiveLightIndex <= m_activeLightList.GetCount())
	{
		if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount()) )
		{
			SEditCutfLightInfo *pActiveLightIndex = m_activeLightList[m_iActiveLightIndex - 1];

			if(pActiveLightIndex)
			{
				for(int i=0; i < m_editLightList.GetCount(); i++)
				{
					if(m_editLightList[i] == pActiveLightIndex)
					{
						m_iSelectedLightIndex = i+1; 
						BankLightSelectedCallback();
						UpdateActiveLightSelectionHistory(); 
					}
				}
			}
		}
	}
}

void cutsManager::CreateDuplicateLightName(const atString& lightName)
{
	atString OriginalLightName(lightName); 
	atArray<atString> atSplit; 

	OriginalLightName.Split(atSplit, "_" ,true);

	bool haveValidDuplicateName = false; 

	if(atSplit.GetCount() > 0)
	{
		atString numberSuffix = atSplit[atSplit.GetCount()-1]; 
		int number = atoi(numberSuffix.c_str()); 

		while(!haveValidDuplicateName)
		{
			atString DuplicateLightName = lightName; 
			
			number += 1; 

			char suffix[8] = {0};

			if(number < 10)
			{
				formatf(suffix, 8, "00%d", number);
			}
			else if(number < 99)
			{
				formatf(suffix, 8, "0%d", number);
			}
			else
			{
				formatf(suffix, 8, "%d", number);
			}

			DuplicateLightName.Replace( atSplit[atSplit.GetCount()-1].c_str(), suffix);

			bool foundDuplicate = false; 
			for(int i=0; i < m_editLightList.GetCount(); i++)
			{
				if(m_editLightList[i]->pObject)
				{
					if(m_editLightList[i]->pObject->GetDisplayName() == DuplicateLightName)
					{
						foundDuplicate = true; 
					}
				}
			}

			if(!foundDuplicate)
			{
				formatf(m_duplicatedLightName, 256, "%s",  DuplicateLightName.c_str()); 
				haveValidDuplicateName = true; 
			}
		}
	}
}




void cutsManager::BankLightSelectedCallback()
{
    static atNonFinalHashString info; 
	if ( IsPaused() || IsPlaying() )
    {
        RAGE_TRACK( cutsManager_BankLightSelectedCallback );
 
        if ( (m_pEditLightGroup != NULL) && (m_pBank != NULL) )
        {
            while ( m_pEditLightGroup->GetChild() )
            {
                m_pBank->Remove( *(m_pEditLightGroup->GetChild()) );
            }

            if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount()) )
            {
                m_pBank->SetCurrentGroup( *m_pEditLightGroup );
				{
					SEditCutfLightInfo *pEditLightInfo = GetSelectedLightInfo();
					
					if( pEditLightInfo && pEditLightInfo->pObject)
					{	
						const cutfLightObject *pLightObject = static_cast <const cutfLightObject *>( pEditLightInfo->pObject );

						if(pEditLightInfo->pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
						{
							CreateDuplicateLightName(pLightObject->GetDisplayName()); 

							formatf(m_LightNewName, 256, "%s",  pLightObject->GetName().GetCStr()); 

						}
					
						if(pEditLightInfo->IsActive)
						{
							//updating the widget with pdata
							UpdateLightEditInfo(); 

							UpdateRagForLightFlags(); 
							UpdateRagForLightTimeFlags(); 

							char cLightType[32]; 
							switch ( pLightObject->GetLightType())
							{
							case CUTSCENE_DIRECTIONAL_LIGHT_TYPE:
								strcpy( cLightType, "Directional" );
								break;
							case CUTSCENE_POINT_LIGHT_TYPE:
								strcpy( cLightType, "Point" );
								break;
							case CUTSCENE_SPOT_LIGHT_TYPE:
								strcpy( cLightType, "Spot" );
								break;
							default:
								strcpy( cLightType, "(unknown)" );
								break;
							}

							if(pEditLightInfo->pObject->GetType() == CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE)
							{
								atVarString LightInfo("Animated | %s | %s ",  pLightObject->GetName().GetCStr(), cLightType); 
								info.Clear(); 
								info = LightInfo; 
							}
							else
							{
								atVarString LightInfo("%s | %s ",  m_LightNewName, cLightType); 
								info.Clear(); 
								info = LightInfo; 
							}
					

							m_pBank->AddText( "Info", &info, true );

							if(pEditLightInfo->pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
							{
								m_pBank->AddToggle( "Override Position", &pEditLightInfo->bOverridePosition,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							}
							m_pBank->AddVector( "Position", &m_vLightPosition, -10000.0f, 10000.0f, 0.1f,
								datCallback(MFA(cutsManager::BankLightPositionChangedCallBack),this) );
							
							if(pEditLightInfo->pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
							{
							m_pBank->AddToggle( "Override Direction", &pEditLightInfo->bOverrideDirection,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							}

							m_pBank->AddVector( "Direction", &m_vLightOrientation, -180.0f, 180.0f, 1.0f,
								datCallback(MFA(cutsManager::BankLightDirectionChangedCallBack),this) );
							
							if(pEditLightInfo->pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
							{
							m_pBank->AddToggle( "Override Intensity", &pEditLightInfo->bOverrideIntensity, 
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							}

							m_pBank->AddSlider( "Intensity", &m_fLightIntensity, 0.0f, 100.0f, 0.01f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );

							if(pEditLightInfo->pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
							{
							m_pBank->AddToggle( "Override FallOff", &pEditLightInfo->bOverrideFallOff,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							}

							m_pBank->AddSlider( "FallOff", &m_fLightFallOff, 0.0f, 100.0f, 0.1f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );
							
							if(pEditLightInfo->pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
							{
							m_pBank->AddToggle( "Override FallOff Exponent", &pEditLightInfo->bOverrideExpoFallOff,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							}
							m_pBank->AddSlider( "Exponent FallOff", &m_fLightExponentFallOff, 0.0f, 256.0f, 0.01f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );

							if(pEditLightInfo->pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
							{
							m_pBank->AddToggle( "Override Color", &pEditLightInfo->bOverrideColor,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							}
							m_pBank->AddColor( "Color", &m_vLightColor, 0.01f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );

							if(pEditLightInfo->pObject->GetType() == CUTSCENE_LIGHT_OBJECT_TYPE)
							{
							m_pBank->AddToggle( "Override ConeAngle", &pEditLightInfo->bOverrideConeAngle,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							}
							m_pBank->AddAngle( "ConeAngle (degrees)", &m_fLightConeAngle, bkAngleType::DEGREES, 0.0f, 180.0f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );
						
							m_pBank->AddToggle( "Override Inner ConeAngle", &pEditLightInfo->bOverrideInnerConeAngle,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							m_pBank->AddAngle( "Inner ConeAngle (degrees)", &m_fInnerConeAngle, bkAngleType::DEGREES, 0.0f, 180.0f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );

							m_pBank->AddToggle( "Override Shadow Blur", &pEditLightInfo->bOverrideShadowBlur,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							m_pBank->AddSlider( "Shadow Blur", &m_fShadowBlur, 0.0f, 255.0f, 1.0f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );

							m_pBank->PushGroup("Volume");
							
							m_pBank->AddToggle( "Override Volume Outer Colour And Intensity", &pEditLightInfo->bOverrideOuterColourAndIntensity,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
								m_pBank->AddVector( "Volume Outer Colour And Intensity", &m_vVolumeOuterColourAndIntensity, 0.0f, 256.0f, 0.01f,
									datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );
							
								m_pBank->AddToggle( "Override Volume", &pEditLightInfo->bOverrideVolumeIntensity,
									datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
						
								m_pBank->AddSlider( "Volume Intensity", &m_fVolumeIntensity, 0.0f, 100.0f, 0.1f,
									datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );
							
								m_pBank->AddToggle( "Override Volume Size Scale", &pEditLightInfo->bOverrideVolumeSizeScale,
									datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							
								m_pBank->AddSlider( "Volume Size Scale", &m_fVolumeSizeScale, 0.0f, 100.0f, 0.1f,
									datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );

							m_pBank->PopGroup();

							m_pBank->PushGroup("Corona");
						
							m_pBank->AddToggle( "Override Corona Intensity", &pEditLightInfo->bOverrideCoronaIntensity,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							m_pBank->AddSlider( "Corona Intensity", &m_fCoronaIntensity, 0.0f, 100.0f, 0.1f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );
						
							m_pBank->AddToggle( "Override Corona Size", &pEditLightInfo->bOverrideCoronaSize,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							m_pBank->AddSlider( "Corona Size", &m_fCoronaSize, 0.0f, 100.0f, 0.1f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );

						
							m_pBank->AddToggle( "Override Corona Z Bias", &pEditLightInfo->bOverrideCoronaZbias,
								datCallback(MFA(cutsManager::BankLightOverrideChangedCallback),this) );
							m_pBank->AddSlider( "Corona Z Bias", &m_fCoronaZBias, 0.0f, 100.0f, 0.1f,
								datCallback(MFA(cutsManager::BankLightValueChangedCallback),this) );

							m_pBank->PopGroup();


							m_pBank->PushGroup("Light Flags");
							m_pBank->AddToggle( "Over write light flags", &pEditLightInfo->bOverrideLightFlags,
								datCallback(MFA(cutsManager::BankLightOverrideFlagsChangedCallback),this) );	

							m_pBank->AddToggle( "Cast Static Object Shadow", &m_LightCastStaticObjectShadowFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) );

							m_pBank->AddToggle( "Cast Dynamic Object Shadow", &m_LightCastDynamicObjectShadowFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) );

							m_pBank->AddToggle( "Enable Buzzing", &m_LightEnableBuzzingFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) );

							m_pBank->AddToggle( "Force Buzzing", &m_LightForceBuzzingFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) );

							m_pBank->AddToggle( "Draw Volume", &m_LightDrawVolumeFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) );

							m_pBank->AddToggle( "No Specular", &m_lightNoSpecularFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) );

							m_pBank->AddToggle( "Corona Only", &m_LightCoronaOnlyFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) );

							m_pBank->AddToggle( "Interior And Exterior", &m_LightBothIntAndExtFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) );

							m_pBank->AddToggle( "Render not In Reflection", &m_LightNotInReflection,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) );

							m_pBank->AddToggle( "Render only In Reflection", &m_LightOnlyInReflection,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) ); 
						
							m_pBank->AddToggle( "Calculate From Sun", &m_LightCalcFromSunFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) ); 
						
							m_pBank->AddToggle( "Reflector", &m_LightReflector,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) ); 

							m_pBank->AddToggle( "Diffuser", &m_LightDiffuser,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) ); 
						
							m_pBank->AddToggle( "Calc by Ambient", &m_LightCalcByAmbientFlag,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) ); 
						
							m_pBank->AddToggle( "Dont Light Alpha", &m_LightFlagsDontLightAlpha,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) ); 

							m_pBank->AddToggle( "Use As Ped Light", &m_LightUseAsPedLight,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) ); 
						
							m_pBank->AddToggle( "Use Intensity As Multiplier", &m_UseIntensityAsMultiplier,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) ); 

							m_pBank->AddToggle( "Only Affects Peds", &m_IsPedOnlyLight,
								datCallback(MFA(cutsManager::BankUpdateLightFlagsCallback),this) ); 

							m_pBank->PopGroup();

							m_pBank->PushGroup("Hour Flags");
							m_pBank->AddToggle( "Over write light time flags", &pEditLightInfo->bOverrideTimeFlags,
								datCallback(MFA(cutsManager::BankLightOverrideTimeFlagsChangedCallback),this) );	

							m_pBank->AddToggle( "Hour 1", &m_LightHour1,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 2", &m_LightHour2,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 3", &m_LightHour3,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 4", &m_LightHour4,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 5", &m_LightHour5,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 6", &m_LightHour6,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 7", &m_LightHour7,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 8", &m_LightHour8,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 9", &m_LightHour9,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 10", &m_LightHour10,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 11", &m_LightHour11,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 12", &m_LightHour12,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 13", &m_LightHour13,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 14", &m_LightHour14,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 15", &m_LightHour15,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 16", &m_LightHour16,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 17", &m_LightHour17,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 18", &m_LightHour18,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 19", &m_LightHour19,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 20", &m_LightHour20,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 21", &m_LightHour21,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 22", &m_LightHour22,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 23", &m_LightHour23,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->AddToggle( "Hour 24", &m_LightHour24,
								datCallback(MFA(cutsManager::BankUpdateLightTimeFlagsCallback),this) );
							m_pBank->PopGroup();
							m_pBank->AddButton( "Reset", datCallback(MFA(cutsManager::BankResetSelectedLightCallback),this) );
						}
						else
						{
							atVarString info("%s not active. Active in shot: %s",  pLightObject->GetName().GetCStr(), pEditLightInfo->CamCutName.GetCStr()); 
							m_pBank->AddTitle(info.c_str()); 
						}
					}
				}
				m_pBank->UnSetCurrentGroup( *m_pEditLightGroup );
            }
        }
    }
}

void cutsManager::UpdateFlagsRagWidgets(bool &RagValue, u32 flags, u32 flag)
{
	if(IsFlagSet(flags, flag))
	{
		RagValue = true; 
	}
	else
	{
		RagValue = false; 
	}
}

void cutsManager::BankLightOverrideTimeFlagsChangedCallback()
{
	if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount() ) )
	{
		SEditCutfLightInfo *pEditLightInfo = GetSelectedLightInfo();
		
		if(!pEditLightInfo->bOverrideTimeFlags)
		{
			pEditLightInfo->TimeFlags = m_LightTimeFlags;
		}
		else
		{
			pEditLightInfo->bHaveEditedTimeFlags = true; 
		}
		UpdateLightEditInfo(); 

		UpdateRagForLightTimeFlags();
	}
}

void cutsManager::BankLightOverrideFlagsChangedCallback()
{
	if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount() ) )
	{
		SEditCutfLightInfo *pEditLightInfo = m_editLightList[m_iSelectedLightIndex - 1];

		if( !pEditLightInfo->bOverrideLightFlags)
		{
			pEditLightInfo->LightFlags = m_LightFlags;
		}
		else
		{
			pEditLightInfo->bHaveEditedFlags = true; 
		}
		UpdateLightEditInfo(); 

		UpdateRagForLightFlags();
	}
}

void cutsManager::BankLightOverrideChangedCallback()
{
    if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount() ) )
    {
        SEditCutfLightInfo *pEditLightInfo = GetSelectedLightInfo();

        // if we just switched off the override, remember what it was
        if ( !pEditLightInfo->bOverrideIntensity )
        {
            pEditLightInfo->fIntensity = m_fLightIntensity;
        }
		else
		{
			pEditLightInfo->bHaveEditedIntensity = true; 
		}

        if ( !pEditLightInfo->bOverrideFallOff )
        {
            pEditLightInfo->fFallOff = m_fLightFallOff;
        }
		else
		{
			pEditLightInfo->bHaveEditedFalloff = true; 
		}

        if ( !pEditLightInfo->bOverrideConeAngle )
        {
            pEditLightInfo->fConeAngle = m_fLightConeAngle;
        }
		else
		{
			pEditLightInfo->bHaveEditedConeAngle = true; 
		}

        if ( !pEditLightInfo->bOverrideColor )
        {
            pEditLightInfo->vColor = m_vLightColor;
        }
		else
		{
			pEditLightInfo->bHaveEditedColor = true; 
		}
        
		if ( !pEditLightInfo->bOverridePosition )
        {
			pEditLightInfo->AuthoringPos = pEditLightInfo->OriginaAuthoringPos; 
			pEditLightInfo->vPosition = pEditLightInfo->OriginaAuthoringPos; 
        }
		else
		{
			Matrix34 LocalAuthoringHelper; 
			Matrix34 UpdatedAuthoringMatWorld; 

			pEditLightInfo->AuthoringQuat.Normalize(); 
			LocalAuthoringHelper.FromQuaternion(pEditLightInfo->AuthoringQuat); 
			LocalAuthoringHelper.d = pEditLightInfo->AuthoringPos; 
			BankOriginalSceneSpaceToWorldSpace(LocalAuthoringHelper, UpdatedAuthoringMatWorld); 

			m_vLightPosition = UpdatedAuthoringMatWorld.d; 

			pEditLightInfo->bHaveEditedPosition = true; 
		}

        if ( !pEditLightInfo->bOverrideDirection )
        {
            pEditLightInfo->vDirection = m_vLightDirection;
			pEditLightInfo->AuthoringQuat = pEditLightInfo->OrginalAuthoringQuat; 
        }
		else
		{
			Matrix34 LocalAuthoringHelper; 
			Matrix34 UpdatedAuthoringMatWorld; 
			
			pEditLightInfo->AuthoringQuat.Normalize(); 
			LocalAuthoringHelper.FromQuaternion(pEditLightInfo->AuthoringQuat); 
			LocalAuthoringHelper.d = pEditLightInfo->AuthoringPos; 
			BankOriginalSceneSpaceToWorldSpace(LocalAuthoringHelper, UpdatedAuthoringMatWorld); 

			Vector3 orient; 
			UpdatedAuthoringMatWorld.ToEulersXYZ(orient); 
			m_vLightOrientation = orient * RtoD; 

			pEditLightInfo->bHaveEditedDirection = true; 
		}
			
		if(!pEditLightInfo->bOverrideCoronaIntensity)
		{
			pEditLightInfo->CoronaIntensity = m_fCoronaIntensity; 
		}
		else
		{
			pEditLightInfo->bHaveEditedCoronaIntensity = true; 
		}

		if(!pEditLightInfo->bOverrideCoronaSize)
		{
			pEditLightInfo->CoronaSize = m_fCoronaSize; 
		}
		else
		{
			pEditLightInfo->bHaveEditedCoronaSize = true; 
		}

		if(!pEditLightInfo->bOverrideCoronaZbias)
		{
			pEditLightInfo->CoronaZbias = m_fCoronaZBias; 
		}
		else
		{
			pEditLightInfo->bHaveEditedCoronaZBias = true; 
		}

		if(!pEditLightInfo->bOverrideInnerConeAngle)
		{
			pEditLightInfo->InnerConeAngle = m_fInnerConeAngle; 
		}
		else
		{
			pEditLightInfo->bHaveEditedInnerConeAngle = true;
		}

		if(!pEditLightInfo->bOverrideExpoFallOff)
		{
			pEditLightInfo->ExpoFallOff = m_fLightExponentFallOff; 
		}
		else
		{
			pEditLightInfo->bHaveEditedExpoFallOff = true; 
		}
		if(!pEditLightInfo->bOverrideOuterColourAndIntensity)
		{
			pEditLightInfo->OuterColourAndIntensity = m_vVolumeOuterColourAndIntensity; 
		}
		else
		{
			pEditLightInfo->bHaveEditedOuterColourAndIntensity = true; 
		}

		if(pEditLightInfo->bOverrideVolumeIntensity)
		{
			pEditLightInfo->bHaveEditedVolumeIntensity = true;
		}

		if(pEditLightInfo->bOverrideVolumeSizeScale)
		{
			pEditLightInfo->bHaveEditedVolumeSizeScale = true; 
		}

		if ( !pEditLightInfo->bOverrideShadowBlur )
		{
			pEditLightInfo->ShadowBlur = m_fShadowBlur;
		}
		else
		{
			pEditLightInfo->bHaveEditedShadowBlur = true; 
		}

      UpdateLightEditInfo(); 
	}
}

void cutsManager::BankLightDirectionChangedCallBack()
{
	if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount() ))
	{
		SEditCutfLightInfo *pEditLightInfo = GetSelectedLightInfo();

		if(pEditLightInfo->bOverrideDirection)
		{
			Matrix34 WorlightLightMat(M34_IDENTITY); 
			Vector3 orient = m_vLightOrientation * DtoR; 
			WorlightLightMat.FromEulersXYZ(orient); 

			Matrix34 SceneSpaceMat; 

			BankWorldSpaceToOriginalSceneSpace(WorlightLightMat, SceneSpaceMat); 

			SceneSpaceMat.ToQuaternion(pEditLightInfo->AuthoringQuat);
		}
	}
}

void cutsManager::BankLightPositionChangedCallBack()
{
	if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount() ) )
	{
		SEditCutfLightInfo *pEditLightInfo = GetSelectedLightInfo();
		if(pEditLightInfo->bOverridePosition)
		{
			Matrix34 WorlightLightMat(M34_IDENTITY); 
			WorlightLightMat.d = m_vLightPosition; 

			Matrix34 SceneSpaceMat; 

			BankWorldSpaceToOriginalSceneSpace(WorlightLightMat, SceneSpaceMat); 

			pEditLightInfo->AuthoringPos = SceneSpaceMat.d; 
		}
	}
}


void cutsManager::BankWorldSpaceToOriginalSceneSpace(const Matrix34& WorldMat, Matrix34& Local)
{
	//Matrix34 OriginalSceneMat(M34_IDENTITY);
	Matrix34 SceneMat(M34_IDENTITY); 
	Matrix34 LightMat(WorldMat); 

	//GetOriginalSceneOrientationMatrix(OriginalSceneMat);
	GetSceneOrientationMatrix(SceneMat); 

	LightMat.DotTranspose(SceneMat); 

	//LightMat.Dot(OriginalSceneMat); 

	//LightMat.DotTranspose(OriginalSceneMat);

	Local = LightMat; 
}

void cutsManager::BankOriginalSceneSpaceToWorldSpace(const Matrix34& LocalMat, Matrix34& World)
{
	//Matrix34 OriginalSceneMat(M34_IDENTITY);
	Matrix34 SceneMat(M34_IDENTITY); 
	Matrix34 LightMat(LocalMat); 

	//GetOriginalSceneOrientationMatrix(OriginalSceneMat);
	GetSceneOrientationMatrix(SceneMat); 
	
	//LightMat.Dot(OriginalSceneMat); 

	//LightMat.DotTranspose(OriginalSceneMat); 

	LightMat.Dot(SceneMat); 

	World = LightMat; 
}

void cutsManager::BankLightValueChangedCallback()
{
    if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount() ) )
    {
        SEditCutfLightInfo *pEditLightInfo = GetSelectedLightInfo();

        // save the new values

        if ( pEditLightInfo->bOverrideIntensity )
        {
            pEditLightInfo->fIntensity = m_fLightIntensity;
        }

		if ( pEditLightInfo->bOverrideShadowBlur )
		{
			pEditLightInfo->ShadowBlur = m_fShadowBlur;
		}

        if ( pEditLightInfo->bOverrideFallOff )
        {
            pEditLightInfo->fFallOff = m_fLightFallOff;
        }

        if ( pEditLightInfo->bOverrideConeAngle )
        {
            pEditLightInfo->fConeAngle = m_fLightConeAngle;
        }

        if ( pEditLightInfo->bOverrideColor )
        {
            pEditLightInfo->vColor = m_vLightColor;
        }

		if(pEditLightInfo->bOverrideVolumeIntensity)
		{
			 pEditLightInfo->VolumeIntensity = m_fVolumeIntensity;
		}
		
		if(pEditLightInfo->bOverrideVolumeSizeScale)
		{
			pEditLightInfo->VolumeSizeScale = m_fVolumeSizeScale;
		}

		if(pEditLightInfo->bOverrideCoronaIntensity)
		{
			pEditLightInfo->CoronaIntensity = m_fCoronaIntensity;
		}
		
		if(pEditLightInfo->bOverrideCoronaSize)
		{
			pEditLightInfo->CoronaSize = m_fCoronaSize;
		}

		if(pEditLightInfo->bOverrideCoronaZbias)
		{
			pEditLightInfo->CoronaZbias = m_fCoronaZBias;
		}
		
		if(pEditLightInfo->bOverrideExpoFallOff)
		{
			pEditLightInfo->ExpoFallOff = m_fLightExponentFallOff; 
		}
		
		if(pEditLightInfo->bOverrideInnerConeAngle)
		{
			pEditLightInfo->InnerConeAngle = m_fInnerConeAngle; 
		}

		if(pEditLightInfo->bOverrideOuterColourAndIntensity)
		{
			pEditLightInfo->OuterColourAndIntensity = m_vVolumeOuterColourAndIntensity; 
		}
    }
}

void cutsManager::UpdateDebugFlag(u32& LightInfoFlag, u32 CutsceneFlagToChange, bool RagLightFlag, u32& flags )
{
	if(RagLightFlag)
	{
		if(!IsFlagSet(flags, CutsceneFlagToChange))
		{
			flags |= CutsceneFlagToChange; 
			LightInfoFlag = flags; 
		}
	}
	else
	{
		if(IsFlagSet(flags, CutsceneFlagToChange))
		{
			ClearFlag(flags, CutsceneFlagToChange); 
			LightInfoFlag = flags; 
		}
	}
}

//populate rag correctly
void cutsManager::UpdateLightEditInfo()
{
	if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount() ) )
	{
		SEditCutfLightInfo *pEditLightInfo = GetSelectedLightInfo();

		if(pEditLightInfo)
		{
			m_fLightIntensity = pEditLightInfo->bOverrideIntensity ? pEditLightInfo->fIntensity : pEditLightInfo->fOriginalIntensity;
			m_fLightFallOff = pEditLightInfo->bOverrideFallOff ? pEditLightInfo->fFallOff : pEditLightInfo->fOriginalFallOff;
			m_fLightConeAngle = pEditLightInfo->bOverrideConeAngle ? pEditLightInfo->fConeAngle : pEditLightInfo->fOriginalConeAngle;
			m_vLightColor = pEditLightInfo->bOverrideColor ? pEditLightInfo->vColor : pEditLightInfo->vOriginalColor;

			Vector3 LightPos = pEditLightInfo->bOverrideDirection ? pEditLightInfo->vDirection : pEditLightInfo->vOriginalDirection;

			Matrix34 Mat;
			GetSceneOrientationMatrix(Mat); 
			Mat.Transform(LightPos, m_vLightPosition); 

			m_vLightDirection = pEditLightInfo->bOverrideDirection ? pEditLightInfo->vDirection : pEditLightInfo->vOriginalDirection;
			m_LightFlags = pEditLightInfo->bOverrideLightFlags ? pEditLightInfo->LightFlags : pEditLightInfo->OriginalLightFlags; 
			m_LightTimeFlags = pEditLightInfo->bOverrideTimeFlags ? pEditLightInfo->TimeFlags : pEditLightInfo->OriginalTimeFlags; 
			m_fVolumeIntensity =  pEditLightInfo->bOverrideVolumeIntensity ? pEditLightInfo->VolumeIntensity : pEditLightInfo->OriginalVolumeIntensity; 
			m_fVolumeSizeScale = pEditLightInfo->bOverrideVolumeSizeScale ? pEditLightInfo->VolumeSizeScale : pEditLightInfo->OriginalVolumeSizeScale;
			m_fCoronaIntensity = pEditLightInfo->bOverrideCoronaIntensity ? pEditLightInfo->CoronaIntensity : pEditLightInfo->OriginalCoronaIntensity;
			m_fCoronaSize = pEditLightInfo->bOverrideCoronaSize ? pEditLightInfo->CoronaSize : pEditLightInfo->OriginalCoronaSize;
			m_fCoronaZBias = pEditLightInfo->bOverrideCoronaZbias ? pEditLightInfo->CoronaZbias : pEditLightInfo->OriginalCoronaZbias;
			m_fInnerConeAngle = pEditLightInfo->bOverrideInnerConeAngle ? pEditLightInfo->InnerConeAngle : pEditLightInfo->OriginalInnerConeAngle;
			m_fLightExponentFallOff = pEditLightInfo->bOverrideExpoFallOff ? pEditLightInfo->ExpoFallOff : pEditLightInfo->OriginalExpoFallOff;
			m_fShadowBlur = pEditLightInfo->bOverrideShadowBlur ? pEditLightInfo->ShadowBlur : pEditLightInfo->OriginalShadowBlur;
			m_vVolumeOuterColourAndIntensity = pEditLightInfo->bOverrideOuterColourAndIntensity ? pEditLightInfo->OuterColourAndIntensity : pEditLightInfo->OriginalOuterColourAndIntensity;
	
		}
	}
}

void cutsManager::BankUpdateLightFlagsCallback()
{
	if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount() ) )
	{
		SEditCutfLightInfo *pEditLightInfo = m_editLightList[m_iSelectedLightIndex - 1];
		
		if(pEditLightInfo->bOverrideLightFlags)
		{
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_CAST_STATIC_GEOM_SHADOWS, m_LightCastStaticObjectShadowFlag, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_CAST_DYNAMIC_GEOM_SHADOWS, m_LightCastDynamicObjectShadowFlag, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_CALC_FROM_SUN, m_LightCalcFromSunFlag, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_ENABLE_BUZZING, m_LightEnableBuzzingFlag, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_FORCE_BUZZING, m_LightForceBuzzingFlag, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_DRAW_VOLUME, m_LightDrawVolumeFlag, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_NO_SPECULAR, m_lightNoSpecularFlag, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_CORONA_ONLY, m_LightCoronaOnlyFlag, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_BOTH_INTERIOR_AND_EXTERIOR, m_LightBothIntAndExtFlag, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_NOT_IN_REFLECTION, m_LightNotInReflection, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_ONLY_IN_REFLECTION, m_LightOnlyInReflection, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_REFLECTOR, m_LightReflector, m_LightFlags); 
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_DIFFUSER, m_LightDiffuser, m_LightFlags);
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_ADD_AMBIENT_LIGHT, m_LightCalcByAmbientFlag, m_LightFlags);
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_DONT_LIGHT_ALPHA, m_LightFlagsDontLightAlpha, m_LightFlags);
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_IS_CHARACTER_LIGHT, m_LightUseAsPedLight, m_LightFlags);
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_CHARACTER_LIGHT_INTENSITY_AS_MULTIPLIER, m_UseIntensityAsMultiplier, m_LightFlags);
			UpdateDebugFlag(pEditLightInfo->LightFlags, CUTSCENE_LIGHTFLAG_IS_PED_ONLY_LIGHT, m_IsPedOnlyLight, m_LightFlags);
		}
	}
}

void cutsManager::BankUpdateLightTimeFlagsCallback()
{
	if ( (m_iSelectedLightIndex > 0) && (m_iSelectedLightIndex <= m_editLightList.GetCount() ) )
	{
		SEditCutfLightInfo *pEditLightInfo = GetSelectedLightInfo();

		if(pEditLightInfo && pEditLightInfo->bOverrideTimeFlags)
		{
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT0, m_LightHour1, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT1, m_LightHour2, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT2, m_LightHour3, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT3, m_LightHour4, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT4, m_LightHour5, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT5, m_LightHour6, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT6, m_LightHour7, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT7, m_LightHour8, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT8, m_LightHour9, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT9, m_LightHour10, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT10, m_LightHour11, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT11, m_LightHour12, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT12, m_LightHour13, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT13, m_LightHour14, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT14, m_LightHour15, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT15, m_LightHour16, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT16, m_LightHour17, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT17, m_LightHour18, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT18, m_LightHour19, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT19, m_LightHour20, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT20, m_LightHour21, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT21, m_LightHour22, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT22, m_LightHour23, m_LightTimeFlags);
			UpdateDebugFlag(pEditLightInfo->TimeFlags, BIT23, m_LightHour24, m_LightTimeFlags);
		}
	}
}

void cutsManager::BankResetSelectedLightCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
		SEditCutfLightInfo *pEditLightInfo = GetSelectedLightInfo();

		if(pEditLightInfo)
		{
			pEditLightInfo->fIntensity = pEditLightInfo->fOriginalIntensity;
			pEditLightInfo->fFallOff = pEditLightInfo->fOriginalFallOff;
			pEditLightInfo->fConeAngle = pEditLightInfo->fOriginalConeAngle;
			pEditLightInfo->vColor = pEditLightInfo->vOriginalColor;
			pEditLightInfo->vPosition = pEditLightInfo->vOriginalPosition;
			pEditLightInfo->vDirection = pEditLightInfo->vOriginalDirection;
		}
    }
}

void cutsManager::BankResetAllDrawDistancesCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        for ( int i = 0; i < m_editDrawDistanceList.GetCount(); ++i )
        {
            m_editDrawDistanceList[i]->fNearClip = m_editDrawDistanceList[i]->fOriginalNearClip;
            m_editDrawDistanceList[i]->bOverrideNearClip = true;

            m_editDrawDistanceList[i]->fFarClip = m_editDrawDistanceList[i]->fOriginalFarClip;
            m_editDrawDistanceList[i]->bOverrideFarClip = true;
        }

        if ( (m_iSelectedDrawDistanceIndex > 0) && (m_iSelectedDrawDistanceIndex <= m_editDrawDistanceList.GetCount()) )
        {
            SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = m_editDrawDistanceList[m_iSelectedDrawDistanceIndex - 1];

            m_fDrawDistanceNearClip = pEditDrawDistanceInfo->fNearClip;
            m_fDrawDistanceFarClip = pEditDrawDistanceInfo->fFarClip;

            DispatchSetDrawDistanceEvent( m_iSelectedDrawDistanceIndex - 1 );   // subtract 1 because index 0 = "none"
        }
    }
}

void cutsManager::BankSetToDefaultAllDrawDistancesCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        for ( int i = 0; i < m_editDrawDistanceList.GetCount(); ++i )
        {
            m_editDrawDistanceList[i]->fNearClip = GetDefaultNearDrawDistance();
            m_editDrawDistanceList[i]->bOverrideNearClip = true;

            m_editDrawDistanceList[i]->fFarClip = GetDefaultFarDrawDistance();
            m_editDrawDistanceList[i]->bOverrideFarClip = true;
        }

        if ( (m_iSelectedDrawDistanceIndex > 0) && (m_iSelectedDrawDistanceIndex <= m_editDrawDistanceList.GetCount()) )
        {
            SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = m_editDrawDistanceList[m_iSelectedDrawDistanceIndex - 1];

            m_fDrawDistanceNearClip = pEditDrawDistanceInfo->fNearClip;
            m_fDrawDistanceFarClip = pEditDrawDistanceInfo->fFarClip;

            DispatchSetDrawDistanceEvent( m_iSelectedDrawDistanceIndex - 1 );   // subtract 1 because index 0 = "none"
        }
    }
}

void cutsManager::BankSetToUseTimecycleAllDrawDistancesCallback()
{
	if ( IsPaused() || IsPlaying() )
	{
		for ( int i = 0; i < m_editDrawDistanceList.GetCount(); ++i )
		{
			m_editDrawDistanceList[i]->fNearClip = -1.0f;
			m_editDrawDistanceList[i]->bOverrideNearClip = true;

			m_editDrawDistanceList[i]->fFarClip = -1.0f;
			m_editDrawDistanceList[i]->bOverrideFarClip = true;
		}

		if ( (m_iSelectedDrawDistanceIndex > 0) && (m_iSelectedDrawDistanceIndex <= m_editDrawDistanceList.GetCount()) )
		{
			SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = m_editDrawDistanceList[m_iSelectedDrawDistanceIndex - 1];

			m_fDrawDistanceNearClip = pEditDrawDistanceInfo->fNearClip;
			m_fDrawDistanceFarClip = pEditDrawDistanceInfo->fFarClip;

			DispatchSetDrawDistanceEvent( m_iSelectedDrawDistanceIndex - 1 );   // subtract 1 because index 0 = "none"
		}
	}
}


void cutsManager::BankSaveDrawDistancesCallback()
{
    if ( (IsPaused() || IsPlaying()) && (m_pCameraObject != NULL) )
    {
        SaveDrawDistanceFromWidget(); 
    }
}

void cutsManager::SaveDrawDistanceFromWidget()
{
	SaveDrawDistances();
	SaveCutfile();
}


void cutsManager::BankDrawDistanceSelectedCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        RAGE_TRACK( cutsManager_BankDrawDistanceSelectedCallback );

        if ( (m_pEditDrawDistanceGroup != NULL) && (m_pBank != NULL) )
        {
            while ( m_pEditDrawDistanceGroup->GetChild() )
            {
                m_pBank->Remove( *(m_pEditDrawDistanceGroup->GetChild()) );
            }

            if ( (m_iSelectedDrawDistanceIndex > 0) && (m_iSelectedDrawDistanceIndex <= m_editDrawDistanceList.GetCount()) )
            {
                m_pBank->SetCurrentGroup( *m_pEditDrawDistanceGroup );
                {
                    SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = m_editDrawDistanceList[m_iSelectedDrawDistanceIndex - 1];

                    m_fDrawDistanceNearClip = pEditDrawDistanceInfo->bOverrideNearClip 
                        ? pEditDrawDistanceInfo->fNearClip : pEditDrawDistanceInfo->fOriginalNearClip;
                    m_fDrawDistanceFarClip = pEditDrawDistanceInfo->bOverrideFarClip 
                        ? pEditDrawDistanceInfo->fFarClip : pEditDrawDistanceInfo->fOriginalFarClip;
                    
                    m_pBank->AddToggle( "Override Near Distance", &pEditDrawDistanceInfo->bOverrideNearClip,
                        datCallback(MFA(cutsManager::BankDrawDistanceOverrideChangedCallback),this) );                    
                    m_pEditDrawDistanceNearClipSlider = dynamic_cast<bkSlider *>( m_pBank->AddSlider( "Near Distance", 
                        &m_fDrawDistanceNearClip, -1.0f, 5000.0f, 0.1f, 
                        datCallback(MFA(cutsManager::BankDrawDistanceValueChangedCallback),this) ) );
                    
                    m_pBank->AddToggle( "Override Far Distance", &pEditDrawDistanceInfo->bOverrideFarClip,
                        datCallback(MFA(cutsManager::BankDrawDistanceOverrideChangedCallback),this) );
                    m_pEditDrawDistanceFarClipSlider = dynamic_cast<bkSlider *>( m_pBank->AddSlider( "Far Distance", 
                        &m_fDrawDistanceFarClip, -1.0f, 5000.0f, 0.1f, 
                        datCallback(MFA(cutsManager::BankDrawDistanceValueChangedCallback),this) ) );
                    
					m_pBank->AddButton( "Use Time cycle values", datCallback(MFA(cutsManager::BankUseTimeCycleValuesCallback),this), "Sets the draw distance values to -1  so we use the timcycle defaults" );
                  
					m_pBank->AddButton( "Reset", datCallback(MFA(cutsManager::BankResetSelectedDrawDistanceCallback),this), "Resets the override values back to the originals." );
                    m_pBank->AddButton( "Set to Defaults", datCallback(MFA(cutsManager::BankSetToDefaultDrawDistancesCallback),this), "Sets the override values to the game-specific defaults." );
                }
                m_pBank->UnSetCurrentGroup( *m_pEditDrawDistanceGroup );
            }
        }
    }
}

void cutsManager::BankDrawDistanceOverrideChangedCallback()
{
    if ( (m_iSelectedDrawDistanceIndex > 0) && (m_iSelectedDrawDistanceIndex <= m_editDrawDistanceList.GetCount()) )
    {
        SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = m_editDrawDistanceList[m_iSelectedDrawDistanceIndex - 1];

        // if we just switched off an override, remember what it was
        if ( !pEditDrawDistanceInfo->bOverrideNearClip )
        {
            pEditDrawDistanceInfo->fNearClip = m_fDrawDistanceNearClip;
        }

        if ( !pEditDrawDistanceInfo->bOverrideFarClip )
        {
            pEditDrawDistanceInfo->fFarClip = m_fDrawDistanceFarClip;
        }

        m_fDrawDistanceNearClip = pEditDrawDistanceInfo->bOverrideNearClip ? pEditDrawDistanceInfo->fNearClip : pEditDrawDistanceInfo->fOriginalNearClip;
        m_fDrawDistanceFarClip = pEditDrawDistanceInfo->bOverrideFarClip ? pEditDrawDistanceInfo->fFarClip : pEditDrawDistanceInfo->fOriginalFarClip;

        DispatchSetDrawDistanceEvent( m_iSelectedDrawDistanceIndex - 1 );   // subtract 1 because index 0 = "none"
    }
}

void cutsManager::BankDrawDistanceValueChangedCallback()
{
    if ( (m_iSelectedDrawDistanceIndex > 0) && (m_iSelectedDrawDistanceIndex <= m_editDrawDistanceList.GetCount()) )
    {
        SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = m_editDrawDistanceList[m_iSelectedDrawDistanceIndex - 1];

        // save the new values
        if ( pEditDrawDistanceInfo->bOverrideNearClip )
        {
            pEditDrawDistanceInfo->fNearClip = m_fDrawDistanceNearClip;
        }

        if ( pEditDrawDistanceInfo->bOverrideFarClip )
        {
            pEditDrawDistanceInfo->fFarClip = m_fDrawDistanceFarClip;
        }

        DispatchSetDrawDistanceEvent( m_iSelectedDrawDistanceIndex - 1 );   // subtract 1 because index 0 = "none"
    }
}

void cutsManager::BankUseTimeCycleValuesCallback()
{
	if ( (m_iSelectedDrawDistanceIndex > 0) && (m_iSelectedDrawDistanceIndex <= m_editDrawDistanceList.GetCount()) )
	{
		SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = m_editDrawDistanceList[m_iSelectedDrawDistanceIndex - 1];

		// save the new values
		m_fDrawDistanceNearClip = -1.0f; 
		m_fDrawDistanceFarClip = -1.0f; 

		pEditDrawDistanceInfo->fNearClip = m_fDrawDistanceNearClip;
		
		pEditDrawDistanceInfo->fFarClip = m_fDrawDistanceFarClip;
		
		DispatchSetDrawDistanceEvent( m_iSelectedDrawDistanceIndex - 1 );   // subtract 1 because index 0 = "none"
	}

}
void cutsManager::BankResetSelectedDrawDistanceCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        if ( (m_iSelectedDrawDistanceIndex > 0) && (m_iSelectedDrawDistanceIndex <= m_editDrawDistanceList.GetCount()) )
        {
            SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = m_editDrawDistanceList[m_iSelectedDrawDistanceIndex - 1];
            
            pEditDrawDistanceInfo->fNearClip = pEditDrawDistanceInfo->fOriginalNearClip;
            pEditDrawDistanceInfo->bOverrideNearClip = true;

            pEditDrawDistanceInfo->fFarClip = pEditDrawDistanceInfo->fOriginalFarClip;
            pEditDrawDistanceInfo->bOverrideFarClip = true;

            m_fDrawDistanceNearClip = pEditDrawDistanceInfo->fNearClip;
            m_fDrawDistanceFarClip = pEditDrawDistanceInfo->fFarClip;

            DispatchSetDrawDistanceEvent( m_iSelectedDrawDistanceIndex - 1 );   // subtract 1 because index 0 = "none"
        }
    }
}

void cutsManager::BankSetToDefaultDrawDistancesCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        if ( (m_iSelectedDrawDistanceIndex > 0) && (m_iSelectedDrawDistanceIndex <= m_editDrawDistanceList.GetCount()) )
        {
            SEditCutfDrawDistanceInfo *pEditDrawDistanceInfo = m_editDrawDistanceList[m_iSelectedDrawDistanceIndex - 1];

            pEditDrawDistanceInfo->fNearClip = GetDefaultNearDrawDistance();
            pEditDrawDistanceInfo->bOverrideNearClip = true;

            pEditDrawDistanceInfo->fFarClip = GetDefaultFarDrawDistance();
            pEditDrawDistanceInfo->bOverrideFarClip = true;

            m_fDrawDistanceNearClip = pEditDrawDistanceInfo->fNearClip;
            m_fDrawDistanceFarClip = pEditDrawDistanceInfo->fFarClip;

            DispatchSetDrawDistanceEvent( m_iSelectedDrawDistanceIndex - 1 );   // subtract 1 because index 0 = "none"
        }
    }
}

void cutsManager::BankAddHiddenObjectCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        // get a new hidden object info
        SEditCutfObjectLocationInfo *pObjectLocationInfo = GetHiddenObjectInfo();
        if ( pObjectLocationInfo != NULL )
        {
           if(AddHiddenObject( pObjectLocationInfo ))
		   {
				//added a hidden object to the list, lets add a hide event 
				SaveHiddenObjects();
				BankPopulateHiddenObjectEventList(pObjectLocationInfo->pObject); 
				SaveCutfile();
		   }
        }
    }
}

void cutsManager::BankRemoveHiddenObjectCallback()
{
    if ( (IsPaused() || IsPlaying()) && (m_iSelectedHiddenObjectIndex > 0) && (m_iSelectedHiddenObjectIndex <= m_editHiddenObjectList.GetCount()) )
    {
        RemoveHiddenObject( m_editHiddenObjectList[m_iSelectedHiddenObjectIndex - 1] );
		//have removed an object lets remove it from the cut file. 
		SaveHiddenObjects();
    }
}

/////////////////////////////////////////////////////////////////////////////////////
//Update the ped variation event list for a new ped or new events. 

void cutsManager::BankPopulateHiddenObjectEventList(const cutfObject* pObject)
{
	if (m_pEditHiddenObjectEvents == NULL)
	{
		return; 
	}
	//Nothing selected
	if(!pObject)
	{
		m_pEditHiddenObjectEvents->UpdateCombo( "Hidden var events", &m_iSelectedHiddenObjectEventIndex, 1, NULL );
		m_pEditHiddenObjectEvents->SetString( 0 , "none");

		return; 
	}

	atArray<cutfEvent *> EventList;
	GetCutfFile()->FindEventsForObjectIdOnly( pObject->GetObjectId(), GetCutfFile()->GetEventList(), EventList );

	//look to edit a current event 
	if (EventList.GetCount() == 0)
	{
		m_pEditHiddenObjectEvents->SetString( 0 , "none");
	}
	else
	{
		if (EventList.GetCount() > 0)
		{
			m_pEditHiddenObjectEvents->UpdateCombo( "Hidden var events", &m_iSelectedHiddenObjectEventIndex, EventList.GetCount(), NULL );

			//Clear the list of events, need to repopulate. 
			m_HiddenObjectEvents.Reset();  
			
			for ( int i = 0; i < EventList.GetCount(); ++i )
			{
				if (EventList[i]->GetEventId() == CUTSCENE_HIDE_OBJECTS_EVENT || EventList[i]->GetEventId() == CUTSCENE_SHOW_OBJECTS_EVENT )
				{
					cutfEventArgs *pEventArgs = const_cast<cutfEventArgs *>( EventList[i]->GetEventArgs() );

					if(pEventArgs->GetType() == CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE)
					{						
						char EventInfo[64]; 	

						u32 FrameNumber = u32(EventList[i]->GetTime() * CUTSCENE_FPS); 

						if(EventList[i]->GetEventId() == CUTSCENE_HIDE_OBJECTS_EVENT )
						{
							formatf(EventInfo, sizeof(EventInfo)-1,"Hide Event %d: Frame: %d , time: %f", i , FrameNumber, EventList[i]->GetTime());
						}
						else
						{
							formatf(EventInfo, sizeof(EventInfo)-1,"Show Event %d: Frame: %d , time: %f", i , FrameNumber, EventList[i]->GetTime());
						}

						
						m_pEditHiddenObjectEvents->SetString( i, EventInfo );

						//Store a list of pointers to our objects
						m_HiddenObjectEvents.PushAndGrow(EventList[i]); 
					}
				}
			}
		}
	}
}

void cutsManager::BankHiddenObjectSelectedCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        RAGE_TRACK( cutsManager_BankHiddenObjectSelectedCallback );

        if ( (m_pEditHiddenObjectGroup != NULL) && (m_pBank != NULL) )
        {
            while ( m_pEditHiddenObjectGroup->GetChild() )
            {
                m_pBank->Remove( *(m_pEditHiddenObjectGroup->GetChild()) );
            }
            
            if ( (m_iSelectedHiddenObjectIndex > 0) && (m_iSelectedHiddenObjectIndex <= m_editHiddenObjectList.GetCount()) )
            {
                m_pBank->SetCurrentGroup( *m_pEditHiddenObjectGroup );
                {
                    SEditCutfObjectLocationInfo *pObjectLocationInfo = m_editHiddenObjectList[m_iSelectedHiddenObjectIndex - 1];

                    m_pBank->AddText( "Name", pObjectLocationInfo->cName, sizeof(pObjectLocationInfo->cName) );
                    m_pBank->AddVector( "Search Position", &pObjectLocationInfo->vPosition, -100000.0f, 100000.0f, 0.1f );
                    m_pBank->AddSlider( "Search Radius", &pObjectLocationInfo->fRadius, 0.0f, 10.0f, 0.1f );		
	
					const char *EventList[] = { "(none)" };
					m_pEditHiddenObjectEvents = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Hidden Object Events", &m_iSelectedHiddenObjectEventIndex, 1, EventList ) );
					//add an event list for the object 
					BankPopulateHiddenObjectEventList(pObjectLocationInfo->pObject);
					m_pBank->AddButton("Add Show Event",datCallback(MFA(cutsManager::BankAddHiddenShowEvent),this)  );
					m_pBank->AddButton("Add Hide Event",datCallback(MFA(cutsManager::BankAddHiddenHideEvent),this) );
					m_pBank->AddButton("Remove Event", datCallback(MFA(cutsManager::BankHiddenRemoveEvent),this));

                }
                m_pBank->UnSetCurrentGroup( *m_pEditHiddenObjectGroup );
            }
        }
    }
}

void cutsManager::BankAddHiddenShowEvent()
{
	SEditCutfObjectLocationInfo *pObjectLocationInfo = m_editHiddenObjectList[m_iSelectedHiddenObjectIndex - 1];
	
	const cutfObject* pObject = pObjectLocationInfo->pObject; 

	if(pObject)
	{
		bool bAddEvent = true; 
		
		atArray<cutfEvent*>HiddenObjectEvents; 
		GetCutfFile()->FindEventsForObjectIdOnly( pObject->GetObjectId(), GetCutfFile()->GetEventList(), HiddenObjectEvents );
		
#if	CUTSCENE_EXPORT_EVENTS_WITH_ZERO_BASE_TIME
		float EventTime = GetCutSceneTimeWithRangeOffset();  
#else
		float EventTime = GetTime(); 
#endif

		for(int i=0; i < HiddenObjectEvents.GetCount(); i++)
		{
			if(HiddenObjectEvents[i]->GetTime() == EventTime )
			{
				bAddEvent = false;
			}
		}

		cutsAssertf(bAddEvent, "Can't add show event, as there is already an event on this frame.  Remove that first and try again (you may need to add another event on another frame first to be able to remove it)");
	
		if(bAddEvent)
		{
			GetCutfFile()->AddEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), EventTime,
				CUTSCENE_SHOW_OBJECTS_EVENT, rage_new cutfObjectIdEventArgs( pObject->GetObjectId() ) ) );
			BankPopulateHiddenObjectEventList(pObject); 
			GetCutfFile()->SortEvents(); 
			SaveCutfile(); 
		}
	}
}


void cutsManager::BankAddHiddenHideEvent()
{
	SEditCutfObjectLocationInfo *pObjectLocationInfo = m_editHiddenObjectList[m_iSelectedHiddenObjectIndex - 1];

	const cutfObject* pObject = pObjectLocationInfo->pObject; 

	if(pObject)
	{
		bool bAddEvent = true; 

		atArray<cutfEvent*>HiddenObjectEvents; 
		GetCutfFile()->FindEventsForObjectIdOnly( pObject->GetObjectId(), GetCutfFile()->GetEventList(), HiddenObjectEvents );
		
#if	CUTSCENE_EXPORT_EVENTS_WITH_ZERO_BASE_TIME
		float EventTime = GetCutSceneTimeWithRangeOffset();  
#else
		float EventTime = GetTime(); 
#endif

		for(int i=0; i < HiddenObjectEvents.GetCount(); i++)
		{
			if(HiddenObjectEvents[i]->GetTime() == EventTime )
			{
				bAddEvent = false; 			
			}
		}
		
		cutsAssertf(bAddEvent, "Can't add hide event, as there is already an event on this frame.  Remove that first and try again (you may need to add another event on another frame first to be able to remove it)");

		if(bAddEvent)
		{
			GetCutfFile()->AddEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), EventTime,
				CUTSCENE_HIDE_OBJECTS_EVENT, rage_new cutfObjectIdEventArgs( pObject->GetObjectId() ) ) );
			GetCutfFile()->SortEvents(); 
			BankPopulateHiddenObjectEventList(pObject); 
			SaveCutfile(); 
		}
	}
}

void cutsManager::BankHiddenRemoveEvent()
{
	if (m_HiddenObjectEvents.GetCount() > 1)
	{
		if(m_HiddenObjectEvents[m_iSelectedHiddenObjectEventIndex])
		{
			SEditCutfObjectLocationInfo *pObjectLocationInfo = m_editHiddenObjectList[m_iSelectedHiddenObjectIndex - 1];
			GetCutfFile()->RemoveEvent(m_HiddenObjectEvents[m_iSelectedHiddenObjectEventIndex]);
			BankPopulateHiddenObjectEventList(pObjectLocationInfo->pObject); 
			SaveCutfile(); 
		}
	}
	else
	{
		cutfAssertf(0, "An object must have at least one event, add the correct event and remove the undesierd event, or remove the object from the list"); 
	}
}


void cutsManager::BankAddFixupObjectCallback()
{
	if ( IsPaused() || IsPlaying() )
    {
        // get a new hidden object info
        SEditCutfObjectLocationInfo *pObjectLocationInfo = GetFixupObjectInfo();
        if ( pObjectLocationInfo != NULL )
        {
            if(AddFixupObject( pObjectLocationInfo ))
			{
				//added a hidden object to the list, lets add a hide event 
				SaveFixupObjects();
				BankPopulateFixupObjectEventList(pObjectLocationInfo->pObject); 
				
			}
        }
    }
}

void cutsManager::BankRemoveFixupObjectCallback()
{
    if ( (IsPaused() || IsPlaying()) && (m_iSelectedFixupObjectIndex > 0) && (m_iSelectedFixupObjectIndex <= m_editFixupObjectList.GetCount()) )
    {
        RemoveFixupObject( m_editFixupObjectList[m_iSelectedFixupObjectIndex - 1] );
		//have removed an object lets remove it from the cut file. 
		SaveFixupObjects();
    }
}

void cutsManager::BankFixupObjectSelectedCallback()
{
    if ( IsPaused() || IsPlaying() )
    {
        RAGE_TRACK( cutsManager_BankFixupObjectSelectedCallback );

        if ( (m_pEditFixupObjectGroup != NULL) && (m_pBank != NULL) )
        {
            while ( m_pEditFixupObjectGroup->GetChild() )
            {
                m_pBank->Remove( *(m_pEditFixupObjectGroup->GetChild()) );
            }

            if ( (m_iSelectedFixupObjectIndex > 0) && (m_iSelectedFixupObjectIndex <= m_editFixupObjectList.GetCount()) )
            {
                m_pBank->SetCurrentGroup( *m_pEditFixupObjectGroup );
                {
                    SEditCutfObjectLocationInfo *pObjectLocationInfo = m_editFixupObjectList[m_iSelectedFixupObjectIndex - 1];

                    m_pBank->AddText( "Name", pObjectLocationInfo->cName, sizeof(pObjectLocationInfo->cName) );
                    m_pBank->AddVector( "Search Position", &pObjectLocationInfo->vPosition, -100000.0f, 100000.0f, 0.1f );
                    m_pBank->AddSlider( "Search Radius", &pObjectLocationInfo->fRadius, 0.0f, 10.0f, 0.1f );
					
					const char *EventList[] = { "(none)" };
					m_pEditFixupObjectEvents = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Fixup Object Events", &m_iSelectedFixupObjectEventIndex, 1, EventList ) );
					//add an event list for the object 
					BankPopulateFixupObjectEventList(pObjectLocationInfo->pObject);
					m_pBank->AddButton("Add Fixup Event",datCallback(MFA(cutsManager::BankAddFixupEvent),this)  );
					m_pBank->AddButton("Remove Event", datCallback(MFA(cutsManager::BankRemoveFixupEvent),this));
	
                }
                m_pBank->UnSetCurrentGroup( *m_pEditFixupObjectGroup );
            }
        }
    }
}

void cutsManager::BankPopulateFixupObjectEventList(const cutfObject* pObject)
{
	if (m_pEditFixupObjectEvents == NULL)
	{
		return; 
	}
	//Nothing selected
	if(!pObject)
	{
		m_pEditFixupObjectEvents->UpdateCombo( "Fixup var events", &m_iSelectedFixupObjectEventIndex, 1, NULL );
		m_pEditFixupObjectEvents->SetString( 0 , "none");

		return; 
	}

	atArray<cutfEvent *> EventList;
	GetCutfFile()->FindEventsForObjectIdOnly( pObject->GetObjectId(), GetCutfFile()->GetEventList(), EventList );

	//look to edit a current event 
	if (EventList.GetCount() == 0)
	{
		m_pEditFixupObjectEvents->SetString( 0 , "none");
	}
	else
	{
		if (EventList.GetCount() > 0)
		{
			m_pEditFixupObjectEvents->UpdateCombo( "Fixup var events", &m_iSelectedFixupObjectEventIndex, EventList.GetCount(), NULL );

			//Clear the list of events, need to repopulate. 
			m_FixupObjectEvents.Reset();  

			for ( int i = 0; i < EventList.GetCount(); ++i )
			{
				if (EventList[i]->GetEventId() == CUTSCENE_FIXUP_OBJECTS_EVENT)
				{
					cutfEventArgs *pEventArgs = const_cast<cutfEventArgs *>( EventList[i]->GetEventArgs() );

					if(pEventArgs->GetType() == CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE)
					{						
						char EventInfo[64]; 	

						u32 FrameNumber = u32(EventList[i]->GetTime() * CUTSCENE_FPS); 
						
						formatf(EventInfo, sizeof(EventInfo)-1,"Fixup Event %d: Frame: %d , time: %f", i , FrameNumber, EventList[i]->GetTime());
					
						m_pEditFixupObjectEvents->SetString( i, EventInfo );

						//Store a list of pointers to our objects
						m_FixupObjectEvents.PushAndGrow(EventList[i]); 
					}
				}
			}
		}
	}
}

void cutsManager::BankAddFixupEvent()
{
	SEditCutfObjectLocationInfo *pObjectLocationInfo = m_editFixupObjectList[m_iSelectedFixupObjectIndex - 1];

	const cutfObject* pObject = pObjectLocationInfo->pObject; 

	if(pObject)
	{
		bool bAddEvent = true; 

		atArray<cutfEvent*>ObjectEvents; 
		GetCutfFile()->FindEventsForObjectIdOnly( pObject->GetObjectId(), GetCutfFile()->GetEventList(), ObjectEvents );

#if	CUTSCENE_EXPORT_EVENTS_WITH_ZERO_BASE_TIME
		float EventTime = GetCutSceneTimeWithRangeOffset();  
#else
		float EventTime = GetTime(); 
#endif

		for(int i=0; i < ObjectEvents.GetCount(); i++)
		{
			if(ObjectEvents[i]->GetTime() == EventTime )
			{
				bAddEvent = false; 			
			}
		}

		if(bAddEvent)
		{
			GetCutfFile()->AddEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), EventTime,
				CUTSCENE_FIXUP_OBJECTS_EVENT, rage_new cutfObjectIdEventArgs( pObject->GetObjectId() ) ) );
			GetCutfFile()->SortEvents(); 
			SaveCutfile(); 
			BankPopulateFixupObjectEventList(pObject); 
		}
	}
}

void cutsManager::BankRemoveFixupEvent()
{
	if (m_FixupObjectEvents.GetCount() > 1)
	{
		if(m_FixupObjectEvents[m_iSelectedFixupObjectEventIndex])
		{
			SEditCutfObjectLocationInfo *pObjectLocationInfo = m_editFixupObjectList[m_iSelectedFixupObjectIndex - 1];
			GetCutfFile()->RemoveEvent(m_FixupObjectEvents[m_iSelectedFixupObjectEventIndex]);
			BankPopulateFixupObjectEventList(pObjectLocationInfo->pObject); 
			SaveCutfile(); 
		}
	}
	else
	{
		cutfAssertf(0, "An object must have at least one event, add the correct event and remove the undesierd event, or remove the object from the list"); 
	}
}

void cutsManager::BankAddBlockingBoundObjectCallback()
{
	if ( IsPaused() || IsPlaying() )
	{
		// get a new hidden object info
		sEditCutfBlockingBoundsInfo *pObjectLocationInfo = GetBlockingBoundObjectInfo();
		if ( pObjectLocationInfo != NULL )
		{
			if(AddBlockingBoundObject(pObjectLocationInfo))
			{
				SaveBlockingBoundObjects(); 
				BankPopulateBlockingBoundObjectEventList(pObjectLocationInfo->pObject); 
			}
		}
	}
}

void cutsManager::BankPopulateBlockingBoundObjectEventList(const cutfObject* pObject)
{
	if (m_pEditBlockingBoundObjectEvents == NULL)
	{
		return; 
	}
	//Nothing selected
	if(!pObject)
	{
		m_pEditBlockingBoundObjectEvents->UpdateCombo( "Blocking Bound var events", &m_iSelectedBlockingBoundObjectEventIndex, 1, NULL );
		m_pEditBlockingBoundObjectEvents->SetString( 0 , "none");

		return; 
	}

	atArray<cutfEvent *> EventList;
	GetCutfFile()->FindEventsForObjectIdOnly( pObject->GetObjectId(), GetCutfFile()->GetEventList(), EventList );

	//look to edit a current event 
	if (EventList.GetCount() == 0)
	{
		m_pEditBlockingBoundObjectEvents->SetString( 0 , "none");
	}
	else
	{
		if (EventList.GetCount() > 0)
		{
			m_pEditBlockingBoundObjectEvents->UpdateCombo( "Hidden var events", &m_iSelectedBlockingBoundObjectEventIndex, EventList.GetCount(), NULL );

			//Clear the list of events, need to repopulate. 
			m_BlockingBoundObjectEvents.Reset();  

			for ( int i = 0; i < EventList.GetCount(); ++i )
			{
				if (EventList[i]->GetEventId() == CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT ||  EventList[i]->GetEventId() == CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT )
				{
					cutfEventArgs *pEventArgs = const_cast<cutfEventArgs *>( EventList[i]->GetEventArgs() );

					if(pEventArgs->GetType() == CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE)
					{						
						char EventInfo[64]; 	

						u32 FrameNumber = u32(EventList[i]->GetTime() * CUTSCENE_FPS); 

						if(EventList[i]->GetEventId() == CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT )
						{
							formatf(EventInfo, sizeof(EventInfo)-1,"Activate Blocking Bound Event %d: Frame: %d , time: %f", i , FrameNumber, EventList[i]->GetTime());
						}
						else
						{
							formatf(EventInfo, sizeof(EventInfo)-1,"Deactivate Blocking Bound Event %d: Frame: %d , time: %f", i , FrameNumber, EventList[i]->GetTime());
						}


						m_pEditBlockingBoundObjectEvents->SetString( i, EventInfo );

						//Store a list of pointers to our objects
						m_BlockingBoundObjectEvents.PushAndGrow(EventList[i]); 
					}
				}
			}
		}
	}
}

void cutsManager::BankRemoveBlockingBoundObjectCallback()
{
	if ( (IsPaused() || IsPlaying()) && (m_iSelectedBlockingBoundObjectIndex > 0) && (m_iSelectedBlockingBoundObjectIndex <= m_editBlockingBoundObjectList.GetCount()) )
	{
		RemoveBlockingBoundObject( m_editBlockingBoundObjectList[m_iSelectedBlockingBoundObjectIndex - 1] );

		SaveBlockingBoundObjects(); 
	}
}

void cutsManager::BankRenameBlockingBoundObjectCallback()
{
	if ( (IsPaused() || IsPlaying()) && (m_iSelectedBlockingBoundObjectIndex > 0) && (m_iSelectedBlockingBoundObjectIndex <= m_editBlockingBoundObjectList.GetCount()) )
	{

		// run over all the existing blocking bounds and rename them
		char BoundName[CUTSCENE_OBJNAMELEN];

		for (int i=0; i<m_editBlockingBoundObjectList.GetCount(); i++)
		{
			formatf(BoundName, sizeof(BoundName)-1, "%s_BB_%d",  GetCutsceneName(), i+1);
			strncpy(m_editBlockingBoundObjectList[i]->cName, BoundName, CUTSCENE_OBJNAMELEN);
			if (m_editBlockingBoundObjectList[i]->pObject)
			{
				const_cast<cutfObject*>(m_editBlockingBoundObjectList[i]->pObject)->SetName(BoundName);
			}
			m_pEditBlockingBoundObjectCombo->SetString( i+1, m_editBlockingBoundObjectList[i]->cName );
		}

		SaveBlockingBoundObjects();
	}
}

void cutsManager::BankBlockingBoundObjectSelectedCallback()
{
	if ( IsPaused() || IsPlaying() )
	{
		RAGE_TRACK( cutsManager_BankBlockingBoundObjectSelectedCallback );

		if ( (m_pEditBlockingBoundObjectGroup != NULL) && (m_pBank != NULL) )
		{
			while ( m_pEditBlockingBoundObjectGroup->GetChild() )
			{
				m_pBank->Remove( *(m_pEditBlockingBoundObjectGroup->GetChild()) );
			}

			if ( (m_iSelectedBlockingBoundObjectIndex > 0) && (m_iSelectedBlockingBoundObjectIndex <= m_editBlockingBoundObjectList.GetCount()) )
			{
				m_pBank->SetCurrentGroup( *m_pEditBlockingBoundObjectGroup );
				{
					sEditCutfBlockingBoundsInfo *pObjectBoundInfo = m_editBlockingBoundObjectList[m_iSelectedBlockingBoundObjectIndex - 1];

					float creationTime = 0.0f;

					if (pObjectBoundInfo && pObjectBoundInfo->pObject)
					{
						atArray<cutfEvent*>ObjectEvents; 
						GetCutfFile()->FindEventsForObjectIdOnly( pObjectBoundInfo->pObject->GetObjectId(), GetCutfFile()->GetEventList(), ObjectEvents );

						for (s32 i=0; i<ObjectEvents.GetCount(); i++)
						{
							if (ObjectEvents[i]->GetEventId()==CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT)
							{
								creationTime = ObjectEvents[i]->GetTime();
							}
						}
					}	
					
					ConvertAngledAreaToMatrixAndVectors(pObjectBoundInfo->vCorners, pObjectBoundInfo->fHeight, pObjectBoundInfo->vMin, pObjectBoundInfo->vMax, pObjectBoundInfo->LocateMat, creationTime); 
					Vector3 vRot= VEC3_ZERO; 
					pObjectBoundInfo->LocateMat.ToEulersXYZ(vRot); 
					m_LocateMatRot.z = vRot.z * RtoD; 
					m_pBank->AddVector( "Position", &pObjectBoundInfo->LocateMat.d, -100000.0f, 100000.0f, 0.1f );
					m_pBank->AddSlider( "width", &pObjectBoundInfo->vMax.x, 0.0f, 100.0f, 0.25f );
					m_pBank->AddSlider( "Length", &pObjectBoundInfo->vMax.y, 0.0f, 100.0f, 0.25f );
					m_pBank->AddSlider( "Height", &pObjectBoundInfo->vMax.z, 0.0f, 100.0f, 0.25f );
					m_pBank->AddSlider( "Rotation", &m_LocateMatRot.z, 0.0f, 360.0f, 0.25f  );
					const char *EventList[] = { "(none)" };
					m_pEditBlockingBoundObjectEvents = dynamic_cast<bkCombo *>( m_pBank->AddCombo( "Blocking Bound Object Events", &m_iSelectedBlockingBoundObjectEventIndex, 1, EventList ) );
					//add an event list for the object 
					BankPopulateBlockingBoundObjectEventList(pObjectBoundInfo->pObject);
					m_pBank->AddButton("Add Activate Event",datCallback(MFA(cutsManager::BankAddAddBlockingBoundEvent),this)  );
					m_pBank->AddButton("Add Deactivate Event",datCallback(MFA(cutsManager::BankAddRemoveBlockingBoundEvent),this) );
					m_pBank->AddButton("Remove Selected Event", datCallback(MFA(cutsManager::BankRemoveBlockingBoundEvent),this));

				}
				m_pBank->UnSetCurrentGroup( *m_pEditBlockingBoundObjectGroup );
			}
		}
	}
}

void cutsManager::BankAddAddBlockingBoundEvent()
{
	sEditCutfBlockingBoundsInfo *pObjectLocationInfo = m_editBlockingBoundObjectList[m_iSelectedBlockingBoundObjectIndex - 1];

	const cutfObject* pObject = pObjectLocationInfo->pObject; 

	if(pObject)
	{
		bool bAddEvent = true; 

		atArray<cutfEvent*>ObjectEvents; 
		GetCutfFile()->FindEventsForObjectIdOnly( pObject->GetObjectId(), GetCutfFile()->GetEventList(), ObjectEvents );

#if	CUTSCENE_EXPORT_EVENTS_WITH_ZERO_BASE_TIME
		float EventTime = GetCutSceneTimeWithRangeOffset();  
#else
		float EventTime = GetTime(); 
#endif

		for(int i=0; i < ObjectEvents.GetCount(); i++)
		{
			if(ObjectEvents[i]->GetTime() == EventTime )
			{
				bAddEvent = false; 			
			}
		}

		if(bAddEvent)
		{
			GetCutfFile()->AddEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), EventTime,
				CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT, rage_new cutfObjectIdEventArgs( pObject->GetObjectId() ) ) );
			GetCutfFile()->SortEvents(); 
			SaveCutfile(); 
			BankPopulateBlockingBoundObjectEventList(pObject); 
		}
	}
}

void cutsManager::BankAddRemoveBlockingBoundEvent()
{
	sEditCutfBlockingBoundsInfo *pObjectLocationInfo = m_editBlockingBoundObjectList[m_iSelectedBlockingBoundObjectIndex - 1];

	const cutfObject* pObject = pObjectLocationInfo->pObject; 

	if(pObject)
	{
		bool bAddEvent = true; 

		atArray<cutfEvent*>ObjectEvents; 
		GetCutfFile()->FindEventsForObjectIdOnly( pObject->GetObjectId(), GetCutfFile()->GetEventList(), ObjectEvents );

#if	CUTSCENE_EXPORT_EVENTS_WITH_ZERO_BASE_TIME
		float EventTime = GetCutSceneTimeWithRangeOffset();  
#else
		float EventTime = GetTime(); 
#endif

		for(int i=0; i < ObjectEvents.GetCount(); i++)
		{
			if(ObjectEvents[i]->GetTime() == EventTime )
			{
				bAddEvent = false; 			
			}
		}

		if(bAddEvent)
		{
			GetCutfFile()->AddEvent( rage_new cutfObjectIdEvent( pObject->GetObjectId(), EventTime,
				CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT, rage_new cutfObjectIdEventArgs( pObject->GetObjectId() ) ) );
			GetCutfFile()->SortEvents(); 
			SaveCutfile(); 
			BankPopulateBlockingBoundObjectEventList(pObject); 
		}
	}
}

void cutsManager::BankRemoveBlockingBoundEvent()
{
	if (m_BlockingBoundObjectEvents.GetCount() > 1)
	{
		if(m_BlockingBoundObjectEvents[m_iSelectedBlockingBoundObjectEventIndex])
		{
			sEditCutfBlockingBoundsInfo *pObjectLocationInfo = m_editBlockingBoundObjectList[m_iSelectedBlockingBoundObjectIndex - 1];
			GetCutfFile()->RemoveEvent(m_BlockingBoundObjectEvents[m_iSelectedBlockingBoundObjectEventIndex]);
			BankPopulateBlockingBoundObjectEventList(pObjectLocationInfo->pObject); 
			SaveCutfile(); 
		}
	}
	else
	{
		cutfAssertf(0, "An object must have at least one event, add the correct event and remove the undesierd event, or remove the object from the list"); 
	}
}

#endif // __BANK

#if !__FINAL

void cutsManager::DisplayStatus()
{
    char playbackRateText[32];
    playbackRateText[0] = 0;

    if ( IsPlaying() || IsFadingOutAtEnd() )
    {
        if ( m_fPlaybackRate > 1.0f )
        {
            sprintf( playbackRateText, " >> %dx", (int)m_fPlaybackRate );
        }
        else if ( m_fPlaybackRate <= -1.0f )
        {
            sprintf( playbackRateText, " << %dx", (int)fabsf(m_fPlaybackRate) );
        }
        else if ( (m_fPlaybackRate > 0.0f) && (m_fPlaybackRate < 1.0f) )
        {
            sprintf( playbackRateText, " >> %d fps", (int)(m_fPlaybackRate * CUTSCENE_FPS) );
        }
        else if ( (m_fPlaybackRate > -1.0f) && (m_fPlaybackRate < 0.0f) )
        {
            sprintf( playbackRateText, " << %d fps", (int)fabsf(m_fPlaybackRate * CUTSCENE_FPS) );
        }
    }

	bool bOldLighting = grcLighting( false );
    Color32 prevColor = grcFont::GetCurrent().GetInternalColor();
    
    grcFont::GetCurrent().SetInternalColor( Color32( 250, 250, 250 ) );
    grcFont::GetCurrent().Drawf( m_fStatusDisplayX, m_fStatusDisplayY, "[CUTSCENE] %s %s %s%s", 
        m_cCurrentSceneName, m_cDisplayTime, c_cutsceneStateNames[m_cutsceneState], playbackRateText );       

    s32 iObjectId = FindCameraObjectId();
    if ( iObjectId != -1 )
    {
        SEntityObject *pEntityObject = m_cutsceneEntityObjects.Access( iObjectId );
        if ( pEntityObject != NULL )
        {
            const cutsAnimatedCameraEntity *pCameraEntity = dynamic_cast<cutsAnimatedCameraEntity *>( pEntityObject->pEntity );
            if ( pCameraEntity != NULL )
            {
                grcFont::GetCurrent().Drawf( m_fStatusDisplayX, m_fStatusDisplayY + grcFont::GetCurrent().GetHeight() * 2, 
                    "[CAMERA] %s", pCameraEntity->GetCameraCutName() );

                if ( m_iSelectedPedModelIndex != 0 )
                {
                    grcFont::GetCurrent().SetInternalColor( pCameraEntity->FaceZoomHasFace() ? Color32( 0, 180, 0 ) : Color32( 200, 0, 0 ) );
                    grcFont::GetCurrent().Drawf( m_fStatusDisplayX, m_fStatusDisplayY + grcFont::GetCurrent().GetHeight() * 4, 
                        "[FACE ZOOM] %s", pCameraEntity->GetFaceZoomName() );
                }
            }
        }
    }

	grcFont::GetCurrent().SetInternalColor( prevColor );
	grcLighting( bOldLighting );
}

void cutsManager::DisplayMemoryUsage()
{
    RAGE_TRACK( cutsManager_DisplayMemoryUsage );
	if (!cutsVerifyf(GetCutfFile(), "BankFaceZoomFarDrawDistanceChangedCallback: No cut file obect loaded"))
	{
		return;
	}

	bool bOldLighting = grcLighting( false );

    Color32 prevColor = grcFont::GetCurrent().GetInternalColor();
    grcFont::GetCurrent().SetInternalColor( Color32( 250, 250, 250 ) );

    float y = m_fStatusDisplayY + (grcFont::GetCurrent().GetHeight() * 6.0f);

    grcFont::GetCurrent().Draw( m_fStatusDisplayX, y, "MEMORY USAGE" );
    y += grcFont::GetCurrent().GetHeight();

    grcFont::GetCurrent().Draw( m_fStatusDisplayX, y, "----------------" );
    y += grcFont::GetCurrent().GetHeight();

    int iTotalSize = 0;

    atArray<cutfObject *> modelObjectList;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_MODEL_OBJECT_TYPE, modelObjectList );

    for ( int i = 0; i < modelObjectList.GetCount(); )
    {
        SEntityObject *pEntityObject = GetEntityObject( modelObjectList[i], false );
        if ( pEntityObject == NULL )
        {
            continue;
        }

        cutsAnimatedEntity *pAnimEntity = dynamic_cast<cutsAnimatedEntity *>( pEntityObject->pEntity );
        if ( pAnimEntity == NULL )
        {
            continue;
        }

        grcFont::GetCurrent().Drawf( m_fStatusDisplayX, y, "%s - %dk", 
            modelObjectList[i]->GetDisplayName().c_str(), pAnimEntity->GetMemoryUsage() );

        iTotalSize += pAnimEntity->GetMemoryUsage();
        ++i; 
        y += grcFont::GetCurrent().GetHeight();
    }

    atArray<cutfObject *> animMgrObjectList;
    GetCutfFile()->FindObjectsOfType( CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE, animMgrObjectList );

    if ( animMgrObjectList.GetCount() > 0 )
    {
        SEntityObject *pEntityObject = GetEntityObject( animMgrObjectList[0], false );
        if ( pEntityObject != NULL )
        {
            cutsAnimationManagerEntity *pAnimMgrEntity = dynamic_cast<cutsAnimationManagerEntity *>( pEntityObject->pEntity );
            if ( pAnimMgrEntity != NULL )
            {
                grcFont::GetCurrent().Drawf( m_fStatusDisplayX, y, "%s - %dk", 
                    animMgrObjectList[0]->GetDisplayName().c_str(), pAnimMgrEntity->GetMemoryUsage() );

                iTotalSize += pAnimMgrEntity->GetMemoryUsage();
                y += grcFont::GetCurrent().GetHeight();
            }
        }       
    }

    grcFont::GetCurrent().Draw( m_fStatusDisplayX, y, "----------------" );
    y += grcFont::GetCurrent().GetHeight();

    grcFont::GetCurrent().Drawf( m_fStatusDisplayX, y, "Total - %dk", iTotalSize );

    grcFont::GetCurrent().SetInternalColor( prevColor );

	grcLighting( bOldLighting );
}

void cutsManager::DrawSceneOrigin()
{
    bool bOldLighting = grcLighting( false );

    Matrix34 m;
    GetSceneOrientationMatrix( m );
    grcDrawAxis( 1.0f, m, true );

    grcLighting( bOldLighting );
}

void cutsManager::PopulatePedModelFaceList()
{
    if ( !IsRestarting() )
    {
        m_iSelectedPedModelIndex = 0;
    }

    // only create them once
    if ( m_pedModelObjectList.GetCount() > 0 )
    {
        return;
    }

    RAGE_TRACK( cutsManager_PopulatePedModelFaceList );

    GetModelObjectsOfType( m_pedModelObjectList, CUTSCENE_PED_MODEL_TYPE );

    if ( m_pedModelObjectList.GetCount() == 0 )
    {
#if __BANK
        if ( m_pEditFaceCombo != NULL )
        {
            m_iSelectedPedModelIndex = 0;

            m_pEditFaceCombo->UpdateCombo( "Face Selector", &m_iSelectedPedModelIndex, 1, NULL, 
                datCallback(MFA(cutsManager::BankFaceSelectedCallback),this) );

            m_pEditFaceCombo->SetString( 0, "(none)" );
			
		}

		BankFaceSelectedCallback();

		if ( m_pIsolatePedCombo != NULL )
		{
			// same for isolate ped
			m_iSelectedIsolatePedIndex = 0;

			m_pIsolatePedCombo->UpdateCombo( "Isolate ped", &m_iSelectedIsolatePedIndex, 1, NULL);
			m_pIsolatePedCombo->SetString( 0, "(none)" );
        }
#endif
        return;
    }

    cutsDisplayf( "Found %d Ped Models with Face Animations.", m_pedModelObjectList.GetCount() );

#if __BANK
    // update the combo
    if ( m_pEditFaceCombo != NULL )
    {
        m_pEditFaceCombo->UpdateCombo( "Face Selector", &m_iSelectedPedModelIndex, m_pedModelObjectList.GetCount() + 1, NULL, 
            datCallback(MFA(cutsManager::BankFaceSelectedCallback),this) );

        m_pEditFaceCombo->SetString( 0, "(none)" );
        for ( int i = 0; i < m_pedModelObjectList.GetCount(); ++i )
        {
            m_pEditFaceCombo->SetString( i + 1, m_pedModelObjectList[i]->GetDisplayName().c_str() );
        }
    }

	if ( m_pIsolatePedCombo != NULL )
	{
		m_pIsolatePedCombo->UpdateCombo( "Isolate ped", &m_iSelectedIsolatePedIndex, m_pedModelObjectList.GetCount() + 1, NULL);

		m_pIsolatePedCombo->SetString( 0, "(none)" );
		for ( int i = 0; i < m_pedModelObjectList.GetCount(); ++i )
		{
			m_pIsolatePedCombo->SetString( i + 1, m_pedModelObjectList[i]->GetDisplayName().c_str() );
		}
	}
#endif // __BANK
}

#if !__NO_OUTPUT
void cutsManager::CommonDebugStr(char * debugStr)
{
	sprintf(debugStr, "CT(%6.2f) - cutsManager (%s) - ", 
		m_fTime,
		GetCutsceneName());
}
#endif //!__NO_OUTPUT

#endif // !__FINAL

} // namespace rage
