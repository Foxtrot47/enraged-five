// 
// cutscene/cutsevent.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutsevent.h"
#include "cutseventargs.h"
#include "cutsoptimisations.h"

CUTS_OPTIMISATIONS()

namespace rage {

//##############################################################################

const char* cutsEvent::c_cutscenePlaybackEventIdDisplayNames[] = 
{
    "Start of Scene",
    "End of Scene",
    "Play",
    "Update Fading Out At Beginning",
    "Update Loading",
    "Update",
    "Update Unloading",
    "Unloaded",
    "Pause",
    "Stop",
    "Cancel Load",
    "Set Animation",
    "Set Dual Animation",
    "Set Clip",
    "Set Dual Clip",
    "Set Dual Clip Anim",
	"Dictionary loaded",
	"Skipped",
    "Step Forward",
    "Step Backward",
    "Fast Forward",
    "Rewind",
    "Restart",
    "Set Face Zoom",
    "Clear Face Zoom",
    "Set Face Zoom Distance",
    "Set Face Zoom Near Draw Distance",
    "Set Face Zoom Far Draw Distance",
    "Show Debug Lines",
    "Hide Debug Lines",
    "Scene Orientation Changed",
    "Mute Audio",
    "Unmute Audio",
    "Enable Depth Of Field",
    "Disable Depth Of Field"
};

void cutsEvent::InitClass()
{
    cutfEvent::SetEventIdDisplayNameFunc( MakeFunctorRet( &GetPlaybackEventDisplayName ), CUTSCENE_EVENT_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY );
    cutsEventArgs::InitClass();
}

void cutsEvent::ShutdownClass()
{
    cutsEventArgs::ShutdownClass();
    cutfEvent::SetEventIdDisplayNameFunc( 0, CUTSCENE_EVENT_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY );
}

const char* cutsEvent::GetPlaybackEventDisplayName( s32 iEventId )
{
    if ( (iEventId >= CUTSCENE_EVENT_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY) && (iEventId <= CUTSCENE_LAST_PLAYBACK_EVENT) )
    {
        int iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY;
        return c_cutscenePlaybackEventIdDisplayNames[iEventIdOffset];
    }

    return "";
}

//##############################################################################

} // namespace rage
