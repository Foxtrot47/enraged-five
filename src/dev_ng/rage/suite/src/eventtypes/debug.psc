<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<autoregister allInFile="true"/>
<structdef name="PrintEvent" type="::rage::evtInstancePrintf" base="::rage::evtInstance">
	<string name="m_String" type="member" size="64" onWidgetChanged="UpdateEditor"/>
</structdef>

<structdef name="DrawTickEvent" type="::rage::evtInstanceDrawTick" base="::rage::evtInstance">
	<u8 name="m_Bone"/>
	<Vector3 name="m_Offset" min="-1000.0f" max="1000.0f" step="0.01f"/>
	<Color32 name="m_Color"/>
</structdef>

</ParserSchema>