// 
// eventtypes/ptfx.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "ptfx.h"
#include "data/safestruct.h"
#include "event/evtchannel.h"
#include "event/player.h"
#include "event/runner.h"
#include "event/updatedata.h"
#include "paging/rscbuilder.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxmanager.h"
#include "crskeleton/skeleton.h"

using namespace rage;
#include "ptfx_parser.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
// Struct      : evtInstancePtfx
//------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
evtInstancePtfx::evtInstancePtfx()
{
	m_Offset.Zero();
	m_EmitterName[0]=0;
	m_MinRelativeSpeed=0.0f;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void evtInstancePtfx::Copy(const evtInstance& other)
{
	evtAssertf(dynamic_cast<const evtInstancePtfx*>(&other), "Wrong type for Copy()");
	const evtInstancePtfx& typedOther = static_cast<const evtInstancePtfx&>(other);

	evtInstance::Copy(typedOther);

	strncpy(m_EmitterName, typedOther.m_EmitterName, sizeof(m_EmitterName));
	m_Offset.Set(typedOther.m_Offset);
	m_MinRelativeSpeed = typedOther.m_MinRelativeSpeed;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __DECLARESTRUCT
void evtInstancePtfx::DeclareStruct(datTypeStruct &s)
{
	evtInstance::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(evtInstancePtfx, evtInstance)
	SSTRUCT_CONTAINED_ARRAY(evtInstancePtfx, m_EmitterName)
	SSTRUCT_FIELD(evtInstancePtfx, m_MinRelativeSpeed)
	SSTRUCT_FIELD(evtInstancePtfx, m_Offset)
	SSTRUCT_END(evtInstancePtfx)
}
#endif // __DECLARESTRUCT
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
// Struct      : evtTypePtfx
//------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
evtTypePtfx::evtTypePtfx()
: m_Matrix(evtParamManager::PARAM_NOT_FOUND)
, m_Skeleton(evtParamManager::PARAM_NOT_FOUND)
{
}

void evtTypePtfx::LookupParameterIds()
{
	m_Matrix = EVENTPARAM.GetParamId("matrix");
	m_Skeleton = EVENTPARAM.GetParamId("skeleton");
	m_Position = EVENTPARAM.GetParamId("position");
	m_Normal = EVENTPARAM.GetParamId("normal");
	m_RelativeSpeed = EVENTPARAM.GetParamId("relative speed");
}

void evtTypePtfx::Start(const evtInstance& inst, const evtUpdateData& data)
{
	//Grab an instance and go
	evtInstancePtfx* realInst =(evtInstancePtfx*)(&inst);

	bool hasMatrix = data.m_EventRunner->GetParamList()->ParamExists(m_Matrix);
	//bool hasSkeleton = data.m_EventRunner->GetParamList()->ParamExists(m_Skeleton);
	bool hasPosition = data.m_EventRunner->GetParamList()->ParamExists(m_Position);
	//bool hasNormal = data.m_EventRunner->GetParamList()->ParamExists(m_Normal);

	// Filter the event based on input.
	// TODO: Split this out into a separate filter event type, with the particle event as a subordinate event.
	if (data.m_EventRunner->GetParamList()->ParamExists(m_RelativeSpeed))
	{
		float relSpeed = atAnyCast<float>(data.m_EventRunner->GetParamList()->GetParam(m_RelativeSpeed));

		if (relSpeed < realInst->m_MinRelativeSpeed)
		{
			return;
		}
	}

#if !__FINAL
	if (!pgRscBuilder::IsBuilding())
#endif // !__FINAL
	{
		if(!g_ptxManager::IsInstantiated())
		{
			evtAssertf(false, "RMPTFXMGR is not instantiated.");
			return;
		}

		data.m_RunnerInstanceData = RMPTFXMGR.GetEffectInst(atStringHash(realInst->m_EmitterName));

#if RMPTFX_XML_LOADING
		//Can't find one so do nothing
		if(!data.m_RunnerInstanceData)
		{
			RMPTFXMGR.LoadEffectRule(realInst->m_EmitterName);
			data.m_RunnerInstanceData = RMPTFXMGR.GetEffectInst(atStringHash(realInst->m_EmitterName));
		}
#endif
	}

	if(!data.m_RunnerInstanceData)		
		return;

	ptxEffectInst * effect = (ptxEffectInst*)data.m_RunnerInstanceData;
	effect->SetOffsetPos(RCC_VEC3V(realInst->m_Offset));

	//based on parameters set emitter position
	if(hasMatrix)
	{
		Mat34V pmtx = atAnyCast<Mat34V>(data.m_EventRunner->GetParamList()->GetParam(m_Matrix));
		effect->SetBaseMtx( pmtx );
		effect->Start();
		return;
	}
	if(hasPosition)
	{
		/*if(hasNormal)
		{
			Matrix34 matrix;
			matrix.Identity();
			Vector3 pos = atAnyCast<Vector3>(data.m_EventRunner->GetParamList()->GetParam(m_Position));
			Vector3 normal = atAnyCast<Vector3>(data.m_EventRunner->GetParamList()->GetParam(m_Normal));

			emitter->SetPosToMatrix34(pmtx);
			emitter->Start();
			return;
		}*/
		Vector3 pos = VEC3V_TO_VECTOR3(atAnyCast<Vec3V>(data.m_EventRunner->GetParamList()->GetParam(m_Position)));
		effect->SetBasePos(VECTOR3_TO_VEC3V(pos));
		effect->Start();
		return;
	}

		/*else
		{
			//Support attaching to characters
			evtAssertf(m_Skeleton != evtParamManager::PARAM_NOT_FOUND);
			const crSkeleton* skel = atAnyCast<const crSkeleton*>(data.m_EventRunner->GetParamList()->GetParam(m_Skeleton));

			if (0 < skel->GetBoneCount()) {
				emitter->SetPosToMatrix34(&skel->GetGlobalMtx(0));
			}
		}*/
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void evtTypePtfx::Update(const evtInstance& BANK_ONLY(inst), const evtUpdateData& BANK_ONLY(data))
{
#if __BANK
	evtInstancePtfx* realInst =(evtInstancePtfx*)(&inst);
	ptxEffectInst * effect = (ptxEffectInst*)data.m_RunnerInstanceData;
	if(effect)
		effect->SetOffsetPos(VECTOR3_TO_VEC3V(realInst->m_Offset));
#endif
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void evtTypePtfx::Stop(const evtInstance& , const evtUpdateData& data)
{
	//Stop the emitter  
	ptxEffectInst * effect = (ptxEffectInst*)data.m_RunnerInstanceData;
	//only stop infinite looping emitters
	if(effect)
		if(effect->GetEffectRule()->GetNumLoops()==-1)
			effect->Finish(false);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
