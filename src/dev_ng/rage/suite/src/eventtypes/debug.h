// 
// eventtypes/debug.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENTTYPES_DEBUG_H
#define EVENTTYPES_DEBUG_H

#include "event/instance.h"
#include "event/param.h"
#include "event/type.h"

#include "vector/color32.h"
#include "vector/vector3.h"

namespace rage {

class evtInstancePrintf : public evtInstance {
public:
	evtInstancePrintf();
	evtInstancePrintf(datResource&) { }

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	const char* ToString() const {return m_String;}

	char m_String[64];

protected:
	virtual void Copy(const evtInstance& other);

	PAR_PARSABLE;
};

inline evtInstancePrintf::evtInstancePrintf()
{	
	m_String[0]='\0';
}

class evtTypePrintf : public evtType
{
public:
	virtual void Start(const evtInstance& , const evtUpdateData& );
	virtual void Update(const evtInstance& , const evtUpdateData& );
	virtual void Stop(const evtInstance& , const evtUpdateData& );
};

class evtInstanceDrawTick : public evtInstance {
public:
	evtInstanceDrawTick() 
	: m_Offset(ORIGIN)
	, m_Color(255,255,255,255)
	, m_Bone(0)
	{
	}

	evtInstanceDrawTick(datResource&) { }

	const char* ToString() const {return "Draw Tick";}

	Vector3 m_Offset;
	Color32 m_Color;
	u8 m_Bone;

protected:
	virtual void Copy(const evtInstance& other);

	PAR_PARSABLE;
};

class evtTypeDrawTick : public evtType {
public:
	evtTypeDrawTick();

	virtual void Update(const evtInstance&, const evtUpdateData&);

protected:
	virtual void LookupParameterIds();

	evtParamId m_Matrix;
	evtParamId m_Skeleton;
};

} // namespace rage

#endif // EVENTTYPES_DEBUG_H

