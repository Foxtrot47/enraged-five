// 
// eventtypes/sound.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENTTYPES_SOUND_H
#define EVENTTYPES_SOUND_H

#include "audioengine/entity.h"
#include "event/instance.h"
#include "event/type.h"
#include "event/param.h"

namespace rage 
{

	class audSound;
	class phInst;

	//////////////////////////////////////////////////////////////////////////
	// evtInstanceAudOneShot
	// 
	// PURPOSE: defines an instance of a collide event
	//////////////////////////////////////////////////////////////////////////
	class evtInstanceAudOneShot : public evtInstance
	{
	public:
		evtInstanceAudOneShot();
		evtInstanceAudOneShot(datResource&)	{ }

		virtual const char * ToString() const { return m_SoundName; }
		virtual const char * GetSoundName() const { return m_SoundName; }

#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct &s);
#endif

	protected:
		virtual void Copy(const evtInstance &other);

		// tunable data
		char	m_SoundName[128];

		PAR_PARSABLE;
	};

	//////////////////////////////////////////////////////////////////////////
	// evtTypeAudOneShot
	//
	// PURPOSE: audio collide type
	//////////////////////////////////////////////////////////////////////////
	class evtTypeAudOneShot : public evtType, public audEntity
	{
	public:
		evtTypeAudOneShot();

		virtual void Start(const evtInstance &, const evtUpdateData &);
		virtual void Update(const evtInstance &, const evtUpdateData &);
		virtual void Stop(const evtInstance &, const evtUpdateData &);

		virtual const char *GetEntityName() const;

	protected:
		virtual void LookupParameterIds();

		evtParamId m_PosPtr;
		evtParamId m_InstPtr;
        evtParamId m_ImpactIterator;

		bool m_HasInstance;
		bool m_HasPosition;
		phInst *m_pInst;
		audSound *m_pCollideSound;
		audSound *m_pCollideSound2;
	};

} // namespace rage

#endif // EVENTTYPES_SOUND_H

