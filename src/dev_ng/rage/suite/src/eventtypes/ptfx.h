// 
// eventtypes/ptfx.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#ifndef EVENTTYPES_PTFX_H
#define EVENTTYPES_PTFX_H

#include "event/instance.h"
#include "event/param.h"
#include "event/type.h"

#include "vector/vector3.h"


namespace rage {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
// Struct      : evtInstancePtfx
// Description : an instance of the particle event type
//------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class evtInstancePtfx: public evtInstance {
public:
	evtInstancePtfx();
	evtInstancePtfx(datResource&) { }

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	const char* ToString() const {return "RmPtfx";}
	
	//Tuneable
	char			 m_EmitterName[120];
	float            m_MinRelativeSpeed;
	Vector3			 m_Offset;

protected:
	virtual void Copy(const evtInstance& other);
	
	PAR_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
// Struct      : evtTypePtfx
// Description : Particle trigger event type
//------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class evtTypePtfx : public evtType
{
public:
	evtTypePtfx();

	virtual void Start(const evtInstance& , const evtUpdateData& );
	virtual void Update(const evtInstance& , const evtUpdateData& );
	virtual void Stop(const evtInstance& , const evtUpdateData& );

protected:
	virtual void LookupParameterIds();

	evtParamId m_Matrix;
	evtParamId m_Skeleton;
	evtParamId m_Position;
	evtParamId m_Normal;
	evtParamId m_RelativeSpeed;
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}; //namespace rage
#endif //EVENTTYPES_PTFX_H
