<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<autoregister allInFile="true"/>
<structdef name="AudOneShot" type="::rage::evtInstanceAudOneShot" base="::rage::evtInstance">
	<string name="m_SoundName" type="member" size="128"/>
	</structdef>

</ParserSchema>