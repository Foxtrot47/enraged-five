// 
// eventtypes/debug.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "debug.h"
#include "debug_parser.h"

#include "crskeleton/skeleton.h"
#include "data/safestruct.h"
#include "event/evtchannel.h"
#include "event/player.h"
#include "event/updatedata.h"
#include "grprofile/drawmanager.h"
#include "vector/colors.h"
#include "vectormath/legacyconvert.h"
#include "vectormath/mat34v.h"

using namespace rage;

PFD_DECLARE_GROUP(Event);
PFD_DECLARE_ITEM_ON(DrawTick, Color_white, Event);


#if __DECLARESTRUCT
void evtInstancePrintf::DeclareStruct(datTypeStruct &s)
{
	evtInstance::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(evtInstancePrintf, evtInstance)

	SSTRUCT_CONTAINED_ARRAY(evtInstancePrintf, m_String)

	SSTRUCT_END(evtInstancePrintf)
}
#endif // __DECLARESTRUCT

void evtInstancePrintf::Copy(const evtInstance& other)
{
	evtAssertf(dynamic_cast<const evtInstancePrintf*>(&other), "Wrong type for Copy()");
	const evtInstancePrintf& typedOther = static_cast<const evtInstancePrintf&>(other);

	evtInstance::Copy(typedOther);

	strncpy(m_String, typedOther.m_String, sizeof(m_String));
	m_String[sizeof(m_String) - 1] = '\0';
}

void evtTypePrintf::Start(const evtInstance& inst, const evtUpdateData& OUTPUT_ONLY(upData))
{
	const evtInstancePrintf* realInst = dynamic_cast<const evtInstancePrintf*>(&inst);
	if (!realInst) {
		return;
	}

	evtDebugf1("   Start:  %s (%.3f->%.3f)\n", realInst->m_String, upData.m_LocalPhaseBegin, upData.m_LocalPhaseEnd);
}

void evtTypePrintf::Update(const evtInstance& inst, const evtUpdateData& /*upData*/)
{
	const evtInstancePrintf* realInst = dynamic_cast<const evtInstancePrintf*>(&inst);
	if (!realInst) {
		return;
	}

//	evtDisplayf("   Update: %s (%.3f->%.3f)\n", realInst->m_String, upData.m_LocalPhaseBegin, upData.m_LocalPhaseEnd);
}

void evtTypePrintf::Stop(const evtInstance& inst, const evtUpdateData& OUTPUT_ONLY(upData))
{
	const evtInstancePrintf* realInst = dynamic_cast<const evtInstancePrintf*>(&inst);
	if (!realInst) {
		return;
	}

	evtDebugf1("   Stop:   %s (%.3f->%.3f)\n", realInst->m_String, upData.m_LocalPhaseBegin, upData.m_LocalPhaseEnd);
}

void evtInstanceDrawTick::Copy(const evtInstance& other)
{
	evtAssertf(dynamic_cast<const evtInstanceDrawTick*>(&other), "Wrong type for Copy()");
	const evtInstanceDrawTick& typedOther = static_cast<const evtInstanceDrawTick&>(other);

	evtInstance::Copy(typedOther);
	
	m_Offset = typedOther.m_Offset;
	m_Color = typedOther.m_Color;
	m_Bone = typedOther.m_Bone;
}

evtTypeDrawTick::evtTypeDrawTick()
: m_Matrix(evtParamManager::PARAM_NOT_FOUND)
, m_Skeleton(evtParamManager::PARAM_NOT_FOUND)
{
}

void evtTypeDrawTick::LookupParameterIds()
{
	m_Matrix   = EVENTPARAM.GetParamId("matrix");
	m_Skeleton = EVENTPARAM.GetParamId("skeleton pointer");
}

void evtTypeDrawTick::Update(const evtInstance& inst, const evtUpdateData& upData)
{
	const evtInstanceDrawTick* realInst = dynamic_cast<const evtInstanceDrawTick*>(&inst);
	if (!realInst) {
		return;
	}

#if __PFDRAW
	const float SIZE = 0.3f;
#endif // __PFDRAW

	Vector3 vec;

	Vector3 left(ORIGIN);
	Vector3 up(ORIGIN);
	Vector3 in(ORIGIN);
	Vector3 center(ORIGIN);

	if (m_Matrix != evtParamManager::PARAM_NOT_FOUND)
	{
		Matrix34 mtx = atAnyCast<Matrix34>(upData.m_EventRunner->GetParamList()->GetParam(m_Matrix));
		left.Set(mtx.a);
		up.Set(mtx.b);
		in.Set(mtx.c);
		center.Set(mtx.d);
	}
	else if (m_Skeleton != evtParamManager::PARAM_NOT_FOUND)
	{
		const crSkeleton* skel = atAnyCast<const crSkeleton*>(upData.m_EventRunner->GetParamList()->GetParam(m_Skeleton));

		if (realInst->m_Bone < skel->GetBoneCount()) {
			Matrix34 mtx;
			skel->GetGlobalMtx(realInst->m_Bone, RC_MAT34V(mtx));

			left.Set(mtx.a);
			up.Set(mtx.b);
			in.Set(mtx.c);
			center.Set(mtx.d);
		}
	}

	center.AddScaled(left, realInst->m_Offset.x);
	center.AddScaled(up, realInst->m_Offset.y);
	center.AddScaled(in, realInst->m_Offset.z);

#if __PFDRAW
	left.Scale(SIZE);
	up.Scale(SIZE);
	in.Scale(SIZE);

	left.Add(center);
	up.Add(center);
	in.Add(center);

	if (PFD_DrawTick.Begin())
	{
		bool oldLighting = grcLighting(false);
		grcBegin(drawLines, 6);
		grcColor(realInst->m_Color);
		grcVertex3f(center);
		grcVertex3f(left);
		grcVertex3f(center);
		grcVertex3f(up);
		grcVertex3f(center);
		grcVertex3f(in);
		grcEnd();

		grcLighting(oldLighting);

		PFD_DrawTick.End();
	}
#endif	
}

