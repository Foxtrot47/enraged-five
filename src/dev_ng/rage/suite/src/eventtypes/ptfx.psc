<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<autoregister allInFile="true"/>
<structdef name="PtfxEvent" type="::rage::evtInstancePtfx" base="::rage::evtInstance">
<string name="m_EmitterName" type="member" size="120" onWidgetChanged="UpdateEditor"/>
<Vector3 name="m_Offset" min="-1000.0f" max="1000.0f" step="0.01f"/>
<float name="m_MinRelativeSpeed" min="0.0f" max="10000.0f" step="1.0f"/>
</structdef>

</ParserSchema>