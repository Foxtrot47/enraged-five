// 
// eventtypes/sound.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "sound.h"
#include "sound_parser.h"

#include "data/safestruct.h"
#include "data/struct.h"
#include "event/evtchannel.h"
#include "event/updatedata.h"
#include "event/runner.h"
#include "fragment/instance.h"

#include "audiosoundtypes/soundcontrol.h"
#include "audioengine/controller.h"

namespace rage 
{

	evtInstanceAudOneShot::evtInstanceAudOneShot()
	{
		m_SoundName[0] = '\0';
	}

	void evtInstanceAudOneShot::Copy(const evtInstance &other)
	{
		evtAssertf(dynamic_cast<const evtInstanceAudOneShot*>(&other), "Wrong type for Copy()");
		const evtInstanceAudOneShot &typedOther = static_cast<const evtInstanceAudOneShot&>(other);

		evtInstance::Copy(typedOther);

		strncpy(m_SoundName, typedOther.GetSoundName(), sizeof(m_SoundName));
	}

#if __DECLARESTRUCT
	void evtInstanceAudOneShot::DeclareStruct(datTypeStruct &s)
	{
		evtInstance::DeclareStruct(s);

		SSTRUCT_BEGIN_BASE(evtInstanceAudOneShot, evtInstance)
		SSTRUCT_CONTAINED_ARRAY(evtInstanceAudOneShot, m_SoundName)
		SSTRUCT_END(evtInstanceAudOneShot)
	}
#endif

	//////////////////////////////////////////////////////////////////////////

	evtTypeAudOneShot::evtTypeAudOneShot() :
	m_PosPtr(evtParamManager::PARAM_NOT_FOUND),
		m_InstPtr(evtParamManager::PARAM_NOT_FOUND),
		m_HasPosition(false),
		m_HasInstance(false),
		m_pCollideSound(NULL),
		m_pCollideSound2(NULL)
	{
		// helper check for tool's that don't require audio
		if (audController::FindControllerForThread())
		{
			audEntity::Init();
		}
	}

	void evtTypeAudOneShot::LookupParameterIds()
	{
		m_InstPtr = EVENTPARAM.GetParamId("instance pointer");
		m_PosPtr = EVENTPARAM.GetParamId("position");
        m_ImpactIterator = EVENTPARAM.GetParamId("impact iterator");
	}

	void evtTypeAudOneShot::Start(const evtInstance &inst, const evtUpdateData &data)
	{
		evtInstanceAudOneShot *pAudioInst = (evtInstanceAudOneShot*)(&inst);

		m_HasPosition = data.m_EventRunner->GetParamList()->ParamExists(m_PosPtr);
		m_HasInstance = data.m_EventRunner->GetParamList()->ParamExists(m_InstPtr);

		Vector3 startPos(ORIGIN);

		if (m_HasInstance)
		{
			m_pInst = atAnyCast<phInst*>(data.m_EventRunner->GetParamList()->GetParam(m_InstPtr));
			startPos.Set(RCC_MATRIX34(m_pInst->GetMatrix()).d);
		}
		else if (m_HasPosition)
		{
			startPos.Set(atAnyCast<Vector3>(data.m_EventRunner->GetParamList()->GetParam(m_PosPtr)));
		}

#if 0
        if (data.m_EventRunner->GetParamList()->ParamExists(m_ImpactIterator))
        {
            phImpact::Iterator iterator = atAnyCast<phImpact::Iterator>(data.m_EventRunner->GetParamList()->GetParam(m_ImpactIterator));

            // Do something with the iterator here
        }
#endif

		if (m_pCollideSound)
		{
			if(m_pCollideSound2)
				m_pCollideSound2->StopAndForget();
			
			CreateSound_PersistentReference(pAudioInst->GetSoundName(), &m_pCollideSound2);

			if (m_pCollideSound2)
			{
				m_pCollideSound2->SetRequestedPosition(startPos);
				m_pCollideSound2->PrepareAndPlay();
			}

		}
		else
		{
			CreateSound_PersistentReference(pAudioInst->GetSoundName(), &m_pCollideSound);

			if (m_pCollideSound)
			{
				m_pCollideSound->SetRequestedPosition(startPos);
				m_pCollideSound->PrepareAndPlay();
			}
		}
		
	}

	void evtTypeAudOneShot::Update(const evtInstance &, const evtUpdateData &data)
	{
		if (m_pCollideSound)
		{
			Vector3 currentPos(ORIGIN);

			if (m_HasInstance)
			{
				currentPos.Set(RCC_MATRIX34(m_pInst->GetMatrix()).d);
			}
			else if (m_HasPosition)
			{
				currentPos.Set(atAnyCast<Vector3>(data.m_EventRunner->GetParamList()->GetParam(m_PosPtr)));
			}

			m_pCollideSound->SetRequestedPosition(currentPos);
		}

		if (m_pCollideSound2)
		{
			Vector3 currentPos(ORIGIN);

			if (m_HasInstance)
			{
				currentPos.Set(RCC_MATRIX34(m_pInst->GetMatrix()).d);
			}
			else if (m_HasPosition)
			{
				currentPos.Set(atAnyCast<Vector3>(data.m_EventRunner->GetParamList()->GetParam(m_PosPtr)));
			}

			m_pCollideSound2->SetRequestedPosition(currentPos);
		}
	}

	void evtTypeAudOneShot::Stop(const evtInstance &, const evtUpdateData &)
	{
	}

	const char *evtTypeAudOneShot::GetEntityName() const
	{ 
		return evtType::GetName(); 
	}


} // namespace rage
