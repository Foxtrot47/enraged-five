// 
// demesh/deedge.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEMESH_DEEDGE_H
#define DEMESH_DEEDGE_H

#include "deconfig.h"
#include "mesh/mesh.h"

#include "atl/binheap.h"
#include "atl/bitset.h"

#include "vector/vector3.h"
#include "vector/vector4.h"

#if MESH_LIBRARY


namespace rage {

class deEdgeSet;
class deVertex;
class deWedge;
class parStreamOut;

class deFace {
	// material data goes here?

	// does it need an edge pointer?
};

/*

Have a choice here for what an edge should actually point to.
          
   /       \
  /\P     X/\   P = prev edge, X = next edge 
A -----------> B

Possibilities:

AP,AX,BP,BX

Of these, AX, BP let us get A and B with 1 or 2 pointer derefs.

Of those two, BP has one more nice feature. CCW iteration around a vert is cheap, and the CW-most edge will be a boundary edge.
For AX, CW iteration is cheaper, but the CCW-most edge is not a boundary edge. 

(actually AX and BP can be totally equivalent if we change verts to store either outgoing or incoming edges. So BP + outgoing = AX + incoming)

*/

class deEdge;

typedef atBinHeap<float, deEdge*> deEdgeHeap;

// PURPOSE:
//  deEdge is in a sense the key data structure for a deMesh. The deMesh itself is just a list of vertices
//  and the deVertex has a pointer to one edge, but for any mesh traversals you're mostly dealing with edges.
//  Each edge maintains pointers to neighboring structures. From an edge you can get the next and previous
//  edges around one face, you can get the next edge clockwise or counterclockwise around a vertex,
//  you can get the neighboring half-edge, or for boundary edges you can get the next and previous edges
//  around the boundary.
// NOTES:
//  There are a number of possibilities for which exact pointers to maintain.
//  For an edge from A to B, with previous edge P, next edge X, and neighbor edge N, we could
//  maintain all of these pointers, or replace some of them with constant time calculations. For
//  example we can replace the pointer to A with P->B. In our case we've chosen to maintain
//  B, P, and N. This has some nice properties:
//  Getting A and B costs 2 and 1 pointer dereferences (respectively)
//  Counterclockwise iteration around an vertex costs 2 pointer dereferences and no compares
//  If A lies on a boundary, the clockwise-most edge also lies on the boundary.
// 
//  For every face, one edge is marked as the "face" edge. Which particular edge in the face
//  is marked this way may change during mesh operations.
//  Also, for every pair of half-edges, one is marked as the "primary" edge. Again, some mesh
//  operations may change which edge in a pair is the primary edge.
class deEdge {

public:
	deEdge()
		: m_Vertex(NULL)
		, m_Prev(NULL)
		, m_Wedge(0)
		, m_HeapNode(NULL)
		, m_Error(0.0f)
		, m_ErrorPos(0.5f)
		, m_Index(-1)
		, m_Neighbor(NULL)
	{
		m_Flags.Reset();
	}

	// Gets previous edge in triangle
	deEdge* GetPrev() const;

	// Gets next edge in triangle
	deEdge* GetNext() const;

	// Gets the neighboring edge (aka the symmetric half-edge)
	deEdge* GetNeighbor() const;

	// Gets the vertex at the start of this edge
	deVertex* GetStart() const;

	// Gets the vertex at the end of this edge
	deVertex* GetEnd() const;

	void GetFaceVerts(deVertex*& v1, deVertex*& v2, deVertex*& v3);
	void GetFaceEdges(deEdge*& e1, deEdge*& e2, deEdge*& e3); // this matches GetFaceVerts order. Edges are v1->v2, v2->v3, v3->v1

	void GetFaceVertPositions(Vector3& startPos, Vector3& endPos, Vector3& oppositePos) const;

	// Gets the next edge clockwise around the start vertex
	deEdge* GetNextCW() const;

	// Gets the next edge counterclockwise around the start vertex
	deEdge* GetNextCCW() const;

	deEdge* GetNextAlongBoundary();
	deEdge* GetPrevAlongBoundary();

	// The difference between these two and the ones above are that these assume that you're
	// already on a boundary edge (needed for stitching where we're technically not on a boundary
	// edge after the stitch
	deEdge* UnsafeGetNextAlongBoundary();
	deEdge* UnsafeGetPrevAlongBoundary();


	deWedge& GetStartWedge();
	deWedge& GetEndWedge();

	float GetLength2() const;
	float GetLength() const;

	float GetFaceArea();

	Vector4 GetFacePlane();

	Vector3 GetFaceNormal();

	float GetStartAngle();

	// PURPOSE: If the start point on this edge were moved to newPos, what would be the difference between face normals
	// RETURNS: > 0 if the new normal points in the same direction as the old. < 0 if the new normal points away from the old.
	//          0 if the normals are at right angles or the new tri would have 0 area
	float GetFaceNormalChange(const Vector3& newPos);

	void GetDebugDrawVerts(Vector3& start, Vector3& end, Vector3& perp, float& len);

	bool IsOnBoundary() const;

	void FlipFaceWinding();

	void Write(parStreamOut* stream);

	static void SetNeighbors(deEdge* a, deEdge* b);

	// you should really use the SetNeighbors() function whenever possible.
	void UnsafeSetNeighbor(deEdge* a);

	deEdge* GetPrimaryEdge();

	// Returns true if features are discontinuous across this edge.
	// Boundary edges are always sharp.
	bool IsSharp();
	bool IsStartSharp(); // returns true if the edge is sharp at the start (the end may or may not be sharp)

	// destination vertex
	deVertex* m_Vertex;

	atFixedBitSet<32> m_Flags;

	enum {
		FLAG_PROCESSED,
		FLAG_FACE_EDGE, // one edge in each face is marked as the face edge
		FLAG_PRIMARY_EDGE, // for an edge with a neighbor, exactly one of those edges will be marked "primary"
		FLAG_NO_COLLAPSE, // this edge will not be collapsed. ever.
		FLAG_RES_PROCESSED, // only used during reduce edge set, to know if an edge is a basis edge or not
							// (Can't use normal FLAG_PROCESSED because the flags could be in an unknown state when RES is called.
	};

	// For non-manifold surfaces, may have multiple neighbors

	deEdge* m_Prev;

	int		m_Wedge;

	deEdgeHeap::Node* m_HeapNode;
	float m_Padding;
	float m_ErrorPos; // 0.0 = start, 1.0 = end

	int m_Index; // for serialization. -1 == NULL

	struct EdgeConnections {
		int m_Neighbor, m_Prev, m_Vertex, m_Wedge;
		int m_Flags;
	};

protected:
	deEdge* m_Neighbor;

public:
	float m_Error;
};


inline deEdge* deEdge::GetPrev() const
{
	return m_Prev;
}

inline deEdge* deEdge::GetNext() const
{
	return m_Prev->m_Prev;
}

inline deEdge* deEdge::GetNeighbor() const
{
	return m_Neighbor;
}

inline deVertex* deEdge::GetStart() const
{
	return m_Prev->m_Vertex;
}

inline deVertex* deEdge::GetEnd() const
{
	return m_Vertex;
}

inline deEdge* deEdge::GetNextCW() const
{
	return m_Neighbor ? m_Neighbor->m_Prev->m_Prev : NULL;
}

inline deEdge* deEdge::GetNextCCW() const
{
	return m_Prev->m_Neighbor;
}

inline bool deEdge::IsOnBoundary() const
{
	return !m_Neighbor;
}

inline void deEdge::UnsafeSetNeighbor(deEdge* a)
{
	m_Neighbor = a;	
}

inline deEdge* deEdge::GetPrimaryEdge()
{
	if (m_Flags.IsSet(FLAG_PRIMARY_EDGE)) {
		Assert(!m_Neighbor || m_Neighbor->m_Flags.IsClear(FLAG_PRIMARY_EDGE));
		return this;
	}
	else {
		Assert(m_Neighbor && m_Neighbor->m_Flags.IsSet(FLAG_PRIMARY_EDGE));
		return m_Neighbor;
	}
}

} // namespace rage

#endif // MESH_LIBRARY

#endif
