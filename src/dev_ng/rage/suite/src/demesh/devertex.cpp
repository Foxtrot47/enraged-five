// 
// demesh/devertex.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "devertex.h"
#include "demesh.h"

#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "system/alloca.h"

#include <vector>

#if MESH_LIBRARY

#define MULTIPLE_MATERIALS 1

using namespace rage;

float deWedge::sm_VertSnapDist = 0.001f;
float deWedge::sm_NormalSnapDist = 0.001f;
float deWedge::sm_ColorSnapDist = 0.001f;
float deWedge::sm_TexSnapDist = 0.001f;

deVertex::EdgeIterator deVertex::EdgeIterator::sm_EndEdges = {NULL, -1, NULL};
deVertex::VertexIterator deVertex::VertexIterator::sm_EndVertices = { {NULL, -1, NULL}, true};

bool deWedge::operator ==(const deWedge& that) const
{
	if (!m_VertexData.Pos.IsClose(that.m_VertexData.Pos, deWedge::GetVertSnapDist())) {
		return false;
	}
#if MULTIPLE_MATERIALS
	if (m_Material != that.m_Material) {
		return false;
	}

	if (!m_VertexData.Nrm.IsClose(that.m_VertexData.Nrm, sm_NormalSnapDist)) {
		return false;
	}

	if (!m_VertexData.Cpv.IsClose(that.m_VertexData.Cpv, sm_ColorSnapDist)) {
		return false;
	}

	if (m_VertexData.TexCount != that.m_VertexData.TexCount) {
		return false;
	}
	for(int i = 0; i < m_VertexData.TexCount; i++) {
		if (!m_VertexData.Tex[i].IsClose(that.m_VertexData.Tex[i], sm_TexSnapDist)) {
			return false;
		}
	}

	if (m_VertexData.TanBiCount != that.m_VertexData.TanBiCount) {
		return false;
	}
	for(int i = 0; i < m_VertexData.TanBiCount; i++) {
		if (!m_VertexData.TanBi[i].B.IsClose(that.m_VertexData.TanBi[i].B, sm_NormalSnapDist)) {
			return false;
		}
		if (!m_VertexData.TanBi[i].T.IsClose(that.m_VertexData.TanBi[i].T, sm_NormalSnapDist)) {
			return false;
		}
	}
#endif
	return true;
}

void deWedge::InterpolateAttributes(float t, deWedge& a, deWedge& b) {
	demAssertf(a.m_Material == b.m_Material, "Materials must match to interpolate attributes");
	demAssertf(a.m_VertexData.TexCount == b.m_VertexData.TexCount, "TexCoord counts must match to interpolate attributes");
	if (a.m_VertexData.TexCount != b.m_VertexData.TexCount)
	{
		deMeshErrorPoint err;
		err.m_Point = a.m_VertexData.Pos;
		char buf[256];
		formatf(buf, "Interpolated between wedges with different tex coord counts (%d and %d)", a.m_VertexData.TexCount, b.m_VertexData.TexCount);
		err.m_Description = buf;
		deMesh::ReportError(err);
	}
	demAssertf(a.m_VertexData.TanBiCount == b.m_VertexData.TanBiCount, "TanBi counts must match to interpolate attributes");
	if (a.m_VertexData.TanBiCount != b.m_VertexData.TanBiCount)
	{
		deMeshErrorPoint err;
		err.m_Point = a.m_VertexData.Pos;
		char buf[256];
		formatf(buf, "Interpolated between wedges with different tanbi counts (%d and %d)", a.m_VertexData.TanBiCount, b.m_VertexData.TanBiCount);
		err.m_Description = buf;
		deMesh::ReportError(err);
	}

	m_Material = a.m_Material;

	m_VertexData.Pos.Lerp(t, a.m_VertexData.Pos, b.m_VertexData.Pos);
	m_VertexData.Nrm.Lerp(t, a.m_VertexData.Nrm, b.m_VertexData.Nrm);
	m_VertexData.Nrm.Normalize();
	m_VertexData.Cpv.Lerp(t, a.m_VertexData.Cpv, b.m_VertexData.Cpv);
    m_VertexData.Cpv2.Lerp(t, a.m_VertexData.Cpv2, b.m_VertexData.Cpv2);
    m_VertexData.Cpv3.Lerp(t, a.m_VertexData.Cpv3, b.m_VertexData.Cpv3);
	m_VertexData.TexCount = a.m_VertexData.TexCount;
	for(int i = 0; i < m_VertexData.TexCount; i++) {
		m_VertexData.Tex[i].Lerp(t, a.m_VertexData.Tex[i], b.m_VertexData.Tex[i]);
	}
	m_VertexData.TanBiCount = a.m_VertexData.TanBiCount;
	for(int i = 0; i < m_VertexData.TanBiCount; i++) {
		m_VertexData.TanBi[i].T.Lerp(t, a.m_VertexData.TanBi[i].T, b.m_VertexData.TanBi[i].T);
		m_VertexData.TanBi[i].T.Normalize();
		m_VertexData.TanBi[i].B.Lerp(t, a.m_VertexData.TanBi[i].B, b.m_VertexData.TanBi[i].B);
		m_VertexData.TanBi[i].B.Normalize();
	}

	// TODO: something with channel data
    
    // TODO: something with the binding data
    
    // TODO: something with the float blind data
}

deVertex::EdgeIterator& deVertex::EdgeIterator::operator ++() {
	if (vert) {
		if (edge) {
			edge = edge->GetNextCCW();
		}
		if (!edge || edge == vert->m_Edges[basisEdge]) {
			basisEdge++;
			if (basisEdge < vert->m_Edges.GetCount()) {
				edge = vert->m_Edges[basisEdge];
			}
			else {
				vert = NULL;
				basisEdge = -1;
				edge = NULL;
			}
		}
	}

	return *this;
}

deVertex::EdgeIterator deVertex::BeginEdges() {
	EdgeIterator iter;
	iter.vert = this;
	iter.basisEdge = 0;
	iter.edge = m_Edges[0];
	return iter;
}

/*
deVertex::EdgeIterator deVertex::EndEdges() {
	EdgeIterator iter;
	iter.vert = NULL;
	iter.basisEdge = -1;
	iter.edge = NULL;
	return iter;
}
*/
deVertex::VertexIterator& deVertex::VertexIterator::operator ++() {
	if (lastEdge) {
		lastEdge = false;
		++ei;
	}
	else if (!ei->GetNextCCW()) {
		lastEdge = true;
	}
	else {
		++ei;
		lastEdge = false;
	}
	return *this;
}

deVertex::VertexIterator deVertex::BeginVertices() {
	VertexIterator iter;
	iter.ei = BeginEdges();
	iter.lastEdge = false;
	return iter;
}
/*
deVertex::VertexIterator deVertex::EndVertices() {
	VertexIterator iter;
	iter.ei = EndEdges();
	iter.lastEdge = true;
	return iter;
}
*/
bool deVertex::SanityCheck()
{
	// vert must have at least 1 outgoing edge. Outgoing edge must reconnect to vert.
	// if vert is not on boundary, must be of at least degree 3

	demAssertf(m_Edges.GetCount() > 0, "Every vert must have one outgoing edge");

	if (m_Edges.GetCount() == 1) {
		demAssertf(FindDegree() >= 3, "Every vert must be degree three or more"); // Is this right?!?!
	}
	return true;
}

void deVertex::ReduceEdgeSet()
{
	if (m_Flags.IsSet(FLAG_EDGES_REDUCED)) {
		return; // already reduced
	}

	if (!IsOnBoundary()) {
		// just need to pick any edge as the basis edge then.
		deEdge* theEdge = GetEdge();
		m_Edges.Reset();
		m_Edges.Resize(1);
		m_Edges[0] = theEdge;
		return;
	}

	// Mark all edges as unprocessed
	// For each edge in the basis set, if already unprocessed, walk as far CW as possible. The mark all CCW edges from there as processed.
	// delete all processed edges from the set.

	/*
	for(int basisEdge = 0; basisEdge < m_Edges.GetCount(); basisEdge++) {
		m_Edges[basisEdge]->m_Flags.Clear(deEdge::FLAG_RES_PROCESSED);
	}
	*/

	for(int basisEdge = 0; basisEdge < m_Edges.GetCount(); basisEdge++) {
		if (m_Edges[basisEdge]->m_Flags.IsSet(deEdge::FLAG_RES_PROCESSED)) {
			continue;
		}

		// find the cw-most edge (if it exists, else pick any edge)
		deEdge* cwEdge = m_Edges[basisEdge]; // this will have the clockwise-most edge
		deEdge* next = cwEdge->GetNextCW();
		while(next && next != m_Edges[basisEdge]) {
			cwEdge = next;
			next = next->GetNextCW();
		}
		if (next) {
			// if we went in a loop, use the current edge as the "CW-most" edge.
			// That way we mark it as a keeper and all future edges in that loop
			// can be skipped over.
			cwEdge = m_Edges[basisEdge]; 
		}

		// Now mark any further CCW edge as processed
		for(deEdge* e = cwEdge->GetNextCCW(); e && e != cwEdge; e = e->GetNextCCW()) {
			e->m_Flags.Set(deEdge::FLAG_RES_PROCESSED);
		}

		// There must be a better way to do this. The problem is that cwEdge might not be one of the basis edges.
		// So we'd delete the edges we marked as redundant but not keep a pointer to cwEdge.
		// So now if cwEdge is not currently a basis edge, add it to the set.
		bool found = false;
		for(int i = 0; i < m_Edges.GetCount(); i++) {
			if (m_Edges[i] == cwEdge) {
				found = true;
				break;
			}
		}
		if (!found) {
			m_Edges.Grow() = cwEdge;
			cwEdge->m_Flags.Clear(deEdge::FLAG_RES_PROCESSED);
		}
	}

	for(int basisEdge = 0; basisEdge < m_Edges.GetCount(); basisEdge++) {
		if (m_Edges[basisEdge]->m_Flags.IsSet(deEdge::FLAG_RES_PROCESSED)) {
			m_Edges.Delete(basisEdge);
			basisEdge--;
		}
	}

	// Now clear all the FLAG_RES_PROCESSED flags
	for(EdgeIterator ei = BeginEdges(); ei != EndEdges(); ++ei) {
		ei->m_Flags.Clear(deEdge::FLAG_RES_PROCESSED);
	}

#if 0
	// so there's at least 1 edge discontinuity. For each edge, walk as far CW as possible, then 
	// eliminate edges from the list that are reachable through the start edge
	for(int basisEdge = 0; basisEdge < m_Edges.GetCount(); basisEdge++) {
		deEdge* cwEdge = m_Edges[basisEdge]; // this will have the clockwise-most edge
		deEdge* next = cwEdge->GetNextCW();
		while(next && next != m_Edges[basisEdge]) {
			cwEdge = next;
			next = next->GetNextCW();
		}
		m_Edges[basisEdge] = cwEdge;
//		startEdge = startEdge->GetNextCCW();
		if (cwEdge) {
			for(int i = basisEdge+1; i < m_Edges.GetCount(); i++) {
				deEdge* edge = cwEdge;
				while(edge && edge != m_Edges[i]) {
					edge = edge->GetNextCCW();
				}
				if (edge) {
					// m_Edges[i] was reachable through startEdge
					m_Edges.Delete(i);
					i--;
				}
			}
		}
	}
#endif
	// Edges all cleaned up, warn about >1 basis edge? (i.e. bowties)

	m_Flags.Set(FLAG_EDGES_REDUCED);
}

void deVertex::ReplaceEdge(deEdge* oldEdge, deEdge* newEdge)
{
	for(int i = 0; i < m_Edges.GetCount(); i++) {
		if (m_Edges[i] == oldEdge) {
			m_Flags.Clear(FLAG_EDGES_REDUCED);
			m_Edges[i] = newEdge;
			return;
		}
	}

}

int deVertex::FindDegree()
{
	int degree = 0;
	// for each basis edge
	for(int basisEdge = 0; basisEdge < m_Edges.GetCount(); basisEdge++) {
		degree++;
		deEdge* e = m_Edges[basisEdge]->GetNextCCW();
		while(e && e != m_Edges[basisEdge]) {
			e = e->GetNextCCW();
			degree++;
		}
	}
	return degree;
}

bool deVertex::IsOnBoundary()
{
	if (m_Flags.IsSet(FLAG_EDGES_REDUCED)) {
		// if edges have been reduced, m_Edges[0] should always lie on a boundary
		if (m_Edges.GetCount()) {
			return m_Edges[0]->GetNeighbor() == NULL;
		}
	}

	// special case for degenerate verts
	if (m_Edges.GetCount() == 0) {
		return true;
	}

	// mark all starting edges (except the first) as processed
	for(int i = 1; i < m_Edges.GetCount(); i++) {
		m_Edges[i]->m_Flags.Set(deEdge::FLAG_PROCESSED);
	}

	// start with any edge, marking every edge we come to as unprocessed
	deEdge* startEdge = GetEdge();
	deEdge* currEdge = startEdge->GetNextCCW();

	while(currEdge && currEdge != startEdge) {
		currEdge->m_Flags.Clear(deEdge::FLAG_PROCESSED);
		currEdge = currEdge->GetNextCCW();
	}

	if (currEdge == NULL) {
		// we've definitely hit a boundary. 
		return true;
	}
	else {
		// make sure we've touched every edge. If one is still marked processed, we must have a non-manifold vert (and thus 
		// be on a boundary... kind of)
		bool processedEdge = false;
		for(int i = 1; i < m_Edges.GetCount(); i++) {
			processedEdge |= m_Edges[i]->m_Flags.IsSet(deEdge::FLAG_PROCESSED);
			m_Edges[i]->m_Flags.Clear(deEdge::FLAG_PROCESSED);
		}
		return processedEdge;
	}
}

deEdge* deVertex::WalkAlongBoundary() {
	demAssertf(m_Flags.IsSet(FLAG_EDGES_REDUCED), "Edges must be reduced before walking along boundaries");
	if (m_Edges.GetCount() == 1 && IsOnBoundary()) {
		return m_Edges[0];
	}
	return NULL;
}

deEdge* deVertex::FindEdge(deVertex* to) {
	for(int basis = 0; basis < m_Edges.GetCount(); basis++) {
		deEdge* edge = m_Edges[basis];
		do {
			if (edge->m_Vertex == to) {
				return edge;
			}
			edge = edge->GetNextCCW();
		}
		while(edge && edge != m_Edges[basis]);
	}
	return NULL;
}

void deVertex::DisconnectEdge(deEdge* e) {
	if (!e) {
		return;
	}
	// check all basis edges. If any of them are e, change the basis to its CCW neighbor.
	for(int basis = 0; basis < m_Edges.GetCount(); basis++) {
		if (m_Edges[basis] == e) {
			m_Edges[basis] = m_Edges[basis]->GetNextCCW();
		}
		if (m_Edges[basis] == NULL) {
			// remove NULLs from basis list
			m_Edges.Delete(basis);
			basis--;
		}
	}
}

void deVertex::WriteCoreData(parStreamOut* stream)
{
/*	CoreVtxData data;
	data.pos.Set(m_Position);
	data.cpv.Set(m_Cpv);
	data.normal.Set(m_Normal);
	data.tex.Set(m_Tex);
	data.edge = (m_Edges.GetCount() == 1) ? m_Edges[0]->m_Index : -1;
	data.wedge = 0;
	data.flags = *reinterpret_cast<int*>(&m_Flags); 
*/
	CoreVtxData data;
	memset(&data, 0, sizeof(data));
	data.edge = (m_Edges.GetCount() == 1) ? m_Edges[0]->m_Index : -1;
	data.flags = *reinterpret_cast<int*>(&m_Flags);
	data.material = m_Wedges[0].GetMaterial();
	stream->WriteData(reinterpret_cast<char*>(&data), sizeof(data), false, parStream::BINARY);

	// TODO: something with the channel data
	stream->WriteData(reinterpret_cast<char*>(&m_Wedges[0].GetVertexData()), sizeof(mshVertex), false, parStream::BINARY);
}

bool deVertex::HasExtraData() {
	if (m_Edges.GetCount() == 1 && 
		(m_Wedges.GetCount() == 0 || m_Wedges.GetCount() == 1)) {
			return false;
		}
	return true;
}

void deVertex::WriteExtraData(parStreamOut* stream)
{
	// when is additional data necessary?

	if (m_Edges.GetCount() == 1 && 
		(m_Wedges.GetCount() == 0 || m_Wedges.GetCount() == 1)) {
			return;
	}

	parElement elt;

	elt.SetName("Vertex", false);
	elt.AddAttribute("Index", m_Index, false);

	stream->WriteBeginElement(elt);
	{
#if 0
		if (m_Edges.GetCount() > 1) {
			parElement edges;
			edges.SetName("Edges", false);
			edges.AddAttribute("NumEdges", m_Edges.GetCount());
			stream->WriteBeginElement(edges);
			int* indices = Alloca(int, m_Edges.GetCount());
			for(int i = 0; i < m_Edges.GetCount(); i++) {
				indices[i] = m_Edges[i]->m_Index;
			}
			stream->WriteData(reinterpret_cast<char*>(indices), sizeof(int) * m_Edges.GetCount(), true, parStream::INT_ARRAY);
			stream->WriteEndElement(edges);
		}
#endif
		if (m_Wedges.GetCount() > 1) {
			parElement wedges;
			wedges.SetName("Wedges", false);
			wedges.AddAttribute("NumWedges", m_Wedges.GetCount());
			stream->WriteBeginElement(wedges);
			// TODO: something with the channel data
			for(int i = 1; i < m_Wedges.GetCount(); i++) {
				int material = m_Wedges[i].GetMaterial();
				stream->WriteData(reinterpret_cast<const char*>(&material), sizeof(int), false, parStream::BINARY);
				stream->WriteData(reinterpret_cast<char*>(&m_Wedges[i].GetVertexData()), sizeof(mshVertex), false, parStream::BINARY);
			}
			stream->WriteEndElement(wedges);
		}
	}
	stream->WriteEndElement(elt);
}	


float deVertex::GetNeighborhoodArea()
{
	float area = 0.0f;
	for(EdgeIterator i = BeginEdges(); i != EndEdges(); ++i) {
		area += i->GetFaceArea();
	}
	return area;
}

#if 0
#define LERP_VECTOR_ATTR(AttrName) \
	m_Wedges[0].GetVertexData().AttrName.Lerp(t, a.m_Wedges[0].GetVertexData().AttrName, b.m_Wedges[0].GetVertexData().AttrName);

void deVertex::InterpolateAttributes(float t, deVertex& a, deVertex& b)
{
	m_Wedges[0].GetVertexData().TexCount = a.m_Wedges[0].GetVertexData().TexCount;
	m_Wedges[0].GetVertexData().TanBiCount = a.m_Wedges[0].GetVertexData().TanBiCount;

	LERP_VECTOR_ATTR(Nrm);
	m_Wedges[0].GetVertexData().Nrm.Normalize();

	LERP_VECTOR_ATTR(Cpv);

	for(int i = 0; i < a.m_Wedges[0].GetVertexData().TexCount; i++) {
		LERP_VECTOR_ATTR(Tex[i]);
	}

	for(int i = 0; i < a.m_Wedges[0].GetVertexData().TanBiCount; i++) {
		LERP_VECTOR_ATTR(TanBi[i].T);
		m_Wedges[i].GetVertexData().TanBi[i].T.Normalize();

		LERP_VECTOR_ATTR(TanBi[i].B);
		m_Wedges[i].GetVertexData().TanBi[i].B.Normalize();
	}

	// Do something clever with binding data
}
#endif

int deVertex::ClassifyVertex()
{
	if (IsLeft()) {
		return CLASSIFY_LEFT;
	}
	else if (IsRight()) {
		return CLASSIFY_RIGHT;
	}
	else if (IsCoplanar()) {
		return CLASSIFY_COPLANAR;
	}
	Quitf("Shouldn't get here!");
	return 0;
}

int deVertex::ClassifyNeighborhood()
{
	int ret = 0;
	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
		ret |= vi->ClassifyVertex();
	}
	return ret;
}

int deVertex::CountSharpEdges()
{
	int counter = 0;
	for(EdgeIterator ei = BeginEdges(); ei != EndEdges(); ++ei)
	{
		if (ei->IsSharp()) {
			counter++;
		}
	}
	// Normally that would miss the second boundary edge (the incoming one) if there was one so add it in here
	if (IsOnBoundary()) {
		counter++;
	}
	return counter;
}


inline mshVertex CloneAndZeroVertex(mshVertex& srcVertex)
{
	mshVertex vtx;
	vtx.Cpv.Zero();
	vtx.Cpv2.Zero();
	vtx.Cpv3.Zero();

	vtx.Pos = srcVertex.Pos;
	// Copy binding info (and don't average it later. Binding better not be different for two wedges on the same vert)
	for(int i = 0; i < mshMaxMatricesPerVertex; i++)
	{
		vtx.Binding.Mtx[i] = srcVertex.Binding.Mtx[i];
		vtx.Binding.Wgt[i] = srcVertex.Binding.Wgt[i];
	}
	vtx.TexCount = srcVertex.TexCount;
	vtx.TanBiCount = srcVertex.TanBiCount;
	vtx.FloatBlindDataCount = srcVertex.FloatBlindDataCount;

	return vtx;
}

inline void AddScaledVertex(mshVertex& destVtx, const mshVertex& srcVtx, float scale)
{
	destVtx.Nrm.AddScaled(srcVtx.Nrm, scale);
	destVtx.Cpv.AddScaled(srcVtx.Cpv, scale);
	destVtx.Cpv2.AddScaled(srcVtx.Cpv2, scale);
	destVtx.Cpv3.AddScaled(srcVtx.Cpv3, scale);
	for(int i = 0; i < destVtx.TanBiCount; i++)
	{
		destVtx.TanBi[i].B.AddScaled(srcVtx.TanBi[i].B, scale);
		destVtx.TanBi[i].T.AddScaled(srcVtx.TanBi[i].T, scale);
	}
	for(int i = 0; i < destVtx.TexCount; i++)
	{
		destVtx.Tex[i].AddScaled(srcVtx.Tex[i], scale);
	}
	for(int i = 0; i < destVtx.FloatBlindDataCount; i++)
	{
		// I hope blind data interpolates
		destVtx.FloatBlindData[i] += scale * srcVtx.FloatBlindData[i];
	}
}

inline void ConstrainVertex(mshVertex& vtx)
{
	vtx.Nrm.Normalize();
	for(int i = 0; i < vtx.TanBiCount; i++)
	{
		vtx.TanBi[i].B.Normalize();
		vtx.TanBi[i].T.Normalize();
	}
}

bool deVertex::Unsharpen()
{

	int numWedges = m_Wedges.GetCount();

	const int maxWedges = 16;
	demAssertf(numWedges < maxWedges, "Too many wedges on one vert to unsharpen, bump the deVertex::Unsharpen::maxWedges count or fix the vert at <%f,%f,%f>", GetPosition().x, GetPosition().y, GetPosition().z);

	// Take care of some of the easy cases first

	if (numWedges < 2) { // No sharp edges
		return false;
	}

	if (numWedges == 2 && 
		m_Wedges[0].GetMaterial() != m_Wedges[1].GetMaterial()) // Only a material boundary
	{
		return false;
	}

	bool didSomething = false;

	// Loop over each wedge, for each other wedge sharing that material
	// average up all of the attributes


	// Initialize some arrays
	atFixedArray<float, maxWedges> wedgeArea, wedgeWeight;
	atFixedArray<float, maxWedges> processedWedge;
	atFixedArray<int, maxWedges> newWedgeIndex;
	wedgeArea.Resize(numWedges);
	wedgeWeight.Resize(numWedges);
	processedWedge.Resize(numWedges);
	newWedgeIndex.Resize(numWedges);
	for(int i = 0; i < numWedges; i++)
	{
		wedgeArea[i] = 0.0f;
		wedgeWeight[i] = 0.0f;
		processedWedge[i] = false;
		newWedgeIndex[i] = i;
	}

#if 0
	// The average should be weighted based on the face area for each of the wedges
	// (arguably we could look at a larger region, this will work for now tho)
	for(EdgeIterator ei = BeginEdges(); ei != EndEdges(); ++ei)
	{
		wedgeArea[ei->m_Wedge] += ei->GetFaceArea();
	}
#else

	// Use the arc size for the wedge instead of face area (as above) so that
	// small obtuse triangles will weigh more
	for(EdgeIterator ei = BeginEdges(); ei != EndEdges(); ++ei)
	{
		Vector3 start, end, opp;
		ei->GetFaceVertPositions(start, end, opp);

		Vector3 ray1 = end - start;
		Vector3 ray2 = opp - start;
		wedgeArea[ei->m_Wedge] += ray1.Angle(ray2);
	}
#endif

	atFixedArray<int, maxWedges> wedgeSet;

	for(int i = 0; i < m_Wedges.GetCount(); i++)
	{
		if (processedWedge[i])
		{
			continue;
		}
		processedWedge[i] = true;

		wedgeSet.Reset();
		wedgeSet.Push(i);

		// Find the set of wedges that can be merged with this one
		int wedgeMtl = m_Wedges[i].GetMaterial();
		float totalSetArea = wedgeArea[i];
		int minTexCount = m_Wedges[i].GetVertexData().TexCount;
		int minTanBiCount = m_Wedges[i].GetVertexData().TanBiCount;

		for(int j = i+1; j < numWedges; j++)
		{
			if (!processedWedge[j] && m_Wedges[j].GetMaterial() == wedgeMtl)
			{
				wedgeSet.Push(j);
				processedWedge[j] = true;
				totalSetArea += wedgeArea[j];

				static bool texCountMismatchWarn = false;
				if (m_Wedges[j].GetVertexData().TexCount != m_Wedges[i].GetVertexData().TexCount)
				{
					if (!texCountMismatchWarn)
					{
						demWarningf("Mismatch in texture counts during unsharpen. Same material had two different texture counts (%d and %d)",
						m_Wedges[j].GetVertexData().TexCount,
						m_Wedges[i].GetVertexData().TexCount);
						texCountMismatchWarn = true;
					}
					minTexCount = Min(minTexCount, m_Wedges[j].GetVertexData().TexCount);
				}

				static bool tanBiCountMismatchWarn = false;
				if (m_Wedges[j].GetVertexData().TanBiCount != m_Wedges[i].GetVertexData().TanBiCount)
				{
					if (!tanBiCountMismatchWarn)
					{
						demWarningf("Mismatch in tangent/binormal counts during unsharpen. Same material had two different texture counts (%d and %d)",
							m_Wedges[j].GetVertexData().TanBiCount,
							m_Wedges[i].GetVertexData().TanBiCount);
						tanBiCountMismatchWarn = true;
					}
					minTanBiCount = Min(minTanBiCount, m_Wedges[j].GetVertexData().TanBiCount);
				}
			}
		}

		if (wedgeSet.GetCount() <= 1) // If there's only one wedge in this set, there's nothing to do
		{
			continue;
		}

		// Now we have a wedge set, merge them all together.
		mshVertex vtx = CloneAndZeroVertex(m_Wedges[i].GetVertexData());
		demAssertf(m_Wedges[i].GetChannelData() == NULL, "We don't support unsharpening for verts with channel data yet. Channel data will be lost");

		if (totalSetArea == 0.0f)
		{
			deMeshErrorPoint err(NULL, "Found a wedge with 0 area during unsharpen", this);
			deMesh::ReportError(err);
			demWarningf("Found a wedge with 0 area during unsharpen: %f %f %f", GetPosition().x, GetPosition().y, GetPosition().z);
		}

		for(int j = 0; j < wedgeSet.GetCount(); j++)
		{
			int wedgeIdx = wedgeSet[j];
			deWedge& wedge = m_Wedges[wedgeIdx];
			// What's the weight for this wedge?
			float wedgeWeight = totalSetArea != 0 ? wedgeArea[wedgeIdx] / totalSetArea : 1.0f / wedgeSet.GetCount();

			AddScaledVertex(vtx, wedge.GetVertexData(), wedgeWeight);

			newWedgeIndex[wedgeIdx] = i; // i (from above) will contain the new merged wedge info
		}
		ConstrainVertex(vtx);
		vtx.TexCount = minTexCount;
		vtx.TanBiCount = minTanBiCount;

		// Set wedge[i] to the new average wedge
		m_Wedges[i].SetVertexData(vtx);
		didSomething = true;
	}

	// Only reduce the wedge set if anything actually changed.
	if (didSomething)
	{
		// Point all the edges to the new wedges
		for(EdgeIterator ei = BeginEdges(); ei != EndEdges(); ++ei)
		{
			SetEdgeWedge(&*ei, newWedgeIndex[ei->m_Wedge]);
		}

		ReduceWedgeSet();
	}
	return didSomething;
}

void deVertex::SetEdgeWedge(deEdge* edge, int wedgeIdx)
{
	demAssertf(wedgeIdx >= 0 && wedgeIdx < m_Wedges.GetCount(), "Wedge index %d out of range [%d, %d]", wedgeIdx, 0, m_Wedges.GetCount());
	demAssertf(edge->GetStart() == this, "Edge is not an outgoing edge from this vertex");
	edge->m_Wedge = wedgeIdx;
}

int deVertex::FindOrAddWedge(deWedge& w) {
	demAssertf(w.GetPosition().IsClose(GetPosition(), deWedge::GetVertSnapDist()), "New wedge must be close to existing wedge");
	for(int i = 0; i < m_Wedges.GetCount(); i++) {
		if (w == m_Wedges[i]) {
			return i;
		}
	}
	m_Wedges.Grow(2) = w;
	// Make sure position on all wedges is the same
	m_Wedges[m_Wedges.GetCount()-1].SetPosition(m_Wedges[0].GetPosition());
	return m_Wedges.GetCount()-1;
}

void deVertex::ReduceWedgeSet() {

	// Say you have N existing wedges, [0, 1, 2, 3, 4, 5] and a wedge set [0,0,0,2,2,3,2,0,5,0]
	// First create a vector that says whether or not a given wedge is used [t, f, t, t, f, t]
	// Then create a mapping vector that maps the old indices to the new [0, -1, 1, 2, -1, 3] and reduce the old wedge set to [0, 1, 2, 3]
	// And remap all the wedge indices using the mapping vector [0,0,0,1,1,2,1,0,3,0]

	std::vector<int> usedWedges;
	usedWedges.resize(m_Wedges.GetCount(), false);
	for(EdgeIterator ei = BeginEdges(); ei != EndEdges(); ++ei) {
		usedWedges[ei->m_Wedge] = true;
	}

   	// The newIndices vector will contain -1 for any wedge that's not needed, and
	// 0..N for any wedge that is.
	std::vector<int> newIndices;
	newIndices.resize(m_Wedges.GetCount(), -1);
	int newNumWedges = 0;

	for(int i = 0; i < (int)usedWedges.size(); i++) {
		if (usedWedges[i]) {
			newIndices[i] = newNumWedges;
			if (newNumWedges != i) {
				m_Wedges[newNumWedges] = m_Wedges[i];
			}
			newNumWedges++;
		}
	}

	demAssertf(newNumWedges > 0, "Each vert needs at least one wedge");

	for(EdgeIterator ei = BeginEdges(); ei != EndEdges(); ++ei) {
		ei->m_Wedge = newIndices[ei->m_Wedge];
	}

	// newNumWedges will hold the # of new wedges. Truncate the array to this size.
	m_Wedges.Resize(newNumWedges);
}

#endif // MESH_LIBRARY