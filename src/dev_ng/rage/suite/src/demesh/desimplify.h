// 
// demesh/desimplify.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEMESH_DESIMPLIFY_H
#define DEMESH_DESIMPLIFY_H

#include "deconfig.h"
#include "deedge.h"

#include "vectormath/classes.h"


#if MESH_LIBRARY

namespace rage {

class deMesh;
class deVertex;
class Matrix44;  

class deSimplify {
public:
	deSimplify();
	~deSimplify();

	void Init(deMesh* mesh);

	void CalcGeomQuadricErrorMatrix(Mat44V_InOut quadricMatrix, ScalarV_InOut totalArea, deVertex* v);

	void CalcEdgeCollapseCost(deEdge* e, ScalarV_InOut err, float& errPos);

	int CollapseMinEdge();

	void InsertInHeap(deEdge* e);
	void RemoveFromHeap(deEdge* e);
	void UpdateEdgeError(deEdge* e);

	void DrawNextCollapse();

	// The matrix represents coefficients to a function that gives the sum of squared distances from pos to a set of planes
	void AddPlaneToQuadric(Mat44V_InOut quad, Vec4V_In plane, ScalarV_In weight) const;

	ScalarV_Out EvaluateQuadric(Mat44V_In quad, Vec3V_In point) const;

	void OffsetQuadric(Mat44V_InOut quad, Vec3V_In offset) const;

	float GetNextCollapseError();

	// Sets the scale for the dihedral error planes. The higher this is the more we discourage skinny triangles
	// and encourage more equalateral ones
	void SetDihedralScale(float scale) { m_DihedralScale = ScalarVFromF32(scale);}

	// Sets the scale for boundary edges. This higher this is, the less likely we are to move
	// verts away from boundary edges.
	void SetBoundaryScale(float scale) { m_BoundaryScale = ScalarVFromF32(scale);}

	// Sets the scale on any discontinuity edge. The higher this is the less likely we are
	// to change the shape of the discontinuity
	void SetDiscontinuityScale(float scale) { m_DiscontinuityScale = ScalarVFromF32(scale);}

	// Sets the dihedral scale for boundary verts. Use this to particularly discourage 
	// skinny triangles near the boundaries.
	void SetBoundaryDihedralScale(float scale) { m_BoundaryDihedralScale = ScalarVFromF32(scale);}

	deMesh* m_Mesh;

	deEdgeHeap* m_EcolHeap;

	ScalarV m_DihedralScale;
	ScalarV m_BoundaryScale;
	ScalarV m_BoundaryDihedralScale;
	ScalarV m_DiscontinuityScale;
};

}

#endif // MESH_LIBRARY

#endif
