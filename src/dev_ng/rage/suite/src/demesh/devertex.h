// 
// demesh/devertex.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEMESH_DEVERTEX_H
#define DEMESH_DEVERTEX_H

#include "deedge.h"

#include "atl/array.h"
#include "parsercore/stream.h"
#include "vector/vector3.h"


#if MESH_LIBRARY


namespace rage {

struct mshVertex;

class deWedge {
public:
	deWedge() 
	  : m_Material(0)
	  , m_Index(0)
	  , m_ChannelData(NULL)
	{
	}

	~deWedge()
	{
		delete m_ChannelData;
	}

	bool operator==(const deWedge& that) const;

	const Vector3& GetPosition() const {
		return m_VertexData.Pos;
	}

	void SetPosition(const Vector3& p) {
		m_VertexData.Pos.Set(p);
	}

	int GetMaterial() const {
		return m_Material;
	}

	void SetMaterial(int mtl) {
		m_Material = mtl;
	}

	mshChannelData* GetChannelData() {
		return m_ChannelData;
	}

	void SetChannelData(mshChannelData* data) {
		m_ChannelData = data;
	}
	
	mshVertex& GetVertexData() {
		return m_VertexData;
	}

	void SetVertexData(const mshVertex& data) {
		m_VertexData = data;
	}

	static float GetVertSnapDist() {
		return sm_VertSnapDist;
	}

	static void SetVertSnapDist(float dist) {
		sm_VertSnapDist = dist;
	}

	void InterpolateAttributes(float t, deWedge& a, deWedge& b);

	int GetIndex() const {
		return m_Index;
	}

	void SetIndex(int index) {
		m_Index = index;
	}

protected:

	int m_Material;
	mshVertex m_VertexData;
	int m_Index; // for serialization

	// has to hold all of the attributes for a vertex
	mshChannelData* m_ChannelData;

	static float sm_VertSnapDist;
	static float sm_NormalSnapDist;
	static float sm_ColorSnapDist;
	static float sm_TexSnapDist;
};

// PURPOSE:
//  A deVertex represents one vertex in a deMesh. It owns its outgoing edges, though it only maintains
//  a pointer to one of them. If a vertex is an internal vertex, it can point to any of its
//  outgoing edges, and certain mesh operations may change which outgoing edge it uses. For external
//  (boundary) vertices the edge it points to is always the edge along the boundary.
class deVertex {
public:
	deVertex(const deWedge& wedgeData) 
	: m_Index(-1)
//	, m_Normal(0.0f, 1.0f, 0.0f)
//	, m_Cpv(1.0f, 1.0f, 1.0f, 1.0f)
//	, m_Tex(0.0f, 0.0f)
//	, m_Position(0.0f, 0.0f, 0.0f)
	{
		m_Flags.Reset();
		m_Wedges.Grow(1) = wedgeData;
	}

	// The location for this vertex
	const Vector3& GetPosition() const;

	Vector2 GetPositionXZ();

	int GetMaterial(int i) const {
		return GetWedge(i).GetMaterial();
	}

	deWedge& GetWedge(int i) {
		return m_Wedges[i];
	}
	const deWedge& GetWedge(int i) const {
		return m_Wedges[i];
	}

	int GetNumWedges() const {
		return m_Wedges.GetCount();
	}

	void WriteCoreData(parStreamOut* stream);
	void WriteExtraData(parStreamOut* stream);

	bool HasExtraData();

	// One of the outgoing edges from this vertex. For manifolds, all other edges are reachable from this one
	deEdge* GetEdge();

	bool SanityCheck();

	void ReduceEdgeSet(); // reduces to a minimal edge set.

	void ReduceWedgeSet(); // removed unused wedges, reindexes all outgoing edges

	deEdge* FindEdge(deVertex* to);

	void DisconnectEdge(deEdge* e); // make sure deVertex doesn't hold any pointers to e

	void ReplaceEdge(deEdge* oldEdge, deEdge* newEdge);

	int FindOrAddWedge(deWedge& w);

	// Return number of outgoing edges. This should match # of incoming edges.
	// Only works after ReduceEdgeSet has been called
	int FindDegree();

	bool IsClose(const Vector3& pos, float tolerance) const;

	// Works whether or not ReduceEdgeSet has been called.
	// Could have a faster version for reduced edges. Then just need to test m_Edges[0]->m_Neighbor
	bool IsOnBoundary();

	// Returns the next edge along a mesh boundary. Will return null if the vertex is not a boundary vert _or_
	// if the vert is a non-manifold boundary (has multiple boundary edges)
	deEdge* WalkAlongBoundary();

	float GetNeighborhoodArea();

	bool IsCoplanar() const;
	void SetCoplanar();

	bool IsLeft() const;
	void SetLeft();

	bool IsRight() const;
	void SetRight();

	bool IsFixed() const;
	void SetFixed(bool val = true);

	bool IsCollapseAllowed() const;
	void SetCollapseAllowed(bool val = true);

	bool IsOnCutEdge() const;
	void SetOnCutEdge(bool val = true);

	// A "sharp" edge is the boundary of a discontinuity. An edge is sharp if the wedges to the left and right
	// are different on either end. Boundary edges are always sharp
	int CountSharpEdges();

	bool Unsharpen(); // Where possible, unsharpens verts by averaging their attributes and combining their wedges

	void SetEdgeWedge(deEdge* edge, int wedgeIdx);

	struct EdgeIterator
	{
		EdgeIterator& operator++();
		deEdge* operator->();
		deEdge& operator*();

		bool operator==(const EdgeIterator& other) const;
		bool operator!=(const EdgeIterator& other) const;

		EdgeIterator& operator=(const EdgeIterator& other);

		deVertex* vert;
		int basisEdge;
		deEdge* edge;

		static EdgeIterator sm_EndEdges;
	};

	EdgeIterator BeginEdges();
	const EdgeIterator& EndEdges() {return EdgeIterator::sm_EndEdges;}

	// Iterates over all vertices connected by an edge to this one
	struct VertexIterator
	{
		VertexIterator& operator++();
		deVertex* operator->();
		deVertex& operator*();

		bool operator==(const VertexIterator& other);
		bool operator!=(const VertexIterator& other);

		EdgeIterator ei;
		bool lastEdge;

		static VertexIterator sm_EndVertices;
	};

	VertexIterator BeginVertices();
	const VertexIterator& EndVertices() {return VertexIterator::sm_EndVertices;}

#if 0
	// This probably isn't valid once we switch to wedges and expand the attribute list...
	void InterpolateAttributes(float t, deVertex& a, deVertex& b);
#endif

	int ClassifyVertex();
	int ClassifyNeighborhood(); // Returns a bitfield containing bits from the CLASSIFY_ enums below. Does not take the current verts into account

	atArray<deEdge*> m_Edges; // elements beyond 0 only used during construction (might need to keep this around longer if the vertex is non-manifold)

	atArray<deWedge> m_Wedges;

	enum {
		FLAG_PROCESSED,
		FLAG_EDGES_REDUCED,
		FLAG_PENDING_DELETION,

		FLAG_LEFT,			// For cutting, is the vert on the left or right of the cut plane
		FLAG_RIGHT,			// If vert is coplanar, both left and right are set

		FLAG_FIXED_POSITION, // This vert can't be moved
		FLAG_NO_COLLAPSE, // This vert can't be part of a collapse (implies FIXED_POSITION)
		FLAG_ON_CUT_EDGE // This vert is on a cut edge
	};

	enum {
		CLASSIFY_LEFT = 1 << 0,
		CLASSIFY_RIGHT = 1 << 1,
		CLASSIFY_COPLANAR = 1 << 2
	};

	atFixedBitSet<32>	m_Flags;

	int					m_Index; // for serialization

	// The error quadric is symmetric and for each vertex row 3 and column 3 should be 0
	// vQv^t = 0 when v = [0,0,0,1] 
	// So we only need to store:
	// [ q11         0 ]
	// [ q12 q22     0 ]
	// [ q13 q23 q33 0 ] 
	// [  0   0   0  0 ]
	Vec4V				m_ErrorCoeffs0; // q11 q12 q13 0
	Vec4V				m_ErrorCoeffs1; // q33 q22 q23 0

	struct CoreVtxData {
		int edge;
		int flags;
		int material;
	};

	// Warning! ONLY the deMesh should be calling this function
	void SetPosition(const Vector3& pos);

	// HACK! These should be in wedges
//	Vector3 m_Normal;
//	Vector4 m_Cpv;
//	Vector2 m_Tex;

private:
//	Vector3 m_Position;

};

inline const Vector3& deVertex::GetPosition() const
{
	return GetWedge(0).GetPosition();
}

inline Vector2 deVertex::GetPositionXZ()
{
	Vector3 pos = GetPosition();
	return Vector2(pos.x, pos.z);
}

inline deEdge* deVertex::GetEdge()
{
	return m_Edges[0];
}

inline bool deVertex::IsClose(const Vector3& pos, float tolerance) const
{
	return GetPosition().IsClose(pos, tolerance);
}

inline deEdge* deVertex::EdgeIterator::operator->()
{
	return edge;
}

inline deEdge& deVertex::EdgeIterator::operator*()
{
	return *edge;
}

inline bool deVertex::EdgeIterator::operator==(const EdgeIterator& other) const
{
	if (vert == NULL && other.vert == NULL) {
		return true;
	}
	return (vert == other.vert) && (basisEdge == other.basisEdge) && (edge == other.edge); 
}

inline bool deVertex::EdgeIterator::operator!=(const EdgeIterator& other) const
{
	return !(*this == other);
}

inline deVertex::EdgeIterator& deVertex::EdgeIterator::operator=(const EdgeIterator& other)
{
	vert = other.vert;
	basisEdge = other.basisEdge;
	edge = other.edge;
	return *this;
}

inline deVertex* deVertex::VertexIterator::operator->()
{
	return lastEdge ? ei->GetNext()->GetEnd() : ei->GetEnd();
}

inline deVertex& deVertex::VertexIterator::operator*()
{
	return lastEdge ? *ei->GetNext()->GetEnd() : *ei->GetEnd();
}

inline bool deVertex::VertexIterator::operator==(const deVertex::VertexIterator& other)
{
	if (ei.vert == NULL && other.ei.vert == NULL) {
		return true;
	}
	return (ei == other.ei) && (lastEdge == other.lastEdge);
}

inline bool deVertex::VertexIterator::operator!=(const deVertex::VertexIterator& other)
{
	return !(*this == other);
}

inline bool deVertex::IsCoplanar() const
{
	return m_Flags.IsSet(FLAG_LEFT) && m_Flags.IsSet(FLAG_RIGHT);
}

inline void deVertex::SetCoplanar()
{
	m_Flags.Set(FLAG_LEFT); m_Flags.Set(FLAG_RIGHT);
}

inline bool deVertex::IsLeft() const
{
	return m_Flags.IsSet(FLAG_LEFT) && m_Flags.IsClear(FLAG_RIGHT);
}

inline void deVertex::SetLeft()
{
	m_Flags.Set(FLAG_LEFT); m_Flags.Clear(FLAG_RIGHT);
}

inline bool deVertex::IsRight() const
{
	return m_Flags.IsSet(FLAG_RIGHT) && m_Flags.IsClear(FLAG_LEFT);
}

inline void deVertex::SetRight()
{
	m_Flags.Set(FLAG_RIGHT); m_Flags.Clear(FLAG_LEFT);
}

inline bool deVertex::IsFixed() const
{
	return m_Flags.IsSet(FLAG_FIXED_POSITION);
}

inline void deVertex::SetFixed(bool val)
{
	m_Flags.Set(FLAG_FIXED_POSITION, val);
}

inline bool deVertex::IsCollapseAllowed() const
{
	return m_Flags.IsClear(FLAG_NO_COLLAPSE);
}

inline void deVertex::SetCollapseAllowed(bool val)
{
	m_Flags.Set(FLAG_NO_COLLAPSE, !val);
}

inline bool deVertex::IsOnCutEdge() const
{
	return m_Flags.IsSet(FLAG_ON_CUT_EDGE);
}

inline void deVertex::SetOnCutEdge(bool val)
{
	m_Flags.Set(FLAG_ON_CUT_EDGE, val);
}

inline void deVertex::SetPosition(const Vector3& pos) {
	for(int i = 0; i < m_Wedges.GetCount(); i++) {
		m_Wedges[i].SetPosition(pos);
	}
}


} // namespace rage

#endif // MESH_LIBRARY


#endif

