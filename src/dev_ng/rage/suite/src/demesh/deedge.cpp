// 
// demesh/deedge.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "deedge.h"
#include "devertex.h"

#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"

#if MESH_LIBRARY


using namespace rage;

void deEdge::GetDebugDrawVerts(Vector3& start, Vector3& end, Vector3& perp, float& len)
{
	Vector3 vertC;

	GetFaceVertPositions(start, end, vertC);

	Vector3 a2bdir;
	a2bdir.Normalize(start - end);
	float para = a2bdir.Dot(vertC - end);
	perp.SubtractScaled(vertC - end, a2bdir, para);
	perp.Normalize();

	Vector3 diff = (end - start);

	len = diff.Mag();

	end.AddScaled(start, diff, 0.90f);
	start.AddScaled(start, diff, 0.05f);

	perp.Scale(Min(0.2f, len * 0.02f));

}

void deEdge::FlipFaceWinding()
{
	deVertex* a;
	deVertex* b;
	deVertex* c;

	GetFaceVerts(a,b,c);

	deEdge* a2b = this;
	deEdge* b2c = GetNext();
	deEdge* c2a = GetPrev();

	int aWedge = a2b->m_Wedge;
	int bWedge = b2c->m_Wedge;
	int cWedge = c2a->m_Wedge;

	// a2b -> b2a
	a2b->m_Vertex = a;
	a2b->m_Prev = b2c;
	a2b->m_Wedge = bWedge;
	
	// b2c -> c2b
	b2c->m_Vertex = b;
	b2c->m_Prev = c2a;
	b2c->m_Wedge = cWedge;

	// c2a -> a2c
	c2a->m_Vertex = c;
	c2a->m_Prev = a2b;
	c2a->m_Wedge = aWedge;

	a->ReplaceEdge(a2b, c2a);
	b->ReplaceEdge(b2c, a2b);
	c->ReplaceEdge(c2a, b2c);
}

void deEdge::Write(parStreamOut* stream) {
//	parElement elt;
//	elt.SetName("Edge", false);
//	elt.AddAttribute("Index", m_Index, false);
//	elt.AddAttribute("Neighbor", GetNeighbor() ? GetNeighbor()->m_Index : -1, false);
//	elt.AddAttribute("Prev", GetPrev()->m_Index, false);
//	elt.AddAttribute("Vertex", GetEnd()->m_Index, false);
//	elt.AddAttribute("Wedge", m_Wedge->m_Index);

//	stream->WriteLeafElement(elt);

	EdgeConnections e;
	e.m_Neighbor = m_Neighbor ? m_Neighbor->m_Index : -1;
	e.m_Prev = m_Prev->m_Index;
	e.m_Vertex = m_Vertex->m_Index;
	e.m_Flags = *reinterpret_cast<int*>(&m_Flags);
	e.m_Wedge = m_Wedge;

	stream->WriteData(reinterpret_cast<char*>(&e), sizeof(EdgeConnections), false, parStream::INT_ARRAY);
}

float deEdge::GetFaceArea() {
	Vector3 pos0, pos1, pos2;

	GetFaceVertPositions(pos0, pos1, pos2);

	Vector3 edge1 = pos1 - pos0;
	Vector3 edge2 = pos2 - pos0;
	Vector3 normal;
	normal.Cross(edge1, edge2);

	return 0.5f * normal.Mag();
}

Vector4 deEdge::GetFacePlane()
{
	Vector4 outPlane;

	Vector3 pos0, pos1, pos2;

	GetFaceVertPositions(pos0, pos1, pos2);

	Vector3 edge1 = pos1 - pos0;
	Vector3 edge2 = pos2 - pos0;
	Vector3 normal;
	normal.Cross(edge1, edge2);

	normal.Normalize();

	outPlane.SetVector3(normal);
	outPlane.w = -normal.Dot(pos0);

//	outPlane.ComputePlane(pos0, normal);

	return outPlane;
}

float deEdge::GetLength() const
{
	return GetStart()->GetPosition().Dist(GetEnd()->GetPosition());
}

float deEdge::GetLength2() const
{
	return GetStart()->GetPosition().Dist2(GetEnd()->GetPosition());
}

void deEdge::GetFaceVerts(deVertex*& v1, deVertex*& v2, deVertex*& v3)
{
	v1 = GetStart();
	v2 = GetEnd();
	v3 = GetPrev()->GetStart();
}

void deEdge::GetFaceEdges(deEdge*& e1, deEdge*& e2, deEdge*& e3)
{
	e1 = this;
	e2 = GetNext();
	e3 = GetPrev();
}

void deEdge::GetFaceVertPositions(Vector3& startPos, Vector3& endPos, Vector3& oppositePos) const
{
	startPos = GetStart()->GetPosition();
	endPos = GetEnd()->GetPosition();
	oppositePos = GetPrev()->GetStart()->GetPosition();
}

Vector3 deEdge::GetFaceNormal()
{
	Vector3 pos0, pos1, pos2;

	GetFaceVertPositions(pos0, pos1, pos2);

	Vector3 edge1 = pos1 - pos0;
	Vector3 edge2 = pos2 - pos0;
	Vector3 normal;
	normal.Cross(edge1, edge2);

	normal.Normalize();

	return normal;
}

float deEdge::GetFaceNormalChange(const Vector3& newPos)
{
	Vector3 pos0, pos1, pos2;

	GetFaceVertPositions(pos0, pos1, pos2);

	Vector3 edge1A = pos1 - pos0;
	Vector3 edge2A = pos2 - pos0;
	Vector3 normalA;
	normalA.Cross(edge1A, edge2A);

	Vector3 edge1B = pos1 - newPos;
	Vector3 edge2B = pos2 - newPos;
	Vector3 normalB;
	normalB.Cross(edge1B, edge2B);

	// Disallow collapses that would create 0 area triangles
//	if (normalB.Mag2() == 0.0f) {
//		return -1;
//	}

	return normalA.Dot(normalB);
}

deEdge* deEdge::GetNextAlongBoundary()
{
	if (GetNeighbor()) {
		return NULL;
	}
	return m_Vertex->GetEdge();
}

deEdge* deEdge::GetPrevAlongBoundary()
{
	if (GetNeighbor()) {
		return NULL;
	}

	deEdge* e = this;
	deEdge* next = e->GetNextCCW();
	while(next) {
		e = next;
		next = next->GetNextCCW();
	}
	return e->GetPrev();
}

deEdge* deEdge::UnsafeGetNextAlongBoundary()
{
	return m_Vertex->GetEdge();
}

deEdge* deEdge::UnsafeGetPrevAlongBoundary()
{
	deEdge* e = this;
	deEdge* next = e->GetNextCCW();
	while(next && next != this) {
		e = next;
		next = next->GetNextCCW();
	}
	return e->GetPrev();
}

void deEdge::SetNeighbors(deEdge* a, deEdge* b) {
	if (a == NULL) {
		if (b == NULL) {
			// do nothing
			return;
		}
		else {
			b->m_Neighbor = NULL;
			b->m_Flags.Set(FLAG_PRIMARY_EDGE);
			return;
		}
	}
	else {
		if (b == NULL) {
			a->m_Neighbor = NULL;
			a->m_Flags.Set(FLAG_PRIMARY_EDGE);
			return;
		}
		else {
			// handled below
		}
	}

	a->m_Neighbor = b;
	b->m_Neighbor = a;

	a->m_Flags.Set(FLAG_PRIMARY_EDGE);
	b->m_Flags.Clear(FLAG_PRIMARY_EDGE);
}

bool deEdge::IsSharp() {
	if (IsOnBoundary()) {
		return true;
	}
	else {
		return (m_Wedge != GetNeighbor()->GetNext()->m_Wedge) || (GetNext()->m_Wedge != GetNeighbor()->m_Wedge);
	}
}

bool deEdge::IsStartSharp() {
	if (IsOnBoundary()) {
		return true;
	}
	else {
		return (m_Wedge != GetNeighbor()->GetNext()->m_Wedge);
	}
}

deWedge& deEdge::GetStartWedge() {
	return GetStart()->GetWedge(m_Wedge);
}

deWedge& deEdge::GetEndWedge() {
	return GetEnd()->GetWedge(GetNext()->m_Wedge);
}

float deEdge::GetStartAngle()
{
	Vector3 start, end, opposite;
	GetFaceVertPositions(start, end, opposite);
	Vector3 edgeVec = end - start;
	Vector3 oppositeVec = opposite - start;
	return edgeVec.Angle(oppositeVec);
}

#endif // MESH_LIBRARY
