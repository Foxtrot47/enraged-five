// 
// demesh/demesh.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEMESH_DEMESH_H
#define DEMESH_DEMESH_H

#include "deedge.h"
#include "devertex.h"

#include "atl/array.h"
#include "spatialdata/grid2d.h"


#include <limits.h>
#include <vector>

#if MESH_LIBRARY


namespace rage {

struct mshMaterial;
class mshMesh;

// PURPOSE:
//  A directed edge mesh data structure (based on Directed Edges - A Scalable Representation for 
//  Triangle Meshes by Campagna, Kobbelt and Seidel). A directed edge mesh different from the mshMesh
//  representation by storing topology information for the mesh. This information includes connections
//  from each vertex to its surrounding edges and faces, from each face to its neighboring verts, edges, and faces, and
//  connections along boundaries of the mesh.
// NOTES:
//  Our mesh representation contains 2 fundamental objects, vertices and half edges. A half edge is different from
//  a normal edge in that it represents a connection from A to B but not from B to A. Each half edge has a pointer
//  to its neighboring half edge. For boundary edges this neighbor pointer is NULL. In this way, we can distinguish
//  between the two faces adjacent to an edge. In addition, each half edge has a reference to its destination vertex
//  and to the previous edge around its face. The last bit of connectivity information we maintain is giving each
//  vertex a pointer to one of its outgoing edges, and we maintain the property that if a vertex lies on a 
//  mesh boundary, its outgoing edge pointer should point to the outgoing boundary edge.
//  There are some restictions on this mesh structure:
//  The mesh must be a bounded manifold. This is a manifold surface where some edges only have one face adjacent. The mesh
//  can contain multiple disconnected pieces, but at every edge there may only be 1 or 2 adjacent faces, and at every vertex
//  given one outgoing edge we can iterate around to get every other edge.
//  Two boundaries may not meet at a vertex. Imagine two holes that both share one vertex. This isn't allowed, and the
//  holes will be merged into one.
//  The surfaces must have a consistent winding order. Mobius strip geometry is not allowed. Faces that can't be inserted
//  in the mesh in either winding order will be discarded.
class deMesh
{
public:

	deMesh(const char* meshName = NULL);
	~deMesh();

	// Adds any existing vertices into the grid.
	void InitGrid(float minX, float maxX, float minY, float maxY, int xCells, int yCells);
	void ExpandGrid(float minX, float maxX, float minY, float maxY, int xCells, int yCells);
	void DestroyGrid();

	// Data that needs serialization:

	// For each edge:
	//  Write dest. index, prev edge index, neighbor edge index
	// For each vertex:
	//  Write vtx position, wedges, flags
	//  Write list of outgoing edges
	//  Write which edges are basis edges

	// PURPOSE: Loads a demesh from a file
	void Load(const char* filename);

	// PURPOSE: Saves a demesh to a file
	void Save(const char* filename);

	// PURPOSE: Saves the demesh as an mshMesh
	void GetMshMesh(mshMesh * mesh, bool tristrip = false);
	void SaveMshMesh(const char* filename, bool tristrip = false, bool textFormat = false);

	// PURPOSE: Saves the demesh as an rmcDrawable entity
	void SaveEntity( const char* filename, const char * typefilename, const char * meshfilename, const char *presetBasename, bool tristrip = false );

	// PURPOSE: Find the approximate bounding box for the mesh.
	// NOTES:
	//  If the mesh has a grid, it'll use the grid size, which should be roughly but not
	//  exactly the size of the mesh. If it doesn't have a grid it'll compute a bounding box.
	void FindApproxBounds(float& minX, float& maxX, float& minZ, float& maxZ);

	// PURPOSE: Find the actual bounding box for a mesh.
	void FindBounds(float& minX, float& maxX, float& minZ, float& maxZ);

	// PURPOSE: Find the actual bounding box for a mesh.
	void FindBounds(Vector3& outMin, Vector3& outMax);

	static deMesh* CreateFromMesh(mshMesh* mesh);

	// PURPOSE: Creates a mesh from an rmcDrawable entity (entity.type + mesh files + shader variable files)
	static deMesh* CreateFromEntity(const char* filename);

	// Can load a mshMesh, a deMesh, or an entity.type
	static deMesh* CreateFromFile(const char* filename);

	void Stitch(deMesh& otherMesh); // combines this and otherMesh, destroys otherMesh in the process
	void Combine(deMesh& otherMesh); // combines this and otherMesh, destroys otherMesh in the process - same as combine but uses a different algorithm

	struct MaterialData {
		mshMaterial* m_Material;
		ConstString m_ShaderPreset;		// name of the .sps file
		ConstString m_ShaderVariables;	// contents of the .sva file

		// new data (cmc)
		ConstString m_ShaderVarFilename;// name of the .sva file?

		ConstString m_MtlFileName;
		ConstString m_MtlTemplate;
		ConstString m_MtlVariables;

		MaterialData()
			: m_Material(NULL)
		{
		}

		MaterialData& operator=(const MaterialData& other) {
			if (!m_Material && other.m_Material) {
				m_Material = rage_new mshMaterial;
			}
			if (other.m_Material)
			{
				other.m_Material->CopyNongeometricData(*this->m_Material);
			}
			m_ShaderPreset = other.m_ShaderPreset;
			m_ShaderVariables = other.m_ShaderVariables;
			m_MtlFileName = other.m_MtlFileName;
			m_MtlTemplate = other.m_MtlTemplate;
			m_MtlVariables = other.m_MtlVariables;
			return *this;
		}

		bool operator==(const MaterialData& other);

		PAR_SIMPLE_PARSABLE;
	};

	void CreateFromMaterial(MaterialData& material);

	void RemoveUnusedMaterials();

	struct FaceInfo {
		FaceInfo();

		const Vector3& GetPosition(int i) {
			return inData[i].GetPosition();
		}

		deVertex* inoutVtx[3]; // If you know the face should use existing verts, assign those vert pointers here.
		deWedge inData[3];
		int inoutWedgeIndices[3];
	};

	// Returns true if we were able to add the face
	bool AddFace(FaceInfo& f);

	// This really just marks the vertex as deleted. Memory isn't reclaimed until you call CollectGarbage()
	void DeleteVertex(deVertex* v);

	// Deletes anything no longer pertinent in this mesh
	void CollectGarbage();

	void MoveVertex(deVertex& v, const Vector3& newPosition);

	deVertex* FindOrAddVertex(const deWedge& data, bool& outAdded, int& outWedgeIdx);

	deVertex* FindVertex(const Vector3& pos) const;

	// Like FindVertex, but given a position will return the closest boundary vertex within
	// the deVertex snap distance. It also returns the number of nearby verts that were rejected.
	// (which typically correspond to very very small triangles in the mesh that should be cleaned up)
	deVertex* FindClosestBoundaryVertex(const Vector3& pos, int& otherNearby) const;

	void ReduceEdgeSets();

	void FlipWinding();

	void DebugDraw(bool drawLeft = true, bool drawRight = true);
	void DebugDrawEdge(deEdge* edge, Color32 edgeColor);
	void DrawFast();
	void DrawSharps(bool drawBoundaries = true, bool drawSharps = true);
	void DrawSolid();
	void DrawSmooth();
	void DrawError();
	void DrawNormals();
	void DrawStars();
	void DrawTexCoords();

	struct EdgeResults {
		enum {
			EDGE_A_B,   
			EDGE_B_A,   
			EDGE_B_C,   
			EDGE_C_B,   
			EDGE_C_A,
			EDGE_A_C,
			NUM_EDGES
		};

		enum {
			WIND_NONE = 0x0,
			WIND_ABC  = 0x1,
			WIND_ACB  = 0x2,
			WIND_BOTH = 0x3
		};

		deEdge* m_Edges[NUM_EDGES];
		int m_NumEdges;
		int m_Winding; // Bit 0: is there an ABC winding edge. Bit 1: is there an ACB winding edge

		void AddEdge(deEdge* edge, int which);

		bool IsPair(int which) const;

	};
	
	struct VertexIterator {
		VertexIterator() {}
		VertexIterator(std::vector<deVertex*>::iterator begin, std::vector<deVertex*>::iterator end) : m_Iter(begin), m_End(end) {};
		VertexIterator& operator++()
		{
			do 
			{
				m_Iter++;
			}
			while(m_Iter != m_End && (*m_Iter)->m_Flags.IsSet(deVertex::FLAG_PENDING_DELETION));
			return *this;
		}
		deVertex** operator->() {return m_Iter;}
		deVertex*& operator*() {return *m_Iter;}

		bool operator==(const VertexIterator& other) {return m_Iter == other.m_Iter;}
		bool operator!=(const VertexIterator& other) {return m_Iter != other.m_Iter;}
		VertexIterator& operator=(const VertexIterator& other) {m_Iter = other.m_Iter; m_End = other.m_End; return *this;}

		std::vector<deVertex*>::iterator m_Iter;
		std::vector<deVertex*>::iterator m_End;
	};
	VertexIterator BeginVertices();
	VertexIterator EndVertices();

	struct ConstVertexIterator {
		ConstVertexIterator() {}
		ConstVertexIterator(std::vector<deVertex*>::const_iterator begin, std::vector<deVertex*>::const_iterator end) : m_Iter(begin), m_End(end) {};
		ConstVertexIterator& operator++()
		{
			do 
			{
				m_Iter++;
			}
			while(m_Iter != m_End && (*m_Iter)->m_Flags.IsSet(deVertex::FLAG_PENDING_DELETION));
			return *this;
		}
		deVertex* const * operator->() {return m_Iter;}
		deVertex* const & operator*() {return *m_Iter;}

		bool operator==(const ConstVertexIterator& other) {return m_Iter == other.m_Iter;}
		bool operator!=(const ConstVertexIterator& other) {return m_Iter != other.m_Iter;}
		ConstVertexIterator& operator=(const ConstVertexIterator& other) {m_Iter = other.m_Iter; m_End = other.m_End; return *this;}

		std::vector<deVertex*>::const_iterator m_Iter;
		std::vector<deVertex*>::const_iterator m_End;
	};
	ConstVertexIterator BeginConstVertices() const;
	ConstVertexIterator EndConstVertices() const;

	struct EdgeIterator {
		EdgeIterator& operator++();
		deEdge* operator->();
		deEdge& operator*();

		bool operator==(const EdgeIterator& other);

		bool operator!=(const EdgeIterator& other);

		EdgeIterator& operator=(const EdgeIterator& other);

		deMesh* mesh;
		VertexIterator vertex;
		deVertex::EdgeIterator edges;

	};
	EdgeIterator BeginEdges();
	EdgeIterator EndEdges();

	struct FaceEdgeIterator : public EdgeIterator {
		FaceEdgeIterator& operator++();
	};

	FaceEdgeIterator BeginFaceEdges();
	FaceEdgeIterator EndFaceEdges();

   	void FindEdges(deVertex* a, deVertex* b, deVertex* c, EdgeResults& e);
	void AddNewFace(deVertex* a, deVertex* b, deVertex* c, EdgeResults& e, int *wedgeIndices, bool abcOrder = true);

	void MarkVertsUnprocessed();

	void SnapToPlane(const Vector4& plane, float tolerance);

	// If addBoundary is true, the cut will be done and a new boundary edge will be added along the cut plane.
	// Also, after addBoundary all vertices will be marked either IsLeft() or IsRight(), and not coplanar

	// Coplanar verts will be marked with FLAG_PROCESSED
	void CutWithPlane(const Vector4& plane, bool addBoundary = false); // in-place version of the above

	void MarkVertsOnPlane(const Vector4& plane, int bitToSet, bool newValue = true);

	// Sets this bit on all verts with FLAG_PROCEESSED set, clears it on other verts if clearExisting is true
	void MarkProcessedVerts(int bitToSet, bool clearExisting = true); 

	deMesh* Partition(); // copies all the vertices marked "right" over to a new deMesh, removes them from this mesh

	void ClassifyVerts(const Vector4& plane); // mark verts as left, right, or coplanar

	void FindNearbyCells(const Vector3& pos, int indices[4]) const;

	deVertex* AddVertex(const deWedge& wedgeData);

	void Collapse(deEdge* e, float t);

	// Given an edge, splits the edge (at the parametric value t) and for each face
	// adjacent to the edge creates two faces
	// Assumes valid topology, maintains valid topology
	deVertex* SplitEdge(deEdge* e, float t);

	// Given two boundary edges, joins their endpoints together (if they aren't already
	// possibly removing 2 verts) and stitches them together
	void StitchEdge(deEdge* a, deEdge* b, bool meetMidway = false);

	bool CanCollapse(deEdge* e); // does topo tests to make sure collapse is legal

	bool ClassifyFaceForSplit(deEdge* e); // returns true if the face is on the left (or coplanar)

	int RemoveHoles(float angleTolerance=0.01f, float areaTolerance=0.1f);

	// Just adds v to the grid, doesn't check similarity to existing vertices
	void AddVertexToGrid(deVertex* v);
	void RemoveVertexFromGrid(deVertex* v);

	// Offsets entire mesh (if it had a grid, the grid is rebuilt)
	void OffsetMesh(const Vector3& offset);
	void ScaleMesh(const Vector3& scale);

	// Points all of the edges around oldVert to keptVert, and copies the wedge list over. The wedge list
	// must be reduced later
	// keptVert, oldVert need not be in the same mesh
	void CombineVerts(deVertex* keptVert, deVertex* oldVert); 

	// PURPOSE: Finds face with closest intersection
	// PARAMS: 
	//		origin - origin of the ray
	//		direction - direction of the ray
	//		t - if non-NULL, contains the t value of the intersecting face (or FLT_MAX if no intersections)
	// NOTES: This is not fast (tests every face)
	// RETURNS: An edge adjacent to this face.
	deEdge* PickNearest(Vector3::Param origin, Vector3::Param direction, float* t = NULL);

	// PURPOSE: Opens a new stream for writing errors
	static void OpenErrorStream(const char* filename);

	// PURPOSE: Closes the error stream
	static void CloseErrorStream();

	static void ReportError(const struct deMeshError& err);

	// need better structure for manipulation. Linked list maybe?
	std::vector<deVertex*> m_Vertices;

	// Each vert is placed in one cell. When searching, need to check 4 cells
	spdGrid2DContainer< atArray<deVertex*> >* m_VertexGrid;

	atArray<MaterialData> m_Materials; // materials w/o primitives, verts or channel data

	int m_SkippedTriangles;

	struct DrawData
	{
		bool m_Dirty; // does the draw data need to be updated?
		int m_TotalEdges; // each half edge counts once here
		int m_TotalFaces; // keep seperate count here in case topo is messed up
		int m_StartSharpEdges;
		int m_BoundaryEdges;
	};

	DrawData m_DrawData;

	ConstString m_Name; // Name of the mesh (mostly for error reporting)

	void GatherDrawData();
	void MarkDrawDataDirty();

	static const int sm_VersionNumber = 3;

	static parStreamOut* sm_ErrorStream;

public:
	static float sm_DebugDrawArrowScale;
	static float sm_DebugDrawNeighborSeperation;
	static float sm_DebugDrawPointerScale;
};


struct deMeshError
{
	deMeshError(){};
	deMeshError(const deMesh* mesh, const char* desc);
	virtual ~deMeshError() {}
	ConstString m_MeshName;
	ConstString m_Description;

	enum ErrorType
	{
		GENERAL,
		POINT,
		EDGE,
		FACE
	};

	virtual ErrorType GetType() {return GENERAL;}

	PAR_PARSABLE;
};

struct deMeshErrorPoint : public deMeshError
{
	deMeshErrorPoint(){};
	deMeshErrorPoint(const deMesh* mesh, const char* desc, const deVertex* vtx);
	deMeshErrorPoint(const deMesh* mesh, const char* desc, Vector3::Param point);

	virtual ErrorType GetType() {return POINT;}

	Vector3 m_Point;
	PAR_PARSABLE;
};

struct deMeshErrorEdge : public deMeshError
{
	deMeshErrorEdge(){};
	deMeshErrorEdge(const deMesh* mesh, const char* desc, const deEdge* edge);
	deMeshErrorEdge(const deMesh* mesh, const char* desc, Vector3::Param pointA, Vector3::Param pointB);

	virtual ErrorType GetType() {return EDGE;}

	Vector3 m_EdgeBegin;
	Vector3 m_EdgeEnd;
	PAR_PARSABLE;
};

struct deMeshErrorFace : public deMeshError
{
	deMeshErrorFace(){};
	deMeshErrorFace(const deMesh* mesh, const char* desc, const deEdge* edge);
	deMeshErrorFace(const deMesh* mesh, const char* desc, Vector3::Param pointA, Vector3::Param pointB, Vector3::Param pointC);

	virtual ErrorType GetType() {return FACE;}

	Vector3 m_PointA;
	Vector3 m_PointB;
	Vector3 m_PointC;
	PAR_PARSABLE;
};

struct deMeshErrorList 
{
	atArray<deMeshError*> m_Errors;
	PAR_SIMPLE_PARSABLE;
};


inline deMesh::FaceInfo::FaceInfo()
{
	inoutVtx[0] = inoutVtx[1] = inoutVtx[2] = NULL;
}

inline void deMesh::EdgeResults::AddEdge(deEdge* edge, int which)
{
	if (edge) {
		m_Edges[which] = edge;
		m_NumEdges++;
		m_Winding |= 1 << (which % 2);
	}
}

inline bool deMesh::EdgeResults::IsPair(int which) const
{
	int edgeItem = which & ~0x1; // clear the low bit
	return m_Edges[edgeItem] && m_Edges[edgeItem + 1];
}

inline deEdge* deMesh::EdgeIterator::operator->()
{
	return &(*edges);
}

inline deEdge& deMesh::EdgeIterator::operator*()
{
	return *edges;
}

inline bool deMesh::EdgeIterator::operator==(const deMesh::EdgeIterator& other)
{
	if (mesh == NULL && other.mesh == NULL) {
		return true;
	}
	return (mesh == other.mesh) && (vertex == other.vertex) && (edges == other.edges); 
}

inline bool deMesh::EdgeIterator::operator!=(const deMesh::EdgeIterator& other)
{
	return !(*this == other);
}

inline deMesh::EdgeIterator& deMesh::EdgeIterator::operator=(const deMesh::EdgeIterator& other)
{
	mesh = other.mesh;
	vertex = other.vertex;
	edges = other.edges;
	return *this;
}

inline deMesh::EdgeIterator deMesh::EndEdges()
{
	EdgeIterator iter;
	iter.mesh = NULL;
	iter.vertex = VertexIterator(NULL, NULL);
	return iter;
}

inline deMesh::VertexIterator deMesh::BeginVertices() 
{
	VertexIterator vi(m_Vertices.begin(), m_Vertices.end());
	if (m_Vertices.size() > 0 && m_Vertices[0]->m_Flags.IsSet(deVertex::FLAG_PENDING_DELETION))
	{
		++vi; // increments to the first valid (non-deleted) vertex
	}
	return vi;
}

inline deMesh::VertexIterator deMesh::EndVertices() 
{
	return VertexIterator(m_Vertices.end(), m_Vertices.end());
}

inline deMesh::ConstVertexIterator deMesh::BeginConstVertices() const 
{
	ConstVertexIterator vi(m_Vertices.begin(), m_Vertices.end());
	if (m_Vertices.size() > 0 && m_Vertices[0]->m_Flags.IsSet(deVertex::FLAG_PENDING_DELETION))
	{
		++vi; // increments to the first valid (non-deleted) vertex
	}
	return vi;
}

inline deMesh::ConstVertexIterator deMesh::EndConstVertices() const 
{
	return ConstVertexIterator(m_Vertices.end(), m_Vertices.end());
}

inline void deMesh::MarkDrawDataDirty() { m_DrawData.m_Dirty = true; }


} // namespace rage

#endif // MESH_LIBRARY


#endif
