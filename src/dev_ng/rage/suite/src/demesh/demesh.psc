<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>


<autoregister allInFile="true"/>
<structdef type="rage::deMesh::MaterialData">
  <string name="m_ShaderPreset" type="ConstString"/>
  <string name="m_ShaderVariables" type="ConstString"/>
  <string name="m_MtlFileName" type="ConstString"/>
  <string name="m_MtlTemplate" type="ConstString"/>
  <string name="m_MtlVariables" type="ConstString"/>
</structdef>

<structdef name="deMeshError" type="rage::deMeshError">
  <string name="m_MeshName" type="ConstString"/>
  <string name="m_Description" type="ConstString"/>
</structdef>

<structdef base="rage::deMeshError" name="deMeshPointError" type="rage::deMeshErrorPoint">
  <Vector3 name="m_Point"/>
</structdef>

<structdef base="rage::deMeshError" name="deMeshEdgeError" type="rage::deMeshErrorEdge">
  <Vector3 name="m_EdgeBegin"/>
  <Vector3 name="m_EdgeEnd"/>
</structdef>

<structdef base="rage::deMeshError" name="deMeshFaceError" type="rage::deMeshErrorFace">
  <Vector3 name="m_PointA"/>
  <Vector3 name="m_PointB"/>
  <Vector3 name="m_PointC"/>
</structdef>

<structdef type="rage::deMeshErrorList">
	<array name="m_Errors" type="atArray">
		<pointer policy="owner" type="rage::deMeshError"/>
	</array>
</structdef>

</ParserSchema>