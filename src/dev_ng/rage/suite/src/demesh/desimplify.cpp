// 
// demesh/desimplify.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "desimplify.h"

#include "demesh.h"
#include "devertex.h"

#include "grcore/im.h"
#include "grcore/light.h"
#include "profile/element.h"
#include "vector/matrix44.h"
#include "vectormath/legacyconvert.h"

#include <set>

#if MESH_LIBRARY


using namespace rage;

EXT_PF_TIMER(EcolCollapseTimer);
EXT_PF_TIMER(EcolErrorEvalTimer);
EXT_PF_TIMER(EcolFoldoverTimer);
EXT_PF_TIMER(EcolTopoCheckTimer);
EXT_PF_TIMER(EcolHeapOpsTimer);

// Edges with errors above this number are not considered for collapse
const float ErrorCutoff = 1.0e20f;
const ScalarV ErrorCutoffV = ScalarVFromF32(ErrorCutoff);

const float GiganticError = 1.0e30f;
const ScalarV GiganticErrorV = ScalarVFromF32(GiganticError);


deSimplify::deSimplify()
: m_Mesh(NULL)
, m_EcolHeap(NULL)
{
	SetDihedralScale(0.01f);
	SetBoundaryScale(1.0f);
	SetBoundaryDihedralScale(3.0f);
	SetDiscontinuityScale(1.0f);
}

deSimplify::~deSimplify()
{
	for(deMesh::EdgeIterator ei = m_Mesh->BeginEdges(); ei != m_Mesh->EndEdges(); ++ei)
	{
		ei->m_HeapNode = NULL;
	}
	delete m_EcolHeap;
	m_Mesh = NULL;
}

inline void deSimplify::AddPlaneToQuadric(Mat44V_InOut quad, Vec4V_In plane, ScalarV_In weight) const
{
	Mat44V singleQuad;
	
	// We want weight * plane * plane^T
	// = outer product of (weight * plane, plane)

	Vec4V scaledPlane = Scale(weight, plane);

	OuterProduct(singleQuad, scaledPlane, plane);

	Add(quad, quad, singleQuad);
}

inline ScalarV_Out deSimplify::EvaluateQuadric(Mat44V_In quad, Vec3V_In point) const 
{
	Vec4V point4(point, ScalarV(V_ONE));

	Vec4V errorCoeffs = Multiply(quad, point4);

	return Dot(point4, errorCoeffs);
}


// The derivation for OffsetQuadric:
// To evaluate a quadric function Qf and point v we have
// Qf(v) = v^t * Q * v
// For precision reasons we'd like to be able to compute the quadric somewhere else
// (near the origin) and still be able to evaluate it as if it were in its original location.
//
// So what we really want is a new quadric function Qo(v) such that Qo(v) = Qf(v + r) for some offset vector r
// Qo(v) = v^t * Qo * v = (v + r)^t * Qf * (v + r)
//
// r is a translation, so create a translation matrix R from it. Then we have
// v^t * Qo * v = (R * v)^t * Qf * (R * v)
//              = v^t * R^t * Qf * R * v
//       Qo     =       R^t * Qf * R
// 
// Expanding the multiplications out we get the code below, where only row 3 and column 3 of quad are changed.
//
// Also we're taking advantage of the fact that quad should be symmetric, so row 3 and column 3 will be
// the same after offsetting (which saves us from doing a transposition to get the rows and an extra set
// of dot products)

inline void deSimplify::OffsetQuadric(Mat44V_InOut quad, Vec3V_In offset) const
{	
	Vec4V offset4(offset, ScalarV(V_ONE));

	Vec4V products(V_ZERO);
	products.SetX(Dot(offset4, quad.GetCol0()));
	products.SetY(Dot(offset4, quad.GetCol1()));
	products.SetZ(Dot(offset4, quad.GetCol2()));
	products.SetW(Dot(offset4, quad.GetCol3()));

	products.SetW(Dot(offset4, products));

	quad.GetCol0Ref().SetW(products.GetX());
	quad.GetCol1Ref().SetW(products.GetY());
	quad.GetCol2Ref().SetW(products.GetZ());
	quad.SetCol3(products);
}

inline Vec3V_Out GetFaceNormalUnnormalized(deEdge* e)
{
	Vec3V pos0, pos1, pos2;

	e->GetFaceVertPositions(
		RC_VECTOR3(pos0), 
		RC_VECTOR3(pos1), 
		RC_VECTOR3(pos2));

	Vec3V edge1 = Subtract(pos1, pos0);
	Vec3V edge2 = Subtract(pos2, pos0);

	Vec3V normal = Cross(edge1, edge2);

	return normal;
}



inline Vec3V_Out GetFaceNormal(deEdge* e)
{
	return Normalize(GetFaceNormalUnnormalized(e));
}


// Not the true face plane, but the face plane as if e->GetStart() were at the origin
Vec4V_Out GetFacePlaneAtOrigin(deEdge* e)
{
	Vec3V normal = GetFaceNormal(e);
	return Vec4V(normal, ScalarV(V_ZERO));
}


ScalarV GetFaceArea(deEdge* e)
{
	Vec3V pos0, pos1, pos2;

	e->GetFaceVertPositions(
		RC_VECTOR3(pos0), 
		RC_VECTOR3(pos1), 
		RC_VECTOR3(pos2));

	Vec3V edge1 = Subtract(pos1, pos0);
	Vec3V edge2 = Subtract(pos2, pos0);

	Vec3V normal;
	normal = Cross(edge1, edge2);

	return Scale(Mag(normal), ScalarV(V_HALF));
}

void deSimplify::CalcGeomQuadricErrorMatrix(Mat44V_InOut quadricMatrix, ScalarV_InOut totalArea, deVertex* v)
{
	// Zero the matrix for this vertex
	quadricMatrix = Mat44V(V_ZERO);
	totalArea = ScalarV(V_ZERO);

	ScalarV boundDhScale = v->IsOnBoundary() ? m_BoundaryDihedralScale : ScalarV(V_ONE);
	ScalarV numTris(V_ZERO);
	
	Vec3V origin = VECTOR3_TO_VEC3V(v->GetPosition());

	for(deVertex::EdgeIterator ei = v->BeginEdges(); ei != v->EndEdges(); ++ei)
	{
		ScalarV triArea = GetFaceArea(&*ei);
		totalArea += triArea;
		numTris = Add(numTris, ScalarV(V_ONE));

		AddPlaneToQuadric(quadricMatrix, GetFacePlaneAtOrigin(&*ei), triArea);
	}

	for(deVertex::EdgeIterator ei = v->BeginEdges(); ei != v->EndEdges(); ++ei)
	{
		// Add dihedral plane
		if (!ei->IsOnBoundary()) {
			Vec3V edgeVec;
			edgeVec = RCC_VEC3V(ei->GetEnd()->GetPosition()) - 
						RCC_VEC3V(ei->GetStart()->GetPosition());

			Vec3V normal1 = GetFaceNormal(&*ei);
			Vec3V normal2 = GetFaceNormal(&(*ei->GetNeighbor()));

			Vec3V avgNormalA = normal1 + normal2;
			Vec3V avgNormal;

			avgNormal = Normalize(Cross(avgNormalA, edgeVec));

			// Used to do Vector4::ComputePlane here, but now the plane is 
			// always computed as if ei->GetStart were at 0, which means the dist from
			// the plane to the origin is always 0, so plane.w = 0
			Vec4V plane = Vec4V(avgNormal, ScalarV(V_ZERO));

			ScalarV weight = ei->IsSharp() ? m_DiscontinuityScale : m_DihedralScale;

			weight = Scale(weight, Dot(normal1, normal2));
			weight = Scale(weight, boundDhScale);

			AddPlaneToQuadric(quadricMatrix, plane, weight);
		}
	}

	if (v->IsOnBoundary()) {
		// Add in virtual planes perpendicular to the edge faces, so that we can collapse along a straight edge
		// but penalize corner collapses

		// What should the weight be?
		ScalarV avgArea = InvScale(totalArea, numTris);
		ScalarV weight = Scale(m_BoundaryScale, avgArea);

		deEdge* next = v->GetEdge();
		deEdge* prev = v->GetEdge()->GetPrevAlongBoundary();

		Vec3V edgeVec;
		Vec3V faceNormal;
		Vec3V normal;
		Vec4V plane;

		faceNormal = GetFaceNormalUnnormalized(next);
		edgeVec = Subtract(
			RCC_VEC3V(next->GetEnd()->GetPosition()),
			RCC_VEC3V(next->GetStart()->GetPosition()));

		normal = Normalize(Cross(edgeVec, faceNormal));

		// Used to do Vector4::ComputePlane here, but now the plane is 
		// always computed as if ei->GetStart were at 0, which means the dist from
		// the plane to the origin is always 0, so plane.w = 0
		plane = Vec4V(normal, ScalarV(V_ZERO));

		AddPlaneToQuadric(quadricMatrix, plane, weight);


		faceNormal = GetFaceNormalUnnormalized(prev);
		edgeVec = Subtract(
			RCC_VEC3V(prev->GetEnd()->GetPosition()),
			RCC_VEC3V(prev->GetStart()->GetPosition()));

		normal = Normalize(Cross(edgeVec, faceNormal));

		// Used to do Vector4::ComputePlane here, but now the plane is 
		// always computed as if ei->GetStart were at 0, which means the dist from
		// the plane to the origin is always 0, so plane.w = 0
		plane = Vec4V(normal, ScalarV(V_ZERO));

		AddPlaneToQuadric(quadricMatrix, plane, weight);
	}

#if __ASSERT && 0
	Mat44V quadricT;
	Transpose(quadricT, quadricMatrix);
	ScalarV epsilon(V_FLT_SMALL_4);
	demAssertf(IsCloseAll(quadricMatrix, quadricT, epsilon), "Quadric must be symmetric");
	demAssertf(IsEqualAll(quadricMatrix.GetCol3(), Vec4V(V_ZERO)), "Quadric's last row and column must be 0");
#endif

	v->m_ErrorCoeffs0 = quadricMatrix.GetCol0();
	v->m_ErrorCoeffs1 = GetFromTwo<Vec::Z2, Vec::Y1, Vec::Z1, Vec::W1>(quadricMatrix.GetCol1(), quadricMatrix.GetCol2());

	/*
	// Check that the quadric is nearly zero at V
	
	// Error = v'Qv = v * (q * v)
	float err = EvaluateQuadric(*quadricMatrix, v->GetPosition());

	if (err > 0.01f) {
		Printf("Bad quadric!\n");
	}

	if (err < -0.01f) {
		Printf("-Bad quadric!\n");
	}
	*/
}

Mat44V_Out GetQuadricFromVertex(deVertex* v)
{
	Mat44V quad;
	quad.GetCol0Ref() = v->m_ErrorCoeffs0;
	quad.GetCol1Ref() = GetFromTwo<Vec::Y1, Vec::Y2, Vec::Z2, Vec::W2>(v->m_ErrorCoeffs0, v->m_ErrorCoeffs1);
	quad.GetCol2Ref() = GetFromTwo<Vec::Z1, Vec::Z2, Vec::X2, Vec::W2>(v->m_ErrorCoeffs0, v->m_ErrorCoeffs1);
	quad.GetCol3Ref() = Vec4V(V_ZERO);
	return quad;
}

void deSimplify::CalcEdgeCollapseCost(deEdge* e, ScalarV_InOut err, float& errPos) {
	// Do this the slow way for now.

	if (!e->GetEnd()->IsCollapseAllowed() && !e->GetStart()->IsCollapseAllowed()) {
		// Non-modifiable edge
		err = GiganticErrorV;
		errPos = 0.0f;
		return;
	}

	PF_START(EcolTopoCheckTimer);
	bool collapsable = m_Mesh->CanCollapse(e);
	PF_STOP(EcolTopoCheckTimer);
	if (!collapsable) {
		err = GiganticErrorV;
		errPos = 0.0f;
		return;
	}

	// Get quadrics for each endpoint
	Mat44V startQuadric, endQuadric;
//	ScalarV startArea, endArea;
//	CalcGeomQuadricErrorMatrix(startQuadric, startArea, e->GetStart());
//	CalcGeomQuadricErrorMatrix(endQuadric, endArea, e->GetEnd());
	startQuadric = GetQuadricFromVertex(e->GetStart());
	endQuadric = GetQuadricFromVertex(e->GetEnd());

	Vector3 offset = e->GetStart()->GetPosition();

	OffsetQuadric(endQuadric, VECTOR3_TO_VEC3V( -(e->GetEnd()->GetPosition() - offset)));

	Add(startQuadric, startQuadric, endQuadric);

	// Check three places for minimum error. start, end, and (start + end) / 2
	// Should really be solving for position and attributes, but this'll work for a first pass

	// The difference between non-collapsable and fixed:
	// Non-collapsable verts must remain post-collapse
	// Fixed verts may be moved, but only if the other vert is also fixed

	Vector3 positions[3];
	float tVals[3];
	ScalarV errors[3];

	positions[0].Set(e->GetStart()->GetPosition());
	tVals[0] = 0.0f;
	errors[0] = ScalarV(V_ZERO);

	positions[2].Set(e->GetEnd()->GetPosition());
	tVals[2] = 1.0f;
	errors[2] = ScalarV(V_ZERO);

	positions[1].Average(positions[0], positions[2]);
	tVals[1] = 0.5f;
	errors[1] = ScalarV(V_ZERO);

	if (!e->GetStart()->IsCollapseAllowed())
	{
		errors[1] = errors[2] = GiganticErrorV; // we can't collapse _to_ positions 1 or 2
	}

	if (!e->GetEnd()->IsCollapseAllowed()) {
		errors[0] = errors[1] = GiganticErrorV; // we can't collapse _to_ positions 0 or 1
	}

	// if both endpoints are fixed, collapse is OK. If just one is, disallow collapse away from it
	if (e->GetStart()->IsFixed() && !e->GetEnd()->IsFixed()) {
		errors[1] = errors[2] = GiganticErrorV;
	}
	else if (e->GetEnd()->IsFixed() && !e->GetStart()->IsFixed()) {
		errors[0] = errors[1] = GiganticErrorV;
	}

	PF_START(EcolFoldoverTimer);
	for(int i = 0; i < 3; i++) {
		if (IsGreaterThanOrEqualAll(errors[i], ErrorCutoffV)) {
			// no sense measuring error, it's already too high
			continue;
		}

		// Penalize foldovers here
		// Loop over all faces connected to start and end (skipping the left and right faces)
		for(deVertex::EdgeIterator ei = e->GetStart()->BeginEdges(); ei != e->GetStart()->EndEdges(); ++ei)
		{
			if (&(*ei) == e || ei->GetPrev() == e->GetNeighbor()) {
				continue;
			}
			if (ei->GetFaceNormalChange(positions[i]) <= 0.0f) {
//				demWarningf("Collapse would flip face");
				errors[i] += GiganticErrorV;
				break;
			}

			// If the other two verts in the face are marked non-collapsable, don't collapse this edge
			if ((!e->GetStart()->IsCollapseAllowed() || !e->GetEnd()->IsCollapseAllowed()) && 
				!ei->GetEnd()->IsCollapseAllowed() && 
				!ei->GetNext()->GetEnd()->IsCollapseAllowed()) {
				errors[i] += GiganticErrorV;
				break;
			}

			// Ditto for fixed verts
			if ((e->GetStart()->IsFixed() || e->GetEnd()->IsFixed()) && 
				ei->GetEnd()->IsFixed() && 
				ei->GetNext()->GetEnd()->IsFixed()) {
				errors[i] += GiganticErrorV;
				break;
			}
		}

		for(deVertex::EdgeIterator ei = e->GetEnd()->BeginEdges(); ei != e->GetEnd()->EndEdges(); ++ei)
		{
			if (&(*ei) == e->GetNeighbor() || ei->GetPrev() == e) {
				continue;
			}
			if (ei->GetFaceNormalChange(positions[i]) <= 0.0f) {
//				demWarningf("Collapse would flip face");
				errors[i] += GiganticErrorV;
				break;
			}

			// If the other two verts in the face are marked non-collapsable, don't collapse this edge
			if ((!e->GetStart()->IsCollapseAllowed() || !e->GetEnd()->IsCollapseAllowed()) && 
				!ei->GetEnd()->IsCollapseAllowed() && 
				!ei->GetNext()->GetEnd()->IsCollapseAllowed()) {
				errors[i] += GiganticErrorV;
				break;
			}

			// Ditto for fixed verts
			if ((e->GetStart()->IsFixed() || e->GetEnd()->IsFixed()) 
				&& ei->GetEnd()->IsFixed() && 
				ei->GetNext()->GetEnd()->IsFixed()) {
				errors[i] += GiganticErrorV;
				break;
			}
		}

		if (IsGreaterThanOrEqualAll(errors[i], ErrorCutoffV)) {
			// no sense measuring error, it's already too high
			continue;
		}

		errors[i] += EvaluateQuadric(startQuadric, VECTOR3_TO_VEC3V(positions[i] - offset));
	}
	PF_STOP(EcolFoldoverTimer);

	err = errors[0];
	errPos = tVals[0];
	for(int i = 1; i < 3; i++) {
		if (IsLessThanAll(errors[i],err)) {
			err = errors[i];
			errPos = tVals[i];
		}
	}

	// Break ties, so we spread out the collapses a bit
//	err += g_ReplayRand.GetVaried(1.0f);

//	err += e->GetLength2();

//	if (e->IsOnBoundary()) {
//		err += e->GetLength2() * 100.0f;
//	}
}

void deSimplify::InsertInHeap(deEdge* e)
{
//	if (!e->m_Flags.IsSet(deEdge::FLAG_PRIMARY_EDGE)) {
//		return;
//	}
	demAssertf(!e->m_HeapNode, "edge is already in the heap");
	ScalarV error = ScalarVFromF32(e->m_Error);
	CalcEdgeCollapseCost(e, error, e->m_ErrorPos);
	e->m_Error = error.Getf();
	PF_START(EcolHeapOpsTimer);
	if (e->m_Error < ErrorCutoff) {
		m_EcolHeap->Insert(e->m_Error, e, &e->m_HeapNode);
	}
	else {
		e->m_HeapNode = NULL;
	}
	PF_STOP(EcolHeapOpsTimer);
}

void deSimplify::UpdateEdgeError(deEdge* e) {
//	if (!e->m_Flags.IsSet(deEdge::FLAG_PRIMARY_EDGE)) {
//		return;
//	}
	RemoveFromHeap(e);
	if (e->GetNeighbor())
	{
		RemoveFromHeap(e->GetNeighbor());
	}
	InsertInHeap(e->GetPrimaryEdge());
}

void deSimplify::RemoveFromHeap(deEdge* e)
{
	PF_FUNC(EcolHeapOpsTimer);
	if (e->m_HeapNode) {
		m_EcolHeap->Delete(e->m_HeapNode);
		e->m_HeapNode = NULL;
	}
	if (e->GetNeighbor() && e->GetNeighbor()->m_HeapNode)
	{
		m_EcolHeap->Delete(e->GetNeighbor()->m_HeapNode);
		e->GetNeighbor()->m_HeapNode = NULL;
	}
}

struct CompareEdges {
	bool operator()(const deEdge * const & a, const deEdge * const & b) {
		int diff = a->GetEnd()->m_Index - b->GetEnd()->m_Index;
		if (diff == 0) {
			return a->GetStart()->m_Index < b->GetStart()->m_Index;
		}
		else {
			return diff < 0;
		}
	}
};


int deSimplify::CollapseMinEdge() {
	// get min edge from heap

	float err;
	deEdge* minEdge;

	if (!m_EcolHeap->ExtractMin(err, minEdge)) {
		demWarningf("No edges to collapse");
		return -1;
	}

	if (err >= ErrorCutoff) {
		demWarningf("No more allowed collapses");
		return -1;
	}

	minEdge->m_HeapNode = NULL;

	PF_START(EcolTopoCheckTimer);
	bool okToCollapse = m_Mesh->CanCollapse(minEdge);
	PF_STOP(EcolTopoCheckTimer);

	if (okToCollapse) {
//		Printf("Error = %f\n", err);
		deVertex* keptVert = minEdge->GetStart();

		int removedFaces = minEdge->GetNeighbor() ? 2 : 1;

		// Remove edges from heap (edges around the minEdge face and edges around minEdge's neighbor's face)
		RemoveFromHeap(minEdge->GetPrev()->GetPrev());
		RemoveFromHeap(minEdge->GetPrev());
		if (minEdge->GetNeighbor()) {
			RemoveFromHeap(minEdge->GetNeighbor()->GetPrev()->GetPrev());
			RemoveFromHeap(minEdge->GetNeighbor()->GetPrev());
			RemoveFromHeap(minEdge->GetNeighbor());
		}

#if 0
		Printf("c %d %d %f\n", minEdge->GetStart()->m_Index, minEdge->GetEnd()->m_Index, minEdge->m_Error);
#endif

		PF_START(EcolCollapseTimer);
		m_Mesh->Collapse(minEdge, minEdge->m_ErrorPos);
		PF_STOP(EcolCollapseTimer);

		PF_START(EcolErrorEvalTimer);
		// Whenever a vertex is moved (and in this case we basically moved S and T to S')
		// we need to update the error quadrics on a bunch of verts and 
		// update the error on a bunch of edges. The error on a given edge E depends
		// on all the faces around E->Start and E->End. So therefore we need to update error for 
		// all edges which are 1 edge away from S
	
		Mat44V crapM;
		ScalarV crapV;
		CalcGeomQuadricErrorMatrix(crapM, crapV, keptVert);
		for(deVertex::VertexIterator vi = keptVert->BeginVertices(); vi != keptVert->EndVertices(); ++vi)
		{
			CalcGeomQuadricErrorMatrix(crapM, crapV, &*vi);
		}

		// build a set of edges
		std::set<deEdge*, CompareEdges> recomp_edges;
//		std::vector<deEdge*> recomp_edges;

		for (deVertex::VertexIterator vi = keptVert->BeginVertices(); vi != keptVert->EndVertices(); ++vi) {
			for(deVertex::EdgeIterator ei = vi->BeginEdges(); ei != vi->EndEdges(); ++ei) {
				recomp_edges.insert(ei->GetPrimaryEdge());
				recomp_edges.insert(ei->GetPrev()->GetPrimaryEdge());
//				UpdateEdgeError(&(*ei));
			}
		}

		for (std::set<deEdge*, CompareEdges>::iterator setiter = recomp_edges.begin(); setiter != recomp_edges.end(); ++setiter) {
			UpdateEdgeError(*setiter);
//			Printf("(%d,%d)  ", (*setiter)->GetStart()->m_Index, (*setiter)->GetEnd()->m_Index);
		}

//		UpdateEdgeError(&(*ei)); // outgoing
//		UpdateEdgeError(ei->GetPrev()); // incoming

		PF_STOP(EcolErrorEvalTimer);

		return removedFaces;
	}
	else {
		return 0;
	}
}

void deSimplify::DrawNextCollapse()
{
	if (!m_EcolHeap || !m_Mesh) {
		return;
	}

	deEdgeHeap::Node* node = m_EcolHeap->FindMin();
	if (node) {
		grcLightState::SetEnabled(false);

		Vector3 intermed;
		intermed.Lerp(0.8f, node->Data->GetStart()->GetPosition(), node->Data->GetEnd()->GetPosition());

		grcColor3f(1.0f, 0.0f, 0.0f);
		grcBegin(drawLineStrip, 5);
		grcVertex3f(node->Data->GetStart()->GetPosition() + Vector3(0.0f, 10.0f, 0.0f));
		grcVertex3f(node->Data->GetStart()->GetPosition());
		grcVertex3f(node->Data->GetEnd()->GetPosition());
		grcVertex3f(intermed + Vector3(0.0f, 5.0f, 0.0f));
		grcVertex3f(intermed);
		grcEnd();

		Vector3 newPos;
		newPos.Lerp(node->Data->m_ErrorPos, node->Data->GetStart()->GetPosition(), node->Data->GetEnd()->GetPosition());
		grcColor3f(1.0f, 1.0f, 0.0f);
		grcBegin(drawLineStrip, 2);
		grcVertex3f(newPos + Vector3(1.0f, 0.0f, 1.0f));
		grcVertex3f(newPos + Vector3(-1.0f, 0.0f, -1.0f));
		grcEnd();
		grcBegin(drawLineStrip, 2);
		grcVertex3f(newPos + Vector3(1.0f, 0.0f, -1.0f));
		grcVertex3f(newPos + Vector3(-1.0f, 0.0f, 1.0f));
		grcEnd();
	}
}

void deSimplify::Init(deMesh* mesh)
{
	m_Mesh = mesh;

	// count edges in mesh
	int edges = 0;
	for(deMesh::EdgeIterator ei = mesh->BeginEdges(); ei != mesh->EndEdges(); ++ei) {
		++edges;
		ei->m_HeapNode = NULL;
	}

	// Compute error quadrics for every vertex
	for(deMesh::VertexIterator vi = mesh->BeginVertices(); vi != mesh->EndVertices(); ++vi)
	{
		ScalarV crapV;
		Mat44V crapM;
		CalcGeomQuadricErrorMatrix(crapM, crapV, *vi);
	}

#if 0
	// Check (very slowly) for two verts with the same outgoing edge
	for(deMesh::EdgeIterator ei1 = mesh->BeginEdges(); ei1 != mesh->EndEdges(); ++ei1) {
		deMesh::EdgeIterator ei2 = ei1;
		++ei2;
		for(; ei2 != mesh->EndEdges(); ++ei2) {
			if (&(*ei1) == &(*ei2)) {
				Printf("Duplicate edge found.\n");
				Vector3 pos1 = ei1.mesh->m_Vertices[ei1.vertex]->GetPosition();
				Vector3 pos2 = ei2.mesh->m_Vertices[ei2.vertex]->GetPosition();

				Vector3 posES = ei1->GetStart()->GetPosition();
				Vector3 posEE = ei1->GetEnd()->GetPosition();
				Printf("  Edge from (%f %f %f) to (%f %f %f)\n", posES.x, posES.y, posES.z, posEE.x, posEE.y, posEE.z);
				Printf("  Vert 1 = %f %f %f\n", pos1.x, pos1.y, pos1.z);
				Printf("  Vert 1 = %f %f %f\n", pos2.x, pos2.y, pos2.z);
			}
		}
	}
#endif

	m_EcolHeap = rage_new deEdgeHeap(edges);

	for(deMesh::EdgeIterator ei = mesh->BeginEdges(); ei != mesh->EndEdges(); ++ei) {
		InsertInHeap(&(*ei));
	}
}

float deSimplify::GetNextCollapseError()
{
	deEdgeHeap::Node* node = m_EcolHeap->FindMin();

	return (node)? node->Key : 0.0f;
}

#endif // MESH_LIBRARY
