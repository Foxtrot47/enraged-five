// 
// demesh/demesh.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "demesh.h"

#if MESH_LIBRARY

#include "demesh_parser.h"
#include "deedge.h"
#include "devertex.h"

#include "atl/binmap.h"
#include "file/token.h"
#include "grcore/im.h"
#include "grcore/light.h"
#include "math/amath.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/streamxml.h"
#include "profile/element.h"
#include "profile/group.h"
#include "system/param.h"
#include "vector/geometry.h"

#include <vector>

RAGE_DEFINE_CHANNEL(Demesh);

// Process for adding a mshMesh to a deMesh
/*
1. For each face in the mshMesh
1.1. Look for the three verts in the deMesh
1.2. Add any rage_new verts
1.3. Add three rage_new edges w/ pointers to eachother
1.4. For existing verts, see if an edge connected them (?)
*/

using namespace rage;

PF_GROUP(DeMesh);

PF_TIMER(LoadVerts, DeMesh);
PF_TIMER(LoadEdges, DeMesh);
PF_TIMER(CreateEdges, DeMesh);

PARAM(nomshmtlcheck, "[demesh] When doing deMesh::MaterialData::operator== the material info (mshMaterial) from the .mesh file is ignored.");
PARAM(preservewinding, "[demesh] Preserve the current winding order when building or stitching meshes");

float deMesh::sm_DebugDrawArrowScale = 1.0f;
float deMesh::sm_DebugDrawNeighborSeperation = 1.0f;
float deMesh::sm_DebugDrawPointerScale = 0.3f;
parStreamOut* deMesh::sm_ErrorStream = NULL;

deMesh::EdgeIterator& deMesh::EdgeIterator::operator ++()
{
	++edges;
	if (edges == (*vertex)->EndEdges()) {

		++vertex;
		if (vertex != mesh->EndVertices()) {
			edges = (*vertex)->BeginEdges();
		}
		else {
			mesh = NULL;
		}
	}
	return *this;
}

deMesh::FaceEdgeIterator& deMesh::FaceEdgeIterator::operator ++()
{
	do {
		this->EdgeIterator::operator ++();
	} while(mesh && (this->operator *()).m_Flags.IsClear(deEdge::FLAG_FACE_EDGE));
	return *this;
}

deMesh::EdgeIterator deMesh::BeginEdges()
{
	if (m_Vertices.size() == 0) {
		return EndEdges();
	}
	EdgeIterator iter;
	iter.mesh = this;
	iter.vertex = iter.mesh->BeginVertices();
	iter.edges = (*iter.vertex)->BeginEdges();
	return iter;
}

deMesh::FaceEdgeIterator deMesh::BeginFaceEdges()
{
	if (m_Vertices.size() == 0) {
		return EndFaceEdges();
	}
	FaceEdgeIterator iter;
	iter.mesh = this;
	iter.vertex = iter.mesh->BeginVertices();
	iter.edges = (*iter.vertex)->BeginEdges();

	if (iter->m_Flags.IsClear(deEdge::FLAG_FACE_EDGE)) {
		++iter;
	}
	return iter;
}

deMesh::FaceEdgeIterator deMesh::EndFaceEdges()
{
	FaceEdgeIterator iter;
	iter.mesh = NULL;
	iter.vertex = VertexIterator(NULL, NULL);
	return iter;
}

deMesh::deMesh(const char* meshName)
: m_VertexGrid(NULL)
, m_SkippedTriangles(0)
, m_Name(meshName)
{
	MarkDrawDataDirty();
}

deMesh::~deMesh()
{
	DestroyGrid();
	for(int i = 0; i < m_Materials.GetCount(); i++) {
		delete m_Materials[i].m_Material;
	}

	// Deleting all edges kinda sucks, since there's no master list.
	std::vector<deEdge*> faces;
	FaceEdgeIterator lastFace = EndFaceEdges();
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != lastFace; ++fi)
	{
		faces.push_back(&(*fi));
	}
	for(std::vector<deEdge*>::iterator face = faces.begin(); face != faces.end(); ++face)
	{
		delete (*face)->GetPrev()->GetPrev();
		delete (*face)->GetPrev();
		delete (*face);
	}

//	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
	// Don't use the vertex iterator here because we want to visit every vertex, not just the 
	// non-"deleted" ones.
	for(size_t i = 0; i < m_Vertices.size(); i++)
	{
		if (m_Vertices[i])
		{
			delete m_Vertices[i];
		}
	}
}

void deMesh::FindNearbyCells(const Vector3& pos, int indices[4]) const {
	demAssertf(m_VertexGrid, "Can't find nearby cells without a vertex grid");
	indices[0] = indices[1] = indices[2] = indices[3] = -1;

	Vector2 pos2d(pos.x, pos.z);

	int x,y;

	if (!m_VertexGrid->CalcCellXY(x,y,pos2d)) {
		demWarningf("Vertex is outside of grid cell (%f, %f)", pos2d.x, pos2d.y);
		deMeshErrorPoint error(this, "Vertex is outside of grid cell", pos);
		ReportError(error);
		m_VertexGrid->CalcClampedCellX(x, pos2d.x);
		m_VertexGrid->CalcClampedCellY(y, pos2d.y);
	}

	Vector2 cellMin, cellMax;
	m_VertexGrid->CalcCellExtents(x,y,cellMin, cellMax);

	int ctr = 0;
	int xoff = 0, yoff = 0;
	indices[ctr++] = m_VertexGrid->CalcCellIndex(x,y);

	if (x > m_VertexGrid->GetMinCellX() && pos2d.x - cellMin.x < 2.0f * deWedge::GetVertSnapDist()) {
		indices[ctr++] = m_VertexGrid->CalcCellIndex(x-1,y);
		xoff = -1;
	}
	else if (x < m_VertexGrid->GetMaxCellX() && cellMax.x - pos2d.x < 2.0f * deWedge::GetVertSnapDist()) {
		indices[ctr++] = m_VertexGrid->CalcCellIndex(x+1,y);
		xoff = 1;
	}
	if (y > m_VertexGrid->GetMinCellY() && pos2d.y - cellMin.y < 2.0f * deWedge::GetVertSnapDist()) {
		indices[ctr++] = m_VertexGrid->CalcCellIndex(x,y-1);
		yoff = -1;
	}
	else if (y < m_VertexGrid->GetMaxCellY() && cellMax.y - pos2d.y < 2.0f * deWedge::GetVertSnapDist()) {
		indices[ctr++] = m_VertexGrid->CalcCellIndex(x,y+1);
		yoff = 1;
	}

	if (ctr == 3) {
		// We added a vert and horiz neighbor cell, so add a corner cell too
		indices[ctr++] = m_VertexGrid->CalcCellIndex(x + xoff, y + yoff);
	}

}

deVertex* deMesh::FindVertex(const Vector3& pos) const {
    if (m_VertexGrid)
    {
        // find the grid cell (or cells) that the vertex could be in
        int indices[4];

        FindNearbyCells(pos, indices);

        for(int i = 0; i < 4; i++) {
            if (indices[i] >= 0) {
                const atArray<deVertex*>& verts = m_VertexGrid->GetData(indices[i]);
                for(int v = 0; v < verts.GetCount(); v++) {
                    deVertex* vert = verts[v];
					demAssertf(vert->m_Flags.IsClear(deVertex::FLAG_PENDING_DELETION), 
						"A deleted vert was left in the vtx grid - coords are %f, %f, %f",
						vert->GetPosition().x, vert->GetPosition().y, vert->GetPosition().z);
                    if (vert->IsClose(pos, deWedge::GetVertSnapDist())) {
                        return vert;
                    }
                }
            }
        }
    }
    else {
        for(ConstVertexIterator vi = BeginConstVertices(); vi != EndConstVertices(); ++vi)
        {
            if ((*vi)->IsClose(pos, deWedge::GetVertSnapDist())) {
                return *vi;
            }
        }
    }
    return NULL;

}

deVertex* deMesh::FindClosestBoundaryVertex(const Vector3& pos, int& otherNearby) const {
    otherNearby = 0;

    deVertex* minVert = NULL;
    float minDist = FLT_MAX;

    if (m_VertexGrid)
    {
        // find the grid cell (or cells) that the vertex could be in
        int indices[4];

        FindNearbyCells(pos, indices);

        for(int i = 0; i < 4; i++) {
            if (indices[i] >= 0) {
                const atArray<deVertex*>& verts = m_VertexGrid->GetData(indices[i]);
                for(int v = 0; v < verts.GetCount(); v++) {
                    deVertex* vert = verts[v];
                    if (vert->IsClose(pos, deWedge::GetVertSnapDist())) {
                        otherNearby++;
						if (vert->IsOnBoundary()) {
							demAssertf(vert->m_Flags.IsClear(deVertex::FLAG_PENDING_DELETION), 
								"A deleted vert was left in the vtx grid - coords are %f, %f, %f",
								vert->GetPosition().x, vert->GetPosition().y, vert->GetPosition().z);
                            float dist = vert->GetPosition().Dist2(pos);
                            if (dist < minDist) {
                                minDist = dist;
                                minVert = vert;
                            }
                        }
                    }
                }
            }
        }
    }
    else {
        for(ConstVertexIterator vi = BeginConstVertices(); vi != EndConstVertices(); ++vi)
        {
            if ((*vi)->IsClose(pos, deWedge::GetVertSnapDist())) {
                otherNearby++;
                if ((*vi)->IsOnBoundary()) {
                    float dist = (*vi)->GetPosition().Dist2(pos);
                    if (dist < minDist) {
                        minDist = dist;
                        minVert = (*vi);
                    }
                }
            }
        }
    }

    if (minVert) {
        otherNearby--; // don't count the one we found
    }
    return minVert;
}

deVertex* deMesh::AddVertex(const deWedge& wedge) {
	MarkDrawDataDirty();
	deVertex* vert = rage_new deVertex(wedge);
	m_Vertices.push_back(vert);
	AddVertexToGrid(vert);
	return vert;
}

deVertex* deMesh::FindOrAddVertex(const deWedge& wedge, bool& added, int& wedgeIdx) {
	MarkDrawDataDirty();
	deVertex* vert = FindVertex(wedge.GetPosition());
	added = false;
	if (!vert) {
		vert = AddVertex(wedge);
		added = true;
		wedgeIdx = 0;
	}
	else {
		bool found = false;
		for(int i = 0; i < vert->m_Wedges.GetCount(); i++) {
			if (wedge == vert->m_Wedges[i]) {
				wedgeIdx = i;
				found = true;
				break;
			}
		}
		if (!found) {
			vert->m_Wedges.Grow(1) = wedge;
			wedgeIdx = vert->m_Wedges.GetCount() - 1;
		}
	}
	return vert;
}

void deMesh::MoveVertex(deVertex& v, const Vector3& newPosition)
{
	MarkDrawDataDirty();
	RemoveVertexFromGrid(&v);
	v.SetPosition(newPosition);
	AddVertexToGrid(&v);
}

void deMesh::MarkVertsUnprocessed()
{
	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
		(*vi)->m_Flags.Clear(deVertex::FLAG_PROCESSED);
	}
}

void deMesh::FindEdges(deVertex* a, deVertex* b, deVertex* c, EdgeResults& e) {
	for(int i = 0; i < EdgeResults::NUM_EDGES; i++) {
		e.m_Edges[i] = NULL;
	}
	e.m_Winding = EdgeResults::WIND_NONE;
	e.m_NumEdges = 0;

	deEdge* edge;
	edge = a->FindEdge(b);
	if (edge) {
		e.AddEdge(edge, EdgeResults::EDGE_A_B);
		e.AddEdge(edge->GetNeighbor(), EdgeResults::EDGE_B_A);
	}
	else {
		e.AddEdge(b->FindEdge(a), EdgeResults::EDGE_B_A);
	}

	edge = b->FindEdge(c);
	if (edge) {
		e.AddEdge(edge, EdgeResults::EDGE_B_C);
		e.AddEdge(edge->GetNeighbor(), EdgeResults::EDGE_C_B);
	}
	else {
		e.AddEdge(c->FindEdge(b), EdgeResults::EDGE_C_B);
	}

	edge = c->FindEdge(a);
	if (edge) {
		e.AddEdge(edge, EdgeResults::EDGE_C_A);
		e.AddEdge(edge->GetNeighbor(), EdgeResults::EDGE_A_C);
	}
	else {
		e.AddEdge(a->FindEdge(c), EdgeResults::EDGE_A_C);
	}
}

void deMesh::AddNewFace(deVertex* a, deVertex* b, deVertex* c, EdgeResults& e, int *wedgeIndices, bool abcOrder)
{
	MarkDrawDataDirty();
	// Adding verts in X->Y->Z order. XYZ could be ABC or could be ACB, depending on abcOrder

	deVertex* x;
	deVertex* y;
	deVertex* z;

	if (abcOrder) {
		x = a;
		y = b;
		z = c;
	}
	else {
		x = a;
		y = c;
		z = b;
	}

	deEdge* x2y = rage_new deEdge;
	deEdge* y2z = rage_new deEdge;
	deEdge* z2x = rage_new deEdge;

	x2y->m_Vertex = y;
	x2y->m_Prev = z2x;
	x->m_Edges.Grow(4) = x2y;
	x->m_Flags.Clear(deVertex::FLAG_EDGES_REDUCED);
	x2y->m_Wedge = wedgeIndices[0];

	y2z->m_Vertex = z;
	y2z->m_Prev = x2y;
	y->m_Edges.Grow(4) = y2z;
	y->m_Flags.Clear(deVertex::FLAG_EDGES_REDUCED);
	y2z->m_Wedge = abcOrder ? wedgeIndices[1] : wedgeIndices[2];

	z2x->m_Vertex = x;
	z2x->m_Prev = y2z;
	z->m_Edges.Grow(4) = z2x;
	z->m_Flags.Clear(deVertex::FLAG_EDGES_REDUCED);
	z2x->m_Wedge = abcOrder ? wedgeIndices[2] : wedgeIndices[1];

	// Mark one edge as the face edge
	x2y->m_Flags.Set(deEdge::FLAG_FACE_EDGE);

	// Check for neighbors

	// x2y could be a2b or a2c, so neighbor is b2a or c2a
	deEdge::SetNeighbors(x2y, e.m_Edges[abcOrder ? EdgeResults::EDGE_B_A : EdgeResults::EDGE_C_A]);

	// y2z could be b2c or c2b, so neighbor is c2b or b2c
	deEdge::SetNeighbors(y2z, e.m_Edges[abcOrder ? EdgeResults::EDGE_C_B : EdgeResults::EDGE_B_C]);

	// z2x could be c2a or b2a, so neighbor is a2c or a2b
	deEdge::SetNeighbors(z2x, e.m_Edges[abcOrder ? EdgeResults::EDGE_A_C : EdgeResults::EDGE_A_B]);
}

bool deMesh::AddFace(FaceInfo& f) {
	MarkDrawDataDirty();

	bool added[3];
	added[0] = added[1] = added[2] = false;
	for(int i = 0; i < 3; i++) {
		if (f.inoutVtx[i] == NULL) {
			// Not in cache, check in big list. Add new vert if necessary
			f.inoutVtx[i] = FindOrAddVertex(f.inData[i], added[i], f.inoutWedgeIndices[i]);
		}
	}

	// First, discard any degenerate triangles

	if (f.inoutVtx[0] == f.inoutVtx[1] || f.inoutVtx[1] == f.inoutVtx[2] || f.inoutVtx[2] == f.inoutVtx[0]) {

		for(int i = 0; i < 3; i++) {
			if (added[i]) {
				DeleteVertex(f.inoutVtx[i]);
				f.inoutVtx[i] = NULL;
			}
		}
		return false;
	}

	// Count how many existing edges among verts a,b,c
	EdgeResults edges;
	FindEdges(f.inoutVtx[0], f.inoutVtx[1], f.inoutVtx[2], edges);

	switch(edges.m_Winding) {
	case EdgeResults::WIND_NONE:
		AddNewFace(f.inoutVtx[0], f.inoutVtx[1], f.inoutVtx[2], edges, f.inoutWedgeIndices);
		break;
	case EdgeResults::WIND_ABC:
		// existing edges wind in ABC order, so new edges should wind in ACB
		if (PARAM_preservewinding.Get())
		{
			demWarningf("Can't add tri with current winding order!");
			demWarningf("verts: (%f, %f, %f), (%f, %f, %f), (%f, %f, %f)",
				f.inData[0].GetPosition().x,
				f.inData[0].GetPosition().y,
				f.inData[0].GetPosition().z,
				f.inData[1].GetPosition().x,
				f.inData[1].GetPosition().y,
				f.inData[1].GetPosition().z,
				f.inData[2].GetPosition().x,
				f.inData[2].GetPosition().y,
				f.inData[2].GetPosition().z
				);
			deMeshErrorFace error(this, "Adding tri would create non-manifold mesh!",
				f.inData[0].GetPosition(), f.inData[1].GetPosition(), f.inData[2].GetPosition());
			ReportError(error);

			for(int i = 0; i < 3; i++) {
				if (added[i]) {
					DeleteVertex(f.inoutVtx[i]);
					f.inoutVtx[i] = NULL;
				}
			}
			return false;
		}
		else
		{
			AddNewFace(f.inoutVtx[0], f.inoutVtx[1], f.inoutVtx[2], edges, f.inoutWedgeIndices, false);
		}
		break;
	case EdgeResults::WIND_ACB:
		// existing edges wind in ACB order, so new edges should wind in ABC
		AddNewFace(f.inoutVtx[0], f.inoutVtx[1], f.inoutVtx[2], edges, f.inoutWedgeIndices);
		break;
	case EdgeResults::WIND_BOTH:
		// A few possible cases here. If an edge has a neighbor, we'd be trying to create a non-manifold mesh
		// Else, we have boundary edges where one or more needs to have its winding reversed.

		if (edges.IsPair(EdgeResults::EDGE_A_B) ||
			edges.IsPair(EdgeResults::EDGE_B_C) ||
			edges.IsPair(EdgeResults::EDGE_C_A))
		{
			demWarningf("Adding tri would create non-manifold mesh!");
			demWarningf("verts: (%f, %f, %f), (%f, %f, %f), (%f, %f, %f)",
				f.inData[0].GetPosition().x,
				f.inData[0].GetPosition().y,
				f.inData[0].GetPosition().z,
				f.inData[1].GetPosition().x,
				f.inData[1].GetPosition().y,
				f.inData[1].GetPosition().z,
				f.inData[2].GetPosition().x,
				f.inData[2].GetPosition().y,
				f.inData[2].GetPosition().z
				);
			deMeshErrorFace error(this, "Adding tri would create non-manifold mesh!",
				f.inData[0].GetPosition(), f.inData[1].GetPosition(), f.inData[2].GetPosition());
			ReportError(error);

			for(int i = 0; i < 3; i++) {
				if (added[i]) {
					DeleteVertex(f.inoutVtx[i]);
					f.inoutVtx[i] = NULL;
				}
			}

			return false;
		}
		else {
			// Technically should be able to flip the winding for one of the existing faces
			// (and all its neighbors) unless we have non-orientable (Mobius like) geometry.
//			Printf("Case not handled yet, need to flip winding order for some subset of the mesh\n");
			m_SkippedTriangles++;

			for(int i = 0; i < 3; i++) {
				if (added[i]) {
					DeleteVertex(f.inoutVtx[i]);
					f.inoutVtx[i] = NULL;
				}
			}
			return false;
		}
		/*NOTREACHED*/
		break;
	default:
		Quitf("Shouldn't get here");
	}

	return true;
}

void deMesh::CreateFromMaterial(MaterialData& material)
{
	MarkDrawDataDirty();
//	demAssertf(m_Vertices.size() == 0, "Material had data in it already");

	bool done = false;
	int start = 0;
	int currMaterialIdx = -1;
	do 
	{
		currMaterialIdx = -1;
		for(int j = start; j < m_Materials.GetCount(); j++) {
			if (material.m_Material && 
				m_Materials[j].m_Material &&
				material == m_Materials[j]) 
			{
				currMaterialIdx = j;
				break;
			}
		}

		//did not find anything so done
		if (currMaterialIdx == -1)
		{
			done = true;
		}
		else
		{
			//Count the number of verts that are part of this material 
			//		should be better approached later but for now do this way.
			//int vtxCount = 0;
			//for(deMesh::VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
			//	int count = (*vi)->GetNumWedges();
			//	for(int idx = 0; idx < count; ++idx) {
			//		if (currMaterialIdx == (*vi)->GetMaterial(idx))
			//		{
			//			vtxCount++;
			//		}
			//	}
			//}

			//are we over the vertex limit.
			//if (vtxCount + material.m_Material->GetVertexCount() > 65000)
			//{
			//	//not done yet
			//	start = currMaterialIdx + 1;
			//	demDisplayf("Over the vert limit");
			//}
			//else
			//{
				//we can add ourselves to this material so do it.
				done = true;
			//}
		}
	} while (!done);

	if (currMaterialIdx == -1) {
		MaterialData& mtl = m_Materials.Grow(4);
		mtl = material;
		currMaterialIdx = m_Materials.GetCount() - 1;
	}

	// Add all the faces in this material to the deMesh
	// Also could create a map from index to deVertex

	int a = -1, b = -1, c = -1;

	int lastIndices[3];
	deVertex* lastVerts[3];
	int lastWedgeIndices[3];

	lastIndices[0] = lastIndices[1] = lastIndices[2] = -1;
	lastVerts[0] = lastVerts[1] = lastVerts[2] = NULL;
	lastWedgeIndices[0] = lastWedgeIndices[1] = lastWedgeIndices[2] = -1;

	m_SkippedTriangles = 0;

	mshMaterial::TriangleIterator end = material.m_Material->EndTriangles();
	for(mshMaterial::TriangleIterator ti = material.m_Material->BeginTriangles(); ti != end; ++ti)
	{
		ti.GetVertIndices(a,b,c);

		FaceInfo f;

		f.inData[0].SetChannelData(NULL);
		f.inData[1].SetChannelData(NULL);
		f.inData[2].SetChannelData(NULL);

		f.inData[0].SetMaterial(currMaterialIdx);
		f.inData[1].SetMaterial(currMaterialIdx);
		f.inData[2].SetMaterial(currMaterialIdx);

		f.inData[0].SetVertexData(material.m_Material->GetVertex(a));
		f.inData[1].SetVertexData(material.m_Material->GetVertex(b));
		f.inData[2].SetVertexData(material.m_Material->GetVertex(c));

		f.inoutVtx[0] = NULL;
		f.inoutVtx[1] = NULL;
		f.inoutVtx[2] = NULL;

		// Are these verts in the cache
		for(int i = 0; i < 3; i++) {
			if (lastIndices[i] == a) {
				f.inoutVtx[0] = lastVerts[i];
				f.inoutWedgeIndices[0] = lastWedgeIndices[i];
			}
			if (lastIndices[i] == b) {
				f.inoutVtx[1] = lastVerts[i];
				f.inoutWedgeIndices[1] = lastWedgeIndices[i];
			}
			if (lastIndices[i] == c) {
				f.inoutVtx[2] = lastVerts[i];
				f.inoutWedgeIndices[2] = lastWedgeIndices[i];
			}
		}

		if (!AddFace(f)) 
		{
			// couldn't add face, flush the cache in case we deleted any added verts

			lastIndices[0] = lastIndices[1] = lastIndices[2] = -1;
			lastVerts[0] = lastVerts[1] = lastVerts[2] = NULL;
			lastWedgeIndices[0] = lastWedgeIndices[1] = lastWedgeIndices[2] = -1;
		}
		else
		{
			lastIndices[0] = a;
			lastIndices[1] = b;
			lastIndices[2] = c;

			lastVerts[0] = f.inoutVtx[0];
			lastVerts[1] = f.inoutVtx[1];
			lastVerts[2] = f.inoutVtx[2];

			lastWedgeIndices[0] = f.inoutWedgeIndices[0];
			lastWedgeIndices[1] = f.inoutWedgeIndices[1];
			lastWedgeIndices[2] = f.inoutWedgeIndices[2];
		}
	}

	if (m_SkippedTriangles > 0) {
		char buf[256];
		formatf(buf, "Skipped %d triangles because of winding order inconsistencies\n", m_SkippedTriangles);
		demWarningf(buf);
		deMeshError error(this, buf);
		ReportError(error);
	}

//	ReduceEdgeSets();
}

void deMesh::RemoveUnusedMaterials()
{
	int numMaterials = m_Materials.GetCount();
	atArray<int> materialRemap(numMaterials, numMaterials);
	atArray<int> materialCounts(numMaterials, numMaterials);
	for (int i = 0; i < numMaterials; i++)
	{
		materialRemap[i] = -1;
		materialCounts[i] = 0;
	}

	for (std::vector<deVertex*>::iterator i = m_Vertices.begin(), e = m_Vertices.end(); i != e; ++i)
	{
		deVertex* vert = *i;

		for (int j = 0, n = vert->GetNumWedges(); j < n; j++)
		{
			int material = vert->GetWedge(j).GetMaterial();
			materialRemap[material] = material;
			materialCounts[material]++;
		}
	}

	int newNumMaterials = 0;
	for (int i = 0; i < numMaterials; i++)
	{
		if (materialRemap[i] == -1)
			continue;

		m_Materials[newNumMaterials] = m_Materials[i];
		materialRemap[i] = newNumMaterials;
		newNumMaterials++;
	}
	m_Materials.Resize(newNumMaterials);

	for (std::vector<deVertex*>::iterator i = m_Vertices.begin(), e = m_Vertices.end(); i != e; ++i)
	{
		deVertex* vert = *i;

		for (int j = 0, n = vert->GetNumWedges(); j < n; j++)
		{
			deWedge& wedge = vert->GetWedge(j);
			int newMaterial = materialRemap[wedge.GetMaterial()];
			demAssertf(newMaterial != -1 && newMaterial < newNumMaterials,
				"Error in remapping table, found material with index %d, max is %d", newMaterial, newNumMaterials);
			wedge.SetMaterial(newMaterial);
		}
	}
}

//TODO: This should be more destructive, and actually delete oldVert.
// But we need to make sure oldVert isn't referenced anywhere after a CombineVerts call.

void deMesh::CombineVerts(deVertex* keptVert, deVertex* oldVert) {
	MarkDrawDataDirty();
	for(deVertex::EdgeIterator vertEdge = oldVert->BeginEdges(); vertEdge != oldVert->EndEdges(); ++vertEdge) {
		vertEdge->m_Wedge = keptVert->FindOrAddWedge(vertEdge->GetStartWedge()); // not the fastest algorithm, but it shoudl work.
		vertEdge->m_Prev->m_Vertex = keptVert;
	}
	keptVert->m_Flags.Set(deVertex::FLAG_PROCESSED);
}

void deMesh::Combine(deMesh& mesh)
{
	MarkDrawDataDirty();

	// Make sure current mesh has a vertex grid
	if (!m_VertexGrid) {
		demWarningf("Destination mesh should have a vertex grid, or it will be real slow\n");
		deMeshError error(this, "Destination mesh should have a vertex grid, or it will be real slow");
		ReportError(error);
	}

	deMesh* srcMesh = &mesh;
	deMesh* dstMesh = this;

	// Add all the materials in srcMesh into dstMesh (combining same materials) and fix up material pointers.
	atBinaryMap<int, int> newMaterialMap;
	atArray<int> dstMatVrtCounts;
	dstMatVrtCounts.Resize(dstMesh->m_Materials.GetCount());
	for(int dstMatIdx = 0; dstMatIdx < dstMesh->m_Materials.GetCount(); dstMatIdx++) {
		dstMatVrtCounts[dstMatIdx] = 0;
	}

	for(int srcMatIdx = 0; srcMatIdx < srcMesh->m_Materials.GetCount(); srcMatIdx++) {
		bool found = false;

		//Count the number of verts that are part of this material 
		//		should be better approached later but for now do this way.
		//int srcVtxCount = 0;
		//for(deMesh::VertexIterator vi = srcMesh->BeginVertices(); vi != srcMesh->EndVertices(); ++vi) {
		//	int count = (*vi)->GetNumWedges();
		//	for(int idx = 0; idx < count; ++idx) {
		//		if (srcMatIdx == (*vi)->GetMaterial(idx))
		//		{
		//			srcVtxCount++;
		//		}
		//	}
		//}

		for(int dstMatIdx = 0; !found && dstMatIdx < dstMesh->m_Materials.GetCount(); dstMatIdx++) {
			if (dstMesh->m_Materials[dstMatIdx].m_Material && 
				srcMesh->m_Materials[srcMatIdx].m_Material && 
				dstMesh->m_Materials[dstMatIdx] == srcMesh->m_Materials[srcMatIdx]) {
				//		if (dstMatVrtCounts[dstMatIdx] + srcVtxCount < 65000)
				//		{
				newMaterialMap.Insert(srcMatIdx, dstMatIdx);
				found = true;
				//			dstMatVrtCounts[dstMatIdx] += srcVtxCount;
				//		}
			}
		}
		if (!found) {
			dstMesh->m_Materials.Grow(4) = srcMesh->m_Materials[srcMatIdx];
			dstMatVrtCounts.Grow(4) = 0;
			newMaterialMap.Insert(srcMatIdx, dstMesh->m_Materials.GetCount()-1);
		}
	}

	newMaterialMap.FinishInsertion();

	// Clear the processed flag on the vert edges (really just need to clear boundary edges, but this should be fine).
	// Also remap materials to the dst mesh materials
	for(VertexIterator vi = srcMesh->BeginVertices(); vi != srcMesh->EndVertices(); ++vi) {
		(*vi)->m_Flags.Clear(deVertex::FLAG_PROCESSED);
		(*vi)->GetEdge()->m_Flags.Clear(deEdge::FLAG_PROCESSED);
		for(int w = 0; w < (*vi)->m_Wedges.GetCount(); ++w) {
			(*vi)->m_Wedges[w].SetMaterial(newMaterialMap[(*vi)->m_Wedges[w].GetMaterial()]);
		}
	}

	deEdge* lastDstEdges[3];
	deVertex* lastSrcVerts[3];
	int lastWedgeIndices[3];

	lastDstEdges[0] = lastDstEdges[1] = lastDstEdges[2] = NULL;
	lastSrcVerts[0] = lastSrcVerts[1] = lastSrcVerts[2] = NULL;
	lastWedgeIndices[0] = lastWedgeIndices[1] = lastWedgeIndices[2] = -1;

	for(FaceEdgeIterator face = srcMesh->BeginFaceEdges(); face != srcMesh->EndFaceEdges(); ++face)
	{
		deEdge* a;
		deEdge* b;
		deEdge* c;
		face->GetFaceEdges(a,b,c);

		FaceInfo f;

		f.inData[0] = a->GetEndWedge();
		f.inData[1] = b->GetEndWedge();
		f.inData[2] = c->GetEndWedge();

		f.inoutVtx[0] = NULL;
		f.inoutVtx[1] = NULL;
		f.inoutVtx[2] = NULL;

		for(int i = 0; i < 3; i++)
		{
			if (lastDstEdges[i] == a) {
				f.inoutVtx[0] = lastSrcVerts[i];
				f.inoutWedgeIndices[0] = lastWedgeIndices[i];
			}
			if (lastDstEdges[i] == b) {
				f.inoutVtx[1] = lastSrcVerts[i];
				f.inoutWedgeIndices[1] = lastWedgeIndices[i];
			}
			if (lastDstEdges[i] == c) {
				f.inoutVtx[2] = lastSrcVerts[i];
				f.inoutWedgeIndices[2] = lastWedgeIndices[2];
			}
		}

		dstMesh->AddFace(f);

		lastSrcVerts[0] = f.inoutVtx[0];
		lastSrcVerts[1] = f.inoutVtx[1];
		lastSrcVerts[2] = f.inoutVtx[2];

		lastWedgeIndices[0] = f.inoutWedgeIndices[0];
		lastWedgeIndices[1] = f.inoutWedgeIndices[1];
		lastWedgeIndices[2] = f.inoutWedgeIndices[2];
	}

	if (m_SkippedTriangles > 0) {
		char buf[256];
		formatf(buf, "Skipped %d triangles because of winding order inconsistencies\n", m_SkippedTriangles);
		demWarningf(buf);
		deMeshError error(this, buf);
		ReportError(error);
	}

	dstMesh->ReduceEdgeSets();
}

void deMesh::Stitch(deMesh& mesh)
{
	MarkDrawDataDirty();
#if 0
	static int meshNumber = 0;	
	char buf[64];
	formatf(buf, 64, "C:\\Meshes\\stitch_src_%d.dmsh", meshNumber);
//	this->Save(buf);
	formatf(buf, 64, "C:\\Meshes\\stitch_dst_%d.dmsh", meshNumber);
	mesh.Save(buf);
	meshNumber++;
#endif
	// mark all edges in mesh as unprocessed
	// for each edge in mesh
	//  if edge is an unprocessed boundary vertex: 
	//    look for a matching edge in this mesh
	//      if found, walk in both directions, replacing mesh->vert with this->vert and connecting edges
	//      mark as processed
	//      remove vert from mesh
	// add all verts in mesh to this

	// Make sure current mesh has a vertex grid
	if (!m_VertexGrid && 0) {
		demWarningf("Destination mesh should have a vertex grid, or it will be real slow\n");
		deMeshError error(this, "Destination mesh should have a vertex grid, or it will be real slow");
		ReportError(error);
	}

	// We really shouldn't need to use this, cause we shouldn't be creating bad topology in the first place
	bool doubleCheckTopology = false; 

	deMesh* srcMesh = &mesh;
	deMesh* dstMesh = this;

	VertexIterator vi = BeginVertices();

	// Add all the materials in srcMesh into dstMesh (combining same materials) and fix up material pointers.
	atBinaryMap<int, int> newMaterialMap;
	atArray<int> dstMatVrtCounts;
	dstMatVrtCounts.Resize(dstMesh->m_Materials.GetCount());
	for(int dstMatIdx = 0; dstMatIdx < dstMesh->m_Materials.GetCount(); dstMatIdx++) {
		dstMatVrtCounts[dstMatIdx] = 0;
	}

	for(int srcMatIdx = 0; srcMatIdx < srcMesh->m_Materials.GetCount(); srcMatIdx++) {
		bool found = false;

		//Count the number of verts that are part of this material 
		//		should be better approached later but for now do this way.
		//int srcVtxCount = 0;
		//for(deMesh::VertexIterator vi = srcMesh->BeginVertices(); vi != srcMesh->EndVertices(); ++vi) {
		//	int count = (*vi)->GetNumWedges();
		//	for(int idx = 0; idx < count; ++idx) {
		//		if (srcMatIdx == (*vi)->GetMaterial(idx))
		//		{
		//			srcVtxCount++;
		//		}
		//	}
		//}

		for(int dstMatIdx = 0; !found && dstMatIdx < dstMesh->m_Materials.GetCount(); dstMatIdx++) {
			if (dstMesh->m_Materials[dstMatIdx].m_Material && dstMesh->m_Materials[dstMatIdx] == srcMesh->m_Materials[srcMatIdx]) {
		//		if (dstMatVrtCounts[dstMatIdx] + srcVtxCount < 65000)
		//		{
					newMaterialMap.Insert(srcMatIdx, dstMatIdx);
					found = true;
		//			dstMatVrtCounts[dstMatIdx] += srcVtxCount;
		//		}
			}
		}
		if (!found) {
			dstMesh->m_Materials.Grow(4) = srcMesh->m_Materials[srcMatIdx];
			dstMatVrtCounts.Grow(4) = 0;
			newMaterialMap.Insert(srcMatIdx, dstMesh->m_Materials.GetCount()-1);
		}
	}

	newMaterialMap.FinishInsertion();

	// Clear the processed flag on the vert edges (really just need to clear boundary edges, but this should be fine).
	// Also remap materials to the dst mesh materials
	for(vi = srcMesh->BeginVertices(); vi != srcMesh->EndVertices(); ++vi) {
		(*vi)->m_Flags.Clear(deVertex::FLAG_PROCESSED);
		(*vi)->GetEdge()->m_Flags.Clear(deEdge::FLAG_PROCESSED);
		for(int w = 0; w < (*vi)->m_Wedges.GetCount(); ++w) {
			(*vi)->m_Wedges[w].SetMaterial(newMaterialMap[(*vi)->m_Wedges[w].GetMaterial()]);
		}
	}


	// See if we need to flip the winding order
//	for(ei = mesh.BeginEdges(); ei != mesh.EndEdges(); ++ei) {
	for(vi = srcMesh->BeginVertices(); vi != srcMesh->EndVertices(); ++vi) {
		deEdge* edge = (*vi)->GetEdge();
		if (edge->IsOnBoundary()) {
			// see if there's a matching edge in dstMesh
			deVertex* srcStart = edge->GetStart();
			deVertex* srcEnd = edge->GetEnd();

			int extraVerts;

			deVertex* dstEnd = dstMesh->FindClosestBoundaryVertex(srcStart->GetPosition(), extraVerts);
			if (!dstEnd) {
				continue;
			}
			if (extraVerts) {
				demWarningf("Multiple vertices very close to (%f, %f, %f).\n", srcStart->GetPosition().x, srcStart->GetPosition().y, srcStart->GetPosition().z);
				deMeshErrorPoint error(dstMesh, "Multiple vertices very close to source point", srcStart);
				ReportError(error);
			}
			deVertex* dstStart = dstMesh->FindClosestBoundaryVertex(srcEnd->GetPosition(), extraVerts);
			if (!dstStart) {
				continue;
			}
			if (extraVerts) {
				demWarningf("Multiple vertices very close to (%f, %f, %f).\n", srcEnd->GetPosition().x, srcEnd->GetPosition().y, srcEnd->GetPosition().z);
				deMeshErrorPoint error(dstMesh, "Multiple vertices very close to source point", srcEnd);
				ReportError(error);
			}

			deEdge* dstEdge = dstEnd->FindEdge(dstStart);
			if (dstEdge) {
				demAssertf(dstEdge->IsOnBoundary() , "Stitch would create nonmanifold geometry");
				if (dstEdge->IsOnBoundary())
				{
					deMeshErrorEdge error(dstMesh, "Stitch would create nonmanifold geometry", dstEdge);
					ReportError(error);
				}
				if (!PARAM_preservewinding.Get())
				{
					srcMesh->FlipWinding();
				}
				break;
			}

			dstEdge = dstStart->FindEdge(dstEnd);
			if (dstEdge) {
				demAssertf(dstEdge->IsOnBoundary() , "Stitch would create nonmanifold geometry");
				if (dstEdge->IsOnBoundary())
				{
					deMeshErrorEdge error(dstMesh, "Stitch would create nonmanifold geometry", dstEdge);
					ReportError(error);
				}
				// don't flip
				break;
			}

			// If we get here we found an odd but valid case where the two verts matched but no edges
			// (e.g. a hole has been split across two meshes)
			// So keep checking for other matches.
		}
	}

	//	for(ei = srcMesh->BeginEdges(); ei != srcMesh->EndEdges(); ++ei) {

	// Just need to check one outgoing edge on each vert, instead of all edges in the mesh, because that
	// is the only edge that could be on a boundary. Also lets us skip processed verts better.
	for(vi = srcMesh->BeginVertices(); vi != srcMesh->EndVertices(); ++vi) {
		deEdge* edge = (*vi)->GetEdge();
		if (edge->IsOnBoundary() && edge->m_Flags.IsClear(deEdge::FLAG_PROCESSED)) {
			// see if there's a matching edge in this mesh
			deEdge* srcEdge = edge;

			deVertex* srcStart = srcEdge->GetStart();
			deVertex* srcEnd = srcEdge->GetEnd();

			int extraVerts = 0;
			deVertex* dstEnd = dstMesh->FindClosestBoundaryVertex(srcStart->GetPosition(), extraVerts);
			if (!dstEnd) {
				continue;
			}
			if (extraVerts) {
				demWarningf("Multiple vertices very close to (%f, %f, %f).\n", srcStart->GetPosition().x, srcStart->GetPosition().y, srcStart->GetPosition().z);
				deMeshErrorPoint error(dstMesh, "Multiple vertices very close to source point", srcStart);
				ReportError(error);
			}
			deVertex* dstStart = dstMesh->FindClosestBoundaryVertex(srcEnd->GetPosition(), extraVerts);
			if (!dstStart) {
				continue;
			}
			if (extraVerts) {
				demWarningf("Multiple vertices very close to (%f, %f, %f).\n", srcEnd->GetPosition().x, srcEnd->GetPosition().y, srcEnd->GetPosition().z);
				deMeshErrorPoint error(dstMesh, "Multiple vertices very close to source point", srcEnd);
				ReportError(error);
			}


			if (dstEnd == NULL || dstStart == NULL) {
				// There are other cases, for example when srcStart matches dstEnd but the other two verts don't match.
				// We shouldn't need to deal with this because we'll catch those cases while stitching along the adjacent
				// edge that does have matches.
				continue;
			}

            deEdge* dstEdge = dstStart->FindEdge(dstEnd);
			deEdge* dstOppositeEdge = dstEnd->FindEdge(dstStart);

			// It's possible for dstEdge to be null here, if there was a "notch" in the dst side that's being turned into a hole
			// by closing off one side of the notch.
			if (dstEdge) {

				// If we get here, it's because we tried to stitch to an interior edge.
				// That shouldn't happen, because FindClosestBoundaryVertex shouldn't return
				// an interior vert (an no vert => no edge)
				//demAssertf(!dstOppositeEdge, "Can't stitch to an interior edge (edge between (%f, %f, %f) and (%f, %f, %f))", 
                //        srcStart->GetPosition().x, srcStart->GetPosition().y, srcStart->GetPosition().z,
                //        srcEnd->GetPosition().x, srcEnd->GetPosition().y, srcEnd->GetPosition().z);

				if (dstOppositeEdge)
				{
					demWarningf("Can't stitch to an interior edge (edge between (%f, %f, %f) and (%f, %f, %f))", 
						srcStart->GetPosition().x, srcStart->GetPosition().y, srcStart->GetPosition().z,
						srcEnd->GetPosition().x, srcEnd->GetPosition().y, srcEnd->GetPosition().z);
					deMeshErrorEdge error(dstMesh, "Can't stitch to an interior edge", srcStart->GetPosition(), srcEnd->GetPosition());
					ReportError(error);
					continue;
				}

				deEdge::SetNeighbors(dstEdge, srcEdge);

				if (srcEnd->m_Flags.IsClear(deVertex::FLAG_PROCESSED)) {
					// Normal case
					CombineVerts(dstStart, srcEnd);
					// Update dstStart's outgoing edge (dstEnd's outgoing edge should already be fine)
					dstStart->m_Edges[0] = srcEnd->GetEdge();

					DeleteVertex(srcEnd); // Doesn't really delete, so we can still test flags later
					// TODO: do this better so we don't deref srcEnd

				}
				else {
					// Unusual case (see comments below)
					CombineVerts(srcEnd, dstStart);

					DeleteVertex(dstStart); // Doesn't really delete, so we can still test flags later
					// TODO: do this better so we don't deref dstStart
				}


				if (srcStart->m_Flags.IsClear(deVertex::FLAG_PROCESSED)) {
					// Normal case
					CombineVerts(dstEnd, srcStart);

					DeleteVertex(srcStart); // Doesn't really delete, so we can still test flags later
					// TODO: do this better so we don't deref srcStart

				}
				else {
					// Unusual case (see comments below)

					CombineVerts(srcStart, dstEnd);

					srcStart->m_Edges[0] = dstEnd->GetEdge();


					DeleteVertex(dstEnd); // Doesn't really delete, so we can still test flags later
					// TODO: do this better so we don't deref dstEnd
				}

				// Make sure this got the assignment right
				demAssertf(srcEdge->GetEnd() == dstStart || (srcEnd->m_Flags.IsSet(deVertex::FLAG_PROCESSED) && srcEdge->GetEnd() == srcEnd), 
					"Incorrect edge assignment during stitch");
				demAssertf(srcEdge->GetStart() == dstEnd || (srcStart->m_Flags.IsSet(deVertex::FLAG_PROCESSED) && srcEdge->GetStart() == srcStart),
					"Incorrect edge assignment during stitch");

				// And mark the edge as processed so that if we hit it again we know we've gone in a loop around the boundary
				srcEdge->m_Flags.Set(deEdge::FLAG_PROCESSED);

				// Walk along the edge in both directions, stitching.

				deEdge* firstSrcEdge = srcEdge;
				deEdge* firstDstEdge = dstEdge;

				// while we haven't looped or hit another piece of the loop that's been checked...

				// (Can't use srcEdge->GetNextAlongBoundary because srcEdge has a neighbor now, so it isn't technically a boundary)
				while(srcEdge->UnsafeGetNextAlongBoundary()->m_Flags.IsClear(deEdge::FLAG_PROCESSED))
				{
					dstEdge = dstEdge->UnsafeGetPrevAlongBoundary();
					srcEdge = srcEdge->UnsafeGetNextAlongBoundary();

					srcStart = srcEdge->GetStart();
					srcEnd = srcEdge->GetEnd();
					dstStart = dstEdge->GetStart();
					dstEnd = dstEdge->GetEnd();

					demAssertf(srcStart == dstEnd, "Something went wrong on previous stitch, while stitching a loop");

					if(!srcEnd->IsClose(dstStart->GetPosition(), deWedge::GetVertSnapDist())) {
						// Hit the end of the chain
						break;
					}

					// Zip the next pair
					deEdge::SetNeighbors(dstEdge, srcEdge);
					srcEdge->m_Flags.Set(deEdge::FLAG_PROCESSED);



					if (srcEnd->m_Flags.IsClear(deVertex::FLAG_PROCESSED)) {
						CombineVerts(dstStart, srcEnd);

						// Update dstStart's outgoing edge (dstEnd's outgoing edge should already be fine)
						dstStart->m_Edges[0] = srcEnd->GetEdge();

						DeleteVertex(srcEnd);
					}
					else {
						// have we looped or not?
						if (dstStart == srcEnd) // we've already combined those verts
						{
							break;
						}
						else
						{
							// Unusual (but legal) case where there are two verts at the same place, and one has been
							// hooked up already.  Imagine these are boundary edges:
							//
							//              x______x____________x srcMesh
							//
							//              x______xx___________x dstMesh
							//                     /\                               //
							//                   x/__\x								
							//
							// dstMesh has two verts in the same spot, so that we don't have problems with the manifold shape.
							// Its possible that one of these verts has already been stitched.

							CombineVerts(srcEnd, dstStart);

							demWarningf("Early exit from stitching a loop. Dest mesh had 2 verts in same spot. Check mesh topology");
							deMeshErrorPoint error(dstMesh, "Early exit from stitching a loop. Dest mesh had 2 verts in same spot. Check mesh topology", dstStart);
							ReportError(error);

							DeleteVertex(dstStart);

							break;
						}

					}

				}

				srcEdge = firstSrcEdge;
				dstEdge = firstDstEdge;

				while(srcEdge->UnsafeGetPrevAlongBoundary()->m_Flags.IsClear(deEdge::FLAG_PROCESSED))
				{
					dstEdge = dstEdge->UnsafeGetNextAlongBoundary();
					srcEdge = srcEdge->UnsafeGetPrevAlongBoundary();

					srcStart = srcEdge->GetStart();
					srcEnd = srcEdge->GetEnd();
					dstStart = dstEdge->GetStart();
					dstEnd = dstEdge->GetEnd();

					demAssertf(srcEnd == dstStart, "Something went wrong with a previous stitch, while stitching a loop");

//					Printf("line(%f, %f, %f, %f)\n", srcEnd->GetPosition().x + 4000.0f, srcEnd->GetPosition().z - 3500,
//						srcStart->GetPosition().x + 4000.0f, srcStart->GetPosition().z - 3500);

					if (!srcStart->IsClose(dstEnd->GetPosition(), deWedge::GetVertSnapDist())) {
						// Hit the end of the chain
						break;
					}
					
					// Zip the next pair

					deEdge::SetNeighbors(dstEdge, srcEdge);
					srcEdge->m_Flags.Set(deEdge::FLAG_PROCESSED);		

					if (srcStart == dstEnd) {
						demWarningf("Weird case, src and dst edges have same endpoints already. Something's up with Get*AlongBoundary for partially stitched meshes?\n");
						deMeshErrorPoint error(dstMesh, "Weird case, src and dst edges have same endpoints already. Something's up with Get*AlongBoundary for partially stitched meshes?", dstEnd);
						ReportError(error);
						doubleCheckTopology = true;
						break;
					}

					if (srcStart->m_Flags.IsClear(deVertex::FLAG_PROCESSED)) {
						CombineVerts(dstEnd, srcStart);
						DeleteVertex(srcStart);
					}
					else {
						// Unusual (but legal) case where there are two verts at the same place, and one has been
						// hooked up already.  Imagine these are boundary edges:
						//
						//              x______x____________x srcMesh
						//
						//              x______xx___________x dstMesh
						//                     /\								//
						//                   x/__\x
						//
						// dstMesh has two verts in the same spot, so that we don't have problems with the manifold shape.
						// Its possible that one of these verts has already been stitched.

						// delete the dst vertex instead
						CombineVerts(srcStart, dstEnd);
						srcStart->m_Edges[0] = dstEnd->GetEdge();

						// In this case we can't iterate around srcStart to get the previous edge
						// along the source mesh, so skip out here and hopefully the boundary will be fixed up
						// later
						demWarningf("Early exit from stitching a loop. Dest mesh had 2 verts in same spot. Check mesh topology");
						deMeshErrorPoint error(dstMesh, "Early exit from stitching a loop. Dest mesh had 2 verts in same spot. Check mesh topology", dstEnd);
						ReportError(error);

						DeleteVertex(dstEnd);

						break;
					}


				}

			}
			else { // if (dstEdge)
				if (!dstOppositeEdge) {
					// A couple of things might have gone wrong here.
					// 1) incorrect winding order. It's possible that just one (disconnected) part of the
					//    mesh has a bad winding order, so it wouldn't have been caught above.
					// 2) we're trying to add a face that connects to a hole in the dst mesh
					// Either way, for now skip the edge
					demWarningf("Found a possible winding order or mesh topology problem at (%f, %f, %f) and (%f, %f, %f)", 
						dstStart->GetPosition().x,
						dstStart->GetPosition().y,
						dstStart->GetPosition().z,
						dstEnd->GetPosition().x,
						dstEnd->GetPosition().y,
						dstEnd->GetPosition().z);
					deMeshErrorEdge error(dstMesh, "Found a possible winding order or mesh topology problem", dstStart->GetPosition(), dstEnd->GetPosition());
					ReportError(error);

					continue;
				}
			}
		}
	}

	// Add all remaining verts to the dstMesh
	for(vi = srcMesh->BeginVertices(); vi != srcMesh->EndVertices(); ++vi) {
		dstMesh->AddVertexToGrid(*vi);
	}
	dstMesh->m_Vertices.insert(dstMesh->m_Vertices.end(), srcMesh->m_Vertices.begin(), srcMesh->m_Vertices.end());

	// Destroy the remnants of srcMesh
	srcMesh->DestroyGrid();
	srcMesh->m_Vertices.clear();

	if (doubleCheckTopology) {
		Printf("Double checking topology\n");
		for(FaceEdgeIterator fi = dstMesh->BeginFaceEdges(); fi != dstMesh->EndFaceEdges(); ++fi) {
			deEdge* e[3];
			fi->GetFaceEdges(e[0], e[1], e[2]);
			for(int i = 0; i < 3; i++) {
				if (e[i]->GetStart() == e[i]->GetEnd()) {
					demWarningf("Found invalid face (one edge had the same begin, end vertex)");
					demWarningf("     (%f, %f, %f)", e[i]->m_Vertex->GetPosition().x, e[i]->m_Vertex->GetPosition().y, e[i]->m_Vertex->GetPosition().z);
					demWarningf("     Trying to nuke it, this may create an invalid mesh...");

					deEdge* ss = e[i];
					deEdge* st = e[(i+1)%3];
					deEdge* ts = e[(i+2)%3];

					deVertex* vertS = ts->GetEnd();
					deVertex* vertT = st->GetEnd();

					deEdge::SetNeighbors(st->GetNeighbor(), ts->GetNeighbor());

					vertT->DisconnectEdge(ts);
					vertS->DisconnectEdge(st);
					vertS->DisconnectEdge(ss);

					delete ss;
					delete st;
					delete ts;

					fi = dstMesh->BeginFaceEdges();
					break;
				}
			}
		}
	}
}

void deMesh::InitGrid(float minX, float maxX, float minY, float maxY, int xCells, int yCells)
{
	demAssertf(!m_VertexGrid, "This mesh already has a grid");

	if (minX >= maxX || minY >= maxY)
	{
		return;
	}

	m_VertexGrid = rage_new spdGrid2DContainer< atArray< deVertex* > >;

	float cellSizeX, cellSizeY; 

	// add an extra cell to either end of the grid (to handle the situation where we're putting
	// something exactly on the grid boundary)
	cellSizeX = (maxX - minX) / (float)xCells;
	cellSizeY = (maxY - minY) / (float)yCells;

	m_VertexGrid->Init(minX - cellSizeX, maxX + cellSizeX, cellSizeX, minY - cellSizeY, maxY + cellSizeY, cellSizeY);
//	m_VertexGrid->Init(minX, maxX, cellSizeX, minY, maxY, cellSizeY);

	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi)
	{
		AddVertexToGrid(*vi);
	}
}

void deMesh::ExpandGrid(float minX, float maxX, float minY, float maxY, int xCells, int yCells)
{
	float oldMinX, oldMinY, oldMaxX, oldMaxY;
	FindApproxBounds(oldMinX, oldMaxX, oldMinY, oldMaxY);

	float newMinX, newMinY, newMaxX, newMaxY;
	newMinX = Min(oldMinX, minX);
	newMaxX = Max(oldMaxX, maxX);
	newMinY = Min(oldMinY, minY);
	newMaxY = Max(oldMaxY, maxY);

	int oldCellsX = -1;
	int oldCellsY = -1;
	if (m_VertexGrid)
	{
		oldCellsX = (m_VertexGrid->GetMaxCellX() - m_VertexGrid->GetMinCellX()) + 1;
		oldCellsY = (m_VertexGrid->GetMaxCellY() - m_VertexGrid->GetMinCellY()) + 1;
	}

	if (newMinX != oldMinX || newMinY != oldMinY ||
		newMaxX != oldMaxX || newMaxY != oldMaxY ||
		oldCellsX < xCells ||
		oldCellsY < yCells)
	{
		if (m_VertexGrid)
		{
			DestroyGrid();
		}
		InitGrid(newMinX, newMaxX, newMinY, newMaxY, Max(oldCellsX, xCells), Max(oldCellsY, yCells));
	}
}

void deMesh::AddVertexToGrid(deVertex* v)
{
	if (!m_VertexGrid) {
		return;
	}

	Vector3 pos = v->GetPosition();

	atArray<deVertex*>* verts = m_VertexGrid->GetData(Vector2(pos.x, pos.z));
	demAssertf(verts , "This vertex doesn't fit in the vertex grid");
	if (!verts)
	{
		return;
	}
	// Could skip this loop if we're sure we aren't adding something twice.
	for(int i = 0; i < verts->GetCount(); i++) {
		if ((*verts)[i] == v) {
			return; // already in the list
		}
	}
	verts->Grow() = v;
}

void deMesh::RemoveVertexFromGrid(deVertex* v)
{
	if (!m_VertexGrid) {
		return;
	}

	Vector3 pos = v->GetPosition();

	atArray<deVertex*>* verts = m_VertexGrid->GetData(Vector2(pos.x, pos.z));
	demAssertf(verts, "Couldn't find grid data for position %f, %f", pos.x, pos.y);
	for(int i = 0; i < verts->GetCount(); i++) {
		if ((*verts)[i] == v) {
			verts->DeleteFast(i);
		}
	}
}

void deMesh::DestroyGrid()
{
	if (!m_VertexGrid) {
		return;
	}

	for(int i = 0; i < m_VertexGrid->GetNumCells(); i++) {
		m_VertexGrid->GetData(i).Reset();
	}
	delete m_VertexGrid;
	m_VertexGrid = NULL;
}

deMesh* deMesh::CreateFromEntity(const char* filename) {
	deMesh* finalMesh = rage_new deMesh(filename); // add materials to this mesh.

	// Scan the .type file for:
	//   1) a list of materials (shader preset, shader variable) pairs
	//   2) a list of meshes

	// Create the material list
	// When converting from meshes, use the "#X" material name as an index into the master mtl list

	char dirname[256];
	ASSET.RemoveNameFromPath(dirname, 256, filename);

	fiSafeStream typeStream(ASSET.Open(filename, "type"));
	if (typeStream) {
		fiTokenizer tok("entity.type",typeStream);

		bool found = false;

		ASSERT_ONLY(int version =) tok.MatchInt("Version:");
		demAssertf(version == 103 , "Currently can only read .type version 103");

		// Read blocks in until we hit "shadinggroup"
		while(!found) {
			if (tok.CheckIToken("shadinggroup")) {
				found = true;
				break;
			}
			else {
				tok.IgnoreToken();
			}
			tok.GetDelimiter("{");
			tok.Pop();
		}
		demAssertf(found , "Couldn't find shadinggroup block");

		tok.GetDelimiter("{");

		tok.MatchIToken("shadinggroup");
		tok.GetDelimiter("{");
		if (tok.CheckToken("Count", false))
		{
			ASSERT_ONLY(int count =) tok.MatchInt("Count");
			demAssertf(count == 1 , "Count > 1 isn't handled yet");
		}
		int numShaders = tok.MatchInt("Shaders");
		finalMesh->m_Materials.Resize(numShaders);

		tok.GetDelimiter("{");
		int shadervers = tok.MatchInt("Vers:");

		if (shadervers == 1)
		{
			char spsName[256];
			char svaName[256];

			for(int i = 0; i < numShaders; i++) {
				tok.GetToken(spsName, 256);
				demVerifyf(tok.GetInt() == 1, "Expected '1' following sps name");
				tok.GetToken(svaName, 256);

				finalMesh->m_Materials[i].m_ShaderPreset = spsName;

				char svaFileName[256];
				formatf(svaFileName, 256, "%s/%s", dirname, svaName);

				fiSafeStream svaStream(ASSET.Open(svaFileName, "sva"));
				if (svaStream) {
					char* svaData = rage_new char[2048];
					int bytes = svaStream->Read(svaData, 2048);
					svaData[bytes] = '\0';
					finalMesh->m_Materials[i].m_ShaderVariables = svaData;
					delete svaData;
				}
			}

		}
		else if (shadervers == 2)
		{
			char mtlName[256];
			char mtlData[4096];
			for(int i = 0; i < numShaders; i++) {
				tok.GetLine(mtlName, 256);
				finalMesh->m_Materials[i].m_MtlFileName = mtlName;

				tok.GetLine(mtlName, 256);
				finalMesh->m_Materials[i].m_MtlTemplate = mtlName;

				// read from the next line down to the matching '}'
				int numVariables;
				tok.GetToken(mtlName, 256);
				numVariables = tok.GetInt();

				if (!strcmp(mtlName, "Bucket"))
				{
					tok.GetToken(mtlName, 256);
					numVariables = tok.GetInt();
				}

				formatf(mtlData, 4096, "%s %d\n", mtlName, numVariables);
				int bufSize = (int) strlen(mtlData);
				// initial {
				demAssertf(4096-bufSize,"Type file's shader group section is too big not enough space in my buffer [%s]", filename);
				bufSize += tok.GetLine(mtlData+bufSize, 4096-bufSize, false);
				mtlData[bufSize] = '\n'; bufSize++;
				for(int var = 0; var < numVariables; var++) {
					// 4 lines per variable
					demAssertf(4096-bufSize,"Type file's shader group section is too big not enough space in my buffer [%s]", filename);
					bufSize += tok.GetLine(mtlData+bufSize, 4096-bufSize, false);
					mtlData[bufSize] = '\n'; bufSize++;
					demAssertf(4096-bufSize,"Type file's shader group section is too big not enough space in my buffer [%s]", filename);
					bufSize += tok.GetLine(mtlData+bufSize, 4096-bufSize, false);
					mtlData[bufSize] = '\n'; bufSize++;
					demAssertf(4096-bufSize,"Type file's shader group section is too big not enough space in my buffer [%s]", filename);
					bufSize += tok.GetLine(mtlData+bufSize, 4096-bufSize, false);
					mtlData[bufSize] = '\n'; bufSize++;
					demAssertf(4096-bufSize,"Type file's shader group section is too big not enough space in my buffer [%s]", filename);
					bufSize += tok.GetLine(mtlData+bufSize, 4096-bufSize, false);
					mtlData[bufSize] = '\n'; bufSize++;
				}
				// final }
				demAssertf(4096-bufSize,"Type file's shader group section is too big not enough space in my buffer [%s]", filename);
				bufSize += tok.GetLine(mtlData+bufSize, 4096-bufSize, false);
				mtlData[bufSize] = '\n'; bufSize++;
				mtlData[bufSize] = '\0';

				finalMesh->m_Materials[i].m_MtlVariables = mtlData;
			}
		}

		tok.Pop();
		tok.Pop();
		tok.Pop();

		// Now scan through until we hit lodgroup
		// Read blocks in until we hit "shadinggroup"
		found = false;
		while(!found) {
			if (tok.CheckToken("lodgroup")) {
				found = true;
				break;
			}
			else
			{
				tok.IgnoreToken();
			}
			tok.GetDelimiter("{");
			tok.Pop();
		}
		demAssertf(found , "Couldn't find lodgroup block");

		tok.GetDelimiter("{");

		tok.MatchToken("mesh");
		tok.GetDelimiter("{");
		int highLodMeshes = tok.MatchInt("high");
		demAssertf(highLodMeshes > 0 , "Can't convert an entity with no high-LOD meshes");

		for(int meshIdx = 0; meshIdx < highLodMeshes; meshIdx++) {
			char meshName[256];
			char meshFileName[256];
			tok.GetToken(meshName, 256);
			tok.GetInt();

			formatf(meshFileName, 256, "%s/%s", dirname, meshName);

			mshMesh* srcMesh = rage_new mshMesh;
			if ( SerializeFromFile(meshFileName, *srcMesh, "mesh") == false ) 
			{
				demErrorf("Can't load mesh file %s", filename);
			}

			Vector3 meshMin, meshMax;
			srcMesh->ComputeBoundingBox(meshMin, meshMax);

			finalMesh->ExpandGrid(meshMin.x, meshMax.x, meshMin.z, meshMax.z, 100, 100);

//			deMesh* singleMesh = rage_new deMesh(meshFileName);
//			singleMesh->InitGrid(meshMin.x, meshMax.x, meshMin.z, meshMax.z, 100, 100);


			for(int meshMtlIdx = 0; meshMtlIdx < srcMesh->GetMtlCount(); meshMtlIdx++) {
				// get material index
				int mtlIndex = atoi(srcMesh->GetMtl(meshMtlIdx).Name + 1);

				demAssertf(mtlIndex < finalMesh->m_Materials.GetCount(), "Mesh is looking for material number %d, which isn't in the .type file", mtlIndex);

				// If the final mesh's material hasn't been set yet, do that now
				if (finalMesh->m_Materials[mtlIndex].m_Material == NULL) {
					finalMesh->m_Materials[mtlIndex].m_Material = rage_new mshMaterial;
					srcMesh->GetMtl(meshMtlIdx).CopyNongeometricData(*finalMesh->m_Materials[mtlIndex].m_Material);
				}
				else { 
					// It's possible that the materials aren't the same (one could have an extra texture channel for example)
					// in this case, create a copy of the material with the same shader data as the existing one, but with
					// texture count and tanbi count of the new material.
					if (!finalMesh->m_Materials[mtlIndex].m_Material->SameNongeometricData(srcMesh->GetMtl(meshMtlIdx))) {
						demWarningf("Found two materials with the same name, but different number of texture or tanbi channels");
						MaterialData newMaterial;
						newMaterial = finalMesh->m_Materials[mtlIndex]; // Copy shader data
						srcMesh->GetMtl(meshMtlIdx).CopyNongeometricData(*newMaterial.m_Material); // copy mesh mtl data

						bool done = false;
						int start = 0;
						do 
						{
							mtlIndex = -1;
							for(int i = start; i < finalMesh->m_Materials.GetCount(); i++) {
								if (finalMesh->m_Materials[i] == newMaterial) {
									mtlIndex = i;
									break;
								}
							}

							//did not find anything so done
							if (mtlIndex == -1)
							{
								done = true;
							}
							else
							{
								//Count the number of verts that are part of this material 
								//		should be better approached later but for now do this way.
								//int vtxCount = 0;
								//for(deMesh::VertexIterator vi = finalMesh->BeginVertices(); vi != finalMesh->EndVertices(); ++vi) {
								//	int count = (*vi)->GetNumWedges();
								//	for(int idx = 0; idx < count; ++idx) {
								//		if (mtlIndex == (*vi)->GetMaterial(idx))
								//		{
								//			vtxCount++;
								//		}
								//	}
								//}

								//are we over the vertex limit.
								//if (vtxCount + srcMesh->GetMtl(meshMtlIdx).GetVertexCount() > 65000)
								//{
								//	//not done yet
								//	start = mtlIndex;
								//	demDisplayf("Over the vert limit");
								//}
								//else
								//{
									//we can add ourselves to this material so do it.
									done = true;
								//}
							}
						} while (!done);

						if (mtlIndex == -1) {
							finalMesh->m_Materials.Grow(4) = newMaterial;
							mtlIndex = finalMesh->m_Materials.GetCount();
						}
					}
				}

				MaterialData mtl = finalMesh->m_Materials[mtlIndex]; // get shader data from final mesh
				mtl.m_Material = &srcMesh->GetMtl(meshMtlIdx); // change to the mshMaterial that has all the vertices

				finalMesh->CreateFromMaterial(mtl);
			}

			finalMesh->ReduceEdgeSets();
/*
			// Make sure we copy the smaller (in vtx count) mesh into the larger
			// (faster that way)
			if (singleMesh->m_Vertices.size() < finalMesh->m_Vertices.size())
			{
				finalMesh->ExpandGrid(meshMin.x, meshMax.x, meshMin.z, meshMax.z, 100, 100);
				finalMesh->Combine(*singleMesh);
				singleMesh->DestroyGrid();
				delete singleMesh;
			}
			else
			{
				float minX, maxX, minY, maxY;
				finalMesh->FindApproxBounds(minX, maxX, minY, maxY);
				singleMesh->ExpandGrid(minX, maxX, minY, maxY, 100, 100);
				singleMesh->Combine(*finalMesh);
				finalMesh->DestroyGrid();
				delete finalMesh;
				finalMesh = singleMesh;
			}
*/
			delete srcMesh;
		}

		tok.Pop();
		tok.Pop();
	}

	// Set the indices for all edges, verts.

	int numEdges = 0;
	EdgeIterator ei;
	for(ei = finalMesh->BeginEdges(); ei != finalMesh->EndEdges(); ++ei) {
		ei->m_Index = numEdges++;
	}

	int numVerts = 0;
	for(VertexIterator vi = finalMesh->BeginVertices(); vi != finalMesh->EndVertices(); ++vi) {
		(*vi)->m_Index = numVerts++;
	}

	return finalMesh;
}

deMesh* deMesh::CreateFromMesh(mshMesh* mesh) {
	deMesh* newMesh = rage_new deMesh;

	// Init the vertex grid
	Vector3 meshMin, meshMax;
	mesh->ComputeBoundingBox(meshMin, meshMax);

	// should merge current bbox with meshes bbox, reinit the grid
/*
	float lenX = meshMax.x - meshMin.x;
	float lenZ = meshMax.z - meshMin.z;

	float paddingX = lenX * 0.01f;
	float paddingZ = lenZ * 0.01f;
*/
	newMesh->InitGrid(meshMin.x-deWedge::GetVertSnapDist(), meshMax.x+deWedge::GetVertSnapDist(), meshMin.z-deWedge::GetVertSnapDist(), meshMax.z+deWedge::GetVertSnapDist(), 100, 100);

	// check to see if materials already exist

//	Printf("Building topology...\n");

	MaterialData mtl;
	mtl.m_Material = &mesh->GetMtl(0);
	newMesh->CreateFromMaterial(mtl);

	for(int i = 1; i < mesh->GetMtlCount(); i++) {
		mtl.m_Material = &mesh->GetMtl(i);

		newMesh->CreateFromMaterial(mtl);
	}

	newMesh->ReduceEdgeSets();
	newMesh->DestroyGrid();

	// Set the indices for all edges, verts.

	int numEdges = 0;
	EdgeIterator ei;
	for(ei = newMesh->BeginEdges(); ei != newMesh->EndEdges(); ++ei) {
		ei->m_Index = numEdges++;
	}

	int numVerts = 0;
	for(VertexIterator vi = newMesh->BeginVertices(); vi != newMesh->EndVertices(); ++vi) {
		(*vi)->m_Index = numVerts++;
	}

	return newMesh;
}

void deMesh::ReduceEdgeSets()
{
	MarkDrawDataDirty();
	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
		deVertex* baseVert = (*vi);
		baseVert->ReduceEdgeSet();
		if (baseVert->m_Edges.GetCount() > 1) {
			// need to split this vert into multiple verts, with each vert containing 1 basis edge
			for(int extraEdge = baseVert->m_Edges.GetCount()-1; extraEdge >= 1; extraEdge--) {
				deVertex* newVert;
				newVert = rage_new deVertex(baseVert->GetWedge(0));
				newVert->m_Edges.Grow(4) = baseVert->m_Edges[extraEdge];

				deEdge* fixEdge = newVert->GetEdge();
				do {
					fixEdge->m_Wedge = newVert->FindOrAddWedge(fixEdge->GetStartWedge());
					fixEdge->GetPrev()->m_Vertex = newVert;
					fixEdge = fixEdge->GetNextCCW();
				} while(fixEdge && fixEdge != newVert->GetEdge());
				newVert->ReduceWedgeSet();

				AddVertexToGrid(newVert);

				m_Vertices.push_back(newVert);

				// have to restart iteration here because the array may have been reallocated
				// TODO: see if we need to make this faster. hopefully this is a rare case so 
				// worst case O(N^2) never happens
				vi = BeginVertices();

				baseVert->m_Edges.Pop();
			}
		}
	}
}

void deMesh::DrawFast()
{
	GatherDrawData();
	if (m_DrawData.m_TotalFaces == 0)
	{
		return;
	}
	//	grcState::Default();
	//	grcLightState::SetEnabled(false);
	//	grcState::Default();

	//	grcColor3f(1.0f, 1.0f, 1.0f);

	const grcDrawMode drawMode = drawLines;
	const int vertsPerLoop = 6;// 3 edges per face, 2 verts per edge
	int numLoops = m_DrawData.m_TotalFaces;

	int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
	int vertsRemaining = numLoops * vertsPerLoop; 
	int vertsRemainingInBatch = Min(batchSize, vertsRemaining);

	grcBegin(drawMode, vertsRemainingInBatch);

	FaceEdgeIterator lastFace = EndFaceEdges();
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != lastFace; ++fi)
	{
		if (Unlikely(vertsRemainingInBatch == 0))
		{
			vertsRemaining -= batchSize;
			vertsRemainingInBatch = Min(batchSize, vertsRemaining);
			if (vertsRemainingInBatch > 0)
			{
				grcEnd();
				grcBegin(drawMode, vertsRemainingInBatch);
			}
		}
		vertsRemainingInBatch -= vertsPerLoop;

		Vector3 pos0, pos1, pos2;
		fi->GetFaceVertPositions(pos0, pos1, pos2);

		grcVertex3f(pos0);
		grcVertex3f(pos1);
		grcVertex3f(pos1);
		grcVertex3f(pos2);
		grcVertex3f(pos2);
		grcVertex3f(pos0);
	}

	grcEnd();
}

void deMesh::DrawStars()
{
	GatherDrawData();

	if (m_DrawData.m_TotalEdges == 0)
	{
		return;
	}

	const grcDrawMode drawMode = drawLines;
	const int vertsPerLoop = 2; // 2 verts per edge
	int numLoops = m_DrawData.m_TotalEdges;

	int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
	int vertsRemaining = numLoops * vertsPerLoop; 
	int vertsRemainingInBatch = Min(batchSize, vertsRemaining);

	grcBegin(drawMode, vertsRemainingInBatch);

	VertexIterator vi = BeginVertices();
	for(; vi != EndVertices(); ++vi) {
		for(deVertex::EdgeIterator ei = (*vi)->BeginEdges(); ei != (*vi)->EndEdges(); ++ei) {
			if (Unlikely(vertsRemainingInBatch == 0))
			{
				vertsRemaining -= batchSize;
				vertsRemainingInBatch = Min(batchSize, vertsRemaining);
				if (vertsRemainingInBatch > 0)
				{
					grcEnd();
					grcBegin(drawMode, vertsRemainingInBatch);
				}
			}
			vertsRemainingInBatch -= vertsPerLoop;

			Vector3 begin = ei->GetStart()->GetPosition();
			Vector3 end = ei->GetEnd()->GetPosition();

			grcVertex3f(begin);
			end.Average(begin, end);
			grcVertex3f(end);
		}
	}
	grcEnd();
}

void deMesh::DrawError()
{
	GatherDrawData();
	grcLightState::SetEnabled(false);

	if (m_DrawData.m_TotalEdges == 0)
	{
		return;
	}

	EdgeIterator ei = BeginEdges();
	float minError = ei->m_Error;
	float maxError = ei->m_Error;

	for(; ei != EndEdges(); ++ei) {
		if (ei->m_Flags.IsClear(deEdge::FLAG_PRIMARY_EDGE)) {
			continue;
		}
		minError = Min(minError, ei->m_Error);
		if (ei->m_Error < 1.0e10f) {
			maxError = Max(maxError, ei->m_Error);
		}
	}

	minError -= 0.01f;
	maxError += 0.01f;

	const grcDrawMode drawMode = drawLines;
	const int vertsPerLoop = 2; // 2 verts per edge
	int numLoops = (m_DrawData.m_TotalEdges / 2); // only drawing half the half-edges

	int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
	int vertsRemaining = numLoops * vertsPerLoop; 
	int vertsRemainingInBatch = Min(batchSize, vertsRemaining);

	grcBegin(drawMode, vertsRemainingInBatch);

	for(ei = BeginEdges(); ei != EndEdges(); ++ei)
	{
		if (ei->m_Flags.IsClear(deEdge::FLAG_PRIMARY_EDGE)) {
			continue;
		}

		if (Unlikely(vertsRemainingInBatch == 0))
		{
			vertsRemaining -= batchSize;
			vertsRemainingInBatch = Min(batchSize, vertsRemaining);
			if (vertsRemainingInBatch > 0)
			{
				grcEnd();
				grcBegin(drawMode, vertsRemainingInBatch);
			}
		}
		vertsRemainingInBatch -= vertsPerLoop;

		double err = ei->m_Error;

		if (err > 1.0e10f) {
			grcColor3f(1.0f, 0.0f, 0.0f);
		}
		else {
			float errScale = Range((float)err, (float)minError, (float)maxError);
			grcColor3f(errScale, errScale, errScale);
		}

		grcVertex3f(ei->GetStart()->GetPosition());
		grcVertex3f(ei->GetEnd()->GetPosition());
	}

	grcEnd();
}

void deMesh::DrawSolid()
{
	GatherDrawData();
	if (m_DrawData.m_TotalFaces == 0)
	{
		return;
	}

//	grcLightState::SetEnabled(true);
	grcLightState::SetEnabled(false);

	grcColor3f(0.7f, 0.7f, 0.7f);

	const grcDrawMode drawMode = drawTris;
	const int vertsPerLoop = 3; // 3 verts per face
	int numLoops = m_DrawData.m_TotalFaces;

	int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
	int vertsRemaining = numLoops * vertsPerLoop; 
	int vertsRemainingInBatch = Min(batchSize, vertsRemaining);

	grcBegin(drawMode, vertsRemainingInBatch);

	FaceEdgeIterator lastFace = EndFaceEdges();
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != lastFace; ++fi)
	{
		if (Unlikely(vertsRemainingInBatch == 0))
		{
			vertsRemaining -= batchSize;
			vertsRemainingInBatch = Min(batchSize, vertsRemaining);
			if (vertsRemainingInBatch > 0)
			{
				grcEnd();
				grcBegin(drawMode, vertsRemainingInBatch);
			}
		}
		vertsRemainingInBatch -= vertsPerLoop;

		Vector3 normal = fi->GetFaceNormal();
		Vector3 v0, v1, v2;
		fi->GetFaceVertPositions(v0, v1, v2);

		Vector3 color = (normal + VEC3_IDENTITY);
		color.Scale(0.5f);

		grcColor3f(color);
		grcNormal3f(normal);
		grcVertex3f(v0);
		grcNormal3f(normal);
		grcVertex3f(v1);
		grcNormal3f(normal);
		grcVertex3f(v2);
	}
	grcEnd();
}

void deMesh::DrawNormals()
{
	GatherDrawData();
	if (m_DrawData.m_TotalFaces == 0)
	{
		return;
	}

	grcLightState::SetEnabled(false);

	const grcDrawMode drawMode = drawTris;
	const int vertsPerLoop = 3; // 3 verts per face
	int numLoops = m_DrawData.m_TotalFaces;

	int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
	int vertsRemaining = numLoops * vertsPerLoop; 
	int vertsRemainingInBatch = Min(batchSize, vertsRemaining);


	grcBegin(drawMode, vertsRemainingInBatch);

	FaceEdgeIterator lastFace = EndFaceEdges();
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != lastFace; ++fi)
	{
		if (Unlikely(vertsRemainingInBatch == 0))
		{
			vertsRemaining -= batchSize;
			vertsRemainingInBatch = Min(batchSize, vertsRemaining);
			if (vertsRemainingInBatch > 0)
			{
				grcEnd();
				grcBegin(drawMode, vertsRemainingInBatch);
			}
		}
		vertsRemainingInBatch -= vertsPerLoop;

		deEdge* edges[3];
		fi->GetFaceEdges(edges[0], edges[1], edges[2]);
		for(int i = 0; i < 3; i++) {
			Vector3 normal = edges[i]->GetStartWedge().GetVertexData().Nrm;
			normal.Scale(0.5f);
			normal.Add(0.5f, 0.5f, 0.5f);
			grcColor3f(normal);
			grcVertex3f(edges[i]->GetStart()->GetPosition());
		}
	}
	grcEnd();
}

void deMesh::DrawTexCoords()
{
	GatherDrawData();
	if (m_DrawData.m_TotalFaces == 0)
	{
		return;
	}

	grcLightState::SetEnabled(false);
	// grcState::SetSmoothShade(true);

	const grcDrawMode drawMode = drawTris;
	const int vertsPerLoop = 3; // 3 verts per face
	int numLoops = m_DrawData.m_TotalFaces;

	int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
	int vertsRemaining = numLoops * vertsPerLoop; 
	int vertsRemainingInBatch = Min(batchSize, vertsRemaining);


	grcBegin(drawMode, vertsRemainingInBatch);

	FaceEdgeIterator lastFace = EndFaceEdges();
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != lastFace; ++fi)
	{
		if (Unlikely(vertsRemainingInBatch == 0))
		{
			vertsRemaining -= batchSize;
			vertsRemainingInBatch = Min(batchSize, vertsRemaining);
			if (vertsRemainingInBatch > 0)
			{
				grcEnd();
				grcBegin(drawMode, vertsRemainingInBatch);
			}
		}
		vertsRemainingInBatch -= vertsPerLoop;

		deEdge* edges[3];
		fi->GetFaceEdges(edges[0], edges[1], edges[2]);
		for(int i = 0; i < 3; i++) {
			Vector3 texCoord;
			texCoord.x = edges[i]->GetStartWedge().GetVertexData().Tex[0].x;
			texCoord.y = edges[i]->GetStartWedge().GetVertexData().Tex[0].y;
//			texCoord.z = edges[i]->GetStartWedge().GetVertexData().TexCount == 1 ? 0.0f : 1.0f;
			texCoord.z = 0.0f;
			grcColor3f(texCoord);
			//			switch(edges[i]->m_Wedge) {
			//				case 0:	grcColor3f(1.0f, 0.0f, 0.0f); break;
			//				case 1:	grcColor3f(0.0f, 1.0f, 0.0f); break;
			//				case 2:	grcColor3f(0.0f, 0.0f, 1.0f); break;
			//				case 3:	grcColor3f(1.0f, 1.0f, 0.0f); break;
			//				case 4:	grcColor3f(1.0f, 0.0f, 1.0f); break;
			//				case 5:	grcColor3f(0.0f, 1.0f, 1.0f); break;
			//			}
			grcVertex3f(edges[i]->GetStart()->GetPosition());
		}
	}
	grcEnd();
}

void deMesh::DrawSmooth()
{
	GatherDrawData();
	if (m_DrawData.m_TotalFaces == 0)
	{
		return;
	}
	grcLightState::SetEnabled(true);
	// grcState::SetSmoothShade(true);


	const grcDrawMode drawMode = drawTris;
	const int vertsPerLoop = 3; // 3 verts per face
	int numLoops = m_DrawData.m_TotalFaces;

	int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
	int vertsRemaining = numLoops * vertsPerLoop; 
	int vertsRemainingInBatch = Min(batchSize, vertsRemaining);


	grcBegin(drawMode, vertsRemainingInBatch);

	FaceEdgeIterator lastFace = EndFaceEdges();
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != lastFace; ++fi)
	{
		if (Unlikely(vertsRemainingInBatch == 0))
		{
			vertsRemaining -= batchSize;
			vertsRemainingInBatch = Min(batchSize, vertsRemaining);
			if (vertsRemainingInBatch > 0)
			{
				grcEnd();
				grcBegin(drawMode, vertsRemainingInBatch);
			}
		}
		vertsRemainingInBatch -= vertsPerLoop;
		//		deVertex* verts[3];
		//		fi->GetFaceVerts(verts[0], verts[1], verts[2]);

		deEdge* edges[3];
		fi->GetFaceEdges(edges[0], edges[1], edges[2]);

		for(int i = 0; i < 3; i++) {
			grcColor4f(edges[i]->GetStartWedge().GetVertexData().Cpv);
			grcNormal3f(edges[i]->GetStartWedge().GetVertexData().Nrm);
			grcTexCoord2f(edges[i]->GetStartWedge().GetVertexData().Tex[0].x, edges[i]->GetStartWedge().GetVertexData().Tex[0].y);
			grcVertex3f(edges[i]->GetStart()->GetPosition());
		}
	}
	grcEnd();
}

void deMesh::DrawSharps(bool drawBoundaries, bool drawSharps)
{
	GatherDrawData();
	int totalSharps = m_DrawData.m_BoundaryEdges + m_DrawData.m_StartSharpEdges;
	if (totalSharps == 0)
	{
		return;
	}

	grcLightState::SetEnabled(false);


	const grcDrawMode drawMode = drawLines;
	const int vertsPerLoop = 2; 
	int numLoops = 0;
	if (drawBoundaries)
	{
		numLoops += m_DrawData.m_BoundaryEdges;
	}
	if (drawSharps)
	{
		numLoops += m_DrawData.m_StartSharpEdges;
	}

	int batchSize = (grcBeginMax / vertsPerLoop) * vertsPerLoop;
	int vertsRemaining = numLoops * vertsPerLoop; 
	int vertsRemainingInBatch = Min(batchSize, vertsRemaining);

	grcBegin(drawMode, vertsRemainingInBatch);

	for(EdgeIterator ei = BeginEdges(); ei != EndEdges(); ++ei) {

		if (Unlikely(vertsRemainingInBatch == 0))
		{
			vertsRemaining -= batchSize;
			vertsRemainingInBatch = Min(batchSize, vertsRemaining);
			if (vertsRemainingInBatch > 0)
			{
				grcEnd();
				grcBegin(drawMode, vertsRemainingInBatch);
			}
		}

		if (ei->IsOnBoundary() && drawBoundaries) {
			grcColor3f(1.0f, 1.0f, 0.0f);
			grcVertex3f(ei->GetStart()->GetPosition());
			grcVertex3f(ei->GetEnd()->GetPosition());
			vertsRemainingInBatch -= vertsPerLoop;
		}
		else if (ei->IsStartSharp() && drawSharps) {
			grcColor4f(0.0f, 1.0f, 1.0f, 1.0f);
			grcVertex3f(ei->GetStart()->GetPosition());
			grcColor4f(0.0f, 1.0f, 1.0f, 0.0f);
			grcVertex3f(ei->GetEnd()->GetPosition());
			vertsRemainingInBatch -= vertsPerLoop;
		}
	}
	grcEnd();

	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi)
	{
		bool noCollapse = (*vi)->m_Flags.IsSet(deVertex::FLAG_NO_COLLAPSE);
		bool fixedPos = (*vi)->m_Flags.IsSet(deVertex::FLAG_FIXED_POSITION);

		if (noCollapse || fixedPos) {
			Vector3 pos = (*vi)->GetPosition();
			grcColor3f(noCollapse ? 1.0f : 0.0f, fixedPos ? 1.0f : 0.0f, 0.0f);

			grcBegin(drawLines, 4);
			grcVertex3f(pos + Vector3(1.0f, 0.0f, 1.0f));
			grcVertex3f(pos + Vector3(-1.0f, 0.0f, -1.0f));
			grcVertex3f(pos + Vector3(-1.0f, 0.0f, 1.0f));
			grcVertex3f(pos + Vector3(1.0f, 0.0f, -1.0f));
			grcEnd();
		}
	}
}


void deMesh::DebugDraw(bool drawLeft, bool drawRight)
{
	GatherDrawData();
	grcLightState::SetEnabled(false);

	Color32 basisEdge;
	basisEdge.Setf(0.7f, 0.7f, 0.7f);
	Color32 normalEdge;
	normalEdge.Setf(0.5f, 0.5f, 0.5f);

	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
		deVertex& vert = *(*vi);

		if ((vert.IsLeft() && !drawLeft) ||
			(vert.IsRight() && !drawRight)) {
				continue;
			}

			for(int e = 0; e < vert.m_Edges.GetCount(); e++) {
				deEdge* edge = vert.m_Edges[e];
				do {
					Color32 edgeColor;
					if (edge == vert.m_Edges[e]) {
						edgeColor = basisEdge;
					}
					else {
						edgeColor = normalEdge;
					}

					DebugDrawEdge(edge, edgeColor);

					edge = edge->GetNextCCW();
				} while (edge && edge != vert.m_Edges[e]);
			}
	}
}

void deMesh::CutWithPlane(const Vector4& plane, bool addBoundary)
{
	MarkDrawDataDirty();
	std::vector<deVertex*> copVerts;

	float tolerance = deWedge::GetVertSnapDist() * M_SQRT2;

	// Mark verts as left, right or coplanar
	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
		float dist = plane.DistanceToPlane((*vi)->GetPosition());
		if (dist < -tolerance) {
			(*vi)->SetLeft();
			(*vi)->m_Flags.Clear(deVertex::FLAG_PROCESSED);
		}
		else if (dist > tolerance) {
			(*vi)->SetRight();
			(*vi)->m_Flags.Clear(deVertex::FLAG_PROCESSED);
		}
		else {
			(*vi)->SetCoplanar();
			(*vi)->m_Flags.Set(deVertex::FLAG_PROCESSED);
			if (addBoundary) {
				copVerts.push_back(*vi);
			}
		}
	}

	std::vector<deEdge*> facesToCut;

	// Build a list of affected faces
	FaceEdgeIterator stop = EndFaceEdges();
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != stop; ++fi)
	{
		deVertex* v[3];
		fi->GetFaceVerts(v[0], v[1], v[2]);

		int numLeft = 0;
		int numRight = 0;
		int numCoplanar = 0;
		for(int i = 0; i < 3; i++) {
			if (v[i]->IsCoplanar()) {
				numCoplanar++;
			}
			else if (v[i]->IsLeft()) {
				numLeft++;
			}
			else if (v[i]->IsRight()) {
				numRight++;
			}
		}


		// Possibilities we can ignore (no cutting necessary)
		// L = 3, R = 0, C = 0
		// L = 2, R = 0, C = 1
		// L = 1, R = 0, C = 2

		// L = 0, R = 3, C = 0
		// L = 0, R = 2, C = 1
		// L = 0, R = 1, C = 2

		// L = 0, R = 0, C = 3

		if (numLeft == 0 || numRight == 0) {
			continue;
		}

		// Faces that need cutting

		// L = 1, R = 1, C = 1
		// Cut into two faces, one on each side

		// L = 2, R = 1, C = 0
		// Cut into 3 faces. Two on left, one on right

		// L = 1, R = 2, C = 0
		// Cut into 3 faces, One on left, two on right
		
		facesToCut.push_back(&(*fi));
	}

	// Now do the cutting (couldn't do it above cause you can't modify the topology while using a mesh iterator
	for(std::vector<deEdge*>::iterator face = facesToCut.begin(); face != facesToCut.end(); ++face)
	{
		deVertex* v[3];
		(*face)->GetFaceVerts(v[0], v[1], v[2]);

		deEdge* e[3];
		(*face)->GetFaceEdges(e[0], e[1], e[2]);

		int numLeft = 0;
		int numRight = 0;
		int numCoplanar = 0;
		int i;
		for(i = 0; i < 3; i++) {
			if (v[i]->IsCoplanar()) {
				numCoplanar++;
			}
			else if (v[i]->IsLeft()) {
				numLeft++;
			}
			else if (v[i]->IsRight()) {
				numRight++;
			}
		}

		demAssertf(numLeft > 0 || numRight > 0, "A coplanar face ended up in the cutting list");

		if (numCoplanar == 1)
		{
			// find the coplanar, left and right verts

			// Face is ordered c->l->r->c
			// New vertex is created between l and r

			deVertex* c = NULL;
			deVertex* l = NULL;
			deVertex* r = NULL;

			deEdge* cl = NULL;
			deEdge* lr = NULL;
			deEdge* rc = NULL;

			for(i = 0; i < 3; i++) {
				if (v[i]->IsCoplanar())
				{
					c = v[i];
					cl = e[i];
					l = v[(i+1) % 3];
					lr = e[(i+1) % 3];
					r = v[(i+2) % 3];
					rc = e[(i+2) % 3];
					break;
				}
			}

			// Clear the face edge flags (we'll be restoring them later)
			cl->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);
			lr->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);
			rc->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);

			bool vertExisted = false;
			deVertex* newVert = NULL;

			deEdge* lrNeighbor = lr->GetNeighbor();

			// See if a previous cut has put a vertex in the right spot
			if (lrNeighbor) {
				if (lrNeighbor->GetEnd() != l) {
					newVert = lrNeighbor->GetEnd();
					vertExisted = true;
				}
			}

			deWedge newWedge;

			// Find intersection point
			float t;
			geomSegments::CollideRayPlane(l->GetPosition(), r->GetPosition() - l->GetPosition(), plane, &t);

			newWedge.InterpolateAttributes(t, lr->GetStartWedge(), lr->GetEndWedge());

			if (!newVert)
			{
				// No new vert, so create one
				newVert = rage_new deVertex(newWedge);

				newVert->SetCoplanar();
				newVert->m_Flags.Set(deVertex::FLAG_PROCESSED);
				if (addBoundary) {
					copVerts.push_back(newVert);
				}
			
				m_Vertices.push_back(newVert);
				AddVertexToGrid(newVert);
			}

			// Connect it up
			deEdge* nc = rage_new deEdge;
			nc->m_Wedge = newVert->FindOrAddWedge(newWedge);
			nc->m_Vertex = c;

			deEdge* cn = rage_new deEdge;
			cn->m_Wedge = cl->m_Wedge;
			cn->m_Vertex = newVert;

			deEdge* nr = rage_new deEdge;
			nr->m_Wedge = newVert->FindOrAddWedge(newWedge);
			nr->m_Vertex = r;

			deEdge* ln = lr;
			// no change in wedge
			ln->m_Vertex = newVert;

			// Set up new prev pointers

			// Left face
			ln->m_Prev = cl;
			cl->m_Prev = nc;
			nc->m_Prev = ln;

			// Right face
			nr->m_Prev = cn;
			cn->m_Prev = rc;
			rc->m_Prev = nr;

			// Set up new neighbors
			deEdge::SetNeighbors(nc, cn);

			if (vertExisted) {
				// we can find neighbors for the new edges and hook them up
				deEdge::SetNeighbors(nr, lrNeighbor);
				deEdge::SetNeighbors(ln, newVert->GetEdge());
			}
			else {
				newVert->m_Edges.Grow(1) = nr;
				deEdge::SetNeighbors(nr, NULL);
				deEdge::SetNeighbors(ln, NULL);
			}

			// Restore the face edge flags
			cl->m_Flags.Set(deEdge::FLAG_FACE_EDGE);
			rc->m_Flags.Set(deEdge::FLAG_FACE_EDGE);
		}
		else {
			demAssertf(numCoplanar == 0, "numCoplanar should be 0 or 1, not %d", numCoplanar);

			// Vertex a is on one side of the plane.
			// Vertex b1 and b2 are on the other side

			// Face is ordered a->b1->b2->a

			// n1 is created between a and b1
			// n2 is created between a and b2

			deVertex* a = NULL;
			deVertex* b1 = NULL;
			deVertex* b2 = NULL;

			deEdge* ab1 = NULL;
			deEdge* b1b2 = NULL;
			deEdge* b2a = NULL;

			for(int i = 0; i < 3; i++) {
				if ((numLeft == 1 && v[i]->IsLeft()) ||
					(numRight == 1 && v[i]->IsRight())) {
					a = v[i];
					b1 = v[(i+1) % 3];
					b2 = v[(i+2) % 3];
					
					ab1 = e[i];
					b1b2 = e[(i+1) % 3];
					b2a = e[(i+2) % 3];
					break;
				}
			}

			// Clear the face edge flags (we'll be restoring them later)
			ab1->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);
			b1b2->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);
			b2a->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);

			bool n1existed = false;
			bool n2existed = false;

			deVertex* n1 = NULL;
			deVertex* n2 = NULL;

			deEdge* ab1Neighbor = ab1->GetNeighbor();
			deEdge* b2aNeighbor = b2a->GetNeighbor();

			if (ab1Neighbor) {
				if (ab1Neighbor->GetEnd() != a) {
					n1 = ab1Neighbor->GetEnd();
					n1existed = true;
				}
			}
			if (b2aNeighbor) {
				if (b2aNeighbor->GetEnd() != b2) {
					n2 = b2aNeighbor->GetEnd();
					n2existed = true;
				}
			}

			float t;

			// Hook up vertex n1

			// Find the wedge for the n1 intersection
			deWedge n1Wedge;
			// Find intersection point
			geomSegments::CollideRayPlane(a->GetPosition(), b1->GetPosition() - a->GetPosition(), plane, &t);

			if (ab1->GetStartWedge().GetMaterial() != ab1->GetEndWedge().GetMaterial()) {
				// do something
				Printf("Error! Material changed across a face");
				deMeshErrorEdge error(this, "Material changed across a face", ab1);
				ReportError(error);
			}

			n1Wedge.InterpolateAttributes(t, ab1->GetStartWedge(), ab1->GetEndWedge());

			if (!n1) {
				// No new vert, so create one
				n1 = rage_new deVertex(n1Wedge);

				n1->SetCoplanar();
				n1->m_Flags.Set(deVertex::FLAG_PROCESSED);
				if (addBoundary) {
					copVerts.push_back(n1);
				}
				m_Vertices.push_back(n1);
				AddVertexToGrid(n1);
			}
			else
			{
				n1Wedge.SetPosition(n1->GetPosition());
			}

			deEdge* an1 = ab1;
			an1->m_Vertex = n1;

			deEdge* n1b1 = rage_new deEdge;
			n1b1->m_Wedge = n1->FindOrAddWedge(n1Wedge);
			n1b1->m_Vertex = b1;


			// Hook up vertex n2

			// Find the wedge for the n2 intersection
			deWedge n2Wedge;
			// Find intersection point
			geomSegments::CollideRayPlane(b2->GetPosition(), a->GetPosition() - b2->GetPosition(), plane, &t);

			n2Wedge.InterpolateAttributes(t, b2a->GetStartWedge(), b2a->GetEndWedge());

			if (!n2) {
				n2 = rage_new deVertex(n2Wedge);

				n2->m_Flags.Set(deVertex::FLAG_PROCESSED);
				if (addBoundary) {
					copVerts.push_back(n2);
				}
				n2->SetCoplanar();

				m_Vertices.push_back(n2);
				AddVertexToGrid(n2);
			}
			else
			{
				n2Wedge.SetPosition(n2->GetPosition());
			}

			deEdge* b2n2 = b2a;
			b2n2->m_Vertex = n2;

			deEdge* n2a = rage_new deEdge;
			n2a->m_Wedge = n2->FindOrAddWedge(n2Wedge);
			n2a->m_Vertex = a;

			deEdge* n1n2 = rage_new deEdge;
			n1n2->m_Wedge = n1->FindOrAddWedge(n1Wedge);
			n1n2->m_Vertex = n2;

			deEdge* n2n1 = rage_new deEdge;
			n2n1->m_Wedge = n2->FindOrAddWedge(n2Wedge);
			n2n1->m_Vertex = n1;

			// Connect face pointers
			// a->n1->n2
			an1->m_Prev = n2a;
			n2a->m_Prev = n1n2;
			n1n2->m_Prev = an1;

			deEdge::SetNeighbors(n1n2, n2n1);

			// Connect whichever edge (n1b2 or n2b1) would be shorter
			if (n2->GetPosition().Dist2(b1->GetPosition()) < n1->GetPosition().Dist2(b2->GetPosition())) {
				// Connect n2b1
				deEdge* n2b1 = rage_new deEdge;
				n2b1->m_Wedge = n2->FindOrAddWedge(n2Wedge);
				n2b1->m_Vertex = b1;

				deEdge* b1n2 = rage_new deEdge;
				b1n2->m_Wedge = b1b2->m_Wedge;
				b1n2->m_Vertex = n2;

				deEdge::SetNeighbors(n2b1, b1n2);

				// Face n1->b1->n2
				n1b1->m_Prev = n2n1;
				n2n1->m_Prev = b1n2;
				b1n2->m_Prev = n1b1;

				// Face b1->b2->n2
				b1b2->m_Prev = n2b1;
				n2b1->m_Prev = b2n2;
				b2n2->m_Prev = b1b2;
			}
			else {
				// Connect n1b2

				deEdge* n1b2 = rage_new deEdge;
				n1b2->m_Wedge = n1->FindOrAddWedge(n1Wedge);
				n1b2->m_Vertex = b2;

				deEdge* b2n1 = rage_new deEdge;
				b2n1->m_Wedge = b2n2->m_Wedge;
				b2n1->m_Vertex = n1;

				deEdge::SetNeighbors(n1b2, b2n1);

				// Face n1->b1->b2
				n1b1->m_Prev = b2n1;
				b2n1->m_Prev = b1b2;
				b1b2->m_Prev = n1b1;

				// Face n1->b2->n2
				n1b2->m_Prev = n2n1;
				n2n1->m_Prev = b2n2;
				b2n2->m_Prev = n1b2;
			}	

			// Now connect external neighbors
			if (n1existed) {
				deEdge::SetNeighbors(n1b1, ab1Neighbor);
				deEdge::SetNeighbors(n1->GetEdge(), an1);
			}
			else {
				n1->m_Edges.Grow(1) = n1b1;
				deEdge::SetNeighbors(n1b1, NULL);
				deEdge::SetNeighbors(an1, NULL);
			}

			if (n2existed) {
				deEdge::SetNeighbors(n2a, b2aNeighbor);
				deEdge::SetNeighbors(b2n2, n2->GetEdge());
			}
			else {
				n2->m_Edges.Grow(1) = n2a;
				deEdge::SetNeighbors(n2a, NULL);
				deEdge::SetNeighbors(b2n2, NULL);
			}

			// Restore the face edge flags
			n2a->m_Flags.Set(deEdge::FLAG_FACE_EDGE);
			b2n2->m_Flags.Set(deEdge::FLAG_FACE_EDGE);
			n1b1->m_Flags.Set(deEdge::FLAG_FACE_EDGE);
		}
	}

	if (addBoundary) {
		// build a list of coplanar verts
		// for each one, find the list of coplanar outgoing edges
		//   for each item in the list, create a new vert with that edge as a primary edge
		
		// Some special cases: One edge coplanar, no neighbor. Whole face coplanar.

		// The vertex only needs to be split if it has left _and_ right outgoing edge ends.
		// If not, just classify the vert as whatever its non-coplanar edge is. 

		// Starting at the basis edge, loop till we find a coplanar edge (it's possible there aren't any, if this was a "corner"
		// vertex that was coplanar, in which case classify the vert the same as the other two verts in the face)
		// Once we find a coplanar edge, loop till the end of the edge is no longer coplanar. Classify the vert 

		// New marking contains a bool for whether or not the current vert should be marked "left" at the end of this function
		std::vector<std::pair<deVertex*, bool> > newMarking;
		std::vector<deEdge*> edgesToSplit;

		const bool LeftSide = true;
		const bool RightSide = false;

		for(std::vector<deVertex*>::iterator cop_iter = copVerts.begin(); cop_iter != copVerts.end(); ++cop_iter)
		{
#if 0 // TODO: Remove me
			Printf("cop %d\n", (*cop_iter)->m_Index);
#endif

			int classification = (*cop_iter)->ClassifyNeighborhood();
			switch(classification) {
			case deVertex::CLASSIFY_COPLANAR:
			case deVertex::CLASSIFY_LEFT:
			case deVertex::CLASSIFY_LEFT | deVertex::CLASSIFY_COPLANAR:
				newMarking.push_back(std::make_pair((*cop_iter), LeftSide));
				break;
			case deVertex::CLASSIFY_RIGHT:
				newMarking.push_back(std::make_pair((*cop_iter), RightSide));
				break;
			case deVertex::CLASSIFY_RIGHT | deVertex::CLASSIFY_COPLANAR:
			case deVertex::CLASSIFY_COPLANAR | deVertex::CLASSIFY_LEFT | deVertex::CLASSIFY_RIGHT:
			case deVertex::CLASSIFY_LEFT | deVertex::CLASSIFY_RIGHT:
				// need to split this vertex. 
				// for consistency, consider coplanar faces "left" faces  
				{
					// For a boundary edge...

					// Start on the edge. If the first face is left, then while the face is left (or coplanar) continue around.
					// Once we hit an edge on a right face, split the vertex, make the edge the new vert's basis edge, and 
					// continue until hitting a left (or coplanar) face.

					// For a non-boundary edge, loop around until we hit a splitting edge. Make this the new basis edge

					deEdge* edge = (*cop_iter)->GetEdge();
					if (edge->IsOnBoundary()) {
						// do nothing, this is a fine edge to start on
					}
					else {
						// circle around until we find a splitting edge

						deEdge* firstEdge = edge;
						bool splitEdge = true;

						while(ClassifyFaceForSplit(edge) == ClassifyFaceForSplit(edge->GetNextCCW())) {
							edge = edge->GetNextCCW();
							if (edge == firstEdge) {
								// We've looped and not found any splitting edge
								// There is a special case for right/coplanar verts, where only edges (not faces) are coplanar. In this case
								// classify the vert as right and don't split any edges
								splitEdge = false;
								break;
							}
						}
						if (!splitEdge) {
							// This should be the only time no split edge can be found
							demAssertf(classification == (deVertex::CLASSIFY_COPLANAR | deVertex::CLASSIFY_RIGHT), "Invalid classification: %d", classification);
							newMarking.push_back(std::make_pair((*cop_iter), RightSide));
							break; // break out of the switch
						}

						edge = edge->GetNextCCW(); // step once more so that edge and edge->GetNeighbor() are on opposite sides
						(*cop_iter)->m_Edges[0] = edge;
						edgesToSplit.push_back(edge);
#if 0 // TODO: Remove me
						Printf("  s %d\n", edge->m_Index);
#endif
					}

					deEdge* firstEdge = edge;

					bool faceSide = ClassifyFaceForSplit(edge);
					deVertex* v = (*cop_iter);
					newMarking.push_back(std::make_pair(v, faceSide));

					do {
						bool currFaceSide = ClassifyFaceForSplit(edge);
						if (faceSide != currFaceSide) {
							faceSide = currFaceSide;
							v = rage_new deVertex((*cop_iter)->GetWedge(0));
							v->m_Wedges.Reset();
							v->m_Wedges.Resize((*cop_iter)->GetNumWedges());
							for(int i = 0; i < (*cop_iter)->GetNumWedges(); i++) {
								// Was using FindOrAddWedge here but (1) its slower and (2) if a vert had a duplicate wedge
								// for some reason, FindOrAddWedge would eliminate it, which breaks the indices.
								// So for now we leave the dups in place.
								v->m_Wedges[i] = (*cop_iter)->GetWedge(i);
								// we'll reduce the wedge sets once the edges have been all split apart.
							}
							v->m_Flags.Set(deVertex::FLAG_PROCESSED);
							v->SetCoplanar();
							v->m_Edges.Grow(1) = edge;
							edgesToSplit.push_back(edge);
#if 0 // TODO: Remove me
							Printf("  s %d\n", edge->m_Index);
#endif
							AddVertexToGrid(v);
							m_Vertices.push_back(v);
							newMarking.push_back(std::make_pair(v, faceSide));
						}
						edge->GetPrev()->m_Vertex = v;
						edge = edge->GetNextCCW();
					} while(edge && edge != firstEdge);
				}
				break;
			case 0:
			default:
				Quitf("Invalid result from ClassifyNeighborhood()");
			}
		}

		for(std::vector<deEdge*>::iterator ei = edgesToSplit.begin(); ei != edgesToSplit.end(); ++ei)
		{
			deEdge* neighbor = (*ei)->GetNeighbor();
			deEdge::SetNeighbors(neighbor, NULL);
			deEdge::SetNeighbors((*ei), NULL);
		}

		for(std::vector<std::pair<deVertex*, bool> >::iterator pi = newMarking.begin(); pi != newMarking.end(); ++pi)
		{
			if ((*pi).second == LeftSide) {
				(*pi).first->SetLeft();
			}
			else {
				(*pi).first->SetRight();
			}

			// Now that the edges have been split, see if there are unnecessary wedges in the wedge list
			(*pi).first->ReduceWedgeSet();
		}

	}

}

void deMesh::MarkVertsOnPlane(const Vector4& plane, int bitToSet, bool newValue)
{
	float tolerance = deWedge::GetVertSnapDist() * M_SQRT2;

	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi)
	{
		float dist = plane.DistanceToPlane((*vi)->GetPosition());
		if (fabsf(dist) < tolerance) {
			(*vi)->m_Flags.Set(bitToSet, newValue);
		}
	}
}


/*
Diagram of an edge collapse (taken from Hoppe)

                    \  /
                     Vs
                    / | \
                   /  |  \
                  /   |   \   /
               \ /    |    \ /         \    \ /    /
               Vl     |    Vr   =>     Vl---Vs---Vr 
               / \    |    / \         /    /|\    \
                  \   |   /   \
                   \  |  /
                    \ | /
                  ---Vt---
                      |
*/

bool deMesh::CanCollapse(deEdge* e)
{
	demAssertf(e, "Can't collapse NULL edge");

	deVertex* vertS = e->GetStart();
	deVertex* vertT = e->GetEnd();

	deVertex* vertR = e->GetNext()->GetEnd();

	deVertex* vertL = NULL;
	if (e->GetNeighbor()) {
		vertL = e->GetNeighbor()->GetNext()->GetEnd();
	}

	if (vertR == vertL) {
		demWarningf("Possible non-manifold triangle found at the following coords:");
		demWarningf("    (%f, %f, %f)", vertS->GetPosition().x, vertS->GetPosition().y, vertS->GetPosition().z);
		demWarningf("    (%f, %f, %f)", vertT->GetPosition().x, vertS->GetPosition().y, vertS->GetPosition().z);
		demWarningf("    (%f, %f, %f)", vertR->GetPosition().x, vertS->GetPosition().y, vertS->GetPosition().z);
		return false;
	}

	// Get a bunch of edge pointers
	deEdge* st = e;
	deEdge* rs = st->GetPrev();
	deEdge* tr = rs->GetPrev();
	deEdge* rt = tr->GetNeighbor();
	deEdge* sr = rs->GetNeighbor();

	deEdge* ts = e->GetNeighbor();
	deEdge* ls = NULL;
	deEdge* sl = NULL;
	deEdge* lt = NULL;
	deEdge* tl = NULL;
	if (ts) {
		lt = ts->GetPrev();
		sl = lt->GetPrev();
		ls = sl->GetNeighbor();
		tl = lt->GetNeighbor();
	}

	// Disallow collapse of last triangle
	if (ts == NULL && sr == NULL && rt == NULL) {
//		demWarningf("Collapse would remove last triangle");
		return false;
	}

#if 0
	// Check for "discontinuity" curves (right now a discontinuity is a boundary edge)
	// (Rules taken from Hoppe, simplified)
	if ((ts != NULL && ls == NULL && lt == NULL) ||
		(rs == NULL && rt == NULL) ||
		(vertS->IsOnBoundary() && vertT->IsOnBoundary() && ts != NULL)
		) 
	{
//		demWarningf("Collapse would modify topology");
		return false;
	}
#endif

	// Check that the edge collapse wouldn't affect the topology of any discontinuity curves
	// Tests taken from Hoppe 
	bool slSharp = (ls && ls->IsSharp()) || (sl && sl->IsSharp());
	bool tlSharp = (tl && tl->IsSharp()) || (lt && lt->IsSharp());
	bool srSharp = rs->IsSharp(); // rs must exist
	bool trSharp = tr->IsSharp(); // tr must exist
	bool stSharp = st->IsSharp(); // st must exist

	int numSharpsS = vertS->CountSharpEdges();
	int numSharpsT = vertT->CountSharpEdges();

	if (slSharp && tlSharp) {
		return false;
	}
	if (srSharp && trSharp) {
		return false;
	}
	if (numSharpsS >= 1 && numSharpsT >= 1 && !stSharp) {
		return false;
	}
	if (numSharpsS >= 3 && numSharpsT >= 3 && stSharp) {
		return false;
	}
	if (stSharp && numSharpsS == 1 && numSharpsT != 2) {
		return false;
	}
	if (stSharp && numSharpsT == 1 && numSharpsS != 2) {
		return false;
	}

	// TODO: See if there's a better way to handle these cases than by totally disallowing them
	// Special checks, if st is only half sharp, disallow (cause we don't know how to merge the 
	// two wedges on one vert with the one wedge on the other vert.
	if (stSharp && ts && (!st->IsStartSharp() || !ts->IsStartSharp())) {
		return false;
	}

#if 0
	// Disallow subgraphs that look like this (or the equivalent with l in place of r)
	// s--
	// |\  \							// 
	// | r--x
	// |/  /
	// t--

	if (rt && sr && rt->GetNext()->GetEnd() == sr->GetNext()->GetEnd()) {
//		demWarningf("Collapse would create degenerate triangles");
		return false;
	}

	if (tl && ls && tl->GetNext()->GetEnd() == ls->GetNext()->GetEnd()) {
//		demWarningf("Collapse would create degenerate triangles");
		return false;
	}
#endif


	// Need stricter check than the above
	// Count how many of S's neighbors are also T's neighbor. 
	// We allow 2 if ST has a left and right face, otherwise only allow 1

	int sharedVerts = 0;
	for(deVertex::VertexIterator vi = vertS->BeginVertices(); vi != vertS->EndVertices(); ++vi)
	{
		if (vi->FindEdge(vertT) || vertT->FindEdge(&*vi)) {
			sharedVerts++;
		}
	}

	if (sharedVerts > 2 ||  // At most only r and l may be shared
		(sharedVerts > 1 && vertL == NULL)) // If l does not exist, only r may be shared
	{
//		demWarningf("Collapse would modify topology (2)");
		return false;
	}

	// One more special case. Don't collapse any side of a tetrahedron
	if (vertL) {
		// One face (TLR or SRL) is ok, but not both
		bool hasSRL = ls && ls->GetNext()->GetEnd() == vertR;
		bool hasTLR = rt && rt->GetNext()->GetEnd() == vertL;
		if (hasSRL && hasTLR) {
//			demWarningf("Collapse would squish a tetrahedron");
			return false;
		}
	}

	return true;
}

// Collapse the S->T half edge (and the symmetric edge)
void deMesh::Collapse(deEdge* e, float t)
{
	MarkDrawDataDirty();
	demAssertf(e, "Can't collapse NULL edge");
	demAssertf(t >= 0.0f && t <= 1.0f, "t value out of rage [0, 1]");

	deVertex* vertS = e->GetStart();
	deVertex* vertT = e->GetEnd();

	deVertex* vertR = e->GetNext()->GetEnd();

	// vertL could be NULL!
	deVertex* vertL = NULL;
	if (e->GetNeighbor()) {
		vertL = e->GetNeighbor()->GetNext()->GetEnd();
	}

	// Get a bunch of edge pointers
	deEdge* st = e;
	deEdge* rs = st->GetPrev();
	deEdge* tr = rs->GetPrev();
	deEdge* rt = tr->GetNeighbor();
	deEdge* sr = rs->GetNeighbor();

	deEdge* ts = e->GetNeighbor();
	deEdge* ls = NULL;
	deEdge* sl = NULL;
	deEdge* lt = NULL;
	deEdge* tl = NULL;
	if (ts) {
		lt = ts->GetPrev();
		sl = lt->GetPrev();
		ls = sl->GetNeighbor();
		tl = lt->GetNeighbor();
	}


	Vector3 newPos;
	newPos.Lerp(t, vertS->GetPosition(), vertT->GetPosition());


	// Move vertS into new position
	MoveVertex(*vertS, newPos);

	// Move vertT into new position too (really just updates the vert positions on all the wedges)
	vertT->SetPosition(newPos);

	if ((!sr || !sr->IsStartSharp()) || (!rt || !tr->IsStartSharp())) {
		// wedges are interpolable
		deWedge interpWedge;
//		demAssertf(fabsf(st->GetStartWedge().GetVertexData().Tex[0].x - st->GetEndWedge().GetVertexData().Tex[0].x) < 0.5f, "Tex coord change too large");
		interpWedge.InterpolateAttributes(t, st->GetStartWedge(), st->GetEndWedge());
		st->GetStartWedge() = interpWedge;
		st->GetEndWedge() = interpWedge;
	}

	if (ts && ((!ls || !sl->IsStartSharp()) || (!tl || !tl->IsStartSharp()))) {
		deWedge interpWedge;
//		demAssertf(fabsf(ts->GetStartWedge().GetVertexData().Tex[0].x - ts->GetEndWedge().GetVertexData().Tex[0].x) < 0.5f, "Tex coord change too large");
		interpWedge.InterpolateAttributes(t, ts->GetEndWedge(), ts->GetStartWedge());
		ts->GetStartWedge() = interpWedge;
		ts->GetEndWedge() = interpWedge;
	}

	// If vertT was fixed or incollapsable, copy those flags to vertS
	if (vertT->IsFixed()) {
		vertS->SetFixed();
	}
	if (!vertT->IsCollapseAllowed()) {
		vertS->SetCollapseAllowed(false);
	}

	// Disconnect edges we'll be removing from the verts that might hold pointers to them.
	
	// Find a new basis edge for S
	deEdge* sBasis = NULL;
	if (vertT->IsOnBoundary()) {
		if (vertT->GetEdge() == tr) {
			// if the basis edge is the one we're going to delete, change it to its new value
			sBasis = sr;
		}
		else {
			sBasis = vertT->GetEdge();
		}
	}
	else if (vertS->IsOnBoundary()) {
		if (vertS->GetEdge() == sl) {
			// if the basis edge is the one we're going to delete, change it to its new value
			sBasis = tl;
		}
		else {
			sBasis = vertS->GetEdge();
		}
	}
	else {
		sBasis = sr; // Any edge is OK. This one is exists (or else vertS would be on a boundary) and will remain after collapse.
	}

	if (vertL) {
		vertL->DisconnectEdge(lt);
	}
	vertR->DisconnectEdge(rs);

	vertS->m_Edges[0] = sBasis;

	// Point all edges that pointed to vertT to vertS instead
	// Skipping st and rt?
	// Using edge iterator here should be OK because iterator uses prev and neighbor, not vertex
	CombineVerts(vertS, vertT);

	// Reconnect edges so none of the edges that stay hold pointers to the removed edges
	if (vertL) {
		deEdge::SetNeighbors(ls, tl);
	}
	deEdge::SetNeighbors(sr, rt);

	// delete some edges!
	delete st;
	delete tr;
	delete rs;

	delete ts;
	delete sl;
	delete lt;

	DeleteVertex(vertT);

	// clean up wedge list
	vertS->ReduceWedgeSet();

}

/*
Diagram of an edge split. Note this is not the same as a vertex split 
(which is the opposite of an edge collapse for progressive meshes)

                    \  /                   \  /
                     Vs                     Vs
					/ | \                  / | \
				   /  |  \                /  |  \
                  /   |   \   /          /   |   \   /
               \ /    |    \ /        \ /    |    \ /         
               Vl     |    Vr   =>    Vl----Vc----Vr 
               / \    |    / \        / \    |    / \
                  \   |   /   \          \   |   /   \
                   \  |  /                \  |  /
                    \ | /                  \ | /
                  ---Vt---               ---Vt---
                      |                      |
*/


deVertex* deMesh::SplitEdge(deEdge* e, float tVal)
{
	demAssertf(e, "Can't split NULL edge");

	MarkDrawDataDirty();

	// Start by getting all the pointers lined up
	deEdge* st = e;
	deEdge* rs = st->GetPrev();
	deEdge* tr = rs->GetPrev();

	// Clear the face edge flags (we'll restore them later)
	st->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);
	tr->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);
	rs->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);


	// Get the ts pointer, but clear the neighbor connection for now (for simplicity)
	deEdge* ts = e->GetNeighbor();
	deEdge::SetNeighbors(st, NULL);
	deEdge::SetNeighbors(ts, NULL);

	deVertex* s = rs->GetEnd();
	deVertex* t = st->GetEnd();
	deVertex* r = tr->GetEnd();


	deWedge wedgeR;
	wedgeR.InterpolateAttributes(tVal, st->GetStartWedge(), st->GetEndWedge());

	deVertex* c = rage_new deVertex(wedgeR);
	m_Vertices.push_back(c);
	AddVertexToGrid(c);

	// Make some new edges (and reuse an existing one)
	deEdge* sc = st;
	// no change in wedge
	sc->m_Vertex = c;

	deEdge* cr = rage_new deEdge;
	cr->m_Wedge = c->FindOrAddWedge(wedgeR);
	cr->m_Vertex = r;

	deEdge* rc = rage_new deEdge;
	rc->m_Wedge = rs->m_Wedge;
	rc->m_Vertex = c;

	deEdge* ct = rage_new deEdge;
	ct->m_Wedge = cr->m_Wedge;
	ct->m_Vertex = t;

	// Now set the new prev pointers
	
	// s->c->r->s
	rs->m_Prev = cr;
	cr->m_Prev = sc;
	sc->m_Prev = rs;

	// t->r->c->t
	ct->m_Prev = rc;
	rc->m_Prev = tr;
	tr->m_Prev = ct;

	// Set up new neighbors
	deEdge::SetNeighbors(rc, cr);

	// Add the new edge to c
	c->m_Edges.Grow(1) = ct;

	// Restore the face edge flags
	sc->m_Flags.Set(deEdge::FLAG_FACE_EDGE);
	ct->m_Flags.Set(deEdge::FLAG_FACE_EDGE);

	// Now hook up the neighboring face (if there was one)
	if (ts)
	{
		// Need to do the same stuff for the neighboring face
		deEdge* lt = ts->GetPrev();
		deEdge* sl = lt->GetPrev();

		deVertex* l = sl->GetEnd();

		// Clear the face edge flags (we'll restore them later)
		ts->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);
		sl->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);
		lt->m_Flags.Clear(deEdge::FLAG_FACE_EDGE);

		deWedge wedgeL;
		wedgeL.InterpolateAttributes(tVal, ts->GetEndWedge(), ts->GetStartWedge()); // Interp from s to t still

		deEdge* tc = ts;
		// leave wedge alone
		tc->m_Vertex = c;

		deEdge* cl = rage_new deEdge;
		cl->m_Wedge = c->FindOrAddWedge(wedgeL);
		cl->m_Vertex = l;

		deEdge* lc = rage_new deEdge;
		lc->m_Wedge = lt->m_Wedge;
		lc->m_Vertex = c;

		deEdge* cs = rage_new deEdge;
		cs->m_Wedge = cl->m_Wedge;
		cs->m_Vertex = s;

		// Now set up the new prev pointers

		// t->c->l->t
		lt->m_Prev = cl;
		cl->m_Prev = tc;
		tc->m_Prev = lt;

		// s->l->c->s
		cs->m_Prev = lc;
		lc->m_Prev = sl;
		sl->m_Prev = cs;

		// Set up new neighbors
		deEdge::SetNeighbors(cl, lc);

		// Join with the other faces
		deEdge::SetNeighbors(sc, cs);
		deEdge::SetNeighbors(ct, tc);

		// Don't need to add the new edge to c
		// (it already has an outgoing edge)

		// Restore the face edge flags
		tc->m_Flags.Set(deEdge::FLAG_FACE_EDGE);
		cs->m_Flags.Set(deEdge::FLAG_FACE_EDGE);
	}
	else
	{
		deEdge::SetNeighbors(ct, NULL);
	}

	c->ReduceEdgeSet();

	return c;
}

void deMesh::StitchEdge(deEdge* a, deEdge* b, bool meetMidway /* = false */)
{
	MarkDrawDataDirty();

	demAssertf(a && a->IsOnBoundary(), "Can only stitch non-NULL boundary edges");
	demAssertf(b && b->IsOnBoundary(), "Can only stitch non-NULL boundary edges");
	demAssertf(a != b, "Can't stitch an edge to itself");
	demAssertf(a->GetNextAlongBoundary()->GetNextAlongBoundary() != b, "Can't stitch a triangular hole closed"); // Can't use this function to collapse a triangle
	demAssertf(b->GetNextAlongBoundary()->GetNextAlongBoundary() != a, "Can't stitch a triangular hole closed"); // Can't use this function to collapse a triangle

	deVertex* aStart = a->GetStart();
	deVertex* aEnd = a->GetEnd();

	deVertex* bStart = b->GetStart();
	deVertex* bEnd = b->GetEnd();

	// There are a few special cases but they all boil down to:
	// Does aStart == bEnd or not, and 
	// Does bStart == aEnd or not

	// Note that the choice of vertex we keep and the one we delete is 
	// intentional. we keep the 'end' verts because they have the right outgoing edge
	// for the boundary already

	if (aEnd != bStart)
	{
		Vector3 newAEndPos = aEnd->GetPosition();
		if (meetMidway)
		{
			newAEndPos.Lerp(0.5f, aEnd->GetPosition(), bStart->GetPosition());
		}
		deMesh::MoveVertex(*aEnd, newAEndPos);
		deMesh::MoveVertex(*bStart, newAEndPos);

		CombineVerts(aEnd, bStart);
		DeleteVertex(bStart);
	}

	if (aStart != bEnd)
	{
		Vector3 newBEndPos = bEnd->GetPosition();
		if (meetMidway)
		{
			newBEndPos.Lerp(0.5f, bEnd->GetPosition(), aStart->GetPosition());
		}
		deMesh::MoveVertex(*aStart, newBEndPos);
		deMesh::MoveVertex(*bEnd, newBEndPos);
		CombineVerts(bEnd, aStart);
		DeleteVertex(aStart);
	}

	deEdge::SetNeighbors(a, b);
}

#define SAVE_EVERY_STITCH 0

float ProjectedSignedAngle(Vector3::Param normal, Vector3::Param v1Param, Vector3::Param v2Param)
{
	Vector3 v1(v1Param);
	Vector3 v2(v2Param);
	// normal must be normalized
	// Project v1 onto N, subtract that projection from v1 to get the projection of v1 onto the plane
	Vector3 v1DotN = v1.DotV(normal);
	Vector3 v1ProjN;
	v1ProjN.Multiply(v1, v1DotN);

	Vector3 v1ProjP = v1 - v1ProjN;

	// Project v2 onto N, subtract that projection from v2 to get the projection of v2 onto the plane
	Vector3 v2DotN = v2.DotV(normal);
	Vector3 v2ProjN;
	v2ProjN.Multiply(v2, v2DotN);

	Vector3 v2ProjP = v2 - v2ProjN;

	Vector3 cross;
	cross.Cross(v1ProjP, v2ProjP);

	float direction = cross.Dot(normal);
	float scale = direction > 0.0f ? 1.0f : -1.0f;
	
	return v1ProjP.Angle(v2ProjP) * scale;
}

int deMesh::RemoveHoles(float angleTolerance/* = 0.01f*/, float areaTolerance /* = 0.1f */)
{
	MarkVertsUnprocessed();

	int edgesStitched = 0;

	float areaSquared4 = 4.0f * areaTolerance * areaTolerance;
	// for vectors a->b->c, |ba x bc|^2 = (2 * area)^2

	VertexIterator vi = BeginVertices();
	while(vi != EndVertices())
	{
		if ((*vi)->m_Flags.IsSet(deVertex::FLAG_PROCESSED))
		{
			++vi;
			continue; // Only process each vertex once
		}
		(*vi)->m_Flags.Set(deVertex::FLAG_PROCESSED);

		if (!(*vi)->IsOnBoundary())
		{
			++vi;
			continue; // We only want to process boundary edges
		}

		bool startOver = false;

		// ok found a boundary edge. Call it and its adjacent verts a->b->c
		deVertex* b = (*vi);
		bool keepGoing = true;

		while(keepGoing)
		{
			deEdge* bc = b->WalkAlongBoundary();
			deVertex* c = bc->GetEnd();
			deEdge* ab = bc->GetPrevAlongBoundary();
			deVertex* a = ab->GetStart();

			Vector3 bcVec = c->GetPosition() - b->GetPosition();
			Vector3 baVec = a->GetPosition() - b->GetPosition();

			Vector3 cross;
			cross.Cross(bcVec, baVec);

			// Only fill the small area if bc and ba are < 90 degrees apart
			// (prevents stitching of edges that are nearly 180 deg. apart)
			bool smallArea = cross.Mag2() < areaSquared4 && bcVec.Dot(baVec) > 0.0f;

			// We need to test to make sure the edge we're about to stitch is a concave edge
			// This is a little tricky. Here are some things that don't work:
			//
			// 1) Compare the cross product (bcVec x baVec) with the average face normal
			//		If you consider a vert and its neighborhood as a pacman shape, this fails
			//		when you stretch pacmans upper lip below his lower (flipping the order of the 
			//		boundary edges and reversing the sign of the cross product)
			//
			// 2) Summing the face angles from bc to ba.
			//		Mostly works, but if you have a very narrow convex angle with lots of faces
			//		(imagine a pleated fan) you could easily get a sum of angles that's
			//		> 180 or whatever you're testing for.

			// Latest test:
			//	Project the mesh faces onto the plane formed by the average face normal, and sum those 
			//  (signed) angles. 
			bool isConcave = false;

			float wedgeAngle = bcVec.Angle(baVec);

			bool wideAngle = wedgeAngle > PI * 0.5f; // only consider acute angles

			if (wideAngle)
			{
				break;
			}

			bool narrowAngle = wedgeAngle < angleTolerance;

			float totalAngle = 0.0f;

			Vector3 averageNormal;
			averageNormal.Zero();
			for(deVertex::EdgeIterator ei = b->BeginEdges(); ei != b->EndEdges(); ++ei)
			{
				averageNormal.Add(ei->GetFaceNormal());
			}
			averageNormal.Normalize();

			for(deVertex::EdgeIterator ei = b->BeginEdges(); ei != b->EndEdges(); ++ei)
			{
				Vector3 mid, next, prev;
				ei->GetFaceVertPositions(mid, next, prev);
				Vector3 vecA = next - mid;
				Vector3 vecB = prev - mid;

				totalAngle += ProjectedSignedAngle(averageNormal, vecA, vecB);
			}

			if (totalAngle > PI * (1.5f) || totalAngle < - PI * (1.5f))
			{
				isConcave = true;
			}

			if (isConcave && (smallArea || narrowAngle))
			{
				bool isATriangle = (bc->GetNextAlongBoundary()->GetNextAlongBoundary() == ab);
				if (!isATriangle && c->GetPosition().IsClose(a->GetPosition(), deWedge::GetVertSnapDist()))
				{
					StitchEdge(bc, ab, true);
					edgesStitched++;
#if SAVE_EVERY_STITCH
					Save("C:\\Meshes\\stitch.dmsh");
#endif
				}
				else
				{
					if (bcVec.Mag2() > baVec.Mag2())
					{
						// bc > ba
						float t = geomTValues::FindTValueSegToPoint(b->GetPosition(), bcVec, a->GetPosition());
						t = Clamp(t, 0.05f, 0.95f);
//						demAssertf(t > 0.0f && t < 1.0f, "T value out of range [0,1]);

						ASSERT_ONLY(deVertex* newVert = ) SplitEdge(bc, t);
						demAssertf(b->FindEdge(newVert) == bc, "Splitting didn't allocate edges the way we expected");
						StitchEdge(ab, bc, true);
						edgesStitched++;
#if SAVE_EVERY_STITCH
						Save("C:\\Meshes\\stitch.dmsh");
#endif
					}
					else
					{
						// ba > bc
						float t = geomTValues::FindTValueSegToPoint(b->GetPosition(), baVec, c->GetPosition());
						t = Clamp(t, 0.05f, 0.95f);

//						demAssertf(t > 0.0f && t < 1.0f, "T value out of range [0,1]");
						ASSERT_ONLY(deVertex* newVert = ) SplitEdge(ab, 1.0f - t);
						demAssertf(a->FindEdge(newVert) == ab, "Splitting didn't allocate edges the way we expected");
						demAssertf(ab->GetNextAlongBoundary()->GetEnd() == b, "Splitting didn't allocate edges the way we expected");
						StitchEdge(ab->GetNextAlongBoundary(), bc, true);
						edgesStitched++;
#if SAVE_EVERY_STITCH
						Save("C:\\Meshes\\stitch.dmsh");
#endif
					}
				}
				// We did something to the mesh, reset the iterator
				startOver = true;
				b = bc->GetEnd();

				if (!b->IsOnBoundary()) // If we completely closed up the hole, stop
				{
					keepGoing = false;
				}
			}
			else
			{
				keepGoing = false;
			}
		}

		if (startOver)
		{
			vi = BeginVertices();
		}
		else
		{
			++vi;
		}
	}

	return edgesStitched;
}


void deMesh::DeleteVertex(deVertex* vertex) {
	MarkDrawDataDirty();

	RemoveVertexFromGrid(vertex);

#if 0
	// some quick checks to make sure vertex isn't still in use
	// (i.e. someone else is responsible for making sure topology is preserved 
	// when deleting a vertex)

	for(int i = 0; i < vertex->m_Edges.GetCount(); i++)
	{
#if __ASSERT
		deEdge* edge = vertex->m_Edges[i];
		demAssertf(!edge->GetNeighbor() || edge->GetNeighbor()->GetEnd() != vertex, "Sanity check failed. Outgoing edge's neighbor shouldn't point to a vertex we're deleting"); // if it has a neighbor, the neighbor shouldn't point here.
		demAssertf(edge->m_Prev->m_Vertex != vertex, "Sanity check failed, outgoing edge's prev. edge shouldn't point to the vertex we're deleting"); // edge prev. shouldn't point here either
#endif
	}
#endif

	// OK, the vertex seems dead enough.
//	vertex->m_Edges.Reset();
	vertex->m_Flags.Set(deVertex::FLAG_PENDING_DELETION);
}

void deMesh::CollectGarbage()
{
	RemoveUnusedMaterials();
	ReduceEdgeSets();

	// Delete all verts that have FLAG_PENDING_DELETION set
	for(size_t i = 0; i < m_Vertices.size(); i++)
	{
		if (m_Vertices[i]->m_Flags.IsSet(deVertex::FLAG_PENDING_DELETION))
		{
			deVertex* deadVert = m_Vertices[i];
			m_Vertices[i] = m_Vertices.back();
			m_Vertices.pop_back();
			delete deadVert;
			--i;
		}
	}
}

void deMesh::Save(const char* file) {
	if (m_Vertices.size() == 0) {
		demWarningf("The mesh to save to file %s would be empty", file);
		return;
	}

	CollectGarbage();

	// Set the indices for all verts, edges.

	int numEdges = 0;
	EdgeIterator ei;
	for(ei = BeginEdges(); ei != EndEdges(); ++ei) {
		ei->m_Index = numEdges++;
		demAssertf(ei->m_Wedge < ei->GetStart()->GetNumWedges(), "Invalid wedge index %d", ei->m_Wedge);
	}

	int numVerts = 0;

	float minX,maxX,minZ,maxZ;
	minX = maxX = m_Vertices[0]->GetPosition().x;
	minZ = maxZ = m_Vertices[0]->GetPosition().z;

	VertexIterator vi;
	for(vi = BeginVertices(); vi != EndVertices(); ++vi) {
		(*vi)->m_Index = numVerts;
		numVerts++;
		Vector3 v = (*vi)->GetPosition();
		minX = Min(minX, v.x);
		maxX = Max(maxX, v.x);
		minZ = Min(minZ, v.z);
		maxZ = Max(maxZ, v.z);
	}

	parSettings oldSettings = PARSER.Settings();
	PARSER.Settings().SetFlag(parSettings::WRITE_XML_HEADER, false);

	parStreamOut* stream = PARSER.OpenOutputStream(file, "dmsh", parManager::XML);

	parHeaderInfo info;
	stream->WriteHeader(info);

	demAssertf(stream, "Couldn't open file %s for writing", file);

	parElement topLevel;

	topLevel.SetName("DirectedEdgeMesh", false);
//	topLevel.SetVersion(parVersionInfo(sm_VersionNumber, 0));
	char sbuf[32];
	formatf(sbuf, 32, "%d.0", sm_VersionNumber);
	topLevel.AddAttribute("v", sbuf, false, true);
	topLevel.AddAttribute("NumEdges", numEdges, false);
	topLevel.AddAttribute("NumVerts", numVerts, false);
	topLevel.AddAttribute("MinX", minX, false);
	topLevel.AddAttribute("MaxX", maxX, false);
	topLevel.AddAttribute("MinZ", minZ, false);
	topLevel.AddAttribute("MaxZ", maxZ, false);

	stream->WriteBeginElement(topLevel);
	{
		parElement materials;
		materials.SetName("Materials", false);
		materials.AddAttribute("NumMaterials", m_Materials.GetCount(), false);
		stream->WriteBeginElement(materials);
		{
			for(int i = 0; i < m_Materials.GetCount(); i++) {
				parElement mtl;
				mtl.SetName("Material", false);
				mtl.AddAttribute("name", m_Materials[i].m_Material->Name, false, false);
				mtl.AddAttribute("priority", m_Materials[i].m_Material->Priority, false);
				mtl.AddAttribute("texsetcount", m_Materials[i].m_Material->TexSetCount, false);
				mtl.AddAttribute("tanbisetcount", m_Materials[i].m_Material->TanBiSetCount, false);

				stream->WriteBeginElement(mtl);

				PARSER.SaveObject(stream->GetFiStream(), &m_Materials[i]);

				stream->WriteEndElement(mtl);
			}
		}
		stream->WriteEndElement(materials);

		parElement verts;
		verts.SetName("Vertices", false);
		stream->WriteBeginElement(verts);
		{
			for(vi = BeginVertices(); vi != EndVertices(); ++vi) {
				(*vi)->WriteCoreData(stream);
			}
		}
		stream->WriteEndElement(verts);

		// Count verts w/ extra data
		int numExtras = 0;
		for(vi = BeginVertices(); vi != EndVertices(); ++vi) {
			if ((*vi)->HasExtraData()) {
				numExtras++;
			}
		}

		parElement vertExtra;
		vertExtra.SetName("ExtraVertData", false);
		vertExtra.AddAttribute("NumVerts", numExtras);
		stream->WriteBeginElement(vertExtra);
		{
			for(vi = BeginVertices(); vi != EndVertices(); ++vi) {
				(*vi)->WriteExtraData(stream);
			}
		}
		stream->WriteEndElement(vertExtra);

		parElement edges;
		edges.SetName("Edges", false);
		stream->WriteBeginElement(edges);
		{
			for(ei = BeginEdges(); ei != EndEdges(); ++ei) {
				ei->Write(stream);
			}
		}
		stream->WriteEndElement(edges);

	}
	stream->WriteEndElement(topLevel);

	stream->Close();

	delete stream;

	PARSER.Settings() = oldSettings;
}

void deMesh::Load(const char* file) {
	if (!m_Name)
	{
		m_Name = file;
	}
	MarkDrawDataDirty();
	int i;

	parStreamIn* stream = PARSER.OpenInputStream(file, "dmsh");
	demAssertf(stream, "Couldn't open file %s for reading", file);

	parElement topLevel;
	stream->ReadBeginElement(topLevel);

	parVersionInfo vers;
	if (topLevel.FindAttribute("v"))
	{
		vers = parVersionInfo(topLevel.FindAttribute("v")->GetStringValue());
	}

	if (vers.GetMajor() < 2 || vers.GetMajor() > (u32)sm_VersionNumber) // we can read 2.0 or newer w/ backward compatibility
	{
		demErrorf("Version number mismatch when reading a demesh file");
		stream->Close();
		return;
	}

	int numEdges = topLevel.FindAttribute("NumEdges")->FindIntValue();
	int numVerts = topLevel.FindAttribute("NumVerts")->FindIntValue();

	/*
	float minX = topLevel.FindAttribute("MinX")->FindFloatValue();
	float maxX = topLevel.FindAttribute("MaxX")->FindFloatValue();
	float minZ = topLevel.FindAttribute("MinZ")->FindFloatValue();
	float maxZ = topLevel.FindAttribute("MaxZ")->FindFloatValue();
	// Create vertex grid here?
	*/

	// Create materials
	parElement materials;
	stream->ReadBeginElement(materials);
	demAssertf(!strcmp(materials.GetName(), "Materials"), "Expected <Materials>, found <%s>", materials.GetName());
	int numMaterials = materials.FindAttribute("NumMaterials")->FindIntValue();

	m_Materials.Resize(numMaterials);
	for(i = 0; i < numMaterials; i++) {

		parElement mtl;
		stream->ReadBeginElement(mtl);
		m_Materials[i].m_Material = rage_new mshMaterial;
		m_Materials[i].m_Material->Name = mtl.FindAttribute("name")->GetStringValue();
		m_Materials[i].m_Material->Priority = mtl.FindAttribute("priority")->FindIntValue();
		m_Materials[i].m_Material->TexSetCount = mtl.FindAttribute("texsetcount")->FindIntValue();
		m_Materials[i].m_Material->TanBiSetCount = mtl.FindAttribute("tanbisetcount")->FindIntValue();

		PARSER.LoadObject(stream->GetFiStream(), m_Materials[i]);

		stream->ReadEndElement();
	}
	stream->ReadEndElement();

	PF_START(CreateEdges);

	deEdge** edges = rage_new deEdge*[numEdges];

	for(i = 0; i < numEdges; i++) {
		edges[i] = rage_new deEdge;
	}

	PF_STOP(CreateEdges);

	PF_START(LoadVerts);

	m_Vertices.clear();
	m_Vertices.resize(numVerts);

	parElement verts;
	stream->ReadBeginElement(verts);
	demAssertf(!strcmp(verts.GetName(), "Vertices"), "Expected <Vertices>, found <%s>", verts.GetName());

//	deVertex::CoreVtxData* vertdata = new deVertex::CoreVtxData[numVerts];
//	stream->ReadData(reinterpret_cast<char*>(vertdata), numVerts * sizeof(deVertex::CoreVtxData), parStream::BINARY);

	deWedge emptyWedge;

	for(i = 0; i < numVerts; i++) {
		deVertex* vert = rage_new deVertex(emptyWedge);

		deVertex::CoreVtxData vertdata;
		stream->ReadData(reinterpret_cast<char*>(&vertdata), sizeof(vertdata), parStream::BINARY);

		stream->ReadData(reinterpret_cast<char*>(&vert->m_Wedges[0].GetVertexData()), sizeof(mshVertex), parStream::BINARY);

		vert->m_Wedges[0].SetMaterial(vertdata.material);

		if (vertdata.edge != -1) {
			vert->m_Edges.Resize(1);
			vert->m_Edges[0] = edges[vertdata.edge];
		}
		// "Moving" the vertex to where it already is just makes sure it's in the right spot in the grid.
		MoveVertex(*vert, vert->GetPosition());

		reinterpret_cast<int&>(vert->m_Flags) = vertdata.flags;

		vert->m_Index = i;

		m_Vertices[i] = vert;
	}

	stream->ReadEndElement();

	PF_STOP(LoadVerts);

	parElement vertExtra;
	stream->ReadBeginElement(vertExtra);
	demAssertf(!strcmp(vertExtra.GetName(), "ExtraVertData"), "Expected <ExtraVertData>, found <%s>", vertExtra.GetName());
	{
		int numExtras = vertExtra.FindAttribute("NumVerts")->FindIntValue();
		for(i = 0; i < numExtras; i++) {
			parElement extraData;
			stream->ReadBeginElement(extraData);
			demAssertf(!strcmp(extraData.GetName(), "Vertex"), "Expected <Vertex>, found <%s>", extraData.GetName());
			int index = extraData.FindAttribute("Index")->FindIntValue();

			deVertex* vert = m_Vertices[index];

			parElement wedgesElt;
			stream->ReadBeginElement(wedgesElt);


#if 0
			if (!strcmp(edgesElt->GetName(), "Edges")) {
	            int numBasisEdges = edgesElt.FindAttribute("NumEdges")->FindIntValue();
				int* edgeList = Alloca(int, numBasisEdges);

				stream->ReadData(reinterpret_cast<char*>(edgeList), numBasisEdges * sizeof(int), parStream::INT_ARRAY);

				for(int j = 0; j < numBasisEdges; j++) {
					int newEdge = edgeList[j];
					vert->m_Edges.Grow(4) = edges[newEdge];
				}

				stream->ReadEndElement();
			}
#endif

			int numWedges = wedgesElt.FindAttribute("NumWedges")->FindIntValue();
			if (vers.GetMajor() == 2)
			{
				vert->m_Wedges.Reset();
				vert->m_Wedges.Resize(numWedges);
				// TODO: something with the material pointer and channel data
				for(int w = 0; w < numWedges; w++) {
					int material;
					stream->ReadData(reinterpret_cast<char*>(&material), sizeof(int), parStream::BINARY);
					vert->m_Wedges[w].SetMaterial(material);

					stream->ReadData(reinterpret_cast<char*>(&vert->m_Wedges[w].GetVertexData()), sizeof(mshVertex), parStream::BINARY);
				}
			}
			else
			{
				// TODO: something with the material pointer and channel data
				for(int w = 1; w < numWedges; w++) {
					int material;
					stream->ReadData(reinterpret_cast<char*>(&material), sizeof(int), parStream::BINARY);
					vert->m_Wedges.Grow(numWedges);
					vert->m_Wedges[w].SetMaterial(material);

					stream->ReadData(reinterpret_cast<char*>(&vert->m_Wedges[w].GetVertexData()), sizeof(mshVertex), parStream::BINARY);
				}
			}

			stream->ReadEndElement();

			stream->ReadEndElement();
		}
		
	}
	stream->ReadEndElement();

	PF_START(LoadEdges);

	parElement edgesElt;
	stream->ReadBeginElement(edgesElt);
	demAssertf(!strcmp(edgesElt.GetName(), "Edges"), "Expected <Edges>, found <%s>", edgesElt.GetName());

	deEdge::EdgeConnections* connections = rage_new deEdge::EdgeConnections[numEdges];
	stream->ReadData(reinterpret_cast<char*>(connections), numEdges * sizeof(deEdge::EdgeConnections), parStream::INT_ARRAY);

	for(i = 0; i < numEdges; i++) {
		parElement edgeElt;
//		stream->ReadLeafElement(edgeElt);

		int neighborIdx = connections[i].m_Neighbor;
		int prevIdx = connections[i].m_Prev;
		int vertexIdx = connections[i].m_Vertex;

		edges[i]->UnsafeSetNeighbor(neighborIdx >= 0 ? edges[neighborIdx] : NULL);
		edges[i]->m_Prev = edges[prevIdx];
		edges[i]->m_Vertex = m_Vertices[vertexIdx];
		edges[i]->m_Flags = *reinterpret_cast<atFixedBitSet<32>*>(&connections[i].m_Flags);
		edges[i]->m_Wedge = connections[i].m_Wedge;
	}

	stream->ReadEndElement();

	for(i = 0; i < numEdges; i++) {
		demAssertf(edges[i]->m_Wedge < edges[i]->GetStart()->GetNumWedges(), "Wedge index %d is out of range", edges[i]->m_Wedge);
	}

	delete [] connections;

	delete [] edges;

	PF_STOP(LoadEdges);

#if __STATS
	Printf("Loaded: create = %f, verts = %f, edges = %f\n", PF_READ_TIME(CreateEdges) * 1000.0f, PF_READ_TIME(LoadVerts) * 1000.0f, PF_READ_TIME(LoadEdges) * 1000.0f);
#endif

	stream->Close();

	delete stream;
}

void deMesh::GetMshMesh(mshMesh * mesh, bool tristrip)
{
	// Remove unused materials
	RemoveUnusedMaterials();

	// foreach m in material

	// foreach v in vertices
	//  if v uses m
	//    add v to m's vert list
	//    set v's index to m.verts.GetCount()
	// foreach f in faces
	//  if f uses m
	//    add three verts to m.prims, using v->index for indices

	// may want to check that a material is used before adding it to the mshMesh?

	atArray<int> vertCountPerMaterial;
	atArray<int> faceCountPerMaterial;

	vertCountPerMaterial.Resize(m_Materials.GetCount());
	faceCountPerMaterial.Resize(m_Materials.GetCount());
	for(int i = 0; i < m_Materials.GetCount(); i++) {
		vertCountPerMaterial[i] = 0;
		faceCountPerMaterial[i] = 0;
	}

	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
		for(int wedge = 0; wedge < (*vi)->GetNumWedges(); wedge++) {
			int mtlIdx = (*vi)->GetWedge(wedge).GetMaterial();
			(*vi)->GetWedge(wedge).SetIndex(-1); // clear out the index, just to make sure bad indices don't show up later
			vertCountPerMaterial[mtlIdx]++;
		}
	}

	FaceEdgeIterator fiEnd = EndFaceEdges();
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != fiEnd; ++fi) {
		deEdge* e[3];
		fi->GetFaceEdges(e[0], e[1], e[2]);
		// Verify that all edges around the face share the same material
		demAssertf(e[0]->GetStartWedge().GetMaterial() == e[1]->GetStartWedge().GetMaterial(), "Material mismatch within face");
		demAssertf(e[1]->GetStartWedge().GetMaterial() == e[2]->GetStartWedge().GetMaterial(), "Material mismatch within face");
		demAssertf(e[2]->GetStartWedge().GetMaterial() == e[0]->GetStartWedge().GetMaterial(), "Material mismatch within face");
		int mtlIdx = e[0]->GetStartWedge().GetMaterial();
		faceCountPerMaterial[mtlIdx]++;
	}

	for(int mtlIdx = 0; mtlIdx < m_Materials.GetCount(); mtlIdx++) {
		mshMaterial* material = rage_new mshMaterial;
		char mtlName[32];
		formatf(mtlName, 32, "#%d", mtlIdx);
		material->Name = mtlName;

		material->SetVertexCount(vertCountPerMaterial[mtlIdx]);
		material->Prim.Resize(1);
		material->Prim[0].Type = mshTRIANGLES;
		material->Prim[0].Idx.Resize(faceCountPerMaterial[mtlIdx] * 3);
		material->Prim[0].Priority=0;  // Not initializing the priority can cause crashes when you run  mshMaterial::Triangulate(); If the uninit value is -1

		int usedWedges = 0;
		for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
			for(int wedge =0; wedge < (*vi)->GetNumWedges(); wedge++) {
				deWedge& theWedge = (*vi)->GetWedge(wedge);
				if (theWedge.GetMaterial() == mtlIdx) {
					theWedge.SetIndex(usedWedges);
					material->SetVertex(usedWedges, theWedge.GetVertexData()); // TODO: Handle channel data
					usedWedges++;
				}
			}
		}

		int faceNum = 0;
		for(FaceEdgeIterator fi = BeginFaceEdges(); fi != fiEnd; ++fi) {
			deEdge* e[3];
			fi->GetFaceEdges(e[0], e[1], e[2]);
			if (e[0]->GetStartWedge().GetMaterial() == mtlIdx)
			{
				material->Prim[0].Idx[faceNum * 3 + 0] = e[0]->GetStartWedge().GetIndex();
				material->Prim[0].Idx[faceNum * 3 + 1] = e[1]->GetStartWedge().GetIndex();
				material->Prim[0].Idx[faceNum * 3 + 2] = e[2]->GetStartWedge().GetIndex();
				faceNum++;
			}
		}

		mesh->AddMtl(*material);
		delete material;
	}

	mesh->CleanModel();
	if (tristrip)
	{
		mesh->Tristrip();
	}
	mesh->ReorderVertices();
}

void deMesh::SaveMshMesh(const char* filename, bool tristrip, bool textFormat)
{
	// may want to check that a material is used before adding it to the mshMesh?

	mshMesh* mesh = rage_new mshMesh;

	GetMshMesh(mesh, tristrip);

    fiSafeStream stream(ASSET.Create(filename, "mesh"));
    if( textFormat ) 
    {
		stream->PutCh(13);
		stream->PutCh(10);
		fiAsciiTokenizer tok;
		mshSerializer serializer(tok,true);
		tok.Init(filename,stream);
		mesh->Serialize(serializer);
	}
	else
	{
		stream->PutCh(26); // i'm binary
		fiBinTokenizer tok;
		tok.Init("Mesh", stream);
		mshSerializer serializer(tok, true);
		mesh->Serialize(serializer);
	}


	delete mesh;
}


void deMesh::SaveEntity(const char* /* dirname */, const char * typefilename, const char * meshfilename, const char *presetBasename, bool tristrip )
{
	//demAssertf(dirname, "Directory name is required");
	// Saves three things for the mesh:
	// 1: One mesh file containing N materials
	// 2: An entity.type file containing
	//		a: a reference to each shader preset and shader variable combination
	//		b: a reference to the mesh file
	// 3: One shader variable (sva) file for each material

	// Write entity.type file
	//char filename[256];
	//char meshname[256];
	//	formatf(filename, 256, "%s/demesh_entity.type", dirname);	// (cmc) changed this so we don't overwrite the entity.type exported by makeworld.
	// TODO: Maybe take another parameter to specify the output filename of the .type file?????(hack:)
	//formatf(filename, sizeof(filename), typefilename );// "demesh_entity.type");

	// Write mesh file to themesh.mesh
	//formatf(filename, 256, "%s/themesh.mesh", dirname);
	//formatf( meshname, sizeof(filename), meshfilename );


	//ASSET.CreateLeadingPath(filename);

	RemoveUnusedMaterials();

	// It's possible when creating the mshMesh that it finds some unused materials 
	// (for example the material contained only degenerate triangles and was removed)
	// So find out which materials actually made it through, and only write those to the 
	// output .type file.

	mshMesh* mesh = rage_new mshMesh;

	GetMshMesh(mesh, tristrip);


	atArray<MaterialData*> usedMaterials;
	for(int newIdx = 0; newIdx < mesh->GetMtlCount(); newIdx++)
	{
		// All the materials are named #N from GetMshMesh, so figure out which #Ns are used
		// and rename the materials in the mshMesh
		const char* oldMtlName = mesh->GetMtl(newIdx).Name;
		demAssertf(oldMtlName && oldMtlName[0] == '#', "Invalid format for material name: %s", oldMtlName);

		char* crap;
		int oldMtlIndex = strtol(oldMtlName + 1, &crap, 0);
		usedMaterials.PushAndGrow(&m_Materials[oldMtlIndex]);

		// need to re-index this mtl name
		char newMtlName[32];
		formatf(newMtlName, "#%d", usedMaterials.GetCount()-1);
		mesh->GetMtl(newIdx).Name = newMtlName;
	}

	fiSafeStream meshstream(ASSET.Create(meshfilename, "mesh"));
	meshstream->PutCh(26); // i'm binary
	fiBinTokenizer tok;
	tok.Init("Mesh", meshstream);
	mshSerializer serializer(tok, true);
	mesh->Serialize(serializer);

	delete mesh;

	fiSafeStream stream(ASSET.Create( typefilename , "type"));

	if (stream) {
		fprintf(stream, "Version: 103\r\n");

		fprintf(stream, "shadinggroup {\r\n");
		fprintf(stream, "    shadinggroup {\r\n");
		fprintf(stream, "        Count 1\r\n");
		fprintf(stream, "        Shaders %d {\r\n", usedMaterials.GetCount());
		if (usedMaterials.GetCount() > 0 && usedMaterials[0]->m_ShaderPreset != ConstString())
		{
			fprintf(stream, "            Vers: 1\r\n");


			// fixme: preserve original .sva filenames
			for(int i = 0; i < usedMaterials.GetCount(); i++) {
				fprintf(stream, "            %s 1 %s_shader_preset_%03d.sva\r\n", usedMaterials[i]->m_ShaderPreset.m_String, presetBasename, i);
			}
		}
		else
		{
			fprintf(stream,"            Vers: 2\r\n");

			for(int i = 0; i < usedMaterials.GetCount(); i++) {
				fprintf(stream, "            %s\r\n", usedMaterials[i]->m_MtlFileName.m_String);
				fprintf(stream, "            %s\r\n", usedMaterials[i]->m_MtlTemplate.m_String);
				fprintf(stream, "            ");
				fputs(usedMaterials[i]->m_MtlVariables.m_String, stream);
				if (i+1 < usedMaterials.GetCount()) // doing this so we dont get empty lines
				{
					fprintf(stream, "\r\n");
				}
			}

		}
		fprintf(stream, "        }\r\n");
		fprintf(stream, "    }\r\n");
		fprintf(stream, "}\r\n");

		float lodSwitchRange = 9999.0f; // TODO: See if this needs to be higher

		Vector3 min, max;
		FindBounds(min, max);

		Vector3 center;
		center.Average(min, max);

		float radius = center.Dist(min);

		// It might be possible to do something clever with the LOD groups here
		// to have automatic switching between reduced LODs
		fprintf(stream, "lodgroup {\r\n");
		fprintf(stream, "    mesh {\r\n");
		fprintf(stream, "        high 1\r\n");
		fprintf(stream, "            %s 0\r\n", meshfilename );
		fprintf(stream, "            %f\r\n", lodSwitchRange);
		fprintf(stream, "        med none %f\r\n", lodSwitchRange);
		fprintf(stream, "        low none %f\r\n", lodSwitchRange);
		fprintf(stream, "        vlow none %f\r\n", lodSwitchRange);
		fprintf(stream, "        center %f %f %f\r\n", center.x, center.y, center.z);
		fprintf(stream, "        radius %f\r\n", radius);
		fprintf(stream, "        box %f %f %f %f %f %f\r\n", min.x, min.y, min.z, max.x, max.y, max.z);
		fprintf(stream, "    }\r\n");
		fprintf(stream, "}\r\n");
	}

	// Write shader variable (sva) files:
	// fixme: preserve original .sva filenames
	for(int i = 0; i < usedMaterials.GetCount(); i++)
	{
		if (usedMaterials[i]->m_ShaderVariables != ConstString())
		{
			char filename[256];
			//		formatf(filename, sizeof(filename), "%s/shader_preset_%03d.sva", dirname, i);
			formatf(filename, sizeof(filename), "%s_shader_preset_%03d.sva", presetBasename, i);

			fiSafeStream svaStream(ASSET.Create(filename, "sva"));

			if (svaStream)
			{
				svaStream->Write(usedMaterials[i]->m_ShaderVariables, (int) strlen(usedMaterials[i]->m_ShaderVariables));
			}
		}
	}

}


void deMesh::FlipWinding()
{
	MarkDrawDataDirty();
	// need to make a list of all faces, since we can't use the iterator if we're modifying the faces
	std::vector<deEdge*> faces;
	FaceEdgeIterator lastFace = EndFaceEdges();
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != lastFace; ++fi)
	{
		faces.push_back(&(*fi));
	}

	for(std::vector<deEdge*>::iterator face_iter = faces.begin(); face_iter != faces.end(); ++face_iter) {
		(*face_iter)->FlipFaceWinding();
	}

	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi) {
		(*vi)->ReduceEdgeSet(); // fix up outgoing edges
	}
}

void deMesh::FindApproxBounds(float& minX, float& maxX, float& minZ, float& maxZ)
{
	if (m_VertexGrid) {
		minX = m_VertexGrid->GetMinGridX();
		maxX = m_VertexGrid->GetMaxGridX();
		minZ = m_VertexGrid->GetMinGridY();
		maxZ = m_VertexGrid->GetMaxGridY();
	}
	else {
		FindBounds(minX, maxX, minZ, maxZ);
	}
}

void deMesh::FindBounds(float& minX, float& maxX, float& minZ, float& maxZ)
{
	Vector3 min, max;

	FindBounds(min, max);
	minX = min.x;
	maxX = max.x;
	minZ = min.y;
	maxZ = max.y;
}

void deMesh::FindBounds(Vector3& outMin, Vector3& outMax)
{
	VertexIterator vi = BeginVertices();
	if (vi == EndVertices())
	{
		outMin.Set(FLT_MAX);
		outMax.Set(-FLT_MAX);
		return;
	}
	outMin = (*vi)->GetPosition();
	outMax = (*vi)->GetPosition();
	++vi;

	for(; vi != EndVertices(); ++vi) {
		Vector3 pos = (*vi)->GetPosition();
		outMin.Min(outMin, pos);
		outMax.Max(outMax, pos);
	}
}

bool deMesh::ClassifyFaceForSplit(deEdge* e)
{
	if (e->GetEnd()->IsRight() || e->GetNext()->GetEnd()->IsRight()) {
		return false;
	}
	return true;
}

deMesh* deMesh::Partition()
{
	MarkDrawDataDirty();
	deMesh* leftMesh = rage_new deMesh("Left_of_partition");
	leftMesh->m_Materials.Resize(m_Materials.GetCount()); // TODO: Only copy over materials in use
	for(int i = 0; i < m_Materials.GetCount(); i++) {
		leftMesh->m_Materials[i] = m_Materials[i];
	}

	VertexIterator vi = BeginVertices();
	while(vi != EndVertices())
	{
		if ((*vi)->IsLeft()) {
			deVertex* vert = *vi;

			RemoveVertexFromGrid(vert);
			(*vi) = m_Vertices.back();
			m_Vertices.pop_back();

			leftMesh->m_Vertices.push_back(vert);
			leftMesh->AddVertexToGrid(vert);

			// leave vi alone. Already points to the next element to check
		}
		else {
			++vi;
		}
	}

	return leftMesh;
}

deMesh* deMesh::CreateFromFile(const char* filename)
{
	if (strstr(filename, ".mesh")) {
		mshMesh* mesh = rage_new mshMesh;
		if ( SerializeFromFile(filename, *mesh, "mesh") == false ) 
		{
			demErrorf("Can't load mesh file %s", filename);
			delete mesh;
			return NULL;
		}
		deMesh* ret = deMesh::CreateFromMesh(mesh);
		ret->m_Name = filename;
		delete mesh;
		return ret;
	}
	else if (strstr(filename, ".type")) {
		return deMesh::CreateFromEntity(filename);
	}
	else {
		deMesh* ret = rage_new deMesh(filename);
		ret->Load(filename);
		return ret;
	}
}

void deMesh::OffsetMesh(const Vector3& offset)
{
	MarkDrawDataDirty();
	bool hasGrid = (m_VertexGrid != NULL);
	DestroyGrid();

	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi)
	{
		(*vi)->SetPosition((*vi)->GetPosition() + offset);
	}

	if (hasGrid) {
		float minX, maxX, minY, maxY;
		FindApproxBounds(minX, maxX, minY, maxY);
		InitGrid(minX, maxX, minY, maxY, 100, 100);
	}
}

void deMesh::ScaleMesh(const Vector3& scale)
{
	MarkDrawDataDirty();
	bool hasGrid = (m_VertexGrid != NULL);
	DestroyGrid();

	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi)
	{
		Vector3 newPos;
		newPos.Multiply((*vi)->GetPosition(), scale);
		(*vi)->SetPosition(newPos);
	}

	if (hasGrid) {
		float minX, maxX, minY, maxY;
		FindApproxBounds(minX, maxX, minY, maxY);
		InitGrid(minX, maxX, minY, maxY, 100, 100);
	}
}

void deMesh::MarkProcessedVerts(int bitToSet, bool clearExisting)
{
	for(VertexIterator vi = BeginVertices(); vi != EndVertices(); ++vi)
	{
		if ((*vi)->m_Flags.IsSet(deVertex::FLAG_PROCESSED)) {
			(*vi)->m_Flags.Set(bitToSet);
		}
		else if (clearExisting)
		{
			(*vi)->m_Flags.Clear(bitToSet);
		}
	}
}

bool deMesh::MaterialData::operator==(const MaterialData& other) {
	demAssertf(m_Material && other.m_Material, "Both materials are required for comparison");

	if (!PARAM_nomshmtlcheck.Get())
	{
		if(!(*m_Material == *other.m_Material)) {
			return false;
		}
	}
	if (!(m_ShaderPreset == other.m_ShaderPreset)) {
		return false;
	}
	if (!(m_ShaderVariables == other.m_ShaderVariables)) {
		return false;
	}
	if (!(m_MtlFileName == other.m_MtlFileName)) {
		return false;
	}
	if (!(m_MtlTemplate == other.m_MtlTemplate)) {
		return false;
	}
	if (!(m_MtlVariables == other.m_MtlVariables)) {
		return false;
	}
	return true;
}

void deMesh::GatherDrawData()
{
	if (!m_DrawData.m_Dirty)
	{
		return;
	}

	m_DrawData.m_Dirty = false;
	m_DrawData.m_TotalEdges = 0;
	m_DrawData.m_StartSharpEdges = 0;
	m_DrawData.m_BoundaryEdges = 0;
	m_DrawData.m_TotalFaces = 0;

	for(EdgeIterator ei = BeginEdges(); ei != EndEdges(); ++ei)
	{
		++m_DrawData.m_TotalEdges;
		if (ei->IsOnBoundary())
		{
			++m_DrawData.m_BoundaryEdges;
		}
		else if (ei->IsStartSharp())
		{
			++m_DrawData.m_StartSharpEdges;
		}
	}

	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != EndFaceEdges(); ++fi)
	{
		++m_DrawData.m_TotalFaces;
	}
}

deEdge* deMesh::PickNearest(Vector3::Param origin, Vector3::Param direction, float* t /* = NULL */)
{
	float minT = FLT_MAX;
	deEdge* minEdge = NULL;
	for(FaceEdgeIterator fi = BeginFaceEdges(); fi != EndFaceEdges(); ++fi)
	{
		float t;
		Vector3 v1, v2, v3;
		fi->GetFaceVertPositions(v1, v2, v3);
		if (geomSegments::CollideRayTriangle(v1, v2, v3, origin, direction, &t))
		{
			if (t > 0.0f && t < minT)
			{
				minT = t;

				// We found a colliding face, now pick the edge whose start vtx is closes to the ray
				deEdge* edge1 = &(*fi);
				deEdge* edge2 = fi->GetNext();
				deEdge* edge3 = fi->GetPrev();
				
				float dist1 = geomDistances::Distance2LineToPoint(origin, direction, edge1->GetStart()->GetPosition());
				float dist2 = geomDistances::Distance2LineToPoint(origin, direction, edge2->GetStart()->GetPosition());
				float dist3 = geomDistances::Distance2LineToPoint(origin, direction, edge3->GetStart()->GetPosition());

				if (dist1 < dist2)
				{
					if (dist1 < dist3)
					{
						minEdge = edge1;
					}
					else
					{
						minEdge = edge3;
					}
				}
				else
				{
					if (dist2 < dist3)
					{
						minEdge = edge2;
					}
					else
					{
						minEdge = edge3;
					}
				}
			}
		}
	}
	if (t)
	{
		*t = minT;
	}
	return minEdge;
}

inline void deMesh::DebugDrawEdge( deEdge* edge, Color32 edgeColor )
{
	Vector3 start, end, perp;
	float len;
	edge->GetDebugDrawVerts(start, end, perp, len);

	start.AddScaled(perp, sm_DebugDrawNeighborSeperation);
	end.AddScaled(perp, sm_DebugDrawNeighborSeperation);

	Vector3 diff = (end - start);

	Vector3 arrow(start);
	arrow.AddScaled(perp, 2.0f * sm_DebugDrawArrowScale);
	arrow.AddScaled(diff, 0.9f * sm_DebugDrawArrowScale);

	grcColor(edgeColor);

	int edgeVerts = 6;
	if (edge->GetNeighbor())
	{
		edgeVerts += 2;
	}

	grcBegin(drawLines, edgeVerts);
	grcVertex3f(start);
	grcVertex3f(end);
	grcVertex3f(end);
	grcVertex3f(arrow);

	// Draw neighbor pointer (if any)
	if (edge->GetNeighbor()) {
		Vector3 ns, ne, np;
		float nl;
		edge->GetNeighbor()->GetDebugDrawVerts(ns, ne, np, nl);
		ns.AddScaled(np, sm_DebugDrawNeighborSeperation);
		ne.AddScaled(np, sm_DebugDrawNeighborSeperation);

		Vector3 neighStart(start);
		neighStart.AddScaled(diff, (1.0f - sm_DebugDrawPointerScale) * 1.05f);
		Vector3 neighEnd(ns);
		neighEnd.AddScaled(ne - ns, sm_DebugDrawPointerScale);

		grcColor3f(0.7f, 0.0f, 0.0f);
		grcVertex3f(neighStart);
		grcVertex3f(neighEnd);
	}

	// Draw prev pointer
	Vector3 ps, pe, pp;
	float pl;
	edge->GetPrev()->GetDebugDrawVerts(ps, pe, pp, pl);
	ps.AddScaled(pp, sm_DebugDrawNeighborSeperation);
	pe.AddScaled(pp, sm_DebugDrawNeighborSeperation);

	Vector3 prevStart(start);
	prevStart.AddScaled(diff, sm_DebugDrawPointerScale);
	Vector3 prevEnd(ps);
	prevEnd.AddScaled(pe - ps, (1.0f - sm_DebugDrawPointerScale) * 1.05f);

	grcColor3f(0.0f, 0.7f, 0.0f);
	grcVertex3f(prevStart);
	grcVertex3f(prevEnd);

	grcEnd();
}

void deMesh::OpenErrorStream(const char* filename)
{
	sm_ErrorStream = PARSER.OpenOutputStream(filename, "dmerr");
	if (sm_ErrorStream)
	{
		parHeaderInfo info;
		sm_ErrorStream->WriteHeader(info);

		parElement root("rage__deMeshErrorList", false);
		root.AddAttribute("v", "1.0", false, false);
		sm_ErrorStream->WriteBeginElement(root);

		parElement rageRel("RageRel", false);
		rageRel.AddAttribute("value", RAGE_RELEASE, false);
		sm_ErrorStream->WriteLeafElement(rageRel);

		parElement demeshVers("DemeshVersion", false);
		demeshVers.AddAttribute("value", sm_VersionNumber, false);
		sm_ErrorStream->WriteLeafElement(demeshVers);

		parElement errorList("Errors", false);
		sm_ErrorStream->WriteBeginElement(errorList);
	}
}

void deMesh::CloseErrorStream()
{
	if (sm_ErrorStream)
	{
		parElement errorList("Errors", false);
		sm_ErrorStream->WriteEndElement(errorList);

		parElement root("DemeshErrors", false);
		sm_ErrorStream->WriteEndElement(root);

		sm_ErrorStream->Close();
		delete sm_ErrorStream;
	}
	sm_ErrorStream = NULL;
}

void deMesh::ReportError(const deMeshError& error)
{
	if (!sm_ErrorStream)
	{
		return;
	}
	PARSER.SaveObject(sm_ErrorStream, &error, parManager::UNKNOWN, "Item");
}

rage::deMeshError::deMeshError( const deMesh* mesh, const char* desc ) : m_MeshName(mesh?mesh->m_Name:"unknown"), m_Description(desc)
{

}

rage::deMeshErrorPoint::deMeshErrorPoint( const deMesh* mesh, const char* desc, const deVertex* vtx ) : deMeshError(mesh, desc)
{
	m_Point = vtx->GetPosition();
}

rage::deMeshErrorPoint::deMeshErrorPoint( const deMesh* mesh, const char* desc, Vector3::Param point ) 
: deMeshError(mesh, desc)
, m_Point(point)
{
}

rage::deMeshErrorEdge::deMeshErrorEdge( const deMesh* mesh, const char* desc, const deEdge* edge ) : deMeshError(mesh, desc)
{
	demAssertf(edge, "Can't error about a NULL edge");
	m_EdgeBegin = edge->GetStart()->GetPosition();
	m_EdgeEnd = edge->GetEnd()->GetPosition();
}

rage::deMeshErrorEdge::deMeshErrorEdge( const deMesh* mesh, const char* desc, Vector3::Param pointA, Vector3::Param pointB ) 
: deMeshError(mesh, desc)
, m_EdgeBegin(pointA)
, m_EdgeEnd(pointB)
{
}

rage::deMeshErrorFace::deMeshErrorFace( const deMesh* mesh, const char* desc, const deEdge* edge ) : deMeshError(mesh, desc)
{
	demAssertf(edge, "Can't error about a NULL face");
	edge->GetFaceVertPositions(m_PointA, m_PointB, m_PointC);
}

rage::deMeshErrorFace::deMeshErrorFace( const deMesh* mesh, const char* desc, Vector3::Param pointA, Vector3::Param pointB, Vector3::Param pointC) 
: deMeshError(mesh, desc)
, m_PointA(pointA)
, m_PointB(pointB)
, m_PointC(pointC)
{
}

#endif // MESH_LIBRARY