// 
// demesh/deconfig.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef DEMESH_DECONFIG_H 
#define DEMESH_DECONFIG_H 

#include "diag/channel.h"

// DOM-IGNORE-BEGIN
RAGE_DECLARE_CHANNEL(Demesh)
#define demAssertf(cond,fmt,...)			RAGE_ASSERTF(Demesh,cond,fmt,##__VA_ARGS__)
#define demVerifyf(cond,fmt,...)			RAGE_VERIFYF(Demesh,cond,fmt,##__VA_ARGS__)
#define demErrorf(fmt,...)					RAGE_ERRORF(Demesh,fmt,##__VA_ARGS__)
#define demWarningf(fmt,...)				RAGE_WARNINGF(Demesh,fmt,##__VA_ARGS__)
#define demDisplayf(fmt,...)				RAGE_DISPLAYF(Demesh,fmt,##__VA_ARGS__)
#define demDebugf1(fmt,...)					RAGE_DEBUGF1(Demesh,fmt,##__VA_ARGS__)
#define demDebugf2(fmt,...)					RAGE_DEBUGF2(Demesh,fmt,##__VA_ARGS__)
#define demDebugf3(fmt,...)					RAGE_DEBUGF3(Demesh,fmt,##__VA_ARGS__)
#define demLogf(severity,fmt,...)			RAGE_LOGF(Demesh,severity,fmt,##__VA_ARGS__)
#define demCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,Demesh,severity,fmt,##__VA_ARGS__)
// DOM-IGNORE-END

#endif // DEMESH_DECONFIG_H 
