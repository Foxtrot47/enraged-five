
#include "math.h"
#include "vector/vector3.h"
#include "mesh/mesh.h"
#include "triintersect.h"
#include "kdtree.h"
#include "raymemory.h"

namespace rage 
{

MManager::MManager() : m_OList( 0 )
{
	// build a 32-byte aligned array of KdTreeNodes
	m_KdArray = (char*)(rage_new KdTreeNode[1000000]);
	m_ObjArray = (char*)(rage_new ObjectList[100000]);
	unsigned long addr = (unsigned long)m_KdArray;
	m_KdPtr = (KdTreeNode*)((addr + 32) & (0xffffffff - 31));
	addr = (unsigned long)m_ObjArray;
	m_ObjPtr = (ObjectList*)((addr + 32) & (0xffffffff - 31));
	ObjectList* ptr = m_ObjPtr;
	for ( int i = 0; i < 99995; i++ ) 
	{
		ptr->SetNext( ptr + 1 );
		ptr++;
	}
	ptr->SetNext( 0 );
	m_OList = m_ObjPtr;
}

ObjectList* MManager::NewObjectList()
{
	ObjectList* retval;
	retval = m_OList;
	m_OList = m_OList->GetNext();
	retval->SetNext( 0 );
	retval->SetPrimitive( 0 );
	return retval;
}

void MManager::FreeObjectList( ObjectList* list )
{
	ObjectList* lst = list;
	while (lst->GetNext()) lst = lst->GetNext();
	lst->SetNext( m_OList );
	m_OList = list;
}

KdTreeNode* MManager::NewKdTreeNodePair()
{ 
	unsigned long* tmp = (unsigned long*)m_KdPtr;
	tmp[1] = tmp[3] = 6;
	KdTreeNode* node = m_KdPtr;
	m_KdPtr += 2;
	return node;
}

};
