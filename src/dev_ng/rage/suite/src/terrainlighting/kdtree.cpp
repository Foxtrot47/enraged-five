#include "math.h"
#include "vector/vector3.h"
#include "vector/geometry.h"
#include "mesh/mesh.h"
#include "triintersect.h"
#include "rayMemory.h"
#include "kdtree.h"


namespace rage
{


MManager* KdTree::s_MManager = 0;

#define MAXTREEDEPTH		30

class Aabb
{
public:
	Aabb() : m_Pos( Vector3( 0, 0, 0 ) ), m_Size( Vector3( 0, 0, 0 ) ) 
	{}

	Aabb( const Vector3& a_Pos, const Vector3& a_Size ) : m_Pos( a_Pos ), m_Size( a_Size ) 
	{}

	Vector3& GetPos()  { return m_Pos; }
	Vector3& GetSize() { return m_Size; }
	bool Intersect( Aabb& b2 )
	{
		Vector3 v1 = b2.GetPos(), v2 = b2.GetPos() + b2.GetSize();
		Vector3 v3 = m_Pos, v4 = m_Pos + m_Size;
		return ((v4.x >= v1.x) && (v3.x <= v2.x) && // x-axis overlap
			(v4.y >= v1.y) && (v3.y <= v2.y) && // y-axis overlap
			(v4.z >= v1.z) && (v3.z <= v2.z));   // z-axis overlap
	}
	bool Contains( const Vector3& a_Pos )
	{
		Vector3 v1 = m_Pos, v2 = m_Pos + m_Size;
		return ((a_Pos.x >= v1.x) && (a_Pos.x <= v2.x) &&
			(a_Pos.y >= v1.y) && (a_Pos.y <= v2.y) &&
			(a_Pos.z >= v1.z) && (a_Pos.z <= v2.z));
	}
	float w() { return m_Size.x; }
	float h() { return m_Size.y; }
	float d() { return m_Size.z; }
	float x() { return m_Pos.x; }
	float y() { return m_Pos.y; }
	float z() { return m_Pos.z; }
private:
	Vector3 m_Pos, m_Size;
};

// -----------------------------------------------------------
// KdTree class implementation
// -----------------------------------------------------------
KdTree::KdTree()
{
	m_Root = rage_new KdTreeNode();
}
KdTree::~KdTree()
{
	delete m_Root;
}
int KdTree::AddPrimitives( mshMesh& mesh , Vector3& boxMin, Vector3& boxMax )
{
	Vector3 bSize = mesh.GetBoundBoxSize();
	boxMin = mesh.GetBoundBoxMatrix().d;
	boxMax = boxMin + bSize;

	int cnt = 0;
	for( int i = 0; i < mesh.GetMtlCount(); i++ )
	{
		mshMaterial& mtl = mesh.GetMtl( i );

		for ( mshMaterial::TriangleIterator itor = mtl.BeginTriangles();
			itor !=  mtl.EndTriangles();
			++itor )
		{
			int a;
			int b;
			int c;
			itor.GetVertIndices( a, b, c );

			mshVertex v1 = mtl.GetVertex( a);
			mshVertex v2 = mtl.GetVertex( b);
			mshVertex v3 = mtl.GetVertex( c);

			TriInfo*			info = rage_new TriInfo( v1, v2, v3 );
			TriIntersect*	tri = rage_new TriIntersect( info );

			m_Root->Add( tri );
			cnt++;
		}
	}
	return cnt;
}

void KdTree::Build( mshMesh* mesh )
{
	Vector3 boxMin;
	Vector3 boxMax;

	int prims = AddPrimitives( *mesh , boxMin, boxMax );
	
	// may move this list out?
	int splitCount = prims * 2 + 8;
	m_SPool = rage_new SplitList[ splitCount ];
	for ( int i = 0; i < (splitCount - 2); i++ ) 
	{
		m_SPool[i].next = &m_SPool[i + 1];
	}
	m_SPool[ splitCount -2 ].next = 0;


	m_SList = 0;
	Aabb		bound(boxMin, boxMax - boxMin);

	Subdivide( m_Root, bound , 0, prims );
}

void KdTree::InsertSplitPos( float splitPos )
{
	// insert a split position candidate in the list if unique
	SplitList* entry = m_SPool;
	m_SPool = m_SPool->next;
	entry->next = 0;
	entry->splitpos = splitPos;
	entry->n1count = 0;
	entry->n2count = 0;
	if (!m_SList) m_SList = entry; else
	{
		if (splitPos < m_SList->splitpos)
		{
			entry->next = m_SList;
			m_SList = entry;
		}
		else if (splitPos == m_SList->splitpos)
		{
			entry->next = m_SPool; // redundant; recycle
			m_SPool = entry;
		}
		else
		{
			SplitList* list = m_SList;
			while ((list->next) && (splitPos >= list->next->splitpos)) 
			{
				if (splitPos == list->next->splitpos)
				{
					entry->next = m_SPool; // redundant; recycle
					m_SPool = entry;
					return;
				}
				list = list->next;
			}
			entry->next = list->next;
			list->next = entry;
		}
	}
}


void KdTree::Subdivide( KdTreeNode* node, Aabb& box, int depth, int prims )
{
	// recycle used split list nodes
	if (m_SList)
	{
		SplitList* list = m_SList;
		while (list->next) list = list->next;
		list->next = m_SPool;
		m_SPool = m_SList, m_SList = 0;
	}
	// determine split axis
	Vector3 s = box.GetSize();
	if ((s.x >= s.y) && (s.x >= s.z)) node->SetAxis( 0 );
	else if ((s.y >= s.x) && (s.y >= s.z)) node->SetAxis( 1 );
	int axis = node->GetAxis();

	// make a list of the split position candidates
	ObjectList* l = node->GetList();
	float p1;
	
	float pos1 = box.GetPos()[axis];
	float pos2 = box.GetPos()[axis] +  box.GetSize()[axis];

	bool* pright = rage_new bool[ prims];
	float* eleft = new float[ prims], *eright = rage_new float[ prims];

	Primitive** parray = rage_new Primitive*[ prims];
	int aidx = 0;
	while (l)
	{
		Primitive* p = parray[aidx] = l->GetPrimitive();
		pright[aidx] = true;
		p->CalculateRange( eleft[aidx], eright[aidx], axis );
		aidx++;
	
		for ( int i = 0; i < 3; i++ )
		{
			p1 = p->GetVertex( i ).Pos[axis];
			if ((p1 >= pos1) && (p1 <= pos2)) InsertSplitPos( p1 );
		}
		l = l->GetNext();
	}

	// determine n1count / n2count for each split position
	Aabb b1, b2, b3 = box, b4 = box;
	SplitList* splist = m_SList;
	float b3p1 = b3.GetPos()[axis];
	float b4p2 = b4.GetPos()[axis] + b4.GetSize()[axis];
	while (splist)
	{
		b4.GetPos()[axis] = splist->splitpos;
		b4.GetSize()[axis] = pos2 - splist->splitpos;
		b3.GetSize()[axis] = splist->splitpos - pos1;
		float b3p2 = b3.GetPos()[axis] + b3.GetSize()[axis];
		float b4p1 = b4.GetPos()[axis];
		for ( int i = 0; i < prims; i++ ) 
		{
			if (pright[i])
			{
				Primitive* p = parray[i];
				if ((eleft[i] <= b3p2) && (eright[i] >= b3p1))
				{
					if (p->IntersectBox( b3.GetPos(), b3.GetPos() + b3.GetSize()  )) 
					{
						splist->n1count++;
					}
				}
				if ((eleft[i] <= b4p2) && (eright[i] >= b4p1))
				{
					if (p->IntersectBox( b3.GetPos(), b3.GetPos() + b3.GetSize()  )) 
					{
						splist->n2count++; 
					}
					else 
					{
						pright[i] = false;
					}
				}
			}
			else splist->n1count++;
		}
		splist = splist->next;
	}
	delete pright;
	// calculate surface area for current node
	float SAV = 0.5f / ( box.w() * box.d() + box.w() * box.h() + box.d() * box.h());
	// calculate cost for not splitting
	float Cleaf = prims * 1.0f;
	// determine optimal split plane position
	splist = m_SList;
	float lowcost = 10000;
	float bestpos = 0;
	while (splist)
	{
		// calculate child node extends
		b4.GetPos()[axis] = splist->splitpos;
		b4.GetSize()[axis] = pos2 - splist->splitpos;
		b3.GetSize()[axis] = splist->splitpos - pos1;
		// calculate child node cost
		float SA1 = 2 * (b3.w() * b3.d() + b3.w() * b3.h() + b3.d() * b3.h());
		float SA2 = 2 * (b4.w() * b4.d() + b4.w() * b4.h() + b4.d() * b4.h());
		float splitcost = 0.3f + 1.0f * (SA1 * SAV * splist->n1count + SA2 * SAV * splist->n2count);
		// update best cost tracking variables
		if (splitcost < lowcost)
		{
			lowcost = splitcost;
			bestpos = splist->splitpos;
			b1 = b3, b2 = b4;
		}
		splist = splist->next;
	}
	if (lowcost > Cleaf) return;
	node->SetSplitPos( bestpos );
	// construct child nodes
	KdTreeNode* left = s_MManager->NewKdTreeNodePair();
	int n1count = 0, n2count = 0, total = 0;
	// assign primitives to both sides
	float b1p1 = b1.GetPos()[axis];
	float b2p2 = b2.GetPos()[axis] + b2.GetSize()[axis];
	float b1p2 = b1.GetPos()[axis] + b1.GetSize()[axis];
	float b2p1 = b2.GetPos()[axis];
	for ( int i = 0; i < prims; i++ )
	{
		Primitive* p = parray[i];
		total++;
		if ((eleft[i] <= b1p2) && (eright[i] >= b1p1)) 
		{
			if (p->IntersectBox( b1.GetPos(), b1.GetPos() + b1.GetSize() )) 
			{
				left->Add( p );
				n1count++;
			}
		}
		if ((eleft[i] <= b2p2) && (eright[i] >= b2p1)) 
		{
			if (p->IntersectBox(  b2.GetPos(), b2.GetPos() + b2.GetSize() )) 
			{
				(left + 1)->Add( p );
				n2count++;
			}
		}
	}
	delete eleft;
	delete eright;
	delete parray;
	s_MManager->FreeObjectList( node->GetList() );
	node->SetLeft( left );
	node->SetLeaf( false );
	if ( depth < MAXTREEDEPTH)
	{
		if (n1count > 2) Subdivide( left, b1, depth + 1, n1count );
		if (n2count > 2) Subdivide( left + 1, b2, depth + 1, n2count );
	}
}

// -----------------------------------------------------------
// KdTreeNode class implementation
// -----------------------------------------------------------

void KdTreeNode::Add( Primitive* prim )
{
	ObjectList* lnode = KdTree::s_MManager->NewObjectList();
	lnode->SetPrimitive( prim );
	lnode->SetNext( GetList() );
	SetList( lnode );
}

}
