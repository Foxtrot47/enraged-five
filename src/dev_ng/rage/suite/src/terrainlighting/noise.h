// 
// noise.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// TITLE: Noise generation for terrain
// PURPOSE:
//		This is useful classes for generating terrain
//

#ifndef NOISE_GEN_H
#define NOISE_GEN_H


#include "parser/manager.h"

namespace rage
{

//
//	PURPOSE
//		Generates perlin noise so all types of procedural effects can be created.
//	NOTES
//		Currently just uses a direction translation of perlin noise but could be optimized
//
class Noise 
{
public:

	float operator()(float x, float y) const
	{
		int Xint = (int)x;
		int Yint = (int)y;
		float Xfrac = x - Xint;
		float Yfrac = y - Yint;

		float x0y0 = pseudoRandomValue(Xint, Yint);  //find the noise values of the four corners
		float x1y0 = pseudoRandomValue(Xint+1, Yint);
		float x0y1 = pseudoRandomValue(Xint, Yint+1);
		float x1y1 = pseudoRandomValue(Xint+1, Yint+1);

		Xfrac = scurve( Xfrac );
		Yfrac = scurve( Yfrac );

		//interpolate between those values according to the x and y fractions
		float v1 = Interpolate(x0y0, x1y0, Xfrac); //interpolate in x direction (y)
		float v2 = Interpolate(x0y1, x1y1, Xfrac); //interpolate in x direction (y+1)
		float fin = Interpolate(v1, v2, Yfrac);  //interpolate in y direction

		return fin;
	}
private:
	float scurve( float v ) const	{		return v * v * ( 3.0f - 2.0f * v );  	}
	float Interpolate(float x, float y, float a)  const
	{
		return x + a * ( y - x ); //add the weighted factors
	}
	float pseudoRandomValue(int x, int y) const
	{
		int n;
		n = x + y * 57;
		n = (n<<13) ^ n;
		float res = (float)( 1.0 - ( (n * (n * n * 15731 + 789221)
			+ 1376312589) & 0x7fffffff ) / 1073741824.0);
		return res;
	}	
};

//
//	PURPOSE
//		Generates a multifractal value on a lattice. This uses the ridged multifractal
//		from Texturing & Modeling a procedural approach.
//
class MultiFractal : public	datBase
{
public:
	MultiFractal( float H = 1.0f, float lacunarity = 2.0001f, float octaves = 8.0f, float offset = 1.0f, float gain = 2.0f, const Vector3& scale =Vector3( 1.0f, 1.0f, 1.0f)  )
		:  m_lacunarity( lacunarity), m_octaves( octaves ), m_offset( offset ), m_H( H ), m_gain ( gain ), m_scale( scale ), m_OnModificationEvent( NullCB )
	{
		Setup();
	}

	void Init( float H = 1.0f, float lacunarity = 2.0001f, float octaves = 8.0f, float offset = 1.0f, float gain = 2.0f, const Vector3& scale =Vector3( 1.0f, 1.0f, 1.0f)  )
	{
		m_lacunarity = lacunarity;
		m_octaves =  octaves;
		m_offset = offset;
		m_H =  H ;
		m_gain = gain;
		m_scale =  scale;

		Setup();
	}

	void Begin()			{ Setup(); }
	void End()				{}

	float GetHeight() const		{ return m_scale.y; }

	// Ridged
	float operator()( float x, float y)
	{
		Noise	noiseGen;

		x *= m_scale.x;
		y *= m_scale.z;

		x += 10000.0f;  // to stop errors at zero line
		y += 10000.0f;
		float signal =(  m_offset - abs( noiseGen( x, y )));
		signal *= signal;
		float value = signal;
		float weight = 1.0f;

		for ( int i = 1; i < m_octaves; i++ )
		{
			x *= m_lacunarity;
			y *= m_lacunarity;

			weight = Clamp( signal * m_gain, 0.0f, 1.0f );

			float signal = ( m_offset - abs( noiseGen( x, y ) ) ) ;
			signal *= signal * weight;

			value += signal * m_exponents[i];
		}
		return value * m_scale.y;
	}


	float hybridMultiFractal( float x, float y)
	{
		Noise	noiseGen;

		x += 10000.0f;  // to stop errors at zero line
		y += 10000.0f;
		float value =(  m_offset + noiseGen( x, y ) ) * m_exponents[0];;
		float weight = value;

		x *= m_lacunarity;
		y *= m_lacunarity;

		for ( int i = 1; i < m_octaves; i++ )
		{
			float signal = ( noiseGen( x, y ) + m_offset ) * m_exponents[i];
			weight = Min( weight, 1.0f );

			value += weight * signal;
			weight *= signal;

			// scale by altitude
			x *= m_lacunarity;
			y *= m_lacunarity;
		}
		return value;
	}


	static const int MaxOctaves = 16;

#if __BANK
	void AddWidgets( bkBank& bank, datCallback OnModificationCallback = NullCB ) 
	{
		m_OnModificationEvent = OnModificationCallback;

		bank.PushGroup("Terrain Generation", false);

		bank.AddSlider("Fractal Parameter ( H )",&m_H,0.000001f,5.0,0.01f  );
		bank.AddSlider("lacunarity",&m_lacunarity, 0.5f, 8.0f, 0.1f  );
		bank.AddSlider("offset",&m_offset, 0.1f, (float)MultiFractal::MaxOctaves, 0.15f );
		bank.AddSlider("Num Octaves",&m_octaves, 1.0f, 32.0f, 0.15f  );
		bank.AddSlider("Gain",&m_gain, 0.0f, 4.0f, 0.15f );
		bank.AddSlider("Height Scale",&m_scale.y, 0.001f, 100.0f, 0.15f );
		bank.AddSlider("Width Scale",&m_scale.x, 0.001f, 100.0f, 0.25f );
		bank.AddSlider("Depth Scale",&m_scale.z, 0.001f, 100.0f, 0.25f );

	
		bank.PopGroup();
	}
#endif
	
	PAR_PARSABLE;
private:

	void Setup()
	{
		float freq = 1.0f;
		for ( int i = 0; i < m_octaves; i++ )
		{
			m_exponents[i] = pow( freq, -m_H );
			freq *= m_lacunarity;
		}
	}

	//void OnModification()
	//{
	//	m_OnModificationEvent.Call();
	//}

	float m_lacunarity;
	float m_octaves;
	float m_offset;
	float m_H;
	float m_gain;
	Vector3 m_scale;
	float m_exponents[ MaxOctaves ];

	datCallback	m_OnModificationEvent;
};

}

#endif
