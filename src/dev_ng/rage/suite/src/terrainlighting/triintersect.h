// 
// rage_ray/triIntersect/triintersect.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// TITLE: Triangle Intersection Code
// PURPOSE:
//		This is for fast ray triangle intersections.


#ifndef TRIINTERSECT_H
#define TRIINTERSECT_H
#include "vector/vector3.h"
#include "mesh/mesh.h"

#if MESH_LIBRARY

namespace rage
{

class Ray
{
	Vector3		m_origin;
	Vector3		m_direction;
public:
	Ray( const Vector3&	o, Vector3&  d ) : m_origin( o ), m_direction( d )
	{}

	const Vector3&		GetOrigin()	{ return m_origin; }
	const Vector3&		GetDirection() { return m_direction; }
};


class TriInfo
{
	mshVertex		m_v[3];
public:
	TriInfo( const mshVertex& v1, const mshVertex& v2, const mshVertex& v3 )
	{
		m_v[ 0] = v1;
		m_v[ 1] = v2;
		m_v[ 2] = v3;
	}
	void Dot( const Matrix34& mtx )
	{
		 m_v[ 0].Pos.Dot( mtx);	m_v[ 0].Nrm.Dot3x3( mtx );
		 m_v[ 1].Pos.Dot( mtx);	m_v[ 1].Nrm.Dot3x3( mtx );
		 m_v[ 2].Pos.Dot( mtx);	m_v[ 2].Nrm.Dot3x3( mtx );
	}
	const mshVertex&	GetVertex( int index ) const
	{
		return m_v[ index ];
	}
	
	Vector3	GetNormal( float u, float v ) const
	{
		Vector3		n1 = m_v[0].Nrm;
		Vector3		n2 = m_v[1].Nrm;
		Vector3		n3 = m_v[2].Nrm;
		
		Vector3 N = n1 + u * (n2 - n1) + v * (n3 - n1);
		N.Normalize();
		return N;
	}
	Vector3	GetPosition( float u, float v ) const
	{
		Vector3		n1 =  m_v[0].Pos;
		Vector3		n2 =  m_v[1].Pos;
		Vector3		n3 =  m_v[2].Pos;

		Vector3 N = n1 + u * (n2 - n1) + v * (n3 - n1);
		return N;
	}
	Vector2	GetTexCoord( float u, float v, int UVSet = 0 ) const
	{
		Assert( m_v[0].Tex.GetCount() > UVSet );
		Vector2		uv1 =  m_v[0].Tex[ UVSet];
		Vector2		uv2 =  m_v[1].Tex[ UVSet];
		Vector2		uv3 =  m_v[2].Tex[ UVSet];
		
		Vector2		d1 = uv2 - uv1 ;
		d1 = Vector2( d1.x * u, d1.y * u );

		Vector2		d2 = uv3 - uv1;
		d2 = Vector2( d2.x * v, d2.x * v );

		Vector2 R = uv1 + d1 + d2;
		return R;
	}
	void CreateBox( Vector3& min, Vector3& max )
	{
		max.Max( m_v[0].Pos, m_v[1].Pos );
		max.Max( m_v[2].Pos, max );

		min.Min( m_v[0].Pos, m_v[1].Pos );
		min.Min( m_v[2].Pos, min );
	}

};



// Intersection method return values
#define HIT		 1		// Ray hit primitive
#define MISS	 0		// Ray missed primitive
#define INPRIM	-1		// Ray started inside primitive


//
//	PURPOSE
//		A class for optimally intersecting a triangle with a ray
//	NOTES
//		The triangle needs to be static as a lot of the ray intersection calculations are precomputed.
//		It is supposed to be used separately from the main triangle data so as minimize size to minimize cache misses.
//
class TriIntersect
{
public: 
	TriIntersect() {};
	TriIntersect( TriInfo* Tri )		{ SetTri( Tri ); }
	
	int Intersect( Ray& ray, float& length , float& u, float& v );
	bool IntersectBox( const Vector3& boxMin, const Vector3& boxMax );
	void CalculateRange( float& p1, float& p2, int axis );
	// triangle primitive methods
	void SetTri( TriInfo* Tri  ) 
	{  
		Assert( Tri );
		m_tri = Tri;
		SetTri( Tri->GetVertex( 0).Pos, Tri->GetVertex( 1).Pos, Tri->GetVertex( 2).Pos );
	}

	const	TriInfo*	GetInfo()				{ return m_tri; }
	const mshVertex&	GetVertex( int i )		{ return m_tri->GetVertex( i); }
	

private:
	void SetTri( const Vector3& v1, const Vector3& v2, const Vector3& v3 );
	
	TriInfo*								m_tri;		// 4
	int k;									// 4						// could fold into m_tri; and save 4 bytes ( yee hah )
	float au;								// 4
	float av;								//4
	float nu, nv, nd;						// 12		
	float bnu, bnv;							// 8
	float cnu, cnv;							// 8, total: 44
};

}
#endif // MESH_LIBRARY

#endif
