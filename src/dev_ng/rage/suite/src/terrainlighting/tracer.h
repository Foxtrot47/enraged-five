


#ifndef TRACER_H
#define TRACER_H

namespace rage
{

struct	kdstack;
//class	Kdtree;
//class	Primitive;


class Tracer
{
public:
	Tracer();
	~Tracer();
	int FindNearest( Ray& ray, float& dist, const TriInfo*& prim, float& u, float& v );
	int FindOccluder( Ray& ray, float& dist  );

	KdTree*		GetKdTree()		{ return m_kdtree; }

protected:
	
	// traversal data
	KdTree*			m_kdtree;
	kdstack*		m_Stack;
	int*			m_Mod;
public:
	// counters
	int m_RaysCast, m_Intersections;
};

#define TILE_SIZE			32

template< class PrimaryRayPolicy, class ResultGenerationPolicy >
class RayEngine
{

public:
	RayEngine( Tracer& trace ) : m_tracer( trace )
	{}

	
	void Render( TileRender*	creator, grcTexture* tex )
	{
		int width = surface->GetWidth();
		int height = surface->GetHeight();

		
		// break up into tiles
		for( int x =0; x < width; x += TILE_SIZE )
		{
			for ( int y = 0; y < height; y += TILE_SIZE )
			{	
				creator->SetDetails( tex, x, Min( x+ TILE_SIZE, width ),
										  y, Min( y + TILE_SIZE, height ));
				creator->Generate( trace );
				
			}
		}

		surface->Lock();

		surface->Unlock();
	}
};

class TileRender
{
	int m_sX;
	int m_eX;
	int m_sY;
	int m_eY;

	int Width;
	int Height;
public:
	void TileRender
	void SetDetails( grcTexture* tex, int sX, int eX, int sY, int eY )
	{
		m_Tex = tex;
		m_sX = sX;
		m_eX = eX;
		m_sY = sY;
		m_eY = eY;
	}
	virtual void Generate( Tracer& trace) = 0;
};


template<class PrimaryRayPolicy, class ResultGenerationPolicy >
class RayTileRenderer : public PrimaryRayPolicy, public ResultGenerationPolicy, public TileRender
{
	int m_sX;
	int m_eX;
	int m_sY;
	int m_eY;

	grcTexture*	m_Tex;
public:
	TileRenderer( grcTexture* tex, int sX, int eX, int sY, int eY )
		: m_Tex( tex), m_sX( sX ), m_eX( eX ), m_sY( sY), m_eY( eY )
	{}

	void Generate( Tracer& trace )
	{
		for ( int y = sY; y < eY; y++ )
		{
			for ( int x = sX; x < eX; x++ )
			{
				Ray ray = GetPrimaryRay(  x, y );

				int texelOffset = y * m_Tex->GetWidth() + x;

				m_Tex[texelOffset] = CalculateResult( trace, ray ))
			}
		}
	}
};


namespace RayGen
{
	class CameraRays
	{
	public:
		void SetViewPort( grcViewport*		vp )
		{
			// set eye and screen plane position
			m_Origin = vector3( 0, 0, -5 );
			m_P1 = vector3( -4,  3, 0 );
			m_P2 = vector3(  4,  3, 0 );
			m_P3 = vector3(  4, -3, 0 );
			m_P4 = vector3( -4, -3, 0 );

			Matrix34	cameraInverse = vp->GetCameraMatrix();
			cameraInverse.Inverse();

			// move camera
			m_Origin = cameraInverse.Dot( m_Origin );
			m_P1 = cameraInverse.Dot( m_P1 );
			m_P2 = cameraInverse.Dot( m_P2 );
			m_P3 = cameraInverse.Dot( m_P3 );
			m_P4 = cameraInverse.Dot( m_P4 );

			// calculate screen plane interpolation vectors
			m_DX = (m_P2 - m_P1) * (1.0f / m_Width);
			m_DY = (m_P4 - m_P1) * (1.0f / m_Height);
		}

		
		Ray GetPrimaryRay(  int x, int y )
		{
			// convert to camera space
			Vector3 direction = m_dx * x + m_dy * y; 
			direction -= m_origin;
			direction.Normalize();

			return Ray( m_origin, direction );	
		}
	};


	// each have bank stuff
	class SimpleLighting		// should be able to set bank up to edit this
	{
	public:
		static const int	NumResultColors = 1;

		void SetLightDirection( Vector3 lightDir )
		{
			m_lightDirection = lightDir;
		}
		void SetDiffuseColor( Color32 col )
		{
			m_diffuseColor = col;
		}
		Color32 CalculateResult( Tracer& t, Ray& primRay  )
		{
			TriInfo*	tri;
			float		u;
			float		v;

			if ( t.FindNearest( primRay, dist, tri, u, v ) )
			{
				// have hit so render it
				float lit = Clamp( tri->GetNormal( u, v ).Dot( m_lightDirection ), 0.0f, 1.0f );
				return m_diffuseColor * lit;
			}
			return 0.0f;
		}
	private:
		Vector3 m_lightDirection;
		Color32 m_diffuseColor;
	}

}
;
}

#endif
