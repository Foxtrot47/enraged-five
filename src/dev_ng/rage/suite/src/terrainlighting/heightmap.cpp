
// 
// height map.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 


#include "heightmap.h"
#include "mesh/grcrenderer.h"

#include "mesh/serialize.h"

#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/indexbuffer.h"
#include "grcore/device.h"
#include "grmodel/modelfactory.h"

#include "grmodel/shadergroup.h"

#include "triintersect.h"
#include "math.h"
#include "math\Float16.h"
#include "system/xtl.h"
#include "grcore/image.h"
#include "grcore/texture.h"


namespace rage
{

inline u32 ConvertToU32( const Vector3& value )
{
	u8 r =(u8) ( value.x * 255.0f );
	u8 g =(u8) ( value.y * 255.0f );
	u8 b = (u8) (value.z * 255.0f );

	return  (u32) (( (((r)&0xff)<<16) | (((g)&0xff)<<8) | ((b)&0xff) ));
}
inline u32 ConvertNormToU32( const Vector3& v )
{

	Vector3 value = ( v  + Vector3( 1.0f , 1.0f, 1.0f ) ) / Vector3( 2.0f, 2.0f, 2.0f );
	return ConvertToU32( value );
}

void HeightMap::CalculateNormalMaps()
{
	// now do the normals
	for( int x = 0; x < m_width; x++ )
	{
		for( int y = 0; y < m_hieght; y++ )
		{
			m_normals[ x + y * m_width ] = GetNormal( x, y );
		}
	}
}

void HeightMap::BuildLighting()
{

	CalculateNormalMaps();
	// and calulate visiblity
	CalculateIntervalMap();
	CalculateAmbientOcclusion();
//	UpdateTextures();
}

Vector3	HeightMap::intervalFlippedY( int x, int y )
{
	x = Max( Min(x, m_width -1 ), 0 );
	y = Max( Min(y, m_width -1 ), 0 );

	return m_intervalMap[ x + ( m_width - y  - 1) * m_width ] ;
}

grcImage*		HeightMap::MakeHeightTexture( int width, int offset , float& minimum, float& scale ) const
{
	grcImage* pImage = grcImage::Create( width, width, 1, grcImage::G16R16, grcImage::STANDARD, 0, 0);

	int end = width + offset;

	// get the min and max
	float maximum = -FLT_MAX;
	minimum = FLT_MAX;

	for( int i = 0; i < m_width*  m_hieght; i++  )
	{
		
		maximum = Max( m_heights[ i], maximum );
		minimum = Min( m_heights[ i], minimum );
	}

	// 
	scale = maximum - minimum;
	float invScale = 1.0f/scale;
	
	for( int y = offset; y < end; y++ )
	{
		for ( int x =offset; x < end ; x++ )
		{
			float occ = (GetVal( x, m_width - y -1 ) - minimum )* invScale ;
			Assert( occ >= 0.0f && occ <= 1.0f );

			pImage->SetPixel( x - offset, end - y - 1,  (u32)( occ * ( 1 << 16 ) ) );
		}
	}

	return pImage;
}

void HeightMap::WriteRawFile( const char* filename ) const
{
	fiStream* pStream = fiStream::Create( filename );
	pStream->WriteInt(&m_width, 1);

	pStream->WriteFloat( m_heights, m_width * m_width);
	pStream->Close();
}

float HeightMap::GetSplodgeValue( int x, int y )
{
	float occ = m_lightOcclusion[ x + ( m_width - y -1  ) * m_width ];
	occ = 1.0f - Clamp( occ, 0.0f, 1.0f );
	return occ;
}

grcImage* 		HeightMap::MakeAmbOccTexture( int width, int offset )
{
	grcImage* pImage = grcImage::Create( width, width, 1, grcImage::L8, grcImage::STANDARD, 0, 0);

	int end = width + offset;

	for( int y = offset; y < end; y++ )
	{
		for ( int x =offset; x < end ; x++ )
		{
			float occ = GetAmbOcculsion( x, m_width - y -1 );
			occ = Min( GetSplodgeValue( x, y )+ 0.05f, occ );
			//occ *= treeOcclusion;
			pImage->SetPixel( x - offset, end - y - 1,  (u32)( occ * 255.0f) );
		}
	}
	return pImage;
}
grcImage* 		HeightMap::MakeLightOccTexture( int width, int offset )
{
	grcImage* pImage = grcImage::Create( width, width, 1, grcImage::L8, grcImage::STANDARD, 0, 0);

	int end = width + offset;

	for( int y = offset; y < end; y++ )
	{
		for ( int x =offset; x < end ; x++ )
		{
			float occ = GetSplodgeValue( x, y );
			pImage->SetPixel( x - offset, end - y - 1,  (u32)( occ * 255.0f) );
		}
	}
	return pImage;
}
grcImage* 		HeightMap::MakePrtTexture( int width, int offset )
{
	grcImage* pImage = grcImage::Create( width, width, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);

	int end = width + offset;

	for( int y = offset; y < end; y++ )
	{
		for ( int x =offset; x < end ; x++ )
		{
			Vector4	coeff = m_prtMap[ x + ( m_width - y -1  ) * m_width ].Get();
			coeff = coeff * 0.5f + Vector4( 0.5f, 0.5f, 0.5f, 0.5f );
			Color32 col( coeff );

			pImage->SetPixel( x - offset, end - y - 1,  col.GetColor() );
		}
	}
	return pImage;
}
grcImage* HeightMap::MakeIntervalTexture( int width, int offset , bool smooth )
{
	grcImage* pImage = grcImage::Create( width, width, 1, grcImage::G16R16, grcImage::STANDARD, 6, 0);  // G16R16F
	int end = width + offset;

	int step = 1;

	grcImage* player = pImage;

	while ( player )
	{
	
		for( int y = offset; y < end; y += step )
		{
			for ( int x =offset; x < end ; x+= step )
			{
				// apply smoothing to shadow
				Vector3		interval( intervalFlippedY( x, y ) );

				smooth = true;
				if ( smooth )
				{
					interval = interval / 2.0f +
											(	intervalFlippedY( x + 1, y ) + 
												intervalFlippedY( x - 1, y ) + 
												intervalFlippedY( x, y + 1 ) + 
												intervalFlippedY( x, y - 1) )  / 8.0f ;
				}

				Assert( interval.x >= 0.0f && interval.x <= 1.0f );
				Assert( interval.y >= 0.0f && interval.y <= 1.0f );

			
				//interval *= interval; // power 4
				//interval *= interval;

				u16 red = (u16)( interval.x * (float)( 1 << 16 ) );
				u16 green = (u16)( interval.y * (float)( 1 << 16) );
				u32 encodedValue = ((u32) green <<16 )| (u32)red;
				player->SetPixel( ( x - offset ) / step , ( end - y - 1 ) / step,  encodedValue );
			}
		}
		step *=2;
		player = player->GetNext();

	}

	return pImage;
}
void	HeightMap::CheckIntervalTexture(  grcImage* UNUSED_PARAM( IntervalMapImage ), int UNUSED_PARAM(width ), int UNUSED_PARAM( offset ) )
{
}
Vector3 HeightMap::GetNormal( int x, int y )
{
	int xs = Min( x + 1, m_width - 1 );
	int ys = Min( y + 1, m_hieght - 1 );
	int xe = Max(x - 1, 0 );
	int ye = Max(y - 1, 0 );


	Vector3 acc( 1.0f, GetVal( xs, y ) - GetVal( xe, y), 0.0f );
	Vector3 down( 0.0f, GetVal( x, ys ) - GetVal( x, ye ), 1.0f );

	Vector3 norm;
	acc.Normalize();
	down.Normalize();

	norm.Cross(  down, acc );
	norm.Normalize();

	return norm;
}

float HeightMap::MaxAngle( const Vector3& direction, const Vector3& pos, int maxSteps, float& steps , float startAngle  )
{
	steps = 0.0f;
	float InAngle = startAngle;

	Vector3 pt = pos;
	Vector3 maxRange(0.0f, 0.0f, 0.0f );

	maxRange.x = direction.x >= 0.0f ? ( m_width -pos.x) : -pos.x;
	maxRange.z = direction.z >= 0.0f ? ( m_hieght -pos.z) : -pos.z;
	
	maxRange /= direction + Vector3( 0.0001f, 0.00001f, 0.00001f );

	maxSteps = Min( (int)floor( maxRange.x  - 0.5f ), (int) floor( maxRange.z  - 0.5f ), maxSteps );
	
	float s =1.0f;
	steps = Max( (float) maxSteps, s );
	ASSERT_ONLY( float mStep = Max( (float) maxSteps, s ); )


	while( maxSteps-- > 0)
	{
		// calculate angle

		pt += direction;
		int x = (int) pt.x;
		int z = (int) pt.z;

	
		Assert( ! ( ( x >= m_width || x < 0 ) ||
			( z >= m_hieght || z < 0 ) ) );
		
		float hdiff = ( GetVal( x, z) - pt.y ) / s;
		if ( hdiff > InAngle )
		{
			InAngle = hdiff;
			steps = s;
		}
		s += 1.0f;
	}
	Assert( steps <= (float) mStep && steps >= 1.0f );
	return InAngle;
}
float HeightMap::MaxAngleNorm( const Vector3& direction, const Vector3& pos, int maxSteps, float& steps , float startAngle , const Vector3& norm )
{
	steps = 0.0f;
	float InAngle = startAngle;

	Vector3 pt = pos;
	Vector3 maxRange(0.0f, 0.0f, 0.0f );

	maxRange.x = direction.x >= 0.0f ? ( m_width -pos.x) : -pos.x;
	maxRange.z = direction.z >= 0.0f ? ( m_hieght -pos.z) : -pos.z;

	maxRange /= direction + Vector3( 0.0001f, 0.00001f, 0.00001f );

	maxSteps = Min( (int)floor( maxRange.x  - 0.5f ), (int) floor( maxRange.z  - 0.5f ), maxSteps );

	float s =1.0f;
	steps = Max( (float) maxSteps, s );
	ASSERT_ONLY( float mStep = Max( (float) maxSteps, s ); );


	while( maxSteps-- > 0)
	{
		// calculate angle

		pt += direction;
		int x = (int) pt.x;
		int z = (int) pt.z;

		Assert( ! ( ( x >= m_width || x < 0 ) ||
			( z >= m_hieght || z < 0 ) ) );

		Vector3 newpos( pt.x, GetVal( x, z) , pt.z );
		Vector3 dir = newpos - pos;
		dir.Normalize();

		float hdiff = norm.Dot( dir );
		
		InAngle = Max(  hdiff , InAngle );
	}
	Assert( steps <= (float) mStep && steps >= 1.0f );
	return InAngle;
}

void HeightMap::SetWaterPlane( float h )
{
	for ( int i = 0; i < m_width * m_hieght; i++ )
	{
		m_heights[i] = Max( h, m_heights[i] );
	}
}
void HeightMap::UpdateTextures()
{
	// Lock the texture
	//grcTextureLock lockNorm;
	//grcTextureLock lockInterval;
	//m_NormalTexMap->LockRect(0, 0, lockNorm);
	//m_IntervalTexMap->LockRect( 0, 0, lockInterval );

	//// Copy the data into the texture
	//u32* pNormalTexturePtr = (u32*)lockNorm.Base;
	//u32* pIntervalTexturePtr = (u32*)lockInterval.Base;
	//for( int y = 0; y < m_width; y++ )
	//{
	//	for ( int x =0; x < m_width ; x++ )
	//	{
	//		*pNormalTexturePtr = ConvertNormToU32( m_normals[ x + ( m_width- y  - 1) * m_width ] );
	//		pNormalTexturePtr = (u32*)(((u8*)pNormalTexturePtr) + 4);

	//		*pIntervalTexturePtr = ConvertToU32( m_intervalMap[ x + ( m_width - y  - 1) * m_width ] );
	//		pIntervalTexturePtr = (u32*)(((u8*)pIntervalTexturePtr) + 4);
	//	}
	//}

	//// Unlock the texture
	//m_NormalTexMap->UnlockRect( lockNorm);
	//m_IntervalTexMap->UnlockRect( lockInterval);

	if ( m_IntervalTexMap )
	{
		m_IntervalTexMap->Release();
	}
	grcImage*	pImage = MakeIntervalTexture( m_width, m_haveEdgeTiles ? m_hieght : 0  , false );

	m_IntervalTexMap = grcTextureFactory::GetInstance().Create( pImage );

	pImage->Release();
}

void HeightMap::CalculateAmbientOcclusion()
{
	for( int x = 0; x < m_width; x++ )
	{
		for( int y = 0; y < m_hieght; y++ )
		{
			// Temporally removed as to speed up process
			
			m_intervalMap[ x + y * m_width ].z = AmbientOcclusionSample( x, y , m_normals[ x + y * m_width ],
																				m_prtMap[ x + y * m_width ] );
		}
	}
}
//void AddArc( SphericalVis2ndOrder& prtSample, Vector3 dirn , float solidAngle)
//{
//	float numSegments = 32.0f
//	int sy = (int) floor( dirn.y * numSegments);
//	for ( int y = sy; y < (int) numSegments; y++ )
//	{
//		Vector3 angle = 
//	}
//
//}
//
inline float sinc(float x)
{               /* Supporting sinc function */
	if (fabs(x) < 1.0e-4) return 1.0 ;
	else return(sin(x)/x) ;
}
bool FloatIsClose( float a, float b, float ep )
{
	return abs( a - b ) < ep;
}
void ComputeArc( SphericalVis2ndOrder&  arc, int numSlicesAcross, int numSlicesDown, const Vector3& direction  )
{
	
	float y =  acos( Clamp( direction.y, -1.0f, 1.0f ) );
	ASSERT_ONLY( float invCos = cos( y) ; );
	Assert(  FloatIsClose( direction.y ,invCos , 0.0001f ) );
	
	float delta = 2.0f * PI /numSlicesDown;
	for ( float theta = 0.0f; theta < y ; theta += delta )
	{
		Vector3 dir = direction;
		dir.y =  cos(theta);
		// scale so it's nomalize
		if ( dir.y == 1.0f )
		{
			dir.y = 0.9999999f;		// a bit of error never hurts
		}
		float xScale = sqrtf( 1.0f - dir.y * dir.y );
		dir.x *= xScale;
		dir.z *= xScale;

		float domega = (2*PI/numSlicesAcross)*(2*PI/numSlicesDown)*sinc(theta) ;
		arc.Add( dir ,domega);
	}
}
float HeightMap::AmbientOcclusionSample( int x, int y, const Vector3&   norm , SphericalVis2ndOrder& prtSample)
{
	const int NumCasts = 8;
	const float MaxSteps = 128.0f;//32.0f;

	Vector3 pos( (float)x, GetVal( x, y ), (float)y );

	float occ = 0.0f;
	for ( int i = 0; i < NumCasts; i++ )
	{
		float theta = ( PI * 2.0f * (float) i ) / (float)NumCasts;
		Vector3 direction( cos( theta), 0.0f, sin( theta ) );


		int MSteps = (int)MaxSteps;

		float steps;
		//float angle = MaxAngle( direction, pos, MSteps, steps, -1.0f );

		float angle = MaxAngleNorm( direction, pos, MSteps, steps, 0.0f , norm );

		Assert( steps > 0.0f && steps <= MSteps );
		// add in the cone segment

		float occAmount = ( 1.0f - angle );
		 occ += occAmount; // Max(normal.Dot( oAng ), 0.0f)

		 // lets do prt as well
		 Vector3 prtDirection( direction.x, angle, direction.z );
		 //float xScale = sqrtf( 1.0f - prtDirection.y * prtDirection.y );
		 //prtDirection.x *= xScale;
		 //prtDirection.z *= xScale;
		 //prtDirection.Normalize();
		 
		 ComputeArc( prtSample, NumCasts, 32, prtDirection );
		 //prtSample.Add( prtDirection, PI * occAmount/( NumCasts) );

		Assert( occ >= 0.0f );
	}
	Assert( occ > 0.0f );
	occ = occ / (float) NumCasts;
	return pow( occ, m_occPower );
}
Vector3 HeightMap::CalculateIntervalGeneral( const Vector3& arc, int x, int y )
{
	Vector3 Direction = arc;
	Vector3 pos( (float)x,  GetVal( x, y ), (float)y );

	Vector3	NegativeDir;
	NegativeDir.Negate( Direction );

	float steps = 1.0f;
	float InAngle = MaxAngle( NegativeDir, pos, m_width, steps );
	float OutAngle = MaxAngle( Direction, pos, m_width, steps );

	Vector3 ang( -1.0f, InAngle, 0.0f );
	Vector3 oAng( 1.0f, OutAngle, 0.0f );
	ang.Normalize();
	oAng.Normalize();

	Assert( ang.y >= 0.0f && ang.y <= 1.0f );
	Assert( oAng.y >= 0.0f && oAng.y <= 1.0f );
	return Vector3( ang.y, oAng.y, 0.0f );
}
Vector3 HeightMap::CalculateInterval( int x, int y )
{
	Vector3 Direction( 1.0f, 0.0f, 0.0f );
	float h = GetVal( x, y );
	Vector3 pos( (float)x, h, (float)y );
		// do scan backwards
	float InAngle = 0.0f;
	float steps = 1.0f;

	for ( int i = x -1; i >= 0 ; i-- , steps+= 1.0f )
	{
		// calculate angle
		float hdiff = ( GetVal( i, y) - h ) / steps;
		if ( hdiff > InAngle )
		{
			InAngle = hdiff;
		}
	}

	Vector3	NegativeDir;
	NegativeDir.Negate( Direction );
	ASSERT_ONLY( float inAng2 = MaxAngle( NegativeDir, pos, m_width, steps ); );
	Assert( inAng2 == InAngle );


	Vector3 ang( -1.0f, InAngle, 0.0f );
	ang.Normalize();

	// do scan forwards
	float OutAngle = 0.0f;
	steps = 1.0f;

	for ( int i = x + 1; i < m_width; i++, steps+= 1.0f )
	{
		// calculate angle
		float hdiff = ( GetVal( i, y) - h ) / steps;
		if ( hdiff > OutAngle )
		{
			OutAngle = hdiff;
		}

	}
	ASSERT_ONLY(	float outAng2 = MaxAngle( Direction, pos, m_width, steps ); );
	Assert( outAng2 == OutAngle );

	Vector3 oAng( 1.0f, OutAngle, 0.0f );
	oAng.Normalize();

	Assert( ang.y >= 0.0f && ang.y <= 1.0f );
	Assert( oAng.y >= 0.0f && oAng.y <= 1.0f );
	return Vector3( ang.y, oAng.y, 0.0f );
}
void HeightMap::AddTreeSplodge( const Vector3& position, 
											float radius, 
											float height )
{
	Assert( radius > 0.0f && height > 0.0f );

	const float splodgeScale =6.0f;

	int sx = Clamp( (int)( floor( position.x - splodgeScale * radius ) ), 0, m_width - 1 );
	int sy = Clamp( (int)( floor( position.y - splodgeScale * radius ) ), 0, m_width - 1 );

	int ex = Clamp( (int)( ceil( position.x + splodgeScale * radius ) ), 0, m_width - 1 );
	int ey = Clamp( (int)( ceil( position.y + splodgeScale * radius ) ), 0, m_width - 1 );

	for ( int x = sx; x < ex; x++ )
	{
		for ( int y = sy; y < ey; y++ )
		{
			float h = ( position.x - (float)x ) * ( position.x - (float)x )
						+ ( position.y - (float)y) * ( position.y - (float)y );

			h = sqrtf( h );
			h /= radius;
			h = 1.0f - h;
			if ( h > 0.0f && x == (int) position.x )
			{
				float hieghtOffset  = height;
				SetDirect( (float)x, (float)y, position.z + hieghtOffset );
			}
			if ( h > ( 1.0f- splodgeScale ))
			{
				float splodge = ( h + (splodgeScale -1 )) / splodgeScale;
				
				splodge *= splodge;	// square falloff
				splodge *= 0.5f;
				
				SetLightOcc( x, y, splodge );
			}
		}
	}
//	SetLightOcc( (int)position.x, (int)position.y, 0.2f );


}

void HeightMap::CalculateIntervalMap()
{
	m_isBuilt = true;

	if ( m_haveEdgeTiles )
	{
		int wSize = m_width / 3;
		int hSize = m_hieght / 3;
		for( int x = wSize -1 ; x < ( 2.0f * wSize + 1 ); x++ )
		{
			for( int y = hSize- 1; y < ( 2.0f * hSize + 1 ); y++ )
			{
				m_intervalMap[ x + y * m_width ] = CalculateIntervalGeneral( m_sunArc, x, y ); //CalculateInterval( x, y );			
			}
		}
	}
	else
	{
		for( int x = 0 ; x < m_width; x++ )
		{
			for( int y = 0; y < m_hieght; y++ )
			{
				m_intervalMap[ x + y * m_width ] = CalculateIntervalGeneral( m_sunArc, x, y ); //CalculateInterval( x, y );			
			}
		}
	}

}
void HeightMap::CreateTextures()
{
	grcImage* pImage = grcImage::Create( m_width, m_width, 1,  grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);
	m_NormalTexMap = grcTextureFactory::GetInstance().Create(pImage);
	pImage->Release();

	pImage = grcImage::Create( m_width, m_width, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);
	m_IntervalTexMap = grcTextureFactory::GetInstance().Create(pImage);
	pImage->Release();
}

const float Eplision = 0.01f;
bool IsNear( float a, float b)
{
	return ( abs( a - b ) < Eplision );
}

bool isDegenerate( const Vector3&	v1,  const Vector3&	v2,  const Vector3&	v3 )
{
	Vector3	A = v2 - v1;
	Vector3 B = v3 - v1;
	Vector3 N;
	 N.Cross(  A, B );
	return ( A.Dot(A ) == 0.0f || B.Dot( B) == 0.0f  ||  N.Dot( N ) == 0.0f );

}

#if MESH_LIBRARY

void ScanTri( HeightMap& h, const Matrix34& transform, const mshVertex& v1, const mshVertex& v2, const mshVertex& v3 )
{
	// transform triangle
	Vector3		max;
	Vector3		min;

	if ( isDegenerate( v1.Pos , v2.Pos, v3.Pos ) )
	{
		return;
	}
	TriInfo			info( v1, v2, v3 );

	info.Dot( transform );
	info.CreateBox(  min, max );

	max = Vector3( ceil( max.x + 1.0f ), ceil( max.y + 1.0f ), max.z );
	min = Vector3( floor( min.x ), floor( min.y  ), min.z );

	TriIntersect	tri( &info );

	for ( float y = min.y ; y <  max.y ; y+=1.0f )
	{
		for ( float x = min.x ; x< max.x ; x+=1.0f )
		{
			// create box
			float			ox = x + 0.5f;
			float			oy = y + 0.5f;
			Vector3			origin( ox, oy, max.z + 100.0f);
			Vector3			direction( 0.0f, 0.0f, -1.0f );
			float			length = 100000.0f;

			Ray sample( origin, direction );

			// convert ray to world space
			float u;
			float v;
			int hit  = tri.Intersect( sample, length, u, v );
			if ( !hit )
			{	
				direction.Negate();
				origin.z -= 1000.0f;
				Ray sample2( origin, direction );
				length = 100000.0f;
				hit  = tri.Intersect( sample2, length, u, v );
			}
			if ( hit )
			{
				// add sample point
				Vector3 pos = tri.GetInfo()->GetPosition( u, v );
				Vector3 hitPos = origin + direction * length;
				Assert( IsNear( pos.x ,ox ) );
				Assert( IsNear( pos.y, oy ) );
				Assert( pos.IsClose(hitPos, Eplision ));
				h.SetDirect( ox, oy, pos.z );
			}
		}
	}
}

void ScanMesh( HeightMap& h  ,mshMesh& mesh,  Matrix34& bound )
{

	for( int i = 0; i < mesh.GetMtlCount(); i++ )
	{
		mshMaterial& mtl = mesh.GetMtl( i );

		for ( mshMaterial::TriangleIterator itor = mtl.BeginTriangles();
			itor !=  mtl.EndTriangles();
			++itor )
		{
			int a;
			int b;
			int c;
			itor.GetVertIndices( a, b, c );


			if ( ( a != b ) &&( b != c ) && ( a != c ) )	// make sure it is not degenerate
			{
				ScanTri( h, bound, mtl.GetVertex( a ), mtl.GetVertex( b), mtl.GetVertex(c));
			}
		}
	}
};

#endif //MESH_LIBRARY

//------------------------------------------------------------------------------------------------------------------
//
//	HeightField renderer
//------------------------------------------------------------------------------------------------------------------
void HeightFieldRender::Init( int size, const char* shaderName )
{
	m_gridSize = size;	


	// STEP #1. Set up a vertex format.
	grcFvf fvf;
	fvf.SetPosChannel(true);
	fvf.SetNormalChannel(true);
	fvf.SetTextureChannel(0,true);
	fvf.SetTextureChannel(1, true, grcFvf::grcdsFloat3 );

	m_VertexDecl = grmModelFactory::BuildDeclarator(&fvf, NULL, NULL, NULL);

	// STEP #2. Create a vertex buffer and fill it with some interesting data
	m_VertexBuffer = grcVertexBuffer::Create( m_gridSize* m_gridSize,fvf);

	// STEP #3. Create an index buffer and fill it with data as well.
	m_IndexBuffer = grcIndexBuffer::Create(( m_gridSize-1)*( m_gridSize-1)*6);
	u16 *lockPtr = m_IndexBuffer->LockRW();
	int offset = 0;
	for (int i=0; i< m_gridSize-1; i++) {
		for (int j=0; j< m_gridSize-1; j++) {
			// First triangle for cell
			Assign(lockPtr[offset++], j+ m_gridSize*i);
			Assign(lockPtr[offset++], j+ m_gridSize*i+ m_gridSize);
			Assign(lockPtr[offset++], j+ m_gridSize*i+1);
			// Second triangle for cell
			Assign(lockPtr[offset++], j+ m_gridSize*i+1);
			Assign(lockPtr[offset++], j+ m_gridSize*i+ m_gridSize);
			Assign(lockPtr[offset++], j+ m_gridSize*i+ m_gridSize+1);
		}
	}
	m_IndexBuffer->UnlockRW();

	// STEP #4. Get a shader to render the geometry with
	m_Shader = grmShaderFactory::GetInstance().Create( );
	m_Shader->Load( shaderName);
}

void HeightFieldRender::UpdateVertices( HeightMap* terrain )
{
	int offset = m_offset;;
	m_isSetup = true;

	grcVertexBufferEditor vertexBufferEditor(m_VertexBuffer);
	float cx = ( m_gridSize-1)/-2.0f;
	float cy = ( m_gridSize-1)/-2.0f;
	for (int i=0; i< m_gridSize; i++) {
		for (int j =0; j < m_gridSize; j++) {
			int idx = i *  m_gridSize + j;
			Vector3 v(cx + i,0.0f,cy + j);
			Vector3 normal = terrain->GetNorm( i + offset, j + offset );

			v.y = terrain->GetVal( i  + offset, j  + offset ) ;
			vertexBufferEditor.SetPosition(idx,v);

			float occ =  terrain->GetAmbOcculsion( i  + offset, j  + offset);


			vertexBufferEditor.SetNormal( idx, normal );
			vertexBufferEditor.SetUV( idx,0,Vector2(i / float( m_gridSize),j / float( m_gridSize)));

			Vector2 interval = terrain->GetInterval( i  + offset, j  + offset );
			Vector3 terraShad( interval.x, interval.y, occ );

			vertexBufferEditor.SetUV( idx, 1, terraShad );
		}
	}
}


void HeightFieldRender::Draw()
{
	if (!m_isSetup )
	{
		return;
	}
	
	grcEffectTechnique technique = m_Shader->LookupTechnique( "drawHeightField" , true);
	if ( m_Shader->BeginDraw( grmShader::RMC_DRAW,true , technique ) )
	{
		m_Shader->Bind();
		GRCDEVICE.SetVertexDeclaration( m_VertexDecl );

		GRCDEVICE.SetIndices(*m_IndexBuffer );
		GRCDEVICE.SetStreamSource( 0, *m_VertexBuffer , 0, m_VertexBuffer->GetVertexStride() );
		GRCDEVICE.DrawIndexedPrimitive( drawTris, 0,  m_IndexBuffer->GetIndexCount() );

		m_Shader->UnBind();
		m_Shader->EndDraw();
	}
}

HeightFieldRender::~HeightFieldRender()
{
	
	delete m_IndexBuffer;
	delete m_VertexBuffer;

}


}
