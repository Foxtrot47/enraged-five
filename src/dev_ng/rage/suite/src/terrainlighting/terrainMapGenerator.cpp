
// 
// terrain map generator.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 


#include "terrainMapGenerator.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "system/param.h"
#include "vector/matrix44.h"
#include <strstream>


#if MESH_LIBRARY


namespace rage
{


void TerrainMapGenerator::Init( int gridSize, int tiles, bool doAmbient, bool doHeightTexture   )
{
	int scale = tiles > 2 ? tiles : 1;
	m_tiles = tiles;
	// create the heightmap
	m_terrainHieghts = rage_new HeightMap( gridSize * scale, gridSize * scale, m_tiles > 2 );

	m_createAmbient = doAmbient;
	m_createHeightTexture = doHeightTexture;
}


Matrix44 TerrainMapGenerator::CreateHeightMapFromTiles()
{
	Matrix44	texTransfrom;
	m_tControl.GenerateHeightMap( *m_terrainHieghts, texTransfrom, m_tiles );
	// minimum amount of work.

	return texTransfrom;
}

void TerrainMapGenerator::LoadTile( int tileIndex, const sysParam& param )
{
	const char* tilePath;
	Assert( tileIndex >= 0 && tileIndex <= 9 );

	if ( param.Get( tilePath ) )
	{
		if ( ! m_tControl.LoadTile( tileIndex, tilePath ) )
		{
			Errorf("\nTerrain Tile could not be loaded %s \n", tilePath );
		}
	}
}
std::string ToText( const Vector4& v )
{
	std::strstream	vString;

	vString << v.x << " " << v.y << " " << v.z << " " << v.w << " ";
	vString << '\0';

	return vString.str();
}
std::string ToText( const Vector3& v )
{
	std::strstream	vString;

	vString << v.x << " " << v.y << " " << v.z << " ";
	vString << '\0';

	return vString.str();
}
std::string ToText( const Matrix44& v )
{
	return "\t" + ToText( v.a )  + "\n\t\t" + ToText( v.b ) + "\n\t\t" + ToText( v.c )+ "\n\t\t" + ToText( v.d ) + "\n\t\t";
}

void TerrainMapGenerator::SaveIntervalMap( Matrix44& texTransfrom )
{
	std::string		baseTilePath = m_outputPath == "NotSet" ? m_tControl.GetTilePath() : m_outputPath;
	std::string		fileName = "intervalImage.dds";
	std::string		occFileName = "ambientOcclusionImage.dds";
	std::string		heightName = "heightImage.dds";
	std::string		tilePath;

	if ( !m_tControl.IsGenerated() )
	{
		m_terrainHieghts->CalculateIntervalMap();
		if ( m_createAmbient )
		{
			m_terrainHieghts->CalculateNormalMaps();
			m_terrainHieghts->CalculateAmbientOcclusion();
		}
		m_tControl.SetGenerated();
		//texTransfrom = CreateHeightMapFromTiles();
	}
	
	int offset = m_terrainHieghts->GetWidth() / 3;
	int width = offset;

	if ( m_tiles < 3 )
	{
		width = m_terrainHieghts->GetWidth();
		offset = 0;
	}

	grcImage* IntervalMapImage = m_terrainHieghts->MakeIntervalTexture( width, offset );
	tilePath =  baseTilePath + "\\" + fileName;

	IntervalMapImage->SaveDDS( tilePath.c_str() );

	Displayf( "\nSaved File %s \n ", tilePath.c_str());

	IntervalMapImage->Release();

	std::string addition = "\nIntervalTex \n{ \n\tgrcTexture " + fileName + "\n}\n";

	if ( m_createAmbient )
	{
		grcImage* occMapImage = m_terrainHieghts->MakeAmbOccTexture( width, offset );
		tilePath = baseTilePath +  "\\" + occFileName;

		occMapImage->SaveDDS( tilePath.c_str() );

		Displayf( "\nSaved File %s \n ", tilePath.c_str());

		occMapImage->Release();
		addition += "\nAmbientOccTex \n{ \n\tgrcTexture " + occFileName + "\n}\n";
	}

	bool m_createLightOcculsionTexture = true;
	if ( m_createLightOcculsionTexture )
	{
		std::string		lightOccFileName = "LightOcclusionImage.dds";
		grcImage* occMapImage = m_terrainHieghts->MakeLightOccTexture( width, offset );
		tilePath = baseTilePath +  "\\" + lightOccFileName;

		occMapImage->SaveDDS( tilePath.c_str() );

		Displayf( "\nSaved File %s \n ", tilePath.c_str());

		occMapImage->Release();
		addition += "\nLightOccTex \n{ \n\tgrcTexture " + lightOccFileName + "\n}\n";
	}

	if ( m_createHeightTexture )
	{
		float HeightScale;
		float HieghtMin;

		grcImage* heightMapImage = m_terrainHieghts->MakeHeightTexture( width, offset, HieghtMin, HeightScale );
		tilePath = baseTilePath +  "\\" + heightName;

		heightMapImage->SaveDDS( tilePath.c_str() );

		Displayf( "\nSaved File %s \n ", tilePath.c_str());

		heightMapImage->Release();

		
		addition += "\nHeightTex \n{ \n\tgrcTexture " + heightName + "\n}\n";

		std::strstream	str;
		str << "\nHeightScale \n{ \n\tfloat " << HeightScale << "\n}\n";
		str << "\nHeightMin \n{ \n\tfloat " << HieghtMin << "\n}\n";
		str <<'\0';
		addition += str.str();
	}
	bool m_createPRTTexture = true;
	if ( m_createPRTTexture )
	{
		
		std::string		lightOccFileName = "prtOcclusionImage.dds";
		grcImage* occMapImage = m_terrainHieghts->MakePrtTexture( width, offset );
		tilePath = baseTilePath +  "\\" + lightOccFileName;

		occMapImage->SaveDDS( tilePath.c_str() );

		Displayf( "\nSaved File %s \n ", tilePath.c_str());

		occMapImage->Release();
		addition += "\nprtOccTex \n{ \n\tgrcTexture " + lightOccFileName + "\n}\n";
	}

	bool m_writeRawFloatFile = true;
	if ( m_writeRawFloatFile  )
	{
		std::string rawFileName = baseTilePath +  "\\" + "HeightField.raw";
		m_terrainHieghts->WriteRawFile( rawFileName.c_str() );
	}
	// add transfrom to sva file as well
	addition += "\nIntervalShadowHeightMapProjection \n{ \n\tMatrix44  ";
	addition += ToText( texTransfrom  );
	addition +=  "\n}\n";

	// update SVA file for shader if required
	m_tControl.UpdateShaderFiles(  addition , m_outputPath );
}

}

#endif // MESH_LIBRARY
