// 
// rage_ray/triIntersect/triintersect.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "triintersect.h"
#include "math.h"
#include "vector/geometry.h"

#if MESH_LIBRARY


using namespace rage;


void TriIntersect::SetTri( const Vector3& v1, const Vector3& v2, const Vector3& v3 )
{
	// init precomp
	Vector3 A = v1;
	Vector3 B = v2;
	Vector3 C = v3;
	Vector3 c = B - A;
	Vector3 b = C - A;
	Vector3 m_N;
	
	m_N.Cross(  b, c );

	int u, v;
	if ( fabs( m_N.x ) > fabs( m_N.y))
	{
		if ( fabs( m_N.x ) > fabs( m_N.z )) k = 0; else k = 2;
	}
	else
	{
		if ( fabs( m_N.y ) > fabs( m_N.z )) k = 1; else k = 2;
	}
	u = (k + 1) % 3;
	v = (k + 2) % 3;

	Assert( m_N[k] != 0.0f );  // degenerate
	// precomp
	float krec = 1.0f / m_N[k];
	nu = m_N[u] * krec;
	nv = m_N[v] * krec;
	nd = m_N.Dot( A ) * krec;
	// first line equation
	float reci = 1.0f / (b[u] * c[v] - b[v] * c[u]);
	bnu = b[u] * reci;
	bnv = -b[v] * reci;
	// second line equation
	cnu = c[v] * reci;
	cnv = -c[u] * reci;
	// finalize normal
	au = A[u], av = A[v];
}
const float Eplision = 0.0001f;

unsigned int modulo[] = { 0, 1, 2, 0, 1 };
int TriIntersect::Intersect( Ray& ray, float& dist, float& u, float& v  )
{
	
#define ku modulo[k + 1]
#define kv modulo[k + 2]

	Vector3 O = ray.GetOrigin();
	Vector3 D = ray.GetDirection();

	const float lnd = 1.0f / (D[k] + nu * D[ku] + nv * D[kv]);
	const float t = (nd - O[k] - nu * O[ku] - nv * O[kv]) * lnd;
	if (!( dist > t && t > 0)) return MISS;

	float hu = O[ku] + t * D[ku] - au;
	float hv = O[kv] + t * D[kv] - av;
	float beta = u = hv * bnu + hu * bnv;
	float gamma = v = hu * cnu + hv * cnv;

	if (beta < -Eplision) return MISS;
	if (gamma < -Eplision) return MISS;
	if ((u + v) > (1.0 + Eplision )) return MISS;
	dist = t;

	return HIT;
	//return ( D.Dot( m_N ) > 0) ? INPRIM:HIT;  // do we need this?
}

bool TriIntersect::IntersectBox( const Vector3& boxMin, const Vector3& boxMax  )
{
	return geomBoxes::TestTriangleToAlignedBox( m_tri->GetVertex( 0 ).Pos,  m_tri->GetVertex( 1 ).Pos,  m_tri->GetVertex(2 ).Pos , boxMin, boxMax);
}

void TriIntersect::CalculateRange( float& p1, float& p2, int axias )
{
	Vector3 pos = m_tri->GetVertex( 0 ).Pos;
	p1 = pos[ axias];
	p2 = pos[ axias];
	for ( int i = 1; i < 3; i++ )
	{
		pos =  m_tri->GetVertex( i ).Pos;
		p1 = Min( pos[ axias], p1 );
		p2 = Max( pos[ axias], p2 );
	}
}

#endif // MESH_LIBRARY
