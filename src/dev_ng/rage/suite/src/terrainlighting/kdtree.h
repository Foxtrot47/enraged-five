
#ifndef RAGE_KDTREE_H
#define RAGE_KDTREE_H


namespace rage
{
#include "triintersect.h"

// to allow for easily changing the primitive type
typedef TriIntersect	Primitive;

// -----------------------------------------------------------
// Object list helper class
// -----------------------------------------------------------

class ObjectList
{
public:
	ObjectList() : m_Primitive( 0 ), m_Next( 0 ) {}
	~ObjectList() { delete m_Next; }
	void SetPrimitive( Primitive* a_Prim ) { m_Primitive = a_Prim; }
	Primitive* GetPrimitive() { return m_Primitive; }
	void SetNext( ObjectList* a_Next ) { m_Next = a_Next; }
	ObjectList* GetNext() { return m_Next; }
private:
	Primitive* m_Primitive;
	ObjectList* m_Next;
};


// -----------------------------------------------------------
// KdTree class definition
// -----------------------------------------------------------

class MManager;
class Aabb;


class KdTreeNode
{
public:
	KdTreeNode() : m_Data( 6 ) {};
	void SetAxis( int axis ) { m_Data = (m_Data & 0xfffffffc) + axis; }
	int GetAxis() { return m_Data & 3; }
	void SetSplitPos( float pos ) { m_Split = pos; }
	float GetSplitPos() { return m_Split; }
	void SetLeft( KdTreeNode* left ) { m_Data = (unsigned long)left + (m_Data & 7); }
	KdTreeNode* GetLeft() { return (KdTreeNode*)(m_Data&0xfffffff8); }
	KdTreeNode* GetRight() { return ((KdTreeNode*)(m_Data&0xfffffff8)) + 1; }
	void Add( Primitive* prim );
	bool IsLeaf() { return ((m_Data & 4) > 0); }
	void SetLeaf( bool leaf ) { m_Data = (leaf)?(m_Data|4):(m_Data&0xfffffffb); }
	ObjectList* GetList() { return (ObjectList*)(m_Data&0xfffffff8); }
	void SetList( ObjectList* list ) { m_Data = (unsigned long)list + (m_Data & 7); }
private:
	// int m_Flags;
	float m_Split;
	unsigned long m_Data;
};

struct SplitList
{
	float splitpos;
	int n1count, n2count;
	SplitList* next;
};


class KdTree
{
public:
	KdTree();
	~KdTree();
	void Build( mshMesh* mesh  );
	KdTreeNode* GetRoot() { return m_Root; }
	void SetRoot( KdTreeNode* root ) { m_Root = root; }

	// tree generation
	void InsertSplitPos( float a_SplitPos );
	void Subdivide( KdTreeNode* node, Aabb&	bound, int depth, int prims );

	// memory manager
	static void SetMemoryManager( MManager* MM ) { s_MManager = MM; }
private:
	int AddPrimitives(  mshMesh& mesh ,Vector3& bMin, Vector3& bMax );

	KdTreeNode* m_Root;
	SplitList* m_SList, *m_SPool;
public:
	static MManager* s_MManager;
};


}

#endif
