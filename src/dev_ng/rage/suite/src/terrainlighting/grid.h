

// 
// grid/grid.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#ifndef RAGE_GRID_H
#define RAGE_GRID_H
#include <string>
#include <string.h>
#include <hash_map>
#include <vector>

#include "mesh/mesh.h"

#if MESH_LIBRARY

namespace rage
{

class rmcDrawable;
//class mshMesh;

// TITLE: Grid Control code
// PURPOSE:
//		A grid stores allows access to the terrain tiles for the game world
class Grid
{
public:

	Grid( const char* pathName )
		: m_loadPath( pathName )
	{
	}

	//
	// PURPOSE:
	//		Parses a configuration file which states which models are which tiles
	//
	void Load( const char* configFile	// configuration file to read
		);

	//
	// PURPOSE:
	//		Returns the rmcDrawable object for that tile.
	//	NOTES:
	//		Returns NULL if there is no tile with this code
	rmcDrawable*		GetTile( const char* tileCode );
	rmcDrawable*		GetTile( int x, int y )
	{
		return GetTile( GetBattleShipCode( x, y ).c_str());
	}

	//
	// PURPOSE:
	//		Returns the mshMesh object for that tile.
	//	NOTES:
	//		Returns NULL if there is no tile with this code
	//
	std::vector<mshMesh*>		GetMesh( const char* tileCode );
	std::vector<mshMesh*>		GetMesh( int x, int y )
	{
		return GetMesh( GetBattleShipCode( x, y ).c_str());
	}

	//
	// PURPOSE:
	//		Returns the mshMesh object for the entity at the given path
	//	NOTES:
	//		Returns NULL if there is no tile with this code
	//
	std::vector<mshMesh*>		GetMeshFromPath( const char* path );
	//
	// PURPOSE:
	//		Returns the file path to where the models on the tile is stored
	//	NOTES:
	//		Returns NULL if there is no tile with this code
	//
	std::string GetPath( int x, int y )
	{
		std::string		fileName;
		GetFileName( GetBattleShipCode( x, y ).c_str() , fileName );
		std::string		path = fileName.substr( 0, fileName.length() - strlen( "entity.type" ));
		return path;
	}

	//
	// PURPOSE:
	//		Adds the addition string to the file with the given suffix for the tile
	//	NOTES:
	//		Returns false if could not append it.
	//
	bool AppendToTileFile( int x, int y,					//	code for tile to append stuff
		const std::string& addition,			//  text to add to the file
		const std::string& suffix				// file suffix of which to add
		)
	{
		return AppendToTileFile( GetBattleShipCode( x, y ).c_str() , addition, suffix , "NotSet");
	}

	bool AppendToTileFile( const char* tileCode,					//	code for tile to append stuff
							const std::string& addition,			//  text to add to the file
							const std::string& suffix,				// file suffix of which to add
							const std::string& path
							);
	

private:
	std::string 	GetBattleShipCode( int x, int y );;
	std::string		decodeNameToKey( const std::string& name );
	bool parseFileName( const char* configFile,  const char* line, std::string& name  );


	bool FindFileNameInFile( const char*  fileName, std::vector<std::string>& meshName, const std::string& suffix );
	bool GetFileName( const char* tileCode, std::string& fileName );


	bool AddToFile( const std::string& fileName, const std::string& addition, const std::string& newFile );

	typedef std::hash_map<std::string, std::string>		codeLookupType;
	codeLookupType		m_lookup;

	std::string		m_loadPath;
};


void unitTestGrid();

}

#endif // MESH_LIBRARY


#endif
