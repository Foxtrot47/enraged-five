
// 
// height map.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// TITLE: Hieghtmap control for terrain.
// PURPOSE:
//		This is a file which is useful for manipulating heightfields for use a terrain
//


#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include "vector/vector3.h"
#include "grmodel/shader.h"
#include "grcore/vertexdecl.h"
#include "mesh/mesh.h"
#include "atl/array.h"

namespace rage
{

	//	fwd references
	class grcVertexBuffer;
	class grcIndexBuffer;
	class grmShader;		
	class grcImage;
	class grcTexture;

class SphericalVis2ndOrder
{
public:
	SphericalVis2ndOrder()
	{
		val.Zero();
	}
	int GetNumberCoefficients()	const		{ return 4; }

	void Add( const Vector3& direction, float solidAngle )
	{
		const float fNormalisation = PI*16/17.0f;
		const float fConst1 = square(0.282095f) * fNormalisation * 1;
		const float fConst2 = square(0.488603f) * fNormalisation * (2/3.0f);

		Vector4	 weights(  fConst2, fConst2,fConst2, fConst1 );
		Vector4	spherical( direction.x, direction.y, direction.z, 1.0f );
		val += weights * spherical * solidAngle;
	}

	Vector4 Get()	{ return val; }
private:
	Vector4	val;

};

//
//	PURPOSE
//		Creates a heightmap for use as terrain
//	NOTES
//		Creates a normal map, ambient occlusion map and interval map for lighting the terrain heightmap
//		
class HeightMap
{

public:
	HeightMap( int width = 128, int hieght = 128 , bool edgeTiles = false)
		: m_width( width ), 
			m_hieght( hieght ), 
			m_occPower( 1.0f ),
			m_haveEdgeTiles( edgeTiles ),
			m_isBuilt( false ),
			m_heights( rage_new float[ width * hieght] ),
			m_lightOcclusion( rage_new float[ width * hieght] ),
			m_normals( rage_new Vector3[ width * hieght ]),
			m_intervalMap( rage_new Vector3[ width * hieght ]),
			m_prtMap( rage_new SphericalVis2ndOrder[ width * hieght ]),
			m_sunArc( 1.0f, 0.0f, 0.0f ),
			m_IntervalTexMap( 0 )
			
	{
	}


	~HeightMap()
	{
		delete[] m_heights;
		delete[] m_normals;
		delete[] m_intervalMap;
		delete[] m_lightOcclusion;
		delete[] m_prtMap;
	}
	// accessors
	float	GetVal( int x, int y )	const	
	{
		Assert( x>=0 && x < m_width );
		Assert( y>=0 && y < m_hieght );
		return m_heights[ x + y * m_width ];	
	}
	Vector3 GetNorm( int x, int y ) const	{		return m_normals[ x + y * m_width ];	}
	int		GetWidth()				const 	{		return m_width; }
	int		GetHeight()			const 	{		return m_hieght; }
	float	GetWidthF()			const	{ return static_cast<float>( m_width ); }
	float	GetHeightF()			const { return static_cast<float>( m_hieght ); }
	bool	IsBuilt()				const { return m_isBuilt; }

	float	GetSplodgeValue( int x, int y );

	void	SetSunArc( const Vector3& sunArc ) 	
	{		
		Assert( sunArc.y == 0.0f );
		m_sunArc.Normalize( sunArc );	
	}
	//
	//	PURPOSE
	//		Creates the values for the hieghtmap using the passed in functor. The functor
	//		should match stl style.
	//	
	template<class GENERATOR>
		void Generate( GENERATOR& gen,			// functor to create height values
		bool keepLighting		// if true will not generate new lighting maps
		)
	{
		gen.Begin();
		for( int x = 0; x < m_width; x++ )
		{
			for( int y = 0; y < m_hieght; y++ )
			{
				m_heights[ x + y * m_width ] = gen( (float)x /m_width  , (float)y /m_hieght  );
			}
		}
		gen.End();
		if ( !keepLighting )
		{
			BuildLighting();
			return;
		}
	}

	//
	//	PURPOSE
	//		Converts from a direction in the XY plane to sun arc plane
	//
	Vector3	ConvertToSunArc( const Vector3& arc )
	{
		Vector3 dir =  Vector3( -arc.x * m_sunArc.x, arc.y, -arc.x * m_sunArc.z);
		dir.Normalize();
		return dir;
	}
	//
	//	PURPOSE
	//		Creates the lighting maps for the heightmap ( normal / amb occlus and interval )
	//
	void BuildLighting();

	void CalculateIntervalMap();

	void CalculateAmbientOcclusion();

	void CalculateNormalMaps();

	//
	//	PURPOSE
	//		Adds a cone to the height map so as to create shadow and stuff from trees
	//
	void AddTreeSplodge( const Vector3& position, 
								float radius, 
								float height );

	//
	//	PURPOSE
	//		Sets a value on the heightfield.
	//
	void SetDirect( float x, float y, float z )
	{
		int Ix = int( x );
		int Iy = int( y );
		m_heights[ Ix + Iy * m_width ] = Max( z , m_heights[ Ix + Iy * m_width ] );
	}

	//
	//	PURPOSE
	//		Clears the hieght field to sero hieghts
	//
	void Clear()
	{
		for (int i = 0; i < ( m_width * m_hieght ) ; i++ )
		{
			m_heights[ i] = 0.0f;
			m_lightOcclusion[ i] = 0.0f;
		}
	}

	//
	//	PURPOSE
	//		Uses additive blending to output occlusion from trees map
	//
	void SetLightOcc( int x, int y, float v )	{	m_lightOcclusion[ x + y * m_width ] += v;	}
	//
	//	PURPOSE
	//		Returns the visibility of a point using the interval maps
	//
	float GetVisibility( int x, int y, const Vector3& LightDir, float soft )
	{
		if ( LightDir.x < 0.0f )
		{
			return  Clamp( ( LightDir.y - GetInterval( x, y ).x ) * soft, 0.0f, 1.0f  );
		}
		return Clamp( ( LightDir.y - GetInterval( x, y ).y ) * soft , 0.0f, 1.0f );
	}

	Vector2 GetInterval( int x, int y )
	{
		return Vector2( m_intervalMap[ x + y * m_width ].x, m_intervalMap[ x + y * m_width ].y );
	}

	void SetWaterPlane( float h );

	float GetAmbOcculsion( int x, int y ) const
	{
		return m_intervalMap[ x + y * m_width ].z;
	}

	grcImage* 		MakeAmbOccTexture( int width, int offset );
	grcImage* 		MakeIntervalTexture( int width, int offset ,  bool smooth = false );
	grcImage*		MakeHeightTexture( int width, int offset , float& minimum, float& scale ) const;
	grcImage* 		MakeLightOccTexture( int width, int offset );
	grcImage* 		MakePrtTexture( int width, int offset );

	void			CheckIntervalTexture(  grcImage* IntervalMapImage, int width, int offset );
	

	grcTexture*			GetNormalMap() { return m_NormalTexMap; }
	grcTexture*			GetIntervalMap() { return m_IntervalTexMap; }



	void				UpdateTextures();

	void	SetOccPower( float f )		{ Assert( f > 0.0f && f < 512.0f ); m_occPower = f; }

	void				WriteRawFile( const char* filename ) const;
private:

	Vector3	intervalFlippedY( int x, int y );

	//
	//	PURPOSE
	//		Calculates a normal for a hieghtmap
	//
	Vector3 GetNormal( int x, int y );

	//
	//	PURPOSE
	//		Calculates the maximum angle along the height field in a certain direction
	//
	float MaxAngle( const Vector3& direction, const Vector3& pos, int maxSteps, float& steps ,  float startAngle  = 0.0f );
	
	float MaxAngleNorm( const Vector3& direction, const Vector3& pos, int maxSteps, float& steps , float startAngle , const Vector3& norm );

	//
	// PURPOSE
	//		Creates and maintains internal textures for interval and normal maps
	//
	void CreateTextures();

	//
	// PURPOSE
	//		Calculates the ambient occlusion for the hieght field by using multiple interval style samples
	//
	float AmbientOcclusionSample( int x, int y, const Vector3& UNUSED_PARAM( normal ) , SphericalVis2ndOrder& prt );
	//
	// PURPOSE
	//		Creates the interval map by using looking up into the rows horizontally
	//
	
	Vector3 CalculateInterval( int x, int y );
	Vector3 CalculateIntervalGeneral( const Vector3& arc, int x, int y );


	int m_width;
	int m_hieght;


	float										m_occPower;

	bool										m_haveEdgeTiles;
	bool										m_isBuilt;
	float*										m_heights;
	float*										m_lightOcclusion;
	Vector3*									m_normals;
	Vector3*									m_intervalMap;
	SphericalVis2ndOrder*						m_prtMap;

	Vector3					m_sunArc;

	grcTexture*				m_NormalTexMap;
	grcTexture*				m_IntervalTexMap;
};

#if MESH_LIBRARY
//
// PURPOSE
//	Renders a mesh full of triangles into a hieght map
//
void ScanMesh( HeightMap& h  ,mshMesh& mesh,  Matrix34& bound );
//
// PURPOSE
//	Renders an idividual triangle into the height map
//
void ScanTri( HeightMap& h, const Matrix34& transform, const mshVertex& v1, const mshVertex& v2, const mshVertex& v3 );

#endif // MESH_LIBRARY

//
//	PURPOSE
//		Controls the construction and rendering of a grid.
//	NOTES
//		It uses a heightfield class to create the values for each of it's vertices
//		This has not been designed for explicit game use but could be used.
//
class HeightFieldRender
{
public:
	HeightFieldRender( int offset = 0 ) : m_isSetup( false ), m_offset( offset )
	{}
	~HeightFieldRender();

	
	//
	//	PURPOSE 
	//		Sets the grid to the given width and hieght of polygons.
	//	NOTES
	//		Can use a lot of CPU if the there are a lot of points in the mesh.
	//
	void Init( int size,		// width and height of grid
				const char* shaderName		// name of shader to use to render heightfield
				);
	//
	//	PURPOSE 
	//		Reads the hieghtmap and reads the normal, position, amb occlusion and interval map information from the
	//		heightmap
	//	
	void UpdateVertices( HeightMap* terrain		// heightmap to read information from
								);
	//
	//	PURPOSE 
	//		shoots out to the GPU the grid of polygons with the supplied shader
	//	
	void Draw();

	// grcEffect&	GetEffect()		{ return m_Shader->GetEffect(); }
	grmShader*	GetShader()		{ return m_Shader; }
private:

	bool					m_isSetup;
	int						m_gridSize;
	int						m_offset;

	grcVertexDeclaration*	m_VertexDecl;
	grcVertexBuffer*		m_VertexBuffer;
	grcIndexBuffer*			m_IndexBuffer;
	grmShader*				m_Shader;
};

}
#endif

