
// 
// terrainmapgenerattor.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// PURPOSE:
//		This links the models with the height map and converts models to heightmaps and saves the necesary textures
//


#ifndef TERRAINMAPGENERATOR_H
#define TERRAINMAPGENERATOR_H

#include "vector/vector3.h"
#include "tileControl.h"
#include "heightmap.h"
#include "bank/bank.h"
#include <string>

#if MESH_LIBRARY


namespace rage
{

// fwd references
class sysParam;

//
//	PURPOSE
//		Creates a heightmap from a mesh and uses it to generate texture maps
//	NOTES
//		This links the models with the height map and converts models to heightmaps and saves the necesary textures
//	
class TerrainMapGenerator : public	datBase
{
public:

	TerrainMapGenerator()
	{}
	~TerrainMapGenerator()
	{
		delete m_terrainHieghts;
	}

	//
	//	PURPOSE
	//		Returns the hieght map generated from a given terrain tile
	//	
	HeightMap*	heightMap()		{ return m_terrainHieghts;	}

	void Init( int gridSize,		// size of hieght map grid to create
				int tiles,
				bool doAmbientOcc = false,
				bool doHeightTexture = false
		);

	//
	//	PURPOSE
	//		Generates a hieghtmap from the given terrain tile and it's neighbours
	//	
	Matrix44 CreateHeightMapFromTiles();
	void LoadTile( int tileIndex, const sysParam& param );


	void InitTiles()
	{
		// update control of the various tiles
		m_tControl.Init();
	}

	template<class GENERATOR>
		void Generate( GENERATOR& gen )
	{
		m_terrainHieghts->Generate( gen, true );
	}
	void SetOutputPath( const std::string& outPutFile )	{ m_outputPath = outPutFile; }
	void SetSunArc( const Vector3& arc )				{ m_terrainHieghts->SetSunArc( arc ); }

	void SetAmbientOcclusionPower( float f )			{ 	m_terrainHieghts->SetOccPower( f ); }

	void SaveIntervalMap(  Matrix44& texTransfrom );

	void Draw( const Vector3& lightDirection, float shadowSoftness )
	{
		m_tControl.Draw( lightDirection, shadowSoftness );		
	}


	void AddTreeSplodge( const Vector3& position , float radius, float height  )
	{
		Matrix34 trans = m_tControl.GetWorldToHeightMapTransform( *m_terrainHieghts, m_tiles );
		Vector3 pos = position;
		pos.Dot( trans );
		Vector3 pos2 = position + Vector3( radius, height, 0.0f );
		pos2.Dot( trans );

		m_terrainHieghts->AddTreeSplodge( pos, fabs( pos2.x - pos.x) ,  fabs( pos2.z - pos.z) );
	} 

	void SetWaterPlane( float h )
	{
		Matrix34 trans = m_tControl.GetWorldToHeightMapTransform( *m_terrainHieghts, m_tiles );
		Vector3 pos( 0.0f, h, 0.0f );
		pos.Dot( trans );
		m_terrainHieghts->SetWaterPlane( pos.z );

	}
#if __BANK
	void AddWidgets( bkBank& bank) 
	{
		bank.AddButton( "Make Interval Map", datCallback(MFA( TerrainMapGenerator::SaveIntervalMap ), this ));
		m_tControl.AddWidgets( bank );	
	}
#endif // __BANK

private:
	HeightMap*			m_terrainHieghts;
	TileControl			m_tControl;
	std::string			m_outputPath;
	int					m_tiles;

	bool				m_createAmbient;
	bool				m_createHeightTexture;
};

};

#endif // MESH_LIBRARY


#endif
