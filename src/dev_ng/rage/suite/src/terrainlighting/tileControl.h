// 
// tileControl.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// TITLE:	TileControl
// PURPOSE:
//		Controls and displays tiles of terrain for use with terrain texture map generation

#ifndef TILECONTROL_GEN_H
#define TILECONTROL_GEN_H
#include "grid.h"
#include "heightmap.h"
#include "math.h"

#if MESH_LIBRARY

namespace rage
{
// fwd ref
class bkBank;


//
//	PURPOSE
//		Loads a tile and 8 surrounding tiles and allows for drawing them and conversion to height field representatnio
//	
class TileControl : public datBase
{
public:
	TileControl() 
		:	
		m_gridX( 0 ),
		m_gridY( 0 ),
		m_draw( false ),
		m_gameGrid( "\\\\rscha\\rdr2\\ASSETS\\level\\territory" ),
		
		m_guy( 0 ),
		m_MiddleTilePath( "NotSet" )
	{
	
		m_isGenerated = false;
	}

	~TileControl()
	{
		for ( int i = 0; i < NumberOfTiles; i++ )
		{
			for( mshMeshList::iterator itor = m_tilesToDraw[i].begin();
				itor != m_tilesToDraw[i].end();
				++itor )
			{
				delete *itor;
			}
		}
	}
	//
	//	PURPOSE
	//		Sets up the grid paths
	//	
	void Init()
	{
		m_gameGrid.Load( "\\\\rscha\\rdr2\\Art\\Worlds\\territory\\sw\\groupFiles\\swTerrain.group" );
	}

	//
	//	PURPOSE
	//		Loads 9 tiles in as mesh files and the centre one as a drawable model.
	//	
	void LoadCurrentTiles();

	bool LoadTile( int tileIndex,		// load index of tile to set
					const char* tilePath	// path and filename of terrain tile
					);

	
	//
	//	PURPOSE
	//		controls whether the heightmap has been generated for the newly loaded tile pieaces
	//
	bool IsGenerated()		const	{ return m_isGenerated; }
	void SetGenerated()				{ m_isGenerated = true ; }

	std::string	GetTilePath()		{		return m_gameGrid.GetPath( m_gridX, m_gridY );	}


	//
	//	PURPOSE
	//		Appends a texture name to the shader specifications
	//
	void UpdateShaderFiles( std::string& intervalFileName , const std::string& path );

	//
	//	PURPOSE
	//		Generates a heightmap from the terrain by using the ScanMesh functions
	//	SEEALSO
	//		ScanMesh
	//
	void GenerateHeightMap( HeightMap& h , Matrix44& texTransform, int tileNumber = 3 );


	//
	//	PURPOSE
	//		Renders out the central tile with the terrain shader and the 9 mesh tiles if selected
	//
	void Draw( const Vector3& LightDirection, float shadowSoftness );

#if __BANK
		//
		//	PURPOSE
		//		Allows modification of the current grid position
		//
		void AddWidgets( bkBank& bank);
#endif

public:

	Matrix34 GetTransformUniformSize( mshMesh* mesh );
	Matrix34 GetXZTextureProjection( mshMesh* mesh );
	Matrix34 GetXZTextureProjection( std::vector<mshMesh*> mesh );
	
	
	rmcDrawable*	GetTileModel( int x, int y, Matrix34& bound , Matrix34& texProjection );


	Matrix34 GetWorldToHeightMapTransform( HeightMap& h, int tileNumber );

private:

	static const int	NumberOfTiles = 9;
	static const int	MiddleTile = 4;

	int					m_gridX;
	int					m_gridY;

	bool				m_useDirect;

	// grid stuff
	bool							m_draw;
	Grid							m_gameGrid;

	typedef	std::vector<mshMesh*>	mshMeshList;
	mshMeshList						m_tilesToDraw[ NumberOfTiles ];

	rmcDrawable*		m_guy;
	Matrix34			m_guyBound;
	Matrix34			m_guyProjection;

	bool				m_isGenerated;

	std::string			m_MiddleTilePath;

};

}

#endif // MESH_LIBRARY

#endif
