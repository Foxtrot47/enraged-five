#if 0

class DepthToHeightMap : public datBase
{
public:
	// constructor
	DepthToHeightMap( int size );

	// destructor
	virtual	~DepthToHeightMap(void);

	//
	// PURPOSE
	//	creates and loads the shaders
	// PARAMS
	//	path - the path to load the shaders from.  If it == NULL, use the currently set path.
	//
	void InitShaders(const char *path=NULL); 

	// if the device changes or the user wants a different resolution: 
	// delete and create new shadow map
	void	CreateRenderTargets(void);
	void	DeleteRenderTargets(void);

	//
	// PURPOSE
	//	renders into the shadowmap (calls overloaded RenderShadowCasters()
	//  to render objects into shadowmap.  
	// PARAMS
	//	clearBuffers - if true, this function clears the back buffer
	//  and the z buffer.  If false, you should call clear yourself
	//  (otherwise you'll get corruption from the rendered shadowmap
	//  because it uses the same area of memory as the backbuffer or 
	//	z-buffer.)
	//
	void Begin(  const Matrix34& BoundMatrix, float height );
	void End();

	// shows the shadow render target
	void ShowRenderTargets();

	void SetGlobals();

	void ConvertToFloatArray( float* arr );

private:
	void ShowShadowMap(grcTexture * texture, 
		grcEffectTechnique technique,					// technique in FX file
		int DestX, int DestY, int SizeX, int SizeY);	


	// clear a render target to a color
	void ClearRT(grcRenderTarget * target, const Color32 * color);

private:
	grcRenderTarget *m_DepthBuffer;

	grcRenderTarget *m_DepthMap;

	// The shader used for rendering to depth targets
	grmShader * m_DepthShader;				

	// shader parameter handles
	grcEffectGlobalVar m_DepthTexture0ID;
	grcEffectGlobalVar m_DepthTexture1ID;


	grcEffectTechnique m_CopyTechnique;	// technique that is used to view the depth map

	bool				m_ShowShadowMap;
	int					m_TextureSize;
	const grcViewport*	m_oldVp;
};


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
DepthToHeightMap::DepthToHeightMap( int size ) :
m_DepthBuffer(NULL),
m_DepthMap(NULL),
m_ShowShadowMap(false),
m_TextureSize( size )
{
	m_DepthTexture0ID = grcegvNONE;
	m_DepthTexture1ID = grcegvNONE;

}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void DepthToHeightMap::InitShaders(const char *path)
{	
	if (path)
		ASSET.PushFolder(path);

	// PC - a R32F shadow map is used to store the depth values
	const char *shaderName = "rage_shadowdepth";
	m_DepthShader = grmShaderFactory::GetInstance().Create(shaderName);
	m_DepthShader->Load(shaderName, 0 ,0);

	// depth texture
	m_DepthTexture0ID = grcEffect::LookupGlobalVar("DepthTexture0");

	// depth texture
	m_DepthTexture1ID = grcEffect::LookupGlobalVar("DepthTexture1");

	// grab handel to copy technique
	m_CopyTechnique = m_DepthShader->GetEffect().LookupTechnique("CopyRT");	

	if (path)
		ASSET.PopFolder();
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
DepthToHeightMap::~DepthToHeightMap(void)
{
	delete m_DepthShader;
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void DepthToHeightMap::CreateRenderTargets()
{
	int rezx,rezy;

	//Create the offscreen target to hold depth information (Note: on XENON it can be the actual z-buffer)
	grcTextureFactory::CreateParams cp;

	cp.Multisample = 0;
	// PC uses a R32F render target to store the shadow map + a z-buffer for rendering
	rezx = m_TextureSize;
	rezy = m_TextureSize;
	m_DepthBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("Depth Buffer", grcrtDepthBuffer, rezx, rezy, 32);
	cp.Format = grctfR32F;
	m_DepthMap = grcTextureFactory::GetInstance().CreateRenderTarget("m_LightDepthBuffer0", grcrtPermanent, rezx, rezy, 32, &cp);
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void DepthToHeightMap::DeleteRenderTargets(void)
{
	if (m_DepthBuffer)
	{
		m_DepthBuffer->Release();
		m_DepthBuffer = NULL;	
	}

	if(m_DepthMap)
	{
		m_DepthMap->Release();
		m_DepthMap = NULL;	
	}
}


//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void DepthToHeightMap::Begin(  const Matrix34& BoundMatrix, float height )
{
	// grab current viewport and save it
	m_oldVp = grcViewport::GetCurrent();

	// lock R32F shadow map + depth buffer
	grcTextureFactory::GetInstance().LockRenderTarget(0, m_DepthMap, m_DepthBuffer);

	// Clear R32F shadow map + depth buffer
	GRCDEVICE.Clear(true, Color32(0.0f, 0.0f, 0.0f, 1.0f), true, 1.0f, false);

	grcViewport vp;

	// keep viewport class happy by sending down an identity matrix
	vp.SetWorldMtx(M34_IDENTITY);
	// send down the camera view matrix
	vp.SetCameraMtx(BoundMatrix);

	vp.Ortho( -0.5f, 0.5f , -0.5f, 0.5f, -height , height );


	//grcWorldMtx( BoundMatrix);
	// set new view port
	grcViewport::SetCurrent(&vp);

	// force depth shader
	grmModel::SetForceShader(m_DepthShader);

	SetGlobals();

	m_DepthShader->BeginDraw(grmShader::RMC_DRAW,true);
	m_DepthShader->Bind();

}



//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void DepthToHeightMap::End()
{
	m_DepthShader->UnBind();
	m_DepthShader->EndDraw();

	// undo the force of the shader
	grmModel::SetForceShader(0);

	grcTextureFactory::GetInstance().UnlockRenderTarget(0);

	// clear back buffer and z buffer:
	GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.8f,0.f),true,1.0f,0);

	// set old viewport
	grcViewport::SetCurrent( m_oldVp );
}

//---------------------------------------------------------------------------
//  
//
//---------------------------------------------------------------------------
void DepthToHeightMap::SetGlobals()
{
	if(m_DepthTexture0ID!=grcegvNONE)
		grcEffect::SetGlobalVar(m_DepthTexture0ID, m_DepthMap);

	// texel size - in the shader this is Shadowmap_PixelSize0
	//if(m_PixelBlurID!=grcegvNONE)
	//	grcEffect::SetGlobalVar(m_PixelBlurID, 1.0f / m_TextureSize );

	grcEffect& effect = m_DepthShader->GetEffect();
	grcEffectVar var = effect.LookupVar( "DepthTexture0");
	effect.SetVar( var, m_DepthTexture0ID );


	effect = m_DepthShader->GetEffect();
	var = effect.LookupVar( "Shadowmap_PixelSize0" );
	effect.SetVar( var, 1.0f / m_TextureSize );


}
void DepthToHeightMap::ConvertToFloatArray( float* arr )
{
	grcTextureLock	lock;
	m_DepthMap->Lock();
	if ( m_DepthMap->LockRect( 0,0, lock ) )
	{
		float* ptr = reinterpret_cast<float*>( lock.Base );
		for (int i = 0; i < m_TextureSize * m_TextureSize ; i++ )	// just copy this boyo accross
		{
			*arr++ = *ptr++;
		}
	}
	m_DepthMap->UnlockRect( lock );
	m_DepthMap->Unlock();
};

//---------------------------------------------------------------------------
// shows the shadow map as a quad on the screen for debugging purposes
// should be relative to the screen size
//---------------------------------------------------------------------------
void DepthToHeightMap::ShowShadowMap(grcTexture * texture, 
									 grcEffectTechnique technique,					// technique in FX file
									 int DestX, int DestY, int SizeX, int SizeY)								
{
	Color32 white(1.0f,1.0f,1.0f,1.0f);

	grcEffect::SetGlobalVar(m_DepthTexture0ID, texture); 

	// blit
	m_DepthShader->Blit(DestX,			// x1 - Destination base x
		DestY,			// y1 - Destination base y
		SizeX,			// x2 - Destination opposite-corner x
		SizeY,			// y1 - Destination opposite-corner y
		0.1f,			// z - Destination z value.  Note that the z value is expected to be in 0..1 space
		0.0f,			// u1 - Source texture base u (in normalized texels)
		0.0f,			// v1 - Source texture base v (in normalized texels)
		1.0f,			// u2 - Source texture opposite-corner u (in normalized texels)
		1.0f,			// v2 - Source texture opposite-corner v (in normalized texels)
		(Color32)white,		// color - reference to Packed color value
		technique);

	grcEffect::SetGlobalVar(m_DepthTexture0ID, (grcTexture*)grcTexture::None);
}

//---------------------------------------------------------------------------
// visualize render targets
//
//---------------------------------------------------------------------------
void DepthToHeightMap::ShowRenderTargets()
{
	const int OriginX = 32;
	const int OriginY = 96;
	const int RectWidth = 164;
	const int RectHeight = 164;

	ShowShadowMap(m_DepthMap, m_CopyTechnique,  
		OriginX,					// top left x
		OriginY,					// top left y 
		OriginX + RectWidth,		// bottom right x
		OriginY + RectHeight);		// bottom right y
}

#endif
