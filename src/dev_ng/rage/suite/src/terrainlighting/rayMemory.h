
#ifndef RAYMEMORY_H
#define RAYMEMORY_H

namespace rage 
{

	class ObjectList;
	class KdTreeNode;
	class MManager
	{
	public:
		MManager();
		ObjectList* NewObjectList();
		void FreeObjectList( ObjectList* list );
		KdTreeNode* NewKdTreeNodePair();
	private:
		ObjectList* m_OList;
		char* m_KdArray, *m_ObjArray;
		KdTreeNode* m_KdPtr;
		ObjectList* m_ObjPtr;
		int m_KdUsed;
	};

}; 

#endif
