
#include "math.h"
#include "vector/vector3.h"
#include "vector/geometry.h"
#include "mesh/mesh.h"
#include "triintersect.h"
#include "rayMemory.h"
#include "kdtree.h"
#include "tracer.h"


namespace rage
{

struct kdstack
{
	KdTreeNode* node;
	float t;
	Vector3 pb;
	int prev, dummy1, dummy2;
};


Tracer::Tracer()
{
	m_kdtree = rage_new KdTree();
	KdTree::SetMemoryManager( rage_new MManager() );
	m_Mod = rage_new int[64];
	m_Mod = (int*)((((unsigned long)m_Mod) + 32) & (0xffffffff - 31));
	m_Mod[0] = 0, m_Mod[1] = 1, m_Mod[2] = 2, m_Mod[3] = 0, m_Mod[4] = 1;
	m_Stack = rage_new kdstack[64];
	m_Stack = (kdstack*)((((unsigned long)m_Stack) + 32) & (0xffffffff - 31));
}

Tracer::~Tracer()
{
	delete m_kdtree;
	delete m_Mod;
	delete m_Stack;
}


//  PURPOSE
//		Finds the nearest intersection in a KdTree for a ray
//
int Tracer::FindNearest( Ray& ray, float& dist, const TriInfo*& prim, float& u, float& v )
{
	float tnear = 0, tfar = dist, t;
	int retval = 0;
	Vector3 D = ray.GetDirection();
	Vector3 O = ray.GetOrigin();
	
	// init stack
	int entrypoint = 0, exitpoint = 1;
	// init traversal
	KdTreeNode* farchild, *currnode;
	currnode = GetKdTree()->GetRoot();
	m_Stack[entrypoint].t = tnear;
	if (tnear > 0.0f) m_Stack[entrypoint].pb = O + D * tnear;
	else m_Stack[entrypoint].pb = O;
	m_Stack[exitpoint].t = tfar;
	m_Stack[exitpoint].pb = O + D * tfar;
	m_Stack[exitpoint].node = 0;

	// traverse kd-tree
	while (currnode)
	{
		while (!currnode->IsLeaf())
		{
			float splitpos = currnode->GetSplitPos();
			int axis = currnode->GetAxis();
			if (m_Stack[entrypoint].pb[axis] <= splitpos)
			{
				if (m_Stack[exitpoint].pb[axis] <= splitpos)
				{
					currnode = currnode->GetLeft();
					continue;
				}
				if (m_Stack[exitpoint].pb[axis] == splitpos)
				{
					currnode = currnode->GetRight();
					continue;
				}
				currnode = currnode->GetLeft();
				farchild = currnode + 1; // GetRight();
			}
			else
			{
				if (m_Stack[exitpoint].pb[axis] > splitpos)
				{
					currnode = currnode->GetRight();
					continue;
				}
				farchild = currnode->GetLeft();
				currnode = farchild + 1; // GetRight();
			}
			t = (splitpos - O[axis]) / D[axis];
			int tmp = exitpoint++;
			if (exitpoint == entrypoint) exitpoint++;
			m_Stack[exitpoint].prev = tmp;
			m_Stack[exitpoint].t = t;
			m_Stack[exitpoint].node = farchild;
			m_Stack[exitpoint].pb[axis] = splitpos;
			int nextaxis = m_Mod[axis + 1];
			int prevaxis = m_Mod[axis + 2];
			m_Stack[exitpoint].pb[nextaxis] = O[nextaxis] + t * D[nextaxis];
			m_Stack[exitpoint].pb[prevaxis] = O[prevaxis] + t * D[prevaxis];
		}
		ObjectList* list = currnode->GetList();
		float d = m_Stack[exitpoint].t;
		while (list)
		{
			Primitive* pr = list->GetPrimitive();
			int result;
			m_Intersections++;
			result = pr->Intersect( ray, d , u, v  );
			if ( result)
			{
				retval = result;
				dist = d;
				prim = pr->GetInfo();
			}
			list = list->GetNext();
		}
		if (retval) return retval;
		entrypoint = exitpoint;
		currnode = m_Stack[exitpoint].node;
		exitpoint = m_Stack[entrypoint].prev;
	}
	return 0;
}

#define EPSILON		0.00001f
// -----------------------------------------------------------
// Engine::FindOccluder
// Finds any occluder between the origin and a light source
// -----------------------------------------------------------
int Tracer::FindOccluder( Ray& ray, float& dist  )
{
	float tnear = EPSILON,  t;
	Vector3 O  = ray.GetOrigin();
	Vector3 D = ray.GetDirection();

	// init stack
	int entrypoint = 0, exitpoint = 1;
	// init traversal
	KdTreeNode* farchild, *currnode = GetKdTree()->GetRoot();
	m_Stack[entrypoint].t = tnear;
	m_Stack[entrypoint].pb = O;
	m_Stack[exitpoint].t = dist;
	m_Stack[exitpoint].pb = O + D * dist;
	m_Stack[exitpoint].node = 0;
	// traverse kd-tree

	while (currnode)
	{
		while (!currnode->IsLeaf())
		{
			float splitpos = currnode->GetSplitPos();
			int axis = currnode->GetAxis();
			if (m_Stack[entrypoint].pb[axis] <= splitpos)
			{
				if (m_Stack[exitpoint].pb[axis] <= splitpos)
				{
					currnode = currnode->GetLeft();
					continue;
				}
				if (m_Stack[exitpoint].pb[axis] == splitpos)
				{
					currnode = currnode->GetRight();
					continue;
				}
				currnode = currnode->GetLeft();
				farchild = currnode + 1; // GetRight();
			}
			else
			{
				if (m_Stack[exitpoint].pb[axis] > splitpos)
				{
					currnode = currnode->GetRight();
					continue;
				}
				farchild = currnode->GetLeft();
				currnode = farchild + 1; // GetRight();
			}
			t = (splitpos - O[axis]) / D[axis];
			int tmp = exitpoint;
			if (++exitpoint == entrypoint) exitpoint++;
			m_Stack[exitpoint].prev = tmp;
			m_Stack[exitpoint].t = t;
			m_Stack[exitpoint].node = farchild;
			m_Stack[exitpoint].pb[axis] = splitpos;
			int nextaxis = m_Mod[axis + 1];
			int prevaxis = m_Mod[axis + 2];
			m_Stack[exitpoint].pb[nextaxis] = O[nextaxis] + t * D[nextaxis];
			m_Stack[exitpoint].pb[prevaxis] = O[prevaxis] + t * D[prevaxis];
		}
		ObjectList* list = currnode->GetList();
		float d = dist; // m_Stack[exitpoint].t;
		float u;
		float v;
		while (list)
		{
			m_Intersections++;
			if (list->GetPrimitive()->Intersect( ray, d , u, v))
			{
				return 1;
			}
			list = list->GetNext();
		}
		entrypoint = exitpoint;
		currnode = m_Stack[exitpoint].node;
		exitpoint = m_Stack[entrypoint].prev;
	}
	return 0;
}
}
