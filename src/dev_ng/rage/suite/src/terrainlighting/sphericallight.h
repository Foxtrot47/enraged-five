



class SphericalLight
{
public:
	void SphericalLight()
	{
		float  fNormalisation = PI*16/17.0f;
		fConst1 = square(0.282095f) * fNormalisation * 1;
		fConst2 = square(0.488603f) * fNormalisation * (2/3.0f);
		fConst3 = square(1.092548f) * fNormalisation * (1/4.0f);
		fConst4 = square(0.315392f) * fNormalisation * (1/4.0f);
		fConst5 = square(0.546274f) * fNormalisation * (1/4.0f);
	}

	void Reset()
	{
		for (int i = 0; i < 9; i++)
		{
			SHCoeff[i].Zero();
		}
	}
	//
	//	PURPOSE Given a light information will project it into SH 
	//
	 void CalculateCoefficents( Vector3 color, Vector3 direction )
	{
		Vector3 d2 = dirn * dirn;
		float f1 = 3.0f * d2.z - 1.0f;
		float f2 = d2.x - d2.y;
		Vector3 dirnSecond = normal.xzy * normal.zyx;

		SHCoeff[0] += color * fConst1;
		SHCoeff[1] += color * fConst2 * direction.x;
		SHCoeff[2] += color * fConst2 * direction.y;
		SHCoeff[3] += color * fConst2 * direction.z;
		SHCoeff[4] +=  color * fConst3 * dirnSecond.x ;	
		SHCoeff[5] += color * fConst3 *  dirnSecond.y ;
		SHCoeff[6] += color * fConst3 *  dirnSecond.z ;
		SHCoeff[7] += color * fConst4 * f1;	
		SHCoeff[8] += color * fConst5 * f2;
	}

	 Vector3* GetCoeffs( Vector3 coeffs[9])
	{
		return SHCoeff;
	}
	/*****************************************************************************
	* Function Name  : CalculateL
	* Inputs		  : l, m
	* Returns		  : SHCoeffStruct
	* Description    : Given the degree of the SH this function will calculate
	an SH coefficient from a environment map. The input arguments
	l and m decide which coeficient to calculate
	*****************************************************************************/
	static SHCoeffStruct CalculateL(int width, int l, int m, Functor<> Sample )
	{
		float xp,yp; /* xplane and yplane are coordinates of the 2D environment map */
		int i, j;
		float theta, phi; /* spherical coordiantes */
		float x, y, z;		/* cartesian coordinates */
		SHCoeffStruct integral = {0,0,0}; /* 3 because we have r g and b */
		float Ylm;

		for(i=0; i<SizeOf2DEnvMap; i++)
		{
			for(j=0; j<SizeOf2DEnvMap; j++)
			{
				/* build 2D coordinates */
				xp = (float)i/(float)SizeOf2DEnvMap;
				yp = (float)j/(float)SizeOf2DEnvMap;

				/* build spherical coordinates out of 2D coordinates */
				theta = 2.0f * (float)acos(sqrt(1.0f - xp));
				phi = 2.0f * PI * yp;

				/* build the 3D coordiantes out of spherical coordinates */
				x = (float)(sin(theta)*cos(phi));
				y = (float)(sin(theta)*sin(phi));
				z = (float)cos(theta);

				CalculateCoefficents(  Sample(i, j ), Vector3 direction , PI/ (width * width));
			}
		}

		/* normalize the integral */
		integral= PI * integral/ ( width * width);
		return integral;
	}


	void CalculateFromEnvironmentMap( Functor<> getPixelColor, int width , )
	{
		int l, m;
		int index;
		char string[128];

		InitCoefficients();

		SizeOf2DEnvMap = EnvMap->sizeX;

		for(l=0; l<3; ++l) 
		{
			for(m=-l; m<=l; ++m) 
			{
				index = l*(l+1)+m;
				SHCoeff[index] = CalculateL( getPixelColor, l, m); 
				sprintf(string, "L%d%d = %f %f %f\n", l, m, SHCoeff[index].red, SHCoeff[index].green, SHCoeff[index].blue);
				OutputDebugString(string);
			}
		}

	}

private:
	Vector3	SHCoeff[9];
};

/*
float3 SphericalLightCoefficients [9]: SphericalLightCoefficients;

float3 GetSphericalLightValue( float3 dirn )
{
	float3 d2 = dirn * dirn;
	float f1 = 3.0f * d2.z - 1.0f;
	float f2 = d2.x - d2.y;
	float3 dirnSecond = dirn.xzy * dirn.zyx;

	float3 result = SphericalLightCoefficients[1] * dirn.x + SphericalLightCoefficients[0];
	result = SphericalLightCoefficients[2] * dirn.y + result;
	result = SphericalLightCoefficients[3] * dirn.z + result;
	result = SphericalLightCoefficients[4] * dirnSecond.x + result;
	result = SphericalLightCoefficients[5] * dirnSecond.y + result;
	result = SphericalLightCoefficients[6] * dirnSecond.z + result;
	result = SphericalLightCoefficients[7] * f1 + result;
	result = SphericalLightCoefficients[8] * f2 + result;
}
*/
