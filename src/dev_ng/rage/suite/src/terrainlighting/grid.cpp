

// 
// grid/grid.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "grid.h"
#include "rmcore/drawable.h"
#include "mesh/serialize.h"
#include <string>
#include <fstream>
#include <stdlib.h>

#if MESH_LIBRARY


namespace rage
{




std::string 	Grid::GetBattleShipCode( int x, int y )
{
	// x becomes letter
	std::string		accross;

	accross.push_back( (char)( x + 'a') );
	AssertMsg( x < 26,  "crap ran out of space to decode tile names" );
	char	down[4];

	//_itoa( y, down, 10 );
	down[1] = (char)( y % 10 ) + '0';
	down[0] = (char)((int)y / 10 )+ '0';
	down[2] = '\0';

	std::string gcode = accross + "_" + std::string( down );
	return gcode;
}

#if __PPU
bool isValidCharacter(  const char c ) { return isalnum( c ); }
#else
bool isValidCharacter( const char c ) { return __iscsym( c ); }
#endif

bool Grid::FindFileNameInFile( const char*  fileName, std::vector<std::string>& meshName, const std::string& suffix )
{
	std::ifstream	file( fileName );
	if ( !file )
	{
		Errorf(" Entity file for Grid %s is not found ", fileName );
		return false;
	}

	bool result = false;
	while ( file )
	{
		char				line[512];
		file.getline( line, 512 );

		std::string			ln( line );

		std::string::size_type	idx = ln.find(suffix);

		if ( idx != std::string::npos )
		{
				
			std::string startLine = ln.substr(0, idx + suffix.length() );

			int cnt = (int ) idx - 1;
			while( cnt > -1 && isValidCharacter( startLine[cnt] ) )
			{
				cnt--;
			}
			meshName.push_back( startLine.substr( cnt + 1) );

			result = true;
		}
	}
	return result;
}


bool Grid::GetFileName( const char* tileCode, std::string& fileName )
{
	codeLookupType::iterator		itor = m_lookup.find( tileCode );
	if ( itor == m_lookup.end() )
	{
		return false;
	}

	fileName = m_loadPath + "\\" + itor->second + "\\entity.type";
	return true;
}

#if MESH_LIBRARY

std::vector<mshMesh*>		Grid::GetMesh( const char* tileCode )
{
	std::string					fileName;
	std::vector<mshMesh*>		tiles;
	if ( GetFileName( tileCode, fileName  ) )
	{
		std::vector<std::string> meshFileName;
		if ( FindFileNameInFile( fileName.c_str(), meshFileName , ".mesh") )
		{
			while ( !meshFileName.empty() )
			{
				mshMesh &tile = *(rage_new mshMesh);
				std::string		path = fileName.substr( 0, fileName.length() - strlen( "entity.type" ));
				path += meshFileName.back();
				bool result = SerializeFromFile( path.c_str(), tile, "mesh");
				if (  result == false ) 
				{
					Errorf("Can't load mesh file %s", meshFileName.back().c_str());
					return tiles;
				}
				tiles.push_back( &tile );
				meshFileName.pop_back();
			}
		}
	}
	return tiles;
}
std::vector<mshMesh*>				Grid::GetMeshFromPath( const char* path )
{
	std::string			fileName = std::string( path ) + "\\entity.type";
	std::vector<mshMesh*>		tiles;
	std::vector<std::string> meshFileName;

	if ( FindFileNameInFile( fileName.c_str(), meshFileName , ".mesh") )
	{
		while ( !meshFileName.empty() )
		{
			mshMesh &tile = *(rage_new mshMesh);
			std::string		path = fileName.substr( 0, fileName.length() - strlen( "entity.type" ));
			path += meshFileName.back();
			bool result = SerializeFromFile( path.c_str(), tile, "mesh");
			if (  result == false ) 
			{
				Errorf("Can't load mesh file %s", meshFileName.back().c_str());
				return tiles;
			}
			tiles.push_back( &tile );
			meshFileName.pop_back();
		}
	}
	return tiles;
}

#endif // MESH_LIBRARY

bool parseFileToString( std::string& result, const std::string& fileName )
{
	std::ifstream	in( fileName.c_str() );
	if ( !in )
	{
		return false;
	}
	while ( in )
	{
		char				line[512];
		in.getline( line, 512 );
		result += std::string( line ) + std::string( "\n" );
	}
	return true;
}


bool Grid::AddToFile( const std::string& fileName, const std::string& addition, const std::string& newFile )
{
	// first check
	std::string		parser;
	
	if ( !parseFileToString( parser, fileName ) )  // problem reading file
	{
		return false;
	}
	
	if (  parser.find( addition ) == std::string::npos )		// already there
	{
		// ok add it
		parser += addition;
	}
	
	
	// and write it out to the file
	std::ofstream	out( newFile.c_str() );
	Assert( out );
	out << parser;
	Displayf( "\nModified File %s \n and placed in %s \n", fileName.c_str(), newFile.c_str() );
	return true;
}
bool  Grid::AppendToTileFile( const char* tileCode, const std::string& addition, 
								const std::string& suffix, const std::string& tPath  )
{
	std::string			fileName = tileCode + std::string( "\\entity.type");

	if ( tPath == "NotSet" )
	{
		if (!GetFileName( tileCode, fileName  ) )
		{
			return false;
		}
	}
	
	std::vector<std::string> svaFileName;
	if ( FindFileNameInFile( fileName.c_str(), svaFileName , suffix) )
	{
		std::string		path = fileName.substr( 0, fileName.length() - strlen( "entity.type" ));
		path += svaFileName[0];

		if ( tPath == "NotSet")
		{
			return AddToFile( path, addition , path);
		}
		else
		{
			std::string newFile = tPath + std::string("\\") + svaFileName[0];
			return AddToFile( path, addition, newFile );
		}
	}

	return false;
}
rmcDrawable*		Grid::GetTile( const char* tileCode )
{
	std::string			fileName;
	if ( GetFileName( tileCode, fileName  ) )
	{
		std::string path = ASSET.GetPath(0);
		std::string newAssetPath = fileName.substr( 0, fileName.find_last_of('\\') );

		ASSET.SetPath( newAssetPath.c_str() );

		rmcDrawable*		tile = rage_new rmcDrawable();	
		tile->Load( fileName.c_str() );

		ASSET.SetPath( path.c_str() );

		return tile;
	}
	return 0;
}
void Grid::Load( const char* configFile )
{
	std::ifstream	file( configFile );
	if ( !file )
	{
		Errorf(" Configuration file for Grid %s is not found ", configFile );
		return;
	}
	while ( file )
	{
		char				line[512];
		std::string			name;
		file.getline( line, 512 );

		if ( parseFileName( configFile, line , name ) )
		{
			std::string key = decodeNameToKey( name );
			AssertMsg( m_lookup.count( key ) == 0, "Name is duplicated in configuration file for grid " );
			m_lookup[ key ] = name;
		}
	}
}


std::string		Grid::decodeNameToKey( const std::string& name )
{
	Assert( name.length() == 6 );
	std::string key = name.substr( 0, 4 );
	Assert( std::isalpha( key[0] ) &&  key[1] == '_' && std::isalnum( key[2] ) && std::isalnum( key[3] ) );
	return key;
}
bool Grid::parseFileName( const char* configFile,  const char* line, std::string& name  )
{
	int trimVal = 0;

	
	while ( std::isspace( line[ trimVal] ) )
	{
		trimVal++;
	}
	if ( line[ trimVal] == '#' || line[ trimVal] =='\n' || line[ trimVal] == '\0' )
	{
		return false;		
	}
	std::string	val( &line[ trimVal ] );

	
	// should be a valid file name
	std::string::size_type startIdx = val.find_last_of( '\\' );
	if ( startIdx == std::string::npos )
	{
		Errorf(" Incorrect syntax for file in configuration file %s \n line %s ", configFile, line );
		return false;
	}
	std::string::size_type endIdx = val.find_last_of( '.' );
	if ( endIdx == std::string::npos )
	{
		Errorf(" Incorrect syntax for file in configuration file %s \n line %s ", configFile, line );
		return false;
	}		
	// now get the file name
	startIdx++;		// remove the slash;

	name = val.substr( startIdx, endIdx - startIdx );
	return true;
}


void unitTestGrid()
{
	Grid		testGrid( "\\\\rscha\\rdr2\\ASSETS\\level\\territory" );

	testGrid.Load( "\\\\rscha\\rdr2\\Art\\Worlds\\territory\\sw\\groupFiles\\swTerrain.group" );

	rmcDrawable*	test = testGrid.GetTile( 10, 10 );
	Assert( test == NULL );

	test  = testGrid.GetTile( 16, 16 );

	Assert( test != NULL );
	delete test;
}

}

#endif // MESH_LIBRARY
