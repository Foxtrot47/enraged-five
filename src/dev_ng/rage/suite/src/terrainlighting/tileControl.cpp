

// 
// tileControl.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "tileControl.h"



#include "mesh/grcrenderer.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/combo.h"
#include "rmcore\drawable.h"

#include "system/xtl.h"

#if MESH_LIBRARY


namespace rage
{

template<class T>
void DeleteAll( std::vector<T*> list )
{
	for (  typename std::vector<T*>::iterator itor = list.begin();
									itor != list.end();
									++itor )
	{
		delete *itor;
	}
}
Matrix34 TileControl::GetTransformUniformSize( mshMesh* mesh )
{
	// setup modelview matrix to convert from world scale so it is down the x axias
	const Matrix34& boundBoxMatrix = mesh->GetBoundBoxMatrix();

	Vector3 scale = mesh->GetBoundBoxSize();
	Matrix34	scaledBound = boundBoxMatrix;
	scale.y = scale.x;// since this is a patch in x z keep uniform transformation in the y
	//		scaledBound.Scale( scale  );

	Matrix34 boundInverse;
	boundInverse.Inverse( scaledBound );

	Vector3 translate =  Vector3( 1.0f, 0.0f, 1.0f );
	boundInverse.Translate( translate );

	// shift so top corner is 0
	return boundInverse;
}
Matrix34 TileControl::GetXZTextureProjection( mshMesh* mesh )
{
	// setup modelview matrix to convert from world scale so it is down the x axias
	const Matrix34& boundBoxMatrix = mesh->GetBoundBoxMatrix();

	Vector3 scale = mesh->GetBoundBoxSize();
	Matrix34	scaledBound = boundBoxMatrix;
	scale.y = scale.x;// since this is a patch in x z keep uniform transformation in the y
	scaledBound.Scale( scale );

	scaledBound.RotateLocalX( -PI / 2.0f ); // rotate by 90 degrees


	Matrix34 boundInverse;
	boundInverse.Inverse( scaledBound );


	return boundInverse;
}
Matrix34 TileControl::GetXZTextureProjection( std::vector<mshMesh*> mesh )
{
	// find the bounding box of the mesh
	Vector3 minBound( FLT_MAX, FLT_MAX, FLT_MAX );
	Vector3 maxBound( -FLT_MAX, -FLT_MAX, -FLT_MAX );
	for ( std::vector<mshMesh*>::iterator itor = mesh.begin();
											itor != mesh.end();
											++itor )
	{
		Vector3 max;
		Vector3 min;

		(*itor)->ComputeBoundingBox( min, max );

		minBound.Min( minBound, min );
		maxBound.Max( maxBound, max );
	}

	// now create bound matrix
	Vector3 translation;
	Vector3 scale;
	translation = ( maxBound + minBound ) / 2.0f;
	scale = ( maxBound - minBound ) / 2.0f;
	scale.y = 1.0f;
	translation.y = 0.0f;

	Matrix34 scaledBound;
	scaledBound.Identity();
	scaledBound.Translate( translation );
	scaledBound.Scale( scale );
	
	scaledBound.RotateLocalX( -PI / 2.0f ); // rotate by 90 degrees

	Matrix34 boundInverse;
	boundInverse.Inverse( scaledBound );
	return boundInverse;
}

rmcDrawable*	TileControl::GetTileModel( int x, int y, Matrix34& bound , Matrix34& texProjection )
{
	// get bound ( I know this is really slow but it's the only way I know how to do it currently )
	std::vector<mshMesh*>			boundMesh = m_gameGrid.GetMesh( x, y );
	if ( boundMesh.empty() )
	{
		return 0;
	}

	bound = GetTransformUniformSize( boundMesh[0] );		
	texProjection = GetXZTextureProjection( boundMesh[0] );

	// translate by 0.5f so goes from 0 - 1.0f
	Vector3 translate =  Vector3( 1.0f, 1.0f, 0.0f );
	texProjection.Translate( translate );

	for( mshMeshList::iterator itor = boundMesh.begin();
		itor != boundMesh.end();
		++itor )
	{
		delete *itor;
	}
	

	return m_gameGrid.GetTile( x, y );
}


void TileControl::UpdateShaderFiles( std::string& addition, const std::string& path )
{
	
	if ( path == "NotSet" )
	{
		m_gameGrid.AppendToTileFile( m_gridX, m_gridY,	addition, ".sva" );
	}
	else
	{
		Assert( m_MiddleTilePath != "NotSet" );

		m_gameGrid.AppendToTileFile( m_MiddleTilePath.c_str(),	addition, ".sva" , path );
	}
}

Matrix34 TileControl::GetWorldToHeightMapTransform( HeightMap& h, int tileNumber )
{
	// setup modelview matrix to convert from world scale so it is down the x axias	
	Matrix34	boundInverse = GetXZTextureProjection( m_tilesToDraw[ MiddleTile ]);

	// scale by texture size
	Vector3		scaleTex = Vector3( h.GetWidthF() * 0.5f  / (float)tileNumber ,h.GetHeightF() * 0.5f  /(float)tileNumber ,  1.0f );
	Matrix34	scaleMtx;

	if ( tileNumber == 2)
	{
		Vector3		scaleTex2 = Vector3( h.GetWidthF() * 0.75f  / (float)tileNumber ,h.GetHeightF() * 0.75f  /(float)tileNumber , 1.0f  );
		scaleMtx.MakeTranslate( scaleTex2  * (float)tileNumber  );
	}
	else
	{
		scaleMtx.MakeTranslate( scaleTex  * (float)tileNumber  );
	}

	scaleMtx.MakeScale( scaleTex );

	// scale to fit three tiles into texture
	// make them fit
	boundInverse.Dot( scaleMtx );

	return boundInverse;
}
void TileControl::GenerateHeightMap( HeightMap& h , Matrix44& texTransform, int tileNumber )
{
	if ( m_tilesToDraw[ MiddleTile ].empty())
	{
		return;
	}

	Matrix34 boundInverse = GetWorldToHeightMapTransform( h, tileNumber );
	h.Clear();

	for ( int i = 0; i < NumberOfTiles; i++ )
	{
		if ( !m_tilesToDraw[i].empty())
		{
			for( mshMeshList::iterator itor = m_tilesToDraw[i].begin();
										itor != m_tilesToDraw[i].end();
										++itor )
			{
				ScanMesh( h, *(*itor), boundInverse );
			}
			
		}
	}

	// scale to 0..1
	boundInverse.ScaleFull( Vector3( 1.0f/h.GetWidthF(), 1.0f/h.GetWidthF(), 1.0f/h.GetWidthF()  ));
	texTransform.FromMatrix34( boundInverse );

}

void TileControl::Draw( const Vector3& LightDirection, float shadowSoftness )
{
	if ( m_guy )
	{
		grmShader &effect = m_guy->GetShaderGroup().GetShader( 0 );
		grcEffectVar var = effect.LookupVar( "DirectionOfSun", false );
		if (var != grcevNONE)
		{
			effect.SetVar( var, LightDirection );
		}

		var = effect.LookupVar( "SunShadowSoftness" , false );
		if (var != grcevNONE)
		{
			effect.SetVar( var, shadowSoftness );
		}

		var = effect.LookupVar( "HeightMapProjection" , false );
		Matrix44	transposeMtx;
		Matrix44	guy44;

		guy44.FromMatrix34( m_guyProjection);
		if (var != grcevNONE)
		{
			effect.SetVar( var, guy44 );
		}

		m_guy->Draw( m_guyBound, (u32)4,0 );
		m_guy->Draw( m_guyBound, (u32)0,0 );
		return;
	}
	if ( !m_draw || m_tilesToDraw[ MiddleTile ].empty())
	{
		return;
	}

	const Matrix34& boundBoxMatrix = m_tilesToDraw[ MiddleTile ][0]->GetBoundBoxMatrix();
	Matrix34 boundInverse;
	boundInverse.Inverse( boundBoxMatrix );

	grcWorldMtx( boundInverse );

	for ( int i = 0; i < NumberOfTiles; i++ )
	{
		for( mshMeshList::iterator itor = m_tilesToDraw[i].begin();
			itor != m_tilesToDraw[i].end();
			++itor )
		{

			mshRendererGrc r;
			// now draw the mesh
			(*itor)->Draw(r);
		}
	}
	Matrix34	mtxIdent;
	mtxIdent.Identity();
	grcWorldMtx( mtxIdent );
}
bool TileControl::LoadTile( int tileIndex, const char* tilePath	)
{
	// bypass the grid stuf 

	if ( tileIndex == 0 )		// is the centre tile
	{
		m_MiddleTilePath = tilePath;
	}
	const int remap[ 9 ] = { 4, 0, 1, 2, 5, 8, 7, 6, 3 };
	tileIndex = remap[ tileIndex ];

	m_tilesToDraw[ tileIndex ] = m_gameGrid.GetMeshFromPath( tilePath );
	return !m_tilesToDraw[ tileIndex ].empty();
}

void TileControl::LoadCurrentTiles()
{
	int cnt = 0;
	for ( int x = m_gridX -1; x < ( m_gridX + 2 ); x++ )
	{
		for ( int y = m_gridY -1; y < ( m_gridY + 2); y++ )
		{
			x = Clamp( x, 0, 26);
			y = Clamp( y, 0, 26);

			DeleteAll(  m_tilesToDraw[ cnt ] );
			m_tilesToDraw[ cnt++ ] = m_gameGrid.GetMesh( x, y );
		}
	}
	delete m_guy;

	m_guy = GetTileModel( m_gridX, m_gridY,  m_guyBound, m_guyProjection );
	m_isGenerated = false;
}


#if __BANK
void TileControl::AddWidgets( bkBank& bank) 
{
	bank.PushGroup("Tile Control");

	bank.AddSlider("GridX", &m_gridX, 0, 26, 1 );
	bank.AddSlider("GridY", &m_gridY, 0, 26, 1 );

	bank.AddToggle("Draw Mesh",&m_draw );

	bank.AddButton( "Load Tile", datCallback(MFA(TileControl::LoadCurrentTiles ), this ) );

	bank.PopGroup();
}
#endif

};

#endif // MESH_LIBRARY
