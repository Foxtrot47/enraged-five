//
// crclip/clipdictionary.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRCLIP_CLIPDICTIONARY_H
#define CRCLIP_CLIPDICTIONARY_H

#include "clip.h"

#include "cranimation/animdictionary.h"

namespace rage
{

class crClipLoader;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Simple clip container class
class crClipDictionary : public pgBaseRefCounted
{
public:

	// PURPOSE: Default constructor
	crClipDictionary();

	// PURPOSE: Resource constructor
	crClipDictionary(datResource&);

	// PURPOSE: Destructor
	virtual ~crClipDictionary();

	// PURPOSE: Shutdown, free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Placement
	DECLARE_PLACE(crClipDictionary);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Off-line resourcing version
	static const int RORC_VERSION = 4 + crClip::RORC_VERSION + crAnimDictionary::RORC_VERSION;

#if CR_DEV
	// PURPOSE: Allocate and load dictionary, from clips contained in list file
	// PARAMS:
	// listFilename - filename of a text file, containing a list of clip filenames (one per line)
	// loader - optional derived clip loader class, to control loading of clips (default NULL)
	// subLoader - optional derived animation loader class, to control loading of any animations within clips (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: Newly allocated clip dictionary, or NULL if error encountered
	static crClipDictionary* AllocateAndLoadClips(const char* listFilename, crClipLoader* loader=NULL, crAnimLoader* subLoader=NULL, bool baseNameKeys=false);

	// PURPOSE: Allocate and load dictionary, from clip file names provided
	// PARAMS:
	// clipFilenames - array of clip filenames
	// loader - optional derived clip loader class, to control loading of clips (default NULL)
	// subLoader - optional derived animation loader class, to control loading of any animations within clips (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: Newly allocated clip dictionary, or NULL if error encountered
	static crClipDictionary* AllocateAndLoadClips(const atArray<atString>& clipFilenames, crClipLoader* loader=NULL, crAnimLoader* subLoader=NULL, bool baseNameKeys=false);

	// PURPOSE: Load dictionary, from clips contained in list file
	// PARAMS:
	// listFilename - filename of a text file, containing a list of clip filenames (one per line)
	// loader - optional derived clip loader class, to control loading of clips (default NULL)
	// subLoader - optional derived animation loader class, to control loading of any animations within clips (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: true - load successful, false - error encountered
	bool LoadClips(const char* listFilename, crClipLoader* loader=NULL, crAnimLoader* subLoader=NULL, bool baseNameKeys=false);

	// PURPOSE: Load dictionary, from clip file names provided
	// PARAMS:
	// clipFilenames - array of clip filenames
	// loader - optional derived clip loader class, to control loading of clips (default NULL)
	// subLoader - optional derived animation loader class, to control loading of any animations within clips (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: true - load successful, false - error encountered
	bool LoadClips(const atArray<atString>& clipFilenames, crClipLoader* loader=NULL, crAnimLoader* subLoader=NULL, bool baseNameKeys=false);
#endif // CR_DEV

    // PURPOSE:	Loads a clip dictionary saved out as a resource.
	// The resource is created by rage/base/tools/srorc.
    // PARAMS:	
	// dictionaryFilename - Name of the dictionary; must not have an extension, and "_cdt"
    // is automatically appended to it.
    // RETURNS:	Pointer to clip dictionary object, if load was successful.
    static crClipDictionary* LoadResource(const char* dictionaryFilename);

	// PURPOSE: Dictionary key
	typedef u32 ClipKey;


	// PURPOSE: Retrieve a clip using a filename
	// PARAMS: filename - clip filename
	// RETURNS: pointer to clip, if found, or NULL if not
	// NOTES: Fast, hashes filename into key then uses map to retrieve
	const crClip* GetClip(const char* filename) const;
	DEPRECATED const crClip* FindClip(const char* filename) const { return GetClip(filename); }

	// PURPOSE: Retrieve a clip using a filename (non-const)
	// PARAMS: filename - clip filename
	// RETURNS: Non-const pointer to clip, if found, or NULL if not
	// NOTES: Fast, hashes filename into key then uses map to retrieve
	crClip* GetClip(const char* filename);
	DEPRECATED crClip* FindClip(const char* filename) { return GetClip(filename); }

	// PURPOSE: Retrieve a clip using a hash key
	// PARAMS: key - hash key
	// RETURNS: pointer to clip, if found, or NULL if not
	// NOTES: Very fast, using key to retrieve clip from map
	const crClip* GetClip(ClipKey key) const;
	DEPRECATED const crClip* FindClip(ClipKey key) const { return GetClip(key); }

	// PURPOSE: Retrieve a clip using a hash key (non-const)
	// PARAMS: key - hash key
	// RETURNS: Non-const pointer to clip, if found, or NULL if not
	// NOTES: Very fast, using key to retrieve clip from map
	crClip* GetClip(ClipKey key);
	DEPRECATED crClip* FindClip(ClipKey key) { return GetClip(key); }

	// PURPOSE: Convert clip filename into hash key
	// PARAMS: filename - clip filename
	// RETURNS: Hash key
	ClipKey ConvertNameToKey(const char* filename) const;

	// PURPOSE: Get number of clips in dictionary
	// RETURNS: Total number of clips stored in dictionary
	u32 GetNumClips() const;

	// PURPOSE: Find clip using index
	// PARAMS: idx - clip index [0..numClips-1]
	// RETURNS: Pointer to clip
	// NOTES: Very slow, iterates through clips to find index
	// Use ForAll instead if enumerating all the clips for better performance
	const crClip* FindClipByIndex(u32 idx) const;

	// PURPOSE: Find clip using index (non-const)
	// PARAMS: idx - clip index [0..numClips-1]
	// RETURNS: Non-const pointer to clip
	// NOTES: Very slow, iterates through clips to find index
	// Use ForAll instead if enumerating all the clips for better performance
	crClip* FindClipByIndex(u32 idx);

	// PURPOSE: Find clip hash key using index
	// PARAMS: idx - clip index [0..numClips-1]
	// RETURNS: Hash key
	// NOTES: Very slow, iterates through clips to find index
	// Use ForAll instead if enumerating all the clips for better performance
	ClipKey FindKeyByIndex(u32 idx) const;


	// PURPOSE: Const callback typedef
	typedef bool (*ConstClipDictionaryCallback)(const crClip* clip, ClipKey key, void* data);

	// PURPOSE:	Execute a specified callback on every item in this dictionary
	// PARAMS:
	// cb - const callback function
	// data - user-specified callback data
	// RETURNS:	true - all callbacks succeeded (or dictionary was empty), false - callback failed
	bool ForAll(ConstClipDictionaryCallback cb, void* data) const;


	// PURPOSE: Non-const callback typedef
	typedef bool (*ClipDictionaryCallback)(crClip* clip, ClipKey key, void* data);

	// PURPOSE:	Execute a specified callback on every item in this dictionary
	// PARAMS:
	// cb - non-const callback function
	// data - user-specified callback data
	// RETURNS:	true - all callbacks succeeded (or dictionary was empty), false - callback failed
	bool ForAll(ClipDictionaryCallback cb, void* data);


	// PURPOSE: Get direct access to internal animation dictionary
	// RETURNS: pointer to animation dictionary
	const crAnimDictionary* GetAnimationDictionary() const;

	// PURPOSE: Get direct access to internal animation dictionary
	// RETURNS: Non-const pointer to animation dictionary
	crAnimDictionary* GetAnimationDictionary();

protected:

	// PURPOSE: Internal function for hashing filename to key
	// PARAMS: name - name to hash
	// RETURNS: Hash key
	// NOTES: Uses atStringHash by default, override to modify behavior
	virtual ClipKey CalcKey(const char* name) const;


	datRef<crAnimDictionary> m_AnimDictionary;
	bool m_AnimDictionaryOwner;

	bool m_BaseNameKeys;

	datPadding<2> m_Padding;

	atMap<ClipKey, datOwner<crClip> >  m_Clips;
};

////////////////////////////////////////////////////////////////////////////////
/*
class crClipDictionaryIdMap
{
	crClipDictionaryIdMap();

	crClipDictionaryIdMap(crClipDictionary&, crClipDictionaryMap*);

	virtual ~crClipDictionaryIdMap();

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();

	// PURPOSE: Placement
	DECLARE_PLACE(crClipDictionaryIdMap);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	void Init(const crClipDictionary&, crClipDictionaryMap*);

	const crClipDictionary& GetDictionary() const;

	const crClipDictionaryMap* GetParent() const;

	typedef u32 ClipGameKey;

	bool AddEntry(ClipGameKey gameKey, const char* clipFilename);

	bool AddEntry(ClipGameKey gameKey, crClipDictionary::ClipKey key);

	const crClip* FindEntry(ClipGameKey gameKey, bool searchParents=true) const;

	u32 GetNumEntries() const;

	void GetEntry(u32 idx, ClipGameKey& gameKey, crClipDictionary::ClipKey& key) const;
};
*/
////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base clip loader class
class crClipLoader
{
public:

	// PURPOSE: Constructor
	crClipLoader();

	// PURPOSE: Destructor
	virtual ~crClipLoader();

	// PURPOSE: Allocate and load clip - override this to implement in derived class
	virtual crClip* AllocateAndLoadClip(const char* clipFilename, crAnimLoader* animLoader) = 0;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Basic clip loader class
class crClipLoaderBasic : public crClipLoader
{
public:

	// PURPOSE: Constructor
	crClipLoaderBasic();

	// PURPOSE: Destructor
	virtual ~crClipLoaderBasic();

	// PURPOSE: Override allocate and load clip function
	virtual crClip* AllocateAndLoadClip(const char* clipFilename, crAnimLoader* animLoader);
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Dictionary clip loader - retrieves from dictionary
class crClipLoaderDictionary : public crClipLoader
{
public:

	// PURPOSE: Constructor
	crClipLoaderDictionary();

	// PURPOSE: Initializing constructor
	// PARAMS: dictionary - pointer to dictionary
	crClipLoaderDictionary(const crClipDictionary* dictionary);

	// PURPOSE: Destructor
	virtual ~crClipLoaderDictionary();

	// PURPOSE: Initializer
	// PARAMS: dictionary - pointer to dictionary
	void Init(const crClipDictionary* dictionary);

	// PURPOSE: Override allocate and load clip function
	virtual crClip* AllocateAndLoadClip(const char* clipFilename, crAnimLoader* animLoader);

protected:
	const crClipDictionary* m_Dictionary;
};

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crClipDictionary::GetClip(const char* clipName) const
{
	return GetClip(ConvertNameToKey(clipName));
}

////////////////////////////////////////////////////////////////////////////////

inline crClip* crClipDictionary::GetClip(const char* clipName)
{
	return GetClip(ConvertNameToKey(clipName));
}

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crClipDictionary::GetClip(ClipKey key) const
{
	const datOwner<crClip>* result = m_Clips.Access(key);
	return result?(*result):NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline crClip* crClipDictionary::GetClip(ClipKey key)
{
	datOwner<crClip>* result = m_Clips.Access(key);
	return result?(*result):NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline crClipDictionary::ClipKey crClipDictionary::ConvertNameToKey(const char* clipName) const
{
	if(m_BaseNameKeys)
	{
		char baseName[RAGE_MAX_PATH];
		ASSET.BaseName(baseName, RAGE_MAX_PATH, ASSET.FileName(clipName));

		return CalcKey(baseName);
	}

	return CalcKey(clipName);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crClipDictionary::GetNumClips() const
{
	return u32(m_Clips.GetNumUsed());
}

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crClipDictionary::FindClipByIndex(u32 idx) const
{
	atMap<ClipKey, datOwner<crClip> >::ConstIterator it = m_Clips.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
	return it.GetData();
}

////////////////////////////////////////////////////////////////////////////////

inline crClip* crClipDictionary::FindClipByIndex(u32 idx)
{
	atMap<ClipKey, datOwner<crClip> >::Iterator it = m_Clips.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
	return it.GetData();
}

////////////////////////////////////////////////////////////////////////////////

inline crClipDictionary::ClipKey crClipDictionary::FindKeyByIndex(u32 idx) const
{
	atMap<ClipKey, datOwner<crClip> >::ConstIterator it = m_Clips.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
	return it.GetKey();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crClipDictionary::ForAll(ConstClipDictionaryCallback cb, void* data) const
{
	bool result = true;

	atMap<ClipKey, datOwner<crClip> >::ConstIterator it = m_Clips.CreateIterator();
	while(it)
	{
		if((result = cb(it.GetData(), it.GetKey(), data)) == false)
		{
			break;
		}
		it.Next();
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crClipDictionary::ForAll(ClipDictionaryCallback cb, void* data)
{
	bool result = true;

	atMap<ClipKey, datOwner<crClip> >::Iterator it = m_Clips.CreateIterator();
	while(it)
	{
		if((result = cb(it.GetData(), it.GetKey(), data)) == false)
		{
			break;
		}
		it.Next();
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////

inline const crAnimDictionary* crClipDictionary::GetAnimationDictionary() const
{
	return m_AnimDictionary;
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimDictionary* crClipDictionary::GetAnimationDictionary()
{
	return m_AnimDictionary;
}

////////////////////////////////////////////////////////////////////////////////

inline crClipDictionary::ClipKey crClipDictionary::CalcKey(const char* name) const
{
	return ClipKey(atStringHash(name));
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CRCLIP_CLIPDICTIONARY_H
