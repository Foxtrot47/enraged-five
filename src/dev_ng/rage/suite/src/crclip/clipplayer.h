//
// crclip/clipplayer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRCLIP_CLIPPLAYER_H
#define CRCLIP_CLIPPLAYER_H

#include "atl/functor.h"
#include "cranimation/animation_config.h"
#include "cranimation/frameaccelerator.h"
#include "data/resource.h"
#include "data/struct.h"
#include "paging/ref.h"

#include "clip.h"


namespace rage
{

class crFrame;
class crFrameCompositor;
class crFrameData;
class crClip;
class crTag;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Simple low level player of clips
class crClipPlayer
{
public:

	// PURPOSE: Default constructor
	crClipPlayer();

	// PURPOSE: Initializing constructor
	// PARAMS: clip - initial clip to play
	crClipPlayer(const crClip* clip);

	// PURPOSE: Resource constructor
	crClipPlayer(datResource&);

	// PURPOSE: Destructor
	~crClipPlayer();

	// PURPOSE: Placement
	DECLARE_PLACE(crClipPlayer);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initialization
	// PARAMS: clip - initial clip to play
	void Init(const crClip* clip);

	// PURPOSE: Free/release dynamic resources
	void Shutdown();


	// PURPOSE: Update player by time step (simple)
	// PARAMS: deltaTime - time step duration (in seconds)
	void Update(float deltaTime);

	// PURPOSE: Type definition of functor to call when update triggers a tag enter/exit
	// First parameter is the tag that was triggered,
	// second parameter is true for an enter/exit tag, false for a tick,
	// third parameter is true for an enter event, false for an exit event,
	// forth parameter is the time elapsed since the enter/exit point and the current time.
	typedef Functor4<const crTag&, bool, bool, float> TagTriggerFunctor;

	// PURPOSE: Type definition of functor to call when encountering an internal loop tag
	// First parameter is the identifier of the loop tag encountered
	// Return value is true for obeying this loop tag, false for ignoring it
	typedef Functor1Ret<bool, u32> LoopIdFunctor;

	// PURPOSE: Update player by time step (complex)
	// PARAMS: deltaTime - time step duration (in seconds)
	// outUnusedTime - unused time in seconds (if the clip doesn't loop, an delta greater than remaining duration)
	// outHasLoopedOrEnded - true if delta update causes clip to loop (or end if non-looping)
	// tagTriggerFunctor - optional pointer to functor to call if delta causes triggering of any tag enter/exit
	// NOTES: If supplied, functor will be called for each instance of a tag enter/exit in the delta period.
	// If a tag both starts and ends within the delta, functor will be called twice, first with enter == true
	// then later with enter == false.
	// If delta or rate is negative (causing backwards playback), the end of the tag interval will trigger an enter,
	// followed by the start of the tag triggering an exit.
	void Update(float deltaTime, float& outUnusedTime, bool& outHasLoopedOrEnded, const TagTriggerFunctor* tagTriggerFunctor=NULL, const LoopIdFunctor* loopIdFunctor=NULL);
	
	// PURPOSE: Composite clip using compositor
	// PARAMS: inoutCompositor - frame compositor
	void Composite(crFrameCompositor& inoutCompositor);

	// PURPOSE: Composite animation frame
	// PARAMS: frame - animation frame to composite
	// RETURNS: true - composite successful, false - one or more dofs in frame failed to composite
	bool Composite(crFrame& inoutFrame);

	
	// PURPOSE: Set clip to play
	// PARAMS: clip - pointer to clip, can be NULL
	// NOTES: clip will be reference counted
	void SetClip(const crClip* clip);

	// PURPOSE: Get clip currently playing
	// RETURNS: pointer to current clip, can be NULL
	const crClip* GetClip() const;

	// PURPOSE: Does the clip player have a valid clip
	// RETURNS: true - if clip is valid
	bool HasClip() const;

	// PURPOSE: Set playback time
	// PARAMS: time - new playback time (in seconds)
	// skip - (optional) is period between current and new playback time skipped, or should any tags during it be triggered (default true)
	void SetTime(float time);
	void SetTime(float time, bool skip);

	// PURPOSE: Get playback time
	// RETURNS: current playback time (in seconds)
	float GetTime() const;

	// PURPOSE: Get blocked time
	// RETURNS: current playback time excluding the blocked sections (in seconds)
	float GetBlockedTime() const;

	// PURPOSE: Set delta time since last update
	// PARAMS: deltaTime - new delta time (in seconds)
	void SetDelta(float deltaTime);

	// PURPOSE: Get delta time since last update
	// RETURNS: delta time (in seconds)
	float GetDelta() const;

	// PURPOSE: Set delta supplement
	// PARAMS: supplement - delta supplement (in seconds)
	// NOTES: Delta supplement is additional delta time that is
	// added to the delta provided during the update call.
	// It is useful for taking direct control of the delta
	// (if the rate is set to zero).  The delta is still used
	// to perform clip time advancement, and trigger tags.
	// SEE ALSO: SetDeltaHiddenSupplement
	void SetDeltaSupplement(float supplement);
	
	// PURPOSE: Get current delta supplement
	// RETURNS: Current delta supplement (in seconds)
	float GetDeltaSupplement() const;

	// PURPOSE: Set hidden delta supplement
	// PARAMS: supplement - hidden delta supplement (in seconds)
	// NOTES: Hidden delta supplement is additional delta time that is
	// added to the delta provided during the update call, but it
	// is added after a new clip time has been calculated and tags have
	// been triggered.  It is useful for scrubbing time in tools, and
	// in situations at runtime where an external system is taking
	// explicit control of setting the clip time (ie rate is zero).
	void SetDeltaHiddenSupplement(float supplement);

	// PURPOSE: Get current hidden delta supplement
	// RETURNS: Current hidden delta supplement (in seconds)
	float GetDeltaHiddenSupplement() const;

	// PURPOSE: Set playback rate
	// PARAMS: rate - new playback rate multiplier
	void SetRate(float rate);

	// PURPOSE: Get playback rate
	// RETURNS: current playback rate multiplier
	float GetRate() const;

	// PURPOSE: Set looping status
	// PARAMS: ignoreClipLooping - override the looping property on the clip?
	// forceLooping - if overriding the clip's own looping property, should player loop or not?
	void SetLooped(bool ignoreClipLooping, bool forceLooping);

	// PURPOSE: Get looping status
	// PARAMS: outIgnoreClipLooping - get ignoreClipLooping status
	// outForceLooping - get forceLooping status
	// RETURNS: Result of looping properties combined with current clip (ie will player actually loop?)
	bool GetLooped(bool &outIgnoreClipLooping, bool &outForceLooping) const;

	// PURPOSE: Is looping
	// RETURNS: Result of looping properties combined with current clip (ie will player actually loop?)
	bool IsLooped() const;

	// PURPOSE: Set loop ids
	// PARAMS: loopIds - bit mask of loop identifiers to obey
	void SetLoopIds(u32 loopIds);

	// PURPOSE: Get loop ids
	// RETURNS: bit mask of loop identifiers currently being obeyed
	u32 GetLoopIds() const;

	// PURPOSE: Set clip callback
	void SetFunctor(const Functor0& functor);

#if CR_DEV
	// PURPOSE: Telemetry callback to collect statistics about played clips
	typedef Functor1<const crClip*> TelemetryCallback;
	static TelemetryCallback sm_Telemetry;
#endif // CR_DEV

private:
	pgRef<const crClip> m_Clip;
	Functor0 m_ClipCallback;
	float m_Time;
	float m_BlockedTime;
	float m_PrevTime;
	float m_LastTime;
	float m_Delta;
	float m_DeltaSupplement;
	float m_DeltaHiddenSupplement;
	float m_Rate;
	float m_CachedDuration;
	bool m_IgnoreClipLooping;
	bool m_ForceLooping;
};

////////////////////////////////////////////////////////////////////////////////

inline void crClipPlayer::Update(float deltaTime)
{
	float junkUnusedTime;
	bool junkHasLoopedOrEnded;

	Update(deltaTime, junkUnusedTime, junkHasLoopedOrEnded, NULL);
}

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crClipPlayer::GetClip() const
{
	return m_Clip;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crClipPlayer::HasClip() const
{
	return m_Clip != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipPlayer::GetTime() const
{
	return m_Time;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipPlayer::GetBlockedTime() const
{
	return m_BlockedTime;
}

////////////////////////////////////////////////////////////////////////////////

inline void crClipPlayer::SetDelta(float deltaTime)
{
	m_Delta = deltaTime;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipPlayer::GetDelta() const
{
	return m_Delta;
}

////////////////////////////////////////////////////////////////////////////////

inline void crClipPlayer::SetDeltaSupplement(float supplement)
{
	m_DeltaSupplement = supplement;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipPlayer::GetDeltaSupplement() const
{
	return m_DeltaSupplement;
}

////////////////////////////////////////////////////////////////////////////////

inline void crClipPlayer::SetDeltaHiddenSupplement(float supplement)
{
	m_DeltaHiddenSupplement = supplement;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipPlayer::GetDeltaHiddenSupplement() const
{
	return m_DeltaHiddenSupplement;
}

////////////////////////////////////////////////////////////////////////////////

inline void crClipPlayer::SetRate(float rate)
{
	m_Rate = rate;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipPlayer::GetRate() const
{
	return m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

inline void crClipPlayer::SetLooped(bool ignoreClipLooping, bool forceLooping)
{
	m_IgnoreClipLooping = ignoreClipLooping;
	m_ForceLooping = forceLooping;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crClipPlayer::GetLooped(bool &outIgnoreClipLooping, bool &outForceLooping) const
{
	outIgnoreClipLooping = m_IgnoreClipLooping;
	outForceLooping = m_ForceLooping;

	return IsLooped();
}

////////////////////////////////////////////////////////////////////////////////

__forceinline bool crClipPlayer::IsLooped() const
{
	if(m_IgnoreClipLooping)
	{
		return m_ForceLooping;
	}
	else
	{
		if(m_Clip)
		{
			return m_Clip->IsLooped();
		}
		else
		{
			return false;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage
#endif //CRCLIP_CLIPPLAYER_H

