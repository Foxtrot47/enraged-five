//
// crclip/clip.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "clip.h"

#include "clipanimation.h"
#include "clipanimations.h"
#include "clipanimationexpression.h"

#include "cranimation/animdictionary.h"
#include "cranimation/frame.h"
#include "crclip/framecompositor.h"
#include "crextra/expressions.h"
#include "crmetadata/dumpoutput.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/properties.h"
#include "crmetadata/tag.h"
#include "crmetadata/tagiterators.h"
#include "crmetadata/tags.h"
#include "file/serialize.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

atArray<const crClip::ClipTypeInfo*> crClip::sm_ClipTypeInfos;

////////////////////////////////////////////////////////////////////////////////

crClip::crClip(eClipType clipType)
: m_Type(kClipTypeNone)
, m_Dictionary(NULL)
, m_Flags(0)
, m_Tags(NULL)
, m_Properties(NULL)
, m_RefCount(1)
{
	Assert(clipType != kClipTypeNone);
	Assert(FindClipTypeInfo(clipType) != NULL);

	m_Type = clipType;
}

////////////////////////////////////////////////////////////////////////////////

crClip::crClip(const crClip& other)
{
	m_Type = other.m_Type;
	m_Dictionary = NULL;
	m_Flags = other.m_Flags;
	m_Tags = other.m_Tags?other.m_Tags->Clone():NULL;
	m_Properties = other.m_Properties?other.m_Properties->Clone():NULL;
	m_RefCount = 1;
}

////////////////////////////////////////////////////////////////////////////////

crClip::crClip(datResource& rsc)
: m_Name(rsc)
, m_Tags(rsc)
, m_Properties(rsc)
{
	m_Flags |= kIsResource;
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crClip::eClipType);
void crClip::DeclareStruct(datTypeStruct& s)
{
	pgBase::DeclareStruct(s);
	STRUCT_BEGIN(crClip);
	STRUCT_FIELD(m_Type);
	STRUCT_FIELD(m_Name);
	STRUCT_FIELD(m_Dictionary);
	STRUCT_FIELD(m_Flags);
	STRUCT_IGNORE(m_Padding);
	STRUCT_FIELD(m_Tags);
	STRUCT_FIELD(m_Properties);
	STRUCT_FIELD(m_RefCount);
	STRUCT_END();
}
#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

void crClip::Place(void *that, datResource& rsc)
{
	const ClipTypeInfo* info = FindClipTypeInfo(((crClip*)that)->GetType());
	Assert(info);

	ClipPlaceFn* placeFn = info->GetPlaceFn();
	placeFn(rsc, (crClip*) that);
}

////////////////////////////////////////////////////////////////////////////////

void crClip::VirtualConstructFromPtr(datResource& rsc, crClip* base)
{
	Assert(base);
	base->Place(base,rsc);
}

////////////////////////////////////////////////////////////////////////////////

crClip::~crClip()
{
	if(IsResource())
	{
		if(m_Tags)
		{
			m_Tags->Destroy();
			m_Tags = NULL;
		}

		if(m_Properties)
		{
			m_Properties->Destroy();
			m_Properties = NULL;
		}
	}
	else
	{
		Shutdown();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClip::Shutdown()
{
	m_Type = kClipTypeNone;
	m_Name.Reset();
	m_Dictionary = NULL;

	if(m_Tags)
	{
		delete m_Tags;
		m_Tags = NULL;
	}

	if(m_Properties)
	{
		delete m_Properties;
		m_Properties = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

crClip* crClip::Allocate(eClipType clipType)
{
	const ClipTypeInfo* info = FindClipTypeInfo(clipType);
	if(info)
	{
		ClipAllocateFn* allocateFn = info->GetAllocateFn();
		return allocateFn();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crClip::Composite(crFrame& inoutFrame, float time, float deltaTime) const
{
	crFrameCompositorFrame compositor(inoutFrame);
	Composite(compositor, time, deltaTime);

	// TODO --- return value no longer correct
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crClip::Composite(crFrame& inoutFrame, float time) const
{
	crFrameCompositorFrame compositor(inoutFrame, true);
	Composite(compositor, time, 0.f);

	// TODO --- return value no longer correct
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crClip::ForAllAnimations(AnimationDiscoveryCb, void*) const
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crClip::ForAllExpressions(ExpressionsDiscoveryCb, void*) const
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crClip::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		sm_ClipTypeInfos.Resize(kClipTypeNum);
		for(int i=0; i<kClipTypeNum; ++i)
		{
			sm_ClipTypeInfos[i] = NULL;
		}

		// register all build in clip types
		crClipAnimation::InitClass();
		crClipAnimations::InitClass();
		crClipAnimationExpression::InitClass();
	}

	crProperty::InitClass();
	crTag::InitClass();
	crExpressions::InitClass();
}

////////////////////////////////////////////////////////////////////////////////

void crClip::ShutdownClass()
{
	sm_ClipTypeInfos.Reset();

	crProperty::ShutdownClass();
	crTag::ShutdownClass();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

crClip::ClipTypeInfo::ClipTypeInfo(crClip::eClipType clipType, const char* clipName, ClipAllocateFn* allocateFn, ClipPlaceFn* placeFn)
: m_ClipType(clipType)
, m_ClipName(clipName)
, m_AllocateFn(allocateFn)
, m_PlaceFn(placeFn)
{
}

////////////////////////////////////////////////////////////////////////////////

crClip::ClipTypeInfo::~ClipTypeInfo()
{
}

////////////////////////////////////////////////////////////////////////////////

void crClip::ClipTypeInfo::Register() const
{
	crClip::RegisterClipTypeInfo(*this);
}

////////////////////////////////////////////////////////////////////////////////

crClip::eClipType crClip::ClipTypeInfo::GetClipType() const
{
	return m_ClipType;
}

////////////////////////////////////////////////////////////////////////////////

const char* crClip::ClipTypeInfo::GetClipName() const
{
	return m_ClipName;
}

////////////////////////////////////////////////////////////////////////////////

crClip::ClipAllocateFn* crClip::ClipTypeInfo::GetAllocateFn() const
{
	return m_AllocateFn;
}

////////////////////////////////////////////////////////////////////////////////

crClip::ClipPlaceFn* crClip::ClipTypeInfo::GetPlaceFn() const
{
	return m_PlaceFn;
}

////////////////////////////////////////////////////////////////////////////////

const crClip::ClipTypeInfo* crClip::FindClipTypeInfo(eClipType clipType)
{
	if(clipType < kClipTypeNum)
	{
		Assert(clipType > kClipTypeNone);
		Assert(sm_ClipTypeInfos.GetCount() > 0);
		Assert(clipType < sm_ClipTypeInfos.GetCount());

		return sm_ClipTypeInfos[clipType];
	}
	else
	{
		// TODO --- project specific clip types
		Assert(0);
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

static const crTag::Key TagKeyBlock = crTag::CalcKey("Block", 0xE433D77D);
static const crProperty::Key AttributeKeyFocus = crProperty::CalcKey("Focus", 0xE391E9E5);
static const crProperty::Key AttributeKeyDivision = crProperty::CalcKey("Division", 0xC07BFFA3);

bool crClip::CalcBlockPassed(float oldTime, float newTime) const
{
	if(m_Tags)
	{
		if(m_Tags->HasTags(TagKeyBlock))
		{
			const float duration = GetDuration();
			const float durationInv = 1.f / duration;

			const float oldPhase = oldTime * durationInv;
			const float newPhase = newTime * durationInv;

			float newPhaseWrapped = newPhase;
			if(newPhaseWrapped < oldPhase)
			{
				newPhaseWrapped += 1.f;
			}

			crTagIterator it(*m_Tags, oldPhase, newPhase, TagKeyBlock);
			while(*it)
			{
				const crTag* tagBlock = static_cast<const crTag*>(*it);

				bool focus = false;
				const crPropertyAttribute* attribFocus = tagBlock->GetProperty().GetAttribute(AttributeKeyFocus);
				if(attribFocus && attribFocus->GetType() == crPropertyAttribute::kTypeBool)
				{
					focus = static_cast<const crPropertyAttributeBool*>(attribFocus)->GetBool();
				}

				if(!focus)
				{
					float division = 0.5f;
					const crPropertyAttribute* attribDivision = tagBlock->GetProperty().GetAttribute(AttributeKeyDivision);
					if(attribDivision && attribDivision->GetType() == crPropertyAttribute::kTypeFloat)
					{
						division = static_cast<const crPropertyAttributeFloat*>(attribDivision)->GetFloat();
					}

					const float divisionPhase = tagBlock->Lerp(division);
					float divisionPhaseWrapped = divisionPhase;
					if(newPhase != newPhaseWrapped && divisionPhase <= newPhase)
					{
						divisionPhaseWrapped += 1.f;
					}

					const bool oldPostDivision = (oldPhase >= divisionPhaseWrapped);
					const bool newPostDivision = (newPhaseWrapped >= divisionPhaseWrapped);

					if(oldPostDivision != newPostDivision)
					{
						return true;
					}
				}

				++it;
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

float crClip::CalcBlockedTimeInternal(float time) const
{
	const float duration = GetDuration();
	const float durationInv = 1.f / duration;

	crTagIterator it(*m_Tags, TagKeyBlock);
	while(*it)
	{
		const crTag* tagBlock = static_cast<const crTag*>(*it);

		float phase = time * durationInv;

		if(tagBlock->Contains(phase))
		{
			bool focus = false;
			const crPropertyAttribute* attribFocus = tagBlock->GetProperty().GetAttribute(AttributeKeyFocus);
			if(attribFocus && attribFocus->GetType() == crPropertyAttribute::kTypeBool)
			{
				focus = static_cast<const crPropertyAttributeBool*>(attribFocus)->GetBool();
			}

			float division = 0.5f;
			const crPropertyAttribute* attribDivision = tagBlock->GetProperty().GetAttribute(AttributeKeyDivision);
			if(attribDivision && attribDivision->GetType() == crPropertyAttribute::kTypeFloat)
			{
				division = static_cast<const crPropertyAttributeFloat*>(attribDivision)->GetFloat();
			}

			const float start = tagBlock->GetStart();
			const float mid = tagBlock->Lerp(division);
			const float end = tagBlock->GetEnd();

			if(focus)
			{
				phase = mid;
			}
			else if(start <= end)
			{
				if(phase < mid)
				{
					phase = start;
				}
				else
				{
					phase = end;
				}
			}
			else
			{
				if(start <= mid)
				{
					if(InRange(phase, start, end))
					{
						phase = start;
					}
					else
					{
						phase = end;
					}
				}
				else
				{
					if(InRange(phase, mid, end))
					{
						phase = end;
					}
					else
					{
						phase = start;
					}
				}
			}

			time = phase * duration;
		}
		++it;
	}

	return time;
}

////////////////////////////////////////////////////////////////////////////////

void crClip::RegisterClipTypeInfo(const ClipTypeInfo& info)
{
	Assert(sm_ClipTypeInfos.GetCount() > 0);
	
	if(info.GetClipType() < kClipTypeNum)
	{
		Assert(info.GetClipType() < sm_ClipTypeInfos.GetCount());
		Assert(sm_ClipTypeInfos[info.GetClipType()] == NULL);

		sm_ClipTypeInfos[info.GetClipType()] = &info;
	}
	else
	{
		// TODO --- project specific clip types
		Assert(0);
	}
}

////////////////////////////////////////////////////////////////////////////////

crClip* crClip::CreateBase(eClipType clipType)
{
	crClip* clip = Allocate(clipType);
	
	if(clip)
	{
		clip->CreateTags();
		clip->CreateProperties();
	}

	return clip;
}

////////////////////////////////////////////////////////////////////////////////

void crClip::CreateTags()
{
	if(!m_Tags)
	{
		m_Tags = rage_new crTags();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClip::CreateProperties()
{
	if(!m_Properties)
	{
		m_Properties = rage_new crProperties();
	}
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
int clipVersions[] =
{
	13, // 14-DEC-12 - added parallel flag to crClipAnimations
	12, // 05-JUL-11 - add clip time serialization
	11, // 09-JUN-11 - dropped (unused) comment field from clip header
	10, // 25-JAN-11 - add markers for properties/tags serialization
	9,  // 24-AUG-10 - new style properties/tags
	8,  // 06-MAY-10 - CLIP_ATOMIC_REFCOUNT was missed during integration
	7,  // ???
	6,  // 13-NOV-09 - add tag/property container
	5,  // 09-OCT-09 - add new types of clip serial and animation expression
	4,  // 28-JAN-09 - converted tags to be phase based when embedded in clips (already are in pm's)
	3,  // 15-JAN-09 - moved property data to separate manager (all tags are known embedded from now)
	2,  // 10-JUL-08 - added property data type
	1,  // 30-AUG-06 - first clip version
	0,
};

__THREAD crAnimLoader* s_AnimLoader = NULL;
__THREAD int s_ClipVersion = 0;

////////////////////////////////////////////////////////////////////////////////

bool crClip::PushClipFolder() const
{
	char clipFolderBuf[RAGE_MAX_PATH];
	safecpy(clipFolderBuf, GetName());

	fiAssetManager::RemoveNameFromPath(clipFolderBuf, RAGE_MAX_PATH, clipFolderBuf);

	if(clipFolderBuf[0] && strcmp(clipFolderBuf, GetName()))
	{
		ASSET.PushFolder(clipFolderBuf);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crClip::MakeAnimRelative(char* dest, int destSize, const char* source)
{
	bool pushedFolder = PushClipFolder();
	ASSET.MakeFolderRelative(dest, destSize, source);
	if(pushedFolder)
	{
		ASSET.PopFolder();
	}
}

////////////////////////////////////////////////////////////////////////////////

class datTag
{
public:
	datTag(const char* text, bool enabled=true)
	: m_Text(text)
	, m_Enabled(enabled)
	{
		m_Length = u32(strlen(text));
	}

private:
	friend datSerialize & operator<< ( datSerialize &ser, const datTag &tag ) {
		if ( tag.m_Enabled )
		{
			for(u32 i=0; i < tag.m_Length; ++i)
			{
				u8 origin = tag.m_Text[i];
				u8 data = origin;
				ser.Serialize(data);
				Assert(data==origin);
			}
		}
		return ser;
	}

	const char* m_Text;
	u32 m_Length;
	bool m_Enabled;
};


////////////////////////////////////////////////////////////////////////////////

crClip* crClip::AllocateAndLoad(const char* fileName, crAnimLoader* loader)
{
	s_AnimLoader = loader;

	fiStream* f = ASSET.Open(fileName, "clip", false, true);
	if(!f)
	{
		Errorf("crClip - failed to open file '%s' for reading", fileName);
		return NULL;
	}

	int version;
	int type;
	int magic = 0;
	f->ReadInt(&magic, 1);

	if(magic != 'PILC')
	{
		Errorf("crClip - file is not a clip file");
		f->Close();
		return NULL;
	}

	fiSerialize s(f, true, false);

	s << datLabel("ClipVersion:") << version << datNewLine;
	s << datLabel("ClipType:") << type << datNewLine;

	s_ClipVersion = version;

	const int latestVersion = clipVersions[0];
	if(version > latestVersion)
	{
		Errorf("crClip - attempting to load newer clip version '%d' (only up to '%d' supported)", version, latestVersion);
		return NULL;
	}

	crClip* newClip = Allocate(eClipType(type));
	if(newClip)
	{
		newClip->m_Name = fileName;
		if(fiSerializeFrom(f, *newClip))
		{
			f->Close();
			return newClip;
		}

		delete newClip;
	}

	Errorf("crClip - failed to load file '%s'", fileName);

	f->Close();

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crClip::Save(const char* fileName)
{
	fiStream* f = ASSET.Create(fileName, "clip");
	if(!f)
	{
		Errorf("crClip - failed to open file '%s' for writing", fileName);
		return false;
	}

	m_Name = fileName;

	const bool binary = true;
	int version = clipVersions[0];
	int type = m_Type;

	int magic = 'PILC';
	f->WriteInt(&magic, 1);

	fiSerialize s(f, false, binary);

	s << datLabel("ClipVersion:") << version << datNewLine;
	s << datLabel("ClipType:") << type << datNewLine;

	s_ClipVersion = version;

	bool success = fiSerializeTo(f, *this, binary);

	if(!success)
	{
		Errorf("crClip - failed to save file '%s'", fileName);
	}

	f->Close();

	return success;
}

////////////////////////////////////////////////////////////////////////////////

void crClip::Serialize(datSerialize& s)
{
	if(s_ClipVersion < 11)
	{
		// comment field dropped, use comment property instead
		sysMemStartTemp();
		atString comment;
		s << datLabel("Comment:") << comment << datNewLine;
		comment.Reset();
		sysMemEndTemp();
	}
	bool isLooped = IsLooped();
	s << datLabel("IsLooped:") << isLooped << datNewLine;
	if(s.IsRead())
	{
		SetLooped(isLooped);
	}

	// serialize duration
	sysMemStartTemp();
	if(s_ClipVersion >= 12)
	{
		crPropertyAttributeFloat duration;
		duration.SetName("Duration");
		duration.GetFloat() = GetDuration();
		s << datTag("Duration:") << duration << datTag("DurationEnd");
	}
	sysMemEndTemp();

	// serialize properties
	bool useMarker = s_ClipVersion >= 10;
	if(s_ClipVersion >= 3)
	{
		bool embeddedProperties = true;
		s << datNewLine << datTag("EmbeddedProperties:", useMarker) << embeddedProperties << datNewLine;
		Assert(embeddedProperties);

		if(embeddedProperties)
		{
			if(s.IsRead())
			{
				Assert(!m_Properties);
				m_Properties = rage_new crProperties();
			}

			s << datLabel("{") << datNewLine << datNewLine << *m_Properties << datNewLine << datLabel("}") << datNewLine << datNewLine;
		}
		s << datTag("EmbeddedPropertiesEnd", useMarker);
	}
	else if(s.IsRead())
	{
		// legacy loading old style properties built into clip structure
		int numProperties = 0;
		s << datLabel("NumProperties:") << numProperties << datNewLine;

		Assert(!m_Properties);
		m_Properties = rage_new crProperties;
		m_Properties->m_Properties.Create(u16(numProperties));

		for(int i=0; i<numProperties; ++i)
		{
			crProperty* prop = rage_new crProperty;
			m_Properties->SerializePropertyLegacy(s, *prop);

			Assert(!m_Properties->HasProperty(prop->GetKey()));
			m_Properties->m_Properties.Insert(prop->GetKey(), prop);
		}
	}

	bool embeddedTags = true;
	s << datNewLine << datTag("EmbeddedTags:", useMarker) << embeddedTags << datNewLine;

	if(embeddedTags)
	{
		if(s.IsRead())
		{
			Assert(!m_Tags);
			m_Tags = rage_new crTags();
		}

		s << datLabel("{") << datNewLine << datNewLine << *m_Tags << datNewLine << datLabel("}") << datNewLine << datNewLine;
	}
	else
	{
		AssertMsg(embeddedTags, "crClip::Serialize - no longer support reading of old .tag files");
		Assert(s_ClipVersion < 3);
		if(s.IsRead())
		{
			// no longer support reading in of old .tag files - create empty tag container instead
			m_Tags = rage_new crTags();
		}
	}
	s << datTag("EmbeddedTagsEnd", useMarker);
}

////////////////////////////////////////////////////////////////////////////////

void crClip::PostSerialize(datSerialize& ASSERT_ONLY(s))
{
	if(m_Tags)
	{
		// convert old embedded tags from seconds to phase based
		if(s_ClipVersion < 4)
		{
			Assert(s.IsRead());

			const float duration = GetDuration();
			const float invDuration = (duration > 0.f)?(1.f/duration):0.f;

			const int numTags = m_Tags->GetNumTags();
			for(int i=0; i<numTags; ++i)
			{
				crTag* tag = m_Tags->GetTag(i);

				float start = tag->GetStart();
				float end =  tag->GetEnd();

				start *= invDuration;
				end *= invDuration;

				tag->SetStart(start);
				tag->SetEnd(end);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

crAnimation* crClip::AllocateAndLoadAnimation(const char* animFilename) const
{
	bool pushedFolder = PushClipFolder();

	char relPath[RAGE_MAX_PATH];
	ASSET.MakeFolderRelative(relPath, RAGE_MAX_PATH, animFilename);

    crAnimation* result = NULL;
	if(s_AnimLoader)
	{
		result = s_AnimLoader->AllocateAndLoadAnimation(relPath);
	}
	else
    {
        result = crAnimation::AllocateAndLoad(relPath);
    }

	if(pushedFolder)
	{
		ASSET.PopFolder();
	}

    return result;
}

////////////////////////////////////////////////////////////////////////////////

void crClip::Dump(crDumpOutput& output) const
{
	output.Outputf(1, "filename", "'%s'", m_Name.c_str());
	output.Outputf(1, "cliptype", "'%s' %d", GetTypeName(), int(GetType()));
	output.Outputf(1, "looped", "%d", IsLooped());

	output.Outputf(1, "hasproperties", "%d", int(m_Properties!=NULL));
	if(m_Properties)
	{
		m_Properties->Dump(output);
	}

	output.Outputf(1, "hastags", "%d", int(m_Tags!=NULL));
	if(m_Tags)
	{
		m_Tags->Dump(output);
	}

	DumpDofs(output);
}

////////////////////////////////////////////////////////////////////////////////

void crClip::DumpDofs(crDumpOutput& output) const
{
	crFrameData frameData;
	InitDofs(frameData);

	u32 numFrames = (u32)GetNum30Frames();
	u32 numDofs = frameData.GetNumDofs();

	output.Outputf(1, "",""); 

	for(u32 i=0; i < numDofs; i++)
	{
		const crFrameData::Dof& dof = frameData.GetDof(i);

		output.Outputf(1, "Dof", i, "track %u id %u type %u", dof.m_Track, dof.m_Id, dof.m_Type);

		crFrameDataFixedDofs<1> singleDof;
		singleDof.AddDof(dof.m_Track, dof.m_Id, dof.m_Type);

		crFrameFixedDofs<1> frameDOFs(singleDof);

		for(u32 j=0; j < numFrames; j++)
		{
			Composite(frameDOFs, Convert30FrameToTime((float)j));

			switch (dof.m_Type)
			{
			case kFormatTypeVector3:
				{
					Vec3V vec;
					frameDOFs.GetValue<Vec3V>(dof.m_Track, dof.m_Id, vec);
					output.Outputf(2, "Value", j, "%.3f %.3f %.3f", vec.GetXf(), vec.GetYf(), vec.GetZf());
				}
				break;
			case kFormatTypeQuaternion:
				{
					QuatV quat;
					frameDOFs.GetValue<QuatV>(dof.m_Track, dof.m_Id, quat);
					Vec3V eulers = QuatVToEulersXYZ(quat);
					output.Outputf(2, "Value", j, "%.3f %.3f %.3f", eulers.GetXf(), eulers.GetYf(), eulers.GetZf());
				}
				break;
			case kFormatTypeFloat:
				{
					float val;
					frameDOFs.GetValue<float>(dof.m_Track, dof.m_Id, val);
					output.Outputf(2, "Value", j, "%.3f", val);
				}
				break;
			}
		}

		output.Outputf(1, "",""); 
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClip::Dump(atString& inoutText) const
{
	crDumpOutputString output;
	Dump(output);

	inoutText += output.GetString();
}

////////////////////////////////////////////////////////////////////////////////

const char* crClip::GetTypeName() const
{
	const ClipTypeInfo* info = FindClipTypeInfo(GetType());
	if(info)
	{
		return info->GetClipName();
	}

	return "UNKNOWN_CLIP_TYPE";
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crClip::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
