Project crclip

IncludePath $(RAGE_DIR)\base\src

Files {
	Folder Clips {
		clip.cpp
		clip.h
		clipanimation.cpp
		clipanimation.h
		clipanimations.cpp
		clipanimations.h
		clipserialanimations.cpp
		clipserialanimations.h
		clipplayer.cpp
		clipplayer.h
		clips.cpp
		clips.h
		clipdictionary.cpp
		clipdictionary.h
		clipanimationexpression.cpp
		clipanimationexpression.h
	}
	Folder Compositor {
		framecompositor.h
		framecompositor.cpp
	}
}
