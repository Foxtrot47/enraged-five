//
// crclip/clipanimation.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRCLIP_CLIPANIMATION_H
#define CRCLIP_CLIPANIMATION_H

#include "clip.h"

namespace rage
{


// PURPOSE: Simplest build in derived clip class
// Wraps a single animation in the clip interface.
class crClipAnimation : public crClip
{
public:

	// PURPOSE: Default constructor
	crClipAnimation(eClipType clipType=kClipTypeAnimation);

	// PURPOSE: Copy constructor
	crClipAnimation(const crClipAnimation&);

	// PURPOSE: Resource constructor
	crClipAnimation(datResource&);

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crClipAnimation();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Declare clip type
	CR_DECLARE_CLIP_TYPE(crClipAnimation);

#if CR_DEV
	// PURPOSE: Serialization to/from file
	virtual void Serialize(datSerialize&);

	// PURPOSE: Debug output
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Allocate animation clip
	// RETURNS: newly allocated animation clip, or NULL if creation failed
	static crClipAnimation* Allocate();

	// PURPOSE: Allocate and create animation clip
	// PARAMS: anim - animation to use wrap within this clip
	// RETURNS: newly allocated and created animation clip, or NULL if creation failed
	// NOTES: clip will reference count the animation
	static crClipAnimation* AllocateAndCreate(const crAnimation& anim);

	// PURPOSE: Create animation clip
	// PARAMS: anim - animation to use wrap within this clip
	// RETURNS: true - creation succeeded, false if creation failed
	// NOTES: clip will reference count the animation
	bool Create(const crAnimation& anim);


	// PURPOSE: Get clip duration
	virtual float GetDuration() const;

	// PURPOSE: Composite
	virtual void Composite(crFrameCompositor& compositor, float time, float deltaTime) const;

	// PURPOSE: Initialize dofs
	virtual void InitDofs(crFrameData& frameData, crFrameFilter* filter=NULL) const;

	// PURPOSE: Does the clip contain this degree of freedom
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Override animation discovery
	virtual bool ForAllAnimations(AnimationDiscoveryCb cb, void* data) const;

	// PURPOSE: Get the animation
	// RETURNS: pointer to underlying animation (can be NULL)
	// NOTES: use with caution, preferably to convert existing legacy code (and tools) only
	// breaks data hiding and wrapping functionality of clips, makes it difficult to
	// easily transition to using other clip types
	const crAnimation* GetAnimation() const;

	// PURPOSE: Set the animation
	// PARAMS: anim - new underlying animation wrapped by this clip
	// NOTES: clip will reference count the animation
	// subset start/end times will be reset to 0/duration respectively
	void SetAnimation(const crAnimation& anim);

	// PURPOSE: Get animation start/end range times
	// PARAMS: outStartTime, outEndTime - in seconds (outStartTime <= outEndTime)
	// NOTES: Time will be clamped to legal time within animation duration
	void GetStartEndTimes(float& outStartTime, float& outEndTime) const;

	// PURPOSE: Get animation start time
	float GetStartTime() const;

	// PURPOSE: Get animation end time
	float GetEndTime() const;

	// PURPOSE: Set animation start/end range times
	// PARAMS: startTime, endTime - in seconds (startTime <= endTime)
	// NOTES: Time will be clamped to legal time within animation duration
	void SetStartEndTimes(float startTime, float endTime);

	// PURPOSE: Set playback rate
	// PARAMS: rate - multiplier
	void SetRate(float rate);

	// PURPOSE: Get playback rate
	// RETURNS: rate - multiplier
	float GetRate() const;

private:
	datRef<const crAnimation> m_Animation;

	float m_StartTime;
	float m_EndTime;
	float m_Rate;
};

////////////////////////////////////////////////////////////////////////////////

inline const crAnimation* crClipAnimation::GetAnimation() const
{
	return m_Animation;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipAnimation::GetStartTime() const
{
	return m_StartTime;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipAnimation::GetEndTime() const
{
	return m_EndTime;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipAnimation::GetRate() const
{
	return m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
#endif // CRCLIP_CLIPANIMATION_H
