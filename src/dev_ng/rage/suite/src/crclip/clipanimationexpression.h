//
// crclip/clipanimationexpression.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef CRCLIP_CLIPANIMATIONEXPRESSION_H
#define CRCLIP_CLIPANIMATIONEXPRESSION_H

#include "clipanimation.h"

namespace rage
{

class crExpressions;

// PURPOSE: Clip containing an animation and a list of expressions
class crClipAnimationExpression : public crClipAnimation
{
public:

	// PURPOSE: Constructor
	crClipAnimationExpression();

	// PURPOSE: Resource constructor
	crClipAnimationExpression(datResource&);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crClipAnimationExpression();

	// PURPOSE: Declare clip type
	CR_DECLARE_CLIP_TYPE(crClipAnimationExpression);

#if CR_DEV
	// PURPOSE: Serialization to/from file
	virtual void Serialize(datSerialize&);

	// PURPOSE: Debug output
	virtual void Dump(crDumpOutput&) const;
#endif // CR_DEV

	// PURPOSE: Allocate and create serial animations clip
	// RETURNS: newly allocated and created animation clip, or NULL if creation failed
	static crClipAnimationExpression* AllocateAndCreate(const crAnimation& anim, crExpressions& expressions);

	// PURPOSE: Create serial animations clip
	// RETURNS: true - creation succeeded, false if creation failed
	bool Create(const crAnimation& anim, crExpressions& expressions);

	// PURPOSE: Composite
	virtual void Composite(crFrameCompositor& compositor, float time, float deltaTime) const;

	// PURPOSE: Override expressions discovery
	virtual bool ForAllExpressions(ExpressionsDiscoveryCb cb, void* data) const;

	// PURPOSE: Get expressions
	const crExpressions* GetExpressions() const;

	// PURPOSE: Set the expressions
	// PARAMS: expr - new expression wrapped by this clip
	// NOTES: clip will reference count the expression
	void SetExpressions(crExpressions& expr);


private:

	datOwner<crExpressions> m_Expressions;
};

////////////////////////////////////////////////////////////////////////////////

inline const crExpressions* crClipAnimationExpression::GetExpressions() const
{
	return m_Expressions;
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CRCLIP_CLIPANIMATIONEXPRESSION_H
