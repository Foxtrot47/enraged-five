//
// crclip/clipanimation.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "clipanimation.h"

#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/framedof.h"
#include "cranimation/frameinitializers.h"
#include "crclip/framecompositor.h"
#include "crmetadata/dumpoutput.h"
#include "file/asset.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crClipAnimation::crClipAnimation(eClipType clipType)
: crClip(clipType)
, m_Animation(NULL)
, m_StartTime(-1.f)
, m_EndTime(-1.f)
, m_Rate(1.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crClipAnimation::crClipAnimation(const crClipAnimation& other)
: crClip(other)
{
	m_Animation = other.m_Animation;
	if(m_Animation)
	{
		m_Animation->AddRef();
	}
	m_StartTime = other.m_StartTime;
	m_EndTime = other.m_EndTime;
	m_Rate = other.m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

crClipAnimation::crClipAnimation(datResource& rsc)
: crClip(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crClipAnimation::DeclareStruct(datTypeStruct& s)
{
	crClip::DeclareStruct(s);

	STRUCT_BEGIN(crClipAnimation);
	STRUCT_FIELD(m_Animation);
	STRUCT_FIELD(m_StartTime);
	STRUCT_FIELD(m_EndTime);
	STRUCT_FIELD(m_Rate);
	STRUCT_END();
}
#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

crClipAnimation::~crClipAnimation()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimation::Shutdown()
{
	if(m_Animation)
	{
		m_Animation->Release();
		m_Animation = NULL;
	}

	crClip::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CLIP_TYPE(crClipAnimation, crClip::kClipTypeAnimation);

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crClipAnimation::Serialize(datSerialize& s)
{
	crClip::Serialize(s);

	// serialize animation file name:
	char animFile[RAGE_MAX_PATH];
	animFile[0] = '\0';

	if(!s.IsRead() && m_Animation)
	{
		bool pushedFolder = PushClipFolder();
		ASSET.MakeFolderRelative(animFile, RAGE_MAX_PATH, m_Animation->GetName());
		if(pushedFolder)
		{
			ASSET.PopFolder();
		}
	}

	s << datLabel("AnimFile:") << datString(animFile, RAGE_MAX_PATH) << datNewLine;

	if(s.IsRead())
	{
		if(*animFile != '\0')
		{
			m_Animation = AllocateAndLoadAnimation(animFile);
		}
		if(m_Animation)
		{
			m_EndTime = Min(m_EndTime, m_Animation->GetDuration());
		}
	}

	s << datLabel("StartEndTimes:") << m_StartTime << m_EndTime << datNewLine;
	s << datLabel("Rate:") << m_Rate << datNewLine;

	crClip::PostSerialize(s);
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimation::Dump(crDumpOutput& output) const
{
	crClip::Dump(output);

	output.Outputf(2, "animation", "'%s'", m_Animation?m_Animation->GetName():"NONE");
	output.Outputf(2, "starttime", "%f", m_StartTime);
	output.Outputf(2, "endtime", "%f", m_EndTime);
	output.Outputf(2, "rate", "%f", m_Rate);
}

#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crClipAnimation* crClipAnimation::Allocate()
{
	crClipAnimation* clip = static_cast<crClipAnimation*>(CreateBase(kClipTypeAnimation));
	return clip;
}

////////////////////////////////////////////////////////////////////////////////

crClipAnimation* crClipAnimation::AllocateAndCreate(const crAnimation& anim)
{
	crClipAnimation* clip = static_cast<crClipAnimation*>(CreateBase(kClipTypeAnimation));

	if(clip)
	{
		if(clip->Create(anim))
		{
			return clip;
		}
		delete clip;
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crClipAnimation::Create(const crAnimation& anim)
{
	SetAnimation(anim);
	SetLooped(anim.IsLooped());

	const int maxNameBufSize = RAGE_MAX_PATH;
	char nameBuf[maxNameBufSize];

	ASSET.RemoveExtensionFromPath(nameBuf, maxNameBufSize, anim.GetName());
	safecat(nameBuf, ".clip");
	SetName(nameBuf);

	CreateTags();
	CreateProperties();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

float crClipAnimation::GetDuration() const
{
	return (m_EndTime-m_StartTime)/m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimation::Composite(crFrameCompositor& compositor, float time, float deltaTime) const
{
	time = CalcBlockedTime(time);
	time = Clamp(time*m_Rate+m_StartTime, m_StartTime, m_EndTime);
	deltaTime *= m_Rate;  // TODO --- blocking will affect deltaTime !?!

	compositor.Composite(m_Animation, time, deltaTime);
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimation::InitDofs(crFrameData& frameData, crFrameFilter* filter) const
{
	if(m_Animation)
	{
		crFrameDataInitializerAnimation initializer(*m_Animation, false, filter);
		initializer.InitializeFrameData(frameData);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crClipAnimation::HasDof(u8 track, u16 id) const
{
	return m_Animation && m_Animation->HasDof(track, id);
}

////////////////////////////////////////////////////////////////////////////////

bool crClipAnimation::ForAllAnimations(AnimationDiscoveryCb cb, void* data) const
{
	return cb(m_Animation, m_StartTime, m_EndTime, m_Rate, data);
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimation::SetAnimation(const crAnimation& anim)
{
	anim.AddRef();

	if(m_Animation)
	{
		m_Animation->Release();
	}

	m_Animation = &anim;

	m_StartTime = 0.f;
	m_EndTime = m_Animation->GetDuration();
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimation::GetStartEndTimes(float& outStartTime, float& outEndTime) const
{
	outStartTime = m_StartTime;
	outEndTime = m_EndTime;
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimation::SetStartEndTimes(float startTime, float endTime)
{
	Assert(startTime <= endTime);
	Assert(m_Animation);

	m_StartTime = Clamp(startTime, 0.f, m_Animation->GetDuration());
	m_EndTime = Clamp(endTime, 0.f, m_Animation->GetDuration());
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimation::SetRate(float rate)
{
	Assert(rate > 0.0f);
	m_Rate = rate;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
