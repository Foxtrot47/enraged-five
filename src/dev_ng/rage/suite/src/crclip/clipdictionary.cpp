//
// crclip/clipdictionary.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "clipdictionary.h"
#include "clipanimation.h"

#include "atl/map_struct.h"
#include "cranimation/animtolerance.h"
#include "file/stream.h"
#include "file/token.h"
#include "paging/rscbuilder.h"
#include "system/memory.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crClipDictionary::crClipDictionary()
: pgBaseRefCounted(0)
, m_AnimDictionary(NULL)
, m_AnimDictionaryOwner(true)
, m_BaseNameKeys(false)
{
	if(m_AnimDictionaryOwner)
	{
		m_AnimDictionary = rage_new crAnimDictionary;
	}
}

////////////////////////////////////////////////////////////////////////////////

void FixupDataFunc(datResource &rsc, datOwner<crClip>& data)
{
    data.Place(&data, rsc);
}

crClipDictionary::crClipDictionary(datResource& rsc)
: pgBaseRefCounted(rsc)
, m_AnimDictionary(rsc)
, m_Clips(rsc, NULL, &FixupDataFunc)
{
	if(m_AnimDictionaryOwner)
	{
		crAnimDictionary::Place(m_AnimDictionary, rsc);
	}
}

////////////////////////////////////////////////////////////////////////////////

crClipDictionary::~crClipDictionary()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

bool ReleaseClipFunc(crClip* clip, crClipDictionary::ClipKey, void*)
{
	if(clip)
	{
		clip->Release(crClip::kRefOptionClipOnly);
	}
	return true;
}

void crClipDictionary::Shutdown()
{
	if(m_AnimDictionaryOwner)
	{
		delete m_AnimDictionary;
		m_AnimDictionary = NULL;
	}

	ForAll(&ReleaseClipFunc, NULL);

	m_Clips.Kill();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crClipDictionary);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crClipDictionary::DeclareStruct(datTypeStruct& s)
{
	pgBaseRefCounted::DeclareStruct(s);
	STRUCT_BEGIN(crClipDictionary);
	if(m_AnimDictionaryOwner)
	{
		STRUCT_FIELD_OWNED_PTR(m_AnimDictionary);
	}
	else
	{
		STRUCT_FIELD_VP(m_AnimDictionary);
	}
	STRUCT_FIELD(m_AnimDictionaryOwner);
	STRUCT_FIELD(m_BaseNameKeys);
	STRUCT_IGNORE(m_Padding);
	STRUCT_FIELD(m_Clips);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
crClipDictionary* crClipDictionary::AllocateAndLoadClips(const char* listFilename, crClipLoader* loader, crAnimLoader* subLoader, bool baseNameKeys)
{
	crClipDictionary* dictionary = rage_new crClipDictionary;
	if(dictionary->LoadClips(listFilename, loader, subLoader, baseNameKeys))
	{
		return dictionary;
	}
	delete dictionary;
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crClipDictionary* crClipDictionary::AllocateAndLoadClips(const atArray<atString>& clipFilenames, crClipLoader* loader, crAnimLoader* subLoader, bool baseNameKeys)
{
	crClipDictionary* dictionary = rage_new crClipDictionary;
	if(dictionary->LoadClips(clipFilenames, loader, subLoader, baseNameKeys))
	{
		return dictionary;
	}
	delete dictionary;
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crClipDictionary::LoadClips(const char* listFilename, crClipLoader* loader, crAnimLoader* subLoader, bool baseNameKeys)
{
	fiSafeStream f(ASSET.Open(listFilename, "cliplist"));
	if(f)
	{
		fiTokenizer T(listFilename, f);	

		sysMemStartTemp();
		atArray<atString> clipFilenames;

		const int maxBufSize = RAGE_MAX_PATH;
		char buf[maxBufSize];
		while(T.GetLine(buf, maxBufSize) > 0)
		{
			clipFilenames.Grow() = buf;
		}
		sysMemEndTemp();

		bool success = LoadClips(clipFilenames, loader, subLoader, baseNameKeys);

		sysMemStartTemp();
		clipFilenames.Reset();
		sysMemEndTemp();

		return success;
	}
	else
	{
		Errorf("crClipDictionary::LoadClips - failed to open list file '%s'", listFilename);
		return false;
	}
}


////////////////////////////////////////////////////////////////////////////////

class AnimLoaderDictionaryBuilder : public crAnimLoader
{
public:

	AnimLoaderDictionaryBuilder(crAnimLoader& animLoader)
		: m_AnimLoader(&animLoader)
	{
	}

	virtual ~AnimLoaderDictionaryBuilder()
	{
		Shutdown();
	}
	
	void Shutdown()
	{
		m_AnimationNames.Reset();
	}

	virtual crAnimation* AllocateAndLoadAnimation(const char* animFilename)
	{
		crAnimation* anim = m_AnimLoader->AllocateAndLoadAnimation(animFilename);
		if(anim)
		{
			char fullAnimFilename[RAGE_MAX_PATH];
			ASSET.FullReadPath(fullAnimFilename, RAGE_MAX_PATH, anim->GetName(), "");

			const int numAnims = m_AnimationNames.GetCount();
			for(int i=0; i<numAnims; ++i)
			{
				if(!stricmp((const char*)m_AnimationNames[i], fullAnimFilename))
				{
					return anim;
				}
			}
			m_AnimationNames.Grow() = fullAnimFilename;
		}
		else
		{
			Errorf("AnimLoaderDictionaryBuilder::AllocateAndLoadAnimation - failed to load animation '%s'", animFilename);
		}

		return anim;
	}

	void BuildDictionary(crAnimDictionary& dictionary, bool baseNameKeys)
	{
		dictionary.LoadAnimations(m_AnimationNames, m_AnimLoader, baseNameKeys);
	}

	atArray<atString> m_AnimationNames;
	crAnimLoader* m_AnimLoader;
};

////////////////////////////////////////////////////////////////////////////////

bool crClipDictionary::LoadClips(const atArray<atString>& clipFilenames, crClipLoader* loader, crAnimLoader* subLoader, bool baseNameKeys)
{
	crClipLoaderBasic defaultLoader;
	if(!loader)
	{
		loader = &defaultLoader;
	}

	crAnimLoaderBasic defaultSubLoader;
	if(!subLoader)
	{
		subLoader = &defaultSubLoader;
	}

	AnimLoaderDictionaryBuilder builderLoader(*subLoader);
	
	const int numClips = clipFilenames.GetCount();

	bool success = true;
	for(int i=0; i<numClips; ++i)
	{
		const char* clipFilename = (const char*)clipFilenames[i];

		sysMemStartTemp();
		crClip* clip = loader->AllocateAndLoadClip(clipFilename, &builderLoader);
		sysMemEndTemp();

		if(!clip)
		{
			Errorf("crClipDictionary::LoadClips - failed to load clip '%s'", clipFilename);
			success = false;
			continue;
		}

		sysMemStartTemp();
		delete clip;
		sysMemEndTemp();
	}

	Assert(m_AnimDictionary);
	builderLoader.BuildDictionary(*m_AnimDictionary, baseNameKeys);

	sysMemStartTemp();
	builderLoader.Shutdown();
	sysMemEndTemp();

	crAnimLoaderDictionary dictionaryLoader(m_AnimDictionary);

	m_Clips.Create(u16(numClips));
	m_BaseNameKeys = baseNameKeys;

	for(int i=0; i<numClips; ++i)
	{
		const char* clipFilename = (const char*)clipFilenames[i];

		ClipKey key = ConvertNameToKey(clipFilename);
		const crClip* existingClip = GetClip(key);
		if(existingClip)
		{
			if(!stricmp(existingClip->GetName(), clipFilename))
			{
				Warningf("crClipDictionary::LoadClips - attempted multiple addition of same clip '%s'", clipFilename);
				continue;
			}
			else
			{
				Errorf("crClipDictionary::LoadClips - hash clash '%s' == '%s' (%d) base name keys %d", clipFilename, (const char*)existingClip->GetName(), u32(key), baseNameKeys);
				success = false;
				continue;
			}
		}

		crClip* clip = loader->AllocateAndLoadClip(clipFilename, &dictionaryLoader);
		if(!clip)
		{
			Errorf("crClipDictionary::LoadClips - failed to load clip '%s'", clipFilename);
			success = false;
			continue;
		}

		clip->m_Dictionary = this;

		m_Clips.Insert(key, clip);
	}

	return success;
}

////////////////////////////////////////////////////////////////////////////////

crClipLoaderBasic::crClipLoaderBasic()
{
}

////////////////////////////////////////////////////////////////////////////////

crClipLoaderBasic::~crClipLoaderBasic()
{
}

////////////////////////////////////////////////////////////////////////////////

crClip* crClipLoaderBasic::AllocateAndLoadClip(const char* clipFilename, crAnimLoader* animLoader)
{
	return crClip::AllocateAndLoad(clipFilename, animLoader);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crClipDictionary* crClipDictionary::LoadResource(const char* dictionaryFilename)
{
	crClipDictionary *cdt = NULL;

	pgRscBuilder::Load(cdt, dictionaryFilename, "#cdt", crClipDictionary::RORC_VERSION);

	return cdt;
}

////////////////////////////////////////////////////////////////////////////////

crClipLoader::crClipLoader()
{
}

////////////////////////////////////////////////////////////////////////////////

crClipLoader::~crClipLoader()
{
}

////////////////////////////////////////////////////////////////////////////////

crClipLoaderDictionary::crClipLoaderDictionary()
: m_Dictionary(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crClipLoaderDictionary::crClipLoaderDictionary(const crClipDictionary* dictionary)
: m_Dictionary(NULL)
{
	Init(dictionary);
}

////////////////////////////////////////////////////////////////////////////////

crClipLoaderDictionary::~crClipLoaderDictionary()
{
}

////////////////////////////////////////////////////////////////////////////////

void crClipLoaderDictionary::Init(const crClipDictionary* dictionary)
{
	m_Dictionary = dictionary;
}

////////////////////////////////////////////////////////////////////////////////

crClip* crClipLoaderDictionary::AllocateAndLoadClip(const char* clipFilename, crAnimLoader*)
{
	if(m_Dictionary)
	{
		crClip* clip = const_cast<crClip*>(m_Dictionary->GetClip(clipFilename));
		if(clip)
		{
			clip->AddRef();
			return clip;
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////



} // namespace rage
