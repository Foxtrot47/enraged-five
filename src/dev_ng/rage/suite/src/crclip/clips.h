//
// crclip/clips.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRCLIP_CLIPS_H
#define CRCLIP_CLIPS_H

#include "atl/array.h"
#include "data/resource.h"
#include "data/struct.h"

#include "cranimation/animation_config.h"

namespace rage
{

class crAnimLoader;
class crClip;
class crClipLoader;
class crDumpOutput;
class datSerialize;


// PURPOSE: Specialized container class for multiple clip pointers
// Primarily used by parameterized motion, but available for general use
class crClips
{
public:

	// PURPOSE: Default constructor
	crClips();

	// PURPOSE: Resource constructor
	crClips(datResource&);

	// PURPOSE: Destructor
	~crClips();

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Placement
	DECLARE_PLACE(crClips);

	// PURPOSE: Shutdown, free/release all dynamic resources
	void Shutdown();


	// PURPOSE: Get number of clips contained
	// RETURNS: number of clips contained
	u32 GetNumClips() const;


	// PURPOSE: Get clip by index (const)
	// PARAMS: idx - clip index
	// RETURNS: pointer to clip
	const crClip* GetClip(u32 idx) const;

	// PURPOSE: Get clip by index (non-const)
	// PARAMS: idx - clip index
	// RETURNS: pointer to clip
	crClip* GetClip(u32 idx);

	// PURPOSE: Find clip index
	// PARAMS:
	// clip - pointer to clip
	// outIdx - index of clip, if found (otherwise unchanged)
	// RETURNS: true - clip found, false - clip not found
	bool FindClip(const crClip* clip, u32& outIdx) const;

	// TODO --- find clip (by name)


	// PURPOSE: Add clip
	// PARAMS: clip - pointer to new clip to append
	void AddClip(crClip* clip);

	// PURPOSE: Add all the clips from another clips container
	// PARAMS: clips - clips container to append clips from
	void AddClips(const crClips& clips);

	// PURPOSE: Set clip by index
	// PARAMS: idx - clip index
	// clip - new clip to set
	void SetClip(u32 idx, crClip* clip);

	// PURPOSE: Remove clip by index
	// PARAMS: idx - clip index
	void RemoveClip(u32 idx);


	// PURPOSE: Get clip via [] operator (const)
	// SEE ALSO: GetClip
	const crClip* operator[](u32 idx) const;

	// PURPOSE: Get clip via [] operator (const)
	// SEE ALSO: GetClip
	crClip* operator[](const u32 idx);

	// PURPOSE: size (provided for STL compatibility)
	// SEE ALSO: GetNumClips
	u32 size() const;

	// PURPOSE: clear (provided for STL compatibility)
	// SEE ALSO: Shutdown
	void clear();

#if CR_DEV
	// PURPOSE: Serialization
	// PARAMS:
	// loader - clip loader, NULL for default loading mechanism
	// subLoader - animation loader, NULL for default loading mechanism
	void Serialize(datSerialize&, crClipLoader* loader=NULL, crAnimLoader* subLoader=NULL);

	// PURPOSE: Dump output
	void Dump(crDumpOutput&) const;
#endif // CR_DEV

private:
	atArray<datRef<crClip> > m_Clips;
};


// inline functions

////////////////////////////////////////////////////////////////////////////////

inline u32 crClips::GetNumClips() const
{
	return m_Clips.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crClips::GetClip(u32 idx) const
{
	return m_Clips[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline crClip* crClips::GetClip(u32 idx)
{
	return m_Clips[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline bool crClips::FindClip(const crClip* clip, u32& outIdx) const
{
	if(clip)
	{
		const u32 numClips = m_Clips.GetCount();
		for(u32 i=0; i<numClips; ++i)
		{
			if(clip == m_Clips[i])
			{
				outIdx = i;
				return true;
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline void crClips::SetClip(u32 idx, crClip* clip)
{
	m_Clips[idx] = clip;
}

////////////////////////////////////////////////////////////////////////////////

inline const crClip* crClips::operator[](u32 idx) const
{
	return m_Clips[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline crClip* crClips::operator[](const u32 idx)
{
	return m_Clips[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crClips::size() const
{
	return m_Clips.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline void crClips::clear()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////


} //namespace rage
#endif //CRCLIPS_CLIPS_H


