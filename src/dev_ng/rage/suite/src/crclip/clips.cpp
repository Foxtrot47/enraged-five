//
// crclip/clips.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "clips.h"

#include "atl/array_struct.h"
#include "cranimation/animdictionary.h"
#include "crclip/clipdictionary.h"
#include "crmetadata/dumpoutput.h"
#include "data/safestruct.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crClips::crClips()
{
}

////////////////////////////////////////////////////////////////////////////////

crClips::crClips(datResource& rsc)
: m_Clips(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

crClips::~crClips()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crClips::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crClips)
	SSTRUCT_FIELD(crClips, m_Clips)
	SSTRUCT_END(crClips)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crClips);

////////////////////////////////////////////////////////////////////////////////

void crClips::Shutdown()
{
	const u32 numClips = m_Clips.GetCount();
	for(u32 i=0; i<numClips; ++i)
	{
		if(m_Clips[i])
		{
			m_Clips[i]->Release();
		}
	}
	m_Clips.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crClips::AddClip(crClip* clip)
{
	m_Clips.Grow() = clip;
	if(clip)
	{
		clip->AddRef();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClips::AddClips(const crClips& clips)
{
	const u32 numClips = clips.GetNumClips();
	for(u32 i=0; i<numClips; ++i)
	{
		AddClip(clips.m_Clips[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClips::RemoveClip(u32 idx)
{
	if(m_Clips[idx])
	{
		m_Clips[idx]->Release();
	}

	m_Clips.Delete(idx);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crClips::Serialize(datSerialize& s, crClipLoader* loader, crAnimLoader* subLoader)
{	
	crClipLoaderBasic defaultLoader;
	if(!loader)
	{
		loader = &defaultLoader;
	}

	crAnimLoaderBasic defaultSubLoader;
	if(!subLoader)
	{
		subLoader = &defaultSubLoader;
	}

	u32 numClips = GetNumClips();
	s << numClips;
	if(s.IsRead())
	{
		m_Clips.Resize(numClips);
	}

	for(u32 i=0; i<numClips; ++i)
	{
		char clipName[RAGE_MAX_PATH];
		if(!s.IsRead())
		{
			ASSET.MakeFolderRelative(clipName, RAGE_MAX_PATH, (const char*)m_Clips[i]->GetName());
		}

		s << datString(clipName, RAGE_MAX_PATH);

		if(s.IsRead())
		{
			char relPath[RAGE_MAX_PATH];
			ASSET.MakeFolderRelative(relPath, RAGE_MAX_PATH, clipName);

			m_Clips[i] = loader->AllocateAndLoadClip(relPath, subLoader);
			Assert(m_Clips[i]);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClips::Dump(crDumpOutput& output) const
{
	const u32 numClips = GetNumClips();
	output.Outputf(0, "numclips", "%d", numClips);

	for(u32 i=0; i<numClips; ++i)
	{
		output.Outputf(1, "clip", i, "%s", m_Clips[i]?m_Clips[i]->GetName():"NULL");
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage