//
// crclip/framecompositor.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "framecompositor.h"

#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crextra/expressions.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositor::Invalid()
{
	Composite(NULL, 0.f, 0.f);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorFrame::Composite(const crAnimation* anim, float time, float deltaTime)
{
	Assert(m_Frame);
	if(anim)
	{
		if(anim->HasMoverTracks() && !m_Absolute)
		{
			m_Frame->CompositeWithDelta(*anim, time, deltaTime);
		}
		else
		{
			m_Frame->Composite(*anim, time);
		}
	}
	else
	{
		m_Frame->Invalidate();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorFrame::ApplyExpressions(const crExpressions&, float)
{
	AssertMsg(0, "crFrameCompositeFrame::ApplyExpressions - expression not supported");
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorFrame::Blend(float UNUSED_PARAM(weight))
{
	AssertMsg(0, "crFrameCompositeFrame::Blend - blend not supported, requires a stack of frames");
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorFrame::CompositeBlend(float weight, const crAnimation* anim, float time, float deltaTime)
{
	Assert(m_Frame);
	if(anim)
	{
		if(anim->HasMoverTracks() && !m_Absolute)
		{
			m_Frame->CompositeWithDeltaBlend(weight, *anim, time, deltaTime);
		}
		else
		{
			m_Frame->CompositeBlend(weight, *anim, time);
		}
	}
	else if(weight >= 1.f)
	{
		m_Frame->Invalidate();
	}
}

////////////////////////////////////////////////////////////////////////////////

crFrameCompositorFrameLazy::crFrameCompositorFrameLazy(crFrame& frame, bool absolute)
: crFrameCompositorFrame(frame, absolute)
, m_Animation(NULL)
, m_Time(0.f)
, m_DeltaTime(0.f)
, m_Empty(true)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameCompositorFrameLazy::~crFrameCompositorFrameLazy()
{
	Finalize();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorFrameLazy::Composite(const crAnimation* anim, float time, float deltaTime)
{
	Finalize();

	if(anim)
	{
		m_Animation = anim;
		m_Time = time;
		m_DeltaTime = deltaTime;
	}
	else
	{
		m_Empty = false;
	}
}
////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorFrameLazy::ApplyExpressions(const crExpressions& exprs, float time)
{
	AssertMsg(!m_Empty, "crFrameCompositorFrameLazy::ApplyExpressions - unable to apply expressions, missing composited frame");

	crFrameCompositorFrame::ApplyExpressions(exprs, time);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorFrameLazy::Blend(float weight)
{
	AssertMsg(m_Animation && !m_Empty, "crFrameCompositorFrameLazy::Blend - unable to blend, missing lazy composite or composited frame");

	crFrameCompositorFrame::CompositeBlend(weight, m_Animation, m_Time, m_DeltaTime);

	m_Animation = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorFrameLazy::CompositeBlend(float weight, const crAnimation* anim, float time, float deltaTime)
{
	Finalize();
	
	AssertMsg(!m_Empty, "crFrameCompositorFrameLazy::CompositeBlend - unable to composite blend, missing composited frame");

	crFrameCompositorFrame::CompositeBlend(weight, anim, time, deltaTime);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorFrameLazy::Finalize()
{
	if(m_Animation)
	{
		AssertMsg(m_Empty, "crFrameCompositorFrameLazy::Finalize - unable to finalize, lazy composite but composite frame not empty, contents will overwritten");

		crFrameCompositorFrame::Composite(m_Animation, m_Time, m_DeltaTime);

		m_Animation = NULL;
		m_Empty = false;
	}
}

////////////////////////////////////////////////////////////////////////////////

crFrameCompositorDump::crFrameCompositorDump()
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameCompositorDump::~crFrameCompositorDump()
{
	for(int i=0; i<m_CompositeOps.GetCount(); ++i)
	{
		if(m_CompositeOps[i])
		{
			delete m_CompositeOps[i];
			m_CompositeOps[i] = NULL;
		}
	}

	m_CompositeOps.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorDump::Composite(const crAnimation* anim, float time, float deltaTime)
{
	CompositeOp* op = rage_new CompositeOp;

	op->m_OpType = CompositeOp::kCompositeOpTypeComposite;
	op->m_Animation = anim;
	op->m_Time = time;
	op->m_DeltaTime = deltaTime;

	m_CompositeOps.Grow() = op;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorDump::ApplyExpressions(const crExpressions& UNUSED_PARAM(exprs), float UNUSED_PARAM(time))
{
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorDump::Blend(float weight)
{
	Assert(m_CompositeOps.GetCount() >= 2);

	CompositeOp* childOp1 = m_CompositeOps.Pop();
	CompositeOp* childOp0 = m_CompositeOps.Pop();

	CompositeOp* op = rage_new CompositeOp;

	op->m_OpType = CompositeOp::kCompositeOpTypeBlend;
	op->m_Weight = weight;
	op->m_ChildOps.Grow() = childOp0;
	op->m_ChildOps.Grow() = childOp1;

	m_CompositeOps.Grow() = op;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorDump::CompositeBlend(float weight, const crAnimation* anim, float time, float deltaTime)
{
	Composite(anim, time, deltaTime);
	Blend(weight);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorDump::Dump(atString& outString, bool collapse) const
{
	if(m_CompositeOps.GetCount())
	{
		CompositeOp* op = m_CompositeOps.Top();

		op->Dump(outString, collapse);
	}
}

////////////////////////////////////////////////////////////////////////////////

crFrameCompositorDump::CompositeOp::CompositeOp()
: m_OpType(kCompositeOpTypeNone)
, m_Animation(NULL)
, m_Weight(0.f)
, m_Time(0.f)
, m_DeltaTime(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameCompositorDump::CompositeOp::~CompositeOp()
{
	for(int i=0; i<m_ChildOps.GetCount(); ++i)
	{
		if(m_ChildOps[i])
		{
			delete m_ChildOps[i];
			m_ChildOps[i] = NULL;
		}
	}

	m_ChildOps.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameCompositorDump::CompositeOp::Dump(atString& outString, bool collapse, float weight, int indent) const
{
	if(!collapse)
	{
		for(int i=0; i<indent; ++i)
		{
			outString += "  ";
		}
	}

	char buf[255];

	switch(m_OpType)
	{
	case kCompositeOpTypeComposite:
		if(collapse)
		{
			formatf(buf, "w %f %s t %f dt %f\n", weight, m_Animation?m_Animation->GetName():"NONE", m_Time, m_DeltaTime);
			outString += buf;
		}
		else
		{
			formatf(buf, "composite %s t %f dt %f\n", m_Animation?m_Animation->GetName():"NONE", m_Time, m_DeltaTime);
			outString += buf;
		}
		break;

	case kCompositeOpTypeBlend:
		if(!collapse)
		{
			formatf(buf, "blend w %f\n", m_Weight);
			outString += buf;
		}
		for(int i=0; i<m_ChildOps.GetCount(); ++i)
		{
			m_ChildOps[i]->Dump(outString, collapse, i?(weight*m_Weight):(weight*(1.f-m_Weight)), indent+1);
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
