//
// crclip/clipanimationexpression.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "clipanimationexpression.h"

#include "crclip/framecompositor.h"
#include "crextra/expressions.h"
#include "crmetadata/dumpoutput.h"
#include "file/asset.h"
#include "file/limits.h"
#include "paging/rscbuilder.h"
#include "string/string.h"
#include "system/memory.h"

namespace rage
{


////////////////////////////////////////////////////////////////////////////////

crClipAnimationExpression::crClipAnimationExpression()
: crClipAnimation(kClipTypeAnimationExpression)
, m_Expressions(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crClipAnimationExpression::crClipAnimationExpression(datResource& rsc)
: crClipAnimation(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crClipAnimationExpression::DeclareStruct(datTypeStruct& s)
{
	crClipAnimation::DeclareStruct(s);

	STRUCT_BEGIN(crClipAnimationExpression);
	STRUCT_FIELD(m_Expressions);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crClipAnimationExpression::~crClipAnimationExpression()
{
	if(m_Expressions)
	{
		m_Expressions->Release();
	}
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CLIP_TYPE(crClipAnimationExpression, crClip::kClipTypeAnimationExpression);

////////////////////////////////////////////////////////////////////////////////
#if CR_DEV
void crClipAnimationExpression::Serialize(datSerialize& s)
{
	crClipAnimation::Serialize(s);

	// serialize animation file name:
	char exprFile[RAGE_MAX_PATH];
	exprFile[0] = '\0';

	if(!s.IsRead() && m_Expressions)
	{
		bool pushedFolder = PushClipFolder();
		ASSET.MakeFolderRelative(exprFile, RAGE_MAX_PATH, m_Expressions->GetName());
		if(pushedFolder)
		{
			ASSET.PopFolder();
		}
	}

	s << datLabel("ExprFile:") << datString(exprFile, RAGE_MAX_PATH) << datNewLine;

	if(s.IsRead())
	{
		if(*exprFile != '\0')
		{
			bool rscLoad = pgRscBuilder::IsBuilding() && (&sysMemAllocator::GetCurrent() != &sysMemAllocator::GetMaster());
			m_Expressions = crExpressions::AllocateAndLoad(exprFile, rscLoad);
			m_Expressions->AddRef();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimationExpression::Dump(crDumpOutput& output) const
{
	crClipAnimation::Dump(output);

	output.Outputf(2, "expression", "'%s'", m_Expressions?m_Expressions->GetName():"NONE");
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crClipAnimationExpression* crClipAnimationExpression::AllocateAndCreate(const crAnimation& anim, crExpressions& expressions)
{
	crClipAnimationExpression* clip = static_cast<crClipAnimationExpression*>(CreateBase(kClipTypeAnimationExpression));
	if(clip)
	{
		if(clip->Create(anim, expressions))
		{
			return clip;
		}
		delete clip;
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crClipAnimationExpression::Create(const crAnimation& anim, crExpressions& expressions)
{
	Assert(!m_Expressions);
	m_Expressions = &expressions;
	m_Expressions->AddRef();

	return crClipAnimation::Create(anim);
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimationExpression::Composite(crFrameCompositor& compositor, float time, float deltaTime) const
{
	crClipAnimation::Composite(compositor, time, deltaTime);

	compositor.ApplyExpressions(*m_Expressions, time);
}

////////////////////////////////////////////////////////////////////////////////

bool crClipAnimationExpression::ForAllExpressions(ExpressionsDiscoveryCb cb, void* data) const
{
	return cb(m_Expressions, data);
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimationExpression::SetExpressions(crExpressions& expr)
{
	expr.AddRef();
	if(m_Expressions)
	{
		m_Expressions->Release();
	}
	m_Expressions = &expr;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
