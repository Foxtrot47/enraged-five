//
// crclip/clipplayer.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "clipplayer.h"

#include "cranimation/animation.h"
#include "crclip/framecompositor.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tag.h"
#include "crmetadata/tagiterators.h"
#include "crmetadata/tags.h"
#include "math/simplemath.h"

namespace rage
{

#if CR_DEV
crClipPlayer::TelemetryCallback crClipPlayer::sm_Telemetry = NULL;
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crClipPlayer::crClipPlayer()
: m_Time(0.f)
, m_BlockedTime(0.f)
, m_PrevTime(0.f)
, m_LastTime(0.f)
, m_Delta(0.f)
, m_DeltaSupplement(0.f)
, m_DeltaHiddenSupplement(0.f)
, m_Rate(1.f)
, m_CachedDuration(0.f)
, m_IgnoreClipLooping(false)
, m_ForceLooping(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crClipPlayer::crClipPlayer(const crClip* clip)
: m_Time(0.f)
, m_BlockedTime(0.f)
, m_PrevTime(0.f)
, m_LastTime(0.f)
, m_Delta(0.f)
, m_DeltaSupplement(0.f)
, m_DeltaHiddenSupplement(0.f)
, m_Rate(1.f)
, m_CachedDuration(0.f)
, m_IgnoreClipLooping(false)
, m_ForceLooping(false)
{
	Init(clip);
}

////////////////////////////////////////////////////////////////////////////////

crClipPlayer::crClipPlayer(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crClipPlayer::~crClipPlayer()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crClipPlayer);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crClipPlayer::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crClipPlayer);
	STRUCT_FIELD(m_Time);
	STRUCT_FIELD(m_BlockedTime);
	STRUCT_FIELD(m_PrevTime);
	STRUCT_FIELD(m_LastTime);
	STRUCT_FIELD(m_Delta);
	STRUCT_FIELD(m_DeltaSupplement);
	STRUCT_FIELD(m_DeltaHiddenSupplement);
	STRUCT_FIELD(m_Rate);
	STRUCT_FIELD(m_Clip);
	STRUCT_FIELD(m_IgnoreClipLooping);
	STRUCT_FIELD(m_ForceLooping);
	STRUCT_IGNORE(m_ClipCallback);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crClipPlayer::Init(const crClip* clip)
{
	SetClip(clip);
}

////////////////////////////////////////////////////////////////////////////////

void crClipPlayer::Shutdown()
{
	SetClip(NULL);
}

////////////////////////////////////////////////////////////////////////////////

void crClipPlayer::Update(float deltaTime, float& outUnusedTime, bool& outHasLoopedOrEnded, const TagTriggerFunctor* tagTriggerFunctor, const LoopIdFunctor* loopIdFunctor)
{
	Assert(deltaTime >= 0.f);

	m_Delta = 0.f;

	outUnusedTime = 0.f;
	outHasLoopedOrEnded = false;

	float internalDelta = (deltaTime * m_Rate) + m_DeltaSupplement;

	m_DeltaSupplement = 0.f;

	static const crTag::Key loopKey = crTag::CalcKey("Loop", 0x4C633D07);
	static const crProperty::Key loopIdKey = crProperty::CalcKey("LoopId", 0x7721DC03);

	m_Time += internalDelta;

	// valid clip
	if(m_Clip != NULL)
	{		
		const float duration = m_CachedDuration;
		bool isLooped = IsLooped();

		if(!IsNearZero(internalDelta))
		{
			float prevTime = m_Time;
			float start = 0.f;
			float end = duration;

			if(Unlikely(loopIdFunctor))
			{
				if(m_Clip->HasTags())
				{
					const crTags& tags = *m_Clip->GetTags();
					if(tags.HasTags(loopKey))
					{
						float prevPhase = prevTime / duration;
						crTagIterator it(tags, prevPhase, prevPhase+SMALL_FLOAT, loopKey);

						const crTag* bestLoopTag = NULL;
						int bestLoopId = INT_MAX;
						while(*it)
						{
							const crTag* loopTag =*it;

							int loopId = INT_MAX;
							const crPropertyAttribute* attribLoopId = loopTag->GetProperty().GetAttribute(loopIdKey);
							if(attribLoopId && attribLoopId->GetType() == crPropertyAttribute::kTypeInt)
							{
								loopId = static_cast<const crPropertyAttributeInt*>(attribLoopId)->GetInt();
							}

							if((*loopIdFunctor)(loopId))
							{
								if(loopId < bestLoopId)
								{
									bestLoopTag = loopTag;
									bestLoopId = loopId;
								}
							}
							++it;
						}

						if(bestLoopTag)
						{
							start = bestLoopTag->GetStart() * duration;
							end = bestLoopTag->GetEnd() * duration;
							isLooped = true;
						}
					}
				}
			}
			
			// moving forward in clip
			if(internalDelta > 0.f)
			{
				// at or beyond end of clip
				if(m_Time < end)
				{
					m_Delta = internalDelta;
				}
				else
				{
					float remainingTime = Min(internalDelta, m_Time - end);
					if(isLooped)
					{
						// if looped, use remaining time at start of clip (large deltas could wrap multiple times)
						m_Time = start + ((remainingTime <= (end-start)) ? remainingTime : fmodf(remainingTime, end-start));
						m_Delta = internalDelta;

						outHasLoopedOrEnded = true;
					}
					else
					{
						// if not looped, clamp to end and return wasted time
						m_Time = end;
						m_Delta = Max(internalDelta - remainingTime, 0.f);

						outHasLoopedOrEnded = (m_PrevTime < end);
						outUnusedTime = Min(remainingTime / m_Rate, deltaTime);
					}
				}
			}
			else  // moving backwards in clip
			{
				// at or beyond start of clip
				if(m_Time >= start)
				{
					m_Delta = internalDelta;
				}
				else
				{
					float remainingTime = Max(internalDelta, m_Time - start);
					if(isLooped)
					{
						// if looped, use remaining time at end of clip (large deltas could wrap multiple times)
						m_Time = end - ((-remainingTime <= (end-start)) ? (-remainingTime) : (fmodf(-remainingTime, end-start)));
						m_Delta = internalDelta;

						outHasLoopedOrEnded = true;
					}
					else
					{
						// if not looped, clamp to start and return wasted time
						m_Time = start;
						m_Delta = Min(internalDelta - remainingTime, 0.f);

						outHasLoopedOrEnded = (m_PrevTime > 0.f);
						outUnusedTime = Min(remainingTime / m_Rate, deltaTime);
					}
				}
			}
		}

		// report tag trigger events?
		const crTags* tags = m_Clip->GetTags();
		if(tags != NULL && tags->HasTags())
		{
			if(tagTriggerFunctor != NULL)
			{
				if(!IsNearZero(m_Delta) || m_LastTime != m_Time)
				{
					const float invDuration = 1.f / duration;

					float startPhase = Clamp(m_LastTime * invDuration, 0.f, 1.f);
					float endPhase = Clamp(m_Time * invDuration, 0.f, 1.f);
					
					float phase = endPhase;

					float deltaPhase = endPhase-startPhase;
					
					// unwrap delta phase, and swap start/end if playing backwards
					if(internalDelta >= 0.f)
					{
						if(Unlikely(deltaPhase < 0.f))
						{
							deltaPhase += 1.f;
						}
					}
					else
					{
						SwapEm(startPhase, endPhase);
						if(deltaPhase > 0.f)
						{
							deltaPhase -= 1.f;
						}
					}

					if((startPhase < endPhase) || (isLooped && outHasLoopedOrEnded))
					{
						crTagIteratorKey<false> it(*tags, startPhase, endPhase);
						while(*it)
						{
							const crTag* tag = *it;

							float tagStart = tag->GetStart();
							float tagEnd = tag->GetEnd();
							if(internalDelta < 0.f)
							{
								SwapEm(tagStart, tagEnd);
							}

							if(!tag->Contains(startPhase) || (startPhase == tagStart && startPhase != endPhase))
							{
								(*tagTriggerFunctor)(*tag, true, internalDelta > 0.f, fabsf(phase - tagStart));
							}
							(*tagTriggerFunctor)(*tag, false, internalDelta > 0.f, deltaPhase);
							if(!tag->Contains(endPhase) || tagEnd == endPhase)
							{
								(*tagTriggerFunctor)(*tag, true, internalDelta < 0.f, fabsf(phase - tagEnd));
							}

							++it;
						}
					}
				}
			}

			m_BlockedTime = m_Clip->CalcBlockedTime(m_Time);
		}
		else
		{
			m_BlockedTime = m_Time;
		}
	}

	m_Delta += m_DeltaHiddenSupplement;
	m_DeltaHiddenSupplement = 0.f;
	m_PrevTime = m_Time;
	m_LastTime = m_Time;
}

////////////////////////////////////////////////////////////////////////////////

void crClipPlayer::Composite(crFrameCompositor& inoutCompositor)
{
	if(m_Clip)
	{
		m_Clip->Composite(inoutCompositor, m_Time, m_Delta);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crClipPlayer::Composite(crFrame& inoutFrame)
{
	if(m_Clip)
	{
		return m_Clip->Composite(inoutFrame, m_Time, m_Delta);
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crClipPlayer::SetClip(const crClip* clip)
{
	const crClip* oldClip = m_Clip;
	if(clip != oldClip)
	{
		if(clip)
		{
#if CR_DEV
			if(sm_Telemetry)
			{
				sm_Telemetry(clip);
			}
#endif // CR_DEV
			m_CachedDuration = clip->GetDuration();
			Assertf(m_CachedDuration > 0.f, "Invalid clip duration for %s", clip->GetName());
			clip->AddRef();
			m_Time = Min(m_Time, m_CachedDuration);
			m_LastTime = Min(m_LastTime, m_CachedDuration);
			m_PrevTime = Min(m_PrevTime, m_CachedDuration);
			m_BlockedTime = clip->CalcBlockedTime(m_Time);
		}
		else
		{
			m_CachedDuration = 0.f;
		}

		m_Clip = clip;

		if(oldClip)
		{
			oldClip->Release();
		}

		if(m_ClipCallback)
		{
			m_ClipCallback();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClipPlayer::SetTime(float time)
{
	if(m_Clip)
	{
		m_Time = Clamp(time, 0.f, m_CachedDuration);
		m_BlockedTime = m_Clip->CalcBlockedTime(m_Time);
	}
	else
	{
		m_Time = Max(time, 0.f);
	}
	m_LastTime = m_Time;
}

////////////////////////////////////////////////////////////////////////////////

void crClipPlayer::SetTime(float time, bool skip)
{
	if(m_Clip)
	{
		m_Time = Clamp(time, 0.f, m_CachedDuration);
		m_BlockedTime = m_Clip->CalcBlockedTime(m_Time);
	}
	else
	{
		m_Time = Max(time, 0.f);
	}

	if(skip)
	{
		m_LastTime = m_Time;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClipPlayer::SetFunctor(const Functor0& functor)
{
	m_ClipCallback = functor;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
