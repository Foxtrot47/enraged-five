//
// crclip/clipsanimations.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "clipanimations.h"


#include "atl/array_struct.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/frameinitializers.h"
#include "crclip/framecompositor.h"
#include "crmetadata/dumpoutput.h"
#include "file/asset.h"

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

crClipAnimations::crClipAnimations()
: crClip(kClipTypeAnimations)
, m_TotalDuration(0.f)
, m_Parallel(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crClipAnimations::crClipAnimations(datResource& rsc)
: crClip(rsc)
, m_Animations(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crClipAnimations::DeclareStruct(datTypeStruct& s)
{
	crClip::DeclareStruct(s);

	STRUCT_BEGIN(crClipAnimations);
	STRUCT_FIELD(m_Animations);
	STRUCT_FIELD(m_TotalDuration);
	STRUCT_FIELD(m_Parallel);
	STRUCT_IGNORE(m_Padding);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crClipAnimations::~crClipAnimations()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_CLIP_TYPE(crClipAnimations, crClip::kClipTypeAnimations);

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::Shutdown()
{
	m_Animations.Reset();
	m_TotalDuration = 0.f;
	m_Parallel = false;

	crClip::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crClipAnimations::Serialize(datSerialize& s)
{
	crClip::Serialize(s);

	// serialize the animation count
	int numAnims = m_Animations.GetCount();
	s << datLabel("AnimCount:") << numAnims << datNewLine;
	m_Animations.Resize(numAnims);

	// serialize each animation file
	for(int i=0; i <numAnims; ++i)
	{
		m_Animations[i].Serialize(s, this);
	}

	// serialize clip duration
	s << datLabel("TotalDuration:") << m_TotalDuration << datNewLine;

	if(s_ClipVersion >= 13)
	{
		s << datLabel("Parallel:") << m_Parallel << datNewLine;
	}

	crClip::PostSerialize(s);
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::Dump(crDumpOutput& output) const
{
	crClip::Dump(output);

	output.Outputf(2, "parallel", "'%d'", m_Parallel);

	for(int i=0; i<m_Animations.GetCount(); ++i)
	{
		m_Animations[i].Dump(output);
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crClipAnimations* crClipAnimations::AllocateAndCreate()
{
	crClipAnimations* clip = static_cast<crClipAnimations*>(CreateBase(kClipTypeAnimations));
	if(clip)
	{
		if(clip->Create())
		{
			return clip;
		}
		delete clip;
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crClipAnimations::Create(bool parallel)
{
	// release any existing animations
	m_Animations.Reset();
	m_TotalDuration = 0.f;
	m_Parallel = parallel;

	CreateTags();
	CreateProperties();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::AddAnimation(const crAnimation& anim)
{
	m_Animations.Grow().Init(anim);
	m_TotalDuration = CalcTotalDuration();
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::SetParallel(bool parallel)
{
	m_Parallel = parallel;
	m_TotalDuration = CalcTotalDuration();
}

////////////////////////////////////////////////////////////////////////////////

float crClipAnimations::GetDuration() const
{
	return m_TotalDuration;
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::Composite(crFrameCompositor& compositor, float time, float deltaTime) const
{
	time = CalcBlockedTime(time);

	const int numAnimations = m_Animations.GetCount();
	if(numAnimations > 0)
	{
		if(m_Parallel)
		{
			for(int i=0; i<numAnimations; ++i)
			{
				m_Animations[i].Composite(compositor, time, deltaTime);
			}
		}
		else
		{
			int i=0;
			for(; i<(numAnimations-1); ++i)
			{
				float duration = m_Animations[i].GetDuration();
				if(time > duration)
				{
					time -= duration;
				}
				else
				{
					break;
				}
			}

			m_Animations[i].Composite(compositor, time, deltaTime);
		}
	}
	else
	{
		compositor.Invalid();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::InitDofs(crFrameData& frameData, crFrameFilter* filter) const
{
	const int numAnimations = m_Animations.GetCount();
	if(numAnimations > 0)
	{
		if(m_Parallel)
		{
			for(int i=0; i<numAnimations; ++i)
			{
				crFrameDataInitializerAnimation initializer(*m_Animations[i].GetAnimation(), false, filter);
				initializer.InitializeFrameData(frameData);
			}
		}
		else
		{
			crFrameDataInitializerAnimation initializer(*m_Animations[0].GetAnimation(), false, filter);
			initializer.InitializeFrameData(frameData);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crClipAnimations::HasDof(u8 track, u16 id) const
{
	const int numAnimations = m_Animations.GetCount();
	if(numAnimations > 0)
	{
		if(m_Parallel)
		{
			for(int i=0; i<numAnimations; ++i)
			{
				const Animation& anim = m_Animations[i];

				if(anim.GetAnimation() && anim.GetAnimation()->HasDof(track, id))
				{
					return true;
				}
			}
		}
		else
		{
			return m_Animations[0].GetAnimation() && m_Animations[0].GetAnimation()->HasDof(track, id);
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crClipAnimations::ForAllAnimations(AnimationDiscoveryCb cb, void* data) const
{
	const int numAnimations = m_Animations.GetCount();
	for(int i=0; i<numAnimations; ++i)
	{
		const Animation& anim = m_Animations[i];

		float start, end;
		anim.GetStartEndTimes(start, end);

		if(!cb(anim.GetAnimation(), start, end, anim.GetRate(), data))
		{
			return false;
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::SetAnimation(int idx, const crAnimation& anim)
{
	m_Animations[idx].Shutdown();
	m_Animations[idx].Init(anim);
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::SetStartEndTimes(int idx, float startTime, float endTime)
{
	m_Animations[idx].SetStartEndTimes(startTime, endTime);
	m_TotalDuration = CalcTotalDuration();
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::SetRate(int idx, float rate)
{
	m_Animations[idx].SetRate(rate);
	m_TotalDuration = CalcTotalDuration();
}

////////////////////////////////////////////////////////////////////////////////

float crClipAnimations::CalcTotalDuration() const 
{
	float totalDuration = 0.f;
	if(m_Parallel)
	{
		for(int i=0; i<m_Animations.GetCount(); ++i)
		{
			totalDuration = Max(totalDuration, m_Animations[i].GetDuration());
		}
	}
	else
	{
		for(int i=0; i<m_Animations.GetCount(); ++i)
		{
			totalDuration += m_Animations[i].GetDuration();
		}
	}
	return totalDuration;
}

////////////////////////////////////////////////////////////////////////////////

crClipAnimations::Animation::Animation()
: m_StartTime(0.f)
, m_EndTime(0.f)
, m_Rate(1.f)
, m_Animation(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crClipAnimations::Animation::Animation(const Animation& other)
: m_StartTime(other.m_StartTime)
, m_EndTime(other.m_EndTime)
, m_Rate(other.m_Rate)
, m_Animation(other.m_Animation)
{
	if(m_Animation)
	{
		m_Animation->AddRef();
	}
}

////////////////////////////////////////////////////////////////////////////////

crClipAnimations::Animation::Animation(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crClipAnimations::Animation::~Animation()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::Animation::Init(const crAnimation& anim)
{
	m_Animation = &anim;
	m_Animation->AddRef();

	m_StartTime = 0.f;
	m_EndTime = m_Animation->GetDuration();
	Assert(m_EndTime >= m_StartTime);

	m_Rate = 1.f;
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::Animation::Shutdown()
{
	if(m_Animation)
	{
		m_Animation->Release();
		m_Animation = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crClipAnimations::Animation::Serialize(datSerialize& s, crClipAnimations* clip)
{
	char animFile[RAGE_MAX_PATH];
	animFile[0] = '\0';

	if(m_Animation)
	{
		clip->MakeAnimRelative(animFile, RAGE_MAX_PATH, m_Animation->GetName());
	}

	s << datLabel("AnimFile:") << datString(animFile, RAGE_MAX_PATH) << datNewLine;
	s << datLabel("StartEndTimes:") << m_StartTime << m_EndTime << datNewLine;
	s << datLabel("Rate:") << m_Rate << datNewLine;

	if(s.IsRead())
	{
		Assert(!m_Animation);
		m_Animation = clip->AllocateAndLoadAnimation(animFile);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::Animation::Dump(crDumpOutput& output) const
{
	output.Outputf(2, "animation", "'%s'", m_Animation?m_Animation->GetName():"NONE");
	output.Outputf(2, "starttime", "%f", m_StartTime);
	output.Outputf(2, "endtime", "%f", m_EndTime);
	output.Outputf(2, "rate", "%f", m_Rate);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crClipAnimations::Animation::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(Animation);
	STRUCT_FIELD(m_StartTime);
	STRUCT_FIELD(m_EndTime);
	STRUCT_FIELD(m_Rate);
	STRUCT_FIELD(m_Animation);
	STRUCT_END();
}
#endif //__DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::Animation::Composite(crFrameCompositor& compositor, float time, float deltaTime) const
{
	time = Clamp(time*m_Rate+m_StartTime, m_StartTime, m_EndTime);
	deltaTime *= m_Rate;

	compositor.Composite(m_Animation, time, deltaTime);
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::Animation::SetStartEndTimes(float startTime, float endTime)
{
	Assert(startTime >= 0.f && endTime >= startTime);
	m_StartTime = Clamp(startTime, 0.f, m_Animation->GetDuration());
	m_EndTime = Clamp(endTime, 0.f, m_Animation->GetDuration());
}

////////////////////////////////////////////////////////////////////////////////

void crClipAnimations::Animation::SetRate(float rate)
{
	Assert(rate > 0.f);
	m_Rate = rate;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
