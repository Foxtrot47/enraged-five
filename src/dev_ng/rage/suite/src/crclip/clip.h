//
// crclip/clip.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//


#ifndef CRCLIP_CLIP_H
#define CRCLIP_CLIP_H

#include "atl/array.h"
#include "atl/referencecounter.h"
#include "atl/string.h"
#include "cranimation/animation_config.h"
#include "crmetadata/tags.h"
#include "paging/base.h"
#include "system/interlocked.h"
#include "system/memory.h"

namespace rage
{

class crAnimation;
class crAnimLoader;
class crClipDictionary;
class crDumpOutput;
class crExpressions;
class crFrame;
class crFrameCompositor;
class crFrameData;
class crFrameFilter;
class crProperties;
class crTags;
class datSerialize;
class fiStream;


// PURPOSE: Abstract base class that represents a motion clip.
// Clips are a higher level wrapper for "animated" data.
// They provide additional features, such as metadata (eg properties, tags, events)
// and internally hide their storage implementation details.
class crClip : public pgBase
{
public:

	// PURPOSE: Enumeration of different built in derived clip types
	enum eClipType
	{
		kClipTypeNone,

		kClipTypeAnimation,
		kClipTypeAnimations,
		kClipTypeAnimationExpression,
		// RESERVED - for more Rage specific clip types only

		// this must follow the list of built in clip types
		kClipTypeNum,

		// custom clip types *must* have a unique id >= kClipTypeCustom
		// kClipTypeCustom = 0x80, // TODO - project specific clip types not supported yet.
	};

	// PURPOSE: Enumeration of referencing options
	enum eRefOption
	{
		kRefOptionClipOnly		= 0,
		kRefOptionAndDictionary	= 1,
	};

	// PURPOSE: Constructor
	// PARAMS: clipType - derived constructor must pass in clip type identifier
	// (see crClip::eClipType enumeration for built in clip types)
	crClip(eClipType clipType=kClipTypeNone);

	// PURPOSE: Copy constructor
	crClip(const crClip&);

	// PURPOSE: Resource constructor
	crClip(datResource&);

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Off line resourcing version
	static const int RORC_VERSION = 12;

	// PURPOSE: Placement
	static void Place(void*, datResource&);

	// PURPOSE: Fixes up derived type from pointer to base class only
	static void VirtualConstructFromPtr(datResource&, crClip* base);

	// PURPOSE: Destructor
	virtual ~crClip();

	// PURPOSE: Shutdown, free/release all dynamic resources
	virtual void Shutdown();

	// PURPOSE: Add reference count
	void AddRef() const;

	// PURPOSE: Release reference count, destroy if count reaches zero
	// RETURNS: New reference count
	u32 Release(eRefOption refOption = kRefOptionAndDictionary) const;

	// PURPOSE: Get current reference count
	u32 GetRef() const;


	// PURPOSE: Clip factory
	// PARAMS: clip type identifier
	// (see crClip::eClipType enumeration for built in clip types)
	// RETURNS: pointer to newly created clip, or NULL if clip type unknown
	static crClip* Allocate(eClipType clipType);

	// PURPOSE: Clone a clip
	virtual crClip* Clone() const = 0;


	// PURPOSE: Get clip type
	// RETURNS: clip type identifier
	// (see crClip::eClipType enumeration for built in clip types)
	eClipType GetType() const;

	// PURPOSE: Get clip name
	// RETURNS: const clip name string
	const char* GetName() const;
	
	// PURPOSE: Set clip name
	// PARAMS: name - new clip name
	void SetName(const char* name);

	// PURPOSE: Does clip loop?
	// RETURNS: true - clip loops, false - non-looping (one shot)
	bool IsLooped() const;

	// PURPOSE: Set clip looping status
	// PARAMS: isLooped - should the clip loop
	void SetLooped(bool isLooped);

	// PURPOSE: Is this clip coming from the resource heap
	// RETURNS: true - the clip was resourced
	bool IsResource() const;

	// PURPOSE: Get dictionary clip is contained within
	// RETURNS: Pointer to dictionary (NULL if clip is independent - not contained within a dictionary)
	const crClipDictionary* GetDictionary() const;


	// PURPOSE: Get clip duration
	// RETURNS: clip duration, in seconds
	virtual float GetDuration() const = 0;


	// PURPOSE: Get clip duration in 30Hz frames
	// RETURNS: clip duration, in 30Hz frames
	// NOTES: May not be a whole number.
	// As this is dealing with duration, not a time index,
	// the behavior is slightly different from ConvertTimeTo30Frame,
	// GetNum30Frames adds an extra frame to represent the end of the clip.
	float GetNum30Frames() const;


	// PURPOSE: Convert clip phase to 30 Hz frames
	// PARAMS: phase - clip phase [0..1]
	// RETURNS: 30 Hz frames
	float ConvertPhaseTo30Frame(float phase) const;

	// PURPOSE: Convert clip phase to time (in seconds)
	// PARAMS: phase - clip phase [0..1]
	// RETURNS: time (in seconds)
	float ConvertPhaseToTime(float phase) const;

	// PURPOSE: Convert 30 Hz frames to clip phase
	// PARAMS: frame - time in 30 Hz frames
	// RETURNS: clip phase
	float Convert30FrameToPhase(float frame) const;

	// PURPOSE: Convert 30 Hz frames to clip phase
	// PARAMS: frame - time in 30 Hz frames
	// RETURNS: time (in seconds)
	float Convert30FrameToTime(float frame) const;

	// PURPOSE: Convert time (in seconds) to clip phase
	// PARAMS: time - time (in seconds)
	// RETURNS: clip phase
	float ConvertTimeToPhase(float time) const;

	// PURPOSE: Convert time (in seconds) to 30 Hz frames
	// PARAMS: time - time (in seconds)
	// RETURNS: 30 Hz frames
	float ConvertTimeTo30Frame(float time) const;

	
	// PURPOSE: Composite a frame, using a compositor
	// Compositor will gather steps required to composite the clip into a frame
	// PARAMS:
	// inoutCompositor - frame compositor
	// NOTES: Time will be clamped to legal range [0..duration]
	virtual void Composite(crFrameCompositor& inoutCompositor, float time, float deltaTime) const = 0;

	// PURPOSE: Composite a frame.
	// Where the degrees of freedom specified within a frame match those
	// provided by the clip, fill out those values.
	// PARAMS: inoutFrame - frame to composite
	// time - current time at which to retrieve dof values (in seconds).
	// deltaTime - time that has elapsed since last retrieval
	// RETURNS: true - if all dofs within frame matched, false - if partial/no match
	// NOTES: Time will be clamped to legal range [0..duration].
	bool Composite(crFrame& inoutFrame, float time, float deltaTime) const;

	// PURPOSE: Composite a frame, with no delta.
	// Where the degrees of freedom specified within a frame match those
	// provided by the clip, fill out those values.
	// Mover degrees of freedom will be returned as absolute values
	// PARAMS: inoutFrame - frame to composite
	// time - current time at which to retrieve dof values (in seconds).
	// RETURNS: true - if all dofs within frame matched, false - if partial/no match
	// NOTES: Time will be clamped to legal range [0..duration].
	bool Composite(crFrame& inoutFrame, float time) const;

	// PURPOSE: Set up the degrees of freedom found in the clip in the frame data.
	// PARAMS: frameData - frame data to create degrees of freedom in.
	// filter - optional filter to control which degrees of freedom are created (default NULL)
	virtual void InitDofs(crFrameData& frameData, crFrameFilter* filter=NULL) const = 0;

	// PURPOSE: Does the clip contain this degree of freedom
	virtual bool HasDof(u8 track, u16 id) const = 0;


	// PURPOSE: Does this clip have a property manager
	// RETURNS: true - clip has a property manager, false - no property manager
	bool HasProperties() const;

	// PURPOSE: Get the property manager
	// RETURNS: Pointer to the property manager, can be NULL if clip doesn't have properties
	const crProperties* GetProperties() const;

	// PURPOSE: Get the property manager (non-const)
	// RETURNS: Pointer (non-const) to the property manager, can be NULL if clip doesn't have properties
	crProperties* GetProperties();

	// PURPOSE: Does this clip have a tag manager
	// RETURNS: true - clip has a tag manager, false - no tag manager
	bool HasTags() const;

	// PURPOSE: Get the tag manager
	// RETURNS: Pointer to the tag manager, can be NULL if clip doesn't have tags
	const crTags* GetTags() const;

	// PURPOSE: Get the tag manager (non-const)
	// RETURNS: Pointer (non-const) to the tag manager, can be NULL if clip doesn't have tags
	crTags* GetTags();


	// PURPOSE: Defines callback for animation discovery
	typedef bool (*AnimationDiscoveryCb)(const crAnimation* anim, float start, float end, float rate, void* data);

	// PURPOSE: Discovers all animations used within derived clips (if any), and invokes callback for each of them
	virtual bool ForAllAnimations(AnimationDiscoveryCb cb, void* data) const;

	// PURPOSE: Defines callback for animation discovery
	typedef bool (*ExpressionsDiscoveryCb)(const crExpressions* clip, void* data);

	// PURPOSE: Discovers all expressions used within derived clips (if any), and invokes callback for each of them
	virtual bool ForAllExpressions(ExpressionsDiscoveryCb cb, void* data) const;


	// PURPOSE: Init class
	static void InitClass();

	// PURPOSE: Shutdown class
	static void ShutdownClass();

	// PURPOSE: Definition of clip allocation call
	typedef crClip* ClipAllocateFn();

	// PURPOSE: Definition of clip placement call
	typedef void ClipPlaceFn(datResource&, crClip*);

	// PURPOSE: Clip type information
	struct ClipTypeInfo
	{
	public:

		// PURPOSE: Constructor
		// PARAMS: clipType - clip type identifier (see crClip::eClipType enumeration for built in clip types)
		// clipName - clip name string
		// allocateFn - clip allocation call
		// placeFn - clip resource placement call
		ClipTypeInfo(eClipType clipType, const char* clipName, ClipAllocateFn* allocateFn, ClipPlaceFn* placeFn);

		// PURPOSE: Destructor
		~ClipTypeInfo();

		// PURPOSE: Registration call
		void Register() const;

		// PURPOSE: Get clip type identifier
		// RETURNS: clip type identifier (see crClip::eClipType enumeration for built in clip types)
		eClipType GetClipType() const;

		// PURPOSE: Get clip name string
		// RETURNS: string of clip's name
		const char* GetClipName() const;

		// PURPOSE: Get clip allocation function
		// RETURNS: clip allocation function pointer
		ClipAllocateFn* GetAllocateFn() const;

		// PURPOSE: Get clip resource placement function
		// RETURNS: clip resource placement function pointer
		ClipPlaceFn* GetPlaceFn() const;

	private:
		eClipType m_ClipType;
		const char* m_ClipName;
		ClipAllocateFn* m_AllocateFn;
		ClipPlaceFn* m_PlaceFn;		
	};

	// PURPOSE: Get info about a clip type
	// PARAMS: clipType - clip type identifier
	// (see crClip::eClipType enumeration for built in clip types)
	// RETURNS: const pointer to clip type info structure (may be NULL if clip type unknown)
	static const ClipTypeInfo* FindClipTypeInfo(eClipType clipType);

	// PURPOSE: Get info about this clip
	// RETURNS: const reference to clip type info structure about this clip
	virtual const ClipTypeInfo& GetClipTypeInfo() const = 0;

	// PURPOSE: Declare functions necessary to register a new clip type
	#define CR_DECLARE_CLIP_TYPE(__typename) \
		static crClip* AllocateClip(); \
		static void PlaceClip(datResource& rsc, crClip* base); \
		virtual crClip* Clone() const; \
		static void InitClass(); \
		virtual const ClipTypeInfo& GetClipTypeInfo() const; \
		static const crClip::ClipTypeInfo sm_ClipTypeInfo;

	// PURPOSE: Implement functions necessary to register a new clip type
	#define CR_IMPLEMENT_CLIP_TYPE(__typename, __typeid) \
		crClip* __typename::AllocateClip() \
		{ \
			__typename* clip = rage_aligned_new(16) __typename; \
			return clip; \
		} \
		void __typename::PlaceClip(datResource& rsc, crClip* base) \
		{ \
			::new (base) __typename(rsc); \
		} \
		rage::crClip* __typename::Clone() const \
		{ \
			__typename* clip = rage_aligned_new(16) __typename(*this); \
			return clip; \
		} \
		void __typename::InitClass() \
		{ \
			sm_ClipTypeInfo.Register(); \
		} \
		const crClip::ClipTypeInfo& __typename::GetClipTypeInfo() const \
		{ \
			return sm_ClipTypeInfo; \
		} \
		const crClip::ClipTypeInfo __typename::sm_ClipTypeInfo(__typeid, CR_DEV ? #__typename :  NULL, AllocateClip, PlaceClip);

#if CR_DEV
	// PURPOSE: Allocates and loads a clip from file
	// PARAMS:
	// fileName - clip file name
	// loader - optional animation loader, manages loading of any required animations (default NULL)
	// RETURNS: pointer to newly constructed clip, or NULL if load failed.
	static crClip* AllocateAndLoad(const char* fileName, crAnimLoader* loader=NULL);

	// PURPOSE: Saves a clip
	// PARAMS: fileName - clip file name
	// RETURNS: true - if save succeeded, false - if save failed
	bool Save(const char* fileName);

	// PURPOSE: Serialize a clip
	// NOTES: Derived classes will need to implement custom file serialization here
	virtual void Serialize(datSerialize&);

	// PURPOSE: Allocate and load an animation for a derived class
	crAnimation* AllocateAndLoadAnimation(const char* animFilename) const;

	// PURPOSE: Dump clip output, for debugging
	// PARAMS: output - dump output object to gather debugging output into
	virtual void Dump(crDumpOutput& output) const;
	
	// PURPOSE: Dump all the clip dofs
	// PARAMS: inoutText - string buffer to output dump text into
	void DumpDofs(crDumpOutput& output) const;

	// PURPOSE: Dump clip to text output
	// PARAMS: inoutText - string buffer to output dump text into
	void Dump(atString& inoutText) const;

	// PURPOSE: Get clip type name
	// RETURNS: const pointer to clip's type name (ie string version of class name)
	const char* GetTypeName() const;
#endif // CR_DEV


	// PURPOSE: Applies any block tags to supplied time
	// PARAMS: time - input time, before any blocking
	// RETURNS: time once any blocks have been applied.
	float CalcBlockedTime(float time) const;

	// PURPOSE: Tests if a block tag was encountered, and if the times are on different sides
	// PARAMS: oldTime, newTime - previous and current times to test
	// RETURNS: true - times were on different sides of block tag, false - times on same side of block tag (or none encountered)
	bool CalcBlockPassed(float oldTime, float newTime) const;


protected:

	// PURPOSE: Internal clip function, really calculates block time
	float CalcBlockedTimeInternal(float time) const;

	// PURPOSE: Internal clip function, push asset folder to match path to clip
	// RETURNS: true - if any folder push occurred (remember to pop this later).
	// NOTES: Useful for loading additional files in relative locations
	bool PushClipFolder() const;

	// PURPOSE: Make animation path relative
	void MakeAnimRelative(char* dest, int destSize, const char* source);

	// PURPOSE: Register a new clip type (only call from ClipTypeInfo::Register)
	// PARAMS: clip type info (must be global/class static, persist for entire execution)
	static void RegisterClipTypeInfo(const ClipTypeInfo& info);

	// PUPOSE: Internal clip function, helps derived classes create clips.
	// Allocates clip, and initializes base properties
	// PARAMS: clipType - type of clip to create
	// RETURNS: pointer to newly created clip, or NULL if failed.
	static crClip* CreateBase(eClipType clipType);


	// PURPOSE: Internal clip function, create tag manager
	void CreateTags();

	// PURPOSE: Internal clip function, create property manager
	void CreateProperties();

	// PURPOSE: Internal clip function, called after derived read serialization completes
	void PostSerialize(datSerialize&);

private:
	eClipType m_Type;
	atString m_Name;

	datRef<crClipDictionary> m_Dictionary;

	enum 
	{ 
		kIsLooped = BIT0,
		kIsResource = BIT1,
	};
	u8 m_Flags;

	datPadding<3> m_Padding;

	datOwner<crTags> m_Tags;
	datOwner<crProperties> m_Properties;

	mutable u32 m_RefCount;

	static atArray<const ClipTypeInfo*> sm_ClipTypeInfos;

	static bool sm_InitClassCalled;

	friend class crClipDictionary;
};

////////////////////////////////////////////////////////////////////////////////

extern __THREAD int s_ClipVersion;

////////////////////////////////////////////////////////////////////////////////

inline void crClip::AddRef() const
{
	if (const pgBaseRefCounted* dictionary = (const pgBaseRefCounted*)GetDictionary())
	{
		dictionary->AddRef();
	}
	sysInterlockedIncrement(&m_RefCount);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crClip::Release(eRefOption refOption) const
{
	MEM_USE_USERDATA(MEMUSERDATA_VISEME);		// THIS IS A DIRTY HACK to get around the non-stadnard Viseme allocation scheme

	const pgBaseRefCounted* dictionary = (refOption == kRefOptionAndDictionary) ? (const pgBaseRefCounted*)GetDictionary() : NULL;
	u32 refCount = sysInterlockedDecrement(&m_RefCount);
	if(!refCount)
	{
		delete this;
	}
	if (dictionary) dictionary->Release();
	return refCount;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crClip::GetRef() const
{
	return m_RefCount;	
}

////////////////////////////////////////////////////////////////////////////////

inline crClip::eClipType crClip::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

inline const char* crClip::GetName() const
{
	return m_Name.c_str();
}

////////////////////////////////////////////////////////////////////////////////

inline void crClip::SetName(const char* name)
{
	m_Name = name;
}

////////////////////////////////////////////////////////////////////////////////

inline const crClipDictionary* crClip::GetDictionary() const
{
	return m_Dictionary;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crClip::IsLooped() const
{
	return (m_Flags & kIsLooped) != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline void crClip::SetLooped(bool isLooped)
{
	if(isLooped)
	{
		m_Flags |= kIsLooped;
	}
	else
	{
		m_Flags &= ~kIsLooped;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline bool crClip::IsResource() const
{
	return (m_Flags & kIsResource) != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClip::GetNum30Frames() const
{
	return (GetDuration() * 30.f) + 1.f;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClip::ConvertPhaseTo30Frame(float phase) const
{
	return phase * GetDuration() * 30.f;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClip::ConvertPhaseToTime(float phase) const
{
	return phase * GetDuration();
}

////////////////////////////////////////////////////////////////////////////////

inline float crClip::Convert30FrameToPhase(float frame) const
{
	const float duration = GetDuration();
	if(duration > 0.f)
	{
		return frame / (duration * 30.f);
	}
	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClip::Convert30FrameToTime(float frame) const
{
	return frame * (1.f/30.f);
}

////////////////////////////////////////////////////////////////////////////////

inline float crClip::ConvertTimeToPhase(float time) const
{
	const float duration = GetDuration();
	if(duration > 0.f)
	{
		return time / duration;
	}
	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClip::ConvertTimeTo30Frame(float time) const
{
	return time * 30.f;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crClip::HasTags() const
{
	return m_Tags != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline const crTags* crClip::GetTags() const
{
	return m_Tags;
}

////////////////////////////////////////////////////////////////////////////////

inline crTags* crClip::GetTags()
{
	return m_Tags;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crClip::HasProperties() const
{
	return m_Properties != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline const crProperties* crClip::GetProperties() const
{
	return m_Properties;
}

////////////////////////////////////////////////////////////////////////////////

inline crProperties* crClip::GetProperties()
{
	return m_Properties;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClip::CalcBlockedTime(float time) const
{
	static const crTag::Key TagKeyBlock = crTag::CalcKey("Block", 0xE433D77D);

	if(m_Tags)
	{
		if(m_Tags->HasTags(TagKeyBlock))
		{
			return CalcBlockedTimeInternal(time);
		}
	}

	return time;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
#endif // CRCLIP_CLIP_H
