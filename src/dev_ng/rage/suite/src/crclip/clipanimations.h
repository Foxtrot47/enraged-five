//
// crclip/clipanimations.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRCLIP_CLIPANIMATIONS_H
#define CRCLIP_CLIPANIMATIONS_H

#include "clip.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Clip containing of multiple animations that are played in series and/or parallel.
// Each animation has its own rate, start time and end time.
class crClipAnimations : public crClip
{
public:

	// PURPOSE: Constructor
	crClipAnimations();

	// PURPOSE: Resource constructor
	crClipAnimations(datResource&);

	// PURPOSE:Resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Destructor
	virtual ~crClipAnimations();

	// PURPOSE: Declare clip type
	CR_DECLARE_CLIP_TYPE(crClipAnimations);

	// PURPOSE: Shutdown
	virtual void Shutdown();

#if CR_DEV
	// PURPOSE: Serialization to/from file
	virtual void Serialize(datSerialize&);

	// PURPOSE: Debug output
	virtual void Dump(crDumpOutput& output) const;
#endif

	// PURPOSE: Allocate and create serial/parallel animations clip
	// RETURNS: newly allocated and created serial/parallel animation clip, or NULL if creation failed
	static crClipAnimations* AllocateAndCreate();

	// PURPOSE: Create serial/parallel animations clip
	// PARAMS: parallel - are animations parallel or serial
	// RETURNS: true - creation succeeded, false if creation failed
	bool Create(bool parallel=false);

	// PURPOSE: Add/append a new animation
	void AddAnimation(const crAnimation& anim);


	// PURPOSE: Set parallel or serial mode
	void SetParallel(bool parallel);

	// PURPOSE: Are animations parallel or serial
	bool IsParallel() const;


	// PURPOSE: Get total clip duration
	// RETURNS: clip duration, in seconds
	virtual float GetDuration() const;

	// PURPOSE: Composite
	virtual void Composite(crFrameCompositor& compositor, float time, float deltaTime) const;

	// PURPOSE: Initialize DOFs
	virtual void InitDofs(crFrameData& frameData, crFrameFilter* filter=NULL) const;

	// PURPOSE: Does the clip contain this degree of freedom
	virtual bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Override animation discovery
	virtual bool ForAllAnimations(AnimationDiscoveryCb cb, void* data) const;

	// PURPOSE: Get the number of animation in this serial/parallel clip
	// RETURNS: Current number of animation
	unsigned int GetNumAnimations() const;

	// PURPOSE: Get animation
	// PARAMS: idx - animation index
	// RETURNS: animation pointer (can be NULL)
	const crAnimation* GetAnimation(int idx) const;

	// PURPOSE: Set the animation
	// PARAMS: idx - animation index
	// PARAMS: anim - new underlying animation wrapped by this clip
	void SetAnimation(int idx, const crAnimation& anim);

	// PURPOSE: Get animation start/end range times
	// PARAMS: idx - animation index
	void GetStartEndTimes(int idx, float& outStartTime, float& outEndTime) const;

	// PURPOSE: Set animation start/end range times
	// PARAMS: idx - animation index
	void SetStartEndTimes(int idx, float startTime, float endTime);

	// PURPOSE: Get playback rate
	// PARAMS: idx - animation index
	// RETURNS: rate - multiplier
	float GetRate(int idx) const;

	// PURPOSE: Set playback rate
	// PARAMS: idx - animation index
	// rate - multiplier
	void SetRate(int idx, float rate);

	// PURPOSE: Internal representation of animation section
	class Animation
	{
	public:
		// PURPOSE: Default constructor
		Animation();

		// PURPOSE: Copy constructor
		Animation(const Animation&);

		// PURPOSE: Resource constructor, doing nothing
		Animation(datResource&);

		// PURPOSE: Destructor
		~Animation();

		// PURPOSE: Initializer
		void Init(const crAnimation& anim);

		// PURPOSE: Shutdown, free/release dynamic resources
		void Shutdown();

		// PURPOSE: Placement
		IMPLEMENT_PLACE_INLINE(Animation);

#if CR_DEV
		// PURPOSE: Serialization to/from file
		// PARAMS: clip - parent clip, used to allocate animation
		void Serialize(datSerialize& s, crClipAnimations* clip);

		// PURPOSE: Debug output
		void Dump(crDumpOutput&) const;
#endif // CR_DEV

		// PURPOSE: Resourcing
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

		// PURPOSE: Get duration (influenced by animation duration start time, end time and rate_
		// RETURNS: animation section duration
		float GetDuration() const;

		// PURPOSE: Composite
		void Composite(crFrameCompositor& compositor, float time, float deltaTime) const;

		// PURPOSE: Get animation pointer
		// RETURNS: animation pointer (can be NULL)
		const crAnimation* GetAnimation() const;

		// PURPOSE: Get animation start/end range times
		void GetStartEndTimes(float& outStartTime, float& outEndTime) const;

		// PURPOSE: Set animation start/end range times
		// PARAMS: startTime, endTime - in seconds (startTime <= endTime)
		// NOTES: Time will be clamped to legal time within animation duration
		void SetStartEndTimes(float startTime, float endTime);

		// PURPOSE: Get playback rate
		// RETURNS: rate - multiplier
		float GetRate() const;

		// PURPOSE: Set animation rate
		// PARAMS: rate - multiplier
		void SetRate(float rate);

	private:
		float m_StartTime;
		float m_EndTime;
		float m_Rate;
		datRef<const crAnimation> m_Animation;
	};
	// PURPOSE: Get number of internal animations
	const Animation* GetAnimations() const;

private:

	// PURPOSE: Calculate the total clip duration
	// NOTES: If parallel - longest section; if serial - sum of sections
	float CalcTotalDuration() const;

	friend class Animation;

	atArray<Animation> m_Animations;
	float m_TotalDuration;
	bool m_Parallel;

	datPadding<3> m_Padding;
};

////////////////////////////////////////////////////////////////////////////////

inline bool crClipAnimations::IsParallel() const
{
	return m_Parallel;
}

////////////////////////////////////////////////////////////////////////////////

inline const crAnimation* crClipAnimations::GetAnimation(int idx) const
{
	return m_Animations[idx].GetAnimation();
}

////////////////////////////////////////////////////////////////////////////////

inline void crClipAnimations::GetStartEndTimes(int idx, float& outStartTime, float& outEndTime) const
{
	m_Animations[idx].GetStartEndTimes(outStartTime, outEndTime);
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipAnimations::GetRate(int idx) const
{
	return m_Animations[idx].GetRate();
}

////////////////////////////////////////////////////////////////////////////////

inline unsigned int crClipAnimations::GetNumAnimations() const
{
	return m_Animations.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline const crClipAnimations::Animation* crClipAnimations::GetAnimations() const
{
	return m_Animations.GetElements();
}

////////////////////////////////////////////////////////////////////////////////

inline const crAnimation* crClipAnimations::Animation::GetAnimation() const
{
	return m_Animation;
}

////////////////////////////////////////////////////////////////////////////////

inline void crClipAnimations::Animation::GetStartEndTimes(float& outStartTime, float& outEndTime) const
{
	outStartTime = m_StartTime;
	outEndTime = m_EndTime;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipAnimations::Animation::GetDuration() const
{
	return (m_EndTime - m_StartTime) / m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

inline float crClipAnimations::Animation::GetRate() const
{
	return m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRCLIP_CLIPANIMATIONS_H
