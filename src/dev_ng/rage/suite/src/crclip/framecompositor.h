//
// crclip/framecompositor.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRCLIP_FRAMECOMPOSITOR_H
#define CRCLIP_FRAMECOMPOSITOR_H

#include "cranimation/animation_config.h"

#include "atl/string.h"

namespace rage
{

class crAnimation;
class crFrame;
class crExpressions;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract compositor base class
// Gathers steps required to composite complex object
class crFrameCompositor
{
public:

	// PURPOSE: Constructor
	crFrameCompositor();

	// PURPOSE: Destructor
	virtual ~crFrameCompositor();

	// PURPOSE: Push animation decompression operation into compositor
	virtual void Composite(const crAnimation* anim, float time, float deltaTime) = 0;

	// PURPOSE: Push expressions into compositor
	virtual void ApplyExpressions(const crExpressions& exprs, float time) = 0;

	// PURPOSE: Push blend operation into compositor
	virtual void Blend(float weight) = 0;

	// PURPOSE: Push combined decompression and blend operation into compositor
	virtual void CompositeBlend(float weight, const crAnimation* anim, float time, float deltaTime) = 0;

	// PURPOSE: Push invalid operation into compositor
	virtual void Invalid();

	// PURPOSE: Push N-way blend operation into compositor
//	virtual void BlendN(? ?) = 0;

	// PURPOSE: Push combined decompression and N-way blend operation into compositor
//	virtual void CompositeBlendN(? ?) = 0;

	// TODO --- FILTERS (on blends and separate steps)
	// TODO --- MOVERS?
	// TODO --- EXPRESSION TYPE OPERATIONS (DOF COPIES/SETS ETC)
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Basic single frame compositor
class crFrameCompositorFrame : public crFrameCompositor
{
public:

	// PURPOSE: Constructor
	crFrameCompositorFrame(crFrame& frame, bool absolute=false);

	// PURPOSE: Destructor
	virtual ~crFrameCompositorFrame();

	// PURPOSE: Push animation decompression operation into compositor
	virtual void Composite(const crAnimation* anim, float time, float deltaTime);

	// PURPOSE: Push expressions into compositor
	virtual void ApplyExpressions(const crExpressions& exprs, float time);

	// PURPOSE: Push blend operation into compositor
	virtual void Blend(float weight);

	// PURPOSE: Push combined decompression and blend operation into compositor
	virtual void CompositeBlend(float weight, const crAnimation* anim, float time, float deltaTime);

protected:
	crFrame* m_Frame;
	bool m_Absolute;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Lazy single frame compositor
// Delays compositing, allows it to collapse composite and blend into a composite blend
class crFrameCompositorFrameLazy : public crFrameCompositorFrame
{
public:

	// PURPOSE: Constructor
	crFrameCompositorFrameLazy(crFrame& frame, bool absolute=false);

	// PURPOSE: Destructor
	// NOTES: Automatically finalizes the frame
	virtual ~crFrameCompositorFrameLazy();

	// PURPOSE: Push animation decompression operation into compositor
	// NOTES: Lazy, not immediately reflected in frame
	void Composite(const crAnimation* anim, float time, float deltaTime);

	// PURPOSE: Push expressions into compositor
	void ApplyExpressions(const crExpressions& exprs, float time);

	// PURPOSE: Push blend operation into compositor
	// NOTES: Supported in limited circumstances
	virtual void Blend(float weight);

	// PURPOSE: Push combined decompression and blend operation into compositor
	virtual void CompositeBlend(float weight, const crAnimation* anim, float time, float deltaTime);

	// PURPOSE: Finalize the contents of the frame, processes any deferred lazy step
	void Finalize();

protected:
	const crAnimation* m_Animation;
	float m_Time;
	float m_DeltaTime;
	bool m_Empty;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Debug output frame compositor, records composite calls
class crFrameCompositorDump : public crFrameCompositor
{
public:

	// PURPOSE: Constructor
	crFrameCompositorDump();

	// PURPOSE: Destructor
	virtual ~crFrameCompositorDump();

	// PURPOSE: Push animation decompression operation into compositor
	virtual void Composite(const crAnimation* anim, float time, float deltaTime);

	// PURPOSE: Apply expressions into compositor
	virtual void ApplyExpressions(const crExpressions& exprs, float time);

	// PURPOSE: Push blend operation into compositor
	virtual void Blend(float weight);

	// PURPOSE: Push combined decompression and blend operation into compositor
	virtual void CompositeBlend(float weight, const crAnimation* anim, float time, float deltaTime);

	// PURPOSE: Dump composite operations
	void Dump(atString& outString, bool collapse=true) const;

protected:

	// PURPOSE: Internal composite operation record
	struct CompositeOp
	{
		// PURPOSE: Constructor
		CompositeOp();

		// PURPOSE: Destructor
		~CompositeOp();

		// PURPOSE: Composite type
		enum eCompositeOpType
		{
			kCompositeOpTypeNone,
			kCompositeOpTypeComposite,
			kCompositeOpTypeBlend,
//			kCompositeOperationTypeCompositeBlend,
		};

		// PURPOSE: Internal recursive dump call
		void Dump(atString& outString, bool collapse, float weight=1.f, int indent=0) const;

		eCompositeOpType m_OpType;
		const crAnimation* m_Animation;
		float m_Weight;		
		float m_Time;
		float m_DeltaTime;
		atArray<CompositeOp*> m_ChildOps;
	};

	atArray<CompositeOp*> m_CompositeOps;
};

////////////////////////////////////////////////////////////////////////////////

inline crFrameCompositor::crFrameCompositor()
{
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameCompositor::~crFrameCompositor()
{
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameCompositorFrame::crFrameCompositorFrame(crFrame& frame, bool absolute)
: m_Frame(&frame)
, m_Absolute(absolute)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameCompositorFrame::~crFrameCompositorFrame()
{
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRCLIP_FRAMECOMPOSITOR_H
