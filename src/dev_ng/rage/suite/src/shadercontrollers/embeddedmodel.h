// 
// shadercontrollers/embeddedmodel.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADERCONTROLLERS_EMBEDDED_MODEL_H
#define SHADERCONTROLLERS_EMBEDDED_MODEL_H

#define ARRAY_PARAMS( arr )		arr,  ( sizeof( arr ) / sizeof( arr[0] ))

#include "grcore/fvf.h"
#include "grcore/drawmode.h"
#include "vector/vector3.h"
#include "vector/vector2.h"
#include "grcore/vertexdecl.h"

#include "grcore/vertexbuffer.h"
#include "grmodel/shader.h"
#include "spatialdata/aabb.h"

namespace rage
{

class EmbeddedBuilder
{
public:
	EmbeddedBuilder();

	bool	BumpTexIdx( int idx ) const		{ return m_bumpIdx[idx]; }
	bool	HasNormals() const		{ return m_normals != 0; }
	int		NumTexCoords() const	{ return m_tcSize; }
	int		GetNumIndices() const	{ return m_indexSize; }
	
	Vector3 GetNormal( int index );
	Vector2 GetTexCoord( int tIdx, int index );
	Vector3	GetPosition( int index );

	int GetNumPositions() { return m_posSize/3; }
	void AddPositions( float* positionTable , int size );
	void AddNormals( float* normalTable,  int size );
	void AddTexcoords( int tIdx, float* texcoordTable , int size);
	void SetBumpIdx( int tIdx, bool val )		{ m_bumpIdx[ tIdx ] = val; }
	

	const grcFvf*	fvf() { return &m_fvf; }

private:
	static const int MaxTexCoords = 3;

	grcFvf		m_fvf;
	float*		m_positions;
	int			m_posSize;
	float*		m_normals;
	int			m_normalSize;
	float*		m_texcoords[ MaxTexCoords];
	int			m_texCoordSize[ MaxTexCoords];
	bool		m_bumpIdx[ MaxTexCoords];
	int			m_tcSize;
	int			m_indexSize;
};

class grcVertexBuffer;
class grmShader;

class EmbeddedModel
{
public:
	EmbeddedModel() :
		  m_VertexDeclaration(0),
		  m_DrawMode( drawTris)
	  {}
	EmbeddedModel(datResource&)	{}
	  ~EmbeddedModel();

	  void Create( EmbeddedBuilder& builder, int* indices, int size  );


	  void SetShader( grmShader* shader )	{	 m_shader = shader;	  }
	  grmShader* GetShader()				{   return m_shader;	  }

#if HACK_GTA4
	  // Culling can be controled through enableCulling, this is mainly used when rendering EmbeddedModel in non-convenction spaces
	  // like paraboloidal projections where the culling method employed would usually gives false positive.
	  void Draw( const rage::grcViewport& view, int passIndex = 0, grcEffectTechnique technique = grcetNONE, bool enableCulling = false );
#else // HACK_GTA4
	  void Draw( const rage::grcViewport& view, int passIndex = 0, grcEffectTechnique technique = grcetNONE );
#endif // HACK_GTA4	

	  // Resourcing
	  DECLARE_PLACE(EmbeddedModel);
#if __DECLARESTRUCT
	  void DeclareStruct(datTypeStruct& s);
#endif
	  //


private:
	struct CullBatch
	{
		rage::spdAABB	m_bound;
		int					m_startIndex;
		int					m_endIndex;
	};

	int																	m_amtCullBatches;
	u32																	m_DrawMode;
	grcVertexDeclaration*												m_VertexDeclaration;
	datOwner<grcVertexBuffer>											m_VertexBuffer;
	grcIndexBuffer*														m_IndexBuffer;
	datOwner<grmShader>													m_shader;
	CullBatch*															m_batches;
};

}

#endif
