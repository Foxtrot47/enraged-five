
// 
// shader_fragment_control_h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
//	PURPOSE
//		Controls parts of a shader which are set globally across multiple shaders.

#ifndef SHADERCONTROLLERS_SHADERFRAGMENTCONTROL_H
#define SHADERCONTROLLERS_SHADERFRAGMENTCONTROL_H

#include "atl/singleton.h"
#include "atl/string.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "parser/macros.h"
#include "rmcore/drawable.h"
#include "system/typeinfo.h"
#include "vector/matrix34.h"

#include <list>

namespace rage
{
	class rmcDrawable;
    class parTreeNode;



class VarCache
{	
public:
	virtual void CacheEffectVars( grmShader& effect ) = 0;
	virtual ~VarCache() {}
};


typedef std::pair<grmShader*,VarCache*>	shaderRef;

class ShaderRef
{

	u32	m_currentEffectMarker;
public:
	VarCache*	vars;
	grmShader*	shader;

	ShaderRef( grmShader* s, VarCache* v  = 0)
		: shader( s ), vars( v ), m_currentEffectMarker((u32) ~0 )
	{}

	void CleanUp() { delete vars; }

	void UpdateVariableCache()
	{
		if ( vars &&  m_currentEffectMarker !=  shader->GetHashCode() )		
		{
			vars->CacheEffectVars( *shader );
			m_currentEffectMarker = shader->GetHashCode();
		}
	}

};

inline bool operator==( const ShaderRef& a, const ShaderRef& b )
{
	return a.shader == b.shader;
}

typedef std::list<ShaderRef>		ShaderList;


class ShaderFragment : public datBase
{
public:
	virtual void SetupShader( grmShader& effect, VarCache*	vars ) = 0;
	virtual void SetupShaderPass() = 0;
	virtual ~ShaderFragment() 
	{ 
		for (ShaderList::iterator i = m_shaderList.begin(), e = m_shaderList.end(); i != e; ++i)
			i->CleanUp();
	}

	virtual bool UsesFragment( grmShader & effect ) = 0;

#if !__PPU && !__SPU

	void SetType( ::type_info* TypeName )			{	m_typeName = TypeName; }
	int CheckType( ::type_info* TypeName )	const	{	return  ( (*m_typeName) == (*TypeName) ); }
#endif

	void AddShader( grmShader* shader )
	{
		FastAssert( std::find( m_shaderList.begin(), m_shaderList.end(), shader ) == m_shaderList.end() );
		
		VarCache*	var = CreateVariableCache();
		m_shaderList.push_back( ShaderRef( shader, var) );
	}
	void RemoveShader( grmShader* shader )
	{
		if ( std::find( m_shaderList.begin(), m_shaderList.end(), ShaderRef(shader) ) == m_shaderList.end() )
		{
			return;
		}

		ShaderList::iterator	itor = std::find( m_shaderList.begin(), m_shaderList.end(), ShaderRef(shader) ) ;
		delete itor->vars;

		m_shaderList.erase( itor );
	}

	virtual void UpdateShaders()
	{
		for ( ShaderList::iterator itor = m_shaderList.begin();
									itor != m_shaderList.end();
									++itor )
		{
			itor->UpdateVariableCache();
			SetupShader(  *itor->shader , itor->vars);
		}
	}

	virtual VarCache*	CreateVariableCache()	{ return NULL; }


    bool LoadFile( const char *pFilename );

    bool LoadTreeNode( const parTreeNode *pTreeNode );
    bool SaveTreeNode( parTreeNode *pParentNode );

#if __BANK
	bool SaveFile( const char *pFilename );
    void BankLoadFile();
    void BankSaveFile();

    void AddWidgets( rage::bkBank *pBank );
#endif

	PAR_PARSABLE;

private:

#if !__PPU && !__SPU
	::type_info*		m_typeName;
#endif
	ShaderList			m_shaderList;
};



typedef std::pair< atString, ShaderFragment*>	FragmentDetails;

typedef std::list<FragmentDetails>		ShaderFragmentList;


class ShaderFragmentControl : public datBase
{
	
public:

	template<class ShaderFragmentType>
		void Register( const char* name, ShaderFragmentType* fragment )
	{
		AssertMsg( m_lookup.Access( (const char*)name ) == 0, "Shader Fragment control code has already been added" );

#if !__PPU && !__SPU
		::type_info* TypeName = const_cast< ::type_info *>( &(typeid( fragment) ));
		fragment->SetType( TypeName );
#endif
		m_lookup.Insert( atString(name), fragment );

		m_store.push_back( std::make_pair( name, fragment ));
	}

	bool Exists( const char* name )	{ return m_lookup.Access( name) != 0 ; }

	template<class ShaderFragmentType>
		void	Get( const char* name , ShaderFragmentType*& fragment)
	{
	
		ShaderFragment** fragPtr = m_lookup.Access( name );
		FastAssert( fragPtr != NULL );
		
#if __ASSERT

#if !__PPU && !__SPU
		// check that type is correct
		::type_info* TypeName = const_cast< ::type_info *>( &(typeid( fragment) ));
		FastAssert( (*fragPtr)->CheckType( TypeName ) );
#endif
#endif
		// we have check to see that this is fine so lets do a static cast
		fragment = reinterpret_cast<ShaderFragmentType*>( *fragPtr );
	}


		void	Remove( const char* name)
		{

			ShaderFragment** fragPtr = m_lookup.Access( name );
			Assert( fragPtr != NULL );
			Assert( *fragPtr != NULL );

			for ( ShaderFragmentList::iterator itor = m_store.begin();
				itor != m_store.end();
				++itor )
			{
				if (itor->second == *fragPtr)
				{
					m_store.erase(itor);
					break;
				}
			}

			delete *fragPtr;
			m_lookup.Delete( name );

		}

	void AddShader( grmShader* shader  )
	{
		for ( ShaderFragmentList::iterator itor = m_store.begin();
			itor != m_store.end();
			++itor )
		{
			if ( itor->second->UsesFragment( *shader ))
			{
				itor->second->AddShader( shader );
			}
		}
	}
    void RemoveShader( grmShader* shader  )
    {
        for ( ShaderFragmentList::iterator itor = m_store.begin();
            itor != m_store.end();
            ++itor )
        {
            if ( itor->second->UsesFragment( *shader ))
            {
                itor->second->RemoveShader( shader );
            }
        }
    }
	void AddShaders(rmcDrawable *pDrawable )
	{
		if ( pDrawable )
		{
			grmShaderGroup& grp = pDrawable->GetShaderGroup();
			AddShaders( grp );
		}	
	}
	void AddShaders( const grmShaderGroup& grp )
	{
		for ( int j = 0; j < grp.GetCount(); j++  )
		{
			grmShader& pShader = grp.GetShader( j );
			AddShader( &pShader );
		}
	}
    void RemoveShaders(rmcDrawable *pDrawable )
    {
        if ( pDrawable )
        {
            grmShaderGroup& grp = pDrawable->GetShaderGroup();
            for ( int j = 0; j < grp.GetCount(); j++  )
            {
                grmShader& pShader = grp.GetShader( j );
                RemoveShader( &pShader );
            }
        }	
    }
	void UpdateShaders()
	{
		for ( ShaderFragmentList::iterator itor = m_store.begin();
			itor != m_store.end();
			++itor )
		{
			itor->second->UpdateShaders();
		}
	}
    
	~ShaderFragmentControl()
	{
		for ( ShaderFragmentList::iterator itor = m_store.begin();
			itor != m_store.end();
			++itor )
		{
			delete itor->second;
		}
	}
    // START
    // PURPOSE: Loads/Saves all shader fragments contained in the file
    // PARAMS:
    //      pFilename - must be XML
    // RETURNS:
    //      true on success, false on failure
    // NOTES:
    //      All Shader Fragments must be registered before calling these functions.
    //      After loading pFilename into a parTree, calls Load/Save TreeNodes.  See below
    //          for xml structure specifications.
    bool LoadFiles( const char *pFilename );
#if __BANK
    bool SaveFiles( const char *pFilename );
#endif
    // STOP

    // START
    // PURPOSE: Loads/Saves all shader fragments under pRootNode
    // PARAMS:
    //      pRootNode - node named "shader_fragments" to load/save
    // RETURNS:
    //      true on success, false on failure
    // NOTES:
    //      All Shader Fragments must be registered before calling these functions.
    //      pRootNode must be named "shader_fragments"
    //      Each child name must be the same as the Shader Fragment's PARSER node 
    //          name that you get by calling 'shaderFrag.parser_GetStructure()->GetName()'
    //      On load, if the child node has an attribute named "fileRef" we will treat 
    //          the value as string data and load that file into the Shader Fragment.  
    //          All other attributes and children of this node will be ignored.
    //      Currently, saving will not write any fileRef attributes.  In the future,
    //          we could add that as a widget/option to the ShaderFragment base class
    //          and then test for it on Save.
    bool LoadTreeNodes( parTreeNode *pRootNode );
#if __BANK
    bool SaveTreeNodes( parTreeNode *pRootNode );
#endif
    // STOP

#if __BANK	
    void BankLoadFiles();
    void BankSaveFiles();

    // add in shader controls
	void AddWidgets( bkBank *pBank );
#endif

private:
	typedef atMap< atString,  ShaderFragment* >	LookupType;		// TODO: Use an atStringMap!!
	
	LookupType												m_lookup;
	ShaderFragmentList										m_store;

};

typedef atSingleton<ShaderFragmentControl> ShaderController;


}

#endif
