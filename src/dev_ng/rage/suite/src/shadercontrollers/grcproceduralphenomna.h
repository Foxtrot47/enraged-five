// 
// shadercontrollers/grcproceduralphenomna.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADERCONTROLLERS_GRCPROCEDURALPHENOMNA_H
#define SHADERCONTROLLERS_GRCPROCEDURALPHENOMNA_H

#include "proceduraltexture.h"

#include "grcore/viewport.h"
#include "vector/vector4.h"
#include "grcore//state.h"
#include "atl/ptr.h"
#include "atl/functor.h"
#include "grcore/viewport.h"
#include "grcore/viewport_inline.h"

namespace rage 
{
class VerletWaterSimulation;
class VerletWaterPerturbation;
class VerletWaterSurface;
class SkyhatPerlinNoise;
class SkyhatMiniNoise;

class ProceduralTextureVerletWater : public ProceduralTexture
{
public:

    typedef Functor1 <ProceduralTextureVerletWater *> InitClientFunctor;
    typedef Functor2 <ProceduralTextureVerletWater *, grcViewport &> DrawClientFunctor;

    enum ERenderTargets
    {
        E_DEPTH_RT,
        E_REFRACT_RT,
        E_VERLET_SIM_RT_1,
        E_VERLET_SIM_RT_2,
        E_VERLET_SIM_RT_3,
    };

    enum EShaders
    {
        E_VERLET_SIM_SHADER,    // expects 1 dampening texture
        E_PERTURBATION_SHADER,  // expects 1 perturbation texture
    };

    ProceduralTextureVerletWater();
    ~ProceduralTextureVerletWater();

    void SetTimeOfDay( float time ) { m_timeOfDay = time; }
    void SetInitClientFunctor( InitClientFunctor functor );
    void SetDrawClientFunctor( DrawClientFunctor functor );
    void SetMirrorPlane( const Vector4 &m_mirrorPlane, float farClip );

    void OnPostLoadDerived();

    // call m_initClientFunctor
    bool Load();

    // update shader fragments
    void Update();

    // draw into your render target(s)
    void Draw();

#if __BANK
    // add custom widgets
    void AddWidgets( bkBank &bank );
    
    void BankPerturbQuadScaleSliderChanged();
#endif

    PAR_PARSABLE;

private:

    void DrawReflection();

    float m_timeOfDay;

    static const int TOT_VERLETSIM_RTS = 3;
    int m_iCurrentVerletSimRT;

    bool m_perturbShowSim;
    float m_perturbQuadScale;
    float m_perturbRotAngle;
    float m_perturbTexScale;
    Vector2 m_perturbQuadVerts[4];
    Vector2 m_perturbQuadOffset;
    
    Vector4 m_mirrorPlane;
    float m_farClip;

    VerletWaterSimulation* m_pVerletWaterSimulation;
    VerletWaterPerturbation* m_pVerletWaterPerturbation;
    VerletWaterSurface* m_pVerletWaterSurface;

    InitClientFunctor m_initClientFunctor;
    DrawClientFunctor m_drawClientFunctor;
};

/*
	PURPOSE
		Sets up a viewport for a given reflection plane
*/
class ReflectionPlane
{
	Vector4		m_plane;
	Vector3		m_pos;
	float		m_size;
	float		m_farClip;

	bool		m_isDoubleSided;

public:
	ReflectionPlane() : m_plane( 0.0f, 1.0f, 0.0f, -15.0f ), 
						m_pos( 0.0f, 0.0f, 0.0f ),
								m_size( 100.0f ), 
								m_farClip( 100.0f ),
								m_isDoubleSided( false )
	{}
#if __BANK
	// add custom widgets
	void AddWidgets( bkBank &bank );
#endif
	void Set( const Vector4& plane) 
	{ 
		m_plane = plane; 
		m_pos = Vector3( m_plane.x, m_plane.y, m_plane.z ) * -m_plane.w ;
	}
	void Set( const Vector3& pos, const Vector3& norm )
	{
		m_pos = pos;
		m_plane.ComputePlane( pos, norm );
		m_plane.w = -m_plane.w;
	}
	void Set( const Matrix34& mat  )  // assume y axias is the up vector
	{
		Set( mat.d, mat.b );
	}
	void SetSize( float size ) { m_size = size; }
	void SetFarClip( float clip ) { m_farClip = clip; }
	void SetDoubleSided( bool doubleSided )	{ m_isDoubleSided = doubleSided; }

	bool IsVisible(const grcViewport *viewport = NULL) const
	{
		if (!viewport)
		{
			viewport = grcViewport::GetCurrent();
		}

		Vec4V sphere( m_pos.x, m_pos.y, m_pos.z, m_size );
		bool isVisible = viewport->IsSphereVisibleInline( sphere ) != 0 &&
			( m_isDoubleSided || OnFrontSide( VEC3V_TO_VECTOR3(viewport->GetCameraPosition())) );

		return isVisible;
	}

	bool CalculateReflectionPlane(const grcViewport &inViewport, const Matrix34 &inMatrix, grcViewport &outViewport) const;

	bool OnFrontSide( const Vector3& pos )  const { return m_plane.Dot( Vector4( pos.x, pos.y, pos.z, 1.0f )) > 0; }

	grcViewport*  BeginReflection(  grcViewport& clipReflectionViewport, grcViewport& reflectionViewport );
	void EndReflection( grcViewport* oldViewport );

	grcViewport*  BeginRefraction(  grcViewport& clipRefractionViewport, grcViewport& refractionViewport );
	void EndRefraction( grcViewport* oldViewport );
};

/*
PURPOSE
	Renders a number of planar reflections to a texture. This allows for many reflections to be visible at one time.
*/
class MultiPlanarReflection
{
	atArray<ReflectionPlane>		m_planes;
	typedef Functor2 <rage::u32 , bool&  >					DrawClientFunctor;
	typedef Functor2 <grcViewport &, rage::u32 >			DrawClientLodFunctor;

	atScopedPtr<grcRenderTarget>	m_depthRT;
	atScopedPtr<grcRenderTarget>	m_reflectionRT;

	DrawClientFunctor			m_drawReflectors;
	DrawClientLodFunctor		m_drawScene;

	void SetStencilOnWrite( grcLargeStateStack& stack, int i )
	{
		stack.Set<grcsStencilEnable>(true);
		stack.Set<grcsStencilFunc>(grccfAlways );
		stack.Set<grcsStencilRef>( i );
		stack.Set<grcsStencilWriteMask>(0xff);
		stack.Set<grcsStencilPass>( grcsoReplace);

	}
	void WriteOnlyOnStencil( grcLargeStateStack& stack, int i )
	{
		stack.Set<grcsStencilEnable>(true);
		stack.Set<grcsStencilFunc>( grccfEqual );
		stack.Set<grcsStencilRef>( i );
		stack.Set<grcsStencilPass>( grcsoKeep );
	}


public:
	void Init( int numPlanes = 1, int sizeModifier  =  4)
	{
		// create rendertargets
		int width = GRCDEVICE.GetWidth() / sizeModifier;
		int height = GRCDEVICE.GetHeight() / sizeModifier;
		
		grcTextureFactory::CreateParams cp;

#if __XENON
		cp.Multisample = 4;
		cp.UseHierZ = true;
#endif

		cp.Format = grctfR5G6B5;
		cp.IsResolvable = true;   
		m_reflectionRT =  grcTextureFactory::GetInstance().CreateRenderTarget( "__reflectionplanert__.dds", grcrtPermanent, width, height, 32, &cp);

		cp.IsResolvable = false;
		cp.HasParent = true;
		cp.Parent = m_reflectionRT.GetPtr();
		m_depthRT =  grcTextureFactory::GetInstance().CreateRenderTarget( "reflectionDepth", grcrtPermanent, width, height, 32, &cp);

		grcTextureFactory::GetInstance().RegisterTextureReference(m_reflectionRT->GetName(), m_reflectionRT.GetPtr());
		m_planes.Resize( numPlanes );
	}

	ReflectionPlane& operator[]( int i )	{ return m_planes[i]; }
	
	void SetDrawReflectors( DrawClientFunctor functor )	{ m_drawReflectors = functor; }
	void SetDrawScene( DrawClientLodFunctor functor ) { m_drawScene = functor; }

	void Draw()
	{	
		grcLargeStateStack	states;

		bool*	doDraw = Alloca( bool, m_planes.size() );
		
		grcTextureFactory::GetInstance().LockRenderTarget( 0, m_reflectionRT.GetPtr(), m_depthRT.GetPtr() );
		GRCDEVICE.Clear(true,Color32(1.0f,1.0f,1.0f,0.0f), true,1.f,1);
		
		// check visibility
		bool allInvisible = true;
		for ( int i = 0; i < m_planes.size(); i++)
		{
			doDraw[i] = m_planes[i].IsVisible();
			allInvisible = !doDraw[i] && allInvisible;
		}
		if ( allInvisible )
		{
			return;
		}
		
		FastAssert( DrawClientFunctor::NullFunctor() != m_drawReflectors );
		FastAssert( DrawClientLodFunctor::NullFunctor() != m_drawScene );

		for (int i = 0; i < m_planes.size(); i++)
		{
			if ( doDraw[i] )
			{
				// set the stencil flag using the reflection geometry
				SetStencilOnWrite( states, i + 1 );
				m_drawReflectors(  i, doDraw[i] );		
			}
		}
		states.Restore();
	
		// TODO : don't clear depth
		GRCDEVICE.Clear( false, Color32(), true, 1.0f, 0 );

		// now we have drawn the reflection objects
		for (int i = 0; i < m_planes.size(); i++ )
		{
			if ( doDraw[i] )
			{
				grcViewport		reflectView;
				grcViewport*	oldView;

				oldView = m_planes[ i ].BeginReflection(reflectView, reflectView );
				// Set stencil so only draw on same pixels
				// set the stencil flag using the reflection geometry
				WriteOnlyOnStencil( states, i + 1 );
				m_drawScene(  reflectView, i );		
				m_planes[ i ].EndReflection( oldView );
			}
		}

		grcTextureFactory::GetInstance().UnlockRenderTarget(0  );


		// TODO add a dilate to the texture to fill in the gaps.
	}
};

class ProceduralReflection : public datBase	
{
public:

	typedef Functor3 <ProceduralReflection *, grcViewport &,grcViewport &> DrawClientFunctor;
	
	ProceduralReflection();
	~ProceduralReflection() {}

	void SetDrawClientFunctor( DrawClientFunctor functor );
	
	void SetMirrorPlane( const Vector4 &m_mirrorPlane, float farClip , float size );

	const ReflectionPlane &GetPlane() const				{ return m_plane; }

	// call m_initClientFunctor
	void Init( int shrinkSize = 4 , bool compressedFormat = false , bool allocateStartEdram = true, bool doRefraction = true, bool hdr = false );

	// draw into your render target(s)
	void Draw();

	// Draw the reflection map only
	void DrawReflectionMap();

	// Draw the refraction map only
	void DrawRefractionMap();

	// Call this before you use a water shader
	void BeginReflectionDraw(grcTexture *textureOverride = NULL);

#if __BANK
	// add custom widgets
	void AddWidgets( bkBank &bank );

	void DebugDraw();
#else
	void DebugDraw() {};
#endif

	PAR_PARSABLE;

private:

	void DrawReflection();

	ReflectionPlane	m_plane;
	DrawClientFunctor m_drawClientFunctor;
	
	atScopedPtr<grcRenderTarget>	m_depthRT;
	atScopedPtr<grcRenderTarget>	m_reflectionRT;
	atScopedPtr<grcRenderTarget>	m_refractionRT;

	grcEffectGlobalVar m_DepthBufferTex;

#if __BANK
	bool				m_isShowReflectionMap;
	bool				m_isShowRefractionMap;
#endif // __BANK
};

//#############################################################################

class ProceduralTextureSkyhat : public ProceduralTexture
{
public:

	//PURPOSE : Controls the generation of extra textures for lighting from the skydome
	//REMARKS : There are two textures which can be generated for lighting. A reflection map
	// for lighting reflective and specular objects and a diffuse map for diffuse lighting.
	// 
	struct Params
	{
		bool createMiniReflectionMap;		// If true will generate a reflection map representation of the sky
		bool createMiniDiffuseMap;			// if true will generate a diffuse texture for sky lighting
		int	 miniReflectionSize;			// size of reflection map in texels
		int	 miniDiffuseSize;				// size of Diffuse lighting map in texels

		bool	IsTiled;					// uses tiled memory on the PS3 for perlin noise texture
		bool	IsLocal;					// uses local memory on the PS3 for perlin noise texture.
		int		MipLevels;					// uses mip mapping and  number of levels
		bool	CreateAABuffer;				//
		int		mapSize;

		grcTextureFormat format;			// texture format for the generated textures
		Params() 
			: createMiniReflectionMap( false), createMiniDiffuseMap( false), miniReflectionSize(32),
			miniDiffuseSize( 32 ),format( grctfA8R8G8B8 ),IsTiled(true), IsLocal( true), MipLevels(1),
			CreateAABuffer( false), mapSize(256)
		{}

		Params( bool crefMap, bool cdifMap, int miniRefSize = 32, int miniDiffuseSize = 32, grcTextureFormat  f =  grctfA8R8G8B8) 
			: createMiniReflectionMap( crefMap), createMiniDiffuseMap( cdifMap), miniReflectionSize(miniRefSize),
			miniDiffuseSize( miniDiffuseSize ),format( f ),IsTiled(true), IsLocal( true), MipLevels(1),
			CreateAABuffer( false), mapSize(256)
		{
			IsValid();
		}

		bool IsValid() const
		{
			Assert( miniDiffuseSize  >0 && miniDiffuseSize <= 2048 );
			Assert( miniReflectionSize  >0 && miniReflectionSize <= 2048 );
			return true;
		}
	};


    typedef Functor1 <ProceduralTextureSkyhat *> InitClientFunctor;

    enum ERenderTargets
    {
        E_PERLIN_NOISE_RT,
        E_MINI_SKY_RT,
		E_MINI_SKY_BLUR_RT
    };

    enum EShaders
    {
        E_PERLIN_NOISE_SHADER,  // expects 2 perlin noise textures
        E_MINI_SKY_SHADER,      // expects 1 high detail noise texture
    };

    ProceduralTextureSkyhat();
    ~ProceduralTextureSkyhat();

    void SetTimeOfDay( float time ) { m_timeOfDay = time; }
    float GetTimeOfDay() const { return m_timeOfDay; }

    void SetDay( float day ) { m_day = day; }
    float GetDay() const { return m_day; }

    void SetInitClientFunctor( InitClientFunctor functor );

	void OnPostLoadDerived();

    // call m_initClientFunctor
	bool Load() { return true; }

    bool Load( const ProceduralTextureSkyhat::Params& params ,  const char* shaderNamem );

	// update shader fragments
    void Update();

    // draw into your render target(s)
    void Draw();

	// set phase rate	
	void SetPhaseRate( float phaseRate );
	void SetCloudWarp( float cloudWarp );
	void SetSunSetBump( float sunsetBump );
	
#if __BANK
    // add custom widgets
    void AddWidgets( bkBank &bank );
#endif

	void SetPerlinNoiseBaseTexture( atPtr<grcTexture> pNoise );
	void SetHighDetailTexture( atPtr<grcTexture> pDetail );
	grmShader&	GetMiniSkyHatShader();

	void SetUseRescaledClouds( bool v = "true" ) 
	{ 
		m_useImprovedClouds = v; 
	}

    PAR_PARSABLE;
private: 

	void SetupTechniques();

    void DrawMiniSky();

    float	m_timeOfDay;
    float	m_day;
	bool	m_useImprovedClouds;
    grcEffectTechnique	m_drawMiniMe;
	grcEffectTechnique  m_simpleBlurMiniMe;
	grcEffectTechnique  m_blurMiniMe;
	grcEffectTechnique  m_drawProceduralTexture;


    SkyhatPerlinNoise*	m_pSkyhatPerlinNoise;
    SkyhatMiniNoise*	m_pSkyhatMiniNoise;
	
	
	atPtr<grcTexture>	m_HighDetailTex;
	atPtr<grcTexture>	m_pNoiseBase;

	Params				m_params;
};

} // namespace rage
#endif // SHADERCONTROLLERS_GRCPROCEDURALPHENOMNA_H





