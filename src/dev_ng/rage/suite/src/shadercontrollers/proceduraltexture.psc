<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ProceduralTextureRenderTargetDef" autoregister="true">
    <string name="m_name" type="member" size="64"/>
    <int name="m_parentIndex" init="-1"/>
    <int name="m_type"/>
    <int name="m_width" init="640"/>
    <int name="m_height" init="480"/>
    <int name="m_bitsPerPixel" init="32"/>
  <!--  <pointer name="m_pParams" type="rage::grcTextureFactory::CreateParams" policy="external_named" toString="rage::ProceduralTextureRenderTargetDef::ParamsToString" fromString="rage::ProceduralTextureRenderTargetDef::ParamsFromString" /> -->
</structdef>

<structdef type="rage::ProceduralTextureShaderDef" autoregister="true">
    <string name="m_name" type="member" size="64"/>
    <string name="m_presetName" type="member" size="128"/>
    <array name="m_textures" type="atArray" hideWidgets="true">
        <string type="atString"/>
    </array>
</structdef>

<structdef type="rage::ProceduralTexture" autoregister="true">
    <array name="m_renderTargetDefs" type="atArray">
        <pointer type="rage::ProceduralTextureRenderTargetDef" policy="owner"/>
    </array>
    <array name="m_shaderDefs" type="atArray">
        <pointer type="rage::ProceduralTextureShaderDef" policy="owner"/>
    </array>
</structdef>

</ParserSchema>