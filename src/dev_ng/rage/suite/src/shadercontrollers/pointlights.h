//
// shadercontrollers/pointlights.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef _RDR2_LIGHTING_H_
#define _RDR2_LIGHTING_H_

#include "vector/vector3.h"
#include "vector/vector4.h"
#include "grcore/effect_typedefs.h"
#include "grcore/state.h"
#include "grcore/light.h"
#include "shaderfragmentcontrol.h"

using namespace rage;

namespace rage
{
	class grcLightGroup;


class Rdr2PointLight
{
public:
	Vector3	m_Position;			// Light position
	Vector4	m_ColorAndRange;	// RGB and range (range is 1/range^2)
#if __BANK
	void AddWidgets( bkBank* bank )
	{
		bank->AddSlider( "position", &m_Position, -10000.0f,10000.0f, 0.1f );
		bank->AddColor( "color", (Vector3*)&m_ColorAndRange );
		bank->AddSlider("range", &m_ColorAndRange.w, 0.0f, 10000.0f, 0.01f );
	}
#endif
};


class Rdr2Lighting
{
	
public:
	Rdr2Lighting();

	~Rdr2Lighting();

	void SetupPointLights(const Rdr2PointLight *RESTRICT lightArray);
	void SetupDirectionalLights(Vector3 *directions, Vector4 *colorsAndIntensity);

	void SetLightingGroup(const grcLightGroup &grp);

private:
	
	grcEffectGlobalVar	m_DirectionalLightX, m_DirectionalLightY, m_DirectionalLightZ, m_DirectionalLightR, m_DirectionalLightG, m_DirectionalLightB;
	grcEffectGlobalVar	m_PrimaryDirectionalDir, m_PrimaryDirectionalColor;

	grcEffectGlobalVar	m_PointLightX, m_PointLightY, m_PointLightZ, m_PointLightR, m_PointLightG, m_PointLightB;
	grcEffectGlobalVar	m_PointLightOneOverRangeSq;


	bool					m_notSetup;

};

class Rdr2LightController : public ShaderFragment 
{
	struct EmptyVarCache : public VarCache
	{
		void CacheEffectVars( grmShader&  )		{}
	};

public:
	Rdr2LightController()
	{
		m_pointLights[0].m_ColorAndRange = Vector4( 1.0f, 1.0f, 1.0f, 100.0f);
		m_pointLights[1].m_ColorAndRange = Vector4( 1.0f, 1.0f, 0.0f, 100.0f);
		m_pointLights[2].m_ColorAndRange = Vector4( 1.0f, 0.0f, 1.0f, 100.0f);
		m_pointLights[3].m_ColorAndRange = Vector4( 0.0f, 1.0f, 1.0f, 100.0f);

		m_pointLights[0].m_Position = Vector3( 10.0f, 10.0f, 10.0f);
		m_pointLights[1].m_Position = Vector3( -10.0f, 10.0f, 10.0f);
		m_pointLights[2].m_Position = Vector3( 10.0f, 10.0f, -10.0f);
		m_pointLights[3].m_Position = Vector3( -10.0f, 10.0f, -10.0f);
	}

	void SetupShaderPass()  
	{}

	bool UsesFragment( grmShader& )
	{
		return true; // wrong for now
	}

	VarCache*	CreateVariableCache()	{ return rage_new EmptyVarCache; }

	void SetupShader( grmShader& , VarCache* ) 	
	{ 
		Setup();
	}

	void Setup()
	{
		grcLightGroup* lgrp = grcState::GetLightingGroup();
		m_lighting.SetLightingGroup( *lgrp);
		m_lighting.SetupPointLights( m_pointLights );
	}
#if __BANK
	void AddWidgets( bkBank* bank )
	{
		bank->PushGroup("Point Light 1 ")	;
		m_pointLights[0].AddWidgets( bank );
		bank->PopGroup();

		bank->PushGroup("Point Light 2  ")	;
		m_pointLights[1].AddWidgets( bank );
		bank->PopGroup();

		bank->PushGroup("Point Light 3 ")	;
		m_pointLights[2].AddWidgets( bank );
		bank->PopGroup();

		bank->PushGroup("Point Light 4")	;
		m_pointLights[3].AddWidgets( bank );
		bank->PopGroup();
	}
#endif

private:
	Rdr2Lighting			m_lighting;
	Rdr2PointLight			m_pointLights[4];

	Vector3					directions[4]; 
	Vector4					colorsAndIntensity[4];

};
};


#endif // _RDR2_LIGHTING_H_ 
