// 
// embeddedmodel/embeddedmodel.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "embeddedmodel.h"
#include "fragment/fragshaft.h"

#include "grmodel/modelfactory.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/indexbuffer.h"

using namespace rage;

#define _DEBUG_EMBEDDED_CULLING 0

EmbeddedBuilder::EmbeddedBuilder()
:	m_positions( 0),
	m_normals( 0),
	m_indexSize( 0 ),
	m_tcSize( 0 )
{
	for ( int i = 0;i < MaxTexCoords; i++ )
	{
		m_texcoords[i] = 0;
		m_texCoordSize[i] = 0;
		m_bumpIdx[i] = true;
	}
}

void EmbeddedBuilder::AddPositions( float* positionTable , int size )	
{		
	m_fvf.SetPosChannel(true); 
	m_positions = positionTable;	
	m_posSize = size; 
	m_indexSize++;
}


void EmbeddedBuilder::AddNormals( float* normalTable,  int size )
{		
	m_fvf.SetNormalChannel(true); 
	m_normals = normalTable;	
	m_normalSize = size; 
	m_indexSize++;
}


void EmbeddedBuilder::AddTexcoords( int tIdx, float* texcoordTable , int size)
{		
	Assert( m_tcSize < MaxTexCoords );
	m_fvf.SetTextureChannel( tIdx, true, grcFvf::grcdsFloat2); 
	m_texcoords[ m_tcSize] = texcoordTable;	
	m_texCoordSize[ m_tcSize] = size; 
	m_indexSize++;
	m_tcSize++;
	
}


Vector3	EmbeddedBuilder::GetPosition( int index )
{
	Assert( m_positions );
	Assert(  (index * 3) < m_posSize );
	Vector3 pos( m_positions[ index * 3], m_positions[ index * 3 + 1], m_positions[ index * 3 + 2] );
	return pos;
}

Vector3 EmbeddedBuilder::GetNormal( int index )
{	
	Assert( m_normals );
	Assert( (index * 3) < m_normalSize );
	Vector3 normal( m_normals[ index * 3], m_normals[ index * 3 + 1], m_normals[ index * 3 + 2] );
	return normal;
}


Vector2 EmbeddedBuilder::GetTexCoord( int tIdx, int index )
{
	Assert( tIdx < m_tcSize );
	Assert( m_texcoords[ tIdx ] );
	Assert( (index * 2) < m_texCoordSize[tIdx] );
	Vector2 tex( m_texcoords[tIdx][ index * 2],  m_texcoords[tIdx][ index * 2 + 1] );
	return tex;
}


//-----------------------------------------------------------------------------------------------


EmbeddedModel::~EmbeddedModel()
{
	grmModelFactory::FreeDeclarator( m_VertexDeclaration );	// problems finding the type

	delete m_VertexBuffer;
	delete m_shader;
	delete m_IndexBuffer; 

	delete [] m_batches;
}

 
void EmbeddedModel::Create( EmbeddedBuilder& builder, int* indices, int size  )
{
	m_VertexDeclaration = grmModelFactory::BuildDeclarator(builder.fvf(), 0, 0, 0);

	int	batchSize = ( 2 * 3 * 48);

	m_amtCullBatches = size/(batchSize) + 1;
	m_batches = rage_new CullBatch[ m_amtCullBatches];
	
	int texureIndex[1024];
	Assert( 1024 >  builder.GetNumPositions() );
	int numberOfVertices = builder.GetNumPositions();

	m_IndexBuffer = grcIndexBuffer::Create( size/2  );
	u16* resultantIndices = m_IndexBuffer->LockRW();

	int bNum = 0;
	for ( int j = 0; j < size; j+= batchSize)
	{
		int end = Min( j + batchSize, size );
		m_batches[ bNum ].m_startIndex = j/2;
		m_batches[ bNum ].m_endIndex = end/2;
		m_batches[ bNum ].m_bound.Invalidate();

		for (int i = j; i < end; i+=2 )
		{
			resultantIndices[i/2] = static_cast<u16>(indices[i] );
			texureIndex[ indices[i] ]=indices[i +1];
			m_batches[ bNum ].m_bound.GrowPoint( VECTOR3_TO_VEC3V(builder.GetPosition( indices[i] )) );
		}
		bNum++;
	}
	//Assert( m_amtCullBatches == bNum );
	m_amtCullBatches = bNum;

	m_IndexBuffer->Unlock();


	m_VertexBuffer = grcVertexBuffer::Create( numberOfVertices , *builder.fvf(), !__RESOURCECOMPILER && __PS3);
	grcVertexBufferEditor editor(m_VertexBuffer);

	int idx = 0;
	for ( int i = 0; i < numberOfVertices; i++ )
	{
		editor.SetPosition( i, builder.GetPosition( idx ) );
		if ( builder.HasNormals() )
		{
			editor.SetNormal( i, builder.GetNormal( idx ));
		}
		for ( int j = 0; j < builder.NumTexCoords(); j++ )
		{
			Assert( texureIndex[idx] < numberOfVertices);
			editor.SetUV( i, j, builder.GetTexCoord( j, texureIndex[idx] ));
		
		}
		idx++;
	}
	
}

#if HACK_GTA4
void  EmbeddedModel::Draw( const rage::grcViewport& view, int passIndex, grcEffectTechnique technique, bool enableCulling )
#else // HACK_GTA4
void  EmbeddedModel::Draw( const rage::grcViewport& view, int passIndex, grcEffectTechnique technique )
#endif // HACK_GTA4
{
	fragShaft shaft;
	shaft.Set(view,   RCC_MATRIX34(view.GetCameraMtx()) );

	// Bind the shader
	if ( m_shader->BeginDraw(grmShader::RMC_DRAW, true, technique ) )
	{
		m_shader->Bind( passIndex);

		GRCDEVICE.SetVertexDeclaration( m_VertexDeclaration);
		GRCDEVICE.SetStreamSource(0, *m_VertexBuffer, 0, m_VertexBuffer->GetVertexStride());
		GRCDEVICE.SetIndices( *m_IndexBuffer);

		int start =0;
		int endDraw = -1;
#if HACK_GTA4
		if( enableCulling )
#endif // HACK_GTA4		
		{
			for (int i = 0; i < m_amtCullBatches; i++ )
			{
				spdAABB box = m_batches[i].m_bound;
				box.Transform( view.GetWorldMtx() );
				if (shaft.IsVisible(box.GetBoundingSphere()))
				{
					endDraw = m_batches[i].m_endIndex;
				}
				else
				{
					if ( endDraw != -1 )
					{
						GRCDEVICE.DrawIndexedPrimitive((rage::grcDrawMode)m_DrawMode, 
							start, 
							endDraw - start);
					}
					start =m_batches[i].m_endIndex;
					endDraw =  -1;
				}
			}
		}
#if HACK_GTA4	
		else
		{
			endDraw = m_IndexBuffer->GetIndexCount();
		}
#endif // HACK_GTA4		
		if ( endDraw != -1 )
		{
			GRCDEVICE.DrawIndexedPrimitive((rage::grcDrawMode)m_DrawMode, 
				start, 
				endDraw - start);
		}

		GRCDEVICE.ClearStreamSource(0);

		// Unbind the shader
		m_shader->UnBind();
	}
	m_shader->EndDraw();


#if _DEBUG_EMBEDDED_CULLING
	grcWorldIdentity();
	for ( int j = 0; j < m_amtCullBatches; j++)
	{
		m_batches[j].m_bound.Draw( Color32( 1.0f, 0.0f, 1.0f, 1.0f));
	}
#endif
}

// Resourcing
IMPLEMENT_PLACE(EmbeddedModel);

#if __DECLARESTRUCT
void EmbeddedModel::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(EmbeddedModel);
	STRUCT_FIELD(m_DrawMode);
	//STRUCT_FIELD(m_VertexDeclaration);
	STRUCT_FIELD(m_VertexBuffer);
	STRUCT_FIELD(m_shader );
	STRUCT_END();
}
#endif
