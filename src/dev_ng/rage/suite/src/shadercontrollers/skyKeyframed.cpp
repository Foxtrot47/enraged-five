// #include "grpostfx\postfx.h"

#include "shadercontrollers/skyhat.h"
#include "skyKeyFramed.h"
#include "skyKeyFramed_parser.h"

#include "parser/manager.h"

using namespace rage;


Vector4	gVecOne = Vector4( 1.0f, 1.0f, 1.0f, 1.0f );
Vector4	gVecZero = Vector4( 0.0f, 0.0f, 0.0f, 0.0f );


#if __BANK && RMPTFX_BANK
static void AddKeyedWidget( bkBank &bank, 
						   ptxKeyframe& keyedWidget,
						   const Vector4& min,
						   const Vector4& max,
						   const char* name,
						   const char* l1 = 0, const char* l2 = 0, const char* l3 = 0,  const char* l4 = 0)
{

	float maxVal = Max( max.x, max.y, max.z, max.w );
	float minVal = Min( min.x, min.y, min.z, min.w );

	ptxKeyframeType	keyframeType = PTXKEYFRAMETYPE_FLOAT4;
	if (l2==NULL)
	{
		keyframeType = PTXKEYFRAMETYPE_FLOAT;
	}
	else if (l3==NULL)
	{
		keyframeType = PTXKEYFRAMETYPE_FLOAT2;
	}
	else if (l4==NULL)
	{
		keyframeType = PTXKEYFRAMETYPE_FLOAT3;
	}

	ptxKeyframeDefn* pKeyframeDefn = rage_new ptxKeyframeDefn
	(
		name, keyframeType,
		minVal, maxVal, 0.01f,
		0.0f, 24.0f, 0.01f,
		l1, l2, l3, l4
	);

	keyedWidget.SetDefn(pKeyframeDefn);
	keyedWidget.AddWidgets(bank);
}
#endif

inline Vector3 GetKey( float t, ptxKeyframe& val )
{
	Vec4V vData = val.Query(ScalarVFromF32(t));

	Vector3 res;
	res.Set( vData.GetXf(), vData.GetYf(), vData.GetZf() );
	return res;
}



void SkyDomeKeyFrameTimeElements::Load( const char* name )
{
	PARSER.LoadObject( name, "xml", *this);
}
Vector3		SkyDomeKeyFrameTimeElements::GetSunDirection( float timeOfDay )
{
	Vector3 res = GetKey( timeOfDay, m_SunDirectionKF );
	res.Normalize();
	return res;
}


#if __BANK && RMPTFX_BANK 

void SkyDomeKeyFrameTimeElements::Save( const char* name  )
{
	PARSER.SaveObject( name, "xml", this);
}
void SkyDomeKeyFrameTimeElements::AddWidgets( bkBank &bank )
{
	bank.PushGroup("Sky Timed Controls" );

	ptxKeyframeDefn* pKeyframeDefn = rage_new ptxKeyframeDefn
	(
		"Sun Direction", PTXKEYFRAMETYPE_FLOAT3,
		-3.14728f, 3.14728f, 0.01f,
		0.0f, 24.0f, 0.01f,
		"Sun Direction X", "Sun Direction Y", "Sun Direction Z"
	);

	m_SunDirectionKF.SetDefn(pKeyframeDefn);
	m_SunDirectionKF.AddWidgets(bank);
	bank.PopGroup();
}
SkyDomeKeyFrameTimeElements::~SkyDomeKeyFrameTimeElements()
{
	delete m_SunDirectionKF.GetDefn();
}
#else
SkyDomeKeyFrameTimeElements::~SkyDomeKeyFrameTimeElements() {}

#endif

void SkyDomeKeyFramedControl::UpdateUI()
{

#if __BANK && RMPTFX_BANK 
	m_SkyColorKF.UpdateWidget();
	m_AzimuthColorKF.UpdateWidget();
	m_AzimuthColorEastKF.UpdateWidget();
	m_SunsetColorKF.UpdateWidget();
	m_CloudColorKF.UpdateWidget();
	m_CloudThresholdThruCloudShadowStrengthKF.UpdateWidget();
	m_CloudShadowOffsetThruAzymuthStrengthKF.UpdateWidget();
	m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF.UpdateWidget();
	m_TopCloudColorKF.UpdateWidget();
	m_TBiasThruTDetailThruThresholdThruHeightKF.UpdateWidget();
	m_SunColorKF.UpdateWidget();
	m_UnStrengthThruCSpeedThruStarBrightnessKF.UpdateWidget();
	m_MoonColorKFAndBrightness.UpdateWidget();
	m_MoonBrightnessGlowStarBrightnessThresholdKF.UpdateWidget();
#endif
}



void SkyDomeKeyFramedControl::Load( const char* fileName )
{		
	parRTStructure parRTS;
	bool Loaded = false;

	Create( parRTS);
	
	Loaded = PARSER.LoadFromStructure( fileName, "xml", parRTS, NULL);

	if (Loaded)
	{
		// This will update the Rag controls reflecting the data that was just loaded.
		UpdateUI();
	}
}

SkyDomeKeyFramedControl::SkyDomeKeyFramedControl() : m_SunAxias( 1.0f, 0.0f )
{
	m_StarBlinkFrequency = 0.0f;
	m_StarBlinkIntensity = 0.0f;
}


void SetKeyed( ptxKeyframe& control, float time, const Vector3& col )
{
	Vector4	vec;
	vec.Set( col.x, col.y, col.z, 0.0f );
	if ( time == 0.0f )
	{
		control.Init(ScalarV(V_ZERO), RCC_VEC4V(vec));
	}
    else
    {
        control.AddEntry(ScalarVFromF32(time), RCC_VEC4V(vec));
    }
}
void SetKeyed( ptxKeyframe& control, float time, const Vector4& col )
{
	Vector4	vec;
	vec.Set( col.x, col.y, col.z, 0.0f );
	if ( time == 0.0f )
	{
		control.Init(ScalarV(V_ZERO), RCC_VEC4V(vec));
	}
    else
    {
        control.AddEntry(ScalarVFromF32(time), RCC_VEC4V(vec));
    }
}
void SetKeyed( ptxKeyframe& control, float time, float a, float b, float c, float d )
{	
	Vector4	vec;
	vec.Set( a, b, c, d );
	if ( time == 0.0f )
	{
		control.Init(ScalarV(V_ZERO), RCC_VEC4V(vec));
	}
    else
	{
		control.AddEntry(ScalarVFromF32(time), RCC_VEC4V(vec));
    }
}
void SetKeyed( ptxKeyframe& control, float time, float a, float b, float c )
{	
	Vector4	vec;
	vec.Set( a, b, c, 0.0f );
	if ( time == 0.0f )
	{
		control.Init(ScalarV(V_ZERO), RCC_VEC4V(vec));
	}
    else
	{
		control.AddEntry(ScalarVFromF32(time), RCC_VEC4V(vec));
    }
}

void SkyDomeKeyFramedControl::SetFromSettings( float time, SkyHatPerFrameSettings& settings )
{
	// sky
	SetKeyed( m_SkyColorKF,			time, settings.m_skyColor );
	SetKeyed( m_AzimuthColorKF,		time, settings.m_AzimuthColor );
	SetKeyed( m_AzimuthColorEastKF,	time, settings.m_AzimuthColorEast );
	SetKeyed( m_SunsetColorKF,			time, settings.m_SunsetColor );

	m_SunAxias = settings.m_SunAxias;

	// clouds
	SetKeyed( m_CloudColorKF,			time, settings.m_CloudColor );

	SetKeyed( m_CloudThresholdThruCloudShadowStrengthKF, time, 
		settings.m_CloudThreshold,
		settings.m_CloudBias,
		settings.m_CloudThickness,
		settings.m_CloudShadowStrength );

	
	SetKeyed( m_CloudShadowOffsetThruAzymuthStrengthKF, time, 
		settings.m_CloudShadowOffset,
		settings.m_CloudInscatteringRange,
		settings.m_CloudEdgeSmooth,
		settings.m_AzimuthStrength );
	

	SetKeyed( m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF, time, 
		settings.m_DetailScale,
		settings.m_DetailStrength,
		settings.m_CloudWarp,
		settings.m_CloudFadeOut );

	// top clouds
	SetKeyed( m_TopCloudColorKF,			time, settings.m_TopCloudColor );

	SetKeyed( m_TBiasThruTDetailThruThresholdThruHeightKF, time, 
		settings.m_TopCloudBias,
		settings.m_TopCloudDetail,
		settings.m_TopCloudThreshold,
		settings.m_TopCloudHeight );

	// Sun
	SetKeyed( m_SunColorKF,		time, settings.m_SunColor );

	SetKeyed( m_UnStrengthThruCSpeedThruStarBrightnessKF, time, 
		settings.m_UnderLightStrength,
		settings.m_CloudSpeed,
		settings.m_AzimuthHeight,
		settings.m_MoonVisiblity );

	SetKeyed( m_SunCentreKF, time,
		settings.m_SunCentreStart,
		settings.m_SunCentreEnd,
		settings.m_SunCentreIntensity,
		settings.m_SunOuterSize);

	// moon
	SetKeyed( m_MoonColorKFAndBrightness,		time, settings.m_MoonColor );

	SetKeyed( m_MoonBrightnessGlowStarBrightnessThresholdKF, time, 
		settings.m_MoonBrightness,
		settings.m_MoonGlow,
		settings.m_StarFieldBrightness,
		settings.m_StarFieldThreshold );

	if ( time == 24.0f )
	{
		UpdateUI();
	}
}

void SkyDomeKeyFramedControl::CalculateSkySettings( float timeOfDay, SkyHatPerFrameSettings& settings )
{
	settings.Clear();

	settings.m_skyColor = GetKey( timeOfDay, m_SkyColorKF ); 
	settings.m_AzimuthColor = GetKey( timeOfDay, m_AzimuthColorKF );
	settings.m_AzimuthColorEast = GetKey( timeOfDay, m_AzimuthColorEastKF );
	settings.m_SunsetColor.SetVector3( GetKey( timeOfDay, m_SunsetColorKF ) );

	// clouds
	settings.m_CloudColor = GetKey( timeOfDay, m_CloudColorKF );

	ScalarV vTimeOfDay = ScalarVFromF32(timeOfDay);
	Vec4V vKeyData;
	
	vKeyData = m_CloudThresholdThruCloudShadowStrengthKF.Query(vTimeOfDay);
	Vector4 cloudThresholdThruCloudShadowStrength = RCC_VECTOR4(vKeyData);

	settings.m_CloudThreshold = cloudThresholdThruCloudShadowStrength.x; 
	settings.m_CloudBias = cloudThresholdThruCloudShadowStrength.y;
	settings.m_CloudThickness = cloudThresholdThruCloudShadowStrength.z;
	settings.m_CloudShadowStrength = cloudThresholdThruCloudShadowStrength.w;

	vKeyData = m_CloudShadowOffsetThruAzymuthStrengthKF.Query(vTimeOfDay);
	Vector4 cloudShadowOffsetThruAzymuthStrength = RCC_VECTOR4(vKeyData);

	settings.m_CloudShadowOffset = cloudShadowOffsetThruAzymuthStrength.x; // m_SkyShader->GetEffect().SetVar(m_CloudShadowOffsetID		,cloudShadowOffsetThruAzymuthStrength.GetX());		
	settings.m_CloudInscatteringRange = cloudShadowOffsetThruAzymuthStrength.y; // m_SkyShader->GetEffect().SetVar(m_CloudInscatteringRangeID	,cloudShadowOffsetThruAzymuthStrength.GetY());
	settings.m_CloudEdgeSmooth = cloudShadowOffsetThruAzymuthStrength.z; // m_SkyShader->GetEffect().SetVar(m_CloudEdgeSmoothID			,cloudShadowOffsetThruAzymuthStrength.GetZ());
	settings.m_AzimuthStrength = cloudShadowOffsetThruAzymuthStrength.w; // m_SkyShader->GetEffect().SetVar(m_AzymuthStrengthID			,cloudShadowOffsetThruAzymuthStrength.GetW());

	vKeyData = m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF.Query(vTimeOfDay);
	Vector4 detailScaleThruDetailStrength = RCC_VECTOR4(vKeyData);

	settings.m_DetailScale = detailScaleThruDetailStrength.x;		// m_SkyShader->GetEffect().SetVar(m_DetailScaleID				,detailScaleThruDetailStrength.GetX());
	settings.m_DetailStrength = detailScaleThruDetailStrength.y;	// m_SkyShader->GetEffect().SetVar(m_DetailStrengthID			,detailScaleThruDetailStrength.GetY());
	settings.m_CloudWarp = detailScaleThruDetailStrength.z;
	settings.m_CloudFadeOut = detailScaleThruDetailStrength.w;

	//	settings.m_CloudThickness = detailScaleThruDetailStrength.GetZ();

	// top clouds
	settings.m_TopCloudColor = GetKey( timeOfDay, m_TopCloudColorKF );

	vKeyData = m_TBiasThruTDetailThruThresholdThruHeightKF.Query(vTimeOfDay);
	Vector4  TBiasThruTDetailThruThresholdThruHeight = RCC_VECTOR4(vKeyData);

	settings.m_TopCloudBias = TBiasThruTDetailThruThresholdThruHeight.x;		// m_SkyShader->GetEffect().SetVar(m_DetailScaleID				,detailScaleThruDetailStrength.GetX());
	settings.m_TopCloudDetail = TBiasThruTDetailThruThresholdThruHeight.y;	// m_SkyShader->GetEffect().SetVar(m_DetailStrengthID			,detailScaleThruDetailStrength.GetY());
	settings.m_TopCloudThreshold = TBiasThruTDetailThruThresholdThruHeight.z;
	settings.m_TopCloudHeight = TBiasThruTDetailThruThresholdThruHeight.w;

	// Sun
	settings.m_SunColor = GetKey( timeOfDay, m_SunColorKF );
	vKeyData = m_UnStrengthThruCSpeedThruStarBrightnessKF.Query(vTimeOfDay);
	Vector4  UnStrengthThruCSpeedThruStarBrightnessKF = RCC_VECTOR4(vKeyData);

	vKeyData = m_SunCentreKF.Query(vTimeOfDay);
	Vector4 sunCentre = RCC_VECTOR4(vKeyData);


	settings.m_SunCentreStart = sunCentre.x;
	settings.m_SunCentreEnd = sunCentre.y;
	settings.m_SunCentreIntensity = sunCentre.z;
	settings.m_SunOuterSize = sunCentre.w;

	settings.m_UnderLightStrength = UnStrengthThruCSpeedThruStarBrightnessKF.x;		// m_SkyShader->GetEffect().SetVar(m_DetailScaleID				,detailScaleThruDetailStrength.GetX());
	settings.m_CloudSpeed = UnStrengthThruCSpeedThruStarBrightnessKF.y;	// m_SkyShader->GetEffect().SetVar(m_DetailStrengthID			,detailScaleThruDetailStrength.GetY());
	settings.m_AzimuthHeight = UnStrengthThruCSpeedThruStarBrightnessKF.z;
	settings.m_MoonVisiblity = UnStrengthThruCSpeedThruStarBrightnessKF.w;

	vKeyData = m_MoonBrightnessGlowStarBrightnessThresholdKF.Query(vTimeOfDay);
	Vector4 moonBrightnessGlowStarBrightnessThresholdKF = RCC_VECTOR4(vKeyData);


	settings.m_MoonBrightness = moonBrightnessGlowStarBrightnessThresholdKF.x;
	settings.m_MoonGlow = moonBrightnessGlowStarBrightnessThresholdKF.y;
	settings.m_StarFieldBrightness = moonBrightnessGlowStarBrightnessThresholdKF.z;
	settings.m_StarFieldThreshold = moonBrightnessGlowStarBrightnessThresholdKF.w;


	// mooon
	settings.m_MoonColor =  GetKey( timeOfDay, m_MoonColorKFAndBrightness ); 

}

void SkyDomeKeyFramedControl::Create( parRTStructure& m_parRTS )
{
	m_parRTS.SetName("rmwSkydome");		
	m_parRTS.AddStructureInstance("SkyColorKF",									m_SkyColorKF);
	m_parRTS.AddStructureInstance("AzimuthColorKF",								m_AzimuthColorKF);
	m_parRTS.AddStructureInstance("AzimuthColorEastKF",								m_AzimuthColorEastKF);
	m_parRTS.AddStructureInstance("SunsetColorKF",								m_SunsetColorKF);
	m_parRTS.AddStructureInstance("CloudColorKF",								m_CloudColorKF);
	m_parRTS.AddStructureInstance("CloudThresholdThruCloudShadowStrengthKF",	m_CloudThresholdThruCloudShadowStrengthKF);
	m_parRTS.AddStructureInstance("CloudShadowOffsetThruAzymuthStrengthKF",		m_CloudShadowOffsetThruAzymuthStrengthKF);
	m_parRTS.AddStructureInstance("DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF",	m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF);

	m_parRTS.AddStructureInstance("TopCloudColorKF",										m_TopCloudColorKF);
	m_parRTS.AddStructureInstance("TBiasThruTDetailThruThresholdThruHeightKF",				m_TBiasThruTDetailThruThresholdThruHeightKF);

	m_parRTS.AddStructureInstance("SunColorKF",												m_SunColorKF);
	m_parRTS.AddStructureInstance("SunCentreKF",											m_SunCentreKF );

	m_parRTS.AddStructureInstance("UnStrengthThruCSpeedThruStarBrightnessKF",				m_UnStrengthThruCSpeedThruStarBrightnessKF);
	m_parRTS.AddStructureInstance("m_MoonColorKFAndBrightness",								m_MoonColorKFAndBrightness);
	m_parRTS.AddStructureInstance("m_MoonBrightnessGlowStarBrightnessKF",					m_MoonBrightnessGlowStarBrightnessThresholdKF);

	m_parRTS.AddVectorMember("SunAxias", NULL, &m_SunAxias, parMemberType::TYPE_VECTOR2 );

	m_parRTS.AddValue("StarBlinkFrequency", &m_StarBlinkFrequency );
	m_parRTS.AddValue("StarBlinkIntensity", &m_StarBlinkIntensity );

}

#if __BANK && RMPTFX_BANK 
void SkyDomeKeyFramedControl::Save( const char* fileName )
{
	parRTStructure		parRTS;		
	Create( parRTS);

	PARSER.SaveFromStructure( fileName, "xml", parRTS);
}

void SkyDomeKeyFramedControl::AddWidgets(bkBank &bank)
{
		bank.PushGroup( "Sky", false );

			AddKeyedWidget( bank, 
							m_SkyColorKF,
							gVecZero, gVecOne,
							"Sky Color",
							"SkyColorRed","SkyColorGreen","SkyColorBlue" );


			AddKeyedWidget( bank, 
				m_AzimuthColorKF,
				gVecZero, gVecOne,
				"Azimuth Color",
				"Azimuth Color Red","Azimuth Color Green","Azimuth Color Blue" );

			AddKeyedWidget( bank, 
				m_AzimuthColorEastKF,
				gVecZero, gVecOne,
				"Azimuth Color East",
				"Azimuth Color Red","Azimuth Color Green","Azimuth Color Blue" );

			AddKeyedWidget( bank, 
				m_SunsetColorKF,
				gVecZero, gVecOne,
				"Sunset Color",
				"Sunset Color Red","Sunset Color Green","Sunset Color Blue" );

			bank.AddSlider( "Sun Axias", &m_SunAxias, -1.0f, 1.0f , 0.01f);



		bank.PopGroup();
		// Clouds 
		bank.PushGroup("Clouds", false );

			AddKeyedWidget( bank, 
				m_CloudColorKF,
				gVecZero, gVecOne,
				"Cloud Color",
				"Cloud Color Red","Cloud Color Green","Cloud Color Blue" );

			// Cloud 2:
			AddKeyedWidget( bank, 
				m_CloudThresholdThruCloudShadowStrengthKF,
				Vector4( -10.0f, 0.0f, 0.0f, 0.0f) , Vector4( 10.0f, 2.0f, 1.0f, 1.0f ),
				"Cloud 2",
				"Cloud Threshold","Cloud Bias","Clould Thickness", "Shadow Stength" );
			
			// Cloud 2:
			AddKeyedWidget( bank, 
				m_CloudShadowOffsetThruAzymuthStrengthKF,
				Vector4( 0.0f, 0.01f, 0.0f, 0.0f) , Vector4( 1.0f, 1.5f, 1.0f, 8.0f ),
				"Cloud 2",
				"Shadow Offset","Inscattering Range","Edge Smooth", "Azymuth Strength" );

			
			// Cloud 3:
			AddKeyedWidget( bank, 
				m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF,
				Vector4( 0.0f, 0.0f, 0.0f, -1.0f) , Vector4( 768.0f, 1.0f, 2.0f, 1.0f ),
				"Cloud 2",
				"Detail Scale","Detail Strength", "m_CloudWarp", "m_CloudFadeOut" );

		
			bank.PushGroup("Top Layer", false );

				AddKeyedWidget( bank, 
					m_TopCloudColorKF,
					gVecZero, gVecOne,
					"Top Layer Color",
					"Top Layer  Red","Top Layer Green","Top Layer  Blue" );

				AddKeyedWidget( bank, 
					m_TBiasThruTDetailThruThresholdThruHeightKF,
					Vector4( 0.0f, 0.0f, -10.0f, 0.0f) , Vector4( 2.0f, 10.0f, 10.0f, 10.0f ),
					"Top Layer Settings",
					"Top Bias","Top Detail","Top Threshold", "Top Height" );

			bank.PopGroup();

		bank.PopGroup();

		bank.PushGroup( "Sun And Moon", false );

			AddKeyedWidget( bank, 
				m_SunColorKF,
				gVecZero, gVecOne,
				"Sun Color",
				"Sun Red","Sun Green","Sun Blue" );

			AddKeyedWidget( bank, 
				m_UnStrengthThruCSpeedThruStarBrightnessKF,
				Vector4( 0.0f, 0.0f, 0.0f, 0.0f) , Vector4( 4.0f, 2.0f, 24.0f, 5.0f ),
				"Sun and Moon Details",
				"Under Light Strength","Cloud Speed","Azimuth Height" , "Moon Visiblity");

		
			AddKeyedWidget( bank, 
			m_SunCentreKF,
			Vector4( 0.0f, 0.0f, 0.0f, -1.0f) , gVecOne,
			"Sun Center",
			"Sun Center Start","Sun Center End","Sun Center Intensity" ,"Sun Outer Size");
			
			AddKeyedWidget( bank, 
				m_MoonColorKFAndBrightness,
				gVecZero, gVecOne,
				"Moon Color",
				"Moon Red","Moon Blue","Moon Green" );


			AddKeyedWidget( bank, 
				m_MoonBrightnessGlowStarBrightnessThresholdKF,
				Vector4( 0.0f, 0.0f, 0.0f, 0.0f) , Vector4( 70.0f, 5.0f, 5.0f, 1.0f ),
				"Moon Details",
				"Moon Brightness","Moon Glow","StarField Brightness" , "StarField Threshold");


			bank.AddSlider( "Star Blink Frequency", &m_StarBlinkFrequency, 0.0f, 10000.0f, 0.5f);
			bank.AddSlider( "Star Blink Intensity", &m_StarBlinkIntensity,  0.0f, 0.25f , 0.00001f);

			

		bank.PopGroup();
}
void SkyDomeKeyFramedControl::CleanupUI()
{
	delete m_SkyColorKF.GetDefn();
	delete m_AzimuthColorKF.GetDefn();
	delete m_AzimuthColorEastKF.GetDefn();
	delete m_SunsetColorKF.GetDefn();
	delete m_CloudColorKF.GetDefn();
	delete m_CloudThresholdThruCloudShadowStrengthKF.GetDefn();
	delete m_CloudShadowOffsetThruAzymuthStrengthKF.GetDefn();
	delete m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF.GetDefn();
	delete m_TopCloudColorKF.GetDefn();
	delete m_TBiasThruTDetailThruThresholdThruHeightKF.GetDefn();
	delete m_SunColorKF.GetDefn();
	delete m_UnStrengthThruCSpeedThruStarBrightnessKF.GetDefn();
	delete m_SunCentreKF.GetDefn();
	delete m_MoonColorKFAndBrightness.GetDefn();
	delete m_MoonBrightnessGlowStarBrightnessThresholdKF.GetDefn();
}
#else
void SkyDomeKeyFramedControl::CleanupUI() {}
#endif

#if __BANK && RMPTFX_BANK 

void SkyKeyFramedFogControl::Save( const char* fileName )
{
	parRTStructure parRTS;
	Create( parRTS);

	PARSER.SaveFromStructure( fileName, "xml", parRTS);
}
SkyKeyFramedFogControl::~SkyKeyFramedFogControl()
{
	delete m_FarColorKF.GetDefn();
	delete m_NearColorKF.GetDefn();
	delete m_FogDensityFinalFogOpacityColorBlendStartColorBlendEndKF.GetDefn();
}
void SkyKeyFramedFogControl::AddWidgets(bkBank &bank)
{
	bank.PushGroup( "Fog", false );


//	bank.AddButton("Load",datCallback(MFA(SkyKeyFramedFogControl::Load),this));
//	bank.AddButton("Save",datCallback(MFA(SkyKeyFramedFogControl::Save),this));
	AddKeyedWidget( bank, 
		m_FarColorKF,
		gVecZero, gVecOne,
		"Far Fog Color",
		"Far Fog Red","Far Fog Green","Far Fog Blue" );

	AddKeyedWidget( bank, 
		m_NearColorKF,
		gVecZero, gVecOne,
		"Min Fog Color",
		"Min Fog Red","Min Fog Green","Min Fog Blue" );

	AddKeyedWidget( bank, 
		m_FogDensityFinalFogOpacityColorBlendStartColorBlendEndKF,
		Vector4( 0.0f, 0.0f, 0.0f, 0.0f) , Vector4( 1.0f, 1.0f, 1.0f, 1.0f ),
		"Fog Details",
		"Fog Density","Final Fog Clamp","Color Blend Start" , "Color Blend End");

	bank.AddSlider("Fog Start", &m_fogStart, -1000.0f, 1000.0f, 0.2f, NullCB, "Fog start" );
	bank.AddSlider("Fog Start", &m_fogEnd, -1000.0f, 10000.0f, 0.2f, NullCB, "Fog start" );
	bank.AddSlider("Use Automatic Far", &m_autoFar, 0.0f, 1.0f, 0.01f, NullCB, "Use Automatic Far" );
	bank.PopGroup();
}
#endif

void SkyKeyFramedFogControl::SetFromSettings( float t, SkyHatPerFrameSettings& settings, const Vector3& sunDirection )
{
	Vector3 KeyedFarColor = GetKey( t,m_FarColorKF );
	Vector3 KeyedNearColor = GetKey( t, m_NearColorKF );
	Vector3 fogCol = SkyHatGetFarFogColor( settings , sunDirection );
	fogCol = Lerp( m_autoFar, KeyedFarColor, fogCol );

	fogCol.Min( fogCol, Vector3( 1.0f, 1.0f, 1.0f ));
	KeyedNearColor.Min( KeyedNearColor, Vector3( 1.0f, 1.0f, 1.0f ));

	ScalarV vTime = ScalarVFromF32(t);
	Vec4V vKeyData = m_FogDensityFinalFogOpacityColorBlendStartColorBlendEndKF.Query(vTime);
	Vector4 fogDensityOpacColBlendStartColorBlendEnd = RCC_VECTOR4(vKeyData);

	fogDensityOpacColBlendStartColorBlendEnd.x = fogDensityOpacColBlendStartColorBlendEnd.x / 10.0f; // rescale
	/* if ( GRPOSTFX )
	{
		Vector4 fogParams( m_fogStart, fogDensityOpacColBlendStartColorBlendEnd.z, fogDensityOpacColBlendStartColorBlendEnd.x, m_fogEnd );
		Vector4 fogMinColor(KeyedNearColor.x, KeyedNearColor.y, KeyedNearColor.z, fogDensityOpacColBlendStartColorBlendEnd.w );
		Vector4 fogMaxColor(fogCol.x, fogCol.y, fogCol.z, fogDensityOpacColBlendStartColorBlendEnd.y );

		GRPOSTFX->SetFogMinColor( fogMinColor );
		GRPOSTFX->SetFogMaxColor( fogMaxColor );
		GRPOSTFX->SetFogParams( fogParams );
	} */
}
void SkyKeyFramedFogControl::UpdateUI()
{
#if __BANK && RMPTFX_BANK 
	m_FarColorKF.UpdateWidget();
	m_NearColorKF.UpdateWidget();
	m_FogDensityFinalFogOpacityColorBlendStartColorBlendEndKF.UpdateWidget();
#endif
}

void SkyKeyFramedFogControl::Create( parRTStructure& m_parRTS )
{
	m_parRTS.SetName("skyFog");		
	m_parRTS.AddStructureInstance("FarColor",									m_FarColorKF);
	m_parRTS.AddStructureInstance("NearColorKF",								m_NearColorKF);
	m_parRTS.AddStructureInstance("FogDensityFinalFogOpacityColorBlendStartColorBlendEndKF",								m_FogDensityFinalFogOpacityColorBlendStartColorBlendEndKF);
	
	m_parRTS.AddValue("AutoFar", &m_autoFar );
	m_parRTS.AddValue("FogStart", &m_fogStart );
	m_parRTS.AddValue("FogEnd", &m_fogEnd );
}


void SkyKeyFramedFogControl::Load( const char* fileName )
{		
	parRTStructure parRTS;
	bool Loaded = false;
	Create( parRTS);

	Loaded = PARSER.LoadFromStructure(fileName, "xml", parRTS, NULL);
	
	if (Loaded)
	{
		UpdateUI();
	}
}

// SkyLightUserInterface

#if __BANK && RMPTFX_BANK 
void SkyLightUserInterface::AddWidgets( bkBank& bank )
{
	bank.AddColor( "Average Ground Color", &m_BounceGroundColor );

	ptxKeyframeDefn* pKeyframeDefn = rage_new ptxKeyframeDefn
	(
		"Intensities", PTXKEYFRAMETYPE_FLOAT2,
		0.0f, 2.0f, 0.01f,
		0.0f, 24.0f, 0.01f,
		"Sky Light Intensity" , "Bounce Light Intensity"
	);

	m_Intensity.SetDefn(pKeyframeDefn);
	m_Intensity.AddWidgets(bank);
}
void SkyLightUserInterface::Save( const char* fileName )
{
	parRTStructure		parRTS;		
	Create( parRTS);

	PARSER.SaveFromStructure( fileName, "xml", parRTS);
}
#endif
void SkyLightUserInterface::UpdateUI()
{
#if __BANK && RMPTFX_BANK 
	m_Intensity.UpdateWidget();
#endif
}
void SkyLightUserInterface::Create( parRTStructure& m_parRTS )
{
	m_parRTS.SetName("skyLighting");		
	m_parRTS.AddStructureInstance("Intensities",									m_Intensity);
	m_parRTS.AddValue("GroundColor", &m_BounceGroundColor );
}

void SkyLightUserInterface::Load( const char* filename )
{
	parRTStructure		parRTS;		
	Create( parRTS);

	bool loaded = PARSER.LoadFromStructure(filename, "xml", parRTS, NULL);
	if ( loaded )
	{
		UpdateUI();
	}
}
