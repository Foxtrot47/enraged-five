//**************************************************************//
//  RAGE Perlin Noise shader
//
//  David Serafim 18 Jan 2006
//**************************************************************//
#include "../../../base/src/shaderlib/rage_common.fxh"
#if HACK_GTA4
	// embedded shaders should still obey Jimmy rules when it comes to global register allocation:
	#include "../../../../game/shader_source/common.fxh"
#endif

#include "../../../base/src/shaderlib/rage_samplers.fxh"
#include "../../../base/src/shaderlib/rage_common.fxh"
#include "../../../base/src/shaderlib/rage_drawbucket.fxh"
RAGE_DRAWBUCKET(1);

BeginSampler(sampler2D,noise1Texture,Noise1Sampler,Noise1Tex)
    string UIName = "1st Noise Layer Texture";
ContinueSampler(sampler2D,noise1Texture,Noise1Sampler,Noise1Tex)
		AddressU  = WRAP;        
		AddressV  = WRAP;
		AddressW  = WRAP;
		MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
		LOD_BIAS = 0;
EndSampler;

const float2 Velocity1
<
	string UIName = "1st Noise Layer Velocity";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 0.10;
	float UIStep = 0.01;
> = {-0.01,0.01};

const float2 Velocity2
<
	string UIName = "2nd Noise Layer Velocity";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 0.10;
	float UIStep = 0.01;
> = {0.005,0.02};

const float Frequency1
<
	string UIName = "1st Noise Layer Frequency";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 10.00;
	float UIStep = 0.1;
> = 1.9;

const float Frequency2
<
	string UIName = "2nd Noise Layer Frequency";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 10.00;
	float UIStep = 0.1;
> = 1.0;

const float Contrast
<
	string UIName = "Contrast";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 2.00;
	float UIStep = 0.1;
> = 1.1;

const float Brightness
<
	string UIName = "Brightness";
	string UIWidget = "Numeric";
	float UIMin = 0.00;
	float UIMax = 2.00;
	float UIStep = 0.1;
> = 0.76;

const float NoiseWarp
<
	string UIName = "NoiseWarp";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 64.0f;
	float UIStep = 0.1;
> = 12.0f;


const float Persistance : Persistance = 1.0/1.3;
const float Phase : Phase = 0.0f;
const float ClumpBump : ClumpBump = 1.0f;

//const int Octaves : Octaves =  3;



struct VS_OUTPUT
{
   float4 Pos:       DX_POS;
   float2 TexCoord:   TEXCOORD0;
};


#pragma dcl position

VS_OUTPUT vs_main(float4 inPos: POSITION0)
{
	VS_OUTPUT OUT;

	OUT.Pos = float4(inPos.xy*2-1,0,1);
	OUT.TexCoord.x = inPos.x;
	OUT.TexCoord.y = 1-inPos.y;

	return OUT;
}


float2 SphericalWarp( float2 Tex )
{
	const float hackScale = 12.0f; //16.0f;
	Tex = Tex - 0.5;
	float mag =   ragePow( dot( Tex, Tex ) , NoiseWarp);
	Tex =  mag * normalize( Tex)* hackScale + 0.5;
	return Tex;
}
float3 calculateCloudNoise(VS_OUTPUT IN)
{
	float2 tex = SphericalWarp( IN.TexCoord );
	
	float2 finalnoise = 0;
	const float texelSize = 1.0f/128.0f; // 512;
	float4 freq = float4( Frequency1, Frequency1, Frequency2, Frequency2 );  //float4( 1.9, 1.9, 1.0f, 1.0f );
	float4 scale = (texelSize ) * 16.0f * tex.xyxy;
	float pers = 0.5;
	
	float4 offset =float4( Phase*Velocity1, Phase*Velocity2 ); //  float4( Phase*float2( -0.01,0.01 ), Phase*float2( 0.005,0.02 ) ); //
	
	const int Octaves = 3;
	
	float4 texcoord = scale + offset;
	scale*= freq;
	float4 texcoord2 = scale + offset;
	
	float4 noiseVal1 = float4( tex2D(Noise1Sampler, texcoord.xy ).xy ,
						tex2D(Noise1Sampler, texcoord.zw ).xy );
		
	float4 noiseVal2 = float4( tex2D(Noise1Sampler, texcoord2.xy ).xy,
						tex2D(Noise1Sampler, texcoord2.zw ).xy );
						
	finalnoise.x +=  dot( noiseVal1.xz,  pers.xx);
	finalnoise.y +=  dot( noiseVal1.yw,  pers.xx);

	
	

	pers *= Persistance;
	finalnoise.x += dot( noiseVal2.xz,  pers.xx);
	finalnoise.y +=  dot( noiseVal2.yw,  pers.xx);
	scale*= freq;
	pers *= Persistance;

	float2 midVal;
	midVal = finalnoise.xy;

	for(int i=1;i< Octaves ;i++)
	{
		texcoord = scale + offset;
		scale*= freq;
		float4 texcoord2 = scale + offset;
		
		float4 noiseVal1 = float4( tex2D(Noise1Sampler, texcoord.xy ).xy ,
							tex2D(Noise1Sampler, texcoord.zw ).xy );
			
		float4 noiseVal2 = float4( tex2D(Noise1Sampler, texcoord2.xy ).xy,
							tex2D(Noise1Sampler, texcoord2.zw ).xy );
							
		finalnoise.x +=  dot( noiseVal1.xz,  pers.xx);
		finalnoise.y +=  dot( noiseVal1.yw,  pers.xx);
		pers *= Persistance;
		finalnoise.x += dot( noiseVal2.xz,  pers.xx);
		finalnoise.y +=  dot( noiseVal2.yw,  pers.xx);
		scale*= freq;
		pers *= Persistance;
	}
	return float3( finalnoise,(finalnoise.x - midVal.x ) * 2.0f);
};
float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	float3 finalnoise = calculateCloudNoise( IN);

	float3 color = saturate( (finalnoise.xyz-Contrast.xxx)) * Brightness.xxx;
	color.z= lerp( 1.0f, color.z,  ClumpBump ); 
	
	return float4( saturate(color.xyz), 1.0f);
}
float4 ps_mainTweaked(VS_OUTPUT IN) : COLOR 
{
	float3 finalnoise = calculateCloudNoise( IN);

	float3 color = (finalnoise.xyz-Contrast.xxx) * 0.65f;
	color.z= lerp( 1.0f, color.z,  ClumpBump ); 
	
	
	return float4( saturate(color.xyz), 1.0f);
}

//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
      ZWRITEENABLE = false;
      ZENABLE = false;
      CULLMODE = NONE;
      ALPHABLENDENABLE = false;
      ALPHATESTENABLE = false;

      VertexShader = compile VERTEXSHADER vs_main(); //VS_ragePassThroughNoXformTexOnly();  // 
      PixelShader = compile PIXELSHADER ps_main();
   }
}
//--------------------------------------------------------------//
technique draw_improved
{
   pass p0
   {
      ZWRITEENABLE = false;
      ZENABLE = false;
      CULLMODE = NONE;
      ALPHABLENDENABLE = false;
      ALPHATESTENABLE = false;

      VertexShader = compile VERTEXSHADER vs_main(); //VS_ragePassThroughNoXformTexOnly();  // 
      PixelShader = compile PIXELSHADER ps_mainTweaked();
   }
}
