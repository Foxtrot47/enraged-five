<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::IntervalShadows" base="rage::ShaderFragment" autoregister="true">
	<float name="m_softness" init="16.0f" min="0.0f" max="2048.0f" step="0.1f"/>
	<bool name="m_on" init="1"/>
	<bool name="m_prtOn" init="1"/>
	<float name="m_sunLightIntensity" min="0.0f" max="10.0f" step="0.05f"/>
</structdef>

<structdef type="rage::AtmosphericScattering" base="rage::ShaderFragment" autoregister="true">
	<float name="m_hazeDistance" min="0.0f" max="1.0f" step="0.01f"/>
	<float name="m_hazeIntensity" min="0.0f" max="1.0f" step="0.05f"/>
	<bool name="m_on" init="1"/>
</structdef>

<structdef type="rage::FogControl" base="rage::ShaderFragment" autoregister="true">
	<Color32 name="m_color"/>
	<float name="m_density" init="0.001f" min="0.0f" max="1.0f" step="0.0001f"/>
	<float name="m_speed" init="0.0001f" min="0.0f" max="20.0f" step="0.0001f"/>
	<float name="m_noiseTile" init="0.4f" min="0.0f" max="20.0f" step="0.0001f"/>
	<float name="m_noiseTileY" init="0.4f" min="0.0f" max="20.0f" step="0.0001f"/>
	<float name="m_noiseBias" init="0.0f" min="-5.0f" max="5.0f" step="0.0001f"/>
	<float name="m_noiseScale" init="1.0f" min="0.0f" max="10.0f" step="0.0001f"/>
	<float name="m_maxHeight" init="100.0f" min="-100.0f" max="256.0f" step="0.5f"/>
	<float name="m_minHeight" init="0.0f" min="-100.0f" max="256.0f" step="0.5f"/>
	<float name="m_fuzzHeight" init="10.0f" min="-100.0f" max="256.0f" step="0.5f"/>
	<bool name="m_on" init="1"/>
</structdef>

<structdef type="rage::WaterFogControl" base="rage::ShaderFragment" autoregister="true">
	<Color32 name="m_color"/>
	<float name="m_density" init="0.001f" min="0.0f" max="20.0f" step="0.0001f"/>
	<float name="m_height" init="0.01f" min="-100.0f" max="100.0f" step="0.0001f"/>
	<bool name="m_on" init="1"/>
</structdef>

<structdef type="rage::EnviromentDomeLighting" base="rage::ShaderFragment" autoregister="true">
	<Vector3 name="m_sky" type="color"/>
	<Vector3 name="m_horizon" type="color"/>
	<Vector3 name="m_ground" type="color"/>
	<float name="m_intensity" min="0.0f" max="5.0f" step="0.05f"/>
	<bool name="m_on" init="1"/>
</structdef>

<structdef type="rage::VerletWaterSimulation" base="rage::ShaderFragment" autoregister="true">
    <int name="m_simWidth" init="256" min="0"/>
    <int name="m_simHeight" init="256" min="0"/>
    <bool name="m_on"/>
</structdef>

<structdef type="rage::VerletWaterPerturbation" base="rage::ShaderFragment" autoregister="true">
    <bool name="m_on"/>
</structdef>

<structdef type="rage::VerletWaterSurface" base="rage::ShaderFragment" autoregister="true">
    <float name="m_phaseRate" init="0.2f"/>
    <bool name="m_on"/>
</structdef>

<structdef type="rage::SkyhatPerlinNoise" base="rage::ShaderFragment" autoregister="true">
	<float name="m_phaseRate" init="0.05f" min="0.0f"/>
    <bool name="m_on"/>
</structdef>

<structdef type="rage::DayLighting" base="rage::ShaderFragment" autoregister="true">
</structdef>

</ParserSchema>