//
//	TITLE 
//		Library of rountines for manipulating spherical harmonics 
//


#ifndef SHADERCONTROLLERS_SPHERICALLIGHT_H
#define SHADERCONTROLLERS_SPHERICALLIGHT_H
//#include <vector/vector3.h>
#include "grcore/texture.h"

namespace rage
{

inline float sinc(float x)
{               /* Supporting sinc function */
	if (fabs(x) < 1.0e-4) return 1.0 ;
	else return(sin(x)/x) ;
}

//
//	PURPOSE
//		Controls a 3rd order spherical harmonic
//		which is great for diffuse environment lighting
//		http://www1.cs.columbia.edu/~ravir/papers/envmap/index.html has details of use for irradiance map
//
class SphericalLight3rdOrder
{
public:
	SphericalLight3rdOrder()
	{
		Reset();
	}

	void Reset()
	{
		for (int i = 0; i < NumberCoefficients(); i++)
		{
			SHCoeff[i].Zero();
		}
	}
	int NumberCoefficients()	const 	{ return 9; }

	//
	//	PURPOSE 
	//		Given a light information will project it into SH 
	//
	void AddLight( const Vector3& lightColor, const Vector3& direction, float projectedAngle )
	{
		const float fNormalisation = PI*16/17.0f;
		const float fConst1 = square(0.282095f) * fNormalisation * 1;
		const float fConst2 = square(0.488603f) * fNormalisation * (2/3.0f);
		const float fConst3 = square(1.092548f) * fNormalisation * (1/4.0f);
		const float fConst4 = square(0.315392f) * fNormalisation * (1/4.0f);
		const float fConst5 = square(0.546274f) * fNormalisation * (1/4.0f);

		Vector3 color = lightColor * projectedAngle;

		Vector3 d2 = direction * direction;
		float f1 = 3.0f * d2.z - 1.0f;
		float f2 = d2.x - d2.y;
		Vector3 dirnSecond = Vector3( direction.x * direction.z,
									  direction.z * direction.y,
									direction.y * direction.x );


		SHCoeff[0] += color * fConst1;
		SHCoeff[1] += color * fConst2 * direction.x;		
		SHCoeff[2] += color * fConst2 * direction.y;		
		SHCoeff[3] += color * fConst2 * direction.z;	
		SHCoeff[4] +=  color * fConst3 * dirnSecond.x ;	
		SHCoeff[5] += color * fConst3 *  dirnSecond.y ;		
		SHCoeff[6] += color * fConst3 *  dirnSecond.z ;
		SHCoeff[7] += color * fConst4 * f1;		
		SHCoeff[8] += color * fConst5 * f2;
	}


	Vector3 Sample( const Vector3& direction ) const
	{	
		Vector3 d2 = direction * direction;
		float f1 = 3.0f * d2.z - 1.0f;
		float f2 = d2.x - d2.y;
		Vector3 dirnSecond = Vector3( direction.x * direction.z,
			direction.z * direction.y,
			direction.y * direction.x );

		Vector3 result = SHCoeff[0];
		result += SHCoeff[1] *direction.x;		
		result += SHCoeff[2] * direction.y;		
		result += SHCoeff[3]  * direction.z;	
		result +=  SHCoeff[4]* dirnSecond.x ;	
		result += SHCoeff[5] *  dirnSecond.y ;		
		result += SHCoeff[6]*  dirnSecond.z ;
		result += SHCoeff[7]  * f1;		
		result += SHCoeff[8]  * f2;

		return result;
	}
	Vector3* GetCoeffs()
	{
		return SHCoeff;
	}
	
	typedef Functor3Ret<Vector3, float, float, const Vector3& > SampleFunctor;

	//
	//	PURPOSE 
	//		Using the functor samples a enviroment to create a spherical harmonic
	//
	void AddEnvironmentMap(int width, SampleFunctor&  Sample )
	{
		for( int i=0; i<width; i++)
		{
			for( int j=0; j<width; j++)
			{

				float v = ((float)width/ 2.0f  - i)/((float) width/2.0f);  /* v ranges from -1 to 1 */
				float u = (j-(float)width/ 2.0f )/( (float)width/2.0f);    /* u ranges from -1 to 1 */
				float r = sqrt(u*u+v*v) ;               /* The "radius" */
				if (r > 1.0) continue ;           /* Consider only circle with r<1 */

				float theta = PI*r ;                    /* theta parameter of (i,j) */
				float phi = atan2(v,u) ;                /* phi parameter */

				Vector3 direction(  sin(theta)*cos(phi) ,         /* Cartesian components */
									cos(theta),				
									sin(theta)*sin(phi) );
				
				/* Computation of the solid angle.  This follows from some
				elementary calculus converting sin(theta) d theta d phi into
				coordinates in terms of r.  This calculation should be redone 
				if the form of the input changes */

				float domega = (2*PI/width)*(2*PI/width)*sinc(theta) ;
				AddLight(  Sample( v, u, direction  ), direction ,domega);
			}
		}
	}

	template<class FUNCTOR>
	void AddSkyHat(int width, FUNCTOR&  Sample )
	{
		for( int i=0; i<width; i++)
		{
			for( int j=0; j<width; j++)
			{

				float v = (i - (float)width/ 2.0f)/((float) width/2.0f);  /* v ranges from -1 to 1 */
				float u = (j-(float)width/ 2.0f )/( (float)width/2.0f);    /* u ranges from -1 to 1 */
				float r = sqrt(u*u+v*v) ;               /* The "radius" */
				//if (r > 1.0) continue ;           /* Consider only circle with r<1 */
				static float range = 1.1f;
				if ( r > range ) continue;
			
				Vector3 direction( v, 1.0f - r, u );
	
				//direction.Negate();
				/* Computation of the solid angle. */

				float domega = (2*PI/width)*(PI/width)*0.25f;// only doing top half of dome
				AddLight(  Sample( i, j ), direction ,domega);
			}
		}
	}
private:
	Vector3	SHCoeff[9];
};


// fwd reference
//class SphericalLight3rdOrder;

//
//	PURPOSE 
//		To create a enviroment texture from three colors
//
class EnvironmentLightDome
{
public:
	EnvironmentLightDome(  const Vector3& sky, const Vector3& horizon, const Vector3& ground )
		:  m_sky( sky ),
		   m_horizon( horizon),
		   m_ground( ground )
	{}

	Vector3 Sample( float UNUSED_PARAM( x ), float UNUSED_PARAM(  y ), const Vector3& dir ) const
	{
		float uy = dir.y;
		float top = Clamp( uy, 0.0f, 1.0f );
		float bot = Clamp( -uy, 0.0f, 1.0f );
		float mid =  1.0f - fabs( uy );
		return top * m_sky + mid * m_horizon + bot * m_ground;
	}
private:

	Vector3	m_sky;
	Vector3 m_horizon;
	Vector3 m_ground;
};

		
//
//	PURPOSE 
//		To create a enviroment texture from three colors
//
class EnvironmentSkyMapTexture
{

public:
	EnvironmentSkyMapTexture(  grcTexture* tex, const Vector3& ground , float intensity, int numSamples, bool& succeed )
		:   m_intensity( intensity),
			m_ground( ground ),
			m_tex( tex ),
			m_width( tex->GetWidth() ),
			m_scale( tex->GetWidth() / numSamples )
	{
		succeed = tex->LockRect( 0, 0 , m_lock);
		m_colorMap = (u32*) m_lock.Base;
	}
	~EnvironmentSkyMapTexture()
	{
		m_tex->UnlockRect( m_lock );
	}
	Vector3 operator()( int x, int y  ) const
	{
		x *= m_scale;
		y *= m_scale;
		Color32		col = Color32( m_colorMap[ x + y * (int)m_width ] );
		return Vector3( col.GetRedf()* m_intensity, col.GetGreenf() * m_intensity, col.GetBluef() * m_intensity );
	}
private:
	grcTextureLock		m_lock;
	Vector3				m_ground;
	float				m_intensity;
	grcTexture*			m_tex;
	u32*				m_colorMap;
	int				m_width;
	int				m_scale;
};
};


#endif


