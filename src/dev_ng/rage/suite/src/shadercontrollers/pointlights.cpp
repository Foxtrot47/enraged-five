#if 0
// 
// shadercontrollers/pointlights.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "pointlights.h"

#include "grcore/device.h"
#include "grcore/effect.h"
#include "grcore/light.h"
#include "grcore/state.h"

#include "grprofile/pix.h"
#include "system/xtl.h"



#define POINT_LIGHT_COUNT	4
#define DIR_LIGHT_COUNT		4



using namespace rage;


Rdr2Lighting::Rdr2Lighting() : m_notSetup( true )
{

}

Rdr2Lighting::~Rdr2Lighting()
{
}

void Rdr2Lighting::SetupPointLights(const Rdr2PointLight *RESTRICT lightArray)
{
	Vec4f		pointLight[7];


	if ( m_notSetup )
	{
		m_PointLightOneOverRangeSq	= grcEffect::LookupGlobalVar("PointLightOneOverRangeSq", false);
		m_PointLightX		= grcEffect::LookupGlobalVar("PointLightX", false);
		m_PointLightY		= grcEffect::LookupGlobalVar("PointLightY", false);
		m_PointLightZ		= grcEffect::LookupGlobalVar("PointLightZ", false);
		m_PointLightR		= grcEffect::LookupGlobalVar("PointLightR", false);
		m_PointLightG		= grcEffect::LookupGlobalVar("PointLightG", false);
		m_PointLightB		= grcEffect::LookupGlobalVar("PointLightB", false);

		m_DirectionalLightX		= grcEffect::LookupGlobalVar("DirectionalLightX", false);
		m_DirectionalLightY		= grcEffect::LookupGlobalVar("DirectionalLightY", false);
		m_DirectionalLightZ		= grcEffect::LookupGlobalVar("DirectionalLightZ", false);
		m_DirectionalLightR		= grcEffect::LookupGlobalVar("DirectionalLightR", false);
		m_DirectionalLightG		= grcEffect::LookupGlobalVar("DirectionalLightG", false);
		m_DirectionalLightB		= grcEffect::LookupGlobalVar("DirectionalLightB", false);

		m_PrimaryDirectionalDir = grcEffect::LookupGlobalVar("PrimaryDirectionalDir", false);
		m_PrimaryDirectionalColor = grcEffect::LookupGlobalVar("PrimaryDirectionalColor", false);
		m_notSetup = false;
	}
	pointLight[0] = Vec4f( lightArray[0].m_ColorAndRange.w, lightArray[1].m_ColorAndRange.w, lightArray[2].m_ColorAndRange.w, lightArray[3].m_ColorAndRange.w);
	pointLight[0] *= pointLight[0];
	pointLight[0].InvertSafe();

	pointLight[1] = Vec4f(lightArray[0].m_Position.x, lightArray[1].m_Position.x, lightArray[2].m_Position.x, lightArray[3].m_Position.x);
	pointLight[2] = Vec4f(lightArray[0].m_Position.y, lightArray[1].m_Position.y, lightArray[2].m_Position.y, lightArray[3].m_Position.y);
	pointLight[3] = Vec4f(lightArray[0].m_Position.z, lightArray[1].m_Position.z, lightArray[2].m_Position.z, lightArray[3].m_Position.z);
	pointLight[4] = Vec4f(lightArray[0].m_ColorAndRange.x, lightArray[1].m_ColorAndRange.x, lightArray[2].m_ColorAndRange.x, lightArray[3].m_ColorAndRange.x);
	pointLight[5] = Vec4f(lightArray[0].m_ColorAndRange.y, lightArray[1].m_ColorAndRange.y, lightArray[2].m_ColorAndRange.y, lightArray[3].m_ColorAndRange.y);
	pointLight[6] = Vec4f(lightArray[0].m_ColorAndRange.z, lightArray[1].m_ColorAndRange.z, lightArray[2].m_ColorAndRange.z, lightArray[3].m_ColorAndRange.z);




	grcEffect::SetGlobalVar(m_PointLightOneOverRangeSq, pointLight[0]);
	grcEffect::SetGlobalVar(m_PointLightX, pointLight[1]);
	grcEffect::SetGlobalVar(m_PointLightY, pointLight[2]);
	grcEffect::SetGlobalVar(m_PointLightZ, pointLight[3]);
	grcEffect::SetGlobalVar(m_PointLightR, pointLight[4]);
	grcEffect::SetGlobalVar(m_PointLightG, pointLight[5]);
	grcEffect::SetGlobalVar(m_PointLightB, pointLight[6]);
}

void Rdr2Lighting::SetupDirectionalLights(Vector3 *directions, Vector4 *colorsAndIntensity)
{
	Vector4		dirR, dirG, dirB;
	Vector4		dirX, dirY, dirZ;

	Vector4		colors[4];

	for (int x=0; x<4; x++)
	{
		Vector4 intensity = colorsAndIntensity[x];
		intensity.SplatW();

		colors[x] = colorsAndIntensity[x] * intensity;
	}

	dirR = Vector4(colors[0].x, colors[1].x, colors[2].x, colors[3].x);
	dirG = Vector4(colors[0].y, colors[1].y, colors[2].y, colors[3].y);
	dirB = Vector4(colors[0].z, colors[1].z, colors[2].z, colors[3].z);

	dirX = Vector4(directions[0].x, directions[1].x, directions[2].x, directions[3].x);
	dirY = Vector4(directions[0].y, directions[1].y, directions[2].y, directions[3].y);
	dirZ = Vector4(directions[0].z, directions[1].z, directions[2].z, directions[3].z);

	grcEffect::SetGlobalVar(m_DirectionalLightX, dirX);
	grcEffect::SetGlobalVar(m_DirectionalLightY, dirY);
	grcEffect::SetGlobalVar(m_DirectionalLightZ, dirZ);
	grcEffect::SetGlobalVar(m_DirectionalLightR, dirR);
	grcEffect::SetGlobalVar(m_DirectionalLightG, dirG);
	grcEffect::SetGlobalVar(m_DirectionalLightB, dirB);

	// Set up the primary light.
	grcEffect::SetGlobalVar(m_PrimaryDirectionalDir, directions[0]);
	colors[0].w = 0.0f;
	grcEffect::SetGlobalVar(m_PrimaryDirectionalColor, colors[0]);
}

void Rdr2Lighting::SetLightingGroup(const grcLightGroup &grp)
{
	grp.Validate();

	Vector3 dirLightDirs[DIR_LIGHT_COUNT];
	Vector4 dirLightColors[DIR_LIGHT_COUNT];
	Rdr2PointLight ptLights[POINT_LIGHT_COUNT];

	int dirLightCount = 0;
	int ptLightCount = 0;


	for (int i = 0; i < grp.GetActiveCount(); ++i)
	{
		if ( grcState::GetLightingMode() == grclmPoint && grp.GetLightType(i) == grcLightGroup::LTTYPE_POINT )
		{
			// It's a point light.
			Assert(ptLightCount < POINT_LIGHT_COUNT);
			ptLights[ptLightCount].m_Position = grp.GetPosition(i);

			// Get 1/rangeSq
			float falloff = grp.GetFalloff(i);
			falloff *= falloff;
			falloff = 1.0f / falloff;

			Vector3 color = grp.GetColor(i);

			ptLights[ptLightCount].m_ColorAndRange = Vector4(color.x, color.y, color.z, falloff);

			ptLightCount++;
		}
		else
		{
			// It's a directional light.
			Assert(dirLightCount < DIR_LIGHT_COUNT);

			dirLightDirs[dirLightCount] = grp.GetDirection(i);

			Vector4 colorAndIntensity;

			colorAndIntensity.SetVector3(grp.GetColor(i));
			colorAndIntensity.w = grp.GetIntensity(i);

			dirLightColors[dirLightCount++] = colorAndIntensity;
		}

		// Handle special lighting settings
		//		if ( grp.IsNegative(i) ) {
		//			lightColor[i].Multiply(Vector4(-1,-1,-1,1));
		//		}
		//		if ( grp.IsOversaturate(i) ) {
		//			lightColor[i].w *= -1;
		//		}
	}

	// Fill unused lights
	for (int x=dirLightCount; x<DIR_LIGHT_COUNT; x++)
	{
		dirLightColors[x] = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
		dirLightDirs[x] = Vector3(0.0f, 0.0f, 0.0f);
	}

	for (int x=ptLightCount; x<POINT_LIGHT_COUNT; x++)
	{
		ptLights[x].m_Position = Vector3(0.0f, 0.0f, 0.0f);
		ptLights[x].m_ColorAndRange = Vector4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	SetupPointLights(ptLights);
	SetupDirectionalLights(dirLightDirs, dirLightColors);

	//grcEffect::SetGlobalVar(s_gvLightAmbient,grp.GetAmbient());

	grcState::SetLightingGroup(grp);
}

#endif	// 0
