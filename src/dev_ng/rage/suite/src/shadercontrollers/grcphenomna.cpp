
#include "grcphenomna.h"

#include "sphericallight.h"

#include "atl/functor.h"
#include "grprofile/pix.h"
#include "grmodel/model.h"
// #include "grpostfx/postfx.h"

#include "grcphenomna_parser.h"



using namespace rage;


namespace rage
{

inline float frac( float v )
{
	return v - floor( v );
}
float GetNightFadeOut( float timeOfDay )
{
    // adickinson: time is 24hrs/day, offset by 6hrs to get sunset/sunrise when we want.
    // This has to be sync'd with rage_atmoscatt_clouds.fx and rage_atmoscatt.fx and rage_atmoscatt_terrain.fx
    float angle = 2.0f *PI * (timeOfDay/24.0f + 0.25f);

    // adickinson: seems like we're opposite of the sky, so 1.0f -  clamp...
    //return Clamp( 1.0f + sin( angle ) * 6.0f, 0.0f, 1.0f );
    return 1.0f - Clamp( 1.0f + sin( angle ) * 6.0f, 0.0f, 1.0f );
}

Vector3 ConvertTimeOfDayToVector3( float timeOfDay )
{

	Vector3	sundir;

    // adickinson: time is 24hrs/day, offset by 6hrs to get sunset/sunrise when we want.
    // This has to be sync'd with rage_atmoscatt_clouds.fx and rage_atmoscatt.fx and rage_atmoscatt_terrain.fx
	float angle = 2.0f *PI * (timeOfDay/24.0f + 0.25f);
	sundir.x = cos( angle);
	sundir.y = sin(angle);
	sundir.z = 0.0f;

   // if ( sundir.y < 0.0f )
	//{
	//	sundir.Lerp(  GetNightFadeOut( timeOfDay ),  Vector3( -0.707f, 0.707f, 0.0f ), sundir );
	//}

	return sundir;
}


Vector4	ConvertColor( Color32 col , float alpha )
{
	return Vector4( col.GetRedf(), col.GetGreenf(), col.GetBluef() , alpha);
}
Vector3	ConvertColor3( Color32 col )
{
	return Vector3( col.GetRedf(), col.GetGreenf(), col.GetBluef() );
}
void FogControl::Init()
{
	m_noiseVolumeTexture = 0;
	//m_noiseVolumeTexture 	= grcTextureFactory::GetInstance().Create( "NoiseVolume.dds");
}
void FogControl::SetVolumeTexture( const char* volume )
{
	m_noiseVolumeTexture 	= grcTextureFactory::GetInstance().Create( volume );
}

void FogControl::Set( grmShader& effect, FogControlVarCache* vars ) const
{

	effect.SetVar( vars->m_FogDensityVar, m_density );

	Vector4	height( m_maxHeight, m_minHeight, 0.0f, m_fuzzHeight );
	height.z = 1.0f / height.w;
	effect.SetVar( vars->m_FogHieghtVar, height );
	effect.SetVar( vars->m_FogColorVar, ConvertColor( m_color, 1.0f ) );

	float time = m_timer.GetTime() * 0.01f;
	Vector3	offset( time * m_speed, cos( time * m_speed ) * 0.5f + 0.5f ,  time * m_speed * 0.2f);
	effect.SetVar( vars->m_FogOffsetVar, offset );
	effect.SetVar( vars->m_NoiseVolumeTextureVar, m_noiseVolumeTexture );

	Vector4 noise( m_noiseTile, m_noiseBias, m_noiseScale, m_noiseTileY );
	effect.SetVar( vars->m_FogNoiseVar, noise );
	effect.SetVar( vars->m_volumeFogOnOnVar, m_on );
}


void WaterFogControl::Set( grmShader& effect , WaterFogControlVarCache* vars ) const
{
	effect.SetVar( vars->m_WaterFogDensityVar, m_density );
	effect.SetVar( vars->m_WaterFogHieghtVar, m_height );
	Vector3 col = ConvertColor3( m_color) ;
	col *= Vector3( m_IncidentColor.x,  m_IncidentColor.y,  m_IncidentColor.z);
	effect.SetVar( vars->m_WaterFogColorVar, col);
	effect.SetVar( vars->m_waterFogOnVar, m_on );
}

//#############################################################################


void EnviromentDomeLighting::CreateSkyMap( grcTexture* tex, SphericalLight3rdOrder& env )
{
	static const int	NumberOfSamplesSqrt = 16;
	bool succeded = false;
	EnvironmentSkyMapTexture		dome( tex, 	ConvertColor3( m_ground) ,  m_intensity ,NumberOfSamplesSqrt, succeded );

	if ( succeded )
	{
		env.AddSkyHat( NumberOfSamplesSqrt, dome );
	}
	else
	{
		Create( env );
	}
}
void EnviromentDomeLighting::Create( SphericalLight3rdOrder& env )
{
	EnvironmentLightDome		dome( m_sky * m_intensity, 
									  m_horizon  * m_intensity,
									  m_ground * m_intensity );
	static const int	NumberOfSamplesSqrt = 8;

	
	SphericalLight3rdOrder::SampleFunctor		func = MakeFunctorRet( dome, &rage::EnvironmentLightDome::Sample );
	env.AddEnvironmentMap(NumberOfSamplesSqrt,  func );

	// now check to and TOD lights

}

void EnviromentDomeLighting::UpdateSH( grcTexture* tex )
{
	m_env.Reset();
	if (  tex == 0 || tex == grcTexture::None )
	{
		Create( m_env);
	}
	else
	{
		CreateSkyMap( tex, m_env );
	}
}
void EnviromentDomeLighting::Set( grmShader& effect, EnviromentDomeVarCache* vars )
{
	effect.SetVar( vars->m_sphLightCoefficientsVar, m_env.GetCoeffs() , m_env.NumberCoefficients() );
	effect.SetVar( vars->m_sphericalAmbientOnVar, m_on );
}

//#############################################################################

void VerletWaterSimulation::SetDampeningTex( const char *pDampeningTexFile )
{
    m_pDampeningTex	= grcTextureFactory::GetInstance().Create( pDampeningTexFile );
}

//#############################################################################

void VerletWaterPerturbation::SetPerturbationTex( const char *pPerturbationTexFile )
{
    m_pPerturbationTex = grcTextureFactory::GetInstance().Create( pPerturbationTexFile );
}

} // namespace rage
