


#include "shaderfragmentcontrol.h"

#include "rmcore\drawable.h"

#include "grcore/image.h"
#include "grcore/texture.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"

#include "shaderfragmentcontrol_parser.h"

using namespace rage;
   
namespace rage
{

bool ShaderFragment::LoadFile( const char *pFilename )
{
    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, pFilename );

    return PARSER.LoadObject( pFilename, "xml", *this );
}

bool ShaderFragment::LoadTreeNode( const parTreeNode *pTreeNode )
{
	return PARSER.LoadFromStructure( const_cast<parTreeNode *>(pTreeNode), *parser_GetStructure(), this );
}

#if __BANK
bool ShaderFragment::SaveFile( const char *pFilename )
{
    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, pFilename );

    return PARSER.SaveObject( pFilename, "xml", this, parManager::XML );
}

bool ShaderFragment::SaveTreeNode( parTreeNode *pParentNode )
{
    parTreeNode *pNode = PARSER.BuildTreeNode( this );
    if ( pNode )
    {
        pNode->AppendAsChildOf( pParentNode );
        return true;
    }

    return false;
}



void ShaderFragment::BankLoadFile()
{
    char path[256];
    memset(path, 0, 256);

    if ( BANKMGR.OpenFile(path, 256, "*.xml", false, "Shader Fragment Files (*.xml)") )
    {
        LoadFile( path );
    }
}

void ShaderFragment::BankSaveFile()
{
    char path[256];
    memset(path, 0, 256);

    if ( BANKMGR.OpenFile(path, 256, "*.xml", true, "Shader Fragment Files (*.xml)") )
    {
        SaveFile( path );
    }
}

void ShaderFragment::AddWidgets( bkBank *pBank )
{
    PARSER.AddWidgets( *pBank, this );

    pBank->AddButton("Save", datCallback(MFA(ShaderFragment::BankSaveFile),this) );
    pBank->AddButton("Load", datCallback(MFA(ShaderFragment::BankLoadFile),this) );
}
#endif // __BANK




//#############################################################################
//#############################################################################
//#############################################################################

bool ShaderFragmentControl::LoadFiles( const char *pFilename )
{
    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, pFilename );

    parTree *pTree = PARSER.LoadTree( path, "xml" );
    if ( pTree == NULL )
    {
        Printf( "Shader Controller load tree failed: %s\n", pFilename );
        return false;
    }

    parTreeNode *pRootNode = pTree->GetRoot();
    if ( strcmp(pRootNode->GetElement().GetName(),"shader_fragments") != 0 )
    {
        Printf( "Invalid Shader Controller file: %s\n", pFilename );
		delete pTree;
        return false;
    }    

    bool succeeded = LoadTreeNodes( pRootNode );
	delete pTree;
	return succeeded;
}

#if __BANK
bool ShaderFragmentControl::SaveFiles( const char *pFilename )
{
    parTree tree;
    parTreeNode *pRootNode = tree.CreateRoot();
    pRootNode->GetElement().SetName( "shader_fragments" );

    if ( !SaveTreeNodes(pRootNode) )
    {
        return false;
    }

    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, pFilename );
    if ( !PARSER.SaveTree( path, "xml", &tree, parManager::XML ) )
    {
        Printf( "Shader Controllers save tree error\n" );
        return false;
    }

    return true;
}
#endif

bool ShaderFragmentControl::LoadTreeNodes( parTreeNode *pRootNode )
{
    if ( strcmp(pRootNode->GetElement().GetName(),"shader_fragments") != 0 )
    {
        Printf( "Shader Controller load tree nodes error: %s is not a shader_fragment tree node\n", pRootNode->GetElement().GetName() );
        return false;
    }

    int count = 0;
    for ( ShaderFragmentList::iterator itor = m_store.begin();
        itor != m_store.end();
        ++itor )
    {
		u32 nameHash = itor->second->parser_GetStructure()->GetNameHash();
		parTreeNode* pNode = NULL;
		for(parTreeNode::ChildNodeIterator kid = pRootNode->BeginChildren(); kid != pRootNode->EndChildren(); ++kid)
		{
			if (atLiteralStringHash((*kid)->GetElement().GetName()) == nameHash)
			{
				pNode = *kid;
				break;
			}
		}
        if ( pNode )
        {
            parAttribute *pAtt = pNode->GetElement().FindAttribute( "fileRef" );
            if ( pAtt )
            {
                if ( !itor->second->LoadFile( pAtt->GetStringValue() ) )
                {
                    Printf( "Shader Fragment load error on %s\n", pAtt->GetStringValue() );
                }
                else
                {
                    Printf( "Loaded shader fragment file %s\n", pAtt->GetStringValue() );
                    ++count;
                }
            }
            else
            {                
                if ( !itor->second->LoadTreeNode(pNode) )
                {
                    Printf( "Shader Fragment load tree node error on %s\n", itor->second->parser_GetStructure()->GetName() );
                }
                else
                {
                    Printf( "Loaded shader fragment %s\n", itor->second->parser_GetStructure()->GetName() );
                    ++count;
                }
            }
        }
    }

    Printf( "\tLoaded %d shader fragments\n", count );

    return (count > 0);
}

#if __BANK
bool ShaderFragmentControl::SaveTreeNodes( parTreeNode *pRootNode )
{
    if ( strcmp(pRootNode->GetElement().GetName(),"shader_fragments") != 0 )
    {
        Printf( "Shader Controller save tree nodes error: %s is not a shader_fragment tree node\n", pRootNode->GetElement().GetName() );
        return false;
    }

    for ( ShaderFragmentList::iterator itor = m_store.begin();
        itor != m_store.end();
        ++itor )
    {
        if ( !itor->second->SaveTreeNode(pRootNode) )
        {
            Printf( "Shader Fragment save tree node error on %s\n", (const char *)itor->first );
        }
    }

    return true;
}


void ShaderFragmentControl::BankLoadFiles()
{
    char path[256];
    memset(path, 0, 256);

    if ( BANKMGR.OpenFile(path, 256, "*.xml", false, "Shader Controller Files (*.xml)") )
    {
        LoadFiles( path );
    }
}

void ShaderFragmentControl::BankSaveFiles()
{
    char path[256];
    memset(path, 0, 256);

    if ( BANKMGR.OpenFile(path, 256, "*.xml", true, "Shader Controller Files (*.xml)") )
    {
        SaveFiles( path );
    }
}

// add in shader controls
void ShaderFragmentControl::AddWidgets( bkBank *pBank )
{
    pBank->AddButton( "Save All", datCallback(MFA(ShaderFragmentControl::BankSaveFiles),this) );
    pBank->AddButton( "Load All", datCallback(MFA(ShaderFragmentControl::BankLoadFiles),this) );

    for ( ShaderFragmentList::iterator itor = m_store.begin();
        itor != m_store.end();
        ++itor )
    {

        pBank->PushGroup( itor->first );
        {
            itor->second->AddWidgets( pBank );
        }
        pBank->PopGroup();
    }
}

#endif // __BANK

} // namespace rage
