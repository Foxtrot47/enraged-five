<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::SkyDomeProceduralControl::SkyStruct">
	<Vector3 name="m_gNightSkyColor" type="color"/>
	<Vector3 name="m_gDaySkyColor" type="color"/>
	<Vector3 name="m_gSunsetColor" type="color"/>
	<float name="m_AzimuthHeight" min="0.0" max="24.0" step="0.01f"/>

	<float name="m_SunCentreStart" min="0.0" max="1.0" step="0.01f"/>
	<float name="m_SunCentreEnd" min="0.0" max="1.0" step="0.01f"/>
	<float name="m_SunCentreIntensity" min="0.0" max="1.0" step="0.01f"/>
	<float name="m_SunOuterSize" init="0.0f" min="-1.0" max="4.0" step="0.01f"/>
</structdef>

<structdef type="rage::SkyDomeProceduralControl::TopStruct">
<!--- top layer -->
	<float name="m_TopCloudBias" min="0.0" max="2.0" step="0.02f"/>
	<float name="m_TopCloudDetail" min="0.0" max="10.0" step="0.05f"/>
	<float name="m_TopCloudThreshold" min="-10.0" max="10.0" step="0.02f"/>
	<float name="m_TopCloudHeight" min="0.0" max="10.0" step="0.05f"/>
</structdef>
<structdef type="rage::SkyDomeProceduralControl::CloudStruct">
<!-- clouds -->
	<Vector3 name="m_gCloudColor" type="color"/>
	<Vector3 name="m_NightBounceColor" type="color"/>
	<float name="m_gCloudShadowStrength" min="0.0" max="1.0" step="0.025f"/>
	<float name="m_CloudThreshold" min="-10.0" max="10.0" step="0.05f"/>
	<float name="m_CloudBias" min="0.0" max="2.0" step="0.05f"/>
	<float name="m_CloudShadowOffset" min="0.0" max="1.0" step="0.01f"/>
	<float name="m_CloudInscatteringRange" min="0.01" max="1.5" step="0.05f"/>
	<float name="m_CloudEdgeSmooth" min="0.0" max="1.0" step="0.05f"/>
	<float name="m_CloudThickness" min="0.0" max="1.0" step="0.05f"/>
	<!-- detail layer -->
	<float name="m_DetailScale" min="0.0" max="768.0" step="0.1f"/>
	<float name="m_DetailStrength" min="0.0" max="1.0" step="0.05f"/>
	<float name="m_CloudSpeed" min="0.0" max="30.0" step="0.01f"/>	
	<float name="m_CloudWarp" min="0.0" max="2.0" step="0.01f"/>	
	<float name="m_CloudFadeOut" min="0.0" max="1.0f" step="0.01f"/>
	<struct name="top" type="rage::SkyDomeProceduralControl::TopStruct"/>
</structdef>
<structdef type="rage::SkyDomeProceduralControl::NightStruct">
	<float name="m_StarBlinkFrequency" min="0.0f" max="20000.0f" step="0.5f"/>
	<float name="m_StarBlinkIntensity" min="0.0f" max="0.25f" step="0.0001f"/>
	<float name="m_StarIntensity" min="0.0f" max="5.0f" step="0.01f"/>
	<float name="m_StarFieldThreshold" min="0.0f" max="1.0f" step="0.001f"/>
	<!--- moon -->
	<float name="m_MoonBrightness" min="0.0" max="70.0" step="0.01f"/>
	<float name="m_MoonGlow" min="0.0" max="5.0" step="0.01f"/>
	<float name="m_MoonVisiblity" min="0.0" max="5.0" step="0.01f"/>
	<Vector3 name="m_MoonColor" type="color"/>
</structdef>

<structdef type="rage::SkyDomeProceduralControl::LightStruct">
	<float name="m_LightningPulseFrequency" min="0.0f" max="1000.0f" step="0.5f"/>
	<float name="m_LightningPulseIntensity" min="0.0f" max="10.0f" step="0.1f"/>
</structdef>

<structdef type="rage::SkyDomeProceduralControl" autoregister="true">
	<struct name="sky" type="rage::SkyDomeProceduralControl::SkyStruct"/>
	<struct name="cloud" type="rage::SkyDomeProceduralControl::CloudStruct"/>
	<struct name="night" type="rage::SkyDomeProceduralControl::NightStruct"/>
	<struct name="lightning" type="rage::SkyDomeProceduralControl::LightStruct"/>
</structdef>

<structdef type="rage::SkyhatMiniNoise" base="rage::ShaderFragment" autoregister="true">
<float name="m_HDRExposure" min="0.0f" max="20.0f" step="0.05f"/>
<float name="m_HDRSunExposure" min="0.0f" max="20.0f" step="0.05f"/>
<Vector3 name="m_HDRExposureClamp" min="0.0f" max="20.0f" step="0.05f"/>
<float name="m_StarFieldRepeat" min="0.0" max="30.0" step="0.05f"/>
<float name="m_MoonSize" min="0.0" max="16.0" step="0.01f"/>
<int name="m_MoonPhase" min="0.0" max="24" step="0.05f"/>
<Vector3 name="m_MoonPosition" min="-1.0f" max="1.0f" step="0.05f"/>
<Vector2 name="m_GalaxyOffset" min="-2.0f" max="2.0f" step="0.01f"/>
<float name="m_detailSpeed" min="0.0f" max="0.1f" step="0.0001f"/>
<float name="m_sunSetBump" min="0.0f" max="2.0f" step="0.01f"/>
</structdef>

<structdef type="rage::SkyLightController" base="rage::ShaderFragment" autoregister="true">
<Vector3 name="m_GroundColor" type="color"/>
<float name="m_intensity" init="1.0f" min="0.0f" max="1.0f"/>
<float name="m_bounceIntensity" init="1.0f" min="0.0f" max="1.0f"/>
</structdef>

</ParserSchema>