// 
// shadercontrollers/grcproceduralphenomna.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#if 0

#include "grcproceduralphenomna.h"

#include "grcphenomna.h"
#include "shaderfragmentcontrol.h"

#include "atl/array.h"
#include "grcore/quads.h"
#include "grmodel/shader.h"
#include "parser/manager.h"

#include "grcproceduralphenomna_parser.h"

#if __XENON
#include "grcore/texturexenon.h"
#endif // __XNEON

#include "grprofile/pix.h"


// embedded shaders
#if __XENON
#include "embedded_rage_atmoscatt_clouds_fxl_final.h"
#include "embedded_rage_perlinnoise_fxl_final.h"
#elif __PPU
#include "embedded_rage_atmoscatt_clouds_psn.h"
#include "embedded_rage_perlinnoise_psn.h"
#elif __WIN32PC
#include "embedded_rage_atmoscatt_clouds_win32_30.h"
#include "embedded_rage_perlinnoise_win32_30.h"
#endif


namespace rage
{

//#############################################################################


ProceduralTextureVerletWater::ProceduralTextureVerletWater()
: m_timeOfDay(0.0f)
, m_iCurrentVerletSimRT(E_VERLET_SIM_RT_1)
, m_perturbShowSim(false)
, m_perturbQuadScale(0.05f)
, m_perturbRotAngle(0.0f)
, m_perturbTexScale(0.9f)
, m_pVerletWaterSimulation(NULL)
, m_pVerletWaterPerturbation(NULL)
, m_initClientFunctor(InitClientFunctor::NullFunctor())
, m_drawClientFunctor(DrawClientFunctor::NullFunctor())
{
    m_perturbQuadVerts[0].Set( 0.0f, 0.0f );
    m_perturbQuadVerts[1].Set( 0.0f, m_perturbQuadScale );
    m_perturbQuadVerts[2].Set( m_perturbQuadScale, 0.0f );
    m_perturbQuadVerts[3].Set( m_perturbQuadScale, m_perturbQuadScale );

    m_perturbQuadOffset.Set( 0.5f, 0.5f );

    m_mirrorPlane.Set( 0.0f, 1.0f, 0.0f, -15.0f );
    m_farClip = 100000.0f;
}

ProceduralTextureVerletWater::~ProceduralTextureVerletWater()
{
    if ( m_pVerletWaterSimulation )
    {
        delete m_pVerletWaterSimulation;
        m_pVerletWaterSimulation = NULL;
    }

    if ( m_pVerletWaterPerturbation )
    {
        delete m_pVerletWaterPerturbation;
        m_pVerletWaterPerturbation = NULL;
    }
}

void ProceduralTextureVerletWater::SetInitClientFunctor( InitClientFunctor functor )
{
    m_initClientFunctor = functor;
}

void ProceduralTextureVerletWater::SetDrawClientFunctor( DrawClientFunctor functor )
{
    m_drawClientFunctor = functor;
}

void ProceduralTextureVerletWater::SetMirrorPlane( const Vector4 &mirrorPlane, float farClip )
{
    m_mirrorPlane.Set( mirrorPlane );
    m_farClip = farClip;
}

void ProceduralTextureVerletWater::OnPostLoadDerived()
{
    // create simulation ShaderFragment
    m_pVerletWaterSimulation = rage_new VerletWaterSimulation;
    m_pVerletWaterSimulation->SetDampeningTex( m_shaderDefs[E_VERLET_SIM_SHADER]->GetTextureArray()[0].c_str() );
    int index = ((m_iCurrentVerletSimRT - E_VERLET_SIM_RT_1 + 1) % TOT_VERLETSIM_RTS) + E_VERLET_SIM_RT_1;
    m_pVerletWaterSimulation->SetSimRT( m_renderTargets[index] );
    index = ((m_iCurrentVerletSimRT - E_VERLET_SIM_RT_1 + 2) % TOT_VERLETSIM_RTS) + E_VERLET_SIM_RT_1;
    m_pVerletWaterSimulation->SetPrevSimRT( m_renderTargets[index] );
    ShaderController::GetInstance().Register( "VerletWaterSimulation", m_pVerletWaterSimulation );
    ShaderController::GetInstance().AddShader( m_shaders[E_VERLET_SIM_SHADER] );

    // create perturbation ShaderFragment
    m_pVerletWaterPerturbation = rage_new VerletWaterPerturbation;
    m_pVerletWaterPerturbation->SetPerturbationTex( m_shaderDefs[E_PERTURBATION_SHADER]->GetTextureArray()[0].c_str() );
    ShaderController::GetInstance().Register( "VerletWaterPerturbation", m_pVerletWaterPerturbation );    
    ShaderController::GetInstance().AddShader( m_shaders[E_PERTURBATION_SHADER] );
    
    // create surface ShaderFragment
    m_pVerletWaterSurface = rage_new VerletWaterSurface;
    m_pVerletWaterSurface->SetRefractRT( m_renderTargets[E_REFRACT_RT] );
    m_pVerletWaterSurface->SetSimRT( m_renderTargets[m_iCurrentVerletSimRT] );
    ShaderController::GetInstance().Register( "VerletWaterSurface", m_pVerletWaterSurface );
    // no shader to register the shader because this is on our water entity
}

bool ProceduralTextureVerletWater::Load()
{
    if ( m_initClientFunctor != InitClientFunctor::NullFunctor() )
    {
        m_initClientFunctor( this );
    }
    
    return ProceduralTexture::Load();
}

void ProceduralTextureVerletWater::Update()
{
    // update water surface
    m_pVerletWaterSurface->SetTimeOfDay( m_timeOfDay );
    m_pVerletWaterSurface->SetSimRT( m_renderTargets[m_iCurrentVerletSimRT] );

#ifdef SHOW_VERLETSIM
    if ( m_perturbShowSim )
    {
        m_pVerletWaterSurface->SetRefractRT( m_renderTargets[m_iCurrentVerletSimRT] );
    }
#endif

    // update simulation
    int index = ((m_iCurrentVerletSimRT - E_VERLET_SIM_RT_1 + 1) % TOT_VERLETSIM_RTS) + E_VERLET_SIM_RT_1;
    m_pVerletWaterSimulation->SetSimRT( m_renderTargets[index] );
    
    index = ((m_iCurrentVerletSimRT - E_VERLET_SIM_RT_1 + 2) % TOT_VERLETSIM_RTS) + E_VERLET_SIM_RT_1;
    m_pVerletWaterSimulation->SetPrevSimRT( m_renderTargets[index] );
}

Matrix44 CreateMirrorMatrix44( const Vector4& P )
{
    Matrix44 res;
    res.a = Vector4(	-2.0f * P.x * P.x + 1.0f,	-2.0f * P.x * P.y,			-2.0f * P.x * P.z, 0.0f);
    res.b = Vector4(	-2.0f * P.y * P.x  ,		-2.0f * P.y * P.y + 1.0f,	-2.0f * P.y * P.z, 0.0f	);
    res.c = Vector4(	-2.0f * P.z * P.x ,			-2.0f * P.z * P.y,			-2.0f * P.z * P.z + 1.0f , 0.0f );
    res.d =  Vector4(	-2.0f * P.x * P.w,			-2.0f * P.y * P.w,			-2.0f * P.z * P.w , 1.0f );
    return res;
}

Vector4 TransformPlane( const Matrix44& trans, const Vector4& plane )
{
    Matrix44	transInverse;
    transInverse.Inverse( trans);

    Vector3		vPointOnPlane = Vector3( plane.x * -plane.w, plane.y * -plane.w, plane.z * -plane.w );
    transInverse.Transform( vPointOnPlane, vPointOnPlane);

    Vector3		vTransformedNormal( plane.x, plane.y, plane.z );
    transInverse.Transform3x3( vTransformedNormal, vTransformedNormal);
    vTransformedNormal.Normalize();

    Vector4 result;

    result.ComputePlane( vPointOnPlane, vTransformedNormal );

    return result;
}



float sgn( float v )
{
    return v > 0.0f ?  1.0f : -1.0f;
}


void ProceduralTextureVerletWater::Draw()
{
    DrawReflection();
    return;
    //testReflectionMatrix();
#if 0

    // temporary removed
    grcTextureFactory::GetInstance().LockRenderTarget(0, m_renderTargets[m_iCurrentVerletSimRT], NULL);

    // update simulation
    grmModel::SetForceShader( m_shaders[E_VERLET_SIM_SHADER] );	// simulation shader
    m_shaders[E_VERLET_SIM_SHADER]->BeginDraw(grmShader::RMC_DRAW, true);
    m_shaders[E_VERLET_SIM_SHADER]->Bind(0);
    grcBegin(drawTris, 6);
    grcVertex2f(0,0);
    grcVertex2f(1,1);
    grcVertex2f(0,1);
    grcVertex2f(0,0);
    grcVertex2f(1,0);
    grcVertex2f(1,1);
    grcEnd();
    grmModel::GetForceShader()->UnBind();
    grmModel::GetForceShader()->EndDraw();
    grmModel::SetForceShader(0);

    // render perturbation
    grmModel::SetForceShader( m_shaders[E_PERTURBATION_SHADER] );	// perturbation shader
    m_shaders[E_PERTURBATION_SHADER]->BeginDraw(grmShader::RMC_DRAW, true);
    m_shaders[E_PERTURBATION_SHADER]->Bind(0);

    // rotate
    m_perturbRotAngle += 0.00001f;
    float cos = cosf(m_perturbRotAngle);
    float sin = sinf(m_perturbRotAngle);

#define ROTATEXY(v) { float t = v.x * cos - v.y * sin; v.y = v.y * cos + v.x * sin; v.x = t; }

    ROTATEXY(m_perturbQuadVerts[1]);
    ROTATEXY(m_perturbQuadVerts[2]);
    ROTATEXY(m_perturbQuadVerts[3]);


    Vector2 p2[4];
    p2[0].x = m_perturbQuadVerts[0].y; p2[0].y = m_perturbQuadVerts[0].x;
    p2[1].x = m_perturbQuadVerts[1].y; p2[1].y = m_perturbQuadVerts[1].x;
    p2[2].x = m_perturbQuadVerts[2].y; p2[2].y = m_perturbQuadVerts[2].x;
    p2[3].x = m_perturbQuadVerts[3].y; p2[3].y = m_perturbQuadVerts[3].x;

    grcBegin(drawTris, 6);
    grcTexCoord2f(0.0f,  0.0f);
    grcVertex2f(p2[0]+m_perturbQuadOffset);
    grcTexCoord2f(0.0f,  m_perturbTexScale);
    grcVertex2f(p2[1]+m_perturbQuadOffset);
    grcTexCoord2f(m_perturbTexScale,  m_perturbTexScale);
    grcVertex2f(p2[3]+m_perturbQuadOffset);
    grcTexCoord2f(m_perturbTexScale,  m_perturbTexScale);
    grcVertex2f(p2[3]+m_perturbQuadOffset);
    grcTexCoord2f(m_perturbTexScale,  0.0f);
    grcVertex2f(p2[2]+m_perturbQuadOffset);
    grcTexCoord2f(0,  0);
    grcVertex2f(p2[0]+m_perturbQuadOffset);
    grcEnd();

    grmModel::GetForceShader()->UnBind();
    grmModel::GetForceShader()->EndDraw();
    grmModel::SetForceShader(0);

    grcTextureFactory::GetInstance().UnlockRenderTarget(0);

    // rotate simulation buffer
    --m_iCurrentVerletSimRT;
    if ( m_iCurrentVerletSimRT < E_VERLET_SIM_RT_1 )
    {
        m_iCurrentVerletSimRT = E_VERLET_SIM_RT_3;
    }
#endif
}

void ProceduralTextureVerletWater::DrawReflection()
{
    // render refracted objects onto refraction map

    PIXBegin(0, "Reflection" );

    // For reflection need to flip the viewport
    Matrix34   WorldToReflectionMtx;
	WorldToReflectionMtx.MirrorOnPlane( m_mirrorPlane );

    //grcState::SetCullMode( grccmBack );

    grcViewport* oldViewport = grcViewport::GetCurrent();
    grcViewport viewport = *oldViewport;

    Matrix34 WorldMtx = RCC_MATRIX34(oldViewport->GetCameraMtx());
    WorldMtx.Dot( WorldToReflectionMtx );

    viewport.SetCameraMtx(  RCC_MAT34V(WorldMtx) );
    grcViewport::SetCurrent( &viewport );

    viewport.SetFarClip( m_farClip );

    // set up user clip plane
    Vector4 ClipPlane = m_mirrorPlane;
    GRCDEVICE.SetClipPlaneEnable( 1);
    GRCDEVICE.SetClipPlane( 0, VECTOR4_TO_VEC4V(ClipPlane));

    // RAY - temporally changing this to be a reflection map
    grcTextureFactory::GetInstance().LockRenderTarget( 0, m_renderTargets[E_REFRACT_RT], m_renderTargets[E_DEPTH_RT] );
    GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.0f,0.0f), true,1.f,true);

    // this will draw the scene into our mirror
    if ( m_drawClientFunctor != DrawClientFunctor::NullFunctor() )
    {
        m_drawClientFunctor( this, viewport );
    }

    grcTextureFactory::GetInstance().UnlockRenderTarget(0);

    GRCDEVICE.SetClipPlaneEnable( 0);

    PIXEnd();
}	


#if __BANK

void ProceduralTextureVerletWater::AddWidgets( bkBank &bank )
{
    bank.PushGroup( "Verlet Water" );
    {
#ifdef SHOW_VERLETSIM
        bank.AddToggle( "Show Sim", &m_perturbShowSim );
#endif
        PARSER.AddWidgets( bank, this );
    }
    bank.PopGroup();
}

void ProceduralTextureVerletWater::BankPerturbQuadScaleSliderChanged()
{
    m_perturbQuadVerts[0].Set( 0.0f, 0.0f );
    m_perturbQuadVerts[1].Set( 0.0f, m_perturbQuadScale );
    m_perturbQuadVerts[2].Set( m_perturbQuadScale, 0.0f );
    m_perturbQuadVerts[3].Set( m_perturbQuadScale, m_perturbQuadScale );
}

#endif // __BANK

const bool g_UseClipPlanes = true;

//------------------------------------- Reflection Plane ----------------------------------
bool ReflectionPlane::CalculateReflectionPlane(const grcViewport &inViewport, const Matrix34 &inMatrix, grcViewport &outViewport) const
{
	Matrix34	WorldToReflectionMtx;
	Vector4		plane = m_plane;
	Matrix34	WorldMtx = inMatrix;


	WorldToReflectionMtx.MirrorOnPlane( plane );
	WorldMtx.Dot( WorldToReflectionMtx );

	outViewport = inViewport;
	outViewport.SetCameraMtx(WorldMtx);

	if (m_isDoubleSided && !OnFrontSide( outViewport.GetCameraPosition()))
	{
		// flip the normal
		plane.Negate();
	}

	if ( !g_UseClipPlanes )
	{
		outViewport.ApplyObliqueProjection( plane );
	}

	return 	IsVisible(&inViewport);
}

grcViewport*  ReflectionPlane::BeginReflection( grcViewport& clipViewport, grcViewport& viewport )
{
	Matrix34   WorldToReflectionMtx;
	Vector4		plane = m_plane;


	WorldToReflectionMtx.MirrorOnPlane( plane );

	grcViewport* oldViewport = grcViewport::GetCurrent();

	ASSERT_ONLY( Matrix34 ident( Matrix34::IdentityType) );
	AssertMsg( oldViewport->GetWorldMtx().IsClose( ident, 0.0001f ), "Reflections only work with Identity World Matrices" );
	viewport = *oldViewport;

	Matrix34 WorldMtx = oldViewport->GetCameraMtx();
	WorldMtx.Dot( WorldToReflectionMtx );

	viewport.SetCameraMtx(  WorldMtx );
	grcViewport::SetCurrent( &viewport );
	
	grcState::SetCullMode( grccmFront );


	//viewport.SetFarClip( m_farClip );
	clipViewport = viewport;

	if (m_isDoubleSided && !OnFrontSide( grcViewport::GetCurrent()->GetCameraPosition()))
	{
		// flip the normal
		plane.Negate();
	}

	if ( !g_UseClipPlanes )
	{
		clipViewport.ApplyObliqueProjection( plane );
		grcViewport::SetCurrent( &clipViewport );
	}
	else
	{
		// set up user clip plane
		GRCDEVICE.SetClipPlaneEnable( 1);
		//plane.w = -plane.w;

		GRCDEVICE.SetClipPlane( 0, plane);
	}

	return oldViewport;
}
void ReflectionPlane::EndReflection( grcViewport* oldViewport )
{
	grcViewport::SetCurrent( oldViewport );
	if ( g_UseClipPlanes )
	{
		GRCDEVICE.SetClipPlaneEnable( 0);
	}
	grcState::SetCullMode( grccmBack );
}

//------------------------------------- Refraction Plane ----------------------------------
grcViewport*  ReflectionPlane::BeginRefraction( grcViewport& clipViewport, grcViewport& viewport )
{
	Vector4 plane = m_plane;

	grcViewport* oldViewport = grcViewport::GetCurrent();
	viewport = *oldViewport;

	ASSERT_ONLY( Matrix34 ident( Matrix34::IdentityType) );
	AssertMsg( oldViewport->GetWorldMtx().IsClose( ident, 0.0001f ), "Refraction only works with Identity World Matrices" );

	grcViewport::SetCurrent( &viewport );


	//viewport.SetFarClip( m_farClip );
	clipViewport = viewport;

	bool flip = m_isDoubleSided && !OnFrontSide( grcViewport::GetCurrent()->GetCameraPosition());

	if ( !g_UseClipPlanes )
	{
		if (flip)
		{
			// flip the normal
			plane.Negate();
		}

		clipViewport.ApplyObliqueProjection( plane );
		grcViewport::SetCurrent( &clipViewport );
	}
	else
	{
		if (!flip)
		{
			// flip the normal
			plane.Negate();
		}

		// set up user clip plane
		GRCDEVICE.SetClipPlaneEnable( 1);
		GRCDEVICE.SetClipPlane( 0, plane);
	}

	return oldViewport;
}
void ReflectionPlane::EndRefraction( grcViewport* oldViewport )
{
	grcViewport::SetCurrent( oldViewport );
	if ( g_UseClipPlanes )
	{
		GRCDEVICE.SetClipPlaneEnable( 0);
	}
}

//------------------------------------- Procedural Reflection ----------------------------------

ProceduralReflection::ProceduralReflection()
#if __BANK
	: m_isShowReflectionMap(false)
	, m_isShowRefractionMap(false)
#endif // __BANK
{
	m_plane.Set( Vector4( 0.0f, 1.0f, 0.0f, -15.0f ));
	m_plane.SetFarClip( 100000.0f);
}

void ProceduralReflection::BeginReflectionDraw(grcTexture *textureOverride)
{
	if (m_refractionRT.GetPtr() && m_DepthBufferTex != grcegvNONE)
	{
		// set depth buffer for refraction 
		grcEffect::SetGlobalVar(m_DepthBufferTex, (textureOverride) ? textureOverride : grcTextureFactory::GetInstance().GetBackBufferDepth(true));
	}
}

void ProceduralReflection::Draw()
{
	// render refracted objects onto refraction map

	if ( !m_plane.IsVisible())		// cull if not visible
	{
		return;
	}
	DrawReflectionMap();
	DrawRefractionMap();
}

void ProceduralReflection::DrawReflectionMap()
{
	PIXBegin(0, "Reflection" );

	if (m_refractionRT.GetPtr() && m_DepthBufferTex != grcegvNONE)
	{
		// set depth buffer for refraction 
		grcEffect::SetGlobalVar(m_DepthBufferTex, grcTextureFactory::GetInstance().GetBackBufferDepth(true));
	}

	grcViewport		clipView;
	grcViewport		view;
	grcViewport*	oldView = m_plane.BeginReflection( clipView, view );

/*
	HDR exponent bias test
	
	grcResolveFlags simpleFlags;
	simpleFlags.ClearColor = false;
	simpleFlags.ClearDepthStencil = false;
*/


	grcTextureFactory::GetInstance().LockRenderTarget( 0, m_reflectionRT.GetPtr(), m_depthRT.GetPtr()  );

/*
	HDR exponent bias test
#if __XENON
	((grcRenderTargetXenon *) m_reflectionRT.GetPtr())->SetColorExpBias(0);
#endif // __XENON
*/

	GRCDEVICE.Clear(true, Color32(0.0f,0.0f,0.0f,0.0f), true, 1.0f, 0);

/*
	HDR exponent bias test
	grcTextureFactory::GetInstance().UnlockRenderTarget(0, &simpleFlags);

#if __XENON
	//((grcRenderTargetXenon *) m_reflectionRT.GetPtr())->SetColorExpBias(GRCDEVICE.GetColorExpBias());
#endif // __XENON
	grcTextureFactory::GetInstance().LockRenderTarget( 0, m_reflectionRT.GetPtr(), m_depthRT.GetPtr()  );
*/

	// this will draw the scene into our mirror
	if ( m_drawClientFunctor != DrawClientFunctor::NullFunctor() )
	{
		m_drawClientFunctor( this, clipView, view );
	}

	grcResolveFlags flags;
	flags.ClearColor = false;
	flags.ClearDepthStencil = false;
/*
	HDR exponent bias test
	flags.ColorExpBias = -GRCDEVICE.GetColorExpBias();
*/

	grcTextureFactory::GetInstance().UnlockRenderTarget(0, &flags);

	m_plane.EndReflection( oldView );

	PIXEnd();
}

void ProceduralReflection::DrawRefractionMap()
{
	grcViewport		clipView;
	grcViewport		view;
	grcViewport*	oldView;


	if (m_refractionRT.GetPtr())
	{
		PIXBegin(0, "Refraction");

		oldView = m_plane.BeginRefraction( clipView, view );

		// RAY - temporally changing this to be a reflection map
/*
	HDR exponent bias test
#if __XENON
//		((grcRenderTargetXenon *) m_refractionRT.GetPtr())->SetColorExpBias(GRCDEVICE.GetColorExpBias());
#endif // __XENON
*/
		grcTextureFactory::GetInstance().LockRenderTarget( 0, m_refractionRT.GetPtr(), m_depthRT.GetPtr()  );
/*
	HDR exponent bias test
#if __XENON
		((grcRenderTargetXenon *) m_refractionRT.GetPtr())->SetColorExpBias(0);
#endif // __XENON
*/

		GRCDEVICE.Clear(true, Color32(0.0f,0.0f,0.0f,1.0f), true, 1.0f, 0);


/*
		HDR exponent bias test
		grcTextureFactory::GetInstance().UnlockRenderTarget(0, &simpleFlags);

#if __XENON
		//((grcRenderTargetXenon *) m_refractionRT.GetPtr())->SetColorExpBias(GRCDEVICE.GetColorExpBias());
#endif // __XENON

		grcTextureFactory::GetInstance().LockRenderTarget( 0, m_refractionRT.GetPtr(), m_depthRT.GetPtr()  );
*/
		// this will draw the scene into our mirror
		if ( m_drawClientFunctor != DrawClientFunctor::NullFunctor() )
		{
			m_drawClientFunctor( this, clipView, view );
		}

		grcResolveFlags flags;
		flags.ClearColor = false;
		flags.ClearDepthStencil = false;

		grcTextureFactory::GetInstance().UnlockRenderTarget(0, &flags);

		m_plane.EndRefraction( oldView );

		PIXEnd();
	}
}

void ProceduralReflection::Init( int shrinkSize , bool compressedFormat, bool allocateStart, bool doRefraction, bool hdr  )
{
	Assert( shrinkSize >= 1 && shrinkSize <= 32 );

	int width = GRCDEVICE.GetWidth() / shrinkSize;
	int height = GRCDEVICE.GetHeight() / shrinkSize;
	// create rendertarget
	grcTextureFactory::CreateParams cp;

#if __XENON
	cp.Multisample = 4;
	cp.UseHierZ = true;
#endif // __XENON
#if __PPU
	//cp.Multisample = GRCDEVICE.GetMSAA();
#endif // __PPU

	if ( compressedFormat )
	{
		cp.Format = grctfR5G6B5;
	}
	else
	{
#if !__PPU
		if (hdr)
			cp.Format = grctfA2B10G10R10;
		else
#endif
			cp.Format = grctfA8R8G8B8;
		cp.UseFloat = hdr;
	}
	
	cp.IsResolvable = true;   
	if (allocateStart)
	{
		cp.HasParent = true;
		cp.Parent = NULL;
	}

	m_reflectionRT =  grcTextureFactory::GetInstance().CreateRenderTarget( "__reflectionplanert__.dds", grcrtPermanent, width, height, 32, &cp);
	if (doRefraction)
	{
		m_refractionRT = grcTextureFactory::GetInstance().CreateRenderTarget("__refractionrt__.dds", grcrtPermanent, width, height, 32, &cp);
		grcTextureFactory::GetInstance().RegisterTextureReference(m_refractionRT->GetName(), m_refractionRT.GetPtr());
		m_DepthBufferTex = grcEffect::LookupGlobalVar("DepthMap", false);
	}

	cp.IsResolvable = false;
	cp.HasParent = true;
	cp.Parent = m_reflectionRT.GetPtr();
	m_depthRT =  grcTextureFactory::GetInstance().CreateRenderTarget("refractionDepthMap", grcrtDepthBuffer, width, height, 32, &cp);

	grcTextureFactory::GetInstance().RegisterTextureReference(m_reflectionRT->GetName(), m_reflectionRT.GetPtr());
}

void ProceduralReflection::SetDrawClientFunctor( DrawClientFunctor functor )
{
	m_drawClientFunctor = functor;
}

void ProceduralReflection::SetMirrorPlane( const Vector4 &mirrorPlane, float farClip, float size  )
{
	m_plane.Set( mirrorPlane );
	m_plane.SetFarClip( farClip );
	m_plane.SetSize( size );
}
#if __BANK
void ReflectionPlane::AddWidgets( bkBank &bank )
{
	bank.AddSlider( "plane", &m_plane, -1000.0f, 1000.0f, 0.1f );
	bank.AddSlider( "position", &m_pos, -1000.0f, 1000.0f, 0.1f );
	bank.AddSlider("size", &m_size, 0.000001f, 100000.0f, 0.1f );
	bank.AddToggle( "DoubleSided", &m_isDoubleSided );
}
#endif

#if __BANK
void ProceduralReflection::AddWidgets( bkBank &bank )
{
	bank.PushGroup( "Reflection Plane" );
	{
		m_plane.AddWidgets( bank );

		bank.AddToggle("Show Reflection Map", &m_isShowReflectionMap);
		bank.AddToggle("Show Refraction Map", &m_isShowRefractionMap);
	}
	bank.PopGroup();
}

void ProceduralReflection::DebugDraw()
{
	if (m_isShowReflectionMap && m_reflectionRT.GetPtr())
	{
		PUSH_DEFAULT_SCREEN();

		grcBindTexture(m_reflectionRT.GetPtr());

		grcBeginQuads(1);
		grcDrawQuadf(400.0f, 200.0f, 800.0f, 500.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, Color32(1.0f, 1.0f, 1.0f, 1.0f));
		grcEndQuads();

		POP_DEFAULT_SCREEN();
	}
	else if (m_isShowRefractionMap && m_refractionRT.GetPtr())
	{
		PUSH_DEFAULT_SCREEN();

		grcBindTexture(m_refractionRT.GetPtr());

		grcBeginQuads(1);
		grcDrawQuadf(400.0f, 200.0f, 800.0f, 500.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, Color32(1.0f, 1.0f, 1.0f, 1.0f));
		grcEndQuads();

		POP_DEFAULT_SCREEN();
	}
}

#endif // __BANK
//#############################################################################


ProceduralTextureSkyhat::ProceduralTextureSkyhat()
: m_timeOfDay(0.0f)
, m_day(0.0f)
, m_pSkyhatPerlinNoise(NULL)
,m_useImprovedClouds(false )
//, m_initClientFunctor(InitClientFunctor::NullFunctor())
//, m_HighDetailTex( "$/sample_atmoscatt/skydome_new/NOISE16_p.dds" )
//,m_pNoiseBase( "$/sample_atmoscatt/basePerlinNoise3Channel.dds")
{
}

ProceduralTextureSkyhat::~ProceduralTextureSkyhat()
{
    delete m_pSkyhatPerlinNoise;
}
void ProceduralTextureSkyhat::SetPhaseRate( float phaseRate ) 
{ 
	m_pSkyhatPerlinNoise->SetPhaseRate( phaseRate ); 
}
void ProceduralTextureSkyhat::SetInitClientFunctor( InitClientFunctor/* functor */)
{
  //  m_initClientFunctor = functor;
}

void ProceduralTextureSkyhat::OnPostLoadDerived()
{
    // create perlin noise ShaderFragment
    m_pSkyhatPerlinNoise = rage_new SkyhatPerlinNoise;

	if ( !m_pNoiseBase )
	{
		m_pNoiseBase =  atCreateResourceAtPtr( grcTextureFactory::GetInstance().Create( "$/sample_skydome/textures/basePerlinNoise3Channel.dds") );
	}

    m_pSkyhatPerlinNoise->SetNoise1Tex( m_pNoiseBase );
    m_pSkyhatPerlinNoise->AddShader( m_shaders[E_PERLIN_NOISE_SHADER] );

	SetupTechniques();
  
}

void ProceduralTextureSkyhat::SetupTechniques()
{

	// create mini noise ShaderFragment
	m_drawMiniMe = m_shaders[E_MINI_SKY_SHADER]->LookupTechnique( "drawMiniMe" );
	m_blurMiniMe = m_shaders[E_MINI_SKY_SHADER]->LookupTechnique( "blurMiniMe" );
	m_simpleBlurMiniMe = m_shaders[E_MINI_SKY_SHADER]->LookupTechnique( "FastblurMiniMe" );

	char tech[128];
	safecpy(tech, "draw");
	
	if ( m_useImprovedClouds )
	{
		safecat(tech, "_improved");
	}
	m_drawProceduralTexture = m_shaders[E_PERLIN_NOISE_RT]->LookupTechnique(tech );
}
void ProceduralTextureSkyhat::SetCloudWarp( float cloudWarp )
{
	m_pSkyhatPerlinNoise->SetCloudWarp( cloudWarp ); 
}
void ProceduralTextureSkyhat::SetSunSetBump( float sb )
{
	m_pSkyhatPerlinNoise->SetSunSetBump( sb ); 
}

void ProceduralTextureSkyhat::SetPerlinNoiseBaseTexture( atPtr<grcTexture> pNoiseBase )
{
	m_pNoiseBase = pNoiseBase;
}
void ProceduralTextureSkyhat::SetHighDetailTexture( atPtr<grcTexture> pDetailTex )
{
	m_HighDetailTex = pDetailTex;
}

bool ProceduralTextureSkyhat::Load( const ProceduralTextureSkyhat::Params& params ,  const char* shaderName )
{
	Assert( params.IsValid());

	grcTextureFactory::CreateParams rtparams = grcTextureFactory::CreateParams();
	const int PerlinNoiseMapSize = 256;  	

#if __PPU
	// force some PSN specifics here (connected with format of global rendertarget memorypool):
	rtparams.Format			= grctfA8R8G8B8;
	rtparams.CreateAABuffer	= params.CreateAABuffer;
	rtparams.InTiledMemory	= params.IsTiled;
	rtparams.InLocalMemory	= params.IsLocal;
	
#endif //__PPU...
	rtparams.MipLevels		= params.MipLevels;

	ProceduralTextureRenderTargetDef *pPerlinNoiseRT = rage_new ProceduralTextureRenderTargetDef( "__perlinnoisert__", -1, grcrtPermanent, 
		PerlinNoiseMapSize, PerlinNoiseMapSize, 32, rtparams);
	pPerlinNoiseRT->GetCreateParams().Multisample = 0;
	pPerlinNoiseRT->GetCreateParams().Format = grctfA8R8G8B8;
	pPerlinNoiseRT->SetOverwriteScreenMemory( true );



	AddRenderTargetDef( pPerlinNoiseRT );

	m_params = params;

#if __PPU
	Assert(params.format != grctfA2B10G10R10_DONTUSE); // this seems to be taken care of above by not setting the format to grctfA2B10G10R10 on the ps3
// 	if ( params.format == grctfA2B10G10R10 )  
// 	{
// 		m_params.format = grctfA8R8G8B8;
// 	}
#endif
	if ( params.createMiniDiffuseMap )
	{
		m_params.createMiniReflectionMap = true;

	}

	if ( m_params.createMiniReflectionMap )
	{
		int tSize = params.miniReflectionSize;
		// create Mini Sky render target
		ProceduralTextureRenderTargetDef *pMiniNoiseRT = rage_new ProceduralTextureRenderTargetDef( "__miniskyrt__.dds", -1, grcrtPermanent,
				tSize, tSize,tSize);
		pMiniNoiseRT->GetCreateParams().Multisample = 0;
		pMiniNoiseRT->GetCreateParams().Format = m_params.format;
		AddRenderTargetDef( pMiniNoiseRT );
	}
	if ( m_params.createMiniDiffuseMap )
	{
		int btSize = params.miniDiffuseSize;  // create blurred result for ambient lighting
		ProceduralTextureRenderTargetDef *pMiniNoiseBlurRT = rage_new ProceduralTextureRenderTargetDef( "__miniskyblurredrt__.dds", -1, grcrtPermanent,
			btSize, btSize,btSize );
		pMiniNoiseBlurRT->GetCreateParams().Multisample = 0;
		pMiniNoiseBlurRT->GetCreateParams().Format = m_params.format;
		AddRenderTargetDef( pMiniNoiseBlurRT );
	}

	// setup perlin noise shader
	ProceduralTextureShaderDef *pPerlinNoiseShaderDef = rage_new ProceduralTextureShaderDef( "rage_perlinnoise" );
	AddShaderDef( pPerlinNoiseShaderDef );

	// setup mini sky shader
	ProceduralTextureShaderDef *pMiniNoiseShaderDef = rage_new ProceduralTextureShaderDef( shaderName );
	AddShaderDef( pMiniNoiseShaderDef );
	return ProceduralTexture::Load();
}

grmShader& ProceduralTextureSkyhat::GetMiniSkyHatShader()
{
	Assert( m_shaders[1] );
	return *m_shaders[1];
}

void ProceduralTextureSkyhat::Update()
{
    m_pSkyhatPerlinNoise->SetTime( m_timeOfDay + (m_day * 24.0f) );
	m_pSkyhatPerlinNoise->UpdateShaders();
}

void RenderBlit( grcRenderTarget* target, grmShader* shader, const grcEffectTechnique& tech )
{
	Color32	col;

	/*	grcTextureFactory::GetInstance().LockRenderTarget(0, target, NULL);
	shader->TWODBlit(
		-1.0,								// x1 - Destination base x
		1.0,								// y1 - Destination base y
		1.0,						// x2 - Destination opposite-corner x
		-1.0,						// y1 - Destination opposite-corner y
		0.1f,							// z - Destination z value.  Note that the z value is expected to be in 0..1 space
		0.0f,							// u1 - Source texture base u (in normalized texels)
		0.0f,							// v1 - Source texture base v (in normalized texels)
		1.0f,							// u2 - Source texture opposite-corner u (in normalized texels)
		1.0f,							// v2 - Source texture opposite-corner v (in normalized texels)
		col,						// color - reference to Packed color value
		tech);
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);
*/
	grcTextureFactory::GetInstance().LockRenderTarget(0, target, NULL);
	grmModel::SetForceShader( shader );
	if ( shader->BeginDraw(grmShader::RMC_DRAW, true, tech) )
	{
		shader->Bind(0);
		grcBegin(drawTris, 6);
		grcVertex2f(0,0);
		grcVertex2f(1,1);
		grcVertex2f(0,1);
		grcVertex2f(0,0);
		grcVertex2f(1,0);
		grcVertex2f(1,1);
		grcEnd();
		grmModel::GetForceShader()->UnBind();
		grmModel::GetForceShader()->EndDraw();
	}
	grmModel::SetForceShader(0);
	grcTextureFactory::GetInstance().UnlockRenderTarget(0); 

}

void ProceduralTextureSkyhat::DrawMiniSky()
{
	if ( m_params.createMiniReflectionMap )
	{
		// m_shaders[E_MINI_SKY_SHADER]->UpdatePresets();

		PIXBegin(0, "MiniSkyHat" );

		RenderBlit( m_renderTargets[E_MINI_SKY_RT], m_shaders[E_MINI_SKY_SHADER], m_drawMiniMe );


		grcEffectVar texToBlurID =  m_shaders[ E_MINI_SKY_SHADER]->LookupVar("SkyMapTexture", true );

		if ( m_params.createMiniDiffuseMap )
		{
			m_shaders[ E_MINI_SKY_SHADER]->SetVar( texToBlurID, m_renderTargets[ E_MINI_SKY_RT ] );

			const bool performSingleBlur = true;
			if ( performSingleBlur )
			{
				RenderBlit( m_renderTargets[E_MINI_SKY_RT], m_shaders[E_MINI_SKY_SHADER], m_simpleBlurMiniMe );
				m_shaders[ E_MINI_SKY_SHADER]->SetVar( texToBlurID, m_renderTargets[ E_MINI_SKY_RT ] );
			}
			RenderBlit( m_renderTargets[E_MINI_SKY_BLUR_RT], m_shaders[E_MINI_SKY_SHADER], m_blurMiniMe );
		}

		PIXEnd();
	}
}

void ProceduralTextureSkyhat::Draw()
{
    PIXBegin(0, "Perlin Noise Generator" );

	RenderBlit( m_renderTargets[E_PERLIN_NOISE_RT], m_shaders[E_PERLIN_NOISE_RT], m_drawProceduralTexture );

    PIXEnd();

	DrawMiniSky();
}

#if __BANK

void ProceduralTextureSkyhat::AddWidgets( rage::bkBank &bank )
{
	bank.PushGroup( "Cloud Generation" );
	{
		bank.AddToggle( "Use Rescaled Clouds", &m_useImprovedClouds, 
			datCallback( MFA( ProceduralTextureSkyhat::SetupTechniques), this ));
	}
	
	// Add controls for noise
	
	
	/*
    bank.PushGroup( "Skyhat" );
    {
        PARSER.AddWidgets( bank, this );
		bank.AddToggle( "Use Fuller Clouds", &m_useImprovedClouds, 
						datCallback( MFA( ProceduralTextureSkyhat::SetupTechniques), this ));
    }*/
    bank.PopGroup();
}

#endif // __BANK

} // namespace rage

#endif		// 0