// 
// sample_gfx/proceduraltexture.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "proceduraltexture.h"

#include "shaderfragmentcontrol.h"

#include "bank/bank.h"
#include "grcore/quads.h"
#include "grcore/texture.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "rmcore/drawable.h"

#include "grcore/im.h"
#include "grcore/device.h"
#include "grcore/state.h"

#include "proceduraltexture_parser.h"

namespace rage
{

ProceduralTextureRenderTargetDef::ProceduralTextureRenderTargetDef( const char *name, int parentIndex, 
    rage::grcRenderTargetType type, int width, int height, int bitsPerPixel, const grcTextureFactory::CreateParams pParams )
    : m_parentIndex(parentIndex)
    , m_type(type)
    , m_width(width)
    , m_height(height)
    , m_bitsPerPixel(bitsPerPixel)
    , m_pParams(pParams)
	, m_overwriteScreenMemory( false) 
{
    safecpy( m_name, name, 32 );
}

ProceduralTextureRenderTargetDef::ProceduralTextureRenderTargetDef()
    : m_parentIndex(-1)
    , m_type(grcrtCubeMap)
    , m_width(640)
    , m_height(480)
    , m_bitsPerPixel(32)
	, m_overwriteScreenMemory( false) 
{
    m_name[0] = 0;
}

const char* ProceduralTextureRenderTargetDef::ParamsToString( grcTextureFactory::CreateParams *params )
{
    static char str[256];
    
    sprintf( str, "%d %d %d %d %d %d %d %p %d %d", params->Format, params->HasParent, params->IsRenderable, 
        params->IsResolvable, params->Lockable, params->MipLevels, static_cast<int>(params->Multisample), params->Parent, 
        params->UseFloat, params->UseHierZ );
    return str;
}

grcTextureFactory::CreateParams ProceduralTextureRenderTargetDef::ParamsFromString( const char *str )
{
    grcTextureFactory::CreateParams params;

	int p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0, p7 = 0, p8 = 0, p9 = 0, p10 = 0;
	sscanf( str, "%d %d %d %d %d %d %d %d %d %d", &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &p10 );
	params.Format = (grcTextureFormat) p1;
	params.HasParent = p2!=0;
	params.IsRenderable = p3!=0; 
	params.IsResolvable = p4!=0;
	params.Lockable = p5!=0;
	params.MipLevels = p6;
	params.Multisample = (u8)p7;
	params.Parent = (grcRenderTarget*) p8;
	params.UseFloat = p9!=0;
	params.UseHierZ = p10!=0;

	return params;
}

//#############################################################################

ProceduralTextureShaderDef::ProceduralTextureShaderDef( const char *name, grmShaderGroup &shaderGroup, const char* presetName )
: m_pShaderGroup(&shaderGroup)
{
    safecpy( m_name, name, 64 );

    if ( presetName )
    {
        safecpy( m_presetName, presetName, 128 );
    }
    else
    {
        m_presetName[0] = 0;
    }
}

ProceduralTextureShaderDef::ProceduralTextureShaderDef( const char *name, const char* presetName )
: m_pShaderGroup(NULL)
{
    safecpy( m_name, name, 64 );

    if ( presetName )
    {
        safecpy( m_presetName, presetName, 128 );
    }
    else
    {
        m_presetName[0] = 0;
    }
}

ProceduralTextureShaderDef::ProceduralTextureShaderDef()
: m_pShaderGroup(NULL)
{
    m_name[0] = m_presetName[0] = 0;
}

//#############################################################################
// onPostLoad="OnPostLoad"


ProceduralTexture::~ProceduralTexture()
{
    for (int i = 0; i < m_renderTargetDefs.GetCount(); ++i)
    {
        delete m_renderTargetDefs[i];
    }
    m_renderTargetDefs.clear();

    for (int i = 0; i < GetNumRenderTargets(); ++i)
    {
        m_renderTargets[i]->Release();
        //delete m_renderTargets[i];
    }
    m_renderTargets.clear();

	for (int i = 0; i < m_shaderDefs.GetCount(); ++i)
	{
		delete m_shaderDefs[i];
	}
	m_shaderDefs.clear();

	for (int i = 0; i < GetNumShaders(); ++i)
	{
		//m_shaders[i]->Release();
		delete m_shaders[i];
	}
	m_shaders.clear();
	
		
}

bool ProceduralTexture::LoadFile( const char *pFile )
{
    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, pFile );

    return PARSER.LoadObject( pFile, "xml", *this );
}
bool ProceduralTexture::LoadTreeNode( const parTreeNode *pTreeNode )
{
	return PARSER.LoadFromStructure( const_cast<parTreeNode *>(pTreeNode), *parser_GetStructure(), this );
}

#if __BANK
bool ProceduralTexture::SaveFile( const char *pFile )
{
    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, pFile );

    return PARSER.SaveObject( pFile, "xml", this, parManager::XML );
}

bool ProceduralTexture::SaveTreeNode( parTreeNode *pParentNode )
{
    parTreeNode *pNode = PARSER.BuildTreeNode( this );
    if ( pNode )
    {
        pNode->AppendAsChildOf( pParentNode );
        return true;
    }

    return false;
}
#endif // __BANK

void ProceduralTexture::OnPostLoad()
{
    for (int i = 0; i < m_renderTargetDefs.GetCount(); ++i)
    {
        LoadRenderTarget( m_renderTargetDefs[i] );
    }

    for (int i = 0; i < m_shaderDefs.GetCount(); ++i)
    {
        LoadShader( m_shaderDefs[i] );
    }

    OnPostLoadDerived();

    m_isLoaded = true;
}

int ProceduralTexture::GetNumRenderTargets() const
{
    return m_renderTargets.GetCount();
}

grcRenderTarget* ProceduralTexture::GetRenderTarget( int i ) const
{
    if ( i < GetNumRenderTargets() )
    {
		return m_renderTargets[i];
    }
    return NULL;
}

int ProceduralTexture::GetNumShaders() const
{
    return m_shaders.GetCount();
}

grmShader* ProceduralTexture::GetShader( int i ) const
{
    if ( i < GetNumShaders() )
    {
        return m_shaders[i];
    }
    return NULL;
}

void ProceduralTexture::AddRenderTargetDef( ProceduralTextureRenderTargetDef *pRTDef )
{
    m_renderTargetDefs.Grow() = pRTDef;
}

void ProceduralTexture::AddShaderDef( ProceduralTextureShaderDef *pShaderDef )
{
    m_shaderDefs.Grow() = pShaderDef;
}

grcRenderTarget* ProceduralTexture::LoadRenderTarget( const ProceduralTextureRenderTargetDef *pRTDef )
{
    ProceduralTextureRenderTargetDef rtDef = *pRTDef;

    rtDef.GetCreateParams().HasParent = rtDef.OverwriteScreenMemory() ;   // parent cannot be false - set to false for games
    rtDef.GetCreateParams().Parent = NULL;      // write into edram at memory point 0
    if ( pRTDef->GetParentIndex() > -1 )
    {
        grcRenderTarget *pParentRT = GetRenderTarget( pRTDef->GetParentIndex() );
        if ( pParentRT )
        {
            rtDef.GetCreateParams().HasParent = true;
			rtDef.GetCreateParams().Parent = pParentRT; //rtDef.params.Multisample ? NULL :  pParentRT ;
        }
    }

    grcRenderTarget *pRT = grcTextureFactory::GetInstance().CreateRenderTarget( rtDef.GetName(), 
        rtDef.GetType(), rtDef.GetWidth(), rtDef.GetHeight(), rtDef.GetBitsPerPixel(), 
        &rtDef.GetCreateParams() );
    if ( pRT == NULL )
    {
        return NULL;
    }

    m_renderTargets.Grow() = pRT;

	grcTextureFactory::GetInstance().RegisterTextureReference(pRT->GetName(), pRT);

    return pRT;
}

grmShader* ProceduralTexture::LoadShader( const ProceduralTextureShaderDef *pShaderDef )
{
    grmShader *pShader = grmShaderFactory::GetInstance().Create( );

    if ( !pShader->Load(pShaderDef->GetName()) )
    {
        return NULL;
    }

    m_shaders.Grow() = pShader;
    
    return pShader;
}

//#############################################################################

ProceduralTextureManager::ProceduralTextureManager()
{    
}

ProceduralTextureManager::~ProceduralTextureManager()
{
    int count = m_proceduralTextures.GetCount();
    for (int i = 0; i < count; ++i)
    {
        delete m_proceduralTextures[i];
    }
}

int ProceduralTextureManager::Add( ProceduralTexture *pRTMgr )
{
    m_proceduralTextures.Grow() = pRTMgr;
    return m_proceduralTextures.GetCount() - 1;
}

ProceduralTexture* ProceduralTextureManager::Get( int i )
{
    if ( i < m_proceduralTextures.GetCount() )
    {
        return m_proceduralTextures[i];
    }
    return NULL;
}

int ProceduralTextureManager::GetNum() const
{
    return m_proceduralTextures.GetCount();
}

bool ProceduralTextureManager::LoadAll()
{
    int count = m_proceduralTextures.GetCount();
    for (int i = 0; i < count; ++i)
    {
        if ( !m_proceduralTextures[i]->IsLoaded() && !m_proceduralTextures[i]->Load() )
        {
            return false;
        }
    }

    return true;
}

bool ProceduralTextureManager::LoadFile( const char *pFile )
{
    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, pFile );

    parTree *pTree = PARSER.LoadTree( path, "xml" );
    if ( pTree == NULL )
    {
        Printf( "Procedural Texture Manager load tree failed: %s\n", pFile );
        return false;
    }

    parTreeNode *pRootNode = pTree->GetRoot();
    if ( strcmp(pRootNode->GetElement().GetName(),"procedural_textures") != 0 )
    {
        Printf( "Invalid Procedural Texture Manager file: %s\n", pFile );
        return false;
    }    

    return LoadProceduralTexturesTreeNode( pRootNode );
}

#if __BANK
bool ProceduralTextureManager::SaveFile( const char *pFile )
{
    parTree tree;
    parTreeNode *pRootNode = tree.CreateRoot();
    pRootNode->GetElement().SetName( "procedural_textures" );

    if ( !SaveProceduralTexturesTreeNode(pRootNode) )
    {
        return false;
    }

    char path[256];
    fiAssetManager::RemoveExtensionFromPath( path, 256, pFile );
    if ( !PARSER.SaveTree( path, "xml", &tree, parManager::XML ) )
    {
        Printf( "Procedural Texture Manager save tree error\n" );
        return false;
    }

    return true;
}
#endif // __BANK

bool ProceduralTextureManager::LoadProceduralTexturesTreeNode( const parTreeNode *pRootNode )
{
    if ( strcmp(pRootNode->GetElement().GetName(),"procedural_textures") != 0 )
    {
        Printf( "Procedural Texture Manager load tree nodes error: %s is not a procedural_textures tree node\n", pRootNode->GetElement().GetName() );
        return false;
    }

    int count = 0;
    for ( int i = 0; i < m_proceduralTextures.GetCount(); ++i )
    {
		u32 nameHash = m_proceduralTextures[i]->parser_GetStructure()->GetNameHash();
		parTreeNode* pNode = NULL;
		for(parTreeNode::ChildNodeIterator kid = pRootNode->BeginChildren(); kid != pRootNode->EndChildren(); ++kid)
		{
			if (atLiteralStringHash((*kid)->GetElement().GetName()) == nameHash)
			{
				pNode = *kid;
				break;
			}
		}
        if ( pNode )
        {
            parAttribute *pAtt = pNode->GetElement().FindAttribute( "fileRef" );
            if ( pAtt )
            {
                if ( !m_proceduralTextures[i]->LoadFile( pAtt->GetStringValue() ) )
                {
                    Printf( "Procedural Texture load error on %s\n", pAtt->GetStringValue() );
                }
                else
                {
                    Printf( "Loaded Procedural Texture file %s\n", pAtt->GetStringValue() );
                    ++count;
                }
            }
            else
            {                
                if ( !m_proceduralTextures[i]->LoadTreeNode(pNode) )
                {
                    Printf( "Procedural Texture load tree node error on %s\n", m_proceduralTextures[i]->parser_GetStructure()->GetName() );
                }
                else
                {
                    Printf( "Loaded Procedural Texture %s\n", m_proceduralTextures[i]->parser_GetStructure()->GetName() );
                    ++count;
                }
            }
        }
    }

    Printf( "\tLoaded %d Procedural Textures\n", count );

    return (count > 0);
}

#if __BANK
bool ProceduralTextureManager::SaveProceduralTexturesTreeNode( parTreeNode *pParentNode )
{
    if ( strcmp(pParentNode->GetElement().GetName(),"procedural_textures") != 0 )
    {
        Printf( "Procedural Texture Manager save tree nodes error: %s is not a procedural_textures tree node\n", pParentNode->GetElement().GetName() );
        return false;
    }

    for ( int i = 0; i < m_proceduralTextures.GetCount(); ++i )
    {
        if ( ! m_proceduralTextures[i]->SaveTreeNode(pParentNode) )
        {
            Printf( "Procedural Texture save tree node error on %s\n", m_proceduralTextures[i]->parser_GetStructure()->GetName() );
        }
    }

    return true;
}
#endif // __BANK

void ProceduralTextureManager::Update()
{
    int count = m_proceduralTextures.GetCount();
    for (int i = 0; i < count; ++i)
    {
        m_proceduralTextures[i]->Update();       
    }
}

void ProceduralTextureManager::Draw()
{
    int count = m_proceduralTextures.GetCount();
    for (int i = 0; i < count; ++i)
    {
        m_proceduralTextures[i]->Draw();       
    }
}


static void Blit( grcTexture * texture, 					// technique in FX file
		  int DestX, int DestY, int SizeX, int SizeY )
{
	grcBindTexture( texture);
	
	grcBeginQuads(1);
	grcDrawQuadf( (float)DestX, (float)DestY, (float)SizeX, (float)SizeY, 0.f, 0, 0, 1, 1, Color32(255,255,255));
	grcEndQuads();
}
int ProceduralTexture::Show(int OriginX, int OriginY, int Width  )
{

	PUSH_DEFAULT_SCREEN();
	const int RectWidth = Width;
	const int RectHeight = Width;
	int		offset = 0;
	for ( int  j = 0; j < GetNumRenderTargets(); j ++)
	{
		Blit(	GetRenderTarget(j),
			OriginX,					// top left x
			OriginY + offset * RectHeight,	// top left y 
			OriginX + RectWidth,		// bottom right x
			OriginY + (offset+1) * RectHeight );		// bottom right y
		offset++;
	}


	POP_DEFAULT_SCREEN();
	return offset;
}
void ProceduralTextureManager::Show()
{
	PUSH_DEFAULT_SCREEN();
    const int OriginX = 32;
	const int OriginY = 96;
	const int RectHeight = 96;

	int offset = 0;
	for (int i=0;i< m_proceduralTextures.GetCount();i++)
	{
		offset += m_proceduralTextures[i]->Show( OriginX, OriginY + offset * RectHeight, RectHeight );
	}
	// display depth buffer as well
	//grcTexture*		depthBuffer  = (grcTexture*)grcTextureFactory::GetInstance().GetBackBufferDepth(false);
	//Blit( depthBuffer,  OriginX + RectWidth  , 96,  OriginX + RectWidth + RectWidth, 96 + RectHeight   );
	POP_DEFAULT_SCREEN();
}
#if __BANK
void ProceduralTextureManager::AddWidgets( rage::bkBank &bank )
{
    int count = m_proceduralTextures.GetCount();
    for (int i = 0; i < count; ++i)
    {
        m_proceduralTextures[i]->AddWidgets( bank );
    }
}
#endif // __BANK

} // namespace rage
