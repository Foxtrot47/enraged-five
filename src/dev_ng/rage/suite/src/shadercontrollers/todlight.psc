<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="ltSampleTODLight">
	<string name="m_Name" type="member" size="256"/>
	<string name="m_TextureName" type="member" size="256"/>
	<bool name="m_Enabled"/>
	<float name="m_TimeOfDay"/>
	<float name="m_DayOrbitSpeed"/>
	<float name="m_DayOrbitSpeed2"/>
	<float name="m_NightOrbitSpeed"/>
	<float name="m_RateChangeTime"/>
	<bool name="m_Draw"/>
	<struct name="m_KeyClockTime" type="rage::ptxKeyframe"/>
	<struct name="m_KeySunSize" type="rage::ptxKeyframe"/>
	<struct name="m_KeySunColorMod" type="rage::ptxKeyframe"/>
	<struct name="m_KeyGlowColorMod" type="rage::ptxKeyframe"/>
	<struct name="m_KeyColor" type="rage::ptxKeyframe"/>
	<struct name="m_KeyAmbLight" type="rage::ptxKeyframe"/>
	<struct name="m_KeyOrbit" type="rage::ptxKeyframe"/>
	<struct name="m_KeyDistance" type="rage::ptxKeyframe"/>
	<struct name="m_KeyShadowColor" type="rage::ptxKeyframe"/>
</structdef>

</ParserSchema>