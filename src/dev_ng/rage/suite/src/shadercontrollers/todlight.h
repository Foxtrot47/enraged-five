// 
// shadercontrollers/todlight.h 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//
// PURPOSE:
//		This class defines a time of day directional "sun" shadowcasting light -controlling orbit position and keyframed colors

#ifndef TODLIGHT_H
#define TODLIGHT_H

#include "bank/bank.h"
#include "data/base.h"
#include "grcore/effect.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "rmptfx/ptxkeyframe.h"
#include "parser/macros.h"

namespace rage
{
	class grcTexture;
	class ltLightSource;
	class fiAsciiTokenizer;
	class grcRenderTarget;
	class datParser;
	class parRTStructure;

/***** rmcSampleTODLight ***************************************/
/***************************************************************/
class ltSampleTODLight : public datBase
{
public:
		ltSampleTODLight();
		~ltSampleTODLight();

		void Update();
		void Init(const char*name = "TODSun");
		void SetFromTime();
		void SetTimeOfDay(float time){m_TimeOfDay = time;}
		void Draw();
		void Load();
		void GetOrbit(Vector4 &orbit);

		datCallback    *m_CBTimeChanged;
		ptxKeyframe  m_KeyClockTime;
		ptxKeyframe 	m_KeySunSize;
		ptxKeyframe  m_KeySunColorMod;
		ptxKeyframe  m_KeyGlowColorMod;
		ptxKeyframe  m_KeyColor;
		ptxKeyframe  m_KeyAmbLight;
		ptxKeyframe  m_KeyOrbit;
		ptxKeyframe  m_KeyDistance;
		ptxKeyframe	m_KeyShadowColor;


		bool		m_Enabled;
		Matrix34	m_Matrix;
		ltLightSource* m_LightInfo;
		char		m_Name[256];
		char		m_TextureName[256];

		//Time of day Specific Vars
		float		m_TimeOfDay;
		float		m_DayOrbitSpeed;
		float		m_DayOrbitSpeed2;
		float		m_NightOrbitSpeed;
		float		m_RateChangeTime;
		
		//Vars to visulize the light
		bool		m_Draw;
		grcTexture* m_Texture;

		//Interface vars
		Vector4 m_LightColor;
		Vector4 m_AmbientColor;
		Vector4 m_ShadowColor;
		float   m_ShadowBlur;
		float   m_ShadowZOffset;

		//ShaderVars
		grcEffectGlobalVar m_DepthTextureID;
		grcEffectGlobalVar m_ColorTextureID;
		grcEffectGlobalVar m_ShadowColorID;
		grcEffectGlobalVar m_LightMatrixID;
		grcEffectGlobalVar m_PixelBlurID;
		grcEffectGlobalVar m_ZOffsetID;
		grcEffectGlobalVar m_LightDirID;
		grcRenderTarget* m_LightDepthMap;
		grcRenderTarget* m_LightColorMap;

#if __BANK
		static void TimeChanged(ltSampleTODLight * light);
		static void CallSave(ltSampleTODLight * b);
		static void CallLoad(ltSampleTODLight * b);

		void AddWidgets(bkBank& bank,const char* name=0);
		static void AddStaticWidgets(bkBank& bank);
		void Save();
#endif
		void Create(parRTStructure& m_parRTS);

		PAR_PARSABLE;
private:
#if __BANK
	static bool sm_OverrideAmbient;
#endif
};

}; // namespace rage

#endif // TODLIGHT_H
