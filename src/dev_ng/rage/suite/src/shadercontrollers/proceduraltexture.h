// 
// sample_gfx/proceduraltexture.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADERCONTROLLERS_PROCEDURALTEXTURE_H
#define SHADERCONTROLLERS_PROCEDURALTEXTURE_H

#include "atl/array.h"
#include "atl/singleton.h"
#include "atl/string.h"
#include "data/base.h"
#include "grcore/texture.h"


#include "grmodel/shader.h"
#include "parser/macros.h"
#include "vector/vector2.h"

namespace rage
{

class grcRenderTarget;
class grcTexture;
class grmShader;
class grmShaderGroup;
class parTreeNode;

//class Blitter;		// fwd reference

/************************************************************************/
/* Specifies how to create a Render Target                              */
/************************************************************************/
class ProceduralTextureRenderTargetDef
{
public:
    ProceduralTextureRenderTargetDef( const char *name, int parentIndex=-1, grcRenderTargetType type=grcrtCubeMap,
        int width=640, int height=480, int bitsPerPixel=32, const grcTextureFactory::CreateParams pParams = grcTextureFactory::CreateParams() );
    ProceduralTextureRenderTargetDef();
    virtual ~ProceduralTextureRenderTargetDef() { }


    const char* GetName() const { return m_name; }
    void SetName( const char *name ) { safecpy( m_name, name, 64 ); }

    int GetParentIndex() const { return m_parentIndex; }
    void SetParentIndex( int index ) { m_parentIndex = index; }

    grcRenderTargetType GetType() const { return m_type; }
    void SetType( grcRenderTargetType type ) { m_type = type; }

    int GetWidth() const { return m_width; }
    void SetWidth( int width ) { m_width = width;}
    
    int GetHeight() const { return m_height; }
    void SetHeight( int height ) { m_height = height; }

    int GetBitsPerPixel() const { return m_bitsPerPixel; }
    void SetBitsPerPixel( int bitsPerPixel ) { m_bitsPerPixel = bitsPerPixel; }

    grcTextureFactory::CreateParams& GetCreateParams() { return m_pParams; }
    void SetCreateParams( const grcTextureFactory::CreateParams &params ) { m_pParams = params; }

    static const char* ParamsToString( grcTextureFactory::CreateParams *type );
    static grcTextureFactory::CreateParams ParamsFromString( const char *str );

	bool	OverwriteScreenMemory()		{ return m_overwriteScreenMemory; }
	void	SetOverwriteScreenMemory( bool over )	{ m_overwriteScreenMemory = over; }

    PAR_PARSABLE;

private:
    char	m_name[64];
    int		m_parentIndex; // If not -1, this RT will be parented under the RT with this index in the render target list.  Make sure you load the parent first.
    grcRenderTargetType m_type;
    int		m_width;
    int		m_height;
    int		m_bitsPerPixel;
	bool	m_overwriteScreenMemory;
    grcTextureFactory::CreateParams m_pParams;
};

//#############################################################################

/************************************************************************/
/* Specifies how to load a Shader                                       */
/************************************************************************/
class ProceduralTextureShaderDef
{
public:
    ProceduralTextureShaderDef( const char *name, grmShaderGroup &shaderGroup, const char* presetName=0 );
    ProceduralTextureShaderDef( const char *name, const char* presetName=0 );
    ProceduralTextureShaderDef();
    
    virtual ~ProceduralTextureShaderDef() { }

    const char* GetName() const { return m_name; }
    void SetName( const char *name ) { safecpy( m_name, name, 64 ); }

    const char* GetPresetName() const { return m_presetName; }
    void SetPresetName( const char *name ) { safecpy( m_name, name, 128 ); }

    grmShaderGroup* GetShaderGroup() const { return m_pShaderGroup; }
    void SetShaderGroup( grmShaderGroup *pShaderGroup ) { m_pShaderGroup = pShaderGroup; }

    atArray<atString>& GetTextureArray() { return m_textures; }

    PAR_PARSABLE;

private:
    char m_name[64];
    char m_presetName[128];
    grmShaderGroup *m_pShaderGroup;
    atArray<atString> m_textures;
};


//#############################################################################

/************************************************************************/
/* ProceduralTexture                                                    */
/* PURPOSE:                                                             */
/*      To contain all of the grcRenderTargets, grcTextures,            */
/*      grmShaderGroups and/or grmShaders required to perform certain   */
/*      Render to Texture operations.  Accessors are provided to        */
/*      retrieve each piece by name.                                    */
/************************************************************************/
class ProceduralTexture : public datBase
{
public:
    ProceduralTexture()
        : m_isLoaded(false)
    {
    }

    virtual ~ProceduralTexture();

    bool LoadFile( const char *pFile );
	bool LoadTreeNode( const parTreeNode *pTreeNode );

#if __BANK
    bool SaveFile( const char *pFile );
	bool SaveTreeNode( parTreeNode *pParentNode );
#endif

    /*
    PURPOSE
        After the RT def and shader def arrays have been filled
        by hand or by the PARSER, loads those objects
    */    
    void OnPostLoad();

    /*
    PURPOSE
        Called by OnPostLoad(), this is where you link your shader fragments with their render targets and textures
    */
    virtual void OnPostLoadDerived() {}

    /*
    PURPOSE
        Derived classes can put manual loading code here.  The most efficient way to do it
        is to fill the RT def and shader def arrays, then call OnPostLoad()
    PARAMS
        none
    RETURNS
        true on success, false on failure
    */
    virtual bool Load() { OnPostLoad(); return true; }

    /*
    PURPOSE
        Derived classes put custom update code here.
    PARAMS
    RETURNS
    NOTES
        Typical use would be to update shader fragments with game-specific values.
    */
    virtual void Update() {}

    /*
    PURPOSE
        Derived classes put custom drawing code here.
    PARAMS
    RETURNS
    NOTES
        This where you draw into your RTs.  Currently you have to Lock/Unlock
        your RTs with the grcTextureFactory yourselves.  Same with GRPOSTFX RTs.
    */
    virtual void Draw() {}

	/*
	PURPOSE
		Shows the texture on screen for debugging purposes.
	RETURNS
		Number of rendertargets rendered onscreen
	*/
	int Show(int OriginX, int OriginY, int Width  );

#if __BANK
    /*
    PURPOSE
        Derived classes put custom widget code here.
    PARAMS
    RETURNS
    NOTES
    */
    virtual void AddWidgets( bkBank & /*bank*/ ) {}
#endif

    /************************************************************************/
    /* Accessors                                                            */
    /************************************************************************/

    void AddRenderTargetDef( ProceduralTextureRenderTargetDef *pRtDef );
    void AddShaderDef( ProceduralTextureShaderDef *pShaderDef );

	int GetNumRenderTargets() const;
    grcRenderTarget* GetRenderTarget( int i ) const;

    int GetNumShaders() const;
    grmShader* GetShader( int i ) const;

    bool IsLoaded() const { return m_isLoaded; }

    PAR_PARSABLE;

protected:
    /*
    PURPOSE
        Creates a Render Target and stores it in the RT map
    PARAMS
        const ProceduralTextureRenderTargetDef *
    RETURNS
        grcRenderTarget* -- the newly created RT
    NOTES
        if 'pLoadRTSturct->parentName' is valid, this RT will be parented to the RT of that mapName.  The parent should be loaded first.
    */
    grcRenderTarget* LoadRenderTarget( const ProceduralTextureRenderTargetDef *pRTDef  );
    
    /*
    PURPOSE
        Loads a shader.  A shader group or group index may be specified, as well as a shader preset sps file
    PARAMS
        const ProceduralTextureShaderDef *pShaderDef
    RETURN
        grmShader*
    NOTES
        If no group is provided, a default one is created
    */
    grmShader* LoadShader( const ProceduralTextureShaderDef *pShaderDef );

    //-START PARSER loads into these
    atArray<ProceduralTextureRenderTargetDef *> m_renderTargetDefs;
    atArray<ProceduralTextureShaderDef *> m_shaderDefs;
    //-STOP

    //-START From the array above, we load and create these
    atArray<grcRenderTarget *> m_renderTargets;
    atArray<grmShader *> m_shaders;
    //-STOP

    bool m_isLoaded;
};

//#############################################################################

/************************************************************************/
/* ProceduralTextureManager                                             */
/* PURPOSE:                                                             */
/*      To manage a collection of Procedural Textures                   */
/************************************************************************/
class ProceduralTextureManager
{
public:
    ProceduralTextureManager();
    ~ProceduralTextureManager();

    /*
    PURPOSE
        Adds a procedural texture to the collection
    PARAMS
        ProceduralTexture *
    RETURNS
        int -- ID of the procedural texture that was added
    NOTES
        Once added, ProceduralTextureManager owns the procedural texture and will handle deletion.
    */
    int Add( ProceduralTexture *pRTMgr );

    /*
    PURPOSE
        Retrieve a procedural texture with the given ID
    PARAMS
        int
    RETURNS
        ProceduralTexture * -- NULL if 'i' is invalid
    NOTES
    */
    ProceduralTexture* Get( int i );


    /*
    PURPOSE
        Get the number of procedural textures
    PARAMS
    RETURNS
        int
    NOTES
    */
    int GetNum() const;

    /*
    PURPOSE
        Calls each procedural texture's Load function if it's m_isLoaded flag is false
    PARAMS
    RETURNS
        bool - false on the first failure, true if they all succeed
    NOTES
    */
    bool LoadAll();

    //-START Loads/saves all procedural textures to the file.  
    bool LoadFile( const char *pFile );
#if __BANK
    bool SaveFile( const char *pFile );
#endif
    //-STOP

    //-START Loads/saves all procedural textures to the tree node
    bool LoadProceduralTexturesTreeNode( const parTreeNode *pRootNode );
#if __BANK
    bool SaveProceduralTexturesTreeNode( parTreeNode *pParentNode );
#endif
    //-STOP

    /*
    PURPOSE
        Calls each procedural texture's Update function
    PARAMS
    RETURNS
    NOTES
    */
    void Update();

    /*
    PURPOSE
        Calls each procedural texture's Draw function
    PARAMS
    RETURNS
    NOTES
    */
    void Draw();

	/*
	PURPOSE
		Draws the procedural texture onto the screen for debugging purposes
	PARAMS
	RETURNS
	NOTES
	*/
	void Show();


#if __BANK
    /*
    PURPOSE
        Calls each procedural texture's AddWidgets function
    PARAMS
    RETURNS
    NOTES
    */
    void AddWidgets( bkBank &bank );
#endif

private:
	  atArray<ProceduralTexture *> m_proceduralTextures;
};


typedef atSingleton<ProceduralTextureManager> ProceduralTextureController;

//#############################################################################

} // namespace rage

#endif // SHADERCONTROLLERS_PROCEDURALTEXTURE_H
