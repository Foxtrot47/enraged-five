//**************************************************************//
//  RAGE Atmospheric Scattering & Cloud Generation Shader
//
//  David Serafim 05 Dec 2005
//**************************************************************//

#define NO_SKINNING

#include "../../../base/src/shaderlib/rage_common.fxh"
#if HACK_GTA4
	// NOTE: Jimmy uses its own atmoscatt_clouds.fx shader,
	// but adding these register allocation restrictions just in case:

	// embedded shaders should still obey Jimmy rules when it comes to global register allocation:
	// #include "../../../../game/shader_source/common.fxh"

	// We don't care for skinning matrices here, so we can use a bigger constant register file
	#pragma constant 130
#endif //HACK_GTA4...

#include "../../../base/src/shaderlib/rage_atmoscatt_common.fxh"
#include "../../../base/src/shaderlib/rage_utility.fxh"
#include "../../../base/src/shaderlib/rage_xplatformtexturefetchmacros.fxh"

#define TOPCLOUDS			(1)
#define NIGHTEFFECTS		(1)

//--------------------------- Image biased lighting for diffuse calculation ---------------------

BeginSampler(sampler2D,DepthMap,DepthMapSampler,DepthMap)
ContinueSampler(sampler2D,DepthMap,DepthMapSampler,DepthMap)
	MIN_POINT_MAG_POINT_MIP_NONE;
   AddressU  = CLAMP;
   AddressV  = CLAMP;
EndSampler;

BeginSampler(sampler2D,SkyMapTexture, SkyMapSampler,SkyMapTexture)
ContinueSampler(sampler2D,SkyMapTexture, SkyMapSampler,SkyMapTexture)
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D,MoonTexture, MoonSampler,MoonTexture)
    string UIName = "MoonTexture";
ContinueSampler(sampler2D,MoonTexture, MoonSampler,MoonTexture)
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
	BorderColor = 0xffffffff;
EndSampler;


BeginSampler(sampler2D,MoonGlowTexture, MoonGlowSampler,MoonGlowTexture)
    string UIName = "MoonTexture";
ContinueSampler(sampler2D,MoonGlowTexture, MoonGlowSampler,MoonGlowTexture)
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
	BorderColor = 0xffffffff;
EndSampler;

BeginSampler(sampler2D,StarFieldTexture, StarFieldSampler,StarFieldTexture)
    string UIName = "StarFieldTexture";
ContinueSampler(sampler2D,StarFieldTexture, StarFieldSampler,StarFieldTexture)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D,GalaxyTexture, GalaxySampler,GalaxyTexture)
    string UIName = "GalaxyTexture";
ContinueSampler(sampler2D,GalaxyTexture, GalaxySampler,GalaxyTexture)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_NONE;
	LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D,PerlinNoiseMap,PerlinNoiseSampler,PerlinNoiseMap)
    string UIName = "Perlin Noise Map";
ContinueSampler(sampler2D,PerlinNoiseMap,PerlinNoiseSampler,PerlinNoiseMap)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;
 
BeginSampler(sampler2D,HighDetailNoiseMap, HighDetailNoiseSampler,HighDetailNoiseMap)
    string UIName = "HighDetailNoiseMap";
ContinueSampler(sampler2D,HighDetailNoiseMap, HighDetailNoiseSampler,HighDetailNoiseMap)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;

BeginSampler(sampler2D,HighDetailNoiseBumpMap, HighDetailNoiseBumpSampler,HighDetailNoiseBumpMap)
    string UIName = "HighDetailNoiseBumpMap";
ContinueSampler(sampler2D,HighDetailNoiseBumpMap, HighDetailNoiseBumpSampler,HighDetailNoiseBumpMap)
	AddressU  = WRAP;        
	AddressV  = WRAP;
	AddressW  = WRAP;
	MIN_LINEAR_MAG_LINEAR_MIP_LINEAR;
	LOD_BIAS = 0;
EndSampler;

#if __WIN32PC
#define HALF4	float4
#define HALF3	float3
#define HALF2	float2
#define HALF	float
#else
#define HALF4	half4
#define HALF3	half3
#define HALF2	half2
#define HALF	float
#endif // __WIN32PC

HALF4	SunDirection		: SunDirection;
HALF3	SkyColor			: SkyColor;
HALF3	AzimuthColor		: AzimuthColor;
HALF3	SunColor			: SunColor;
HALF	UnderLightStrength	: UnderLightStrength;

HALF MoonGlow
<
	string UIName = "Moon Glow";
   string UIWidget = "Numeric";
   float UIMin = 0.0;
   float UIMax = 2.0;
   float UIStep = 0.01;
> =  0.2f;

HALF2 SunAxias 
<
	string UIName = "Sun Axias";
   string UIWidget = "Numeric";
   float UIMin = -1.0;
   float UIMax = 1.0;
   float UIStep = 0.01;
> = { 1.0f, 0.0f };

HALF3 AzimuthColorEast
<
	string UIName = "Azimuth Color East";
	string UIWidget = "Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = { 1.0, 0.00, 1.00};

HALF AzimuthHeight
<
	string UIName = "Azimuth Height";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 20.0;
> = 8.0;

HALF4 CloudColor
<
	string UIName = "Cloud Color";
	string UIWidget = "Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = { 0.93, 1.00, 1.00, 1.00 };

HALF4 SunsetColor
<
	string UIName = "Sunset Color";
	string UIWidget = "Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = { 1.00, 0.51, 0.38, 1.00 };

HALF CloudThreshold
<
   string UIName = "Cloud Threshold";
   string UIWidget = "Numeric";
   float UIMin = -5.00;
   float UIMax = 5.00;
	float UIStep = 0.05;
> = 1.80;

HALF CloudBias
<
   string UIName = "Cloud Bias";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 1.00;
	float UIStep = 0.05;
> = 0.8;

HALF CloudFadeOut
<
   string UIName = "Cloud Fade Out";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 1.00;
	float UIStep = 0.05;
> = 1.0;

HALF TopCloudHeight
<
   string UIName = "Top Cloudlayer Height";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 10.00;
	float UIStep = 0.05;
> = 2.0f;


HALF TopCloudDetail
<
   string UIName = "Top Cloudlayer Detail";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 10.00;
	float UIStep = 0.05;
> = 3.0f;

HALF TopCloudThreshold
<
   string UIName = "Top Cloudlayer Threshold";
   string UIWidget = "Numeric";
   float UIMin = -5.00;
   float UIMax = 5.00;
	float UIStep = 0.02;
> = 0.9;

HALF4 TopCloudBiasDetailThresholdHeight
<
   string UIName = "Top CloudLayer Bias Height";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 10.00;
	float UIStep = 0.02;
> = { 0.4, 3.0, 0.9, 2.0 };

HALF3 TopCloudColor
<
	string UIName = "Top Cloud Color";
	string UIWidget = "Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = { 0.93, 1.00, 1.00 };

HALF TopCloudLight
<
	string UIName = "Top Cloud LIght";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 10.0;
> = 0.0f;


HALF CloudShadowStrength
<
   string UIName = "Cloud Shadow Strength";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 1.00;
   float UIStep = 0.025;
> = 0.4;

HALF CloudShadowOffset
<
   string UIName = "Cloud Shadow Offset";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 1.00;
   float UIStep = 0.01;
> = 0.12;

HALF CloudInscatteringRange
<
   string UIName = "Cloud Inscattering Range";
   string UIWidget = "Numeric";
   float UIMin = 0.1;
   float UIMax = 1.00;
   float UIStep = 0.1;
> = 1;

HALF AzimuthStrength
<
   string UIName = "Azimuth Strength";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 1.00;
   float UIStep = 0.1;
> = 0.5;

HALF4 CloudThicknessEdgeSmoothDetailScaleStrength
<
	string UIName = "Cloud Thickness &  EdgeSmooth DetailScale & Strength";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 64.00;
   float UIStep = 0.1;
> = { 0.35, 1.0f, 16.0f, 0.15f };

// night
HALF4 StarFieldBrightness : StarFieldBrightness;
/*<
   string UIName = "StarFieldBrightness";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 1.00;
   float UIStep = 0.01;
> = 0.5;

*/

HALF StarFieldUVRepeat
<
  string UIName = "StarFieldUVRepeat";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 64.00;
   float UIStep = 0.01;
> = 16.0f;

HALF2 GalaxyOffset
<
	string UIName = "GalaxyOffset";
	string UIWidget = "Numeric";
	float UIMin = -16.00;
	float UIMax = 16.00;
	float UIStep = 0.01;
> = { 0.0f, 0.0f };

HALF4 MoonTexPosition <
  string UIName = "MoonTexPosition";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 64.00;
   float UIStep = 0.01;
> = { 0.5f, 0.5, 0.5f, 0.5f };

HALF4 MoonLight <
   string UIName = "Moon Light";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 64.00;
   float UIStep = 0.01;
> = { 0.5f, 0.5, 0.5f, 1.0f };

HALF StarThreshold
<
   string UIName = "StarField Threshold";
   string UIWidget = "Numeric";
   float UIMin = -1.00;
   float UIMax = 1.00;
   float UIStep = 0.01;
> = 0.0f;

HALF MoonVisiblity
<
   string UIName = "Moon Visiblity";
   string UIWidget = "Numeric";
   float UIMin = 0.00;
   float UIMax = 5.00;
   float UIStep = 0.01;
> = 1.0f;
HALF SunSize : SunSize = 1.0f;

HALF4 MoonColorConstant = { 1.0f, 1.0f, 1.0f, 1.0f };

HALF3 MoonPosition: MoonPosition;
HALF3 MoonXVector: MoonXVector;
HALF3 MoonYVector: MoonYVector;
HALF2 DetailOffset: DetailOffset =  {0.0f,0.0f};


const HALF TimeOfDay : TimeOfDay = 5;
float HDRExposure : HDRExposure;
HALF HDRSunExposure: HDRSunExposure;
HALF3 HDRExposureClamp: HDRExposureClamp;

#define GalaxyTexCoord StarGalaxyTexCoord.zw
#define StarTexCoord StarGalaxyTexCoord.xy

struct VS_OUTPUT_FAST
{
	HALF4 Pos									: POSITION;
	HALF4 TexCoordAndWorldPosZ					: TEXCOORD0;
	HALF4 MoonTexCoordAndWorldPosXY			: TEXCOORD1;
	HALF4 HighCloudTexCoordAndDistanceScalar	: TEXCOORD2;
	HALF4 StarGalaxyTexCoord					: TEXCOORD3;
	HALF3 SkyCol								: TEXCOORD4;
#if RAGE_VELOCITYBUFFER
	HALF4 PosTexCoord							: TEXCOORD5;
	HALF4 PrevPosTexCoord						: TEXCOORD6;
#endif // RAGE_VELOCITYBUFFER

	RAGE_CLIPPLANE_VSOUTPUT_MEMBER(ClipPlanes)
};

struct VS_OUTPUT
{
	HALF4 Pos:       POSITION;
	HALF3 TexCoord:   TEXCOORD0;
	HALF3 WorldPos:   TEXCOORD1;
	HALF4 CloudShadowTexCoord:  TEXCOORD2;
	HALF4 StarGalaxyTexCoord: TEXCOORD3;
	HALF3 HighCloudTexCoord:  TEXCOORD4;	
	HALF3 SkyCol								: TEXCOORD5;
};


struct VS_MINIOUTPUT
{
	HALF4 Pos:       POSITION;
	HALF2 TexCoord:   TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 color : COLOR0;
};

float AzimuthColorZone( float3 pt, float2 dir  )
{
	return dot( pt.xz, dir.xy )  * 0.5f + 0.5f;
}

float2 SphericalWarpOffset( float2 Tex , float2 offset )
{
	float2 t = ( 2.0f * Tex - 1.0f );
	float mag = dot( t, t ); 
	return offset * ( 1.0f - mag );
}
float3 RageNormalize3( float3 d )
{
	float mag = dot(d, d);
	float recip = rsqrt( mag );
	return ( mag == 0.0f ) ?  0.0f : d * recip;
}
float2 RageNormalize2( float2 d )
{
	float mag = dot(d, d);
	float recip = rsqrt( mag );
	return ( mag == 0.0f ) ?  0.0f : d * recip;
}
float2 SphericalWarp( float2 Tex )
{
	//const float hackScale = 10.0f;
	const float hackScale = 1.0f;//4.0f;	// ray less for the detail map
	Tex = Tex - 0.5;
	float mag = dot( Tex, Tex ); 
	Tex =  mag * RageNormalize2( Tex)* hackScale;
	return Tex;
}
float detail( float2 Tex , out float3 normal )
{
	// if use rgb could do both in one go
	normal = ( tex2D( HighDetailNoiseBumpSampler, Tex ) * 2.0f - 1.0f ).wyw * CloudThicknessEdgeSmoothDetailScaleStrength.w *4.0f;
	normal.z = 1.0f - dot( normal.xy, normal.xy );
	return ( tex2D( HighDetailNoiseSampler, Tex ) - 0.5f).x * CloudThicknessEdgeSmoothDetailScaleStrength.w; 	
}

float4	CalcScreenSpacePosition	(float4 Position)
{
	float4	ScreenPosition;

	ScreenPosition.x = Position.x * 0.5f + Position.w * 0.5f;
	ScreenPosition.y = Position.w * 0.5f - Position.y * 0.5f;
	ScreenPosition.z = Position.w;
	ScreenPosition.w = Position.w;

	return(ScreenPosition);
}


float dpMapNearClip = 0.0f;
float dpMapFarClip = 20000.0f;
float4 DualParaboloidPosTransform(float3 inPos, bool frontMapFace)
{
	float4 pos = mul( float4(inPos, 1.0f), gWorldViewProj );
		
	float preDepth = pos.z;
	pos.xyz /= abs(pos.w);

	float posMag = length(pos.xyz);
	pos.xyz /= posMag;


	pos.z += 1.0f;
	pos.x /= pos.z;
	pos.y /= pos.z;

	pos.z = preDepth / dpMapFarClip;//((preDepth - dpMapNearClip) / (dpMapFarClip - dpMapNearClip));
	pos.w = 1.0f;


	pos.x *= 0.5f;
	if(!frontMapFace)
	{
		pos.x += 0.5f;
	}
	else
		pos.x -= 0.5f;


	return pos;
}

float3 SkyColorCalculation(float3 view)
{
	float4 azimuthColorAndFade;
	
	azimuthColorAndFade.w =  (1 - saturate(view.y * AzimuthHeight)) * AzimuthStrength;
	// I don't know why, but SunAxias doesn't work for RDR2.
	azimuthColorAndFade.rgb = lerp(AzimuthColorEast, AzimuthColor, AzimuthColorZone(view, float2(1.0f, 0.0f)));
	azimuthColorAndFade.rgb *= azimuthColorAndFade.w;
	
	return (SkyColor + azimuthColorAndFade.rgb );//+ SunsetColor.rgb);
}

float DistanceScalarCalculation(float3 view)
{
	return saturate(normalize(view).y * 4.0f - CloudFadeOut);
}

VS_OUTPUT_FAST vs_main_common_fast(
	float4 transPos,
	float4 inPos,
	float2 inTxr,
	float2 inStarCoord)
{
	VS_OUTPUT_FAST OUT;

	OUT.Pos = transPos;
	float3 worldPos = (float3)mul(inPos, gWorld);
	worldPos -= gWorld[3].xyz;
	OUT.TexCoordAndWorldPosZ.w = worldPos.z;
	OUT.MoonTexCoordAndWorldPosXY.zw = worldPos.xy;
	OUT.HighCloudTexCoordAndDistanceScalar.w = DistanceScalarCalculation(worldPos);

	OUT.TexCoordAndWorldPosZ.xy = inTxr;
	OUT.StarTexCoord = inStarCoord;

	HALF3 view = normalize(worldPos);
	HALF3 newView = view;
	newView.y = newView.y * 1.5f + 0.5f;
	newView = normalize( newView );
	
	// calculate night time uv coords
	HALF2 SkyCoord = HALF2( asin( newView.x)/3.1473 + 0.5,  asin( newView.z)/3.1473 + 0.5 );
	OUT.GalaxyTexCoord = SkyCoord+  GalaxyOffset.xy;
	OUT.StarTexCoord  = OUT.GalaxyTexCoord * StarFieldUVRepeat;
	
	OUT.MoonTexCoordAndWorldPosXY.xy = ( SkyCoord - MoonTexPosition.zw ) * MoonTexPosition.xx;
	
	// shadow tex coord
	HALF3 shadowdir = SunDirection.xyz - view;
	HALF2 ofs = CloudShadowOffset / 10;
	ofs *= shadowdir.xz;
	
	HALF2 scaledTex = ( OUT.TexCoordAndWorldPosZ.xy - 0.5 ) * TopCloudBiasDetailThresholdHeight.w;
	HALF fadeOut = 1.0f - length( scaledTex);
	scaledTex += 0.5f;
	OUT.HighCloudTexCoordAndDistanceScalar.xy = scaledTex.xy;
	OUT.HighCloudTexCoordAndDistanceScalar.z = fadeOut;
	
	OUT.TexCoordAndWorldPosZ.z = 1.0-transPos.z/transPos.w;
	OUT.SkyCol = SkyColorCalculation(view);
	
	rageComputeClipPlanes(OUT.Pos, OUT.ClipPlanes);
	
	return OUT;
}



VS_OUTPUT vs_main_common(HALF4 transPos, HALF4 inPos, HALF2 inTxr, HALF2 inStarCoord)
{
	VS_OUTPUT OUT;

	OUT.Pos = transPos;
	OUT.WorldPos = (HALF3) mul(inPos, gWorld);
	OUT.WorldPos -=gWorld[3].xyz;
	OUT.TexCoord.xy = inTxr;
	OUT.StarTexCoord = inStarCoord;

	HALF3 view = normalize( OUT.WorldPos );
	HALF3 newView = view;
	newView.y = newView.y * 1.5f + 0.5f;
	newView = normalize( newView );
	

	// calculate night time uv coords
	HALF2 SkyCoord = HALF2( asin( newView.x)/3.1473 + 0.5,  asin( newView.z)/3.1473 + 0.5 );
	OUT.GalaxyTexCoord = SkyCoord+  GalaxyOffset.xy;
	OUT.StarTexCoord  = OUT.GalaxyTexCoord * StarFieldUVRepeat;
	
	// shadow tex coord
	HALF3 shadowdir = SunDirection.xyz - view;
	HALF2 ofs = CloudShadowOffset / 10;
	ofs *= shadowdir.xz;
	OUT.CloudShadowTexCoord.xy  = OUT.TexCoord.xy +  SphericalWarpOffset( OUT.TexCoord.xy , ofs );
	OUT.CloudShadowTexCoord.zw = SphericalWarp( OUT.TexCoord.xy )* CloudThicknessEdgeSmoothDetailScaleStrength.z + DetailOffset.xy;
		//OUT.TexCoord.xy* CloudThicknessEdgeSmoothDetailScaleStrength.z + DetailOffset.xy;
		//
	
	
	HALF2 scaledTex = ( OUT.TexCoord.xy - 0.5 ) * TopCloudBiasDetailThresholdHeight.w;
	HALF fadeOut = 1.0f - length( scaledTex);
	scaledTex += 0.5f;
	OUT.HighCloudTexCoord.xy  =scaledTex.xy;
	OUT.HighCloudTexCoord.z  = fadeOut;
	
	OUT.TexCoord.z = 1.0-transPos.z/transPos.w;

	OUT.SkyCol = SkyColorCalculation(view);

	return OUT;
}


float4  vs_stencil( HALF4 inPos: POSITION ) :POSITION
{
	return mul(inPos, gWorldViewProj);
}

#pragma dcl position texcoord0 texcoord1

VS_OUTPUT vs_main(HALF4 inPos: POSITION, HALF2 inTxr: TEXCOORD0, HALF2 inStarCoord : TEXCOORD1 )
{
	HALF4 transPos = mul(inPos, gWorldViewProj);
	return vs_main_common(transPos, inPos, inTxr, inStarCoord);
}
VS_OUTPUT_FAST vs_main_fast(HALF4 inPos: POSITION, HALF2 inTxr: TEXCOORD0, HALF2 inStarCoord : TEXCOORD1 )
{
	HALF4 transPos = mul(inPos, gWorldViewProj);
	return vs_main_common_fast(transPos, inPos, inTxr, inStarCoord);
}

VS_OUTPUT vs_DpFrontMain(HALF4 inPos: POSITION, HALF2 inTxr: TEXCOORD0, HALF2 inStarCoord : TEXCOORD1 )
{
	HALF4 transPos = DualParaboloidPosTransform(inPos.xyz, true);
	return vs_main_common(transPos, inPos, inTxr, inStarCoord);
}

VS_OUTPUT vs_DpBackMain(HALF4 inPos: POSITION, HALF2 inTxr: TEXCOORD0, HALF2 inStarCoord : TEXCOORD1 )
{
	HALF4 transPos = DualParaboloidPosTransform(inPos.xyz, false);
	return vs_main_common(transPos, inPos, inTxr, inStarCoord);
}

VS_MINIOUTPUT vs_MiniSky( HALF4 inPos: POSITION )
{
	VS_MINIOUTPUT OUT;
	
	OUT.Pos = HALF4(inPos.xy*2-1,0,1);
	OUT.TexCoord.x = inPos.x;
	OUT.TexCoord.y = 1 - inPos.y;
	
	return OUT;
}

//--------------------------------------------------------------------
//
//	Calculates the Sky Dome and clouds
//
HALF4 ps_calculateSky( VS_OUTPUT IN , HALF3 view, HALF4 sunColor, HALF4 sunCloudCol , uniform bool doStars, uniform bool doMoon )
{
	
	//------------------------------
	// sky & cloud
	//------------
	//---------------------------------------
	// Calculate the cloud denisty
	//
	HALF3 detailNormal; 
	HALF2 cld = tex2D(PerlinNoiseSampler, IN.TexCoord.xy).xz;

	HALF det =( tex2D( HighDetailNoiseSampler, IN.CloudShadowTexCoord.zw ) - 0.5f).x;

	HALF detailAmt = det* CloudThicknessEdgeSmoothDetailScaleStrength.w;
	HALF cloud =  cld.x +  detailAmt ;

	HALF cloudDensity = saturate(CloudThreshold * cloud - CloudBias );
	HALF amount = pow( cloudDensity , CloudThicknessEdgeSmoothDetailScaleStrength.x) * saturate( cloudDensity * CloudThicknessEdgeSmoothDetailScaleStrength.y );
	
	HALF3 skycol = SkyColor.xyz;
	
	
	//---------------------------------------
	// Calculate the cloud self shadowing
	//
	HALF shadow = tex2D(PerlinNoiseSampler, IN.CloudShadowTexCoord.xy).x;
	shadow += ( tex2D( HighDetailNoiseSampler, IN.CloudShadowTexCoord.xy * CloudThicknessEdgeSmoothDetailScaleStrength.z ) ).x * CloudThicknessEdgeSmoothDetailScaleStrength.w * 0.2f;		// add noise detail to shadow
	shadow = saturate( (CloudThreshold * shadow - CloudBias)  * CloudShadowStrength );
		float inten = 1.0f ;

	// add in a top layer without self shadowing
	HALF topCloud = tex2D(PerlinNoiseSampler, IN.HighCloudTexCoord.xy ).y;  
	topCloud += detailAmt * TopCloudBiasDetailThresholdHeight.y;
	topCloud = saturate(TopCloudBiasDetailThresholdHeight.z * topCloud  - TopCloudBiasDetailThresholdHeight.x) * IN.HighCloudTexCoord.z;


	//-------------------------------------
	// cloud inscattering
	//	using rayleigh scattering
	//
	HALF sDot = dot( view, SunDirection.xyz);
	HALF scatterDot= sDot * sDot;

	float invamount = 1.0f - amount;
	HALF forwardScattering =  1.0f;//1.0f + scatterDot  * saturate( ( invamount - ( shadow * 0.5f) ) ) * CloudInscatteringRange;
	inten *= forwardScattering;


	float lowLOD =saturate( cld.y  + det * 0.2f);
	HALF sunsetAmt = (1.0f - shadow ) * amount * lowLOD   + lowLOD ;
	sunsetAmt *= (sDot * 0.5f * 0.5f+ 0.7f * 0.5f) ;

	//------------------------------------
	// add underlighting at sunset
	HALF3  cloudcol = (inten * CloudColor.xyz - shadow) + SunsetColor.xyz * sunsetAmt;
	

	HALF3 topCloudCol =  TopCloudColor.xyz; // ->now done on cpu + SunsetColor.xyz;
	topCloud *=invamount;
	cloudcol.xyz = lerp( cloudcol.xyz, topCloudCol.xyz, topCloud.xxx );
	amount = invamount * topCloud + amount;
	
	//-------------------------
	// blend in sun 
	HALF3 skyColor = saturate( IN.SkyCol * sunColor.www  ) +  sunColor.xyz ;// use hdr for sun glow through clouds
	
	HALF3 Galaxy = 0.0f;
	if ( doStars  )
	{
		//-- add in stars
		HALF3 Stars =tex2D( StarFieldSampler, IN.StarTexCoord ).xyz * StarFieldBrightness.x - (StarFieldBrightness.z);	// This compensates to equalize Daren's settings.
		HALF3 GalaxyVal = tex2D( GalaxySampler, IN.GalaxyTexCoord ).xyz * StarFieldBrightness.yyy;

		// add Galaxy and stars
		Galaxy =  saturate( Stars.xyz + Stars.xyz * dot(GalaxyVal.xyz,float3( 1.2f,1.2f,1.2f)) + GalaxyVal.xyz );  // 
	}
	else
	{
		Galaxy = tex2D( GalaxySampler, IN.GalaxyTexCoord ).xyz * StarFieldBrightness.yyy;
	}

	if ( doMoon )
	{
		HALF3 dmoon = view -MoonPosition - MoonXVector - MoonYVector;

		float mx = dot(MoonXVector , dmoon );
		float my = dot( MoonYVector , dmoon );
		HALF2 moonTex =  float2( mx, my);
		HALF2 moonTexGlow =  (( moonTex.xy - 0.5f) * ( 1.0f - 0.1f) ) + 0.5f;


			//HALF2 moonMaskGlow;
		HALF2 moonMaskGlow;
		moonMaskGlow.x = (moonTexGlow.x < 0.01f )|| ( moonTexGlow.x > 0.99f);
		moonMaskGlow.y = (moonTexGlow.y < 0.01f )|| ( moonTexGlow.y > 0.99);

		moonTexGlow = ( moonTexGlow * MoonLight.xy ) + MoonLight.zw ;
		HALF2 moonddx = ddx( moonTexGlow );
		HALF2 moonddy = ddy( moonTexGlow );

		if ( ( moonMaskGlow.x + moonMaskGlow.y ) == 0.0f)
		{
		
			HALF3 MoonLighting = 0;
			HALF3 MoonCloudLighting = 0;

			HALF2 moonMask = ( moonTex.xy > 1.0f ) || ( moonTex.xy < 0.0f);
			moonMask = 1.0 - saturate( moonMask.x + moonMask.y );
			// rescale to frame
			moonTex =  ( moonTex  * MoonLight.xy ) + MoonLight.zw ;
			HALF4 moonColor = tex2D( MoonSampler, moonTex /*, moonddx, moonddy*/ ) * moonMask.x;
			moonColor.xyz *= MoonColorConstant.xyz * moonColor.w;

			HALF4 moonGlowTex = tex2D( MoonGlowSampler, moonTexGlow /*, moonddx, moonddy*/ );
			
			moonGlowTex.xyz = moonGlowTex.x * ( 1.0f+ MoonColorConstant.xyz ) * MoonGlow; // MoonColorConstant.xyz * 
			moonColor.xyz +=  moonGlowTex.xyz;// * ( 1.0f - moonColor.w ); // MoonColorConstant.xyz * 

			// calculate mix of moon and sky
			Galaxy.xyz *= 1.0f - moonColor.w;

			// could approximate with constant
			HALF MoonAttenuation = dot( skyColor.xyz, HALF3( 0.3f, 0.7f, 0.17f ) ) * MoonVisiblity;
			MoonAttenuation = saturate( 1.0 - MoonAttenuation);
			MoonAttenuation *= MoonAttenuation;

			// Duck moon brightness behind clouds
			MoonAttenuation *= (1.0f - amount);

			MoonLighting =  moonColor.xyz * MoonAttenuation;
			MoonCloudLighting = moonGlowTex.xyz * MoonAttenuation;

			// add moon lighting to clouds
			cloudcol.xyz *= 1.0f + MoonCloudLighting.xyz * (1.4f - amount );
			
			skyColor.xyz += MoonLighting;
		}		
	}
	skyColor.xyz += Galaxy.xyz;


	//-----------------------------------
	// fade in to sky color in distance
	//
	HALF fadeOutClouds = saturate( view.y * 4.0f - CloudFadeOut );
	amount *= fadeOutClouds;
	//-------------------------------------
	// combine sky and clouds
	//
	HALF3 color = lerp( skyColor, cloudcol, amount);
	//color *= HDRExposure;
	//color.xyz = min(HDRExposureClamp, color.xyz);
	//color.xyz = float3( abs( IN.CloudShadowTexCoord.zw) , 0.0f);
	return HALF4( color, 1.0f);
}


//--------------------------------------------------------------------
//
//	Calculates the Sky Dome and clouds
//
PS_OUTPUT ps_calculateSky_Fast(
	VS_OUTPUT_FAST IN,
	HALF4 sunColor,
	HALF4 sunCloudCol)
{
	//------------------------------
	// sky & cloud
	//------------
	//---------------------------------------
	// Calculate the cloud denisty
	//
	HALF4 noise = tex2D(PerlinNoiseSampler, IN.TexCoordAndWorldPosZ.xy);

	HALF cloudDensity = saturate(CloudThreshold * noise.x - CloudBias );
	HALF amount = pow(cloudDensity, CloudThicknessEdgeSmoothDetailScaleStrength.x) * saturate(cloudDensity * CloudThicknessEdgeSmoothDetailScaleStrength.y);
	
	//---------------------------------------
	// Calculate the cloud self shadowing
	//
	HALF shadow = saturate((CloudThreshold * noise.y - CloudBias) * CloudShadowStrength);

#	if TOPCLOUDS
		// add in a top layer without self shadowing
		HALF topCloud = noise.z;
		topCloud += TopCloudBiasDetailThresholdHeight.y;
		topCloud = saturate(TopCloudBiasDetailThresholdHeight.z * topCloud - TopCloudBiasDetailThresholdHeight.x) * IN.HighCloudTexCoordAndDistanceScalar.z;
		HALF3 topCloudColor = TopCloudColor.rgb + SunsetColor.rgb;
		topCloud *= 1.0f - amount;
#	endif // TOPCLOUDS

#	if TOPCLOUDS
		HALF3 cloudColor = (CloudColor.rgb - shadow.xxx) * TopCloudLight + SunsetColor.rgb;
		cloudColor = lerp(cloudColor, topCloudColor.rgb, topCloud);
		amount = lerp(topCloud, 1.0f, amount); 
#	else
		HALF3 cloudColor = (CloudColor.rgb - shadow.xxx) + SunsetColor.rgb;
#	endif // TOPCLOUDS
	
	//-------------------------
	// blend in sun 
	HALF3 skyColor = saturate(IN.SkyCol * saturate(sunColor.w)) +  max(sunColor.xyz, 0.0f);
	
#if NIGHTEFFECTS
	HALF3 moonLighting = 0;
	
	HALF3 galaxy = 0.0f;
	HALF4 starBright = StarFieldBrightness;
	starBright.w =4.0f;
	
	//-- add in stars
	// This compensates to equalize Daren's settings.
	HALF Stars = tex2D(StarFieldSampler, IN.StarTexCoord).x * StarFieldBrightness.x - (StarFieldBrightness.z * 0.1f);
	HALF4 GalaxyVal = tex2D(GalaxySampler, IN.GalaxyTexCoord).xyzw * starBright.yyyw;

	// add galaxy and stars
	galaxy = Stars + Stars * dot( GalaxyVal.xyz, 1.2f.xxx) + GalaxyVal.xyz;
	galaxy = saturate(galaxy);

	// and we also need the moon
	HALF2 moonTex = IN.MoonTexCoordAndWorldPosXY.xy;
	HALF2 moonTexGlow = (( IN.MoonTexCoordAndWorldPosXY.xy - 0.5f) * ( 1.0f - 0.1f) ) + 0.5f;

	HALF2 moonMaskGlow = step(1.0f, moonTexGlow);
	moonMaskGlow += step(moonTexGlow, 0.0f);
	moonTexGlow = (moonTexGlow * MoonLight.xy) + MoonLight.zw ;
	if ((moonMaskGlow.x + moonMaskGlow.y) == 0.0f)
	{
		HALF2 moonMask = ( moonTex.xy > 1.0f ) || ( moonTex.xy < 0.0f);
		moonMask = 1.0 - saturate( moonMask.x + moonMask.y );
		// rescale to frame
		moonTex =  (moonTex * MoonLight.xy) + MoonLight.zw ;
		HALF4 moonColor = tex2D( MoonSampler, moonTex ) * moonMask.x;
		moonColor.xyz *= MoonColorConstant.xyz * moonColor.w;

		// calculate mix of moon and sky
		galaxy *= 1.0f - moonColor.w;

		// could approximate with constant
		HALF moonAttenuation = dot(skyColor.xyz, HALF3(0.3f, 0.7f, 0.17f)) * MoonVisiblity;
		moonAttenuation = max(0.0f, 1.0 - moonAttenuation);
		moonAttenuation *= moonAttenuation;

		// Duck moon brightness behind clouds
		moonAttenuation *= (1.0f - amount);

		moonLighting = moonColor.xyz * moonAttenuation;
	}		
	
	skyColor.xyz += moonLighting;
	skyColor.xyz += galaxy;
#endif // NIGHTEFFECTS

	skyColor = max(skyColor, 0.0f);
	
	//-----------------------------------
	// fade in to sky color in distance
	//
	cloudColor = lerp(skyColor, cloudColor, IN.HighCloudTexCoordAndDistanceScalar.w);
	
	//-------------------------------------
	// combine sky and clouds
	//
	PS_OUTPUT OUT;
	OUT.color.rgb = lerp( skyColor, cloudColor, amount);
	OUT.color.rgb *= HDRExposure;
	OUT.color.rgb = min(HDRExposureClamp, OUT.color.rgb);
	OUT.color.a = 1;
	
	return OUT;
}


HALF4 ps_sunonly(VS_OUTPUT_FAST IN) : COLOR 
{
	HALF3 view = normalize(HALF3(IN.MoonTexCoordAndWorldPosXY.zw, IN.TexCoordAndWorldPosZ.w));	
	HALF3 sunDir = normalize(SunDirection.xyz);
	HALF4 sunColTex = HALF4( SunColor, 1.0f);
	HALF2 sunsz = CalcSunSize();
	
	// angle between the current vertex and the sun direction vector
	// we add 1 and divide by 2 to move the range from -1..1 to 0..1
	HALF angle = (dot(view, sunDir) + 1) / 2;
	
	HALF centre = smoothstep( SunCentre.x, SunCentre.y, angle ) * SunCentre.z;
	HALF4 suncolor = centre *4;

	const HALF3 lumratio = { 0.3,0.6,0.1 };
	
	// here we calculate an alpha value ...
	HALF destalpha = 1 - dot((HALF3)suncolor,lumratio);
	suncolor.a = saturate(destalpha);

	suncolor.xyz *= sunColTex.xyz;
	suncolor.xyz += pow(suncolor.xyz,4);
	suncolor *= HDRSunExposure;

	return HALF4( min(suncolor.xyz,HDRExposureClamp), 1.0f);	
}

PS_OUTPUT ps_main_fast( VS_OUTPUT_FAST IN) 
{
	HALF3 sunDir = SunDirection.xyz;
	HALF4 sunColTex = HALF4( SunColor, 1.0f);
	
	HALF3 worldPos = HALF3(IN.MoonTexCoordAndWorldPosXY.zw, IN.TexCoordAndWorldPosZ.w);
	IN.MoonTexCoordAndWorldPosXY.w = abs(IN.MoonTexCoordAndWorldPosXY.w);
	HALF4 sunColor = CalcSunColor(  SunDirection.w, sunDir, worldPos ,SunSize);

	sunColor.xyz *= sunColTex.xyz;
	sunColor.xyz += pow(sunColor.xyz,4);
	sunColor *= HDRSunExposure;

	PS_OUTPUT OUT;
	OUT = ps_calculateSky_Fast(IN, sunColor, sunColTex);
	OUT.color = HALF4(min(OUT.color.rgb, HDRExposureClamp), 1.0f);

	OUT.color = rageEncodeOpaqueColor(OUT.color.rgb);

	return OUT;
}
PS_OUTPUT ps_main_options( VS_OUTPUT IN, uniform bool doStars, uniform bool doMoon)
{
	HALF3 sunDir = SunDirection.xyz;
	HALF3 view = normalize( IN.WorldPos.xyz );
	HALF4 sunColTex = HALF4( SunColor, 1.0f);
	IN.WorldPos.y = abs(IN.WorldPos.y);
	HALF4 sunColor = CalcSunColor( SunDirection.w, sunDir, view ,SunSize);

	sunColor.xyz *= sunColTex.xyz;
	sunColor.xyz += pow(sunColor.xyz,4);
	sunColor *= HDRSunExposure;

	HALF4 sky = ps_calculateSky( IN , view, sunColor , sunColTex, doStars, doMoon );
	PS_OUTPUT OUT;
	OUT.color = HALF4( min(sky.xyz,HDRExposureClamp), 1.0f);	

	OUT.color = rageEncodeOpaqueColor(OUT.color.rgb);
	return OUT;
}
#if __XENON
//[reduceTempRegUsage(12)]
#endif
PS_OUTPUT ps_main( VS_OUTPUT IN )
{
	return ps_main_options( IN, false, false );
}
PS_OUTPUT ps_main_withmoon( VS_OUTPUT IN )
{
	return ps_main_options( IN, false, true );
}
#if __XENON
//[reduceTempRegUsage(12)]
#endif
PS_OUTPUT ps_main_withstarfield( VS_OUTPUT IN )
{
	return ps_main_options( IN, true, false );
}
PS_OUTPUT ps_main_withstarfield_withmoon( VS_OUTPUT IN )
{
	return ps_main_options( IN, true, true );
}
float4 ps_miniSky(  VS_MINIOUTPUT IN ) : COLOR
{
	float3 newpos;
	newpos.xz = ( IN.TexCoord - 0.5 ) * 2.0f;
	
	float width = 4000.0f;		// may need this exactly worked out
	float height = 4000.0f;
	float mag = dot( newpos.xz, newpos.xz ); 
	newpos.y =( 1.0 - sqrt( mag ) ) * height;
	
	float3 sunDir = SunDirection.xyz;
	float4 sunColTex =  float4( SunColor, 1.0f);
	
	newpos.xz =  mag * normalize( newpos.xz)* width;
	float3 view = normalize( newpos.xyz );

	
	float4 sunColor = CalcSunColor( SunDirection.w, sunDir, view , SunSize);
	sunColor.xyz *= sunColTex.xyz;
	sunColor.xyz += pow(sunColor.xyz,4);
	
	sunColor *= HDRSunExposure;
	sunColor.xyz = min(sunColor.xyz,HDRExposureClamp);		
	
	VS_OUTPUT	input;
	input.Pos = 0;
	input.WorldPos = newpos.xyz;
	input.TexCoord.xy = IN.TexCoord;
	input.TexCoord.z = 0;
	input.StarTexCoord = IN.TexCoord;
	input.HighCloudTexCoord= IN.TexCoord.xyx;
	input.GalaxyTexCoord= IN.TexCoord;
	input.CloudShadowTexCoord = IN.TexCoord.xyxy;
	input.SkyCol = SkyColorCalculation(view);

	return ps_calculateSky( input, view, sunColor , sunColTex, false, false);
}

//-------------------------------------------------------------
// sky blurring

struct VS_BLUR_OUTPUT
{
   float4 Pos:       POSITION;
   float2 TexCoord:   TEXCOORD0;
};

VS_BLUR_OUTPUT vs_BlurSky(float4 inPos: POSITION)
{
	VS_BLUR_OUTPUT OUT;

	OUT.Pos = float4(inPos.xy*2-1,0,1);
	OUT.TexCoord.x = inPos.x;
	OUT.TexCoord.y = 1-inPos.y;

	return OUT;
}

float4 ps_BlurSkyFast( VS_BLUR_OUTPUT IN) : COLOR
{
	
	float2	offset = float2( -0.5f, 0.5f ) * TexelSize.xx;
	float4 s1 = tex2D( SkyMapSampler, IN.TexCoord +  offset.xx );
	float4 s2 = tex2D( SkyMapSampler, IN.TexCoord +  offset.yx  );
	float4 s3 = tex2D( SkyMapSampler, IN.TexCoord +  offset.xy  );
	float4 s4 = tex2D( SkyMapSampler, IN.TexCoord +  offset.yy );	
	return ( s1 + s2 + s3 + s4 ) / 4.0f;
}
float4 ps_empty() : COLOR
{
	return 0.0f;
}

float4 ps_BlurSky( VS_BLUR_OUTPUT IN) : COLOR
{

	float4 outColor = 0.0f;//tex2D( SkyMapSampler, IN.TexCoord );
	
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( -1.5, -1.5)) * 0.25f;
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( -0.5, -1.5)) * 0.5f;
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( 0.5, -1.5))* 0.5f;
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( 1.5, -1.5)) * 0.25f;

	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( -1.5, -0.5))* 0.5f;
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( -0.5, -0.5));
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( 0.5, -0.5));
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( 1.5, -0.5))* 0.5f;

	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( -1.5,0.5))* 0.5f;
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( -0.5,0.5));
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( 0.5,0.5));
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( 1.5,0.5))* 0.5f;

	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( -1.5,1.5)) * 0.25f;
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( -0.5,1.5))* 0.5f;
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( 0.5,1.5))* 0.5f;
	outColor += _Tex2DOffset( SkyMapSampler, IN.TexCoord, float2( 1.5,1.5)) * 0.25f;
	
	return outColor * 1.0f/( 4.0f * 0.25f +  0.5f* 8.0f + 4.0f);
			
}

//--------------------------------------------------------------//

technique draw
{
   pass p0
   {
      Zenable = true;
      ZwriteEnable = false;
      Cullmode = NONE;
      ALPHABLENDENABLE = false;
      AlphaTestEnable = false;

      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

technique unlit_draw
{
   pass p0
   {
      Zenable = true;
      ZwriteEnable = false;
      Cullmode = NONE;
      ALPHABLENDENABLE = false;
      AlphaTestEnable = false;

      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

technique dpskyfront_draw
{
   pass p0
   {
      Zenable = true;
      ZwriteEnable = false;
      Cullmode = NONE;
      ALPHABLENDENABLE = false;
      AlphaTestEnable = false;

      VertexShader = compile VERTEXSHADER vs_DpFrontMain();
      PixelShader = compile PIXELSHADER ps_main();
   }
}


technique dpskyback_draw
{
   pass p0
   {
      Zenable = true;
      ZwriteEnable = false;
      Cullmode = NONE;
        ALPHABLENDENABLE = false;

      VertexShader = compile VERTEXSHADER vs_DpBackMain();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

//--------------------------------------------------------------//

technique drawMiniMe
{
   pass p0
   {
	  ZENABLE = false;
      ZWRITEENABLE = true;
      CULLMODE = NONE;
      ALPHABLENDENABLE = false;
      AlphaTestEnable = false;

      VertexShader = compile VERTEXSHADER vs_MiniSky();
      PixelShader = compile PIXELSHADER ps_miniSky();
   }
}
technique FastblurMiniMe
{
   pass p0
   {
	  ZENABLE = false;
      ZWRITEENABLE = true;
      CULLMODE = NONE;
      AlphaTestEnable = false;
      ALPHABLENDENABLE = false;

      VertexShader = compile VERTEXSHADER vs_BlurSky();
      PixelShader = compile PIXELSHADER ps_BlurSkyFast();
   }
}
technique blurMiniMe
{
   pass p0
   {
	  ZENABLE = false;
      ZWRITEENABLE = true;
      CULLMODE = NONE;
      ALPHABLENDENABLE = false;
      AlphaTestEnable = false;

      VertexShader = compile VERTEXSHADER vs_BlurSky();
      PixelShader = compile PIXELSHADER ps_BlurSky();
   }
}

technique draw_sunonly
{
   pass p0
   {
		CullMode = NONE;
  		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = true;
		BlendOp = ADD;
		SrcBlend = ONE;
        DestBlend = ONE;
        AlphaTestEnable = false;

      VertexShader = compile VERTEXSHADER vs_main_fast();
      PixelShader = compile PIXELSHADER ps_sunonly();
   }
}

#define DRAW_TECHNIQUE_1( B ) technique draw##B  \
		{\
   pass p0\
   {\
      VertexShader = compile VERTEXSHADER vs_main();\
	  PixelShader = compile PIXELSHADER ps_main##B();\
   }\
}


#define DRAW_TECHNIQUE_2( B,C ) technique draw##B##C  \
		{\
   pass p0\
   {\
      VertexShader = compile VERTEXSHADER vs_main();\
	  PixelShader = compile PIXELSHADER ps_main##B##C();\
   }\
}

DRAW_TECHNIQUE_1( _withmoon )
DRAW_TECHNIQUE_1( _withstarfield )
DRAW_TECHNIQUE_2( _withstarfield, _withmoon )



technique draw_stencil
{
   pass p0
   {
      VertexShader = compile VERTEXSHADER vs_stencil();
#if !__XENON
      PixelShader = compile PIXELSHADER ps_empty();
#else
		PixelShader = NULL;
#endif
   }
}
