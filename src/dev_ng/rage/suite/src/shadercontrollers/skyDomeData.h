// 
// shadercontrollers/skyDomeData.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SHADERCONTROLLERS_SKY_DOME_DATA_H
#define SHADERCONTROLLERS_SKY_DOME_DATA_H

namespace rage
{
	class EmbeddedModel;

	EmbeddedModel*	CreateSkyDomeModel();
}

#endif
