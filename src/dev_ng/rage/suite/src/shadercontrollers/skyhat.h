// 
// shadercontrollers/skyhat.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SKY_HAT_CONTROLLERS
#define SKY_HAT_CONTROLLERS


#include "shadercontrollers\embeddedmodel.h"
#include "shadercontrollers\grcphenomna.h"
#include "shadercontrollers\grcproceduralphenomna.h"

#include "atl/ptr.h"

// remove skyhat offset as only used in one team currently

#ifndef SKYHAT_USE_OFFSET
#define SKYHAT_USE_OFFSET HACK_GTA4
#endif


namespace rage
{
	
//----------------------------------------------------------------------------
// begin vertices renderer stuff
//-------------------------------------------------------------------------

// PURPOSE : wraps begin vertices calls so make it easier to use a vertex format
// and generate vertices onto the command buffer
//
template<class VTX>
class ImmediateRenderer
{
public:
	ImmediateRenderer() { m_VertexDeclaration = 0; }
	~ImmediateRenderer() { Release(); }

	// PURPOSE : sets up the vertex declaration 
	//
	void Init()
	{
		grcVertexElement vertexElements[8];
		int amt = 16;
		VTX::GetVertexDecl( vertexElements, amt );
		Assert( amt <= 8 );

#if __ASSERT
		int size = 0;
		for (int i = 0; i < amt; i++ )
		{
			size += vertexElements[i].size;
		}
		Assert( size == sizeof( VTX) );
#endif
		// Set up the vertex declaration.
		m_VertexDeclaration = GRCDEVICE.CreateVertexDeclaration(vertexElements, amt);
		Assertf(m_VertexDeclaration,  "ImmediateRenderer::InitVertexDeclaration< %d> has never been called. But it should have been.", VTX::Name() );
	}
	void Release()
	{
		LastSafeRelease( m_VertexDeclaration );
	}

	// PURPOSE : Sets up the vertex declaration and everything ready for rendering the verts.
	//
	RETURNCHECK(VTX*) Begin( grcDrawMode drawMode,  int amt ) 
	{
		if (!amt )
		{
			return NULL;
		}
		if ( m_VertexDeclaration == 0 )
		{
			Init();
		}
		Assert( grcEffect::IsInDraw() );

		GRCDEVICE.SetVertexDeclaration(m_VertexDeclaration );

		VTX* drawBuffer;
		// Get the pointer into the ring buffer.
		if ((drawBuffer = (VTX *) GRCDEVICE.BeginVertices(drawMode, amt , sizeof(VTX))) == NULL)
		{
			Warningf("ImmediateRenderer for %s failed - could not allocate vertices", VTX::Name());
			return NULL;
		}
		m_endPoint =  drawBuffer + amt;
		return drawBuffer;
	}

	// PURPOSE : Called after rendering all the vertices
	// checks the pointer passed in points to the end of the block of memory
	//
	void End(const VTX* endptr) const
	{
		Assert( endptr == m_endPoint );
		GRCDEVICE.EndVertices(endptr);
	}
private:
	grcVertexDeclaration*	m_VertexDeclaration;
	VTX*					m_endPoint;

	const ImmediateRenderer& operator=( ImmediateRenderer& other );
	ImmediateRenderer( ImmediateRenderer& other );
};


// PURPOSE:
//  Vertex format for rendering moon circle onto sky
//
struct SkyVtx
{
	Vector4 position;
	//Vector2 uv;			// UV coordinates in xy
	//float star[2];

	// PURPOSE: Set up this vertex.
	static void GetVertexDecl( grcVertexElement* vel, int& amt )
	{
		vel[0].type = grcVertexElement::grcvetPosition;
		vel[0].size = 16;
		vel[0].format = grcFvf::grcdsFloat4;

		//vel[1].type = grcVertexElement::grcvetTexture;
		//vel[2].channel = 0;
		//vel[1].size = 8;
		//vel[1].format = grcFvf::grcdsFloat2;

		//vel[2].type = grcVertexElement::grcvetTexture;
		//vel[2].channel = 1;
		//vel[2].size = 8;
		//vel[2].format = grcFvf::grcdsFloat2;
		amt = 1;
	}
	static const char* Name() { return "Moon Vertex"; }

	// sets the value
	void Write( Vector3::Param p, const Vector2& /*uvs*/)
	{
		position.SetVector3( p );
		position.w = 1.0f;
		//uv = uvs;
		//star[0] = star[1] = 0.0f;
	}
};

	//	PURPOSE:
	//		Settings which are passed into external rountines to be set so as to control
	//		the per frame changes in the sky hat controller
	//
	struct SkyHatPerFrameSettings
	{
		//Sky
		Vector3		m_skyColor;
		Vector3		m_AzimuthColor;
		Vector3		m_AzimuthColorEast;
		Vector4		m_SunsetColor;
		float		m_AzimuthHeight;

		float		m_AzimuthStrength;
		Vector2		m_SunAxias;

		// clouds
		Vector3		m_CloudColor;
		float		m_CloudShadowStrength;
		float		m_CloudThreshold;
		float		m_CloudBias;
		float		m_CloudShadowOffset;
		float		m_CloudInscatteringRange;
		float		m_CloudEdgeSmooth;
		float		m_DetailScale;
		float		m_DetailStrength;
		float		m_CloudThickness;
		float		m_CloudWarp;
		float		m_CloudFadeOut;

		// top cloud layer
		float		m_TopCloudBias;
		float		m_TopCloudDetail;
		float		m_TopCloudThreshold;
		float		m_TopCloudHeight;
		Vector3		m_TopCloudColor;
		float		m_TopCloudLight;

		// Sun
		Vector3		m_SunColor;
		float		m_UnderLightStrength;

		// cloud tex gen
		float		m_CloudSpeed;

		// star field opacity
		float		m_StarFieldBrightness;
		float		m_StarFieldThreshold;
		
		// moon settings
		float		m_MoonBrightness;
		Vector3		m_MoonColor;
		float		m_MoonGlow;
		float		m_MoonVisiblity;

		float		m_SunCentreStart;
		float		m_SunCentreEnd;
		float		m_SunCentreIntensity;
		float		m_SunOuterSize;

		void Clear() {};
		SkyHatPerFrameSettings&	operator*=( float t  );
		SkyHatPerFrameSettings&	operator+=( const SkyHatPerFrameSettings& other );
	};

	SkyHatPerFrameSettings operator+(  const SkyHatPerFrameSettings& a , const SkyHatPerFrameSettings& b );
	SkyHatPerFrameSettings operator-(  const SkyHatPerFrameSettings& a , const SkyHatPerFrameSettings& b );
	SkyHatPerFrameSettings operator*(  const SkyHatPerFrameSettings& a , const float& t );
	SkyHatPerFrameSettings operator*(  float t, const SkyHatPerFrameSettings& a );


	
	Vector4 ConvertPhaseToTextureCoords( int phaseNumber );

	//
	//	PURPOSE
	//		Sky Hat controller controls the settings to be feed into the shader and also
	//		gives an interface for updating the per frame constants with a user defined routine.
	//
	class SkyhatMiniNoise : public ShaderFragment
	{
		struct SkyhatMiniNoiseVarCache : public VarCache
		{
			grcEffectVar	m_perlinNoiseID;
			grcEffectVar	m_highDetailNoiseMapID;
			grcEffectVar	m_highDetailNoiseBumpMapID;
			grcEffectVar	m_timeOfDayID;

			// constants
			grcEffectVar	m_sunDirectionID;
			grcEffectVar	m_skyColorID;
			grcEffectVar	m_AzimuthID;
			grcEffectVar	m_CloudColorID;
			grcEffectVar	m_SunColorID;
			grcEffectVar	m_CloudShadowStrengthID;
			grcEffectVar	m_UnderLightStrengthID;
			grcEffectVar	m_SunAxiasID;
			grcEffectVar	m_AzimuthColorEastID;

			grcEffectVar	m_SunCentreID;

			grcEffectVar	m_HDRExposureVar;
			grcEffectVar	m_HDRSunExposureVar;
			grcEffectVar	m_HDRExposureClampVar;

			//Sky
			grcEffectVar	m_SunsetColorID;
			grcEffectVar	m_AzimuthStrengthID;
			grcEffectVar	m_AzimuthHeightID;

			// clouds
			grcEffectVar	m_CloudThresholdID;
			grcEffectVar	m_CloudBiasID;
			grcEffectVar	m_CloudShadowOffsetID;
			grcEffectVar	m_CloudInscatteringRangeID;

			grcEffectVar	m_CloudFadeOutID;
			grcEffectVar	m_CloudThicknessEdgeSmoothDetailScaleStrengthID;

			//toplevel clouds
			grcEffectVar	m_TopCloudBiasDetailThresholdHeightID;
			grcEffectVar	m_TopCloudColorID;
			grcEffectVar	m_TopCloudLightID;
			
			
			// night
			grcEffectVar	m_StarFieldBrightnessID;
			grcEffectVar	m_StarFieldTexID;
			grcEffectVar	m_StarFieldThresholdID;
			grcEffectVar	m_GalaxyTexID;
			grcEffectVar	m_StarFieldUVRepeatID;
			grcEffectVar	m_GalaxyOffsetID;

			// moon
			grcEffectVar	m_MoonTexPosID;
			grcEffectVar	m_MoonLightID;
			grcEffectVar	m_MoonTexID;
			grcEffectVar	m_MoonColorID;
			grcEffectVar	m_MoonGlowID;
			grcEffectVar	m_MoonGlowTexID;
			grcEffectVar	m_MoonVisiblityID;

			// fog		
			grcEffectVar m_FogMinColorId;
			grcEffectVar m_FogMaxColorId;
			grcEffectVar m_FogParamsId;
			grcEffectVar m_ParticleFogParamsId;
			grcEffectVar m_FogMapId;

			grcEffectVar  m_SunSizeId;

			grcEffectVar m_MoonPositionID;
			grcEffectVar m_MoonXVectorID;
			grcEffectVar m_MoonYVectorID;
		
			grcEffectVar m_DetailOffsetID;

			void CacheEffectVars( grmShader & effect )
			{
				m_perlinNoiseID = effect.LookupVar( "PerlinNoiseMap" , IsPedantic());
				m_highDetailNoiseMapID = effect.LookupVar( "HighDetailNoiseMap", IsPedantic() );
				m_highDetailNoiseBumpMapID = effect.LookupVar( "HighDetailNoiseBumpMap", IsPedantic() );
				m_timeOfDayID = effect.LookupVar( "TimeOfDay" , IsPedantic() );

				m_sunDirectionID =  effect.LookupVar( "SunDirection", IsPedantic());
				m_SunCentreID = effect.LookupVar("SunCentre", IsPedantic() );

				m_skyColorID =  effect.LookupVar( "SkyColor", IsPedantic());
				m_AzimuthID =  effect.LookupVar( "AzimuthColor", IsPedantic());
				m_AzimuthColorEastID =  effect.LookupVar( "AzimuthColorEast", IsPedantic());
				m_SunAxiasID =  effect.LookupVar( "SunAxias", IsPedantic());
				m_AzimuthHeightID = effect.LookupVar( "AzimuthHeight", IsPedantic() );
				

				m_CloudColorID =  effect.LookupVar( "CloudColor", IsPedantic());
				m_SunColorID = effect.LookupVar("SunColor", IsPedantic());
				m_CloudShadowStrengthID = effect.LookupVar("CloudShadowStrength", IsPedantic());
				m_UnderLightStrengthID = effect.LookupVar("UnderLightStrength", IsPedantic());
				// hdr exposure
				m_HDRExposureVar			= effect.LookupVar("HDRExposure", IsPedantic());
				m_HDRSunExposureVar			= effect.LookupVar("HDRSunExposure", IsPedantic());
				m_HDRExposureClampVar			= effect.LookupVar("HDRExposureClamp", IsPedantic());

				//Sky
				m_SunsetColorID = effect.LookupVar("SunsetColor", IsPedantic());
				m_AzimuthStrengthID = effect.LookupVar("AzimuthStrength", IsPedantic());

				// clouds
				m_CloudThresholdID = effect.LookupVar("CloudThreshold", IsPedantic());
				m_CloudBiasID = effect.LookupVar("CloudBias", IsPedantic());
				m_CloudShadowOffsetID = effect.LookupVar("CloudShadowOffset", IsPedantic());
				m_CloudInscatteringRangeID = effect.LookupVar("CloudInscatteringRange", IsPedantic());
				m_CloudFadeOutID = effect.LookupVar( "CloudFadeOut", IsPedantic() );

				m_CloudThicknessEdgeSmoothDetailScaleStrengthID = effect.LookupVar( "CloudThicknessEdgeSmoothDetailScaleStrength", IsPedantic() );

				m_DetailOffsetID = effect.LookupVar( "DetailOffset", IsPedantic());

				// top level clouds
				m_TopCloudBiasDetailThresholdHeightID = effect.LookupVar("TopCloudBiasDetailThresholdHeight" , IsPedantic() );
				m_TopCloudColorID = effect.LookupVar( "TopCloudColor", IsPedantic() );
				m_TopCloudLightID = effect.LookupVar( "TopCloudLight", IsPedantic() );

				// night
				m_StarFieldBrightnessID = effect.LookupVar( "StarFieldBrightness", IsPedantic() );
				m_StarFieldTexID = effect.LookupVar( "StarFieldTexture", IsPedantic() );
				m_GalaxyTexID = effect.LookupVar("GalaxyTexture", IsPedantic() );
				m_StarFieldUVRepeatID = effect.LookupVar("StarFieldUVRepeat", IsPedantic() );
				m_GalaxyOffsetID = effect.LookupVar("GalaxyOffset", IsPedantic() );
				m_StarFieldThresholdID = effect.LookupVar("StarThreshold", IsPedantic() );

				// moon
				m_MoonTexPosID =  effect.LookupVar( "MoonTexPosition", IsPedantic() );
				m_MoonLightID =  effect.LookupVar( "MoonLight", IsPedantic() );
				m_MoonTexID = effect.LookupVar("MoonTexture", IsPedantic() );
				m_MoonColorID = effect.LookupVar("MoonColorConstant", IsPedantic() );
				m_MoonGlowTexID = effect.LookupVar("MoonGlowTexture", IsPedantic() );
				m_MoonGlowID = effect.LookupVar("MoonGlow", IsPedantic() );
				m_MoonVisiblityID = effect.LookupVar("MoonVisiblity", IsPedantic() );

				m_MoonPositionID = effect.LookupVar("MoonPosition", IsPedantic() );
				m_MoonXVectorID = effect.LookupVar("MoonXVector", IsPedantic() );
				m_MoonYVectorID = effect.LookupVar("MoonYVector", IsPedantic() );

				//fogging
				m_FogMinColorId = effect.LookupVar("FogMinColor", false);	
				m_FogMaxColorId = effect.LookupVar("FogMaxColor", false);	
				m_FogParamsId = effect.LookupVar("FogParams", false);	
				m_ParticleFogParamsId = effect.LookupVar("ParticleFogParams", false);
				m_FogMapId = effect.LookupVar("FogMap", false);

				m_SunSizeId = effect.LookupVar("SunSize", false );
			}
		};
	public:
		typedef atFunctor2< void, float, SkyHatPerFrameSettings& >  SettingsCallback;

		SkyhatMiniNoise()
			: m_timeOfDay(0.0f)        
			, m_HDRExposure( 1.0f )
#if HACK_GTA4
			, m_HDRSunExposure( 1.0f, 0.0f, 0.0f )
#else // HACK_GTA4
			, m_HDRSunExposure( 1.0f )
#endif // HACK_GTA4
			, m_HDRExposureClamp( 2.0f, 2.0f, 2.0f )
			, m_setupSettings( false )
			, m_StarFieldRepeat( 8.0f )
			, m_MoonSize( 4.0f)
			, m_MoonPhase( 6 )
			, m_MoonPosition( 0.0f, 0.707f, 0.707f )
			, m_GalaxyOffset( 0.0f, 0.0f )
			, m_FogMinColor(0.0f, 0.0f, 0.0f, 0.0f)
			, m_FogMaxColor(0.0f, 0.0f, 0.0f, 0.0f)
			, m_FogParams(0.0f, 0.0f, 0.0f, 0.0f)
			, m_ParticleFogParams(0.0f, 0.0f, 0.0f, 0.0f)
			, m_FogMapTex(NULL)
#if HACK_GTA4
			,m_detailSpeed( 0.003f)
#else // HACK_GTA4
			,m_detailSpeed( 0.01f)
#endif // HACK_GTA4
			,m_sunSetBump(1.0f)
		{
		}

		void SetSettings( const SkyHatPerFrameSettings& settings )
		{
			m_setupSettings = true;
			m_settings = settings;
		}
		void SetSettingsCallback( SettingsCallback& functor )
		{
			m_PerFrameSettingsFunctor = functor;
		}

		SkyHatPerFrameSettings &GetSettings() {return m_settings;}
		
		
		~SkyhatMiniNoise()
		{
		}

		//////////////////////////////////////////////////////////////////////////
		//
		//	PURPOSE
		//		Allows game code to explicitly load particular SkyhatMiniNoise settings.
		//
		bool LoadSettings(char* filename)
		{
			return LoadFile(filename);
		}

		bool UsesFragment( grmShader& effect )
		{
			return ( effect.LookupVar( "HighDetailNoiseMap" , false ) != 0 );
		}

		VarCache* CreateVariableCache()	{ return rage_new SkyhatMiniNoiseVarCache; }

#if HACK_GTA4
		void SetSunExposure( const Vector3 & v )		{ m_HDRSunExposure = v; }
#else // HACK_GTA4
		void SetSunExposure( float v )		{ m_HDRSunExposure = v; }
#endif // HACK_GTA4
		void SetExposure( float v)			{ m_HDRExposure = v; }
		void SetExposureClamp( const Vector3 &v )		{ m_HDRExposureClamp = v; }
		
		void SetFogMinColor(const Vector4 &v) { m_FogMaxColor = v; }
		void SetFogMaxColor(const Vector4 &v) { m_FogMinColor = v; }
		void SetFogParams(const Vector4 &v) { m_FogParams = v; }
		void SetParticleFogParams(const Vector4 &v) {m_ParticleFogParams = v;}
		void SetFogMapTex(grcTexture *tex) { m_FogMapTex = tex; }
		
#if HACK_GTA4
		const Vector3&  GetSunExposure() const			{ return m_HDRSunExposure; }
#else // HACK_GTA4
		const float GetSunExposure() const			{ return m_HDRSunExposure; }
#endif // HACK_GTA4
		float GetExposure() const					{ return m_HDRExposure; }
		const Vector3 &GetExposureClamp() const		{ return m_HDRExposureClamp; }

		void SetDefaults(  SkyhatMiniNoiseVarCache* var, grmShader& effect,  SkyHatPerFrameSettings& settings )
		{	
			//Sky
			effect.GetVar( var->m_SunsetColorID , settings.m_SunsetColor );
			effect.GetVar( var->m_AzimuthStrengthID , settings.m_AzimuthStrength);

			// clouds
			effect.GetVar( var->m_CloudThresholdID , settings.m_CloudThreshold);
			effect.GetVar( var->m_CloudBiasID , settings.m_CloudBias);
			effect.GetVar( var->m_CloudShadowOffsetID , settings.m_CloudShadowOffset);
			effect.GetVar( var->m_CloudInscatteringRangeID , settings.m_CloudInscatteringRange);
		
			effect.GetVar( var->m_StarFieldBrightnessID , settings.m_StarFieldBrightness);
			effect.GetVar( var->m_TopCloudColorID, settings.m_TopCloudColor );
			effect.GetVar( var->m_TopCloudLightID, settings.m_TopCloudLight );
		}
		void SetupShader( grmShader& effect, VarCache* cVars ) 
		{
			FastAssert( cVars );
			SkyhatMiniNoiseVarCache* var = reinterpret_cast<SkyhatMiniNoiseVarCache*>(cVars);

			effect.SetVar( var->m_perlinNoiseID, m_pPerlinNoiseRT );
			effect.SetVar( var->m_highDetailNoiseMapID, m_pHighDetailNoiseTex );
			effect.SetVar( var->m_highDetailNoiseBumpMapID, m_pHighDetailNoiseBumpTex );

			effect.SetVar( var->m_timeOfDayID, m_timeOfDay );

			if ( !m_setupSettings )
			{
				SetDefaults( var, effect, m_settings );
				m_PerFrameSettingsFunctor( m_timeOfDay,  m_settings  );
			}

			Vector4 sunDir;
			sunDir.SetVector3( m_SunDirection);
			sunDir.w = fabsf( m_SunDirection.y );

			effect.SetVar( var->m_sunDirectionID, sunDir );
			effect.SetVar( var->m_skyColorID, m_settings.m_skyColor );
			effect.SetVar( var->m_AzimuthID, m_settings.m_AzimuthColor );
			effect.SetVar( var->m_AzimuthHeightID, m_settings.m_AzimuthHeight );

			Vector4 cloudCol( m_settings.m_CloudColor.x,  m_settings.m_CloudColor.y, m_settings.m_CloudColor.z, 1.0 );
			effect.SetVar( var->m_CloudColorID,  cloudCol);

			effect.SetVar( var->m_SunColorID, m_settings.m_SunColor );
			effect.SetVar( var->m_CloudShadowStrengthID, m_settings.m_CloudShadowStrength );
			effect.SetVar( var->m_UnderLightStrengthID, m_settings.m_UnderLightStrength );
			effect.SetVar( var->m_HDRExposureVar, m_HDRExposure );
			effect.SetVar( var->m_HDRSunExposureVar, m_HDRSunExposure );
			effect.SetVar( var->m_HDRExposureClampVar, m_HDRExposureClamp );

			// Sun
			Vector3	sunCentre( m_settings.m_SunCentreStart * 0.1f + 0.9f, m_settings.m_SunCentreEnd * 0.1f + 0.9f, m_settings.m_SunCentreIntensity * 8.0f * 4.0f);

			Vector3 sunCentre2 = sunCentre;
			sunCentre2.y = 1.0f/( sunCentre.y - sunCentre.x );
			sunCentre2.x = -sunCentre.x * sunCentre2.y;

			effect.SetVar( var->m_SunCentreID, sunCentre2 );

			//Sky
			Vector4 sunSetColor = Vector4(m_settings.m_SunsetColor.x,m_settings.m_SunsetColor.y, m_settings.m_SunsetColor.z, 1.0f);
			sunSetColor *= m_settings.m_UnderLightStrength;
		
			effect.SetVar( var->m_SunsetColorID , sunSetColor );
			effect.SetVar( var->m_AzimuthStrengthID , m_settings.m_AzimuthStrength);

			effect.SetVar( var->m_AzimuthColorEastID , m_settings.m_AzimuthColorEast );
			effect.SetVar( var->m_SunAxiasID , m_settings.m_SunAxias);

			float detailSpeed = m_settings.m_CloudSpeed * m_timeOfDay * m_detailSpeed;
			effect.SetVar( var->m_DetailOffsetID, Vector2(  detailSpeed,detailSpeed ));

			
			// clouds
			effect.SetVar( var->m_CloudThresholdID , m_settings.m_CloudThreshold);
			effect.SetVar( var->m_CloudBiasID , m_settings.m_CloudBias);
			effect.SetVar( var->m_CloudShadowOffsetID , m_settings.m_CloudShadowOffset);
			effect.SetVar( var->m_CloudInscatteringRangeID , m_settings.m_CloudInscatteringRange);

			//effect.SetVar( var->m_CloudEdgeSmoothID , 1.0f/m_settings.m_CloudEdgeSmooth);
			//effect.SetVar( var->m_DetailScaleID , m_settings.m_DetailScale);
			//effect.SetVar( var->m_DetailStrengthID , m_settings.m_DetailStrength);

			effect.SetVar( var->m_CloudFadeOutID, m_settings.m_CloudFadeOut );

			effect.SetVar( var->m_CloudThicknessEdgeSmoothDetailScaleStrengthID,
							Vector4( m_settings.m_CloudThickness, 1.0f/m_settings.m_CloudEdgeSmooth, m_settings.m_DetailScale, m_settings.m_DetailStrength  ) );

			//toplevel clouds
			effect.SetVar( var->m_TopCloudBiasDetailThresholdHeightID, 
						Vector4( m_settings.m_TopCloudBias, m_settings.m_TopCloudDetail, m_settings.m_TopCloudThreshold, m_settings.m_TopCloudHeight ));
			effect.SetVar( var->m_TopCloudColorID, m_settings.m_TopCloudColor + m_settings.m_SunsetColor.GetVector3() );
			effect.SetVar( var->m_TopCloudLightID, ( m_settings.m_TopCloudLight  + 1.0f) );

			// night
			Vector4 StarScale;
			StarScale.x = m_settings.m_StarFieldBrightness  * 3.0f * m_settings.m_StarFieldBrightness;
			StarScale.z  =  m_settings.m_StarFieldThreshold * StarScale.x;
			StarScale.y = ( 0.3f + 0.5f ) * m_settings.m_StarFieldBrightness * 0.3f;
			StarScale.w = 4.0f;

			effect.SetVar( var->m_StarFieldBrightnessID , StarScale); //m_settings.m_StarFieldBrightness);
			
			effect.SetVar( var->m_StarFieldTexID , m_pStarFieldTex );
			effect.SetVar( var->m_GalaxyTexID, m_pGalaxyTex);
			
			effect.SetVar( var->m_StarFieldUVRepeatID, m_StarFieldRepeat );
			effect.SetVar( var->m_GalaxyOffsetID, m_GalaxyOffset );
			effect.SetVar( var->m_StarFieldThresholdID, m_settings.m_StarFieldThreshold);
			effect.SetVar( var->m_MoonVisiblityID, m_settings.m_MoonVisiblity );

			// moon
			m_MoonPosition.Normalize();
			Vector3 newView = m_MoonPosition;
			newView.y = newView.y * 1.5f + 0.4f;
			newView.Normalize();

			Vector4 MoonTexPos;
			MoonTexPos.x = 100.0f/(m_MoonSize );
			MoonTexPos.y = 100.0f/(m_MoonSize );
			MoonTexPos.z= asin( newView.x)/3.1473f + 0.5f;
			MoonTexPos.w= asin( newView.z)/3.1473f + 0.5f;

			Vector4 MoonLight( m_settings.m_MoonColor.x * m_settings.m_MoonBrightness, 
							m_settings.m_MoonColor.y * m_settings.m_MoonBrightness, 
							m_settings.m_MoonColor.z * m_settings.m_MoonBrightness, 
							m_settings.m_MoonBrightness );

			effect.SetVar( var->m_MoonTexPosID, MoonTexPos);
			effect.SetVar( var->m_MoonLightID, ConvertPhaseToTextureCoords( m_MoonPhase ) );
			effect.SetVar( var->m_MoonColorID, MoonLight );
			effect.SetVar( var->m_MoonGlowID, m_settings.m_MoonGlow );
			

			Vector3 mpos;
			Vector3 mX;
			Vector3 mY;

		
			GetMoonPositionAndTangents( mpos, mX, mY );
			mpos -= ( mX + mY ) * 1.0f;

			effect.SetVar( var->m_MoonPositionID, mpos );
			effect.SetVar( var->m_MoonXVectorID, mX );
			effect.SetVar( var->m_MoonYVectorID, mY );

			effect.SetVar( var->m_MoonTexID, m_MoonDiffuseTex );
			effect.SetVar( var->m_MoonGlowTexID, m_MoonGlowTex );

			if(var->m_FogMinColorId != grcevNONE)
				effect.SetVar(var->m_FogMinColorId, m_FogMinColor);

			if(var->m_FogMaxColorId != grcevNONE)
				effect.SetVar(var->m_FogMaxColorId, m_FogMaxColor);

			if(var->m_FogParamsId != grcevNONE)
				effect.SetVar(var->m_FogParamsId, m_FogParams);

			if(var->m_ParticleFogParamsId != grcevNONE)
				effect.SetVar(var->m_ParticleFogParamsId, m_ParticleFogParams);

			if(var->m_FogMapId != grcevNONE)
				effect.SetVar(var->m_FogMapId, m_FogMapTex);

			effect.SetVar( var->m_SunSizeId, 1.0f + m_settings.m_SunOuterSize );

		}

		void GetMoonPositionAndTangents( Vector3& mpos, Vector3& mX, Vector3& mY ) const
		{
			mpos = m_MoonPosition;

			Vector3 a;
			Vector3 u;
#if HACK_GTA4
			Vector3 up = ZAXIS;
#else // HACK_GTA4
			Vector3 up = YAXIS;
#endif // HACK_GTA4
			a.Cross( mpos, up );
			a.Normalize();
			u.Cross( a,mpos );
			u.Normalize();
			a.Cross( mpos, u );
			a.Normalize();

			float radius = 32.0f/GetMoonSize() ;
			//mpos *= 500.0f;

			mX = a * radius;
			mY = u * radius;
		}


		void SetupShaderPass() 
		{}

		// public interface
		void SetTimeOfDay( float timeOfDay ) { m_timeOfDay = timeOfDay; }
		float GetTimeOFDay() const { return m_timeOfDay; }

		void SetPerlinNoiseRT( const grcRenderTarget *pPerlinNoiseRT ) { m_pPerlinNoiseRT = pPerlinNoiseRT; }
		const grcRenderTarget* GetPerlinNoiseRT() const { return m_pPerlinNoiseRT; ; }

		const grcTexture* GetHighDetailNoiseTex() const { return m_pHighDetailNoiseTex; }
		const grcTexture* GetHighDetailNoiseBumpTex() const { return m_pHighDetailNoiseBumpTex; }
		const grcTexture* GetStarFieldTex() const { return m_pStarFieldTex ; }


		void SetHighDetailNoiseBumpTex( atPtr<grcTexture> tex )		{ m_pHighDetailNoiseBumpTex = tex; }
		void SetHighDetailNoiseTex( atPtr<grcTexture> tex )			{ m_pHighDetailNoiseTex = tex; }
		void SetStarFieldTex( atPtr<grcTexture> tex )				{ m_pStarFieldTex = tex; }
		void SetGalaxyTex( atPtr<grcTexture> tex )					{ m_pGalaxyTex = tex; }
		void SetMoonTex(  atPtr<grcTexture> tex )					{ m_MoonDiffuseTex = tex; }
		void SetMoonGlowTex(  atPtr<grcTexture> tex )				{ m_MoonGlowTex = tex; }


		void SetMoonPosition( const Vector3& pos )	{ m_MoonPosition = pos; }
		Vector3 GetMoonPosition() { return m_MoonPosition; }
		void SetGalaxyOffset( const Vector2& off )	{ m_GalaxyOffset = off; }

		void SetSunDirection( const Vector3& pos )	{ m_SunDirection = pos; }
		Vector3 GetSunDirection() { return m_SunDirection; }

		void SetStarFieldRepeat( float starRepeat ) { m_StarFieldRepeat = starRepeat; }
		void SetDetailRatioSpeed( float detailRatioSpeed ) { m_detailSpeed = detailRatioSpeed; }
		void SetMoonSize( float mSize ) { m_MoonSize = mSize; }
		void SetMoonPhase( int mPhase ) { m_MoonPhase = mPhase; }
		float GetMoonSize() const { return m_MoonSize; }

#if HACK_GTA4
		int GetMoonPhase() { return m_MoonPhase; }
#endif // HACK_GTA4
		float GetSunSetBump() const { return m_sunSetBump; }
		// resourcing
		SkyhatMiniNoise(datResource& rsc);
		DECLARE_PLACE(SkyhatMiniNoise);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		// --
		PAR_PARSABLE;
		private:
		
			const grcRenderTarget* m_pPerlinNoiseRT;
			atPtr<grcTexture>		m_pHighDetailNoiseTex;
			atPtr<grcTexture>		m_pHighDetailNoiseBumpTex;
			atPtr<grcTexture>		m_pStarFieldTex;
			atPtr<grcTexture>		m_pGalaxyTex;
			atPtr<grcTexture>		m_MoonDiffuseTex;
			atPtr<grcTexture>		m_MoonGlowTex;

			float			m_timeOfDay;

			float			m_HDRExposure;
#if HACK_GTA4
			Vector3			m_HDRSunExposure;
#else // HACK_GTA4
			float			m_HDRSunExposure;
#endif // HACK_GTA4
			Vector3			m_HDRExposureClamp;
			float			m_StarFieldRepeat;
			float			m_MoonSize;
			int				m_MoonPhase;
			Vector3			m_MoonPosition;
			Vector2			m_GalaxyOffset;
			Vector3			m_SunDirection;
			float			m_detailSpeed;
			float			m_sunSetBump;
			
			Vector4 m_FogMaxColor;
			Vector4 m_FogMinColor;
			Vector4 m_FogParams;
			Vector4 m_ParticleFogParams;
			grcTexture *m_FogMapTex;
			
		
			atFunctor2< void, float, SkyHatPerFrameSettings& > m_PerFrameSettingsFunctor;
	
			SkyHatPerFrameSettings			m_settings;
			bool							m_setupSettings;
		};

//
//	PURPOSE
//	Updates the skyhat per frame settings procedurally using just the timeOfDay
//

class SkyDomeProceduralControl
{
public:
	struct SkyStruct 
	{
		virtual ~SkyStruct() {}
		Vector3 m_gNightSkyColor;
		Vector3 m_gDaySkyColor;
		Vector3 m_gSunsetColor;
		float	m_AzimuthHeight;

		float		m_SunCentreStart;
		float		m_SunCentreEnd;
		float		m_SunCentreIntensity;
		float		m_SunOuterSize;
		PAR_PARSABLE;
	};

	
	struct TopStruct
	{
		virtual ~TopStruct() {}
		float	m_TopCloudBias;
		float	m_TopCloudDetail;
		float	m_TopCloudThreshold;
		float   m_TopCloudHeight;
		PAR_PARSABLE;

	};
	struct CloudStruct
	{
		virtual ~CloudStruct() {}
		Vector3 m_gCloudColor;
		Vector3 m_NightBounceColor;
		float	m_gCloudShadowStrength;
		float	m_CloudThreshold;
		float	m_CloudBias;
		float	m_CloudShadowOffset;
		float	m_CloudInscatteringRange;
		float	m_CloudEdgeSmooth;
		float	m_CloudThickness;
		float	m_CloudSpeed;
		float	m_DetailScale;
		float   m_DetailStrength;
		float   m_CloudWarp;
		float	m_CloudFadeOut;
		TopStruct	top;
		PAR_PARSABLE;
	};

	struct LightStruct
	{
		virtual ~LightStruct() {}
		float	m_LightningPulseFrequency;
		float	m_LightningPulseIntensity;
		PAR_PARSABLE;
	};

	struct NightStruct
	{
		virtual ~NightStruct() {}
		float		m_StarBlinkFrequency;
		float		m_StarBlinkIntensity;
		float		m_StarIntensity;
		float		m_StarFieldThreshold;
		// moon
		float		m_MoonSize;
		float		m_MoonBrightness;
		int			m_MoonPhase;
		Vector4		m_MoonTex;
		Vector3		m_MoonColor;
		float		m_MoonGlow;
		float		m_MoonVisiblity;
		PAR_PARSABLE;
	};	


public:
	SkyDomeProceduralControl() 
	{
		sky.m_gNightSkyColor = Vector3( 0.00f, 0.04f, 0.08f );
		sky.m_gDaySkyColor = Vector3( 0.62f, 0.81f, 1.00f );
		sky.m_gSunsetColor = Vector3(1.00f, 0.51f, 0.38f );
		sky.m_AzimuthHeight = 12.0f;

		sky.m_SunCentreStart = 0.6f;
		sky.m_SunCentreEnd = 0.9f;
		sky.m_SunCentreIntensity = 0.0f;
		sky.m_SunOuterSize = 0.0f;

		cloud.m_gCloudColor = Vector3( 0.93f, 1.00f, 1.00f );
		cloud.m_NightBounceColor = Vector3( 0.2f, 0.1f, 0.0f );
		cloud.m_gCloudShadowStrength =  0.4f ;
		cloud.m_CloudThreshold =  1.80f ;
		cloud.m_CloudBias =  0.8f ;
		cloud.m_CloudShadowOffset =  0.12f;
		cloud.m_CloudInscatteringRange =  1.0f ;
		cloud.m_CloudEdgeSmooth =   0.1f ;
		cloud.m_CloudThickness =  0.35f ;
		cloud.m_DetailScale =  16.0f;
		cloud.m_DetailStrength =   0.15f ;
		cloud.top.m_TopCloudBias =   0.4f ;
		cloud.top.m_TopCloudDetail =  3.0f;
		cloud.top.m_TopCloudThreshold =   0.9f ;
		cloud.top.m_TopCloudHeight =    2.0f ;
		cloud.m_CloudSpeed =  0.01f ;
		cloud.m_CloudWarp = 1.0f;
		cloud.m_CloudFadeOut = 0.07f;
		night.m_StarBlinkFrequency =   100.0f;
		night.m_StarIntensity =   1.0f;
		lightning.m_LightningPulseFrequency  =   0.0f;
		lightning.m_LightningPulseIntensity =   0.0f ;
		night.m_MoonSize =   4.0f;
		night.m_MoonBrightness =  1.0f ;
		night.m_MoonPhase =   0;
		night.m_MoonColor = Vector3( 0.1f, 0.1f, 0.1f );
		night.m_MoonGlow = 0.2f;
		night.m_StarFieldThreshold = 0.0f;
		night.m_StarBlinkIntensity = 0.0f;
		night.m_MoonVisiblity = 1.0f;
	}

	virtual ~SkyDomeProceduralControl()	{}

	void CalculateSkySettings( float timeOfDay, SkyHatPerFrameSettings& settings );

	static Vector3 GetSunDirection( float timeOfDay )
	{
		Vector3 sunDir = ConvertTimeOfDayToVector3( timeOfDay);
		sunDir = -sunDir;
		return sunDir;
	}

	bool Load( const char* Filename);
	bool Save(const char* Filename);


	PAR_PARSABLE;
private:
	
	SkyStruct sky;
	CloudStruct	cloud;
	LightStruct	lightning;
	NightStruct night;

};



//
//	PURPOSE
//  Facade class which manages the perlin noise texture generator with the skyhat shader controller 
//
// <GROUP SkyHat>
// <FLAG Component>
class SkyDome : public datBase
{
public:
#if HACK_GTA4
	static const int	NumMoonPhases = 24;
#endif // HACK_GTA4

	enum eDrawTypes
	{
		eDrawNormal = 0,
		eDrawDpFront,
		eDrawDpBack,
#if HACK_GTA4
		eDrawDp,
#if  __PS3
		eDrawNormalCompressed, // for fake HDR rendering to a 88888 Target.
#endif // __PS3
#endif // HACK_GTA4
	};

	SkyDome() 
		: m_scale( 100.0f),
		m_yOffset(0.0f),
		m_dpYOffset(-800.0f),
		m_yScaler(10.0f),
		m_DpFrontDrawTechIdx(grcetNONE),
		m_DpBackDrawTechIdx(grcetNONE),
#if HACK_GTA4
		m_DpDrawTechIdx(grcetNONE),
#if  __PS3
		m_DrawCompressedTechIdx(grcetNONE),
#endif // __PS3
#endif // HACK_GTA4
		m_DrawTechniqueIdx(grcetNONE),
		m_DrawMoonTechniqueIdx( grcetNONE),
		m_model( 0 ),
		m_ZUp( false ),
		m_drawMoon( true ),
		m_moonRef( 1 ),
		m_drawStarfield( true )
	{}

	~SkyDome();
	
	void Init( SkyhatMiniNoise::SettingsCallback  setCallback  = SkyhatMiniNoise::SettingsCallback(), const ProceduralTextureSkyhat::Params&  params = ProceduralTextureSkyhat::Params() );
	void SetSettings( const SkyHatPerFrameSettings& settings )
	{
		m_shaderController.SetSettings( settings );
		m_perlinNoiseTextureGen.SetPhaseRate( settings.m_CloudSpeed );
		m_perlinNoiseTextureGen.SetCloudWarp( settings.m_CloudWarp );
		m_perlinNoiseTextureGen.SetSunSetBump( m_shaderController.GetSunSetBump() );
		
	}
	void Update( float time , int nextDay );
	void UpdateCloudGeneration( float time , int nextDay );
	void UpdateSky( float time  );

	void SetConversionScale( float scale )			{ m_scale = scale; }
	
	void DrawProceduralTextures();
#if HACK_GTA4
	void Draw(eDrawTypes drawType = eDrawNormal, bool inverted = false, bool cheapVersion = false, bool caped = false, Color32 capColor = Color32(0x0) );
#else // HACK_GTA4
	void Draw(eDrawTypes drawType = eDrawNormal, bool inverted = false, bool cheapVersion = false );
#endif // HACK_GTA4
	void DrawMT();
	void Show();
	void LinkToModelShader( grmShader& shader );

	void SetPerlinNoiseBaseTexture( const char* pNoiseBase );

	void SetHighDetailTexture( const char* pDetailTex );
	void SetHighDetailBumpTexture( const char* bumpTexture );
	void SetStarFieldTexture( const char* starField );
	void SetGalaxyTexture( const char* galaxy );
	void SetMoonTexture( const char* moon );
	void SetMoonGlowTexture( const char* moonGlow );


	void SetPerlinNoiseBaseTexture( atPtr<grcTexture> tex ) { m_perlinNoiseTextureGen.SetPerlinNoiseBaseTexture( tex );  }
	void SetHighDetailTexture( atPtr<grcTexture>  tex )	{ m_DetailTexture = tex; }
	void SetHighDetailBumpTexture( atPtr<grcTexture>  tex ) { m_DetailBump= tex; }
	void SetStarFieldTexture( atPtr<grcTexture> tex ) { m_Stars= tex; }
	void SetGalaxyTexture( atPtr<grcTexture> tex ) { m_Galaxy= tex; }
	void SetMoonTexture( atPtr<grcTexture> tex ) { m_Moon= tex; }
	void SetMoonGlowTexture( atPtr<grcTexture>  tex ) { m_MoonGlow= tex; }


	void SetHdrExposure(float f) {	m_shaderController.SetExposure(f); }
#if HACK_GTA4
	void SetHdrSunExposure(const Vector3 &v){ m_shaderController.SetSunExposure(v); }
#else // HACK_GTA4
	void SetHdrSunExposure(float f){ m_shaderController.SetSunExposure(f); }
#endif // HACK_GTA4
    void SetHdrClamp(const Vector3 &v) { m_shaderController.SetExposureClamp(v); }
	void SetZUp( bool zup = true ) { m_ZUp = zup; }

#if HACK_GTA4
	void SetGtaSkyDomeFade(const Vector3& f) { grmShader *pShader=m_model->GetShader();  if(pShader) pShader->SetVar(m_gtaSkyDomeFadeID, f);}
	void SetGtaWaterColor(const Vector3& w) { grmShader *pShader=m_model->GetShader();  if(pShader) pShader->SetVar(m_gtaWaterColorID, w); }
	void SetGtaSunHDRExposure(float f)	{ grmShader *pShader=m_model->GetShader();  if(pShader) pShader->SetVar(m_gtaSunHDRExposureID, f);}
#endif //HACK_GTA4...

	Vector3 GetMoonPosition() { return m_shaderController.GetMoonPosition(); }
#if HACK_GTA4
	void SetMoonPosition( const Vector3& pos )	{ m_shaderController.SetMoonPosition( pos); }

	int GetMoonPhase() { return m_shaderController.GetMoonPhase(); }
	void SetMoonPhase( int phase  )	{ m_shaderController.SetMoonPhase( phase % NumMoonPhases ); }
#endif // HACK_GTA4

	float GetHdrExposure() const { return m_shaderController.GetExposure(); }
#if HACK_GTA4
    const Vector3 &GetHdrSunExposure() const { return m_shaderController.GetSunExposure(); }
#else // HACK_GTA4
    float GetHdrSunExposure() const { return m_shaderController.GetSunExposure(); }
#endif // HACK_GTA4
    const Vector3 &GetHdrClamp() const { return m_shaderController.GetExposureClamp(); }

	SkyhatMiniNoise &GetShaderController() {return m_shaderController;}
	
	void SetSunDirection( const Vector3& pos )	{ m_shaderController.SetSunDirection( pos) ; }
	Vector3 GetSunDirection() { return m_shaderController.GetSunDirection(); }

	ProceduralTextureSkyhat			*GetProceduralTextures() {return &m_perlinNoiseTextureGen;}

	void SetUseRescaledClouds( bool v = "true" ) { m_perlinNoiseTextureGen.SetUseRescaledClouds( v ); }

	static void SetShaderName(char* shaderName);

	grcTexture* GetIBLTexture();

	SkyDome(datResource& rsc);
	DECLARE_PLACE(SkyDome);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

#if __BANK
	/*
	PURPOSE
		Sets the widgets for manipulating the sky dome
	*/
	void AddWidgets( bkBank &bank );

	void Save( const char* fileName );

#if  SKYHAT_USE_OFFSET
	void SaveSkyhatOffset();
#endif


#endif
	void Load( const char* fileName );
#if  SKYHAT_USE_OFFSET
	void LoadSkyhatOffset();
#endif
	bool LoadMoonSettings(char* filename);


	void ReloadTextures();

	bool GetDrawMoon() { return m_drawMoon; }
	void SetDrawMoon( bool v = true ) {  m_drawMoon = v; }

	bool GetDrawStarField() { return m_drawStarfield; }
	void DrawStarField( bool v = true ) {  m_drawStarfield = v; }

#if HACK_GTA4
	void SetMoonStencilReferenceValue( int ref ) { m_moonRef = ref; }
	void SetYOffset( float yOffset ) { m_yOffset = yOffset; }
	float GetYOffset() { return m_yOffset; }
#endif // HACK_GTA4
private:

	void DrawMoonQuad();
	void SetupTechniques();

	const SkyDome& operator=( SkyDome& other );
	SkyDome( SkyDome& other );

	SkyhatMiniNoise					m_shaderController;
	ProceduralTextureSkyhat			m_perlinNoiseTextureGen;

	ImmediateRenderer<SkyVtx>		m_vertRenderer;

	EmbeddedModel*					m_model;
	float							m_scale;

	atPtr<grcTexture>				m_DetailTexture;
	atPtr<grcTexture>				m_DetailBump;
	atPtr<grcTexture>				m_Stars;
	atPtr<grcTexture>				m_Galaxy;
	atPtr<grcTexture>				m_Moon;
	atPtr<grcTexture>				m_MoonGlow;

	float							m_yOffset;
	float							m_dpYOffset;
	float							m_yScaler;

	static char*					m_ShaderName;

	grcEffectTechnique				m_DpFrontDrawTechIdx;
	grcEffectTechnique				m_DpBackDrawTechIdx;
#if HACK_GTA4
	grcEffectTechnique				m_DpDrawTechIdx;
#if __PS3
	grcEffectTechnique				m_DrawCompressedTechIdx;
#endif // __PS3
#endif // HACK_GTA4
	grcEffectTechnique				m_DrawTechniqueIdx;
	grcEffectTechnique				m_DrawMoonTechniqueIdx;
	grcEffectTechnique				m_DrawStencilTechniqueIdx;

	bool							m_ZUp;

#if HACK_GTA4
	grcEffectVar					m_gtaSkyDomeFadeID;
	grcEffectVar					m_gtaSunHDRExposureID;
	grcEffectVar					m_gtaWaterColorID;
#endif //HACK_GTA4...
	bool							m_drawMoon;
	bool							m_drawStarfield;
	int								m_moonRef;
};



struct SkyFillLights
{
	Vector3 SkyColor;
	Vector3 MidColor;
	Vector3 GroundColor;
	Vector3 BounceColor;

	Vector3 BounceDirection;

	void ConvertTo3PackedVector4( Vector4& red, Vector4& green, Vector4& blue )
	{
		red = Vector4( SkyColor.x, MidColor.x, GroundColor.x, BounceColor.x );
		green = Vector4( SkyColor.y, MidColor.y, GroundColor.y, BounceColor.y );
		blue = Vector4( SkyColor.z, MidColor.z, GroundColor.z, BounceColor.z );
	}
};

void SkyHatCreateFillLights( const Vector3& SunDirection, const SkyHatPerFrameSettings& settings, const Vector3& groundColor, float intensity,  float bounceIntensity, SkyFillLights& fill );

inline Vector3 SkyHatGetFarFogColor( const SkyHatPerFrameSettings& settings, const Vector3& /*sunDirection */ )
{
#if HACK_GTA4
	float	AzContr = settings.m_AzimuthStrength * 0.1f;

	return (settings.m_skyColor * (1.0f - AzContr)) + (settings.m_AzimuthColor * AzContr);
#else // HACK_GTA4
	Vector3 sunColor( settings.m_SunColor.x, settings.m_SunColor.y, settings.m_SunColor.z );
	float SunFog = 0.0f;
	
	return settings.m_skyColor + settings.m_AzimuthColor * settings.m_AzimuthStrength * 0.1f +
				+ SunFog * sunColor * settings.m_UnderLightStrength ;
#endif // HACK_GTA4
}

inline Vector3 SkyHatGetHorizonColor( const SkyHatPerFrameSettings& settings )
{
	return settings.m_skyColor + settings.m_AzimuthColor * settings.m_AzimuthStrength;

}

#if HACK_GTA4
// PURPOSE : 
// calculates the moon phase (0-23), accurate to 1 segment.
// 0 => full moon.
// 12 = > new moon.
// NOTES: nicked from  http://www.voidware.com/moon_phase.htm 
//
int SkyHatGetMoonPhase(int year,int month,int day);
#endif // HACK_GTA4

class SkyLightController: public ShaderFragment
{
	struct SkyLightVarCache : public VarCache
	{
		grcEffectVar m_skyLight_LightRedID;
		grcEffectVar m_skyLight_LightGreenID;
		grcEffectVar m_skyLight_LightBlueID;

		void CacheEffectVars( grmShader& effect )
		{
			m_skyLight_LightRedID = effect.LookupVar( "skyLight_LightRed", IsPedantic() );
			m_skyLight_LightGreenID = effect.LookupVar( "skyLight_LightGreen", IsPedantic() );
			m_skyLight_LightBlueID = effect.LookupVar( "skyLight_LightBlue" , IsPedantic());
		}
	};

public:
	SkyLightController() : m_intensity( 0.25f ), m_GroundColor( 0.8f, 0.8f, 0.4f ), m_bounceIntensity( 0.5f )
	{}

	VarCache*	CreateVariableCache()	{ return rage_new SkyLightVarCache; }

	void SetupShader( grmShader& effect , VarCache* cachedVars )
	{
		SkyLightVarCache*		vars = reinterpret_cast<SkyLightVarCache*>(cachedVars);

		Vector4 red;
		Vector4 green;
		Vector4 blue;

		m_fill.ConvertTo3PackedVector4( red, green, blue );

		effect.SetVar( vars->m_skyLight_LightRedID, red );
		effect.SetVar( vars->m_skyLight_LightGreenID, green );
		effect.SetVar( vars->m_skyLight_LightBlueID, blue );
	}

	void Update( const Vector3& sunDirection, const SkyHatPerFrameSettings& settings )
	{
		SkyHatCreateFillLights( sunDirection, settings, m_GroundColor, m_intensity,  m_bounceIntensity, m_fill );
	}

	void SetupShaderPass()		{}

	bool UsesFragment( grmShader& effect )
	{
		return ( effect.LookupVar( "skyLight_LightRed" , false ) != 0 );
	}
	Vector3 GetAmbient()	{ return m_fill.MidColor * 0.6f + m_fill.SkyColor * 0.6f + m_fill.BounceColor * 0.5f; }

	PAR_PARSABLE;
private:

	Vector3			m_GroundColor;
	float			m_intensity;
	float			m_bounceIntensity;
	SkyFillLights	m_fill;

};
};


#endif
