// 
// grcphenomna.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//
//	PURPOSE
//		Controls for various global shader effects which are applied to multiple objects are the same time
//
//

#ifndef SHADERCONTROLLERS_GRCPHENOMNA_H
#define SHADERCONTROLLERS_GRCPHENOMNA_H


#include "grcore/state.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grmodel/geometry.h"
#include "parser/macros.h"
#include "rmcore/drawable.h"
#include "system/timer.h"

#include "shaderfragmentcontrol.h"
#include "sphericallight.h"
#include "proceduraltexture.h"

#include "grcore/light.h"
#include "grcore/state.h"
#include "system/timemgr.h"
#include "spatialdata/plane.h"
#include "atl/ptr.h"

namespace rage
{

inline bool IsPedantic()		{ return false;}

Vector3 ConvertTimeOfDayToVector3( float timeOfDay );
float GetNightFadeOut( float timeOfDay );

//#############################################################################



//
//	PURPOSE
//		Calculates atmospheric scattering parameters for use with a shader
//		containing the atmospheric scattering fragment
//
class AtmosphericScattering: public ShaderFragment
{
	struct AtmoscattVarCache : public VarCache
	{
		grcEffectVar m_TimeOfDayVar;
		grcEffectVar m_HazeDistanceVar;
		grcEffectVar m_HazeIntensityVar;
		grcEffectVar m_HDRExposureVar;

		grcEffectVar m_AttenuationMapVar;
		grcEffectVar m_CurrentTimeVar;
		grcEffectVar m_AtmosphericScatteringOnVar;
		grcEffectVar m_SunDirectionVar;
		grcEffectVar m_NightFade;

		void CacheEffectVars( grmShader& effect )
		{
			// Time of Day
			m_TimeOfDayVar				= effect.LookupVar("TimeOfDay", IsPedantic());

			// haze distance
			m_HazeDistanceVar			= effect.LookupVar("HazeDistance", IsPedantic());

			// haze intensity
			m_HazeIntensityVar			= effect.LookupVar("HazeIntensity", IsPedantic());

			// ???
			m_AttenuationMapVar			= effect.LookupVar("AttenuationMap", IsPedantic());

			// why do we have a current time here
			//m_CurrentTimeVar			= effect.LookupVar("CurrentTime", IsPedantic());

			// toggle atmospheric scattering on
			m_AtmosphericScatteringOnVar= effect.LookupVar("AtmosphericScatteringOn", IsPedantic());

			// sun direction ... used internally not exposed
			m_SunDirectionVar			= effect.LookupVar("SunDirection", IsPedantic());

			// night fade ... also used internally
			m_NightFade					= effect.LookupVar("NightFade", IsPedantic());
		}
	};

public:
	AtmosphericScattering()
		: m_timeOfDay( 0.0f ),
		m_hazeDistance( 0.7f ),
		m_hazeIntensity( 0.7f ),
		m_AttenuationMap( 0 ),
		m_on( true )
	{
	}

    ~AtmosphericScattering()
    {
        if ( m_AttenuationMap )
        {
            m_AttenuationMap->Release();
        }
    }

	bool UsesFragment( grmShader& effect )
	{
		return ( effect.LookupVar( "HazeIntensity" , false ) != 0 );
	}
	

	VarCache*	CreateVariableCache()	{ return rage_new AtmoscattVarCache; }

	void SetupShader( grmShader& effect, VarCache*  cVars) 
	{
		FastAssert( cVars );
		AtmoscattVarCache*		var = static_cast<AtmoscattVarCache*>(cVars);

		effect.SetVar( var->m_TimeOfDayVar, m_timeOfDay );
		effect.SetVar( var->m_HazeDistanceVar, m_hazeDistance );
		
		effect.SetVar( var->m_AttenuationMapVar, m_AttenuationMap );
		//effect.SetVar( var->m_CurrentTimeVar, m_ctime.GetTime() );
		effect.SetVar( var->m_AtmosphericScatteringOnVar, m_on );

		Vector3 sunDirection = ConvertTimeOfDayToVector3( m_timeOfDay );
		//sunDirection.x = -sunDirection.x;
		effect.SetVar( var->m_SunDirectionVar, sunDirection );

		effect.SetVar( var->m_HazeIntensityVar, m_hazeIntensity * GetNightFadeOut( m_timeOfDay ) );

		effect.SetVar( var->m_NightFade,  GetNightFadeOut( m_timeOfDay ) );
	}

	void SetupShaderPass() 
	{}

	// public interface
	void		SetTimeOfDay( float timeOfDay )		{ m_timeOfDay = timeOfDay; }
	float		GetTimeOFDay()	const					{ return m_timeOfDay; }

	void		SetAttenuationMap( const char* fileName )	
	{ 
		if ( m_AttenuationMap )
		{
			m_AttenuationMap->Release();
		}
		m_AttenuationMap =  grcTextureFactory::GetInstance().Create( fileName );
	}

	Color32	GetAzimuthColor()
	{
		return Color32( 240,240,255);
	}

	PAR_PARSABLE;
private:
	float			m_timeOfDay;
	float			m_hazeDistance;
	float			m_hazeIntensity;
	char			m_AttenunationMapName[64];
	sysTimer		m_ctime;
	grcTexture*		m_AttenuationMap;

	bool			m_on;
};

//
//	PURPOSE
//		Sets the required paramaters for Interval Shadows fragment. Also sets ambient occlusion
//		and sun direction settings.
//
class IntervalShadows : public ShaderFragment
{
	struct IntervalShadowVarCache : public VarCache
	{
		grcEffectVar m_hieghtShiftVar;
		grcEffectVar m_softnessVar;
		grcEffectVar m_projectXVar;
		grcEffectVar m_projectYVar;
		grcEffectVar m_ambientOccVar;
		grcEffectVar m_intervalVar;
		grcEffectVar m_lightOccVar;
		grcEffectVar m_prtOccVar;
		grcEffectVar m_prtOnVar;
		grcEffectVar m_intervalShadowsVar;
		grcEffectVar m_sunLightVar;

		void CacheEffectVars( grmShader& effect )
		{
			m_hieghtShiftVar	= effect.LookupVar("IntervalShadowHeightShift", IsPedantic());
			m_softnessVar		= effect.LookupVar("IntervalShadowShadowSoftness", IsPedantic());
			m_projectXVar		= effect.LookupVar("IntervalShadowHeightMapProjectionX", IsPedantic());
			m_projectYVar		= effect.LookupVar("IntervalShadowHeightMapProjectionY", IsPedantic());
			m_ambientOccVar		= effect.LookupVar("AmbientOccTex", IsPedantic());
			m_intervalVar		= effect.LookupVar("IntervalTex", IsPedantic());
			m_lightOccVar		= effect.LookupVar("LightOcclusionTex", IsPedantic());
			m_prtOccVar			= effect.LookupVar("PRTOccTex", false );
			m_prtOnVar			= effect.LookupVar("PRTon", false );
			m_intervalShadowsVar = effect.LookupVar("IntervalShadowsOn", false );
			m_sunLightVar		= effect.LookupVar("SunLightIntensity", false );
		}
	};

public:
	IntervalShadows( bool version2 = false )
	{
		Init( version2 );
	}

    ~IntervalShadows()
    {
        if ( m_intervalTexture )
        {
            m_intervalTexture->Release();
        }

        if ( m_occTexture )
        {
            m_occTexture->Release();
        }

        if ( m_lightOccTexture )
        {
            m_lightOccTexture->Release();
        }

        if ( m_PRTOccTexture )
        {
            m_PRTOccTexture->Release();
        }
    }

	VarCache*	CreateVariableCache()	{ return rage_new IntervalShadowVarCache; }
	
	void Init(  bool version2 )
	{
		Matrix44	interProj;
		interProj.a =  Vector4( 0.000973039f, 0.0f, 0.0f, 0.0f ); 
		interProj.b =  Vector4( 0.0f,  -4.25329e-11f, 0.000973039f, 0.0f ); 
		interProj.c =  Vector4( 0.0f,  -0.000976458f, -4.26823e-11f, 0.0f ); 
		interProj.d =  Vector4( 0.501804f,  0.500054f,0.684471f, 1.0f ); 
			
		if ( version2 )
		{
			interProj.a =  Vector4(  0.000325567f, 0.0f, 0.0f, 0.0f ); 
			interProj.b =  Vector4(0.0f, -2.18557e-08f, 0.00195313f, 0.0f );
			interProj.c =  Vector4( 0.0f,  -0.000546984f, -9.33962e-14f, 0.0f );
			interProj.d =  Vector4( 0.5f, 0.321029f, 0.0019531f ,1.0f );
			//0.000325567 0 0 0 
			//	0 -2.18557e-08 0.00195313 0 
			//	0 -0.000546984 -9.33962e-14 0 
			//	0.5 0.321029 0.00195313 1 


		}

		m_intervalProjection.Transpose( interProj );
	
		m_intervalTexture = 0;
		m_occTexture = 0;
		m_lightOccTexture = 0;
		m_PRTOccTexture = 0;

		m_softness = 16.0f;
		m_prtOn = true;
		m_on = true;
		m_sunLightIntensity = 1.0f;

		m_cachedVars = false;
		m_lightDirection = Vector3( 0.0f, 1.0f, 0.0f );
	}

	void SetupShaderPass()
	{}

	void SetupShader( grmShader& effect, VarCache* cachedVars )
	{
		FastAssert( cachedVars );
		IntervalShadowVarCache*		vars = static_cast<IntervalShadowVarCache*>(cachedVars);
		Matrix34	interProj;
		m_intervalProjection.ToMatrix34( interProj );
		
		effect.SetVar( vars->m_hieghtShiftVar, GetHieghtShift(  interProj , 0.0f ) );
		effect.SetVar( vars->m_softnessVar, m_softness  );
		effect.SetVar( vars->m_projectXVar, m_intervalProjection.a );
		effect.SetVar( vars->m_projectYVar, m_intervalProjection.b );
		effect.SetVar( vars->m_ambientOccVar, m_occTexture );
		effect.SetVar( vars->m_intervalVar, m_intervalTexture );		
		effect.SetVar( vars->m_lightOccVar, m_lightOccTexture );
		effect.SetVar( vars->m_prtOccVar, m_PRTOccTexture );
		effect.SetVar( vars->m_prtOnVar, m_prtOn );
		effect.SetVar( vars->m_intervalShadowsVar, m_on );
		effect.SetVar( vars->m_sunLightVar, m_sunLightIntensity );
	}

	bool UsesFragment( grmShader& effect )
	{
		return ( effect.LookupVar( "IntervalShadowHeightMapProjectionX" , false ) != 0 );
	}

	void SetIntervalMapProjectionMtx( Matrix44& intervalProjection )
	{
		m_intervalProjection = intervalProjection;
	}

	void SetTimeOfDay( float timeOfDay )	
	{		
		m_lightDirection = ConvertTimeOfDayToVector3( timeOfDay );
		//m_lightDirection.x = -m_lightDirection.x;
	}
	void SetLightDirection( const Vector3& lightDirection )
	{
		m_lightDirection = lightDirection;
	}

	Vector3 GetLightDirection() const { return m_lightDirection; }

	void SetIntervalTexture( grcTexture*	intervalTexture )	{		m_intervalTexture = intervalTexture; }

	void SetIntervalMap( const char* fileName )	
	{ 
		if ( m_intervalTexture )
		{
			m_intervalTexture->Release();
		}
		m_intervalTexture = grcTextureFactory::GetInstance().Create(fileName);
		grcTextureFactory::GetInstance().RegisterTextureReference("__IntervalTex", m_intervalTexture);
	}
	void SetIntervalMap( grcTexture* tex  )	
	{ 
		 m_intervalTexture  = tex;		// weak reference
	}

	grcTexture* GetIntervalTexture()	{ return m_intervalTexture; }
	grcTexture* GetAmbientOcclusionTexture()	{ return m_occTexture; }
	Matrix44	GetWorldToTextureMatrix()		{ return m_intervalProjection; }
	
	void SetAmbientOcclusionTexture( const char* fileName )
	{
		if ( m_occTexture )
		{
			m_occTexture->Release();
		}
		// load noise look-up texture for the old TV effect in Sepia
		m_occTexture = grcTextureFactory::GetInstance().Create(fileName);
		grcTextureFactory::GetInstance().RegisterTextureReference("__AmbientOccTex", m_occTexture);
	}

	void SetLightOcclusionTexture( const char* fileName )
	{
		if ( m_lightOccTexture )
		{
			m_lightOccTexture->Release();
		}
		// load noise look-up texture for the old TV effect in Sepia
		m_lightOccTexture = grcTextureFactory::GetInstance().Create(fileName);
		grcTextureFactory::GetInstance().RegisterTextureReference("__LightOccTex", m_occTexture);
	}

	void SetPRTOcclusionTexture( const char* fileName )
	{
		if ( m_PRTOccTexture )
		{
			m_PRTOccTexture->Release();
		}
		// load noise look-up texture for the old TV effect in Sepia
		m_PRTOccTexture = grcTextureFactory::GetInstance().Create(fileName);
		grcTextureFactory::GetInstance().RegisterTextureReference("__PRTOccTex", m_PRTOccTexture);
	}
	
	PAR_PARSABLE;
private:

	Vector3		GetHieghtShift(  const Matrix34& texProjMtx , float h ) const
	{
		
		// create vector of xz movement relative to one unit in y
		Vector3	ldir =	m_lightDirection;
		ldir.Negate();
		if ( ldir.y != 0.0f )
		{
			ldir /= abs( ldir.y );
		}
		ldir.y = 0.0f;
		// convert to texture space
		ldir.Dot3x3( texProjMtx );
		return Vector3( ldir.x, ldir.y, h );

	}

	grcTexture*			m_intervalTexture;
	grcTexture*			m_occTexture;
	grcTexture*			m_lightOccTexture;
	grcTexture*			m_PRTOccTexture;
	Vector3				m_lightDirection;
	Matrix44			m_intervalProjection;
	float				m_softness;
	float				m_sunLightIntensity;

	bool				m_on;
	bool				m_prtOn;

	bool				m_cachedVars;

};

//
//	PURPOSE
//		Sets the required parameters for 3D volumetric height fog with 3D noise. It compresses the settings
//		to minimize constant usage and reduce instruction cost.
//
class FogControl : public ShaderFragment
{
	struct FogControlVarCache : public VarCache
	{
		grcEffectVar m_FogDensityVar;
		grcEffectVar m_FogHieghtVar;
		grcEffectVar m_FogColorVar;
		grcEffectVar m_FogOffsetVar;
		grcEffectVar m_NoiseVolumeTextureVar;
		grcEffectVar m_FogNoiseVar;
		grcEffectVar m_volumeFogOnOnVar;

		void CacheEffectVars( grmShader& effect )
		{
			m_FogDensityVar				= effect.LookupVar("FogDensity", IsPedantic());
			m_FogHieghtVar			=	effect.LookupVar("FogHieght", IsPedantic());
			m_FogColorVar				= effect.LookupVar("FogColor", IsPedantic());
			m_FogOffsetVar				= effect.LookupVar("FogOffset", IsPedantic());
			m_NoiseVolumeTextureVar				= effect.LookupVar("NoiseVolumeTexture", IsPedantic());
			m_FogNoiseVar			=	effect.LookupVar("FogNoise", IsPedantic());
			m_volumeFogOnOnVar			=	effect.LookupVar("volumeFogOn", false);
		}
	};

public:

	FogControl():
		 m_density( 0.01f),
		  m_color( 100.0f, 100.0f, 100.0f ),
		  m_speed( 3.0f ),
		  m_noiseTile( 0.4f ),
		  m_noiseTileY( 0.4f ),
		  m_noiseBias( 0.120f ),
		  m_noiseScale( 0.997f ),
		  m_maxHeight( 52.125f ),
		  m_minHeight( -48.5f),
		  m_fuzzHeight( 60.875 ),
		  m_on( true)
	  {}

    ~FogControl()
    {
        if ( m_noiseVolumeTexture )
        {
            m_noiseVolumeTexture->Release();
        }
    }

	void Init();

	void SetVolumeTexture( const char* volume );

	void Set( grmShader& effect , FogControlVarCache* vars ) const ;

	VarCache*	CreateVariableCache()	{ return rage_new FogControlVarCache; }

	void SetupShader( grmShader& effect , VarCache* cachedVars )
	{
		FogControlVarCache*		vars = static_cast<FogControlVarCache*>(cachedVars);
		Set( effect , vars);
	}
	void SetupShaderPass()		{}

	bool UsesFragment( grmShader& effect )
	{
		return ( effect.LookupVar( "FogNoise" , false ) != 0 );
	}

	
	PAR_PARSABLE;
private:

	sysTimer	m_timer;
	float		m_density;
	Color32		m_color;
	float		m_speed;


	float	m_noiseTile;
	float	m_noiseTileY;
	float	m_noiseBias;
	float	m_noiseScale;
	float	m_maxHeight;
	float	m_minHeight;
	float	m_fuzzHeight;
	bool			m_on;

	grcTexture*		m_noiseVolumeTexture;
};




//
//	PURPOSE
//		Sets the required parameters for 3D volumetric height fog with 3D noise. It compresses the settings
//		to minimize constant usage and reduce instruction cost.
//
class TimeControl : public ShaderFragment
{
	struct TimeControlVarCache : public VarCache
	{
		grcEffectVar m_CurentTimeVar;
		
		void CacheEffectVars( grmShader& effect )
		{
			m_CurentTimeVar			=	effect.LookupVar("Scene_CurrentTime");
		}
	};

public:
	  VarCache*	CreateVariableCache()	{ return rage_new TimeControlVarCache; }

	  void SetupShader( grmShader& effect , VarCache* cachedVars )
	  {
		  TimeControlVarCache*		vars = static_cast<TimeControlVarCache*>(cachedVars);
		  float time = TIME.GetElapsedTime();
		  effect.SetVar( vars->m_CurentTimeVar, time );
	  }
	  void SetupShaderPass()		{}

	  bool UsesFragment( grmShader& effect )
	  {
		  return ( effect.LookupVar( "Scene_CurrentTime" , false ) != 0 );
	  }
private:
};



//
//	PURPOSE
//		Sets the required parameters for water based depth fog. 
//
class WaterFogControl : public ShaderFragment
{
	struct WaterFogControlVarCache : public VarCache
	{
		grcEffectVar m_WaterFogDensityVar;
		grcEffectVar m_WaterFogHieghtVar;
		grcEffectVar m_WaterFogColorVar;
		grcEffectVar m_waterFogOnVar;

		void CacheEffectVars( grmShader& effect )
		{
			m_WaterFogDensityVar				= effect.LookupVar("WaterFogDensity", IsPedantic());
			m_WaterFogHieghtVar			=	effect.LookupVar("WaterFogHieght", IsPedantic());
			m_WaterFogColorVar				= effect.LookupVar("WaterFogColor", IsPedantic());
			m_waterFogOnVar			=	effect.LookupVar("waterFogOn", false );
		}
	};
public:

	WaterFogControl():
	  m_density( 0.16f),
		  m_color( 8.0f, 56.0f, 33.0f ),
		  m_height( -7.0f),
		  m_on( true),
		  m_IncidentColor( 1.0f, 1.0f, 1.0f )
	  {}

	
	  void Set( grmShader& effect, WaterFogControlVarCache*		vars ) const ;

	  VarCache*	CreateVariableCache()	{ return rage_new WaterFogControlVarCache; }

	  void SetupShader(  grmShader& effect , VarCache* cachedVars )
	  {
		  WaterFogControlVarCache*		vars = static_cast<WaterFogControlVarCache*>(cachedVars);

		  Set( effect , vars);
	  }
	  void SetupShaderPass()		{}

	  bool UsesFragment( grmShader& effect )
	  {
		  return ( effect.LookupVar( "WaterFogColor" , false ) != 0 );
	  }

	  void SetIncidenceColor( const Vector3& color )
	  {
		  m_IncidentColor = color;
	  }
	  PAR_PARSABLE;
private:

	float		m_density;
	Color32		m_color;
	float		m_height;
	bool			m_on;
	Vector3		m_IncidentColor;

};

//#############################################################################

//fwd reference
class SphericalLight3rdOrder;

//
//	PURPOSE
//		uses the enviroment to light an object
//		uses the SphericalLight3rdOrder to light the object
//
class EnviromentDomeLighting : public ShaderFragment
{
	struct EnviromentDomeVarCache : public VarCache
	{
		grcEffectVar m_sphLightCoefficientsVar;
		grcEffectVar m_sphericalAmbientOnVar;

		void CacheEffectVars( grmShader& effect )
		{
			m_sphLightCoefficientsVar				= effect.LookupVar("SphericalLightCoefficients", IsPedantic());
			m_sphericalAmbientOnVar			=	effect.LookupVar("SphericalAmbientOn", IsPedantic());
		}
	};

public:
    EnviromentDomeLighting():
		 m_sky( 0.3f, 0.3f, 1.0f ),
		 m_horizon( 0.9f, 0.9f, 1.0f ),
		 m_ground( 0.1f, 0.8f, 0.1f ),
		 m_intensity( 1.0f ),
		 m_on( true),
		 m_currentBuffer(0)
	  {
		  for ( int i = 0; i < NumBuffers; i++ )
		  {
			m_enviromentMapTexture[i]= 0;
		  }
	  }

      ~EnviromentDomeLighting()
      {
		  for ( int i = 0; i < NumBuffers; i++ )
		  {
			  m_enviromentMapTexture[i]->Release();
          }
      }

	  void Set( grmShader& effect, EnviromentDomeVarCache* vars );

	  VarCache*	CreateVariableCache()	{ return rage_new EnviromentDomeVarCache; }
 
	  void SetupShader(  grmShader& effect , VarCache*  cachedVars ) 
	  {
		  EnviromentDomeVarCache*		vars = static_cast<EnviromentDomeVarCache*>(cachedVars);
		  Set( effect, vars );
	  }
	  void SetupShaderPass()		{}

	 
	  bool UsesFragment( grmShader& effect )
	  {
		  return ( effect.LookupVar( "SphericalAmbientOn" , false ) != 0 );
	  }

	  void SetEnvironmentMapTexture( const char* UNUSED_PARAM( fileName ) )
	  {
		  AssertMsg( 0, "Deprecated" );
		/*  if ( m_enviromentMapTexture )
		  {
			  m_enviromentMapTexture->Release();
		  }
		  m_enviromentMapTexture = grcTextureFactory::GetInstance().Create(fileName);
		  grcTextureFactory::GetInstance().RegisterTextureReference("__EnviromentMapTex", m_enviromentMapTexture);*/
	  }
	  void SetEnvironmentMapTexture( grcTexture*	tex )
	  {
		   m_enviromentMapTexture[ m_currentBuffer % NumBuffers  ] = tex;
		  UpdateSH( m_enviromentMapTexture[ ( m_currentBuffer  + 1 ) % NumBuffers]);
		  m_currentBuffer++;
	  }
	 
	  const SphericalLight3rdOrder&		GetSHEnvironment() const { return m_env; }

	  Vector3	GetAmbient()	{ return m_env.GetCoeffs()[0];}
	  Vector3	GetGroundColor()	{ return m_ground; }
	 

	  PAR_PARSABLE;
private:
	void Create( SphericalLight3rdOrder& env );
	void CreateSkyMap( grcTexture* tex, SphericalLight3rdOrder& env );
	void UpdateSH( grcTexture* tex = 0 );

	Vector3	m_sky;
	Vector3	m_horizon;
	Vector3 m_ground;
	float	m_intensity;
	bool	m_on;

	SphericalLight3rdOrder		m_env;
	
	static const int	NumBuffers = 4;
	int				m_currentBuffer;
	grcTexture*		m_enviromentMapTexture[ NumBuffers ];	
};

//#############################################################################

/************************************************************************/
/* This class handles updating the verlet water simulation              */
/************************************************************************/
class VerletWaterSimulation : public ShaderFragment
{
    struct VerletWaterSimulationVarCache : public VarCache
    {
        grcEffectVar m_simTexWidthID;
        grcEffectVar m_simTexHeightID;
        grcEffectVar m_dampeningTexID;
        grcEffectVar m_simRTID;
        grcEffectVar m_prevSimRTID;

        void CacheEffectVars( grmShader& effect )
        {
            m_simTexWidthID = effect.LookupVar( "SimulationTexWidth" , IsPedantic() );
            m_simTexHeightID = effect.LookupVar( "SimulationTexHeight" , IsPedantic() );
            m_dampeningTexID = effect.LookupVar( "DampeningTex"  , IsPedantic());
            m_simRTID = effect.LookupVar( "SimulationTex" , IsPedantic() );
            m_prevSimRTID = effect.LookupVar( "PrevSimulationTex" , IsPedantic() );
        }
    };

public:
    VerletWaterSimulation()
        : m_simWidth(256)
        , m_simHeight(256)
        , m_pDampeningTex(NULL)
        , m_pSimRT(NULL)
        , m_pPrevSimRT(NULL)
        , m_on(true)
    {
    }

    ~VerletWaterSimulation()
    {
        if ( m_pDampeningTex )
        {
            m_pDampeningTex->Release();
        }
    }

    bool UsesFragment( grmShader& effect )
    {
        return ( (effect.LookupVar("SimulationTex",false) != 0) && (effect.LookupVar("PrevSimulationTex",false) != 0) );
    }

    VarCache* CreateVariableCache()	{ return rage_new VerletWaterSimulationVarCache; }

    void SetupShader( grmShader& effect, VarCache* cVars ) 
    {
        FastAssert( cVars );
        VerletWaterSimulationVarCache* var = static_cast<VerletWaterSimulationVarCache*>(cVars);

        effect.SetVar( var->m_simTexWidthID, (float)m_simWidth );
        effect.SetVar( var->m_simTexHeightID, (float)m_simHeight );
        effect.SetVar( var->m_dampeningTexID, m_pDampeningTex );
        effect.SetVar( var->m_simRTID, m_pSimRT );
        effect.SetVar( var->m_prevSimRTID, m_pPrevSimRT );
    }

    void SetupShaderPass() 
    {}

    // public interface
    void SetTexWidth( int w ) { m_simWidth = w; }
    int GetTexWidth() const { return m_simWidth; }

    void SetTexHeight( int h ) { m_simHeight = h; }
    int GetTexHeight() const { return m_simHeight; }

    void SetDampeningTex( const char *pDampeningTexFile );
    grcTexture* GetDampeningTex() const { return m_pDampeningTex; }

    void SetSimRT( grcRenderTarget *pRT ) { m_pSimRT = pRT; }
    grcRenderTarget* GetSimRT() const { return m_pSimRT; }

    void SetPrevSimRT( grcRenderTarget *pRT ) { m_pPrevSimRT = pRT; }
    grcRenderTarget* GetPrevSimRT() const { return m_pPrevSimRT; }

    PAR_PARSABLE;

private:
    int m_simWidth;
    int m_simHeight;

    grcTexture* m_pDampeningTex;
    grcRenderTarget* m_pSimRT;
    grcRenderTarget* m_pPrevSimRT;

    bool m_on;
};

//#############################################################################

/************************************************************************/
/* This class handles updating the verlet water perturbation            */
/************************************************************************/
class VerletWaterPerturbation : public ShaderFragment
{
    struct VerletWaterPerturbationVarCache : public VarCache
    {
        grcEffectVar m_perturbationTexID;

        void CacheEffectVars( grmShader& effect )
        {
            m_perturbationTexID = effect.LookupVar( "PerturbationTex", IsPedantic() );
        }
    };

public:
    VerletWaterPerturbation()
        : m_pPerturbationTex(NULL)
        , m_on(true)
    {
    }

    ~VerletWaterPerturbation()
    {
        if ( m_pPerturbationTex )
        {
            m_pPerturbationTex->Release();
        }
    }

    bool UsesFragment( grmShader& effect )
    {
        return ( effect.LookupVar( "PerturbationTex" , false ) != 0 );
    }

    VarCache* CreateVariableCache()	{ return rage_new VerletWaterPerturbationVarCache; }

    void SetupShader( grmShader& effect, VarCache* cVars ) 
    {
        FastAssert( cVars );
        VerletWaterPerturbationVarCache* var = static_cast<VerletWaterPerturbationVarCache*>(cVars);

        effect.SetVar( var->m_perturbationTexID, m_pPerturbationTex );
    }

    void SetupShaderPass() 
    {}

    // public interface
    void SetPerturbationTex( const char *pPerturbationTexFile );
    grcTexture* GetPerturbationTex() const { return m_pPerturbationTex; }

    PAR_PARSABLE;

private:
    grcTexture* m_pPerturbationTex;

    bool m_on;
};

//#############################################################################

/************************************************************************/
/* This class handles updating the shader that is applied to the        */
/* water surface mesh itself.                                           */
/************************************************************************/
class VerletWaterSurface : public ShaderFragment
{
    struct VerletWaterSurfaceVarCache : public VarCache
    {
        grcEffectVar m_phaseID;
        grcEffectVar m_refractRTID;
        grcEffectVar m_simRTID;

        void CacheEffectVars( grmShader& effect )
        {
            m_phaseID = effect.LookupVar( "RipplePhase", IsPedantic() );
            m_refractRTID = effect.LookupVar( "RefractionTex" , IsPedantic());
            //m_simRTID = effect.LookupVar( "VerletSimTex", IsPedantic() );
        }
    };

public:
    VerletWaterSurface()
        : m_timeOfDay(0.0f)    
        , m_phaseRate(8.0f)
        , m_pRefractRT(NULL)
        , m_pSimRT(NULL)
        , m_on(true)
    {
    }

    ~VerletWaterSurface()
    {
    }

    bool UsesFragment( grmShader& effect )
    {
        return ( effect.LookupVar("RipplePhase",false) != 0 );
    }

    VarCache* CreateVariableCache()	{ return rage_new VerletWaterSurfaceVarCache; }

    void SetupShader( grmShader& effect, VarCache* cVars ) 
    {
        FastAssert( cVars );
        VerletWaterSurfaceVarCache* var = static_cast<VerletWaterSurfaceVarCache*>(cVars);

        effect.SetVar( var->m_phaseID, m_timeOfDay * m_phaseRate );
        effect.SetVar( var->m_refractRTID, m_pRefractRT );
      //  effect.SetVar( var->m_simRTID, m_pSimRT );
    }

    void SetupShaderPass() 
    {}

    // public interface
    void SetTimeOfDay( float timeOfDay ) { m_timeOfDay = timeOfDay; }
    float GetTimeOFDay() const { return m_timeOfDay; }

    void SetPhaseRate( float phaseRate ) { m_phaseRate = phaseRate; }
    float GetPhaseRate() const { return m_phaseRate; }

    void SetRefractRT( grcRenderTarget *pRT ) { m_pRefractRT = pRT; }
    grcRenderTarget* GetRefractRT() const { return m_pRefractRT; }

    void SetSimRT( grcRenderTarget *pRT ) { m_pSimRT = pRT; }
    grcRenderTarget* GetSimRT() const { return m_pSimRT; }

    PAR_PARSABLE;

private:
    float m_timeOfDay;
    float m_phaseRate;

    grcRenderTarget* m_pRefractRT;
    grcRenderTarget* m_pSimRT;

    bool m_on;
};

//
//	PURPOSE
//		Sets the required parameters for perlin noise generation shader.
//
class SkyhatPerlinNoise : public ShaderFragment
{
    struct SkyhatPerlinNoiseVarCache : public VarCache
    {
        grcEffectVar m_noise1ID;
        grcEffectVar m_phaseID;
		grcEffectVar m_noiseWarpID;
		grcEffectVar m_clumpBumpID;
		
        void CacheEffectVars( grmShader& effect )
        {
            m_noise1ID = effect.LookupVar( "Noise1Tex", IsPedantic() );
            m_phaseID = effect.LookupVar( "Phase" , IsPedantic());
			m_noiseWarpID = effect.LookupVar( "NoiseWarp", IsPedantic() );
			m_clumpBumpID = effect.LookupVar( "ClumpBump", IsPedantic() );
        }
    };

public:
    SkyhatPerlinNoise()
        : m_time(0.0f)
        , m_phaseRate(0.1f)
        , m_pNoise1Tex(NULL)
        , m_on(true)
		, m_currentPhase( 0.0f )
		, m_cloudWarp( 12.0f)
    {
    }

    ~SkyhatPerlinNoise()
    {
    }

    bool UsesFragment( grmShader& effect )
    {
        return ( effect.LookupVar( "Phase" , false ) != 0 );
    }

    VarCache* CreateVariableCache()	{ return rage_new SkyhatPerlinNoiseVarCache; }

    void SetupShader( grmShader& effect, VarCache* cVars ) 
    {
		FastAssert( cVars );
		SkyhatPerlinNoiseVarCache* var = static_cast<SkyhatPerlinNoiseVarCache*>(cVars);

		effect.SetVar( var->m_noise1ID, m_pNoise1Tex );
		effect.SetVar( var->m_phaseID, m_currentPhase );//m_phaseRate * m_time );
		effect.SetVar( var->m_noiseWarpID, m_cloudWarp ); 
		effect.SetVar( var->m_clumpBumpID, m_sunSetBump );
    }

    void SetupShaderPass() 
    {}

    // public interface
    void SetTime( float t ) 
	{ 
		m_currentPhase +=( t - m_time ) * m_phaseRate;
		m_time = t; 
	}
    float GetTime() const { return m_time; }

    void SetPhaseRate( float phaseRate ) { m_phaseRate = phaseRate; }
    float GetPhaseRate() const { return m_phaseRate; }

	void SetNoise1Tex(  atPtr<grcTexture> noise1 )	{ m_pNoise1Tex = noise1; }
    grcTexture* GetNoise1Tex() const { return m_pNoise1Tex; }

	void SetCloudWarp( float cloudWarp) { m_cloudWarp = cloudWarp; }

	void SetSunSetBump( float sunSetBump ) { m_sunSetBump = sunSetBump; }

    PAR_PARSABLE;

private:
    float m_time;
	float m_currentPhase;
    float m_phaseRate;
	float m_cloudWarp;
	float m_sunSetBump;

    grcTexture* m_pNoise1Tex;

    bool m_on;
};

inline float Saturate( float a )
{
	return Clamp( a, 0.0f, 1.0f );
}
inline Vector3 VLerp( const Vector3& a, const Vector3& b, float t )
{
	Vector3 tV( t, t, t );
	Vector3 res = a;
	res.Lerp(tV, b );
	return res;
}
inline Vector4 VLerp( const Vector4& a, const Vector4& b, float t )
{
	Vector4 tV( t, t, t, t );
	Vector4 res = a;
	res.Lerp(tV, b );
	return res;
}

//----------------------- lighting controllers

//
//	PURPOSE
//		Sets Sun and Moon directional lights with specular in a fast direct way
//

class DayLighting : public ShaderFragment
{
	struct DayLightingVarCache : public VarCache
	{
		grcEffectVar m_lightDirection1ID;
		grcEffectVar m_lightHalfAngle1ID;
		grcEffectVar m_lightDirection2ID;
		grcEffectVar m_lightHalfAngle2ID;

		void CacheEffectVars( grmShader& effect )
		{
			m_lightDirection1ID = effect.LookupVar( "lightDirection1", IsPedantic() );
			m_lightHalfAngle1ID = effect.LookupVar( "lightHalfAngle1", IsPedantic() );
			m_lightDirection2ID = effect.LookupVar( "lightDirection2" , IsPedantic());
			m_lightHalfAngle2ID = effect.LookupVar( "lightHalfAngle2" , IsPedantic());
		}
	};

public:
	DayLighting()
	{}

	bool UsesFragment( grmShader& effect )
	{
		return ( effect.LookupVar( "lightHalfAngle1" , false ) != 0 );
	}

	VarCache* CreateVariableCache()	{ return rage_new DayLightingVarCache; }

	void SetupShader( grmShader& effect, VarCache* cVars ) 
	{
		FastAssert( cVars );
		DayLightingVarCache* var = static_cast<DayLightingVarCache*>(cVars);

		Vector3 direction;
		Vector3 half;
		GetLightValues( 0 , direction, half );
		effect.SetVar( var->m_lightDirection1ID, direction );
		effect.SetVar( var->m_lightHalfAngle1ID, half );

		GetLightValues( 1 , direction, half );
		effect.SetVar( var->m_lightDirection2ID, direction );
		effect.SetVar( var->m_lightHalfAngle2ID, half );
	}

	void SetupShaderPass() 
	{}

	PAR_PARSABLE;


private:
	void GetLightValues( int id , Vector3& dir, Vector3& half )
	{
		grcLightGroup* lgrp = grcState::GetLightingGroup();
		dir = VEC3V_TO_VECTOR3(lgrp->GetDirection( id ));

		if ( grcViewport::GetCurrent() )
		{
			Vector3 view = VEC3V_TO_VECTOR3(grcViewport::GetCurrent()->GetCameraMtx().GetCol2());

			half = dir - view;
			half.NormalizeSafe();
		}
		else
		{
			half.Zero();
		}
	}
};

//
//	PURPOSE
//		set up screen space tesselation information for an object
//
class ScreenSpaceTesselatorControl : public ShaderFragment
{
	struct ScreenSpaceTesselatorControlVarCache : public VarCache
	{
		grcEffectVar	m_ScrTesselationCorners;
		grcEffectVar	m_ScrTesselationClippers;
		grcEffectVar	m_ScrTesselationUV;
		grcEffectVar	m_NearFarPlane;

		void CacheEffectVars( grmShader& effect )
		{
			m_ScrTesselationCorners			=	effect.LookupVar("Scene_ScrTesselationCorners");
			m_ScrTesselationClippers		=	effect.LookupVar("Scene_ScrTesselationClippers");
			m_ScrTesselationUV				=	effect.LookupVar("Scene_ScrTesselationUV");
			m_NearFarPlane					= 	effect.LookupVar("NearFarPlane");
		}
	};

public:

	ScreenSpaceTesselatorControl( rmcDrawable *tesselateable, const Matrix34 *worldTransform = NULL ) : m_Tesselateable( tesselateable ), m_WorldTransform( worldTransform )
	{
		FastAssert( tesselateable );
		
		Vector2 CornerUV[4];
		CornerUV[0] = Vector2( 1.0f, 1.0f );
		CornerUV[1] = Vector2( 0, 1.0f );
		CornerUV[2] = Vector2( 0, 0 );
		CornerUV[3] = Vector2( 1.0f, 0 );

		grcVertexBuffer * vertB = m_Tesselateable->GetLodGroup().GetModel(0).GetGeometry(0).GetVertexBuffer();
		grcVertexBufferEditor vertBEditor(vertB);

		int nVert = vertB->GetVertexCount();
		int iVert;
		for( iVert = 0; iVert < nVert; iVert++ )
		{
			float u = vertBEditor.GetUV( iVert, 0 ).x;
			float v = vertBEditor.GetUV( iVert, 0 ).y;

			if( u - FLT_EPSILON <= CornerUV[0].x && v - FLT_EPSILON <= CornerUV[0].y )
			{
				m_CornersWorld[0] = vertBEditor.GetPosition( iVert );
				CornerUV[0] = Vector2( u, v );
			}

			if( u + FLT_EPSILON >= CornerUV[1].x && v - FLT_EPSILON <= CornerUV[1].y )
			{
				m_CornersWorld[1] = vertBEditor.GetPosition( iVert );
				CornerUV[1] = Vector2( u, v );
			}

			if( u + FLT_EPSILON >= CornerUV[2].x && v + FLT_EPSILON >= CornerUV[2].y )
			{
				m_CornersWorld[2] = vertBEditor.GetPosition( iVert );
				CornerUV[2] = Vector2( u, v );
			}

			if( u - FLT_EPSILON <= CornerUV[3].x && v + FLT_EPSILON >= CornerUV[3].y )
			{
				m_CornersWorld[3] = vertBEditor.GetPosition( iVert );
				CornerUV[3] = Vector2( u, v );
			}


		}

		m_Normal.Cross( (m_CornersWorld[1] - m_CornersWorld[0]), (m_CornersWorld[3] - m_CornersWorld[0]));
		m_Normal.Normalize();
	}

	VarCache*	CreateVariableCache()	{ return rage_new ScreenSpaceTesselatorControlVarCache; }

	void ScreenToWorld( const Matrix44 &reverse, float x, float y, Vector3::Vector3Ref outNear, Vector3::Vector3Ref outFar )
	{
		Vector3 atNear(x,y,0);
		Vector3 atFar(x,y,1);
 
		outNear = reverse.FullTransform(atNear);
		outFar = reverse.FullTransform(atFar);
	}


	void SetupShader( grmShader& effect , VarCache* cachedVars )
	{
/*
		grcViewport* view = grcViewport::GetCurrent();

		if( view )
		{
			const Matrix44 &wvp = view->GetCompositeMtx();

			// set tesselation grid to match tesselation patch
			Vector3 patchVerts[8];

			Vector3 surfn;
			Vector3 p;
			if( m_WorldTransform )
			{
				m_WorldTransform->Transform( m_CornersWorld[0], patchVerts[0] );
				m_WorldTransform->Transform( m_CornersWorld[1], patchVerts[2] );
				m_WorldTransform->Transform( m_CornersWorld[2], patchVerts[4] );
				m_WorldTransform->Transform( m_CornersWorld[3], patchVerts[6] );
				m_WorldTransform->Transform3x3( m_Normal, surfn );
			}
			else
			{
				patchVerts[0] = m_CornersWorld[0];
				patchVerts[2] = m_CornersWorld[1];
				patchVerts[4] = m_CornersWorld[2];
				patchVerts[6] = m_CornersWorld[3];

				surfn = m_Normal;
			}


			p = tesselationGrid[0];

			wvp.Transform( Vector4(tesselationGrid[0].x,tesselationGrid[0].y,tesselationGrid[0].z,1.0f), tesselationGridScreen[0] );
			wvp.Transform( Vector4(tesselationGrid[1].x,tesselationGrid[1].y,tesselationGrid[1].z,1.0f), tesselationGridScreen[1] );
			wvp.Transform( Vector4(tesselationGrid[2].x,tesselationGrid[2].y,tesselationGrid[2].z,1.0f), tesselationGridScreen[2] );
			wvp.Transform( Vector4(tesselationGrid[3].x,tesselationGrid[3].y,tesselationGrid[3].z,1.0f), tesselationGridScreen[3] );

			tesselationGridScreen[0] /= (tesselationGridScreen[0].w);
			tesselationGridScreen[1] /= (tesselationGridScreen[1].w);
			tesselationGridScreen[2] /= (tesselationGridScreen[2].w);
			tesselationGridScreen[3] /= (tesselationGridScreen[3].w);

			{
				float minX, maxX, minY, maxY;

				minX = Min( tesselationGridScreen[0].x, tesselationGridScreen[1].x, tesselationGridScreen[2].x, tesselationGridScreen[3].x );
				minY = Min( tesselationGridScreen[0].y, tesselationGridScreen[1].y, tesselationGridScreen[2].y, tesselationGridScreen[3].y );
				maxX = Max( tesselationGridScreen[0].x, tesselationGridScreen[1].x, tesselationGridScreen[2].x, tesselationGridScreen[3].x );
				maxY = Max( tesselationGridScreen[0].y, tesselationGridScreen[1].y, tesselationGridScreen[2].y, tesselationGridScreen[3].y );

				// add a slight border to keep grid artifact from showing on edges.  
				// This grid artifact will still appear on the original mesh's world-space corners, however
				minX = Max( -1.05f, minX );
				minY = Max( -1.05f, minY );
				maxX = Min( 1.05f, maxX );
				maxY = Min( 1.05f, maxY );

				tesselationGridScreen[0].x = maxX;
				tesselationGridScreen[0].y = maxY;
				tesselationGridScreen[1].x = minX;
				tesselationGridScreen[1].y = maxY;
				tesselationGridScreen[2].x = minX;
				tesselationGridScreen[2].y = minY;
				tesselationGridScreen[3].x = maxX;
				tesselationGridScreen[3].y = minY;

			}

			// project our screen space coord
			Vector3 nearPoint, farPoint;
			grcWorldIdentity();

			Matrix44 reverse;
			reverse.Inverse(grcViewport::GetCurrent()->GetCompositeMtx());

			spdPlane plane( p, surfn );
			//			Vector4 waveHeightOffset( 0,0,0,maxWaveHeight );

			//			spdPlane offsetPlane;

			//			offsetPlane.SetCoefficientVector( plane.GetCoefficientVector() - waveHeightOffset );
			Vector3 cornerProj;
			Vector4 corners[4];
			Vector4 clippers[4];
			int iCorner;
			for( iCorner = 0; iCorner < 4; iCorner++ )
			{
				ScreenToWorld( reverse,tesselationGridScreen[iCorner].x, tesselationGridScreen[iCorner].y, nearPoint, farPoint );
				if( plane.IntersectLine( nearPoint, farPoint, cornerProj ) )
					corners[iCorner].Set( cornerProj.x, cornerProj.y, cornerProj.z, 1.0f );
				else
					FastAssert(0);
				// calculate the depth in worldspace
				Vector4 pw;
				wvp.Transform(corners[iCorner], pw);

				// precompute CornerPoint/depth
				pw.w = 1.0f/pw.w;
				corners[iCorner].Scale( pw.w );
				corners[iCorner].w = pw.w;

				Vector3 edge;
				// calculate the clipping planes, so we can discard verts in the tesselated grid that are outside the tesselated patch
				edge = tesselationGrid[(iCorner + 1)%4] - tesselationGrid[iCorner];
				Vector3 n;
				n.Cross( edge, surfn );
				n.Normalize();
				clippers[iCorner] = spdPlane( tesselationGrid[iCorner], n ).GetCoefficientVector();

			}

			ScreenSpaceTesselatorControlVarCache*		vars = static_cast<ScreenSpaceTesselatorControlVarCache*>(cachedVars);
			effect.SetVar( vars->m_ScrTesselationCorners, corners, 4 );
			effect.SetVar( vars->m_ScrTesselationClippers, clippers, 4 );

		}
*/



		grcViewport* view = grcViewport::GetCurrent();
		
		if( view )
		{
			const Matrix44 &wvp = RCC_MATRIX44(view->GetCompositeMtx());

			// set tesselation grid to match tesselation patch
			Vector3 tesselationGrid[4];
			Vector4 tesselationGridScreen[4];

			Vector2 CornerUV[4];
			CornerUV[0] = Vector2( 1.0f, 1.0f );
			CornerUV[1] = Vector2( 0, 1.0f );
			CornerUV[2] = Vector2( 0, 0 );
			CornerUV[3] = Vector2( 1.0f, 0 );

			Vector3 surfn;
			Vector3 p;
			if( m_WorldTransform )
			{
				m_WorldTransform->Transform( m_CornersWorld[0], tesselationGrid[0] );
				m_WorldTransform->Transform( m_CornersWorld[1], tesselationGrid[1] );
				m_WorldTransform->Transform( m_CornersWorld[2], tesselationGrid[2] );
				m_WorldTransform->Transform( m_CornersWorld[3], tesselationGrid[3] );
				m_WorldTransform->Transform3x3( m_Normal, surfn );
			}
			else
			{
				tesselationGrid[0] = m_CornersWorld[0];
				tesselationGrid[1] = m_CornersWorld[1];
				tesselationGrid[2] = m_CornersWorld[2];
				tesselationGrid[3] = m_CornersWorld[3];

				surfn = m_Normal;
			}

			// adjust for height of waves
			Vector3 waveOffset;//[4];
			{
				float waveHeightMax = 0.0;
				float waveHeightAdd;

				grcEffectVar evWaveHeight;
				evWaveHeight = effect.LookupVar( "DWaveAmp1", false );
				effect.GetVar( evWaveHeight, waveHeightAdd );
				waveHeightMax += waveHeightAdd;
				evWaveHeight = effect.LookupVar( "DWaveAmp2", false );
				effect.GetVar( evWaveHeight, waveHeightAdd );
				waveHeightMax += waveHeightAdd;
		//		evWaveHeight = effect.LookupVar( "DWaveAmp3", false );
		//		effect.GetVar( evWaveHeight, waveHeightAdd );
		//		waveHeightMax += waveHeightAdd;

				Vector3 eye = VEC3V_TO_VECTOR3(view->GetCameraPosition());

				// weird singularity around the wave height, and it gets bad below it as well, so turn off the wave height compensation
				if( (eye - tesselationGrid[0]).Dot( surfn ) < waveHeightMax*waveHeightMax*2.0f )
				{
					waveOffset.Zero();
				}
				else
				{
					waveOffset = surfn*waveHeightMax;				
				}

/*
				// have to account for lateral motion of trochoid waves
				Vector3 lateral;
				lateral = tesselationGrid[0] - tesselationGrid[1];
				lateral.NormalizeFast();
				lateral *= 2.0f;
				tesselationGrid[0] += lateral;
				tesselationGrid[1] -= lateral;
				tesselationGrid[2] += lateral;
				tesselationGrid[3] -= lateral;

				lateral = tesselationGrid[0] - tesselationGrid[3];
				lateral.NormalizeFast();
				lateral *= 2.0f;
				tesselationGrid[0] += lateral;
				tesselationGrid[1] += lateral;
				tesselationGrid[2] -= lateral;
				tesselationGrid[3] -= lateral;
*/
				//	float tessMin, tessMax;
				//	grcEffectVar evTessMin = effect.LookupVar( "tesselationDepthClampMin", false );
				//	grcEffectVar evTessMax = effect.LookupVar( "tesselationDepthClampMax", false );
				//	effect.GetVar( evTessMin, tessMin );
				//	effect.GetVar( evTessMax, tessMax );


			}



			p = tesselationGrid[0];

			float minX = FLT_MAX;
			float maxX = -FLT_MAX;
			float minY = FLT_MAX;
			float maxY = -FLT_MAX;

			Vector4 reversePlane(-1,-1,-1,1);
			Vector4 shiftPlane(0,0,0,0.0f);

			int nVert = 0;
//			view->ResetPerspective();
			if(1)
			{
				const int IN_SIZE = 4;
				const int OUT_SIZE = 8;

				Vector3 inPoly[IN_SIZE];
				Vector3 out[OUT_SIZE];
				Vector3 out2[OUT_SIZE];
				
				inPoly[0] = tesselationGrid[0] + waveOffset;
				inPoly[1] = tesselationGrid[1] + waveOffset;
				inPoly[2] = tesselationGrid[2] + waveOffset;
				inPoly[3] = tesselationGrid[3] + waveOffset;

				// need to do consider peak and trough of waves when clipping to screen
				int iTopBottom;
				for( iTopBottom = 0; iTopBottom < 2; iTopBottom++ )
				{

					Vector4 nc = VEC4V_TO_VECTOR4(view->GetFrustumClipPlane( grcViewport::CLIP_PLANE_NEAR ));
					spdPlane clipper(nc);
					clipper.SetCoefficientVector( clipper.GetCoefficientVector() * reversePlane );
					nVert = clipper.Clip(inPoly, IN_SIZE, out, OUT_SIZE );
					Vector3 *pOut = out;

					if( nVert > 0 )
					{
						clipper.SetCoefficientVector( VEC4V_TO_VECTOR4(view->GetFrustumClipPlane( grcViewport::CLIP_PLANE_LEFT )) );
						clipper.SetCoefficientVector( clipper.GetCoefficientVector() * reversePlane + shiftPlane );
						nVert = clipper.Clip( out, nVert, out2, OUT_SIZE );
						pOut = out2;
						if( nVert > 0 )
						{								
							clipper.SetCoefficientVector( VEC4V_TO_VECTOR4(view->GetFrustumClipPlane( grcViewport::CLIP_PLANE_RIGHT )) );
							clipper.SetCoefficientVector( clipper.GetCoefficientVector() * reversePlane + shiftPlane );
							nVert = clipper.Clip( out2, nVert, out, OUT_SIZE );
							pOut = out;
							if( nVert > 0 )
							{
								clipper.SetCoefficientVector( VEC4V_TO_VECTOR4(view->GetFrustumClipPlane( grcViewport::CLIP_PLANE_TOP )) );
								clipper.SetCoefficientVector( clipper.GetCoefficientVector() * reversePlane );
								nVert = clipper.Clip( out, nVert, out2, OUT_SIZE );
								pOut = out2;
								if( nVert > 0 )
								{
									clipper.SetCoefficientVector( VEC4V_TO_VECTOR4(view->GetFrustumClipPlane( grcViewport::CLIP_PLANE_BOTTOM )) );
									clipper.SetCoefficientVector( clipper.GetCoefficientVector() * reversePlane );
									nVert = clipper.Clip( out2, nVert, out, OUT_SIZE );
									pOut = out;
								}
							}
						}
					}
					
			//		pOut = &inPoly;
			//		nVert = 4;

					int iVert;
					for( iVert = 0; iVert < nVert; iVert++ )
					{
						pOut[iVert] -= waveOffset;
						Vector4 screenPoint;
						wvp.Transform( Vector4(pOut[iVert].x, pOut[iVert].y, pOut[iVert].z,1.0f), screenPoint );
						screenPoint /= (screenPoint.w);

						minX = Min( screenPoint.x, minX );
						minY = Min( screenPoint.y, minY );
						maxX = Max( screenPoint.x, maxX );
						maxY = Max( screenPoint.y, maxY );

					}

					// need to do consider peak and trough of waves when clipping to screen
					waveOffset.Negate();
					inPoly[0] = tesselationGrid[0] + waveOffset;
					inPoly[1] = tesselationGrid[1] + waveOffset;
					inPoly[2] = tesselationGrid[2] + waveOffset;
					inPoly[3] = tesselationGrid[3] + waveOffset;
				}

			}

			Vector4 corners[4];
			Vector4 clippers[4];
			Vector4 gridUV[4];

			if( nVert > 0 )
			{
				{

					// add a slight border to keep grid artifact from showing on edges.  
					// This grid artifact will still appear on the original mesh's world-space corners, however
				//	minX = Max( -1.05f, minX - 0.05f );
			//		minY = Max( -1.05f, minY - 0.05f );
			//		maxX = Min( 1.05f, maxX + 0.05f );
			//		maxY = Min( 1.05f, maxY + 0.05f );
			//		minX = Max( -1.0f, minX );
			//		minY = Max( -1.0f, minY );
			//		maxX = Min( 1.0f, maxX );
			//		maxY = Min( 1.0f, maxY );

					tesselationGridScreen[0].x = maxX;
					tesselationGridScreen[0].y = maxY;
					tesselationGridScreen[1].x = minX;
					tesselationGridScreen[1].y = maxY;
					tesselationGridScreen[2].x = minX;
					tesselationGridScreen[2].y = minY;
					tesselationGridScreen[3].x = maxX;
					tesselationGridScreen[3].y = minY;

				}

				// project our screen space coord
				Vector3 nearPoint, farPoint;
				grcWorldIdentity();

				Matrix44 reverse;
				reverse.Inverse(RCC_MATRIX44(grcViewport::GetCurrent()->GetCompositeMtx()));

				spdPlane plane( p, surfn );
				//			Vector4 waveHeightOffset( 0,0,0,maxWaveHeight );

				//			spdPlane offsetPlane;

				//			offsetPlane.SetCoefficientVector( plane.GetCoefficientVector() - waveHeightOffset );
				Vector3 cornerProj;
				int iCorner;
				for( iCorner = 0; iCorner < 4; iCorner++ )
				{
					ScreenToWorld( reverse,tesselationGridScreen[iCorner].x, tesselationGridScreen[iCorner].y, nearPoint, farPoint );
					if( plane.IntersectLine( nearPoint, farPoint, cornerProj ) )
					{
						corners[iCorner].Set( cornerProj.x, cornerProj.y, cornerProj.z, 1.0f );
					}
					else
					{
//						FastAssert(0);
					}
					
					
					// calculate the depth in worldspace
					Vector4 pw;
					wvp.Transform(corners[iCorner], pw);

					// precompute CornerPoint/depth
					pw.w = 1.0f/pw.w;
					corners[iCorner].Scale( pw.w );
					corners[iCorner].w = pw.w;

					// calculate UVs
					gridUV[iCorner].x = (tesselationGrid[0] - tesselationGrid[1]).Dot( cornerProj - tesselationGrid[1] )/(tesselationGrid[0] - tesselationGrid[1]).Mag2();
					gridUV[iCorner].y = (tesselationGrid[0] - tesselationGrid[3]).Dot( cornerProj - tesselationGrid[3] )/(tesselationGrid[0] - tesselationGrid[3]).Mag2();
					gridUV[iCorner].Scale( pw.w );

					Vector3 edge;
					// calculate the clipping planes, so we can discard verts in the tesselated grid that are outside the tesselated patch
					edge = tesselationGrid[(iCorner + 1)%4] - tesselationGrid[iCorner];
					Vector3 n;
					n.Cross( edge, surfn );
					n.Normalize();
					clippers[iCorner] = spdPlane( tesselationGrid[iCorner], n ).GetCoefficientVector();

					
				}
			}
			else
			{
				corners[0] = corners[1] = corners[2] = corners[3] = Vector4(0,0,0,0);
				clippers[0] = clippers[1] = clippers[2] = clippers[3] = Vector4(0,0,0,0);
			}

			ScreenSpaceTesselatorControlVarCache* vars = static_cast<ScreenSpaceTesselatorControlVarCache*>(cachedVars);
			effect.SetVar( vars->m_ScrTesselationCorners, corners, 4 );
			effect.SetVar( vars->m_ScrTesselationClippers, clippers, 4 );
			effect.SetVar( vars->m_ScrTesselationUV, gridUV, 4 );
		
			// Near/Far Plane for refraction component of water shader
			grcViewport* viewport = grcViewport::GetCurrent();
			Vector2 NearFarPlane(viewport->GetNearClip(), viewport->GetFarClip());
			effect.SetVar(vars->m_NearFarPlane, NearFarPlane);

		}
	}
	void SetupShaderPass()		{}

	bool UsesFragment( grmShader& effect )
	{
		return ( effect.LookupVar( "Scene_ScrTesselationCorners" , false ) != 0 );
	}
private:

	rmcDrawable *m_Tesselateable;
	const Matrix34 *m_WorldTransform;
	Vector3 m_CornersWorld[4];
	Vector3 m_Normal;
};



} // namespace rage

#endif
