//
// shadercontrollers/todlight.cpp
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//
// PURPOSE:
//		This class defines a time of day directional "sun" light -controlling orbit position and keyframed colors

#include "todlight.h"

#include "file/asset.h"
#include "file/token.h"
#include "grcore/im.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "parser/manager.h"
#include "parser/rtstructure.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/vec.h"
#include "grmodel/model.h"

using namespace rage;


#include "todlight_parser.h"


#if __BANK
bool ltSampleTODLight::sm_OverrideAmbient=false;
#endif

/***** ltSampleLightKeyFrame **********************************/
/***************************************************************/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ltSampleTODLight::ltSampleTODLight()
{
	m_Enabled=	true;
	m_Matrix.Identity();

	m_TimeOfDay = 16.0f;
	m_DayOrbitSpeed = 0.0f;
	m_DayOrbitSpeed2 = 0.0f;
	m_RateChangeTime = 2.0f;
	m_NightOrbitSpeed =0.0f;

	sprintf(m_Name,"TODSun");

	//Vars to visulize the light
	m_Draw= true;
	m_Texture =(grcTexture*)grcTexture::None;

	m_DepthTextureID=grcegvNONE;
	m_ColorTextureID=grcegvNONE;
	m_ShadowColorID=grcegvNONE;
	m_LightMatrixID=grcegvNONE;
	m_PixelBlurID=grcegvNONE;
	m_ZOffsetID=grcegvNONE;
	m_LightDirID=grcegvNONE;
	m_LightDepthMap=0;
	m_LightColorMap=0;
	m_ShadowColor.Zero();
	m_ShadowBlur=0.0f;
	m_ShadowZOffset=0.0f;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ltSampleTODLight::~ltSampleTODLight()
{
	if(m_Texture)
		m_Texture->Release();
	m_Texture=0;

#if __BANK
	delete m_CBTimeChanged;
	m_CBTimeChanged=0;
#endif

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::Init(const char*name)
{
	sprintf(m_Name,"%s",name);
	formatf(m_TextureName,sizeof(m_Texture),"SunGlowTexture");
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::Update()
{
	//Now we rely on someone else telling us what time it is

	//Night is twice as fast as day
	/*if(m_TimeOfDay>12.0f)
		m_TimeOfDay+=m_NightOrbitSpeed*TIME.GetSeconds();
	else
	{
		if(m_TimeOfDay>m_RateChangeTime)
			m_TimeOfDay+=m_DayOrbitSpeed2*TIME.GetSeconds();
		else
			m_TimeOfDay+=m_DayOrbitSpeed*TIME.GetSeconds();
	}

	if(m_TimeOfDay>=24.0f)
		m_TimeOfDay=0.0f;
	if(m_TimeOfDay<0.0f)
		m_TimeOfDay=24.0f;
		
		SetFromTime();
		*/
	SetFromTime();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::SetFromTime()
{
	ScalarV vTimeOfDay = ScalarVFromF32(m_TimeOfDay);
	Vec4V vKeyData;
	
	vKeyData = m_KeyColor.Query(vTimeOfDay);
	m_LightColor = RCC_VECTOR4(vKeyData);

	vKeyData = m_KeyAmbLight.Query(vTimeOfDay);
	m_AmbientColor = RCC_VECTOR4(vKeyData);

	vKeyData = m_KeyShadowColor.Query(vTimeOfDay);
	m_ShadowColor = RCC_VECTOR4(vKeyData);
	m_ShadowColor.SplatX();
	m_ShadowColor.w = 1.0f;
#if __BANK
	if (sm_OverrideAmbient)
	{
		m_AmbientColor.Set(1.0f,1.0f,1.0f,1.0f);
	}
#endif

	vKeyData = m_KeyDistance.Query(vTimeOfDay);
	Vector4 distance = RCC_VECTOR4(vKeyData);

	vKeyData = m_KeyOrbit.Query(vTimeOfDay);
	Vector4 orbit = RCC_VECTOR4(vKeyData);
	
	Vector3 pos(0.0f,distance.GetX(),0.0f);
	
	pos.RotateZ(orbit.GetX()*DtoR);
	pos.RotateX(orbit.GetY()*DtoR);

	//Look at origin
	m_Matrix.Identity();
	m_Matrix.d.Set(pos);
	m_Matrix.LookAt(Vector3(0.0f,0.0f,0.0f),YAXIS);

	Vector3 A,B,C(pos);
	A.Set(0.0f,0.0f,-1.0f);
	C.Normalize();
	B.Cross(A,C);
	A.Cross(C,B);
	m_Matrix.a.Set(A);
	m_Matrix.b.Set(B);
	m_Matrix.c.Set(C);
	m_Matrix.d.Set(pos);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::GetOrbit(Vector4 &orbit)
{
	Vec4V vKeyData = m_KeyOrbit.Query(ScalarVFromF32(m_TimeOfDay));
	orbit = RCC_VECTOR4(vKeyData);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::Draw()
{
	if(!(m_Enabled && m_Draw) )
		return;
	grmModel::SetForceShader(0);

	//DrawSun
	Vec4V vKeyData = m_KeySunSize.Query(ScalarVFromF32(m_TimeOfDay));
	Vector4 sunSize = RCC_VECTOR4(vKeyData);

	const Matrix34 & matrix = RCC_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());
	Vector3 A,B,C,D;
	float radius=sunSize.GetX();

	A.Set(m_Matrix.d);
	B.Set(m_Matrix.d);
	C.Set(m_Matrix.d);
	D.Set(m_Matrix.d);

	A.Add(matrix.d);
	B.Add(matrix.d);
	C.Add(matrix.d);
	D.Add(matrix.d);
	
	Vector3 Up(matrix.b);
	Vector3 Left(matrix.a);

	A.AddScaled(Left,radius);
	A.AddScaled(Up,-radius);
	B.AddScaled(Left,radius);
	B.AddScaled(Up,radius);
	C.AddScaled(Left,-radius);
	C.AddScaled(Up,-radius);
	D.AddScaled(Left,-radius);
	D.AddScaled(Up,radius);

	vKeyData = m_KeyAmbLight.Query(ScalarVFromF32(m_TimeOfDay));
	Vector4 ambColordata = RCC_VECTOR4(vKeyData);

	vKeyData = m_KeyGlowColorMod.Query(ScalarVFromF32(m_TimeOfDay));
	Vector4 glowColorMod = RCC_VECTOR4(vKeyData);

	Vector3 ambColor(ambColordata.GetX()+glowColorMod.GetX(),ambColordata.GetY()+glowColorMod.GetY(),ambColordata.GetZ()+glowColorMod.GetZ());
	//ambColor.Scale(glowColorMod.GetW());
	ambColor.x = Clamp(ambColor.x,0.0f,1.0f);
	ambColor.y = Clamp(ambColor.y,0.0f,1.0f);
	ambColor.z = Clamp(ambColor.z,0.0f,1.0f);
	ambColor.w = Clamp(ambColor.w,0.0f,1.0f);

	Color32 c = Color32(ambColor.x,ambColor.y,ambColor.z,1.0f);

	/*grcState::Default();
	grcState::Default();
	grcViewport::SetCurrentWorldIdentity();
	grcState::SetAlphaFunc(grcafAlways);
	grcState::SetDepthTest(false);
	grcState::SetDepthWrite(false);
	grcState::SetAlphaBlend(true);
	grcState::SetAlphaTest(false);
	//grcState::SetBlendSet(grcbsAlphaAdd);
	grcState::SetBlendSet(grcbsNormal);
	grcLightState::SetEnabled(false);
	grcBindTexture( m_Texture);
	grcState::SetCullMode(grccmNone);
	
*/
	//grcEffect::SetDefaultRenderState(grcersZWRITEENABLE, false);
	//grcEffect::SetDefaultRenderState(grcersZENABLE, false);
	//grcEffect::SetDefaultRenderState(grcersALPHABLENDENABLE, true);
	//grcEffect::SetDefaultRenderState(grcersALPHATESTENABLE,false);
	grcState::SetDepthWrite(false);
	grcState::SetDepthTest(true);
	grcState::SetBlendSet(grcbsAlphaAdd);
	grcLightState::SetEnabled(false);
	grcState::SetAlphaBlend(true);
	//grcBindTexture( m_Texture);
	grcState::SetCullMode(grccmNone);
	grcBindTexture(m_Texture);
	grcViewport::SetCurrentWorldIdentity();
	

	grcBegin(drawTriStrip,4);
	grcVertex(A.x,A.y,A.z,0,0,0,c,0.0f,0.0f);
	grcVertex(B.x,B.y,B.z,0,0,0,c,1.0f,0.0f);
	grcVertex(C.x,C.y,C.z,0,0,0,c,0.0f,1.0f);
	grcVertex(D.x,D.y,D.z,0,0,0,c,1.0f,1.0f);
	grcEnd();



	radius=sunSize.GetY();
	A.Set(m_Matrix.d);
	B.Set(m_Matrix.d);
	C.Set(m_Matrix.d);
	D.Set(m_Matrix.d);

	A.Add(matrix.d);
	B.Add(matrix.d);
	C.Add(matrix.d);
	D.Add(matrix.d);


	A.AddScaled(matrix.a,radius);
	A.AddScaled(matrix.b,-radius);
	B.AddScaled(matrix.a,radius);
	B.AddScaled(matrix.b,radius);
	C.AddScaled(matrix.a,-radius);
	C.AddScaled(matrix.b,-radius);
	D.AddScaled(matrix.a,-radius);
	D.AddScaled(matrix.b,radius);

	vKeyData = m_KeyColor.Query(ScalarVFromF32(m_TimeOfDay));
	Vector4 lightColor = RCC_VECTOR4(vKeyData);

	vKeyData = m_KeySunColorMod.Query(ScalarVFromF32(m_TimeOfDay));
	Vector4 sunColorMod = RCC_VECTOR4(vKeyData);

	Vector3 lColor(lightColor.GetX()+sunColorMod.GetX(),lightColor.GetY()+sunColorMod.GetY(),lightColor.GetZ()+sunColorMod.GetZ());
	//lColor.Scale(sunColorMod.GetW());

	lColor.x = Clamp(lColor.x,0.0f,1.0f);
	lColor.y = Clamp(lColor.y,0.0f,1.0f);
	lColor.z = Clamp(lColor.z,0.0f,1.0f);
	lColor.w = Clamp(lColor.w,0.0f,1.0f);
	
	c = Color32(lColor.x,lColor.y,lColor.z,1.0f);

	grcBindTexture( m_Texture);
	
	grcBegin(drawTriStrip,4);
	grcVertex(A.x,A.y,A.z,1.0f,1.0f,1.0f,c,0.0f,0.0f);
	grcVertex(B.x,B.y,B.z,1.0f,1.0f,1.0f,c,1.0f,0.0f);
	grcVertex(C.x,C.y,C.z,1.0f,1.0f,1.0f,c,0.0f,1.0f);
	grcVertex(D.x,D.y,D.z,1.0f,1.0f,1.0f,c,1.0f,1.0f);
	grcEnd();
	//grcLightState::SetEnabled(true);
	//grcState::SetDepthWrite(true);
	//grcState::SetBlendSet(grcbsNormal);
	//grcState::Default();
	//

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	PURPOSE
//		Loads settings from a file directly.
//
void ltSampleTODLight::Load()
{
	parRTStructure		m_parRTS;		
	m_parRTS.SetName("TODLight");		
	m_parRTS.AddStructureInstance("Info",*this);
	m_parRTS.AddStructureInstance("m_KeyClockTime",m_KeyClockTime);
	m_parRTS.AddStructureInstance("m_KeySunSize",m_KeySunSize);
	m_parRTS.AddStructureInstance("m_KeySunColorMod",m_KeySunColorMod);
	m_parRTS.AddStructureInstance("m_KeyGlowColorMod",m_KeyGlowColorMod);
	m_parRTS.AddStructureInstance("m_KeyColor",m_KeyColor);
	m_parRTS.AddStructureInstance("m_KeyAmbLight",m_KeyAmbLight);
	m_parRTS.AddStructureInstance("m_KeyOrbit",m_KeyOrbit);
	m_parRTS.AddStructureInstance("m_KeyDistance",m_KeyDistance);
	m_parRTS.AddStructureInstance("m_KeyShadowColor",m_KeyShadowColor);
    
	char filename[256];
	formatf(filename,sizeof(filename),"%s",m_Name);

	ASSET.PushFolder("$\\tune\\lighting");
	PARSER.LoadFromStructure(filename, "TODLight", m_parRTS, NULL);
	m_Texture = grcTextureFactory::GetInstance().Create(m_TextureName);

	ASSET.PopFolder();

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if __BANK
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::TimeChanged(ltSampleTODLight * /*light*/)
{
	//Tell all the keyframes that the time has changed
	//light->m_KeyClockTime.SetTuneTime(light->m_TimeOfDay);
	//light->m_KeySunSize.SetTuneTime(light->m_TimeOfDay);
	//light->m_KeySunColorMod.SetTuneTime(light->m_TimeOfDay);
	//light->m_KeyGlowColorMod.SetTuneTime(light->m_TimeOfDay);
	//light->m_KeyColor.SetTuneTime(light->m_TimeOfDay);
	//light->m_KeyAmbLight.SetTuneTime(light->m_TimeOfDay);
	//light->m_KeyOrbit.SetTuneTime(light->m_TimeOfDay);
	//light->m_KeyDistance.SetTuneTime(light->m_TimeOfDay);
	//light->m_KeyShadowColor.SetTuneTime(light->m_TimeOfDay);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::CallSave(ltSampleTODLight * b)
{
	b->Save();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::CallLoad(ltSampleTODLight * b)
{
	b->Load();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::AddStaticWidgets(bkBank& bank)
{
	bank.AddToggle("Override Ambient",&sm_OverrideAmbient);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ltSampleTODLight::AddWidgets(bkBank&bank,const char * name)
{
	if(name==0)
		bank.PushGroup(m_Name,false);
	else
		bank.PushGroup(name,false);

	bank.AddButton("Save",datCallback(CFA1(CallSave),this));
	bank.AddButton("Load",datCallback(CFA1(CallLoad),this));

	bank.AddToggle("Enabled",&m_Enabled);
	
	m_CBTimeChanged = rage_new datCallback(CFA1(TimeChanged),this);
	
	//bank.AddSlider("KeyframeTime",&m_TimeOfDay,0.0f,24.0f,0.015625f,*m_CBTimeChanged);
	bank.PushGroup("Distance",false);
	ptxKeyframeDefn data;
// 	data.SetLabels("Sun Distance");
// 	data.SetYRangeMinMax(-10000.0f, 10000.0f);
// 	data.SetXRangeMinMax(0.0f,24.0f);
// 	data.SetXYDelta(0.01f,0.01f);
	//m_KeyDistance.AddWidgets(bank,"Sun Distance",PTXKEYFRAMETYPE_FLOAT,data);
	bank.PopGroup();

	/* $Santoki$ these controls are disconnected, so don't show them to avoid confusing art.
	bank.PushGroup("SunCard",false);
	bank.AddText("Sun Texture",m_TextureName,sizeof(m_TextureName));
	bank.AddToggle("Draw Sun Card",&m_Draw);
	bank.PushGroup("Size",false);
	data.SetLabels("Sun Card Size","Glow Card Size");
	data.m_YRangeMinMax.Set(0.0f,1000.0f);
	data.m_XRangeMinMax.Set(0.0f,24.0f);
	data.m_DeltaXY.Set(0.01f,0.01f);
	m_KeySunSize.AddWidgets(bank,"Sun Size",PTXKEYFRAMETYPE_FLOAT2,data);
	bank.PopGroup();
	bank.PushGroup("Sun CardColor",false);
	data.SetLabels("Red","Green","Blue","Intensity");
	data.m_YRangeMinMax.Set(-2.0f,2.0f);
	data.m_XRangeMinMax.Set(0.0f,24.0f);
	data.m_DeltaXY.Set(0.01f,0.01f);
	m_KeySunColorMod.AddWidgets(bank,"Sun Color Mod",PTXKEYFRAMETYPE_FLOAT4,data);
	bank.PopGroup();
	bank.PushGroup("Glow Card Color",false);
	data.SetLabels("Red","Green","Blue","Intensity");
	data.m_YRangeMinMax.Set(-2.0f,2.0f);
	data.m_XRangeMinMax.Set(0.0f,24.0f);
	data.m_DeltaXY.Set(0.01f,0.01f);
	m_KeyGlowColorMod.AddWidgets(bank,"Glow Color Mod",PTXKEYFRAMETYPE_FLOAT4,data);
	bank.PopGroup();
	bank.PopGroup();
	*/

	bank.PushGroup("Orbit",false);
	ptxKeyframeDefn* pOrbitDefn = rage_new ptxKeyframeDefn
	(
		"Orbit Pos", 
		PTXKEYFRAMETYPE_FLOAT2, 
		-360.0f, 360.0f, 0.01f, 
		0.0f, 24.0f, 0.01f,	
		"Orbit Pos"
	);
	m_KeyOrbit.SetDefn(pOrbitDefn);
	m_KeyOrbit.AddWidgets(bank);
	bank.PopGroup();
	
	bank.PushGroup("Color",false);
	ptxKeyframeDefn* pColorDefn = rage_new ptxKeyframeDefn
	(
		"Light Color", PTXKEYFRAMETYPE_FLOAT4,
		-2.0f, 2.0f, 0.01f,
		0.0f, 24.0f, 0.01f,
		"Red", "Green", "Blue", "Intensity"
		);
	m_KeyColor.SetDefn(pColorDefn);
	m_KeyColor.AddWidgets(bank);
	bank.PopGroup();

	/* $Santoki$ shadow color has been removed from our shaders: use light color's intensity to control the thickness of shadows.  the shadow color itself can no longer be controlled.
	bank.PushGroup("Shadow Color");
	data.SetLabels("Shadow Color");
	data.m_YRangeMinMax.Set(-1.0f, 1.0f);
	m_KeyShadowColor.AddWidgets(bank,"Shadow Color", PTXKEYFRAMETYPE_FLOAT, data);
	bank.PopGroup();
	*/
	//m_KeyAmbLight.AddWidgets(bank,"Ambient Color",PTXKEYFRAMETYPE_FLOAT4,-10.0f,10.0f,0.01f);

	bank.PushGroup("Shadow Color");
	ptxKeyframeDefn* pShadowColorDefn = rage_new ptxKeyframeDefn
	(
		"Shadow Color", PTXKEYFRAMETYPE_FLOAT,
		0.0f, 10.0f, 0.01f,
		0.0f, 24.0f, 0.01f,
		"Shadow Color"
		);
	m_KeyShadowColor.SetDefn(pShadowColorDefn);
	m_KeyShadowColor.AddWidgets(bank);
	bank.PopGroup();

	//bank.AddSlider("DayTimeRate",&m_DayOrbitSpeed,-100.0f,100.0f,0.0001f);
	//bank.AddSlider("DayTimeRate2",&m_DayOrbitSpeed2,-100.0f,100.0f,0.0001f);
	//bank.AddSlider("TimeToSwitchToRate2",&m_RateChangeTime,0.0f,24.0f,0.001f);
	//bank.AddSlider("NightTimeShift",&m_NightOrbitSpeed,-100.0f,100.0f,0.01f);
	bank.PopGroup();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	PURPOSE
//		Saves settings to a file directly.
//
void ltSampleTODLight::Save()
{		
	parRTStructure m_parRTS;
	m_parRTS.SetName("TODLight");
	m_parRTS.AddStructureInstance("Info",*this);
	m_parRTS.AddStructureInstance("m_KeyClockTime",m_KeyClockTime);
	m_parRTS.AddStructureInstance("m_KeySunSize",m_KeySunSize);
	m_parRTS.AddStructureInstance("m_KeySunColorMod",m_KeySunColorMod);
	m_parRTS.AddStructureInstance("m_KeyGlowColorMod",m_KeyGlowColorMod);
	m_parRTS.AddStructureInstance("m_KeyColor",m_KeyColor);
	m_parRTS.AddStructureInstance("m_KeyAmbLight",m_KeyAmbLight);
	m_parRTS.AddStructureInstance("m_KeyOrbit",m_KeyOrbit);
	m_parRTS.AddStructureInstance("m_KeyDistance",m_KeyDistance);
	m_parRTS.AddStructureInstance("m_KeyShadowColor",m_KeyShadowColor);

	char filename[256];
	formatf(filename,sizeof(filename),"%s",m_Name);

	ASSET.PushFolder("$\\tune\\lighting");
	PARSER.SaveFromStructure(filename, "TODLight", m_parRTS);
	ASSET.PopFolder();
}

////////////////////////////////////////////////////////////////////////////////
//	PURPOSE
//		Fill in parser structure for saving/loading.  Used externally
//
void ltSampleTODLight::Create(parRTStructure& m_parRTS)
{
	m_parRTS.SetName("TODLight");
	m_parRTS.AddStructureInstance("Info",*this);
	m_parRTS.AddStructureInstance("m_KeyClockTime",m_KeyClockTime);
	m_parRTS.AddStructureInstance("m_KeySunSize",m_KeySunSize);
	m_parRTS.AddStructureInstance("m_KeySunColorMod",m_KeySunColorMod);
	m_parRTS.AddStructureInstance("m_KeyGlowColorMod",m_KeyGlowColorMod);
	m_parRTS.AddStructureInstance("m_KeyColor",m_KeyColor);
	m_parRTS.AddStructureInstance("m_KeyAmbLight",m_KeyAmbLight);
	m_parRTS.AddStructureInstance("m_KeyOrbit",m_KeyOrbit);
	m_parRTS.AddStructureInstance("m_KeyDistance",m_KeyDistance);
	m_parRTS.AddStructureInstance("m_KeyShadowColor",m_KeyShadowColor);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif // __BANK
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
