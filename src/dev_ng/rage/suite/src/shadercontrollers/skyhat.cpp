// 
// shadercontrollers/skyhat.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if 0

#include "shadercontrollers\grcphenomna.h"
#include "shadercontrollers\grcproceduralphenomna.h"
#include "skyhat.h"
#include "skyDomeData.h"
#include "embeddedmodel.h"
#include "grcore/vertexbuffer.h"
#include "skyhat_parser.h"
#include "math/random.h"
// #include "grpostfx/postfx.h"
#include "parser/manager.h"
#include "system\codecheck.h"
//
#if __XENON
#include "system\xtl.h"
#endif

static const char* s_DefaultDetailTextureFName =  "$/sample_skydome/textures/NOISE16_p.dds";
static const char* s_DefaultDetailBumpFName=  "$/sample_skydome/textures/detailBump2.dds";
static const char* s_DefaultStarsFName=  "$/sample_skydome/textures/starfield.dds";
static const char* s_DefaultGalaxyFName=  "$/sample_skydome/textures/galaxy.dds";
static const char* s_DefaultMoonFName=  "$/sample_skydome/textures/moon.dds";
static const char* s_DefaultMoonGlowFName=  "$/sample_skydome/textures/moon_glow.dds";

namespace rage
{


static atPtr<grcTexture> LoadTexture( const char* name )
{
	return atCreateResourceAtPtr( grcTextureFactory::GetInstance().Create( name ) );
}

mthRandom	g_LightingRand;


Vector4 ConvertPhaseToTextureCoords( int phaseNumber )
{
	const int width = 4;
	const int height = 6;

	int xPos = phaseNumber % width;
	int yPos = (int)floor( (float)phaseNumber / (float)width );


	float xScale = 1.0f/(float)width;
	float yScale = 1.0f/(float) height;

	float offsetX = (float)xPos * xScale;
	float offsetY = (float )yPos * yScale;

	return Vector4( xScale, yScale, offsetX, offsetY);
}
float CalculateLighting( float timeOfDay, float pulseFreq, float pulseInten )
{
	// create a pulse train from timeofday
	float pulse = timeOfDay * pulseFreq;
	pulse = Saturate(  cos( pulse ) - 0.9f ) * 1.0f/0.9f;	// get the top edge of the pulse

	float briteness = ( pulse + g_LightingRand.GetRanged( -0.1f, 0.1f )  - 0.1f);
	return Saturate( briteness ) * pulseInten;
}
void SkyDomeProceduralControl::CalculateSkySettings( float timeOfDay, SkyHatPerFrameSettings& settings )
{
	Vector3 sunDir = ConvertTimeOfDayToVector3( timeOfDay);
	sunDir = -sunDir;
	float nFade = GetNightFadeOut( timeOfDay);

	settings.m_SunAxias = Vector2( 1.0f, 0.0f);

	//sunDir.y = -sunDir.y;
	float sundiry = sunDir.y;
	sunDir.y *= 2.0f;

	float sunSetZenith = fabs( sunDir.y );
//	float zenith = Saturate(sunDir.y);

	// adickinson: clamp and offset to reduce the "bump" we see at sunrise/sunset
	// float4 cloudsunset = normalize(tex1D(AttenuationSampler,sunSetZenith));// + SunsetColor*2);
	Vector3 sunColor = VLerp(Vector3(.39f,.21f,.01f),Vector3(.92f,.87f,.74f), Saturate(sunSetZenith+0.6f) /*Saturate(zenith+0.1f) */);	//FIXTHIS: approximation
	Vector3 cloudsunset = sunColor;
	settings.m_SunColor = sunColor;

	float sunsetinfluence = fabs( Min( Saturate(sunSetZenith),0.2f) - 0.1f) * 10.0f;	

	// adickinson: keep the cloudcol closer to CloudColor
	// float4 cloudcol = lerp( CloudColor, sunCloudCol, 1 - sunSetZenith );//; //lerp(pow(cloudsunset*2,2),pow(CloudColor,1.2),sunsetinfluence) * saturate(zenith*2+0.2);
	Vector3 cloudcol = VLerp( cloud.m_gCloudColor, sunColor, Saturate( Min(0.7f,1.0f - sunSetZenith)  ));

	// adickinson:  better transition between night and day sky color	
	float dayNightFactor = Saturate(( sundiry + 1.0f)*0.7f);  // + 2 ) * 0.45
	Vector3 skycol = VLerp( sky.m_gNightSkyColor,sky.m_gDaySkyColor, dayNightFactor );
	//Vector3 skycol = VLerp( m_gNightSkyColor, m_gDaySkyColor, Saturate( zenith+0.3f*zenith) );
	settings.m_skyColor = skycol;

	static Vector3 nightAzimuth = Vector3( 0.3968f, 0.34086f, 0.27082f ); //Vector3( 255.0f, 219.0f, 274.0f) * 1.0f/255.0f;
	//nightAzimuth *= 1.0f/255.0f;
	//nightAzimuth *= AzimuthInten;

	Vector3 azimuthCol = VLerp( sky.m_gSunsetColor, sky.m_gDaySkyColor, sunsetinfluence);
	static float nightFactor = 0.5f;
	azimuthCol *=((1.0f - nightFactor) + nFade * nightFactor);

	azimuthCol =  VLerp( azimuthCol, nightAzimuth, Saturate( 1.0f - nFade ) );

	// set the color on the side of that the sun is on
	if ( sunDir.x > 0.0f )
	{
		settings.m_AzimuthColor  = azimuthCol;
		settings.m_AzimuthColorEast  = azimuthCol; //sky.m_gDaySkyColor * ((1.0f - nightFactor) + nFade * nightFactor);
	}
	else
	{
		settings.m_AzimuthColorEast  = azimuthCol;
		settings.m_AzimuthColor  = azimuthCol; ;// sky.m_gDaySkyColor * ((1.0f - nightFactor) + nFade * nightFactor);
	}
	

	float underLight = Saturate( Min(0.7f,1.0f-sunSetZenith ));
	settings.m_UnderLightStrength = underLight;

	float cityLightFactor = 1.0f - Saturate( 1.0f + sundiry );
	if ( cityLightFactor < 0.0f )
	{
		settings.m_UnderLightStrength = Lerp( cityLightFactor, settings.m_UnderLightStrength, 1.0f );
	}
	
	// add nighttime darkening
	cloudcol = VLerp( cloudcol *0.1f, cloudcol, nFade); 
	cloudcol *= ( 1.0f - underLight );
	settings.m_CloudColor = cloudcol;

	// night

	settings.m_SunCentreStart   = sky.m_SunCentreStart;
	settings.m_SunCentreEnd   = sky.m_SunCentreEnd;
	settings.m_SunCentreIntensity   = sky.m_SunCentreIntensity;
	settings.m_SunOuterSize = sky.m_SunOuterSize;
	
	
	//float StarBlink = ( cos( timeOfDay * night.m_StarBlinkFrequency) + 1.0f ) * 0.5f * night.m_StarBlinkIntensity;

	settings.m_StarFieldBrightness = Saturate( 1.0f - dayNightFactor* 2.0f ) * night.m_StarIntensity;//(1.0f - nFade ) * (1.0f - nFade );
	settings.m_StarFieldBrightness *= 1.0f;//StarBlink * 0.1f + 0.9f;
	settings.m_StarFieldThreshold = night.m_StarFieldThreshold;// + StarBlink;
	// copy all static ones
	settings.m_CloudShadowStrength = cloud.m_gCloudShadowStrength *  nFade;	
	
	settings.m_SunsetColor.SetVector3( VLerp( sky.m_gSunsetColor, cloud.m_NightBounceColor, cityLightFactor ) );
	settings.m_AzimuthHeight = sky.m_AzimuthHeight;

	//settings.m_SunsetColor = 
	settings.m_CloudFadeOut = cloud.m_CloudFadeOut;

	settings.m_CloudThreshold  = cloud.m_CloudThreshold;
	settings.m_CloudBias = cloud.m_CloudBias;
	settings.m_CloudShadowOffset = cloud.m_CloudShadowOffset;
	settings.m_CloudInscatteringRange = cloud.m_CloudInscatteringRange;
	settings.m_CloudEdgeSmooth = cloud.m_CloudEdgeSmooth;
	settings.m_CloudThickness = cloud.m_CloudThickness;
	settings.m_DetailScale =  cloud.m_DetailScale;
	settings.m_DetailStrength = cloud.m_DetailStrength;
	settings.m_TopCloudBias = cloud.top.m_TopCloudBias;
	settings.m_TopCloudDetail = cloud.top.m_TopCloudDetail;
	settings.m_TopCloudThreshold =  cloud.top.m_TopCloudThreshold;
	settings.m_TopCloudHeight = cloud.top.m_TopCloudHeight;
	settings.m_AzimuthStrength = 0.5f;
	settings.m_CloudSpeed = cloud.m_CloudSpeed;
	settings.m_CloudWarp = cloud.m_CloudWarp;

	settings.m_TopCloudColor = settings.m_CloudColor;
	settings.m_TopCloudLight = 0.0f;

	if ( lightning.m_LightningPulseFrequency > 0.0f ) // lighting effect by brighting up the lower level
	{
		settings.m_TopCloudLight = CalculateLighting( timeOfDay, lightning.m_LightningPulseFrequency, lightning.m_LightningPulseIntensity );
	}
	// moon setup
	settings.m_MoonBrightness = night.m_MoonBrightness;
	settings.m_MoonColor =  night.m_MoonColor;
	settings.m_MoonGlow = night.m_MoonGlow;
	settings.m_MoonVisiblity = night.m_MoonVisiblity;
	
}

bool SkyDomeProceduralControl::Load( const char* Filename)
{
/*	parRTStructure m_parRTS;
	m_parRTS.SetName("rage__SkyDomeProceduralControl");
	m_parRTS.AddStructureInstance("sky", sky);
	m_parRTS.AddStructureInstance("cloud", cloud);
	m_parRTS.AddStructureInstance("night", night);
	m_parRTS.AddStructureInstance("light", lightning);
	PARSER.LoadFromStructure(Filename, "tuning", m_parRTS); */

	PARSER.LoadObject( Filename, "xml", *this );
	return true;
}

bool SkyDomeProceduralControl::Save(const char* Filename)
{
	/*parRTStructure m_parRTS;
	m_parRTS.SetName("rage__SkyDomeProceduralControl");
	m_parRTS.AddStructureInstance("sky", sky);
	m_parRTS.AddStructureInstance("cloud", cloud);
	m_parRTS.AddStructureInstance("night", night);
	m_parRTS.AddStructureInstance("light", lightning);
	PARSER.SaveFromStructure(Filename, "tuning", m_parRTS); */

	PARSER.SaveObject( Filename, "xml", this, parManager::XML );

	
	return true;
}


char* SkyDome::m_ShaderName = "rage_atmoscatt_clouds";

//char* SkyDome::m_ShaderName = "embedded:/rage_atmoscatt_clouds";

SkyDome::~SkyDome()
{
	delete m_model;
}


//////////////////////////////////////////////////////////////////////////
//
// PURPOSE
//		Allows games to set their own shaders for the skydome.  For example,
// RDR2 doesn't use embedded shaders, so we set "rage_atmoscatt_clouds".
//
void SkyDome::SetShaderName(char* shaderName)
{
	m_ShaderName = shaderName;
}

void SkyDome::SetupTechniques()
{
	char draw[256];
	safecpy(draw, "draw");
	if( m_drawStarfield )
	{
		safecat(draw, "_withstarfield");
	}

	//m_DualParaboloidDrawTechniqueIdx = shader->LookupTechnique("dualparaboloidskyhat_draw", true);


	m_DpFrontDrawTechIdx = m_model->GetShader()->LookupTechnique("dpskyfront_draw", true);
	m_DpBackDrawTechIdx = m_model->GetShader()->LookupTechnique("dpskyback_draw", true);
	m_DrawTechniqueIdx = m_model->GetShader()->LookupTechnique(draw, true);
#if HACK_GTA4
	m_DpDrawTechIdx = m_model->GetShader()->LookupTechnique("paraboloid_draw",true);
#if __PS3
	m_DrawCompressedTechIdx = m_model->GetShader()->LookupTechnique("compressed_draw",true);
#endif // __PS3
#endif // HACK_GTA4

	safecat(draw, "_withmoon");
	m_DrawMoonTechniqueIdx = m_model->GetShader()->LookupTechnique( draw , true);
	m_DrawStencilTechniqueIdx = m_model->GetShader()->LookupTechnique("draw_stencil", true);

#if HACK_GTA4
	m_gtaSkyDomeFadeID		= m_model->GetShader()->LookupVar("gtaSkyDomeFade", true);
	m_gtaSunHDRExposureID	= m_model->GetShader()->LookupVar("HDRSunExposure", true);
	m_gtaWaterColorID	=  m_model->GetShader()->LookupVar("gtaWaterColor", true);
#endif //HACK_GTA4...

}
void SkyDome::Init( SkyhatMiniNoise::SettingsCallback  setCallback, const ProceduralTextureSkyhat::Params&  params)
{
	m_shaderController.SetSettingsCallback(setCallback );
	m_perlinNoiseTextureGen.Load( params, m_ShaderName );

	// create shader for model

	// generate model 
	grmShader* shader = grmShaderFactory::GetInstance().Create();
	shader->Load( m_ShaderName );

	m_model = rage::CreateSkyDomeModel();
	m_model->SetShader( shader );
	LinkToModelShader( *shader );
	LinkToModelShader( m_perlinNoiseTextureGen.GetMiniSkyHatShader() );
	m_shaderController.SetPerlinNoiseRT(  m_perlinNoiseTextureGen.GetRenderTarget(0) );
	ReloadTextures();

	SetupTechniques();

#if SKYHAT_USE_OFFSET
	LoadSkyhatOffset();
#endif

}


void SkyDome::LinkToModelShader( grmShader& shader )
{
	m_shaderController.AddShader( &shader );
}

void SkyDome::DrawProceduralTextures()
{
	m_perlinNoiseTextureGen.Draw();
}
void SkyDome::Show()
{
	m_perlinNoiseTextureGen.Show( 256, 96, 128);
}

enum vals
{
	D3DRS_HISTENCILENABLE            = 360,  /* TRUE to enable early culling based on hi-stencil bit */
	D3DRS_HISTENCILWRITEENABLE       = 364,  /* TRUE to enable update of hi-stencil bit based on hi-stencil test */
	D3DRS_HISTENCILFUNC              = 368,  /* D3DHISTENCILCMPFUNC - bit is set to cull if (ref histencilfn stencil) is true */
	D3DRS_HISTENCILREF               = 372,  /* BYTE reference value used in hi-stencil test */
#if HACK_GTA4
	D3DHSCMP_EQUAL              = 0,
	D3DHSCMP_NOTEQUAL           = 1,
#endif // HACK_GTA4
};



void SkyDome::DrawMoonQuad()
{
	
	Vector3 position = m_shaderController.GetMoonPosition();
	Vector3 a;
	Vector3 u;

	m_shaderController.GetMoonPositionAndTangents( position, a, u);
	
	position *= 500.0f;

	if ( m_model->GetShader()->BeginDraw(grmShader::RMC_DRAW, true, m_DrawStencilTechniqueIdx ) ) //m_DrawMoonTechniqueIdx ) )
	{
		m_model->GetShader()->Bind(0);
		int grcVerts = 8;
		int steps = 8;
		float r=  0.5f * m_shaderController.GetMoonSize() *  m_shaderController.GetMoonSize();
#if HACK_GTA4
		const float radiusScale = 0.7f;//2.4f;
		r *= radiusScale;
#endif // HACK_GTA4
		Vector3 axisX = u * r;
		Vector3 axisY = a * r;

#if HACK_GTA4
		const float positionOffset = 0.7f;
		position += ( axisX  + axisY  ) *positionOffset;
#else // HACK_GTA4
		position += ( axisX  + axisY  ) * 0.25f;
#endif // HACK_GTA4
		SkyVtx* m_vtxptr = m_vertRenderer.Begin( drawTriFan, steps + 2 );
		if ( m_vtxptr )
		{
			Vector2 half( 0.5f,0.5f);
			grcVerts = steps+1;
			m_vtxptr->Write( position, half);
			m_vtxptr++;

			for( int j=0;j<grcVerts;j++)
			{
				float  x=cosf(2.0f*PI*float(j)/float(steps));
				float  y=sinf(2.0f*PI*float(j)/float(steps));
				Vector2 xy( x, y );
				Vector3 pos;
				pos.SetScaled(axisX, x);
				pos.AddScaled(axisY, y);
				pos.Add(position);
		
				m_vtxptr->Write( pos, half + xy * half );

				m_vtxptr++;
			}
			m_vertRenderer.End( m_vtxptr );
		}

		m_model->GetShader()->UnBind();
		m_model->GetShader()->EndDraw();
	}

}

#if HACK_GTA4 && __PS3
#define DEFAULT_STENCIL_VALUE 7
#endif // HACK_GTA4 && __PS3

void stateMarkOutWithStencil( grcLargeStateStack& states, int ref )
{
	states.Set<grcsColorWrite>( grccwNone);

	// write 1 to front-faces
	states.Set<grcsStencilEnable>( true)
		.Set<grcsStencilWriteMask>( 0xFF)
		.Set<grcsStencilMask>( 0xFF)
		.Set<grcsStencilRef>( ref)
		.Set<grcsStencilFunc>( grccfAlways)
		.Set<grcsStencilPass>( grcsoReplace)
		.Set<grcsStencilFail>( grcsoKeep)
		.Set<grcsStencilZFail>( grcsoKeep);

	// write zero to back-faces
	states.Set<grcsTwoSidedStencil>(true)
		.Set<grcsBackStencilWriteMask>(0xFF)
		.Set<grcsBackStencilMask>(0xFF)
		.Set<grcsBackStencilRef>(ref)
		.Set<grcsBackStencilFunc>(grccfAlways)
		.Set<grcsBackStencilPass>(grcsoReplace)
		.Set<grcsBackStencilFail>(grcsoKeep)
		.Set<grcsBackStencilZFail>(grcsoKeep);

#if __XENON
	GRCDEVICE.SetRenderState( D3DRS_HISTENCILENABLE, 0 );
	GRCDEVICE.SetRenderState( D3DRS_HISTENCILWRITEENABLE, 1);
#if HACK_GTA4
	GRCDEVICE.SetRenderState( D3DRS_HISTENCILFUNC, D3DHSCMP_NOTEQUAL);
#else
	GRCDEVICE.SetRenderState( D3DRS_HISTENCILFUNC,  1 /*D3DHSCMP_EQUAL*/);
#endif // HACK_GTA4
	GRCDEVICE.SetRenderState( D3DRS_HISTENCILREF , ref );
#if HACK_GTA4
	// Flush the hi-Z and hi-stencil data - needs to be done even before rendering to hi stencil
	GRCDEVICE.GetCurrent()->FlushHiZStencil(D3DFHZS_ASYNCHRONOUS);
#endif // HACK_GTA4
#endif

}
#if HACK_GTA4
void stateRenderAtStencil( grcLargeStateStack& states, int WIN32_ONLY(ref))
#else // HACK_GTA4
void stateRenderAtStencil( grcLargeStateStack& states)
#endif // HACK_GTA4
{
	states.Set<grcsColorWrite>( grccwRGBA);

	// draw where stencil is not 0 
	states.Set<grcsStencilEnable>(true)
		  .Set<grcsStencilWriteMask>(0)
		  .Set<grcsStencilMask>(0xFF)
#if HACK_GTA4
#if __PS3
		  .Set<grcsStencilRef>( DEFAULT_STENCIL_VALUE)
		  .Set<grcsStencilFunc>( grccfNotEqual)
#else // __PS3
		  .Set<grcsStencilRef>(ref)
		  .Set<grcsStencilFunc>(grccfEqual)
#endif // __PS3
#else // HACK_GTA4
		  .Set<grcsStencilRef>(0)
		  .Set<grcsStencilFunc>(grccfNotEqual)
#endif // HACK_GTA4
		  .Set<grcsStencilPass>(grcsoKeep)
		  .Set<grcsStencilFail>(grcsoKeep)
		  .Set<grcsStencilZFail>(grcsoKeep);

	// draw where stencil is not 0 
	states.Set<grcsTwoSidedStencil>(true)
		  .Set<grcsBackStencilWriteMask>(0)
		  .Set<grcsBackStencilMask>(0xFF)
#if HACK_GTA4
#if __PS3
		  .Set<grcsBackStencilRef>(DEFAULT_STENCIL_VALUE)
		  .Set<grcsBackStencilFunc>( grccfNotEqual)
#else // __PS3
		  .Set<grcsBackStencilRef>(ref)
		  .Set<grcsBackStencilFunc>(grccfEqual)
#endif // __PS3
#else // HACK_GTA4
		  .Set<grcsBackStencilRef>(0)
		  .Set<grcsBackStencilFunc>(grccfNotEqual)
#endif // HACK_GTA4
		  .Set<grcsBackStencilPass>(grcsoKeep)
		  .Set<grcsBackStencilFail>(grcsoKeep)
		  .Set<grcsBackStencilZFail>(grcsoKeep);

#if __XENON
	GRCDEVICE.SetRenderState( D3DRS_HISTENCILENABLE, 1 );
	GRCDEVICE.SetRenderState( D3DRS_HISTENCILWRITEENABLE, 0);
#if HACK_GTA4
	GRCDEVICE.SetRenderState( D3DRS_HISTENCILFUNC, D3DHSCMP_EQUAL);
#else // HACK_GTA4
	GRCDEVICE.SetRenderState( D3DRS_HISTENCILFUNC,  0 /*D3DHSCMP_EQUAL*/);
#endif // HACK_GTA4
	// Flush the hi-Z and hi-stencil data.
	GRCDEVICE.GetCurrent()->FlushHiZStencil(D3DFHZS_ASYNCHRONOUS);
#endif
}

void SkyDome::DrawMT()
{
	GRCDEVICE.BeginDXView();
	GRPOSTFX->BindPCRenderTarget(false);
	Draw(eDrawNormal,false,false);
	GRCDEVICE.EndDXView();
	GRPOSTFX->ReleasePCRenderTarget();
}

#if HACK_GTA4
void SkyDome::Draw(eDrawTypes drawType, bool inverted, bool cheapVersion, bool caped, Color32 capColor )
#else // HACK_GTA4
void SkyDome::Draw(eDrawTypes drawType, bool inverted, bool cheapVersion )
#endif // HACK_GTA4
{
	grcViewport* view = grcViewport::GetCurrent();
	grcLargeStateStack states;

	states.Set<grcsCullMode>(grccmNone )
		.Set<grcsDepthTest>( true )
		.Set<grcsDepthWrite>(false )
		.Set<grcsAlphaBlend>( false )
		.Set<grcsAlphaTest>(false);


	float farClip = 1.0f;
#if HACK_GTA4
	const float VeryFar = 25000.0f;
#else // HACK_GTA4
	const float VeryFar = 20000.0f;
#endif // HACK_GTA4
	float useYOffset = m_yOffset;

#if HACK_GTA4
#if __PS3
	if (drawType == eDrawDp || drawType == eDrawNormal || drawType == eDrawNormalCompressed )
#else // __PS3	
	if(drawType == eDrawDp || drawType == eDrawNormal)
#endif // __PS3
#else // HACK_GTA4
	if (drawType == eDrawNormal)
#endif // HACK_GTA4
	{
		farClip = view->GetFarClip();
		view->SetFarClip( VeryFar );
	}
	else
	{
#if !HACK_GTA4
		useYOffset = m_dpYOffset;
#endif
	}

	bool drawMoon = drawType == eDrawNormal && m_drawMoon && !cheapVersion && m_DrawMoonTechniqueIdx;
#if HACK_GTA4 && __PS3
	drawMoon |= (drawType == eDrawNormalCompressed);
#endif // HACK_GTA4 && __PS3
	WIN32PC_ONLY( drawMoon = false );
	
	Matrix34	worldMtx;

	worldMtx.Identity();

	worldMtx.Translate( view->GetCameraPosition() );
	view->SetWorldMtx( worldMtx );
	

	Matrix34 cameraMtx = view->GetCameraMtx();
	Matrix34 camOrig = cameraMtx;

#if HACK_GTA4 //forces sky to always write on the far clip plane
	const int viewX = view->GetUnclippedWindow().GetX();
	const int viewY = view->GetUnclippedWindow().GetY();
	const int viewW = view->GetUnclippedWindow().GetWidth();
	const int viewH = view->GetUnclippedWindow().GetHeight();
	view->SetWindow(viewX, viewY, viewW, viewH, 1.0f, 1.0f);
#endif // HACK_GTA4

	if ( m_ZUp )
	{
		Matrix34 camRot;
		camRot.Identity();
		camRot.RotateX(-PI/2.0f);
		cameraMtx.Dot(camRot);
		view->SetCameraMtx(cameraMtx);
	}

	if ( drawMoon )
	{
		stateMarkOutWithStencil( states, m_moonRef );
		DrawMoonQuad();
	}

	worldMtx.Identity();
	worldMtx.b.y = inverted ? -1.0f : 1.0f;
	worldMtx.Scale( 50.0f * m_scale,m_yScaler * m_scale,50.0f * m_scale);
	worldMtx.Translate( view->GetCameraPosition() );
	worldMtx.Translate(0.0f, useYOffset, 0.0f);
	view->SetWorldMtx( worldMtx );

#if HACK_GTA4
	if( caped )
	{
		states.Set<grcsColorWrite>( grccwRGBA);
		grcBindTexture(NULL);
		grcLightState::SetEnabled(false);

		Vector3 pMax(2.5f,1.0f,2.5f);
		Vector3 pMin(-2.5f,-1.0f,-2.5f);

		Vector3 p1,p2,p3;
		grcBegin(drawTris, 6);

		p1 = Vector3(pMax.x,pMin.y,pMax.z);
		p2 = Vector3(pMax.x,pMin.y,pMin.z);
		p3 = Vector3(pMin.x,pMin.y,pMax.z);

		grcColor(capColor);
		grcVertex3f(p1);
		grcVertex3f(p2);
		grcVertex3f(p3);

		p1 = Vector3(pMin.x,pMin.y,pMin.z);
		p2 = Vector3(pMax.x,pMin.y,pMin.z);
		p3 = Vector3(pMin.x,pMin.y,pMax.z);

		grcVertex3f(p1);
		grcVertex3f(p2);
		grcVertex3f(p3);

		grcEnd();
	}
#endif // HACK_GTA4

	grcEffectTechnique techIdx = m_DrawTechniqueIdx;
	if(drawType == eDrawDpFront)
		techIdx = m_DpFrontDrawTechIdx;
	else if(drawType == eDrawDpBack)
		techIdx = m_DpBackDrawTechIdx;
#if HACK_GTA4
	else if(drawType == eDrawDp)
		techIdx = m_DpDrawTechIdx;
#if __PS3
	else if(drawType == eDrawNormalCompressed)
		techIdx = m_DrawCompressedTechIdx;
#endif // __PS3
#endif // HACK_GTA4

	if ( drawMoon )
	{
		
		Matrix44 mat = view->GetCameraMtx44();
		mat.Transpose();

#if HACK_GTA4
		stateRenderAtStencil( states,  m_moonRef);
#else // HACK_GTA4
		stateRenderAtStencil( states );
#endif // HACK_GTA4

		m_model->Draw( *view, 0, m_DrawMoonTechniqueIdx);

#if HACK_GTA4
#if __XENON
		GRCDEVICE.SetRenderState( D3DRS_HISTENCILENABLE , 0 );
		states.Set<grcsStencilFunc>(grccfNotEqual );
		states.Set<grcsStencilRef>(m_moonRef);
#else
		states.Set<grcsStencilFunc>(grccfNotEqual );
		states.Set<grcsStencilRef>(m_moonRef);

		states.Set<grcsBackStencilFunc>(grccfNotEqual);
		states.Set<grcsBackStencilRef>(m_moonRef);
#endif
		states.Set<grcsStencilPass>( grcsoKeep);
#else // HACK_GTA4
#if __XENON
		GRCDEVICE.SetRenderState( D3DRS_HISTENCILENABLE , 0 );
#endif

		states.Set<grcsStencilFunc>(grccfEqual );
		states.Set<grcsStencilPass>( grcsoKeep);
#endif // HACK_GTA4
		
		m_model->Draw(*view, 0, m_DrawTechniqueIdx);
	}
	else
	{
		m_model->Draw( *view, 0, techIdx);
	}


	worldMtx.Identity();
#if HACK_GTA4
#if __PS3
	if(drawType == eDrawDp || drawType == eDrawNormal || drawType == eDrawNormalCompressed )
#else // __PS3	
	if(drawType == eDrawDp || drawType == eDrawNormal)
#endif // __PS3
#else // HACK_GTA4
	if (drawType == eDrawNormal)
#endif // HACK_GTA4
	{
		view->SetWorldMtx( worldMtx );
		view->SetFarClip( farClip );
	}

	if ( m_ZUp )
	{
		view->SetCameraMtx( camOrig );
	}

#if HACK_GTA4
	view->SetWindow(viewX,viewY, viewW, viewH);
#endif // HACK_GTA4
}

void SkyDome::Update( float time , int day )
{
	UpdateCloudGeneration( time, day );

	UpdateSky( time );
}

void SkyDome::UpdateCloudGeneration( float time , int day )
{
	m_perlinNoiseTextureGen.SetTimeOfDay( time );
	m_perlinNoiseTextureGen.SetDay( (float)day );
	m_perlinNoiseTextureGen.Update();
}

void SkyDome::UpdateSky( float time  )
{
	m_shaderController.SetTimeOfDay( time );
	m_shaderController.UpdateShaders();
}

void SkyDome::ReloadTextures()
{

	// load defaults
	if ( !m_DetailTexture )	{  m_DetailTexture = LoadTexture( s_DefaultDetailTextureFName ); }
	if ( !m_DetailBump )	{ m_DetailBump = LoadTexture( s_DefaultDetailBumpFName ); }
	if ( !m_Stars )			{ m_Stars = LoadTexture( s_DefaultStarsFName ); }
	if ( !m_Galaxy )		{ m_Galaxy = LoadTexture( s_DefaultGalaxyFName ); }
	if ( !m_Moon )			{ m_Moon = LoadTexture( s_DefaultMoonFName ); }
	if ( !m_MoonGlow )		{ m_MoonGlow = LoadTexture( s_DefaultMoonGlowFName ); }


	m_shaderController.SetHighDetailNoiseTex(  m_DetailTexture  );
	m_shaderController.SetHighDetailNoiseBumpTex( m_DetailBump );
	m_shaderController.SetStarFieldTex( m_Stars );
	m_shaderController.SetGalaxyTex( m_Galaxy );
	m_shaderController.SetMoonTex( m_Moon );
	m_shaderController.SetMoonGlowTex( m_MoonGlow );
}
#if  SKYHAT_USE_OFFSET
void SkyDome::LoadSkyhatOffset()
{
	const char* fileName = "SkyhatOffset";
	parRTStructure m_parRTS;

	Displayf("Loading skyhat offset settings from '%s'...", fileName);
	ASSET.PushFolder("$/tune/skyhat");

	m_parRTS.SetName("rage__SkyDome");
	m_parRTS.AddValue("yOffset", &m_yOffset);
	m_parRTS.AddValue("yScale", &m_yScaler);
	PARSER.LoadFromStructure(fileName, "tuning", m_parRTS);

	ASSET.PopFolder();
}
#endif


void SkyDome::SetHighDetailBumpTexture( const char* bumpTexture ) 
{ 
	m_DetailBump = LoadTexture( bumpTexture );
}
void SkyDome::SetStarFieldTexture( const char* starField ) 
{ 
	m_Stars = LoadTexture( starField );
}
void SkyDome::SetGalaxyTexture( const char* galaxy ) 
{ 
	m_Galaxy = LoadTexture( galaxy );
}
void SkyDome::SetMoonTexture( const char* moon )
{ 
	m_Moon = LoadTexture( moon );
}
void SkyDome::SetMoonGlowTexture( const char* moonGlow )	
{ 
	m_MoonGlow = LoadTexture( moonGlow );
}
bool SkyDome::LoadMoonSettings(char* filename)
{
	return (m_shaderController.LoadSettings(filename));
}



#if __BANK
void SkyDome::AddWidgets( bkBank &bank )
{
#if  SKYHAT_USE_OFFSET
	bank.AddButton("Load",datCallback(MFA(SkyDome::LoadSkyhatOffset),this));
	bank.AddButton("Save",datCallback(MFA(SkyDome::SaveSkyhatOffset),this));
	bank.AddTitle("\n");
	bank.AddSlider("Y Offset", &m_yOffset, -20000.0f, 20000.0f, 1.0f);
	bank.AddSlider("Y Scaler", &m_yScaler, 0.1f, 500.0f, 0.1f);
#endif


	bank.AddToggle( "Draw Moon", &m_drawMoon );
	bank.AddToggle( "Draw Starfield", &m_drawStarfield , datCallback( MFA(SkyDome::SetupTechniques ), this) );


	bank.PushGroup( "Time Controlled", false );
	//m_shaderController.AddWidgets( &bank );
	PARSER.AddWidgets( bank, &m_shaderController );
	bank.PopGroup();

	m_perlinNoiseTextureGen.AddWidgets( bank );

}

#if  SKYHAT_USE_OFFSET
void SkyDome::SaveSkyhatOffset()
{
	const char* fileName = "SkyhatOffset";
	parRTStructure m_parRTS;

	Displayf("Saving skyhat offset settings to '%s'...", fileName);
	ASSET.PushFolder("$/tune/skyhat");

	m_parRTS.SetName("rage__SkyDome");
	m_parRTS.AddValue("yOffset", &m_yOffset);
	m_parRTS.AddValue("yScale", &m_yScaler);
	PARSER.SaveFromStructure(fileName, "tuning", m_parRTS);

	ASSET.PopFolder();
}
#endif

void SkyDome::Save( const char* fileName )
{
	atString nameShader = atString(fileName ); 
	nameShader += atString( "_shader" );
	PARSER.SaveObject(nameShader, "xml", &m_shaderController, parManager::XML );
}

#endif
void SkyDome::Load( const char* fileName )
{
	atString nameShader = atString(fileName ); 
	nameShader += atString( "_shader" );

	PARSER.LoadObject(nameShader, "xml", m_shaderController );
}
void SkyDome::SetPerlinNoiseBaseTexture( const char* pNoiseBase )
{
	m_perlinNoiseTextureGen.SetPerlinNoiseBaseTexture(  LoadTexture( pNoiseBase ) );
}
void SkyDome::SetHighDetailTexture( const char* pDetailTex )
{
	m_DetailTexture = LoadTexture( pDetailTex );
	m_perlinNoiseTextureGen.SetHighDetailTexture(  m_DetailTexture );
	m_shaderController.SetHighDetailNoiseTex( m_DetailTexture );
	
}
grcTexture* SkyDome::GetIBLTexture()
{
	return m_perlinNoiseTextureGen.GetRenderTarget(ProceduralTextureSkyhat::E_MINI_SKY_RT);
}

IMPLEMENT_PLACE(SkyDome);

SkyDome::SkyDome(datResource&)
{
}
#if __DECLARESTRUCT
void SkyDome::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(SkyDome);
	STRUCT_FIELD(m_shaderController);
	STRUCT_FIELD(m_scale);
	//STRUCT_FIELD(m_DetailTexture);
	STRUCT_END();
}
#endif



//#############################################################################


IMPLEMENT_PLACE(SkyhatMiniNoise);

SkyhatMiniNoise::SkyhatMiniNoise(datResource&)
{
}
#if __DECLARESTRUCT
void SkyhatMiniNoise::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(SkyhatMiniNoise);
	STRUCT_FIELD(m_HDRExposure);
	STRUCT_END();
}
#endif
//#############################################################################

//
// -- skyhat frame settings maths

void SkyHatPerFrameSettings::Clear()
{
	m_skyColor.Zero();
	m_AzimuthColor.Zero();
	m_SunsetColor.Zero();
	m_AzimuthColorEast.Zero();
	m_SunAxias.Zero();
	m_AzimuthStrength = 0.0f;
	m_AzimuthHeight = 0.0f;

	// clouds
	m_CloudColor.Zero();
	m_CloudShadowStrength  = 0.0f;
	m_CloudThreshold  = 0.0f;
	m_CloudBias  = 0.0f;
	m_CloudShadowOffset  = 0.0f;
	m_CloudInscatteringRange = 0.0f;
	m_CloudEdgeSmooth    = 0.0f;
	m_DetailScale   = 0.0f;
	m_DetailStrength   = 0.0f;
	m_CloudThickness  = 0.0f;
	m_CloudFadeOut = 0.0f;
	m_CloudWarp = 0.0f;

	// top cloud layer
	m_TopCloudBias  = 0.0f;
	m_TopCloudDetail   = 0.0f;
	m_TopCloudThreshold   = 0.0f;
	m_TopCloudHeight  = 0.0f;
	m_TopCloudLight = 0.0f;
	m_TopCloudColor.Zero();

	m_CloudSpeed= 0.0f;
	// Sun
	m_SunColor.Zero();
	m_UnderLightStrength   = 0.0f;
	m_SunCentreStart   = 0.0f;
	m_SunCentreEnd   = 0.0f;
	m_SunCentreIntensity   = 0.0f;
	m_SunOuterSize = 0.0f;

	// night
	m_StarFieldBrightness = 0.0f;
	m_StarFieldThreshold  = 0.0f;
	// moon
	m_MoonBrightness = 0.0f;
	m_MoonColor.Zero();
	m_MoonGlow = 0.0f;
	m_MoonVisiblity = 0.0f;
	
}
SkyHatPerFrameSettings&	SkyHatPerFrameSettings::operator*=( float t  )
{
	*this = *this * t;
	return *this;
}
SkyHatPerFrameSettings&	SkyHatPerFrameSettings::operator+=( const SkyHatPerFrameSettings& other )
{
	*this = *this + other;
	return *this;
}
SkyHatPerFrameSettings operator*(  float t, const SkyHatPerFrameSettings& a )
{
	return a * t;
}
SkyHatPerFrameSettings operator-(  const SkyHatPerFrameSettings& a , const SkyHatPerFrameSettings& b )
{
	return a + ( b * -1.0f );
}
SkyHatPerFrameSettings operator+(  const SkyHatPerFrameSettings& a , const SkyHatPerFrameSettings& b )
{
	SkyHatPerFrameSettings result;
	result.m_skyColor  = a.m_skyColor + b.m_skyColor;
	result.m_AzimuthColor = a.m_AzimuthColor + b.m_AzimuthColor;
	result.m_SunsetColor =  a.m_SunsetColor + b.m_SunsetColor;
	result.m_AzimuthStrength  =  a.m_AzimuthStrength +  b.m_AzimuthStrength;
	result.m_AzimuthHeight = a.m_AzimuthHeight + b.m_AzimuthHeight;
	result.m_AzimuthColorEast =  a.m_AzimuthColorEast + b.m_AzimuthColorEast;
	result.m_SunAxias  =  a.m_SunAxias +  b.m_SunAxias;

	// clouds
	result.m_CloudColor  =   a.m_CloudColor  +  b.m_CloudColor;
	result.m_CloudShadowStrength  =   a.m_CloudShadowStrength  +  b.m_CloudShadowStrength;
	result.m_CloudThreshold   =   a.m_CloudThreshold +  b.m_CloudThreshold;
	result.m_CloudBias  =   a.m_CloudBias +  b.m_CloudBias;
	result.m_CloudShadowOffset  =   a.m_CloudShadowOffset +  b.m_CloudShadowOffset;
	result.m_CloudInscatteringRange =   a.m_CloudInscatteringRange  +  b.m_CloudInscatteringRange;
	result.m_CloudEdgeSmooth   =   a.m_CloudEdgeSmooth +  b.m_CloudEdgeSmooth;
	result.m_DetailScale   =   a.m_DetailScale +  b.m_DetailScale;
	result.m_DetailStrength  =   a.m_DetailStrength +  b.m_DetailStrength;
	result.m_CloudThickness  =   a.m_CloudThickness  +  b.m_CloudThickness;
	result.m_CloudWarp = a.m_CloudWarp + b.m_CloudWarp;
	result.m_CloudFadeOut = a.m_CloudFadeOut + b.m_CloudFadeOut;
	result.m_CloudSpeed  = a.m_CloudSpeed + b.m_CloudSpeed;

	// top cloud layer
	result.m_TopCloudBias  =   a.m_TopCloudBias  +  b.m_TopCloudBias;
	result.m_TopCloudDetail  =   a.m_TopCloudDetail  +  b.m_TopCloudDetail;
	result.m_TopCloudThreshold  =   a.m_TopCloudThreshold +  b.m_TopCloudThreshold;
	result.m_TopCloudHeight  =   a.m_TopCloudHeight  +  b.m_TopCloudHeight;
	result.m_TopCloudColor = a.m_TopCloudColor +  b.m_TopCloudColor;
	result.m_TopCloudLight = a.m_TopCloudLight +  b.m_TopCloudLight;

	// Sun
	result.m_SunColor  =   a.m_SunColor  +  b.m_SunColor;
	result.m_UnderLightStrength  =   a.m_UnderLightStrength +  b.m_UnderLightStrength;

	result.m_SunCentreStart =   a.m_SunCentreStart  +  b.m_SunCentreStart;
	result.m_SunCentreEnd =   a.m_SunCentreEnd  +  b.m_SunCentreEnd;
	result.m_SunCentreIntensity =   a.m_SunCentreIntensity  +  b.m_SunCentreIntensity;
	result.m_SunOuterSize = a.m_SunOuterSize + b.m_SunOuterSize;

	// night 
	result.m_StarFieldBrightness = a.m_StarFieldBrightness + b.m_StarFieldBrightness;
	result.m_StarFieldThreshold = a.m_StarFieldThreshold + b.m_StarFieldThreshold;
	
	// moon
	result.m_MoonBrightness = a.m_MoonBrightness + b.m_MoonBrightness;
	result.m_MoonColor = a.m_MoonColor + b.m_MoonColor;
	result.m_MoonGlow = a.m_MoonGlow + b.m_MoonGlow;
	result.m_MoonVisiblity = a.m_MoonVisiblity + b.m_MoonVisiblity;

	return result;
}


SkyHatPerFrameSettings operator*(  const SkyHatPerFrameSettings& a , const float& t )
{
	SkyHatPerFrameSettings result;
	result.m_skyColor  = a.m_skyColor * t;
	result.m_AzimuthColor = a.m_AzimuthColor * t;
	result.m_SunsetColor =  a.m_SunsetColor * t;
	result.m_AzimuthStrength  =  a.m_AzimuthStrength * t;
	result.m_AzimuthColorEast =  a.m_AzimuthColorEast * t;
	result.m_SunAxias  =  a.m_SunAxias  * t;
	result.m_AzimuthHeight = a.m_AzimuthHeight * t;


	// clouds
	result.m_CloudColor  =   a.m_CloudColor  * t;
	result.m_CloudShadowStrength  =   a.m_CloudShadowStrength  * t;
	result.m_CloudThreshold   =   a.m_CloudThreshold * t;
	result.m_CloudBias  =   a.m_CloudBias  * t;
	result.m_CloudShadowOffset  =   a.m_CloudShadowOffset  * t;
	result.m_CloudInscatteringRange =   a.m_CloudInscatteringRange   * t;
	result.m_CloudEdgeSmooth   =   a.m_CloudEdgeSmooth  * t;
	result.m_DetailScale   =   a.m_DetailScale  * t;
	result.m_DetailStrength  =   a.m_DetailStrength  * t;
	result.m_CloudThickness  =   a.m_CloudThickness  * t;
	result.m_CloudWarp = a.m_CloudWarp  * t;
	result.m_CloudFadeOut = a.m_CloudFadeOut * t;

	// top cloud layer
	result.m_TopCloudBias  =   a.m_TopCloudBias  * t;
	result.m_TopCloudDetail  =   a.m_TopCloudDetail  * t;
	result.m_TopCloudThreshold  =   a.m_TopCloudThreshold  * t;
	result.m_TopCloudHeight  =   a.m_TopCloudHeight  * t;
	result.m_TopCloudColor = a.m_TopCloudColor * t;
	result.m_TopCloudLight = a.m_TopCloudLight* t;

	// Sun
	result.m_SunColor  =   a.m_SunColor  * t;
	result.m_UnderLightStrength  =   a.m_UnderLightStrength  * t;
	result.m_CloudSpeed  = a.m_CloudSpeed * t;

	result.m_SunCentreStart =   a.m_SunCentreStart * t;
	result.m_SunCentreEnd =   a.m_SunCentreEnd  * t;
	result.m_SunCentreIntensity =   a.m_SunCentreIntensity  * t;
	result.m_SunOuterSize = a.m_SunOuterSize* t;

	// night
	result.m_StarFieldBrightness = a.m_StarFieldBrightness * t;
	result.m_StarFieldThreshold = a.m_StarFieldThreshold * t;
	
	// moon
	result.m_MoonBrightness = a.m_MoonBrightness * t;
	result.m_MoonColor = a.m_MoonColor * t;
	result.m_MoonGlow = a.m_MoonGlow * t;
	result.m_MoonVisiblity = a.m_MoonVisiblity * t;


	return result;
}

void SkyHatCreateFillLights( const Vector3& SunDirection, const SkyHatPerFrameSettings& settings, const Vector3& groundColor, float skyIntensity, float bounceIntensity, SkyFillLights& fill )
{
	Vector3 bounceColor = groundColor * settings.m_SunColor;
	Vector3 azimuthColor = settings.m_skyColor + settings.m_AzimuthColor * settings.m_AzimuthStrength * 0.3f;

	Vector3 sunSetColor( settings.m_SunsetColor.x, settings.m_SunsetColor.y, settings.m_SunsetColor.z );

	fill.SkyColor = settings.m_skyColor * skyIntensity;
	fill.MidColor = azimuthColor * skyIntensity  + bounceIntensity * sunSetColor * settings.m_UnderLightStrength ;
	fill.GroundColor = fill.SkyColor * groundColor * skyIntensity;
	fill.BounceColor = bounceColor * Max( SunDirection.y , 0.0f ) * bounceIntensity;
	fill.BounceDirection = -SunDirection;
	
}
#if HACK_GTA4
int SkyHatGetMoonPhase(int year,int month,int day)
{
	Assert( day >=0 && day <= 31 );
	Assert( month >=0 && month <= 12 );

	int d = day,g,e;
	if (month == 2) d += 31;
	else if (month > 2) d += 59+(int)( (float)(month-3)*30.6f+0.5f );
	g = (year-1900)%19;
	e = (11*g + 29) % 30;
	if (e == 25 || e == 24) ++e;
	int phase = ((((e + d)*6+5)%177)/22 );
	return (phase + 12 ) % 24;
}
#endif // HACK_GTA4

}

#endif	// 0
