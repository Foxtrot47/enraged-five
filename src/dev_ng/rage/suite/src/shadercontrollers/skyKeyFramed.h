
#ifndef SKY_DOME_KEY_FRAMED_CONTROL_H
#define SKY_DOME_KEY_FRAMED_CONTROL_H

#include "rmptfx/ptxkeyframe.h"

namespace rage
{
	struct SkyHatPerFrameSettings;
	class parRTStructure;

	//////////////////////////////////////////////////////////////////////
	// PURPOSE
	//	 help function to make adding load / Save buttons easier
	//	Assumes that filename is statically created.
	//
#if __BANK
	template<class T>
	inline void WidgetsAddWithFileIO( bkBank& bank, T& object , const char* fileName )
	{
		bank.AddButton("Load",datCallback(MFA1(T::Load),&object, (void*)fileName ));
		bank.AddButton("Save",datCallback(MFA1(T::Save),&object, (void*)fileName ));
		object.AddWidgets( bank );
	}
#endif

	//////////////////////////////////////////////////////////////////////
	// PURPOSE
	//	Controls the timed elements of the skydome with keyframes
	//	currently this is only the sun direction
	//	but moon position, size and galaxy position could be added
	//	if required.
	//
	class SkyDomeKeyFrameTimeElements : public datBase
	{
	public:
		SkyDomeKeyFrameTimeElements() {}
		~SkyDomeKeyFrameTimeElements();

		void Load( const char* name );
		
#if __BANK 
#if RMPTFX_BANK 
		void AddWidgets( bkBank& bank );
		void Save(const char* fileName );
#else
		void AddWidgets( bkBank& ) {};
		void Save(const char* ) {};
#endif
#endif

		Vector3		GetSunDirection( float timeOfDay );

		PAR_PARSABLE;
	private:
		ptxKeyframe m_SunDirectionKF;	//Directional based size scalars
	};


	//////////////////////////////////////////////////////////////////////
	// PURPOSE
	//	Controls the weather dependant elements of the skydome with keyframes
	//
	class SkyDomeKeyFramedControl : public datBase
	{
	public:
		SkyDomeKeyFramedControl();
		~SkyDomeKeyFramedControl() { CleanupUI(); }

		void Load( const char* Filename);

#if __BANK 
#if RMPTFX_BANK 
		void AddWidgets( bkBank& bank );
		void Save(const char* fileName );
#else
		void AddWidgets( bkBank& ) {};
		void Save(const char* ) {};
#endif
#endif
		void CalculateSkySettings( float timeOfDay, SkyHatPerFrameSettings& settings );

		void SetFromSettings( float time, SkyHatPerFrameSettings& settings );
		
	protected:
		void UpdateUI();
		void Create( parRTStructure& m_parRTS );
	private:

		void CleanupUI();
		// sky
		ptxKeyframe m_SkyColorKF;	//Keyframed Acceleration
		ptxKeyframe m_AzimuthColorKF;		//Keyframed world space "friction"
		ptxKeyframe m_AzimuthColorEastKF;	//Weight of attached matrix
		ptxKeyframe m_SunsetColorKF;	//Weight of attached matrix
		
		Vector2			m_SunAxias;


		// clouds
		ptxKeyframe m_CloudColorKF;	//Directional based size scalars
		ptxKeyframe m_CloudThresholdThruCloudShadowStrengthKF;
		ptxKeyframe m_CloudShadowOffsetThruAzymuthStrengthKF;
		ptxKeyframe m_DetailScaleThruDetailStrengthWithEdgeSmoothThicknessKF;

		// top clouds
		ptxKeyframe m_TopCloudColorKF;	
		ptxKeyframe m_TBiasThruTDetailThruThresholdThruHeightKF;

		// Sun
		ptxKeyframe m_SunColorKF;	
		ptxKeyframe m_SunCentreKF;
		ptxKeyframe m_UnStrengthThruCSpeedThruStarBrightnessKF;

		// moon
		ptxKeyframe m_MoonColorKFAndBrightness;	
		ptxKeyframe m_MoonBrightnessGlowStarBrightnessThresholdKF;
		
		float		m_StarBlinkFrequency;
		float		m_StarBlinkIntensity;

	};


	template<class _T1, class _T2> 
	void ConvertFromOneControllerToAnother(  _T1& from, _T2& to , float numSamples )
	{
		float diff = 24.0f / numSamples;
		for ( float t = 0.0f; t <= 24.0f; t += diff )
		{
			rage::SkyHatPerFrameSettings	settings;
			from.CalculateSkySettings( t, settings );
			to.SetFromSettings( t, settings);
		}
	}

	class SkyKeyFramedFogControl : public datBase
	{
	public:
		SkyKeyFramedFogControl()
		{
			m_FarColorKF.AddEntry(ScalarV(V_ZERO), Vec4V(0.8f, 0.8f, 1.0f, 0.0f));
			m_NearColorKF.AddEntry(ScalarV(V_ZERO), Vec4V(0.7f, 0.7f, 0.5f, 0.0f));
			m_FogDensityFinalFogOpacityColorBlendStartColorBlendEndKF.AddEntry(ScalarV(V_ZERO), Vec4V( 0.1f, 0.9f, 0.4f, 0.6f ));
			m_fogStart = -10.0f;
			m_fogEnd = 1800.0f;
			m_autoFar = 1.0f;
		}

		void Load(const char* fileName );

#if __BANK 
#if RMPTFX_BANK 
		~SkyKeyFramedFogControl();

		void AddWidgets( bkBank& bank );
		void Save(const char* fileName );
#else
		void AddWidgets( bkBank& ) {};
		void Save(const char* ) {};
#endif
#endif
		void SetFromSettings( float t, SkyHatPerFrameSettings& settings , const Vector3& sunDir );
	private:
		void UpdateUI();
		void Create( parRTStructure& m_parRTS );
	
		ptxKeyframe m_FarColorKF;	
		ptxKeyframe m_NearColorKF;	
		ptxKeyframe	m_FogDensityFinalFogOpacityColorBlendStartColorBlendEndKF;
		float		m_fogStart;
		float		m_fogEnd;
		float		m_autoFar;
	};

	struct SkyLightUserInterface : public datBase
	{
		Vector3			m_BounceGroundColor;
		ptxKeyframe  m_Intensity;
	public:
		SkyLightUserInterface() : m_BounceGroundColor( 0.6f, 0.6f, 0.4f )
		{
			m_Intensity.AddEntry(ScalarV(V_ZERO), Vec4V(0.5f, 0.5f, 0.0f, 0.0f));
		}
#if __BANK 
#if RMPTFX_BANK 
		void AddWidgets( bkBank& bank );
		void Save(const char* fileName );
#else
		void AddWidgets( bkBank& ) {};
		void Save(const char* ) {};
#endif
#endif


		void UpdateUI();
		void Create( parRTStructure& m_parRTS );
		void Load(const char* fileName );

		const Vector3& GetGroundColor( float )
		{
			return m_BounceGroundColor;
		}
		float GetSkyIntensity( float t )
		{
			return m_Intensity.Query(ScalarVFromF32(t)).GetXf();
		}
		float GetBounceIntensity( float t )
		{
			return m_Intensity.Query(ScalarVFromF32(t)).GetYf();
		}
	};



};


#endif
