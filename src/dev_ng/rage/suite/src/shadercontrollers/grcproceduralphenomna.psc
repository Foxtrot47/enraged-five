<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ProceduralTextureVerletWater" base="rage::ProceduralTexture" autoregister="true">
    <bool name="m_perturbShowSim" init="false" hideWidgets="true"/>
    <float name="m_perturbQuadScale" init="0.05f"/>
    <float name="m_perturbRotAngle" init="0.0f"/>
    <float name="m_perturbTexScale" init="0.9f"/>
    <Vector2 name="m_perturbQuadOffset" onWidgetChanged="BankPerturbQuadScaleSliderChanged"/>
    <Vector4 name="m_mirrorPlane"/>
    <float name="m_farClip" init="100000.0f"/>
</structdef>

<structdef type="rage::ProceduralReflection" autoregister="true">
</structdef>

<structdef type="rage::ProceduralTextureSkyhat" base="rage::ProceduralTexture" autoregister="true">
</structdef>

</ParserSchema>