// 
// cloth/clothcontrollerinl.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CLOTH_CONTROLLER_INL_H
#define CLOTH_CONTROLLER_INL_H


inline Vec3V_Out clothController::InterpolatePinnedVert (int vertexIndex, Vec3V_In skinnedPosition, Vec3V_In simulatedPosition, Vec3V_In vertexNormal, ScalarV_In pinningRadiusScale)
{
	// Get the simulated position, and the offset from the skinned position to the simulated position.
	Vec3V skinToSim = Subtract(simulatedPosition,skinnedPosition);
	ScalarV currLength2 = MagSquared(skinToSim);

	// Get the allowed distance and its square.
	const clothBridgeSimGfx* bridge = GetBridge();
	ScalarV pinRadius = ScalarVFromF32(bridge->m_PinRadius[vertexIndex]) * pinningRadiusScale;
	ScalarV pinRadius2 = Scale(pinRadius,pinRadius);

	// Get the skinned position offset toward the simulated position by the pin radius.
	Vec3V unitSkinToSim = Scale(skinToSim,InvSqrtSafe(currLength2,ScalarV(V_ZERO)));
	Vec3V offsetSkinnedPosition = AddScaled(skinnedPosition,unitSkinToSim,pinRadius);

	// Get the offset from the offset skinned position to the simulated position.
	skinToSim = Subtract(simulatedPosition,offsetSkinnedPosition);

	// Find the fraction of the distance to move - the soft pin value.
	// This used to do:
	//ScalarV pinScale = Add(ScalarVFromF32(bridge->m_SoftPinValues[vertexIndex]),Scale(MagFast(skinToSim),ScalarVFromF32(bridge->m_PinRamp[vertexIndex])));
	//Vec3V newPosition = SubtractScaled(simulatedPosition,skinToSim,pinScale);

	// Find the new position, the fraction pinScale of the way from the simulated position to the skinned position.
	Vec3V newPosition = Subtract(simulatedPosition,skinToSim);

	// Move the vertex to the new position, if the current position is farther than the pin radius.
	VecBoolV isWithinRadius = IsLessThan(currLength2,pinRadius2);
	Vec3V interpolatedPosition = SelectFT(isWithinRadius,newPosition,simulatedPosition);

	// Find the distance from the skinned position along the vertex normal.
	skinToSim = Subtract(interpolatedPosition,skinnedPosition);
	ScalarV distanceAlongNormal = Dot(skinToSim,vertexNormal);

	// Clamp the distance along the normal.
	float minAlongNormalFromTuning = -10.0f;
	float maxAlongNormalFromTuning = 10.0f;
	// Don't get these yet because they're not initialized until new resources are made (NC 18Nov09).
	//if (bridge->m_MinAlongNormal)
	//{
	//	minAlongNormalFromTuning = bridge->m_MinAlongNormal[vertexIndex];
	//}
	//if (bridge->m_MaxAlongNormal)
	//{
	//	maxAlongNormalFromTuning = bridge->m_MaxAlongNormal[vertexIndex];
	//}

	ScalarV minAlongNormal = ScalarVFromF32(minAlongNormalFromTuning);
	newPosition = AddScaled(simulatedPosition,vertexNormal,Subtract(minAlongNormal,distanceAlongNormal));
	VecBoolV isAboveLowerLimit = IsGreaterThan(distanceAlongNormal,minAlongNormal);
	interpolatedPosition = SelectFT(isAboveLowerLimit,newPosition,interpolatedPosition);
	ScalarV maxAlongNormal = ScalarVFromF32(maxAlongNormalFromTuning);
	maxAlongNormal = SelectFT(IsGreaterThan(maxAlongNormal,minAlongNormal),minAlongNormal,maxAlongNormal);
	newPosition = SubtractScaled(interpolatedPosition,vertexNormal,Subtract(distanceAlongNormal,maxAlongNormal));
	VecBoolV isBelowUpperLimit = IsLessThan(distanceAlongNormal,maxAlongNormal);
	interpolatedPosition = SelectFT(isBelowUpperLimit,newPosition,interpolatedPosition);

	return interpolatedPosition;
}

inline phVerletCloth* clothController::GetCloth(int lodIndex) const
{
	return m_Cloth[lodIndex];
}

inline clothBridgeSimGfx* clothController::GetBridge ()
{
	return (m_BridgeOwned.ptr ? m_BridgeOwned.ptr : m_BridgeReferenced.ptr);
}

inline const clothBridgeSimGfx*	clothController::GetBridge () const
{
	return (m_BridgeOwned.ptr ? m_BridgeOwned.ptr : m_BridgeReferenced.ptr);
}

inline int clothController::GetLOD () const
{
	return GetBridge()->GetLOD();
}

inline int clothController::GetModelIdx () const
{
	return GetBridge()->GetModelIdx();
}

inline int clothController::GetGeometryIdx () const
{
	return GetBridge()->GetGeometryIdx();
}

inline bool clothController::IsUsingSoftPinning ()
{
	return GetBridge()->IsUsingSoftPinning();
}

inline int clothController::GetMeshVertCount (int lodIndex) const
{
	return GetBridge()->GetMeshVertCount(lodIndex);
}

inline int clothController::GetBoundVertCount (int lodIndex) const
{
	return GetBridge()->GetBoundVertCount(lodIndex);
}

inline int clothController::GetNumPinnedVerts (int lodIndex) const
{
	return GetBridge()->GetNumPinnedVerts(lodIndex);
}

inline int *clothController::GetClothDisplayMap (int lodIndex) const
{
	return GetBridge()->GetClothDisplayMap(lodIndex);
}

inline int *clothController::GetClothDisplayReverseMap (int lodIndex) const
{
	return GetBridge()->GetClothDisplayReverseMap (lodIndex);
}

inline void clothController::SetLOD (int p)
{
	GetBridge()->SetLOD(p);
}

inline void clothController::SetModelIdx (int p)
{
	GetBridge()->SetModelIdx(p);
}

inline void clothController::SetGeometryIdx (int p)
{
	GetBridge()->SetGeometryIdx(p);
}

inline void clothController::SetIsUsingSoftPinning (bool p) {
	GetBridge()->SetIsUsingSoftPinning(p);
}

inline void clothController::SetMeshVertCount (int p, int lodIndex)
{
	GetBridge()->SetMeshVertCount(p, lodIndex);
}

inline void clothController::SetBoundVertCount (int p, int lodIndex)
{
	GetBridge()->SetBoundVertCount(p, lodIndex);
}

inline void clothController::SetNumPinnedVerts (int p, int lodIndex)
{
	GetBridge()->SetNumPinnedVerts(p, lodIndex);
}

inline void clothController::SetDrawable (rmcDrawable* drawable)
{
	m_Drawable = drawable;
}

#endif // CLOTH_CONTROLLER_INL_H
