<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::clothBridgeSimGfx">
	<array name="m_PinRadius" type="atRangeArray" size="4">
		<array type="atArray">
			<float/>
		</array>
	</array>
	<array name="m_InflationScale" type="atRangeArray" size="4">
		<array type="atArray">
			<float/>
		</array>
	</array>
	<array name="m_VertexWeight" type="atRangeArray" size="4">
		<array type="atArray">
			<float/>
		</array>
	</array>
	<array name="m_ClothDisplayMap" type="atRangeArray" size="4">
		<array type="atArray">
			<u16/>
		</array>	
	</array>
</structdef>

</ParserSchema>