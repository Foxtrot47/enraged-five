<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::environmentCloth">
	<array name="m_UserData" type="atArray">
		<int/>
	</array>
</structdef>

<structdef type="::rage::clothVertexBlend">
	<array name="m_Vertex0" type="atArray">
		<Vec3V/>
	</array>
	<array name="m_Vertex1" type="atArray">
		<Vec3V/>
	</array>
</structdef>

<enumdef type="::rage::clothInstanceTuning::enCLOTH_TUNE_FLAGS">  
  <enumval name="CLOTH_TUNE_WIND_FEEDBACK"/>
  <enumval name="CLOTH_TUNE_FLIP_INDICES_ORDER"/>
  <enumval name="CLOTH_TUNE_IGNORE_DISTURBANCES"/>
  <enumval name="CLOTH_TUNE_IS_IN_INTERIOR"/>
  <enumval name="CLOTH_TUNE_NO_PED_COLLISION"/>
  <enumval name="CLOTH_TUNE_USE_DISTANCE_THRESHOLD"/>
  <enumval name="CLOTH_TUNE_CLAMP_HORIZONTAL_FORCE"/>  
  <enumval name="CLOTH_TUNE_FLIP_GRAVITY"/>
  <enumval name="CLOTH_TUNE_ACTIVATE_ON_HIT"/>
  <enumval name="CLOTH_TUNE_FORCE_VERTEX_RESISTANCE"/>
  <enumval name="CLOTH_TUNE_UPDATE_IF_VISIBLE"/>
</enumdef>
  
<structdef type="::rage::clothInstanceTuning">
  <Vec3V name="m_ExtraForce"/>
  <float name="m_RotationRate"/>
  <float name="m_AngleThreshold"/>
  <bitset name="m_Flags" type="fixed32" numBits="32" values="::rage::clothInstanceTuning::enCLOTH_TUNE_FLAGS"/>
  <float name="m_Weight"/>
  <float name="m_DistanceThreshold"/>
  <u8 name="m_PinVert"/>
  <u8 name="m_NonPinVert0"/>
  <u8 name="m_NonPinVert1"/>
</structdef>

</ParserSchema>