// 
// cloth/characterclothcontroller.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "charactercloth.h"
#include "pheffects/clothconnectivitydata.h"
#include "pheffects/tune.h"
#include "system/memory.h"
#include "grmodel/geometry.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "fragment/instance.h"
#include "fragment/type.h"
#include "fragment/typegroup.h"
#include "fragment/typechild.h"
#include "fragment/drawable.h"
#include "grcore/matrix43.h"
#include "system/ipc.h"
#include "parser/restparserservices.h"
#include "pheffects/morphgeometry.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundtaperedcapsule.h"
#include "mesh/mesh.h"
#include "profile/profiler.h"
#include "system/task.h"
#include "physics/simulator.h"
#include "bank/bank.h"
#include "system/param.h"
#include "pheffects/cloth_verlet_spu.h"

#include "phbound/boundcapsule.h"
#include "characterclothcontroller_parser.h"


DECLARE_TASK_INTERFACE(UpdateCharacterCloth);

//#pragma warning( disable:4100 )

#include "cloth/characterclothcontrollershared.h"

//#pragma optimize("", off)

#define		DEFAULT_MAPPING_THRESHOLD		0.05f
#define		DEFAULT_ERROR_THRESHOLD			1.04f


namespace rage
{

	EXT_PFD_DECLARE_ITEM( ClothColBounds );
	EXT_PFD_DECLARE_ITEM( ClothNormals );
	EXT_PFD_DECLARE_ITEM( ClothEdges );

	bool characterClothController::sm_EnableSpuUpdate = true;
	const char* characterClothController::sm_CharacterName = NULL;
	const char* characterClothController::sm_MetaDataFileAppendix = "_cccontroller";

	extern bool g_PauseClothSimulation;

using namespace phCharClothStats;


#define		CONST_INT_I0_I7(i)	const int	i0 = i,		\
											i1 = i + 1,	\
											i2 = i + 2,	\
											i3 = i + 3,	\
											i4 = i + 4,	\
											i5 = i + 5,	\
											i6 = i + 6,	\
											i7 = i + 7


IMPLEMENT_PLACE(characterClothController::BindingInfo);

characterClothController::BindingInfo::BindingInfo(class datResource& /*rsc*/)
{
	// do something
}

#if !__SPU
const char* characterClothControllerTuning::sm_MetaDataFileAppendix = "_tuning";
void characterClothControllerTuning::Load(const char* filePath, const char* fullName)
{
	char buff[256];
	formatf( buff, "%s%s%s", filePath, fullName, characterClothControllerTuning::sm_MetaDataFileAppendix );

	parSettings s = parSettings::sm_StrictSettings; 
	s.SetFlag(parSettings::USE_TEMPORARY_HEAP, true); 
	PARSER.LoadObject( buff, "xml", *this, &s);	

	clothDebugf1("Loaded character cloth tuning file: %s", buff);
}
#endif // !__SPU


#if MESH_LIBRARY

void SkinMeshFromMaterial( Matrix43* mtx, const mshMaterial* mtl, Vec3V* poseOut, Vec3V* normalOut )
{
	Assert( poseOut );
	Assert( mtl );

	const int vtxCount = mtl->GetVertexCount();
	for( int i = 0; i < vtxCount; ++i )
	{
		Vector3 v = mtl->GetVertex(i).Pos;
		Vector3 n = mtl->GetVertex(i).Nrm;

		const mshBinding& binding = mtl->GetBinding(i);
		Matrix34 skinMtx;
		if( binding.IsPassThrough == false )
		{
			const int blendIndices[4] = {	binding.Mtx[0],
											binding.Mtx[1],
											binding.Mtx[2], 
											binding.Mtx[3], };

			Vector4 blendWeights(	binding.Wgt[0], 
									binding.Wgt[1], 
									binding.Wgt[2], 
									binding.Wgt[3] );

			int index = blendIndices[0];
			mtx[ index ].ToMatrix34(RC_MAT34V(skinMtx));
			skinMtx.ScaleFull(blendWeights.x);
			index = blendIndices[1];
			Matrix34 temp;
			mtx[ index ].ToMatrix34(RC_MAT34V(temp));
			temp.ScaleFull(blendWeights.y);
			skinMtx.Add(temp);
			index = blendIndices[2];
			mtx[ index ].ToMatrix34(RC_MAT34V(temp));;
			temp.ScaleFull(blendWeights.z);
			skinMtx.Add(temp);
			index = blendIndices[3];
			mtx[ index ].ToMatrix34(RC_MAT34V(temp));
			temp.ScaleFull(blendWeights.w);
			skinMtx.Add(temp);
		}
		else
		{
			mtx[ 0 ].ToMatrix34(RC_MAT34V(skinMtx));
		}

		skinMtx.Transform( v );
		poseOut[i] = VECTOR3_TO_VEC3V( v );

		if( normalOut )
		{
			skinMtx.Transform( n );
			normalOut[i] = VECTOR3_TO_VEC3V( n );
		}
	}
}

#endif // MESH_LIBRARY



characterClothController::characterClothController()
: m_PinningRadiusScale(1.0f)
, m_ControllerType(NULL)
, m_ForcePin(0)
, m_Flags(0)
, m_WindScale(1.0f)
, m_PackageIndex(0)
, m_NewPackageIndex(0)
#if NO_BONE_MATRICES
, m_fPinRadiusSetThreshold( PIN_RADIUSSETS_PACKAGE_THRESHOLD )
#endif
{
}

characterClothController::~characterClothController()
{
}

IMPLEMENT_PLACE(characterClothController);
IMPLEMENT_PLACE(Matrix43);


characterClothController::characterClothController( class datResource& rsc ) 
: clothController(rsc)
, m_BindingInfo( rsc, true )
, m_OriginalPos( rsc, true )
, m_TriIndices( rsc )
#if !NO_BONE_MATRICES
, m_PoseMatrixSet( rsc )
#endif
, m_BoneIDMap( rsc )
, m_BoneIndexMap( rsc )
{
	if(datResource_IsDefragmentation)
	{
		Displayf("Defrag characterClothController %s", GetName());
	}
}

#if !__SPU
void characterClothController::Load(const char* filePath )
{
	char buff[256];
	formatf( buff, "%s%s%s", filePath, GetName(), characterClothController::sm_MetaDataFileAppendix );

	parSettings s = parSettings::sm_StrictSettings; 
	s.SetFlag(parSettings::USE_TEMPORARY_HEAP, true); 
	PARSER.LoadObject( buff, "xml", *this, &s);

	clothDebugf1("Loaded character cloth controller from file: %s", buff);
}
void characterClothController::Save(
#if __FINAL
	const char*
#else
	const char* filePath
#endif
	)
{
#if !__FINAL

	char buff[256];
	formatf( buff, "%s%s%s", filePath, GetName(), characterClothController::sm_MetaDataFileAppendix );
	AssertVerify(PARSER.SaveObject( buff, "xml", m_ControllerType.ptr/*this*/, parManager::XML));

	clothDebugf1("Saved character cloth controller to file: %s", buff);
#endif
}

#if __BANK && !__RESOURCECOMPILER
characterClothController* s_CurrentCClothController = NULL;
bool s_RegisterCClotControllerhRestInterface = false;

void characterClothController::RegisterRestInterface(const char* controllerName)
{
	if( !s_RegisterCClotControllerhRestInterface )
	{		
		s_RegisterCClotControllerhRestInterface = true;
		s_CurrentCClothController = const_cast<characterClothController*>(/*m_ControllerType ? m_ControllerType:*/ this);
		parRestRegisterSingleton("Physics/Cloth/CharClothController", *s_CurrentCClothController, NULL);

		clothDebugf1("Registered Physics/Cloth/CharClothController REST interface for: %s", controllerName);
	}
}

void characterClothController::UnregisterRestInterface(const char* controllerName)
{
	if( s_RegisterCClotControllerhRestInterface )
	{	
		s_RegisterCClotControllerhRestInterface = false;
		s_CurrentCClothController = NULL;
		REST.RemoveAndDeleteService("Physics/Cloth/CharClothController");

		clothDebugf1("Unregistered Physics/Cloth/CharClothController REST interface for: %s", controllerName);
	}
}
#endif // __BANK && !__RESOURCECOMPILER

#endif // !__SPU

void characterClothController::SkinMesh( const crSkeleton* skeleton )
{
	const int lodIndex = GetLOD();
// NOTE: there is no support for cloth lods in character cloth yet
	Assert( lodIndex == 0 );
	phVerletCloth* cloth = GetCloth(lodIndex);
	Assert( cloth );

	const atArray<Vec3V>* originalPos = GetOriginalPos();
	Assert( originalPos );
	Assert( originalPos->GetCount() );

	const atArray<BindingInfo>* bindingInfo = GetBindingInfo();
	Assert( bindingInfo );
	Assert( bindingInfo->GetCount() );

	Vec3V* RESTRICT vPos = cloth->GetClothData().GetVertexPointer();
	Assert( vPos );

	const int numBones = m_BoneIndexMap.GetCount();

#if NO_BONE_MATRICES
	Matrix43* RESTRICT mtx = Alloca(Matrix43, numBones );
#else
	Matrix43* RESTRICT mtx = (Matrix43* RESTRICT)m_PoseMatrixSet.GetElements();
#endif
	Assert(mtx);
	const Mat34V* RESTRICT invJointScaleMtx = skeleton->GetSkeletonData().GetCumulativeInverseJoints();		
	for(int i=0; i < numBones; ++i)
	{
		const int boneIdx = m_BoneIndexMap[i];
		Assert( boneIdx < skeleton->GetSkeletonData().GetNumBones() );
		Mat34V m;
		rage::Transform( m, skeleton->GetObjectMtx( boneIdx ), invJointScaleMtx[ boneIdx ] );
		mtx[i].FromMatrix34( m );
	} 

	const int numVerts = cloth->GetNumVertices();
	SkinMesh( false, mtx, numVerts, (Vector3*)originalPos->GetElements(), bindingInfo->GetElements(), 0, vPos );

	Mat34V skParent = *skeleton->GetParentMtx();
	skParent.SetCol3( Vec3V(V_ZERO) );

	for( int i = 0; i < numVerts; ++i )
	{		
		vPos[i] = Transform( skParent, vPos[i] );
	}
}

#if CLOTH_INSTANCE_FROM_DATA
void characterClothController::InstanceFromData(int vertsCapacity, int edgesCapacity, int boneIDsCapacity)
{
	clothController::InstanceFromData(vertsCapacity, edgesCapacity);

	m_TriIndices.Resize( vertsCapacity * 4 );
	m_OriginalPos.Resize( vertsCapacity );
	m_BindingInfo.Resize( vertsCapacity );
	m_BoneIndexMap.Resize( boneIDsCapacity );
	for (int i=0; i < boneIDsCapacity; ++i)
	{
		m_BoneIndexMap[i] = -1;
	}
}
#endif // CLOTH_INSTANCE_FROM_DATA

void characterClothController::InstanceFromTemplate( const characterClothController* copyme )
{
	Assert( copyme );
	clothController::InstanceFromTemplate( copyme );

#if __DEV
//	copyme->DebugDisplayReferencing();
#endif

	m_ControllerType		= copyme;

#if NO_BONE_MATRICES
	const int matrixCount	= copyme->m_BoneIDMap.GetCount();
#else
	const int matrixCount	= copyme->m_PoseMatrixSet.GetCount();
#endif

	Assert( matrixCount > 0 );
#if !NO_BONE_MATRICES
	m_PoseMatrixSet.Resize(matrixCount);
#endif
//	m_BoneIDMap.Resize(matrixCount);
	m_BoneIndexMap.Resize(matrixCount);
	for (int i=0; i < matrixCount; ++i)
	{
#if !NO_BONE_MATRICES
		m_PoseMatrixSet[i]	= copyme->m_PoseMatrixSet[i];
#endif
//		m_BoneIDMap[i]		= copyme->m_BoneIDMap[i];
		m_BoneIndexMap[i]	= -1;
	}

#if !NO_BONE_MATRICES
// TODO: need 4 bytes to store somewhere pin radius sets threshold
// m_OriginalPos is not used in the instance, but is used in the type
	m_OriginalPos.Resize(1);
	m_OriginalPos[0].SetWf( PIN_RADIUSSETS_PACKAGE_THRESHOLD );
#endif
}


#if __DECLARESTRUCT
void characterClothController::DeclareStruct( datTypeStruct &s )
{
	m_ControllerType = NULL;

	clothController::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE( characterClothController, clothController )
		SSTRUCT_FIELD(characterClothController,m_PinningRadiusScale)
		SSTRUCT_CONTAINED_ARRAY(characterClothController,m_Pad)
		SSTRUCT_FIELD(characterClothController,m_TriIndices)
		SSTRUCT_FIELD(characterClothController,m_OriginalPos)
#if NO_BONE_MATRICES
		SSTRUCT_FIELD(characterClothController,m_fPinRadiusSetThreshold)
		SSTRUCT_CONTAINED_ARRAY(characterClothController,m_Padding)
#else
		SSTRUCT_FIELD(characterClothController,m_PoseMatrixSet)
#endif
		SSTRUCT_FIELD(characterClothController,m_BoneIndexMap)
		SSTRUCT_FIELD(characterClothController,m_BindingInfo)
		SSTRUCT_IGNORE(characterClothController,m_ControllerType)				
		SSTRUCT_FIELD(characterClothController,m_ForcePin)
		SSTRUCT_FIELD(characterClothController,m_Flags)
		SSTRUCT_FIELD(characterClothController,m_NewPackageIndex)
		SSTRUCT_FIELD(characterClothController,m_PackageIndex)
		SSTRUCT_FIELD(characterClothController,m_WindScale)
		SSTRUCT_FIELD(characterClothController,m_BoneIDMap)
	SSTRUCT_END(characterClothController)
}
#endif

void characterClothController::SetPackageIndex(u8 index, u8 transitionFrames)
{
// TODO: no lods yet
	const int lodIndex = 0;

	phVerletCloth* pCloth = GetCloth(lodIndex);
	Assert(pCloth);
	const int numVertices = pCloth->GetNumVertices();
	const clothBridgeSimGfx* pClothBridge = GetBridge();
	Assert( pClothBridge );
	const int vertexBlendsCount = pClothBridge->GetVertexBlendsCount(lodIndex);

	if( ((int)index+1)*numVertices <= vertexBlendsCount )
	{
		if( transitionFrames > 0 )
			m_ForcePin = transitionFrames;
		else
			m_PackageIndex = index;

		m_NewPackageIndex = index;
		
		clothDebugf1("Set package index: %d   Num cloth vertices: %d  Num vertex blends: %d", index, numVertices, vertexBlendsCount );
	}
	else
	{
		clothDebugf1("Trying to set package index: %d  . There is no enough data for such package. Num cloth vertices: %d . Num vertex blends: %d", index, numVertices, vertexBlendsCount );
	}
}

// void characterClothController::InitCollision()
// {
// 	m_Cloth[0]->InitCollisionData();			// compute bound volume
// }


void characterClothController::ResetW()
{
	phVerletCloth* pCloth = GetCloth(0);
	Assert( pCloth );
	Vec3V* clothPos = pCloth->GetClothData().GetVertexPointer();
	Assert( clothPos );
	const int vertsCount = pCloth->GetNumVertices();
//	Assert( vertsCount );
	for( int i = 0; i < vertsCount; ++i )
	{
		clothPos[i].SetW( MAGIC_ZERO );		// since cloth simulation is using compressed deltas, those need to be reset to 0
	}
}


#define		THRESHOLD_FLOAT		10.0f

bool characterClothController::FindCoordinates( float* RESTRICT mappingThreshold, float* RESTRICT errorThreshold, int& pinnedMatch, int numPinned, int vertexIndexHi, Vec3V_In skinnedVtxHi, bool inwardVtx, int triIndicesCount, const u16* RESTRICT triIndices, Vec3V* skinnedVerts, phMorphDataAoS* outData )
{	
	//	Assert( mtlCloth );
	Assert( skinnedVerts );

	ScalarV distThreshold = ScalarVFromF32( 0.000001f );
	for(int i = 0; i < numPinned; ++i )
	{
		Vec3V distV = Subtract( skinnedVerts[i], skinnedVtxHi );
		ScalarV distScalar = Dot( distV, distV );

		if(	IsLessThanAll( distScalar, distThreshold ) != 0 )
		{
			++pinnedMatch;
			return false;						// vtx is same as pinned one
		}
	}

	ScalarV lastError;
	lastError.Set( THRESHOLD_FLOAT );

	const Vec4V _zero(V_ZERO);

	int sCount = 0;
	const int MAX_SELECTION = 32;
	phMorphDataAoS newData[MAX_SELECTION];
	float sError[MAX_SELECTION]; 

	for( int i = 0; i < MAX_SELECTION; ++i)
	{
		sError[i]			= 999.0f;
		newData[i].weights	= _zero;
		newData[i].vtxIndex	= (u16)(-1);
	}

	// TODO: clean this up
	// 	const mshPrimitive& primitive = mtlCloth->Prim[0];
	// 	Assert( primitive.Type == mshTRIANGLES );
	const int triangleCount = triIndicesCount / 3; //  primitive.Idx.GetCount() / 3 ;
	for( int i = 0; i < triangleCount; ++i)
	{
		static int indexStride = 3;
		const int indexOffset = i*indexStride;

		// !!! IMPORTANT: Normal is not necessary but the correct winding order has to be guaranteed by the indices !!!

		// 		int index0 = (int)primitive.Idx[indexOffset];
		// 		int index1 = (int)primitive.Idx[indexOffset + 1];
		// 		int index2 = (int)primitive.Idx[indexOffset + 2];

		int index0 = (int)triIndices[indexOffset];
		int index1 = (int)triIndices[indexOffset+1];
		int index2 = (int)triIndices[indexOffset+2];
		//////////
		if( inwardVtx )
		{
			int tempIndex = index0;
			index0 = index1;
			index1 = tempIndex;
		}
		/////////
		const Vec3V planeVtx0 = skinnedVerts[ index0 ];
		const Vec3V planeVtx1 = skinnedVerts[ index1 ];
		const Vec3V planeVtx2 = skinnedVerts[ index2 ];


		ScalarV d, w0, w1, w2;
		Vec3V n;

		const Vec3V projectedVtx = phMorphController::FindProjectedVtx( skinnedVtxHi, planeVtx0, planeVtx1, planeVtx2, n, d );
		const Vec3V weightedVtx = phMorphController::FindWeightedVtx( projectedVtx, planeVtx0, planeVtx1, planeVtx2, n, d, w0, w1, w2);

		Vec3V errThresholdV = Subtract( projectedVtx, weightedVtx );
		ScalarV err = Dot( errThresholdV, errThresholdV );

		// give some room for verts that fall outside the bound of the higher res mesh ... otherwise those verts will not map to any triangle

		// TODO: do a index0 ..1..2 average of error and mapping thresholds

		// NOTE: protect art from themselfs ... i.e. patch bogus values
		const float errTFLoat = Selectf( errorThreshold[index0] - 0.002f,  errorThreshold[index0], DEFAULT_ERROR_THRESHOLD );
		const float distTFloat = Selectf( mappingThreshold[index0] - 0.002f, mappingThreshold[index0], DEFAULT_MAPPING_THRESHOLD );

		ScalarV errThreshold = ScalarVFromF32( /*errorThreshold[index0]*/ errTFLoat );
		ScalarV distThreshold = ScalarVFromF32( /*mappingThreshold[index0]*/ distTFloat );
		if(		IsLessThanAll( Add(w0, Add(w1,w2) ), errThreshold ) != 0 
			&&	IsLessThanAll( Abs(d), distThreshold ) != 0 
			) 
		{
			Vec4V weights;
			weights.SetX( w0 );
			weights.SetY( w1 );
			weights.SetZ( w2 );

			newData[sCount].vtxIndex = static_cast<u16>(vertexIndexHi);

			newData[sCount].index0	= (u16)index0;
			newData[sCount].index1	= (u16)index1;
			newData[sCount].index2	= (u16)index2;

			newData[sCount].weights = weights;
			newData[sCount].weights.SetW( d );

			sError[sCount] = err.Getf();

			sCount++;
			Assertf( sCount < MAX_SELECTION, "too many matching vertices have been found ?!" );
		}
	}


	if (sCount <= 0)
	{
		//Displayf( "No match in lower lod has been found for vertex: %d ", vertexIndexHi);
		return false;
	}
	else
	{
		bool bubble = false;
		do
		{
			bubble = false;
			for( int i = 0; i < sCount-1; ++i )
			{
				if( sError[i] > sError[i+1] )
				{
					float tempErr = sError[i+1];
					sError[i+1] = sError[i];
					sError[i] = tempErr;

					phMorphDataAoS tempMData = newData[i+1];
					newData[i+1] = newData[i];
					newData[i] = tempMData;

					bubble = true;
				}
			}

		} while ( bubble );
	}

	Assert( outData );
	*outData = newData[0];

	// flip the winding order of the verts if normal is facing the other way so inside polys are properly lit
	// TODO: still work in progress
	// 	if( fabsf(outData->weights.GetWf()) > 0.01f )
	// 	{
	// 		u16 tempIndex = outData->index0;
	// 		outData->index0 = outData->index1;
	// 		outData->index1 = tempIndex;
	// 
	// 		ScalarV tempWeight = outData->weights.GetX();
	// 		outData->weights.SetX( outData->weights.GetY() );
	// 		outData->weights.SetY( tempWeight );
	// 	}

	return true;
}


#if MESH_LIBRARY


void characterClothController::Load( Vec3V** drawableVertsSkinnedOut, bool doTheMorph, phMorphDataAoS* &tempMorphData, int& tempMorphDataCount, const crSkeleton* skeleton, const mshMaterial* mtlBase, mshMaterial* mtlCloth, int /*nModel*/ )
{
	Assert( mtlCloth );

	CreateBridge();

	SetLOD( 0 );

//	SetModelIdx(nModel);
//	SetGeometryIdx( 0 );	

// Note: skin cloth sim mesh

	const int vertsClothCount =  mtlCloth->GetVertexCount();
	Assert( vertsClothCount );

	Vec3V* vertsClothSkinned = Alloca(Vec3V, vertsClothCount);	
	Assert( vertsClothSkinned );

	Assert( skeleton );
	const int numBones = skeleton->GetBoneCount();
	Matrix43* mtxCloth = Alloca(Matrix43, numBones);
	skeleton->Attach( true, mtxCloth );
	SkinMeshFromMaterial( mtxCloth, mtlCloth, vertsClothSkinned, NULL );

	// Generate a bound from the input geometry
	sysMemStartTemp();
	phBoundGeometry* clothBound = GenerateMeshBound( mtlCloth, (Vector3*)vertsClothSkinned );

	phClothConnectivityData *connectivity = rage_new phClothConnectivityData();
	int nEdge = phVerletCloth::CountEdges( clothBound->GetPolygonPointer(), clothBound->GetNumPolygons() );
	connectivity->Init( clothBound->GetNumVertices(), nEdge, clothBound->GetNumPolygons() );
	connectivity->BuildConnectivity( clothBound->GetPolygonPointer(), clothBound->GetNumPolygons() );
	sysMemEndTemp();

	m_Cloth[0] = rage_new phVerletCloth;

// TODO: add extra data
	int extraVerts = 0;

	bool allocateNormals = true;
	m_Cloth[0]->InitWithBound( connectivity->m_EdgeToVertexIndices, *clothBound, allocateNormals, extraVerts);

// Note: store poly indices
	const int numPolygons = clothBound->GetNumPolygons();
	const int triIndicesCount = numPolygons * 3;				// assuming all triangles
	m_TriIndices.Resize( triIndicesCount );
	for(int i = 0; i < numPolygons; ++i )
	{
		const int off = i*3;
		const phPolygon& triangle = clothBound->GetPolygon(i);
		m_TriIndices[ off   ] = (u16)triangle.GetVertexIndex(0);
		m_TriIndices[ off+1 ] = (u16)triangle.GetVertexIndex(1);
		m_TriIndices[ off+2 ] = (u16)triangle.GetVertexIndex(2);
	}

	GenerateMeshBoundMap( 0/*lod*/, (Vector3*)vertsClothSkinned );

	sysMemStartTemp();

	int nChannelElements = vertsClothCount;

	float* m_bendSpringStrengthChannelData = rage_new float[nChannelElements];
	float* m_bendSpringLengthChannelData = rage_new float[nChannelElements];

	if(!ReadVertexBlindData( m_bendSpringLengthChannelData, mtlCloth, mshVtxBlindCLOTHBENDSPRINGLENGTH, 1.0f ))
	{
		delete [] m_bendSpringLengthChannelData;
		m_bendSpringLengthChannelData = NULL;
	}
	if(!ReadVertexBlindData( m_bendSpringStrengthChannelData, mtlCloth, mshVtxBlindCLOTHBENDSPRINGSTRENGTH, 0.7f ))
	{
		delete [] m_bendSpringStrengthChannelData;
		m_bendSpringStrengthChannelData = NULL;
	}

	characterClothControllerTuning cccTuning;			// by default all flags are 0
	cccTuning.m_ExtraPinRadiusChannels = 0;
	const char* pControllerName = GetName();
	if( phVerletCloth::sm_tuneFilePath && pControllerName )
	{
		char buff[256];
		char nameBuff[128];
		formatf( nameBuff, "%s_%s", characterClothController::sm_CharacterName, pControllerName );
		formatf( buff, "%s%s", nameBuff, characterClothControllerTuning::sm_MetaDataFileAppendix );

		ASSET.PushFolder( phVerletCloth::sm_tuneFilePath );
		if( ASSET.Exists(buff, "xml") )
		{
			cccTuning.Load("", nameBuff);
		}
		ASSET.PopFolder();
	}

	sysMemEndTemp();	

	PinClothVerts( mtlCloth, connectivity, cccTuning.m_ExtraPinRadiusChannels );

	phVerletCloth* pCloth = GetCloth(0);
	Assert( pCloth );
	Vector3* clothPos = (Vector3*)pCloth->GetClothData().GetVertexPointer();

	// store the bindings, should be always after the PinClothVerts because verts are shuffled ( swapped )	
	const int vertsCount = pCloth->GetNumVertices();
	m_BindingInfo.Resize( vertsCount );

	const int MAX_NUM_BONES = 256;
	s32 tempBoneIndices[MAX_NUM_BONES];
	memset( tempBoneIndices, 0, sizeof(s32) * MAX_NUM_BONES);

	Vector3* tempPositions = Alloca( Vector3, vertsCount );
	for( int i = 0; i < vertsCount; ++i )
	{
		tempPositions[i] = clothPos[i];

		ScalarV errThreshold = ScalarVFromF32(1000.0f);
		int k = -1;
		for(int j = 0; j < vertsClothCount; ++j )
		{
			Vec3V v = vertsClothSkinned[j];
			Vec3V tempV = Subtract( VECTOR3_TO_VEC3V(tempPositions[i]), v );
			ScalarV errV = Dot( tempV, tempV );
			if( IsLessThanAll( errV, errThreshold ) != 0 )
			{
				errThreshold = errV;
				k = j;
			}
		}

		Assert( k > -1 );
		const mshBinding& binding = mtlCloth->GetBinding(k);
		m_BindingInfo[i].blendIndices[0] = binding.Mtx[0];
		m_BindingInfo[i].blendIndices[1] = binding.Mtx[1];
		m_BindingInfo[i].blendIndices[2] = binding.Mtx[2];
		m_BindingInfo[i].blendIndices[3] = binding.Mtx[3];
		m_BindingInfo[i].weights.Set( binding.Wgt[0], binding.Wgt[1], binding.Wgt[2], binding.Wgt[3] );

		Assert( m_BindingInfo[i].blendIndices[0] < MAX_NUM_BONES );
		Assert( m_BindingInfo[i].blendIndices[1] < MAX_NUM_BONES );
		Assert( m_BindingInfo[i].blendIndices[2] < MAX_NUM_BONES );
		Assert( m_BindingInfo[i].blendIndices[3] < MAX_NUM_BONES );
		tempBoneIndices[ m_BindingInfo[i].blendIndices[0] ]++;
		tempBoneIndices[ m_BindingInfo[i].blendIndices[1] ]++;
		tempBoneIndices[ m_BindingInfo[i].blendIndices[2] ]++;
		tempBoneIndices[ m_BindingInfo[i].blendIndices[3] ]++;
	}

	// count how many unique bones are used
	int uniqueMatricesCount = 0;
	for( int i = 1; i < MAX_NUM_BONES; ++i )
	{
		uniqueMatricesCount += (int)( tempBoneIndices[i] > 0);
	}
	Assert( uniqueMatricesCount );
	m_BoneIndexMap.Resize( uniqueMatricesCount );
	for( int i = 0; i < uniqueMatricesCount; ++i )
		m_BoneIndexMap[i] = -1;

	uniqueMatricesCount = 0;
	// set unique matrix indices
	for( int i = 0; i < vertsCount; ++i )
	{
		const int weightsCount = 4;
		for( int k = 0; k < weightsCount; ++k )
		{
			bool foundIndex = false;
			int blendMatrixIndex = m_BindingInfo[i].blendIndices[k];
			for( int j = 0; j < uniqueMatricesCount; ++j )
			{
				if( blendMatrixIndex == m_BoneIndexMap[j] )
				{
					foundIndex = true;
					break;
				}
			}
// NOTE: don't use root bone (index 0)
			if( !foundIndex && blendMatrixIndex )
			{
				m_BoneIndexMap[ uniqueMatricesCount++ ] = blendMatrixIndex;
			}
		}
	}

	// set bind pose matrices
	Assert( m_BoneIndexMap.GetCount() == uniqueMatricesCount );	

#if !NO_BONE_MATRICES
	m_PoseMatrixSet.Resize( uniqueMatricesCount );
	for( int i = 0; i < uniqueMatricesCount; ++i )
	{
		m_PoseMatrixSet[i] = mtxCloth[ m_BoneIndexMap[i] ];
	}
#endif

	// remap bones in the matrix set map
	for( int i = 0; i < vertsCount; ++i )
	{
		for( int h = 0; h < 4; ++h )				// remap all blend indices
		{
			for( int j = 0; j < uniqueMatricesCount; ++j )
			{
				if( m_BindingInfo[i].blendIndices[h] == m_BoneIndexMap[j] )
				{
					m_BindingInfo[i].blendIndices[h] = j;
					break;
				}
			}
		}
	}

	// Note: load character cloth when executable is in tool mode
	if( drawableVertsSkinnedOut )
	{
		const int vertComponentCount = mtlBase->GetVertexCount();
		Assert( vertComponentCount > 0 );
		Vec3V* vertsComponentSkinned = Alloca(Vec3V, vertComponentCount);
		Assert( vertsComponentSkinned );

		const int numBonesBase = skeleton->GetBoneCount();
		Matrix43* mtxBase = Alloca(Matrix43, numBonesBase);
		skeleton->Attach( true, mtxBase );
		SkinMeshFromMaterial( mtxBase, mtlBase, vertsComponentSkinned, NULL/*normComponentSkinned*/ );

		int* drawableVertsSkinnedCountOut = (int*)(((char*)drawableVertsSkinnedOut) - 4);
		*drawableVertsSkinnedCountOut = vertComponentCount;

		*drawableVertsSkinnedOut = rage_new Vec3V[vertComponentCount];
		for( int i = 0; i < vertComponentCount; ++i )
		{
			(*drawableVertsSkinnedOut)[i] = vertsComponentSkinned[i];
		}
	}


	if( doTheMorph )
	{
// read the morph/mapping distances
		float* mappingThreshold = Alloca(float,nChannelElements);
		float* errorThreshold = Alloca(float,nChannelElements);

		if( !ReadVertexBlindData( mappingThreshold, mtlCloth, mshVtxBlindCLOTH_MAPPING_THRESHOLD, DEFAULT_MAPPING_THRESHOLD ) )
		{
			for( int i = 0; i < nChannelElements; ++i )
				mappingThreshold[i] = DEFAULT_MAPPING_THRESHOLD;
 		}

		if( !ReadVertexBlindData( errorThreshold, mtlCloth, mshVtxBlindCLOTH_ERROR_THRESHOLD, DEFAULT_ERROR_THRESHOLD ) )
		{
			for( int i = 0; i < nChannelElements; ++i )
				errorThreshold[i] = DEFAULT_ERROR_THRESHOLD;
		}


		const int vertComponentCount = mtlBase->GetVertexCount();
		Assert( vertComponentCount > 0 );
		Vec3V* vertsComponentSkinned = Alloca(Vec3V, vertComponentCount);
		Assert( vertsComponentSkinned );

 		Vec3V* normComponentSkinned = Alloca(Vec3V, vertComponentCount);
 		Assert( normComponentSkinned );

		const int numBonesBase = skeleton->GetBoneCount();
		Matrix43* mtxBase = Alloca(Matrix43, numBonesBase);
		skeleton->Attach( true, mtxBase );
		SkinMeshFromMaterial( mtxBase, mtlBase, vertsComponentSkinned, /*NULL*/ normComponentSkinned );

		sysMemStartTemp();
		tempMorphData = rage_new phMorphDataAoS[vertComponentCount];
		sysMemEndTemp();

// NOTE: use SKEL_Spine0 to check for inward verts 
		int boneIndex = 0;
		const crBoneData* boneData = skeleton->GetSkeletonData().FindBoneData( "SKEL_Spine0" );
		if( !boneData )
		{
			skeleton->GetSkeletonData().ConvertBoneIdToIndex( atHash16U("SKEL_Spine0"), boneIndex );
		}
		else
		{
			boneIndex = boneData->GetIndex();
		}
		Assert( boneIndex > 0 );
		Vec3V spineVtx = skeleton->GetObjectMtx(boneIndex).GetCol3();

#if NO_PIN_VERTS_IN_VERLET
		const int pinnedCount = pCloth->GetClothData().GetNumPinVerts();
#else
		const int pinnedCount = pCloth->GetPinCount();
#endif
		int pinnedMatch = 0;
		for(int i = 0; i < vertComponentCount; ++i )
		{
			Vec3V tempV = Subtract( spineVtx, vertsComponentSkinned[i] );
  			const bool inwardVtx = Dot( normComponentSkinned[i], tempV).Getf() > 0.0f ? true: false;

			if( FindCoordinates( mappingThreshold, errorThreshold, pinnedMatch, pinnedCount, i, vertsComponentSkinned[i], inwardVtx, triIndicesCount, m_TriIndices.GetElements() /*mtlCloth*/, (Vec3V*)clothPos, &tempMorphData[tempMorphDataCount] ) )
			{
				++tempMorphDataCount;
			}
	//		Assert( pinnedMatch < pinnedCount );	// why more verts are mapped to the pinned than initial pinned count
		}

	//	Assert( pinnedMatch == pinnedCount );	// pinned count from cloth sim should match what has been found in the drawable
	}


	Vec3V* clothPosV = (Vec3V*)clothPos;
	for( int i = 0; i < vertsCount; ++i )
	{
		clothPosV[i].SetW( MAGIC_ZERO );		// since cloth simulation is using compressed deltas, those need to be reset to 0
	}

	if( !cccTuning.GetFlag(characterClothControllerTuning::FLAG_DONT_USE_BEND_EDGES) )
	{
		AddCustomEdges( 0/*lodIndex*/, connectivity, m_bendSpringStrengthChannelData );
	}

#if USE_BONEID
	m_BoneIDMap.Resize(uniqueMatricesCount);
	for( int i = 0; i < uniqueMatricesCount; ++i )
	{
		const crSkeletonData& skelData = skeleton->GetSkeletonData();
		u16 boneId;
		if( skelData.ConvertBoneIndexToId(m_BoneIndexMap[i], boneId) )
		{
			m_BoneIDMap[i] = (int)boneId;
		}
		else
		{
			m_BoneIDMap[i] = -1;
			AssertMsg(0, "Can't find boneId ?!" );
		}
	}
#endif

	// Cleanup the bound
	sysMemStartTemp();
	if( phConfig::IsRefCountingEnabled() )
	{
		clothBound->Release();
	}
	else
	{
		delete clothBound;
	}

//	m_Cloth[0]->GetClothData().VerifyMesh( connectivity );

	delete connectivity;

	if( m_bendSpringStrengthChannelData )
		delete [] m_bendSpringStrengthChannelData;
	if( m_bendSpringLengthChannelData )
		delete [] m_bendSpringLengthChannelData;

	m_bendSpringStrengthChannelData = NULL;
	m_bendSpringLengthChannelData = NULL;

	sysMemEndTemp();
}

void characterClothController::AddCustomEdges( int lodIndex, phClothConnectivityData* connectivity, float* m_bendSpringStrengthChannelData )
{
	m_Cloth[lodIndex]->GetClothData().VerifyMesh( connectivity );
	m_Cloth[lodIndex]->AddCustomEdges( m_bendSpringStrengthChannelData );
}


bool characterClothController::ReadVertexBlindData( float *perVertexData, const mshMaterial* mtl, int channel, float defaultValue ) const
{
	Assert( mtl );

	int nChannel = mtl->FindChannel( channel );
	if( nChannel >= 0 )
	{
		int nBoundVert = m_Cloth[0]->GetNumVertices();
		for( int iBoundVert = 0; iBoundVert < nBoundVert; iBoundVert++ )
		{
			perVertexData[ iBoundVert ] = defaultValue;
		}

		const mshChannel& channel = mtl->GetChannel(nChannel);
		int nVerts = channel.GetVertexCount();

		for( int i = 0; i < nVerts; i++ )
		{
			const mshChannelData& data = channel.Access(i, 0);
			perVertexData[ i ] = data.f;
		}
		return true;
	}
	return false;
}
#endif // MESH_LIBRARY


// Note: technically the following function MorphLevelONE is not used in character cloth ... here for debug purposes and reference only
#if __PFDRAW
void characterClothController::MorphLevelONE( const phMorphData* morphData, const int morphDataCount, Vec3V* RESTRICT /*streamControlled*/, Vec3V* RESTRICT streamController )
{
	Assert( morphData );
	Assert( morphDataCount > 0 );

	for( int i = 0; i < morphDataCount; ++i)
	{
		// technically in the game wLevel should be _ONE .. here for reference
		const int k = i /*  * wLevel*/;
	
		//		const u16 vtxIndex	= morphData->vtxIndex[k];
		const Vec4V weights = morphData->weights.m_Data[k];
		const u16 idx0		= morphData->index0.m_Data[k];
		const u16 idx1		= morphData->index1.m_Data[k];
		const u16 idx2		= morphData->index2.m_Data[k];

		const ScalarV w0 = weights.GetX();
		const ScalarV w1 = weights.GetY();
		const ScalarV w2 = weights.GetZ();
		const ScalarV  d = weights.GetW();

		const Vec3V vtxLow0 = streamController[ idx0 ];
		const Vec3V vtxLow1 = streamController[ idx1 ];
		const Vec3V vtxLow2 = streamController[ idx2 ];

		// Find the unit normal vector, perpendicular to the plane containing the three points
		const Vec3V v1 = Subtract( vtxLow0, vtxLow1 );
		const Vec3V v2 = Subtract( vtxLow2, vtxLow1 );
		const Vec3V n = Normalize( Cross( v1, v2 ) );

		/*streamControlled[ vtxIndex ]  = */ 
		Vec3V morphedVtx =	Scale( w2 , vtxLow0 ) + 
			Scale( w1 , vtxLow1 ) + 
			Scale( w0 , vtxLow2 ) + 
			Scale( d ,  n );

		PF_DRAW_SPHERE_COLOR( ClothEdges, 0.008f, VEC3V_TO_VECTOR3( morphedVtx ), Color_red );		

	}
}
#endif

sysTaskHandle characterClothController::UpdateSpu( 
#if (__PPU)
	const crSkeleton* pSkeleton, float fTime, const phBoundComposite* customBound, int* bonesIndices, Vec3V_In force
#else
	const crSkeleton* , float , const phBoundComposite* , int* , Vec3V_In  
#endif
	, phVerletSPUDebug* SPU_DEBUG_DRAW_ONLY( verletSpuDebug )
)
{
#if (__PPU)
	if (sm_EnableSpuUpdate)
	{
		Assert( pSkeleton );
		phVerletCloth* pCloth = m_Cloth[0];
		Assert( pCloth );

		const int allignedSizeOfCharacterClothController = ((sizeof(*this)+15)&~15);
		const int allignedSizeOfCloth = ((sizeof(*pCloth)+15)&~15);
		int inputsize = allignedSizeOfCloth + allignedSizeOfCharacterClothController + 16;		
		int stacksize = 10 * 1024;
		int reduceScratchSize = 1 * 1024;
		int scratchSize = kMaxSPUJobSize - stacksize - inputsize - (int)TASK_INTERFACE_SIZE(UpdateCharacterCloth) - reduceScratchSize;
		sysTaskContext c(TASK_INTERFACE(UpdateCharacterCloth), 0, scratchSize, stacksize);

		c.SetInputOutput();

		c.AddInput(this, allignedSizeOfCharacterClothController);
		c.AddInput(pCloth, allignedSizeOfCloth);

		phVerletCharacterClothUpdate& u = *c.AllocUserDataAs<phVerletCharacterClothUpdate>();
		u.spu_skeleton	= const_cast<crSkeleton*>(pSkeleton);
		u.m_Gravity		= PHSIM->GetGravity();
		u.m_Gravity.w	= fTime;	
		u.customBound	= customBound;
		u.m_Force		= VEC3V_TO_VECTOR3(force);

		characterCloth* owner = (characterCloth*)this->GetOwner();
		Assert(owner);
		u.entityVertexBufferAddress = (u32)owner->GetEntityVerticesPtr();
		u.flagsOffsetAddress		= (u32)&this->m_ForcePin;
		u.bonesIndices				= bonesIndices;

		u.instLastMtxIdxAddrMM = PHLEVEL->GetLastInstanceMatrixIndexMapBaseAddr();
		u.instLastMatricsAddrMM = PHLEVEL->GetLastInstanceMatricesBaseAddr();		

		u.pauseSimulation = g_PauseClothSimulation;

 #if ENABLE_SPU_COMPARE
		if (gEnableSpuCompare)
			gpCompareBuf = gCompareBuf;
		gSpuCompareFailed = 0;
		u.compareBuf = gpCompareBuf;
 #endif

 #if ENABLE_SPU_DEBUG_DRAW
		if( verletSpuDebug )
		{
			Assert( verletSpuDebug->debugIdx < MAX_VERLET_SPU_DEBUG );
			Vec4V* v = (Vec4V*)&gDebugDrawPrimCount[verletSpuDebug->debugIdx]; 
			v->ZeroComponents();

			verletSpuDebug->debugDrawCountOut	= (u32)v;
			verletSpuDebug->debugDrawBuf = gDebugDrawBuf + verletSpuDebug->debugIdx * VERLET_DEBUG_BUFFER;

			u.verletSpuDebugAddr = (u32)verletSpuDebug;
		}
		else
		{
			u.verletSpuDebugAddr = 0;
		}
 #endif // ENABLE_SPU_DEBUG_DRAW

// NOTE: character cloth is using custom collision bound, so this is not used ... for now
//		SetCollisionInst( collisionInst );

		sysTaskHandle h = c.Start();
 #if ENABLE_SPU_COMPARE
		sysTaskManager::Wait(h);
		Update( force.GetWf(), skeleton, fTime, customBound, bonesIndices );
		Assert(gSpuCompareFailed < 64);
		gpCompareBuf = 0;
		return 0;
 #else		
		return h;
 #endif // ENABLE_SPU_COMPARE
	}
#endif // (__PPU)

// NOTE: the following shouldn't be used anymore - svetli
	Assert(0);
//	Update( VECTOR3_TO_VEC3V(PHSIM->GetGravity()), force.GetWf(), skeleton, fTime, customBound, bonesIndices);
	return 0;
}


// NOTE: for character cloth this fn should be called only in resource time

void characterClothController::OnClothVertexSwap( int swapIndexA, int swapIndexB, int lodIdnex, bool swapEdges ) 
{ 
	clothController::OnClothVertexSwap(swapIndexA,swapIndexB,lodIdnex,swapEdges); 

// Note: triangles indices don't support LODs ... yet
	const int indexCount = m_TriIndices.GetCount();
	for( int i = 0; i < indexCount; ++i )	
	{
		if( m_TriIndices[i] == swapIndexA )
			m_TriIndices[i] = (u16)swapIndexB;
		else if( m_TriIndices[i] == swapIndexB )
			m_TriIndices[i] = (u16)swapIndexA;
	}

	Vec3V tempPos =	m_OriginalPos[swapIndexA];
	m_OriginalPos[swapIndexA] = m_OriginalPos[swapIndexB];
	m_OriginalPos[swapIndexB] = tempPos;
}


#if MESH_LIBRARY

void characterClothController::PinClothVerts( const mshMaterial* mtl, phClothConnectivityData *connectivity, const int extraPinRadiusChannels )
{
	Assert( mtl );
	int lodIndex = GetLOD();
	phVerletCloth* cloth = GetCloth(lodIndex);
	Assert( cloth );

	int vertexCount = mtl->GetVertexCount();
	int* pinMap = Alloca(int, vertexCount);

	clothBridgeSimGfx* pClothBridge = GetBridge();
	Assert( pClothBridge );

	float clothWeight;
	const int numPinnedVerts = pClothBridge->CreatePinData( extraPinRadiusChannels, lodIndex, mtl, vertexCount, pinMap, true, clothWeight );

// Note: read original verts from the material	
	m_OriginalPos.Resize( vertexCount );

 	sysMemStartTemp();	
	atArray< int > pinList;
	pinList.Resize( numPinnedVerts );
	sysMemEndTemp();

	for( int i = 0; i < numPinnedVerts; i++ )
	{
		pinList[i] = pinMap[i];
	}

	for( int i = 0; i < vertexCount; i++ )
	{
		m_OriginalPos[i] = VECTOR3_TO_VEC3V( mtl->GetVertex( i ).Pos );
	}


	atFunctor4< void, int, int, int, bool > funcy;
	funcy.Reset< characterClothController, &characterClothController::OnClothVertexSwap >( this );
	cloth->PinVerts( lodIndex, pinList, connectivity, &funcy );
	cloth->SetClothWeight(clothWeight);

	sysMemStartTemp();
	pinList.Reset();
	sysMemEndTemp();
}

#if __PFDRAW
void characterClothController::DebugStuff()
{
/*
	static float radius = 0.01f;
	const Vec3V* RESTRICT pos = GetCloth(0)->GetClothData().GetVertexPointer();
	const int vPinCount = GetCloth(0)->GetPinCount();
//	const int vTotalCount = GetCloth(0)->GetNumVertices();
	int i=0;
	for( ; i < vPinCount; ++i )
	{
		PF_DRAW_SPHERE_COLOR( ClothEdges, radius, VEC3V_TO_VECTOR3(pos[ i ]), Color_red );		
	}

// 	for( ; i < vTotalCount; ++i )
// 	{
// 		PF_DRAW_SPHERE_COLOR( ClothEdges, radius, VEC3V_TO_VECTOR3(pos[ clothDisplayMap[i] ]), Color_white );		
// 	}

*/


//	MorphLevelONE( m_MorphData.GetElements(), m_MorphData.GetCount(), NULL, GetCloth(0)->GetClothData().GetVertexPointer() );


// 
// 	static float offX = 0.0f;
// 	static float offZ = 0.0f; //0.21f;
// 	Vec3V off(V_ZERO);
// 	off.SetXf( offX );
// 	off.SetZf( offZ );
// 
// 	const int vTotalCount = GetCloth(0)->GetNumVertices();
// 	for( int i = 0; i < vTotalCount; ++i )
// 	{
// 		PF_DRAW_SPHERE_COLOR( ClothEdges, 0.005f, VEC3V_TO_VECTOR3( Add( vertsClothSkinned[i], off ) ), Color_red );		
// 	}
// 

// 	for( int i = 0; i < vertComponentCount; ++i )
// 	{
// 		PF_DRAW_SPHERE_COLOR( ClothEdges, 0.002f, VEC3V_TO_VECTOR3( vertsComponentSkinned[i] ), Color_white );
// 	}

}
#endif


#endif	// MESH_LIBRARY
}
