// 
// cloth/characterclothcontroller.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef CLOTH_CHARACTERCLOTHCONTROLLER_H
#define CLOTH_CHARACTERCLOTHCONTROLLER_H

#include "clothcontroller.h"
#include "crskeleton/skeleton.h"

#define USE_BONEID				1
#define NO_BONE_MATRICES		1

namespace rage
{

class crSkeleton;

#define  PIN_RADIUSSETS_PACKAGE_THRESHOLD  0.04f

#if !__SPU

struct characterClothControllerTuning
{
	enum enFlags
	{
		FLAG_DONT_USE_BEND_EDGES	= 0,
	};

	atFixedBitSet<32, u32>		m_Flags;
	int m_ExtraPinRadiusChannels;

	void Load(const char* filePath, const char* fullName);
	bool GetFlag(enFlags flag) const { return m_Flags.IsSet(flag); }

	static const char* sm_MetaDataFileAppendix;

	PAR_SIMPLE_PARSABLE;
};

#endif // !__SPU


class characterClothController : public clothController
{
public:
	static bool sm_EnableSpuUpdate;

	characterClothController();
	virtual ~characterClothController();

	DECLARE_PLACE(characterClothController);
	characterClothController(class datResource& rsc);

#if CLOTH_INSTANCE_FROM_DATA
	void InstanceFromData(int vertsCapacity, int edgesCapacity, int boneIDsCapacity);
#endif

	void InstanceFromTemplate( const characterClothController* copyme );

	void Load( Vec3V** drawableVertsSkinnedOut, bool doTheMorph, phMorphDataAoS* &tempMorphData, int& tempMorphDataCount, const crSkeleton* skeleton, const mshMaterial* mtlBase, mshMaterial* mtl, int nModel );
	void Update( Vec3V_In gravityV, float timeScale, const crSkeleton* skeleton, float fTime, const phBoundComposite* customBound, int* boneIndices);
	void UpdateAgainstCollisionInsts( const crSkeleton* skeleton, const phBoundComposite* customBound, int* boneIndices);
	sysTaskHandle UpdateSpu ( const crSkeleton* skeleton, float fTime, const phBoundComposite* customBound, int* bonesIndices, Vec3V_In force, phVerletSPUDebug* s_VerletSpuDebug );

	void RecomputeNormals( int nVerts, Vec3V_In 
#if __PFDRAW
		vParentOffset 
#endif
		);

	void SetPinningRadiusScale(float pinningRadiusScale) { m_PinningRadiusScale = pinningRadiusScale; }
	float GetPinningRadiusScale() const { return m_PinningRadiusScale; }
	float* GetPinningRadiusScalePtr() { return &m_PinningRadiusScale; }

//	void InitCollision();
	void ResetW();

#if __PFDRAW
	void DebugStuff();
#endif

#if __BANK
	static void AddWidgets(bkBank& bk);
#endif

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	class BindingInfo
	{
	public:
		BindingInfo(class datResource& rsc);
		BindingInfo() {}

		DECLARE_PLACE(BindingInfo);

		Vector4 weights;
		int blendIndices[4];
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct &s)
		{
			SSTRUCT_BEGIN(BindingInfo)
				SSTRUCT_FIELD(BindingInfo,weights)
				SSTRUCT_CONTAINED_ARRAY(BindingInfo,blendIndices)
			SSTRUCT_END(BindingInfo)
		}
#endif
		PAR_SIMPLE_PARSABLE;
	};

	const atArray<BindingInfo>* GetBindingInfo() const { return (m_ControllerType? &m_ControllerType->m_BindingInfo: &m_BindingInfo); }
#if !NO_BONE_MATRICES
	atArray<Matrix43>& GetMatrixSetRef() { return m_PoseMatrixSet; }
#endif
	
	atArray<s32>& GetBoneIndexMapRef() { return m_BoneIndexMap; }
	const atArray<s32>& GetBoneIDMapRef() const { return (m_ControllerType ? m_ControllerType->m_BoneIDMap: m_BoneIDMap); }
	const atArray<u16>*	GetTriIndices() const { return (m_ControllerType? &m_ControllerType->m_TriIndices: &m_TriIndices); }
	const atArray<Vec3V>* GetOriginalPos() const { return (m_ControllerType? &m_ControllerType->m_OriginalPos: &m_OriginalPos);	}

	enum enFlags
	{
		enIsForceSkin		= 1 << 0,
		enIsFalling			= 1 << 1,
		enIsSkydiving		= 1 << 2,
		enIsProne			= 1 << 3,
		enIsProneFlipped	= 1 << 4,
		enIsQueuedPinning	= 1 << 5,
		enIsQueuedPose		= 1 << 6,
		enIsEndCutscene		= 1 << 7,
	};

	bool GetFlag(enFlags flagToCheck) const { return (m_Flags & flagToCheck) ? true: false;	}
	void SetFlag(enFlags flagToSet, bool isTrue)
	{
		if( isTrue )
			m_Flags |= flagToSet;
		else
			m_Flags &= (~flagToSet);
	}

	float GetWindScale() const { return m_WindScale; }
	void SetWindScale(float windScale) { m_WindScale = windScale; }
	void SetForcePin(const u8 pinnedFrames) { m_ForcePin = pinnedFrames; }
	void AddForcePin(const u8 pinnedFrames) { m_ForcePin += pinnedFrames; }
	void SkinMesh( const bool, const Matrix43* RESTRICT mtx, const int vtxCount, const Vector3* RESTRICT posIn, const BindingInfo* RESTRICT bInfo, const int bInfoOffset, Vec3V* RESTRICT poseOut );

	void SkinMesh( const crSkeleton* skeleton );
	void SetPackageIndex(u8 index, u8 transitionFrames = 0);
	u8 GetPackageIndex() const { return m_PackageIndex; }

	static bool FindCoordinates( float* RESTRICT mappingThreshold, float* RESTRICT errorThreshold, int& pinnedMatch, int numPinned, int vertexIndexHi, Vec3V_In skinnedVtxHi, bool inwardVtx, int triIndicesCount, const u16* RESTRICT triIndices, Vec3V* skinnedVerts, phMorphDataAoS* outData );

	void SetPinRadiusSetThreshold(float thresholdf)
	{
		Assert( thresholdf > -0.000001f );
#if NO_BONE_MATRICES
		m_fPinRadiusSetThreshold = thresholdf;
#else
		Assert( m_OriginalPos.GetCount() == 1 );
		m_OriginalPos[0].SetWf(thresholdf);
#endif
	}	

	float GetPinRadiusThreshold() const
	{
#if NO_BONE_MATRICES
		return m_fPinRadiusSetThreshold;
#else
		// TODO: need 4 bytes to store somewhere pin radius sets threshold
		// m_OriginalPos is not used in the instance, but is used in the type
		Assert( m_OriginalPos.GetCount() == 1 );
		m_OriginalPos[0].GetW();
#endif
	}

	void CheckQueuedPinning()
	{
		if( GetFlag(enIsQueuedPinning) )
		{
			SetForcePin(1);
			SetFlag(enIsQueuedPinning,false);
		}
	}

	u8 GetFlags() const { return m_Flags; }
	void  SetFlags(u8 _flags) { m_Flags = _flags; }

#if !__SPU
	void Load(const char* filePath = "common:/data/cloth/");	
	void Save(const char* filePath = "common:/data/cloth/");
 #if __BANK && !__RESOURCECOMPILER
	void RegisterRestInterface(const char* controllerName);
	void UnregisterRestInterface(const char* controllerName);
 #endif
#endif


	static const char* sm_CharacterName;
	static const char* sm_MetaDataFileAppendix;

#if !__SPU
private:
#endif

	void ApplySimulationToMesh();
	void PinClothVerts( const mshMaterial* mtl, phClothConnectivityData *connectivity, const int extraPinRadiusChannels);

	void AddCustomEdges(  int lodIndex, phClothConnectivityData* connectivity, float* );
	bool ReadVertexBlindData( float *perVertexData, const mshMaterial* mtl, int channel, float defaultValue ) const;

	void MorphLevelONE( const phMorphData* morphData, const int morphDataCount, Vec3V* RESTRICT /*streamControlled*/, Vec3V* RESTRICT streamController );		

#if !__SPU
protected:
#endif
	void OnClothVertexSwap( int swapIndexA, int swapIndexB, int lodIdnex, bool swapEdges );

	float							m_PinningRadiusScale;
	char							m_Pad[4];
	atArray<u16>					m_TriIndices;				// used in RecomputeNormals

	atArray<Vec3V>					m_OriginalPos;

#if NO_BONE_MATRICES
	float							m_fPinRadiusSetThreshold;
	char							m_Padding[sizeof(atArray<Matrix43>) - 4];
#else
	atArray<Matrix43>				m_PoseMatrixSet;
#endif

// NOTE: boneidxs/boneids here are for simulation purposes

	atArray<s32>					m_BoneIndexMap;
	atArray<BindingInfo>			m_BindingInfo;
	pgRef<const characterClothController> m_ControllerType;

	// TODO: may be m_ForcePin, m_Flags should be moved in the characterCloth class ??

	// NOTE: forcepin has 2 meanings:
	//	when packageuindex != newpackageindex then forcepin is framecount ( counting downwards ) for switching between packageuindex and newpackageindex
	//	when packageuindex == newpackageindex then forcepin is just framecount ( counting downwards ) for the "special case" - skinning
	u8								m_ForcePin;				// forcepin has to fall on 16 byte aligned adddr
	u8								m_Flags;
	u8								m_NewPackageIndex;
	u8								m_PackageIndex;			// which set of values from the bridge should be used
	float							m_WindScale;

	atArray<s32>					m_BoneIDMap;

	PAR_SIMPLE_PARSABLE;
};


}

#endif	// CLOTH_CHARACTERCLOTHCONTROLLER_H

