<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::dataVec3V">
  <array name="m_Data" type="member" size="4">
    <u32/>
  </array>
</structdef>
  
<structdef type="::rage::characterCloth">
  <array name="m_Poses" type="atArray" align="16">
    <struct type="::rage::dataVec3V"/>
  </array>
  <pointer name="m_BoundComposite" type="::rage::phBoundComposite" policy="owner"/>
  <array name="m_BoneIndex" type="atArray" align="16">
    <int />
  </array> 
</structdef>

<structdef type="::rage::characterClothDebug">
  <array name="m_Poses" type="atArray">
    <Vec3V/>
  </array>    
</structdef>

</ParserSchema>