// 
// cloth/environmentcloth.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "environmentcloth.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "data/safestruct.h"
#include "diag/tracker.h"
#include "file/token.h"
#include "grcore/texture.h"
#include "grmodel/geometry.h"
#include "grrope/grroperender.h"
#include "mesh/serialize.h"
#include "phcore/phmath.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundsphere.h"
#include "pheffects/clothconnectivitydata.h"
#include "pheffects/clothverletinst.h"
#include "physics/collider.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "profile/page.h"
#include "profile/group.h"
#include "profile/element.h"
#include "rmcore/drawable.h"
#include "system/memory.h"
#include "vector/matrix34.h"
#include "vectormath/legacyconvert.h"

#include "rmcore/lodgroup.h"

#include "environmentcloth_parser.h"

#define		MIN_ROPE_RADIUS			0.01f
#define		DEFAULT_ROPE_RADIUS		0.02f

//#pragma optimize("", off)


namespace rage
{

const char* clothInstanceTuning::sm_tuneFilePath = "common:/data/cloth/";
const char* clothInstanceTuning::sm_MetaDataFileAppendix = "_tuning";
const char* clothVertexBlend::sm_tuneFilePath = "common:/data/cloth/";
const char* clothVertexBlend::sm_MetaDataFileAppendix = "_blenddata";
const char* environmentCloth::sm_MetaDataFileAppendix = "_userdata";


EXT_PFD_DECLARE_ITEM(ClothPendants);

#if (__BANK && !__RESOURCECOMPILER)
extern datCallback	g_ClothManagerCallback;
#endif

#if __RESOURCECOMPILER
extern	CustomBoundsCB	g_CustomBoundsCB;
#endif

namespace phClothStats
{
	EXT_PF_TIMER(EnvClothSimUpdate);
	EXT_PF_TIMER(EnvClothSimSpuUpdate);	
	EXT_PF_TIMER(EnvClothPostSpuUpdate);	
	EXT_PF_TIMER(ApplyAirResistance);
	EXT_PF_TIMER(EnvClothCheckDistance);	
}
using namespace phClothStats;


IMPLEMENT_PLACE( phEnvClothVerletBehavior );


phEnvClothVerletBehavior::phEnvClothVerletBehavior(class datResource& rsc)
	: phClothVerletBehavior(rsc)
	, m_EnvCloth(rsc)
{
}

sysTaskHandle phEnvClothVerletBehavior::UpdateRopeSimSpu( float timeStep )
{
	clothController* pClothController = m_EnvCloth->GetClothController();
	Assert( pClothController );
	phVerletCloth* pCloth = m_EnvCloth->GetCloth();
	Assert( pCloth );
	Assert( pCloth->GetFlag(phVerletCloth::FLAG_IS_ROPE) );
 	const phArchetype* pArchetype = m_Instance->GetArchetype();
 	Assert( pArchetype );
	return pCloth->UpdateEnvironmentRopeSpu( ScalarVFromF32(timeStep),GetCollisionInstRef(), ScalarVFromF32(pArchetype->GetGravityFactor()), pClothController, sizeof(clothController) );
}

sysTaskHandle phEnvClothVerletBehavior::UpdateClothSimSpu ( Mat34V_In attachedFrame, float timeStep, grmGeometryQB* pGeom, clothController* pController, Vec3V_In force, phVerletSPUDebug* verletSpuDebug)
{
	Assert( pController );
	const int simLodIndex = pController->GetLOD();
//	const int drwLodIndex = m_EnvCloth->GetDrawable()->GetLodGroup().GetLodIndex();

	bool bClothSimMeshOnly = false;
#if CLOTH_SIM_MESH_ONLY	
	bClothSimMeshOnly = m_EnvCloth->GetFlag(environmentCloth::CLOTH_IsSimMeshOnly);
#endif

	phVerletCloth* pCloth = m_EnvCloth->GetCloth();
	Assert( pCloth );
	Assert( pCloth->GetNumVertices() < MAX_CLOTH_VERTS_ON_SPU );

	phVerletCloth* drwCloth = m_EnvCloth->GetFlag(environmentCloth::CLOTH_IsMorph) ? pController->GetCloth(0 /*drwLodIndex*/): NULL;

	Assert( !pCloth->GetFlag(phVerletCloth::FLAG_IS_ROPE) );

	float gravityFactor;
#if CLOTH_SIM_MESH_ONLY
	if( m_EnvCloth->GetFlag(environmentCloth::CLOTH_IsSimMeshOnly) )
	{
		gravityFactor = 1.0f;
	}
	else
#endif
	{
		const phArchetype* pArchetype = m_Instance->GetArchetype();
		Assert( pArchetype );
		gravityFactor = pArchetype->GetGravityFactor();
	}

	const clothInstanceTuning* pClothTuning = m_EnvCloth->GetTuning();
	const float sign = (pClothTuning && pClothTuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_FLIP_GRAVITY)) ? -1.0f : 1.0f;
	ScalarV gravityScale = ScalarVFromF32( gravityFactor * sign);	

	return pCloth->UpdateEnvironmentClothSpu( attachedFrame, ScalarVFromF32(timeStep),GetCollisionInstRef(), gravityScale, pGeom, pController, sizeof(clothController), force, simLodIndex, drwCloth, bClothSimMeshOnly, IsMotionSeparated(), GetUserDataAddress(), verletSpuDebug );
}


void phEnvClothVerletBehavior::UpdateClothSim (float timeScale, float timeStep, Mat34V_In attachedFrame )
{
	const int curSimLodIndex = m_EnvCloth->GetClothController()->GetLOD();
	const int curDrawableLodIndex = 0; //m_EnvCloth->GetDrawable()->GetLodGroup().GetLodIndex();

	m_EnvCloth->GetClothController()->ApplyPinning(attachedFrame, curSimLodIndex);

	if( m_EnvCloth->GetFlag( environmentCloth::CLOTH_IsSkipSimulation ) )
	{
		m_EnvCloth->SetFlag( environmentCloth::CLOTH_IsSkipSimulation, false );
		return;
	}

	if( m_EnvCloth->GetFlag( environmentCloth::CLOTH_ForceInactiveForReplay ) )
	{
		return;
	}

	phVerletCloth* pCloth = m_EnvCloth->GetCloth(curSimLodIndex);
	Assert( pCloth );

	Assert( !pCloth->GetFlag(phVerletCloth::FLAG_IS_ROPE) );

	float gravityFactor;
#if CLOTH_SIM_MESH_ONLY
	if( m_EnvCloth->GetFlag(environmentCloth::CLOTH_IsSimMeshOnly) )
	{
		gravityFactor = 1.0f;
	}
	else
#endif
	{
		Assert( m_Instance );
		const phArchetype* pArchetype = m_Instance->GetArchetype();
		Assert( pArchetype );
		gravityFactor = pArchetype->GetGravityFactor();
	}


	const clothInstanceTuning* pClothTuning = m_EnvCloth->GetTuning();
	const float sign = (pClothTuning && pClothTuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_FLIP_GRAVITY)) ? -1.0f : 1.0f;
	ScalarV gravityScale = ScalarVFromF32( gravityFactor * sign );

	pCloth->UpdateEnvironmentCloth( m_EnvCloth->GetOffset(), timeScale, ScalarVFromF32(timeStep),GetCollisionInstRef(), phSimulator::GetGravityV(),gravityScale );

	if( m_EnvCloth->GetFlag(environmentCloth::CLOTH_IsMorph) )
	{		
		m_EnvCloth->GetClothController()->Morph( curDrawableLodIndex, curSimLodIndex );
	}

	const bool bMoved = UpdateVerletBound(pCloth);
	if( bMoved )
	{		
		if( pClothTuning && pClothTuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_USE_DISTANCE_THRESHOLD) )
		{
			Vec3V tempV = Subtract( m_EnvCloth->GetInitialPosition(), VECTOR3_TO_VEC3V(m_EnvCloth->GetFramePosition()) );
			BoolV overThreshold = IsGreaterThan(Dot(tempV,tempV), ScalarVFromF32(pClothTuning->m_DistanceThreshold));
			if( overThreshold.Getb() )
				m_EnvCloth->SetFlag( environmentCloth::CLOTH_IsMoving, true );
		}		
	}

	Reset();
}


void phEnvClothVerletBehavior::UpdateAgainstCollisionInsts(Mat34V_In attachedFrame)
{
	const int curSimLodIndex = m_EnvCloth->GetClothController()->GetLOD();
	const int curDrawableLodIndex = 0; //m_EnvCloth->GetDrawable()->GetLodGroup().GetLodIndex();
	
	phVerletCloth* pCloth = m_EnvCloth->GetCloth(curSimLodIndex);
	Assert( pCloth );
	Assert( !pCloth->GetFlag(phVerletCloth::FLAG_IS_ROPE) );

	m_EnvCloth->GetClothController()->ApplyPinning(attachedFrame, curSimLodIndex);
	
	pCloth->UpdateEnvironmentClothAgainstCollisionInsts( m_EnvCloth->GetOffset(), GetCollisionInstRef());
	
	if( m_EnvCloth->GetFlag(environmentCloth::CLOTH_IsMorph) )
	{		
		m_EnvCloth->GetClothController()->Morph( curDrawableLodIndex, curSimLodIndex );
	}
	const bool bMoved = UpdateVerletBound(pCloth);
	(void)bMoved;

	Reset();
}


void phEnvClothVerletBehavior::UpdateRopeSim ( float timeStep, Mat34V_In attachedFrame )
{
	m_EnvCloth->GetClothController()->ApplyPinning( attachedFrame, m_EnvCloth->GetClothController()->GetLOD() );

	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert( cloth );
	Assert( cloth->GetFlag(phVerletCloth::FLAG_IS_ROPE) );

	const phArchetype* archetype = m_Instance->GetArchetype();
	Assert( archetype );
	cloth->UpdateEnvironmentRope( phSimulator::GetGravityV(), ScalarVFromF32(timeStep), GetCollisionInstRef(), ScalarVFromF32(archetype->GetGravityFactor()) );

	UpdateVerletBound(cloth);

	Reset();
}

bool phEnvClothVerletBehavior::CollideObjects (Vec::V3Param128 /*timeStep*/, phInst* myInst, phCollider* /*myCollider*/, phInst* otherInst, phCollider* /*otherCollider*/, phInstBehavior* /*otherInstBehavior*/ )
{
	Assert(myInst);
	const phArchetype* myArchetype = myInst->GetArchetype();
	Assert(myArchetype);
	phVerletCloth* cloth = m_EnvCloth->GetCloth();
	Assert(cloth);
	if (PHSIM->LastUpdateThisFrame() )
	{
		// Don't collide with anything if the cloth is not visible, because it doesn't update invisible and doesn't clear its collision instance list.
		// Don't collide with anything before the last simulator update.

// TODO: once IsCloth is remove the rest is not needed

// 		if( otherInstBehavior && otherInstBehavior->IsCloth() )
// 		{
// 			if (!cloth->GetFlag(phVerletCloth::FLAG_IS_ROPE))
// 			{
// 				Assert( otherInst == otherInstBehavior->GetInstance() );
// 				const phBound *pBound = otherInst->GetArchetype()->GetBound();
// 				Assert( pBound );
// 				if(pBound && pBound->IsTypeComposite(pBound->GetType()))
// 				{
// 					const phBoundComposite *pBoundComposite = static_cast<const phBoundComposite*>(pBound);
// 					Assert( pBoundComposite );
// 					Assert( pBoundComposite->GetNumBounds() );
// 					if( pBoundComposite->GetBound(0)->GetType() != phBound::SPHERE )
// 						return true;	// NOTE: frag has cloth and non-cloth bounds ... return true for proper collision response if is not sphere ... vehicle may be ?
// 				}
// 			}
// 		}

		if( otherInst )
		{
			if (!cloth->GetFlag(phVerletCloth::FLAG_IS_ROPE) )
			{
				const phBound *myBound = myArchetype->GetBound();
				Assert( myBound );
				if(myBound && myBound->IsTypeComposite(myBound->GetType()))
				{
					const phBoundComposite *myBoundComposite = static_cast<const phBoundComposite*>(myBound);
					Assert( myBoundComposite );
					Assert( myBoundComposite->GetNumBounds() );

					// B*1931270:  Make sure to check against the first valid active bound for this inst. 
					// Frags that were broken off another may have the bound at index 0 set as NULL. Previously if this was the case, non-root frag insts with cloth wouldn't collide with anything.
					int firstBoundIndex = -1;
					phBound *firstBound = myBoundComposite->GetFirstActiveBound(firstBoundIndex);

					if( firstBound && firstBound->GetType() != phBound::SPHERE )
						return true;	// NOTE: frag has cloth and non-cloth bounds ... return true for proper collision response if is not sphere
				}
				return false;
			}

#if 1
			if(		otherInst->GetClassType() != DUP_PH_INST_FRAG_VEH
				&&  otherInst->GetClassType() != DUP_PH_INST_FRAG_PED
				&&	otherInst->GetClassType() != 9
				)
			{				
				u16 levelIndex = otherInst->GetLevelIndex();
				u32 fatID = (levelIndex & 0xffff) | (PHLEVEL->GetGenerationID(levelIndex) << 16);  // Justin says gen. ids are 16 bit.		

				phInstDatRefArray& collisionInstances = GetCollisionInstRef();			
				if( collisionInstances.Find( fatID ) == -1 )
				{
					if( collisionInstances.GetCount() < collisionInstances.GetCapacity() )
					{
						collisionInstances.Append() = fatID;	// NOTE: Add the other instance to the cloth's list of colliding objects.
 #if (__PPU)
						cloth->AddCollisionInst( fatID );
 #endif
					}
					else
					{
//						clothWarningf( "Cloth collision inst array is full !" );
					}
				}
			}
#endif // 1

		}
	}	
	return false;	// Return false to indicate that the simulator should not handle this collision.
}



#if __DECLARESTRUCT
void phEnvClothVerletBehavior::DeclareStruct(datTypeStruct &s)
{
	phClothVerletBehavior::DeclareStruct(s);
	STRUCT_BEGIN(phEnvClothVerletBehavior);
	STRUCT_FIELD_VP(m_EnvCloth);
	STRUCT_END();
}
#endif



// clothVertexBlend

IMPLEMENT_PLACE(clothVertexBlend);

#if __DECLARESTRUCT
void clothVertexBlend::DeclareStruct(datTypeStruct &s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE( clothVertexBlend, pgBase )		
		SSTRUCT_FIELD( clothVertexBlend, m_Vertex0 )
		SSTRUCT_FIELD( clothVertexBlend, m_Vertex1 )
	SSTRUCT_END( clothVertexBlend )
}
#endif

void clothVertexBlend::Load(const char* filePath, const char* clothName)
{
	char buff[256];
	formatf( buff, "%s%s%s", filePath, clothName, clothVertexBlend::sm_MetaDataFileAppendix );

	parSettings s = parSettings::sm_StrictSettings; 
	s.SetFlag(parSettings::USE_TEMPORARY_HEAP, true); 
	PARSER.LoadObject( buff, "xml", *this, &s);	

	clothDebugf1("Loaded cloth instance blend data from file: %s", buff);
}

void clothVertexBlend::Save(void* 
#if !__FINAL
							   clothName
#endif
							   )
{
#if !__FINAL
	Assert( clothName );

	char buff[256];
	formatf( buff, "%s%s%s", clothVertexBlend::sm_tuneFilePath, (char*)clothName, clothVertexBlend::sm_MetaDataFileAppendix );
	AssertVerify(PARSER.SaveObject( buff, "xml", this, parManager::XML));

	clothDebugf1("Saved cloth instance blend data to file: %s", buff);
#endif
}




// clothInstanceTuning

IMPLEMENT_PLACE(clothInstanceTuning);

#if __DECLARESTRUCT
void clothInstanceTuning::DeclareStruct(datTypeStruct &s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE( clothInstanceTuning, pgBase )
		SSTRUCT_FIELD( clothInstanceTuning, m_RotationRate )
		SSTRUCT_FIELD( clothInstanceTuning, m_AngleThreshold )
		SSTRUCT_FIELD( clothInstanceTuning, m_ExtraForce )		
		SSTRUCT_FIELD( clothInstanceTuning, m_Flags )
		SSTRUCT_FIELD( clothInstanceTuning, m_Weight )
		SSTRUCT_FIELD( clothInstanceTuning, m_DistanceThreshold )
		SSTRUCT_FIELD( clothInstanceTuning, m_PinVert )
		SSTRUCT_FIELD( clothInstanceTuning, m_NonPinVert0 )
		SSTRUCT_FIELD( clothInstanceTuning, m_NonPinVert1 )
		SSTRUCT_CONTAINED_ARRAY( clothInstanceTuning, m_Padding )		
	SSTRUCT_END( clothInstanceTuning)
}
#endif

#if __BANK && !__RESOURCECOMPILER
void clothInstanceTuning::AddWidgets(bkGroup* ragGroup)
{
	Assert( ragGroup );
	ragGroup->AddSlider("Tuning:  m_DistanceThreshold", &m_DistanceThreshold, 0.0f, 10.0, 0.001f, NullCB, "[Work in progress]." );	
}
#endif

void clothInstanceTuning::Load(const char* filePath, const char* clothName)
{
	char buff[256];
	formatf( buff, "%s%s%s", filePath, clothName, clothInstanceTuning::sm_MetaDataFileAppendix );

	parSettings s = parSettings::sm_StrictSettings; 
	s.SetFlag(parSettings::USE_TEMPORARY_HEAP, true); 
	PARSER.LoadObject( buff, "xml", *this, &s);	

	clothDebugf1("Loaded cloth instance tuning from file: %s", buff);
}

void clothInstanceTuning::Save(void* 
#if !__FINAL
							   clothName
#endif
							   )
{
#if !__FINAL
	Assert( clothName );

	char buff[256];
	formatf( buff, "%s%s%s", clothInstanceTuning::sm_tuneFilePath, (char*)clothName, clothInstanceTuning::sm_MetaDataFileAppendix );
	AssertVerify(PARSER.SaveObject( buff, "xml", this, parManager::XML));

	clothDebugf1("Saved cloth instance tuning to file: %s", buff);
#endif
}

void clothInstanceTuning::SaveEmpty(void* 
#if !__FINAL
							   clothName
#endif
							   )
{
#if !__FINAL
	clothInstanceTuning oEmptyTuning;
	oEmptyTuning.m_Flags.Set(CLOTH_TUNE_ACTIVATE_ON_HIT);

	Assert( clothName );

	char buff[256];
	formatf( buff, "%s%s%s", clothInstanceTuning::sm_tuneFilePath, (char*)clothName, clothInstanceTuning::sm_MetaDataFileAppendix );
	AssertVerify(PARSER.SaveObject( buff, "xml", &oEmptyTuning, parManager::XML));

	clothDebugf1("Saved empty cloth instance tuning to file: %s", buff);
#endif
}

// environmentCloth

IMPLEMENT_PLACE(environmentCloth);

environmentCloth::environmentCloth()
	: m_Force(Vec3V(V_ZERO))
	, m_ReferencedDrawable(NULL)
	, m_Behavior(NULL)
	, m_ClothController(NULL)
#if USE_CLOTH_PHINST
	, m_cthInst(NULL)
	, m_BoundIdx(-1)
#endif
	, m_AttachedToFrame(NULL)
	, m_Tuning(NULL)
	, m_Flags(0)
	, m_InitialPosition(Vec3V(V_ZERO))
{	
}

environmentCloth::environmentCloth( class datResource& rsc )
: m_UserData(rsc)
#if USE_CLOTH_PHINST
, m_BoundIdx(-1)
#endif
{
	if(datResource_IsDefragmentation)
	{
		clothDebugf1("Defrag environmentCloth: 0x%p.", this);
	}
#if USE_CLOTH_PHINST
	if( m_cthInst )
	{
		Assert( m_cthInst->GetArchetype() );
		phArchetype::Place( m_cthInst->GetArchetype(), rsc);
	}
#endif
}



#if !__SPU

#define CLOTH_FRAME() 	Mat34V frame = GetAttachedToFrame();		\
						Assert( m_Behavior );						\
						if( !m_Behavior->IsMotionSeparated() )		\
							frame.SetCol3( GetInitialPosition() );	\
						else										\
							frame.SetCol3( Vec3V(V_ZERO) );


Mat34V_Out environmentCloth::GetClothFrame() const
{
	CLOTH_FRAME()
	return frame;
}

void environmentCloth::LoadVerts()
{	
/*
	CLOTH_FRAME()
*/
	phClothData& clothData = GetCloth()->GetClothData();

	Assert( m_ClothController );
	clothData.Load( phClothData::sm_tuneFilePath, m_ClothController->GetName() );
// TODO: verts should be loaded directly in proper format ??
//	clothData.TransformVertexPositions( frame, 0 );	
}

void environmentCloth::LoadAll()
{
	Assert( m_ClothController );
	m_ClothController->Load();

// TODO: verts should be loaded directly in proper format ??
/*
	CLOTH_FRAME()

	GetCloth()->GetClothData().TransformVertexPositions( frame, 0 );
*/
}

void environmentCloth::LoadUserData(const char* filePath)
{
	char buff[256];
	formatf( buff, "%s%s%s", filePath, (char*)m_ClothController->GetName(), environmentCloth::sm_MetaDataFileAppendix );
	PARSER.LoadObject( buff, "xml", *this, &parSettings::sm_StrictSettings);	

	clothDebugf1("Loaded cloth user data from file: %s", buff);
}

void environmentCloth::SaveUserData(const char* 
#if !__FINAL
									filePath
#endif
									)
{
#if !__FINAL
	char buff[256];
	formatf( buff, "%s%s%s", filePath, (char*)m_ClothController->GetName(), environmentCloth::sm_MetaDataFileAppendix );
	AssertVerify(PARSER.SaveObject( buff, "xml", this, parManager::XML));

	clothDebugf1("Saved cloth user data to file: %s", buff);
#endif
}

void environmentCloth::SaveEmptyTuning()
{
#if !__FINAL
	clothInstanceTuning::SaveEmpty( const_cast<char*>(m_ClothController->GetName()) );
#endif
}

void environmentCloth::SaveVerts()
{
#if !__FINAL
/*
	CLOTH_FRAME()

	Mat34V frameInv;
	InvertTransformFull( frameInv, frame );
*/
	phClothData& clothData = GetCloth()->GetClothData();
//	clothData.TransformVertexPositions( frameInv, 0 );

	Assert( m_ClothController );
	clothData.Save( const_cast<char*>(m_ClothController->GetName()) );

//	clothData.TransformVertexPositions( frame, 0 );
#endif
}

void environmentCloth::SaveAll()
{
#if !__FINAL
/*
	CLOTH_FRAME()

	Mat34V frameInv;
	InvertTransformFull( frameInv, frame );
*/
//	phClothData& clothData = GetCloth()->GetClothData();
//	clothData.TransformVertexPositions( frameInv, 0 );

	Assert( m_ClothController );
	m_ClothController->Save();

//	clothData.TransformVertexPositions( frame, 0 );
#endif
}

void environmentCloth::InitUserData()
{
	Assert( GetCloth() );
	const int numVerts = GetCloth()->GetNumVertices();
	m_UserData.Resize( numVerts );
	for( int i = 0; i < numVerts; ++i )
		m_UserData[i] = 0;
}


#if __BANK && !__RESOURCECOMPILER
void environmentCloth::AddWidgets(bkGroup* ragGroup)
{
	Assert( ragGroup );
#if CLOTH_SIM_MESH_ONLY
	if(GetFlag(CLOTH_IsSimMeshOnly))
	{
		ragGroup->AddButton("Force active", datCallback( MFA(environmentCloth::ForceVisible), this ) );
	}
#endif
	ragGroup->AddButton("Load Cloth", datCallback( MFA(environmentCloth::LoadAll), this ) );	
	ragGroup->AddButton("Save Cloth", datCallback( MFA(environmentCloth::SaveAll), this ) );
	ragGroup->AddButton("Load Verts", datCallback( MFA(environmentCloth::LoadVerts), this ) );
	ragGroup->AddButton("Save Verts", datCallback( MFA(environmentCloth::SaveVerts), this ) );
	ragGroup->AddButton("Load User Data", datCallback( MFA(environmentCloth::LoadUserData), this ) );
	ragGroup->AddButton("Save User Data", datCallback( MFA1(environmentCloth::SaveUserData), this, (CallbackData)phClothData::sm_tuneFilePath, false ) );

	ragGroup->AddButton("Save empty tuning", datCallback( MFA(environmentCloth::SaveEmptyTuning), this ) );

	if( m_Tuning )
	{
		m_Tuning->AddWidgets(ragGroup);
	}
}
#endif // __BANK

#endif // !__SPU

// void environmentCloth::AddInstance( environmentCloth* /*inst*/ )
// {
// 	if( !GetIsDrawableOwned() )
// 	{
// 		clothDebugf1( "Added cloth instance of %s.", GetClothController()->GetName() );
// 	}
// }
// 
// void environmentCloth::RemoveInstance( environmentCloth* /*inst*/ )
// {
// 	if( !GetIsDrawableOwned() )
// 	{
// 		clothDebugf1( "Removed cloth instance of %s.", GetClothController()->GetName() );
// 	}
// }

void environmentCloth::SwitchToDrawable( void* index )
{
	Assert( index );
	const int newIndex = *(int*)index;
	clothDebugf1("SwitchToDrawable %d: %s", newIndex, GetClothController()->GetName());
	GetDrawable()->GetLodGroup().SetLodIndex( newIndex );
	SwitchToLOD(index);
}

void environmentCloth::SwitchToLOD( void* index )
{
	Assert( index );
	const int newLodIndex = *(int*)index;
	const int oldLodIndex = GetClothController()->GetLOD();

	clothDebugf1("SwitchToLOD from %d to %d: %s", oldLodIndex, newLodIndex, GetClothController()->GetName());

	rmcLodGroup* lodGroup = &GetDrawable()->GetLodGroup();
	Assert( lodGroup );
	if(lodGroup)
	{
		const int currentDrawableLodIndex = lodGroup->GetCurrentLOD();

		if( oldLodIndex < newLodIndex )
			SwitchLOD( newLodIndex, oldLodIndex, oldLodIndex, false );
		else		
			SwitchLOD( newLodIndex, oldLodIndex, newLodIndex, true );

		SetFlag( CLOTH_IsMorph, ( currentDrawableLodIndex == newLodIndex) ? false: true );
	}


#if (__BANK && !__RESOURCECOMPILER)
	g_ClothManagerCallback.Call( this->GetClothController() );
#endif
}

#if CLOTH_INSTANCE_FROM_DATA

void environmentCloth::InstanceFromData(int vertsCapacity, int edgesCapacity)
{
// TODO:
// - no tuning
// - no user data
	SetFlag( CLOTH_IsTuningOwned, false );
#if CLOTH_SIM_MESH_ONLY
	SetFlag( CLOTH_IsSimMeshOnly, true );
#endif
	m_ClothController = rage_aligned_new(16) clothController();

	/*phMorphController* pMorphController = */m_ClothController->CreateMorphController();
	/*Assert(pMorphController);*/
	clothBridgeSimGfx* pClothBridge = m_ClothController->CreateBridge();
	Assert( pClothBridge );
	pClothBridge->InstanceFromData(vertsCapacity);

	m_ClothController->SetLOD( 0 );
	m_ClothController->SetOwner( this );
	m_ClothController->SetFlag(clothController::CLOTH_IsMorphControllerOwned,true);
	m_ClothController->SetFlag(clothController::CLOTH_IsBridgeOwned,true);

	m_ClothController->InstanceFromData(vertsCapacity, edgesCapacity);	
}
#endif // CLOTH_INSTANCE_FROM_DATA


void environmentCloth::InstanceFromTemplate( const environmentCloth* copyme )
{
	Assert( copyme );

	m_Tuning = const_cast<clothInstanceTuning*>(copyme->m_Tuning.ptr);
	if( m_Tuning )
	{
		SAFE_ADD_KNOWN_REF(m_Tuning);
	}

 	if( copyme->m_UserData.GetCount() )
 	{
 		m_UserData.Resize( copyme->m_UserData.GetCount() );
 		m_UserData = copyme->m_UserData;
 	}

	m_Flags = copyme->m_Flags;
	SetFlag( CLOTH_IsTuningOwned, false );

	m_ClothController = rage_aligned_new(16) clothController();
	m_ClothController->InstanceFromTemplate( copyme->GetClothController() );
	m_ClothController->SetOwner( this );
}

void environmentCloth::AllocateDamageArray()
{
	Assert( m_Behavior );
	if( m_Behavior->IsMotionSeparated() )
	{
		phVerletCloth* pCloth = GetCloth();
		Assert( pCloth );
#if NO_PIN_VERTS_IN_VERLET
		const int pinCount = pCloth->GetClothData().GetNumPinVerts();
#else
		const int pinCount = pCloth->GetPinCount();
#endif
		Assert( pinCount );
		pCloth->GetClothData().ResizePrevPositionsArray(pinCount);
	}
}

void environmentCloth::Shutdown()
{
	clothDebugf1("Shutdown environmentCloth 0x%p", this );

	if( GetFlag(CLOTH_IsTuningOwned) )
	{
		clothDebugf1("Shutdown environmentCloth type (m_Tuning): %s", m_ClothController->GetName() );

		Assert( m_Tuning.ptr );
		delete m_Tuning;
	}
	else
	{
		if( m_Tuning.ptr )
		{
			clothDebugf1("Shutdown environmentCloth instance (m_Tuning): %s", m_ClothController->GetName() );

			SAFE_REMOVE_KNOWN_REF(m_Tuning);
			m_Tuning.ptr = NULL;
		}
	}
}

#if !USE_CLOTH_PHINST
void environmentCloth::DeletePhInst( phInst* pphInst )
{
	Assert(pphInst);
	
	clothDebugf1("Delete Archetype, phInst and Bound on environmentCloth 0x%p", this );

	phArchetypePhys* pClothArchetype = (phArchetypePhys*)pphInst->GetArchetype();
	Assert( pClothArchetype );				// if phinst exists then must have archetype ( and is owned ) so just delete it below
	phBound* pBound = pClothArchetype->GetBound();
	Assert( pBound );		

	if (phConfig::IsRefCountingEnabled()) 
	{		
#if !__RESOURCECOMPILER
		Assert( pClothArchetype->GetRefCount() == 1 );
#endif
	}
	delete pphInst;	// NOTE: phInsts will delete their phArchetype after setting refcount to 0

	// Bound has to be last because archetype holds ref count to it
	if (phConfig::IsRefCountingEnabled()) 
	{
#if !__RESOURCECOMPILER
		Assert( pBound->GetRefCount() == 1 );
#endif
		pBound->Release();
	}
	else
	{
		delete pBound;
	}
}
#endif // USE_CLOTH_PHINST


environmentCloth::~environmentCloth()
{
	Shutdown();
	clothDebugf1("Delete environmentCloth 0x%p: %s", this, m_ClothController->GetName() );

	if (GetIsDrawableOwned())
	{
		Assert( m_ReferencedDrawable.ptr );
		delete m_ReferencedDrawable;
	}

	if (m_Behavior)
	{
		delete m_Behavior;
	}

	Assert( m_ClothController.ptr );
	delete m_ClothController;		

#if USE_CLOTH_PHINST

// TODO: once phinst is removed from the env cloth type this whole thing will go away, here to please old assets for a week
	if (m_cthInst)
	{
		clothDebugf1("Delete Archetype, phInst and Bound on environmentCloth 0x%p", this );

		phArchetypePhys* pClothArchetype = (phArchetypePhys*)m_cthInst->GetArchetype();
		Assert( pClothArchetype );				// if phinst exists then must have archetype ( and is owned ) so just delete it below
		phBound* pBound = pClothArchetype->GetBound();
		Assert( pBound );		

// TODO: old assets hack for a week		
		if (phConfig::IsRefCountingEnabled()) 
		{
			const int refCount = pClothArchetype->GetRefCount();
			Assert( refCount == 2 || refCount == 1 );
			if( refCount == 2 )
			{
				pClothArchetype->Release();
			}
#if !__RESOURCECOMPILER
			Assert( pClothArchetype->GetRefCount() == 1 );
#endif
		}
		delete m_cthInst;	// NOTE: and the archetype gets deleted from withing phinst destructor

// Bound has to be last because archetype holds ref count to it
		if (phConfig::IsRefCountingEnabled()) 
		{
#if !__RESOURCECOMPILER
			Assert( pBound->GetRefCount() == 1 );
#endif
			pBound->Release();
		}
		else
		{
			delete pBound;
		}
	}

#endif // USE_CLOTH_PHINST

}

#if __DECLARESTRUCT
void environmentCloth::DeclareStruct(datTypeStruct &s)
{
	m_AttachedToFrame = NULL;

	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE( environmentCloth, pgBase )
		SSTRUCT_FIELD( environmentCloth, m_Tuning)
		SSTRUCT_FIELD( environmentCloth, m_ReferencedDrawable)
		SSTRUCT_FIELD( environmentCloth, m_Behavior)
		SSTRUCT_FIELD( environmentCloth, m_ClothController)
#if USE_CLOTH_PHINST
		SSTRUCT_FIELD( environmentCloth, m_cthInst)
		SSTRUCT_FIELD( environmentCloth, m_BoundIdx)
#else
		SSTRUCT_CONTAINED_ARRAY( environmentCloth, m_Padding0 )
#endif
		
#if __64BIT
		SSTRUCT_CONTAINED_ARRAY( environmentCloth, m_Padding )
#endif
		SSTRUCT_FIELD( environmentCloth, m_InitialPosition)
		SSTRUCT_FIELD( environmentCloth, m_Force)		
		SSTRUCT_FIELD( environmentCloth, m_UserData )
		SSTRUCT_IGNORE( environmentCloth, m_AttachedToFrame)
		SSTRUCT_FIELD( environmentCloth, m_Flags)
	SSTRUCT_END( environmentCloth)
}
#endif


#if MESH_LIBRARY

void environmentCloth::LoadCloth( rmcTypeFileCbData* data )
{
	RAGE_TRACK(EnvCloth);

	fiTokenizer* tokenizer = data->m_T;
	tokenizer->CheckToken("{");
	tokenizer->CheckToken("mesh");

	if( tokenizer->CheckToken("{") )
	{
		Vector3 extrLocalOffset;
		if( tokenizer->CheckToken("offset") )
			tokenizer->GetVector(extrLocalOffset);
		else
			extrLocalOffset.Zero();

		m_ClothController = rage_aligned_new(16) clothController;
		phMorphController* morphController = m_ClothController->CreateMorphController();
		Assert(morphController);
		m_ClothController->CreateBridge();
		m_ClothController->SetLOD( 0 );		

		sysMemStartTemp();
		phClothConnectivityData* connectivityHigh = rage_aligned_new(16) phClothConnectivityData;
		phClothConnectivityData* connectivityMed = rage_aligned_new(16) phClothConnectivityData;
		phClothConnectivityData* connectivityLow = rage_aligned_new(16) phClothConnectivityData;
		phClothConnectivityData* connectivityVLow = rage_aligned_new(16) phClothConnectivityData;
		sysMemEndTemp();

		int existingLODs[LOD_COUNT];
		int existingLODsCount = 0;

		// TODO: still in experimental stage .. but should work just fine though
		int extraVerts = 0;
  #if USE_EXTRA_VERTS
		if(	   GetDrawable() 
			&& GetDrawable()->GetLodGroup().ContainsLod(LOD_HIGH)
			&& GetDrawable()->GetLodGroup().GetLod(LOD_HIGH).GetModel(0) )
			extraVerts = GetDrawable()->GetLodGroup().GetLod(LOD_HIGH).GetModel(0)->GetGeometry(0).GetExtraVertsCount();
  #endif

		if( LoadLod( *tokenizer, "high",	LOD_HIGH,	0xffffffff, VECTOR3_TO_VEC3V(extrLocalOffset), connectivityHigh, extraVerts ) )
			existingLODs[ existingLODsCount++ ] = 0;

		int extraVertsPerLOD = 0;

		if( LoadLod( *tokenizer, "med",		LOD_MED,	0xffffffff, VECTOR3_TO_VEC3V(extrLocalOffset), connectivityMed, extraVertsPerLOD ) )
			existingLODs[ existingLODsCount++ ] = 1;

		if( LoadLod( *tokenizer, "low",		LOD_LOW,	0xffffffff, VECTOR3_TO_VEC3V(extrLocalOffset), connectivityLow, extraVertsPerLOD ) )
			existingLODs[ existingLODsCount++ ] = 2;

		if( LoadLod( *tokenizer, "vlow",	LOD_VLOW,	0xffffffff, VECTOR3_TO_VEC3V(extrLocalOffset), connectivityVLow, extraVertsPerLOD ) )
			existingLODs[ existingLODsCount++ ] = 3;

		int k = 0;
		int i = k;
		while( i < (existingLODsCount-1) )
		{
			const int index0 = existingLODs[k];
			const int index1 = existingLODs[i+1];

			int numVerts0 = m_ClothController->GetCloth(index0)->GetNumVertices();
			int numVerts1 = m_ClothController->GetCloth(index1)->GetNumVertices();

			morphController->CreateDeformMapping(
				numVerts0
				, (Vec3V*)m_ClothController->GetCloth(index0)->GetClothData().GetVertexPointer()
				, (Vec3V*)m_ClothController->GetCloth(index1)->GetClothData().GetVertexPointer()
				, index0, index1, phMapData::enWEIGHT_ONE
				);

#if NO_PIN_VERTS_IN_VERLET
			const int numPinnedVerts = m_ClothController->GetCloth(index0)->GetClothData().GetNumPinVerts();
#else
			const int numPinnedVerts = m_ClothController->GetCloth(index0)->GetPinCount();
#endif
			const u16* clothDisplayMap = m_ClothController->GetBridge()->GetClothDisplayMap( index0 );
			Assert( clothDisplayMap );
			for( int h = 0; h < numPinnedVerts; ++h )
			{
				morphController->SwapMorphData( h, clothDisplayMap[h], index0, index1, phMapData::enWEIGHT_ONE );
			}

			morphController->CreateDirectMapping(
				  numVerts1
				, numVerts0
				, (Vec3V*)m_ClothController->GetCloth(index1)->GetClothData().GetVertexPointer()
				, (Vec3V*)m_ClothController->GetCloth(index0)->GetClothData().GetVertexPointer()
				, index0, index1
				, m_ClothController->GetBridge()->GetClothDisplayMap(index0)
				);

			++i;
			if( i == (existingLODsCount-1) && k < (existingLODsCount-1) )
			{
				i = ++k;
			}
		}

		// Note: the following  sysMemStartTemp, sysMemEndTemp should match in void phMorphController::CreateIndexMap
		sysMemStartTemp();
		for( int i = 0; i < LOD_COUNT; ++i )
		{
			if( morphController->m_MapData[i] )
			{
				morphController->m_MapData[i]->m_IMap[i].m_Data.Reset();
			}
		}
		delete connectivityHigh;
		delete connectivityMed;
		delete connectivityLow;
		delete connectivityVLow;
		sysMemEndTemp();

		Vector3 cullSphere, boxMin, boxMax;
		if( tokenizer->CheckToken("center") )
			tokenizer->GetVector( cullSphere);
		else
			cullSphere.Zero();

		/*float cullRadius =*/ tokenizer->MatchFloat("radius");

		if( tokenizer->CheckToken("box") )
		{
			tokenizer->GetVector( boxMin );
			tokenizer->GetVector( boxMax );
		}

		const char* pClothControllerName = m_ClothController->GetName();		
		Assert( pClothControllerName );
		if( phVerletCloth::sm_tuneFilePath )
		{
			char buff[256];
			ASSET.PushFolder( phVerletCloth::sm_tuneFilePath );
			
			formatf( buff, "%s%s", pClothControllerName, environmentCloth::sm_MetaDataFileAppendix );
			if( ASSET.Exists(buff, "xml") )
			{
				LoadUserData( "" );
			}

			formatf( buff, "%s%s", pClothControllerName, clothInstanceTuning::sm_MetaDataFileAppendix );
			if( ASSET.Exists(buff, "xml") )
			{
				m_Tuning = rage_aligned_new(16) clothInstanceTuning();
				Assert( m_Tuning );
				m_Tuning->Load( "", pClothControllerName );
				SetFlag( CLOTH_IsTuningOwned, true );
				SetFlag( CLOTH_IsInInterior, m_Tuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_IS_IN_INTERIOR) );

				if( m_Tuning->m_Weight > 0.0f )
				{
					for( int i = 0; i < LOD_COUNT; ++i )
					{
						phVerletCloth* pCloth = GetCloth(i);
						if( pCloth )
						{
							pCloth->m_ClothWeight = m_Tuning->m_Weight;
						}
					}
				}

				if( m_Tuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_FORCE_VERTEX_RESISTANCE) )
				{
					// TODO: fix it on RDR3 so force vertex resistance has its own member
					Assert( !m_Tuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_USE_DISTANCE_THRESHOLD) );
					float fVertexResistance = m_Tuning->m_DistanceThreshold;

					clothBridgeSimGfx* pClothBridge = m_ClothController->GetBridge();
					Assert( pClothBridge );
					for( int i = 0; i < LOD_COUNT; ++i )
					{
						int vertWeightsCount = pClothBridge->GetVertexWeightsCount(i);
						if( vertWeightsCount )
						{
							float* pVertsWeights = const_cast<float*>( pClothBridge->GetVertexWeights(i) );
							Assert( pVertsWeights );
							for( int k = 0; k < vertWeightsCount; ++k )
							{
								pVertsWeights[k] = fVertexResistance;
							}								
						}
					}
					
				}

			}

			formatf( buff, "%s%s", pClothControllerName, phClothData::sm_MetaDataFileAppendix );
			if( ASSET.Exists(buff, "xml") )
			{
				phVerletCloth* pCloth = GetCloth(0);
				if( pCloth )
				{
					pCloth->GetClothData().Load( "", pClothControllerName );
				}
			}

  #if __RESOURCECOMPILER
			Assert( g_CustomBoundsCB );
			g_CustomBoundsCB( this, "environmentcloth", "_custombounds", pClothControllerName, false );
  #endif // __RESOURCECOMPILER

			ASSET.PopFolder();
		}

  #if __RESOURCECOMPILER
		Assert( g_CustomBoundsCB );
		g_CustomBoundsCB( this, "clothCollision", "", pClothControllerName, true );
  #endif // __RESOURCECOMPILER

	}
	else
	{
		Quitf("OLD CLOTH FORMAT IS NOT SUPPORTED. PLEASE RE-EXPORT THE CLOTH PIECE !");
	}
}


bool environmentCloth::LoadLod( fiTokenizer &t, const char *match, int lod, int lodMask, Vec3V_In extraOffsetV, phClothConnectivityData* connectivity, int extraVerts )
{
	bool result = false;
	// Are we suppose to load this LOD?
	if (lodMask & (1 << lod))
	{
		// Yeah, load it here.
		char buf[128];
		t.MatchToken(match); 
		if (!t.CheckToken("none")) 
		{
			t.GetToken(buf, sizeof(buf));		// support old format i guess
			t.GetToken(buf, sizeof(buf));

			sysMemStartTemp();
			mshMesh* mesh = rage_aligned_new(16) mshMesh;
			SerializeFromFile(buf, *mesh, "mesh");
			sysMemEndTemp();

			// TODO: add checks to make sure only one material exists
			Assert( mesh );

			// NOTE: material index hardcoded to 0 !
			const int mtlIdx = 0;		
			Assert( mtlIdx <= mesh->GetMtlCount() );
			mshMaterial* mtl = &mesh->GetMtl(mtlIdx);

			/*int nModel =*/ t.GetInt();
			m_ClothController->Load( extraVerts, mtl, extraOffsetV, lod, buf, connectivity);

#if 0 && __DEV
			Assert(!m_ClothController->CheckForBadPolygons(lod, GetDrawable() ));
#endif

			sysMemStartTemp();
			delete mesh;
			sysMemEndTemp();

			result = true;
		}

		float switchDistanceDown = t.GetFloat();
		float switchDistanceUp = t.GetFloat();

		Assert( switchDistanceDown < switchDistanceUp );
		if( switchDistanceDown > switchDistanceUp )
		{
			Quitf("Lod switch distances for environment cloth are not setup correctly, game will crash with these assets. Art need to fix those.");
		}

		m_ClothController->SetSwitchDistanceDown( lod, switchDistanceDown );
		m_ClothController->SetSwitchDistanceUp( lod, switchDistanceUp );
	}
	else
	{
		AssertMsg(0, "We shouldn't be here ! Wrong LOD index processed ?!" );
	}

	return result;
}


#endif	// MESH_LIBRARY

extern bool g_PauseClothSimulation;

void environmentCloth::ApplyAirResistance(Vec3V* RESTRICT pNormals, const Vector3& vAirSpeed)
{
	if(g_PauseClothSimulation)
		return;

	PF_FUNC(ApplyAirResistance);

	Assert( m_AttachedToFrame );
	Assert( m_ClothController );
	Assert( pNormals );
	
	const int currentLOD = GetLOD();
	Assert( currentLOD < LOD_VLOW );
	phVerletCloth* pCloth = GetCloth(currentLOD);
	Assert( pCloth );

// 	Vec3V* RESTRICT pNormals = Alloca(Vec3V, cloth->GetNumVertices());
// 	Assert( pNormals );
// 	m_ClothController->GetVertexNormalsFromDrawable( (Vector3*)pNormals, GetDrawable() );

	clothBridgeSimGfx* clothBridge = m_ClothController->GetBridge();
	Assert( clothBridge );


	const float* RESTRICT vertWeights = clothBridge->GetVertexWeights( currentLOD );
	Assert( vertWeights );
	const float* RESTRICT inflationScale = clothBridge->GetInflationScale( currentLOD );	
	Assert( inflationScale );
	if( m_UserData.GetCount() )
	{
// TODO: allocate matrices with Alloca?
		Mat34V tMat[3] = { Mat34V(V_IDENTITY), Mat34V(V_IDENTITY), Mat34V(V_IDENTITY), };
		
		Vec3V atVec = Negate(Vec3V(m_AttachedToFrame->b.xyzw));

		Vec3V vForce = Normalize( VECTOR3_TO_VEC3V(vAirSpeed) );
		Vec3V vUp	 = Normalize( Cross( vForce, atVec ) );
		Vec3V vRight = Normalize( Cross( vForce, vUp) );

		tMat[1].SetCol2( vUp );
		tMat[1].SetCol1( vForce );
		tMat[1].SetCol0( vRight );

		tMat[2].SetCol2( Negate(vUp) );
		tMat[2].SetCol1( vForce );
		tMat[2].SetCol0( vRight );
//	
		pCloth->ApplyAirResistanceTransform( m_UserData.GetElements(), tMat, inflationScale, vertWeights, pNormals, 1, vAirSpeed );
	}
	else
	{
		pCloth->ApplyAirResistance( inflationScale, vertWeights, pNormals, 1, vAirSpeed );
	}
}


void environmentCloth::ApplyAirResistanceRope(const Vector3& vAirSpeed, float fDragCoeff /* = 0.1f */)
{
	if(g_PauseClothSimulation)
		return;

	GetCloth()->ApplyAirResistanceRope( fDragCoeff, vAirSpeed );
}


void environmentCloth::AllocateClothController ()
{
	Assert(!m_ClothController);
	m_ClothController = rage_aligned_new(16) clothController;
	m_ClothController->AllocateCloth();
	m_ClothController->CreateBridge();
}

void environmentCloth::SetReferencedDrawable( rmcDrawable *drawit, bool isDrawableOwned )
{
	m_ReferencedDrawable = drawit;
	SetIsDrawableOwned(isDrawableOwned);
}

bool environmentCloth::GetIsVisible() 
{ 
	bool temp = (m_Flags & CLOTH_IsVisible) ? true: false; 
#if CLOTH_SIM_MESH_ONLY
	if( !GetFlag(CLOTH_IsSimMeshOnly) )
#endif
	{
		SetFlag(CLOTH_IsVisible,false); 
	}
	return temp; 
}

void environmentCloth::SetIsDrawableOwned(bool isDrawableOwned)
{
	if( isDrawableOwned )
		m_Flags |= CLOTH_IsDrawableOwned;
	else
		m_Flags &= (~CLOTH_IsDrawableOwned);
}

void environmentCloth::CleanUpDrawable()
{
	if( GetIsDrawableOwned() )
	{
#if RSG_PC
		// DO NOT destroy the vertex buffer if we're resourcing this object here.
		if (!sysMemAllocator::GetCurrent().IsBuildingResource())
		{
			for(int lod = 0; lod < LOD_COUNT; ++lod)
			{
				if( m_ReferencedDrawable->GetLodGroup().ContainsLod(lod))
				{
					rmcLod& ld =  m_ReferencedDrawable->GetLodGroup().GetLod(lod);
					grmModel* md = ld.GetModel( 0 /*GetClothController()->GetModelIdx()*/);
					if(md)
					{
						int nGeom = md->GetGeometryCount();
						for(int geometry=0; geometry<nGeom; ++geometry)
						{
							grmGeometry& geo =  md->GetGeometry( 0 /*GetClothController()->GetGeometryIdx()*/);
							geo.DestroyVertexBuffersHackForFragmentCloth();
						}
					}
				}
			}
		}
#endif

	}
}

#if USE_CLOTH_PHINST
grcCullStatus environmentCloth::IsVisible(const grcViewport &vp,u8 &lod, float* retZDist) const
{
	float zDist = 0.0;
	float radius = m_cthInst->GetArchetype()->GetBound()->GetRadiusAroundCentroid();
	const Vector3 center = VEC3V_TO_VECTOR3(m_cthInst->GetWorldCentroid());
	grcCullStatus status = vp.IsSphereVisible(center.x,center.y,center.z,radius,&zDist);
	if (GetCloth()->IsRope())
	{
		lod = 0;
	}
	else
	{
		lod = (u8) (status? GetDrawable()->GetLodGroup().ComputeLod(zDist - GetDrawable()->GetLodGroup().GetCullRadius()) : LOD_VLOW);
	}

	if (retZDist)
	{
		*retZDist = Max(0.0f,zDist-radius);
	}

	return status;
}
#endif // USE_CLOTH_PHINST

phInst* environmentCloth::CreateInstance(phBound* pClothBound)
{
	phArchetypePhys* pClothArchetype = rage_aligned_new(16) phArchetypePhys;
	Assert( pClothArchetype );
	pClothArchetype->SetBound(pClothBound);

	phInst* pPhInst = rage_aligned_new(16) phInst;
	Assert( pPhInst );
	pPhInst->SetMatrix(Mat34V(V_IDENTITY));
	pPhInst->SetArchetype(pClothArchetype);

#if USE_CLOTH_PHINST
	m_cthInst = pPhInst;
#endif
	m_Behavior->SetInstance( *pPhInst );

	phVerletCloth* pCloth = GetCloth();
	Assert( pCloth );
	if (pCloth->GetFlag(phVerletCloth::FLAG_IS_ROPE))
	{
		m_Behavior->UpdateRopeBound(pCloth);
#if !NO_CLOTH_BOUND_CLAMP
		phBoundComposite* pCompositeBound = static_cast<phBoundComposite*>(pClothBound);
		Assert( pCompositeBound );
		pCloth->ClampCompositeExtents(*pCompositeBound,MAX_CLOTH_RADIUS);
#endif
	}

	return pPhInst;
}

#if USE_CLOTH_PHINST
void environmentCloth::ClearBound()
{
	Assert( m_cthInst.ptr );
	phArchetype* pArchetype = m_cthInst->GetArchetype();
	Assert( pArchetype );
	phBoundComposite* pCompositeBound = (phBoundComposite*)pArchetype->GetBound();
	Assert( pCompositeBound );
	Assert( pCompositeBound->GetType() == phBound::COMPOSITE );

	Assert( m_BoundIdx > -1 );
	Assert( pCompositeBound->GetBound(m_BoundIdx) && pCompositeBound->GetBound(m_BoundIdx)->GetRefCount() == 1 );
	pCompositeBound->SetBound( m_BoundIdx, NULL );
}
#endif // USE_CLOTH_PHINST

void environmentCloth::SwitchLOD( int newLodIndex, int oldLodIndex, int mapIndex, bool shouldMorph )
{	
	m_ClothController->SwitchLOD( newLodIndex, oldLodIndex, mapIndex, shouldMorph );
	m_ClothController->SetLOD( (s8)newLodIndex );
	phVerletCloth* pCloth = m_ClothController->GetCloth( newLodIndex );
	Assert( pCloth );
	pCloth->ComputeClothBoundingVolume();
}


void environmentCloth::CheckDistance( Vec3V_In pointOfInterest )			// point of interest
{
	// This function should be called only for static env cloth ... i.e cloth not attached to vehicles

	PF_FUNC(EnvClothCheckDistance);

	Assert( m_ClothController );

	const int currentLOD = GetLOD();

	Vec3V clothPos = m_Behavior->IsMotionSeparated() ? VECTOR3_TO_VEC3V(m_AttachedToFrame->d) : m_InitialPosition;
	const Vec3V vecPOI = Subtract( clothPos, pointOfInterest );
	const ScalarV distSQR = Dot( vecPOI, vecPOI );
	const phVerletCloth* cloth = 0;
	int i = 0;
	while( (cloth = GetCloth(i)) != 0 )
	{			
		if( i < currentLOD )
		{
			const float dist = cloth->GetClothData().GetSwitchDistanceUp();
			if( distSQR.Getf() < (dist * dist) ) 
			{
				SwitchToLOD( (void*)&(i) );
			}
		}
		else if ( i > currentLOD )
		{
			const float dist = cloth->GetClothData().GetSwitchDistanceDown();
			if( distSQR.Getf() > (dist * dist) ) 
			{
				SwitchToLOD( (void*)&(i) );
			}
		}

		i++;
		Assert( i < LOD_COUNT );
	}
}

phEnvClothVerletBehavior* environmentCloth::CreateBehavior()
{
	Assert( !m_Behavior );
	m_Behavior = rage_aligned_new(16) phEnvClothVerletBehavior(this);
	return m_Behavior;
}

phInst* environmentCloth::CreatePhysics( phInst* pPhInst, float ropeRadius )
{ 
	Assert( !m_Behavior );
	m_Behavior = rage_aligned_new(16) phEnvClothVerletBehavior(this);

// NOTE: when creating cloth bound use the highest LOD - 0
	const int lodIndex = 0;				// m_ClothController->GetLOD();
	phVerletCloth* pCloth = m_ClothController->GetCloth(lodIndex);
	Assert( pCloth );

	// set up a bounding sphere collision object to encompass our cloth
	phBound* pClothBound = NULL;
#if USE_CLOTH_PHINST
	Vector3 position;
	float radius;
	pCloth->ComputeBoundingVolume(pClothBound,&position,&radius);
#endif

	if (pCloth->GetFlag(phVerletCloth::FLAG_IS_ROPE))
	{		
		Assert( ropeRadius > 0.0f );
		ropeRadius = Selectf( -ropeRadius, DEFAULT_ROPE_RADIUS, ropeRadius );
		pClothBound = &pCloth->CreateRopeBound(ScalarVFromF32(ropeRadius));
	}
#if USE_CLOTH_PHINST
	else
	{
		pClothBound = rage_aligned_new(16) phBoundSphere(radius);
	}
#endif

	if (pPhInst)
	{				
		Assert( !pCloth->GetFlag(phVerletCloth::FLAG_IS_ROPE) );
		m_InitialPosition = pPhInst->GetPosition();		

#if USE_CLOTH_PHINST
		Assert( pClothBound );
		phArchetypePhys* pClothArchetype = static_cast<phArchetypePhys*>(pPhInst->GetArchetype());
		phBound* pBound = pClothArchetype->GetBound();
		Assert(pBound->GetType()==phBound::COMPOSITE);
		phBoundComposite* pCompositeBound = static_cast<phBoundComposite*>(pBound);
		int compositePartIndex = pCompositeBound->GetNumActiveBounds();
		pCompositeBound->SetBound(compositePartIndex,pClothBound);
		Assert( pClothBound->GetRefCount() == 2 );
		pClothBound->Release();

		Matrix34 clothPartPose(M34_IDENTITY);
		if (compositePartIndex>0)
		{
			Vector3 localPosition;
			MAT34V_TO_MATRIX34(pPhInst->GetMatrix()).UnTransform(position,localPosition);
			clothPartPose.d.Set(localPosition);
		}
		pCompositeBound->SetCurrentMatrix(compositePartIndex,RCC_MAT34V(clothPartPose));

		// NOTE: if the first bound is not sphere, then we have composite object ... cloth and non-cloth bounds ... so just "enable" the cloth bound too
		if( setExtraBound && pCompositeBound->GetBound(0) && pCompositeBound->GetBound(0)->GetType() != phBound::SPHERE )
		{
			Assert( pCompositeBound->GetMaxNumBounds() >= compositePartIndex+1 );
			pCompositeBound->SetNumBounds( compositePartIndex+1 );
		}

		pCompositeBound->CalculateCompositeExtents();
		if(pPhInst->IsInLevel())
		{
			PHLEVEL->RebuildCompositeBvh(pPhInst->GetLevelIndex());
		}
		else
		{
			pCompositeBound->UpdateBvh(true);
		}
#if !NO_CLOTH_BOUND_CLAMP
		phVerletCloth::ClampCompositeExtents(*pCompositeBound,MAX_CLOTH_RADIUS);
#endif
		// Make sure the composite bound has per-part type and include flags.
		if (!pCompositeBound->GetTypeAndIncludeFlags())
		{
			pCompositeBound->AllocateTypeAndIncludeFlags();
		}

		// Zero the cloth part's include flag, so that the rest of the object can do collisions without the cloth part.
		pCompositeBound->SetIncludeFlags(compositePartIndex,0);
		Assert( m_BoundIdx == -1 );
		m_BoundIdx = compositePartIndex;

		m_cthInst = pPhInst;
#endif // USE_CLOTH_PHINST
	}
	else
	{	
		pPhInst = CreateInstance(pClothBound);
	}

	if (pPhInst->IsInLevel())
	{
		int levelIndex = pPhInst->GetLevelIndex();
		PHLEVEL->UpdateObjectLocationAndRadius(levelIndex,(Mat34V_Ptr)(NULL));
		PHLEVEL->SetInactiveCollidesAgainstFixed(levelIndex,true);
		PHLEVEL->SetInactiveCollidesAgainstInactive(levelIndex,true);
	}

	if (!((const Matrix34*)&pPhInst->GetMatrix())->IsOrthonormal())
	{
		// Set the cloth instance matrix, to avoid crashing in sample_wilderness when cloth is added to the world before it is positioned.
		pPhInst->SetMatrix(Mat34V(V_IDENTITY));
	}

	// hook the instance behavior with the cloth instance
	m_Behavior->SetInstance(*pPhInst);

	return pPhInst;
}


// phBoundComposite& environmentCloth::CreateRopeBound (int numEdges, float ropeLength, float startRadius, float endRadius)
// {
// 	return GetCloth()->CreateRopeBound(numEdges,ropeLength,startRadius,endRadius);
// }
// 
// 
// phBoundComposite& environmentCloth::CreateRopeBound (float ropeRadius)
// {
// 	return GetCloth()->CreateRopeBound( Max(ropeRadius,MIN_ROPE_RADIUS) );
// }

Vec3V_Out environmentCloth::GetBBMinWorldSpace()
{
	phVerletCloth* pCloth = GetCloth();
	Assert( pCloth );
	return Add( pCloth->GetBBMin(), GetOffset() );
}

Vec3V_Out environmentCloth::GetBBMaxWorldSpace()
{
	phVerletCloth* pCloth = GetCloth();
	Assert( pCloth );
	return Add( pCloth->GetBBMax(), GetOffset() );
}

Vec3V_Out environmentCloth::GetOffset()
{
	Assert( m_Behavior );

	if( !m_Behavior->IsMotionSeparated() )
	{
		// GTAV - HACK - B*2902855 - Stunt - no flags are slowing up in the races
		// The flags have no tuning data but when placed in the creator their initial position isn't set every time they move so if they translate is far from the 
		// initial position and the initial position is at the origin then it is probably wrong and set the motion seperated / activate on hit flag
		if( !m_Tuning &&
			GetIsAttachedToFrame() )
		{
			if( MagSquared( GetInitialPosition() ).Getf() < 10.0f &&
				MagSquared( GetAttachedToFrame().GetCol3() - GetInitialPosition() ).Getf() > 100.0f )
			{
				m_Behavior->SetActivateOnHit( true );
				m_Behavior->SetActivateOnHitOverridden( true );

				Mat34V frame = GetAttachedToFrame();
				return frame.GetCol3();
			}
		}

		return GetInitialPosition();
	}
	else
	{
		Mat34V frame = GetAttachedToFrame();
		return frame.GetCol3();
	}
}

void environmentCloth::UpdateCloth (float timeStep, float timeScale)
{
	PF_FUNC(EnvClothSimUpdate);

	Mat34V frame = GetAttachedToFrame();
	Vec3V vDebugOffset = frame.GetCol3();
	frame.SetCol3( Vec3V(V_ZERO) );
	Vec3V translateV(V_ZERO);

	const int simLodIndex = GetLOD();	
	Assert( simLodIndex > -1 && simLodIndex < LOD_COUNT );
	phVerletCloth* pCloth = GetCloth(simLodIndex);
	Assert(pCloth);

	m_Behavior->UpdateClothSim( timeScale, timeStep, frame );
	

#if CLOTH_SIM_MESH_ONLY
	if( !GetFlag(CLOTH_IsSimMeshOnly) )
#endif
	{
		const grmModel* pModel = GetDrawable()->GetLodGroup().GetLod( 0 /*renderableLodIndex*/ ).GetModel( 0 /*GetModelIdx()*/);
		Assert(pModel);
		m_ClothController->ApplySimulationToMesh( (grmGeometryQB&)pModel->GetGeometry( 0 /*GetGeometryIdx()*/), VEC3V_TO_VECTOR3(translateV), 0 /*renderableLodIndex*/, vDebugOffset );
	}

#if !__SPU
	Vector3 pos;
	float rad;

 #if NO_BOUND_CENTER_RADIUS
	Vec3V center = pCloth->GetCenter();
	pos = VEC3V_TO_VECTOR3( Add(GetOffset(),center) );
	rad = pCloth->GetRadius(center);
 #else
	pCloth->GetBoundingSphere( pos, rad );
 #endif

 #if CLOTH_SIM_MESH_ONLY
	if( !GetFlag(CLOTH_IsSimMeshOnly) )
 #endif
	{		
		GetDrawable()->GetLodGroup().SetCullSphere( pos +  VEC3V_TO_VECTOR3(translateV) );
		GetDrawable()->GetLodGroup().SetCullRadius( rad );
	} 
#endif // !__SPU

	pCloth->ResetCollisionInst();
}


void environmentCloth::UpdateAgainstCollisionInsts(bool updateAgainstCollisionInsts, bool clearCollisionInsts)
{
	PF_FUNC(EnvClothSimUpdate);

	Mat34V frame = GetAttachedToFrame();
	Vec3V vDebugOffset = frame.GetCol3();
	
	frame.SetCol3( Vec3V(V_ZERO) );
	Vec3V translateV(V_ZERO);

	const int simLodIndex = GetLOD();	
	Assert( simLodIndex > -1 && simLodIndex < LOD_COUNT );
	phVerletCloth* pCloth = GetCloth(simLodIndex);
	Assert(pCloth);

	if(updateAgainstCollisionInsts)
		m_Behavior->UpdateAgainstCollisionInsts(frame);

#if CLOTH_SIM_MESH_ONLY
	if( !GetFlag(CLOTH_IsSimMeshOnly) )
#endif
	{
		const grmModel* pModel = GetDrawable()->GetLodGroup().GetLod( 0 /*renderableLodIndex*/ ).GetModel( 0 /*GetModelIdx()*/);
		Assert(pModel);
		m_ClothController->ApplySimulationToMesh( (grmGeometryQB&)pModel->GetGeometry( 0 /*GetGeometryIdx()*/), VEC3V_TO_VECTOR3(translateV), 0 /*renderableLodIndex*/, vDebugOffset );
	}

#if !__SPU
	Vector3 pos;
	float rad;

#if NO_BOUND_CENTER_RADIUS
	Vec3V center = pCloth->GetCenter();
	pos = VEC3V_TO_VECTOR3( Add(GetOffset(),center) );
	rad = pCloth->GetRadius(center);
#else
	pCloth->GetBoundingSphere( pos, rad );
#endif

#if CLOTH_SIM_MESH_ONLY
	if( !GetFlag(CLOTH_IsSimMeshOnly) )
#endif
	{		
		GetDrawable()->GetLodGroup().SetCullSphere( pos +  VEC3V_TO_VECTOR3(translateV) );
		GetDrawable()->GetLodGroup().SetCullRadius( rad );
	} 
#endif // !__SPU

	if(clearCollisionInsts)
		pCloth->ResetCollisionInst();
}


void environmentCloth::UpdateRope (float timeStep)
{
	PF_FUNC(EnvClothSimUpdate);

	Mat34V frame(V_IDENTITY);
	Vec3V translateV = frame.GetCol3();

	Assert( m_Behavior );
	if( !m_Behavior->IsMotionSeparated() )
	{
		translateV.ZeroComponents();
		frame.SetCol3( GetInitialPosition() );
	}
	else
		frame.SetCol3( Vec3V(V_ZERO) );

	m_Behavior->UpdateRopeSim( timeStep, frame );
}

sysTaskHandle environmentCloth::UpdateRopeSpu (float timeStep, phVerletSPUDebug* /*s_VerletSpuDebug*/)
{
	PF_FUNC(EnvClothSimSpuUpdate);

	Assert( GetCloth()->GetFlag(phVerletCloth::FLAG_IS_ROPE) );

	return m_Behavior->UpdateRopeSimSpu( timeStep );
}


sysTaskHandle environmentCloth::UpdateSpu (float timeStep, phVerletSPUDebug* s_VerletSpuDebug)
{
	PF_FUNC(EnvClothSimSpuUpdate);

	clothController* pClothController = GetClothController();
	Assert( pClothController );

	Assert( !GetCloth()->GetFlag(phVerletCloth::FLAG_IS_ROPE) );

	grmGeometryQB* geom = NULL;
#if CLOTH_SIM_MESH_ONLY
	if( !GetFlag(CLOTH_IsSimMeshOnly) )
#endif
	{
//		const int curDrawableLodIndex = drawable->GetLodGroup().GetLodIndex();
//		Assert( curDrawableLodIndex > -1 && curDrawableLodIndex < 4 );
		rmcDrawable* drawable = GetDrawable();
		Assert( drawable );
		geom = (grmGeometryQB*)&drawable->GetLodGroup().GetLod(0 /*curDrawableLodIndex*/).GetModel( 0 /*controller->GetModelIdx()*/)->GetGeometry( 0 /*controller->GetGeometryIdx()*/);
		Assert( geom );
	}

	Mat34V frame = GetAttachedToFrame();
	if( !m_Behavior->IsMotionSeparated() )
	{
		frame.SetCol3( GetInitialPosition() );	
	}

	return m_Behavior->UpdateClothSimSpu( frame, timeStep, geom, pClothController, m_Force, s_VerletSpuDebug);
}

void environmentCloth::UpdateRopePostSpu()
{
	PF_FUNC(EnvClothPostSpuUpdate);

	Assert( m_Behavior );
	m_Behavior->UpdateVerletBound( GetCloth() );

	m_Behavior->Reset();
	GetCloth()->ResetCollisionInst();
}

void environmentCloth::UpdateClothPostSpu()
{
	PF_FUNC(EnvClothPostSpuUpdate);		

#if CLOTH_SIM_MESH_ONLY
	if( !GetFlag(CLOTH_IsSimMeshOnly) )
#endif
	{
		const rmcDrawable* drawable = GetDrawable();
		Assert( drawable );
	//	const int curDrawableLodIndex = drawable->GetLodGroup().GetLodIndex();
		grmGeometry* geom = &drawable->GetLodGroup().GetLod(0 /*curDrawableLodIndex*/).GetModel( 0 /*controller->GetModelIdx()*/)->GetGeometry( 0 /*controller->GetGeometryIdx()*/);
		geom->GetVertexBuffer()->UnlockRW();
	}

	Assert( m_Behavior );
	const bool bMoved = m_Behavior->UpdateVerletBound( GetCloth() );
	if( bMoved )
	{
		const clothInstanceTuning* pClothTuning = GetTuning();
		if( pClothTuning && pClothTuning->GetFlag(clothInstanceTuning::CLOTH_TUNE_USE_DISTANCE_THRESHOLD) )
		{
			Vec3V tempV = Subtract( GetInitialPosition(), VECTOR3_TO_VEC3V(GetFramePosition()) );
			BoolV overThreshold = IsGreaterThan(Dot(tempV,tempV), ScalarVFromF32(pClothTuning->m_DistanceThreshold));
			if( overThreshold.Getb() )
				SetFlag( CLOTH_IsMoving, true );
		}	
	}
	m_Behavior->Reset();
	GetCloth()->ResetCollisionInst();
}

void environmentCloth::CopyVerletDrawVertsForRope()
{
	const phVerletCloth* pCloth = GetCloth();
	Assert(pCloth);
	const Vector3* RESTRICT src = (const Vector3 *)pCloth->GetClothData().GetVertexPointer();

// TODO: isn't it better/cleaner to call GetVertexCount() ??
	const int vertCount = pCloth->GetNumActiveEdges()+1;

	const int lockedFromFront = pCloth->GetNumLockedEdgesFront();

	rmcRopeDrawable *ropeDrawable = smart_cast<rmcRopeDrawable*>(GetDrawable());

	atFixedArray<Vector3,RopeModel::sm_MaxDrawVerts> *dst = &ropeDrawable->m_DrawVerts;
	dst->Resize(vertCount);

#if ROPE_USE_BRIDGE

	clothBridgeSimGfx* pBridge = GetClothController()->GetBridge();
	Assert( pBridge );
	const u16* RESTRICT pDisplayMap = pBridge->GetClothDisplayMap(0);
	Assert( pDisplayMap );

	int k = 0;
	for( int i = lockedFromFront; i < vertCount; ++i )
	{
		(*dst)[k++] = src[ pDisplayMap[i] ];
	}
#else
	sysMemCpy(&(*dst)[0], &src[lockedFromFront], sizeof(Vector3)*vertCount);
#endif
}

phVerletCloth* environmentCloth::GetCloth(int lodIndex)
{
	Assert( m_ClothController );
	Assert( lodIndex > -1 && lodIndex < LOD_COUNT );
	return m_ClothController->GetCloth(lodIndex);
}

phVerletCloth* environmentCloth::GetCloth()
{
	Assert(m_ClothController);
	const int lodIndex = m_ClothController->GetLOD();
	return m_ClothController->GetCloth(lodIndex);
}

const phVerletCloth *environmentCloth::GetCloth () const
{
	Assert(m_ClothController);
	const int lodIndex = m_ClothController->GetLOD();
	return m_ClothController->GetCloth(lodIndex);
}

void environmentCloth::ControlVertex (int vertexIndex, Vec3V_In worldPosition, bool teleport)
{
	phVerletCloth* cloth = GetCloth();
	Assert( cloth );
	phClothData& clothData = cloth->GetClothData();
	int numVerts = cloth->GetNumVertices();
	if (vertexIndex < numVerts)
	{
		clothData.SetVertexPosition(vertexIndex,worldPosition);
		if (teleport)
		{
			if (cloth->GetFlag(phVerletCloth::FLAG_IS_ROPE))
			{
				clothData.SetVertexPrevPosition(vertexIndex,worldPosition);
			}
		}

		if (cloth->GetFlag(phVerletCloth::FLAG_IS_ROPE))
		{
			cloth->DynamicPinVertex(vertexIndex);
		}
	}
}


void environmentCloth::ControlVertices (int* vertexIndexList, int numVertices, Vec3V_In worldPosition)
{
	for (int controlledVertIndex=0; controlledVertIndex<numVertices; controlledVertIndex++)
	{
		ControlVertex(vertexIndexList[controlledVertIndex],worldPosition);
	}
}


} // namespace rage

