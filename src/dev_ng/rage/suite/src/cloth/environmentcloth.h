// 
// cloth/environmentcloth.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef ENVIRONMENT_CLOTH_H
#define ENVIRONMENT_CLOTH_H


#include "fragment/cloth_config.h"

#include "data/base.h"
#include "data/resource.h"
#include "data/struct.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/viewport.h"
#include "mesh/mesh.h"
#include "rmcore/typefileparser.h"
#include "vector/matrix34.h"
#include "system/taskheader.h"

#include "clothcontroller.h"

#define		MAX_CUSTOM_BOUNDS_PER_CLOTH		8
#define		ROPE_USE_BRIDGE					0

namespace rage
{

class grcVertexBuffer;
class Matrix34;
class Vector3;

class rmcDrawable;
class rmcDrawableBase;

class phBoundSphere;
class phInst;
class phArchetypePhys;
class phSimulator;
class phVerletCloth;
class phClothVerletBehavior;
class environmentCloth;


class phEnvClothVerletBehavior : public phClothVerletBehavior
{
public:
	phEnvClothVerletBehavior (environmentCloth* cloth)
	{
		m_EnvCloth = cloth;
	}
	phEnvClothVerletBehavior(class datResource& rsc);
	DECLARE_PLACE(phEnvClothVerletBehavior);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	void UpdateClothSim(float timeScale, float timeStep, Mat34V_In attachedFrame);
	void UpdateAgainstCollisionInsts(Mat34V_In attachedFrame);
	void UpdateRopeSim(float timeStep, Mat34V_In attachedFrame);
	sysTaskHandle UpdateRopeSimSpu (float timeStep);
	sysTaskHandle UpdateClothSimSpu( Mat34V_In attachedFrame, float timeStep, grmGeometryQB* vb, clothController* controller, Vec3V_In force, phVerletSPUDebug* s_VerletSpuDebug);

	environmentCloth* GetEnvironmentCloth () { return m_EnvCloth; }
	virtual bool CollideObjects (Vec::V3Param128 timeStep, phInst* myInst, phCollider* myCollider, phInst* otherInst, phCollider* otherCollider, phInstBehavior* otherInstBehavior);

private:
	datRef<environmentCloth> m_EnvCloth;
};


// clothVertexBlend

class clothVertexBlend : public pgBase
{
public:

	clothVertexBlend() {}	
	clothVertexBlend(class datResource& rsc)
		: m_Vertex0(rsc,true)
		, m_Vertex1(rsc,true)
	{

	}
	DECLARE_PLACE(clothVertexBlend);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	void Load(const char* filePath, const char* clothName);
	void Save(void* clothName);

	atArray<Vec3V> m_Vertex0;
	atArray<Vec3V> m_Vertex1;

	static const char* sm_tuneFilePath;
	static const char* sm_MetaDataFileAppendix;

	PAR_SIMPLE_PARSABLE;

};


// clothInstanceTuning

class clothInstanceTuning : public pgBase
{
public:

	enum enCLOTH_TUNE_FLAGS
	{
		CLOTH_TUNE_WIND_FEEDBACK		= 0,
		CLOTH_TUNE_FLIP_INDICES_ORDER	= 1,
		CLOTH_TUNE_IGNORE_DISTURBANCES	= 2,
		CLOTH_TUNE_IS_IN_INTERIOR		= 3,
		CLOTH_TUNE_NO_PED_COLLISION		= 4,
		CLOTH_TUNE_USE_DISTANCE_THRESHOLD	= 5,
		CLOTH_TUNE_CLAMP_HORIZONTAL_FORCE	= 6,
		CLOTH_TUNE_FLIP_GRAVITY				= 7,
		CLOTH_TUNE_ACTIVATE_ON_HIT			= 8,
		CLOTH_TUNE_FORCE_VERTEX_RESISTANCE	= 9,
		CLOTH_TUNE_UPDATE_IF_VISIBLE		= 10,
	};

	clothInstanceTuning()
		: m_ExtraForce(Vec3V(V_ZERO))		
		, m_RotationRate( PI )
		, m_AngleThreshold( (PI / 6.0f) )
		, m_Weight(-1.0f)
		, m_DistanceThreshold(0.0f)
	{
	}
	clothInstanceTuning(class datResource& rsc) 
		: m_Flags(rsc)
	{}
	DECLARE_PLACE(clothInstanceTuning);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	float	m_RotationRate;
	float	m_AngleThreshold;	
	Vec3V	m_ExtraForce;
	atFixedBitSet<32, u32>		m_Flags;
	float	m_Weight;
	float	m_DistanceThreshold; // NOTE: it is used as vertex resistance ONLY when CLOTH_TUNE_FORCE_VERTEX_RESISTANCE is set in tuning
	u8		m_PinVert;
	u8		m_NonPinVert0;
	u8		m_NonPinVert1;
	char	m_Padding[1];

	void Reset()
	{
		m_ExtraForce			= Vec3V(V_ZERO);
		m_RotationRate			= PI;
		m_AngleThreshold		= (PI / 6.0f);
		m_DistanceThreshold		= 0.0f;
		m_PinVert				= 0;
		m_NonPinVert0			= 0;
		m_NonPinVert1			= 0;
		m_Flags.Reset();
	}

	bool GetFlag(enCLOTH_TUNE_FLAGS flag) const { return m_Flags.IsSet(flag); }

#if __BANK && !__RESOURCECOMPILER
	void AddWidgets(bkGroup* ragGroup);
#endif

	void Load(const char* filePath, const char* clothName);
	void Save(void* clothName);
	static void SaveEmpty(void* clothName);

	static const char* sm_tuneFilePath;
	static const char* sm_MetaDataFileAppendix;

	PAR_SIMPLE_PARSABLE;
};


class environmentCloth : public clothBase
{
public:
	static const int RORC_VERSION = 5;

	environmentCloth();
	virtual ~environmentCloth();

	DECLARE_PLACE(environmentCloth);
	environmentCloth(class datResource& rsc);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

#if CLOTH_INSTANCE_FROM_DATA
	void InstanceFromData(int vertsCapacity, int edgesCapacity);
#endif

	void InstanceFromTemplate( const environmentCloth* copyme );
	void Shutdown();

	void LoadCloth( rmcTypeFileCbData* data );
	bool LoadLod( fiTokenizer &t, const char *match, int lod, int lodMask, Vec3V_In extraOffsetV, phClothConnectivityData* connectivity, int extraVerts );

	void AllocateClothController ();

	phInst* CreateInstance(phBound* clothBound);
#if !USE_CLOTH_PHINST
	void DeletePhInst(phInst* pphInst);
#endif
	phInst* CreatePhysics( phInst* clothInstance=NULL, float ropeRadius=-1.0f );
	phEnvClothVerletBehavior* CreateBehavior();

// 	phBoundComposite& CreateRopeBound (int numEdges, float ropeLength, float startRadius, float endRadius=-1.0f);
// 	phBoundComposite& CreateRopeBound (float ropeRadius=-1.0f);

	void SetAttachedToFrame( const Matrix34 &frame ) { m_AttachedToFrame = &frame; }
	bool GetIsAttachedToFrame() { return m_AttachedToFrame != NULL; }
	Mat34V_Out GetAttachedToFrame() const { Assert( m_AttachedToFrame ); return MATRIX34_TO_MAT34V(*m_AttachedToFrame); }
	Vector3 GetFramePosition() const { Assert(m_AttachedToFrame); return (*m_AttachedToFrame).d; }

	Vec3V_Out GetOffset();
	Vec3V_Out GetBBMinWorldSpace();
	Vec3V_Out GetBBMaxWorldSpace();


#if !__SPU
	Mat34V_Out GetClothFrame() const;
#endif

	void UpdateCloth(float dt,float timeScale);
	void UpdateAgainstCollisionInsts(bool updateAgainstCollisionInsts, bool clearCollisionInsts);
	void UpdateRope(float dt);

	sysTaskHandle UpdateSpu(float dt, phVerletSPUDebug* s_VerletSpuDebug);
	sysTaskHandle UpdateRopeSpu(float dt, phVerletSPUDebug* s_VerletSpuDebug = NULL);
	void UpdateClothPostSpu();
	void UpdateRopePostSpu();

	void CopyVerletDrawVertsForRope();
	void ForceNormalsToDrawable();

	phEnvClothVerletBehavior* GetBehavior () { return m_Behavior; }
	const	phEnvClothVerletBehavior* GetBehavior () const { return m_Behavior; }
	phVerletCloth* GetCloth(int lodIndex);
	phVerletCloth* GetCloth ();
	const	phVerletCloth* GetCloth () const;
	clothController* GetClothController () const { return m_ClothController; }

	int GetLOD() const { return m_ClothController->GetLOD(); }

	void ControlVertex (int vertexIndex, Vec3V_In worldPosition, bool teleport=false);
	//	NOTE: This can be used to hold coil rope, as for a lasso.
	void ControlVertices (int* vertexIndexList, int numVertices, Vec3V_In worldPosition);


	inline int GetNumRopeVertices () const 
	{ 
		const phVerletCloth* pCloth = GetCloth();
		Assert( pCloth );
		int numVerts = pCloth->GetNumVertices();
		return numVerts - pCloth->GetNumLockedEdgesBack() - pCloth->GetNumLockedEdgesFront();
	}

	bool GetIsDrawableOwned() const { return (m_Flags & CLOTH_IsDrawableOwned) ? true: false; }
	void SetIsDrawableOwned(bool isDrawableOwned);
	rmcDrawable* GetDrawable() const { return m_ReferencedDrawable.ptr; }

	void SetReferencedDrawable( rmcDrawable *drawit, bool isDrawableOwned );
	void CleanUpDrawable();

#if USE_CLOTH_PHINST
	grcCullStatus IsVisible(const grcViewport &vp,u8 &lod, float* retZDist = NULL) const;
#endif

	void SwitchLOD( int newLodIndex, int oldLodIndex, int mapIndex, bool shouldMorph );

	Vec3V_Out GetInitialPosition () const { return m_InitialPosition; }
	void SetInitialPosition(Vec3V_In pos) { m_InitialPosition = pos; }

	void LoadLod( fiTokenizer &t, const char *match, int lod, int lodMask );
	void CheckDistance( Vec3V_In pointOfInterest );

	bool GetIsVisible();

	void AllocateDamageArray();

	void ApplyAirResistance(Vec3V* RESTRICT pNormals, const Vector3& vAirSpeed);
	void ApplyAirResistanceRope(const Vector3& vAirSpeed, float fDragCoeff = 0.1f);

	// every environment cloth should have a force member that is updated according to various conditions
	// - is the cloth indoors
	// - change/update the wind based on events like opening and closing a door
	inline void SetForce( Vec3V_In force ) { m_Force = force; }

// 	void AddInstance( environmentCloth* inst );
// 	void RemoveInstance( environmentCloth* inst );

	atArray<int>& GetUserData() { return m_UserData; }
	void InitUserData();

#if !__SPU
	void ForceVisible() { SetFlag(CLOTH_IsVisible, true); }
	void LoadAll();
	void SaveAll();
	void LoadVerts();
	void SaveVerts();
	void LoadUserData(const char* filePath = "common:/data/cloth/");
	void SaveUserData(const char* filePath = "common:/data/cloth/");
	void SaveEmptyTuning();

#if __BANK && !__RESOURCECOMPILER
	void AddWidgets(bkGroup* ragGroup);
#endif
#endif
	enum enFlags
	{
		CLOTH_IsDrawableOwned			=	1 << 1,
		CLOTH_IsMorph					=	1 << 2,
		CLOTH_IsVisible					=	1 << 3,
		CLOTH_IsTuningOwned				=	1 << 4,
		CLOTH_IsInInterior				=	1 << 5,
		CLOTH_IsBlendOwned				=	1 << 6,
		CLOTH_IsAllowedCollisionWithPed	=	1 << 7,
		CLOTH_IsMoving					=	1 << 8,
		CLOTH_IsSkipSimulation			=	1 << 9,
		CLOTH_HasLocalBounds			=	1 << 10,
		CLOTH_IsUpdating				=	1 << 11,
		CLOTH_ForceInactiveForReplay	=   1 << 12,
#if CLOTH_SIM_MESH_ONLY
		CLOTH_IsSimMeshOnly				=	1 << 31,	// all debug flags should go at the end of the range
#endif
	};

	bool GetFlag(enFlags flagToCheck) const { return (m_Flags & flagToCheck) ? true: false;	}
	void SetFlag(enFlags flagToSet, bool isTrue)
	{
		if( isTrue )
			sysInterlockedOr( &m_Flags, (u32)flagToSet ); 
		else
			sysInterlockedAnd( &m_Flags, (u32)~flagToSet );
	}


	void SwitchToLOD( void* data );
	void SwitchToDrawable( void* data );
	const clothInstanceTuning* GetTuning() const { return m_Tuning; }
#if USE_CLOTH_PHINST
	void ClearBound();
	void ClearInst() { m_cthInst = NULL; }
#endif

	static const char* sm_MetaDataFileAppendix;

private:

	datOwner<clothInstanceTuning>		m_Tuning;
	pgRef<rmcDrawable>					m_ReferencedDrawable;

	datOwner<phEnvClothVerletBehavior>	m_Behavior;
	datOwner<clothController>			m_ClothController;
#if USE_CLOTH_PHINST
	datOwner<phInst>					m_cthInst;
	int									m_BoundIdx;
#else
	char								m_Padding0[sizeof(datOwner<phInst>) + sizeof(int)];
#endif

#if __64BIT
	char								m_Padding[4];
#endif

	Vec3V								m_InitialPosition;
	Vec3V								m_Force;

	atArray< int >						m_UserData;
	const Matrix34*						m_AttachedToFrame;
	u32									m_Flags;

	PAR_SIMPLE_PARSABLE;
};


#if __RESOURCECOMPILER
typedef void (*CustomBoundsCB)(environmentCloth* pEnvCloth, const char* fileName, const char* fileNameAppendix, const char* pClothControllerName, bool bSetFlag);
#endif


} // namespace rage

#endif // ENVIRONMENT_CLOTH_H
