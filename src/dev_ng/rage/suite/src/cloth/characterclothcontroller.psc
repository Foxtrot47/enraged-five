<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<enumdef type="::rage::characterClothControllerTuning::enFlags">
  <enumval name="FLAG_DONT_USE_BEND_EDGES"/>
</enumdef>

  
<structdef type="::rage::characterClothControllerTuning">
  <bitset name="m_Flags" type="fixed32" numBits="32" values="::rage::characterClothControllerTuning::enFlags"/>
  <int name ="m_ExtraPinRadiusChannels"/>
</structdef>

  
<structdef type="::rage::characterClothController::BindingInfo">
  <Vector4 name="weights"/>
  <array name="blendIndices" type="member" size="4">
    <int/>
  </array>
</structdef>
  
  
<structdef type="::rage::characterClothController">
  <array name="m_TriIndices" type="atArray">
    <u16/>
  </array>
  <array name="m_OriginalPos" type="atArray">
    <Vec3V/>
  </array>
  <array name="m_BoneIndexMap" type="atArray">
    <s32/>
  </array>
  <array name="m_BindingInfo" type="atArray">
    <struct type="::rage::characterClothController::BindingInfo"/>
  </array>
</structdef>

  
</ParserSchema>