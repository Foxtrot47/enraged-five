// 
// cloth/clothbridgesimgfx.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CLOTH_BRIDGE_SIM_GFX_H
#define CLOTH_BRIDGE_SIM_GFX_H

#include "data/base.h"
#include "data/resource.h"
#include "phcore/constants.h"
#include "pheffects/cloth_verlet.h"
#include "grprofile/drawcore.h"
#include "rmcore/lodgroup.h"
#include "vector/vector3.h"

#if __WIN32
#pragma warning( push )
#pragma warning( disable : 4201 )
#endif

namespace rage {

struct mshMaterial;
class grcVertexBuffer;
class phBoundGeometry;


template<class _Type>
struct atArrayWrap
{
	atArray<_Type> m_Data;
	atArrayWrap() : m_Data(!datResource_sm_Current) {}
	atArrayWrap(datResource& rsc) : m_Data( rsc ) {}
	~atArrayWrap() {}

	__forceinline _Type& operator[](int index) { return m_Data[index]; }
	DECLARE_PLACE(atArrayWrap);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		SSTRUCT_BEGIN(atArrayWrap)
			SSTRUCT_FIELD(atArrayWrap, m_Data)
		SSTRUCT_END(atArrayWrap)
	}
#endif
};


struct phMorphDataAoS
{
	Vec4V	weights;
	u16 vtxIndex, index0, index1, index2;
	char m_Pad[8];
};

struct phMorphData2AoS
{
	Vec4V	weights;
	u16 vtxIndex, index0, index1, index2, index3;
	char m_Pad[2];
};


// phMorphData

class phMorphData
{
public:						
	atArrayWrap<Vec4V>	weights;
	atArrayWrap<u16>	vtxIndex;
	atArrayWrap<u16>	index0;
	atArrayWrap<u16>	index1;
	atArrayWrap<u16>	index2;

	DECLARE_PLACE(phMorphData);
	phMorphData( class datResource& rsc ) : weights(rsc), vtxIndex(rsc), index0(rsc), index1(rsc), index2(rsc) {}
	phMorphData() {}
	~phMorphData() {}

	void Alloc( const int vtxCount );
	void Swap( const int oldIndex, const int newIndex );

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif
};


// phMapData

class phMapData
{
public:
	enum enWEIGHT_LEVEL
	{
		enWEIGHT_NONE,
		enWEIGHT_ONE,
		enWEIGHT_TWO,
	};

	atRangeArray< phMorphData, LOD_COUNT >		m_MMap;
	atRangeArray< atArrayWrap<u16>, LOD_COUNT >	m_IMap;
	int m_PolyCount;

	DECLARE_PLACE(phMapData);
	phMapData( class datResource& rsc )
		: m_MMap(rsc,true)
		, m_IMap(rsc,true)
	{}

	phMapData() : m_PolyCount(0) {}
	~phMapData() {}

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(phMapData);			
			STRUCT_FIELD(m_MMap);
			STRUCT_FIELD(m_IMap);
			STRUCT_FIELD(m_PolyCount);
		STRUCT_END();
	}
#endif
};


// phMorphController

class phMorphController : public pgBase
{
public:

	char		m_pad[4];
	datOwner<phMapData>	m_MapData[LOD_COUNT];

#if __PFDRAW
	void DrawNormals( int lodIndex, Vector3* RESTRICT vecStream );
	void DrawMapping(int mapIndex, int levelIndex, Vector3* vertexStream );
#endif

	DECLARE_PLACE(phMorphController);
	phMorphController( class datResource& ) {}

	phMorphController()
	{
		for( int i = 0; i < LOD_COUNT; ++i)
			m_MapData[i] = 0;
	}

	virtual ~phMorphController()
	{
		for( int i = 0; i < LOD_COUNT; ++i)
		{
			if( m_MapData[i] )			
				delete m_MapData[i];
		}
	}

	static const int RORC_VERSION = 1;

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	void SwapMorphData( const int oldIndex, const int newIndex, int mapperIndex, int mapToLevel, phMapData::enWEIGHT_LEVEL /*wLevel*/ );

	void CreateIndexMap( const int mapIndex, const int levelIndex, const int indexCount );
	void CreateMorphMap( const int mapIndex, const int levelIndex, const int indexCount, phMapData::enWEIGHT_LEVEL wLevel );

	void Morph( int hiLOD,  int lowLOD, Vec3V* RESTRICT hiLODStream, Vec3V* RESTRICT lowLODStream, phMapData::enWEIGHT_LEVEL wLevel,  phMapData::enWEIGHT_LEVEL wLevel2 );
	void MorphLevelONE( int hiLOD, int lowLOD, Vec3V* RESTRICT hiLODStream, Vec3V* RESTRICT lowLODStream );
	void MorphLevelTWO( int hiLOD, int lowLOD, Vec3V* hiLODStream, Vec3V* RESTRICT lowLODStream );

	void CreateDeformMapping( const int numVerticesMeshHi, Vec3V* RESTRICT vecStreamMeshHi, Vec3V* RESTRICT vecStreamMeshLow, int mapperIndex, int mapToLevel, phMapData::enWEIGHT_LEVEL wLevel );
	void CreateDirectMapping( const int meshVerts, const int meshverts2, Vec3V* mesh, Vec3V* RESTRICT streamToSearchInto, int mapIndex, int levelIndex, u16* indexMap );

	u16 FindIndex( Vec3V_In vtx, Vec3V* RESTRICT vectorStream, int vtxCount, u16* indexMap );
	void FindCoordinates(int mapperIndex, int vertexIndexHi, Vec3V* RESTRICT vtxHi, int indexLow, Vec3V* RESTRICT vectorStream, phMapData::enWEIGHT_LEVEL wLevel  );

	static const Vec3V_Out FindProjectedVtx(	Vec3V_In vtx3D, 
		Vec3V_In planeVtx0, 
		Vec3V_In planeVtx1, 
		Vec3V_In planeVtx2, 
		Vec3V_InOut n, ScalarV_InOut d );

	static const Vec3V_Out FindWeightedVtx(	Vec3V_In projectedVtx, 
		Vec3V_In planeVtx0, 
		Vec3V_In planeVtx1, 
		Vec3V_In planeVtx2, 
		Vec3V_In n, 
		ScalarV_In d, 	
		ScalarV_InOut w0, ScalarV_InOut w1, ScalarV_InOut w2 );

	const POLYGON_INDEX* GetMap( int mapIndex, int ) const;
	int GetIndexMapCount( int mapIndex, int ) const ;
	int GetMorphMapCount( int mapIndex, int ) const ;

	int GetLODsCount(int* indices) const
	{
		Assert( indices );
		int result = 0;
		indices[result++] = 0;
		for( int i = 1; i < LOD_COUNT; ++i)
		{
			if( m_MapData[0] && GetIndexMapCount(0, i) )
			{
				indices[result++] = i;
			}
		}
		return result;
	}

	// TODO: move this function out of this class
	void ClearW( const int vertsCount, Vec3V* stream );

};

inline const POLYGON_INDEX* phMorphController::GetMap( int mapIndex, int level ) const
{ 
	Assert( m_MapData[ mapIndex ] );
	return (POLYGON_INDEX*)m_MapData[ mapIndex ]->m_IMap[level].m_Data.GetElements();
}

inline int phMorphController::GetIndexMapCount( int mapIndex, int level ) const
{ 
	Assert( m_MapData[ mapIndex ] );
	return m_MapData[ mapIndex ]->m_IMap[level].m_Data.GetCount();
}

inline int phMorphController::GetMorphMapCount( int mapIndex, int level ) const
{ 
	Assert( m_MapData[ mapIndex ] );
	return m_MapData[ mapIndex ]->m_MMap[level].weights.m_Data.GetCount();
}


// clothBridgeSimGfx

class clothBridgeSimGfx : public pgBase
{
public:

	clothBridgeSimGfx();
	virtual ~clothBridgeSimGfx() {}

	DECLARE_PLACE(clothBridgeSimGfx);
	clothBridgeSimGfx(class datResource& rsc);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	void AllocateDisplayMappings(int lodIndex)
	{
		Assert( !m_ClothDisplayMap[lodIndex].m_Data.GetCount() );
		Assert( GetMeshVertCount(lodIndex) );
		m_ClothDisplayMap[lodIndex].m_Data.Resize( GetMeshVertCount(lodIndex) );
	}

	void CreateDisplayMapping (int count, int lodIndex);
	void SwapDisplayMapVertices (int vertexIndexA, int vertexIndexB, int lodIndex);
	void SwapVertexPinningData (int vertexInedxA, int vertexIndexB, int lodIndex);

	int GetMeshVertCount (int lodIndex) const { return m_MeshVerts[lodIndex]; }
	u16 GetClothDisplayMapValue (int index, int lodIndex) const { return m_ClothDisplayMap[lodIndex].m_Data[index];	}
	const	u16* GetClothDisplayMap(int lodIndex) const { return m_ClothDisplayMap[lodIndex].m_Data.GetElements(); }
			u16* GetClothDisplayMap(int lodIndex)		{ return m_ClothDisplayMap[lodIndex].m_Data.GetElements(); }

	void SetMeshVertCount (int p, int lodIndex) { m_MeshVerts[lodIndex] = p; }
	int CreatePinData( const int extraPinRadiusChannels, int lodIndex, const mshMaterial* mtl, int vertexCount, int* pinMap, bool softPinned, float& clothWeight );

#if CLOTH_INSTANCE_FROM_DATA
	void InstanceFromData(int vertsCapacity);
#endif

	bool FindFile(const char* pCharacterName, const char* pControllerName);

#if __SPU
	  u16* &GetClothDisplayMapRef(int lodIndex) { return m_ClothDisplayMap[lodIndex].m_Data.GetElements(); }
	float* &GetInflationScaleRef(int lodIndex) { return m_InflationScale[lodIndex].m_Data.GetElements(); }
	float* &GetVertexWeightsRef(int lodIndex) { return m_VertexWeight[lodIndex].m_Data.GetElements(); }
	float* &GetVertexBlendsRef(int lodIndex) { return m_PinRadius[lodIndex].m_Data.GetElements(); }
#endif // __SPU

	atBitSet& GetStateList() { return m_PinnableList;	}
	const atBitSet& GetStateList() const { return m_PinnableList;	}
	int GetVertexBlendsCount(int lodIndex) const { return m_PinRadius[lodIndex].m_Data.GetCount(); }
	int GetVertexWeightsCount(int lodIndex) const { return m_VertexWeight[lodIndex].m_Data.GetCount(); }

	const float* GetVertexBlends(int lodIndex) const { return m_PinRadius[lodIndex].m_Data.GetElements(); }
	const float* GetVertexWeights(int lodIndex) const { return m_VertexWeight[lodIndex].m_Data.GetElements(); }
	const float* GetInflationScale(int lodIndex) const { return m_InflationScale[lodIndex].m_Data.GetElements(); }
	
// TODO: TEMP
		float* GetInflationScale(int lodIndex) { return m_InflationScale[lodIndex].m_Data.GetElements(); }

#if !__SPU
	void Load(const char* controllerName, const char* filePath = "common:/data/cloth/");	
	void Save(const char* controllerName, const char* filePath = "common:/data/cloth/");
 #if __BANK
	void RegisterRestInterface(const char* controllerName);
	void UnregisterRestInterface(const char* controllerName);
 #endif
#endif

private:

	int	m_MeshVerts[LOD_COUNT];
	atRangeArray< atArrayWrap<float>, LOD_COUNT >	m_PinRadius;
	atRangeArray< atArrayWrap<float>, LOD_COUNT >	m_VertexWeight;
	atRangeArray< atArrayWrap<float>, LOD_COUNT >	m_InflationScale;
	atRangeArray< atArrayWrap<u16>,	  LOD_COUNT >	m_ClothDisplayMap;	

	char		m_Pad[4];
	atBitSet	m_PinnableList;	

	static const char* sm_MetaDataFileAppendix;

	PAR_SIMPLE_PARSABLE;
};


} // namespace rage


#if __WIN32
#pragma warning( pop )
#endif

#endif // CLOTH_BRIDGE_SIM_GFX_H
