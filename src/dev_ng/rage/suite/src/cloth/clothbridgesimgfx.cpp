#include "cloth/clothbridgesimgfx.h"
#include "system/cache.h"
#include "grmodel/geometry.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "mesh/mesh.h"
#include "parser/restparserservices.h"
#include "phbound/boundgeom.h"
#include "rmcore/drawable.h"
#include "vectormath/classes_soa.h"
#include "vectormath/layoutconvert.h"
#include "vector/colors.h"


#include "clothbridgesimgfx_parser.h"


//#pragma optimize("", off)

#define		DEFAULT_VERTEX_WEIGHT		0.0025f
#define		SECURE_THRESHOLD_VERTEX_WEIGHT		0.1f



namespace rage
{

const char* clothBridgeSimGfx::sm_MetaDataFileAppendix = "_bridge";

#if !__SPU && __BANK

clothBridgeSimGfx* s_CurrentClothBridge = NULL;
bool s_RegisterRestInterface = false;

#endif

template<> IMPLEMENT_PLACE(atArrayWrap<float>);
template<> IMPLEMENT_PLACE(atArrayWrap<u16>);
IMPLEMENT_PLACE(clothBridgeSimGfx);
IMPLEMENT_PLACE(phMorphController);
IMPLEMENT_PLACE(phMorphData);
IMPLEMENT_PLACE(phMapData);

clothBridgeSimGfx::clothBridgeSimGfx()
{
	for( int i = 0; i < LOD_COUNT; ++i)
	{
		m_MeshVerts[i] = 0;
	}
}

clothBridgeSimGfx::clothBridgeSimGfx(class datResource& rsc)
: m_PinnableList( rsc )
, m_ClothDisplayMap( rsc, true )
, m_PinRadius( rsc, true )
, m_InflationScale( rsc, true )
, m_VertexWeight( rsc, true )
{
	if(datResource_IsDefragmentation)
	{
		clothDebugf1("Defrag clothBridgeSimGfx");
	}
}

#if __DECLARESTRUCT
void clothBridgeSimGfx::DeclareStruct(datTypeStruct &s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(clothBridgeSimGfx, pgBase)
		SSTRUCT_CONTAINED_ARRAY(clothBridgeSimGfx, m_MeshVerts)
		SSTRUCT_FIELD(clothBridgeSimGfx, m_PinRadius)		
		SSTRUCT_FIELD(clothBridgeSimGfx, m_VertexWeight)
		SSTRUCT_FIELD(clothBridgeSimGfx, m_InflationScale)
		SSTRUCT_FIELD(clothBridgeSimGfx, m_ClothDisplayMap)		
		SSTRUCT_CONTAINED_ARRAY(clothBridgeSimGfx, m_Pad)
		SSTRUCT_FIELD(clothBridgeSimGfx, m_PinnableList)
	SSTRUCT_END(clothBridgeSimGfx)
}
#endif


void clothBridgeSimGfx::CreateDisplayMapping (int count, int lodIndex)
{
	Assert( !m_ClothDisplayMap[lodIndex].m_Data.GetCount() );
	m_ClothDisplayMap[lodIndex].m_Data.Resize(count);
	for (u16 index = 0; index < (u16)count; ++index )
	{
		m_ClothDisplayMap[lodIndex][index] = index;
	}
}

void clothBridgeSimGfx::SwapDisplayMapVertices (int vertexIndexA, int vertexIndexB, int lodIndex)
{
	Assert( m_ClothDisplayMap[lodIndex].m_Data.GetCount() );
	m_ClothDisplayMap[lodIndex][vertexIndexA] = (u16)vertexIndexB;
	m_ClothDisplayMap[lodIndex][vertexIndexB] = (u16)vertexIndexA;
}

void clothBridgeSimGfx::SwapVertexPinningData (int vertexIndexA, int vertexIndexB, int lodIndex )
{	
#if __RESOURCECOMPILER
	Assert( m_VertexWeight[lodIndex].m_Data.GetCount() );
	Assert( m_InflationScale[lodIndex].m_Data.GetCount() );
#else
	if( m_VertexWeight[lodIndex].m_Data.GetCount() == 0 || m_InflationScale[lodIndex].m_Data.GetCount() )
		return;
#endif

	if( m_PinRadius[lodIndex].m_Data.GetCount() )
	{
		const int vertsCount = m_VertexWeight[lodIndex].m_Data.GetCount();
		const int totalPinRadiusCount = m_PinRadius[lodIndex].m_Data.GetCount();

		const int numPinRadiusSets = totalPinRadiusCount / vertsCount;
		Assert( numPinRadiusSets > 0 );
		for( int i = 0; i < numPinRadiusSets; ++i )
		{
			const int offset = i * vertsCount;

			float temp = m_PinRadius[lodIndex][offset + vertexIndexA];
			m_PinRadius[lodIndex][offset + vertexIndexA] = m_PinRadius[lodIndex][offset + vertexIndexB];
			m_PinRadius[lodIndex][offset + vertexIndexB] = temp;
		}		
	}

	float temp2 = m_InflationScale[lodIndex][vertexIndexA];
	m_InflationScale[lodIndex][vertexIndexA] = m_InflationScale[lodIndex][vertexIndexB];
	m_InflationScale[lodIndex][vertexIndexB] = temp2;

	float tempV = m_VertexWeight[lodIndex][vertexIndexA];
	m_VertexWeight[lodIndex][vertexIndexA] = m_VertexWeight[lodIndex][vertexIndexB];
	m_VertexWeight[lodIndex][vertexIndexB] = tempV;

	bool tempValueA = m_PinnableList.IsSet( vertexIndexA );
	bool tempValueB = m_PinnableList.IsSet( vertexIndexB );

	m_PinnableList.Set( vertexIndexA, tempValueB );
	m_PinnableList.Set( vertexIndexB, tempValueA );
}

#if !__SPU 

#if __BANK
void clothBridgeSimGfx::RegisterRestInterface(const char* controllerName)
{
	if( !s_RegisterRestInterface )
	{		
		s_RegisterRestInterface = true;
		s_CurrentClothBridge = this;
		parRestRegisterSingleton("Physics/Cloth/Bridge", *this, NULL);

		clothDebugf1("Registered Physics/Cloth/Bridge REST interface for: %s", controllerName);
	}
}
void clothBridgeSimGfx::UnregisterRestInterface(const char* controllerName)
{
	if( s_RegisterRestInterface )
	{		
		s_RegisterRestInterface = false;
		s_CurrentClothBridge = NULL;
		REST.RemoveAndDeleteService("Physics/Cloth/Bridge");

		clothDebugf1("Unregistered Physics/Cloth/Bridge REST interface for: %s", controllerName);
	}	
}
#endif // __BANK


void clothBridgeSimGfx::Load(const char* controllerName, const char* filePath)
{
	Assert( controllerName );
	char buff[256];
	formatf( buff, "%s%s%s", filePath, controllerName, clothBridgeSimGfx::sm_MetaDataFileAppendix );
	PARSER.LoadObject( buff, "xml", *this, &parSettings::sm_StrictSettings);	

	clothDebugf1("Loaded cloth bridge from file: %s", buff);
}
void clothBridgeSimGfx::Save(
#if __FINAL
						const char* , const char*
#else
						const char* controllerName, const char* filePath					 
#endif
						 )
{
#if !__FINAL
	Assert( controllerName );
	char buff[256];
	formatf( buff, "%s%s%s", filePath, controllerName, clothBridgeSimGfx::sm_MetaDataFileAppendix );
	AssertVerify(PARSER.SaveObject( buff, "xml", this, parManager::XML));

	clothDebugf1("Saved cloth bridge to file: %s", buff);
#endif
}
#endif // !__SPU


#if MESH_LIBRARY

bool clothBridgeSimGfx::FindFile(const char* pCharacterName, const char* pControllerName)
{
// NOTE: the ASSET path should have been set by the caller through ASSET.PushFolder
	char buff[256];
	formatf( buff, "%s_%s%s", pCharacterName, pControllerName, clothBridgeSimGfx::sm_MetaDataFileAppendix );
	return ASSET.Exists(buff, "xml");
}

#if CLOTH_INSTANCE_FROM_DATA
void clothBridgeSimGfx::InstanceFromData(int vertsCapacity)
{
// TODO: cloth sim lods not supported yet
	const int lodIndex = 0;

	Assert( !m_VertexWeight[lodIndex].m_Data.GetCount() );
	Assert( !m_InflationScale[lodIndex].m_Data.GetCount() );
	Assert( !m_ClothDisplayMap[lodIndex].m_Data.GetCount() );

	m_VertexWeight[lodIndex].m_Data.Resize(vertsCapacity);
	m_InflationScale[lodIndex].m_Data.Resize(vertsCapacity);
	m_ClothDisplayMap[lodIndex].m_Data.Resize(vertsCapacity);
	
	for( int i = 0; i < vertsCapacity; ++i)
	{
		m_VertexWeight[lodIndex].m_Data[i] = 1.0f;
		m_InflationScale[lodIndex].m_Data[i] = 1.0f;
		m_ClothDisplayMap[lodIndex].m_Data[i] = 999;
	}
}
#endif // CLOTH_INSTANCE_FROM_DATA

// Note: return number of pinned verts
int clothBridgeSimGfx::CreatePinData( const int extraPinRadiusChannels, int lodIndex, const mshMaterial* mtl, int vertCount, int* pinMap, bool isSoftPinned, float& clothWeight)
{
	Assert( mtl );

	int numPinnedVerts = 0;
	clothWeight = DEFAULT_CLOTH_WEIGHT;
	
	Assert( !m_VertexWeight[lodIndex].m_Data.GetCount() );
	Assert( !m_InflationScale[lodIndex].m_Data.GetCount() );

	m_VertexWeight[lodIndex].m_Data.Resize(vertCount);
	m_InflationScale[lodIndex].m_Data.Resize(vertCount);	
	sysMemSet(m_VertexWeight[lodIndex].m_Data.GetElements(), 0, sizeof(float) * vertCount);
	sysMemSet(m_InflationScale[lodIndex].m_Data.GetElements(), 0, sizeof(float) * vertCount);

// TODO: pinnable list only for high res LOD?
	if( !lodIndex )
		m_PinnableList.Init(vertCount);

	int vtxWeightChannelIndex		= mtl->FindChannel(mshVtxBlindCLOTH_WEIGHT);
	int pinChannelIndex				= mtl->FindChannel(mshVtxBlindCLOTHPIN);
	
	int vtxResistanceChannelIndex	= mtl->FindChannel(mshVtxBlindCLOTHPINRAMP);		// The channel is called PinRamp but should be VertexWeight
	int vtxStateChannelIndex		= mtl->FindChannel(mshVtxBlindCLOTHPINTYPE);		// is vertex locked/pinnable - if is 1.0f vertex can be pinned/unpinned, 0.0f vertex can't change the original state ( can't be pinned or unpinned )
	int inflationScaleChannelIndex	= mtl->FindChannel(mshVtxBlindCLOTH_INFLATION_SCALE);

	const mshChannel* pinChannel = ( pinChannelIndex >= 0 )? &mtl->GetChannel(pinChannelIndex): NULL;	
	const mshChannel* vtxResistanceChannel = ( vtxResistanceChannelIndex >= 0 )? &mtl->GetChannel(vtxResistanceChannelIndex): NULL;
	const mshChannel* vtxWeightChannel = ( vtxWeightChannelIndex >= 0 )? &mtl->GetChannel(vtxWeightChannelIndex): NULL;
	const mshChannel* vtxStateChannel = ( vtxStateChannelIndex >= 0 )? &mtl->GetChannel(vtxStateChannelIndex): NULL;
	const mshChannel* inflationScaleChannel = ( inflationScaleChannelIndex >= 0 )? &mtl->GetChannel(inflationScaleChannelIndex): NULL;

	if( !pinChannel )
	{
		Warningf( "Why there is no pin channel ?!" );
		return 0;
	}

	const int channelVertCount = pinChannel->GetVertexCount();

	if( isSoftPinned )
	{
		Assert( channelVertCount == vertCount );

		const int radiusChannelIndex = mtl->FindChannel(mshVtxBlindCLOTHPINRADIUS);
		const mshChannel* radiusChannel = ( radiusChannelIndex >= 0 )? &mtl->GetChannel(radiusChannelIndex): NULL;
		Assert(radiusChannel==NULL || radiusChannel->GetVertexCount() == channelVertCount);
		if (radiusChannel)
		{
			const int totalPinRadiusChannels = extraPinRadiusChannels + 1;

			Assert( !m_PinRadius[lodIndex].m_Data.GetCount() );
			m_PinRadius[lodIndex].m_Data.Resize(channelVertCount * totalPinRadiusChannels);

			for (int i=0; i<channelVertCount; i++)
			{
				const mshChannelData& radiusData = radiusChannel->Access(i, 0);
				m_PinRadius[lodIndex][i] = radiusData.f;
			}

			for(int j=0; j<extraPinRadiusChannels; ++j )
			{
				int extraChannelIndex = mtl->FindChannel(mshVtxBlindCLOTHPINRADIUSEXTRA0+j);
				const mshChannel* extraChannel = ( extraChannelIndex >= 0 )? &mtl->GetChannel(extraChannelIndex): NULL;
				if( extraChannel )
				{
					Assert(extraChannel->GetVertexCount() == channelVertCount );
					const int offset = channelVertCount*(j+1);
					for (int i=0; i<channelVertCount; i++)
					{
						const mshChannelData& extraChannelData = extraChannel->Access(i, 0);
						m_PinRadius[lodIndex][ offset + i ] = extraChannelData.f;	
					}
				}
			}
		}
	}

	
	Assert(vtxResistanceChannel==NULL || vtxResistanceChannel->GetVertexCount() == channelVertCount);
	for (int i=0; i<channelVertCount; i++)
	{
		if (pinChannel)
		{
			const mshChannelData& pinData = pinChannel->Access(i, 0);
			if (  pinData.f >= (1.0f - SMALL_FLOAT) )
			{
				Assert( numPinnedVerts < vertCount );
				pinMap[ numPinnedVerts++ ] = i;
			}
		}

		float vtxResistance = DEFAULT_VERTEX_WEIGHT;
		if (vtxResistanceChannel)
		{			
			const mshChannelData& vtxResistanceData = vtxResistanceChannel->Access(i, 0);
			if( vtxResistanceData.f < 0.1f )
				vtxResistance = vtxResistanceData.f;
			else
			{
				if( vtxResistanceData.f > 1.0f )
					vtxResistance = 0.01f;
				else
				{
					vtxResistance = vtxResistanceData.f * vtxResistanceData.f * 0.01f;
				}
			}
		}
		Assert( vtxResistance > -0.0000001f );
		m_VertexWeight[lodIndex][i] = vtxResistance;

		if( vtxWeightChannel )
		{
			const mshChannelData& vtxWeightData = vtxWeightChannel->Access(i, 0);
			clothWeight = Max( clothWeight, vtxWeightData.f );
		}

		if (vtxStateChannel)
		{			
// NOTE: for different states use some other values from the channel , 0.0f and 1.0f is what is expected at the moment
			const mshChannelData& stateData = vtxStateChannel->Access(i, 0);
			m_PinnableList.Set( i, (stateData.f > 0.0f? true: false) );

		}

		if( inflationScaleChannel )
		{
			const mshChannelData& channelData = inflationScaleChannel->Access(i, 0);
			m_InflationScale[lodIndex][i] = channelData.f;
		}		
	}

	return numPinnedVerts;
}

#endif // MESH_LIBRARY



// phMorphController

void phMorphController::CreateIndexMap( const int mapIndex, const int levelIndex, const int indexCount )
{
	if( !m_MapData[ mapIndex ] )
		m_MapData[ mapIndex ] = rage_new phMapData;

	Assert( m_MapData[ mapIndex ] );

// Note: the following  sysMemStartTemp should match in void environmentCloth::LoadCloth( rmcTypeFileCbData* data )
	if( mapIndex == levelIndex)
	{
		sysMemStartTemp();
	}

	m_MapData[ mapIndex ]->m_IMap[ levelIndex ].m_Data.Resize( indexCount );

// Note: the following  sysMemEndTemp should match in void environmentCloth::LoadCloth( rmcTypeFileCbData* data )
	if( mapIndex == levelIndex)
	{
		sysMemEndTemp();
	}
}



#define		THRESHOLD_FLOAT		10.0f


#if __DECLARESTRUCT
void phMorphController::DeclareStruct(datTypeStruct &s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(phMorphController, pgBase)
		SSTRUCT_CONTAINED_ARRAY(phMorphController,m_pad)
		SSTRUCT_CONTAINED_ARRAY(phMorphController,m_MapData)
	SSTRUCT_END(phMorphController)
}
#endif


// OUT
// n - plane's normal
// d - distance to the projected vertex

const Vec3V_Out phMorphController::FindProjectedVtx(	Vec3V_In vtx3D, 
														Vec3V_In planeVtx0, 
														Vec3V_In planeVtx1, 
														Vec3V_In planeVtx2, 
														Vec3V_InOut n, ScalarV_InOut d )
{

#if __PFDRAW
//	float radius = 0.03f;
	static bool drawVerts = false;
	if( drawVerts )
	{
// 		PF_DRAW_SPHERE_COLOR(MorphTemp, radius, VEC3V_TO_VECTOR3(planeVtx0), Color_red3);
// 		PF_DRAW_SPHERE_COLOR(MorphTemp, radius, VEC3V_TO_VECTOR3(planeVtx1), Color_red3);
// 		PF_DRAW_SPHERE_COLOR(MorphTemp, radius, VEC3V_TO_VECTOR3(planeVtx2), Color_red3);
	}	
#endif

	ScalarV w;			// plane's distance factor

	// Find the unit normal vector, perpendicular to the plane containing the three points.
	const Vec3V v1 = Subtract( planeVtx0, planeVtx1 );
	const Vec3V v2 = Subtract( planeVtx2, planeVtx1 );
	n = Cross( v1, v2 );
	n = Normalize( n );

	// Find the distance from the plane to the origin.
	w = Dot( n, planeVtx1 );
	w = -w;

	//-(n.x * vtx3D.x + n.y * vtx3D.y + n.z * vtx3D.z + p.w);
	d = Dot( n, vtx3D );
	d = Add( d, w );			
	d = -d;			

	const Vec3V projectedVtx = AddScaled( vtx3D, n, d );		
	d = -d;			 // restore the distance

#if __PFDRAW
	static bool drawProjectedVtx = false;
	if( drawProjectedVtx )
	{
//		PF_DRAW_SPHERE_COLOR(MorphTemp, 0.03f, VEC3V_TO_VECTOR3(projectedVtx), Color_green);
	}
#endif

	return projectedVtx;
}



const Vec3V_Out phMorphController::FindWeightedVtx(	Vec3V_In projectedVtx, 
													Vec3V_In planeVtx0, 
													Vec3V_In planeVtx1, 
													Vec3V_In planeVtx2, 
													Vec3V_In n, 
													ScalarV_In d, 	
													ScalarV_InOut w0, ScalarV_InOut w1, ScalarV_InOut w2 )
{
	const ScalarV wOne(V_ONE);
	const ScalarV wHalf(V_HALF);
	const ScalarV errThreshold(V_FLT_SMALL_3);

#if __PFDRAW
	static bool drawEdges = false;
	if( drawEdges )
	{
// 		PF_DRAW_LINE_COLOR(MorphTemp, VEC3V_TO_VECTOR3(planeVtx1), VEC3V_TO_VECTOR3(planeVtx0), Color_yellow);
// 		PF_DRAW_LINE_COLOR(MorphTemp, VEC3V_TO_VECTOR3(planeVtx2), VEC3V_TO_VECTOR3(planeVtx0), Color_yellow);
// 
// 		PF_DRAW_LINE_COLOR(MorphTemp, VEC3V_TO_VECTOR3(projectedVtx), VEC3V_TO_VECTOR3(planeVtx0), Color_red);
// 		PF_DRAW_LINE_COLOR(MorphTemp, VEC3V_TO_VECTOR3(projectedVtx), VEC3V_TO_VECTOR3(planeVtx1), Color_purple);
// 		PF_DRAW_LINE_COLOR(MorphTemp, VEC3V_TO_VECTOR3(projectedVtx), VEC3V_TO_VECTOR3(planeVtx2), Color_orange);
	}
#endif

	// Note: the idea is to calculate the surface area for each triangle ( from projected vtx to any other vtx )
	// and find the ratios/barycentric coordinates

	Vec3V edge0	= Subtract( planeVtx1, planeVtx0 );
	Vec3V edge1	= Subtract( planeVtx2, planeVtx0 );

	Vec3V edge2	= Subtract( projectedVtx, planeVtx0 );
	Vec3V edge3	= Subtract( projectedVtx, planeVtx1 );
	Vec3V edge4	= Subtract( projectedVtx, planeVtx2 );

	ScalarV e0 = Mag( edge0 );
	ScalarV e1 = Mag( edge1 );

	ScalarV e2 = Mag( edge2 );
	ScalarV e3 = Mag( edge3 );
	ScalarV e4 = Mag( edge4 );

	ScalarV a0 = Dot( edge2, edge3 );
	ScalarV b0 = Dot( edge2, edge4 );
	ScalarV c0 = Dot( edge3, edge4 );

	ScalarV t0 = Dot( edge0, edge1 );

	ScalarV area0	= Sqrt( Abs( Subtract( Scale(Scale(e2,e2), Scale(e3,e3)), Scale(a0,a0)) ) );
	ScalarV area1	= Sqrt( Abs( Subtract( Scale(Scale(e2,e2), Scale(e4,e4)), Scale(b0,b0)) ) );
	ScalarV area2	= Sqrt( Abs( Subtract( Scale(Scale(e3,e3), Scale(e4,e4)), Scale(c0,c0)) ) );

	ScalarV areaT	= Sqrt( Abs( Subtract( Scale(Scale(e0,e0), Scale(e1,e1)), Scale(t0,t0)) ) );		// whole triangle area

	w0	= Scale( area0, wHalf );
	w1	= Scale( area1, wHalf );
	w2	= Scale( area2, wHalf );

	ScalarV w3	= Scale( areaT, wHalf );

#if __PFDRAW
	static bool drawWeightedVtx = false;
	Color32 colorCue;
	if( drawWeightedVtx )
	{		
		if( IsGreaterThanAll( Add(Add(w0,w1),w2), Add(w3, errThreshold) ) != 0 )
			colorCue = Color_red;
		else
			colorCue = Color_yellow;
	}
#endif

	// ratios / barycentric coordinates / weights
	w0 /= w3;
	w1 /= w3;
	w2 /= w3;

	// NOTE: the sum of all weights should be less than 1.0f, otherwise the vtx is outside the triangle

	const Vec3V weightedVtx =	Scale( w2 , planeVtx0 ) + 
		Scale( w1 , planeVtx1 ) + 
		Scale( w0 , planeVtx2 ) + 
		Scale( d ,  n );


#if __PFDRAW
	if( drawWeightedVtx )
	{
//		PF_DRAW_SPHERE_COLOR(MorphTemp, 0.03f, VEC3V_TO_VECTOR3(weightedVtx), colorCue);
	}
#endif

	return weightedVtx;
}


void phMorphController::CreateMorphMap( const int index, const int level, const int vertexCount, phMapData::enWEIGHT_LEVEL wLevel )
{
	Assert( wLevel );
	Assert( m_MapData[index] );
	m_MapData[index]->m_MMap[level].Alloc( vertexCount * wLevel );
}


#if __PFDRAW
void phMorphController::DrawMapping(int mapIndex, int levelIndex, Vector3* /*vertexStream*/ )
{
//	Assert( vertexStream );
//	float radius = 0.03f;
	static bool drawVerts = false;
	if( drawVerts )
	{
		int vtxCount = GetIndexMapCount(mapIndex, levelIndex);
		Assert( vtxCount > 0 );

// 		u16* map = GetMap( mapIndex, levelIndex );
// 		Assert( map );

		for( int i = 0; i < vtxCount; ++i )
		{
//			PF_DRAW_SPHERE_COLOR(MorphTemp, radius, vertexStream[ map[i] ], Color_red3 );
		}
	}	
}
#endif


void phMorphController::FindCoordinates( int mapIndex, int vertexIndexHi, Vec3V* RESTRICT vtxHi, int indexLow, Vec3V* RESTRICT vectorStream, phMapData::enWEIGHT_LEVEL wLevel )
{	
	Assert( vectorStream );
	Assert( m_MapData[ mapIndex ] );
	Assert( m_MapData[indexLow] );

	AssertMsg( wLevel, "Make sure the correct weight level is passed, no 0 allowed !" );

	ScalarV lastError, distError;

	lastError.Set( THRESHOLD_FLOAT );
	distError.Set( THRESHOLD_FLOAT );

/*	u16 closestVtxIndex = 9999;*/
	
	const Vec4V vZero(V_ZERO);

	int sCount = 0;
	const int MAX_SELECTION = 32;
	phMorphDataAoS newData[MAX_SELECTION];
	float sError[MAX_SELECTION]; 

	
	for( int i = 0; i < MAX_SELECTION; ++i)
	{
		sError[i]			= 999.0f;
		newData[i].weights	= vZero;
		newData[i].vtxIndex	= (u16)(-1);
	}

	for( int i = 0; i < m_MapData[indexLow]->m_PolyCount ; ++i)
	{
		u16 PolyIndex = static_cast<u16>(i);	
		phMorphDataAoS mData;

		// !!! IMPORTANT: Normal is not necessary but the correct winding order has to be guaranteed by the indices !!!

		// TODO: I assume the indices are consecutive, and only triangles exist as polies
		static u16 PolyIndexStride = 3;

		u16 indexOffset = PolyIndex * PolyIndexStride;

		mData.index0 = m_MapData[indexLow]->m_IMap[indexLow].m_Data[ indexOffset ];
		mData.index1 = m_MapData[indexLow]->m_IMap[indexLow].m_Data[ indexOffset + 1 ];
		mData.index2 = m_MapData[indexLow]->m_IMap[indexLow].m_Data[ indexOffset + 2 ];

		const Vec3V planeVtx0 = vectorStream[ mData.index0 ];
		const Vec3V planeVtx1 = vectorStream[ mData.index1 ];
		const Vec3V planeVtx2 = vectorStream[ mData.index2 ];

		// !!! IMPORTANT: Normal is not necessary but the correct winding order has to be guaranteed by the indices !!!
//-------------
// TODO: work in progress
		// find closest vertex ...

		Vec3V distV = Subtract( planeVtx0, (*vtxHi) );
		ScalarV distVSq = Dot( distV, distV );
		if( distVSq.Getf() < distError.Getf() )
		{
			distError = distVSq;
/*			closestVtxIndex = mData.index0;*/
		}

		distV = Subtract( planeVtx1, (*vtxHi) );
		distVSq = Dot( distV, distV );
		if( distVSq.Getf() < distError.Getf() )
		{
			distError = distVSq;
/*			closestVtxIndex = mData.index1;*/
		}


		distV = Subtract( planeVtx2, (*vtxHi) );
		distVSq = Dot( distV, distV );
		if( distVSq.Getf() < distError.Getf() )
		{
			distError = distVSq;
/*			closestVtxIndex = mData.index2;*/
		}


//-------------


		ScalarV d, w0, w1, w2;
		Vec3V n;

		const Vec3V projectedVtx = FindProjectedVtx( (*vtxHi), planeVtx0, planeVtx1, planeVtx2, n, d );
		const Vec3V weightedVtx = FindWeightedVtx( projectedVtx, planeVtx0, planeVtx1, planeVtx2, n, d, w0, w1, w2);

		Vec3V errThresholdV = Subtract( projectedVtx, weightedVtx );
		ScalarV err = Dot( errThresholdV, errThresholdV );


		// check against square something
//		static float weightError = -0.001f;		
		
		// give some room for verts that fall outside the bound of the higher res mesh ... otherwise those verts will not map to any triangle
		static float errThresholdF = 1.4f;
		const ScalarV errThreshold = ScalarVFromF32( errThresholdF );
		if( IsLessThanAll( Add(w0, Add(w1,w2) ), errThreshold ) != 0 )		
		{
// TODO: figure out what effect w2 clamp has on the quality
			ScalarV w2clamped = Subtract( ScalarV(V_ONE), Add(w0,w1) );

			mData.weights.SetX( w0 );
			mData.weights.SetY( w1 );
			mData.weights.SetZ( w2clamped /*w2*/ );

			newData[sCount].vtxIndex = static_cast<u16>(vertexIndexHi);
			
			newData[sCount].index0	= mData.index0;
			newData[sCount].index1	= mData.index1;
			newData[sCount].index2	= mData.index2;

			newData[sCount].weights = mData.weights;
			newData[sCount].weights.SetW( d );

			sError[sCount] = err.Getf();

			sCount++;
			AssertMsg( sCount < MAX_SELECTION, "too many matching vertices have been found ?!" );
		}



		// TODO: 
		// 1. for vertices that don't have any good match use the closest vertex and the respective normal on that vertex
		// 2. keep a sign (positive or negative respectively) if the vertex is on one or the other side of the plane
		// Need to test with more complex meshes

	}

	if (sCount <= 0)
	{
		Quitf( ERR_GFX_MORPH_1, "No match in lower lod has been found for the vertex, game will crash with these assets. Art need to check environment cloth LODs size and allignment !" );
//		AssertMsg(sCount > 0, "No match in lower lod has been found for the vertex.");
	}
	else
	{
		bool bubble = false;
		do
		{
			bubble = false;
			for( int i = 0; i < sCount-1; ++i )
			{
				if( sError[i] > sError[i+1] )
				{
					float tempErr = sError[i+1];
					sError[i+1] = sError[i];
					sError[i] = tempErr;

					phMorphDataAoS tempMData = newData[i+1];
					newData[i+1] = newData[i];
					newData[i] = tempMData;

					bubble = true;
				}
			}

		} while ( bubble );
	}

	const int vtxFinalIndex = vertexIndexHi * wLevel;

	phMorphData& morphData = m_MapData[ mapIndex ]->m_MMap[indexLow];

	Assert( morphData.weights.m_Data.GetCount() );
	Assert( morphData.vtxIndex.m_Data.GetCount() );
	Assert( morphData.index0.m_Data.GetCount() );
	Assert( morphData.index1.m_Data.GetCount() );
	Assert( morphData.index2.m_Data.GetCount() );

	for( int i = 0; i < (int)wLevel; ++i)
	{		
		const int k = vtxFinalIndex + i;
		morphData.weights[k]	= newData[i].weights;
		morphData.vtxIndex[k]	= newData[i].vtxIndex;
		morphData.index0[k]		= newData[i].index0;
		morphData.index1[k]		= newData[i].index1;
		morphData.index2[k]		= newData[i].index2;
	}

	// NOTE: every vertex should have been associated with a poly   ?!?

// TODO: work in progress - use closest vertex
// 	if( 0 /*lastError.Getf() > (THRESHOLD_FLOAT - 1.0f)*/ )
// 	{
// 		finalData->index0	= closestVtxIndex;
// 		finalData->index1	= 0;
// 		finalData->index2	= 0;
// 
// 		finalData->weights = vZero;
// 		finalData->weights.SetYf( 1.0f );
// 	}

}



void phMorphController::CreateDeformMapping( const int numVerticesMeshHi, Vec3V* RESTRICT vecStreamMeshHi, Vec3V* RESTRICT vecStreamMeshLow, int mapperIndex, int mapToLevel, phMapData::enWEIGHT_LEVEL wLevel )
{
	Assert( vecStreamMeshHi );
	Assert( vecStreamMeshLow );

	CreateMorphMap( mapperIndex, mapToLevel, numVerticesMeshHi, wLevel );
	Assert( m_MapData[mapperIndex] );

	for(int i = 0; i < numVerticesMeshHi; ++i )
	{
		FindCoordinates( mapperIndex, i, &vecStreamMeshHi[i] , mapToLevel, vecStreamMeshLow, wLevel );
	}

}

void phMorphController::SwapMorphData( const int oldIndex, const int newIndex, int mapperIndex, int mapToLevel, phMapData::enWEIGHT_LEVEL /*wLevel*/ )
{
	Assert( m_MapData[mapperIndex] );
	m_MapData[mapperIndex]->m_MMap[ mapToLevel ].Swap( oldIndex, newIndex );
}


void phMorphController::CreateDirectMapping( const int meshVerts, const int meshverts2, Vec3V* mesh, Vec3V* RESTRICT streamToSearchInto, int mapIndex, int levelIndex, u16* indexMap )
{
	Assert( mesh );
	Assert( streamToSearchInto );

	CreateIndexMap( mapIndex, levelIndex, meshVerts );
	Assert( m_MapData[mapIndex] );

	for(int i = 0; i < meshVerts; ++i )
	{
		m_MapData[mapIndex]->m_IMap[ levelIndex ].m_Data[i] = FindIndex( mesh[i], streamToSearchInto, meshverts2, indexMap );
	}
}


u16 phMorphController::FindIndex( Vec3V_In vtx, Vec3V* RESTRICT vectorStream, int vtxCount, u16* indexMap )
{	
	Assert( vectorStream );

	ScalarV lastError;
	lastError.Set( THRESHOLD_FLOAT );

	u16 result = 9999;
	for( int i = 0; i < vtxCount; ++i)
	{
		const Vec3V v = vectorStream[ indexMap[i] ];
		const Vec3V v1 = Subtract( vtx, v );

		ScalarV d = Dot( v1, v1 );
		if( d.Getf() < lastError.Getf() )			
		{				
			result = (u16)indexMap[i];
			lastError = d;
		}
	}

	// every vertex should have been associated with a poly
	if( result >= 9999 )
	{
		Quitf(ERR_GFX_MORPH_2,"Cloth lod meshes are not alligned, please fix those in MAX !");
	}
	return result;
}


#if __PFDRAW
void phMorphController::DrawNormals( int lodIndex, Vector3* RESTRICT vecStream )
{
	static bool drawNormals = false;
	if( drawNormals )
	{
		ScalarV scaleHalf(V_HALF);
		Assert( m_MapData[lodIndex] );

		for( int i = 0; i < m_MapData[lodIndex]->m_PolyCount; ++i)
		{
			const Vec3V vtxLow0 = VECTOR3_TO_VEC3V( vecStream[ m_MapData[lodIndex]->m_IMap[lodIndex].m_Data[i*3] ] );
			const Vec3V vtxLow1 = VECTOR3_TO_VEC3V( vecStream[ m_MapData[lodIndex]->m_IMap[lodIndex].m_Data[i*3+1] ] );
			const Vec3V vtxLow2 = VECTOR3_TO_VEC3V( vecStream[ m_MapData[lodIndex]->m_IMap[lodIndex].m_Data[i*3+2] ] );

			// find the normal
			const Vec3V v1 = Subtract( vtxLow0, vtxLow1 );
			const Vec3V v2 = Subtract( vtxLow2, vtxLow1 );
			Vec3V n = Cross( v1, v2 );
			n = Normalize( n );
			n = Scale( scaleHalf, n );


#if __PFDRAW
//			float radius = 0.03f;
			static bool drawVerts = true;	
			//			if( drawVerts && control == i )
			if( drawVerts )
			{
// 				PF_DRAW_SPHERE_COLOR(MorphTemp, radius, VEC3V_TO_VECTOR3(vtxLow0), Color_red3);
// 				PF_DRAW_SPHERE_COLOR(MorphTemp, radius, VEC3V_TO_VECTOR3(vtxLow1), Color_red3);
// 				PF_DRAW_SPHERE_COLOR(MorphTemp, radius, VEC3V_TO_VECTOR3(vtxLow2), Color_red3);
// 
// 				const Vector3 atVec = VEC3V_TO_VECTOR3(vtxLow0) + VEC3V_TO_VECTOR3(n);
// 				PF_DRAW_LINE_COLOR(MorphTemp, VEC3V_TO_VECTOR3(vtxLow0), atVec, Color_yellow);
			}	
#endif
		}
	}
}
#endif


void phMorphController::ClearW( const int vertsCount, Vec3V* RESTRICT stream )
{
	Assert( stream );
	ScalarV __zero = ScalarVFromF32( MAGIC_ZERO );

//	PrefetchDC( stream );
	const int verts8 = (vertsCount >> 3) << 3;
	int i;
	for( i = 0; i < verts8; i += 8 )
	{
//		PrefetchDC( &stream[i+8] );
		stream[i  ].SetW( __zero );
		stream[i+1].SetW( __zero );
		stream[i+2].SetW( __zero );
		stream[i+3].SetW( __zero );
		stream[i+4].SetW( __zero );
		stream[i+5].SetW( __zero );
		stream[i+6].SetW( __zero );
		stream[i+7].SetW( __zero );
	}
	for(  ;i < vertsCount; ++i )
	{
		stream[i].SetW( __zero );
	}
}


void phMorphController::MorphLevelONE( int hiLOD, int lowLOD, Vec3V* RESTRICT hiLODStream, Vec3V* RESTRICT lowLODStream )
{
	const int hiLODvertsCount = GetMorphMapCount(hiLOD, lowLOD);
	const phMorphData& morphData = m_MapData[hiLOD]->m_MMap[lowLOD];
	int i = 0;
	const Vec4V* RESTRICT weightsStream = morphData.weights.m_Data.GetElements();
	const u16* RESTRICT vtxIndexStream  = morphData.vtxIndex.m_Data.GetElements();
	const u16* RESTRICT idx0Stream = morphData.index0.m_Data.GetElements();
	const u16* RESTRICT idx1Stream = morphData.index1.m_Data.GetElements();
	const u16* RESTRICT idx2Stream = morphData.index2.m_Data.GetElements();

#if 1

	const int count4 = (hiLODvertsCount >> 2) << 2;
//	const int count8 = (hiLODvertsCount >> 3) << 3;
//	for( ; i < count8; i += 8)
	for( ; i < count4; i += 4)
	{
// 		PrefetchDC( &weightsStream[i+8] );
// 		PrefetchDC( &vtxIndexStream[i+8] );
// 		PrefetchDC( &idx0Stream[i+8] );
// 		PrefetchDC( &idx1Stream[i+8] );
// 		PrefetchDC( &idx2Stream[i+8] );

		const int	i0	= i,
					i1	= i + 1,
					i2	= i + 2,
					i3	= i + 3;
// 					i4	= i + 4,
// 					i5	= i + 5,
// 					i6	= i + 6,
// 					i7	= i + 7;

		Vec4V weights0 = weightsStream[i0];
		Vec4V weights1 = weightsStream[i1];
		Vec4V weights2 = weightsStream[i2];
		Vec4V weights3 = weightsStream[i3];
// 		Vec4V weights4 = weightsStream[i4];
// 		Vec4V weights5 = weightsStream[i5];
// 		Vec4V weights6 = weightsStream[i6];
// 		Vec4V weights7 = weightsStream[i7];

		SoA_Vec4V soa_weights_0123;
		ToSoA( soa_weights_0123, weights0, weights1, weights2, weights3 );
//		SoA_Vec4V soa_weights_4567
//		ToSoA( soa_weights_4567, weights4, weights5, weights6, weights7 );

		SoA_ScalarV soa_w0_0123 = soa_weights_0123.GetX();
		SoA_ScalarV soa_w1_0123 = soa_weights_0123.GetY();
		SoA_ScalarV soa_w2_0123 = soa_weights_0123.GetZ();
		SoA_ScalarV soa_d_0123  = soa_weights_0123.GetW();
// 		SoA_ScalarV soa_w0_4567 = soa_weights_4567.GetX();
// 		SoA_ScalarV soa_w1_4567 = soa_weights_4567.GetY();
// 		SoA_ScalarV soa_w2_4567 = soa_weights_4567.GetZ();
// 		SoA_ScalarV soa_d_4567  = soa_weights_4567.GetW();

		const u16 idx00 = idx0Stream[i0];
		const u16 idx01 = idx0Stream[i1];
		const u16 idx02 = idx0Stream[i2];
		const u16 idx03 = idx0Stream[i3];
// 		const u16 idx04 = idx0Stream[i4];
// 		const u16 idx05 = idx0Stream[i5];
// 		const u16 idx06 = idx0Stream[i6];
// 		const u16 idx07 = idx0Stream[i7];

		const u16 idx10 = idx1Stream[i0];
		const u16 idx11 = idx1Stream[i1];
		const u16 idx12 = idx1Stream[i2];
		const u16 idx13 = idx1Stream[i3];
// 		const u16 idx14 = idx1Stream[i4];
// 		const u16 idx15 = idx1Stream[i5];
// 		const u16 idx16 = idx1Stream[i6];
// 		const u16 idx17 = idx1Stream[i7];

		const u16 idx20 = idx2Stream[i0];
		const u16 idx21 = idx2Stream[i1];
		const u16 idx22 = idx2Stream[i2];
		const u16 idx23 = idx2Stream[i3];
// 		const u16 idx24 = idx2Stream[i4];
// 		const u16 idx25 = idx2Stream[i5];
// 		const u16 idx26 = idx2Stream[i6];
// 		const u16 idx27 = idx2Stream[i7];

		Vec3V vtxLow00 = lowLODStream[ idx00 ];
		Vec3V vtxLow01 = lowLODStream[ idx01 ];
		Vec3V vtxLow02 = lowLODStream[ idx02 ];
		Vec3V vtxLow03 = lowLODStream[ idx03 ];
// 		Vec3V vtxLow04 = lowLODStream[ idx04 ];
// 		Vec3V vtxLow05 = lowLODStream[ idx05 ];
// 		Vec3V vtxLow06 = lowLODStream[ idx06 ];
// 		Vec3V vtxLow07 = lowLODStream[ idx07 ];

		Vec3V vtxLow10 = lowLODStream[ idx10 ];
		Vec3V vtxLow11 = lowLODStream[ idx11 ];
		Vec3V vtxLow12 = lowLODStream[ idx12 ];
		Vec3V vtxLow13 = lowLODStream[ idx13 ];
// 		Vec3V vtxLow14 = lowLODStream[ idx14 ];
// 		Vec3V vtxLow15 = lowLODStream[ idx15 ];
// 		Vec3V vtxLow16 = lowLODStream[ idx16 ];
// 		Vec3V vtxLow17 = lowLODStream[ idx17 ];

		Vec3V vtxLow20 = lowLODStream[ idx20 ];
		Vec3V vtxLow21 = lowLODStream[ idx21 ];
		Vec3V vtxLow22 = lowLODStream[ idx22 ];
		Vec3V vtxLow23 = lowLODStream[ idx23 ];
// 		Vec3V vtxLow24 = lowLODStream[ idx24 ];
// 		Vec3V vtxLow25 = lowLODStream[ idx25 ];
// 		Vec3V vtxLow26 = lowLODStream[ idx26 ];
// 		Vec3V vtxLow27 = lowLODStream[ idx27 ];

		SoA_Vec3V soa_vtxLow0_0123, soa_vtxLow1_0123, soa_vtxLow2_0123;
		ToSoA( soa_vtxLow0_0123, vtxLow00, vtxLow01, vtxLow02, vtxLow03 );
		ToSoA( soa_vtxLow1_0123, vtxLow10, vtxLow11, vtxLow12, vtxLow13 );
		ToSoA( soa_vtxLow2_0123, vtxLow20, vtxLow21, vtxLow22, vtxLow23 );

// 		SoA_Vec3V soa_vtxLow0_4567, soa_vtxLow1_4567, soa_vtxLow2_4567;
// 		ToSoA( soa_vtxLow0_4567, vtxLow04, vtxLow05, vtxLow06, vtxLow07 );
// 		ToSoA( soa_vtxLow1_4567, vtxLow14, vtxLow15, vtxLow16, vtxLow17 );
// 		ToSoA( soa_vtxLow2_4567, vtxLow24, vtxLow25, vtxLow26, vtxLow27 );

		SoA_Vec3V soa_v1_0123, soa_v2_0123;
		Subtract( soa_v1_0123, soa_vtxLow0_0123, soa_vtxLow1_0123 );
		Subtract( soa_v2_0123, soa_vtxLow2_0123, soa_vtxLow1_0123 );

// 		SoA_Vec3V soa_v1_4567, soa_v2_4567;
// 		Subtract( soa_v1_4567, soa_vtxLow0_4567, soa_vtxLow1_4567 );
// 		Subtract( soa_v2_4567, soa_vtxLow2_4567, soa_vtxLow1_4567 );

		SoA_Vec3V soa_crossTemp_0123;
		Cross(soa_crossTemp_0123, soa_v1_0123, soa_v2_0123);
//		SoA_Vec3V soa_crossTemp_4567;
//		Cross(soa_crossTemp_4567, soa_v1_4567, soa_v2_4567);

		SoA_Vec3V soa_n0123;
		NormalizeFast( soa_n0123, soa_crossTemp_0123 );
//		SoA_Vec3V soa_n4567;
//		NormalizeFast( soa_n4567, soa_crossTemp_4567 );

		SoA_Vec3V soa_w2_0123_vtx0123, soa_w1_0123_vtx0123, soa_w0_0123_vtx0123, soa_d_0123_n0123;
		Scale( soa_w2_0123_vtx0123, soa_vtxLow0_0123, soa_w2_0123 );
		Scale( soa_w1_0123_vtx0123, soa_vtxLow1_0123, soa_w1_0123 );
		Scale( soa_w0_0123_vtx0123, soa_vtxLow2_0123, soa_w0_0123 );
		Scale( soa_d_0123_n0123, soa_n0123, soa_d_0123 );

// 		SoA_Vec3V soa_w2_4567_vtx4567, soa_w1_4567_vtx4567, soa_w0_4567_vtx4567, soa_d_4567_n4567;
// 		Scale( soa_w2_4567_vtx4567, soa_vtxLow0_4567, soa_w2_4567 );
// 		Scale( soa_w1_4567_vtx4567, soa_vtxLow1_4567, soa_w1_4567 );
// 		Scale( soa_w0_4567_vtx4567, soa_vtxLow2_4567, soa_w0_4567 );
// 		Scale( soa_d_4567_n4567, soa_n4567, soa_d_4567 );

		SoA_Vec3V soa_temp0_0123, soa_temp1_0123;
		Add( soa_temp0_0123, soa_w2_0123_vtx0123, soa_w1_0123_vtx0123 );
		Add( soa_temp1_0123, soa_w0_0123_vtx0123, soa_d_0123_n0123 );
// 		SoA_Vec3V soa_temp0_4567, soa_temp1_4567;
// 		Add( soa_temp0_4567, soa_w2_4567_vtx4567, soa_w1_4567_vtx4567 );
// 		Add( soa_temp1_4567, soa_w0_4567_vtx4567, soa_d_4567_n4567 );

		SoA_Vec3V soa_hiLODStream4_0123;
		Add( soa_hiLODStream4_0123, soa_temp0_0123, soa_temp1_0123 );
//		SoA_Vec3V soa_hiLODStream4_4567;
//		Add( soa_hiLODStream4_4567, soa_temp0_4567, soa_temp1_4567 );

		Vec3V v0, v1, v2, v3;
		ToAoS( v0, v1, v2, v3, soa_hiLODStream4_0123 );
//		Vec3V v4, v5, v6, v7;
//		ToAoS( v4, v5, v6, v7, soa_hiLODStream4_4567 );

		hiLODStream[vtxIndexStream[i0]] = v0;
		hiLODStream[vtxIndexStream[i1]] = v1;
		hiLODStream[vtxIndexStream[i2]] = v2;
		hiLODStream[vtxIndexStream[i3]] = v3;
// 		hiLODStream[vtxIndexStream[i4]] = v4;
// 		hiLODStream[vtxIndexStream[i5]] = v5;
// 		hiLODStream[vtxIndexStream[i6]] = v6;
// 		hiLODStream[vtxIndexStream[i7]] = v7;
	}

#endif

	for( ; i < hiLODvertsCount; ++i )
	{
 		  Vec4V weights = weightsStream[i];
		u16 vtxIndex	= vtxIndexStream[i];
		u16 idx0		= idx0Stream[i];
		u16 idx1		= idx1Stream[i];
		u16 idx2		= idx2Stream[i];

		ScalarV w0 = weights.GetX();
		ScalarV w1 = weights.GetY();
		ScalarV w2 = weights.GetZ();
		ScalarV  d = weights.GetW();

		Vec3V vtxLow0 = lowLODStream[ idx0 ];
		Vec3V vtxLow1 = lowLODStream[ idx1 ];
		Vec3V vtxLow2 = lowLODStream[ idx2 ];

		Vec3V v1 = Subtract( vtxLow0, vtxLow1 );
		Vec3V v2 = Subtract( vtxLow2, vtxLow1 );
		Vec3V n = Normalize( Cross( v1, v2 ) );

 	#if __ASSERT	
// 		Assert( vtxIndex != 9999 && vtxIndex != ((u16)-1) );
//		Assert( vtxIndex < MMapCount );
 	#endif

		hiLODStream[vtxIndex] =	Scale( w2 , vtxLow0 ) + 
								Scale( w1 , vtxLow1 ) + 
								Scale( w0 , vtxLow2 ) + 
								Scale( d ,  n );
	} // end for
}



void phMorphController::MorphLevelTWO( int mapperIndex, int oldLodIndex, Vec3V* streamControlled, Vec3V* RESTRICT streamController )
{
	ScalarV halfScalarV(V_HALF);
	const int MMapCount = GetMorphMapCount(mapperIndex, oldLodIndex);
	const phMapData::enWEIGHT_LEVEL wLevel = phMapData::enWEIGHT_TWO;

	PrefetchDC( streamController );
	PrefetchDC( streamControlled );

	const phMorphData& morphData = m_MapData[mapperIndex]->m_MMap[oldLodIndex];

	for( int i = 0; i < MMapCount; ++i)
	{
		PrefetchDC( &streamControlled[i+1] );

		const int k = i * wLevel;
		const int k1 = k+1;
		
		const Vec4V weights = morphData.weights.m_Data[k];
		const Vec4V weights1= morphData.weights.m_Data[k1];
		const u16 vtxIndex	= morphData.vtxIndex.m_Data[k];

		const u16 idx0_0	= morphData.index0.m_Data[k];
		const u16 idx1_0	= morphData.index1.m_Data[k];
		const u16 idx2_0	= morphData.index2.m_Data[k];

		const u16 idx0_1	= morphData.index0.m_Data[k1];
		const u16 idx1_1	= morphData.index1.m_Data[k1];
		const u16 idx2_1	= morphData.index2.m_Data[k1];

		const ScalarV w0 = weights.GetX();
		const ScalarV w1 = weights.GetY();
		const ScalarV w2 = weights.GetZ();
		const ScalarV  d = weights.GetW();

		const ScalarV w0_1 = weights1.GetX();
		const ScalarV w1_1 = weights1.GetY();
		const ScalarV w2_1 = weights1.GetZ();
		const ScalarV  d_1 = weights1.GetW();

		const Vec3V vtxLow0 = streamController[ idx0_0];
		const Vec3V vtxLow1 = streamController[ idx1_0 ];
		const Vec3V vtxLow2 = streamController[ idx2_0 ];

		const Vec3V vtxLow0_1 = streamController[ idx0_1 ];
		const Vec3V vtxLow1_1 = streamController[ idx1_1 ];
		const Vec3V vtxLow2_1 = streamController[ idx2_1 ];

		// Find the unit normal vector, perpendicular to the plane containing the three points
		const Vec3V v1 = Subtract( vtxLow0, vtxLow1 );
		const Vec3V v2 = Subtract( vtxLow2, vtxLow1 );
		const Vec3V n = Normalize( Cross( v1, v2 ) );

		const Vec3V v1_1 = Subtract( vtxLow0_1, vtxLow1_1 );
		const Vec3V v2_1 = Subtract( vtxLow2_1, vtxLow1_1 );
		const Vec3V n_1 = Normalize( Cross( v1_1, v2_1 ) );

		const Vec3V projV	=	Scale( w2 , vtxLow0 ) + Scale( w1 , vtxLow1 ) + Scale( w0 , vtxLow2 );
		const Vec3V projV_1 =	Scale( w2_1 , vtxLow0_1 ) + Scale( w1_1 , vtxLow1_1 ) + Scale( w0_1 , vtxLow2_1 );

		const Vec3V v =	projV + Scale( d ,  n ) + projV_1 + Scale( d_1 ,  n_1 );
		streamControlled[vtxIndex] = Scale( halfScalarV, v );
	} // end for
}


void phMorphController::Morph( int hiLOW, int lowLOD, Vec3V* RESTRICT hiLODStream, Vec3V* RESTRICT lowLODStream, phMapData::enWEIGHT_LEVEL /*wLevel*/, phMapData::enWEIGHT_LEVEL /*wLevel2*/ )
{
	if( hiLOW == lowLOD )
		return;							// direct mapping is used

	Assert( hiLODStream );
	Assert( lowLODStream );
	Assert( m_MapData[lowLOD] );
	Assert( m_MapData[hiLOW] );
//	Assert( wLevel == phMapData::enWEIGHT_ONE );
//	if( wLevel == phMapData::enWEIGHT_ONE )
	{
		MorphLevelONE( hiLOW, lowLOD, hiLODStream, lowLODStream );
	}
/*	// TODO: disable it for now
	else if( phMapData::enWEIGHT_TWO )
	{
		MorphLevelTWO( hiLOW, lowLOD, hiLODStream, lowLODStream );
	}
	else
	{
		Assert(0); 		// invalid morph level ?!
	}
*/
}


// phMorphData

void phMorphData::Alloc( const int vtxCount )
{
	weights.m_Data.Resize( vtxCount );
	vtxIndex.m_Data.Resize( vtxCount );
	index0.m_Data.Resize( vtxCount );
	index1.m_Data.Resize( vtxCount );
	index2.m_Data.Resize( vtxCount );
}	

void phMorphData::Swap( const int oldIndex, const int newIndex )
{
	Vec4V tempWeight	= weights[oldIndex];
	u16	tempVtxIndex	= vtxIndex[oldIndex];
	u16	tempIndex0		= index0[oldIndex];
	u16	tempIndex1		= index1[oldIndex];
	u16	tempIndex2		= index2[oldIndex];

	weights[oldIndex]	= weights[newIndex];
	vtxIndex[oldIndex]	= vtxIndex[newIndex];
	index0[oldIndex]	= index0[newIndex];
	index1[oldIndex]	= index1[newIndex];
	index2[oldIndex]	= index2[newIndex];

	weights[newIndex]	= tempWeight;
	vtxIndex[newIndex]	= tempVtxIndex;
	index0[newIndex]	= tempIndex0;
	index1[newIndex]	= tempIndex1;
	index2[newIndex]	= tempIndex2;
}


#if __DECLARESTRUCT
void phMorphData::DeclareStruct(datTypeStruct &s)
{
	SSTRUCT_BEGIN(phMorphData)
		SSTRUCT_FIELD(phMorphData,weights)
		SSTRUCT_FIELD(phMorphData,vtxIndex)
		SSTRUCT_FIELD(phMorphData,index0)
		SSTRUCT_FIELD(phMorphData,index1)
		SSTRUCT_FIELD(phMorphData,index2)
	SSTRUCT_END(phMorphData)
}
#endif


}
