// 
// charactercloth/charactercloth.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cloth/charactercloth.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "diag/tracker.h"
#include "file/token.h"
#include "grcore/texture.h"
#include "grmodel/geometry.h"
#include "grmodel/matrixset.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "parser/restparserservices.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/memory.h"
#if __RESOURCECOMPILER
#include "paging/rscbuilder.h"
#endif

#include "charactercloth_parser.h"

#if !__SPU && !__FINAL && !__RESOURCECOMPILER
XPARAM(clothxmlformat);
#endif

namespace rage
{

#if MESH_LIBRARY
	bool characterCloth::typehascloth = false;
	int s_BoundsCount = 0;
#endif


characterCloth::characterCloth()
	: m_BoundComposite(NULL)
	, m_ParentMatrix(Mat34V(V_IDENTITY))
	, m_BoundCompositeRef(NULL)
	, m_CharacterClothType(NULL)
	, m_EntityParentMatrixPtr(NULL)
	, m_EntityVerticesPtr(NULL)
	, m_IsActive( true )
	, m_NegateForce( false )
	, m_LockCounter(0)
{
}

#if !__SPU
const char* characterCloth::sm_MetaDataFileAppendix = "_poses";

#if __BANK && !__RESOURCECOMPILER
void characterCloth::AddWidgets(bkGroup* pRagGroup)
{
	Assert( pRagGroup );
	pRagGroup->AddButton("Load Verts", datCallback( MFA(characterCloth::LoadVerts), this ) );
	pRagGroup->AddButton("Save Verts", datCallback( MFA(characterCloth::SaveVerts), this ) );
	pRagGroup->AddButton("Save Pose", datCallback( MFA(characterCloth::SavePose), this ) );
}
#endif // __BANK && !__RESOURCECOMPILER

void characterCloth::LoadVerts()
{	
	Assert( m_ClothController.ptr && m_ClothController->GetCloth(0) );
	phClothData& clothData = m_ClothController->GetCloth(0)->GetClothData();
	clothData.Load( phClothData::sm_tuneFilePath, m_ClothController->GetName() );
}

void characterCloth::SaveVerts()
{
#if !__FINAL
	Assert( m_ClothController.ptr && m_ClothController->GetCloth(0) );
	phClothData& clothData = m_ClothController->GetCloth(0)->GetClothData();
	clothData.Save( const_cast<char*>(m_ClothController->GetName()) );
#endif
}

void characterCloth::SavePose()
{
#if !__FINAL

	sysMemStartTemp();
	Assert( m_Poses.GetCount() == 0 );

	Assert( m_ClothController.ptr );
	phVerletCloth* pCloth = m_ClothController->GetCloth(0);
	Assert( pCloth );
	Vec3V* RESTRICT pClothVerts = pCloth->GetClothData().GetVertexPointer();
	Assert( pClothVerts );

	const int numVerts = pCloth->GetNumVertices();
	const int totalNumVertsInPose = numVerts * 2;
	m_Poses.Resize( totalNumVertsInPose );

	for(int i = 0; i < numVerts; ++i)
	{
		m_Poses[i*2] = pClothVerts[i];
		m_Poses[i*2+1].SetIntrin128(  UnpackV1010102( pClothVerts[i].GetIntrin128() ) );
	}

	sysMemEndTemp();

	char buff[256];
	formatf( buff, "%s%s%s", phClothData::sm_tuneFilePath, m_ClothController->GetName(), characterCloth::sm_MetaDataFileAppendix );

#if !__RESOURCECOMPILER
	int xmlIdxType = !PARAM_clothxmlformat.Get(xmlIdxType) ? 1: 0;
	if( !xmlIdxType )
		AssertVerify(PARSER.SaveObject( buff, "xml", (characterClothDebug*)this, parManager::XML));
	else
#endif
		AssertVerify(PARSER.SaveObject( buff, "xml", this, parManager::XML));

#if !__RESOURCECOMPILER
	const char* xmlDesc[] = { "Vec3V format", "U32 format" };
	clothDebugf1("Saved character cloth pose to file(%s): %s", xmlDesc[xmlIdxType], buff);
#endif

	sysMemStartTemp();
	m_Poses.Reset();
	sysMemEndTemp();

#endif // !__FINAL
}

void characterCloth::LoadPoses(const char* pFilePath, const char* pCharacterName)
{
	Assert( pCharacterName );
	Assert( pFilePath );

	char buff[256];
	char nameBuff[128];
	formatf( nameBuff, "%s_%s", pCharacterName, (char*)m_ClothController->GetName() );
	formatf( buff, "%s%s%s", pFilePath, nameBuff, characterCloth::sm_MetaDataFileAppendix );

	parSettings s = parSettings::sm_StrictSettings; 
	s.SetFlag(parSettings::USE_TEMPORARY_HEAP, true); 

#if !__FINAL && !__RESOURCECOMPILER
	int xmlIdxType = !PARAM_clothxmlformat.Get(xmlIdxType) ? 1: 0;
	if( !xmlIdxType )
		PARSER.LoadObject( buff, "xml", *(characterClothDebug*)this, &s);
	else
#endif
		PARSER.LoadObject( buff, "xml", *this, &s);

#if !__FINAL && !__RESOURCECOMPILER
	const char* xmlDesc[] = { "Vec3V format", "U32 format" };
	clothDebugf1("Loaded character cloth poses from file(%s): %s", xmlDesc[xmlIdxType], buff);
#endif
}

void characterCloth::LoadFromFile(const char* filePath )
{
	Assert( m_ClothController );
	char buff[256];
	formatf( buff, "%s%s%s", filePath, m_ClothController->GetName(), "_ccloth" );

	parSettings s = parSettings::sm_StrictSettings; 
	s.SetFlag(parSettings::USE_TEMPORARY_HEAP, true); 
	PARSER.LoadObject( buff, "xml", *this, &s);

	clothDebugf1("Loaded character cloth from file: %s", buff);
}
void characterCloth::SaveToFile(
#if __FINAL
	const char*
#else
	const char* filePath
#endif
	)
{
#if !__FINAL
	Assert( m_ClothController );
	char buff[256];
	formatf( buff, "%s%s%s", filePath, m_ClothController->GetName(), "_ccloth" );
	AssertVerify(PARSER.SaveObject( buff, "xml", m_CharacterClothType.ptr /*this*/, parManager::XML));

	clothDebugf1("Saved character cloth to file: %s", buff);
#endif
}

void characterCloth::LoadAll()
{
	LoadFromFile();
	m_ClothController->Load();
	((clothController*)m_ClothController)->Load();

	char* pCCName = const_cast<char*>(m_ClothController->GetName());
	Assert( pCCName );
	phVerletCloth* pCloth = m_ClothController->GetCloth(0);
	Assert( pCloth );
	pCloth->Load( pCCName );

	phClothData& clothData = pCloth->GetClothData();	
	clothData.Load( phClothData::sm_tuneFilePath, pCCName );
}
void characterCloth::SaveAll()
{
	SaveToFile();
	m_ClothController->Save();
	((clothController*)m_ClothController)->Save();

	char* pCCName = const_cast<char*>(m_ClothController->GetName());
	Assert( pCCName );
	phVerletCloth* pCloth = m_ClothController->GetCloth(0);
	Assert( pCloth );
	pCloth->Save( pCCName );
	
	phClothData& clothData = pCloth->GetClothData();	
	clothData.Save( pCCName );
}

#if __BANK && !__RESOURCECOMPILER
characterCloth* s_CurrentCCloth = NULL;
bool s_RegisterCClothRestInterface = false;

void characterCloth::RegisterRestInterface(const char* controllerName)
{
	if( !s_RegisterCClothRestInterface )
	{		
		s_RegisterCClothRestInterface = true;
		s_CurrentCCloth = const_cast<characterCloth*>(/*m_CharacterClothType ? m_CharacterClothType:*/ this);
		parRestRegisterSingleton("Physics/Cloth/CharCloth", *s_CurrentCCloth, NULL);

		m_ClothController->RegisterRestInterface(controllerName);

		clothDebugf1("Registered Physics/Cloth/CharCloth REST interface for: %s", controllerName);
	}
}

void characterCloth::UnregisterRestInterface(const char* controllerName)
{
	if( s_RegisterCClothRestInterface )
	{	
		m_ClothController->UnregisterRestInterface(controllerName);

		s_RegisterCClothRestInterface = false;
		s_CurrentCCloth = NULL;
		REST.RemoveAndDeleteService("Physics/Cloth/CharCloth");

		clothDebugf1("Unregistered Physics/Cloth/CharCloth REST interface for: %s", controllerName);
	}
}
#endif // __BANK && !__RESOURCECOMPILER

#endif // !__SPU

#if CLOTH_INSTANCE_FROM_DATA
void characterCloth::InstanceFromData(int vertsCapacity, int edgesCapacity, int boneIDsCapacity)
{
	m_ClothController = rage_new characterClothController();

	clothBridgeSimGfx* pClothBridge = m_ClothController->CreateBridge();
	Assert( pClothBridge );
	pClothBridge->InstanceFromData(vertsCapacity);

	m_ClothController->SetLOD( 0 );
	m_ClothController->SetOwner( this );

	m_ClothController->InstanceFromData(vertsCapacity, edgesCapacity, boneIDsCapacity);	

	m_BoneIndex.Resize( boneIDsCapacity );	
}
#endif // CLOTH_INSTANCE_FROM_DATA

void characterCloth::InstanceFromTemplate( const characterCloth* copyme )
{
	Assert( copyme );
	RAGE_TRACK(characterCloth);

	m_ClothController = rage_new characterClothController();
	m_ClothController->InstanceFromTemplate( copyme->GetClothController() );
	m_ClothController->SetOwner(this);

	m_BoundCompositeRef = copyme->m_BoundComposite;
	const int boneIDsCount = copyme->m_BoneID.GetCount();
	if( boneIDsCount )
	{
		m_BoneIndex.Resize( boneIDsCount );
// 		m_BoneID.Resize( boneIDsCount );
// 		for( int i = 0; i < boneIDsCount; ++i )
// 		{
// 			m_BoneID[i] = copyme->m_BoneID[i];
// 		}
	}

	m_CharacterClothType = copyme;
}

characterCloth::~characterCloth()
{
	delete m_ClothController;
	m_ClothController = NULL;

	if (m_BoundComposite)
	{
		if (phConfig::IsRefCountingEnabled()) 
		{
			m_BoundComposite->Release();
		}
		else
		{
			delete m_BoundComposite;
		}
	}
}

IMPLEMENT_PLACE(characterCloth);

characterCloth::characterCloth(class datResource& rsc) 
	: m_BoundComposite(rsc)
	, m_BoneID(rsc)
	, m_Poses(rsc)
	, m_CharacterClothType(NULL)
{
	if(datResource_IsDefragmentation)
	{
		clothDebugf1("Defrag character cloth");
	}
}

#if MESH_LIBRARY

struct LocalMorphData
{
	Vector4* weights;
	int* blendIndices;
	int* morphCount;
	int mtlBaseIndex;
};

// NOTE: used only for resource purposes
datOwner<grmShaderGroup> s_ShaderGroup = NULL;
const mshMesh* s_MeshDrawable = NULL;

void BakeClothMorphData( fiTokenizer &T, Vector4* weights, int* blendIndices, int& morphCount, int mtlBaseIndex, const mshMesh* meshDrawable )
{
	Assert( weights );
	Assert( blendIndices );
	T.Reset();

	LocalMorphData mData;
	mData.weights = weights;
	mData.blendIndices = blendIndices;
	mData.morphCount = &morphCount;
	mData.mtlBaseIndex = mtlBaseIndex;

	Assert( meshDrawable );
	s_MeshDrawable = meshDrawable;

	characterCloth* charCloth = Alloca( characterCloth, 1 );
	rmcTypeFileParser parser;
	parser.RegisterLoader( "charcloth", "all", datCallback(MFA2(characterCloth::LoadClothMorphData), charCloth, &mData ) );

	/*bool result =*/  parser.ProcessTypeFile( T );
}

int FindClothMaterial( const mshMesh& meshDrawable, const grmShaderGroup* shaderGroup )
{
	return FindClothMaterial( &meshDrawable, shaderGroup );
}

int FindClothMaterial( const mshMesh* meshDrawable, const grmShaderGroup* shaderGroup )
{
	Assert( shaderGroup );
	Assert( meshDrawable );

	for( int i = 0; i < meshDrawable->GetMtlCount(); ++i )
	{
		if( grmShaderGroup::IsPedClothMaterial( meshDrawable->GetMtl(i).Name, shaderGroup ) )
			return i;
	}
	return -1;
}



bool characterCloth::Load(const char* basename)
{
	bool result = false;
	fiStream *S = ASSET.Open(basename, "type");
	if (S) 
	{
		fiTokenizer T;
		T.Init(basename,S);

		s_BoundsCount = 0;
		// Register types
		rmcTypeFileParser parser;

		s_ShaderGroup = NULL;
		parser.RegisterLoader( "shadinggroup", "shadinggroup", datCallback(MFA1(characterCloth::LoadShaders), this, 0, true) );
		result = parser.ProcessTypeFile( T );

		T.Reset();
		parser.Reset();
		parser.RegisterLoader( "collisionbounds", "all", datCallback(MFA1(characterCloth::CountCollisionBounds), this, 0, true) );
		parser.RegisterLoader( "charcloth", "all", datCallback(MFA1(characterCloth::LoadCloth), this, 0, true) );
		result = parser.ProcessTypeFile( T );

		if( s_BoundsCount )
		{
			m_BoundComposite = rage_new phBoundComposite;
			Assert( m_BoundComposite );

			m_BoundComposite->Init( s_BoundsCount, true );
			m_BoundComposite->SetNumBounds( s_BoundsCount );

			m_BoneID.Resize( s_BoundsCount );

			s_BoundsCount = 0;

			T.Reset();
			parser.Reset();
			parser.RegisterLoader( "collisionbounds", "all", datCallback(MFA1(characterCloth::LoadCollisionBounds), this, 0, true) );
			result = parser.ProcessTypeFile( T );
		}

		S->Close();
	}

	return result;	
}
#endif // MESH_LIBRARY

void characterCloth::UpdateClothPostSpu()
{
	// do nothing 
}

sysTaskHandle characterCloth::UpdateSpu( const crSkeleton* skeleton, float fTime, phVerletSPUDebug* verletSpuDebug)
{
	return m_ClothController->UpdateSpu( skeleton, fTime, GetBoundComposite(), m_BoneIndex.GetElements(), m_Force, verletSpuDebug);
}

void characterCloth::Update( float timeScale, const crSkeleton* skeleton, float fTime )
{
	m_ClothController->Update( phSimulator::GetGravityV(), timeScale, skeleton, fTime, GetBoundComposite(),  m_BoneIndex.GetElements());

	// TODO: is it really needed to re-compute/update character cloth bound ??
	//	Assert( m_ClothController->GetCloth(0) );
	//	m_ClothController->GetCloth(0)->ComputeClothBoundingVolume();

	if( m_EntityVerticesPtr )
	{
		Vec3V* RESTRICT verts = m_EntityVerticesPtr;
		Assert( verts );
		Vec3V* RESTRICT vtxPtr = m_ClothController->GetCloth(0)->GetClothData().GetVertexPointer();
		Assert( vtxPtr );

		int numVerts = m_ClothController->GetCloth(0)->GetNumVertices();
		for( int i = 0; i < numVerts; ++i )
		{
			verts[i] = vtxPtr[i];
#if RSG_PC || RSG_DURANGO || RSG_ORBIS
			// clear out W channel to prevent NaNs in shader vars
			verts[i].SetW(MAGIC_ZERO);
#endif
		}
	}
}


void characterCloth::UpdateAgainstCollisionInsts(const crSkeleton* skeleton)
{
	m_ClothController->UpdateAgainstCollisionInsts( skeleton, GetBoundComposite(),  m_BoneIndex.GetElements());

	// TODO: is it really needed to re-compute/update character cloth bound ??
	//	Assert( m_ClothController->GetCloth(0) );
	//	m_ClothController->GetCloth(0)->ComputeClothBoundingVolume();

	if( m_EntityVerticesPtr )
	{
		Vec3V* RESTRICT verts = m_EntityVerticesPtr;
		Assert( verts );
		Vec3V* RESTRICT vtxPtr = m_ClothController->GetCloth(0)->GetClothData().GetVertexPointer();
		Assert( vtxPtr );

		int numVerts = m_ClothController->GetCloth(0)->GetNumVertices();
		for( int i = 0; i < numVerts; ++i )
		{
			verts[i] = vtxPtr[i];
#if RSG_PC || RSG_DURANGO || RSG_ORBIS
			// clear out W channel to prevent NaNs in shader vars
			verts[i].SetW(MAGIC_ZERO);
#endif
		}
	}
}

void characterCloth::ApplyAirResistance(const float* RESTRICT inflationScale, Vec3V* RESTRICT normalsBuffer, const Vector3& combinedForce, const float* RESTRICT vertWeights)
{
	Assert( m_ClothController && m_ClothController->GetCloth(0) );
	m_ClothController->GetCloth(0)->ApplyAirResistance( inflationScale, vertWeights, normalsBuffer, 1, combinedForce);
}

void characterCloth::TransformClothVertexPositions( Mat34V_In mtx )
{
	m_ClothController->GetCloth( m_ClothController->GetLOD() )->GetClothData().TransformVertexPositions( mtx, 0 );
}

void characterCloth::SetPose(int poseIndex)
{
	Assert( poseIndex > -1 );
	Assert( m_CharacterClothType.ptr );
	const int posesVertsCount = m_CharacterClothType->m_Poses.GetCount();
	if( posesVertsCount )
	{
		Assert( m_ClothController.ptr );
// TODO: lods not supported yet
		int lodIndex = 0;
		phVerletCloth* pCloth = m_ClothController->GetCloth(lodIndex);
		Assert( pCloth );
		const int numVertices = pCloth->GetNumVertices();
		if( (poseIndex+1)*numVertices <= (posesVertsCount/2) )
		{
			Vec3V* RESTRICT pPoses = const_cast<Vec3V*>(m_CharacterClothType->m_Poses.GetElements() + poseIndex*numVertices*2);
			Vec3V* RESTRICT pClothVerts = pCloth->GetClothData().GetVertexPointer();
			Assert( pClothVerts );			
			for( int i = 0; i < numVertices; ++i )
			{
				pClothVerts[i].SetIntrin128( PackV1010102( pPoses[i*2].GetIntrin128Ref(), pPoses[i*2+1].GetIntrin128() ) );
			}

			clothDebugf1("Set pose index: %d   Num cloth vertices: %d  Num pose verts: %d", poseIndex, numVertices, posesVertsCount/2 );
		}
		else
		{
			clothDebugf1("Trying to set pose index: %d  . There is no enough data for such pose. Num cloth vertices: %d . Num vertex blends: %d", poseIndex, numVertices, posesVertsCount );
		}
	}
}


#if __DECLARESTRUCT
void characterCloth::DeclareStruct(datTypeStruct &s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE( characterCloth, pgBase )
		SSTRUCT_FIELD( characterCloth, m_Poses )
		SSTRUCT_FIELD( characterCloth, m_ClothController )
		SSTRUCT_FIELD( characterCloth, m_BoundComposite )
		SSTRUCT_FIELD( characterCloth, m_BoneID )		
		SSTRUCT_FIELD( characterCloth, m_Force )
		SSTRUCT_FIELD( characterCloth, m_ParentMatrix )
		SSTRUCT_FIELD( characterCloth, m_BoneIndex )		
		SSTRUCT_IGNORE( characterCloth, m_EntityVerticesPtr )	
		SSTRUCT_IGNORE( characterCloth, m_EntityParentMatrixPtr )
		SSTRUCT_FIELD( characterCloth, m_BoundCompositeRef )	
		SSTRUCT_FIELD( characterCloth, m_CharacterClothType )			
		SSTRUCT_FIELD( characterCloth, m_IsActive )
		SSTRUCT_FIELD( characterCloth, m_NegateForce )		
		SSTRUCT_FIELD( characterCloth, m_LockCounter )
		SSTRUCT_CONTAINED_ARRAY(  characterCloth, m_Pad )
		SSTRUCT_END( characterCloth )
}

#endif

#if MESH_LIBRARY


#define MESH_LIB_MEM_START(x) if( x ) sysMemStartTemp();
#define MESH_LIB_MEM_END(x) if( x ) sysMemEndTemp();

void characterCloth::LoadShaders(rmcTypeFileCbData *data) {

	fiTokenizer *T = data->m_T;
	T->CheckToken("{");

	sysMemStartTemp();
	s_ShaderGroup = grmShaderFactory::GetInstance().LoadGroup(*T);
	sysMemEndTemp();

	T->MatchToken("}");
}

void characterCloth::LoadClothMorphData( void* mData, rmcTypeFileCbData* data )
{
	Assert( mData );
	Assert( data );

	LoadClothMemControlled( data, true, s_MeshDrawable, mData );
}

void characterCloth::LoadCloth( rmcTypeFileCbData* data )
{
	Assert( data );
	LoadClothMemControlled( data, false, NULL );
}

void characterCloth::LoadClothMemControlled( rmcTypeFileCbData* data, bool writeBack, const mshMesh* meshDrawableForBaking, void* mData )
{
	characterCloth::typehascloth = true;

	fiTokenizer *T = data->m_T;
	T->CheckToken("{");
	T->CheckToken("mesh");

	// Load drawable mesh	
	char buf[128];
	T->GetToken(buf, sizeof(buf));
	sysMemStartTemp();
	mshMesh* meshDrawable = const_cast<mshMesh*>(meshDrawableForBaking);
	if( !meshDrawable )
	{
		meshDrawable = rage_new mshMesh;
#if __ASSERT
		bool success =
#endif
			SerializeFromFile(buf, *meshDrawable, "mesh");
#if __ASSERT
		Assert( success );
#endif
	}
	sysMemEndTemp();


	// Note: just read the idx
	T->GetInt();

	// Load simulation mesh
	T->GetToken(buf, sizeof(buf));
	sysMemStartTemp();
	mshMesh* mesh = rage_new mshMesh;
	SerializeFromFile(buf, *mesh, "mesh");
	sysMemEndTemp();

	// Grab the model index
	int nModel = 0;
	nModel = T->GetInt();

	// Read the bones
	sysMemStartTemp();
	int nBones = 0;
	char** ppBones = 0;
	if( T->CheckToken("boneCount") )
	{
		nBones = T->GetInt();
		if( nBones > 0 )
		{
			ppBones = rage_new char*[nBones];
			for( int i = 0; i < nBones; i++ )
			{
				T->CheckToken("bone");
				T->GetToken(buf, sizeof(buf));
				ppBones[i] = StringDuplicate(buf);
			}
		}
	}

	// create a temp skeleton
	Mat34V temp(V_IDENTITY);

	int version;
	crSkeletonData* skelData = crSkeletonData::AllocateAndLoad( "skeleton.skel", &version );
	// Note: skeleton file must exist !
	Assert( skelData );				
	crSkeleton* tempSkeleton = rage_new crSkeleton();
	Assert( tempSkeleton );
	tempSkeleton->Init(*skelData, &temp);

	sysMemEndTemp();

	T->CheckToken("}");

	Assert( mesh );	
	const int mtlIdx = 0;
	Assert( mtlIdx < mesh->GetMtlCount() );
	mshMaterial* mtl = &mesh->GetMtl(mtlIdx);
	Assert( mtl );

	MESH_LIB_MEM_START( writeBack )


	characterClothController* pClothController = rage_new characterClothController();

#if __RESOURCECOMPILER
	const char* buildName = pgRscBuilder::GetBuildName();
	if( buildName )
	{
		const char* dotChar = strstr(buildName,".");
		if( dotChar )
		{
			char controllerName[MAX_CLOTHCONTROLLER_NAME];
			const u32 nameLen = (u32)(dotChar-buildName)+1;
			formatf(controllerName, (nameLen < MAX_CLOTHCONTROLLER_NAME? nameLen: MAX_CLOTHCONTROLLER_NAME), "%s", buildName );
			pClothController->SetName(controllerName);
		}
	}
#endif

	int mtlIdxBase = -1;
	if( writeBack )
	{
		Assert( mData );
		LocalMorphData* mdata = (LocalMorphData*)mData;
		mtlIdxBase = mdata->mtlBaseIndex;
	}
	else
	{
		//Assert( s_ShaderGroup );
		if( !s_ShaderGroup )
			mtlIdxBase = 0;
		else
		{	
			mtlIdxBase = FindClothMaterial( meshDrawable, s_ShaderGroup );
			if( mtlIdxBase == -1 )
				mtlIdxBase = 0;
		}

		sysMemStartTemp();
		delete s_ShaderGroup;
		sysMemEndTemp();
	}

	// NOTE: cloth material must be found !
	Assert( mtlIdxBase > -1 );
	Assert( mtlIdxBase < meshDrawable->GetMtlCount() );

	// TODO: those will be used in tool/sample mode, but not in game
	Vec3V** drawableSkinnedVerts = NULL;

	phMorphDataAoS* tempMorphData = NULL;
	int tempMorphDataCount = 0;
	pClothController->Load( drawableSkinnedVerts, writeBack, tempMorphData, tempMorphDataCount, tempSkeleton, &meshDrawable->GetMtl(mtlIdxBase), mtl, nModel );

	if( writeBack )
	{
		Assert( tempMorphData );
		Assert( tempMorphDataCount );
		if( tempMorphDataCount > 0 )
		{
			Assert( mData );
			LocalMorphData* mdata = (LocalMorphData*)mData;
			*mdata->morphCount = tempMorphDataCount;
			const phMorphDataAoS* morphData = tempMorphData;

			for( int i = 0; i < tempMorphDataCount; ++i )
			{
				const phMorphDataAoS& phmdata	= morphData[i];

				mdata->weights[i].Set( VEC4V_TO_VECTOR4( phmdata.weights ) );

				const int idxOffset = i*4;

				// NOTE: phmdata.vtxIndex is the index of the skinned vtx from the mesh matterial, will be replaced with 255 in the model hook
				mdata->blendIndices[idxOffset]		= (int) phmdata.vtxIndex;
				mdata->blendIndices[idxOffset+1]	= (int) phmdata.index0;
				mdata->blendIndices[idxOffset+2]	= (int) phmdata.index1;
				mdata->blendIndices[idxOffset+3]	= (int) phmdata.index2;
			}
		}

		delete pClothController;
		delete[] tempMorphData;
	}

	MESH_LIB_MEM_END( writeBack );

	sysMemStartTemp();	

	delete mesh;
	if( !meshDrawableForBaking )
		delete meshDrawable;

	delete tempSkeleton;
	delete skelData;

	if( ppBones )
	{
		for( int i = 0; i < nBones; i++ )
		{
			delete[] (ppBones[i]);
		}
		delete[] ppBones;
	}
	sysMemEndTemp();

	// Add the cloth to the list
	if( !writeBack )
	{
		m_ClothController = pClothController;


		const char* pControllerName = m_ClothController->GetName();
		if( phVerletCloth::sm_tuneFilePath && pControllerName )
		{
			char buff[256];
			char nameBuff[128];
			formatf( nameBuff, "%s_%s", characterClothController::sm_CharacterName, pControllerName );
			formatf( buff, "%s%s", nameBuff, characterCloth::sm_MetaDataFileAppendix );

			ASSET.PushFolder( phVerletCloth::sm_tuneFilePath );
			if( ASSET.Exists(buff, "xml") )
			{
				LoadPoses("", characterClothController::sm_CharacterName);
			}
			ASSET.PopFolder();
		}

	}
}

void characterCloth::CountCollisionBounds( rmcTypeFileCbData *data )
{
	LoadBounds( data->m_EntityName, data->m_T, 0, true );
}

void characterCloth::LoadCollisionBounds( rmcTypeFileCbData *data )
{
	sysMemStartTemp();
	int version;
	crSkeletonData* skelData = crSkeletonData::AllocateAndLoad( "skeleton.skel", &version );
	// Note: skeleton file must exist !
	Assert( skelData );	
	sysMemEndTemp();

	LoadBounds( data->m_EntityName, data->m_T, skelData, false );

	sysMemStartTemp();
	delete skelData;
	sysMemEndTemp();
}

void characterCloth::LoadBounds( const char* boneName, fiTokenizer* T, crSkeletonData* skelData, bool countBounds )
{
	Assert( T );
	T->CheckIToken("{");

	// NOTE: we may have more than one bound per bone
	do
	{
		// Note: when loading collision bounds skeleton data must exist!
		Assert( countBounds || skelData );
		if( skelData )
		{
			const crBoneData* boneData = skelData->FindBoneData( boneName );
			if( !boneData )
			{
				Quitf("Bone with name: %s  doesn't exist in the skeleton.skel file! The exported skeleton.skel is most likely not complete. Character cloth can't be created correctly if cloth collision bounds is not complete. Ragebuilder will QUIT now!", boneName);
			}
			const u16 boneId = boneData->GetBoneId();
			Assert( s_BoundsCount > -1 && s_BoundsCount < skelData->GetNumBones() );
			m_BoneID[ s_BoundsCount ] = (int)boneId;
		}

		LoadBound(T, countBounds);

	} while ( !T->CheckIToken("}") );
}

void characterCloth::LoadBound( fiTokenizer* T, bool countBounds )
{
	Assert( T );

	char buff[fiBaseTokenizer::PUSH_BUFFER_SIZE];
	T->GetToken(buff, sizeof(buff)); 
	Assert( !stricmp(buff, "bound") );

	T->GetToken(buff, sizeof(buff)); 

	phBound* bound = NULL;
	if( !countBounds )
	{
		Assert( stricmp(buff,"none") );
		bound = phBound::Load(buff);
	}

	Matrix34 matrix;
	// If specified, load position
	if (T->CheckIToken("<", false))
	{
		T->CheckIToken("<");

		matrix.d.x = T->GetFloat();
		matrix.d.y = T->GetFloat();
		matrix.d.z = T->GetFloat();

		T->CheckIToken(">");
	}
	else
	{
		// No position was specified, default to the origin.
		matrix.d.Zero();
	}

	// If specified, load orientation
	if (T->CheckIToken("<", false))
	{
		T->CheckIToken("<");

		Quaternion quat;
		quat.x = T->GetFloat();
		quat.y = T->GetFloat();
		quat.z = T->GetFloat();
		quat.w = T->GetFloat();
		quat.Normalize();

		matrix.FromQuaternion(quat);

		T->CheckIToken(">");
	}
	else
	{
		// No orientation was specified, default to the identity orientation.
		matrix.Identity3x3();
	}

	if( !countBounds )
	{
		Assert( m_BoundComposite );
		m_BoundComposite->SetBound( s_BoundsCount, bound );
		m_BoundComposite->SetCurrentMatrix( s_BoundsCount, RCC_MAT34V(matrix) );
		m_BoundComposite->SetLastMatrix( s_BoundsCount, RCC_MAT34V(matrix) );
	}

	++s_BoundsCount;
}


#endif // MESH_LIBRARY

} // namespace rage
