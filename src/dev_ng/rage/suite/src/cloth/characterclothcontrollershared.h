// 
// cloth/characterclothcontrollershared.h
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "grcore/matrix43.h"
#include "profile/profiler.h"

//#pragma optimize("", off)


namespace rage
{
	EXT_PFD_DECLARE_ITEM( ClothNormals );
	EXT_PFD_DECLARE_ITEM( SkinnedMesh );

	namespace phCharClothStats
	{
		PF_PAGE(PCharCloth, "Char Cloth");
		PF_GROUP(CharCloth);
		PF_LINK(PCharCloth,CharCloth);

		PF_TIMER( ControllerUpdate, CharCloth );
		PF_TIMER( SkinMesh, CharCloth );
		PF_TIMER( UpdateCharacterCloth, CharCloth );
	};

	using namespace phCharClothStats;

	void characterClothController::SkinMesh( const bool 
#if 0//__PFDRAW
		shouldDraw
#endif
		, const Matrix43* RESTRICT mtx, const int vtxCount, const Vector3* RESTRICT posIn, const BindingInfo* RESTRICT bInfo, const int bInfoOffset, Vec3V* RESTRICT poseOut )
	{
		PF_FUNC( SkinMesh );

#if 0//__PFDRAW
		const bool readyToDraw = shouldDraw && PFD_SkinnedMesh.Begin(true);
#endif
		Assert( poseOut );
		for( int i = 0; i < vtxCount; ++i )
		{
			const int vtxIndex = i+bInfoOffset;
			Vector3 v = posIn[vtxIndex];

			const BindingInfo& binding = bInfo[vtxIndex];
			const int* RESTRICT blendIndices = binding.blendIndices;
			const Vector4 blendWeights = binding.weights;

			Matrix34 skinMtx;
			Matrix34 temp;
			mtx[ blendIndices[0] ].ToMatrix34(RC_MAT34V(skinMtx));
			skinMtx.ScaleFull(blendWeights.x);			
			mtx[ blendIndices[1] ].ToMatrix34(RC_MAT34V(temp));
			temp.ScaleFull(blendWeights.y);
			skinMtx.Add(temp);
			mtx[ blendIndices[2] ].ToMatrix34(RC_MAT34V(temp));;
			temp.ScaleFull(blendWeights.z);
			skinMtx.Add(temp);
			mtx[ blendIndices[3] ].ToMatrix34(RC_MAT34V(temp));
			temp.ScaleFull(blendWeights.w);
			skinMtx.Add(temp);

			skinMtx.Transform( v );

			poseOut[i] = VECTOR3_TO_VEC3V( v );
#if 0//__PFDRAW
			if( readyToDraw )
			{
				grcColor( Color32(1.0f,1.0f,0.0f,0.5f) );
				grcDrawSphere( 0.02f, v );
			}
#endif
		}
#if 0//__PFDRAW
		if( readyToDraw )
			PFD_SkinnedMesh.End();
#endif
	}

	void characterClothController::UpdateAgainstCollisionInsts(const crSkeleton* skeleton, const phBoundComposite* customBound, int* bonesIndices )
	{
		PF_FUNC( ControllerUpdate );

		const int lodIndex = GetLOD();
		// NOTE: there is no support for cloth lods in character cloth yet
		Assert( lodIndex == 0 );
		phVerletCloth* cloth = GetCloth(lodIndex);
		Assert( cloth );

		const int numBones = m_BoneIndexMap.GetCount();
		Assert( numBones );

#if NO_BONE_MATRICES
#if __SPU
		Matrix43* mtx = sysScratchAllocObj<Matrix43>( numBones );
#else
		Matrix43* RESTRICT mtx = Alloca(Matrix43, numBones );
#endif
#else
		Matrix43* RESTRICT mtx = (Matrix43* RESTRICT)m_PoseMatrixSet.GetElements();
#endif
		Assert(mtx);
		const Mat34V* RESTRICT invJointScaleMtx = skeleton->GetSkeletonData().GetCumulativeInverseJoints();		
		for(int i=0; i < numBones; ++i)
		{
			const int boneIdx = m_BoneIndexMap[i];
			Assert( boneIdx < skeleton->GetSkeletonData().GetNumBones() );
			Mat34V m;
			rage::Transform( m, skeleton->GetObjectMtx( boneIdx ), invJointScaleMtx[ boneIdx ] );
			mtx[i].FromMatrix34( m );
		} 

		const int numVertices = cloth->GetNumVertices();
		Mat34V skParent = *skeleton->GetParentMtx();
		Vec3V parentOffset = skParent.GetCol3();
		parentOffset.SetW( ScalarV(V_ZERO) );
		Mat34V attachedFrame = skParent;	// root bone: idx = 0( most likely ) = parent matrix = my attached frame

		Assert( cloth->GetEdgeList().GetCount() );

		Mat34V* customMatrices = NULL;		
		{
			if( customBound )
			{
			#if __SPU
				// NOTE: I assume custom bound for character cloth purposes is composite bound
				const int customBoundSize = sizeof(phBoundComposite);
				u8 boundBuffer[customBoundSize] ;
				//	u8* boundBuffer = (u8*)sysScratchAllocObj<phBoundComposite>(1);		// allocate from the scratch pad not the stack
				sysDmaLargeGetAndWait(boundBuffer, (u32)customBound, customBoundSize, 0);

				const int matrixCount = ((const phBoundComposite*)boundBuffer)->GetNumBounds();
				customMatrices = sysScratchAllocObj<Mat34V>(matrixCount);

				int* bonesIndicesSpu = (int*)sysScratchAllocObj<int>(matrixCount);
				sysDmaLargeGetAndWait(bonesIndicesSpu, (u32)bonesIndices, sizeof(int)*matrixCount, 0);
				bonesIndices = bonesIndicesSpu;
			#else
				const int matrixCount = customBound->GetNumBounds();
				customMatrices = Alloca( Mat34V, matrixCount ) ;
			#endif
				Assert( bonesIndices );
				for(int boundPartIndex = 0; boundPartIndex < matrixCount; ++boundPartIndex )
				{
					Mat34V boneMat;
					Transform( boneMat, attachedFrame, skeleton->GetObjectMtx( bonesIndices[boundPartIndex] ) );
				#if __SPU
					sysDmaGet( &customMatrices[boundPartIndex], (u32)&((const phBoundComposite*)boundBuffer)->GetCurrentMatrix(boundPartIndex), sizeof(Mat34V), 0);
					sysDmaWait(1<<0);
				#else
					customMatrices[boundPartIndex] = customBound->GetCurrentMatrix(boundPartIndex);				
				#endif
					Transform( customMatrices[boundPartIndex], boneMat, customMatrices[boundPartIndex] );
				}

				phBoundComposite* nonConstBound = const_cast<phBoundComposite*>(customBound);
				cloth->m_CustomBound = nonConstBound;
			}

			PF_START( UpdateCharacterCloth );
			cloth->UpdateCharacterClothAgainstCollisionInsts( attachedFrame, customMatrices, parentOffset );
			PF_STOP( UpdateCharacterCloth );
		}

		RecomputeNormals( numVertices, parentOffset );
		cloth->m_CustomBound = NULL;
	}


	void characterClothController::Update( Vec3V_In gravityV, float timeScale, const crSkeleton* skeleton, float fTime, const phBoundComposite* customBound, int* bonesIndices )
	{
		PF_FUNC( ControllerUpdate );

		// #if __SPU
		//		sysScratchScope s;
		// #endif

		const int lodIndex = GetLOD();
		// NOTE: there is no support for cloth lods in character cloth yet
		Assert( lodIndex == 0 );
		phVerletCloth* cloth = GetCloth(lodIndex);
		Assert( cloth );

#if NO_PIN_VERTS_IN_VERLET
		const int numPinnedVerts = cloth->GetClothData().GetNumPinVerts();
#else
		const int numPinnedVerts = cloth->GetPinCount();
#endif
		Assert( numPinnedVerts );

		const atArray<Vec3V>* originalPos = GetOriginalPos();
		Assert( originalPos );
		Assert( originalPos->GetCount() );

		const atArray<BindingInfo>* bindingInfo = GetBindingInfo();
		Assert( bindingInfo );
		Assert( bindingInfo->GetCount() );

		Vec3V* RESTRICT vPos = cloth->GetClothData().GetVertexPointer();
		Assert( vPos );
		Vec3V oldPos0 = vPos[0];

		const int numBones = m_BoneIndexMap.GetCount();
		Assert( numBones );

#if NO_BONE_MATRICES
	#if __SPU
		Matrix43* mtx = sysScratchAllocObj<Matrix43>( numBones );
	#else
		Matrix43* RESTRICT mtx = Alloca(Matrix43, numBones );
	#endif
#else
		Matrix43* RESTRICT mtx = (Matrix43* RESTRICT)m_PoseMatrixSet.GetElements();
#endif
		Assert(mtx);
		const Mat34V* RESTRICT invJointScaleMtx = skeleton->GetSkeletonData().GetCumulativeInverseJoints();		
		for(int i=0; i < numBones; ++i)
		{
			const int boneIdx = m_BoneIndexMap[i];
			Assert( boneIdx < skeleton->GetSkeletonData().GetNumBones() );
			Mat34V m;
			rage::Transform( m, skeleton->GetObjectMtx( boneIdx ), invJointScaleMtx[ boneIdx ] );
			mtx[i].FromMatrix34( m );
		} 

		//NOTE: hijacked pad[0] for debug draw purposes
		bool debugDraw = false; 

#if __PFDRAW
		//		debugDraw = (cloth->m_Pad[0] == 1);
		// 		if( debugDraw && PFD_SkinnedMesh.Begin(true) )
		// 		{
		// 			for( int i = 0; i < numBones; ++i )
		// 			{
		// 				Mat34V m;
		// 				mtx[i].ToMatrix34(m);
		// 				grcDrawAxis( 0.2f, RC_MATRIX34(m) );
		// 			}
		// 			PFD_SkinnedMesh.End();
		// 		}
#endif

		SkinMesh( debugDraw, mtx, numPinnedVerts, (Vector3*)originalPos->GetElements(), bindingInfo->GetElements(), 0, vPos );

		const int numVertices = cloth->GetNumVertices();
		const int numNotPinnedVerts = (numVertices-numPinnedVerts);
#if __SPU
		Vec3V* RESTRICT pSkinnedPosNotPinned = sysScratchAllocObj<Vec3V>( numNotPinnedVerts );
#else
		Vec3V* RESTRICT pSkinnedPosNotPinned = Alloca( Vec3V, numNotPinnedVerts );
#endif
		Assert( pSkinnedPosNotPinned );
		SkinMesh( debugDraw, mtx, numNotPinnedVerts, (Vector3*)originalPos->GetElements(), bindingInfo->GetElements(), numPinnedVerts, pSkinnedPosNotPinned );

		// NOTE: Transform, the respective UnTransform has to exist in the shader - works - don't delete
		Assert( skeleton );

		Mat34V skParent = *skeleton->GetParentMtx();
		Vec3V parentOffset = skParent.GetCol3();
		parentOffset.SetW( ScalarV(V_ZERO) );

		Mat34V attachedFrame = skParent;	// root bone: idx = 0( most likely ) = parent matrix = my attached frame

		skParent.SetCol3( Vec3V(V_ZERO) );
		int i;
		int numPinnedVerts8 = (numPinnedVerts >> 3) << 3; 
		for( i = 0; i < numPinnedVerts8; i += 8 )
		{
			const int	i0 = i,
				i1 = i + 1,
				i2 = i + 2,
				i3 = i + 3,
				i4 = i + 4,
				i5 = i + 5,
				i6 = i + 6,
				i7 = i + 7;
			vPos[i0] = Transform( skParent, vPos[i0] );
			vPos[i1] = Transform( skParent, vPos[i1] );
			vPos[i2] = Transform( skParent, vPos[i2] );
			vPos[i3] = Transform( skParent, vPos[i3] );
			vPos[i4] = Transform( skParent, vPos[i4] );
			vPos[i5] = Transform( skParent, vPos[i5] );
			vPos[i6] = Transform( skParent, vPos[i6] );
			vPos[i7] = Transform( skParent, vPos[i7] );
		}
		for( ; i < numPinnedVerts; ++i )
		{		
			vPos[i] = Transform( skParent, vPos[i] );
		}

		int numNotPinnedVerts8 = (numNotPinnedVerts >> 3) << 3;
		for( i = 0; i < numNotPinnedVerts8; i += 8 )
		{
			const int	i0 = i,
				i1 = i + 1,
				i2 = i + 2,
				i3 = i + 3,
				i4 = i + 4,
				i5 = i + 5,
				i6 = i + 6,
				i7 = i + 7;
			pSkinnedPosNotPinned[i0] = Transform( skParent, pSkinnedPosNotPinned[i0] );
			pSkinnedPosNotPinned[i1] = Transform( skParent, pSkinnedPosNotPinned[i1] );
			pSkinnedPosNotPinned[i2] = Transform( skParent, pSkinnedPosNotPinned[i2] );
			pSkinnedPosNotPinned[i3] = Transform( skParent, pSkinnedPosNotPinned[i3] );
			pSkinnedPosNotPinned[i4] = Transform( skParent, pSkinnedPosNotPinned[i4] );
			pSkinnedPosNotPinned[i5] = Transform( skParent, pSkinnedPosNotPinned[i5] );
			pSkinnedPosNotPinned[i6] = Transform( skParent, pSkinnedPosNotPinned[i6] );
			pSkinnedPosNotPinned[i7] = Transform( skParent, pSkinnedPosNotPinned[i7] );
		}
		for( ; i < numNotPinnedVerts; ++i )
		{		
			pSkinnedPosNotPinned[i] = Transform( skParent, pSkinnedPosNotPinned[i] );
		}

		// TODO: use states of some sort , similar to MaxPayne3
		{
			Vec3V temp = Subtract(vPos[0], oldPos0);
			static float errThresh = 0.2f;
			if( IsGreaterThanAll( Dot(temp,temp), ScalarVFromF32(errThresh*errThresh)) != 0 )
			{
				ScalarV _magicZero = ScalarVFromF32(MAGIC_ZERO);
				for(int i = numPinnedVerts; i < numVertices; ++i )
				{
					vPos[i] = Add(vPos[i], temp);
					vPos[i].SetW(_magicZero);
				}
			}
		}


#if __PFDRAW
		// 		if( debugDraw && PFD_SkinnedMesh.Begin(true) )
		// 		{
		// 			const int poseMatSetCount = m_PoseMatrixSetMap.GetCount();
		// 			for( int i = 0; i < poseMatSetCount; ++i )
		// 			{
		// 				Mat34V m;
		// 				skeleton->GetGlobalMtx( m_PoseMatrixSetMap[i], m );
		// 				grcDrawAxis( 0.2f, RC_MATRIX34(m) );
		// 			}
		// 			PFD_SkinnedMesh.End();
		// 		}
#endif

//		ScalarV gravityScale = ScalarVFromF32( Max(cloth->m_GravityFactor, 1.0f ));
		ScalarV gravityScale(V_ONE);

		Assert( cloth->GetEdgeList().GetCount() );

		Mat34V* customMatrices = NULL;		

		bool specialCase = m_ForcePin ? true: false;
		m_ForcePin -= (u8)specialCase;
		u8 packageIndex = m_PackageIndex;
		if( m_PackageIndex != m_NewPackageIndex )
		{
			if( m_ForcePin == 0 )
				m_PackageIndex = m_NewPackageIndex;
			specialCase = false;
		}

		if( specialCase || (m_Flags & enIsForceSkin) )
		{
			ScalarV _magicZero = ScalarVFromF32(MAGIC_ZERO);
			Assert( numVertices > numPinnedVerts );
			int numVerts8 = ((numVertices - numPinnedVerts)>>3)<<3;

			for ( i = numPinnedVerts; i < numVerts8; i += 8 )
			{
				const int	i0 = i,
					i1 = i + 1,
					i2 = i + 2,
					i3 = i + 3,
					i4 = i + 4,
					i5 = i + 5,
					i6 = i + 6,
					i7 = i + 7;

				const int vertexIndex0 = i0-numPinnedVerts;
				const int vertexIndex1 = i1-numPinnedVerts;
				const int vertexIndex2 = i2-numPinnedVerts;
				const int vertexIndex3 = i3-numPinnedVerts;
				const int vertexIndex4 = i4-numPinnedVerts;
				const int vertexIndex5 = i5-numPinnedVerts;
				const int vertexIndex6 = i6-numPinnedVerts;
				const int vertexIndex7 = i7-numPinnedVerts;

				vPos[i0] = pSkinnedPosNotPinned[vertexIndex0];
				vPos[i0].SetW(_magicZero);
				vPos[i1] = pSkinnedPosNotPinned[vertexIndex1];
				vPos[i1].SetW(_magicZero);
				vPos[i2] = pSkinnedPosNotPinned[vertexIndex2];
				vPos[i2].SetW(_magicZero);
				vPos[i3] = pSkinnedPosNotPinned[vertexIndex3];
				vPos[i3].SetW(_magicZero);
				vPos[i4] = pSkinnedPosNotPinned[vertexIndex4];
				vPos[i4].SetW(_magicZero);
				vPos[i5] = pSkinnedPosNotPinned[vertexIndex5];
				vPos[i5].SetW(_magicZero);
				vPos[i6] = pSkinnedPosNotPinned[vertexIndex6];
				vPos[i6].SetW(_magicZero);
				vPos[i7] = pSkinnedPosNotPinned[vertexIndex7];
				vPos[i7].SetW(_magicZero);
			}
			for ( ; i < numVertices; ++i )
			{
				const int vertexIndex = i-numPinnedVerts;
				vPos[i] = pSkinnedPosNotPinned[vertexIndex];
				vPos[i].SetW(_magicZero);
			}
		}
		else
		{
			if( customBound )
			{
#if __SPU
				// NOTE: I assume custom bound for character cloth purposes is composite bound
				const int customBoundSize = sizeof(phBoundComposite);
				u8 boundBuffer[customBoundSize] ;
				//	u8* boundBuffer = (u8*)sysScratchAllocObj<phBoundComposite>(1);		// allocate from the scratch pad not the stack
				sysDmaLargeGetAndWait(boundBuffer, (u32)customBound, customBoundSize, 0);

				const int matrixCount = ((const phBoundComposite*)boundBuffer)->GetNumBounds();
				customMatrices = sysScratchAllocObj<Mat34V>(matrixCount);

				int* bonesIndicesSpu = (int*)sysScratchAllocObj<int>(matrixCount);
				sysDmaLargeGetAndWait(bonesIndicesSpu, (u32)bonesIndices, sizeof(int)*matrixCount, 0);
				bonesIndices = bonesIndicesSpu;
#else
				const int matrixCount = customBound->GetNumBounds();
				customMatrices = Alloca( Mat34V, matrixCount ) ;
#endif
				Assert( bonesIndices );
				for(int boundPartIndex = 0; boundPartIndex < matrixCount; ++boundPartIndex )
				{
					Mat34V boneMat;
					Transform( boneMat, attachedFrame, skeleton->GetObjectMtx( bonesIndices[boundPartIndex] ) );
#if __SPU
					sysDmaGet( &customMatrices[boundPartIndex], (u32)&((const phBoundComposite*)boundBuffer)->GetCurrentMatrix(boundPartIndex), sizeof(Mat34V), 0);
					sysDmaWait(1<<0);
#else
					customMatrices[boundPartIndex] = customBound->GetCurrentMatrix(boundPartIndex);				
#endif
					Transform( customMatrices[boundPartIndex], boneMat, customMatrices[boundPartIndex] );

#if 0//__PFDRAW
					if( debugDraw && PFD_SkinnedMesh.Begin(true) )
					{
						grcDrawAxis( 0.2f, RC_MATRIX34(customMatrices[boundPartIndex]) );
						PFD_SkinnedMesh.End();
					}
#endif
				}

				phBoundComposite* nonConstBound = const_cast<phBoundComposite*>(customBound);
				cloth->m_CustomBound = nonConstBound;
			}

			clothBridgeSimGfx* clothBridge = GetBridge();
			Assert( clothBridge );
#if __SPU
			const int radiusesSize = sizeof(float)*numVertices*(packageIndex+1);		// dma all pin radiuses
			u8 radiusesBuffer[radiusesSize] ;

			sysDmaLargeGet( radiusesBuffer, (u32)clothBridge->GetVertexBlendsRef(0), radiusesSize, 0);
			sysDmaWait(1<<0);

			float* pinRadiuses = (float*)( radiusesBuffer + (packageIndex*numVertices*sizeof(float)));
#else
			const float* RESTRICT pinRadiuses = (const float* RESTRICT)(((char*)clothBridge->GetVertexBlends(0)) + (packageIndex*numVertices*sizeof(float)));
#endif

			Assert( numVertices > numPinnedVerts );
			int numVerts8 = ((numVertices - numPinnedVerts)>>3)<<3;

			for ( i = numPinnedVerts; i < numVerts8; i += 8 )
			{
				const int	i0 = i,
					i1 = i + 1,
					i2 = i + 2,
					i3 = i + 3,
					i4 = i + 4,
					i5 = i + 5,
					i6 = i + 6,
					i7 = i + 7;

				const int vertexIndex0 = i0-numPinnedVerts;
				const int vertexIndex1 = i1-numPinnedVerts;
				const int vertexIndex2 = i2-numPinnedVerts;
				const int vertexIndex3 = i3-numPinnedVerts;
				const int vertexIndex4 = i4-numPinnedVerts;
				const int vertexIndex5 = i5-numPinnedVerts;
				const int vertexIndex6 = i6-numPinnedVerts;
				const int vertexIndex7 = i7-numPinnedVerts;

				Vec3V skinnedPosition0 = pSkinnedPosNotPinned[vertexIndex0];
				Vec3V skinnedPosition1 = pSkinnedPosNotPinned[vertexIndex1];
				Vec3V skinnedPosition2 = pSkinnedPosNotPinned[vertexIndex2];
				Vec3V skinnedPosition3 = pSkinnedPosNotPinned[vertexIndex3];
				Vec3V skinnedPosition4 = pSkinnedPosNotPinned[vertexIndex4];
				Vec3V skinnedPosition5 = pSkinnedPosNotPinned[vertexIndex5];
				Vec3V skinnedPosition6 = pSkinnedPosNotPinned[vertexIndex6];
				Vec3V skinnedPosition7 = pSkinnedPosNotPinned[vertexIndex7];

				Vec3V simulatedPosition0 = vPos[i0];
				Vec3V simulatedPosition1 = vPos[i1];
				Vec3V simulatedPosition2 = vPos[i2];
				Vec3V simulatedPosition3 = vPos[i3];
				Vec3V simulatedPosition4 = vPos[i4];
				Vec3V simulatedPosition5 = vPos[i5];
				Vec3V simulatedPosition6 = vPos[i6];
				Vec3V simulatedPosition7 = vPos[i7];

				ScalarV pinRadiusV0 = ScalarVFromF32( pinRadiuses[i0] * m_PinningRadiusScale );
				ScalarV pinRadiusV1 = ScalarVFromF32( pinRadiuses[i1] * m_PinningRadiusScale );
				ScalarV pinRadiusV2 = ScalarVFromF32( pinRadiuses[i2] * m_PinningRadiusScale );
				ScalarV pinRadiusV3 = ScalarVFromF32( pinRadiuses[i3] * m_PinningRadiusScale );
				ScalarV pinRadiusV4 = ScalarVFromF32( pinRadiuses[i4] * m_PinningRadiusScale );
				ScalarV pinRadiusV5 = ScalarVFromF32( pinRadiuses[i5] * m_PinningRadiusScale );
				ScalarV pinRadiusV6 = ScalarVFromF32( pinRadiuses[i6] * m_PinningRadiusScale );
				ScalarV pinRadiusV7 = ScalarVFromF32( pinRadiuses[i7] * m_PinningRadiusScale );

				ScalarV compressedDelta0 = vPos[i0].GetW();
				ScalarV compressedDelta1 = vPos[i1].GetW();
				ScalarV compressedDelta2 = vPos[i2].GetW();
				ScalarV compressedDelta3 = vPos[i3].GetW();
				ScalarV compressedDelta4 = vPos[i4].GetW();
				ScalarV compressedDelta5 = vPos[i5].GetW();
				ScalarV compressedDelta6 = vPos[i6].GetW();
				ScalarV compressedDelta7 = vPos[i7].GetW();


				Vec3V skinToSim0 = Subtract(simulatedPosition0,skinnedPosition0);
				Vec3V skinToSim1 = Subtract(simulatedPosition1,skinnedPosition1);
				Vec3V skinToSim2 = Subtract(simulatedPosition2,skinnedPosition2);
				Vec3V skinToSim3 = Subtract(simulatedPosition3,skinnedPosition3);
				Vec3V skinToSim4 = Subtract(simulatedPosition4,skinnedPosition4);
				Vec3V skinToSim5 = Subtract(simulatedPosition5,skinnedPosition5);
				Vec3V skinToSim6 = Subtract(simulatedPosition6,skinnedPosition6);
				Vec3V skinToSim7 = Subtract(simulatedPosition7,skinnedPosition7);

				vPos[i0] = AddScaled( skinnedPosition0, skinToSim0, pinRadiusV0 );
				vPos[i1] = AddScaled( skinnedPosition1, skinToSim1, pinRadiusV1 );
				vPos[i2] = AddScaled( skinnedPosition2, skinToSim2, pinRadiusV2 );
				vPos[i3] = AddScaled( skinnedPosition3, skinToSim3, pinRadiusV3 );
				vPos[i4] = AddScaled( skinnedPosition4, skinToSim4, pinRadiusV4 );
				vPos[i5] = AddScaled( skinnedPosition5, skinToSim5, pinRadiusV5 );
				vPos[i6] = AddScaled( skinnedPosition6, skinToSim6, pinRadiusV6 );
				vPos[i7] = AddScaled( skinnedPosition7, skinToSim7, pinRadiusV7 );

				vPos[i0].SetW( compressedDelta0 );
				vPos[i1].SetW( compressedDelta1 );
				vPos[i2].SetW( compressedDelta2 );
				vPos[i3].SetW( compressedDelta3 );
				vPos[i4].SetW( compressedDelta4 );
				vPos[i5].SetW( compressedDelta5 );
				vPos[i6].SetW( compressedDelta6 );
				vPos[i7].SetW( compressedDelta7 );
			}
			for ( ; i < numVertices; ++i)
			{
				const int vertexIndex = i-numPinnedVerts;
				Vec3V skinnedPosition = pSkinnedPosNotPinned[vertexIndex];
				Vec3V simulatedPosition = vPos[i];				
				ScalarV pinRadiusV = ScalarVFromF32( pinRadiuses[i] * m_PinningRadiusScale );
				ScalarV compressedDelta = vPos[i].GetW();

				Vec3V skinToSim = Subtract(simulatedPosition,skinnedPosition);				
				vPos[i] = AddScaled( skinnedPosition, skinToSim, pinRadiusV );
				vPos[i].SetW( compressedDelta );
			}


			Mat34V gravityTransform(V_IDENTITY);
			if( (m_Flags & enIsFalling) || (m_Flags & enIsSkydiving) || (m_Flags & enIsProne) || (m_Flags & enIsProneFlipped))
			{
				Mat34V parentMat = *skeleton->GetParentMtx();

				// 			const crSkeletonData& skelData = skeleton->GetSkeletonData();
				// 			const char* pBoneName = "SKEL_ROOT";
				// 			const crBoneData* boneData = skelData.FindBoneData( pBoneName );
				// 			Assertf( boneData, "Ped skeleton doesn't have bone with name: %s", pBoneName );

				// TODO: can we just run away with bone 0 all the time ??
				int boneIndex = 0; //boneData ? boneData->GetIndex(): 0;
				Mat34V boneMat;
				Transform( boneMat, parentMat, skeleton->GetObjectMtx( boneIndex ) );
				if( m_Flags & enIsProneFlipped )
				{
					gravityTransform.SetCol0( boneMat.GetCol1() );
					gravityTransform.SetCol1( boneMat.GetCol0() );
					gravityTransform.SetCol2( boneMat.GetCol2() );
				}
				else
				{
					gravityTransform.SetCol0( boneMat.GetCol0() );
					gravityTransform.SetCol1( boneMat.GetCol1() );
					gravityTransform.SetCol2( boneMat.GetCol2() );
				}
			}

			PF_START( UpdateCharacterCloth );
			cloth->UpdateCharacterCloth( timeScale, gravityTransform, attachedFrame, fTime, gravityV, gravityScale, customMatrices, parentOffset );
			PF_STOP( UpdateCharacterCloth );

			if( packageIndex > 0 )
			{
				ScalarV _magicZero = ScalarVFromF32(MAGIC_ZERO);				
				ScalarV _threshold = ScalarVFromF32( GetPinRadiusThreshold() );

				for( int i = numPinnedVerts; i < numVertices; ++i )
				{
					ScalarV pinRadiusV = ScalarVFromF32( pinRadiuses[i] );

					vPos[i]		= SelectFT( IsLessThanOrEqual(pinRadiusV, _threshold), vPos[i], pSkinnedPosNotPinned[i-numPinnedVerts] );
					vPos[i].SetW( SelectFT( IsLessThanOrEqual(pinRadiusV, _threshold), vPos[i].GetW(), _magicZero ) );
				}
			}
		}

		RecomputeNormals( numVertices, parentOffset );
		cloth->m_CustomBound = NULL;
	}


	void characterClothController::RecomputeNormals( int nVerts, Vec3V_In
#if __PFDRAW
		vParentOffset 
#endif
		)
	{
		const int lodIndex = GetLOD();
		phVerletCloth* cloth = GetCloth(lodIndex);
		Assert( cloth );

		Vec3V* RESTRICT pVerts = cloth->GetClothData().GetVertexPointer();
		Vec3V* RESTRICT pNormalsBuffer = cloth->GetClothData().GetNormalsPointer();

		sysMemSet(pNormalsBuffer, 0, sizeof(Vec3V) * nVerts);	

		const atArray<u16>* RESTRICT triIndices = GetTriIndices();
		Assert( triIndices );
		const int indexCount = triIndices->GetCount();
		int i;
		for( i = 0; i < indexCount; i += 3 )
		{
			const int i0 = (*triIndices)[i];
			const int i1 = (*triIndices)[i+1];
			const int i2 = (*triIndices)[i+2];

			const Vec3V n1 = pNormalsBuffer[i0];
			const Vec3V n2 = pNormalsBuffer[i1];
			const Vec3V n3 = pNormalsBuffer[i2];

			const Vec3V vEdge1 = Subtract( pVerts[i1], pVerts[i0] );
			const Vec3V vEdge2 = Subtract( pVerts[i2], pVerts[i0] );

			const Vec3V normal = Cross(vEdge1, vEdge2);

			pNormalsBuffer[i0] = Add( n1, normal );
			pNormalsBuffer[i1] = Add( n2, normal );
			pNormalsBuffer[i2] = Add( n3, normal );
		}
#if __SPU
		// Note: for whatever reason g_UnitUp is not defined for __SPU
		const Vec3V g_UpV(0.0f,1.0f,0.0f);
#else
		const Vec3V g_UpV = VECTOR3_TO_VEC3V(g_UnitUp);
#endif

#if __PFDRAW
		bool canDrawNormals = PFD_ClothNormals.Begin(true);
		ScalarV scaleV = ScalarVFromF32(0.1f);
#endif
		for( i = 0; i < nVerts; ++i )
		{
			pNormalsBuffer[i] = NormalizeSafe( pNormalsBuffer[i], g_UpV );
#if __PFDRAW
			if( canDrawNormals )
			{				
				grcDrawLine( VEC3V_TO_VECTOR3( vParentOffset + pVerts[i]), VEC3V_TO_VECTOR3((vParentOffset + pVerts[i]+Scale(pNormalsBuffer[i],scaleV))), Color_white, Color_red );
			}
#endif
		}
#if __PFDRAW
		if( canDrawNormals )
		{				
			PFD_ClothNormals.End();
		}
#endif

	}



} // namespace rage
