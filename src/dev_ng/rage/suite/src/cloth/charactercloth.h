// 
// charactercloth/charactercloth.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CHARACTER_CLOTH_H
#define CHARACTER_CLOTH_H

#include "characterclothcontroller.h"
#include "vector/matrix34.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "rmcore/drawable.h"
#include "rmcore/typefileparser.h"
#include "fragment/cachemanager.h"
#include "fragment/cache.h"
#include "data/resource.h"
#include "paging/ref.h"


namespace rage
{

// TODO: same as clothdata
struct dataVec3V
{
	u32 m_Data[4];

	PAR_SIMPLE_PARSABLE;
};
// TODO: this class is here just so we can load/save vec3v in floats
class characterClothDebug : public clothBase
{
public:
	characterClothDebug() {}
	void UpdateClothPostSpu() { /* do nothing */}

	atArray<Vec3V>	m_Poses;

	PAR_SIMPLE_PARSABLE;
};



class characterCloth : public clothBase
{
public:
	characterCloth();
	characterCloth(class datResource& rsc);
	virtual ~characterCloth();
	DECLARE_PLACE(characterCloth);

	void PostPlace() { }
	int Release() { delete this; return 0; } 

#if CLOTH_INSTANCE_FROM_DATA
	void InstanceFromData(int vertsCapacity, int edgesCapacity, int boneIDsCapacity);
#endif

	void InstanceFromTemplate( const characterCloth* copyme );

	bool Load(const char* szCharacterEntity);
	void Update( float timeScale, const crSkeleton* skeleton, float fTimeStep );
	void UpdateAgainstCollisionInsts(const crSkeleton* skeleton);
	sysTaskHandle UpdateSpu ( const crSkeleton* skeleton, float timeStep, phVerletSPUDebug* s_VerletSpuDebug = NULL);
	void UpdateClothPostSpu();

	void Teleport(const Matrix34& mtx, bool updateSkeleton=true);

	// only move the cloth verts, don't reskin, move skeleton or anything else
	void TransformClothVertexPositions( Mat34V_In mtx );

	characterClothController* GetClothController() const { return m_ClothController; }
	void ApplyAirResistance(const float* RESTRICT inflationScale, Vec3V* RESTRICT normalsBuffer, const Vector3& vAirSpeed, const float* RESTRICT vertWeights);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

#if MESH_LIBRARY
	static bool typehascloth;
#endif

	void LoadClothMorphData( void* mData, rmcTypeFileCbData* data );
	void LoadClothMemControlled( rmcTypeFileCbData* data, bool memcontrol, const mshMesh* meshDrawableForBaking, void* mData = NULL );
	void LoadCloth( rmcTypeFileCbData* data );
	void LoadShaders( rmcTypeFileCbData* data );

	void LoadCollisionBounds( rmcTypeFileCbData* data );
	void CountCollisionBounds( rmcTypeFileCbData* data );
	void LoadBound( fiTokenizer*, bool countBounds );
	void LoadBounds( const char* firstBoneName, fiTokenizer*, crSkeletonData* skelData, bool countBounds );

	const phBoundComposite* GetBoundComposite() const { return m_BoundComposite? m_BoundComposite.ptr: m_BoundCompositeRef.ptr; }
	void SetForce( Vec3V_In force ) { m_Force = force; }

	int GetBoneIndex(int idx) const { return m_BoneIndex[idx]; }
	const atArray<int>& GetBoneIDMapRef() const { return (m_CharacterClothType? m_CharacterClothType->m_BoneID: m_BoneID); }
	atArray<int>& GetBoneIndexMapRef() { return m_BoneIndex; }

	Mat34V* GetParentMtx() { return &m_ParentMatrix; }
	void SetParentMtx(Mat34V_In mat) 
	{ 
		m_ParentMatrix = mat; 
		if( m_EntityParentMatrixPtr )
			(*m_EntityParentMatrixPtr) = mat;
	}
	Vec3V_Out GetParentLastPos() const { return m_ParentMatrix.GetCol3(); }
	Vec3V_Out GetParentAtVec() const { return m_ParentMatrix.GetCol0(); }

	void SetEntityParentMtxPtr(Mat34V* mat) { m_EntityParentMatrixPtr = mat; }
	void SetEntityVerticesPtr(Vec3V* ptr) { m_EntityVerticesPtr = ptr; }
	Vec3V* GetEntityVerticesPtr() const { return m_EntityVerticesPtr; }

	bool IsActive() const { return m_IsActive; }
	void SetActive(bool isActive) { m_IsActive = isActive; }

	bool IsForceNegated() const { return m_NegateForce; }
	void SetForceNegate(bool negateForce) { m_NegateForce = negateForce; }

	void SetLockCounter(u8 count) { m_LockCounter = count; }
	u8 GetLockCounter() const { return m_LockCounter; }

#if !__SPU	
	void AddWidgets(bkGroup* pRagGroup);
	void SaveVerts();
	void LoadVerts();
	void SavePose();
	void LoadPoses(const char* pFilePath, const char* pCharacterName);

	void LoadFromFile(const char* filePath = "common:/data/cloth/");	
	void SaveToFile(const char* filePath = "common:/data/cloth/");

	void LoadAll();
	void SaveAll();
#if __BANK && !__RESOURCECOMPILER
	void RegisterRestInterface(const char* controllerName);
	void UnregisterRestInterface(const char* controllerName);
#endif
#endif // !__SPU

	void SetPose(int poseIndex);
	int GetPosesVertsCount() const { return m_CharacterClothType ? m_CharacterClothType->m_Poses.GetCount(): m_Poses.GetCount(); }

	static const char* sm_MetaDataFileAppendix;

	// NOTE: poses are stored vertex, delta, vertex, delta ... etc 
	// it is twice the memory but works on all platforms ( deltas are different on 360 and non-360 platforms )
	// TODO: revise in the future - Svetli
	atArray<Vec3V>							m_Poses;

protected:

	datOwner<characterClothController> 		m_ClothController;
	datOwner<phBoundComposite>				m_BoundComposite;
// NOTE: boneids here are for collision purposes
	atArray<int>							m_BoneID;				// bone ids where the bounds are attached to	
	Vec3V									m_Force;
	Mat34V									m_ParentMatrix;
	atArray<int>							m_BoneIndex;

	Vec3V*									m_EntityVerticesPtr;
	Mat34V*									m_EntityParentMatrixPtr;
	pgRef<phBoundComposite>					m_BoundCompositeRef;
	pgRef<const characterCloth>				m_CharacterClothType;

	bool									m_IsActive;
	bool									m_NegateForce;
	u8										m_LockCounter;

#if __64BIT
	char									m_Pad[1];
#else
	char									m_Pad[5];
#endif

	PAR_SIMPLE_PARSABLE;
};


#if MESH_LIBRARY

void BakeClothMorphData( fiTokenizer &T, Vector4* weights, int* blendIndices, int& morphCount, int mtlBaseIndex, const mshMesh* meshDrawable );
int FindClothMaterial( const mshMesh* meshDrawable, const grmShaderGroup* shaderGroup);
int FindClothMaterial( const mshMesh& meshDrawable, const grmShaderGroup* shaderGroup );

#endif

} // namespace rage

#endif // CHARACTER_CLOTH_H
