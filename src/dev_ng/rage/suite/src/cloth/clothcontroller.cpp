// 
// cloth/clothcontroller.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "clothcontroller.h"

#include "atl/array.h"
#include "crskeleton/skeletondata.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/fvfchannels.h"
#include "phbound/boundtaperedcapsule.h"
#include "phbound/support.h"
#include "pheffects/clothconnectivitydata.h"
#include "pheffects/morphgeometry.h"
#include "profile/page.h"
#include "profile/group.h"
#include "profile/element.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/timemgr.h"


#include "cloth/clothcontrollershared.h"

//#pragma optimize("g",off)


namespace rage
{

namespace phClothControllerStats
{
	PF_PAGE(ClothController,"ClothController");

	PF_GROUP( ClothControllerUpdate );
	PF_LINK( ClothController, ClothControllerUpdate );

	PF_TIMER( ClothMapping, ClothControllerUpdate );	
	PF_TIMER( ClothMorph, ClothControllerUpdate );	
}

namespace phClothStats
{
	EXT_PF_TIMER(GetVertexNormalsFromDrawable);
	EXT_PF_TIMER(GetVertexNormals);
}

using namespace phClothControllerStats;
using namespace phClothStats;

clothController::clothController()
	: m_Bridge(NULL)
	, m_ClothInstance(NULL)
	, m_Owner(NULL)
	, m_MorphController(NULL)
	, m_NormalOffset(0)
	, m_UVOffset(0)
	, m_LOD(0)
	, m_IndicesWindingOrder(false)
	, m_Flags(0)
{
	for( int i = 0; i < LOD_COUNT; ++i )
	{
		m_Cloth[i] = NULL;
	}
}

#if  !__SPU

void clothController::RegisterRestInterface()
{
#if __BANK && !__RESOURCECOMPILER
	GetCloth( GetLOD())->RegisterRestInterface( m_Name );
	GetBridge()->RegisterRestInterface( m_Name );
#endif
}

void clothController::UnregisterRestInterface()
{
#if __BANK && !__RESOURCECOMPILER
	GetCloth( GetLOD())->UnregisterRestInterface( m_Name );
	GetBridge()->UnregisterRestInterface( m_Name );
#endif
}

void clothController::Load()
{
	GetCloth( GetLOD())->Load( m_Name );
	GetBridge()->Load( m_Name );
}

void clothController::Save()
{
#if !__FINAL
	GetCloth( GetLOD())->Save( m_Name );
	GetBridge()->Save( m_Name );
#endif
}

#if __BANK && !__RESOURCECOMPILER
void clothController::AddWidgets(bkBank& UNUSED_PARAM(bank))
{
	// 	bank.AddButton("Load", datCallback( MFA(clothController::Load), this ) );
	// 	bank.AddButton("Save", datCallback( MFA(clothController::Save), this ) );
}
#endif // __BANK

#endif // !__SPU


#if CLOTH_INSTANCE_FROM_DATA
void clothController::InstanceFromData(int vertsCapacity, int edgesCapacity)
{
// TODO: no lod's for now
	for( int i = 0; i < 1/*LOD_COUNT*/; ++i )
	{
		phVerletCloth* pCloth = rage_new phVerletCloth();
		Assert( pCloth );
		pCloth->InstanceFromData(vertsCapacity, edgesCapacity);
		Assert( !m_Cloth[i] );
		m_Cloth[i] = pCloth;
	}
}
#endif // CLOTH_INSTANCE_FROM_DATA


void clothController::InstanceFromTemplate( const clothController* copyme )
{
	Assert( copyme );
	memcpy( m_Name, copyme->GetName(), sizeof(m_Name) );

	m_MorphController = copyme->m_MorphController.ptr;
	SAFE_ADD_KNOWN_REF(m_MorphController);
	SetFlag(CLOTH_IsMorphControllerOwned,false);

	m_Bridge = copyme->m_Bridge.ptr;
	SAFE_ADD_KNOWN_REF(m_Bridge);
	SetFlag(CLOTH_IsBridgeOwned,false);		

	for( int i = 0; i < LOD_COUNT; ++i )
	{
		phVerletCloth* verletClothType = copyme->m_Cloth[i];
		if( verletClothType )
		{
// TODO: enable this once resourcing has been switched

// #if NO_PIN_VERTS_IN_VERLET
// 			Assertf( verletClothType->GetClothData().GetNumPinVerts(), "Cloth piece %s has 0 pinned verts. The cloth will fly away, art need to pin some verts.", m_Name );
// #else
// 			Assertf( verletClothType->GetPinCount(), "Cloth piece %s has 0 pinned verts. The cloth will fly away, art need to pin some verts.", m_Name );
// #endif


			phVerletCloth* pCloth = rage_new phVerletCloth();
			Assert( pCloth );
			pCloth->InstanceFromTemplate( verletClothType );
			Assert( !m_Cloth[i] );
			m_Cloth[i] = pCloth;

// TODO: TEMP TEMP TEMP, patch inflation scale from 0.0 to 1.0
			int numVerts = pCloth->GetNumVertices();
			float* RESTRICT pInflationScale = m_Bridge->GetInflationScale(i);
			Assert( pInflationScale );
			for( int k = 0; k < numVerts; ++k )
			{
				pInflationScale[k] = Selectf( -pInflationScale[k], 1.0f, pInflationScale[k] );
			}
//
		}
	}	
}

void clothController::Shutdown()
{
	for( int i = 0; i < LOD_COUNT; ++i )
	{
		if( m_Cloth[i] )
		{
			m_Cloth[i]->Shutdown();
		}
	}

	if( GetFlag(CLOTH_IsBridgeOwned) )
	{
		clothDebugf1("Shutdown clothController type (m_Bridge): %s", m_Name);

		Assert( m_Bridge.ptr );
		delete m_Bridge;

		SetFlag(CLOTH_IsBridgeOwned, false);
		m_Bridge.ptr = NULL;
	}
	else
	{
		if( m_Bridge.ptr )
		{
			clothDebugf1("Shutdown clothController instance (m_Bridge): %s", m_Name);

			SAFE_REMOVE_KNOWN_REF(m_Bridge);
			m_Bridge.ptr = NULL;
		}
	}

	if( GetFlag(CLOTH_IsMorphControllerOwned) )
	{		
		Assert( m_MorphController.ptr );
		delete m_MorphController;

		SetFlag(CLOTH_IsMorphControllerOwned, false);
		m_Bridge.ptr = NULL;
	}
	else
	{
		// Ropes don't have morph controller
		if( m_MorphController.ptr )
		{
			clothDebugf1("Shutdown clothController instance (m_MorphController): %s", m_Name);

			SAFE_REMOVE_KNOWN_REF(m_MorphController);
			m_MorphController.ptr = NULL;
		}
	}
}

clothController::~clothController()
{
	Shutdown();

	clothDebugf1("Delete clothController: %s", m_Name);

	for( int i = 0; i < LOD_COUNT; ++i )
	{		
		if( m_Cloth[i] )
		{
			clothDebugf1("Deleting verlet with num verts: %d", m_Cloth[i]->GetNumVertices() );
			delete m_Cloth[i];
		}
	}	
}

IMPLEMENT_PLACE(clothController);

clothController::clothController(class datResource & /*rsc*/) 
{
	if(datResource_IsDefragmentation)
	{
		clothDebugf1("Defrag clothController: %s", m_Name);
	}
}

#if __DECLARESTRUCT
void clothController::DeclareStruct(datTypeStruct &s)
{
	m_ClothInstance = NULL;
	m_Owner			= NULL;

	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE( clothController ,pgBase )
		SSTRUCT_FIELD( clothController, m_Bridge )
		SSTRUCT_FIELD( clothController, m_MorphController )			
		SSTRUCT_CONTAINED_ARRAY( clothController, m_Cloth )
		SSTRUCT_IGNORE( clothController, m_ClothInstance )
		SSTRUCT_IGNORE( clothController, m_Owner )
		SSTRUCT_FIELD( clothController, m_Flags )
		SSTRUCT_FIELD( clothController, m_NormalOffset )
		SSTRUCT_FIELD( clothController, m_UVOffset )
		SSTRUCT_FIELD( clothController, m_LOD )
		SSTRUCT_FIELD( clothController, m_IndicesWindingOrder )		
		SSTRUCT_CONTAINED_ARRAY( clothController, m_Name )
		SSTRUCT_END( clothController )		
}
#endif


void clothController::SwitchLOD( int newLodIndex, int oldLodIndex, int mapIndex, bool shouldMorph )
{
	phMorphController* morphController = GetMorphController();
	Assert( morphController );
	Assert( newLodIndex > -1 && oldLodIndex > -1 && mapIndex > -1);	

	// TODO: add support for jumping over LOD levels 
	//	Assert( abs(newLodIndex - oldLodIndex) == 1 );				

	phVerletCloth* clothNew = GetCloth( newLodIndex );
	Assert( clothNew );
	phVerletCloth* clothOld = GetCloth( oldLodIndex );
	Assert( clothOld );

	phClothData* clothDataNew = &clothNew->GetClothData();
	Assert( clothDataNew );
	phClothData* clothDataOld = &clothOld->GetClothData();
	Assert( clothDataOld );

	if( !shouldMorph )
	{
		Assert( mapIndex != newLodIndex );
		const POLYGON_INDEX* map = morphController->GetMap( mapIndex, newLodIndex );

		Assert( map );			//  why there is no map ?!

		const int indexCount = morphController->GetIndexMapCount(mapIndex, newLodIndex);
		for( int i = 0; i < indexCount; ++i )
		{
			if (Verifyf(map[i] != 9999, "map[i] not set to valid mapping"))
			{
				clothDataNew->SetVertexPosition( i, clothDataOld->GetVertexPosition(map[i]) );	
			}
		}
	}
	else
	{
		Morph( newLodIndex, oldLodIndex );
		morphController->ClearW( morphController->GetMorphMapCount( newLodIndex, oldLodIndex ), (Vec3V*)GetCloth(newLodIndex)->GetClothData().GetVertexPointer() );
	}
}

#if MESH_LIBRARY

phBoundGeometry* clothController::GenerateMeshBound( mshMaterial* mtl, Vector3* vPos )
{
	Assert( vPos );
	Assert( mtl );

	int nVerts = mtl->GetVertexCount();
	int nMaterials = 1;
	int generatedPolyCount = mtl->GetTriangleCount();

	phBoundGeometry* meshBound = rage_new phBoundGeometry();
	const int numPerVertAttribs=0;
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	const int nMaterialColors=0;
	meshBound->Init(Fourtify(nVerts), numPerVertAttribs, nMaterials, nMaterialColors, generatedPolyCount, 0);
#else
	meshBound->Init(Fourtify(nVerts), numPerVertAttribs, nMaterials, generatedPolyCount, 0);
#endif
#else
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	const int nMaterialColors=0;
	meshBound->Init(Fourtify(nVerts), numPerVertAttribs, nMaterials, nMaterialColors, generatedPolyCount);
#else
	meshBound->Init(Fourtify(nVerts), numPerVertAttribs, nMaterials, generatedPolyCount);
#endif
#endif

	sysMemEndTemp();

#if COMPRESSED_VERTEX_METHOD > 0
	Vector3 aabbMin(VEC3_MAX), aabbMax(-VEC3_MAX);
	for (int i=0; i<nVerts; ++i)
	{
		aabbMin.Min(aabbMin, vPos[i]);
		aabbMax.Max(aabbMax, vPos[i]);
	}

	aabbMin.Subtract(VEC3V_TO_VECTOR3(meshBound->GetMarginV()));
	aabbMax.Add(VEC3V_TO_VECTOR3(meshBound->GetMarginV()));
	meshBound->InitQuantization(RCC_VEC3V(aabbMin), RCC_VEC3V(aabbMax));
#endif

	for(int i=0; i<nVerts; ++i)
	{
		meshBound->SetVertex(i, RCC_VEC3V(vPos[i]));
	}
	sysMemStartTemp();

	// set extra vertices to <0,0,0>, so that NAN exceptions aren't raised in Fourtified functions
	// We now set it to the center of the bounding box because having any verts outside of the bounding box is bad news.
	// I'm not even sure that we need this 'fourtification' business any more - I think that was only relevant under the old, non-Bullet
	//   collision detection scheme.
	Vector3 centerVector;
	centerVector.Average(VEC3V_TO_VECTOR3(meshBound->GetBoundingBoxMin()), VEC3V_TO_VECTOR3(meshBound->GetBoundingBoxMax()));
	int fourtifiedNumVerts = Fourtify(nVerts);
	for(int i=nVerts; i<fourtifiedNumVerts; ++i)
	{
		meshBound->SetVertex(i, RCC_VEC3V(centerVector));
	}

	meshBound->SetMaterialId(0, 0);

	sysMemEndTemp();

	phPolygon poly;
	int physPolyCount = 0;

	for(mshMaterial::TriangleIterator ti = mtl->BeginTriangles(); ti != mtl->EndTriangles(); ++ti) 
	{
		int indices[3];
		ti.GetVertIndices(indices[0], indices[1], indices[2]);

		phPolygon::Index i0 = static_cast<phPolygon::Index>( indices[0] );
		phPolygon::Index i1 = static_cast<phPolygon::Index>( indices[1] );
		phPolygon::Index i2 = static_cast<phPolygon::Index>( indices[2] );

		poly.InitTriangle(i0, i1, i2, meshBound->GetVertex(i0), meshBound->GetVertex(i1), meshBound->GetVertex(i2));
		meshBound->SetPolygon(physPolyCount++, poly);
	}

	sysMemStartTemp();

	meshBound->PostLoadCompute();				// do post-processing on bound

	// we used to just use the centroid offset as the center of gravity, but CalcCGOffset should give us a much better value
	Vector3 newCGOffset;
	meshBound->CalcCGOffset(RC_VEC3V(newCGOffset));
	meshBound->SetCGOffset(RCC_VEC3V(newCGOffset));

#if !__FINAL
	meshBound->WeldVertices(1e-3f);
	meshBound->ComputeNeighbors("simcloth");
#endif

	return meshBound;
}

#endif // MESH_LIBRARY


#if __DEV
bool clothController::CheckForBadPolygons( int lodIndex, const rmcDrawable* pDrawable ) const
{
	bool badPolygonFound = false;
	Assert( pDrawable );
	const rmcLodGroup& lodGroup = pDrawable->GetLodGroup();
	if( !lodGroup.ContainsLod( lodIndex ) )
		return false;						// there is no mesh, don't break out though

	grmModel* pModel = lodGroup.GetLod( lodIndex ).GetModel( 0 /*GetModelIdx()*/);	
	Assert(pModel);
	grcVertexBuffer* pMeshBuffer = pModel->GetGeometry( 0 /*GetGeometryIdx()*/).GetVertexBuffer();		
	Assert(pMeshBuffer);
	if( !pMeshBuffer )
		return false;

	grcIndexBuffer* pIndexBuffer = pModel->GetGeometry( 0 /*GetGeometryIdx()*/).GetIndexBuffer();		
	Assert(pIndexBuffer);
	if( !pIndexBuffer )
		return false;

	const u16 * RESTRICT clothDisplayMap = GetClothDisplayMap( lodIndex );

	const int indexCount = pIndexBuffer->GetIndexCount(); Assert(indexCount%3 == 0);
	const int triCount = indexCount/3;
	const u16* indexData = pIndexBuffer->LockRO();

	Assert( lodIndex >= LOD_HIGH && lodIndex < LOD_COUNT );
	phVerletCloth* cloth = m_Cloth[lodIndex];
	Assert( cloth );

	grcVertexBufferEditor editor(pMeshBuffer);
	for(int i=0; i<triCount; ++i)
	{
		const u16 i0 = indexData[(i * 3) + 0];
		const u16 i1 = indexData[(i * 3) + 1];
		const u16 i2 = indexData[(i * 3) + 2];

		Vector3 p0(editor.GetPosition(i0));
		Vector3 p1(editor.GetPosition(i1));
		Vector3 p2(editor.GetPosition(i2));

		// entry can't be negative
		//		if(clothDisplayMap[i0] != -1)
		{
			p0.Set( VEC3V_TO_VECTOR3(cloth->GetClothData().GetVertexPosition(clothDisplayMap[i0])) );
		}

		// entry can't be negative
		//		if(clothDisplayMap[i1] != -1)
		{
			p1.Set( VEC3V_TO_VECTOR3(cloth->GetClothData().GetVertexPosition(clothDisplayMap[i1])) );
		}

		// entry can't be negative
		//		if(clothDisplayMap[i2] != -1)
		{
			p2.Set( VEC3V_TO_VECTOR3(cloth->GetClothData().GetVertexPosition(clothDisplayMap[i2])) );
		}

		Vector3 v01 = p1 - p0;
		Vector3 v02 = p2 - p0;
		Vector3 v12 = p2 - p1;

		if((v01.Mag2() == 0.0f) || (v02.Mag2() == 0.0f) || (v12.Mag2() == 0.0f))
		{
			Errorf("Polygon has zero-length edge<%d>",
				i);
			Errorf("\t vertex indices<%d %d, %d %d, %d %d>", 
				i0, clothDisplayMap[i0], 
				i1, clothDisplayMap[i1], 
				i2, clothDisplayMap[i2]);
			Errorf("\t vertex positions <%f %f %f> <%f %f %f> <%f %f %f>", 
				p0.x, p0.y, p0.z, 
				p1.x, p1.y, p1.z, 
				p2.x, p2.y, p2.z);
			Errorf("\t UVs <%f %f>,<%f %f>,<%f %f>",
				editor.GetUV(i0, 0).x, editor.GetUV(i0, 0).y,
				editor.GetUV(i1, 0).x, editor.GetUV(i1, 0).y,
				editor.GetUV(i2, 0).x, editor.GetUV(i2, 0).y);
			badPolygonFound = true;
		}
	}

	pIndexBuffer->UnlockRO();

	return badPolygonFound;
}
#endif


const Vector3 ClothTolerance(2e-3f,2e-3f,2e-3f);


#if MESH_LIBRARY
void clothController::GenerateMeshBoundMap( int clothLodIndex, Vector3* vPos )
{
	phVerletCloth* cloth = GetCloth( clothLodIndex );
	Assert( cloth );
	const phClothData& clothData = cloth->GetClothData();
	int numVerts = cloth->GetNumVertices();

	SetMeshVertCount( numVerts, clothLodIndex );

	GetBridge()->AllocateDisplayMappings( clothLodIndex );

	u16* clothDisplayMap = GetClothDisplayMap( clothLodIndex );

#if __DEV
	// Keep track of the closest vertex pair.
	Vector3 nearest(VEC3_MAX);
#endif

	// NOTE: in the case of character cloth those verts should be skinned already
	Assert( vPos );

	int numMeshVerts= GetMeshVertCount( clothLodIndex );
	int posOffset	= -1;
	u8* basePtr		= 0;
	int vtxStride	= 0;

	basePtr			= (u8*)clothData.GetVertexPointer();

	vtxStride		= 16;
	posOffset		= 0;

	Assert( numMeshVerts > 0 );
	Assert( posOffset > -1 );
	Assert( basePtr );
	Assert( vtxStride > 0 );

	for (int meshVertIndex=0; meshVertIndex < numMeshVerts; ++meshVertIndex)
	{
		const Vector3& vMeshPos = vPos[meshVertIndex];

		for (int idx = 0; idx < numMeshVerts/*numBoundVerts*/; ++idx )
		{
			// Get the position of this bound vertex, with the offset, and see if it is close to the mesh vertex.
			const Vector3 pos = *((Vector3*)(basePtr + vtxStride*idx + posOffset));
			if( vMeshPos.IsClose( pos, ClothTolerance ))
			{
				// This bound vertex is sufficiently close to the mesh vertex, so map it.
				clothDisplayMap[meshVertIndex] = (u16)idx;
				break;
			}

#if __DEV
			// Keep track of the closest distance of a bound vertex to this mesh vertex.
			Vector3 diff(pos - vMeshPos);
			Vector3 mag2 = diff.Mag2V();
			nearest.Min(nearest, mag2);
#endif
		}
	}
}

#endif // MESH_LIBRARY

void clothController::OnClothVertexSwap( int swapIndexA, int swapIndexB, int lodIndex, bool swapEdges )
{
	u16* clothDisplayMap = GetClothDisplayMap( lodIndex );
	Assert( clothDisplayMap );

	phVerletCloth* cloth = m_Cloth[lodIndex];
	Assert( cloth );
	const int numVerts = cloth->GetNumVertices();
	for( int k = 0; k < numVerts; k++ )	
	{
		if( clothDisplayMap[k] == swapIndexA )
		{
			clothDisplayMap[k] = (u16)swapIndexB;
		}
		else if( clothDisplayMap[k] == swapIndexB )
		{
			clothDisplayMap[k] = (u16)swapIndexA;
		}
	}	


	if( swapEdges )
	{		
		cloth->SwapVertexInEdges(swapIndexA, swapIndexB); // Swap the old and new vertices in all the connecting edges.
	}

	GetBridge()->SwapVertexPinningData(swapIndexA,swapIndexB,lodIndex);

	phMorphController* morphController = GetMorphController();
	if (morphController)
	{
		phMapData* mapData = morphController->m_MapData[lodIndex];
		if( mapData )
		{
			for( int i = 0; i < LOD_COUNT; ++i )
			{
				const int indexCount = morphController->GetIndexMapCount(lodIndex, i);
				if( indexCount > 0 )
				{					
					for( int j = 0; j < indexCount; ++j )
					{
						if( (int)mapData->m_IMap[i][j] == (swapIndexA) )
						{
							mapData->m_IMap[i][j] =  (u16)(swapIndexB);
						}
						else if( (int)mapData->m_IMap[i][j] == (swapIndexB) )
						{
							mapData->m_IMap[i][j] =  (u16)(swapIndexA);
						}
					}
				}
			}
		}
	}

}

grmGeometry& clothController::GetGeometry(rmcDrawable* drawable) const
{
	FastAssert(drawable);
	const rmcLodGroup &group = drawable->GetLodGroup();
	const rmcLod &lod = group.GetLod( GetLOD() );
	const grmModel *model = lod.GetModel( 0 /*GetModelIdx()*/);
	FastAssert(model);
	return model->GetGeometry( 0 /*GetGeometryIdx()*/);
}

void clothController::Morph( const int hiLOD, const int lowLOD )
{
	PF_FUNC( ClothMorph );	

	// TODO: place weight level somewhere !
	phMapData::enWEIGHT_LEVEL wLevel = phMapData::enWEIGHT_ONE;

	phMorphController* morphController = GetMorphController();
	Assert( morphController );
	morphController->Morph( hiLOD, lowLOD
		, (Vec3V*)GetCloth( hiLOD )->GetClothData().GetVertexPointer()
		, (Vec3V*)GetCloth( lowLOD )->GetClothData().GetVertexPointer()
		, wLevel
		, phMapData::enWEIGHT_ONE
		);
}

#if __PFDRAW
#define		DBG_SPH_RADIUS		0.01f
#endif 


void clothController::ApplyPinning( Mat34V_In attachedToFrame, int simLodIndex )
{
	Assert( simLodIndex > -1 && simLodIndex < LOD_COUNT );
	phVerletCloth* cloth = GetCloth(simLodIndex);
	Assert(cloth);

	phClothData& clothData = cloth->GetClothData();
#if NO_PIN_VERTS_IN_VERLET
	const int numPinnedVerts = clothData.GetNumPinVerts();
#else
	const int numPinnedVerts = cloth->GetPinCount();
#endif
	if( numPinnedVerts )
	{
		const Vec3V* RESTRICT pinVerts = clothData.GetPinVertexPointer();
		Assert( pinVerts );

		const int prevPosCount = clothData.GetPrevPositionVertexCount();
		if( !prevPosCount)
		{
			for (int i=0; i < numPinnedVerts; ++i )
			{
				clothData.SetVertexPosition( i, Transform(attachedToFrame, pinVerts[i]) );
			}
		}
		else
		{
			Vec3V* RESTRICT damageOffsets = clothData.GetVertexPrevPointer();
			Assert(damageOffsets);
			for (int i=0; i < numPinnedVerts; ++i )
			{
				clothData.SetVertexPosition( i, Transform(attachedToFrame, Add( pinVerts[i], damageOffsets[i]) ) );
			}
		}		
	}
}


clothBridgeSimGfx* clothController::CreateBridge()
{
	Assert(!m_Bridge);
	m_Bridge = rage_new clothBridgeSimGfx();
	Assert( m_Bridge );
	SetFlag(CLOTH_IsBridgeOwned,true);
	return m_Bridge;
}


#if MESH_LIBRARY
bool clothController::IsSoftPinned( const mshMaterial* mtl ) const
{
	Assert( mtl );

	int nChannel = mtl->FindChannel(mshVtxBlindCLOTHPIN);
	int nRadiusChannel = mtl->FindChannel(mshVtxBlindCLOTHPINRADIUS);

	if( nChannel >= 0 )
	{
		const mshChannel& channel = mtl->GetChannel(nChannel);
		int nVerts = channel.GetVertexCount();

		const mshChannel *radiusChannel = NULL;
		if( nRadiusChannel >= 0 )
		{
			radiusChannel = &(mtl->GetChannel(nRadiusChannel));
			Assert( radiusChannel->GetVertexCount() == nVerts );
		}

		for( int i = 0; i < nVerts; i++ )
		{
			const mshChannelData& data = channel.Access(i, 0);

			float radPin = 0.0f;

			if( nRadiusChannel >= 0 )
			{
				const mshChannelData& radData = radiusChannel->Access(i, 0);
				radPin = radData.f;
			}

			// check it's soft pinned, if we find one soft pinned value, use soft pinning
			if( !(data.f >= (1.0f - SMALL_FLOAT) && fabsf(radPin) < SMALL_FLOAT) && (data.f > SMALL_FLOAT || fabsf(radPin) > SMALL_FLOAT ) )
			{
				return true;

			}
		}

	}

	return false;
}


void clothController::PinClothVerts( int lodIndex, const mshMaterial* mtl, phClothConnectivityData *connectivity, bool allocNormals, bool softPinned )
{
	Assert( mtl );
	int vertexCount = mtl->GetVertexCount();

	Assert( vertexCount > 0 );
	int* pinMap = Alloca(int, vertexCount );

	float clothWeight;
	const int numPinnedVerts = GetBridge()->CreatePinData( 0, lodIndex, mtl, vertexCount, pinMap, softPinned, clothWeight );

	Assert( numPinnedVerts );

	// Pin the verts in the cloth simulation
	sysMemStartTemp();
	atArray< int > pinList;
	pinList.Resize( numPinnedVerts );
	sysMemEndTemp();

	for (int pinnedVertIndex = 0; pinnedVertIndex < numPinnedVerts; ++pinnedVertIndex )
	{
		pinList[pinnedVertIndex] = pinMap[pinnedVertIndex];
	}

	atFunctor4< void, int, int, int, bool > funcSwap;
	funcSwap.Reset< clothController, &clothController::OnClothVertexSwap >( this );

	datOwner<phVerletCloth> cloth = GetCloth( lodIndex );
	Assert( cloth );
	cloth->PinVerts( lodIndex, pinList, connectivity, &funcSwap, NULL, allocNormals );
	cloth->SetClothWeight(clothWeight);

	sysMemStartTemp();
	pinList.Reset();
	sysMemEndTemp();
}

void clothController::Load( int extraVerts, mshMaterial* mtl, Vec3V_In extraOffsetV, int lodIndex, const char* entityName, phClothConnectivityData* connectivity )
{
	Assert( entityName );

	// NOTE: removing the ".mesh" extension
	const char* dotChar = strstr(entityName,".");
	if( dotChar )
	{
		char controllerName[MAX_CLOTHCONTROLLER_NAME];
		const u32 nameLen = (u32)(dotChar-entityName)+1;
		formatf(controllerName, (nameLen < MAX_CLOTHCONTROLLER_NAME? nameLen: MAX_CLOTHCONTROLLER_NAME), "%s", entityName );
		SetName(controllerName);
	}
	else
		SetName( entityName );

	Assert( mtl );

	//	SetModelIdx(nModel);
	//	SetGeometryIdx( 0 );

	// NOTE: No need to skin for env cloth
	const int vertsClothCount =  mtl->GetVertexCount();
	Assert( vertsClothCount );
	Vector3* vertsClothSkinned = Alloca(Vector3, vertsClothCount);	
	Assert( vertsClothSkinned );
	for(int i = 0; i < vertsClothCount; ++i)
		vertsClothSkinned[i] = mtl->GetPos(i);

	sysMemStartTemp();
	phBoundGeometry* clothBound = GenerateMeshBound( mtl, vertsClothSkinned );
	Assert( clothBound );
	sysMemEndTemp();

	const int numPolygons = clothBound->GetNumPolygons();

	Assert( m_MorphController );			
	int indexCount =  numPolygons * 3;
	m_MorphController->CreateIndexMap( lodIndex, lodIndex, indexCount );
	m_MorphController->m_MapData[lodIndex]->m_PolyCount = clothBound->GetNumPolygons();
	POLYGON_INDEX* clothIndex = (POLYGON_INDEX*)m_MorphController->GetMap(lodIndex,lodIndex);
	for(int i = 0; i < numPolygons; ++i)
	{
		const phPolygon& p = clothBound->GetPolygon(i);
		*clothIndex++ = p.GetVertexIndex(0);
		*clothIndex++ = p.GetVertexIndex(1);
		*clothIndex++ = p.GetVertexIndex(2);
	}

	Assert( connectivity );
	sysMemStartTemp();

	int numEdges =  phVerletCloth::CountEdges( clothBound->GetPolygonPointer(), clothBound->GetNumPolygons() );

	connectivity->Init(clothBound->GetNumVertices(),numEdges,numPolygons);
	connectivity->BuildConnectivity(clothBound->GetPolygonPointer(),numPolygons);
	sysMemEndTemp();

	// Create the cloth
	phVerletCloth* pCloth = rage_new phVerletCloth;
	Assert( pCloth );
	m_Cloth[ lodIndex ] = pCloth;
	bool allocNormals = false;
	pCloth->InitWithBound( connectivity->m_EdgeToVertexIndices, *clothBound, allocNormals, extraVerts );

	GenerateMeshBoundMap( lodIndex, vertsClothSkinned );
	bool softPinned = IsSoftPinned( mtl );

	PinClothVerts( lodIndex, mtl, connectivity, allocNormals, softPinned );

	// TODO: ... read some flag first	
	//	cloth->AddCustomEdges(connectivity);


// TODO: test well, extra offset is applied because cloth sim mesh "center" is not coinceed with fragment center ??, Gunnar asked about this
	phClothData& clothData = pCloth->GetClothData();
	Vec3V* pVerts = clothData.GetVertexPointer();
	for( int i = 0; i < pCloth->GetNumVertices(); ++i)
	{
		pVerts[i] = Add( pVerts[i], extraOffsetV );
	}


	sysMemStartTemp();
	if (phConfig::IsRefCountingEnabled())
	{
		clothBound->Release();
	}
	else
	{
		delete clothBound;
	}
	sysMemEndTemp();

// TODO: remove it, I don't think bound volume should be compute in resource time
//	pCloth->ComputeBoundingVolume();

	if( phVerletCloth::sm_tuneFilePath )
	{
		ASSET.PushFolder( phVerletCloth::sm_tuneFilePath );
		const char* clothName = GetName();
		char buff[256];
		formatf( buff, "%s%s", clothName, phVerletCloth::sm_MetaDataFileAppendix );
		if( ASSET.Exists(buff, "xml") )
		{
			pCloth->Load( const_cast<char*>(clothName), "" );
		}

		ASSET.PopFolder();
	}
}
#endif			// MESH_LIBRARY

void clothController::AllocateCloth ()
{
	Assert(!m_Cloth[0]);
	m_Cloth[0] = rage_new phVerletCloth;
}


void clothController::GetVertexNormalsFromDrawable( Vector3* RESTRICT vertNormals, const rmcDrawable* pDrawable ) const
{
	PF_FUNC(GetVertexNormalsFromDrawable);

	Assert( pDrawable );
	const rmcLodGroup& group = pDrawable->GetLodGroup();
	const int drawLODIndex = group.GetLodIndex();
	const rmcLod* pLod = &group.GetLod( drawLODIndex );
	Assert( pLod );

	const grmModel* pModel = pLod->GetModel( 0 /*GetModelIdx()*/);
	AssertMsg(pModel,"[clothController] model is missing" );
	grcVertexBuffer* pMeshBuffer = pModel->GetGeometry( 0 /*GetGeometryIdx()*/).GetVertexBuffer();
	Assert( pMeshBuffer );

	int simLODIndex = GetLOD();
	int nVert = GetCloth( simLODIndex )->GetNumVertices();
	const phMorphController* morphController = GetMorphController();

	Assert( sizeof(u16) == sizeof(POLYGON_INDEX) );
	Assert( (simLODIndex == drawLODIndex ) || (morphController && (drawLODIndex < simLODIndex) && (nVert == morphController->GetIndexMapCount(drawLODIndex, simLODIndex))) );

	const POLYGON_INDEX* indexMap = (const POLYGON_INDEX*)((simLODIndex == drawLODIndex ) ? GetClothDisplayMap( drawLODIndex ): morphController->GetMap( drawLODIndex, simLODIndex ));
	Assert( indexMap );

	// TODO: fix it
	//	Assert( pMeshBuffer->GetFvf()->GetDataSizeType(grcFvf::grcfcNormal) != grcFvf::grcdsPackedNormal );

	grcVertexBufferEditor editor(pMeshBuffer,true, true);

	const int normalOffset = m_NormalOffset;
	Assert( normalOffset );

	// TEMP: B*1731376
	if( pMeshBuffer->GetVertexStride() != 32 )
	{
		return;
	}
	GetVertexNormals( vertNormals, normalOffset, pMeshBuffer->GetVertexStride(), (u8*)editor.GetVertexBuffer()->GetFastLockPtr(), indexMap, nVert, pMeshBuffer->GetFvf()->GetTangentChannel(0) );
}

}  // namespace rage
