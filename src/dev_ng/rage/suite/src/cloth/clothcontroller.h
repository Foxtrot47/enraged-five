// 
// cloth/clothcontroller.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CLOTH_CONTROLLER_H
#define CLOTH_CONTROLLER_H

#include "mesh/mesh.h"
#include "rmcore/drawable.h"
#include "grcore/vertexbuffer.h"
#include "grmodel/geometry.h"  // MAX_VERT_BUFFERS
#include "phbound/boundgeom.h"
#include "pheffects/cloth_verlet.h"
#include "pheffects/clothverletinst.h"
#include "data/resource.h"
#include "cloth/clothbridgesimgfx.h"
#include "data/resourcehelpers.h"
#include "data/struct.h"


namespace rage
{
#define		MAX_CLOTHCONTROLLER_NAME		(28)

class clothBase : public pgBase
{
public:
	virtual void UpdateRopePostSpu() {}
	virtual void UpdateClothPostSpu() = 0;
};


class clothController: public pgBase
{
public:

	clothController(class datResource& rsc);
	clothController();
	virtual ~clothController();

	DECLARE_PLACE(clothController);

#if CLOTH_INSTANCE_FROM_DATA
	void InstanceFromData(int vertsCapacity, int edgesCapacity);
#endif

	void InstanceFromTemplate( const clothController* copyme );
	void Shutdown();
	void AllocateCloth ();

	void ApplySimulationToMesh( grmGeometryQB& geometry, const Vector3& translationV, int renderableLodIndex, Vec3V_In vDebugOffset );
	void ApplyPinning (Mat34V_In attachedToFrame, int simLodIndex = -1);

#if MESH_LIBRARY
	void Load( int extraVerts, mshMaterial* mtl, Vec3V_In extraOffsetV, int lodIndex, const char* entityName, phClothConnectivityData* connectivity=NULL );
	bool IsSoftPinned( const mshMaterial* mtl ) const;
	void PinClothVerts( int lodIndex, const mshMaterial* mtl, phClothConnectivityData *connectivity, bool allocNormals, bool softPinned );
#endif

	void GetVertexNormalsFromDrawable( Vector3* RESTRICT vertNormals, const rmcDrawable* pDrawable ) const;
	void GetVertexNormals( Vector3* RESTRICT vertNormals, u32 normalOffset, u32 vertexStride, u8* blockPtr, const POLYGON_INDEX* RESTRICT indexMap, int nVerts, bool packedNormals ) const;	

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	int GetMeshVertCount (int lodIndex) const { return GetBridge()->GetMeshVertCount(lodIndex); }
	void SetMeshVertCount (int p, int lodIndex) { GetBridge()->SetMeshVertCount(p, lodIndex); }

	const	u16* GetClothDisplayMap (int lodIndex) const { Assert(GetBridge()); return GetBridge()->GetClothDisplayMap(lodIndex); }
	u16* GetClothDisplayMap (int lodIndex)		 { Assert(GetBridge()); return GetBridge()->GetClothDisplayMap(lodIndex); }

	void SwitchLOD( int newLodIndex, int oldLodIndex, int mapIndex, bool shouldMorph );
	void SetLOD(char p) { m_LOD = p; }
	char GetLOD() const { return m_LOD; }	

#if __DEV
	bool CheckForBadPolygons ( int lodIndex, const rmcDrawable* pDrawable ) const;
#endif

	void OnClothVertexSwap( int swapIndexA, int swapIndexB, int lodIndex, bool swapEdges );

	grmGeometry& GetGeometry(rmcDrawable* drawable) const;


	clothBridgeSimGfx* CreateBridge();
	clothBridgeSimGfx* GetBridge() { return m_Bridge; }
	const clothBridgeSimGfx* GetBridge() const { return m_Bridge; }	

	void Morph(  const int controlledMeshIdx, const int controllerMeshIdx );

	datOwner<phVerletCloth> GetCurrentCloth() { return m_Cloth[ (int)GetLOD() ]; }
	datOwner<phVerletCloth> GetCloth(int lodIndex) { return m_Cloth[lodIndex]; }
	phVerletCloth* GetCloth(int lodIndex) const { return m_Cloth[lodIndex]; }

	void SetSwitchDistanceUp( int lodIndex, float distance ) { if( m_Cloth[lodIndex] ) { m_Cloth[lodIndex]->GetClothData().SetSwitchDistanceUp( distance );	} }
	void SetSwitchDistanceDown( int lodIndex, float distance ) { if( m_Cloth[lodIndex] ) { m_Cloth[lodIndex]->GetClothData().SetSwitchDistanceDown( distance );	} }
	float GetSwitchDistanceUp( int lodIndex ) const { Assert( m_Cloth[lodIndex] ); return m_Cloth[lodIndex]->GetClothData().GetSwitchDistanceUp(); }
	float GetSwitchDistanceDown( int lodIndex ) const { Assert( m_Cloth[lodIndex] ); return m_Cloth[lodIndex]->GetClothData().GetSwitchDistanceDown(); }

	void  SetClothInstance(void* ptr) { Assert( ptr ); m_ClothInstance = ptr; }
	void* GetClothInstance() const { return m_ClothInstance; }
	void* GetOwner() const { return m_Owner; }
	void  SetOwner( void* ptr ) { Assert( ptr ); m_Owner = ptr; } // owner is either enviromentCloth or characterCloth

	void  SetName(const char* name) { Assert( name ); formatf( m_Name, sizeof(m_Name), "%s", name); }
	const char* GetName() const { return m_Name; }

#if MESH_LIBRARY || CLOTH_INSTANCE_FROM_DATA
	phMorphController* CreateMorphController()
	{
		m_MorphController = rage_new phMorphController; 
		Assert( m_MorphController );
		SetFlag(CLOTH_IsMorphControllerOwned,true);
		return m_MorphController;
	}
#endif // MESH_LIBRARY

	const phMorphController* GetMorphController() const { return m_MorphController.ptr; }
	phMorphController* GetMorphController()		{ return m_MorphController.ptr; }

	float GetClothWeight() const { return GetCloth(GetLOD())->GetClothWeight(); }
	void  SetClothWeight(float weight) { GetCloth(GetLOD())->SetClothWeight(weight); }

	void  SetNormalOffset(char offset) { m_NormalOffset = offset; }
	void  SetUVOffset(char offset) { m_UVOffset = offset; }

	void  SetIndicesWindingOrder(bool windingOrder) { m_IndicesWindingOrder = windingOrder; }

#if !__SPU
	void Load();
	void Save();

	void AddWidgets(bkBank& bank);
	void RegisterRestInterface();
	void UnregisterRestInterface();
#endif

	enum enFlags
	{
		CLOTH_IsMorphControllerOwned	=	1 << 0,
		CLOTH_IsBridgeOwned				=	1 << 1,
		CLOTH_CaptureVertsForReplay		=	1 << 2,
		CLOTH_CollectVertsFromReplay	=	1 << 3,
	};

	bool GetFlag(enFlags flagToCheck) const { return (m_Flags & flagToCheck) ? true: false;	}
	void SetFlag(enFlags flagToSet, bool isTrue)
	{
		if( isTrue )
			m_Flags |= flagToSet;
		else
			m_Flags &= (~flagToSet);
	}

	static const int sm_VertexBufferCount = MAX_VERT_BUFFERS;
	static const int RORC_VERSION = 2 + phMorphController::RORC_VERSION;

protected:

#if MESH_LIBRARY
	phBoundGeometry* GenerateMeshBound( mshMaterial* mtl, Vector3* vPos );
	void GenerateMeshBoundMap( int clothLodIndex, Vector3* vPos );
#endif

	datOwner<clothBridgeSimGfx>	m_Bridge;
	datOwner<phMorphController>	m_MorphController;
	datOwner<phVerletCloth>		m_Cloth[LOD_COUNT];

	void*						m_ClothInstance;
	void*						m_Owner;

	u32							m_Flags;

	char						m_NormalOffset;
	char						m_UVOffset;
	char						m_LOD;
	bool						m_IndicesWindingOrder;

	char						m_Name[MAX_CLOTHCONTROLLER_NAME];
};




} // namespace rage

#endif // CLOTH_CONTROLLER_H
