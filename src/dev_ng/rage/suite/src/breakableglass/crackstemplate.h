//
// breakableglass/crackstemplate.h
//
// Copyright (C) 2008 Rockstar Games.  All Rights Reserved. 
//

// crackstemplate.h
//
// This class holds templates for all the glass cracking/breakage patterns for all 
// combinations of [glass type, weapon / ammo type]
//
// Usage: 
//        Call bgCracksTemplate::InitClass() specifying the arrays of glass types and crack types.
//        The initialization data folder should have a sub-folder named for each 
//        glass type and within that folder an .xml file whose name matches each 
//        crack type name.
//        Crack types and glass types are susequently referenced by index within 
//        the arrays initially passed to InitClass().
//
// -------------------------------------------------------------------------------------------------

#ifndef BREAKABLEGLASS_CRACKSTEMPLATE_H
#define BREAKABLEGLASS_CRACKSTEMPLATE_H

#include "edge.h"
#include "piecegeometry.h"
#include "atl/array.h"
#include "atl/bintree.h"
#include "atl/referencecounter.h"
#include "atl/singleton.h"
#include "parser/macros.h"
#include "shaderlib/rage_constants.h"
#include "vector/Vector3.h"
#include "vector/matrix34.h"
#include "grcore/texture.h"
#include "grcore/effect.h"

namespace rage {

class grcIndexBuffer;
class grcVertexBuffer;

//////////////////////////////////////////////////////////////////////////
//
//
// A single solid piece, may be concave
//
class bgOneSolidPiece
{
public:
	bgArray<bgEdgeRef> m_silhouette; // (potentially concave) silhouette of the entire piece; 
									 // edges reference other pieces as well as the two endpoints
	bgArray<bgPolygon> m_polygons;   // Decomposition of piece into convex polygons. References points directly.

	int GetDynamicStorage() const
	{
		int storage = m_silhouette.GetCapacity() * sizeof(bgEdgeRef);
		storage += m_polygons.GetCapacity() * sizeof(bgPolygon);
		for (int iPolygon = 0; iPolygon < m_polygons.GetCount(); iPolygon++)
		{
			const bgPolygon& polygon = m_polygons[iPolygon];
			storage += polygon.GetCapacity() * sizeof(bgIndex);
		}
		return storage;
	}
};

//
// Centralized repository for all points and edges as well as pieces;
// points and edges may be referenced by multiple pieces; edges can
// be used directly to determine connectivity between pieces
//
class bgCrackMesh
{
public:
	//Interface for bgCrackMeshPointAdder<bgCrackMesh>
	void Add(Vector2 const &point)
	{
		m_points.Grow() = point;
	}

	int GetPointCount() const
	{
		return m_points.GetCount();
	}

	//Interface for bgEdgeAdder<bgCrackMesh>
	void Add(bgEdge const &edge)
	{
		m_edges.Grow() = edge;
	}

	int GetEdgeCount() const
	{
		return m_edges.GetCount();
	}

	bgIndex GetP0(bgEdgeRef ref) const
	{
		return ref.GetP0(m_edges);
	}

	bgIndex GetP1(bgEdgeRef ref) const
	{
		return ref.GetP1(m_edges);
	}

	bgIndex GetLeftPoly(bgEdgeRef ref) const
	{
		return ref.GetLeftPoly(m_edges);
	}

	bgIndex GetRightPoly(bgEdgeRef ref) const
	{
		return ref.GetRightPoly(m_edges);
	}

	void SetLeftPoly(bgEdgeRef ref, bgIndex in_polygon)
	{
		ref.SetLeftPoly(m_edges, in_polygon);
	}

	void SetRightPoly(bgEdgeRef ref, bgIndex in_polygon)
	{
		ref.SetRightPoly(m_edges, in_polygon);
	}

	// all silhouette edges
	bgArray<bgEdge> m_edges;
	// all points
	atArray<Vector2> m_points;
	// all solid pieces
	atArray<bgOneSolidPiece> m_pieces;
};

//////////////////////////////////////////////////////////////////////////
//
// bgTextureVarData: texture shader var info
//
class bgTextureVarData
{
public:
	virtual ~bgTextureVarData()
	{
	}

	// parsed from XML
	ConstString m_varName;
	ConstString m_textureName;

	PAR_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////
//
// bgFloat4VarData: texture shader var info
//
class bgFloat4VarData
{
public:
	virtual ~bgFloat4VarData()
	{
	}

	ConstString m_varName;
	Vector4     m_value;

	PAR_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////
//
// bgFloat2VarData: texture shader var info
//
class bgFloat2VarData
{
public:
	virtual ~bgFloat2VarData()
	{
	}

	ConstString m_varName;
	Vector2     m_value;

	PAR_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////
//
// bgFloatVarData: texture shader var info
//
class bgFloatVarData
{
public:
	virtual ~bgFloatVarData()
	{
	}

	ConstString m_varName;
	float     m_value;

	PAR_PARSABLE;
};


////////////////////////////////////////////////////////////////////////////////
// A class to hold one set of crack/star map data
////////////////////////////////////////////////////////////////////////////////
class bgCrackStarMap
{
public:

	// Constructor
	bgCrackStarMap() : m_pCrackMesh(NULL)
	{
		m_cracks = "Undefined";
		// default lod switching table for backwards-compatibility
		for(int i=bgLod::LOD_VLOW; i <= bgLod::LOD_HIGH; i++)
			m_LodPixelSizeArray[i] = m_DefLodPixelSizeArray[i];
	}

	// Destructor
	virtual ~bgCrackStarMap()
	{
	}

	// Data from XML
	rage::ConstString m_cracks;
	typedef atRangeArray<float,bgLod::LOD_COUNT> LodArray;
	
	// Default values for the LOD array
	static const float m_DefLodPixelSizeArray[bgLod::LOD_COUNT];

#if __BANK
	static LodArray m_TuneLodPixelSizeArray;
#endif // __BANK

	LodArray m_LodPixelSizeArray; // pixel size at which to switch lod
	atArray<bgFloat4VarData> m_float4Vars;
	atArray<bgFloat2VarData> m_float2Vars;
	atArray<bgFloatVarData> m_floatVars;
	atArray<bgTextureVarData> m_textureVars;

	// Generated data
	bgCrackMesh* m_pCrackMesh;
	atArray<Vector4> m_float4VarValues;
	atArray<Vector2> m_float2VarValues;
	atArray<float> m_floatVarValues;
	atArray<grcTextureHandle> m_textureVarValues;

	PAR_PARSABLE;
};//bgCrackStarMap
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// A class to hold all the information about a particular type of crack
////////////////////////////////////////////////////////////////////////////////
class bgCrackType
{
public:
	// Constructor
	bgCrackType();
	virtual ~bgCrackType();

	void Init();
	void Term();

	bool Load(const char *filename);
#if __BANK
	bool Save(const char *filename);
#endif
	// Array of maps for this particular crack type
	typedef rage::atArray<bgCrackStarMap> CrackMapArray;
	CrackMapArray m_crackArray;  // Array of crack/star maps

	// Various cracking parameters
	float m_initialImpactKillRadius1;
	float m_initialImpactKillRadius2;
	float m_subsequentImpactKillRadiusMin;
	float m_subsequentImpactKillRadiusMax;
	float m_decalRadiusImpactShowValueMin;
	float m_decalRadiusImpactShowValueMax;
	float m_crackRadiusImpactShowValueMin;
	float m_crackRadiusImpactShowValueMax;
	float m_breakPieceSizeMin;
	float m_breakPieceSizeMax;
	float m_crackInBetweenPiecesSizeMax;
	float m_bevelSizeMax;
	float m_minThickness;
	float m_decalTextureScaleMin;
	float m_decalTextureScaleMax;

	// Sequential hit
	bool m_enableSequentialHit;
	float m_minSequentialHitPieceSize;

	// Enumeration for the decal texture channel that should be used with the template
	enum bgDecalChannel
	{
		ec_red,
		ec_green,
		ec_blue,
		ec_alpha,
		ec_randomRedGreen,
		ec_randomRedGreenAlpha,
		ec_randomAll,
	};
	
	// Decal texture channel used by the template
	bgDecalChannel m_decalTextureChannel;

	//Give more control over scaling and rotation on the crack map.  This is now needed because of the new tempered glass.
	//
	// Random rotation choices: we need more choices now that we might not only have perfectly rotation-able crack map
	enum bgCrackRotationType
	{
		//No rotation applied to our crack map
		er_none,
		//Random rotation between 0 and 360 degree
		er_0_to_360,
		//Random rotation with value that will be either 0, 90, 180, 270.  This give you some randomness without the 1.5x scale applied to the crack map
		// when having a full 360 degree rotation.
		er_stepOf90,
		//Randomly choose a rotation of either 0 and 180.  This give you some randomness without the 1.5x scale applied to the crack map
		// when having a full 360 degree rotation.
		er_flip,
		//Crack map or texture is made for a horizontal rectangle pane of glass.  Expect the data to still be squared but stretch on the vertical 
		// side in order to look un-stretched once applied to the rectangle pane of glass.  If the pane of glass is longer on the vertical side 
		// the code will apply a 90 degree rotation in order to match the pane of glass properly.
		er_rectangle,
		// Same as e_rectangle + either 0 or 180 degree.  This will give some randomness...
		er_rectangleFlip
	};
	//Where do we went the crack to be center on the pane of glass
	enum bgCrackPlacementType
	{
		//Center of crack map will be centered at the location of the impact
		ep_glassImpactLocation,
		//Center of crack map will be on the center of the pane of glass
		ep_glassCenter,
		//Center of crack map will be randomly placed on the pane of glass
		ep_glassRandom
	};
	//How do we want to scale our crack map
	enum bgCrackScalingType
	{
		//No scaling applied:  The crack map will 100% fit on the pan of glass.  No loss of data.
		es_none,
		//Scaling will be set depending of what m_crackMapCenterLocation and m_crackMapRotationType are set with.
		//	Depending of m_crackMapCenterLocation and m_crackMapRotationType, the crack map might be scaled up by a lot.  Might
		//	have a high percentage of the crack map not visible.  Lot of loss of data.
		es_automatic,
		//Longer side will have a scale of 1 and the shorter side will have a scale of "longer/shorter".  Some crack map data
		//	will not be visible.
		es_forceSquared,
		// No other scaling applied outside of the user scaling values entered in m_crackMapOverwriteScalingX and m_crackMapOverwriteScalingY.
		es_overwrite,
		// No other scaling applied outside of the user scaling values entered in  m_crackMapOverwriteScalingX.  m_crackMapOverwriteScalingX 
		//	will be applied on X and Y.
		es_overwriteSquared
	};
	// --------------------------------------
	// THESE ARE PARSED FROM XML
	bgCrackPlacementType	m_crackMapCenterLocation;
	bgCrackRotationType		m_crackMapRotationType;
	bgCrackScalingType		m_crackMapScalingType;
	float					m_crackMapOverwriteScalingX;
	float					m_crackMapOverwriteScalingY;
	// END OF PARSED STUFF
	// --------------------------------------


	atArray<bgFloat4VarData> m_float4Vars;
	atArray<bgFloat2VarData> m_float2Vars;
	atArray<bgFloatVarData> m_floatVars;
	atArray<bgTextureVarData> m_textureVars;

	PAR_PARSABLE;
};//bgCrackType
////////////////////////////////////////////////////////////////////////////////

// List of shader vars used by the breakable glass shader(s)
class bgShaderVars
{
public:
	bgShaderVars();
	virtual ~bgShaderVars() 
	{
	}

	atArray<bgTextureVarData> m_textureVarDefaults;
	atArray<bgFloat4VarData>  m_float4VarDefaults;
	atArray<bgFloat2VarData>  m_float2VarDefaults;
	atArray<bgFloatVarData>  m_floatVarDefaults;
	ConstString m_matrixGlobalVarName; // name of the global matrix variable (usually skinning matrices)
	int         m_matrixCount;  // maximum number of matrices supported
	PAR_PARSABLE;
};

// This is nicer than an atArray<ConstString> because it's easier to convert to 
// a char** for use in creating widgets
class bgParsableStringArray : public atArray<char*>
{
public:
	// delete the parsed strings
	~bgParsableStringArray();
};

class bgGlassSize
{
public:
	virtual ~bgGlassSize()
	{

	}
	bool Init();

	ConstString m_glassSize;
	PAR_PARSABLE;

public:
	float m_height;
	float m_width;
};
class bgGlassTypeConfig
{
public:
	enum bgGlassPieceMaxNum
	{
		gp_Max_Num_Pieces_1x = 1,
		gp_Max_Num_Pieces_2x,
		gp_Max_Num_Pieces_3x,
		gp_Max_Num_Pieces_4x,
		gp_Max_Num_Pieces_5x
	};
	//bgGlassTypeConfig();
	virtual ~bgGlassTypeConfig() 
	{
	}

	ConstString m_glassType;
	bgGlassPieceMaxNum m_maxNumPieces;
	atArray<bgGlassSize> m_glassSizes;

	const ConstString &GetTypeName() const
	{
		return m_glassType;
	}
	bgGlassPieceMaxNum GetNumBatches() const
	{
		return m_maxNumPieces;
	}
	PAR_PARSABLE;
};
class bgConfig
{
public:
	virtual ~bgConfig()
	{
	}

	bgParsableStringArray m_crackTypes;
	atArray<bgGlassTypeConfig> m_glassTypes;

	rage::bgShaderVars m_shaderVars;
	PAR_PARSABLE;
};

#if __BANK
// Memory usage structures
struct bgDebugCrackTypeMemStats
{
	bgDebugCrackTypeMemStats() : geomMemUsage(0), texMemUsage(0), pName(0), numPieces(0), numEdges(0), numPoints(0), numTextures(0) {};
	const char* pName;
	u32 geomMemUsage;
	u32 texMemUsage;
	u16 numPieces;
	u16 numEdges;
	u16 numPoints;
	u16 numTextures;
};

struct bgDebugGlassTypeMemStats
{
	bgDebugGlassTypeMemStats() : pName(0) {};
	const char* pName;
	atArray<bgDebugCrackTypeMemStats> crackTypes;
};

struct bgDebugCrackTextureMemStats
{
	bgDebugCrackTextureMemStats() : size(0), pName(0) {};
	bool  operator ==(const bgDebugCrackTextureMemStats& other) const { return (pName == other.pName && size == other.size); }

	const char* pName;
	u32 size;
};

struct bgDebugCracksTemplateMemStats
{
	atArray<bgDebugGlassTypeMemStats>		glassTypes;
	atArray<bgDebugCrackTextureMemStats>	textures;
};
#endif //  __BANK

////////////////////////////////////////////////////////////////////////////////
// A class to hold the library of our crack data
////////////////////////////////////////////////////////////////////////////////
class bgCracksTemplate
{
public:
	// Con/Destruction
	bgCracksTemplate();
	~bgCracksTemplate();

	// Implementation details for atSingleton
	// Before calling this, set ASSET to point to the root of the glass data
	// for your project.
	// This method only loads config data; call LoadResources() to actually load archetypes.
	void InitClass();

	void ShutdownClass();

	// returns true if bgSCracksTemplate::Instantiate() was called and InitClass() successfully loaded crack type and glass type names.
	// if true is returned, out_pCracksTemplate is set to bgSCracksTemplate::InstancePtr() and out_pErrMsg is not affected
	// if false is returned, out_pCracksTemplate is set to NULL and out_pErrMsg indicates the reason for failure.
	static bool IsInitialized(bgCracksTemplate*& out_pCracksTemplate, const char** out_pErrMsg);

	// returns true if InitClass() successfully loaded crack type and glass type names.
	// if true is returned out_pErrMsg is not affected
	// if false is returned out_pErrMsg indicates the reason for failure.
	bool IsInitialized(const char** out_pErrMsg) const;

	// load all archetypes and textures
	void LoadResources();

	// returns true if IsInitialized() returns true and LoadResources() successfully loaded crack archetypes for
	// all GlassType/CrackType combos
	static bool IsReady(bgCracksTemplate*& out_pCracksTemplate, const char** out_pErrMsg);

	// returns true if LoadResources() successfully loaded crack archetypes for
	// all GlassType/CrackType combos
	bool IsReady(const char** out_pErrMsg) const;

	// list of impact/weapon types
	const atArray<char*>& GetCrackTypes() const;

	// list of types of glass available (pane types)
	const atArray<bgGlassTypeConfig>& GetGlassTypes() const;

	// list of types of glass sizes available for glass type (pane size types)
	const atArray<bgGlassSize>& GetGlassSizes(int in_iGlassType) const;
	
	// Find the crack data associated with the given combination of crack and glass type
	// in_eCrackType is determined by the weapon/ammo/physical responsible for causing the breakage.
	// in_eGlassType is determined by the object being broken.
	bgCrackType* GetCrackData(int in_eGlassType, int in_iGlassSizeIndex, int in_eCrackType);

	//------------------------------------------------------------------------------
	// Compute the center point of a polygon
	//
	// input: polygon:		Points to compute the center from
	//
	Vector2 ComputeCenter(atArray<Vector2>& polygon);

	//------------------------------------------------------------------------------
	// Compute the center point of a polygon
	//
	// input:	polygon:		Points to compute the center from
	//			count			Point count in polygon
	//
	Vector2 ComputeCenter(Vector2* polygon, int count);


	static Vector3 triangleNormal(Vector3* va, Vector3* vb, Vector3* vc);
	static bool IsPointInPoly(rage::Vector2 in_pointInside, rage::Vector2* in_polyPoint, int in_pointCount);

#if __BANK
	// Get memory usage for all loaded bgCrackMesh.
	int GetGeometryMemoryUsage() const;
	int GetTextureMemoryUsage() const;
	void PrintMemoryUsageStats(bool bShowGlassInfo, bool bShowTextureInfo) const;
#endif

	const bgShaderVars& GetShaderVars() const
	{
		return m_config.m_shaderVars;
	}

private:

#if __BANK
	// Cache memory usage info
	void CacheMemoryUsageStats();
	bgDebugCracksTemplateMemStats m_memStats;
#endif

	// Helpers for setting up crack library
//	void LoadPaneTextFile(bgCrackSet& in_set, const char* m_fileName);
//	bgCrackSet* GetOrLoadCrackSet(const char* in_filename);

	static void LoadCrackMesh(bgCrackMesh& in_set, const char* m_fileName);

	void LoadCrackData(bgCrackType* in_pCrackData);

	bgCrackMesh* GetOrLoadCrackMesh(const char* in_filename);

	rage::grcTexture* GetOrLoadTexture(const char* in_filename);

	bool InitErrLogf(const char* file, int line, const char* fmt, ...);

	// lists of crack and glass types as well as 
	// shader vars used by this project's breakable glass shader(s)
	bgConfig m_config;

	// All our different crack data sorted by type
	rage::atArray<bgCrackType> m_glassCrackArray;

	// A pool of all the crack data files loaded (hash using name string)
	typedef atMap<const char*, bgCrackMesh*> CrackMeshPool;
	CrackMeshPool m_crackMeshPool;

	// A pool of the textures we've loaded
	typedef rage::atBinTree<rage::u32, rage::grcTexture*> TexturePool;
	TexturePool m_texturePool;

	ConstString m_InitErrMsg;

	//Store total number of glass size types specified in glassconfig.xml
	int m_TotalGlassSizeTypes;
};//bgCracksTemplate

typedef atSingleton<bgCracksTemplate> bgSCracksTemplate;

//-----------------
//Array of hit creating cracks and pieces:  When a hit happen, data get added to this array 
//for each hit.
class bgOneHit
{
public:
	bgOneHit() :
	  m_scaleData(2.0f, 2.0f),
		  m_location(0.0f, 0.0f),
		  m_rotation(0.f),
		  m_worldImpactVector(0.0f, 0.0f, 0.0f),
		  m_pGlassCrack(NULL),
		  m_pCrackStarMap(NULL)
	  {
	  }
	  ~bgOneHit()
	  {
		  m_scaleData.Set(2.0f, 2.0f);
		  m_location.Set(0.0f, 0.0f);
		  m_rotation = 0.f;
		  m_worldImpactVector.Set(0.0f, 0.0f, 0.0f);
		  m_pGlassCrack = NULL;
		  m_pCrackStarMap = NULL;
	  }

	  //Data scale:  How much to scale up the data in our library:  Minimum scale is 2.
	  Vector2 m_scaleData;

	  //Location of the impact on the window.  Value between 0 and 1
	  Vector2 m_location;

	  // 0->2PI rotation angle applied to crack pattern
	  float m_rotation;

	  // Impact location in world space
	  Vector3 m_worldImpactVector;

	  // Pointers to the actual crack data
	  const bgCrackType* m_pGlassCrack;
	  const bgCrackStarMap* m_pCrackStarMap;
};

// a reference counted class that manages buffers (i.e., index and vertex) used by the GPU
// This is just the public interface; see bgGpuBuffers_Imp in bgDrawable.cpp for implementation
class bgGpuBuffers : public atReferenceCounter
{
public:
	//
	// Constructor
	//
	bgGpuBuffers()
		: m_pVertexBuffer(NULL)
		, m_pIndexBuffer(NULL)
	{
	}

	// Index buffer accessor
	grcIndexBuffer* GetIndexBuffer()
	{
		return m_pIndexBuffer;
	}

	// Vertex buffer accessor
	grcVertexBuffer* GetVertexBuffer()
	{
		return m_pVertexBuffer;
	}

	int GetPaneIndexCount() const
	{
		return m_paneIndexCount;
	}

	void SetPaneIndexCount(int in_paneIndexCount)
	{
		m_paneIndexCount = in_paneIndexCount;
	}

protected:
	grcVertexBuffer* m_pVertexBuffer;
	grcIndexBuffer* m_pIndexBuffer;
	int m_paneIndexCount;
};

// A constant for how many matrices are possible
enum bgConstants
{
	MAX_ANIMATED_GLASS_PIECES = SKINNING_COUNT
};

} // namespace rage

#endif // BREAKABLEGLASS_CRACKSTEMPLATE_H
