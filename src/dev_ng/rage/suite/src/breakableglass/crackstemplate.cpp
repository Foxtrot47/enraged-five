//
// breakableglass/crackstemplate.cpp
//
// Copyright (C) 2008-2014 Rockstar Games.  All Rights Reserved. 
//

#include "crackstemplate.h"
#include "optimisations.h"

// Local includes
#include "bgchannel.h"
#include "crackstemplate_parser.h"

// Rage includes
#include "atl/string.h"
#include "file/asset.h"
#include "file/stream.h"
#include "grcore/texture.h"
#include "math/simplemath.h"
#include "parser/manager.h"
#include "system/param.h"
#include "grcore/debugdraw.h"


// optimisations
BG_OPTIMISATIONS()

namespace rage {

bgParsableStringArray::~bgParsableStringArray()
{
	for (int i=0; i<GetCount(); i++)
	{
		delete m_Elements[i];
		m_Elements[i] = NULL;
	}
}

bgShaderVars::bgShaderVars() : 
	  m_matrixGlobalVarName("gBoneMtx")
	, m_matrixCount(SKINNING_COUNT)
{
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// bgCrackMeshPointAdder implementation
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class bgCrackMeshPointAdder
{
public:
	bgCrackMeshPointAdder(bgCrackMesh &mesh) : m_pMesh(&mesh)
	{
	}

	typedef Vector2 Point;
	typedef bgIndex Index;

	Index FindOrAddPoint(Point const &point)
	{
		if (Index const *pIndex = FindPoint(point))
			return *pIndex;
		return Add(point);
	}

private:
	class Key
	{
	public:
		Key(Point const &point)
		{
			m_point[0] = point[0];
			m_point[1] = point[1];
		}

		float &operator [](size_t index)
		{
			return m_point[index];
		}

		float const &operator [](size_t index) const
		{
			return m_point[index];
		}

		friend bool operator ==(Key const &lhs, Key const &rhs)
		{
			return lhs[0] == rhs[0] && lhs[1] == rhs[1];
		}

		friend bool operator !=(Key const &lhs, Key const &rhs)
		{
			return lhs[0] != rhs[0] || lhs[1] != rhs[1];
		}

		struct Hash
		{
			unsigned operator ()(Key const &key) const
			{
				return unsigned(atDataHash(key.m_bytes, sizeof(key.m_bytes), 0));
			}
		};

	private:
		union
		{
			float m_point[2];
			char m_bytes[sizeof(Point)];
		};
	};

	Index const *FindPoint(Key const &key)
	{
		return m_map.Access(key);
	}

	Index Add(Point const &point)
	{
		Index index(Index(m_pMesh->GetPointCount()));
		m_pMesh->Add(point);
		m_map[point] = index;
		return index;
	}

	typedef atMap<Key, Index, Key::Hash> Map;
	Map m_map;
	bgCrackMesh *m_pMesh;
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// bgCrackType Implementation
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Construction
//------------------------------------------------------------------------------
bgCrackType::bgCrackType() :
	m_initialImpactKillRadius1(0.00000004f),
	m_initialImpactKillRadius2(0.00000004f),
	m_subsequentImpactKillRadiusMin(0.004f),
	m_subsequentImpactKillRadiusMax(0.004f),
	m_decalRadiusImpactShowValueMin(0.0225f),
	m_decalRadiusImpactShowValueMax(0.0225f),
	m_crackRadiusImpactShowValueMin(0.0225f),
	m_crackRadiusImpactShowValueMax(0.0225f),
	m_breakPieceSizeMin(0.25f),
	m_breakPieceSizeMax(0.25f),
	m_crackInBetweenPiecesSizeMax(0.003f),
	m_bevelSizeMax(0.01f),
	m_minThickness(0.03f),
	m_decalTextureScaleMin(1.f),
	m_decalTextureScaleMax(1.f),
	m_decalTextureChannel(ec_red),
	//Set the next 5 values to match original value before tempered glass was created.  This should ensure
	//	all glass created previously without those variables will functioned as intended originally.
	m_crackMapCenterLocation(ep_glassImpactLocation),
	m_crackMapRotationType(er_0_to_360),
	m_crackMapScalingType(es_automatic),
	m_crackMapOverwriteScalingX(1.0f),
	m_crackMapOverwriteScalingY(1.0f),
	m_enableSequentialHit(false),
	m_minSequentialHitPieceSize(0.12f)
{
}//Constructor

//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
bgCrackType::~bgCrackType()
{
	bgAssertf(m_crackArray.empty(), "Destroying bgCrackType with existing data." );
}//Destructor

//------------------------------------------------------------------------------
// Initialize the parser for a crack
//------------------------------------------------------------------------------
void bgCrackType::Init()
{
	using namespace rage;

	PARSER.InitObject(*this);
}//Init

//------------------------------------------------------------------------------
// Get rid of the parser and associated data for this class
//------------------------------------------------------------------------------
void bgCrackType::Term()
{
	m_crackArray.Reset();
}//Term

//------------------------------------------------------------------------------
// Load up an instance of crack data
//------------------------------------------------------------------------------
bool bgCrackType::Load(const char *filename)
{
	bool ret = PARSER.LoadObject(filename, "xml", *this);
	return ret;
}//Load

//------------------------------------------------------------------------------
// Save an instance of our crack data
//------------------------------------------------------------------------------
#if __BANK
bool bgCrackType::Save(const char *filename)
{
	return PARSER.SaveObject(filename, "xml", this);
}//Save
#endif

bool bgGlassSize::Init()
{
	const char *glassType = m_glassSize.c_str();

	//Find glass type dimensions
	m_height = -1;
	m_width = -1;

	if(StringLength(glassType) == 3 &&  glassType[1] == 'x')
	{
		if(isdigit(glassType[0]))
		{
			m_width = (float)(glassType[0] - 48);
			if(isdigit(glassType[2]))
			{
				m_height = (float)(glassType[2] - 48);
				return true;
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// bgCracksTemplate Implementation
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const float bgCrackStarMap::m_DefLodPixelSizeArray[bgLod::LOD_COUNT] = { 100.f, 250.f, 500.f, 750.f };

#if __BANK
bgCrackStarMap::LodArray bgCrackStarMap::m_TuneLodPixelSizeArray;
#endif // __BANK

//------------------------------------------------------------------------------
// Constructor: init some values
//------------------------------------------------------------------------------
bgCracksTemplate::bgCracksTemplate()
{
}//Constructor

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
bgCracksTemplate::~bgCracksTemplate()
{
}//Destructor

//avoid throwing errors in the resource compiler if at all possible...
#if __ASSERT
#define bgInitClass_Verifyf(cond, fmt, ...) (Likely(cond) || ((InitErrLogf(__FILE__,__LINE__,fmt,##__VA_ARGS__) || (__debugbreak(),0)),false))
#define bgInitClass_Assertf(cond, fmt, ...) (Likely(cond) || ((InitErrLogf(__FILE__,__LINE__,fmt,##__VA_ARGS__) || (__debugbreak(),0))))
#else
#define bgInitClass_Verifyf(cond, fmt, ...) (Likely(cond))
#define bgInitClass_Assertf(cond, fmt, ...)
#endif

#if __ASSERT && !__NO_OUTPUT
// Log an initialization error.
// In resource compilers, don't throw an error immediately, just cache the err message and show it later if breakable glass was ever actually needed.
bool bgCracksTemplate::InitErrLogf(const char* file, int line, const char* fmt, ...)
{
	va_list args;
	char buffer[512];
	va_start(args,fmt);
	vformatf(buffer,sizeof(buffer),fmt,args);
	va_end(args);
	// cache the error message
	m_InitErrMsg = buffer;
#if __RESOURCECOMPILER
	diagSeverity severity = DIAG_SEVERITY_WARNING;
#else
	diagSeverity severity = DIAG_SEVERITY_ASSERT;
#endif
	return diagLogf(Channel_BreakableGlass,severity,file,line,"bgCracksTemplate::InitClass() FAILED: %s breakable glass will be disabled.",buffer);
}
#endif

namespace
{
#if __ASSERT
	const char* GetFullPath(char* out_buf, int in_maxLen, const char* in_fileName, const char* in_fileExt)
	{
		ASSET.FullPath(out_buf, in_maxLen, in_fileName, in_fileExt);
		return out_buf;
	}
#endif
}

//------------------------------------------------------------------------------
// Initialize our singleton
// Before calling this, set ASSET to point to the root of the glass data
// for your project.
//------------------------------------------------------------------------------
void bgCracksTemplate::InitClass()
{
	// intialize the parser for this class
	INIT_PARSER;

	static const char* configFileName = "glassconfig";
	static const char* configFileExt = "xml";
	ASSERT_ONLY(char buf[RAGE_MAX_PATH];)
	if (bgInitClass_Verifyf(PARSER.LoadObject(configFileName, configFileExt, m_config), "Failed to load %s!", 
			GetFullPath(buf, RAGE_MAX_PATH, configFileName, configFileExt)))
	{
		bgInitClass_Assertf(0 < m_config.m_glassTypes.GetCount(), "No glass types specified in %s!", 
			GetFullPath(buf, RAGE_MAX_PATH, configFileName, configFileExt));
		bgInitClass_Assertf(0 < m_config.m_crackTypes.GetCount(), "No crack types specified in %s!", 
			GetFullPath(buf, RAGE_MAX_PATH, configFileName, configFileExt));
	}

	// Loop over glass types
	int nGlassTypes = m_config.m_glassTypes.GetCount();
	m_TotalGlassSizeTypes = 0;
	//Calculate total glass Types alongwith Sizes
	for (int iGlassType = 0; iGlassType < nGlassTypes; iGlassType++)
	{
		m_TotalGlassSizeTypes += m_config.m_glassTypes[iGlassType].m_glassSizes.GetCount();
	}

	SHUTDOWN_PARSER;
}

void bgCracksTemplate::LoadResources()
{
	// intialize the parser for this class
	INIT_PARSER;


	int nGlassTypes = m_config.m_glassTypes.GetCount();
	int nCrackTypes = m_config.m_crackTypes.GetCount();
	int iGlassCrackArrayIndex = 0;

	m_glassCrackArray.Resize(m_TotalGlassSizeTypes * nCrackTypes);
	for (int iGlassType = 0; iGlassType < nGlassTypes; iGlassType++)
	{
		// Setup directory for Glass Type
		rage::ASSET.PushFolder(m_config.m_glassTypes[iGlassType].m_glassType);
		for(int iSizeType = 0; iSizeType < m_config.m_glassTypes[iGlassType].m_glassSizes.GetCount(); iSizeType++)
		{
			if(!m_config.m_glassTypes[iGlassType].m_glassSizes[iSizeType].Init())
			{
				bgAssertf(false ,
					"glass Size '%s' for glassType '%s' is of wrong format. It should be <digit>x<digit> where digit = [0,9]. Skipping load of crack Types for this size", 
					m_config.m_glassTypes[iGlassType].m_glassSizes[iSizeType].m_glassSize.c_str(), m_config.m_glassTypes[iGlassType].m_glassType.c_str());
				continue;
			}
			// Setup directory for Size Type
			rage::ASSET.PushFolder(m_config.m_glassTypes[iGlassType].m_glassSizes[iSizeType].m_glassSize);

			// Loop over the weapon classes and try to load the file
			for (int iCrackType = 0; iCrackType < nCrackTypes; iCrackType++)
			{
				bgCrackType& glassCrack = m_glassCrackArray[iGlassCrackArrayIndex];
				glassCrack.Init();
				if (bgVerifyf(glassCrack.Load(m_config.m_crackTypes[iCrackType]),
					"CrackType '%s/%s' failed to load\n", 
					m_config.m_glassTypes[iGlassType].GetTypeName().c_str(), m_config.m_crackTypes[iCrackType]))
				{
					bgAssertf(0 < glassCrack.m_crackArray.GetCount(),
						"no star maps specified for crack type '%s/%s'", 
						m_config.m_glassTypes[iGlassType].GetTypeName().c_str(), m_config.m_crackTypes[iCrackType]);
				}
				iGlassCrackArrayIndex++;
			}//for iCrackType
			// Undo directory for the next glass Size
			rage::ASSET.PopFolder();
		}//for iSizeType

		// Undo directory for the next glass type
		rage::ASSET.PopFolder();
	}//for iGlassType

	// Now that we've read the xml files, do the post-load, this sets the directories so
	// the root Tune/Glass is the top level when we are looking for crack data files.
	for(int iGlassCrackType = 0; iGlassCrackType < m_glassCrackArray.GetCount(); iGlassCrackType++)
	{
		bgCrackType* pGlassCrack = &m_glassCrackArray[iGlassCrackType];
		LoadCrackData(pGlassCrack);
	}
	SHUTDOWN_PARSER;

#if __BANK
	// cache memory usage stats after loading resources
	CacheMemoryUsageStats();
#endif

}//LoadResources

const atArray<bgGlassTypeConfig>& bgCracksTemplate::GetGlassTypes() const
{
	return m_config.m_glassTypes;
}

const atArray<char*>& bgCracksTemplate::GetCrackTypes() const
{
	return m_config.m_crackTypes;
}

const atArray<bgGlassSize>& bgCracksTemplate::GetGlassSizes(int in_iGlassType) const
{
	if (!bgVerifyf(0 <= in_iGlassType && in_iGlassType < GetGlassTypes().GetCount(), "Glass type %i is out of range [0, %i). Using glasstype 0.\n", in_iGlassType, GetGlassTypes().GetCount()))
	{
		in_iGlassType = 0;
	}
	return m_config.m_glassTypes[in_iGlassType].m_glassSizes;
}

//------------------------------------------------------------------------------
// Clean up our singleton
//------------------------------------------------------------------------------
void bgCracksTemplate::ShutdownClass()
{
	// Loop over glass types
	for (int iCrack = 0; iCrack < m_glassCrackArray.GetCount(); iCrack++)
	{
		m_glassCrackArray[iCrack].Term();
	}//for iGlassType

	// Clear our crack pool
	CrackMeshPool::Iterator iter = m_crackMeshPool.CreateIterator();	
	while (!iter.AtEnd())
	{	
		delete iter.GetData();
		iter.Next();
	}
	m_crackMeshPool.Kill();

	// Clear our texture pool
	TexturePool::Iterator iterTex = m_texturePool.CreateIterator();	
	while (!iterTex.AtEnd())
	{	
		iterTex.GetData()->Release();
		iterTex.Next();
	}
	m_texturePool.DeleteAll();

}//ShutdownClass

bool bgCracksTemplate::IsInitialized(bgCracksTemplate*& out_pCracksTemplate, const char** out_pErrMsg)
{
	out_pCracksTemplate = NULL;
	if (bgSCracksTemplate::IsInstantiated())
	{
		if (bgSCracksTemplate::InstanceRef().IsInitialized(out_pErrMsg))
		{
			out_pCracksTemplate = bgSCracksTemplate::InstancePtr();
			return true;
		}
	}
	else if (out_pErrMsg)
	{
		*out_pErrMsg = "bgSCracksTemplate not instantiated!";
	}
	return false;
}

bool bgCracksTemplate::IsInitialized(const char** out_pErrMsg) const
{
	if ((0 < m_config.m_glassTypes.GetCount()) && (0 < m_config.m_crackTypes.GetCount()))
	{
		return true;
	}
	if (out_pErrMsg)
	{
		*out_pErrMsg = m_InitErrMsg.c_str();
	}
	return false;
}

// Returns true if InitClass() and LoadResources() were both successfully invoked
bool bgCracksTemplate::IsReady(const char** out_pErrMsg) const
{
	if (!IsInitialized(out_pErrMsg))
	{
		return false;
	}

	if (m_glassCrackArray.GetCount() == m_TotalGlassSizeTypes * GetCrackTypes().GetCount())
	{
		return true;
	}
	if (out_pErrMsg)
	{
		*out_pErrMsg = "Crack types failed to load";
	}
	return false;
}

bool bgCracksTemplate::IsReady(bgCracksTemplate*& out_pCracksTemplate, const char** out_pErrMsg)
{
	if (!IsInitialized(out_pCracksTemplate, out_pErrMsg))
	{
		return false;
	}
	return (out_pCracksTemplate->IsReady(out_pErrMsg));
}

//------------------------------------------------------------------------------
// Get a set of crack data for the given crack type / glass type combo.
// in_eCrackType is determined by the weapon/ammo/physical responsible for causing the breakage.
// in_eGlassType is determined by the object being broken.
//------------------------------------------------------------------------------
bgCrackType* bgCracksTemplate::GetCrackData(int in_iGlassType,
										  int in_iGlassSizeIndex,
										  int in_iCrackType)
{
	int nCrackTypes = GetCrackTypes().GetCount();
	int nGlassTypes = GetGlassTypes().GetCount();
	if (!bgVerifyf(0 <= in_iGlassType && in_iGlassType < nGlassTypes, "Glass type %i is out of range [0, %i). Using glasstype 0.\n", in_iGlassType, nGlassTypes))
	{
		in_iGlassType = 0;
	}
	int nGlassSizeCount = GetGlassSizes(in_iGlassType).GetCount();
	if (!bgVerifyf(0 <= in_iGlassSizeIndex && in_iGlassSizeIndex < nGlassSizeCount, "Glass Size Index %i is out of range [0, %i) for glass Type %i. Using glass Size Index 0.\n", in_iGlassSizeIndex, nGlassSizeCount, in_iGlassType))
	{
		in_iGlassSizeIndex = 0;
	}

	if (!bgVerifyf(0 <= in_iCrackType && in_iCrackType < nCrackTypes, "Crack type %i is out of range [0, %i). Using crack type 0.\n", in_iCrackType, nCrackTypes))
	{
		in_iCrackType = 0;
	}

	int glassCrackIndex = 0;
	for(int i=0; i<in_iGlassType; i++)
	{
		glassCrackIndex	+= GetGlassSizes(i).GetCount() * nCrackTypes; 
	}
	glassCrackIndex += nCrackTypes * in_iGlassSizeIndex + in_iCrackType;
	return &(m_glassCrackArray[glassCrackIndex]);
}//GetCrackData

void OverrideTextureVars(atArray<ConstString>& io_textureNames, 
						 const atArray<bgTextureVarData>& in_textureOverrides,
						 const atArray<bgTextureVarData>& in_textureDefaults)
{
	int overrideCount = in_textureOverrides.GetCount();
	int varCount = io_textureNames.GetCount();
	bgAssertf(varCount == in_textureDefaults.GetCount(), "texture defaults array length (%i) does not match names array (%i)\n", in_textureDefaults.GetCount(), varCount);
	for (int iOverride = 0; iOverride < overrideCount; iOverride++)
	{
		const bgTextureVarData& overrideData = in_textureOverrides[iOverride];
		int iVar;
		for (iVar = 0; iVar < varCount; iVar++)
		{
			if (in_textureDefaults[iVar].m_varName == overrideData.m_varName)
			{
				io_textureNames[iVar] = overrideData.m_textureName;
				break;
			}
		}
		bgAssertf(iVar != varCount, "texture var %s not found!\n", overrideData.m_varName.c_str());
	}
}

void OverrideFloat4Vars(atArray<Vector4>& io_float4Values, 
						 const atArray<bgFloat4VarData>& in_float4Overrides,
						 const atArray<bgFloat4VarData>& in_float4Defaults)
{
	int overrideCount = in_float4Overrides.GetCount();
	int varCount = io_float4Values.GetCount();
	bgAssertf(varCount == in_float4Defaults.GetCount(), "float4 defaults array length (%i) does not match names array (%i)\n", in_float4Defaults.GetCount(), varCount);;
	for (int iOverride = 0; iOverride < overrideCount; iOverride++)
	{
		const bgFloat4VarData& overrideData = in_float4Overrides[iOverride];
		int iVar;
		for (iVar = 0; iVar < varCount; iVar++)
		{
			if (in_float4Defaults[iVar].m_varName == overrideData.m_varName)
			{
				io_float4Values[iVar] = overrideData.m_value;
				break;
			}
		}
		bgAssertf(iVar != varCount, "float4 var %s not found!\n", overrideData.m_varName.c_str());
	}
}

void OverrideFloat2Vars(atArray<Vector2>& io_float2Values, 
						const atArray<bgFloat2VarData>& in_float2Overrides,
						const atArray<bgFloat2VarData>& in_float2Defaults)
{
	int overrideCount = in_float2Overrides.GetCount();
	int varCount = io_float2Values.GetCount();
	bgAssertf(varCount == in_float2Defaults.GetCount(), "float2 defaults array length (%i) does not match names array (%i)\n", in_float2Defaults.GetCount(), varCount);;
	for (int iOverride = 0; iOverride < overrideCount; iOverride++)
	{
		const bgFloat2VarData& overrideData = in_float2Overrides[iOverride];
		int iVar;
		for (iVar = 0; iVar < varCount; iVar++)
		{
			if (in_float2Defaults[iVar].m_varName == overrideData.m_varName)
			{
				io_float2Values[iVar] = overrideData.m_value;
				break;
			}
		}
		bgAssertf(iVar != varCount, "float2 var %s not found!\n", overrideData.m_varName.c_str());
	}
}

void OverrideFloatVars(atArray<float>& io_floatValues, 
						const atArray<bgFloatVarData>& in_floatOverrides,
						const atArray<bgFloatVarData>& in_floatDefaults)
{
	int overrideCount = in_floatOverrides.GetCount();
	int varCount = io_floatValues.GetCount();
	bgAssertf(varCount == in_floatDefaults.GetCount(), "float defaults array length (%i) does not match names array (%i)\n", in_floatDefaults.GetCount(), varCount);;
	for (int iOverride = 0; iOverride < overrideCount; iOverride++)
	{
		const bgFloatVarData& overrideData = in_floatOverrides[iOverride];
		int iVar;
		for (iVar = 0; iVar < varCount; iVar++)
		{
			if (in_floatDefaults[iVar].m_varName == overrideData.m_varName)
			{
				io_floatValues[iVar] = overrideData.m_value;
				break;
			}
		}
		bgAssertf(iVar != varCount, "float var %s not found!\n", overrideData.m_varName.c_str());
	}
}

//------------------------------------------------------------------------------
// Load up the crack data (maybe we should defer this?)
//------------------------------------------------------------------------------
void bgCracksTemplate::LoadCrackData(bgCrackType* in_pWeaponCrack)
{
	int float4VarCount = m_config.m_shaderVars.m_float4VarDefaults.GetCount();
	int float2VarCount = m_config.m_shaderVars.m_float2VarDefaults.GetCount();
	int floatVarCount = m_config.m_shaderVars.m_floatVarDefaults.GetCount();
	int textureVarCount = m_config.m_shaderVars.m_textureVarDefaults.GetCount();
	atArray<Vector4>     float4VarValues(float4VarCount, float4VarCount);
	atArray<Vector2>     float2VarValues(float2VarCount, float2VarCount);
	atArray<float>		 floatVarValues(floatVarCount, floatVarCount);
	atArray<ConstString> textureVarNames(textureVarCount, textureVarCount);
	// determine default values:
	for (int iTextureVar = 0; iTextureVar < textureVarCount; iTextureVar++)
	{
		textureVarNames[iTextureVar] = m_config.m_shaderVars.m_textureVarDefaults[iTextureVar].m_textureName;
	}
	for (int iFloat4Var = 0; iFloat4Var < float4VarCount; iFloat4Var++)
	{
		float4VarValues[iFloat4Var] = m_config.m_shaderVars.m_float4VarDefaults[iFloat4Var].m_value;
	}
	for (int iFloat2Var = 0; iFloat2Var < float2VarCount; iFloat2Var++)
	{
		float2VarValues[iFloat2Var] = m_config.m_shaderVars.m_float2VarDefaults[iFloat2Var].m_value;
	}
	for (int iFloatVar = 0; iFloatVar < floatVarCount; iFloatVar++)
	{
		floatVarValues[iFloatVar] = m_config.m_shaderVars.m_floatVarDefaults[iFloatVar].m_value;
	}
	// override with values from weapon crack]
	OverrideTextureVars(textureVarNames, in_pWeaponCrack->m_textureVars, m_config.m_shaderVars.m_textureVarDefaults);
	OverrideFloat4Vars(float4VarValues, in_pWeaponCrack->m_float4Vars, m_config.m_shaderVars.m_float4VarDefaults);
	OverrideFloat2Vars(float2VarValues, in_pWeaponCrack->m_float2Vars, m_config.m_shaderVars.m_float2VarDefaults);
	OverrideFloatVars(floatVarValues, in_pWeaponCrack->m_floatVars, m_config.m_shaderVars.m_floatVarDefaults);

	// Loop over the array of crack data files and initialize them.
	// Maybe we should defer this until we actually use one? Or only do it
	// for the types of glass we have in a level?
	for (int iCount = 0; iCount < in_pWeaponCrack->m_crackArray.GetCount(); iCount++)
	{
		// Store a pointer to the specific crack data
		bgCrackStarMap* pCrackMap = &(in_pWeaponCrack->m_crackArray[iCount]);
		pCrackMap->m_pCrackMesh = GetOrLoadCrackMesh(pCrackMap->m_cracks);
		// copy default values
		pCrackMap->m_float4VarValues = float4VarValues;
		pCrackMap->m_float2VarValues = float2VarValues;
		pCrackMap->m_floatVarValues = floatVarValues;
		atArray<ConstString> starMapTextureVarNames(textureVarNames);
		OverrideTextureVars(starMapTextureVarNames, pCrackMap->m_textureVars, m_config.m_shaderVars.m_textureVarDefaults);
		OverrideFloat4Vars(pCrackMap->m_float4VarValues, pCrackMap->m_float4Vars, m_config.m_shaderVars.m_float4VarDefaults);
		OverrideFloat2Vars(pCrackMap->m_float2VarValues, pCrackMap->m_float2Vars, m_config.m_shaderVars.m_float2VarDefaults);
		OverrideFloatVars(pCrackMap->m_floatVarValues, pCrackMap->m_floatVars, m_config.m_shaderVars.m_floatVarDefaults);
		pCrackMap->m_textureVarValues.Resize(textureVarCount);
		// lookup the actual textures
		for (int iTextureVar = 0; iTextureVar < textureVarCount; iTextureVar++)
		{
			pCrackMap->m_textureVarValues[iTextureVar] = GetOrLoadTexture(starMapTextureVarNames[iTextureVar]);
		}
	}//for iCount
}//LoadCrackData

//------------------------------------------------------------------------------
// Find or load a crack set with the given filename
//------------------------------------------------------------------------------
bgCrackMesh* bgCracksTemplate::GetOrLoadCrackMesh(const char* in_filename)
{
	// First check our hash to see if we have this name.
	bgCrackMesh** pMesh = m_crackMeshPool.Access(in_filename);
	if (pMesh != NULL)
	{
		return *pMesh;
	}

	// Not found? Create and add it
	bgCrackMesh* pReturn = rage_new bgCrackMesh;
	LoadCrackMesh(*pReturn, in_filename);
	m_crackMeshPool.Insert(in_filename, pReturn);

	// All set
	return pReturn;
}//GetOrLoadCrackMesh

//------------------------------------------------------------------------------
// Either returns an already loaded texture or loads up the filename and
// returns the new pointer.
//------------------------------------------------------------------------------
rage::grcTexture* bgCracksTemplate::GetOrLoadTexture(const char* in_filename)
{
	// First check our hash to see if we have this name.
	rage::u32 hashName = rage::atStringHash(in_filename);
	rage::grcTexture** pTex = m_texturePool.Access(hashName);
	rage::grcTexture* pReturn = NULL;
	if (pTex != NULL)
	{
		pReturn = *pTex;
	}

	// Not found? Create and add it
	if (pReturn == NULL)
	{
		//Assertf(0, "bgCracksTemplate::GetOrLoadTexture(\"%s\") failed", in_filename);
		pReturn = rage::grcTextureFactory::CreateTexture(in_filename);
		m_texturePool.Insert(hashName, pReturn);
	}

	// All set
	return pReturn;
}//GetOrLoadTexture

//------------------------------------------------------------------------------
// Load up a set of cracks from a data file
//------------------------------------------------------------------------------
void bgCracksTemplate::LoadCrackMesh(bgCrackMesh& in_mesh, const char* in_fileName)
{
	//We will need this string to hold a line of char when getting a line in the text file
	char s[100];

	//Set the name file
	const int size = 256;
	char path[size];
	rage::ASSET.GetStackedPath(path, size);
	atString nameFile(path);
	nameFile += in_fileName;

	//Open the file
	fiSafeStream paneFile(fiStream::Open(nameFile));
	bgAssertf(paneFile,"Error opening Glass Pane file [%s]",nameFile.c_str());
	if( paneFile )
	{
		//Read how many pieces we will have
		fgets(s,100,paneFile);
		int pieceCount;
		sscanf(s, "%d", &pieceCount);

		//Allocate the piece array
		in_mesh.m_pieces.Resize(pieceCount);

		bgCrackMeshPointAdder pointAdder(in_mesh);
		typedef bgEdgeAdder<bgCrackMesh> CrackMeshEdgeAdder;
		CrackMeshEdgeAdder edgeAdder(in_mesh);

		//Iterate the pieces
		for( u16 pieceIndex = 0; pieceIndex < pieceCount; ++pieceIndex )
		{
			//Read how many vertices we will have
			bgOneSolidPiece& piece = in_mesh.m_pieces[pieceIndex];
			fgets(s,100,paneFile);
			int pointCount;
			sscanf(s, "%d", &pointCount);

			// map silhouette points to indices into the shared mesh point list
			atArray<bgIndex> silhouettePointIndices;
			silhouettePointIndices.Resize(pointCount);

			for( int pointIndex = 0; pointIndex != pointCount; ++ pointIndex )
			{
				//Read the location of this point
				fgets(s,100,paneFile);
				float x, y;
				sscanf(s, "%f\t%f", &x, &y);

				//Add or find point within mesh 
				Vector2 point;
				point.x = x / 511;
				point.y = y / 511;
				silhouettePointIndices[pointIndex] = pointAdder.FindOrAddPoint(point);
			}

			int edgeCount = 0;
			for (int p0 = pointCount-1, p1 = 0; p1 < pointCount; p0 = p1, p1++)
			{
				if (silhouettePointIndices[p0] != silhouettePointIndices[p1])
					edgeCount++;
			}
			// generate the silhouette polygon edges
			bgAssertf( edgeCount >=3, "egde count %i is too small; need at least 3 per glass piece", edgeCount );
			piece.m_silhouette.Resize(edgeCount);
			for (int edgeIndex = 0, p0 = pointCount-1, p1 = 0; p1 < pointCount; p0 = p1, p1++)
			{
				if (silhouettePointIndices[p0] != silhouettePointIndices[p1])
				{
					piece.m_silhouette[edgeIndex++] = edgeAdder.AddOrUpdateEdge(
						silhouettePointIndices[p0], 
						silhouettePointIndices[p1], 
						pieceIndex,
						nameFile.c_str());
				}
			}

			//Read how many polygons we will have
			fgets(s,100,paneFile);
			int polygonCount;
			sscanf(s, "%d", &polygonCount);

			piece.m_polygons.Resize( polygonCount );

			for( int polygonIndex = 0; polygonIndex < polygonCount; ++polygonIndex )
			{
				//Read how many indices we will have
				fgets(s,100,paneFile);
				int pointCount;
				sscanf(s, "%d", &pointCount);

				bgPolygon polyTemp;
				polyTemp.Resize( pointCount );

				for( int pointIndex = 0; pointIndex < pointCount; ++pointIndex )
				{
					fgets(s,100,paneFile);
					// read the index a the point on the silhouette
					int silhouettePointIndex;
					sscanf(s, "%d", &silhouettePointIndex);
					// translate from silhouettePointIndex to meshPointIndex
					polyTemp[ pointIndex ] = silhouettePointIndices[silhouettePointIndex];
				}

				// need to remove degenerate points here as well...
				int truePointCount = 0;
				for (int p0 = pointCount-1, p1 = 0; p1 < pointCount; p0=p1,p1++)
				{
					if (polyTemp[p0] != polyTemp[p1])
						truePointCount++;
				}

				bgPolygon& polygon = piece.m_polygons[ polygonIndex ];
				bgAssertf(truePointCount >= 3, "glass piece has too many degenerate points, only %i points remain; need at least 3\n", truePointCount);
				polygon.Resize(truePointCount);
				int p=0; // index into polygon; degenerate points removed
				for (int p0 = pointCount-1, p1 = 0; p1 < pointCount; p0=p1,p1++)
				{
					if (polyTemp[p0] != polyTemp[p1])
						polygon[p++] = polyTemp[p1];
				}
				bgAssertf(p == truePointCount, "Thought there were %i non-degenerate points, but found %i instead.  That's really not good.", truePointCount, p);
			}
		}
	}
}//LoadCrackMesh

//------------------------------------------------------------------------------
// Compute the center point of a polygon
//
// input: polygon:		Points to compute the center from
//
Vector2 bgCracksTemplate::ComputeCenter(atArray<Vector2>& polygon)
{
	Vector2 average(0,0);
	for (int i = 0;i < polygon.GetCount();i++)
		average += polygon[i];
	return average / (float)polygon.GetCount();
}

//------------------------------------------------------------------------------
// Compute the center point of a polygon
//
// input:	polygon:		Points to compute the center from
//			count			Point count in polygon
//
Vector2 bgCracksTemplate::ComputeCenter(Vector2* polygon, int count)
{
	Vector2 average(0,0);
	for (int i = 0;i < count;i++)
		average += polygon[i];
	return average / (float)count;
}



//*******************************************************************************************************************
//Math that I probably do not need but for the moment I keep them because I found a bug in the RAGE plane 
// function (the w is wrong sign I believe and broke my code previously).  I need to look into that at some point
// and replace my function with the RAGE one.
//*******************************************************************************************************************
bool bgCracksTemplate::IsPointInPoly(rage::Vector2 in_pointInside, rage::Vector2* in_polyPoint, int in_pointCount)
{
	int c = 0;
	int npol = in_pointCount;

	int j = npol-1;//npol-1;
	for (int i = 0; i < npol; j = i++) 
	{
		if ((((in_polyPoint[i].y <=in_pointInside.y) && (in_pointInside.y<in_polyPoint[j].y)) || 
			((in_polyPoint[j].y<=in_pointInside.y) && (in_pointInside.y<in_polyPoint[i].y))) &&
			(in_pointInside.x < (in_polyPoint[j].x - in_polyPoint[i].x) * (in_pointInside.y - in_polyPoint[i].y) / 
			(in_polyPoint[j].y - in_polyPoint[i].y) + in_polyPoint[i].x))
		{
			c = !c;
		}
	}

	if (c == 0)
		return false;
	return true;
}

#if __BANK
int bgCracksTemplate::GetGeometryMemoryUsage() const
{
	int totalUsage = 0;
	CrackMeshPool::ConstIterator it = m_crackMeshPool.CreateIterator();
	for (it.Start(); !it.AtEnd(); it.Next())
	{
		bgCrackMesh* pCrackMesh = it.GetData();
		totalUsage+=sizeof(*pCrackMesh);
		const atArray<bgOneSolidPiece>& pieces = pCrackMesh->m_pieces;
		totalUsage+=pieces.GetCapacity()*sizeof(bgOneSolidPiece);
		for (int iPiece=0; iPiece < pieces.GetCount(); iPiece++)
		{
			totalUsage+=pieces[iPiece].GetDynamicStorage();
		}
		const atArray<Vector2>& points = pCrackMesh->m_points;
		totalUsage+=points.GetCapacity()*sizeof(Vector2);
		const atArray<bgEdge>& edges = pCrackMesh->m_edges;
		totalUsage += edges.GetCapacity()*sizeof(bgEdge);
	}
	return totalUsage;
}
#endif

#if __BANK
int bgCracksTemplate::GetTextureMemoryUsage() const
{
	int totalUsage = 0;

	// Clear our texture pool
	TexturePool::Iterator iterTex = m_texturePool.CreateIterator();	
	while (!iterTex.AtEnd())
	{	
		totalUsage += iterTex.GetData()->GetPhysicalSize();
		iterTex.Next();
	}

	return totalUsage;
}
#endif

#if __BANK
void bgCracksTemplate::CacheMemoryUsageStats()
{

	// Loop over glass types
	int nGlassTypes = m_config.m_glassTypes.GetCount();
	int nCrackTypes = m_config.m_crackTypes.GetCount();

	m_memStats.glassTypes.Resize(nGlassTypes);

	for (int iGlassType = 0; iGlassType < nGlassTypes; iGlassType++)
	{
		m_memStats.glassTypes[iGlassType].pName = m_config.m_glassTypes[iGlassType].GetTypeName();
		m_memStats.glassTypes[iGlassType].crackTypes.Resize(nCrackTypes);

		// Loop over crack types
		for (int iCrackType = 0; iCrackType < nCrackTypes; iCrackType++)
		{
			const bgCrackType* pGlassCrack = &m_glassCrackArray[iGlassType * nCrackTypes + iCrackType];
		
			for (int iCount = 0; iCount < pGlassCrack->m_crackArray.GetCount(); iCount++)
			{
				// compute geometry memory usage
				const bgCrackStarMap* pCrackMap = &(pGlassCrack->m_crackArray[iCount]);
				const bgCrackMesh* pCrackMesh = pCrackMap->m_pCrackMesh;

				m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].pName = m_config.m_crackTypes[iCrackType];
				m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].geomMemUsage +=sizeof(*pCrackMesh);

				const atArray<bgOneSolidPiece>& pieces = pCrackMesh->m_pieces;
				m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].geomMemUsage += pieces.GetCapacity()*sizeof(bgOneSolidPiece);
				m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].numPieces = (u16)pieces.GetCount();
				for (int iPiece=0; iPiece < pieces.GetCount(); iPiece++)
				{
					m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].geomMemUsage += pieces[iPiece].GetDynamicStorage();
				}
				const atArray<Vector2>& points = pCrackMesh->m_points;
				m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].geomMemUsage += points.GetCapacity()*sizeof(Vector2);
				m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].numPoints = (u16)points.GetCount();
				const atArray<bgEdge>& edges = pCrackMesh->m_edges;
				m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].geomMemUsage += edges.GetCapacity()*sizeof(bgEdge);
				m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].numEdges = (u16)edges.GetCount();

				// compute texture memory usage
				m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].numTextures = (u16)pCrackMap->m_textureVarValues.GetCount();
				for (int iTextureVar = 0; iTextureVar < pCrackMap->m_textureVarValues.GetCount(); iTextureVar++)
				{
					u32 size = pCrackMap->m_textureVarValues[iTextureVar]->GetPhysicalSize();
					m_memStats.glassTypes[iGlassType].crackTypes[iCrackType].texMemUsage += size;
					
					bgDebugCrackTextureMemStats bgTexture;
					bgTexture.pName = pCrackMap->m_textureVarValues[iTextureVar]->GetName();
					bgTexture.size = size;
					if (m_memStats.textures.Find(bgTexture) == -1)
					{
						m_memStats.textures.PushAndGrow(bgTexture);
					}
				}
			}

		}//for iCrackType

	}//for iGlassType

}
#endif //__BANK

#if __BANK
void bgCracksTemplate::PrintMemoryUsageStats(bool bShowGlassInfo, bool bShowTextureInfo) const
{

	// output totals
	grcDebugDraw::AddDebugOutputSeparator(8);
	grcDebugDraw::AddDebugOutputEx(false, "CRACKS TEMPLATE GLOBAL MEM STATS");

	atString line;
	line += atVarString(" %20s",	"Total Geometry Mem:"	);
	line += atVarString(" %20s",	atVarString("%d kb", bgCracksTemplate::GetGeometryMemoryUsage()/1024	).c_str());
	grcDebugDraw::AddDebugOutputEx(false, line.c_str()); line = "";

	line += atVarString(" %20s",	"Total Texture Mem:"		);
	line += atVarString(" %20s",	atVarString("%d kb", bgCracksTemplate::GetTextureMemoryUsage()/1024		).c_str());
	grcDebugDraw::AddDebugOutputEx(false, line.c_str()); line = "";
	grcDebugDraw::AddDebugOutputSeparator(3);

	if (bShowGlassInfo)
	{
		// output glass and crack types info
		grcDebugDraw::AddDebugOutputSeparator(8);
		grcDebugDraw::AddDebugOutputEx(false, "GLASS AND CRACK TYPES");

		line += atVarString(" %12s",	"Glass Type"		);
		line += atVarString(" %12s",	"Num Cracks"		);
		line += atVarString(" %12s",	"Crack Type"		);
		line += atVarString(" %12s",	"Total Geom"		);
		line += atVarString(" %12s",	"Total Tex"			);
		line += atVarString(" %8s",		"Pieces"			);
		line += atVarString(" %8s",		"Points"			);
		line += atVarString(" %8s",		"Edges"				);
		line += atVarString(" %8s",		"Tex"				);

		grcDebugDraw::AddDebugOutputEx(false, line.c_str()); line = "";
		grcDebugDraw::AddDebugOutputSeparator(3);

		for (int i = 0; i < m_memStats.glassTypes.GetCount(); i++)
		{
			for (int j = 0; j < m_memStats.glassTypes[i].crackTypes.GetCount(); j++)
			{
				if (j == 0)
				{
					line += atVarString(" %12s",	atVarString("%s"	, m_memStats.glassTypes[i].pName						).c_str());
					line += atVarString(" %12s",	atVarString("%d"	, m_memStats.glassTypes[i].crackTypes.GetCount()		).c_str());
				}
				else
				{
					line += atVarString(" %12s",	atVarString("").c_str());
					line += atVarString(" %12s",	atVarString("").c_str());
				}
				line += atVarString(" %12s",	atVarString("%s"	, m_memStats.glassTypes[i].crackTypes[j].pName				).c_str());
				line += atVarString(" %12s",	atVarString("%d kb"	, m_memStats.glassTypes[i].crackTypes[j].geomMemUsage/1024	).c_str());
				line += atVarString(" %12s",	atVarString("%d kb"	, m_memStats.glassTypes[i].crackTypes[j].texMemUsage/1024	).c_str());
				line += atVarString(" %8s",		atVarString("%d"	, m_memStats.glassTypes[i].crackTypes[j].numPieces			).c_str());
				line += atVarString(" %8s",		atVarString("%d"	, m_memStats.glassTypes[i].crackTypes[j].numPoints			).c_str());
				line += atVarString(" %8s",		atVarString("%d"	, m_memStats.glassTypes[i].crackTypes[j].numEdges			).c_str());
				line += atVarString(" %8s",		atVarString("%d"	, m_memStats.glassTypes[i].crackTypes[j].numTextures		).c_str());

				grcDebugDraw::AddDebugOutputEx(false, line.c_str()); line = "";
			}
			grcDebugDraw::AddDebugOutputSeparator(3);
		}
	}

	if (bShowTextureInfo)
	{
		// output texture info
		grcDebugDraw::AddDebugOutputSeparator(8);
		grcDebugDraw::AddDebugOutputEx(false, "CRACK TEXTURES");
		line += atVarString(" %32s",	"Texture Name"		);
		line += atVarString(" %8s",		"Size"				);
		grcDebugDraw::AddDebugOutputEx(false, line.c_str()); line = "";
		grcDebugDraw::AddDebugOutputSeparator(3);

		for (int i = 0; i < m_memStats.textures.GetCount(); i++)
		{
			line += atVarString(" %32s",	atVarString("%s"	, m_memStats.textures[i].pName				).c_str());
			line += atVarString(" %8s",		atVarString("%d kb"	, m_memStats.textures[i].size/1024			).c_str());
			grcDebugDraw::AddDebugOutputEx(false, line.c_str()); line = "";
		}
		grcDebugDraw::AddDebugOutputSeparator(8);
	}
}
#endif // __BANK

} // namespace rage
