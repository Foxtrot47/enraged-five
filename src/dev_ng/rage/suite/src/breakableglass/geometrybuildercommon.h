//
// breakableglass/geometrybuildercommon.h 
// 
// Copyright (C) 2009 Rockstar Games.  All Rights Reserved. 
//

#ifndef BREAKABLEGLASS_GEOMETRYBUILDERCOMMON_H
#define BREAKABLEGLASS_GEOMETRYBUILDERCOMMON_H

#include "vector/vector2.h"
#include "vector/vector3.h"

#define DRAW_CRACKS
#define DRAW_PANES
#define BREAKABLE_GLASS_ON_SPU 0

#endif // BREAKABLEGLASS_GEOMETRYBUILDERCOMMON_H
