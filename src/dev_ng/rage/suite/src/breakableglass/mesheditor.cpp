// 
// breakableglass/mesheditor.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "mesheditor.h"
#include "optimisations.h"

#include "grcore/indexbuffer.h"
#include "mesh/mesh.h"

#if MESH_LIBRARY

// optimisations
BG_OPTIMISATIONS()

namespace rage {

int bgMeshEditor::GetMajorBone(int in_vertexIndex)
{
	Color32 indices = GetBlendIndices(in_vertexIndex);
	Vector4 weights = GetBlendWeights(in_vertexIndex);
	float largestWeight = weights.x;
	int majorBone = indices.GetRed();
	if (largestWeight < weights.y)
	{
		largestWeight = weights.y;
		majorBone = indices.GetGreen();
	}
	if (largestWeight < weights.z)
	{
		largestWeight = weights.z;
		majorBone = indices.GetBlue();
	}
	if (largestWeight < weights.w)
	{
		majorBone = indices.GetAlpha();
	}
	return majorBone;
}

void bgMeshEditor::FindUVRange(int in_uvSet, int in_boneIndex, Vector2& out_uvMin, Vector2& out_uvMax)
{
	Vector2 uvMin( FLT_MAX, FLT_MAX);
	Vector2 uvMax(-FLT_MAX,-FLT_MAX);
	const bool kHasBindingsChannel = (-1 != in_boneIndex) && HasBindingsChannel();
	int nVertices = GetVertexCount();
	for (int i=0; i<nVertices; i++)
	{
		if (kHasBindingsChannel)
		{
			const int boneIndex0 = GetBlendIndices(i).GetRed();
			if (boneIndex0 != in_boneIndex)
			{
				continue;
			}
		}

		Vector2 uv = GetUV(i, in_uvSet);
		if (uvMin.x > uv.x)
			uvMin.x = uv.x;
		if (uvMin.y > uv.y)
			uvMin.y = uv.y;
		if (uvMax.x < uv.x)
			uvMax.x = uv.x;
		if (uvMax.y < uv.y)
			uvMax.y = uv.y;
	}
	out_uvMin = uvMin;
	out_uvMax = uvMax;
}

int bgMeshEditor::FindLargestTriangle(int in_uvSet, int in_boneIndex)
{
	const bool kHasBindingsChannel = (-1 != in_boneIndex) && HasBindingsChannel();
	const u16* indices = GetIndices();
	int nIndices = GetIndexCount();
	int iLargestTriangle = -1;
	float fLargestArea2 = -1.f;
	for (int i=0; i<nIndices; i+=3)
	{
		Vector2 uv0, uv1, uv2;
		if (kHasBindingsChannel)
		{
			const int boneIndex0 = GetMajorBone(indices[i]);
			const int boneIndex1 = GetMajorBone(indices[i+1]);
			const int boneIndex2 = GetMajorBone(indices[i+2]);
			if (boneIndex0 != in_boneIndex &&
				boneIndex1 != in_boneIndex &&
				boneIndex2 != in_boneIndex)
			{
				continue;
			}
		}

		uv0 = GetUV(indices[i], in_uvSet);
		uv1 = GetUV(indices[i+1], in_uvSet);
		uv2 = GetUV(indices[i+2], in_uvSet);
		Vector2 uv10 = uv1 - uv0;
		Vector2 uv20 = uv2 - uv0;
		float fArea2 = uv20.y * uv10.x - uv10.y * uv20.x;
		fArea2 *= fArea2;
		if (iLargestTriangle < 0 || fArea2 > fLargestArea2)
		{
			fLargestArea2 = fArea2;
			iLargestTriangle = i;
		}
	}
	return iLargestTriangle;
}




bgVertexBufferMeshEditor::bgVertexBufferMeshEditor(grcVertexBuffer* in_pVertexBuffer, grcIndexBuffer* in_pIndexBuffer) :
	m_pIndexBuffer(in_pIndexBuffer),
	m_VertexBufferEditor(in_pVertexBuffer, true, true)
{
	m_pIndices = m_pIndexBuffer->LockRO();
}

bgVertexBufferMeshEditor::~bgVertexBufferMeshEditor()
{
	Reset();
}

void bgVertexBufferMeshEditor::Reset()
{
	m_pIndices = NULL;
	m_pIndexBuffer->UnlockRO();
	m_pIndexBuffer = NULL;
}

const u16* bgVertexBufferMeshEditor::GetIndices() const
{
	return m_pIndices;
}

int bgVertexBufferMeshEditor::GetIndexCount() const
{
	return m_pIndexBuffer->GetIndexCount();
}

int bgVertexBufferMeshEditor::GetVertexCount() const
{
	return m_VertexBufferEditor.GetVertexBuffer()->GetVertexCount();
}

bool bgVertexBufferMeshEditor::HasBindingsChannel() const
{
	return m_VertexBufferEditor.GetVertexBuffer()->GetFvf()->GetBindingsChannel();
}

Color32 bgVertexBufferMeshEditor::GetBlendIndices(int in_vertexIndex)
{
	return m_VertexBufferEditor.GetBlendIndices(in_vertexIndex);
}

Vector4 bgVertexBufferMeshEditor::GetBlendWeights(int in_vertexIndex)
{
	return m_VertexBufferEditor.GetBlendWeights(in_vertexIndex);
}

Vector2 bgVertexBufferMeshEditor::GetUV(int in_vertexIndex, int in_uvSet)
{
	Vector2 uv = m_VertexBufferEditor.GetUV(in_vertexIndex, in_uvSet);
	uv.y = 1-uv.y; // grcVertexBufferEditor flips this; we need to flip it back :P
	return uv;
}

Vector3 bgVertexBufferMeshEditor::GetPosition(int in_vertexIndex)
{
	return m_VertexBufferEditor.GetPosition(in_vertexIndex);
}

Vector3 bgVertexBufferMeshEditor::GetTangent(int in_vertexIndex)
{
	return m_VertexBufferEditor.GetTangent(in_vertexIndex, 0).GetVector3();
}

bgMshMaterialMeshEditor::bgMshMaterialMeshEditor(mshMaterial* in_pMshMaterial) : m_pMshMaterial(in_pMshMaterial)
{
	m_Indices.Reserve(m_pMshMaterial->GetTriangleCount()*3);
	for (mshMaterial::TriangleIterator i=m_pMshMaterial->BeginTriangles(); i!= m_pMshMaterial->EndTriangles(); ++i)
	{
		int v0, v1, v2;
		i.GetVertIndices(v0, v1, v2);
		m_Indices.Append() = u16(v0);
		m_Indices.Append() = u16(v1);
		m_Indices.Append() = u16(v2);
	}
}

bgMshMaterialMeshEditor::~bgMshMaterialMeshEditor()
{
	Reset();
}


void bgMshMaterialMeshEditor::Reset()
{
	m_Indices.Reset();
	m_pMshMaterial = NULL;
}

const u16* bgMshMaterialMeshEditor::GetIndices() const
{
	if (m_Indices.GetCount() > 0)
		return &m_Indices[0];

	return NULL;
}

int bgMshMaterialMeshEditor::GetIndexCount() const
{
	return m_Indices.GetCount();
}

int bgMshMaterialMeshEditor::GetVertexCount() const
{
	return m_pMshMaterial->GetVertexCount();
}

bool bgMshMaterialMeshEditor::HasBindingsChannel() const
{
	return true;
}

Color32 bgMshMaterialMeshEditor::GetBlendIndices(int in_vertexIndex)
{
	const mshBinding& binding = m_pMshMaterial->GetBinding(in_vertexIndex);
	Color32 result;
	result.Set(binding.Mtx[0], binding.Mtx[1], binding.Mtx[2], binding.Mtx[3]);
	return result;
}

Vector4 bgMshMaterialMeshEditor::GetBlendWeights(int in_vertexIndex)
{
	const mshBinding& binding = m_pMshMaterial->GetBinding(in_vertexIndex);
	Vector4 result;
	result.x = binding.Wgt[0];
	result.y = binding.Wgt[1];
	result.z = binding.Wgt[2];
	result.w = binding.Wgt[3];
	return result;
}

Vector2 bgMshMaterialMeshEditor::GetUV(int in_vertexIndex, int in_uvSet)
{
	// need to flip v to match the values that will eventually be handed to the GPU
	const Vector2& rawUV = m_pMshMaterial->GetVertex(in_vertexIndex).Tex[in_uvSet];
	return Vector2(rawUV.x, 1.f - rawUV.y);
}

Vector3 bgMshMaterialMeshEditor::GetPosition(int in_vertexIndex)
{
	const mshVertex& vert = m_pMshMaterial->GetVertex(in_vertexIndex);
	return vert.Pos;
}

Vector3 bgMshMaterialMeshEditor::GetTangent(int in_vertexIndex)
{
	const mshVertex& vert = m_pMshMaterial->GetVertex(in_vertexIndex);
	return vert.TanBi[0].T;
}
} // namespace rage

#endif
