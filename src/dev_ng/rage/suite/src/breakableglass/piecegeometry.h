// 
// breakableglass/piecegeometry.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_PIECEGEOMETRY_H 
#define BREAKABLEGLASS_PIECEGEOMETRY_H 

#include "edge.h"
#include "vector/vector2.h"
#include "vector/vector3.h"

namespace rage {

	// Represent a piece of glass still attached to the pane.
	// Must maintain neighbor information, but doesn't need and dynamics data.
	class bgPanePiece
	{
	public:
		enum ePieceFlags
		{
			kPF_Active  =  0x01,      // Piece is still visible and on the pane
			kPF_Floating = 0x02,      // if set this piece is 'floating' and should be moved from the 'pane' to the 'falling' list
			kPF_Visited =  0x04,      // only used during the 'floating' computation: if set, 'floating' state has been determined for this piece
			kPF_Visiting = 0x08,      // only used during the 'floating' computation: if set, 'floating' state is being determined for this piece
			// and we should not revisit it.
		};

		//
		// Constructor
		//
		bgPanePiece() : m_hit(-1), m_flags(0), m_area(0.0f)
		{
		}

#if __SPU
		//
		// 'Resource' constructor
		// used on SPU only to fix up pointers
		//
		bgPanePiece(datResource& rsc) 
			: m_polygons(rsc, true)
			, m_silhouette(rsc)
		{
		}

		//
		// Place: used on SPU only to fix up pointers
		//
		static void Place(void *that, datResource& rsc)
		{
			::new (that) bgPanePiece(rsc);
		}
#endif

#if !__SPU
		//
		// Free all memory.
		// @NOTE: ensure that all refs to this piece have been cleared before calling this method!
		//
		void Reset()
		{
			m_silhouette.Reset();
			m_polygons.Reset();
			m_hit = -1;
			m_flags = 0;
			m_area = -1.0f;
		}
#endif // !__SPU

		bgArray<bgEdgeRef>  m_silhouette; // (possibly concave) silhouette of the entire piece
		bgArray<bgPolygon>  m_polygons;   // convex decomposition of silhouette
		int					m_hit;
		u32                 m_flags;
		float				m_area;
	};

	class bgFallingPiece
	{
	public:
		//
		// 'Resource' constructor
		// used on SPU only to fix up pointers
		//
		bgFallingPiece() : m_hit(-1) // pieces need to start out inactive
		{
		}

#if __SPU
		//
		// Place: used on SPU only to fix up pointers after dma
		//
		bgFallingPiece(datResource& rsc) : m_points(rsc)
		{
		}

		static void Place(void *that, datResource& rsc)
		{
			::new (that) bgFallingPiece(rsc);
		}
#endif

		//---------------------------------
		//Piece flying out data
		//

		// Falling pieces must be a single convex polygon; 
		// all points are always 'visible'; no point sharing is needed.
		bgArray<Vector2>                    m_points;

		//When a piece is flying out its vertices's must be centered on itself, not the pane, so we can rotate it.  This translation
		// is found at impact time ands should be applied on the vertex before submission to the shader because the matrix passed to the 
		// shader will add this value to the vertex
		Vector3								m_massCenteredTranslation;

		// The center of the 2D AABB for the piece
		Vector2								m_center;

		// Half the diagonal of the AABB for the piece
		Vector2								m_extents;

		// Hit that established decal coordinates
		int									m_hit;

		// Piece area
		float								m_area;

		//Life
		// This is initialized at creation time with the same value as m_originalLife.  However this value will be decremented at every tick.
		float								m_life;	
		
		//This was the life set at creation time.  Useful to know how old is the shard.
		float								m_originalLife;
	};

	namespace bgLod
	{
		enum eLod
		{
			LOD_VLOW,
			LOD_LOW,
			LOD_MED,
			LOD_HIGH,
			LOD_COUNT
		};
	}

	namespace bgMatrixArray
	{
		// in the vertex shader matrix array, the '0' element is the identity transform, used
		// for all pieces still attached to the frame.  
		static const int FIRST_FALLING_PIECE = 1;
	}
} // namespace rage

#endif // BREAKABLEGLASS_PIECEGEOMETRY_H 
