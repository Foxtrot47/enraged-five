#ifndef BREAKABLEGLASS_SWAPVECTOR_H
#define BREAKABLEGLASS_SWAPVECTOR_H

#include <algorithm>
#include <vector>
#include "system\stl_wrapper.h"

namespace rage
{
	template< class T >
	void bgSwap( T &lhs, T &rhs )
	{
		using std::swap;
		swap( lhs, rhs );
	}

	//bgSwapClearer must set the input object to a state externally
	//indistinguishable from the object's initial state.
	template< class T >
	struct bgSwapClearer
	{
		void Clear( T &t ) const
		{
			t = T();
		}
	};

	template< class T, class A >
	struct bgSwapClearer< std::vector< T, A > >
	{
		void Clear( std::vector< T, A > &ts ) const
		{
			ts.clear();
		}
	};

	//bgSwapVector< std::vector< T > > will not free the memory allocated by the vectors it contains.
	//Instead, it clears them; destroying the objects they contain, but retaining the memory.
	//It also swaps objects when resizing beyond its capacity, instead of copying and deleting.
	template< class T, class C = bgSwapClearer< T >, class A = stlAllocator< T > >
	class bgSwapVector : C
	{
		typedef std::vector< T, A > Values;

	public:
		typedef typename Values::allocator_type allocator_type;
		typedef typename Values::const_iterator const_iterator;
		typedef typename Values::const_pointer const_pointer;
		typedef typename Values::const_reference const_reference;
		typedef typename Values::const_reverse_iterator const_reverse_iterator;
		typedef typename Values::difference_type difference_type;
		typedef typename Values::iterator iterator;
		typedef typename Values::pointer pointer;
		typedef typename Values::reference reference;
		typedef typename Values::reverse_iterator reverse_iterator;
		typedef typename Values::size_type size_type;
		typedef typename Values::value_type value_type;

		typedef C clearer_type;

		explicit bgSwapVector( clearer_type const &clearer = clearer_type(), allocator_type const &allocator = allocator_type() ) : clearer_type( clearer ), m_values( allocator ), m_size()
		{
		}

		explicit bgSwapVector( size_type size, const_reference value = value_type(), clearer_type const &clearer = clearer_type(), allocator_type const &allocator = allocator_type() ) : clearer_type( clearer ), m_values( size, value, allocator ), m_size( size )
		{
		}

		template< class InputIterator >
		bgSwapVector( InputIterator begin, InputIterator end, clearer_type const &clearer = clearer_type(), allocator_type const &allocator = allocator_type() ) : clearer_type( clearer ), m_values( begin, end, allocator )
		{
			using std::distance;
			m_size = distance( begin, end );
		}

		bgSwapVector( bgSwapVector const &rhs ) : clearer_type( rhs.get_clearer() ), m_values( rhs.begin(), rhs.end(), rhs.get_allocator() ), m_size( rhs.size() )
		{
		}

		bgSwapVector &operator =( bgSwapVector const &rhs )
		{
			if( this != &rhs )
				assign( rhs.begin(), rhs.end() );
			return *this;
		}

		void assign( size_type size, const_reference value )
		{
			using std::fill;
			if( m_size < size )
			{
				using std::min;
				m_size = min( size, m_values.size() );
				if( m_values.empty() )
					push_back( value );
				else
					*begin() = value;
				fill( begin() + 1, end(), *begin() );
				while( m_size != size )
					push_back( *begin() );
			}
			else
			{
				if( ! size )
					clear();
				else
				{
					iterator first( begin() ), last( first + size );
					fill( first + 1, last, *first = value );
					ClearRange( rbegin(), reverse_iterator( last ) );
					m_size = size;
				}
			}
		}

		template< class InputIterator >
		void assign( InputIterator first, InputIterator last )
		{
			AssignCategory( first, last, typename std::iterator_traits< InputIterator >::iterator_category() );
		}

		//reference at( size_type index );

		//const_reference at( size_type index ) const;

		reference back()
		{
			return *( end() - 1 );
		}

		const_reference back() const
		{
			return *( end() - 1 );
		}

		iterator begin()
		{
			return m_values.begin();
		}

		const_iterator begin() const
		{
			return m_values.begin();
		}

		size_type capacity() const
		{
			return m_values.capacity();
		}

		void clear()
		{
			if( ! empty() )
			{
				ClearRange( rbegin(), rend() );
				m_size = 0;
			}
		}

		bool empty() const
		{
			return ! size();
		}

		iterator end()
		{
			return m_values.begin() + size();
		}

		const_iterator end() const
		{
			return m_values.begin() + size();
		}

		void erase( iterator position )
		{
			erase( position, position + 1 );
		}

		void erase( iterator first, iterator last )
		{
			using std::distance;
			difference_type difference( distance( first, last ) );
			if( ! difference )
				return;
			iterator finish( end() );
			if( last == finish )
				ClearRange( reverse_iterator( last ), reverse_iterator( first ) );
			else
			{
				difference_type remainder( distance( last, finish ) );
				for( ; difference < remainder; remainder = distance( last, finish ) )
				{
					iterator next( last );
					while( first != next )
						bgSwap( *first ++, *last ++ );
				}
				reverse_iterator rfirst( last );
				if( remainder < difference )
				{
					rfirst += difference - remainder;
					ClearRange( reverse_iterator( last ), rfirst );
				}
				for( reverse_iterator rlast( first ), rnext( rbegin() ); rfirst != rlast; ++ rfirst, ++ rnext )
				{
					bgSwap( *rfirst, *rnext );
					this->Clear( *rnext );
				}
			}
			m_size -= difference;
		}

		reference front()
		{
			return *begin();
		}

		const_reference front() const
		{
			return *begin();
		}

		allocator_type get_allocator() const
		{
			return m_values.get_allocator();
		}

		clearer_type get_clearer() const
		{
			return *this;
		}

		iterator insert( iterator position, const_reference value )
		{
			using std::distance;
			size_type index( size_type( distance( begin(), position ) ) );
			push_back( value );
			for( reverse_iterator rfirst( rbegin() ), rnext( rfirst + 1 ), rlast( position + 1 ); rfirst != rlast; ++ rfirst, ++ rnext )
				bgSwap( *rfirst, *rnext );
			return begin() + index;
		}

		void insert( iterator position, size_type size, const_reference value )
		{
			using std::distance;
			size_type index( size_type( distance( begin(), position ) ) );
			resize( m_size + size, value );
			while( size )
			{
				for( reverse_iterator rfirst( rbegin() + -- size ), rnext( rfirst + 1 ), rlast( begin() + ++ index ); rfirst != rlast; ++ rfirst, ++ rnext )
					bgSwap( *rfirst, *rnext );
			}
		}

		template< class InputIterator >
		void insert( iterator position, InputIterator first, InputIterator last )
		{
			using std::distance;
			InsertCategory( distance( begin(), position ), first, last, typename std::iterator_traits< InputIterator >::iterator_category() );
		}

		size_type max_size() const
		{
			return m_values.max_size();
		}

		void pop_back()
		{
			this->Clear( m_values[ -- m_size ] );
		}

		void push_back( const_reference value )
		{
			if( m_values.size() == size() )
				PushBackSwap( value );
			else
				m_values[ m_size ++ ] = value;
		}

		reverse_iterator rbegin()
		{
			return reverse_iterator( end() );
		}

		const_reverse_iterator rbegin() const
		{
			return const_reverse_iterator( end() );
		}

		reverse_iterator rend()
		{
			return reverse_iterator( begin() );
		}

		const_reverse_iterator rend() const
		{
			return const_reverse_iterator( begin() );
		}

		void reserve( size_type capacity )
		{
			if( m_values.capacity() < capacity )
				ReserveSwap( capacity );
		}

		void resize( size_type size, const_reference value = value_type() )
		{
			if( m_size < size )
				ResizeSwap( size, value );
			else
			{
				ClearRange( rbegin(), rend() - size );
				m_size = size;
			}
		}

		size_type size() const
		{
			return m_size;
		}

		void swap( bgSwapVector &rhs )
		{
			m_values.swap( rhs.m_values );
			bgSwap( m_size, rhs.m_size );
		}

		reference operator []( size_type index )
		{
			return m_values[ index ];
		}

		const_reference operator []( size_type index ) const
		{
			return m_values[ index ];
		}

	private:
		template< class InputIterator, class IteratorCategory >
		void AssignCategory( InputIterator first, InputIterator last, IteratorCategory const & )
		{
			clear();
			using std::copy;
			using std::back_inserter;
			copy( first, last, back_inserter( *this ) );
		}

		template< class RandomAccessIterator >
		void AssignCategory( RandomAccessIterator first, RandomAccessIterator last, std::random_access_iterator_tag const & )
		{
			using std::copy;
			using std::distance;
			using std::back_inserter;
			size_type size( size_type( distance( first, last ) ) );
			if( m_size < size )
			{
				reserve( size );
				RandomAccessIterator next( first + m_size );
				copy( first, next, begin() );
				copy( next, last, back_inserter( *this ) );
			}
			else
			{
				resize( size );
				copy( first, last, begin() );
			}
		}

		template< class InputIterator >
		void ClearRange( InputIterator begin, InputIterator end )
		{
			for( ; begin != end; ++ begin )
				this->Clear( *begin );
		}

		template< class InputIterator, class IteratorCategory >
		void InsertCategory( size_type index, InputIterator first, InputIterator last, IteratorCategory const & )
		{
			Insert( index, first, last );
		}

		template< class RandomAccessIterator >
		void InsertCategory( size_type index, RandomAccessIterator first, RandomAccessIterator last, std::random_access_iterator_tag const & )
		{
			using std::distance;
			resize( size() + distance( first, last ) );
			Insert( index, first, last );
		}

		template< class InputIterator >
		void Insert( size_type index, InputIterator first, InputIterator last )
		{
			while( first != last )
			{
				push_back( *first ++ );
				for( reverse_iterator rfirst( rbegin() ), rnext( rfirst + 1 ), rlast( begin() + ++ index ); rfirst != rlast; ++ rfirst, ++ rnext )
					bgSwap( *rfirst, *rnext );
			}
		}

		void PushBackSwap( const_reference value )
		{
			if( m_values.size() != m_values.capacity() )
			{
				m_values.push_back( value );
				++ m_size;
			}
			else
			{
				value_type v( value );
				ReserveSwap( NextCapacity( 1 ) );
				m_values.push_back( value_type() );
				++ m_size;
				bgSwap( m_values.back(), v );
			}
		}

		size_type NextCapacity( size_type capacity ) const
		{
			using std::max;
			return m_values.capacity() + max( m_values.capacity(), capacity );
		}

		void ReserveSwap( size_type capacity )
		{
			Values values;
			values.reserve( capacity );
			values.resize( m_values.size() );
			for( size_type index( 0 ), size( m_values.size() ); index != size; ++ index )
				bgSwap( values[ index ], m_values[ index ] );
			values.swap( m_values );
		}

		void ResizeSwap( size_type size, const_reference value )
		{
			using std::fill;
			if( m_values.size() < size )
			{
				value_type v( value );
				if( m_values.capacity() < size )
					ReserveSwap( NextCapacity( size ) );
				fill( end(), m_values.end(), v );
				m_values.resize( size - 1, v );
				m_size = m_values.size();
				m_values.push_back( value_type() );
				++ m_size;
				bgSwap( m_values.back(), v );
			}
			else
			{
				iterator begin( end() );
				m_size = size;
				fill( begin + 1, end(), *begin = value );
			}
		}

		Values m_values;
		size_type m_size;
	};

	template< class T, class C, class A >
	inline void bgSwap( bgSwapVector< T, C, A > &lhs, bgSwapVector< T, C, A > &rhs )
	{
		lhs.swap( rhs );
	}

	template< class T, class C, class A >
	struct bgSwapClearer< bgSwapVector< T, C, A > >
	{
		void Clear( bgSwapVector< T, C, A > &ts ) const
		{
			ts.clear();
		}
	};
}

#endif
