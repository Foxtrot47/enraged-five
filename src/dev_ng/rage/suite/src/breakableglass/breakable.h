//
// breakableglass/breakable.h
//
// Copyright (C) 2008 Rockstar Games.  All Rights Reserved. 
//

// Special types of plane glass (windows, tempered, bullet proof) that are breakable in different ways. 
//  When the glass pane is hit, the pane will break into smaller pieces, where some will fall and some 
//  other will stick on the pane.  Intended for use primarily by the main/simulation thread.
//
//	How to use the code:
//      0:      Call Init() specifying the glass type (see bgCracksTemplate.h/cpp) and vertices and indices 
//              of the glass pane geometry.
//      
//		1:		When the user knows that a collision has occured with the pane (usually via collision 
//              detection with the model whose geometry was pssed into Init())... 
//				a) Convert the world-space impact location to model object space.
//              b) Call Convert3dPointTo2dPoint() to convert the object-space world location to 2d 'pane' space
//		        c) Hit() to actually cause damage to the pane.
//
//      2:      Every frame, during the 'DMZ' when the rendering and main threads are not running simultaneously, 
//              call UpdateGlassDrawable(), which will regenerate rendering geometry when necessary.
//
// -------------------------------------------------------------------------------------------------

#ifndef BREAKABLEGLASS_BREAKABLE_H
#define BREAKABLEGLASS_BREAKABLE_H

#include "geometrydata.h"
#include "crackstemplate.h"
#include "memutils.h"
#include "piecegeometry.h"
#include "physics.h"

#include "atl/ownedptr.h"
#include "atl/ptr.h"
#include "grcore/fvf.h"
#include "grcore/indexbuffer.h"
#include "grcore/matrix43.h"
#include "grcore/vertexbuffer.h"
#include "math/random.h"
#include "shaderlib/rage_constants.h"
#include "system/container.h"
#include "vector/matrix34.h"
#include "crackstemplate.h"

namespace rage {

class bgCrackStarMap;
class bgCrackType;
class bgDrawable;
class bgGpuBuffers;
class bgHitInfo;
class bgMesh;
class bgMeshEditor;
class bgOneSolidPiece;
struct mshMaterial;
class phBound;


class bgPaneModelInfoBase
{
public:
	bgPaneModelInfoBase();
	bgPaneModelInfoBase(datResource& rsc);
	bgPaneModelInfoBase(const bgPaneModelInfoBase& in_cloneMe);
	DECLARE_PLACE(bgPaneModelInfoBase);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	static bgPaneModelInfoBase* CreateModelInfo(mshMaterial* in_pMaterial, 
		                                        u32 in_channelMask, 
												u8 in_shaderindex, 
												u8 in_glassType, 
												int in_boneIndex);

	static bgPaneModelInfoBase* CreateModelInfo(grcVertexBuffer* in_pVertexBuffer,
												grcIndexBuffer* in_pIndexBuffer,
												u8 in_shaderIndex,
												u8 in_glassType,
												int in_boneIndex);

	void ComputeBoundsOffsets(Mat34V_In in_matrix, const phBound& in_bound);

	// Determine whether the specified mesh forms one or more sides of the frame of this pane.
	u8 FindFrameOverlap(mshMaterial* in_pMaterial, int in_boneIndex, Mat34V_In in_paneMatrix, Mat34V_In in_frameMatrix);

	bool ComputeModelInfo(bgMeshEditor& in_MeshEd, int in_boneIndex = -1);

	void ComputePlane(Vector4& out_plane) const;

	// Info needed to transform from 'pane-space' to model-space
	Vector3 m_posBase;
	Vector3 m_posWidth;
	Vector3 m_posHeight;
	// Info to generate texture coordinates from pane coordinates
	Vector2 m_uvMin;
	Vector2 m_uvMax;
	// fvf computed from original model
	grcFvf m_fvf;

	// visual thickness
	float m_thickness;
	// special flags
	u16 m_flags;
	// glass pane type
	u8 m_glassType;
	// shader index into the source drawable's shadergroup
	u8 m_shaderIndex;

	// physics bounds offset from the glass pane
	float m_boundsOffsetFront;
	float m_boundsOffsetBack;

	Vector3 m_tangent;			   // use only if (m_flags & kTangent) is true

	static const int kDecal = 1;   // indicates this has decal coords
	static const int kTangent = 2; //

};

class bgFallingPhysics
{
public:
	bgFallingPhysics() : m_pPhysics()
	{
	}

	bgPhysics* m_pPhysics;

	void Cleanup()
	{
		if (m_pPhysics)
		{
			m_pPhysics->Cleanup();
		}
	}

	// Just a pointer copy here
	bgFallingPhysics& operator = (bgFallingPhysics& that)
	{
		m_pPhysics = that.m_pPhysics;
		return *this;
	}
};

class bgPhysicsData : public atArray<bgFallingPhysics>
{
public:
	bgPhysicsData() {}

	// deep copy
	bgPhysicsData(bgPhysicsData& rhs)
	{
		*this = rhs;
	}

	void Cleanup()
	{
		for (int index = 0; index < m_Count; ++index)
		{
			m_Elements[index].Cleanup();
		}
	}

	// Performs a deep copy of all elements in use
	bgPhysicsData& operator = (const bgPhysicsData& rhs);
};

//#define GLASS_PROFILING

// *************************************************************************************
// ******* structure (class) needed to hold data
// *************************************************************************************

class bgBreakable 
{
public:
	enum eFrameFlags
	{
		kFF_Top      = 0x01,
		kFF_Bottom   = 0x02,
		kFF_Left     = 0x04,
		kFF_Right    = 0x08,
		kFF_All		 = 0x0F
	};

	static const int kPieceId_Visited = -1;  // used to flag bgMesh polygons that have already been added to a piece
	static const int kPieceId_Visiting = -2; // used to flag bgMesh polygons being added to a piece during it's construction

public:
	// A constant for how many matrices are possible
	enum eConstants
	{
		FIRST_FALLING_PIECE = 1,
		REINFORCED_FALLING_COUNT = 8
	};

	//----------------------------------------
	//
	// Functor to allow the client to modify a falling piece
	// before it is added to the physics world.  Through the
	// piece, it also has access to all the physics objects.
	// It can get the physics instance through the collider.
	//
	typedef Functor2<bgPhysics &, float &> PhysicsLifeModifier;

	static void InitClass(int in_heapSize);

	static void ShutdownClass();

	// specify viewport data used to compute lod
	static void SetViewportData(Vec3V_In in_cameraPos, int in_viewportSize, float in_tanFov);

	//Construction.
	bgBreakable();

	//----------------------------------------
	//
	// Given the specified buffers, compute a 2D convex silhouette that encompasses that geometry
	// and values that will map any 3d vertex into a 2d (0->1) range that indicates the location
	// of that point projected onto the pane, and also the inverse mapping from 2d space to 3d.
	//
	// That's what it should do, anyway.  What it actually does is find the largest 
	// triangle in the mesh, and use the texture coordinates of that triangle's vertices 
	// to compute a simple rectangular silhouette that would contain all the geometry, 
	// assuming a uv's are restricted to a 0-1 range and follow the same planar mapping 
	// as the test triangle.
	//

	// Init normal glass with default physics instances and functor for falling pieces
	//DEPRECATED
	void Init(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, bool bulletproof = false, int boneIndex = -1)
	{
		if (bulletproof)
			InitBulletproof(in_glassType, in_pVertices, in_pIndices, boneIndex);
		else
			InitNormal(in_glassType, in_pVertices, in_pIndices, boneIndex);
	}

	// Init normal glass with custom physics instances and default functor for falling pieces
	//DEPRECATED
	void Init(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int boneIndex = -1)
	{
		InitNormal(in_glassType, in_pVertices, in_pIndices, boneIndex);
	}

	// Init normal glass with default physics instances and custom functor for falling pieces
	//DEPRECATED
	void Init(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, PhysicsLifeModifier modifier, int boneIndex = -1)
	{
		InitNormal(in_glassType, in_pVertices, in_pIndices, modifier, boneIndex);
	}

	// Normal
	// Init normal glass with default physics instances and functor for falling pieces
	void InitNormal(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int boneIndex = -1)
	{
		InitNormal(in_glassType, in_pVertices, in_pIndices, PhysicsLifeModifier(), boneIndex);
	}

	// Init normal glass with custom physics instances and functor for falling pieces
	void InitNormal(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, PhysicsLifeModifier modifier, int boneIndex = -1);

	// Reinforced
	// Init reinforced glass with default physics instances and functor for falling pane
	void InitReinforced(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int in_health, int boneIndex = -1)
	{
		InitReinforced(in_glassType, in_pVertices, in_pIndices, in_health, PhysicsLifeModifier(), boneIndex);
	}

	// Init reinforced glass with custom physics instances and functor for falling pane
	void InitReinforced(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int in_health, PhysicsLifeModifier modifier, int boneIndex = -1);

	// Bulletproof
	// Init bulletproof glass
	void InitBulletproof(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int boneIndex = -1);

	// init using resourced model info
	void InitFromModelInfo(const bgPaneModelInfoBase& in_modelInfo, Mat34V_In in_xform, float in_mass);

	/**
	 * This is a bit of a hack but its good for performance.
	 */
	static void SetSafeToUseVSLight(bool bSafe) { m_bSafeToUseVSLight = bSafe; }

	static void SetSilentHit(bool bEnable) { sm_bSilentHit = bEnable; }

	static bool IsSilentHit() { return sm_bSilentHit; }

#if __ASSERT
	// Check if this pane has any life pieces left
	bool IsAlive() const { return GetCurrentPieceCount()[0] > 0; }
#endif // __ASSERT

private:
	void InitInternal(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int boneIndex);

	void InitInternalFromModelInfo(const bgPaneModelInfoBase& in_modelInfo);

	void CalculateSizeTypeIndex(const bgPaneModelInfoBase& modelInfo);

	// Indicate if VFX and audio should be turned of for glass hits
	static bool sm_bSilentHit;

public:

	// copy allocated data to private heap
	// recycle other glass as necessary to make room
	void Pack();

	// unpack from private heap to stack heap in preparation for generating impacts
	void Unpack();

	//----------------------------------------
	// Remove any hits, restoring the pane to it's pristine, unbroken state.
	// Return glass to state after Init.
	//
	void Reset();

	//----------------------------------------
	// Clear pointers and arrays
	// Must call Init before using glass again.
	//
	void Shutdown();

	typedef u32 TriangleId;
	static const TriangleId kInvalidTriangle = 0xffffffff;
	TriangleId GetNextPaneTriangle(TriangleId id) const;
	bool GetPaneTriangleInfo(TriangleId id,
							 Vector3::Vector3Ref outPosA, 
							 Vector3::Vector3Ref outPosB, 
							 Vector3::Vector3Ref outPosC, 
							 Vector3::Vector3Ref outNormal) const;
	
	TriangleId GetNextFallingTriangle(TriangleId id) const;
	bool GetFallingTriangleInfo(TriangleId id,
								Vector3::Vector3Ref outPosA, 
								Vector3::Vector3Ref outPosB, 
								Vector3::Vector3Ref outPosC, 
								Vector3::Vector3Ref outNormal) const;

	void GetFallingTriangleMatrix(TriangleId id, Mat34V_InOut outMatrix)const;

	//----------------------------------------
	// Set and get glass' transform
	//
	void SetTransform(const Matrix34& transform)
	{
		m_transform = transform;
	}

	const Matrix34& GetTransform() const
	{
		return m_transform;
	}

#if __BANK
	static void AddWidgets(bkBank& bank);

	
#endif
	//----------------------------------------
	// Get the amount of falling pieces
	int GetFallingPieceCount() const;
	//----------------------------------------
	// Mostly update the pieces flying out of 
	//  the pane by applying physics and collision
	//
	// input: in_delatTime ==>		time elapsed since last call
	//
	void Update(float in_deltaTime);

	float CalculateViewportCoverage() const;
	void AdjustLod();

	//-------------------------------------------------------
	//We got hit so we trigger a remake of the glass geometry &
	//	keep setup some data
	//
	// in_CrackType:            variety of crack to generate, determined by weapon/ammo responsible for causing the impact.
	//                          See bgCracksTemplate.h/cpp
	// in_hitLoc :				world-space impact location
	// in_worldImpactImpulse:	Impulse of the impact
	// in_damage:				Damage from impact
	//
	void Hit(int in_CrackType,
			 Vec3V_In in_worldHitLoc,
			 Vec3V_In in_worldImpactImpulse,
			 int in_damage = 0);

	void HitInternal(int in_CrackType,
		Vec2V_In in_hitLoc2d,
		Vec3V_In in_worldImpactImpulse,
		int in_damage = 0);

	//
	// Inform this pane that one or more frame/muntin pieces were broken off.
	//
	void BreakFrame(int in_CrackType, u32 in_BrokenFrameFlags, Vec3V_In in_worldImpulseDir);

	//------------------------------------------------------------------------------
	// convert a 3D point on the glass plane (must be inside the rectangle) to a 
	// 2D point with value between 0,1 telling us where the point is located on our
	// 2D virtual window.
	//
	// This function is useful if you have a 3D point on the pane plane (inside the pane rectangle)
	// and you want to know the unit value of this point in 2D to be use in the Hit() function 
	// as in_hitLocX,in_hitLocY.
	//
	// Input: in_point3D:		3D point inside the pane rectangle, also found on the pane plane
	//
	Vec2V_Out Convert3DPointTo2DPoint(Vec3V_In in_point3D) const;

	// -----------------------------------------------------------------------------
	// Get the lod level currently in use (use constants from bgGeometryBuilder::eGlassLOD)
	//
	int GetLod() const
	{
		return GetGeometryData().m_lod;
	}

	// -----------------------------------------------------------------------------
	// Get the lod level currently in use (constants from bgGeometryBuilder::eGlassLOD)
	//
	void SetLod( int lod );
	static void SetMaxLod( int lod );

	//////////////////////////////////////////////////////////////////////////
	// bgDrawable interface

	// Get a copy of the matrix data
	atArray<Matrix43>& GetCurrentMatrices()
	{
		return m_FallingPieceMatrix;
	}
	
	const int* GetCurrentPieceCount() const
	{
		return m_PieceIndexCount;
	}

	const int* GetCurrentCrackCount() const
	{
		return m_CrackIndexCount;
	}

	bgPhysicsData const & GetFallingPhysics() const
	{
		return m_BreakableData.GetPhysicsData();
	}

	const bgHitInfo* GetCurrentHit() const;
	const bgHitInfo* GetHit(int index) const;
	int GetIndex(const bgHitInfo& hit) const;

	//Choose the matrix for a location on the pane
	unsigned ChoosePaneMatrix(Vector2 const &location) const;

	const bgGeometryData& GetGeometryData() const
	{
		return m_BreakableData.GetGeometryData();
	}

	bool IsValid() const
	{
		return (&GetGeometryData() != NULL) && (&GetFallingPhysics() != NULL);
	}

	typedef atArray<bgPanePiece> PanePieces;
	const PanePieces& GetPanePieces() const
	{
		return GetGeometryData().m_panePieces;
	}

	typedef atArray<bgFallingPiece> FallingPieces;
	const FallingPieces& GetFallingPieces() const
	{
		return GetGeometryData().m_fallingPieces;
	}

	typedef atArray<bgEdge> PaneEdges;
	const PaneEdges& GetPaneEdges() const
	{
		return GetGeometryData().m_paneEdges;
	}

	typedef atArray<Vector2> PanePoints;
	const PanePoints& GetPanePoints() const
	{
		return GetGeometryData().m_panePoints;
	}

	typedef atArray<float> PaneDepths;
	const PaneDepths& GetPaneDepths() const
	{
		return GetGeometryData().m_paneDepths;
	}

	bool IsPointDecalVisible(int index) const
	{
		return GetGeometryData().m_pointDecalVisibility.IsSet(index);
	}

	bool IsPointCrackVisible(int index) const
	{
		return GetGeometryData().m_pointCrackVisibility.IsSet(index);
	}

	const atBitSet& GetPointDecalVisibility() const
	{
		return GetGeometryData().m_pointDecalVisibility;
	}

	const atBitSet& GetPointCrackVisibility() const
	{
		return GetGeometryData().m_pointCrackVisibility;
	}

	float GetThickness() const
	{
		return GetGeometryData().m_thickness;
	}

	float GetBoundsOffsetFront() const
	{
		return m_boundsOffsetFront;
	}

	float GetBoundsOffsetBack() const
	{
		return m_boundsOffsetBack;
	}

	// Get ratio of fallen area to total
	float GetFallenRatio() const
	{
		return m_fallenRatio;
	}
	float GetLastFallenRatio() const
	{
		return m_lastFallenRatio;
	}
	// Get ratio of cracked or fallen area to total
	float GetCrackedRatio() const
	{
		return m_crackedRatio;
	}
	float GetLastCrackedRatio() const
	{
		return m_lastCrackedRatio;
	}
	// Get maximum health of reinforced pane
	int GetFullHealth() const
	{
		return m_fullhealth;
	}

	// Get health of reinforced pane
	int GetHealth() const
	{
		return m_health;
	}

	// Get a bounding box surrounding all shards as well as the pane itself
	// NOTE: doesn't work with reinforced glass...I don't have any reinforced glass to test with at the moment.
	void GetBoundingBox(Vector3::Ref boxMin, Vector3::Ref boxMax) const 
	{
		boxMin = m_boxMin;
		boxMax = m_boxMax;
	}

	// Get a bounding sphere surrounding all shards as well as the pane itself
	// NOTE: doesn't work with reinforced glass...I don't have any reinforced glass to test with at the moment.
	Vector4 GetBoundingSphere()
	{
		return m_boundingSphere;
	}

	//*************************************************************************************
	// Geometry manipulation for rendering
	//*************************************************************************************
	
	//-----------------------------------------------------------------------------
	//When a state change, the geometry has to be updated.  The following happen:
	//	1 - Previous to this function, all data must have been updated properly.  That generally
	//		means all 2D triangles and other pieces/cracks data must have been updated and ready to be use.
	//
	//	2 - This function will call multiple function to create different geometry:
	//		2.A - 3D Cracks
	//		2.B - 3D pieces
	//
	void UpdateBuffers();

	// Called by bgDrawable to check for updated buffers
	bool GetUpdatedBuffers(atPtr<bgGpuBuffers>& out_pBuffers);

	// Get strength of pane
	enum Strength
	{
		STRENGTH_NORMAL,
		STRENGTH_REINFORCED,
		STRENGTH_BULLETPROOF
	};
	Strength GetStrength() const
	{
		return m_strength;
	}

	//Interface for bgEdgeAdder<bgBreakable>
	void Add(bgEdge const &edge)
	{
		GetPaneEdgesNonConst().Grow() = edge;
	}

	int GetEdgeCount() const
	{
		return GetPaneEdges().GetCount();
	}

	bgIndex GetP0(bgEdgeRef ref) const
	{
		return ref.GetP0(GetPaneEdges());
	}

	bgIndex GetP1(bgEdgeRef ref) const
	{
		return ref.GetP1(GetPaneEdges());
	}

	const grcFvf& GetFvf() const
	{
		return m_fvf;
	}

	bgIndex GetLeftPoly(bgEdgeRef ref) const
	{
		return ref.GetLeftPoly(GetPaneEdges());
	}

	bgIndex GetRightPoly(bgEdgeRef ref) const
	{
		return ref.GetRightPoly(GetPaneEdges());
	}

	void SetLeftPoly(bgEdgeRef ref, bgIndex in_polygon)
	{
		ref.SetLeftPoly(GetPaneEdgesNonConst(), in_polygon);
	}

	void SetRightPoly(bgEdgeRef ref, bgIndex in_polygon)
	{
		ref.SetRightPoly(GetPaneEdgesNonConst(), in_polygon);
	}

	static sysMemAllocator& GetPrivateHeap();

	static void GetPrivateHeapStats(size_t& used, size_t& available, size_t& largest);

private:

	//-----------------------------------------
	//Return a location on the pane of glass where the crack map should 
	// have it's center based on what the user intended
	//
	// Input:	centerType:			xml user data telling us where the center should be
	//			impactLocation:		Center good go at the i8mpact location.  Passed in in case we need it.
	// Output:  out_location:		x,y location with value between 0 and 1.
	//								Extreme example:
	//								0,0 = Crack map center should be located at the top-left corner of the pane of glass
	//								1,1 = Crack map center should be located at the bottom-right corner of the pane of glass
	//
	void SetCrackMapCenterLocationValue(const enum bgCrackType::bgCrackPlacementType centerType, Vector2& impactLocation, Vector2* out_location);

	//-----------------------------------------
	//Return the proper crack map rotation based on what the user intended
	//
	// Input:	rotationType:		xml user data telling us what type of rotation we need to calculate
	//			paneDimension:		Pane of glass dimension where the crack map will be applied
	//			
	// Output:	out_rotation:		angle in radian at which the crack map should be rotated
	//
	void SetCrackMapRotationValue(const enum bgCrackType::bgCrackRotationType rotationType, Vector2& paneDimension, float* out_rotation);

	//-----------------------------------------
	//Return the proper crack map scaling based on what the user intended
	//
	// Input:	scalingType			xml user data telling us what type of scaling we need to calculate
	//			centerType:			xml user data telling us where the center should be
	//			rotationType:		xml user data telling us what type of rotation we need to calculate
	//			paneDimension:		Pane of glass dimension where the crack map will be applied
	//			overwriteScaling:	xml x,y scaling value if we want to overwrite the scaling
	//
	// Output:	out_scaling			Crack map scaling
	//
	void SetCrackMapScalingValue(	const enum bgCrackType::bgCrackScalingType scalingType, 
		const enum bgCrackType::bgCrackPlacementType centerType, 
		const enum bgCrackType::bgCrackRotationType rotationType,
		Vector2& paneDimension, Vector2& overwriteScaling, Vector2* out_scaling);


	static const int kGeometryBucket = 0; // memory bucket for geometry data
	static const int kPhysicsBucket = 1;  // memory bucket for physics data

	// Storage for variable-size, dynamically allocated data goes here.
	class BreakableData
	{
	public:
		BreakableData()
		{
		}

		~BreakableData()
		{
			Shutdown();
		}

		// Pack all data into a single block of memory allocated from our private heap
		void Pack();

		// Unpack to the current (temporary stack-allocated) heap to allow modification.
		void Unpack();

		// Initialize to a valid, but empty, packed state
		void Init();

		// clean up, freeing all memory
		void Shutdown();

		const bgGeometryData& GetGeometryData() const
		{
			return m_geometryData.GetData();
		}

		bgGeometryData& GetGeometryData()
		{
			return m_geometryData.GetData();
		}

		size_t GetGeometryDataSize() const
		{
			return m_geometryData.GetSize();
		}

		const bgPhysicsData& GetPhysicsData() const
		{
			return m_physicsData.GetData();
		}

		bgPhysicsData& GetPhysicsData()
		{
			return m_physicsData.GetData();
		}

	protected:
		// Data for falling shard physics
		bgPackableData<bgPhysicsData, kPhysicsBucket> m_physicsData;
		// Data for geometry
		bgPackableData<bgGeometryData, kGeometryBucket> m_geometryData;
	};

	PanePieces& GetPanePiecesNonConst()
	{
		return GetGeometryDataNonConst().m_panePieces;
	}

	FallingPieces& GetFallingPiecesNonConst() 
	{
		return GetGeometryDataNonConst().m_fallingPieces;
	}

	PanePoints& GetPanePointsNonConst()
	{
		return GetGeometryDataNonConst().m_panePoints;
	}

	PaneEdges& GetPaneEdgesNonConst()
	{
		return GetGeometryDataNonConst().m_paneEdges;
	}

	atArray<float>& GetPaneDepthsNonConst()
	{
		return GetGeometryDataNonConst().m_paneDepths;
	}

	atBitSet& GetPointDecalVisibilityNonConst()
	{
		return GetGeometryDataNonConst().m_pointDecalVisibility;
	}

	atBitSet& GetPointCrackVisibilityNonConst()
	{
		return GetGeometryDataNonConst().m_pointCrackVisibility;
	}

	bgGeometryData& GetGeometryDataNonConst()
	{
		return m_BreakableData.GetGeometryData();
	}

	bgPhysicsData& GetFallingPhysicsNonConst()
	{
		return m_BreakableData.GetPhysicsData();
	}

	//*************************************************************************************
	// ******* Initialization
	//*************************************************************************************

	//-----------------------------------------------------------------
	//This function read the model and keep some information such as the model bounding box
	// in order to later create geometry for rendering.
	//
	//  NOTE:	The drawbable model must be a rectangle shape.  More work would be needed to create
	//			more complex shape.
	//
	// Things we need from the model:
	//			1 - We keep the 4 corner vertex of the model for later interpolating in between them
	//				for rendering.
	//
	//			2 - Compute the plane of the glass
	//
	void CreateInitialSilhouetteFromModel(grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int boneIndex = -1);

	//-----------------------------------------------------------------
	// When a hit happen for the first time, the pane get broken into smaller 
	// pieces that might not all be visible yet but never less are there for future breaking...
	//
	// Input: in_hitInfo:  Data about the hit coming from artist template library
	//
	void Create2DDataFromHit(const bgHitInfo& in_hitInfo);

	// Generate the 2D data using the template for any hit after the first one
	void Create2DDataFromSequentHit(const bgHitInfo& in_hitInfo, int iPanePiece);

	//-----------------------------------------------------------------
	// Break a piece of glass into smaller pieces
	//
	// Input:	in_iPanePiece:					Index in array of pieces of the piece to cut
	//			in_brokenPiecesCount:			total pieces that are falling.  This does not include the one that will be cut into smaller one
	//			in_pieceSizeTriggerCut:			If a piece is equal or bigger then this size we will try to cut the piece smaller
	//			inOut_piecesToCutReamining:		This is the remaining number of slot available for new pieces created when we cut.  If this reach 0
	//											it means that we cannot cut anymore.
	//
	void CreateSmallerPieces(int in_iPanePiece, int in_brokenPiecesCount, float in_pieceSizeTriggerCutSqr, int* inOut_piecesToCutReamining);

	//-----------------------------------------------------------------
	// Construct a pane piece by recursively adding adjacent polygons and the
	// appropriate edges from those polygons to form the silhouette.
	//
	// in_panePieceIndex: index of the pane piece being constructed
	// in_iPoly: polygon we're currently adding to the piece
	// in_startEdge: edge _before_ the first edge in the polygon that we want to consider adding to the silhouette
	// in_mesh: mesh containing the polygons and edges we're considering adding to the pane piece
	// in_polyList: list of polygon indices of all the polygons we add to the piece
	//
	typedef bgEdgeAdder<bgBreakable> BreakableEdgeAdder;
	void ConstructPanePiece(BreakableEdgeAdder &adder, u16 in_panePieceIndex, u16 in_iPoly, bgEdgeRef startEdge, bgMesh& in_mesh, atArray<u16>& in_polyList, u16 iPointOffset = 0);

	//------------------------------------------------------------------------------
	// When adding a piece, copy all the relevant data from the archetype
	//
	// Input:	out_destinationNewPiece:		Destination to copy the data to
	//			in_sourceArhcetype:				Source to take the data from
	//			in_hitInfo:						This copy happen when a hit happen.  This is 
	//											the hit information.
	//
	static void CopyPoints(bgMesh& out_dest, const atArray<Vector2>& in_sourcePoints, const bgHitInfo& in_hitInfo);

	//-----------------------------------------------------------------------------
	// 
	// Convert a pane piece into a falling piece
	//
	// WARNING: The function use the latest impact info m_hit(last)
	//
	// Input:	in_piece:				Piece we want flying out
	//			distanceFromImpact:		How far from the impact this piece is.
	//									0 = right on the impact, fly out very fast
	//									1 = at the limit of the impact, fly out slow
	//									>1 = very far, might just fall down
	//
	void PieceFlyOutOnImpact(int in_iPanePiece, int in_brokenPiecesCount);

	//-----------------------------------------------------------------
	// Create a falling piece from a set of points.  This function creates
	// the geometry and all other data necessary to update dynamics.
	//
	// Input:	in_points				Set of points to create the piece
	//			in_pointCount			Point count in in_points
	//
	// Output:	One more solid piece added to m_fallingPieces array.  However before
	//			adding a new one at the end of the array the code will look if one piece
	//			already in the array is inactive and ready to be deleted, therefore replaced 
	//			by this new piece.
	//
	void CreateOneFallingPiece(const bgPanePiece& panePiece, const Vector2* in_points, int in_pointCount, int in_brokenPiecesCount);

	//-----------------------------------------------------------------
	// Apply impulse to falling physics
	//
	// Input:	in_physics				FallingPhysics object that receives the impulse
	//			in_inst					Physics instance to associate with the FallingPhysics
	//			in_center				Pane-space center of the piece
	//			in_size					Dimensions of the piece in the standard basis
	//			in_mass					Piece's mass
	//			in_impulseLocation		Pane-space location at which to apply the impulse
	//			in_worldImpactImpulse	World-space impulse to apply
	//
	void ApplyImpulse(bgFallingPhysics &in_physics, Vector3::Param in_center, Vector3::Param in_size, float in_mass, Vector3::Param in_impulseLocation, Vector3::Param in_worldImpactImpulse, float area2 = 1.f );

	//-----------------------------------------------------------------------------
	// Return if a piece is floating or not.  A piece if floating if it is not directly or 
	// indirectly connected to the window frame
	//
	bool IsFloating(int in_pieceIndex);

	//-----------------------------------------------------------------------------
	// 
	// Set the 'float' flag on this piece and all its neighbors
	//
	void SetFloatState(int in_pieceIndex, u32 in_floatFlag);

	//-----------------------------------------------------------------------------
	// After impact, some piece must break and fly out of the pane
	//
	// Input: in_hitInfo:		The current hit
	//
	// return: true if the window was hit, false if the hit did not touched anything
	bool HitPane(bgHitInfo& in_hitInfo, bool bUseInitKillRadius);

	//-----------------------------------------------------------------------------
	// Flag the specified pane piece as inactive.
	// Make sure all its silhouette edges no longer point back to it.
	// Free up as much associated memory as possible; pane pieces are never reused.
	//
	void KillPanePiece(int in_iPanePiece, bool bSetVisibility = true);

public:
	//-----------------------------------------------------------------------------
	//
	// Modify the physics using a client-supplied functor
	//
	void ModifyPhysicsLife(bgPhysics &physics, float &life) const
	{
		if (m_physicsLifeModifier)
			m_physicsLifeModifier(physics, life);
	}

	// This controls the maximum amount of batches the glass pieces can use
	static const s32 sm_iMaxBatches = 5;

private:
	//-----------------------------------------------------------------------------
	//
	// Update normal glass
	//
	void UpdateNormal(float in_deltaTime);

	//-----------------------------------------------------------------------------
	//
	// Update reinforced glass
	//
	void UpdateReinforced(float in_deltaTime);

	Vector3 Transform(const Vector2& paneCoord) const
	{
		return GetGeometryData().Transform(paneCoord);
	}

	//*************************************************************************************
	//DATA
	//*************************************************************************************

	// Functor for modifying falling pieces
	PhysicsLifeModifier m_physicsLifeModifier;

	// glass world transform
	Matrix34 m_transform;

	// rotation from pane to AABB
	Matrix33 m_rotation;

	// Bounding box, surrounding all shards as well as the pane itself
	Vector3 m_boxMin;
	Vector3 m_boxMax;

	// Bounding Sphere, surrounding all shards as well as the pane itself
	Vector4 m_boundingSphere;

	// glass type
	int m_glassType;

	// glass size Index
	int m_glassSizeIndex;

	// Set true when new vertex/index buffers must be regenerated
	// e.g., when a new impact occurs or lod is changed
	bool		m_bUpdateBuffers;
	bool        m_bHasUpdatedBuffers;
	bool		m_bHasAssignedBuffers;

	BreakableData						    m_BreakableData;

	//Each pieces refer to this array:
	//	Each falling piece use a different matrix
	//	Each solid piece use the first matrix, which is set to identity
	//
	//We are using the skinning bones array in the shader to place our movable pieces matrix so it make sense to use
	// the skinning maximum bones count for our maximum count.
	atArray<Matrix43>						m_FallingPieceMatrix;

	// The amount of indices used by each one of the pieces.
	int m_PieceIndexCount[sm_iMaxBatches];

	// The amount of indices used by each one of the piece cracks.
	int m_CrackIndexCount[sm_iMaxBatches];

	//Ratio of areas to total
	float									m_fallenRatio;
	float									m_crackedRatio;
	float									m_lastFallenRatio;
	float									m_lastCrackedRatio;


	float									m_boundsOffsetFront;
	float									m_boundsOffsetBack;

	// vertex format from original model
	grcFvf									m_fvf;
	//Maximum health of reinforced glass
	int										m_fullhealth;

	//Health of reinforced glass
	int										m_health;

	//Number of fallen pieces created by this hit
	int										m_fallen;

	// Falling pieces start fading square distance
	static float sm_fDistFadeStart;

	// Falling pieces end fading square distance
	static float sm_fDistFadeRange;

public:
	//Physics and bend data for reinforced glass
	struct Reinforced
	{
		bgFallingPhysics m_physics;
		float m_life;
		Vector3 m_axes[REINFORCED_FALLING_COUNT];
	};
	atOwnedPtr< Reinforced >				m_pReinforced;

private:
	float									m_mass;

	u32										m_BrokenFrameFlags;
	//Strength of glass
	Strength								m_strength;

	atPtr<bgGpuBuffers>                     m_pBuffers;
public:
#ifdef GLASS_PROFILING
	//Profiling
	// Total amount of time to process a hit
	float									m_profileHitTotal;
	//Total amount of time to clamp the pieces when touching the edge of the window pane
	float									m_profileHitClampTotal;
	//Total amount of time to decompose the pieces silhouette into triangle
	float									m_profileHitTriangleDecompositionTotal;
	//Total amount of time to displace the silhouette vertex to create bevel and cracks between pieces in the geometry pass
	float									m_profileHitPieceDisplacement;
	//2D data need update by all the hits together
	float									m_profileBreakPieces;
	//Bunch of counter for decomposing the break piece
	float									m_profileBreakPieces1;
	float									m_profileBreakPieces2;
	float									m_profileBreakPieces3;
	float									m_profileBreakPieces4;
	float									m_profileBreakPieces5;
	float									m_profileBreakPieces6;
	float									m_profileBreakPieces7;
	float									m_profileBreakPieces8;
	float									m_profileBreakPieces9;
	float									m_profileBreakPieces10;
	float									m_profileBreakPieces11;
	float									m_profileBreakPieces12;
	float									m_profileBreakPieces13;
	// make pieces and crack visible total time
	float									m_profileVisibility;
	//Total amount of time to copy the data from archetype to our new piece
	float									m_profileCopy;
#endif
	
	//Get the sphere around the glass
	Vec3V_Out GetPaneCenter() const;

	float GetPaneSize() const;

	// Get the total quantity, in bytes, of dynamically allocated memory owned by a 
	// bgBreakable instance
	int GetDynamicStorage() const;

	int GetGlassType() const
	{
		return m_glassType;
	}
	int GetGlassSizeIndex() const
	{
		return m_glassSizeIndex;
	}

#if __PFDRAW
	void ProfileDraw();
#endif // __PFDRAW

protected:
	//
	// Convert a crack mesh to a bgMesh for clipping.
	//
	static void ConvertCrackMesh(bgMesh& out_dest, const bgHitInfo& in_hitInfo, int in_glassType, int in_glassSizeIndex);

private:

	static mthRandom sm_Rand;

	/**
	 * Indicate if its safe to skip low LOD to very low LOD.
	 */
	static bool m_bSafeToUseVSLight;

//	friend class phGlass;
};
// structure used for encapsulating data required for draw command
class bgDrawableDrawData
{
public:

	//-------------------------------------------------------
	//Sets up the drawable data used for adding to the Draw Command
	//
	// in_breakable:			Breakable Glass
	// in_drawable:				Breakable Glass Drawable	
	//
	void SetupDrawData(const bgBreakable &in_breakable, const bgDrawable &in_drawable, float* transforms, int numTransforms);

	const Matrix34 &GetMatrix() const {return m_matrix;}
	const Vector4 &GetCrackTexMatrix() const {return m_crackTexMatrix;}
	const Vector4 &GetCrackTexOffset() const {return m_crackTexOffset;}
	bgGpuBuffers* GetBuffers() const {return m_pBuffers;}
	bgCrackStarMap* GetCrackStarMap() const {return m_pCrackStarMap;}
	float* GetTransforms() const {return m_transforms;}
	int GetNumTransforms() const {return m_numTransforms;}
	int GetLOD() const {return m_lod;}
	const int* GetPieceIndexCount() const { return m_arrPieceIndexCount; }
	const int* GetCrackIndexCount() const { return m_arrCrackIndexCount; }

private:
	Matrix34 m_matrix;
	Vector4 m_crackTexMatrix;
	Vector4 m_crackTexOffset;
	bgGpuBuffers* m_pBuffers;
	bgCrackStarMap* m_pCrackStarMap;
	float* m_transforms;
	int m_numTransforms;
	int m_lod;
	const int* m_arrPieceIndexCount;
	const int* m_arrCrackIndexCount;
};

} // namespace rage

#endif //BREAKABLEGLASS_BREAKABLE_H
