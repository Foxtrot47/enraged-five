// 
// breakableglass/memutils.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_MEMUTILS_H 
#define BREAKABLEGLASS_MEMUTILS_H 

#if __SPU
// use #include instead of #error, as then gcc gives the include stack
#include "error, memutils.h not supported in spu code"
#endif

#include "bgchannel.h"
#include "atl/delegate.h"
#include "physics/simulator.h"
#include "system/container.h"
#include "system/tinyheap.h"

namespace rage {
	class bgUseMemoryBucket
	{
	public:
		// PURPOSE: Constructor
		// PARAMS:	bucket - New bucket to bill memory allocations to, or -1 to keep the current one
		bgUseMemoryBucket(int bucket)	{
			m_OldBucket=sysMemCurrentMemoryBucket;
			if (bucket != -1)
				sysMemCurrentMemoryBucket=bucket;
		}

		static void SwitchTo(int bucket) {
			sysMemCurrentMemoryBucket=bucket;
		}

		// PURPOSE: Destructor; restores previous memory bucket
		~bgUseMemoryBucket()			{sysMemCurrentMemoryBucket=m_OldBucket;}
	private:
		int m_OldBucket;
	};

	//////////////////////////////////////////////////////////////////////////
	//
	// Wrap sysTinyHeap to make it a full-blown sysMemAllocator
	// Support a very small number of buckets by stealing bits in the node header.
	// This basically couldn't be much more evil and hacky.  But it is much 
	// faster than sysMemSimpleAllocator.
	//
	class sysMemTinyAllocator : public sysMemAllocator
	{
	public:
		static const int kBucketCount = 2; // bg only needs two buckets, but up to 8 would probably be ok
		static const int kBucketMask = kBucketCount - 1; // kBucketCount must be power of two for this to work

		sysMemTinyAllocator(void* start, size_t size, int overrideBucket = -1) 
			: m_tinyHeap(start, size)
			, m_size(size)
			, m_overrideBucket(overrideBucket)
		{
			Assert(m_overrideBucket < kBucketCount);
			for (int bucket=0; bucket < kBucketCount; bucket++)
			{
				m_bucketSizes[bucket]=0;
			}
		}

		SYS_MEM_VIRTUAL void* Allocate(size_t size,size_t UNUSED_PARAM(align),int UNUSED_PARAM(heapIndex))
		{
			int bucket = m_overrideBucket >= 0 ? m_overrideBucket : sysMemCurrentMemoryBucket;
			void* ptr = m_tinyHeap.Allocate(size,16,bucket);
			Assertf(ptr, "TinyHeap Allocate failed");

			if (ptr)
				m_bucketSizes[bucket] += m_tinyHeap.GetSize(ptr);
			return ptr;
		}

		SYS_MEM_VIRTUAL void Free(const void *ptr)
		{
			m_bucketSizes[m_tinyHeap.GetBucket(const_cast<void*>(ptr))] -= m_tinyHeap.GetSize(const_cast<void*>(ptr));
			m_tinyHeap.Free(const_cast<void*>(ptr));
		}

		// '-1' to tally all memory
		SYS_MEM_VIRTUAL size_t GetMemoryUsed(int bucket)
		{
			if (bucket < 0)
			{	// tally all buckets
				int totalUsed = bucket = 0;
				do 
				{
					totalUsed += (int)m_bucketSizes[bucket++];
				} while (bucket < kBucketCount);
				return totalUsed;
			}
			if (Verifyf(bucket < kBucketCount, "bucket index %i >= bucket count %i", bucket, kBucketCount))
			{	// just this bucket size
				return m_bucketSizes[bucket];
			}
			// invalid bucket specified
			return 0;
		}

		SYS_MEM_VIRTUAL size_t GetMemoryAvailable()
		{
			return m_size - GetMemoryUsed(-1);
		}

		// Maximum size allocation that this allocator
		SYS_MEM_VIRTUAL size_t GetLargestAvailableBlock()
		{
			// Because the bucket hack is cleared before freeing, 
			// this one should work enough to catch an assert..
			return m_tinyHeap.GetLargestFreeBlock();
		}

		SYS_MEM_VIRTUAL size_t GetHeapSize() const 
		{ 
			return m_tinyHeap.GetHeapSize(); 
		}

		size_t GetLowWaterMark(bool reset)
		{
			return m_tinyHeap.GetLowWaterMark(reset);
		}
		
	private:
		atRangeArray<size_t, kBucketCount> m_bucketSizes;
		sysTinyHeap m_tinyHeap;
		size_t m_size;
		int m_overrideBucket;
	};

	//
	// Utility to create a temporary heap on the stack and set it to be the default heap within
	// the current scope.
	//
	class bgAutoStackHeap
	{
	public:
		bgAutoStackHeap(void* pStackMem, int size, bool bUseBuckets = true) 
			: m_stackAllocator(pStackMem, size, bUseBuckets ? -1 : 0)
			, m_prevAllocator(sysMemAllocator::GetCurrent())
			, m_prevBucket(sysMemCurrentMemoryBucket)
#if __ASSERT
			, m_prevStackHeap(sm_currentStackHeap)
#endif
		{
			sysMemAllocator::SetCurrent(m_stackAllocator);
			sysMemCurrentMemoryBucket=0;
			ASSERT_ONLY(sm_currentStackHeap = &m_stackAllocator;)
		}

		~bgAutoStackHeap()
		{
			bgAssertf(sysMemCurrentMemoryBucket==0, "bgAutoStackHeap::~bgAutoStackHeap(): expected current memory bucket to be 0, is actually %i", sysMemCurrentMemoryBucket);
			bgAssertf(m_stackAllocator.GetMemoryUsed(0) == 0, "%u bytes still allocated by stack allocator.", (unsigned)m_stackAllocator.GetMemoryUsed(0) );
			sysMemCurrentMemoryBucket=m_prevBucket;
			sysMemAllocator::SetCurrent(m_prevAllocator);
			ASSERT_ONLY(sm_currentStackHeap = m_prevStackHeap;)
		}

		size_t GetHeapSize()
		{
			return m_stackAllocator.GetHeapSize();
		}

		size_t GetMemoryUsed()
		{
			return m_stackAllocator.GetMemoryUsed(-1);
		}

		size_t GetLowWaterMark(bool reset)
		{
			return m_stackAllocator.GetLowWaterMark(reset);
		}

#if __ASSERT
		static sysMemAllocator* GetCurrentStackHeap()
		{
			return sm_currentStackHeap;
		}
#endif

	private:
		bgAutoStackHeap& operator = (const bgAutoStackHeap &);
		int m_prevBucket;
		sysMemTinyAllocator m_stackAllocator;
		sysMemAllocator& m_prevAllocator;
#if __ASSERT
		sysMemAllocator* m_prevStackHeap;
		static __THREAD sysMemAllocator* sm_currentStackHeap;
#endif

	};


#define MEMORY_BLOCK_HEAP(name,block,size)\
	int name##__heapSize = size;\
	void *name##__heapMem = (void*)block;

#define ALLOCATE_STACK_HEAP(name,size)\
	sysMemTinyAllocator::Node actual__##name##__heapMem[(size)/sizeof(sysMemTinyAllocator::Node)];\
	int name##__heapSize = sizeof(actual__##name##__heapMem);\
	void *name##__heapMem = (void*)actual__##name##__heapMem;

#define STACK_HEAP(name) name##__heapMem, name##__heapSize

#define STACK_HEAP_PARAMS(name) void * name##__heapMem, int name##__heapSize

#define USE_STACK_HEAP(name, useBuckets)\
	bgAutoStackHeap __heap(name##__heapMem, name##__heapSize, useBuckets);

	typedef bool (bgRecycleFunc)(size_t);

	template <class T, int _Bucket=-1>
	class bgPackableData
	{
	public:
		bgPackableData() : m_pData(NULL), m_Size(0)
		{
		}

		// Initialize to packed state
		void InitPacked(sysMemAllocator &heap, bgRecycleFunc recycler)
		{
			if (heap.GetLargestAvailableBlock() < sizeof(T) &&
				!(*recycler)(sizeof(T)))
			{
				return;
			}

			m_pData = rage_placement_new(heap.Allocate(sizeof(T), 16)) T;
			m_Size = sizeof(T);
		}

		// Initialize to unpacked state
		void InitUnpacked(T* in_pData)
		{
			TrapNZ((size_t)m_pData); // double-Init() w/o shutdown
			m_pData = in_pData;
		}

		// Free all data
		void Shutdown(sysMemAllocator& heap)
		{
			if (m_pData)
			{
				bgUseMemoryBucket memoryBucket(_Bucket);

				m_pData->Cleanup();

				if (IsPacked())
				{
					heap.Free(m_pData);
				}
				else
				{
#if __ASSERT 
					// Assuming the unpacked data was allocated from a stack heap,
					// we only need to actually delete it in ASSERT builds, where 
					// we verify in the stack heap destructor that all memory was freed.
					delete m_pData;
#endif
				}
				m_pData = 0;
			}
			m_Size = 0;
		}

		// Copy data into a container allocated from the specified heap 
		// Uses copy constructor, which is assumed to perform a deep copy
		// the memory used in the current heap by the specified bucket is used
		// as the initial estimate.
		void Pack(sysMemAllocator& heap, bgRecycleFunc recycler)
		{
			bgAssertf(!IsPacked(), "bgPackableData::Pack(): data is already packed!");

			sysMemAllocator& prev = sysMemAllocator::GetCurrent();
			bgAssertf(&prev == bgAutoStackHeap::GetCurrentStackHeap(), "bgPackableData::Pack() unpacked data must be allocated from a stack heap!");
			T* pDest = NULL;
			size_t initialSize = prev.GetMemoryUsed(_Bucket);
			bgDebugf2("bgPackableData::Pack(): container initialSize: %" SIZETFMT "d, heap largest available: %" SIZETFMT "d", initialSize, heap.GetLargestAvailableBlock());
			if ((heap.GetLargestAvailableBlock() < initialSize) &&
				!recycler(initialSize))
			{
				// not enough memory available
				pDest = NULL; // never happens unless not enough mem for a single piece of glass
			}
			else
			{
#if !__TOOL
				sysMemAllocator::SetCurrent(heap);
				sysMemContainerData containerData;
				sysMemContainer container(containerData);
				container.Init(initialSize); // @TODO: integrate and enable prefetch
				sysMemAllocator::SetCurrent(container);
				pDest = rage_new T(*m_pData);
				sysMemAllocator::SetCurrent(heap);
				container.Finalize();
				sysMemAllocator::SetCurrent(prev);
				m_Size = containerData.m_Size;
				bgDebugf2("bgPackableData::Pack(): final size %d", m_Size);
				bgAssertf(pDest == containerData.m_Base, "bgPackableData::Pack(): container base %p does not match pDest: %p", containerData.m_Base, pDest);
#endif
			}
#if __ASSERT
			// Since the unpacked data was allocated from a stack heap,
			// we only need to actually delete it in ASSERT builds, where 
			// we verify in the stack heap destructor that all memory was freed.
			delete m_pData;
#endif
			m_pData = pDest;
		}

		// move data out of container and into stack-allocated temporary heap
		void Unpack(sysMemAllocator &heap)
		{
			if (!m_pData)
				return;

			bgAssertf(IsPacked(), "bgPackableData::Unpack(): data is not packed!");
			void* pPacked = m_pData;
			int prevBucket = sysMemCurrentMemoryBucket;
			if (_Bucket >= 0)
				sysMemCurrentMemoryBucket = _Bucket;
			m_pData = rage_new T(*m_pData);
			m_Size = 0;
			sysMemCurrentMemoryBucket = prevBucket;
			heap.Free(pPacked);
		}

		T& GetData()
		{
			return *m_pData;
		}

		const T& GetData() const
		{
			return *m_pData;
		}

		u32 GetSize() const
		{
			return m_Size;
		}

		bool IsPacked() const
		{
			return m_Size > 0;
		}

		bool IsValid() const
		{
			return m_pData != NULL;
		}
	protected:
		u32 m_Size;
		T* m_pData;
	};

} // namespace rage

#endif // BREAKABLEGLASS_MEMUTILS_H 
