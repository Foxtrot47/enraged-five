//
// breakableglass/geometrybuilder.h
//
// Copyright (C) 2008 Rockstar Games.  All Rights Reserved. 
//

// Methods use to construct rendered geometry (owned by bgDrawable) from 
// broken glass pane data (owned by bgBreakable).

#ifndef BREAKABLEGLASS_GEOMETRYBUILDER_H
#define BREAKABLEGLASS_GEOMETRYBUILDER_H

#include "piecegeometry.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "vector/color32.h"
#include "vector/vector2.h"
#include "vector/vector3.h"

namespace rage {

class bgGeometryData;
class bgDrawable;
class bgGpuBuffers;
class grcFvf;


class bgGeometryBuilder
{
public:
	struct bgDrawablePoint
	{
		Vector2 m_surfacePos;	 // 2D position on the surface of the glass.
		// Adjusted a fair amount inward to show bevel when point is visible.
		Vector2 m_interiorPos;	 // 2D position in the middle of the glass.
		// Adjusted a very small amount inward to show cracks when point is visible.
		Vector2 m_surfaceDecal;  // 2D position on the surface of the glass.
		// Adjusted a fair amount inward to show bevel when point is visible.
		Vector2 m_interiorDecal; // 2D position in the middle of the glass.
		// Adjusted a very small amount inward to show cracks when point is visible.
		float	m_depth;		 // Depth
		//Bit field:
		//	Bit 1 =				0 = Crack is not visible
		//						1 = Crack is visible
		//	Bit 2 =				0 = decal is not visible
		//						1 = decal is visible
#define			BREAKABLE_CRACK_BIT		1
#define			BREAKABLE_DECAL_BIT		2
		int     m_vertexRed;	 // should be 255 if the point is visible; 0 otherwise
	};

	struct VertexBuffer
	{
		void *p;
		size_t stride;
		struct Data
		{
			size_t offset;
			size_t size;
		};
		Data position;
		Data normal;
		Data tangent;
		Data texture0;
		Data texture1;
		Data texture2;
		Data diffuse;
	};

#if !__SPU
	static bgGpuBuffers* CreatePane(const bgGeometryData& in_geometryData, u32 in_geometryDataSize, const grcFvf& in_Fvf, int arrPieceIndexCount[], int arrCrackIndexCount[]);

	//------------------------------------------------------------------------------
	// Get how many indices/vertices will be needed for a single piece
	//
	// input:	in_numVerts ==> number of vertices forming the perimeter of the piece
	//
	static void GetVertexIndexCountForOnePiece(int in_lod, int in_numVerts, int& io_numVertices, int&io_numPaneIndices, int& io_numIndices);

	//------------------------------------------------------------------------------
	//Get how many triangle will be needed when creating the remaining pieces on the window
	//
	static void GetVertexIndexCountForAllSolidPieces(const bgGeometryData& in_geometryData, int& out_numVertices, int& io_numPaneIndicees, int& out_numIndices, int arrPieceIndexCount[], int arrCrackIndexCount[]);

	void SetBuffers(const VertexBuffer& in_vb, u16* in_pIndices);
#endif // !__SPU

	//Constructor.  Initialize some boolean and other variables at default value, nothing more
	bgGeometryBuilder(const bgGeometryData& in_geometryData);

	// Accessors
	void* GetVertexPointer()
	{
		return m_vb.p;
	}

	u16* GetIndexPointer()
	{
		return m_pIndices;
	}

#if !__SPU
private:
#endif // !__SPU

	//-----------------------------------------------------------------------------
	// Render the pane at lod
	//
	void CreateAllGeometry();

	void CalculatePointLocations(	bgDrawablePoint& in_point,
									const Vector2& in_basePos,
									const Vector2& in_normal,
									float in_crackOffset,
									float in_bevelOffset,
									int hit	);

	// generate just the front (or back) faces for the pane pieces
	void CreatePanePiecesGeometry(bool in_bFlipFace, float fThickness);

	// generate just the front (or back) faces for the falling pieces
	void CreateFallingPiecesGeometry(bool bTwoSides);

	void CreateSingleFallingPieceGeometry(const bgFallingPiece& piece, const atArray<bgDrawablePoint>& piecePoints, const Vector3& normal, int iFallingPiece, bool bFrontFace, float fThickness);

	// add the edges of a piece of glass
	void AddCrackGeometry(
		const atArray<bgDrawablePoint>& piecePoints,
		const Vector3& centerMass,
		Color32 vertexColor);

	// Helpers for setting vertices in the vertex buffer
	void SetVertex(	const rage::Vector2& in_2dPoint,
					const rage::Vector3& in_offset,
					const rage::Vector3& in_normal,
					const rage::Vector2& in_2dDecal,
					const u32 in_deviceColor	);

	void SetVertex(	const rage::Vector3& in_position,
					const rage::Vector3& in_normal,
					const rage::Vector2& in_texCoord0,
					const rage::Vector2& in_texCoord1,
					const u32 in_deviceColor	);

	// This is unused and unimplemented, but it's declaration is required to make Win32PC happy.
	bgGeometryBuilder& operator = (const bgGeometryBuilder& rhs);

	// How much of the texture the bevels carve out
	static const float kBevelPercent;
	static const int kNumRand = 32; // must be power of 2
	static const int kRandMask = kNumRand-1;

	atRangeArray<float, kNumRand> m_randArray;
	atArray< atArray< bgDrawablePoint > > m_allPoints;

	Vector3 m_normal;

	const bgGeometryData& m_geometryData;

	u16* m_pIndices;
	VertexBuffer m_vb;

	int m_currentVertex;
	int m_currentIndex;

	int m_iRandom;
};

} // namespace rage

#endif // BREAKABLEGLASS_GEOMETRYBUILDER_H
