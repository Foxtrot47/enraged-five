//
// breakableglass/drawable.h
//
// Copyright (C) 2008 Rockstar Games.  All Rights Reserved. 
//

// Actual rendered geometry for a broken pane of glass (including falling pieces)
// Geometry is regenerated on every impact that causes more breakage; Falling pieces 
// are rendered using skinning matrices updated every frame during the 'DMZ'.
// Objects of this class are intended primarily for use by the rendering thread.
//
// (XENON ONLY): Index and vertex buffer memory is allocated from a fixed pool 
// reserved at ClassInit().

#ifndef BREAKABLEGLASS_DRAWABLE_H 
#define BREAKABLEGLASS_DRAWABLE_H 

#include "geometrydata.h"
#include "glassmanager.h"

#include "atl/ptr.h"
#include "atl/referencecounter.h"
#include "grcore/effect_typedefs.h"
#include "grcore/effect.h"
#include "grcore/fvf.h"
#include "grcore/matrix43.h"
#include "vector/vector2.h"
#include "vector/matrix34.h"

namespace rage {

class bgBreakable;
class bgGpuBuffers;
class grcIndexBuffer;
class grcVertexBuffer;
class grcFvf;
struct grcVertexDeclaration;



////////////////////////////////////////////////////////////////////////////////
// A class to manage the drawing of our broken pieces of glass (replaces
// the original exported drawable)
////////////////////////////////////////////////////////////////////////////////
class bgDrawable
{
public:
	//Constructor/destructor
	bgDrawable();
	~bgDrawable();

	//----------
	//FUNCTIONS

	//----------
	// MANAGER interface

#if RAGE_GLASS_USE_PRIVATE_HEAP
	// Steal physical memory for use by our private vertex/index buffer heap
	// in_maxBufferAllocations: maximum total number of index and vertex buffers
	//							we can simultaneously allocate
	// in_totalBufferMemory:    size of physical memory buffer (in kilobytes) to preallocate
	//                          for use by broken glass vertex and index buffers
	static void InitClass(int in_maxBufferAllocations, int in_totalBufferMemory);
#elif RAGE_GLASS_USE_USER_HEAP
	// Use the heap provided by the user for all vertex and index buffers
	//	bufferAllocator - the allocator to use
	static void InitClass(sysMemAllocator* bufferAllocator);
#else
	static void InitClass();
#endif

	// return stolen physical memory
	static void ShutdownClass();

	// Check whether all buffers have been freed.
	// ShutdownClass() should not be called unless this returns true.
	static bool AreAllBuffersFreed();

	//
	// Countdown and eventually release buffers in the countdown list
	//
	static void EndFrame();

	// Initialize with shader and vertex format information from the original model
	void Init();

	// Release resources
	void Term();

	grcInstanceData& GetInstanceData()
	{
		return m_EffectInstance;
	}

	// Check whether this drawable is actually ready to draw.
	bool IsReadyToDraw() const;

	// Indicate that this drawable has been rendered - this will make sure there's
	// a ref count on the buffer, and guarantees that they stay resident for the next
	// four frames.
	void MarkRenderedThisFrame();

	// Get a pointer to the object containing the index and vertex buffer.
	// Use this to bump the refcount before rendering so the GPU can still
	// access the buffers even after AllocateBuffers is called later.
	atReferenceCounter* GetBuffers() const;

	// Setup the information needed to draw the object. This should copy
	// any relevant data into this glass, since this object will end up
	// in the render thread and there's no guarantee that any pointers
	// from the main thread will be vaild when it goes to draw.
	void SetupDrawable(bgBreakable* in_pGlassBreakable);

	// Actually render this pane right now using immediate mode function calls
	void Draw(const float *transforms, int numTransforms, const Matrix34 &matrix, const Vector4 &crackTexMatrix, const Vector4 &crackTexOffset, bgGpuBuffers* pBuffers,
		const bgCrackStarMap* pCrackStarMap, int lod, int* arrPieceIndexCount, int* arrCrackIndexCount, u32 bucketMask) const;

	// Returns true after Init() until Term() is called.
	inline bool IsInUse() const
	{
		return m_bInUse;
	}

	//----------
	// Geometry builder interface

	// Get the vertex buffer
	grcVertexBuffer* GetVertexBuffer() const;

	// Get the index buffer
	grcIndexBuffer* GetIndexBuffer() const;

	// Get the dynamic memory used by this drawable 
	// (really just index and vertex buffer memory: shader, fvf, 
	// and vertex declaration are assumed to be shared)
	int GetDynamicMemoryUsage() const;

	// Lookup and cache effect vars
	void CacheEffectVars();

	void SetForceVSLighting(bool bForceVSLighting) { m_bForceVSLighting = bForceVSLighting; }

	static bgGpuBuffers*  AllocateBuffers(int in_VertexCount, int in_IndexCount, const grcFvf& in_fvf);

	static grcIndexBuffer* GetIndexBuffer(bgGpuBuffers* in_pBuffers);

	static grcVertexBuffer* GetVertexBuffer(bgGpuBuffers* in_pBuffers);

#if __BANK
	void DebugOverrideFloat2Var(int iFloat2VarIndex, Vector2& inOutValue) const;
	void DebugOverrideFloatVar(int iFloatVarIndex, float& inOutValue) const;
#endif

private:
	// clean up the list of buffers still potentially in use by the gpu.
	static void FlushCountdownList();

	// Call from main thread while rendering thread is idle
	// If this drawable was rendered last frame, refresh the countdown on buffers
	void CheckBuffersInUse(ASSERT_ONLY(bool IsGlassAlive));

	// Helper functions for drawing
	void DrawPanes(int iStartIndex, int in_numPaneIndices, bool bHighQuality, const float *pMatrices, int iNumMatrices, int iVarAddress) const;
	void DrawCracks(int in_numPaneIndices, int in_numCrackIndices, bool bHighQuality, const float *pMatrices, int iNumMatrices, int iVarAddress) const;

	// Function to set the pieces transformation matrices
	static void SetMatrices(const float *transforms, int numTransforms, int varAddress);

	// @NOTE: MUST BE 16-byte aligned for PS3!
	// Graphics instance data
	grcInstanceData m_EffectInstance;

	// Shader variable handles so we don't need to look them up every frame.
	grcEffectVar m_varCrackTexMatrix; // transform base tex coords -> crack tex coords (rotate and scale)
	grcEffectVar m_varCrackTexOffset; // transform base tex coords -> crack tex coords (offset)
	grcEffectGlobalVar m_varMatrices; // global transforms var for falling pieces
	atArray<grcEffectVar> m_textureVars;
	atArray<grcEffectVar> m_float4Vars;
	atArray<grcEffectVar> m_float2Vars;
	atArray<grcEffectVar> m_floatVars;	

	// smart pointer to index and vertex buffer.
	// when buffers are reallocated, the old ones 
	// can live on until the GPU is done with them
	atPtr<bgGpuBuffers> m_pBuffers;

	// Vertex data
	bool m_bHitValid;

	// Force using the low quality lighting
	bool m_bForceVSLighting;

	// true if this drawable is currently in use
	bool m_bInUse;

	mutable bool m_bDrawnLastFrame;

#if __ASSERT
	mutable u32 m_nLastDrawnFrame;
#endif // __ASSERT

	// Vertex data
	static grcVertexDeclaration* sm_pVertexDecl;

public:
#if __BANK
	static bool sm_bUseOverrideCrackBumpSettings;
	static bool sm_bDisableBumps;
	static Vec2V sm_vCrackEdgeBumpTileScale;
	static Vec2V sm_vCrackDecalBumpTileScale;
	static float sm_fCrackEdgeBumpAmount;
	static float sm_fCrackDecalBumpAmount;
	static float sm_fCrackDecalBumpAlphaThreshold;
#endif
} ;//class bgDrawable
// Added alignment for drawableSPU on PS3

} // namespace rage

#endif // BREAKABLEGLASS_DRAWABLE_H 
