<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<const name="::rage::bgLod::LOD_COUNT" value="4"/>
  
<structdef type="::rage::bgCrackStarMap">
	<string name="m_cracks" type="ConstString"/>
	<array name="m_LodPixelSizeArray" type="atRangeArray" size="::rage::bgLod::LOD_COUNT">
		<float noInit="true"/>
	</array>
	<array name="m_textureVars" type="atArray">
		<struct type="::rage::bgTextureVarData"/>
	</array>
	<array name="m_float4Vars" type="atArray">
		<struct type="::rage::bgFloat4VarData"/>
	</array>
  <array name="m_float2Vars" type="atArray">
    <struct type="::rage::bgFloat2VarData"/>
  </array>
  <array name="m_floatVars" type="atArray">
    <struct type="::rage::bgFloatVarData"/>
  </array>
</structdef>

<structdef type="::rage::bgTextureVarData">
	<string name="m_varName" type="ConstString"/>
	<string name="m_textureName" type="ConstString"/>
</structdef>

<structdef type="::rage::bgFloat4VarData">
	<string name="m_varName" type="ConstString"/>
	<Vector4 name="m_value" min="0.0f" max="1.0f"/>
</structdef>

<structdef type="::rage::bgFloat2VarData">
    <string name="m_varName" type="ConstString"/>
    <Vector2 name="m_value"/>
</structdef>

<structdef type="::rage::bgFloatVarData">
    <string name="m_varName" type="ConstString"/>
    <float name="m_value"/>
</structdef>

<structdef type="::rage::bgShaderVars">
	<array name="m_textureVarDefaults" type="atArray">
		<struct type="::rage::bgTextureVarData"/>
	</array>
	<array name="m_float4VarDefaults" type="atArray">
		<struct type="::rage::bgFloat4VarData"/>
	</array>
  <array name="m_float2VarDefaults" type="atArray">
    <struct type="::rage::bgFloat2VarData"/>
  </array>
  <array name="m_floatVarDefaults" type="atArray">
    <struct type="::rage::bgFloatVarData"/>
  </array>
	<string name="m_matrixGlobalVarName" type="ConstString" noInit="true"/>
	<int name="m_matrixCount"/>
</structdef>

<structdef type="::rage::bgGlassSize">
<string name="m_glassSize" type="ConstString" size="3"/>
</structdef>

<enumdef type="bgGlassPieceMaxNum">
<enumval name="Max_Num_Pieces_1x" value="1"/>
<enumval name="Max_Num_Pieces_2x" value="2"/>
<enumval name="Max_Num_Pieces_3x" value="3"/>
<enumval name="Max_Num_Pieces_4x" value="4"/>
<enumval name="Max_Num_Pieces_5x" value="5"/>
</enumdef>

<structdef type="::rage::bgGlassTypeConfig">
<string name="m_glassType" type="ConstString"/>
<enum name="m_maxNumPieces" type="bgGlassPieceMaxNum" init="1"/>
<array name="m_glassSizes" type="atArray">
	<struct type="::rage::bgGlassSize"/>	
</array>
</structdef>

<structdef type="::rage::bgConfig">
	<array name="m_crackTypes" type="atArray">
		<string type="pointer"/>
	</array>
	<array name="m_glassTypes" type="atArray">
		<struct type="::rage::bgGlassTypeConfig"/>
	</array>
	<struct name="m_shaderVars" type="::rage::bgShaderVars"/>
</structdef>

<enumdef type="bgDecalChannel">
<enumval name="red" value="0"/>
<enumval name="green" value="1"/>
<enumval name="blue" value="2"/>
<enumval name="alpha" value="3"/>
<enumval name="randomRedGreen" value="4"/>
<enumval name="randomRedGreenAlpha" value="5"/>
<enumval name="randomAll" value="6"/>
</enumdef>

<enumdef type="bgCrackRotationType">
<enumval name="none" value="0"/>
<enumval name="0_to_360" value="1"/>
<enumval name="stepOf90" value="2"/>
<enumval name="flip" value="3"/>
<enumval name="rectangle" value="4"/>
<enumval name="rectangleFlip" value="5"/>
</enumdef>

<enumdef type="bgCrackPlacementType">
<enumval name="glassImpactLocation"/>
<enumval name="glassCenter"/>
<enumval name="glassRandom"/>
</enumdef>

<enumdef type="bgCrackScalingType">
<enumval name="none" value="0"/>
<enumval name="automatic" value="1"/>
<enumval name="forceSquared" value="2"/>
<enumval name="overwrite" value="3"/>
<enumval name="overwriteSquared" value="4"/>
</enumdef>

<structdef type="::rage::bgCrackType">
	<array name="m_crackArray" type="atArray">
		<struct type="::rage::bgCrackStarMap"/>
	</array>
	<array name="m_textureVars" type="atArray">
		<struct type="::rage::bgTextureVarData"/>
	</array>
	<array name="m_float4Vars" type="atArray">
		<struct type="::rage::bgFloat4VarData"/>
	</array>
  <array name="m_float2Vars" type="atArray">
    <struct type="::rage::bgFloat2VarData"/>
  </array>
  <array name="m_floatVars" type="atArray">
    <struct type="::rage::bgFloatVarData"/>
  </array>
	<float name="m_initialImpactKillRadius1" noInit="true"/>
	<float name="m_initialImpactKillRadius2" noInit="true"/>
	<float name="m_subsequentImpactKillRadiusMin" noInit="true"/>
	<float name="m_subsequentImpactKillRadiusMax" noInit="true"/>
	<float name="m_decalRadiusImpactShowValueMin" init="0.0225f"/>
	<float name="m_decalRadiusImpactShowValueMax" init="0.0225f"/>
	<float name="m_crackRadiusImpactShowValueMin" init="0.0225f"/>
	<float name="m_crackRadiusImpactShowValueMax" init="0.0225f"/>
	<float name="m_breakPieceSizeMin" init="0.25f" min="0.0f" max="1.0f" step="0.05f"/>
	<float name="m_breakPieceSizeMax" init="0.25f" min="0.0f" max="1.0f" step="0.05f"/>
	<float name="m_crackInBetweenPiecesSizeMax"/>
	<float name="m_bevelSizeMax"/>
	<float name="m_minThickness" noInit="true"/>
	<float name="m_decalTextureScaleMin" noInit="true"/>
	<float name="m_decalTextureScaleMax" noInit="true"/>
	<enum name="m_decalTextureChannel" type="bgDecalChannel" noInit="true"/>
	<enum name="m_crackMapCenterLocation" type="bgCrackPlacementType" noInit="true"/>
	<enum name="m_crackMapRotationType" type="bgCrackRotationType" noInit="true"/>
	<enum name="m_crackMapScalingType" type="bgCrackScalingType" noInit="true"/>
	<float name="m_crackMapOverwriteScalingX" noInit="true"/>
	<float name="m_crackMapOverwriteScalingY" noInit="true"/>
	<bool name="m_enableSequentialHit" noInit="true"/>
	<float name="m_minSequentialHitPieceSize" noInit="true"/>
</structdef>

</ParserSchema>