// 
// breakableglass/bgchannel.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_BGCHANNEL_H 
#define BREAKABLEGLASS_BGCHANNEL_H 

#include "diag/channel.h"

namespace rage {
RAGE_DECLARE_CHANNEL(BreakableGlass)

#define bgAssertf(cond,fmt,...)			RAGE_ASSERTF(BreakableGlass,cond,fmt,##__VA_ARGS__)
#define bgVerifyf(cond,fmt,...)			RAGE_VERIFYF(BreakableGlass,cond,fmt,##__VA_ARGS__)
#define bgErrorf(fmt,...)				RAGE_ERRORF(BreakableGlass,fmt,##__VA_ARGS__)
#define bgWarningf(fmt,...)				RAGE_WARNINGF(BreakableGlass,fmt,##__VA_ARGS__)
#define bgDisplayf(fmt,...)				RAGE_DISPLAYF(BreakableGlass,fmt,##__VA_ARGS__)
#define bgDebugf1(fmt,...)				RAGE_DEBUGF1(BreakableGlass,fmt,##__VA_ARGS__)
#define bgDebugf2(fmt,...)				RAGE_DEBUGF2(BreakableGlass,fmt,##__VA_ARGS__)
} // namespace rage

#endif // BREAKABLEGLASS_BGCHANNEL_H 
