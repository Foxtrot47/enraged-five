#ifndef BREAKABLEGLASS_EDGE_H 
#define BREAKABLEGLASS_EDGE_H 

#include "bgarray.h"
#include "bgchannel.h"
#include "atl/array.h"
#include "atl/map.h"
#include "math/amath.h"

#define BG_DEBUG_PRINT 0
#if BG_DEBUG_PRINT
#define BG_DEBUG_PRINT_ONLY(...)  __VA_ARGS__
#else
#define BG_DEBUG_PRINT_ONLY(...)
#endif

namespace rage
{
	typedef u16 bgIndex;
	typedef bgArray<bgIndex> bgPolygon;

	class bgEdge
	{
	public:
		static bgIndex const kPointRef_None = bgIndex(-1);
		static bgIndex const kPolygonRef_None = bgIndex(-1);
		static bgIndex const kPieceId_None  = bgIndex(-1);
		static bgIndex const kPieceId_FrameTop    = bgIndex(-1) - 1;   // special piece id used with edges that border the frame.
		static bgIndex const kPieceId_FrameBottom = bgIndex(-1) - 2;   // special piece id used with edges that border the frame.
		static bgIndex const kPieceId_FrameLeft   = bgIndex(-1) - 3;   // special piece id used with edges that border the frame.
		static bgIndex const kPieceId_FrameRight  = bgIndex(-1) - 4;   // special piece id used with edges that border the frame.

		enum Side
		{
			left,
			right
		};

		bgEdge()
		{
			m_indices[0] = kPointRef_None;
			m_indices[1] = kPointRef_None;
			m_polygons[left] = kPolygonRef_None;
			m_polygons[right] = kPolygonRef_None;
		}

		bgEdge(bgIndex p0, bgIndex p1, bgIndex poly)
		{
			Assertf(p0 != p1, "bgEdge between a single point.");
			m_indices[0] = p0;
			m_indices[1] = p1;
			m_polygons[left] = poly;
			m_polygons[right] = kPolygonRef_None;
		}

		void Polygon(Side side, bgIndex polygon)
		{
			m_polygons[side] = polygon;
		}

		bgIndex Polygon(Side side) const
		{
			return m_polygons[ side ];
		}

		void Left(bgIndex polygon)
		{
			Polygon(left, polygon);
		}

		bgIndex Left() const
		{
			return Polygon(left);
		}

		void Right(bgIndex polygon)
		{
			Polygon(right, polygon);
		}

		bgIndex Right() const
		{
			return Polygon(right);
		}

		bgIndex &operator [](size_t index)
		{
			return m_indices[index];
		}

		bgIndex const &operator [](size_t index) const
		{
			return m_indices[index];
		}

	private:
		// endpoints
		bgIndex m_indices[2];
		// polygons
		bgIndex m_polygons[2];
	};

	class bgEdgeRef
	{
	public:
		bgEdgeRef() : m_edgeIndex()
		{
		}

		bgEdgeRef(bgIndex in_edgeIndex) : m_edgeIndex(in_edgeIndex)
		{
			CompileTimeAssert(sizeof(m_edgeIndex) >= sizeof(in_edgeIndex));
		}

		bgIndex GetEdgeIndex() const
		{
			return bgIndex(m_edgeIndex & (bgIndex(-1) >> int(sizeof(m_edgeIndex) == sizeof(bgIndex))));
		}

		void SetEdgeIndex(bgIndex index)
		{
			m_edgeIndex &= m_flip;
			m_edgeIndex |= index;
		}

		bgEdgeRef Flip() const
		{
			bgEdgeRef ref;
			ref.m_edgeIndex = m_edgeIndex ^ m_flip;
			return ref;
		}

		bool IsFlipped() const
		{
			return m_edgeIndex < 0;
		}

		template<class Array>
		bgIndex GetP0(const Array& in_edges) const
		{
			return in_edges[GetEdgeIndex()][IsFlipped()];
		}

		template<class Array>
		bgIndex GetP1(const Array& in_edges) const
		{
			return in_edges[GetEdgeIndex()][!IsFlipped()];
		}

		template<class Array>
		bgIndex GetLeft(const Array& in_edges) const
		{
			return GetLeftPoly(in_edges);
		}

		template<class Array>
		bgIndex GetRight(const Array& in_edges) const
		{
			return GetRightPoly(in_edges);
		}

		template<class Array>
		bgIndex GetLeftPoly(const Array& in_edges) const
		{
			return in_edges[GetEdgeIndex()].Polygon(bgEdge::Side(IsFlipped()));
		}

		template<class Array>
		bgIndex GetRightPoly(const Array& in_edges) const
		{
			return in_edges[GetEdgeIndex()].Polygon(bgEdge::Side(!IsFlipped()));
		}

		template<class Array>
		void SetP0(Array& in_edges, bgIndex p)
		{
			in_edges[GetEdgeIndex()][IsFlipped()] = p;
		}

		template<class Array>
		void SetP1(Array& in_edges, bgIndex p)
		{
			in_edges[GetEdgeIndex()][!IsFlipped()] = p;
		}

		template<class Array>
		void SetLeft(Array& in_edges, bgIndex p)
		{
			SetLeftPoly(in_edges, p);
		}

		template<class Array>
		void SetRight(Array& in_edges, bgIndex p)
		{
			SetRightPoly(in_edges, p);
		}

		template<class Array>
		void SetLeftPoly(Array& in_edges, bgIndex in_iPoly)
		{
			in_edges[GetEdgeIndex()].Polygon(bgEdge::Side(IsFlipped()), in_iPoly);
		}

		template<class Array>
		void SetRightPoly(Array& in_edges, bgIndex in_iPoly)
		{
			in_edges[GetEdgeIndex()].Polygon(bgEdge::Side(!IsFlipped()), in_iPoly);
		}

		friend bool operator ==(bgEdgeRef const &lhs, bgEdgeRef const &rhs)
		{
			return lhs.m_edgeIndex == rhs.m_edgeIndex;
		}

		friend bool operator !=(bgEdgeRef const &lhs, bgEdgeRef const &rhs)
		{
			return lhs.m_edgeIndex != rhs.m_edgeIndex;
		}

	private:
		static int const m_flip = int(~(unsigned(-1) >> 1));
		int m_edgeIndex;
	};

	class bgEdgeKey
	{
		friend struct bgEdgeHash;

	public:
		bgEdgeKey(bgIndex first, bgIndex second)
		{
			m_indices[0] = rage::Min(first, second);
			m_indices[1] = rage::Max(first, second);
		}

		bgIndex operator [](size_t index) const
		{
			return m_indices[index];
		}

		friend bool operator ==(bgEdgeKey const &lhs, bgEdgeKey const &rhs)
		{
			return lhs[0] == rhs[0] && lhs[1] == rhs[1];
		}

		friend bool operator !=(bgEdgeKey const &lhs, bgEdgeKey const &rhs)
		{
			return lhs[0] != rhs[0] || lhs[1] != rhs[1];
		}

	private:
		union
		{
			bgIndex m_indices[2];
			u32 m_key;
		};
	};

	struct bgEdgeHash
	{
		unsigned operator ()(bgEdgeKey const &key) const
		{
			return unsigned(key.m_key);
		}
	};

	typedef atMap<bgEdgeKey, bgEdgeRef, bgEdgeHash>	bgEdgeMap;

	template<class Mesh>
	class bgEdgeAdder
	{
	public:
		bgEdgeAdder(Mesh &mesh) : m_pMesh(&mesh)
		{
		}

		bgEdgeRef AddOrUpdateEdge(bgIndex in_p0, bgIndex in_p1, bgIndex in_polygon, const char* source = NULL);

		void AddEdgeRef(bgEdgeRef& in_edgeRef)
		{
			m_map[bgEdgeKey(m_pMesh->GetP0(in_edgeRef), m_pMesh->GetP1(in_edgeRef))] = in_edgeRef;
		}

		// Check if the given two points dont belong to an edge connected to pieces from both sides
		bool EdgeFree(bgIndex in_p0, bgIndex in_p1)
		{
			if (bgEdgeRef const *pTestEdgeRef = FindEdge(bgEdgeKey(in_p0, in_p1)))
			{
				return m_pMesh->GetLeftPoly(*pTestEdgeRef) == bgEdge::kPieceId_None || m_pMesh->GetRightPoly(*pTestEdgeRef) == bgEdge::kPieceId_None;
			}

			return true;
		}

	private:
		bgEdgeRef const *FindEdge(bgEdgeKey const &key)
		{
			return m_map.Access(key);
		}

		bgEdgeRef Add(bgEdge const &edge)
		{
			bgEdgeRef ref(bgEdgeRef(bgIndex(m_pMesh->GetEdgeCount())));
			m_pMesh->Add(edge);
			m_map[bgEdgeKey(edge[0], edge[1])] = ref;
			return ref;
		}

		bgEdgeMap m_map;
		Mesh *m_pMesh;
	};
}

template<class Mesh>
rage::bgEdgeRef rage::bgEdgeAdder<Mesh>::AddOrUpdateEdge(bgIndex in_p0, bgIndex in_p1, bgIndex in_polygon, const char* ASSERT_ONLY(in_source))
{
	if (bgEdgeRef const *pTestEdgeRef = FindEdge(bgEdgeKey(in_p0, in_p1)))
	{
		// On sequential hits the missing piece can actually be on the other side so the edge has to be flipped
		bgEdgeRef pEdgeRef;
		bool bEmptyEdge = m_pMesh->GetLeftPoly(*pTestEdgeRef) == bgEdge::kPieceId_None && m_pMesh->GetRightPoly(*pTestEdgeRef) == bgEdge::kPieceId_None;
		if(m_pMesh->GetRightPoly(*pTestEdgeRef) != bgEdge::kPieceId_None)
		{
			//Assertf(m_pMesh->GetLeftPoly(*pTestEdgeRef) == bgEdge::kPieceId_None, "Both sides of the edge attached???");
			pEdgeRef = pTestEdgeRef->Flip();
			Assertf(m_pMesh->GetRightPoly(pEdgeRef) == bgEdge::kPieceId_None, "Flip didn't work");
		}
		else
		{
			pEdgeRef = *pTestEdgeRef;
		}

		if(bEmptyEdge)
		{
			//Empty edges occur on subsequent shots when we kill off a pane piece and reset the
			//left poly for the edge (when the right poly is already empty)
#if BG_DEBUG_PRINT
			Displayf("Got Empty Edge - Poly In Edge: %d %d, Index In Edge: %d %d, using key %d %d", 
				m_pMesh->GetLeftPoly(pEdgeRef), 
				m_pMesh->GetRightPoly(pEdgeRef),
				m_pMesh->GetP0(pEdgeRef),
				m_pMesh->GetP1(pEdgeRef),
				in_p0, in_p1
				);
#endif
			//In this case we have to first check what order the edge is present in the table, 
			// and flip it if it was stored flipped in the first place. 
			if(m_pMesh->GetP0(pEdgeRef) != in_p0 || m_pMesh->GetP1(pEdgeRef) != in_p1)
			{
				pEdgeRef = pTestEdgeRef->Flip();
			}

			// Check that we are not trying to modify the edge by replacing a valid piece index
			bgAssertf(m_pMesh->GetP0(pEdgeRef) == in_p0 || m_pMesh->GetP1(pEdgeRef) == in_p1 ||
				m_pMesh->GetLeftPoly(pEdgeRef) == bgEdge::kPieceId_None ||
				m_pMesh->GetLeftPoly(pEdgeRef) == bgEdge::kPieceId_FrameTop ||
				m_pMesh->GetLeftPoly(pEdgeRef) == bgEdge::kPieceId_FrameBottom ||
				m_pMesh->GetLeftPoly(pEdgeRef) == bgEdge::kPieceId_FrameLeft ||
				m_pMesh->GetLeftPoly(pEdgeRef) == bgEdge::kPieceId_FrameRight,
				"%s: edge (%d, %d) already initialized to piece %d - %d (trying to set for piece %d)\n", 
				in_source ? in_source : "(no source)", in_p0, in_p1, m_pMesh->GetLeftPoly(pEdgeRef), m_pMesh->GetRightPoly(pEdgeRef), in_polygon);
			//Set Left Poly and return it without flipping
			m_pMesh->SetLeftPoly(pEdgeRef, in_polygon); // edge already exists, just update it
			return pEdgeRef;
		}
		else
		{
			// Check that we are not trying to modify the edge by replacing a valid piece index
			bgAssertf(m_pMesh->GetP0(pEdgeRef) != in_p0 || m_pMesh->GetP1(pEdgeRef) != in_p1 ||
				m_pMesh->GetRightPoly(pEdgeRef) == bgEdge::kPieceId_None ||
				m_pMesh->GetRightPoly(pEdgeRef) == bgEdge::kPieceId_FrameTop ||
				m_pMesh->GetRightPoly(pEdgeRef) == bgEdge::kPieceId_FrameBottom ||
				m_pMesh->GetRightPoly(pEdgeRef) == bgEdge::kPieceId_FrameLeft ||
				m_pMesh->GetRightPoly(pEdgeRef) == bgEdge::kPieceId_FrameRight,
				"%s: edge (%d, %d) already initialized to piece %d - %d (trying to set for piece %d)\n", 
				in_source ? in_source : "(no source)", in_p0, in_p1, m_pMesh->GetLeftPoly(pEdgeRef), m_pMesh->GetRightPoly(pEdgeRef), in_polygon);
			m_pMesh->SetRightPoly(pEdgeRef, in_polygon); // edge already exists, just update it
			return pEdgeRef.Flip(); // and return edge-flipping ref

		}
	}

	return Add(bgEdge(in_p0, in_p1, in_polygon));
}

#endif
