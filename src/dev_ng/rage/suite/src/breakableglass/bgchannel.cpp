// 
// breakableglass/bgchannel.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "bgchannel.h"
#include "optimisations.h"

#include "system/param.h"

// optimisations
BG_OPTIMISATIONS()

namespace rage {
	RAGE_DEFINE_CHANNEL(BreakableGlass)
} // namespace rage
