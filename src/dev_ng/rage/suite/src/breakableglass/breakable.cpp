//
// breakableglass/breakable.cpp
//
// Copyright (C) 2008 Rockstar Games.  All Rights Reserved. 
//

// Special types of plane glass (windows, tempered, bullet proof) that are breakable in different ways. 
//  When the glass pane is hit, the pane will break into smaller pieces, where some will fall and some 
//  other will stick on the pane.  
//
// Effect general explanation:
//	How to use the code:
//      0:      Call Init() specifying the glass type (see crackstemplate.h/cpp) and vertices and indices 
//              of the glass pane geometry.
//      
//		1:		When the user knows that a collision has occured with the pane (usually via collision 
//              detection with the model whose geometry was pssed into Init())... 
//				a) Convert the world-space impact location to object-space.
//              b) Call Convert3dPointTo2dPoint() to convert the object-space impact location to 2d 'pane' space
//		        c) Hit() to actually cause damage to the pane.
//
//      2:      Once per frame, call Update() from the sim thread to simulate physics, adjust lod, 
//              and regenerate gpu buffers as necessary.
//
// -------------------------------------------------------------------------------------------------

#include "breakable.h"

#include "bgchannel.h"
#include "bgdrawable.h"
#include "geometrydata.h"
#include "geometrybuilder.h"
#include "glassmanager.h"
#include "polygondicer.h"
#include "memutils.h"
#include "meshclipper.h"
#include "mesheditor.h"
#include "optimisations.h"

#include "bank/bank.h"
#include "diag/output.h"
#include "grcore/indexbuffer.h"
#include "grcore/VertexBufferEditor.h"
#include "grmodel/geometry.h"
#include "math/simplemath.h"
#include "physics/simulator.h"
#include "spatialdata/aabb.h"
#include "spatialdata/plane.h"
#include "system/container.h"
#include "grprofile/pix.h"
#include "system/simpleallocator.h"
#include "system/timer.h"
#include "vectormath/legacyconvert.h"
#include <cmath>

// For profile drawing:
#include "grprofile/drawmanager.h"
#include "vector/colors.h"
#include "grcore/debugdraw.h"

// optimisations
BG_OPTIMISATIONS()

namespace rage {

// Random number generator is common across all instances so each one reacts differently.
mthRandom bgBreakable::sm_Rand;

float sm_crackOffsetScale = 1.0f;
float sm_bevelOffsetScale = 1.0f;

// Set the distance fade values
float bgBreakable::sm_fDistFadeStart = 80.0f;
float bgBreakable::sm_fDistFadeRange = 160.0f;

bool bgBreakable::sm_bSilentHit = false;

// anonymous namespace for tuning parameters
namespace
{
	// Parameters used to compute/adjust lod
	Vec3V sm_viewPosition;
	int   sm_viewportWidth;
	float sm_viewportTanHFOV;

	bool sm_bKillBrokenPieces = 0;//__PS3;	 // if true, just kill broken pieces instead of breaking them up and/or making them fall
	bool sm_bCreateSmallerPieces = true; // break up shot pieces into smaller fragments
	//bool sm_bPieceFlyOutOnImpact = true; // if false, broken pieces will remain attached to the pane (ignored while sm_bCreateSmallerPieces is true)
	int  sm_LodOverride = -1;            // set to override glass lod with a particular lod
	int  sm_MaxDynamicLod = bgLod::LOD_HIGH; 
	float sm_SimulationTimeScale = 1.f;  // useful to slow down the glass fragment simulation for debugging
	float sm_LodHysteresisFudge = 0.1f;  // hysteresis fudge factor for LOD; 0= no hysteresis; 1= maximum hysteresis

	float sm_minShardLife(3.f);
	float sm_maxShardLife(7.f);
	float sm_fade(1.f);										// Life to start fading dying glass
	float sm_maxDepth(0.125f);								// Maximum depth to stretch reinforced and bulletproof glass
	float sm_paneMass(10.f);								// Mass of pane for falling reinforced glass
	float sm_velocityScale(0.05f);							// Velocity scale to control bend of falling reinforced glass
	float sm_maxBend(0.19634954084936207740391521145497f);	// Maximum bend of falling reinforced glass

	float sm_minShardMass(0.f);							// Minimum mass of a falling shard

	bool sm_alwaysUseSimplePhysics = false;				// Use simple physics regardless of LOD settings
	bool sm_neverUseSimplePhysics = false;				// Use RAGE physics regardless of LOD settings
	float sm_physicsImpulseRatio = 0.5f;				// Ratio to apply to impulse for RAGE physics shards
	Vector4 sm_physicsThreshold(0.005f, 0.01f, 0.015f, 0.03f);	// Shard area threshold below which to use simple physics
	Vector4 sm_cullThreshold(0.000075f, 0.001f, 0.0015f, 0.0003f);	// Shard area threshold below which to cull the shard

	sysMemSimpleAllocator* sm_PrivateAllocator;
	void* sm_RawHeap;

#if __BANK
	bool sm_bEnableProfileDraw = false;

	// moved these out of anonymous namespace
	//float sm_crackOffsetScale = 1.0f;
	//float sm_bevelOffsetScale = 1.0f;

	bool sm_bOverrideGlassType = false;
	int sm_GlassType = 0;

	bool sm_bOverrideCrackRotation = false; // set true to use sm_CrackRotation instead of generating a random rotation
	float sm_CrackRotation = 0.f;           // when sm_bOverrideCrackRotation is specified, the angle to use (0-1) => (0-2PI)

	bool sm_bOverrideCrackVariation = false;
	int sm_CrackVariation = 0;
	
	bool sm_bDisableCrackScaling = false;
	bool sm_bDisableCrackClipping = false;
	bool sm_bSkipHit = false;
	
	// Helpers to visualize the hit radius values
	bool sm_bShowHitKillRadius = false;
	bool sm_bShowHitDecalRadius = false;
	bool sm_bShowHitCrackRadius = false;
	Vec3V sm_vLastHitPos;
	float sm_fLastKillRadius = 0.0f;
	float sm_fLastDecalRadius = 0.0f;
	float sm_fLastCrackRadius = 0.0f;

	// Show/hide the falling pieces squered distance from the pane
	bool sm_bShowPiecesDist = false;

	// Override LOD selection for tuning
	bool sm_bOverrideLOD = false;

	// Used for showing the LODs per each broken pane
	bool sm_bShowLOD = false;

	// Used for showing batch information for the falling pieces
	bool sm_bShowBatchInfo = false;

	// Show the size of a falling piece used for the split into smaller pieces logic
	bool sm_bShowPieceSize = false;

	// Marks falling pieces based on the physics they are using
	bool sm_bShowPhysicsType = false;

	// Show the size and silhoutte of the pieces and polygons
	bool sm_bShowSequentialHit = false;

	// Show the anchoring of the pieces
	bool sm_bShowAnchoring = false;

	// Use the initial kill radius for pieces large enough to recive sequential hits
	bool ms_bUseInitialKillRadiusSeqPiece = true;

	//Tell us if we should debug the crack map scaling/rotation/placement on the pane of glass by
	// not clipping the crack map on the pane of glass anymore
	bool sm_crackMapClipOnPane = true;

	// Enable skip from low to very low LOD in when directional light/shadow is off.
	bool sm_bEnableLODSkip = true;

	// Override the decal mask selection
	int sm_decalMaskSelection = -1;

	// Disable template usage on sequential hits instead of just dropping pieces
	bool sm_bDisableSequentialHit = false;

	// Disable glass thickness
	bool sm_bNoThickness = false;
#endif

	// This controls the maximum amount of batches the glass pieces will use
	// Each batch has SKINNING_COUNT bones
	bank_s32 sm_iTotalBatches = 5;

	//-----------------------------------------------------------------------------
	// Utility used to test for point inside polygon:
	// If the specified edge instersects the line segment (p_test) - (p_test.x, -inf), negate 'intersect'.
	//
	// Usage: initialize 'intersect' to false and call this function for every edge of a polygon.  
	// If, at the end, intersect is true, p_test is inside the polygon.
	void CheckEdgeIntersection(const Vector2& p0, const Vector2& p1, const Vector2& p_test, bool& intersect)
	{
		if ((((p1.y <=p_test.y) && (p_test.y<p0.y)) || 
			((p0.y<=p_test.y) && (p_test.y<p1.y))) &&
			(p_test.x < (p0.x - p1.x) * (p_test.y - p1.y) / 
			(p0.y - p1.y) + p1.x))
		{
			intersect = !intersect;
		}
	}

	//-----------------------------------------------------------------------------
	//
	// Compute the distance squared from in_point to the specified edge (in_p0->in_p1)
	//
	float PointToEdgeDist2(const Vector2& in_p0, const Vector2& in_p1, const Vector2& in_point)
	{
		// @TODO: could use geomSpheres::TestSphereToCapsule() instead, which is all new vector lib-style.
		// but may not be faster than old-school for 2d points?

		// algorithm: compute distance along the edge segment of the perpendicular projection of in_point
		Vector2 pointToP0 = in_point - in_p0;
		Vector2 segment = in_p1 - in_p0;
		float dotProd = segment.Dot(pointToP0);
		if (dotProd < 0.f)
			// projection is before p0, so just return distance to p0
			return pointToP0.Mag2();

		float segmentLength2 = segment.Mag2();
		if (dotProd > segmentLength2)
			// projection is beyond p1, so just return distance to p1
			return in_point.Dist2(in_p1);

		// it's in between, so compute the actual projected point
		Vector2 p;
		p.AddScaled(in_p0, segment, dotProd / segmentLength2);

		// and return the distance to it
		return p.Dist2(in_point);
	}

	//------------------------------------------------------------------------------
	//
	// Compute the bounds of an array of 2d points
	//
	void FindBounds(const Vector2* in_points, int in_pointCount, float& left, float& top, float& right, float& bottom)
	{
		//-------
		//First we find the center of mass as a value between 0 and 1 because all our point are in-between 0 and 1
		//
		//Find the bounding box of the point
		left = in_points[0].x;
		right = in_points[0].x;
		top = in_points[0].y;
		bottom = in_points[0].y;
		//Iterate through all the points
		for (int i = 1;i < in_pointCount;i++)
		{
			//We just found a point that is more on the left
			if (in_points[i].x < left)
				left = in_points[i].x;

			//We just found a point that is more on the right
			if (in_points[i].x > right)
				right = in_points[i].x;

			//We just found a point that is more on the top
			if (in_points[i].y < top)
				top = in_points[i].y;

			//We just found a point that is more on the top
			if (in_points[i].y > bottom)
				bottom = in_points[i].y;
		}
	}
}

bool bgBreakable::m_bSafeToUseVSLight = false;

void bgBreakable::GetPrivateHeapStats(size_t& used, size_t& available, size_t& largest)
{
	if (bgVerifyf(sm_PrivateAllocator, "GetPrivateHeapStats(): private heap not allocated!"))
	{
		used = sm_PrivateAllocator->GetMemoryUsed(0);
		available = sm_PrivateAllocator->GetMemoryAvailable();
		largest = sm_PrivateAllocator->GetLargestAvailableBlock();
	}
}

sysMemAllocator& bgBreakable::GetPrivateHeap()
{
	TrapZ((size_t)sm_PrivateAllocator);
	return *sm_PrivateAllocator;
}

void bgBreakable::BreakableData::Init()
{
	// Start out packed
	m_physicsData.InitPacked(*sm_PrivateAllocator, &bgGlassManager::RecycleGlass);
	m_geometryData.InitPacked(*sm_PrivateAllocator, &bgGlassManager::RecycleGlass);
}

void bgBreakable::BreakableData::Shutdown()
{
	m_physicsData.Shutdown(*sm_PrivateAllocator);
	m_geometryData.Shutdown(*sm_PrivateAllocator);
}

void bgBreakable::BreakableData::Pack()
{
	m_physicsData.Pack(*sm_PrivateAllocator, &bgGlassManager::RecycleGlass);
	m_geometryData.Pack(*sm_PrivateAllocator, &bgGlassManager::RecycleGlass);
}

void bgBreakable::BreakableData::Unpack()
{
	m_physicsData.Unpack(*sm_PrivateAllocator);
	m_geometryData.Unpack(*sm_PrivateAllocator);
}

void bgBreakable::InitClass(int in_heapSizeKb)
{
	int heapSize = in_heapSizeKb * 1024;
	// intentionally bogus heap id
	static const int bgHeapId = -1;
	 // don't use smallocator; we won't generally create small allocations and can't afford to lose 16k pages for them
	static const bool bgUseSmallocator = false;
	sm_RawHeap = rage_new char[heapSize];
	sm_PrivateAllocator = rage_new sysMemSimpleAllocator(sm_RawHeap, heapSize, bgHeapId, bgUseSmallocator);
#if RAGE_TRACKING
	sm_PrivateAllocator->SetTallied(false);
#endif
}

void bgBreakable::ShutdownClass()
{
	delete sm_PrivateAllocator;
	sm_PrivateAllocator = NULL;
	delete[] (char*) sm_RawHeap;
	sm_RawHeap = NULL;
}


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
bgBreakable::bgBreakable() :
	m_glassType(-1),
	m_fallenRatio(),
	m_crackedRatio(),
	m_lastFallenRatio(),
	m_lastCrackedRatio(),
	m_BrokenFrameFlags(0),
	m_bHasUpdatedBuffers(false),
	m_bHasAssignedBuffers(false),
	m_bUpdateBuffers(false),
	m_boxMin(-1.0f, -1.0f, -1.0f),
	m_boxMax(1.0f, 1.0f, 1.0f)
#ifdef GLASS_PROFILING
	,m_profileHitTotal(0.0f),
	m_profileHitClampTotal(0.0f),
	m_profileHitTriangleDecompositionTotal(0.0f),
	m_profileHitPieceDisplacement(0.0f),
	m_profileBreakPieces(0.0f),
	m_profileBreakPieces1(0.0f),
	m_profileBreakPieces2(0.0f),
	m_profileBreakPieces3(0.0f),
	m_profileBreakPieces4(0.0f),
	m_profileBreakPieces5(0.0f),
	m_profileBreakPieces6(0.0f),
	m_profileBreakPieces7(0.0f),
	m_profileBreakPieces8(0.0f),
	m_profileBreakPieces9(0.0f),
	m_profileBreakPieces10(0.0f),
	m_profileBreakPieces11(0.0f),
	m_profileBreakPieces12(0.0f),
	m_profileBreakPieces13(0.0f),
	m_profileVisibility(0.0f),
	m_profileCopy(0.0f),
#endif
{
}//Constructor

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
#if MESH_LIBRARY
void bgBreakable::InitNormal(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, PhysicsLifeModifier modifier, int boneIndex)
{
	m_mass = sm_paneMass;
	m_strength = STRENGTH_NORMAL;
	m_physicsLifeModifier = modifier;
	m_fullhealth = 0;
	InitInternal(in_glassType, in_pVertices, in_pIndices, boneIndex);
}

void bgBreakable::InitReinforced(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int health, PhysicsLifeModifier modifier, int boneIndex)
{
	m_mass = sm_paneMass;
	m_strength = STRENGTH_REINFORCED;
	m_physicsLifeModifier = modifier;
	m_fullhealth = health;
	InitInternal(in_glassType, in_pVertices, in_pIndices, boneIndex);
}

void bgBreakable::InitBulletproof(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int boneIndex)
{
	m_mass = sm_paneMass;
	m_strength = STRENGTH_BULLETPROOF;
	m_physicsLifeModifier = PhysicsLifeModifier();
	m_fullhealth = 0;
	InitInternal(in_glassType, in_pVertices, in_pIndices, boneIndex);
}
#endif

void bgBreakable::InitFromModelInfo(const bgPaneModelInfoBase& in_modelInfo, Mat34V_In in_matrix, float in_mass)
{
	m_mass = in_mass;
	m_strength = STRENGTH_NORMAL;
	m_physicsLifeModifier = PhysicsLifeModifier();
	m_fullhealth = 0;
	Assert(RCC_MATRIX34(in_matrix).IsOrthonormal());
	SetTransform(RCC_MATRIX34(in_matrix));
	InitInternalFromModelInfo(in_modelInfo);
}

#if MESH_LIBRARY
void bgBreakable::InitInternal(int in_glassType, grcVertexBuffer* in_pVertices, grcIndexBuffer* in_pIndices, int boneIndex)
{
	Assert(in_pVertices);
	Assert(in_pIndices);
	bgVertexBufferMeshEditor meshEd(in_pVertices, in_pIndices);
	bgPaneModelInfoBase modelInfo;
	modelInfo.m_fvf = *(in_pVertices->GetFvf());
	modelInfo.ComputeModelInfo(meshEd, boneIndex);

	modelInfo.m_glassType = (u8)in_glassType;

	//Read the model data and get data we need from it
	InitInternalFromModelInfo(modelInfo);

}//InitInternal
#endif


//----------------------------------------
// Reset the class: All glass is back on the 
//  pane, perfectly healthy
//
void bgBreakable::Reset()
{
	//Reset all array
	m_FallingPieceMatrix.Resize(0);

	// clear out breakable data
	m_BreakableData.Shutdown();

	// clear solid pieces array
	m_pReinforced = 0;

	m_health = m_fullhealth;
	m_fallen = 0;
	m_fallenRatio = 0.f;
	m_crackedRatio = 0.f;
	m_lastFallenRatio = 0.f;
	m_lastCrackedRatio = 0.f;

	m_bUpdateBuffers = false;

	// update drawable as necessary (get rid of old buffers)
	m_pBuffers = NULL;
	m_bHasUpdatedBuffers = true;
	m_bHasAssignedBuffers = false;
}//Reset

//------------------------------------------------------------------------------
// Clear any pointers we may have as the GlassNode we used to belong to is gone.
//------------------------------------------------------------------------------
void bgBreakable::Shutdown()
{
	// early-out if Term() already called
	if (m_glassType == -1)
		return;

	m_BreakableData.Shutdown();
	m_glassType = -1;
	m_FallingPieceMatrix.Reset();
	m_pReinforced = 0;
	m_physicsLifeModifier = PhysicsLifeModifier();
	m_fallenRatio = 0.f;
	m_crackedRatio = 0.f;
	m_lastFallenRatio = 0.f;
	m_lastCrackedRatio = 0.f;
	// don't need to regenerate buffers anymore
	m_bUpdateBuffers = false;
	m_bHasUpdatedBuffers = true;
	m_bHasAssignedBuffers = false;
	m_pBuffers = NULL;
}//Term


#if __BANK
void OverrideLOD()
{
	if(sm_bOverrideLOD)
	{
		for(int i=bgLod::LOD_VLOW; i <= bgLod::LOD_HIGH; i++)
		{
			bgCrackStarMap::m_TuneLodPixelSizeArray[i] = bgCrackStarMap::m_DefLodPixelSizeArray[i];
		}
	}
}

void ClampLOD()
{
	static const float fPixelSizeDelta = 0.01f;
	if(sm_bOverrideLOD)
	{
		if(bgCrackStarMap::m_TuneLodPixelSizeArray[0] < fPixelSizeDelta)
		{
			bgCrackStarMap::m_TuneLodPixelSizeArray[0] = fPixelSizeDelta;
		}

		for(int i=bgLod::LOD_VLOW; i < bgLod::LOD_HIGH; i++)
		{
			if(bgCrackStarMap::m_TuneLodPixelSizeArray[i] >= bgCrackStarMap::m_TuneLodPixelSizeArray[i+1])
			{
				bgCrackStarMap::m_TuneLodPixelSizeArray[i+1] = bgCrackStarMap::m_TuneLodPixelSizeArray[i] + fPixelSizeDelta;
			}
		}
	}
}

//------------------------------------------------------------------------------
// Add debugging widgets
//------------------------------------------------------------------------------
void bgBreakable::AddWidgets(bkBank& bank)
{
	bank.AddToggle("Enable Profile Draw", &sm_bEnableProfileDraw);
	bank.AddToggle("Show batch info", &sm_bShowBatchInfo);
	bank.AddSlider("Total bone batches (SKINNING_COUNT each)", &sm_iTotalBatches, 1, sm_iMaxBatches, 1);
	bank.AddSlider("Crack offset scale", &sm_crackOffsetScale, 0.0f, 64.0f, 1.0f/32.0f);
	bank.AddSlider("Bevel offset scale", &sm_bevelOffsetScale, 0.0f, 64.0f, 1.0f/32.0f);

	bank.AddToggle("Override glass type", &sm_bOverrideGlassType);
	bank.AddSlider("Glass type override value", &sm_GlassType, 0, 20, 1);
	bank.AddToggle("Override crack variation", &sm_bOverrideCrackVariation);
	bank.AddSlider("Crack variation override value", &sm_CrackVariation, 0, 20, 1);
	bank.AddToggle("Override crack rotation", &sm_bOverrideCrackRotation);
	bank.AddSlider("Crack rotation override value", &sm_CrackRotation, 0.f, 1.f, 0.001f);
	bank.AddSlider("Decal mask override value", &sm_decalMaskSelection, -1, 4, 1);

	bank.AddToggle("Disable crack Scaling",	&sm_bDisableCrackScaling);
	bank.AddToggle("Disable crack clipping", &sm_bDisableCrackClipping);
	bank.AddToggle("Disable hit", &sm_bSkipHit);
	bank.AddToggle("Disable thickness", &sm_bNoThickness);

	bank.PushGroup("Hit Radius Visualization");
	bank.AddToggle("Show last hit kill radius (red)", &sm_bShowHitKillRadius);
	bank.AddToggle("Show last hit decal radius (blue)", &sm_bShowHitDecalRadius);
	bank.AddToggle("Show last hit crack radius (gray)", &sm_bShowHitCrackRadius);
	bank.PopGroup();

	bank.PushGroup("Sequential Hit");
	bank.AddToggle("Disable sequential hit", &sm_bDisableSequentialHit);
	bank.AddToggle("Show piece size and silhouette", &sm_bShowSequentialHit);
	bank.AddToggle("Show piece anchoring", &sm_bShowAnchoring);
	bank.AddToggle("Use initial kill radius for large pieces", &ms_bUseInitialKillRadiusSeqPiece);
	bank.PopGroup();

	bank.PushGroup("LOD");
	bank.AddToggle("Show LOD", &sm_bShowLOD);
	bank.AddSlider("LOD hysteresis fudge factor", &sm_LodHysteresisFudge, 0.f, 1.f, 0.001f);
	bank.AddSlider("LOD override", &sm_LodOverride, -1, bgLod::LOD_HIGH, 1);
	bank.AddSlider("Maximum dynamic lod", &sm_MaxDynamicLod, bgLod::LOD_VLOW, bgLod::LOD_HIGH, 1);
	bank.AddToggle("Enable LOD Skip", &sm_bEnableLODSkip);
	bank.AddToggle("Override LOD pixel size", &sm_bOverrideLOD, OverrideLOD);
	bank.AddSlider("Pixel size - Very Low", &bgCrackStarMap::m_TuneLodPixelSizeArray[bgLod::LOD_VLOW], 0.001f, 1000.0f, 1.0f, ClampLOD);
	bank.AddSlider("Pixel size - Low", &bgCrackStarMap::m_TuneLodPixelSizeArray[bgLod::LOD_LOW], 0.001f, 1000.0f, 1.0f, ClampLOD);
	bank.AddSlider("Pixel size - Med", &bgCrackStarMap::m_TuneLodPixelSizeArray[bgLod::LOD_MED], 0.001f, 1000.0f, 1.0f, ClampLOD);
	bank.AddSlider("Pixel size - High", &bgCrackStarMap::m_TuneLodPixelSizeArray[bgLod::LOD_HIGH], 0.001f, 1000.0f, 1.0f, ClampLOD);
	bank.PopGroup();

	bank.PushGroup("Crack Bump Detail");
	bank.AddToggle("Disable Bumps", &bgDrawable::sm_bDisableBumps);
	bank.AddToggle("Override Crack Bump Settings", &bgDrawable::sm_bUseOverrideCrackBumpSettings);
	bank.AddVector("Edge Bump Tile Scale", &bgDrawable::sm_vCrackEdgeBumpTileScale, 0.0f, 100.0f, 0.01f);
	bank.AddSlider("Edge Bump Amount", &bgDrawable::sm_fCrackEdgeBumpAmount, 0.0f, 100.0f, 0.01f);

	bank.AddVector("Decal Bump Tile Scale", &bgDrawable::sm_vCrackDecalBumpTileScale, 0.0f, 100.0f, 0.01f);
	bank.AddSlider("Decal Bump Amount", &bgDrawable::sm_fCrackDecalBumpAmount, 0.0f, 100.0f, 0.01f);
	bank.AddSlider("Decal Bump Alpha Threshold", &bgDrawable::sm_fCrackDecalBumpAlphaThreshold, 0.0f, 1.0f, 0.001f);
	bank.PopGroup();

	bank.PushGroup("Distance Fade");
	bank.AddToggle("Show falling pieces square distance", &sm_bShowPiecesDist);
	bank.AddSlider("Distance fade start square distance", &sm_fDistFadeStart, 1.0f, 2500.0f, 1.0f);
	bank.AddSlider("Distance fade range", &sm_fDistFadeRange, 1.0f, 250.0f, 1.0f);
	bank.PopGroup();
	
	bank.PushGroup("Simulation");
	bank.AddSlider("Simulation time scale", &sm_SimulationTimeScale, 0.f, 2.f, 0.0001f);
	bank.AddToggle("Just kill broken pieces instead of making them fall", &sm_bKillBrokenPieces);
	bank.AddToggle("Create smaller pieces on impact", &sm_bCreateSmallerPieces);
	bank.AddToggle("Show piece size", &sm_bShowPieceSize);
	bank.AddToggle("Show piece physics type", &sm_bShowPhysicsType);
	//bank.AddToggle("Make smashed pieces actually fly off the pane", &sm_bPieceFlyOutOnImpact);

	bank.AddSlider("min shard lifetime", &sm_minShardLife, 0.f, 100.f, 0.01f);
	bank.AddSlider("max shard lifetime", &sm_maxShardLife, 0.f, 100.f, 0.01f);
	bank.AddSlider("shard fade time", &sm_fade, 0.f, 10.f, 0.01f);
	bank.AddSlider("Maximum pane depth", &sm_maxDepth, 0.f, 1.f, 0.001f);
	bank.AddSlider("Pane mass", &sm_paneMass, 0.f, 1000.f, 0.01f);
	bank.AddSlider("Falling pane velocity scale", &sm_velocityScale, 0.f, 2.f, 0.001f);
	bank.AddSlider("Maximum pane bend", &sm_maxBend, 0.f, 2.f, 0.001f);

	bank.AddToggle("Always use simple physics", &sm_alwaysUseSimplePhysics);
	bank.AddToggle("Never use simple physics", &sm_neverUseSimplePhysics);
	bank.AddSlider("Physics impulse ratio", &sm_physicsImpulseRatio, 0.f, 1.f, 0.001f);
	bank.AddVector("Simple physics shard area threshold (LOD High-Low)", &sm_physicsThreshold, 0.f, 1.f, 0.0001f);
	bank.AddVector("Cull shard area threshold (LOD High-Low)", &sm_cullThreshold, 0.0000001f, 0.01f, 0.00001f);

	bank.AddSlider("Minimum shard mass (kg)", &sm_minShardMass, 0.f, 2500.f, 0.001f);

	//Add a toggle to debug the glass scaling/rotation/placement:  If this is tagged, the crack map will not be clip on the pane of glass anymore.  This
	// will result in seeing the full crack map in game. 
	bank.AddToggle("DEBUG: Clip crack map on pane of glass", &sm_crackMapClipOnPane);

	bgSimplePhysics::AddWidgets(bank);
	bank.PopGroup();
}

#endif // __BANK

int bgBreakable::GetFallingPieceCount() const
{
	return m_FallingPieceMatrix.GetCount()-bgMatrixArray::FIRST_FALLING_PIECE;
}

//------------------------------------------------------------------------------
// Get the current hit data if any
//------------------------------------------------------------------------------
const bgHitInfo* bgBreakable::GetCurrentHit() const
{
	const atArray<bgHitInfo>& hits = GetGeometryData().m_hitInfo;
	return hits.empty() ? NULL : &hits.back();
}

const bgHitInfo* bgBreakable::GetHit(int index) const
{
	const atArray<bgHitInfo>& hits = GetGeometryData().m_hitInfo;
	return 0 <= index && index < hits.size() ? &hits[index] : NULL;
}

int bgBreakable::GetIndex(const bgHitInfo& hit) const
{
	const atArray<bgHitInfo>& hits = GetGeometryData().m_hitInfo;
	if (hits.empty())
		return -1;
	bgHitInfo const *pBegin(&hits[0]), *pEnd(pBegin + hits.size()), *pHit(&hit);
	return (int)(pBegin <= pHit && pHit < pEnd ? pHit - pBegin : -1);
}

unsigned bgBreakable::ChoosePaneMatrix(Vector2 const &location) const
{
	if (GetStrength() != bgBreakable::STRENGTH_REINFORCED)
		return 0;
/*
	float x(location.x - 0.5f);
	float y(0.5f - location.y);
	unsigned quadrant;
	if (y >= 0.f)
	{
		if (x >= 0.f)
			quadrant = 0;
		else
			quadrant = 1;
	}
	else
	{
		if (x >= 0.f)
			quadrant = 3;
		else
			quadrant = 2;
	}
	float sincos[][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
	float x1(x * sincos[quadrant][1] + y * sincos[quadrant][0]);
	float y1(x * -sincos[quadrant][0] + y * sincos[quadrant][1]);
	if (x1 >= 2 * y1)
		return quadrant * 2;
	if(x1 >= 0.25f * y1)
		return (quadrant * 2) + 1;
	return ((quadrant + 1) * 2) % 8;
*/
	bool b(location.y > 0.5f), c(location.x < 0.5f);
	unsigned quadrant((unsigned(b) << 1) | unsigned(b ^ c));
	float x(location.x - 0.5f);
	float y(0.5f - location.y);
	float sincos[][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
	float x1(x * sincos[quadrant][1] + y * sincos[quadrant][0]);
	float y1(x * -sincos[quadrant][0] + y * sincos[quadrant][1]);
	b = y1 > 2 * x1;
	c = y1 > 0.25f * x1;
	return (((quadrant + b) << 1) + (b ^ c)) & 7;
}

void CreateFrameBox(const Vector3& v0, const Vector3& v1, const Vector3& normal, float width, atRangeArray<spdPlane, 6>& planes)
{
	Vector3 delta = v1 - v0;
	delta.Normalize();
	planes[0].Set(v0, -delta);
	planes[1].Set(v1, delta);
	planes[2].Set(v0 + normal * width, normal);
	planes[3].Set(v0 - normal * width, -normal);
	Vector3 cross;
	cross.Cross(normal, delta);
	planes[4].Set(v0 + width * cross, cross);
	planes[5].Set(v0 - width * cross, -cross);
}

float ComputePolygonArea(const atArray<Vector3>& poly)
{
	int vertexCount = poly.GetCount();
	float twiceArea = 0.f;
	for (int i0=0,i1=1,i2=2; i2 < vertexCount; i1++,i2++)
	{
		Vector3 p0 = poly[i0];
		Vector3 p1 = poly[i1];
		Vector3 p2 = poly[i2];
		Vector3 p10 = p1 - p0;
		Vector3 p20 = p2 - p0;
		Vector3 cross;
		cross.Cross(p20, p10);
		twiceArea += cross.Mag();
	}
	return 0.5f * twiceArea;
}

bgPaneModelInfoBase::bgPaneModelInfoBase() : 
	 m_flags(0), m_thickness(0.01f)
{
	// pick some default values that shouldn't get us in too much trouble if real initialization fails for some reason
	m_posBase.x = m_posBase.y = m_posBase.z = 0.f;
	m_posWidth.x = 1.f;
	m_posWidth.y = 0.f;
	m_posWidth.z = 0.f;
	m_posHeight.x = 0.f;
	m_posHeight.y = 1.f;
	m_posHeight.z = 0.f;
	m_uvMin.x = m_uvMin.y = 0.f;
	m_uvMax.x = m_uvMax.y = 1.f;
	m_boundsOffsetBack = m_boundsOffsetFront = 0.5f*m_thickness;
}

bgPaneModelInfoBase::bgPaneModelInfoBase(const bgPaneModelInfoBase& in_cloneMe) :
	 m_posBase(in_cloneMe.m_posBase)
	,m_posWidth(in_cloneMe.m_posWidth)
	,m_posHeight(in_cloneMe.m_posHeight)
	,m_uvMin(in_cloneMe.m_uvMin)
	,m_uvMax(in_cloneMe.m_uvMax)
	,m_fvf(in_cloneMe.m_fvf)
	,m_thickness(in_cloneMe.m_thickness)
	,m_flags(in_cloneMe.m_flags)
	,m_glassType(in_cloneMe.m_glassType)
	,m_shaderIndex(in_cloneMe.m_shaderIndex)
	,m_boundsOffsetBack(in_cloneMe.m_boundsOffsetBack)
	,m_boundsOffsetFront(in_cloneMe.m_boundsOffsetFront)
	,m_tangent(in_cloneMe.m_tangent)
{
}

bgPaneModelInfoBase::bgPaneModelInfoBase(datResource& in_rsc) : m_fvf(in_rsc)
{
}

IMPLEMENT_PLACE(bgPaneModelInfoBase);

#if __DECLARESTRUCT
void bgPaneModelInfoBase::DeclareStruct(datTypeStruct &s)
{
	SSTRUCT_BEGIN(bgPaneModelInfoBase)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_posBase)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_posWidth)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_posHeight)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_uvMin)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_uvMax)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_fvf)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_thickness)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_flags)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_glassType)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_shaderIndex)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_boundsOffsetFront)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_boundsOffsetBack)
		SSTRUCT_FIELD(bgPaneModelInfoBase, m_tangent)
	SSTRUCT_END(bgPaneModelInfoBase)
}
#endif

#if MESH_LIBRARY
u8 bgPaneModelInfoBase::FindFrameOverlap(mshMaterial* in_pMaterial, int in_boneIndex, Mat34V_In in_paneMatrix, Mat34V_In in_frameMatrix)
{
	sysMemAutoUseTempMemory useTempMem;

	Vector3 normal;
	normal.Cross(m_posWidth, m_posHeight);
	normal.Normalize();
	atRangeArray<spdPlane, 6> planes;
	bgMshMaterialMeshEditor meshEditor(in_pMaterial);
	const u16* indices = meshEditor.GetIndices();
	int indexCount = meshEditor.GetIndexCount();
	u8 frameFlags = 0;

	Vector3 corners[4];
	corners[0] = m_posBase;             // topleft
	corners[1] = m_posBase + m_posWidth; // topright
	corners[2] = m_posBase + m_posWidth + m_posHeight; // bottomRight
	corners[3] = m_posBase + m_posHeight; // bottomleft
	{
		Matrix34 paneMatrix = RCC_MATRIX34(in_paneMatrix);
		paneMatrix.Transform(corners[0]);
		paneMatrix.Transform(corners[1]);
		paneMatrix.Transform(corners[2]);
		paneMatrix.Transform(corners[3]);
	}
	u8 sides[4] = {bgBreakable::kFF_Left, bgBreakable::kFF_Top, bgBreakable::kFF_Right, bgBreakable::kFF_Bottom};
	int i0 = 3;
	atArray<Vector3> polyArrays[2];
	polyArrays[0].Reserve(20);
	polyArrays[1].Reserve(20);

	const float kFrameWidth = 0.05f;

	Matrix34 frameMatrix = RCC_MATRIX34(in_frameMatrix);
	for (int i1 = 0; i1<4; i1++)
	{
		CreateFrameBox(corners[i0], corners[i1], normal, kFrameWidth, planes);
		float frameArea = corners[i1].Dist(corners[i0]) * kFrameWidth;
		i0 = i1;
		float meshAreaInFrameBox = 0.f;
		for (int index=0; index<indexCount; index+=3)
		{
			int polyIn = 0;
			int polyOut = 1;
			int v0 = indices[index];
			int v1 = indices[index+1];
			int v2 = indices[index+2];
			if (in_boneIndex != -1)
			{
				if ((meshEditor.GetBlendIndices(v0).GetRed() != in_boneIndex) ||
					(meshEditor.GetBlendIndices(v1).GetRed() != in_boneIndex) ||
					(meshEditor.GetBlendIndices(v2).GetRed() != in_boneIndex))
				{
					continue;
				}
			}
			atArray<Vector3>& triangle = polyArrays[polyIn];
			triangle.Resize(0);
			triangle.Append() = meshEditor.GetPosition(v0);
			triangle.Append() = meshEditor.GetPosition(v1);
			triangle.Append() = meshEditor.GetPosition(v2);
			frameMatrix.Transform(triangle[0]);
			frameMatrix.Transform(triangle[1]);
			frameMatrix.Transform(triangle[2]);
			for (int planeIndex = 0; planeIndex<6; planeIndex++)
			{
				polyOut = 1 - polyIn;
				if (planes[planeIndex].Clip(&polyArrays[polyIn], &polyArrays[polyOut]) == 0)
					break;
				polyIn = polyOut;
			}
			atArray<Vector3>& polyFinal = polyArrays[polyOut];
			if (polyFinal.GetCount() > 0)
			{
				meshAreaInFrameBox += ComputePolygonArea(polyFinal); // area
			}
		}
		if (meshAreaInFrameBox > 0.5f * frameArea)
		{
			frameFlags |= sides[i1];
		}
	}
	return frameFlags;
}

bgPaneModelInfoBase* bgPaneModelInfoBase::CreateModelInfo(grcVertexBuffer* in_pVertexBuffer, grcIndexBuffer* in_pIndexBuffer, u8 in_shaderIndex, u8 in_glassType, int in_boneIndex)
{
	bgPaneModelInfoBase modelInfo;
	bool bModelOk = false;
	modelInfo.m_glassType = u8(in_glassType);
	modelInfo.m_shaderIndex = u8(in_shaderIndex);
	modelInfo.m_fvf = *(in_pVertexBuffer->GetFvf());
	sysMemStartTemp();
	{
		bgVertexBufferMeshEditor meshEditor(in_pVertexBuffer, in_pIndexBuffer);
		bModelOk = modelInfo.ComputeModelInfo(meshEditor, in_boneIndex);
	}
	sysMemEndTemp();
	return bModelOk ? rage_aligned_new(16) bgPaneModelInfoBase(modelInfo) : NULL;
}

bgPaneModelInfoBase* bgPaneModelInfoBase::CreateModelInfo(mshMaterial* in_pMaterial, u32 in_channelMask, u8 in_shaderIndex, u8 in_glassType, int in_boneIndex)
{
	// @TODO: use poor-man's polymorphism to allocate modelinfo subclass as appropriate..
	bgPaneModelInfoBase modelInfo;
	bool bModelOk = false;
	modelInfo.m_glassType = in_glassType;
	modelInfo.m_shaderIndex = in_shaderIndex;
	grmGeometry::ComputeFVF(*in_pMaterial, false, in_channelMask, modelInfo.m_fvf);

	sysMemStartTemp();
	{
		bgMshMaterialMeshEditor meshEditor(in_pMaterial);
		bModelOk = modelInfo.ComputeModelInfo(meshEditor, in_boneIndex);
	}
	sysMemEndTemp();
	return bModelOk ? rage_aligned_new(16) bgPaneModelInfoBase(modelInfo) : NULL;
}


bool bgPaneModelInfoBase::ComputeModelInfo(bgMeshEditor& in_meshEd, int in_boneIndex)
{
	// Find the largest triangle in the model and use it to compute the 'plane' of the glass pane
	{
		// find the triangle with the greatest area and use it to compute the plane normal
		int iLargestTriangle = in_meshEd.FindLargestTriangle(0, in_boneIndex);
		if (iLargestTriangle < 0)
		{
			bgWarningf("Mesh contains no geometry using bone %i; breakable glass will not function for this pane\n", in_boneIndex);
			return false;
		}
		const u16* indices = in_meshEd.GetIndices();
		Vector3 p0, p1, p2;
		p0 = in_meshEd.GetPosition(indices[iLargestTriangle]);
		p1 = in_meshEd.GetPosition(indices[iLargestTriangle+1]);
		p2 = in_meshEd.GetPosition(indices[iLargestTriangle+2]);
		Vector2 uv0, uv1, uv2;
		uv0 = in_meshEd.GetUV(indices[iLargestTriangle], 0);
		uv1 = in_meshEd.GetUV(indices[iLargestTriangle+1], 0);
		uv2 = in_meshEd.GetUV(indices[iLargestTriangle+2], 0);

		// Compute vectors a, b, such that for any u,v pair we can generate the position p for that pair:
		// p = a + b * u + c * v
		Vector3 p10 = p1 - p0;
		Vector3 p20 = p2 - p0;
		Vector2 uv10 = uv1 - uv0;
		Vector2 uv20 = uv2 - uv0;
		float denom = uv20.x * uv10.y - uv10.x * uv20.y;
		if (fabs(denom) < FLT_EPSILON)
		{
			Errorf("bgPaneModelInfoBase::ComputeModelInfo: Bad uv coords for glass drawable!");
		}
		Assertf(fabs(denom) >= FLT_EPSILON, "%s: bad uv coords for glass drawable!\n", __FUNCTION__);
		float alpha = 1.0f / denom;
		Vector3 b = (uv10.y * alpha) * p20 - (uv20.y * alpha) * p10;
		Vector3 c = (uv20.x * alpha) * p10 - (uv10.x * alpha) * p20;
		Vector3 a = p0 - uv0.x * b - uv0.y * c;

		in_meshEd.FindUVRange(0, in_boneIndex, m_uvMin, m_uvMax);

		m_posBase = a + m_uvMin.x * b + m_uvMin.y * c;
		m_posWidth = (m_uvMax.x - m_uvMin.x) * b;
		Assert(IsFiniteAll(RCC_VEC3V(m_posWidth)));
		m_posHeight = (m_uvMax.y - m_uvMin.y) * c;
		Assert(IsFiniteAll(RCC_VEC3V(m_posHeight)));

		// this was a "rage::Plane", changing to a Vector4
		Vector4 plane;
		{
			Vector3 v1, v2, n;
			v1 = p0 - p1;
			v2 = p2 - p1;
			n.Cross(v1,v2);
			n.Normalize();
			plane.x = n.x;
			plane.y = n.y;
			plane.z = n.z;
			plane.w = -n.x * p1.x -n.y * p1.y -n.z * p1.z;
		}
		//Plane plane;
		//plane.ComputePlane(p0, p1, p2);

		// Find min/max texcoords, compute average tangent and compute pane thickness.
		float minDepth = FLT_MAX;
		float maxDepth = -FLT_MAX;
		int vertexCount = in_meshEd.GetVertexCount();
		Vector3 tangent(0.f, 0.f, 0.f);
		const bool kHasBindingsChannel = (-1 != in_boneIndex) && in_meshEd.HasBindingsChannel();
		for (int vertexIndex = 0; vertexIndex < vertexCount; vertexIndex++)
		{
			if (kHasBindingsChannel)
			{
				const int boneIndex0 = in_meshEd.GetBlendIndices(vertexIndex).GetRed();
				if (boneIndex0 != in_boneIndex)
				{
					continue;
				}
			}

			tangent += in_meshEd.GetTangent(vertexIndex);

			Vector3 p = in_meshEd.GetPosition(vertexIndex);
			//float depth = plane.DistanceToPlane(p);
			float depth = plane.Dot3(p) - plane.w;
			if (maxDepth < depth)
				maxDepth = depth;
			if (minDepth > depth)
				minDepth = depth;
		}
		tangent.Normalize();
		m_tangent = tangent;
		m_flags |= kTangent;
		m_thickness = maxDepth - minDepth;
		// Set some default values for bounds offsets.
		// Call ComputeBoundsOffsets() to ensure that phGlass bounds match fragment bounds exactly
		m_boundsOffsetFront = m_boundsOffsetBack = 0.5f * m_thickness;
	}

	// add minimum set of vertex properties to fvf
	m_fvf.SetPosChannel(true);
	m_fvf.SetDiffuseChannel(true);
	m_fvf.SetTextureChannel(0, true, grcFvf::grcdsFloat2);
	m_fvf.SetTextureChannel(1, true, grcFvf::grcdsFloat2);
	m_fvf.SetTextureChannel(2, false, grcFvf::grcdsFloat2); // This channel is no longer used
	m_fvf.SetNormalChannel(true);
	m_fvf.SetTangentChannel(0, false, grcFvf::grcdsFloat3); // Tangent values are going to be calculated on in the vertex shader
	return true;
}
#endif

void bgPaneModelInfoBase::ComputeBoundsOffsets(Mat34V_In in_matrix, const phBound& in_bound)
{
	Vector3 normal;
	normal.Cross(m_posHeight, m_posWidth);
	normal.Normalize();

	// transform the normal to bounds-space
	Vec3V boundNorm = UnTransform3x3Ortho(in_matrix, RCC_VEC3V(normal));
	// transfrom base to bounds-space
	Vec3V boundsBase = UnTransformOrtho(in_matrix, RCC_VEC3V(m_posBase));

	Vector3 boundMin = VEC3V_TO_VECTOR3(in_bound.GetBoundingBoxMin());
	Vector3 boundMax = VEC3V_TO_VECTOR3(in_bound.GetBoundingBoxMax());
	Vector3 boundFront, boundBack;
	boundFront.x = Selectf(boundNorm.GetXf(), boundMax.x, boundMin.x);
	boundFront.y = Selectf(boundNorm.GetYf(), boundMax.y, boundMin.y);
	boundFront.z = Selectf(boundNorm.GetZf(), boundMax.z, boundMin.z);
	boundBack.x = Selectf(boundNorm.GetXf(), boundMin.x, boundMax.x);
	boundBack.y = Selectf(boundNorm.GetYf(), boundMin.y, boundMax.y);
	boundBack.z = Selectf(boundNorm.GetZf(), boundMin.z, boundMax.z);
	m_boundsOffsetFront = Dot(boundNorm, RCC_VEC3V(boundFront) - boundsBase).Getf();
	m_boundsOffsetBack = Dot(boundNorm, boundsBase - RCC_VEC3V(boundBack)).Getf();
}

void bgPaneModelInfoBase::ComputePlane(Vector4& out_plane) const
{
	out_plane.ComputePlane(m_posBase, m_posBase + m_posWidth, m_posBase + m_posHeight);
}

Vec3V_Out bgBreakable::GetPaneCenter() const
{
	const bgGeometryData& data = GetGeometryData();
	Vector3 center = data.m_posTopLeft + 0.5f * (data.m_posHeight + data.m_posWidth);
	return RCC_VEC3V(center);
}

float bgBreakable::GetPaneSize() const
{
	const bgGeometryData& data = GetGeometryData();
	Vector3 diagonal = data.m_posHeight + data.m_posWidth;
	return 0.5f * diagonal.Mag();
}


void bgBreakable::CalculateSizeTypeIndex(const bgPaneModelInfoBase& modelInfo)
{

	const float modelWidth2 = modelInfo.m_posWidth.Mag2();
	const float modelHeight2 = modelInfo.m_posHeight.Mag2();

	//Set default glass type
	const atArray<bgGlassTypeConfig>& glassTypes = bgSCracksTemplate::InstanceRef().GetGlassTypes();
	int bestGlassSizeIndex = 0;
	float bestMag2 = FLT_MAX;

	//Find best glass type
	for(int index = 0, size = glassTypes[m_glassType].m_glassSizes.size() ; index != size; ++ index)
	{

		//Find glass type dimensions
		float typeWidth = glassTypes[m_glassType].m_glassSizes[index].m_width;
		float typeHeight = glassTypes[m_glassType].m_glassSizes[index].m_height;
		//Verify dimensions
		if(typeWidth < 0 || typeHeight < 0)
			continue;

		//Choose best dimensions
		float typeWidth2 = typeWidth * typeWidth;
		float typeHeight2 = typeHeight * typeHeight;
		float mag2 = Min(
			Vector2(modelWidth2 - typeWidth2, modelHeight2 - typeHeight2).Mag2(),
			Vector2(modelHeight2 - typeWidth2, modelWidth2 - typeHeight2).Mag2()
			);
		if(mag2 < bestMag2)
		{
			bestMag2 = mag2;
			bestGlassSizeIndex = index;
		}
	}
	m_glassSizeIndex = u8(bestGlassSizeIndex);
}
void bgBreakable::InitInternalFromModelInfo(const bgPaneModelInfoBase& modelInfo)
{
	Reset();
	m_glassType = modelInfo.m_glassType;

	//Calculate Size Type Index Here
	CalculateSizeTypeIndex(modelInfo);
	m_BreakableData.Init();
	if (!IsValid())
		return;

	bgGeometryData& data = GetGeometryDataNonConst();
	data.m_isReinforced = m_strength == STRENGTH_REINFORCED;
	data.m_lod = bgLod::LOD_VLOW; // Always start from the lowest LOD
	data.m_posTopLeft = modelInfo.m_posBase;
	data.m_posWidth = modelInfo.m_posWidth;
	Assert(IsFiniteAll(RCC_VEC3V(data.m_posWidth)));
	data.m_posHeight = modelInfo.m_posHeight;
	Assert(IsFiniteAll(RCC_VEC3V(data.m_posHeight)));
	data.m_thickness = modelInfo.m_thickness;

	m_boundsOffsetBack = modelInfo.m_boundsOffsetBack;
	m_boundsOffsetFront = modelInfo.m_boundsOffsetFront;

	// Convert the existing frame flags to broken frame flags by inverting
	m_BrokenFrameFlags = (~(modelInfo.m_flags >> 2)) & kFF_All;

	m_fvf = modelInfo.m_fvf;

	// Remove the tangent channel if set - we are going to use the transformation right vector instead
	m_fvf.SetTangentChannel(0, false, grcFvf::grcdsFloat3);

	// Disable the 3rd UV channel as don't use it anymore
	m_fvf.SetTextureChannel(2, false, grcFvf::grcdsFloat2);

	//Find rotation from pane to AABB space
	m_rotation.a = data.m_posWidth;
	m_rotation.a.Normalize();
	m_rotation.b = -data.m_posHeight;
	m_rotation.b.Normalize();
	m_rotation.c.Cross(m_rotation.a, m_rotation.b);
	m_rotation.Transpose();

	bgCracksTemplate* pCracksTemplate;
	const char* szErrMsg = NULL;
	if (Verifyf(bgCracksTemplate::IsInitialized(pCracksTemplate, &szErrMsg), "Breakable glass system not initialized: %s\n", szErrMsg))
	{

		const atArray<bgGlassTypeConfig>& glassTypes = pCracksTemplate->GetGlassTypes();
		if (0 > m_glassType || 
			m_glassType > glassTypes.GetCount())
		{
			Warningf("glass type %i out of range; defaulting to type 0 (%s)\n", m_glassType, glassTypes[0].GetTypeName().c_str());
			m_glassType = 0;
		}
#if __BANK
		if(Verifyf(sm_iTotalBatches > 0 && sm_iTotalBatches <= sm_iMaxBatches, "Breakable is trying to use too many batches - this will end up in a crash"))
		{
			// All good
		}
		else
		{
			// Clamp to avoid crash
			sm_iTotalBatches = Clamp(sm_iTotalBatches, 1, sm_iMaxBatches);
		}
#endif // __BANK
		const int numBatches = Min(sm_iTotalBatches, (int)glassTypes[m_glassType].GetNumBatches());
		if(m_FallingPieceMatrix.GetCapacity() != numBatches * SKINNING_COUNT)
		{
			m_FallingPieceMatrix.Reset();
			m_FallingPieceMatrix.Reserve(numBatches * SKINNING_COUNT);

		}
	}
}

void bgBreakable::Pack()
{
	m_BreakableData.Pack();
}

void bgBreakable::Unpack()
{
	m_BreakableData.Unpack();
}

void bgBreakable::Hit(int in_CrackType,
					  Vec3V_In in_worldHitLoc,
					  Vec3V_In in_worldImpactImpulse,
					  int in_damage)
{
	BANK_ONLY(sm_vLastHitPos = in_worldHitLoc);

	Vec3V localHitLoc = UnTransformOrtho(RCC_MAT34V(m_transform), in_worldHitLoc);
	Vec2V hitLoc2d = Convert3DPointTo2DPoint(localHitLoc);
	HitInternal(in_CrackType, hitLoc2d, in_worldImpactImpulse, in_damage);
}

//------------------------------------------------------------------------------
// Our piece of glass was hit, update our geometry appropriately
//------------------------------------------------------------------------------
void bgBreakable::HitInternal(int in_CrackType,
					  Vec2V_In in_hitLoc2d,
					  Vec3V_In in_worldImpactImpulse,
					  int in_damage)
{
	//Reset the number of fallen pieces
	if (m_strength == STRENGTH_NORMAL)
		m_fallen = 0;

	//Create the geometry that will be render on the screen
	//Profiling
#ifdef GLASS_PROFILING
	m_profileHitClampTotal = 0;
	m_profileHitTriangleDecompositionTotal = 0;
	m_profileHitPieceDisplacement = 0;
	m_profileCopy = 0;
	
	//Reset all profiling counter
	m_profileHitTotal = 0;
	//Total amount of time to clamp the pieces when touching the edge of the window pane
	m_profileHitClampTotal = 0;
	//Total amount of time to decompose the pieces silhouette into triangle
	m_profileHitTriangleDecompositionTotal = 0;
	//Total amount of time to displace the silhouette vertex to create bevel and cracks between pieces in the geometry pass
	m_profileHitPieceDisplacement = 0;
	//2D data need update by all the hits together
	m_profileBreakPieces = 0;
	m_profileVisibility = 0;
	
	//Profiling
	sysTimer timerHit;
#endif

	// Make sure templates are available.
	bgCracksTemplate* pCracksTemplate;
	const char* pErrMsg;
	if (!bgVerifyf(bgCracksTemplate::IsReady(pCracksTemplate, &pErrMsg), "bgSCracksTemplate not ready: %s", pErrMsg ))
		return;

#if __BANK
	if (sm_bOverrideGlassType) 
	{
		int maxCount = pCracksTemplate->GetGlassTypes().GetCount()-1;
		sm_GlassType = sm_GlassType > maxCount ? maxCount : sm_GlassType;
		m_glassType = sm_GlassType;
	}
#endif // __BANK

	// Make sure input values are in range.
	const atArray<bgGlassTypeConfig>& glassTypes = pCracksTemplate->GetGlassTypes();
	if (!bgVerifyf(0<= m_glassType && m_glassType < glassTypes.GetCount(),
		"glass type %i out of range! (max %i)\n", m_glassType, glassTypes.GetCount()))
		return;

	OUTPUT_ONLY(const atArray<char*>& crackTypes = pCracksTemplate->GetCrackTypes());
	if (!bgVerifyf(0<= in_CrackType && in_CrackType < pCracksTemplate->GetCrackTypes().GetCount(),
		"crack type %i out of range! (max %i)\n", in_CrackType, crackTypes.GetCount()))
		return;

	// Get the crack data needed for this kind of weapon
	const bgCrackType* pCrackType = bgSCracksTemplate::InstanceRef().GetCrackData(m_glassType, m_glassSizeIndex, in_CrackType);
	if (!bgVerifyf(pCrackType, "Cracks template missing bgCrackType for glass type %i crack type %i\n", m_glassType, in_CrackType))
	{
		return;
	}

	bgUseMemoryBucket geometryBucket(kGeometryBucket);
	//First things to do is to add the new hit to our array of hits
	atArray<bgHitInfo>& hits = GetGeometryDataNonConst().m_hitInfo;
	bgHitInfo& oneHit = hits.Grow();
	oneHit.m_crackType = (u8)in_CrackType;

	// Store hit data
	using std::min;
	using std::max;
	oneHit.m_location.x = Clamp(in_hitLoc2d.GetXf(), 1e-6f, 1.f - 1e-6f);
	oneHit.m_location.y = Clamp(in_hitLoc2d.GetYf(), 1e-6f, 1.f - 1e-6f);
	oneHit.m_worldImpactImpulse = RCC_VECTOR3(in_worldImpactImpulse);

	//-----------------
	//Set the crack map center location on the pane of glass
	SetCrackMapCenterLocationValue(pCrackType->m_crackMapCenterLocation, oneHit.m_location, &oneHit.m_crackMapTranslation);

	//-----------------
	//Set the crack map rotation based on on user xml input.
	Vector2 paneDimension = Vector2(GetGeometryData().m_posWidth.Mag(), GetGeometryData().m_posHeight.Mag());
	SetCrackMapRotationValue(pCrackType->m_crackMapRotationType, paneDimension, &oneHit.m_rotation);

	//-----------------
	//Set the crack map scaling based on on user xml input.
	Vector2 overwriteScaling = Vector2(pCrackType->m_crackMapOverwriteScalingX, pCrackType->m_crackMapOverwriteScalingY);
	SetCrackMapScalingValue(	pCrackType->m_crackMapScalingType,	pCrackType->m_crackMapCenterLocation, pCrackType->m_crackMapRotationType,
		paneDimension, overwriteScaling, &oneHit.m_scaleData);


	bool bHitPane = m_strength == STRENGTH_REINFORCED ? !m_fallen : true;

	// The first hit is special since it generates the initial crack pattern.
	// After that we procedurally crack the glass
	bool bUseInitKillRadius = false;
	if (hits.GetCount() == 1)
	{
		if (0 == pCrackType->m_crackArray.GetCount())
		{
			// Can't break glass without a star map template :-(.  Print nice warning message.  
			// Note range checking for glass type and crack type already performed earlier :-)
			Warningf("No crack star map for %s/%s\n", 
				glassTypes[m_glassType].GetTypeName().c_str(), 
				crackTypes[in_CrackType]);
			bHitPane = false;
		}
		else
		{
			// Pick a random crack set to use
#if __BANK
			if (sm_bOverrideCrackVariation) 
			{
				int maxCount = pCrackType->m_crackArray.GetCount()-1;
				sm_CrackVariation = sm_CrackVariation > maxCount ? maxCount : sm_CrackVariation;
				oneHit.m_starMapIndex = (u8)sm_CrackVariation;
			}
			else
#endif
				oneHit.m_starMapIndex = (u8)sm_Rand.GetRanged((int)(0), (int)(pCrackType->m_crackArray.GetCount()-1));

			// We create the pieces that the glass will be composed of.  We only do
			// that for the first hit as the  subsequent hit will be done procedurally
			PIXBegin(0, "bgBreakable::Create2DDataFromHit() - first hit");
			Create2DDataFromHit(oneHit);
			PIXEnd();
			bHitPane = true;
		}
		bUseInitKillRadius = true;

		// Pick a mask
		if(pCrackType->m_decalTextureChannel == bgCrackType::ec_randomRedGreen)
		{
			oneHit.m_decalChannelIndex = static_cast<u8>(sm_Rand.GetRanged((int)(0), (int)(1)));
		}
		else if(pCrackType->m_decalTextureChannel == bgCrackType::ec_randomRedGreenAlpha)
		{
			oneHit.m_decalChannelIndex = static_cast<u8>(sm_Rand.GetRanged((int)(0), (int)(2)));
			oneHit.m_decalChannelIndex = (oneHit.m_decalChannelIndex == 2) ? 3 : oneHit.m_decalChannelIndex;
		}
		else if(pCrackType->m_decalTextureChannel == bgCrackType::ec_randomAll)
		{
			oneHit.m_decalChannelIndex = static_cast<u8>(sm_Rand.GetRanged((int)(0), (int)(3)));
		}
		else
		{
			oneHit.m_decalChannelIndex = static_cast<u8>(pCrackType->m_decalTextureChannel);
		}
		BANK_ONLY(oneHit.m_decalChannelIndex = sm_decalMaskSelection < 0 ? oneHit.m_decalChannelIndex : static_cast<u8>(sm_decalMaskSelection));

		Assertf(oneHit.m_decalChannelIndex <= 3, "Decal channel index out of range");
		Assertf(oneHit.m_decalChannelIndex != 2, "Decal channel index 2 should not be used");
	}
	else
	{
		// Support changing the star map crack index on sequential hits
#if __BANK
		if (sm_bOverrideCrackVariation) 
		{
			int maxCount = pCrackType->m_crackArray.GetCount()-1;
			sm_CrackVariation = sm_CrackVariation > maxCount ? maxCount : sm_CrackVariation;
			oneHit.m_starMapIndex = (u8)sm_CrackVariation;
		}
		else
#endif
		oneHit.m_starMapIndex = hits[0].m_starMapIndex;
		oneHit.m_decalChannelIndex = hits[0].m_decalChannelIndex;

		// Add support for applying the crack template on a broken pane for large pieces
		// Don't support thsi for the lowest LOD
		if(pCrackType->m_enableSequentialHit && (GetLod() > 0) BANK_ONLY(&& !sm_bDisableSequentialHit))
		{
			// Lets try to apply the template another time on large triangles using the initial impact radius
			const bgCrackType& glassCrack = *oneHit.GetCrackType(m_glassType, m_glassSizeIndex);
			float killRadius = sm_Rand.GetFloat() < 0.5 ? glassCrack.m_initialImpactKillRadius1 : glassCrack.m_initialImpactKillRadius2;

			bgGeometryData& data = GetGeometryDataNonConst();
			PanePieces& panePieces = data.m_panePieces;
			PaneEdges& paneEdges = data.m_paneEdges;
			PanePoints& panePoints = data.m_panePoints;

			int maxInterations = panePieces.GetCount() + 40; // I'm 40.
			for (int iPanePiece = 0, interationCount = 0; iPanePiece < panePieces.GetCount() && interationCount < maxInterations; iPanePiece++,interationCount++)
			{
				bgPanePiece& piece = panePieces[iPanePiece];

				// Skip concave pieces and pieces that are too small
				if(piece.m_area > pCrackType->m_minSequentialHitPieceSize && piece.m_polygons.GetCount() == 1)
				{
					bool bIsBroken = !(piece.m_flags & bgPanePiece::kPF_Active);
					bool bIsHit = bIsBroken || (-1 != piece.m_hit); // true if piece is already broken or decal coords already specified for this piece
					bool isPointInsidePolygon = false;
					if(!bIsHit && !bIsBroken)
					{
						for (int iEdge = 0; iEdge < piece.m_silhouette.GetCount();iEdge++)
						{
							bgEdgeRef edge = piece.m_silhouette[iEdge];
							const Vector2& p0 = panePoints[edge.GetP0(paneEdges)];
							const Vector2& p1 = panePoints[edge.GetP1(paneEdges)];
							float fDist2 = PointToEdgeDist2(p0, p1, oneHit.m_location);

							// Take the maximum value here to get more detail in the geometry
							if ((!bIsHit) && (fDist2 <= pCrackType->m_decalRadiusImpactShowValueMax))
							{
								bIsHit = true;
							}

							if (fDist2 <= killRadius)
							{
								// An edge of this piece is within 'kill' radius; queue it up to be broken off the pane
								bIsBroken = true;
							}

							if (!bIsHit && !bIsBroken)
							{
								// test if the impact location is within this piece by counting edge intersections
								CheckEdgeIntersection(p0, p1, oneHit.m_location, isPointInsidePolygon);
							}
						}

						// If inside the break area, see if can apply the star map on it again
						if (bIsHit || bIsBroken || isPointInsidePolygon)
						{
							PIXBegin(0, "bgBreakable::Create2DDataFromHit() - additional hit");
							Create2DDataFromSequentHit(oneHit, iPanePiece);
							bUseInitKillRadius = true;
							PIXEnd();
						}
					}
				}
			}
		}
	}

	float fDecalScaleMin = oneHit.GetCrackType(m_glassType, m_glassSizeIndex)->m_decalTextureScaleMin;
	float fDecalScaleMax = oneHit.GetCrackType(m_glassType, m_glassSizeIndex)->m_decalTextureScaleMax;
	oneHit.m_decalTextureScale = fDecalScaleMin + sm_Rand.GetFloat() * (fDecalScaleMax - fDecalScaleMin);
	
#ifdef GLASS_PROFILING
	//Data that need to be updated with all the different hit is done here
	sysTimer timerAllHit;
	m_profileBreakPieces = 0;
#endif
	//Hit the pane with the current hit and make piece fly out
	m_health -= in_damage;

	// Need to compute lod first so we know thresholds for discarding shards and physics lod
	AdjustLod();

	PIXBegin(0, "bgBreakable::HitPane");
	bHitPane = bHitPane && HitPane(oneHit, bUseInitKillRadius);
	PIXEnd();
	if (!bHitPane)
	{
		//We did not hit the pane:  We must stop our work and remove the current hit from our array of hit
		hits.DeleteFast(hits.GetCount()-1);
		m_health += in_damage;
	}
#ifdef GLASS_PROFILING
	m_profileBreakPieces = timerAllHit.GetMsTime();
	m_profileHitTotal = timerHit.GetMsTime();
#endif //GLASS_PROFILING

	m_bUpdateBuffers = bHitPane;

#if !__NO_OUTPUT
	size_t used, available, largest;
	bgBreakable::GetPrivateHeapStats(used, available, largest);
	bgDebugf2("Post-Break: used: %" SIZETFMT "d, available: %" SIZETFMT "d largest: %" SIZETFMT "d", used, available, largest );
#endif
}//Hit

//-----------------------------------------
//Return a location on the pane of glass where the crack map should 
// have it's center based on what the user intended
//
// Input:	centerType:			xml user data telling us where the center should be
//			impactLocation:		Center good go at the i8mpact location.  Passed in in case we need it.
// Output:  out_location:		x,y location with value between 0 and 1.
//								Extreme example:
//								0,0 = Crack map center should be located at the top-left corner of the pane of glass
//								1,1 = Crack map center should be located at the bottom-right corner of the pane of glass
//
void bgBreakable::SetCrackMapCenterLocationValue(const enum bgCrackType::bgCrackPlacementType centerType, Vector2& impactLocation, Vector2* out_location)
{
	// Crack map center should be at impact location
	if (centerType == bgCrackType::ep_glassImpactLocation)
	{
		*out_location = impactLocation;
	}
	//Crack map center should be at the center of the pane of glass
	else if (centerType == bgCrackType::ep_glassCenter)
	{
		*out_location = Vector2(0.5f, 0.5f);
	}
	//Crack map center should be randomly placed on the pane of glass
	else //if (centerType == bgCrackType::ep_glassRandom)
	{
		(*out_location).x = sm_Rand.GetFloat();
		(*out_location).y = sm_Rand.GetFloat();
	}
}

//-----------------------------------------
//Return the proper crack map rotation based on what the user intended
//
// Input:	rotationType:		xml user data telling us what type of rotation we need to calculate
//			paneDimention:		Pane of glass dimension where the crack map will be applied
//			
// Output:	out_rotation:		angle in radian at which the crack map should be rotated
//
void bgBreakable::SetCrackMapRotationValue(const enum bgCrackType::bgCrackRotationType rotationType, Vector2& paneDimension, float* out_rotation)
{
	//Bo rotation requested:  Set it at 0
	if (rotationType == bgCrackType::er_none)
	{
		*out_rotation = 0;
	}
	//Random rotation between 0 and 360 requested
	else if (rotationType == bgCrackType::er_0_to_360)
	{
		*out_rotation = sm_Rand.GetFloat() * (2.f * PI);
	}
	//90 angle step angle requested:  Either set the rotation to 0, 90, 180, or 270 degree.
	else if (rotationType == bgCrackType::er_stepOf90)
	{
		float randTemp = sm_Rand.GetFloat();
		if (randTemp < 0.25f)
			*out_rotation = 0;
		else if (randTemp < 0.5f)
			*out_rotation = PI * 0.5f;
		else if (randTemp < 0.75f)
			*out_rotation = PI;
		else
			*out_rotation = PI * 1.5f;
	}
	//either have 0 or 180 degree rotation
	else if (rotationType == bgCrackType::er_flip)
	{
		if (sm_Rand.GetFloat() > 0.5f)
			*out_rotation = 0;
		else
			*out_rotation = PI;
	}
	//Crack map or texture are setup with horizontal data ==> Rotate only of we have a vertical pane
	else if (rotationType == bgCrackType::er_rectangle)
	{
		//Horizontal is wider or we are dealing with a square:  no rotation
		if (paneDimension.x >= paneDimension.y)
			*out_rotation = 0;
		//Flip
		else
			*out_rotation = PI * 0.5f;
	}
	//Crack map or texture are setup with horizontal data ==> Rotate we have a vertical pane and then 
	// randomly check if we need to flip
	else if (rotationType == bgCrackType::er_rectangleFlip)
	{
		//Horizontal is wider or we are dealing with a square:  no rotation
		if (paneDimension.x >= paneDimension.y)
			*out_rotation = 0;
		//Flip
		else
			*out_rotation = PI * 0.5f;

		//50% chance to flip
		if (sm_Rand.GetFloat() > 0.5f)
			*out_rotation += PI;
	}

	//Rotation might be overwritten from RAG value
#if __BANK && __DEV
	if (sm_bOverrideCrackRotation) 
		*out_rotation = sm_CrackRotation * (2.f * PI);
#endif
}


//-----------------------------------------
//Return the proper crack map scaling based on what the user intended
//
// Input:	scalingType			xml user data telling us what type of scaling we need to calculate
//			centerType:			xml user data telling us where the center should be
//			rotationType:		xml user data telling us what type of rotation we need to calculate
//			paneDimension:		Pane of glass dimension where the crack map will be applied
//			overwriteScaling:	xml x,y scaling value if we want to overwrite the scaling
//
// Output:	out_scaling			Crack map scaling
//
void bgBreakable::SetCrackMapScalingValue(	const enum bgCrackType::bgCrackScalingType scalingType, 
										  const enum bgCrackType::bgCrackPlacementType centerType, 
										  const enum bgCrackType::bgCrackRotationType rotationType,
										  Vector2& paneDimension, Vector2& overwriteScaling, Vector2* out_scaling)
{
	//No scaling:  Return 1,1
	if (scalingType == rage::bgCrackType::es_none)
	{
		*out_scaling = Vector2(1,1);
	}
	//Scaling is based on what we entered for rotation and translation
	else if (scalingType == rage::bgCrackType::es_automatic)
	{
		//Start at 1
		*out_scaling = Vector2(1,1);

		//If we are allowing the center of the crack map to move away from the center of the pane of glass:
		//  we need to double the scaling in order to fully fill up the pane of glass.
		if (centerType == bgCrackType::ep_glassImpactLocation || centerType == bgCrackType::ep_glassRandom)
		{
			*out_scaling = Vector2(2,2);
		}

		//If the pane of glass is rectangular:  Resize the crack map to keep a square shape.
		if (paneDimension.x > paneDimension.y)
		{
			(*out_scaling).y *= (paneDimension.x / paneDimension.y);
		}
		else
		{
			(*out_scaling).x *= (paneDimension.y / paneDimension.x);
		}

		//Add rotation scaling to account for 45 degree rotation to fully fill up the pane of glass
		if (rotationType == rage::bgCrackType::er_0_to_360)
			*out_scaling *= M_SQRT2;
	}
	//Just keep the crack map square
	else if (scalingType == rage::bgCrackType::es_forceSquared)
	{
		//If the pane of glass is rectangular:  Resize the crack map to keep a square shape.
		if (paneDimension.x > paneDimension.y)
		{
			(*out_scaling).x = 1.0f;
			(*out_scaling).y = (paneDimension.x / paneDimension.y);
		}
		else 
		{
			(*out_scaling).x = (paneDimension.y / paneDimension.x);
			(*out_scaling).y = 1.0f;
		}
	}
	//Overwrite the scaling using the user data
	else if (scalingType == rage::bgCrackType::es_overwrite)
	{
		(*out_scaling).x = overwriteScaling.x;
		(*out_scaling).y = overwriteScaling.y;
	}
	//Overwrite the scaling using the user data
	else if (scalingType == rage::bgCrackType::es_overwriteSquared)
	{
		//User data
		(*out_scaling).x = overwriteScaling.x;
		(*out_scaling).y = overwriteScaling.y;

		//resize based on rectangle size
		if (paneDimension.x > paneDimension.y)
		{
			(*out_scaling).y *= (paneDimension.x / paneDimension.y);
		}
		else 
		{
			(*out_scaling).x *= (paneDimension.y / paneDimension.x);
		}
	}
}

// return the amount of dynamically allocated memory we are responsible for
int bgBreakable::GetDynamicStorage() const
{
	return 0;
}

#if __PFDRAW
void bgBreakable::ProfileDraw()
{
#if __BANK
	if(!sm_bEnableProfileDraw)
		return;

	// Draw the falling pieces distance from the pane
	if(sm_bShowPiecesDist)
	{
		Matrix34 zero;
		zero.Zero();
		FallingPieces& fallingPieces = GetFallingPiecesNonConst();
		for(int i=0; i < fallingPieces.GetCount(); i++)
		{
			Vector3 vPiecePosWorld;
			Matrix34 curMat;
			m_FallingPieceMatrix[bgMatrixArray::FIRST_FALLING_PIECE + i].ToMatrix34(RC_MAT34V(curMat));
			if(!curMat.IsEqual(zero)) // Skip dead pieces
			{
				vPiecePosWorld = curMat.d;
				float fDistFromOrg = vPiecePosWorld.Dot(vPiecePosWorld);
				m_transform.Transform(vPiecePosWorld, vPiecePosWorld);
				char strDist[32];
				formatf(strDist, "%.2f", fDistFromOrg);
				grcDebugDraw::Text(vPiecePosWorld, Color_red, strDist); // Render the text
				grcDebugDraw::Line(vPiecePosWorld, m_transform.d, Color_red); // Draw a line from the pane to the piece
			}
		}
	}

	// Draw the LOD on the broken panes
	if(sm_bShowLOD)
	{
		// Show the selected LOD (position has to be recalculated so this would work with the override)
		Vec3V worldPos = GetPaneCenter();
		Mat34V transform = RCC_MAT34V(m_transform);
		worldPos = rage::Transform(transform, worldPos); // transform model-space sphere pos to world-space
		int lod = GetLod();
		static const Color32 lodColor[] = {Color_blue, Color_green, Color_orange, Color_red};
		static const char strLODNames[bgLod::LOD_COUNT][9] = {"LOD_VLOW", "LOD_LOW", "LOD_MED", "LOD_HIGH"};
		grcDebugDraw::Text(worldPos, lodColor[lod], strLODNames[lod]);
	}

	// Show the batch information for each falling piece
	if(sm_bShowBatchInfo)
	{
		Matrix34 zero;
		zero.Zero();
		FallingPieces& fallingPieces = GetFallingPiecesNonConst();
		for(int i=0; i < fallingPieces.GetCount(); i++)
		{
			Vector3 vPiecePosWorld;
			Matrix34 curMat;
			m_FallingPieceMatrix[bgMatrixArray::FIRST_FALLING_PIECE + i].ToMatrix34(RC_MAT34V(curMat));
			if(!curMat.IsEqual(zero)) // Skip dead pieces
			{
				int iBatch = (bgMatrixArray::FIRST_FALLING_PIECE + i) / SKINNING_COUNT;
				vPiecePosWorld = curMat.d;
				m_transform.Transform(vPiecePosWorld, vPiecePosWorld);
				static const Color32 batchColor[] = {Color_red, Color_green, Color_blue, Color_orange, Color_yellow, Color_cyan};
				char strBatchInfo[32];
				formatf(strBatchInfo, "%d", iBatch);
				grcDebugDraw::Text(vPiecePosWorld, batchColor[iBatch], strBatchInfo); // Render the text
			}
		}
	}

	if(sm_bShowPieceSize)
	{
		// Show that total pane size
		Vec3V worldPos = RCC_VEC3V(GetGeometryData().m_posTopLeft);
		Mat34V transform = RCC_MAT34V(m_transform);
		worldPos = rage::Transform(transform, worldPos); // transform model-space sphere pos to world-space
		char strInfo[32];
		float fWidth = GetGeometryData().m_posWidth.x;
		float fHeight = -GetGeometryData().m_posHeight.z;
		formatf(strInfo, "%.5f (%.3f_%.3f)", fWidth*fWidth + fHeight*fHeight, fWidth, fHeight);
		grcDebugDraw::Text(worldPos, Color_green, strInfo); // Render the text

		// Go over each piece that is still alive and show its size
		Matrix34 zero;
		zero.Zero();
		FallingPieces& fallingPieces = GetFallingPiecesNonConst();
		for(int i=0; i < fallingPieces.GetCount(); i++)
		{
			Vector3 vPiecePosWorld;
			Matrix34 curMat;
			m_FallingPieceMatrix[bgMatrixArray::FIRST_FALLING_PIECE + i].ToMatrix34(RC_MAT34V(curMat));
			if(!curMat.IsEqual(zero)) // Skip dead pieces
			{
				vPiecePosWorld = curMat.d;
				m_transform.Transform(vPiecePosWorld, vPiecePosWorld);
				fWidth = fallingPieces[i].m_extents.x * 2.0f;
				float fHeight = fallingPieces[i].m_extents.y * 2.0f;
				formatf(strInfo, "%.5f (%.3f_%.3f)", fWidth*fWidth + fHeight*fHeight, fWidth, fHeight);
				grcDebugDraw::Text(vPiecePosWorld, Color_red, strInfo); // Render the text
			}
		}
	}

	if(sm_bShowPhysicsType)
	{
		// Show that total pane size
		Vec3V worldPos = RCC_VEC3V(GetGeometryData().m_posTopLeft);
		Mat34V transform = RCC_MAT34V(m_transform);
		worldPos = rage::Transform(transform, worldPos); // transform model-space sphere pos to world-space
		char strInfo[32];
		float fWidth = GetGeometryData().m_posWidth.x;
		float fHeight = -GetGeometryData().m_posHeight.z;
		formatf(strInfo, "%.5f (%.3f_%.3f)", fWidth*fWidth + fHeight*fHeight, fWidth, fHeight);
		grcDebugDraw::Text(worldPos, Color_green, strInfo); // Render the text

		// Go over each piece that is still alive and show its size
		Matrix34 zero;
		zero.Zero();
		bgPhysicsData& fallingPhysics = GetFallingPhysicsNonConst();
		FallingPieces& fallingPieces = GetFallingPiecesNonConst();
		for(int i=0; i < fallingPieces.GetCount(); i++)
		{
			Vector3 vPiecePosWorld;
			Matrix34 curMat;
			m_FallingPieceMatrix[bgMatrixArray::FIRST_FALLING_PIECE + i].ToMatrix34(RC_MAT34V(curMat));
			if(!curMat.IsEqual(zero) && fallingPhysics[i].m_pPhysics) // Skip dead pieces
			{
				vPiecePosWorld = curMat.d;
				m_transform.Transform(vPiecePosWorld, vPiecePosWorld);
				formatf(strInfo, "%s", fallingPhysics[i].m_pPhysics->GetCollider() ? "R" : "S");
				grcDebugDraw::Text(vPiecePosWorld, Color_red, strInfo); // Render the text
			}
		}
	}

	if(sm_bShowSequentialHit)
	{
		// Copy actual data to reduce the chance of a crash
		char strInfo[32];
		const bgGeometryData& data = GetGeometryData();
		const PanePieces& panePieces = data.m_panePieces;
		const PaneEdges& paneEdges = data.m_paneEdges;
		const PanePoints& panePoints = data.m_panePoints;

		for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++)
		{
			const bgPanePiece& piece = panePieces[iPanePiece];
			if (piece.m_flags & bgPanePiece::kPF_Active)
			{
				// Draw the polys
				for(int iPoly = 0; iPoly < piece.m_polygons.GetCount(); iPoly++)
				{
					bgPolygon const& polygon = piece.m_polygons[iPoly];
					for(int iCurP = 0; iCurP < polygon.GetCount()-1;  iCurP++)
					{
						const Vector2& p0 = panePoints[polygon[iCurP]];
						const Vector2& p1 = panePoints[polygon[iCurP+1]];

						// Draw the polygon silhouette
						Vector3 p0WS = data.Transform(p0);
						m_transform.Transform(p0WS);
						Vector3 p1WS = data.Transform(p1);
						m_transform.Transform(p1WS);
						grcDebugDraw::Line(p0WS, p1WS, Color32(0, 125, 255, 255)); // Draw the silhouette
					}
					const Vector2& p0 = panePoints[polygon[polygon.GetCount()-1]];
					const Vector2& p1 = panePoints[polygon[0]];

					// Draw the polygon silhouette
					Vector3 p0WS = data.Transform(p0);
					m_transform.Transform(p0WS);
					Vector3 p1WS = data.Transform(p1);
					m_transform.Transform(p1WS);
					grcDebugDraw::Line(p0WS, p1WS, Color32(0, 125, 255, 255)); // Draw the silhouette
				}

				// Find the piece center position and draw the silhouette
				Vector3 centerPos = Vector3(0.0f, 0.0f, 0.0f);
				for (int iEdge = 0; iEdge < piece.m_silhouette.GetCount();iEdge++)
				{
					bgEdgeRef edge = piece.m_silhouette[iEdge];
					const Vector2& p0 = panePoints[edge.GetP0(paneEdges)];
					const Vector2& p1 = panePoints[edge.GetP1(paneEdges)];

					Vector3 p0WS = data.Transform(p0);
					m_transform.Transform(p0WS);
					Vector3 p1WS = data.Transform(p1);
					m_transform.Transform(p1WS);
					grcDebugDraw::Line(p0WS, p1WS, Color32(0, 0, 255, 255)); // Draw the silhouette
					centerPos += p0WS; // Sum the positions so we can get the center position
				}
				centerPos /= static_cast<float>(piece.m_silhouette.GetCount());

				if(piece.m_area >= 0.001)
					formatf(strInfo, "%.5f", piece.m_area);
				else if(piece.m_area >= 0.0001)
					formatf(strInfo, "%.6f", piece.m_area);
				else if(piece.m_area >= 0.00001)
					formatf(strInfo, "%.7f", piece.m_area);
				else
					formatf(strInfo, "%.9f", piece.m_area);
				grcDebugDraw::Text(centerPos, Color_green, strInfo); // Render the text
			}
		}
	}
	
	if(sm_bShowAnchoring)
	{
		const bgGeometryData& data = GetGeometryData();
		const PanePieces& panePieces = data.m_panePieces;
		const PaneEdges& paneEdges = data.m_paneEdges;
		const PanePoints& panePoints = data.m_panePoints;

		for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++)
		{
			const bgPanePiece& piece = panePieces[iPanePiece];
			if (piece.m_flags & bgPanePiece::kPF_Active)
			{
				// Draw the pieces
				for (int iEdge = 0; iEdge < piece.m_silhouette.GetCount();iEdge++)
				{
					bgEdgeRef edge = piece.m_silhouette[iEdge];
					const Vector2& p0 = panePoints[edge.GetP0(paneEdges)];
					const Vector2& p1 = panePoints[edge.GetP1(paneEdges)];

					// Draw the silhouette
					Vector3 p0WS = data.Transform(p0);
					m_transform.Transform(p0WS);
					Vector3 p1WS = data.Transform(p1);
					m_transform.Transform(p1WS);
					Color32 color(0, 255, 0, 255);
					bgIndex edgeIndex = edge.GetRight(paneEdges);
					if(edgeIndex == bgEdge::kPieceId_FrameTop || edgeIndex == bgEdge::kPieceId_FrameBottom ||
						edgeIndex == bgEdge::kPieceId_FrameLeft || edgeIndex == bgEdge::kPieceId_FrameRight)
					{
						color.Set(0, 0, 255, 255);
					}
					else if(edgeIndex == bgEdge::kPieceId_None)
					{
						color.Set(255, 0, 0, 255);
					}
					grcDebugDraw::Line(p0WS, p1WS, color); // Draw the silhouette
				}
			}
		}
	}

	if(sm_fLastKillRadius > 0.0f)
	{
		const bgGeometryData& data = GetGeometryData();
		float fWidth = Abs(data.GetPosWidth().x);
		float fHeight = Abs(data.GetPosHeight().z);
		Vec3V vHorAxix = RCC_VEC3V(m_transform.a) * ScalarV(fWidth);
		Vec3V vVerAxis = RCC_VEC3V(m_transform.c) * ScalarV(fHeight);
		if(sm_bShowHitKillRadius)
		{
			grcDebugDraw::Circle(sm_vLastHitPos, sm_fLastKillRadius, Color32(255, 0, 0, 255), vHorAxix, vVerAxis);
		}

		if(sm_bShowHitDecalRadius)
		{
			grcDebugDraw::Circle(sm_vLastHitPos, sm_fLastDecalRadius, Color32(0, 0, 255, 255), vHorAxix, vVerAxis);
		}

		if(sm_bShowHitCrackRadius)
		{
			grcDebugDraw::Circle(sm_vLastHitPos, sm_fLastCrackRadius, Color32(120, 120, 120, 255), vHorAxix, vVerAxis);
		}
	}
#endif // __BANK
}
#endif // __PFDRAW

//-----------------------------------------------------------------------------
// After impact, some piece must break and fly out of the pane
//
// Input: in_hitInfo:		The current hit
//
// return: true if the window was hit, false if the hit did not touched anything

//struct NewFallingPiece
//{
//	static std::size_t const max_size = 63;
//	float points[max_size][2];
//	std::size_t size;
//	float area;
//};
//
//class NewFallingPieces
//{
//public:
//	template<std::size_t size>
//	explicit NewFallingPieces(NewFallingPiece (&pieces)[size]) : m_pPieces(pieces), m_capacity(size), m_size(), m_free()
//	{
//	}
//
//	NewFallingPieces(NewFallingPiece *pPieces, std::size_t size) : m_pPieces(pPieces), m_capacity(size), m_size(), m_free()
//	{
//	}
//
//	NewFallingPiece &GetFreePiece() const
//	{
//		return m_pPieces[m_free];
//	}
//
//	void ReleaseFreePiece()
//	{
//		if(m_size < m_capacity)
//		{
//			++ m_size;
//			if(m_size != m_capacity)
//			{
//				++ m_free;
//				return;
//			}
//		}
//		for(std::size_t index = 0; index != m_size; ++ index)
//		{
//			if(m_pPieces[index].area < m_pPieces[m_free].area)
//				m_free = index;
//		}
//	}
//
//	std::size_t Size() const
//	{
//		return m_size == m_capacity ? m_size - 1 : m_size;
//	}
//
//	NewFallingPiece &operator [](std::size_t index)
//	{
//		return m_pPieces[index < m_free ? index : index + 1];
//	}
//
//	NewFallingPiece const &operator [](std::size_t index) const
//	{
//		return const_cast<NewFallingPieces &>(*this)[index];
//	}
//
//private:
//	NewFallingPiece *m_pPieces;
//	std::size_t m_capacity;
//	std::size_t m_size;
//	std::size_t m_free;
//};

bool bgBreakable::HitPane(bgHitInfo& in_hitInfo, bool bUseInitKillRadius)
{
	//Keep track of what piece to break off. We will make them fly out
	// at the end of the function once we have them all.  This need to be done that
	// way because when we are breaking pieces in smaller piece they can be added anywhere
	// in our pieces array and therefore we could end-up either processing twice the same piece 
	// or forgetting a few.
	atArray<int> brokenPieces;
	bgGeometryData& data = GetGeometryDataNonConst();
	PanePieces& panePieces = data.m_panePieces;
	FallingPieces& fallingPieces = data.m_fallingPieces;
	PanePoints& panePoints = data.m_panePoints;
	PaneEdges& paneEdges = data.m_paneEdges;
	atBitSet& pointDecalVisibility = data.m_pointDecalVisibility;
	atBitSet& pointCrackVisibility = data.m_pointCrackVisibility;
	const bgCrackType* pCrackType = in_hitInfo.GetCrackType(m_glassType, m_glassSizeIndex);
	brokenPieces.Reserve(panePieces.GetCount());

	const bgCrackType& glassCrack = *in_hitInfo.GetCrackType(m_glassType, m_glassSizeIndex);
	float killRadius;
	float initialKillRadius = sm_Rand.GetFloat() < 0.5f ? glassCrack.m_initialImpactKillRadius1 : glassCrack.m_initialImpactKillRadius2;
	if (bUseInitKillRadius)
	{
		killRadius = initialKillRadius;
	}
	else
	{
		float killRadiusMin = glassCrack.m_subsequentImpactKillRadiusMin;
		float killRadiusMax = glassCrack.m_subsequentImpactKillRadiusMax;
		killRadius = killRadiusMin + sm_Rand.GetFloat() * (killRadiusMax - killRadiusMin);
	}	
	float decalShowRadius = pCrackType->m_decalRadiusImpactShowValueMin +
		sm_Rand.GetFloat() * (pCrackType->m_decalRadiusImpactShowValueMax - pCrackType->m_decalRadiusImpactShowValueMin);
	float crackShowRaidus = pCrackType->m_crackRadiusImpactShowValueMin +
		sm_Rand.GetFloat() * (pCrackType->m_crackRadiusImpactShowValueMax - pCrackType->m_crackRadiusImpactShowValueMin);
#ifdef GLASS_PROFILING
	sysTimer timerBreak2;
#endif

#if __BANK
	sm_fLastKillRadius = sqrtf(killRadius);
	sm_fLastDecalRadius = sqrtf(decalShowRadius);
	sm_fLastCrackRadius = sqrtf(crackShowRaidus);
#endif // __BANK

	//Iterate through all pieces and see which should fall from the impact
	for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++)
	{
#if __BANK
		if(sm_bSkipHit)
		{
			// Don't add any of the broken pieces to the falling array
			break;
		}
#endif // __BANK

		bgPanePiece& piece = panePieces[iPanePiece];

		// Concave pieces that are large enough to receive a sequential hit should always use initial kill radius
		float fPieceKillRadius = killRadius;
		if(!bUseInitKillRadius && piece.m_area > pCrackType->m_minSequentialHitPieceSize && piece.m_polygons.GetCount() == 1 BANK_ONLY(&& ms_bUseInitialKillRadiusSeqPiece))
		{
			fPieceKillRadius = initialKillRadius;
		}

		// Iterate all edges and check impact-to-edge distance
		// to determine whether piece should be broken off and 
		// whether this hit should be used to determine decal coords
		bool bIsBroken = !(piece.m_flags & bgPanePiece::kPF_Active);
		bool bIsHit = bIsBroken || (-1 != piece.m_hit); // true if piece is already broken or decal coords already specified for this piece
		bool isPointInsidePolygon = false;
		for (int iEdge = 0; iEdge < piece.m_silhouette.GetCount();iEdge++)
		{
			//if piece is already hit and broken, there's nothing more to do
			if (bIsHit && bIsBroken)
			{
				break;
			}

			bgEdgeRef edge = piece.m_silhouette[iEdge];
			const Vector2& p0 = panePoints[edge.GetP0(paneEdges)];
			const Vector2& p1 = panePoints[edge.GetP1(paneEdges)];
			float fDist2 = PointToEdgeDist2(p0, p1, in_hitInfo.m_location);

			if ((!bIsHit) && (fDist2 <= decalShowRadius))
			{
				//if in radius, fix decal coordinates to this hit
				piece.m_hit = GetIndex(in_hitInfo);
				bIsHit = true;
			}

			if ((!bIsBroken) && (fDist2 <= fPieceKillRadius))
			{
				// An edge of this piece is within 'kill' radius; queue it up to be broken off the pane
				brokenPieces.Append() = iPanePiece;
				bIsBroken = true;
			}

			if (!bIsBroken)
			{
				// test if the impact location is within this piece by counting edge intersections
				CheckEdgeIntersection(p0, p1, in_hitInfo.m_location, isPointInsidePolygon);
			}
		}
		// Append piece if only edge intersected with the kill radius
		if (!bIsBroken && isPointInsidePolygon)
		{
			brokenPieces.Append() = iPanePiece;
		}
	}

	for (int iPanePoint=0; iPanePoint < panePoints.GetCount(); iPanePoint++)
	{
		const Vector2& point = panePoints[iPanePoint];
		//Visibility for crack
		if (point.Dist2(in_hitInfo.m_location) <= crackShowRaidus)
		{
			// point is close to impact location; show it.
			pointCrackVisibility.Set(iPanePoint);
		}

		//Visibility for decal
		if (point.Dist2(in_hitInfo.m_location) <= decalShowRadius)
		{
			// point is close to impact location; show it.
			pointDecalVisibility.Set(iPanePoint);
		}
	}

#ifdef GLASS_PROFILING
	m_profileBreakPieces2 = timerBreak2.GetMsTime();

	
	sysTimer timerBreak3;
#endif

	if (m_strength == STRENGTH_NORMAL)
	{
		//NewFallingPiece newFallingPieceArray[256];
		//NewFallingPieces newFallingPieces(newFallingPieceArray, std::min(sizeof(newFallingPieceArray) / sizeof(newFallingPieceArray[0]), std::size_t(m_FallingPieceMatrix.GetCapacity() - bgMatrixArray::FIRST_FALLING_PIECE + 1)));

		// Iterate through current falling pieces and reassign matrix indices
		int maxFallingPieces = m_FallingPieceMatrix.GetCapacity() - bgMatrixArray::FIRST_FALLING_PIECE;
		Assert(fallingPieces.GetCount() <= maxFallingPieces);
		bgPhysicsData& fallingPhysics = GetFallingPhysicsNonConst();
		int iFallingPiece;
		for (iFallingPiece = fallingPieces.GetCount()-1; iFallingPiece >= 0; iFallingPiece--)
		{
			bgFallingPiece& piece = fallingPieces[iFallingPiece];
			if (piece.m_life >= 0.f)
			{
				break; // keep this piece and all previous pieces.
			}
			else
			{
				// Make sure the physics we're discarding gets cleaned up
				fallingPhysics[iFallingPiece].Cleanup();
			}
		}
		int currentPieceCount = iFallingPiece + 1;
		bgAssertf(currentPieceCount <= fallingPieces.GetCount(), "Growing fallingPieces results in uninitialized data.  It should only shrink here!");
		fallingPieces.Resize(currentPieceCount);
		// add some room for the falling pieces array to grow into.
		{
			FallingPieces newFallingPieces;
			newFallingPieces.Reserve(maxFallingPieces);
			if (fallingPieces.GetCount() > 0)
			{
				newFallingPieces.CopyFrom(&fallingPieces[0], u16(fallingPieces.GetCount()));
			}
			fallingPieces.Assume(newFallingPieces);
		}
		// do the same for physics data
		{
		bgUseMemoryBucket physicsBucket(kPhysicsBucket);
		fallingPhysics.Resize(currentPieceCount);
		{
			bgPhysicsData newFallingPhysics;
			newFallingPhysics.Reserve(maxFallingPieces);
			int count = fallingPhysics.GetCount();
			newFallingPhysics.Resize(count);
			for (int i=0; i<count; i++)
			{
				newFallingPhysics[i] = fallingPhysics[i];
			}
			fallingPhysics.Assume(newFallingPhysics);
		}
		}

		int currentMatrixCount = currentPieceCount + bgMatrixArray::FIRST_FALLING_PIECE;
		m_FallingPieceMatrix.Resize(currentMatrixCount);

		//Before we loop and see if we either render a falling piece as it is or if we cut it into smaller pieces:  We need to check how many
		// pieces we can cut without going over our rendering limit.  The rendering code only have one pass and cannot go over SKINNING_COUNT pieces (all pieces on the pane count for 1).
		// So we calculate how many pieces we have before cutting.  If this number is lower than SKINNING_COUNT we have some room to cut.
		// 
		int convexPiecesCount = 0;
		for (int iBrokenPiece = 0;iBrokenPiece < brokenPieces.GetCount();iBrokenPiece++)
		{
			convexPiecesCount += panePieces[brokenPieces[iBrokenPiece]].m_polygons.GetCount();
		}

		int totalSkinningCount = SKINNING_COUNT - bgMatrixArray::FIRST_FALLING_PIECE;
		// Make sure templates are available.
		bgCracksTemplate* pCracksTemplate;
		const char* szErrMsg = NULL;

		if (Verifyf(bgCracksTemplate::IsInitialized(pCracksTemplate, &szErrMsg), "Breakable glass system not initialized: %s\n", szErrMsg))
		{
			const atArray<bgGlassTypeConfig>& glassTypes = pCracksTemplate->GetGlassTypes();
			totalSkinningCount = ((int)glassTypes[m_glassType].GetNumBatches()) * SKINNING_COUNT - bgMatrixArray::FIRST_FALLING_PIECE;
		}
		//Check how many pieces can be cut (remove one from out total because we need to count our glass shards remaining on the pane)
		int piecesAvailableToCutCount = 0;
		if (convexPiecesCount < totalSkinningCount)
		{
			piecesAvailableToCutCount = totalSkinningCount - convexPiecesCount;
		}

		// Calculate the size at which a piece would need to be cut into smaller pieces
		// The size is based on the squared diagnoal size of the pane and the pieces
		float sizePercent = sm_Rand.GetRanged(pCrackType->m_breakPieceSizeMin, pCrackType->m_breakPieceSizeMax);
		float fPaneWidth = GetGeometryData().m_posWidth.x;
		float fPaneHeight = -GetGeometryData().m_posHeight.z;
		float fPaneDiagSqr = fPaneWidth*fPaneWidth + fPaneHeight*fPaneHeight;
		float pieceSizeToCutSqr = fPaneDiagSqr * sizePercent;

		//1 - Make pieces flying out
		//2 - some piece will also break out
		const float threshold(sm_cullThreshold[std::max(0, 3 - GetLod())]);
		for (int iBrokenPiece = 0;iBrokenPiece < brokenPieces.GetCount();iBrokenPiece++)
		{
			if (sm_bKillBrokenPieces || sm_bSilentHit)
			{
				KillPanePiece(brokenPieces[iBrokenPiece]);
			}
			// Cut if we are allowed with our global bool, LOD is higher than very low, piece is large enough and if the rendering still has room for more pieces
			else if (sm_bCreateSmallerPieces && GetLod() > bgLod::LOD_VLOW && piecesAvailableToCutCount > 0  && panePieces[brokenPieces[iBrokenPiece]].m_area > threshold)
			{
				CreateSmallerPieces(brokenPieces[iBrokenPiece], brokenPieces.GetCount(), pieceSizeToCutSqr, &piecesAvailableToCutCount);
			}
			else// if (sm_bPieceFlyOutOnImpact)
			{
				// Don't break up the piece; just make it fall
				PieceFlyOutOnImpact(brokenPieces[iBrokenPiece], brokenPieces.GetCount());
			}
		}

#ifdef GLASS_PROFILING
		m_profileBreakPieces3 = timerBreak3.GetMsTime();


		//--------------------------
		//We are done with all the pieces flying, lets see if a piece is now alone ready to fall
		// TODO: There is a better way to handle that I am sure... I will take some time at some point to think about it

		sysTimer timerBreak4;
#endif
		// We need to go through all piece of glass and check if any are floating
		// if they are, make them fall.

		// first clear the 'visited' flag since it is now stale (we already made some pieces fall)
		for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++)
		{
			bgPanePiece& piece = panePieces[iPanePiece];
			piece.m_flags &= ~bgPanePiece::kPF_Visited;
		}

		// reuse brokenPieces for floating pieces
		brokenPieces.Resize(0);

		// gather up all the floating pieces...
		for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++)
		{
			bgPanePiece& piece = panePieces[iPanePiece];
			 // only check active pieces
			if (piece.m_flags & bgPanePiece::kPF_Active)
			{
				Assertf(!(piece.m_flags & bgPanePiece::kPF_Visiting), "Visiting flag was not cleared for active piece %i\n", iPanePiece);
				if ((piece.m_flags & bgPanePiece::kPF_Visited) == 0)
				{
					// Determine whether this piece is floating by recursively 
					// visiting neighbors until we hit the frame
					u32 floatFlag = IsFloating(iPanePiece) ? bgPanePiece::kPF_Floating : 0;

					// This will set the specified flag all pieces connected to this one;
					// it will also clear the PF_Visiting flag and set the PF_Visited flag
					SetFloatState(iPanePiece, floatFlag);
				}
				if (piece.m_flags & bgPanePiece::kPF_Floating)
				{
					brokenPieces.Grow() = iPanePiece;
				}
			}
		}

		// ...and make them fall or just kill them as appropriate
		//int count = sm_bKillBrokenPieces ? 0 : (std::min(brokenPieces.GetCount(), fallingPieces.GetCapacity() - m_fallen));
		int count = sm_bKillBrokenPieces ? 0 : brokenPieces.GetCount();
		for (int iBrokenPiece(0); iBrokenPiece != count; ++ iBrokenPiece)
		{
			//PanePoints const& panePoints = GetPanePoints();
			//bgPanePiece const& panePiece = GetPanePieces()[brokenPieces[iBrokenPiece]];
			//for(int iPolygon=0; iPolygon < panePiece.m_polygons.GetCount(); iPolygon++)
			//{
			//	bgPolygon const& polygon = panePiece.m_polygons[iPolygon];
			//	if(NewFallingPiece::max_size < polygon.GetCount())
			//		continue;
			//	NewFallingPiece &piece(newFallingPieces.GetFreePiece());
			//	piece.size = polygon.GetCount();
			//	piece.points[0][0] = panePoints[polygon[0]].x;
			//	piece.points[0][1] = panePoints[polygon[0]].y;
			//	piece.area = 0.f;
			//	for(std::size_t index = 1; index != piece.size; ++ index)
			//	{
			//		piece.points[index][0] = panePoints[polygon[index]].x;
			//		piece.points[index][1] = panePoints[polygon[index]].y;
			//		piece.area += panePoints[polygon[index-1]].Cross(panePoints[polygon[index]]);
			//	}
			//	piece.area += panePoints[polygon[piece.size-1]].Cross(panePoints[polygon[0]]);
			//	piece.area *= 0.5f;
			//	newFallingPieces.ReleaseFreePiece();
			//}

			PieceFlyOutOnImpact(brokenPieces[iBrokenPiece], brokenPieces.GetCount());
		}

		// Kill excess pieces
		for (int iBrokenPiece(count); iBrokenPiece != brokenPieces.GetCount(); ++ iBrokenPiece)
		{
			KillPanePiece(brokenPieces[iBrokenPiece]);
		}

#ifdef GLASS_PROFILING
		m_profileBreakPieces4 = timerBreak4.GetMsTime();
#endif
	}
	else
	{
		atArray<float>& paneDepths = data.m_paneDepths;
		//Push the glass at the impact point
		float maximum(sm_maxDepth);

		//Find push depth
		float depth(maximum * (1.f - 2.f * (in_hitInfo.m_location - Vector2(0.5f, 0.5f)).Mag()));
		if (depth > 0.f)
		{
			//Find push direction
			Vector3 normal;
			normal.Cross(GetGeometryData().m_posWidth, GetGeometryData().m_posHeight);
			float sign(normal.Dot(in_hitInfo.m_worldImpactImpulse) < 0.f ? -1.f : 1.f);

			//Find distance from impact point to nearest pane edge
			float radius(0.5f - std::max(abs(in_hitInfo.m_location.x - 0.5f), abs(in_hitInfo.m_location.y - 0.5f)));

			//Adjust the depth of all the points in all the pieces
			for (PanePieces::iterator begin(panePieces.begin()), end(panePieces.end()); begin != end; ++ begin)
			{
				bgPanePiece& piece(*begin);
				for (atArray<bgEdgeRef>::iterator begin(piece.m_silhouette.begin()), end(piece.m_silhouette.end()); begin != end; ++ begin)
				{
					Vector2& panePoint(panePoints[begin->GetP0(paneEdges)]);
					float& paneDepth(paneDepths[begin->GetP0(paneEdges)]);

					//Accumulate depth based on distance from impact, within limits
					float distance(1.f - (panePoint - in_hitInfo.m_location).Mag() / radius);
					distance = sign * depth * std::max(0.f, distance);
					paneDepth += (distance - paneDepth) / 2;
				}
			}
		}
		m_FallingPieceMatrix.Resize(REINFORCED_FALLING_COUNT);
	}
	m_lastFallenRatio = m_fallenRatio;
	m_lastCrackedRatio = m_crackedRatio;
	// Find ratios of areas of cracked and fallen pieces to total area
	float pristineRatio(0.f);
	m_crackedRatio = 0.f;
	for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++)
	{
		bgPanePiece& piece = panePieces[iPanePiece];
		for (int iEdge = 0; iEdge < piece.m_silhouette.GetCount();iEdge++)
		{
			bgEdgeRef const &edge(piece.m_silhouette[iEdge]);
			Vector2 const &p0(panePoints[edge.GetP0(paneEdges)]);
			Vector2 const &p1(panePoints[edge.GetP1(paneEdges)]);
			if (piece.m_hit == -1)
				pristineRatio += p0.Cross(p1);
			else
				m_crackedRatio += p0.Cross(p1);
		}
	}
	pristineRatio *= 0.5f;
	m_crackedRatio *= 0.5f;
	m_fallenRatio = m_strength != STRENGTH_NORMAL ? 0.f : 1.f - m_crackedRatio - pristineRatio;
	m_crackedRatio += m_fallenRatio;

	if (m_strength == STRENGTH_REINFORCED)
	{
		// Determine if the pane should fall
		if (m_health <= 0 && !m_fallen)
		{
			m_fallen = 1;

			// Apply impact impulse to pane
			float mass(sm_paneMass);
			const bgHitInfo& hit(*GetCurrentHit());
			m_pReinforced->m_life = sm_Rand.GetRanged(sm_minShardLife, sm_maxShardLife);
			Vector3 hitWorldLoc = Transform(hit.m_location);
			Vector3 paneCenter = Transform(Vector2(0.5f, 0.5f));
			const bgGeometryData& data = GetGeometryData();
			ApplyImpulse(m_pReinforced->m_physics, paneCenter, 
			             Vector3(data.m_posWidth.Mag(), data.m_posHeight.Mag(), GetThickness()), 
						 mass, hitWorldLoc, hit.m_worldImpactImpulse);

			// Find rotation axes for bending the pane.
			Vector3 *pAxes(m_pReinforced->m_axes);
			Vector3 normal;
			normal.Cross(GetGeometryData().m_posWidth, GetGeometryData().m_posHeight);
			Vector2 points[] = {
				Vector2( 1.f, 0.5f ), Vector2( 1.f, 0.f ),
				Vector2( 0.5f, 0.f ), Vector2( 0.f, 0.f ),
				Vector2( 0.f, 0.5f ), Vector2( 0.f, 1.f ),
				Vector2( 0.5f, 1.f ), Vector2( 1.f, 1.f )
			};
			for (size_t index(0); index != REINFORCED_FALLING_COUNT; ++ index)
			{
				Vector3 &axis(pAxes[index]);
				axis = Transform(points[index]) - paneCenter;
				axis.Cross(normal);
				axis.Normalize();
			}
		}
	}

	return true;
}

//
// Recursively set the 'IsFloating' flag to the specified value on this piece and 
// all connected pieces.
//
void bgBreakable::SetFloatState(int in_pieceIndex, u32 in_flags)
{
	PanePieces& panePieces = GetPanePiecesNonConst();
	PaneEdges& paneEdges = GetPaneEdgesNonConst();
	bgPanePiece& piece = panePieces[in_pieceIndex];
	if (piece.m_flags & bgPanePiece::kPF_Visited)
	{
		// already flagged this piece; make sure we did it correctly
		Assert((piece.m_flags & bgPanePiece::kPF_Visiting) == 0);
		Assert((piece.m_flags & bgPanePiece::kPF_Floating) == in_flags);
		return;
	}
	// clear the 'visiting' flag
	piece.m_flags &= ~bgPanePiece::kPF_Visiting;
	// Set the 'visited' flag, and the 'is floating' flag, if specified.
	piece.m_flags |= (bgPanePiece::kPF_Visited | in_flags);
	// flag all our neighbors as well
	for (int iEdge = 0;iEdge < piece.m_silhouette.GetCount();iEdge++)
	{
		bgEdgeRef edgeRef = piece.m_silhouette[iEdge];
		bgIndex neighbor = edgeRef.GetRight(paneEdges);
		if (neighbor < panePieces.GetCount())
		{
			SetFloatState(neighbor, in_flags);
		}
	}
}

//-----------------------------------------------------------------------------
// Determine whether a pane piece is floating or not.  A piece if floating if it is not directly or 
// indirectly connected to the window frame.  When called recursively, a 'true' result does
// not necessarily indicate this piece is actually floating, just that we haven't found the 
// frame yet.
//
bool bgBreakable::IsFloating(int in_pieceIndex)
{
	PanePieces& panePieces = GetPanePiecesNonConst();
	bgPanePiece& piece = panePieces[in_pieceIndex];

	// shouldn't ask about inactive pieces
	if (!Verifyf(piece.m_flags & bgPanePiece::kPF_Active, "Only active pieces should be queried for floating status!\n"))
		return true;

	if (!Verifyf(((piece.m_flags & bgPanePiece::kPF_Visited) == 0), 
		         "Float state already determined for this piece!\n"))
		return true;

	if (piece.m_flags & bgPanePiece::kPF_Visiting)
	{
		// we're visiting this piece recursively; we don't know yet
		// whether it is floating
		Assertf(!(piece.m_flags & bgPanePiece::kPF_Visited), "Visited flag should not be specified on pieces still determining floating status!");
		return true;
	}

	// Set the 'visiting' flag to avoid recursion with this piece while we're figuring things out.
	piece.m_flags |= bgPanePiece::kPF_Visiting; 

	// If we haven't been visited yet, we should not have been flagged 'floating'
	Assertf(!(piece.m_flags & bgPanePiece::kPF_Floating), "Unvisited piece is floating! It should have already been removed from the pane!");

	PaneEdges& paneEdges = GetPaneEdgesNonConst();
	// Check all our neighbors
	for (int iEdge = 0;iEdge < piece.m_silhouette.GetCount();iEdge++)
	{
		bgEdgeRef edgeRef = piece.m_silhouette[iEdge];
		bgIndex neighbor = edgeRef.GetRight(paneEdges);
		switch (neighbor)
		{
		case bgEdge::kPieceId_FrameTop:
			if (0 == (m_BrokenFrameFlags & kFF_Top))
			{
				return false;
			}
			break;

		case bgEdge::kPieceId_FrameBottom:
			if (0 == (m_BrokenFrameFlags & kFF_Bottom))
			{
				return false;
			}
			break;
		case bgEdge::kPieceId_FrameLeft:
			if (0 == (m_BrokenFrameFlags & kFF_Left))
			{
				return false;
			}
			break;
		case bgEdge::kPieceId_FrameRight:
			if (0 == (m_BrokenFrameFlags & kFF_Right))
			{
				return false;
			}
			break;
		default:
			if (neighbor < panePieces.GetCount() &&
				!IsFloating(neighbor))
			{
				return false;
			}
			break;
		}
	}
	return true;
}

void bgBreakable::BreakFrame(int in_CrackType, u32 in_BrokenFrameFlags, Vec3V_In in_worldImpulse)
{
	m_BrokenFrameFlags |= in_BrokenFrameFlags;
	// auto-generate an impact location
	float x = sm_Rand.GetFloat();
	float y = sm_Rand.GetFloat();

	if (in_BrokenFrameFlags & kFF_Top)
	{
		// weight the top edge
		float w = 0.5f * y;
		y = w*w*w;
	}
	else if (in_BrokenFrameFlags & kFF_Bottom)
	{
		float w = 0.5f * y;
		y = 1.f - w*w*w;
		// weight the bottom edge
	}
	if (in_BrokenFrameFlags & kFF_Left)
	{
		// weight the left edge
		float w = 0.5f * x;
		x = w*w*w;
	}
	else if (in_BrokenFrameFlags & kFF_Right)
	{
		// weight the right edge
		float w = 0.5f * x;
		x = 1.f - w*w*w;
	}

	HitInternal(in_CrackType, Vec2V(x, y), in_worldImpulse, 0);
}

//-----------------------------------------------------------------------------
//Make one piece flying out of the pane
//
// WARNING: The function use the latest impact info m_hit(last)
//
// Input:	in_piece:				Pane Piece we want flying out
//
void bgBreakable::PieceFlyOutOnImpact(int in_iPanePiece, int in_brokenPiecesCount)
{
	PanePoints& panePoints = GetPanePointsNonConst();
	bgPanePiece& panePiece = GetPanePiecesNonConst()[in_iPanePiece];
	// first create a new falling piece for each convex polygon
	Vector2 polygonPoints[SmallPiecesMaxVertexPerPiece];
	for (int iPolygon=0; iPolygon < panePiece.m_polygons.GetCount(); iPolygon++)
	{
		bgPolygon& polygon = panePiece.m_polygons[iPolygon];
		if (SmallPiecesMaxVertexPerPiece < polygon.GetCount())
			continue;
		for (int i=0;i<polygon.GetCount();i++)
		{
			polygonPoints[i] = panePoints[polygon[i]];
		}
		if (Verifyf(polygon.GetCount() >= 3, "Polygon has %i points! That can't be right!", polygon.GetCount()))
		{
			CreateOneFallingPiece(panePiece, polygonPoints, polygon.GetCount(), in_brokenPiecesCount);
		}
	}

	// We've created all the falling pieces, now we can kill this pane piece
	KillPanePiece(in_iPanePiece);
}


//-----------------------------------------------------------------------------
//When a state change, the geometry has to be updated.  The following happen:
//	1 - Previous to this function, all data must have been updated properly.  That generally
//		means all 2D triangles and other pieces/cracks data must have been updated and ready to be use.
//
//	2 - This function will call multiple function to create different geometry:
//		2.A - 3D Cracks
//		2.B - 3D pieces
//
void bgBreakable::UpdateBuffers()
{
	if (m_bUpdateBuffers && (GetGeometryData().GetHitCount() > 0))
	{
		// (re)create the actual vertex and index buffers used to draw the broken glass
		m_pBuffers = bgGeometryBuilder::CreatePane(m_BreakableData.GetGeometryData(), (u32)m_BreakableData.GetGeometryDataSize(), m_fvf, m_PieceIndexCount, m_CrackIndexCount);
		// Use this to keep track if there is some geometry assigned
		m_bHasAssignedBuffers = (m_pBuffers != NULL);
		m_bHasUpdatedBuffers = true;
		m_bUpdateBuffers = false;
	}
}

bool bgBreakable::GetUpdatedBuffers(atPtr<bgGpuBuffers>& out_pBuffers)
{
	if (!m_bHasUpdatedBuffers)
	{
		return false;
	}

	out_pBuffers = m_pBuffers;
	m_pBuffers = NULL;
	m_bHasUpdatedBuffers = false;
	return true;
}

//-----------------------------------------------------------------
// Break a piece of glass into smaller pieces
//
// Input:	in_iPanePiece:					Index in array of pieces of the piece to cut
//			in_brokenPiecesCount:			total pieces that are falling.  This does not include the one that will be cut into smaller one
//			in_pieceSizeTriggerCut:			If a piece is equal or bigger then this size we will try to cut the piece smaller
//			inOut_piecesToCutReamining:		This is the remaining number of slot available for new pieces created when we cut.  If this reach 0
//											it means that we cannot cut anymore.
//
void bgBreakable::CreateSmallerPieces(int in_iPanePiece, int in_brokenPiecesCount, float in_pieceSizeTriggerCutSqr, int* inOut_piecesToCutReamining)
{
	PanePieces& panePieces = GetPanePiecesNonConst();
#ifdef GLASS_PROFILING
	sysTimer timerBreak5;
#endif
		bgPolygonDicer::Cut(GetCurrentHit()->m_location, panePieces[in_iPanePiece].m_polygons, GetPanePoints(), in_pieceSizeTriggerCutSqr, inOut_piecesToCutReamining);
#ifdef GLASS_PROFILING
	m_profileBreakPieces5 = timerBreak5.GetMsTime();
	
	
	//Set timer
	m_profileBreakPieces7 = 0;
	m_profileBreakPieces8 = 0;
	m_profileBreakPieces9 = 0;
	m_profileBreakPieces10 = 0;
	m_profileBreakPieces11 = 0;
	m_profileBreakPieces12 = 0;
	m_profileBreakPieces13 = 0;
	
	
	sysTimer timerBreak6;
#endif
	//We accumulated a bunch of pieces to create, now we must create the actual 2D piece to be later render
	for (int i = 0;i < bgPolygonDicer::GetPieceCount();i++)
	{
		//Get the piece
		bgPolygonDicer::CutPiece& piece = bgPolygonDicer::GetPiece(i);
		//If this piece have been cut, we do not create it
		if (!piece.m_use)
			continue;
		
		// Create the piece, replacing an older piece if necessary
		CreateOneFallingPiece(panePieces[in_iPanePiece], piece.m_point, piece.m_pointCount, in_brokenPiecesCount * bgPolygonDicer::GetPieceCount());
	}
#ifdef GLASS_PROFILING
	m_profileBreakPieces6 = timerBreak6.GetMsTime();
#endif

	//We are all done, make this current piece inactive because we just replaced it.
	// Also release all associated memory and fixup edges to no longer point to it
	KillPanePiece(in_iPanePiece);
}

//-----------------------------------------------------------------
// Create a falling piece from a set of points.  
// Called when a pane piece is directly hit and broken into smaller pieces _or_
// when floating piece(s) fall.
//
// Input:	in_points				Set of points to create the piece
//			in_pointCount			Point count in in_points
//          in_imapctPos            position of the impact that caused this piece to fall
//
void bgBreakable::CreateOneFallingPiece(const bgPanePiece& panePiece,
										const Vector2* in_points, 
										int in_pointCount,
										int in_brokenPiecesCount)
{
	// Find the area of the new piece
	float left, top, right, bottom;
	FindBounds(in_points, in_pointCount, left, top, right, bottom);
	Vector2 rightTop(right, top), leftBottom(left, bottom);
	float boundAreaSqr((rightTop - leftBottom).Mag2());
	float threshold(sm_cullThreshold[std::max(0, 3 - GetLod())]);
	if (boundAreaSqr < threshold * threshold)
		return;

	// Find the volume of the new piece
	Vector3 size(Transform(rightTop) - Transform(leftBottom));

	m_rotation.Transform(size);
	size.Abs();
	if (!(size[0] > 0.f) || !(size[1] > 0.f))
		return;
	size[2] = GetThickness();

	// Compute area (no need to find differences when points are coplanar with origin)
	float area = in_points[in_pointCount-1].Cross(in_points[0]);
	for (int iEdge = 1; iEdge < in_pointCount;iEdge++)
		area += in_points[iEdge-1].Cross(in_points[iEdge]);
	area *= 0.5f;
	if(area < threshold) // Get rid of small pieces
	{
		return;
	}

	FallingPieces& fallingPieces = GetFallingPiecesNonConst();

	//Prevent overwriting pieces created by the current hit
	//if (m_fallen == fallingPieces.GetCapacity())
	//	return;
	//++ m_fallen;

	if (m_fallen != fallingPieces.GetCapacity())
		++ m_fallen;

	//---------------------
	//Try to find a piece available to replace
	//
	int bestPieceIndex = fallingPieces.GetCount();
	float bestPieceArea = area;
	float bestPieceLife = FLT_MAX;
	for (int pieceIndex = 0; pieceIndex != fallingPieces.GetCount(); ++ pieceIndex)
	{
		bgFallingPiece& piece = fallingPieces[pieceIndex];

		//The piece is already dead: this is the best candidate
		if (piece.m_life < 0.f)
		{
			bestPieceIndex = pieceIndex;
			bestPieceArea = piece.m_area;
			bestPieceLife = piece.m_life;
			break;
		}

		//If a piece is over 3 seconds old we probably do not see it anymore: This is the best candidate
		if (piece.m_originalLife-piece.m_life >= 3.0f)
		{
			bestPieceIndex = pieceIndex;
			bestPieceArea = piece.m_area;
			bestPieceLife = piece.m_life;
			break;
		}

		//If our life is under 3 second we are going to make a piece disappear for sure.  Lets chose the smaller one
		if (piece.m_area < bestPieceArea)		
		{
			bestPieceIndex = pieceIndex;
			bestPieceArea = piece.m_area;
			bestPieceLife = piece.m_life;
		}
	}
	int iOldestPiece = bestPieceIndex < fallingPieces.GetCount() ? bestPieceIndex : -1;
	float oldestLife = bestPieceLife; // how long the oldest piece has left to live

	//int iOldestPiece = -1;
	//float oldestLife = FLT_MAX; // how long the oldest piece has left to live
	//for (int iPiece = 0; iPiece < fallingPieces.GetCount();iPiece++)
	//{
	//	fallingPieces[iPiece].m_area;
	//	fallingPieces[iPiece].m_life;

	//	float life = fallingPieces[iPiece].m_life;
	//	if (oldestLife > life)
	//	{
	//		oldestLife = life;
	//		iOldestPiece = iPiece;
	//		if (life <= 0.f)
	//		{
	//			// No need to look further...
	//			break;
	//		}
	//	}
	//}

	bgPhysicsData& fallingPhysics = GetFallingPhysicsNonConst();
	if (oldestLife > 0.f)
	{
		// the oldest piece is still active, try to add a new one instead of recycling
		if (fallingPieces.GetCount() < fallingPieces.GetCapacity())
		{
			iOldestPiece = fallingPieces.GetCount();
			fallingPieces.Append();
			fallingPhysics.Append();
		}
	}
	if (iOldestPiece < 0)
	{
		return;
	}

	// Cleanup the physics of the piece we're going to replace
	fallingPhysics[iOldestPiece].Cleanup();

	//Matrix we will use to transform our piece in the shader
	if (m_FallingPieceMatrix.GetCount() <= bgMatrixArray::FIRST_FALLING_PIECE + iOldestPiece)
		m_FallingPieceMatrix.Resize(1 + bgMatrixArray::FIRST_FALLING_PIECE + iOldestPiece);

	// recycle/init
	bgFallingPiece& piece = fallingPieces[iOldestPiece];

	// ensure that CopyFrom() will succeed...
	if (piece.m_points.GetCapacity() < in_pointCount)
		piece.m_points.Reset();
	piece.m_points.CopyFrom(in_points, u16(in_pointCount));

	//We found our BBX, lets find the center point and extents
	piece.m_center.x = (left + right) * 0.5f;
	piece.m_center.y = (top + bottom) * 0.5f;
	static const Vector2 VEC2_HALF(0.5f, 0.5f);
	piece.m_extents.x = size.x * 0.5f;
	piece.m_extents.y = size.y * 0.5f;
	piece.m_massCenteredTranslation = Transform(piece.m_center);

	bgHitInfo const &hit(*GetCurrentHit());
	Vector3 impact(hit.m_worldImpactImpulse);
/*
	//Discard impact direction
	Vector3 normal;
	normal.Cross(GetGeometryData().m_posWidth, GetGeometryData().m_posHeight);
	float sign(normal.Dot(impact) < 0.f ? -1.f : 1.f);
	impact = normal * std::sqrt(impact.Mag2() / normal.Mag2()) * sign;
*/
	//Bend impact impulse
	float cosine(1 - 0.5f * hit.m_location.Dist2(piece.m_center));
	float cosine2(cosine * cosine);
	impact *= cosine2 / in_brokenPiecesCount;
	if( 0.f < cosine && cosine < 1.f )
	{
		Vector3 axis(impact);
		Vector3 difference(Vector3(Transform(piece.m_center)) - Vector3(Transform(hit.m_location)));
		GetTransform().Transform3x3(difference);
		axis.Cross(difference);
		if(float mag2 = axis.Mag2())
		{
			axis *= sqrt((1 - cosine2) / mag2);
			Quaternion(axis[0], axis[1], axis[2], cosine).Transform(impact);
		}
	}

	//Copy hit
	piece.m_hit = panePiece.m_hit;

	//Set area
	piece.m_area = area;

	//Apply impulse to piece
	//ApplyImpulse(piece, &(*m_pInstArray)[iOldestPiece], piece.m_massCenteredTranslation, size, 1.f, Transform(hit.m_location), impact, area * area);
	// determine shard lifetime	

	//Set its life
	piece.m_originalLife = sm_Rand.GetRanged(sm_minShardLife, sm_maxShardLife);
	piece.m_life = piece.m_originalLife;

	ApplyImpulse(fallingPhysics[iOldestPiece], piece.m_massCenteredTranslation, size, std::max(sm_minShardMass, m_mass * area), Transform(hit.m_location), impact, area * area);
}

/*
class bgPhysicsCreator : public bgPhysics
{
public:
	bgPhysicsCreator(bgBreakable &breakable,
					 bgBreakable::FallingPhysics &physics,
					 phInst *pInst,
					 Vector3 const &size,
					 float mass,
					 Matrix34 const &transform,
					 Vector3 const &impulse,
					 Vector3 const &position,
					 bool useRage) : m_pBreakable(&breakable),
									 m_pPhysics(&physics),
									 m_transform(transform),
									 m_size(size),
									 m_impulse(impulse),
									 m_position(position),
									 m_pInst(pInst),
									 m_mass(mass),
									 m_useRage(useRage)
	{
	}

	//These are used internally by breakable glass
	void Activate()
	{
	}

	Matrix34 Update(float)
	{
		//Create physics
		bgBreakable &breakable(*m_pBreakable);
		bgBreakable::FallingPhysics &physics(*m_pPhysics);
		if (m_useRage && m_pInst)
			physics.m_pPhysics = rage_new bgRagePhysics(*m_pInst, m_size, m_mass, m_transform, m_impulse, m_position);
		else
			physics.m_pPhysics = rage_new bgSimplePhysics(m_size, m_mass, m_transform, m_impulse, m_position);
		breakable.ModifyPhysicsLife(*physics.m_pPhysics, physics.m_life);
		if (physics.m_life > 0.f)
			physics.m_pPhysics->Activate();
		return physics.m_pPhysics->GetTransform();
	}

	int GetStorage() const
	{
		return int(sizeof(*this));
	}

	//These can be used externally through functors
	float GetMass() const
	{
		return m_mass;
	}

	Vector3 GetSize() const
	{
		return m_size;
	}

	Matrix34 GetTransform() const
	{
		return m_transform;
	}

	void ApplyImpulse(Vector3 const &impulse, Vector3 const &position)
	{
		m_impulse += impulse;
		m_position = (m_position + position) * 0.5f;
	}

	void SetMass(float mass)
	{
		m_mass = mass;
	}

	void SetTransform(Matrix34 const &transform)
	{
		m_transform = transform;
	}

private:
	bgBreakable *m_pBreakable;
	bgBreakable::FallingPhysics *m_pPhysics;
	phInst *m_pInst;
	Matrix34 m_transform;
	Vector3 m_size;
	Vector3 m_impulse;
	Vector3 m_position;
	float m_mass;
	bool m_useRage;
};
*/

//-----------------------------------------------------------------
// Apply impulse to falling physics
//
// Input:	in_physics				bgFallingPhysics object that receives the impulse
//			in_inst					Physics instance to associate with the FallingPhysics
//			in_center				Pane-space center of the piece
//			in_size					Dimensions of the piece in the standard basis
//			in_mass					Piece's mass
//			in_impulseLocation		Pane-space location at which to apply the impulse
//			in_worldImpactImpulse	World-space impulse to apply
//
void bgBreakable::ApplyImpulse(bgFallingPhysics &in_physics, Vector3::Param in_center, Vector3::Param in_size, float in_mass, Vector3::Param in_impulseLocation, Vector3::Param in_worldImpactImpulse, float area2)
{
//	if (in_physics.m_pPhysics)
//		m_oldPhysics.Grow() = in_physics.m_pPhysics;

	//Create transform from pane- to AABB-space
	Matrix34 transform;
	transform.a = m_rotation.a;
	transform.b = m_rotation.b;
	transform.c = m_rotation.c;
	transform.d = in_center;
	transform.Transpose();
	transform.Dot(GetTransform());

	// Orthonormalize the matrix since m_rotation is often not orthonormal
	transform.b.Normalize();
	transform.c.Cross(transform.a, transform.b);
	transform.c.Normalize();
	transform.a.Cross(transform.b, transform.c);

	//Find impulse location
	Vector3 worldLocation(in_impulseLocation);
	GetTransform().Transform(worldLocation);

	//Create physics
	float threshold(sm_physicsThreshold[std::max(0, 3 - GetLod())]);
	bgUseMemoryBucket physicsBucket(kPhysicsBucket);
	bgPhysics* physics;

	Vector3 impulse = in_worldImpactImpulse;
	if (!sm_alwaysUseSimplePhysics && !bgRagePhysics::PoolIsFull() && (sm_neverUseSimplePhysics || area2 > threshold * threshold))
	{
		impulse *= SCALARV_TO_VECTOR3(ScalarVFromF32(sm_physicsImpulseRatio));
		physics = rage_aligned_new(16) bgRagePhysics(in_size);
	}
	else
	{
		physics = rage_aligned_new(16) bgSimplePhysics(in_size);
	}

	physics->Init(in_mass, transform, impulse, worldLocation);

	in_physics.m_pPhysics = physics;
}

//-----------------------------------------------------------------
//
// Convert the crack mesh into bgMesh so we can clip it.
//
//
void bgBreakable::ConvertCrackMesh(bgMesh& out_dest, const bgHitInfo& in_hitInfo, int in_glassType, int in_glassSizeIndex)
{
	// just copy points directly
	const bgCrackMesh& crackMesh = *in_hitInfo.GetCrackStarMap(in_glassType, in_glassSizeIndex)->m_pCrackMesh;
	CopyPoints(out_dest, crackMesh.m_points, in_hitInfo);
	
	// copy polygons; insert edges as necessary
	typedef rage::bgEdgeAdder<bgMesh> MeshEdgeAdder;
	MeshEdgeAdder adder(out_dest);
	out_dest.ReserveEdgeCount(bgMesh::Index(crackMesh.GetEdgeCount()));
	for (int iPiece=0; iPiece < crackMesh.m_pieces.GetCount(); iPiece++)
	{
		const bgOneSolidPiece& piece = crackMesh.m_pieces[iPiece];
		// for each polygon in the piece, create or add edges as necessary
		for (int iPoly=0; iPoly<piece.m_polygons.GetCount(); iPoly++)
		{
			const bgPolygon& poly = piece.m_polygons[iPoly];
			int nPoints = poly.GetCount();
			bgMesh::Index iNewPoly(out_dest.AddPolygon(iPiece));
			out_dest.ReservePolygonEdgeCount(iNewPoly, bgMesh::Index(poly.GetCount()));
			for (int iPrevPoint = nPoints - 1, iPoint = 0; iPoint< nPoints; iPrevPoint = iPoint, iPoint++)
			{
				out_dest.AddPolygonEdge(iNewPoly, adder.AddOrUpdateEdge(poly[iPrevPoint], poly[iPoint], iNewPoly));
			}
		}
	}
}

//-----------------------------------------------------------------
//
// Recursively construct a pane piece starting from a single polygon from the clipped archetype mesh.
// All neighboring polygons with the same piece id will be merged into a single piece.
//
void bgBreakable::ConstructPanePiece(BreakableEdgeAdder &adder, u16 in_panePieceIndex, u16 in_iPoly, bgEdgeRef startEdge, bgMesh& in_mesh, atArray<u16>& in_polyList, u16 iPointOffset)
{
	bgMesh::Polygon const& poly = in_mesh.GetPolygon(in_iPoly);
	bgPanePiece& panePiece = GetPanePiecesNonConst()[in_panePieceIndex];
	bgPolygon& panePoly = panePiece.m_polygons.Grow();

	//Maintain polygon ID 
	int polyID = panePiece.m_polygons.GetCount() - 1;
	// save the original piece id.
	int pieceId = poly.PieceId();
	// flag this polygon as being visited so we don't recurse to it
	in_mesh.PolygonPieceId(in_iPoly, kPieceId_Visiting);
	in_polyList.Grow() = in_iPoly;
	// walk the polygon until we get to the starting edge
	int iEdge=0;
	int nEdges=poly.GetEdgeCount();
	for (; iEdge<nEdges; iEdge++)
	{
		if (poly[iEdge] == startEdge)
			break;
	}
	Assertf(iEdge<nEdges, "starting edge not found in polygon!");

	float area = 0.0f;
	panePoly.Reserve(nEdges);
	int iFinalEdge = iEdge;
	// walk the edges and add silhouette edges/recurse as necessary;
	const PanePoints& panePoints = GetPanePoints();

	//On subsequent shots, we discard edges that end up being too close to one of the existing verts
	//We should make sure to discard the points in the poly also. This can be done by keeping track of
	//what the previous point added was, and skip adding it if it was the same. Last point should be 
	//skipped if the first point is the same
	u16 prevP1 = u16(-1);
	do
	{
		iEdge++;
		if (iEdge == nEdges)
			iEdge = 0;

		bgMesh::EdgeRef edge = poly[iEdge];
		u16 p1 = in_mesh.GetP1(edge) + iPointOffset;
		BG_DEBUG_PRINT_ONLY(u16 p1_debug = p1;)

		// For sequential hits, search for a duplicate vertex reference
		static const float fEpsilon = 0.000000001f; //1e-9
		//static const float fEpsilon = 1e-10f;
		if(iPointOffset > 0)
		{
			// See we can reuse points from previous hits
			const Vector2& vP1 = panePoints[p1];
			for(u16 iCurIdx = 0; iCurIdx < iPointOffset; iCurIdx++)
			{
				const Vector2& vCurSil = panePoints[iCurIdx];
				if(vCurSil.Dist2(vP1) < fEpsilon)
				{
					p1 = iCurIdx;
					break;
				}
			}
		}

		//Skip adding the point if its the same as previous point or
		//same as the first point (if its the last point)
		if(p1!=prevP1 && (iEdge != iFinalEdge || p1 != panePoly[0]))
		{
			panePoly.Append() = p1;
		}

		prevP1 = p1;
		BG_DEBUG_PRINT_ONLY(Displayf("Adding index %d into Poly %d", p1, polyID);)

		u16 iOtherPoly = in_mesh.GetRightPoly(edge);
		if (iOtherPoly != bgMesh::Edge::kPolygonRef_None)
		{
			bgMesh::Polygon const& otherPoly = in_mesh.GetPolygon(iOtherPoly);
			if (otherPoly.PieceId() == kPieceId_Visiting)
			{
				// already visited 'other', and it's part of this piece,
				// so don't add this edge to the silhouette
				continue;
			}
			if (otherPoly.PieceId() == pieceId)
			{
				// other poly shares this edge so recurse to it; flip edgeref
				ConstructPanePiece(adder, in_panePieceIndex, iOtherPoly, edge.Flip(), in_mesh, in_polyList, iPointOffset);
				// and this is not a silhouette edge...
				continue;
			}
		}
		// this is a silhouette edge; add it...
		u16 p0 = in_mesh.GetP0(edge) + iPointOffset;
		BG_DEBUG_PRINT_ONLY(u16 p0_debug = p0;)

		bool bAddEdge = true;
		// For sequential hits, search for a duplicate vertex reference
		if(iPointOffset > 0)
		{
			// See if we can reuse points from previous hits
			const Vector2& vP0 = panePoints[p0];
			for(u16 iCurIdx = 0; iCurIdx < iPointOffset; iCurIdx++)
			{
				const Vector2& vCurSil = panePoints[iCurIdx];
				if(vCurSil.Dist2(vP0) < fEpsilon)
				{
					//if both points are close to an existing point, we
					//can skip adding the edge.
					if(p1 == iCurIdx)
					{
						bAddEdge = false;
					}
					else
					{
						p0 = iCurIdx;
						break;
					}
				}
			}
		}

		if(bAddEdge)
		{
			if(iPointOffset > 0 && !adder.EdgeFree(p0, p1))
			{				
				p0 = in_mesh.GetP0(edge) + iPointOffset;
			}

			bgEdgeRef edge01 = adder.AddOrUpdateEdge(p0, p1, in_panePieceIndex);
#if BG_DEBUG_PRINT
			Displayf("Adding/Updating Edge");
			Displayf("Initial P0 = %d, P0 = %d, P0 in Edge = %d, Initial P1 = %d, P1 = %d, P1 in Edge = %d, Edge Ref Index = %d, Edge Ref = %d", 
				p0_debug, p0, edge01.GetP0(GetPaneEdges()), p1_debug, p1,
				edge01.GetP1(GetPaneEdges()),
				panePiece.m_silhouette.GetCount(),
				edge01.GetEdgeIndex()
				);
#endif
			panePiece.m_silhouette.Grow() = edge01;

			// Area computation
			const Vector2& vP0 = panePoints[edge01.GetP0(GetPaneEdges())];
			const Vector2& vP1 = panePoints[edge01.GetP1(GetPaneEdges())];

			area += vP0.Cross(vP1);
		}

	} while (iEdge != iFinalEdge);

	//Because we start skipping edges, there are times when the poly gets
	//less than 3 verts. We can safely delete these polys as they end up
	//having zero area
	if(panePoly.GetCount() < 3)
	{
		//once we process the polygon, check to see if it has atleast 3 vertices. If not, get rid of it
		panePiece.m_polygons.DeleteFast(polyID);
	}

	panePiece.m_area += area;

#if BG_DEBUG_PRINT
	Displayf("Area of Polygon %d: %3.20f", polyID, area);
	Displayf("For Pane Piece %d: ", in_panePieceIndex);
	Displayf("Edges:");
	for(int iDebug = 0; iDebug < panePiece.m_silhouette.GetCount(); iDebug++)
	{
		bgEdgeRef edgeDebug = panePiece.m_silhouette[iDebug];
		Displayf("Point 0 = %d, Point 1 = %d", edgeDebug.GetP0(GetPaneEdges()), edgeDebug.GetP1(GetPaneEdges()));
	}
#endif
}


#if BG_DEBUG_PRINT
//This function is used for verifying if all the piece silhouette indices match up with it's polygon indices
void VerifyPanePieces(atArray<bgPanePiece>& panePieces, const bgBreakable::PaneEdges& paneEdges, int iOrigPointCount, const bgBreakable::PanePoints& panePoints, int iPanePiece) 
{
	static atFixedArray< bgIndex, 2048 > myPanePointMap;

	//for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++)
	{
		const bgPanePiece& piece = panePieces[iPanePiece];
		//if (piece.m_silhouette.GetCount() < 3)
			//continue;

		int pointCount = piece.m_silhouette.GetCount();

		myPanePointMap.Resize(0);

		for (int iPoint = 0; iPoint < pointCount; iPoint++)
		{
			// map shared point into the section of the vertex buffer used by this piece
			bgIndex pointIndex = piece.m_silhouette[iPoint].GetP0(paneEdges);
			myPanePointMap.Push(pointIndex);
		}


		for (int iPolygon = 0; iPolygon < piece.m_polygons.GetCount(); iPolygon++)
		{
			// convert convex polygons to trifans
			const bgPolygon& polygon = piece.m_polygons[iPolygon];
			int polygonIndexCount = polygon.GetCount();
			Assertf(polygonIndexCount >= 3, "Polygons must reference at least three vertices!\n");

			for (int iIndex = 0; iIndex < polygonIndexCount; iIndex++)
			{
				bgIndex nPanePointMapIndexN = polygon[iIndex];
				Vector2 currentPoint = panePoints[nPanePointMapIndexN];
				if( 0 > myPanePointMap.Find(nPanePointMapIndexN) )
				{

					Displayf("----------------------------------------------------------------------------------------------------------------");
					Displayf("Original Point Count %d", iOrigPointCount);
					Displayf("Looking for %d", nPanePointMapIndexN);
					Displayf("List of indices in Point Map:");
					for(int iDebug=0; iDebug<myPanePointMap.GetCount(); iDebug++)
					{
						float dist = currentPoint.Dist(panePoints[myPanePointMap[iDebug]]);
						float dist2 = currentPoint.Dist2(panePoints[myPanePointMap[iDebug]]);
						Displayf("%d \t %.20f \t\t %.20f", myPanePointMap[iDebug], dist, dist2);
					}
					Displayf("");
					Displayf("");
					Assertf(false,"Couldnt find index in Point Map");
				}
			}
		}
	}
}
#endif

//-----------------------------------------------------------------
// When a hit happens for the first time, the pane is broken into smaller 
// pieces that might not all be visible yet but nevertheless are there for future breaking...
//
// Input: in_hitInfo:  Data about the hit coming from artist template library
//
void bgBreakable::Create2DDataFromHit(const bgHitInfo& in_hitInfo)
{
	const bgCrackType* pCrackType = in_hitInfo.GetCrackType(m_glassType, m_glassSizeIndex);

	bgGeometryData& data = GetGeometryDataNonConst();
	data.m_bevelSize = pCrackType->m_bevelSizeMax;
	data.m_crackSize = pCrackType->m_crackInBetweenPiecesSizeMax;
	float minThickness = pCrackType->m_minThickness * GetPaneSize();
	if (data.m_thickness < minThickness)
	{
		data.m_thickness = minThickness;
	}
#if __BANK
	if(sm_bNoThickness)
	{
		data.m_thickness = 0.0f;
	}
#endif // __BANK

	// First, we need to take the archetype mesh and clip it by the pane.
	bgMesh mesh;
	{
		ConvertCrackMesh(mesh, in_hitInfo, m_glassType, m_glassSizeIndex);

		//We might want to not clip for debugging reason:  This would allow us to see the entire crack map on screen
		bool	m_clippingOn = true;
		BANK_ONLY(m_clippingOn = sm_crackMapClipOnPane;)
		if (m_clippingOn)
		{
			// @TODO: support arbitrary silhouette
			Vector2 silhouette[] = {
				Vector2(0.f, 0.f),
				Vector2(1.f, 0.f),
				Vector2(1.f, 1.f),
				Vector2(0.f, 1.f)
			};
			bgClipMesh(mesh, silhouette);
		}
		
	}
	bgPhysicsData& fallingPhysics = GetFallingPhysicsNonConst();
	fallingPhysics.Reset();
	mesh.SwapPoints(data.m_panePoints);

	PanePieces& panePieces = data.m_panePieces;

	// iterate through clipped polys and glom together into pieces where possible
	bgMesh::Index nPolys = bgMesh::Index(mesh.GetPolygonCount());
	BreakableEdgeAdder adder(*this);
	atArray<bgMesh::Index> polyList; // temp list of visited polgons
	polyList.Reserve(nPolys);
	for (bgMesh::Index iPoly=0; iPoly<nPolys; iPoly++)
	{
		bgMesh::Polygon const& poly = mesh.GetPolygon(iPoly);
		// skip polygons that have already been processed
		if (poly.PieceId() == kPieceId_Visited)
		{
			continue;
		}

		Assert(poly.PieceId() >= 0);

		u16 panePieceIndex = u16(panePieces.GetCount());

		// Ensure there is room to grow
		if (panePieces.GetCount() == panePieces.GetCapacity())
		{
			PanePieces pieces;
			pieces.Reserve(panePieces.GetCount() + 16);
			for (PanePieces::iterator begin(panePieces.begin()), end(panePieces.end()); begin != end; ++ begin)
			{
				bgPanePiece &from(*begin), &to(pieces.Grow());
					to.m_silhouette.Swap(from.m_silhouette);
					to.m_polygons.Swap(from.m_polygons);
					to.m_hit = from.m_hit;
					to.m_flags = from.m_flags;
					to.m_area = from.m_area;
				}
			panePieces.Swap(pieces);
		}
		bgPanePiece& panePiece = panePieces.Append();
		panePiece.m_flags |= bgPanePiece::kPF_Active;
		panePiece.m_area = 0.0f;
		// Construct the piece by recursively adding adjacent polygons.
		// Adds pane edges as necessary.
		ConstructPanePiece(adder, panePieceIndex, iPoly, poly[0], mesh, polyList);

		panePiece.m_area *= 0.5f;
		// convert polygons we flagged as 'visiting' to 'visited'
		for (int iiPoly=0; iiPoly<polyList.GetCount(); iiPoly++)
		{
			bgMesh::Index polygon(polyList[iiPoly]);
			Assert(mesh.GetPolygon(polygon).PieceId() == kPieceId_Visiting);
			mesh.PolygonPieceId(polygon, kPieceId_Visited);
		}
		BG_DEBUG_PRINT_ONLY(VerifyPanePieces(panePieces, GetPaneEdges(), 0, GetPanePoints(), panePieces.GetCount() - 1);)
		polyList.Resize(0); // clear temp list, but don't free memory
	}

	PanePoints& panePoints = data.m_panePoints;
	int pointCount = panePoints.GetCount();

	switch (m_strength)
	{
	case STRENGTH_NORMAL:
		break;
	case STRENGTH_REINFORCED:
		m_pReinforced = rage_aligned_new(16) Reinforced;
	case STRENGTH_BULLETPROOF:
		{
			PaneDepths& paneDepths = data.m_paneDepths;
			paneDepths.Resize(pointCount);
			for (PaneDepths::iterator begin(paneDepths.begin()), end(paneDepths.end()); begin != end; ++ begin)
				*begin = 0.f;
		}
		break;
	}

	// reset all point visibility flags
	GetPointDecalVisibilityNonConst().Init(pointCount);
	GetPointCrackVisibilityNonConst().Init(pointCount);
	static const int kFrameFlag_Left  = 1<<0;
	static const int kFrameFlag_Right = 1<<1;
	static const int kFrameFlag_Top   = 1<<2;
	static const int kFrameFlag_Bottom= 1<<3;
	// Figure out which edges are actually frame edges and flag appropriately
	PaneEdges &paneEdges = data.m_paneEdges;
	for (int iEdge=0; iEdge < paneEdges.GetCount(); iEdge++)
	{
		bgEdge& edge = paneEdges[iEdge];
		if (edge.Right()!=bgEdge::kPieceId_None)
		{
			continue;
		}

		u32 p0flags = 0;
		u32 p1flags = 0;
		const Vector2& p0 = panePoints[edge[0]];
		if (p0.x <= 0.0001f)
			p0flags |= kFrameFlag_Left;
		else if (p0.x >= 0.9999f)
			p0flags |= kFrameFlag_Right;
		if (p0.y <= 0.0001f)
			p0flags |= kFrameFlag_Top;
		else if (p0.y >= 0.9999f)
			p0flags |= kFrameFlag_Bottom;

		const Vector2& p1 = panePoints[edge[1]];
		if (p1.x <= 0.0001f)
			p1flags |= kFrameFlag_Left;
		else if (p1.x >= 0.9999f)
			p1flags |= kFrameFlag_Right;
		if (p1.y <= 0.0001f)
			p1flags |= kFrameFlag_Top;
		else if (p1.y >= 0.9999f)
			p1flags |= kFrameFlag_Bottom;

		u32 edgeFlags = p0flags & p1flags;
		switch (edgeFlags)
		{
		case kFrameFlag_Top:
			edge.Right(bgEdge::kPieceId_FrameTop);
			break;
		case kFrameFlag_Bottom:
			edge.Right(bgEdge::kPieceId_FrameBottom);
			break;
		case kFrameFlag_Left:
			edge.Right(bgEdge::kPieceId_FrameLeft);
			break;
		case kFrameFlag_Right:
			edge.Right(bgEdge::kPieceId_FrameRight);
			break;
		case 0:
			// frame not contacted
			break;
		default:
			// if more than one frame side is contacted, the edge must be degenerate, so we can ignore it
			break;
		}
	}
}

void bgBreakable::Create2DDataFromSequentHit(const bgHitInfo& in_hitInfo, int iPanePiece)
{
	const bgCrackType* pCrackType = in_hitInfo.GetCrackType(m_glassType, m_glassSizeIndex);

	bgGeometryData& data = GetGeometryDataNonConst();
	data.m_bevelSize = pCrackType->m_bevelSizeMax;
	data.m_crackSize = pCrackType->m_crackInBetweenPiecesSizeMax;
	float minThickness = pCrackType->m_minThickness * GetPaneSize();
	if (data.m_thickness < minThickness)
	{
		data.m_thickness = minThickness;
	}
#if __BANK
	if(sm_bNoThickness)
	{
		data.m_thickness = 0.0f;
	}
#endif // __BANK

	// First, we need to take the archetype mesh and clip it by the pane.
	bgMesh fullPaneMesh;
		ConvertCrackMesh(fullPaneMesh, in_hitInfo, m_glassType, m_glassSizeIndex);

		// Generate the clipping bound from the pane silhouette
		PanePoints& panePoints = data.m_panePoints;
		PanePieces& panePieces = data.m_panePieces;
		PaneEdges &paneEdges = data.m_paneEdges;
		bgPanePiece& panePiece = panePieces[iPanePiece];
		
		const int iNumClipPoints = panePiece.m_silhouette.GetCount();	
		Vector2 *clipSilhouette = Alloca(Vector2, iNumClipPoints);
		for(int iCurPoint = 0; iCurPoint < iNumClipPoints; iCurPoint++)
		{
			clipSilhouette[iCurPoint] = panePoints[panePiece.m_silhouette[iCurPoint].GetP0(paneEdges)];
		}

#if BG_DEBUG_PRINT
		Displayf("*************************************************************************");
		Displayf("KILLING PIECE %d:", iPanePiece);
		Displayf("*************************************************************************");
#endif

	// Remove the original piece and make room for the new piece and clip the mesh
	KillPanePiece(iPanePiece, false);
	bgClipMesh(fullPaneMesh, clipSilhouette, iNumClipPoints);

	// Prepare room for the additional points and copy them
	const int iOrigPointCount = panePoints.GetCount();
	panePoints.ResizeGrow(iOrigPointCount + fullPaneMesh.GetPointCount());
	for(int iCurNewPoint = iOrigPointCount; iCurNewPoint < panePoints.GetCount(); iCurNewPoint++)
	{
		panePoints[iCurNewPoint] = fullPaneMesh.GetPoints()[iCurNewPoint - iOrigPointCount];
	}

	// Add all the exsisting edges to the edge map
	BreakableEdgeAdder adder(*this);
	for(int iCurEdge = 0; iCurEdge < paneEdges.GetCount(); iCurEdge++)
	{
		bgEdgeRef edgeRef;
		edgeRef.SetEdgeIndex((bgIndex)iCurEdge);
		adder.AddEdgeRef(edgeRef);
	}

	// iterate through clipped polys and glom together into pieces where possible
	bgMesh::Index nPolys = bgMesh::Index(fullPaneMesh.GetPolygonCount());
	atArray<bgMesh::Index> polyList; // temp list of visited polgons
	polyList.Reserve(nPolys);
	for (bgMesh::Index iPoly=0; iPoly<nPolys; iPoly++)
	{
		bgMesh::Polygon const& poly = fullPaneMesh.GetPolygon(iPoly);
		// skip polygons that have already been processed
		if (poly.PieceId() == kPieceId_Visited)
		{
			continue;
		}

		Assert(poly.PieceId() >= 0);

		u16 panePieceIndex = u16(panePieces.GetCount());

		// Ensure there is room to grow
		if (panePieces.GetCount() == panePieces.GetCapacity())
		{
			PanePieces pieces;
			pieces.Reserve(panePieces.GetCount() + 16);
			for (PanePieces::iterator begin(panePieces.begin()), end(panePieces.end()); begin != end; ++ begin)
			{
				bgPanePiece &from(*begin), &to(pieces.Grow());
					to.m_silhouette.Swap(from.m_silhouette);
					to.m_polygons.Swap(from.m_polygons);
					to.m_hit = from.m_hit;
					to.m_flags = from.m_flags;
					to.m_area = from.m_area;
				}
			panePieces.Swap(pieces);
		}

#if BG_DEBUG_PRINT
		Displayf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		Displayf("NEW PIECE %d:", panePieces.GetCount());
		Displayf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
#endif

		bgPanePiece& panePiece = panePieces.Append();
		panePiece.m_flags |= bgPanePiece::kPF_Active;
		panePiece.m_area = 0.0f;
		// Construct the piece by recursively adding adjacent polygons.
		// Adds pane edges as necessary.
		ConstructPanePiece(adder, panePieceIndex, iPoly, poly[0], fullPaneMesh, polyList, static_cast<u16>(iOrigPointCount));
		if(panePiece.m_polygons.GetCount() == 0)
		{
			// Kill off this pane piece if it ends up having zero polys (poly's get deleted when they have less than 3 verts)
			KillPanePiece(panePieceIndex, false);
			panePieces.Pop();
		}
		else
		{
			panePiece.m_area *= 0.5f;
			BG_DEBUG_PRINT_ONLY(Displayf("Area of piece: %3.20f", panePiece.m_area);)
		}
		// convert polygons we flagged as 'visiting' to 'visited'
		for (int iiPoly=0; iiPoly<polyList.GetCount(); iiPoly++)
		{
			bgMesh::Index polygon(polyList[iiPoly]);
			Assert(fullPaneMesh.GetPolygon(polygon).PieceId() == kPieceId_Visiting);
			fullPaneMesh.PolygonPieceId(polygon, kPieceId_Visited);
		}
		BG_DEBUG_PRINT_ONLY(VerifyPanePieces(panePieces, GetPaneEdges(), iOrigPointCount, GetPanePoints(), panePieces.GetCount() - 1);)
		polyList.Resize(0); // clear temp list, but don't free memory
	}

	int pointCount = panePoints.GetCount();

	switch (m_strength)
	{
	case STRENGTH_NORMAL:
		break;
	case STRENGTH_REINFORCED:
		m_pReinforced = rage_aligned_new(16) Reinforced;
	case STRENGTH_BULLETPROOF:
		{
			PaneDepths& paneDepths = data.m_paneDepths;
			paneDepths.Resize(pointCount);
			for (PaneDepths::iterator begin(paneDepths.begin()), end(paneDepths.end()); begin != end; ++ begin)
				*begin = 0.f;
		}
		break;
	}

	// Increase the visibility size to include new points but keep previous values
	atBitSet prevDecalVis = GetPointDecalVisibilityNonConst();
	atBitSet prevCrackVis = GetPointCrackVisibilityNonConst();
	GetPointDecalVisibilityNonConst().Init(pointCount);
	GetPointCrackVisibilityNonConst().Init(pointCount);
	GetPointDecalVisibilityNonConst().Union(prevDecalVis);
	GetPointCrackVisibilityNonConst().Union(prevCrackVis);

	static const int kFrameFlag_Left  = 1<<0;
	static const int kFrameFlag_Right = 1<<1;
	static const int kFrameFlag_Top   = 1<<2;
	static const int kFrameFlag_Bottom= 1<<3;
	// Figure out which edges are actually frame edges and flag appropriately
	for (int iEdge=0; iEdge < paneEdges.GetCount(); iEdge++)
	{
		bgEdge& edge = paneEdges[iEdge];
		if (edge.Right() != bgEdge::kPieceId_None)
		{
			continue;
		}

		u32 p0flags = 0;
		u32 p1flags = 0;
		const Vector2& p0 = panePoints[edge[0]];
		if (p0.x <= 0.0001f)
			p0flags |= kFrameFlag_Left;
		else if (p0.x >= 0.9999f)
			p0flags |= kFrameFlag_Right;
		if (p0.y <= 0.0001f)
			p0flags |= kFrameFlag_Top;
		else if (p0.y >= 0.9999f)
			p0flags |= kFrameFlag_Bottom;

		const Vector2& p1 = panePoints[edge[1]];
		if (p1.x <= 0.0001f)
			p1flags |= kFrameFlag_Left;
		else if (p1.x >= 0.9999f)
			p1flags |= kFrameFlag_Right;
		if (p1.y <= 0.0001f)
			p1flags |= kFrameFlag_Top;
		else if (p1.y >= 0.9999f)
			p1flags |= kFrameFlag_Bottom;

		u32 edgeFlags = p0flags & p1flags;
		switch (edgeFlags)
		{
		case kFrameFlag_Top:
			edge.Right(bgEdge::kPieceId_FrameTop);
			break;
		case kFrameFlag_Bottom:
			edge.Right(bgEdge::kPieceId_FrameBottom);
			break;
		case kFrameFlag_Left:
			edge.Right(bgEdge::kPieceId_FrameLeft);
			break;
		case kFrameFlag_Right:
			edge.Right(bgEdge::kPieceId_FrameRight);
			break;
		case 0:
		// frame not contacted
			break;
		default:
			// if more than one frame side is contacted, the edge must be degenerate, so we can ignore it
			break;
		}
	}
}

//------------------------------------------------------------------------------
// When copying points from the archetype, rescale and translate based on hit info
//
// Input:	out_destinationNewPiece:		Destination to copy the data to
//			in_sourceArchetype:				Source to take the data from
//			in_hitInfo:						This copy happen when a hit happen.  This is 
//											the hit information.
//
void bgBreakable::CopyPoints(bgMesh& out_dest, const atArray<Vector2>& in_sourcePoints, const bgHitInfo& in_hitInfo)
{
	float cost, sint;
	cos_and_sin(cost, sint, in_hitInfo.m_rotation);
	int pointCount = in_sourcePoints.GetCount();
	out_dest.ReservePointCount(bgMesh::Index(pointCount));
	// need to scale up a bit more to guarantee full pane coverage with 45 degree rotations
	Vector2 scale = in_hitInfo.m_scaleData; 

	float mxx = cost * scale.x;
	float mxy = -sint * scale.x;
	float myx = sint * scale.y;
	float myy = cost * scale.y;

	for (int i = 0; i < pointCount; i++)
	{
		//Scale up the piece data
		const Vector2& srcPoint = in_sourcePoints[i];
		out_dest.Add(
			in_hitInfo.m_crackMapTranslation + Vector2(
			(srcPoint.x - 0.5f) * mxx + (srcPoint.y - 0.5f) * mxy,
			(srcPoint.x - 0.5f) * myx + (srcPoint.y - 0.5f) * myy
			)
			);
	}
}


//----------------------------------------
// Kill the specified pane piece.
//  Clear all refs to the piece
//  Flag all perimeter points 'visible'
//  Reset the piece
//
// input: in_iPanePiece ==>		piece to kill
//
void bgBreakable::KillPanePiece(int in_iPanePiece, bool bSetVisibility)
{
	PaneEdges& paneEdges = GetPaneEdgesNonConst();
	//atBitSet& pointDecalVisibility = GetPointDecalVisibilityNonConst();
	atBitSet& pointCrackVisibility = GetPointCrackVisibilityNonConst();
	bgPanePiece& piece = GetPanePiecesNonConst()[in_iPanePiece];
	// first clear all edge refs to this edge:
	BG_DEBUG_PRINT_ONLY(Displayf("Resetting Edge Information %d", in_iPanePiece);)

	for (int iEdge=0; iEdge< piece.m_silhouette.GetCount(); iEdge++)
	{
		bgEdgeRef& edgeRef = piece.m_silhouette[iEdge];
#if BG_DEBUG_PRINT
		Displayf("P0 = %d, P1 = %d, Edge Left = %d, Edge Right= %d ", 
			edgeRef.GetP0(paneEdges),
			edgeRef.GetP1(paneEdges),
			edgeRef.GetLeft(paneEdges), 
			edgeRef.GetRight(paneEdges) );
#endif
		Assertf(edgeRef.GetLeft(paneEdges) == in_iPanePiece, "edgeRef.GetLeft(paneEdges) == in_iPanePiece (%d, %d)", edgeRef.GetLeft(paneEdges), in_iPanePiece);
		edgeRef.SetLeft(paneEdges, bgEdge::kPieceId_None);
		
		if(bSetVisibility)
		{
			// flag all perimeter points as 'visible'
			//pointDecalVisibility.Set( edgeRef.GetP0(paneEdges) );
			pointCrackVisibility.Set( edgeRef.GetP0(paneEdges) );
		}
	}
	piece.Reset();
}

float bgBreakable::CalculateViewportCoverage() const
{
	// transform model-space sphere pos to world-space
	Mat34V transform = RCC_MAT34V(m_transform);
	Vec3V worldPos = rage::Transform(transform, GetPaneCenter());

	// compute roughly how large the pane is in screen pixels
	return sm_viewportWidth * GetPaneSize() / (sm_viewportTanHFOV * 
		Dist(sm_viewPosition, worldPos).Getf());
}

// -----------------------------------------------------------------------------
// Get the lod level currently in use (constants from bgGeometryBuilder::eGlassLOD)
//

void bgBreakable::SetLod( int lod)
{
	bgGeometryData& data = GetGeometryDataNonConst();
	if (lod != data.m_lod)
	{
		//Skip geometry change if glass changes between VLOW and LOW LOD's as the only change
		//between them is the Vertex/Pixel Shader lighting. Between other stages
		//there will be a change in the geometry.
		const bool bChangeGeometry = !m_bHasAssignedBuffers || !((lod == bgLod::LOD_VLOW && data.m_lod == bgLod::LOD_LOW) ||
			(lod == bgLod::LOD_LOW && data.m_lod == bgLod::LOD_VLOW));

		data.m_lod = lod;
		if(bChangeGeometry)
		{
			m_bUpdateBuffers = true;
		}
	}
}

void bgBreakable::AdjustLod()
{
	if (sm_LodOverride >= 0)
	{
		SetLod(sm_LodOverride);
	}
	else if (0.0f != sm_viewportTanHFOV)
	{
		const bgCrackStarMap::LodArray& lodPixelSizeArray = BANK_ONLY(sm_bOverrideLOD ? bgCrackStarMap::m_TuneLodPixelSizeArray :) GetCurrentHit()->GetCrackStarMap(m_glassType, m_glassSizeIndex)->m_LodPixelSizeArray;

		Vec3V worldPos = GetPaneCenter();
		Mat34V transform = RCC_MAT34V(m_transform);
		// transform model-space sphere pos to world-space
		worldPos = rage::Transform(transform, worldPos);

		// compute roughly how large the pane is in screen pixels
		float pixelSize = sm_viewportWidth * GetPaneSize() / (sm_viewportTanHFOV * 
			rage::Dist(sm_viewPosition, worldPos).Getf());

		// first check for LOD decrease
		int lod;
		int curLod = GetLod();
		// if I were really cool, I'd make lod iterators that went low->high regardless of how the values are arranged
		for (lod = bgLod::LOD_VLOW; lod < curLod; lod++)
		{
			if (pixelSize < lodPixelSizeArray[lod])
			{
				break;
			}
		}
		if (lod == curLod)
		{
			// no decrease; now check if lod increase is needed
			for (lod = sm_MaxDynamicLod; lod > curLod; lod--)
			{
				float hysteresisFudge = (lodPixelSizeArray[lod] - lodPixelSizeArray[lod - 1]) * sm_LodHysteresisFudge;
				if (pixelSize > lodPixelSizeArray[lod-1] + hysteresisFudge)
				{
					break;
				}
			}
		}

		// Check if we can switch to very low LOD
		BANK_ONLY(if(sm_bEnableLODSkip))
		{
			lod = (lod == bgLod::LOD_LOW && m_bSafeToUseVSLight) ? bgLod::LOD_VLOW : lod;
		}

		// Set the LOD value
		SetLod(lod);
	}
}


//----------------------------------------
// Mostly update the pieces flying out of 
//  the pane by applying physics and collision
//
// input: in_deltaTime ==>		time elapsed since last call
//
void bgBreakable::Update(float in_deltaTime)
{
	if (!IsValid())
		return;

	if (GetGeometryData().GetHitCount() == 0)
		return;

	in_deltaTime *= sm_SimulationTimeScale;

	AdjustLod();
//	m_oldPhysics.Reset();
	switch (m_strength)
	{
	case STRENGTH_NORMAL:
		UpdateNormal(in_deltaTime);
		break;
	case STRENGTH_REINFORCED:
		UpdateReinforced(in_deltaTime);
		break;
	case STRENGTH_BULLETPROOF:
		{
			Matrix43 identity;
			identity.Identity();
			m_FallingPieceMatrix[0] = identity;
		}
		break;
	}
}

PFD_DECLARE_GROUP(BreakableGlass);
PFD_DECLARE_ITEM_ON(BoundingBoxes,Color_white,BreakableGlass);
PFD_DECLARE_ITEM_ON(PieceBoundingBoxes,Color_white,BreakableGlass);

void bgBreakable::UpdateNormal(float in_deltaTime)
{
	// Find the bounding box for the pane
	Vector3 normal;
	normal.Cross(GetGeometryData().GetPosWidth(), GetGeometryData().GetPosHeight());
	normal.Normalize();
	Vector3 depth(normal * VEC3_HALF * GetThickness());
	Vector3 rightTop(Vector3(GetGeometryData().GetPosTopLeft()) - depth);
	Vector3 leftBottom(Vector3(GetGeometryData().GetPosTopLeft()) +
		Vector3(GetGeometryData().GetPosWidth()) +
		Vector3(GetGeometryData().GetPosHeight()) +
		depth);

	// From now on we want the worldspace noraml
	m_transform.Transform3x3(normal);

	// Calculate the bone transformation for the pane
	Matrix34 paneMat;
	paneMat.Identity(); // For LOD higher than med there is no transformation
	if(GetLod() <= bgLod::LOD_MED)
	{
		// For low LOD the pane pieces need to be pushed to the correct side based on the camera position
		Vector3 worldPos = 0.5f * (rightTop + leftBottom);
		m_transform.Transform(worldPos);
		float fPlaneD = normal.Dot(worldPos);
		float fCamPlaneDist = normal.Dot(RCC_VECTOR3(sm_viewPosition));
		paneMat.d = fCamPlaneDist >= fPlaneD ? depth : -depth;
	}
	m_FallingPieceMatrix[0].FromMatrix34(RCC_MAT34V(paneMat));

	FallingPieces& fallingPieces = GetFallingPiecesNonConst();
	bgPhysicsData& fallingPhysics = GetFallingPhysicsNonConst();

	bool anyAliveBefore = false, allDeadAfter = true;

	Vector3 boxMin = VEC3_MAX;
	Vector3 boxMax = -VEC3_MAX;

	//Update the pieces flying out of the pane
	//
	//Iterate through the array of pieces
	for (int i = 0;i < fallingPieces.GetCount();i++)
	{
		bgFallingPiece& piece = fallingPieces[i];
		bgFallingPhysics& physics = fallingPhysics[i];

		//Update life
		anyAliveBefore |= !(piece.m_life < 0.f);
		piece.m_life -= in_deltaTime;

		// if we're dead, stop updating
		if (piece.m_life < 0.f)
		{
			Matrix43 zero;
			zero.Zero();
			m_FallingPieceMatrix[bgMatrixArray::FIRST_FALLING_PIECE + i] = zero;

			// Be sure to clean up the physics before we discard it
			physics.Cleanup();
			physics.m_pPhysics = NULL;
			continue;
		}

		// HACK!!!  This isn't supposed to happen, you shouldn't be able to get here with a NULL m_pPhysics, but somehow sometimes you can.
		if(physics.m_pPhysics == NULL)
		{
			continue;
		}

		allDeadAfter = false;

		// Find piece's transform in object space
		Matrix34 objectMatrix = physics.m_pPhysics->Update(in_deltaTime);
		Matrix34 inverse;
		inverse.FastInverse(GetTransform());
		objectMatrix.Dot(inverse);

		// Scale the life based on the distance from the pane
		float fScaledLife = piece.m_life;
		float fDistFromOrg = objectMatrix.d.Dot(objectMatrix.d);
		if(fDistFromOrg > sm_fDistFadeStart)
		{
			// Start to fade this piece
			fScaledLife = 1.0f - (fDistFromOrg - sm_fDistFadeStart) / sm_fDistFadeRange;

			// If this piece is completely faded out just skip it all together
			if (fScaledLife <= 0.f)
			{
				piece.m_life = -1.0f; // Kill this piece

				Matrix43 zero;
				zero.Zero();
				m_FallingPieceMatrix[bgMatrixArray::FIRST_FALLING_PIECE + i] = zero;

				// Be sure to clean up the physics before we discard it
				physics.Cleanup();
				physics.m_pPhysics = NULL;
				continue;
			}
		}

		//Find piece's transform in glass space
		Matrix34 glassMatrix;
		glassMatrix.a = m_rotation.a;
		glassMatrix.b = m_rotation.b;
		glassMatrix.c = m_rotation.c;
		glassMatrix.d.Zero();
		glassMatrix.Dot(objectMatrix);

		// --- Compute the enclosing bounding box

		// Get the size of the piece, expanded by its thickness
		Vector3 pieceExtents(piece.m_extents.x, piece.m_extents.y, GetThickness() * 0.5f);
		Vector3 pieceCenter(objectMatrix.d);

 		// Transform the extents.
 		objectMatrix.Abs();
 		objectMatrix.Transform3x3(pieceExtents);

		// Expand the overall bounding box
		boxMax.Max(boxMax, pieceCenter + pieceExtents);
		boxMin.Min(boxMin, pieceCenter - pieceExtents);

#if __PFDRAW
		if (PFD_PieceBoundingBoxes.Begin())
		{
			Matrix34 pieceMatrix = GetTransform();
			GetTransform().Transform(pieceCenter, pieceMatrix.d);
			grcDrawBox(pieceExtents * (VEC3_IDENTITY + VEC3_IDENTITY), pieceMatrix, Color_white);
 			PFD_BoundingBoxes.End();
		}
#endif

		if (fScaledLife < sm_fade)
			glassMatrix.Scale(fScaledLife / sm_fade);

		//Update the matrix that will be sent to the Shader
		m_FallingPieceMatrix[bgMatrixArray::FIRST_FALLING_PIECE + i].FromMatrix34(RCC_MAT34V(glassMatrix));
	}

	// And add it into the overall bounding box
	boxMax.Max(boxMax, rightTop);
	boxMax.Max(boxMax, leftBottom);
	boxMin.Min(boxMin, rightTop);
	boxMin.Min(boxMin, leftBottom);
	m_boxMax = boxMax;
	m_boxMin = boxMin;

#if __PFDRAW
	if (PFD_BoundingBoxes.Begin())
	{
		Matrix34 boxMatrix = GetTransform();
		GetTransform().Transform((boxMax + boxMin) * VEC3_HALF, boxMatrix.d);
		grcDrawBox(boxMax - boxMin, boxMatrix, Color_yellow);
		PFD_BoundingBoxes.End();
	}
#endif

	m_boundingSphere.SetVector3((boxMax + boxMin) * VEC3_HALF);
	m_boundingSphere.SetW((boxMax - boxMin).Mag() * 0.5f);

	//Update buffers when all pieces have fallen
	if (anyAliveBefore && allDeadAfter)
		m_bUpdateBuffers = true;
}

void bgBreakable::UpdateReinforced(float in_deltaTime)
{
	if (m_fallen)
	{
		bgFallingPhysics &physics(m_pReinforced->m_physics);
		//Update life
		m_pReinforced->m_life -= in_deltaTime;

		// if we're dead, stop updating
		if (m_pReinforced->m_life < 0.f)
		{
			if (physics.m_pPhysics)
			{
				physics.m_pPhysics = 0;
				Matrix43 zero;
				zero.Zero();
				for (size_t index(0); index != REINFORCED_FALLING_COUNT; ++ index)
					m_FallingPieceMatrix[(u32)index] = zero;

				// Be sure to clean up the physics before we discard it
				physics.Cleanup();
				physics.m_pPhysics = NULL;
			}
			return;
		}

		//Find piece's transform in glass space
		Matrix34 matrix;
		matrix.a = m_rotation.a;
		matrix.b = m_rotation.b;
		matrix.c = m_rotation.c;
		matrix.d = -Vector3(Transform(Vector2(0.5f, 0.5f)));
		matrix.Transform3x3(matrix.d);
		matrix.Dot(physics.m_pPhysics->Update(in_deltaTime));
		Matrix34 inverse;
		inverse.FastInverse(GetTransform());
		matrix.Dot(inverse);
		if (m_pReinforced->m_life < sm_fade)
			matrix.Scale(m_pReinforced->m_life / sm_fade);

		if (phCollider *pCollider = physics.m_pPhysics->GetCollider())
		{
			// Determine angle from pane speed
			float scale(sm_velocityScale);
			float maximum(sm_maxBend);
			Vector3 velocity(RCC_VECTOR3(pCollider->GetVelocity()));
			float angle(velocity.Mag() * scale);
			if (!(angle < 1.f))
				angle = maximum;
			else
				angle *= maximum;

			// Find pane normal in world space
			Vector3 normal;
			normal.Cross(GetGeometryData().m_posWidth, GetGeometryData().m_posHeight);
			RCC_MATRIX34(pCollider->GetMatrix()).Transform3x3(normal);

			// Bend away from gravity
			if (normal.Dot(PHSIM->GetGravity()) > 0.f)
				angle = -angle;

			// Find pane center
			Vector3 center(Transform(Vector2(0.5f, 0.5f)));
			for (size_t index(0); index != REINFORCED_FALLING_COUNT; ++ index)
			{
				Vector3 const &axis(m_pReinforced->m_axes[index]);

				// Rotate around pane center
				Matrix34 transform;
				transform.MakeRotateUnitAxis(axis, angle);
				transform.Transform3x3(-center, transform.d);
				transform.Translate(center);

				// Multiply by glass space matrix
				transform.Dot(matrix);

				//Update the matrix that will be sent to the Shader
				m_FallingPieceMatrix[(u32)index].FromMatrix34(RCC_MAT34V(transform));
			}
		}
	}
	else
	{
		Matrix43 identity;
		identity.Identity();
		for (size_t index(0); index != REINFORCED_FALLING_COUNT; ++ index)
		{
			m_FallingPieceMatrix[(u32)index] = identity;
		}
	}
}

//------------------------------------------------------------------------------
// convert a 3D point on the glass plane (must be inside the rectangle) to a 
// 2D point with value between 0,1 telling us where the point is located on our
// 2D virtual window.
//
// This function is useful if you have a 3D point on the pane plane (inside the pane rectangle)
// and you want to know the unit value of this point in 2D to be use in the Hit() function 
// as in_hitLocX,in_hitLocY.
//
// Input: in_point3D:		3D point inside the pane rectangle, also found on the pane plane
//
Vec2V_Out bgBreakable::Convert3DPointTo2DPoint(Vec3V_In in_point3D) const
{
	//This will be the actual 2D point at the end of the function
	Vec2V point2D;
	const bgGeometryData& data = GetGeometryData();
	Vector3 input = RCC_VECTOR3(in_point3D) - data.m_posTopLeft;
	Vector3 top = data.m_posWidth;
	Vector3 left = data.m_posHeight;
	point2D.SetX(input.Dot(top) / top.Mag2());
	point2D.SetY(input.Dot(left) / left.Mag2());

	return point2D;
}//Convert3DPointTo2DPoint

void bgBreakable::SetMaxLod(int lod)
{
	sm_MaxDynamicLod = lod;
}

const bgCrackType* bgHitInfo::GetCrackType(int in_glassType, int in_glassSizeIndex) const
{
	return bgSCracksTemplate::GetInstance().GetCrackData(in_glassType, in_glassSizeIndex, m_crackType);
}

const bgCrackStarMap* bgHitInfo::GetCrackStarMap(int in_glassType, int in_glassSizeIndex) const
{
	return &GetCrackType(in_glassType, in_glassSizeIndex)->m_crackArray[m_starMapIndex];
}

void bgBreakable::SetViewportData(Vec3V_In in_cameraPos, int in_viewportSize, float in_tanFov)
{
	sm_viewPosition = in_cameraPos;
	sm_viewportWidth = in_viewportSize;
	sm_viewportTanHFOV = in_tanFov;
}


bgGeometryData& bgGeometryData::operator =(const bgGeometryData& rhs)
{
	m_crackSize = rhs.m_crackSize;
	m_bevelSize = rhs.m_bevelSize;
	m_lod = rhs.m_lod;
	m_thickness = rhs.m_thickness;
	m_isReinforced = rhs.m_isReinforced;
	m_posTopLeft = rhs.m_posTopLeft;
	m_posWidth = rhs.m_posWidth;
	Assert(IsFiniteAll(RCC_VEC3V(m_posWidth)));
	m_posHeight = rhs.m_posHeight;
	Assert(IsFiniteAll(RCC_VEC3V(m_posHeight)));

	m_pointDecalVisibility = rhs.m_pointDecalVisibility;
	m_pointCrackVisibility = rhs.m_pointCrackVisibility;
	m_hitInfo = rhs.m_hitInfo;
	m_paneEdges = rhs.m_paneEdges;
	m_panePieces = rhs.m_panePieces;
	m_panePoints = rhs.m_panePoints;
	m_paneDepths = rhs.m_paneDepths;
	m_fallingPieces = rhs.m_fallingPieces;
	return *this;
}

// deep copy
bgPhysicsData& bgPhysicsData::operator =(const bgPhysicsData& rhs)
{
	Reset();
	Resize(rhs.GetCount());
	for (int i=0; i<m_Count; i++)
	{
		// Physics may have been removed if the lifetime expired
		if (bgPhysics* physics = rhs[i].m_pPhysics)
		{
			m_Elements[i].m_pPhysics = physics->Clone();
		}
	}
	return *this;
}

static bgBreakable::TriangleId PackTriangleId(unsigned pieceIndex,
											  unsigned polyIndex,
											  unsigned triIndex)
{
	return (pieceIndex << 16) + (polyIndex << 8) + triIndex;
}

bool bgBreakable::GetPaneTriangleInfo(TriangleId id, Vector3::Vector3Ref outPosA, Vector3::Vector3Ref outPosB, Vector3::Vector3Ref outPosC, Vector3::Vector3Ref outNormal) const 
{
	unsigned pieceIndex = id>>16;
	const bgGeometryData& geomData = GetGeometryData();
	const bgPanePiece& piece = geomData.GetPanePieces()[pieceIndex];
	if (!(piece.m_flags & bgPanePiece::kPF_Active))
		return false;

	unsigned polyIndex = (id>>8)&0xff;
	unsigned triIndex = id&0xff;
	const bgPolygon& poly = piece.m_polygons[polyIndex];
	const atArray<Vector2>& points = geomData.GetPanePoints();
	const Vector2& a = points[poly[0]];
	const Vector2& b = points[poly[triIndex + 1]];
	const Vector2& c = points[poly[triIndex + 2]];
	outPosA = geomData.Transform(a);
	outPosB = geomData.Transform(b);
	outPosC = geomData.Transform(c);
	Vector3 normal;
	normal.Cross(geomData.GetPosWidth(), geomData.GetPosHeight());
	normal.Normalize();
	outNormal = normal;
	return true;
}

bgBreakable::TriangleId bgBreakable::GetNextPaneTriangle(TriangleId id) const
{
	unsigned pieceIndex = 0;
	const atArray<bgPanePiece>& pieces = (GetGeometryData().GetPanePieces());
	if (id != kInvalidTriangle)
	{
		pieceIndex = id >> 16;
		const bgPanePiece& piece = pieces[pieceIndex];
		if (piece.m_flags & bgPanePiece::kPF_Active)
		{
			unsigned polyIndex = (id >> 8) & 0xff;
			unsigned triIndex = id & 0xff;
			unsigned triCount = (unsigned)(piece.m_polygons[polyIndex].GetCount() - 2);
			// Any triangles left in this poly?
			if (++triIndex < triCount)
			{   // return next triangle in this polygon
				return PackTriangleId(pieceIndex, polyIndex, triIndex);
			}
			// Any polygons left in this piece?
			unsigned polyCount =(unsigned)(piece.m_polygons.GetCount());
			if (++polyIndex < polyCount)
			{   // return first triangle in next polygon
				return PackTriangleId(pieceIndex, polyIndex, 0);
			}
		}
		// done with this piece, move to the next one
		pieceIndex++;
	}
	// return the first triangle of the first poly of the next active piece we find
	unsigned nPieces = (unsigned)(pieces.GetCount());
	while (pieceIndex < nPieces)
	{
		if (pieces[pieceIndex].m_flags & bgPanePiece::kPF_Active)
		{
			// this one's good
			return PackTriangleId(pieceIndex, 0, 0);
		}
		// piece not valid; try the next one
		pieceIndex++;
	}
	return kInvalidTriangle;
}

bgBreakable::TriangleId bgBreakable::GetNextFallingTriangle(TriangleId id) const
{
	const atArray<bgFallingPiece>& pieces = GetGeometryData().GetFallingPieces();
	unsigned pieceIndex = 0;
	if (id != kInvalidTriangle)
	{
		pieceIndex = id>>16;
		unsigned triIndex = id &0xff;
		const bgFallingPiece& piece = pieces[pieceIndex];
		if (piece.m_life > 0)
		{
			unsigned triCount = piece.m_points.GetCount()-2;
			if (++triIndex < triCount)
				return PackTriangleId(pieceIndex, 0, triIndex);
		}
		pieceIndex++;
	}
	unsigned pieceCount = pieces.GetCount();
	while (pieceIndex < pieceCount)
	{
		if (pieces[pieceIndex].m_life > 0)
		{
			return PackTriangleId(pieceIndex, 0, 0);
		}
		pieceIndex++;
	}
	return kInvalidTriangle;
}

bool bgBreakable::GetFallingTriangleInfo(TriangleId id,
							Vector3::Vector3Ref outPosA, 
							Vector3::Vector3Ref outPosB, 
							Vector3::Vector3Ref outPosC, 
							Vector3::Vector3Ref outNormal) const
{
	unsigned pieceIndex = id>>16;
	const bgGeometryData& geomData = GetGeometryData();
	const bgFallingPiece& piece = geomData.GetFallingPieces()[pieceIndex];
	if (piece.m_life <= 0.f)
		return false;

	unsigned triIndex = id&0xff;
	const Vector2& a = piece.m_points[0];
	const Vector2& b = piece.m_points[triIndex + 1];
	const Vector2& c = piece.m_points[triIndex + 2];
	outPosA = Vector3(geomData.Transform(a)) - piece.m_massCenteredTranslation;
	outPosB = Vector3(geomData.Transform(b)) - piece.m_massCenteredTranslation;
	outPosC = Vector3(geomData.Transform(c)) - piece.m_massCenteredTranslation;
	Vector3 normal;
	normal.Cross(geomData.GetPosWidth(), geomData.GetPosHeight());
	normal.Normalize();
	outNormal = normal;
	return true;
}

void bgBreakable::GetFallingTriangleMatrix(TriangleId id, Mat34V_InOut outMatrix) const
{
	int pieceIndex = id>>16;
	m_FallingPieceMatrix[bgMatrixArray::FIRST_FALLING_PIECE + pieceIndex].ToMatrix34(outMatrix);
}


void bgDrawableDrawData::SetupDrawData(const bgBreakable &in_breakable, const bgDrawable &in_drawable, float* transforms, int numTransforms)
{
	//Perform Calculations here instead of in bgDrawable::SetupDrawable, as
	// data could get changed in bgDrawable in Main Thread while it is
	// being rendered. 
	// Passing data through Draw Command will ensure that the renderThread will get
	// data that will not be changed by the main thread concurrently

	m_matrix = in_breakable.GetTransform();
	const bgHitInfo* pHit = in_breakable.GetHit(0);

	// rotate crack mask texture and rescale to fit pane
	float sint, cost;
	cos_and_sin(cost, sint, pHit->m_rotation);
	Vector4 textureScale(0.5f,0.5f,0.0f,0.0f);
	textureScale.x = cost/pHit->m_scaleData.x;
	textureScale.y = sint/pHit->m_scaleData.y;
	textureScale.z = -sint/pHit->m_scaleData.x;
	textureScale.w = cost/pHit->m_scaleData.y;

	// combine everything into a single simple transform
	m_crackTexMatrix = textureScale;

	m_crackTexOffset.x = 0.5f - (pHit->m_location.x * textureScale.x + pHit->m_location.y * textureScale.y);
	m_crackTexOffset.y = 0.5f - (pHit->m_location.x * textureScale.z + pHit->m_location.y * textureScale.w);
	m_crackTexOffset.z = 0.f;
	m_crackTexOffset.w = 1.f;

	m_pCrackStarMap = const_cast<bgCrackStarMap*> (pHit->GetCrackStarMap(in_breakable.GetGlassType(), in_breakable.GetGlassSizeIndex()));

	m_lod = in_breakable.GetLod();
	m_pBuffers = static_cast<bgGpuBuffers*> (in_drawable.GetBuffers());

	m_transforms = transforms; 
	m_numTransforms = numTransforms;

	m_arrPieceIndexCount = in_breakable.GetCurrentPieceCount();
	m_arrCrackIndexCount = in_breakable.GetCurrentCrackCount();
};

} 


// namespace rage
