//
// breakableglass/polygondicer.h
// 
// Copyright (C) 2008 Rockstar Games.  All Rights Reserved. 
//

// -------------------------------------------------------------------------------------------------
// This class takes a silhouette and cut it into smaller pieces.  This function must be optimize and
// therefore we do not use dynamic array.  This function has two define telling us the maximum of data
// required:
//		Max number of piece to be cut into
//		Max numbert of vertex per peice
//
// Once the piece is cut into smaller pieces, the user is responsible to read the structure in this 
// function and create the final pieces with the data.  Also this has to be done right away because
// the memory use to hold the data is static and therefore will be erase by an other piece being cut 
// (NOT MULTI THREAD-SAFE).
//
// Direction:
//		1 - You have one piece you want to be cut in X pieces
//		2 - Call Cut() with the proper parameters
//		3 - All pieces in m_pieces that have a m_use == true should be use to create a piece
// -------------------------------------------------------------------------------------------------

#ifndef BREAKABLEGLASS_POLYGONDICER_H
#define BREAKABLEGLASS_POLYGONDICER_H

#include "crackstemplate.h"

#include "atl/array.h"
#include "math/random.h"
#include "vector/Vector3.h"

namespace rage {

//These define tell how much memory to allocate for different things
//
// How many pieces to allocate total: This number does not represent how many pieces we can create total 
// but how many pieces we can store.  When a piece is cut in two, the piece is not discard and the two new pieces 
// are stored following the last piece added, therefore a piece cut in two take 3 slot (1 old piece + 2 new pieces = 3 pieces total).
#define SmallPiecesMaxPieces				50
//Each pieces is composed of 2D point: This value tell us what is the maximum number of point we can have
#define SmallPiecesMaxVertexPerPiece		100

class bgPolygonDicer
{
public:
	//Class holding the pieces data
	class CutPiece
	{
	public:
		CutPiece()
		{
			m_pointCount = 0;
			m_use = false;
		}

		//true = we should create a polygon with it
		// This variable will be false if the cell is not use or if the piece was cut
		bool				m_use;
		//Center of mass (approximation)
		Vector2				m_center;
		//This is the piece that received the impact causing the glasses to fall
		bool				m_hitbyImpact;
		//Size of the piece: this is not the volume - it is the longest distance between the farthest point on both axis
		Vector2				m_size;
		//Vertices's
		Vector2				m_point[SmallPiecesMaxVertexPerPiece];
		int					m_pointCount;
	};

	bgPolygonDicer();

	//-----------------------------------------------------------------
	// Break a piece of glass into smaller pieces
	//
	// Input:	impactLoc:		Where the impact trigger pieces falling
	//			in_polygons:	Piece of glass to break
	//			in_points:		Points referred by the pieces (referred by in_polygons)
	//			in_pieceSizeTriggerCut:			If a piece is equal or bigger then this size we will try to cut the piece smaller
	//			inOut_piecesToCutReamining:		This is the remaining number of slot available for new pieces created when we cut.  If this reach 0
	//											it means that we cannot cut anymore.
	//
	static void Cut(const Vector2& impactLoc, const atArray<bgPolygon>& in_polygons, const atArray<Vector2>& in_points, float in_pieceSizeTriggerCutSqr, int* inOut_piecesToCutReamining );

	//-----------------------------------------------------------------
	// This function output a line to cut a piece in two.  The line direction
	//  will be based on the piece 2D (x-y) length.  The line will be created to
	//  cut the piece on the longer side in order to reduce its total length.
	//	This is useful in the case where we do not want big pieces falling from
	//  a pane of glass.  This will ensure that a piece of glass is always getting smaller
	//  after being cut with the line returned from this function.
	//
	static void GetCuttingLine(Vector4& out_line, float lengthX, float lengthY);

	//-----------------------------------------------------------------
	// This function output a line to cut a piece in two.  The line direction
	// is chosen randomly.
	static void GetCuttingLine(Vector4& out_line);

	//-----------------------------------------------------------------
	// Take a piece and cut it in two pieces
	//
	// Input:	in_piece:					The vertices of the piece to cut in two.  
	//			in_pieceVertexCount			Number of vertices in the the piece
	//			in_line:					Line cutting the piece in two.
	//										in_line.xy = point1
	//										in_line.xy = point2
	//										The two point have to be outside of the polygon and must collide twide with the polygon to work.
	//			in_lineGranularity:			How many points do you want the line to be created with:  The line start straight (2 points) but with 
	//										more point less straight the line will be and therefore the cut should look better.  By default
	//										the value is 0, meaning we do not want to cut the line in multiple smaller line.
	//			out_pieceOne				One half of the original piece
	//			out_pieceTwo				Other half of the original piece
	//
	// Output:  Return eraly if the line does not touch the polygon or only one point is touching.
	//
	static bool CutInTwoPiece(	CutPiece* in_piece, Vector4& in_line, int /*in_lineGranularity*/, 
										CutPiece* out_pieceOne, CutPiece* out_pieceTwo);

	//-----------------------------------------------------------------
	// Take a piece and cut it in two pieces
	//
	// Input:	in_piece:					The vertices of the piece to cut in two.  
	//			out_pieceOne				One half of the original piece
	//			out_pieceTwo				Other half of the original piece
	//
	// Output:  Return eraly if the line does not touch the polygon or only one point is touching.
	//
	static void CutInTwoPieceFast(CutPiece* in_piece, CutPiece* out_pieceOne, CutPiece* out_pieceTwo);

	//-----------------------------------------------------------------
	// Create a piece of glass that will be half of a full piece of glass.
	// The original piece (in_silhouette) have been cut in two with a line (in_intersection1 &
	// in_intersection2).  The output (out_silhouette) will be one side of the line, from 
	// in_intersection1 to in_intersection2, following the segments.
	// 
	// Input:	in_silhouette		Original polygon being cut in two
	//			out_silhouette		Half the original polygon starting from in_intersection1 to in_intersection2
	//			in_intersection1	Intersection point between original polygon and line dividing the polygon in 2.
	//								This is also the first point of the output polygon
	//			in_segmentIndex1	Index segment (first point of the segment) of the original polygon the line intersect first
	//			in_intersection2	Intersection point between original polygon and line dividing the polygon in 2.
	//								This is also the last point of the output polygon
	//			in_segmentIndex2	Index segment (first point of the segment) of the original polygon the line intersect last
	//
	static void CreateHalfPiece(	CutPiece* in_silhouette, CutPiece* out_silhouette, 
									Vector2& in_intersection1, int in_segmentIndex1, 
									Vector2& in_intersection2, int in_segmentIndex2);

	//----------------------
	//Access the class data
	//
	// Retrieve how many piece were created the last time our cutting function (cut()) was called
	static int GetPieceCount()
	{
		return m_piecesCount;
	}
	
	//Retrieve a piece from our array by a passing what index we want to retrieve
	static CutPiece& GetPiece(int in_index)
	{
		return m_pieces[in_index];
	}

private:
	//This is the random number we are using in this class
	rage::mthRandom							m_rand;

	//We cut the piece with a line.  These are the lines available to us.  They will be rotated around axis 0,0.
	//Maximum line
	#define LINE_CUT_COUNT	20
	//Lines
	static Vector4							m_cutLines[LINE_CUT_COUNT];
	//Next line to use: Index in cutLines
	static int								m_cutLineIndex;

	//Pieces data
	static CutPiece						m_pieces[SmallPiecesMaxPieces];
	//Pieces count:  This tell us how many pieces have been stored in our array
	static int								m_piecesCount;

	//Create a cutting class so that the constructor get called at least one.  This is necessary
	// in order for our constructor to initialize some values.
	static bgPolygonDicer							m_notUseCuttingClass;
		

	static int LinesIntersectFloat (float ax, float ay, float bx, float by, float cx, float cy, float dx, float dy, float *px, float *py);
};

} // namespace rage

#endif // BREAKABLEGLASS_POLYGONDICER_H
