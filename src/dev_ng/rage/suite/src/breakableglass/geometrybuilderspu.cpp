// 
// breakableglass/geometrybuilderspu.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#if __PS3

#if __SPU
#define SIMPLIFIED_ATL
#endif // __SPU

#include "geometrydata.h"
#include "geometrybuilder.h"
#include "geometrybuilderspu.h"
#include "geometrybuildercommon.h"

#if BREAKABLE_GLASS_ON_SPU

#include "grcore/wrapper_gcm.h"
#include "math/random.h"
#include "vectormath/classes.h"

#define VERBOSE_SPU_DEBUG 0

#define BIT_SIZE_TO_BYTE_SIZE(val)	((((val)+0x7)&(~0x7))>>3)

#define BG_DMA_TAG 9

namespace rage {
//

struct vertexStreamInfo
{
	u32 Stride;
	u32 PositionOffset;
	u32 PositionSize;
	u32 NormalOffset;
	u32 NormalSize;
	u32 TangentOffset;
	u32 TangentSize;
	u32 TextureOffset0;
	u32 TextureSize0;
	u32 TextureOffset1;
	u32 TextureSize1;
	u32 TextureOffset2;
	u32 TextureSize2;
	u32 DiffuseOffset;
	u32 DiffuseSize;
};

struct BuilderTaskParams
{
	vertexStreamInfo	StreamInfo;
	u32					OutPutIndexBufferEa;
	u32					OutputIndexBufferSize;
	u32					OutputVertexBuffeEa;
	u32					OutputVertexBufferSize;
	u32                 GeometryDataEa;
	u32                 GeometryDataSize;
} ALIGNED(128);

} // namespace rage

#if __SPU

#include <cell/dma.h>
#include <cell/atomic.h>
#include <cell/spurs/common.h>
#include <cell/spurs/job_chain.h>
#include <cell/spurs/job_context.h>
#include <vmx2spu.h>
#include <spu_printf.h> 
#include <string.h>

namespace rage {

void geometrybuilderspu(sysTaskContext& context, CellSpursJobContext2* jobContext, CellSpursJob256 *job)
{
	void* scratchBuffer = context.GetScratch(0);

	BuilderTaskParams* params = (BuilderTaskParams*)context.GetInputAs<BuilderTaskParams>();
	bgGeometryData* pGeometryData = (bgGeometryData*)context.GetInput(params->GeometryDataSize);
	datResource resource(pGeometryData, (void*)params->GeometryDataEa, params->GeometryDataSize);
	// fixup all pointers
	bgGeometryData::Place(pGeometryData, resource);
	u16* indexBuffer = (u16*)scratchBuffer;

	bgGeometryBuilder geometryBuilder(*pGeometryData);

	// Internal geometry builder setup
	geometryBuilder.m_pIndices = indexBuffer;

	geometryBuilder.m_vb.p = (void*)params->OutputVertexBuffeEa;
	geometryBuilder.m_vb.stride = params->StreamInfo.Stride;
	geometryBuilder.m_vb.position.offset = params->StreamInfo.PositionOffset;
	geometryBuilder.m_vb.position.size = params->StreamInfo.PositionSize;
	geometryBuilder.m_vb.normal.offset = params->StreamInfo.NormalOffset;
	geometryBuilder.m_vb.normal.size = params->StreamInfo.NormalSize;
	geometryBuilder.m_vb.tangent.offset = params->StreamInfo.TangentOffset;
	geometryBuilder.m_vb.tangent.size = params->StreamInfo.TangentSize;
	geometryBuilder.m_vb.texture0.offset = params->StreamInfo.TextureOffset0;
	geometryBuilder.m_vb.texture0.size = params->StreamInfo.TextureSize0;
	geometryBuilder.m_vb.texture1.offset = params->StreamInfo.TextureOffset1;
	geometryBuilder.m_vb.texture1.size = params->StreamInfo.TextureSize1;
	geometryBuilder.m_vb.texture2.offset = params->StreamInfo.TextureOffset2;
	geometryBuilder.m_vb.texture2.size = params->StreamInfo.TextureSize2;
	geometryBuilder.m_vb.diffuse.offset = params->StreamInfo.DiffuseOffset;
	geometryBuilder.m_vb.diffuse.size = params->StreamInfo.DiffuseSize;
	geometryBuilder.CreateAllGeometry();

	// Write out generated buffer
	sysDmaLargePutAndWait(geometryBuilder.m_pIndices, params->OutPutIndexBufferEa, RAGE_ALIGN(geometryBuilder.m_currentIndex * sizeof(u16), 4), BG_DMA_TAG);
}

void bgGeometryBuilder::SetVertex(
	const rage::Vector3& in_position,
	const rage::Vector3& in_normal,
	const rage::Vector3& in_tangent,
	const rage::Vector2& in_texCoord0,
	const rage::Vector2& in_texCoord1,
	const rage::Vector2& in_texCoord2,
	const u32 in_deviceColor)
{
	sysDmaWaitTagStatusAll(1 << BG_DMA_TAG);

	static const std::size_t MAX_VERTEX_STRIDE = 128;
	static char vertexBuffer[MAX_VERTEX_STRIDE];

	Assert(m_vb.stride < MAX_VERTEX_STRIDE);

	std::memcpy(vertexBuffer + m_vb.position.offset, &in_position, m_vb.position.size);
	std::memcpy(vertexBuffer + m_vb.normal.offset, &in_normal, m_vb.normal.size);
	std::memcpy(vertexBuffer + m_vb.tangent.offset, &in_tangent, m_vb.tangent.size);
	std::memcpy(vertexBuffer + m_vb.texture0.offset, &in_texCoord0, m_vb.texture0.size);
	std::memcpy(vertexBuffer + m_vb.texture1.offset, &in_texCoord1, m_vb.texture1.size);
	std::memcpy(vertexBuffer + m_vb.texture2.offset, &in_texCoord2, m_vb.texture2.size);
	std::memcpy(vertexBuffer + m_vb.diffuse.offset, &in_deviceColor, m_vb.diffuse.size);

	char *ea(static_cast<char *>(m_vb.p) + m_vb.stride * m_currentVertex++);
	sysDmaLargePut(vertexBuffer, (uint64_t)ea, m_vb.stride, BG_DMA_TAG);
}

} // namespace rage

#elif __PPU // #if __SPU

#include "system/task.h"

DECLARE_TASK_INTERFACE(geometrybuilderspu);

namespace rage {

static const u32 kStackSize = 60*1024;	// TODO: Dial this in
static BuilderTaskParams s_Params ALIGNED(128);


bool bgGeometryBuilder_AddGeometryBuilderJob(
	u32 GeometryDataEa,
	u32 GeometryDataSize,
	bgGeometryBuilder::VertexBuffer& VbDesc,
	u32 IndexBufferEa, 
	u32 IndexBufferSize,
	u32 VertexBufferEa,
	u32 VertexBufferSize)
{
	u32 kScratchSize = RAGE_ALIGN(IndexBufferSize, 4);

	// Grab necessary data from bgBreakable to prepare dma
	s_Params.StreamInfo.Stride = VbDesc.stride;
	s_Params.StreamInfo.PositionOffset = VbDesc.position.offset;
	s_Params.StreamInfo.PositionSize = VbDesc.position.size;
	s_Params.StreamInfo.NormalOffset = VbDesc.normal.offset;
	s_Params.StreamInfo.NormalSize = VbDesc.normal.size;
	s_Params.StreamInfo.TangentOffset = VbDesc.tangent.offset;
	s_Params.StreamInfo.TangentSize = VbDesc.tangent.size;
	s_Params.StreamInfo.TextureOffset0 = VbDesc.texture0.offset;
	s_Params.StreamInfo.TextureSize0 = VbDesc.texture0.size;
	s_Params.StreamInfo.TextureOffset1 = VbDesc.texture1.offset;
	s_Params.StreamInfo.TextureSize1 = VbDesc.texture1.size;
	s_Params.StreamInfo.TextureOffset2 = VbDesc.texture2.offset;
	s_Params.StreamInfo.TextureSize2 = VbDesc.texture2.size;
	s_Params.StreamInfo.DiffuseOffset = VbDesc.diffuse.offset;
	s_Params.StreamInfo.DiffuseSize = VbDesc.diffuse.size;

	s_Params.GeometryDataEa = GeometryDataEa;
	s_Params.GeometryDataSize = GeometryDataSize;

	s_Params.OutPutIndexBufferEa = IndexBufferEa;
	s_Params.OutputIndexBufferSize = IndexBufferSize;
	s_Params.OutputVertexBuffeEa = VertexBufferEa;
	s_Params.OutputVertexBufferSize = VertexBufferSize;
	sysTaskContext context(TASK_INTERFACE(geometrybuilderspu), 0, kScratchSize, kStackSize);
	context.SetInputOutput();

	context.AddInput(&s_Params, RAGE_ALIGN(sizeof(BuilderTaskParams), 4));// inputSizes[BuilderTaskDma::kParams]);
	context.AddInput((void *)GeometryDataEa, GeometryDataSize);

	if (sysTaskHandle handle = context.Start())
	{
		sysTaskManager::Wait(handle);

		return true;
	}
	else
	{
		return false;
	}
}

} // namespace rage

#endif // #elif __PPU

#else // BREAKABLE_GLASS_ON_SPU

#if __SPU
namespace rage {

// stub, just so link doesn't fail
void geometrybuilderspu(sysTaskContext& context, CellSpursJobContext2* jobContext, CellSpursJob256 *job)
{
}

}
#endif


#endif // BREAKABLE_GLASS_ON_SPU

#endif // __PS3
