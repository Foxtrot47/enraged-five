// 
// breakableglass/geometrydata.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_GEOMETRYDATA_H 
#define BREAKABLEGLASS_GEOMETRYDATA_H 

#include "piecegeometry.h"

#include "atl/bitset.h"

namespace rage {

class bgCrackType;
class bgCrackStarMap;

class bgHitInfo
{
public:
#if !__SPU
	const bgCrackType* GetCrackType(int in_glassType, int in_glassSizeIndex) const;
	const bgCrackStarMap* GetCrackStarMap(int in_glassType, int in_glassSizeIndex) const;
#endif

	//2d location of impact - Value are between 0,0 and 1,1
	Vector2 m_location;
	// Crack map vertices will be scaled by this value.
	Vector2 m_scaleData;
	//3D world location of impact
	Vector3 m_worldImpactImpulse;
	//How much to translate the crack map - Value are between 0,0 and 1,1
	// Values:
	//		0,0 = center of the crack map us located at the top-left corner of the pane of glass
	//		1,1 = center of the crack map is located at the bottom-right corner of the pane of glass
	// Why do we not use m_location for this anymore: Because we do not always want to have the center
	//		of the crack map located at the impact location.
	Vector2	m_crackMapTranslation;
	float m_rotation;
	float m_decalTextureScale;
	u8    m_crackType;
	u8    m_starMapIndex;
	u8    m_decalChannelIndex;	
	u8    m_pad;
};


class bgGeometryData
{
public:
#if __SPU
	bgGeometryData(datResource&rsc)
		: m_hitInfo(rsc)
		, m_pointDecalVisibility(rsc)
		, m_pointCrackVisibility(rsc)
		, m_paneEdges(rsc)
		, m_panePieces(rsc, true)
		, m_panePoints(rsc)
		, m_paneDepths(rsc)
		, m_fallingPieces(rsc, true)
	{
	}

	static void Place(void *that, datResource& rsc)
	{
		::new (that) bgGeometryData(rsc);
	}
#else
	bgGeometryData() {}

	explicit bgGeometryData(const bgGeometryData& src)
	{
		*this = src;
	}

	bgGeometryData& operator =(const bgGeometryData& rhs);

#endif

	void Cleanup()
	{
	}

	Vector3::Return Transform(const Vector2& in_point) const
	{
		return m_posTopLeft + in_point.x * m_posWidth + in_point.y * m_posHeight;
	}

	unsigned ChoosePaneMatrix(const Vector2& in_point) const;

	const Vector3& GetPosTopLeft() const
	{
		return m_posTopLeft;
	}
	const Vector3& GetPosWidth() const
	{
		return m_posWidth;
	}

	const Vector3& GetPosHeight() const
	{
		return m_posHeight;
	}

	const atArray<bgEdge>& GetPaneEdges() const
	{
		return m_paneEdges;
	}
	const atArray<Vector2>& GetPanePoints() const
	{
		return m_panePoints;
	}
	const atArray<float>& GetPaneDepths() const
	{
		return m_paneDepths;
	}
	const atArray<bgPanePiece>& GetPanePieces() const
	{
		return m_panePieces;
	}

	const atArray<bgFallingPiece>& GetFallingPieces() const
	{
		return m_fallingPieces;
	}

	const atArray<bgHitInfo>& GetHitInfo() const
	{
		return m_hitInfo;
	}

	const atBitSet& GetPointDecalVisibility() const
	{
		return m_pointDecalVisibility;
	}

	const atBitSet& GetPointCrackVisibility() const
	{
		return m_pointCrackVisibility;
	}

	int GetLod() const
	{
		return m_lod;
	}

	float GetThickness() const
	{
		return m_thickness;
	}

	float GetCrackSize() const
	{
		return m_crackSize;
	}
	
	float GetBevelSize() const
	{
		return m_bevelSize;
	}

	bool IsPointDecalVisible(int index) const
	{
		return m_pointDecalVisibility.IsSet(index);
	}

	bool IsPointCrackVisible(int index) const
	{
		return m_pointCrackVisibility.IsSet(index);
	}

	const bgHitInfo* GetHit(int hitIndex) const
	{
		if ((hitIndex < 0) ||
			(hitIndex >= m_hitInfo.GetCount()))
		{
			return NULL;
		}
		return &m_hitInfo[hitIndex];
	}

	int GetHitCount() const
	{
		return m_hitInfo.GetCount();
	}

	bool IsReinforced() const
	{
		return m_isReinforced;
	}

public:
	float m_crackSize;
	float m_bevelSize;
	int m_lod;
	float m_thickness;
	bool m_isReinforced;
	Vector3 m_posTopLeft;
	Vector3 m_posWidth;
	Vector3 m_posHeight;

	atArray<bgHitInfo> m_hitInfo;
	atArray<bgEdge> m_paneEdges;
	atArray<bgPanePiece> m_panePieces;
	atArray<Vector2> m_panePoints;
	atArray<float> m_paneDepths;
	atArray<bgFallingPiece> m_fallingPieces;

	//----------------------------------
	//Crack and decal visibility:
	//
	//	Each point is tested for visibility and the results are stored here.  This
	//	flag is use for the pane decal visibility and the 3D cracks visibility
	//	simultaneously.
	//		0 = cracks and decal not visible
	//		1 = cracks and decal visible
	atBitSet m_pointDecalVisibility;
	atBitSet m_pointCrackVisibility;
}; // class bgGeometryData

} // namespace rage

#endif // BREAKABLEGLASS_GEOMETRYDATA_H 
