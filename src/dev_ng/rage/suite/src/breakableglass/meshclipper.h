// 
// breakableglass/meshclipper.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_MESHCLIPPER_H 
#define BREAKABLEGLASS_MESHCLIPPER_H 

#include "edge.h"
#include "swapvector.h"
#include "atl/array.h"
#include "atl/map.h"
#include "vector/vector2.h"

namespace rage {

	class bgMeshPolygon
	{
	public:
		explicit bgMeshPolygon(int id = 0) : m_pieceId(id)
		{
		}

		void Clear()
		{
			m_edges.clear();
			m_pieceId = 0;
		}

		bool Empty() const
		{
			return m_edges.empty();
		}

		void ReserveEdgeCount(bgIndex count)
		{
			m_edges.reserve(count);
		}

		bgIndex GetEdgeCount() const
		{
			return bgIndex(m_edges.size());
		}

		void Add(bgEdgeRef edge)
		{
			m_edges.push_back(edge);
		}

		void PieceId(int id)
		{
			m_pieceId = id;
		}

		int PieceId() const
		{
			return m_pieceId;
		}

		bgEdgeRef &operator [](size_t index)
		{
			return m_edges[index];
		}

		bgEdgeRef const &operator [](size_t index) const
		{
			return m_edges[index];
		}

		void Swap(bgMeshPolygon &polygon)
		{
			m_edges.swap(polygon.m_edges);
			std::swap(m_pieceId, polygon.m_pieceId);
		}

	private:
		typedef stlVector<bgEdgeRef> EdgeRefs;
		EdgeRefs m_edges;
		// glass piece id, so polys can be glommed back together easily at the end...
		int m_pieceId; 
	};

	template<>
	struct bgSwapClearer<bgMeshPolygon>
	{
		void Clear(bgMeshPolygon &polygon) const
		{
			polygon.Clear();
		}
	};

	inline void bgSwap(bgMeshPolygon &lhs, bgMeshPolygon &rhs)
	{
		lhs.Swap(rhs);
	}

	class bgMesh
	{
	public:
		typedef bgIndex Index;
		typedef bgEdge Edge;
		typedef bgEdgeRef EdgeRef;
		typedef bgEdgeKey EdgeKey;
		typedef bgEdgeHash EdgeHash;
		typedef bgMeshPolygon Polygon;

		typedef Vector2 Point;

		typedef atArray<Point> Points;
		typedef stlVector<Edge> Edges;
		typedef bgSwapVector<Polygon> Polygons;

		void Clear()
		{
			m_points.Reset();
			m_edges.clear();
			m_polygons.clear();
		};

		// Retrieve the mesh's content and clear it.
		void Clear(Points &points, Edges &edges, Polygons &polygons)
		{
			m_points.Swap(points);
			m_edges.swap(edges);
			m_polygons.swap(polygons);
			Clear();
		};

		// Add a point to the mesh and return its index.
		Index Add(Point const &point)
		{
			Index index(GetPointCount());
			m_points.Grow(/*GetPointCount() + 1*/) = point;
			return index;
		}

		// GetPoints allows modification of point locations.
		// This does not invalidate edges or polygons.
		Point *GetPoints()
		{
			return m_points.empty() ? 0 : &m_points[0];
		}

		Point const *GetPoints() const
		{
			return m_points.empty() ? 0 : &m_points[0];
		}

		Index GetPointCount() const
		{
			return Index(m_points.GetCount());
		}

		void ReservePointCount(Index count)
		{
			m_points.Reserve(count);
		}

		// SetPointCount could be used to reduce the number of points in the mesh,
		// potentially invalidating edges and polygons.
		void SetPointCount(Index count)
		{
			m_points.Resize(count);
		}

		// SwapPoints could be used to reduce the number of points in the mesh,
		// potentially invalidating edges and polygons.
		void SwapPoints(Points &points)
		{
			m_points.Swap(points);
		}

		// Add an edge to the mesh and return a reference.
		// Assumes the edge refers to valid points in the mesh.
		EdgeRef Add(Edge const &edge)
		{
			Assert(edge[0] < GetPointCount());
			Assert(edge[1] < GetPointCount());
			Assert(edge.Left() < GetPolygonCount());
			Assert(edge.Right() < GetPolygonCount() || edge.Right() == Edge::kPolygonRef_None);
			EdgeRef ref((EdgeRef(GetEdgeCount())));
			m_edges.push_back(edge);
			return ref;
		}

		Edge const &GetEdge(EdgeRef ref) const
		{
			Assert(!m_edges.empty());
			return m_edges[ref.GetEdgeIndex()];
		}

		Index GetEdgeCount() const
		{
			return Index(m_edges.size());
		}

		void ReserveEdgeCount(Index count)
		{
			m_edges.reserve(count);
		}

		// SetEdge could be used to change an edge to refer to invalid points or polygons.
		void SetEdge(EdgeRef ref, Edge const &edge)
		{
			Index index(ref.GetEdgeIndex());
			Assert(index < GetEdgeCount());
			Assert(edge[0] < GetPointCount());
			Assert(edge[1] < GetPointCount());
			Assert(edge.Left() < GetPolygonCount());
			Assert(edge.Right() < GetPolygonCount() || edge.Right() == Edge::kPolygonRef_None);
			m_edges[index] = edge;
		}

		// SetEdgeCount could be used to reduce the number of edges in the mesh,
		// potentially invalidating polygons.
		void SetEdgeCount(Index count)
		{
			m_edges.resize(count);
		}

		// Give edge references access to the mesh's edges.
		Index GetP0(EdgeRef ref) const
		{
			return ref.GetP0(m_edges);
		}

		Index GetP1(EdgeRef ref) const
		{
			return ref.GetP1(m_edges);
		}

		Index GetLeftPoly(EdgeRef ref) const
		{
			return ref.GetLeftPoly(m_edges);
		}

		Index GetRightPoly(EdgeRef ref) const
		{
			return ref.GetRightPoly(m_edges);
		}

		void SetP0(EdgeRef ref, Index in_point)
		{
			Assert(in_point < GetPointCount());
			ref.SetP0(m_edges, in_point);
		}

		void SetP1(EdgeRef ref, Index in_point)
		{
			Assert(in_point < GetPointCount());
			ref.SetP1(m_edges, in_point);
		}

		void SetLeftPoly(EdgeRef ref, Index in_iPoly)
		{
			Assert(in_iPoly < GetPolygonCount());
			ref.SetLeftPoly(m_edges, in_iPoly);
		}

		void SetRightPoly(EdgeRef ref, Index in_iPoly)
		{
			Assert(in_iPoly < GetPolygonCount());
			ref.SetRightPoly(m_edges, in_iPoly);
		}

		// SwapEdges could be used to reduce the number of edges in the mesh,
		// potentially invalidating polygons.  The new edges could reference invalid points.
		void SwapEdges(Edges &edges)
		{
			m_edges.swap(edges);
		}

		// AddPolygon adds an empty polygon with the specified id and returns its index.
		Index AddPolygon(int id)
		{
			Index index(GetPolygonCount());
			m_polygons.push_back(Polygon(id));
			return index;
		}

		Polygon const &GetPolygon(size_t index) const
		{
			return m_polygons[index];
		}

		Index GetPolygonCount() const
		{
			return Index(m_polygons.size());
		}

		void ReservePolygonCount(Index count)
		{
			m_polygons.reserve(count);
		}

		// SwapEdges could be used to add polygons containing invalid edges.
		void SwapPolygons(Polygons &polygons)
		{
			m_polygons.swap(polygons);
		}

		void ReservePolygonEdgeCount(Index index, Index count)
		{
			m_polygons[index].ReserveEdgeCount(count);
		}

		void AddPolygonEdge(Index index, EdgeRef ref)
		{
			Assert(ref.GetEdgeIndex() < GetEdgeCount());
			m_polygons[index].Add(ref);
		}

		void PolygonPieceId(Index index, int id)
		{
			m_polygons[index].PieceId(id);
		}

	private:
		Points m_points;
		Edges m_edges;
		Polygons m_polygons;
	};

	void bgClipMesh(bgMesh& io_mesh, Vector2 const *pPoints, size_t size);

	template<size_t size>
	void bgClipMesh(bgMesh& io_mesh, Vector2 const (&points)[size])
	{
		bgClipMesh(io_mesh, points, size);
	}

} // namespace rage

#endif // BREAKABLEGLASS_MESHCLIPPER_H 
