//
// breakableglass/geometrybuilder.cpp 
// 
// Copyright (C) 2008-2012 Rockstar Games.  All Rights Reserved. 
//

#include "geometrybuilder.h"
#include "geometrybuildercommon.h"

#include "bgdrawable.h"
#include "breakable.h"
#include "geometrydata.h"
#include "crackstemplate.h"
#include "optimisations.h"

#include "grcore/indexbuffer.h"
#include "math/random.h"

#if __PS3
#include "geometrybuilderspu.h"
#endif // __PS3

// optimisations
BG_OPTIMISATIONS()

namespace rage {

	extern u32 g_AllowVertexBufferVramLocks;

#if __PS3 && BREAKABLE_GLASS_ON_SPU

// Special lock helper to disable the vram lock warning:
// There's no performance penalty because the spu has speedy access to vram
class bgVertexBufferLockHelper
{
public:
	bgVertexBufferLockHelper(grcVertexBuffer* in_pVertexBuffer) : m_helper(in_pVertexBuffer) 
	{
	}

	void* GetLockPtr()
	{
		return m_helper.GetLockPtr();
	}
protected:
	class VRamBufferLockAssertSuppressor
	{
	public:
		VRamBufferLockAssertSuppressor()
		{
			++g_AllowVertexBufferVramLocks;
		}
		~VRamBufferLockAssertSuppressor()
		{
			--g_AllowVertexBufferVramLocks;
		}
	} m_VRamBufferLockAssertSuppressor;
	grcVertexBuffer::LockHelper m_helper;
};
#else //__PS3 && BREAKABLE_GLASS_ON_SPU
	// Not using spu: leave vram lock warnings enabled
	typedef grcVertexBuffer::LockHelper bgVertexBufferLockHelper;
#endif//__PS3 && BREAKABLE_GLASS_ON_SPU

void bgGeometryBuilder::SetBuffers(const bgGeometryBuilder::VertexBuffer& in_vb, u16* in_pIndices)
{
	m_vb = in_vb;
	m_pIndices = in_pIndices;
}

// Macro to avoid unused parameter warnings
#if __PS3 && BREAKABLE_GLASS_ON_SPU
#define BREAKABLE_GLASS_ON_SPU_ONLY(x) x
#else
#define BREAKABLE_GLASS_ON_SPU_ONLY(x)
#endif

//------------------------------------------------------------------------------
//This function take the pane information (hits, 2d cracks, 2d pieces) and create 
// 3D geometry and place it in the vertex buffer
//
// Return: Number of vertex in the vertex buffer
//------------------------------------------------------------------------------
bgGpuBuffers* bgGeometryBuilder::CreatePane(
	const bgGeometryData& in_geometryData, 
	u32 BREAKABLE_GLASS_ON_SPU_ONLY(in_geometryDataSize), 
	const grcFvf& in_fvf, int* arrPieceIndexCount, int* arrCrackIndexCount)
{
	// No data, don't bother
	if (in_geometryData.GetPanePieces().GetCount() + in_geometryData.GetFallingPieces().GetCount() == 0)
	{
		return NULL;
	}

	int numIndices, numVertices, numPaneIndices;
	// Figure out the size of the buffers we require
	GetVertexIndexCountForAllSolidPieces(in_geometryData, numVertices, numPaneIndices, numIndices, arrPieceIndexCount, arrCrackIndexCount);

	// Make sure we got valid data
	if (numVertices <= 0 || numIndices <= 0)
	{
		return NULL;
	}

	// Make sure we successfully allocate vertex and index buffers
	// (this will automatically release any buffers previously used by the drawable)
	bgGpuBuffers* pBuffers = bgDrawable::AllocateBuffers(numVertices, numIndices, in_fvf);
	if (NULL == pBuffers)
	{
		return NULL;
	}

	// grab the index buffer
	// index buffers must always be locked before vertex buffers, this ensures
	// that we don't get a deadlock by threads locking in a different order.
	grcIndexBuffer* pIndexBuffer(pBuffers->GetIndexBuffer());
	u16* pIndices = pIndexBuffer->LockRW();

	// grab the vertex buffer
	g_AllowVertexBufferVramLocks++; // We are going to lock a static buffer
	VertexBuffer vb;
	grcVertexBuffer *pVertexBuffer(pBuffers->GetVertexBuffer());
	bgVertexBufferLockHelper helper(pVertexBuffer);
	vb.p = helper.GetLockPtr();
	vb.stride = pVertexBuffer->GetVertexStride();

	// Position
	grcFvf const &fvf(*pVertexBuffer->GetFvf());
	Assert(fvf.GetPosChannel());
	vb.position.offset = fvf.GetOffset(grcFvf::grcfcPosition);
	vb.position.size = std::min(sizeof(Vector3), size_t(fvf.GetSize(grcFvf::grcfcPosition)));

	// Normal
	Assert(fvf.GetNormalChannel());
	vb.normal.offset = fvf.GetOffset(grcFvf::grcfcNormal);
	vb.normal.size = std::min(sizeof(Vector3), size_t(fvf.GetSize(grcFvf::grcfcNormal)));

	// Tangent
	Assert(!fvf.GetTangentChannel(0));

	// Texture 0
	Assert(fvf.GetTextureChannel(0));
	vb.texture0.offset = fvf.GetOffset(grcFvf::grcfcTexture0);
	vb.texture0.size = std::min(sizeof(Vector2), size_t(fvf.GetSize(grcFvf::grcfcTexture0)));

	// Texture 1
	Assert(fvf.GetTextureChannel(1));
	vb.texture1.offset = fvf.GetOffset(grcFvf::grcfcTexture1);
	vb.texture1.size = std::min(sizeof(Vector2), size_t(fvf.GetSize(grcFvf::grcfcTexture1)));

	// Texture 2
	Assert(!fvf.GetTextureChannel(2));

	// Diffuse
	Assert(fvf.GetDiffuseChannel());
	vb.diffuse.offset = fvf.GetOffset(grcFvf::grcfcDiffuse);
	vb.diffuse.size = std::min(sizeof(Color32), size_t(fvf.GetSize(grcFvf::grcfcDiffuse)));

	// Actually create the geometry:
	//   faces and sides/cracks for both falling pieces and pieces still on the pane
#if __PS3 && BREAKABLE_GLASS_ON_SPU
	if (!bgGeometryBuilder_AddGeometryBuilderJob(
		(u32)(&in_geometryData), 
		in_geometryDataSize,
		vb, 
		(u32)pIndices, 
		sizeof(u16) * numIndices, 
		(u32)vb.p, 
		numVertices * vb.stride))
	{
		pIndexBuffer->UnlockRW();
		pBuffers->Release();
		return NULL;
	}
#else
	{
		bgGeometryBuilder builder(in_geometryData);
		builder.SetBuffers(vb, pIndices);
		builder.CreateAllGeometry();
		Assert(builder.m_currentVertex == numVertices);
		Assert(builder.m_currentIndex == numIndices);
	}

	// make sure the values we precomputed were correct
#endif // !__SPU

	// Done writing indices
	pIndexBuffer->UnlockRW();

	// tell drawable how many indices belong to the pane
	pBuffers->SetPaneIndexCount(numPaneIndices);

	g_AllowVertexBufferVramLocks--; // No more static buffer lock

	return pBuffers;
}//CreatePane


void bgGeometryBuilder::GetVertexIndexCountForOnePiece(int in_lod, int in_numVerts, int& io_numVertices, int& io_numPaneIndices, int& io_numIndices)
{
	int numVerts = in_numVerts;
	int numTriangles = numVerts - 2;
	//Side 1 of the pane
	io_numVertices += numVerts;
	io_numIndices += numTriangles*3;
	io_numPaneIndices += numTriangles*3;
	// for LOW lod, that's it; we're done
	if (in_lod <= bgLod::LOD_LOW)
		return;
	
	// Side 2 of the pane for high quality LOD only
	if (in_lod >= bgLod::LOD_HIGH)
	{
		io_numVertices += numVerts;
		io_numIndices += numTriangles*3;
		io_numPaneIndices += numTriangles*3;
	}

#ifdef DRAW_CRACKS
	// 'Crack' geometry
	switch (in_lod)
	{
	case bgLod::LOD_MED:
		{
			// We need two extra vertices per-point in addition to the normal
			// front/back vertices to make the bevel and crack.
			if(numVerts>0)
			{
				// extra vertices at the end to prevent weird texture wrapping
				io_numVertices += ((numVerts+1)*2);
			}

			// for each edge in the silhouette create 2 triangles with 3 indices 
			io_numIndices += (numVerts*2*3);

		}
		break;
	case bgLod::LOD_HIGH:
		{
			// We need two extra vertices per-point in addition to the normal
			// front/back vertices to make the bevel and crack.
			if(numVerts>0)
			{
				// extra vertices at the end to prevent weird texture wrapping
				io_numVertices += ((numVerts+1)*3);
			}

			// We create 4 triangles with 3 indices per-edge for the crack/bevel
			io_numIndices += (numVerts*4*3);
		}
		break;
	}
#endif // DRAW_CRACKS
}


//------------------------------------------------------------------------------
//Get how many triangle will be needed when creating the remaining pieces on the window
//
// input:	in_lod	==>  see enum eGlassLod
//			in_fallingPieces ==> information about falling pieces animated using 'skinning' matrices
//			in_panePieces    ==> information about pieces still attached to the pane
//
// output:  in_rNumVerts ==> Number of vertices needed
//          in_rNumIndices ==> Number of indices needed
//------------------------------------------------------------------------------
void bgGeometryBuilder::GetVertexIndexCountForAllSolidPieces(const bgGeometryData& in_geometryData, int& out_numVertices, int& out_numPaneIndices, int& out_numIndices, int* arrPieceIndexCount, int* arrCrackIndexCount)
{
	// A flattened out view of the geometry (HIGH LOD)
	//
	//  |  / | <Front Pane Geometry>
	//  | /  |
	//  0 -- 1  Front Pane
	//  | 1/ | <Front Bevel Geometry>
	//  | /2 |
	//  2 -- 3  Bevel
	//  | 3/ | <Back Bevel Geometry>
	//  | /4 |
	//  3 -- 4  Back Pane
	//  |  / |<Back Pane Geometry>
	//  | /  |

	// Clear
	out_numVertices = 0;
	out_numIndices = 0;
	out_numPaneIndices = 0;
	int lod = in_geometryData.GetLod();

	//Iterate over all the pane pieces
	const atArray<bgPanePiece>& panePieces(in_geometryData.GetPanePieces());
	for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++)
	{
		//If this piece is not active we pass
		const bgPanePiece& piece = panePieces[iPanePiece];
		if (piece.m_flags & bgPanePiece::kPF_Active)
		{
			GetVertexIndexCountForOnePiece(lod, piece.m_silhouette.GetCount(), out_numVertices, out_numPaneIndices, out_numIndices);
		}
	}//for iPanePiece

	//Iterate over all the falling pieces
	const atArray<bgFallingPiece>& fallingPieces(in_geometryData.GetFallingPieces());
	if(fallingPieces.GetCount() > 0)
	{
		// Keep track of batch related data
		int iCurBatch = 0;
		int prevNumPaneIndices = 0;
		int prevNumCrackIndices = 0;
		
		// Go over all the faling pieces and count the indices needed for those that are still alive
		int iFallingPiece = 0;
		for (; iFallingPiece < fallingPieces.GetCount(); iFallingPiece++)
		{
			//If this piece is not active we pass
			const bgFallingPiece& piece = fallingPieces[iFallingPiece];
			if (piece.m_life >= 0.f)
			{
				GetVertexIndexCountForOnePiece(lod, piece.m_points.GetCount(), out_numVertices, out_numPaneIndices, out_numIndices);
			}

			// Check if this is the last falling piece thats going to make it into the batch (+2 => 1 for the pane pieces and 1 for the current piece)
			if(((iFallingPiece + 2) % SKINNING_COUNT) == 0)
			{
				arrPieceIndexCount[iCurBatch] = out_numPaneIndices - prevNumPaneIndices;
				arrCrackIndexCount[iCurBatch] = (out_numIndices - out_numPaneIndices) - prevNumCrackIndices;
				prevNumPaneIndices = out_numPaneIndices;
				prevNumCrackIndices = out_numIndices - out_numPaneIndices;
				iCurBatch++;
				Assertf(iCurBatch <= bgBreakable::sm_iMaxBatches, "Breakable is trying to use too many batches - this will end up in a crash");
			}
		}//for iFallingPiece

		// Add the last batch if needed
		if(((iFallingPiece + 1) % SKINNING_COUNT) != 0)
		{
			arrPieceIndexCount[iCurBatch] = out_numPaneIndices - prevNumPaneIndices;
			arrCrackIndexCount[iCurBatch] = (out_numIndices - out_numPaneIndices) - prevNumCrackIndices;
		}
	}
	else
	{
		// No falling pieces - only pane pieces in the batch
		arrPieceIndexCount[0] = out_numPaneIndices;
		arrCrackIndexCount[0] = out_numIndices - out_numPaneIndices;
	}
}//GetVertexIndexCountForAllSolidPieces


} // namespace rage
