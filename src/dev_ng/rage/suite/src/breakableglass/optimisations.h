//
// breakableglass/optimisations.h
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef BG_OPTIMISATIONS_H
#define BG_OPTIMISATIONS_H

#define BG_OPTIMISATIONS_OFF		0

#if BG_OPTIMISATIONS_OFF
#define BG_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else
#define BG_OPTIMISATIONS()
#endif	

#endif // BG_OPTIMISATIONS_H