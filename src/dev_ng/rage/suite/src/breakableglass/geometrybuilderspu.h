// 
// breakableglass/geometrybuilderspu.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_GEOMETRYBUILDERSPU_H
#define BREAKABLEGLASS_GEOMETRYBUILDERSPU_H

#if __PS3

#include "geometrybuilder.h"

namespace rage {

bool bgGeometryBuilder_AddGeometryBuilderJob(
	u32 GeometryDataEa, 
	u32 GeometryDataSize,
	bgGeometryBuilder::VertexBuffer& VbDesc,
	u32 IndexBufferEa, 
	u32 IndexBufferSize,
	u32 VertexBufferEa,
	u32 VertexBufferSize);

} // namespace rage
#endif // __PS3

#endif // BREAKABLEGLASS_GEOMETRYBUILDERSPU_H
