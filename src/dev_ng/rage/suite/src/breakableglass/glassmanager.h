// 
// breakableglass/glassmanager.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_GLASSMANAGER_H 
#define BREAKABLEGLASS_GLASSMANAGER_H 

#include "crackstemplate.h"
#include "atl/delegate.h"
#include "phcore/materialmgr.h"
#include "vectormath/mat34v.h"
#include "vectormath/vec3v.h"
#include "grprofile/drawcore.h"

#define RAGE_GLASS_USE_PRIVATE_HEAP 0
#define RAGE_GLASS_USE_USER_HEAP (__XENON || __PPU)

#define RAGE_GLASS_USE_HEAP (RAGE_GLASS_USE_PRIVATE_HEAP || RAGE_GLASS_USE_USER_HEAP)

// Currently experimental
// Ditching the BVH saves memory which lets us reduce the size of the private heap
#define BREAKABLE_GLASS_USE_BVH 1

namespace rage {

class bgBreakable;
class bgDrawable;
class bgPaneModelInfoBase;
class bkBank;
class fragInst;
class grcInstanceData;
class grcViewport;
class grmShader;
class phGlassInst;
class phInst;
class phBound;
class spdAABB;
class sysMemAllocator;

typedef s16 bgGlassHandle;
static const s16 bgGlassHandle_Invalid = -1;

struct bgContactReport
{
	fragInst *pFragInst;
	bgGlassHandle handle;
	u8 childIdx;
	bool firstContact;
};


typedef atDelegate<void (phBound&)> bgInitGlassBoundForGameFunctor;
typedef atDelegate<void (phGlassInst&)> bgInitGlassInstForGameFunctor;
typedef atDelegate<void (phGlassInst&)> bgShutdownGlassInstForGameFunctor;
typedef atDelegate<void (phInst&)> bgInitShardInstForGameFunctor;
typedef atDelegate<void (bgGlassHandle)> bgGlassImpactFunc;
typedef atDelegate<const grcInstanceData& (int shaderIndex)>  bgGetInstanceDataFunc;
typedef atDelegate<void (bgContactReport *report)>  bgContactFunctor;

#ifdef GTA_REPLAY_RAGE
class glassRecorderInterface
{
public:
	virtual void OnCreateBreakableGlass(const phInst* pInst, int groupIndex, int boneIndex, Vec3V_In position, Vec3V_In impact, int glassCrackType) = 0;
	virtual void OnHitGlass(bgGlassHandle handle, int crackType, Vec3V_In position, Vec3V_In impulse)  = 0;
	virtual void OnTransferGlass(const phInst* pSourceInst, const phInst* pDestInst, u16 groupIndex) = 0;
};

class glassRecordingRage
{
public:
	static glassRecorderInterface* Get() { return sm_GlassRecorder; }
	static void Set(glassRecorderInterface* glassRecorder) { sm_GlassRecorder = glassRecorder; }

	static bool IsActive() { return sm_GlassRecorder != NULL; }
	
private:
	static glassRecorderInterface* sm_GlassRecorder;

};

#define GLASS_RECORDER_ONLY(...)			__VA_ARGS__
#define GLASS_RECORDER						1

#else
#define GLASS_RECORDER_ONLY(...)
#define GLASS_RECORDER						0
#endif


namespace bgGlassManager
{
	void InitClass();
	void ShutdownClass();

	bgGlassHandle GetHeadFreeList();

#if RAGE_GLASS_USE_PRIVATE_HEAP
	// in_maxActiveGlass: maximum supported individual panes.  Additional panes will cause older panes to be recycled.
	// in_bufferMemory: kilobytes of memory to reserve for gpu (vertex/index) buffers
	// in_heapMem: kilobytes of memory to reserve for bgBreakable system memory usage
	void Malloc(int in_maxActiveGlass, int in_bufferMemory, int in_heapMem);
#elif RAGE_GLASS_USE_USER_HEAP
	void Malloc(int in_maxActiveGlass, int in_heapMem);
	// in_maxActiveGlass: maximum supported individual panes.  Additional panes will cause older panes to be recycled.
	// in_bufferAllocator: allocator for gpu (vertex/index) buffers
	// in_heapMem: kilobytes of memory to reserve for bgBreakable system memory usage
	void Malloc(int in_maxActiveGlass, sysMemAllocator* in_bufferAllocator, int in_heapMem);
#endif

	// in_maxActiveGlass: maximum supported individual panes.  Additional panes will cause older panes to be recycled.
	// in_bufferMemory: kilobytes of memory to reserve for gpu (vertex/index) buffers
	// in_heapMem: kilobytes of memory to reserve for bgBreakable system memory usage
	void Malloc(int in_maxActiveGlass, int in_heapMem);

	void Free();

	void SetInitGlassBoundForGameFunctor(bgInitGlassBoundForGameFunctor in_functor);

	void SetInitGlassInstForGameFunctor(bgInitGlassInstForGameFunctor in_functor);
	void SetShutdownGlassInstForGameFunctor(bgShutdownGlassInstForGameFunctor in_functor);

	void SetInitShardInstForGameFunctor(bgInitShardInstForGameFunctor in_functor);
	void SetCleanUpGlassAudioFunctor(bgGlassImpactFunc in_functor);

	void CallInitShardInstForGameFunctor(phInst& inst);

	void SetContactFunctor(bgContactFunctor in_functor);

	void InitPhysicsInstPool(u16 numInsts);

	// Set the shader template for rendering broken glass
	void SetGlassShader(grmShader* in_pShader);

	void SetViewport(const grcViewport*);

	// Create a bgBreakable, bgDrawable, and phGlassInst
	// in_pHandle is cached and will be set to bgGlassHandle_Invalid
	// if either the glass cannot be allocated or needs to be recycled.
	// The memory pointed to by in_pHandle must remain valid until 
	// DestroyBreakableGlass() is called.
	bool CreateBreakableGlass(
		bgGlassHandle* in_pHandle,
		Mat34V_In in_glassTransform, 
		const bgPaneModelInfoBase& in_modelInfo, 
		bgGetInstanceDataFunc in_effectInstance, 
		phInst* in_ignoreInst,
		phMaterialMgr::Id in_materialId,
		float in_mass,
		int childIdx);

	void TransferBreakableGlassOwner(
		bgGlassHandle* originalHandle,
		bgGlassHandle* newHandle,
		phInst* newIgnoreInst);

	bool IsHandleValid(bgGlassHandle in_glassHandle);

	// Call from sim thread; graphics and physics object deletion is defered
	void DestroyBreakableGlass(bgGlassHandle in_glassHandle);

	// sim thread update
	void UpdateBreakableGlass(
		bgGlassHandle in_glassHandle, 
		Mat34V_In in_glassMatrix,
		float in_dt);

	// returns false if bound is not available
	bool GetSphereBound(bgGlassHandle in_glassHandle, Vec4V_Ref out_s);

	// returns false if bound is not available
	bool GetAABB(bgGlassHandle in_glassHandle, spdAABB& out_spdAABB);

	// DMZ update: copy matrices from breakable to drawable.
	void SyncBreakableGlass();

	// Clean out old drawables and phInsts.
	void ProcessDeferredDeletes();

	// @TODO: optimize: inline, which means exposing internal data structures.  sigh.
	bgDrawable& GetGlassDrawable(bgGlassHandle in_glassHandle);

	// @TODO: optimize: inline, which means exposing internal data structures.  sigh.
	bgBreakable& GetGlassBreakable(bgGlassHandle in_glassHandle);

	// get the physics inst of the breakable glass
	phInst* GetGlassPhysicsInst(bgGlassHandle in_glassHandle);

	// Impact a glass pane at to specified location
	void HitGlass(bgGlassHandle in_glassHandle, int in_crackType, Vec3V_In in_worldImpactLoc, Vec3V_In in_woldImpactImpulse);

	// Flag one or more sections of a pane's frame as missing
	void BreakFrame(bgGlassHandle in_glassHandle, int in_crackType, u32 in_frameFlags, Vec3V_In in_worldImpactImpulse);

	// Recycle (or defragment) until a block of the specified size is available on the private heap
	bool RecycleGlass(size_t in_memRequired);

	void UpdateBuffers();

	void SetMaxLod(int lod);

	int GetCrackTypeCount();

	bool GetLastImpactPositionAndMagnitude(bgGlassHandle in_glassHandle,Vec3V_Ref hitPosition,Vec3V_Ref hitMagnitude);

	// specify viewport data used to compute lod
	void SetViewportData(Vec3V_In in_cameraPos, int in_viewportSize, float in_tanFov);

	void SetSilentHit(bool bEnable);

	bool IsSilentHit();

#if __BANK
	void AddWidgets(bkBank& in_bank);

	void GetCracksTemplateMemoryUsage(int& usedGeomMemory, int& usedTexMemory);
	int GetNumActiveInstances();
	int GetNumFreeInstances();
	int GetDrawableMemoryStats();

	bool GetShowVisibleBreakableGlassEntities();
#endif // __BANK

#if __PFDRAW
	// Profile draw entry point
	void ProfileDraw();
#endif // __PFDRAW
}

} // namespace rage

#endif // BREAKABLEGLASS_GLASSMANAGER_H 
