// 
// breakableglass/physics.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "physics.h"

#include "glassmanager.h"
#include "bgchannel.h"
#include "optimisations.h"

#include "bank/bank.h"
#include "phcore/phmath.h"
#include "physics/simulator.h"
#include "physics/shapetest.h"
#include <cmath>

// optimisations
BG_OPTIMISATIONS()

namespace rage
{

static const char* s_GlassShardArchetypeFileName = "bgGlassShard";

atPool<phInst>* bgRagePhysics::sm_instPool = NULL;

bgRagePhysics::bgRagePhysics(const bgRagePhysics& that) 
#if !(!__SPU || defined(__SPURS_TASK__) || COMPRESSED_VERTEX_METHOD == 0)
	: 
	m_box(NULL)
#endif
{
	Assert(!__SPU);

	// OK, so this is where we have to move ourselves over to a new location in memory and keep on ticking

	// We don't want to call all the copy constructors because, well it would be slow and would be hard to get working anyway
	sysMemCpy((void*)this, (void*)&that, sizeof(bgRagePhysics));

	// So, we have to re-hook-up our pointers
	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 2);
	Assert(that.m_box.GetRefCount() == 2);
	Assert(that.m_archetype.GetRefCount() == 2);

	m_box.Release();

	Assert(m_box.GetRefCount() == 1);
	Assert(m_archetype.GetRefCount() == 2);
	Assert(that.m_box.GetRefCount() == 2);
	Assert(that.m_archetype.GetRefCount() == 2);

	that.m_box.AddRef();

	Assert(m_box.GetRefCount() == 1);
	Assert(m_archetype.GetRefCount() == 2);
	Assert(that.m_box.GetRefCount() == 3);
	Assert(that.m_archetype.GetRefCount() == 2);

	m_archetype.SetBound(&m_box);

	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 2);
	Assert(that.m_box.GetRefCount() == 2);
	Assert(that.m_archetype.GetRefCount() == 2);

	m_archetype.Release();

	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 1);
	Assert(that.m_box.GetRefCount() == 2);
	Assert(that.m_archetype.GetRefCount() == 2);

	m_inst->SetArchetype(&m_archetype, false);

	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 2);
	Assert(that.m_box.GetRefCount() == 2);
	Assert(that.m_archetype.GetRefCount() == 1);
}

bgRagePhysics::bgRagePhysics(Vector3 const &size) : 
#if !__SPU || defined(__SPURS_TASK__) || COMPRESSED_VERTEX_METHOD == 0
	m_box(size)
#else
	m_box(NULL, size)
#endif
{
	Assert(!__SPU);

	Assert(m_box.GetRefCount() == 1);
	Assert(m_archetype.GetRefCount() == 0);
	m_archetype.AddRef();
	m_archetype.SetBound(&m_box);
	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 1);
}

bgRagePhysics::~bgRagePhysics()
{
	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 1);

	m_archetype.SetBound(NULL);

	Assert(m_box.GetRefCount() == 1);
	Assert(m_archetype.GetRefCount() == 1);

	m_archetype.Release(false);
	m_box.Release(false);

	Assert(m_box.GetRefCount() == 0);
	Assert(m_archetype.GetRefCount() == 0);
}

void bgRagePhysics::Init(float mass,
						 Matrix34 const &transform,
						 Vector3 const &impulse,
						 Vector3 const &position)
{
	Assert(!sm_instPool->IsFull());

	//Get a new physics instance from the pool
	phInst* inst = sm_instPool->Construct();
	m_inst = inst;

	// Set it up
	m_archetype.SetMass(mass);

	m_archetype.SetFilename(s_GlassShardArchetypeFileName);

	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 1);

	inst->SetArchetype(&m_archetype, false);

	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 2);

	inst->SetMatrix(MATRIX34_TO_MAT34V(transform));
	bgGlassManager::CallInitShardInstForGameFunctor(*inst);

	// Add to the simulator
	int levelIndex = PHSIM->AddActiveObject(inst);
	
	// Send it flying
	if (levelIndex != phInst::INVALID_INDEX)
	{
		PHSIM->ApplyImpulse(levelIndex, impulse, position);
	}

	if (phCollider* collider = PHSIM->GetCollider(inst))
	{
		collider->DisablePushCollisions();
	}
}

void bgRagePhysics::Cleanup()
{
	phInst* inst = m_inst;
	if (inst->IsInLevel())
	{
		//Remove the instance is from the level
		PHSIM->DeleteObject(inst->GetLevelIndex());
	}
	
	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 2);

	//Shut it down
	inst->SetArchetype(NULL, false);

	Assert(m_box.GetRefCount() == 2);
	Assert(m_archetype.GetRefCount() == 1);

	// Return it from the pool
	sm_instPool->Destruct(inst);
}

Matrix34 bgRagePhysics::Update(float)
{
	return RCC_MATRIX34(m_inst->GetMatrix());
}

int bgRagePhysics::GetStorage() const
{
	return int(sizeof(*this));
}

float bgRagePhysics::GetMass() const
{
	return m_archetype.GetMass();
}

Vector3 bgRagePhysics::GetSize() const
{
	Vector3 boxSize = VEC3V_TO_VECTOR3(m_box.GetBoundingBoxSize());
	return boxSize;
}

Matrix34 bgRagePhysics::GetTransform() const
{
	return RCC_MATRIX34(m_inst->GetMatrix());
}

void bgRagePhysics::SetMass(float mass)
{
	m_archetype.SetMass(mass);
}

void bgRagePhysics::SetTransform(Matrix34 const &transform)
{
	Assert(transform.IsOrthonormal());
	m_inst->SetMatrix(RCC_MAT34V(transform));
}

bgPhysics* bgRagePhysics::Clone() const
{
	return rage_aligned_new(16) bgRagePhysics(*this);
}

void bgRagePhysics::InitInstPool(u16 numInsts)
{
#if COMMERCE_CONTAINER
	sm_instPool = rage_aligned_new(16) atPool<phInst>(numInsts, true);
#else
	sm_instPool = rage_aligned_new(16) atPool<phInst>(numInsts);
#endif
}

phCollider* bgRagePhysics::GetCollider()
{
	return PHSIM->GetCollider(m_inst);
}

bool bgRagePhysics::IsAboveGround(u32 uIncludeFlags, u32 uExcludeFlags)
{
	const Vector3& vStart = RCC_VECTOR3(m_inst->GetPosition());
	return bgPhysics::IsAboveGround(vStart, uIncludeFlags, uExcludeFlags);
}

//bgPhysics
static dev_float s_fZEnd = -1000.0f;
static dev_float s_fDepth = 0.125f;//bgBreakable::sm_maxDepth

bool bgPhysics::IsAboveGround(Vector3 const &vPosition, u32 uIncludeFlags, u32 uExcludeFlags)
{
	//Setup the probe data from current position Z + depth to -1000.0f Z
	const Vector3 vStart(vPosition.x, vPosition.y, vPosition.z + s_fDepth);
	const Vector3 vEnd(vPosition.x, vPosition.y, s_fZEnd);

	phSegment segment;
	segment.Set(vStart, vEnd);

	phShapeTest<phShapeProbe> probeTester;

	const int nNumIntersections = 1;
	phIntersection intersection;
	probeTester.InitProbe(segment, &intersection, nNumIntersections);

	float fGroundPosZ = -FLT_MAX;
	u32 uTypeFlags = BIT(22);//GTA_GLASS_TYPE

	bool bHitGround = probeTester.TestInLevel(NULL,uIncludeFlags,uTypeFlags,phLevelBase::STATE_FLAGS_ALL,uExcludeFlags,probeTester.GetLevel()) > 0;
	if(bHitGround)
	{
		fGroundPosZ = intersection.GetPosition().GetZf();
	}

	return bHitGround && vStart.z >= fGroundPosZ;
}

//bgSimplePhysics

bgSimplePhysics::bgSimplePhysics(Vector3 const &size)
	: m_size(size)
{
	//Components of size must not be zero to prevent division by zero
	Assert(size.x > 0.f && size.y > 0.f && size.z > 0.f);
	m_linear.impulse.Zero();
	m_linear.velocity.Zero();
	m_linear.position.Zero();
	m_angular.impulse.Zero();
	m_angular.velocity.Zero();
	m_angular.position.Zero();
}

void bgSimplePhysics::Init(float mass,
						   Matrix34 const &transform,
						   Vector3 const &impulse,
						   Vector3 const &position)
{
	SetMass(mass);
	Assert(transform.IsOrthonormal());
	SetTransform(transform);
	ApplyImpulse(impulse, position);
}

void bgSimplePhysics::Cleanup()
{
}

bgPhysics* bgSimplePhysics::Clone() const
{
	return rage_aligned_new(16) bgSimplePhysics(*this);
}

Matrix34 bgSimplePhysics::Update(float timeStep)
{
	//Apply gravity
	ApplyImpulse(m_linearMass * PHSIM->GetGravity() * sm_gravityScale / (sm_linearImpulseScale ? sm_linearImpulseScale : 1.f) * timeStep, m_linear.position);

	//Update linear position
	m_linear.position += m_linear.velocity * timeStep;

	//Update angular position
	m_angular.position += m_angular.velocity * timeStep;

	//Create transform matrix
	Matrix34 transform(GetTransform());

	//Update linear velocity
	m_linear.velocity += m_linear.impulse / m_linearMass;
	m_linear.impulse.Zero();
	float linearSpeed2(m_linear.velocity.Mag2());
	if (linearSpeed2 > DEFAULT_MAX_SPEED * DEFAULT_MAX_SPEED)
		m_linear.velocity *= DEFAULT_MAX_SPEED / sqrt(linearSpeed2);

	//Update angular velocity
	Vector3 angularMass(m_angularMass);
	transform.Transform3x3(angularMass);
	m_angular.velocity += m_angular.impulse / angularMass;
	m_angular.impulse.Zero();
	float angularSpeed2(m_angular.velocity.Mag2());
	if (angularSpeed2 > DEFAULT_MAX_ANG_SPEED * DEFAULT_MAX_ANG_SPEED)
		m_angular.velocity *= DEFAULT_MAX_ANG_SPEED / sqrt(angularSpeed2);

	return transform;
}

int bgSimplePhysics::GetStorage() const
{
	return int(sizeof(*this));
}

float bgSimplePhysics::GetMass() const
{
	return m_linearMass;
}

Vector3 bgSimplePhysics::GetSize() const
{
	return m_size;
}

Matrix34 bgSimplePhysics::GetTransform() const
{
	Matrix34 transform;
	transform.FromEulersXYZ(m_angular.position);
	transform.d = m_linear.position;
	return transform;
}

void bgSimplePhysics::ApplyImpulse(Vector3 const &impulse, Vector3 const &position)
{
	//Accumulate linear impulse
	m_linear.impulse += impulse * sm_linearImpulseScale;

	//Calculate angular impulse
	Vector3 rxI(position - m_linear.position);
	rxI.Cross(impulse);
	m_angular.impulse += rxI * sm_angularImpulseScale;
}

void bgSimplePhysics::SetMass(float mass)
{
	//Mass must not be zero to prevent division by zero
	Assert(mass > 0.f);
	m_linearMass = mass;
	phMathInertia::FindBoxAngInertia(mass, m_size.x, m_size.y, m_size.z, &m_angularMass);
}

void bgSimplePhysics::SetTransform(Matrix34 const &transform)
{
	Assert(transform.IsOrthonormal());
	transform.ToEulersXYZ(m_angular.position);
	m_linear.position = transform.d;
}

bool bgSimplePhysics::IsAboveGround(u32 uIncludeFlags, u32 uExcludeFlags)
{
	const Vector3& vStart = m_linear.position;
	return bgPhysics::IsAboveGround(vStart, uIncludeFlags, uExcludeFlags);
}

#if __BANK
//------------------------------------------------------------------------------
// Add debugging widgets
//------------------------------------------------------------------------------
void bgSimplePhysics::AddWidgets(bkBank& bank)
{
	bank.AddSlider("Gravity scale", &sm_gravityScale, 0.f, 2.f, 0.001f);
	bank.AddSlider("Linear impulse scale", &sm_linearImpulseScale, 0.f, 2.f, 0.001f);
	bank.AddSlider("Angular impulse scale", &sm_angularImpulseScale, 0.f, 2.f, 0.001f);
}
#endif

float bgSimplePhysics::sm_gravityScale = 1.f;
float bgSimplePhysics::sm_linearImpulseScale = 1.f;
float bgSimplePhysics::sm_angularImpulseScale = 1.f;
}
