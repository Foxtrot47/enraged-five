//
// breakableglass/glassmanager.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "glassmanager.h"

#include "bgchannel.h"
#include "bgdebug.h"
#include "bgdrawable.h"
#include "breakable.h"
#include "crackstemplate.h"
#include "memutils.h"
#include "optimisations.h"

#include "grcore/viewport.h"
#include "grmodel/shader.h"
#include "phbound/boundbvh.h" // only needed so that the compiler knows a phBoundBVH is a phBound.  sigh.
#include "phglass/glass.h"
#include "phglass/glassinstance.h"
#include "phsolver/contactmgr.h"
#include "physics/simulator.h"
#include "spatialdata/aabb.h"
#include "system/debugmemoryfill.h"
#include "system/memmanager.h"
#include "system/memops.h"
#include "grprofile/pix.h"
#include "system/simpleallocator.h"
#include "system/timemgr.h"
#include "grcore/debugdraw.h"

// optimisations
BG_OPTIMISATIONS()

namespace rage {

static const char* s_GlassPaneArchetypeFileName = "bgGlassPane";

// this really belongs in memutils.cpp, except memutils.cpp doesn't exist and it doesn't seem worth it to make it for this alone:
ASSERT_ONLY(__THREAD sysMemAllocator* bgAutoStackHeap::sm_currentStackHeap;)

GLASSMANAGER_DEBUGGING_ONLY(static __THREAD bool gMainThread=false;)

#if GLASSMANAGER_DEBUGGING
#	define GLASSMANAGER_INVARIANT_CHECK(EXP)                                   \
	do{                                                                        \
		if(Unlikely(!(EXP)))                                                   \
		{                                                                      \
			/*Printf("Invariant failure: "#EXP"\n");*/                         \
			/*__debugbreak();*/                                                \
			return false;                                                      \
		}                                                                      \
	}while(0)

static unsigned gMaxBreakableGlass;

#endif // GLASSMANAGER_DEBUGGING

#ifdef GTA_REPLAY_RAGE
glassRecorderInterface* glassRecordingRage::sm_GlassRecorder = NULL;
#endif

//
// Copy all effect values from the source instance that have a matching
// effect (same name and type) in the destination
//
void CopyEffectInstanceData(grcInstanceData* in_pDest, const grcInstanceData* in_pSrc)
{
	GLASSMANAGER_ASSERT(gMainThread);

	// copy effect vars from the base fragment
	grcEffect &destEffect = in_pDest->GetBasis();
	int numDestVars = in_pDest->GetCount();
	for(int i = 0; i < numDestVars; ++i)
	{
		u32 destNameHash = 0;
		grcEffect::VarType destType;
		grcEffectVar destVar = destEffect.GetVarByIndex(i);
		destEffect.GetVarDesc(destVar, destNameHash, destType);

		grcEffect &srcEffect = in_pSrc->GetBasis();
		grcEffectVar srcVar = srcEffect.LookupVarByHash(destNameHash);
		if (srcVar != grcevNONE)
		{
			u32 srcNameHash;
			grcEffect::VarType srcType;
			srcEffect.GetVarDesc(srcVar, srcNameHash, srcType);
			if (srcType == destType)
			{
				// copy values
				switch (srcType)
				{
				case grcEffect::VT_FLOAT:
					{
						float val;
						srcEffect.GetVar(*in_pSrc, srcVar, val);
						destEffect.SetVar(*in_pDest, destVar, val);
					}
					break;
				case grcEffect::VT_VECTOR2:
					{
						Vector2 val;
						srcEffect.GetVar(*in_pSrc, srcVar, val);
						destEffect.SetVar(*in_pDest, destVar, val);
					}
					break;
				case grcEffect::VT_VECTOR3:
					{
						Vector3 val;
						srcEffect.GetVar(*in_pSrc, srcVar, val);
						destEffect.SetVar(*in_pDest, destVar, val);
					}
					break;
				case grcEffect::VT_VECTOR4:
					{
						Vector4 val;
						srcEffect.GetVar(*in_pSrc, srcVar, val);
						destEffect.SetVar(*in_pDest, destVar, val);
					}
					break;
				case grcEffect::VT_TEXTURE:
					{
						grcTexture *tex = NULL;
						srcEffect.GetVar(*in_pSrc, srcVar, tex);
						destEffect.SetVar(*in_pDest, destVar, tex);
					}
					break;
				default:
					break;
				}
			}
		}
	}
}


namespace bgGlassManager
{
	class GlassInfo;
	template<class T, unsigned FRAME_DELAY, class INDEX_TYPE=u16> class DeferredDeletionPool;

	// When deferred deleting a pool entry, it is added to queue m_FrameIdx.
	// m_FrameIdx is incremented during the render thread safe zone, and then
	// entries in queue m_FrameIdx are deleted.
	//
	// m_FrameIdx
	//    n     main thread builds draw list referencing grcInstanceData, then defers deletion
	//   n+1    counter is incremented in render thread safe zone, and deferred deletion run
	//   n+1    render thread executes draw list
	//   n+2    counter is incremented in render thread safe zone, and deferred deletion run
	//
	// So a frame delay here of 2 protects the bgDrawables from being deleted
	// while still in use by the render thread.
	//
	typedef DeferredDeletionPool<bgDrawable, 2> bgDrawablePool;

	// The glass info pool only needs to delay deletion until the next render
	// thread safe zone, so frame delay of 1 is sufficient.
	atArray<GlassInfo> sm_GlassInfo;
	typedef DeferredDeletionPool<phGlassInst, 1> phGlassInstPool;

	phGlassInstPool* sm_pGlassInstPool;
	bgDrawablePool* sm_pDrawablePool;
	grmShader* sm_pGlassShader;
	const grcViewport* sm_pViewport;
	bgInitGlassBoundForGameFunctor sm_InitGlassBoundForGameFunctor;
	bgInitGlassInstForGameFunctor sm_InitGlassInstForGameFunctor;
	bgInitShardInstForGameFunctor sm_InitShardInstForGameFunctor;
	static const int sm_iMaxContactFunctors = 2;
	int sm_iNumContactFunctors = 0;
	bgContactFunctor sm_ContactFunctor[sm_iMaxContactFunctors];
	bgGlassImpactFunc sm_CleanUpGlassAudioFunctor;

	int sm_CreateCount;							// # panes created on the current frame; limit to kMaxCreateCount for performance
	static const int kMaxCreateCount = 4;		// max number of panes to create in a frame
	static const int kMaxFrameImpactCount = 12; // maximum number of frame impacts to process in a frame
												// subsequent impacts cause impacted pane to disappear
	static const int kMaxPaneImpactCount = 12;  // maximum number of 'regular' impacts to process in a frame
												// subsequent impacts are ignored

	bool sm_bSilentHit = false;

#if __BANK
	static bool ms_bShowVisBGEntities = false;	// Do we want to highlight the visible glass entitys
#endif // __BANK

	// A mutex to avoid crashing when profile draw is on and glass data changes
#	define GLASSMANAGER_MUTEX  (__PFDRAW)
#	if GLASSMANAGER_MUTEX
		sysIpcMutex             sm_Mutex;
		sysIpcCurrentThreadId   sm_MutexLockOwner;

#		define GLASSMANAGER_MUTEX_ONLY(...)     __VA_ARGS__

#		define GLASSMANAGER_MUTEX_LOCK(FUNC)                                   \
		do                                                                     \
		{                                                                      \
			Verifyf(sysIpcLockMutex(bgGlassManager::sm_Mutex),                 \
				"%s - Failed to acquire mutex", FUNC);                         \
			bgGlassManager::sm_MutexLockOwner = sysIpcGetCurrentThreadId();    \
		} while (0)

#		define GLASSMANAGER_MUTEX_UNLOCK()                                     \
		do                                                                     \
		{                                                                      \
			bgGlassManager::sm_MutexLockOwner = sysIpcCurrentThreadIdInvalid;  \
			sysIpcUnlockMutex(bgGlassManager::sm_Mutex);                       \
		} while (0)

		struct GlassManagerMutexAutoLock
		{
			GlassManagerMutexAutoLock(const char *func)     {GLASSMANAGER_MUTEX_LOCK(func); (void)func;}
			~GlassManagerMutexAutoLock()                    {GLASSMANAGER_MUTEX_UNLOCK();}
		};

#		ifdef _MSC_VER // Microsoft compilers don't support C99 __func__, but do have a __FUNCTION__ value available
#			define GLASSMANAGER_MUTEX_AUTO_LOCK()   GlassManagerMutexAutoLock glassManagerMutexAutoLock(__FUNCTION__)
#		else
#			define GLASSMANAGER_MUTEX_AUTO_LOCK()   GlassManagerMutexAutoLock glassManagerMutexAutoLock(__func__)
#		endif

#		define GLASSMANAGER_MUTEX_IS_LOCKED()   (bgGlassManager::sm_MutexLockOwner == sysIpcGetCurrentThreadId())

#	else // !GLASSMANAGER_MUTEX
#		define GLASSMANAGER_MUTEX_ONLY(...)
#		define GLASSMANAGER_MUTEX_LOCK(FUNC)    (void)0
#		define GLASSMANAGER_MUTEX_UNLOCK()      (void)0
#		define GLASSMANAGER_MUTEX_AUTO_LOCK()   (void)0
#		define GLASSMANAGER_MUTEX_IS_LOCKED()   false
#	endif


	class ImpactInfo;

	class ImpactHandle
	{
	public:
		ImpactHandle() : m_handle(kNullHandle)
		{
		}

		static const u32 kNullHandle = (u32)-1;
		static const u32 kFrameFlag = 0x80000000;

		void SetFrameImpactIndex(u32 index)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			m_handle = index | kFrameFlag;
		}

		void SetPaneImpactIndex(u32 index)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			m_handle = index;
		}

		bool IsNull() const
		{
			GLASSMANAGER_ASSERT(gMainThread);
			return kNullHandle == m_handle;
		}

		bool IsFrameImpact() const
		{
			GLASSMANAGER_ASSERT(gMainThread);
			return (m_handle & kFrameFlag) ? true : false;
		}

		ImpactInfo* GetImpact() const;

		void Reset()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			m_handle = kNullHandle;
		}

		u32 m_handle;
	};

	class ImpactInfo
	{
	public:
		Vec3V m_impactImpulse;
		ImpactHandle m_nextImpact;
		u32 m_crackType;
		bool bSilentHit;
	};

	class PaneImpactInfo : public ImpactInfo
	{
	public:
		Vec3V m_impactLoc;		
	};

	class FrameImpactInfo : public ImpactInfo
	{
	public:
		u32 m_frameFlags;
	};

	atFixedArray<PaneImpactInfo, kMaxPaneImpactCount> sm_PaneImpacts;
	atFixedArray<FrameImpactInfo, kMaxFrameImpactCount> sm_FrameImpacts;

	ImpactInfo* ImpactHandle::GetImpact() const
	{
		GLASSMANAGER_ASSERT(gMainThread);
		GLASSMANAGER_ASSERTF(!IsNull(), "ImpactHandle::GetImpact() Null Handle!");
		u32 index = m_handle & ~kFrameFlag;
		if (IsFrameImpact())
		{
			Assert(index < (u32)sm_FrameImpacts.GetCount());
			return &sm_FrameImpacts[index];
		}
		else
		{
			Assert(index < (u32)sm_PaneImpacts.GetCount());
			return &sm_PaneImpacts[index];
		}
	}

	class GlassInfoList
	{
	public:
		GlassInfoList() : m_head(bgGlassHandle_Invalid), m_count(0)
		{
		}

#if GLASSMANAGER_DEBUGGING
		bool Invariant() const;
#endif

		int m_count;
		bgGlassHandle m_head;
	};

	GlassInfoList sm_FreeList;
	GlassInfoList sm_ActiveList;


	bgGlassHandle GetHeadFreeList()
	{
		return sm_FreeList.m_head;
	}

	template <class T, unsigned FRAME_DELAY, class INDEX_TYPE>
	class DeferredDeletionPool
	{
	public:
		typedef atDelegate<void (T&)> ShutdownFunc;

#if GLASSMANAGER_DEBUGGING
		bool Invariant() const
		{
			GLASSMANAGER_ASSERT(gMainThread);

			// Count free
			unsigned numFree = 0;
			for (INDEX_TYPE i=m_FreeIndices; i!=NULL_IDX; i=m_Next[i])
			{
				++numFree;
			}

			// Check allocated list sentinal
			GLASSMANAGER_INVARIANT_CHECK(m_AllocedIndices == m_PoolSize);

			// Forward count allocated, and verify that prev links match up
			unsigned numAllocated = 0;
			GLASSMANAGER_INVARIANT_CHECK(m_Next[m_Prev[m_AllocedIndices]] == m_AllocedIndices);
			for (INDEX_TYPE i=m_Next[m_AllocedIndices]; i!=m_AllocedIndices; i=m_Next[i])
			{
				++numAllocated;
				GLASSMANAGER_INVARIANT_CHECK(m_Next[m_Prev[i]] == i);
			}

			// Verify reverse count allocated matches forward count
			unsigned numAllocatedReversed = 0;
			for (INDEX_TYPE i=m_Prev[m_AllocedIndices]; i!=m_AllocedIndices; i=m_Prev[i])
			{
				++numAllocatedReversed;
			}
			GLASSMANAGER_INVARIANT_CHECK(numAllocated == numAllocatedReversed);

			// Count deferred deletions
			unsigned numDeferredDeletions = 0;
			for (unsigned f=0; f<FRAME_DELAY; ++f)
			{
				for (INDEX_TYPE i=m_DeferredDeletionQueue[f]; i!=NULL_IDX; i=m_Next[i])
				{
					++numDeferredDeletions;
				}
			}

			// Check we haven't leaked anything
			GLASSMANAGER_INVARIANT_CHECK(numFree + numAllocated + numDeferredDeletions == m_PoolSize);

			return true;

		}
#endif // GLASSMANAGER_DEBUGGING

		explicit DeferredDeletionPool(unsigned poolSize)
			: m_FrameIdx(0)
			, m_AllObjs(NULL)
			, m_Next(rage_new INDEX_TYPE[(poolSize+1)*2]) // +1 for allocated list sentinal, *2 for m_Prev
			, m_Prev(m_Next+poolSize+1)
			, m_FreeIndices(0)
			, m_AllocedIndices((INDEX_TYPE)poolSize)
#if GLASSMANAGER_DEBUGGING
			, m_PoolSize(poolSize)
#endif
		{
			GLASSMANAGER_ASSERT(gMainThread);

			const size_t poolSizeBytes = poolSize*sizeof(T);
#			if __PPU
				if (!g_bDontUseResidualAllocator && g_pResidualAllocator)
				{
					m_AllObjs = (T*)(g_pResidualAllocator->RAGE_LOG_ALLOCATE(poolSizeBytes, __alignof(T)));
				}
#				if COMMERCE_CONTAINER
					if (!m_AllObjs && sysMemManager::GetInstance().IsFlexHeapEnabled())
					{
						m_AllObjs = (T*)(sysMemManager::GetInstance().GetFlexAllocator()->RAGE_LOG_ALLOCATE(poolSizeBytes, __alignof(T)));
					}
#				endif
#			endif
			if (!m_AllObjs)
			{
				m_AllObjs = (T*)(sysMemAllocator::GetCurrent().RAGE_LOG_ALLOCATE(poolSizeBytes, __alignof(T)));
			}

			// Initialize the main entries of m_Next
			GLASSMANAGER_ASSERT(poolSize < (1<<(sizeof(INDEX_TYPE)*8))); // < not <= because of NULL_IDX
			for (INDEX_TYPE i=0; i<poolSize-1; ++i)
			{
				m_Next[i] = i+1;
			}
			m_Next[poolSize-1] = NULL_IDX;

			// Initialize the next and prev sentinals used by the allocated list
			m_Next[poolSize] = (INDEX_TYPE)poolSize;
			m_Prev[poolSize] = (INDEX_TYPE)poolSize;

			// Don't need to initialize the rest of m_Prev, since it is only valid once an object is allocated

			// Initialize the deletion queues to empty
			for (INDEX_TYPE i=0; i<FRAME_DELAY; ++i)
			{
				m_DeferredDeletionQueue[i] = NULL_IDX;
			}

			IF_DEBUG_MEMORY_FILL_N(sysMemSet((char*)m_AllObjs,0xCD,poolSize*sizeof(T)),DMF_GAMEPOOL);

			GLASSMANAGER_ASSERT(Invariant());
		}

		~DeferredDeletionPool()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERT(Invariant());

			ListCallDtor(m_Next[m_AllocedIndices], m_AllocedIndices);
			for (unsigned i=0; i<FRAME_DELAY; ++i)
			{
				ListCallDtor(m_DeferredDeletionQueue[i], NULL_IDX);
			}

			delete[] m_Next;

			RAGE_LOG_DELETE(m_AllObjs);
			sysMemAllocator *allocator = &sysMemAllocator::GetCurrent();
#			if __PPU
				if (g_pResidualAllocator && g_pResidualAllocator->IsValidPointer(m_AllObjs))
				{
					allocator = g_pResidualAllocator;
				}
				else if (sysMemManager::GetInstance().GetFlexAllocator()->IsValidPointer(m_AllObjs))
				{
					allocator = sysMemManager::GetInstance().GetFlexAllocator();
				}
#			endif
			allocator->Free(m_AllObjs);
		}

		T* New()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERT(Invariant());

			// Remove from free list
			const INDEX_TYPE idx = m_FreeIndices;
			if (Unlikely(idx == NULL_IDX))
			{
				return NULL;
			}
			m_FreeIndices = m_Next[idx];

			// Add to allocated list
			const INDEX_TYPE aprev = m_AllocedIndices;
			const INDEX_TYPE anext = m_Next[aprev];
			m_Next[idx] = anext;
			m_Prev[idx] = aprev;
			m_Next[aprev] = idx;
			m_Prev[anext] = idx;

			T *const ptr = m_AllObjs + idx;
			IF_DEBUG_MEMORY_FILL_N(sysMemSet((char*)ptr,0xAA,sizeof(*ptr)),DMF_GAMEPOOL);
			rage_placement_new(ptr) T();
			GLASSMANAGER_ASSERT(Invariant());
			return ptr;
		}

		void Delete(T*& ptr)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERT(Invariant());
			if (!ptr)
			{
				return;
			}

			if (m_ShutdownFunc.IsBound())
				m_ShutdownFunc(*ptr);
			ptr->~T();
			IF_DEBUG_MEMORY_FILL_N(sysMemSet((char*)ptr,0xFE,sizeof(*ptr)),DMF_GAMEPOOL);

			// Remove from allocated list
			const INDEX_TYPE idx = (INDEX_TYPE)(ptr - m_AllObjs);
			const INDEX_TYPE aprev = m_Prev[idx];
			const INDEX_TYPE anext = m_Next[idx];
			m_Next[aprev] = anext;
			m_Prev[anext] = aprev;

			// Add to free list
			m_Next[idx] = m_FreeIndices;
			m_FreeIndices = idx;

			ptr = NULL;
			GLASSMANAGER_ASSERT(Invariant());
		}

		void DeferredDelete(T*& ptr)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERT(Invariant());
			if (NULL == ptr)
			{
				return; // No need to delete
			}

			// Remove from allocated list
			const INDEX_TYPE idx = (INDEX_TYPE)(ptr - m_AllObjs);
			const INDEX_TYPE aprev = m_Prev[idx];
			const INDEX_TYPE anext = m_Next[idx];
			m_Next[aprev] = anext;
			m_Prev[anext] = aprev;

			// Add to deferred deletion queue
			TrapGE(m_FrameIdx, FRAME_DELAY);
			INDEX_TYPE *const currentQueue = m_DeferredDeletionQueue + m_FrameIdx;
			m_Next[idx] = *currentQueue;
			*currentQueue = idx;
			GLASSMANAGER_ASSERT(Invariant());
		}

		void ProcessDeferredDeletes()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERT(Invariant());

			// advance to the 'next' frame, which is also the oldest frame and hence the one we need to clean out
			if (++m_FrameIdx >= FRAME_DELAY)
			{
				m_FrameIdx = 0;
			}

			INDEX_TYPE *const queueToProcess = m_DeferredDeletionQueue + m_FrameIdx;
			const INDEX_TYPE first = *queueToProcess;
			if (first != NULL_IDX)
			{
				INDEX_TYPE next = first;
				INDEX_TYPE idx;
				do
				{
					idx = next;
					T *const ptr = m_AllObjs + idx;
					if (m_ShutdownFunc.IsBound())
						m_ShutdownFunc(*ptr);
					ptr->~T();
					IF_DEBUG_MEMORY_FILL_N(sysMemSet((char*)ptr,0xFE,sizeof(*ptr)),DMF_GAMEPOOL);
				}
				while ((next = m_Next[idx]) != NULL_IDX);

				m_Next[idx] = m_FreeIndices;
				m_FreeIndices = first;
				*queueToProcess = NULL_IDX;
			}

			GLASSMANAGER_ASSERT(Invariant());
		}

		void SetShutdownFunc(ShutdownFunc func)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			m_ShutdownFunc = func;
		}

	private:
		unsigned m_FrameIdx;

		// Array of all allocated pool objects
		T *m_AllObjs;

		enum { NULL_IDX = (INDEX_TYPE)~0 };

		// Parallel array of next indices.  Indexed same as m_AllObjs.  Value
		// stored in array entry is the next index in which ever list the object
		// belongs to.
		INDEX_TYPE *m_Next;

		// Just like m_Next, but only valid when the object is in the m_AllocedIndices list
		INDEX_TYPE *m_Prev;

		// Pseudo singly linked list of free objects
		INDEX_TYPE m_FreeIndices;

		// Array of pseudo singly linked lists of objects waiting to be deleted
		INDEX_TYPE m_DeferredDeletionQueue[FRAME_DELAY];

		// Pseudo doubly linked list of allocated objects not in m_DeferredDeletionQueue
		// This is required only for calling ~T() on pool destruction
		INDEX_TYPE m_AllocedIndices;

		atDelegate<void (T&)> m_ShutdownFunc;

#if GLASSMANAGER_DEBUGGING
		unsigned m_PoolSize;
#endif

		void ListCallDtor(INDEX_TYPE idx, INDEX_TYPE end)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			while (idx != end)
			{
				T *const ptr = m_AllObjs + idx;
				if (m_ShutdownFunc.IsBound())
					m_ShutdownFunc(*ptr);
				ptr->~T();
				idx = m_Next[idx];
			}
		}
	};

	class GlassInfo
	{
	public:

		GlassInfo() : m_pHandle(NULL)
			        , m_pDrawable(NULL)
					, m_pGlassInst(NULL)
					, m_Prev(bgGlassHandle_Invalid)
					, m_Next(bgGlassHandle_Invalid)
					, m_bShouldShutdown(false)
					, m_bCanRecycle(true)
		{
			GLASSMANAGER_ASSERT(gMainThread);
		}

		void RemoveFromList(GlassInfoList* in_pList)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERT(in_pList->Invariant());
			TrapEQ(in_pList->m_head, bgGlassHandle_Invalid); // list to remove from can't be empty...
			if (m_Prev != bgGlassHandle_Invalid)
			{
				sm_GlassInfo[m_Prev].m_Next = m_Next;
			}
			else
			{
				GLASSMANAGER_ASSERTF(&sm_GlassInfo[in_pList->m_head] == this, "GlassInfo::RemoveFromList(): this is not in the specified list!");
				in_pList->m_head = m_Next;
			}
			TrapZ(in_pList->m_count);
			in_pList->m_count--;
			if (m_Next != bgGlassHandle_Invalid)
			{
				Assert(m_Next < sm_GlassInfo.GetCapacity());
				sm_GlassInfo[m_Next ].m_Prev = m_Prev;
			}
			m_Prev = m_Next = bgGlassHandle_Invalid;
			GLASSMANAGER_ASSERT(in_pList->Invariant());
		}

		void AddToList(GlassInfoList* in_pList, bgGlassHandle handle)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERT(in_pList->Invariant());
			GLASSMANAGER_ASSERT(this-&sm_GlassInfo[0] == handle);
			m_Prev = bgGlassHandle_Invalid;
			m_Next = in_pList->m_head;
			if (in_pList->m_head != bgGlassHandle_Invalid)
			{
				Assert(in_pList->m_head < sm_GlassInfo.GetCapacity());
				sm_GlassInfo[in_pList->m_head].m_Prev = handle;
			}
			in_pList->m_head = handle;
			in_pList->m_count++;
			GLASSMANAGER_ASSERT(in_pList->Invariant());
		}

		void ShutdownPhysics()
		{
			GLASSMANAGER_ASSERT(gMainThread);

			phBound* bound = m_phArchetype.GetBound();
			if (bound && phConfig::IsRefCountingEnabled())
				bound->AddRef();

			if (m_pGlassInst)
			{
				u16 levelIndex = m_pGlassInst->GetLevelIndex();
				if (phInst::INVALID_INDEX != levelIndex)
				{
					PHSIM->DeleteObject(levelIndex, true);
				}
				m_pGlassInst->SetArchetype(NULL, false);
				m_pGlassInst->SetGlass(NULL);
			}

			m_phArchetype.SetBound(NULL);
			if (bound && phConfig::IsRefCountingEnabled())
				(void)Verifyf(bound->Release(false) == 0, "bound has outstanding references!");

			m_phGlass.Shutdown(bgBreakable::GetPrivateHeap());
		}

		// Called from simulation thread, potentially during physics update:
		// phInsts and bgDrawables may not be deleted or recycled here; instead their
		// deletion is deferred to the DMZ.
		void Shutdown()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERTF(m_pHandle, "GlassInfo::Shutdown(): glassinfo was already shut down!");

			bgGlassHandle handle = *m_pHandle;
			*m_pHandle = bgGlassHandle_Invalid;
			m_pHandle = NULL;

			m_Breakable.Shutdown();

			ShutdownPhysics();

			m_ImpactQueue.Reset();
			m_bShouldShutdown = false;
			m_bCanRecycle = true;

			sm_pGlassInstPool->DeferredDelete(m_pGlassInst);
			sm_pDrawablePool->DeferredDelete(m_pDrawable);

			RemoveFromList(&sm_ActiveList);
			AddToList(&sm_FreeList, handle);

			bgDebugf1("Shutdown handle:%i frame:%d", handle, TIME.GetFrameCount());
		}

		// returns true if Shutdown() has been called, whether or not it was complete.
		bool IsShutdown() const
		{
			GLASSMANAGER_ASSERT(gMainThread);
			return NULL == m_pHandle;
		}

		bool IsShutdownQueued() const
		{
			GLASSMANAGER_ASSERT(gMainThread);
			return m_bShouldShutdown;
		}

		bool CanRecycle() const
		{
			GLASSMANAGER_ASSERT(gMainThread);
			return m_bCanRecycle;
		}

		void SetupPhysics(STACK_HEAP_PARAMS(ApplyImpacts))
		{
			PIXBegin(0, "GlassInfo::SetupPhysics");

			// Don't hurt me...
			SetCanRecycle(false);
			{
				USE_STACK_HEAP(ApplyImpacts, false);
				phGlass* pTempGlass = rage_new phGlass;
				GLASSMANAGER_ASSERT(m_pHandle != NULL);
				pTempGlass->Init(*m_pHandle, m_materialId);
				m_phGlass.InitUnpacked(pTempGlass);
				m_phGlass.Pack(bgBreakable::GetPrivateHeap(), &RecycleGlass);
				SetCanRecycle(true);
				PHSIM->GetContactMgr()->MarkScratchpadUsed((int)__heap.GetHeapSize() - (int)__heap.GetLowWaterMark(false));
			}
			PIXEnd();

			if (!m_phGlass.IsValid())
			{
				bgDebugf1("SetupPhysics() failed; shutting down");
				Shutdown(); // @GAME TEST!
				return;
			}
			PIXBegin(0, "GlassInfo::FinishInit");
			phBound* bound = m_phGlass.GetData().GetBound();
			m_phArchetype.SetBound(bound);
			m_phArchetype.SetFilename(s_GlassPaneArchetypeFileName);
			bound->Release();

			m_pGlassInst->SetGlass(&m_phGlass.GetData());
			m_pGlassInst->SetArchetype(&m_phArchetype);

			if (sm_InitGlassInstForGameFunctor.IsBound())
				sm_InitGlassInstForGameFunctor(*m_pGlassInst);

			if (sm_InitGlassBoundForGameFunctor.IsBound())
				sm_InitGlassBoundForGameFunctor(*bound);

			m_pGlassInst->SetInstFlag(phInst::FLAG_NEVER_ACTIVATE, true);
			PHSIM->AddInactiveObject(m_pGlassInst);
			PIXEnd(); //PIXBegin("GlassInfo::FinishInit");
		}

#if BREAKABLE_GLASS_USE_BVH
		// Basically this does the same thing as ShutdownPhysics(); SetupPhysics(); except that it avoids removing
		// and reinserting the object in the physics level (at least)
		void RebuildPhysics(STACK_HEAP_PARAMS(ApplyImpacts))
		{
			GLASSMANAGER_ASSERT(gMainThread);
			PIXBegin(0, "GlassInfo::RebuildPhysics");

#if ENABLE_PHYSICS_LOCK
			// Make sure all shape tests stop before swap out the bound
			PHLOCK_SCOPEDWRITELOCK;
#endif	// ENABLE_PHYSICS_LOCK

			phBound* oldBound = m_phArchetype.GetBound();
			if (oldBound && phConfig::IsRefCountingEnabled())
				oldBound->AddRef();

			if (m_pGlassInst)
			{
				m_pGlassInst->SetArchetype(NULL, false);
				m_pGlassInst->SetGlass(NULL);
			}

			m_phArchetype.SetBound(NULL);
			if (oldBound && phConfig::IsRefCountingEnabled())
				(void)Verifyf(oldBound->Release(false) == 0, "bound has outstanding references!");

			m_phGlass.Shutdown(bgBreakable::GetPrivateHeap());

			// Don't hurt me...
			SetCanRecycle(false);
			{
				USE_STACK_HEAP(ApplyImpacts, false);
				phGlass* pTempGlass = rage_new phGlass;
				pTempGlass->Init(*m_pHandle, m_materialId);
				m_phGlass.InitUnpacked(pTempGlass);
				m_phGlass.Pack(bgBreakable::GetPrivateHeap(), &RecycleGlass);
				SetCanRecycle(true);
				PHSIM->GetContactMgr()->MarkScratchpadUsed((int)__heap.GetHeapSize() - (int)__heap.GetLowWaterMark(false));
			}

			if (!m_phGlass.IsValid())
			{
				bgDebugf1("RebuildPhysics() failed; shutting down");
				Shutdown(); // @GAME TEST!
				return;
			}
			phBound* newBound = m_phGlass.GetData().GetBound();
			m_phArchetype.SetBound(newBound);
			newBound->Release();

			m_pGlassInst->SetGlass(&m_phGlass.GetData());
			m_pGlassInst->SetArchetype(&m_phArchetype);

			if (sm_InitGlassBoundForGameFunctor.IsBound())
				sm_InitGlassBoundForGameFunctor(*newBound);

			Assert(m_pGlassInst->IsInLevel());
			PIXEnd(); //PIXBegin("GlassInfo::FinishInit");
		}
#endif // BREAKABLE_GLASS_USE_BVH

		bool Init(
			bgGlassHandle* in_pHandle,
			Mat34V_In in_glassTransform,
			const bgPaneModelInfoBase& in_modelInfo,
			bgGetInstanceDataFunc in_getInstanceDataFunc,
			phInst* in_ignoreInst,
			phMaterialMgr::Id in_materialId,
			float in_mass,
			int childIdx)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			m_bShouldShutdown = false;
			m_bCanRecycle = true;
			if (!Verifyf(m_pHandle == NULL, "GlassInfo::Init(): already initialized!\n"))
			{
				Shutdown();
			}

			Assertf(m_ImpactQueue.IsNull(),"GlassInfo::Init impact new is not clean.");
			m_shaderIndex = in_modelInfo.m_shaderIndex;
			m_sourceChildIdx  = (u8)childIdx;

			// Initialize breakable
			m_Breakable.InitFromModelInfo(in_modelInfo, in_glassTransform, in_mass);
			if (!m_Breakable.IsValid())
			{
				m_Breakable.Shutdown();
				*in_pHandle = bgGlassHandle_Invalid;
				return false;
			}

			// Attempt to allocate the phGlassInst and bgDrawable
			phGlassInst *glassInst = sm_pGlassInstPool->New();
			bgDrawable  *drawable  = sm_pDrawablePool ->New();
			if (Unlikely(!glassInst || !drawable))
			{
				sm_pGlassInstPool->Delete(glassInst);
				sm_pDrawablePool ->Delete(drawable );
				m_Breakable.Shutdown();
				*in_pHandle = bgGlassHandle_Invalid;
				return false;
			}

			RemoveFromList(&sm_FreeList);
			GLASSMANAGER_ASSERT(in_pHandle != NULL);
			m_pHandle = in_pHandle;
			AddToList(&sm_ActiveList, *m_pHandle);

			m_materialId = in_materialId;
			m_pGlassInst = glassInst;
			m_pGlassInst->SetIgnoreInst(in_ignoreInst);
			m_pGlassInst->SetMatrix(in_glassTransform);

			m_pDrawable = drawable;
			Assertf(m_pDrawable, "GlassInfo failed to allocate drawable");
			m_GetInstanceDataFunc = in_getInstanceDataFunc;
			m_pDrawable->Init();
			return true;
		}

		void TransferOwner(bgGlassHandle* newHandle, phInst* newIgnoreInst)
		{
			m_pGlassInst->SetIgnoreInst(newIgnoreInst);

			if(newIgnoreInst)
			{
				m_pGlassInst->SetUserData(newIgnoreInst->GetUserData());
			}
			m_pHandle = newHandle;
		}

		void Update(Mat34V_In in_glassMatrix, float in_dt)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			Mat34V lastMatrix = m_pGlassInst->GetMatrix();
			Assert(RCC_MATRIX34(in_glassMatrix).IsOrthonormal());
			m_pGlassInst->SetMatrix(in_glassMatrix);
			if (m_pGlassInst->GetLevelIndex()!=phInst::INVALID_INDEX)
				PHLEVEL->UpdateObjectLocation(m_pGlassInst->GetLevelIndex(), &RCC_MATRIX34(lastMatrix));
			m_Breakable.SetTransform(RCC_MATRIX34(in_glassMatrix));
			m_Breakable.Update(in_dt);
		}

		bgGlassHandle GetNext() const
		{
			GLASSMANAGER_ASSERT(gMainThread || GLASSMANAGER_MUTEX_IS_LOCKED());
			return m_Next;
		}

		bgGlassHandle GetPrev() const
		{
			GLASSMANAGER_ASSERT(gMainThread);
			return m_Prev;
		}

		void QueueImpact(ImpactHandle in_handle)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			in_handle.GetImpact()->m_nextImpact = m_ImpactQueue;
			m_ImpactQueue = in_handle;
		}

		bool GetLastQueueImpact(ImpactHandle &in_handle)
		{
			if(HasImpacts())
			{
				in_handle = m_ImpactQueue;
				return true;
			}
			return false;
		}

		bool HasImpacts() const
		{
			GLASSMANAGER_ASSERT(gMainThread);
			return !m_ImpactQueue.IsNull();
		}

		void Sync()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERT(m_pHandle != NULL);

			grcInstanceData& effectInstance = m_pDrawable->GetInstanceData();
			if (effectInstance.TotalSize == 0)
			{
				const grcInstanceData& srcInstanceData = m_GetInstanceDataFunc(m_shaderIndex);
				if (sm_pGlassShader)
				{
					// Create cloned instance data
					sm_pGlassShader->GetInstanceData().Clone(effectInstance);

					// copy effect vars from the base fragment
					CopyEffectInstanceData(&effectInstance, &srcInstanceData);
				}
				else // no special glass shader; just clone base shader
				{
					srcInstanceData.Clone(effectInstance);
				}
				m_pDrawable->CacheEffectVars();

				// Check if the original shader was using the low quality lighting
				bool bForceVSLighting = srcInstanceData.GetBasis().GetHashCode() == ATSTRINGHASH("glass_pv_env",  0xc5098ee2);
				m_pDrawable->SetForceVSLighting(bForceVSLighting);

				// Copy the bucket mask so the glass can render using the proper bucket
				effectInstance.DrawBucketMask = srcInstanceData.DrawBucketMask;
			}
			m_pDrawable->SetupDrawable(&m_Breakable);
		}

		inline bgDrawable& GetDrawable() const
		{
			GLASSMANAGER_ASSERT(gMainThread || GLASSMANAGER_MUTEX_IS_LOCKED());
			GLASSMANAGER_ASSERT(m_pDrawable);
			return *m_pDrawable;
		}

		inline bgBreakable& GetBreakable()
		{
			GLASSMANAGER_ASSERT(gMainThread || GLASSMANAGER_MUTEX_IS_LOCKED());
			return m_Breakable;
		}

		inline phGlassInst* GetPhysicsInst()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			return m_pGlassInst;
		}

		bool GetSphereBound(Vec4V_Ref out_s1)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			const phBound* bound;
			if (m_pGlassInst &&
				(NULL != (bound = m_phArchetype.GetBound())) )
			{
				out_s1 = Vec4V(bound->GetWorldCentroid(m_pGlassInst->GetMatrix()), bound->GetRadiusAroundCentroidV());
				return true;
			}
			return false;
		}

		bool GetAABB(spdAABB& out_AABB)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			const phBound* bound;
			if (m_pGlassInst &&
				(NULL != (bound = m_phArchetype.GetBound())) )
			{
				Vec3V obbCenter, obbExtents;
				bound->GetBoundingBoxHalfWidthAndCenter(obbCenter, obbExtents);
				Mat34V_ConstRef mtx = m_pGlassInst->GetMatrix();
				const Vec3V aabbExtents = geomBoxes::ComputeAABBExtentsFromOBB(mtx.GetMat33ConstRef(), obbExtents);
				const Vec3V aabbCenter = Transform(mtx, obbCenter);
				out_AABB.Set(aabbCenter - aabbExtents, aabbCenter + aabbExtents);
				return true;
			}
			return false;
		}

		void QueueShutdown()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			m_bShouldShutdown = true;
		}

		void SetCanRecycle(bool b)
		{
			m_bCanRecycle = b;
		}

		bgGlassHandle UpdateBuffers()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			bgGlassHandle nextHandle = GetNext();
			if (m_bShouldShutdown)
			{
				Shutdown();
			}
			else
			{
				nextHandle = ApplyImpacts();
				GetBreakable().UpdateBuffers();
			}

			return nextHandle;
		}

	private:
		void ApplyFrameImpact(const FrameImpactInfo& info)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			m_Breakable.BreakFrame(info.m_crackType, info.m_frameFlags, info.m_impactImpulse);
		}

		void ApplyPaneImpact(const PaneImpactInfo& info)
		{
			GLASSMANAGER_ASSERT(gMainThread);
			m_Breakable.Hit(info.m_crackType, info.m_impactLoc, info.m_impactImpulse, 0);
		}

		bgGlassHandle ApplyImpacts()
		{
			GLASSMANAGER_ASSERT(gMainThread);
			GLASSMANAGER_ASSERTF(!IsShutdown());
			if (IsShutdown())
			{
				return GetNext();
			}

			bool contact = false;
			bool allSilent = false;
			bool firstContact = false;
#if BREAKABLE_GLASS_USE_BVH
			bool paneContact = false;
#endif // BREAKABLE_GLASS_USE_BVH

			// Get some memory we need for temporary purposes
			void* tempMemory = PHSIM->GetContactMgr()->GetScratchpad();
			size_t tempMemorySize = PHSIM->GetContactMgr()->GetScratchpadSize();
			MEMORY_BLOCK_HEAP(ApplyImpacts,tempMemory,(int)tempMemorySize);

			ImpactHandle nextHandle = m_ImpactQueue;
			if (!nextHandle.IsNull())
			{
				allSilent = true;
				{
					USE_STACK_HEAP(ApplyImpacts, true);
					m_Breakable.Unpack();
					do {
						const ImpactInfo* impact = nextHandle.GetImpact();

						bgDebugf1("ApplyImpacts handle:%i impact handle:%i frame:%d", *m_pHandle, nextHandle.m_handle, TIME.GetFrameCount());

						if(impact->bSilentHit)
						{
							bgBreakable::SetSilentHit(true);
						}
						else
						{
							allSilent = false;
						}

						if (nextHandle.IsFrameImpact())
						{
							ApplyFrameImpact(static_cast<const FrameImpactInfo&>(*impact));
						}
						else
						{
							ApplyPaneImpact(static_cast<const PaneImpactInfo&>(*impact));
#if BREAKABLE_GLASS_USE_BVH
							paneContact = true;
#endif // BREAKABLE_GLASS_USE_BVH
						}

						if(impact->bSilentHit)
						{
							bgBreakable::SetSilentHit(false);
						}

						contact = true;
						nextHandle = impact->m_nextImpact;
					} while (!nextHandle.IsNull());

					// Don't hurt me.,,
					SetCanRecycle(false);
					m_Breakable.Pack();
					SetCanRecycle(true);
					PHSIM->GetContactMgr()->MarkScratchpadUsed((int)__heap.GetHeapSize() - (int)__heap.GetLowWaterMark(false));
				}

				if (!m_Breakable.IsValid())
				{
					bgGlassHandle nextGlassHandle = GetNext();
					Shutdown(); // @TEST GAME!
					return nextGlassHandle;
				}
				m_Breakable.Update(0); // make sure we have valid matrices for our first render
			}

			if (!m_phGlass.IsValid())
			{
				if (m_ImpactQueue.IsNull())
				{
					bgGlassHandle nextGlassHandle = GetNext();
					Shutdown();
					return nextGlassHandle;
				}
				firstContact = true;
				SetupPhysics(STACK_HEAP(ApplyImpacts));
			}
#if BREAKABLE_GLASS_USE_BVH
			else if (paneContact)
			{
				RebuildPhysics(STACK_HEAP(ApplyImpacts));
			}
#endif // BREAKABLE_GLASS_USE_BVH

			if( contact )
			{
				for(int iCurCF=0; iCurCF < sm_iNumContactFunctors; iCurCF++)
				{
					// This is a very ugly hack...
					// We want to skip the audio callback when the hit is hidden
					if(allSilent && iCurCF == 0)
						continue;

					if( sm_ContactFunctor[iCurCF].IsBound() )
					{
						bgContactReport report;

						report.pFragInst = (fragInst*)m_pGlassInst->GetIgnoreInst();
						report.handle = *m_pHandle;
						report.childIdx = m_sourceChildIdx;
						report.firstContact = firstContact;

						(sm_ContactFunctor[iCurCF])(&report);
					}
				}
			}

			m_ImpactQueue.Reset();

			return GetNext();
		}

		bgBreakable             m_Breakable;
		phArchetype             m_phArchetype;
		bgPackableData<phGlass> m_phGlass;
		bgDrawable*             m_pDrawable;
		phGlassInst*            m_pGlassInst;
		bgGlassHandle*          m_pHandle;
		bgGetInstanceDataFunc   m_GetInstanceDataFunc;
		int                     m_shaderIndex;
		phMaterialMgr::Id       m_materialId;
		ImpactHandle            m_ImpactQueue;
		bool                    m_bShouldShutdown;
		bool                    m_bCanRecycle;
		u8                      m_sourceChildIdx;

		bgGlassHandle           m_Prev;
		bgGlassHandle           m_Next;
	};


#if GLASSMANAGER_DEBUGGING
	bool GlassInfoList::Invariant() const
	{
		GLASSMANAGER_ASSERT(gMainThread);
		bgGlassHandle h = m_head;
		if (h == bgGlassHandle_Invalid)
		{
			GLASSMANAGER_INVARIANT_CHECK(!m_count);
		}
		else
		{
			const GlassInfo *inf = &sm_GlassInfo[h];
			GLASSMANAGER_INVARIANT_CHECK(inf->GetPrev() == bgGlassHandle_Invalid);
			unsigned c = 1;
			bgGlassHandle prev = h;
			h = inf->GetNext();
			while (h != bgGlassHandle_Invalid)
			{
				inf = &sm_GlassInfo[h];
				GLASSMANAGER_INVARIANT_CHECK(inf->GetPrev() == prev);
				prev = h;
				h = inf->GetNext();
				++c;
			}
			GLASSMANAGER_INVARIANT_CHECK(c == (unsigned)m_count);
		}
		return true;
	}
#endif // GLASSMANAGER_DEBUGGING


	bgGlassHandle FreeGlass(size_t in_memRequired)
	{
		GLASSMANAGER_ASSERT(gMainThread);
		sysMemAutoUseTempMemory useTemp; // make sure we switch to game heap
		sysMemAllocator& heap = bgBreakable::GetPrivateHeap();

		// free as much glass as necessary
		do
		{
			bgGlassHandle handle = sm_ActiveList.m_head;
			if (handle == bgGlassHandle_Invalid)
			{
				// no active glass to free...
				return bgGlassHandle_Invalid;
			}

			// walk to the end of the active list...
			for(bgGlassHandle nextHandle = sm_GlassInfo[handle].GetNext(); nextHandle != bgGlassHandle_Invalid; nextHandle = sm_GlassInfo[nextHandle].GetNext())
				handle = nextHandle;

			struct Rank
			{
				GlassInfo* pGlassInfo;
				float coverage;
				float dot;
				float fallen;
				float score;
				bool hasImpacts;
				bool visible;
			}
			best = {};

			// walk back up the list, recycling as we go...
			do
			{
				Assert(handle < sm_GlassInfo.GetCapacity());
				Rank rank = {&sm_GlassInfo[handle]};

				// grab the previous handle _before_ calling Shutdown()
				handle = rank.pGlassInfo->GetPrev();
				GLASSMANAGER_ASSERT(!rank.pGlassInfo->IsShutdown());
				if (rank.pGlassInfo->IsShutdown() || rank.pGlassInfo->CanRecycle() == false )
					continue;

				bgBreakable& breakable = rank.pGlassInfo->GetBreakable();

				// get glass remaining
				rank.fallen = breakable.GetFallenRatio();

				// get impacts
				rank.hasImpacts = rank.pGlassInfo->HasImpacts();

				if (grcViewport const* pViewport = sm_pViewport)
				{
					// find viewport coverage ratio
					rank.coverage = Min(breakable.CalculateViewportCoverage() / (pViewport->GetWidth() * 0.70710678118654752440084436210485f), 1.f);

					// find angle from viewport to glass
					const Matrix34& viewportMtx = RCC_MATRIX34(pViewport->GetCameraMtx());
					Vector3 d = viewportMtx.d - breakable.GetTransform().d;
					d.Normalize();
					rank.dot = viewportMtx.c.Dot(d);

					// find glass visibility
					Vec4V sphere;
					if (rank.pGlassInfo->GetSphereBound(sphere))
						rank.visible = pViewport->IsSphereVisible(sphere) != 0;
					else
						rank.visible = true;
				}

				// calculate score from viewport coverage, angle to glass, glass remaining, and impacts
				rank.score = rank.dot + rank.coverage * (1 - rank.fallen) + rank.hasImpacts;

				// compare current rank to best
				if (!best.pGlassInfo)
					best = rank;
				else
				{
					if (best.visible != rank.visible)
					{
						// favor glass that isn't in the view frustum
						if (best.visible)
							best = rank;
					}
					else
					{
						// choose best glass based on lowest score
						if (best.score > rank.score)
							best = rank;
					}
				}

			} while (bgGlassHandle_Invalid != handle);

			// ...and free it up
			if (!best.pGlassInfo)
				return bgGlassHandle_Invalid;
			best.pGlassInfo->Shutdown();

		} while (heap.GetLargestAvailableBlock() < in_memRequired);

		return sm_FreeList.m_head;
	}

	bgGlassHandle FreeOldest()
	{
		GLASSMANAGER_ASSERT(gMainThread);
		return FreeGlass(0);
	}

	void SetGlassShader(grmShader* in_pShader)
	{
		GLASSMANAGER_ASSERT(gMainThread);
		sm_pGlassShader = in_pShader;
	}

	void SetViewport(const grcViewport* pViewport)
	{
		GLASSMANAGER_ASSERT(gMainThread);
		sm_pViewport = pViewport;
	}
}

bool bgGlassManager::CreateBreakableGlass(
	bgGlassHandle* in_pGlassHandle,
	Mat34V_In in_glassTransform,
	const bgPaneModelInfoBase& in_modelInfo,
	bgGetInstanceDataFunc in_getInstanceDataFunc,
	phInst* in_ignoreInst,
	phMaterialMgr::Id in_materialId,
	float in_mass,
	int childIdx)
{
	GLASSMANAGER_ASSERT(gMainThread);
	RAGE_TRACK(BreakableGlass);

	GLASSMANAGER_MUTEX_AUTO_LOCK();

	if (sm_CreateCount++ >= kMaxCreateCount)
	{
		*in_pGlassHandle = bgGlassHandle_Invalid;
		bgDebugf1( "bgGlassManager::CreateBreakableGlass failure; CreateCount '%d' exceeds kMaxCreateCount '%d'.  frame:%d",
			sm_CreateCount, kMaxCreateCount, TIME.GetFrameCount() );
		return false;
	}

	PIXBegin(0, "bgGlassManager::CreateBreakableGlass");
	*in_pGlassHandle = sm_FreeList.m_head;
	if (bgVerifyf(*in_pGlassHandle != bgGlassHandle_Invalid, "bgGlassManager::CreateBreakableGlass(): no free handle!"))
	{
		bgDebugf1( "bgGlassManager::CreateBreakableGlass handle:%i frame:%d", *in_pGlassHandle, TIME.GetFrameCount() );
		Assert(*in_pGlassHandle < sm_GlassInfo.GetCapacity());
		sm_GlassInfo[*in_pGlassHandle].Init(in_pGlassHandle, in_glassTransform, in_modelInfo, in_getInstanceDataFunc, in_ignoreInst, in_materialId, in_mass, childIdx);
	}
	PIXEnd(); //PIXBegin(0, "bgGlassManager::CreateBreakableGlass");

	return (*in_pGlassHandle != bgGlassHandle_Invalid);
}

void bgGlassManager::TransferBreakableGlassOwner(bgGlassHandle* originalHandle,
							bgGlassHandle* newHandle,
							phInst* newIgnoreInst)
{
	bgAssertf(originalHandle && *originalHandle != bgGlassHandle_Invalid, "Invalid original handle");
	bgAssertf(newHandle, "Invalid new handle");
	sm_GlassInfo[*originalHandle].TransferOwner(newHandle,newIgnoreInst);
	*newHandle = *originalHandle;
	*originalHandle = bgGlassHandle_Invalid;
}

bool bgGlassManager::RecycleGlass(size_t in_memRequired)
{
	GLASSMANAGER_ASSERT(gMainThread);
	if (FreeGlass(in_memRequired) != bgGlassHandle_Invalid)
		return true;
	sysMemAutoUseTempMemory useTemp; // make sure we switch to game heap
	OUTPUT_ONLY(sysMemAllocator& heap = bgBreakable::GetPrivateHeap();)
	bgDebugf1("bgGlassManager::RecycleGlass(): Failed to free enough memory to satisfy request for %" SIZETFMT "d bytes", in_memRequired);
	bgDebugf1("%" SIZETFMT "d total, %" SIZETFMT "d used, %" SIZETFMT "d available, %" SIZETFMT "d largest", heap.GetHeapSize(), heap.GetMemoryUsed(), heap.GetMemoryAvailable(), heap.GetLargestAvailableBlock());
	return false;
}

bool bgGlassManager::IsHandleValid(bgGlassHandle in_glassHandle)
{
	GLASSMANAGER_ASSERT(gMainThread);
	bgBreakable& breakable = sm_GlassInfo[in_glassHandle].GetBreakable();
	return breakable.IsValid();
}

void bgGlassManager::DestroyBreakableGlass(bgGlassHandle in_glassHandle)
{
	GLASSMANAGER_ASSERT(gMainThread);
	GLASSMANAGER_MUTEX_AUTO_LOCK();

	if (Verifyf(bgGlassHandle_Invalid != in_glassHandle, "bgGlassManager::DestroyBreakableGlass() called with invalid glass handle"))
	{
		Assert(in_glassHandle < sm_GlassInfo.GetCapacity());
		sm_GlassInfo[in_glassHandle].Shutdown();
		sm_CleanUpGlassAudioFunctor(in_glassHandle);
	}
}

void bgGlassManager::UpdateBreakableGlass(bgGlassHandle in_glassHandle, Mat34V_In in_glassMatrix, float in_dt)
{
	GLASSMANAGER_ASSERT(gMainThread);
	GLASSMANAGER_MUTEX_AUTO_LOCK();

	if (Verifyf(bgGlassHandle_Invalid != in_glassHandle, "bgGlassManager::UpdateBreakableGlass(): invalid glass handle"))
	{
		Assert(in_glassHandle < sm_GlassInfo.GetCapacity());
		sm_GlassInfo[in_glassHandle].Update(in_glassMatrix, in_dt);
	}
}

bgDrawable& bgGlassManager::GetGlassDrawable(bgGlassHandle in_glassHandle)
{
	GLASSMANAGER_ASSERT(gMainThread);
	return sm_GlassInfo[in_glassHandle].GetDrawable();
}

bgBreakable& bgGlassManager::GetGlassBreakable(bgGlassHandle in_glassHandle)
{
	GLASSMANAGER_ASSERT(gMainThread);
	bgBreakable& breakable = sm_GlassInfo[in_glassHandle].GetBreakable();
	GLASSMANAGER_ASSERT(breakable.IsValid());
	return breakable;
}

phInst* bgGlassManager::GetGlassPhysicsInst(bgGlassHandle in_glassHandle)
{
	GLASSMANAGER_ASSERT(gMainThread);
	return sm_GlassInfo[in_glassHandle].GetPhysicsInst();
}

#if __BANK
namespace
{
	char s_glassPath[RAGE_MAX_PATH];
	pgDictionary<grcTexture>* s_pTextureDict;
	bool s_reloadGlassConfiguration = false;

	bool sm_bEnableProfile = false;

	// Memory stats
	int sm_iNumActiveInstances;
	int sm_iFreeInstances;
	int sm_iDrawableMem;
	int sm_iTotalFallingPieces;
	int sm_PrivateHeapUsed;
	int sm_PrivateHeapAvailable;
	int sm_PrivateHeapLargest;
	bool sm_bPrintCrackStats = false;
	bool sm_bPrintGlassTypeInfo = false;
	bool sm_bShowTextureInfo = false;
}
#endif

void bgGlassManager::InitClass()
{
	GLASSMANAGER_DEBUGGING_ONLY(gMainThread = true;)
	ShutdownClass();
	GLASSMANAGER_DEBUGGING_ONLY(gMainThread = true;)

	RAGE_TRACK(BreakableGlass);
#if __BANK
	ASSET.GetStackedPath(s_glassPath,RAGE_MAX_PATH);
	s_pTextureDict = pgDictionary<grcTexture>::GetCurrent();
#endif
	bgSCracksTemplate::Instantiate();
	bgSCracksTemplate::InstanceRef().InitClass();
	bgSCracksTemplate::InstanceRef().LoadResources();

#if GLASSMANAGER_MUTEX
	sm_Mutex = sysIpcCreateMutex();
	sm_MutexLockOwner = sysIpcCurrentThreadIdInvalid;
#endif // GLASSMANAGER_MUTEX
}

void bgGlassManager::ShutdownClass()
{
	Free();

	if (bgSCracksTemplate::IsInstantiated())
	{
		bgSCracksTemplate::InstanceRef().ShutdownClass();
		bgSCracksTemplate::Destroy();
	}

#if GLASSMANAGER_MUTEX
	if(sm_Mutex != NULL)
	{
		sysIpcDeleteMutex(sm_Mutex);
		sm_Mutex = NULL;
	}
#endif // GLASSMANAGER_MUTEX

	GLASSMANAGER_DEBUGGING_ONLY(gMainThread = false;)
}

void bgGlassManager::Malloc(int in_maxActiveGlass, int in_heapMem)
{
	GLASSMANAGER_ASSERT(gMainThread);

	Free();

	RAGE_TRACK(BreakableGlass);

	in_maxActiveGlass += kMaxCreateCount;
	GLASSMANAGER_DEBUGGING_ONLY(gMaxBreakableGlass = in_maxActiveGlass);

	bgBreakable::InitClass(in_heapMem);

	sm_GlassInfo.Resize(in_maxActiveGlass);
	sm_pGlassInstPool = rage_new phGlassInstPool(in_maxActiveGlass);
	sm_pDrawablePool = rage_new bgDrawablePool(in_maxActiveGlass);
	for (bgGlassHandle i=bgGlassHandle(in_maxActiveGlass)-1; i>=0; i--)
	{
		Assert(i < sm_GlassInfo.GetCapacity());
		sm_GlassInfo[i].AddToList(&sm_FreeList, i);
	}
}

#if RAGE_GLASS_USE_PRIVATE_HEAP
void bgGlassManager::Malloc(int in_maxActiveGlass, int in_bufferMemory, int in_heapMem)
{
	Malloc(in_maxActiveGlass, in_heapMem);
	bgDrawable::InitClass(in_maxActiveGlass, in_bufferMemory);
}
#elif RAGE_GLASS_USE_USER_HEAP
void bgGlassManager::Malloc(int in_maxActiveGlass, sysMemAllocator* in_bufferAllocator, int in_heapMem)
{
	Malloc(in_maxActiveGlass, in_heapMem);
	bgDrawable::InitClass(in_bufferAllocator);
}
#endif

void bgGlassManager::Free()
{
	GLASSMANAGER_ASSERT(gMainThread);

	if (sm_pGlassInstPool && sm_pDrawablePool)
	{
		// destroy anything still active
		while (sm_ActiveList.m_count)
		{
			DestroyBreakableGlass(sm_ActiveList.m_head);
		}

		SyncBreakableGlass();

		delete sm_pDrawablePool;
		sm_pDrawablePool = NULL;

		delete sm_pGlassInstPool;
		sm_pGlassInstPool = NULL;
	}

	sm_GlassInfo.Reset();

	GLASSMANAGER_ASSERT(!sm_ActiveList.m_count);
	sm_FreeList.m_head = bgGlassHandle_Invalid;
	sm_FreeList.m_count = 0;

	// free vertex/index buffer memory
	bgDrawable::ShutdownClass();

	// free bgBreakable heap
	bgBreakable::ShutdownClass();
}

void bgGlassManager::SetInitGlassBoundForGameFunctor(bgInitGlassBoundForGameFunctor in_functor)
{
	sm_InitGlassBoundForGameFunctor = in_functor;
}

void bgGlassManager::SetInitGlassInstForGameFunctor(bgInitGlassInstForGameFunctor in_functor)
{
	GLASSMANAGER_ASSERT(gMainThread);
	sm_InitGlassInstForGameFunctor = in_functor;
}

void bgGlassManager::SetShutdownGlassInstForGameFunctor(bgInitGlassInstForGameFunctor in_functor)
{
	GLASSMANAGER_ASSERT(gMainThread);
	sm_pGlassInstPool->SetShutdownFunc(in_functor);
}

void bgGlassManager::SetInitShardInstForGameFunctor(bgInitShardInstForGameFunctor in_functor)
{
	GLASSMANAGER_ASSERT(gMainThread);
	sm_InitShardInstForGameFunctor = in_functor;
}

void bgGlassManager::SetCleanUpGlassAudioFunctor(bgGlassImpactFunc in_functor)
{
	// This function is not called from the main thread, but it should only be
	// called during initialization, which make it safe.
	sm_CleanUpGlassAudioFunctor = in_functor;
}

void bgGlassManager::CallInitShardInstForGameFunctor(phInst& inst)
{
	GLASSMANAGER_ASSERT(gMainThread);
	sm_InitShardInstForGameFunctor(inst);
}

void bgGlassManager::SetContactFunctor(bgContactFunctor in_functor)
{
	// This function is not called from the main thread, but it should only be
	// called during initialization, which make it safe.
	Assertf(sm_iNumContactFunctors < sm_iMaxContactFunctors, "Tooo many contact functors added");
	sm_ContactFunctor[sm_iNumContactFunctors++] = in_functor;
}

void bgGlassManager::InitPhysicsInstPool(u16 numInsts)
{
	GLASSMANAGER_ASSERT(gMainThread);
	bgRagePhysics::InitInstPool(numInsts);
}

void bgGlassManager::SyncBreakableGlass()
{
	PIXBegin(0, "bgGlassManager::SyncBreakableGlass");

	GLASSMANAGER_ASSERT(gMainThread);
	GLASSMANAGER_ASSERT(sm_ActiveList.Invariant());
	GLASSMANAGER_ASSERT(sm_FreeList.Invariant());
	GLASSMANAGER_ASSERT((unsigned)(sm_ActiveList.m_count + sm_FreeList.m_count) == gMaxBreakableGlass);

	// sync active drawables to breakables
	bgGlassHandle handle = sm_ActiveList.m_head;
	while (bgGlassHandle_Invalid != handle)
	{
		Assert(handle < sm_GlassInfo.GetCapacity());
		GlassInfo& glassInfo = sm_GlassInfo[handle];
		// Get the next handle before calling Sync(); Sync() may move glassInfo to the free list...
		bgGlassHandle nextHandle = glassInfo.GetNext();
		glassInfo.Sync();
		handle = nextHandle;
	}

	PIXEnd();
}

void bgGlassManager::ProcessDeferredDeletes()
{
	PIXBegin(0, "bgGlassManager::ProcessDeferredDeletes");

	GLASSMANAGER_ASSERT(gMainThread);

	if (sm_pDrawablePool)
	{
		sm_pDrawablePool->ProcessDeferredDeletes();
	}

	if (sm_pGlassInstPool)
	{
		sm_pGlassInstPool->ProcessDeferredDeletes();
	}

#if __BANK
	if (s_reloadGlassConfiguration)
	{
		s_reloadGlassConfiguration = false;
		bgSCracksTemplate::InstanceRef().ShutdownClass();

		ASSET.PushFolder(s_glassPath);

		s_pTextureDict->Push();

		RAGE_TRACK(BreakableGlass);
		bgSCracksTemplate::InstanceRef().InitClass();
		bgSCracksTemplate::InstanceRef().LoadResources();
		ASSET.PopFolder();

		s_pTextureDict->Pop();
	}
#endif

	PIXEnd();
}

void bgGlassManager::UpdateBuffers()
{
	GLASSMANAGER_ASSERT(gMainThread);
	GLASSMANAGER_MUTEX_AUTO_LOCK();

	// We'd like to have at least sm_maxImpactCount glassInfos free for the start of next frame
	// since we are not permitted to do recycling mid-frame.  We might as well do this first
	// to avoid calling ApplyImpacts() on glassInfo that is about to be tossed.
	while ((sm_FreeList.m_count < kMaxCreateCount) &&
		   (bgGlassHandle_Invalid!=FreeOldest()));

	// Process queued impacts and generate gpu buffers and needed
	bgGlassHandle handle = sm_ActiveList.m_head;
	while (bgGlassHandle_Invalid != handle)
	{
		GLASSMANAGER_ASSERT((unsigned)handle < (unsigned)sm_GlassInfo.GetCount());
		GlassInfo& glassInfo = sm_GlassInfo[handle];
		handle = glassInfo.UpdateBuffers();
	}
	// Reset impact queues
	sm_FrameImpacts.Reset();
	sm_PaneImpacts.Reset();
	// Reset per-frame pane creation counter
	sm_CreateCount = 0;
}

bool bgGlassManager::GetSphereBound(bgGlassHandle in_glassHandle, Vec4V_Ref out_s)
{
	GLASSMANAGER_ASSERT(gMainThread);
	if (bgGlassHandle_Invalid == in_glassHandle)
	{
		return false;
	}
	Assert(in_glassHandle < sm_GlassInfo.GetCapacity());
	return (sm_GlassInfo[in_glassHandle].GetSphereBound(out_s));
}

bool bgGlassManager::GetAABB(bgGlassHandle in_glassHandle, spdAABB& out_AABB)
{
	GLASSMANAGER_ASSERT(gMainThread);
	if (bgGlassHandle_Invalid == in_glassHandle)
	{
		return false;
	}
	Assert(in_glassHandle < sm_GlassInfo.GetCapacity());
	return (sm_GlassInfo[in_glassHandle].GetAABB(out_AABB));
}

#if __BANK
void ReloadGlassConfiguration()
{
	GLASSMANAGER_ASSERT(gMainThread);
	s_reloadGlassConfiguration = true;
}

void bgGlassManager::AddWidgets(bkBank& bank)
{
	GLASSMANAGER_ASSERT(gMainThread);

	bank.AddToggle("Enable profile (not safe)", &sm_bEnableProfile);

	bgBreakable::AddWidgets(bank);
	bank.AddButton("Reload Glass Configuration",&ReloadGlassConfiguration);

	bank.PushGroup("Stats");
	bank.AddSlider("Active Panes", &sm_iNumActiveInstances, 0, 25, -1);
	bank.AddSlider("Free Panes", &sm_iFreeInstances, 0, 25, -1);
	bank.AddSlider("Drawable Buffer Size (in Kb)", &sm_iDrawableMem, 0, 1000, -1);
	bank.AddSlider("Private Heap Used Size (in Kb)", &sm_PrivateHeapUsed, 0, 1000, -1);
	bank.AddSlider("Private Heap Available Size (in Kb)", &sm_PrivateHeapAvailable, 0, 1000, -1);
	bank.AddSlider("Private Heap Largest Size (in Kb)", &sm_PrivateHeapLargest, 0, 1000, -1);
	bank.AddSlider("Total Falling Pieces", &sm_iTotalFallingPieces, 0, 1000, -1);
	bank.AddToggle("Highlight Visible Panes", &ms_bShowVisBGEntities);
	bank.AddToggle("Print Crack Stats", &sm_bPrintCrackStats);
	bank.AddToggle("Print Glass Types Info", &sm_bPrintGlassTypeInfo);
	bank.AddToggle("Print Texture Info", &sm_bShowTextureInfo);
	bank.PopGroup();
}
#endif

void bgGlassManager::HitGlass(bgGlassHandle in_glassHandle, int in_crackType, Vec3V_In in_worldImpactLoc, Vec3V_In in_worldImpactImpulse)
{
	GLASSMANAGER_ASSERT(gMainThread);
	bgDebugf1("HitGlass handle:%i framecount:%d cracktype: %i", in_glassHandle, TIME.GetFrameCount(), in_crackType);
	Assert(in_glassHandle < sm_GlassInfo.GetCapacity());
	GlassInfo& glassInfo = sm_GlassInfo[in_glassHandle];
	if (glassInfo.IsShutdown() || glassInfo.IsShutdownQueued())
		return;

	if (sm_PaneImpacts.GetCount() < sm_PaneImpacts.GetMaxCount())
	{
		ImpactHandle handle;

		// We don't want to overflow the array with too many hits that are very close to each other
		// Go over previous impacts and skip those that are below the minimum distance
		static float fMinHitDistSqr = 0.00025f;
		if(glassInfo.HasImpacts())
		{
			glassInfo.GetLastQueueImpact(handle);

			do
			{
				ImpactInfo* pImpact = handle.GetImpact();

				Vec3V vPos = (static_cast<const PaneImpactInfo*>(pImpact))->m_impactLoc;
				float fDist = MagSquared(vPos - in_worldImpactLoc).Getf();
				if(fDist < fMinHitDistSqr)
				{
					bgDebugf1("Skipping impact in location <%f, %f, %f> due to %f distance from location <%f, %f, %f>", in_worldImpactLoc.GetXf(), in_worldImpactLoc.GetYf(), in_worldImpactLoc.GetZf(), sqrtf(fDist), vPos.GetXf(), vPos.GetYf(), vPos.GetZf());
					return;
				}

				handle = pImpact->m_nextImpact;
			} while (!handle.IsNull());
		}

		handle.SetPaneImpactIndex(sm_PaneImpacts.GetCount());
		bgDebugf1("HitGlass handle:%i impact handle %i framecount:%d cracktype: %i position <%f, %f, %f>", in_glassHandle, handle.m_handle, TIME.GetFrameCount(), in_crackType, in_worldImpactLoc.GetXf(), in_worldImpactLoc.GetYf(), in_worldImpactLoc.GetZf());

		PaneImpactInfo& impactInfo = sm_PaneImpacts.Append();
		impactInfo.m_crackType = in_crackType;
		impactInfo.m_impactImpulse = in_worldImpactImpulse;
		impactInfo.m_impactLoc = in_worldImpactLoc;
		impactInfo.bSilentHit = bgGlassManager::IsSilentHit();
		glassInfo.QueueImpact(handle);
	}
}

bool bgGlassManager::GetLastImpactPositionAndMagnitude(bgGlassHandle in_glassHandle,Vec3V_Ref hitPosition,Vec3V_Ref hitMagnitude)
{
	GlassInfo& glassInfo = sm_GlassInfo[in_glassHandle];
	if (!glassInfo.IsShutdown() && !glassInfo.IsShutdownQueued())
	{
		if (sm_PaneImpacts.GetCount() > 0)
		{
			ImpactHandle handle;
			glassInfo.GetLastQueueImpact(handle);
			const ImpactInfo* impact = handle.GetImpact();
			const PaneImpactInfo &impactInfo = static_cast<const PaneImpactInfo&>(*impact);
			hitPosition = impactInfo.m_impactLoc;
			hitMagnitude = impactInfo.m_impactImpulse;
			return true;
		}
	}
	return false;
}

void bgGlassManager::BreakFrame(bgGlassHandle in_glassHandle, int in_crackType, u32 in_frameFlags, Vec3V_In in_worldImpactImpulse)
{
	GLASSMANAGER_ASSERT(gMainThread);
	bgDebugf1("BreakFrame handle:%i frame:%d cracktype: %i", in_glassHandle, TIME.GetFrameCount(), in_crackType);
	GlassInfo& glassInfo = sm_GlassInfo[in_glassHandle];
	if (glassInfo.IsShutdown() || glassInfo.IsShutdownQueued())
		return;

	if (sm_FrameImpacts.GetCount() < sm_FrameImpacts.GetMaxCount())
	{
		ImpactHandle handle;
		handle.SetFrameImpactIndex(sm_FrameImpacts.GetCount());
		bgDebugf1("BreakFrame handle:%i impact handle %i frame:%d cracktype: %i", in_glassHandle, handle.m_handle, TIME.GetFrameCount(), in_crackType);
		FrameImpactInfo& impactInfo = sm_FrameImpacts.Append();
		impactInfo.m_frameFlags = in_frameFlags;
		impactInfo.m_impactImpulse = in_worldImpactImpulse;
		impactInfo.m_crackType = in_crackType;
		glassInfo.QueueImpact(handle);
	}
}

void bgGlassManager::SetMaxLod(int lod)
{
	GLASSMANAGER_ASSERT(gMainThread);
	bgBreakable::SetMaxLod(lod);
}

int bgGlassManager::GetCrackTypeCount()
{
	bgCracksTemplate* pCracksTemplate;
	const char* szErrMsg = NULL;
	if (Verifyf(bgCracksTemplate::IsInitialized(pCracksTemplate, &szErrMsg), "Breakable glass system not initialized: %s\n", szErrMsg))
	{
		return pCracksTemplate->GetCrackTypes().GetCount();
	}

	return 0;
}

void bgGlassManager::SetViewportData(Vec3V_In in_cameraPos, int in_viewportSize, float in_tanFov)
{
	GLASSMANAGER_ASSERT(gMainThread);
	bgBreakable::SetViewportData(in_cameraPos, in_viewportSize, in_tanFov);
}


void bgGlassManager::SetSilentHit(bool bEnable)
{
	sm_bSilentHit = bEnable;
}

bool bgGlassManager::IsSilentHit()
{
	return sm_bSilentHit;
}

#if __BANK
void bgGlassManager::GetCracksTemplateMemoryUsage(int& usedGeomMemory, int& usedTexMemory)
{
	usedGeomMemory = bgSCracksTemplate::InstanceRef().GetGeometryMemoryUsage();
	usedTexMemory = bgSCracksTemplate::InstanceRef().GetTextureMemoryUsage();
}

int bgGlassManager::GetNumActiveInstances()
{
	int activeInstances = sm_ActiveList.m_count;
	return activeInstances;
}

int bgGlassManager::GetNumFreeInstances()
{
	return sm_FreeList.m_count;
}

int bgGlassManager::GetDrawableMemoryStats()
{
	int drawableMemoryUsage = 0;
	bgGlassHandle handle = sm_ActiveList.m_head;
	while (bgGlassHandle_Invalid != handle)
	{
		GlassInfo& glassInfo = sm_GlassInfo[handle];

		drawableMemoryUsage += glassInfo.GetDrawable().GetDynamicMemoryUsage();

		// Get the next handle before calling Sync(); Sync() may move glassInfo to the free list...
		bgGlassHandle nextHandle = glassInfo.GetNext();
		handle = nextHandle;
	}

	return drawableMemoryUsage;
}

 bool bgGlassManager::GetShowVisibleBreakableGlassEntities()
 {
	 return ms_bShowVisBGEntities;
 }
#endif // __BANK

#if __PFDRAW
void bgGlassManager::ProfileDraw()
{
// HACK for B*1694518, this is causing deadlocks with the D3D11 engine.
#if !__WIN32PC

	GLASSMANAGER_MUTEX_AUTO_LOCK();

	// Update memory stats
	sm_iNumActiveInstances = bgGlassManager::GetNumActiveInstances();
	sm_iFreeInstances = bgGlassManager::GetNumFreeInstances();
	sm_iDrawableMem = bgGlassManager::GetDrawableMemoryStats()/1024;
	sm_iTotalFallingPieces = 0;
	size_t used, available, largest;
	bgBreakable::GetPrivateHeapStats(used, available, largest);
	sm_PrivateHeapUsed = static_cast<int>(used / 1024);
	sm_PrivateHeapAvailable = static_cast<int>(available / 1024);
	sm_PrivateHeapLargest = static_cast<int>(largest / 1024);

	if(sm_bPrintCrackStats)
	{
		bgSCracksTemplate::InstanceRef().PrintMemoryUsageStats(sm_bPrintGlassTypeInfo, sm_bShowTextureInfo);
	}

	bgGlassHandle handle = sm_ActiveList.m_head;
	while (bgGlassHandle_Invalid != handle)
	{
		GlassInfo& glassInfo = sm_GlassInfo[handle];

		// Go over all the pieces and render the profile stuff
		bgBreakable& breakable = glassInfo.GetBreakable();
		breakable.ProfileDraw();

		// Update the amount of falling pieces
		sm_iTotalFallingPieces += breakable.GetFallingPieceCount();

		// Get the next handle
		handle = glassInfo.GetNext();
	}

#endif
}
#endif // __PFDRAW

#if GLASSMANAGER_DEBUGGING
#	undef GLASSMANAGER_INVARIANT_CHECK
#endif

} // namespace rage
