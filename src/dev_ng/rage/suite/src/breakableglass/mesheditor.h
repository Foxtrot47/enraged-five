// 
// breakableglass/mesheditor.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_MESHEDITOR_H 
#define BREAKABLEGLASS_MESHEDITOR_H 

#include "grcore/VertexBufferEditor.h"

namespace rage {
	struct mshMaterial;
	class grcIndexBuffer;

	class bgMeshEditor
	{
	public:
		virtual ~bgMeshEditor() {}

		int FindLargestTriangle(int in_uvSet, int in_boneIndex);
		void FindUVRange(int in_uvSet, int in_boneIndex, Vector2& out_uvMin, Vector2& out_uvMax);
		int GetMajorBone(int vertexIndex);

		virtual const u16* GetIndices() const = 0;
		virtual int GetIndexCount() const = 0;
		virtual int GetVertexCount() const = 0;
		virtual bool HasBindingsChannel() const = 0;
		virtual Color32 GetBlendIndices(int in_vertexIndex) = 0;
		virtual Vector4 GetBlendWeights(int in_vertexIndex) = 0;
		virtual Vector2 GetUV(int in_vertexIndex, int in_uvSet) = 0;
		virtual Vector3 GetPosition(int in_vertexIndex) = 0;
		virtual Vector3 GetTangent(int in_vertexIndex) = 0;
		virtual void Reset() = 0; // free all allocated memory; release all resources
	};

	// old vertex buffer/index buffer editor; for runtime generation of pane info
	class bgVertexBufferMeshEditor : public bgMeshEditor
	{
	public:
		bgVertexBufferMeshEditor(grcVertexBuffer* in_pVertexBuffer, grcIndexBuffer* in_pIndexBuffer);
		virtual ~bgVertexBufferMeshEditor();

		virtual const u16* GetIndices() const;
		virtual int GetIndexCount() const;
		virtual int GetVertexCount() const;
		virtual bool HasBindingsChannel() const;
		virtual Color32 GetBlendIndices(int in_vertexIndex);
		virtual Vector4 GetBlendWeights(int in_vertexIndex);
		virtual Vector2 GetUV(int in_vertexIndex, int in_uvSet);
		virtual Vector3 GetPosition(int in_vertexIndex);
		virtual Vector3 GetTangent(int in_vertexIndex);
		virtual void Reset();
	protected:
		grcIndexBuffer* m_pIndexBuffer;
		const u16* m_pIndices;
		grcVertexBufferEditor m_VertexBufferEditor;
	};

	class bgMshMaterialMeshEditor : public bgMeshEditor
	{
	public:
		bgMshMaterialMeshEditor(mshMaterial* in_pMshMaterial);
		virtual ~bgMshMaterialMeshEditor();

		virtual const u16* GetIndices() const;
		virtual int GetIndexCount() const;
		virtual int GetVertexCount() const;
		virtual bool HasBindingsChannel() const;
		virtual Color32 GetBlendIndices(int in_vertexIndex);
		virtual Vector4 GetBlendWeights(int in_vertexIndex);

		virtual Vector2 GetUV(int in_vertexIndex, int in_uvSet);
		virtual Vector3 GetPosition(int in_vertexIndex);
		virtual Vector3 GetTangent(int in_vertexIndex);

		virtual void Reset();
	protected:
		mshMaterial* m_pMshMaterial;
		atArray<u16> m_Indices;
	};

	// @TODO: mesh editor implementation using platform-independent resource-time mesh

} // namespace rage

#endif // BREAKABLEGLASS_MESHEDITOR_H 
