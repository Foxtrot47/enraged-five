//
// breakableglass/drawable.cpp
//
// Copyright (C) 2008-2014 Rockstar Games.  All Rights Reserved.
//

// Actual rendered geometry for a broken pane of glass (including falling pieces)
// Geometry is regenerated on every impact that causes more breakage; Falling pieces 
// are rendered using skinning matrices updated every frame during the 'DMZ'
//
// (XENON ONLY): Index and vertex buffer memory is allocated from a fixed pool 
// reserved at ClassInit().
//------------------------------------------------------------------------------

#include "bgdrawable.h"

#include "breakable.h"
#include "crackstemplate.h"
#include "geometrybuildercommon.h"
#include "optimisations.h"

#include "diag/tracker.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grcore/viewport.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "system/externalallocator.h"
#include "system/externalheap.h"
#include "system/memory.h"
#include "grprofile/pix.h"
#include "vectormath/legacyconvert.h"

// optimisations
BG_OPTIMISATIONS()

#define BG_SHADER_HIGH_QUALITY_BUMP_PASS 0
#define BG_SHADER_HIGH_QUALITY_PASS 1
#define BG_SHADER_LOW_QUALITY_PASS 2
#define BG_SHADER_CRACKEDGE_OFFSET_PASS 3

namespace rage {


grcVertexDeclaration *bgDrawable::sm_pVertexDecl = NULL;

#if __BANK
bool bgDrawable::sm_bUseOverrideCrackBumpSettings = false;
bool bgDrawable::sm_bDisableBumps = false;
Vec2V bgDrawable::sm_vCrackEdgeBumpTileScale = Vec2V(V_ONE);
Vec2V bgDrawable::sm_vCrackDecalBumpTileScale = Vec2V(V_ONE);
float bgDrawable::sm_fCrackEdgeBumpAmount = 1.0f;
float bgDrawable::sm_fCrackDecalBumpAmount = 1.0f;
float bgDrawable::sm_fCrackDecalBumpAlphaThreshold = 0.0f;
#endif

#if RAGE_GLASS_USE_PRIVATE_HEAP
#define RAGE_GLASS_HEAP_MEMTYPE 0

// Private heap for vertex and index buffer allocations
namespace 
{
	sysMemAllocator* g_pBufferAllocator;
	void* g_pPrivateHeapMemory;
	void* g_pWorkspace;
}

// InitClass():
// reserve physical memory for our vertex and index buffers
void bgDrawable::InitClass(int in_maxBufferAllocations, int in_totalBufferMemory)
{
	Assert(g_pBufferAllocator == NULL);
	in_maxBufferAllocations *= 4;
	g_pWorkspace = sysMemAllocator::GetCurrent().Allocate(COMPUTE_WORKSPACE_SIZE(in_maxBufferAllocations), 0);
	int align = rage::Max(RAGE_INDEXBUFFER_ALIGNMENT, RAGE_VERTEXBUFFER_ALIGNMENT);
	g_pPrivateHeapMemory = sysMemAllocator::GetCurrent().Allocate(in_totalBufferMemory * 1024, align, MEMTYPE_GAME_VIRTUAL);
	g_pBufferAllocator = rage_new sysMemExternalAllocator(in_totalBufferMemory * 1024, g_pPrivateHeapMemory, in_maxBufferAllocations, g_pWorkspace);
}

// ShutdownClass(): Call only after GPU has completed all rendering
// flush the gpu buffer countdown list and verify that all buffers are freed
// release physical memory we'd previously reserved
void bgDrawable::ShutdownClass()
{
	FlushCountdownList();

	Assert(AreAllBuffersFreed());
		delete g_pBufferAllocator;
		g_pBufferAllocator = NULL;
	if (g_pPrivateHeapMemory)
	{
		sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_GAME_VIRTUAL)->Free(g_pPrivateHeapMemory);
		g_pPrivateHeapMemory = NULL;
	}
	if (g_pWorkspace)
	{
		sysMemAllocator::GetCurrent().Free(g_pWorkspace);
		g_pWorkspace = NULL;
	}

	if(sm_pVertexDecl)
	{
		// release the static declarator
		grmModelFactory::FreeDeclarator(sm_pVertexDecl);
		sm_pVertexDecl = NULL;
	}
}

bool bgDrawable::AreAllBuffersFreed()
{
	return ! g_pBufferAllocator || 0 == g_pBufferAllocator->GetMemoryUsed();
}

#elif RAGE_GLASS_USE_USER_HEAP

#if __PS3 && BREAKABLE_GLASS_ON_SPU
#define RAGE_GLASS_HEAP_MEMTYPE MEMTYPE_RESOURCE_VIRTUAL
#else
#define RAGE_GLASS_HEAP_MEMTYPE MEMTYPE_RESOURCE_PHYSICAL
#endif

namespace 
{
	sysMemAllocator* g_pBufferAllocator;
}

void bgDrawable::InitClass(sysMemAllocator* bufferAllocator)
{
	g_pBufferAllocator = bufferAllocator;
}

void bgDrawable::ShutdownClass()
{

	if(sm_pVertexDecl)
	{
		// release the static declarator
		grmModelFactory::FreeDeclarator(sm_pVertexDecl);
		sm_pVertexDecl = NULL;
	}
}

#else

void bgDrawable::InitClass()
{
}

void bgDrawable::ShutdownClass()
{
	if(sm_pVertexDecl)
	{
		// release the static declarator
		grmModelFactory::FreeDeclarator(sm_pVertexDecl);
		sm_pVertexDecl = NULL;
	}
	FlushCountdownList();
}

bool bgDrawable::AreAllBuffersFreed()
{
	return true;
}

#endif

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
bgDrawable::bgDrawable() :
	m_bInUse(false),
	m_varCrackTexMatrix(grcevNONE),
	m_varCrackTexOffset(grcevNONE),
	m_pBuffers(NULL),
	m_bHitValid(false),
	m_bDrawnLastFrame(false)
	ASSERT_ONLY(, m_nLastDrawnFrame(0))
{
}//Constructor

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
bgDrawable::~bgDrawable()
{
	Term();
}//Destructor

//------------------------------------------------------------------------------
// Initialize our drawable based on the original exported version.
//------------------------------------------------------------------------------
void bgDrawable::Init()
{
	// We should already be cleaned up
	Assert(NULL == m_pBuffers);

	// Clear some stuff that might have been set from our last client
	m_bHitValid = false;
	memset(&m_EffectInstance, 0, sizeof(grcInstanceData));
	m_bDrawnLastFrame = false;
	ASSERT_ONLY(m_nLastDrawnFrame = 0);
	m_bInUse = true;
}//Init


//------------------------------------------------------------------------------
// Term(): Cleanup our data, happens on destruction or when we are discarded
//------------------------------------------------------------------------------
void bgDrawable::Term()
{
	m_bInUse = false;
	CheckBuffersInUse(ASSERT_ONLY(false));
	// Release our buffers (but don't free until GPU is done with them)
	m_pBuffers = NULL;
	m_bHitValid = false;
	// Cleanup effect instance
	m_EffectInstance.Shutdown();
	delete m_EffectInstance.Entries; // this would be done in ~grcInstanceData, but since it's not set NULL in there, do it here anyway
	m_EffectInstance.Entries = NULL;
	m_EffectInstance.TotalSize = 0;
}//Term()

//------------------------------------------------------------------------------
// Setup the information needed to draw the object. This should copy
// any relevant data into this glass, since this object will end up
// in the render thread and there's no guarantee that any pointers
// from the main thread will be valid when it goes to draw.
//------------------------------------------------------------------------------
void bgDrawable::SetupDrawable(bgBreakable* in_pGlassBreakable)
{
	// If we were drawn last frame, make sure our buffers stick around for a few frames even if we toss or replace them.
	// The only exception is when all the glass pieces have died and there is nothing left to draw
	CheckBuffersInUse(ASSERT_ONLY(in_pGlassBreakable->IsAlive())); 

	// Assert that we have valid parameters
	m_bHitValid = false;
	Assert(in_pGlassBreakable);
	if (in_pGlassBreakable == NULL)
	{
		return;
	}

	// check for replacement buffers
	if (in_pGlassBreakable->GetUpdatedBuffers(m_pBuffers))
	{
		// If Declaration is not set, then build it.
		if (m_pBuffers && m_pBuffers->GetVertexBuffer() && !sm_pVertexDecl)
		{
			// find/addref or create vertex declartion
			sm_pVertexDecl = grmModelFactory::BuildDeclarator(m_pBuffers->GetVertexBuffer()->GetFvf(), NULL, NULL, NULL);
		}
	}

	// Copy over hit information
	// @TODO: use hits other than 0?
	if (in_pGlassBreakable->GetHit(0))
	{
		m_bHitValid = true;
	}


} //SetupDrawable




//------------------------------------------------------------------------------
// Lookup effect vars and store them for more efficient use later.
//------------------------------------------------------------------------------
void bgDrawable::CacheEffectVars()
{
	// Shader parameters
	m_varCrackTexMatrix = m_EffectInstance.GetBasis().LookupVar("CrackMatrix", false);
	m_varCrackTexOffset = m_EffectInstance.GetBasis().LookupVar("CrackOffset", false);
	const bgShaderVars& shaderVars = bgSCracksTemplate::GetInstance().GetShaderVars();
	m_varMatrices = grcEffect::LookupGlobalVar(shaderVars.m_matrixGlobalVarName);

	int textureVarCount = shaderVars.m_textureVarDefaults.GetCount();
	m_textureVars.Reset();
	m_textureVars.Resize(textureVarCount);
	const grcEffect& basis = m_EffectInstance.GetBasis();
	for (int iTextureVar = 0; iTextureVar<textureVarCount; iTextureVar++)
	{
		m_textureVars[iTextureVar] = basis.LookupVar(shaderVars.m_textureVarDefaults[iTextureVar].m_varName, false);
	}
	int float4VarCount = shaderVars.m_float4VarDefaults.GetCount();
	m_float4Vars.Reset();
	m_float4Vars.Resize(float4VarCount);
	for (int iFloat4Var = 0; iFloat4Var<float4VarCount; iFloat4Var++)
	{
		m_float4Vars[iFloat4Var] = basis.LookupVar(shaderVars.m_float4VarDefaults[iFloat4Var].m_varName, false);
	}

	int float2VarCount = shaderVars.m_float2VarDefaults.GetCount();
	m_float2Vars.Reset();
	m_float2Vars.Resize(float2VarCount);
	for (int iFloat2Var = 0; iFloat2Var<float2VarCount; iFloat2Var++)
	{
		m_float2Vars[iFloat2Var] = basis.LookupVar(shaderVars.m_float2VarDefaults[iFloat2Var].m_varName, false);
	}

	int floatVarCount = shaderVars.m_floatVarDefaults.GetCount();
	m_floatVars.Reset();
	m_floatVars.Resize(floatVarCount);
	for (int iFloatVar = 0; iFloatVar<floatVarCount; iFloatVar++)
	{
		m_floatVars[iFloatVar] = basis.LookupVar(shaderVars.m_floatVarDefaults[iFloatVar].m_varName, false);
	}
}

bool bgDrawable::IsReadyToDraw() const
{
	if (!m_bHitValid)
		return false;

	if (!Verifyf(m_EffectInstance.TotalSize > 0, "No effect instance!\n"))
		return false;

	if (NULL == m_pBuffers)
		return false;
	
	return true;
}

//------------------------------------------------------------------------------
// Draw this bit of glass.  Execute in render thread.
//------------------------------------------------------------------------------
void bgDrawable::Draw(const float *transforms, int numTransforms, const Matrix34 &matrix, const Vector4 &crackTexMatrix,
						const Vector4 &crackTexOffset, bgGpuBuffers* pBuffers, const bgCrackStarMap* pCrackStarMap, int lod,
						int* arrPieceIndexCount, int* arrCrackIndexCount, u32 bucketMask) const
{
	if (!Verifyf(this != (bgDrawable*)0xdfdfdfde, "%p: bad glass pointer.\n", __FUNCTION__))
	{
		return;
	}
	
	if (!Verifyf(this != (bgDrawable*)0xdfdfdfdf, "%p: bad glass pointer.\n", __FUNCTION__))
	{
		return;
	}

	// If we don't have valid hit data, punt.  
	if (!Verifyf(pBuffers, "%s: no valid buffer passed in! nothing to render.\n", __FUNCTION__))
	{
		return;
	}

	// Early out if we don't have anything to draw
	if (!Verifyf(m_EffectInstance.TotalSize > 0, "No effect instance!\n"))
	{
		return;
	}

	// Make sure we are rendering with the right bucket
	if(!BUCKETMASK_MATCH(m_EffectInstance.DrawBucketMask, bucketMask))
	{
		return;
	}

	m_bDrawnLastFrame = true;
	ASSERT_ONLY(m_nLastDrawnFrame = GRCDEVICE.GetFrameCounter());

	// We shouldn't have issued this draw command if there are no transforms
	Assertf(numTransforms > 0, "Broken glass DC with no matrices - this should have been skipped");

	// Set world transform
	grcViewport::SetCurrentWorldMtx(RCC_MAT34V(matrix));
	// Setup shader

	grcInstanceData& instanceData = const_cast<grcInstanceData&>(m_EffectInstance);
	if (m_varCrackTexMatrix != grcevNONE)
	{
		m_EffectInstance.GetBasis().SetVar(instanceData, m_varCrackTexMatrix, crackTexMatrix);
	}

	if (m_varCrackTexOffset != grcevNONE)
	{
		m_EffectInstance.GetBasis().SetVar(instanceData, m_varCrackTexOffset, crackTexOffset);
	}

	// If this is the right bucket, draw
	// Should have these
	Assert(sm_pVertexDecl);

	grcEffect& basis = m_EffectInstance.GetBasis();
	// Setup texture vars
	for (int iTextureVar = 0; iTextureVar < m_textureVars.GetCount(); iTextureVar++)
	{
		basis.SetVar(instanceData, 
					 m_textureVars[iTextureVar], 
					 pCrackStarMap->m_textureVarValues[iTextureVar]);
	}

	// Setup float4 vars
	for (int iFloat4Var = 0; iFloat4Var < m_float4Vars.GetCount(); iFloat4Var++)
	{
		basis.SetVar(instanceData, 
			m_float4Vars[iFloat4Var], 
			pCrackStarMap->m_float4VarValues[iFloat4Var]);
	}

	// Setup float2 vars
	for (int iFloat2Var = 0; iFloat2Var < m_float2Vars.GetCount(); iFloat2Var++)
	{
		Vector2 value = pCrackStarMap->m_float2VarValues[iFloat2Var];
		BANK_ONLY(DebugOverrideFloat2Var(iFloat2Var, value);)
		basis.SetVar(instanceData, 
			m_float2Vars[iFloat2Var], 
			value);
	}

	// Setup float4 vars
	for (int iFloatVar = 0; iFloatVar < m_floatVars.GetCount(); iFloatVar++)
	{
		float value = pCrackStarMap->m_floatVarValues[iFloatVar];
		BANK_ONLY(DebugOverrideFloatVar(iFloatVar, value);)
		basis.SetVar(instanceData, 
			m_floatVars[iFloatVar], 
			value);
	}

	// Setup our buffers (cloned from device_d3d.cpp)
	GRCDEVICE.SetVertexDeclaration(sm_pVertexDecl);
	GRCDEVICE.SetIndices(*(pBuffers->GetIndexBuffer()));
	grcVertexBuffer& vb = *(pBuffers->GetVertexBuffer());
	GRCDEVICE.SetStreamSource(0, vb, 0, vb.GetVertexStride());

	int numPaneIndices = pBuffers->GetPaneIndexCount();

	const grcRasterizerStateHandle RS_prev = grcStateBlock::RS_Active;
	grcStateBlock::SetRasterizerState(grcStateBlock::RS_NoBackfaceCull);

	// Draw all the batches
	static const size_t transSize = sizeof(Matrix43) / sizeof(float);
	int varAddress = grcEffect::GetGlobalVarRegister(m_varMatrices);
	if (lod <= bgLod::LOD_LOW)
	{
		int iStartIndex = 0;
		for(int numTransformsRendered = 0, iCurBatch = 0; numTransformsRendered < numTransforms; numTransformsRendered += SKINNING_COUNT, iCurBatch++)
		{
			// Set up the matrices for the current batch
			int numBatchTransforms = numTransforms - numTransformsRendered > SKINNING_COUNT ? SKINNING_COUNT : numTransforms - numTransformsRendered;
			const float *const matrices = transforms + numTransformsRendered * transSize;

			// Update the counts and draw the pane pieces
			int iTotalPieces = arrPieceIndexCount[iCurBatch];
			DrawPanes(iStartIndex, iTotalPieces, !m_bForceVSLighting && lod == bgLod::LOD_LOW, matrices, numBatchTransforms, varAddress); // LOD_LOW is high quality,LOD_VLOW is low qaulity
			iStartIndex += iTotalPieces;
		}
		
		grcStateBlock::SetRasterizerState(RS_prev);
	}
	else
	{
		Matrix43 matPane;
		if(lod == bgLod::LOD_MED)
		{
			// Hack the first matrix to be identity
			matPane = *((Matrix43*)transforms);
			((Matrix43*)transforms)->Identity();
		}

		// Draw all the crack batches (two sided)
		int iStartIndex = 0;
		for(int numTransformsRendered = 0, iCurBatch = 0; numTransformsRendered < numTransforms; numTransformsRendered += SKINNING_COUNT, iCurBatch++)
		{
			// Set up the matrices for the current batch
			int numBatchTransforms = numTransforms - numTransformsRendered > SKINNING_COUNT ? SKINNING_COUNT : numTransforms - numTransformsRendered;
			const float *const matrices = transforms + numTransformsRendered * transSize;

			// Update the counts and draw the pane pieces
			int iTotalPieces = arrCrackIndexCount[iCurBatch];
			DrawCracks(iStartIndex + numPaneIndices, iTotalPieces, !m_bForceVSLighting, matrices, numBatchTransforms, varAddress);
			iStartIndex += iTotalPieces;
		}

		if(lod == bgLod::LOD_MED)
		{
			// Restore the panes matrix with the offset
			*((Matrix43*)transforms) = matPane;
		}
		else
		{
			// Switch to back face culling
			grcStateBlock::SetRasterizerState(RS_prev);
		}


		// Draw all the pane batches (two sided for LOD_MED)
		iStartIndex = 0;
		for(int numTransformsRendered = 0, iCurBatch = 0; numTransformsRendered < numTransforms; numTransformsRendered += SKINNING_COUNT, iCurBatch++)
		{
			// Set up the matrices for the current batch
			int numBatchTransforms = numTransforms - numTransformsRendered > SKINNING_COUNT ? SKINNING_COUNT : numTransforms - numTransformsRendered;
			const float *const matrices = transforms + numTransformsRendered * transSize;

			// Update the counts and draw the pane pieces
			int iTotalPieces = arrPieceIndexCount[iCurBatch];
			DrawPanes(iStartIndex, iTotalPieces, !m_bForceVSLighting, matrices, numBatchTransforms, varAddress);
			iStartIndex += iTotalPieces;
		}

		if(lod == bgLod::LOD_MED)
		{
			// Switch to back face culling
			grcStateBlock::SetRasterizerState(RS_prev);
		}
	}

	// Cloned from device_d3d.cpp
	GRCDEVICE.ClearStreamSource(0);

	// Set back our flag
	Vector4 texOffset = crackTexOffset;
	texOffset.w = 0.0f;

	if (m_varCrackTexOffset != grcevNONE)
	{
		m_EffectInstance.GetBasis().SetVar(instanceData, m_varCrackTexOffset, texOffset);
	}
}//Draw

//------------------------------------------------------------------------------
// Setup the matrices for the glass pieces
//------------------------------------------------------------------------------
void bgDrawable::SetMatrices(const float *transforms, int numTransforms, int varAddress)
{
	// Setup transform matrices
	// @NOTE: shader must be setup to use 4x3 matrices even if rage doesn't normally
	// We have to do this manually, as 4x3 matrices are not 4x3 but 4x4, 
	// if you don't believe me go check the way the skinning matrices are sets vs the SetGlobalVar for 4x3 matrices.
#if RSG_DURANGO || RSG_ORBIS
	(void) varAddress;
	const u32 sizeBytes = numTransforms*3*16;
	void *const cbuf = GRCDEVICE.BeginVertexShaderConstantF(SKINNING_CBUFFER,sizeBytes);
	sysMemCpy(cbuf,transforms,sizeBytes);
	GRCDEVICE.EndVertexShaderConstantF(SKINNING_CBUFFER);
#elif __PS3
	GCM_DEBUG(GCM::cellGcmSetVertexProgramParameterBlock(GCM_CONTEXT,varAddress,numTransforms*3,transforms));
#elif RSG_PC && __D3D11
	//On PC do this the same way the skinning matrices are done in grmGeometryQB::DrawSkinned
	GRCDEVICE.SetVertexShaderConstantF(varAddress,transforms,numTransforms*3,0,g_SkinningBase->GetDataPtr());
	g_SkinningBase->Unlock();
#elif RSG_PC
	GRCDEVICE.SetVertexShaderConstantF(varAddress,transforms,numTransforms*3,0,0);
#else
	grcVertexProgram::SetParameter(varAddress,transforms,numTransforms*3);
#endif
}

//------------------------------------------------------------------------------
// Draw the front and back glass panes
//------------------------------------------------------------------------------
void bgDrawable::DrawPanes(int iStartIndex, int iNumPaneIndices, bool bHighQuality, const float *pMatrices, int iNumMatrices, int iVarAddress) const
{
	{
		int groupId = grmShader::GetTechniqueGroupId();
		grcEffectTechnique tech = m_EffectInstance.GetBasis().GetDrawTechnique(groupId,grmShader::RMC_DRAW);
		Assertf(tech != grcetNONE, "bgDrawable::DrawPanes - missing group technique %d. Either we're missing a technique or unintentionally drawing in an unsupported renderphase", groupId);
		if(tech == grcetNONE)
		{
			return;
		}

		const char *techName = NULL;
#if EFFECT_PRESERVE_STRINGS
		techName = m_EffectInstance.GetBasis().GetTechniqueName(tech);
#endif
		PIXBeginN(0, "%s (Pane) - %s", m_EffectInstance.GetBasis().GetEffectName(), techName ? techName : "N/A");

		// NOTE: This function might return zero now if handed an invalid technique handle.
		// EndDraw knows to ignore this properly, but calling code needs to know to not draw anything.
#if __DEV
		PS3_ONLY(if (!g_grcCommandBuffer))
			GRCDBG_PUSH(m_EffectInstance.GetBasis().GetEffectName());
#endif
		int numPasses = m_EffectInstance.GetBasis().BeginDraw(tech, true);
		if(numPasses < 2)
		{
#if EFFECT_PRESERVE_STRINGS
			Quitf(ERR_GFX_DRAW_GLASS_1,"Breakable glass shader %s has only %d passes for group ID %s", m_EffectInstance.GetBasis().GetEffectName(), numPasses, techName);
#else
			Quitf(ERR_GFX_DRAW_GLASS_1,"Breakable glass shader %s has only %d passes for group ID %d", m_EffectInstance.GetBasis().GetEffectName(), numPasses, groupId);
#endif
		}
	}

	int iPass = (bHighQuality ? (BANK_ONLY(sm_bDisableBumps ? BG_SHADER_HIGH_QUALITY_PASS: )BG_SHADER_HIGH_QUALITY_BUMP_PASS) : BG_SHADER_LOW_QUALITY_PASS); // Choose the pass based on requested quality
	m_EffectInstance.GetBasis().BeginPass(iPass, m_EffectInstance);
	SetMatrices(pMatrices, iNumMatrices, iVarAddress);
	GRCDEVICE.DrawIndexedPrimitive(drawTris, iStartIndex, iNumPaneIndices);
	m_EffectInstance.GetBasis().EndPass();


	//m_pShader->EndDraw();
	{
		PIXEnd();

		m_EffectInstance.GetBasis().EndDraw();
#if __DEV
		PS3_ONLY(if (!g_grcCommandBuffer))
			GRCDBG_POP();
#endif
	}
}//DrawPanes

//------------------------------------------------------------------------------
// Draw the crack geometry
//------------------------------------------------------------------------------
void bgDrawable::DrawCracks(int iCrackStartIndex, int iNumCracIndices, bool bHighQuality, const float *pMatrices, int iNumMatrices, int iVarAddress) const
{
	// Draw glass cracks/bevels
	if ((iNumCracIndices) > 0)
	{
		{
			int groupId = grmShader::GetTechniqueGroupId();
			grcEffectTechnique tech = m_EffectInstance.GetBasis().GetDrawTechnique(groupId,grmShader::RMC_DRAW);
			Assertf(tech != grcetNONE, "B*1112166 - bgDrawable::DrawCracks - missing group technique %d", groupId);
			if(tech == grcetNONE)
			{
				return;
			}

			const char *techName = NULL;
#if EFFECT_PRESERVE_STRINGS
			techName = m_EffectInstance.GetBasis().GetTechniqueName(tech);
#endif
			PIXBeginN(0, "%s (Cracks) - %s", m_EffectInstance.GetBasis().GetEffectName(), techName ? techName : "N/A");

			// NOTE: This function might return zero now if handed an invalid technique handle.
			// EndDraw knows to ignore this properly, but calling code needs to know to not draw anything.
#if __DEV
			PS3_ONLY(if (!g_grcCommandBuffer))
				GRCDBG_PUSH(m_EffectInstance.GetBasis().GetEffectName());
#endif
			int numPasses = m_EffectInstance.GetBasis().BeginDraw(tech, true);
			if(numPasses < 2)
			{
#if EFFECT_PRESERVE_STRINGS
				Quitf(ERR_GFX_DRAW_GLASS_2,"Breakable glass shader %s has only %d passes for group ID %s", m_EffectInstance.GetBasis().GetEffectName(), numPasses, techName);
#else
				Quitf(ERR_GFX_DRAW_GLASS_2,"Breakable glass shader %s has only %d passes for group ID %d", m_EffectInstance.GetBasis().GetEffectName(), numPasses, groupId);
#endif
			}
		}

		int iPass = BG_SHADER_CRACKEDGE_OFFSET_PASS + (bHighQuality ? (BANK_ONLY(sm_bDisableBumps ? BG_SHADER_HIGH_QUALITY_PASS: ) BG_SHADER_HIGH_QUALITY_BUMP_PASS) : BG_SHADER_LOW_QUALITY_PASS); // Choose the pass based on requested quality
		m_EffectInstance.GetBasis().BeginPass(iPass, m_EffectInstance);
		SetMatrices(pMatrices, iNumMatrices, iVarAddress);
		GRCDEVICE.DrawIndexedPrimitive(drawTris, iCrackStartIndex, iNumCracIndices);
		m_EffectInstance.GetBasis().EndPass();

		//m_pShader->EndDraw();
		{
			PIXEnd();

			m_EffectInstance.GetBasis().EndDraw();
#if __DEV
			PS3_ONLY(if (!g_grcCommandBuffer))
				GRCDBG_POP();
#endif
		}

	}//if we have crack indices
}//DrawCracks

//------------------------------------------------------------------------------
// Geometry builder interface:

//-----------------------------------------------------------------------------
// Reference counting wrapper for index and vertex buffers.
// Supports use of pre-allocated memory on XENON
//-----------------------------------------------------------------------------
class bgGpuBuffers_Imp : public bgGpuBuffers
{
public:
	//
	// Constructor
	//
	bgGpuBuffers_Imp(int in_vertexCount, int in_indexCount, const rage::grcFvf* in_pFvf)
	  : m_pPreAllocatedVB(NULL)
	  , m_pPreAllocatedIB(NULL)
	  , m_pNextBuffers(NULL)
	  , m_frameCountdown(-1)
	{
		// need to have a vertex format to know how big vertices are
		Assert(in_pFvf);
#if RAGE_GLASS_USE_HEAP
		int vbSize = in_vertexCount * in_pFvf->GetTotalSize();
		{
			MEM_USE_USERDATA(MEMUSERDATA_BGGLASS);
			m_pPreAllocatedVB = g_pBufferAllocator->Allocate(vbSize, RAGE_VERTEXBUFFER_ALIGNMENT, RAGE_GLASS_HEAP_MEMTYPE);
		}

		// if we're out of memory, bail out
		if (m_pPreAllocatedVB == NULL)
		{
			return;
		}

		const bool bReadWrite = true;
		const bool bDynamic = false; // No need for dynamic allocation - we are only going to lock this once
		m_pVertexBuffer = grcVertexBuffer::Create(in_vertexCount, *in_pFvf, bReadWrite, bDynamic, m_pPreAllocatedVB);

		int ibSize = in_indexCount * sizeof(u16);

		{
			MEM_USE_USERDATA(MEMUSERDATA_BGGLASS);
			m_pPreAllocatedIB = g_pBufferAllocator->Allocate(ibSize, RAGE_INDEXBUFFER_ALIGNMENT, RAGE_GLASS_HEAP_MEMTYPE);
		}

		// if we're out of memory, bail out
		if (m_pPreAllocatedIB == NULL)
		{
			return;
		}
		// @NOTE: even though we use preallocated physical memory, it's still CPU cached memory, so
		// we need to specify 'dynamic'
		m_pIndexBuffer = grcIndexBuffer::Create(in_indexCount, true, m_pPreAllocatedIB);
#else
		const bool bReadWrite = true;
		const bool bDynamic = true;
		m_pVertexBuffer = grcVertexBuffer::Create(in_vertexCount, *in_pFvf, bReadWrite, bDynamic, NULL);
		m_pIndexBuffer = grcIndexBuffer::Create(in_indexCount);
#endif
	}

	// Destructor
	~bgGpuBuffers_Imp()
	{
		Assert(m_frameCountdown < 0);
		Assert(NULL == m_pNextBuffers);

		// @TODO: SafeDelete() template function might be nice here...
		if (NULL != m_pVertexBuffer)
		{
			delete m_pVertexBuffer;
			m_pVertexBuffer = NULL;
		}
		if (NULL != m_pIndexBuffer)
		{
			delete m_pIndexBuffer;
			m_pIndexBuffer = NULL;
		}
#if RAGE_GLASS_USE_HEAP
		if (NULL != m_pPreAllocatedVB)
		{
			MEM_USE_USERDATA(MEMUSERDATA_BGGLASS);
			g_pBufferAllocator->Free(m_pPreAllocatedVB);
			m_pPreAllocatedVB = NULL;
		}
		if (NULL != m_pPreAllocatedIB)
		{
			MEM_USE_USERDATA(MEMUSERDATA_BGGLASS);
			g_pBufferAllocator->Free(m_pPreAllocatedIB);
			m_pPreAllocatedIB = NULL;
		}
#endif
	}

	//
	// RefreshCountdown()
	//
	void RefreshCountdown()
	{
		if (m_frameCountdown < 0)
		{
			AddRef();
			Assert(m_pNextBuffers == NULL);
			m_pNextBuffers = sm_pCountdownList;
			sm_pCountdownList = this;
		}
		m_frameCountdown = sm_NumCountdownFrames;
	}

	//
	// Call once per frame to update the countdown list
	//
	static void UpdateCountdownList()
	{
		bgGpuBuffers_Imp** ppPrev = &sm_pCountdownList;
		bgGpuBuffers_Imp* pBuffers = sm_pCountdownList;
		while (pBuffers)
		{
			// grab the next list element
			bgGpuBuffers_Imp* pNextBuffers = pBuffers->m_pNextBuffers;
			// if the countdown is complete...
			Assert(pBuffers->m_frameCountdown >= 0);
			if (pBuffers->references == 1 && --(pBuffers->m_frameCountdown) < 0)
			{
				// remove buffers from list
				*ppPrev = pNextBuffers;
				pBuffers->m_pNextBuffers = NULL;
				// and decrement the refcount
				ASSERT_ONLY(int refcount =) pBuffers->Release();
				Assert(refcount == 0);
			}
			else
			{
				// update the prev pointer
				ppPrev = &pBuffers->m_pNextBuffers;
			}
			// move to the next element
			pBuffers = pNextBuffers;
		}
	}

	static void FlushCountdownList()
	{
		bgGpuBuffers_Imp* pBuffers = sm_pCountdownList;
		sm_pCountdownList = NULL;
		while (pBuffers)
		{
			bgGpuBuffers_Imp* pNextBuffers = pBuffers->m_pNextBuffers;
			// remove buffers from list
			pBuffers->m_frameCountdown = -1;
			pBuffers->m_pNextBuffers = NULL;
			ASSERT_ONLY(int refcount =) pBuffers->Release();
			Assert(refcount == 0);
			// move to next
			pBuffers = pNextBuffers;
		}		
	}

private:
	int m_frameCountdown;
	bgGpuBuffers_Imp* m_pNextBuffers; // next set of buffers in the global countdown list
#ifdef RAGE_GLASS_USE_HEAP
	// Do these need to still be here?
	ATTR_UNUSED void* m_pPreAllocatedVB;
	ATTR_UNUSED void* m_pPreAllocatedIB;
#endif
	static bgGpuBuffers_Imp* sm_pCountdownList;
	static int sm_NumCountdownFrames; // number of frames to keep buffers around after rendering
};


int bgGpuBuffers_Imp::sm_NumCountdownFrames = 4; // @TODO: hookup to bank/PARAM/bgInit
bgGpuBuffers_Imp* bgGpuBuffers_Imp::sm_pCountdownList;

//
// Call once per frame from main thread while rendering thread is idle
//
void bgDrawable::EndFrame()
{
	bgGpuBuffers_Imp::UpdateCountdownList();
}


//------------------------------------------------------------------------------
// Use this method to add a reference to our vertex and index buffers in the 
// current draw list.  We need to ensure that these are not deleted until the GPU
// has finished processing them.
//------------------------------------------------------------------------------
atReferenceCounter* bgDrawable::GetBuffers() const
{
	return m_pBuffers;
}

//------------------------------------------------------------------------------
// Get a pointer to the vertex buffer currently in use
//
//------------------------------------------------------------------------------
grcVertexBuffer* bgDrawable::GetVertexBuffer() const
{
	return m_pBuffers->GetVertexBuffer();
}

//------------------------------------------------------------------------------
// Get a pointer to the index buffer currently in use
//
//------------------------------------------------------------------------------
grcIndexBuffer* bgDrawable::GetIndexBuffer() const
{
	return m_pBuffers->GetIndexBuffer();
}

bgGpuBuffers* bgDrawable::AllocateBuffers(int in_vertexCount, int in_indexCount, const grcFvf& in_fvf)
{
	RAGE_TRACK(BreakableGlass);
	bgGpuBuffers* pResult = rage_new bgGpuBuffers_Imp(in_vertexCount, in_indexCount, &in_fvf);
	if (NULL == pResult->GetIndexBuffer() ||
		NULL == pResult->GetVertexBuffer())
	{
		delete pResult;
		pResult = NULL;
	}
	return pResult;
}

// Get memory usage (not including the bgDrawable class itself)
// @TODO: separate vb and ib tallies?
int bgDrawable::GetDynamicMemoryUsage() const
{
	int totalMem = 0;
	if (m_pBuffers)
	{
		totalMem += sizeof(bgGpuBuffers);
		const grcIndexBuffer* pIndexBuffer = m_pBuffers->GetIndexBuffer();
		if (pIndexBuffer)
			totalMem +=  pIndexBuffer->GetIndexCount() * sizeof(u16);
		const grcVertexBuffer* pVertexBuffer = m_pBuffers->GetVertexBuffer();
		if (pVertexBuffer)
			totalMem += pVertexBuffer->GetVertexCount() * pVertexBuffer->GetVertexStride();
		// @TODO: add platform-dependent code here for size of IB and VB structs
	}
	return totalMem;
}

void bgDrawable::CheckBuffersInUse(ASSERT_ONLY(bool IsGlassAlive))
{
	if (!m_bDrawnLastFrame)
		return;

	ASSERT_ONLY( if(IsGlassAlive || m_pBuffers) ) // Avoid the assert when the glass pieces are dead and the buffer is NULL on the next frame after it was drawn
	{
		if (Verifyf(m_pBuffers, "bgDrawable was rendered last frame (rendered: %u, current %u), but it's buffers are gone! That should not happen... (Update B*1119640 with full call stack)\n", m_nLastDrawnFrame, GRCDEVICE.GetFrameCounter()))
		{
			static_cast<bgGpuBuffers_Imp*>(m_pBuffers.p)->RefreshCountdown();
		}
	}


	m_bDrawnLastFrame = false;
}

void bgDrawable::MarkRenderedThisFrame()
{
	if (m_pBuffers)
	{
		static_cast<bgGpuBuffers_Imp*>(m_pBuffers.p)->RefreshCountdown();
	}
}


void bgDrawable::FlushCountdownList()
{
	bgGpuBuffers_Imp::FlushCountdownList();
}


#if __BANK
void bgDrawable::DebugOverrideFloat2Var(int iFloat2VarIndex, Vector2& inOutValue) const
{
	const bgShaderVars& shaderVars = bgSCracksTemplate::GetInstance().GetShaderVars();
	if(!sm_bUseOverrideCrackBumpSettings)
	{
		return;
	}

	if(shaderVars.m_float2VarDefaults[iFloat2VarIndex].m_varName == "CrackEdgeBumpTileScale")
	{
		inOutValue = RCC_VECTOR2(sm_vCrackEdgeBumpTileScale);
	}
	else if(shaderVars.m_float2VarDefaults[iFloat2VarIndex].m_varName == "CrackDecalBumpTileScale")
	{
		inOutValue = RCC_VECTOR2(sm_vCrackDecalBumpTileScale);
	}
	

}
void bgDrawable::DebugOverrideFloatVar(int iFloatVarIndex, float& inOutValue) const
{
	const bgShaderVars& shaderVars = bgSCracksTemplate::GetInstance().GetShaderVars();
	if(!sm_bUseOverrideCrackBumpSettings)
	{
		return;
	}

	if(shaderVars.m_floatVarDefaults[iFloatVarIndex].m_varName == "CrackEdgeBumpAmount")
	{
		inOutValue = sm_fCrackEdgeBumpAmount;
	}
	else if(shaderVars.m_floatVarDefaults[iFloatVarIndex].m_varName == "CrackDecalBumpAmount")
	{
		inOutValue = sm_fCrackDecalBumpAmount;
	}
	else if(shaderVars.m_floatVarDefaults[iFloatVarIndex].m_varName == "CrackDecalBumpAlphaThreshold")
	{
		inOutValue = sm_fCrackDecalBumpAlphaThreshold;
	}

}
#endif


} // namespace rage
