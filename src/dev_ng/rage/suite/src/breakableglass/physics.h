// 
// breakableglass/physics.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_PHYSICS_H
#define BREAKABLEGLASS_PHYSICS_H

#include "atl/pool.h"
#include "phbound/boundbox.h"
#include "physics/archetype.h"
#include "physics/collider.h"
#include "physics/inst.h"
#include "physics/sleep.h"

namespace rage
{
	//Physics interface for breakable glass
	struct bgPhysics
	{
		virtual ~bgPhysics()
		{
		}

		//These are used internally by breakable glass
		virtual void Init(float mass,
						  Matrix34 const &transform,
						  Vector3 const &impulse,
						  Vector3 const &position) = 0;
		virtual void Cleanup() = 0;
		virtual Matrix34 Update(float timeStep) = 0;
		virtual int GetStorage() const = 0;

		//These can be used externally through functors
		virtual float GetMass() const = 0;
		virtual Vector3 GetSize() const = 0;
		virtual Matrix34 GetTransform() const = 0;

		virtual void SetMass(float mass) = 0;
		virtual void SetTransform(Matrix34 const &transform) = 0;

		virtual bgPhysics* Clone() const = 0;

		virtual phCollider *GetCollider()
		{
			return 0;
		}

		//GTA_MAP_TYPE_MOVER = BIT(2)
		virtual bool IsAboveGround(u32 uIncludeFlags, u32 uExcludeFlags) = 0;
		bool IsAboveGround(Vector3 const &vPosition, u32 uIncludeFlags = BIT(2), u32 uExcludeFlags = TYPE_FLAGS_NONE);
	};

	//Rage physics implementation
	class bgRagePhysics : public bgPhysics
	{
	public:
		bgRagePhysics(const bgRagePhysics& that);
		bgRagePhysics(Vector3 const &size);
		~bgRagePhysics();

		void operator delete(void*) { }

		//These are used internally by breakable glass
		void Init(float mass,
				  Matrix34 const &transform,
				  Vector3 const &impulse,
				  Vector3 const &position);
		void Cleanup();
		Matrix34 Update(float);
		int GetStorage() const;

		//These can be used externally through functors
		float GetMass() const;
		Vector3 GetSize() const;
		Matrix34 GetTransform() const;

		void SetMass(float mass);
		void SetTransform(Matrix34 const &transform);

		bgPhysics* Clone() const;

		static void InitInstPool(u16 numInsts);

		phCollider* GetCollider();

		static bool PoolIsFull()
		{
			return !sm_instPool || sm_instPool->IsFull();
		}
		
		//GTA_MAP_TYPE_MOVER = BIT(2)
		bool IsAboveGround(u32 uIncludeFlags = BIT(2), u32 uExcludeFlags = TYPE_FLAGS_NONE);

	private:
		phBoundBox m_box;
		phArchetypePhys m_archetype;
		phInst* m_inst;

		static atPool<phInst>* sm_instPool;
	};

	//Simple implementation with no collision
	class bgSimplePhysics : public bgPhysics
	{
	public:
		bgSimplePhysics(Vector3 const &size);

		//These are used internally by breakable glass
		void Init(float mass,
				  Matrix34 const &transform,
				  Vector3 const &impulse,
				  Vector3 const &position);
		void Cleanup();
		Matrix34 Update(float timeStep);
		int GetStorage() const;

		//These can be used externally through functors
		float GetMass() const;
		Vector3 GetSize() const;
		Matrix34 GetTransform() const;

		void ApplyImpulse(Vector3 const &impulse, Vector3 const &position);

		void SetMass(float mass);
		void SetTransform(Matrix34 const &transform);

		bgPhysics* Clone() const;

		//GTA_MAP_TYPE_MOVER = BIT(2)
		bool IsAboveGround(u32 uIncludeFlags = BIT(2), u32 uExcludeFlags = TYPE_FLAGS_NONE);

#if __BANK
		static void AddWidgets(bkBank& bank);
#endif

	private:
		float m_linearMass;

		struct
		{
			Vector3 impulse;
			Vector3 velocity;
			Vector3 position;
		}
		m_linear, m_angular;

		Vector3 m_angularMass;
		Vector3 m_size;

		static float sm_gravityScale;
		static float sm_linearImpulseScale;
		static float sm_angularImpulseScale;
	};
}

#endif
