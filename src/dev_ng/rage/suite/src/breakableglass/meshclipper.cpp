// 
// breakableglass/meshclipper.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "meshclipper.h"
#include "optimisations.h"

#include "atl/bitset.h"

// optimisations
BG_OPTIMISATIONS()

namespace rage {

	// clip a mesh by a single plane, adding new points and edges and modifying old edges as appropriate
	class bgMeshClipper
	{
	public:
		typedef bgMesh::Polygons Polygons;

		struct Plane
		{
			Plane(const Vector2& p0, const Vector2& p1)
			{
				m_normal.x = p0.y - p1.y;
				m_normal.y = p1.x - p0.x;
				m_distance = m_normal.x * p0.x + m_normal.y * p0.y;
			}

			Vector2 m_normal;
			float   m_distance; // distance of plane from origin; if negative, half space includes the origin
		};

		bgMeshClipper(
			bgMesh& mesh)
			: m_mesh(mesh)
			, m_originalPointCount(m_mesh.GetPointCount())
		{
		}

		void Clip(const Vector2& in_p0, const Vector2& in_p1)
		{
			m_originalPointCount = m_mesh.GetPointCount();
			m_outPolygons.clear();
			m_edgeMap.clear();
			m_pointMap.clear();

			Plane plane(in_p0, in_p1);
			CodePoints(plane);
			// compute a map from old edge index to clipped edge index
			m_edgeMap.resize(m_mesh.GetEdgeCount());
			// reserve sufficient 
			// clip all edges
			ClipEdges(plane);
			for (int i=0; i<m_mesh.GetPolygonCount(); i++)
			{
				ClipPolygon(m_mesh.GetPolygon(i));
			}
			// sadly, there are now left-over clipped away edges and points polluting
			// our edge and point lists; let's get rid of them.
			m_pointMap.resize(m_originalPointCount);
			// remove all the clipped points
			bgIndex iNextPoint=0;
			bgMesh::Point *pPoints(m_mesh.GetPoints());
			for (int iPoint=0; iPoint < m_originalPointCount; iPoint++)
			{
				if (m_pointCodes.IsSet(iPoint))
				{
					m_pointMap[iPoint] = bgMesh::Edge::kPointRef_None;
					continue;
				}
				m_pointMap[iPoint] = iNextPoint;
				pPoints[iNextPoint++] = pPoints[iPoint];
			}
			int nNewPoints = m_mesh.GetPointCount() - m_originalPointCount;
			// copy all remaining points
			if (nNewPoints > 0)
				memmove(&pPoints[iNextPoint], &pPoints[m_originalPointCount], nNewPoints * sizeof(Vector2));
			// resize point arrays;
			m_mesh.SetPointCount(bgMesh::Index(iNextPoint + nNewPoints));
			// compute the point index delta for all the newly added points
			int delta = m_originalPointCount - iNextPoint;
			// fix up edges
			for (bgMesh::Index iEdge(0); iEdge < m_mesh.GetEdgeCount(); ++ iEdge)
			{
				bgMesh::Edge const &edge(m_mesh.GetEdge(iEdge));
				m_mesh.SetP0(iEdge, edge[0] < m_originalPointCount ? m_pointMap[edge[0]] : bgMesh::Index(edge[0] - delta));
				m_mesh.SetP1(iEdge, edge[1] < m_originalPointCount ? m_pointMap[edge[1]] : bgMesh::Index(edge[1] - delta));
			}
			// swap input and output poly arrays and move on to next plane
			m_mesh.SwapPolygons(m_outPolygons);
		}

	private:
		static const bgIndex kPointIndex_None = 0x7fff;
		static const bgIndex kEdgeIndex_None = 0x7fff;

		bool IsFirstPointIn(bgMesh::EdgeRef in_edgeRef) const
		{
			return m_mesh.GetP0(in_edgeRef) < m_originalPointCount;
		}

		void SetLeftPoly(bgMesh::EdgeRef in_edgeRef, bgIndex in_iPoly)
		{
			m_mesh.SetLeftPoly(in_edgeRef, in_iPoly);
		}

		bgMesh::EdgeRef AddEdge(bgIndex p0, bgIndex p1, bgIndex iPoly)
		{
			// @TODO: reserve sufficient space that Add() won't reallocate
			return m_mesh.Add(bgMesh::Edge(p0, p1, iPoly));
		}

		// if the specified edge was clipped, provide a handle to the clipped point
		// if it was not clipped, leaves out_clippedPoint unchanged
		bool GetClipped(bgMesh::EdgeRef in_edgeRef, bgIndex& out_clippedPoint)
		{
			// @OPTIMIZE: method to get both points at once, irrespective of direction...
			bgIndex p0 = m_mesh.GetP0(in_edgeRef);
			bgIndex p1 = m_mesh.GetP1(in_edgeRef);

			if (p0 >= m_originalPointCount)
			{
				out_clippedPoint = p0;
				Assertf(p1 < m_originalPointCount, "GetClipped() should not be called on completely clipped edges!\n");
				return true;
			}

			if (p1 >= m_originalPointCount)
			{
				out_clippedPoint = p1;
				return true;
			}
			return false;
		}


		bool RemapEdgeRef(bgMesh::EdgeRef& io_edgeRef)
		{
			bgMesh::Index newEdgeIndex(m_edgeMap[io_edgeRef.GetEdgeIndex()]);
			if (newEdgeIndex == kEdgeIndex_None)
				return false;
			io_edgeRef.SetEdgeIndex(newEdgeIndex);
			return true;
		}


		//
		// m_edgeMap is the mapping from original edge indices to clipped edge index
		//
		void ClipPolygon(const bgMesh::Polygon& in_poly)
		{
			bgMesh::Index nEdges = in_poly.GetEdgeCount();

			bgIndex iFirstClipped = kPointIndex_None;
			bgIndex iLastClipped = kPointIndex_None;

			bgIndex iPolygon = bgIndex(m_outPolygons.size());
			m_outPolygons.push_back(bgMesh::Polygon(in_poly.PieceId()));
			bgMesh::Polygon& outPoly = m_outPolygons.back();
			outPoly.ReserveEdgeCount(nEdges + 1);
			for (int iEdge=0; iEdge < nEdges; iEdge++)
			{
				bgMesh::EdgeRef edgeRef(in_poly[iEdge]);
				if (!RemapEdgeRef(edgeRef))
					continue; // edge was entirely clipped

				// fix up the edge to reference the poly we're adding
				SetLeftPoly(edgeRef, iPolygon);
				if (IsFirstPointIn(edgeRef))
				{
					// first add the old edge
					outPoly.Add(edgeRef);
					// now check whether we need to create a new edge
					if (GetClipped(edgeRef, iFirstClipped) && 
						(iLastClipped != kPointIndex_None))
					{
						outPoly.Add(AddEdge(iFirstClipped, iLastClipped, iPolygon));
					}
				}
				else
				{
					// First check whether we need to create a new edge
					if (GetClipped(edgeRef, iLastClipped) &&
						(iFirstClipped != kPointIndex_None))
					{
						// need to create a new edge between the clipped points
						outPoly.Add(AddEdge(iFirstClipped, iLastClipped, iPolygon));
					}
					// now add the old edge
					outPoly.Add(edgeRef);
				}
			}
			// if the polygon was entirely clipped, delete it
			if (outPoly.GetEdgeCount() == 0)
			{
				outPoly.Clear();
				m_outPolygons.erase(m_outPolygons.begin() + iPolygon);
			}
		}

		bool PointOutsidePlane(const Vector2& in_point, const Plane& in_plane)
		{
			return in_plane.m_normal.Dot(in_point) < in_plane.m_distance;
		}

		void CodePoints(const Plane& in_plane)
		{
			if (m_mesh.GetPointCount() < m_pointCodes.GetNumBits())
				m_pointCodes.Reset();
			else
				m_pointCodes.Init(m_mesh.GetPointCount());
			bgMesh::Point const *pPoints(m_mesh.GetPoints());
			for (int i=0; i<m_mesh.GetPointCount(); i++)
			{
				if (PointOutsidePlane(pPoints[i], in_plane))
					m_pointCodes.Set(i);
			}
		}

		Vector2 Intersect(bgIndex in_p0, bgIndex in_p1, const Plane& in_plane)
		{
			bgMesh::Point const *pPoints(m_mesh.GetPoints());
			const Vector2& p0 = pPoints[in_p0];
			const Vector2& p1 = pPoints[in_p1];
			float d0 = in_plane.m_normal.Dot(p0) - in_plane.m_distance;
			//if the distance is zero, this means point is on the plane
			if(Unlikely(d0 == 0.0f))	{ return p0; }
			float d1 = in_plane.m_normal.Dot(p1) - in_plane.m_distance;
			if(Unlikely(d1 == 0.0f)) { return p1; }
			float delta = d1 - d0;

			if (!Verifyf(Abs(delta) > VERY_SMALL_FLOAT, 
				         "Plane::Intersect() intersecting points are parallel: d0: %f, d1: %f, delta: %f", d0, d1, delta))
				return p0;

			if (!Verifyf(d0 * d1 <= 0.f, "Plane::Intersect(): line segment does not intersect plane! d0: %f d1: %f\n", d0, d1))
				return p0;

			float invDelta = 1.f/delta;
			d0 *= -invDelta; d1 *= invDelta;

			Vector2 result;
			result.Scale(p0, d1);
			result.AddScaled(p1, d0);
			return result;
		}

		void ClipEdges(const Plane& in_plane)
		{
			bgMesh::Index nextEdge=0;
			for (bgMesh::Index iEdge=0; iEdge<m_mesh.GetEdgeCount(); iEdge++)
			{
				bgMesh::Edge edge(m_mesh.GetEdge(iEdge));
				bool code0 = m_pointCodes.IsSet(edge[0]);
				bool code1 = m_pointCodes.IsSet(edge[1]);
				if (code0 && code1)
				{
					// edge is completely clipped; we'll just kill it by overwriting it
					m_edgeMap[iEdge] = kEdgeIndex_None;
					continue;
				}

				// map old edge index to new
				m_edgeMap[iEdge] = nextEdge;

				if (code0 != code1)
				{
					// edge must be clipped
					// @TODO: reserve sufficient space that Add() won't reallocate
					edge[code1] = m_mesh.Add(Intersect(edge[0], edge[1], in_plane));
				}
				m_mesh.SetEdge(nextEdge++, edge);
			}
			// resize to remove clipped edges
			m_mesh.SetEdgeCount(nextEdge);
		}

		bgMeshClipper& operator = (const bgMeshClipper& in_rhs); // using this makes baby jesus cry...

		typedef stlVector<bgIndex> EdgeMap;
		typedef stlVector<bgIndex> PointMap;

		Polygons m_outPolygons;
		EdgeMap m_edgeMap;
		PointMap m_pointMap;

		struct BitSet : atBitSet
		{
			// PURPOSE: Reclaim memory associated with bitset
			void Kill() { atBitSet::Kill(); m_Size = 0; m_BitSize = 0; }

			// PURPOSE: Default constructor, produces empty bitset
			BitSet() { m_BitSize = 0; }
		};

		BitSet m_pointCodes;
		bgMesh& m_mesh;
		int m_originalPointCount;
	};

} // namespace rage

void rage::bgClipMesh(bgMesh& io_mesh, Vector2 const *pPoints, size_t size)
{
	int nBoundsPoints = int(size);
	bgMeshClipper clipper(io_mesh);
	for (int iPrev = nBoundsPoints-1, iNext = 0; iNext<nBoundsPoints; iPrev = iNext, iNext++)
		clipper.Clip(pPoints[iPrev], pPoints[iNext]);
}
