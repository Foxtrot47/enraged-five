//
// breakableglass/bgdebug.h
//
// Copyright (C) 2012-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef RAGE_SUITE_SRC_BREAKABLEGLASS_BGDEBUG_H
#define RAGE_SUITE_SRC_BREAKABLEGLASS_BGDEBUG_H


#if __ASSERT
#	define GLASSMANAGER_DEBUGGING   1
#else
#	define GLASSMANAGER_DEBUGGING   0
#endif

#if GLASSMANAGER_DEBUGGING
#	define GLASSMANAGER_DEBUGGING_ONLY(...)     __VA_ARGS__
#	if __ASSERT
#		define GLASSMANAGER_ASSERT(EXP)             Assert(EXP)
#		define GLASSMANAGER_ASSERTF(EXP, ...)       Assertf(EXP, __VA_ARGS__)
#	else
#		define GLASSMANAGER_ASSERT(EXP)             do{if(Unlikely(!(EXP)))__debugbreak();}while(0)
#		define GLASSMANAGER_ASSERT(EXP)             do{if(Unlikely(!(EXP))){printf("%s(%u):%s\n",__FILE__,__LINE__,#EXP);__debugbreak();}}while(0)
#		define GLASSMANAGER_ASSERTF(EXP, ...)       GLASSMANAGER_ASSERT(EXP)
#	endif
#else
#	define GLASSMANAGER_DEBUGGING_ONLY(EXP)
#	define GLASSMANAGER_ASSERT(EXP)             (void)0
#	define GLASSMANAGER_ASSERTF(EXP, ...)       (void)0
#endif


#endif // RAGE_SUITE_SRC_BREAKABLEGLASS_BGDEBUG_H
