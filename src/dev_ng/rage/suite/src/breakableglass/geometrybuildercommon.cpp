//
// breakableglass/geometrybuildercommon.cpp 
// 
// Copyright (C) 2009 Rockstar Games.  All Rights Reserved. 
//


#if __SPU
#define SIMPLIFIED_ATL
#endif // __SPU

#include "geometrydata.h"
#include "geometrybuildercommon.h"
#include "geometrybuilder.h"
#include "optimisations.h"
#include "shaderlib/rage_constants.h"

#include "math/random.h"

#if (!__PS3) || (__SPU && BREAKABLE_GLASS_ON_SPU) || (__PPU && !BREAKABLE_GLASS_ON_SPU)

// optimisations
BG_OPTIMISATIONS()

namespace rage
{

// How much of the texture the bevels carve out.
const float bgGeometryBuilder::kBevelPercent = 0.2f;


// Given three 2d point compute the 'average' normal of the two line segments (in_prev, in_point) and (in_point, in_next).
// It would be more accurate to compute the angles of the two normals and interpolate those appropriately
inline void ComputeNormal(const rage::Vector2& in_point, const rage::Vector2& in_prev, const rage::Vector2& in_next, rage::Vector2& out_normal)
{
	rage::Vector2 n0, n1; // normal for each edge
	n0.x = in_prev.y - in_point.y;
	n0.y = in_point.x - in_prev.x;
	n1.x = in_point.y - in_next.y;
	n1.y = in_next.x - in_point.x;
	n0.NormalizeSafe(); n1.NormalizeSafe();
	out_normal = n0 + n1;
	out_normal.NormalizeSafe();
}


// Compute triangle normal
rage::Vector3 TriangleNormal(const rage::Vector3& va, const rage::Vector3& vb, const rage::Vector3& vc)
{
	rage::Vector3 v1 = vb - va;
	rage::Vector3 v2 = vc - va;

	rage::Vector3 v;
	v.Cross(v1, v2);
	v.Normalize();

	return v;
}


//Constructor.  Initialize some boolean and other variables at default value, nothing more
bgGeometryBuilder::bgGeometryBuilder(const bgGeometryData& in_geometryData) : m_geometryData(in_geometryData)
{
	// setup fixed array of random numbers for fast access.
	// these need to be deterministic so cracks don't change with multiple impacts.
	mthRandom randomGenerator;
	for (int i=0; i<kNumRand; i++)
		m_randArray[i] = randomGenerator.GetFloat();
}


unsigned bgGeometryData::ChoosePaneMatrix(Vector2 const &location) const
{
	if (!m_isReinforced)
		return 0;

	bool b(location.y > 0.5f), c(location.x < 0.5f);
	unsigned quadrant((unsigned(b) << 1) | unsigned(b ^ c));
	float x(location.x - 0.5f);
	float y(0.5f - location.y);
	float sincos[][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
	float x1(x * sincos[quadrant][1] + y * sincos[quadrant][0]);
	float y1(x * -sincos[quadrant][0] + y * sincos[quadrant][1]);
	b = y1 > 2 * x1;
	c = y1 > 0.25f * x1;
	return (((quadrant + b) << 1) + (b ^ c)) & 7;
}


// Generate the pane geometry (front and back faces of pieces; not the edges)
//
//			in_vertexBuffer ==>	vertex buffer to place the vertices in.  Each triangle has 3 vertex
//			in_vertexCount ==>	Current number of vertex in the buffer.  This counter must be
//								incremented in this function for other function to add
//								to the buffer properly.
void bgGeometryBuilder::CreatePanePiecesGeometry(bool in_bFlipFace, float fThickness)
{
	Vector3 normal(in_bFlipFace ? -m_normal : m_normal);
	Vector3 thickness(0.5f * fThickness * normal);
	Color32 vertexColor(0,0,0,255);

#if __SPU
	bgIndex panePointMap[m_geometryData.GetPanePoints().GetCount()];
#else
	atArray< bgIndex > panePointMap;
	panePointMap.Resize(m_geometryData.GetPanePoints().GetCount());
#endif // __SPU

	int iPiece = 0;
	const atArray<bgPanePiece>& panePieces(m_geometryData.GetPanePieces());
	for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++, iPiece++)
	{
		const bgPanePiece& piece = panePieces[iPanePiece];
		if (piece.m_silhouette.GetCount() < 3)
			continue;

		const atArray<bgDrawablePoint>& piecePoints = m_allPoints[iPiece];
		ASSERT_ONLY(bgIndex paneVtxStart = bgIndex(m_currentVertex);)

		int pointCount = piece.m_silhouette.GetCount();
		// Add all the vertices for the front pane
		for (int iPoint = 0; iPoint < pointCount; iPoint++)
		{
			// map shared point into the section of the vertex buffer used by this piece
			bgIndex pointIndex = piece.m_silhouette[iPoint].GetP0(m_geometryData.GetPaneEdges());
			panePointMap[pointIndex] = bgIndex(m_currentVertex);

			const bgDrawablePoint& point = piecePoints[iPoint];

			Assertf(piece.m_hit == -1 ? true : m_geometryData.GetHit(piece.m_hit)->m_decalChannelIndex <= 3, "Bad decal index % d for hit %d", m_geometryData.GetHit(piece.m_hit)->m_decalChannelIndex, piece.m_hit);

			//Decal visibility:  We know BREAKABLE_DECAL_BIT is 2.  therefore shifting it right will give us 0 or 1.
			// From there multiplying by 255 give is 0 or 255 which is what we want.
			vertexColor.SetRed(255 * ((point.m_vertexRed & BREAKABLE_DECAL_BIT) >> 1));
			vertexColor.SetBlue(piece.m_hit == -1 ? 0 : m_geometryData.GetHit(piece.m_hit)->m_decalChannelIndex);
			vertexColor.SetAlpha(m_geometryData.ChoosePaneMatrix(point.m_surfacePos));

			// actually add point into vertex buffer
			SetVertex(point.m_surfacePos, thickness - point.m_depth * m_normal, normal, point.m_surfaceDecal, vertexColor.GetDeviceColor());
		}//for iPoint

		// Add indices
		ASSERT_ONLY(bgIndex paneVtxStop = bgIndex(m_currentVertex);)
		for (int iPolygon = 0; iPolygon < piece.m_polygons.GetCount(); iPolygon++)
		{
			// convert convex polygons to trifans
			const bgPolygon& polygon = piece.m_polygons[iPolygon];
			int polygonIndexCount = polygon.GetCount();
			Assertf(polygonIndexCount >= 3, "Polygons must reference at least three vertices!\n");
			bgIndex fanBase = panePointMap[polygon[0]];
			Assertf(fanBase != 0xCDCD, "panePointMap does not contain %d (polygon[0])", polygon[0]);
			Assertf(fanBase >= paneVtxStart && fanBase < paneVtxStop, "Vertex index %d out of range [%d, %d]\n", fanBase, paneVtxStart, paneVtxStop );
			bgIndex fanPrev = panePointMap[polygon[1]];
			Assertf(fanPrev != 0xCDCD, "panePointMap does not contain %d (polygon[1])", polygon[1]);
			Assertf(fanPrev >= paneVtxStart && fanPrev < paneVtxStop, "Vertex index %d out of range [%d, %d]\n", fanPrev, paneVtxStart, paneVtxStop );
			for (int iIndex = 2; iIndex < polygonIndexCount; iIndex++)
			{
				bgIndex fanNext = panePointMap[polygon[iIndex]];
				Assertf(fanNext != 0xCDCD, "panePointMap does not contain %d (polygon[iIndex])", polygon[iIndex]);
				Assertf(fanNext >= paneVtxStart && fanNext < paneVtxStop, "Vertex index %d out of range [%d, %d]\n", fanNext, paneVtxStart, paneVtxStop );
				m_pIndices[m_currentIndex] = fanBase;
				m_pIndices[m_currentIndex+1+in_bFlipFace] = fanPrev;
				m_pIndices[m_currentIndex+2-in_bFlipFace] = fanNext;
				fanPrev = fanNext;
				m_currentIndex += 3;
			}
		}
	}//iPanePiece
}

void bgGeometryBuilder::CreateFallingPiecesGeometry(bool bTwoSides)
{
	int iPiece = m_geometryData.GetPanePieces().GetCount();
	const atArray<bgFallingPiece>& fallingPieces(m_geometryData.GetFallingPieces());
	for (int iFallingPiece = 0; iFallingPiece < fallingPieces.GetCount(); iFallingPiece++, iPiece++)
	{
		const bgFallingPiece& piece = fallingPieces[iFallingPiece];

		// If this piece is not active we pass
		if (piece.m_life < 0.f)
			continue;

		// Create the pieces
		const atArray<bgDrawablePoint>& piecePoints = m_allPoints[iPiece];
		if(bTwoSides)
		{
			float fThickness = m_geometryData.GetThickness();
			CreateSingleFallingPieceGeometry(piece, piecePoints, m_normal, iFallingPiece, false, fThickness);
			CreateSingleFallingPieceGeometry(piece, piecePoints, -m_normal, iFallingPiece, true, fThickness);
		}
		else
		{
			// For one side just create the piece with one side in the center
			CreateSingleFallingPieceGeometry(piece, piecePoints, m_normal, iFallingPiece, false, 0.0f);
		}
	}
}

void bgGeometryBuilder::CreateSingleFallingPieceGeometry(const bgFallingPiece& piece, const atArray<bgDrawablePoint>& piecePoints, const Vector3& normal, int iFallingPiece, bool bFrontFace, float fThickness)
{
	// Set the index in our matrix array
	Color32 vertexColor(0,0,0,(bgMatrixArray::FIRST_FALLING_PIECE + iFallingPiece) % SKINNING_COUNT);

	bgIndex paneVtxStart = bgIndex(m_currentVertex);

	// Store the center of mass
	Vector3 thickness(0.5f * fThickness * normal);
	Vector3 massThickness(thickness - piece.m_massCenteredTranslation);
	int pointCount = piece.m_points.GetCount();
	// Add all the vertices for the front pane
	for (int iPoint = 0; iPoint < pointCount; iPoint++)
	{
		Assertf(piece.m_hit == -1 ? true : m_geometryData.GetHit(piece.m_hit)->m_decalChannelIndex <= 3, "Bad decal index % d for hit %d", m_geometryData.GetHit(piece.m_hit)->m_decalChannelIndex, piece.m_hit);

		// Add point into vertex buffer
		vertexColor.SetRed(255); // falling glass is always visible
		vertexColor.SetBlue(piece.m_hit == -1 ? 0 : m_geometryData.GetHit(piece.m_hit)->m_decalChannelIndex);
		const bgDrawablePoint& point(piecePoints[iPoint]);
		SetVertex(point.m_surfacePos, massThickness - point.m_depth * m_normal, normal, point.m_surfaceDecal, vertexColor.GetDeviceColor());
	}//for iPoint (front pane) 

	// falling pieces must be convex, so just add face as trifan
	bgIndex fanBase = (bgIndex)paneVtxStart;
	bgIndex fanPrev = fanBase + 1;
	for (int index = 2; index < pointCount; index++)
	{
		bgIndex fanNext = fanBase + bgIndex(index);
		m_pIndices[m_currentIndex++] = fanBase;
		if (bFrontFace)
		{
			m_pIndices[m_currentIndex++] = fanNext;
			m_pIndices[m_currentIndex++] = fanPrev;
		}
		else
		{
			m_pIndices[m_currentIndex++] = fanPrev;
			m_pIndices[m_currentIndex++] = fanNext;
		}
		fanPrev = fanNext;
	}
}

#if !__SPU && __BANK
extern float sm_crackOffsetScale;
extern float sm_bevelOffsetScale;
#endif

// Generate all geometry (vertices and indices) for all pane faces and edges for both on-pane pieces and falling pieces.
void bgGeometryBuilder::CreateAllGeometry()
{
	m_currentVertex = 0;
	m_currentIndex = 0;

	// compute 2D positions of all perimeter points for all pieces
	const atArray<bgFallingPiece>& fallingPieces(m_geometryData.GetFallingPieces());
	const atArray<bgPanePiece>& panePieces(m_geometryData.GetPanePieces());

#if !__SPU && __BANK
	const float crackOffsetScale = sm_crackOffsetScale;
	const float bevelOffsetScale = sm_bevelOffsetScale;
#else
	const float crackOffsetScale = 1.0f;
	const float bevelOffsetScale = 1.0f;
#endif

	float crackOffset(m_geometryData.GetCrackSize() * crackOffsetScale);
	float bevelOffset(m_geometryData.GetBevelSize() * bevelOffsetScale);
	int numPieces = fallingPieces.GetCount() + panePieces.GetCount();

#if __SPU
	// Most of what follows below is done for the sake of not duplicating code

	// Loop and store drawable point array sizes
	int drawPointCounts[numPieces];
	int iPiece = 0;
	int drawPointTotalCount = 0;
	for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++, iPiece++)
	{
		const bgPanePiece& piece = panePieces[iPanePiece];
		drawPointCounts[iPiece] = piece.m_silhouette.GetCount();
		drawPointTotalCount += drawPointCounts[iPiece];
	}

	for (int iFallingPiece = 0; iFallingPiece < fallingPieces.GetCount(); iFallingPiece++, iPiece++)
	{
		const bgFallingPiece& piece = fallingPieces[iFallingPiece];
		if (piece.m_life < 0.f)
		{
			drawPointCounts[iPiece] = 0;
			continue;
		}

		drawPointCounts[iPiece] = piece.m_points.GetCount();
		drawPointTotalCount += drawPointCounts[iPiece];
	}

	// drawPointBuf contains all drawable points, whether they are part of a falling piece or pane piece
	bgDrawablePoint drawPointBuf[drawPointTotalCount];
	bgDrawablePoint* pCurDrawPoint = drawPointBuf;

	// Allocate for the arrays since the spu lacks a proper heap
	atArray<bgDrawablePoint> pointArrays[iPiece];
	m_allPoints.m_Elements = pointArrays;
	m_allPoints.m_Capacity = m_allPoints.m_Count = iPiece; 

	for (int i = 0; i < iPiece; ++i)
	{
		pointArrays[i].m_Elements = pCurDrawPoint;
		pointArrays[i].m_Capacity = pointArrays[i].m_Count = drawPointCounts[i];
		pCurDrawPoint += drawPointCounts[i];
	}

	iPiece = 0;

#else
	m_allPoints.Reset();
	m_allPoints.Resize( numPieces );

	int iPiece = 0;
#endif // __SPU

	const atArray<Vector2>& points(m_geometryData.GetPanePoints());
	const atArray<float>& depths(m_geometryData.GetPaneDepths());
	for (int iPanePiece=0; iPanePiece < panePieces.GetCount(); iPanePiece++,iPiece++)
	{
		const bgPanePiece& piece = panePieces[iPanePiece];
		int numPoints = piece.m_silhouette.GetCount();
		atArray<bgDrawablePoint>& piecePoints = m_allPoints[iPiece];

		// Compute normals for bevel and crack displacement
#if __SPU
		Vector2 normals[numPoints];
#else
		atArray< Vector2 > normals;
		normals.Resize(numPoints);
#endif

#if !__SPU
		piecePoints.Resize(numPoints);
#endif // !__SPU

		m_iRandom = iPanePiece * 17;
		for (int iPrev = numPoints-2, iPoint = numPoints-1, iNext = 0;
			iNext < numPoints;
			iPrev = iPoint, iPoint = iNext, iNext++)
		{
			bgIndex pointIndex = piece.m_silhouette[iPoint].GetP0(m_geometryData.GetPaneEdges());
			const Vector2& point = points[pointIndex];
			bgDrawablePoint& drawablePoint = piecePoints[iPoint];

			// if this point is not visible, set the normal to zero so it will not be displaced
			Vector2 normal;
			if (!m_geometryData.IsPointCrackVisible(pointIndex))
			{
				normal.Zero();
				//Nothing visible yet
				drawablePoint.m_vertexRed = 0;
			}
			else
			{
				// compute normal in the usual fashion
				const Vector2& prev = points[piece.m_silhouette[iPrev].GetP0(m_geometryData.GetPaneEdges())];
				const Vector2& next = points[piece.m_silhouette[iNext].GetP0(m_geometryData.GetPaneEdges())];
				ComputeNormal(point, prev, next, normal);
				//Cracks are visible
				drawablePoint.m_vertexRed = BREAKABLE_CRACK_BIT;
			}

			//Decal is visible
			if (m_geometryData.IsPointDecalVisible(pointIndex))
			{
				drawablePoint.m_vertexRed |= BREAKABLE_DECAL_BIT;
			}

			CalculatePointLocations(drawablePoint, point, normal, crackOffset, bevelOffset, piece.m_hit);
#if __SPU
			drawablePoint.m_depth = ((depths.m_Count == 0) ? 0.f : depths[pointIndex]);
#else
			drawablePoint.m_depth = depths.empty() ? 0.f : depths[pointIndex];
#endif // !__SPU
		}
	}

	for (int iFallingPiece=0; iFallingPiece < fallingPieces.GetCount(); iFallingPiece++, iPiece++)
	{
		const bgFallingPiece& piece = fallingPieces[iFallingPiece];
		if (piece.m_life < 0.f)
			continue;

		int numPoints = piece.m_points.GetCount();
		atArray<bgDrawablePoint>& piecePoints = m_allPoints[iPiece];

#if !__SPU
		piecePoints.Resize(numPoints);
#endif // !__SPU

		// Compute normals for bevel and crack displacement
#if __SPU
		Vector2 normals[numPoints];
#else
		atArray< Vector2 > normals;
		normals.Resize(numPoints);
#endif

		for (int iPrev = numPoints-2, iPoint = numPoints-1, iNext = 0;
			iNext < numPoints;
			iPrev = iPoint, iPoint = iNext, iNext++)
		{
			const Vector2& prev = piece.m_points[iPrev];
			const Vector2& point = piece.m_points[iPoint];
			const Vector2& next = piece.m_points[iNext];
			Vector2& normal = normals[iPoint];
			ComputeNormal(point, prev, next, normal);
		}

		m_iRandom = iFallingPiece * 29;
		for (int iPoint = 0; iPoint < numPoints; iPoint++)
		{
			bgDrawablePoint& point = piecePoints[iPoint];
			CalculatePointLocations(point, piece.m_points[iPoint], normals[iPoint], crackOffset, bevelOffset, piece.m_hit);
			point.m_depth = 0.f;
			point.m_vertexRed = 255; // all falling piece points are always visible
		}
	}

	// A flattened out view of the geometry (HIGH LOD)
	//
	//  |  / | <Front Pane Geometry>
	//  | /  |
	//  0 -- 1  Front Pane
	//  | 1/ | <Front Bevel Geometry>
	//  | /2 |
	//  2 -- 3  Bevel
	//  | 3/ | <Back Bevel Geometry>
	//  | /4 |
	//  3 -- 4  Back Pane
	//  |  / |<Back Pane Geometry>
	//  | /  |

	//Compute the steps
	//
	//Normal
	m_normal.Cross(m_geometryData.GetPosWidth(), m_geometryData.GetPosHeight());
	m_normal.Normalize();

#ifdef DRAW_PANES
	// Make the pane two sided only for the highest LOD
	bool bTwoSided = m_geometryData.GetLod() > bgLod::LOD_MED;
	if(bTwoSided)
	{
		float fThickness = m_geometryData.GetThickness();
		CreatePanePiecesGeometry(false, fThickness); // Front pane pieces
		CreatePanePiecesGeometry(true, fThickness); // Back pane pieces
	}
	else
	{
		CreatePanePiecesGeometry(false, 0.0f); // Just do one side
	}

	// Falling pieces
	CreateFallingPiecesGeometry(bTwoSided);

	// For low lod, don't generate the cracks geometry
	if (m_geometryData.GetLod() <= bgLod::LOD_LOW)
		return;

#endif // DRAW_PANES

	//--------------------------------------------------------------------------
	// Cracks
	//--------------------------------------------------------------------------
#ifdef DRAW_CRACKS
	// Green means we're on crack/bevel geometry
	Color32 vertexColor(0,255,0,0);
	iPiece = 0;
	Vector3 centerMass(Vector3::ZeroType);
	for (int iPanePiece = 0; iPanePiece < panePieces.GetCount(); iPanePiece++, iPiece++)
	{
		// Store pointer to this piece
		const bgPanePiece& piece = panePieces[iPanePiece];

		// If this piece is not active we pass
		if (!(piece.m_flags & bgPanePiece::kPF_Active))
			continue;

		AddCrackGeometry(m_allPoints[iPiece], centerMass, vertexColor);
	}

	for (int iFallingPiece = 0; iFallingPiece < fallingPieces.GetCount(); iFallingPiece++, iPiece++)
	{
		// Store pointer to this piece
		const bgFallingPiece& piece = fallingPieces[iFallingPiece];

		// If this piece is not active we pass
		if (piece.m_life < 0.f)
			continue;

		// Set the index in our matrix array
		vertexColor.SetAlpha((bgMatrixArray::FIRST_FALLING_PIECE + iFallingPiece) % SKINNING_COUNT);

		AddCrackGeometry(m_allPoints[iPiece], piece.m_massCenteredTranslation, vertexColor);
	}//for iPiece
#endif// DRAW_CRACKS
}//CreateAllGeometry


void bgGeometryBuilder::CalculatePointLocations(	
	bgDrawablePoint& out_point,
	const Vector2& in_basePos,
	const Vector2& in_normal,
	float in_crackOffset,
	float in_bevelOffset,
	int hit)
{
	float crackOffset = in_crackOffset * m_randArray[(m_iRandom++)&kRandMask];
	// bevelOffset needs to always be greater than crackoffset
	float bevelOffset = crackOffset + in_bevelOffset * m_randArray[(m_iRandom++)&kRandMask];
	out_point.m_interiorPos.AddScaled(in_basePos, in_normal, crackOffset);
	out_point.m_surfacePos.AddScaled(in_basePos, in_normal, bevelOffset);
	if (const bgHitInfo* pHit = m_geometryData.GetHit(hit))
	{
		// need a hit location and rotation for decal to make sense
		float cost, sint;
		cos_and_sin(cost, sint, pHit->m_rotation);
		Vector2 scale(2.f / pHit->m_scaleData.x, 2.f / pHit->m_scaleData.y), offset(0.5f, 0.5f);
		float mxx = cost * scale.x;
		float mxy = sint * scale.y;
		float myx = -sint * scale.x;
		float myy = cost * scale.y;

		Vector2 delta = (out_point.m_surfacePos - pHit->m_location) * pHit->m_decalTextureScale;
		out_point.m_surfaceDecal.x = delta.x * mxx + delta.y * mxy + offset.x;
		out_point.m_surfaceDecal.y = delta.x * myx + delta.y * myy + offset.y;

		delta = (out_point.m_interiorPos - pHit->m_location);
		out_point.m_interiorDecal.x = delta.x * mxx + delta.y * mxy + offset.x;
		out_point.m_interiorDecal.y = delta.x * myx + delta.y * myy + offset.y;
	}
	else
	{
		out_point.m_interiorDecal.Zero();
		out_point.m_surfaceDecal.Zero();
	}
}


void bgGeometryBuilder::AddCrackGeometry(
	const atArray<bgDrawablePoint>& piecePoints, 
	const Vector3& centerMass,
	Color32 vertexColor)
{
	Vector2 texCoord0;
	//  0 -- 1  Front Pane
	//  | 1/ |
	//  | /2 |
	//  2 -- 3  Bevel
	//  | 3/ |
	//  | /4 |
	//  5 -- 6  Back Pane

	// Loop over our points and create vertices
	int vtxStart = m_currentVertex;
	float distanceSum = 0;
	int nPoints = piecePoints.GetCount();
	int pointsPerEdge = m_geometryData.GetLod() == bgLod::LOD_HIGH ? 3 : 2;
	// Need to add the 0th point twice; different texcoords are used the 2nd time...
	// @TODO: computed vertex normals here only take into account a single face
	// even though vertices are shared by multiple faces with potentially different 
	// normals; this probably produces noticeable artifacts and ought to be fixed.
	float inverseThickness(1.f / m_geometryData.GetThickness());
	Vector3 distanceNormal(0.5f * m_geometryData.GetThickness() * m_normal);
	Vector3 nextMassDepth(centerMass + piecePoints[0].m_depth * m_normal);
	Vector3 nextSurfacePos(Vector3(m_geometryData.Transform(piecePoints[0].m_surfacePos)) - nextMassDepth);
	Vector3 nextSurfacePosPlusDistance(nextSurfacePos + distanceNormal);
	Vector3 nextInteriorPos(Vector3(m_geometryData.Transform(piecePoints[0].m_interiorPos)) - nextMassDepth);
	for (int iPoint = 0; iPoint <= nPoints; iPoint++)
	{
		// Store pointer to this point and the "next" one (1,3,5,7)
		int iCurrent = iPoint;
		if (iCurrent == nPoints)
			iCurrent = 0;
		int iNext = iCurrent + 1;
		if (iNext == nPoints)
			iNext = 0;

		// A few things are different if we're visible or not
		const bgDrawablePoint& current = piecePoints[iCurrent];
		const bgDrawablePoint& next = piecePoints[iNext];

		//Crack visibility - Red color is the alpha for the cracks
		//
		// We know that BREAKABLE_CRACK_BIT is bit 1 so multiplying by it will give us 0 or 255 which is what we are looking for
		vertexColor.SetRed(255 * (current.m_vertexRed & BREAKABLE_CRACK_BIT));

		Vector3 currentSurfacePos(nextSurfacePos);
		Vector3 currentSurfacePosPlusDistance(nextSurfacePosPlusDistance);
		Vector3 currentInteriorPos(nextInteriorPos);

		Vector3 nextMassDepth(centerMass + next.m_depth * m_normal);
		nextSurfacePos = Vector3(m_geometryData.Transform(next.m_surfacePos)) - nextMassDepth;
		nextSurfacePosPlusDistance = nextSurfacePos + distanceNormal;
		nextInteriorPos = Vector3(m_geometryData.Transform(next.m_interiorPos)) - nextMassDepth;

		Vector3 pos0(currentSurfacePosPlusDistance);
		Vector3 pos1(nextSurfacePosPlusDistance);
		Vector3 pos2(currentInteriorPos);
		Vector3 pos3(nextInteriorPos);
		Vector3 pos4(currentSurfacePos - distanceNormal);

		// X texture coordinate for first set of texture coords
		texCoord0.x = distanceSum * inverseThickness;
		distanceSum += (next.m_interiorPos - current.m_interiorPos).Mag();

		//  0 -- 1  Front Pane
		//  | 1/ |
		//  | /2 |
		//  2 -- 3  Bevel Front
		//  | 3/ |
		//  | /4 |
		//  4 -- 5  Back Pane

		if (m_geometryData.IsReinforced())
			vertexColor.SetAlpha(m_geometryData.ChoosePaneMatrix(current.m_surfacePos));

		Vector3 frontNormal = TriangleNormal(pos0, pos1, pos2);
		// Pooint 0
		texCoord0.y = 0.0f;
		// @TODO: better normal computation; and adjust correctly with lod :P
		SetVertex(pos0, frontNormal, texCoord0, current.m_surfaceDecal, vertexColor.GetDeviceColor());

		Vector3 bevelNormal(TriangleNormal(pos4, pos3, pos2));

		// Point 2 (only needed for high lod)
		if (m_geometryData.GetLod() == bgLod::LOD_HIGH)
		{
			Color32 interiorColor(vertexColor);
			if (m_geometryData.IsReinforced())
				interiorColor.SetAlpha(m_geometryData.ChoosePaneMatrix(current.m_interiorPos));
			texCoord0.y = kBevelPercent;
			SetVertex(pos2, -bevelNormal, texCoord0, current.m_interiorDecal, interiorColor.GetDeviceColor());
		}

		// Point 4
		texCoord0.y = 1.0f - kBevelPercent;
		SetVertex(pos4, bevelNormal, texCoord0, current.m_surfaceDecal, vertexColor.GetDeviceColor());
	}//for iPoint

	// Now add the triangles for the bevel/crack
	//  0 -- 1  Front Pane
	//  | 1/ |
	//  | /2 |
	//  2 -- 3  Bevel
	//  | 3/ |
	//  | /4 |
	//  4 -- 5  Back Pane
	for (int iPoint = 0; iPoint < piecePoints.GetCount(); iPoint++)
	{
		// Figure out indices
		u16 p0 = u16(iPoint * pointsPerEdge + vtxStart);
		u16 p1 = u16((iPoint+1) * pointsPerEdge + vtxStart);
		u16 p2 = p0 + 1;
		u16 p3 = p1 + 1;
		Assert(p3 < m_currentVertex);
		// Bevel for the front pane (triangle 1)
		m_pIndices[m_currentIndex++] = p0;
		m_pIndices[m_currentIndex++] = p2;
		m_pIndices[m_currentIndex++] = p1;

		// Bevel for the front pane (triangle 2)
		m_pIndices[m_currentIndex++] = p1;
		m_pIndices[m_currentIndex++] = p2;
		m_pIndices[m_currentIndex++] = p3;

		if (m_geometryData.GetLod() == bgLod::LOD_HIGH)
		{
			u16 p4 = p0 + 2;
			u16 p5 = p1 + 2;
			Assert(p5 < m_currentVertex);

			// Crack (triangle 3)
			m_pIndices[m_currentIndex++] = p2;
			m_pIndices[m_currentIndex++] = p4;
			m_pIndices[m_currentIndex++] = p3;

			// Crack (triangle 4)
			m_pIndices[m_currentIndex++] = p3;
			m_pIndices[m_currentIndex++] = p4;
			m_pIndices[m_currentIndex++] = p5;
		}
	}//for iPoint
}


// Set vertex using the texture coordinates of the original model
void bgGeometryBuilder::SetVertex(
	const rage::Vector2& in_2dPoint,
	const rage::Vector3& in_offset,
	const rage::Vector3& in_normal,
	const rage::Vector2& in_2dDecal,
	const u32 in_deviceColor)
{
	SetVertex(
		Vector3(m_geometryData.Transform(in_2dPoint)) + in_offset,
		in_normal, in_2dPoint, in_2dDecal, in_deviceColor
		);
}//SetVertex


#if !__SPU
void bgGeometryBuilder::SetVertex(
	const rage::Vector3& in_position,
	const rage::Vector3& in_normal,
	const rage::Vector2& in_texCoord0,
	const rage::Vector2& in_texCoord1,
	const u32 in_deviceColor)
{
	char *p(static_cast<char *>(m_vb.p) + m_vb.stride * m_currentVertex++);
	memcpy(p + m_vb.position.offset, &in_position, m_vb.position.size);
	memcpy(p + m_vb.normal.offset, &in_normal, m_vb.normal.size);
	memcpy(p + m_vb.texture0.offset, &in_texCoord0, m_vb.texture0.size);
	memcpy(p + m_vb.texture1.offset, &in_texCoord1, m_vb.texture1.size);
	memcpy(p + m_vb.diffuse.offset, &in_deviceColor, m_vb.diffuse.size);
}
#endif

} // namespace rage
#endif // (!__PS3) || (__SPU && BREAKABLE_GLASS_ON_SPU) || (__PPU && !BREAKABLE_GLASS_ON_SPU)
