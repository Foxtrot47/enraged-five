//
// breakableglass/polygondicer.cpp 
// 
// Copyright (C) 2008 Rockstar Games.  All Rights Reserved. 
//

// -------------------------------------------------------------------------------------------------
// This class takes a silhouette and cuts it into smaller pieces.  This function must be optimize and
// therefore we do not use dynamic array.  This function has two define telling us the maximum of data
// required:
//		Max number of piece to be cut into
//		Max numbert of vertex per peice
//
// Once the piece is cut into smaller pieces, the user is responsible to read the structure in this 
// function and create the final pieces with the data.  Also this has to be done right away because
// the memory use to hold the data is static and therefore will be erase by an other piece being cut 
// (NOT MULTI THREAD-SAFE).
//
// Direction:
//		1 - You have one piece you want to be cut in X pieces
//		2 - Call Cut() with the proper parameters
//		3 - All pieces in m_pieces that have a m_use == true should be use to create a piece
// -------------------------------------------------------------------------------------------------

#include "polygondicer.h"

#include "crackstemplate.h"
#include "optimisations.h"

#include "math/simplemath.h"

#define	DONT_INTERSECT2    0
#define	DO_INTERSECT2      1
#define COLLINEAR2         0

// optimisations
BG_OPTIMISATIONS()

namespace rage {

//We cut the piece with a line.  These are the lines available to us.  They will be rotated around axis 0,0.
//Lines
Vector4	bgPolygonDicer::m_cutLines[LINE_CUT_COUNT];
//Next line to use: Index in cutLines
int bgPolygonDicer::m_cutLineIndex = 0;

//Pieces data
bgPolygonDicer::CutPiece	bgPolygonDicer::m_pieces[SmallPiecesMaxPieces];
//Pieces count:  This tell us how many pieces have been stored in our array
int						bgPolygonDicer::m_piecesCount;

//Create a cutting class so that the constructor get called at least one.  This is necessary
// in order for our constructor to initialize some values.
bgPolygonDicer					bgPolygonDicer::m_notUseCuttingClass;

//-----------------------------------------------------------------
//Constructor:  Init some values
//
bgPolygonDicer::bgPolygonDicer()
{
	//-----------------------------
	//Init the line use for cutting
	//
	//First line to be use is the index 0
	m_cutLineIndex = 0;

	//Create all the lines we will be using to cut pieces in two
	//	
	//Create a new line for the line count
	for (int i = 0;i < LINE_CUT_COUNT;i++)
	{
		//This is our line not rotated yet
		Vector4 lineTemp;
		lineTemp.x = 0;
		lineTemp.y = - 100;
		lineTemp.z = 0;
		lineTemp.w = 100;

		//Get random rotation angle
		float rot = m_rand.GetFloat() * 6;
		float cosV = cos(rot);
		float sinV = sin(rot);
		
		//Rotate the 2 points
		m_cutLines[i].x = (cosV*lineTemp.x - sinV*lineTemp.y);
		m_cutLines[i].y = (sinV*lineTemp.x + cosV*lineTemp.y);
		m_cutLines[i].z = (cosV*lineTemp.z - sinV*lineTemp.w);
		m_cutLines[i].w = (sinV*lineTemp.z + cosV*lineTemp.w);
	}

	//-----------------------------
	//Init the pieces
	//
	//Nothing have been stored in our array yet
	m_piecesCount = 0;
}

//-----------------------------------------------------------------
// Break a piece of glass into smaller pieces
//
// Input:	impactLoc:		Where the impact trigger pieces falling
//			in_polygons:	Piece of glass to break
//			in_points:		Points referred by the pieces (referred by in_polygons)
//			in_pieceSizeTriggerCut:			If a piece is equal or bigger then this size we will try to cut the piece smaller
//			inOut_piecesToCutReamining:		This is the remaining number of slot available for new pieces created when we cut.  If this reach 0
//											it means that we cannot cut anymore.
//
void bgPolygonDicer::Cut(const Vector2& impactLoc, const atArray<bgPolygon>& in_polygons, const atArray<Vector2>& in_points, float in_pieceSizeTriggerCutSqr, int* inOut_piecesToCutReamining )
{
	//Create pieces from convex polygons
	int count = in_polygons.GetCount();
	if( count > SmallPiecesMaxPieces )
		count = SmallPiecesMaxPieces;

	for( m_piecesCount = 0; m_piecesCount < count; ++ m_piecesCount )
	{
		CutPiece &piece( m_pieces[ m_piecesCount ] );
		const bgPolygon &polygon( in_polygons[ m_piecesCount ] );

		//Copy vertices and find center
		piece.m_pointCount = polygon.GetCount();
		piece.m_center.Zero();

		//Extreme location
		Vector2 topLeft = in_points[polygon[0]];
		Vector2 bottomRight = in_points[polygon[0]];

		for(int index =  0; index < polygon.GetCount(); index++ )
		{
			piece.m_point[ index ] = in_points[ polygon[ index ] ];
			piece.m_center += piece.m_point[ index ];

			//We need to find out this piece size:  keep the extremes
			if (topLeft.x > in_points[ polygon[ index ] ].x)
				topLeft.x = in_points[ polygon[ index ] ].x;
			if (topLeft.y > in_points[ polygon[ index ] ].y)
				topLeft.y = in_points[ polygon[ index ] ].y;

			if (bottomRight.x < in_points[ polygon[ index ] ].x)
				bottomRight.x = in_points[ polygon[ index ] ].x;
			if (bottomRight.y < in_points[ polygon[ index ] ].y)
				bottomRight.y = in_points[ polygon[ index ] ].y;

		}
		//Get the piece size
		piece.m_size.x = fabs(topLeft.x - bottomRight.x);
		piece.m_size.y = fabs(topLeft.y - bottomRight.y);

		//Keep the piece center
		piece.m_center /= float( piece.m_pointCount );

		//The piece is actually active
		piece.m_use = true;

		//Determine if this piece contains the point of contact
		if( bgCracksTemplate::IsPointInPoly( impactLoc, piece.m_point, piece.m_pointCount ) )
		{
			piece.m_center = impactLoc;
			piece.m_hitbyImpact = true;
			//pointInPiece = m_piecesCount;
		}
		else
		{
			piece.m_hitbyImpact = false;
		}
	}

	//Quit if we do not have any remaining slot to add pieces into.  This is a protection in case the outside caller did not protect us from that.
	if (*inOut_piecesToCutReamining <= 0)
		return;
	//Iterate all our valid polygon in our array
	for( int i = 0; i < m_piecesCount; ++ i )
	{
		//We found a polygon to cut
		if (m_pieces[i].m_use)
		{
			//Do we want to cut this piece?  Two reason to do so:
			//	1 - We cut if this piece is too big
			//	2 - this is the piece that received the impact
			float fDiagonalSqr = m_pieces[i].m_size.x*m_pieces[i].m_size.x + m_pieces[i].m_size.y*m_pieces[i].m_size.y;
			if (fDiagonalSqr < in_pieceSizeTriggerCutSqr && !m_pieces[i].m_hitbyImpact)
				continue;

			//Get a line to cut our piece
			//
			Vector4 line;
			// This line will ensure we are cutting the piece on the piece longer side
			bgPolygonDicer::GetCuttingLine(line, m_pieces[i].m_size.x, m_pieces[i].m_size.y);
			//Move the line to the center of the piece
			line.x += m_pieces[i].m_center.x;
			line.y += m_pieces[i].m_center.y;
			line.z += m_pieces[i].m_center.x;
			line.w += m_pieces[i].m_center.y;
			
			//Cut the piece into two more
			//
			//Get two more slot into our piece array if there is room
			if (m_piecesCount > SmallPiecesMaxPieces-2)
				return;
			m_piecesCount += 2;

			//Reset the pieces
			m_pieces[m_piecesCount-2].m_use = false;
			m_pieces[m_piecesCount-2].m_pointCount = 0;
			m_pieces[m_piecesCount-2].m_hitbyImpact = false;
			m_pieces[m_piecesCount-2].m_size.x = -1;
			m_pieces[m_piecesCount-2].m_size.y = -1;
			m_pieces[m_piecesCount-1].m_use = false;
			m_pieces[m_piecesCount-1].m_pointCount = 0;
			m_pieces[m_piecesCount-1].m_hitbyImpact = false;
			m_pieces[m_piecesCount-1].m_size.x = -1;
			m_pieces[m_piecesCount-1].m_size.y = -1;

			//Do the cut
			if(CutInTwoPiece(&m_pieces[i], line, 0, &m_pieces[m_piecesCount-2], &m_pieces[m_piecesCount-1]))
			//CutInTwoPieceFast(&m_pieces[i], &m_pieces[m_piecesCount-2], &m_pieces[m_piecesCount-1]);
			{
				//If we did cut them, lets create new piece
				if (m_pieces[m_piecesCount-2].m_use)
				{
					//Center point	
					if (m_pieces[i].m_hitbyImpact)
						m_pieces[m_piecesCount-2].m_center = impactLoc;
					else
						m_pieces[m_piecesCount-2].m_center = bgSCracksTemplate::InstanceRef().ComputeCenter(m_pieces[m_piecesCount-2].m_point, m_pieces[m_piecesCount-2].m_pointCount);
				}

				//If we did cut them, lets create new piece
				if (m_pieces[m_piecesCount-1].m_use)
				{
					//Center point
					if (m_pieces[i].m_hitbyImpact)
						m_pieces[m_piecesCount-1].m_center = impactLoc;
					else
						m_pieces[m_piecesCount-1].m_center = bgSCracksTemplate::InstanceRef().ComputeCenter(m_pieces[m_piecesCount-1].m_point, m_pieces[m_piecesCount-1].m_pointCount);
				}

				//We added one more piece:  remove one from our available slot count (we added 2 pieces but we made one not use anymore = 1 more)
				(*inOut_piecesToCutReamining)--;
			}
			else
			{
				// We didn't use the extra two pieces
				m_piecesCount -= 2;
			}

			//Quit if we cannot add more pieces
			if (*inOut_piecesToCutReamining == 0)
				break;
		}
	}
}

//-----------------------------------------------------------------
// This function output a line to cut a piece in two.  The line direction
//  will be based on the piece 2D (x-y) length.  The line will be created to
//  cut the piece on the longer side in order to reduce its total length.
//	This is useful in the case where we do not want big pieces falling from
//  a pane of glass.  This will ensure that a piece of glass is always getting smaller
//  after being cut with the line returned from this function.
//
void bgPolygonDicer::GetCuttingLine(Vector4& out_line, float lengthX, float lengthY)
{
	//x,y = xy for line point 1
	//z,w = xy for line point 2
	Vector4 lineTemp;

	//Angle to rotate our line with
	float angle;

	//We want to reduce the piece size on the X axis
	if (lengthX > lengthY)
	{
		lineTemp.x = 0;
		lineTemp.y = -100;
		lineTemp.z = 0;
		lineTemp.w = 100;

		//Compute our max angle:  90 degree max.  Possible values:
		//	90 = both length are equal so we do not care what side get cut.  90 degree can cut in any direction (between -90 and 90)
		//	0 = size Y is 0 (impossible), therefore we do not rotate our line, just keep it verticale
		angle = 3.1416f*0.5f * lengthY/lengthX;
		//Now that we know how far we can rotate: Randomly chose between this max rotation and its negative
		mthRandom random;
		angle = random.GetRanged(-angle, angle);
	}
	//We want to reduce the piece size on the Y axis
	else
	{
		lineTemp.x = -100;
		lineTemp.y = 0;
		lineTemp.z = 100;
		lineTemp.w = 0;

		//Compute our max angle:  90 degree max.  Possible values:
		//	90 = both length are equal so we do not care what side get cut.  90 degree can cut in any direction (between -90 and 90)
		//	0 = size Y is 0 (impossible), therefore we do not rotate our line, just keep it verticale
		angle = 3.1416f*0.5f * lengthX/lengthY;
		//Now that we know how far we can rotate: Randomly chose between this max rotation and its negative
		mthRandom random;
		angle = random.GetRanged(-angle, angle);
	}

	//Rotate our line for randomness
	float cosV = cos(angle);
	float sinV = sin(angle);
	out_line.x = (cosV*lineTemp.x - sinV*lineTemp.y);
	out_line.y = (sinV*lineTemp.x + cosV*lineTemp.y);
	out_line.z = (cosV*lineTemp.z - sinV*lineTemp.w);
	out_line.w = (sinV*lineTemp.z + cosV*lineTemp.w);
}

//-----------------------------------------------------------------
// This function output a line to cut a piece in two.  The line direction
// is chosen randomly.
void bgPolygonDicer::GetCuttingLine(Vector4& out_line)
{
	//Random line only
	out_line.x = m_cutLines[m_cutLineIndex].x; 
	out_line.y = m_cutLines[m_cutLineIndex].y;
	out_line.z = m_cutLines[m_cutLineIndex].z;
	out_line.w = m_cutLines[m_cutLineIndex].w;
	m_cutLineIndex++;
	if (m_cutLineIndex == LINE_CUT_COUNT)
		m_cutLineIndex = 0;
}

//-----------------------------------------------------------------
// Take a piece and cut it in two pieces
//
// Input:	in_piece:					The vertices of the piece to cut in two.  
//			in_pieceVertexCount			Number of vertices in the the piece
//			in_line:					Line cutting the piece in two.
//										in_line.xy = point1
//										in_line.xy = point2
//										The two point have to be outside of the polygon and must collide twide with the polygon to work.
//			in_lineGranularity:			How many points do you want the line to be created with:  The line start straight (2 points) but with 
//										more point less straight the line will be and therefore the cut should look better.  By default
//										the value is 0, meaning we do not want to cut the line in multiple smaller line.
//			out_pieceOne				One half of the original piece
//			out_pieceTwo				Other half of the original piece
//
// Output:  true if at the pieces where created, otherwise false.
//
bool bgPolygonDicer::CutInTwoPiece(	CutPiece* in_piece, Vector4& in_line, int /*in_lineGranularity*/, 
									CutPiece* out_pieceOne, CutPiece* out_pieceTwo)
{
	//---------------------------------
	//Find at least two points of intersection between the line and the polygon:  This is nescessary to cut the piece.
	//

	//These two point are the two intersection create by the input line and the polygon
	class intersectionClass
	{
	public:
		//Point colliding with the line and the polygon
		Vector2			m_point;
		//Index of the first polygon point of the segment colliding with the input line
		int				m_PointPolygonIndex;
	};
	intersectionClass	intersectionData[50];
	int intersectionCount = 0;

	//Find the two point first point of intersection:  A conc ave polugon might have more then 2 but we dont care as long as we have two
	for (int i = 0;i < in_piece->m_pointCount;i++)
	{
		//Next point index
		int nextIndex = i + 1;
		if (nextIndex == in_piece->m_pointCount)
			nextIndex = 0;

		//Do the line collide with the current polygon segment
		Vector2 intersection;
		if (LinesIntersectFloat(in_piece->m_point[i].x, in_piece->m_point[i].y, in_piece->m_point[nextIndex].x, in_piece->m_point[nextIndex].y, 
			in_line.x, in_line.y, in_line.z, in_line.w, &intersection.x, &intersection.y))
		{
			//We collide:  Keep some info for later
			intersectionData[intersectionCount].m_point = intersection;
			intersectionData[intersectionCount].m_PointPolygonIndex = i;
			intersectionCount++;

			//This was the second point we can stop now
			if (intersectionCount == 2)
				break;
		}
	}

	//If we do not have two points, get out
	if (intersectionCount < 2)
		return false;

	// Handle case where the two intersection points are on an edge
	// If we allow this the triangle is going to have zero area
	// Note: This may not cover all the possible cases - keep an eye on the assert in bgBreakable::CreateOneFallingPiece
	const float fMinOverlapDist = 0.000001f;
	for (int i = 0; i < in_piece->m_pointCount; i++)
	{
		if(in_piece->m_point[i].Dist2(intersectionData[0].m_point) < fMinOverlapDist)
		{
			if(in_piece->m_point[(in_piece->m_pointCount + i-1) % in_piece->m_pointCount].Dist2(intersectionData[1].m_point) < fMinOverlapDist ||
				in_piece->m_point[(i+1) % in_piece->m_pointCount].Dist2(intersectionData[1].m_point) < fMinOverlapDist)
			{
				return false;
			}
		}
	}

	//We have been cut so the parent is not active anymore
	in_piece->m_use = false;

	//--------------------------------
	//Create the first polygon from this piece
	//

	CreateHalfPiece(	in_piece, out_pieceOne, 
		intersectionData[0].m_point, intersectionData[0].m_PointPolygonIndex, 
		intersectionData[1].m_point, intersectionData[1].m_PointPolygonIndex);

	//--------------------------------
	//Create the second polygon from this piece
	//

	CreateHalfPiece(	in_piece, out_pieceTwo, 
		intersectionData[1].m_point, intersectionData[1].m_PointPolygonIndex, 
		intersectionData[0].m_point, intersectionData[0].m_PointPolygonIndex);

#if __BANK
	float area1 = out_pieceOne->m_point[out_pieceOne->m_pointCount-1].Cross(out_pieceOne->m_point[0]);
	for (int iEdge = 1; iEdge < out_pieceOne->m_pointCount;iEdge++)
		area1 += out_pieceOne->m_point[iEdge-1].Cross(out_pieceOne->m_point[iEdge]);
	area1 *= 0.5f;
	if(area1 <= 0.0f)
		bgDebugf1("CreateHalfPiece 1 created a glass piece cant be zero! (%f)", area1);
	float area2 = out_pieceTwo->m_point[out_pieceTwo->m_pointCount-1].Cross(out_pieceTwo->m_point[0]);
	for (int iEdge = 1; iEdge < out_pieceTwo->m_pointCount;iEdge++)
		area2 += out_pieceTwo->m_point[iEdge-1].Cross(out_pieceTwo->m_point[iEdge]);
	area2 *= 0.5f;
	if(area2 <= 0.0f)
		bgDebugf1("CreateHalfPiece 2 created a glass piece cant be zero! (%f)", area2);
	if(area1 <= 0.0f || area2 <= 0.0f)
	{
		bgDebugf1("Total points parent %d", in_piece->m_pointCount);
		for (int iEdge = 0; iEdge < in_piece->m_pointCount; iEdge++)
		{
			bgDebugf1("Point %d: <%f, %f>", iEdge, in_piece->m_point[iEdge].x, in_piece->m_point[iEdge].y);
		}
		bgDebugf1("Total points 1: %d", out_pieceOne->m_pointCount);
		for (int iEdge = 0; iEdge < out_pieceOne->m_pointCount; iEdge++)
		{
			bgDebugf1("Point %d: <%f, %f>", iEdge, out_pieceOne->m_point[iEdge].x, out_pieceOne->m_point[iEdge].y);
		}
		bgDebugf1("Total points 2: %d", out_pieceTwo->m_pointCount);
		for (int iEdge = 0; iEdge < out_pieceTwo->m_pointCount; iEdge++)
		{
			bgDebugf1("Point %d: <%f, %f>", iEdge, out_pieceTwo->m_point[iEdge].x, out_pieceTwo->m_point[iEdge].y);
		}
		bgDebugf1("Total intersection points: %d", intersectionCount);
		for (int i = 0; i < intersectionCount; i++)
		{
			bgDebugf1("Point %d: <%f, %f>", i, intersectionData[i].m_point.x, intersectionData[i].m_point.y);
		}
	}
#endif // __BANK

	return true;
}

//-----------------------------------------------------------------
// Take a piece and cut it in two pieces
//
// Input:	in_piece:					The vertices of the piece to cut in two.  
//			out_pieceOne				One half of the original piece
//			out_pieceTwo				Other half of the original piece
//
// Output:  Return early if the line does not touch the polygon or only one point is touching.
//
void bgPolygonDicer::CutInTwoPieceFast(	CutPiece* in_piece, CutPiece* out_pieceOne, CutPiece* out_pieceTwo)
{
	//---------------------------------
	//Find at least two points of intersection between the line and the polygon:  This is nescessary to cut the piece.
	//

	//These two point are the two intersection create by the input line and the polygon
	class intersectionClass
	{
	public:
		//Point colliding with the line and the polygon
		Vector2			m_point;
		//Index of the first polygon point of the segment colliding with the input line
		int				m_PointPolygonIndex;
	};
	intersectionClass	intersectionData[50];
//	int intersectionCount = 0;
/*
	//Find the two point first point of intersection:  A conc ave polugon might have more then 2 but we dont care as long as we have two
	for (int i = 0;i < in_piece->m_pointCount;i++)
	{
		//Next point index
		int nextIndex = i + 1;
		if (nextIndex == in_piece->m_pointCount)
			nextIndex = 0;

		//Do the line collide with the current polygon segment
		Vector2 intersection;
		if (LinesIntersectFloat(in_piece->m_point[i].x, in_piece->m_point[i].y, in_piece->m_point[nextIndex].x, in_piece->m_point[nextIndex].y, 
			in_line.x, in_line.y, in_line.z, in_line.w, &intersection.x, &intersection.y))
		{
			//We collide:  Keep some info for later
			intersectionData[intersectionCount].m_point = intersection;
			intersectionData[intersectionCount].m_PointPolygonIndex = i;
			intersectionCount++;

			//This was the second point we can stop now
			if (intersectionCount == 2)
				break;
		}
	}
*/

	
	if (in_piece->m_pointCount < 4)
		return;
	
	intersectionData[0].m_PointPolygonIndex = 0;
	intersectionData[0].m_point = in_piece->m_point[0] + ((in_piece->m_point[1] - in_piece->m_point[0]) * 0.5f);
		
	
	intersectionData[1].m_PointPolygonIndex = in_piece->m_pointCount / 2;
	intersectionData[1].m_point = in_piece->m_point[intersectionData[1].m_PointPolygonIndex] + ((in_piece->m_point[intersectionData[1].m_PointPolygonIndex+1] - in_piece->m_point[intersectionData[1].m_PointPolygonIndex]) * 0.5f);


	//If we do not have two points, get out
//	if (intersectionCount < 2)
//		return;


	//We have been cut so the parent is not active anymore
	in_piece->m_use = false;

	//--------------------------------
	//Create the first polygon from this piece
	//

	CreateHalfPiece(	in_piece, out_pieceOne, 
		intersectionData[0].m_point, intersectionData[0].m_PointPolygonIndex, 
		intersectionData[1].m_point, intersectionData[1].m_PointPolygonIndex);

	//--------------------------------
	//Create the second polygon from this piece
	//

	CreateHalfPiece(	in_piece, out_pieceTwo, 
		intersectionData[1].m_point, intersectionData[1].m_PointPolygonIndex, 
		intersectionData[0].m_point, intersectionData[0].m_PointPolygonIndex);
}

//-----------------------------------------------------------------
// Create a piece of glass that will be half of a full piece of glass.
// The original piece (in_silhouette) have been cut in two with a line (in_intersection1 &
// in_intersection2).  The output (out_silhouette) will be one side of the line, from 
// in_intersection1 to in_intersection2, following the segments.
// 
// Input:	in_silhouette		Original polygon being cut in two
//			out_silhouette		Half the original polygon starting from in_intersection1 to in_intersection2
//			in_intersection1	Intersection point between original polygon and line dividing the polygon in 2.
//								This is also the first point of the output polygon
//			in_segmentIndex1	Index segment (first point of the segment) of the original polygon the line intersect first
//			in_intersection2	Intersection point between original polygon and line dividing the polygon in 2.
//								This is also the last point of the output polygon
//			in_segmentIndex2	Index segment (first point of the segment) of the original polygon the line intersect last
//
void bgPolygonDicer::CreateHalfPiece(	CutPiece* in_silhouette, CutPiece* out_silhouette, 
								Vector2& in_intersection1, int in_segmentIndex1, 
								Vector2& in_intersection2, int in_segmentIndex2)
{
	//This silhouette is now active
	out_silhouette->m_use = true;

	//Create the points
	//
	//Add the first intersection as the first point
	out_silhouette->m_point[out_silhouette->m_pointCount] = in_intersection1;
	out_silhouette->m_pointCount++;

	//We need to find the size of this piece for later use in knowing if this piece should also be cut into 2 smaller pieces
	Vector2 topLeft = in_intersection1;
	Vector2 bottomRight = in_intersection1;

	//Add the remaining points:  We need to iterate throw all the points of the polygon 
	// with the first index being the first intersection following point
	int currentIndex = in_segmentIndex1+1;
	for (int i = 0;i < in_silhouette->m_pointCount;i++)
	{
		//Our index is out of bound, restart
		if (currentIndex >= in_silhouette->m_pointCount)
			currentIndex = 0;

		//Add the current point to the new polygon
		out_silhouette->m_point[out_silhouette->m_pointCount] = in_silhouette->m_point[currentIndex];
		out_silhouette->m_pointCount++;

		//Did this new point is an extreme
		if (topLeft.x > in_silhouette->m_point[currentIndex].x)
			topLeft.x = in_silhouette->m_point[currentIndex].x;
		if (topLeft.y > in_silhouette->m_point[currentIndex].y)
			topLeft.y = in_silhouette->m_point[currentIndex].y;
		if (bottomRight.x < in_silhouette->m_point[currentIndex].x)
			bottomRight.x = in_silhouette->m_point[currentIndex].x;
		if (bottomRight.y < in_silhouette->m_point[currentIndex].y)
			bottomRight.y = in_silhouette->m_point[currentIndex].y;

		//If the current index is equal to the second intersection segment, we are done
		if (currentIndex == in_segmentIndex2)
		{
			//Add the intersection for our final point
			out_silhouette->m_point[out_silhouette->m_pointCount] = in_intersection2;
			out_silhouette->m_pointCount++;
			break;
		}

		//Go to the next index
		currentIndex++;
	}	

	//Get the piece size
	out_silhouette->m_size.x = fabs(topLeft.x - bottomRight.x);
	out_silhouette->m_size.y = fabs(topLeft.y - bottomRight.y);
}



int bgPolygonDicer::LinesIntersectFloat (float ax, float ay, float bx, float by, float cx, float cy, float dx, float dy, float *px, float *py)
{
/*	if (ax < cx && ax < dx &&
		bx < cx && bx < dx)
		return DONT_INTERSECT2;
	if (ax > cx && ax > dx &&
		bx > cx && bx > dx)
		return DONT_INTERSECT2;
	if (ay < cy && ay < dy &&
		by < cy && by < dy)
		return DONT_INTERSECT2;
	if (ay > cy && ay > dy &&
		by > cy && by > dy)
		return DONT_INTERSECT2;
*/

	float  divider, num, r, s;

	divider = (float)(((bx - ax) * (dy - cy)) - ((by - ay) * (dx - cx)));

	num = (float)(((ay - cy) * (dx - cx)) - ((ax - cx) * (dy - cy)));

	if (divider == 0.f)
	{
		if (num == 0.f)
		{
			return (COLLINEAR2);
		}
		else
			return (DONT_INTERSECT2);
	}

	r = num / divider;

	if ((r < 0.f) || (r > 1.f))
		return (DONT_INTERSECT2);

	s = (((ay - cy) * (bx - ax)) - ((ax - cx) * (by - ay))) / divider;

	if ((s < 0.f) || (s > 1.f))
		return (DONT_INTERSECT2);

	// At this point, we know there's an intersection between the lines.  Find the point where they do
	*px = (ax + (r * (bx - ax)));
	*py = (ay + (r * (by - ay)));

	return (DO_INTERSECT2);
}

} // namespace rage
