// 
// breakableglass/bgarray.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BREAKABLEGLASS_BGARRAY_H 
#define BREAKABLEGLASS_BGARRAY_H 

#include "atl/array.h"

namespace rage {

#if __SPU
#define bgArray atArray
#else
	// align allocations to element align to save memory
	template <class _Type>
	class bgArray : public atArray<_Type, __alignof(_Type)>
	{
	public:
		// align is only relevant for allocations, so it's safe to treat 
		// const references as having the default (or any other) align
		operator const atArray<_Type>& () const
		{
			return *((const atArray<_Type>*)this);
		}
	};
#endif

} // namespace rage

#endif // BREAKABLEGLASS_BGARRAY_H 
