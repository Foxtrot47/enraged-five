// 
// audmusic/music.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "music.h"
#include "songinfo.h"
#include "system/new.h"
#include "system/memops.h"

namespace rage
{

audMusic::audMusic()
{
	m_ChangeTrackFunc = NULL;
	m_FindUserMusicDoneFunc = NULL;
	m_pUserSongInfo = rage_new audUserSongInfo;
	sysMemZeroBytes<sizeof(audUserSongInfo)>(m_pUserSongInfo);
}

audMusic::~audMusic()
{
	delete m_pUserSongInfo;
}

void audMusic::SetChangeTrackFunc(const Functor0 *pFunc)
{
	m_ChangeTrackFunc = *pFunc;
}

void audMusic::SetFindUserMusicFunc(const Functor0 *pFunc)
{
	m_FindUserMusicDoneFunc = *pFunc;
}


}	// namespace

