// 
// audmusic/songinfo.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDMUSIC_SONGINFO_H 
#define AUDMUSIC_SONGINFO_H 

#include "system/xtl.h"

#if __XENON
#include <xmp.h>
#endif

namespace rage
{
	struct audUserPlaylistInfo 
	{
#if __XENON
		XMP_USERPLAYLISTINFO	UserPlaylist;
#else
		int						unused;
#endif
	};

	struct audUserSongInfo
	{
#if __XENON
		XMP_SONGINFO			SongInfo;
#else
		int						unused;
#endif
	};
} // namespace rage

#endif // AUDMUSIC_SONGINFO_H 
