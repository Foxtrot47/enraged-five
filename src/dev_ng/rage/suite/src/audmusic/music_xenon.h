// 
// audmusic/music_xenon.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#ifndef AUDMUSIC_MUSICXENON_H
#define AUDMUSIC_MUSICXENON_H

#if __XENON

#include "music.h"
#include "system/xtl.h"
#include <xmp.h>

namespace rage
{
	struct audUserSongInfo;

	class audMusicXenon : public audMusic
	{
	public:
		audMusicXenon();
		virtual ~audMusicXenon();

		virtual void Update();

		virtual void Play(audUserPlaylistInfo *pSongInfo);
		virtual void Pause();
		virtual void Continue();
		virtual void Stop();
		virtual void Next();
		virtual void Previous();

		virtual const audUserSongInfo &GetCurrentSongInfo() const;
		virtual int GetState() const { return m_State; }
		virtual int GetPlayState() const { return m_PlayState; }

		virtual void SetVolume(const float volume);

		virtual void FindUserMusic(const int startIndex, const int total, audUserPlaylistInfo& userInfo, int& numSongInfo);

	protected:
		void CloseUserHandle();
		void SetState(const int state);
		void QueryXMPlayState();
		void BeginFindUserMusic();

		void UpdateXMP();
		void UpdateInternal();

		int						m_State;
		int						m_PlayState;
		int						m_SongsLeftToGrab;
		int						m_SongTotal;
		int						m_StartIndex;
		int						m_IteratorIndex;
		int						m_SongDuration;

		int *					m_pNumSongInfo;
		audUserPlaylistInfo *	m_pUserPlaylistInfo;

		XOVERLAPPED				m_OverlappedResult;
 		XMP_USERPLAYLISTINFO *	m_pEnumUserPlaylist;
		HANDLE					m_UserHandle;

		HANDLE					m_Notification;
		XMP_STATE				m_XMPState;
	};
}

#endif // __XENON
#endif // AUDMUSIC_MUSICXENON_H

