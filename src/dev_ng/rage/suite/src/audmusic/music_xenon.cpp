// 
// audmusic/music_xenon.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if __XENON

#include "music_xenon.h"
#include "songinfo.h"
#include "system/new.h"

using namespace rage;

#define NUM_SONGS_PER_QUERY		(16)

audMusicXenon::audMusicXenon()
{
	m_pEnumUserPlaylist = rage_new XMP_USERPLAYLISTINFO[NUM_SONGS_PER_QUERY];
	m_UserHandle = 0;
	m_SongsLeftToGrab = 0;

	SetState(kStateNone);

	m_Notification = XNotifyCreateListener(XNOTIFY_XMP);
	Assert(m_Notification);

	QueryXMPlayState();
}

audMusicXenon::~audMusicXenon()
{
	delete [] m_pEnumUserPlaylist;
	m_pEnumUserPlaylist = NULL;
	CloseUserHandle();

	DWORD dwStatus;
	XMPStop(NULL);

	dwStatus = XMPGetStatus(&m_XMPState);
	Assert(dwStatus == ERROR_SUCCESS);

	while (m_XMPState != XMP_STATE_IDLE)
	{
		Sleep(5);
		dwStatus = XMPGetStatus(&m_XMPState);
		Assert(dwStatus == ERROR_SUCCESS);
	}
}

void audMusicXenon::CloseUserHandle()
{
	if (m_UserHandle)
	{
		CloseHandle(m_UserHandle);
		m_UserHandle = 0;
	}
}

void audMusicXenon::SetState(const int state)
{
	Assert(state >= kStateNone && state <= kStateFindUserMusic);
	m_State = state;
}

void audMusicXenon::QueryXMPlayState()
{
	AssertVerify(ERROR_SUCCESS == XMPGetStatus(&m_XMPState));

	switch (m_XMPState)
	{
	case XMP_STATE_IDLE:
		m_PlayState = kIdle;
		break;
	case XMP_STATE_PLAYING:
		if (GetState() == kStateBusy)
			SetState(kStateNone);

		m_PlayState = kPlay;
		break;
	case XMP_STATE_PAUSED:
		if (GetState() == kStateBusy)
			SetState(kStateNone);

		m_PlayState = kPaused;
		break;
	}
}

void audMusicXenon::FindUserMusic(const int startIndex, const int total, audUserPlaylistInfo& userInfo, int& numSongInfo)
{
	Assert(!m_UserHandle);		// can't kick off a search while something else is happening
	Assert(total > 0);			// total less than 1
	Assert(startIndex >= 0);	// start index must be greater than equal to 0

	// set initial params
	m_SongsLeftToGrab = m_SongTotal = total;
	m_StartIndex = startIndex;
	m_IteratorIndex = 0;
	m_pUserPlaylistInfo = &userInfo;
	m_pNumSongInfo = &numSongInfo;
	*m_pNumSongInfo = 0;

	// create the enumerator
	DWORD cbBufferSize;
	XMPCreateUserPlaylistEnumerator(NULL, NUM_SONGS_PER_QUERY, &cbBufferSize, &m_UserHandle);

	// start the search
	BeginFindUserMusic();
}

void audMusicXenon::BeginFindUserMusic()
{
	ZeroMemory(&m_OverlappedResult, sizeof(XOVERLAPPED));
	XEnumerate(m_UserHandle, m_pEnumUserPlaylist, NUM_SONGS_PER_QUERY * sizeof(XMP_USERPLAYLISTINFO), 0, &m_OverlappedResult);
	SetState(kStateFindUserMusic);
}

void audMusicXenon::Play(audUserPlaylistInfo *pSongInfo)
{
	SetState(kStateBusy);
	AssertVerify(ERROR_SUCCESS==XMPPlayUserPlaylist(&pSongInfo->UserPlaylist, NULL));
}

void audMusicXenon::Pause()
{
	SetState(kStateBusy);
	AssertVerify(ERROR_SUCCESS==XMPPause(NULL));
}

void audMusicXenon::Continue()
{
	AssertVerify(ERROR_SUCCESS==XMPContinue(NULL));
}

void audMusicXenon::Stop()
{
	AssertVerify(ERROR_SUCCESS==XMPStop(NULL));
}

void audMusicXenon::Next()
{
	AssertVerify(ERROR_SUCCESS==XMPNext(NULL));
}

void audMusicXenon::Previous()
{
	AssertVerify(ERROR_SUCCESS==XMPPrevious(NULL));
}

void audMusicXenon::SetVolume(const float volume)
{
	// valid range is 0->1
	Assert(volume>=0.0f && volume<=1.0f);
	AssertVerify(ERROR_SUCCESS==XMPSetVolume(volume,NULL));
}

const audUserSongInfo & audMusicXenon::GetCurrentSongInfo() const
{
	return *m_pUserSongInfo;
}

void audMusicXenon::Update()
{
	UpdateXMP();

	UpdateInternal();
}

void audMusicXenon::UpdateXMP()
{
	DWORD dwMsgFilter;
	ULONG param;

	if (XNotifyGetNext(m_Notification, 0, &dwMsgFilter, &param))
	{
		switch (dwMsgFilter)
		{
		case XN_XMP_STATECHANGED:
			{
				QueryXMPlayState();
					
				// we're playing something
				if (m_XMPState != XMP_STATE_IDLE)
				{
					// cache off song info?
					AssertVerify(ERROR_SUCCESS==XMPGetCurrentSong(&m_pUserSongInfo->SongInfo, NULL));
					m_SongDuration = (int)m_pUserSongInfo->SongInfo.dwDuration;

					// if set, call back any function hook
					if (m_ChangeTrackFunc)
						m_ChangeTrackFunc();
				}
			}
			break;

		}
	}
}

void audMusicXenon::UpdateInternal()
{
	switch (GetState())
	{
		case kStateNone:
		{
			return;
			break;
		}

		case kStateFindUserMusic:
		{
			DWORD cbNumResults;
			if(XGetOverlappedResult(&m_OverlappedResult, &cbNumResults, FALSE) == ERROR_SUCCESS)
			{
				bool shouldFindMore = false;
				if (cbNumResults)
				{
					const int NumResults = (int)cbNumResults;

					shouldFindMore = true;

					// found the start of the client data
					if (m_IteratorIndex + NumResults > m_StartIndex)
					{
						// index falls on the start of a window boundary, set index to start of returned array
						if (m_IteratorIndex == m_StartIndex)
							m_StartIndex = 0;

						if (m_SongsLeftToGrab > 0)
						{
							m_SongsLeftToGrab -= (NumResults - m_StartIndex);
							shouldFindMore = (m_SongsLeftToGrab > 0) ? true : false;
						}

						// Copy into our client's array
						for (int i=m_StartIndex; i<NumResults && ((*m_pNumSongInfo) < m_SongTotal); ++i)
						{
							m_pUserPlaylistInfo[(*m_pNumSongInfo)].UserPlaylist = m_pEnumUserPlaylist[i];
							++(*m_pNumSongInfo);
						}

						// the next copy, if needed, will start at the beginning of the returned array
						m_StartIndex = 0;
					}

					// increment iterator index by results
					m_IteratorIndex += NumResults;
				}

				if (shouldFindMore)
				{
					BeginFindUserMusic();
				}
				else
				{
					if (m_FindUserMusicDoneFunc)
						m_FindUserMusicDoneFunc();

					SetState(kStateNone);
					CloseUserHandle();
				}
			}
		}
		break;
	}
}

#endif
