// 
// audmusic/music.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef AUDMUSIC_MUSIC_H
#define AUDMUSIC_MUSIC_H

#ifndef ATL_FUNCTOR_H
#include "atl/functor.h"
#endif

namespace rage
{
	struct audUserPlaylistInfo;
	struct audUserSongInfo;

	//////////////////////////////////////////////////////////////////////////

	class audMusic
	{
	public:
		audMusic();
		virtual ~audMusic();

		//////////////////////////////////////////////////////////////////////////
		// PURPOSE:
		//   Allows the music player to update and process state changes
		virtual void Update() = 0;

		//////////////////////////////////////////////////////////////////////////
		// PURPOSE:
		//   Set volume level for the underlying user music engine
		virtual void SetVolume(const float volume) = 0;

		//////////////////////////////////////////////////////////////////////////
		// PURPOSE
		//   Set a callback function for when a track changes
		virtual void SetChangeTrackFunc(const Functor0 *pFunc);

		//////////////////////////////////////////////////////////////////////////
		// PURPOSE
		virtual void SetFindUserMusicFunc(const Functor0 *pFunc);

		//////////////////////////////////////////////////////////////////////////
		// PURPOSE:
		//   Provide basic playback control functions to the underlying user 
		//   music engine.
		virtual void Play(audUserPlaylistInfo *pSongInfo) = 0;
		virtual void Pause() = 0;
		virtual void Continue() = 0;
		virtual void Stop() = 0;
		virtual void Next() = 0;
		virtual void Previous() = 0;

		//////////////////////////////////////////////////////////////////////////
		// PURPOSE:
		//   Returns platform specific song data for the currently playing track.
		virtual const audUserSongInfo &GetCurrentSongInfo() const = 0;

		//////////////////////////////////////////////////////////////////////////
		// PURPOSE:
		//   Returns current state of music player, listed above.
		// NOTES:
		//   State can change when the caller issues a FindUserMusic() call, or
		//   any Play/Stop/Next/Previous operation.  The underlying music engine
		//   operates asynchronously and may incur a delay in processing a request.
		// SEE ALSO:
		//   GetPlayState() 
		enum { kStateNone, kStateBusy, kStateFindUserMusic };
		virtual int GetState() const = 0;

		//////////////////////////////////////////////////////////////////////////
		// PURPOSE:
		//   Returns the current state of the underlying user music engine
		enum { kPlay, kIdle, kPaused };
		virtual int GetPlayState() const = 0;

		//////////////////////////////////////////////////////////////////////////
		// PURPOSE:
		//   Enumerates the user music tracks in order and stores the results into 
		//   the paSongInfo array.
		// PARAMS:
		//   startIndex	- index of first user music playlist to return
		//   total		- total number of user music playlists to return
		//   songInfo   - [out] array for user playlist results.  should be at least 'total' elements in size.
		//   numSongInfo- [out] number of elements copied into the 'songInfo' array.
		// NOTES:
		//   This call operates asynchronously.  A callback can be issued when complete, set SetFindUserMusicFunc().
		//   Also, state is set to kStateFindUserMusic during this operation, kStateNone when complete.
		virtual void FindUserMusic(const int startIndex, const int total, audUserPlaylistInfo& userInfo, int& numSongInfo) = 0;

	protected:
		Functor0				m_ChangeTrackFunc;
		Functor0				m_FindUserMusicDoneFunc;
		audUserSongInfo *		m_pUserSongInfo;
	};


};

#endif

