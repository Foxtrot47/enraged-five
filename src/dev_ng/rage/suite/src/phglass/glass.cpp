// 
// phglass/glass.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "glass.h"

#include "breakableglass/breakable.h"
#include "breakableglass/glassmanager.h"
#include "breakableglass/optimisations.h"
#include "fragment/manager.h"
#include "diag/tracker.h"
#include "phbound/support.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundbvh.h"
#include "phbound/boundbox.h"


// optimisations
BG_OPTIMISATIONS()

namespace rage {

phGlass::phGlass()
	: m_GlassHandle(bgGlassHandle_Invalid)
	, m_Bound(NULL)
	, m_Material(phMaterialMgr::DEFAULT_MATERIAL_ID)
{
}

phGlass::~phGlass()
{
	Shutdown();
}

phBound* phGlass::Init(bgGlassHandle glassHandle, phMaterialMgr::Id material)
{
	SetMaterial(material);
	m_GlassHandle = glassHandle;
	return RegenerateBound();
}

void phGlass::Shutdown()
{
	m_Material = phMaterialMgr::DEFAULT_MATERIAL_ID;
	m_GlassHandle = bgGlassHandle_Invalid;

	if (m_Bound)
	{
		// ensure bounds are deleted regardless of whether refcounting is enabled
		for(int boundIndex = 0; boundIndex < m_Bound->GetNumBounds(); ++boundIndex)
		{
			phBound* tempBound = m_Bound->GetBound(boundIndex);
			if(tempBound)
			{
				m_Bound->SetBound(boundIndex, NULL, false);
				tempBound->Release(false);
				delete tempBound;
			}
		}
		m_Bound->Release(false);
		delete m_Bound;
		m_Bound = NULL;
	}
}

void phGlass::Cleanup()
{
}

// deep copy
phGlass& phGlass::operator =(const phGlass& rhs)
{
	m_GlassHandle = rhs.m_GlassHandle;

	m_Bound = rage_aligned_new(16) phBoundComposite;

	phBoundBox* tempBoundBox = rage_aligned_new(16) phBoundBox;

#if BREAKABLE_GLASS_USE_BVH
	phBoundBVH* tempBoundBVH = NULL;
	if (rhs.GetBoundBVH())
	{
		tempBoundBVH = rage_aligned_new(16) phBoundBVH;
	}

	m_Bound->Init(BoundIndex_Count, false);

	m_Bound->Copy(rhs.m_Bound);
	if (tempBoundBVH)
	{
		tempBoundBVH->Copy(rhs.GetBoundBVH());
	}
	tempBoundBox->Copy(rhs.GetBoundBox());

	m_Bound->SetBound(BoundIndex_BVH, tempBoundBVH);
	m_Bound->SetBound(BoundIndex_Box, tempBoundBox);
#else
	m_Bound->Init(1, false);

	m_Bound->Copy(rhs.m_Bound);

	tempBoundBox->Copy(rhs.GetBoundBox());

	m_Bound->SetBound(0, tempBoundBox);
#endif // BREAKABLE_GLASS_USE_BVH

	m_Material = rhs.m_Material;
	return *this;
}

bgGlassHandle phGlass::GetGlassHandle() const
{
	return m_GlassHandle;
}

#if BREAKABLE_GLASS_USE_BVH
bool phGlass::GetIsElementActive(int element) const
{
	(void)element;
	return true;
}
#else
bool phGlass::GetIsHitActive(const Vector3& vHitPos) const
{
	//sysTimer t;

	bgBreakable& breakable = bgGlassManager::GetGlassBreakable(m_GlassHandle);

	// Create a transformaion from world space to the glass 2D space
	const Matrix34& matTrans = breakable.GetTransform();
	Vec3V vHitPosGlassSpace = UnTransformOrtho(RCC_MAT34V(matTrans), RCC_VEC3V(vHitPos));
	Vec2V vHitPos2D = breakable.Convert3DPointTo2DPoint(vHitPosGlassSpace);
	//Displayf("Convert3DPointTo2DPoint %f, %f", vHitPos2D.GetXf(), vHitPos2D.GetYf());

	// Go over all the active pieces
	const bgGeometryData& breakableData = breakable.GetGeometryData();
	const atArray<Vector2>& arrPanePoints = breakableData.GetPanePoints();
	const atArray<bgEdge>& arrPaneEdges = breakableData.GetPaneEdges();
	const atArray<bgPanePiece>& appPanePieces = breakableData.GetPanePieces();
	for(int iCurPiece = 0; iCurPiece < appPanePieces.GetCount(); iCurPiece++)
	{
		const bgPanePiece& piece = appPanePieces[iCurPiece];
		if(piece.m_flags & bgPanePiece::kPF_Active)
		{
			/*for(int iCurPoly = 0; iCurPoly < piece.m_polygons.GetCount(); iCurPoly++)
			{
				bHit = false;
				const bgPolygon& poly = piece.m_polygons[iCurPoly];
				for(int iCurVert = 0, iPrevVert = poly.GetCount() - 1; iCurVert < poly.GetCount(); iPrevVert = iCurVert++)
				{
					// Taken from the net:
					// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
					const Vector2& prevVec = arrPanePoints[poly[iPrevVert]];
					const Vector2& curVec = arrPanePoints[poly[iCurVert]];
					Vector2 delta = prevVec - curVec;
					if ( ((curVec.y > vHitPos2D.GetYf()) != (prevVec.y > vHitPos2D.GetYf())) &&
						(vHitPos2D.GetXf() < delta.x * (vHitPos2D.GetYf() - curVec.y) / delta.y + curVec.x) )
					{
						bHit = !bHit;
					}
				}

				// See if we found a hit
				if(bHit)
				{
					//Displayf("phGlass::GetIsHitActive total time %.4fms", t.GetMsTime());
					return true;
				}
			}*/

			// Try to use the poly when possible because the walk on the silhouette is not memory friendly
			bool bHit = false;
			if(piece.m_polygons.GetCount() == 0)
			{
				const bgPolygon& poly = piece.m_polygons[0];
				for(int iCurVert = 0, iPrevVert = poly.GetCount() - 1; iCurVert < poly.GetCount(); iPrevVert = iCurVert++)
				{
					// Taken from the net:
					// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
					const Vector2& prevVec = arrPanePoints[poly[iPrevVert]];
					const Vector2& curVec = arrPanePoints[poly[iCurVert]];
					Vector2 delta = prevVec - curVec;
					if ( ((curVec.y > vHitPos2D.GetYf()) != (prevVec.y > vHitPos2D.GetYf())) &&
						(vHitPos2D.GetXf() < delta.x * (vHitPos2D.GetYf() - curVec.y) / delta.y + curVec.x) )
					{
						bHit = !bHit;
					}
				}
			}
			else
			{
				const bgArray<bgEdgeRef>& arrSilhouette = piece.m_silhouette;
				for(int iCurEdge = 0; iCurEdge < arrSilhouette.GetCount(); iCurEdge++)
				{
					// Taken from the net:
					// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
					const bgEdgeRef edge = arrSilhouette[iCurEdge];
					const Vector2& prevVec = arrPanePoints[edge.GetP0(arrPaneEdges)];
					const Vector2& curVec = arrPanePoints[edge.GetP1(arrPaneEdges)];
					Vector2 delta = prevVec - curVec;
					if ( ((curVec.y > vHitPos2D.GetYf()) != (prevVec.y > vHitPos2D.GetYf())) &&
						(vHitPos2D.GetXf() < delta.x * (vHitPos2D.GetYf() - curVec.y) / delta.y + curVec.x) )
					{
						bHit = !bHit;
					}
				}
			}

			// See if we found a hit
			if(bHit)
			{
				//Displayf("phGlass::GetIsHitActive total time %.4fms", t.GetMsTime());
				return true;
			}
		}
	}

	//Displayf("phGlass::GetIsHitActive total time %.4fms", t.GetMsTime());

	return false;
}
#endif // BREAKABLE_GLASS_USE_BVH

void phGlass::SetMaterial(phMaterialMgr::Id material)
{
	m_Material = material;

#if BREAKABLE_GLASS_USE_BVH
	phBoundBVH* tempBoundBVH = GetBoundBVH();
	if (tempBoundBVH)
	{
		tempBoundBVH->SetMaterialId(0, material);
	}
#endif // BREAKABLE_GLASS_USE_BVH
}

phBound* phGlass::RegenerateBound()
{
	if (m_Bound)
	{
		for(int boundIndex = 0; boundIndex < m_Bound->GetNumBounds(); ++boundIndex)
		{
			if(m_Bound)
			{
				m_Bound->GetBound(boundIndex)->Release();
			}
		}

		m_Bound->Release();
		m_Bound = NULL;
	}

#if BREAKABLE_GLASS_USE_BVH
	//Create BVH
	bgBreakable& breakable = bgGlassManager::GetGlassBreakable(m_GlassHandle);
	const bgBreakable::PanePieces& panePieces = breakable.GetPanePieces();
	const bgBreakable::PanePoints& panePoints = breakable.GetPanePoints();
	// This could result in some unused points being added when many pieces of the pane are broken,
	// but is usually more efficient than re-adding points for each polygon
	int paneVertCount = panePoints.GetCount(); 
	int triCount = 0;

	// Go through and count up the triangles first
	for (int pieceIndex = 0; pieceIndex < panePieces.GetCount(); ++pieceIndex)
	{
		const bgPanePiece& panePiece = panePieces[pieceIndex];

		if (panePiece.m_flags & bgPanePiece::kPF_Active)
		{
			triCount += panePiece.m_silhouette.GetCount()-2;
		}
	}

	const bgGeometryData& breakableData = breakable.GetGeometryData();
	Vector3 topDelta = breakableData.GetPosWidth();    // delta along top edge of pane
	Vector3 leftDelta = breakableData.GetPosHeight();  // delta along left edge of pane
	Vector3 topLeft = breakableData.GetPosTopLeft();
	Vector3 bottomRight = topLeft + topDelta + leftDelta;
	Vector3 boxMin, boxMax;
	boxMin.Min(topLeft, bottomRight);
	boxMax.Max(topLeft, bottomRight);

	phBoundBVH* tempBoundBVH = NULL;
	if (triCount > 0)
	{
		tempBoundBVH = rage_aligned_new(16) phBoundBVH;

		const float MIN_THICKNESS = 0.01f;
		float frontOffset = breakable.GetBoundsOffsetFront();
		float backOffset = breakable.GetBoundsOffsetBack();
		float clampedThickness = Max(frontOffset+backOffset, MIN_THICKNESS);
		Vector3 margin(clampedThickness, clampedThickness, clampedThickness);

		// need separate tris for front and back faces
		const int trisTotal = triCount * 2;
		// need separate vertices for front and back faces
		int vertCount = 2 * paneVertCount;

		const int numPerVertAttribs=0;
	#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
		#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			const int nMaterialColors=0;
			tempBoundBVH->Init(vertCount, numPerVertAttribs, 1, nMaterialColors, trisTotal, 0);
		#else
			tempBoundBVH->Init(vertCount, numPerVertAttribs, 1, trisTotal, 0);
		#endif
	#else
		#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			const int nMaterialColors=0;
			tempBoundBVH->Init(vertCount, numPerVertAttribs, 1, nMaterialColors, trisTotal);
		#else
			tempBoundBVH->Init(vertCount, numPerVertAttribs, 1, trisTotal);
		#endif
	#endif

	#if !__TOOL
		tempBoundBVH->InitQuantization(VECTOR3_TO_VEC3V(boxMin - margin), VECTOR3_TO_VEC3V(boxMax + margin));
	#endif // !__TOOL

		tempBoundBVH->SetMaterialId(0, m_Material);

		int vertsAdded = 0;
		int polysAdded = 0;

		Vector3 normal;
		normal.Cross(leftDelta, topDelta);
		normal.Normalize();

		int panePointCount = panePoints.GetCount();

		// points for front faces
		Vector3 frontBase = topLeft - frontOffset * normal;
		for (int vertIndex = 0; vertIndex < panePointCount; ++vertIndex)
		{
			const Vector2& panePoint(panePoints[vertIndex]);
			tempBoundBVH->SetVertex(vertsAdded++, VECTOR3_TO_VEC3V(frontBase + topDelta * panePoint.x + leftDelta * panePoint.y));
		}

		// points for back faces
		Vector3 backBase = topLeft + backOffset * normal;
		for (int vertIndex = 0; vertIndex < panePointCount; ++vertIndex)
		{
			const Vector2& panePoint(panePoints[vertIndex]);
			tempBoundBVH->SetVertex(vertsAdded++, VECTOR3_TO_VEC3V(backBase + topDelta * panePoint.x + leftDelta * panePoint.y));
		}

		// iterate over (potentially concave) pieces
		for (int pieceIndex = 0; pieceIndex < panePieces.GetCount(); ++pieceIndex)
		{
			const bgPanePiece& panePiece = panePieces[pieceIndex];

			if (panePiece.m_flags & bgPanePiece::kPF_Active)
			{
				int polyCount = panePiece.m_polygons.GetCount();
				// iterate over convex decomposition
				for (int polyIndex = 0; polyIndex < polyCount; ++polyIndex)
				{
					const bgPolygon& panePolygon = panePiece.m_polygons[polyIndex];
					bgIndex index0 = panePolygon[0];
					Vector3 vert0(VEC3V_TO_VECTOR3(tempBoundBVH->GetVertex(index0)));
					bgIndex index1 = panePolygon[1];
					Vector3 vert1(VEC3V_TO_VECTOR3(tempBoundBVH->GetVertex(index1)));
					// generate trifan-style triangles
					for (int i=2; i<panePolygon.GetCount(); i++)
					{
						bgIndex index2 = panePolygon[i];
						Vector3 vert2(VEC3V_TO_VECTOR3(tempBoundBVH->GetVertex(index2)));

						//there are cases where we end up with zero area triangles
						//So we get an assert while calling poly.InitTriangle. 
						//In this case it's harmless as we end up calling WeldVertices() below
						//which should group up all the vertices that are closeby and so
						// all the zero area triangles go away. So we pass false (for disabling
						//the assert) when we call poly.InitTriangle

						// Front face:
						{
							phPolygon poly;
							poly.InitTriangle(phPolygon::Index(index0),
								phPolygon::Index(index1),
								phPolygon::Index(index2),
								RCC_VEC3V(vert0), RCC_VEC3V(vert1), RCC_VEC3V(vert2) ASSERT_ONLY(, false));

							phPrimitive *primitive = &poly.GetPrimitive();
							primitive->SetType(PRIM_TYPE_POLYGON);

							tempBoundBVH->SetPolygon(polysAdded, poly);
							tempBoundBVH->SetPolygonMaterialIndex(polysAdded, 0);
							++polysAdded;
						}
						// Back face:
						{
							phPolygon poly;
							poly.InitTriangle(phPolygon::Index(index0 + panePointCount),
								phPolygon::Index(index2 + panePointCount),
								phPolygon::Index(index1 + panePointCount),
								RCC_VEC3V(vert0), RCC_VEC3V(vert2), RCC_VEC3V(vert1) ASSERT_ONLY(, false));

							phPrimitive *primitive = &poly.GetPrimitive();
							primitive->SetType(PRIM_TYPE_POLYGON);

							tempBoundBVH->SetPolygon(polysAdded, poly);
							tempBoundBVH->SetPolygonMaterialIndex(polysAdded, 0);
							++polysAdded;
						}
						// start iteration to next triangle
						vert1 = vert2;
						index1 = index2;
					}
				}
			}
		}

		Assert(trisTotal == polysAdded);
		Assert(vertCount == vertsAdded);

		tempBoundBVH->PostLoadCompute();

		tempBoundBVH->WeldVertices(0.001f);

		// It's possible that WeldVertices took all of our polygons away.  That's okay, we'll just abort the process at this point.
		if(tempBoundBVH->GetNumPolygons() == 0)
		{
			tempBoundBVH->Release(true);
			//delete tempBoundBVH;	// Deletion is handled by the Release() above.
			tempBoundBVH = NULL;
		}
		else
		{
			tempBoundBVH->ComputeNeighbors(NULL);
			tempBoundBVH->CalculatePolyNormals();
			tempBoundBVH->Build();
		}
	}

	//Create Bound Box
	phBoundBox* tempBoundBox = rage_aligned_new(16) phBoundBox;
	tempBoundBox->SetBoxSize(Max(Vec3V(boxMax-boxMin), Vec3V(V_FLT_SMALL_2)));

	//Create Composite box
	m_Bound = rage_aligned_new(16) phBoundComposite;

	m_Bound->Init(BoundIndex_Count, false);
	m_Bound->SetBound(BoundIndex_BVH, tempBoundBVH);
	m_Bound->SetBound(BoundIndex_Box, tempBoundBox);
	m_Bound->CalculateCompositeExtents(false);
	m_Bound->UpdateBvh(true);

#else

	bgBreakable& breakable = bgGlassManager::GetGlassBreakable(m_GlassHandle);
	const bgBreakable::PanePieces& panePieces = breakable.GetPanePieces();
	phBoundBox* tempBoundBox = NULL;
	if(panePieces.GetCount() > 0)
	{
		const bgGeometryData& breakableData = breakable.GetGeometryData();
		const Vector3& topDelta = breakableData.GetPosWidth();    // delta along top edge of pane
		const Vector3& leftDelta = breakableData.GetPosHeight();  // delta along left edge of pane
		const Vector3& topLeft = breakableData.GetPosTopLeft();
		Vector3 bottomRight = topLeft + topDelta + leftDelta;
		Vector3 boxMin, boxMax;
		boxMin.Min(topLeft, bottomRight);
		boxMax.Max(topLeft, bottomRight);

		//Create Bound Box
		tempBoundBox = rage_aligned_new(16) phBoundBox;
		tempBoundBox->SetBoxSize(Max(Vec3V(boxMax-boxMin), Vec3V(V_FLT_SMALL_2)));
	}

	//Create Composite box
	m_Bound = rage_aligned_new(16) phBoundComposite;

	m_Bound->Init(1, false);
	m_Bound->SetBound(0, tempBoundBox);
	m_Bound->CalculateCompositeExtents(false);
	m_Bound->UpdateBvh(true);

#endif // BREAKABLE_GLASS_USE_BVH

	return m_Bound;
}

void phGlass::HitElement(Vec3V_In impulse, Vec3V_In position, int element)
{
#if BREAKABLE_GLASS_USE_BVH
	phBoundBVH* tempBoundBVH = GetBoundBVH();
	if (tempBoundBVH && tempBoundBVH->GetNumPolygons() > element)
	{
		if (GetIsElementActive(element))
		{
			//Detect which weapon was used and use that for crack type
			int glassCrackType = 0;
			fragManager::ComputeGlassCrackTypeCallbackFunctor computeCrackTypeCallback =
				FRAGMGR->GetComputeGlassCrackTypeCallback();

			if (computeCrackTypeCallback.IsBound())
			{
				glassCrackType = computeCrackTypeCallback();
			}
			bgGlassManager::HitGlass(m_GlassHandle, glassCrackType, position, impulse);
			GLASS_RECORDER_ONLY(if(glassRecordingRage::IsActive()) { glassRecordingRage::Get()->OnHitGlass(m_GlassHandle, glassCrackType, position, impulse); })
			// @NOTE: 'this' could potentially be deleted; don't do anything here: just get out!
			return;
		}
	}
#else
	(void)element; // Without a BVH this is worthless

	//Detect which weapon was used and use that for crack type
	int glassCrackType = 0;
	fragManager::ComputeGlassCrackTypeCallbackFunctor computeCrackTypeCallback =
		FRAGMGR->GetComputeGlassCrackTypeCallback();

	if (computeCrackTypeCallback.IsBound())
	{
		glassCrackType = computeCrackTypeCallback();
	}
	bgGlassManager::HitGlass(m_GlassHandle, glassCrackType, position, impulse);
	GLASS_RECORDER_ONLY(if(glassRecordingRage::IsActive()) { glassRecordingRage::Get()->OnHitGlass(m_GlassHandle, glassCrackType, position, impulse); })
#endif // BREAKABLE_GLASS_USE_BVH
}

phBound* phGlass::GetBound()
{
	return m_Bound;
}

#if BREAKABLE_GLASS_USE_BVH
phBoundBVH* phGlass::GetBoundBVH()
{
	return m_Bound ? static_cast<phBoundBVH*>(m_Bound->GetBound(BoundIndex_BVH)) : NULL;
}
#endif // BREAKABLE_GLASS_USE_BVH

phBoundBox* phGlass::GetBoundBox()
{
#if BREAKABLE_GLASS_USE_BVH
	return m_Bound ? static_cast<phBoundBox*>(m_Bound->GetBound(BoundIndex_Box)) : NULL;
#else
	return m_Bound ? static_cast<phBoundBox*>(m_Bound->GetBound(0)) : NULL;
#endif // BREAKABLE_GLASS_USE_BVH
}

const phBound* phGlass::GetBound() const
{
	return m_Bound;
}

#if BREAKABLE_GLASS_USE_BVH
const phBoundBVH* phGlass::GetBoundBVH() const
{
	return m_Bound ? static_cast<phBoundBVH*>(m_Bound->GetBound(BoundIndex_BVH)) : NULL;
}
#endif // BREAKABLE_GLASS_USE_BVH

const phBoundBox* phGlass::GetBoundBox() const
{
#if BREAKABLE_GLASS_USE_BVH
	return m_Bound ? static_cast<phBoundBox*>(m_Bound->GetBound(BoundIndex_Box)) : NULL;
#else
	return m_Bound ? static_cast<phBoundBox*>(m_Bound->GetBound(0)) : NULL;
#endif // BREAKABLE_GLASS_USE_BVH;
}

} // namespace rage
