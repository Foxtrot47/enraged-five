// 
// phglass/phglass.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PHGLASS_PHGLASS_H 
#define PHGLASS_PHGLASS_H 

#include "glass.h"

#include "physics/instbreakable.h"

namespace rage {

class phGlassInst : public phInstBreakable
{
public:
	phGlassInst();

	int GetClassType() const { return PH_INST_GLASS; }				// for game-level use to identify derived classes

	phInst* PrepareForActivation(phCollider** UNUSED_PARAM(colliderToUse), phInst* UNUSED_PARAM(otherInst), const phConstraintBase * UNUSED_PARAM(constraint)) { return NULL; }

	void SetGlass(phGlass* glass);

	const phInst* GetIgnoreInst() const { return m_IgnoreInst; }
	void SetIgnoreInst(phInst* ignoreInst);

	virtual bool ShouldFindImpacts(const phInst* otherInst) const;

	virtual void PreComputeImpacts(phContactIterator impacts);

	virtual void NotifyImpulse (const Vector3& impulse, const Vector3& position, int component=0, int element=0, float breakScale=1.0f);

	bgGlassHandle GetGlassHandle() const;

#if BREAKABLE_GLASS_USE_BVH
	bool GetIsElementActive(int element) const;
#else
	bool GetIsHitActive(const Vector3& vHitPos) const;
#endif // BREAKABLE_GLASS_USE_BVH

	// Safe casting of phInst to game derivations using phInst->GetClassType() tests.
	__forceinline static phGlassInst* GetGlassInst(phInst* pInst)
	{
		switch(pInst->GetClassType())
		{
		case phInst::PH_INST_GLASS:
#if 0//__DEV -- Would be nice, but man does that kill framerate
			// Just make sure it really is a phGlassInst.
			Assert(dynamic_cast<phGlassInst*>(pInst));
#endif
			return static_cast<phGlassInst*>(pInst);
		}

#if 0//__DEV -- Would be nice, but man does that kill framerate
		// Just make sure it isn't really a phGlassInst.
		Assert(!dynamic_cast<phGlassInst*>(pInst));
#endif
		return 0;
	}

	__forceinline static const phGlassInst* GetGlassInst(const phInst* pInst)
	{
		return GetGlassInst(const_cast<phInst*>(pInst));
	}

private:
	phGlass* m_Glass;
	phInst* m_IgnoreInst;
};

inline bool phGlassInst::ShouldFindImpacts(const phInst* otherInst) const
{
	return otherInst != m_IgnoreInst && otherInst->GetUserData() != GetUserData();
}

inline bgGlassHandle phGlassInst::GetGlassHandle() const
{
	if (NULL == m_Glass)
	{
		// This can happen legitimately when glass is recycled; phInst deletion must be defered
		return bgGlassHandle_Invalid;
	}
	return m_Glass->GetGlassHandle();
}

#if BREAKABLE_GLASS_USE_BVH
inline bool phGlassInst::GetIsElementActive(int element) const
{
	if (NULL == m_Glass)
	{
		// This can happen legitimately when glass is recycled; phInst deletion must be defered
		return false;
	}
	return m_Glass->GetIsElementActive(element);
}
#else
inline bool phGlassInst::GetIsHitActive(const Vector3& vHitPos) const
{
	if (NULL == m_Glass)
	{
		// This can happen legitimately when glass is recycled; phInst deletion must be defered
		return false;
	}
	return m_Glass->GetIsHitActive(vHitPos);
}
#endif // BREAKABLE_GLASS_USE_BVH


} // namespace rage

#endif // PHGLASS_PHGLASS_H 
