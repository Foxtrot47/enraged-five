// 
// phglass/glass.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PHGLASS_GLASS_H 
#define PHGLASS_GLASS_H 

#include "breakableglass/glassmanager.h"
#include "phcore/materialmgr.h"

namespace rage {

class bgBreakable;
class phBound;
class phBoundComposite;
class phBoundBVH;
class phBoundBox;

class phGlass
{
public:
#if BREAKABLE_GLASS_USE_BVH
	enum BoundIndices
	{
		BoundIndex_BVH = 0,
		BoundIndex_Box,
		BoundIndex_Count
	};
#endif // BREAKABLE_GLASS_USE_BVH

	phGlass();
	phGlass(const phGlass& rhs)
	{
		*this = rhs;
	}

	~phGlass();

	phBound* Init(bgGlassHandle in_handle, phMaterialMgr::Id material);
	void Shutdown();
	bgGlassHandle GetGlassHandle() const;
	void Cleanup();
#if BREAKABLE_GLASS_USE_BVH
	bool GetIsElementActive(int element) const;
#else
	// Check if a 3D position is inside any of the active glass pieces
	bool GetIsHitActive(const Vector3& vHitPos) const;
#endif // BREAKABLE_GLASS_USE_BVH
	void HitElement(Vec3V_In impulse, Vec3V_In position, int element);
	phBound* GetBound();
#if BREAKABLE_GLASS_USE_BVH
	phBoundBVH* GetBoundBVH();
#endif // BREAKABLE_GLASS_USE_BVH
	phBoundBox* GetBoundBox();
	const phBound* GetBound() const;
#if BREAKABLE_GLASS_USE_BVH
	const phBoundBVH* GetBoundBVH() const;
#endif // BREAKABLE_GLASS_USE_BVH
	const phBoundBox* GetBoundBox() const;

	phBound* RegenerateBound();

private:

	phGlass& operator = (const phGlass& rhs);

	void SetMaterial(phMaterialMgr::Id material);

	bgGlassHandle m_GlassHandle;

	phBoundComposite* m_Bound;
	phMaterialMgr::Id m_Material;
};

} // namespace rage

#endif // PHGLASS_GLASS_H 
