// 
// phglass/phglass.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "glassinstance.h"

#include "physics/collider.h"

namespace rage {

phGlassInst::phGlassInst()
	: m_Glass(NULL)
	, m_IgnoreInst(NULL)
{

}

void phGlassInst::SetGlass(phGlass* glass)
{
	m_Glass = glass;
}

void phGlassInst::SetIgnoreInst(phInst* ignoreInst)
{
	m_IgnoreInst = ignoreInst;
}

void phGlassInst::PreComputeImpacts(phContactIterator impacts)
{
	if (NULL == m_Glass)
	{
		return;
	}

	for ( ; !impacts.AtEnd(); ++impacts)
	{
		if (impacts.GetOtherInstance() != m_IgnoreInst)
		{
			Vec3V otherImpulse(V_ZERO);
			if (phCollider *otherCollider = impacts.GetOtherCollider())
			{
				otherImpulse = otherCollider->CalculateMomentum();
			}
			if (m_Glass)
			{  // HitElement() can cause m_Glass to be set to NULL, so this check is not redundant with the one at the start of the function
				m_Glass->HitElement(otherImpulse, impacts.GetMyPosition(), impacts.GetMyElement());
			}
		}
		if (m_Glass)
		{
			impacts.DisableImpact();
		}
	}
}

void phGlassInst::NotifyImpulse(const Vector3& impulse, const Vector3& position, int UNUSED_PARAM(component), int element, float UNUSED_PARAM(breakScale))
{
	if (NULL == m_Glass)
	{
		return;
	}

	m_Glass->HitElement(RCC_VEC3V(impulse), RCC_VEC3V(position), element);
}

} // namespace rage

