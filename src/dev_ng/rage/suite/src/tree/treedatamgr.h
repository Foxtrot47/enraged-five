// 
// speedtree/treedatamgr.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef SPEEDTREE_TREEDATAMGR_H
#define SPEEDTREE_TREEDATAMGR_H


#include "atl/array.h"
#include "atl/string.h"
#include "data/struct.h"
#include "tree/treedata.h"

#include "paging/dictionary.h"


namespace rage
{
class bkBank;
class TreeRenderer;

typedef u32 TreeHash;

template<class TREEDATA>
struct TreeDataRequest
{
	TreeDataRequest() {};
	TreeDataRequest(datResource& /*dat*/) {};

	TREEDATA *	pTreeData;
	atString	szTreeFile;
	//atString	szHashString;
	TreeHash	uHash;
	int			nRenderTargetSize;
	int			nSeed;
	int			nReferenceCount;
	float		fSize;
	float		fSizeVariance;
	bool		bCreateBounds;
	bool		bNoBuffers;
	bool		pad[2];

	int Release()
	{
		// Don't do anything - this object is managed by its own dictionary.
		return 0;
	}

	void BeforeCopy() { }	// TODO: this is required for defragmentation
	//void PatchReferences(const datResourcePatch&) { }

	void DeclareStruct(datTypeStruct &s);

	DECLARE_PLACE(TreeDataRequest);
};


template<class TREEDATA>
class TreeDataMgr : public pgDictionary<TreeDataRequest<TREEDATA> >
{
	enum
	{
		// Maximum number of individual tree data elements that can be stored
		DEFAULT_MAX_TREE_DATA_COUNT = 64
	};

	
public:

	enum ResourceMode
	{
		Mode_CreateAndStore,
#if !__FINAL
		Mode_CreateAndRemember,
#endif // !__FINAL
	};

	enum
	{
		RORC_VERSION = 2
	};

	TreeDataMgr(int maxTreeDataCount);

	// PURPOSE: Request a certain tree data with specific parameters. If this particular
	// tree type is already in memory, a pointer will be returned and its reference
	// count will be incremented. If not, the tree will be loaded from memory.
	TREEDATA *RequestTreeData(const char *szTreeFile, int nRenderTargetSize, int nSeed = 0, float fSize = 1.0f, float fSizeVariance = 0.0f, TreeHash *puHashValue = NULL, bool bCreateBounds = true, bool bOfflineCreation = false);

	// PURPOSE: Call this to tell the system that there is a certain tree type that
	// will be needed. This function will not actually load the tree - to do that,
	// use AddRef.
	TreeHash RegisterTreeData(const char *szTreeFile, int nRenderTargetSize, int nSeed = 0, float fSize = 1.0f, float fSizeVariance = 0.0f, bool bCreateBounds = true, bool bNoBuffers = false);

	void RenderAllBillboardTextures(TreeRenderer* renderer, Vector3::Vector3Param sunLightDirection);

	// PURPOSE: Look up a specific tree. NOTE that this function will not modify
	// the reference counter. It may also return NULL (and assert) if AddRef has never been 
	// called (which shouldn't have happened anyway - you should have first called
	// AddRef to express interest before calling GetTree).
	TREEDATA *GetTree(TreeHash uHash);
	TREEDATA *GetTree(TreeDataRequest<TREEDATA> &request);

	int AddRef(TreeHash uHash);
	int AddRef(TreeDataRequest<TREEDATA> &request);

	int Release(TreeHash uHash);

	static TreeDataMgr<TREEDATA> &GetInstance()
	{
		FastAssert(sm_Instance);
		return *sm_Instance;
	}

	static bool IsInstantiated()
	{
		return (sm_Instance != NULL);
	}

	static void InitClass(int maxTreeDataCount = DEFAULT_MAX_TREE_DATA_COUNT);

	static TreeHash CreateTreeTypeHash(const char *szTreeFile, int nSeed = 0, float fSize = 1.0f, float fSizeVariance = 0.0f);

	static void ShutdownClass();

	static void InitFromResource(const char *szRscFile, const char *szRscExt = "#sd");

	void SetUseAutoResource( bool val = true )	{ m_UseAutoResource = val; }
	void SetUseResource( bool val=true ) { m_UseResource = val; }

#if __RESOURCECOMPILER
	void CreateDictionaryFromRequests();

	static void SetResourceMode(ResourceMode mode)
	{
		m_ResourceMode = mode;
	}

	static ResourceMode GetResourceMode()
	{
		return m_ResourceMode;
	}
#endif // __RESOURCECOMPILER

#if __BANK
	void AddWidgets(bkBank &bank);

#endif // __BANK
	

private:

#if __BANK
	void UpdateCurrentTree();


	short		m_CurrentTree;					// Current tree that's being investigated in the bank
	short		m_ActiveTreeTypeCount;			// Number of resident tree types
	char		m_ActiveTreeCountString[8];		// ASCII version of m_ActiveTreeTypeCount
	char		m_CurrentTreeName[64];			// Name of the current tree being investigated
	TreeHash	m_CurrentTreeHash;				// Hash value of the tree that's being investigated
	char		m_CurrentTreeHashString[12];	// ASCII version of m_CurrentTreeHash
	char		m_CurrentTreeStatus[48];		// Status of the current tree
#endif // __BANK

	bool		m_UseAutoResource;
	bool		m_UseResource;



	TREEDATA *LoadTreeType(const TreeDataRequest<TREEDATA> &request);

	void DeleteTreeType(TREEDATA *pTreeData);

	static bool RenderBillboardTextureCB(TreeDataRequest<TREEDATA> &pData, TreeHash uHash, void *pRenderer);

	// List of requests. This is an atArray since the order matters - speedtree forests use an index
	// to identify a tree.
	atArray<TreeDataRequest<TREEDATA> > m_TreeDataRequests;


	static TreeDataMgr *sm_Instance;

	
#if __RESOURCECOMPILER

	// The current mode we're operating at
	static ResourceMode m_ResourceMode;

#endif // !__FINAL
};

typedef TreeDataMgr<instanceTreeData> instanceTreeDataMgr;

} // namespace rage

#endif // SPEEDTREE_TREEDATAMGR_H
