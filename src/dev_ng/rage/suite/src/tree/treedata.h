// 
// speedtree/treedata.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPEEDTREE_TREEDATA_H
#define SPEEDTREE_TREEDATA_H

#include "vector/vector4.h"
#include "atl/array.h"
#include "grcore/device.h"
#include "data/struct.h"
#include "grmodel/imposter.h"
#include "paging/base.h"
//#include "phbound/boundcomposite.h"
#include "phbound/boundsphere.h"
#include "phbound/boundbox.h"
#include "phbound/boundcapsule.h"
#include "physics/archetype.h"

#include "system/timer.h"
#include "atl/string.h"


namespace rage
{

class TreeDataBase: public pgBaseRefCounted
{
public:
	TreeDataBase() : pgBaseRefCounted(0) { }
	TreeDataBase(datResource& rsc) : pgBaseRefCounted(rsc) {}

	DECLARE_PLACE(TreeDataBase);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif
};

class grcTexture;
class grcVertexBuffer;
class grcIndexBuffer;
class grcFvf;
class grmImposter;
class rmcDrawable;

class InstancedVerts : public datBase
{
	atArray<Vector3>		m_positions;
	atArray<Vector3>		m_normals;
	atArray<Vector2>		m_uvs;
public:

	InstancedVerts() {}
	InstancedVerts(datResource&);
	DECLARE_PLACE(InstancedVerts);

	void Begin( int size ) 
	{ 
		m_positions.Reserve( size +1 );
		m_normals.Reserve( size +1);
		m_uvs.Reserve( size +1);
	}
	void Add( const Vector3& pos, const Vector3& normal, const Vector2& uv )
	{
		m_positions.Push( pos);
		m_normals.Push( normal);
		m_uvs.Push( uv);
	}

	void End()
	{
		if ( 0 &&  m_positions.GetCount() > 0)
		{
			// add a degenerate vert to the end ( could do in shader )
			m_positions.Push( m_positions[ m_positions.GetCount() - 1] );
			m_normals.Push( m_normals[ m_normals.GetCount() - 1] );
			m_uvs.Push( m_uvs[ m_uvs.GetCount() - 1] );
		}
		
	}
	int GetAmount() const { return m_positions.GetCount(); }

	const Vector3*		GetPositions()	 const { return &m_positions[0];}
	const Vector3*		GetNormals()	 const { return &m_normals[0];}
	const Vector2*		GetUVS()		 const { return &m_uvs[0];}

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif
};



//=============================================================================
// instanceTreeData
//
// PURPOSE
//	instanceTreeData is the rage data representation of a speedtree
class instanceTreeData : public TreeDataBase
{
public:
	static const int RORC_VERSION = 2;

	instanceTreeData() {}
	~instanceTreeData() {}

	// PURPOSE: Create the tree data
	bool Create( const char* drawableName );

	bool Create(const char* name, int /*nSeed*/, float /*fSize*/, float /*fSizeVariance*/, bool /*bCreateBounds*/ = true, bool /*bNoBuffers*/ = false, bool  /*bUseSelfShadowMaps*/ = false,  bool /*bNoVertBuffers*/ = false)
	{
		return Create( name );
	}


	// PURPOSE: Test to see a billboard texture has been generated for this tree
	// RETURNS: true if a billboard texture has been generated for this tree, false otherwise
	bool IsBillboardGenerated() const										{ return m_bBillboardGenerated; }

	// PURPOSE: Set the billboard generated flag
	// PARAMS:
	//	bGenerated - true = a billboard has been generated for this tree, false otherwise
	void SetBillboardGenerated(bool bGenerated)								
	{ 
		m_bBillboardGenerated = bGenerated; 
		m_billboardTimeStamp = sysTimer::GetSystemMsTime();
	}

	u32 GetBillboardGenerationTimeStamp() { return m_billboardTimeStamp; }


	void GetBoundingBox( Vector3& minPos, Vector3& maxPos ) const;
	
	// PURPOSE: Get the bounding sphere for this tree
	// RETURNS: A Vector4 containing the sphere center in the xyz components and the sphere radius in the w component.
	// NOTES:
	//	The sphere returned by this function has an extra 1.0f added to its radius
	Vector4 GetCullSphere() const;
	
	
	// PURPOSE: Get the discreet lod levels for parts of this tree
	// RETURNS: A Vector4 containing:
	//				the branch lod count in the x component
	//				the frond lod count in the y component
	//				the leaf lod count in the z component
	Vector4 GetDiscreetLodLevels() const									{ return Vector4( 0.0f,0.0f,0.0f,0.0f); }

	// PURPOSE: Get a pointer to the physicis archetype for this tree
	// RETURNS: A pointer to the physics archetype, NULL if there is no physics bound
	phArchetype* GetPhysicsArchetype() const							{ return 0; }


	const grcRenderTarget* GetBillboardTexture() const							{ return m_imposter->GetColor(); }

	const grcRenderTarget* GetBillboardShadowTexture() const						{ return m_imposter->GetShadow(); }

	grmImposter* GetImposter()						{ return m_imposter; }

	// PURPOSE: Set the size of the render targets for this tree
	// PARAMS:
	//	nSize - The width and hight in texels of the desired render target
	// NOTES:
	//	These render targets are always square.
	//	This size should be setup before creating the tree.
	void SetRenderTargetSize(int nSize)										{ m_rtWidth = static_cast<u16>( nSize ); }


private:
	instanceTreeData& operator=( const instanceTreeData& );

	grmImposter*						m_imposter;
	rmcDrawable*						m_Drawable;

	unsigned int						m_billboardTimeStamp;

	u16									m_rtWidth;

	bool								m_bBillboardGenerated;
	char								m_pad[1 ];
};

};
#endif // SPEEDTREE_TREEDATA_H
