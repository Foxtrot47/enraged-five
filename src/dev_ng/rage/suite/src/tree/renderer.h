// 
// speedtree/renderer.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPEEDTREE_RENDERER_H
#define SPEEDTREE_RENDERER_H

#include "grcore/device.h"
#include "grcore/effect_typedefs.h"
#include "vector/vector3.h"
#include "atl\array.h"

#include "grmodel/imposter.h"
#include "rendererInterface.h"
#include "system/typeinfo.h"

namespace rage
{

class bkBank;
class grmShader;
class TreeInstance;
class instanceTreeData;
class instanceTreeData;
class Matrix44;

class InstancedVerts;






#define REGISTER_BILLBOARDS __XENON




//=============================================================================
// speedTreeRenderer
//
// PURPOSE
//   speedTreeRenderer manages shaders and rendering functionality for speedtree objects
//	Any shader can be replaced at anytime as long as the shader has the required variables.
//	The requried variables are:
//		- Branch Shader -
//			DiffuseTex - A 2D texture sampler to hold the branch texture
//			WindMatrices - An array of 4x4 matrices to recieve the wind matrices from speedwind
//
//		- Frond Shader -
//			DiffuseTex - A 2D texture sampler to hold the frond texture
//			WindMatrices - An array of 4x4 matrices to recieve the wind matrices from speedwind
//
//		- Leaf Shader -
//			DiffuseTex - A 2D texture sampler to hold the leaf texture
//			WindMatrices - An array of 4x4 matrices to recieve the wind matrices from speedwind
//			LeafClusters - An array of 48 float4s to recieve the leaf corner offest vectors
//			LeafAngleMatrices - An array of 4x4 matrices to recieve the leaf rocking matrices
//
//		- Billboard Shader -
//			DiffuseTex - A 2D texture sampler to hold the billboard texture
class speedTreeRenderer : public TreeRenderer
{
public:

	enum DrawPasses
	{
		DP_NORMAL = 0,
		DP_DEPTH_WRITE,
		DP_BLENDED_SHADOW_WRITE,
		DP_USE_SHADOW_MAP,
		DP_NO_WIND,
	};

	speedTreeRenderer();
	~speedTreeRenderer();

	// PURPOSE: Load the default rage speedtree shaders
	// NOTES: This function calls the following functions:
	//			LoadDefaultBranchShader
	//			LoadDefaultFrondShader
	//			LoadDefaultLeafShader
	//			LoadDefaultBillboardShader
	void LoadDefaultShaders();

	// PURPOSE: Load the default rage branch shader
	void LoadDefaultBranchShader();

	// PURPOSE: Load the default rage frond shader
	void LoadDefaultFrondShader();

	// PURPOSE: Load the default rage leaf shader
	void LoadDefaultLeafShader();

	// PURPOSE: Load the default rage billboard shader
	void LoadDefaultBillboardShader();

	// PURPOSE: Cleanup all the default shaders
	// NOTES:
	//	 This should be called when you are done using the shaders to cleanly shutdown the system
	void CleanupDefaultShaders();

	// PURPOSE: Set the current branch shader
	// PARAMS:
	//	pShader - A pointer to the new shader
	void SetBranchShader(grmShader* pShader);

	// PURPOSE: Set the current frond shader
	// PARAMS:
	//	pShader - A pointer to the new shader
	void SetFrondShader(grmShader* pShader);

	// PURPOSE: Set the current leaf shader
	// PARAMS:
	//	pShader - A pointer to the new shader
	void SetLeafShader(grmShader* pShader);

	// PURPOSE: Set the current billboard shader
	// PARAMS:
	//	pShader - A pointer to the new shader
	void SetBillboardShader(grmShader* pShader);


	// internal matrix registers access (they are set to default values in LoadDefaultXXXShader()):
	void SetBranchMatrixRegister(int r)								{ m_nBranchMatrixRegister = r;	}
	void SetFrondMatrixRegister(int r)								{ m_nFrondMatrixRegister = r;		}
	void SetLeafMatrixRegister(int r)								{ m_nLeafMatrixRegister = r;		}
	void SetBillboardMatrixRegister(int r)							{ m_nBillboardMatrixRegister = r;	}

	int GetBranchMatrixRegister() const								{ return m_nBranchMatrixRegister;	}
	int GetFrondMatrixRegister() const								{ return m_nFrondMatrixRegister;		}
	int GetLeafMatrixRegister() const								{ return m_nLeafMatrixRegister;		}
	int GetBillboardMatrixRegister() const							{ return m_nBillboardMatrixRegister;	}



	// PURPOSE: Get the current branch shader
	// RETURNS: A pointer to the current branch shader
	const grmShader* GetBranchShader() const						{ return m_pBranchShader; }

	// PURPOSE: Get the current frond shader
	// RETURNS: A pointer to the current frond shader
	const grmShader* GetFrondShader() const							{ return m_pFrondShader; }

	// PURPOSE: Get the current leaf shader
	// RETURNS: A pointer to the current leaf shader
	const grmShader* GetLeafShader() const							{ return m_pLeafShader; }

	// PURPOSE: Get the current billboard shader
	// RETURNS: A pointer to the current billboard shader
	const grmShader* GetBillboardShader() const						{ return m_pBillboardShader; }

	void SetUpVector(const Vector3& vUp);

	void SetDrawFronds( bool val );

	
	// PURPOSE: Set the current wind matrices
	// PARAMS:
	//	pWindMatrices - An array of wind matrices
	//	nWindMatrixCount - Number of wind matrices in the pWindMatrices array
	//	pRockMatrices - An array of leaf rock matrices
	//	nRockMatrixCount - Number of matrices in the pRockMatrices array
	void SetWindMatrices(const Matrix44* pWindMatrices, int nWindMatrixCount, const Matrix44* pRockMatrices, int nRockMatrixCount);

	void RenderTreeBillboardTexture(TreeDataBase* pTreeData, bool bForce, bool needClear = true, bool isShadow = false, Vector3* viewDirection = 0 )
	{
		RenderTreeBillboardTexture( smart_cast<instanceTreeData*>( pTreeData ), bForce, needClear, isShadow , viewDirection );
	}

	void DrawTrees(TreeDataBase* pTreeData, TreeInstance** pInstanceList, int nInstanceCount)
	{
		DrawTrees( smart_cast<instanceTreeData*>( pTreeData ), pInstanceList, nInstanceCount);
	}

	void DrawShadowTrees( TreeDataBase* pTreeData, TreeInstance** pInstanceList, int nInstanceCount, Vector3* lightDirection )
	{
		DrawShadowTrees( smart_cast<instanceTreeData*>( pTreeData ), pInstanceList, nInstanceCount, lightDirection );
	}

private:
	// PURPOSE: Generate the billboard texture for the specified treee
	// PARAMS:
	//	pTreeData - The tree whos billboard is to be created
	//	bForce - Generate a new billboard even if one already exists
	//  needClear - Clears the render target before use 
	// NOTES: This function will not generate a new billboard texture if one already exists unless you
	//		specify true for the bForce parameter.  Also, this should be called before anything is 
	//		drawn as this will clear the depth buffer after it is done rendering the render target.
	void RenderTreeBillboardTexture(instanceTreeData* pTreeData, bool bForce, bool needClear = true, bool isShadow = false, Vector3* viewDirection = 0 );

	// PURPOSE: Draw instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	// NOTES:
	//	This function calls the following functions:
	//		DrawBillboards
	void DrawTrees(instanceTreeData* pTreeData, TreeInstance** pInstanceList, int nInstanceCount);


	// PURPOSE: Draw instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	// NOTES:
	//	This function calls the following functions:
	//		DrawBranches
	//		DrawFronds
	//		DrawLeaves
	//		DrawBillboards
	void DrawShadowTrees( instanceTreeData* pTreeData, TreeInstance** pInstanceList, int nInstanceCount, Vector3* lightDirection );

	// PURPOSE: Draw branch instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	void DrawBranches(const instanceTreeData& pTreeData, TreeInstance** pInstanceList, int nInstanceCount, grcEffectTechnique tech = grcetNONE );

	// PURPOSE: Draw frond instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	void DrawFronds(const instanceTreeData& pTreeData, TreeInstance** pInstanceList, int nInstanceCount, grcEffectTechnique tech = grcetNONE );

	// PURPOSE: Draw leaf instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	void DrawLeaves(const instanceTreeData& pTreeData, TreeInstance** pInstanceList, int nInstanceCount, grcEffectTechnique tech = grcetNONE );

	// PURPOSE: Draw billboard instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	void DrawBillboards(instanceTreeData& pTreeData, TreeInstance** pInstanceList, int nInstanceCount, bool isShadow = false, Vector3* viewDirection  = 0);

	
public:
	
	void SetBackGroundClearColor( Color32 col )		{ m_cBillboardBackgroundColor = col; }

#if __BANK
	void AddWidgets(bkBank& bank);
#endif

	void UseSubPixelAlpha( bool subPixelAlpha )		{ m_useSubPixelAlpha = subPixelAlpha; }
	
private:
#if REGISTER_BILLBOARDS
	void DrawRegisterBillboards(TreeInstance** pInstanceList, const instanceTreeData& pTreeData, int nInstanceCount, float fDimension, float fHalfDimension, bool forceUp, Vector3* viewDirection );
#endif

protected:
	grmShader*					m_pBranchShader;
	grmShader*					m_pFrondShader;
	grmShader*					m_pLeafShader;
	grmShader*					m_pBillboardShader;

	int							m_nBranchMatrixRegister;
	int							m_nFrondMatrixRegister;
	int							m_nLeafMatrixRegister;
	int							m_nBillboardMatrixRegister;

	grcEffectVar				m_evBranchTexture;
	grcEffectVar				m_evFrondTexture;
	grcEffectVar				m_evLeafTexture;
	grcEffectVar				m_evBillboardTexture;
	grcEffectVar				m_evNormalMapBillboardTexture;
	grcEffectTechnique			m_BillboardImposterTechnique;



	grcEffectVar				m_evBranchWindMatrices;
	grcEffectVar				m_evFrondWindMatrices;
	grcEffectVar				m_evLeafWindMatrices;
	grcEffectVar				m_evLeafRockMatrices;
	grcEffectVar				m_evLeafTable;

	grcEffectVar				m_evLeafUVTable;

	grcEffectVar				m_evBranchWorldMatrix;
	grcEffectVar				m_evBranchWorldInvTransposeMatrix;
	grcEffectVar				m_evBranchTreeCentre;
	grcEffectVar				m_evBranchTreeAlpha;
	grcEffectTechnique			m_createBranchImposterTechnique;

	grcEffectVar				m_evFrondWorldMatrix;
	grcEffectVar				m_evFrondWorldInvTransposeMatrix;
	grcEffectVar				m_evFrondTreeCentre;
	grcEffectVar				m_evFrondTreeAlpha;
	grcEffectTechnique			m_createFrondImposterTechnique;

	grcEffectVar				m_evLeafWorldMatrix;
	grcEffectVar				m_evLeafWorldInvTransposeMatrix;
	grcEffectVar				m_evleafTreeCentre;
	grcEffectVar				m_evLeafTreeAlpha;
	grcEffectTechnique			m_createLeafImposterTechnique;

	grcEffectVar				m_evBillboardWorldMatrices;
	grcEffectVar				m_evbillboardTreeCentre;


	Vector3						m_vUp;

	bool						m_bBlendLeafLod;
	float						m_fCamDist;

	int							m_nLeafAlphaOffset;
	int							m_nBillboardLeafAlpha;

	Color32						m_cBillboardBackgroundColor;
	bool						m_useSubPixelAlpha;	

	grcEffectTechnique			m_tech;

};



//=============================================================================
// InstanceTreeRenderer
//
// PURPOSE
//   speedTreeRenderer manages shaders and rendering functionality for speedtree objects
//	Any shader can be replaced at anytime as long as the shader has the required variables.
//	The requried variables are:
//
class InstanceTreeRenderer : public TreeRenderer
{
public:
	InstanceTreeRenderer() {}
	~InstanceTreeRenderer() {}

	// PURPOSE: Set the index of the pass to draw when rendering
	// PARAMS:
	//	nPass - The pass of the draw technique to render with
	void SetDrawPassIndex(int nPass)								{ m_nDrawPassIndex = nPass; }

	// PURPOSE: Get the index of the pass to draw when rendering
	// RETURNS: The pass index of the draw technique used to render
	int GetDrawPassIndex() const									{ return m_nDrawPassIndex; }



	// PURPOSE: Set the current wind matrices
	// PARAMS:
	//	pWindMatrices - An array of wind matrices
	//	nWindMatrixCount - Number of wind matrices in the pWindMatrices array
	//	pRockMatrices - An array of leaf rock matrices
	//	nRockMatrixCount - Number of matrices in the pRockMatrices array
	void SetWindMatrices(const Matrix44* /*pWindMatrices*/, int /*nWindMatrixCount*/, const Matrix44* /*pRockMatrices*/, int /*nRockMatrixCount*/)
	{}
	



	// PURPOSE: Generate the billboard texture for the specified treee
	// PARAMS:
	//	pTreeData - The tree whos billboard is to be created
	//	bForce - Generate a new billboard even if one already exists
	//  needClear - Clears the render target before use 
	// NOTES: This function will not generate a new billboard texture if one already exists unless you
	//		specify true for the bForce parameter.  Also, this should be called before anything is 
	//		drawn as this will clear the depth buffer after it is done rendering the render target.
	void RenderTreeBillboardTexture( TreeDataBase* pTreeData, bool bForce, bool needClear = true, bool isShadow = false, Vector3* viewDirection = 0 )
	{
		RenderTreeBillboardTexture( smart_cast<instanceTreeData*>( pTreeData ), bForce, needClear,isShadow, viewDirection);
	}

	// PURPOSE: Draw instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	//
	void DrawTrees( TreeDataBase* pTreeData, TreeInstance** pInstanceList, int nInstanceCount)
	{
		DrawTrees( smart_cast<instanceTreeData*>( pTreeData ), pInstanceList, nInstanceCount);
	}

	// PURPOSE: Draw instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	void DrawShadowTrees(  TreeDataBase* pTreeData, TreeInstance** pInstanceList, int nInstanceCount, Vector3* lightDirection )
	{
		DrawShadowTrees( smart_cast<instanceTreeData*>( pTreeData ), pInstanceList, nInstanceCount, lightDirection );
	}
	

#if __BANK
	void AddWidgets(bkBank& bank);
#endif

private:

	// PURPOSE: Generate the billboard texture for the specified treee
	// PARAMS:
	//	pTreeData - The tree whos billboard is to be created
	//	bForce - Generate a new billboard even if one already exists
	//  needClear - Clears the render target before use 
	// NOTES: This function will not generate a new billboard texture if one already exists unless you
	//		specify true for the bForce parameter.  Also, this should be called before anything is 
	//		drawn as this will clear the depth buffer after it is done rendering the render target.
	void RenderTreeBillboardTexture( instanceTreeData* pTreeData, bool bForce, bool needClear = true, bool isShadow = false, Vector3* viewDirection = 0 );

	// PURPOSE: Draw instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	//
	void DrawTrees(instanceTreeData* pTreeData, TreeInstance** pInstanceList, int nInstanceCount);


	// PURPOSE: Draw instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	void DrawShadowTrees( instanceTreeData* pTreeData, TreeInstance** pInstanceList, int nInstanceCount, Vector3* lightDirection );




	void DrawTreesList( instanceTreeData* pTreeData, TreeInstance** pInstanceList, int nInstanceCount, bool useShadow, Vector3* lightDirection );

	

	grmImposterShader		m_shader;
};


} // namespace rage

#endif // SPEEDTREE_RENDERER_H
