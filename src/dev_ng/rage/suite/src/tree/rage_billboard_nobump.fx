#define DIFFUSE_CLAMP Clamp

#include "../../../base/src/shaderlib/rage_common.fxh"

#define SHADOWS 0
#define FOURSHADOWMAPS 1
#define ZBIAS_PER_OBJECT 1
#if SHADOWS
#include "../../../base/src/shaderlib/rage_shadowmap_common.fxh"
#include "../../../base/src/shaderlib/rage_shadow.fxh"
#endif


BeginSampler(sampler2D,diffuseBillboardTexture,DiffuseBillboardSampler,DiffuseBillboardTex)
    string UIName = "Diffuse Billboard Texture";
ContinueSampler(sampler2D,diffuseBillboardTexture,DiffuseBillboardSampler,DiffuseBillboardTex)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
EndSampler;


BeginSampler(sampler2D,NormalMapBillboardTexture,NormalMapBillboardSampler,NormalMapBillboardTex)
    string UIName = "Normal Map  Billboard Texture";
ContinueSampler(sampler2D,NormalMapBillboardTexture,NormalMapBillboardSampler,NormalMapBillboardTex)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
EndSampler;

float3 g_vUpVector : UpVector = {0.0f, 1.0f, 0.0f};


float4 BillboardVertex(float3 pos, float3 center)
{
	// Compute the view normal
	float3 viewNrm = normalize(gViewInverse[3].xyz - center);
    
    // Compute the right vector
	float3 up = normalize(g_vUpVector);
	float3 viewRight = normalize(cross(viewNrm, up));
    
    // Compute the new view normal
	viewNrm = normalize(cross(up, viewRight));
    
    // Compute rotation basis matrix
	float3x3 mBillboardBasis;
	mBillboardBasis[0] = float3(viewNrm.x, up.x, -viewRight.x);
	mBillboardBasis[1] = float3(viewNrm.y, up.y, -viewRight.y);
	mBillboardBasis[2] = float3(viewNrm.z, up.z, -viewRight.z);
        
	// Rotate to face the camera
	float4 vWorldPos = float4(mul(pos, mBillboardBasis), 1.0f); 
	
	// Translate to the correct position
	vWorldPos += float4(center, 0.0f);
	
	return vWorldPos;
}

#if  0
//__XENON
// xenon optimized version removed until good profiling case setup

float4 quadVerts[4] : QuadVerts : register(vs, c120);
float4 quadUVs[4] : QuadUVs : register(vs, c124);
	
struct vertexBillboardOut {
    float4 pos				: POSITION;
    float3 texCoord0		: TEXCOORD0;
	
#if SHADOWS
	float4 shadowPixelPos0			: TEXCOORD1;
//	float4 shadowPixelPos1			: TEXCOORD2;
//	float4 shadowPixelPos2			: TEXCOORD3;
//	float4 shadowPixelPos3			: TEXCOORD4;
	float4 WorldPos					: TEXCOORD5;
#endif
	float3 ScreenPos			    : TEXCOORD6;
	float2 ExtraData				: TEXCOORD2;

};

struct vertexBillboardDepthOut
{
	float4 pos				: POSITION;
	float2 texCoord0		: TEXCOORD0;
};

vertexBillboardOut VS_Transform_Billboard(int i : INDEX)
{
	int index = i % 4;
	int vertex = i / 4;
	float4 center4;
	asm {
		vfetch	center4, vertex, position0
	};
	
	float4 vVertex = quadVerts[index];
	float4 vUV = quadUVs[index];
	
    vertexBillboardOut OUT;
    
	float4 vWorldPos = float4( center4.xyz + vVertex.xyz, 1.0f);
	    
    // Transform by the view projection matrix
	OUT.pos = mul(vWorldPos, gWorldViewProj);
    
	OUT.texCoord0.xy = vUV.xy;
	OUT.texCoord0.z = center4.w;
	

    return OUT;
}

vertexBillboardDepthOut VS_Transform_BillboardDepth(int i : INDEX)
{
	int index = i % 4;
	int vertex = i / 4;
	float4 center4;
	asm {
		vfetch	center4, vertex, position0
	};
	
	float4 vVertex = quadVerts[index];
	float4 vUV = quadUVs[index];
	
    vertexBillboardDepthOut OUT;
    
    float4 vWorldPos = BillboardVertex(vVertex.xyz, center4.xyz);
    	    
    // Transform by the view projection matrix
	OUT.pos = mul(vWorldPos, gWorldViewProj);
    
	OUT.texCoord0 = vUV.xy;
    
    return OUT;
}
#else
	

struct vertexBillboardIn {
    float4 pos				: POSITION;
    float4 texCoord0		: TEXCOORD0;
};
	
struct vertexBillboardOut {
    float4 pos				: POSITION;
    float3 texCoord0		: TEXCOORD0;

#if SHADOWS
	float4 shadowPixelPos0			: TEXCOORD1;
	float4 shadowPixelPos1			: TEXCOORD2;
	float4 shadowPixelPos2			: TEXCOORD3;
	float4 shadowPixelPos3			: TEXCOORD4;
	float4 WorldPos					: TEXCOORD5;
#endif
	float3 ScreenPos			    : TEXCOORD6;

};

struct vertexBillboardDepthOut
{
	float4 pos				: POSITION;
	float2 texCoord0		: TEXCOORD0;
};
	
vertexBillboardOut VS_Transform_Billboard(vertexBillboardIn IN)
{
    vertexBillboardOut OUT = (vertexBillboardOut)0;
    
    // Compute rotation basis matrix
    float4 vWorldPos = float4( IN.pos.xyz,1.0f); 
	    
    // Transform by the view projection matrix
    OUT.pos = mul(vWorldPos, gWorldViewProj);
    
    OUT.texCoord0.xy = IN.texCoord0.xy;
	OUT.texCoord0.z = 1.0f;
	
#if SHADOWS
	OUT.WorldPos = vWorldPos;
    OUT.shadowPixelPos0 = 0;
#endif    
    
    return OUT;
}

vertexBillboardDepthOut VS_Transform_BillboardDepth(vertexBillboardIn IN)
{
    vertexBillboardDepthOut OUT;
    
    // Compute rotation basis matrix
    float4 vWorldPos = float4( IN.pos.xyz, 1.0f);
	    
    // Transform by the view projection matrix
    OUT.pos = mul(vWorldPos, gWorldViewProj);
    
    OUT.texCoord0 = IN.texCoord0.xy;
    
    return OUT;
}
#endif
	
float4 PS_Textured_Billboard(vertexBillboardOut IN): COLOR
{

#if !__XENON
	float4 diffuse = tex2D( DiffuseBillboardSampler, IN.texCoord0.xy);
#else
	float LOD;
	float2 InTex = IN.texCoord0.xy * 0.5f;
	float2 texC = IN.texCoord0.xy;
	float4 diffuse;
	
     asm {
        getCompTexLOD2D LOD.x, InTex.xy, DiffuseBillboardSampler
					,AnisoFilter=max4to1

        setTexLOD LOD.x

        tfetch2D diffuse, texC.xy, DiffuseBillboardSampler,
				  UseComputedLOD=false, UseRegisterLOD=true
	};
#endif
	return float4( diffuse.xyz,  diffuse.w * saturate( 2.0f * IN.texCoord0.z  ));
}

float4 PS_TexturedNormalMapped_Billboard(vertexBillboardOut IN): COLOR
{

#if !__XENON
	float4 diffuse = tex2D( DiffuseBillboardSampler, IN.texCoord0.xy);
#else
	float LOD;
	float2 InTex = IN.texCoord0.xy * 0.5f;
	float2 texC = IN.texCoord0.xy;
	float4 diffuse;
	
     asm {
        getCompTexLOD2D LOD.x, InTex.xy, DiffuseBillboardSampler
					,AnisoFilter=max4to1

        setTexLOD LOD.x

        tfetch2D diffuse, texC.xy, DiffuseBillboardSampler,
				  UseComputedLOD=false, UseRegisterLOD=true
	};
#endif

	float3 normal = tex2D( NormalMapBillboardSampler, IN.texCoord0.xy).xyz * 2.0f - 1.0f;
	float3 lightColor =  saturate(dot( normalize( normal ), -gLightPosDir[0].xyz )) * gLightColor[0].xyz + gLightAmbient.xyz;
	return float4( diffuse.xyz * lightColor.xyz,  diffuse.w * saturate( 2.0f * IN.texCoord0.z  ));
}

float4 PS_DepthCutout(vertexBillboardDepthOut IN) : COLOR
{
	float4 diffuseColor = tex2D(DiffuseBillboardSampler, IN.texCoord0.xy);
	return diffuseColor;
}

float4 PS_BlendShadows(vertexBillboardOut IN) : COLOR
{
	float3 viewD =0.0f;// IN.WorldPos.xyz - gViewInverse[3].xyz;
	float camDistance = dot(viewD, -gViewInverse[2].xyz);
	
	float4 diffuseColor = tex2D(DiffuseBillboardSampler, IN.texCoord0.xy);
	
	return 1.0f;
}


technique draw 
{
	pass p0 
	{
		AlphaBlendEnable = false;	
		AlphaTestEnable = true;
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Transform_Billboard();
		PixelShader  = compile PIXELSHADER  PS_Textured_Billboard();
	}
	
	pass p1
	{
		// Shadow Pass
		ColorWriteEnable = 0;
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = false;
		AlphaTestEnable = true;
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Transform_BillboardDepth();
		PixelShader = compile PIXELSHADER	PS_DepthCutout();
	}
	
	pass p2 
	{
		AlphaBlendEnable = false;
		AlphaTestEnable = true;
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Transform_Billboard();
		PixelShader  = compile PIXELSHADER  PS_BlendShadows();
	}
	
	pass p3 
	{
		AlphaBlendEnable = false;
		AlphaTestEnable = true;
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Transform_Billboard();
		PixelShader  = compile PIXELSHADER  PS_Textured_Billboard();
	}
}


// PURPOSE
//	This technique is the default way imposters are created. 
//
technique ImpostorDefault
{
	pass p0
	{  
		AlphaBlendEnable = false;	
		AlphaTestEnable = true;
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Transform_Billboard();
		PixelShader  = compile PIXELSHADER  PS_Textured_Billboard();
	}
	pass p1
	{  
		//ColorWriteEnable = 0;
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = false;
		AlphaTestEnable = true;
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
//#if !__XENON
		VertexShader = compile VERTEXSHADER VS_Transform_Billboard();
		PixelShader = compile PIXELSHADER	PS_DepthCutout();
//#else
//		VertexShader = compile VERTEXSHADER VS_Transform_BillboardShadow();
//		PixelShader = compile PIXELSHADER	PS_DepthCutout();
//#endif
	}
}
// PURPOSE
//	This technique allows for creating a Normal mapped imposter. 
//
technique ImpostorNormalMapped
{
	pass p0			
	{  
		AlphaBlendEnable = false;	
		AlphaTestEnable = true;
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Transform_Billboard();
		PixelShader  = compile PIXELSHADER  PS_TexturedNormalMapped_Billboard();
	}
}
