// 
// speedtree/treedatamgr.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "treedatamgr.h"

#include "bank/bank.h"
#include "paging/rscbuilder.h"

#include "system/memory.h"
#include "rendererInterface.h"
#include "paging/rscbuilder.h"


namespace rage
{
template<class TREEDATA>
TreeDataMgr<TREEDATA> *TreeDataMgr<TREEDATA>::sm_Instance = NULL;

template<class TREEDATA>
IMPLEMENT_PLACE(TreeDataRequest<TREEDATA>);


#if __RESOURCECOMPILER
template<class TREEDATA>
typename TreeDataMgr<TREEDATA>::ResourceMode TreeDataMgr<TREEDATA>::m_ResourceMode = Mode_CreateAndStore;
#endif

#if __DECLARESTRUCT
template<class TREEDATA>
void TreeDataRequest<TREEDATA>::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(TreeDataRequest<TREEDATA>);
	STRUCT_FIELD_VP(pTreeData);
	STRUCT_FIELD(szTreeFile);
	STRUCT_FIELD(uHash);
	STRUCT_FIELD(nRenderTargetSize);
	STRUCT_FIELD(nSeed);
	STRUCT_FIELD(nReferenceCount);
	STRUCT_FIELD(fSize);
	STRUCT_FIELD(fSizeVariance);
	STRUCT_FIELD(bCreateBounds);
	STRUCT_FIELD(bNoBuffers);
	STRUCT_CONTAINED_ARRAY(pad);
	STRUCT_END();
}


#endif // !__DECLARESTRUCT




template<class TREEDATA>
TreeDataMgr<TREEDATA>::TreeDataMgr(int maxTreeDataCount)
	: pgDictionary<TreeDataRequest<TREEDATA> >(maxTreeDataCount)
	, m_UseAutoResource( false )
	, m_UseResource( false )
#if __BANK
	, m_CurrentTree(0)
	, m_ActiveTreeTypeCount(0)
	, m_CurrentTreeHash(0)
#endif // __BANK
{
	m_TreeDataRequests.Reserve(maxTreeDataCount);

#if __BANK
	m_ActiveTreeCountString[0] = '0';
	m_ActiveTreeCountString[1] = 0;
	m_CurrentTreeName[0] = 0;
	m_CurrentTreeHashString[0] = 0;
	m_CurrentTreeStatus[0] = 0;
#endif // __BANK
}



/* PURPOSE: Create the hash value that identifies a particular tree type.
 */

template<class TREEDATA>
TreeHash TreeDataMgr<TREEDATA>::CreateTreeTypeHash(const char *szTreeFile, int nSeed, float fSize, float fSizeVariance)
{
	char szTreeDataName[512];

	sprintf(szTreeDataName, "%s?S=%d,F=%.3f,V=%.3f", szTreeFile, nSeed, fSize, fSizeVariance);

	Assertf(strlen(szTreeDataName) < sizeof(szTreeDataName) - 1, "The tree data filename '%s' is WAY too long and has just successfully trashed the stack. Congratulations.");

	// Get the hash value
	return pgDictionary<TreeDataRequest<TREEDATA> >::ComputeHash(szTreeDataName);
}


/* PURPOSE: Request a certain tree data with specific parameters. If this particular
 * tree type is already in memory, a pointer will be returned and its reference
 * count will be incremented. If not, the tree will be loaded from memory.
 *
 * PARAMS:
 * szTreeFile - File name of the tree type to be loaded. This may be the just the file
 * or the full pathname.
 *
 * nRenderTargetSize - Render target size to set for this tree.
 *
 * puHashValue - If not NULL, this pointer will receive the hash value that the tree
 * is stored under in the dictionary.
 */

template<class TREEDATA>
TREEDATA *TreeDataMgr<TREEDATA>::RequestTreeData(const char *szTreeFile, int nRenderTargetSize, int nSeed, float fSize, float fSizeVariance, TreeHash *puHashValue, bool bCreateBounds, bool bOfflineCreation)
{
	// Create a string for this tree data - it's not just the file itself, it's also the parameters
	// with which we create it.
	TreeHash uHash = CreateTreeTypeHash(szTreeFile, nSeed, fSize, fSizeVariance);

	// If the user wants to know the hash value, relay that information.
	if (puHashValue)
	{
		*puHashValue = uHash;
	}	

	// Is it already in the dictionary?
	TreeDataRequest<TREEDATA> *pRequest = pgDictionary<TreeDataRequest<TREEDATA> >::Lookup(uHash);

	if (pRequest)
	{
		// It's already, so just return it - but increment
		// the reference count.
		AddRef(uHash);		// TODO: Add an AddRef override to take a TreeDataRequest
		return pRequest->pTreeData;
	}

	// We have to load it.
	Assertf(m_TreeDataRequests.GetCount() < m_TreeDataRequests.GetCapacity(), "Too many tree requests (more than %d). The constructor of the speedTreeDataMgr wasn't given the right amount of trees.", m_TreeDataRequests.GetCapacity());

	// Store the request.
	TreeDataRequest<TREEDATA> &request = m_TreeDataRequests.Append();

	char buffer[128];

//	ASSET.FullReadPath(request.szTreeFile, sizeof(request.szTreeFile), szTreeFile, "");
	ASSET.FullReadPath(buffer, sizeof(buffer), szTreeFile, "");
	request.szTreeFile = buffer;

	//request.szTreeFile = szTreeFile;
	request.nRenderTargetSize = nRenderTargetSize;
	request.nSeed = nSeed;
	request.fSize = fSize;
	request.fSizeVariance = fSizeVariance;
	request.bCreateBounds = bCreateBounds;
	request.bNoBuffers = bOfflineCreation;
	request.nReferenceCount = 0;
	request.pTreeData = NULL;

	AddEntry(uHash, &request);

	AddRef(request);

	return GetTree(request);
}

template<class TREEDATA>
TreeHash TreeDataMgr<TREEDATA>::RegisterTreeData(const char *szTreeFile, int nRenderTargetSize, int nSeed, float fSize, float fSizeVariance, bool bCreateBounds, bool bNoBuffers)
{
	// Create a string for this tree data - it's not just the file itself, it's also the parameters
	// with which we create it.
	TreeHash uHash = CreateTreeTypeHash(szTreeFile, nSeed, fSize, fSizeVariance);

	// Do we already have a tree like that?
	if (pgDictionary<TreeDataRequest<TREEDATA> >::Lookup(uHash))
	{
		// Yep, already in.
		return uHash;
	}

	Assertf(m_TreeDataRequests.GetCount() < m_TreeDataRequests.GetCapacity(), "Too many tree requests (more than %d). The constructor of the speedTreeDataMgr wasn't given the right amount of trees.", m_TreeDataRequests.GetCapacity());

	// Store the request.
	TreeDataRequest<TREEDATA> &request = m_TreeDataRequests.Append();

	char buffer[128];

	//	ASSET.FullReadPath(request.szTreeFile, sizeof(request.szTreeFile), szTreeFile, "");
	ASSET.FullReadPath(buffer, sizeof(buffer), szTreeFile, "");
	request.szTreeFile = buffer;

//	request.szTreeFile = szTreeFile;
	request.uHash = uHash;
	request.nRenderTargetSize = nRenderTargetSize;
	request.nSeed = nSeed;
	request.fSize = fSize;
	request.fSizeVariance = fSizeVariance;
	request.bCreateBounds = bCreateBounds;
	request.bNoBuffers = bNoBuffers;
	request.nReferenceCount = 0;
	request.pTreeData = NULL;

	AddEntry(uHash, &request);

	return uHash;
}

template<class TREEDATA>
int TreeDataMgr<TREEDATA>::AddRef(TreeHash uHash)
{
	// Find the tree.
	TreeDataRequest<TREEDATA> *request = pgDictionary<TreeDataRequest<TREEDATA> >::Lookup(uHash);

	Assertf(request, "AddRef() called on tree hash %x, but this tree has never been registered with RegisterTreeType.", uHash);

	if (request)
	{
		return AddRef(*request);
	}

	return -1;
}

template<class TREEDATA>
int TreeDataMgr<TREEDATA>::AddRef(TreeDataRequest<TREEDATA> &request)
{
	int refCount = ++request.nReferenceCount;

	if (refCount == 1)
	{
		// This tree has just been requested. We should load it now.
		Assertf(!request.pTreeData, "Tree type %s (hash %x) already has tree data in memory even though it was only just requested. Did something else secretly load the tree data?", request.szTreeFile.c_str(), request.uHash);
		request.pTreeData = LoadTreeType(request);

#if __BANK
		m_ActiveTreeTypeCount++;
		sprintf(m_ActiveTreeCountString, "%d", m_ActiveTreeTypeCount);
#endif // __BANK
	}

	return refCount;
}

template<class TREEDATA>
int TreeDataMgr<TREEDATA>::Release(TreeHash uHash)
{
	// Find the tree.
	TreeDataRequest<TREEDATA> *request = pgDictionary<TreeDataRequest<TREEDATA> >::Lookup(uHash);

	Assertf(request, "Release() called on tree hash %x, but this tree has never been registered with RegisterTreeType.", uHash);

	if (request)
	{
		int refCount = --request->nReferenceCount;

		Assertf(refCount >= 0, "Unbalanced AddRef/Release for tree type %x", uHash);

		if (refCount == 0)
		{
			// Delete this tree. Nobody wants it anymore.
			// TODO: Add a delay?
			if (request->pTreeData)
			{
				DeleteTreeType(request->pTreeData);
				request->pTreeData = NULL;
			}

#if __BANK
			m_ActiveTreeTypeCount--;
			sprintf(m_ActiveTreeCountString, "%d", m_ActiveTreeTypeCount);
#endif // __BANK
		}

		return refCount;
	}

	return -1;
}

template<class TREEDATA>
TREEDATA *TreeDataMgr<TREEDATA>::GetTree(TreeHash uHash)
{
	// Find the tree.
	TreeDataRequest<TREEDATA> *request = pgDictionary<TreeDataRequest<TREEDATA> >::Lookup(uHash);

	Assertf(request, "Release() called on tree hash %x, but this tree has never been registered with RegisterTreeType.", uHash);

	if (request)
	{
		return GetTree(*request);
	}

	return NULL;
}

template<class TREEDATA>
TREEDATA *TreeDataMgr<TREEDATA>::GetTree(TreeDataRequest<TREEDATA> &request)
{
	//Assertf(request->pTreeData, "Can't find tree data for tree hash %x (ref count is %d). Maybe AddRef() has never been called for it?", uHash, request->nReferenceCount);

	return request.pTreeData;
}

template<class TREEDATA>
TREEDATA *TreeDataMgr<TREEDATA>::LoadTreeType(const TreeDataRequest<TREEDATA> &request)
{
	TREEDATA *pResult;

	// We have to load it.
	if ( m_UseResource ) // simplest and fastest
	{
		char	szTreeResourceFile[256];
		safecpy( szTreeResourceFile, (const char *) request.szTreeFile, sizeof(szTreeResourceFile) );

		char* ptr = strchr( szTreeResourceFile,'.');
		if ( ptr)
		{
			*ptr = '\0';  // remove extension
		}

		// load the resourced file
		pgRscBuilder::Load( pResult, szTreeResourceFile, "#st", TREEDATA::RORC_VERSION);

		Assertf(pResult, "Error loading resourced tree %s", szTreeResourceFile);
	}
	else
	{
#if __RESOURCECOMPILER
		if (  m_UseAutoResource  )
		{
			pgRscBuilder::BeginBuild(true, 16 * 1024 * 1024,0,0);
			//bOfflineCreation = true;
		}
#endif // __RESOURCECOMPILER

		pResult = rage_new TREEDATA();
		pResult->SetRenderTargetSize(request.nRenderTargetSize);

		char base[256];
		safecpy(base, fiAssetManager::FileName(request.szTreeFile), sizeof(base));

		char path[256];
		fiAssetManager::RemoveNameFromPath(path, 256, request.szTreeFile);

		// Move into the folder (unless there is no folder in the first place)
		if( strcmp(path, request.szTreeFile) )
		{
			ASSET.PushFolder(path);
		}

		// Load the tree.
		if (!pResult->Create(request.szTreeFile, request.nSeed, request.fSize, request.fSizeVariance, request.bCreateBounds, request.bNoBuffers) )
		{
			Assertf(0, "Error creating tree %s (hash %x)", (const char *) request.szTreeFile, request.uHash);
		}

#if __RESOURCECOMPILER
		if (  m_UseAutoResource  )
		{
			pgRscBuilder::EndBuild(pResult);
			pgRscBuilder::BeginBuild(true, 16 * 1024 * 1024,0,0);

			pResult = rage_new TREEDATA();
			pResult->SetRenderTargetSize(request.nRenderTargetSize);

			// Load the tree again.
			if (pResult->Create(request.szTreeFile, request.nSeed, request.fSize, request.fSizeVariance, request.bCreateBounds, request.bNoBuffers))
			{
				datTypeStruct treeStruct;
				pResult->DeclareStruct( treeStruct );
				pgRscBuilder::SaveBuild(request.szTreeFile,"#st", TREEDATA::RORC_VERSION);
				delete pResult;
				pgRscBuilder::EndBuild(pResult);

				pgRscBuilder::Load(pResult, request.szTreeFile, "#st", TREEDATA::RORC_VERSION );
			}
			else
			{
				// Tree creation failed
				delete pResult;
				pResult = NULL;
			}
		}
#endif // __RESOURCECOMPILER


		if( strcmp(path, request.szTreeFile) )
		{
			ASSET.PopFolder();
		}
	}

	return pResult;
}

template<class TREEDATA>
void TreeDataMgr<TREEDATA>::DeleteTreeType(TREEDATA *pTreeData)
{
	delete pTreeData;
}


template<class TREEDATA>
void TreeDataMgr<TREEDATA>::InitClass(int maxTreeDataCount)
{
	Assert(!sm_Instance);
	sm_Instance = rage_new TreeDataMgr<TREEDATA>(maxTreeDataCount);
}

template<class TREEDATA>
void TreeDataMgr<TREEDATA>::ShutdownClass()
{
	Assert(sm_Instance);
	delete sm_Instance;
	sm_Instance = NULL;
}

template<class TREEDATA>
void TreeDataMgr<TREEDATA>::InitFromResource(const char *szRscFile, const char *szRscExt)
{
	AssertMsg(!sm_Instance, "The main speed tree data manager has already been instantiated");

	TreeDataMgr<TREEDATA> *manager;
	
	pgRscBuilder::Load(manager, szRscFile, szRscExt, RORC_VERSION);

	if (!manager)
	{
		Errorf("Error loading speed tree manager resource '%s'", szRscFile);
	}

	sm_Instance = manager;
}

struct TemporaryHack
{
	// This will be gone with the new manager system. Stay tuned.
	Vector3 sunLightDirection;
	TreeRenderer *renderer;
};


template<class TREEDATA>
void TreeDataMgr<TREEDATA>::RenderAllBillboardTextures(TreeRenderer* renderer, Vector3::Vector3Param sunLightDirection)
{
	TemporaryHack hack;

	hack.sunLightDirection = sunLightDirection;
	hack.renderer = renderer;
	ForAll(RenderBillboardTextureCB, &hack);
}

template<class TREEDATA>
bool TreeDataMgr<TREEDATA>::RenderBillboardTextureCB(TreeDataRequest<TREEDATA> &pData, TreeHash UNUSED_PARAM(uHash), void *pRenderer)
{
	TemporaryHack *hack = (TemporaryHack *) pRenderer;
	TREEDATA *RESTRICT treeData = pData.pTreeData;

	if (treeData)
	{
		hack->renderer->RenderTreeBillboardTexture(treeData, true);
		hack->renderer->RenderTreeBillboardTexture(treeData, true, true, true, &hack->sunLightDirection );
	}

	return true;
}

#if __BANK

template<class TREEDATA>
void TreeDataMgr<TREEDATA>::AddWidgets(bkBank &bank)
{
	bank.PushGroup("tree Manager");
	bank.AddText("Active Trees", m_ActiveTreeCountString, sizeof(m_ActiveTreeCountString), true);
	bank.AddSlider("Current Tree", &m_CurrentTree, 0, 100, 1.0f, datCallback(MFA(TreeDataMgr<TREEDATA>::UpdateCurrentTree), this));
	bank.AddText("Tree Name", m_CurrentTreeName, sizeof(m_CurrentTreeName), true);
	bank.AddText("Status", m_CurrentTreeStatus, sizeof(m_CurrentTreeStatus), true);
	bank.AddText("Hash Value", m_CurrentTreeHashString, sizeof(m_CurrentTreeHashString), true);
	bank.PopGroup();
}

template<class TREEDATA>
void TreeDataMgr<TREEDATA>::UpdateCurrentTree()
{
	if (m_CurrentTree >= m_TreeDataRequests.GetCount() || m_CurrentTree < 0)
	{
		safecpy(m_CurrentTreeName, "** INVALID TREE**", sizeof(m_CurrentTreeName));
		m_CurrentTreeStatus[0] = 0;
		m_CurrentTreeHashString[0] = 0;
	}
	else
	{
		const TreeDataRequest<TREEDATA> &request = m_TreeDataRequests[m_CurrentTree];
		safecpy(m_CurrentTreeName, (const char *) request.szTreeFile, sizeof(m_CurrentTreeName));
		sprintf(m_CurrentTreeHashString, "%08x", request.uHash);
		
		if (request.nReferenceCount > 0)
		{
			sprintf(m_CurrentTreeStatus, "%s (%d requests)", (request.pTreeData) ? "RESIDENT" : "requested (streaming or broken)", request.nReferenceCount);
		}
		else
		{
			safecpy(m_CurrentTreeStatus, (request.pTreeData) ? "!!RESIDENT BUT NOT REQUESTED!!" : "unloaded", sizeof(m_CurrentTreeStatus));
		}
	}
}

#endif // __BANK


#if __RESOURCECOMPILER

template<class TREEDATA>
void TreeDataMgr<TREEDATA>::CreateDictionaryFromRequests()
{
	SetResourceMode(Mode_CreateAndStore);

	for (int x=0; x<m_TreeDataRequests.GetCount(); x++)
	{
		const TreeDataRequest<TREEDATA> &request = m_TreeDataRequests[x];

		RequestTreeData(request.szTreeFile, request.nRenderTargetSize, request.nSeed, request.fSize, request.fSizeVariance, NULL, true, request.bCreateBounds);
	}
}

#endif // __RESOURCECOMPILER


//explicit instaniation
template class TreeDataMgr<instanceTreeData>;

} // namespace rage
