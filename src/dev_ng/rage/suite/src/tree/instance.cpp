// 
// speedtree/instance.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tree/instance.h"
#include "physics/simulator.h"
#include "data/safestruct.h"

namespace rage
{

TreeInstance::TreeInstance()
{
	m_vPosition.Zero();
	m_fLOD = 0.0f;
	m_uFlags = FLAG_VISIBLE | FLAG_VALID;

#if SPEEDTREE_PHYSICS
	m_pPhysInst = 0;
#endif

#if SPEEDTREE_GIZMOS
	m_pGizmo = rage_new gzTranslation(m_vPosition, Functor0::NullFunctor(), MakeFunctor(*this, &TreeInstance::RecomputeMatrices));
	m_pGizmo->Deactivate();
#endif
}

IMPLEMENT_PLACE(TreeInstance);
TreeInstance::TreeInstance(datResource&)
{
#if SPEEDTREE_PHYSICS
	if( m_pPhysInst )
	{
		PHSIM->AddFixedObject(m_pPhysInst);
	}
#endif

#if SPEEDTREE_GIZMOS
	m_pGizmo = rage_new gzTranslation(m_vPosition, Functor0::NullFunctor(), MakeFunctor(*this, &TreeInstance::RecomputeMatrices));
	m_pGizmo->Deactivate();
#endif
}

#if __DECLARESTRUCT
void TreeInstance::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(TreeInstance)
		SSTRUCT_FIELD( TreeInstance, m_vPosition)
		SSTRUCT_FIELD( TreeInstance, m_fLOD)
		SSTRUCT_FIELD( TreeInstance, m_uLodLevels)
		SSTRUCT_FIELD( TreeInstance, m_uFlags)
		SSTRUCT_FIELD( TreeInstance, m_uPad0)
		SSTRUCT_FIELD( TreeInstance, m_fadeOut)
#if SPEEDTREE_PHYSICS
		SSTRUCT_FIELD( TreeInstance, m_pPhysInst)
#endif

#if SPEEDTREE_GIZMOS
		SSTRUCT_FIELD_VP( TreeInstance, m_pGizmo)
#endif

#if !SPEEDTREE_PHYSICS && !SPEEDTREE_GIZMOS
		SSTRUCT_FIELD( TreeInstance, m_Pad1)
#endif

#if SPEEDTREE_PHYSICS && !SPEEDTREE_GIZMOS
		SSTRUCT_FIELD( TreeInstance, m_Pad2)
		SSTRUCT_FIELD( TreeInstance, m_Pad3)
		SSTRUCT_FIELD( TreeInstance, m_Pad4)
#endif

	SSTRUCT_END( TreeInstance)
}
#endif


TreeInstance::~TreeInstance()
{
#if SPEEDTREE_PHYSICS
	if ( atSharedGetRefCount( m_pPhysInst ) == 1 )
	{
		PHSIM->DeleteObject( m_pPhysInst->GetLevelIndex() );
	}
#endif
}

void TreeInstance::Copy( const TreeInstance& other )
{
	m_vPosition = other.m_vPosition;
	m_fLOD = other.m_fLOD;
	m_uLodLevels = other.m_uLodLevels;
	m_uFlags = other.m_uFlags;
	m_fadeOut = other.m_fadeOut;
#if SPEEDTREE_PHYSICS
	m_pPhysInst = other.m_pPhysInst;
#endif

#if SPEEDTREE_GIZMOS
	if ( other.m_pGizmo )
	{
		m_pGizmo = rage_new gzTranslation( *other.m_pGizmo);
	}
#endif
}

void TreeInstance::CreatePhysicsInstance( phArchetype* pArch)
{
	if( pArch )
	{
#if SPEEDTREE_PHYSICS
		if( m_pPhysInst )
		{
			m_pPhysInst->Init(*pArch, GetMatrix34());
		}
		else
		{
			m_pPhysInst = rage_new phInst();
			m_pPhysInst->Init(*pArch, GetMatrix34());

			PHSIM->AddFixedObject(m_pPhysInst);
		}
#endif
	}
}


int DiscreetLod( int NumLevels, float fLodLevel )
{
	short sLevel = 0;
	// -1.0f passed in means to use the current interval value
	Assert(fLodLevel != -1.0f);

	int nNumLodLevels = NumLevels;
	sLevel = static_cast<short>((1.0f - fLodLevel) * nNumLodLevels);
	if (sLevel == nNumLodLevels)
		sLevel--;

	Assert(sLevel >= 0 && sLevel < NumLevels);
	return sLevel;
}

void TreeInstance::SetLod(const Vector4& lods, float fLod)
{
	m_fLOD = fLod;
/*	CSpeedTreeRT* pSpeedTree = pTreeData->GetSpeedTreeRT();

	Assert(pSpeedTree->GetDiscreteBranchLodLevel(fLod) < 16);
	Assert(pSpeedTree->GetDiscreteFrondLodLevel(fLod) < 16);
	Assert(pSpeedTree->GetDiscreteLeafLodLevel(fLod) < 16);

	m_uLodLevels = (unsigned char) (pSpeedTree->GetDiscreteBranchLodLevel(fLod) | (pSpeedTree->GetDiscreteFrondLodLevel(fLod) << 4));
	m_uFlags &= 0xf0;
	m_uFlags |= pSpeedTree->GetDiscreteLeafLodLevel(fLod); 
*/
	Vector4 Lods = lods;

	m_uLodLevels = (unsigned char) DiscreetLod( (int)Lods.x, fLod ) | ( (unsigned char)DiscreetLod( (int)Lods.y, fLod ) << 4);
	m_uFlags &= 0xf0;
	m_uFlags |= (unsigned char) DiscreetLod( (int)Lods.z, fLod ); 
}

void TreeInstance::SetLod( const Vector4& lods, Vector4::Vector4Param vLod)
{
	Vector4 vDiscreetLods = lods;

	Vector4 vOneMinusLod = VECTOR4_IDENTITY - vLod;
	Vector4 vLevels = vOneMinusLod * vDiscreetLods;
	Vector4 vIntLevels = vLevels;

	vIntLevels.FloatToInt();
	vDiscreetLods.FloatToInt();

	Vector4 vCmp = vIntLevels.IsEqualIV(vDiscreetLods);
	Vector4 vSub = VECTOR4_IDENTITY & vCmp;
	vLevels -= vSub;

	vLevels.FloatToInt();

	vLevels &= VEC4_ANDW;
	vLevels |= (VEC4_ONEW * vLod);

	Assert(vLevels.ix != -1);
	Assert(vLevels.iy != -1);
	Assert(vLevels.iz != -1);

	Assert(vLevels.ix < 16);
	Assert(vLevels.iy < 16);
	Assert(vLevels.iz < 16);

	m_fLOD = Vector4(vLod).w;

	m_uLodLevels = (unsigned char) (vLevels.ix | (vLevels.iy << 4));
	m_uFlags &= 0xf0;
	m_uFlags |= vLevels.iz;
}

#if SPEEDTREE_GIZMOS
void TreeInstance::ActivateGizmo()
{
	m_pGizmo->Activate();
}

void TreeInstance::DeactivateGizmo()
{
	m_pGizmo->Deactivate();
}
#endif

} // namespace rage
