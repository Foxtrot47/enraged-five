// 
// speedtree/forest.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPEEDTREE_FOREST_H
#define SPEEDTREE_FOREST_H

#include "tree/treedata.h"
#include "tree/treedatamgr.h"
#include "tree/instance.h"

#include "atl/array.h"
#include "data/struct.h"
#include "data/base.h"
#include "grcore/viewport.h"
#include "grcore/viewport_inline.h"
#include "paging/dictionary.h"

namespace rage
{

class grcViewport;
class TreeRenderer;


// This structure contains all the debugging information for a
// TreeForest object.
struct TreeForestDebugInfo
{
#if __BANK
	Vector3											m_vAddTreePos;
	char											m_szAddTreeType[16];

	bool											m_bActivateGizmos;
	bool											m_bGizmosWereActive;

	int												m_nLoadSeed;
	float											m_fLoadSize;
	float											m_fLoadSizeVar;

	int												m_nTreeTypeCount;
#endif
};

// PURPOSE: This is a little helper class that is basically just
// a fixed-size string that can be used inside an atArray.
struct speedTreeDebugName
{
	enum
	{
		TreeDebugNameLength = 32
	};

	// The actual name.
	char	Name[TreeDebugNameLength];

	speedTreeDebugName()	{ Name[0] = 0;	}

	speedTreeDebugName(const speedTreeDebugName &c)
	{
		safecpy(Name, c.Name, sizeof(Name));
	}

	speedTreeDebugName &operator =(const speedTreeDebugName &c)
	{
		safecpy(Name, c.Name, sizeof(Name));
		return *this;
	}

	// PURPOSE: Assign a name to this object.
	void SetDebugName(const char *treeName)
	{
		safecpy(Name, treeName, sizeof(Name));
	}

	// RETURNS: The debug name of this object.
	const char *GetDebugName() const
	{
		return Name;
	}

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s)
	{
		STRUCT_BEGIN(speedTreeDebugName);
		STRUCT_CONTAINED_ARRAY(Name);
		STRUCT_END();
	}
#endif
};


//=============================================================================
// TreeForestImp
//
// PURPOSE
//   TreeForestImp manages a forest of trees loaded from one or more speedtree
//	stf files.
template<class TREEDATA>
class TreeForestImp : public pgBase
{
public:
	TreeForestImp(const char* szWindFile = 0, bool noWind  = false, int maxTreeTypes = 64 );
	TreeForestImp(datResource&);
	~TreeForestImp();

	enum
	{
		RORC_VERSION = 5,
	};

	DECLARE_PLACE(TreeForestImp);

	// PURPOSE: Set the size of the render targets used for tree billboards.
	// PARAMS:
	//	nSize - The width and height of the render targets
	void SetRenderTargetSize(int nSize)											{ m_nRenderTargetSize = nSize; }

	// PURPOSE: Load forest information from a .stf file
	// PARAMS:
	//	szForestFile - A string containing the path of the file to load
	// RETURNS:	true on success, false on failure
	bool Load(const char* szForestFile);

	// PURPOSE: Load a forest from the rdr2 xml format
	// PARAMS:
	//	szRDR2File - A string containing the path of the file to load
	//	locateGround - Will find the ground using a down hit
	// RETURNS: true on success, false on failure

	bool LoadFromRDR2File(const char* szRDR2File, bool locateGround = false );

	// PURPOSE: Load a tree from a .spt file
	// PARAMS:
	//	szTreeFile - A string containing the path of the tree file to load
	// RETURNS: An integer containing the index of the loaded tree, -1 if there is an error
	int LoadTree(const char* szTreeFile, int nSeed = 0, float fSize = 1.0f, float fSizeVariance = 0.0f, bool bCreateBounds = true, bool bOfflineCreation = false);

	//
	// PURPOSE
	//	Accessor for a specific tree data
	// PARAMS
	//	i - which tree data to access
	// RETURNS
	//	returns a reference to the requested tree data
	//
	inline TREEDATA& GetTreeData(int i);

	// RETURNS: TRUE if the tree with this particular index is resident in memory, FALSE otherwise.
	bool IsTreeDataResident(int i) const	{ return m_Trees[i] != NULL; }

	//
	// PURPOSE
	//	Accessor for a specific tree data
	// PARAMS
	//	i - which tree data to access
	// RETURNS
	//	returns a reference to the requested tree data
	//
	inline const TREEDATA& GetTreeData(int i) const;

	// PURPOSE: Allocate space for instances
	// PARAMS:
	//	count - The number of instances to reserve space for
	void PreAllocateInstances(int nCount);

	void PreAllocateInstances(int type, int nCount);

	// PURPOSE: Create an instance of a tree
	// PARAMS:
	//	nTreeIndex - The index of the tree to create an instance of
	//	vPosition - The position of the instance
	//	fRotX - The rotation in the x axis in radians
	//	fRotY - The rotation in the y axis in radians
	//	fRotZ - The rotation in the z axis in radians
	// RETURNS: The TreeInstance object of the newly created instance
	TreeInstance& AddTreeInstance(int nTreeIndex, const Vector3& vPosition, float fRotX = 0.0f, float fRotY = 0.0f, float fRotZ = 0.0f, bool bFindFree = true);

	// PURPOSE: Remove the last instance of the specified tree
	// PARAMS:
	//	nTreeIndex - The index of the tree whos last instance is to be removed
	void RemoveLastTreeInstance(int nTreeIndex);

	// PURPOSE: Remove an instances of a tree from the forest
	// PARAMS:
	//	nTreeIndex - The index of the tree whose instance is to be removed
	//	nInstanceIndex - The index of the instance to be removed
	void RemoveTreeInstance(int nTreeIndex, int nInstanceIndex);

	// PURPOSE: Remove all instances at the specified position
	// PARAMS:
	//	vPosition - The position at which to remove any tree instances
	void RemoveInstance(const Vector3& vPosition);

	// PURPOSE: Remove all of the instances from the forest
	void RemoveAllInstances();

	// PURPOSE: Remove a tree and all of its instances from the forest
	// PARAMS:
	//	nTreeIndex - The index of the tree to remove
	// NOTES:
	//	This function does not re-order the tree indices
	void RemoveTree(int nTreeIndex);

	// PURPOSE: Remove all the trees and all of the instances from the forest
	void RemoveAllTrees();

	// PURPOSE: Get the index of the instance at the specified position
	// PARAMS:
	//	nTreeIndex - The index of the tree for the desired instance
	//	vPosition - The position of the desired instance
	// RETURNS: An integer containing the index of the instance of the tree at the specifed position. -1 if not found
	// NOTES:
	//	This function only searches the tree specified by nTreeIndex
	int FindTreeInstance(int nTreeIndex, const Vector3& vPosition) const;

	// PURPOSE: Get the index of the instance at the specified position
	// PARAMS:
	//	vPosition - The position of the desired instance
	//	nTreeIndex - OUT The index of the tree for the desired instance
	// RETURNS: An integer containing the index of the instance of the tree at the specifed position. -1 if not found
	// NOTES:
	//	This function searches all of the tree lists for the first instance at this position, the tree
	//	index is returned in nTreeIndex.
	int FindInstance(const Vector3& vPosition, int& nTreeIndex) const;

	// PURPOSE: Get a pointer to the instance at the specified index
	// PARAMS:
	//	nIndex - The absolute index of the desired instance
	//	nTreeIndex - OUT The index of the tree of the desired instance
	//	nInstanceIndex - OUT The local index of the desired isntance
	// RETURNS: A pointer to a TreeInstance object representing the specified instance. NULL if the instance is not found
	const TreeInstance* GetInstance(int nIndex, int& nTreeIndex, int& nInstanceIndex);

	// PURPOSE: Get a pointer to the instance at the specified index
	// PARAMS:
	//	nTreeIndex - The index of the tree whos instance is to be retreived
	//	nInstanceIndex - The index of the desired instance
	const TreeInstance* GetInstance(int nTreeIndex, int nInstanceIndex);

	// PURPOSE: Clears the visible instance list
	void ResetVisibleInstances();

	// PURPOSE: Remove all tree hashes of trees that have zero instances.
	virtual void RemoveUnusedTreeHashes();

	// PURPOSE: Update the forest
	// PARAMS:
	//	fTime - The amount of time elapsed since the beginning of time
	//	pViewport - The viewport to use for frustum culling
	// NOTES:
	//	This function updates speedwind.  This function also does visibility tests
	//	with the given viewport setting up the visibility and lods for each instance.
	void Update(float fTime, const grcViewport& pViewport);

	// PURPOSE: Update the shadow visisbilty list using the specified viewport
	// PARAMS:
	//	pLightViewport - The viewport used to determine visibility
	//	pCamViewport - The viewport used to determine lod
	void UpdateShadows(const grcViewport& pLightViewport, const grcViewport& pCamViewport);

	// PURPOSE: Render the forest
	// PARAMS:
	//	pRenderer - A pointer to a TreeRenderer object that will be used to render the trees
	void Draw(TreeRenderer& pRenderer, int nDrawPass = 0, int renderOnly = -1);

	// PURPOSE: Render the trees in the shadow visiblity list into the shadow map
	// PARAMS:
	//	pRenderer - A pointer to a TreeRenderer object that will be used to render the shadows
	void DrawShadows(TreeRenderer& pRenderer, int nDrawPass = 1, int renderOnly = -1);

	// PURPOSE: Generate billboard textures for all trees in the forest
	// PARAMS:
	//	pRenderer - A reference to a TreeRenderer object that will be used to render the trees
	void RenderAllTreeBillboards(TreeRenderer& pRenderer);

	// PURPOSE: Get the distance at which the billboards start being displayed
	// RETURNS: A float containing the near billboard distance
	float GetBillboardNearDistance() const								{ return m_fBillboardNearDistance.x; }

	// PURPOSE: Get the distance at which only billboards are displayed
	// RETURNS: A float containing the far billboard distance
	float GetBillboardFarDistance() const								{ return m_fBillboardFarDistance.x; }

	// PURPOSE: Set the distance at which the billboards start being displayed
	void SetBillboardNearDistance(float fDistance)						{ m_fBillboardNearDistance.Set(fDistance); }

	// PURPOSE: Set the distance at which only billboards are displayed
	void SetBillboardFarDistance(float fDistance)						{ m_fBillboardFarDistance.Set(fDistance); }

	// PURPOSE: Hook this forest up to a tree data manager. Use this if you are using an external tree data resource
	// rather than calling LoadTree(). DO NOT use this if you're using LoadTree().
	virtual void AttachToManager(TreeDataMgr<TREEDATA> &manager, atArray<u32> *treeTypeHashes = NULL);

	// PURPOSE: Get the number of tree types in this forest
	// RETURNS: An integer containing the number of tree types in this forest
	int GetTreeCount() const;	

	// PURPOSE: Get the total number of instances in this forest
	// RETURNS: An integer containing the number of instances in this forest
	int GetInstanceCount() const;

	// PURPOSE: Get the number of instances of a given tree
	// RETURNS: An integer containing the number of instances of a given tree in this forest
	int GetTreeInstanceCount(int nTreeIndex) const;

	// PURPOSE: Identify the tree type index in this forest by the tree type hash.
	int IdentifyTreeIndex(u32 uTreeHash) const;

	// PURPOSE: Set the debug name of a certain tree type.
	void SetTreeTypeDebugName(int nTreeIndex, const char *debugName)		{ m_TreeDebugNames[nTreeIndex].SetDebugName(debugName); }

	int Release() const { delete this; return 0; }	// Not true reference counting yet!

	void SetAlwaysRegenerateBillboards( bool regen ) { m_RegenerateAll = regen; }
	void SetNumBillboardsRegeneratePerFame( int num ) { FastAssert( num >=0 && num <=128); m_MaxBillboardsPerFrame = num; }


	void UseBillboardSizeSelection( bool val = true )		{ m_useBillboardSizeSelection = val; }
	void SetBillboardBlendRange( float range )				{ FastAssert( range >= 0.0f && range <= 1024.0f); m_billboardBlendRange= range; }
	void SetLightDirection( const Vector3& direction )		{ m_SunLightDirection = direction; }
#if __BANK


#if SPEEDTREE_GIZMOS
	void ActivateGizmos();
	void DeactivateGizmos();
#endif

	void LoadTreeWidget();
	void LoadResourceTreeWidget();
	void LoadRDR2ForestWidget();
	void AddInstanceWidget();

	void AddWidgets(class bkBank& bank);
#endif

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

protected:
	void SetupDraw(TreeRenderer& pRenderer);
	void CreateManager( int maxTreeTypes = 64 );

	// unused
	//static bool AttachToManagerCB(TREEDATA &treeData, u32 hash, void *forest);
	//bool AttachToManagerCB(TREEDATA &treeData, u32 hash);
	void RenderTreeBillboard( TREEDATA* tree , Matrix44* rockMatrices, Matrix44* rustleMatrices, TreeRenderer& pRenderer);
	
	template<class VisiblitySelector>
	__forceinline void TestAndAddInstance(int &nCount, TreeInstance &instance, VisiblitySelector &vis, Vector4::Vector4Param vTreeCullSphere, const grcViewport &pViewport, TreeInstance **&visibleTreeList, TREEDATA *treeData);
	
	template<class VisiblitySelector>
	void UpdateVisiblity( const rage::grcViewport &, VisiblitySelector vis );

protected:
	atArray<datRef<TREEDATA> >						m_Trees;
	Vector4											m_vWind;
	Vector4											m_fBillboardNearDistance;
	Vector4											m_fBillboardFarDistance;
	Vector3											m_SunLightDirection;		// should refactor this out
	atArray<u32>									m_TreeSourceHashes;
	atArray<speedTreeDebugName>						m_TreeDebugNames;
	atArray<atArray<TreeInstance> >			m_TreeInstances;
	atArray<atArray<datRef<TreeInstance> > >	m_VisibleTreeInstances;
	atArray<atArray<datRef<TreeInstance> > >	m_VisibleShadowTreeInstances;
	TreeForestDebugInfo *						m_pDebugInfo;			// UNDEFINED if __BANK is not defined

	int												m_nRenderTargetSize;
	int												m_MaxBillboardsPerFrame;
	float											m_billboardBlendRange;

	bool											m_RegenerateAll;
	bool											m_useBillboardSizeSelection;
	char											m_Pad[6];


	class CalculateImposterDistBasedFade
	{
		Vector4	m_farDistance;
		Vector4	m_nearDistance;
	public:
		CalculateImposterDistBasedFade( const Vector4& nearD, const Vector4& farD)
		{
			m_farDistance = farD;
			m_nearDistance = nearD;
		}

		void SetTexSize( int  /*texSize*/)	{}

		float GetFade( float /*width*/, float dist, float &flod )
		{
			Vector4 vZDist;
			vZDist.Set( dist );

			bool bBillboardOnly = m_farDistance.IsLessThan(vZDist);
			bool bBillboardActive = bBillboardOnly || m_nearDistance.IsLessThan(vZDist);

			float fade = 0.0f;

			vZDist.Min(vZDist, m_farDistance);
			Vector4 vLod = VECTOR4_IDENTITY - (vZDist / m_farDistance);
			flod = vLod.x;

			if ( bBillboardOnly)
			{

				fade = 1.0f;
			}
			else if ( bBillboardActive )
			{
				fade = 0.5f;
			}
			return fade;
		}

	};


	class CalculateImposterSizeBasedFade
	{
	public:
		CalculateImposterSizeBasedFade( float texRange ) 
			: m_texRange(texRange), m_invTexRange( 1.0f/texRange )
		{
			m_divisor1 = m_divisor2 = 1;
			m_texSize = 1;
			if ( grcViewport::GetCurrent() )
			{
				m_divisor1 = 2.0f *  grcViewport::GetCurrent()->GetTanHFOV();
			}
			m_screenWidth = static_cast<float>( GRCDEVICE.GetWidth() );
		}
		void SetTexSize( int texSize )	
		{
			m_texSize = static_cast<float>(  texSize );
			m_divisor2 = 1.0f/(  m_screenWidth * 0.5f - texSize); 
		}
		float GetFade( float width, float dist, float &flod )
		{
			float texRes;
			texRes =( m_screenWidth * width ) / ( dist * m_divisor1 );
			texRes += m_texRange * 0.5f;

			float fade = (  m_texSize  - texRes + m_texRange * 0.5f) * m_invTexRange; 
			flod =( texRes - m_texSize - m_texRange)  * m_divisor2; // max size should be specified-+
			flod = Clamp( flod, 0.0f, 1.0f );
			fade = Clamp( fade, 0.0f, 1.0f );
			return fade;
		}
	private:
		float m_divisor1;
		float m_divisor2;
		float m_texRange;
		float m_invTexRange;
		float m_texSize;
		float m_screenWidth;
	};
};

//
// Inlined functions
//
template<class TREEDATA>
inline TREEDATA& TreeForestImp<TREEDATA>::GetTreeData(int i)
{
	return *m_Trees[i];
}
template<class TREEDATA>
inline const TREEDATA& TreeForestImp<TREEDATA>::GetTreeData(int i) const
{
	return *m_Trees[i];
}

template<class TREEDATA>
template<class VisibilitySelector>
__forceinline void TreeForestImp<TREEDATA>::TestAndAddInstance(int &nCount, TreeInstance &instance, VisibilitySelector &vis, Vector4::Vector4Param vTreeCullSphere, const grcViewport &pViewport, TreeInstance **&visibleTreeList, TREEDATA *treeData)
{
	//if( instance.IsValid() )

	Vector4 vLocalCullSphere = vTreeCullSphere;
	Vector4 vPos;
	vPos.SetVector3(instance.GetPosition());
	vPos &= VEC4_ANDW;
	vLocalCullSphere.Add(vPos);
	vPos |= VEC4_ONEW;

	bool bVisible = (pViewport.IsSphereVisibleInline(VECTOR4_TO_VEC4V(vLocalCullSphere)) != cullOutside );
	if( bVisible )
	{
		Vector4 vNearPlane = VEC4V_TO_VECTOR4(pViewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_NEAR));
		Vector4 vZDist = vNearPlane.DotV(vPos);
		vZDist.Max(vZDist, VECTOR4_ORIGIN);

		float flod;
		float fade = vis.GetFade( 2.0f * Vector4(vTreeCullSphere).w, vZDist.x, flod);

		bool bBillboardOnly  = fade >= 1.0f;
		bool bBillboardActive = fade > 0.0f;

		instance.SetBillboardOnly(bBillboardOnly);
		instance.SetBillboardActive(bBillboardActive);

		if( !bBillboardOnly )
		{
			instance.SetLodInline(treeData->GetDiscreetLodLevels(), flod);
		}
		instance.SetFadeOut( fade );
		*(visibleTreeList++) = &instance;
		nCount++;
	}
	instance.SetVisible(bVisible);

//	PF_INCREMENT(TreesInScene);
}
typedef TreeForestImp<instanceTreeData>		TreeForest;


}		// namespace rage

#endif // SPEEDTREE_FOREST_H
