// 
// speedtree/renderer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tree/renderer.h"
#include "tree/instance.h"
#include "tree/treedata.h"

#include "grprofile/pix.h"
#include "system/xtl.h"

#include "system/performancetimer.h"
#include "grcore/device.h"
#include "grcore/vertexbuffer.h"
#include "grcore/indexbuffer.h"
#include "grcore/texture.h"
#include "grmodel/shader.h"
#include "grmodel/modelfactory.h"
#include "profile/profiler.h" 
#include "bank/bkmgr.h"
#include "bank/bank.h"

#if __D3D
#include <d3d9.h>
#include <d3dx9.h>
#endif

#if __XENON
#include "embedded_rage_billboard_nobump_fxl_final.h"
#include "embedded_rage_tree_fxl_final.h"
#include "embedded_rage_tree_leaves_fxl_final.h"
#elif __PPU
#include "embedded_rage_billboard_nobump_psn.h"
#include "embedded_rage_tree_leaves_psn.h"
#include "embedded_rage_tree_psn.h"
#elif __WIN32PC
#include "embedded_rage_billboard_nobump_win32_30.h"
#include "embedded_rage_tree_leaves_win32_30.h"
#include "embedded_rage_tree_win32_30.h"
#endif


namespace rageSpeedTreeRendererStats
{
	PF_PAGE(RAGESpeedTreeRenderer,"tree Renderer");

	PF_GROUP(Renderer);
	PF_LINK(RAGESpeedTreeRenderer,	Renderer);
	PF_TIMER(Branches,				Renderer);
	PF_TIMER(Fronds,				Renderer);
	PF_TIMER(Leaves,				Renderer);
	PF_TIMER(Billboards,			Renderer);
	PF_COUNTER(BillboardsDrawn,		Renderer);
}
using namespace rageSpeedTreeRendererStats;

namespace rage
{

static grmShader*	s_pDefaultBranchShader = 0;
static grmShader*	s_pDefaultFrondShader = 0;
static grmShader*	s_pDefaultLeafShader = 0;
static grmShader*	s_pDefaultBillboardShader = 0;

static const char*	s_szDefaultBranchShaderName = "rage_speedtree";
static const char*	s_szDefaultFrondShaderName = "rage_speedtree";
static const char*	s_szDefaultLeafShaderName = "rage_speedtree_leaves";


static const char*	s_szDefaultBillboardShaderName = "rage_billboard_nobump";

static const int	s_nDefaultBranchMatrixRegister = 120;
static const int	s_nDefaultFrondMatrixRegister = 120;
static const int	s_nDefaultLeafMatrixRegister =	240;
static const int	s_nDefaultBillboardMatrixRegister = 120;

const char*	TreeRenderer::sm_ImposterTechnique = "ImpostorDefault";


#if __BANK
bool s_DrawBillboards	=true;
bool s_DrawBranches		=true;
bool s_DrawFronds		=true;
bool s_DrawLeaves		=true;
bool s_DrawBounds		=false;
#endif

typedef struct _billboardVertex
{
	Vector4		vPosition;
	Vector4		vUV;
} billboardVertex;



//----------------------------------------------------------------
void CalculateBillboardAxias( const Vector3& vUp, Vector3& up, Vector3& across,float fHalfDimension, float fHeight)
{
	// Compute the view normal
	Matrix34 camMtx = MAT34V_TO_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());
	Vector3 viewRight;
	Vector3 viewUp;
	Vector3 viewNrm;
	viewNrm.Normalize( camMtx.c );

	// Compute the right vector
	viewRight.Cross( viewNrm, vUp);
	viewRight.Normalize();

	// Compute the new view normal
	viewNrm.Cross( vUp, viewRight);
	viewUp = vUp;

	Vector3 halfWidth;
	halfWidth.Set( fHalfDimension,  fHalfDimension, fHalfDimension);
	Vector3 height;
	height.Set( fHeight, fHeight, fHeight );

	up = viewUp * height;
	across =  halfWidth* viewRight;
}



void CalculateLeafAxias( const Vector3& vUp, Vector3& viewNrm, Vector3& viewUp, Vector3& viewRight )
{
	// Compute the view normal
	Matrix34 camMtx = MAT34V_TO_MATRIX34(grcViewport::GetCurrent()->GetCameraMtx());
	viewNrm.Normalize( camMtx.c );

	// Compute the right vector

	viewRight.Cross( viewNrm, vUp);
	viewRight.Normalize();

	// Compute the new view normal
	//viewNrm.Cross( vUp, viewRight);

	viewUp.Cross( viewRight, viewNrm );

}

speedTreeRenderer::speedTreeRenderer() 
	: m_useSubPixelAlpha( false ),
	m_pBranchShader(0),
	m_pFrondShader(0),
	m_pLeafShader(0),
	m_pBillboardShader(0),
	m_vUp( g_UnitUp )
{
	m_nBranchMatrixRegister = -1;
	m_nFrondMatrixRegister = -1;
	m_nLeafMatrixRegister = -1;
	m_nBillboardMatrixRegister = -1;
	m_nDrawPassIndex = 0;

	m_bBlendLeafLod = true;
	m_fCamDist = 5000.0f;

	m_nLeafAlphaOffset = 0;
	m_nBillboardLeafAlpha = -10;

	m_cBillboardBackgroundColor.Set(0, 0, 0, 0);
}

speedTreeRenderer::~speedTreeRenderer()
{
	delete m_pBranchShader;
	delete m_pFrondShader;
	delete m_pLeafShader;
	delete m_pBillboardShader;
	
	grmImposterShader::FreeVertexDeclaration();
}

void speedTreeRenderer::LoadDefaultShaders()
{
	LoadDefaultBranchShader();
	LoadDefaultFrondShader();
	LoadDefaultLeafShader();
	LoadDefaultBillboardShader();
}

void speedTreeRenderer::LoadDefaultBranchShader()
{
	if( !s_pDefaultBranchShader )
	{
		s_pDefaultBranchShader = grmShaderFactory::GetInstance().Create();
		s_pDefaultBranchShader->Load(s_szDefaultBranchShaderName);
	}

	SetBranchShader(s_pDefaultBranchShader);
	m_nBranchMatrixRegister = s_nDefaultBranchMatrixRegister;
}

void speedTreeRenderer::LoadDefaultFrondShader()
{
	if( !s_pDefaultFrondShader )
	{
		s_pDefaultFrondShader = grmShaderFactory::GetInstance().Create();
		s_pDefaultFrondShader->Load(s_szDefaultFrondShaderName);
	}

	SetFrondShader(s_pDefaultFrondShader);
	m_nFrondMatrixRegister = s_nDefaultFrondMatrixRegister;
}

void speedTreeRenderer::LoadDefaultLeafShader()
{
	if( !s_pDefaultLeafShader )
	{
		s_pDefaultLeafShader = grmShaderFactory::GetInstance().Create();
		s_pDefaultLeafShader->Load(s_szDefaultLeafShaderName);
	}

	SetLeafShader(s_pDefaultLeafShader);
	m_nLeafMatrixRegister = s_nDefaultLeafMatrixRegister;
}

void speedTreeRenderer::LoadDefaultBillboardShader()
{
	if( !s_pDefaultBillboardShader )
	{
		s_pDefaultBillboardShader = grmShaderFactory::GetInstance().Create();
		s_pDefaultBillboardShader->Load(s_szDefaultBillboardShaderName);
	}

	
	SetBillboardShader(s_pDefaultBillboardShader);
	
	m_nBillboardMatrixRegister = s_nDefaultBillboardMatrixRegister;

	grmImposterShader::InitVertexDeclaration();
}

void speedTreeRenderer::CleanupDefaultShaders()
{
	delete s_pDefaultBranchShader;
	s_pDefaultBranchShader = 0;
	
	delete s_pDefaultFrondShader;
	s_pDefaultFrondShader = 0;
	
	delete s_pDefaultLeafShader;
	s_pDefaultLeafShader = 0;
	
	delete s_pDefaultBillboardShader;
	s_pDefaultBillboardShader = 0;
}

void speedTreeRenderer::SetBranchShader(grmShader* pShader)
{
	if ( m_pBranchShader )
	{
		delete m_pBranchShader;
	}
	m_pBranchShader = pShader;

	m_evBranchTexture =			pShader->LookupVar("DiffuseTex");
	m_evBranchWindMatrices =	pShader->LookupVar("WindMatrices");

	m_evBranchWorldMatrix =				pShader->LookupVar("WorldMatrix");
	m_evBranchWorldInvTransposeMatrix =	pShader->LookupVar("WorldInverseTransposeMatrix");

	m_evBranchTreeCentre = pShader->LookupVar("TreeCentre", false);
	m_evBranchTreeAlpha = pShader->LookupVar("TreeAlpha", false);

	m_createBranchImposterTechnique  = pShader->LookupTechnique( sm_ImposterTechnique, false );
}

void speedTreeRenderer::SetFrondShader(grmShader* pShader)
{
	if ( m_pFrondShader )
	{
		delete m_pFrondShader;
	}
	m_pFrondShader = pShader;

	m_evFrondTexture =			pShader->LookupVar("DiffuseTex");
	m_evFrondWindMatrices =		pShader->LookupVar("WindMatrices");

	m_evFrondWorldMatrix =				pShader->LookupVar("WorldMatrix");
	m_evFrondWorldInvTransposeMatrix =	pShader->LookupVar("WorldInverseTransposeMatrix");

	m_evFrondTreeCentre = pShader->LookupVar("TreeCentre" , false);
	m_evFrondTreeAlpha = pShader->LookupVar("TreeAlpha" , false);

	m_createFrondImposterTechnique = pShader->LookupTechnique( sm_ImposterTechnique, false );
}

void speedTreeRenderer::SetLeafShader(grmShader* pShader)
{
	if ( m_pLeafShader )
	{
		delete m_pLeafShader;
	}
	m_pLeafShader = pShader;

	m_evLeafTexture =			pShader->LookupVar("DiffuseTex");
	m_evLeafTable =				pShader->LookupVar("LeafClusters");
//	m_evLeafRockMatrices =		pShader->LookupVar("LeafAngleMatrices");
	m_evLeafWindMatrices =		pShader->LookupVar("WindMatrices");
#if __XENON
	m_evLeafUVTable			= 	pShader->LookupVar("LeafUVTable", false );
#endif
	grcEffectVar up = pShader->LookupVar("UpVector", false);
	if (up != grcevNONE)
	{
		pShader->SetVar(up, m_vUp);
	}

	m_evLeafWorldMatrix =				pShader->LookupVar("LeafWorldMatrix");
	m_evLeafWorldInvTransposeMatrix =	pShader->LookupVar("LeafWorldInvTrans");

	m_evleafTreeCentre = pShader->LookupVar("TreeCentre", false);
	m_evLeafTreeAlpha = pShader->LookupVar("TreeAlpha", false);

	m_createLeafImposterTechnique = pShader->LookupTechnique( sm_ImposterTechnique, false );
}


void speedTreeRenderer::SetBillboardShader(grmShader* pShader)
{
	if ( m_pBillboardShader )
	{
		delete m_pBillboardShader;
	}
	m_pBillboardShader = pShader;

 	m_evBillboardTexture			=	pShader->LookupVar("DiffuseBillboardTex"); // DiffuseTex
	m_evNormalMapBillboardTexture	=	pShader->LookupVar("NormalMapBillboardTex",false); // DiffuseTex
	
	grcEffectVar up = pShader->LookupVar("UpVector", false);
	if (up != grcevNONE)
	{
		pShader->SetVar(up, m_vUp);
	}

	m_evBillboardWorldMatrices = pShader->LookupVar("WorldPositions", false );
	m_BillboardImposterTechnique = pShader->LookupTechnique( sm_ImposterTechnique, false );

	m_evbillboardTreeCentre = pShader->LookupVar("TreeCentre" , false);
}

void speedTreeRenderer::SetUpVector(const Vector3& vUp)
{
	m_vUp = vUp;

	grcEffectVar up = m_pLeafShader->LookupVar("UpVector", false);
	if (up != grcevNONE)
	{
		m_pLeafShader->SetVar(up, m_vUp);
	}

	up = m_pBillboardShader->LookupVar("UpVector", false);
	if (up != grcevNONE)
	{
		m_pBillboardShader->SetVar(up, m_vUp);
	}
}

void speedTreeRenderer::SetWindMatrices(const Matrix44* pWindMatrices, int nWindMatrixCount, const Matrix44* /*pRockMatrices*/, int /*nRockMatrixCount*/)
{
	m_pBranchShader->SetVar(m_evBranchWindMatrices, pWindMatrices, nWindMatrixCount);
	m_pFrondShader->SetVar(m_evFrondWindMatrices, pWindMatrices, nWindMatrixCount);
	m_pLeafShader->SetVar(m_evLeafWindMatrices, pWindMatrices, nWindMatrixCount);
	//m_pLeafShader->SetVar(m_evLeafRockMatrices, pRockMatrices, nRockMatrixCount);
}

void speedTreeRenderer::DrawShadowTrees( instanceTreeData* pTreeData, TreeInstance** pInstanceList, int nInstanceCount, Vector3* viewDirection )
{
	// Setup the world matrix to idenity so that WorldViewProj in the shaders is actually ViewProj
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
	

	// Draw the Billboards
	DrawBillboards( *pTreeData, pInstanceList, nInstanceCount, true , viewDirection);
}

void speedTreeRenderer::DrawTrees(instanceTreeData* pTreeData, TreeInstance** pInstanceList, int nInstanceCount)
{

#ifdef UPVECTOR_POS_Z
	Assert( g_UnitUp == Vector3( 0.0f, 0.0f, 1.0f ));
#else
	Assert( g_UnitUp == Vector3( 0.0f, 1.0f, 0.0f ));
#endif


	// Setup the world matrix to idenity so that WorldViewProj in the shaders is actually ViewProj
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
	

	if ( m_useSubPixelAlpha )
	{
		grcState::SetAlphaToMask( true );
		//grcState::SetAlphaToMaskOffsets( grcamoSolid);
		grcState::SetAlphaToMaskOffsets( grcamoDithered);
	}
	else
	{
		grcState::SetAlphaTest( true );
	}

	// TODO : create matrix and draw it
	// for each instance Draw
	for ( int i = 0; i < nInstanceCount; i++ )
	{
		//pTreeData->m_Drawable->Draw()
	}

	grcState::SetAlphaTest( !m_useSubPixelAlpha); // if using subpixel alpha we don't need it
	//PIXBegin(0, "Leaves");

	
	if ( m_useSubPixelAlpha )
	{
		grcState::SetAlphaToMask( false );
	}
	grcState::SetAlphaTest( false); 

	//PIXBegin(0, "Billboards");
	// Draw the Billboards
	DrawBillboards( *pTreeData, pInstanceList, nInstanceCount);
	//PIXEnd();



	//PIXEnd();

#if __BANK
	if ( s_DrawBounds )
	{
		for( int i = 0; i < nInstanceCount; i++ )
		{
			Vector4 sphere = pTreeData->GetCullSphere();
			sphere.AddVector3( pInstanceList[i]->GetPosition() );

			grcDrawSphere(sphere.w, Vector3(sphere.x, sphere.y, sphere.z), 16, true );
		}
	}

#endif
}
Vector4 GetHighDetailFade( TreeInstance* inst, bool useFading )
{
	Vector4 fade;
	fade.x = Max( inst->GetFadeOut() * 2.0f - 1.0f, 0.0f );
	fade.x = 1.0f - fade.x;
	Assert( fade.x >= 0.0f && fade.x <= 1.0f );
	fade.x = ( !useFading ) ? 1.0f : fade.x;
	fade.SplatX();
	return fade;
}


inline int RoundToNearestInt(float fVal)
{
	return (int)(fVal + 0.5f);
}
void speedTreeRenderer::RenderTreeBillboardTexture(instanceTreeData* pTreeData, bool bForce, bool needClear, bool isShadow, Vector3* viewDirection )
{
	// Assert( pTreeData.IsValid() );
	//Assert( viewDirection || IsBadStringPtr(viewDirection) );
	if ( isShadow &&  !pTreeData->GetBillboardShadowTexture() )	// doesn't support shadow imposters
	{
		return;
	}
	if( !pTreeData->IsBillboardGenerated() || bForce )
	{
		//PIXBegin(0, "Billboard");

		Vector3 minPos;
		Vector3 maxPos;

		pTreeData->GetBoundingBox( minPos, maxPos);
		grmImposterDraw imposterCreator( *pTreeData->GetImposter(), minPos, maxPos, isShadow, viewDirection, needClear );

		pTreeData->SetBillboardGenerated(true);

		// Create a temporary instance at the origin and highest lod
		TreeInstance inst;
		inst.SetVisible(true);
		inst.SetBillboardOnly(false);
		inst.SetBillboardActive(false);
		inst.SetLod( pTreeData->GetDiscreetLodLevels(), 0.0f);
		inst.SetFadeOut(0.0f);

		// Render with the normal draw pass
		int nOldDrawPass = m_nDrawPassIndex;
		m_nDrawPassIndex = ( m_createBranchImposterTechnique == grcetNONE ) ? DP_NO_WIND : 0;
		
		if ( m_createBranchImposterTechnique == grcetNONE )
		{
			m_createFrondImposterTechnique= grcetNONE;
			m_createLeafImposterTechnique = grcetNONE;
		}

		m_nLeafAlphaOffset = m_nBillboardLeafAlpha;

		// Render the tree
		//TreeInstance* pInstList = &inst;
		// TODO : draw the rmcDrawmable model
		//inst->Draw();

		m_nLeafAlphaOffset = 0;

		// Set the draw pass back
		m_nDrawPassIndex = nOldDrawPass;

		//PIXEnd();
	}
}






void speedTreeRenderer::DrawBillboards(instanceTreeData& pTreeData, TreeInstance** pInstanceList, int nInstanceCount, bool isShadow, Vector3* viewDirection  )
{
#if __BANK
	if (!s_DrawBillboards)
		return;
#endif

	if ( !*pInstanceList ) { return ; }

	PF_FUNC(Billboards);
	Assert(m_nBillboardMatrixRegister >= 0 && m_nBillboardMatrixRegister <= 127);

	// Generate the billboard texture for this tree if it hasnt been generated yet
	if( !pTreeData.IsBillboardGenerated() )
	{
		RenderTreeBillboardTexture( &pTreeData, false);
	}
	int				billboardCount = 0;
	Vector4*		worldPositions = Alloca( Vector4, nInstanceCount);

	for ( int i = 0; i < nInstanceCount; i++ )
	{
		if ( pInstanceList[i]->IsBillboardActive() )
		{
			worldPositions[billboardCount].SetVector3(pInstanceList[i]->GetPosition() );
			worldPositions[billboardCount++].w = pInstanceList[i]->GetFadeOut();
		}
	}
	if ( !billboardCount)
	{
		return;
	}

	// Bind the billboard texture
	m_pBillboardShader->SetVar(m_evBillboardTexture, isShadow ? pTreeData.GetBillboardShadowTexture() : pTreeData.GetBillboardTexture() );//pTreeData.GetBillboardTexture());
	m_pBillboardShader->SetVar(m_evNormalMapBillboardTexture, pTreeData.GetImposter()->GetNormal() );//pTreeData.GetBillboardTexture());
	m_pBillboardShader->SetVar(m_evbillboardTreeCentre, Vector4( 0.0f, 0.0f, 0.0f, pTreeData.GetCullSphere().w ) );

	// Bind the billboard shader
	if (m_pBillboardShader->BeginDraw(grmShader::RMC_DRAW, true, m_BillboardImposterTechnique ) > m_nDrawPassIndex) {
		m_pBillboardShader->Bind(m_nDrawPassIndex);

		grmImposterShader::SetImposterVertexDeclaration();

		unsigned int alpha = Min(Max((unsigned int)m_nBillboardLeafAlpha + 84, (unsigned int)0), (unsigned int)255);
		grcState::SetState( grcsAlphaRef, alpha );

		Vector3 minPos;
		Vector3 maxPos;

		pTreeData.GetBoundingBox( minPos, maxPos);
		pTreeData.GetImposter()->RenderList( viewDirection, worldPositions, billboardCount, minPos, maxPos );

		// Unbind the shader
		m_pBillboardShader->UnBind();
	}
	m_pBillboardShader->EndDraw();

	// reset the reference count of the textures
	m_pBillboardShader->SetVar(m_evBillboardTexture, (const grcTexture *)NULL);
	m_pBillboardShader->SetVar(m_evNormalMapBillboardTexture, (const grcTexture *)NULL);
}

#if __BANK
void speedTreeRenderer::AddWidgets(bkBank& bank)
{
	bank.AddToggle("Blend Lods", &m_bBlendLeafLod);
	bank.AddSlider("Billboard Cam Distance", &m_fCamDist, 0, 50000, 1.0f);
	bank.AddToggle("Draw Billboards",&s_DrawBillboards);
	bank.AddToggle("Draw Branches",&s_DrawBranches);
	bank.AddToggle("Draw Fronds",&s_DrawFronds);
	bank.AddToggle("Draw Leaves",&s_DrawLeaves);
	bank.AddSlider("Billboard Leaf Alpha", &m_nBillboardLeafAlpha, -127, 127, 1);
	bank.AddColor("Billboard Background Color", &m_cBillboardBackgroundColor);
	bank.AddToggle("Draw Bounds", &s_DrawBounds );
	bank.AddToggle("Use Sub Pixel Alpha Mask", &m_useSubPixelAlpha );
}
#endif


// Instance renderer  -----------------------------------------------------



void InstanceTreeRenderer::RenderTreeBillboardTexture(instanceTreeData* pTreeData, bool /*bForce*/, bool needClear, bool isShadow, Vector3* viewDirection)
{
	Vector3 minPos;
	Vector3 maxPos;

	pTreeData->GetBoundingBox( minPos, maxPos);
	grmImposterDraw imposterCreator( *pTreeData->GetImposter(), minPos, maxPos, isShadow, viewDirection, needClear );
	pTreeData->SetBillboardGenerated(true);

	// Create a temporary instance at the origin and highest lod
	TreeInstance inst;
	inst.SetVisible(true);
	inst.SetBillboardOnly(false);
	inst.SetBillboardActive(false);
	inst.SetLod( pTreeData->GetDiscreetLodLevels(), 0.0f);
	inst.SetFadeOut(0.0f);

	// Render with the normal draw pass
	int nOldDrawPass = m_nDrawPassIndex;
	m_nDrawPassIndex = 0;


	// Render the tree
	TreeInstance* pInstList = &inst;

	DrawTrees( pTreeData, &pInstList, 1);


	// Set the draw pass back
	m_nDrawPassIndex = nOldDrawPass;
}

// PURPOSE: Draw instances of a specific tree
// PARAMS:
//	pTreeData - The tree to be drawn
//	pInstanceList - The instances of the tree to be drawn
//	nInstanceCount - Number of instances in the pInstanceList array
//
void InstanceTreeRenderer::DrawTrees(instanceTreeData* pTreeData, TreeInstance** pInstanceList, int nInstanceCount)
{
	DrawTreesList( pTreeData, pInstanceList, nInstanceCount, false, NULL);
}
// PURPOSE: Draw instances of a specific tree
// PARAMS:
//	pTreeData - The tree to be drawn
//	pInstanceList - The instances of the tree to be drawn
//	nInstanceCount - Number of instances in the pInstanceList array
void InstanceTreeRenderer::DrawShadowTrees( instanceTreeData* pTreeData, TreeInstance** pInstanceList, int nInstanceCount, Vector3* lightDirection )
{
	DrawTreesList( pTreeData, pInstanceList, nInstanceCount, true, lightDirection);
}
void InstanceTreeRenderer::DrawTreesList( instanceTreeData* pTreeData, 
											TreeInstance** pInstanceList, 
											int nInstanceCount, 
											bool useShadow, 
											Vector3* viewDirection )
{
	// Draw each instance
	for( int i = 0; i < nInstanceCount; i++ )
	{
		if( !pInstanceList[i]->IsBillboardOnly() )
		{
			//DrawModel(pTreeData, pInstanceList[i]);
		}
	}
	// Generate the billboard texture for this tree if it hasnt been generated yet
	if( !pTreeData->IsBillboardGenerated() )
	{
		RenderTreeBillboardTexture(pTreeData, false);
	}
	int				billboardCount = 0;
	Vector4*		worldPositions = Alloca( Vector4, nInstanceCount);

	for ( int i = 0; i < nInstanceCount; i++ )
	{
		if ( pInstanceList[i]->IsBillboardActive() )
		{
			worldPositions[billboardCount].SetVector3(pInstanceList[i]->GetPosition() );
			worldPositions[billboardCount++].w = pInstanceList[i]->GetFadeOut();
		}
	}
	if ( !billboardCount)
	{
		return;
	}
	
	const grmImposter* imp = pTreeData->GetImposter();
	// Bind the billboard shader
	if (m_shader.BeginDraw( *imp, useShadow) )
	{
		Vector3 minPos;
		Vector3 maxPos;

		pTreeData->GetBoundingBox( minPos, maxPos);
		imp->RenderList( viewDirection, worldPositions, billboardCount, minPos, maxPos );
	}
	m_shader.EndDraw();
}





} // namespace rage
