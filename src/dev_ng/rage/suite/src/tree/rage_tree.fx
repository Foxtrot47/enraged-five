#include "rage_tree_common.fxh"

struct vertexSpeedtree {
    float4 pos				: POSITION;
    float2 texCoord0		: TEXCOORD0;
    float2 vWindAttribs		: TEXCOORD1;
    float3 normal			: NORMAL;
};

float4x4 gWorldMatrix : WorldMatrix;
float4x4 gWorldInverseTransposeMatrix : WorldInverseTransposeMatrix;


speedTreeVertexOutput VS_Speedtree(vertexSpeedtree IN)
{
    speedTreeVertexOutput OUT = (speedTreeVertexOutput)0;
    
	float3 vVertexPosition = SpeedtreeComputeWind(IN.pos.xyz, IN.vWindAttribs.x, IN.vWindAttribs.y);
    							
    // Write out final position & texture coords
    
    vVertexPosition = mul(float4(vVertexPosition,1.0f), gWorldMatrix).xyz;
     
    OUT.pos =  mul( float4( vVertexPosition, 1.0f), gWorldViewProj);
    OUT.texCoord.xy = IN.texCoord0;
    
	OUT.worldEyePos.xyz = vVertexPosition.xyz;
    OUT.worldEyePos.w = 1.0f;

	OUT.worldNormal = normalize(mul(IN.normal, (float3x3)gWorldInverseTransposeMatrix));
	
	FILL_SHADOW_PARAMS(vVertexPosition.xyz);
    OUT.texCoord.zw= float2(  1.0f , g_TreeAlpha.x );
	return OUT;
}

speedTreeVertexOutput VS_SpeedtreeNoWind(vertexSpeedtree IN)
{
    speedTreeVertexOutput OUT = (speedTreeVertexOutput)0;
    
	float4 vVertexPosition = float4(IN.pos.xyz, 1);
	
     vVertexPosition = mul(vVertexPosition, gWorldMatrix);
         							
    // Write out final position & texture coords
    OUT.pos =  mul(vVertexPosition, gWorldViewProj);
    OUT.texCoord.xy = IN.texCoord0;
    OUT.worldEyePos.xyz = vVertexPosition.xyz;
	OUT.worldEyePos.w = 1.0f;
	OUT.worldNormal = normalize(mul(IN.normal, (float3x3)gWorldInverseTransposeMatrix));
	OUT.texCoord.zw= float2(  1.0f , g_TreeAlpha.x );
	FILL_SHADOW_PARAMS(vVertexPosition.xyz);
    
	return OUT;
}

speedTreeVertexDepthOutput VS_SpeedtreeDepth(vertexSpeedtree IN)
{
    speedTreeVertexDepthOutput OUT;
    
	float3 vVertexPosition = SpeedtreeComputeWind(IN.pos.xyz, IN.vWindAttribs.x, IN.vWindAttribs.y);
    							
    // Write out final position & texture coords
    OUT.pos =  mul( float4(vVertexPosition, 1.0f), gWorldViewProj);
    OUT.texCoord = IN.texCoord0;
    
 
   	float3 worldNormal = mul(IN.normal, (float3x3)gWorld);
    
    OUT.ExtraData.x = saturate(dot(worldNormal,-gLightPosDir[0].xyz));  // dot product of normal and light source
    OUT.ExtraData.y = 0.0f; //saturate((length(vVertexPosition.xyz - gViewInverse[3].xyz) - fadeOutLastSMap.y) / fadeOutLastSMap.x);  // fade value...
    
	   
	return OUT;
}

technique draw
{
	pass p0 
	{  
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaRef = 84;
		AlphaFunc = Greater;
		ZEnable = true;
		ZWriteEnable = true;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Speedtree();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	}
	
	pass p1
	{
		// Shadow pass, disable color writes and enable depth writes
		ColorWriteEnable = 0;
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaRef = 84;
		AlphaFunc = Greater;		
		
		VertexShader = compile VERTEXSHADER VS_SpeedtreeDepth();
		PixelShader = compile PIXELSHADER PS_DepthCutout_Speedtree();
	}
#if SHADOWS
	pass p2
	{
		// Blended Shadow mode
		ZEnable = true;
		CullMode = None;
		
		AlphaTestEnable = true;  
		AlphaRef = 84;
		AlphaFunc = Greater;
		
		
		VertexShader = compile VERTEXSHADER VS_Speedtree();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree_BlendShadow();
	}
	pass p3
	{
		// Slow shadow map lookup
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Speedtree();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree_Shadow();
	}
#else // SHADOWS

	// ** NOTE ** THESE ARE DUMMY SHADERS.
	// THEY ARE PLACED HERE SO THE INDEX OF pass p4 REMAINS THE SAME
	// WITH AND WITHOUT SHADOWS. USING THESE SHADERS IN A BUILD WITHOUT
	// SHADOWS IS NOT DEFINED, AND, FRANKLY SPEAKING, DUMB.
	
	pass p2
	{  
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Speedtree();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	}
	
	pass p3
	{
		// Shadow pass, disable color writes and enable depth writes
		ColorWriteEnable = 0;
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaRef = 84;
		AlphaFunc = Greater;		
		
		VertexShader = compile VERTEXSHADER VS_SpeedtreeDepth();
		PixelShader = compile PIXELSHADER PS_DepthCutout_Speedtree();
	}
#endif
	pass p4
	{  
		// No Wind
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		ColorWriteEnable1=0xff;

		VertexShader = compile VERTEXSHADER VS_SpeedtreeNoWind();
		PixelShader  = compile PIXELSHADER  PS_Textured_SpeedtreeMultiBillboard(); // PS_Textured_Speedtree();
	}
}
// PURPOSE
//	This technique is the default way imposters are created. 
//
technique ImpostorDefault
{
	pass p0
	{  
		// No Wind
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		ColorWriteEnable1=0xff;

		VertexShader = compile VERTEXSHADER VS_SpeedtreeNoWind();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree(); // PS_Textured_Speedtree();
	}
}
// PURPOSE
//	This technique allows for creating a Normal mapped imposter. 
//
technique ImpostorNormalMapped
{
	pass p0			
	{  
		// no wind
		AlphaBlendEnable = false;
#if __PS3
		AlphaTestEnable = false;  
#else
		AlphaTestEnable = true;
#endif 
		AlphaFunc = Greater;
		CullMode = None;
		ColorWriteEnable1=0xff;

		VertexShader = compile VERTEXSHADER VS_SpeedtreeNoWind(); //VS_Transform_Speedtree_Leaf_NoWind();
		PixelShader  = compile PIXELSHADER PS_Textured_SpeedtreeMultiBillboard();
	}
}
