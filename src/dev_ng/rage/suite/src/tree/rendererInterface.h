// 
// speedtree/renderer.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPEEDTREE_RENDERERINTERFACE_H
#define SPEEDTREE_RENDERERINTERFACE_H

#include "grcore/device.h"
#include "grcore/effect_typedefs.h"
#include "vector/vector3.h"
#include "atl\array.h"
namespace rage
{

class bkBank;
class grmShader;
class TreeInstance;
class TreeDataBase;
class Matrix44;

//=============================================================================
// TreeRenderer
//
// PURPOSE
//		Interface class to allow for switching implementation incrementally over to use
//		rmcInstance rather than speedtree, and allow for more flexibility
//

class TreeRenderer
{
public:
	TreeRenderer() : m_nDrawPassIndex(0) {}
	virtual ~TreeRenderer() {}

	// PURPOSE: Set the index of the pass to draw when rendering
	// PARAMS:
	//	nPass - The pass of the draw technique to render with
	void SetDrawPassIndex(int nPass)								{ m_nDrawPassIndex = nPass; }

	// PURPOSE: Get the index of the pass to draw when rendering
	// RETURNS: The pass index of the draw technique used to render
	int GetDrawPassIndex() const									{ return m_nDrawPassIndex; }


	// PURPOSE: Set the current wind matrices
	// PARAMS:
	//	pWindMatrices - An array of wind matrices
	//	nWindMatrixCount - Number of wind matrices in the pWindMatrices array
	//	pRockMatrices - An array of leaf rock matrices
	//	nRockMatrixCount - Number of matrices in the pRockMatrices array
	virtual void SetWindMatrices(const Matrix44* /*pWindMatrices*/, int /*nWindMatrixCount*/, const Matrix44* /*pRockMatrices*/, int /*nRockMatrixCount*/) = 0;


	// PURPOSE: Generate the billboard texture for the specified treee
	// PARAMS:
	//	pTreeData - The tree whos billboard is to be created
	//	bForce - Generate a new billboard even if one already exists
	//  needClear - Clears the render target before use 
	// NOTES: This function will not generate a new billboard texture if one already exists unless you
	//		specify true for the bForce parameter.  Also, this should be called before anything is 
	//		drawn as this will clear the depth buffer after it is done rendering the render target.
	virtual void RenderTreeBillboardTexture(TreeDataBase* pTreeData, bool bForce, bool needClear = true, bool isShadow = false, Vector3* viewDirection = 0 ) = 0;


	// PURPOSE: Draw instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	//
	virtual void DrawTrees(TreeDataBase*  pTreeData, TreeInstance** pInstanceList, int nInstanceCount) = 0;


	// PURPOSE: Draw instances of a specific tree
	// PARAMS:
	//	pTreeData - The tree to be drawn
	//	pInstanceList - The instances of the tree to be drawn
	//	nInstanceCount - Number of instances in the pInstanceList array
	virtual void DrawShadowTrees( TreeDataBase* pTreeData, TreeInstance** pInstanceList, int nInstanceCount, Vector3* lightDirection ) = 0;

	static void SetImposterTechnique( const char* tech ) { sm_ImposterTechnique= tech; }

#if __BANK
	void AddWidgets(bkBank& bank);
#endif

private:
protected:
	static const char*			sm_ImposterTechnique;
	int							m_nDrawPassIndex;
};

} // namespace rage

#endif // SPEEDTREE_RENDERERINTERFACE_H
