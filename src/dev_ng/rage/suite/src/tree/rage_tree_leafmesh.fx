//------------------------------------------------------------
//
//		Renders the leafs as instanced gemotry meshs
//

#include "rage_tree_common.fxh"

#if !__XENON
struct vertexSpeedtreeLeaf {
    float4 pos				: POSITION;
    float2 texCoord0		: TEXCOORD0;
    float2 vWindAttribs		: TEXCOORD1;
    float2 vLeafAttribs		: TEXCOORD2;
    float3 normal			: NORMAL;
};

#else

struct vertexSpeedtreeLeaf {
    float4 pos				: POSITION;
    float2 vWindAttribs		: TEXCOORD0;
    float2 vLeafAttribs		: TEXCOORD1;
    float3 normal			: NORMAL;
};

#endif


#define MAX_INSTANCE_VERTS		32
float2	gUVTable[ MAX_INSTANCE_VERTS ]: inst_UVTable;
float3  gPositionTable[ MAX_INSTANCE_VERTS ]: inst_PositionTable;
float3  gNormalTable[ MAX_INSTANCE_VERTS ]: inst_NormalTable;
float	gNumInstanceIndices: inst_NumInstanceIndices;
#if __XENON

struct InstancedVert
{
	float3 Position;
	float3 Normal;
	float2 uvs;
};

InstancedVert	Instance_GetVertex( int i, float4x4 transformMtx )
{
	int tIdx = i % gNumInstanceIndices;
	
	InstancedVert OUT = (InstancedVert)0;
    
	
	OUT.Position = mul( gPositionTable[ tIdx], transformMtx );
	OUT.Normal = mul( gNormalTable[ tIdx], (float3x3)transformMtx );
	OUT.uvs = gUVTable[ tIdx];
	return OUT;
};	
#endif

#if __XENON

speedTreeVertexOutput VS_Transform_Speedtree_Leaf(int i : INDEX)
{
	int vertex = i / gNumInstanceIndices;
	float4 center4;
	float4 normal4;
	float4 texcoord4;
	float4 windattribs4;
	float4 leafattribs4;
	
	asm {
		vfetch	center4, vertex, position0
		vfetch	normal4, vertex, normal0
		vfetch	windattribs4, vertex, texcoord0
		vfetch	leafattribs4, vertex, texcoord1
	};
	 speedTreeVertexOutput OUT = (speedTreeVertexOutput)0;
    
    // Calculate leaf center postition with wind
	//float3 vVertexPosition = SpeedtreeComputeWind(center4, windattribs4.x, windattribs4.y);      
 
	// Calculate the position of the leave vertex in world space
	//leafattribs4.x += index;
    //vVertexPosition = ComputeLeafVertex(vVertexPosition, leafattribs4.xy);
    
    // create transfrom for the normal
    float4x4 transformMtx;
    float3 tan = float3( 0.0f, 1.0f,0.0f );
    float3 bi;
    bi = normalize( cross( tan, normal4.xyz ) );
    tan  = normalize(cross( bi, normal4.xyz )  );
    transformMtx[0] = float4( tan.xyz, 0.0f );
    transformMtx[1] = float4( bi.xyz, 0.0f );
    transformMtx[2] = float4( normal4.xyz, 0.0f );
    transformMtx[3] = float4( center4.xyz, 1.0f );
    
    
    InstancedVert vert = Instance_GetVertex( i, transformMtx );
										
    // Write out final position & texture coords
    OUT.pos =  mul(float4( vert.Position.xyz, 1), gWorldViewProj);
    OUT.worldNormal = normalize(mul( vert.Normal.xyz, (float3x3)gWorld));//normalize(mul(IN.normal, (float3x3)gWorldInvTrans));
    OUT.texCoord.xy = vert.uvs;
    OUT.worldEyePos.xyz = vert.Position.xyz;
	OUT.worldEyePos.w = 1.0f;
	OUT.texCoord.zw = 1.0f;

	FILL_SHADOW_PARAMS(vert.Position.xyz);
    
	return OUT;
}
#else
	
speedTreeVertexOutput VS_Transform_Speedtree_Leaf(vertexSpeedtreeLeaf IN)
{
    speedTreeVertexOutput OUT = (speedTreeVertexOutput)0;
    
    // Calculate leaf center postition with wind
	float3 vVertexPosition = SpeedtreeComputeWind(IN.pos.xyz, IN.vWindAttribs.x, IN.vWindAttribs.y);      
 
	// Calculate the position of the leave vertex in world space
    vVertexPosition = ComputeLeafVertex(vVertexPosition, IN.vLeafAttribs);
										
    // Write out final position & texture coords

    OUT.pos =  mul(float4(vVertexPosition.xyz, 1.0f), gWorldViewProj);
    OUT.worldNormal = normalize(mul(IN.normal, (float3x3)gWorld));//normalize(mul(IN.normal, (float3x3)gWorldInvTrans));
#if !__XENON
    OUT.texCoord.xy = IN.texCoord0.xy;
 #else
    OUT.texCoord = 1.0f;
 #endif
	 OUT.worldEyePos.xyz = vVertexPosition.xyz;
	OUT.worldEyePos.w = 1.0f;
	OUT.texCoord.zw = 1.0f;
    
	FILL_SHADOW_PARAMS(vVertexPosition.xyz);
    
	return OUT;
}
#endif
speedTreeVertexOutput VS_Transform_Speedtree_Leaf_NoWind(vertexSpeedtreeLeaf IN)
{
	speedTreeVertexOutput OUT = (speedTreeVertexOutput)0;
    
    
	// Calculate the position of the leave vertex in world space
    float3 vVertexPosition = ComputeLeafVertex(IN.pos.xyz, IN.vLeafAttribs);
										
    // Write out final position & texture coords

    OUT.pos =  mul(float4(vVertexPosition.xyz, 1), gWorldViewProj);
    OUT.worldNormal = normalize(mul(IN.normal, (float3x3)gWorldInvTrans));
  #if !__XENON
    OUT.texCoord.xy = IN.texCoord0.xy;
 #else
    OUT.texCoord = 1.0f;
 #endif
    OUT.worldEyePos.xyz = vVertexPosition.xyz;
	OUT.worldEyePos.w = 1.0f;
    OUT.texCoord.zw = 1.0f;

	FILL_SHADOW_PARAMS(vVertexPosition.xyz);
    
	return OUT;
}

speedTreeVertexDepthOutput VS_Transform_Speedtree_Leaf_Depth(vertexSpeedtreeLeaf IN)
{
    speedTreeVertexDepthOutput OUT; // = (speedTreeVertexDepthOutput)0;
    
    // Calculate leaf center postition with wind
	float3 vVertexPosition = SpeedtreeComputeWind(IN.pos.xyz, IN.vWindAttribs.x, IN.vWindAttribs.y);
	
    // Calculate the position of the leave vertex in world space
    vVertexPosition = ComputeLeafVertex(vVertexPosition, IN.vLeafAttribs);
										
    // Write out final position & texture coords
    OUT.pos =  mul(float4(vVertexPosition.xyz, 1), gWorldViewProj);
  #if !__XENON
    OUT.texCoord = IN.texCoord0;
 #else
    OUT.texCoord = 1.0f;
 #endif
 
   	float3 worldNormal = mul(IN.normal, (float3x3)gWorld);
    
    OUT.ExtraData.x = saturate(dot(worldNormal,-gLightPosDir[0].xyz));  // dot product of normal and light source
    OUT.ExtraData.y = 0.0f; //saturate((length(vVertexPosition.xyz - gViewInverse[3].xyz) - fadeOutLastSMap.y) / fadeOutLastSMap.x);  // fade value...
    
	return OUT;
}

technique draw
{
	pass p0 
	{  
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
	//	AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		
#if !__PS3
//		SeparateAlphaBlendEnable = true;
//		SrcBlendAlpha =  ONE;
//		DestBlendAlpha = ONE;
//		BlendOpAlpha = Add;
#endif
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	}
	
	pass p1
	{
		// Shadow pass, disable color writes and enable depth writes
		ColorWriteEnable = 0;
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
	//	AlphaRef = 84;
		AlphaFunc = Greater;
		
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf_Depth();
		PixelShader = compile PIXELSHADER PS_DepthCutout_Speedtree();
	}
	
#if SHADOWS
	pass p2
	{
		// Blended Shadowmaps
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
		AlphaTestEnable = true;  
	//	AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		ZEnable = true;
		
		AlphaBlendEnable = true;
		SrcBlend = Zero;
		DestBlend = SrcColor;
		
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree_BlendShadow();
	}
	
	pass p3
	{
		// Slow shadowmap technique
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
	//	AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree_Shadow();
	}
#else // SHADOWS

	// ** NOTE ** THESE ARE DUMMY SHADERS.
	// THEY ARE PLACED HERE SO THE INDEX OF pass p4 REMAINS THE SAME
	// WITH AND WITHOUT SHADOWS. USING THESE SHADERS IN A BUILD WITHOUT
	// SHADOWS IS NOT DEFINED, AND, FRANKLY SPEAKING, DUMB.

	pass p2 
	{  
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
	//	AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		
#if !__PS3
//		SeparateAlphaBlendEnable = true;
//		SrcBlendAlpha =  ONE;
//		DestBlendAlpha = ONE;
//		BlendOpAlpha = Add;
#endif
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	}
	
	pass p3
	{
		// Shadow pass, disable color writes and enable depth writes
		ColorWriteEnable = 0;
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
	//	AlphaRef = 84;
		AlphaFunc = Greater;
		
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf_Depth();
		PixelShader = compile PIXELSHADER PS_DepthCutout_Speedtree();
	}

#endif // SHADOWS
	
	pass p4			
	{  
		// no wind
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
	//	AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;

		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf_NoWind();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	}
}
