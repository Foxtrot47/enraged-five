#define NO_SKINNING
#include "../../../../rage/base/src/shaderlib/rage_common.fxh"


//#include "../../../../rage/base/src/shaderlib/rage_diffuse_sampler.fxh"
#include "../../../../rage/base/src/shaderlib/rage_samplers.fxh"


BeginSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
    string UIName = "Diffuse Texture";
ContinueSampler(sampler2D,diffuseTexture,DiffuseSampler,DiffuseTex)
			AddressU = Wrap;
			AddressV = Wrap;
			AddressW = Wrap;
			MIPFILTER = LINEAR;
			MINFILTER = LINEAR;
			MAGFILTER = LINEAR; 
EndSampler;

#define SHADOWS 0
#define FOURSHADOWMAPS 1
#define ZBIAS_PER_OBJECT 1
#if SHADOWS
#include "../../../../rage/base/src/shaderlib/rage_shadowmap_common.fxh"
#include "../../../../rage/base/src/shaderlib/rage_shadow.fxh"
#endif

#if __WIN32PC 
    #if __SHADERMODEL < 20
        // Currently, all we can go down to is 1.4
        //    If we need lower, we can allow it
        #define PIXELSHADER		ps_1_4
        #define VERTEXSHADER	vs_1_1
    #elif __SHADERMODEL >= 30
		#if __SHADERMODEL >= 40
			#define PIXELSHADER		ps_4_0
			#define VERTEXSHADER	vs_4_0
		#else
			#define PIXELSHADER		ps_3_0
			#define VERTEXSHADER	vs_3_0
		#endif
    #else
        #define PIXELSHADER ps_2_0
        #define VERTEXSHADER vs_2_0
    #endif    // __SHADERMODEL
#elif __XENON || __PS3
    #define PIXELSHADER ps_3_0
    #define VERTEXSHADER vs_3_0
#endif

struct speedTreeVertexOutput 
{
    float4 pos						: POSITION;
    float4 texCoord					: TEXCOORD0;
	float3 worldNormal				: TEXCOORD1;
	float4 worldEyePos				: TEXCOORD2;
//#if __XENON || __PS3
	float3 ScreenPos			    : TEXCOORD7;
//#endif	
	
#if SHADOWS
	float4 shadowPixelPos0			: TEXCOORD3;
/*	float4 shadowPixelPos1			: TEXCOORD4;
	float4 shadowPixelPos2			: TEXCOORD5;
	float4 shadowPixelPos3			: TEXCOORD6;*/
#endif

	float2 ExtraData				: TEXCOORD4;
};

struct speedTreeVertexDepthOutput
{
	float4 pos						: POSITION;
	float2 texCoord					: TEXCOORD0;
	float2 ExtraData				: TEXCOORD1;
};



// Wind
float4x4	g_amWindMatrices[5] : WindMatrices;

float3 SpeedtreeComputeWind(float3 vertexPosition, float matrixIndex, float weight)
{
	float3 windPosition = mul( float4(vertexPosition, 1.0f), g_amWindMatrices[matrixIndex]).xyz;
	return lerp(vertexPosition, windPosition, weight);
}

#define FILL_SHADOW_PARAMS(f3WorldPos)		{}
#define FILL_SHADOW_PARAMS_JUNK	

#if __XENON
#define LIGHT_COUNT gLightCount
#else
#define LIGHT_COUNT 3
#endif



float4		g_treeCentre: TreeCentre;
float4		g_TreeAlpha: TreeAlpha;

///////////////////////////////////////////////////////////////////////////////////////////////
// Leaf Stuff
///////////////////////////////////////////////////////////////////////////////////////////////
float3		g_vUpVector : UpVector = {0.0f, 1.0f, 0.0f};

// leaf table
float4		g_avLeafClusters[48] : LeafClusters;//g_avLeafClusters[48] : LeafClusters;


// speedwind

float4x4 gLeafWorldMatrix : LeafWorldMatrix 
#if !__PS3
	//: register(vs, c240)
#endif
;
float4x4 gWorldInvTrans : LeafWorldInvTrans 
#if !__PS3
	//: register(vs, c244)
#endif
;

float3x3 CalculateBillboardMatrix(float4 vVertexPosition)
{
	// Transform the leaf center into world space
    float3 center = mul(float4(vVertexPosition.xyz, 1), gLeafWorldMatrix).xyz;
    
    // Compute the view normal		center -> view
    //float3 viewNrm = normalize(gViewInverse[3].xyz - center);
    float3 viewNrm = normalize(-gViewInverse[2].xyz);
    
	//viewNrm = normalize(mul((float3x3)gWorldInvTrans, viewNrm));
    
    float3 up = g_vUpVector;
        
    // Compute the view right vector
    float3 viewRight = normalize(cross(viewNrm, up));
    
    // Compute the new up vector
	up = normalize(cross(viewRight, viewNrm));
    
    // Compute rotation basis matrix
    float3x3 mBillboardBasis;
	mBillboardBasis[0] = float3(viewNrm.x, up.x, viewRight.x);
	mBillboardBasis[1] = float3(viewNrm.y, up.y, viewRight.y);
	mBillboardBasis[2] = float3(viewNrm.z, up.z, viewRight.z);
	
	return mBillboardBasis;
}

float3 ComputeLeafVertex(float3 vVertexPosition, float2 vLeafAttribs)
{
	// Compute the corner offset for this vertex
	vVertexPosition = g_avLeafClusters[vLeafAttribs.x].xyz * vLeafAttribs.y + vVertexPosition;
	
	// Transform the leaf to the position of the tree
	vVertexPosition += gLeafWorldMatrix[3].xyz;
	
	return vVertexPosition;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Lighting
///////////////////////////////////////////////////////////////////////////////////////////////
float4 CalcLitDiffuse(float2 texCoord, float3 worldNormal, float3 worldEyePos)
{
	float4 diffuseColor = tex2D(DiffuseSampler, texCoord);
	float3 N = worldNormal;
	float3 viewDir = worldEyePos;
	
	float3 lightColor = float3(0,0,0);
	for( int i = 0; i < LIGHT_COUNT; i++ )
	{		
		rageLightOutput lightData = rageComputeLightData(viewDir, i);
	    float3 L = lightData.lightPosDir.xyz;

		// Compute falloff term
		float falloff = 1.0 - min((lightData.lightPosDir.w / gLightPosDir[i].w), 1.0);
		float lightIntensity = gLightColor[i].w;
		if ( lightIntensity < 0.f ) {
			falloff *= -lightIntensity;		// Allow oversaturation
		}
		else {
			falloff = min(falloff * lightIntensity, 1.0);
		}

		float3 light = max(dot(N,L), 0.0);

		// Sum up color contribution of light sources
		lightColor += gLightColor[i].rgb * light.y * falloff;
	}
	
	lightColor += gLightAmbient.xyz;
	
	float4 litDiffuse = (diffuseColor * float4(lightColor, 1.0f) * float4(1.0f, 1.0f, 1.0f, 1.0f));// gLightAmbient.w));
	return litDiffuse;
}

/* ====== PIXEL SHADERS =========== */
//-----------------------------------
float4 PS_Textured_Speedtree( speedTreeVertexOutput IN): COLOR
{	
	float4 litDiffuse = CalcLitDiffuse(IN.texCoord.xy, IN.worldNormal, IN.worldEyePos.xyz);

	return litDiffuse;//return float4( litDiffuse.xyz, 1.0f);
}
struct PSMultiBillboardOutput
{
	float4 col[2] : COLOR0;
};

PSMultiBillboardOutput PS_Textured_SpeedtreeMultiBillboard( speedTreeVertexOutput IN)
{
	PSMultiBillboardOutput Output;
	
	float depth = (dot(IN.worldEyePos.xyz, gViewInverse[2].xyz) + IN.worldEyePos.w ) / (2.0f * IN.worldEyePos.w );
	Output.col[0] = tex2D(DiffuseSampler, IN.texCoord.xy);
	
#if __PS3
	if (Output.col[0].w < 84.0f/255.0f)
	{
		discard;
	}
	Output.col[0].w = 1;
#else
	Output.col[0].w = step( 84.0f/255.0f, Output.col[0].w );
#endif

	Output.col[1] = float4( normalize( IN.worldNormal ) * 0.5 + 0.5, depth );
	return Output;
}


float4 PS_Textured_SpeedtreeForBillboard( speedTreeVertexOutput IN): COLOR
{	
	float4 litDiffuse = CalcLitDiffuse(IN.texCoord, IN.worldNormal, IN.worldEyePos);
	const float boostAmount = 64.0f;
	
	// boost alpha value so noise is cancelled out
	litDiffuse.w = step( 84.0f/255.0f, litDiffuse.w );//saturate( litDiffuse.w * boostAmount );
	return litDiffuse;
}
float4 PS_Textured_SpeedtreeSolid( speedTreeVertexOutput IN): COLOR
{	
	float4 litDiffuse = CalcLitDiffuse(IN.texCoord, IN.worldNormal, IN.worldEyePos);
	// boost alpha value so noise is cancelled out
	litDiffuse.w =1.0f;
	return litDiffuse;
}
#if SHADOWS
float4 PS_Textured_Speedtree_Shadow( speedTreeVertexOutput IN, float2 vPos : VPOS ): COLOR
{	
	float4 litDiffuse = CalcLitDiffuse(IN.texCoord, IN.worldNormal, IN.worldEyePos);
	
#if __SHADERMODEL >= 30
    float2 TexCoord = vPos * gShadowCollectorTexelSize;
    float4 BlackNWhite = tex2D( ShadowCollectorSampler, TexCoord);
	float fadeShadowValue = BlackNWhite.x;  
#else
	float fadeShadowValue = 1.0f;	
#endif
	float4 diffuseColor = tex2D(DiffuseSampler, IN.texCoord);
	diffuseColor.xyz *= gLightAmbient.xyz;
	
	return lerp(litDiffuse, diffuseColor , 1 - fadeShadowValue);
}
#endif

#if SHADOWS
float4 PS_Textured_Speedtree_BlendShadow(speedTreeVertexOutput IN) : COLOR
{
	float3 viewD = IN.worldEyePos - gViewInverse[3].xyz;
	float camDistance = dot(viewD, -gViewInverse[2].xyz);
	
	float4 diffuseColor = tex2D(DiffuseSampler, IN.texCoord);
	
	return float4(BlendShadows(IN.shadowPixelPos0, camDistance).xyz, diffuseColor.w);
}
#endif

float4 PS_DepthCutout_Speedtree(speedTreeVertexDepthOutput  IN) : COLOR  // speedTreeVertexOutput
{
	float4 diffuseColor = tex2D(DiffuseSampler, IN.texCoord);
	return diffuseColor;
}

float4 PS_DrawDepth_Dummy() : COLOR
{
	return float4(0.0, 0.0,0.0, 0.0);
}

