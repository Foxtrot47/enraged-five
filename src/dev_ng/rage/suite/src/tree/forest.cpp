// 
// speedtree/forest.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tree/forest.h"

#include "tree/rendererInterface.h"
#include "tree/constants.h"

#include "atl/array_struct.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "bank/slider.h"
#include "data/safestruct.h"
#include "diag/tracker.h"
#include "file/asset.h"
#include "file/token.h"
#include "grcore/viewport.h"
#include "grcore/texture.h"
#include "grprofile/pix.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "parsercore/stream.h"
#include "profile/profiler.h"
#include "system/memory.h"

#include "phcore\isect.h"
#include "physics\levelnew.h"

#include <algorithm>



namespace rageTreeForestStats
{
	PF_PAGE(RAGETreeForest,"tree Forest");

	PF_GROUP(Forest);
	PF_LINK(RAGETreeForest,	Forest);
	PF_TIMER(UpdateWind,			Forest);
	PF_TIMER(UpdateVisibility,		Forest);
	PF_TIMER(UpdateShadowVisibility,Forest);
	PF_TIMER(BuildLeafAngles,		Forest);
	PF_TIMER(UpdateWindMatrices,	Forest);
	PF_TIMER(Draw,					Forest);
	PF_TIMER(DrawShadows,			Forest);
	PF_COUNTER(TreesDrawn,			Forest);
	PF_COUNTER(TreesInScene,		Forest);
}
using namespace rageTreeForestStats;

namespace rage
{

template<class TREEDATA>
TreeForestImp<TREEDATA>::TreeForestImp(const char* /*szWindFile*/, bool /*noWind*/, int maxTreeTypes  )
		:m_SunLightDirection( 0.0f, 1.0f, 0.0f )
{
#if __BANK
	sysMemStartTemp();
	m_pDebugInfo = rage_new TreeForestDebugInfo;
	sysMemEndTemp();
#endif // __BANK

	CreateManager( maxTreeTypes );


	m_vWind.Set(1.0f, 0.0f, 0.0f, 0.0f);

	m_fBillboardNearDistance.Set(180.0f);
	m_fBillboardFarDistance.Set(180.0f);

	m_nRenderTargetSize = 256;

	m_RegenerateAll = false;
	m_MaxBillboardsPerFrame = 4;

	m_useBillboardSizeSelection = false;
	m_billboardBlendRange = 32.0f;

#if __BANK
	m_pDebugInfo->m_bActivateGizmos = false;
	m_pDebugInfo->m_bGizmosWereActive = false;

	m_pDebugInfo->m_nLoadSeed = 0;
	m_pDebugInfo->m_fLoadSize = -1.0f;
	m_pDebugInfo->m_fLoadSizeVar = -1.0f;

	m_pDebugInfo->m_nTreeTypeCount = 0;
	m_pDebugInfo->m_vAddTreePos.Zero();
	m_pDebugInfo->m_szAddTreeType[0] = 0;
#endif
}

template<class TREEDATA>
TreeForestImp<TREEDATA>::~TreeForestImp()
{
	for( int i = 0; i < m_Trees.GetCount(); i++ )
	{
		// Release this tree if we used it.
		if (m_TreeSourceHashes[i])
		{
			TreeDataMgr<TREEDATA>::GetInstance().Release(m_TreeSourceHashes[i]);
		}
	}	

#if __BANK
	sysMemStartTemp();
	delete m_pDebugInfo;
	sysMemEndTemp();
#endif // __BANK
}


template<class TREEDATA>
IMPLEMENT_PLACE(TreeForestImp<TREEDATA>);

template<class TREEDATA>
TreeForestImp<TREEDATA>::TreeForestImp(datResource& rsc) : m_Trees(rsc), m_TreeInstances(rsc, 1), m_VisibleTreeInstances(rsc, 1), m_VisibleShadowTreeInstances(rsc, 1), m_TreeSourceHashes(rsc), m_TreeDebugNames(rsc)
{
	AssertMsg(TreeDataMgr<TREEDATA>::IsInstantiated(), "Loading a resourced speedTree forest, but no speedTreeDataMgr has been instantiated. Having a TreeForest resource now requires a manager for the data. You might want to save a speedTreeDataMgr resource along with your TreeForest.");

#if __BANK
	m_pDebugInfo = rage_new TreeForestDebugInfo;
#endif // __BANK


	AssertMsg(m_Trees.GetCount() == m_TreeSourceHashes.GetCount(), "tree grid cell resource is broken - can't resolve the trees");

	// I disabled this. What happened was that a particular speedtree resource got called,
	// which had these arrays set up already, and the resource constructor would call into
	// sysMemExternalAllocator to reset the arrays. Apparently, if you free the first
	// allocation in a chunk (which may not be the first allocation in the full resource),
	// it actually frees up the memory for the whole chunk. As a result, the memory would
	// actually be reused for another resource, while the rest of the speedtree code still
	// pointed to it, and very bad things would happen.
	// I'm not 100% certain if this is the right thing to do, but I figure if our resource
	// compilers still reserve space in the resource for these arrays, we may as well use it.
	// The asserts below were added by me, so we can more easily catch if there are resources
	// which may not already contain the arrays. /FF
	Assert(m_VisibleTreeInstances.GetCount() == m_Trees.GetCount());
	Assert(m_VisibleShadowTreeInstances.GetCount() == m_Trees.GetCount());
#if 0
	// Allocate space for the visibility list (and reset if it was created with an old tool)
	m_VisibleTreeInstances.Reset();
	m_VisibleShadowTreeInstances.Reset();
	m_VisibleTreeInstances.Reserve(m_Trees.GetCount());
	m_VisibleShadowTreeInstances.Reserve(m_Trees.GetCount());
#endif

	for( int i = 0; i < m_Trees.GetCount(); i++ )
	{
		// Request this tree if we use it.
		if (m_TreeSourceHashes[i])
		{
			TreeDataMgr<TREEDATA>::GetInstance().AddRef(m_TreeSourceHashes[i]);
			m_Trees[i] = TreeDataMgr<TREEDATA>::GetInstance().GetTree(m_TreeSourceHashes[i]);
		}
		// NOTE that this pointer may very well be NULL - the data could be invalid
		// or (in later versions) not streamed in yet.

		// The comment above regarding array reallocation in
		// resources applies here too. /FF
#if 0
		// Let's also reserve space for the visible lists.
		m_VisibleTreeInstances.Grow().Reserve(m_TreeInstances[i].GetCount());
		m_VisibleShadowTreeInstances.Grow().Reserve(m_TreeInstances[i].GetCount());
#endif
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::CreateManager( int maxTreeTypes )
{
	if (!TreeDataMgr<TREEDATA>::IsInstantiated())
	{
		sysMemStartTemp();
		TreeDataMgr<TREEDATA>::InitClass( maxTreeTypes );
		sysMemEndTemp();
	}
}


#if __DECLARESTRUCT
template<class TREEDATA>
void TreeForestImp<TREEDATA>::DeclareStruct(datTypeStruct& s)
{
	// We can't  have debug info in resources.
#if __BANK
	sysMemStartTemp();
	delete m_pDebugInfo;
	m_pDebugInfo = NULL;
	sysMemEndTemp();
#endif // __BANK

	pgBase::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(TreeForestImp<TREEDATA>, pgBase)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_Trees)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_vWind)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_fBillboardNearDistance)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_fBillboardFarDistance)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_SunLightDirection)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_TreeSourceHashes)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_TreeDebugNames)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_TreeInstances)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_VisibleTreeInstances)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_VisibleShadowTreeInstances)
	SSTRUCT_IGNORE(TreeForestImp<TREEDATA>, m_pDebugInfo)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_nRenderTargetSize)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_MaxBillboardsPerFrame)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_billboardBlendRange)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_RegenerateAll)
	SSTRUCT_FIELD(TreeForestImp<TREEDATA>, m_useBillboardSizeSelection)
	SSTRUCT_CONTAINED_ARRAY(TreeForestImp<TREEDATA>, m_Pad)
	SSTRUCT_END(TreeForestImp<TREEDATA>)
}
#endif


template<class TREEDATA>
bool TreeForestImp<TREEDATA>::Load(const char* szForestFile)
{
	RAGE_TRACK(SpeedTree);

	
	bool bSuccess = false;

	fiStream* pFile = ASSET.Open(szForestFile, "stf");
	if( pFile )
	{
		fiAsciiTokenizer tok;
		tok.Init(szForestFile, pFile);

		char szTempStr[1024];

		while( pFile->Tell() < pFile->Size() )
		{
			if( tok.GetToken(szTempStr, sizeof(szTempStr)) == 0 )
				continue;

			// Read the tree parameters from the file
			int nSeed = tok.GetInt();
			float fSize = tok.GetFloat();
			float fSizeVariance = tok.GetFloat();
			Vector3 vPos;
			tok.GetVector(vPos);
			int nNumInstances = tok.GetInt();

			// Create the tree
			int nTreeIndex = LoadTree(szTempStr, nSeed, fSize, fSizeVariance);
			Assert(nTreeIndex > -1);

			// Allocate space for the instances. 1 Plus the instance count since the tree data itself counts as an instance
			m_TreeInstances[nTreeIndex].Resize(nNumInstances + 1);
			m_VisibleTreeInstances[nTreeIndex].Resize(nNumInstances + 1);
			m_VisibleShadowTreeInstances[nTreeIndex].Resize(nNumInstances + 1);

			// Create an instance at the location in the tree data
			m_TreeInstances[nTreeIndex][0].SetPosition(vPos);

			// Create all the other instances
			for( int i = 0; i < nNumInstances; i++ )
			{
				tok.GetVector(vPos);
				m_TreeInstances[nTreeIndex][i + 1].SetPosition(vPos);
				m_TreeInstances[nTreeIndex][i + 1].CreatePhysicsInstance(m_Trees[nTreeIndex]->GetPhysicsArchetype() );
			}
		}

		pFile->Close();
		bSuccess = true;
	}

	return bSuccess;
}

template<class TREEDATA>
bool TreeForestImp<TREEDATA>::LoadFromRDR2File(const char* szRDR2File, bool locateGround )
{
	RAGE_TRACK(SpeedTree);

	char base[256];
	strcpy(base, fiAssetManager::FileName(szRDR2File));

	char path[256];
	fiAssetManager::RemoveNameFromPath(path, 256, szRDR2File);

	if( strcmp(path, szRDR2File) )
		ASSET.PushFolder(path);

	sysMemStartTemp();
	parStreamIn* pStream = PARSER.OpenInputStream(szRDR2File, "");
	sysMemEndTemp();

	if ( !pStream )
	{
		Warningf("Could not load Forest file %s ", szRDR2File );
		return false;
	}
	parElement element;
	pStream->ReadBeginElement(element);
		pStream->ReadBeginElement(element);
		int count = element.FindRequiredAttribute("count").FindIntValue();

		const int treeNameSize = 128;
		sysMemStartTemp();
		char* treeNames = rage_new char[treeNameSize * count];
		int* treeDataIndices = rage_new int[count];
		int* treeInstanceCounts = rage_new int[count];
		sysMemEndTemp();
		memset(treeInstanceCounts, 0, sizeof(int) * count);
		int uniqueTrees = 0;

		for( int i = 0; i < count; i++ )
		{
			pStream->ReadLeafElement(element);
			strncpy(treeNames + (i * treeNameSize), element.FindRequiredAttribute("filename").GetStringValue(), treeNameSize);
			
			bool bAlreadyExists = false;
			for( int j = 0; j < i; j++ )
			{
				if( !strcmp(treeNames + (i * treeNameSize), treeNames + (j * treeNameSize)) )
				{
					treeDataIndices[i] = treeDataIndices[j];
					bAlreadyExists = true;
					break;
				}
			}
			if( !bAlreadyExists )
			{
				// Create the tree
				treeDataIndices[i] = LoadTree(treeNames + (i * treeNameSize), 1, -1.0f, -1.0f);
				uniqueTrees++;
			}
		}
		pStream->ReadEndElement();

		pStream->ReadBeginElement(element);
		int instanceCount = element.FindRequiredAttribute("count").FindIntValue();

		sysMemStartTemp();
		Matrix34* treeInstanceMatrices = rage_new Matrix34[instanceCount];
		int* treeInstanceIndices = rage_new int[instanceCount];
		sysMemEndTemp();

		for( int i = 0; i < instanceCount; i++ )
		{
			pStream->ReadLeafElement(element);
			int index = element.FindRequiredAttribute("index").FindIntValue();
			//float rand = element.FindRequiredAttribute("rand").FindFloatValue();
			//Vector3 pos;
			//sscanf(element.FindRequiredAttribute("pos").GetStringValue(), "%f %f %f", &pos.x, &pos.y, &pos.z);
			Matrix34 mtx;
			sscanf(element.FindRequiredAttribute("matrix").GetStringValue(), "%f %f %f\t%f %f %f\t%f %f %f\t%f %f %f\t", &mtx.a.x, &mtx.a.y, &mtx.a.z, &mtx.b.x, &mtx.b.y, &mtx.b.z, &mtx.c.x, &mtx.c.y, &mtx.c.z, &mtx.d.x, &mtx.d.y, &mtx.d.z);

			treeInstanceMatrices[i] = mtx;
			treeInstanceIndices[i] = index;

			treeInstanceCounts[index]++;
		}
		pStream->ReadEndElement();

		// Create all the instances
		for( int i = 0; i < uniqueTrees; i++ )
		{
			// Count the instances of this tree
			int totalInstances = 0;
			for( int j = 0; j < count; j++ )
			{
				if( treeDataIndices[j] == i )
					totalInstances += treeInstanceCounts[j];
			}

			// Allocate space for the instances
			m_TreeInstances[i].Resize(totalInstances);
			m_VisibleTreeInstances[i].Resize(totalInstances);
			m_VisibleShadowTreeInstances[i].Resize(totalInstances);

			// Create the instances
			int currentInstance = 0;
			for( int k = 0; k < instanceCount; k++ )
			{
				if( treeDataIndices[treeInstanceIndices[k]] == i )
				{
					if ( locateGround )
					{
						phSegment segmentDown;
						phSegment segmentUp;

						segmentDown.Set(treeInstanceMatrices[k].d, treeInstanceMatrices[k].d - Vector3( 0.0f, 100000.0f, 0.0f ) );
						segmentUp.Set(treeInstanceMatrices[k].d  + Vector3( 0.0f, 100000.0f, 0.0f ), treeInstanceMatrices[k].d );
						phIntersection isect;
						
						PHLEVEL->TestEdge(segmentDown, &isect);

						if ( !isect.GetInstance() )
						{
							PHLEVEL->TestEdge(segmentUp, &isect);
						}
						if ( isect.GetInstance()  )
						{
							treeInstanceMatrices[k].d = RCC_VECTOR3(isect.GetPosition());
						}
					}
					m_TreeInstances[i][currentInstance].SetPosition(treeInstanceMatrices[k].d);
					m_TreeInstances[i][currentInstance++].CreatePhysicsInstance(m_Trees[i]->GetPhysicsArchetype() );
				}
			}
		}

		sysMemStartTemp();
		delete[] treeNames;
		delete[] treeDataIndices;
		delete[] treeInstanceCounts;
		delete[] treeInstanceMatrices;
		delete[] treeInstanceIndices;
		sysMemEndTemp();

		pStream->ReadBeginElement(element);
		pStream->ReadEndElement();
	pStream->ReadEndElement();

	sysMemStartTemp();
	delete pStream;
	sysMemEndTemp();

	if( strcmp(path, szRDR2File) )
		ASSET.PopFolder();

	return true;
}

template<class TREEDATA>
int TreeForestImp<TREEDATA>::LoadTree(const char* szTreeFile, int nSeed, float fSize, float fSizeVariance, bool bCreateBounds, bool bOfflineCreation)
{
	RAGE_TRACK(SpeedTree);

	int nIndex = -1;
	u32 uHashValue;

	TREEDATA *pTreeData = TreeDataMgr<TREEDATA>::GetInstance().RequestTreeData(szTreeFile, m_nRenderTargetSize, nSeed, fSize, fSizeVariance, &uHashValue, bCreateBounds, bOfflineCreation);

	if( pTreeData )
	{
		// Check for an empty tree slot
		for( int i = 0; i < m_Trees.GetCount(); i++ )
		{
			if( !m_Trees[i] )
			{
				nIndex = i;
				break;
			}
		}
		if( nIndex == -1 )
		{
			nIndex = m_Trees.GetCount();
			m_Trees.Grow() = pTreeData;
			m_TreeSourceHashes.Grow() = uHashValue;
			m_TreeDebugNames.Grow().SetDebugName(szTreeFile);

			m_TreeInstances.Grow();
			m_VisibleTreeInstances.Grow();
			m_VisibleShadowTreeInstances.Grow();

			BANK_ONLY(m_pDebugInfo->m_nTreeTypeCount++);
		}
		else
		{
			m_Trees[nIndex] = pTreeData;
			m_TreeSourceHashes[nIndex] = uHashValue;
		}
	}

	return nIndex;
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::AttachToManager(TreeDataMgr<TREEDATA> & /*manager*/, atArray<u32> *treeTypeHashes)
{
	AssertMsg(m_Trees.GetCount() == 0, "SetTreeTypeCount() has been called in conjunction with LoadTree().");

	//	int nTreeTypes = manager.GetCount();
	int nTreeTypes = treeTypeHashes->GetCount();

	m_Trees.Reserve(nTreeTypes);
	m_TreeSourceHashes.Reserve(nTreeTypes);
	m_TreeDebugNames.Reserve(nTreeTypes);
	m_TreeInstances.Resize(nTreeTypes);
	m_VisibleTreeInstances.Resize(nTreeTypes);
	m_VisibleShadowTreeInstances.Resize(nTreeTypes);

	if (treeTypeHashes)
	{
		for (int x=0; x<treeTypeHashes->GetCount(); x++)
		{
			u32 hash = (*treeTypeHashes)[x];

			//m_Trees.Append() = manager.Lookup(hash);
			m_Trees.Append() = NULL;

			//Assertf(m_Trees[x], "Cannot find the tree with the hash %x", hash);
			m_TreeSourceHashes.Append() = hash;
			m_TreeDebugNames.Append();
		}
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::PreAllocateInstances(int nCount)
{
	for( int i = 0; i < m_Trees.GetCount(); i++ )
	{
		PreAllocateInstances(i, nCount);
	}

}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::PreAllocateInstances(int type, int nCount)
{
	Assertf(nCount < m_TreeInstances[type].max_size(), "Too many tree instances in the forest. Can't exceed %d trees.", m_TreeInstances[type].max_size());
	Assert(m_VisibleTreeInstances[type].max_size() == m_TreeInstances[type].max_size());
	Assert(m_VisibleShadowTreeInstances[type].max_size() == m_TreeInstances[type].max_size());

	m_TreeInstances[type].Reserve(nCount);
	m_VisibleTreeInstances[type].Reserve(nCount);
	m_VisibleShadowTreeInstances[type].Reserve(nCount);
}

template<class TREEDATA>
TreeInstance& TreeForestImp<TREEDATA>::AddTreeInstance(int nTreeIndex, const Vector3& vPosition, float UNUSED_PARAM(fRotX), float fRotY, float UNUSED_PARAM(fRotZ), bool bFindFree)
{
	RAGE_TRACK(SpeedTree);

	// See if there is an invalid instance
	int nIndex = -1;
	if( bFindFree )
	{
		Assertf(nTreeIndex < m_TreeInstances.GetCount(), "Using a tree type %d, but there are only %d tree types (or m_TreeInstances has not been initialized properly)", nTreeIndex, m_TreeInstances.GetCount());

		for( int i = 0; i < m_TreeInstances[nTreeIndex].GetCount(); i++ )
		{
			if( !m_TreeInstances[nTreeIndex][i].IsValid() )
			{
				nIndex = i;
				break;
			}
		}
	}
	TreeInstance* inst;
	if( nIndex != -1 )
		inst = &m_TreeInstances[nTreeIndex][nIndex];
	else
	{
		inst = &m_TreeInstances[nTreeIndex].Grow();
		m_VisibleTreeInstances[nTreeIndex].Grow();
		m_VisibleShadowTreeInstances[nTreeIndex].Grow();
	}
	inst->SetPosition(vPosition); 
	//inst->SetRotationX(fRotX);
	inst->SetRotationY(fRotY);
	//inst->SetRotationZ(fRotZ);

	if (m_Trees[nTreeIndex])
	{
		inst->CreatePhysicsInstance(m_Trees[nTreeIndex]->GetPhysicsArchetype());
	}

	return *inst;
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::RemoveLastTreeInstance(int nTreeIndex)
{
	m_TreeInstances[nTreeIndex].Pop();
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::RemoveTreeInstance(int nTreeIndex, int nInstanceIndex)
{
	m_TreeInstances[nTreeIndex][nInstanceIndex].Invalidate();

	// Wipe any invalid instances from the end of the array
	int nRemoveCount = 0;
	int i;
	for( i = m_TreeInstances[nTreeIndex].GetCount() - 1; i >= 0; i-- )
	{
		if( m_TreeInstances[nTreeIndex][i].IsValid() )
			break;

		nRemoveCount++;
	}
	for( i = 0; i < nRemoveCount; i++ )
	{
		m_TreeInstances[nTreeIndex].Pop();
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::RemoveInstance(const Vector3& vPosition)
{
	int nTreeIndex;
	int nInstance = FindInstance(vPosition, nTreeIndex);
	if( nInstance > -1 )
	{
		RemoveTreeInstance(nTreeIndex, nInstance);
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::RemoveAllInstances()
{
	for( int i = 0; i < m_TreeInstances.GetCount(); i++ )
	{
		m_TreeInstances[i].Resize(0);
	}
}


// PURPOSE: Remove all tree hashes of trees that have zero instances.
template<class TREEDATA>
void TreeForestImp<TREEDATA>::RemoveUnusedTreeHashes()
{
	for (int x=0; x<m_TreeInstances.GetCount(); x++)
	{
		if (m_TreeInstances[x].GetCount() == 0)
		{
			m_TreeSourceHashes[x] = 0;
		}
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::RemoveTree(int nTreeIndex)
{
	// Remove all the instances of the tree
	m_TreeInstances[nTreeIndex].Resize(0);
	m_VisibleTreeInstances[nTreeIndex].Resize(0);
	m_VisibleShadowTreeInstances[nTreeIndex].Resize(0);

	// Delete the tree data
	if( m_Trees[nTreeIndex] )
	{
		delete m_Trees[nTreeIndex];
		m_Trees[nTreeIndex] = 0;
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::RemoveAllTrees()
{
	for( int i = 0; i < m_Trees.GetCount(); i++ )
	{
		RemoveTree(i);
	}

	m_Trees.Resize(0);
	m_TreeInstances.Resize(0);
	m_VisibleTreeInstances.Resize(0);
	m_VisibleShadowTreeInstances.Resize(0);
	BANK_ONLY(m_pDebugInfo->m_nTreeTypeCount = 0);
}

template<class TREEDATA>
int TreeForestImp<TREEDATA>::FindTreeInstance(int nTreeIndex, const Vector3& vPosition) const
{
	for( int i = 0; i < m_TreeInstances[nTreeIndex].GetCount(); i++ )
	{
		if( m_TreeInstances[nTreeIndex][i].GetPosition() == vPosition )
		{
			return i;
		}
	}
	return -1;
}

template<class TREEDATA>
int TreeForestImp<TREEDATA>::FindInstance(const Vector3& vPosition, int& nTreeIndex) const
{
	for( int i = 0; i < m_TreeInstances.GetCount(); i++ )
	{
		for( int j = 0; j < m_TreeInstances[i].GetCount(); j++ )
		{
			if( m_TreeInstances[i][j].GetPosition() == vPosition )
			{
				nTreeIndex = i;
				return j;
			}
		}
	}
	return -1;
}

template<class TREEDATA>
const TreeInstance* TreeForestImp<TREEDATA>::GetInstance(int nIndex, int& nTreeIndex, int& nInstanceIndex)
{
	for( int i = 0; i < m_TreeInstances.GetCount(); i++ )
	{
		if( nIndex < m_TreeInstances[i].GetCount() )
		{
			nTreeIndex = i;
			nInstanceIndex = nIndex;
			return &m_TreeInstances[i][nIndex];
		}

		nIndex -= m_TreeInstances[i].GetCount();
	}
	nTreeIndex = -1;
	nInstanceIndex = -1;
	return 0;
}

template<class TREEDATA>
const TreeInstance* TreeForestImp<TREEDATA>::GetInstance(int nTreeIndex, int nInstanceIndex)
{
	return &m_TreeInstances[nTreeIndex][nInstanceIndex];
}


template<class TREEDATA>
int TreeForestImp<TREEDATA>::GetTreeCount() const
{
	int nCount = 0;
	for( int i = 0; i < m_Trees.GetCount(); i++ )
	{
		if( m_Trees[i] )
			nCount++;
	}
	return nCount;
}

template<class TREEDATA>
int TreeForestImp<TREEDATA>::GetInstanceCount() const
{
	int nCount = 0;
	for( int i = 0; i < m_TreeInstances.GetCount(); i++ )
	{
		for( int j = 0; j < m_TreeInstances[i].GetCount(); j++ )
		{
			if( m_TreeInstances[i][j].IsValid() )
				nCount++;
		}
	}
	return nCount;
}
template<class TREEDATA>
int TreeForestImp<TREEDATA>::GetTreeInstanceCount(int nTreeIndex) const
{
	int nCount = 0;
	for( int j = 0; j < m_TreeInstances[nTreeIndex].GetCount(); j++ )
	{
		if( m_TreeInstances[nTreeIndex][j].IsValid() )
			nCount++;
	}
	return nCount;
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::ResetVisibleInstances()
{
	for( int i = 0; i < m_TreeInstances.GetCount(); i++ )
	{
		m_VisibleTreeInstances[i].Resize(0);
		m_VisibleShadowTreeInstances[i].Resize(0);
	}
}

float CalculateImposterDistance( float width, float dist , float texSize, float texRange, float& flod )
{
	float texRes = 64.0f;
	if ( grcViewport::GetCurrent())
	{
		texRes = ( GRCDEVICE.GetWidth() * width )/ ( 2.0f * dist *  grcViewport::GetCurrent()->GetTanHFOV() );
		texRes += texRange * 0.5f;
	}
	float fade = (  texSize  - texRes + texRange * 0.5f) / texRange; //( texRes < 128.0f );
	flod =( texRes - texSize - texRange) / (  GRCDEVICE.GetWidth() * 0.5f - texSize); // max size should be specified-+
	flod = Clamp( flod, 0.0f, 1.0f );
	fade = Clamp( fade, 0.0f, 1.0f );
	return fade;
}

template<class TREEDATA>
template<class VisibilitySelector>
void TreeForestImp<TREEDATA>::UpdateVisiblity( const grcViewport& pViewport, VisibilitySelector vis )
{
	// Compute visibility and lods for each instance
	PF_START(UpdateVisibility);
	for( int i = 0; i < m_TreeInstances.GetCount(); i++ )
	{
		// Set the visible list to be allocated to the max
		int nCount = 0;
		TREEDATA *treeData = m_Trees[i];

		if (treeData)
		{
			m_VisibleTreeInstances[i].Resize(m_VisibleTreeInstances[i].GetCapacity());
			TreeInstance **visibleList = (TreeInstance **) &m_VisibleTreeInstances[i][nCount];

			Vector4 vTreeCullSphere = m_Trees[i]->GetCullSphere();
			vis.SetTexSize( m_Trees[i]->GetBillboardTexture()->GetWidth() );

			for( int j = 0; j < m_TreeInstances[i].GetCount(); j++ )
			{
				TreeInstance& inst = m_TreeInstances[i][j];

				TestAndAddInstance(nCount, inst, vis, vTreeCullSphere, pViewport, visibleList, treeData);
				PF_INCREMENT(TreesInScene);
			}
		}
		// Resize to the final count
		m_VisibleTreeInstances[i].Resize(nCount);
	}
	PF_STOP(UpdateVisibility);
}
template<class TREEDATA>
void TreeForestImp<TREEDATA>::Update(float /*fTime*/, const grcViewport& pViewport)
{
#if SPEEDTREE_GIZMOS
	if( m_bActivateGizmos != m_bGizmosWereActive )
	{
		if( m_bActivateGizmos )
			ActivateGizmos();
		else
			DeactivateGizmos();
		m_bGizmosWereActive = m_bActivateGizmos;
	}
#endif

	// Compute visibility and lods for each instance
	if ( m_useBillboardSizeSelection)
	{
		UpdateVisiblity( pViewport,  CalculateImposterSizeBasedFade( m_billboardBlendRange ) );
	}
	else
	{
		UpdateVisiblity( pViewport, CalculateImposterDistBasedFade( m_fBillboardNearDistance,  m_fBillboardFarDistance) );
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::UpdateShadows(const grcViewport& pLightViewport, const grcViewport& pCamViewport)
{

	// Compute visibility and lods for each instance
	PF_START(UpdateShadowVisibility);
	for( int i = 0; i < m_TreeInstances.GetCount(); i++ )
	{
		// Set the visible list to be allocated to the max
		int nCount = 0;
		TREEDATA *treeData = m_Trees[i];

		if (treeData)
		{
			m_VisibleShadowTreeInstances[i].Resize(m_VisibleShadowTreeInstances[i].GetCapacity());

			Vector4 vTreeCullSphere = m_Trees[i]->GetCullSphere();
			for( int j = 0; j < m_TreeInstances[i].GetCount(); j++ )
			{
				TreeInstance& inst = m_TreeInstances[i][j];

				if( inst.IsValid() )
				{
					Vector4 vLocalCullSphere = vTreeCullSphere;
					Vector4 vPos;
					vPos.SetVector3(inst.GetPosition());
					vPos &= VEC4_ANDW;
					vLocalCullSphere.Add(vPos);
					vPos |= VEC4_ONEW;

					bool bVisible = (pLightViewport.IsSphereVisible(VECTOR4_TO_VEC4V(vLocalCullSphere)) != cullOutside );
					if( bVisible )
					{
						const Vector4 vNearPlane = VEC4V_TO_VECTOR4((pCamViewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_NEAR)));
						Vector4 vZDist = vNearPlane.DotV(vPos);

						// Use the absolute z distance so that we get lod falloff behind the viewer as well as in front
						vZDist.Abs(vZDist);

						
						bool bBillboardOnly = m_fBillboardFarDistance.IsLessThan(vZDist);
						bool bBillboardActive = bBillboardOnly || m_fBillboardNearDistance.IsLessThan(vZDist);
					//	float flod;


						//bBillboardOnly  = CalculateImposterDistance( vTreeCullSphere.w, vZDist.x, (float)m_Trees[i]->GetBillboardTexture()->GetWidth(), 10.0f, flod ) > 0.0f;
						bBillboardOnly = true;
						bBillboardActive = bBillboardOnly;
						inst.SetBillboardOnly( bBillboardOnly);
						inst.SetBillboardActive( bBillboardActive);

						if( !bBillboardOnly )
						{
							vZDist.Min(vZDist, m_fBillboardFarDistance);
							Vector4 vLod = VECTOR4_IDENTITY - (vZDist / m_fBillboardFarDistance);

							inst.SetLod(treeData->GetDiscreetLodLevels(), vLod);
						}

						m_VisibleShadowTreeInstances[i][nCount++] = &inst;
					}
					inst.SetVisible(bVisible);
				}
			}
		}

		// Resize to the final count
		m_VisibleShadowTreeInstances[i].Resize(nCount);
	}
	PF_STOP(UpdateShadowVisibility);
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::SetupDraw(TreeRenderer& /*pRenderer*/)
{
}
template<class TREEDATA>
void TreeForestImp<TREEDATA>::Draw(TreeRenderer& pRenderer, int nDrawPass, int renderOnly)
{
	SetupDraw(pRenderer);

	// Render with pass 0
	pRenderer.SetDrawPassIndex(nDrawPass);

	// Render the trees
	PF_START(Draw);

	PIXBeginN(0, "TreeForestImp::Draw (pass: %d)", nDrawPass);

	for( int i = 0; i < m_VisibleTreeInstances.GetCount(); i++ )
	{
		if (renderOnly == -1 || i == renderOnly)
		{
			if (m_Trees[i])
			{
				int nInstances = m_VisibleTreeInstances[i].GetCount();
				if( nInstances > 0 )
				{
					pRenderer.DrawTrees( m_Trees[i], &m_VisibleTreeInstances[i][0].ptr, nInstances);

					PF_INCREMENTBY(TreesDrawn, nInstances);
				}
			}
		}
	}

	PIXEnd();

	PF_STOP(Draw);
}
template<class TREEDATA>
void TreeForestImp<TREEDATA>::DrawShadows(TreeRenderer& pRenderer, int nDrawPass, int renderOnly)
{
	SetupDraw(pRenderer);

	// Render with pass 1
	pRenderer.SetDrawPassIndex(nDrawPass);

	// Render the trees
	PF_START(DrawShadows);

	PIXBeginN(0, "TreeForestImp::DrawShadows (pass: %d)", nDrawPass);
	
	for( int i = 0; i < m_VisibleTreeInstances.GetCount(); i++ )
	{
		if (renderOnly == -1 || i == renderOnly)
		{
			if (m_Trees[i])
			{
				int nInstances = m_VisibleTreeInstances[i].GetCount();
				if( nInstances > 0 )
				{
					pRenderer.DrawShadowTrees( m_Trees[i], &m_VisibleTreeInstances[i][0].ptr, nInstances, &m_SunLightDirection );
				}
			}
		}
	}

	PIXEnd();

	PF_STOP(DrawShadows);
}


struct SortPairDescending
{
	template<class T>
	bool operator()( const T& a, const T& b ) const
	{
		return a.first > b.first;
	}
};
template<class TREEDATA>
void TreeForestImp<TREEDATA>::RenderTreeBillboard( TREEDATA* tree , Matrix44* /*rockMatrices*/, Matrix44* /*rustleMatrices*/, TreeRenderer& pRenderer)
{
	pRenderer.RenderTreeBillboardTexture( tree, true );
	pRenderer.RenderTreeBillboardTexture( tree, true, true, true, &m_SunLightDirection );
}
template<class TREEDATA>
void TreeForestImp<TREEDATA>::RenderAllTreeBillboards(TreeRenderer& pRenderer)
{
	PIXBegin(0, "TreeForestImp::RenderAllTreeBillboards");

	Matrix44 rockMatrices[32];
	Matrix44 rustleMatrices[32];
	for( int i = 0; i < 32; i++ )
	{
		rockMatrices[i].Identity();
		rustleMatrices[i].Identity();
	}
		
	if ( m_RegenerateAll )
	{
		for( int i = 0; i < m_Trees.GetCount(); i++ )
		{
			if (m_Trees[i])
			{
				RenderTreeBillboard( m_Trees[i], rockMatrices, rustleMatrices, pRenderer );
			}
		}
	}
	else
	{
		for( int i = 0; i < m_Trees.GetCount(); i++ )  // must generate if not generated before
		{
			if (m_Trees[i])
			{
				if ( !m_Trees[i]->IsBillboardGenerated() )
				{
					RenderTreeBillboard( m_Trees[i], rockMatrices, rustleMatrices, pRenderer );
				}
			}
		}

		Assert( m_Trees.GetCount() < 256);
		std::pair<int, TREEDATA* >		sortList[256];
		
		// Choose the billboards with the maximum error   
		// A priority queue may be faster if this shows up in profiling

		u32 currentTime	 = sysTimer::GetSystemMsTime();
		int activeCount = 0;

		for( int i = 0; i < m_Trees.GetCount(); i++ )
		{
			if (m_Trees[i])
			{
				int visibleBias = m_VisibleTreeInstances[i].GetCount() > 0 ?  100000 : 0;

				sortList[i].first = currentTime - m_Trees[i]->GetBillboardGenerationTimeStamp() + visibleBias;
				sortList[i].second = m_Trees[i];
				activeCount++;
			}
		}
		int numCount = Min( m_MaxBillboardsPerFrame, activeCount );
		std::partial_sort( sortList, sortList + numCount, sortList + activeCount, SortPairDescending() );
		
		// now render the first n billboards as they are the most out of date.
		for( int i = 0; i < numCount; i++ )
		{
			RenderTreeBillboard( sortList[i].second, rockMatrices, rustleMatrices, pRenderer );
		}
	}

	PIXEnd();
}

// PURPOSE: Identify the tree type index in this forest by the tree type hash.
// RETURNS: The index within the forest that identifies the given tree type, or
// -1 if this type of tree is not used in this forest.
template<class TREEDATA>
int TreeForestImp<TREEDATA>::IdentifyTreeIndex(u32 uTreeHash) const
{
	// Search for this hash.
	for (int x=m_TreeSourceHashes.GetCount() - 1; x>=0; x--)
	{
		if (m_TreeSourceHashes[x] == uTreeHash)
		{
			// We found it.
			return x;
		}
	}

	// This hash is not present in this forest.
	return -1;
}


#if __BANK
template<class TREEDATA>
void TreeForestImp<TREEDATA>::AddWidgets(bkBank& bank)
{

	bank.AddToggle("Show Gizmos", &m_pDebugInfo->m_bActivateGizmos);

	bank.PushGroup("Load Tree");

	bank.AddSlider("Seed", &m_pDebugInfo->m_nLoadSeed, 0, 0x7FFFFFFF, 1);
	bank.AddSlider("Size", &m_pDebugInfo->m_fLoadSize, -1, 10000.0f, 1.0f);
	bank.AddSlider("Size Var", &m_pDebugInfo->m_fLoadSizeVar, -1, 10000.0f, 1.0f);
	bank.AddButton("Load", datCallback(MFA(TreeForestImp<TREEDATA>::LoadTreeWidget), this));
	bank.AddButton("LoadResourced", datCallback(MFA(TreeForestImp<TREEDATA>::LoadResourceTreeWidget), this));
	
	bank.AddButton("Load RDR2 Forest", datCallback(MFA(TreeForestImp<TREEDATA>::LoadRDR2ForestWidget), this));
	bank.PopGroup();

	bank.PushGroup("Add Instance");

	bank.AddSlider("Tree Type Count", &m_pDebugInfo->m_nTreeTypeCount, 0, 100000, 0);
	bank.AddText("Tree Type", m_pDebugInfo->m_szAddTreeType, sizeof(m_pDebugInfo->m_szAddTreeType));
	bank.AddSlider("Tree Position", &m_pDebugInfo->m_vAddTreePos, -10000.0f, 10000.0f, 0.1f);
	bank.AddButton("Add", datCallback(MFA(TreeForestImp<TREEDATA>::AddInstanceWidget), this));

	bank.PopGroup();


	bank.PushGroup("Billboard Selection");
	bank.AddSlider("Billboard Near Distance", &m_fBillboardNearDistance, 0.0f, 1000000.0f, 1.0f);
	bank.AddSlider("Billboard Far Distance", &m_fBillboardFarDistance, 0.0f, 1000000.0f, 1.0f);

	bank.AddToggle("Use Billboard Size Selection", &m_useBillboardSizeSelection);
	bank.AddSlider("Billboard Blend Range", &m_billboardBlendRange, 0.0f, 256.0f, 1.0f);

	bank.PopGroup();

	bank.AddSlider("Wind Direction X", &m_vWind.x, -1.0f, 1.0f, 0.1f);
	bank.AddSlider("Wind Direction Y", &m_vWind.y, -1.0f, 1.0f, 0.1f);
	bank.AddSlider("Wind Direction Z", &m_vWind.z, -1.0f, 1.0f, 0.1f);
	bank.AddSlider("Wind Strength", &m_vWind.w, 0.0f, 2000.0f, 0.01f);

}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::LoadTreeWidget()
{
	char fullPresetPath[256];
	memset(fullPresetPath, 0, 256);


	if( BANKMGR.OpenFile(fullPresetPath, 256, "*.spt", false, "tree (*.spt)") )
	{
		LoadTree(fullPresetPath, m_pDebugInfo->m_nLoadSeed, m_pDebugInfo->m_fLoadSize, m_pDebugInfo->m_fLoadSizeVar);
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::LoadResourceTreeWidget()
{
	char fullPresetPath[256];
	memset(fullPresetPath, 0, 256);


	if( BANKMGR.OpenFile(fullPresetPath, 256, "*.*", false, "tree Resourced(*.spt)") )
	{
		TreeDataMgr<TREEDATA>::GetInstance().SetUseResource( true );
		LoadTree(fullPresetPath, m_pDebugInfo->m_nLoadSeed, m_pDebugInfo->m_fLoadSize, m_pDebugInfo->m_fLoadSizeVar);
		TreeDataMgr<TREEDATA>::GetInstance().SetUseResource( false );
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::LoadRDR2ForestWidget()
{
	char fullPresetPath[256];
	memset(fullPresetPath, 0, 256);

	if( BANKMGR.OpenFile(fullPresetPath, 256, "*.txt", false, "tree Forest (*.txt)") )
	{
		LoadFromRDR2File(fullPresetPath);
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::AddInstanceWidget()
{
	int treeType = atoi(m_pDebugInfo->m_szAddTreeType);
	Assert(treeType < m_Trees.GetCount() && m_Trees[treeType] );
	if( treeType < m_Trees.GetCount() && m_Trees[treeType] )
	{
#if SPEEDTREE_GIZMOS
		TreeInstance& inst = AddTreeInstance(treeType, m_vAddTreePos);
		if( m_bActivateGizmos )
			inst.ActivateGizmo();
#else
		AddTreeInstance(treeType, m_pDebugInfo->m_vAddTreePos);
#endif
	}
}
#endif

#if SPEEDTREE_GIZMOS

template<class TREEDATA>
void TreeForestImp<TREEDATA>::ActivateGizmos()
{
	for( int i = 0; i < m_TreeInstances.GetCount(); i++ )
	{
		for( int j = 0; j < m_TreeInstances[i].GetCount(); j++ )
		{
			m_TreeInstances[i][j].ActivateGizmo();
		}
	}
}

template<class TREEDATA>
void TreeForestImp<TREEDATA>::DeactivateGizmos()
{
	for( int i = 0; i < m_TreeInstances.GetCount(); i++ )
	{
		for( int j = 0; j < m_TreeInstances[i].GetCount(); j++ )
		{
			m_TreeInstances[i][j].DeactivateGizmo();
		}
	}
}
#endif



// explicit instantiation
template class TreeForestImp<instanceTreeData>;

} // namespace rage
