// 
// speedtree/instance.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPEEDTREE_INSTANCE_H
#define SPEEDTREE_INSTANCE_H

#include "atl/ptr.h"
#include "vector/matrix44.h"
#include "gizmo/translation.h"
#include "physics/inst.h"

// Change this to non-zero if you want to enable gizmos for speedtrees
#define SPEEDTREE_GIZMOS	0

#ifndef SPEEDTREE_PHYSICS

// Change this to non-zero if you want to enable physics instances for speedtrees
#define SPEEDTREE_PHYSICS	1

#endif

namespace rage
{


//=============================================================================
// TreeInstance
//
// PURPOSE
//   TreeInstance is the data needed to exist an instance of a speedtree
//	in the world.
class TreeInstance
{
	// These are the bits for m_uFlags.
	enum
	{
		FLAG_VISIBLE			= (1 << 4),
		FLAG_BILLBOARD_ONLY		= (1 << 5),
		FLAG_BILLBOARD_ACTIVE	= (1 << 6),
		FLAG_VALID				= (1 << 7),
	};

	void SetFlag(unsigned char flagMask, bool setOrClear)
	{
		if (setOrClear) 		{
			m_uFlags |= flagMask;
		} else {
			m_uFlags &= ~flagMask;
		}
	}

public:
	TreeInstance();
	TreeInstance( const TreeInstance& other ) { Copy( other ); }
	TreeInstance(datResource&);
	~TreeInstance();

	DECLARE_PLACE(TreeInstance);

	// PURPOSE: Get the current position of the instance
	// RETURNS: A Vector3 containing the current position of the instance
	Vector3 GetPosition() const									{ return Vector3(m_vPosition.x, m_vPosition.y, m_vPosition.z); }

	// PURPOSE: Get the rotation of the instance in the X axis
	// RETURNS: The rotation of this instance in the X axis. This value is in radians.
	float GetRotationX() const									{ return 0.0f; }

	// PURPOSE: Get the rotation of the instance in the Y axis
	// RETURNS: The rotation of this instance in the Y axis. This value is in radians.
	float GetRotationY() const									{ return m_vPosition.w; }

	// PURPOSE: Get the rotation of the instance in the Z axis
	// RETURNS: The rotation of this instance in the Z axis. This value is in radians.
	float GetRotationZ() const									{ return 0.0f; }

	// PURPOSE: Get the world matrix for this instance
	// RETURNS: A Matrix34 representing the rotation and translation of this instance
	Matrix34 GetMatrix34() const
	{
		Matrix34 retMatrix;
		float ca = cosf(m_vPosition.w);
		float sa = sinf(m_vPosition.w);

		retMatrix.a.Set(ca,   0.0f, -sa );
		retMatrix.b.Set(0.0f, 1.0f, 0.0f);
		retMatrix.c.Set(sa,  0.0f, ca );
		retMatrix.d.Set( m_vPosition.x, m_vPosition.y, m_vPosition.z );
		return retMatrix;
	}

	// PURPOSE: Get the world matrix for this instance
	// RETURNS: A Matrix44 representing the rotation and translation of this instance
	Matrix44 GetMatrix44() const
	{
		Matrix44 retMatrix;

		float ca = cosf(m_vPosition.w);
		float sa = sinf(m_vPosition.w);

		retMatrix.a.Set(ca,   0.0f, sa,   m_vPosition.x);
		retMatrix.b.Set(0.0f, 1.0f, 0.0f, m_vPosition.y);
		retMatrix.c.Set(-sa,  0.0f, ca,   m_vPosition.z);
		retMatrix.d.Set(0.0f, 0.0f, 0.0f, 1.0f);

		Matrix44 transMat;
		transMat.Transpose( retMatrix );

		return transMat;
	}

	// PURPOSE: Get the world matrix for this instance
	// RETURNS: A Matrix44 representing the inverse transpose rotation and translation of this instance
	Matrix44 GetInvTransposeMatrix44() const
	{
		Matrix44 retMatrix;

		float ca = cosf(-m_vPosition.w);
		float sa = sinf(-m_vPosition.w);

		retMatrix.a.Set(ca,   0.0f, sa,   -m_vPosition.x);
		retMatrix.b.Set(0.0f, 1.0f, 0.0f, -m_vPosition.y);
		retMatrix.c.Set(-sa,  0.0f, ca,   -m_vPosition.z);
		retMatrix.d.Set(0.0f, 0.0f, 0.0f, 1.0f);

		Matrix44 transMat;
		transMat.Transpose( retMatrix );
		return transMat;
	}

	// PURPOSE: Test to see if this instance is visible
	// RETURNS: true if this instance is visible, false if this instance is not visible
	bool IsVisible() const										{ return (m_uFlags & FLAG_VISIBLE) != 0; }

	// PURPOSE: Test to see if this instance is only a billboard
	// RETURNS: true if this instance is only a billboard, false otherwise
	bool IsBillboardOnly() const								{ return (m_uFlags & FLAG_BILLBOARD_ONLY) != 0; }

	// PURPOSE: Test to see if this instance should draw a billboard
	// RETURNS: true if this instance should draw a billboard, false otherwise
	bool IsBillboardActive() const								{ return (m_uFlags & FLAG_BILLBOARD_ACTIVE) != 0; }

	// PURPOSE: Test to see if this instance is valid
	// RETURNS: true if the instance is valid, false otherwise
	bool IsValid() const										{ return (m_uFlags & FLAG_VALID) != 0; }

	// PURPOSE: Get the current level of detail
	// RETURNS: The current LOD value between 0.0f (lowest) and 1.0f (highest)
	float GetLod() const										{ return m_fLOD; }

	// PURPOSE: Get the descreet branch level of detail
	// RETURNS: The descreet branch level of detail retrieved from the speedtree runtime
	int GetBranchLod() const									{ return (int) (m_uLodLevels & 0xf); }

	// PURPOSE: Get the descreet frond level of detail
	// RETURNS: The descreet frond level of detail retrieved from the speedtree runtime
	int GetFrondLod() const										{ return (int) (m_uLodLevels >> 4); }

	// PURPOSE: Get the descreet leaf level of detail
	// RETURNS: The descreet leaf level of detail retrieved from the speedtree runtime
	int GetLeafLod() const										{ return (int) (m_uFlags >> 4); }

	// PURPOSE: Return the lod info vector
	// RETURNS: A vector containing the lod information for this instance
	//Vector4 GetLodInfo() const									{ return m_LodInfo; }

	// PURPOSE: Set the current position of this instance
	// PARAMS:
	//	vPos - A Vector3 containing the new position of the instance
	void SetPosition(const Vector3& vPos)						{ m_vPosition = Vector4(vPos.x, vPos.y, vPos.z, m_vPosition.w); }

	// PURPOSE: Set the rotation in the X axis
	// PARAMS:
	//	fRadsX - The rotation in the X axis in radians
	void SetRotationX(float UNUSED_PARAM(fRadsX))				{  }

	// PURPOSE: Set the rotation in the Y axis
	// PARAMS:
	//	fRadsY - The rotation in the Y axis in radians
	void SetRotationY(float fRadsY)								{ m_vPosition.w = fRadsY; }

	// PURPOSE: Set the rotation in the Z axis
	// PARAMS:
	//	fRadsZ - The rotation in the Z axis in radians
	void SetRotationZ(float UNUSED_PARAM(fRadsZ))				{  }

	// PURPOSE: Set the current world matrix of this instance
	// PARAMS:
	//	mMatrix - A Matrix34 containing the new world matrix.
	//void SetMatrix(const Matrix34& mMatrix);

	// PURPOSE: Set the current world matrix of this instance
	// PARAMS:
	//	mMatrix - A Matrix44 containing the new world matrix.
	//void SetMatrix(const Matrix44& mMatrix);

	// PURPOSE: Create the physics instance for this tree
	// PARAMS:
	//	pTreeData - A pointer to the tree data for this tree
	void CreatePhysicsInstance(  phArchetype* pArch );

	// PURPOSE: Set the validity state of this instance to invalid
//	void Invalidate()											{ SetValid(false); }
	void Invalidate()											{ SetFlag(FLAG_VALID, false); }

	// PURPOSE: Set the visibility of this instance
	// PARAMS:
	//	bVisisble - true = visible, false = not visisble
	void SetVisible(bool bVisible)								{ SetFlag(FLAG_VISIBLE, bVisible); }

	// PURPOSE: Set the billboard only state of this instance
	// PARAMS:
	//	bBillboardOnly - true = this instance is only a billboard, false = this instance is not only a billboard
	void SetBillboardOnly(bool bBillboardOnly)					{ SetFlag(FLAG_BILLBOARD_ONLY, bBillboardOnly); }

	// PURPOSE: Set the billboard active state of this instance
	// PARAMS:
	//	bBillboardActive - true = this instance should draw a billboard, false = this instance should not draw a billboard
	void SetBillboardActive(bool bBillboardActive)				{ SetFlag(FLAG_BILLBOARD_ACTIVE, bBillboardActive); }

	// PURPOSE: Set the level of detail of this instance
	// PARAMS:
	//	pTreeData - a speedTreeData object representing the base tree
	//	fLod - The new lod value between 0.0f (lowest) and 1.0f (highest)
	void SetLod(const Vector4&, float fLod);
	__forceinline void SetLodInline( const Vector4& lods, float fLod);
	void SetLod( const Vector4& lods, Vector4::Vector4Param vLod);

	void SetFadeOut( float fade )	{ m_fadeOut = Clamp( fade, 0.0f, 1.0f) ; }
	float GetFadeOut() const { return m_fadeOut; }
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

#if SPEEDTREE_GIZMOS
	void ActivateGizmo();
	void DeactivateGizmo();
#endif


	TreeInstance& operator=( const TreeInstance& other )		{ Copy( other ); return *this; }
private:
	void RecomputeMatrices();
	void Copy( const TreeInstance& other );

protected:
	Vector4				m_vPosition;		// Position with rotation as .w
	float				m_fLOD;
	unsigned char		m_uLodLevels;		// LOD Levels[0-1], 4 bits each
	unsigned char		m_uFlags;			// Flags (see FLAG_ define), and LOD Level[2]
	unsigned short		m_uPad0;

	// will compress later
	float				m_fadeOut;

#if SPEEDTREE_PHYSICS
	atSharedPtr< phInst >	m_pPhysInst;
#endif

#if SPEEDTREE_GIZMOS
	// We have to each the cost of this in all builds if we want resources to be compatible.
	gzTranslation*		m_pGizmo;
#endif

#if !SPEEDTREE_PHYSICS && !SPEEDTREE_GIZMOS
	int					m_Pad1;
#endif

//#if SPEEDTREE_PHYSICS && SPEEDTREE_GIZMOS
//	int					m_Pad2, m_Pad3;
//#endif

#if SPEEDTREE_PHYSICS && !SPEEDTREE_GIZMOS
	int					m_Pad2, m_Pad3, m_Pad4;
#endif


};


inline int DiscreetLodFast( int NumLevels, float fLodLevel )
{
	short sLevel = 0;
	// -1.0f passed in means to use the current interval value
//	FastAssert(fLodLevel != -1.0f);

	int nNumLodLevels = NumLevels;
	sLevel = static_cast<short>((1.0f - fLodLevel) * nNumLodLevels);
	if (sLevel == nNumLodLevels)
		sLevel--;

//	FastAssert(sLevel >= 0 && sLevel < NumLevels);
	return sLevel;
}

__forceinline void TreeInstance::SetLodInline(  const Vector4& lods, float fLod)
{
	m_fLOD = fLod;
	Vector4 Lods = lods;

	m_uLodLevels = (unsigned char) DiscreetLodFast( (int)Lods.x, fLod ) | ( (unsigned char)DiscreetLodFast( (int)Lods.y, fLod ) << 4);
	m_uFlags &= 0xf0;
	m_uFlags |= (unsigned char) DiscreetLodFast( (int)Lods.z, fLod ); 
}





} // namespace rage

#endif // SPEEDTREE_INSTANCE_H
