#define N0_SKINING
#include "rage_tree_common.fxh"

#define UPVECTOR_POS_Y

#if !__XENON
struct vertexSpeedtreeLeaf {
    float4 pos				: POSITION;
    float2 texCoord0		: TEXCOORD0;
    float2 vWindAttribs		: TEXCOORD1;
    float2 vLeafAttribs		: TEXCOORD2;
    float3 normal			: NORMAL;
};
struct vertexSpeedtreeLeafMesh {
    float4 pos				: POSITION;
    float2 texCoord0		: TEXCOORD0;
    float2 vWindAttribs		: TEXCOORD1;
    float2 vLeafAttribs		: TEXCOORD2;
    float3 normal			: NORMAL;
    float3 vOrientX			: TEXCOORD3;
    float3 vOrientY			: TEXCOORD4;
};

#else
struct vertexSpeedtreeLeaf {
    float4 pos				: POSITION;
    float2 vWindAttribs		: TEXCOORD0;
    float2 vLeafAttribs		: TEXCOORD1;
    float3 normal			: NORMAL;
};
struct vertexSpeedtreeLeafMesh {
    float4 pos				: POSITION;
    float2 vWindAttribs		: TEXCOORD0;
    float2 vLeafAttribs		: TEXCOORD1;
    float3 normal			: NORMAL;
    float3 vOrientX			: TEXCOORD3;
    float3 vOrientY			: TEXCOORD4;
};
#endif

float4 g_vTreeRotationTrig = float4( 0.0f, 1.0f, -0.00, 0.0f);

#define NUM_WIND_MATRICES 10
///////////////////////////////////////////////////////////////////////  
//  WindEffect
//
//  New with 4.0 is a two-weight wind system that allows the tree model
//  to bend at more than one branch level.
//
//  In order to keep the vertex size small, the wind parameters have been
//  compressed as detailed here:
//
//      vWindInfo.x = (wind_matrix_index1 * 10.0) / NUM_WIND_MATRICES  + wind_weight1
//      vWindInfo.y = (wind_matrix_index2 * 10.0) / NUM_WIND_MATRICES  + wind_weight2
//
//  * Note that NUM_WIND_MATRICES cannot be larger than 10 in this case

float3 WindEffect(float3 vPosition, float2 vWindInfo)
{
    // decode both wind weights and matrix indices at the same time in order to save
    // vertex instructions
 //   vWindInfo.xy += g_fWindMatrixOffset.xx;
    float2 vWeights = frac(vWindInfo.xy);
    float2 vIndices = (vWindInfo - vWeights) * 0.05f * NUM_WIND_MATRICES;
    
    // first-level wind effect - interpolate between static position and fully-blown
    // wind position by the wind weight value
	float3 v2 = mul(vPosition.xyz, (float3x3)g_amWindMatrices[int(vIndices.x)]).xyz;
    float3 vWindEffect = lerp (vPosition.xyz, v2, vWeights.x);
    
    // second-level wind effect - interpolate between first-level wind position and 
    // the fully-blown wind position by the second wind weight value
#ifdef TWO_WEIGHT_WIND
    return lerp(vWindEffect, mul(vWindEffect, float3x3(g_amWindMatrices[int(vIndices.y)])), vWeights.y);
#else
    return vWindEffect;
#endif
}

// PURPoSE
//  renders the leaves as meshes
//
speedTreeVertexOutput LeafMeshVS(float3   vPosition  : POSITION,   // xyz = position, w = compressed wind param 1
                       float4   vTexCoord0 : TEXCOORD0,  // xy = diffuse texcoords, z = wind angle index, w = dimming
                       float3   vOrientX   : TEXCOORD1,  // xyz = vector xyz
                       float3   vOrientZ   : TEXCOORD2,  // xyz = vector xyz
                       float4   vOffset    : TEXCOORD3,  // xyz = mesh placement position, w = compressed wind param 2
                       float3   vNormal    : NORMAL)     // xyz = normal xyz
{
    // this will be fed to the leaf pixel shader
    speedTreeVertexOutput sOutput=(speedTreeVertexOutput)0;
    
    // define attribute aliases for readability
    float fWindAngleIndex = vTexCoord0.z;       // which wind matrix this leaf card will follow
    float fDimming = vTexCoord0.w;              // interior leaves are darker (range = [0.0,1.0])
    float2 vWindParams = float2( vOffset.w, 0.0f);
    
    // vOffset represents the location where the leaf mesh will be placed - here it is rotated into place
    // and has the wind effect motion applied to it
#ifdef UPVECTOR_POS_Y
  //  vOffset.xz = float2(dot(g_vTreeRotationTrig.ywz, vOffset.xyz), dot(g_vTreeRotationTrig.xwy, vOffset.xyz));
#else
  //  vOffset.xy = float2(dot(g_vTreeRotationTrig.yxw, vOffset.xyz), dot(g_vTreeRotationTrig.zyw, vOffset.xyz));
#endif
   // vOffset.xyz = WindEffect(vOffset.xyz, vWindParams);
    
		  // Calculate leaf center postition with wind
	vOffset.xyz = WindEffect( vOffset.xyz, vWindParams ); //SpeedtreeComputeWind( vOffset.xyz, 0.0f, vOffset.w);      
 

    // compute rock and rustle values (all trees share the g_avLeafAngles table), but g_vLeafAngleScalars
    // scales the angles to match wind settings specified in SpeedTreeCAD
    /*float2 vLeafRockAndRustle = 1g_vLeafAngleScalars.xy * g_avLeafAngles[fWindAngleIndex].xy;
    
    // vPosition stores the leaf mesh geometry, not yet put into place at position vOffset.
    // leaf meshes rock and rustle, which requires rotations on two axes (rustling is not
    // useful on leaf mesh geometry)
    float3x3 matRockRustle = RotationMatrix_xAxis(vLeafRockAndRustle.x); // rock
    vPosition.xyz = mul(matRockRustle, vPosition.xyz);
    */
    
    // build mesh orientation matrix - cannot be done beforehand on CPU due to wind effect / rotation order issues.
    // it is used to orient each mesh into place at vOffset
    float3 vOrientY = cross(vOrientX, vOrientZ);
#ifdef UPVECTOR_POS_Y
    vOrientY = -vOrientY;
#endif
    float3x3 matOrientMesh =
    {
        vOrientX, vOrientY, vOrientZ
    };
    

    // apply orientation matrix to the mesh positon & normal
    vPosition.xyz = mul(matOrientMesh, vPosition.xyz);
    vNormal.xyz = mul(matOrientMesh, vNormal.xyz);
    
    // rotate the whole tree (each tree instance can be uniquely rotated) - use optimized z-axis rotation
    // algorithm where float(sin(a), cos(a), -sin(a), 0.0f) is uploaded instead of angle a
#ifdef UPVECTOR_POS_Y
   // vPosition.xz = float2(dot(g_vTreeRotationTrig.ywz, vPosition.xyz), dot(g_vTreeRotationTrig.xwy, vPosition.xyz));
   // vNormal.xz = float2(dot(g_vTreeRotationTrig.ywz, vNormal.xyz), dot(g_vTreeRotationTrig.xwy, vNormal.xyz));
#else
   vPosition.xy = float2(dot(g_vTreeRotationTrig.yxw, vPosition.xyz), dot(g_vTreeRotationTrig.zyw, vPosition.xyz));
    vNormal.xy = float2(dot(g_vTreeRotationTrig.yxw, vNormal.xyz), dot(g_vTreeRotationTrig.zyw, vNormal.xyz));
#endif

    // put oriented mesh into place at rotated and wind-affected vOffset
    vPosition.xyz += vOffset.xyz;

    // scale the geometry (each tree instance can be uniquely scaled)
    //vPosition.xyz *= g_fTreeScale;
    
    // translate tree into position (must be done after the rotation)
    //vPosition.xyz += g_vTreePos.xyz;
    
    // compute the leaf lighting (not using normal mapping, but per-vertex lighting)
  //  sOutput.vColor.rgb = fDimming * LightDiffuse(vPosition.xyz, vNormal.xyz, g_vLightDir, g_vLightDiffuse.rgb, g_vMaterialDiffuse.rgb);
//    sOutput.vColor.a = 1.0f;

    // project position to the screen
	vPosition.xyz = mul( float4( vPosition.xyz, 1.0f), gLeafWorldMatrix).xyz;
    sOutput.pos = mul( float4(vPosition.xyz, 1.0f), gWorldViewProj);
    
    // pass through other texcoords exactly as they were received
    sOutput.texCoord.xy = vTexCoord0.xy;

	sOutput.worldNormal =   mul( vNormal.xyz,(float3x3) gWorld);
	
	sOutput.worldEyePos.xyz	= vPosition.xyz;
	sOutput.worldEyePos.w = g_treeCentre.w;
#ifdef USE_FOG      
    // calc fog (cheap in vertex shader, relatively expensive later in the pixel shader)
    sOutput.fFog = FogValue(sOutput.vPosition.z);
#endif

	sOutput.texCoord.zw= float2(  fDimming , g_TreeAlpha.x );


    return sOutput;
}


// PURPoSE
//  renders the leaves as meshes
//
speedTreeVertexOutput LeafMeshVS_NoWind(float3   vPosition  : POSITION,   // xyz = position, w = compressed wind param 1
                       float4   vTexCoord0 : TEXCOORD0,  // xy = diffuse texcoords, z = wind angle index, w = dimming
                       float3   vOrientX   : TEXCOORD1,  // xyz = vector xyz
                       float3   vOrientZ   : TEXCOORD2,  // xyz = vector xyz
                       float4   vOffset    : TEXCOORD3,  // xyz = mesh placement position, w = compressed wind param 2
                       float3   vNormal    : NORMAL)     // xyz = normal xyz
{
    // this will be fed to the leaf pixel shader
    speedTreeVertexOutput sOutput=(speedTreeVertexOutput)0;
    
    // define attribute aliases for readability
    float fWindAngleIndex = vTexCoord0.z;       // which wind matrix this leaf card will follow
    float fDimming = vTexCoord0.w;              // interior leaves are darker (range = [0.0,1.0])
    float2 vWindParams = float2( vOffset.w, 0.0f);
    
    // Calculate leaf center postition with wind
	vOffset.xyz = WindEffect( vOffset.xyz, vWindParams ); //SpeedtreeComputeWind( vOffset.xyz, 0.0f, vOffset.w);      
 
    
    // build mesh orientation matrix - cannot be done beforehand on CPU due to wind effect / rotation order issues.
    // it is used to orient each mesh into place at vOffset
    float3 vOrientY = cross(vOrientX, vOrientZ);
#ifdef UPVECTOR_POS_Y
    vOrientY = -vOrientY;
#endif
    float3x3 matOrientMesh =
    {
        vOrientX, vOrientY, vOrientZ
    };
    

    // apply orientation matrix to the mesh positon & normal
    vPosition.xyz = mul(matOrientMesh, vPosition.xyz);
    vNormal.xyz = mul(matOrientMesh, vNormal.xyz);
    
    // rotate the whole tree (each tree instance can be uniquely rotated) - use optimized z-axis rotation
    // algorithm where float(sin(a), cos(a), -sin(a), 0.0f) is uploaded instead of angle a
#ifdef UPVECTOR_POS_Y
#else
   vPosition.xy = float2(dot(g_vTreeRotationTrig.yxw, vPosition.xyz), dot(g_vTreeRotationTrig.zyw, vPosition.xyz));
    vNormal.xy = float2(dot(g_vTreeRotationTrig.yxw, vNormal.xyz), dot(g_vTreeRotationTrig.zyw, vNormal.xyz));
#endif

    // put oriented mesh into place at rotated and wind-affected vOffset
    vPosition.xyz += vOffset.xyz;

     // project position to the screen
	vPosition.xyz = mul( float4( vPosition.xyz, 1.0f), gLeafWorldMatrix);
    sOutput.pos = mul( float4(vPosition.xyz, 1.0f), gWorldViewProj);
    
    // pass through other texcoords exactly as they were received
    sOutput.texCoord.xy = vTexCoord0.xy;

	sOutput.worldNormal =   mul( vNormal.xyz,(float3x3) gWorld);
	
	sOutput.worldEyePos.xyz	= vPosition.xyz;
	sOutput.worldEyePos.w = g_treeCentre.w;
#ifdef USE_FOG      
    // calc fog (cheap in vertex shader, relatively expensive later in the pixel shader)
    sOutput.fFog = FogValue(sOutput.vPosition.z);
#endif
	sOutput.texCoord.zw = float2(  fDimming, g_TreeAlpha.x );

    return sOutput;
}

float2	gLeafUVTable[16]: LeafUVTable;


#if __XENON
speedTreeVertexOutput VS_Transform_Speedtree_Leaf(int i : INDEX)
{
	int index = i % 4;
	int uvIndex = i % 16;
	int vertex = i / 4;
	float4 center4;
	float4 normal4;
	float4 texcoord4;
	float4 windattribs4;
	float4 leafattribs4;
	
	asm {
		vfetch	center4, vertex, position0
		vfetch	normal4, vertex, normal0
		vfetch	windattribs4, vertex, texcoord0
		vfetch	leafattribs4, vertex, texcoord1
	};
	 speedTreeVertexOutput OUT = (speedTreeVertexOutput)0;
    
    // Calculate leaf center postition with wind
	float3 vVertexPosition = SpeedtreeComputeWind(center4, windattribs4.x, windattribs4.y);      
 
	// Calculate the position of the leave vertex in world space
	leafattribs4.x += index;
    vVertexPosition = ComputeLeafVertex(vVertexPosition, leafattribs4.xy);
										
    // Write out final position & texture coords
    OUT.pos =  mul(float4(vVertexPosition.xyz, 1), gWorldViewProj);
    OUT.worldNormal = normalize(mul( normal4.xyz, (float3x3)gWorld));//normalize(mul(IN.normal, (float3x3)gWorldInvTrans));
    OUT.texCoord.xy = gLeafUVTable[ uvIndex ] ;
    OUT.worldEyePos.xyz = vVertexPosition.xyz;
	OUT.worldEyePos.w = 1.0f;
    
	FILL_SHADOW_PARAMS(vVertexPosition.xyz);
    OUT.texCoord.zw= float2(  1.0f , g_TreeAlpha.x );

	return OUT;
}
speedTreeVertexOutput VS_Transform_Speedtree_Leaf_NoWind(int i : INDEX)
{
	int index = i % 4;
	int uvIndex = i % 16;
	int vertex = i / 4;
	float4 center4;
	float4 normal4;
	float4 texcoord4;
	float4 windattribs4;
	float4 leafattribs4;
	
	asm {
		vfetch	center4, vertex, position0
		vfetch	normal4, vertex, normal0
		vfetch	windattribs4, vertex, texcoord0
		vfetch	leafattribs4, vertex, texcoord1
	};
	 speedTreeVertexOutput OUT = (speedTreeVertexOutput)0;
    
    // Calculate the position of the leave vertex in world space
	leafattribs4.x += index;
   	float3  vVertexPosition = ComputeLeafVertex(center4, leafattribs4.xy);
										
    // Write out final position & texture coords
    OUT.pos =  mul(float4(vVertexPosition.xyz, 1), gWorldViewProj);
    OUT.worldNormal = normalize(mul( normal4.xyz, (float3x3)gWorld));//normalize(mul(IN.normal, (float3x3)gWorldInvTrans));
    OUT.texCoord.xy = gLeafUVTable[ uvIndex ] ;
    OUT.worldEyePos.xyz = vVertexPosition.xyz;
	OUT.worldEyePos.w = g_treeCentre.w;
    OUT.texCoord.zw= float2(  1.0f , g_TreeAlpha.x );

	return OUT;
}
#else
	
speedTreeVertexOutput VS_Transform_Speedtree_Leaf(vertexSpeedtreeLeaf IN)
{
    speedTreeVertexOutput OUT = (speedTreeVertexOutput)0;
    
    // Calculate leaf center postition with wind
	float3 vVertexPosition = SpeedtreeComputeWind(IN.pos.xyz, IN.vWindAttribs.x, IN.vWindAttribs.y);      
 
	// Calculate the position of the leave vertex in world space
    vVertexPosition = ComputeLeafVertex(vVertexPosition, IN.vLeafAttribs);
										
    // Write out final position & texture coords

    OUT.pos =  mul(float4(vVertexPosition.xyz, 1.0f), gWorldViewProj);
    OUT.worldNormal = normalize(mul(IN.normal, (float3x3)gWorld));//normalize(mul(IN.normal, (float3x3)gWorldInvTrans));
	OUT.texCoord.xy = IN.texCoord0;
  
    OUT.worldEyePos.xyz = vVertexPosition.xyz;
	OUT.worldEyePos.w = g_treeCentre.w;
    
	FILL_SHADOW_PARAMS(vVertexPosition.xyz);
    OUT.texCoord.zw= float2(  1.0f , g_TreeAlpha.x );
    
 
   	float3 worldNormal = mul(IN.normal, (float3x3)gWorld);
    
    OUT.ExtraData.x = saturate(dot(worldNormal,-gLightPosDir[0].xyz));  // dot product of normal and light source
    OUT.ExtraData.y = 0.0f; //saturate((length(vVertexPosition.xyz - gViewInverse[3].xyz) - fadeOutLastSMap.y) / fadeOutLastSMap.x);  // fade value...
    
	return OUT;
}
speedTreeVertexOutput VS_Transform_Speedtree_Leaf_NoWind(vertexSpeedtreeLeaf IN)
{
	speedTreeVertexOutput OUT = (speedTreeVertexOutput)0;
    
    
	// Calculate the position of the leave vertex in world space
    float3 vVertexPosition = ComputeLeafVertex(IN.pos.xyz, IN.vLeafAttribs);
										
    // Write out final position & texture coords

    OUT.pos =  mul(float4(vVertexPosition.xyz, 1), gWorldViewProj);
    OUT.worldNormal = normalize(mul(IN.normal, (float3x3)gWorldInvTrans));
    OUT.texCoord.xy = IN.texCoord0;

    OUT.worldEyePos.xyz = vVertexPosition.xyz;
	OUT.worldEyePos.w = g_treeCentre.w;
    
	FILL_SHADOW_PARAMS(vVertexPosition.xyz);
     OUT.texCoord.zw= float2(  1.0f , g_TreeAlpha.x );
	return OUT;
}
#endif


speedTreeVertexDepthOutput VS_Transform_Speedtree_Leaf_Depth(vertexSpeedtreeLeaf IN)
{
    speedTreeVertexDepthOutput OUT;
    
    // Calculate leaf center postition with wind
	float3 vVertexPosition = SpeedtreeComputeWind(IN.pos.xyz, IN.vWindAttribs.x, IN.vWindAttribs.y);
	
    // Calculate the position of the leave vertex in world space
    vVertexPosition = ComputeLeafVertex(vVertexPosition, IN.vLeafAttribs);
										
    // Write out final position & texture coords
    OUT.pos =  mul(float4(vVertexPosition.xyz, 1), gWorldViewProj);
  #if !__XENON
    OUT.texCoord.xy = IN.texCoord0.xy;
 #else
    OUT.texCoord.xy = 1.0f;
 #endif
 
 
   	float3 worldNormal = mul(IN.normal, (float3x3)gWorld);
    
    OUT.ExtraData.x = saturate(dot(worldNormal,-gLightPosDir[0].xyz));  // dot product of normal and light source
    OUT.ExtraData.y = 0.0f; //saturate((length(vVertexPosition.xyz - gViewInverse[3].xyz) - fadeOutLastSMap.y) / fadeOutLastSMap.x);  // fade value...
 
	return OUT;
}

technique draw
{
	pass p0 
	{  
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaFunc = Greater;
		CullMode = None;

		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	}
	
	pass p1
	{
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaFunc = Greater;
		CullMode = None;
		
#if __XENON
		VertexShader = compile VERTEXSHADER LeafMeshVS();
#else
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
#endif
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	
		// Shadow pass, disable color writes and enable depth writes
	}
	
#if SHADOWS
	pass p2
	{
		// Blended Shadowmaps
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;
		AlphaTestEnable = true;  
		AlphaFunc = Greater;
		CullMode = None;
		ZEnable = true;
		
		AlphaBlendEnable = true;
		SrcBlend = Zero;
		DestBlend = SrcColor;
		
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree_BlendShadow();
	}
	
	pass p3
	{
		// Slow shadowmap technique
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaFunc = Greater;
		CullMode = None;
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree_Shadow();
	}
#else // SHADOWS

	// ** NOTE ** THESE ARE DUMMY SHADERS.
	// THEY ARE PLACED HERE SO THE INDEX OF pass p4 REMAINS THE SAME
	// WITH AND WITHOUT SHADOWS. USING THESE SHADERS IN A BUILD WITHOUT
	// SHADOWS IS NOT DEFINED, AND, FRANKLY SPEAKING, DUMB.

	pass p2 
	{  
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
	//	AlphaRef = 84;
		AlphaFunc = Greater;
		CullMode = None;
		
#if !__PS3
#endif
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	}
	
	pass p3
	{
		// Shadow pass, disable color writes and enable depth writes
		ColorWriteEnable = 0;
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
	//	AlphaRef = 84;
		AlphaFunc = Greater;
		
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf_Depth();
		PixelShader = compile PIXELSHADER PS_DepthCutout_Speedtree();
	}

#endif // SHADOWS
	
	pass p4			
	{  
		// no wind
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaFunc = Greater;
		CullMode = None;
		ColorWriteEnable1=0xff;

		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf_NoWind(); //VS_Transform_Speedtree_Leaf_NoWind();
		PixelShader  = compile PIXELSHADER PS_Textured_SpeedtreeMultiBillboard();
	}
	pass p5			
	{  
		// no wind
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaFunc = Greater;
		CullMode = None;

#if __XENON
		VertexShader = compile VERTEXSHADER LeafMeshVS_NoWind();
#else
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
#endif
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	}
}

// PURPOSE
//	This technique is the default way imposters are created. It has two passes to allow for leaf meshes
//
technique ImpostorDefault
{
	pass p0			
	{  
		// no wind
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaFunc = Greater;
		CullMode = None;
		ColorWriteEnable1=0xff;

		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf_NoWind(); //VS_Transform_Speedtree_Leaf_NoWind();
		PixelShader  = compile PIXELSHADER PS_Textured_Speedtree();
	}
	pass p1			
	{  
		// no wind
		AlphaBlendEnable = false;
		AlphaTestEnable = true;  
		AlphaFunc = Greater;
		CullMode = None;

#if __XENON
		VertexShader = compile VERTEXSHADER LeafMeshVS();
#else
		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf();
#endif
		PixelShader  = compile PIXELSHADER  PS_Textured_Speedtree();
	}
}

// PURPOSE
//	This technique allows for creating a Normal mapped imposter. It has two passes to allow for leaf meshes
//
technique ImpostorNormalMapped
{
	pass p0			
	{  
#if __PS3	// on PS3 set alpha blending as MRT doesn't work with Alpha ref
		AlphaBlendEnable = true;
#else
		AlphaBlendEnable = false;
#endif
		AlphaTestEnable = false; 
		AlphaFunc = Greater;
		CullMode = None;
		ColorWriteEnable1=0xff;

		VertexShader = compile VERTEXSHADER VS_Transform_Speedtree_Leaf_NoWind(); //VS_Transform_Speedtree_Leaf_NoWind();
		PixelShader  = compile PIXELSHADER PS_Textured_SpeedtreeMultiBillboard();
	}
	pass p1			
	{  
#if __PS3	// on PS3 set alpha blending as MRT doesn't work with Alpha ref
		AlphaBlendEnable = true;
#else
		AlphaBlendEnable = false;
#endif
		AlphaTestEnable = false;  
		AlphaFunc = Greater;
		CullMode = None;

		VertexShader = compile VERTEXSHADER LeafMeshVS();
		PixelShader  = compile PIXELSHADER  PS_Textured_SpeedtreeMultiBillboard();
	}
}

