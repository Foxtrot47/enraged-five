// 
// speedtree/forest_grid.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SPEEDTREE_FOREST_GRID_H
#define SPEEDTREE_FOREST_GRID_H

#include "tree/forest.h"

namespace rage
{
	class spdSphere;

struct TreeForestGridCell
{
	TreeForestGridCell();
	TreeForestGridCell(datResource& rsc);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

	DECLARE_PLACE(TreeForestGridCell);

	Vector4											m_BoundSphere;
	atArray<atArray<TreeInstance > >			m_InstanceList;
	bool											m_Empty;
	bool											m_Finalized;
	char											m_Pad[6];
};

//=============================================================================
// TreeForestGrid
//
// PURPOSE
//   TreeForestGrid manages a forest using a grid culling system.  Each instance is
//	put into a grid cell based on its position.  Entire grid cells are rejected with a
//	single sphere visiblity test in the Update function
class TreeForestGrid : public TreeForest
{
public:
	TreeForestGrid(int nLeft, int nRight , int nTop, int nBottom, int nWidthStep, int nHeightStep, bool bYUp = true);
	TreeForestGrid(datResource&);
	~TreeForestGrid();

	DECLARE_PLACE(TreeForestGrid);

	enum
	{
		RORC_VERSION = TreeForest::RORC_VERSION + 6
	};


	// PURPOSE: Load a tree from a .spt file
	// PARAMS:
	//	szTreeFile - A string containing the path of the tree file to load
	// RETURNS: An integer containing the index of the loaded tree, -1 if there is an error
	int LoadTree(const char* szTreeFile, int nSeed = 0, float fSize = 1.0f, float fSizeVariance = 0.0f, bool bCreateBounds = true, bool bOfflineCreation = false);

	// PURPOSE: Create an instance of a tree
	// PARAMS:
	//	nTreeIndex - The index of the tree to create an instance of
	//	vPosition - The position of the instance
	//	fRotX - The rotation in the x axis in radians
	//	fRotY - The rotation in the y axis in radians
	//	fRotZ - The rotation in the z axis in radians
	// RETURNS: The TreeInstance object of the newly created instance
	TreeInstance& AddTreeInstance(int nTreeIndex, const Vector3& vPosition, float fRotX = 0.0f, float fRotY = 0.0f, float fRotZ = 0.0f);

	// PURPOSE: Remove the last instance of the specified tree
	// PARAMS:
	//	nTreeIndex - The index of the tree whos last instance is to be removed
	void RemoveLastTreeInstance(int nTreeIndex);

	// PURPOSE: Rebuild the grid instance list.
	// NOTES:
	//	Call this function anytime you add or remove instances.  It only needs to be called once when
	//	you are done adding or removing instances.
	void RemapGridInstances();

	// PURPOSE: Remove all tree hashes of trees that have zero instances.
	virtual void RemoveUnusedTreeHashes();

	// PURPOSE: Hook this forest up to a tree data manager. Use this if you are using an external tree data resource
	// rather than calling LoadTree(). DO NOT use this if you're using LoadTree().
	void AttachToManager(instanceTreeDataMgr &manager, atArray<u32> *treeTypeHashes = NULL);

	// PURPOSE: Update the forest
	// PARAMS:
	//	fTime - The amount of time elapsed since the beginning of time
	//	pViewport - The viewport to use for frustum culling
	//  Shadows - if True, this is the shadow pass. Billboards will be used for everything.
	// NOTES:
	//	This function updates speedwind.  This function also does visibility tests
	//	with the given viewport setting up the visibility and lods for each instance.
	void Update(float fTime, const grcViewport& pViewport, bool Shadows = false);

	// PURPOSE: Update the forest, within a restricted range of cells.
	// PARAMS:
	//	fTime - The amount of time elapsed since the beginning of time
	//	pViewport - The viewport to use for frustum culling
	//	nW1 - The first cell in the W dimension to consider.
	//  nW2 - The last cell in the W dimension to consider.
	//	nH1 - The first cell in the H dimension to consider.
	//  nH2 - The last cell in the H dimension to consider.
	// NOTES:
	//	This works like Update(float, const grcViewport&), except that the user
	//	choose a rectangular region of cells to update. Cells outside of that
	//	region are considered not visible.
	//	Note that the end of the range is inccludive, i.e. nW1 = 4, nW2 = 6
	//  will include cells w = 4, w = 5, and w = 6.
	//  Also note that if !m_bEnableGrid, the whole forest will be updated,
	//  disregarding the range passed in by the user.
	void Update(float fTime, const grcViewport& pViewport, int nW1, int nW2,
			int nH1, int nH2, bool Shadows);

	// PURPOSE: Remove all trees within a certain radius.
	void ClearAreaOfTrees(const spdSphere &sphere);

	template<bool ShadowsEnabled, class VisibilitySelector>
	void UpdateInternal(float fTime, const grcViewport& pViewport, int nW1, int nW2,
		int nH1, int nH2, VisibilitySelector vis);

	// PURPOSE:	Get the height of the grid.
	// RETURNS:	The number of cells along the height of the grid.
	int GetCellsH() const
	{	return m_nCellsH;	}

	// PURPOSE:	Get the width of the grid.
	// RETURNS:	The number of cells along the width of the grid.
	int GetCellsW() const
	{	return m_nCellsW;	}

	const TreeForestGridCell &GetForestGridCell(int nCellW, int nCellH) const
	{
		FastAssert(nCellW < GetCellsW());
		FastAssert(nCellH < GetCellsH());
		return m_GridCells[nCellW][nCellH];
	}

	// PURPOSE:	Get the current value of the "entire cell cull" option.
	// RETURNS:	True if culling of entire cells is enabled.
	bool GetEntireCellCull() const
	{	return m_bEntireCellCull;	}

	// PURPOSE:	Set the current value of the "entire cell cull" option.
	// PARAMS:
	//	enable - If true, enables culling of entire cells.
	void SetEntireCellCull(bool enable)
	{	m_bEntireCellCull = enable;	}

	// PURPOSE:	Get the coordinates of the grid cell where a position is in.
	// PARAMS:
	//	vPosition - the position to find cell coordinates of.
	//	nCellW - Output of the cell coordinate in the W dimension.
	//	nCellH - Output of the cell coordinate in the H dimension.
	void GetGridCell(const Vector3& vPosition, int& nCellW, int& nCellH);

	// RETURNS: The boundaries of this grid.
	void GetBoundaries(int &nLeft, int &nRight, int &nTop, int &nBottom) const;

#if __BANK
	void AddWidgets(class bkBank& bank);
#endif

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

#if __DEV
	void DebugDraw(bool drawGrid, bool drawCellBoundSpheres) const;
#endif

protected:
	void ComputeBoundingSpheres();
	void ComputeBoundingSphereForCell(int nGridW, int nGridH);

	int											m_nLeft;
	int											m_nRight;
	int											m_nTop;
	int											m_nBottom;
	int											m_nWidthStep;
	int											m_nHeightStep;
	int											m_nWidth;
	int											m_nHeight;
	int											m_nCellsW;
	int											m_nCellsH;
	bool										m_bYUp;
	bool										m_bEnableGrid;
	bool										m_bEntireCellCull;
	char										m_Pad;

	atArray<atArray<TreeForestGridCell> >	m_GridCells;

	int											m_Pad1, m_Pad2, m_Pad3;
};

} // namespace rage

#endif // SPEEDTREE_FOREST_GRID_H
