// 
// speedtree/treedata.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "treedata.h"

#include "constants.h"
#include "tree_rsc.h"

#include "atl/array_struct.h"
#include "atl/ptr.h"
#include "data/safestruct.h"
#include "diag/tracker.h"
#include "file/asset.h"
#include "file/stream.h"
#include "grcore/fvf.h"
#include "grcore/indexbuffer.h"
#include "grcore/texture.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/imposter.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "phbound/boundcomposite.h"
#include "system/memory.h"
#include "system/platform.h"
#include "system/codecheck.h"
#include "vector/geometry.h"
#include "rmcore/drawable.h"
#include <time.h>


#if __PPU
#include <time.h>
// Temporary body for time function until sony actually adds it to their gcc distribution
namespace std {
time_t time(time_t*)
{
	return 1;
}
}
#endif // __PPU

#define REMOVE_SPEEDTREE_MEM 1
#define NO_SPEEDTREE_RT	1

namespace rage
{


IMPLEMENT_PLACE(TreeDataBase);

#if __DECLARESTRUCT
void TreeDataBase::DeclareStruct(datTypeStruct& s)
{
	pgBaseRefCounted::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(TreeDataBase,pgBaseRefCounted)
	SSTRUCT_END(TreeDataBase)
}
#endif



IMPLEMENT_PLACE(InstancedVerts);
InstancedVerts::InstancedVerts(datResource& rsc) :
m_positions(rsc, 1), 
m_normals(rsc, 1),
m_uvs(rsc, 1)
{
}

#if __DECLARESTRUCT
void InstancedVerts::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN_BASE(InstancedVerts, datBase)
		SSTRUCT_FIELD(InstancedVerts, m_positions)
		SSTRUCT_FIELD(InstancedVerts, m_normals)
		SSTRUCT_FIELD(InstancedVerts, m_uvs)
	SSTRUCT_END(InstancedVerts)
}
#endif



template<class ContextType, int MaxSize = 32>
class sysContextMessage
{
	static atFixedArray< const char*, MaxSize>  m_messages;
public:
	sysContextMessage( const char* message )
	{
		m_messages.Push( message );
	}
	~sysContextMessage()
	{
		m_messages.Pop();
	}
};


//------------------- Instanced Tree
bool instanceTreeData::Create( const char* drawableName )
{
	m_Drawable = rage_new rmcDrawable();
	if ( !m_Drawable->Load( drawableName ) )
	{
		delete m_Drawable;
		m_Drawable = 0;
		return false;
	}
	return true;
}
Vector4 instanceTreeData::GetCullSphere() const
{ 
	Vector4 sphere;
	If_Assert( m_Drawable)
	{
		sphere.SetVector3( m_Drawable->GetLodGroup().GetCullSphere() );
		sphere.w = m_Drawable->GetLodGroup().GetCullRadius();
	}
	return sphere;
}
void instanceTreeData::GetBoundingBox( Vector3& minPos, Vector3& maxPos ) const
{ 
	If_Assert( m_Drawable )
	{
		m_Drawable->GetLodGroup().GetBoundingBox( minPos, maxPos );
	}
}


}	 // namespace rage

