// 
// speedtree/forest_grid.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tree/forest_grid.h"

#include "atl/array_struct.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "diag/tracker.h"
#include "grcore/viewport.h"
#include "profile/profiler.h"
#include "spatialdata/sphere.h"

#if __DEV
#include "grcore/im.h"	// Needed for some debug drawing. /FF
#endif

namespace rageTreeForestStats
{
	EXT_PF_TIMER(UpdateVisibility);
	EXT_PF_TIMER(UpdateWind);
}
using namespace rageTreeForestStats;

namespace rage
{

IMPLEMENT_PLACE(TreeForestGridCell);


TreeForestGrid::TreeForestGrid(int nLeft, int nRight, int nTop, int nBottom, int nWidthStep, int nHeightStep, bool bYUp) :
	m_nLeft(nLeft),
	m_nRight(nRight),
	m_nTop(nTop),
	m_nBottom(nBottom),
	m_nWidthStep(nWidthStep),
	m_nHeightStep(nHeightStep),
	m_bYUp(bYUp)
{
	RAGE_TRACK(SpeedTreeGrid);

	m_bEnableGrid = true;
	m_bEntireCellCull = true;

	// Create the grid
	m_nWidth = m_nRight - m_nLeft;
	m_nHeight = m_nBottom - m_nTop;
	m_nCellsW = (int)(m_nWidth / m_nWidthStep);
	m_nCellsH = (int)(m_nHeight / m_nHeightStep);
	if( m_nWidth % m_nWidthStep )
		m_nCellsW++;
	if( m_nHeight % m_nHeightStep )
		m_nCellsH++;
    
	m_GridCells.Resize(m_nCellsW);
	for( int i = 0; i < m_nCellsW; i++ )
	{
		m_GridCells[i].Resize(m_nCellsH);
		for( int j = 0; j < m_nCellsH; j++ )
		{
			// Calculate the bounding sphere of this grid cell
			Vector3 vCorner;
			Vector3 vCenter;

			if( m_bYUp )
			{
				vCorner.Set((float)(nLeft + (i * m_nWidthStep)), 0.0f, (float)(nTop + (j * m_nHeightStep)));
				vCenter = vCorner + Vector3((float)m_nWidthStep * 0.5f, 0.0f, (float)m_nHeightStep * 0.5f);
			}
			else
			{
				vCorner.Set((float)(nLeft + (i * m_nWidthStep)), (float)(nTop + (j * m_nHeightStep)), 0.0f);
				vCenter = vCorner + Vector3((float)m_nWidthStep * 0.5f, (float)m_nHeightStep * 0.5f, 0.0f);
			}

			Vector3 vCenterToCorner = vCorner - vCenter;
			float fRadius = vCenterToCorner.Mag();

			m_GridCells[i][j].m_BoundSphere.Set(vCenter.x, vCenter.y, vCenter.z, fRadius);
		}
	}
}

IMPLEMENT_PLACE(TreeForestGrid);
TreeForestGrid::TreeForestGrid(datResource& rsc) : TreeForest(rsc), m_GridCells(rsc, 1)
{
	// We need to fix up the inner array of the grid cells.
	for (int x=0; x<m_GridCells.GetCount(); x++)
	{
		int count = m_GridCells[x].GetCount();

		for (int y=0; y<count; y++)
		{
            m_GridCells[x][y].Place(&m_GridCells[x][y],rsc);			
		}
	}
}

#if __DECLARESTRUCT
void TreeForestGrid::DeclareStruct(datTypeStruct& s)
{
	TreeForest::DeclareStruct(s);
	STRUCT_BEGIN(TreeForestGrid);
	STRUCT_FIELD(m_nLeft);
	STRUCT_FIELD(m_nRight);
	STRUCT_FIELD(m_nTop);
	STRUCT_FIELD(m_nBottom);
	STRUCT_FIELD(m_nWidthStep);
	STRUCT_FIELD(m_nHeightStep);
	STRUCT_FIELD(m_nWidth);
	STRUCT_FIELD(m_nHeight);
	STRUCT_FIELD(m_nCellsW);
	STRUCT_FIELD(m_nCellsH);
	STRUCT_FIELD(m_bYUp);
	STRUCT_FIELD(m_bEnableGrid);
	STRUCT_FIELD(m_bEntireCellCull);
	STRUCT_FIELD(m_Pad);
	STRUCT_FIELD(m_GridCells);
	STRUCT_FIELD(m_Pad1);
	STRUCT_FIELD(m_Pad2);
	STRUCT_FIELD(m_Pad3);
	STRUCT_END();
}
#endif

TreeForestGridCell::TreeForestGridCell()
	: m_Finalized(false)
{
}



TreeForestGridCell::TreeForestGridCell(datResource& rsc) : m_InstanceList(rsc, 1)
{
	// We need to fix up the inner array of the grid cells.
	for (int x=0; x<m_InstanceList.GetCount(); x++)
	{
		int count = m_InstanceList[x].GetCount();

		for (int y=0; y<count; y++)
		{
            m_InstanceList[x][y].Place(&m_InstanceList[x][y],rsc);			
		}
	}
}

#if __DECLARESTRUCT
void TreeForestGridCell::DeclareStruct(datTypeStruct& s)
{
	AssertMsg(m_Finalized, "RemoveUnusedTreeHashes() must be called before a cell can be resourced");

	STRUCT_BEGIN(TreeForestGridCell);
	STRUCT_FIELD(m_BoundSphere);
	STRUCT_FIELD(m_InstanceList);
	STRUCT_FIELD(m_Empty);
	STRUCT_FIELD(m_Finalized);
	STRUCT_CONTAINED_ARRAY(m_Pad);
	STRUCT_END();
}
#endif

TreeForestGrid::~TreeForestGrid()
{
}

void TreeForestGrid::GetGridCell(const Vector3& vPosition, int& nCellW, int& nCellH)
{
	if( m_bYUp )
	{
		nCellW = (int)((vPosition.x - (float)m_nLeft) / (float)m_nWidthStep);
		nCellH = (int)((vPosition.z - (float)m_nTop) / (float)m_nHeightStep);
	}
	else
	{
		nCellW = ((int)vPosition.x - m_nLeft) / m_nWidthStep;
		nCellH = ((int)vPosition.y - m_nTop) / m_nHeightStep;
	}
}

int TreeForestGrid::LoadTree(const char* szTreeFile, int nSeed, float fSize, float fSizeVariance, bool bCreateBounds, bool bOfflineCreation)
{
	// Let the base class load the tree
	int nTreeIndex = TreeForest::LoadTree(szTreeFile, nSeed, fSize, fSizeVariance, bCreateBounds, bOfflineCreation);

	// Allocate space in the grid for the new tree
	if( nTreeIndex > -1 )
	{
		for( int gridW = 0; gridW < m_GridCells.GetCount(); gridW++ )
		{
			for( int gridH = 0; gridH < m_GridCells[gridW].GetCount(); gridH++ )
			{
				m_GridCells[gridW][gridH].m_InstanceList.Grow();
			}
		}
	}

	return nTreeIndex;
}

TreeInstance& TreeForestGrid::AddTreeInstance(int nTreeIndex, const Vector3& vPosition, float fRotX, float fRotY, float fRotZ)
{
	// Add the instance to the global list
	sysMemStartTemp();
	TreeInstance& inst = TreeForest::AddTreeInstance(nTreeIndex, vPosition, fRotX, fRotY, fRotZ);
	sysMemEndTemp();

	return inst;
}

void TreeForestGrid::RemoveLastTreeInstance(int nTreeIndex)
{
	// Remove the instance from the global list
	TreeInstance& inst = m_TreeInstances[nTreeIndex].Pop();

	// Find the grid cell
	int nCellW;
	int nCellH;
	GetGridCell(inst.GetPosition(), nCellW, nCellH);
	TreeForestGridCell& gridCell = m_GridCells[nCellW][nCellH];

	// Find and remove the instance in the grid cell
	for( int i = 0; i < gridCell.m_InstanceList[nTreeIndex].GetCount(); i++ )
	{
		if( &gridCell.m_InstanceList[nTreeIndex][i] == &inst )
		{
			gridCell.m_InstanceList[nTreeIndex].Delete(i);
			break;
		}
	}
}

// PURPOSE: Remove all tree hashes of trees that have zero instances.
void TreeForestGrid::RemoveUnusedTreeHashes()
{
	// See if there are unused hashes.
	for (int x=0; x<m_TreeSourceHashes.GetCount(); x++)
	{
		bool treeNotused = true;

		for( int gridW = 0; gridW < m_GridCells.GetCount(); gridW++ )
		{
			for( int gridH = 0; gridH < m_GridCells[gridW].GetCount(); gridH++ )
			{
				if (m_GridCells[gridW][gridH].m_InstanceList[x].GetCount())
				{
					// At least one visible element - break the loop.
					treeNotused = false;
					gridW = m_GridCells.GetCount();
					break;
				}
			}
		}

		if (treeNotused)
		{
			m_TreeSourceHashes[x] = 0;
		}
	}

	// Now go the other way around - go through each cell and see if it
	// is completely empty.
	for( int gridW = 0; gridW < m_GridCells.GetCount(); gridW++ )
	{
		for( int gridH = 0; gridH < m_GridCells[gridW].GetCount(); gridH++ )
		{
			TreeForestGridCell &cell = m_GridCells[gridW][gridH];
			cell.m_Empty = true;

			for (int x=0; x<m_TreeSourceHashes.GetCount(); x++)
			{
				if (cell.m_InstanceList[x].GetCount())
				{
					// At least one visible element - break the loop.
					cell.m_Empty = false;
					break;
				}
			}

			cell.m_Finalized = true;
		}
	}
}

void TreeForestGrid::RemapGridInstances()
{
	RAGE_TRACK(SpeedTreeGrid);

	// Clear the grid cells
	int i;
	for( i = 0; i < m_GridCells.GetCount(); i++ )
	{
		for( int j = 0; j < m_GridCells[i].GetCount(); j++ )
		{
			for( int k = 0; k < m_TreeInstances.GetCount(); k++ )
			{
				m_GridCells[i][j].m_InstanceList[k].Resize(0);
			}
		}
	}

	// Add the instances to the appropriate cells
	int *instCount;

	int treeTypes = m_TreeInstances.GetCount();
	int width = m_nCellsW;
	int height = m_nCellsH;
	int totalCount = treeTypes * width * height;

	sysMemStartTemp();
	instCount = rage_new int[totalCount];
	sysMemSet(instCount, 0, sizeof(int) * totalCount);
	sysMemEndTemp();

	for (int pass=0; pass<2; pass++)
	{
		for( i = 0; i < m_TreeInstances.GetCount(); i++ )
		{
			// Second pass: Reserve the proper amounts.
			if (pass == 1)
			{
				for (int xPos = 0; xPos < width; xPos++)
				{
					for (int yPos = 0; yPos < height; yPos++)
					{
						m_GridCells[xPos][yPos].m_InstanceList[i].Reserve(instCount[i + (((yPos * width) + xPos) * treeTypes)]);
					}
				}
			}

			for( int j = 0; j < m_TreeInstances[i].GetCount(); j++ )
			{
				TreeInstance& inst = m_TreeInstances[i][j];

				// Find the grid cell this instance belongs in
				int nCellW;
				int nCellH;
				GetGridCell(inst.GetPosition(), nCellW, nCellH);

				if (nCellW < 0 || nCellH < 0 || nCellW >= m_nCellsW || nCellH >= m_nCellsH)
				{
					Warningf("Tree is outside of the boundaries. Position is at %.2f %.2f %.2f", inst.GetPosition().x, inst.GetPosition().y, inst.GetPosition().z);
				}
				else
				{
					if (pass == 0)
					{
						instCount[i + (((nCellH * width) + nCellW) * treeTypes)]++;
					}
					else
					{
						// Put the instance in the grid
						m_GridCells[nCellW][nCellH].m_InstanceList[i].Append() = inst;
					}
				}

				// TODO: Possibly grow the radius of this grid cell if the instance will be partially outside the bounding sphere
				// - Update: These days, ComputeBoundingSpheres() takes care of that. It's done after all
				//   instances have been added, rather than on the fly, so that it can pick a suitable
				//   center position. /FF
			}
		}
	}

	sysMemStartTemp();
	delete[] instCount;

	// Empty out the global instances
	for (int x=0; x<m_TreeInstances.GetCount(); x++)
	{
		m_TreeInstances[x].Resize(0);
	}

	sysMemEndTemp();

	ComputeBoundingSpheres();
}


void TreeForestGrid::Update(float fTime, const grcViewport& pViewport, bool Shadows)
{
	Update(fTime, pViewport, 0, m_nCellsW - 1, 0, m_nCellsH - 1, Shadows);
}


void TreeForestGrid::Update(float fTime, const grcViewport& pViewport,
		int nW1, int nW2, int nH1, int nH2, bool Shadows)
{
	CalculateImposterSizeBasedFade vis(m_billboardBlendRange);

	if (Shadows)
	{
		UpdateInternal<true>(fTime, pViewport, nW1, nW2, nH1, nH2, vis);
	}
	else
	{
		UpdateInternal<false>(fTime, pViewport, nW1, nW2, nH1, nH2, vis);
	}
}

template<bool ShadowsEnabled, class VisibilitySelector>
void TreeForestGrid::UpdateInternal(float fTime, const grcViewport& pViewport,
								 int nW1, int nW2, int nH1, int nH2, VisibilitySelector vis)
{

	RAGE_TRACK(SpeedTreeGrid);

	// Note: I added the coordinate ranges here, to allow for optimizations at
	// a higher level. Currently these involve computing the range based on the
	// grcViewport. It might make sense to move that code down to this level,
	// but for now I left it out to keep the changes here as small as possible. /FF

	Assert(nW1 >= 0);
	Assert(nW2 >= nW1);
	Assert(nW2 < m_nCellsW);
	Assert(nH1 >= 0);
	Assert(nH2 >= nH1);
	Assert(nH2 < m_nCellsH);

	if( m_bEnableGrid )
	{
		int treeTypes = m_Trees.GetCount();

		PF_START(UpdateVisibility);

		// Set the visible list to be allocated to the max
		int* pVisibleCounts = Alloca(int, m_VisibleTreeInstances.GetCount());
		for( int i = 0; i < m_VisibleTreeInstances.GetCount(); i++ )
		{
			m_VisibleTreeInstances[i].Resize(m_VisibleTreeInstances[i].GetCapacity());
			pVisibleCounts[i] = 0;
		}		

		// Update the grid
		for( int gridW = nW1; gridW <= nW2; gridW++ )
		{
			for( int gridH = nH1; gridH <= nH2; gridH++ )
			{
				// Test this grid cell for visibility
				TreeForestGridCell& gridCell = m_GridCells[gridW][gridH];

				// Ignore completely empty cells.
				if (!gridCell.m_Empty)
				{
					Vector4 vCullSphere = gridCell.m_BoundSphere;
					float fZDist;
					grcCullStatus cullStatus = pViewport.IsSphereVisible(vCullSphere.x, vCullSphere.y, vCullSphere.z, vCullSphere.w, &fZDist);
					if( cullStatus != cullOutside )
					{
						// This grid cell is visible
						if(ShadowsEnabled || (m_bEntireCellCull && cullStatus == cullInside && fZDist - (2.0f * vCullSphere.w) >= m_fBillboardFarDistance.x ))
						{
							// Add all the instnaces in this cell to the visible list
							for( int i = 0; i < treeTypes; i++ )
							{
								if (m_Trees[i])
								{
									int instances = gridCell.m_InstanceList[i].GetCount();

									if (instances)
									{
										int visibleCount = pVisibleCounts[i];
										datRef<TreeInstance> *visibleTreeInstances = &m_VisibleTreeInstances[i][visibleCount];

										TreeInstance *instanceArray = &gridCell.m_InstanceList[i][0];
										for( int j = 0; j < instances; j++ )
										{
											TreeInstance* inst = &instanceArray[j];

											inst->SetVisible(true);
											inst->SetBillboardOnly(true);
											inst->SetBillboardActive(true);
											inst->SetFadeOut(1.0f);

											*(visibleTreeInstances++) = inst;
											visibleCount++;
										}

										pVisibleCounts[i] = visibleCount;
									}
								}
							}
						}
						else
						{
							// test each instance inside for visibility
							for( int i = 0; i < treeTypes; i++ )
							{
								instanceTreeData *treeData = m_Trees[i];

								if (treeData)
								{
									int instances = gridCell.m_InstanceList[i].GetCount();

									if (instances)
									{
										Vector4 vTreeCullSphere = treeData->GetCullSphere();
										vis.SetTexSize( treeData->GetBillboardTexture()->GetWidth() );

										int visibleCount = pVisibleCounts[i];

										TreeInstance *instanceList =&gridCell.m_InstanceList[i][0];
										TreeInstance **visibleList = (TreeInstance **) &m_VisibleTreeInstances[i][visibleCount];


										for( int j = 0; j < instances; j++ )
										{
											TestAndAddInstance(visibleCount, *(instanceList++), vis, vTreeCullSphere, pViewport, visibleList, treeData);
										}

										pVisibleCounts[i] = visibleCount;
									}
								}
							}
						}
					}
				}
			}
		}

		// Resize the visible list to contain the number visisble
		for( int i = 0; i < treeTypes; i++ )
		{
			m_VisibleTreeInstances[i].Resize(pVisibleCounts[i]);
		}

		PF_STOP(UpdateVisibility);
	}
	else
	{
		TreeForest::Update(fTime, pViewport);
	}
}

#if __BANK
void TreeForestGrid::AddWidgets(bkBank& bank)
{
	TreeForest::AddWidgets(bank);
	bank.AddToggle("Enable Grid", &m_bEnableGrid);
	bank.AddToggle("Cull Full Cell", &m_bEntireCellCull);
}
#endif

#if __DEV

void TreeForestGrid::DebugDraw(bool drawGrid, bool drawCellBoundSpheres) const
{
	if(!drawGrid && !drawCellBoundSpheres)
		return;

	grcSetRenderState(grcsCullMode,grccmNone);
	grcSetRenderState(grcsLighting,grclmNone);
	grcBindTexture(NULL);
	grcSetRenderState(grcsDepthTest,true);
	grcSetRenderState(grcsDepthFunc,grccfLessEqual);
	grcSetRenderState(grcsDepthWrite,false);
	grcSetRenderState(grcsAlphaBlend,true);
	grcSetRenderState(grcsBlendSet,grcbsNormal);

	grcWorldIdentity();

	if(drawGrid)
	{
		grcColor(Color32(64, 192, 64, 96));

		// MAGIC! Quite arbitrary limits in the Y (or Z) direction.
		const float minElev = 0.0f;
		const float maxElev = 100.0f;

		int gridW;

		float minX = (float)m_nLeft;
		float maxX = (float)(m_nCellsW*m_nWidthStep + m_nLeft);
		float minY = (float)m_nTop;
		float maxY = (float)(m_nCellsH*m_nHeight + m_nTop);

		for(gridW = 0; gridW <= m_nCellsW; gridW++)
		{
			float x = (float)(gridW*m_nWidthStep + m_nLeft);

			grcBegin(drawTriStrip, 4);
			if(m_bYUp)
			{
				grcVertex3f(x, minElev, minY);
				grcVertex3f(x, maxElev, minY);
				grcVertex3f(x, minElev, maxY);
				grcVertex3f(x, maxElev, maxY);
			}
			else
			{
				grcVertex3f(x, minY, minElev);
				grcVertex3f(x, minY, maxElev);
				grcVertex3f(x, maxY, minElev);
				grcVertex3f(x, maxY, maxElev);
			}
			grcEnd();
		}

		int gridH;
		for(gridH = 0; gridH <= m_nCellsH; gridH++)
		{
			float y = (float)(gridH*m_nHeightStep + m_nTop);

			if(m_bYUp)
			{
				grcBegin(drawTriStrip, 4);
				grcVertex3f(minX, minElev, y);
				grcVertex3f(minX, maxElev, y);
				grcVertex3f(maxX, minElev, y);
				grcVertex3f(maxX, maxElev, y);
				grcEnd();
			}
			else
			{
				grcBegin(drawTriStrip, 4);
				grcVertex3f(minX, y, minElev);
				grcVertex3f(minX, y, maxElev);
				grcVertex3f(maxX, y, minElev);
				grcVertex3f(maxX, y, maxElev);
				grcEnd();
			}
		}
	}

	grcViewport *vp = grcViewport::GetCurrent();
	if(drawCellBoundSpheres && vp)
	{
		grcColor(Color32(192, 192, 192, 128));

		const float cullDist = 400.0f;
		Vector3 camPos = VEC3V_TO_VECTOR3(vp->GetCameraPosition());

		int gridW, gridH;
		for(gridW = 0; gridW < m_nCellsW; gridW++)
		{
			for(gridH = 0; gridH < m_nCellsH; gridH++)
			{
				const TreeForestGridCell &gridCell = m_GridCells[gridW][gridH];

				Vector3 pos;
				gridCell.m_BoundSphere.GetVector3(pos);

				const float radius = gridCell.m_BoundSphere.w; 
				if(pos.Dist2(camPos) >= square(radius + cullDist))
					continue;

				grcDrawSphere(radius, pos, 16, true);
			}
		}
	}
}

#endif


void TreeForestGrid::ComputeBoundingSpheres()
{
	int gridW, gridH;
	for(gridW = 0; gridW < m_nCellsW; gridW++)
	{
		for(gridH = 0; gridH < m_nCellsH; gridH++)
		{
			ComputeBoundingSphereForCell(gridW, gridH);
		}
	}
}


void TreeForestGrid::ComputeBoundingSphereForCell(int nGridW, int nGridH)
{
	int treeType;
	int i;

	TreeForestGridCell &gridCell = m_GridCells[nGridW][nGridH];

	// First, find the minimum and maximum elevation of trees in the cell.
	float maxElevation = -FLT_MAX;
	float minElevation = FLT_MAX;
	bool found = false;
	for(treeType = 0; treeType < gridCell.m_InstanceList.GetCount(); treeType++)
	{
		Vector4 treeCullSphere;

		if(!m_Trees[treeType])
		{
		// TODO: This is a hack. We should get the proper cull sphere through the physics info.
			treeCullSphere = Vector4(0.0f, 0.0f, 0.0f, 5.0f);
		}
		else
		{
			treeCullSphere = m_Trees[treeType]->GetCullSphere();
		}

		for(i = 0; i < gridCell.m_InstanceList[treeType].GetCount(); i++)
		{
			const TreeInstance *inst = &gridCell.m_InstanceList[treeType][i];
			if(!inst)
				continue;	// ?
			Vector4 localCullSphere = treeCullSphere;
			localCullSphere.AddVector3(inst->GetPosition());

			// Careful of AddVector3()... At first I didn't expect it
			// to kill my w component, but it does. /FF
			localCullSphere.w = treeCullSphere.w;

			float minPt = m_bYUp ? localCullSphere.y : localCullSphere.z;
			float maxPt = minPt;
			minPt -= localCullSphere.w;
			maxPt += localCullSphere.w;
			if(minPt < minElevation)
				minElevation = minPt;
			if(maxPt > maxElevation)
				maxElevation = maxPt;
			found = true;
		}
	}

	// Compute the elevation at which to center the sphere.
	// At this time, that's simply the average of the min and max elevation
	// of instances in the cell.
	float elev = found ? 0.5f*(maxElevation + minElevation) : 0.0f;

	// The center of the sphere is at the center of the square for now.
	Vector3 vCorner;
	Vector3 vCenter;
	if( m_bYUp )
	{
		vCorner.Set((float)(m_nLeft + (nGridW * m_nWidthStep)), 0.0f, (float)(m_nTop + (nGridH * m_nHeightStep)));
		vCenter = vCorner + Vector3((float)m_nWidthStep * 0.5f, elev, (float)m_nHeightStep * 0.5f);
	}
	else
	{
		vCorner.Set((float)(m_nLeft + (nGridW * m_nWidthStep)), (float)(m_nTop + (nGridH * m_nHeightStep)), 0.0f);
		vCenter = vCorner + Vector3((float)m_nWidthStep * 0.5f, (float)m_nHeightStep * 0.5f, elev);
	}

	// Loop over the instances in the cell again, and compute how large
	// the sphere has to be.
	float maxDist = 0.0f;
	for(treeType = 0; treeType < gridCell.m_InstanceList.GetCount(); treeType++)
	{
		Vector4 treeCullSphere;

		if(!m_Trees[treeType])
		{
			treeCullSphere = Vector4(0.0f, 0.0f, 0.0f, 5.0f);
		}
		else
		{
			treeCullSphere = m_Trees[treeType]->GetCullSphere();
		}

		for(i = 0; i < gridCell.m_InstanceList[treeType].GetCount(); i++)
		{
			const TreeInstance *inst = &gridCell.m_InstanceList[treeType][i];
			if(!inst)
				continue;	// ?
			Vector3 localCullSphere;
			treeCullSphere.GetVector3(localCullSphere);
			localCullSphere.Add(inst->GetPosition());
			float dist = localCullSphere.Dist(vCenter) + treeCullSphere.w;	// Note: could save square roots here if we .
			if(dist > maxDist)
				maxDist = dist;
		}
	}

	// Note: if no trees were found in this cell, the bound sphere will be centered
	// at the center of the grid cell, at Y (or Z) = 0, and with a radius of 0.
	gridCell.m_BoundSphere.SetVector3(vCenter);
	gridCell.m_BoundSphere.w = maxDist;

	// TODO: Consider computing an optimal (or at least, "more optimal") sphere.
	// It may be possible to use geomSpheres::ComputeBoundSphere(), but that one
	// currently works for a set of points, not a set of spheres. Finding a sphere
	// large enough to cover the center points, and then growing it to include
	// the full spheres may be a good way. /FF
}

// RETURNS: The boundaries of this grid.
void TreeForestGrid::GetBoundaries(int &nLeft, int &nRight, int &nTop, int &nBottom) const
{
	nLeft = m_nLeft;
	nRight = m_nRight;
	nTop = m_nTop;
	nBottom = m_nBottom;
}

void TreeForestGrid::AttachToManager(instanceTreeDataMgr &manager, atArray<u32> *treeTypeHashes)
{
	TreeForest::AttachToManager(manager, treeTypeHashes);

	//int nTreeTypes = manager.GetCount();
	int nTreeTypes = treeTypeHashes->GetCount();

	for( int gridW = 0; gridW < m_GridCells.GetCount(); gridW++ )
	{
		for( int gridH = 0; gridH < m_GridCells[gridW].GetCount(); gridH++ )
		{
			m_GridCells[gridW][gridH].m_InstanceList.Resize(nTreeTypes);
		}
	}
}

// PURPOSE: Remove all trees within a certain radius.
void TreeForestGrid::ClearAreaOfTrees(const spdSphere &sphere)
{
	for( int gridW = 0; gridW < m_GridCells.GetCount(); gridW++ )
	{
		for( int gridH = 0; gridH < m_GridCells[gridW].GetCount(); gridH++ )
		{
			TreeForestGridCell &cell = m_GridCells[gridW][gridH];

			int trees = cell.m_InstanceList.GetCount();

			for (int x=0; x<trees; x++)
			{
				int instances = cell.m_InstanceList[x].GetCount();
				for (int y=0; y<instances; y++)
				{
					TreeInstance *instance = &cell.m_InstanceList[x][y];
					if (instance && sphere.Contains(instance->GetPosition()))
					{
						instance->SetPosition(Vector3(9999999.0f, 999999.0f, 999999.0f));
					}
				}
			}
		}
	}
}


} // namespace rage
