// 
// snu/snugamerdata.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNU_SNUGAMERDATA_H
#define SNU_SNUGAMERDATA_H

#include "snubitbuffer.h"
#include "snuconfig.h"

#include "atl/pool.h"
#include "atl/array.h"
#include "rline/rlgamerid.h"
//#include "rline/rlfriendsreader.h"
//#include "rline/rlpresence.h"
//#include "rline/rlfriendsreader.h"
//#include "rline/rlachievement.h"
//#include "snet/session.h"

class snuFriendInvites;
class snuGamer;
struct snuGamerJoinWish;
class snuSessionData;

namespace rage 
{ 
    class rlStats; 
    class rlLeaderboard;
    class rlLeaderboardInfo;
}

struct snuGamerDataDef
{
    const char*	m_szName;	
    int			m_BitSize;
    snuSyncSign	m_Sign;
    snuSyncType	m_SyncType;
};

class snuGamerDataSyncMsg
{
public:
    NET_MESSAGE_DECL(snuGamerDataSyncMsg);

    NET_MESSAGE_SER(bb, msg)
    {
        return msg.m_SyncBuffer.Ser(bb)							
               && bb.SerBytes(&msg.m_GamerId, sizeof(msg.m_GamerId))	
               && bb.SerBytes(&msg.m_IsFieldPresent, sizeof(msg.m_IsFieldPresent));
    }

    const rage::rlGamerId& GetGamerID()	const { return m_GamerId; }
    
    void WriteGamerData(snuGamer& gamer) const;
    void ReadDirtyGamerData(const snuGamer& gamer);
    void ReadSyncableGamerData(const snuGamer& gamer);

protected:
    rage::rlGamerId		m_GamerId;			//TODO: make this smaller
    snuBitBuffer		m_SyncBuffer;
    unsigned int		m_IsFieldPresent;
};

class snuGamerData
{
    friend class snuGamer;
    friend class snuGamerLocal;
    friend class snuGamerDataSyncMsg;

public:
    enum { MAX_GAMER_VARS = 32}; //TODO: think about lowering to 16

    static bool 		gInitialise(const snuGamerDataDef[], int dataCount);
    static int			gGetDataIndexByName(const char *name);
    static int			gGetBitSize(const int i);
    static int			gGetBitSize() { return g_aBitOffset[gDataCount]; }
    static int			gGetDataCount() { return gDataCount; }
    static const char*	gGetDataName(const int i) { return g_aDataDef[i].m_szName; }
    static bool			gIsSigned(const int i);
    static bool			gIsSyncable(const int i) { return g_aDataDef[i].m_SyncType != syncTypeNone; }

    snuGamerData();

    snuBitBuffer* GetDataBuffer(){return &m_Data;}
    
    const rage::datBitBuffer* GetDataBuffer() const	{return &m_Data;}

    int GetData(const int i, void *d) const;
    int	GetData(const int i, const void *v);
    int	GetDataInt(const int i)	const;
    unsigned GetDataUns(const int i) const;

    int SetData(const int idx, const void *source);
    int	SetDataInt(const int i, const int v);
    int	SetDataUns(const int i, const unsigned v);

    void Clear();

    void DisplayData(int minDebugLevelDirty, int minDebugLevelClean) const;

    void SetFieldDirty(const int idx) { m_IsFieldDirty |= 1<<idx; }
    bool IsFieldDirty(const int idx) const { return (m_IsFieldDirty & 1<<idx)> 0; }
    bool IsDirty() const { return m_IsFieldDirty!=0; }
    void CleanDirtyFlags() { m_IsFieldDirty=0; }

protected:
    snuBitBuffer					m_Data;
    unsigned int					m_IsFieldDirty : MAX_GAMER_VARS;  // dirty = data not yet synced with host

    static int						g_aBitOffset[MAX_GAMER_VARS+1];
    static const snuGamerDataDef* 	g_aDataDef;
    static int						gDataCount;
};

#endif //SNU_SNUGAMERDATA_H

