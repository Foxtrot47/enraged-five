// 
// snu/snuconfig.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNU_SNUCONFIG_H
#define SNU_SNUCONFIG_H

#include "net/net.h"
#include "rline/rlsession.h"

namespace rage
{
	class rlSessionSearchResult;
};

class snuGamerLocal;
class snuGamer;

enum
{
    SNU_INVALID_CONNECTION_ID       = -1,
    SNU_DEFAULT_CPU_AFFINITY        = 1,

    //Maximum number of connections managed by the connection manager.
    MAX_CXNS					    = 16,

    SNU_MAX_USER_OPTIONS	        = 32,
    SNU_MAX_CHOICES_PER_USER_OPTION = 16,
	SNU_MAX_SESSIONS				= 4,
};

// snu channel ids
enum
{
	//A channel for every session managed by SNU
	SNU_FIRST_SESSION_CHANNEL,
	SNU_LAST_SESSION_CHANNEL=SNU_MAX_SESSIONS-1,
	COMPLAINT_CHANNEL_ID,
	SNET_CHANNEL_ID,
	SEARCH_CHANNEL_ID,
	VOICE_CHANNEL_ID,

	//channels between min and max (inclusive) are reserved for the app
	SNU_APP_CHANNEL_ID_MIN,
	SNU_APP_CHANNEL_ID_MAX=rage::NET_MAX_CHANNEL_ID,

	SNU_MAX_CHANNELS
};

enum snuSyncType
{
    syncTypeNone,
    syncTypeNormal,
};

enum snuSyncSign
{
    syncSigned,
    syncUnsigned
};

enum snuSocketTypeID
{
    snuSocketTypeVoid = -1,

    snuSocketLocal,
    snuSocketLan,
    snuSocketOnline,

    snuSocketTypesCount
};

enum snuSocketStatusID
{
    snuSocketVoid = -1,

    snuSocketUnInitialised = 0,
    snuSocketInitialised,
    snuSocketConnected,

    snuNumSocketStates
};

typedef rage::s8 snuSessionId;
enum 
{
    snuSessionId_Invalid = -1
};

class snuNotifyBase
{
public:

    virtual ~snuNotifyBase() {}
};

struct snuGamerJoinWish
{
	rage::rlSessionInfo m_SessionInfo;
	snuGamer* m_pGamer;
	bool m_FromInvite;
	rage::rlNetworkMode m_NetMode;
	rage::rlSession::ConfigParams m_Config;
	rage::netStatus m_Status;                

	void Reset()
	{ 
		m_pGamer = 0; 
		m_FromInvite= false; 
		m_NetMode = rage::RL_NETMODE_INVALID;
		m_Config.Clear();
		m_Status.Reset(); 
	}

	void Reset(snuGamerLocal* sgl, const class rage::rlSessionSearchResult* sr,  bool bFromInvite=false);
	void Reset(snuGamerLocal*, const rage::rlSession::ConfigParams&, const rage::rlSessionInfo&, bool bFromInvite=false);

	bool IsValid() const { return m_pGamer != 0; }
	bool IsReady() const { return IsValid() && !m_Status.Pending(); }
};

class snuMessage
{
public:
	virtual ~snuMessage() {}
	virtual bool Export(void* buf, const unsigned sizeofBuf, unsigned* size = 0) const = 0;
	virtual bool Import(const void* buf, const unsigned sizeofBuf, unsigned* size=0) = 0;
#if !__FINAL
	virtual const char* GetMsgName() const = 0;
#endif
};



#endif //SNU_SNUCONFIG_H

