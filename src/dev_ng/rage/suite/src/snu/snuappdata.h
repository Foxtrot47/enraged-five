// 
// snu/snuappdata.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNUAPPDATA_H
#define SNUAPPDATA_H

#include "net/netmessage.h"

namespace rage
{

class snuAppData : public datBitBuffer
{
public:
    enum
    {
        MAX_SIZEOF_DATA     = 64,
        MAX_BITSIZEOF_DATA  = MAX_SIZEOF_DATA << 3,
    };

    snuAppData()
    {
        this->Reset();
    }

    snuAppData(const snuAppData& that)
    {
        *this = that;
    }

    snuAppData& operator=(const snuAppData& that)
    {
        if(this != &that)
        {
            this->Reset(&that);
        }

        return *this;
    }

    void Reset()
    {
        this->SetReadWriteBytes(m_Data, sizeof(m_Data));
    }

    void Reset(const snuAppData* appData)
    {
        this->Reset();

        if(appData
           && appData->GetBitLength()
           && AssertVerify(this->WriteBits(appData->GetReadOnlyBits(),
                                           appData->GetBitLength(),
                                           appData->GetBaseBitOffset())))
        {
            this->SetCursorPos(0);
        }
    }

    //Import
    bool Ser(const netb::netImportBuffer& bb)
    {
        this->Reset();

        unsigned myBitSize = 0;

        const bool success =
            AssertVerify(bb.ReadUns(myBitSize, datBitsNeeded< MAX_BITSIZEOF_DATA >::COUNT))
            && AssertVerify(myBitSize <= MAX_BITSIZEOF_DATA)
            && AssertVerify(this->CanWriteBits(myBitSize))
            && myBitSize ? AssertVerify(bb.ReadBits(this->GetReadWriteBits(),
                                                    myBitSize,
                                                    this->GetBaseBitOffset())) : true;

        if(success)
        {
            this->SetNumBitsWritten(myBitSize);
            this->SetCursorPos(0);
        }

        return success;
    }

    //Export
    bool Ser(netb::netExportBuffer& bb) const
    {
        const unsigned myBitSize = this->GetBitLength();

        Assert(myBitSize <= MAX_BITSIZEOF_DATA);

        return
            AssertVerify(bb.WriteUns(myBitSize, datBitsNeeded< MAX_BITSIZEOF_DATA >::COUNT))
            && myBitSize ? AssertVerify(bb.WriteBits(this->GetReadOnlyBits(),
                                                    myBitSize,
                                                    this->GetBaseBitOffset())) : true;
    }

private:
    u8 m_Data[MAX_SIZEOF_DATA];
};

}   //namespace rage

#endif  //SNET_APPDATA_H
