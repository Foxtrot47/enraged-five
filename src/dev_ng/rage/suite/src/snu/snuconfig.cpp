// 
// snu/snuconfig.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "snuconfig.h"
#include "snugamer.h"
namespace rage {

} // namespace rage

using namespace rage;

void snuGamerJoinWish::Reset(snuGamerLocal* pGamer, const rlSession::ConfigParams& rConfig, const rlSessionInfo& rInfo, bool bFromInvite)
{
	Reset();

	//TODO (Ali) - Implement LAN mode
	if (rConfig.m_NetworkMode==RL_NETMODE_LAN)
	{
		AssertMsg(0,"LAN mode not implemented yet for this feature");
		return;
	}

	m_pGamer		= pGamer;
	m_FromInvite	= bFromInvite;
	m_SessionInfo	= rInfo;

	m_Config.Reset
		(
		rConfig.m_NetworkMode,
		rConfig.m_MaxPublicSlots,
		rConfig.m_MaxPrivateSlots,
		rConfig.m_Attrs,
		rConfig.m_CreateFlags,
		rConfig.m_PresenceFlags,
		0,0	//sessionId and arbCookie
		);

	//TODO (Ali) - Implement LAN mode
	/*
	if (rConfig.m_NetworkMode==RL_NETMODE_LAN)
	m_HostAddr.Init(rInfo.GetHostPeerAddress(), SNUSOCKET.GetSocket()->GetPort());
	*/
}



void snuGamerJoinWish::Reset(snuGamerLocal* pGamer, const rage::rlSessionSearchResult* pTarget, bool bFromInvite)
{
	Reset();
	Assert(pTarget);

	bool bRanked = pTarget->m_Attrs.GetGameType()==RL_GAMETYPE_RANKED;
	bool bOnline = pTarget->m_NetMode==RL_NETMODE_ONLINE;

	m_pGamer		= pGamer;
	m_FromInvite	= bFromInvite;
	m_SessionInfo	= pTarget->m_SessionInfo;
	m_Config.Reset
		(
		pTarget->m_NetMode, 
		pTarget->m_MaxPublicSlots, 
		pTarget->m_MaxPrivateSlots, 
		pTarget->m_Attrs,
		bRanked				? rlSession::CREATE_ARBITRATED	: 0, //createFlags,
		bOnline&&!bRanked	? rlSession::PRESENCE_INVITABLE	: 0, //presenceFlags
		0,0	//sessionId and arbCookie
		);

	m_NetMode = pTarget->m_NetMode;
}

