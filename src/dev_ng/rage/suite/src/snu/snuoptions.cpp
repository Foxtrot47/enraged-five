// 
// snu\snuoptions.h 
// 
// Copyright(C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "snuoptions.h"
#include "snudiag.h"

#include "rline/rlmatchingattributes.h"
#include "rline/rlsessionfinder.h"
#include "string/stringhash.h"

#include <string.h>

using namespace rage;

int    
snuUserOption::GetByName(const char *szName) const
{
    for(int i=0; m_aValueName[i] && (i!=SNU_MAX_CHOICES_PER_USER_OPTION); i++)
    {
        if(0==strcmp(szName,m_aValueName[i]))
        {
            return i;
        }
    }

    AssertMsg(0,"bad option name");
    
    return -1;
}

bool 
snuUserOptions::Init(snuUserOption aUserOption[])
{
    m_aOption = aUserOption;
    return true;
}

int 
snuUserOptions::GetOptionIndex(const char* option) const
{
    //find the field
    for(int i=0; i!=SNU_MAX_USER_OPTIONS && m_aOption[i].m_Name!=NULL; i++)
    {
        if(strcmp(option, m_aOption[i].m_Name)==0)
            return i;
    }

    AssertMsg(0,"option name not found");
    return -1;
}

bool 
snuUserOptions::SetOptionByIndex(const int optionIdx, const int value)
{
    Assert(optionIdx>=0 && optionIdx<SNU_MAX_USER_OPTIONS);

    m_aOption[optionIdx].m_Value=value;
    snuDebug2( "%s set to %d", m_aOption[optionIdx].m_Name, value );

    return true;
}

bool 
snuUserOptions::SetOption(const char* szOption, const char* szValue)
{
    int optionIdx    = GetOptionIndex(szOption);

    if(!AssertVerify(optionIdx>=0))
    {
        return false;
    }
    
    SetOptionByIndex(optionIdx, GetOption(optionIdx).GetByName(szValue));
    return true;
}

void 
snuUserOptions::CopyToAttrib(rlMatchingAttributes &attrs) const
{
    int i;
	attrs.ClearAllValues();

	for(i=0; m_aOption[i].m_Name; i++)
	{
		if ( AssertVerify(m_aOption[i].m_Name) && m_aOption[i].IsSet() )
		{
			if (0==strcmp(m_aOption[i].m_Name,"GameMode"))
			{
				attrs.SetGameMode(m_aOption[i].m_Value);
			}
			else if (0==strcmp(m_aOption[i].m_Name,"GameType"))
			{
				attrs.SetGameType((rlGameType)m_aOption[i].m_Value);
			}
			else if (m_aOption[i].m_ID!=-1)
			{
				attrs.SetValueById(m_aOption[i].m_ID, m_aOption[i].m_Value);
			}
		}
	}
}

void 
snuUserOptions::CopyToFilter(rlMatchingFilter &filter) const
{
	u32 i;

	//clear values
	for (i=0; i!=filter.GetConditionCount(); i++)
	{
		filter.ClearValue(i);
	}

	for(i=0; m_aOption[i].m_Name; i++)
	{
		if( m_aOption[i].IsSet() )
		{
			if (0==strcmp("GameMode",m_aOption[i].m_Name))
			{
				filter.SetGameMode(m_aOption[i].m_Value);
			}
			else if (0==strcmp("GameType",m_aOption[i].m_Name))
			{
				filter.SetGameType((rlGameType)m_aOption[i].m_Value);
			}
			else if (m_aOption[i].m_ID!=-1)
			{
				//find the condition index
				u32 conditionIdx=0;
				for (conditionIdx=0; conditionIdx<filter.GetConditionCount(); conditionIdx++)
				{
					if (filter.GetSessionAttrFieldIdForCondition(conditionIdx)==(u32)m_aOption[i].m_ID)
						break;
				}

				if (conditionIdx!=filter.GetConditionCount())
					filter.SetValue(conditionIdx,m_aOption[i].m_Value);
			}
		}
	}
}

#if !__NO_OUTPUT
void 
snuUserOptions::DisplayAttributeData(const rlMatchingAttributes& attrs)    const
{
	snuDebug2( "GameMode = %u", attrs.GetGameMode() );
	snuDebug2( "GameType = %s", attrs.GetGameType()==RL_GAMETYPE_STANDARD ? "STANDARD_MATCH" : "RANKED_MATCH" );

    for(unsigned i=0; m_aOption[i].m_ID!=-1; i++)
    {
        const u32* pval = attrs.GetValueById(m_aOption[i].m_ID);
        const u32 val = pval ? *pval : 0;

        snuDebug2( "%s = %d %s", m_aOption[i].m_Name, val, !pval ? "(unset)" : "" );
	}
}

void 
snuUserOptions::DisplayFilterData(const rlMatchingFilter& filter) const
{
	snuDebug2( "GameMode = %u", filter.GetGameMode() );
	snuDebug2( "GameType = %s", filter.GetGameType()==RL_GAMETYPE_STANDARD ? "STANDARD_MATCH" : "RANKED_MATCH" );

	for(unsigned i=0; i < filter.GetConditionCount(); i++)
	{
		const u32 attrIndex = filter.GetSessionAttrIndexForCondition(i);
		const u32* pval = filter.GetValue(i);
		const u32 val = pval ? *pval : 0;

		snuDebug2( "%s = %d %s", m_aOption[attrIndex].m_Name, val, !pval ? "(wild)" : "" );
    }
}

#else
void 
snuUserOptions::DisplayAttributeData(const rlMatchingAttributes&)    const
{
}
void 
snuUserOptions::DisplayFilterData(const rlMatchingFilter&) const
{
}
#endif  //__NO_OUTPUT

void 
snuUserOptions::ClearValues()
{
    for(unsigned i=0; m_aOption[i].m_ID!=-1; i++)
    {
        m_aOption[i].m_Value = -1;
    }
}
