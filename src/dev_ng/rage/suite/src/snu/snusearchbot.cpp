#include "snusearchbot.h"
#include "snudiag.h"
#include "snugamer.h"
#include "snuoptions.h"
#include "snupostoffice.h"
#include "snusocket.h"
#include "snuTasks.h"

#include "math/amath.h"
#include "system/nelem.h"
#include "system/param.h"

#if __LIVE
#include "system/xtl.h"
#endif

using namespace rage;

enum
{
	GROUPJOIN_REQUEST_TIMEOUT = 3000,
};

PARAM(nosearchbotqos,"disable QoS probing of session search results, all returned sessions will be considered good");

//==================================================================================================================================
// class snuSearchBot
//==================================================================================================================================

snuSearchBot::snuSearchBot() 
: m_SearchResultArray	(m_aSearchResult,maxQueryResults)
, m_LastMsTime			(0)
, m_NetMode				(rage::RL_NETMODE_INVALID)
, m_bIsQosEnabled		(true)
, m_pGroupJoinTask		(NULL)
{
}

snuSearchBot::~snuSearchBot()
{
	if(m_pGroupJoinTask)
		delete m_pGroupJoinTask;
}
void snuSearchBot::Init(netConnectionManager* pCxn)
{
	m_SessionFinder.Init(pCxn->GetSocket()->GetHardware(), pCxn->GetSocket()->GetPort()+1);

	m_Status = searchBotIdle;
	ClearQueryResults();

	for( int i = 0; i < NELEM(m_SessionQueryQos); i++)
	{
		m_SessionQueryQosNotifyHandler[ i ].Reset< snuSearchBot, &snuSearchBot::SessionFinderQosNotifyHandler >( this );
		m_SessionQueryQos[ i ].AddNotifyHandler( &m_SessionQueryQosNotifyHandler[ i ] );
	}

	if (PARAM_nosearchbotqos.Get())
		EnableQosChecking(false);
}

void snuSearchBot::Shutdown()
{
	m_SessionFinder.Shutdown();

	for( int i = 0; i < NELEM(m_SessionQueryQos); i++)
	{
		m_SessionQueryQos[ i ].RemoveNotifyHandler( &m_SessionQueryQosNotifyHandler[ i ] );
	}
}

void snuSearchBot::Update(const u32 curTime)
{
	u32 deltaMs = Clamp(curTime - m_LastMsTime, 1u, 1000u);

	// pump the rl session finder update
	m_SessionFinder.Update();

	// update the qos object for each result
	for(int i = 0; i < GetQueryResultCount(); i++)
	{
		m_SessionQueryQos[ i ].Update(deltaMs);
	}

	//just finished a search?
	if ( m_Status==searchBotActive && !m_CurrentTask.Pending() && AssertVerify(!m_SessionFinder.Pending()) )
	{
		if (!m_SearchResultArray.m_NumResults)
		{
			Notify(snuNotifyOk, NULL); //this is a fail notification)
		}
		//attempt to start up a parallel qos for each result
		else for (unsigned i=0; i!=m_SearchResultArray.m_NumResults; i++)
		{
			bool bValid = false;

			if (SNUSOCKET.IsLocallyJoined(m_SearchResultArray.m_Results[i].m_SessionInfo.GetToken()))
			{
				snuDebug("Ignoring locally joined game");
			}
			else if (m_bIsQosEnabled)
			{
				if(AssertVerify(m_SessionQueryQos[i].QueryPeer
				(
					m_SearchResultArray.m_Results[i].m_SessionInfo,
					m_SearchResultArray.m_Results[i].m_SessionInfo.GetHostPeerAddress(),
					m_NetMode,SNUSOCKET.GetPostOffice()->GetConnectionManager(),SNET_CHANNEL_ID
				)))
				{
					m_SessionQueryQosResults[i].m_IsPending = true;
					bValid = true;
				}
			}
			else
			{
				bValid = true;
				//immediately call success on the qos notify handler.
				SessionFinderQosNotifyHandler(&m_SessionQueryQos[i], rlQos::NOTIFY_QOS_QUERY_SUCCEEDED, &m_SessionQueryQosResults[i].m_QosResult);
			}

			if (!bValid)
			{
				m_SessionQueryQosResults[i].m_IsValid=false;
				SessionFinderQosNotifyHandler(&m_SessionQueryQos[i], rlQos::NOTIFY_QOS_QUERY_FAILED, &m_SessionQueryQosResults[i].m_QosResult);
			}
		}

		m_Status = searchBotIdle;
	}

	if (m_pGroupJoinTask)
	{
		m_pGroupJoinTask->Update(deltaMs);

		if (!m_pGroupJoinTask->IsBusy())
		{
			snuGroupJoinRequestTask::snuGroupJoinResponseData data;

			if (m_pGroupJoinTask->GetResponse(data))
			{
				Notify(snuNotifyJoinRequestAccepted, &data);
			}
			else
			{
				snuError(("Group join request task failed"));
			}

			delete m_pGroupJoinTask;
			m_pGroupJoinTask = NULL;
		}
	}
	

	m_LastMsTime = curTime;
}

bool snuSearchBot::IsQueryPending() const
{
	if(m_CurrentTask.Pending())
		return true;

	for(int i = 0; i < NELEM( m_SessionQueryQos ); i++)
	{
		if( m_SessionQueryQos[ i ].IsPending() )
			return true;
	}
	return false;
}

void snuSearchBot::CancelQueryResultsQos()
{
	for(int i = 0; i < NELEM( m_SessionQueryQos ); i++)
	{
		if( m_SessionQueryQos[ i ].IsPending() )
			m_SessionQueryQos[ i ].Cancel();

		m_SessionQueryQosResults[ i ].m_IsValid		= false;
		m_SessionQueryQosResults[ i ].m_IsPending	= false;
	}
}

void snuSearchBot::ClearQueryResults()
{
	CancelQueryResultsQos();
	m_SearchResultArray.m_NumResults = 0;
}

bool snuSearchBot::IsIdle() const
{
	return m_Status == searchBotIdle && !m_SessionFinder.Pending() && !IsQueryPending();
}

bool snuSearchBot::FindSessions(const snuGamerLocal* requestor, int slots, rlMatchingFilter* filter, const bool sysLinkNotOnline)
{
	if( !IsIdle() )
	{
		snuError(( "FindSessions() called while search bot busy" ));
		return false;
	}

	if(!requestor->IsSignedIn())
	{
		snuError(( "FindSessions() called while not signed in" ));
		return false;
	}

	if(!requestor->IsOnline() && !sysLinkNotOnline)
	{
		snuError(( "FindSessions() called while not online" ));
		return false;
	}
	
	m_Status = searchBotActive;
	ClearQueryResults();
	
	SNUSOCKET.GetUserOptions()->DisplayFilterData(*filter);

    m_NetMode = sysLinkNotOnline? RL_NETMODE_LAN : RL_NETMODE_ONLINE;

#if __PS3
	// On PS3, crypto must be disabled for LAN games as it requires communication with PSN,
	// which can't be assumed for LAN.
	SNUSOCKET.GetSocket()->SetCryptoEnabled( !sysLinkNotOnline );
#endif

	bool success = m_SessionFinder.Find(
		m_NetMode,
		requestor->GetGamerInfo(), 
		slots, 
        *filter,
		&m_SearchResultArray,
		&m_CurrentTask );

	if (!success)
	{
		m_Status = searchBotIdle;
	}
	return success;
}

rlSessionSearchResult*	snuSearchBot::GetValidQueryResult(int idx)
{
	int validCount = 0;
	for (int i=0; i!=GetQueryResultCount(); i++)
	{
		if (m_SessionQueryQosResults[i].m_IsValid)
		{
			if (validCount==idx)
				return &GetQueryResult(i);

			validCount++;
		}
	}

	return NULL;
}

int snuSearchBot::GetValidQueryResultCount()
{
	int validCount = 0;
	for (int i=0; i!=GetQueryResultCount(); i++)
	{
		if (m_SessionQueryQosResults[i].m_IsValid)
		{
			validCount++;
		}
	}

	return validCount;
}

void snuSearchBot::SessionFinderQosNotifyHandler( rlQos* qos, const rlQos::NotifyCode code, rlQos::QosResult* result )
{
	int qosIndex = qos - m_SessionQueryQos;
	Assert( qos == &m_SessionQueryQos[ qosIndex ] );

	QueryQosResult& qosResult = m_SessionQueryQosResults[ qosIndex ];
	qosResult.m_IsPending = false;

	switch(code)
	{
	case rlQos::NOTIFY_INVALID:
		qosResult.m_IsValid = false;
		Notify(snuNotifyOk, NULL);
		break;

	case rlQos::NOTIFY_CANCELLED:
		qosResult.m_IsValid = false;
		break;

	case rlQos::NOTIFY_QOS_QUERY_FAILED:
		qosResult.m_IsValid = false;
		snuDebug( "Found a bad session" );
		//if there are valid or pending results, dont do anything
		for (unsigned i=0; i!=m_SearchResultArray.m_NumResults; i++)
		{
			if(m_SessionQueryQosResults[i].m_IsPending || m_SessionQueryQosResults[i].m_IsValid)
				return;
		}
		snuDebug( "All found sessions seem to be bad, notifying fail" );
		Notify(snuNotifyOk, NULL);
		break;

	case rlQos::NOTIFY_QOS_QUERY_SUCCEEDED:
		qosResult.m_IsValid = true;
		qosResult.m_QosResult = *result;

		snuDebug2( "latency for Search Result[%d] = %d ms", qosIndex, result->m_MedRtTimeMS/2 );
		Notify(snuNotifyOk, (snuNotifyBase*)result);
		break;
	}
}

// GROUP JOIN //////////////////////////////////////////

bool snuSearchBot::SendGroupJoinRequest(const snuGamerLocal* pRequestor, int nResultIdx, int nSlotsToReserve)
{
	const rlSessionSearchResult*	pResult = GetValidQueryResult(nResultIdx);
	bool							success	= false;

	if (!pResult && !AssertVerify(m_pGroupJoinTask==NULL))
		return false;

	m_pGroupJoinTask = new snuGroupJoinRequestTask;

	success = m_pGroupJoinTask->Configure(&pRequestor->GetGamerHandle(),pResult,nSlotsToReserve);	

	if (success)
		m_pGroupJoinTask->Start();

	return success;
}

