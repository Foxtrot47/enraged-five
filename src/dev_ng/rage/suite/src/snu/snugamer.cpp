#include "snugamer.h"

#include "snudiag.h"
#include "snusessiondata.h"
#include "snupostoffice.h"
#include "snuoptions.h"
#include "snusocket.h"

#include "rline/rlstats.h"
#include "rline/rlnp.h"
#include "data/bitbuffer.h"
#include "system/nelem.h"

using namespace rage;
using namespace rage::datbitbuffer;

NET_MESSAGE_IMPL( snuGamer::snuGamerMuteMsg );
NET_MESSAGE_IMPL( snuGamer::snuGamerUnmuteMsg );
NET_MESSAGE_IMPL( snuGamer::snuGamerData::snuGamerDataSyncMsg );

#if __XENON
	#include	<xdk.h>
	#include	"system/xtl.h"
	#include	<xbox.h>
#endif // __XENON


int		snuGamer::snuGamerData::g_aBitOffset	[maxGamerVariables+1];
const	snuGamerDataDef*						snuGamer::snuGamerData::g_aDataDef	= NULL;
int		snuGamer::snuGamerData::gDataCount		= 0;

bool snuGamer::snuGamerData::gInitialise(const snuGamerDataDef aGamerDataDef[], int dataCount)
{
	g_aDataDef = aGamerDataDef;
	gDataCount = dataCount;

	Assert(gDataCount <= maxGamerVariables);

	for (int i=0, bit=0; i!=gDataCount+1; i++)
	{
		g_aBitOffset[i] = bit;
		bit += aGamerDataDef[i].m_BitSize;
	}

	return true;
}

int snuGamer::snuGamerData::gGetDataIndexByName	(const char *name)
{
	for (int i=0; i!=gDataCount; i++)
	{
		if (strcmp(name, g_aDataDef[i].m_szName) == 0)
		{
			return i;
		}
	}

	return -1;
}

int	snuGamer::snuGamerData::gGetBitSize(const int idx)				
{ 
	Assert(idx>=0 && idx<gDataCount);

	return g_aBitOffset[idx+1] - g_aBitOffset[idx]; 
}


bool snuGamer::snuGamerData::gIsSigned(const int idx)
{
	Assert(idx>=0 && idx<gDataCount);
	return g_aDataDef[ idx ].m_Sign == syncSigned;
}
const char* snuGamer::snuGamerData::gGetDataName(const int i)
{ 
	return g_aDataDef[i].m_szName;		
}
bool snuGamer::snuGamerData::gIsSyncable(const int i)			
{ 
	return g_aDataDef[i].m_SyncType != syncTypeNone; 
}

//NOTE: this constructor needs to be called on all its objects AFTER snuSocket::Init()
snuGamer::snuGamerData::snuGamerData() : 
	m_IsFieldDirty(0)
{
	m_Data.SetNumBitsWritten(gGetBitSize()); 
}


int snuGamer::snuGamerData::GetData(const int idx, void *dst)	const	
{
	SYS_CS_SYNC(m_CsToken);

	Assert(idx>=0 && idx<gDataCount);

	const int dstBitOffset = 0;
	int numBitsRead = 0;
	if( m_Data.PeekBits( dst, gGetBitSize(idx), dstBitOffset, g_aBitOffset[idx] ) )
	{
		numBitsRead = gGetBitSize(idx);
	}

	return numBitsRead;
}

int snuGamer::snuGamerData::SetData(const int idx, const void *source)
{
	SYS_CS_SYNC(m_CsToken);

	Assert(idx>=0 && idx<gDataCount);

	const int numBitsToWrite = gGetBitSize(idx);
	const int sourceBitOffset = 0;
	const int destBitOffset = g_aBitOffset[idx];
	int numBitsWritten = 0;

	if (numBitsToWrite>0 && 
		AssertVerify(m_Data.PokeBits(source, numBitsToWrite, sourceBitOffset, destBitOffset)) )
	{
		numBitsWritten = numBitsToWrite;
		if (gIsSyncable(idx))
		{
			SetFieldDirty(idx);
		}		
	}

	return numBitsWritten;
}

//FIX 
// this could be even more generic and type safe if value is returned by template reference,
// that way we can precisely check if the stored value will fit in the given size
int snuGamer::snuGamerData::GetDataInt(const int idx) const
{
	SYS_CS_SYNC(m_CsToken);

	const int numBits = gGetBitSize(idx);

	Assert(idx>=0 && idx<gDataCount);
	AssertMsg(gIsSigned(idx)==IntTraits< int >::IsSigned , "Sign mismatch, use GetDataUns()");
	AssertMsg(numBits <= IntTraits< int >::NumBits , "Stored data size is too big for an int");
	AssertMsg(numBits > 1 , "Use unsigned for boolean values");

	int data = 0;
	m_Data.PeekInt( data, numBits, g_aBitOffset[idx] );
	return data;
}

int snuGamer::snuGamerData::SetDataInt(const int idx, const int v)
{
	SYS_CS_SYNC(m_CsToken);

	const int numBits = gGetBitSize(idx);

	Assert(idx>=0 && idx<gDataCount);
	AssertMsg(gIsSigned(idx)==IntTraits< int >::IsSigned , "Sign mismatch, use SetDataUns()");
	AssertMsg(numBits <= IntTraits< int >::NumBits , "Stored data size is too big for an int");
	AssertMsg(numBits > 1 , "Use unsigned for boolean values");

	if (GetDataInt(idx)==v)
		return 0;

	//make sure we are not assigning an int value greater than what the bits can hold
	const int maxValue = (1 << (numBits-1)) - 1;
	if(!AssertVerify(v <= maxValue ) )
		return 0;
	
	const int minValue = -maxValue - 1;
	if(!AssertVerify(v >= minValue ) )
		return 0;

	if( m_Data.PokeInt( v, numBits, g_aBitOffset[idx] ) )
	{
		if( gIsSyncable(idx) )
			SetFieldDirty(idx);

		return numBits;
	}
	return 0;
}

unsigned snuGamer::snuGamerData::GetDataUns(const int idx) const
{
	SYS_CS_SYNC(m_CsToken);

	const int numBits = gGetBitSize(idx);

	Assert(idx>=0 && idx<gDataCount);
	AssertMsg(gIsSigned(idx)==IntTraits< unsigned >::IsSigned , "Sign mismatch, use GetDataInt()");
	AssertMsg(numBits <= IntTraits< unsigned >::NumBits , "Stored data size is too big for an int");

	int data = 0;
	m_Data.PeekUns( data, numBits, g_aBitOffset[idx] );
	return data;
}

int	snuGamer::snuGamerData::SetDataUns(const int idx, const unsigned v)
{
	SYS_CS_SYNC(m_CsToken);

	const int numBits = gGetBitSize(idx);

	Assert(idx>=0 && idx<gDataCount);
	AssertMsg(gIsSigned(idx)==IntTraits< unsigned >::IsSigned , "Sign mismatch, use SetDataInt()");
	AssertMsg(numBits <= IntTraits< unsigned >::NumBits , "Stored data size is too big for an int");

	unsigned uOldValue = GetDataUns(idx);
	if (uOldValue==v)
		return 0;

	//make sure we are not assigning a value greater than what the bits can hold
	unsigned maxValue = numBits-1;
	maxValue = (1<<maxValue) - 1 + (1<<maxValue);
	if(!AssertVerify(v <= maxValue ) )
		return 0;

	if( m_Data.PokeUns( v, numBits, g_aBitOffset[idx] ) )
	{
		if( gIsSyncable(idx) )
		{
			SetFieldDirty(idx);
		}		
		return numBits;
	}
	return 0;
}

void snuGamer::snuGamerData::Clear()
{
	SYS_CS_SYNC(m_CsToken);

	m_Data.Clear();
	m_IsFieldDirty = 0;
}

void snuGamer::snuGamerData::DisplayData(int OUTPUT_ONLY(minDebugLevelDirty), int OUTPUT_ONLY(minDebugLevelClean)) const
{
#if !__NO_OUTPUT

	SYS_CS_SYNC(m_CsToken);

	char	buf[128]	= "";

	for (int i=0; i!=gDataCount; i++)
	{
		if ((IsFieldDirty(i) && snuDebugGetLevel() >= minDebugLevelDirty) || 
			(!IsFieldDirty(i) && snuDebugGetLevel() >= minDebugLevelClean))
		{
			if (g_aDataDef[i].m_PrinterFunc)
				g_aDataDef[i].m_PrinterFunc(*this, i, buf, sizeof(buf));
			else if ( gGetBitSize(i) <= 32)
				formatf(buf, sizeof(buf)-1, "%d", gIsSigned(i) ? GetDataInt(i) : GetDataUns(i) );
			else
				formatf(buf, sizeof(buf)-1, "???");

			if ( IsFieldDirty(i) )
				safecat(buf, " <dirty>", sizeof(buf));

			snuDebug( "\t%s = %s", gGetDataName(i), buf );
		}
	}
#endif
}

snuGamer::snuGamer()
{
	for(int i=0; i!=SNU_MAX_SESSIONS; i++)
		m_SessionSlot[i].Reset();

	m_GamerData.Clear();
	Clear();
}

void snuGamer::Clear()
{
	m_IsReady		= false;
	m_KeepAlive		= false;
	m_Dead			= false;
	m_IsInitialized = false;
	m_IsDataReady	= false;
	m_IsMuted		= false;
	m_IsMutedBy		= false;

	//ClearStats();
	m_GamerData.Clear();
    m_GamerInfo.Clear();
}

void snuGamer::Update()
{
}

int snuGamer::GetFreeSessionSlot() const
{
	for(int i=0; i!=SNU_MAX_SESSIONS; i++)
	{
		if(m_SessionSlot[i].m_pSessionData == NULL )
			return i;
	}

	return -1;
}

int	snuGamer::GetSessionCount()	const
{
	int count=0;

	for(int i=0; i!=SNU_MAX_SESSIONS; i++)
	{
		if (m_SessionSlot[i].m_pSessionData != NULL)
			count++;
	}

	return count;
}

snSession* snuGamer::GetMySession(int i) const
{ 
	Assert(0<=i && i<SNU_MAX_SESSIONS);
	Assert(m_SessionSlot[i].m_pSessionData!=NULL);

	return m_SessionSlot[i].m_pSessionData->GetSession();
}

void snuGamerLocal::LeaveSession(snuSessionData *sessionData, netStatus* pTaskStatus)
{
	if (pTaskStatus==NULL && !m_CurrentTask.Pending())
		pTaskStatus = &m_CurrentTask;

	snuGamer::LeaveSession(sessionData,pTaskStatus);
}

bool snuGamerLocal::DestroySession(snuSessionData *sessionData, netStatus* pTaskStatus)
{
	if (pTaskStatus==NULL && !m_CurrentTask.Pending())
		pTaskStatus = &m_CurrentTask;

	if(AssertVerify(sessionData))
		return sessionData->GetSession()->Destroy(pTaskStatus);

	return false;
}

void snuGamer::LeaveSession(snuSessionData *sessionData, netStatus* pTaskStatus)
{

	int i;
	for(i=0; i!=SNU_MAX_SESSIONS; i++)
	{
		if(m_SessionSlot[i].m_pSessionData == sessionData)
		{
			if(sessionData->GetSession()->Exists() && sessionData->GetSession()->IsMember(GetGamerInfo()))
				sessionData->GetSession()->Leave(GetGamerInfo(), pTaskStatus);
#if __PPU

			{
				//This looks wrong. Local gamer isn't necessarily leaving here....
				SNUSOCKET.GetPresenceManager()->SetPresence(0,0);
			}
#endif
			m_SessionSlot[i].Reset();
            break;
		}
	}

	//Assert(i!=SNU_MAX_SESSIONS && "matching session not found");
}

void snuGamer::SetMuted(bool b)
{ 
	if (m_IsMuted != b)
	{
		m_IsMuted = b;

		if (m_IsMuted)
		{
			snuDebug1( "Sending snuGamerMuteMsg to %s", GetName() );
			snuGamerMuteMsg muteMsg;
			SNUSOCKET.GetPostOffice()->SendToGamer(*this, muteMsg, NET_SEND_RELIABLE);
		}
		else
		{
			snuDebug1( "Sending snuGamerUnmuteMsg to %s", GetName() );
			snuGamerUnmuteMsg unmuteMsg;
			SNUSOCKET.GetPostOffice()->SendToGamer(*this, unmuteMsg, NET_SEND_RELIABLE);
		}
	}
}


void snuGamer::snuGamerData::snuGamerDataSyncMsg::WriteGamerData(snuGamer& gamer) const
{
	snuGamerData* pData	= gamer.GetGamerData();

	SYS_CS_SYNC(pData->m_CsToken);

#if !__NO_OUTPUT
	if (gamer.IsDataReady())
	{
		snuDebug3( "Writing data from snuGamerDataSyncMsg to %s. Before:", gamer.GetName() );
		pData->DisplayData(3,3);
	}
#endif

	m_SyncBuffer.SetCursorPos(0);

	for (int i=0; i!=pData->gGetDataCount(); i++)
	{
		//field present?
		if ((m_IsFieldPresent & 1<<i) > 0)
		{
			snuDebug3( "snuGamerDataSyncMsg from Gamer, %s; Writing data %s" , gamer.GetName(), pData->gGetDataName(i) );

			pData->m_Data.SetCursorPos(pData->g_aBitOffset[i]);
			m_SyncBuffer.ReadBits(pData->m_Data, pData->gGetBitSize(i));
		}
	}

	pData->m_IsFieldDirty = m_IsFieldPresent;

	snuDebug2( "Gamer Data updated for %s. After:", gamer.GetName() );
	pData->DisplayData(2,3);
}

void snuGamer::snuGamerData::snuGamerDataSyncMsg::ReadDirtyGamerData(const snuGamer& gamer)
{
	m_SyncBuffer.SetCursorPos	(0);
	m_IsFieldPresent			= 0;
	const snuGamerData* pData	= gamer.GetGamerData();

	SYS_CS_SYNC(pData->m_CsToken);

	for (int i=0; i!=snuGamer::snuGamerData::gDataCount; i++)
	{
		if(	pData->IsFieldDirty(i) && AssertVerify(pData->gIsSyncable(i))	)
		{
			pData->m_Data.SetCursorPos(pData->g_aBitOffset[i]);
			m_SyncBuffer.WriteBits(gamer.GetGamerData()->m_Data, pData->gGetBitSize(i));

			m_IsFieldPresent |= 1<<i;
		}
	}

	m_GamerID = gamer.GetGamerId();
}

void snuGamer::snuGamerData::snuGamerDataSyncMsg::ReadGamerData(const snuGamer& gamer, int fieldID)
{
	m_SyncBuffer.SetCursorPos	(0);
	m_IsFieldPresent			= 0;
	const snuGamerData* pData	= gamer.GetGamerData();

	SYS_CS_SYNC(pData->m_CsToken);

	if (AssertVerify(fieldID<gDataCount) && AssertVerify(pData->gIsSyncable(fieldID)) )
	{
		pData->m_Data.SetCursorPos(pData->g_aBitOffset[fieldID]);
		m_SyncBuffer.WriteBits(pData->m_Data, pData->gGetBitSize(fieldID));

		m_IsFieldPresent |= 1<<fieldID;
	}

	m_GamerID = gamer.GetGamerId();
}

void snuGamer::snuGamerData::snuGamerDataSyncMsg::ReadSyncableGamerData(const snuGamer& gamer)
{
	m_SyncBuffer.SetCursorPos	(0);
	m_IsFieldPresent			= 0;
	const snuGamerData* pData	= gamer.GetGamerData();

	SYS_CS_SYNC(pData->m_CsToken);

	for (int i=0; i!=snuGamer::snuGamerData::gDataCount; i++)
	{
		if (pData->gIsSyncable(i))
		{
			pData->m_Data.SetCursorPos(pData->g_aBitOffset[i]);
			m_SyncBuffer.WriteBits(gamer.GetGamerData()->m_Data, pData->gGetBitSize(i));

			m_IsFieldPresent |= 1<<i;
		}
	}

	m_GamerID = gamer.GetGamerId();
}

int	snuGamer::SetDataInt(const int i, const int v)
{ 
	int bits = m_GamerData.SetDataInt(i,v);

	if (!IsLocal() && bits && m_GamerData.gIsSyncable(i) && !IsDead() )
	{
		snuDebug2( "Setting and synching %s data of remote gamer %s", snuGamerData::gGetDataName(i), GetName() );

		snuGamer::snuGamerData::snuGamerDataSyncMsg msg;
		msg.ReadGamerData(*this,i);
		SNUSOCKET.GetPostOffice()->SendToGamer(*this, msg, NET_SEND_RELIABLE);
	}

	return bits;
}

int	snuGamer::SetDataUns(const int i, const unsigned v)	
{
	int bits = m_GamerData.SetDataUns(i,v);

	if (!IsLocal() && bits && m_GamerData.gIsSyncable(i) && !IsDead() )
	{
		snuDebug2( "Setting and synching %s data of remote gamer %s", snuGamerData::gGetDataName(i), GetName() );

		snuGamer::snuGamerData::snuGamerDataSyncMsg msg;
		msg.ReadGamerData(*this,i);
		SNUSOCKET.GetPostOffice()->SendToGamer(*this, msg, NET_SEND_RELIABLE);
	}

	return bits;
}

int	snuGamer::SetData(const int i, const void *v)	
{ 
	int bits = m_GamerData.SetData(i,v);

	if (!IsLocal() && bits && m_GamerData.gIsSyncable(i) && !IsDead() )
	{
		snuDebug2( "Setting and synching %s data of remote gamer %s", snuGamerData::gGetDataName(i), GetName() );

		snuGamer::snuGamerData::snuGamerDataSyncMsg msg;
		msg.ReadGamerData(*this,i);
		SNUSOCKET.GetPostOffice()->SendToGamer(*this, msg, NET_SEND_RELIABLE);
	}

	return bits;
}

//==================================================================================================================================
//											snuGamerLocal
//==================================================================================================================================

snuGamerLocal::snuGamerLocal() 
: snuGamer			()
, m_pSessionJoining	(0)
, m_FriendCount		(0)
, m_FriendsToRead	(false)
, m_pStats			(rage_new rlStats)
, m_pFriendInvites	(rage_new snuFriendInvites)
{
	m_CurrentJoinWish.Reset();
}

snuGamerLocal::~snuGamerLocal()
{
	delete m_pStats;
	delete m_pFriendInvites;
}


bool snuGamerLocal::SyncAllMyDataToGamer(const snuGamer* pGamer)
{
	snuGamerData::snuGamerDataSyncMsg msg;

	snuDebug2( "Local Gamer, %s sending all data to %s...", GetName(), pGamer->GetName() );
	m_GamerData.DisplayData(3,3);
	msg.ReadSyncableGamerData(*this);

	return SNUSOCKET.GetPostOffice()->SendToGamer(*pGamer, msg, NET_SEND_RELIABLE );
}


bool snuGamerLocal::SynchroniseMyData()
{
	if (!m_GamerData.IsDirty())
		return true;

	snuGamerData::snuGamerDataSyncMsg msg;

	snuDebug2( "Local Gamer, %s, has dirty data; Broadcasting dirty bits...", GetName() );
	m_GamerData.DisplayData(2,3);
	msg.ReadDirtyGamerData(*this);

	bool success = SNUSOCKET.GetPostOffice()->SendToAll( msg, NET_SEND_RELIABLE );

	SNUSOCKET.Notify(snuSocket::snuNotifyGamerDataUpdated, this);

	return success;
}

bool snuGamerLocal::Init(const rlGamerInfo& gamerInfo)
{
	Assert(m_pStats);

	if (!gamerInfo.IsValid())
	{
		snuDebug2( "snuGamerLocal::Init() aborted, valid gamer required" );
		return false;
	}

	if (m_GamerInfo.IsValid() && gamerInfo.IsValid() && gamerInfo == m_GamerInfo)
	{
		snuDebug2( "snuGamerLocal::Init() aborted, attempted to reinitialize to the same gamer" );
		return true;
	}

	UnInit();

	m_pSessionJoining = NULL;
	m_CurrentJoinWish.Reset();

    //friends
	m_FriendsToRead		= false;
	AssertVerify( m_FriendsReader.Init(SNUSOCKET.GetCpuAffinity()) );

	this->m_rlAchievement.Init(SNUSOCKET.GetCpuAffinity());

	this->m_TitleStorage.Init(SNUSOCKET.GetCpuAffinity());


	//TODO: set handlers for pics
	//GiveAchievement(this);

	m_pStats->Init();

	//gamer
    return this->snuGamer::InitGamerInfo(gamerInfo);
}

void snuGamerLocal::UnInit()
{
	if (!m_GamerInfo.IsValid())
	{
		snuDebug1( "snuGamerLocal::UnInit() Attempting to UnInit an invalid gamer?" );
		return;
	}

	m_pSessionJoining = NULL;
	m_CurrentJoinWish.Reset();

	while (m_FriendsReader.Pending())
	{
		snuDebug1( "snuGamerLocal::UnInit() waiting for pending FriendsReader op before Shutdown..." );
		sysIpcSleep(1);
	}
	m_FriendsReader.Shutdown();

	while (m_rlAchievement.Pending())
	{
		snuDebug1( "snuGamerLocal::UnInit() waiting for pending Achievement op before Shutdown..." );
		sysIpcSleep(1);
	}
	m_rlAchievement.Shutdown();

	while (m_TitleStorage.Pending())
	{
		snuDebug1( "snuGamerLocal::UnInit() waiting for pending title storage op before Shutdown..." );
		sysIpcSleep(1);
	}
	m_TitleStorage.Shutdown();

	m_pStats->Shutdown();
	Shutdown();
}

void snuGamerLocal::Update()
{
	//just finished a join task?
	if (m_pSessionJoining && !m_CurrentTask.Pending()) 
	{
		snuSessionData* pSession = m_pSessionJoining;

		m_pSessionJoining = NULL;

		if(m_CurrentTask.Succeeded())
		{
#if __PPU
			{
				//Put our session info in our presence data, so friends can join via presence.
				u8 data[RL_MAX_PRESENCE_DATA_SIZE];
				unsigned dataSize = 0;

				const rlSessionInfo& sessionInfo = pSession->GetSession()->GetSessionInfo();

				if(sessionInfo.Export(data, sizeof(data), &dataSize))
				{
					SNUSOCKET.GetPresenceManager()->SetPresence(data, dataSize);
				}
				//SNUSOCKET.GetPresenceManager()->SetPresence(&pSession->GetSession()->GetSessionInfo(), sizeof(rlSessionInfo));
			}
#endif
			SNUSOCKET.Notify(snuSocket::snuNotifyJoined, pSession);

		}
		//We've failed
		else
		{
			LeaveSession(pSession);
			snuNotifyDataFailedRequested notifyData;
			notifyData.m_pSessionData = pSession;
			notifyData.m_pJoinWish = &m_CurrentJoinWish;
			SNUSOCKET.Notify(snuSocket::snuNotifyFailedRequest, &notifyData);

			m_CurrentJoinWish.Reset();
			
		}
	}

	//Friends Reader status is not really used at the moment.
	if (!m_FriendsReaderStatus.Pending())
	{
		switch(m_FriendsReaderStatus.GetStatus())
		{
		case netStatus::NET_STATUS_SUCCEEDED:
            {
#if !__NO_OUTPUT
			    snuDebug1( "Friends Read operation succeeeded (%d found)", m_FriendCount );
                for(unsigned i = 0; i < m_FriendCount; i++)
                {
                    rlFriend& f = m_aFriend[i];
                    snuDebug1("Friend %d: %s [%s] [%s]", 
                               i, 
                               f.GetName(), 
                               f.IsOnline() ? "ONLINE" : "OFFLINE",
                               f.IsInSameTitle() ? "SAME GAME" : "DIFFERENT GAME");
                }
#endif

			    m_FriendsReaderStatus.Reset();
            }
			break;

		case netStatus::NET_STATUS_FAILED:
			snuError(( "Friends Read operation failed!" ));
			m_FriendsReaderStatus.Reset();
			break;

		case netStatus::NET_STATUS_CANCELED:
			snuError(( "Friends Read operation canceled!" ));
			m_FriendsReaderStatus.Reset();
			break;

		default:
			break;
		}
	}

	if(m_AchievementsToWrite.GetCount() > 0)
	{
		if(!m_AchievementStatus.Pending())
		{
			int nTop =  m_AchievementsToWrite.Top();
			this->m_rlAchievement.Write(this->GetGamerInfo(), nTop, &m_AchievementStatus);
			m_AchievementsToWrite.Pop();
		}
	}

	m_pStats->Update();
	snuGamer::Update();
}

void snuGamerLocal::ShowSignInUI(bool XENON_ONLY(b_OnlineNeeded) PPU_ONLY(b_OnlineNeeded))
{
#if __XENON
	// NOTE: 
	//   With the ONLYONLINEENABLED flag, users get the option to choose Guest profiles.
	//   Guest profiles are useless in our game with regard to online play (insufficient privileges).
	//   Might be good to remove all the signin ui flags, and just let users pick from whatever
//	 actual, non-guest, profiles that they have.

	XShowSigninUI(1, b_OnlineNeeded ? XSSUI_FLAGS_SHOWONLYONLINEENABLED : XSSUI_FLAGS_LOCALSIGNINONLY);
#elif __PPU
	if(b_OnlineNeeded)
		SNUSOCKET.GetSysUi().ShowSigninUi();
#endif
}

bool snuGamerLocal::ShowGamerCardUI(const rlGamerHandle* pGamerHandle) const
{
	return pGamerHandle && SNUSOCKET.GetSysUi().ShowGamerProfileUi(GetLocalGamerIndex(), *pGamerHandle);
}

bool snuGamerLocal::ShowGamerReviewUI(const rlGamerHandle* pGamerHandle) const
{
	return pGamerHandle && SNUSOCKET.GetSysUi().ShowGamerFeedbackUi(GetLocalGamerIndex(), *pGamerHandle);
}

snuSessionData*	snuGamerLocal::NewSession(const snuSessionId& sid, const rlSessionInfo *info)
{
	int slot = GetFreeSessionSlot();

	if( slot!=-1 )
	{
		m_SessionSlot[slot].Reset();
		m_SessionSlot[slot].m_pSessionData	= SNUSOCKET.NewSession(sid, info);
		snuDebug2( "Session[%d] allocated by Gamer, %s, in Session Slot[%d]", 
			m_SessionSlot[slot].m_pSessionData->GetSessionIndex(), GetName(), slot );
		return m_SessionSlot[slot].m_pSessionData;
	}
	else
	{
		return NULL;
	}
}

/*void snuGamerLocal::LeaveSession(snuSessionData* sessionData)
{
	snuGamer::LeaveSession(sessionData);

	if (IsOnline() && IsMember(sessionData->GetSession()))
		sessionData->GetSession()->LeaveLocal( this->GetGamerInfo() );
}
*/

bool snuGamerLocal::JoinSession(const snuSessionId& sid, const snuGamerJoinWish& rWish)
{
	const rlNetworkMode netMode	= rWish.m_NetMode;
    Assert(RL_NETMODE_INVALID != netMode);

	snuDebug1( "Requesting to join game session..." );

	if(!IsSignedIn())
	{
		snuError(( "JoinSession() called while not signed in" ));
		return false;
	}

	if(!IsOnline() && netMode==RL_NETMODE_ONLINE)
	{
		snuError(( "JoinSession() called while not online" ));
		return false;
	}

	if( m_pSessionJoining!=NULL || m_CurrentTask.Pending() )
	{
		snuError(( "JoinSession() called while gamer is busy" ));
		return false;
	}

#if __PS3
	// On PS3, crypto must be disabled for LAN games as it requires communication with PSN,
	// which can't be assumed for LAN.
	SNUSOCKET.GetSocket()->SetCryptoEnabled( netMode == RL_NETMODE_ONLINE );
#endif

	snSession*		session		= NULL;
	snuSessionData*	sessionData = NewSession(sid, &rWish.m_SessionInfo);
	u32				createFlags = (rWish.m_Config.m_Attrs.GetGameType()==RL_GAMETYPE_RANKED)? rlSession::CREATE_ARBITRATED:0; //MIGRATE TODO: should CREATE_INVITES_DISABLED be added for ranked?


	if(sessionData==NULL)
	{
		snuError(( "Error: SessionData slots full!" ));
		return false;
	}
	session = sessionData->GetSession();

	//only session zero gets presence (yes this is somewhat of a hardcoded hack)
	if (sid>0)
	{
		createFlags |= rlSession::CREATE_NO_PRESENCE;
	}

	if(!session->Join
	(
		this->GetGamerInfo(), 
		rWish.m_SessionInfo,
		netMode,
		rWish.m_Config.m_Attrs.GetGameType(),
		rWish.m_FromInvite? RL_SLOT_PRIVATE : RL_SLOT_PUBLIC,
		//rWish.m_HostAddr,	//MIGRATE TODO: lan host addr no longer needed, remove from Wish?
		createFlags,
		NULL,
		0,
		&m_CurrentTask
	))
	{
		LeaveSession(sessionData);
		return false;
	}

	/* NOT NEEDED?
	int	gamerIndex	= -1;
	gamerIndex = sessionData->GetFreeGamerSlot();
	if(gamerIndex==snuSessionData::GamerIndexVoid)
	{
		LeaveSession(sessionData);
		snuError(( "peer slots full" ));
		return false;
	}
	sessionData->SetGamerSlot(gamerIndex, *this);
	*/

	m_pSessionJoining = sessionData;
	m_CurrentJoinWish = rWish;
	return true;
}

bool snuGamerLocal::HostSession(const snuSessionId& sid, const rlMatchingAttributes& _attrs, bool sysLink, bool bAllowJoinInProgress)
{
	snuDebug1( "Requesting to host game session..." );

	if(!IsSignedIn())
	{
		snuError(( "HostSession() called while not signed in" ));
		return false;
	}

	if(!IsOnline() && !sysLink)
	{
		snuError(( "HostSession() called while not online" ));
		return false;
	}

	if( m_pSessionJoining!=NULL || m_CurrentTask.Pending() )
	{
		snuError(( "HostSession() called while gamer is busy" ));
		return false;
	}

    const bool isRanked = (_attrs.GetGameType() == RL_GAMETYPE_RANKED);

	rlNetworkMode			netMode			= sysLink? RL_NETMODE_LAN: RL_NETMODE_ONLINE;
	u32						createFlags		= sysLink? 0 : (isRanked? rlSession::CREATE_ARBITRATED:0);  //MIGRATE TODO: should CREATE_INVITES_DISABLED be added for ranked?
	u32						port			= SNUSOCKET.GetPostOffice()->GetConnectionManager()->GetSocket()->GetPort();
	rlMatchingAttributes	attrs			= _attrs;
	int						gamerIndex		= snuSessionData::GamerIndexVoid;
	snuSessionData*			sessionData 	= NewSession(sid, NULL);
	const snuUserOptions&	rOptions		= *SNUSOCKET.GetUserOptions();


	//only session zero gets presence (yes this is somewhat of a hardcoded hack)
	if (sid>0)
	{
		createFlags |= rlSession::CREATE_NO_PRESENCE;
	}

	//If we don't want to allow "join in progress", update the flag
	if(!bAllowJoinInProgress)
	{
		createFlags |= rlSession::CREATE_JOIN_IN_PROGRESS_DISABLED;
	}

	if(sessionData==NULL)
	{
		snuError(( "SessionData slots full!" ));
		return false; 
	}

	gamerIndex = sessionData->GetFreeGamerSlot();
	if(gamerIndex==snuSessionData::GamerIndexVoid)
	{
		LeaveSession(sessionData);
		snuError(( "peer slots full" ));
		return false;
	}
	sessionData->SetGamerSlot(gamerIndex, *this);

	int	fieldIdx;
	
	int	numPrivateSlots;
	int	numPublicSlots;
	
	//get private slots
	fieldIdx = rOptions.GetOptionIndex("NumPrivateSlots");
	Assert(fieldIdx>=0);
	Assert(attrs.GetValueById(rOptions.GetOption(fieldIdx).GetID()));
	numPrivateSlots = *attrs.GetValueById(rOptions.GetOption(fieldIdx).GetID());

	//get public slots
	fieldIdx = rOptions.GetOptionIndex("MaxPlayers");		
	Assert(fieldIdx>=0);
	Assert(attrs.GetValueById(rOptions.GetOption(fieldIdx).GetID()));
	numPublicSlots = *attrs.GetValueById(rOptions.GetOption(fieldIdx).GetID());
	numPublicSlots -= numPrivateSlots;

	//set network port
	fieldIdx = rOptions.GetOptionIndex("NetworkPort");		
	Assert(fieldIdx>=0);
	Assert(attrs.GetValueById(rOptions.GetOption(fieldIdx).GetID()));
	port = *attrs.GetValueById(rOptions.GetOption(fieldIdx).GetID());

	//debug
    //FIXME FIXME FIXME
	//Assert("FIXME FIXME FIXME" && false);//SNUSOCKET.GetUserOptions()->DisplaySchemaData(schema);

#if __PS3
	// On PS3, crypto must be disabled for LAN games as it requires communication with PSN,
	// which can't be assumed for LAN.
	SNUSOCKET.GetSocket()->SetCryptoEnabled(netMode == RL_NETMODE_ONLINE);
#endif

	SNUSOCKET.GetUserOptions()->DisplayAttributeData(attrs);

	//host session
	if(!sessionData->GetSession()->Host
	( 
		this->GetGamerInfo(),
		netMode,
		numPublicSlots,
		numPrivateSlots,
		attrs,
		createFlags,
		NULL,
		0,
		&m_CurrentTask
	))
	{
		LeaveSession(sessionData);
		return false;
	}

	m_pSessionJoining = sessionData;
	return true;
}



void snuGamerLocal::LeaveMySession(int sessionIdx)
{
	snuSessionData* pSession = GetSessionData(sessionIdx);
	
	if (pSession)
		pSession->GetSession()->Leave(GetGamerInfo(), &m_CurrentTask);
}


void snuGamerLocal::RefreshFriends()
{
	if (!m_FriendsReader.Pending())
	{
		m_FriendsReader.Read( GetGamerInfo().GetLocalIndex(), 0, m_aFriend, NELEM(m_aFriend), &m_FriendCount, &m_FriendsReaderStatus );
	}
}

bool snuGamerLocal::AcceptInvite(const snuInvite* pInvite)
{
	if (AssertVerify(pInvite))
 	{
		m_pFriendInvites->RemoveInvites(pInvite->m_SessionInfo);

#if !__LIVE
		SNUSOCKET.GetPresenceManager()->NotifyInviteAccepted(GetLocalGamerIndex(), pInvite->m_SessionInfo, pInvite->m_Inviter);
//TMS: This was in MC's cut of RAGE only. Will need further integration.
//		SNUSOCKET.GetPresenceManager()->NotifyInviteAccepted(GetLocalGamerIndex(), pInvite->m_SessionInfo, NULL);
		return true;
#endif
	}

	return false;
}

bool snuGamerLocal::AddFriend(const rlGamerHandle& rHandle)
{
	if (AssertVerify(rHandle.IsValid()) && !m_FriendTask.Pending())
	{
	#if __PPU
		AssertMsg(0,"TMS: Currently not implemented");
		//return g_rlNp.AddFriend(rHandle.GetSceNpId(), "", &m_FriendTask);
	#endif
	}	

	return false;
}

const rage::rlFriend* snuGamerLocal::GetFriend(const rlGamerHandle& rHandle) const
{
	for (unsigned i=0; i<m_FriendCount; i++ )
	{
		rlGamerHandle hFriend;
		m_aFriend[i].GetGamerHandle(&hFriend);

		if (hFriend==rHandle)
			return m_aFriend+i;
	}

	return NULL;
}


//Achievements
void snuGamerLocal::AwardAchievement( const int achievementId)
{
	m_AchievementsToWrite.PushAndGrow(achievementId);
	//return this->m_rlAchievement.Write(this->GetGamerInfo(), achievementId, status);
}
bool snuGamerLocal::ViewAchievements(const rlGamerInfo& gamerInfo,rlAchievementInfo* achievements,
												const int maxAchievements,int* numAchievements,netStatus* status)
{
	return this->m_rlAchievement.Read(this->GetGamerInfo(), gamerInfo, achievements, maxAchievements, numAchievements, status);
}

//Leader-boards
bool 
snuGamerLocal::ReadStatsByGamer(rlLeaderboard* leaderboard,
							const rlGamerHandle* gamerHandles,
							const int numGamers,
							netStatus* status)
{
	const bool success =  m_pStats->ReadByGamer(leaderboard, gamerHandles,numGamers,status);

	return success;
}

bool 
snuGamerLocal::ReadStatsByRank( rlLeaderboard* leaderboard,
							   const unsigned startRank,
							   netStatus* status)
{
	const bool success = m_pStats->ReadByRank(leaderboard,
		startRank,
		status);

	return success;
}
bool 
snuGamerLocal::GetLeaderboardSize(const rlLeaderboardInfo* lbInfo,
                            unsigned* numRows,
                            netStatus* status)
{
	const bool success = m_pStats->GetLeaderboardSize(lbInfo,
		numRows,
		status);

	return success;
}

// Title Storage
bool 
snuGamerLocal::DownloadFile(const char* filename,              
							char* buf,
							const unsigned bufSize,
							unsigned *downloadSize,
							netStatus* status)
{
	const bool success = m_TitleStorage.DownloadFile(this->GetGamerInfo(),
		filename,              
		buf,
		bufSize,
		2000 /*timeout*/,
		downloadSize,
		status);
	return success;
}


/////////////////////////////////////////// NOTIFICATION HANDLERS ///////////////////////////////////////////////////////////

void snuGamerLocal::HandlePresenceEvent(const rage::rlPresenceEvent* pEvent)
{
	switch(pEvent->GetId())
	{
	case PRESENCE_EVENT_INVITE_RECEIVED:
		if (HasMultiplayerPrivileges())
		{
			rlPresenceEventInviteReceived* ie = (rlPresenceEventInviteReceived*)pEvent;

			m_pFriendInvites->AddInvite(ie);
		}
		break;
	}
}


bool 
snuGamerLocal::HasMultiplayerPrivileges() const
{
	return rlGamerInfo::HasMultiplayerPrivileges( GetLocalGamerIndex() );
}

bool 
snuGamerLocal::HasChatPrivileges(bool bFriendsOnly) const
{
    const rlGamerInfo::PrivilegeType privType =
        bFriendsOnly ? rlGamerInfo::PRIVILEGE_FRIENDS_ONLY
                    : rlGamerInfo::PRIVILEGE_EVERYONE;
	return rlGamerInfo::HasChatPrivileges( GetLocalGamerIndex(), privType );
}

bool 
snuGamerLocal::HasUserContentPrivileges() const
{
#if __PPU
	// Sony does not have anything like user content privileges so jay and mike 
	// assume if your not old enough for multi player your not old enough for user content
	return HasMultiplayerPrivileges();
#else
	return rlGamerInfo::HasUserContentPrivileges( GetLocalGamerIndex(), rlGamerInfo::PRIVILEGE_EVERYONE );
#endif
}

bool 
snuGamerLocal::HasUserContentPrivilegesFriendsOnly() const
{
	return rlGamerInfo::HasUserContentPrivileges( GetLocalGamerIndex(), rlGamerInfo::PRIVILEGE_FRIENDS_ONLY );
}

bool 
snuGamerLocal::IsFriendsWith(const snuGamer& otherGamer) const
{
#if __XENON
	return rlGamerInfo::IsFriendsWith( GetLocalGamerIndex(), otherGamer.GetGamerHandle() );
#else
	rlGamerHandle friendHandle;
	for (unsigned i=0; i < m_FriendCount; ++i)
	{
		const rage::rlFriend* myFriend = GetFriend(i);
		if (myFriend)
		{
			myFriend->GetGamerHandle(&friendHandle);
			if (friendHandle == otherGamer.GetGamerHandle())
			{
				return true;
			}
		}
	}
	return false;
#endif
}

//Downloadables
void snuGamerLocal::GetContentCounts( int & numNewContent, int & numExistingContent ) const
{
	numNewContent = 0;
	numExistingContent = 0;
#if __XENON
	XOFFERING_CONTENTAVAILABLE_RESULT contentCounts;
	if (ERROR_SUCCESS  == XContentGetMarketplaceCounts( GetLocalGamerIndex(), 0xffff, sizeof(contentCounts), &contentCounts, NULL))
	{
		numNewContent = contentCounts.dwNewOffers;
		numExistingContent = contentCounts.dwTotalOffers;
	}
#endif // __XENON
}

