// 
// snu\snudiag.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "snudiag.h"
#include "bank/bank.h"
#include "system/param.h"

RAGE_DEFINE_CHANNEL(snu)

#if !__NO_OUTPUT
int
snuDebugGetLevel()
{
	int level = Channel_snu.TtyLevel - rage::DIAG_SEVERITY_DISPLAY;
	return (level > 0)? level : 0;
}
#endif
