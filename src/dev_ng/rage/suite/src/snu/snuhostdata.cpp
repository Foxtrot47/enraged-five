// 
// snu/snugamerdata.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "snuhostdata.h"
#include "snudiag.h"
#include "snugamer.h"
#include "snuoptions.h"
#include "snupostoffice.h"
#include "snusessiondata.h"
#include "snusocket.h"

using namespace rage;
using namespace rage::datbitbuffer;

////////////////////////////////////////////////////////////////////////////////
// snuHostDataSyncMsg
////////////////////////////////////////////////////////////////////////////////
NET_MESSAGE_IMPL(snuHostDataSyncMsg);

void 
snuHostDataSyncMsg::InitForWrite(const snuSessionId& sid, bool isAllData)
{
    m_SessionId = sid;
    m_IsAllData = isAllData;
}

snuHostData*
snuHostDataSyncMsg::GetHostDataPtr() const
{
    Assert(m_SessionId != snuSessionId_Invalid);
    snuSessionData* sessionData = SNUSOCKET.GetSessionById(m_SessionId);
    if(sessionData)
    {
        return &sessionData->GetHostData();
    }
    return NULL;
}

////////////////////////////////////////////////////////////////////////////////
// snuHostData
////////////////////////////////////////////////////////////////////////////////
snuHostData::snuHostData(snuSessionData& sessionData)
: m_IsDefined(false)
, m_IsReady(false)
, m_SessionData(sessionData)
{
    m_FieldDefs.Reset();
    m_BitOffsets.Reset();
    m_BitsUsed.Reset();

    memset(m_DataBuffer, 0, sizeof(m_DataBuffer));
    m_DataBits.Reset();
    m_DataBits.SetReadWriteBits(m_DataBuffer, sizeof(m_DataBuffer)*8, 0);
    m_DataBits.SetNumBitsWritten(sizeof(m_DataBuffer)*8);

    memset(m_DirtyBitsBuffer, 0, sizeof(m_DirtyBitsBuffer));
    m_DirtyBits.Reset();
    m_DirtyBits.SetReadWriteBits(m_DirtyBitsBuffer, MAX_FIELDS, 0);
    m_DirtyBits.SetNumBitsWritten(MAX_FIELDS);

    memset(m_ValidBitsBuffer, 0, sizeof(m_ValidBitsBuffer));
    m_ValidBits.Reset();
    m_ValidBits.SetReadWriteBits(m_ValidBitsBuffer, MAX_FIELDS, 0);
    m_ValidBits.SetNumBitsWritten(MAX_FIELDS);
}

snuHostData::~snuHostData()
{
    m_IsDefined = false;
}

bool 
snuHostData::DefineFields(const snuFieldDef fieldDefs[], 
                          int fieldCount, 
                          bool bSessionCreator)
{
    SYS_CS_SYNC(m_CsToken);

    if(AssertVerify(!m_IsDefined) && AssertVerify(fieldCount <= MAX_FIELDS))
    {
        int bitOffset = 0;
        for(int i=0; i!=fieldCount+1; ++i)
        {
            m_BitOffsets.Append() = bitOffset;
            m_BitsUsed.Append() = 0;

            if(i < fieldCount)
            {
                bitOffset += fieldDefs[i].m_BitSize;
                m_FieldDefs.Append() = fieldDefs[i];
            }
        }
        m_IsDefined = true;

        //clear all the fields first since this will set the dirty bits
        ClearAllFields();

        //then clear dirty bits
        ClearDirtyBits();

        m_IsReady = bSessionCreator;

        return true;
    }
    return false;
}

void 
snuHostData::UndefineFields()
{
    SYS_CS_SYNC(m_CsToken);

    Assert(m_IsDefined);
    m_FieldDefs.Reset();
    m_BitOffsets.Reset();
    m_BitsUsed.Reset();
    m_IsDefined = false;
    m_IsReady = false;
}

int	
snuHostData::GetFieldIndexByName(const char *name) const
{
    if(m_IsDefined)
    {
        for(int i=0; i!=GetFieldCount(); ++i)
        {
            if(!strcmp(m_FieldDefs[i].m_szName, name))
            {
                return i;
            }
        }
    }
    return -1;
}

int	
snuHostData::GetFieldBitSize(const int i) const
{
    if(m_IsDefined)
    {
        return m_BitOffsets[i+1] - m_BitOffsets[i];
    }
    return 0;
}

int	
snuHostData::GetTotalBitSize() const
{ 
    if(m_IsDefined)
    {
        const int fieldCount = GetFieldCount();
        return m_BitOffsets[ fieldCount ];
    }
    return 0;
}

int	
snuHostData::GetFieldCount() const
{ 
    if(m_IsDefined)
    {
        return m_FieldDefs.GetCount();
    }
    return 0;
}

snuHostData::FieldType 
snuHostData::GetFieldType(const int i) const
{ 
    if(m_IsDefined)
    {
        return m_FieldDefs[i].m_Type;
    }
    return FIELD_UNDEFINED;
}

snuSessionData& 
snuHostData::GetSessionData()
{
    return m_SessionData;
}

int 
snuHostData::GetData(const int idx, void *dst) const
{
    SYS_CS_SYNC(m_CsToken);

    int numBitsRead = 0;
    if(m_IsDefined && AssertVerify(m_IsReady) && AssertVerify(GetFieldType(idx)==FIELD_VOID)
        && AssertVerify(IsFieldValid(idx)))
    {
        const int dstBitOffset = 0;
        if(m_DataBits.PeekBits(dst, GetFieldBitSize(idx), dstBitOffset, m_BitOffsets[idx]))
        {
            numBitsRead = GetFieldBitSize(idx);
        }
    }
    return numBitsRead;
}

int 
snuHostData::SetData(const int idx, const void *source)
{
    SYS_CS_SYNC(m_CsToken);

    m_BitsUsed[idx] = 0;

    if(m_IsDefined && AssertVerify(GetFieldType(idx)==FIELD_VOID))
    {
        const int numBitsToWrite = GetFieldBitSize(idx);
        const int sourceBitOffset = 0;
        const int destBitOffset = m_BitOffsets[idx];

        //FIX -- should check that new bits are different so we don't needlessly set the field as dirty
        if (numBitsToWrite>0 && 
            AssertVerify(m_DataBits.PokeBits(source, numBitsToWrite, sourceBitOffset, destBitOffset)))
        {
            m_BitsUsed[idx] = numBitsToWrite;
            SetFieldDirty(idx);

            SetFieldValid(idx);
        }
    }
    return m_BitsUsed[idx];
}

int 
snuHostData::GetDataInt(const int idx) const
{
    SYS_CS_SYNC(m_CsToken);

    int data = 0;
    if(m_IsDefined && AssertVerify(m_IsReady) && AssertVerify(GetFieldType(idx)==FIELD_INT)
        && AssertVerify(IsFieldValid(idx)))
    {
        const int numBits = GetFieldBitSize(idx);

        AssertMsg(numBits <= IntTraits< int >::NumBits , "Stored data size is too big for an int");
        AssertMsg(numBits > 1 , "Use unsigned for boolean values");

        m_DataBits.PeekInt(data, numBits, m_BitOffsets[idx]);
    }
    return data;
}

int 
snuHostData::SetDataInt(const int idx, const int v)
{
    SYS_CS_SYNC(m_CsToken);

    m_BitsUsed[idx] = 0;
    
    if(!m_IsDefined && AssertVerify(GetFieldType(idx)==FIELD_INT))
        return 0;

    const int numBits = GetFieldBitSize(idx);

    AssertMsg(numBits <= IntTraits< int >::NumBits , "Stored data size is too big for an int");
    AssertMsg(numBits > 1 , "Use unsigned for boolean values");

    //make sure we are not assigning an int value greater than what the bits can hold
    const int maxValue = (1 << (numBits-1)) - 1;
    if(!AssertVerify(v <= maxValue))
        return 0;

    const int minValue = -maxValue - 1;
    if(!AssertVerify(v >= minValue))
        return 0;

    if (IsFieldValid(idx) && GetDataInt(idx)==v)
        return 0;

    if(m_DataBits.PokeInt(v, numBits, m_BitOffsets[idx]))
    {
        SetFieldDirty(idx);

        SetFieldValid(idx);

        m_BitsUsed[idx] = numBits;
    }

    return m_BitsUsed[idx];
}

unsigned 
snuHostData::GetDataUns(const int idx) const
{
    SYS_CS_SYNC(m_CsToken);

    int data = 0;
    if(m_IsDefined && AssertVerify(m_IsReady) && AssertVerify(GetFieldType(idx)==FIELD_UNS)
        && AssertVerify(IsFieldValid(idx)))
    {
        const int numBits = GetFieldBitSize(idx);

        AssertMsg(numBits <= IntTraits< unsigned >::NumBits , "Stored data size is too big for an int");

        m_DataBits.PeekUns(data, numBits, m_BitOffsets[idx]);
    }
    return data;
}

int	
snuHostData::SetDataUns(const int idx, const unsigned v)
{
    SYS_CS_SYNC(m_CsToken);

    m_BitsUsed[idx] = 0;

    if(!m_IsDefined && AssertVerify(GetFieldType(idx)==FIELD_UNS))
        return 0;

    const int numBits = GetFieldBitSize(idx);

    AssertMsg(numBits <= IntTraits< unsigned >::NumBits , "Stored data size is too big for an int");

    //make sure we are not assigning a value greater than what the bits can hold
    unsigned maxValue = numBits-1;
    maxValue = (1<<maxValue) - 1 + (1<<maxValue);
    if(!AssertVerify(v <= maxValue))
        return 0;

    if (IsFieldValid(idx) && GetDataUns(idx)==v)
        return 0;

    if(m_DataBits.PokeUns(v, numBits, m_BitOffsets[idx]))
    {
        SetFieldDirty(idx);

        SetFieldValid(idx);

        m_BitsUsed[idx] = numBits;
    }

    return m_BitsUsed[idx];
}

float 
snuHostData::GetDataFloat(const int idx) const
{
    SYS_CS_SYNC(m_CsToken);

    float v = 0.0f;
    if(m_IsDefined && AssertVerify(m_IsReady) && AssertVerify(GetFieldType(idx)==FIELD_FLOAT)
        && AssertVerify(IsFieldValid(idx)))
    {
        m_DataBits.PeekFloat(v, m_BitOffsets[idx]);
    }
    return v;
}

int 
snuHostData::SetDataFloat(const int idx, const float v)
{
    SYS_CS_SYNC(m_CsToken);

    m_BitsUsed[idx] = 0;
    
    if(!m_IsDefined && AssertVerify(GetFieldType(idx)==FIELD_FLOAT))
        return 0;

    const int numBits = GetFieldBitSize(idx);

    if (IsFieldValid(idx) && GetDataFloat(idx)==v)
        return 0;

    if(m_DataBits.PokeFloat(v, m_BitOffsets[idx]))
    {
        SetFieldDirty(idx);

        SetFieldValid(idx);

        m_BitsUsed[idx] = numBits;
    }

    return m_BitsUsed[idx];
}

void
snuHostData::DisplayField(const int idx) const
{
	char strFieldValue[128];

	if(!IsFieldValid(idx))
	{
		strcpy(strFieldValue, "<invalid>");
	}
	else
	{
		switch(GetFieldType(idx))
		{
		case FIELD_UNDEFINED:
			formatf(strFieldValue, sizeof(strFieldValue), "<undefined field>");
			break;

		case FIELD_INT:
			formatf(strFieldValue, sizeof(strFieldValue), "%d", GetDataInt(idx));
			break;

		case FIELD_UNS:
			formatf(strFieldValue, sizeof(strFieldValue), "%d", GetDataUns(idx));
			break;

		case FIELD_FLOAT:
			formatf(strFieldValue, sizeof(strFieldValue), "%f", GetDataFloat(idx));
			break;

		case FIELD_SER:
			formatf(strFieldValue, sizeof(strFieldValue), "<serialized object>");
			break;

		case FIELD_VOID:
			if (GetFieldBitSize(idx) == 64)
			{
				u64 data;
				GetData(idx, &data);
				formatf(strFieldValue, sizeof(strFieldValue), "64-bit: 0x%" I64FMT "x", data);
			}
			else
			{
				formatf(strFieldValue, sizeof(strFieldValue), "<void* field>");
			}
			break;
		}
	}

	snuDebug2("hostdata\t%s = %s%s", 
		GetFieldName(idx),
		strFieldValue,
		IsFieldDirty(idx) ? " <dirty>" : "");
}

void 
snuHostData::DisplayData(bool dirtyOnly) const
{
    if(m_IsDefined && m_IsReady)
    {
        for(int i=0; i!=GetFieldCount(); ++i)
        {
            if(dirtyOnly && !IsFieldDirty(i))
                continue;

			DisplayField(i);
		}
    }
    else
    {
            snuDebug2("hostdata can't be displayed, either undefined or unready... (defined=%d, ready=%d)", 
                m_IsDefined?1:0, m_IsReady?1:0);
    }
}

void 
snuHostData::SetFieldDirty(const int idx)
{
    SYS_CS_SYNC(m_CsToken);

    if(m_IsDefined)
    {
        Assert(idx >= 0 && idx<GetFieldCount());
        m_DirtyBits.PokeBool(true, idx);
    }
}

void 
snuHostData::SetFieldClean(const int idx)
{
    SYS_CS_SYNC(m_CsToken);

    if(m_IsDefined)
    {
        Assert(idx >= 0 && idx<GetFieldCount());
        m_DirtyBits.PokeBool(false, idx);
    }
}

bool 
snuHostData::IsFieldDirty(const int idx) const
{
    SYS_CS_SYNC(m_CsToken);

    bool dirty = false;
    if(m_IsDefined)
    {
        Assert(idx >= 0 && idx<GetFieldCount());
        m_DirtyBits.PeekBool(dirty, idx);
    }
    return dirty;
}

bool 
snuHostData::IsDirty() const
{
    bool dirty = false;
    if(m_IsDefined)
    {
        for(int i=0; i<GetFieldCount(); ++i)
        {
            if(IsFieldDirty(i))
            {
                dirty = true;
                break;
            }
        }
    }
    return dirty;
}

void 
snuHostData::ClearDirtyBits()
{
    if(m_IsDefined)
    {
        for(int i=0; i<GetFieldCount(); ++i)
        {
            SetFieldClean(i);
        }
    }
}

void 
snuHostData::ClearField(const int idx)
{
    SYS_CS_SYNC(m_CsToken);

    if(m_IsDefined && IsFieldValid(idx))
    {
        Assert(idx >= 0 && idx<GetFieldCount());
        m_ValidBits.PokeBool(false, idx);
        SetFieldDirty(idx);
        m_BitsUsed[idx] = 0;
    }
}

void 
snuHostData::ClearAllFields()
{
    if(m_IsDefined)
    {
        for(int i=0; i<GetFieldCount(); ++i)
        {
            ClearField(i);
        }
    }
}

void 
snuHostData::SetFieldValid(int idx)
{
    SYS_CS_SYNC(m_CsToken);

    if(m_IsDefined && !IsFieldValid(idx))
    {
        Assert(idx >= 0 && idx<GetFieldCount());
        m_ValidBits.PokeBool(true, idx);
        SetFieldDirty(idx);
    }
}

bool 
snuHostData::IsFieldValid(const int idx) const
{
    SYS_CS_SYNC(m_CsToken);

    bool bValid = false;
    if(m_IsDefined)
    {
        Assert(idx >= 0 && idx<GetFieldCount());
        m_ValidBits.PeekBool(bValid, idx);
    }
    return bValid;
}

void 
snuHostData::SendDirtyData(const snuGamer* destGamer/*=NULL*/)
{
    if(m_IsDefined && AssertVerify(m_IsReady))
    {
        if(IsDirty())
        {
            snuHostDataSyncMsg	    msg;
			int						channelId	= SNU_FIRST_SESSION_CHANNEL+m_SessionData.GetId();

            //update peers
            msg.InitForWrite(m_SessionData.GetId(), false);
            if(destGamer==NULL)
            {
                snuDebug2("Sending Dirty HostData...");
                SNUSOCKET.GetPostOffice()->SendToSession(m_SessionData, msg, NET_SEND_RELIABLE, channelId );
            }
            else
            {
                snuDebug2("Sending Dirty HostData to %s...", destGamer->GetName());
                SNUSOCKET.GetPostOffice()->SendToGamer(*destGamer, msg, NET_SEND_RELIABLE, channelId );
            }

            //update schema
            Assert(this->GetFieldCount() <= RL_MAX_MATCHING_ATTRS);

			const rlMatchingAttributes& oldAttrs = GetSessionData().GetSession()->GetConfig().m_Attrs;
            unsigned attrIds[RL_MAX_MATCHING_ATTRS];
			unsigned newVals[RL_MAX_MATCHING_ATTRS];
			unsigned hostDataIndices[RL_MAX_MATCHING_ATTRS];
            unsigned numAttrIds = 0;

            for(int i=0; i<GetFieldCount(); ++i)
            {
                const int fldId = this->GetSchemaFieldID(i);
                if(fldId < 0 || !IsFieldValid(i) || !IsFieldDirty(i))
                {
                    continue;
                }

				//skip fields that match the existing config
				u32 newVal;
				switch(GetFieldType(i))
				{
				case FIELD_INT: newVal = static_cast<u32>(GetDataInt(i)); break;
				case FIELD_UNS: newVal = GetDataUns(i); break;
				default: continue;
				}

				const u32* pOldVal = oldAttrs.GetValueById(fldId);
				if(pOldVal && *pOldVal==newVal)
				{
					continue;
				}

                attrIds[numAttrIds] = this->GetSchemaFieldID(i);
				newVals[numAttrIds] = newVal;
				hostDataIndices[numAttrIds] = i;
                ++numAttrIds;
            }

			if (numAttrIds > 0)
			{
				snuDebug2("HostData changed, updating matchmaking attributes...");

				rlMatchingAttributes newAttrs = GetSessionData().GetSession()->GetConfig().m_Attrs;
				newAttrs.Reset(attrIds, numAttrIds);
				newAttrs.SetGameMode( m_SessionData.GetSession()->GetConfig().m_Attrs.GetGameMode() );
				newAttrs.SetGameType( m_SessionData.GetSession()->GetConfig().m_Attrs.GetGameType() );

				for(unsigned i=0; i<numAttrIds; ++i)
				{
					newAttrs.SetValueById(attrIds[i], newVals[i]);

					DisplayField(hostDataIndices[i]);
				}
				AssertVerify(GetSessionData().GetSession()->ChangeAttributes(newAttrs, NULL));
			}
        }
    }
}

void 
snuHostData::CopyFieldToSchema(rlSchema& rSchema, int idx) const
{
    int	fieldId	= GetSchemaFieldID(idx);

    if(IsFieldValid(idx) && fieldId!=-1)
    {
        int 		schemaFieldIdx	= rSchema.GetFieldIndex(fieldId);
        int 		dataSize		= GetFieldBitSize(idx);
        const int	alignment		= 8*sizeof(int);
        int			iValue;
        unsigned	uValue;

        if (AssertVerify(dataSize))
        {
            //align
            dataSize = (dataSize + (alignment - 1)) & ~(alignment - 1);
            dataSize/= 8;

            if(AssertVerify(rSchema.GetSizeofFieldData(schemaFieldIdx)==dataSize))
            {
                switch(GetFieldType(idx))
                {
                case FIELD_INT:	iValue = GetDataInt(idx); rSchema.SetFieldData(schemaFieldIdx, &iValue, sizeof(iValue)); break;
                case FIELD_UNS:	uValue = GetDataUns(idx); rSchema.SetFieldData(schemaFieldIdx, &uValue, sizeof(uValue)); break;
                default:		AssertMsg(false , "unsupported matchmaking field type");
                }
            }
        }
    }	
}

void 
snuHostData::SendAllData(const snuGamer* destGamer/*=NULL*/)
{
    if(m_IsDefined && AssertVerify(m_IsReady))
    {
        snuHostDataSyncMsg msg;
        msg.InitForWrite(m_SessionData.GetId(), true);
		int channelId = SNU_FIRST_SESSION_CHANNEL+m_SessionData.GetId();

        if(destGamer==NULL)
        {
            snuDebug2("Sending All HostData...");
            SNUSOCKET.GetPostOffice()->SendToSession(m_SessionData, msg, NET_SEND_RELIABLE, channelId);
        }
        else
        {
            snuDebug2("Sending All HostData to %s...", destGamer->GetName());
            SNUSOCKET.GetPostOffice()->SendToGamer(*destGamer, msg, NET_SEND_RELIABLE, channelId);
        }
    }	
}
