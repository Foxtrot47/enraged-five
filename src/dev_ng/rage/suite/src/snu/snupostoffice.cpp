// 
// mcNet\snu\snupostoffice.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#include "snupostoffice.h"

#include "snudiag.h"
#include "snusocket.h"
#include "snutasks.h"

#include "math/amath.h"
#include "system/simpleallocator.h"
#include "net/net.h"

#include "diag/seh.h"

using namespace rage;

NET_MESSAGE_IMPL( snuPostOffice::snuConnectionQosMessage );
NET_MESSAGE_IMPL( snuPostOffice::snuConnectionQosMessageAck );


snuPostOffice::snuPostOffice() 	
: m_pAllocator	(NULL)
, m_pCxnMgr		(NULL)
, m_pTransactor	(NULL)
{
	m_pTransactor = rage_new netTransactor;
}
snuPostOffice::~snuPostOffice()
{
	if(AssertVerify(m_pTransactor))
		delete m_pTransactor;
}



bool snuPostOffice::Init(rage::netSocket* pSocket)
{
	m_pAllocator = rage_new sysMemSimpleAllocator(GetHeap(), GetSizeofHeap(), sysMemSimpleAllocator::HEAP_SNU);
	Assert(m_pAllocator);
	m_pAllocator->SetQuitOnFail(false);

	Assert(!m_pCxnMgr);
	m_pCxnMgr = rage_new netConnectionManager;
	m_pCxnMgr->Init( m_pAllocator, MAX_CXNS, pSocket, SNUSOCKET.GetCpuAffinity() );

	for (int channel=0; channel < m_pChannelEventDelegates.GetMaxCount(); ++channel)
	{
		//FIX: It's a little wasteful to have unused channel delegates. Could give the app control
		//over which channels may be used and need delegates (in addition to the SNU channel).
		m_pChannelEventDelegates[channel] = rage_new netConnectionManager::Delegate;
		m_pChannelEventDelegates[channel]->Bind( this, &snuPostOffice::OnCxnEvent );
		m_pCxnMgr->AddDelegate(m_pChannelEventDelegates[channel], channel);
	}

	m_pTransactor->Init(GetConnectionManager());
	m_TransactionrRequest.Bind(this, &snuPostOffice::OnTransactionRequest);

	m_pTransactor->AddRequestHandler(&m_TransactionrRequest, SNU_FIRST_SESSION_CHANNEL);

	m_GroupJoinPromise.Reset();

	return true;
}

void snuPostOffice::Shutdown()	
{
	Assert(m_pCxnMgr);

	m_pTransactor->Shutdown();

	//Shut down the connection manager.
	if (m_pCxnMgr)
	{
		for (int channel=0; channel < m_pChannelEventDelegates.GetMaxCount(); ++channel)
		{
			m_pCxnMgr->RemoveDelegate(m_pChannelEventDelegates[channel]);
		}

		m_pCxnMgr->Shutdown();
		delete m_pCxnMgr;
		m_pCxnMgr = 0;
	}

	delete m_pAllocator;
	m_pAllocator = 0;
}

void snuPostOffice::Update()
{
	static u32	m_LastMsTime= 0;
	const unsigned	curTime	= sysTimer::GetSystemMsTime();
	u32				deltaMs	= Clamp(curTime - m_LastMsTime, 1u, 1000u);

	m_pCxnMgr->Update( curTime );
	m_pTransactor->Update(deltaMs);
	m_GroupJoinPromise.Update(deltaMs);

	m_LastMsTime = curTime;
}

rage::netSocket* snuPostOffice::GetSocket()	
{ 
	Assert(GetConnectionManager());
	return GetConnectionManager()->GetSocket(); 
}

unsigned short snuPostOffice::GetPort()
{
	Assert(GetSocket());
	return GetSocket()->GetAddress().GetPort();
}

size_t snuPostOffice::GetHeapBytesLeft()
{
	return m_pAllocator->GetMemoryAvailable();
}

size_t snuPostOffice::GetHeapLowWaterMark()
{
	return m_pAllocator->GetLowWaterMark(false);
}

//PURPOSE
//  Add gamer to the post office.
void snuPostOffice::AddConnection(snuGamer& gamer)
{
	snuConnection* pCxn = GetGamerConnection( gamer );
	if (!pCxn)
	{
		pCxn = GetAvailableCxn();

		if (pCxn)
		{
			pCxn->Reset();
			pCxn->m_Available = false;
			pCxn->m_Gamer = &gamer;
		}
	}
	
	if (pCxn)
		OnAddConnection(pCxn);
	else
		snuError( "AddConnection(): Failed! No connections available for %s", gamer.GetName() );
}


//PURPOSE
//  Remove gamer from the post office.
void snuPostOffice::FreeConnection(snuGamer& gamer)
{
	snuConnection* pCxn = GetGamerConnection( gamer );

	if (pCxn)
	{
		OnFreeConnection(pCxn);
		pCxn->Reset();
	}
}

snuPostOffice::snuConnection* snuPostOffice::OpenConnection(snuGamer& gamer, int channelId, int sessionId)
{
	snuConnection* cxn = GetGamerConnection( gamer );

	if( cxn )
	{
		snuChannel& rChannel = cxn->m_Channels[channelId];
		
		if( !rChannel.IsAssigned() )
		{
			const netAddress& addr = gamer.GetNetAddress(sessionId);

			rChannel.m_CxnId			= m_pCxnMgr->OpenConnection( addr, channelId, NULL, 0, NULL );
			rChannel.m_CxnState			= CXNSTATE_REQUESTED;
			rChannel.m_TimeDisconnected = 0;
			rChannel.m_SessionId		= sessionId;

			snuDebug1( "OpenConnection(): %08x.%d[%d.%d.%d.%d:%d] to %s",
				rChannel.m_CxnId, channelId, NET_ADDR_FOR_PRINTF(addr), gamer.GetName() );

			return cxn;
		}
		
	}

	snuError( "OpenConnection(): Failed! No connection available for %s?", gamer.GetName() );

	return NULL;
}

void snuPostOffice::CloseConnection( const snuGamer& gamer, int channelId )
{
	snuConnection* cxn = GetGamerConnection( gamer );

	if (cxn)
	{
		snuChannel& rChannel = cxn->m_Channels[channelId];

		bool validCxnState = cxn && rChannel.IsAssigned() && AssertVerify( rChannel.m_CxnId >= 0 );

		if( validCxnState )
		{
			snuDebug1( "CloseConnection(): %08x.%d[%d.%d.%d.%d:%d] to %s",
				rChannel.m_CxnId, channelId, NET_ADDR_FOR_PRINTF(gamer.GetNetAddress(rChannel.m_SessionId)), gamer.GetName() );

			rChannel.m_CxnState = CXNSTATE_CLOSED;
			m_pCxnMgr->CloseConnection( rChannel.m_CxnId, NET_CLOSE_GRACEFULLY );
		}
		else
		{
			snuDebug1( "CloseConnection(): Cxn to %s on channel %d is not in a closable state", gamer.GetName(), channelId );
		}
	}
	else
	{
		snuDebug1( "CloseConnection(): Cxn to %s on channel %d doesn't exist", gamer.GetName(), channelId );
	}
}


void snuPostOffice::CloseSessionChannels( const snuGamer& gamer, int sessionId )
{
	snuConnection* cxn = GetGamerConnection( gamer );

	if (!cxn)
		return;

	for (int channelId=0; channelId < cxn->m_Channels.GetMaxCount(); ++channelId)
	{
		snuChannel& rChannel		= cxn->m_Channels[channelId];
		bool		validCxnState	= rChannel.IsAssigned() && AssertVerify( rChannel.m_CxnId >= 0 );

		if( validCxnState && rChannel.m_SessionId==sessionId )
			CloseConnection( gamer, channelId );
	}
}


void snuPostOffice::CloseAllChannels( const snuGamer& gamer )
{
	snuConnection* cxn = GetGamerConnection( gamer );
	int channelId=0;

	if (!cxn)
		return;

	for (channelId=0; channelId < cxn->m_Channels.GetMaxCount(); ++channelId)
	{
		snuChannel& rChannel		= cxn->m_Channels[channelId];
		bool		validCxnState	= rChannel.IsAssigned() && AssertVerify( rChannel.m_CxnId >= 0 );

		if( validCxnState )
			CloseConnection( gamer, channelId );
	}
	
	for (channelId=0; channelId < cxn->m_Channels.GetMaxCount(); ++channelId)
	{
		//it's possible that rage has a connection on this channel (e.g., snet)
		//make sure that these channels are closed as well
		//(a potentially better way to achieve this: record connections that are opened by rage. we already have a
		//delegate on every channel, so this should be possible)

		for (int sessionId=0; sessionId!=SNU_MAX_SESSIONS; sessionId++)
		{
			int cxnId = GetConnectionManager()->GetConnectionId(gamer.GetNetAddress(sessionId), channelId);
			if (cxnId != -1)
			{
				snuDebug1( "CloseAllChannels(): Closing %08x.%d[%d.%d.%d.%d:%d] to %s...",
					cxnId, channelId, NET_ADDR_FOR_PRINTF(gamer.GetNetAddress(sessionId)), gamer.GetName() );

				GetConnectionManager()->CloseConnection(cxnId, NET_CLOSE_GRACEFULLY);
			}
		}			
	}
}

//PURPOSE
//  This function is bound to the delegate (callback) that we register
//  with the connection manager.  The connection manager will pass all
//  connection events to this function.
void snuPostOffice::OnCxnEvent( netConnectionManager* pCxnMgr, const netEvent* evt )
{
	snuConnection* pCxn = NULL;

	switch(evt->GetId())
	{
	case NET_EVENT_CONNECTION_ESTABLISHED:
		{
			pCxn = GetConnection(evt->m_CxnId, evt->m_ChannelId);
#if !__NO_OUTPUT
			const netAddress& addr = pCxnMgr->GetAddress(evt->m_CxnId);
			const snuGamer* pGamer = pCxn ? pCxn->m_Gamer : NULL;
			snuDebug1( "cxn event: established, %08x.%d[%d.%d.%d.%d:%d] to %s", 
				evt->m_CxnId, evt->m_ChannelId, NET_ADDR_FOR_PRINTF(addr), pGamer ? pGamer->GetName() : "???" );
#endif
			if( pCxn )
			{
				pCxn->m_Channels[evt->m_ChannelId].m_TimeDisconnected = 0;
				pCxn->OnOpened(evt->m_ChannelId);
			}
		}
		break;

	case NET_EVENT_CONNECTION_CLOSED:
		{
			pCxn = GetConnection(evt->m_CxnId, evt->m_ChannelId);
#if !__NO_OUTPUT
			snuDebug1( "cxn event: closed, %08x.%d", 
				evt->m_CxnId, evt->m_ChannelId );
#endif
			if( pCxn )
			{
				if (!pCxn->IsDisconnected(evt->m_ChannelId))
				{
					pCxn->m_Channels[evt->m_ChannelId].m_TimeDisconnected = sysTimer::GetSystemMsTime();

					snuDebug1( "cxn %08x.%d is disconnected (TimeDisconnected=%d)", 
						evt->m_CxnId, evt->m_ChannelId, pCxn->m_Channels[evt->m_ChannelId].m_TimeDisconnected );
				}
				pCxn->OnClosed(evt->m_ChannelId);
			}
		}
		break;

	case NET_EVENT_CONNECTION_ERROR:
		{
			pCxn = GetConnection(evt->m_CxnId, evt->m_ChannelId);
#if !__NO_OUTPUT
			const netAddress& addr = pCxnMgr->GetAddress(evt->m_CxnId);
			snuDebug1( "cxn event: error code %d, %08x.%d[%d.%d.%d.%d:%d]",
				evt->m_CxnError->m_ErrorCode, evt->m_CxnId, evt->m_ChannelId, NET_ADDR_FOR_PRINTF(addr) );
#endif
			if( pCxn )
			{
				if (!pCxn->IsDisconnected(evt->m_ChannelId))
				{
					pCxn->m_Channels[evt->m_ChannelId].m_TimeDisconnected = sysTimer::GetSystemMsTime();

					snuDebug1( "cxn %08x.%d is disconnected (TimeDisconnected=%d)", 
						evt->m_CxnId, evt->m_ChannelId, pCxn->m_Channels[evt->m_ChannelId].m_TimeDisconnected );
				}

				pCxn->m_Channels[evt->m_ChannelId].m_CxnState = CXNSTATE_TIMEOUT;
			}

			if (!pCxnMgr->IsClosed(evt->m_CxnId))
				pCxnMgr->CloseConnection( evt->m_CxnId, NET_CLOSE_IMMEDIATELY );
		}
		break;

	case NET_EVENT_FRAME_RECEIVED:
		//snuDebug4( "frame received!" );

		//Recieved a frame of data.
		//A frame is the basic unit of data sent and recieved via
		//channels.  Typically we would know what kind of data it
		//is by looking at the channel id.  IOW, we segregate
		//different types of data onto different channels.
		{
			netEventFrameReceived*	fr		= evt->m_FrameReceived;
			unsigned				msgId	= 0;

			//if GetId fails, it probably means we received a non-netMessage packet
			//which we aren't expecting, so ignore it
			if( netMessage::GetId( &msgId, fr->m_Payload, fr->m_SizeofPayload ) )
				HandleMessage(msgId, fr);
		}

		break;

	case NET_EVENT_ACK_RECEIVED:
		//We received an ACK for one of the frames we sent.
		//If we had previously recorded the sequence number generated
		//when we sent a frame (by passing a pointer to a netSequence
		//instance to netConnectionManager::Send()) we could compare it
		//to the sequence number here and determine if our frame was
		//received by the remote peer.
		//
		//Note that an ACK for an unreliable frame means only that that
		//particular frame arrived at the remote peer.  Other frames sent
		//may or may not have arrived.
		//
		//Whereas every reliable frame is guaranteed to generate an
		//ACK the same cannot be said for unreliable frames.
		break;
	}
}

bool snuPostOffice::HandleMessage(unsigned msgId, rage::netEventFrameReceived* fr)
{
	if( snuConnectionQosMessage::MSG_ID() == msgId )
	{
		snuConnectionQosMessage msg;
		if( msg.Import( fr->m_Payload, fr->m_SizeofPayload ) )
		{
			snuConnectionQosMessageAck ack(msg.GetOriginTime());
			SNUSOCKET.GetPostOffice()->SendToCxn(fr->m_CxnId, ack, NET_SEND_RELIABLE);
		}
		return true;
	}

	if( snuConnectionQosMessageAck::MSG_ID() == msgId )
	{
		snuConnectionQosMessageAck msg;
		if(msg.Import( fr->m_Payload, fr->m_SizeofPayload))
		{
			unsigned		currentTime = sysTimer::GetSystemMsTime();
			snuConnection*	pCxn		= GetConnection(fr->m_CxnId, fr->m_ChannelId);

			if (pCxn)
			{
				Assert(pCxn->m_TimePingSent<currentTime);//sanity
				pCxn->m_Qos = currentTime-pCxn->m_TimePingSent;
				pCxn->OnQosRecived();
			}
		}
		return true;
	}

	return false;
}

char snuPostOffice::GetJoinRequestResponse(const snuSessionData*, const rage::rlGamerInfo& rGamerInfo) const
{
	//Auto-reject gamers on the banned list.
	if (SNUSOCKET.GetBannedGamers().FindGamerIndex(rGamerInfo) >= 0)
	{
		snuDebug( "Join Request: Rejected banned gamer '%s'", rGamerInfo.GetName() );

		return 'B';  //for Banned
	}

	return 'Y';
}

snuPostOffice::snuConnection::snuConnection()
: m_Gamer(NULL)
, m_Qos(0)
, m_TimePingSent(0)
, m_Available(true)
{
	for(int i=0; i < m_Channels.GetMaxCount(); ++i)
	{
		m_Channels[i].m_CxnId			= -1;
		m_Channels[i].m_CxnState		= CXNSTATE_INVALID;
	}
}

int	snuPostOffice::snuConnection::GetAnyValidSnuChannel() const
{
	for(int i=0; i < m_Channels.GetMaxCount(); ++i)
	{
		if (m_Channels[i].IsAssigned() )
			return i;
	}

	return -1;
}

bool snuPostOffice::snuConnection::IsOpen(int id) const
{ 
	return m_Channels[id].m_CxnState == CXNSTATE_OPEN && 
		AssertVerify(m_Channels[id].m_CxnId>=0);
}

void snuPostOffice::snuConnection::RequestQos()
{
	unsigned				currentTime	= sysTimer::GetSystemMsTime();
	snuConnectionQosMessage msg			(currentTime);
	int						channelId	= GetAnyValidSnuChannel();

	if(AssertVerify(channelId>=0))
	{
		SNUSOCKET.GetPostOffice()->SendToCxn(m_Channels[channelId].m_CxnId, msg, NET_SEND_RELIABLE);
		m_TimePingSent = currentTime;
	}
	
}

unsigned 
snuPostOffice::snuConnection::GetElapsedDisconnectMs(int channelId) const
{
	if (IsDisconnected(channelId))
	{
		return sysTimer::GetSystemMsTime() - m_Channels[channelId].m_TimeDisconnected;
	}

	return 0;
}

const snuPostOffice::snuConnection* snuPostOffice::GetGamerConnection(const snuGamer &rGamer) const
{
	for( int pid = 0; pid < GetConnectionCount(); pid++ )
	{
		const snuConnection *pConnection = GetPlayerConnection(pid);
		if (pConnection && (pConnection->m_Gamer == &rGamer))
		{
			return pConnection;
		}
	}
	return 0;
}

snuPostOffice::snuConnection* snuPostOffice::GetGamerConnection(const snuGamer& rGamer)
{
	for( int pid = 0; pid < GetConnectionCount(); pid++ )
	{
		snuConnection *pConnection = GetPlayerConnection(pid);
		if (pConnection && (pConnection->m_Gamer == &rGamer))
		{
			return pConnection;
		}
	}
	return 0;
}

const snuPostOffice::snuConnection* snuPostOffice::GetPeerConnection(rage::u64 peerId) const
{
	for( int pid = 0; pid < GetConnectionCount(); pid++ )
	{
		const snuConnection *pConnection = GetPlayerConnection(pid);
		if (	pConnection 
			&&	pConnection->m_Gamer 
			&&	pConnection->m_Gamer->GetPeerInfo().GetPeerId() == peerId
			)
		{
			return pConnection;
		}
	}
	return 0;
}

snuPostOffice::snuConnection* snuPostOffice::GetPeerConnection	(rage::u64 peerId)
{
	for( int pid = 0; pid < GetConnectionCount(); pid++ )
	{
		snuConnection *pConnection = GetPlayerConnection(pid);
		if (	pConnection 
			&&	pConnection->m_Gamer 
			&&	pConnection->m_Gamer->GetPeerInfo().GetPeerId() == peerId
			)
		{
			return pConnection;
		}
	}
	return 0;
}

snuPostOffice::snuConnection* snuPostOffice::GetConnection		(int cxnId, int channelId)
{
	// find this connection
	for( int pid = 0; pid < GetConnectionCount(); pid++ )
	{
		snuConnection *pConnection = GetPlayerConnection(pid);
		if( pConnection && pConnection->m_Channels[channelId].m_CxnId == cxnId )
			return pConnection;
	}

	return 0;
}

snuPostOffice::snuConnection* snuPostOffice::GetAvailableCxn		()
{
	//Index 1 is reserved for the local player
	for( int pid = 1; pid < GetConnectionCount(); pid++ )
	{
		snuConnection *pConnection = GetPlayerConnection(pid);
		if ( pConnection && pConnection->m_Available )
		{
			return pConnection;
		}
	}

	//no cxns available!
	return 0;
}





//================================ TRANSACTION ====================================================

void snuPostOffice::OnTransactionRequest(netTransactor* transactor,
							  netRequestHandler* /*handler*/,
							  const netRequest* rqst)
{
	rtry
	{
		unsigned msgId;
		rcheck(netMessage::GetId(&msgId,
			rqst->m_Data,
			rqst->m_SizeofData), catchall,);

		if(snuGroupJoinRequestTask::snuMsgGroupJoinRequest::MSG_ID() == msgId)
		{
			snuGroupJoinRequestTask::snuMsgGroupJoinRequest msgRequest;
			if( AssertVerify(msgRequest.Import(rqst->m_Data, rqst->m_SizeofData)) ) 
			{
				snuDebug2("Received %s from:%d.%d.%d.%d:%d", msgRequest.GetMsgName(), NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));

				const snuSessionData* pSessionData = SNUSOCKET.GetSessionByInfo(msgRequest.m_SessionInfo);

				if( AssertVerify(pSessionData) )
				{
					snuGroupJoinRequestTask::snuMsgGroupJoinResponse msgResponse;
					int	nSlots = pSessionData->GetSession()->GetEmptySlots(RL_SLOT_PUBLIC);

					if (m_GroupJoinPromise.IsValid())
					{
						snuDebug2("Refusing group join request because of current promise");
						nSlots = 0;
					}
					else if (nSlots >= msgRequest.m_NumJoiners)
					{
						snuDebug2("Accepting group join request");
						m_GroupJoinPromise.Reset(10000);
						nSlots = msgRequest.m_NumJoiners;
					}
					
					msgResponse.Reset(pSessionData->GetSession()->GetSessionInfo(), pSessionData->GetSession()->GetConfig(), nSlots);
					transactor->SendResponse(rqst->m_TxInfo, msgResponse);
				}
			}
		}

#if !__NO_OUTPUT
		else
		{
			const char* msgName = netMessage::GetName(rqst->m_Data, rqst->m_SizeofData);
			if(msgName)
				snuDebug2("Ignoring %s from:%d.%d.%d.%d:%d", msgName, NET_ADDR_FOR_PRINTF(rqst->m_TxInfo.m_Addr));
		}
#endif  //!__NO_OUTPUT
	}
	rcatchall
	{
	}
}


//================================ FRIENDS ========================================================
snuFriendInvites::snuFriendInvites()
: m_Dirty(true)
{
	for(unsigned i = 0; i < MAX_INVITES; i++)
	{
		snuInvite *inv = &m_Pile[i];
		m_FreeList.push_back(inv);
	}
}

const snuInvite* snuFriendInvites::GetInvite(const rlSessionInfo& info) const
{
	if(!m_ActiveList.empty())
	{
		ListType::const_iterator it = m_ActiveList.begin();
		while(it != m_ActiveList.end())
		{
			const snuInvite* tmp = *it;

			if(tmp->m_SessionInfo == info)
				return tmp;

			it++;
		}
	}

	return false;
}

const snuInvite* snuFriendInvites::GetInvite(int idx) const
{
	if(!m_ActiveList.empty())
	{
		ListType::const_iterator it = m_ActiveList.begin();
		while(it != m_ActiveList.end())
		{
			if(idx==0)
				return *it;

			it++;
			idx--;
		}
	}

	return 0;
}

const snuInvite* snuFriendInvites::GetInvite(const rlGamerHandle& handle) const
{
	if(!m_ActiveList.empty())
	{
		ListType::const_iterator it = m_ActiveList.begin();
		while(it != m_ActiveList.end())
		{
			const snuInvite* tmp = *it;

			if(tmp->m_Inviter == handle)
				return tmp;

			it++;
		}
	}

	return 0;
}

void snuFriendInvites::AddInvite(const rlPresenceEventInviteReceived* pInvite)
{
	//ignore spam
	if(!m_ActiveList.empty())
	{
		ListType::const_iterator it = m_ActiveList.begin();
		while(it != m_ActiveList.end())
		{
			const snuInvite* tmp = *it;
			
			if (tmp->m_Inviter==pInvite->m_Inviter && tmp->m_SessionInfo==pInvite->m_SessionInfo)
			{
				snuDebug("Ignoring invite spam from %s", pInvite->m_InviterName);
				return;
			}

			it++;
		}
	}

	//remove any old invite from this inviter
	RemoveInvite(pInvite->m_Inviter);

	if(!m_FreeList.empty())
	{
		snuInvite* tmp = *m_FreeList.begin();

		m_FreeList.pop_front();       

		safecpy(tmp->m_InviterName, pInvite->m_InviterName, sizeof(tmp->m_InviterName));
		tmp->m_SessionInfo	= pInvite->m_SessionInfo;
		tmp->m_Inviter		= pInvite->m_Inviter;

		m_ActiveList.push_back(tmp);
		m_Dirty = true;

		SNUSOCKET.Notify(snuSocket::snuNotifyGamerInviteReceived, tmp);
	}
}

void snuFriendInvites::RemoveInvites(const rlSessionInfo& info)
{
	if(!m_ActiveList.empty())
	{
		ListType::iterator it = m_ActiveList.begin();
		while(it != m_ActiveList.end())
		{
			snuInvite* tmp = *it;

			if(tmp->m_SessionInfo == info)
			{
				m_ActiveList.erase(tmp);
				m_FreeList.push_back(tmp);
				m_Dirty = true;
			}

			it++;
		}
	}
}

void snuFriendInvites::RemoveInvite(const rlGamerHandle& rGamer)
{
	if(!m_ActiveList.empty())
	{
		ListType::iterator it = m_ActiveList.begin();
		while(it != m_ActiveList.end())
		{
			snuInvite* tmp = *it;

			if(tmp->m_Inviter == rGamer)
			{
				m_ActiveList.erase(tmp);
				m_FreeList.push_back(tmp);
				m_Dirty = true;
				return;
			}

			it++;
		}
	}
}

void snuFriendInvites::Clear()
{
	while(!m_ActiveList.empty())
	{
		RemoveInvite((*(m_ActiveList.begin()))->m_Inviter);
	}

	m_Dirty = true;
}

