// 
// snu\snusocket.cpp
// 
// Copyright(C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "snudiag.h"
#include "snugamer.h"
#include "snuhostdata.h"
#include "snusearchbot.h"
#include "snusessiondata.h"
#include "snupostoffice.h"
#include "snuoptions.h"
#include "snusocket.h"

#include "rline/rlnp.h"
#include "system/timemgr.h"
#include "system/param.h"
#include "net/net.h"

PARAM(snuCpu, "Request the network systems that snu uses to run on a specific cpu");

namespace rage
{
    XPARAM(noaudio);
}

using namespace rage;

//HACK: this is needed because some tasks require a hanging session.
snSession gWorkerSession;

snuSocket::snuSocket()
: m_Hardware(rage_new netHardware)
, m_Socket(rage_new netSocket)
, m_pSearchBot(rage_new snuSearchBot)
, m_pRemoteGamers(rage_new snuGamerPool)
, m_pUserOptions(rage_new snuUserOptions)
, m_aLocalGamer(rage_new snuGamerLocal[RL_MAX_LOCAL_GAMERS])
, m_aSessionData(rage_new snuSessionData[SNU_MAX_SESSIONS])
, m_CpuAffinity(SNU_DEFAULT_CPU_AFFINITY)
, m_Status(snuSocketUnInitialised)
, m_Voice(0)
, m_Allocator(m_Heap, sizeof(m_Heap), sysMemSimpleAllocator::HEAP_SNU)
, m_pPostOffice(0)
, m_StartingMatchmakingService(false)
{
    m_RecentGamers.Clear();
    m_PresenceDlgt.Bind(this, &snuSocket::OnPresenceEvent);
    m_SysJoinWish.Reset();

	for(int i=0;i!=NELEM(m_aCxnDelegate); i++)
		m_aCxnDelegate[i].Bind(this, &snuSocket::OnCxnEvent);

    PARAM_snuCpu.Get(m_CpuAffinity);
    snuDebug("cpuAffinity = %d", m_CpuAffinity);
}

snuSocket::~snuSocket()
{
    Shutdown();

    delete[]    m_aLocalGamer;
    delete[]    m_aSessionData;
    delete        m_pSearchBot;
    delete        m_pRemoteGamers;
    delete        m_pUserOptions;
    delete        m_Socket;
    delete        m_Hardware;
}

static void 
HandleGetGamerData(snSession*, const rlGamerInfo&, snGetGamerData*)
{
}

static bool 
HandleJoinRequest(snSession*, const rlGamerInfo&, snJoinRequest*)
{
    return false;
}

bool 
snuSocket::Init(rlTitleId* pTitleID,	
                u32 nMinAge,	
                unsigned short port,
                snuComponentFactory& factory,
                snuUserOption aUserOption[],        
                const snuGamerDataDef aGamerDataDef[],    
                const int gamerDataCount)
{
    int i;

    m_Allocator.BeginLayer();

    //initialize socket
#if __XENON
	AssertVerify(m_Hardware->CreateSocket(m_Socket, port, NET_PROTO_VDP, NET_SOCKET_NONBLOCKING));
#elif __PS3
	AssertVerify(m_Hardware->CreateSocket(m_Socket, port, NET_PROTO_UDPP2P, NET_SOCKET_NONBLOCKING));
#else
    AssertVerify(m_Hardware->CreateSocket(m_Socket, port, NET_PROTO_UDP, NET_SOCKET_NONBLOCKING));
#endif

    //Because we're using net messages we have to initialize the
    //net message system.
    netMessage::InitClass();

	//Post office
	m_pPostOffice = factory.CreatePostOffice();
    m_pPostOffice->Init(m_Socket);

	AssertVerify( m_pUserOptions->Init(aUserOption) );
	GetRemoteGamers()->Init(snuSessionData::maxGamerSlots);

    AssertMsg(m_Status==snuSocketUnInitialised , "Call Shutdown() before calling Init()");
    rlInit(&m_Allocator, GetPostOffice()->GetConnectionManager()->GetSocket(), pTitleID, nMinAge);

	snuGamer::snuGamerData::gInitialise(aGamerDataDef, gamerDataCount);

	InitVoiceChat();

	for(i=0;i!=NELEM(m_aCxnDelegate); i++)
		GetPostOffice()->GetConnectionManager()->AddDelegate(m_aCxnDelegate+i, SNU_FIRST_SESSION_CHANNEL+i);

	GetSearchBot()->Init(GetPostOffice()->GetConnectionManager());

    m_PresenceMgr.AddDelegate(&m_PresenceDlgt);
    AssertVerify(m_PresenceMgr.Init());
    
    m_SysUi.Init();

    //sessions
    snSessionOwner owner;
    owner.GetGamerData.Bind(&HandleGetGamerData);
    owner.HandleJoinRequest.Bind(&HandleJoinRequest);
    gWorkerSession.Init(&m_Allocator, owner, GetPostOffice()->GetConnectionManager(), SNET_CHANNEL_ID, SNUSOCKET.GetPostOffice()->GetConnectionManager()->GetSocket()->GetPort() + 1);
    for(i=0; i<SNU_MAX_SESSIONS; i++)
    {
        m_aSessionData[i].Reset();
        m_aSessionData[i].Init(i);
    }

    //local gamers
    for(i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
    {
        const rlGamerInfo* gamerInfo = m_PresenceMgr.GetGamerInfo(i);
        if(gamerInfo && gamerInfo->IsValid())
        {
            if(!m_aLocalGamer[i].Init(*gamerInfo))
            {
                AssertMsg(0, "Could not Initialise local gamer");
                return false;
            }
        }
    }

    m_Status = snuSocketInitialised;
    return true;
}

void snuSocket::InitVoiceChat()
{
	if (!PARAM_noaudio.Get() && m_Voice == NULL)
	{
		//Set the encryption off for the voice channel
		netChannelPolicies policies;
		GetPostOffice()->GetConnectionManager()->GetChannelPolicies(VOICE_CHANNEL_ID, &policies);
		if (policies.m_EncryptionEnabled)
		{
			policies.m_EncryptionEnabled = false;
			GetPostOffice()->GetConnectionManager()->SetChannelPolicies(VOICE_CHANNEL_ID, policies);
		}

		const int maxLocalTalkers = 1;
		const int maxRemoteTalkers = snuSessionData::maxGamerSlots - maxLocalTalkers;
		m_Voice = rage_new VoiceChat;
		AssertVerify( m_Voice->Init(maxLocalTalkers, maxRemoteTalkers, GetPostOffice()->GetConnectionManager(), VOICE_CHANNEL_ID, m_CpuAffinity) );
	}
}

void snuSocket::Shutdown()
{
	if( m_Status==snuSocketUnInitialised )
		return;

    for(int i=0; i!=SNU_MAX_SESSIONS; i++)
    {
        m_aSessionData[i].UnInit();
        while(!m_aSessionData[i].IsFree())
        {
            Update();
        }
    }

    for(int i = 0; i < RL_MAX_LOCAL_GAMERS; i++)
    {
        m_aLocalGamer[ i ].UnInit();
    }

    m_SysUi.Shutdown();

    m_PresenceMgr.RemoveDelegate(&m_PresenceDlgt);
    m_PresenceMgr.Shutdown();

	GetSearchBot()->Shutdown();

	ShutdownVoiceChat();

    rlShutdown();

	m_pPostOffice->Shutdown();
	delete m_pPostOffice;
	m_pPostOffice = 0;

	m_Hardware->DestroySocket( m_Socket );

    netMessage::ShutdownClass();

    m_Allocator.EndLayer("snuSocket", 0);

    m_Status = snuSocketUnInitialised;
}

void snuSocket::ShutdownVoiceChat()
{
	if (m_Voice)
	{
		m_Voice->Shutdown();
		delete m_Voice;
	}
	m_Voice = 0;
}

void snuSocket::StartMatchmakingService()
{
#if __PS3
	if (!m_StartingMatchmakingService)
	{
		g_rlNp.GetMatching().Start();
		m_StartingMatchmakingService = true;
	}
#else
	Notify(snuNotifyMatchmakingServiceStartSucceeded);
#endif
}

void snuSocket::StopMatchmakingService()
{
#if __PS3
	if (g_rlNp.GetMatching().Started() || g_rlNp.GetMatching().Starting())
	{
		m_StartingMatchmakingService = false;
		g_rlNp.GetMatching().Stop();
	}
#endif
}

void snuSocket::Update()
{
    int i;
    const unsigned curTime = sysTimer::GetSystemMsTime();

    m_Hardware->Update();
    rlUpdate();
    m_PresenceMgr.Update();
    m_SysUi.Update();
    m_pPostOffice->Update();

    if(m_SysJoinWish.IsValid())
    {
        if(m_SysJoinWish.IsReady())
        {
            if(m_SysJoinWish.m_Status.Succeeded())
            {
                Notify(snuNotifyGamerJoinWish, &m_SysJoinWish);
            }
            else
            {
                snuError("Could not get config from system's join wish");
                Notify(snuNotifyGamerJoinWishFailed, &m_SysJoinWish);
            }
            
            m_SysJoinWish.Reset();
        }
    }

    gWorkerSession.Update(curTime);
    for(i=0; i!=SNU_MAX_SESSIONS; i++)
    {
        m_aSessionData[i].Update(curTime);

		//recycle any dead gamers that are no longer needed
		for(int j=0; j<snuSessionData::maxGamerSlots; j++) 
		{
			snuGamer* gamer = m_aSessionData[i].GetGamer(j);
			if (gamer && gamer->IsDead() && !gamer->GetKeepAlive())
			{
				snuDebug( "Recycling dead gamer %s", gamer->GetName() );
				gamer->LeaveSession( &m_aSessionData[i] );
				m_aSessionData[i].ClearGamerSlot( gamer );

				GetRemoteGamers()->FreeGamer( gamer );
			}
		}
	}

	for( int i = 0; i < RL_MAX_LOCAL_GAMERS; i++ )
		m_aLocalGamer[ i ].Update();

	GetSearchBot()->Update(curTime);

	if (m_Voice)
	{
		m_Voice->Update();
	}

#if __PS3
	if (m_StartingMatchmakingService)
	{
		if (!g_rlNp.GetMatching().Starting())
		{
			m_StartingMatchmakingService = false;
			if (g_rlNp.GetMatching().Started())
			{
				snuDebug("Matchmaking service started successfully");
				Notify(snuNotifyMatchmakingServiceStartSucceeded);
			}
			else
			{
				//failed to start
				snuError("Matchmaking service failed to start");
				Notify(snuNotifyMatchmakingServiceStartFailed);
				//rlNpMatching requires that Stop be called if the initial Start call succeeds and the async part of it fails
				StopMatchmakingService();
			}
		}
	}
#endif
}


snuGamerLocal* 
snuSocket::GetLocalGamer(int i)
{
	Assert(i<=RL_MAX_LOCAL_GAMERS);
	return m_aLocalGamer+i;							
}



snuSessionData* snuSocket::GetSessionById(const snuSessionId& sid)
{
	for (int i=0; i!=SNU_MAX_SESSIONS; i++)
	{
		if (m_aSessionData[i].GetId() == sid)
		{
			return &m_aSessionData[i];
		}
	}

	return NULL;
}

const snuSessionData* snuSocket::GetSessionById(const snuSessionId& sid) const
{
    for(int i=0; i!=SNU_MAX_SESSIONS; i++)
    {
        if(m_aSessionData[i].GetId() == sid)
        {
            return &m_aSessionData[i];
        }
    }

    return 0;
}

snuSessionData* snuSocket::GetSessionByInfo(const rlSessionInfo& info) 
{
	for (int i=0; i!=SNU_MAX_SESSIONS; i++)
	{
		if (m_aSessionData[i].GetSession()->GetSessionInfo() == info)
		{
			return &m_aSessionData[i];
		}
	}

	return NULL;
}

const snuSessionData* snuSocket::GetSessionByInfo(const rlSessionInfo& info) const
{
	for (int i=0; i!=SNU_MAX_SESSIONS; i++)
	{
		if (m_aSessionData[i].GetSession()->GetSessionInfo() == info)
		{
			return &m_aSessionData[i];
		}
	}

	return NULL;
}

bool snuSocket::IsLocallyJoined(u64 uSessionToken) const
{
	for (int i=0; i!=SNU_MAX_SESSIONS; i++)
	{
		if (m_aSessionData[i].IsLocallyJoined() && m_aSessionData[i].GetSession()->GetSessionInfo().GetToken()==uSessionToken)
			return true;
	}
	return false;
}


netAddress snuSocket::GetGamerNetAddress(const snuGamer* pGamer) const
{
	Assert(pGamer);
	netAddress result;

	for (int i=0; i!=SNU_MAX_SESSIONS; i++)
	{
		const snSession* pSession = m_aSessionData[i].GetSession();
		if (pSession && pSession->Exists())
		{
			if(pSession->GetPeerNetAddress(pGamer->GetPeerInfo().GetPeerId(), &result))
				return result;
		}
	}

	result.Clear();
	return result;
}


snuHostData* snuSocket::GetHostData(const snuSessionId& sid)
{
	snuSessionData* sessionData = GetSessionById(sid);
	if( sessionData )
	{
		return &sessionData->GetHostData();
	}
	return NULL;
}

snuSessionData*    
snuSocket::NewSession(const snuSessionId& sid, const rlSessionInfo *info)
{
    int i;

    //find if already exists
    if(info!=0)
    {
        for(i=0; i!=SNU_MAX_SESSIONS; i++)
        {
            if(!m_aSessionData[i].IsFree()            
                && !m_aSessionData[i].IsTerminating()    
                && m_aSessionData[i].GetSession()->GetSessionInfo() == *info)
            {
                //why would ever new a session that already exists? 
                // ali: you dont, this actually ensures you reuse already instantiated sessions.
                
                //it's possible that this assert isn't valid
                Assert(sid == m_aSessionData[i].GetId());
                m_aSessionData[i].SetId(sid);
                return &m_aSessionData[i];
            }
        }
    }

    //otherwise find a free session object
    for(i=0; i!=SNU_MAX_SESSIONS; i++)
    {
        if(m_aSessionData[i].IsFree())
        {
            m_aSessionData[i].Reset();
            m_aSessionData[i].SetId(sid);
            return &m_aSessionData[i];
        }
    }

    return 0;
}

snuGamer* 
snuSocket::GetGamerFromID(const rlGamerId& gamerId) const
{
	//see if it matches a local gamer
	for( int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i )
	{
		if( m_aLocalGamer[i].IsSignedIn() && gamerId == m_aLocalGamer[i].GetGamerId() )
		{
			return &m_aLocalGamer[i];
		}
	}

	//if not, check the remote gamers
	for( int i = 0; i < GetRemoteGamers()->m_ActiveList.GetCount(); ++i )
	{
		if( gamerId == GetRemoteGamers()->m_ActiveList[i]->GetGamerId() )
			return GetRemoteGamers()->m_ActiveList[i];
	}
	return NULL;
}

snuGamer* 
snuSocket::GetGamerFromID(const u64& gamerId) const
{
	//see if it matches a local gamer
	for( int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i )
	{
		if( m_aLocalGamer[i].IsSignedIn())
		{
			u64 tmpId;
			m_aLocalGamer[i].GetPeerInfo().GetLocalPeerId(&tmpId);
			if (gamerId == tmpId)
			{
				return &m_aLocalGamer[i];
			}
		}
	}

	const atArray<snuGamer*> &rRemotes = GetRemoteGamers()->m_ActiveList;

	//if not, check the remote gamers
	for( int i = 0; i < rRemotes.GetCount(); ++i )
	{
		u64 tmpId;
		rRemotes[i]->GetPeerInfo().GetLocalPeerId(&tmpId);
		if (gamerId == tmpId)
		{
			return rRemotes[i];
		}
	}
	return NULL;
}

void 
snuSocket::OnPresenceEvent(rlPresence* /*presenceMgr*/, const rlPresenceEvent* evt)
{
    const int        gIdx    = evt->m_GamerInfo.GetLocalIndex();
    snuGamerLocal*    pGamer    = &m_aLocalGamer[gIdx];

	switch(evt->GetId())
	{
        case PRESENCE_EVENT_SIGNIN_STATUS_CHANGED:
            if(evt->m_SigninStatusChanged->SignedIn())
            {
                pGamer->Init(evt->m_GamerInfo);
        		Notify(snuNotifyGamerSignedIn, pGamer);
            }
            else if(evt->m_SigninStatusChanged->SignedOut())
            {
                pGamer->m_GamerInfo = evt->m_GamerInfo;
        		Notify(snuNotifyGamerSignedOut, pGamer);
            }
            else if(evt->m_SigninStatusChanged->SignedOnline())
            {
                pGamer->m_GamerInfo = evt->m_GamerInfo;
				pGamer->RefreshFriends();
        		Notify(snuNotifyGamerSignedOnline, pGamer);
            }
            else if(evt->m_SigninStatusChanged->SignedOffline())
            {
                pGamer->m_GamerInfo = evt->m_GamerInfo;
        		Notify(snuNotifyGamerSignedOffline, pGamer);
            }
		break;

    	case PRESENCE_EVENT_FRIEND_STATUS_CHANGED:
		    pGamer->RefreshFriends();
		    break;

	    case PRESENCE_EVENT_PROFILE_CHANGED:	
		    break;
		
		//case PRESENCE_EVENT_CONTENT_INSTALLED:
		//	Notify(snuNotifyNewContentInstalled, pGamer);
		//	break;
	
	    case PRESENCE_EVENT_INVITE_ACCEPTED:
		case PRESENCE_EVENT_JOINED_VIA_PRESENCE:
			if(AssertVerify(!m_SysJoinWish.IsValid()))
			{
				rlSessionInfo*	pSessionInfo	= NULL;

				snuDebug( "%s got a notification to join a game", pGamer->GetName() );

				if (evt->GetId()==PRESENCE_EVENT_INVITE_ACCEPTED)
					pSessionInfo = &evt->m_InviteAccepted->m_SessionInfo;
				else
					pSessionInfo = &evt->m_JoinedViaPresence->m_SessionInfo;


				//make sure we are not trying to join a local game
				int	i = 0;
				for (i=0; i!=SNU_MAX_SESSIONS; i++)
				{
					if(!m_aSessionData[i].IsFree() && m_aSessionData[i].GetSession()->GetSessionInfo() == *pSessionInfo)
					{
						Notify(snuNotifyGamerJoinWishFailedLocalGame, pGamer);
						break;
					}		
				}
				if (i!=SNU_MAX_SESSIONS)
					break;

				m_SysJoinWish.m_FromInvite	= evt->GetId()==PRESENCE_EVENT_INVITE_ACCEPTED;
			    m_SysJoinWish.m_pGamer		= pGamer;
			    m_SysJoinWish.m_SessionInfo	= *pSessionInfo;
                m_SysJoinWish.m_NetMode     = RL_NETMODE_ONLINE;

				gWorkerSession.QueryConfig
				(
					*pSessionInfo, 
					m_SysJoinWish.m_NetMode, 
					&m_SysJoinWish.m_Config,
					&m_SysJoinWish.m_Status
				);
		    }
		    break;
	}

	pGamer->HandlePresenceEvent(evt);
}

void 
snuSocket::OnCxnEvent(netConnectionManager* /*cxnMgr*/,
                      const netEvent* evt)
{
	//Get the event id.
	const unsigned eid = evt->GetId();

	if( NET_EVENT_FRAME_RECEIVED == eid )
	{
		//Received a frame of data.
		//A frame is the basic unit of data sent and recieved via
		//channels.  Typically we would know what kind of data it
		//is by looking at the channel id.  IOW, we segregate
		//different types of data onto different channels.

		netEventFrameReceived* fr = evt->m_FrameReceived;

		unsigned msgId;

		//if GetId fails, it probably means we received a non-netMessage packet
		//which we aren't expecting, so ignore it
		if( netMessage::GetId( &msgId, fr->m_Payload, fr->m_SizeofPayload ) )
		{
			//Deserialize the message
			if (snuGamer::snuGamerData::snuGamerDataSyncMsg::MSG_ID() == msgId)
			{
				snuGamer::snuGamerData::snuGamerDataSyncMsg msg;
                if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
                {
                    snuDebug2("%08x.%d: Received %s", fr->m_CxnId, fr->m_ChannelId, msg.GetMsgName());

                    snuGamer* pGamer = SNUSOCKET.GetGamerFromID(msg.GetGamerID());

                    if(pGamer)
                    {
                        msg.WriteGamerData(*pGamer);

						//first gamer data sync?
						if(!pGamer->IsDataReady() || pGamer->IsDead())
						{
							//because the remote gamer queues up a full snuGamerDataSyncMsg to you immediately 
							//after calling open connection,it is assumed that after you have received the 
							//first snuGamerDataSyncMsg from a gamer, then the gamer data is ready and the app 
							//should now know about this gamer being added.
							pGamer->m_IsDataReady=true;
							pGamer->SetDead(false);

							snuNotifyDataGamerAdded ga;
							ga.m_Gamer	= SNUSOCKET.GetGamerFromID( pGamer->GetGamerId() );

							for (int i=0; i!=SNU_MAX_SESSIONS; i++)
							{
								if 
								(
									pGamer->GetSessionData(i)				&& 
									!pGamer->GetSessionData(i)->IsFree()	&& 
									!pGamer->GetSessionData(i)->IsTerminating()
								)
								{
									ga.m_SessionData= pGamer->GetSessionData(i);
									ga.m_Addr		= pGamer->GetNetAddress(pGamer->GetSessionData(i)->GetId());
									snuDebug( "Remote gamer, %s, ready for session[%d]", pGamer->GetName(), i );
									Notify(snuNotifyGamerAdded, &ga);

									//add to recent gamers list only once the gamer is ready
									GetRecentGamers().AddGamer( pGamer->GetGamerInfo() );
								}
							}
							pGamer->GetGamerData()->DisplayData(3,3);
						}

						if(pGamer->IsRemote())
							Notify(snuNotifyGamerDataUpdated, pGamer);
				    }
				    else
				    {	
						snuWarning(( "Received message from unknown gamer" ));
				    }
                }
			}
			else if (snuHostDataSyncMsg::MSG_ID() == msgId)
			{
				snuHostDataSyncMsg msg;
				if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
				{
					snuDebug1( "%08x.%d: Received %s (%s Data)", fr->m_CxnId, fr->m_ChannelId, msg.GetMsgName(), msg.IsAllData() ? "All" : "Dirty" );
					snuHostData* hostData = msg.GetHostDataPtr();
					if( hostData )
					{
						hostData->DisplayData( !msg.IsAllData() );
						Notify(snuNotifyHostDataUpdated, hostData);
						hostData->ClearDirtyBits();
					}
				}
			}
			else if (snuSessionData::MigrateHostMsg::MSG_ID() == msgId)
			{
				snuSessionData::MigrateHostMsg msg;
				if(msg.Import(fr->m_Payload, fr->m_SizeofPayload))
				{
					snuDebug( "%08x.%d: Received %s", fr->m_CxnId, fr->m_ChannelId, msg.GetMsgName() );
					if (AssertVerify(msg.GetSessionId() != snuSessionId_Invalid))
					{
						//FIXME (Ali) Is this multi session compatible?
						snuSessionData* sessionData = SNUSOCKET.GetSessionById(msg.GetSessionId());
						if (AssertVerify(sessionData))
						{
							sessionData->MigrateHost(msg.GetNewHostPeerId());
						}
					}
				}
			}
			else if (snuGamer::snuGamerMuteMsg::MSG_ID() == msgId)
			{
				snuPostOffice::snuConnection* pCxn = GetPostOffice()->GetConnection(fr->m_CxnId, fr->m_ChannelId);
				if (pCxn && pCxn->GetGamer())
				{
					pCxn->GetGamer()->SetMutedBy(true);
				}
			}
			else if (snuGamer::snuGamerUnmuteMsg::MSG_ID() == msgId)
			{
				snuPostOffice::snuConnection* pCxn = GetPostOffice()->GetConnection(fr->m_CxnId, fr->m_ChannelId);
				if (pCxn && pCxn->GetGamer())
				{
					pCxn->GetGamer()->SetMutedBy(false);
				}
			}
		}
	}
}

//==================================================================================================================================
// class snuGamerPool
//==================================================================================================================================

snuGamer* snuGamerPool::AddGamer( const rlGamerInfo& gamerInfo, const netAddress& addr, int sessionId )
{
	snuGamer*	pSnuGamer	= SNUSOCKET.GetGamerFromID(gamerInfo.GetGamerId());

	if(!pSnuGamer)
    {
  	    if( AssertVerify(!m_Pool->IsFull()) )
  	    {
	  	    pSnuGamer = m_Pool->Construct();
		    m_ActiveList.Append() = pSnuGamer;
  	    }
    }
	else if(pSnuGamer->IsDead())
	{
		snuDebug( "Dead remote gamer '%s' is rejoining", pSnuGamer->GetName() );
	}

	if( AssertVerify(pSnuGamer) )
	{
		pSnuGamer->InitGamerInfo( gamerInfo );
		pSnuGamer->m_aAddr[sessionId] = addr;
	}

	if (pSnuGamer->IsRemote())
	{
		SNUSOCKET.GetPostOffice()->AddConnection( *pSnuGamer );
	}

	return pSnuGamer;
}

void snuGamerPool::FreeGamer( snuGamer* gamer )
{
	snuDebug( "Attempting to free %s gamer %s...", gamer->IsLocal()? "local" : "remote", gamer->GetName() );

	//don't free gamer if he is still in a session
	if( gamer->GetSessionCount()>0)
	{
		snuDebug( "Not freeing gamer '%s', still in a session", gamer->GetName() );
		return;
	}

	if( SNUSOCKET.GetVoice() && SNUSOCKET.GetVoice()->HaveTalker( gamer->GetGamerId() ) )
	{
		SNUSOCKET.GetVoice()->RemoveTalker( gamer->GetGamerId() );
	}

	//close connections
	if( gamer->IsRemote() )
	{
		SNUSOCKET.GetPostOffice()->CloseAllChannels( *gamer );
	}

	//don't delete the gamer if app has requested he be kept alive
	if( gamer->GetKeepAlive() )
	{
		snuDebug( "Keeping gamer '%s' alive, not deallocated!", gamer->GetName() );
		return;
	}

	//delete if remote gamer
	if (gamer->IsRemote())
	{
		SNUSOCKET.GetPostOffice()->FreeConnection( *gamer );
	
		int activeIdx = m_ActiveList.Find( gamer );
		if( AssertVerify( activeIdx >= 0 ) )
		{
			SNUSOCKET.Notify(snuSocket::snuNotifyGamerDestroying, gamer);

			m_ActiveList.Delete( activeIdx );
		}

		m_Pool->Destruct( gamer );
	}
}
