// 
// snu/snugamerdata.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNU_HOSTDATA_H
#define SNU_HOSTDATA_H

#include "snuconfig.h"
#include "atl/array.h"
#include "snet/messages.h"
#include "system/criticalsection.h"

class snuSessionData;
class snuGamer;

namespace rage
{
    class rlSchema;
}

class snuHostData
{
public:
    enum FieldType 
    {
        FIELD_UNDEFINED = -1,
        FIELD_INT,
        FIELD_UNS,
        FIELD_FLOAT,
        FIELD_SER,
        FIELD_VOID
    };

    struct snuFieldDef
    {
        const char*  m_szName;
        rage::u16    m_BitSize;
        FieldType    m_Type;
        int          m_SchemaFieldID;        // -1 if no corresponding schema field id
    };

    snuHostData(snuSessionData& sessionData);
    ~snuHostData();

    bool DefineFields(const snuFieldDef fieldDefs[], 
                      int fieldCount, 
                      bool bSessionCreator);
    void UndefineFields();
    bool IsDefined() const { return m_IsDefined; }
    bool IsReady() const { return m_IsReady; }
    int GetFieldIndexByName(const char *name) const;
    int GetFieldBitSize(const int i) const;
    int GetTotalBitSize() const;
    int GetSchemaFieldID(const int i) const { return AssertVerify(m_IsDefined)?m_FieldDefs[i].m_SchemaFieldID:0; }
    int GetFieldCount() const;
    const char* GetFieldName(const int i) const { return AssertVerify(m_IsDefined)?m_FieldDefs[i].m_szName:NULL; }
    FieldType GetFieldType(const int i) const;
    
    snuSessionData&    GetSessionData();

    int GetData(const int i, void *d) const;
    int GetDataInt(const int i) const;
    unsigned GetDataUns(const int i) const;
    float GetDataFloat(const int i) const;

    int SetData(const int i, const void *v);
    int    SetDataInt(const int i, const int v);
    int    SetDataUns(const int i, const unsigned v);
    int SetDataFloat(const int i, const float v);

    // a serialize-able field can hold any type that implements Export and Import:
    //    bool Import(const datImportBuffer& bb);
    //    bool Export(datExportBuffer& bb) const;

    // uses T::Import to extract the bits into destData
    // returns number of bits that were read, 0 if data is invalid
    template<typename T>
    int GetDataSer(const int i, T& destData) const
    {
        SYS_CS_SYNC(m_CsToken);

        int numBitsRead = 0;
        if(AssertVerify(m_IsDefined) &&
            AssertVerify(GetFieldType(i) == FIELD_SER) &&
            AssertVerify(IsFieldValid(i)))
        {
            //read the data
            rage::datImportBuffer bb;
            bb.SetReadOnlyBits(m_DataBuffer, GetFieldBitSize(i), m_BitOffsets[i]);

            if(destData.Import(bb))
            {
                numBitsRead = bb.GetNumBitsRead();
            }
        }
        return numBitsRead;
    }

    // uses T::Export to write the bits from srcData
    // returns number of bits that were written, 0 on failure
    template<typename T>
    int SetDataSer(const int i, const T& srcData)
    {
        SYS_CS_SYNC(m_CsToken);

        m_BitsUsed[i] = 0;
        if(AssertVerify(m_IsDefined) && 
            AssertVerify(GetFieldType(i) == FIELD_SER))
        {
            //write the data
            rage::datExportBuffer bb;
            bb.SetReadWriteBits(m_DataBuffer, GetFieldBitSize(i), m_BitOffsets[i]);

            if(srcData.Export(bb))
            {
                m_BitsUsed[i] = bb.GetNumBitsWritten();

                SetFieldDirty(i);

                SetFieldValid(i);
            }
        }
        return m_BitsUsed[i];
    }

    //dirty bits
    //typical updates will only send fields that have changed since the last update
    void DisplayData(bool dirtyOnly) const;
	void DisplayField(const int idx) const;
    void SetFieldDirty(const int idx);
    void SetFieldClean(const int idx);
    bool IsFieldDirty(const int idx) const;
    bool IsDirty() const;
    void ClearDirtyBits();

    //valid bits
    //a field is valid once it is set, fields can then be cleared manually
    //the data bits for invalid fields are not sent
    void ClearField(const int idx);
    void ClearAllFields();
    bool IsFieldValid(const int idx) const;

    void SendDirtyData(const snuGamer* destGamer=NULL);
    void SendAllData(const snuGamer* destGamer=NULL);

private:
    friend class snuHostDataSyncMsg;

    void SetFieldValid(int idx);
    void CopyFieldToSchema(rage::rlSchema&, int idx) const;

    enum 
    { 
        MAX_FIELDS = 32, 
        MAX_BYTES = 512 
    };

    snuSessionData& m_SessionData; //the session that owns this host data object

    bool m_IsDefined; // have the fields been defined yet
    bool m_IsReady; // if we are a client, data is ready after receiving a copy from the host. if we are the session creator, data is ready as soon as the fields are defined.

    rage::u8 m_DataBuffer[MAX_BYTES];    // raw storage for the data bit buffer
    rage::datBitBuffer m_DataBits; // bit buffer for accessing the fields with arbitrary bit offsets and sizes

    //FIXME(RDT): Use a bitset instead, will simplify.
    rage::u8 m_DirtyBitsBuffer[(MAX_FIELDS+7) / 8]; // raw storage for the dirty bits
    rage::datBitBuffer m_DirtyBits; // bit buffer for accessing the dirty bits

    rage::u8 m_ValidBitsBuffer[(MAX_FIELDS+7) / 8]; // raw storage for the valid bits
    rage::datBitBuffer m_ValidBits; // bit buffer for accessing the valid bits

    rage::atFixedArray< int, MAX_FIELDS+1 > m_BitOffsets; // index into data bit buffer for each field
    rage::atFixedArray< int, MAX_FIELDS > m_BitsUsed; // how many bits is the field using now (out of it's predefined max), only useful for serialized fields
    rage::atFixedArray< snuFieldDef, MAX_FIELDS > m_FieldDefs; // definitions for each field

    mutable rage::sysCriticalSectionToken m_CsToken;
};

class snuHostDataSyncMsg
{
public:
    NET_MESSAGE_DECL(snuHostDataSyncMsg);

    //FIXME(RDT): Why explicit? there's no conversion
    explicit snuHostDataSyncMsg()
    : m_IsAllData(false)
    , m_SessionId(snuSessionId_Invalid)
    {
    }

    void InitForWrite(const snuSessionId& sid, bool isAllData);

    const snuSessionId& GetSessionId() const { return m_SessionId; }
    bool IsAllData() const { return m_IsAllData; }

    snuHostData* GetHostDataPtr() const;

    static int CalcBitsNeededForUns(rage::u32 v)
    {
        int numBits = 0;

        while(v)
        {
            numBits++;
            v = v >> 1;
        }

        return numBits;
    }

    NET_MESSAGE_SER(bb, msg)
    {
        bool bReceivingMsg = bb.IsReadOnly();
        bool success = bb.SerInt(msg.m_SessionId, sizeof(msg.m_SessionId)*8)
                       && bb.SerBool(msg.m_IsAllData)
                       && msg.GetHostDataPtr() != NULL;

        if(success)
        {
            snuHostData* hostData = msg.GetHostDataPtr();

            SYS_CS_SYNC(hostData->m_CsToken);

            //serialize dirty bits if needed
            if(success && !msg.m_IsAllData)
            {
                hostData->m_DirtyBits.SetCursorPos(0);
                success = bb.SerBits(hostData->m_DirtyBits, hostData->GetFieldCount());
            }

            //serialize the valid bits
            if(success)
            {
                hostData->m_ValidBits.SetCursorPos(0);
                success = bb.SerBits(hostData->m_ValidBits, hostData->GetFieldCount());
            }

            //serialize each valid dirty field, or all valid fields if requested
            for(int i=0; success && i<hostData->GetFieldCount(); ++i)
            {
                if(msg.m_IsAllData || hostData->IsFieldDirty(i))
                {
                    if(hostData->IsFieldValid(i))
                    {
                        rage::u32 maxBits = (rage::u32) hostData->GetFieldBitSize(i);
                        rage::u32 numBits = maxBits;

                        //serialize the number of bits used along with serialized fields
                        if(hostData->GetFieldType(i)==snuHostData::FIELD_SER)
                        {
                            if(!bb.IsReadOnly())    //sending data
                                numBits = (rage::u32) hostData->m_BitsUsed[i];

                            int numSerBits = CalcBitsNeededForUns(maxBits);
                            success = bb.SerUns(numBits, numSerBits);
                        }

                        if(success)
                        {
                            hostData->m_DataBits.SetCursorPos(hostData->m_BitOffsets[i]);
                            success = bb.SerBits(hostData->m_DataBits, numBits);

                            if(bReceivingMsg)
                            {
                                hostData->m_BitsUsed[i] = numBits;
                                hostData->SetFieldDirty(i);    //ensure recipient knows this field was updated (in case this was an AllData message)
                            }
                        }
                    }
                }
            }
        }

        if (success && bReceivingMsg)
        {
            snuHostData* hostData = msg.GetHostDataPtr();
            hostData->m_IsReady = true;
        }

        return success;
    }

protected:
    bool m_IsAllData; //if true, all valid fields are included, dirty or not
    snuSessionId m_SessionId; //id of session to which the host data belongs

private:
    const snuHostDataSyncMsg &operator=(const snuHostDataSyncMsg&);    //not implemented
};

#endif // SNU_HOSTDATA_H



