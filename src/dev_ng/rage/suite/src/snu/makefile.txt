Project snu

IncludePath $(RAGE_DIR)\base\src

Files {
	snuappdata.h
	snubitbuffer.h
	snuconfig.h
	snuconfig.cpp
	snudiag.cpp
	snudiag.h
	snugamer.cpp
	snugamer.h
	snuhostdata.cpp
	snuhostdata.h
	snuoptions.cpp
	snuoptions.h
	snupostoffice.cpp
	snupostoffice.h
	snusearchbot.cpp
	snusearchbot.h
	snusessiondata.cpp
	snusessiondata.h
	snutasks.cpp
	snutasks.h
	snusocket.cpp
	snusocket.h
}
