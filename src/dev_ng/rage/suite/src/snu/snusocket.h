// 
// snu\snusocket.h
// 
// Copyright(C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNU_SOCKET_H
#define SNU_SOCKET_H

#include "snuconfig.h"

#include "atl/queue.h"
#include "atl/singleton.h"

#include "avchat/voicechat.h"

#include "net/connectionmanager.h"
#include "net/nethardware.h"

#include "rline/rlsession.h"
#include "rline/rlgamerinfo.h"
#include "rline/rlsystemui.h"
#include "rline/rlpresence.h"
#include "rline/rlnotifier.h"

#include "snet/session.h"
#include "system/simpleallocator.h"

class   snuSessionData;
class   snuGamer;
class   snuGamerLocal;
class   snuSearchBot;
class   snuHostData;
class   snuPostOffice;
struct  snuUserOption;
class   snuUserOptions;
struct  snuGamerDataDef;
class   snuGamerPool;
class   snuComponentFactory;

class snuNotifyDataGamer : public snuNotifyBase 
{
public:
	snuNotifyDataGamer()
	: m_SessionData	(NULL)
	, m_Gamer		(NULL)
	{}
	virtual ~snuNotifyDataGamer() {}

	snuSessionData*     m_SessionData;
	snuGamer*           m_Gamer;
};

class snuNotifyDataGamerAdded : public snuNotifyDataGamer 
{
public:
	snuNotifyDataGamerAdded()
	: snuNotifyDataGamer()
	, m_JoinData		(NULL)
	, m_SizeofJoinData	(0)
	{
		m_Addr.Clear();
	}
    virtual ~snuNotifyDataGamerAdded() {}

    rage::netAddress    m_Addr;
    const void*         m_JoinData;			//the host determined application data for this gamer (e.g., a synchronized index)
    int                 m_SizeofJoinData;	//size of data
};

class snuNotifyDataGamerRemoved : public snuNotifyDataGamer 
{
public:
    virtual ~snuNotifyDataGamerRemoved() {}
};

class snuNotifyDataGetGamerJoinData : public snuNotifyBase 
{
public:
    virtual ~snuNotifyDataGetGamerJoinData() {}

    //input params (passed to app from snu)
    snuGamer*        m_Gamer;            //gamer who is joining
    int              m_MaxSizeofData;    //max size of data the app can return

    //return params (passed from app back to snu)
    rage::u8*        m_Data;                //pack the host determined application data in here (e.g., a synchronized index)
    int              m_SizeofData;        //actual size of data being returned
};

class snuNotifyDataHostMigrated : public snuNotifyBase
{
public:
    virtual ~snuNotifyDataHostMigrated() {}
    snuSessionData* m_pSessionData;
    snuGamer*       m_pNewHost;
};

class snuNotifyDataFailedRequested : public snuNotifyBase
{
public:
	virtual ~snuNotifyDataFailedRequested() {}
	snuSessionData* m_pSessionData;
	snuGamerJoinWish* m_pJoinWish;
};


class snuGamerInfoQueue
{
    //Maintains a FIFO list of gamers. If the same gamer is added twice it moves to the 
    //end of the list instead of adding a duplicate.
    //Used for recent gamers list and banned gamers list.

    //FIX make size app configurable. Possibly by creating the queues through the component factory.
    //May require using a data structure with a size that is not a template parameter. atArray?
    enum { QUEUE_SIZE = 100 };    

public:
    snuGamerInfoQueue();
    ~snuGamerInfoQueue();

    void Clear();
    void AddGamer(const rage::rlGamerInfo& info);
    int GetNumGamers() const;
    const rage::rlGamerInfo* GetGamer(int n) const;
    int FindGamerIndex(const rage::rlGamerInfo& info) const;    //returns -1 if not found

private:
    rage::atQueue< rage::rlGamerInfo, QUEUE_SIZE > m_Gamers;
};

class snuSocket
{
    friend class snuGamerLocal;
    friend class snuSessionData;
    friend class snuGamerPool;
    friend class snuFriendInvites;

public:
    enum NotifyCode    
    { 
        snuNotifyFailedRequest=0, 
        snuNotifyRequestDenied,
        snuNotifyJoined,
        snuNotifyFriendsUpdated,
        snuNotifySessionStarted,
        snuNotifySessionStartFailed,
        snuNotifySessionDestroyed,
        snuNotifyGamerAdded,
        snuNotifyGamerRemoved,
        snuNotifyGamerDestroying,
        snuNotifyGamerJoinWish,
        snuNotifyGamerJoinWishFailed,
        snuNotifyGamerJoinWishFailedLocalGame,        
        snuNotifyGamerInviteReceived,
        snuNotifyGamerDataUpdated,
        snuNotifyGamersUpdated,
        snuNotifyGamerSignedIn,
        snuNotifyGamerSignedOut,
        snuNotifyGamerSignedOnline,
        snuNotifyGamerSignedOffline,
        snuNotifyGetGamerJoinData,
        snuNotifyNewContentInstalled,
        snuNotifyHostDataUpdated,
        snuNotifyHostMigrated,
        snuNotifyHostMigrateFailed,
        snuNotifyArbitrationFailed,
		snuNotifyMatchmakingServiceStartSucceeded,
		snuNotifyMatchmakingServiceStartFailed
    };    

    RL_NOTIFIER(snuSocket, void);

    bool Init(rage::rlTitleId*,
            rage::u32 nMinAge,
            unsigned short port,
            snuComponentFactory& factory,
            snuUserOption[],
            const snuGamerDataDef[],
            const int gamerDataCount);
    void Shutdown();
    void Update();

    void InitVoiceChat();
    void ShutdownVoiceChat();

    bool IsInitialized() { return m_Status >= snuSocketInitialised; }

	//PURPOSE:
	//  Start matchmaking service (asynchrnous). Not strictly required, but highly recommended to call this
	//  before you begin any PS3 matchmaking operations. snuNotifyMatchmakingServiceStartSucceeded (or Failed)
	//  is sent when the request completes.
	//NOTES:
	//	Safe to call on all platforms, but only useful on PS3.
	//
	//  Ideally StartMatchmakingServices() should be called just prior to entering multiplayer mode, 
	//  and StopMatchmakingServices() should be called when exiting MP mode. If those calls aren't
	//  made then NP matching will be started/stopped repeatedly as find, create, and join 
	//  operations are performed, and that's likely to put undue load on NP servers.
	void StartMatchmakingService();

	//PURPOSE:
	//  Stop matchmaking services. See notes on StartMatchmakingService().
	//  Safe to call this even if matchmaking services weren't actually started.
	void StopMatchmakingService();

    rage::netSocket*            GetSocket() { return m_Socket; }
    snuPostOffice*              GetPostOffice() { return m_pPostOffice; }
    const snuPostOffice*        GetPostOffice() const { return m_pPostOffice; }
    snuGamerPool*               GetRemoteGamers() { return m_pRemoteGamers; }
	const snuGamerPool*         GetRemoteGamers() const { return m_pRemoteGamers; }
	rage::rlPresence*           GetPresenceManager() { return &m_PresenceMgr; }
    snuUserOptions*             GetUserOptions() { return m_pUserOptions; }
    snuSearchBot*               GetSearchBot() { return m_pSearchBot; }
    snuGamerLocal*              GetLocalGamer(int i);
    snuGamer*                   GetGamerFromID(const rage::rlGamerId& id) const;
	snuGamer*					GetGamerFromID(const rage::u64& gamerId) const;
	const rage::netHardware*	GetHardware() const { return m_Hardware; }
    snuHostData*                GetHostData(const snuSessionId& id);
	rage::netAddress			GetGamerNetAddress	(const snuGamer* pGamer) const;
    rage::VoiceChat*            GetVoice() { return m_Voice; }
    snuGamerInfoQueue&          GetRecentGamers() { return m_RecentGamers;    }
    const snuGamerInfoQueue&    GetRecentGamers() const { return m_RecentGamers;    }
    snuGamerInfoQueue&          GetBannedGamers() { return m_BannedGamers;    }
    const snuGamerInfoQueue&    GetBannedGamers() const { return m_BannedGamers;    }
    rage::rlSystemUi&           GetSysUi() { return m_SysUi; }
    rage::sysMemSimpleAllocator*GetAllocator() { return &m_Allocator; }
    int                         GetCpuAffinity() const { return m_CpuAffinity; }

    const snuSessionData*		GetSessionByInfo(const rage::rlSessionInfo& id)	const;
    snuSessionData*				GetSessionByInfo(const rage::rlSessionInfo& id);
    const snuSessionData*		GetSessionById(const snuSessionId& id) const;
    snuSessionData*				GetSessionById(const snuSessionId& id);
	bool						IsLocallyJoined(rage::u64 uSessionToken) const;


protected:
    // Make the constructor/destructor protected, so this can only be
    // instantiated as a singleton.
    snuSocket();
    ~snuSocket();

    // no implementation of assignment or copy constructor
    const snuSocket &operator=(const snuSocket&);
    snuSocket(const snuSocket&);

    snuSessionData*    NewSession(const snuSessionId& sid, const rage::rlSessionInfo*);
    void OnPresenceEvent(rage::rlPresence* presenceMgr, const rage::rlPresenceEvent* evt);
    void OnCxnEvent(rage::netConnectionManager* cxnMgr, const rage::netEvent* event);

    rage::netHardware*  m_Hardware;
    rage::netSocket*    m_Socket;
    int                 m_CpuAffinity;

    snuSocketStatusID   m_Status;
    snuGamerJoinWish    m_SysJoinWish;

    rage::rlPresence    m_PresenceMgr;
    snuGamerPool*       m_pRemoteGamers;
    snuGamerLocal*      m_aLocalGamer;
    snuSessionData*     m_aSessionData;
    snuSearchBot*       m_pSearchBot;
    snuPostOffice*      m_pPostOffice;
    rage::VoiceChat*    m_Voice;
    snuUserOptions*     m_pUserOptions;
    snuGamerInfoQueue   m_RecentGamers;
    snuGamerInfoQueue   m_BannedGamers;
    
    //snSession::NotifyHandler                m_SessionNotifyHandlers[snuMaxTotalSessions];
    rage::rlPresence::Delegate m_PresenceDlgt;
    rage::netConnectionManager::Delegate m_aCxnDelegate[SNU_MAX_SESSIONS];

    //allocator
    rage::sysMemSimpleAllocator m_Allocator;
    rage::u8 m_Heap[1024*(256+64)];
 
    //We'll use this to show the sign-in UI in the case where a gamer
    //is not yet signed in.
    rage::rlSystemUi m_SysUi;

	bool m_StartingMatchmakingService;
};

typedef atSingleton<snuSocket> snuSocketSingleton;
#define SNUSOCKET (snuSocketSingleton::InstanceRef())

//snuComponentFactory
//
//An abstract class for creating custom components to be used by snu.
//The app must create a factory subclass and provide implementations for
//creating each component. A factory must be passed into snuSocket::Init(). 
//
//Still undecided as to whether this is a good idea....we'll see.
//The main problem this solves is that its no longer necessary for the app
//to allocate and manage the post office object which is passed into snuSocket.
//This way snuSocket is in total control of the object's life cycle and usage.
//
//This is only used for the snuPostOffice at the moment, but there's potential
//for other uses, such as creating a pool of app specific gamers.
//MRB 9/7/07
class snuComponentFactory 
{
public:
    virtual ~snuComponentFactory() {}
    virtual snuPostOffice* CreatePostOffice()    = 0;
};

#undef IGNORE
#undef GetObject

#endif // SNU_SOCKET_H
