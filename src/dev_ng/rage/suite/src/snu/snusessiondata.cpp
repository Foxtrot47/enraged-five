// 
// snu\snusessiondata.cpp 
// 
// Copyright(C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "snusessiondata.h"


#include "snudiag.h"
#include "snuhostdata.h"
#include "snugamer.h"
#include "snupostoffice.h"
#include "snusocket.h"

#include "atl/array.h"
#include "system\param.h"
#include "system\typeinfo.h"

#include "file/stream.h"


PARAM(netRandomHostMigrations, "randomly trigger a host migration if i am the current host");

NET_MESSAGE_IMPL(snuSessionData::MigrateHostMsg);

snuSessionData::snuSessionData()
 : m_HostData(0)
{
	m_aGamer.SetCount(maxGamerSlots);
	for (int i=0 ; i<maxGamerSlots ; i++)
	{
		m_aGamer[i].m_pGamer = 0;
		m_aGamer[i].m_Reserved = 0;
	}
	Reset();
	m_HostData = rage_new snuHostData(*this);
	m_SessionDlgt.Bind(this, &snuSessionData::HandleEvent);
	m_pHost = NULL;
}

snuSessionData::~snuSessionData()
{
	m_SnSession.Shutdown();

	delete m_HostData;
	m_HostData = NULL;
}

void snuSessionData::Init(int i)
{
	m_SessionIdx = i;

    snSessionOwner owner;
    owner.GetGamerData.Bind(this, &snuSessionData::HandleGetGamerData);
    owner.HandleJoinRequest.Bind(this, &snuSessionData::HandleJoinRequest);
	
	m_SnSession.Init(
		&SNUSOCKET.m_Allocator, 
        owner,
		SNUSOCKET.GetPostOffice()->GetConnectionManager(), 
		SNET_CHANNEL_ID,
		SNUSOCKET.GetPostOffice()->GetConnectionManager()->GetSocket()->GetPort() + 1);

	m_SnSession.AddDelegate(&m_SessionDlgt);
}

void snuSessionData::UnInit()
{
	m_SnSession.Shutdown();

	if (!IsFree())
		Terminate();
}

void snuSessionData::Reset()
{ 
	m_Id				= snuSessionId_Invalid;
	m_DestroyingSession	= false;
	m_MigratingHost		= false;
	m_pHost				= NULL;
	m_NextRandomMigrationTime = 0;

	// Reset gamer slots
	ResetGamerSlots();

	// Reset 
	if( m_HostData && m_HostData->IsDefined() )
	{
		m_HostData->UndefineFields();
	}
}

void snuSessionData::Update(const unsigned curTime)
{
	m_SnSession.Update(curTime);

	if (!m_StartStatus.Pending())
	{
		if (m_StartStatus.Failed())
		{
			snuWarning(( "Session start failed! Notifying Arbitration failiure!" ));
			SNUSOCKET.Notify(snuSocket::snuNotifyArbitrationFailed, NULL);
			//Terminate();
		}
		
		m_StartStatus.Reset();
	}
	if (!m_EndStatus.Pending())
	{
		if (m_EndStatus.Failed())
		{
			snuWarning(( "Session end failed! Notifying Arbitration failiure!" ));
			SNUSOCKET.Notify(snuSocket::snuNotifyArbitrationFailed, NULL);
			//Terminate();
		}
		m_EndStatus.Reset();
	}

	//send dirty host data
	if( GetSession()->Exists() && !m_DestroyingSession )
	{
		if( m_HostData && m_HostData->IsDefined() && GetSession()->IsHost() )
		{
			if( m_HostData->IsDirty() )
			{
				m_HostData->SendDirtyData();
				m_HostData->DisplayData(true);
				SNUSOCKET.Notify(snuSocket::snuNotifyHostDataUpdated, m_HostData);
				m_HostData->ClearDirtyBits();
			}
		}

#if !__FINAL
		//trigger random host migrations
		u64 localPeerId;

		if (PARAM_netRandomHostMigrations.Get() && GetSession()->IsHost()
			&& !GetSession()->IsMigrating() && GetSession()->GetPeerCount()
			&& rlPeerInfo::GetLocalPeerId(&localPeerId))
		{
			if (m_NextRandomMigrationTime == 0)
			{
				//schedule a migration
				m_Rand.Reset((int)curTime);
				unsigned nextMigrationDelta = (unsigned) m_Rand.GetRanged(5, 20);
				m_NextRandomMigrationTime = curTime + (nextMigrationDelta*1000);
				snuDebug( "Random host migration in %d seconds", nextMigrationDelta );
			}
			else if (curTime >= m_NextRandomMigrationTime)
			{
				snuDebug( "Starting a random host migration..." );

				//find a new host peer
				int			i				= 0;
				rlGamerInfo gamerInfos		[maxGamerSlots];
				int			gamerCount		= GetSession()->GetGamers(gamerInfos, maxGamerSlots);

				for (i=0; i < gamerCount; ++i)
				{
					if (gamerInfos[i].GetPeerInfo().GetPeerId() != localPeerId)
					{
						u64 newHostPeerId = gamerInfos[i].GetPeerInfo().GetPeerId();
						
						MigrateHost(newHostPeerId);
						MigrateHostMsg msg(m_Id, newHostPeerId);
						SNUSOCKET.GetPostOffice()->SendToSession(*this, msg, NET_SEND_RELIABLE);

						break;
					}
				}

				if (i == gamerCount)
					snuDebug( "Failed to find a new host for random migration!" );
			}
		}
		else
		{
			m_NextRandomMigrationTime = 0;
		}
#endif
	}

	// Resolve any gamer slots that match their reservation
	// TODO: Add timeout for old reservations
	for(int i=0; i<maxGamerSlots; i++) 
	{
		// If a slot has a gamer, and also a reservation
		if((m_aGamer[i].m_pGamer != 0) && (m_aGamer[i].m_Reserved != 0))
		{
			// Make sure they match up
			if(m_aGamer[i].m_pGamer->GetGamerInfo().GetPeerInfo().GetPeerId() == m_aGamer[i].m_Reserved)
			{
				// Match. Go ahead and clear the reservation
				m_aGamer[i].m_Reserved = 0;
			}
			else
			{
				// Uh oh. Mismatch!
				Assertf(0, "snuSessionData::Update(): Gamer slot %d has mismatch with slot reservation!", i);

				// Well, go ahead and clear the reservation I guess
				m_aGamer[i].m_Reserved = 0;
			}
		}
	}
}

void snuSessionData::ResetGamerSlots()
{ 
	for(int i=0; i<maxGamerSlots; i++) 
	{
		if (m_aGamer[i].m_pGamer != 0)
		{
			snuDebug( "Session Reset: Cleaning up gamer '%s'", m_aGamer[i].m_pGamer->GetName() );
			m_aGamer[i].m_pGamer->LeaveSession(this);
			SNUSOCKET.GetRemoteGamers()->FreeGamer(m_aGamer[i].m_pGamer);
			m_aGamer[i].m_pGamer = 0;	
			m_aGamer[i].m_Reserved = 0;
		}
	}
}

void snuSessionData::MigrateHost(u64 newHostPeerId)
{
	rlGamerInfo gamerInfos[maxGamerSlots];
	int gamerCount = GetSession()->GetGamers(gamerInfos, maxGamerSlots);

	for (int i=0; i < gamerCount; ++i)
	{
		if (gamerInfos[i].GetPeerInfo().GetPeerId() == newHostPeerId)
		{
			snuDebug1( "MigrateHost: newHostPeerId=0x%"I64FMT"x", newHostPeerId );

			rlPeerInfo hostCandidatePeers[1];
			hostCandidatePeers[0] = gamerInfos[i].GetPeerInfo();
			GetSession()->Migrate(hostCandidatePeers, 1);
			return;
		}
	}

	snuError( "MigrateHost: failed to find peer with id 0x%" I64FMT "x", newHostPeerId );
}

bool snuSessionData::IsLocallyJoined() const	
{
	// Session exists?
	if(!GetSession()->Exists())
		return false;

	// Session is not in the middle of terminating?
	if(IsTerminating())
		return false;

	// We're not officially in a session until we have a session host, unless we're migrating...
	if(!GetHost() && !IsMigratingHost())
	{
		return false;
	}

	// We're not officially in a session until we have a local gamer
	for(int i=0; i<snuSessionData::maxGamerSlots; ++i)
	{
		const snuGamer* pGamer = GetGamer(i);
		if(pGamer)
		{
			if(pGamer->IsLocal())
				return true;
		}
	}

	return false;
}

bool snuSessionData::IsLocallyHosted() const	
{
	return m_pHost && m_pHost->IsLocal();
}

snuGamer* snuSessionData::GetGamerByPeerId(const u64 peerId)
{
	for (int i=0; i!=maxGamerSlots; ++i)
	{
		if (m_aGamer[i].m_pGamer && (m_aGamer[i].m_pGamer->GetPeerInfo().GetPeerId() == peerId))
		{
			return m_aGamer[i].m_pGamer;
		}
	}

	return NULL;
}

const snuGamer* snuSessionData::GetGamerByPeerId(const u64 peerId) const
{
	for (int i=0; i!=maxGamerSlots; ++i)
	{
		if (m_aGamer[i].m_pGamer && (m_aGamer[i].m_pGamer->GetPeerInfo().GetPeerId() == peerId))
		{
			return m_aGamer[i].m_pGamer;
		}
	}

	return NULL;
}

int snuSessionData::GetGamerIndex(const u64 peerId) const
{
	for (int i=0; i!=maxGamerSlots; ++i)
	{
		if (m_aGamer[i].m_pGamer && (m_aGamer[i].m_pGamer->GetPeerInfo().GetPeerId() == peerId))
		{
			return i;
		}
	}
	return -1;
}

int snuSessionData::GetGamerIndex(const rage::rlGamerId& id) const
{
	for (int i=0; i!=maxGamerSlots; ++i)
	{
		if (m_aGamer[i].m_pGamer && m_aGamer[i].m_pGamer->GetGamerId() == id)
		{
			return i;
		}
	}
	return -1;
}

const snuGamer* snuSessionData::GetGamerById(const rlGamerId& id) const
{
	for (int i=0; i!=maxGamerSlots; i++)
	{
		if (m_aGamer[i].m_pGamer && m_aGamer[i].m_pGamer->GetGamerId() == id)
		{
			return m_aGamer[i].m_pGamer;
		}
	}

	return NULL;
}

snuGamer* snuSessionData::GetGamerById(const rlGamerId& id)
{
	for (int i=0; i!=maxGamerSlots; i++)
	{
		if (m_aGamer[i].m_pGamer && m_aGamer[i].m_pGamer->GetGamerId() == id)
		{
			return m_aGamer[i].m_pGamer;
		}
	}

	return NULL;
}

snuGamer* snuSessionData::GetGamerByHandle(const rlGamerHandle& handle)
{
	for (int i=0; i!=maxGamerSlots; i++)
	{
		if (m_aGamer[i].m_pGamer && m_aGamer[i].m_pGamer->GetGamerHandle() == handle)
		{
			return m_aGamer[i].m_pGamer;
		}
	}

	return NULL;
}

const snuGamer* snuSessionData::GetGamerByHandle(const rlGamerHandle& handle) const
{
	for (int i=0; i!=maxGamerSlots; i++)
	{
		if (m_aGamer[i].m_pGamer && m_aGamer[i].m_pGamer->GetGamerHandle() == handle)
		{
			return m_aGamer[i].m_pGamer;
		}
	}

	return NULL;
}


bool snuSessionData::IsMember(const rlGamerHandle& rHandle) const
{
	for (int i=0; i!=maxGamerSlots; i++)
	{
		if (m_aGamer[i].m_pGamer && m_aGamer[i].m_pGamer->GetGamerHandle() == rHandle)
		{
			return true;
		}
	}

	return false;
}


int	snuSessionData::GetGamerCount()	const
{
	int count = 0;

	for (int i=0; i!=maxGamerSlots; i++)
	{
		if (m_aGamer[i].m_pGamer)
		{
			count++;
		}
	}

	return count;
}

u32	snuSessionData::GetReadyGamerCount(rlSlotType type) const
{
	u32 count = 0;

	for (int i=0; i!=maxGamerSlots; i++)
	{
		if 
		(
			m_aGamer[i].m_pGamer && m_aGamer[i].m_pGamer->IsDataReady() && 
			( type==RL_SLOT_INVALID || GetSession()->GetSlotType(m_aGamer[i].m_pGamer->GetGamerId())==type )
		) 
		{
			count++;
		}
	}

	return count;
}

int 
snuSessionData::ReserveGamerSlot(u64 peerId)
{
	Assert(m_SessionIdx>=0 && m_SessionIdx<SNU_MAX_SESSIONS);

	for(int i=0; i<maxGamerSlots; i++)
	{
		if( (m_aGamer[i].m_pGamer == 0) && (m_aGamer[i].m_Reserved == 0) )
		{
			m_aGamer[i].m_Reserved = peerId;
			return i;
		}
	}

	snuError(( "snuSessionData::ReserveGamerSlot(): Out of Gamer slots!!" ));

	return GamerIndexVoid;
}

// Find a free slot, preferring the slot reserved for peerId
int snuSessionData::GetFreeGamerSlot(u64 peerId) const
{
    Assert(m_SessionIdx>=0 && m_SessionIdx < SNU_MAX_SESSIONS);

	// If we are looking for a reserved slot, check for that first
	if(peerId != 0)
	{
		for(int i=0; i<maxGamerSlots; i++)
		{
			if( (m_aGamer[i].m_pGamer == 0) && (m_aGamer[i].m_Reserved == peerId) )
			{
				return i;
			}
		}
	}

	for(int i=0; i<maxGamerSlots; i++)
	{
		if( (m_aGamer[i].m_pGamer == 0) && (m_aGamer[i].m_Reserved == 0) )
		{
			return i;
		}
	}

	snuError(( "snuSessionData::GetFreeGamerSlot(): Out of Gamer slots!!" ));

	return GamerIndexVoid;
}

void snuSessionData::ClearGamerSlot(snuGamer *gamer)
{
	for(int i=0; i!=maxGamerSlots; i++)
	{
		if((m_aGamer[i].m_pGamer != 0) && (m_aGamer[i].m_pGamer->GetGamerId()==gamer->GetGamerId()))
		{
			Assert(m_aGamer[i].m_pGamer == gamer);
			m_aGamer[i].m_pGamer = 0;
			m_aGamer[i].m_Reserved = 0;
			return;
		}
	}
}

void snuSessionData::SetGamerSlot(int gamerIndex, snuGamer &gamer)
{
	Assert( 0<=gamerIndex && gamerIndex<maxGamerSlots );
	Assert( m_SessionIdx != -1 );

	// Make sure we aren't snarfing someone else's slot
	if(m_aGamer[gamerIndex].m_Reserved)
	{
		Assertf(m_aGamer[gamerIndex].m_Reserved==gamer.GetPeerInfo().GetPeerId(), "snuSessionData::SetGamerSlot(): Gamer %s trying to snarf a reserved slot %d!", gamer.GetGamerInfo().GetName(), gamerIndex );
		
		// Well, I guess we just allow it
	}

	m_aGamer[gamerIndex].m_pGamer = &gamer;
	m_aGamer[gamerIndex].m_Reserved = 0;

	snuDebug1( "Session[%d]: GamerSlot[%d] Set to \"%s\"", m_SessionIdx, gamerIndex, gamer.GetName() );
}

bool snuSessionData::HandleGamerAdded( snEventAddedGamer* ag )
{
	snuGamer*		gamer		= NULL;
	rlPeerAddress	hostPeerAdd	;


	if(ag->m_GamerInfo.IsRemote())
	{
		SNUSOCKET.GetRemoteGamers()->AddGamer(ag->m_GamerInfo, ag->m_Addr, GetId());
	}

	gamer = SNUSOCKET.GetGamerFromID( ag->m_GamerInfo.GetGamerId() );

	if (!AssertVerify(gamer))
	{
		return false;
	}

	if(!AssertVerify(GetSession()->GetHostPeerAddress(&hostPeerAdd)))
	{
		return false;
	}

	if (hostPeerAdd == ag->m_GamerInfo.GetPeerInfo().GetPeerAddress())
	{
		m_pHost = gamer;
	}

	//setup data buffer for remote gamers
	if (gamer->IsRemote())
	{
		gamer->GetGamerData()->GetDataBuffer()->SetNumBitsWritten(gamer->GetGamerData()->gGetBitSize());
	}

	//get a peer Index
	int gamerIndex=GamerIndexVoid;
	for(int i=0; i!=maxGamerSlots; i++)	//check if already assigned
	{
		if((m_aGamer[i].m_pGamer != 0) && (gamer->GetGamerId()==m_aGamer[i].m_pGamer->GetGamerId()))
		{
			Assert(gamer == m_aGamer[i].m_pGamer);
			gamerIndex=i;
			break;
		}
	}
	if(gamerIndex==GamerIndexVoid)				//otherwise assign a free index
	{
		gamerIndex = GetFreeGamerSlot(ag->m_GamerInfo.GetPeerInfo().GetPeerId());
		if(gamerIndex==GamerIndexVoid)
		{
			snuError(( "peer slots full" ));
			return false;
		}
	}

	// Check to see whether a session slot index has already been assigned
	int	sessionSlotIdx = -1;
	for (int i=0; i!=SNU_MAX_SESSIONS; i++)
	{
		if (gamer->GetSessionData(i)==this)
		{
			sessionSlotIdx = i;
			break;
		}
	}
	// If none assigned, assign a free one
	if(sessionSlotIdx==-1)
	{
		sessionSlotIdx = gamer->GetFreeSessionSlot();
	}


	if (!AssertVerify(sessionSlotIdx!=-1))
	{
		snuError(( "Gamer's Session slots are full" ));
		return false;
	}
	
	snuGamer::snuSessionSlot* sessionSlot = gamer->GetSessionSlot(sessionSlotIdx);
	Assert(sessionSlot != NULL);

	// associate the peer index with the gamer
	SetGamerSlot(gamerIndex, *gamer);

    sessionSlot->m_pSessionData = this;
	sessionSlot->m_InSession	= true;
	//sessionSlot->m_PrivateSlot	= GetSession()->IsInPrivateSlot(gamer->GetGamerId());
	gamer->SetInitialized(true);

	snuDebug2( "Gamer %s: Session[%d] assigned to SessionSlot[%d]", gamer->GetName(), m_SessionIdx, sessionSlotIdx );
	if (gamer->IsDataReady())
	{
		gamer->GetGamerData()->DisplayData(3,3);
	}

	SNUSOCKET.Notify(snuSocket::snuNotifyGamersUpdated, this);

	//send a gamer added notification for listeners
	//who need to know which gamer was just added
	snuNotifyDataGamerAdded ga;
	ga.m_SessionData = this;
	ga.m_Gamer = SNUSOCKET.GetGamerFromID( ag->m_GamerInfo.GetGamerId() );
	ga.m_Addr = ag->m_Addr;
	ga.m_JoinData = ag->m_Data;
	ga.m_SizeofJoinData = ag->m_SizeofData;

	//open connections to remote gamers
	if (ga.m_Gamer->IsRemote())
	{
		snuDebug( "Opening SNU CHANNEL %d cxn to %s at [%d.%d.%d.%d:%d]", SNU_FIRST_SESSION_CHANNEL+GetId(), ga.m_Gamer->GetName(), NET_ADDR_FOR_PRINTF(ag->m_Addr) );
		SNUSOCKET.GetPostOffice()->OpenConnection( *ga.m_Gamer, SNU_FIRST_SESSION_CHANNEL+GetId(), GetId() );
	}

	SNUSOCKET.GetPostOffice()->OnGamerAdded(ga);

	//u must first wait for the data to be synced before notifying the app
	if ( !gamer->IsDataReady() || gamer->IsDead() )
	{
		//send gamer data of this session's local gamers to new member
		for(int i=0; i<maxGamerSlots; i++) 
		{
			if ((m_aGamer[i].m_pGamer != 0) && (m_aGamer[i].m_pGamer->IsLocal() && m_aGamer[i].m_pGamer->IsMember(GetSession())))
			{
				snuGamerLocal* pLocalGamer = SNUSOCKET.GetLocalGamer(m_aGamer[i].m_pGamer->GetLocalGamerIndex());

				if(AssertVerify(pLocalGamer))
				{
					pLocalGamer->SyncAllMyDataToGamer(ga.m_Gamer);
				}
			}
		}
	}
	else
	{
		SNUSOCKET.Notify(snuSocket::snuNotifyGamerAdded, &ga);
		SNUSOCKET.Notify(snuSocket::snuNotifyGamerDataUpdated, ga.m_Gamer);
	}

	return true;
}

bool snuSessionData::HandleGamerRemoved( snuGamer *gamer )
{
	if(!AssertVerify(gamer))
		return false;
	
	snuDebug( "Removing Gamer, %s...", gamer->GetName() );

	if (gamer == m_pHost)
	{
		m_pHost = NULL;
	}

	SNUSOCKET.Notify(snuSocket::snuNotifyGamerDataUpdated, gamer);

	//send a gamer removed notification for listeners
	//who need to know which gamer was just removed
	snuNotifyDataGamerRemoved gr;
	gr.m_SessionData = this;
	gr.m_Gamer = gamer;
	SNUSOCKET.Notify(snuSocket::snuNotifyGamerRemoved, &gr);

	SNUSOCKET.GetPostOffice()->OnGamerRemoved(gr);

	if( !gamer->GetKeepAlive() )
	{
		gamer->LeaveSession(this);
		ClearGamerSlot(gamer);
	}
	else
	{
		snuDebug( "Keeping gamer '%s' alive, not removed from the snu session!", gamer->GetName() );
		gamer->SetDead(true);
	}

	bool bWasLocalGamer = gamer->IsLocal();

	SNUSOCKET.GetRemoteGamers()->FreeGamer(gamer);

	//close connections for this session to remote gamers
	if (gamer->IsRemote())
	{
		snuDebug( "Closing channels to %s for session %d", gamer->GetName(), GetId() );
		SNUSOCKET.GetPostOffice()->CloseSessionChannels( *gamer, GetId() );
	}

	//terminate session if no more local members
	if (bWasLocalGamer && !IsTerminating())
	{
		bool localMembers = false;
		for (int i=0; i!=maxGamerSlots; i++)
		{
			if (GetGamer(i) && GetGamer(i)->IsLocal())
			{
				localMembers = true;
				break;
			}
		}
		if (!localMembers)
		{
			snuGamerLocal* pLocal=NULL;

			if (bWasLocalGamer)
				pLocal = dynamic_cast<snuGamerLocal*>(gamer);

			snuDebug( "No more local members, terminating session..." );
			Terminate(pLocal);
		}
	}

	SNUSOCKET.Notify(snuSocket::snuNotifyGamersUpdated, this);
	return true;
}

void snuSessionData::HandleGetGamerData(snSession* pSession, const rlGamerInfo& gamerInfo, snGetGamerData* getGamerData) const
{
	if (pSession->IsHost())
	{
		snuGamer* pGamer = SNUSOCKET.GetGamerFromID( gamerInfo.GetGamerId() );
		if(AssertVerify(pGamer))
		{
			snuNotifyDataGetGamerJoinData ggj;
			ggj.m_Gamer = pGamer;
			ggj.m_Data = getGamerData->m_Data;
			ggj.m_MaxSizeofData = sizeof(getGamerData->m_Data);
			ggj.m_SizeofData = 0;

			SNUSOCKET.Notify(snuSocket::snuNotifyGetGamerJoinData, &ggj);

			getGamerData->m_SizeofData = ggj.m_SizeofData;
		}
	}
}

bool snuSessionData::HandleJoinRequest(snSession* /*pSession*/, const rlGamerInfo& gamerInfo, snJoinRequest* joinRequest)
{
	char cResponse = SNUSOCKET.GetPostOffice()->GetJoinRequestResponse(this,gamerInfo);

	if (cResponse != 'Y')
	{
		AssertMsg(cResponse!='S' , "In Join reponse, the character S is reservered by SNU for when slots are full");

		joinRequest->m_ResponseData[0]	= cResponse;
		joinRequest->m_SizeofResponseData = 1;

		return false;
	}

	// Try to reserve a gamer slot
	int slot = ReserveGamerSlot(gamerInfo.GetPeerInfo().GetPeerId());
	if(slot == GamerIndexVoid)
	{
		// No slots, reject
		joinRequest->m_ResponseData[0]	= 'S'; // for Slot-less
		joinRequest->m_SizeofResponseData = 1;

		return false;
	}

	// Init bitbuffer to point to response data
	rage::datExportBuffer bb;
	bb.Reset();
	bb.SetReadWriteBits( joinRequest->m_ResponseData, sizeof(joinRequest->m_ResponseData)*8, 0 );

	// Export current hostdata into the response buffer, to be sent to the joining gamer
	snuHostDataSyncMsg msg;
	msg.InitForWrite( GetId(), true );
	AssertVerify( snuHostDataSyncMsg::SerMsgPayload( bb, msg ) );

	// Also export the gamer slot table, so the newbie knows who is in which slot
	GamerSlotTableMsg slotTableMsg(m_aGamer);
	AssertVerify(GamerSlotTableMsg::SerMsgPayload(bb, slotTableMsg));

	// Set size of response data
	joinRequest->m_SizeofResponseData = bb.GetNumBytesWritten();

	return true;
}

bool snuSessionData::HandleMigration(snEventSessionMigrateEnd* pHmEvent)
{
	Assert(pHmEvent);

	if(pHmEvent->m_Succeeded)
	{
		if(	GetSession()->IsHost() )
		{
			snuDebug( "Host Migration Success: We are the new session host" );

			//send copy of host data to peers, to ensure they 
			//are in sync with the new host
			if(	m_HostData && m_HostData->IsDefined() )
			{
				m_HostData->SendAllData();
				m_HostData->DisplayData(true);
				m_HostData->ClearDirtyBits();
			}
		}

		m_MigratingHost = false;

		//update the session's host pointer
		m_pHost = NULL;
		for (int i=0; i!=maxGamerSlots; i++)
		{
			if (m_aGamer[i].m_pGamer && 
				m_aGamer[i].m_pGamer->GetPeerInfo().GetPeerId() == pHmEvent->m_NewHost.GetPeerId())
			{
				m_pHost = m_aGamer[i].m_pGamer;
				break;
			}
		}

		Assertf(m_pHost, "Failed to find new host 0x%" I64FMT "x in peer array", pHmEvent->m_NewHost.GetPeerId());

		if(m_pHost)
		{
			snuNotifyDataHostMigrated hm;
			hm.m_pSessionData = this;
			hm.m_pNewHost = m_pHost;
			SNUSOCKET.Notify(snuSocket::snuNotifyHostMigrated, &hm);

			return true;
		}
	}

	snuDebug( "Host Migration Failed!" );
	SNUSOCKET.Notify(snuSocket::snuNotifyHostMigrateFailed, NULL);
	//Terminate();
	return false;
}

// Only the host can Start the session
// On Live, Starting a session (i.e. XSessionStart()) can have overhead.
bool snuSessionData::Start()
{
	Assert(!IsStarted() && !IsStarting());

	//You must be in a session
	if(!AssertVerify(IsLocallyJoined()))
		return false;

	//You must be the host
	if(!AssertVerify(IsLocallyHosted()))
		return false;

	//start me up
	if (GetSession()->Start(&m_StartStatus) == false)
	{
		//FIXME (Ali) Consider removing the terminate
		Terminate();
		return false;
	}

	return true;
}

//the opposite of Start()
bool snuSessionData::End()
{
	Assert((IsStarted() || IsStarting()) && !IsEnding());

	//You must be in a session
	if(!AssertVerify(IsLocallyJoined()))
		return false;

	//You must be the host... no you dont
	//if(!AssertVerify(IsLocallyHosted()))
	//	return false;

	if (GetSession()->End(&m_EndStatus) == false)
	{
		//FIXME (Ali) Consider removing the terminate
		Terminate();
		return false;
	}

	return true;
}


void snuSessionData::Terminate(snuGamerLocal* pOwner)
{
	if( !IsTerminating() && GetSession()->Exists() )
	{
		snuDebug( "Destroying session %d", m_SessionIdx );
		m_DestroyingSession = true;

		if (pOwner)
		{
			pOwner->DestroySession(this);
		}
		else
		{
			snuError(("Terminating an exisiting session without an instigator!"));
			GetSession()->Destroy(NULL);
		}

		//clean up any dead gamers still associated with this session
		for (int i=0; i!=maxGamerSlots; i++)
		{
			snuGamer* pGamer = GetGamer(i);
			if (pGamer && pGamer->IsDead())
			{
				snuDebug( "Session Terminate: Cleaning up dead gamer '%s'", pGamer->GetName() );
				pGamer->LeaveSession(this);
				ClearGamerSlot(pGamer);

				//He will only be freed if this is the last session he was in
				SNUSOCKET.GetRemoteGamers()->FreeGamer(pGamer);
			}
		}		
	}
}



void snuSessionData::HandleEvent(snSession* pSession, snEvent* evt)
{
	if (AssertVerify(&m_SnSession==pSession)) 
	{
		switch (evt->GetId())
		{
		case SNET_EVENT_ADDED_GAMER:
			HandleGamerAdded(evt->m_AddedGamer);
			break;

		case SNET_EVENT_REMOVED_GAMER:
			{
				snuGamer* pGamer = SNUSOCKET.GetGamerFromID( evt->m_RemovedGamer->m_GamerInfo.GetGamerId() );

				if(AssertVerify(pGamer))
					HandleGamerRemoved(pGamer);
			}
			break;

		case SNET_EVENT_SESSION_MIGRATE_START:
			m_MigratingHost = true;
			break;

		case SNET_EVENT_SESSION_MIGRATE_END:
			{
				snEventSessionMigrateEnd* pHmEvent = verify_cast< snEventSessionMigrateEnd* >(evt);
				HandleMigration(pHmEvent);
			}
			break;

		case SNET_EVENT_JOIN_FAILED:
			{
				snEventJoinFailed* pJoinFailedEvent = verify_cast< snEventJoinFailed* >(evt);
				if (pJoinFailedEvent->m_SizeofResponse)
					SNUSOCKET.Notify(snuSocket::snuNotifyRequestDenied, pJoinFailedEvent);
			}
			break;
		
		case SNET_EVENT_SESSION_JOINED:
			{
				snEventSessionJoined* pJoinEvent = verify_cast< snEventSessionJoined* >(evt);
				rage::datImportBuffer bb;
				bb.Reset();
				bb.SetReadOnlyBits( pJoinEvent->m_Data, pJoinEvent->m_SizeofData*8, 0 );

				// Import hostdata
				snuHostDataSyncMsg msg;
				if ( AssertVerify(snuHostDataSyncMsg::SerMsgPayload( bb, msg )) )
				{
					snuDebug1( "Received copy of hostdata in join response" );
					snuHostData* hostData = msg.GetHostDataPtr();
					if( hostData )
					{
						hostData->DisplayData( !msg.IsAllData() );
						SNUSOCKET.Notify(snuSocket::snuNotifyHostDataUpdated, hostData);
						hostData->ClearDirtyBits();
					}
				}

				// Import the gamer slot table, so the newbie knows who is in which slot
				GamerSlotTableMsg slotTableMsg;
				if ( AssertVerify(GamerSlotTableMsg::SerMsgPayload( bb, slotTableMsg )) )
				{
					snuDebug1( "Received copy of gamer slot table in join response" );
					slotTableMsg.Dump();

					// Inject this data into our actual gamer table
					slotTableMsg.Apply(m_aGamer);
				}

			}
			break;

		case SNET_EVENT_SESSION_STARTED:
			SNUSOCKET.Notify(snuSocket::snuNotifySessionStarted, this);
			break;

		case SNET_EVENT_SESSION_DESTROYED:
			{
				//Reset the session data before sending notification as the app 
				//may immediately wish to create a new session with the same id

				int sid = m_Id;
				Reset();
				SNUSOCKET.Notify(snuSocket::snuNotifySessionDestroyed, &sid);
			}
			break;

		default:
			break;
		};
	}
}


// Find a gamer's slot number from his snuGamer
int snuSessionData::GetGamerSlot(const snuGamer* pGamer) const
{
	for(int i=0; i<maxGamerSlots; ++i)
	{
		if( m_aGamer[i].m_pGamer == pGamer )
		{
			return i;
		}
	}

	snuError(( "snuSessionData::GetGamerSlot(): Failed to locate gamer!" ));

	return GamerIndexVoid;
}

int snuSessionData::GetHostSlot() const	
{
	//If we're not in a session, then there is no host
	if(!IsLocallyJoined())
		return -1;

	if(GetHost() == NULL)
		return -1;

	//Find the slot of the host
	for(int i=0; i<snuSessionData::maxGamerSlots; ++i)
	{
		const snuGamer* pGamer = GetGamer(i);
		if(pGamer == GetHost())
			return i;
	}

	//We should have found a slot for the host...
	Assert(false);
	return -1;
}

void snuSessionData::DumpInfo(fiStream *pOptionalFile) const
{
	if (pOptionalFile)
	{
		fprintf(pOptionalFile, "===================\r\n");
		fprintf(pOptionalFile, "Session Gamer Slots\r\n");
		fprintf(pOptionalFile, "===================\r\n");
	}
	else
	{
		snuDebug1("Session Gamer Slots");
		snuDebug1("===================");
	}
	for(int i=0; i<maxGamerSlots; ++i)
	{
		if(m_aGamer[i].m_pGamer)
		{			
			if (pOptionalFile)
			{
				fprintf(pOptionalFile, "[%d] %s 0x%"I64FMT"x\r\n", i, m_aGamer[i].m_pGamer->GetGamerInfo().GetName(), m_aGamer[i].m_pGamer->GetGamerInfo().GetPeerInfo().GetPeerId());
			}
			else
			{
				snuDebug1("[%d] %s 0x%"I64FMT"x", i, m_aGamer[i].m_pGamer->GetGamerInfo().GetName(), m_aGamer[i].m_pGamer->GetGamerInfo().GetPeerInfo().GetPeerId());
			}
		}
		else if(m_aGamer[i].m_Reserved)
		{
			if (pOptionalFile)
			{
				fprintf(pOptionalFile, "[%d] RESERVED 0x%"I64FMT"x\r\n", i, m_aGamer[i].m_Reserved);
			}
			else
			{
				snuDebug1("[%d] RESERVED 0x%"I64FMT"x", i, m_aGamer[i].m_Reserved);
			}
		}
	}

	if (pOptionalFile)
	{
		fprintf(pOptionalFile, "===================\r\n");
	}
	else
	{
		snuDebug1("===========");
	}
}


///////////// snuGamerInfoQueue /////////////////////////////

snuGamerInfoQueue::snuGamerInfoQueue()
{
    Clear();
}

snuGamerInfoQueue::~snuGamerInfoQueue()
{
    Clear();
}

void 
snuGamerInfoQueue::Clear()
{
    while(m_Gamers.GetCount())
    {
        m_Gamers.Pop().Clear();
    }
    m_Gamers.Reset();
}

void 
snuGamerInfoQueue::AddGamer(const rlGamerInfo& info)
{
    //if gamer already exists, delete the old copy so he gets moved to the front
    int gamerIndex = FindGamerIndex(info);
    
    if(gamerIndex >= 0)
    {
        m_Gamers.Delete(gamerIndex);
    }

    //drop oldest gamer if history fills up
    if(m_Gamers.IsFull())
    {
        m_Gamers.Pop().Clear();
    }

    //add to end of queue
    m_Gamers.Append() = info;
}

int 
snuGamerInfoQueue::GetNumGamers() const
{
    return m_Gamers.GetCount();
}

const rlGamerInfo* 
snuGamerInfoQueue::GetGamer(int n) const
{
    Assert(n >= 0 && n < GetNumGamers());
    
    if(n >= 0 && n < GetNumGamers())
    {
        return &m_Gamers[n];
    }

    return 0;
}

int 
snuGamerInfoQueue::FindGamerIndex(const rlGamerInfo& info) const
{
    for(int i = 0; i < m_Gamers.GetCount(); ++i)
    {
        if(m_Gamers[i] == info)
        {
            return i;
        }
    }
    return -1;
}

