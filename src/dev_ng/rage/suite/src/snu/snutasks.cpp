// 
// snu/snutasks.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "snu/snutasks.h"

#include "net/transaction.h"
#include "net/tunneler.h"
#include "rline/rlpeeraddress.h"

#include "snu/snudiag.h"
#include "snu/snuconfig.h"
#include "snu/snusocket.h"
#include "snu/snupostoffice.h"

#if __LIVE
#include "system/xtl.h"
#elif __PPU
#elif __GAMESPY
#include "rline/rlgamespytitleid.h"
#include "rline/rlgamespy.h"
#endif

namespace rage {

//============================================= snuTransactionTask ==========================================================

NET_MESSAGE_IMPL( snuGroupJoinRequestTask::snuMsgGroupJoinResponse );
NET_MESSAGE_IMPL( snuGroupJoinRequestTask::snuMsgGroupJoinRequest );
NET_MESSAGE_IMPL( snuGroupJoinRequestTask::snuMsgGroupJoinCommand );

bool snuTransactionTask::Configure
(
	const rlSessionInfo& sessionInfo,
	const rlNetworkMode netMode,
	const unsigned channelId,
	const snuMessage* request,
	snuMessage* response
)
{
	Assert(sessionInfo.GetHostPeerAddress().IsValid());

	snuDebug2("snuTransactionTask(%s, %s): Configuring", 
			 request->GetMsgName(),
			 response->GetMsgName());

	bool success = false;
	netConnectionManager* pCxn = SNUSOCKET.GetPostOffice()->GetConnectionManager();

	m_ChannelId = channelId;
	m_Request = request;
	m_Response = response;
	m_NetAddr.Clear();
	m_SessionInfo = sessionInfo;
	m_Transactor.Init(pCxn);
	m_RequestCount = 0;

#if __LIVE
	const XSESSION_INFO& xsinfo = (const XSESSION_INFO&) sessionInfo;
	XNetRegisterKey(&xsinfo.sessionID, &xsinfo.keyExchangeKey);
	netTunnelSecurityParams secParams(sessionInfo.GetToken());
#elif __PPU
	netTunnelSecurityParams secParams;
#elif __GAMESPY
	const rlGamespyTitleId &titleId = rlGamespy::GetTitleId();
	netTunnelSecurityParams secParams(titleId.GetGameName(), titleId.GetSecretKey());
#endif

	if(AssertVerify(pCxn->OpenTunnel(sessionInfo.GetHostPeerAddress(),
									secParams,
									(RL_NETMODE_LAN == netMode) ? NET_TUNNELTYPE_LAN : NET_TUNNELTYPE_ONLINE,
									&m_TunnelRqst,
									&m_MyStatus)))
	{
		m_State = STATE_WAIT_FOR_TUNNEL;
		success = true;
	}
	else
	{
		snuError(("Failed to open tunnel with peer; possible cause is cookie already in use"));
	}

	return success;
}

void snuTransactionTask::Start()
{
    snuDebug2("snuTransactionTask(%s, %s): Starting", 
             m_Request->GetMsgName(),
             m_Response->GetMsgName());

    rlTaskBase::Start();
}

void snuTransactionTask::Finish(const FinishType finishType, const int resultCode)
{
    snuDebug2("snuTransactionTask(%s, %s): %s", 
             m_Request->GetMsgName(),
             m_Response->GetMsgName(),
             FinishString(finishType));

    if(!FinishSucceeded(finishType))
    {
        AssertMsg(false, "FIX THIS!!!");
        //m_Ctx->CancelTunnelRequest(&m_TunnelRqst);
    }

    if(m_RespHandler.Pending())
    {
        m_RespHandler.Cancel();
    }

    m_Transactor.Shutdown();

#if __LIVE
	if (m_SessionInfo.GetHostPeerAddress().IsValid())
	{
		const XSESSION_INFO& xsinfo = (const XSESSION_INFO&) m_SessionInfo;
		XNetUnregisterKey(&xsinfo.sessionID);
	}
#endif

    rlTaskBase::Finish(finishType, resultCode);
}

void snuTransactionTask::Update(const unsigned timeStep)
{
    rlTaskBase::Update(timeStep);

    m_Transactor.Update(timeStep);

    if(IsActive())
    {
        switch(m_State)
        {
        case STATE_WAIT_FOR_TUNNEL:
            if(m_MyStatus.Succeeded())
            {
                snuDebug2("snuTransactionTask(%s, %s): Tunnel opened", 
                         m_Request->GetMsgName(),
                         m_Response->GetMsgName());

                m_NetAddr = m_TunnelRqst.GetNetAddress();
                m_State = STATE_SEND_REQUEST;
            }
            else if(!m_MyStatus.Pending())
            {
                snuError(("Failed to negotiate NAT"));

                //Generate fake IPs in the range 127.0.0.4 to 127.255.255.255
                static unsigned s_NextFakeIp = 0;
                const unsigned fakeIp = 0x7F000010 | (s_NextFakeIp & 0x00FFFFFF);

                ++s_NextFakeIp;

                m_NetAddr.Init(fakeIp, 0xFFFF);
                m_State = STATE_SEND_REQUEST;
            }
            break;

        case STATE_SEND_REQUEST:
            if(!m_RespHandler.Pending())
            {
                if(m_RequestCount < MAX_ATTEMPTS)
                {
                    if(!this->SendRequest())
                    {
                        this->Finish(FINISH_FAILED);
                    }
                }
                else
                {
					snuDebug2("snuTransactionTask(%s, %s): Max attempts failed", 
                             m_Request->GetMsgName(),
                             m_Response->GetMsgName());
                    this->Finish(FINISH_FAILED);
                }
            }
        }
    }
}

bool snuTransactionTask::SendRequest()
{
    bool success = false;

    if(m_RequestCount)
    {
        success = m_Transactor.ResendRequest(m_NetAddr,
                                             m_ChannelId,
                                             m_TxId,
                                             *m_Request,
                                             QUERY_TIMEOUT,
                                             &m_RespHandler);
    }
    else
    {
        success = m_Transactor.SendRequest(m_NetAddr,
                                           m_ChannelId,
                                           &m_TxId,
                                           *m_Request,
                                           QUERY_TIMEOUT,
                                           &m_RespHandler);
    }

    ++m_RequestCount;

    if(success)
    {
        snuDebug2("snuTransactionTask(%s, %s): Sent request (%d of %d tries)", 
                 m_Request->GetMsgName(),
                 m_Response->GetMsgName(),
                 m_RequestCount,
                 MAX_ATTEMPTS);
    }

    return success;
}

void snuTransactionTask::OnResponse(netTransactor* /*transactor*/, netResponseHandler* /*handler*/, const netResponse* resp)
{
    snuDebug2("snuTransactionTask(%s, %s): Received response", 
             m_Request->GetMsgName(),
             m_Response->GetMsgName());

    if(resp->Complete() && resp->Answered())
    {
        if(!AssertVerify(m_Response->Import(resp->m_Data, resp->m_SizeofData)))
        {
            snuError("Failed to import %s", m_Response->GetMsgName());
            Finish(FINISH_FAILED);
        }
        else
        {
            snuDebug2("snuTransactionTask(%s, %s): Successfully imported response", 
                     m_Request->GetMsgName(),
                     m_Response->GetMsgName());
            Finish(FINISH_SUCCEEDED);
        }
    }
    else
    {
        snuDebug2("%s %d.%d.%d.%d:%d timed out or failed",
                 m_Request->GetMsgName(),
                 NET_ADDR_FOR_PRINTF(resp->m_TxInfo.m_Addr));
        Finish(FINISH_FAILED);
    }
}

//============================================ snuGroupJoinRequestTask =========================================================

bool snuGroupJoinRequestTask::Configure
(
	const rlGamerHandle*			pRequestor, 
	const rlSessionSearchResult*	pResult,
	int								nNumJoiners
)
{
	Assert(pRequestor);
	Assert(pResult);

	m_pRequestor = pRequestor;
	m_NumJoiners = nNumJoiners;

	m_RequestMsg.Reset(*pRequestor,nNumJoiners,pResult->m_SessionInfo);

	bool success =
        rlTaskBase::Configure((snuTransactionTask*)this,
		                    pResult->m_SessionInfo,
		                    pResult->m_NetMode,
		                    (unsigned) SNU_FIRST_SESSION_CHANNEL, //FIXME (Ali) 
		                    &m_RequestMsg,&m_ResponseMsg,
		                    &m_TransactionStatus);

	return success;
}

bool snuGroupJoinRequestTask::GetResponse(snuGroupJoinResponseData& data)
{
	if (m_TransactionStatus.Succeeded())
	{
		data.m_SessionInfo	= m_ResponseMsg.m_SessionInfo;
		data.m_SessionConfig= m_ResponseMsg.m_Config;
		data.m_SlotsReserved= m_ResponseMsg.m_NumJoiners;
		return true;
	}

	return false;
}


}//namespace rage
