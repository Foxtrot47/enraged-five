// 
// snu/snubitbuffer.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNU_SNUBITBUFFER_H
#define SNU_SNUBITBUFFER_H

#include "data\bitbuffer.h"
#include "net\message.h"

class snuBitBuffer  : public rage::datBitBuffer
{
public:
    enum
    {
        MAX_SIZEOF_DATA     = 256+64,
        MAX_BITSIZEOF_DATA  = MAX_SIZEOF_DATA << 3,
    };

    snuBitBuffer()
    {
        this->Reset();
    }

    snuBitBuffer(const snuBitBuffer& that)
    {
        *this = that;
    }

    snuBitBuffer& operator=(const snuBitBuffer& that)
    {
        if(this != &that)
        {
            this->Reset(&that);
        }

        return *this;
    }

    void Reset()
    {
        this->SetReadWriteBytes(m_Data, sizeof(m_Data));
    }

    void Reset(const snuBitBuffer* appData)
    {
        this->Reset();

        if(appData
            && appData->GetBitLength()
            && AssertVerify(this->WriteBits(appData->GetReadOnlyBits(),
            appData->GetBitLength(),
            appData->GetBaseBitOffset())))
        {
            this->SetCursorPos(0);
        }
    }

    //Import
    bool Ser(const rage::datImportBuffer& bb)
    {
        this->Reset();

        unsigned myBitSize = 0;

        const bool success =
            AssertVerify(bb.ReadUns(myBitSize, rage::datBitsNeeded< MAX_BITSIZEOF_DATA >::COUNT))
            && AssertVerify(myBitSize <= MAX_BITSIZEOF_DATA)
            && AssertVerify(this->CanWriteBits(myBitSize))
            && myBitSize ? AssertVerify(bb.ReadBits(this->GetReadWriteBits(),
            myBitSize,
            this->GetBaseBitOffset())) : true;

        if(success)
        {
            this->SetNumBitsWritten(myBitSize);
            this->SetCursorPos(0);
        }

        return success;
    }

    //Export
    bool Ser(rage::datExportBuffer& bb) const
    {
        const unsigned myBitSize = this->GetBitLength();

        Assert(myBitSize <= MAX_BITSIZEOF_DATA);

        return AssertVerify(bb.WriteUns(myBitSize, rage::datBitsNeeded< MAX_BITSIZEOF_DATA >::COUNT))
               && myBitSize ? AssertVerify(bb.WriteBits(this->GetReadOnlyBits(),
               myBitSize,
               this->GetBaseBitOffset())) : true;
    }

private:
    rage::u8 m_Data[MAX_SIZEOF_DATA];
};

#endif //SNU_SNUBITBUFFER_H

