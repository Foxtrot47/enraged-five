#ifndef SNU_SNUGAMER_H
#define SNU_SNUGAMER_H

#include "snet/session.h"

#include "atl/pool.h"
#include "atl/array.h"
#include "rline/rlfriendsreader.h"
#include "rline/rlpresence.h"
#include "rline/rlfriendsreader.h"
#include "rline/rlachievement.h"
#include "rline/rltitlestorage.h"


#include "snubitbuffer.h"
#include "snuconfig.h"

class	snuSessionData;
struct	snuGamerJoinWish;
class	snuFriendInvites;
class	snuInvite;
struct	snuGamerDataDef;

namespace rage 
{ 
	class rlStats; 
	class rlLeaderboard;
	class rlLeaderboardInfo;
}

//==============================================================================================================================
class snuGamer
//==============================================================================================================================
{
	

	friend class snuGamerPool;
	friend class snuSessionData;
	friend class snuSocket;

public:

	struct snuSessionSlot
	{
		snuSessionData*	m_pSessionData;
		bool			m_PrivateSlot;
		bool			m_InSession;
		bool			m_Quitter;

		snuSessionSlot() 
			: 
			m_pSessionData(NULL)
			//, m_PrivateSlot(false)
			,m_InSession(false)
		{
		}

		void Reset()		
		{
			//m_PrivateSlot	= false; 
			m_InSession		= false; 
			m_Quitter		= false; 
			m_pSessionData	= NULL;
		}
	};

	class snuGamerMuteMsg
	{
	public:
		NET_MESSAGE_DECL( snuGamerMuteMsg );
		NET_MESSAGE_SER( UNUSED_PARAM(bb), UNUSED_PARAM(msg) )
		{
			return	true;
		}
		snuGamerMuteMsg()	{}
	};

	class snuGamerUnmuteMsg
	{
	public:
		NET_MESSAGE_DECL( snuGamerUnmuteMsg );
		NET_MESSAGE_SER( UNUSED_PARAM(bb), UNUSED_PARAM(msg) )
		{
			return	true;
		}
		snuGamerUnmuteMsg()	{}
	};

	class snuGamerData
	{
		friend class snuGamer;
		friend class snuGamerLocal;

	public:
		enum {maxGamerVariables = 32}; //TODO: think about lowering to 16

		class snuGamerDataSyncMsg
		{
		public:
			NET_MESSAGE_DECL( snuGamerDataSyncMsg );
			NET_MESSAGE_SER( bb, msg )
			{
				return	msg.m_SyncBuffer.Ser( bb )						&&
					bb.SerBytes(&msg.m_GamerID, sizeof(msg.m_GamerID))	&&
					bb.SerBytes(&msg.m_IsFieldPresent, sizeof(msg.m_IsFieldPresent));
			}

			snuGamerDataSyncMsg(){}

			const rage::rlGamerId&	GetGamerID				()						const	{ return m_GamerID; }
			void 					WriteGamerData			(snuGamer&)				const	;
			void 					ReadGamerData			(const snuGamer&, int fieldID)	;
			void 					ReadDirtyGamerData		(const snuGamer&)				;
			void 					ReadSyncableGamerData	(const snuGamer&)				;

		protected:

			snuGamerData* GetGamerDataPtr() const;

			rage::rlGamerId	m_GamerID;			//TODO: make this smaller
			snuBitBuffer	m_SyncBuffer;
			unsigned int	m_IsFieldPresent;
		};

		snuBitBuffer*				GetDataBuffer()			{return &m_Data;}
		const rage::datBitBuffer*	GetDataBuffer() const	{return &m_Data;}

	protected:
		snuBitBuffer							m_Data;
		unsigned int							m_IsFieldDirty : maxGamerVariables;  // dirty = data not yet synced with host
		mutable rage::sysCriticalSectionToken	m_CsToken;

		static int								g_aBitOffset[maxGamerVariables+1];
		static const snuGamerDataDef* 			g_aDataDef;
		static int								gDataCount;

	public:

		static int			gGetDataIndexByName	(const char *name)		;
		static int			gGetBitSize			(const int i)			;
		static int			gGetBitSize			()						{ return g_aBitOffset[gDataCount];		}
		static int			gGetDataCount		()						{ return gDataCount;					}
		static bool			gIsSigned			(const int i)			;
		static const char*	gGetDataName		(const int i)			;
		static bool			gIsSyncable			(const int i)			;
		
		static bool gInitialise	(const snuGamerDataDef[], int dataCount);
		void		DisplayData	(int minDebugLevelDirty, int minDebugLevelClean) const	;

		snuGamerData();
		int			GetData			(const int i, void *d)		const	;
		int			SetData			(const int i, const void *v)		;
		int			GetDataInt		(const int i)				const	;
		int			SetDataInt		(const int i, const int v)			;
		unsigned	GetDataUns		(const int i)				const	;
		int			SetDataUns		(const int i, const unsigned v)		;
		void		Clear			()									;
		void		SetFieldDirty	(const int idx)						{ m_IsFieldDirty |= 1<<idx;				}
		bool		IsFieldDirty	(const int idx)				const	{ return (m_IsFieldDirty & 1<<idx) > 0;	}
		bool		IsDirty			()							const	{ return m_IsFieldDirty!=0;				}
		void		CleanDirtyFlags	()									{ m_IsFieldDirty=0;						}
	};

protected:

	rage::netAddress	m_aAddr[SNU_MAX_SESSIONS];
	rage::rlGamerInfo	m_GamerInfo;
	snuGamerData		m_GamerData;
	snuSessionSlot		m_SessionSlot[SNU_MAX_SESSIONS];
	int					m_HitCount;
	bool				m_IsReady;
	bool				m_KeepAlive;
	bool				m_Dead;
	bool				m_IsInitialized;
	bool				m_IsDataReady;
	bool				m_IsMuted;		//local gamer muted this gamer
	bool				m_IsMutedBy;	//local gamer was muted by this gamer

	snuGamerData*		GetGamerData	()				{ return &m_GamerData;						}
	snuSessionData*		GetSessionData	(int i)	const	{ return m_SessionSlot[i].m_pSessionData;	}

public:

	snuGamer();
	virtual ~snuGamer() {};

	const rage::rlGamerInfo&  GetGamerInfo	() const {return m_GamerInfo;}
	const rage::netAddress&   GetNetAddress	(int i) const {return m_aAddr[i];}
	const char*			GetName				() 							const	{ return m_GamerInfo.GetName(); 						}
	const rage::rlGamerHandle&GetGamerHandle		() 							const	{ return m_GamerInfo.GetGamerHandle(); 					}
	const rage::rlGamerId&    GetGamerId			() 							const	{ return m_GamerInfo.GetGamerId(); 						}

	bool 				InitGamerInfo   	(const rage::rlGamerInfo& gamerInfo)		{ m_GamerInfo = gamerInfo; return true;					}
	void 				Shutdown			()									{ this->Clear();										}
	bool 				IsSignedIn			() 							const 	{ return m_GamerInfo.IsSignedIn();						}
	bool 				IsOnline			() 							const 	{ return m_GamerInfo.IsOnline();						}
	bool 				IsLocal				() 							const 	{ return m_GamerInfo.IsLocal(); 						}
	bool 				IsRemote			() 							const 	{ return m_GamerInfo.IsRemote();						}
	bool 				IsMember			( const rage::snSession* session )const	{ return session->IsMember(m_GamerInfo.GetGamerHandle());	}
	int					GetLocalGamerIndex	()							const	{ return m_GamerInfo.GetLocalIndex();					}
	//void 				AddNotifyHandler	( rlGamer::NotifyHandler* handler ) { m_SnGamer.AddNotifyHandler( handler );				}
	//void 				RemoveNotifyHandler	( rlGamer::NotifyHandler* handler ) { m_SnGamer.RemoveNotifyHandler( handler ); 			}
	const rage::rlPeerInfo&	GetPeerInfo	()									const	{ return m_GamerInfo.GetPeerInfo(); }

	const snuGamerData*	GetGamerData		()							const	{ return &m_GamerData;									}
	void				Clear				()									;
	rage::snSession*			GetMySession		(int i)						const	;
	snuSessionSlot*		GetSessionSlot		(int i)								{ return &m_SessionSlot[i];								}
	int					GetSessionCount		()							const	;
	int					GetFreeSessionSlot	()							const	;
	bool				IsInitialized		()							const	{ return m_IsInitialized;								}
	void				SetInitialized		(bool flag)							{ m_IsInitialized = flag;								}
	void				Update				()									;
	virtual void		LeaveSession		(snuSessionData*, rage::netStatus* s=NULL);
	bool				GetKeepAlive		()							const	{ return m_KeepAlive;									}
	void				SetKeepAlive		(bool b)							{ m_KeepAlive = b;										}
	bool				IsDead				()							const	{ return m_Dead;										}
	void				SetDead				(bool b)							{ m_Dead = b;											}
	bool				IsMuted				()							const	{ return m_IsMuted;										}
	void				SetMuted			(bool b);
	bool				IsMutedBy			()							const	{ return m_IsMutedBy;									}
	void				SetMutedBy			(bool b)							{ m_IsMutedBy = b;										}

	int					GetDataInt			(const int i)				const	{ return AssertVerify(IsDataReady() || !m_GamerData.gIsSyncable(i)) ? m_GamerData.GetDataInt(i) : 0; }
	unsigned			GetDataUns			(const int i)				const	{ return AssertVerify(IsDataReady() || !m_GamerData.gIsSyncable(i)) ? m_GamerData.GetDataUns(i) : 0; }
	int					GetData				(const int i, void *d)		const	{ return AssertVerify(IsDataReady() || !m_GamerData.gIsSyncable(i)) ? m_GamerData.GetData(i,d)	: 0; }
	int					SetDataInt			(const int i, const int v)			;
	int					SetDataUns			(const int i, const unsigned v)		;
	int					SetData				(const int i, const void *v)		;
	bool				IsDataDirty			(const int i)				const	{ return m_GamerData.IsFieldDirty(i);					}
	bool 				IsDataReady			() 							const 	{ return IsInitialized()&&(m_IsDataReady||IsLocal());	}
	void				CleanDirtyDataFlags	()									{ m_GamerData.CleanDirtyFlags();						}
	void				InitGamerData		()									{ ::new(&m_GamerData)snuGamerData();					}
};
//==============================================================================================================================



//==============================================================================================================================
class snuGamerLocal : public snuGamer
//==============================================================================================================================
{
	friend class snuSocket;

	enum
	{
		snuMaxFriends		= 50,
		snuAchievementCount	= 29,	// must match schema
	};

protected:

	//tasking
	rage::netStatus				m_CurrentTask;
	rage::netStatus				m_PresenceTask;
	snuSessionData*				m_pSessionJoining;
	snuGamerJoinWish			m_CurrentJoinWish;

	//community
	unsigned					m_FriendCount;
	rage::rlFriend				m_aFriend[snuMaxFriends];
	bool						m_FriendsToRead;
	rage::rlFriendsReader		m_FriendsReader;
	rage::netStatus				m_FriendsReaderStatus;
	rage::netStatus				m_FriendTask;
	snuFriendInvites*			m_pFriendInvites;


	//Achievements
	rage::atArray<int>			m_AchievementsToWrite;
	rage::rlAchievement			m_rlAchievement;
	rage::netStatus				m_AchievementStatus;

	//leader boards
	rage::rlStats*				m_pStats;

	//Title storage
	rage::rlTitleStorage		m_TitleStorage;

	//Session Allocator
	snuSessionData* NewSession	(const snuSessionId& sid, const rage::rlSessionInfo*);

public:

	snuGamerLocal(); 
	~snuGamerLocal();

	//life cycle
	bool Init	(const rage::rlGamerInfo&);
	void UnInit	();
	void Update	();
	void Clear	(){ snuGamer::Clear(); m_IsDataReady=true; }

	//sessions
	bool JoinSession	(const snuSessionId&, const snuGamerJoinWish&);
	bool HostSession	(const snuSessionId&, const rage::rlMatchingAttributes&, bool sysLink, bool bAllowJoinInProgress);
	void LeaveMySession	(int sessionIdx);
	void LeaveSession	(snuSessionData*,rage::netStatus*s=NULL);
	bool DestroySession	(snuSessionData*,rage::netStatus*s=NULL);

	//gamer data
	bool SyncAllMyDataToGamer(const snuGamer*);
	bool SynchroniseMyData	();

	//friends
	void 					RefreshFriends	()									;
	const rage::rlFriend*	GetFriend		(int idx)					const	{ return m_aFriend+idx;		}
	const rage::rlFriend*	GetFriend		(const rage::rlGamerHandle&)const	;
	bool					AddFriend		(const rage::rlGamerHandle&)		;
	int						GetFriendCount	()							const	{ return m_FriendCount;		}

	//invites
	const snuFriendInvites*	GetInvitations	()							const	{ return m_pFriendInvites;	}
	snuFriendInvites*		GetInvitations	()									{ return m_pFriendInvites;	}
	bool					AcceptInvite	(const snuInvite*)					;

	//Achievements
	void AwardAchievement( const int achievementId);
	bool ViewAchievements(const rage::rlGamerInfo& gamerInfo, rage::rlAchievementInfo* achievements,
		const int maxAchievements,int* numAchievements,rage::netStatus* status);

	//Leaderboards
	//PURPOSE
	//  Read rows from a leaderboard corresponding to specific gamers.
	//PARAMS
	//  columnIds       - Array of column ids specifying which columns
	//                    to read.
	//  numColumns      - Number of columns to read from the leaderboard.
	//  leaderboard     - Instance of a concrete leaderboard. 
	//                    The ID of this leaderboard indicates which leaderboard to read from.
	//  gamerInfos      - Array of gamer infos for whom stats will be read.
	//  numGamers       - Number of gamers in the gamerInfos array.
	//  status          - Optional status object that can be polled for completion.
	//NOTES
	//	See documentation for rlStats.
	//
	//  This is an asynchronous operation.  It is not complete until the
	//  status object's Pending() function returns false.
	bool ReadStatsByGamer		(rage::rlLeaderboard*, const rage::rlGamerHandle*, const int numGamers,	rage::netStatus*);
	bool ReadStatsByRank		(rage::rlLeaderboard*, const unsigned startRank,						rage::netStatus*);
	bool GetLeaderboardSize		(const rage::rlLeaderboardInfo* lbInfo, unsigned* numRows, rage::netStatus* status);

	// Title storage
	bool DownloadFile( const char* filename, char* buf, const unsigned bufSize, unsigned *downloadSize, rage::netStatus* status );


	/*bool SendMessage
	(
	const netMessage&, const unsigned flags, 
	const int gamerSessionSlot=-1, const int sessionGamerSlot=-1, netSequence* s=NULL
	);*/

	void				ShowSignInUI			(bool b_OnlineNeeded)								;
	bool				ShowGamerCardUI			(const rage::rlGamerHandle*)const					;
	bool				ShowGamerReviewUI		(const rage::rlGamerHandle*)const					;
	const rage::netStatus&	GetNetStatus			()							const					{ return this->m_CurrentTask; }
	rage::netStatus&	GetPresenceStatus		()													{ return m_PresenceTask; }
	void				HandlePresenceEvent		(const rage::rlPresenceEvent*)						;

	//PURPOSE
	//  Returns true if the gamer is permitted to host/join multi-player sessions.
	bool HasMultiplayerPrivileges() const;

	//PURPOSE
	//  Returns true if the gamer is permitted to voice/text chat in multi-player sessions.
	bool HasChatPrivileges(bool bFriendsOnly) const;

	//PURPOSE
	//  Returns true if the gamer is permitted to view and play user created content 
	//  with no restrictions.
	bool HasUserContentPrivileges() const;

	//PURPOSE
	//  Returns true if the gamer is permitted to view and play user created content 
	//  with just his friends.
	bool HasUserContentPrivilegesFriendsOnly() const;

	//PURPOSE
	//  Returns true if the gamer is a friend of the other gamer.
	bool IsFriendsWith(const snuGamer& otherGamer) const;

	//PURPOSE
	//  Get the counts of available downloadable content (content you could download, not necissary content you have already grabbed)
	void GetContentCounts( int & numNewContent, int & numExistingContent ) const;

};
//==============================================================================================================================


//==============================================================================================================================
class snuGamerPool
//==============================================================================================================================
{
	friend class snuSocket;
	friend class snuSessionData;

	int							m_MaxGamers;
	rage::atPool< snuGamer >*	m_Pool;				// preallocated storage for gamers
	rage::atArray< snuGamer* >	m_ActiveList;		// list of allocated gamers (from the pool)

	snuGamer*	AddGamer( const rage::rlGamerInfo& gamerInfo, const rage::netAddress& addr, int sessionId );
	void		FreeGamer( snuGamer* gamer );

	void Init(const int maxGamers)
	{
		if (AssertVerify(!m_Pool))
		{
			m_Pool = rage_new rage::atPool< snuGamer >( ( rage::u16 ) maxGamers );
			m_MaxGamers	= maxGamers;
			m_ActiveList.Reserve( ( rage::u32 ) maxGamers );
		}
	}

	void Shutdown()
	{
		if( m_Pool )
		{
			delete m_Pool;
			m_Pool = 0;
		}

		m_ActiveList.Reset();
		m_MaxGamers = 0;
	}

	snuGamerPool ()
		: m_MaxGamers	(0)
		, m_Pool		(NULL)
	{
	}
};
//==============================================================================================================================


struct snuGamerDataDef
{
	typedef void (*PrintDataToString)(const snuGamer::snuGamerData& rData, rage::u32 idx, char* buf, rage::u32 bufSize);

	const char*	m_szName;	
	int			m_BitSize;
	snuSyncSign	m_Sign;
	snuSyncType	m_SyncType;

#if !__NO_OUTPUT
	PrintDataToString m_PrinterFunc;
#endif
};


#endif //SNU_SNUGAMER_H

