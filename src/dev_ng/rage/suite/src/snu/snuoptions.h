// 
// snu\snuoptions.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNU_OPTIONS_H
#define SNU_OPTIONS_H

#include "snuconfig.h"

namespace rage
{
class rlMatchingAttributes;
class rlMatchingFilter;
};

struct snuUserOption
{
    int GetByName(const char*) const;
    int GetValue() const { return m_Value; }
    int GetID()    const { return m_ID; }
    int IsSet()    const { return GetValue()!=-1; }

    bool SetByName(const char*);
    void SetValue(int newValue) { m_Value = newValue; }

    int         m_ID;
    const char* m_Name;
    const char* m_aValueName[SNU_MAX_CHOICES_PER_USER_OPTION];
    int         m_Value;
};

class snuUserOptions
{
public:
    bool Init(snuUserOption aUserOption[]);

    int    GetOptionIndex(const char*) const;
    const snuUserOption& GetOption(int idx) const { return m_aOption[idx]; }
    snuUserOption& GetOption(int idx) { return m_aOption[idx]; }

    bool SetOption(const char* option, const char* v);
    bool SetOptionByValue(const int optionIdx, const int v);
    bool SetOptionByIndex(const int optionIdx, const int v);


	void					DisplayAttributeData	( const rage::rlMatchingAttributes& )const	;
	void					DisplayFilterData		( const rage::rlMatchingFilter&	) const;
	void					CopyToAttrib		( rage::rlMatchingAttributes& )		const	;
	void					CopyToFilter		( rage::rlMatchingFilter& )			const	;
	void					ClearValues			( void )									;


private:
    snuUserOption* m_aOption;
};

#endif // SNU_OPTIONS_H
