// 
// snu/snutasks.h
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNUTASKS_H
#define SNUTASKS_H

#include "snu/snuconfig.h"

#include "net/status.h"
#include "net/transaction.h"
#include "net/connectionmanager.h"
#include "rline/rl.h"
#include "rline/rlpeeraddress.h"
#include "rline/rltask.h"
#include "rline/rlsession.h"

namespace rage {

class rlSessionSearchResult;

// Call Configure(), giving it the snet channel ID, message to send (T_REQUEST), and message to hold response (T_RESPONSE)
// Call Update() once per frame 
// The netStatus object you pass in tells when finished, at which point response message will be filled in
class snuTransactionTask : public rlTaskBase
{
public:
    snuTransactionTask()
    : m_State		(STATE_INVALID)
    , m_TxId		(NET_INVALID_TRANSACTION_ID)
    , m_RequestCount(0)
    {
        m_RespHandler.Bind(this, &snuTransactionTask::OnResponse);
    }

    virtual ~snuTransactionTask() {}

    virtual const char* GetTaskName() const { return "snuTransactionTask"; }

    bool Configure(const rlSessionInfo& sessionInfo,
                   const rlNetworkMode netMode,
                   const unsigned channelId,
                   const snuMessage* request,
                   snuMessage* response);

    virtual void Start();
    virtual void Finish(const FinishType finishType, const int resultCode = 0);
    virtual void Update(const unsigned timeStep);

private:
    bool SendRequest();
    void OnResponse(netTransactor*, netResponseHandler*, const netResponse*);

    enum
    {
        QUERY_TIMEOUT   = 3000,
        MAX_ATTEMPTS    = 10
    };

    enum State
    {
        STATE_INVALID = -1,
        STATE_WAIT_FOR_TUNNEL,
        STATE_SEND_REQUEST,

        NUM_STATES
    };

    int m_State;

    const snuMessage* m_Request;
    snuMessage* m_Response;

    netTunnelRequest m_TunnelRqst;
    netAddress m_NetAddr;
	rlSessionInfo m_SessionInfo;
    unsigned m_ChannelId;

    netTransactor m_Transactor; 
    unsigned m_RequestCount;
    unsigned m_TxId;
    netResponseHandler m_RespHandler;

    netStatus m_MyStatus;
};

//PURPOSE
//  Used by a host to request a group for it's session members
class snuGroupJoinRequestTask : private snuTransactionTask
{
    friend class rlTaskBase;

public:
	snuGroupJoinRequestTask()
	: snuTransactionTask	()
	, m_NumJoiners			(-1)
	, m_pRequestor			(NULL)
	, m_TransactionStatus	()
	{
	}

	struct snuGroupJoinResponseData : public snuNotifyBase
	{
		int						m_SlotsReserved;
		rlSessionInfo			m_SessionInfo;
		rlSession::ConfigParams	m_SessionConfig;
	};

	bool Configure(const rlGamerHandle* pRequestor, 
		const rlSessionSearchResult*,
		int nNumJoiners);

	bool	IsBusy		()							{ return m_TransactionStatus.Pending();}
	void	Start		()							{ snuTransactionTask::Start();			}
	void	Update		(const unsigned timeStep)	{ snuTransactionTask::Update(timeStep); }
	bool	GetResponse	(snuGroupJoinResponseData&)	;

	//PURPOSE
	//  Sent to a session host to request a group join
	//NOTES
	//  H->H:       YES
	//  P<->H:      NO
	struct snuMsgGroupJoinRequest : public snuMessage
	{
		NET_MESSAGE_DECL(snuMsgGroupJoinRequest);

		void Reset(const rage::rlGamerHandle& rRequestor, int nSlots, const rage::rlSessionInfo& info)
		{
			m_Requestor		= rRequestor;
			m_NumJoiners	= nSlots;
			m_SessionInfo	= info;
		}

		NET_MESSAGE_SER(bb, msg)
		{
			return 
				bb.SerUser(msg.m_SessionInfo) && 
				bb.SerUser(msg.m_Requestor) && 
				bb.SerInt(msg.m_NumJoiners, 32);
		}

		rage::rlGamerHandle	m_Requestor;
		int					m_NumJoiners;
		rlSessionInfo		m_SessionInfo;
	};

	//PURPOSE
	//  Sent by a session host to respond to a group join request
	//NOTES
	//  H<-H:       YES
	//  H<->P:      NO
	struct snuMsgGroupJoinResponse : public snuMessage
	{
		NET_MESSAGE_DECL(snuMsgGroupJoinResponse);

		void Reset(const rage::rlSessionInfo& rSessionInfo, const rage::rlSession::ConfigParams& rConfig, int nSlots)
		{
			m_SessionInfo	= rSessionInfo;
			m_Config		= rConfig;
			m_NumJoiners	= nSlots;
		}

		NET_MESSAGE_SER(bb, msg)
		{
			return 
				bb.SerUser(msg.m_SessionInfo) && 
				bb.SerUser(msg.m_Config) && 
				bb.SerInt(msg.m_NumJoiners, 32);
		}

		rage::rlSession::ConfigParams	m_Config;
		rage::rlSessionInfo				m_SessionInfo;
		int								m_NumJoiners;
	};

	//PURPOSE
	//  Sent by a session host to respond to a group join request
	//NOTES
	//  H<->H:     NO
	//  H->P:      YES
	//  P->H:      NO
	struct snuMsgGroupJoinCommand
	{
		NET_MESSAGE_DECL(snuMsgGroupJoinCommand);

		void Reset(const rage::rlSessionInfo& rSessionInfo, const rage::rlSession::ConfigParams& rConfig)
		{
			m_SessionInfo	= rSessionInfo;
			m_Config		= rConfig;
		}

		NET_MESSAGE_SER(bb, msg)
		{
			return 
				bb.SerUser(msg.m_SessionInfo) && 
				bb.SerUser(msg.m_Config);
		}

		rage::rlSession::ConfigParams	m_Config;
		rage::rlSessionInfo				m_SessionInfo;
	};

private:
	const rlGamerHandle*		m_pRequestor;
	int							m_NumJoiners;
	netStatus					m_TransactionStatus;
	snuMsgGroupJoinRequest		m_RequestMsg;
	snuMsgGroupJoinResponse		m_ResponseMsg;
};

}//namespace rage


#endif //SNUTASKS_H
