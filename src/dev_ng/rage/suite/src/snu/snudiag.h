// 
// snu\snudiag.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SNU_SNUDIAG_H
#define SNU_SNUDIAG_H

#include "diag/channel.h"
#include "diag/output.h"
#include "system/timemgr.h"

//PURPOSE
//  Returns the current debug level for the net module
int snuDebugGetLevel();

RAGE_DECLARE_CHANNEL(snu)

// Could reproduce old method via
// #define snuError(fmt,...) RAGE_ERRORF(snu,"[%d %d]"fmt,TIME.GetFrameCount(), rage::sysTimer::GetSystemMsTime(),##__VA_ARGS__)
// but the timestamp functionality exists down in channel.cpp already.
// Note that snuError1/2 and snuWarning1/2 just collapse to the single error and warning channel.
#define snuError(fmt,...)					RAGE_ERRORF(snu,fmt,##__VA_ARGS__)
#define snuError1(fmt,...)					RAGE_ERRORF(snu,fmt,##__VA_ARGS__)
#define snuError2(fmt,...)					RAGE_ERRORF(snu,fmt,##__VA_ARGS__)
#define snuWarning(fmt,...)					RAGE_WARNINGF(snu,fmt,##__VA_ARGS__)
#define snuWarning1(fmt,...)				RAGE_WARNINGF(snu,fmt,##__VA_ARGS__)
#define snuWarning2(fmt,...)				RAGE_WARNINGF(snu,fmt,##__VA_ARGS__)
#define snuDebug(fmt,...)					RAGE_DISPLAYF(snu,fmt,##__VA_ARGS__)
#define snuDebug1(fmt,...)					RAGE_DEBUGF1(snu,fmt,##__VA_ARGS__)
#define snuDebug2(fmt,...)					RAGE_DEBUGF2(snu,fmt,##__VA_ARGS__)
#define snuDebug3(fmt,...)					RAGE_DEBUGF3(snu,fmt,##__VA_ARGS__)

#endif // SNU_SNUDIAG_H
