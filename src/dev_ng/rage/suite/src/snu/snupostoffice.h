#ifndef SNU_POSTOFFICE_H
#define SNU_POSTOFFICE_H

#include "snuconfig.h"
#include "snudiag.h"
#include "snugamer.h"
#include "snusessiondata.h"

#include "atl/array.h"
#include "net/connectionmanager.h"
#include "net/netsequence.h"
#include "system/timemgr.h"
#include "system/timer.h"


namespace rage
{
	class netConnectionManager;
	class sysMemSimpleAllocator;
	class rlPresenceEventInviteReceived;
}

class snuNotifyDataGamerAdded;
class snuNotifyDataGamerRemoved;
class snuGamer;
class snuSessionData;

struct snuGroupJoinPromise
{
	void Reset(int time=0)
	{
		ms_TimeLeft=time;
	}
	void Update(int msDeltaTime)
	{
		if (ms_TimeLeft>0)
			ms_TimeLeft -= msDeltaTime;
	}
	bool IsValid() const
	{ 
		return ms_TimeLeft>0; 
	}

	int ms_TimeLeft;
};


class snuPostOffice
{
protected:

	enum 
	{
		CXNSTATE_INVALID,
		CXNSTATE_REQUESTED,
		CXNSTATE_OPEN,
		CXNSTATE_CLOSED,
		CXNSTATE_TIMEOUT
	};

public:

	snuPostOffice();
	virtual ~snuPostOffice();

	struct snuChannel 
	{
		snuChannel() 
			: m_CxnId			(-1)
			, m_CxnState		(CXNSTATE_INVALID)
			, m_TimeDisconnected(0) 
			, m_SessionId		(-1)
		{}

		int			m_CxnId;
		rage::u8	m_CxnState;
		unsigned	m_TimeDisconnected;
		int			m_SessionId;

		bool IsAssigned() const { return m_CxnState==CXNSTATE_OPEN || m_CxnState==CXNSTATE_REQUESTED;  }
	};

	class snuConnection
	{
		friend class snuPostOffice;

	public:
		snuConnection();
		virtual ~snuConnection()	{}

		virtual void Reset(void)
		{
			for(int i=0; i < m_Channels.GetMaxCount(); ++i)
			{
				m_Channels[i].m_CxnId			= -1;
				m_Channels[i].m_CxnState		= CXNSTATE_INVALID;
				m_Channels[i].m_TimeDisconnected= 0;
			}

			m_Gamer			= NULL;
			m_Available		= true;
			m_Qos			= 0;
			m_TimePingSent	= 0;
		}

		virtual void	OnOpened	(int id) 	{ m_Channels[id].m_CxnState = CXNSTATE_OPEN;								}
		virtual void	OnClosed	(int id) 	{ m_Channels[id].m_CxnState = CXNSTATE_CLOSED; m_Channels[id].m_CxnId = -1;	}
		virtual void	OnQosRecived() 			{;}
		void			RequestQos	() 			;

		unsigned		GetQos					()			const	{ return m_Qos; }
		unsigned		GetTimePingSent			()			const	{ return m_TimePingSent; }
		bool			IsOpen      			(int id)	const	;
		bool			IsClosed    			(int id)	const	{ return m_Channels[id].m_CxnState == CXNSTATE_CLOSED; }
		bool			IsDisconnected			(int id)	const	{ return m_Channels[id].m_TimeDisconnected > 0; }
		unsigned		GetElapsedDisconnectMs	(int id)	const	;
		int				GetConnectionId			(int id)	const	{ return m_Channels[id].m_CxnId; }
		snuGamer*		GetGamer				()					{ return m_Gamer; }
		int				GetAnyValidSnuChannel	()			const	;

	protected:
		rage::atRangeArray<snuChannel, rage::NET_MAX_CHANNELS> m_Channels;
		snuGamer*	m_Gamer;
		int			m_Qos;
		unsigned	m_TimePingSent;
		bool		m_Available : 1; // is this connection slot available?
	};

	class snuConnectionQosMessage
	{
	public:
		NET_MESSAGE_DECL( snuConnectionQosMessage );

		snuConnectionQosMessage(rage::u32 time=0)	{ m_OriginTime = time;												}
		NET_MESSAGE_SER(bb, msg)					{ return bb.SerUns(msg.m_OriginTime, 8*sizeof(msg.m_OriginTime));	}
		const rage::u32& GetOriginTime() const		{ return m_OriginTime;												}

	protected:
		rage::u32 m_OriginTime;
	};

	class snuConnectionQosMessageAck
	{
	public:
		NET_MESSAGE_DECL( snuConnectionQosMessageAck );

		snuConnectionQosMessageAck(rage::u32 time=0){ m_OriginTime = time;												}
		NET_MESSAGE_SER(bb, msg)					{ return bb.SerUns(msg.m_OriginTime, 8*sizeof(msg.m_OriginTime));	}
		const rage::u32& GetOriginTime() const		{ return m_OriginTime;												}

	protected:
		rage::u32 m_OriginTime;
	};

	//ACCESORS
	rage::netConnectionManager*	GetConnectionManager()			{ Assert(m_pCxnMgr); return m_pCxnMgr; }
	rage::netSocket* 			GetSocket			()			;
	rage::netTransactor*		GetTransactor		()			{ return m_pTransactor; }
	unsigned short 				GetPort				()			;
	virtual rage::u8*			GetHeap				()			= 0;
	virtual rage::u32			GetSizeofHeap		()	const	= 0;
	size_t						GetHeapBytesLeft	()			;
	size_t						GetHeapLowWaterMark ()			;
	bool						IsRemoteGroupJoining()	const	{ return m_GroupJoinPromise.IsValid();}

#if __BANK
	// DEBUG USE ONLY!!!
	sysMemSimpleAllocator*		GetAllocator()
	{
		return m_pAllocator;
	}
#endif

	//Life cycle
	bool						Init				(rage::netSocket*);
	void						Shutdown			()			;
	virtual void				Update				()			;

	//binded handling of net connection events
	void						OnCxnEvent			(rage::netConnectionManager*,const rage::netEvent*);

	//PURPOSE
	//  Add gamer to the post office.
	void			AddConnection	(snuGamer& gamer);
	virtual void	OnAddConnection	(snuConnection* )	{}

	//PURPOSE
	//  Remove gamer from the post office.
	void			FreeConnection	(snuGamer& gamer);
	virtual void	OnFreeConnection(snuConnection* )	{}

	//PURPOSE
	//  Open a connection channel to the given gamer.
	virtual snuConnection*		OpenConnection		( snuGamer&, int channelId, int sessionId);

	//PURPOSE
	//  Close connection channel to the given gamer.
	void						CloseConnection		( const snuGamer&, int channelId );

	//PURPOSE
	//  Close connection on all channels to the given gamer.
	void						CloseAllChannels	( const snuGamer& );

	//PURPOSE
	//  Close all the gamer's channels of a given session.
	void						CloseSessionChannels( const snuGamer&, int sessionId );


	//PURPOSE
	//  Responds to transactions
	void OnTransactionRequest						(rage::netTransactor*, rage::netRequestHandler*, const rage::netRequest*);

	//PURPOSE
	//  Application specific tasks for determining connections 
	virtual	const snuConnection*GetPlayerConnection	( int playerId )						const	= 0;
	virtual	snuConnection*		GetPlayerConnection	( int playerId )								= 0;
	virtual int					GetConnectionCount	()										const	= 0;
	virtual const snuConnection*GetHostConnection	()										const	= 0;

	//PURPOSE
	//  Helper functions that can, if needed, be overridden at an application level
	virtual const snuConnection*GetGamerConnection	( const snuGamer&)						const	;
	virtual	snuConnection*		GetGamerConnection	( const snuGamer& gamer)						;
	virtual	const snuConnection*GetPeerConnection	( rage::u64 peerId )					const	;
	virtual	snuConnection*		GetPeerConnection	( rage::u64 peerId )							;
	virtual snuConnection*		GetConnection		( int cxnId, int channelId )					;
	virtual snuConnection*		GetAvailableCxn		()												;
	//  Application specific tasks to do when a gamer is added
	virtual void				OnGamerAdded		(snuNotifyDataGamerAdded& /*ga*/)				{}

	//PURPOSE
	//  Application specific tasks to do when a gamer is removed
	virtual void				OnGamerRemoved		(snuNotifyDataGamerRemoved& /*gr*/)				{}

	//PURPOSE
	//  Use this to handle messages
	virtual bool				HandleMessage		(unsigned msgId, rage::netEventFrameReceived*)	;

	//PURPOSE
	//  Use this to refuse join requests
	virtual char			GetJoinRequestResponse	(const snuSessionData*, const rage::rlGamerInfo&) const;

	//PURPOSE
	//  Send a message to the given gamer.
	template< typename T >
	bool SendToGamer( const snuGamer& gamer,
		const T& msg,
		const unsigned flags,
		int channelId=-1,
		rage::netSequence* frameSeq = 0 )
	{
		const snuConnection* cxn = GetGamerConnection( gamer );

		if (cxn)
		{
			if (channelId<0)
				channelId=cxn->GetAnyValidSnuChannel();

			bool validCxnState = channelId>=0 &&  cxn->m_Channels[channelId].IsAssigned();

			if( validCxnState )
			{
				snuDebug3( "Sending %s to gamer '%s' on channel %d...", msg.GetMsgName(), gamer.GetName(), channelId );
				return SendToCxn( cxn->m_Channels[channelId].m_CxnId, msg, flags, frameSeq );
			}
			else
			{
				snuError( "Tried to send %s to gamer '%s' on invalid cxn on channel %d!", msg.GetMsgName(), gamer.GetName(), channelId );
			}
		}
		return false;
	}

	//PURPOSE
	//  Send a message to the given player.
	template< typename T >
	bool SendToPlayer( const int playerId,
		const T& msg,
		const unsigned flags,
		int channelId,
		rage::netSequence* frameSeq = 0 )
	{
		const snuConnection* cxn = GetPlayerConnection( playerId );
		bool validCxnState = cxn && cxn->m_Channels[channelId].IsAssigned();

		if( validCxnState )
		{
			return SendToCxn( cxn->m_Channels[channelId].m_CxnId, msg, flags, frameSeq );
		}
		else
		{
			snuError2( "Tried to send %s to playerId %d on invalid cxn on channel %d!", msg.GetMsgName(), playerId, channelId );
		}

		return false;
	}

	//PURPOSE
	//  Send a message to each remote member of the given session.
	template< typename T >
	bool SendToSession( const snuSessionData& session,
		const T& msg,
		const unsigned flags, 
		int channelId=-1 )
	{
		bool success = true;

		if (channelId<0)
			channelId=SNU_FIRST_SESSION_CHANNEL+session.GetId();

		for( int i=0; i!=snuSessionData::maxGamerSlots; ++i )
		{
			const snuGamer* gamer = session.GetGamer( i );
			if( gamer && gamer->IsRemote() && !gamer->IsDead())
			{
				success &= SendToGamer( *gamer, msg, flags, channelId );
			}
		}
		return success;
	}

	//PURPOSE
	//  Send a message to the given peer.
	template< typename T >
	bool SendToPeer( rage::u64 peerId,
		const T& msg,
		const unsigned flags, 
		int channelId,
		rage::netSequence* frameSeq = 0 )
	{
		const snuConnection* cxn = GetPeerConnection( peerId );
		bool validCxnState = cxn && cxn->m_Channels[channelId].IsAssigned();

		if( validCxnState )
		{
			return SendToCxn( cxn->m_Channels[channelId].m_CxnId, msg, flags, frameSeq );
		}
		else
		{
			snuError2( "Tried to send %s to peer 0x%" I64FMT "x on invalid cxn on channel %d!", msg.GetMsgName(), peerId, channelId );
		}

		return false;
	}

	//PURPOSE
	//  Send a message to the given connection.
	template< typename T >
	bool SendToCxn( const int cxnId,
		const T& msg,
		const unsigned flags,
		rage::netSequence* frameSeq = 0 )
	{
		if( AssertVerify( cxnId >= 0 ) )
		{
			if (GetConnectionManager()->IsOpen(cxnId) || GetConnectionManager()->IsPendingOpen(cxnId))
			{
				return GetConnectionManager()->Send( cxnId, msg, flags, frameSeq );
			}
			else
			{
				snuError2( "Tried to send %s to unopen cxnId %d!", msg.GetMsgName(), cxnId );
			}
		}
		return false;
	}

	//PURPOSE
	//  Send the same message to all our open connections on the given channel.
	template< typename T >
	bool SendToAll( const T& msg,
		const unsigned flags,
		int _channelId=-1 )
	{
		bool success = true;
		for( int playerId = 0; playerId < MAX_CXNS; playerId++ )
		{
			int channelId = _channelId;
			const snuConnection* cxn = GetPlayerConnection( playerId );

			if (cxn)
			{
				if (channelId<0)
					channelId=cxn->GetAnyValidSnuChannel();

				bool validCxnState = channelId>=0 && cxn->m_Channels[channelId].IsAssigned();

				if( validCxnState )
				{
					success &= SendToCxn( cxn->m_Channels[channelId].m_CxnId, msg, flags, NULL );
				}
			}			
		}
		return success;
	}

	//PURPOSE
	//  Send a message to the host.
	template< typename T >
	bool SendToHost( const T& msg,
		const unsigned flags,
		int channelId,
		rage::netSequence* frameSeq = 0 )
	{
		const snuConnection* cxn = GetHostConnection();
		if( cxn )
		{
			return SendToCxn( cxn->m_Channels[channelId].m_CxnId, msg, flags, frameSeq );
		}
		else
		{
			//possibly a host migration, or not yet in a session, or before
			//a cxn to host was established
			snuError2( "Can't send '%s' to host: no host cxn", msg.GetMsgName() );
		}
		return false;
	}

protected:

	rage::atRangeArray<rage::netConnectionManager::Delegate*, rage::NET_MAX_CHANNELS>	m_pChannelEventDelegates;
	rage::netConnectionManager*	m_pCxnMgr;
	rage::netTransactor*		m_pTransactor;
	rage::netRequestHandler		m_TransactionrRequest;	

	//The allocator used for allocating connections, channels, events, frames, etc.
	rage::sysMemSimpleAllocator*m_pAllocator;
	snuGroupJoinPromise			m_GroupJoinPromise;
};

class snuInvite
{
public:
	snuInvite() {m_InviterName[0] = '\0';}

	char m_InviterName[rage::RL_MAX_NAME_BUF_SIZE];
	rage::rlSessionInfo m_SessionInfo;
	rage::rlGamerHandle m_Inviter;

	rage::inlist_node<snuInvite> m_ListLink;
};

class snuFriendInvites
{
public:

	enum { MAX_INVITES    = 20 };

	typedef rage::inlist<snuInvite, &snuInvite::m_ListLink> ListType;

	snuFriendInvites();

	void						Clear();
	void 						AddInvite			(const rage::rlPresenceEventInviteReceived*);
	void 						RemoveInvites		(const rage::rlSessionInfo&);
	void 						RemoveInvite		(const rage::rlGamerHandle&);
	const rage::rlSessionInfo*	GetInvitedSession	(int idx)			const	;
	const ListType&				GetList				()					const	{ return m_ActiveList;}
	ListType&					GetList				()							{ return m_ActiveList;}
	const snuInvite*			GetInvite			(int idx)			const	;
	const snuInvite*			GetInvite			(const rage::rlGamerHandle&) const;
	const snuInvite*			GetInvite			(const rage::rlSessionInfo&) const;

private:	

	snuInvite	m_Pile[MAX_INVITES];    //Source of instances we can put on lists
	ListType	m_FreeList;             //List of instances currently not used
	ListType	m_ActiveList;           //List of instances currently in use

	bool m_Dirty;
};


#endif //SNU_POSTOFFICE_H

