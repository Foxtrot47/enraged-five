#include "snugamer.h"

#include "snudiag.h"
#include "snusessiondata.h"
#include "snupostoffice.h"
#include "snuoptions.h"
#include "snusocket.h"

#include "rline/rlstats.h"
#include "data/bitbuffer.h"
#include "system/nelem.h"

using namespace rage;
using namespace rage::datbitbuffer;

#if __XENON
#include	<xdk.h>
#include	"system/xtl.h"
#include	<xbox.h>
#endif // __XENON

//------------------------------------------------------------------------------
// snuGamerDataSyncMsg
//------------------------------------------------------------------------------
NET_MESSAGE_IMPL(snuGamerDataSyncMsg);

void 
snuGamerDataSyncMsg::WriteGamerData(snuGamer& gamer) const
{
#if !__NO_OUTPUT
    if(gamer.IsDataReady())
    {
        snuDebug3(("Writing data from snuGamerDataSyncMsg to %s. Before:", gamer.GetName()));
        gamer.GetGamerData()->DisplayData(3,3);
    }
#endif

    m_SyncBuffer.SetCursorPos(0);

    for(int i=0; i!=gamer.GetGamerData()->gGetDataCount(); i++)
    {
        //field present?
        if((m_IsFieldPresent & 1<<i) > 0)
        {
            snuDebug3(("snuGamerDataSyncMsg from Gamer, %s; Writing data %s" , gamer.GetName(), gamer.GetGamerData()->gGetDataName(i)));

            gamer.GetGamerData()->m_Data.SetCursorPos(gamer.GetGamerData()->g_aBitOffset[i]);
            m_SyncBuffer.ReadBits(gamer.GetGamerData()->m_Data, gamer.GetGamerData()->gGetBitSize(i));
        }
    }

    gamer.GetGamerData()->m_IsFieldDirty = m_IsFieldPresent;

    snuDebug2(("Gamer Data updated for %s. After:", gamer.GetName()));
    gamer.GetGamerData()->DisplayData(2,3);
}

void 
snuGamerDataSyncMsg::ReadDirtyGamerData(const snuGamer& gamer)
{
    m_SyncBuffer.SetCursorPos	(0);
    m_IsFieldPresent			= 0;

    for(int i=0; i!=snuGamerData::gDataCount; i++)
    {
        if
       (
            gamer.GetGamerData()->IsFieldDirty(i) && 
            AssertVerify(gamer.GetGamerData()->gIsSyncable(i))
       )
        {
            gamer.GetGamerData()->m_Data.SetCursorPos(gamer.GetGamerData()->g_aBitOffset[i]);
            m_SyncBuffer.WriteBits(gamer.GetGamerData()->m_Data, gamer.GetGamerData()->gGetBitSize(i));

            m_IsFieldPresent |= 1<<i;
        }
    }

    m_GamerId = gamer.GetGamerId();
}

void 
snuGamerDataSyncMsg::ReadSyncableGamerData(const snuGamer& gamer)
{
    m_SyncBuffer.SetCursorPos	(0);
    m_IsFieldPresent			= 0;

    for(int i=0; i!=snuGamerData::gDataCount; i++)
    {
        if(gamer.GetGamerData()->gIsSyncable(i))
        {
            gamer.GetGamerData()->m_Data.SetCursorPos(gamer.GetGamerData()->g_aBitOffset[i]);
            m_SyncBuffer.WriteBits(gamer.GetGamerData()->m_Data, gamer.GetGamerData()->gGetBitSize(i));

            m_IsFieldPresent |= 1<<i;
        }
    }

    m_GamerId			= gamer.GetGamerId();
}

//------------------------------------------------------------------------------
// snuGamerData
//------------------------------------------------------------------------------
int snuGamerData::g_aBitOffset[MAX_GAMER_VARS+1];
const snuGamerDataDef* snuGamerData::g_aDataDef = 0;
int	snuGamerData::gDataCount = 0;

bool 
snuGamerData::gInitialise(const snuGamerDataDef aGamerDataDef[], int dataCount)
{
    g_aDataDef = aGamerDataDef;
    gDataCount = dataCount;

    Assert(gDataCount <= MAX_GAMER_VARS);

    for(int i=0, bit=0; i!=gDataCount+1; i++)
    {
        g_aBitOffset[i] = bit;
        bit += aGamerDataDef[i].m_BitSize;
    }

    return true;
}

int 
snuGamerData::gGetDataIndexByName(const char *name)
{
    for(int i=0; i!=gDataCount; i++)
    {
        if(!strcmp(name, g_aDataDef[i].m_szName))
        {
            return i;
        }
    }

    return -1;
}

int	
snuGamerData::gGetBitSize(const int idx)				
{ 
    Assert(idx>=0 && idx<gDataCount);
    return g_aBitOffset[idx+1] - g_aBitOffset[idx]; 
}


bool 
snuGamerData::gIsSigned(const int idx)
{
    Assert(idx>=0 && idx<gDataCount);
    return g_aDataDef[ idx ].m_Sign == syncSigned;
}

//NOTE: this constructor needs to be called on all its objects AFTER snuSocket::Init()
snuGamerData::snuGamerData() 
: m_IsFieldDirty(0)
{
    m_Data.SetNumBitsWritten(gGetBitSize()); 
}


int 
snuGamerData::GetData(const int idx, void *dst)	const	
{
    Assert(idx>=0 && idx<gDataCount);

    const int dstBitOffset = 0;
    int numBitsRead = 0;
    if(m_Data.PeekBits(dst, gGetBitSize(idx), dstBitOffset, g_aBitOffset[idx]))
    {
        numBitsRead = gGetBitSize(idx);
    }

    return numBitsRead;
}

int 
snuGamerData::SetData(const int idx, const void *source)
{
    Assert(idx>=0 && idx<gDataCount);

    const int numBitsToWrite = gGetBitSize(idx);
    const int sourceBitOffset = 0;
    const int destBitOffset = g_aBitOffset[idx];
    int numBitsWritten = 0;

    if(numBitsToWrite>0 && 
        AssertVerify(m_Data.PokeBits(source, numBitsToWrite, sourceBitOffset, destBitOffset)))
    {
        numBitsWritten = numBitsToWrite;
        if(gIsSyncable(idx))
        {
            SetFieldDirty(idx);
        }		
    }

    return numBitsWritten;
}

//FIX 
// this could be even more generic and type safe if value is returned by template reference,
// that way we can precisely check if the stored value will fit in the given size
int 
snuGamerData::GetDataInt(const int idx) const
{
    const int numBits = gGetBitSize(idx);

    Assert(idx>=0 && idx<gDataCount);
    Assert(gIsSigned(idx)==IntTraits< int >::IsSigned && "Sign mismatch, use GetDataUns()");
    Assert(numBits <= IntTraits< int >::NumBits && "Stored data size is too big for an int");
    Assert(numBits > 1 && "Use unsigned for boolean values");

    int data = 0;
    m_Data.PeekInt(data, numBits, g_aBitOffset[idx]);
    return data;
}

int 
snuGamerData::SetDataInt(const int idx, const int v)
{
    const int numBits = gGetBitSize(idx);

    Assert(idx>=0 && idx<gDataCount);
    Assert(gIsSigned(idx)==IntTraits< int >::IsSigned && "Sign mismatch, use SetDataUns()");
    Assert(numBits <= IntTraits< int >::NumBits && "Stored data size is too big for an int");
    Assert(numBits > 1 && "Use unsigned for boolean values");

    if(GetDataInt(idx)==v)
        return 0;

    //make sure we are not assigning an int value greater than what the bits can hold
    const int maxValue =(1 <<(numBits-1)) - 1;
    if(!AssertVerify(v <= maxValue))
        return 0;
    
    const int minValue = -maxValue - 1;
    if(!AssertVerify(v >= minValue))
        return 0;

    if(m_Data.PokeInt(v, numBits, g_aBitOffset[idx]))
    {
        if(gIsSyncable(idx))
            SetFieldDirty(idx);

        return numBits;
    }
    return 0;
}

unsigned 
snuGamerData::GetDataUns(const int idx) const
{
    const int numBits = gGetBitSize(idx);

    Assert(idx>=0 && idx<gDataCount);
    Assert(gIsSigned(idx)==IntTraits< unsigned >::IsSigned && "Sign mismatch, use GetDataInt()");
    Assert(numBits <= IntTraits< unsigned >::NumBits && "Stored data size is too big for an int");

    int data = 0;
    m_Data.PeekUns(data, numBits, g_aBitOffset[idx]);
    return data;
}

int	
snuGamerData::SetDataUns(const int idx, const unsigned v)
{
    const int numBits = gGetBitSize(idx);

    Assert(idx>=0 && idx<gDataCount);
    Assert(gIsSigned(idx)==IntTraits< unsigned >::IsSigned && "Sign mismatch, use SetDataInt()");
    Assert(numBits <= IntTraits< unsigned >::NumBits && "Stored data size is too big for an int");

    if(GetDataUns(idx)==v)
        return 0;

    //make sure we are not assigning a value greater than what the bits can hold
    unsigned maxValue = numBits-1;
    maxValue =(1<<maxValue) - 1 +(1<<maxValue);
    if(!AssertVerify(v <= maxValue))
        return 0;

    if(m_Data.PokeUns(v, numBits, g_aBitOffset[idx]))
    {
        if(gIsSyncable(idx))
        {
            SetFieldDirty(idx);
        }		
        return numBits;
    }
    return 0;
}

void 
snuGamerData::Clear()
{
    m_Data.Clear();
    m_IsFieldDirty = 0;
}

#if __NO_OUTPUT
void snuGamerData::DisplayData(int, int) const
#else
void snuGamerData::DisplayData(int minDebugLevelDirty, int minDebugLevelClean) const
#endif
{
#if !__NO_OUTPUT
    char buf[128] = "";

    for(int i=0; i!=gDataCount; i++)
    {
        if((IsFieldDirty(i) && snuDebugGetLevel() >= minDebugLevelDirty) || 
           (!IsFieldDirty(i) && snuDebugGetLevel() >= minDebugLevelClean))
        {
            if(gGetBitSize(i) <= 32)
            {
                formatf(buf, sizeof(buf)-1, "%d", gIsSigned(i) ? GetDataInt(i) : GetDataUns(i));
            }
            else
            {
                formatf(buf, sizeof(buf)-1, "???");
            }

            if(IsFieldDirty(i))
            {
                safecat(buf, " <dirty>", sizeof(buf));
            }

            snuDebug(("\t%s = %s", gGetDataName(i), buf));
        }
    }
#endif
}
