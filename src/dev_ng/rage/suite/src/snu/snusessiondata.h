#ifndef SNU_SESSIONDATA_H
#define SNU_SESSIONDATA_H

#include "snuconfig.h"
#include "snet/session.h"
#include "atl/array.h"
#include "snu/snugamer.h"
#include "snu/snudiag.h"

using namespace rage;

class snuHostData;

class snuSessionData
{
	friend class snuSocket;

public:
	enum
	{
		maxGamerSlots		= 31,	//only 30 remote talkers allowed by voice chat, so limit remote gamers to 30
		GamerIndexVoid		= -1,
	};

protected:

	struct GamerSlot
	{
		snuGamer*	m_pGamer;
		u64			m_Reserved;
	};

	typedef atFixedArray<GamerSlot, maxGamerSlots>	snuGamerArray;

	snuSessionId							m_Id;	//user defined session id, set when session is hosted/joined
	int										m_SessionIdx;
	bool									m_DestroyingSession;
	bool									m_MigratingHost;
	snuGamerArray							m_aGamer;

	snuHostData*							m_HostData;
	rage::snSession							m_SnSession;
	rage::snSession::Delegate				m_SessionDlgt;
	snuGamer*								m_pHost;
	rage::netStatus							m_StartStatus;
	rage::netStatus							m_EndStatus;

	rage::mthRandom							m_Rand;
	unsigned								m_NextRandomMigrationTime;

	///////////////////////////////////////////

	// This is used to package up info about which Gamer is in which slot
	class GamerSlotTableMsg
	{
	public:

		NET_MESSAGE_DECL( GamerSlotTableMsg );

		GamerSlotTableMsg()
		{
			// Reset all PeerIds to zero
			for(int i=0; i<maxGamerSlots; ++i)
			{
				m_SlotPeerIds[i] = 0;
			}
		}

		GamerSlotTableMsg( snuGamerArray& gamerArray )
		{
			// Extract each slot's PeerId
			for(int i=0; i<maxGamerSlots; ++i)
			{
				// Current gamers go in the list
				snuGamer* pGamer = gamerArray[i].m_pGamer;
				if(pGamer)
				{
					m_SlotPeerIds[i] = pGamer->GetGamerInfo().GetPeerInfo().GetPeerId();
				}
				// Reserved slots go in the list too
				else
				{
					m_SlotPeerIds[i] = gamerArray[i].m_Reserved;
				}
			}
		}

		NET_MESSAGE_SER( bb, msg )
		{
			bool res = true;

			// Extract each slot's PeerId
			for(int i=0; i<maxGamerSlots; ++i)
			{
				res &= bb.SerBytes(&msg.m_SlotPeerIds[i], 8);
			}

			return res;
		}

		u64 GetSlotPeerId(u32 slotId) const
		{
			return m_SlotPeerIds[slotId];
		}

		void Dump() const
		{
#if !__FINAL
			snuDebug1( "GamerSlotTableMsg::Dump()" );
			snuDebug1( "=========================" );

			for(int i=0; i<maxGamerSlots; ++i)
			{
				u64 peerId = GetSlotPeerId(i);
				snuDebug1( "[%d] = 0x%"I64FMT"x", i, peerId );
			}

			snuDebug1( "=========================" );
#endif
		}

		// Inject our data into the given snuGamerArray
		void Apply(snuGamerArray& gamerArray)
		{
			for(int i=0; i<maxGamerSlots; ++i)
			{
				u64 peerId = GetSlotPeerId(i);

				gamerArray[i].m_Reserved = peerId;
			}
		}

	private:

		// We just need to keep track of which slot contains which PeerId
		u64 m_SlotPeerIds[maxGamerSlots];
	};


	///////////////////////////////////////////

	class MigrateHostMsg
	{
	public:

		NET_MESSAGE_DECL( MigrateHostMsg );

		MigrateHostMsg()
			: m_SessionId(0), m_NewHostPeerId(0)
		{
		}

		MigrateHostMsg( snuSessionId sid, rage::u64 newHostPeerId )
			: m_SessionId(sid), m_NewHostPeerId( newHostPeerId )
		{
		}

		NET_MESSAGE_SER( bb, msg )
		{
			return bb.SerInt(msg.m_SessionId, sizeof(msg.m_SessionId)*8)
				&& bb.SerUns(msg.m_NewHostPeerId, sizeof(msg.m_NewHostPeerId)*8);
		}

		snuSessionId GetSessionId() const { return m_SessionId; }
		rage::u64 GetNewHostPeerId() const { return m_NewHostPeerId; }

	private:
		snuSessionId m_SessionId;
		rage::u64 m_NewHostPeerId;
	};

	///////////////////////////////////////////

	void		HandleEvent			(rage::snSession* pSession, rage::snEvent* evt);
	bool		HandleMigration		(rage::snEventSessionMigrateEnd* evt)	;
	bool		HandleGamerAdded	(rage::snEventAddedGamer*)				;
	bool		HandleGamerRemoved	(snuGamer*)								;
	void    	HandleGetGamerData  (rage::snSession* pSession, const rage::rlGamerInfo& gamerInfo, rage::snGetGamerData* getGamerData) const;
	bool  	HandleJoinRequest   (rage::snSession* pSession, const rage::rlGamerInfo& gamerInfo, rage::snJoinRequest* joinRequest);
	void		SetId				(const snuSessionId& sid)				{ m_Id = sid; }

public:

	snuSessionData();
	~snuSessionData();

	// Debugging
	void			DumpInfo			(fiStream *pFile=0)			const	;

	//life cycle
	void 			Init				(int i)								;
	void			UnInit				()									;
	void			Update				(const unsigned curTime)			;

	//progress
	bool			Start				()									;
	bool			End					()									;
	bool			IsStarted			()							const	{ return GetSession()->IsMatchInProgress();	} 
	bool			IsStarting			()							const	{ return m_StartStatus.Pending();		} 
	bool			IsEnding			()							const	{ return m_EndStatus.Pending();			} 

	//accessors
	const snuSessionId&		GetId		()							const	{ return m_Id;								}
	snuHostData&			GetHostData	()									{ Assert(m_HostData); return *m_HostData;	}
	rage::snSession*		GetSession	()									{ return &m_SnSession;						}
	const rage::snSession*	GetSession	()							const	{ return &m_SnSession;						}
	int				GetSessionIndex		()							const	{ return m_SessionIdx;					}
	bool 			IsLocallyJoined		()							const	;
	bool 			IsLocallyHosted		()							const	;
	
	//operations
	void 			Reset				()									;
	bool			IsFree				()							const	{ return !m_SnSession.Exists() && !m_DestroyingSession; }
	bool			IsTerminating		()							const	{ return m_SnSession.Exists() && m_DestroyingSession; }
	bool			IsMigratingHost		()							const	{ return m_MigratingHost;				}
	void			MigrateHost			(rage::u64 newHostPeerId)			;
	void			Terminate			(snuGamerLocal* killer=NULL)		;

	//slots
	void			ResetGamerSlots		()									;
	int				ReserveGamerSlot	(u64 peerId)						;
	void			ClearGamerSlot		(snuGamer*)							;
	int 			GetGamerSlot		(const snuGamer*)			const	;
	int  			GetHostSlot			()							const	;
	rage::u32		GetOpenSlotCount	(rage::rlSlotType t)				{ return m_SnSession.GetMaxSlots(t)-GetReadyGamerCount(t); }
	void			SetGamerSlot		(int i, snuGamer&)					;
	int				GetFreeGamerSlot	(u64 peerId=0)				const	;

	//participants
	int 			GetGamerIndex		(const u64 peerId)			const;
	int 			GetGamerIndex		(const rage::rlGamerId&)	const;
	const snuGamer* GetGamer			(int i)						const	{ return m_aGamer[i].m_pGamer;			}
	snuGamer* 		GetGamer			(int i)								{ return m_aGamer[i].m_pGamer;			}
	const snuGamer* GetGamerById		(const rage::rlGamerId&)	const;
	snuGamer* 		GetGamerById		(const rage::rlGamerId&)			;
	const snuGamer* GetGamerByHandle	(const rage::rlGamerHandle&)const;
	snuGamer* 		GetGamerByHandle	(const rage::rlGamerHandle&)		;
	snuGamer* 		GetGamerByPeerId	(const u64 peerId)					;
	const snuGamer* GetGamerByPeerId	(const u64 peerId)			const	;
	int				GetGamerCount		()							const	;
	rage::u32		GetReadyGamerCount	(rage::rlSlotType=rage::RL_SLOT_INVALID) const;
	snuGamer*		GetHost				()									{ return m_pHost;						}
	const snuGamer*	GetHost				()							const	{ return m_pHost;						}
	bool			IsMember			(const rage::rlGamerHandle&)const	;
};
//==============================================================================================================================

#endif //SNU_SESSIONDATA_H


