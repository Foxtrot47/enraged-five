#ifndef SNU_SNUSEARCHBOT_H
#define SNU_SNUSEARCHBOT_H

#include "rline/rlqos.h"
#include "rline/rlsession.h"
#include "rline/rlsessionfinder.h"
#include "snuconfig.h"




namespace rage
{
	class rlMatchingFilter;
	class snuGroupJoinRequestTask;
}


class snuGamerLocal;


class snuSearchBot
{
	friend class snuSocket;

	enum
	{
		maxQueryResults = 32
	};

	enum snuSearchBotStatus
	{
		searchBotIdle,
		searchBotActive,
		searchBotFoundResults,
		searchBotQueryFailed,
	};

	struct QueryQosResult 
	{
		rage::rlQos::QosResult	m_QosResult;
		bool					m_IsValid;
		bool					m_IsPending;
	};

	snuSearchBotStatus					m_Status;
	rage::netStatus						m_CurrentTask;
	rage::rlSessionFinder				m_SessionFinder;	
	rage::rlSessionSearchResultArray	m_SearchResultArray;
	rage::u32							m_LastMsTime;
    rage::rlNetworkMode					m_NetMode;
	bool								m_bIsQosEnabled;

	rage::snuGroupJoinRequestTask*		m_pGroupJoinTask;
	rage::rlSessionSearchResult			m_aSearchResult					[ maxQueryResults ];
	rage::rlQos							m_SessionQueryQos				[ maxQueryResults ]; //Qos poller.
	QueryQosResult						m_SessionQueryQosResults		[ maxQueryResults ]; //polling results
	rage::rlQos::NotifyHandler			m_SessionQueryQosNotifyHandler	[ maxQueryResults ];

	void SessionFinderQosNotifyHandler	( rage::rlQos* qos,	const rage::rlQos::NotifyCode code, rage::rlQos::QosResult* result );
	void CancelQueryResultsQos			();

public:

	enum NotifyCode	
	{ 
		snuNotifyError=0, 
		snuNotifyOk=1, 

		snuNotifyJoinRequestAccepted,
	};
	RL_NOTIFIER(snuSearchBot, snuNotifyBase);

	snuSearchBot();
	~snuSearchBot();

	bool FindSessions (const snuGamerLocal*, int slots, rage::rlMatchingFilter* filter, const bool sysLinkNotOnline=false);

	void 							Init					(rage::netConnectionManager*);
	void 							Shutdown				()				;
	void 							Update					(const rage::u32 curTime);
	rage::rlSessionFinder*			GetSessionFinder		()				{ return &m_SessionFinder;					}
	void							ClearQueryResults		()				;
	rage::rlSessionSearchResult&	GetQueryResult			(int i)			{ return m_aSearchResult[i];				}
	rage::rlSessionSearchResult*	GetValidQueryResult		(int i)			;
	int								GetQueryResultCount		()				{ return m_SearchResultArray.m_NumResults;	}
	int								GetValidQueryResultCount()				;
	bool							IsIdle					()		const	;
	bool							IsQueryPending			()		const	;

	//group join
	bool SendGroupJoinRequest		(const snuGamerLocal*, int nResultIdx, int nSlotsToReserve);

#if __LIVE
	void							EnableQosChecking	(bool)			{;}
#else
	void							EnableQosChecking	(bool enable)	{ m_bIsQosEnabled = enable;					}			
#endif
	
	
};


#undef IGNORE
#undef GetObject

#endif //SNU_SNUSEARCHBOT_H


