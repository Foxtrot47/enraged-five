//
// crmotiontree/requestmirror.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTMIRROR_H
#define CRMOTIONTREE_REQUESTMIRROR_H

#include "request.h"

namespace rage
{

class crFrameFilter;

// PURPOSE: Mirror request
// Mirrors input.
class crmtRequestMirror : public crmtRequestSource<1>
{
public:

	// PURPOSE: Default constructor
	crmtRequestMirror();

	// PURPOSE: Initializing constructor - for mirror with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// filter - optional pointer to frame filter (can be NULL)
	crmtRequestMirror(crmtRequest& source, crFrameFilter* filter=NULL);

	// PURPOSE: Initializing constructor - for mirroring existing node(s)
	// PARAMS: filter - optional pointer to frame filter (can be NULL)
	crmtRequestMirror(crFrameFilter* filter);

	// PURPOSE: Destrucutor
	virtual ~crmtRequestMirror();

	// PURPOSE: Initializer - for mirror with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// filter - optional pointer to frame filter (can be NULL)
	void Init(crmtRequest& source, crFrameFilter* filter=NULL);

	// PURPOSE: Initializer - for mirroring existing node(s)
	// PARAMS: filter - optional pointer to frame filter (can be NULL)
	void Init(crFrameFilter* filter=NULL);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get the optional frame filter for this mirroring operation
	// RETURNS: pointer to the frame filter, can be NULL.
	// NOTES: Using a filter with a mirror operation allows partial mirroring
	crFrameFilter* GetFilter() const;


	// PURPOSE: Set the optional frame filter for this mirroring operation
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	crFrameFilter* m_Filter;
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTMIRROR_H 	
