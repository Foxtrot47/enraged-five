//
// crmotiontree/node.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODE_H
#define CRMOTIONTREE_NODE_H

#include "iterator.h"
#include "motiontree_config.h"
#include "observer.h"
#include "updater.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "atl/map.h"
#include "data/resource.h"
#include "data/struct.h"
#include "cranimation/animation_config.h"
#include "cranimation/frameaccelerator.h"
#include "system/bit.h"
#include "system/param.h"

// force registration of non-core nodes - can cause pulling in of extra modules
#define INIT_NON_CORE_NODES (0)
#define CRMT_UPDATE_INFLUENCES (0)
#define CRMT_NODE_SIGNATURE (0)

namespace rage
{

// forward declarations
class crmtDestructor;
class crmtDumper;
class crmtMessage;
class crmtMotionTree;
class crmtNodeFactory;
class crmtNodeParent;
class crmtObserver;
class crmtValidator;
class crmtVisitor;

////////////////////////////////////////////////////////////////////////////////

struct UpdateMsgPayload
{
	float m_DeltaTime;
#if CRMT_UPDATE_INFLUENCES
	InfluenceStack* m_Influences;
#endif
};

////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Abstract base class for motion tree nodes
class crmtNode
{
public:

	// PURPOSE: Enumeration of (built in) node types
	enum eNodes
	{
		kNodeNone,

		kNodeBase,
		kNodeParent,
		kNodeN,
		kNodeAnimation,
		kNodeBlend,
		kNodeAddSubtract,
		kNodeFilter,
		kNodeMirror,
		kNodeFrame,
		kNodeIk,
		kNodeRagdoll,
		kNodeMover,
		kNodeBlendN,
		kNodePonyTail,
		kNodeClip,
		kNodeStyle,
		kNodePm,
		kNodeExtrapolate,
		kNodeExpression,
		kNodeCapture,
		kNodeProxy,
		kNodeAddN,
		kNodeIdentity,
		kNodeMerge,
		kNodePose,
		kNodeMergeN,
		kNodeState,
		kNodeInvalid,
		kNodeJointLimit,
		kNodeStateRoot,
		kNodeStateMachine,
		kNodeSubNetwork,
		kNodeReference,
		// future nodes go here...

		// this must follow the list of built in nodes
		kNodeNum,

		// custom node types *must* have a unique id >= kNodeCustom
		kNodeCustom = 0x80,
	};


	// PURPOSE: Default constructor
	// PARAMS: nodeType - node type identifier (see crmtNode::eNodes enumeration for built in nodes)
	crmtNode(eNodes nodeType=kNodeNone);

	// PURPOSE: Resource constructor
	crmtNode(datResource&);

	// PURPOSE: Fixes up derived type from pointer to base class only
	static void VirtualConstructFromPtr(datResource&, crmtNode* base);

	// PURPOSE: Destructor
	virtual ~crmtNode();

	// PURPOSE: Pre-initialization, call to register which tree a node belongs to
	// PARAMS: tree - motion tree this node belongs to
	void PreInit(crmtMotionTree& tree, eNodes nodeType, u16 defaultFlags=0);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Sends out the kMsgShuttingDown message. All derived classes should call this function at the beginning of their
	// own Shutdown overrides, before doing any other work.
	void ShuttingDown();

	// PURPOSE: Update call, implement to receive call during update pass
	// PARAMS: dt - delta time
	virtual void Update(float);

	// PURPOSE: Pre update call, implement to receive call during update pass before any children
	// PARAMS: dt - delta time
	virtual void PreUpdate(float) {}

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;

	// PURPOSE: Validate.  Implement any validation of node's state.
	virtual void Validate(crmtValidator& validator) const;

	
	// PURPOSE: Get node name
	// RETURNS: Node type name string
	const char* GetNodeTypeName() const;
#endif // CR_DEV

	// PURPOSE: Get node type identifier
	// RETURNS: Node type identifier (see crmtNode::eNodes enumeration for built in nodes)
	eNodes GetNodeType() const;

	// PURPOSE: Is node a built in type
	// RETURNS: false - if built in node type, true - if custom/project specific
	bool IsBuiltInNodeType() const;

	// PURPOSE: Is node a custom type
	// RETURNS: true - if node is a custom node (project specific), false - if built in
	bool IsCustomNodeType() const;

	// PURPOSE: Get motion tree
	// RETURNS: Motion tree this node belongs to
	crmtMotionTree& GetMotionTree() const;

	// PURPOSE: Is node disabled
	// RETURNS: Node disabled (will be culled from compose pass)
	bool IsDisabled() const;

	// PURPOSE: Set node disabled
	// PARAMS: disabled - cull node from compose pass
	void SetDisabled(bool disabled);

	// PURPOSE: Is node silent
	// RETURNS: Node silent (no dependency will be generated)
	bool IsSilent() const;

	// PURPOSE: Set node silent
	// PARAMS: silent - no dependency will be generated
	void SetSilent(bool silent);

	// PURPOSE: Is node target of a proxy
	// RETURNS: Node proxied (by one or more proxies)
	bool IsProxied() const;

	// PURPOSE: Mark node as target of a proxy
	// PARAMS: proxied - node is proxy target
	void SetProxied(bool proxied);

	// PURPOSE: Skip fist child
	// RETURNS: First child is ignored (subsequent branches are composed)
	bool SkipFirstChild() const;

	// PURPOSE: Set skipping of first child (compose subsequent branches only)
	// PARAMS: skip - skip first child from compose pass
	void SetSkipFirstChild(bool skip);

	// PURPOSE: Get parent node
	// RETURNS: Parent node, or NULL if this is the top node in the tree (or not attached to anything)
	crmtNodeParent* GetParent() const;

	// PURPOSE: Get next sibling node
	// RETURNS: Next sibling node, or NULL if no younger siblings
	crmtNode* GetNextSibling() const;

	// PURPOSE: Get first child node
	// RETURNS: First child node, or NULL if no children
	crmtNode* GetFirstChild() const;

	// PURPOSE: Find out if this node has any children
	// RETURNS: true if this node has children
	// NOTES: Much quicker than GetNumChildren.
	// Only use GetNumChildren when actual count is important.
	bool HasChildren() const;

	// PURPOSE: Find out if this node can have any children
	// RETURNS: true if this node is capable of having children
	// (even though it may not actually have any at the moment)
	// SEE ALSO: HasChildren
	virtual bool CanHaveChildren() const;

	// PURPOSE: Find the number of children this node has
	// RETURNS: number of children
	// NOTES: Use much quicker HasChildren() call rather than GetNumChildren() > 0
	// when testing for presence of children before conditionally executing code
	// SEE ALSO: HasChildren
	virtual u32 GetNumChildren() const;

	// PURPOSE: Minimum number of children this node can support
	// RETURNS: minimum number of children
	// SEE ALSO: GetNumChildren, GetMaxNumChildren
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: Maximum number of children this node can support
	// RETURNS: maximum number of children
	// SEE ALSO: GetNumChildren, GetMinNumChildren
	virtual u32 GetMaxNumChildren() const;

	// PURPOSE: Get child
	// PARAM: childIdx - index of child node
	// RETURNS: Child node, or NULL if invalid index/no children.
	// NOTES: For enumeration of children, GetFirstChild() followed by GetNextSibling() is much
	// quicker that repeated calls to GetChild() with an incrementing index.
	virtual crmtNode* GetChild(u32 childIdx) const;

	// PURPOSE: Orphan this node, break link with parent and siblings
	// NOTES: Orphaned nodes retain any children they may have, but have no parent or siblings.
	// Orphaning the root node causes the motion tree root node to become NULL.
	// Node may be automatically destroyed following orphan call, add a reference
	// before calling Orphan if this is not desired.
	void Orphan();

	// PURPOSE: Replace this node
	// PARAMS: newNode - node that will replace this one
	// destroy - destroy the replaced node (default true)
	// NOTES: if not destroyed, take care to retain reference or memory *will* leak.
	// if is destroyed, take care not to access as may have deallocated.
	void Replace(crmtNode& newNode, bool destroy=true);


	// PURPOSE: Add an observer
	// PARAM: observer - observer to add
	// NOTES: If observer is already attached to another node, it will be detached first
	void AddObserver(crmtObserver& observer);

	// PURPOSE: Remove an observer
	// PARAM: observer - observer to remove
	// RETURNS: true - if removal is successful, false - if observer wasn't attached to this node
	bool RemoveObserver(crmtObserver& observer);

	// PURPOSE: Remove all observers
	void RemoveObservers();

	// PURPOSE: Message all observers
	// PARAM: msg - message to send to each of the attached observers
	void MessageObservers(const crmtMessage& msg);

	// PURPOSE: Reset message intercepts
	// NOTES: Triggers any attached observers to register ids of any messages
	// they wish to intercept.
	// Implement in derived nodes as optimization, to optionally suppress
	// generation of non-intercepted messages.
	// Disable generation on receiving this call (then pass to base version),
	// Re-enable if subsequently receive a RegisterMessageIntercept call (with matching id).
	virtual void ResetMessageIntercepts();

	// PURPOSE: Register message intercept
	// PARAMS: msgId - id of message that is being intercepted
	// NOTES: Generated if an attached observer is intercepting this
	// message type (therefore meaning node can no longer suppress
	// generation of this message type as an optimization)
	virtual void RegisterMessageIntercept(u32 msgId);

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh() {}

	// PURPOSE: Get child influence
	virtual float GetChildInfluence(u32 childIdx) const;


	// PURPOSE: Make message ids from message and node ids
	// NOTES: This is for internal node use only!
	#define CRMT_PACK_MSG_ID(msgId, nodeId) ((u32(msgId)&0xffff)|(u32(nodeId)<<16))

	// PURPOSE: Decode message ids, extracts the node id
	// NOTES: This is for internal node use only!
	#define CRMT_UNPACK_MSG_ID_NODE_ID(packedMsgId) (u32(packedMsgId)>>16)

	// PURPOSE: Enumeration of node messages
	enum eMessages
	{
		kMsgNone,

		kMsgAttach = CRMT_PACK_MSG_ID(kMsgNone+1, kNodeBase),
		kMsgDetach,
		kMsgPreUpdate,			// Payload: UpdateMsgPayload
		kMsgUpdate,				// Payload: UpdateMsgPayload
		kMsgPostUpdate,			// Payload: UpdateMsgPayload
		kMsgShuttingDown,
		kMsgDisconnect,
		kMsgChanged,

		kMsgEnd,
	};

	// PURPOSE: Node flags
	enum eFlags
	{
		kFlagShutdown = BIT0,

		kFlagProxied = BIT1,

		kFlagGenerateMsgPreUpdate = BIT2,
		kFlagGenerateMsgUpdate = BIT3,
		kFlagGenerateMsgPostUpdate = BIT4,
		kFlagGenerateMsgDetach = BIT5,

		kFlagGenerateMsgMask = (kFlagGenerateMsgPreUpdate|kFlagGenerateMsgUpdate|kFlagGenerateMsgPostUpdate|kFlagGenerateMsgDetach),

		kFlagSkipFirstChild = BIT6,

		kFlagReceivePreUpdate = BIT7,
		kFlagReceiveUpdate = BIT8,
	};

	// PURPOSE: Initialize the built in node types (core and possibly non-core)
	// NOTES: custom node types will require explicit registration in project code.
	// Non-core built in nodes will only be automatically registered if
	// #define INIT_NON_CORE_NODES is true at top of nodes.h
	// otherwise they too will require explicit registration in project code.
	static void InitClass();

	// PURPOSE: Shutdown the node type registry
	static void ShutdownClass();


	// PURPOSE: Definition of node base create call
	typedef crmtNode* NodeCreateFn(crmtMotionTree&);

	// PURPOSE: Definition of node placement call
	typedef void NodePlaceFn(crmtNode*);

	// PURPOSE: Definition of node resource placement call
	typedef void NodePlaceRscFn(crmtNode*, datResource&);

	// PURPOSE: Node type info, used to register node types
	class NodeTypeInfo
	{
	public:

		// PURPOSE: Constructor
		// PARAMS: nodeType - node type identifier (see crmtNode::eNodes enumeration for built in nodes)
		// nodeName - node name string
		// createFn - node base creation call
		// placeFn - node placement call
		// placeRscFn - node resource placement call
		// size - sizeof node
		NodeTypeInfo(eNodes nodeType, const char* nodeName, NodeCreateFn* createFn, NodePlaceFn* placeFn, NodePlaceRscFn* placeRscFn, u32 size);

		// PURPOSE: Destructor
		~NodeTypeInfo();

		// PURPOSE: Registration call
		void Register() const;

		// PURPOSE: Get node type identifier
		// RETURNS: node type identifier (see crmtNode::eNodes enumeration for built in nodes)
		eNodes GetNodeType() const;

		// PURPOSE: Get node name string
		// RETURNS: string of node's name
		const char* GetNodeName() const;

		// PURPOSE: Get node base creation function
		// RETURNS: node base creation function pointer
		NodeCreateFn* GetCreateFn() const;

		// PURPOSE: Get node placement function
		// RETURNS: node placement function pointer
		NodePlaceFn* GetPlaceFn() const;

		// PURPOSE: Get node resource placement function
		// RETURNS: node resource placement function pointer
		NodePlaceRscFn* GetPlaceRscFn() const;

		// PURPOSE: Get node size (as returned by sizeof)
		// RETURNS: sizeof node
		u32 GetSize() const;

	private:
		eNodes m_NodeType;
		const char* m_NodeName;
		NodeCreateFn* m_CreateFn;
		NodePlaceFn* m_PlaceFn;
		NodePlaceRscFn* m_PlaceRscFn;
		u32 m_Size;
	};

	// PURPOSE: Get info about a node type
	// PARAMS: nodeType - node type identifier (see crmtNode::eNodes enumeration for built in nodes)
	// RETURNS: const pointer to node type info structure (may be NULL if node type unknown)
	static const NodeTypeInfo* FindNodeTypeInfo(eNodes nodeType);

	// PURPOSE: Get info about this node
	// RETURNS: const reference to node type info structure about this node
	virtual const NodeTypeInfo& GetNodeTypeInfo() const = 0;


	// PURPOSE: Declare functions necessary to register a node type
	// Registers node in central node type registry, sets up create calls
	// (allowing nodes to be dynamically allocated or retrieved from a pool)
	#define CRMT_DECLARE_NODE_TYPE(__typename) \
		static rage::crmtNode* CreateNodeBase(rage::crmtMotionTree& tree); \
		static __typename* CreateNode(rage::crmtMotionTree& tree); \
		static void PlaceNode(rage::crmtNode* node); \
		static void PlaceRscNode(rage::crmtNode* node, rage::datResource& rsc); \
		static void InitClass(); \
		virtual const rage::crmtNode::NodeTypeInfo& GetNodeTypeInfo() const; \
		static const rage::crmtNode::NodeTypeInfo sm_NodeTypeInfo;

	// PURPOSE: Implement functions necessary to register a node type
	#define CRMT_IMPLEMENT_NODE_TYPE(__typename, __typeid, __flags) \
		rage::crmtNode* __typename::CreateNodeBase(rage::crmtMotionTree& tree) \
		{ \
			__typename* node = static_cast<__typename*>(CreateNodeFromNodeFactory(tree, rage::crmtNode::eNodes(__typeid))); \
			if(!node) \
			{ \
				node = rage_aligned_new(16) __typename; \
			} \
			node->PreInit(tree, rage::crmtNode::eNodes(__typeid), u16(__flags)); \
			return node; \
		} \
		__typename* __typename::CreateNode(rage::crmtMotionTree& tree) \
		{ \
			return static_cast<__typename*>(CreateNodeBase(tree)); \
		} \
		void __typename::PlaceNode(rage::crmtNode* node) \
		{ \
			::new (node) __typename; \
		} \
		void __typename::PlaceRscNode(rage::crmtNode* node, rage::datResource& rsc) \
		{ \
			::new (node) __typename(rsc); \
		} \
		void __typename::InitClass() \
		{ \
			sm_NodeTypeInfo.Register(); \
		} \
		const rage::crmtNode::NodeTypeInfo& __typename::GetNodeTypeInfo() const \
		{ \
			return sm_NodeTypeInfo; \
		} \
		const rage::crmtNode::NodeTypeInfo __typename::sm_NodeTypeInfo(rage::crmtNode::eNodes(__typeid), CR_DEV ? #__typename :  NULL, CreateNodeBase, PlaceNode, PlaceRscNode, sizeof(__typename));

	// PURPOSE: Pass into CRMT_IMPLEMENT_NODE_TYPE to receive PreUpdate and/or Update calls
	#define CRMT_NODE_RECEIVE_PREUPDATES (crmtNode::kFlagReceivePreUpdate)
	#define CRMT_NODE_RECEIVE_UPDATES (crmtNode::kFlagReceiveUpdate)


	// PURPOSE: Reference counting add operation
	void AddRef() const;

	// PURPOSE: Reference counting release operation
	void Release() const;

#if CRMT_NODE_SIGNATURE
	// PURPOSE: Get current node signature
	// NOTES: Used to detect structural changes in tree - do not rely on format
	u32 GetSignature() const;
#endif

protected:

	// PURPOSE: Internal reference counting, frees node (or returns to pool) if count is zero.
	void ReleaseInternal() const;

	// PURPOSE: Internal function, used to attempt to allocate a node from a node factory.
	// RETURNS: Newly allocated node, or NULL if factory not present, or allocation failed.
	static crmtNode* CreateNodeFromNodeFactory(crmtMotionTree& tree, eNodes nodeType);

private:

	// PURPOSE: Register a new node type (only call from NodeTypeInfo::Register)
	// PARAMS: node type info (must be global/class static, persist for entire execution)
	static void RegisterNodeTypeInfo(const NodeTypeInfo& info);

private:

	mutable u16 m_RefCount;

	u16 m_Flags;

	datPadding<1> m_Padding;
	u8 m_NodeType;
	u8 m_Disabled;
	u8 m_Silent;

	crmtNode* m_ChildNode;
	crmtNode* m_SiblingNode;
	
	crmtNodeParent* m_ParentNode;

	crmtObserver* m_FirstObserver;

	crmtMotionTree* m_MotionTree;

#if CRMT_NODE_SIGNATURE
	u32 m_Signature;
#endif

	crmtNodeFactory* m_Factory;

	static atArray<const NodeTypeInfo*> sm_NodeTypeInfos;

	static bool sm_InitClassCalled;

	friend class crmtDestructor;
	friend class crmtNodeFactory;
	friend class crmtNodeParent;
	friend class crmtUpdater;
};

////////////////////////////////////////////////////////////////////////////////

#if CRMT_NODE_2_DEF_MAP
XPARAM(movedebug);
extern atMap<const crmtNode *, const void *> g_crmtNode2DefMap;
#endif //CRMT_NODE_2_DEF_MAP

////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Abstract base class for motion tree nodes who have children
class crmtNodeParent : public crmtNode
{
public:

	// PURPOSE: Default constructor
	// PARAMS: nodeType - node type identifier (see crmtNode::eNodes enumeration for built in nodes)
	crmtNodeParent(eNodes nodeType=kNodeNone);

	// PURPOSE: Resource constructor
	crmtNodeParent(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeParent();

	// PURPOSE: Shutdown, free/release all dynamic memory
	virtual void Shutdown();

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;

	// PURPOSE: Validate.  Implement any validation of node's state.
	virtual void Validate(crmtValidator& validator) const;
#endif // CR_DEV

	// PURPOSE: Find out if this node can have any children
	// RETURNS: true if this node is capable of having children
	// (even though it may not actually have any at the moment)
	// SEE ALSO: HasChildren
	virtual bool CanHaveChildren() const;

	// PURPOSE: Find the number of children this node has
	// RETURNS: number of children
	// NOTES: Use much quicker HasChildren() call rather than GetNumChildren() > 0
	// when testing for presence of children before conditionally executing code
	// SEE ALSO: HasChildren
	virtual u32 GetNumChildren() const;

	// PURPOSE: Maximum number of children this node can support
	// RETURNS: maximum number of children
	// SEE ALSO: GetNumChildren, GetMinNumChildren
	virtual u32 GetMaxNumChildren() const;

	// PURPOSE: Get child
	// PARAMS: childIdx - index of child node
	// RETURNS: Child node, or NULL if invalid index/no children.
	// NOTES: For enumeration of children, GetFirstChild() followed by GetNextSibling() is much
	// quicker that repeated calls to GetChild() with an incrementing index.
	virtual crmtNode* GetChild(u32 childIdx) const;

	// PURPOSE: Get the child's node index
	// PARAMS: node - the child node
	// RETURNS: index of the child node
	// NOTES: Asserts if node passed in isn't a child of this node, or if error in structure found
	u32 GetChildIndex(crmtNode& node) const;

	// PURPOSE: Add the node as a new child
	// PARAM: newNode - new child node
	// NOTES: This will be appended as the youngest child (i.e. child index == (num children - 1)
	// unless append is false in which case the child will be inserted as the eldest child (i.e. child index == 0)
	void AddChild(crmtNode& newNode, bool append=true);

	// PURPOSE: Remove the child node
	// PARAMS: node - child node to remove
	// destroy - destroy the removed node (default true)
	// NOTES: if node is not a child, will assert
	// if not destroyed, take care to retain reference or memory *will* leak.
	// if is destroyed, take care not to access as may have deallocated.
    void RemoveChild(crmtNode& node, bool destroy=true);

	// PURPOSE: Remove the child node
	// PARAMS: childIdx - index of child node remove
	void RemoveChild(u32 childIdx);

	// PURPOSE: Remove all child nodes
	void RemoveChildren();

	// PURPOSE: Replace child node
	// PARAMS: node - child node to replace
	// newNode - new child node to use in place
	// destroy - destroy the old removed node (default true)
	// NOTES: if node is not a child, will assert
	// if not destroyed, take care to retain reference or memory *will* leak.
	// if is destroyed, take care not to access as may have deallocated.
	void ReplaceChild(crmtNode& node, crmtNode& newNode, bool destroy=true);

	// PURPOSE: Replace child node
	// PARAMS: childIdx - index of child node to replace
	// newNode - new child node to use in place
	void ReplaceChild(u32 childIdx, crmtNode& newNode);

	// PURPOSE: Insert child node
	// PARAMS: childIdx - desired index of new child node (ie insert child node before node identified by this index)
	// newNode - new child node to insert
	void InsertChild(u32 childIdx, crmtNode& newNode);

	// PURPOSE: Promote child node
	// PARAMS: node - child node to promote
	// destroy - destroy this node, and other children
	// NOTES: if node is not a child, will assert
	// if not destroyed, take care to retain reference or memory *will* leak.
	// if is destroyed, take care not to access as may have deallocated.
	void PromoteChild(crmtNode& node, bool destroy=true);

	// PURPOSE: Promote child node
	// PARAMS: childIdx - index of child node to promote
	void PromoteChild(u32 childIdx);

public:
	friend class crmtNode;
};


////////////////////////////////////////////////////////////////////////////////

inline crmtNode::eNodes crmtNode::GetNodeType() const
{
	return eNodes(m_NodeType);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNode::IsBuiltInNodeType() const
{
	return GetNodeType() < kNodeCustom;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNode::IsCustomNodeType() const
{
	return GetNodeType() >= kNodeCustom;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNode::IsDisabled() const
{
	return m_Disabled != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNode::SetDisabled(bool disabled)
{
	m_Disabled = disabled;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNode::IsSilent() const
{
	return m_Silent != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNode::SetSilent(bool silent)
{
	m_Silent = silent;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNode::IsProxied() const
{
	return (m_Flags & kFlagProxied) != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNode::SetProxied(bool proxied)
{
	if(proxied)
	{
		m_Flags |= kFlagProxied;
	}
	else
	{
		m_Flags &= ~kFlagProxied;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNode::SkipFirstChild() const
{
	return (m_Flags & kFlagSkipFirstChild) != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNode::SetSkipFirstChild(bool skip)
{
	if(skip)
	{
		m_Flags |= kFlagSkipFirstChild;
	}
	else
	{
		m_Flags &= ~kFlagSkipFirstChild;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline crmtMotionTree& crmtNode::GetMotionTree() const
{
	FastAssert(m_MotionTree);
	return *m_MotionTree;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNodeParent* crmtNode::GetParent() const
{
	return m_ParentNode;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode* crmtNode::GetNextSibling() const
{
	return m_SiblingNode;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode* crmtNode::GetFirstChild() const
{
	return m_ChildNode;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNode::HasChildren() const
{
	return GetFirstChild() != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNode::CanHaveChildren() const
{
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtNode::GetNumChildren() const
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtNode::GetMinNumChildren() const
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtNode::GetMaxNumChildren() const
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode* crmtNode::GetChild(u32) const
{
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNode::MessageObservers(const crmtMessage& msg)
{
	crmtObserver* observer = m_FirstObserver;
	while(observer)
	{
		crmtObserver* nextObserver = observer->m_NextObserver;

		observer->ReceiveMessage(msg);

		observer = nextObserver;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode::eNodes crmtNode::NodeTypeInfo::GetNodeType() const
{
	return m_NodeType;
}

////////////////////////////////////////////////////////////////////////////////

inline const char* crmtNode::NodeTypeInfo::GetNodeName() const
{
	return m_NodeName;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode::NodeCreateFn* crmtNode::NodeTypeInfo::GetCreateFn() const
{
	return m_CreateFn;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode::NodePlaceFn* crmtNode::NodeTypeInfo::GetPlaceFn() const
{
	return m_PlaceFn;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode::NodePlaceRscFn* crmtNode::NodeTypeInfo::GetPlaceRscFn() const
{
	return m_PlaceRscFn;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtNode::NodeTypeInfo::GetSize() const
{
	return m_Size;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNode::AddRef() const
{
	++m_RefCount;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNode::Release() const
{
	if(!--m_RefCount)
	{
		ReleaseInternal();
	}
}

////////////////////////////////////////////////////////////////////////////////
#if CRMT_NODE_SIGNATURE
inline u32 crmtNode::GetSignature() const
{
	return m_Signature;
}
#endif
////////////////////////////////////////////////////////////////////////////////

inline void crmtNode::ShuttingDown()
{
	if((m_Flags & kFlagShutdown) == 0)
	{
		m_Flags |= kFlagShutdown;
		MessageObservers(crmtMessage(kMsgShuttingDown, NULL));
	}
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtNode::GetChildInfluence(u32 UNUSED_PARAM(childIdx)) const
{
	return 1.f;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNodeParent::CanHaveChildren() const
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtNodeParent::GetNumChildren() const
{
	int numChildren = 0;

	crmtNode* child = m_ChildNode;
	while(child)
	{
		numChildren++;

		child = child->GetNextSibling();
	}

	return numChildren;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtNodeParent::GetMaxNumChildren() const
{
	return 0xffffffff;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode* crmtNodeParent::GetChild(u32 childIdx) const
{
	// FastAssert(childIdx >= 0);

	crmtNode* child = m_ChildNode;
	while(child)
	{
		if(!childIdx--)
		{
			return child;
		}
		child = child->GetNextSibling();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtNodeParent::GetChildIndex(crmtNode& node) const
{
	FastAssert(node.GetParent() == this);

	u32 childIdx = 0;
	crmtNode* child = m_ChildNode;
	while(child)
	{
		if(child == &node)
		{
			return childIdx;
		}

		childIdx++;
		child = child->GetNextSibling();
	}

	FastAssert(0);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODE_H
