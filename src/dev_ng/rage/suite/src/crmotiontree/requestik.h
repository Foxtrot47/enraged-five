//
// crmotiontree/requestik.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTIK_H
#define CRMOTIONTREE_REQUESTIK_H

#include "request.h"

namespace rage
{

class crKinematics;

// PURPOSE: Inverse kinematics request
// Uses the kinematics system to control inverse kinematics
class crmtRequestIk : public crmtRequestSource<1>
{
public:

	// PURPOSE: Default constructor
	crmtRequestIk();

	// PURPOSE: Initializing constructor - for IK with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// kinematics - reference to kinematics system
	crmtRequestIk(crmtRequest& source, crKinematics& kinematics);

	// PURPOSE: Initializing constructor - for applying IK to existing node(s)
	// PARAMS: kinematics - reference to kinematics system
	crmtRequestIk(crKinematics& kinematics);

	// PURPOSE: Destructor
	virtual ~crmtRequestIk();

	// PURPOSE: Initializer - for IK with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// kinematics - reference to kinematics system
	void Init(crmtRequest& source, crKinematics& kinematics);

	// PURPOSE: Initializer - for applying IK to existing node(s)
	// PARAMS: kinematics - reference to kinematics system
	void Init(crKinematics& kinematics);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get the kinematics system
	// RETURNS: kinematics system (will assert if ik request not yet initialized)
	crKinematics& GetKinematics() const;


	// PURPOSE: Set the kinematics system
	// PARAMS: kinematics - reference to kinematics system
	void SetKinematics(crKinematics& kinematics);


	// TODO --- provide ownership over kinematics system

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	crKinematics* m_Kinematics;
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTIK_H 	
