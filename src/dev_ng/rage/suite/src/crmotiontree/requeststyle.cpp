//
// crmotiontree/requeststyle.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requeststyle.h"

#include "nodestyle.h"

#include "crstyletransfer\style.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestStyle::crmtRequestStyle()
: m_Style(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestStyle::crmtRequestStyle(crmtRequest& source, crstStyle* style)
: m_Style(NULL)
{
	Init(source, style);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestStyle::crmtRequestStyle(crstStyle* style)
: m_Style(NULL)
{
	Init(style);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestStyle::~crmtRequestStyle()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestStyle::Init(crmtRequest& source, crstStyle* style)
{
	SetSource(&source, 0);
	SetStyle(style);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestStyle::Init(crstStyle* style)
{
	SetStyle(style);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestStyle::Shutdown()
{
	SetStyle(NULL);

	crmtRequestSource<1>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crstStyle* crmtRequestStyle::GetStyle() const
{
	return m_Style;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestStyle::SetStyle(crstStyle* style)
{
	// TODO --- reference counting!
	m_Style = style;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestStyle::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeStyle* styleNode = crmtNodeStyle::CreateNode(tree);
	styleNode->Init(m_Style);

	GenerateSourceNodes(tree, node, styleNode);

	return styleNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage