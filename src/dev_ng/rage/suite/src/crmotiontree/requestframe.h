//
// crmotiontree/requestframe.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTFRAME_H
#define CRMOTIONTREE_REQUESTFRAME_H

#include "request.h"

namespace rage
{

class crFrame;


// PURPOSE: Frame request
// Requests a motion tree construct a frame node.
class crmtRequestFrame : public crmtRequest
{
public:

	// PURPOSE: Default constructor
	crmtRequestFrame();

	// PURPOSE: Simple constructor, performs basic initialization
	// PARAMS: frame - pointer to the frame to be used (can be NULL)
	crmtRequestFrame(const crFrame* frame);

	// PURPOSE: Destructor
	virtual ~crmtRequestFrame();

	// PURPOSE: Basic initializer
	// PARAMS: frame - pointer to the frame to be used (can be NULL)
	void Init(const crFrame* frame);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Set frame to use
	// PARAMS: frame - pointer to frame to use (can be NULL)
	void SetFrame(const crFrame* frame);

	// PURPOSE: Get frame to use
	// RETURNS: pointer to frame to use (can be NULL)
	const crFrame* GetFrame() const;

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	const crFrame* m_Frame;
};

} // namespace rage

#endif //CRMOTIONTREE_REQUESTFRAME_H
