//
// crmotiontree/motiontree.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "motiontree.h"

#include "iterator.h"
#include "node.h"
#include "observer.h"
#include "request.h"
#include "updater.h"

#include "creature/creature.h"
#include "crmetadata/dumpoutput.h"

#define VALIDATE_MOTION_TREE (__ASSERT)

#define USE_DESTRUCTOR_ITERATOR (1)

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtMotionTree::crmtMotionTree()
: m_RootNode(NULL)
, m_Creature(NULL)
, m_NodeFactory(NULL)
{
	m_States[0] = m_States[1] = 0;
}

////////////////////////////////////////////////////////////////////////////////

crmtMotionTree::~crmtMotionTree()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::Init(crCreature& creature, fragInst* fragment, bool finalize, bool physical)
{
	Assert(!m_Creature);

	m_Creature = &creature;

	m_ComposerData.Init(*this, fragment, finalize, physical);

	Refresh();
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		crmtNode::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::ShutdownClass()
{
	crmtNode::ShutdownClass();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::Shutdown()
{
	Assertf(!m_States[0] && !m_States[1], "Trying to delete a motion tree while still scheduled");
	Reset();

	m_Creature = NULL;
	m_NodeFactory = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::Reset()
{
	if(m_RootNode)
	{
#if !USE_DESTRUCTOR_ITERATOR
		m_RootNode->Shutdown();
		m_RootNode->Release();
#else // !USE_DESTRUCTOR_ITERATOR
		crmtDestructor destructor(*this);
		Iterate(destructor);
#endif // !USE_DESTRUCTOR_ITERATOR
		m_RootNode = NULL;
	}
}

///////////////////////////////////////////////////////////////////////

class crmtRefreshIterator : public crmtIterator
{
	virtual void Visit(crmtNode& node)
	{
		node.Refresh();
	}
};

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::Update(float dt)
{
	if(m_RootNode)
	{	
		crmtUpdater updater;
		updater.Iterate(*m_RootNode, dt);
	}

	// validate - check update didn't corrupt tree
#if VALIDATE_MOTION_TREE
	AssertVerify(Validate());
#endif // VALIDATE_MOTION_TREE
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::Refresh()
{
	m_ComposerData.Refresh();

	if(m_RootNode)
	{
		crmtRefreshIterator it;
		it.Iterate(*m_RootNode);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::Request(crmtRequest& request, const crmtObserver* target)
{
	if(target && target->GetNode())
	{
		request.Generate(*this, target->GetNode());
	}
	else
	{
		crmtNode* oldRootNode = m_RootNode;
		m_RootNode = request.Generate(*this, m_RootNode);

		if(oldRootNode == NULL && m_RootNode != NULL)
		{
			m_RootNode->AddRef();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::AttachToRoot(crmtObserver& observer)
{
	if(m_RootNode)
	{
		observer.Attach(*m_RootNode);
	}
	else
	{
		observer.Detach();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::Iterate(crmtIterator& iterator) const
{
	iterator.Start();
	if(m_RootNode)
	{
		iterator.Iterate(*m_RootNode);
	}
	iterator.Finish();
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::SetNodeFactory(crmtNodeFactory* factory)
{
	m_NodeFactory = factory;
}

////////////////////////////////////////////////////////////////////////////////

crSkeleton* crmtMotionTree::GetSkeleton() const
{
	return m_Creature->GetSkeleton();
}

////////////////////////////////////////////////////////////////////////////////

crFrameAccelerator& crmtMotionTree::GetFrameAccelerator()
{
	return m_Creature->GetFrameAccelerator();
}

////////////////////////////////////////////////////////////////////////////////

const crFrameData& crmtMotionTree::GetFrameData() const
{
	return m_Creature->GetFrameData();
}

////////////////////////////////////////////////////////////////////////////////

bool crmtMotionTree::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
bool crmtMotionTree::Validate() const
{
	crmtValidator validator(*this);
	Iterate(validator);

	return validator.IsValid();
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::Dump(crDumpOutput& output) const
{
	crmtDumperOutput dumper(output);
	Iterate(dumper);
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTree::Dump(int verbosity, char* buf, u32 bufSize) const
{
	if(buf)
	{
		Assert(bufSize > 0);

		crDumpOutputString output;
		output.SetVerbosity(verbosity);

		Dump(output);

		memcpy(buf, output.GetString().c_str(), Min<int>(bufSize, output.GetString().GetLength()));
		buf[bufSize-1] = '\0';
	}
	else
	{
		crDumpOutputStdIo output;
		output.SetVerbosity(verbosity);

		Dump(output);
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
