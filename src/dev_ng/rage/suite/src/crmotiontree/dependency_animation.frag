//
// crmotiontree/dependency_animation.frag
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_ANIMATION_FRAG
#define MOTIONTREE_DEPENDENCY_ANIMATION_FRAG

#include "cranimation/animstream.h"
#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"
#include "system/dma.h"
#include "system/memops.h"
#include "vectormath/classes.h"
#include <limits.h>

#if __SPU
extern "C" unsigned int datDecompressInPlace(unsigned char *dest, const unsigned int destSize, const unsigned int offsetOfSrc, const unsigned int srcSize);
#include "cranimation/animstream.cpp"
#else // __SPU
#include "data/compress.h"
#endif // __SPU

using namespace rage;

////////////////////////////////////////////////////////////////////////////////

bool CalcMoverUnsafe(const crBlockStream* block, float time, TransformV_InOut moverAbsolute)
{
	moverAbsolute = TransformV(V_IDENTITY);

	unsigned int tq = Min((unsigned int)(time), (unsigned int)(block->m_NumFrames-2));
	float tr = Clamp(time - float(tq), 0.f, 1.f);	

	unsigned int numTransChannels = block->m_NumMoverChannels >> 4;
	unsigned int numRotChannels = block->m_NumMoverChannels & 15;
	const crBlockStream::Channel* channels = reinterpret_cast<const crBlockStream::Channel*>(reinterpret_cast<const u8*>(block) + block->m_ChannelOffset);
	block->EvaluateChannels(reinterpret_cast<u8*>(&moverAbsolute.GetPositionRef()), tq, tr, numTransChannels, channels);
	block->EvaluateChannels(reinterpret_cast<u8*>(&moverAbsolute.GetRotationRef()), tq, tr, numRotChannels, channels + numTransChannels);

	return numTransChannels!=0 || numRotChannels!=0;
}

////////////////////////////////////////////////////////////////////////////////

bool CalcMoverDeltaUnsafe(const crBlockStream* block, float time, float delta, TransformV_InOut moverDelta)
{
	moverDelta = TransformV(V_IDENTITY);

	if(delta != 0.f)
	{
		float start;
		if(delta > 0.f)
		{
			delta = Min(time, delta);
			start = time - delta;
		}
		else
		{
			delta = Max(time-float(block->m_NumFrames-1), delta);
			start = time - delta;
		}

		TransformV moverStart, moverEnd;
		if(CalcMoverUnsafe(block, start, moverStart) && CalcMoverUnsafe(block, time, moverEnd))
		{
			UnTransform(moverDelta, moverStart, moverEnd);

			return true;
		}

		return false;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool CalcMoverDelta(const crBlockStream* block, float time, float delta, float rangeStart, float rangeEnd, TransformV_InOut moverDelta)
{
	float rangeDuration = rangeEnd-rangeStart;
	float start = time - delta;
	
	moverDelta = TransformV(V_IDENTITY);

	// nothing to compute if the range is null
	if (rangeDuration == 0.f)
		return false;

	if(delta > 0.f)
	{
		// positive delta, start time is before current time
		if(start < rangeStart)
		{
			// delta caused looping
			float loopedDelta = fmodf(-(start-rangeStart), rangeDuration);
			if(!CalcMoverDeltaUnsafe(block, rangeEnd, loopedDelta, moverDelta))
			{
				return false;
			}

			if(-(start-rangeStart) > rangeDuration)
			{
				// loops multiple times
				TransformV moverLoop;
				if(!CalcMoverDeltaUnsafe(block, rangeEnd, rangeDuration, moverLoop))
				{
					return false;
				}

				int loops = int(floor(-(start-rangeStart) / rangeDuration));
				// WARNING: loops should be >= 1

				while(loops--)
				{
					Transform(moverDelta, moverDelta, moverLoop);
				}
			}
		}

		// calculate the delta in current loop
		TransformV moverCurrent;
		if(!CalcMoverDeltaUnsafe(block, time, Min(time-rangeStart, delta), moverCurrent))
		{
			return false;
		}

		Transform(moverDelta, moverDelta, moverCurrent);
	}
	else if(delta < 0.f)
	{
		// negative delta, start time is after current time
		if((start-rangeStart) > rangeDuration)
		{
			// delta caused looping
			float loopedDelta = -fmodf((start-rangeStart), rangeDuration);
			if(!CalcMoverDeltaUnsafe(block, rangeStart, loopedDelta, moverDelta))
			{
				return false;
			}

			if((start-rangeEnd) > rangeDuration)
			{
				// loops multiple times
				TransformV moverLoop;
				if(!CalcMoverDeltaUnsafe(block, rangeStart, -rangeDuration, moverLoop))
				{
					return false;
				}

				int loops = int(floor((start-rangeStart) / rangeDuration)-1.f);
				// WARNING: loops should be >= 1

				while(loops--)
				{
					Transform(moverDelta, moverDelta, moverLoop);
				}
			}
		}

		// calculate the delta in the current loop
		TransformV moverCurrent;
		if(!CalcMoverDeltaUnsafe(block, time, Max(time-rangeEnd, delta), moverCurrent))
		{
			return false;
		}

		Transform(moverDelta, moverDelta, moverCurrent);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

SPUFRAG_DECL(bool, dependency_animation, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_animation, const sysDependency& dep)
{
	u8* destFrame = static_cast<u8*>(dep.m_Params[0].m_AsPtr);
	crBlockStream* block = static_cast<crBlockStream*>(dep.m_Params[1].m_AsPtr);
	const u16* indices = static_cast<const u16*>(dep.m_Params[2].m_AsConstPtr);
	float time = dep.m_Params[3].m_AsFloat;
	float delta = dep.m_Params[4].m_AsFloat;
	bool calcMover = dep.m_Params[5].m_AsBool;
	u16 moverTranslationOffset = dep.m_Params[6].m_AsShort.m_Low;
	u16 moverRotationOffset = dep.m_Params[6].m_AsShort.m_High;
	float rangeStart = Min(dep.m_Params[7].m_AsFloat, f32(block->m_NumFrames-1));
	float rangeEnd = Min(dep.m_Params[8].m_AsFloat, f32(block->m_NumFrames-1));
    unsigned int indiceOffset = dep.m_Params[9].m_AsInt;
    unsigned int destFrameSize = dep.m_DataSizes[0];

	crmtDebug3("dependency_animation - time %.3f delta %.3f calcMover %d", time, delta, calcMover);

	// reset frame only for the first animation
	indices += indiceOffset;
	if(!indiceOffset)
	{
		sysMemSet(destFrame, 0xff, destFrameSize);
	}

	unsigned int tq = Min((unsigned int)(time), (unsigned int)(block->m_NumFrames-2));
	float tr = Clamp(time - f32(tq), 0.f, 1.f);	

	// uncompress block if necessary
	if(block->IsCompact())
	{
		u8* buffer = reinterpret_cast<u8*>(block+1);
		unsigned int offset = block->m_DataSize - block->m_CompactSize + block->m_SlopSize;
		memmove(buffer + offset, buffer, block->m_CompactSize);
		Assert(buffer + offset + block->m_CompactSize <= (u8*)block + dep.m_DataSizes[1]);
		ASSERT_ONLY(u32 decompressed=) datDecompressInPlace(buffer, block->m_DataSize, offset, block->m_CompactSize);
		Assert(buffer + decompressed <= (u8*)block + dep.m_DataSizes[1]);
		block->m_CompactSize = 0;
	}

	block->EvaluateFrame(destFrame, tq, tr, indices);

	// compute mover
	if(calcMover)
	{
		TransformV moverDelta;
		if(CalcMoverDelta(block, time, delta, rangeStart, rangeEnd, moverDelta))
		{
			Vec3V v = moverDelta.GetPosition();
			QuatV q = moverDelta.GetRotation();
			if(moverTranslationOffset != USHRT_MAX)
			{
				Assertf(IsFiniteAll(v), "dependency_animation: <%f, %f, %f> is valid translation", v.GetXf(), v.GetYf(), v.GetZf());
				FastAssert(moverTranslationOffset < destFrameSize);
				*reinterpret_cast<Vec3V*>(destFrame + moverTranslationOffset) = v;
			}
			if(moverRotationOffset != USHRT_MAX)
			{
				Assertf(IsCloseAll(MagSquared(q), ScalarV(V_ONE), ScalarV(V_FLT_SMALL_2)), "dependency_animation: <%f, %f, %f, %f> is not a pure rotation", q.GetXf(), q.GetYf(), q.GetZf(), q.GetWf());
				FastAssert(moverRotationOffset < destFrameSize);
				*reinterpret_cast<QuatV*>(destFrame + moverRotationOffset) = q;
			}
		}
		else
		{
			// invalidate mover if the calculation failed
			Vec::Vector_4V _invalid = Vec::V4VConstant(V_MASKXYZW);
			if(moverTranslationOffset != USHRT_MAX)
			{
				*reinterpret_cast<Vec::Vector_4V*>(destFrame + moverTranslationOffset) = _invalid;
			}
			if(moverRotationOffset != USHRT_MAX)
			{
				*reinterpret_cast<Vec::Vector_4V*>(destFrame + moverRotationOffset) = _invalid;
			}
		}
	}
	return true;
}

#endif // MOTIONTREE_DEPENDENCY_ANIMATION_FRAG
