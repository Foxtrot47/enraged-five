//
// crmotiontree/nodeframe.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodeframe.h"

#include "motiontree.h"

#include "cranimation/frame.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeFrame::crmtNodeFrame()
: crmtNode(kNodeFrame)
, m_Frame(NULL)
, m_FrameSignature(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeFrame::crmtNodeFrame(datResource& rsc)
: crmtNode(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeFrame::~crmtNodeFrame()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeFrame, crmtNode::kNodeFrame, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFrame::Init(const crFrame* frame)
{
	SetFrame(frame);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFrame::Shutdown()
{
	ShuttingDown();
	SetFrame(NULL);

	crmtNode::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFrame::Update(float)
{
	if(m_Frame && m_Frame->GetSignature() != m_FrameSignature)
	{
		Refresh();
	}

	SetDisabled(m_Frame == NULL);
	SetSilent(false);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeFrame::Dump(crmtDumper& dumper) const
{
	crmtNode::Dump(dumper);

	dumper.Outputf(2, "frame", "%p", m_Frame);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFrame::SetFrame(const crFrame* frame)
{
	if(frame != m_Frame)
	{
		if(frame)
		{
			frame->AddRef();
		}
		if(m_Frame)
		{
			m_Frame->Release();
		}
		m_Frame = frame;

		Refresh();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFrame::Refresh()
{
	if(m_Frame && m_Frame->GetFrameData())
	{
		GetMotionTree().GetFrameAccelerator().FindFrameIndices(m_Indices, GetMotionTree().GetFrameData(), *m_Frame->GetFrameData());
		m_FrameSignature = m_Frame->GetSignature();
	}
	else
	{
		m_Indices.Unlock();
		m_FrameSignature = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
