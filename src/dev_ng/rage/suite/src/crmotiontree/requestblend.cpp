//
// crmotiontree/requestblend.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestblend.h"

#include "nodeblend.h"

#include "cranimation/framefilters.h"
#include "cranimation/weightmodifier.h"
#include "math/amath.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestBlend::crmtRequestBlend()
: m_Blend(0.f)
, m_BlendRate(0.f)
, m_DestroyWhenComplete(false)
, m_MergeBlend(false)
, m_Filter(NULL)
, m_Modifier(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestBlend::crmtRequestBlend(crmtRequest& source0, crmtRequest& source1, float blendStart, float blendRate)
: m_Blend(0.f)
, m_BlendRate(0.f)
, m_DestroyWhenComplete(false)
, m_MergeBlend(false)
, m_Filter(NULL)
, m_Modifier(NULL)
{
	Init(source0, source1, blendStart, blendRate);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestBlend::crmtRequestBlend(crmtRequest& target, float blendStart, float blendRate)
: m_Blend(0.f)
, m_BlendRate(0.f)
, m_DestroyWhenComplete(false)
, m_MergeBlend(false)
, m_Filter(NULL)
, m_Modifier(NULL)
{
	Init(target, blendStart, blendRate);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestBlend::~crmtRequestBlend()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlend::Init(crmtRequest& source0, crmtRequest& source1, float blendStart, float blendRate)
{
	SetSource(&source0, 0);
	SetSource(&source1, 1);
	SetBlend(blendStart);
	SetBlendRate(blendRate);

	// assume destroy when complete if there is any rate of change in blend
	if(blendRate != 0.f)
	{
		SetDestroyWhenComplete(true);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlend::Init(crmtRequest& target, float blendStart, float blendRate)
{
	SetSource(&target, 1);
	SetBlend(blendStart);
	SetBlendRate(blendRate);

	// assume destroy when complete if there is any rate of change in blend
	if(blendRate != 0.f)
	{
		SetDestroyWhenComplete(true);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlend::Shutdown()
{
	SetFilter(NULL);
	SetBlendModifier(NULL);

	crmtRequestSource<2>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestBlend::GetBlend() const
{
	return m_Blend;
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestBlend::GetBlendRate() const
{
	return m_BlendRate;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtRequestBlend::IsDestroyWhenComplete() const
{
	return m_DestroyWhenComplete;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtRequestBlend::IsMergeBlend() const
{
	return m_MergeBlend;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtRequestBlend::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifier* crmtRequestBlend::GetBlendModifier() const
{
	return m_Modifier;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlend::SetBlend(float blend)
{
	m_Blend = Clamp(blend, 0.f, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlend::SetBlendRate(float blendRate)
{
	m_BlendRate = blendRate;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlend::SetDestroyWhenComplete(bool destroyWhenComplete)
{
	m_DestroyWhenComplete = destroyWhenComplete;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlend::SetMergeBlend(bool mergeBlend)
{
	m_MergeBlend = mergeBlend;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlend::SetFilter(crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	if(m_Filter)
	{
		m_Filter->Release();
	}
	m_Filter = filter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlend::SetBlendModifier(crWeightModifier* modifier)
{
	if(modifier)
	{
		modifier->AddRef();
	}
	if(m_Modifier)
	{
		m_Modifier->Release();
	}
	m_Modifier = modifier;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestBlend::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	if(HasValidSource())
	{
		crmtNodeBlend* blendNode = crmtNodeBlend::CreateNode(tree);
		blendNode->Init(m_Blend, m_BlendRate, m_DestroyWhenComplete, m_MergeBlend, m_Filter, m_Modifier);

		GenerateSourceNodes(tree, node, blendNode);

		return blendNode;
	}
	else
	{
		// TODO --- could assume this case will just modify node's parameters (if it is a blend node)
		return node;
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
