//
// crmotiontree/nodemirror.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEMIRROR_H
#define CRMOTIONTREE_NODEMIRROR_H

#include "node.h"

namespace rage
{

class crFrameFilter;

// PURPOSE: Mirror dofs from input.
class crmtNodeMirror : public crmtNodeParent
{
public:

	// PURPOSE: Default constructor
	crmtNodeMirror();

	// PURPOSE: Resource constructor
	crmtNodeMirror(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeMirror();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeMirror);

	// PURPOSE: Initialization
	// PARAMS: filter - frame filter to use for filtering (default NULL)
	void Init(crFrameFilter* filter=NULL);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;


	// PURPOSE: Set frame filter
	// PARAMS: filter - frame filter to use for filtering (can be NULL)
	void SetFilter(crFrameFilter* filter);


	// PURPOSE: Get frame filter
	// RETURNS: frame filter currently used for filtering (may be NULL)
	crFrameFilter* GetFilter() const;


private:
	crFrameFilter* m_Filter;
};

////////////////////////////////////////////////////////////////////////////////

inline crFrameFilter* crmtNodeMirror::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRMOTIONTREE_NODEMIRROR_H
