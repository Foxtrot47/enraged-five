//
// crmotiontree/managermixer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRMOTIONTREE_MANAGERMIXER_H
#define CRMOTIONTREE_MANAGERMIXER_H

#include "manager.h"
#include "nodeblendn.h"

namespace rage
{

class crFrameFilter;
class crmtNodeBlendN;
class crmtRequest;

// PURPOSE: Manager that provides a input channel mixing paradigm
class crmtManagerMixer : public crmtManager
{
public:

	// PURPOSE: Default constructor
	crmtManagerMixer();

	// PURPOSE: Helpful constructor that allows provision of motion tree
	// SEE ALSO: Init()
	crmtManagerMixer(crmtMotionTree& tree, const crmtObserver* location=NULL);

	// PURPOSE: Destructor
	virtual ~crmtManagerMixer();

	// PURPOSE: Resource constructor
	crmtManagerMixer(datResource&);

	DECLARE_PLACE(crmtManagerMixer);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initialization
	// PARAMS: tree - motion tree this manager is going to manage
	// location - identifies root node, if manager is not controlling entire tree
	virtual void Init(crmtMotionTree& tree, const crmtObserver* location=NULL);

	// PURPOSE: Shutdown the manager
	virtual void Shutdown();


	// PURPOSE: Get number of channels
	u32 GetNumChannels() const;

	// PURPOSE: Get max number of channels?
	u32 GetMaxNumChannels() const;

	// PURPOSE: Set channel
	const crmtObserver* SetChannel(u32 channelIdx, crmtRequest&);

	// PURPOSE: Get channel
	bool GetChannel(u32 channelIdx, crmtObserver& outObserver) const;

	// PURPOSE: Insert channel
	const crmtObserver* InsertChannel(u32 channelIdx, crmtRequest& request);

	// PURPOSE: Remove channel
	void RemoveChannel(u32 channelIdx);

	// PURPOSE: Remove all channels
	void RemoveAllChannels();

	// PURPOSE: Swap channels
//	void SwapChannels(u32 channelIdx0, u32 channelIdx1);


	// PURPOSE: Set channel weight
	void SetWeight(u32 channelIdx, float weight);

	// PURPOSE: Set channel weight rate
	void SetWeightRate(u32 channelIdx, float weightRate);

	// PURPOSE: Get channel weight
	float GetWeight(u32 channelIdx) const;

	// PURPOSE: Get channel weight rate
	float GetWeightRate(u32 channelIdx) const;


	// PURPOSE: Set channel filter
	void SetFilter(crFrameFilter* filter);

	// PURPOSE: Get channel filter
	crFrameFilter* GetFilter() const;

protected:

	// PURPOSE: Internal initialization
	virtual void InternalInit(const crmtObserver* location);

	// PURPOSE: Get the underlying n way node, if any
	crmtNodeN* GetNodeN() const;

	// PURPOSE: Internal call, (overrideable) notification of channel insert
	virtual void ChannelInserted(u32 channelIdx);

	// PURPOSE: Internal call, (overrideable) notification of channel removal
	virtual void ChannelRemoved(u32 channelIdx);

	// PURPOSE: Direct access to the mixer node
	crmtObserver* m_Mixer;
};


// inline functions

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtManagerMixer::GetMaxNumChannels() const
{
	return crmtNodeBlendN::sm_MaxChildren;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_MANAGERMIXER_H
