//
// crmotiontree/managermixer.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "managermixer.h"

#include "motiontree.h"
#include "nodeblendn.h"
#include "observer.h"
#include "requestblendn.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtManagerMixer::crmtManagerMixer()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtManagerMixer::crmtManagerMixer(crmtMotionTree& tree, const crmtObserver* location)
: crmtManager(tree, location)
{
	InternalInit(location);
}

////////////////////////////////////////////////////////////////////////////////

crmtManagerMixer::~crmtManagerMixer()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtManagerMixer::crmtManagerMixer(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crmtManagerMixer);

#if __DECLARESTRUCT
void crmtManagerMixer::DeclareStruct(datTypeStruct&)
{
	Assert(0);
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::Init(crmtMotionTree& tree, const crmtObserver* location)
{
	crmtManager::Init(tree, location);

	InternalInit(location);
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::Shutdown()
{
	if(m_Mixer)
	{
		m_Mixer->Shutdown();
		m_Mixer->Release();
		m_Mixer = NULL;
	}
	
	crmtManager::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtManagerMixer::GetNumChannels() const
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		return nodeN->GetNumChildren();
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////

const crmtObserver* crmtManagerMixer::SetChannel(u32 channelIdx, crmtRequest& request)
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		crmtNode* child = nodeN->GetChild(channelIdx);
		if(child)
		{
			crmtObserver observer;
			observer.Attach(*child);

			crmtMotionTree& mt = GetMotionTree();
			mt.Request(request, &observer);

			return &request.GetObserver();
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtManagerMixer::GetChannel(u32 channelIdx, crmtObserver& outObserver) const
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		crmtNode* child = nodeN->GetChild(channelIdx);
		if(child)
		{
			outObserver.Attach(*child);
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

const crmtObserver* crmtManagerMixer::InsertChannel(u32 channelIdx, crmtRequest& request)
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		crmtObserver observer;
		observer.AddRef();
		observer.Attach(*nodeN);

		crmtRequestInsert reqInsert(request, channelIdx);

		crmtMotionTree& mt = GetMotionTree();
		mt.Request(reqInsert, &observer);

		ChannelInserted(channelIdx);

		return &request.GetObserver();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::RemoveChannel(u32 channelIdx)
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		nodeN->RemoveChild(channelIdx);

		ChannelRemoved(channelIdx);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::RemoveAllChannels()
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		const u32 numChildren = nodeN->GetNumChildren();

		nodeN->RemoveChildren();

		for(u32 i=0; i<numChildren; ++i)
		{
			nodeN->SetWeight(i, 0.f);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
/*
void crmtManagerMixer::SwapChannels(u32 channelIdx0, u32 channelIdx1)
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		crmtObserver observer;
		observer->Attach();

		nodeN->RemoveChild();
	}
}
*/
////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::SetWeight(u32 channelIdx, float weight)
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		nodeN->SetWeight(channelIdx, weight);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::SetWeightRate(u32 channelIdx, float weightRate)
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		nodeN->SetWeightRate(channelIdx, weightRate);
	}
}

////////////////////////////////////////////////////////////////////////////////

float crmtManagerMixer::GetWeight(u32 channelIdx) const
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		return nodeN->GetWeight(channelIdx);
	}
	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

float crmtManagerMixer::GetWeightRate(u32 channelIdx) const
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		return nodeN->GetWeightRate(channelIdx);
	}
	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::SetFilter(crFrameFilter* filter)
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		nodeN->SetFilter(filter);
	}
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtManagerMixer::GetFilter() const
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		return nodeN->GetFilter();
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::InternalInit(const crmtObserver* location)
{
	crmtMotionTree& mt = GetMotionTree();

	crmtRequestBlendN reqBlendN;
	mt.Request(reqBlendN, location);

	m_Mixer = rage_new crmtObserver();
	m_Mixer->AddRef();

	m_Mixer->Attach(reqBlendN.GetObserver());
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeN* crmtManagerMixer::GetNodeN() const
{
	if(m_Mixer && m_Mixer->IsAttached())
	{
		return static_cast<crmtNodeN*>(m_Mixer->GetNode());
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::ChannelInserted(u32 channelIdx)
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		nodeN->InsertData(channelIdx);
		nodeN->SetWeight(channelIdx, 0.f);
		nodeN->SetWeightRate(channelIdx, 0.f);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerMixer::ChannelRemoved(u32 channelIdx)
{
	crmtNodeN* nodeN = GetNodeN();
	if(nodeN)
	{
		nodeN->RemoveData(channelIdx);
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
