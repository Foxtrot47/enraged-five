//
// crmotiontree/nodeaddsubtract.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEADDSUBTRACT_H
#define CRMOTIONTREE_NODEADDSUBTRACT_H

#include "node.h"
#include "nodeblend.h"
#include <math.h>

namespace rage
{

class crFrameFilter;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Combine, by adding (or subtracting), a pair of input child nodes.
class crmtNodeAddSubtract : public crmtNodePairWeighted
{
public:
	// PURPOSE: Default constructor
	crmtNodeAddSubtract();

	// PURPOSE: Resource construction
	crmtNodeAddSubtract(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeAddSubtract();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeAddSubtract);

	// PURPOSE: Initialization
	// PARAMS: weight - additive weight (-ve for subtract, +ve for add)
	// filter - frame filter to use for this add/subtract (default NULL)
	void Init(float weight, crFrameFilter* filter=NULL);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Update node
	virtual void Update(float dt);

	// PURPOSE: override influence query
	virtual float GetChildInfluence(u32 childIdx) const;

private:
};

////////////////////////////////////////////////////////////////////////////////

inline float crmtNodeAddSubtract::GetChildInfluence(u32 childIdx) const
{
	return ApplyInfluenceOverride(childIdx, childIdx?fabsf(m_Weight):1.f);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRMOTIONTREE_NODEADDSUBTRACT_H
