//
// crmotiontree/requestik.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "requestik.h"

#include "nodeik.h"

#include "crbody/kinematics.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestIk::crmtRequestIk()
: m_Kinematics(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestIk::crmtRequestIk(crmtRequest& source, crKinematics& kinematics)
: m_Kinematics(NULL)
{
	Init(source, kinematics);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestIk::crmtRequestIk(crKinematics& kinematics)
: m_Kinematics(NULL)
{
	Init(kinematics);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestIk::~crmtRequestIk()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestIk::Init(crmtRequest& source, crKinematics& kinematics)
{
	SetSource(&source, 0);
	SetKinematics(kinematics);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestIk::Init(crKinematics& kinematics)
{
	SetKinematics(kinematics);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestIk::Shutdown()
{
	m_Kinematics = NULL;

	crmtRequestSource<1>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crKinematics& crmtRequestIk::GetKinematics() const
{
	Assert(m_Kinematics);
	return *m_Kinematics;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestIk::SetKinematics(crKinematics& kinematics)
{
	m_Kinematics = &kinematics;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestIk::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	Assert(m_Kinematics);

	crmtNodeIk* ikNode = crmtNodeIk::CreateNode(tree);
	ikNode->Init(*m_Kinematics);

	GenerateSourceNodes(tree, node, ikNode);

	return ikNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
