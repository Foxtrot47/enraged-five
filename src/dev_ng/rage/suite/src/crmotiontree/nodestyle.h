//
// crmotiontree/nodestyle.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODESTYLE_H
#define CRMOTIONTREE_NODESTYLE_H

#include "node.h"

#include "crstyletransfer/styleplayer.h"

namespace rage
{

// PURPOSE: Applies style transfer
class crmtNodeStyle : public crmtNodeParent
{
public:

	// PURPOSE: Default constructor
	crmtNodeStyle();

	// PURPOSE: Resource constructor
	crmtNodeStyle(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeStyle();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeStyle);

	// PURPOSE: Initialization
	// PARAMS: style - pointer to the style to apply
	void Init(crstStyle* style);  // TODO -- no timewarping, heuristic, blend weight, up index, confidence, preference

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;


	// PURPOSE: Returns style player.
	// RETURNS: Style player (const).
	const crstStylePlayer& GetStylePlayer() const;

	// PURPOSE: Returns style player.
	// RETURNS: Style player (non-const).
	crstStylePlayer& GetStylePlayer();

	// PURPOSE: Set time modifier.
	void SetTimeModifier(float f);

private:
	crstStylePlayer m_StylePlayer;
	float m_TimingModifier;
};


}; // namespace rage

#endif //CRMOTIONTREE_NODESTYLE_H
