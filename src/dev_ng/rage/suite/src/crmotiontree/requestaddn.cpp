//
// crmotiontree/requestaddn.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "requestaddn.h"

#include "nodeaddn.h"

#include "cranimation/framefilters.h"

namespace rage
{
	

////////////////////////////////////////////////////////////////////////////////

crmtRequestAddN::crmtRequestAddN()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestAddN::~crmtRequestAddN()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAddN::Shutdown()
{
	crmtRequestN::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestAddN::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeAddN* addNNode = crmtNodeAddN::CreateNode(tree);
	addNNode->Init(m_Filter);
	addNNode->SetZeroDestination(m_ZeroDest);

	GenerateSourceNodes(tree, node, addNNode);

	u32 n=0;
	for(u32 i=0; i<crmtNodeN::sm_MaxChildren; ++i)
	{
		if(GetSource(i))
		{
			addNNode->SetWeight(n, m_Weights[i]);
			addNNode->SetWeightRate(n, m_WeightRates[i]);
			addNNode->SetInputFilter(n, m_InputFilters[i]);
			n++;
		}
	}
	addNNode->SetNumChildren(n);

	return addNNode;
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage
