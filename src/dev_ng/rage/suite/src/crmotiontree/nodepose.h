//
// crmotiontree/nodepose.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEPOSE_H
#define CRMOTIONTREE_NODEPOSE_H

#include "node.h"

namespace rage
{

// PURPOSE: Uses an skeleton pose as an input source
class crmtNodePose : public crmtNode
{
public:

	// PURPOSE: Default constructor
	crmtNodePose();

	// PURPOSE: Resource constructor
	crmtNodePose(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodePose();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodePose);

	// PURPOSE: Initialization
	void Init();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh();

	// PURPOSE: Set the frame filter for this operation
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);

	// PURPOSE: Returns indices
	const crLock& GetIndices() const;

private:
	crLock m_Indices;
	crFrameFilter* m_Filter;
};

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodePose::GetIndices() const
{
	return m_Indices;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODEPOSE_H
