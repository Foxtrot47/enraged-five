//
// crmotiontree/requestfilter.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTFILTER_H
#define CRMOTIONTREE_REQUESTFILTER_H

#include "request.h"

namespace rage
{

class crFrameFilter;

// PURPOSE: Filter request
// Filters input using frame filter provided.
class crmtRequestFilter : public crmtRequestSource<1>
{
public:

	// PURPOSE: Default constructor
	crmtRequestFilter();

	// PURPOSE: Initializing constructor - for filter with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// filter - pointer to frame filter (can be NULL)
	crmtRequestFilter(crmtRequest& source, crFrameFilter* filter);

	// PURPOSE: Initializing constructor - for filtering existing node(s)
	// PARAMS: filter - pointer to frame filter (can be NULL)
	crmtRequestFilter(crFrameFilter* filter);

	// PURPOSE: Destructor
	virtual ~crmtRequestFilter();

	// PURPOSE: Initializer - for filter with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// filter - pointer to frame filter (can be NULL)
	void Init(crmtRequest& source, crFrameFilter* filter);

	// PURPOSE: Initializer - for filtering existing node(s)
	// PARAMS: filter - pointer to frame filter (can be NULL)
	void Init(crFrameFilter* filter);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get the frame filter for this operation
	// RETURNS: pointer to the frame filter, can be NULL.
	crFrameFilter* GetFilter() const;


	// PURPOSE: Set the frame filter for this operation
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	crFrameFilter* m_Filter;
};


} // namespace rage

#endif // CRMOTIONTREE_REQUESTFILTER_H 	
