//
// crmotiontree/requestik.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTJOINTLIMIT_H
#define CRMOTIONTREE_REQUESTJOINTLIMIT_H

#include "request.h"

namespace rage
{

class crFrameFilter;
class crJointData;

// PURPOSE: Request to apply limit on rotation of specific joints
// Will use joint limits coming from the skeleton data
class crmtRequestJointLimit : public crmtRequestSource<1>
{
public:

	// PURPOSE: Default constructor
	crmtRequestJointLimit();

	// PURPOSE: Initializing constructor with new child node(s)
	// PARAMS: source - request to generate new child nodes
	crmtRequestJointLimit(crmtRequest& source);

	// PURPOSE: Destructor
	virtual ~crmtRequestJointLimit();

	// PURPOSE: Initializer
	// PARAMS: source - request to generate new child nodes
	void Init(crmtRequest& source);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	void SetJointData(const crJointData& jointData);

	// PURPOSE: Set the frame filter for this blend
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	const crJointData* m_JointData;
	crFrameFilter* m_Filter;
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTJOINTLIMIT_H 	
