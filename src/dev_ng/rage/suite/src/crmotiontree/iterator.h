//
// crmotiontree/iterator.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_ITERATOR_H
#define CRMOTIONTREE_ITERATOR_H

#include "atl/array.h"

#include <stdarg.h>

#include "cranimation/animation_config.h"
#include "motiontree_config.h"

namespace rage
{

class crDumpOutput;
class crmtMotionTree;
class crmtNode;
class crmtNodeParent;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Iterates through all the nodes within a motion tree
class crmtIterator
{
public:

	// PURPOSE: Default constructor
	crmtIterator();

	// PURPOSE: Resource constructor
	crmtIterator(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtIterator();

	// PURPOSE: Start iteration
	// NOTES: Override this to receive a call when the iteration is started
	virtual void Start();

	// PURPOSE: Finish iteration
	// NOTES: Override this to receive a call when the iteration is finished
	virtual void Finish();

	// PURPOSE: Iterate a node
	virtual void Iterate(crmtNode&);

	// PURPOSE: Pre-visit a node
	// NOTES: Override this to receive a call before a node's children are iterated
	virtual void PreVisit(crmtNode&);

	// PURPOSE: Visit a node
	// NOTES: Override this to receive a call after a node's children are iterated,
	// but before any of a node's younger siblings are iterated.
	virtual void Visit(crmtNode&);

	// PURPOSE: Cull a node
	// NOTES: Override this to receive a call if a node is culled (due to being disabled)
	virtual void Cull(crmtNode&);

private:

	// PURPOSE: Batch iteration
	template<int N, bool cull>
	void IterateBatch(crmtNode&);

protected:
	bool m_Cull;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Specialized iterator, destroys tree structure (quickly)
class crmtDestructor : public crmtIterator
{
public:

	// PURPOSE: Default constructor
	crmtDestructor(crmtMotionTree& motionTree);

	// PURPOSE: Destructor
	virtual ~crmtDestructor();

	// PURPOSE: Iterator start override
	virtual void Start();

	// PURPOSE: Iterator finish override
	virtual void Finish();

	// PURPOSE: Iterator visit override
	virtual void Visit(crmtNode&);

protected:

	// PURPOSE: Flush internal list of nodes
	void Flush();

private:

	crmtMotionTree* m_MotionTree;

	static const u32 sm_MaxNumNodes = 128;
	atFixedArray<crmtNode*, sm_MaxNumNodes> m_Nodes;
};

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
// PURPOSE: Specialized iterator, validates tree structure
class crmtValidator : public crmtIterator
{
public:

	// PURPOSE: Default constructor
	crmtValidator(const crmtMotionTree& motionTree);

	// PURPOSE: Destructor
	virtual ~crmtValidator();

	// PURPOSE: Iterator start override
	virtual void Start();

	// PURPOSE: Iterator finish override
	virtual void Finish();

	// PURPOSE: Iterator pre-visit override
	virtual void PreVisit(crmtNode&);

	// PURPOSE: Iterator visit override
	virtual void Visit(crmtNode&);


	// PURPOSE: Is the tree valid (call after iterating!)
	// RETURNS: true - tree structure was valid, false - problems found
	bool IsValid() const;


	// PURPOSE: Get current motion tree
	const crmtMotionTree* GetMotionTree() const;


	// PURPOSE: Report a validation problem
	void Reportf(const char* fmt, ...);

private:

	const crmtMotionTree* m_MotionTree;
	int m_Depth;
	int m_MaxDepth;
	bool m_Valid;
	bool m_Dump;
};

////////////////////////////////////////////////////////////////////////////////

inline bool crmtValidator::IsValid() const
{
	return m_Valid;
}

////////////////////////////////////////////////////////////////////////////////

inline const crmtMotionTree* crmtValidator::GetMotionTree() const
{
	return m_MotionTree;
}

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Specialized iterator, defines interface for dumping out tree structure
class crmtDumper : public crmtIterator
{
public:

	// PURPOSE: Default constructor
	crmtDumper();

	// PURPOSE: Destructor
	virtual ~crmtDumper();

	// PURPOSE: Iterator start override
	virtual void Start();

	// PURPOSE: Iterator finish override
	virtual void Finish();

	// PURPOSE: Iterator pre-visit override
	virtual void PreVisit(crmtNode&);

	// PURPOSE: Iterator visit override
	virtual void Visit(crmtNode&);

	
	// PURPOSE: Output text (will be properly formatted and indented automatically)
	// PARAM: verbosity - verbosity level [0..3]
	virtual void Outputf(int verbosity, const char* field, const char* fmt, ...) = 0;

	// PURPOSE: Output text (will be properly formatted and indented automatically)
	// PARAM: verbosity - verbosity level [0..3]
	virtual void Outputf(int verbosity, const char* field, int element, const char* fmt, ...) = 0;

	// PURPOSE: Output text (will be properly formatted and indented automatically)
	// PARAM: verbosity - verbosity level [0..3]
	virtual void Outputf(int verbosity, const char* field, int element, int subelement, const char* fmt, ...) = 0;

	// PURPOSE: Push indent
	virtual void PushLevel(const char* name) = 0;

	// PURPOSE: Pop indent
	virtual void PopLevel() = 0;

private:
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Implements dumping out tree structure, using dump output object
class crmtDumperOutput : public crmtDumper
{
public:

	// PURPOSE: Default constructor
	// PARAMS: output - dump output object used to gather, format and output structure
	crmtDumperOutput(crDumpOutput& output);

	// PURPOSE: Override to implement output
	virtual void Outputf(int verbosity, const char* field, const char* fmt, ...);

	// PURPOSE: Override to implement output
	virtual void Outputf(int verbosity, const char* field, int element, const char* fmt, ...);

	// PURPOSE: Override to implement output
	virtual void Outputf(int verbosity, const char* field, int element, int subelement, const char* fmt, ...);

	// PURPOSE: Push indent
	virtual void PushLevel(const char* name);

	// PURPOSE: Pop indent
	virtual void PopLevel();

private:

	crDumpOutput* m_Output;
};
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_ITERATOR_H
