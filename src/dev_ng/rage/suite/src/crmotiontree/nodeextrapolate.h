//
// crmotiontree/nodeextrapolate.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEEXTRAPOLATE_H
#define CRMOTIONTREE_NODEEXTRAPOLATE_H

#include "node.h"

namespace rage
{

class crFrame;

// PURPOSE: Extrapolates ceased input for short period of time.
class crmtNodeExtrapolate : public crmtNodeParent
{
public:

	// PURPOSE: Default constructor
	crmtNodeExtrapolate();

	// PURPOSE: Resource constructor
	crmtNodeExtrapolate(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeExtrapolate();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeExtrapolate);

	// PURPOSE: Initialization
	// PARAMS:
	// damping - damping to use during extrapolation stage (< 0 and >= 1, 1 indicates no damping)
	void Init(float damping);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh();

	// PURPOSE: Get last frame to use
	// RETURNS: pointer to last frame to use (can be NULL)
	crFrame* GetLastFrame() const;

	// PURPOSE: Get delta frame to use
	// RETURNS: pointer to frame to use (can be NULL)
	crFrame* GetDeltaFrame() const;

	// PURPOSE: Set damping value to use during extrapolation
	// PARAMS: damping - damping value (>0 and <=1, 1 indicates no damping)
	void SetDamping(float damping);

	// PURPOSE: Get damping value used during extrapolation
	float GetDamping() const;

	// PURPOSE: Get maximum angular velocity used during extrapolation
	float GetMaxAngularVelocity() const;

private:
	float m_Damping;
	float m_MaxAngularVelocity;
	crFrame* m_LastFrame;
	crFrame* m_DeltaFrame;
};

////////////////////////////////////////////////////////////////////////////////

inline crFrame* crmtNodeExtrapolate::GetLastFrame() const
{
	return m_LastFrame;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame* crmtNodeExtrapolate::GetDeltaFrame() const
{
	return m_DeltaFrame;
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtNodeExtrapolate::GetDamping() const
{
	return m_Damping;
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtNodeExtrapolate::GetMaxAngularVelocity() const
{
	return m_MaxAngularVelocity;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRMOTIONTREE_NODEEXTRAPOLATE_H
