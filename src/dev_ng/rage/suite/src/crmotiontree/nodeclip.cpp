//
// crmotiontree/nodeclip.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodeclip.h"

#include "crmtdiag.h"
#include "motiontree.h"
#include "observer.h"

#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "crclip/clipanimations.h"
#include "crclip/clipanimationexpression.h"
#include "crmetadata/tag.h"
#include "crskeleton/skeleton.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeClip::crmtNodeClip()
: crmtNode(kNodeClip)
, m_ClipPlayer()
, m_LoopIds(0)
, m_ZeroWeight(false)
{
	m_ClipPlayer.SetFunctor(MakeFunctor(*this, &crmtNodeClip::Refresh));
	m_TagTriggerFunctor = MakeFunctor(*this, &crmtNodeClip::TagTriggerCallback);
	m_LoopIdFunctor = MakeFunctorRet(*this, &crmtNodeClip::LoopIdCallback);
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeClip::crmtNodeClip(datResource& rsc)
: crmtNode(rsc)
, m_ClipPlayer(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeClip::~crmtNodeClip()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeClip, crmtNode::kNodeClip, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeClip::Init(const crClip* clip, float time, float rate)
{
	m_ClipPlayer.SetClip(clip);
	m_ClipPlayer.SetTime(time);
	m_ClipPlayer.SetRate(rate);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeClip::Shutdown()
{
	ShuttingDown();
	crmtNode::Shutdown();

	m_ClipPlayer.SetClip(NULL);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeClip::Update(float dt)
{
	float unusedTime;
	bool hasLoopedOrEnded;

	m_ClipPlayer.Update(dt, unusedTime, hasLoopedOrEnded, &m_TagTriggerFunctor, m_LoopIds?&m_LoopIdFunctor:NULL);

	if(hasLoopedOrEnded)
	{
		if(m_ClipPlayer.IsLooped())
		{
			MessageObservers(crmtMessage(kMsgClipLooped));
		}
		else
		{
			// TODO --- may want to do something with unused time here...
			MessageObservers(crmtMessage(kMsgClipEnded));
		}
	}

	SetDisabled(m_ClipPlayer.GetClip() == NULL);
	SetSilent(false);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeClip::Dump(crmtDumper& dumper) const
{
	crmtNode::Dump(dumper);

	dumper.Outputf(2, "clip", "'%s'", (const char*)(m_ClipPlayer.HasClip()?m_ClipPlayer.GetClip()->GetName():"NONE"));
	dumper.Outputf(2, "time", "%.3f", m_ClipPlayer.GetTime());
	dumper.Outputf(2, "duration", "%.3f", m_ClipPlayer.HasClip()?m_ClipPlayer.GetClip()->GetDuration():0.f);
	dumper.Outputf(2, "looped", "%d", m_ClipPlayer.IsLooped());
	dumper.Outputf(2, "rate", "%.3f",  m_ClipPlayer.GetRate());
	dumper.Outputf(2, "loop ids", "%x", m_LoopIds);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNodeClip::Refresh()
{
	// retrieve animation and expression from clip
	unsigned int numAnims = 0;
	const unsigned int maxAnims = 8;
	const crAnimation* anims[maxAnims];
	const crExpressions* exprs = NULL;
	const crClip* clip = m_ClipPlayer.GetClip();
	if(clip)
	{
		crClip::eClipType clipType = clip->GetType();
		switch(clipType)
		{
		case crClip::kClipTypeAnimationExpression:
			exprs = static_cast<const crClipAnimationExpression*>(clip)->GetExpressions();
			// fall through

		case crClip::kClipTypeAnimation:
			{
				const crAnimation* anim = static_cast<const crClipAnimation*>(clip)->GetAnimation();
				if(anim)
				{
					anims[numAnims++] = anim;
				}
			}
			break;

		case crClip::kClipTypeAnimations:
			{
				const crClipAnimations* clipAnims = static_cast<const crClipAnimations*>(clip);
				for(unsigned int i=0; i < Min(clipAnims->GetNumAnimations(), maxAnims); i++)
				{
					const crAnimation* anim = clipAnims->GetAnimation(i);
					if(anim)
					{
						anims[numAnims++] = anim;
					}
				}
			}
			break;

		default: Assertf(false, "Unsupported clip type %u", clipType);
		}
	}

	// create animation accelerator indices
	if(numAnims)
	{
		GetMotionTree().GetFrameAccelerator().FindFrameAnimIndices(m_AnimIndices, GetMotionTree().GetFrameData(), numAnims, anims);
	}
	else
	{
		m_AnimIndices.Unlock();
	}

	// create expression accelerator indices
	if(exprs)
	{
		crmtMotionTree& motionTree = GetMotionTree();
		exprs->FindExpressionFrameIndices(m_ExprFrameIndices, motionTree.GetFrameAccelerator(), motionTree.GetFrameData());
		exprs->FindExpressionSkelIndices(m_ExprSkelIndices, motionTree.GetFrameAccelerator(), motionTree.GetSkeleton()->GetSkeletonData());
	}
	else
	{
		m_ExprFrameIndices.Unlock();
		m_ExprSkelIndices.Unlock();
	}
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeClip::TagMsgPayload::TagMsgPayload(const crClip* clip, const crTag* tag, float phase)
: m_Clip(clip)
, m_Tag(tag)
, m_Phase(phase)
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeClip::TagTriggerCallback(const crTag& tag, bool enterOrExit, bool enter, float phase)
{
	crmtDebug3("Clip node tag trigger '%s' #%08x enter %d phase %f\n", tag.GetName(), u32(tag.GetKey()), enter, phase);

	u32 msgId = enterOrExit?(enter?kMsgClipTagEnter:kMsgClipTagExit):kMsgClipTagUpdate;
	TagMsgPayload payload(m_ClipPlayer.GetClip(), &tag, phase);

	MessageObservers(crmtMessage(msgId, reinterpret_cast<void*>(&payload)));
}

////////////////////////////////////////////////////////////////////////////////

bool crmtNodeClip::LoopIdCallback(u32 loopId)
{
	crmtDebug3("Clip node loop id %d callback\n", loopId);

	return ((1<<loopId)|m_LoopIds) != 0;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
