//
// crmotiontree/composer.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_COMPOSER_H
#define CRMOTIONTREE_COMPOSER_H

#include "cranimation/animation_config.h"
#include "cranimation/frameaccelerator.h"
#include "system/dependency.h"
#include "system/dependency_spu.h"
#include "vectormath/vectormath.h"
#include "vectormath/vec4v.h"

namespace rage
{

class crAnimation;
class crmtComposerData;
class crExpressions;
class crFrame;
class crFrameData;
class crFrameFilter;
class crIKSolverBase;
class crKinematics;
class crmtMotionTreeScheduler;
class crmtNode;
class crpmParameterizedMotion;
class sysDependencyScheduler;

///////////////////////////////////////////////////////////////////////

// Composer that creates a dependency tree from a motion node tree
// Accelerator indices must be locked inside the motion tree node
class crmtComposer
{
public:
	// PURPOSE: Constructor
	crmtComposer(const crFrameData& frameData, const crmtComposerData& data, u8* scratchBuffer, u32 scratchSize);

	// PURPOSE: Compose dependencies from the motion tree nodes
	bool Compose(const crmtNode* rootNode);

	// PURPOSE: Commit dependencies
	bool Commit(crHeap* heap, sysDependency* parentDep, u32 priority);

private:
	// PURPOSE: Compute frame offsets
	u32 ComputeFrameOffsets(u32* offsets);

	// PURPOSE: Create dependencies for node
	void Visit(const crmtNode& refNode, const crmtNode& node);

	enum EPairType { kPairBlend, kPairMerge, kPairAdd };

	// PURPOSE: Push/pop proxied node from the cache
	void PushProxyNode(const crmtNode* nodeRef);
	bool PopProxyNode(const crmtNode* nodeRef);

	// PURPOSE: Push dependencies
	void PopFrame(crFrame* destFrame, const crLock& indices);
	void PopSkeleton(bool suppressReset);
	u32 PushAnimation(const crAnimation* anim, f32 time, f32 delta, f32 rangeStart, f32 rangeEnd, u32 sizeIndices, const u8* indices, u32 indiceOffset=0);
	void PushExpressions(const crExpressions* expressions, f32 time, f32 deltaTime, f32 weight, const Vec4V* variables, u32 numVariables, const crLock& frameIndices, const crLock& skelIndices);
	void PushFilter(const crLock& weights);
	void PushFrame(const crFrame* srcFrame, const crLock& indices);
	void PushIdentity();
	void PushInvalid();
	void PushNormalize();
	void PushPair(EPairType pairType, f32 weight, bool merge, u32 filterSize=0, const u8* filter=NULL);
	void PushSkeleton(bool localPoseOnly, const crLock& indices);
	void PoseBounds(bool copyLastMatrices);
	void PushIK(u32 numSolvers, crIKSolverBase** solvers);
	void PushPhysical();
	void PushExtrapolate(float damping, float maxAngularVelocity, crFrame* lastFrame, crFrame* deltaFrame);
	void PushNodePm(const crmtNode& node);
	void PushNodeN(const crmtNode& node, u32 numFrames);
	void PushNodeMirror(const crmtNode& node);
	void PushShaderVars();

	static const u32 sm_MaxNumDependencies = 256;
	static const u32 sm_MaxStackSize = 65;
	static const u32 sm_MaxNumFrames = 128;
	static const u32 sm_MaxUsedFrames = 12;
	static const u32 sm_MaxNumProxies = 56;
	static const u32 sm_MinDependencies = 8;
	static const u32 sm_MinFrames = 4;

	crScratch m_Scratch;
	sysDependency* m_Dependencies;
	sysDependency* m_DepStack[sm_MaxStackSize];
	sysDependency* m_FrameAllocators[sm_MaxNumFrames];
	sysDependency* m_FrameReleasers[sm_MaxNumFrames];
	u32 m_FrameStack[sm_MaxStackSize];

	u32 m_NumDeps, m_NumFrames;
	s32 m_TopDep, m_TopFrame;

	sysDependency* m_ProxyDeps[sm_MaxNumProxies];
	const crmtNode* m_ProxyNodes[sm_MaxNumProxies];
	u32 m_ProxyFrames[sm_MaxNumProxies];
	u32 m_NumProxies;

	const crFrameData& m_FrameData;
	const crmtComposerData& m_Data;

	friend class crmtMotionTreeScheduler;
};

///////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_COMPOSER_H
