//
// crmotiontree/managersinglestate.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "managersinglestate.h"

#include "motiontree.h"
#include "requestblend.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtManagerSingleState::crmtManagerSingleState()
: m_CurrentState(NULL)
, m_LastBlend(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtManagerSingleState::crmtManagerSingleState(crmtMotionTree& tree, const crmtObserver* location)
: crmtManager(tree, location)
, m_CurrentState(NULL)
, m_LastBlend(NULL)
{
	InternalInit(location);
}

////////////////////////////////////////////////////////////////////////////////

crmtManagerSingleState::~crmtManagerSingleState()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtManagerSingleState::crmtManagerSingleState(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crmtManagerSingleState);

#if __DECLARESTRUCT
void crmtManagerSingleState::DeclareStruct(datTypeStruct& s)
{
	crmtManager::DeclareStruct(s);
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crmtManagerSingleState::Init(crmtMotionTree& tree, const crmtObserver* location)
{
	crmtManager::Init(tree, location);
	
	InternalInit(location);
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerSingleState::Shutdown()
{
	if(m_CurrentState)
	{
		m_CurrentState->Release();
		m_CurrentState = NULL;
	}
	if(m_LastBlend)
	{
		m_LastBlend->Release();
		m_LastBlend = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

const crmtObserver* crmtManagerSingleState::GetRoot() const
{
	Assert(m_CurrentState);
	Assert(m_LastBlend);

	if(m_LastBlend->IsAttached())
	{
		return m_LastBlend;
	}

	return m_CurrentState;
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerSingleState::RequestNewState(crmtRequest& request, float crossFadeDuration)
{
	Assert(crossFadeDuration >= 0.f);

	if(crossFadeDuration > 0.f && m_CurrentState->IsAttached())
	{
		crmtRequestBlend requestBlend;
		requestBlend.Init(request, 0.f, 1.f / crossFadeDuration);
		GetMotionTree().Request(requestBlend, GetRoot());
		
		m_LastBlend->Attach(requestBlend.GetObserver());
	}
	else
	{
		GetMotionTree().Request(request, GetRoot());

		m_LastBlend->Detach();
	}

	m_CurrentState->Attach(request.GetObserver());
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerSingleState::RequestNewState(crmtRequest& request, crmtRequestBlend& crossfadeRequest)
{
	crossfadeRequest.SetDestroyWhenComplete(true);
	crossfadeRequest.SetSource(NULL, 0);
	crossfadeRequest.SetSource(&request, 1);

	GetMotionTree().Request(crossfadeRequest, GetRoot());

	m_CurrentState->Attach(request.GetObserver());
	m_LastBlend->Attach(crossfadeRequest.GetObserver());
}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerSingleState::InternalInit(const crmtObserver* location)
{
	m_CurrentState = rage_new crmtObserver();
	m_CurrentState->AddRef();
	if(location)
	{
		m_CurrentState->Attach(*location);
	}

	m_LastBlend = rage_new crmtObserver();
	m_LastBlend->AddRef();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
