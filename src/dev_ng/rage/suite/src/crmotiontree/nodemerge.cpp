//
// crmotiontree/nodemerge.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "nodemerge.h"

#include "motiontree.h"

#include "cranimation/framefilters.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeMerge::crmtNodeMerge()
: crmtNodePair(kNodeMerge)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeMerge::crmtNodeMerge(datResource& rsc)
: crmtNodePair(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeMerge::~crmtNodeMerge()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeMerge, crmtNode::kNodeMerge, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeMerge::Init(crFrameFilter* filter)
{
	SetFilter(filter);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeMerge::Shutdown()
{
	ShuttingDown();

	crmtNodePair::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeMerge::Update(float dt)
{
	crmtNodePair::Update(dt);

	crmtNode* child0 = GetFirstChild();
	crmtNode* child1 = child0->GetNextSibling();

	bool disabled = false, silent = false;
	if(child0->IsDisabled() && child1->IsDisabled())
	{
		disabled = true;
	}
	else if(!m_Filter && child0->IsDisabled())
	{
		child0->SetSilent(true);
		silent = true;
	}
	else if(child1->IsDisabled())
	{
		child1->SetSilent(true);
		silent = true;
	}

	SetDisabled(disabled);
	SetSilent(silent);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeMerge::Dump(crmtDumper& dumper) const
{
	crmtNodePair::Dump(dumper);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
