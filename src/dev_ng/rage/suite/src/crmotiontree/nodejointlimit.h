//
// crmotiontree/nodejointlimit.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEJOINTLIMIT_H
#define CRMOTIONTREE_NODEJOINTLIMIT_H

#include "node.h"

namespace rage
{

class crFrameFilter;
class crJointData;

// PURPOSE: Node that applies limit on the rotation and translation of joints
class crmtNodeJointLimit : public crmtNodeParent
{
public:

	// PURPOSE: Default constructor
	crmtNodeJointLimit();

	// PURPOSE: Resource constructor
	crmtNodeJointLimit(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeJointLimit();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeJointLimit);

	// PURPOSE: Initialization
	// filter - frame filter to use for this blend (default NULL)
	// useJointData - request to use the joint data
	void Init(const crJointData& jointData, crFrameFilter* filter=NULL);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Override maximum number of children this node can support
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: Override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;

	// PURPOSE: Set the joint data
	// PARAMS: jointData - joint data
	void SetJointData(const crJointData& jointData);

	// PURPOSE: Set the frame filter for this operation
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);

private:
	const crJointData* m_JointData;
	crFrameFilter* m_Filter;
};

} // namespace rage

#endif //CRMOTIONTREE_NODEJOINTLIMIT_H
