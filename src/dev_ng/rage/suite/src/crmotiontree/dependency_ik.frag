//
// crmotiontree/dependency_ik.frag
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_IK_FRAG
#define MOTIONTREE_DEPENDENCY_IK_FRAG

#include "crbody/iksolver.h"
#include "crbody/kinematics.h"
#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"
#include "system/dependency_spu.h"
#include "system/dma.h"

#if __SPU
#include "crbody/iksolver.cpp"
#include "crskeleton/jointdata.cpp"

#include "math/angmath.cpp"
#endif

using namespace rage;

SPUFRAG_DECL(bool, dependency_ik, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_ik, const sysDependency& dep)
{
	crmtDebug3("dependency_ik");

	u8* scratchBuffer = static_cast<u8*>(dep.m_Params[0].m_AsPtr);
	crIKSolverBase** solverPtrs = static_cast<crIKSolverBase**>(dep.m_Params[1].m_AsPtr);
	const s16* parentIndices = static_cast<const s16*>(dep.m_Params[2].m_AsConstPtr);
	const Mat34V* parent = static_cast<const Mat34V*>(dep.m_Params[3].m_AsConstPtr);
	Mat34V* locals = static_cast<Mat34V*>(dep.m_Params[4].m_AsPtr);
	unsigned int numBones = dep.m_Params[5].m_AsInt;
	unsigned int numSolvers = dep.m_Params[6].m_AsInt;
	unsigned int scratchSize = dep.m_DataSizes[0];

	crScratch scratch;
	scratch.Init(scratchBuffer, scratchSize);

	// transfer solvers
	const crIKSolverBase* solvers[crKinematics::sm_MaxNumSolvers];
	const unsigned int maxSolverSize = MAX_RAGE_SOLVER_SIZE;
	for(u32 i=0; i < numSolvers; i++)
	{
		solvers[i] = static_cast<const crIKSolverBase*>(scratch.Push(solverPtrs[i], maxSolverSize));
	}

	// wait for solvers transfer
	scratch.Wait();

	// execute solvers
	crIKSolverBase::SolverHelper sh;
	sh.m_ParentIndices = parentIndices;
	sh.m_Locals = locals;
	sh.m_Objects = locals + numBones;
	sh.m_Parent = *parent;
	sh.m_NumBones = numBones;

	for(u32 i=0; i < numSolvers; i++)
	{
		const crIKSolverBase* solver = solvers[i];
		switch(solver->GetType())
		{
#if USE_RAGE_SOLVERS
		case crIKSolverBase::IK_SOLVER_TYPE_SIMPLE_SPINE:	static_cast<const crIKSolverSimpleSpine*>(solver)->crIKSolverSimpleSpine::Solve(sh); break;
		case crIKSolverBase::IK_SOLVER_TYPE_SIMPLE_HEAD:	static_cast<const crIKSolverSimpleHead*>(solver)->crIKSolverSimpleHead::Solve(sh); break;
		case crIKSolverBase::IK_SOLVER_TYPE_GLEICHER_LIMB:	static_cast<const crIKSolverLimbBase*>(solver)->crIKSolverLimbBase::Solve(sh); break;
		case crIKSolverBase::IK_SOLVER_TYPE_ITERATIVE_LIMB: static_cast<const crIKSolverLimbBase*>(solver)->crIKSolverLimbBase::Solve(sh); break;
#endif // USE_RAGE_SOLVERS
		case crIKSolverBase::IK_SOLVER_TYPE_ARMS:			static_cast<const crIKSolverArms*>(solver)->crIKSolverArms::Solve(sh); break;
		case crIKSolverBase::IK_SOLVER_TYPE_LEGS:			static_cast<const crIKSolverLegs*>(solver)->crIKSolverLegs::Solve(sh); break;
		case crIKSolverBase::IK_SOLVER_TYPE_QUADRUPED:		static_cast<const crIKSolverQuadruped*>(solver)->crIKSolverQuadruped::Solve(sh); break;
		default: break;
		}
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_IK_FRAG
