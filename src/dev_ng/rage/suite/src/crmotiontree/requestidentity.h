//
// crmotiontree/requestidentity.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTIDENTITY_H
#define CRMOTIONTREE_REQUESTIDENTITY_H

#include "request.h"

namespace rage
{


// PURPOSE: Identity request
// Uses skeleton data or creature to generate identity
class crmtRequestIdentity : public crmtRequest
{
public:

	// PURPOSE: Default/Initializing constructor
	crmtRequestIdentity();

	// PURPOSE: Destructor
	virtual ~crmtRequestIdentity();

	// PURPOSE: Initializer
	void Init();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
};


} // namespace rage

#endif // CRMOTIONTREE_REQUESTIDENTITY_H
