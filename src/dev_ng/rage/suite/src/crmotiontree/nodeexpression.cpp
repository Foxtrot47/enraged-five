//
// crmotiontree/nodeexpression.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodeexpression.h"

#include "motiontree.h"

#include "crextra/expressions.h"
#include "crskeleton/skeleton.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeExpression::crmtNodeExpression()
: crmtNodeParent(kNodeExpression)
, m_Weight(1.f)
{
	m_ExpressionPlayer.SetFunctor(MakeFunctor(*this, &crmtNodeExpression::Refresh));
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeExpression::crmtNodeExpression(datResource& rsc)
: crmtNodeParent(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeExpression::~crmtNodeExpression()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeExpression, crmtNode::kNodeExpression, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeExpression::Init(const crExpressions* expressions)
{
	m_ExpressionPlayer.SetExpressions(expressions);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeExpression::Shutdown()
{
	ShuttingDown();
	m_ExpressionPlayer.Shutdown();

	crmtNodeParent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeExpression::Update(float dt)
{
	m_ExpressionPlayer.Update(dt);

	SetDisabled(false);
	SetSilent(false);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeExpression::Dump(crmtDumper& dumper) const
{
	crmtNodeParent::Dump(dumper);

	dumper.Outputf(2, "expressions", "'%s'", (m_ExpressionPlayer.GetExpressions()?m_ExpressionPlayer.GetExpressions()->GetName():"NONE"));
	dumper.Outputf(2, "enabled", "%d", m_ExpressionPlayer.IsEnabled());
	dumper.Outputf(2, "weight", "%f", m_Weight);
	dumper.Outputf(2, "numvariables", "%d", m_ExpressionPlayer.GetNumVariables());
	for(u32 i=0; i<m_ExpressionPlayer.GetNumVariables(); ++i)
	{
		dumper.Outputf(2, "hash", i, "%x", m_ExpressionPlayer.GetExpressions()?m_ExpressionPlayer.GetExpressions()->GetVariableByIndex(i):0);
		dumper.Outputf(2, "value", i, "%f %f %f %f", m_ExpressionPlayer.GetVariablesPtr()[i].GetXf(), m_ExpressionPlayer.GetVariablesPtr()[i].GetYf(), m_ExpressionPlayer.GetVariablesPtr()[i].GetZf(), m_ExpressionPlayer.GetVariablesPtr()[i].GetWf());
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNodeExpression::Refresh()
{
	const crExpressions* expressions = m_ExpressionPlayer.GetExpressions();
	if(expressions)
	{
		crmtMotionTree& motionTree = GetMotionTree();
		expressions->FindExpressionFrameIndices(m_FrameIndices, motionTree.GetFrameAccelerator(), motionTree.GetFrameData());
		expressions->FindExpressionSkelIndices(m_SkelIndices, motionTree.GetFrameAccelerator(), motionTree.GetSkeleton()->GetSkeletonData());
	}
	else
	{
		m_FrameIndices.Unlock();
		m_SkelIndices.Unlock();
	}
}
////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeExpression::GetMinNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeExpression::GetMaxNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

crExpressionPlayer& crmtNodeExpression::GetExpressionPlayer()
{
	return m_ExpressionPlayer;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeExpression::SetWeight(float weight)
{
	m_Weight = Clamp(weight, 0.f, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
