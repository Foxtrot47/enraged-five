//
// crmotiontree/nodeproxy.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodeproxy.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeProxy::crmtNodeProxy()
: crmtNode(kNodeProxy)
{
	m_Observer.AddRef();
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeProxy::crmtNodeProxy(datResource& rsc)
: crmtNode(rsc)
{
	m_Observer.AddRef();
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeProxy::~crmtNodeProxy()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeProxy, crmtNode::kNodeProxy, 0);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeProxy::Init(crmtNode* node)
{
	SetProxyNode(node);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeProxy::Shutdown()
{
	ShuttingDown();
	SetProxyNode(NULL);

	crmtNode::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeProxy::Dump(crmtDumper& dumper) const
{
	crmtNode::Dump(dumper);

	const crmtNode* node = m_Observer.GetNode();
	dumper.Outputf(2, "proxy", "%p", node);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNodeProxy::SetProxyNode(crmtNode* node)
{
	if(node)
	{
		m_Observer.Attach(*node);
	}
	else
	{
		m_Observer.Detach();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeProxy::ProxyObserver::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	if(IsAttached())
	{
		GetNode()->SetProxied(true);
	}

	crmtObserver::ResetMessageIntercepts(interceptor);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
