//
// crmotiontree/requestmergen.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTMERGEN_H
#define CRMOTIONTREE_REQUESTMERGEN_H

#include "requestblendn.h"

namespace rage
{


////////////////////////////////////////////////////////////////////////////////


// PURPOSE: N-way merge request
// Capable of merging multiple inputs into a single output
class crmtRequestMergeN : public crmtRequestN
{
public:

	// PURPOSE: Default constructor
	crmtRequestMergeN();

	// PURPOSE: Destructor
	virtual ~crmtRequestMergeN();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);
};

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CRMOTIONTREE_REQUESTMERGEN_H
