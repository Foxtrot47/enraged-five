//
// crmotiontree/nodeblendn.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodeblendn.h"

#include "cranimation/framedata.h"
#include "cranimation/framefilters.h"
#include "math/simplemath.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeN::crmtNodeN(eNodes nodeType)
: crmtNodeParent(nodeType)
, m_Filter(NULL)
, m_ZeroDest(false)
, m_NumChildren(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeN::crmtNodeN(datResource& rsc)
: crmtNodeParent(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeN::~crmtNodeN()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::Init(crFrameFilter* filter)
{
	SetFilter(filter);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::Shutdown()
{
	ShuttingDown();
	SetFilter(NULL);
	SetZeroDestination(false);

	crmtNodeParent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::Update(float dt)
{
	const u32 numChildren = GetNumChildren();
	for(u32 i=0; i<numChildren; ++i)
	{
		if(!IsNearZero(m_NData[i].m_WeightRate))
		{
			float oldWeight = m_NData[i].m_Weight;
			float newWeight = Clamp(oldWeight + m_NData[i].m_WeightRate * dt, 0.f, 1.f);

			m_NData[i].m_Weight = newWeight;

			if(IsNearZero(newWeight) && !IsNearZero(oldWeight))
			{
				MessageObservers(crmtMessage(kMsgNZeroWeight, &i));
			}
		}
	}

	bool allDisabled = !m_ZeroDest;
	for(u32 i=(m_ZeroDest?1:0); i<numChildren; ++i)
	{
		if(m_NData[i].m_Weight <= WEIGHT_ROUNDING_ERROR_TOLERANCE)
		{
			crmtNode* child = GetChild(i);
			child->SetDisabled(true);
			child->SetSilent(true);
		}
		else
		{
			allDisabled = false;
		}
	}

	SetDisabled(allDisabled);
	SetSilent(false);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeN::Dump(crmtDumper& dumper) const
{
	crmtNodeParent::Dump(dumper);

	for(u32 i=0; i<GetNumChildren(); ++i)
	{
		dumper.Outputf(2, "weight", i, "%f", m_NData[i].m_Weight);
		dumper.Outputf(2, "weightrate", i, "%f", m_NData[i].m_WeightRate);
		dumper.Outputf(2, "filter", i, "%p", m_NData[i].m_InputFilter);
	}

	dumper.Outputf(2, "filter", "%p", m_Filter);
	dumper.Outputf(2, "zerodest", "%d", m_ZeroDest);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeN::GetMinNumChildren() const
{
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeN::GetMaxNumChildren() const
{
	return sm_MaxChildren;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::SetWeight(u32 childIdx, float weight)
{
	m_NData[childIdx].m_Weight = weight; // Clamp(weight, 0.f, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::SetWeightRate(u32 childIdx, float weightRate)
{
	m_NData[childIdx].m_WeightRate = weightRate;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::SetFilter(crFrameFilter* filter)
{	
	if(filter)
	{
		filter->AddRef();
	}
	if(m_Filter)
	{
		m_Filter->Release();
	}
	m_Filter = filter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::SetInputFilter(u32 childIdx, crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	crFrameFilter*& currentFilter = m_NData[childIdx].m_InputFilter;
	if(currentFilter)
	{
		currentFilter->Release();
	}
	currentFilter = filter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::SetZeroDestination(bool zeroDest)
{
	m_ZeroDest = zeroDest;
}

////////////////////////////////////////////////////////////////////////////////

float crmtNodeN::GetWeight(u32 childIdx) const
{
	return m_NData[childIdx].m_Weight;
}

////////////////////////////////////////////////////////////////////////////////

float crmtNodeN::GetWeightRate(u32 childIdx) const
{
	return m_NData[childIdx].m_WeightRate;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtNodeN::GetInputFilter(u32 childIdx) const
{
	return m_NData[childIdx].m_InputFilter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::InsertData(u32 childIdx)
{
	for(u32 i=sm_MaxChildren-1; i>childIdx; --i)
	{
		m_NData[i] = m_NData[i-1];
	}

	m_NData[childIdx].Reset();
	m_NumChildren++;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::RemoveData(u32 childIdx)
{
	for(u32 i=childIdx+1; i<sm_MaxChildren; ++i)
	{
		m_NData[i-1] = m_NData[i];
	}

	m_NData[sm_MaxChildren-1].Reset();
	m_NumChildren--;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::SetNumChildren(u32 n)
{
	m_NumChildren = n;
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeN::NData::NData()
{
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeN::NData::Reset()
{
	m_Weight = 0.f;
	m_WeightRate = 0.f;
	m_InputFilter = NULL;
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeBlendN::crmtNodeBlendN()
: crmtNodeN(kNodeBlendN)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeBlendN::crmtNodeBlendN(datResource& rsc)
: crmtNodeN(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeBlendN::~crmtNodeBlendN()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeBlendN, crmtNode::kNodeBlendN, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeBlendN::Shutdown()
{
	ShuttingDown();
	crmtNodeN::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
