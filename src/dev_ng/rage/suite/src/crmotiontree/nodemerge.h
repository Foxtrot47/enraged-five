//
// crmotiontree/nodemerge.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEMERGE_H
#define CRMOTIONTREE_NODEMERGE_H

#include "node.h"
#include "nodeblend.h"

#include "system/nelem.h"

namespace rage
{

class crFrameFilter;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Merge pair of input child nodes into a single output
class crmtNodeMerge : public crmtNodePair
{
public:
	// PURPOSE: Default constructor
	crmtNodeMerge();

	// PURPOSE: Resource constructor
	crmtNodeMerge(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeMerge();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeMerge);

	// PURPOSE: Initialization
	// PARAMS: filter - frame filter to use for this blend (default NULL)
	void Init(crFrameFilter* filter=NULL);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Update
	virtual void Update(float dt);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: override influence query
	virtual float GetChildInfluence(u32 childIdx) const;

protected:
};

////////////////////////////////////////////////////////////////////////////////

inline float crmtNodeMerge::GetChildInfluence(u32 childIdx) const
{
	return ApplyInfluenceOverride(childIdx, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODEMERGE_H
