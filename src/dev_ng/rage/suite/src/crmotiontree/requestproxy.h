//
// crmotiontree/requestproxy.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTPROXY_H
#define CRMOTIONTREE_REQUESTPROXY_H

#include "request.h"

namespace rage
{

class crKinematics;

// PURPOSE: Proxy request
// Introduces graph link behavior into the motion tree
class crmtRequestProxy : public crmtRequest
{
public:

	// PURPOSE: Default constructor
	crmtRequestProxy();

	// PURPOSE: Initializing constructor
	// PARAMS: node - pointer to new proxy node, NULL for none.
	crmtRequestProxy(crmtNode* node);

	// PURPOSE: Destructor
	virtual ~crmtRequestProxy();

	// PURPOSE: Initializer
	// PARAMS: node - pointer to new proxy node, NULL for none.
	void Init(crmtNode* node);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get proxy node
	// RETURNS: pointer to proxy node (may be NULL)
	crmtNode* GetProxyNode() const;

	// PURPOSE: Set proxy node
	// PARAMS: node - pointer to new proxy node, NULL for none.
	// NOTES: Internally, node is held inside an observer.
	void SetProxyNode(crmtNode* node);
		
protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	crmtObserver m_Observer;
};


} // namespace rage

#endif // CRMOTIONTREE_REQUESTPROXY_H 	
