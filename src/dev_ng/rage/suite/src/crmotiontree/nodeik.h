//
// crmotiontree/nodeik.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEIK_H
#define CRMOTIONTREE_NODEIK_H

#include "node.h"

namespace rage
{

class crKinematics;

// PURPOSE: Applies IK.
class crmtNodeIk : public crmtNodeParent
{
public:

	// PURPOSE: Default constructor
	crmtNodeIk();

	// PURPOSE: Resource constructor
	crmtNodeIk(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeIk();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeIk);

	// PURPOSE: Initialization
	// PARAMS: kinematics - reference to the kinematics object
	void Init(crKinematics& kinematics);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Update
	virtual void Update(float dt);

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;


	// PURPOSE: Set the kinematics object
	// PARAMS: kinematics - reference to kinematics object
	void SetKinematics(crKinematics& kinematics);


	// PURPOSE: Get the kinematics object
	crKinematics* GetKinematics() const;
	

private:
	crKinematics* m_Kinematics;
};


////////////////////////////////////////////////////////////////////////////////

inline crKinematics* crmtNodeIk::GetKinematics() const
{
	return m_Kinematics;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRMOTIONTREE_NODEIK_H
