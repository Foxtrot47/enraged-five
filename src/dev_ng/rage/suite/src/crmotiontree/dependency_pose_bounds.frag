//
// crmotiontree/dependency_pose_bounds.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_POSE_BOUNDS_FRAG
#define MOTIONTREE_DEPENDENCY_POSE_BOUNDS_FRAG

#include "crmotiontree/crmtdiag.h"
#include "fragment/drawable.h"
#include "fragment/typechild.h"
#include "phbound/boundcomposite.h"
#include "system/dependency.h"

namespace rage
{

bool dependency_pose_bounds(const sysDependency& dep)
{
	crmtDebug3("dependency_pose_bounds");

	phBoundComposite* boundComposite = static_cast<phBoundComposite*>(dep.m_Params[0].m_AsPtr);
	Vec3V* localBoxMinMaxs = static_cast<Vec3V*>(dep.m_Params[1].m_AsPtr);
	const fragTypeChild** children = static_cast<const fragTypeChild**>(dep.m_Params[2].m_AsPtr);
	const s32* boundIndices = static_cast<const s32*>(dep.m_Params[3].m_AsConstPtr);
	const Mat34V* objects = static_cast<const Mat34V*>(dep.m_Params[4].m_AsConstPtr);
	Mat34V* lastBounds = static_cast<Mat34V*>(dep.m_Params[5].m_AsPtr);
	Mat34V* currentBounds = static_cast<Mat34V*>(dep.m_Params[6].m_AsPtr);
	u32 numBounds = dep.m_Params[7].m_AsInt;
	bool copyLastBounds = dep.m_Params[8].m_AsBool;

	// copy current to last bounds
	if(copyLastBounds)
	{
		sysMemCpy(lastBounds, currentBounds, numBounds*sizeof(Mat34V));
	}

	Vec3V boundingBoxMin = Vec3V(V_FLT_MAX);
	Vec3V boundingBoxMax = Vec3V(V_NEG_FLT_MAX);
	for(u32 boundIdx=0; boundIdx < numBounds; boundIdx++)
	{
		// compute current bound matrix from skeleton
		u32 boneIdx = boundIndices[boundIdx];

		// Don't propagate zero'd out bone matrices to the component matrices
		Mat34V objectMtx = objects[boneIdx];
		if(!IsZeroAll(objectMtx.GetCol0()))
		{
			const fragTypeChild* child = children[boundIdx];
			Mat34V componentMtx = RCC_MAT34V(child->GetUndamagedEntity()->GetBoundMatrix());

			Mat34V mtx;
			Transform(mtx, objectMtx, componentMtx);
			currentBounds[boundIdx] = mtx;

			// calculate axis-aligned bounding box using center and extents
			u32 twiceBoundIdx = boundIdx << 1;
			Vec3V boxMin = localBoxMinMaxs[twiceBoundIdx];
			Vec3V boxMax = localBoxMinMaxs[twiceBoundIdx + 1];

			Vec3V center = Average(boxMax, boxMin);
			Vec3V extents = Subtract(boxMax, center);

			Mat33V absolute = mtx.GetMat33();
			Abs(absolute);
			extents = Multiply(absolute, extents);
			center = Transform(mtx, center);

			boundingBoxMin = Min(boundingBoxMin, center - extents);
			boundingBoxMax = Max(boundingBoxMax, center + extents);
		}
	}

	Assert(IsLessThanOrEqualAll(boundingBoxMin, boundingBoxMax));
	Vec3V centroidOffset = Average(boundingBoxMax, boundingBoxMin);

	boundComposite->SetBoundingBoxMax(boundingBoxMax);
	boundComposite->SetBoundingBoxMin(boundingBoxMin);
	boundComposite->SetCentroidOffset(centroidOffset);
	boundComposite->SetRadiusAroundCentroid(Mag(boundingBoxMax - centroidOffset));

	return true;
}

} // namespace rage

#endif // MOTIONTREE_DEPENDENCY_POSE_BOUNDS_FRAG
