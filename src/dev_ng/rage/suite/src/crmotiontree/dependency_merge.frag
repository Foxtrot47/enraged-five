//
// crmotiontree/dependency_merge.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_MERGE_FRAG
#define MOTIONTREE_DEPENDENCY_MERGE_FRAG

#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"
#include "vectormath/vectormath.h"

using namespace rage;
using namespace Vec;

// dependency_merge pseudo-code
// 
// if(dest valid)
//		dest = dest
// else
//		dest = src

SPUFRAG_DECL(bool, dependency_merge, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_merge, const sysDependency& dep)
{
	crmtDebug3("dependency_merge");

	Vector_4V* RESTRICT pDest = static_cast<Vector_4V*>(dep.m_Params[0].m_AsPtr);
	const Vector_4V* RESTRICT pSrc = static_cast<const Vector_4V*>(dep.m_Params[1].m_AsConstPtr);

	unsigned int countQuads = RAGE_COUNT(dep.m_Params[4].m_AsInt, 2) + RAGE_COUNT(dep.m_Params[5].m_AsInt, 2);
	unsigned int countFloats = RAGE_COUNT(dep.m_Params[6].m_AsInt, 2);

	Vector_4V _invalid = V4VConstant(V_MASKXYZW);

	// merge quaternions and vectors
	for(; countQuads; countQuads--)
	{
		Vector_4V dest0 = pDest[0];
		Vector_4V dest1 = pDest[1];
		Vector_4V dest2 = pDest[2];
		Vector_4V dest3 = pDest[3];

		Vector_4V src0 = pSrc[0];
		Vector_4V src1 = pSrc[1];
		Vector_4V src2 = pSrc[2];
		Vector_4V src3 = pSrc[3];

		Vector_4V choice0 = V4IsEqualIntV(dest0, _invalid);
		Vector_4V choice1 = V4IsEqualIntV(dest1, _invalid);
		Vector_4V choice2 = V4IsEqualIntV(dest2, _invalid);
		Vector_4V choice3 = V4IsEqualIntV(dest3, _invalid);

		pDest[0] = V4SelectFT(choice0, dest0, src0);
		pDest[1] = V4SelectFT(choice1, dest1, src1);
		pDest[2] = V4SelectFT(choice2, dest2, src2);
		pDest[3] = V4SelectFT(choice3, dest3, src3);

		pDest += 4;
		pSrc += 4;
	}

	// merge floats
	for(; countFloats; countFloats--)
	{
		Vector_4V dest = *pDest;
		Vector_4V src = *pSrc;
		Vector_4V choice = V4IsEqualIntV(dest, _invalid);
		*pDest = V4SelectFT(choice, dest, src);
		pDest += 1;
		pSrc += 1;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_MERGE_FRAG
