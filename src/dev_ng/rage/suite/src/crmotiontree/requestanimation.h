//
// crmotiontree/requestanimation.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRMOTIONTREE_REQUESTANIMATION_H
#define CRMOTIONTREE_REQUESTANIMATION_H

#include "request.h"

namespace rage
{

class crAnimation;

// PURPOSE: Animation request
// Requests a motion tree construct an animation playback node.
class crmtRequestAnimation : public crmtRequest
{
public:

	// PURPOSE: Default constructor
	crmtRequestAnimation();

	// PURPOSE: Simple constructor, performs basic initialization
	// PARAMS: anim - pointer to animation to be played (can be NULL)
	// time - initial start time used to play animation (default 0.0)
	// rate - animation playback rate (can be positive, negative or zero, default 1.0)
	// NOTES: See Init()
	crmtRequestAnimation(const crAnimation* anim, float time=0.f, float rate=1.f);

	// PURPOSE: Destructor
	virtual ~crmtRequestAnimation();

	// PURPOSE: Basic initializer
	// PARAMS: anim - pointer to animation to be played (can be NULL)
	// time - initial start time used to play animation (default 0.0)
	// rate - playback rate (can be positive, negative or zero, default 1.0)
	void Init(const crAnimation* anim=NULL, float time=0.f, float rate=1.f);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Set animation to be played
	// PARAMS: anim - pointer to animation to be played (can be NULL)
	// NOTES: Requester uses AddRef and Release internally to control
	// reference counting on the animation.
	void SetAnimation(const crAnimation* anim);

	// PURPOSE: Set initial animation start time
	// PARAMS: time - initial start time used to play animation
	// NOTES: Value will be clamped to ensure within legal range
	// animation can support [0..duration]
	void SetTime(float time);

	// PURPOSE: Set playback rate
	// PARAMS: rate - animation playback rate (can be negative or zero)
	void SetRate(float rate);

	// PURPOSE: Set looping behavior
	// PARAMS: ignoreAnimLooping - override the animation's own looping status (default false)
	// forceLooping - force looping (only used if ignoreAnimLooping is true)
	void SetLooping(bool ignoreAnimLooping, bool forceLooping);

	// TODO ---- mover ids, mover update process


	// PURPOSE: Get animation to be played
	// RETURNS: pointer to animation to be played (can be NULL)
	const crAnimation* GetAnimation() const;

	// PURPOSE: Get initial animation start time
	// RETURNS: Initial start time used to play animation
	// NOTES: If animation is non-NULL, time will be in range [0..duration].
	float GetTime() const;

	// PURPOSE: Get playback rate
	// RETURNS: Animation playback rate (can be positive, negative or zero)
	float GetRate() const;

	// PURPOSE: Get the looping behavior and current status
	// PARAMS: outIgnoreAnimLooping - returns the "ignore the animation's own looping status" value.
	// outForceLooping - returns the "force looping (only used if ignoreAnimLooping is true)" value.
	// RETURNS: The net result of the two parameters, when combined with the animation's looping flag.
	bool GetLooping(bool& outIgnoreAnimLooping, bool& outForceLooping) const;

	// TODO --- mover update process

	// TODO --- add support for selection of different update times?

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	const crAnimation* m_Animation;
	float m_Time;
	float m_Rate;
	bool m_IgnoreAnimLooping;
	bool m_ForceLooping;
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTANIMATION_H
