//
// crmotiontree/motiontree.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_MOTIONTREE_H
#define CRMOTIONTREE_MOTIONTREE_H

#include "composerdata.h"

namespace rage
{

class crCreature;
class crDumpOutput;
class crFrameAccelerator;
class crFrameData;
class crFrameDataFactory;
class crSkeleton;
class crmtComposerData;
class crmtIterator;
class crmtNode;
class crmtNodeFactory;
class crmtNodeParent;
class crmtObserver;
class crmtRequest;

// PURPOSE: Motion tree instance.
// Motion trees represent the current state of a character,
// they manage the character update and composition process.
// The trees consist of a hierarchy of nodes, the structure
// of which is controlled by passing requests into the tree.
class crmtMotionTree
{
public:

	// PURPOSE: Default Constructor
	crmtMotionTree();

	// PURPOSE: Destructor
	~crmtMotionTree();

	// PURPOSE: Initialization
	// PARAMS:
	// creature - instance registered with the motion tree
	// finalize - automatically pose the creature in the motion tree finalize step
	// physical - simulate physical expressions
	// NOTES: Allocates dynamic memory
	void Init(crCreature& creature, fragInst* frag=NULL, bool finalize=true, bool physical=true);

	// PURPOSE: Shutdown
	// NOTES: Frees dynamic memory allocation
	void Shutdown();

	// PURPOSE: Reset contents of the tree
	// NOTES: Removes all the nodes from a tree
	void Reset();

	// PURPOSE: Update motion tree
	// PARAMS:
	// dt - time elapsed since last frame
	// NOTES: Call this every frame when updating your character
	void Update(float dt);

	// PURPOSE: Request changes in the tree
	// PARAMS: request - request structure
	// target - identifies node targeted to receive the request
	void Request(crmtRequest& request, const crmtObserver* target=NULL);

	// PURPOSE: Attach an observer to the top of the tree
	void AttachToRoot(crmtObserver& observer);

	// PURPOSE: Iterate over the nodes in the tree
	// PARAMS: iterator - iterator object
	void Iterate(crmtIterator& iterator) const;

	// PURPOSE: Refresh motion tree when the creature has changed
	void Refresh();

	// PURPOSE: Is tree empty (contains no nodes)
	// RETURNS: true - tree is empty, false - tree has root node
	bool IsEmpty() const;

	// PURPOSE: Get creature registered with motion tree
	// RETURNS: Pointer to creature (may be NULL if motion tree not initialized)
	crCreature* GetCreature() const;

	// PURPOSE: Get skeleton registered with motion tree
	// RETURNS: Pointer to skeleton (may be NULL if motion tree not initialized)
	crSkeleton* GetSkeleton() const;

	// PURPOSE: Set node factory
	// PARAMS: factory - node factory (can be NULL for none)
	// NOTES: caller remains responsible for any destruction of the factory
	void SetNodeFactory(crmtNodeFactory* factory);

	// PURPOSE: Get node factor
	// RETURNS: Pointer to current node factory (can be NULL for none)
	crmtNodeFactory* GetNodeFactory() const;

	// PURPOSE: Get frame accelerator
	crFrameAccelerator& GetFrameAccelerator();

	// PURPOSE: Get current frame data
	const crFrameData& GetFrameData() const;

	// PURPOSE: Get composer data
	crmtComposerData& GetComposerData();

	// PURPOES: Get root node
	crmtNode* GetRootNode();

	// PURPOSE: Initialize motion tree system
	static void InitClass();

	// PURPOSE: Shutdown motion tree system
	static void ShutdownClass();

#if CR_DEV
	// PURPOSE: Validate the tree and nodes
	// RETURNS: true - if tree structure valid, false - if problems found
	bool Validate() const;

	// PURPOSE: Dump out the tree and nodes (via dump output object).
	void Dump(crDumpOutput& output) const;

	// PURPOSE: Dump out the tree and nodes (to standard out).
	// PARAMS: verbosity - verbosity of output [0..3] (see notes)
	// buf - optional buffer (NULL buffer indicates direct output).
	// bufSize - optional buffer's size (must be specified if buffer is not NULL)
	// NOTES: verbosity levels; 0==one line per node, 1==terse, 2==full, 3==debug
	void Dump(int verbosity=2, char* buf=NULL, u32 bufSize=0) const;
#endif // CR_DEV

private:

	sysDependency m_Update;
	sysDependency m_Compose;
	sysDependency m_Recycle;
	crmtComposerData m_ComposerData;
	crmtNode* m_RootNode;
	crCreature* m_Creature;
	crmtNodeFactory* m_NodeFactory;
	u32 m_States[2];
	u32 m_Threads[2];

	static bool sm_InitClassCalled;

	friend class crmtNode;
	friend class crmtNodeParent;
	friend class crmtMotionTreeScheduler;
};

////////////////////////////////////////////////////////////////////////////////

inline bool crmtMotionTree::IsEmpty() const
{
	return m_RootNode == NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline crCreature* crmtMotionTree::GetCreature() const
{
	return m_Creature;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNodeFactory* crmtMotionTree::GetNodeFactory() const
{
	return m_NodeFactory;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtComposerData& crmtMotionTree::GetComposerData()
{
	return m_ComposerData;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode* crmtMotionTree::GetRootNode()
{
	return m_RootNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_MOTIONTREE_H
