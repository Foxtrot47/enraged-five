//
// crmotiontree/nodecapture.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodecapture.h"

#include "motiontree.h"

#include "cranimation/frame.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeCapture::crmtNodeCapture()
: crmtNodeParent(kNodeCapture)
, m_Frame(NULL)
, m_FrameSignature(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeCapture::crmtNodeCapture(datResource& rsc)
: crmtNodeParent(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeCapture::~crmtNodeCapture()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeCapture, crmtNode::kNodeCapture, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeCapture::Init(crFrame* frame)
{
	SetFrame(frame);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeCapture::Update(float)
{
	if(m_Frame && m_Frame->GetSignature() != m_FrameSignature)
	{
		Refresh();
	}

	SetDisabled(false);
	SetSilent(false);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeCapture::Shutdown()
{
	ShuttingDown();
	SetFrame(NULL);

	crmtNodeParent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeCapture::GetMaxNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeCapture::SetFrame(crFrame* frame)
{
	if(frame != m_Frame)
	{
		if(frame)
		{
			frame->AddRef();
		}
		if(m_Frame)
		{
			m_Frame->Release();
		}
		m_Frame = frame;

		Refresh();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeCapture::Refresh()
{
	if(m_Frame && m_Frame->GetFrameData())
	{
		GetMotionTree().GetFrameAccelerator().FindFrameIndices(m_Indices, *m_Frame->GetFrameData(), GetMotionTree().GetFrameData());
		m_FrameSignature = m_Frame->GetSignature();
	}
	else
	{
		m_Indices.Unlock();
		m_FrameSignature = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
