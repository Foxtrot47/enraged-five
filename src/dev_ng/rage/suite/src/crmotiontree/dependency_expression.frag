//
// crmotiontree/dependency_expression.frag
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_EXPRESSION_FRAG
#define MOTIONTREE_DEPENDENCY_EXPRESSION_FRAG

#include "crmotiontree/crmtdiag.h"
#include "crextra/expressionstream.h"
#include "system/dependency.h"
#include "system/dependency_spu.h"

#if __SPU
#include "crextra/expressionstream.cpp"
#endif

using namespace rage;

SPUFRAG_DECL(bool, dependency_expression, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_expression, const sysDependency& dep)
{
	crmtDebug3("dependency_expression");

	// fill expression stream helper
	ExprStreamHelper ph;
	ph.m_Frame = static_cast<u8*>(dep.m_Params[0].m_AsPtr);
	ph.m_Dofs = static_cast<const crFrameData::Dof*>(dep.m_Params[1].m_AsConstPtr);
	ph.m_Bones = static_cast<const crBoneData*>(dep.m_Params[2].m_AsConstPtr);

	const crExpressionStream* expression = static_cast<const crExpressionStream*>(dep.m_Params[3].m_AsConstPtr);
	ph.m_FrameIndices = static_cast<const u16*>(dep.m_Params[4].m_AsConstPtr);
	ph.m_SkelIndices = static_cast<const u16*>(dep.m_Params[5].m_AsConstPtr);
	ph.m_FrameSize = dep.m_DataSizes[0];
	ph.m_Variables = static_cast<Vector_4V*>(dep.m_Params[6].m_AsPtr);
	ph.m_NumVariables = dep.m_DataSizes[6] / sizeof(Vector_4V);
	ph.m_Time = dep.m_Params[7].m_AsFloat;
	ph.m_DeltaTime = dep.m_Params[8].m_AsFloat;
	ph.m_NumDofs = dep.m_DataSizes[1] / sizeof(crFrameData::Dof);
	ph.m_NumBones = dep.m_DataSizes[2] / sizeof(crBoneData);
	ph.m_NumIndices = dep.m_DataSizes[4] / sizeof(u16);

	expression->Process(ph);

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_EXPRESSION_FRAG
