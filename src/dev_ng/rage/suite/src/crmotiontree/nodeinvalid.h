//
// crmotiontree/nodeinvalid.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEINVALID_H
#define CRMOTIONTREE_NODEINVALID_H

#include "node.h"

namespace rage
{

// PURPOSE: Produces invalid (aka no-op) as output
class crmtNodeInvalid : public crmtNode
{
public:

	// PURPOSE: Default constructor
	crmtNodeInvalid();

	// PURPOSE: Resource constructor
	crmtNodeInvalid(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeInvalid();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeInvalid);

	// PURPOSE: Initialization
	void Init();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Update
	virtual void Update(float dt);

private:
};

} // namespace rage

#endif // CRMOTIONTREE_NODEINVALID_H
