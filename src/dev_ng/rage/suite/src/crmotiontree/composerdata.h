//
// crmotiontree/composerdata.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_COMPOSERDATA_H
#define CRMOTIONTREE_COMPOSERDATA_H

#include "system/dependency.h"
#include "system/dependency_spu.h"

#include "cranimation/frameaccelerator.h"
#include "cranimation/framedata.h"
#include "crextra/expressions.h"

namespace rage
{

class crFrame;
class crSkeleton;
class crmtMotionTree;
class fragInst;

///////////////////////////////////////////////////////////////////////

// PURPOSE: List of PPU callbacks for the composer
enum
{
	kComposerCallbackMoverDelta,
	kComposerCallbackNodePm,
	kComposerCallbackNodeMirror,
	kComposerCallbackNodeN,
	kComposerCallbackNodeExpressions
};

///////////////////////////////////////////////////////////////////////

// PURPOSE: Packaged data used by the composer
class crmtComposerData
{
public:
	friend class crmtComposer;

	// PURPOSE: Default constructor
	crmtComposerData();

	// PURPOSE: Initialization
	// PARAMS: 
	// finalize - automatically finalize the creature when the compose is finished
	// physical - simulate physical expressions
	void Init(crmtMotionTree& motionTree, fragInst* fragment, bool finalize, bool physical);

	// PURPOSE: Refresh composer when the creature or skeleton has changed
	void Refresh();

	// PURPOSE: Start the building
	void Start(float deltaTime);

	// PURPOSE: Finish the building
	void Finish();

	// PURPOSE: Get heap
	static crHeap& GetHeap();

private:
	// PURPOSE: Composer PPU callback
	static bool Callback(const sysDependency& dep);

	// PURPOSE: Implements PPU mover delta
	static bool ImplementMoverDelta(const sysDependency& dep);

	// PURPOSE: Implements PPU parameterized motion
	static bool ImplementNodePm(const sysDependency& dep);

	// PURPOSE: Implements mirror
	static bool ImplementNodeMirror(const sysDependency& dep);

	// PURPOSE: Implements PPU n-way node
	static bool ImplementNodeN(const sysDependency& dep);

#if CR_TEST_EXPRESSIONS
	// PURPOSE: Implements node expressions
	static bool ImplementNodeExpressions(const sysDependency& dep);
#endif

	struct Fragment
	{
		void Set(fragInst* frag);
		void Refresh();

		fragInst* m_Ptr;
		void** m_Children;
		void* m_BoundComposite;
		const Vec3V* m_LocalBoxMinMaxs;
		const s32* m_BoundIndices;
		Mat34V* m_LastMatrices;
		Mat34V* m_CurrentMatrices;
		s32 m_NumBounds;
		bool m_CopyLastMatrices;
	};

	static crHeap sm_Heap;

	Fragment m_Fragment;
	crSkeleton* m_Skeleton;
	crmtMotionTree* m_MotionTree;
	crFrame* m_ExtraDofsFrame;
	const Mat34V* m_Defaults;
	Mat34V* m_Locals;
	u8* m_LastFrame;
	const void* m_Parent;
	const void* m_Bones;
	const void* m_ParentIndices;
	const void* m_ChildParentIndices;
	void* m_Motions;
	void* m_ShaderVars;
	void* m_WorkBuffer;
	sysDependency::Callback* m_Callback;
	crLock m_SkelIndices;
	crLock m_ExtraDofIndices;
	crLock m_ExtraDofInverseIndices;
	f32 m_DeltaTime;
	u16 m_NumMotions;
	u16 m_NumBones;
	u16 m_NumChildParents;
	u16 m_NumShaderVars;
	u16 m_MoverTranslationOffset;
	u16 m_MoverRotationOffset;
	bool m_SuppressReset;
	bool m_Finalize;
	bool m_CreatureFinalize;
	bool m_Physical;
};

///////////////////////////////////////////////////////////////////////

inline crHeap& crmtComposerData::GetHeap()
{
	return sm_Heap;
}

///////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_COMPOSERDATA_H