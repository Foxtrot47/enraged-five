//
// crmotiontree/requestmergen.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "requestmergen.h"

#include "nodemergen.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestMergeN::crmtRequestMergeN()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestMergeN::~crmtRequestMergeN()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestMergeN::Shutdown()
{
	crmtRequestN::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestMergeN::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeMergeN* mergeNNode = crmtNodeMergeN::CreateNode(tree);
	mergeNNode->Init(m_Filter);
	mergeNNode->SetZeroDestination(m_ZeroDest);

	GenerateSourceNodes(tree, node, mergeNNode);

	u32 n=0;
	for(u32 i=0; i<crmtNodeN::sm_MaxChildren; ++i)
	{
		if(GetSource(i))
		{
			mergeNNode->SetWeight(n, m_Weights[i]);
			mergeNNode->SetWeightRate(n, m_WeightRates[i]);
			mergeNNode->SetInputFilter(n, m_InputFilters[i]);
			n++;
		}
	}
	mergeNNode->SetNumChildren(n);

	return mergeNNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
