//
// crmotiontree/requestfilter.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "requestfilter.h"

#include "nodefilter.h"

#include "cranimation/framefilters.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestFilter::crmtRequestFilter()
: m_Filter(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestFilter::crmtRequestFilter(crmtRequest& source, crFrameFilter* filter)
: m_Filter(NULL)
{
	Init(source, filter);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestFilter::crmtRequestFilter(crFrameFilter* filter)
: m_Filter(NULL)
{
	Init(filter);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestFilter::~crmtRequestFilter()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestFilter::Init(crmtRequest& source, crFrameFilter* filter)
{
	SetSource(&source, 0);
	SetFilter(filter);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestFilter::Init(crFrameFilter* filter)
{
	SetFilter(filter);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestFilter::Shutdown()
{
	SetFilter(NULL);

	crmtRequestSource<1>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtRequestFilter::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestFilter::SetFilter(crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	if(m_Filter)
	{
		m_Filter->Release();
	}
	m_Filter = filter;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestFilter::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeFilter* filterNode = crmtNodeFilter::CreateNode(tree);
	filterNode->Init(m_Filter);

	GenerateSourceNodes(tree, node, filterNode);

	return filterNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
