//
// crmotiontree/composerdata.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "composerdata.h"

#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/frameiterators.h"
#include "creature/componentextradofs.h"
#include "creature/componentphysical.h"
#include "creature/componentshadervars.h"
#include "creature/componentskeleton.h"
#include "creature/creature.h"
#include "crextra/expression.h"
#include "crextra/expressionstream.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeblendn.h"
#include "crmotiontree/nodemirror.h"
#include "crmotiontree/nodepm.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "fragment/cache.h"
#include "phbound/boundcomposite.h"

namespace rage
{

crHeap crmtComposerData::sm_Heap(CRMT_COMPOSER_HEAPSIZE);

///////////////////////////////////////////////////////////////////////

crmtComposerData::crmtComposerData()
: m_MotionTree(NULL)
, m_WorkBuffer(NULL)
{
	m_Callback = &crmtComposerData::Callback;
}

///////////////////////////////////////////////////////////////////////

void crmtComposerData::Init(crmtMotionTree& motionTree, fragInst* fragment, bool finalize, bool physical)
{
	m_MotionTree = &motionTree;
	m_Finalize = finalize;
	m_Physical = physical;
	m_Fragment.Set(physical ? fragment : NULL);
}

///////////////////////////////////////////////////////////////////////

void crmtComposerData::Refresh()
{
	crCreature* creature = m_MotionTree->GetCreature();

	// lock skeleton indices
	const crFrameData& frameData = m_MotionTree->GetFrameData();
	crFrameAccelerator& accelerator = m_MotionTree->GetFrameAccelerator();
	accelerator.FindFrameSkelIndices(m_SkelIndices, frameData, creature->GetSkeleton()->GetSkeletonData());

	// lock extra dofs indices
	crCreatureComponentExtraDofs* extraDofsComponent = creature->FindComponent<crCreatureComponentExtraDofs>();
	if(extraDofsComponent)
	{
		crFrame& poseFrame = extraDofsComponent->GetPoseFrame();
		accelerator.FindFrameIndices(m_ExtraDofIndices, *poseFrame.GetFrameData(), frameData);
		accelerator.FindFrameIndices(m_ExtraDofInverseIndices, frameData, *poseFrame.GetFrameData());
		m_ExtraDofsFrame = &poseFrame;
	}
	else
	{
		m_ExtraDofsFrame = NULL;
	}

	// cache mover frame offsets
	const crFrameData::Dof* moverTrans = frameData.FindDof(kTrackMoverTranslation, 0);
	const crFrameData::Dof* moverRot = frameData.FindDof(kTrackMoverRotation, 0);
	m_MoverTranslationOffset = moverTrans ? moverTrans->m_Offset : USHRT_MAX;
	m_MoverRotationOffset = moverRot ? moverRot->m_Offset : USHRT_MAX;

	// update skeleton data, could change at runtime
	m_Skeleton = creature->GetSkeleton();
	m_Parent = m_Skeleton->GetParentMtx();
	m_Locals = m_Skeleton->GetLocalMtxs();

	// copy skeleton flags
	crCreatureComponentSkeleton* skelComponent = creature->FindComponent<crCreatureComponentSkeleton>();
	if(skelComponent)
	{
		m_SuppressReset = skelComponent->GetSuppressReset();
	}
	else
	{
		m_SuppressReset = false;
	}

	// copy shader vars
	crCreatureComponentShaderVars* shadervarsComponent = creature->FindComponent<crCreatureComponentShaderVars>();
	if(shadervarsComponent)
	{
		m_ShaderVars = shadervarsComponent->GetShaderVarDofPairs();
		Assign(m_NumShaderVars, shadervarsComponent->GetNumShaderVarDofPairs());
	}
	else
	{
		m_ShaderVars = NULL;
		m_NumShaderVars = 0;
	}

	// copy physical simulations
	crCreatureComponentPhysical* physicalComponent = creature->FindComponent<crCreatureComponentPhysical>();
	if(physicalComponent)
	{
		m_Motions = physicalComponent->GetMotions();
		Assign(m_NumMotions, physicalComponent->GetNumMotions());
	}
	else
	{
		m_Motions = NULL;
		m_NumMotions = 0;
	}

	// check if we can skip finalize
	m_CreatureFinalize = false;
	if(m_Finalize)
	{
		for(unsigned int i=0; i < creature->GetNumComponents(); i++)
		{
			if(creature->GetComponentByIndex(i)->GetType() >= crCreatureComponent::kCreatureComponentTypeMover)
			{
				m_CreatureFinalize = true;
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////

void crmtComposerData::Start(float deltaTime)
{
	m_DeltaTime = deltaTime;

	// refresh pointers to resource because of defrag
	m_Fragment.Refresh();
	const crSkeletonData& skelData = m_Skeleton->GetSkeletonData();
	m_Defaults = skelData.GetDefaultTransforms();
	m_Bones = skelData.GetBoneData(0);
	m_ParentIndices = skelData.GetParentIndices();
	m_ChildParentIndices = skelData.GetChildParentIndices();
	Assign(m_NumBones, skelData.GetNumBones());
	Assign(m_NumChildParents, skelData.GetNumChildParents());
}

///////////////////////////////////////////////////////////////////////

void crmtComposerData::Finish()
{
	if(Unlikely(!m_WorkBuffer))
	{
		Assertf(false, "motion tree %p failed to compose", m_MotionTree);

#if __ASSERT
		static bool dump = true;
		if(dump)
		{
			Displayf("generating dump of failed motion tree, please include following in any bugs (one time only, dump will not repeat with any subsequent failures)");
			m_MotionTree->Dump(3);
			dump = false;
		}
#endif // __ASSERT
		return;
	}

	if(Unlikely(m_CreatureFinalize))
	{
		const crFrameData& frameData = m_MotionTree->GetFrameData();
		crFrame frame(frameData, m_LastFrame, frameData.GetFrameBufferSize(), false, false);
		crCreature* creature = m_MotionTree->GetCreature();
		creature->Reset(true);
		creature->Pose(frame, NULL, true);
		creature->Finalize(m_DeltaTime);
		sm_Heap.Free(m_WorkBuffer);
	}

	m_WorkBuffer = NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtComposerData::Callback(const sysDependency& dep)
{
	u32 callback = dep.m_Params[0].m_AsInt;
	switch(callback)
	{
	case kComposerCallbackMoverDelta:		return ImplementMoverDelta(dep);
	case kComposerCallbackNodePm:			return ImplementNodePm(dep);
	case kComposerCallbackNodeMirror:		return ImplementNodeMirror(dep);
	case kComposerCallbackNodeN:			return ImplementNodeN(dep);
#if CR_TEST_EXPRESSIONS
	case kComposerCallbackNodeExpressions:	return ImplementNodeExpressions(dep);
#endif
	default: FastAssert(false);
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtComposerData::ImplementMoverDelta(const sysDependency& dep)
{
	u8* destBuffer = static_cast<u8*>(dep.m_Params[1].m_AsPtr);
	const crmtMotionTree* motionTree = static_cast<const crmtMotionTree*>(dep.m_Params[2].m_AsConstPtr);
	const crAnimation* anim = static_cast<const crAnimation*>(dep.m_Params[3].m_AsConstPtr);
	float time = dep.m_Params[4].m_AsFloat;
	float delta = dep.m_Params[5].m_AsFloat;
	float rangeStart = dep.m_Params[6].m_AsFloat;
	float rangeEnd = dep.m_Params[7].m_AsFloat;
	const crFrameData& frameData = motionTree->GetFrameData();

	crFrame frame(frameData, destBuffer, frameData.GetFrameBufferSize(), false, false);
	frame.CalcMoverDelta(*anim, Clamp(time, rangeStart, rangeEnd), delta, rangeStart, rangeEnd);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtComposerData::ImplementNodePm(const sysDependency& dep)
{
	u8* destBuffer = static_cast<u8*>(dep.m_Params[1].m_AsPtr);
	const crmtMotionTree* motionTree = static_cast<const crmtMotionTree*>(dep.m_Params[2].m_AsConstPtr);
	crmtNodePm* node = static_cast<crmtNodePm*>(dep.m_Params[3].m_AsPtr);
	const crFrameData& frameData = motionTree->GetFrameData();

	u32 destBufferSize = frameData.GetFrameBufferSize();
	sysMemSet(destBuffer, 0xff, destBufferSize);

	crFrame frame(frameData, destBuffer, destBufferSize, false, false);
	node->GetParameterizedMotion()->Composite(frame);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtComposerData::ImplementNodeMirror(const sysDependency& dep)
{
	u8* destBuffer = static_cast<u8*>(dep.m_Params[1].m_AsPtr);
	const crmtMotionTree* motionTree = static_cast<const crmtMotionTree*>(dep.m_Params[2].m_AsConstPtr);
	const crmtNodeMirror* node = static_cast<const crmtNodeMirror*>(dep.m_Params[3].m_AsConstPtr);
	const crFrameData& frameData = motionTree->GetFrameData();
	const crSkeletonData& skelData = motionTree->GetSkeleton()->GetSkeletonData();

	crFrame frame(frameData, destBuffer, frameData.GetFrameBufferSize(), false, false);
	frame.Mirror(skelData, node->GetFilter());

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtComposerData::ImplementNodeN(const sysDependency& dep)
{
	u8* destBuffer = static_cast<u8*>(dep.m_Params[1].m_AsPtr);
	const crmtMotionTree* motionTree = static_cast<const crmtMotionTree*>(dep.m_Params[2].m_AsConstPtr);
	const crmtNodeN* node = static_cast<const crmtNodeN*>(dep.m_Params[3].m_AsConstPtr);
	u32 numFrames = dep.m_Params[4].m_AsInt;
	const crFrameData& frameData = motionTree->GetFrameData();

	// build frames
	u32 frameSize = frameData.GetFrameBufferSize();
	crFrame* framesPtr = Alloca(crFrame, numFrames);
	crFrame** frames = Alloca(crFrame*, numFrames);
	const u8* frameStack = reinterpret_cast<const u8*>(&dep.m_Params[5].m_AsInt);
	for(u32 i=0; i < numFrames; i++)
	{
		u8* buffer = destBuffer + frameSize*frameStack[i];
		frames[i] = rage_placement_new(framesPtr+i) crFrame(frameData, buffer, frameSize, false, false);
	}

	// compute weights and filters
	crFrameFilter* filter = node->GetFilter();
	u32 numChildren = node->GetNumChildren();
	f32 childWeights[crmtNodeN::sm_MaxChildren];
	crFrameFilter* childFilters[crmtNodeN::sm_MaxChildren];
	const crmtNodeN::NData* pData = node->GetData();
	const crmtNode* child = node->GetFirstChild();
	u32 n=0;
	for(u32 i=0; i < numChildren; i++)
	{
		if(!child->IsDisabled() || !child->IsSilent())
		{
			childWeights[n] = pData->m_Weight;
			childFilters[n] = pData->m_InputFilter;
			++n;
		}
		pData++;
		child = child->GetNextSibling();
	}

	f32* weights = childWeights;
	crFrameFilter** filters = childFilters;
	crFrame* destFrame = frames[numFrames-1];
	if(node->IsZeroDestination())
	{
		n--;
		weights++;
		filters++;
	}

	// do blending
	switch(node->GetNodeType())
	{
	case crmtNode::kNodeBlendN:		destFrame->BlendN(n, weights, frames, true, true, filter, filters); break;
	case crmtNode::kNodeMergeN:		destFrame->MergeN(n, weights, frames, filter, filters); break;
	case crmtNode::kNodeAddN:		destFrame->AddN(n, weights, frames, filter, filters); break;
	default: Assert(false);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crmtComposerData::Fragment::Set(fragInst* frag)
{
	m_Ptr = frag;
}

////////////////////////////////////////////////////////////////////////////////

void crmtComposerData::Fragment::Refresh()
{
	if(m_Ptr)
	{
		fragCacheEntry* entry = m_Ptr->GetCacheEntry();
		phBoundComposite* boundComposite = entry->GetBound();
		m_BoundComposite = boundComposite;
		m_LocalBoxMinMaxs = boundComposite->GetLocalBoxMinMaxsArray();
		m_BoundIndices = entry->GetComponentToBoneIndexMap();
		m_Children = (void**)m_Ptr->GetTypePhysics()->GetAllChildren();
		m_LastMatrices = boundComposite->GetLastMatrices();
		m_CurrentMatrices = boundComposite->GetCurrentMatrices();
		m_NumBounds = boundComposite->GetNumBounds();
		m_CopyLastMatrices = m_Ptr->IsBoundsPrerenderCopyLastMatricesEnabled();
	}
}

////////////////////////////////////////////////////////////////////////////////

#if CR_TEST_EXPRESSIONS

void CompareFrames(const crFrameData& frameData, const u8* buffer0, const u8* buffer1)
{
	static float tolerance = 0.0001f;
	ScalarV epsilon = ScalarVFromF32(tolerance);
	for(u32 j=0; j < frameData.GetNumDofs(); j++)
	{
		const crFrameData::Dof& dof = frameData.GetDof(j);
		if(dof.m_Id == 2123 || dof.m_Id == 34093) continue; // HACK to skip the lookat bones
		switch(dof.m_Type)
		{
		case kFormatTypeVector3:
			{
				Vec3V v0 = *reinterpret_cast<const Vec3V*>(buffer0 + dof.m_Offset);
				Vec3V v1 = *reinterpret_cast<const Vec3V*>(buffer1 + dof.m_Offset);
				if(!IsCloseAll(v0, v1, epsilon) && !IsEqualIntAll(v0, v1))
				{
					__debugbreak();
				}
				break;
			}

		case kFormatTypeQuaternion:
			{
				QuatV v0 = *reinterpret_cast<const QuatV*>(buffer0 + dof.m_Offset);
				QuatV v1 = *reinterpret_cast<const QuatV*>(buffer1 + dof.m_Offset);
				QuatV v2 = PrepareSlerp(v0, v1);
				if(!IsCloseAll(v0, v2, epsilon) && !IsEqualIntAll(v0, v1))
				{
					__debugbreak();
				}
				break;
			}
		case kFormatTypeFloat:
			{
				f32 v0 = *reinterpret_cast<const f32*>(buffer0 + dof.m_Offset);
				f32 v1 = *reinterpret_cast<const f32*>(buffer1 + dof.m_Offset);
				if(fabs(v0-v1) > tolerance && (u32&)v0!=(u32&)v1)
				{
					__debugbreak();
				}
				break;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crmtComposerData::ImplementNodeExpressions(const sysDependency& dep)
{
	u8* destBuffer = static_cast<u8*>(dep.m_Params[1].m_AsPtr);
	crmtMotionTree* motionTree = static_cast<crmtMotionTree*>(dep.m_Params[2].m_AsPtr);
	const crExpressions* expressions = static_cast<const crExpressions*>(dep.m_Params[3].m_AsConstPtr);
	float time = dep.m_Params[4].m_AsFloat;
	float deltaTime = dep.m_Params[9].m_AsFloat;
	const u16* frameIndices = static_cast<const u16*>(dep.m_Params[5].m_AsConstPtr);
	const u16* skelIndices = static_cast<const u16*>(dep.m_Params[6].m_AsConstPtr);
	Vector_4V* variables = static_cast<Vector_4V*>(dep.m_Params[7].m_AsPtr);
	u32 numVariable = dep.m_Params[8].m_AsInt;
	u32 numIndices = dep.m_DataSizes[5] / sizeof(u16);

	crFrameFilter* filter = NULL;
	const crmtComposerData& composerData = motionTree->GetComposerData();
	const crFrameData& frameData = motionTree->GetFrameData();
	const crSkeletonData& skelData = motionTree->GetSkeleton()->GetSkeletonData();
	crCreature* creature = motionTree->GetCreature();
	crFrame destFrame(frameData, destBuffer, frameData.GetFrameBufferSize(), false, false);
	u8* streamFrameBuffer = AllocaAligned(u8, destFrame.GetBufferSize(), 16);
	sysMemCpy(streamFrameBuffer, destFrame.GetBuffer(), destFrame.GetBufferSize());

	// create stream helper
	ExprStreamHelper ph;
	ph.m_Bones = static_cast<const crBoneData*>(composerData.m_Bones);
	ph.m_Dofs = frameData.GetBuffer();
	ph.m_Frame = streamFrameBuffer;
	ph.m_FrameIndices = frameIndices;
	ph.m_SkelIndices = skelIndices;
	ph.m_Time = time;
	ph.m_DeltaTime = deltaTime;
	ph.m_Motions = static_cast<const crCreatureComponentPhysical::Motion*>(composerData.m_Motions);
	ph.m_FrameSize = frameData.GetFrameBufferSize();
	ph.m_NumIndices = numIndices;
	ph.m_NumBones = composerData.m_NumBones;
	ph.m_NumDofs = destFrame.GetNumDofs();
	ph.m_NumMotions = composerData.m_NumMotions;
	ph.m_Variables = variables;
	ph.m_NumVariables = numVariable;

	// process both expressions and compare result frames
	crExpressionStream*const* exprStreams = expressions->GetExpressionStreams();
	crExpression*const* exprs = expressions->GetExpressions();
	u32 numExpressions = expressions->GetNumExpressions();
	for(u32 i=0; i<numExpressions; i++)
	{
		exprs[i]->Process(destFrame, time, deltaTime, creature, &skelData, filter);
		exprStreams[i]->Process(ph);

		CompareFrames(frameData, destFrame.GetBuffer(), streamFrameBuffer);
	}

	return true;
}

#endif // CR_TEST_EXPRESSIONS

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
