//
// crmotiontree/dependency_identity.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_IDENTITY_FRAG
#define MOTIONTREE_DEPENDENCY_IDENTITY_FRAG

#include "crmotiontree/crmtdiag.h"
#include "crskeleton/bonedata.h"
#include "system/dependency.h"
#include "system/memops.h"
#include "vectormath/classes.h"

using namespace rage;
using namespace Vec;

SPUFRAG_DECL(bool, dependency_identity, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_identity, const sysDependency& dep)
{
	crmtDebug3("dependency_identity");

	u8* RESTRICT destFrame = static_cast<u8*>(dep.m_Params[0].m_AsPtr);
	const ua16* RESTRICT indices = static_cast<const ua16*>(dep.m_Params[1].m_AsConstPtr);
	const crBoneData* RESTRICT bones = static_cast<const crBoneData*>(dep.m_Params[2].m_AsConstPtr);
    unsigned int destFrameSize = dep.m_DataSizes[0];

	// invalidate the destination frame
	sysMemSet(destFrame, 0xff, destFrameSize);

	unsigned int countTranslations = RAGE_COUNT(indices[0], 2);
	unsigned int countRotations = RAGE_COUNT(indices[1], 2);
	unsigned int countScales = RAGE_COUNT(indices[2], 2);
	indices += 8;

	// apply bone default translations to dofs
	for(; countTranslations; countTranslations--)
	{
		Vec3V* pDest0 = reinterpret_cast<Vec3V*>(destFrame + indices[0]);
		Vec3V* pDest1 = reinterpret_cast<Vec3V*>(destFrame + indices[2]);
		Vec3V* pDest2 = reinterpret_cast<Vec3V*>(destFrame + indices[4]);
		Vec3V* pDest3 = reinterpret_cast<Vec3V*>(destFrame + indices[6]);

		const crBoneData* pSrc0 = bones + indices[1];
		const crBoneData* pSrc1 = bones + indices[3];
		const crBoneData* pSrc2 = bones + indices[5];
		const crBoneData* pSrc3 = bones + indices[7];

		Vec3V src0 = pSrc0->GetDefaultTranslation();
		Vec3V src1 = pSrc1->GetDefaultTranslation();
		Vec3V src2 = pSrc2->GetDefaultTranslation();
		Vec3V src3 = pSrc3->GetDefaultTranslation();

		*pDest0 = src0;
		*pDest1 = src1;
		*pDest2 = src2;
		*pDest3 = src3;

		indices += 8;
	}

	// apply bone default rotations to dofs
	for(; countRotations; countRotations--)
	{
		QuatV* pDest0 = reinterpret_cast<QuatV*>(destFrame + indices[0]);
		QuatV* pDest1 = reinterpret_cast<QuatV*>(destFrame + indices[2]);
		QuatV* pDest2 = reinterpret_cast<QuatV*>(destFrame + indices[4]);
		QuatV* pDest3 = reinterpret_cast<QuatV*>(destFrame + indices[6]);

		const crBoneData* pSrc0 = bones + indices[1];
		const crBoneData* pSrc1 = bones + indices[3];
		const crBoneData* pSrc2 = bones + indices[5];
		const crBoneData* pSrc3 = bones + indices[7];

		QuatV src0 = pSrc0->GetDefaultRotation();
		QuatV src1 = pSrc1->GetDefaultRotation();
		QuatV src2 = pSrc2->GetDefaultRotation();
		QuatV src3 = pSrc3->GetDefaultRotation();

		*pDest0 = src0;
		*pDest1 = src1;
		*pDest2 = src2;
		*pDest3 = src3;

		indices += 8;
	}

	// apply bone default scales to dofs
	for(; countScales; countScales--)
	{
		Vec3V* pDest0 = reinterpret_cast<Vec3V*>(destFrame + indices[0]);
		Vec3V* pDest1 = reinterpret_cast<Vec3V*>(destFrame + indices[2]);
		Vec3V* pDest2 = reinterpret_cast<Vec3V*>(destFrame + indices[4]);
		Vec3V* pDest3 = reinterpret_cast<Vec3V*>(destFrame + indices[6]);

		const crBoneData* pSrc0 = bones + indices[1];
		const crBoneData* pSrc1 = bones + indices[3];
		const crBoneData* pSrc2 = bones + indices[5];
		const crBoneData* pSrc3 = bones + indices[7];

		Vec3V src0 = pSrc0->GetDefaultScale();
		Vec3V src1 = pSrc1->GetDefaultScale();
		Vec3V src2 = pSrc2->GetDefaultScale();
		Vec3V src3 = pSrc3->GetDefaultScale();

		*pDest0 = src0;
		*pDest1 = src1;
		*pDest2 = src2;
		*pDest3 = src3;

		indices += 8;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_IDENTITY_FRAG
