//
// crmotiontree/dependency_filter.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_FILTER_FRAG
#define MOTIONTREE_DEPENDENCY_FILTER_FRAG

#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"
#include "system/dma.h"
#include "vectormath/vectormath.h"

using namespace rage;
using namespace Vec;

// dependency_filter pseudo-code
// 
// if(filter != 0)
//		dest = dest
// else
//		dest = invalid

SPUFRAG_DECL(bool, dependency_filter, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_filter, const sysDependency& dep)
{
	crmtDebug3("dependency_filter");

	Vector_4V* RESTRICT pDest = static_cast<Vector_4V*>(dep.m_Params[0].m_AsPtr);
	const Vector_4V* RESTRICT pFilter = static_cast<const Vector_4V*>(dep.m_Params[1].m_AsConstPtr);
	unsigned int countQuads = RAGE_COUNT(dep.m_Params[2].m_AsInt, 2) + RAGE_COUNT(dep.m_Params[3].m_AsInt, 2);
	unsigned int countFloats = RAGE_COUNT(dep.m_Params[4].m_AsInt, 2);

	Vector_4V _zero = V4VConstant(V_ZERO);

	// filter quaternions and vectors
	for(; countQuads; countQuads--)
	{
		Vector_4V dest0 = pDest[0];
		Vector_4V dest1 = pDest[1];
		Vector_4V dest2 = pDest[2];
		Vector_4V dest3 = pDest[3];

		// if filter is zero, set destination to invalid
		Vector_4V filter = *pFilter;
		Vector_4V filterZero = V4IsEqualV(filter, _zero);

		pDest[0] = V4Or(V4SplatX(filterZero), dest0);
		pDest[1] = V4Or(V4SplatY(filterZero), dest1);
		pDest[2] = V4Or(V4SplatZ(filterZero), dest2);
		pDest[3] = V4Or(V4SplatW(filterZero), dest3);

		pFilter += 1;
		pDest += 4;
	}

	// filter floats
	for(; countFloats; countFloats--)
	{
		Vector_4V dest = *pDest;
		Vector_4V filter = *pFilter;
		Vector_4V filterZero = V4IsEqualV(filter, _zero);
		*pDest = V4Or(filterZero, dest);
		pFilter += 1;
		pDest += 1;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_FILTER_FRAG
