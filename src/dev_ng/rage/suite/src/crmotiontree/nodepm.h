//
// crmotiontree/nodepm.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEPM_H
#define CRMOTIONTREE_NODEPM_H

#include "node.h"

#include "crparameterizedmotion/parameterizedmotionplayer.h"

namespace rage
{

class crTag;
class crpmParameterizedMotion;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Parameterized motion playback node
class crmtNodePm : public crmtNode
{
public:

	// PURPOSE: Default constructor
	crmtNodePm();

	// PURPOSE: Resource constructor
	crmtNodePm(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodePm();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodePm);

	// PURPOSE: Initialization
	// PARAMS: pm - parameterized motion to play (can be NULL).
	void Init(crpmParameterizedMotion* pm);

	// PURPOSE: Shutdown, free/release all dynamic memory.
	virtual void Shutdown();

	// PURPOSE: Update
	virtual void Update(float dt);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: Get the parameterized motion player
	// RETURNS: Reference to parameterized motion player (const)
	const crpmParameterizedMotionPlayer& GetParameterizedMotionPlayer() const;

	// PURPOSE: Get the parameterized motion player
	// RETURNS: Reference to parameterized motion player (non-const)
	crpmParameterizedMotionPlayer& GetParameterizedMotionPlayer();


	// PURPOSE: Set parameterized motion
	// PARAM: pm - pointer to parameterized motion instance (may be NULL)
	void SetParameterizedMotion(crpmParameterizedMotion* pm);

	// PURPOSE: Returns parameterized motion
	// RETURNS: Parameterized motion instance (may be NULL)
	const crpmParameterizedMotion* GetParameterizedMotion() const;

	// PURPOSE: Returns parameterized motion (non-const)
	// RETURNS: Parameterized motion instance (may be NULL)
	crpmParameterizedMotion* GetParameterizedMotion();


	// PURPOSE: Enumeration of messages
	enum
	{
		kMsgPmLooped = CRMT_PACK_MSG_ID(crmtNode::kMsgEnd, kNodePm),
		kMsgPmEnded,
		kMsgPmTagEnter,
		kMsgPmTagExit,
		kMsgPmTagUpdate,

		kMsgEnd,
	};

	// PURPOSE: Tag Enter/Exit message payload
	struct TagMsgPayload
	{
		TagMsgPayload(const crpmParameterizedMotion* pm, const crTag* tag);

		const crpmParameterizedMotion* m_Pm;
		const crTag* m_Tag;
	};

	// PURPOSE: Internal use only, functor callback for clip tag triggers
	void TagTriggerCallback(const crTag&, bool, bool);

private:
	crpmParameterizedMotionPlayer m_Player;
};

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterizedMotionPlayer& crmtNodePm::GetParameterizedMotionPlayer() const
{
	return m_Player;
}

////////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotionPlayer& crmtNodePm::GetParameterizedMotionPlayer()
{
	return m_Player;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODEPM_H
