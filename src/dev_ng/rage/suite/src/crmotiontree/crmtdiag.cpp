//
// crmotiontree/crmtdiag.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "crmtdiag.h"
#include "system/param.h"


RAGE_DEFINE_CHANNEL(crmt, rage::DIAG_SEVERITY_DEBUG1, rage::DIAG_SEVERITY_DEBUG1)