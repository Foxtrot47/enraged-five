//
// crmotiontree/requestcustom.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTCUSTOM_H
#define CRMOTIONTREE_REQUESTCUSTOM_H

#include "request.h"

#include "atl/functor.h"

namespace rage
{

// PURPOSE: Custom request
// Use this custom request to construct and initialize a custom node
// derived from crmtNodeCustomNoInput (i.e. one that is output only, no input children)
template<class T>
class crmtRequestCustomNoInput : public crmtRequestSource<1>
{
public:

	// PURPOSE: Node initialization functor definition
	// NOTES: Use this to initialize the node created by the custom request
	// Functions must take the form; void Fn(T&)
	// Where T is the custom node type (derived off of crmtNode
	typedef Functor1<T&> InitFunctor;


	// PURPOSE: Default constructor
	crmtRequestCustomNoInput();

	// PURPOSE: Initializing constructor
	// PARAMS: initFunctor - optional functor used to initialize the newly constructed node
	crmtRequestCustomNoInput(const InitFunctor& initFunctor);

	// PURPOSE: Destructor
	virtual ~crmtRequestCustomNoInput();

	// PURPOSE: Initializer
	// PARAMS: initFunctor - optional functor used to initialize the newly constructed node
	void Init(const InitFunctor& initFunctor=InitFunctor::NullFunctor());

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	InitFunctor m_InitFunctor;
};



// PURPOSE: Custom request
// Use this custom request to construct and initialize a custom node
// derived from crmtNodeCustomSingleInput (i.e. one that is input/output, with exactly one input)
template<class T>
class crmtRequestCustomSingleInput : public crmtRequestSource<1>
{
public:

	// PURPOSE: Node initialization functor definition
	// NOTES: Use this to initialize the node created by the custom request
	// Functions must take the form; void Fn(T&)
	// Where T is the custom node type (derived off of crmtNodeCustomSingleInput)
	typedef Functor1<T&> InitFunctor;


	// PURPOSE: Default constructor
	crmtRequestCustomSingleInput();

	// PURPOSE: Initializing constructor - for custom node with new child node
	// PARAMS: initFunctor - optional functor used to initialize the newly constructed node
	crmtRequestCustomSingleInput(crmtRequest& source, const InitFunctor& initFunctor=InitFunctor::NullFunctor());

	// PURPOSE: Initializing constructor - for applying to existing node
	// PARAMS: initFunctor - optional functor used to initialize the newly constructed node
	crmtRequestCustomSingleInput(const InitFunctor& initFunctor);

	// PURPOSE: Destructor
	virtual ~crmtRequestCustomSingleInput();

	// PURPOSE: Initializing - for custom node with new child node
	// PARAMS: initFunctor - optional functor used to initialize the newly constructed node
	void Init(crmtRequest& source, const InitFunctor& initFunctor=InitFunctor::NullFunctor());

	// PURPOSE: Initializing - for applying to existing node
	// PARAMS: initFunctor - optional functor used to initialize the newly constructed node
	void Init(const InitFunctor& initFunctor=InitFunctor::NullFunctor());

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	InitFunctor m_InitFunctor;
};


// TODO --- IMPLEMENT THE N-WAY INPUT CUSTOM REQUEST (AS SOON AS THE N-WAY CUSTOM NODE IS ADDED)


// inline functions

////////////////////////////////////////////////////////////////////////////////

template<class T>
crmtRequestCustomNoInput<T>::crmtRequestCustomNoInput()
: m_InitFunctor(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
crmtRequestCustomNoInput<T>::crmtRequestCustomNoInput(const InitFunctor& initFunctor)
{
	Init(initFunctor);
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
crmtRequestCustomNoInput<T>::~crmtRequestCustomNoInput()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
void crmtRequestCustomNoInput<T>::Init(const InitFunctor& initFunctor)
{
	m_InitFunctor = initFunctor;
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
void crmtRequestCustomNoInput<T>::Shutdown()
{
	crmtRequest::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
crmtNode* crmtRequestCustomNoInput<T>::GenerateNode(crmtMotionTree& tree, crmtNode* UNUSED_PARAM(node))
{
	T* newNode = T::CreateNode(tree);
	if(m_InitFunctor)
	{
		m_InitFunctor(*newNode);
	}

	return newNode;
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
crmtRequestCustomSingleInput<T>::crmtRequestCustomSingleInput()
{
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
crmtRequestCustomSingleInput<T>::crmtRequestCustomSingleInput(crmtRequest& source, const InitFunctor& initFunctor)
{
	Init(source, initFunctor);
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
crmtRequestCustomSingleInput<T>::crmtRequestCustomSingleInput(const InitFunctor& initFunctor)
{
	Init(initFunctor);
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
crmtRequestCustomSingleInput<T>::~crmtRequestCustomSingleInput()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
void crmtRequestCustomSingleInput<T>::Init(crmtRequest& source, const InitFunctor& initFunctor)
{
	SetSource(&source, 0);
	m_InitFunctor = initFunctor;
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
void crmtRequestCustomSingleInput<T>::Init(const InitFunctor& initFunctor)
{
	m_InitFunctor = initFunctor;
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
void crmtRequestCustomSingleInput<T>::Shutdown()
{
	crmtRequestSource<1>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

template<class T>
crmtNode* crmtRequestCustomSingleInput<T>::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	T* newNode = T::CreateNode(tree);
	if(m_InitFunctor)
	{
		m_InitFunctor(*newNode);
	}

	GenerateSourceNodes(tree, node, newNode);

	return newNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_REQUESTCUSTOM_H 	
