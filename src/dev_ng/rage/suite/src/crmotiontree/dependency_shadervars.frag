//
// crmotiontree/dependency_shadervars.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_SHADERVARS_FRAG
#define MOTIONTREE_DEPENDENCY_SHADERVARS_FRAG

#include "creature/componentshadervars.h"
#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"

using namespace rage;

SPUFRAG_DECL(bool, dependency_shadervars, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_shadervars, const sysDependency& dep)
{
	crmtDebug3("dependency_shadervars");

	const u8* RESTRICT srcFrame = static_cast<const u8*>(dep.m_Params[0].m_AsConstPtr);
	const crFrameData::Dof* RESTRICT dofs = static_cast<const crFrameData::Dof*>(dep.m_Params[1].m_AsConstPtr);
	crCreatureComponentShaderVars::ShaderVarDofPair* RESTRICT shaderVars = static_cast<crCreatureComponentShaderVars::ShaderVarDofPair*>(dep.m_Params[2].m_AsPtr);
	unsigned int numDofs = dep.m_Params[3].m_AsInt;
	unsigned int numPairs = dep.m_Params[4].m_AsInt;

	unsigned int pairIdx = 0;

	for(unsigned int i=0; i < numDofs; i++)
	{
		const crFrameData::Dof& dof = dofs[i];
		if(Likely(dof.m_Track != kTrackAnimatedNormalMaps))
			continue;

		const u8* pValue = srcFrame + dof.m_Offset;
		if(*reinterpret_cast<const u32*>(pValue) == UINT_MAX)
			continue;

		u32 id = dof.m_Id;
		for(; pairIdx < numPairs; pairIdx++)
		{
			crCreatureComponentShaderVars::ShaderVarDofPair& pair = shaderVars[pairIdx];
			if(pair.m_Id == id)
			{
				Assert(dof.m_Type == kFormatTypeFloat);
				pair.m_Value = *reinterpret_cast<const f32*>(pValue);
				if(!pair.m_Duplicate)
				{
					pairIdx++;
					break;
				}
			}
			else if(pair.m_Id > id)
			{
				break;
			}
			else
			{
				pair.m_Value = 0.f;
			}
		}
	}

	for(; pairIdx < numPairs; pairIdx++)
	{
		shaderVars[pairIdx].m_Value = 0.f;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_SHADERVARS_FRAG