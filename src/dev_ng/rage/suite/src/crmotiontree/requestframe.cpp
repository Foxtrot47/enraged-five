//
// crmotiontree/requestframe.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "requestframe.h"

#include "nodeframe.h"

#include "cranimation/frame.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestFrame::crmtRequestFrame()
: m_Frame(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestFrame::crmtRequestFrame(const crFrame* frame)
: m_Frame(NULL)
{
	Init(frame);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestFrame::~crmtRequestFrame()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestFrame::Init(const crFrame* frame)
{
	SetFrame(frame);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestFrame::Shutdown()
{
	SetFrame(NULL);

	crmtRequest::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestFrame::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeFrame* frameNode = crmtNodeFrame::CreateNode(tree);
	Assert(frameNode);

	frameNode->Init(m_Frame);

	if(node)
	{
		node->Replace(*frameNode);
	}

	return frameNode;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestFrame::SetFrame(const crFrame* frame)
{
	if(frame)
	{
		frame->AddRef();
	}
	if(m_Frame)
	{
		m_Frame->Release();
	}
	m_Frame = frame;
}

////////////////////////////////////////////////////////////////////////////////

const crFrame* crmtRequestFrame::GetFrame() const
{
	return m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
