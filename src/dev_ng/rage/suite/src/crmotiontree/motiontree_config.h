//
// crmotiontree/motiontree_config.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_MOTIONTREE_CONFIG_H
#define CRMOTIONTREE_MOTIONTREE_CONFIG_H


// configure various compile time sized objects:

// maximum depth chunk size in iterator recursion
#define CRMT_ITERATE_BATCH (64)

// heap size used by all the frames and dependencies currently executed
#define CRMT_COMPOSER_HEAPSIZE (512*1024)

// adds a map of nodes to node defs
#define CRMT_NODE_2_DEF_MAP (__DEV)

#endif // CRMOTIONTREE_MOTIONTREE_CONFIG_H
