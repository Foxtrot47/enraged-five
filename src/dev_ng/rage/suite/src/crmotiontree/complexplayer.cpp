//
// crmotiontree/complexplayer.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "complexplayer.h"

#include "managersinglestate.h"
#include "motiontree.h"
#include "nodeanimation.h"
#include "requestanimation.h"

#include "cranimation/animplayer.h"
#include "creature/creature.h"
#include "crskeleton/skeleton.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtComplexPlayer::crmtComplexPlayer()
: m_Skeleton(NULL)
, m_MoverMtx(NULL)
, m_MotionTree(NULL)
, m_Manager(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtComplexPlayer::crmtComplexPlayer(crFrameDataFactory& factory, crFrameAccelerator& accelerator, crSkeleton& skeleton, Mat34V* moverMtx, crmtMotionTree* tree)
: m_Creature(NULL)
, m_Skeleton(NULL)
, m_MoverMtx(NULL)
, m_MotionTree(NULL)
, m_Manager(NULL)
{
	Init(factory, accelerator, skeleton, moverMtx, tree);
}

////////////////////////////////////////////////////////////////////////////////

crmtComplexPlayer::~crmtComplexPlayer()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtComplexPlayer::Init(crFrameDataFactory& factory, crFrameAccelerator& accelerator, crSkeleton& skeleton, Mat34V* moverMtx, crmtMotionTree* tree)
{
	Assert(!moverMtx || (skeleton.GetParentMtx()) == moverMtx);

	m_Creature = rage_new crCreature();
	m_Creature->Init(factory, accelerator, &skeleton, moverMtx);
	m_Skeleton = &skeleton;
	m_MoverMtx = moverMtx;

	m_MotionTree = tree;
	if(!m_MotionTree)
	{
		m_MotionTree = rage_new crmtMotionTree;
		m_MotionTree->Init(*m_Creature);
	}
	m_Manager = rage_new crmtManagerSingleState(*m_MotionTree);
}

////////////////////////////////////////////////////////////////////////////////

void crmtComplexPlayer::Shutdown()
{
	delete m_Creature;
	m_Creature = NULL;

	m_Skeleton = NULL;
	m_MotionTree = NULL;

	if(m_Manager)
	{
		delete m_Manager;
		m_Manager = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtComplexPlayer::Update(float deltaTime)
{
	Assert(m_MotionTree);

	m_MotionTree->Update(deltaTime);
}

////////////////////////////////////////////////////////////////////////////////

void crmtComplexPlayer::Reset()
{
	m_Manager->Reset();
	if(m_MoverMtx)
	{
		*m_MoverMtx = Mat34V(V_IDENTITY);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtComplexPlayer::SetPaused(bool UNUSED_PARAM(paused))
{
}

////////////////////////////////////////////////////////////////////////////////

bool crmtComplexPlayer::IsPaused() const
{
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crmtComplexPlayer::SetRate(float rate)
{
	Assert(m_Manager);

	crmtNode* node = m_Manager->GetCurrentState().GetNode();
	if(node)
	{
		switch(node->GetNodeType())
		{
			case crmtNode::kNodeAnimation:
				{
					crmtNodeAnimation* animNode = static_cast<crmtNodeAnimation*>(node);
					animNode->GetAnimPlayer().SetRate(rate);
				}
				break;

			default:
				break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

float crmtComplexPlayer::GetRate() const
{
	Assert(m_Manager);

	crmtNode* node = m_Manager->GetCurrentState().GetNode();
	if(node)
	{
		switch(node->GetNodeType())
		{
			case crmtNode::kNodeAnimation:
				{
					crmtNodeAnimation* animNode = static_cast<crmtNodeAnimation*>(node);
					return animNode->GetAnimPlayer().GetRate();
				}
				// break;

			default:
				break;
		}
	}

	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

void crmtComplexPlayer::SetTime(float time)
{
	Assert(m_Manager);

	crmtNode* node = m_Manager->GetCurrentState().GetNode();
	if(node)
	{
		switch(node->GetNodeType())
		{
			case crmtNode::kNodeAnimation:
				{
					crmtNodeAnimation* animNode = static_cast<crmtNodeAnimation*>(node);
					animNode->GetAnimPlayer().SetTime(time);
				}
				break;

			default:
				break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

float crmtComplexPlayer::GetTime() const
{
	Assert(m_Manager);

	crmtNode* node = m_Manager->GetCurrentState().GetNode();
	if(node)
	{
		switch(node->GetNodeType())
		{
			case crmtNode::kNodeAnimation:
				{
					crmtNodeAnimation* animNode = static_cast<crmtNodeAnimation*>(node);
					return animNode->GetAnimPlayer().GetTime();
				}
				// break;

			default:
				break;
		}
	}

	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

float crmtComplexPlayer::GetDuration() const
{
	Assert(m_Manager);

	crmtNode* node = m_Manager->GetCurrentState().GetNode();
	if(node)
	{
		switch(node->GetNodeType())
		{
			case crmtNode::kNodeAnimation:
				{
					crmtNodeAnimation* animNode = static_cast<crmtNodeAnimation*>(node);
					const crAnimation* anim = animNode->GetAnimPlayer().GetAnimation();
					if(anim)
					{
						return anim->GetDuration();
					}
				}
				break;

			default:
				break;
		}
	}

	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

void crmtComplexPlayer::PlayAnimation(const crAnimation* anim, float crossfade)
{
	Assert(m_Manager);

	crmtRequestAnimation reqAnim(anim);
	m_Manager->RequestNewState(reqAnim, crossfade);
}

////////////////////////////////////////////////////////////////////////////////

void crmtComplexPlayer::SetAnimation(const crAnimation* anim)
{
	Assert(m_Manager);

	crmtNode* node = m_Manager->GetCurrentState().GetNode();
	if(node && node->GetNodeType() == crmtNode::kNodeAnimation)
	{
		crmtNodeAnimation* animNode = static_cast<crmtNodeAnimation*>(node);
		animNode->GetAnimPlayer().SetAnimation(anim);
	}
	else
	{
		PlayAnimation(anim);
	}
}

////////////////////////////////////////////////////////////////////////////////

const crAnimation* crmtComplexPlayer::GetAnimation() const
{
	Assert(m_Manager);

	crmtNode* node = m_Manager->GetCurrentState().GetNode();
	if(node && node->GetNodeType() == crmtNode::kNodeAnimation)
	{
		crmtNodeAnimation* animNode = static_cast<crmtNodeAnimation*>(node);
		return animNode->GetAnimPlayer().GetAnimation();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void crmtComplexPlayer::AddWidgets(bkBank& UNUSED_PARAM(bk))
{
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
