//
// crmotiontree/synchronizer.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_SYNCHRONIZER_H
#define CRMOTIONTREE_SYNCHRONIZER_H

#include "iterator.h"
#include "observer.h"

#include "crmetadata/tag.h"
#include "crmetadata/tagiterators.h"


namespace rage
{

class crTag;
class crTags;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base synchronizer
// Synchronizers enforce various synchronization rules on their target nodes.
class crmtSynchronizer : public crmtObserver
{
public:

	// PURPOSE: Default constructor
	crmtSynchronizer();

	// PURPOSE: Destructor
	virtual ~crmtSynchronizer();

	// PURPOSE: Shutdown, free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Add a target (by observer)
	// PARAMS:
	// observer - observer pointing to node to add
	// weight - target weight (controls influence over synchronization)
	// RETURNS: true - successfully added
	bool AddTarget(const crmtObserver& observer, float weight=1.f, bool delay=false, bool autoUpdateWeight=true, bool immutable=false, bool warp=false);

	// PURPOSE: Add a target (by node)
	// PARAMS:
	// node - reference to node to add
	// weight - target weight (controls influence over synchronization)
	// RETURNS: true - successfully added
	// NOTES: Override to implement target filtering, selectively chaining up to base
	virtual bool AddTargetByNode(crmtNode& node, float weight=1.f, bool delay=false, bool autoUpdateWeight=true, bool immutable=false, bool warp=false);

	// PURPOSE: Remove a target (by observer)
	// PARAMS: observer - observer pointing to node to remove
	// RETURNS: true - successfully removed
	bool RemoveTarget(const crmtObserver&);

	// PURPOSE: Remove a target (by node)
	// PARAMS: observer - observer pointing to node to remove
	// RETURNS: true - successfully removed
	bool RemoveTargetByNode(crmtNode&);

	// PURPOSE: Remove all targets
	void RemoveAllTargets();
	
	// PURPOSE: Reset all target weights
	void ResetAllTargetWeights();

	// PURPOSE: Get current number of targets
	int GetNumTargets() const;

	// PURPOSE: Is in automated target adding mode
	bool IsAutoAddTarget() const;

	// PURPOSE: Set automated target adding mode
	void SetAutoAddTarget(bool autoDiscovery);

	// PURPOSE: Override receive message call
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: Reset message interception override
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

protected:

	// PURPOSE: Synchronization call, implement in derived synchronizers
	virtual void Synchronize(float deltaTime) = 0;

	// PURPOSE: Add target iterator
	class AddTargetIterator : public crmtIterator
	{
	public:

		// PURPOSE: Default constructor
		AddTargetIterator(crmtSynchronizer& synchronizer);

		// PURPOSE: Destructor
		virtual ~AddTargetIterator();

		// PURPOSE: Override visit call
		virtual void Visit(crmtNode&);

	protected:

		crmtSynchronizer* m_Synchronizer;
	};

	friend class AddTargetIterator;

	// PURPOSE: Add targets (override to control how targets are added)
	virtual void AddTargets();


	// PURPOSE: Sync observer, used internally to attach to targets
	class SyncObserver : public crmtObserver
	{
		friend class crmtSynchronizer;

	public:

		// PURPOSE: Initializing constructor
		SyncObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp);

		// PURPOSE: Destructor
		virtual ~SyncObserver();

		// PURPOSE: Shutdown, free/release dynamic resources
		virtual void Shutdown();

		// PURPOSE: Override receive message call
		virtual void ReceiveMessage(const crmtMessage& msg);

		// PURPOSE: Reset message interception override
		virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

#if CR_DEV
		// PURPOSE: Dump.  Implement output of debugging info. 
		virtual void Dump(crmtDumper&) const;
#endif // CR_DEV

		// PURPOSE: Get sync observer weight
		float GetWeight() const;

		// PURPOSE: Set sync observer weight
		void SetWeight(float weight);
	
		// PURPOSE: Is sync observer disabled?
		bool IsDisabled() const;

		// PURPOSE: Disable sync observer
		void SetDisabled(bool disable);

		// PURPOSE: Is delayed?
		bool IsDelayed() const;

		// PURPOSE: Clear delay
		void ClearDelay();

		// PURPOSE: Is first sync
		bool IsFirstSync() const;

		// PURPOSE: Clear first sync
		void ClearFirstSync();

		// PURPOSE: Is first sync warping
		bool DoesFirstSyncWarp() const;

		// PURPOSE: Is immutable
		bool IsImmutable() const;

		// PURPOSE: Set immutability
		void SetImmutable(bool immutable);

		// PURPOSE: Get phase of target
		float GetPhase() const;

		// PURPOSE: Set phase of target
		void SetPhase(float phase);

		// PURPOSE: Get delta of target
		float GetDelta() const;

		// PURPOSE: Set delta of target
		void SetDelta(float delta);

		// PURPOSE: Set phase and delta of target
		// NOTES: Delta is automatically calculated, based on new phase, and existing phase/delta
		void SetPhaseAndAutoDelta(float phase);

		// PURPOSE: Is target looped
		bool IsLooped() const;

		// PURPOSE: Get target tags
		const crTags* GetTags() const;

		// PURPOSE: Get target properties
		const crProperties* GetProperties() const;

	protected:

		// PURPOSE: Remove sync observer from synchronizer
		void Remove();

	public:
		crmtSynchronizer* m_Synchronizer;
		SyncObserver* m_NextSyncObserver;
		float m_Weight;
		bool m_ZeroWeight;
		bool m_Disabled;
		bool m_Delay;
		bool m_AutoUpdateWeight;
		bool m_FirstSync;
		bool m_FirstSyncWarp;
		bool m_Immutable;
	};

	SyncObserver* m_SyncObservers;

	// PURPOSE: Allocate sync observer (allows derived classes to override)
	virtual SyncObserver* CreateSyncObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp);

	// PURPOSE: Used internally to temporarily track targets
	struct Target
	{
		// PURPOSE: Default constructor
		Target();

		// PURPOSE: Initialize target
		bool Init(crmtNode* node);

		// PURPOSE: Adjust target phase and delta
		void Adjust(float phase, float delta) const;

		// PURPOSE: Adjust target (automatically calculating delta)
		void AdjustPhaseAutoDelta(float phase, float deltaSum) const;

		// PURPOSE: Sorting callback
		static int ComparePhaseCb(const void* p0, const void* p1);


		crmtNode* m_Node;
		float m_Weight;
		float m_Phase;
		float m_Delta;
		float m_Rate;
		bool m_Loop;
		bool m_Delay;
		bool m_FirstSync;

		const crTags* m_Tags;
		int m_NumTags;
		const crTag** m_TagPtrs;
		bool m_TagMatched;
	};

	// PURPOSE: Internal function, initializes targets
	int InitTargets(int numTargets, Target* targets, int& numWeighted, Target** weightedTargets, float& weightSum, float& deltaSum, bool& looping);

	// PURPOSE: Internal function, resets the delayed flag on the observers
	void ResetDelayedObservers();

private:

	// PURPOSE: Internal function, adds target without checking for duplication
	SyncObserver* AddTargetUnsafe(crmtNode& node, bool delay, bool autoUpdateWeight, bool immutable, bool warp);

	// PURPOSE: Internal function to find sync observer
	SyncObserver** FindSyncObserver(crmtNode& node);

	// PURPOSE: Internal function, remove target by sync observer reference
	bool RemoveSyncObserver(SyncObserver& so);

private:
	u32 m_InfluenceDepth;
	bool m_AutoAddTarget;

protected:
	bool m_FirstSync;

	friend struct AscendingWeightSort;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Phase synchronizer
// Synchronizes target phases
class crmtSynchronizerPhase : public crmtSynchronizer
{
public:

	// PURPOSE: Default constructor
	crmtSynchronizerPhase();

	// PURPOSE: Destructor
	virtual ~crmtSynchronizerPhase();

	// PURPOSE: Shutdown, free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Add a target (by node)
	// PARAMS:
	// node - reference to node to add
	// weight - target weight (controls influence over synchronization)
	// RETURNS: true - successfully added
	virtual bool AddTargetByNode(crmtNode& node, float weight=1.f, bool delay=false, bool autoUpdateWeight=true, bool immutable=false, bool warp=false);

protected:

	// PURPOSE: Override synchronize call
	virtual void Synchronize(float deltaTime);
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Tag synchronizer
// Synchronizes targets based upon tags
class crmtSynchronizerTag : public crmtSynchronizer
{
public:

	// PURPOSE: Default constructor
	crmtSynchronizerTag();

	// PURPOSE: Destructor
	virtual ~crmtSynchronizerTag();

	// PURPOSE: Shutdown, free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Add a target (by node)
	// PARAMS:
	// node - reference to node to add
	// weight - target weight (controls influence over synchronization)
	// RETURNS: true - successfully added
	virtual bool AddTargetByNode(crmtNode& node, float weight=1.f, bool delay=false, bool autoUpdateWeight=true, bool immutable=false, bool warp=false);

	// PURPOSE: Set force looping behavior
	void SetForceLooping(bool forceLooping);

	// PURPOSE: Add new property
	void AddProperty(const crProperty* prop);

protected:

	// PURPOSE: Override synchronize call
	virtual void Synchronize(float deltaTime);

	// PURPOSE: Specialized tag observer
	class SyncTagObserver : public SyncObserver
	{
	public:

		// PURPOSE: Initializing constructor
		SyncTagObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp);

		// PURPOSE: Destructor
		virtual ~SyncTagObserver();

		// PURPOSE: Shutdown, free/release dynamic resources
		virtual void Shutdown();

		// PURPOSE: Override receive message call
		virtual void ReceiveMessage(const crmtMessage& msg);

		// PURPOSE: Reset message interception override
		virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

			
		// PURPOSE: Map a phase value to a common reference value for this target
		float MapPhaseToReference(float phase) const;

		// PURPOSE: Map a common reference value back to a phase value for this target
		float MapReferenceToPhase(float reference) const;

		// PURPOSE: Get the minimum reference value for this target
		float GetMinReference() const;

		// PURPOSE: Get the maximum reference value for this target
		float GetMaxReference() const;

		// PURPOSE: Calculate the signature for this target
		u32 CalcSignature() const;


		// PURPOSE: Alignment keys, store mapping between target phase and common reference
		struct AlignmentKey
		{
			float m_Phase;
			float m_Reference;
		};
		atArray<AlignmentKey> m_Keys;

		float m_MinReference;
		float m_MaxReference;
	};

	// PURPOSE: Override sync observer creation (create SyncTagObservers instead)
	virtual SyncObserver* CreateSyncObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp);

private:

	static const int sm_MaxNumTags = 32;
	struct Target
	{
		SyncObserver* m_Observer;
		const crTag* m_Tags[sm_MaxNumTags];
		int m_NumTags;
		bool m_Looping;
	};

	struct Pattern
	{
		const crTag*const* m_Tags;
		int m_NumTags;
		bool m_Looping;
	};

	// PURPOSE: Filter tags from current properties
	int FilterTags(const crTag** outTags, const crTags& tags, float startPhase=-1.f) const;

	// PURPOSE: Insert tag pattern into list, if unique
	bool InsertUniquePattern(const Target& target, int numTags, bool looping, Pattern* patterns, int numPatterns) const;

	// PURPOSE: Calculate a unique set of tag patterns from all available targets (and order by complexity)
	int CalcAlignmentPatterns(Pattern* patterns, const Target* targets, int numTargets) const;

	struct TagPair
	{
		const crTag* m_PatternTag;
		const crTag* m_TargetTag;
		int m_Cycle;
	};

	// PURPOSE: Match a tag pattern with a target set of tags, identifying matching tag pairs
	int CalcPatternMatchWithOffset(Pattern& pattern, const Target& target, float patternOffset, float targetOffset, TagPair* matches) const;
	int CalcPatternMatch(Pattern& pattern, const Target& target, TagPair* matches) const;

	typedef atFixedArray<SyncTagObserver::AlignmentKey, 8> ExtrapolatedAlignmentKeyArray;

	// PRUPOSE: Extrapolate estimates for min/max reference keys - plus optionally generate additional alignment keys
	bool CalcExtrapolatedMinMaxReference(TagPair* matches, int numTags, int lowestTagIdx, bool looping, float& minRef, float& maxRef, ExtrapolatedAlignmentKeyArray& startKeys, ExtrapolatedAlignmentKeyArray& endKeys);

	// PURPOSE: Build, or rebuild alignment data
	void BuildAlignments();

	// PURPOSE: Apply alignment data, synchronizing the targets
	void ApplyAlignments(bool dominant);

	// PURPOSE: Calculate the signature for all targets
	u32 CalcSignature();

private:
	static const u32 sm_MaxNumProperties = 4;
	atFixedArray<const crProperty*, sm_MaxNumProperties> m_Properties;

	u32 m_Signature;
	bool m_ForceLooping;
};

////////////////////////////////////////////////////////////////////////////////

inline bool crmtSynchronizer::AddTarget(const crmtObserver& observer, float weight, bool delay, bool autoUpdateWeight, bool immutable, bool warp)
{
	if(observer.IsAttached())
	{
		return AddTargetByNode(*observer.GetNode(), weight, delay, autoUpdateWeight, immutable, warp);
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtSynchronizer::RemoveTarget(const crmtObserver& observer)
{
	if(observer.IsAttached())
	{
		return RemoveTargetByNode(*observer.GetNode());
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtSynchronizer::IsAutoAddTarget() const
{
	return m_AutoAddTarget;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtSynchronizer::SetAutoAddTarget(bool autoAddTarget)
{
	m_AutoAddTarget = autoAddTarget;
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtSynchronizer::SyncObserver::GetWeight() const
{
	return m_ZeroWeight?0.f:m_Weight;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtSynchronizer::SyncObserver::SetWeight(float weight)
{
	m_Weight = weight;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtSynchronizer::SyncObserver::IsDelayed() const
{
	return m_Delay;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtSynchronizer::SyncObserver::IsDisabled() const
{
	return m_Disabled;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtSynchronizer::SyncObserver::SetDisabled(bool disable)
{
	m_Disabled = disable;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtSynchronizer::SyncObserver::ClearDelay()
{
	m_Delay = false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtSynchronizer::SyncObserver::IsFirstSync() const
{
	return m_FirstSync;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtSynchronizer::SyncObserver::ClearFirstSync()
{
	m_FirstSync = false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtSynchronizer::SyncObserver::DoesFirstSyncWarp() const
{
	return m_FirstSyncWarp;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtSynchronizer::SyncObserver::IsImmutable() const
{
	return m_Immutable;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtSynchronizer::SyncObserver::SetImmutable(bool immutable)
{
	m_Immutable = immutable;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtSynchronizer::SyncObserver* crmtSynchronizer::CreateSyncObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp)
{
	return rage_new SyncObserver(delay, autoUpdateWeight, immutable, warp);
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtSynchronizerTag::SetForceLooping(bool forceLooping)
{
	m_ForceLooping = forceLooping;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtSynchronizerTag::AddProperty(const crProperty* prop)
{
	m_Properties.Append() = prop;
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtSynchronizerTag::SyncTagObserver::GetMinReference() const
{
	return m_MinReference;
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtSynchronizerTag::SyncTagObserver::GetMaxReference() const
{
	return m_MaxReference;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtSynchronizer::SyncObserver* crmtSynchronizerTag::CreateSyncObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp)
{
	return rage_new SyncTagObserver(delay, autoUpdateWeight, immutable, warp);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_SYNCHRONIZER_H
