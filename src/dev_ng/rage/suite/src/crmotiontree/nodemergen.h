//
// crmotiontree/nodemergen.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEMERGEN_H
#define CRMOTIONTREE_NODEMERGEN_H

#include "nodeblendn.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Merge N input child nodes into a single output
class crmtNodeMergeN : public crmtNodeN
{
public:
	// PURPOSE: Default constructor
	crmtNodeMergeN();

	// PURPOSE: Resource constructor
	crmtNodeMergeN(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeMergeN();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeMergeN);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODEMERGEN_H
