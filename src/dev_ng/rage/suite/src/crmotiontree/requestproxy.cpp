//
// crmotiontree/requestproxy.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestproxy.h"

#include "nodeproxy.h"
#include "observer.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestProxy::crmtRequestProxy()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestProxy::crmtRequestProxy(crmtNode* node)
{
	Init(node);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestProxy::~crmtRequestProxy()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestProxy::Init(crmtNode* node)
{
	SetProxyNode(node);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestProxy::Shutdown()
{
	SetProxyNode(NULL);

	crmtRequest::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestProxy::GetProxyNode() const
{
	return m_Observer.GetNode();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestProxy::SetProxyNode(crmtNode* node)
{
	if(node)
	{
		m_Observer.Attach(*node);
	}
	else
	{
		m_Observer.Detach();
	}
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestProxy::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeProxy* proxyNode = crmtNodeProxy::CreateNode(tree);
	Assert(proxyNode);

	proxyNode->Init(m_Observer.GetNode());
	if(node)
	{
		node->Replace(*proxyNode);
	}

	return proxyNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
