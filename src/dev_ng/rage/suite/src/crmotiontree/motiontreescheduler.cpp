//
// crmotiontree/motiontreescheduler.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "motiontreescheduler.h"

#include "composer.h"
#include "composerdata.h"
#include "motiontree.h"

#include "profile/profiler.h"
#include "system/dependencyscheduler.h"

DECLARE_FRAG_INTERFACE(dependency_compose)

PF_PAGE(crmtDependencyPage,"crmt Dependencies");
PF_GROUP(crmtDependencies);
PF_LINK(crmtDependencyPage,crmtDependencies);

#if !__PS3
#include "dependency_compose.frag"
#endif

namespace rage
{

__THREAD u32 crmtMotionTreeScheduler::sm_ThreadIdx = crmtMotionTreeScheduler::sm_MaxNumThreads;

////////////////////////////////////////////////////////////////////////////////

crmtMotionTreeScheduler::crmtMotionTreeScheduler()
{
	Init();
}

////////////////////////////////////////////////////////////////////////////////

crmtMotionTreeScheduler::~crmtMotionTreeScheduler()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTreeScheduler::Init()
{
	m_NumActives = 0;
	for(u32 i=0; i < sm_MaxNumThreads; i++)
	{
		m_Waits[i] = sysIpcCreateSema(0);
	}

	m_NumThreads = 0;
	sm_ThreadIdx = 0;

#if SYS_DEPENDENCY_PROFILE
	static const char* tagNames[] = { "update", "postupdate", "compose", "recycle", "predelegate", "middelegate", "postdelegate", "add", "animation", "blend", "expression", "filter", "frame", "identity", "ik", "invalid", "inversepose", "merge", "normalize", "pose", "mover", "physical", "proxy", "nodepm", "noden", "mirror", "style", "extrapolate", "shadervars", "freeheap", "popbounds"};
	for(u32 i=0, count=NELEM(tagNames); i < count; i++)
	{
		sysDependencyScheduler::RegisterTag(PFGROUP_crmtDependencies, i+1, tagNames[i]);
	}
#endif
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTreeScheduler::Shutdown()
{
	for(u32 i=0; i < sm_MaxNumThreads; i++)
	{
		sysIpcDeleteSema(m_Waits[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTreeScheduler::Schedule(crmtMotionTree& motionTree, float deltaTime, u32 priority, UpdateDelegate preDelegate, UpdateDelegate midDelegate, UpdateDelegate postDelegate, void* payload, bool update, bool compose, crmtMotionTree* parent)
{
	enum { kTagUpdate=1, kTagPostUpdate, kTagCompose, kTagRecycle, kTagPreDelegate, kTagMidDelegate, kTagPostDelegate };

	Assertf(!sm_ThreadIdx, "Only the main thread can schedule a motion tree");
	const u8 maxPriority = sysDependency::kPriorityHigh;
	u8 basePriority = Min(u8(priority), maxPriority);
	u8 biasPriority = Min(u8(priority+1), maxPriority);
	for(u32 i=0; i < 2; i++)
	{
		Assertf(motionTree.m_States[i]==kStateIdle, "Motion tree is still pending!");
		motionTree.m_States[i] = kStateActive;
		motionTree.m_Threads[i] = 0;
	}

	// create dependencies
	sysDependency& depRecycle = motionTree.m_Recycle;
	depRecycle.Init(crmtMotionTreeScheduler::DependencyRecycleHandler, kTagRecycle);
	depRecycle.m_Priority = biasPriority;
	depRecycle.m_Params[0].m_AsPtr = this;
	depRecycle.m_Params[1].m_AsPtr = &motionTree;
	depRecycle.m_Params[2].m_AsPtr = payload;
	depRecycle.m_Params[3].m_AsPtr = reinterpret_cast<void*>(postDelegate);

	const u32 parentType = sysDependency::kParentStrong;
	sysDependency* last = &depRecycle;
	if(compose)
	{
		sysDependency& depCompose = motionTree.m_Compose;
		depCompose.Init(FRAG_SPU_CODE(dependency_compose), kTagCompose, sysDepFlag::INPUT1|sysDepFlag::INPUT2|sysDepFlag::ALLOC0);
		depCompose.m_Priority = basePriority;
		depCompose.m_Params[0].m_AsPtr = NULL;
		depCompose.m_Params[1].m_AsPtr = &motionTree.m_ComposerData;
		depCompose.m_Params[2].m_AsConstPtr = &motionTree.GetFrameData();
		depCompose.m_Params[3].m_AsPtr = &crmtComposerData::GetHeap();
		depCompose.m_Params[4].m_AsPtr = motionTree.GetRootNode();
		depCompose.m_DataSizes[0] = sizeof(sysDependency)*crmtComposer::sm_MaxNumDependencies + SYS_USE_SPU_DEPENDENCY*11*1024;
		depCompose.m_DataSizes[1] = sizeof(crmtComposerData);
		depCompose.m_DataSizes[2] = sizeof(crFrameData);
		depCompose.m_Parents[parentType] = last;
		last->m_NumPending = 1;
		last = &depCompose;
	}

	sysDependency& depUpdate = motionTree.m_Update;
	depUpdate.Init(crmtMotionTreeScheduler::DependencyUpdateHandler, kTagUpdate);
	depUpdate.m_Priority = basePriority;
	depUpdate.m_Params[0].m_AsPtr = this;
	depUpdate.m_Params[1].m_AsPtr = &motionTree;
	depUpdate.m_Params[2].m_AsFloat = deltaTime;
	depUpdate.m_Params[3].m_AsBool = update;
	depUpdate.m_Params[4].m_AsPtr = payload;
	depUpdate.m_Params[5].m_AsPtr = reinterpret_cast<void*>(preDelegate);
	depUpdate.m_Params[6].m_AsPtr = reinterpret_cast<void*>(midDelegate);
	depUpdate.m_Parents[parentType] = last;
	last->m_NumPending = 1;
	last = &depUpdate;

	sysInterlockedIncrement(&m_NumActives);

	// insert dependency
	if(parent && parent->m_States[1] != kStateIdle)
	{
		Attach(last, &parent->m_Recycle);
	}
	else
	{
		sysDependencyScheduler::Insert(last);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTreeScheduler::Attach(sysDependency* dep, sysDependency* parent) const
{
	// get top dependency
	sysDependency* top = NULL;
	for(sysDependency* next = parent;  next != NULL; next = next->m_Parents[sysDependency::kParentStrong])
	{
		top = next;	
	}

	if(sysInterlockedIncrement(&top->m_NumPending) == 1) // parent was already completed
	{
		top->m_NumPending = 0;

		sysDependencyScheduler::Insert(dep);
	}
	else // attach dependency to parent
	{
		dep->AddChild(*top);

		if(!sysInterlockedDecrement(&top->m_NumPending)) // parent just got completed
		{
			sysDependencyScheduler::Insert(top);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTreeScheduler::SignalThreads(u32 threads)
{
	for(u32 i=0, bit=1; i < sm_MaxNumThreads; i++, bit<<=1)
	{
		if(threads & bit)
		{
			sysIpcSignalSema(m_Waits[i]);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTreeScheduler::WaitThread(u32 threadIdx)
{
	while(!sysIpcWaitSemaTimed(m_Waits[threadIdx], 10 * 1000))
	{
		NOTFINAL_ONLY(sysDependencyScheduler::OutputDebug());
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crmtMotionTreeScheduler::IsComplete(const crmtMotionTree& motionTree, bool updateOnly)
{
	return sysInterlockedRead(&motionTree.m_States[!updateOnly]) == kStateIdle;
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTreeScheduler::WaitOnComplete(crmtMotionTree& motionTree, bool elevatePriority, bool updateOnly)
{
	// lazy initialization of the thread index
	u32 threadIdx = sm_ThreadIdx;
	if(Unlikely(threadIdx == crmtMotionTreeScheduler::sm_MaxNumThreads))
	{
		threadIdx = sysInterlockedIncrement(&m_NumThreads);
		Assert(threadIdx < crmtMotionTreeScheduler::sm_MaxNumThreads);
		sm_ThreadIdx = threadIdx;
	}

	// avoid synchronization if motion tree is already finished
	u32 stageIdx = u32(!updateOnly);
	if(Unlikely(motionTree.m_States[stageIdx] != kStateIdle))
	{
		if(elevatePriority)
		{
			motionTree.m_Update.m_Priority = motionTree.m_Compose.m_Priority = motionTree.m_Recycle.m_Priority = sysDependency::kPriorityCritical;
		}

		sysIpcPollSema(m_Waits[threadIdx]);
		sysInterlockedOr(&motionTree.m_Threads[stageIdx], 1<<threadIdx);

		// check again if the motion tree is still not finished
		if(sysInterlockedCompareExchange(&motionTree.m_States[stageIdx], kStatePending, kStateActive) != kStateIdle)
		{
			WaitThread(threadIdx);
		}
		Assert(motionTree.m_States[stageIdx]==kStateIdle);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtMotionTreeScheduler::WaitOnAllComplete()
{
	if(Verifyf(!sm_ThreadIdx, "Can't wait on all motion trees from another thread"))
	{
		u32 numJobs = sysInterlockedOr(&m_NumActives, sm_WaitOnAllFlag);
		Assert(!(numJobs & sm_WaitOnAllFlag));
		if(numJobs != 0)
		{
			WaitThread(0);
		}
		sysInterlockedAnd(&m_NumActives, ~sm_WaitOnAllFlag);
		Assert(!m_NumActives);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crmtMotionTreeScheduler::DependencyUpdateHandler(const sysDependency& dep)
{
	// pre delegate
	void* payload = dep.m_Params[4].m_AsPtr;
	UpdateDelegate preDelegate = reinterpret_cast<UpdateDelegate>(dep.m_Params[5].m_AsPtr);
	if(preDelegate)
	{
		preDelegate(payload);
	}

	// update motion tree
	crmtMotionTreeScheduler& scheduler = *static_cast<crmtMotionTreeScheduler*>(dep.m_Params[0].m_AsPtr);
	crmtMotionTree& motionTree = *static_cast<crmtMotionTree*>(dep.m_Params[1].m_AsPtr);
	f32 deltaTime = dep.m_Params[2].m_AsFloat;
	f32 update = dep.m_Params[3].m_AsBool;
	if(update)
	{
		motionTree.Update(deltaTime);
	}

	// mid delegate
	UpdateDelegate midDelegate = reinterpret_cast<UpdateDelegate>(dep.m_Params[6].m_AsPtr);
	if(midDelegate)
	{
		midDelegate(payload);
	}

	// signal update is complete
	if(sysInterlockedExchange(&motionTree.m_States[0], kStateIdle) == kStatePending)
	{
		scheduler.SignalThreads(motionTree.m_Threads[0]);
	}

	// prepare compose dependency
	motionTree.m_ComposerData.Start(deltaTime);
	motionTree.m_Compose.m_Params[4].m_AsPtr = motionTree.GetRootNode();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtMotionTreeScheduler::DependencyRecycleHandler(const sysDependency& dep)
{
	// post delegate, should be moved after the crmtComposerData::Finish once the shader pose is on SPU
	void* payload = dep.m_Params[2].m_AsPtr;
	UpdateDelegate postDelegate = reinterpret_cast<UpdateDelegate>(dep.m_Params[3].m_AsPtr);
	if(postDelegate)
	{
		postDelegate(payload);
	}

	crmtMotionTreeScheduler& scheduler = *static_cast<crmtMotionTreeScheduler*>(dep.m_Params[0].m_AsPtr);
	crmtMotionTree& motionTree = *static_cast<crmtMotionTree*>(dep.m_Params[1].m_AsPtr);
	motionTree.m_ComposerData.Finish();

	// signal compose is complete
	if(sysInterlockedExchange(&motionTree.m_States[1], kStateIdle) == kStatePending)
	{
		scheduler.SignalThreads(motionTree.m_Threads[1]);
	}

	// signal all motion trees completed
	u32 numActives = sysInterlockedDecrement(&scheduler.m_NumActives);
	if(numActives == sm_WaitOnAllFlag)
	{
		sysIpcSignalSema(scheduler.m_Waits[0]);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
