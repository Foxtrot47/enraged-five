//
// crmotiontree/nodeaddsubtract.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodeaddsubtract.h"

#include "observer.h"

#include "cranimation/frame.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeAddSubtract::crmtNodeAddSubtract()
: crmtNodePairWeighted(kNodeAddSubtract)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeAddSubtract::crmtNodeAddSubtract(datResource& rsc)
: crmtNodePairWeighted(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeAddSubtract::~crmtNodeAddSubtract()
{
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeAddSubtract, crmtNode::kNodeAddSubtract, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeAddSubtract::Init(float weight, crFrameFilter* filter)
{
	SetWeight(weight);
	SetFilter(filter);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeAddSubtract::Shutdown()
{
	ShuttingDown();
	crmtNodePairWeighted::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeAddSubtract::Update(float dt)
{
	crmtNodePairWeighted::Update(dt);

	float weight = m_Weight + m_WeightRate * dt;

	crmtNode* child0 = GetFirstChild();
	crmtNode* child1 = child0->GetNextSibling();

	bool disabled = false, silent = false;
	if(FPAbs(weight) <= WEIGHT_ROUNDING_ERROR_TOLERANCE)
	{
		child1->SetDisabled(true);
		child1->SetSilent(true);
		silent = true;
	}

	if(child0->IsDisabled() && child1->IsDisabled())
	{
		disabled = true;
		silent = false;
	}
	else if(child1->IsDisabled())
	{
		child1->SetSilent(true);
		silent = true;
	}

	SetDisabled(disabled);
	SetSilent(silent);

	m_Weight = weight;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
