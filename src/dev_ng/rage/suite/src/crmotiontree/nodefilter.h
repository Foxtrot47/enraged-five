//
// crmotiontree/nodefilter.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEFILTER_H
#define CRMOTIONTREE_NODEFILTER_H

#include "node.h"

namespace rage
{

class crFrameFilter;


// PURPOSE: Filter dofs from input.
class crmtNodeFilter : public crmtNodeParent
{
public:

	// PURPOSE: Default constructor
	crmtNodeFilter();

	// PURPOSE: Resource constructor
	crmtNodeFilter(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeFilter();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeFilter);

	// PURPOSE: Initialization
	// PARAMS: filter - frame filter to use for filtering (can be NULL)
	void Init(crFrameFilter* filter);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Update node
	void Update(float dt);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh();

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;


	// PURPOSE: Set frame filter
	// PARAMS: filter - frame filter to use for filtering (can be NULL)
	void SetFilter(crFrameFilter* filter);


	// PURPOSE: Get frame filter
	// RETURNS: frame filter currently used for filtering (may be NULL)
	crFrameFilter* GetFilter() const;

	// PURPOSE: Returns indices
	const crLock& GetWeights() const;

private:
	crLock m_Weights;
	crFrameFilter* m_Filter;
	u32 m_FilterSignature;
};

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodeFilter::GetWeights() const
{
	return m_Weights;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRMOTIONTREE_NODEFILTER_H
