//
// crmotiontree/dependency_inversepose.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_INVERSEPOSE_FRAG
#define MOTIONTREE_DEPENDENCY_INVERSEPOSE_FRAG

#include "crmotiontree/crmtdiag.h"
#include "system/cache.h"
#include "system/dependency.h"
#include "system/memops.h"
#include "vectormath/classes.h"

using namespace rage;
using namespace Vec;

SPUFRAG_DECL(bool, dependency_inversepose, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_inversepose, const sysDependency& dep)
{
	crmtDebug3("dependency_inversepose");

	u8* RESTRICT destFrame = static_cast<u8*>(dep.m_Params[0].m_AsPtr);
	const ua16* RESTRICT indices = static_cast<const ua16*>(dep.m_Params[1].m_AsConstPtr);
	const u16* RESTRICT parentIndices = static_cast<const u16*>(dep.m_Params[2].m_AsConstPtr);
	Mat34V* RESTRICT locals = static_cast<Mat34V*>(dep.m_Params[3].m_AsPtr);
	unsigned int numBones = dep.m_Params[4].m_AsInt;
	bool localPoseOnly = dep.m_Params[5].m_AsBool;
	bool suppressReset = dep.m_Params[6].m_AsBool;
	unsigned int destFrameSize = dep.m_DataSizes[0];

	 // Compute local matrices from object matrices
	if(!localPoseOnly)
	{
		const Mat34V* objects = locals + numBones;
		for(s32 i=numBones-1; i > 0; --i)
		{
			PrefetchDC(objects+i-4);
			PrefetchDC(locals+i-4);
			UnTransformOrtho(locals[i], objects[parentIndices[i]], objects[i]);
		}
	}

	// Invalidate destination frame
	if(!suppressReset)
	{
		sysMemSet(destFrame, 0xff, destFrameSize);
	}

	QuatV _invalid = QuatV(V4VConstant(V_MASKXYZW));

	unsigned int countTranslations = RAGE_COUNT(indices[0], 2);
	unsigned int countRotations = RAGE_COUNT(indices[1], 2);
	unsigned int countScales = RAGE_COUNT(indices[2], 2);
	indices += 8;

	// Retrieve translation dofs from local matrices
	for(; countTranslations; countTranslations--)
	{
		Vec3V* pDest0 = reinterpret_cast<Vec3V*>(destFrame + indices[0]);
		Vec3V* pDest1 = reinterpret_cast<Vec3V*>(destFrame + indices[2]);
		Vec3V* pDest2 = reinterpret_cast<Vec3V*>(destFrame + indices[4]);
		Vec3V* pDest3 = reinterpret_cast<Vec3V*>(destFrame + indices[6]);

		const Mat34V* pSrc0 = locals + indices[1];
		const Mat34V* pSrc1 = locals + indices[3];
		const Mat34V* pSrc2 = locals + indices[5];
		const Mat34V* pSrc3 = locals + indices[7];

		*pDest0 = pSrc0->GetCol3();
		*pDest1 = pSrc1->GetCol3();
		*pDest2 = pSrc2->GetCol3();
		*pDest3 = pSrc3->GetCol3();

		indices += 8;
	}

	// Retrieve rotation dofs from local matrices, remove scale and convert to quaternion
	for(; countRotations; countRotations--)
	{
		QuatV* pDest0 = reinterpret_cast<QuatV*>(destFrame + indices[0]);
		QuatV* pDest1 = reinterpret_cast<QuatV*>(destFrame + indices[2]);
		QuatV* pDest2 = reinterpret_cast<QuatV*>(destFrame + indices[4]);
		QuatV* pDest3 = reinterpret_cast<QuatV*>(destFrame + indices[6]);

		const Mat34V* pSrc0 = locals + indices[1];  Mat33V src0 = pSrc0->GetMat33();
		const Mat34V* pSrc1 = locals + indices[3];  Mat33V src1 = pSrc1->GetMat33();
		const Mat34V* pSrc2 = locals + indices[5];  Mat33V src2 = pSrc2->GetMat33();
		const Mat34V* pSrc3 = locals + indices[7];  Mat33V src3 = pSrc3->GetMat33();

		*pDest0 = QuatVFromMat33VSafe(src0, _invalid);
		*pDest1 = QuatVFromMat33VSafe(src1, _invalid);
		*pDest2 = QuatVFromMat33VSafe(src2, _invalid);
		*pDest3 = QuatVFromMat33VSafe(src3, _invalid);

		indices += 8;
	}

	// Retrieve scale dofs from local matrices
	for(; countScales; countScales--)
	{
		Vec3V* pDest0 = reinterpret_cast<Vec3V*>(destFrame + indices[0]);
		Vec3V* pDest1 = reinterpret_cast<Vec3V*>(destFrame + indices[2]);
		Vec3V* pDest2 = reinterpret_cast<Vec3V*>(destFrame + indices[4]);
		Vec3V* pDest3 = reinterpret_cast<Vec3V*>(destFrame + indices[6]);

		const Mat34V* pSrc0 = locals + indices[1];  Mat33V src0 = pSrc0->GetMat33();
		const Mat34V* pSrc1 = locals + indices[3];	Mat33V src1 = pSrc1->GetMat33();
		const Mat34V* pSrc2 = locals + indices[5];	Mat33V src2 = pSrc2->GetMat33();
		const Mat34V* pSrc3 = locals + indices[7];	Mat33V src3 = pSrc3->GetMat33();

		*pDest0 = ScaleFromMat33VTranspose(src0);
		*pDest1 = ScaleFromMat33VTranspose(src1);
		*pDest2 = ScaleFromMat33VTranspose(src2);
		*pDest3 = ScaleFromMat33VTranspose(src3);

		indices += 8;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_INVERSEPOSE_FRAG
