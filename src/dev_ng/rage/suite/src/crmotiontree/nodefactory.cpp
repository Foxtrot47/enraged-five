//
// crmotiontree/nodefactory.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodefactory.h"

#include "crmtdiag.h"

#include "profile/group.h"
#include "profile/page.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

#if CR_STATS
PF_PAGE(crmtNodeFactoryPage, "crmt NodeFactory");
PF_GROUP(crmtNodeFactoryStats);
PF_LINK(crmtNodeFactoryPage, crmtNodeFactoryStats);
PF_COUNTER_CUMULATIVE(crmtNodeFactory_AllocatedBytes, crmtNodeFactoryStats);
PF_COUNTER_CUMULATIVE(crmtNodeFactory_UsedPoolPages, crmtNodeFactoryStats);
PF_COUNTER_CUMULATIVE(crmtNodeFactory_HeapPages, crmtNodeFactoryStats);
PF_COUNTER_CUMULATIVE(crmtNodeFactory_UsedBlocks, crmtNodeFactoryStats);
PF_COUNTER_CUMULATIVE(crmtNodeFactory_WastedBytes, crmtNodeFactoryStats);
#endif // CR_STATS

////////////////////////////////////////////////////////////////////////////////

class crmtNodeFactoryAllocator
{
public:
	// PURPOSE:
	struct FreeChunk
	{
		FreeChunk* m_Next;
	};

	// PURPOSE:
	struct ChunkBlock
	{
		u8 m_Memory[crmtNodeFactory::kChunkMemSize-(sizeof(void*)*2)];
		crmtNodeFactoryAllocator* m_Owner;
		ChunkBlock* m_Next;
	};

	// PURPOSE:
	__forceinline ChunkBlock* CreateChunk(atPoolBase* p)
	{
		if (p != NULL)
		{
			if (!p->IsFull())
			{
#if CR_STATS
				PF_INCREMENT(crmtNodeFactory_UsedPoolPages);
#endif
				return reinterpret_cast<ChunkBlock*>(p->New(sizeof(ChunkBlock)));
			}
		}

#if CR_STATS
		PF_INCREMENT(crmtNodeFactory_HeapPages);
#endif
		return reinterpret_cast<ChunkBlock*>(rage_new u8[sizeof(ChunkBlock)]);
	}

	// PURPOSE:
	void DestroyChunk(ChunkBlock* chunk, atPoolBase* p)
	{
		if (p != NULL)
		{
			if (p->IsInPool((void*)chunk))
			{
#if CR_STATS
				PF_INCREMENTBY(crmtNodeFactory_UsedPoolPages, -1);
#endif
				p->Delete(chunk);
				return;
			}
		}

#if CR_STATS
		PF_INCREMENTBY(crmtNodeFactory_HeapPages, -1);
#endif
		delete reinterpret_cast<u8*>(chunk);
	}

	// PURPOSE:
	void Init(const u32 size)
	{
		Assert(size >= 4);
		m_FreeList = NULL;
		m_Hunks = NULL;
		m_HunkSize = RAGE_ALIGN(size, 4);
		m_ChunkCount = (crmtNodeFactory::kChunkMemSize-(sizeof(void*)*2))/m_HunkSize;
	}

	// PURPOSE:
	void Destroy(atPoolBase* pool)
	{
		ChunkBlock* hunk = m_Hunks;
		m_Hunks = NULL;

		while (hunk)
		{
#if CR_STATS
			PF_INCREMENTBY(crmtNodeFactory_WastedBytes, -(int) ((sizeof(hunk->m_Memory)/sizeof(hunk->m_Memory[0])) % m_HunkSize));
#endif // CR_STATS

			ChunkBlock* next = hunk->m_Next;
			DestroyChunk(hunk, pool);
			hunk = next;
		}
	}

	// PURPOSE:
	void* Alloc(atPoolBase* pool)
	{
		void* ptr = m_FreeList;
		if (ptr)
		{
			m_FreeList = m_FreeList->m_Next;
#if CR_STATS
			PF_INCREMENT(crmtNodeFactory_UsedBlocks);
			PF_INCREMENTBY(crmtNodeFactory_AllocatedBytes, m_HunkSize);
#endif // CR_STATS
			return ptr;
		}

		Extend(pool);

		ptr = m_FreeList;
		m_FreeList = m_FreeList->m_Next;
#if CR_STATS
		PF_INCREMENT(crmtNodeFactory_UsedBlocks);
		PF_INCREMENTBY(crmtNodeFactory_AllocatedBytes, m_HunkSize);
#endif // CR_STATS
		return ptr;
	}

	// PURPOSE:
	void Free(void* p)
	{
		Assert(p);

		((FreeChunk*)p)->m_Next = m_FreeList;
		m_FreeList = (FreeChunk*)p;
#if CR_STATS
		PF_INCREMENTBY(crmtNodeFactory_UsedBlocks, -1);
		PF_INCREMENTBY(crmtNodeFactory_AllocatedBytes, -(int) m_HunkSize);
#endif // CR_STATS
	}

	// PURPOSE:
	void Extend(atPoolBase* pool)
	{
		ChunkBlock* hunk = CreateChunk(pool);
		hunk->m_Owner = this;
		hunk->m_Next = m_Hunks;
		m_Hunks = hunk;

		// add the new chunks to the free list
		u8* ptr = m_Hunks->m_Memory;
		FreeChunk** freePtr = &m_FreeList;
		for (u32 i = 0; i < m_ChunkCount; ++i)
		{
			FreeChunk* free = (FreeChunk*)ptr;
			*freePtr = free;
			freePtr = &(free->m_Next);
			ptr += m_HunkSize;
		}
		*freePtr = NULL;

#if CR_STATS
		PF_INCREMENTBY(crmtNodeFactory_WastedBytes, (sizeof(hunk->m_Memory)/sizeof(hunk->m_Memory[0])) % m_HunkSize);
#endif // CR_STATS
	}

	FreeChunk* m_FreeList;
	ChunkBlock* m_Hunks;
	u32 m_HunkSize;
	u32 m_ChunkCount;
};

////////////////////////////////////////////////////////////////////////////////

__forceinline u32 crmtNodeFactory::IndexToSize(const int index, const int sizes[])
{
	if (index < 0) return 0;
	return sizes[index];
}

////////////////////////////////////////////////////////////////////////////////

__forceinline u32 crmtNodeFactory::SizeToIndex(const crmtNodeFactory& factory, const u32 size)
{
	Assert(size <= factory.m_MaxBytes);
	Assert(factory.m_SizeToIndexTable[factory.m_MaxBytes] != 0);
	return factory.m_SizeToIndexTable[size];
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFactory::InitNodeFactoryAllocator(crmtNodeFactory& factory, const int sizes[], u8 numSizes)
{
	if (factory.m_Allocators)
	{
		return;
	}

	factory.m_MaxBytes = sizes[numSizes-1];
	factory.m_NumAllocators = numSizes;

	Assert(factory.m_MaxBytes <= crmtNodeFactory::kMaxAllocatorBytes);

	crmtNodeFactoryAllocator* allocs = rage_new crmtNodeFactoryAllocator[factory.m_NumAllocators];
	for (u8 i = 0; i < u8(factory.m_NumAllocators); ++i)
	{
		u32 size = IndexToSize(int(i), sizes);
		allocs[i].Init(size);
		while (size > IndexToSize(int(i)-1, sizes))
		{
			factory.m_SizeToIndexTable[size] = i;
			--size;
		}
	}
	factory.m_Allocators = allocs;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFactory::DestroyNodeFactoryAllocator(crmtNodeFactory& factory)
{
	crmtNodeFactoryAllocator* allocs = factory.m_Allocators;
	factory.m_Allocators = NULL;
	if (!allocs)
	{
		return;
	}

	for (u32 i = 0; i < factory.m_NumAllocators; ++i)
	{
		allocs[i].Destroy(factory.m_Pool);
	}

	delete[] allocs;
}

////////////////////////////////////////////////////////////////////////////////

void* crmtNodeFactory::Alloc(crmtNodeFactory& factory, const u32 size)
{
	if (size <= factory.m_MaxBytes)
	{
		const u32 which = SizeToIndex(factory, size);
		Assert(which < factory.m_NumAllocators);

#if NODE_FACTORY_THREAD_SAFE
		sysCriticalSection cs(factory.m_CsToken);
#endif // NODE_FACTORY_THREAD_SAFE

		void* ptr = factory.m_Allocators[which].Alloc(factory.m_Pool);
		return ptr;
	}
	else
	{
		// Print out a warning that we're not allocating out of the buffer
		void* ptr = rage_new u8[size];
		return ptr;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFactory::Free(crmtNodeFactory& factory, void* p, const u32 size, bool unsafe)
{
	if (!p)
	{
		return;
	}

	if (size <= factory.m_MaxBytes)
	{
		Assert(factory.m_Allocators);
		const int which = SizeToIndex(factory, size);
		Assert(which >= 0 && which < int(factory.m_NumAllocators));

		if(!unsafe)
		{
#if NODE_FACTORY_THREAD_SAFE
			factory.m_CsToken.Lock();
#endif // NODE_FACTORY_THREAD_SAFE
		}

		factory.m_Allocators[which].Free(p);

		if(!unsafe)
		{
#if NODE_FACTORY_THREAD_SAFE
			factory.m_CsToken.Unlock();
#endif // NODE_FACTORY_THREAD_SAFE
		}
	}
	else
	{
		delete[] reinterpret_cast<u8*>(p);
	}
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeFactory::~crmtNodeFactory()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeFactory::crmtNodeFactory(const int sizes[], u8 numSizes)
: m_Allocators(NULL)
, m_Pool(NULL)
{
	InitNodeFactoryAllocator(*this, sizes, numSizes);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFactory::CreatePagePool(u16 numPages)
{
#if COMMERCE_CONTAINER
	m_Pool = rage_new atPoolBase(1<<13, numPages, true);
#else
	m_Pool = rage_new atPoolBase(1<<13, numPages);
#endif
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFactory::Shutdown()
{
	DestroyNodeFactoryAllocator(*this);

	delete m_Pool;
	m_Pool = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFactory::DeleteNodeInternal(const crmtNode* node, bool unsafe)
{
#if CRMT_NODE_2_DEF_MAP
	if(PARAM_movedebug.Get())
	{
		const void **ppDef = g_crmtNode2DefMap.Access(node);
		if(Verifyf(ppDef != NULL, "Node does not exist in g_crmtNode2DefMap"))
		{
			g_crmtNode2DefMap.Delete(node);
		}
	}
#endif //CRMT_NODE_2_DEF_MAP

	const crmtNode::NodeTypeInfo& nodeInfo = node->GetNodeTypeInfo();
	Free(*this, const_cast<crmtNode*>(node), nodeInfo.GetSize(), unsafe);
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtNodeFactory::NewNode(crmtNode::eNodes nodeType)
{
	const crmtNode::NodeTypeInfo* nodeInfo = crmtNode::FindNodeTypeInfo(nodeType);
	Assert(nodeInfo);
	crmtNode* node = static_cast<crmtNode*>(Alloc(*this, nodeInfo->GetSize()));
	Assert(node);

#if CRMT_NODE_2_DEF_MAP
	if(PARAM_movedebug.Get())
	{
		const void **ppDef = g_crmtNode2DefMap.Access(node);
		if(Verifyf(ppDef == NULL, "Node already exists in g_crmtNode2DefMap"))
		{
			g_crmtNode2DefMap.Insert(node, NULL);
		}
	}
#endif //CRMT_NODE_2_DEF_MAP

	// call placement new for newly allocated class
	nodeInfo->GetPlaceFn()(node);
	node->m_Factory = this;
	return node;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
