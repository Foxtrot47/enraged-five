//
// crmotiontree/requestcapture.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestcapture.h"

#include "nodecapture.h"

#include "cranimation/frame.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestCapture::crmtRequestCapture()
: m_Frame(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestCapture::crmtRequestCapture(crmtRequest& source, crFrame* frame)
: m_Frame(NULL)
{
	Init(source, frame);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestCapture::crmtRequestCapture(crFrame* frame)
: m_Frame(NULL)
{
	Init(frame);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestCapture::~crmtRequestCapture()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestCapture::Init(crmtRequest& source, crFrame* frame)
{
	SetSource(&source, 0);
	SetFrame(frame);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestCapture::Init(crFrame* frame)
{
	SetFrame(frame);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestCapture::Shutdown()
{
	SetFrame(NULL);

	crmtRequestSource<1>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestCapture::SetFrame(crFrame* frame)
{
	if(frame)
	{
		frame->AddRef();
	}
	if(m_Frame)
	{
		m_Frame->Release();
	}
	m_Frame = frame;
}

////////////////////////////////////////////////////////////////////////////////

crFrame* crmtRequestCapture::GetFrame() const
{
	return m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestCapture::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeCapture* captureNode = crmtNodeCapture::CreateNode(tree);
	Assert(captureNode);

	captureNode->Init(m_Frame);

	GenerateSourceNodes(tree, node, captureNode);

	return captureNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
