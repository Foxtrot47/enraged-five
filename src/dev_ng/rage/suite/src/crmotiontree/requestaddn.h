//
// crmotiontree/requestaddn.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTADDN_H
#define CRMOTIONTREE_REQUESTADDN_H

#include "requestblendn.h"

namespace rage
{
	
////////////////////////////////////////////////////////////////////////////////


// PURPOSE: N-way add request
// Capable of blending multiple inputs into a single output
class crmtRequestAddN : public crmtRequestN
{
public:

	// PURPOSE: Default constructor
	crmtRequestAddN();

	// PURPOSE: Destructor
	virtual ~crmtRequestAddN();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);
};


////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CRMOTIONTREE_REQUESTADDN_H
