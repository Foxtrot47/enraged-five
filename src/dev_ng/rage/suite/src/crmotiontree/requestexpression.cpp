//
// crmotiontree/requestexpression.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "requestexpression.h"

#include "nodeexpression.h"

#include "crextra/expressions.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestExpression::crmtRequestExpression()
: m_Expressions(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestExpression::crmtRequestExpression(crmtRequest& source, const crExpressions* expressions)
: m_Expressions(NULL)
{
	Init(source, expressions);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestExpression::crmtRequestExpression(const crExpressions* expressions)
: m_Expressions(NULL)
{
	Init(expressions);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestExpression::~crmtRequestExpression()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestExpression::Init(crmtRequest& source, const crExpressions* expressions)
{
	SetSource(&source, 0);
	SetExpressions(expressions);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestExpression::Init(const crExpressions* expressions)
{
	SetExpressions(expressions);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestExpression::Shutdown()
{
	SetExpressions(NULL);

	crmtRequestSource<1>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

const crExpressions* crmtRequestExpression::GetExpressions() const
{
	return m_Expressions;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestExpression::SetExpressions(const crExpressions* expressions)
{
	if(expressions)
	{
		expressions->AddRef();
	}
	if(m_Expressions)
	{
		m_Expressions->Release();
	}
	m_Expressions = expressions;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestExpression::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeExpression* expressionNode = crmtNodeExpression::CreateNode(tree);
	expressionNode->Init(m_Expressions);

	GenerateSourceNodes(tree, node, expressionNode);

	return expressionNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
