//
// crmotiontree/requestanimation.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestanimation.h"

#include "nodeanimation.h"

#include "cranimation/animation.h"
#include "cranimation/animplayer.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestAnimation::crmtRequestAnimation()
: m_Animation(NULL)
, m_Time(0.f)
, m_Rate(1.f)
, m_IgnoreAnimLooping(false)
, m_ForceLooping(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestAnimation::crmtRequestAnimation(const crAnimation* anim, float time, float rate)
: m_Animation(NULL)
, m_Time(0.f)
, m_Rate(1.f)
, m_IgnoreAnimLooping(false)
, m_ForceLooping(false)
{
	Init(anim, time, rate);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestAnimation::~crmtRequestAnimation()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAnimation::Init(const crAnimation* anim, float time, float rate)
{
	SetAnimation(anim);
	SetTime(time);
	SetRate(rate);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAnimation::Shutdown()
{
	SetAnimation(NULL);

	crmtRequest::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAnimation::SetAnimation(const crAnimation* anim)
{
	if(anim)
	{
		anim->AddRef();
	}
	if(m_Animation)
	{
		m_Animation->Release();
	}
	m_Animation = anim;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAnimation::SetTime(float time)
{
	m_Time = time;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAnimation::SetRate(float rate)
{
	m_Rate = rate;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAnimation::SetLooping(bool ignoreAnimLooping, bool forceLooping)
{
	m_IgnoreAnimLooping = ignoreAnimLooping;
	m_ForceLooping = forceLooping;
}

////////////////////////////////////////////////////////////////////////////////

const crAnimation* crmtRequestAnimation::GetAnimation() const
{
	return m_Animation;
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestAnimation::GetTime() const
{
	return m_Time;
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestAnimation::GetRate() const
{
	return m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtRequestAnimation::GetLooping(bool& outIgnoreAnimLooping, bool& outForceLooping) const
{
	outIgnoreAnimLooping = m_IgnoreAnimLooping;
	outForceLooping = m_ForceLooping;

	if(m_IgnoreAnimLooping)
	{
		return m_ForceLooping;
	}
	else
	{
		if(m_Animation)
		{
			return m_Animation->IsLooped();
		}
		else
		{
			// odd case, asked to respect the animation's looping status, but no valid animation
			return false;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestAnimation::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeAnimation* animNode = crmtNodeAnimation::CreateNode(tree);
	Assert(animNode);

	animNode->Init(m_Animation, m_Time, m_Rate);
	animNode->GetAnimPlayer().SetLooped(m_IgnoreAnimLooping, m_ForceLooping);

	if(node)
	{
		node->Replace(*animNode);
	}

	return animNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
