//
// crmotiontree/nodeexpression.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRMOTIONTREE_NODEEXPRESSION_H
#define CRMOTIONTREE_NODEEXPRESSION_H

#include "node.h"

#include "crextra/expressionplayer.h"

namespace rage
{

class crExpressionPlayer;
class crExpressions;


// PURPOSE: Calculates expression driven dofs.
class crmtNodeExpression : public crmtNodeParent
{
public:

	// PURPOSE: Default constructor
	crmtNodeExpression();

	// PURPOSE: Resource constructor
	crmtNodeExpression(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeExpression();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeExpression);

	// PURPOSE: Initialization
	// PARAMS: expressions - expressions
	void Init(const crExpressions* expressions);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Update node
	virtual void Update(float dt);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper&) const;
#endif // CR_DEV

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh();

	// PURPOSE: Get the expression player (const)
	// RETURNS: reference to the expression player
	const crExpressionPlayer& GetExpressionPlayer() const;

	// PURPOSE: Get the expression player
	// RETURNS: reference to the expression player
	crExpressionPlayer& GetExpressionPlayer();


	// PURPOSE: Get the current weight
	// RETURNS: Current weight [0..1]
	float GetWeight() const;

	// PURPOSE: Set the current weight
	// PARAMS: weight - [0..1]
	// NOTES: A weight of 0.0 disables the expressions, while a weight of 1.0 fully enables them.
	// Anything in between these values causes the expressions to still be evaluated,
	// but the results are then blended back with the original input values.
	void SetWeight(float weight);

	// PURPOSE: Returns frame indices
	const crLock& GetFrameIndices() const;

	// PURPOSE: Returns skeleton indices
	const crLock& GetSkelIndices() const;

private:
	crExpressionPlayer m_ExpressionPlayer;
	crLock m_FrameIndices;
	crLock m_SkelIndices;
	float m_Weight;
};

////////////////////////////////////////////////////////////////////////////////

inline const crExpressionPlayer& crmtNodeExpression::GetExpressionPlayer() const
{
	return m_ExpressionPlayer;
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtNodeExpression::GetWeight() const
{
	return m_Weight;
}

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodeExpression::GetFrameIndices() const
{
	return m_FrameIndices;
}

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodeExpression::GetSkelIndices() const
{
	return m_SkelIndices;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRMOTIONTREE_NODEEXPRESSION_H
