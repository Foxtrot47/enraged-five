//
// crmotiontree/nodecustom.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRMOTIONTREE_NODECUSTOM_H
#define CRMOTIONTREE_NODECUSTOM_H

#include "node.h"

namespace rage
{

// TODO --- CUSTOM NODES, PROVIDE; NOINPUT, SINGLEINPUT, MULTI[n]INPUT


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Custom node, override this to implement project specific nodes
// which are output only (have no children providing any input)
class crmtNodeCustomNoInput : public crmtNode
{
public:

	// PURPOSE: Default constructor
	crmtNodeCustomNoInput(eNodes nodeType=kNodeNone);

	// PURPOSE: Resource constructor
	crmtNodeCustomNoInput(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeCustomNoInput();

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Custom node, override to implement project specific nodes
// which are input/output (but they have exactly one child providing input)
class crmtNodeCustomSingleInput : public crmtNodeParent
{
public:

	// PURPOSE: Default constructor
	crmtNodeCustomSingleInput(eNodes nodeType=kNodeNone);

	// PURPOSE: Resource constructor
	crmtNodeCustomSingleInput(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeCustomSingleInput();

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;

};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Custom node, override to implement project specific nodes
// which are input/output (but they have exactly one child providing input)
// and which need an efficient compose callback function
class crmtNodeCustomSingleInputCompose : public crmtNodeCustomSingleInput
{
public:

	// PURPOSE: Default constructor
	crmtNodeCustomSingleInputCompose(eNodes nodeType=kNodeNone);

	// PURPOSE: Resource constructor
	crmtNodeCustomSingleInputCompose(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeCustomSingleInputCompose();
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Custom node, override to implement project specific nodes
// which are input/output (but they have exactly one child providing input)
// and which need efficient suspending compose callback functions
class crmtNodeCustomSingleInputComposeSuspend : public crmtNodeCustomSingleInput
{
public:

	// PURPOSE: Default constructor
	crmtNodeCustomSingleInputComposeSuspend(eNodes nodeType=kNodeNone);

	// PURPOSE: Resource constructor
	crmtNodeCustomSingleInputComposeSuspend(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeCustomSingleInputComposeSuspend();
};

////////////////////////////////////////////////////////////////////////////////

// TODO -- IMPLEMENT MULTI/N INPUT CUSTOM NODE (PROBABLY NOT TEMPLATED?)

} // namespace rage

#endif //CRMOTIONTREE_NODECUSTOM_H
