//
// crmotiontree/nodeextrapolate.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "nodeextrapolate.h"

#include "motiontree.h"

#include "cranimation/frame.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeExtrapolate::crmtNodeExtrapolate()
: crmtNodeParent(kNodeExtrapolate)
, m_Damping(1.f)
, m_MaxAngularVelocity(10.f * DtoR)
, m_LastFrame(NULL)
, m_DeltaFrame(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeExtrapolate::crmtNodeExtrapolate(datResource& rsc)
: crmtNodeParent(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeExtrapolate::~crmtNodeExtrapolate()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeExtrapolate, crmtNode::kNodeExtrapolate, 0);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeExtrapolate::Init(float damping)
{
	SetDamping(damping);
	Refresh();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeExtrapolate::Shutdown()
{
	ShuttingDown();

	delete m_LastFrame;
	m_LastFrame = NULL;

	delete m_DeltaFrame;
	m_DeltaFrame = NULL;

	crmtNodeParent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeExtrapolate::GetMaxNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeExtrapolate::Refresh()
{
	const crFrameData& frameData = GetMotionTree().GetFrameData();

	delete m_LastFrame;
	m_LastFrame = rage_new crFrame;
	m_LastFrame->Init(frameData);

	delete m_DeltaFrame;
	m_DeltaFrame = rage_new crFrame;
	m_DeltaFrame->Init(frameData);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeExtrapolate::SetDamping(float damping)
{
	Assert(damping <= 1.f && damping > 0.f);
	m_Damping = damping;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
