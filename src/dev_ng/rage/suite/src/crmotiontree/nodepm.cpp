//
// crmotiontree/nodepm.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodepm.h"

#include "crmtdiag.h"
#include "observer.h"

#include "crmetadata/tag.h"
#include "crparameterizedmotion/parameterizedmotion.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodePm::crmtNodePm()
: crmtNode(kNodePm)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodePm::crmtNodePm(datResource& rsc)
: crmtNode(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodePm::~crmtNodePm()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodePm, crmtNode::kNodePm, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodePm::Init(crpmParameterizedMotion* pm)
{
	SetParameterizedMotion(pm);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePm::Shutdown()
{
	ShuttingDown();
	crmtNode::Shutdown();

	SetParameterizedMotion(NULL);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePm::Update(float dt)
{
	bool hasLoopedOrEnded;
	crpmParameterizedMotionPlayer::TagTriggerFunctor tagTriggerFunctor = MakeFunctor(*this, &crmtNodePm::TagTriggerCallback);
	m_Player.Update(dt, dt, hasLoopedOrEnded, &tagTriggerFunctor);

	if(hasLoopedOrEnded)
	{
		if(m_Player.GetParameterizedMotion()->IsLooped())
		{
			MessageObservers(crmtMessage(kMsgPmLooped));
		}
		else
		{
			MessageObservers(crmtMessage(kMsgPmEnded));
		}
	}

	SetDisabled(m_Player.GetParameterizedMotion() == NULL);
	SetSilent(false);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodePm::Dump(crmtDumper& dumper) const
{
	crmtNode::Dump(dumper);

	dumper.Outputf(2, "pm", "%p", m_Player.GetParameterizedMotion());
	dumper.Outputf(2, "u", "%f", m_Player.GetParameterizedMotion()?m_Player.GetParameterizedMotion()->GetU():0.f);
	dumper.Outputf(2, "data", "'%s'", (m_Player.GetParameterizedMotion()&&m_Player.GetParameterizedMotion()->GetData())?m_Player.GetParameterizedMotion()->GetData()->GetName():"NONE");
	dumper.Outputf(2, "numparameters", "%d", m_Player.GetParameterizedMotion()?m_Player.GetParameterizedMotion()->GetNumParameterDimensions():0);
	if(m_Player.GetParameterizedMotion())
	{
		for(int i=0; i<m_Player.GetParameterizedMotion()->GetNumParameterDimensions(); ++i)
		{
			dumper.Outputf(2, "parameter", i, "%f", m_Player.GetParameterizedMotion()->GetParameter().GetValue(i));
		}
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNodePm::SetParameterizedMotion(crpmParameterizedMotion* pm)
{
	m_Player.Init(pm);
}

////////////////////////////////////////////////////////////////////////////////

const crpmParameterizedMotion* crmtNodePm::GetParameterizedMotion() const
{
	return m_Player.GetParameterizedMotion();
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotion* crmtNodePm::GetParameterizedMotion()
{
	return m_Player.GetParameterizedMotion();
}

////////////////////////////////////////////////////////////////////////////////

crmtNodePm::TagMsgPayload::TagMsgPayload(const crpmParameterizedMotion* pm, const crTag* tag)
: m_Pm(pm)
, m_Tag(tag)
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePm::TagTriggerCallback(const crTag& tag, bool enterOrExit, bool enter)
{
	crmtDebug3("Pm node tag trigger '%s' #%08x enter %d\n", tag.GetName(), u32(tag.GetKey()), enter);

	u32 msgId = enterOrExit?(enter?kMsgPmTagEnter:kMsgPmTagExit):kMsgPmTagUpdate;
	TagMsgPayload payload(m_Player.GetParameterizedMotion(), &tag);

	MessageObservers(crmtMessage(msgId, reinterpret_cast<void*>(&payload)));
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
