//
// crmotiontree/nodeclip.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODECLIP_H
#define CRMOTIONTREE_NODECLIP_H

#include "node.h"

#include "crclip/clipplayer.h"


#define USE_CLIP_COMPOSITOR_COMPOSE (1)

namespace rage
{

class crClip;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Clip playback node
class crmtNodeClip : public crmtNode
{
public:

	// PURPOSE: Default constructor
	crmtNodeClip();

	// PURPOSE: Resource constructor
	crmtNodeClip(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeClip();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeClip);

	// PURPOSE: Initialization
	// PARAMS: clip - clip to play (can be NULL).
	// time - initial clip start time (default 0.0).
	// rate - clip playback rate (can be positive, negative and zero) (default 1.0).
	void Init(const crClip* clip, float time=0.f, float rate=1.f);

	// PURPOSE: Shutdown, free/release all dynamic memory.
	virtual void Shutdown();

	// PURPOSE: Update
	virtual void Update(float dt);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh();

	// PURPOSE: Returns clip player.
	// RETURNS: Clip player (const).
	const crClipPlayer& GetClipPlayer() const;

	// PURPOSE: Returns animation player.
	// RETURNS: Clip player (non-const).
	crClipPlayer& GetClipPlayer();


	// PURPOSE: Get loop identifiers
	// RETURNS: Loop identifier bit mask
	u32 GetLoopIds() const;

	// PURPOSE: Set loop identifiers
	// PARAMS: loopIds - loop identifier bit mask
	void SetLoopIds(u32 loopIds);
	
	// PURPOSE: Get zero weight option
	// RETURNS: Bool whether participates in syncing or not
	bool IsZeroWeight() const;

	// PURPOSE: Set zero weight option
	// PARAMS: zeroWeight - participate in syncing or not
	void SetZeroWeight(bool zeroWeight);

	// PURPOSE: Enumeration of messages
	enum
	{
		kMsgClipLooped = CRMT_PACK_MSG_ID(crmtNode::kMsgEnd, kNodeClip),
		kMsgClipEnded,
		kMsgClipTagEnter,
		kMsgClipTagExit,
		kMsgClipTagUpdate,

		kMsgEnd,
	};

	// PURPOSE: Tag Enter/Exit message payload
	struct TagMsgPayload
	{
		TagMsgPayload(const crClip* clip, const crTag* tag, float phase);

		const crClip* m_Clip;
		const crTag* m_Tag;
		float m_Phase;
	};

	// PURPOSE: Internal use only, functor callback for clip tag triggers
	void TagTriggerCallback(const crTag&, bool, bool, float);

	// PURPOSE: Internal use only, functor callback for clip loop id queries
	bool LoopIdCallback(u32);

	// PURPOSE: Returns accelerator animation indices
	const crLock& GetAnimIndices() const;

	// PURPOSE: Returns accelerator expression frame indices
	const crLock& GetExprFrameIndices() const;

	// PURPOSE: Returns accelerator expression skeleton indices
	const crLock& GetExprSkelIndices() const;

private:
	crClipPlayer m_ClipPlayer;
	crClipPlayer::TagTriggerFunctor m_TagTriggerFunctor;
	crClipPlayer::LoopIdFunctor m_LoopIdFunctor;
	crLock m_AnimIndices, m_ExprFrameIndices, m_ExprSkelIndices;
	u32 m_LoopIds;
	bool m_ZeroWeight;
};

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtNodeClip::GetLoopIds() const
{
	return m_LoopIds;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNodeClip::SetLoopIds(u32 loopIds)
{
	m_LoopIds = loopIds;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNodeClip::IsZeroWeight() const
{
	return m_ZeroWeight;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNodeClip::SetZeroWeight(bool zeroWeight)
{
	m_ZeroWeight = zeroWeight;
}

////////////////////////////////////////////////////////////////////////////////

inline const crClipPlayer& crmtNodeClip::GetClipPlayer() const
{
	return m_ClipPlayer;
}

////////////////////////////////////////////////////////////////////////////////

inline crClipPlayer& crmtNodeClip::GetClipPlayer()
{
	return m_ClipPlayer;
}

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodeClip::GetAnimIndices() const
{
	return m_AnimIndices;
}

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodeClip::GetExprFrameIndices() const
{
	return m_ExprFrameIndices;
}

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodeClip::GetExprSkelIndices() const
{
	return m_ExprSkelIndices;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODECLIP_H
