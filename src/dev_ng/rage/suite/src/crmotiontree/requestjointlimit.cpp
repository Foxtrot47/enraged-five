//
// crmotiontree/nodejointlimit.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#include "requestjointlimit.h"

#include "nodejointlimit.h"

#include "cranimation/framefilters.h"
#include "crskeleton/jointdata.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestJointLimit::crmtRequestJointLimit()
: m_Filter(NULL)
, m_JointData(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestJointLimit::crmtRequestJointLimit(crmtRequest& source)
: m_Filter(NULL)
, m_JointData(NULL)
{
	Init(source);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestJointLimit::~crmtRequestJointLimit()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestJointLimit::Init(crmtRequest& source)
{
	SetSource(&source, 0);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestJointLimit::Shutdown()
{
	if (m_JointData)
	{
		m_JointData->Release();
	}
	SetFilter(NULL);

	crmtRequestSource<1>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestJointLimit::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeJointLimit* jointLimitNode = crmtNodeJointLimit::CreateNode(tree);
	FastAssert(m_JointData); // call crmtRequestJointLimit::SetJointData
	jointLimitNode->Init(*m_JointData, m_Filter);

	GenerateSourceNodes(tree, node, jointLimitNode);

	return jointLimitNode;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestJointLimit::SetJointData(const crJointData& jointData)
{
	jointData.AddRef();

	if (m_JointData)
	{
		m_JointData->Release();
	}

	m_JointData = &jointData;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestJointLimit::SetFilter(crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	if(m_Filter)
	{
		m_Filter->Release();
	}
	m_Filter = filter;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
