//
// crmotiontree/nodeanimation.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEANIMATION_H
#define CRMOTIONTREE_NODEANIMATION_H

#include "node.h"

#include "cranimation/animplayer.h"
#include "cranimation/frameaccelerator.h"

namespace rage
{

// PURPOSE: Animation playback node
class crmtNodeAnimation : public crmtNode
{
public:

	// PURPOSE: Default constructor
	crmtNodeAnimation();

	// PURPOSE: Resource constructor
	crmtNodeAnimation(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeAnimation();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeAnimation);

	// PURPOSE: Initialization
	// PARAMS: anim - animation to play (can be NULL).
	// time - initial animation start time (default 0.0).
	// rate - animation playback rate (can be positive, negative and zero) (default 1.0).
	void Init(const crAnimation* anim, float time=0.f, float rate=1.f);

	// PURPOSE: Shutdown, free/release all dynamic memory.
	virtual void Shutdown();

	// PURPOSE: Update
	virtual void Update(float dt);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh();

	// PURPOSE: Returns animation player.
	// RETURNS: Animation player (const).
	const crAnimPlayer& GetAnimPlayer() const;

	// PURPOSE: Returns animation player.
	// RETURNS: Animation player (non-const).
	crAnimPlayer& GetAnimPlayer();

	// PURPOSE: Returns indices
	const crLock& GetIndices() const;

	// PURPOSE: Enumeration of messages
	enum
	{
		kMsgAnimationLooped = CRMT_PACK_MSG_ID(crmtNode::kMsgEnd, kNodeAnimation),
		kMsgAnimationEnded,

		kMsgEnd,
	};

private:
	crAnimPlayer m_AnimPlayer;
	crLock m_Indices;
};


////////////////////////////////////////////////////////////////////////////////

inline const crAnimPlayer& crmtNodeAnimation::GetAnimPlayer() const
{
	return m_AnimPlayer;
}

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodeAnimation::GetIndices() const
{
	return m_Indices;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODEANIMATION_H
