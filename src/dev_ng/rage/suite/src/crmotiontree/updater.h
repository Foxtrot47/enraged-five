//
// crmotiontree/updater.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_UPDATER_H
#define CRMOTIONTREE_UPDATER_H

#include "motiontree_config.h"

namespace rage
{

class crmtNode;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Updates all the nodes in a tree
class crmtUpdater
{
public:

	// PURPOSE: Iterate a node
	void Iterate(crmtNode& rootNode, float dt);
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Tracks influence on a stack
class InfluenceStack
{
public:

	// PURPOSE: Constructor
	InfluenceStack();

	// PURPOSE: Constructor
	~InfluenceStack();

	// PURPOSE: Copy
	void Copy(const InfluenceStack& that);

	// PURPOSE: Push new influence onto the stack
	void PushInfluence(float);

	// PURPOSE: Pop influence off of the stack
	float PopInfluence();

	// PURPOSE: Get top influence on the stack
	float TopInfluence() const;

	// PURPOSE: Get current depth of the influence stack
	u32 GetInfluenceDepth() const;

	// PURPOSE: Push new node onto the stack, automatically pushing influence (if any)
	void PushNode(crmtNode&);

	// PURPOSE: Pop node off of the stack, automatically popping influence (if any)
	void PopNode(crmtNode&);

	// PURPOSE: Calculate the absolute influence (combine all depths)
	float CalcAbsoluteInfluence() const;

	// PURPOSE: Calculate the relative influence (between two depths)
	float CalcRelativeInfluence(u32 depthMin, u32 depthMax) const;

private:
	u32 m_Depth;
	float m_Influences[CRMT_ITERATE_BATCH];
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_UPDATER_H
