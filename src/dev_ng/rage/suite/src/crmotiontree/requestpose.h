//
// crmotiontree/requestpose.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTPOSE_H
#define CRMOTIONTREE_REQUESTPOSE_H

#include "request.h"

namespace rage
{

// PURPOSE: Pose request
// Requests a motion tree construct a pose node.
class crmtRequestPose : public crmtRequest
{
public:

	// PURPOSE: Default constructor
	crmtRequestPose();

	// PURPOSE: Destructor
	virtual ~crmtRequestPose();

	// PURPOSE: Basic initializer
	void Init();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTPOSE_H
