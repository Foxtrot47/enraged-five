//
// cranimation/crdiag.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_CRMTDIAG_H
#define CRMOTIONTREE_CRMTDIAG_H

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(crmt)

namespace rage
{

#define CRMT_DIAG_NOOUTPUT (__NO_OUTPUT)
#define CRMT_DIAG_NODEBUGOUTPUT (CRMT_DIAG_NOOUTPUT || __OPTIMIZED)

#if CRMT_DIAG_NOOUTPUT

#define CRMT_DIAG_NOOUTPUT_ONLY(x) x
#define CRMT_DIAG_NOOUTPUT_NEVER(x)

#define crmtWarning(fmt,...)
#define crmtError(fmt,...)

#else // CRMT_DIAG_NOOUTPUT

#define CRMT_DIAG_NOOUTPUT_ONLY(x)
#define CRMT_DIAG_NOOUTPUT_NEVER(x) x

#define crmtWarning(fmt,...)			RAGE_WARNINGF(crmt,fmt,##__VA_ARGS__)
#define crmtError(fmt,...)				RAGE_ERRORF(crmt,fmt,##__VA_ARGS__)

#endif // CRMT_DIAG_NOOUTPUT


#if CRMT_DIAG_NODEBUGOUTPUT

#define CRMT_DIAG_NODEBUGOUTPUT_ONLY(x) x
#define CRMT_DIAG_NODEBUGOUTPUT_NEVER(x)

#define crmtDebug1(fmt,...)
#define crmtDebug2(fmt,...)
#define crmtDebug3(fmt,...)

#else // CRMT_DIAG_NODEBUGOUTPUT

#define CRMT_DIAG_NODEBUGOUTPUT_ONLY(x)
#define CRMT_DIAG_NODEBUGOUTPUT_NEVER(x) x

#define crmtDebug1(fmt,...)			RAGE_DEBUGF1(crmt,fmt,##__VA_ARGS__)
#define crmtDebug2(fmt,...)			RAGE_DEBUGF2(crmt,fmt,##__VA_ARGS__)
#define crmtDebug3(fmt,...)			RAGE_DEBUGF3(crmt,fmt,##__VA_ARGS__)

#endif // CRMT_DIAG_NODEBUGOUTPUT


#define crmtAssert			FastAssert
#define crmtAssertf			Assertf
#define crmtAssertMsg		AssertMsg
#define crmtAssertVerify	AssertVerify
#define crmtDebugAssert		DebugAssert

} // namespace rage

#endif  //CRMOTIONTREE_CRMTDIAG_H
