//
// crmotiontree/requestcapture.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTCAPTURE_H
#define CRMOTIONTREE_REQUESTCAPTURE_H

#include "request.h"

namespace rage
{

class crFrame;


// PURPOSE: Capture request
// Requests a motion tree construct a capture node.
class crmtRequestCapture : public crmtRequestSource<1>
{
public:

	// PURPOSE: Default constructor
	crmtRequestCapture();

	// PURPOSE: Simple constructor, performs basic initialization - for capturing new nodes
	// PARAMS: frame - pointer to the frame to be used (can be NULL)
	crmtRequestCapture(crmtRequest& source, crFrame* frame);

	// PURPOSE: Simple constructor, performs basic initialization - for capturing existing nodes
	// PARAMS: frame - pointer to the frame to be used (can be NULL)
	crmtRequestCapture(crFrame* frame);

	// PURPOSE: Destructor
	virtual ~crmtRequestCapture();

	// PURPOSE: Basic initializer - for capturing new nodes
	// PARAMS: source - request that will generate new child node(s)
	// frame - pointer to the frame to be used (can be NULL)
	void Init(crmtRequest& source, crFrame* frame);

	// PURPOSE: Basic initializer - for capturing existing nodes
	// PARAMS: frame - pointer to the frame to be used (can be NULL)
	void Init(crFrame* frame);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Set frame to use
	// PARAMS: frame - pointer to frame to use (can be NULL)
	void SetFrame(crFrame* frame);

	// PURPOSE: Get frame to use
	// RETURNS: pointer to frame to use (can be NULL)
	crFrame* GetFrame() const;

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	crFrame* m_Frame;
};

} // namespace rage

#endif //CRMOTIONTREE_REQUESTCAPTURE_H
