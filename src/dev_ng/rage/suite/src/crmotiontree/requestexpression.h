//
// crmotiontree/requestexpression.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRMOTIONTREE_REQUESTEXPRESSION_H
#define CRMOTIONTREE_REQUESTEXPRESSION_H

#include "request.h"

namespace rage
{

class crExpressions;

// PURPOSE: Expression request
// Uses the expressions to manipulate degrees of freedom procedurally
class crmtRequestExpression : public crmtRequestSource<1>
{
public:

	// PURPOSE: Default constructor
	crmtRequestExpression();

	// PURPOSE: Initializing constructor - for expressions with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// expressions - const pointer to expressions, can be NULL
	crmtRequestExpression(crmtRequest& source, const crExpressions* expressions);

	// PURPOSE: Initializing constructor - for applying expressions to existing node(s)
	// expressions - const pointer to expressions, can be NULL
	crmtRequestExpression(const crExpressions* expressions);

	// PURPOSE: Destructor
	virtual ~crmtRequestExpression();

	// PURPOSE: Initializer - for expressions with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// expressions - const pointer to expressions, can be NULL
	void Init(crmtRequest& source, const crExpressions* expressions);

	// PURPOSE: Initializer - for applying expressions to existing node(s)
	// PARAMS: expressions - const pointer to expressions, can be NULL
	void Init(const crExpressions* expressions);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get the expressions
	// RETURNS: const pointer to expressions, can be NULL
	const crExpressions* GetExpressions() const;


	// PURPOSE: Set the kinematics system
	// PARAMS: expressions - const pointer to expressions, can be NULL
	void SetExpressions(const crExpressions* expressions);


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	const crExpressions* m_Expressions;
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTEXPRESSION_H 	
