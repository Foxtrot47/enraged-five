//
// crmotiontree/nodeidentity.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEIDENTITY_H
#define CRMOTIONTREE_NODEIDENTITY_H

#include "node.h"

namespace rage
{

// PURPOSE: Produces identity degrees of freedom as output
class crmtNodeIdentity : public crmtNode
{
public:

	// PURPOSE: Default constructor
	crmtNodeIdentity();

	// PURPOSE: Resource constructor
	crmtNodeIdentity(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeIdentity();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeIdentity);

	// PURPOSE: Initialization
	void Init();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

private:
};


} // namespace rage

#endif // CRMOTIONTREE_NODEIDENTITY_H
