//
// crmotiontree/nodeaddn.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEADDN_H
#define CRMOTIONTREE_NODEADDN_H

#include "nodeblendn.h"

namespace rage
{

	
////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Add N input child nodes into a single output
class crmtNodeAddN : public crmtNodeN
{
public:
	// PURPOSE: Default constructor
	crmtNodeAddN();

	// PURPOSE: Resource constructor
	crmtNodeAddN(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeAddN();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeAddN);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

private:

};

	
////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODEADDN_H
