//
// crmotiontree/synchronizer.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "synchronizer.h"

#include "node.h"
#include "nodeanimation.h"
#include "nodeclip.h"
#include "nodepm.h"

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crmetadata/tag.h"
#include "crmetadata/tags.h"
#include "crmetadata/tagiterators.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "zlib/zlib.h"

#define DEBUG_SYNC_DISPLAYF if(0) Displayf
#define DEBUG_SYNC_CLIP_NAME(stoi) ((stoi->GetNode()->GetNodeType()==crmtNode::kNodeClip && static_cast<crmtNodeClip*>(stoi->GetNode())->GetClipPlayer().GetClip())?static_cast<crmtNodeClip*>(stoi->GetNode())->GetClipPlayer().GetClip()->GetName():"NONE")
#define DEBUG_SYNC_TAG(tag) (tag?(static_cast<const crPropertyAttributeBool*>(tag->GetProperty().GetAttribute("Right"))->GetBool()?"R":"L"):"0")

#define FIX_EXTRAPOLATED_MIN_MAX_REF (1)
#define FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY (1)
#define USE_POST_SYNC_DISABLE (0)
#define USE_UPDATER_INFLUENCE (0)


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizer::crmtSynchronizer()
: m_SyncObservers(NULL)
, m_InfluenceDepth(0)
, m_AutoAddTarget(false)
, m_FirstSync(true)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizer::~crmtSynchronizer()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::Shutdown()
{
	RemoveAllTargets();

	crmtObserver::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

bool crmtSynchronizer::AddTargetByNode(crmtNode& node, float weight, bool delay, bool autoUpdateWeight, bool immutable, bool warp)
{
	SyncObserver** ppso = FindSyncObserver(node);
	SyncObserver* so;
	if(ppso)
	{
		so = *ppso;
#if !USE_UPDATER_INFLUENCE
		DEBUG_SYNC_DISPLAYF("target %p node %p weight %f + %f = %f", so, &node, so->GetWeight(), weight, (so->GetWeight()+weight));
		so->m_Weight += weight;
#endif // !USE_UPDATER_INFLUENCE
	}
	else
	{
		so = AddTargetUnsafe(node, delay, autoUpdateWeight, immutable, warp);
		DEBUG_SYNC_DISPLAYF("target %p node %p weight %f", so, &node, weight);
		so->m_Weight = weight;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtSynchronizer::RemoveTargetByNode(crmtNode& node)
{
	SyncObserver** ppso = FindSyncObserver(node);
	if(ppso)
	{
		SyncObserver* so = *ppso;
		so->Shutdown();

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::RemoveAllTargets()
{
	SyncObserver* so = m_SyncObservers;
	while(so)
	{
		SyncObserver* nso = so->m_NextSyncObserver;
		so->Shutdown();

		so = nso;
	}
	m_SyncObservers = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::ResetAllTargetWeights()
{
	SyncObserver* so = m_SyncObservers;
	while(so)
	{
		so->m_Weight = 0.f;
		so = so->m_NextSyncObserver;
	}
}

////////////////////////////////////////////////////////////////////////////////

int crmtSynchronizer::GetNumTargets() const
{
	int numTargets = 0;

	SyncObserver* so = m_SyncObservers;
	while(so)
	{
		if (so->GetNode())
		{
			++numTargets;
		}
		so = so->m_NextSyncObserver;
	}

	return numTargets;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::ReceiveMessage(const crmtMessage& msg)
{
	crmtObserver::ReceiveMessage(msg);

	switch(msg.GetMsgId())
	{
#if USE_UPDATER_INFLUENCE
	case crmtNode::kMsgPreUpdate:
		{
			crmtNode::UpdateMsgPayload* payload = reinterpret_cast<crmtNode::UpdateMsgPayload*>(msg.GetPayload());
			m_InfluenceDepth = payload->m_Influences.GetInfluenceDepth();
		}
		break;
#endif // USE_UPDATER_INFLUENCE

#if USE_UPDATER_INFLUENCE
	case crmtNode::kMsgChanged:
#else // USE_UPDATER_INFLUENCE
	case crmtNode::kMsgUpdate:
#endif // USE_UPDATER_INFLUENCE
		{
			// OPTIMIZE - switch to using crmtNode::kMsgChange message when available
			if(m_AutoAddTarget && IsAttached())
			{
				AddTargets();
			}
#if USE_UPDATER_INFLUENCE
		}
		break;

	case crmtNode::kMsgUpdate:
		{
#endif // USE_UPDATER_INFLUENCE
			UpdateMsgPayload* payload = reinterpret_cast<UpdateMsgPayload*>(msg.GetPayload());
			Synchronize(payload->m_DeltaTime);
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	crmtObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(crmtNode::kMsgUpdate);
#if USE_UPDATER_INFLUENCE
	interceptor.RegisterMessageIntercept(crmtNode::kMsgPreUpdate);
	interceptor.RegisterMessageIntercept(crmtNode::kMsgChanged);
#endif // USE_UPDATER_INFLUENCE
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizer::AddTargetIterator::AddTargetIterator(crmtSynchronizer& synchronizer)
: m_Synchronizer(&synchronizer)
{	
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizer::AddTargetIterator::~AddTargetIterator()
{
	m_Synchronizer = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::AddTargetIterator::Visit(crmtNode& node)
{
	switch(node.GetNodeType())
	{
	case crmtNode::kNodeClip:
	case crmtNode::kNodeAnimation:
	case crmtNode::kNodePm:
		m_Synchronizer->AddTargetByNode(node);
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::AddTargets()
{
	AddTargetIterator it(*this);
	it.Iterate(*GetNode());
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizer::SyncObserver::SyncObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp)
: m_Weight(1.f)
, m_ZeroWeight(false)
, m_Synchronizer(NULL)
, m_NextSyncObserver(NULL)
, m_Disabled(false)
, m_Delay(delay)
, m_AutoUpdateWeight(autoUpdateWeight)
, m_FirstSync(true)
, m_FirstSyncWarp(warp)
, m_Immutable(immutable)
{
//	AddRef();
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizer::SyncObserver::~SyncObserver()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::SyncObserver::Shutdown()
{
	Remove();

	crmtObserver::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::SyncObserver::ReceiveMessage(const crmtMessage& msg)
{
	crmtObserver::ReceiveMessage(msg);

#if __DEV && HACK_GTA4
	static const u32 s_debugPrintMsgId = 191800726u; // atStringHash("MotionTreeVisualise");
	atString* str = NULL;
#endif //__DEV && HACK_GTA4

	switch(msg.GetMsgId())
	{
#if USE_UPDATER_INFLUENCE
	case crmtNode::kMsgUpdate:
		{
			crmtNode::UpdateMsgPayload& payload = *reinterpret_cast<crmtNode::UpdateMsgPayload*>(msg.GetPayload());

			if(m_AutoUpdateWeight)
			{
				SetWeight(payload.m_Influences.CalcRelativeInfluence(m_Synchronizer->m_InfluenceDepth, payload.m_Influences.GetInfluenceDepth()));
			}
		}
		break;
#endif // !USE_UPDATER_INFLUENCE

	case crmtNode::kMsgDetach:
		Remove();
		break;

#if __DEV && HACK_GTA4
	case s_debugPrintMsgId:
		char buffer[256];
		str = (atString*)msg.GetPayload();
		sprintf(buffer, "Synchronizer: %p, weight: %.4f, zeroWeight: %s, immutable: %s, disabled: %s, delay: %s, autoUpdateWeight: %s, firstSync: %s\n"
			, m_Synchronizer, GetWeight()
			, m_ZeroWeight ? "true": "false"
			, m_Immutable ? "true" : "false"
			, m_Disabled ? "true": "false"
			, m_Delay ? "true": "false"
			, m_AutoUpdateWeight ? "true": "false"
			, m_FirstSync ? "true": "false"
			);
		*str += buffer;
		break;

#endif //__DEV && HACK_GTA4

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::SyncObserver::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	crmtObserver::ResetMessageIntercepts(interceptor);

#if USE_UPDATER_INFLUENCE
	if(m_AutoUpdateWeight)
	{
		interceptor.RegisterMessageIntercept(crmtNode::kMsgUpdate);
	}
#endif // USE_UPDATER_INFLUENCE
	interceptor.RegisterMessageIntercept(crmtNode::kMsgDetach);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtSynchronizer::SyncObserver::Dump(crmtDumper& dumper) const
{
	crmtObserver::Dump(dumper);

	dumper.Outputf(1, "synchronizer", "%p", m_Synchronizer);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

float crmtSynchronizerTag::SyncObserver::GetPhase() const
{
	if(GetNode())
	{
		switch(GetNode()->GetNodeType())
		{
		case crmtNode::kNodeClip:
			{
				const crmtNodeClip* nodeClip = static_cast<const crmtNodeClip*>(GetNode());
				const crClipPlayer& clipPlayer = nodeClip->GetClipPlayer();
				const crClip* clip = clipPlayer.GetClip();

				if(clip)
				{
					return clip->ConvertTimeToPhase(clipPlayer.GetTime());
				}
			}
			break;

		case crmtNode::kNodePm:
			{
				const crmtNodePm* nodePm = static_cast<const crmtNodePm*>(GetNode());
				const crpmParameterizedMotion* pm = nodePm->GetParameterizedMotionPlayer().GetParameterizedMotion();

				if(pm)
				{
					return pm->GetU();
				}
			}
			break;

		default:
			break;
		}
	}

	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::SyncObserver::SetPhase(float phase)
{
	if(!IsImmutable() && GetNode())
	{
		switch(GetNode()->GetNodeType())
		{
		case crmtNode::kNodeClip:
			{
				crmtNodeClip* nodeClip = static_cast<crmtNodeClip*>(GetNode());
				crClipPlayer& clipPlayer = nodeClip->GetClipPlayer();
				const crClip* clip = clipPlayer.GetClip();

				if(clip)
				{
					clipPlayer.SetTime(clip->ConvertPhaseToTime(phase), DoesFirstSyncWarp()&&IsFirstSync());
				}
			}
			break;

		case crmtNode::kNodePm:
			{
				crmtNodePm* nodePm = static_cast<crmtNodePm*>(GetNode());
				crpmParameterizedMotion* pm = nodePm->GetParameterizedMotionPlayer().GetParameterizedMotion();

				if(pm)
				{
					pm->SetU(phase);
				}
			}
			break;

		default:
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

float crmtSynchronizerTag::SyncObserver::GetDelta() const
{
	if(GetNode())
	{
		switch(GetNode()->GetNodeType())
		{
		case crmtNode::kNodeClip:
			{
				const crmtNodeClip* nodeClip = static_cast<const crmtNodeClip*>(GetNode());
				const crClipPlayer& clipPlayer = nodeClip->GetClipPlayer();
				const crClip* clip = clipPlayer.GetClip();

				if(clip)
				{
					return clip->ConvertTimeToPhase(clipPlayer.GetDelta());
				}
			}
			break;

		case crmtNode::kNodePm:
			{
				const crmtNodePm* nodePm = static_cast<const crmtNodePm*>(GetNode());
				const crpmParameterizedMotion* pm = nodePm->GetParameterizedMotionPlayer().GetParameterizedMotion();

				if(pm)
				{
					return pm->GetDeltaU();
				}
			}
			break;

		default:
			break;
		}
	}

	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::SyncObserver::SetDelta(float delta)
{
	if(!IsImmutable() && GetNode())
	{
		switch(GetNode()->GetNodeType())
		{
		case crmtNode::kNodeClip:
			{
				crmtNodeClip* nodeClip = static_cast<crmtNodeClip*>(GetNode());
				crClipPlayer& clipPlayer = nodeClip->GetClipPlayer();
				const crClip* clip = clipPlayer.GetClip();

				if(clip)
				{
					clipPlayer.SetDelta(clip->ConvertPhaseToTime(delta));
				}
			}
			break;

		case crmtNode::kNodePm:
			{
				crmtNodePm* nodePm = static_cast<crmtNodePm*>(GetNode());
				crpmParameterizedMotion* pm = nodePm->GetParameterizedMotionPlayer().GetParameterizedMotion();

				if(pm)
				{
					pm->SetDeltaU(delta);
				}
			}
			break;

		default:
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::SyncObserver::SetPhaseAndAutoDelta(float phase)
{
	crmtNode* node = GetNode();
	if(node && !IsImmutable())
	{
		if(node->GetNodeType() == crmtNode::kNodeClip)
		{
			crmtNodeClip* nodeClip = static_cast<crmtNodeClip*>(GetNode());
			crClipPlayer& clipPlayer = nodeClip->GetClipPlayer();
			bool isLooped = clipPlayer.IsLooped();
			const crClip* clip = clipPlayer.GetClip();
			if(clip)
			{
				float delta;
				float duration = clip->GetDuration();
				Assert(duration > 0.f);
				if(IsDelayed() || IsFirstSync())
				{
					delta = clipPlayer.GetDelta() / duration;
					if(!isLooped)
					{
						delta = Min(phase, delta);
					}
				}
				else
				{
					float previous = (clipPlayer.GetTime() - clipPlayer.GetDelta()) / duration;
					delta = phase - previous;

					DEBUG_SYNC_DISPLAYF("prev %f phase %f delta %f looped %d", previous, phase, delta, isLooped);

					// if looping and delta is large, might be better to scrub backwards instead
					if(isLooped)
					{
						if(delta <= -0.5f)
						{
							delta += 1.f;
						}
						else if(delta >= 0.5f)
						{
							delta -= 1.f;
						}
					}

					// TODO --- should scrub back ever be supported, or should delta be clamped to zero here

					DEBUG_SYNC_DISPLAYF("applying phase %f delta %f", phase, delta);
				}

				clipPlayer.SetTime(phase * duration, DoesFirstSyncWarp()&&IsFirstSync());
				clipPlayer.SetDelta(delta * duration);
			}
		}
		else if(node->GetNodeType() == crmtNode::kNodePm)
		{
			crmtNodePm* nodePm = static_cast<crmtNodePm*>(GetNode());
			crpmParameterizedMotion* pm = nodePm->GetParameterizedMotionPlayer().GetParameterizedMotion();
			if(pm)
			{
				float delta = pm->GetDeltaU();
				bool isLooped = pm->IsLooped();
				if(IsDelayed() || IsFirstSync())
				{
					if(!isLooped)
					{
						delta = Min(phase, delta);
					}
				}
				else
				{
					float previous = pm->GetU()-delta;
					delta = phase - previous;
					if(isLooped)
					{
						if(delta <= -0.5f)
						{
							delta += 1.f;
						}
						else if(delta >= 0.5f)
						{
							delta -= 1.f;
						}
					}
				}

				pm->SetU(phase);
				pm->SetDeltaU(delta);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crmtSynchronizerTag::SyncObserver::IsLooped() const
{
	if(GetNode())
	{
		switch(GetNode()->GetNodeType())
		{
		case crmtNode::kNodeClip:
			{
				const crmtNodeClip* nodeClip = static_cast<const crmtNodeClip*>(GetNode());
				const crClipPlayer& clipPlayer = nodeClip->GetClipPlayer();
				return clipPlayer.IsLooped();
			}
//			break;  // unreachable

		case crmtNode::kNodePm:
			{
				const crmtNodePm* nodePm = static_cast<const crmtNodePm*>(GetNode());
				const crpmParameterizedMotion* pm = nodePm->GetParameterizedMotionPlayer().GetParameterizedMotion();

				if(pm)
				{
					return pm->IsLooped();
				}
			}
			break;

		default:
			break;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

const crTags* crmtSynchronizerTag::SyncObserver::GetTags() const
{
	const crmtNode* node = GetNode();
	if(node)
	{
		switch(node->GetNodeType())
		{
		case crmtNode::kNodeClip:
			{
				const crmtNodeClip* nodeClip = static_cast<const crmtNodeClip*>(node);
				const crClipPlayer& clipPlayer = nodeClip->GetClipPlayer();
				const crClip* clip = clipPlayer.GetClip();

				if(clip)
				{
					return clip->GetTags();
				}
			}
			break;

		case crmtNode::kNodePm:
			{
				const crmtNodePm* nodePm = static_cast<const crmtNodePm*>(node);
				const crpmParameterizedMotion* pm = nodePm->GetParameterizedMotionPlayer().GetParameterizedMotion();

				if(pm)
				{
					return pm->GetTags();
				}
			}
			break;

		default:
			break;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

const crProperties* crmtSynchronizerTag::SyncObserver::GetProperties() const
{
	if(GetNode())
	{
		switch(GetNode()->GetNodeType())
		{
		case crmtNode::kNodeClip:
			{
				const crmtNodeClip* nodeClip = static_cast<const crmtNodeClip*>(GetNode());
				const crClipPlayer& clipPlayer = nodeClip->GetClipPlayer();
				const crClip* clip = clipPlayer.GetClip();

				if(clip)
				{
					return clip->GetProperties();
				}
			}
			break;

		case crmtNode::kNodePm:
			{
				const crmtNodePm* nodePm = static_cast<const crmtNodePm*>(GetNode());
				const crpmParameterizedMotion* pm = nodePm->GetParameterizedMotionPlayer().GetParameterizedMotion();

				if(pm)
				{
					return pm->GetProperties();
				}
			}
			break;

		default:
			break;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::SyncObserver::Remove()
{
	if(m_Synchronizer)
	{
		m_Synchronizer->RemoveSyncObserver(*this);
		m_Synchronizer = NULL;
	}

	m_NextSyncObserver = NULL;
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizer::Target::Target()
: m_Node(NULL)
, m_Weight(0.f)
, m_Phase(0.f)
, m_Delta(0.f)
, m_Rate(1.f)
, m_Loop(false)
, m_Delay(false)
, m_FirstSync(false)
, m_Tags(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

bool crmtSynchronizer::Target::Init(crmtNode* node)
{
	m_Node = node;

	if(node)
	{
		switch(node->GetNodeType())
		{
		case crmtNode::kNodeAnimation:
			{
				crmtNodeAnimation* nodeAnimation = static_cast<crmtNodeAnimation*>(node);
				const crAnimPlayer& ap = nodeAnimation->GetAnimPlayer();
				if(ap.GetAnimation())
				{
					const float duration = ap.GetAnimation()->GetDuration();

					m_Phase = ap.GetTime() / duration;
					m_Delta = ap.GetDelta() / duration;
					m_Rate = ap.GetRate();
					m_Loop = ap.IsLooped();
					return true;
				}
			}
			break;

		case crmtNode::kNodeClip:
			{
				crmtNodeClip* nodeClip = static_cast<crmtNodeClip*>(node);
				const crClipPlayer& cp = nodeClip->GetClipPlayer();
				if(cp.GetClip())
				{
					const float duration = cp.GetClip()->GetDuration();

					m_Phase = cp.GetTime() / duration;
					m_Delta = cp.GetDelta() / duration;
					m_Rate = cp.GetRate();
					m_Loop = cp.IsLooped();
					m_Tags = cp.GetClip()->GetTags();

					return true;
				}
			}
			break;

		case crmtNode::kNodePm:
			{
				crmtNodePm* nodePm = static_cast<crmtNodePm*>(node);
				const crpmParameterizedMotionPlayer& pmp = nodePm->GetParameterizedMotionPlayer();
				if(pmp.GetParameterizedMotion())
				{
					const crpmParameterizedMotion& pm = *pmp.GetParameterizedMotion();

					m_Phase = pm.GetU();
					m_Delta = pm.GetDeltaU();
					m_Rate = pmp.GetRate();
					m_Loop = pm.IsLooped();
					m_Tags = pm.GetTags();
					return true;
				}
			}
			break;

		default:
			break;
		}	
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::Target::Adjust(float phase, float delta) const
{
	if(m_Node)
	{
		switch(m_Node->GetNodeType())
		{
		case crmtNode::kNodeAnimation:
			{
				crmtNodeAnimation* nodeAnimation = static_cast<crmtNodeAnimation*>(m_Node);
				crAnimPlayer& ap = nodeAnimation->GetAnimPlayer();
				if(ap.GetAnimation())
				{
					const float duration = ap.GetAnimation()->GetDuration();

					ap.SetTime(phase * duration);
					ap.SetDelta(delta * duration);
				}
			}
			break;

		case crmtNode::kNodeClip:
			{
				crmtNodeClip* nodeClip = static_cast<crmtNodeClip*>(m_Node);
				crClipPlayer& cp = nodeClip->GetClipPlayer();
				if(cp.GetClip())
				{
					const float duration = cp.GetClip()->GetDuration();

					cp.SetTime(phase * duration, m_FirstSync);
					cp.SetDelta(delta * duration);
				}
			}
			break;

		case crmtNode::kNodePm:
			{
				crmtNodePm* nodePm = static_cast<crmtNodePm*>(m_Node);
				crpmParameterizedMotionPlayer& pmp = nodePm->GetParameterizedMotionPlayer();
				if(pmp.GetParameterizedMotion())
				{
					crpmParameterizedMotion& pm = *pmp.GetParameterizedMotion();

					pm.SetU(phase);

					float pmDelta = Max(delta, 0.f);
					pm.SetDeltaU(pmDelta);
				}
			}
			break;

		default:
			break;

		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::Target::AdjustPhaseAutoDelta(float phase, float deltaSum) const
{
	float delta = 0.f;

	// was this target progressing in same direction as general trend (if any)
  	if(IsNearZero(m_Delta) || IsNearZero(deltaSum) || SameSign(m_Delta, deltaSum))
	{
		float previous = m_Phase - m_Delta;
		delta = phase - previous;
		if(fabsf(delta) > 0.5f)
		{
			delta = (delta<0.f)?(1.f+delta):(delta-1.f);
		}

		// TODO - check newly calculated delta is still in same direction?
//		if(!SameSign(delta, m_Delta) && !IsNearZero(m_Delta))
	}

	Adjust(phase, delta);
}

////////////////////////////////////////////////////////////////////////////////

int crmtSynchronizer::Target::ComparePhaseCb(const void* p0, const void* p1)
{
	const Target* t0 = *reinterpret_cast<const Target*const*>(p0);
	const Target* t1 = *reinterpret_cast<const Target*const*>(p1);

	return (t0->m_Phase>t1->m_Phase)?1:((t0->m_Phase<t1->m_Phase)?-1:0);
}

////////////////////////////////////////////////////////////////////////////////

int crmtSynchronizer::InitTargets(int numTargets, Target* targets, int& numWeighted, Target** weightedTargets, float& weightSum, float& deltaSum, bool& looping)
{
	numWeighted = 0;
	weightSum = 0.f;
	deltaSum = 0.f;
	looping = false;

	int numValidTargets = 0;

	// gather information about targets from synchronizer's observer list
	crmtSynchronizer::SyncObserver* so = m_SyncObservers;
	for(int i=0; so && so->GetNode() && (i<numTargets); ++i)
	{
		Target& ti = targets[numValidTargets];

		crmtNode* node = so->GetNode();
		Assert(node);
		if(ti.Init(node))
		{
			// if weighted, target will have influence on controlling phase
			// m_Delay means the weight is ignored. It's reset next frame
			ti.m_Delay = so->m_Delay;
			ti.m_FirstSync = so->m_FirstSync;
			ti.m_Weight = !ti.m_Delay?so->m_Weight:0.0F;
			if(so->m_Weight > 0.f)
			{
				weightedTargets[numWeighted++] = &ti;
				weightSum += ti.m_Weight;
				deltaSum += ti.m_Delta * ti.m_Weight;
				looping |= ti.m_Loop;
			}

			ti.m_NumTags = 0;
			ti.m_TagPtrs = NULL;
			ti.m_TagMatched = false;

			numValidTargets++;
		}

		so = so->m_NextSyncObserver;
	}

	return numValidTargets;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizer::ResetDelayedObservers()
{
	SyncObserver** ppso = &m_SyncObservers;
	while(*ppso)
	{
		SyncObserver* so = *ppso;
		so->ClearDelay();
		so->ClearFirstSync();

		ppso = &so->m_NextSyncObserver;
	}	

}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizer::SyncObserver* crmtSynchronizer::AddTargetUnsafe(crmtNode& node, bool delay, bool autoUpdateWeight, bool immutable, bool warp)
{
	SyncObserver* so = CreateSyncObserver(delay, autoUpdateWeight, immutable, warp);
	so->AddRef();
	so->Attach(node);

	so->m_Synchronizer = this;
	so->m_NextSyncObserver = m_SyncObservers;

	m_SyncObservers = so;

	return so;
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizer::SyncObserver** crmtSynchronizer::FindSyncObserver(crmtNode& node)
{
	SyncObserver** ppso = &m_SyncObservers;
	while(*ppso)
	{
		SyncObserver* so = *ppso;
		if(so->GetNode() == &node)
		{
			return ppso;
		}

		ppso = &so->m_NextSyncObserver;
	}	

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtSynchronizer::RemoveSyncObserver(SyncObserver& so)
{
	SyncObserver** ppso = &m_SyncObservers;
	while(*ppso)
	{
		if(*ppso == &so)
		{
			*ppso = so.m_NextSyncObserver;
			so.Release();
			return true;
		}

		ppso = &((*ppso)->m_NextSyncObserver);
	}	

	return false;
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizerPhase::crmtSynchronizerPhase()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizerPhase::~crmtSynchronizerPhase()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerPhase::Shutdown()
{
	crmtSynchronizer::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

bool crmtSynchronizerPhase::AddTargetByNode(crmtNode& node, float weight, bool delay, bool autoUpdateWeight, bool immutable, bool warp)
{
	switch(node.GetNodeType())
	{
	case crmtNode::kNodeAnimation:
	case crmtNode::kNodeClip:
	case crmtNode::kNodePm:
		return crmtSynchronizer::AddTargetByNode(node, weight, delay, autoUpdateWeight, immutable, warp);

	default:
		break;
	};

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerPhase::Synchronize(float deltaTime)
{
	int numTargets = GetNumTargets();
	Target* targets = Alloca(Target, numTargets);
	Target** weightedTargets = Alloca(Target*, numTargets);

	int numWeighted;
	float weightSum, deltaSum;
	bool looping;

	numTargets = InitTargets(numTargets, targets, numWeighted, weightedTargets, weightSum, deltaSum, looping);

	// only adjust the phase, if something is controlling it
	if(numWeighted > 0)
	{		
		float phase = 0.f;

		// multiple controlling influences, more complicated
		if(numWeighted > 1)
		{
			// sort targets by phase
			qsort(weightedTargets, numWeighted, sizeof(Target*), Target::ComparePhaseCb);

			// find largest gap between phases
			int wrap = 0;
			if(looping)
			{
				float maxGap = -1.f;
				for(int i=0; i<numWeighted; ++i)
				{
					Target& ti = *weightedTargets[i];
					Target& ti1 = *weightedTargets[(i+1)%numWeighted];

					float gap = (ti1.m_Phase-ti.m_Phase);
					if(gap < 0.f)
					{
						gap += 1.f;
					}
					if(gap > maxGap)
					{
						wrap = i;
						maxGap = gap;
					}
				}
			}

			// calculate the modal influence, and phase
			int modal = ((numWeighted/2)+wrap+1)%numWeighted;
			float modalPhase = weightedTargets[modal]->m_Phase;

			// if even number of influences, modal phase will be between two central modal influences
			if(!(numWeighted&1))
			{
				int modal_1 = (modal+numWeighted-1)%numWeighted;
				modalPhase = (modalPhase + weightedTargets[modal_1]->m_Phase) * 0.5f;
				if(looping && (weightedTargets[modal_1]->m_Phase > weightedTargets[modal]->m_Phase))
				{
					modalPhase += 0.5f;
					modalPhase -= floor(modalPhase);
				}
			}

			// use modal influence as assumed arithmetic mean,
			// correct this with weighted deviations (and sensitivity to wrapping)
			float adjWeightSum = 0.f;
			for(int i=0; i<numWeighted; ++i)
			{
				Target& ti = *weightedTargets[i];

				float adj = (ti.m_Phase-modalPhase);
				if(looping && (fabs(adj) > 0.5f))
				{
					adj += (adj<0.f)?1.f:-1.f;
				}

				adjWeightSum += adj * ti.m_Weight;
			}
			
			phase = (adjWeightSum / weightSum) + modalPhase;
			if(looping)
			{
				phase -= floor(phase);
			}
			else
			{
				phase = Clamp(phase, 0.f, 1.f);
			}
		}
		else
		{
			phase = weightedTargets[0]->m_Phase;
		}

		// now adjust the targets, given the mean
		for(int i=0; i<numTargets; ++i)
		{
			const Target& ti = targets[i];
			// ti needs to know about delay
			if (Likely(!ti.m_Delay))
			{
				ti.AdjustPhaseAutoDelta(phase, deltaSum);
			}
			else
			{
				float delta = ti.m_Rate * deltaTime;
				if (!ti.m_Loop)
				{
					float diff = 1.0F - phase+delta;
					delta = Selectf(diff, delta, delta-diff);
				}
				
				ti.Adjust(phase, delta);
			}
		}
	}

	ResetDelayedObservers();
	m_FirstSync = false;
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizerTag::crmtSynchronizerTag()
: m_ForceLooping(false)
, m_Signature(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizerTag::~crmtSynchronizerTag()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::Shutdown()
{
	m_Signature = 0;

	crmtSynchronizer::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

bool crmtSynchronizerTag::AddTargetByNode(crmtNode& node, float weight, bool delay, bool autoUpdateWeight, bool immutable, bool warp)
{
	switch(node.GetNodeType())
	{
	case crmtNode::kNodeClip:
	case crmtNode::kNodePm:
		return crmtSynchronizer::AddTargetByNode(node, weight, delay, autoUpdateWeight, immutable, warp);

	default:
		break;
	};

	return false;
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizerTag::SyncTagObserver::SyncTagObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp)
: SyncObserver(delay, autoUpdateWeight, immutable, warp)
, m_MinReference(0.f)
, m_MaxReference(-1.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtSynchronizerTag::SyncTagObserver::~SyncTagObserver()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::SyncTagObserver::Shutdown()
{
	m_Keys.Reset();

	SyncObserver::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::SyncTagObserver::ReceiveMessage(const crmtMessage& msg)
{
#if !USE_POST_SYNC_DISABLE
	switch(msg.GetMsgId())
	{
	case crmtNodeClip::kMsgClipEnded:
		SetDisabled(true);
		break;

	default:
		break;
	}
#endif // !USE_POST_SYNC_DISABLE

	crmtSynchronizerTag::SyncObserver::ReceiveMessage(msg);
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::SyncTagObserver::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
#if !USE_POST_SYNC_DISABLE
	interceptor.RegisterMessageIntercept(crmtNodeClip::kMsgClipEnded);
#endif // !USE_POST_SYNC_DISABLE

	crmtSynchronizerTag::SyncObserver::ResetMessageIntercepts(interceptor);
}

////////////////////////////////////////////////////////////////////////////////

float crmtSynchronizerTag::SyncTagObserver::MapPhaseToReference(float phase) const
{
	const int numKeys = m_Keys.GetCount();
	const AlignmentKey* key0 = m_Keys.GetElements();
	for(int i=0; i<(numKeys-1); ++i, ++key0)
	{
		const AlignmentKey* key1 = key0+1;

		const float phase0 = key0->m_Phase;
		const float reference0 = key0->m_Reference;

		const float phase1 = key1->m_Phase;
		const float reference1 = key1->m_Reference;

		if(Unlikely(phase1 < phase0))
		{
			if(InRange(phase, phase0, 1.f))
			{
				float range = Range(phase, phase0, phase1+1.f);
				if(Unlikely(reference1 < reference0))
				{
					float reference = Lerp(range, reference0, reference1+GetMaxReference());
					return (reference > GetMaxReference())?(reference-GetMaxReference()):reference;
				}
				return Lerp(range, reference0, reference1);
			}
			else if(InRange(phase, 0.f, phase1))
			{
				float range = Range(phase, phase0-1.f, phase1);
				if(Unlikely(reference1 < reference0))
				{
					float reference = Lerp(range, reference0, reference1+GetMaxReference());
					return (reference > GetMaxReference())?(reference-GetMaxReference()):reference;
				}
				return Lerp(range, reference0, reference1);
			}
		}
		else if(InRange(phase, phase0, phase1))
		{
			float range = Range(phase, phase0, phase1);
			if(Unlikely(reference1 < reference0))
			{
				float reference = Lerp(range, reference0, reference1+GetMaxReference());
				return (reference > GetMaxReference())?(reference-GetMaxReference()):reference;
			}
			return Lerp(range, reference0, reference1);
		}
	}

	// unable to map
	return -1.f;
}

////////////////////////////////////////////////////////////////////////////////

float crmtSynchronizerTag::SyncTagObserver::MapReferenceToPhase(float reference) const
{
	const int numKeys = m_Keys.GetCount();
	const AlignmentKey* key0 = m_Keys.GetElements();
	for(int i=0; i<(numKeys-1); ++i, ++key0)
	{
		const AlignmentKey* key1 = key0+1;

		const float phase0 = key0->m_Phase;
		const float reference0 = key0->m_Reference;

		const float phase1 = key1->m_Phase;
		const float reference1 = key1->m_Reference;

		if(Unlikely(reference1 < reference0))
		{
			if(InRange(reference, reference0, GetMaxReference()))
			{
				float range = Range(reference, reference0, reference1+GetMaxReference());
				if(Unlikely(phase1 < phase0))
				{
					float phase = Lerp(range, phase0, phase1+1.f);
					return (phase > 1.f)?(phase-1.f):phase;
				}
				return Min(Lerp(range, phase0, phase1), 1.f);
			}
			else if(InRange(reference, 0.f, reference1))
			{
				float range = Range(reference, reference0-GetMaxReference(), reference1);
				if(Unlikely(phase1 < phase0))
				{
					float phase = Lerp(range, phase0, phase1+1.f);
					return (phase > 1.f)?(phase-1.f):phase;
				}
				return Max(Lerp(range, phase0, phase1), 0.f);
			}
		}
		else if(InRange(reference, reference0, reference1))
		{
			float range = Range(reference, reference0, reference1);
			if(Unlikely(phase1 < phase0))
			{
				float phase = Lerp(range, phase0, phase1+1.f);
				return (phase > 1.f)?(phase-1.f):phase;
			}
			return Lerp(range, phase0, phase1);
		}
	}

	// unable to map
	return -1.f;
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtSynchronizerTag::SyncTagObserver::CalcSignature() const
{
	u32 signature = 0;

	if(GetNode())
	{
		switch(GetNode()->GetNodeType())
		{
		case crmtNode::kNodeClip:
			{
				const crmtNodeClip* nodeClip = static_cast<const crmtNodeClip*>(GetNode());
				const crClipPlayer& clipPlayer = nodeClip->GetClipPlayer();
				const crClip* clip = clipPlayer.GetClip();

				signature = u32(size_t(clip));
				// TODO --- hash clip name too - or a UID from the clip (avoids relocation bugs)
				signature ^= clipPlayer.IsLooped()?1:0;
			}
			break;

		case crmtNode::kNodePm:
			{
				const crmtNodePm* nodePm = static_cast<const crmtNodePm*>(GetNode());
				const crpmParameterizedMotion* pm = nodePm->GetParameterizedMotionPlayer().GetParameterizedMotion();

				signature = crc32(signature, (const u8*)&pm, sizeof(crpmParameterizedMotion*));

				if(pm)
				{
					const crpmParameterizedMotionData* pmData = pm->GetData();

					signature = crc32(signature, (const u8*)&pmData, sizeof(crpmParameterizedMotionData*));
					// TODO --- hash pm name too - or a UID from the pm (avoids relocation bugs)
					signature ^= pm->IsLooped()?1:0;
				}
			}
			break;

		default:
			break;
		}
	}

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::Synchronize(float)
{
	// use dominant alignment method on first sync
	bool dominant = (m_Signature == 0);

	// check signature, if mismatch, [re]build alignments
	u32 signature = CalcSignature();
	DEBUG_SYNC_DISPLAYF("signature %x (old sig %x) synchronizer %p", signature, m_Signature, this);
	DEBUG_SYNC_DISPLAYF("force looping %d", m_ForceLooping);
	if(m_Signature != signature)
	{
		BuildAlignments();
		m_Signature = signature;
	}

	// apply the alignments to synchronize the targets
	ApplyAlignments(dominant);

	m_FirstSync = false;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtSynchronizerTag::InsertUniquePattern(const Target& target, int numTags, bool looping, Pattern* patterns, int numPatterns) const
{
	const crTag*const* tags = target.m_Tags;

	// check the existing patterns for a match
	bool unique = true;
	int p;
	for(p=0; p<numPatterns; ++p)
	{
		const crTag*const* patternTags = patterns[p].m_Tags;
		int itPattern = 0;

		int it = 0;
		int n=0, np=0;
		while(n<numTags && np<patterns[p].m_NumTags)
		{
			if(tags[it]->GetProperty().GetSignature() != patternTags[itPattern]->GetProperty().GetSignature())
			{
				// mismatch, unique
				break;
			}
			++n;
			++it;
			++np;
			++itPattern;
		}

		// patterns matched
		if(n==numTags && np==patterns[p].m_NumTags)
		{
			// looping states matched
			if(looping == patterns[p].m_Looping)
			{
				unique = false;
				break;
			}
		}

		while(np<patterns[p].m_NumTags)
		{
			++np;
			++itPattern;
		}
		if(patterns[p].m_NumTags > numTags)
		{
			// found insert point
			break;
		}
		else if(patterns[p].m_NumTags == numTags && !looping && patterns[p].m_Looping)
		{
			// also an insert point
			break;
		}
	}

	// if unique, then insert
	if(unique)
	{
		for(int k=numPatterns; k>p; --k)
		{
			patterns[k] = patterns[k-1];
		}

		Pattern& newPattern = patterns[p];
		newPattern.m_Tags = tags;
		newPattern.m_NumTags = numTags;
		newPattern.m_Looping = looping;

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

int crmtSynchronizerTag::FilterTags(const crTag** outTags, const crTags& tags, float startPhase) const
{
	int numTags = tags.GetNumTags();

	// calculate basis
	int tagBasis = 0;
	if(startPhase > 0.f)
	{
		for(int i=0; i < numTags; i++)
		{
			const crTag& tag = *tags.GetTag(i);
			if(tag.GetStart() >= startPhase)
			{
				tagBasis = i;
				break;
			}
		}
	}

	// filter tags
	const crProperty*const* properties = m_Properties.GetElements();
	int numProperties = m_Properties.GetCount();
	int numMatches = 0;
	int tagIndex = tagBasis;
	for(int i=0; i < numTags; i++)
	{
		const crTag* tag = tags.GetTag(tagIndex);
		const crProperty& tagProp = tag->GetProperty();
		u32 signature = tagProp.GetSignature();
		for(int j=0; j < numProperties; j++)
		{
			if(properties[j]->GetSignature() == signature)
			{
				if(Verifyf(numMatches < sm_MaxNumTags, "Too many tags found"))
				{
					outTags[numMatches++] = tag;
					break;
				}
			}
		}

		int nextTag = tagIndex+1;
		tagIndex = nextTag != numTags ? nextTag : 0;
	}

	return numMatches;
}

////////////////////////////////////////////////////////////////////////////////

int crmtSynchronizerTag::CalcAlignmentPatterns(Pattern* patterns, const Target* targets, int numTargets) const
{
	int numPatterns=0;
	for(int i=0; i < numTargets; i++)
	{
		const Target& target = targets[i];
		const crTag*const* tags = target.m_Tags;
		int numTags = target.m_NumTags;
		bool looping = target.m_Looping;

		if(numTags>0 && InsertUniquePattern(target, numTags, looping, patterns, numPatterns))
		{
			numPatterns++;
		}

		// search for most reductive pattern, if any
		if(numTags > 1 && (looping || m_ForceLooping))
		{
			for(int r=1; r<=(numTags>>1); ++r)
			{
				if(numTags%r == 0)
				{
					bool different = false;
					for(int m=1; m<(numTags/r) && !different; ++m)
					{
						int it = 0;
						int itReductive = 0;
						for(int j=0; j<r*m; ++j)
						{
							++itReductive;
						}

						for(int j=0; j<r; ++j)
						{
							if(tags[it]->GetProperty().GetSignature() != tags[itReductive]->GetProperty().GetSignature())
							{
								different = true;
								break;
							}
							++it;
							++itReductive;
						}
					}
					
					// found reductive version of the pattern, try to insert it, if unique
					if(!different)
					{
						if(InsertUniquePattern(target, r, (looping || m_ForceLooping), patterns, numPatterns))
						{
							numPatterns++;
							break;
						}
					}
				}
			}
		}
	}

	return numPatterns;
}

////////////////////////////////////////////////////////////////////////////////

int crmtSynchronizerTag::CalcPatternMatchWithOffset(Pattern& pattern, const Target& target, float patternOffset, float targetOffset, TagPair* matches) const
{
	int cycle = 0;
	int i=0, hit=0, miss=0, np=0;

	const crTag*const* patternTags = pattern.m_Tags;
	u32 numPatternTags = pattern.m_NumTags;
	u32 itPattern = 0;
	if(patternOffset > 0.f)
	{
		while(itPattern < numPatternTags && patternTags[itPattern]->GetStart() < patternOffset)
		{
			++np;
			++itPattern;
		}
	}

	const crTag* tempBuffer[sm_MaxNumTags];
	const crTag*const* targetTags = target.m_Tags;
	u32 numTargetTags = target.m_NumTags;
	if(targetOffset > 0.f)
	{
		numTargetTags = FilterTags(tempBuffer, *target.m_Observer->GetTags(), targetOffset);
		targetTags = tempBuffer;
	}

	u32 itTarget = 0;
	while(itPattern < numPatternTags && itTarget < numTargetTags)
	{
		if(patternTags[itPattern]->GetProperty().GetSignature() == targetTags[itTarget]->GetProperty().GetSignature())
		{
			TagPair& pair = matches[i++];
			pair.m_PatternTag = patternTags[itPattern];
			pair.m_TargetTag = targetTags[itTarget];
			pair.m_Cycle = cycle;

			DEBUG_SYNC_DISPLAYF("match[%d] pattern %f %s target %f %s", i-1, pair.m_PatternTag->GetStart(), DEBUG_SYNC_TAG(pair.m_PatternTag), pair.m_TargetTag->GetStart(), DEBUG_SYNC_TAG(pair.m_TargetTag));

			++hit;
			++itTarget;
			++np;
			++itPattern;
		}
		else
		{
			TagPair& pair = matches[i++];
			pair.m_PatternTag = NULL;
			pair.m_TargetTag = NULL;
			pair.m_Cycle = cycle;

			DEBUG_SYNC_DISPLAYF("mismatch[%d] skipping target %f %s", i-1, targetTags[itTarget]->GetStart(), DEBUG_SYNC_TAG(targetTags[itTarget]));

			// TODO --- not strictly true - could skip either or both
			++itTarget;
			++miss;
		}

		// if looping, and pattern exhausted, then multi-cycle, so loop pattern
		if(itTarget < numTargetTags && (itPattern == numPatternTags || np==pattern.m_NumTags) && pattern.m_Looping)
		{
			itPattern = 0;
			np = 0;
			cycle++;

			DEBUG_SYNC_DISPLAYF("looping pattern, cycle %d", cycle);
		}
	}

	while(itTarget < numTargetTags)
	{
		TagPair& pair = matches[i++];
		pair.m_PatternTag = NULL;
		pair.m_TargetTag = NULL;
		pair.m_Cycle = cycle;

		DEBUG_SYNC_DISPLAYF("overflow[%d] skipping target %f %s", i-1, targetTags[itTarget]->GetStart(), DEBUG_SYNC_TAG(targetTags[itTarget]));

		++itTarget;
		++miss;
	}

	while(itPattern < numPatternTags && np<pattern.m_NumTags)
	{
		DEBUG_SYNC_DISPLAYF("overflow skipping pattern %f %s", patternTags[itPattern]->GetStart(), DEBUG_SYNC_TAG(patternTags[itPattern]));

		++np;
		++itPattern;
		++miss;
	}

	return -miss;
}

////////////////////////////////////////////////////////////////////////////////

int crmtSynchronizerTag::CalcPatternMatch(Pattern& pattern, const Target& target, TagPair* matches) const
{
	bool targetLooping = target.m_Looping;
	int numTags = target.m_NumTags;
	int numPatternTags = pattern.m_NumTags;
	const crTag*const* tags = target.m_Tags;
	const crTag*const* patternTags = pattern.m_Tags;

	// if looping, search for suitable start point - otherwise just start from beginning of target
	bool patternLooping = pattern.m_Looping;
	DEBUG_SYNC_DISPLAYF("looping pattern %d target %d", patternLooping, targetLooping);
	if(targetLooping || patternLooping)
	{
		int np=0;
		int itPattern = 0;
		int itTarget = 0;

		while(itPattern < numPatternTags && itTarget < numTags && np<pattern.m_NumTags)
		{
			const crTag* targetTag = tags[itTarget];
			const crTag* patternTag = patternTags[itPattern];
			if(patternTag->GetProperty().GetSignature() == targetTag->GetProperty().GetSignature())
			{
				float patternOffset = 0.f;
				float targetOffset = 0.f;
				if(targetLooping)
				{
					const crTag* firstTag = targetTag;
					targetOffset = firstTag->GetStart();
				}
				else if(patternLooping)
				{
					const crTag* firstTag = patternTag;
					patternOffset = firstTag->GetStart();
				}

				DEBUG_SYNC_DISPLAYF("offsets pattern %f target %f", patternOffset, targetOffset);

				// TODO --- return best, not just first
				return CalcPatternMatchWithOffset(pattern, target, patternOffset, targetOffset, matches);
			}
			if(targetLooping)
			{
				++itTarget;
			}
			else
			{
				++itPattern;
				++np;
			}
		}
	}

	return CalcPatternMatchWithOffset(pattern, target, 0.f, 0.f, matches);
}

////////////////////////////////////////////////////////////////////////////////

bool crmtSynchronizerTag::CalcExtrapolatedMinMaxReference(TagPair* matches, int numTags, int lowestTagIdx, bool looping, float& minRef, float& maxRef, ExtrapolatedAlignmentKeyArray& startKeys, ExtrapolatedAlignmentKeyArray& endKeys)
{
	TagPair* pair0 = &matches[lowestTagIdx];
	TagPair* pair1 = NULL;

	int pair0idx = lowestTagIdx;
	int pair1idx = -1;

	// search for high tags
	for(int t=1; t<numTags; ++t)
	{
		int tagIdx = (t+lowestTagIdx) % numTags;
		TagPair& pair = matches[tagIdx];
		if(pair.m_PatternTag && pair.m_TargetTag)
		{
			pair1 = &pair;
#if !FIX_EXTRAPOLATED_MIN_MAX_REF
			pair1idx = tagIdx;
#else // !FIX_EXTRAPOLATED_MIN_MAX_REF
			pair1idx = t;
#endif // !FIX_EXTRAPOLATED_MIN_MAX_REF
		}
	}

	// extrapolate between tags
	if(pair0 && pair1)
	{
		float diff = fabsf(pair1->m_PatternTag->GetStart()+float(pair1->m_Cycle) - pair0->m_PatternTag->GetStart());
		if(diff > 0.001f && (pair1->m_TargetTag->GetStart() - pair0->m_TargetTag->GetStart()) > 0.001f)
		{
			// if looping pattern, try and extrapolate whole cycles
			if(looping && (diff+SMALL_FLOAT) >= 1.f)
			{
				TagPair* pair0cycle = NULL;
				TagPair* pair1cycle = NULL;

				int pair0cycleidx = -1;
				int pair1cycleidx = -1;

				for(int t=1; t<=pair1idx; ++t)
				{
					int tagIdx = (t+lowestTagIdx) % numTags;
					TagPair& pair = matches[tagIdx];
					if(pair.m_PatternTag && pair.m_TargetTag)
					{
						if(pair.m_PatternTag == pair0->m_PatternTag && pair.m_Cycle == (pair0->m_Cycle+1))
						{
							pair0cycle = &pair;
#if !FIX_EXTRAPOLATED_MIN_MAX_REF
							pair0cycleidx = tagIdx;
#else // !FIX_EXTRAPOLATED_MIN_MAX_REF
							pair0cycleidx = t; 
#endif // !FIX_EXTRAPOLATED_MIN_MAX_REF
							break;
						}
					}
				}
				for(int t=(pair1idx-1); t>=0; --t)
				{
					int tagIdx = (t+lowestTagIdx) % numTags;
					TagPair& pair = matches[tagIdx];
					if(pair.m_PatternTag && pair.m_TargetTag)
					{
						if(pair.m_PatternTag == pair1->m_PatternTag && pair.m_Cycle == (pair1->m_Cycle-1))
						{
							pair1cycle = &pair;
#if !FIX_EXTRAPOLATED_MIN_MAX_REF
							pair1cycleidx = tagIdx;
#else // !FIX_EXTRAPOLATED_MIN_MAX_REF
							pair1cycleidx = t; 
#endif // !FIX_EXTRAPOLATED_MIN_MAX_REF
							break;
						}
					}
				}
					
				if(pair0cycle && pair1cycle)
				{
					float phaselow = pair0->m_TargetTag->GetStart();
					int idxlow = pair0idx;
					int biaslow = 0;

#if FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY
					bool success = true;
					const int maxLoops = 20;
					int numLoops = maxLoops;
#endif // FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY
					do
					{
						int lastidx = idxlow;
						if(idxlow == pair0idx)
						{
							lastidx = pair0cycleidx;
							biaslow--;
						}
						idxlow = lastidx-1;
#if !FIX_EXTRAPOLATED_MIN_MAX_REF
						while (!matches[idxlow].m_PatternTag || !matches[idxlow].m_TargetTag)
#else // !FIX_EXTRAPOLATED_MIN_MAX_REF
						while (!matches[(idxlow+lowestTagIdx)%numTags].m_PatternTag || !matches[(idxlow+lowestTagIdx)%numTags].m_TargetTag)
#endif // !FIX_EXTRAPOLATED_MIN_MAX_REF
						{
							idxlow--;
						} 
						
#if !FIX_EXTRAPOLATED_MIN_MAX_REF
						TagPair& lastpair = matches[lastidx];
						TagPair& pair = matches[idxlow];
#else // !FIX_EXTRAPOLATED_MIN_MAX_REF
						TagPair& lastpair = matches[(lastidx+lowestTagIdx)%numTags];
						TagPair& pair = matches[(idxlow+lowestTagIdx)%numTags];
#endif // !FIX_EXTRAPOLATED_MIN_MAX_REF

						float phasediff = lastpair.m_TargetTag->GetStart() - pair.m_TargetTag->GetStart();
						if(phasediff < 0.f)
						{
							phasediff += 1.f;
						}
						phaselow -= phasediff;

						if(phaselow < 0.f)
						{
							float rangelow = Range(0.f, phaselow, phaselow + phasediff);
							minRef = Lerp(rangelow, pair.m_PatternTag->GetStart()+pair.m_Cycle+biaslow, lastpair.m_PatternTag->GetStart()+lastpair.m_Cycle+biaslow);
							break;
						}
						else if(startKeys.GetCount() < startKeys.GetMaxCount())
						{
							SyncTagObserver::AlignmentKey& key = startKeys.Append();
							key.m_Phase = phaselow;
							key.m_Reference = pair.m_PatternTag->GetStart() + float(pair.m_Cycle+biaslow);
						}
					}
#if !FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY
					while(phaselow >= 0.f);
#else // !FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY
					while(phaselow >= 0.f && --numLoops > 0);
					success &= (numLoops > 0);
#endif // !FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY
					
					float phasehigh = pair1->m_TargetTag->GetStart();
					int idxhigh = pair1idx;
					int biashigh = 0;

#if FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY
					numLoops = maxLoops;
#endif // FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY
					do 
					{
						int lastidx = idxhigh;
						if(idxhigh == pair1idx)
						{
							lastidx = pair1cycleidx;
							biashigh++;
						}						
						idxhigh = lastidx+1;
#if !FIX_EXTRAPOLATED_MIN_MAX_REF
						while (!matches[idxhigh].m_PatternTag || !matches[idxhigh].m_TargetTag)
#else // !FIX_EXTRAPOLATED_MIN_MAX_REF
						while (!matches[(idxhigh+lowestTagIdx)%numTags].m_PatternTag || !matches[(idxhigh+lowestTagIdx)%numTags].m_TargetTag)
#endif // !FIX_EXTRAPOLATED_MIN_MAX_REF
						{
							idxhigh++;
						} 

#if !FIX_EXTRAPOLATED_MIN_MAX_REF
						TagPair& lastpair = matches[lastidx];
						TagPair& pair = matches[idxhigh];
#else // !FIX_EXTRAPOLATED_MIN_MAX_REF
						TagPair& lastpair = matches[(lastidx+lowestTagIdx)%numTags];
						TagPair& pair = matches[(idxhigh+lowestTagIdx)%numTags];
#endif // !FIX_EXTRAPOLATED_MIN_MAX_REF

						float phasediff = pair.m_TargetTag->GetStart() - lastpair.m_TargetTag->GetStart();
						if(phasediff < 0.f)
						{
							phasediff += 1.f;
						}
						phasehigh += phasediff;

						if(phasehigh > 1.f)
						{
							float rangehigh = Range(1.f, phasehigh-phasediff, phasehigh);
							maxRef = Lerp(rangehigh, lastpair.m_PatternTag->GetStart()+lastpair.m_Cycle+biashigh, pair.m_PatternTag->GetStart()+pair.m_Cycle+biashigh);
							break;
						}
						else if(endKeys.GetCount() < endKeys.GetMaxCount())
						{
							SyncTagObserver::AlignmentKey& key = endKeys.Append();
							key.m_Phase = phaselow;
							key.m_Reference = pair.m_PatternTag->GetStart() + float(pair.m_Cycle+biashigh);
						}
					} 
#if !FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY
					while(phasehigh <= 1.f);

					return true;
#else // !FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY
					while(phasehigh <= 1.f && --numLoops > 0);
					success &= (numLoops > 0);

					if(success)
					{
						return true;
					}
					else
					{
						startKeys.Reset();
						endKeys.Reset();
					}
#endif // !FIX_EXTRAPOLATED_MIN_MAX_REF_SANITY

				}
			}


			// fall back to extrapolating entire range
			float range0 = Range(0.f, pair0->m_TargetTag->GetStart(), pair1->m_TargetTag->GetStart());
			float range1 = Range(1.f, pair0->m_TargetTag->GetStart(), pair1->m_TargetTag->GetStart());

			minRef = Lerp(range0, pair0->m_PatternTag->GetStart(), pair0->m_PatternTag->GetStart()+diff);
			maxRef = Lerp(range1, pair0->m_PatternTag->GetStart(), pair0->m_PatternTag->GetStart()+diff);

			return true;
		}
	}

	// insufficient data to calculate a plausible estimate for extrapolated start/end reference values
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template <u32 bufferSize>
class AllocHelper
{
public:

	AllocHelper()
		: m_BufferPtr(m_Buffer)
		, m_DynamicHead(NULL)
	{
		size_t alignedPtr = (reinterpret_cast<size_t>(m_BufferPtr)+15)&~15;
		m_BufferPtr = reinterpret_cast<u8*>(alignedPtr);
	}

	~AllocHelper()
	{
		u8* head = m_DynamicHead;
		while(head)
		{
			u8* del = head;
			head = *reinterpret_cast<u8**>(head);

			delete [] del;
		}
	}

	u8* Alloc(u32 size)
	{
		size = (size+15)&~15;
		
		u32 availableSize = u32(bufferSize - (m_BufferPtr - m_Buffer));
		if(availableSize >= size)
		{
			u8* ptr = m_BufferPtr;
			m_BufferPtr += size;
			return ptr;
		}

		Warningf("AllocHelper::Alloc - fallback on dynamic, buffer size %d, available %d, requested %d", bufferSize, availableSize, size);

		u8* newBuffer = rage_aligned_new(16) u8[size+16];
		*reinterpret_cast<u8**>(newBuffer) = m_DynamicHead;
		m_DynamicHead = newBuffer;

		return (newBuffer+16);
	}

	u8* Alloc(u32 size, u32 count)
	{
		return Alloc(size*count);
	}

	template<typename _T>
	_T* AllocType(u32 count)
	{
		return (_T*)Alloc(sizeof(_T), count);
	}

private:
	u8* m_DynamicHead;
	u8* m_BufferPtr;
	u8 m_Buffer[bufferSize];
};

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::BuildAlignments()
{
	AllocHelper<24*1024> ah;

	// count targets
	int numTargets=0;
	for(SyncObserver* so = m_SyncObservers; so; so = so->m_NextSyncObserver)
	{
		if (!so->IsDisabled() && so->GetTags())
		{
			++numTargets;
		}
	}
	Target* targets = ah.AllocType<Target>(numTargets);

	// create targets
	int totalCount=0;
	int createdTargets=0;
	for(SyncObserver* so = m_SyncObservers; so; so = so->m_NextSyncObserver)
	{
		const crTags* tags = so->GetTags();
		if(!so->IsDisabled() && tags)
		{
			Target& target = targets[createdTargets++];
			const crTag** filterTags = target.m_Tags;
			u32 numTags = FilterTags(filterTags, *tags);
			target.m_Observer = so;
			target.m_NumTags = numTags;
			target.m_Looping = so->IsLooped();
			totalCount += numTags;
		}
	}
	DEBUG_SYNC_DISPLAYF("rebuilding alignments - %d targets", numTargets);

	// find unique tag patterns, sorted from least -> most complex
	Pattern* patterns = ah.AllocType<Pattern>(numTargets*2);
	int numPatterns = CalcAlignmentPatterns(patterns, targets, numTargets);
	Assert(numPatterns<=(numTargets*2));

	DEBUG_SYNC_DISPLAYF("%d patterns", numPatterns);

	// for each tag pattern, try matching against tags in other clips, score results, find best
	TagPair* matchBuffers[2];
	matchBuffers[0] = ah.AllocType<TagPair>(totalCount);
	matchBuffers[1] = ah.AllocType<TagPair>(totalCount);

	int bestScore = INT_MIN;
	int bestPattern = -1;
	int bestMatchesIdx = 0;

	for(int p=0; p<numPatterns; ++p)
	{
		int score = 0;
		int freeIdx = (bestMatchesIdx+1)%2;
		TagPair* matches = matchBuffers[freeIdx];

		DEBUG_SYNC_DISPLAYF("pattern[%d]", p);

		for(int i=0; i < numTargets; ++i)
		{
			const Target& target = targets[i];
			DEBUG_SYNC_DISPLAYF("pattern[%d] vs target[%d] %s", p, i, DEBUG_SYNC_CLIP_NAME(target.m_Observer));
			score += CalcPatternMatch(patterns[p], target, matches);
			matches += target.m_NumTags;
		}

		DEBUG_SYNC_DISPLAYF("pattern[%d] score %d", p, score);

		// found better overall match, store results
		if(score > bestScore)
		{
			bestScore = score;
			bestPattern = p;
			bestMatchesIdx = freeIdx;
		}
	}

	// make alignments from best matches
	if(bestPattern >= 0)
	{
		DEBUG_SYNC_DISPLAYF("found best pattern %d score %d", bestPattern, bestScore);

		TagPair* matches = matchBuffers[bestMatchesIdx];

		for(int i=0; i < numTargets; ++i)
		{
			const Target& target = targets[i];
			crmtSynchronizerTag::SyncTagObserver* stoi = static_cast<crmtSynchronizerTag::SyncTagObserver*>(target.m_Observer);
			int count = target.m_NumTags;

			stoi->m_Keys.Reset();
			stoi->m_MinReference = 0.f;
			stoi->m_MaxReference = 1.f;

			DEBUG_SYNC_DISPLAYF("target[%d] %s", i, DEBUG_SYNC_CLIP_NAME(stoi));

			// find tag with lowest phase
			int numMatches = 0;
			int lowestTagIdx = 0;
			float lowestPhase = FLT_MAX;
			float highestPhase = -FLT_MAX;
			float lowestReference = FLT_MAX;
			float highestReference = -FLT_MAX;
			int maxCycle = 0;
			for(int t=0; t<count; ++t)
			{
				TagPair& pair = matches[t];
				const crTag* patternTag = pair.m_PatternTag;
				const crTag* targetTag = pair.m_TargetTag;
				if(patternTag && targetTag)
				{
					numMatches++;
					if(targetTag->GetStart() < lowestPhase)
					{
						lowestTagIdx = t;
						lowestPhase = targetTag->GetStart();
					}
					highestPhase = Max(targetTag->GetStart(), highestPhase);
					maxCycle = Max(maxCycle, pair.m_Cycle);

					lowestReference = Min(patternTag->GetStart()+float(pair.m_Cycle), lowestReference);
					highestReference = Max(patternTag->GetStart()+float(pair.m_Cycle), highestReference);
				}
			}

			DEBUG_SYNC_DISPLAYF("target[%d] num matches %d lowest tag idx %d max cycle %d", i, numMatches, lowestTagIdx, maxCycle);

			if(numMatches > 0)
			{
				bool looping = stoi->IsLooped();
				bool patternLooping = patterns[bestPattern].m_Looping;
				bool bothLooping = looping && patternLooping;
				int boost = 0;

				// resize keys buffer
				int numKeys = numMatches;
				if(looping)
				{
					// add extra key for looping (for duplicate loop key)
					numKeys++;
				}
				else
				{
					// add two extra keys for non-looping (for start/end keys)
					numKeys += 2;
				}
				stoi->m_Keys.Reserve(numKeys);

				// calculate minimum/maximum reference value
				stoi->m_MinReference = 0.f;
				stoi->m_MaxReference = float(maxCycle+1);

				bool extrapolatedRef = false;
				ExtrapolatedAlignmentKeyArray extrapolatedStartKeys;
				ExtrapolatedAlignmentKeyArray extrapolatedEndKeys;
				if(!looping)
				{
					float minExtrapolatedRef, maxExtrapolatedRef;
					extrapolatedRef = CalcExtrapolatedMinMaxReference(matches, count, lowestTagIdx, patternLooping, minExtrapolatedRef, maxExtrapolatedRef, extrapolatedStartKeys, extrapolatedEndKeys);
					if(extrapolatedRef)
					{
						// clamp extrapolated reference values if non-looping
						if(!patternLooping)
						{
							minExtrapolatedRef = Max(minExtrapolatedRef, stoi->m_MinReference);
							maxExtrapolatedRef = Min(maxExtrapolatedRef, stoi->m_MaxReference);
						}
						DEBUG_SYNC_DISPLAYF("target[%d] extrapolated reference min %f max %f", i, minExtrapolatedRef, maxExtrapolatedRef);
						if(lowestPhase > 0.f)
						{
							stoi->m_MinReference = minExtrapolatedRef;
							boost = (stoi->m_MinReference<0.f)?int(-floorf(stoi->m_MinReference)):0;
						}
						else
						{
							stoi->m_MinReference = lowestReference;
						}
						if(highestPhase < 1.f)
						{
							stoi->m_MaxReference = maxExtrapolatedRef;
						}
						else
						{
							stoi->m_MaxReference = highestReference;
						}

						stoi->m_MinReference += float(boost);
						stoi->m_MaxReference += float(boost);
					}
				}

				DEBUG_SYNC_DISPLAYF("target[%d] min ref %f max ref %f", i, stoi->m_MinReference, stoi->m_MaxReference);
				DEBUG_SYNC_DISPLAYF("target[%d] boost %d", i, boost);

				// add a starting key
				if(!looping && lowestPhase > 0.f)
				{
					if(extrapolatedRef || bothLooping)
					{
						// add initial extrapolated starting key
						SyncTagObserver::AlignmentKey& key = stoi->m_Keys.Grow();
						key.m_Phase = 0.f;
						key.m_Reference = stoi->m_MinReference;
	
						DEBUG_SYNC_DISPLAYF("target[%d] adding starting key, ref %f", i, key.m_Reference);
					}

					// add any extrapolated start keys
					for(int n=extrapolatedStartKeys.GetCount()-1; n>=0; --n)
					{
						SyncTagObserver::AlignmentKey& key = stoi->m_Keys.Grow();
						key = extrapolatedStartKeys[n];
						key.m_Reference += float(boost);

						DEBUG_SYNC_DISPLAYF("target[%d] adding extrapolated start key, phase %f ref %f", i, key.m_Phase, key.m_Reference);
					}
				}
				
				// loop through tags in phase order
				for(int t=0; t<count; ++t)
				{
					int tagIdx = (t+lowestTagIdx) % count;
					TagPair& pair = matches[tagIdx];
					const crTag* patternTag = pair.m_PatternTag;
					const crTag* targetTag = pair.m_TargetTag;
					if(patternTag && targetTag)
					{
						SyncTagObserver::AlignmentKey& key = stoi->m_Keys.Grow();
						key.m_Phase = targetTag->GetStart();
						key.m_Reference = patternTag->GetStart() + float(pair.m_Cycle + boost);

						DEBUG_SYNC_DISPLAYF("target[%d] key[%d] tag idx %d phase %f ref %f t %s p %s", i, stoi->m_Keys.GetCount()-1, t, key.m_Phase, key.m_Reference, DEBUG_SYNC_TAG(targetTag), DEBUG_SYNC_TAG(patternTag));
					}
				}

				// duplicate first/last key if looping
				if(looping)
				{
					stoi->m_Keys.Grow() = stoi->m_Keys[0];
					DEBUG_SYNC_DISPLAYF("target[%d] adding duplicate looping end key", i);
				}
				
				// add an ending key
				if(!looping && highestPhase < 1.f)
				{
					if(extrapolatedRef || bothLooping)
					{
						// add any extrapolated end keys
						for(int n=0; n<extrapolatedEndKeys.GetCount(); ++n)
						{
							SyncTagObserver::AlignmentKey& key = stoi->m_Keys.Grow();
							key = extrapolatedEndKeys[n];
							key.m_Reference += float(boost);

							DEBUG_SYNC_DISPLAYF("target[%d] adding extrapolated end key, phase %f ref %f", i, key.m_Phase, key.m_Reference);
						}

						// add final extrapolated end key
						SyncTagObserver::AlignmentKey& key = stoi->m_Keys.Grow();
						key.m_Phase = 1.f;
						key.m_Reference = stoi->m_MaxReference;

						DEBUG_SYNC_DISPLAYF("target[%d] adding ending key, ref %f", i, key.m_Reference);
					}
				}
			}

			matches += count;
		}
	}
	else
	{
		crmtSynchronizerTag::SyncTagObserver* stoi = static_cast<crmtSynchronizerTag::SyncTagObserver*>(m_SyncObservers);
		for(int i=0; stoi; ++i)
		{
			stoi->m_Keys.Reset();
			stoi->m_MinReference = 0.f;
			stoi->m_MaxReference = 1.f;

			stoi = static_cast<crmtSynchronizerTag::SyncTagObserver*>(stoi->m_NextSyncObserver);
		}
	}

	static const crProperty::Key synchronizerKey = crProperties::CalcKey("Synchronizer");
	static const crProperty::Key zeroWeightKey = crProperties::CalcKey("ZeroWeight");

	// test for synchronization zero weight property
	crmtSynchronizerTag::SyncTagObserver* stoi = static_cast<crmtSynchronizerTag::SyncTagObserver*>(m_SyncObservers);
	for(int i=0; stoi; ++i)
	{
		stoi->m_ZeroWeight = false;

		const crProperties* properties = stoi->GetProperties();
		if(properties)		
		{
			const crProperty* prop = properties->FindProperty(synchronizerKey);
			if(prop)
			{
				crPropertyAttributeAccessor<crPropertyAttributeBool, bool> accessor(prop->GetAttribute(zeroWeightKey));
				accessor.Get(stoi->m_ZeroWeight);
			}
		}

		stoi = static_cast<crmtSynchronizerTag::SyncTagObserver*>(stoi->m_NextSyncObserver);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtSynchronizerTag::ApplyAlignments(bool dominant)
{
	const int numTargets = GetNumTargets();
	int numWeighted = 0;

	crmtSynchronizerTag::SyncTagObserver** observers = Alloca(crmtSynchronizerTag::SyncTagObserver*, numTargets);
	float* references = Alloca(float, numTargets);
	float* basis = Alloca(float, numTargets);

	crmtSynchronizerTag::SyncTagObserver*const sto = static_cast<crmtSynchronizerTag::SyncTagObserver*>(m_SyncObservers);

	// gather phases, convert to common references
	crmtSynchronizerTag::SyncTagObserver* stoi = sto;
	for(int i=0; stoi; ++i)
	{
		references[i] = -1.f;
		basis[i] = 0.f;

		observers[i] = stoi;

		if(!stoi->IsDisabled())
		{
			float pi = stoi->GetPhase();
			float ri = stoi->MapPhaseToReference(pi);
			float wi = stoi->GetWeight();
			bool delay = stoi->IsDelayed();

			if(!delay)
			{
				if(ri >= 1.f)
				{
					// place values into zero to one range, store basis
					basis[i] = floorf(ri);
					references[i] = ri - basis[i];
				}
				else if(ri >= 0.f)
				{
					references[i] = ri;
				}

				if(wi>0.f)
				{
					numWeighted++;
				}
			}

			DEBUG_SYNC_DISPLAYF("INPUT[%d] node %p phase %f ref %f basis %f weight %f loop %d delay %d first %d immutable %d", i, stoi->GetNode(), pi, references[i], basis[i], stoi->GetWeight(), stoi->IsLooped(), stoi->IsDelayed(), stoi->IsFirstSync(), stoi->IsImmutable());
		}

		stoi = static_cast<crmtSynchronizerTag::SyncTagObserver*>(stoi->m_NextSyncObserver);
	}

	DEBUG_SYNC_DISPLAYF("num weighted %d dominant %d", numWeighted, dominant);

	// order by weight
	int* order = Alloca(int, numTargets);
	for(int i=0; i<numTargets; ++i)
	{
		order[i] = i;
	}
	for(int j=0; j<numWeighted; ++j)
	{
		bool ordered = true;
		for(int i=(numTargets-1); i>j; --i)
		{
			crmtSynchronizerTag::SyncTagObserver* sto0 = observers[order[i-1]];
			crmtSynchronizerTag::SyncTagObserver* sto1 = observers[order[i]];

			if(sto1->GetWeight() > sto0->GetWeight() && !sto1->IsDelayed() && !sto1->IsDisabled())
			{
				SwapEm(order[i-1], order[i]);
				ordered = false;
			}
		}
		if(ordered)
		{
			break;
		}
	}

	// perform weighted average to calculate new common reference
	float referenceWeight = 0.f;
	float referenceWeightSum = 0.f;
	for(int n=0; n<numTargets; ++n)
	{
		int i = order[n];
		crmtSynchronizerTag::SyncTagObserver* observer = observers[i];

		if(references[i] >= 0.f)
		{
			float ri = references[i];

			// for a weighted average to work, some values may need to be unwrapped (adjust basis)
			if(referenceWeightSum > 0.f)
			{
				float reference = referenceWeight / referenceWeightSum;
				if((reference-ri) > 0.5f)
				{
					if(observer->IsLooped() || basis[i] > observer->GetMinReference())  // TODO -- needed? should non-looped just do this anyway?
					{
						ri += 1.f;
						basis[i] -= 1.f;
					}
				}
				else if((ri-reference) > 0.5f)
				{
					if(observer->IsLooped() || basis[i] < (observer->GetMaxReference()-1.f))  // TODO -- needed? should non-looped just do this anyway?
					{
						ri -= 1.f;
						basis[i] += 1.f;
					}
				}
			}

			float wi = observer->GetWeight();
			bool delayed = observer->IsDelayed();
			if(wi > 0.f && !delayed && (!dominant || referenceWeightSum == 0.f))
			{
				referenceWeight += ri * wi;
				referenceWeightSum += wi;
			}
		}
	}

	// apply new common reference
	if(referenceWeightSum > 0.f)
	{
		float reference = referenceWeight / referenceWeightSum;
		DEBUG_SYNC_DISPLAYF("reference %f", reference);

		// convert common reference back into phases and apply to targets
		stoi = sto;
		for(int i=0; stoi; ++i)
		{
			// only set phase if able to convert existing phase to reference, or if target just added
			if(references[i] >= 0.f || (stoi->IsDelayed() && !stoi->IsDisabled()))
			{
				// calculate reference for this target
				float ri = reference + basis[i];

				// special case, non looped, first sync targets whose reference is too early bump to next cycle
				if(stoi->IsFirstSync() && !stoi->IsLooped() && (stoi->GetWeight() == 0.f || !m_FirstSync) && ri < stoi->GetMinReference())
				{
					if((ri + 1.f) < stoi->GetMaxReference())
					{
						ri += 1.f;
					}
				}

				if(ri > stoi->GetMaxReference())
				{
					ri = stoi->IsLooped()?(ri-stoi->GetMaxReference()):stoi->GetMaxReference();
				}
				else if(ri < stoi->GetMinReference())
				{
					ri = stoi->IsLooped()?(ri+stoi->GetMaxReference()):stoi->GetMinReference();
				}

				// map back to phase, and apply to target
				float pi = stoi->MapReferenceToPhase(ri);

				DEBUG_SYNC_DISPLAYF("OUTPUT[%d] ref %f (%f + %f) phase %f", i, ri, reference, basis[i], pi);

#if __DEV
				float OLD_PHASE = stoi->GetPhase();
#endif // __DEV


				if(pi >= 0.f)
				{
					stoi->SetPhaseAndAutoDelta(pi);
				}

#if __DEV
				float diff = fabsf(OLD_PHASE-pi);
				if(diff > 0.5f)
					diff = 1.f - diff;
				DEBUG_SYNC_DISPLAYF("PHASE %f -> %f (%f attempted) delta %f delay %d first %d immutable %d %s", OLD_PHASE, stoi->GetPhase(), pi, stoi->GetDelta(), stoi->IsDelayed(), stoi->IsFirstSync(), stoi->IsImmutable(), (!stoi->IsDelayed() && diff>0.2f && stoi->GetWeight() > 0.f)?"*** LARGE PHASE CHANGE ***":"");
#endif // __DEV
			}

#if USE_POST_SYNC_DISABLE
			if(!stoi->IsDisabled() && stoi->GetPhase() == 1.f && !stoi->IsLooped())
			{
				stoi->SetDisabled(true);
				DEBUG_SYNC_DISPLAYF("DISABLING[%d]", i);
			}
#endif // USE_POST_SYNC_DISABLE

			stoi = static_cast<crmtSynchronizerTag::SyncTagObserver*>(stoi->m_NextSyncObserver);
		}
	}

	// clear delay and first statuses
	stoi = sto;
	for(int i=0; stoi; ++i)
	{
		stoi->ClearDelay();
		stoi->ClearFirstSync();

		stoi = static_cast<crmtSynchronizerTag::SyncTagObserver*>(stoi->m_NextSyncObserver);
	}
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtSynchronizerTag::CalcSignature()
{
	u32 signature = 0;

	crmtSynchronizerTag::SyncTagObserver* sto = static_cast<crmtSynchronizerTag::SyncTagObserver*>(m_SyncObservers);
	while(sto)
	{
		if(!sto->IsDisabled())
		{
			signature ^= sto->CalcSignature();
			signature = (signature<<7)|(signature>>25);
		}
		sto = static_cast<crmtSynchronizerTag::SyncTagObserver*>(sto->m_NextSyncObserver);
	}

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
