//
// crmotiontree/managersinglestate.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_MANAGERSINGLESTATE_H
#define CRMOTIONTREE_MANAGERSINGLESTATE_H

#include "manager.h"

namespace rage
{

class crmtObserver;
class crmtRequest;
class crmtRequestBlend;


// PURPOSE: Simple single state manager
// This is the simplest type of manager, it deals with
// tracking a single current state on the motion tree,
// and cross fading this with new states.
class crmtManagerSingleState : public crmtManager
{
public:

	// PURPOSE: Default constructor
	crmtManagerSingleState();

	// PURPOSE: Helpful constructor that allows provision of motion tree
	// SEE ALSO: Init()
	crmtManagerSingleState(crmtMotionTree& tree, const crmtObserver* location=NULL);

	// PURPOSE: Destructor
	virtual ~crmtManagerSingleState();

	// PURPOSE: Resource constructor
	crmtManagerSingleState(datResource&);

	DECLARE_PLACE(crmtManagerSingleState);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initialization
	// PARAMS: tree - motion tree this manager is going to manage
	// location - identifies root node, if manager is not controlling entire tree
	virtual void Init(crmtMotionTree& tree, const crmtObserver* location=NULL);

	// PURPOSE: Shutdown the manager
	virtual void Shutdown();

	// PURPOSE: Override of the get root call
	virtual const crmtObserver* GetRoot() const;

	// PURPOSE: Request a new state, with a simple cross fade from existing state
	// PARAMS: request - reference to the request representing the new state
	// crossFadeDuration - duration of the cross fade (default 0.0)
	void RequestNewState(crmtRequest& request, float crossFadeDuration=0.f);

	// PURPOSE: Request a new state, with a user defined crossfade from existing state
	// PARAMS: request - reference to the request representing the new state
	// crossfadeRequest - blend request representing the crossfade
	void RequestNewState(crmtRequest& request, crmtRequestBlend& crossfadeRequest);

	// PURPOSE: Retrieve observer to the current state
	// NOTE: Observer may not be observing any nodes, if current state has self destructed.
	// Will fail with assert if called on shutdown class
	const crmtObserver& GetCurrentState();

protected:
	// PURPOSE: Internal initialization
	void InternalInit(const crmtObserver* location);

private:
	crmtObserver* m_CurrentState;
	crmtObserver* m_LastBlend;
};

// inlines

////////////////////////////////////////////////////////////////////////////////

inline const crmtObserver& crmtManagerSingleState::GetCurrentState()
{
	FastAssert(m_CurrentState);
	return *m_CurrentState;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_MANAGERSINGLESTATE_H
