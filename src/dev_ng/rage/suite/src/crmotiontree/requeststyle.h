//
// crmotiontree/requeststyle.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTSTYLE_H
#define CRMOTIONTREE_REQUESTSTYLE_H

#include "request.h"

namespace rage
{

class crstStyle;

// PURPOSE: Style transfer request
// Uses the style transfer system to styalize the input
class crmtRequestStyle : public crmtRequestSource<1>
{
public:

	// PURPOSE: Default constructor
	crmtRequestStyle();

	// PURPOSE: Initializing constructor - for style with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// style - pointer to style to use
	crmtRequestStyle(crmtRequest& source, crstStyle* style);

	// PURPOSE: Initializing constructor - for applying style to existing node(s)
	// PARAMS: style - pointer to style to use
	crmtRequestStyle(crstStyle* style);

	// PURPOSE: Destrucutor
	virtual ~crmtRequestStyle();

	// PURPOSE: Initializing constructor - for style with new child node(s)
	// PARAMS: source - request to generate new child nodes
	// style - pointer to style to use
	void Init(crmtRequest& source, crstStyle* style);

	// PURPOSE: Initializing constructor - for applying style to existing node(s)
	// PARAMS: style - pointer to style to use
	void Init(crstStyle* style);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get the current style
	// RETURNS: pointer to current style (can be NULL)
	crstStyle* GetStyle() const;


	// PURPOSE: Set the current style
	// PARAMS: style - pointer to new style to use
	void SetStyle(crstStyle* style);


	// TODO -- heuristic, blend weight, up index, confidence, preference

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	crstStyle* m_Style;
};


}; // namespace rage

#endif // CRMOTIONTREE_REQUESTSTYLE_H 	
