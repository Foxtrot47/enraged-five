//
// crmotiontree/nodeanimation.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodeanimation.h"

#include "motiontree.h"
#include "observer.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeAnimation::crmtNodeAnimation()
: crmtNode(kNodeAnimation)
, m_AnimPlayer()
{
	m_AnimPlayer.SetFunctor(MakeFunctor(*this, &crmtNodeAnimation::Refresh));
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeAnimation::crmtNodeAnimation(datResource& rsc)
: crmtNode(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeAnimation::~crmtNodeAnimation()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeAnimation, crmtNode::kNodeAnimation, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeAnimation::Init(const crAnimation* anim, float time, float rate)
{
	m_AnimPlayer.SetAnimation(anim);
	m_AnimPlayer.SetTime(time);
	m_AnimPlayer.SetRate(rate);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeAnimation::Shutdown()
{
	ShuttingDown();
	crmtNode::Shutdown();

	m_AnimPlayer.SetAnimation(NULL);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeAnimation::Update(float dt)
{
	float unusedTime;
	bool hasLoopedOrEnded;

	m_AnimPlayer.Update(dt, unusedTime, hasLoopedOrEnded);

	if(hasLoopedOrEnded)
	{
		if(m_AnimPlayer.IsLooped())
		{
			MessageObservers(crmtMessage(kMsgAnimationLooped));
		}
		else
		{
			MessageObservers(crmtMessage(kMsgAnimationEnded, reinterpret_cast<void*>(&unusedTime)));
		}
	}

	SetDisabled(m_AnimPlayer.GetAnimation() == NULL);
	SetSilent(false);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeAnimation::Dump(crmtDumper& dumper) const
{
	crmtNode::Dump(dumper);

	dumper.Outputf(2, "anim", "'%s'", (m_AnimPlayer.HasAnimation()?m_AnimPlayer.GetAnimation()->GetName():"NONE"));
	dumper.Outputf(2, "time", "%.3f", m_AnimPlayer.GetTime());
	dumper.Outputf(2, "looping", "%d", m_AnimPlayer.IsLooped());
	dumper.Outputf(2, "rate", "%.3f", m_AnimPlayer.GetRate());
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNodeAnimation::Refresh()
{
	const crAnimation* anim = m_AnimPlayer.GetAnimation();
	if(anim)
	{
		GetMotionTree().GetFrameAccelerator().FindFrameAnimIndices(m_Indices, GetMotionTree().GetFrameData(), 1, &anim);
	}
	else
	{
		m_Indices.Unlock();
	}
}

////////////////////////////////////////////////////////////////////////////////

crAnimPlayer& crmtNodeAnimation::GetAnimPlayer()
{
	return m_AnimPlayer;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
