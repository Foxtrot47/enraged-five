//
// crmotiontree/observer.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "observer.h"

#include "crmtdiag.h"
#include "iterator.h"
#include "node.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtObserver::crmtObserver()
: m_RefCount(0)
, m_Node(NULL)
, m_NextObserver(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtObserver::~crmtObserver()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserver::Shutdown()
{
	if(m_Node)
	{
		m_Node->RemoveObserver(*this);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserver::Attach(crmtNode& node)
{
	Detach();
	node.AddObserver(*this);
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserver::Attach(const crmtObserver& observer)
{
	Detach();

	crmtNode* node = observer.GetNode();
	if(node)
	{
		node->AddObserver(*this);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserver::Detach()
{
	if(m_Node)
	{
		m_Node->RemoveObserver(*this);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserver::ReceiveMessage(const crmtMessage& CRMT_DIAG_NODEBUGOUTPUT_NEVER(msg))
{
	crmtDebug3("Observer %p received msg id %X payload %p", this, msg.GetMsgId(), msg.GetPayload());
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserver::ResetMessageIntercepts(crmtInterceptor&)
{
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtObserver::Dump(crmtDumper& dumper) const
{
	dumper.Outputf(1, "observer", "%p", this);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crmtObserverFunctor::crmtObserverFunctor()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtObserverFunctor::~crmtObserverFunctor()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverFunctor::Shutdown()
{
	UnregisterAllFunctors();
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverFunctor::ReceiveMessage(const crmtMessage& msg)
{
	crmtObserver::ReceiveMessage(msg);

	const u32 msgId = msg.GetMsgId();
	const int numFunctorPairs = m_FunctorPairs.GetCount();

	for(int i=0; i<numFunctorPairs; ++i)
	{
		MsgFunctorPair& funPair = m_FunctorPairs[i];
		if(funPair.m_MsgId == msgId)
		{
			funPair.m_Functor(msg, *this);
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverFunctor::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	crmtObserver::ResetMessageIntercepts(interceptor);

	const int numFunctorPairs = m_FunctorPairs.GetCount();
	for(int i=0; i<numFunctorPairs; ++i)
	{
		MsgFunctorPair& funPair = m_FunctorPairs[i];
		interceptor.RegisterMessageIntercept(funPair.m_MsgId);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverFunctor::RegisterFunctor(u32 msgId, const MsgFunctor& functor)
{
	MsgFunctorPair& funPair = m_FunctorPairs.Grow();
	funPair.m_MsgId = msgId;
	funPair.m_Functor = functor;
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverFunctor::UnregisterFunctor(u32 msgId)
{
	for(int i=0; i<m_FunctorPairs.GetCount(); ++i)
	{
		MsgFunctorPair& funPair = m_FunctorPairs[i];
		if(funPair.m_MsgId == msgId)
		{
			m_FunctorPairs.DeleteFast(i--);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverFunctor::UnregisterAllFunctors()
{
	m_FunctorPairs.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverFunctor::Reserve(int numFunctors)
{
	m_FunctorPairs.Reserve(numFunctors);
}

////////////////////////////////////////////////////////////////////////////////

crmtObserverForwarder::crmtObserverForwarder()
{
	m_RecipientObserver.AddRef();
}

////////////////////////////////////////////////////////////////////////////////

crmtObserverForwarder::~crmtObserverForwarder()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverForwarder::Shutdown()
{
	ClearRecipient();
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverForwarder::ReceiveMessage(const crmtMessage& msg)
{
	if(CRMT_UNPACK_MSG_ID_NODE_ID(msg.GetMsgId()) != crmtNode::kNodeBase)
	{
		if(m_RecipientObserver.IsAttached())
		{
			m_RecipientObserver.GetNode()->MessageObservers(msg);
		}
	}

	crmtObserver::ReceiveMessage(msg);
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverForwarder::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	crmtObserver::ResetMessageIntercepts(interceptor);

	if(IsAttached() && m_RecipientObserver.IsAttached())
	{
		// TODO --- tricky to implement:
		// needs to send new interceptor to all observers on other node
		// collecting all messages that those observers are interested in
		// this interceptor must suppress registering base node messages though
		// as they aren't forwarded
		// Then, if a reset call is seen by recipient observer (not caused by above interceptor)
		// then it needs to trigger recollection (as there's been a shift in observers on
		// recipient node

		// BUT - can do nothing for now, as only base messages are optimized
		// and none of those are forwarded anyway
	}		
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverForwarder::SetRecipient(crmtObserver& observer)
{
	m_RecipientObserver.Attach(observer);
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverForwarder::SetRecipient(crmtNode& node)
{
	m_RecipientObserver.Attach(node);
}

////////////////////////////////////////////////////////////////////////////////

void crmtObserverForwarder::ClearRecipient()
{
	m_RecipientObserver.Detach();
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtObserverForwarder::GetRecipient() const
{
	return m_RecipientObserver.GetNode();
}

////////////////////////////////////////////////////////////////////////////////

void crmtInterceptor::RegisterMessageIntercept(u32 msgId)
{
	m_Node->RegisterMessageIntercept(msgId);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
