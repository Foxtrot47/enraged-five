//
// crmotiontree/managerdrv.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "managerdrv.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtManagerDrV::crmtManagerDrV()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtManagerDrV::crmtManagerDrV(crmtMotionTree& tree, const crmtObserver* location)
: crmtManager(tree, location)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtManagerDrV::~crmtManagerDrV()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtManagerDrV::crmtManagerDrV(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crmtManagerDrV);

#if __DECLARESTRUCT
void crmtManagerDrV::DeclareStruct(datTypeStruct&)
{
	Assert(0);
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crmtManagerDrV::Init(crmtMotionTree& tree, const crmtObserver* location)
{
	crmtManager::Init(tree, location);

}

////////////////////////////////////////////////////////////////////////////////

void crmtManagerDrV::Shutdown()
{

	crmtManager::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
