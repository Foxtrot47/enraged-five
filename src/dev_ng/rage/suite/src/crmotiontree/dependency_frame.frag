//
// crmotiontree/dependency_frame.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_FRAME_FRAG
#define MOTIONTREE_DEPENDENCY_FRAME_FRAG

#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"
#include "system/dma.h"
#include "system/memops.h"
#include "vectormath/vectormath.h"

using namespace rage;
using namespace Vec;

SPUFRAG_DECL(bool, dependency_frame, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_frame, const sysDependency& dep)
{
	crmtDebug3("dependency_frame");

	u8* RESTRICT destFrame = static_cast<u8*>(dep.m_Params[0].m_AsPtr);
	const u8* RESTRICT srcFrame = static_cast<const u8*>(dep.m_Params[1].m_AsConstPtr);
	const ua16* RESTRICT indices = static_cast<const ua16*>(dep.m_Params[2].m_AsConstPtr);
	bool identical = dep.m_Params[3].m_AsBool;
	unsigned int destFrameSize = dep.m_DataSizes[0];

	if(identical) // fast copy when frames have same signature
	{
		sysMemCpy(destFrame, srcFrame, destFrameSize);
		return true;
	}

	sysMemSet(destFrame, 0xff, destFrameSize);

	unsigned int countQuads = RAGE_COUNT(indices[0], 2) + RAGE_COUNT(indices[1], 2);
	unsigned int countFloats = RAGE_COUNT(indices[2], 2);
	indices += 8;

	// copy quaternions and vectors from source to destination frame
	for(; countQuads; countQuads--)
	{
		Vector_4V src0 = *reinterpret_cast<const Vector_4V*>(srcFrame + indices[0]);
		Vector_4V src1 = *reinterpret_cast<const Vector_4V*>(srcFrame + indices[2]);
		Vector_4V src2 = *reinterpret_cast<const Vector_4V*>(srcFrame + indices[4]);
		Vector_4V src3 = *reinterpret_cast<const Vector_4V*>(srcFrame + indices[6]);

		Vector_4V* dest0 = reinterpret_cast<Vector_4V*>(destFrame + indices[1]);
		Vector_4V* dest1 = reinterpret_cast<Vector_4V*>(destFrame + indices[3]);
		Vector_4V* dest2 = reinterpret_cast<Vector_4V*>(destFrame + indices[5]);
		Vector_4V* dest3 = reinterpret_cast<Vector_4V*>(destFrame + indices[7]);

		*dest0 = src0;
		*dest1 = src1;
		*dest2 = src2;
		*dest3 = src3;

		indices += 8;
	}

	// copy scalars from source to destination frame
	for(; countFloats; countFloats--)
	{
		u32 src0 = *reinterpret_cast<const u32*>(srcFrame + indices[0]);
		u32 src1 = *reinterpret_cast<const u32*>(srcFrame + indices[2]);
		u32 src2 = *reinterpret_cast<const u32*>(srcFrame + indices[4]);
		u32 src3 = *reinterpret_cast<const u32*>(srcFrame + indices[6]);

		u32* dest0 = reinterpret_cast<u32*>(destFrame + indices[1]);
		u32* dest1 = reinterpret_cast<u32*>(destFrame + indices[3]);
		u32* dest2 = reinterpret_cast<u32*>(destFrame + indices[5]);
		u32* dest3 = reinterpret_cast<u32*>(destFrame + indices[7]);

		*dest0 = src0;
		*dest1 = src1;
		*dest2 = src2;
		*dest3 = src3;

		indices += 8;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_FRAME_FRAG
