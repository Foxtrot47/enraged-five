//
// crmotiontree/dependency_freeheap.frag
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_FREEHEAP_FRAG
#define MOTIONTREE_DEPENDENCY_FREEHEAP_FRAG

#include "crmotiontree/crmtdiag.h"
#include "system/cache.h"
#include "system/dependency.h"
#include "system/dependency_spu.h"

using namespace rage;

SPUFRAG_DECL(bool, dependency_freeheap, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_freeheap, const sysDependency& dep)
{
	crmtDebug3("dependency_freeheap");

	crHeap* heap = static_cast<crHeap*>(dep.m_Params[0].m_AsPtr);
	void* workBuffer = dep.m_Params[1].m_AsPtr;

	heap->Free(workBuffer);

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_FREEHEAP_FRAG