//
// crmotiontree/requestblend.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTBLEND_H
#define CRMOTIONTREE_REQUESTBLEND_H

#include "request.h"

namespace rage
{

class crFrameFilter;
class crWeightModifier;

// PURPOSE: Blend request
// Capable of requesting many different kinds of pair blending;
// static blends, cross fades, combining body parts, partial body blending etc
class crmtRequestBlend : public crmtRequestSource<2>
{
public:

	// PURPOSE: Default constructor
	crmtRequestBlend();

	// PURPOSE: Initializing constructor - for blends between two new inputs
	// PARAMS: source0, source1 - generate the node(s) that the blend node blends between
	// blendStart - blend weight percentage [0..1] (default 0.5f)
	// blendRate - rate of cross fade (ie 1.0 completes a 100% cross fade in one second, 2.0 in half a second - default 0.0)
	// NOTES: destroyWhenComplete is set when blendRate != 0.0 see SetDestroyWhenComplete() for more details.
	crmtRequestBlend(crmtRequest& source0, crmtRequest& source1, float blendStart=0.5f, float blendRate=0.f);

	// PURPOSE: Initializing constructor - for blends between one existing input and one new input
	// PARAMS: target - this request generates the node(s) that the blend cross fades too.
	// blendStart - starting value for cross fade percentage [0..1] (default 0.0)
	// blendRate - rate of cross fade (ie 1.0 completes a 100% cross fade in one second, 2.0 in half a second - default 1.0)
	// NOTES: destroyWhenComplete is set when blendRate != 0.0 see SetDestroyWhenComplete() for more details.
	crmtRequestBlend(crmtRequest& target, float blendStart=0.f, float blendRate=1.f);

	// PURPOSE: Destructor
	virtual ~crmtRequestBlend();

	// PURPOSE: Initializer - for blends between two new inputs
	// PARAMS: source0, source1 - generate the node(s) that the blend node blends between
	// blendStart - blend weight percentage [0..1] (default 0.5f)
	// blendRate - rate of cross fade (ie 1.0 completes a 100% cross fade in one second, 2.0 in half a second - default 0.0)
	// NOTES: destroyWhenComplete is set when blendRate != 0.0 see SetDestroyWhenComplete() for more details.
	void Init(crmtRequest& source0, crmtRequest& source1, float blendStart=0.5f, float blendRate=0.f);

	// PURPOSE: Initializer - for cross fades
	// PARAMS: target - this request generates the node(s) that the blend cross fades too.
	// blendStart - starting value for cross fade percentage [0..1] (default 0.0)
	// blendRate - rate of cross fade (ie 1.0 completes a 100% cross fade in one second, 2.0 in half a second - default 1.0)
	// NOTES: destroyWhenComplete is set when blendRate != 0.0 see SetDestroyWhenComplete() for more details.
	void Init(crmtRequest& dest, float blendStart=0.f, float blendRate=1.f);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get the [starting] blend value
	// RETURNS: Blend percentage between source0 and source1 [0..1]
	float GetBlend() const;

	// PURPOSE: Get the blend rate
	// RETURNS: Blend rate (ie 1.0 completes a 100% blend in one second, 2.0 in half a second)
	float GetBlendRate() const;

	// PURPOSE: Get destroy when complete setting (only relevant if blend rate != 0.0)
	// RETURNS: true - destroy the blend node when a cross fade has completed
	bool IsDestroyWhenComplete() const;

	// PUROSE: Is merge blend
	// RETURNS: true - is merge blend operation, false - is regular blend operation
	bool IsMergeBlend() const;

	// PURPOSE: Get the frame filter for this blend
	// RETURNS: pointer to the frame filter, can be NULL.
	crFrameFilter* GetFilter() const;

	// PURPOSE: Get blend weight modifier
	// RETURNS: pointer to the blend modifier, can be NULL.
	crWeightModifier* GetBlendModifier() const;


	// PURPOSE: Set the [starting] blend value
	// PARAMS: blend - percentage blend between source0 and source1 [0..1]
	// NOTES: result of blend = source0 * (1 - blend) + source1 * blend
	void SetBlend(float blend);

	// PURPOSE: Set the blend rate
	// PARAMS: blendRate - rate of change in the blend value
	// (ie rate of 1.0 completes a 100% blend in one second, rate of 2.0 completes in half a second)
	void SetBlendRate(float blendRate);

	// PURPOSE: Set the destroy when complete state (only relevant if blend rate != 0.0)
	// PARAMS: destroyWhenComplete - automatically destroys the blend node, and blended out child node
	// when the blend reaches 100% of the other child.
	// NOTES: This is useful for cross fading from existing node to new node, automatically destroys
	// old node and blend node when the cross fade is completed - stops dead nodes accumulating.
	void SetDestroyWhenComplete(bool destroyWhenComplete);

	// PURPOSE: Set merge blend
	// PARAMS: mergeBlend - is merge blend or regular blend operation
	// NOTES: See crFrame::MergeBlend for dicussion of differences
	void SetMergeBlend(bool mergeBlend);

	// PURPOSE: Set the frame filter for this blend
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);

	// PURPOSE: Set blend weight modifier
	// PARAMS: modifier - pointer to the blend modifier (can be NULL)
	// NOTES: modifier will be reference counted
	void SetBlendModifier(crWeightModifier* modifier);


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	float m_Blend;
	float m_BlendRate;
	bool m_DestroyWhenComplete;
	bool m_MergeBlend;
	crFrameFilter* m_Filter;
	crWeightModifier* m_Modifier;
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTBLEND_H
