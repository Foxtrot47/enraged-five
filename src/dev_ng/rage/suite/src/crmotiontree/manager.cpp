//
// crmotiontree/manager.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "manager.h"

#include "motiontree.h"
#include "node.h"
#include "observer.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtManager::crmtManager()
: m_MotionTree(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtManager::crmtManager(crmtMotionTree& tree, const crmtObserver* location)
: m_MotionTree(NULL)
{
	crmtManager::Init(tree, location);
}

////////////////////////////////////////////////////////////////////////////////

crmtManager::~crmtManager()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtManager::crmtManager(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crmtManager);

#if __DECLARESTRUCT
void crmtManager::DeclareStruct(datTypeStruct&)
{
	// TODO
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crmtManager::Init(crmtMotionTree& tree, const crmtObserver* UNUSED_PARAM(location))
{
	Assert(!m_MotionTree);
	m_MotionTree = &tree;
}

////////////////////////////////////////////////////////////////////////////////

void crmtManager::Shutdown()
{
	m_MotionTree = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtManager::Reset()
{
	const crmtObserver* root = GetRoot();
	if(root && root->GetNode())
	{
		crmtNode& node = *root->GetNode();
		node.AddRef();
		node.Orphan();
		node.Shutdown();
		node.Release();
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
