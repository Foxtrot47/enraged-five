//
// crmotiontree/requestclip.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestclip.h"

#include "nodeclip.h"

#include "crclip/clip.h"
#include "crclip/clipplayer.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestClip::crmtRequestClip()
: m_Clip(NULL)
, m_Time(0.f)
, m_Rate(1.f)
, m_IgnoreClipLooping(false)
, m_ForceLooping(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestClip::crmtRequestClip(const crClip* clip, float time, float rate)
: m_Clip(NULL)
, m_Time(0.f)
, m_Rate(1.f)
, m_IgnoreClipLooping(false)
, m_ForceLooping(false)
{
	Init(clip, time, rate);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestClip::~crmtRequestClip()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestClip::Init(const crClip* clip, float time, float rate)
{
	SetClip(clip);
	SetTime(time);
	SetRate(rate);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestClip::Shutdown()
{
	SetClip(NULL);

	crmtRequest::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestClip::SetClip(const crClip* clip)
{
	if(clip)
	{
		clip->AddRef();
	}
	if(m_Clip)
	{
		m_Clip->Release();
	}
	m_Clip = clip;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestClip::SetTime(float time)
{
	m_Time = time;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestClip::SetRate(float rate)
{
	m_Rate = rate;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestClip::SetLooping(bool ignoreClipLooping, bool forceLooping)
{
	m_IgnoreClipLooping = ignoreClipLooping;
	m_ForceLooping = forceLooping;
}

////////////////////////////////////////////////////////////////////////////////

const crClip* crmtRequestClip::GetClip() const
{
	return m_Clip;
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestClip::GetTime() const
{
	return m_Time;
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestClip::GetRate() const
{
	return m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtRequestClip::GetLooping(bool& outIgnoreClipLooping, bool& outForceLooping) const
{
	outIgnoreClipLooping = m_IgnoreClipLooping;
	outForceLooping = m_ForceLooping;

	if(m_IgnoreClipLooping)
	{
		return m_ForceLooping;
	}
	else
	{
		if(m_Clip)
		{
			return m_Clip->IsLooped();
		}
		else
		{
			// odd case, asked to respect the clip's looping status, but no valid clip
			return false;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestClip::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeClip* clipNode = crmtNodeClip::CreateNode(tree);
	Assert(clipNode);

	clipNode->Init(m_Clip, m_Time, m_Rate);
	clipNode->GetClipPlayer().SetLooped(m_IgnoreClipLooping, m_ForceLooping);
	if(node)
	{
		node->Replace(*clipNode);
	}

	return clipNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
