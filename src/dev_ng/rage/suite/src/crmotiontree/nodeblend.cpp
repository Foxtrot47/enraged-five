//
// crmotiontree/nodeblend.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodeblend.h"

#include "motiontree.h"
#include "observer.h"

#include "cranimation/framedata.h"
#include "cranimation/framefilters.h"
#include "cranimation/weightmodifier.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodePair::crmtNodePair(eNodes nodeType)
: crmtNodeParent(nodeType)
, m_Filter(NULL)
, m_FilterSignature(0)
{
	m_InfluenceOverrides[0] = kInfluenceOverrideNone;
	m_InfluenceOverrides[1] = kInfluenceOverrideNone;
}

////////////////////////////////////////////////////////////////////////////////

crmtNodePair::crmtNodePair(datResource& rsc)
: crmtNodeParent(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodePair::~crmtNodePair()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePair::Shutdown()
{
	ShuttingDown();
	SetFilter(NULL);

	crmtNodeParent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePair::Update(float)
{
	if(m_Filter && m_Filter->GetSignature() != m_FilterSignature)
	{
		Refresh();
	}
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodePair::Dump(crmtDumper& dumper) const
{
	crmtNodeParent::Dump(dumper);

	dumper.Outputf(2, "filter", "%p", m_Filter);
	dumper.Outputf(2, "influenceoverride0", "%d", m_InfluenceOverrides[0]);
	dumper.Outputf(2, "influenceoverride1", "%d", m_InfluenceOverrides[1]);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodePair::GetMinNumChildren() const
{
	return 2;
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodePair::GetMaxNumChildren() const
{
	return 2;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePair::SetFilter(crFrameFilter* filter)
{
	if(filter != m_Filter)
	{
		if(filter)
		{
			filter->AddRef();
		}
		if(m_Filter)
		{
			m_Filter->Release();
		}
		m_Filter = filter;

		Refresh();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePair::Refresh()
{
	if(m_Filter)
	{
		GetMotionTree().GetFrameAccelerator().FindFilterWeights(m_Weights, GetMotionTree().GetFrameData(), *m_Filter);
		m_FilterSignature = m_Filter->GetSignature();
	}
	else
	{
		m_Weights.Unlock();
		m_FilterSignature = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtNodePair::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

crmtNodePairWeighted::crmtNodePairWeighted(eNodes nodeType)
: crmtNodePair(nodeType)
, m_Weight(0.f)
, m_WeightRate(0.f)
, m_Modifier(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodePairWeighted::crmtNodePairWeighted(datResource& rsc)
: crmtNodePair(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodePairWeighted::~crmtNodePairWeighted()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePairWeighted::Shutdown()
{
	ShuttingDown();
	SetWeightModifier(NULL);

	crmtNodePair::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodePairWeighted::Dump(crmtDumper& dumper) const
{
	crmtNodePair::Dump(dumper);

	dumper.Outputf(2, "weight", "%.3f", m_Weight);
	dumper.Outputf(2, "weightrate", "%.3f", m_WeightRate);
	dumper.Outputf(2, "modifier", "%p", m_Modifier);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNodePairWeighted::SetWeight(float weight)
{
	m_Weight = weight;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePairWeighted::SetWeightRate(float rate)
{
	m_WeightRate = rate;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodePairWeighted::SetWeightModifier(crWeightModifier* modifier)
{
	if(modifier)
	{
		modifier->AddRef();
	}
	if(m_Modifier)
	{
		m_Modifier->Release();
	}
	m_Modifier = modifier;
}

////////////////////////////////////////////////////////////////////////////////

float crmtNodePairWeighted::GetWeightModified() const
{
	return m_Modifier ? m_Modifier->ModifyWeight(m_Weight) : m_Weight;
}

////////////////////////////////////////////////////////////////////////////////

float crmtNodePairWeighted::GetWeightRate() const
{
	return m_WeightRate;
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifier* crmtNodePairWeighted::GetWeightModifier() const
{
	return m_Modifier;
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeBlend::crmtNodeBlend()
: crmtNodePairWeighted(kNodeBlend)
, m_DestroyWhenComplete(false)
, m_MergeBlend(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeBlend::crmtNodeBlend(datResource& rsc)
: crmtNodePairWeighted(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeBlend::~crmtNodeBlend()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeBlend, crmtNode::kNodeBlend, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeBlend::Init(float weight, float weightRate, bool destroyWhenComplete, bool mergeBlend, crFrameFilter* filter, crWeightModifier* modifier)
{
	SetWeight(weight);
	SetWeightRate(weightRate);
	SetDestroyWhenComplete(destroyWhenComplete);
	SetFilter(filter);
	SetMergeBlend(mergeBlend);
	SetWeightModifier(modifier);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeBlend::Shutdown()
{
	ShuttingDown();
	crmtNodePairWeighted::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeBlend::Update(float dt)
{
	crmtNodePairWeighted::Update(dt);

	float weight = Clamp(m_Weight + m_WeightRate * dt, 0.f, 1.f);

	if(m_WeightRate > 0.f && weight == 1.f)
	{
		MessageObservers(crmtMessage(kMsgBlendComplete));

		// only child 1 should remain
		if(m_DestroyWhenComplete)
		{
			PromoteChild(1);

			// warning - this now potentially deallocated!
			return;
		}
	}
	else if(m_WeightRate < 0.f && weight == 0.f)
	{
		MessageObservers(crmtMessage(kMsgBlendComplete));

		// only child 0 should remain
		if(m_DestroyWhenComplete)
		{
			PromoteChild(0);

			// warning - this now potentially deallocated!
			return;
		}
	}

	crmtNode* child0 = GetFirstChild();
	crmtNode* child1 = child0->GetNextSibling();

	bool disabled = false, silent = false;
	if(weight <= WEIGHT_ROUNDING_ERROR_TOLERANCE)
	{
		child1->SetDisabled(true);
		child1->SetSilent(true);
		silent = true;
	}
	else if(!m_Filter && !m_MergeBlend && weight >= (1.f-WEIGHT_ROUNDING_ERROR_TOLERANCE))
	{
		child0->SetDisabled(true);
		child0->SetSilent(true);
		silent = true;
	}
	if(child0->IsDisabled() && child1->IsDisabled())
	{
		disabled = true;
		silent = false;
	}

	SetDisabled(disabled);
	SetSilent(silent);

	m_Weight = weight;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeBlend::Dump(crmtDumper& dumper) const
{
	crmtNodePairWeighted::Dump(dumper);

	dumper.Outputf(2, "destroy", "%d", m_DestroyWhenComplete);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNodeBlend::SetWeight(float weight)
{
	m_Weight = Clamp(weight, 0.f, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeBlend::SetDestroyWhenComplete(bool destroyWhenComplete)
{
	m_DestroyWhenComplete = destroyWhenComplete;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtNodeBlend::IsDestroyWhenComplete() const
{
	return m_DestroyWhenComplete;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeBlend::SetMergeBlend(bool mergeBlend)
{
	m_MergeBlend = mergeBlend;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
