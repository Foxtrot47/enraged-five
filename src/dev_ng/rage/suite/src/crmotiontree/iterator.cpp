//
// crmotiontree/iterator.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "iterator.h"

#include "motiontree.h"
#include "node.h"
#include "nodefactory.h"

#include "crmetadata/dumpoutput.h"
#include "string/string.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtIterator::crmtIterator()
: m_Cull(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtIterator::crmtIterator(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtIterator::~crmtIterator()
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtIterator::Start()
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtIterator::Finish()
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtIterator::Iterate(crmtNode& node)
{
#if CRMT_ITERATE_BATCH
	if(m_Cull)
	{
		IterateBatch<CRMT_ITERATE_BATCH, true>(node);
	}
	else
	{
		IterateBatch<CRMT_ITERATE_BATCH, false>(node);
	}
#else // CRMT_ITERATE_BATCH
	// prevent node from destruction during update
	node.AddRef();
	if(!m_Cull || !node.IsDisabled())
	{
		PreVisit(node);
	}

	// iterate the eldest child
	if(!m_Cull || !node.IsDisabled())
	{
		if(node.GetFirstChild())
		{
			Iterate(*node.GetFirstChild());
		}
	}

	// take pointer to any potential younger siblings - nodes can destroy themselves during a visit!
	crmtNode* nodeSibling = node.GetNextSibling();
	if(nodeSibling)
	{
		// prevent sibling node from destruction during update
		nodeSibling->AddRef();
	}
	
	// visit self - will happen after all children, but before younger siblings
	if(m_Cull && node.IsDisabled())
	{
		Cull(node)
	}
	else
	{
		Visit(node);
	}

	// iterate the next sibling
	if(nodeSibling)
	{
		Iterate(*nodeSibling);

		// release sibling node
		nodeSibling->Release();
	}

	// release node
	node.Release();
#endif // CRMT_ITERATE_BATCH
}

////////////////////////////////////////////////////////////////////////////////

template<int N, bool cull>
void crmtIterator::IterateBatch(crmtNode& rootNode)
{
	crmtNode* nodes[N];

	rootNode.AddRef();
	if(!cull || !rootNode.IsDisabled())
	{
		PreVisit(rootNode);
	}

	int top = 0;
	nodes[top] = &rootNode;

	do
	{
		crmtNode* node = nodes[top];

		// iterate the eldest child
		if(!cull || !node->IsDisabled())
		{
			if(node->GetFirstChild())
			{
				crmtNode* nodeChild = node->GetFirstChild();
				if(top < (N-1))
				{
					nodeChild->AddRef();
					nodes[++top] = nodeChild;
					if(!cull || !nodeChild->IsDisabled())
					{
						PreVisit(*nodeChild);
					}
					continue;
				}
				else
				{
					if(!cull || !nodeChild->IsDisabled())
					{
						IterateBatch<N, cull>(*nodeChild);
					}
				}
			}
		}

		// iterate the siblings
		do
		{
			node = nodes[top];

			// take pointer to any potential younger siblings - nodes can destroy themselves during a visit!
			crmtNode* nodeSibling = node->GetNextSibling();
			if(nodeSibling)
			{
				// prevent sibling node from destruction during update
				nodeSibling->AddRef();
			}

			// visit self - will happen after all children, but before younger siblings
			if(cull && node->IsDisabled())
			{
				Cull(*node);
			}
			else
			{
				Visit(*node);
			}

			// BAD - elder sibling released before younger siblings finish processing?
			node->Release();

			// continue with siblings or parent
			if(nodeSibling)
			{
				nodes[top] = nodeSibling;
				if(!cull || !nodeSibling->IsDisabled())
				{
					PreVisit(*nodeSibling);
				}
				break;
			}
			else
			{
				top--;
			}
		}
		while(top >= 0);
	}
	while (top >= 0);
}

////////////////////////////////////////////////////////////////////////////////

void crmtIterator::PreVisit(crmtNode&)
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtIterator::Visit(crmtNode&)
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtIterator::Cull(crmtNode&)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtDestructor::crmtDestructor(crmtMotionTree& motionTree)
: m_MotionTree(&motionTree)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtDestructor::~crmtDestructor()
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtDestructor::Start()
{
	crmtIterator::Start();
}

////////////////////////////////////////////////////////////////////////////////

void crmtDestructor::Finish()
{
	Flush();

	crmtIterator::Finish();
}

////////////////////////////////////////////////////////////////////////////////

void crmtDestructor::Visit(crmtNode& node)
{
	if(Unlikely(m_Nodes.GetCount() == m_Nodes.GetMaxCount()))
	{
		Flush();
	}
	m_Nodes.Push(&node);

	crmtIterator::Visit(node);
}

////////////////////////////////////////////////////////////////////////////////

void crmtDestructor::Flush()
{
	for(int i=0; i<m_Nodes.GetCount(); ++i)
	{
		crmtNode& node = *m_Nodes[i];

		node.m_ChildNode = NULL;
		node.m_Flags |= crmtNode::kFlagShutdown;
		node.Shutdown();
	}

	crmtNodeFactory* factory = m_MotionTree->GetNodeFactory();
	if(factory)
	{
		crmtNodeFactory::Batch batcher(*factory);
		for(int i=0; i<m_Nodes.GetCount(); ++i)
		{
			crmtNode& node = *m_Nodes[i];
			if(Likely(node.m_Factory == factory))
			{
				batcher.DeleteNode(&node);
			}
			else
			{
				// force node ref count to 0, so release will guarantee destruction
				node.m_RefCount = 0;
				node.ReleaseInternal();
			}
		}
	}
	else
	{
		for(int i=0; i<m_Nodes.GetCount(); ++i)
		{
			crmtNode& node = *m_Nodes[i];

			// force node ref count to 0, so release will guarantee destruction
			node.m_RefCount = 0;
			node.ReleaseInternal();
		}
	}

	m_Nodes.Reset();
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
crmtValidator::crmtValidator(const crmtMotionTree& motionTree)
: m_MotionTree(&motionTree)
, m_Valid(true)
, m_Dump(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtValidator::~crmtValidator()
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtValidator::Start()
{
	crmtIterator::Start();

	m_Valid = true;
	m_Depth = m_MaxDepth = 0;
}

////////////////////////////////////////////////////////////////////////////////

void crmtValidator::Finish()
{
	crmtIterator::Finish();

	if(m_Depth > 0)
	{
		Reportf("parent node stack not empty on finishing validation, tree structure corrupt");
	}

	if(m_MaxDepth >= CRMT_ITERATE_BATCH)
	{
		Reportf("tree max node depth %d exceeded, tree will not process correctly", CRMT_ITERATE_BATCH);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtValidator::PreVisit(crmtNode& node)
{
	crmtIterator::PreVisit(node);

	m_Depth++;
	m_MaxDepth = Max(m_MaxDepth, m_Depth);
}

////////////////////////////////////////////////////////////////////////////////

void crmtValidator::Visit(crmtNode& node)
{
	m_Depth--;

	crmtIterator::Visit(node);
	node.Validate(*this);
}

////////////////////////////////////////////////////////////////////////////////

void crmtValidator::Reportf(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	const int bufSize = 256;
	char buf[bufSize];
	if(fmt)
	{
		vformatf(buf, bufSize, fmt, args);
	}
	else
	{
		buf[0] = '\0';
	}
	Warningf("%s", buf);

	m_Valid = false;

	if(!m_Dump)
	{
		Displayf("problem with motion tree %p, attempting to dump...", this);
		m_MotionTree->Dump(3);
		m_Dump = true;
	}

	va_end(args);
}

////////////////////////////////////////////////////////////////////////////////

crmtDumper::crmtDumper()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtDumper::~crmtDumper()
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtDumper::Start()
{
	crmtIterator::Start();

}

////////////////////////////////////////////////////////////////////////////////

void crmtDumper::Finish()
{
	crmtIterator::Finish();
}

////////////////////////////////////////////////////////////////////////////////

void crmtDumper::PreVisit(crmtNode& node)
{
	crmtIterator::PreVisit(node);

	PushLevel(node.GetNodeTypeName());
	node.Dump(*this);
}

////////////////////////////////////////////////////////////////////////////////

void crmtDumper::Visit(crmtNode& node)
{
	crmtIterator::Visit(node);

	PopLevel();
}

////////////////////////////////////////////////////////////////////////////////

crmtDumperOutput::crmtDumperOutput(crDumpOutput& output)
: crmtDumper()
, m_Output(&output)
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtDumperOutput::Outputf(int verbosity, const char* field, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	m_Output->InternalOutputf(verbosity, field, fmt, args);

	va_end(args);
}

////////////////////////////////////////////////////////////////////////////////

void crmtDumperOutput::Outputf(int verbosity, const char* field, int element, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	m_Output->InternalOutputf(verbosity, field, element, fmt, args);

	va_end(args);
}

////////////////////////////////////////////////////////////////////////////////

void crmtDumperOutput::Outputf(int verbosity, const char* field, int element, int subelement, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	m_Output->InternalOutputf(verbosity, field, element, subelement, fmt, args);

	va_end(args);
}

////////////////////////////////////////////////////////////////////////////////

void crmtDumperOutput::PushLevel(const char* name)
{
	m_Output->PushLevel(name);
}

////////////////////////////////////////////////////////////////////////////////

void crmtDumperOutput::PopLevel()
{
	m_Output->PopLevel();
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
