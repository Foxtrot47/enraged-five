//
// crmotiontree/request.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "node.h"

#include "request.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequest::crmtRequest()
{
	m_Observer.AddRef();
}

////////////////////////////////////////////////////////////////////////////////

crmtRequest::~crmtRequest()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequest::Shutdown()
{
	m_Observer.Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequest::Generate(crmtMotionTree& tree, crmtNode* node)
{
	crmtNode* newNode = GenerateNode(tree, node);
	if(newNode)
	{
		newNode->AddObserver(m_Observer);
	}
	else
	{
		ClearObserver();
	}

	return newNode;
}

////////////////////////////////////////////////////////////////////////////////

const crmtObserver& crmtRequest::GetObserver() const
{
	return m_Observer;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequest::ClearObserver()
{
	m_Observer.Detach();
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestInsert::crmtRequestInsert()
: m_ChildIdx(0)
{

}

////////////////////////////////////////////////////////////////////////////////

crmtRequestInsert::crmtRequestInsert(crmtRequest& source, u32 childIdx)
: m_ChildIdx(0)
{
	Init(source, childIdx);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestInsert::~crmtRequestInsert()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestInsert::Init(crmtRequest& source, u32 childIdx)
{
	SetSource(&source, 0);
	SetChildIndex(childIdx);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestInsert::Shutdown()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestInsert::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	if(HasValidSource())
	{
		crmtNode* sourceNode = GetSource(0)->Generate(tree, NULL);

		if(node)
		{
			if(node->CanHaveChildren())
			{
				crmtNodeParent* nodeParent = static_cast<crmtNodeParent*>(node);
				nodeParent->InsertChild(m_ChildIdx, *sourceNode);
			}
			else
			{
				node->Replace(*sourceNode);
			}
		}

		return sourceNode;
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestInsert::SetChildIndex(u32 childIdx)
{
	m_ChildIdx = childIdx;
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtRequestInsert::GetChildIndex() const
{
	return m_ChildIdx;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
