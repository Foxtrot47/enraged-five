//
// crmotiontree/dependency_pose.frag
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_POSE_FRAG
#define MOTIONTREE_DEPENDENCY_POSE_FRAG

#include "crmotiontree/crmtdiag.h"
#include "system/cache.h"
#include "system/dependency.h"
#include "system/dma.h"
#include "system/memops.h"
#include "vectormath/classes.h"

using namespace rage;
using namespace Vec;

SPUFRAG_DECL(bool, dependency_pose, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_pose, const sysDependency& dep)
{
	crmtDebug3("dependency_pose");

	const u8* RESTRICT srcFrame = static_cast<const u8*>(dep.m_Params[0].m_AsConstPtr);
	const ua16* RESTRICT indices = static_cast<const ua16*>(dep.m_Params[1].m_AsConstPtr);
	const ua16* RESTRICT parentIndices = static_cast<const ua16*>(dep.m_Params[2].m_AsConstPtr);
	const Mat34V* RESTRICT defaults = static_cast<const Mat34V*>(dep.m_Params[3].m_AsConstPtr);
	Mat34V* RESTRICT locals = static_cast<Mat34V*>(dep.m_Params[4].m_AsPtr);
	unsigned int numBones = dep.m_Params[5].m_AsInt;
	unsigned int numChildParentIndices = dep.m_Params[6].m_AsInt;
	bool localPoseOnly = dep.m_Params[7].m_AsBool;
	bool suppressReset = dep.m_Params[8].m_AsBool;

	Mat34V* RESTRICT objects = locals + numBones;

	// copy default local matrices if reset
	if(!suppressReset)
	{
		sysMemCpy(locals, defaults, sizeof(Mat34V)*numBones);
	}

	Vector_4V _invalid = V4VConstant(V_MASKXYZW);

	unsigned int countTranslations = RAGE_COUNT(indices[0], 2);
	unsigned int countRotations = RAGE_COUNT(indices[1], 2);
	unsigned int countScales = RAGE_COUNT(indices[2], 2);
	indices += 8;

	// apply translation dofs to local matrices, each dof can be invalid
	for(; countTranslations; countTranslations--)
	{
		Vec3V src0 = *reinterpret_cast<const Vec3V*>(srcFrame + indices[0]);
		Vec3V src1 = *reinterpret_cast<const Vec3V*>(srcFrame + indices[2]);
		Vec3V src2 = *reinterpret_cast<const Vec3V*>(srcFrame + indices[4]);
		Vec3V src3 = *reinterpret_cast<const Vec3V*>(srcFrame + indices[6]);

		Mat34V* pDest0 = locals + indices[1];	Vec3V dest0 = pDest0->GetCol3();
		Mat34V* pDest1 = locals + indices[3];	Vec3V dest1 = pDest1->GetCol3();
		Mat34V* pDest2 = locals + indices[5];	Vec3V dest2 = pDest2->GetCol3();
		Mat34V* pDest3 = locals + indices[7];	Vec3V dest3 = pDest3->GetCol3();

		VecBoolV invalid0 = IsEqualInt(src0, Vec3V(_invalid));
		VecBoolV invalid1 = IsEqualInt(src1, Vec3V(_invalid));
		VecBoolV invalid2 = IsEqualInt(src2, Vec3V(_invalid));
		VecBoolV invalid3 = IsEqualInt(src3, Vec3V(_invalid));

		pDest0->SetCol3( SelectFT(invalid0, src0, dest0) );
		pDest1->SetCol3( SelectFT(invalid1, src1, dest1) );
		pDest2->SetCol3( SelectFT(invalid2, src2, dest2) );
		pDest3->SetCol3( SelectFT(invalid3, src3, dest3) );

		indices += 8;
	}

	// apply rotation dofs to local matrices, each dof can be invalid
	for(; countRotations; countRotations--)
	{
		QuatV src0 = *reinterpret_cast<const QuatV*>(srcFrame + indices[0]);
		QuatV src1 = *reinterpret_cast<const QuatV*>(srcFrame + indices[2]);
		QuatV src2 = *reinterpret_cast<const QuatV*>(srcFrame + indices[4]);
		QuatV src3 = *reinterpret_cast<const QuatV*>(srcFrame + indices[6]);

		Mat33V* pDest0 = reinterpret_cast<Mat33V*>(locals + indices[1]);	Mat33V dest0 = *pDest0;
		Mat33V* pDest1 = reinterpret_cast<Mat33V*>(locals + indices[3]);	Mat33V dest1 = *pDest1;
		Mat33V* pDest2 = reinterpret_cast<Mat33V*>(locals + indices[5]);	Mat33V dest2 = *pDest2;
		Mat33V* pDest3 = reinterpret_cast<Mat33V*>(locals + indices[7]);	Mat33V dest3 = *pDest3;

		Mat33V srcMtx0, srcMtx1, srcMtx2, srcMtx3;
		Mat33VFromQuatV(srcMtx0, src0);
		Mat33VFromQuatV(srcMtx1, src1);
		Mat33VFromQuatV(srcMtx2, src2);
		Mat33VFromQuatV(srcMtx3, src3);

		VecBoolV invalid0 = IsEqualInt(src0, QuatV(_invalid));
		VecBoolV invalid1 = IsEqualInt(src1, QuatV(_invalid));
		VecBoolV invalid2 = IsEqualInt(src2, QuatV(_invalid));
		VecBoolV invalid3 = IsEqualInt(src3, QuatV(_invalid));

		*pDest0 = SelectFT(invalid0, srcMtx0, dest0);
		*pDest1 = SelectFT(invalid1, srcMtx1, dest1);
		*pDest2 = SelectFT(invalid2, srcMtx2, dest2);
		*pDest3 = SelectFT(invalid3, srcMtx3, dest3);

		indices += 8;
	}

	// apply scale dofs to local matrices, each dof can be invalid
	for(; countScales; countScales--)
	{
		Vec3V src0 = *reinterpret_cast<const Vec3V*>(srcFrame + indices[0]);
		Vec3V src1 = *reinterpret_cast<const Vec3V*>(srcFrame + indices[2]);
		Vec3V src2 = *reinterpret_cast<const Vec3V*>(srcFrame + indices[4]);
		Vec3V src3 = *reinterpret_cast<const Vec3V*>(srcFrame + indices[6]);

		Mat33V* pDest0 = reinterpret_cast<Mat33V*>(locals + indices[1]);	Mat33V dest0 = *pDest0;
		Mat33V* pDest1 = reinterpret_cast<Mat33V*>(locals + indices[3]);	Mat33V dest1 = *pDest1;
		Mat33V* pDest2 = reinterpret_cast<Mat33V*>(locals + indices[5]);	Mat33V dest2 = *pDest2;
		Mat33V* pDest3 = reinterpret_cast<Mat33V*>(locals + indices[7]);	Mat33V dest3 = *pDest3;

		Mat33V srcMtx0, srcMtx1, srcMtx2, srcMtx3;
		ScaleTranspose(srcMtx0, src0, dest0);
		ScaleTranspose(srcMtx1, src1, dest1);
		ScaleTranspose(srcMtx2, src2, dest2);
		ScaleTranspose(srcMtx3, src3, dest3);

		VecBoolV invalid0 = IsEqualInt(src0, Vec3V(_invalid));
		VecBoolV invalid1 = IsEqualInt(src1, Vec3V(_invalid));
		VecBoolV invalid2 = IsEqualInt(src2, Vec3V(_invalid));
		VecBoolV invalid3 = IsEqualInt(src3, Vec3V(_invalid));

		*pDest0 = SelectFT(invalid0, srcMtx0, dest0);
		*pDest1 = SelectFT(invalid1, srcMtx1, dest1);
		*pDest2 = SelectFT(invalid2, srcMtx2, dest2);
		*pDest3 = SelectFT(invalid3, srcMtx3, dest3);

		indices += 8;
	}

	// update object matrices from local matrices
	if(!localPoseOnly)
	{
#if !__SPU
		for(unsigned int i=0; i < numBones*sizeof(Mat34V); i+=128)
		{
			PrefetchDC2(objects, i);
		}
#endif
		*objects = *locals;

		// unrolled 4 times with indices
		unsigned int count = RAGE_COUNT(numChildParentIndices, 3);
		for(; count; count--)
		{
			unsigned int childIdx0 = parentIndices[0];
			unsigned int childIdx1 = parentIndices[2];
			unsigned int childIdx2 = parentIndices[4];
			unsigned int childIdx3 = parentIndices[6];

			unsigned int parentIdx0 = parentIndices[1];
			unsigned int parentIdx1 = parentIndices[3];
			unsigned int parentIdx2 = parentIndices[5];
			unsigned int parentIdx3 = parentIndices[7];

			Mat34V child0 = locals[childIdx0];	
			Mat34V child1 = locals[childIdx1];	
			Mat34V child2 = locals[childIdx2];	
			Mat34V child3 = locals[childIdx3];	

			Mat34V parent0 = objects[parentIdx0];
			Mat34V parent1 = objects[parentIdx1];
			Mat34V parent2 = objects[parentIdx2];
			Mat34V parent3 = objects[parentIdx3];

			Mat34V* dest0 = objects + childIdx0;
			Mat34V* dest1 = objects + childIdx1;
			Mat34V* dest2 = objects + childIdx2;
			Mat34V* dest3 = objects + childIdx3;

			Transform(*dest0, parent0, child0);
			Transform(*dest1, parent1, child1);
			Transform(*dest2, parent2, child2);
			Transform(*dest3, parent3, child3);

			parentIndices += 8;
		}
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_POSE_FRAG
