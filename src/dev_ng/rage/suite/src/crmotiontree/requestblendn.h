//
// crmotiontree/requestblendn.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTBLENDN_H
#define CRMOTIONTREE_REQUESTBLENDN_H

#include "nodeblendn.h"
#include "request.h"

namespace rage
{

class crFrameFilter;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: N-way request
// Capable of combining multiple inputs into a single output, with weights and filters
class crmtRequestN : public crmtRequestSource<crmtNodeN::sm_MaxChildren>
{
public:

	// PURPOSE: Default constructor
	crmtRequestN();

	// PURPOSE: Destructor
	virtual ~crmtRequestN();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get the initial weight for a source
	// PARAMS: sourceIdx - source index
	// RETURNS: Weight percentage for source
	float GetWeight(u32 sourceIdx) const;

	// PURPOSE: Get the initial weight rate for a source
	// PARAMS: sourceIdx - source index
	// RETURNS: Weight rate (can be +ve or -ve, percentage per second, ie 1.0 = 100% per sec)
	float GetWeightRate(u32 sourceIdx) const;

	// PURPOSE: Get the frame filter for this blend
	// RETURNS: pointer to the frame filter, can be NULL.
	crFrameFilter* GetFilter() const;

	// PURPOSE: Get the input filter of a child
	// PARAMS: childIdx - index of child node [0..(numChildren-1)]
	// RETURNS: pointer to the frame filter, can be NULL.
	crFrameFilter* GetInputFilter(u32 childIdx) const;

	// PURPOSE: Is child zero the destination in the n-way operation
	// RETURNS: true - child zero is destination, false - treated as normal source
	// NOTES: See SetZeroDestiniation for description
	bool IsZeroDestination() const;



	// PURPOSE: Set the [starting] weight value
	// PARAMS: sourceIdx - source index
	// weight - weight percentage for source [0..1]
	void SetWeight(u32 sourceIdx, float weight);

	// PURPOSE: Set all the [starting] weight values
	// PARAMS: weight - blend weight percentage for source [0..1]
	void SetWeights(float weight);

	// PURPOSE: Set the weight rate
	// PARAMS: sourceIdx - source index
	// weightRate - weight rate (can be +ve or -ve, percentage per second, ie 1.0 = 100% per sec)
	void SetWeightRate(u32 sourceIdx, float weightRate);

	// PURPOSE: Set all the weight rates
	// weightRate - weight rate (can be +ve or -ve, percentage per second, ie 1.0 = 100% per sec)
	void SetWeightRates(float weightRate);

	// PURPOSE: Set the frame filter for this blend
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);

	// PURPOSE: Set the input frame filter for a source
	// PARAMS: sourceIdx - source index, filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetInputFilter(u32 sourceIdx, crFrameFilter* filter);

	// PURPOSE: Set child zero as destination in the n-way operation
	// PARAMS: zeroDest - use child zero as destination
	// NOTES: Changes the way input from first child behaves in n-way operation
	// First child becomes default result, with other inputs from
	// other children combined on to it (desirable behavior for layering)
	// Weight and input filter for first child will be ignored
	void SetZeroDestination(bool zeroDest);

protected:
	atRangeArray<float, crmtNodeBlendN::sm_MaxChildren> m_Weights;
	atRangeArray<float, crmtNodeBlendN::sm_MaxChildren> m_WeightRates;
	atRangeArray<crFrameFilter*, crmtNodeBlendN::sm_MaxChildren> m_InputFilters;
	crFrameFilter* m_Filter;
	bool m_ZeroDest;
};


////////////////////////////////////////////////////////////////////////////////


// PURPOSE: N-way blend request
// Capable of blending multiple inputs into a single output
class crmtRequestBlendN : public crmtRequestN
{
public:

	// PURPOSE: Default constructor
	crmtRequestBlendN();

	// PURPOSE: Destructor
	virtual ~crmtRequestBlendN();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// TODO --- add mergeblend property

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_REQUESTBLENDN_H
