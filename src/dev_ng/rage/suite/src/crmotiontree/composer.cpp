//
// crmotiontree/composer.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "composer.h"

#include "composerdata.h"
#include "crmtdiag.h"
#include "nodeanimation.h"
#include "nodeblend.h"
#include "nodeblendn.h"
#include "nodecapture.h"
#include "nodeclip.h"
#include "nodeexpression.h"
#include "nodeextrapolate.h"
#include "nodefilter.h"
#include "nodeframe.h"
#include "nodeik.h"
#include "nodemerge.h"
#include "nodemirror.h"
#include "nodepm.h"
#include "nodepose.h"
#include "nodeproxy.h"
#include "motiontree.h"

#include "cranimation/frameiterators.h"
#include "crbody/iksolver.h"
#include "crbody/kinematics.h"
#include "crclip/clipanimation.h"
#include "crclip/clipanimations.h"
#include "crclip/clipanimationexpression.h"
#include "creature/componentshadervars.h"
#include "crextra/expressionstream.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/dependencyscheduler.h"

DECLARE_FRAG_INTERFACE(dependency_add)
DECLARE_FRAG_INTERFACE(dependency_animation)
DECLARE_FRAG_INTERFACE(dependency_blend)
DECLARE_FRAG_INTERFACE(dependency_expression)
DECLARE_FRAG_INTERFACE(dependency_filter)
DECLARE_FRAG_INTERFACE(dependency_frame)
DECLARE_FRAG_INTERFACE(dependency_freeheap)
DECLARE_FRAG_INTERFACE(dependency_identity)
DECLARE_FRAG_INTERFACE(dependency_ik)
DECLARE_FRAG_INTERFACE(dependency_invalid)
DECLARE_FRAG_INTERFACE(dependency_inversepose)
DECLARE_FRAG_INTERFACE(dependency_merge)
DECLARE_FRAG_INTERFACE(dependency_extrapolate)
DECLARE_FRAG_INTERFACE(dependency_normalize)
DECLARE_FRAG_INTERFACE(dependency_physical)
DECLARE_FRAG_INTERFACE(dependency_pose)
DECLARE_FRAG_INTERFACE(dependency_pose_bounds)
DECLARE_FRAG_INTERFACE(dependency_proxy)
DECLARE_FRAG_INTERFACE(dependency_shadervars)

#if CR_STATS
PF_PAGE(crmtComposePage,"crmt Compose");
PF_GROUP(crmtComposeStats);
PF_COUNTER(crmtCompose_NumTrees, crmtComposeStats);
PF_COUNTER(crmtCompose_NumDeps, crmtComposeStats);
PF_COUNTER(crmtCompose_NumFrames, crmtComposeStats);
PF_COUNTER(crmtCompose_NumRetries, crmtComposeStats);
PF_VALUE_INT(crmtCompose_MaxTrees, crmtComposeStats);
PF_VALUE_INT(crmtCompose_MaxDeps, crmtComposeStats);
PF_VALUE_INT(crmtCompose_MaxFrames, crmtComposeStats);
PF_VALUE_INT(crmtCompose_MaxRetries, crmtComposeStats);
PF_LINK(crmtComposePage,crmtComposeStats);
#endif // CR_STATS

#if !__PS3
#include "dependency_add.frag"
#include "dependency_animation.frag"
#include "dependency_blend.frag"
#include "dependency_expression.frag"
#include "dependency_extrapolate.frag"
#include "dependency_filter.frag"
#include "dependency_frame.frag"
#include "dependency_freeheap.frag"
#include "dependency_identity.frag"
#include "dependency_ik.frag"
#include "dependency_invalid.frag"
#include "dependency_inversepose.frag"
#include "dependency_merge.frag"
#include "dependency_normalize.frag"
#include "dependency_pose.frag"
#include "dependency_pose_bounds.frag"
#include "dependency_physical.frag"
#include "dependency_proxy.frag"
#include "dependency_shadervars.frag"
#endif // !__PS3

#if __SPU
#include "system/dependencyscheduler.cpp"
#endif // __SPU

namespace rage
{

using namespace Vec;
using namespace sysDepFlag;

enum { FRAME0=BIT24, FRAME1=BIT25 };
enum { kTagAdd=8, kTagAnimation, kTagBlend, kTagExpression, kTagFilter, kTagFrame, kTagIdentity, kTagIk, kTagInvalid, kTagInversePose, kTagMerge, kTagNormalize, kTagPose, kTagMoverDelta, kTagPhysical, kTagProxy, kTagNodePm, kTagNodeN, kTagNodeMirror, kTagNodeStyle, kTagExtrapolate, kTagShaderVars, kTagFreeHeap, kTagPopBounds };

///////////////////////////////////////////////////////////////////////

crmtComposer::crmtComposer(const crFrameData& frameData, const crmtComposerData& builderData, u8* scratchBuffer, u32 scratchSize)
: m_FrameData(frameData)
, m_Data(builderData)
{
	m_Scratch.Init(scratchBuffer, scratchSize);
	m_Dependencies = static_cast<sysDependency*>(m_Scratch.Alloc(sizeof(sysDependency)*sm_MaxNumDependencies));
	m_NumDeps = m_NumFrames = m_NumProxies = 0;
	m_TopDep = m_TopFrame = -1;
}

///////////////////////////////////////////////////////////////////////

bool crmtComposer::Commit(crHeap* heap, sysDependency* parentDep, u32 priority)
{
#if CR_STATS
	PF_INCREMENT(crmtCompose_NumTrees);
	PF_INCREMENTBY(crmtCompose_NumDeps, m_NumDeps);
	PF_INCREMENTBY(crmtCompose_NumFrames, m_NumFrames);
	PF_SET(crmtCompose_MaxTrees, Max(PFVALUE_crmtCompose_MaxTrees.GetValue(), PFCOUNTER_crmtCompose_NumTrees.GetValue()));
	PF_SET(crmtCompose_MaxRetries, Max(PFVALUE_crmtCompose_MaxRetries.GetValue(), PFCOUNTER_crmtCompose_NumRetries.GetValue()));
	PF_SET(crmtCompose_MaxDeps, Max(PFVALUE_crmtCompose_MaxDeps.GetValue(), (int)m_NumDeps));
	PF_SET(crmtCompose_MaxFrames, Max(PFVALUE_crmtCompose_MaxFrames.GetValue(), (int)m_NumFrames));
#endif // CR_STATS

	// check if we don't overflow
	Assert(!m_TopDep);
	Assert(!m_TopFrame);
	Assert(m_NumFrames <= sm_MaxNumFrames);

	// push last dependencies
	if(m_Data.m_Finalize)
	{
		PopSkeleton(m_Data.m_SuppressReset);
		if(m_Data.m_Physical && m_Data.m_NumMotions)
		{
			PushPhysical();
		}
		if(m_Data.m_Physical && m_Data.m_Fragment.m_Ptr)
		{
			PoseBounds(m_Data.m_Fragment.m_CopyLastMatrices);
		}
		if(m_Data.m_NumShaderVars)
		{
			PushShaderVars();
		}
		if(m_Data.m_ExtraDofsFrame)
		{
			PopFrame(m_Data.m_ExtraDofsFrame, m_Data.m_ExtraDofIndices);
		}
	}

	// we can free memory if we don't have to finalize the creature
	sysDependency* freeHeapDep = NULL;
	if(!m_Data.m_CreatureFinalize)
	{
		sysDependency& dep = m_Dependencies[m_NumDeps++];
		dep.Init(FRAG_SPU_CODE(dependency_freeheap), kTagFreeHeap);
		dep.AddChild(*m_DepStack[m_TopDep]);
		dep.m_Params[0].m_AsPtr = heap;
		m_DepStack[m_TopDep] = &dep;
		freeHeapDep = &dep;
	}

	u32 topFrame = m_FrameStack[0];
	sysDependency* topDep = m_DepStack[0];
	
	// set last dependency as frame releaser
	m_FrameReleasers[topFrame] = topDep;

	u32 offsets[sm_MaxNumFrames];
	u32 frameScratch = ComputeFrameOffsets(offsets);

	// allocate heap for dependencies and frames
	u32 numDeps = m_NumDeps;
	u32 frameSize = m_FrameData.GetFrameBufferSize();
	u32 depScratch = numDeps * sizeof(sysDependency);
	u32 scratchBufferSize = depScratch + frameScratch;
	const u32 maxAllocation = CRMT_COMPOSER_HEAPSIZE - 16;
	if(scratchBufferSize > maxAllocation)
	{
		crmtWarning("exceed maximum allocation (%d bytes)", scratchBufferSize);
		return true;
	}

	// try to allocate, if it fails retry later on
	void* workBuffer = heap->Allocate(scratchBufferSize);
	if(!workBuffer)
	{
#if CR_STATS
		PF_INCREMENT(crmtCompose_NumRetries);
#endif
		crmtDebug1("cannot allocate %u bytes, will try again later", scratchBufferSize);
		return false;
	}
	if(freeHeapDep)
	{
		freeHeapDep->m_Params[1].m_AsPtr = workBuffer;
	}

	u8* frameBuffer = static_cast<u8*>(workBuffer) + depScratch;
	u8* lastFrame = frameBuffer + topFrame*frameSize;
	crmtComposerData* ppuData = &m_Data.m_MotionTree->GetComposerData();
	sysMemPutPtr(uptr(workBuffer), uptr(&ppuData->m_WorkBuffer), 0);
	sysMemPutPtr(uptr(lastFrame), uptr(&ppuData->m_LastFrame), 0);

	// fixup dependencies and collect leaves
	u8 childrenPriority = static_cast<u8>(Min(priority+1u, u32(sysDependency::kPriorityCritical)));
	ptrdiff_t fixup = (u8*)workBuffer - (u8*)m_Dependencies;
	for(u32 i=0; i < numDeps; ++i)
	{
		sysDependency& dep = m_Dependencies[i];
		for(u32 j=0; j < sysDependency::sm_MaxNumParents; j++)
		{
			dep.m_Parents[j] = dep.m_Parents[j] ? reinterpret_cast<sysDependency*>((u8*)dep.m_Parents[j] + fixup) : NULL;
		}

		u32 flags = dep.m_Flags;
		if(flags & FRAME0)
		{
			dep.m_Params[0].m_AsPtr = frameBuffer + offsets[dep.m_Params[0].m_AsInt];
		}
		if(flags & FRAME1)
		{
			dep.m_Params[1].m_AsPtr = frameBuffer + offsets[dep.m_Params[1].m_AsInt];
		}
		dep.m_Priority = childrenPriority;
	}

	// set recycle dependency as parent
	topDep->m_Parents[sysDependency::kParentStrong] = parentDep;
	sysInterlockedIncrement(&parentDep->m_NumPending);

	// copy dependencies to main memory
	sysMemLargePutAndWait(m_Dependencies, uptr(workBuffer), numDeps*sizeof(sysDependency), 0);

	// insert dependencies built inside the scheduler
	for(u32 i=0; i < numDeps; ++i)
	{
		sysDependency& dep = m_Dependencies[i];
		if(!dep.m_NumPending)
		{
			sysDependencyScheduler::Insert(reinterpret_cast<sysDependency*>((u8*)&dep+fixup));		
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////

u32 crmtComposer::ComputeFrameOffsets(u32* offsets)
{
	// new frames
	u32 numFrames = m_NumFrames;
	u32 frameSize = m_FrameData.GetFrameBufferSize();
	u32 lastFrame = Min(numFrames, sm_MaxUsedFrames);
	u32 lastOffset = 0;
	for(u32 i=0; i < lastFrame; i++)
	{
		offsets[i] = lastOffset;
		lastOffset += frameSize;
	}

	// reused frames
	for(u32 i=lastFrame; i < numFrames; i++)
	{
		sysDependency* allocator = m_FrameAllocators[i];
		u32 j=0;
		for(; j < lastFrame; j++)
		{
			sysDependency*& releaser = m_FrameReleasers[j];
			if(!releaser->m_Parents[sysDependency::kParentWeak] && releaser < allocator)
			{
				allocator->AddWeakChild(*releaser);
				releaser = m_FrameReleasers[i];
				offsets[i] = offsets[j];
				break;
			}
		}
		if(j==lastFrame) // cannot reuse frame, allocate a new one
		{
			offsets[i] = lastOffset;
			lastOffset += frameSize;
		}
	}

	return lastOffset;
}

///////////////////////////////////////////////////////////////////////

bool crmtComposer::Compose(const crmtNode* rootNode)
{
	static const u32 MaxNodeSize = RAGE_ALIGN(sizeof(crmtNodeBlendN), 4);
	const crmtNode* refNode = rootNode;
	const crmtNode* refNodeStack[CRMT_ITERATE_BATCH];
	const crmtNode* nodeStack[CRMT_ITERATE_BATCH];
	u8 proxyStack[CRMT_ITERATE_BATCH];
	u32 numStackNodes = 0;

	// check root node
	if(Unlikely(!rootNode))
	{
		crmtWarning("empty motion tree");
		return false;
	}

	while(1)
	{
		const crmtNode* localNode = static_cast<const crmtNode*>(m_Scratch.Push(refNode, MaxNodeSize));
		m_Scratch.Wait();

		// iterate the eldest child
		if(!localNode->IsDisabled())
		{
			bool parentProxy = localNode->GetNodeType()==crmtNode::kNodeProxy;
			crmtNode* nodeChild = parentProxy ? static_cast<const crmtNodeProxy*>(localNode)->GetProxyNode() : localNode->GetFirstChild();
			if(nodeChild)
			{
				// get sibling if we skip first child
				if(localNode->SkipFirstChild())
				{
					const crmtNode* localChildNode = static_cast<const crmtNode*>(m_Scratch.Push(nodeChild, MaxNodeSize));
					m_Scratch.Wait();
					nodeChild = localChildNode->GetNextSibling();
					Assert(nodeChild);
					m_Scratch.Pop(MaxNodeSize);
				}

				// try getting proxy before composing child node
				bool skip = !__SPU && !nodeChild->IsProxied();
				if(skip || !PopProxyNode(nodeChild))
				{
					if(Unlikely(numStackNodes >= CRMT_ITERATE_BATCH))
					{
						crmtWarning("too many nodes on the stack (%d)", numStackNodes);
						return false;
					}

					refNodeStack[numStackNodes] = refNode;
					nodeStack[numStackNodes] = localNode;
					proxyStack[numStackNodes] = parentProxy;
					numStackNodes++;
					refNode = nodeChild;
					continue;
				}
			}
		}

		// iterate the siblings
		while(1)
		{
			// exit if the motion tree is too big
			if(Unlikely(m_NumDeps > sm_MaxNumDependencies-sm_MinDependencies))
			{
				crmtWarning("too many dependencies generated (%d)", m_NumDeps);
				return false;
			}

			if(Unlikely(m_NumFrames > sm_MaxNumFrames-sm_MinFrames))
			{
				crmtWarning("too many frames generated (%d)", m_NumFrames);
				return false;
			}

			// take pointer to any potential younger siblings - nodes can destroy themselves during a visit!
			const crmtNode* nodeSibling = localNode->GetNextSibling();

			// visit self - will happen after all children, but before younger siblings
			bool isSilent = localNode->IsSilent();
			bool isDisabled = localNode->IsDisabled();
			if(!isSilent)
			{
				if(!isDisabled)
				{
					Visit(*refNode, *localNode);
				}
				else
				{
					PushInvalid();
				}
			}

			// cache result if proxied node
			if(localNode->IsProxied())
			{
				PushProxyNode(refNode);
			}

			m_Scratch.Pop(MaxNodeSize);

			// continue with siblings or parent
			if(nodeSibling && !proxyStack[numStackNodes-1])
			{
				refNode = nodeSibling;
				break;
			}
			else if(numStackNodes > 0)
			{
				numStackNodes--;
				refNode = refNodeStack[numStackNodes];
				localNode = nodeStack[numStackNodes];
				continue;
			}

			return true;
		}
	}
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::Visit(const crmtNode& refNode, const crmtNode& node)
{
	crmtNode::eNodes nodeType = node.GetNodeType();

	switch(nodeType)
	{
	case crmtNode::kNodeAnimation:
		{
			const crmtNodeAnimation& nodeAnim = static_cast<const crmtNodeAnimation&>(node);
			const crLock& animIndices = nodeAnim.GetIndices();
			const crAnimPlayer& player = nodeAnim.GetAnimPlayer();
			const crAnimation* anim = player.GetAnimation();
			if(anim)
			{
				PushAnimation(anim, player.GetTime(), player.GetDelta(), 0.f, FLT_MAX, animIndices.Size(), animIndices.Get());
			}
			else
			{
				PushInvalid();
			}
			break;
		}

	case crmtNode::kNodeClip:
		{
			const crmtNodeClip& nodeClip = static_cast<const crmtNodeClip&>(node);
			const crLock& animIndices = nodeClip.GetAnimIndices();
			const crClipPlayer& player = nodeClip.GetClipPlayer();
			const crClip* clip = player.GetClip();
			const u8* indices = animIndices.Get();
			unsigned int sizeIndices = animIndices.Size();
			if(clip)
			{
				const crClip* localClip = static_cast<const crClip*>(m_Scratch.Get(clip, sizeof(crClipAnimations)));
				crClip::eClipType clipType = localClip->GetType();
				float playerTime = player.GetBlockedTime();
				float playerDelta = player.GetDelta();

				switch(clipType)
				{
				case crClip::kClipTypeAnimationExpression:
					{
						const crClipAnimationExpression* clipAnimExpr = static_cast<const crClipAnimationExpression*>(localClip);
						const crExpressions* exprs = clipAnimExpr->GetExpressions();
						float delta = clipAnimExpr->GetRate() * playerDelta;
						if(exprs)
						{
							PushExpressions(exprs, 0.f, delta, 1.f, NULL, 0, nodeClip.GetExprFrameIndices(), nodeClip.GetExprSkelIndices());
						}
					} // fall through

				case crClip::kClipTypeAnimation:
					{
						const crClipAnimation* clipAnim = static_cast<const crClipAnimation*>(localClip);
						float start = clipAnim->GetStartTime();
						float end = clipAnim->GetEndTime();
						float time = Clamp(start + clipAnim->GetRate() * playerTime, start, end);
						float delta = clipAnim->GetRate() * playerDelta;

						const crAnimation* anim = clipAnim->GetAnimation();
						if(anim)
						{
							PushAnimation(anim, time, delta, start, end, sizeIndices, indices);
						}
						else
						{
							PushInvalid();
						}
						break;
					}

				case crClip::kClipTypeAnimations:
					{
						const crClipAnimations* clipAnims = static_cast<const crClipAnimations*>(localClip);
						unsigned int numAnimations = clipAnims->GetNumAnimations();
						const crClipAnimations::Animation* anims = clipAnims->GetAnimations();
						unsigned int indiceOffset = 0;

						for(unsigned int i=0; i < numAnimations; i++)
						{
							const crClipAnimations::Animation* localAnim = static_cast<const crClipAnimations::Animation*>(m_Scratch.Get(anims+i, sizeof(crClipAnimations::Animation)));
							const crAnimation* anim = localAnim->GetAnimation();
							if(anim)
							{
								float start, end;
								localAnim->GetStartEndTimes(start, end);
								float time = Clamp(start + localAnim->GetRate() * playerTime, start, end);
								float delta = localAnim->GetRate() * playerDelta;
								indiceOffset = PushAnimation(anim, time, delta, start, end, sizeIndices, indices, indiceOffset);
							}
						}

						if(!indiceOffset)
						{
							PushInvalid();
						}
						break;
					}

				default: Assertf(false, "Unsupported clip type %u", clipType);
				}
			}
			break;
		}

	case crmtNode::kNodeFrame:
		{
			const crmtNodeFrame& nodeFrame = static_cast<const crmtNodeFrame&>(node);
			const crFrame* frame = nodeFrame.GetFrame();
			if(frame)
			{
				PushFrame(frame, nodeFrame.GetIndices());
			}
			else
			{
				PushInvalid();
			}
			break;
		}

	case crmtNode::kNodeBlend:
	case crmtNode::kNodeAddSubtract:
		{
			const crmtNodePairWeighted& nodePair = static_cast<const crmtNodePairWeighted&>(node);
			const crLock& weights = nodePair.GetWeights();
			bool mergeBlend = nodeType==crmtNode::kNodeBlend ? static_cast<const crmtNodeBlend&>(node).IsMergeBlend() : false;
			EPairType pairType = nodeType==crmtNode::kNodeBlend ? kPairBlend : kPairAdd;
			PushPair(pairType, nodePair.GetWeight(), mergeBlend, weights.Size(), weights.Get());
			break;
		}

	case crmtNode::kNodeMerge:
		{
			const crmtNodeMerge& nodeMerge = static_cast<const crmtNodeMerge&>(node);
			const crLock& weights = nodeMerge.GetWeights();
			if(weights.Get())
			{
				PushFilter(weights);
			}
			PushPair(kPairMerge, 0.f, false);
			break;
		}

	case crmtNode::kNodeBlendN:
	case crmtNode::kNodeMergeN:
	case crmtNode::kNodeAddN:
		{
			const crmtNodeN& nodeN = static_cast<const crmtNodeN&>(node);
			bool zeroDest = nodeN.IsZeroDestination();
			EPairType pairType = nodeType==crmtNode::kNodeBlendN ? kPairBlend : (nodeType==crmtNode::kNodeMergeN?kPairMerge:kPairAdd);

			// gather weights and filters
			u32 n = 0;
			u32 numChildren = nodeN.crmtNodeN::GetNumChildren();
			bool hasFilters = nodeN.GetFilter()!=NULL;
			f32 weights[crmtNodeN::sm_MaxChildren];
			const crmtNodeN::NData* pData = nodeN.GetData();
			const crmtNode* child = nodeN.GetFirstChild();
			for(u32 i=0; i < numChildren; i++)
			{
				const crmtNode* localChild = static_cast<const crmtNode*>(m_Scratch.Get(child, sizeof(crmtNode)));
				if(!localChild->IsDisabled() || !localChild->IsSilent())
				{
					weights[n] = pData->m_Weight;
					hasFilters |= pData->m_InputFilter!=NULL;
					++n;
				}
				pData++;
				child = localChild->GetNextSibling();
			}

			// push operations
			u32 count = n;
			if(!zeroDest)
			{
				PushInvalid();
				++count;
			}

			// if no filter split in 2-way else go on PPU
			if(!hasFilters)
			{
				FixWeightsReverse(DestWeight(weights, n), weights, n);
				for(s32 i=count-2; i>=0; --i)
				{
					PushPair(pairType, weights[i], true);
				}
			}
			else if(count >= 2)
			{
				PushNodeN(refNode, count);
			}
			break;
		}

	case crmtNode::kNodeExpression:
		{
			const crmtNodeExpression& nodeExpression = static_cast<const crmtNodeExpression&>(node);
			const crLock& frameIndices = nodeExpression.GetFrameIndices();
			const crLock& skelIndices = nodeExpression.GetSkelIndices();
			const crExpressionPlayer& player = nodeExpression.GetExpressionPlayer();
			const crExpressions* expressions = player.GetExpressions();
			f32 weight = nodeExpression.GetWeight();
			if(!IsNearZero(weight) && player.IsEnabled() && expressions)
			{
				PushExpressions(expressions, player.GetTime(), player.GetDeltaTime(), weight, player.GetVariablesPtr(), player.GetNumVariables(), frameIndices, skelIndices);
			}
			break;
		}

	case crmtNode::kNodeFilter:
		{
			const crmtNodeFilter& nodeFilter = static_cast<const crmtNodeFilter&>(node);
			const crLock& weights = nodeFilter.GetWeights();
			if(weights.Get())
			{
				PushFilter(weights);
			}
			break;
		}

	case crmtNode::kNodeCapture:
		{
			const crmtNodeCapture& nodeCapture = static_cast<const crmtNodeCapture&>(node);
			Assert(nodeCapture.HasChildren());
			crFrame* frame = nodeCapture.GetFrame();
			if(frame)
			{
				PopFrame(frame, nodeCapture.GetIndices());
			}
			break;
		}

	case crmtNode::kNodeIk:
		{
			const crmtNodeIk& nodeIk = static_cast<const crmtNodeIk&>(node);
			crKinematics* kinematics = nodeIk.GetKinematics();
			if(kinematics)
			{
				const crKinematics* localKinematics = static_cast<const crKinematics*>(m_Scratch.Get(kinematics, sizeof(crKinematics)));
				u32 numSolvers = localKinematics->GetNumSolvers();
				if(numSolvers!=0)
				{
					PopSkeleton(false);
					PushIK(numSolvers, kinematics->GetSolvers());
					PushSkeleton(false, m_Data.m_SkelIndices);
				}
			}
			break;
		}

	case crmtNode::kNodePose:
		{
			const crmtNodePose& nodePose = static_cast<const crmtNodePose&>(node);
			if(m_Data.m_ExtraDofsFrame)
			{
				PushFrame(m_Data.m_ExtraDofsFrame, m_Data.m_ExtraDofInverseIndices);
			}
			else
			{
				PushInvalid();
			}
			PushSkeleton(m_Data.m_SuppressReset, nodePose.GetIndices());
		}
		break;

	case crmtNode::kNodeIdentity:
		PushIdentity();
		break;

	case crmtNode::kNodeProxy:
		{
			const crmtNodeProxy& nodeProxy = static_cast<const crmtNodeProxy&>(node);
			if(!nodeProxy.GetProxyNode())
			{
				PushInvalid();
			}
			break;
		}

	case crmtNode::kNodeExtrapolate:
		{
			const crmtNodeExtrapolate& nodeExtrapolate = static_cast<const crmtNodeExtrapolate&>(node);
			PushExtrapolate(nodeExtrapolate.GetDamping(), nodeExtrapolate.GetMaxAngularVelocity(), nodeExtrapolate.GetLastFrame(), nodeExtrapolate.GetDeltaFrame());
			break;
		}

	case crmtNode::kNodePm:
		{
			const crmtNodePm& nodePm = static_cast<const crmtNodePm&>(node);
			if(nodePm.GetParameterizedMotionPlayer().GetParameterizedMotion())
			{
				PushNodePm(refNode);
			}
			else
			{
				PushInvalid();
			}
			break;
		}

	case crmtNode::kNodeMirror:
		PushNodeMirror(refNode);
		break;

	case crmtNode::kNodeInvalid:
		PushInvalid();
		break;

	default: Assertf(false, "Unsupported node type %u", nodeType);
	}
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushProxyNode(const crmtNode* nodeRef)
{
	u32 proxyIdx = m_NumProxies;
	if(Likely(proxyIdx < sm_MaxNumProxies))
	{
		// create dependency to copy current frame
		sysDependency& dep = m_Dependencies[m_NumDeps++];
		dep.Init(FRAG_SPU_CODE(dependency_proxy), kTagProxy);
		dep.AddChild(*m_DepStack[m_TopDep]);
		m_DepStack[m_TopDep] = &dep;

		// cache the result
		m_ProxyNodes[proxyIdx] = nodeRef;
		m_ProxyDeps[proxyIdx] = &dep;
		m_ProxyFrames[proxyIdx] = m_FrameStack[m_TopFrame];
		m_NumProxies++;
	}
	else
	{
		crmtWarning("exceed maximum number of proxied nodes (%u)", m_NumProxies);
	}
}

///////////////////////////////////////////////////////////////////////

bool crmtComposer::PopProxyNode(const crmtNode* nodeRef)
{
	// linear search for the proxy
	u32 numProxies = m_NumProxies;
	for(u32 i=0; i < numProxies; i++)
	{
		if(m_ProxyNodes[i] != nodeRef)
			continue;

		// for the first proxy, copy the current frame
		u32 destFrame = m_NumFrames++;
		u32 srcFrame = m_ProxyFrames[i];
		u32 frameSize = m_FrameData.GetFrameBufferSize();
		spuCodeFragment cb = FRAG_SPU_CODE(dependency_frame);
		sysDependency& depFrame = *m_ProxyDeps[i];
		depFrame.m_Id = kTagFrame;
		depFrame.m_Chain = SYS_USE_SPU_DEPENDENCY ? sysDependency::kChainSpu : sysDependency::kChainPpu;
		depFrame.m_Callback = reinterpret_cast<sysDependency::Callback*>(cb.pCodeFrag);
		Assign(depFrame.m_CodeSize, cb.codeFragSize);
		depFrame.m_Flags = INPUT1|OUTPUT0|FRAME0|FRAME1;
		depFrame.m_Params[0].m_AsInt = destFrame;
		depFrame.m_Params[1].m_AsInt = srcFrame;
		depFrame.m_Params[2].m_AsConstPtr = NULL;
		depFrame.m_Params[3].m_AsBool = true;
		depFrame.m_DataSizes[0] = frameSize;
		depFrame.m_DataSizes[1] = frameSize;
		m_FrameAllocators[destFrame] = &depFrame;
		m_ProxyFrames[i] = destFrame;
		m_FrameStack[++m_TopFrame] = destFrame;

		// for each proxy, create a new dependency 
		sysDependency& depProxy = m_Dependencies[m_NumDeps++];
		depProxy.Init(FRAG_SPU_CODE(dependency_proxy), kTagProxy);
		depProxy.AddWeakChild(depFrame);
		m_DepStack[++m_TopDep] = &depProxy;
		m_ProxyDeps[i] = &depProxy;
		return true;
	}
	return false;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushInvalid()
{
	u32 destFrame = m_NumFrames++;
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_invalid), kTagInvalid, OUTPUT0|FRAME0);
	dep.m_Params[0].m_AsInt = destFrame;
	dep.m_DataSizes[0] = m_FrameData.GetFrameBufferSize();
	m_FrameAllocators[destFrame] = &dep;
	m_FrameStack[++m_TopFrame] = destFrame;
	m_DepStack[++m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

u32 crmtComposer::PushAnimation(const crAnimation* anim, f32 time, f32 delta, f32 rangeStart, f32 rangeEnd, u32 sizeIndices, const u8* indices, u32 indiceOffset)
{
	const crAnimation* localAnim = static_cast<const crAnimation*>(m_Scratch.Get(anim, sizeof(crAnimation)));
	Assertf(localAnim->IsPacked(), "Can't push unpacked animation");
	Assertf(localAnim->GetNumBlocks(), "Can't push empty animation");
	time = Min(time, localAnim->GetDuration());
	f32 internalFrame = localAnim->ConvertTimeToInternalFrame(time);
	u32 blockIdx = localAnim->ConvertInternalFrameToBlockIndex(internalFrame);
	f32 blockTime = localAnim->ConvertInternalFrameToBlockTime(internalFrame, blockIdx);
	f32 interval = localAnim->ConvertTimeToInternalFrame(delta);
	bool calcMover = localAnim->HasMoverTracks();
	rangeEnd = Min(rangeEnd, localAnim->GetDuration());
	rangeStart = Min(rangeStart, rangeEnd);
	u32 nextOffset = indiceOffset + localAnim->GetNumDofs() + 1;
	bool singleBlock = (localAnim->GetNumBlocks() == 1) || (blockIdx == localAnim->ConvertTimeToBlockIndex(time-delta) && InRange(time-delta, rangeStart, rangeEnd)) || (blockIdx == localAnim->ConvertTimeToBlockIndex(rangeStart) && blockIdx == localAnim->ConvertTimeToBlockIndex(rangeEnd));
	f32 rangeStartBlockTime = localAnim->ConvertTimeToBlockTime(rangeStart, blockIdx);
	f32 rangeEndBlockTime = localAnim->ConvertTimeToBlockTime(rangeEnd, blockIdx);
	// Assertf(rangeEndBlockTime-rangeStartBlockTime > 0.f, "The range duration can be zero when we are at the beginning of a block");
	const crBlockStream* block = reinterpret_cast<const crBlockStream*>(sysMemGetPtr(uptr(localAnim->GetBlocks() + blockIdx), 0));
	const crBlockStream* localBlock = static_cast<const crBlockStream*>(m_Scratch.Get(block, sizeof(crBlockStream)));
	u32 destFrame = indiceOffset ? m_NumFrames-1 : m_NumFrames++;
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_animation), kTagAnimation, INPUT1|INPUT2|OUTPUT0|FRAME0);
	dep.m_Params[0].m_AsInt = destFrame;
	dep.m_Params[1].m_AsConstPtr = block;
	dep.m_Params[2].m_AsConstPtr = indices;
	dep.m_Params[3].m_AsFloat = blockTime;
	dep.m_Params[4].m_AsFloat = interval;
	dep.m_Params[5].m_AsBool = calcMover && singleBlock;
	dep.m_Params[6].m_AsShort.m_Low = m_Data.m_MoverTranslationOffset;
	dep.m_Params[6].m_AsShort.m_High = m_Data.m_MoverRotationOffset;
	dep.m_Params[7].m_AsFloat = rangeStartBlockTime;
	dep.m_Params[8].m_AsFloat = rangeEndBlockTime;
	dep.m_Params[9].m_AsInt = indiceOffset;
	dep.m_DataSizes[0] = m_FrameData.GetFrameBufferSize();
	dep.m_DataSizes[1] = localBlock->GetBlockSize() + localBlock->m_SlopSize;
	dep.m_DataSizes[2] = sizeIndices;

	if(indiceOffset)
	{
		dep.m_Flags |= INPUT0;
		dep.AddChild(*m_DepStack[m_TopDep]);
		m_DepStack[m_TopDep] = &dep;
	}
	else
	{
		m_FrameAllocators[destFrame] = &dep;
		m_FrameStack[++m_TopFrame] = destFrame;
		m_DepStack[++m_TopDep] = &dep;
	}
	
	if(calcMover && !singleBlock) // fall back mover computation on PPU if evaluation requires multi-block
	{
		sysDependency& depMover = m_Dependencies[m_NumDeps++];
		depMover.Init(m_Data.m_Callback, kTagMoverDelta, FRAME1);
		depMover.AddChild(*m_DepStack[m_TopDep]);
		depMover.m_Params[0].m_AsInt = kComposerCallbackMoverDelta;
		depMover.m_Params[1].m_AsInt = destFrame;
		depMover.m_Params[2].m_AsConstPtr = m_Data.m_MotionTree;
		depMover.m_Params[3].m_AsConstPtr = anim;
		depMover.m_Params[4].m_AsFloat = time;
		depMover.m_Params[5].m_AsFloat = delta;
		depMover.m_Params[6].m_AsFloat = rangeStart;
		depMover.m_Params[7].m_AsFloat = rangeEnd;
		m_DepStack[m_TopDep] = &depMover;
	}
	return nextOffset;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushPair(EPairType pairType, f32 weight, bool merge, u32 filterSize, const u8* filter)
{
	const spuCodeFragment fragments[] = {FRAG_SPU_CODE(dependency_blend), FRAG_SPU_CODE(dependency_merge), FRAG_SPU_CODE(dependency_add)};
	const u8 names[] = {kTagBlend, kTagMerge, kTagAdd};
	u32 topDep = m_TopDep, topFrame = m_TopFrame;
	const crFrameData& frameData = m_FrameData;
	u32 srcFrame = m_FrameStack[topFrame];
	u32 destFrame = m_FrameStack[topFrame-1];
	u32 frameSize = frameData.GetFrameBufferSize();
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(fragments[pairType], names[pairType], INPUT0|INPUT1|INPUT2|OUTPUT0|FRAME0|FRAME1);
	dep.AddChild(*m_DepStack[topDep]);
	dep.AddChild(*m_DepStack[topDep-1]);
	dep.m_Params[0].m_AsInt = destFrame;
	dep.m_Params[1].m_AsInt = srcFrame;
	dep.m_Params[2].m_AsConstPtr = filter;
	dep.m_Params[3].m_AsFloat = weight;
	dep.m_Params[4].m_AsInt = frameData.GetNumDofsType(kFormatTypeVector3);
	dep.m_Params[5].m_AsInt = frameData.GetNumDofsType(kFormatTypeQuaternion);
	dep.m_Params[6].m_AsInt = frameData.GetNumDofsType(kFormatTypeFloat);
	dep.m_Params[7].m_AsBool = merge;
	dep.m_DataSizes[0] = frameSize;
	dep.m_DataSizes[1] = frameSize;
	dep.m_DataSizes[2] = filterSize;
	m_FrameReleasers[srcFrame] = &dep;
	m_DepStack[topDep-1] = &dep;
	m_TopDep = topDep-1;
	m_TopFrame = topFrame-1;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PopSkeleton(bool suppressReset)
{
	u32 numBones = m_Data.m_NumBones;
	u32 srcFrame = m_FrameStack[m_TopFrame];
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_pose), kTagPose, INPUT0|INPUT1|INPUT2|INPUT3|INPUT4|OUTPUT4|FRAME0);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsInt = srcFrame;
	dep.m_Params[1].m_AsConstPtr = m_Data.m_SkelIndices.Get();
	dep.m_Params[2].m_AsConstPtr = m_Data.m_ChildParentIndices;
	dep.m_Params[3].m_AsConstPtr = m_Data.m_Defaults;
	dep.m_Params[4].m_AsPtr = m_Data.m_Locals;
	dep.m_Params[5].m_AsInt = numBones;
	dep.m_Params[6].m_AsInt = m_Data.m_NumChildParents;
	dep.m_Params[7].m_AsBool = false;
	dep.m_Params[8].m_AsBool = suppressReset;
	dep.m_DataSizes[0] = m_FrameData.GetFrameBufferSize();
	dep.m_DataSizes[1] = m_Data.m_SkelIndices.Size();
	dep.m_DataSizes[2] = m_Data.m_NumChildParents*sizeof(u16);
	dep.m_DataSizes[3] = numBones*sizeof(Mat34V);
	dep.m_DataSizes[4] = 2*numBones*sizeof(Mat34V);
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushSkeleton(bool localPoseOnly, const crLock& indices)
{
	u32 numBones = m_Data.m_NumBones;
	u32 destFrame = m_FrameStack[m_TopFrame];
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_inversepose), kTagInversePose, INPUT0|INPUT1|INPUT2|INPUT3|OUTPUT0|FRAME0);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsInt = destFrame;
	dep.m_Params[1].m_AsConstPtr = indices.Get();
	dep.m_Params[2].m_AsConstPtr = m_Data.m_ParentIndices;
	dep.m_Params[3].m_AsPtr = m_Data.m_Locals;
	dep.m_Params[4].m_AsInt = numBones;
	dep.m_Params[5].m_AsBool = localPoseOnly;
	dep.m_Params[6].m_AsBool = true;
	dep.m_DataSizes[0] = m_FrameData.GetFrameBufferSize();
	dep.m_DataSizes[1] = indices.Size();
	dep.m_DataSizes[2] = numBones*sizeof(s16);
	dep.m_DataSizes[3] = 2*numBones*sizeof(Mat34V);
	m_DepStack[m_TopDep] = &dep;
}
///////////////////////////////////////////////////////////////////////

void crmtComposer::PoseBounds(bool copyLastMatrices)
{
	u32 numBones = m_Data.m_NumBones;
	u32 numBounds = m_Data.m_Fragment.m_NumBounds;
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(dependency_pose_bounds, kTagPopBounds);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsPtr = m_Data.m_Fragment.m_BoundComposite;
	dep.m_Params[1].m_AsConstPtr = m_Data.m_Fragment.m_LocalBoxMinMaxs;
	dep.m_Params[2].m_AsConstPtr = m_Data.m_Fragment.m_Children;
	dep.m_Params[3].m_AsConstPtr = m_Data.m_Fragment.m_BoundIndices;
	dep.m_Params[4].m_AsConstPtr = m_Data.m_Locals + numBones;
	dep.m_Params[5].m_AsPtr = m_Data.m_Fragment.m_LastMatrices;
	dep.m_Params[6].m_AsPtr = m_Data.m_Fragment.m_CurrentMatrices;
	dep.m_Params[7].m_AsInt = numBounds;
	dep.m_Params[8].m_AsBool = copyLastMatrices;
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushShaderVars()
{
	u32 srcFrame = m_FrameStack[m_TopFrame];
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_shadervars), kTagShaderVars, INPUT0|INPUT1|INPUT2|OUTPUT2|FRAME0);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsInt = srcFrame;
	dep.m_Params[1].m_AsConstPtr = m_FrameData.GetBuffer();
	dep.m_Params[2].m_AsPtr = m_Data.m_ShaderVars;
	dep.m_Params[3].m_AsInt = m_FrameData.GetNumDofs();
	dep.m_Params[4].m_AsInt = m_Data.m_NumShaderVars;
	dep.m_DataSizes[0] = m_FrameData.GetFrameBufferSize();
	dep.m_DataSizes[1] = m_FrameData.GetNumDofs()*sizeof(crFrameData::Dof);
	dep.m_DataSizes[2] = m_Data.m_NumShaderVars*sizeof(crCreatureComponentShaderVars::ShaderVarDofPair);
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushExpressions(const crExpressions* expressions, f32 time, f32 deltaTime, f32 weight, const Vec4V* variables, u32 numVariables, const crLock& frameIndices, const crLock& skelIndices)
{
	sysDependency* topDep = m_DepStack[m_TopDep];
	const crFrameData& frameData = m_FrameData;
	u32 frameSize = frameData.GetFrameBufferSize();
	bool blend = !IsClose(weight, 1.f);
	if(blend)
	{
		sysDependency& dep = m_Dependencies[m_NumDeps++];
		u32 srcFrame = m_FrameStack[m_TopFrame];
		u32 destFrame = m_NumFrames++;
		dep.Init(FRAG_SPU_CODE(dependency_frame), kTagFrame, INPUT1|OUTPUT0|FRAME0|FRAME1);
		dep.AddChild(*topDep);
		dep.m_Params[0].m_AsInt = destFrame;
		dep.m_Params[1].m_AsInt = srcFrame;
		dep.m_Params[2].m_AsConstPtr = NULL;
		dep.m_Params[3].m_AsBool = true;
		dep.m_DataSizes[0] = frameSize;
		dep.m_DataSizes[1] = frameSize;
		m_FrameAllocators[destFrame] = &dep;
		m_FrameStack[++m_TopFrame] = destFrame;
		topDep = &dep;
	}

	u32 topFrame = m_FrameStack[m_TopFrame];
#if CR_TEST_EXPRESSIONS
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(m_Data.m_Callback, kTagExpression, FRAME1);
	dep.AddChild(*topDep);
	dep.m_Params[0].m_AsInt = kComposerCallbackNodeExpressions;
	dep.m_Params[1].m_AsInt = topFrame;
	dep.m_Params[2].m_AsConstPtr = m_Data.m_MotionTree;
	dep.m_Params[3].m_AsConstPtr = expressions;
	dep.m_Params[4].m_AsFloat = time;
	dep.m_Params[5].m_AsConstPtr = frameIndices.Get();
	dep.m_Params[6].m_AsConstPtr = skelIndices.Get();
	dep.m_Params[7].m_AsConstPtr = variables;
	dep.m_Params[8].m_AsInt = numVariables;
	dep.m_Params[9].m_AsFloat = deltaTime;
	dep.m_DataSizes[5] = frameIndices.Size();
	dep.m_DataSizes[6] = skelIndices.Size();
	topDep = &dep;
#else
	const crExpressions* localExpressions = static_cast<const crExpressions*>(m_Scratch.Get(expressions, sizeof(crExpressions)));
	crExpressionStream*const* streams = localExpressions->GetExpressionStreams();
	u32 numExpressions = localExpressions->GetNumExpressionStreams();
	for(u32 i=0; i < numExpressions; ++i)
	{
		const crExpressionStream* stream = reinterpret_cast<const crExpressionStream*>(sysMemGetPtr(uptr(streams + i), 0));
		const crExpressionStream* localStream = static_cast<const crExpressionStream*>(m_Scratch.Get(stream, sizeof(crExpressionStream)));
		sysDependency& dep = m_Dependencies[m_NumDeps++];
		dep.Init(FRAG_SPU_CODE(dependency_expression), kTagExpression, INPUT0|INPUT1|INPUT2|INPUT3|INPUT4|INPUT5|INPUT6|OUTPUT0|OUTPUT6|FRAME0);
		dep.AddChild(*topDep);
		dep.m_Params[0].m_AsInt = topFrame;
		dep.m_Params[1].m_AsConstPtr = frameData.GetBuffer();
		dep.m_Params[2].m_AsConstPtr = m_Data.m_Bones;
		dep.m_Params[3].m_AsConstPtr = stream;
		dep.m_Params[4].m_AsConstPtr = frameIndices.Get();
		dep.m_Params[5].m_AsConstPtr = skelIndices.Get();
		dep.m_Params[6].m_AsConstPtr = numVariables?variables:NULL;
		dep.m_Params[7].m_AsFloat = time;
		dep.m_Params[8].m_AsFloat = deltaTime;
		dep.m_DataSizes[0] = frameSize;
		dep.m_DataSizes[1] = frameData.GetNumDofs()*sizeof(crFrameData::Dof);
		dep.m_DataSizes[2] = m_Data.m_NumBones*sizeof(crBoneData);
		dep.m_DataSizes[3] = localStream->GetSize();
		dep.m_DataSizes[4] = frameIndices.Size();
		dep.m_DataSizes[5] = skelIndices.Size();
		dep.m_DataSizes[6] = numVariables * sizeof(Vector_4V);
		topDep = &dep;
	}
#endif // CR_TEST_EXPRESSIONS

	if(blend)
	{
		sysDependency& dep = m_Dependencies[m_NumDeps++];
		u32 srcFrame = m_FrameStack[m_TopFrame--];
		u32 destFrame = m_FrameStack[m_TopFrame];
		dep.Init(FRAG_SPU_CODE(dependency_blend), kTagBlend, INPUT0|INPUT1|OUTPUT0|FRAME0|FRAME1);
		dep.AddChild(*topDep);
		dep.m_Params[0].m_AsInt = destFrame;
		dep.m_Params[1].m_AsInt = srcFrame;
		dep.m_Params[2].m_AsConstPtr = NULL;
		dep.m_Params[3].m_AsFloat = weight;
		dep.m_Params[4].m_AsInt = frameData.GetNumDofsType(kFormatTypeVector3);
		dep.m_Params[5].m_AsInt = frameData.GetNumDofsType(kFormatTypeQuaternion);
		dep.m_Params[6].m_AsInt = frameData.GetNumDofsType(kFormatTypeFloat);
		dep.m_Params[7].m_AsBool = true;
		dep.m_DataSizes[0] = frameSize;
		dep.m_DataSizes[1] = frameSize;
		m_FrameReleasers[srcFrame] = &dep;
		topDep = &dep;
	}

	m_DepStack[m_TopDep] = topDep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushFilter(const crLock& weights)
{
	const crFrameData& frameData = m_FrameData;
	u32 destFrame = m_FrameStack[m_TopFrame];
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_filter), kTagFilter, INPUT0|INPUT1|OUTPUT0|FRAME0);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsInt = destFrame;
	dep.m_Params[1].m_AsConstPtr = weights.Get();
	dep.m_Params[2].m_AsInt = frameData.GetNumDofsType(kFormatTypeVector3);
	dep.m_Params[3].m_AsInt = frameData.GetNumDofsType(kFormatTypeQuaternion);
	dep.m_Params[4].m_AsInt = frameData.GetNumDofsType(kFormatTypeFloat);
	dep.m_DataSizes[0] = frameData.GetFrameBufferSize();
	dep.m_DataSizes[1] = weights.Size();
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushNormalize()
{
	const crFrameData& frameData = m_FrameData;
	u32 destFrame = m_FrameStack[m_TopFrame];
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_normalize), kTagNormalize, INPUT0|OUTPUT0|FRAME0);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsInt = destFrame;
	dep.m_Params[1].m_AsInt = frameData.GetNumDofsType(kFormatTypeVector3);
	dep.m_Params[2].m_AsInt = frameData.GetNumDofsType(kFormatTypeQuaternion);
	dep.m_DataSizes[0] = frameData.GetFrameBufferSize();
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushFrame(const crFrame* srcFrame, const crLock& indices)
{
	const crFrame* localFrame = static_cast<const crFrame*>(m_Scratch.Get(srcFrame, sizeof(crFrame)));
	bool identical = localFrame->GetSignature() == m_FrameData.GetSignature();
	u32 destFrame = m_NumFrames++;
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_frame), kTagFrame, INPUT1|INPUT2|OUTPUT0|FRAME0);
	dep.m_Params[0].m_AsInt = destFrame;
	dep.m_Params[1].m_AsConstPtr = localFrame->GetBuffer();
	dep.m_Params[2].m_AsConstPtr = indices.Get();
	dep.m_Params[3].m_AsBool = identical;
	dep.m_DataSizes[0] = m_FrameData.GetFrameBufferSize();
	dep.m_DataSizes[1] = localFrame->GetBufferSize();
	dep.m_DataSizes[2] = indices.Size();
	m_FrameAllocators[destFrame] = &dep;
	m_FrameStack[++m_TopFrame] = destFrame;
	m_DepStack[++m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PopFrame(crFrame* destFrame, const crLock& indices)
{
	crFrame* localFrame = static_cast<crFrame*>(m_Scratch.Get(destFrame, sizeof(crFrame)));
	bool identical = localFrame->GetSignature() == m_FrameData.GetSignature();
	u32 srcFrame = m_FrameStack[m_TopFrame];
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_frame), kTagFrame, INPUT1|INPUT2|OUTPUT0|FRAME1);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsPtr = localFrame->GetBuffer();
	dep.m_Params[1].m_AsInt = srcFrame;
	dep.m_Params[2].m_AsConstPtr = indices.Get();
	dep.m_Params[3].m_AsBool = identical;
	dep.m_DataSizes[0] = localFrame->GetBufferSize();
	dep.m_DataSizes[1] = m_FrameData.GetFrameBufferSize();
	dep.m_DataSizes[2] = indices.Size();
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushIdentity()
{
	u32 destFrame = m_NumFrames++;
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_identity), kTagIdentity, INPUT1|INPUT2|OUTPUT0|FRAME0);
	dep.m_Params[0].m_AsInt = destFrame;
	dep.m_Params[1].m_AsConstPtr = m_Data.m_SkelIndices.Get();
	dep.m_Params[2].m_AsConstPtr = m_Data.m_Bones;
	dep.m_DataSizes[0] = m_FrameData.GetFrameBufferSize();
	dep.m_DataSizes[1] = m_Data.m_SkelIndices.Size();
	dep.m_DataSizes[2] = m_Data.m_NumBones*sizeof(crBoneData);
	m_FrameAllocators[destFrame] = &dep;
	m_FrameStack[++m_TopFrame] = destFrame;
	m_DepStack[++m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushIK(u32 numSolvers, crIKSolverBase** solvers)
{
	const u32 maxSolverSize = MAX_RAGE_SOLVER_SIZE;
	u32 numBones = m_Data.m_NumBones;
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_ik), kTagIk, ALLOC0|INPUT1|INPUT2|INPUT3|INPUT4|OUTPUT4);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsPtr = NULL;
	dep.m_Params[1].m_AsConstPtr = solvers;
	dep.m_Params[2].m_AsConstPtr = m_Data.m_ParentIndices;
	dep.m_Params[3].m_AsConstPtr = m_Data.m_Parent;
	dep.m_Params[4].m_AsPtr = m_Data.m_Locals;
	dep.m_Params[5].m_AsInt = numBones;
	dep.m_Params[6].m_AsInt = numSolvers;
	dep.m_DataSizes[0] = numSolvers*maxSolverSize;
	dep.m_DataSizes[1] = numSolvers*sizeof(crIKSolverBase*);
	dep.m_DataSizes[2] = numBones*sizeof(s16);
	dep.m_DataSizes[3] = sizeof(Mat34V);
	dep.m_DataSizes[4] = 2*numBones*sizeof(Mat34V);
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushPhysical()
{
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_physical), kTagPhysical, INPUT0|INPUT1|INPUT2|INPUT3|OUTPUT1|OUTPUT2);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsPtr = m_Data.m_Motions;
	dep.m_Params[1].m_AsConstPtr = m_Data.m_Locals;
	dep.m_Params[2].m_AsConstPtr = m_Data.m_Locals + m_Data.m_NumBones;
	dep.m_Params[3].m_AsConstPtr = m_Data.m_Parent;
	dep.m_Params[4].m_AsFloat = m_Data.m_DeltaTime;
	dep.m_Params[5].m_AsInt = m_Data.m_NumMotions;
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushExtrapolate(float damping, float maxAngularVelocity, crFrame* lastFrame, crFrame* deltaFrame)
{
	const crFrameData& frameData = m_FrameData;
	u32 frameSize = frameData.GetFrameBufferSize();
	u32 destFrame = m_FrameStack[m_TopFrame];
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(FRAG_SPU_CODE(dependency_extrapolate), kTagExtrapolate, INPUT0|OUTPUT0|INPUT1|OUTPUT1|INPUT2|OUTPUT2|FRAME0);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsInt = destFrame;
	dep.m_Params[1].m_AsPtr = reinterpret_cast<void*>(sysMemGetPtr(uptr(lastFrame), 0));
	dep.m_Params[2].m_AsPtr = reinterpret_cast<void*>(sysMemGetPtr(uptr(deltaFrame), 0));
	dep.m_Params[3].m_AsInt = frameData.GetNumDofsType(kFormatTypeVector3);
	dep.m_Params[4].m_AsInt = frameData.GetNumDofsType(kFormatTypeQuaternion);
	dep.m_Params[5].m_AsInt = frameData.GetNumDofsType(kFormatTypeFloat);
	dep.m_Params[6].m_AsFloat = pow(damping, m_Data.m_DeltaTime*30.f);
	dep.m_Params[7].m_AsFloat = maxAngularVelocity;
	dep.m_DataSizes[0] = frameSize;
	dep.m_DataSizes[1] = frameSize;
	dep.m_DataSizes[2] = frameSize;
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushNodePm(const crmtNode& node)
{
	u32 destFrame = m_NumFrames++;
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(m_Data.m_Callback, kTagNodePm, FRAME1);
	dep.m_Params[0].m_AsInt = kComposerCallbackNodePm;
	dep.m_Params[1].m_AsInt = destFrame;
	dep.m_Params[2].m_AsConstPtr = m_Data.m_MotionTree;
	dep.m_Params[3].m_AsConstPtr = &node;
	m_FrameAllocators[destFrame] = &dep;
	m_FrameStack[++m_TopFrame] = destFrame;
	m_DepStack[++m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushNodeMirror(const crmtNode& node)
{
	u32 destFrame = m_FrameStack[m_TopFrame];
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(m_Data.m_Callback, kTagNodeMirror, FRAME1);
	dep.AddChild(*m_DepStack[m_TopDep]);
	dep.m_Params[0].m_AsInt = kComposerCallbackNodeMirror;
	dep.m_Params[1].m_AsInt = destFrame;
	dep.m_Params[2].m_AsConstPtr = m_Data.m_MotionTree;
	dep.m_Params[3].m_AsConstPtr = &node;
	m_DepStack[m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

void crmtComposer::PushNodeN(const crmtNode& node, u32 numFrames)
{
	sysDependency& dep = m_Dependencies[m_NumDeps++];
	dep.Init(m_Data.m_Callback, kTagNodeN, FRAME1);
	dep.m_Params[0].m_AsInt = kComposerCallbackNodeN;
	dep.m_Params[1].m_AsPtr = NULL;
	dep.m_Params[2].m_AsConstPtr = m_Data.m_MotionTree;
	dep.m_Params[3].m_AsConstPtr = &node;
	dep.m_Params[4].m_AsInt = numFrames;
	u8* frameStack = reinterpret_cast<u8*>(&dep.m_Params[4].m_AsInt);
	for(s32 i=numFrames-1; i>=0; i--)
	{
		u32 frameIdx = u32(m_FrameStack[m_TopFrame--]);
		Assert(frameIdx<=UCHAR_MAX);
		frameStack[i] = static_cast<u8>(frameIdx);
		dep.AddChild(*m_DepStack[m_TopDep--]);
		m_FrameReleasers[frameIdx] = &dep;
	}
	m_FrameStack[++m_TopFrame] = frameStack[numFrames-1];
	m_FrameReleasers[frameStack[numFrames-1]] = NULL;
	m_DepStack[++m_TopDep] = &dep;
}

///////////////////////////////////////////////////////////////////////

} // namespace rage
