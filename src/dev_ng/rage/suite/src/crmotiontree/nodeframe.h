//
// crmotiontree/nodeframe.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEFRAME_H
#define CRMOTIONTREE_NODEFRAME_H

#include "node.h"

namespace rage
{

class crFrame;

// PURPOSE: Uses an animation frame as an input source
class crmtNodeFrame : public crmtNode
{
public:

	// PURPOSE: Default constructor
	crmtNodeFrame();

	// PURPOSE: Resource constructor
	crmtNodeFrame(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeFrame();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeFrame);

	// PURPOSE: Initialization
	// PARAM: frame - pointer to an animation frame (can be NULL)
	void Init(const crFrame* frame);

	// PURPOSE: Update node
	void Update(float dt);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh();

	// PURPOSE: Set animation frame
	// PARAM: frame - pointer to an animation frame (can be NULL)
	void SetFrame(const crFrame* frame);


	// PURPOSE: Get animation frame
	// RETURNS: animation frame (can be NULL)
	const crFrame* GetFrame() const;

	// PURPOSE: Returns indices
	const crLock& GetIndices() const;

private:
	crLock m_Indices;
	const crFrame* m_Frame;
	u32 m_FrameSignature;
};

////////////////////////////////////////////////////////////////////////////////

inline const crFrame* crmtNodeFrame::GetFrame() const
{
	return m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodeFrame::GetIndices() const
{
	return m_Indices;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRMOTIONTREE_NODEFRAME_H
