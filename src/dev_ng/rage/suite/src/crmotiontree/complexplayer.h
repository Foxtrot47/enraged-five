//
// crmotiontree/complexplayer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_COMPLEXPLAYER_H
#define CRMOTIONTREE_COMPLEXPLAYER_H

#include "vectormath/mat34v.h"

namespace rage
{

class bkBank;
class crAnimation;
class crCreature;
class crFrameAccelerator;
class crFrameDataFactory;
class crSkeleton;
class crmtManagerSingleState;
class crmtMotionTree;

// PURPOSE: Complex player of multiple motion types.
// The complex player is a helper class, intended mainly for use in tools,
// it is _not_ primarily intended for runtime use.
// The player uses the motion tree and the single state manager,
// to enable the playback of a wide variety of different motion types.
class crmtComplexPlayer
{
public:

	// PURPOSE: Default constructor
	crmtComplexPlayer();

	// PURPOSE: Initializing constructor
	// SEE ALSO: Init()
	crmtComplexPlayer(crFrameDataFactory& factory, crFrameAccelerator& accelerator, crSkeleton& skeleton, Mat34V* moverMtx=NULL, crmtMotionTree* tree=NULL);

	// PURPOSE: Destructor
	~crmtComplexPlayer();

	// PURPOSE: Initialization
	// PARAMS: skeleton - reference to skeleton that will be used for playback
	// moverMtx - optional mover matrix (can be NULL)
	// tree - optional external motion tree (can be NULL, one will be automatically constructed)
	void Init(crFrameDataFactory& factory, crFrameAccelerator& accelerator, crSkeleton& skeleton, Mat34V* moverMtx=NULL, crmtMotionTree* tree=NULL);

	// PURPOSE: Shutdown, release dynamic allocation
	void Shutdown();

	// PURPOSE: Update player
	void Update(float deltaTime);

	// PURPOSE: Reset player to default state
	void Reset();


	// PURPOSE: Pause playback
	// PARAMS: paused - pause playback
	void SetPaused(bool paused);

	// PURPOSE: Is playback paused?
	// RETURNS: true - playback is paused
	bool IsPaused() const;


	// PURPOSE: Set playback rate
	// PARAMS: rate - playback rate
	void SetRate(float rate);

	// PURPOSE: Get playback rate
	// RETURNS: playback rate
	float GetRate() const;

	
	// PURPOSE: Set current time
	// PARAMS: time - in seconds
	void SetTime(float time);

	// PURPOSE: Get current time
	// RETURNS: time in seconds
	float GetTime() const;

	// PURPOSE: Get duration
	// RETURNS: duration in seconds
	float GetDuration() const;


	// PURPOSE: Play animation
	// PARAMS: anim - animation to play
	// crossfade - time in seconds to crossfade from current activity (default 0.0)
	void PlayAnimation(const crAnimation* anim, float crossfade=0.f);

	// PURPOSE: Set animation
	void SetAnimation(const crAnimation* anim);

	// PURPOSE: Get animation
	const crAnimation* GetAnimation() const;


	// TODO --- add support for clips, motion families etc

//	int GetPlayType() const; // TODO --- better name than "play type"

	// TODO --- query parameterization, ranges, dimensions, etc
	// TODO --- reset to origin
	// TODO --- allow movement from origin
	// TODO --- different modes: no root movement, no mover displacement,
	// TOOD ---                  reset to origin following loop
	// TOOD --- sequence play? Support something that generates callback on loop?


	// PURPOSE: Get skeleton
	crSkeleton& GetSkeleton() const;

	// PURPOSE: Get motion tree
	crmtMotionTree& GetMotionTree() const;

	// PURPOSE: Supported motion types
/*	enum
	{
		kPlayTypeNone,
		kPlayTypeAnimation,
		// TODO --- clips, motion families
	};
*/

#if __BANK
	// PURPOSE: Add widgets
	// PARAMS: bk - bank to add widgets to
	void AddWidgets(bkBank& bk);
#endif // __BANK

private:
	crCreature* m_Creature;
	crSkeleton* m_Skeleton;
	Mat34V* m_MoverMtx;

	crmtMotionTree* m_MotionTree;
	crmtManagerSingleState* m_Manager;
};


// inlines

////////////////////////////////////////////////////////////////////////////////

inline crSkeleton& crmtComplexPlayer::GetSkeleton() const
{
	FastAssert(m_Skeleton);
	return *m_Skeleton;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtMotionTree& crmtComplexPlayer::GetMotionTree() const
{
	FastAssert(m_MotionTree);
	return *m_MotionTree;
}

////////////////////////////////////////////////////////////////////////////////
/*
inline int crmtComplexPlayer::GetPlayType() const
{
	return kPlayTypeNone;  // TODO !!!!
}
*/
////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_COMPLEXPLAYER_H
