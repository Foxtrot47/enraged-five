//
// crmotiontree/dependency_extrapolate.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_EXTRAPOLATE_FRAG
#define MOTIONTREE_DEPENDENCY_EXTRAPOLATE_FRAG

#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"
#include "vectormath/vectormath.h"

using namespace rage;
using namespace Vec;

// dependency_extrapolate pseudo-code
//
// if(dest invalid && last valid)
//		dest = last + delta
//		delta = delta * damping
//	else
//		dest = dest
//		delta = dest - last		
//	last = dest

SPUFRAG_DECL(bool, dependency_extrapolate, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_extrapolate, const sysDependency& dep)
{
	crmtDebug3("dependency_extrapolate");

	Vector_4V* RESTRICT pDest = static_cast<Vector_4V*>(dep.m_Params[0].m_AsPtr);
	Vector_4V* RESTRICT pLast = static_cast<Vector_4V*>(dep.m_Params[1].m_AsPtr);
	Vector_4V* RESTRICT pDelta = static_cast<Vector_4V*>(dep.m_Params[2].m_AsPtr);
	unsigned int countVectors = RAGE_ALIGN(dep.m_Params[3].m_AsInt, 2);
	unsigned int countQuats = RAGE_ALIGN(dep.m_Params[4].m_AsInt, 2);
	unsigned int countFloats = RAGE_COUNT(dep.m_Params[5].m_AsInt, 2);
	Vector_4V damping = V4LoadScalar32IntoSplatted(dep.m_Params[6].m_AsFloat);
	Vector_4V maxAngular = V4LoadScalar32IntoSplatted(dep.m_Params[7].m_AsFloat);

	Vector_4V _invalid = V4VConstant(V_MASKXYZW);

	for(; countVectors; countVectors--)
	{
		Vector_4V dest = *pDest;
		Vector_4V last = *pLast;
		Vector_4V delta = *pDelta;

		Vector_4V destInvalid = V4IsEqualIntV(dest, _invalid);
		Vector_4V lastInvalid = V4IsEqualIntV(last, _invalid);
		Vector_4V deltaInvalid = V4IsEqualIntV(delta, _invalid);
		Vector_4V nextInvalid = V4Or(lastInvalid, deltaInvalid);
		Vector_4V diffInvalid = V4Or(destInvalid, lastInvalid);
		Vector_4V extrapolate = V4Andc(destInvalid, lastInvalid);

		Vector_4V damped = V4Scale(delta, damping);
		Vector_4V next = V4Add(last, damped);
		Vector_4V diff = V4Subtract(dest, last);

		next = V4Or(next, nextInvalid);
		diff = V4Or(diff, diffInvalid);
		damped = V4Or(damped, deltaInvalid);

		*pDest = *pLast = V4SelectFT(extrapolate, dest, next);
		*pDelta = V4SelectFT(extrapolate, diff, damped);

		pDest += 1;
		pLast += 1;
		pDelta += 1;
	}

	for(; countQuats; countQuats--)
	{
		Vector_4V dest = *pDest;
		Vector_4V last = *pLast;
		Vector_4V delta = *pDelta;

		Vector_4V destInvalid = V4IsEqualIntV(dest, _invalid);
		Vector_4V lastInvalid = V4IsEqualIntV(last, _invalid);
		Vector_4V deltaInvalid = V4IsEqualIntV(delta, _invalid);
		Vector_4V nextInvalid = V4Or(lastInvalid, deltaInvalid);
		Vector_4V diffInvalid = V4Or(destInvalid, lastInvalid);
		Vector_4V extrapolate = V4Andc(destInvalid, lastInvalid);

		Vector_4V axis;
		Vector_4V angle;
		V4QuatToAxisAngle(axis, angle, delta);
		Vector_4V dampedAngle = V4Min(V4Scale(angle, damping), maxAngular);

		Vector_4V damped = V4QuatFromAxisAngle(axis,  dampedAngle);
		Vector_4V next = V4QuatMultiply(last, damped);
		Vector_4V invLast = V4QuatInvertNormInput(last);
		Vector_4V diff = V4QuatMultiply(invLast, dest);

		next = V4Or(next, nextInvalid);
		diff = V4Or(diff, diffInvalid);
		damped = V4Or(damped, deltaInvalid);

		*pDest = *pLast = V4SelectFT(extrapolate, dest, next);
		*pDelta = V4SelectFT(extrapolate, diff, damped);

		pDest += 1;
		pLast += 1;
		pDelta += 1;
	}

	for(; countFloats; countFloats--)
	{
		Vector_4V dest = *pDest;
		Vector_4V last = *pLast;
		Vector_4V delta = *pDelta;

		Vector_4V destInvalid = V4IsEqualIntV(dest, _invalid);
		Vector_4V lastInvalid = V4IsEqualIntV(last, _invalid);
		Vector_4V deltaInvalid = V4IsEqualIntV(delta, _invalid);
		Vector_4V nextInvalid = V4Or(lastInvalid, deltaInvalid);
		Vector_4V diffInvalid = V4Or(destInvalid, lastInvalid);
		Vector_4V extrapolate = V4Andc(destInvalid, lastInvalid);

		Vector_4V damped = V4Scale(delta, damping);
		Vector_4V next = V4Add(last, damped);
		Vector_4V diff = V4Subtract(dest, last);

		next = V4Or(next, nextInvalid);
		diff = V4Or(diff, diffInvalid);
		damped = V4Or(damped, deltaInvalid);

		*pDest = *pLast = V4SelectFT(extrapolate, dest, next);
		*pDelta = V4SelectFT(extrapolate, diff, damped);

		pDest += 1;
		pLast += 1;
		pDelta += 1;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_EXTRAPOLATE_FRAG