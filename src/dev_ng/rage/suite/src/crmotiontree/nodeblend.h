//
// crmotiontree/nodeblend.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEBLEND_H
#define CRMOTIONTREE_NODEBLEND_H

#include "atl/array.h"

#include "node.h"
#include "request.h"

namespace rage
{

class crFrameFilter;
class crWeightModifier;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base for pair based operations with filters
class crmtNodePair : public crmtNodeParent
{
public:
	// PURPOSE: Default constructor
	crmtNodePair(eNodes nodeType);

	// PURPOSE: Resource constructor
	crmtNodePair(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodePair();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Update node
	virtual void Update(float dt);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh();

	// PURPOSE: Set the frame filter for this operation
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);


	// PURPOSE: Get the frame filter for this operation
	// RETURNS: pointer to the frame filter, can be NULL.
	crFrameFilter* GetFilter() const;

	// PURPOSE: Returns indices
	const crLock& GetWeights() const;

	
	// PURPOSE: Override influence
	enum eInfluenceOverride
	{
		kInfluenceOverrideNone = 0,
		kInfluenceOverrideZero,
		kInfluenceOverrideOne
	};

	// PURPOSE: Set child influence override
	// PARAMS: childIdx - index of child to override influence
	// PARAMS: influence - new override status
	void SetInfluenceOverride(int childIdx, eInfluenceOverride override);

	// PURPOSE : Get child influence override
	// PARAMS: childIdx - index of child to get override influence
	// RETURNS: current override status
	eInfluenceOverride GetInfluenceOverride(int childIdx) const;

	// PURPOSE: Apply child influence override (if any)
	float ApplyInfluenceOverride(u32 childIdx, float influence) const;

protected:
	crLock m_Weights;
	crFrameFilter* m_Filter;
	u32 m_FilterSignature;
	atRangeArray<u8, 2> m_InfluenceOverrides;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base for pair based operations with weights
class crmtNodePairWeighted : public crmtNodePair
{
public:

	// PURPOSE: Default constructor
	crmtNodePairWeighted(eNodes nodeType);

	// PURPOSE: Resource constructor
	crmtNodePairWeighted(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodePairWeighted();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: Set current weight percentage
	// PARAMS: weight - new weight percentage
	virtual void SetWeight(float weight);

	// PURPOSE: Set current weight rate
	// PARAMS: rate - new weight rate (zero for fixed weight)
	void SetWeightRate(float rate);

	// PURPOSE: Set weight modifier
	// PARAMS: modifier - pointer to the weight modifier (can be NULL)
	// NOTES: modifier will be reference counted
	void SetWeightModifier(crWeightModifier* modifier);


	// PURPOSE: Get current weight value
	// RETURNS: Current weight value
	// NOTES: This returns the raw, unmodified version of the weight value
	float GetWeight() const;

	// PURPOSE: Get current weight value (with any modification)
	// RETURNS: Current modified weight value
	// NOTES: This returns the weight value after applying the weight modifier (if present)
	float GetWeightModified() const;

	// PURPOSE: Get current weight rate
	// RETURNS: Current weight rate (zero for fixed weight)
	float GetWeightRate() const;

	// PURPOSE: Get weight modifier
	// RETURNS: pointer to the weight modifier, can be NULL.
	crWeightModifier* GetWeightModifier() const;

protected:
	float m_Weight;
	float m_WeightRate;
	crWeightModifier* m_Modifier;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Blend pair of input child nodes into a single output
// Multipurpose; static blends, cross fades, combining body parts, partial body blending etc
class crmtNodeBlend : public crmtNodePairWeighted
{
public:

	// PURPOSE: Default constructor
	crmtNodeBlend();

	// PURPOSE: Resource constructor
	crmtNodeBlend(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeBlend();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeBlend);

	// PURPOSE: Initialization
	// PARAMS: blend - initial blend percentage 0% -> 100% [0..1]
	// blendRate - change in blend per second (can be positive or negative, zero means fixed blend) (default 0.0)
	// destroyWhenComplete - if blend rate non-zero, destroy self and blended out child when blend reaches limits?
	// mergeBlend - is merge blend?
	// filter - frame filter to use for this blend (default NULL)
	// modifier - blend weight modifier (default NULL)
	void Init(float blend, float blendRate=0.f, bool destroyWhenComplete=false, bool mergeBlend=false, crFrameFilter* filter=NULL, crWeightModifier* modifier=NULL);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Update node
	virtual void Update(float dt);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: override influence query
	virtual float GetChildInfluence(u32 childIdx) const;


	// PURPOSE: Set current weight percentage
	// PARAMS: weight - new weight percentage 0% -> 100% [0..1]
	// NOTES: This is an override of crmtNodePair's SetWeight, it clamps input [0..1]
	// Result of blend = child0 * (1 - weight) + child1 * weight
	virtual void SetWeight(float weight);


	// PURPOSE: Set destroy when complete
	// PARAMS: if weight rate is non-zero, destroy self and blended out child when weight reaches limit.
	// NOTES: This blend node will be replaced with the 100% blended in child.
	void SetDestroyWhenComplete(bool destroyWhenComplete);

	// PURPOSE: Get destroy when complete setting
	// RETURNS: Current destroy when complete behavior (only used if blend rate non-zero)
	bool IsDestroyWhenComplete() const;

	// PURPOSE: Set merge blend
	// PARAMS: mergeBlend - is merge blend or regular blend operation
	void SetMergeBlend(bool mergeBlend);

	// PURPOSE: Is merge blend
	// RETURNS: true - merge blend operation, false - regular blend operation
	bool IsMergeBlend() const;


	// PURPOSE: Enumeration of messages
	enum
	{
		kMsgBlendComplete = CRMT_PACK_MSG_ID(crmtNodePair::kMsgEnd, kNodeBlend),

		kMsgEnd,
	};

private:
	bool m_DestroyWhenComplete;
	bool m_MergeBlend;
};

////////////////////////////////////////////////////////////////////////////////

inline void crmtNodePair::SetInfluenceOverride(int childIdx, crmtNodePair::eInfluenceOverride influence)
{
	m_InfluenceOverrides[childIdx] = u8(influence);
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNodePair::eInfluenceOverride crmtNodePair::GetInfluenceOverride(int childIdx) const
{
	return eInfluenceOverride(m_InfluenceOverrides[childIdx]);
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtNodePair::ApplyInfluenceOverride(u32 childIdx, float influence) const
{
	eInfluenceOverride override = eInfluenceOverride(m_InfluenceOverrides[childIdx]);
	if(override == kInfluenceOverrideNone)
	{
		return influence;
	}
	else 
	{
		return (override == kInfluenceOverrideOne)?1.f:0.f;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtNodePairWeighted::GetWeight() const
{
	return m_Weight;
}

////////////////////////////////////////////////////////////////////////////////

inline float crmtNodeBlend::GetChildInfluence(u32 childIdx) const
{
	return ApplyInfluenceOverride(childIdx, childIdx?m_Weight:(1.f-m_Weight));
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNodeBlend::IsMergeBlend() const
{
	return m_MergeBlend;
}

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodePair::GetWeights() const
{
	return m_Weights;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODEBLEND_H
