//
// crmotiontree/requestmerge.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "requestmerge.h"

#include "nodemerge.h"

#include "cranimation/framefilters.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestMerge::crmtRequestMerge()
: m_Filter(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestMerge::crmtRequestMerge(crmtRequest& source0, crmtRequest& source1)
: m_Filter(NULL)
{
	Init(source0, source1);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestMerge::crmtRequestMerge(crmtRequest& source)
: m_Filter(NULL)
{
	Init(source);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestMerge::~crmtRequestMerge()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestMerge::Init(crmtRequest& source0, crmtRequest& source1)
{
	SetSource(&source0, 0);
	SetSource(&source1, 1);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestMerge::Init(crmtRequest& source)
{
	SetSource(&source, 1);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestMerge::Shutdown()
{
	SetFilter(NULL);

	crmtRequestSource<2>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtRequestMerge::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestMerge::SetFilter(crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	if(m_Filter)
	{
		m_Filter->Release();
	}
	m_Filter = filter;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestMerge::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	if(HasValidSource())
	{
		crmtNodeMerge* mergeNode = crmtNodeMerge::CreateNode(tree);
		mergeNode->Init(m_Filter);

		GenerateSourceNodes(tree, node, mergeNode);

		return mergeNode;
	}
	else
	{
		// TODO --- could assume this case will just modify node's parameters (if it is an merge node)
		return node;
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
