//
// crmotiontree/requestblendn.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestblendn.h"

#include "nodeblendn.h"

#include "cranimation/framefilters.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestN::crmtRequestN()
: m_Filter(NULL)
{
	SetWeights(0.f);
	SetWeightRates(0.f);

	sysMemSet(&m_InputFilters, 0, sizeof(m_InputFilters));
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestN::~crmtRequestN()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestN::Shutdown()
{
	SetFilter(NULL);

	crmtRequestSource<crmtNodeN::sm_MaxChildren>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestN::GetWeight(u32 sourceIdx) const
{
	return m_Weights[sourceIdx];
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestN::GetWeightRate(u32 sourceIdx) const
{
	return m_WeightRates[sourceIdx];
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtRequestN::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtRequestN::GetInputFilter(u32 childIdx) const
{
	return m_InputFilters[childIdx];
}

////////////////////////////////////////////////////////////////////////////////

bool crmtRequestN::IsZeroDestination() const
{
	return m_ZeroDest;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestN::SetWeight(u32 sourceIdx, float weight)
{
	m_Weights[sourceIdx] = Clamp(weight, 0.f, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestN::SetWeights(float weight)
{
	for(u32 i=0; i<crmtNodeN::sm_MaxChildren; ++i)
	{
		m_Weights[i] = weight;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestN::SetWeightRate(u32 sourceIdx, float weightRate)
{
	m_WeightRates[sourceIdx] = Clamp(weightRate, 0.f, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestN::SetWeightRates(float weightRate)
{
	for(u32 i=0; i<crmtNodeN::sm_MaxChildren; ++i)
	{
		m_WeightRates[i] = weightRate;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestN::SetFilter(crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	if(m_Filter)
	{
		m_Filter->Release();
	}
	m_Filter = filter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestN::SetInputFilter(u32 sourceIdx, crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	crFrameFilter*& currentFilter = m_InputFilters[sourceIdx];
	if(currentFilter)
	{
		currentFilter->Release();
	}
	currentFilter = filter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestN::SetZeroDestination(bool zeroDest)
{
	m_ZeroDest = zeroDest;
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestBlendN::crmtRequestBlendN()
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestBlendN::~crmtRequestBlendN()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestBlendN::Shutdown()
{
	crmtRequestN::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestBlendN::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeBlendN* blendNNode = crmtNodeBlendN::CreateNode(tree);
	blendNNode->Init(m_Filter);
	blendNNode->SetZeroDestination(m_ZeroDest);

	GenerateSourceNodes(tree, node, blendNNode);

	u32 n=0;
	for(u32 i=0; i<crmtNodeN::sm_MaxChildren; ++i)
	{
		if(GetSource(i))
		{
			blendNNode->SetWeight(n, m_Weights[i]);
			blendNNode->SetWeightRate(n, m_WeightRates[i]);
			blendNNode->SetInputFilter(n, m_InputFilters[i]);
			n++;
		}
	}

	blendNNode->SetNumChildren(n);

	return blendNNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
