//
// crmotiontree/dependency_invalid.frag
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_INVALID_FRAG
#define MOTIONTREE_DEPENDENCY_INVALID_FRAG

#include "crmotiontree/crmtdiag.h"
#include "system/cache.h"
#include "system/dependency.h"
#include "system/memops.h"

using namespace rage;

SPUFRAG_DECL(bool, dependency_invalid, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_invalid, const sysDependency& dep)
{
	crmtDebug3("dependency_invalid");

	sysMemSet(dep.m_Params[0].m_AsPtr, 0xff, dep.m_DataSizes[0]);

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_INVALID_FRAG