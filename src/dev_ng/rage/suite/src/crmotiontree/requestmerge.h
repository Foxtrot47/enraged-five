//
// crmotiontree/requestmerge.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTMERGE_H
#define CRMOTIONTREE_REQUESTMERGE_H

#include "request.h"

namespace rage
{

class crFrameFilter;

// PURPOSE: Merge request
// Requests a merge operation between two inputs.
class crmtRequestMerge : public crmtRequestSource<2>
{
public:

	// PURPOSE: Default constructor
	crmtRequestMerge();

	// PURPOSE: Initializing constructor - two new inputs
	// PARAMS: source0, source1 - generate nodes(s) that merge node combines
	crmtRequestMerge(crmtRequest& source0, crmtRequest& source1);

	// PURPOSE: Initializing constructor - one new input, one existing
	// PARAMS: source - request that generates node(s) that will be merged
	crmtRequestMerge(crmtRequest& source);

	// PURPOSE: Destructor
	virtual ~crmtRequestMerge();

	// PURPOSE: Initializing constructor - two new inputs
	// PARAMS: source0, source1 - generate nodes(s) that merge node combines
	void Init(crmtRequest& source0, crmtRequest& source1);

	// PURPOSE: Initializing constructor - one new input, one existing
	// PARAMS: source - request that generates node(s) that will be merged
	void Init(crmtRequest& source);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get the frame filter for this blend
	// RETURNS: pointer to the frame filter, can be NULL.
	crFrameFilter* GetFilter() const;


	// PURPOSE: Set the frame filter for this blend
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	crFrameFilter* m_Filter;
};



} // namespace rage

#endif // CRMOTIONTREE_REQUESTMERGE_H
