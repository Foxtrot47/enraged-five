//
// crmotiontree/requestclip.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTCLIP_H
#define CRMOTIONTREE_REQUESTCLIP_H

#include "request.h"

namespace rage
{

class crClip;

// PURPOSE: Clip request
// Requests a motion tree construct an clip playback node.
class crmtRequestClip : public crmtRequest
{
public:

	// PURPOSE: Default constructor
	crmtRequestClip();

	// PURPOSE: Simple constructor, performs basic initialization
	// PARAMS: clip - pointer to clip to be played (can be NULL)
	// time - initial start time used to play clip (default 0.0)
	// rate - clip playback rate (can be positive, negative or zero, default 1.0)
	// NOTES: See Init()
	crmtRequestClip(const crClip* clip, float time=0.f, float rate=1.f);

	// PURPOSE: Destructor
	virtual ~crmtRequestClip();

	// PURPOSE: Basic initializer
	// PARAMS: clip - pointer to clip to be played (can be NULL)
	// time - initial start time used to play clip (default 0.0)
	// rate - playback rate (can be positive, negative or zero, default 1.0)
	void Init(const crClip* clip=NULL, float time=0.f, float rate=1.f);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Set clip to be played
	// PARAMS: clip - pointer to clip to be played (can be NULL)
	// NOTES: Requester uses AddRef and Release internally to control
	// reference counting on the clip.
	void SetClip(const crClip* clip);

	// PURPOSE: Set initial clip start time
	// PARAMS: time - initial start time used to play clip
	// NOTES: Value will be clamped to ensure within legal range
	// clip can support [0..duration]
	void SetTime(float time);

	// PURPOSE: Set playback rate
	// PARAMS: rate - clip playback rate (can be negative or zero)
	void SetRate(float rate);

	// PURPOSE: Set looping behavior
	// PARAMS: ignoreAnimLooping - override the clip's own looping status (default false)
	// forceLooping - force looping (only used if ignoreClipLooping is true)
	void SetLooping(bool ignoreClipLooping, bool forceLooping);

	// PURPOSE: Get clip to be played
	// RETURNS: pointer to clip to be played (can be NULL)
	const crClip* GetClip() const;

	// PURPOSE: Get initial clip start time
	// RETURNS: Intial start time used to play clip
	// NOTES: If clip is non-NULL, time will be in range [0..duration].
	float GetTime() const;

	// PURPOSE: Get playback rate
	// RETURNS: Clip playback rate (can be positive, negative or zero)
	float GetRate() const;

	// PURPOSE: Get the looping behavior and current status
	// PARAMS: outIgnoreClipLooping - returns the "ignore the clip's own looping status" value.
	// outForceLooping - returns the "force looping (only used if ignoreClipLooping is true)" value.
	// RETURNS: The net result of the two parameters, when combined with the clip's looping flag.
	bool GetLooping(bool& outIgnoreClipLooping, bool& outForceLooping) const;

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	const crClip* m_Clip;
	float m_Time;
	float m_Rate;
	bool m_IgnoreClipLooping;
	bool m_ForceLooping;
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTCLIP_H
