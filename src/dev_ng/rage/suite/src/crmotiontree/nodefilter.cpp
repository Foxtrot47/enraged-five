//
// crmotiontree/nodefilter.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodefilter.h"

#include "motiontree.h"

#include "cranimation/framefilters.h"
#include "cranimation/frameaccelerator.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeFilter::crmtNodeFilter()
: crmtNodeParent(kNodeFilter)
, m_Filter(NULL)
, m_FilterSignature(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeFilter::crmtNodeFilter(datResource& rsc)
: crmtNodeParent(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeFilter::~crmtNodeFilter()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeFilter, crmtNode::kNodeFilter, CRMT_NODE_RECEIVE_UPDATES);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFilter::Init(crFrameFilter* filter)
{
	SetFilter(filter);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFilter::Shutdown()
{
	ShuttingDown();
	SetFilter(NULL);

	crmtNodeParent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFilter::Update(float)
{
	if(m_Filter && m_Filter->GetSignature() != m_FilterSignature)
	{
		Refresh();
	}

	SetDisabled(GetFirstChild()->IsDisabled());
	SetSilent(false);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeFilter::Dump(crmtDumper& dumper) const
{
	crmtNodeParent::Dump(dumper);

	dumper.Outputf(2, "filter", "%p", m_Filter);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeFilter::GetMinNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeFilter::GetMaxNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFilter::SetFilter(crFrameFilter* filter)
{
	if(filter != m_Filter)
	{
		if(filter)
		{
			filter->AddRef();
		}
		if(m_Filter)
		{
			m_Filter->Release();
		}
		m_Filter = filter;

		Refresh();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeFilter::Refresh()
{
	if(m_Filter)
	{
		GetMotionTree().GetFrameAccelerator().FindFilterWeights(m_Weights, GetMotionTree().GetFrameData(), *m_Filter);
		m_FilterSignature = m_Filter->GetSignature();
	}
	else
	{
		m_Weights.Unlock();
		m_FilterSignature = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtNodeFilter::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
