//
// crmotiontree/requestpm.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestpm.h"

#include "nodepm.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestPm::crmtRequestPm()
: m_Pm(NULL)
, m_Rate(1.f)
, m_IgnorePmLooping(false)
, m_ForceLooping(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestPm::crmtRequestPm(crpmParameterizedMotion* pm)
: m_Pm(NULL)
, m_Rate(1.f)
, m_IgnorePmLooping(false)
, m_ForceLooping(false)
{
	Init(pm);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestPm::~crmtRequestPm()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestPm::Init(crpmParameterizedMotion* pm)
{
	SetParameterizedMotion(pm);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestPm::Shutdown()
{
	SetParameterizedMotion(NULL);

	crmtRequest::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestPm::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodePm* pmNode = crmtNodePm::CreateNode(tree);
	Assert(pmNode);

	pmNode->Init(m_Pm);
	pmNode->GetParameterizedMotionPlayer().SetRate(m_Rate);
	if(m_IgnorePmLooping)
	{
		pmNode->GetParameterizedMotion()->SetLooped(m_ForceLooping);
	}

	if(node)
	{
		node->Replace(*pmNode);
	}

	return pmNode;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestPm::SetParameterizedMotion(crpmParameterizedMotion* pm)
{
	m_Pm = pm;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestPm::SetRate(float rate)
{
	m_Rate = rate;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestPm::SetLooping(bool ignorePmLooping, bool forceLooping)
{
	m_IgnorePmLooping = ignorePmLooping;
	m_ForceLooping = forceLooping;
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestPm::GetRate() const
{
	return m_Rate;
}

////////////////////////////////////////////////////////////////////////////////

bool crmtRequestPm::GetLooping(bool& outIgnorePmLooping, bool& outForceLooping) const
{
	outIgnorePmLooping = m_IgnorePmLooping;
	outForceLooping = m_ForceLooping;

	if(m_IgnorePmLooping)
	{
		return m_ForceLooping;
	}
	else
	{
		if(m_Pm)
		{
			return m_Pm->IsLooped();
		}
		else
		{
			// odd case, asked to respect the animation's looping status, but no valid animation
			return false;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

crpmParameterizedMotion* crmtRequestPm::GetParameterizedMotion() const
{
	return m_Pm;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
