//
// crmotiontree/observer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_OBSERVER_H
#define CRMOTIONTREE_OBSERVER_H

#include "atl/array.h"
#include "atl/functor.h"
#include "cranimation/animation_config.h"
#include "system/interlocked.h"

namespace rage
{

class crmtDumper;
class crmtInterceptor;
class crmtMessage;
class crmtNode;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Smart pointer object that can be attached to motion tree nodes
// Also receives notification messages from the observed node
class crmtObserver
{
public:

	// PURPOSE: Default constructor
	crmtObserver();

	// PURPOSE: Destructor
	virtual ~crmtObserver();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Attach to a new node
	// PARAMS: node - new node
	void Attach(crmtNode& node);

	// PURPOSE: Attach to a new node
	// PARAMS: observer - observer that's already observing the new node
	void Attach(const crmtObserver& observer);

	// PURPOSE: Detach from node
	void Detach();


	// PURPOSE: Is this observer observing?  Is it attached to a node?
	// RETURNS: true - currently attached to a node
	bool IsAttached() const;

	// PURPOSE: Get node that's being observed
	// RETURNS: Pointer to node that's being observed (will be NULL if not observing anything).
	// NOTES: Do not store this pointer!  Nodes can be destroyed at any time.
	// Always use this function to get a safe, fresh pointer to the node.
	crmtNode* GetNode() const;


	// PURPOSE: Override this to receive notification messages from observed node.
	// PARAMS: msg - notification message from the node
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: Reset message intercept override
	// PARAMS: interceptor - use this object to register interest in receiving particular messages
	// NOTE: If derived observer receives messages, it must implement
	// the reset intercept message call, and register intercepts using
	// the interceptor object, for each message id it wishes to receive.
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);


	// PURPOSE: Get current reference count
	// RETURNS: Reference count
	u32 GetRef() const;

	// PURPOSE: Take out an additional reference
	void AddRef() const;

	// PURPOSE: Release a reference (will destroy object, if references now zero)
	// RETURNS: New reference count
	u32 Release() const;


#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging info. 
	virtual void Dump(crmtDumper&) const;
#endif // CR_DEV

private:
	mutable u32 m_RefCount;
	crmtNode* m_Node;
	crmtObserver* m_NextObserver;

	friend class crmtNode;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Derived version of the smart pointer observer for typed nodes
// Allows checked node retrieval and message receiving
template <class T>
class crmtObserverTyped : public crmtObserver
{
public:
	// PURPOSE: Default constructor
	crmtObserverTyped();

	// PURPOSE: Destructor
	virtual ~crmtObserverTyped();

	// PURPOSE: Shutdown override
	virtual void Shutdown();

	// PURPOSE: Get typed node that's being observed
	// RETURNS: Pointer to node that's being observed (will be NULL if not observing anything).
	// NOTES: Same rules apply as for crmtObserver
	T* GetNodeSafe() const;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Derived version of the smart pointer observer object
// Provides hooks for specifying callback functions to intercept
// particular messages for processing.
class crmtObserverFunctor : public crmtObserver
{
public:

	// PURPOSE: Default constructor
	crmtObserverFunctor();

	// PURPOSE: Destructor
	virtual ~crmtObserverFunctor();

	// PURPOSE: Shutdown, free/release dynamic memory
	void Shutdown();


	// PURPOSE: Message notification override
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: Reset message intercept override
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);


	// PURPOSE: Message functor definition
	// NOTES: Use this to register functions to intercept messages.
	// Functions must take the form; void Fn(const crmtMessage&, crmtObserverFunctor&)
	typedef Functor2<const crmtMessage&, crmtObserverFunctor&> MsgFunctor;


	// PURPOSE: Register a functor against a particular message
	// NOTES: Do not attempt to register multiple functors against a
	// single message id!
	void RegisterFunctor(u32 msgId, const MsgFunctor& functor);
	
	// PURPOSE: Unregister a particular message functor
	void UnregisterFunctor(u32 msgId);

	// PURPOSE: Unregister all functors on all messages
	void UnregisterAllFunctors();

	// PURPOSE: Preallocate space for functors
	void Reserve(int numFunctors);

protected:
	struct MsgFunctorPair
	{
		u32 m_MsgId;
		MsgFunctor m_Functor;
	};

	atArray<MsgFunctorPair> m_FunctorPairs;
};

// TODO --- provide a delegate version of this (as functors are "deprecated")

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Derived version of the smart pointer observer object for typed nodes
// Provides hooks for specifying callback functions to intercept
// particular messages for processing.
template <class T>
class crmtObserverFunctorTyped : public crmtObserverTyped<T>
{
public:
	// PURPOSE: Default constructor
	crmtObserverFunctorTyped();

	// PURPOSE: Destructor
	virtual ~crmtObserverFunctorTyped();

	// PURPOSE: Shutdown, free/release dynamic memory
	void Shutdown();


	// PURPOSE: Message notification override
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: Reset message intercept override
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);


	// PURPOSE: Message functor definition
	typedef Functor2<const crmtMessage&, crmtObserverFunctor&> MsgFunctor;

	// PURPOSE: Register a functor against a particular message
	// NOTES: Do not attempt to register multiple functors against a
	// single message id!
	void RegisterFunctor(u32 msgId, const MsgFunctor& functor);

	// PURPOSE: Unregister a particular message functor
	void UnregisterFunctor(u32 msgId);

	// PURPOSE: Unregister all functors on all messages
	void UnregisterAllFunctors();

	// PURPOSE: Preallocate space for functors
	void Reserve(int n);

protected:
	struct MsgFunctorPair
	{
		u32 m_MsgId;
		MsgFunctor m_Functor;
	};

	atArray<MsgFunctorPair> m_FunctorPairs;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Forwards messages from transmitter node to a receiver node
class crmtObserverForwarder : public crmtObserver
{
public:

	// PURPOSE: Default constructor
	crmtObserverForwarder();

	// PURPOSE: Destructor
	virtual ~crmtObserverForwarder();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Message notification override
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: Reset message intercept override
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);


	// PURPOSE: Set the receiver node from an observer
	void SetRecipient(crmtObserver& observer);

	// PURPOSE: Set the receiver node
	void SetRecipient(crmtNode& node);

	// PURPOSE: Clear the current receiver node
	void ClearRecipient();

	// PURPOSE: Get the current receiver node (can be NULL)
	crmtNode* GetRecipient() const;


private:
	
	crmtObserver m_RecipientObserver;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Message wrapper object
// Used by nodes to send notification messages to observers
class crmtMessage
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS: msgId - message id
	// payload - message payload pointer
	crmtMessage(u32 msgId, void* payload = NULL);

	// PURPOSE: Get the message id
	// RETURNS: message id
	// NOTES: Message ids can be obtained from the enumerated
	// kMsg types on each node.
	u32 GetMsgId() const;

	// PURPOSE: Get the message payload
	// RETURNS: message payload pointer (can be NULL)
	void* GetPayload() const;

private:
	u32 m_MsgId;
	void* m_Payload;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Intercept message collector
// Used by observers to register interest in particular messages
class crmtInterceptor
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS: node - node to pass intercept calls to
	crmtInterceptor(crmtNode& node);

	// PURPOSE: Register interest in intercepting particular message id
	// PARAMS: node id
	void RegisterMessageIntercept(u32 msgId);

private:
	crmtNode* m_Node;
};

////////////////////////////////////////////////////////////////////////////////

inline bool crmtObserver::IsAttached() const
{
	return m_Node != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNode* crmtObserver::GetNode() const
{
	return m_Node;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtObserver::GetRef() const
{
	return m_RefCount;
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtObserver::AddRef() const
{
	sysInterlockedIncrement(&m_RefCount);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtObserver::Release() const
{
	u32 refCount = sysInterlockedDecrement(&m_RefCount);
	if(!refCount)
	{
		delete this;
		return 0;
	}
	else
	{
		return refCount;
	}
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
inline crmtObserverTyped<T>::crmtObserverTyped()
{
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
inline crmtObserverTyped<T>::~crmtObserverTyped()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
inline void crmtObserverTyped<T>::Shutdown()
{
	crmtObserver::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
T* crmtObserverTyped<T>::GetNodeSafe() const
{
	crmtNode* node = GetNode();
    // Does not compile on SPU because crmtNode is forward declared
	// Assert(node->GetNodeType() == T::sm_NodeTypeInfo.GetNodeType());
	return static_cast<T*>(node);
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
crmtObserverFunctorTyped<T>::crmtObserverFunctorTyped()
{
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
crmtObserverFunctorTyped<T>::~crmtObserverFunctorTyped()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
void crmtObserverFunctorTyped<T>::Shutdown()
{
	UnregisterAllFunctors();
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
void crmtObserverFunctorTyped<T>::ReceiveMessage(const crmtMessage& msg)
{
	crmtObserverTyped<T>::ReceiveMessage(msg);

	const u32 msgId = msg.GetMsgId();
	const int numFunctorPairs = m_FunctorPairs.GetCount();

	for (int i = 0; i < numFunctorPairs; ++i)
	{
		MsgFunctorPair& funPair = m_FunctorPairs[i];
		if (funPair.m_MsgId == msgId)
		{
			funPair.m_Functor(msg, *this);
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
void crmtObserverFunctorTyped<T>::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	crmtObserverTyped<T>::ResetMessageIntercepts(interceptor);

	const int numFunctorPairs = m_FunctorPairs.GetCount();
	for(int i=0; i<numFunctorPairs; ++i)
	{
		MsgFunctorPair& funPair = m_FunctorPairs[i];
		interceptor.RegisterMessageIntercept(funPair.m_MsgId);
	}
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
void crmtObserverFunctorTyped<T>::RegisterFunctor(u32 msgId, const MsgFunctor& functor)
{
	MsgFunctorPair& funPair = m_FunctorPairs.Grow();
	funPair.m_MsgId = msgId;
	funPair.m_Functor = functor;
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
void crmtObserverFunctorTyped<T>::UnregisterFunctor(u32 msgId)
{
	for (int i = 0; i < m_FunctorPairs.GetCount(); ++i)
	{
		MsgFunctorPair& funPair = m_FunctorPairs[i];
		if (funPair.m_MsgId == msgId)
		{
			m_FunctorPairs.DeleteFast(i--);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
void crmtObserverFunctorTyped<T>::UnregisterAllFunctors()
{
	m_FunctorPairs.Reset();
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
void crmtObserverFunctorTyped<T>::Reserve(int n)
{
	m_FunctorPairs.Reserve(n);
}


////////////////////////////////////////////////////////////////////////////////

inline crmtMessage::crmtMessage(u32 msgId, void* payload)
: m_MsgId(msgId)
, m_Payload(payload)
{
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtMessage::GetMsgId() const
{
	return m_MsgId;
}

////////////////////////////////////////////////////////////////////////////////

inline void* crmtMessage::GetPayload() const
{
	return m_Payload;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtInterceptor::crmtInterceptor(crmtNode& node)
: m_Node(&node)
{
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_OBSERVER_H
