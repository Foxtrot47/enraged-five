//
// crmotiontree/dependency_physical.frag
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_PHYSICAL_FRAG
#define MOTIONTREE_DEPENDENCY_PHYSICAL_FRAG

#include "crmotiontree/crmtdiag.h"
#include "creature/componentphysical.h"
#include "system/dependency.h"
#include "vectormath/classes.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////

static Vec3V_Out GetAxisFromQuat(QuatV_In q)
{
	Vec3V axis;
	ScalarV angle;
	QuatVToAxisAngle(axis, angle, q);

	Vec3V oppositeAxis = Negate(axis);
	ScalarV oppositeAngle = Add(Negate(angle), ScalarV(V_TWO_PI));
	BoolV isOpposite = IsGreaterThan(angle, ScalarV(V_PI));
	axis = SelectFT(isOpposite, axis, oppositeAxis);
	angle = SelectFT(isOpposite, angle, oppositeAngle);
	return Scale(angle, axis);
}

////////////////////////////////////////////////////////////////////////////////

static QuatV_Out GetQuatFromAxis(Vec3V_In axis)
{
	ScalarV angleDelta = Mag(axis);
	QuatV axisAngle = QuatVFromAxisAngle(InvScale(axis, angleDelta), angleDelta);
	return SelectFT(IsGreaterThan(angleDelta, ScalarV(V_FLT_EPSILON)), QuatV(V_IDENTITY), axisAngle);
}

////////////////////////////////////////////////////////////////////////////////

static bool IsCloseAll(TransformV_In t0, TransformV_In t1, ScalarV_In epsilon)
{
	return IsCloseAll(t0.GetPosition(), t1.GetPosition(), epsilon) && IsCloseAll(t0.GetRotation(), t1.GetRotation(), epsilon);
}

////////////////////////////////////////////////////////////////////////////////

static TransformV_Out Integrate(const crMotionDescription& descr, ScalarV_In dt, TransformV_In prevWorld, TransformV_In currWorld, TransformV_In prevOffset, TransformV_In currOffset)
{
	Vec3V linearDelta, angularDelta;

	// velocity
	{
		linearDelta = UnTransform3x3(currWorld, currWorld.GetPosition() - prevWorld.GetPosition());
		angularDelta = GetAxisFromQuat(Multiply(Invert(prevWorld.GetRotation()), currWorld.GetRotation()));
	}

	// gravity forces
	{
		Vec3V gravityForce = UnTransform3x3(currWorld, Vec3V(descr.m_Gravity.GetIntrin128()));
		Vec3V gravityDelta = dt * dt * gravityForce;

		linearDelta += gravityDelta;
		angularDelta += Cross(descr.m_Direction, gravityDelta);
	}

	// spring forces
	{
		linearDelta += dt * dt * descr.m_Linear.m_Strength * currOffset.GetPosition();
		angularDelta += dt * dt * descr.m_Angular.m_Strength * GetAxisFromQuat(currOffset.GetRotation());
	}

	// spring damping
	{
		Vec3V linearOffset = currOffset.GetPosition() - prevOffset.GetPosition();
		Vec3V angularOffset = GetAxisFromQuat(Multiply(Invert(prevOffset.GetRotation()), currOffset.GetRotation()));

		linearDelta += dt * descr.m_Linear.m_Damping * linearOffset;
		angularDelta += dt * descr.m_Angular.m_Damping * angularOffset;
	}

	TransformV nextWorld;
	Transform(nextWorld, currWorld, TransformV(GetQuatFromAxis(angularDelta), linearDelta));
	return nextWorld;
}

////////////////////////////////////////////////////////////////////////////////

static TransformV_Out Constraint(const crMotionDescription& descr, TransformV_In currWorld, TransformV_In targetWorld)
{
	TransformV deltaLocal;
	UnTransform(deltaLocal, targetWorld, currWorld);

	// position constraint
	Vec3V pos = Clamp(deltaLocal.GetPosition(), descr.m_Linear.m_MinConstraint, descr.m_Linear.m_MaxConstraint);

	// orientation constraint, apply constraint independently on each axis
	QuatV rot(V_IDENTITY);
	const Vec3V min = descr.m_Angular.m_MinConstraint, max = descr.m_Angular.m_MaxConstraint;
	if(!IsZeroAll(min) || !IsZeroAll(max))
	{
		rot = deltaLocal.GetRotation();
		Vec3V euler, clamp;

		const BoolV maskX(Vec::V4VConstant(V_MASKX)), maskY(Vec::V4VConstant(V_MASKY)), maskZ(Vec::V4VConstant(V_MASKZ));
		euler = QuatVToEulersXYZ(rot);
		clamp = Clamp(euler, min, max);
		euler = SelectFT(maskX, euler, clamp);
		rot = QuatVFromEulersXYZ(clamp);

		euler = QuatVToEulersYXZ(rot);
		clamp = Clamp(euler, min, max);
		euler = SelectFT(maskY, euler, clamp);
		rot = QuatVFromEulersYXZ(clamp);

		euler = QuatVToEulersZYX(rot);
		clamp = Clamp(euler, min, max);
		euler = SelectFT(maskZ, euler, clamp);
		rot = QuatVFromEulersZYX(clamp);
	}

	TransformV resultWorld;
	Transform(resultWorld, targetWorld, TransformV(rot, pos));
	return resultWorld;
}

////////////////////////////////////////////////////////////////////////////////

SPUFRAG_DECL(bool, dependency_physical, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_physical, const sysDependency& dep)
{
	crmtDebug3("dependency_physical");

	crCreatureComponentPhysical::Motion* motions = static_cast<crCreatureComponentPhysical::Motion*>(dep.m_Params[0].m_AsPtr);
	Mat34V* localMtxs = static_cast<Mat34V*>(dep.m_Params[1].m_AsPtr);
	Mat34V* objectMtxs = static_cast<Mat34V*>(dep.m_Params[2].m_AsPtr);
	const Mat34V& parentMtx = *static_cast<const Mat34V*>(dep.m_Params[3].m_AsConstPtr);
	float deltaTime = dep.m_Params[4].m_AsFloat;
	u32 numMotions = dep.m_Params[5].m_AsInt;

	// skip if zero delta time
	if(deltaTime < SMALL_FLOAT)
		return true;

	const float maxDeltaTime = 1.f/30.f;
	deltaTime = Min(deltaTime, maxDeltaTime);

	const u32 maxSteps = 32;
	ScalarV epsilon(V_FLT_SMALL_4);
	ScalarV dt = LoadScalar32IntoScalarV(deltaTime);

	for(u32 i=0; i < numMotions; i++)
	{
		crCreatureComponentPhysical::Motion& motion = motions[i];

		// find next target
		Mat34V boneMtx;
		Transform(boneMtx, parentMtx, objectMtxs[motion.m_ParentIdx]);

		TransformV parentWorld;
		TransformVFromMat34V(parentWorld, boneMtx);

		TransformV nextTarget;
		Transform(nextTarget, parentWorld, motion.m_OffsetLocal);

		// reset if needed
		u32 steps = 0;
		if(motion.m_Reset)
		{
			Transform(motion.m_CurrWorld, parentWorld, motion.m_OffsetLocal);
			motion.m_PrevWorld = motion.m_CurrWorld;
			motion.m_PrevOffset = motion.m_CurrOffset = TransformV(V_IDENTITY);
			motion.m_Reset = false;
			steps = maxSteps;
		}
		 
		do // simulate time steps
		{ 
			TransformV nextWorld, nextOffset;
			nextWorld = Integrate(motion.m_Descr, dt, motion.m_PrevWorld, motion.m_CurrWorld, motion.m_PrevOffset, motion.m_CurrOffset);
			nextWorld = Constraint(motion.m_Descr, nextWorld, nextTarget);
			UnTransform(nextOffset, nextWorld, nextTarget);

			motion.m_PrevWorld = Constraint(motion.m_Descr, motion.m_CurrWorld, nextTarget);
			motion.m_PrevOffset = motion.m_CurrOffset;
			motion.m_CurrWorld = nextWorld;
			motion.m_CurrOffset = nextOffset;
		}
		while(steps-- && !IsCloseAll(motion.m_PrevWorld, motion.m_CurrWorld, epsilon));

		// apply to skeleton
		Mat34V worldMtx;
		Mat34VFromTransformV(worldMtx, motion.m_CurrWorld);
		UnTransformOrtho(objectMtxs[motion.m_ChildIdx], parentMtx, worldMtx);
		UnTransformOrtho(localMtxs[motion.m_ChildIdx], objectMtxs[motion.m_ParentIdx], objectMtxs[motion.m_ChildIdx]);
	}

	return true;
}

///////////////////////////////////////////////////////////////////////

#endif // MOTIONTREE_DEPENDENCY_INVALID_FRAG
