//
// crmotiontree/dependency_blend.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_BLEND_FRAG
#define MOTIONTREE_DEPENDENCY_BLEND_FRAG

#include "cranimation/framedata.h"
#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"

using namespace rage;
using namespace Vec;

// dependency_blend pseudo-code
//
// if(dest valid)
//		if(src valid)
//			if(merge || weight < 1-tolerance)
//				dest = dest
//			else if(weight > tolerance)
//				dest = src
//			else
//				dest = lerp(dest, src)
//		else
//			dest = dest
// else
//		dest = invalid
//

SPUFRAG_DECL(bool, dependency_blend, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_blend, const sysDependency& dep)
{
	crmtDebug3("dependency_blend");

	Vector_4V* RESTRICT pDest = static_cast<Vector_4V*>(dep.m_Params[0].m_AsPtr);
	const Vector_4V* RESTRICT pSrc = static_cast<const Vector_4V*>(dep.m_Params[1].m_AsConstPtr);
	const Vector_4V* RESTRICT pFilter = static_cast<const Vector_4V*>(dep.m_Params[2].m_AsConstPtr);
	float alphaFloat = dep.m_Params[3].m_AsFloat;
	unsigned int countVectors = RAGE_COUNT(dep.m_Params[4].m_AsInt, 2);
	unsigned int countQuats = RAGE_COUNT(dep.m_Params[5].m_AsInt, 2);
	unsigned int countFloats = RAGE_COUNT(dep.m_Params[6].m_AsInt, 2);
	bool mergeBool = dep.m_Params[7].m_AsBool;

	Vector_4V _zero = V4VConstant(V_ZERO);
	Vector_4V _one = V4VConstant(V_ONE);
	Vector_4V _invalid = V4VConstant(V_MASKXYZW);
	Vector_4V noMerge = V4IsEqualIntV(V4LoadScalar32IntoSplatted(mergeBool), _zero);
	Vector_4V noFilter = V4IsEqualIntV(V4LoadScalar32IntoSplatted(pFilter!=NULL), _zero);
	Vector_4V alpha = V4LoadScalar32IntoSplatted(alphaFloat);
	Vector_4V tolerance = V4LoadScalar32IntoSplatted(WEIGHT_ROUNDING_ERROR_TOLERANCE);
	Vector_4V invTolerance = V4Subtract(_one, tolerance);

	// if no filter, use the source frame as dummy read-only pointer
	pFilter = pFilter ? pFilter : pSrc;

	for(; countVectors; countVectors--)
	{
		Vector_4V dest0 = pDest[0], src0 = pSrc[0];
		Vector_4V dest1 = pDest[1], src1 = pSrc[1];
		Vector_4V dest2 = pDest[2], src2 = pSrc[2];
		Vector_4V dest3 = pDest[3], src3 = pSrc[3];

		// compute weigths
		Vector_4V filter = *pFilter;
		Vector_4V scaled = V4Scale(alpha, filter);
		Vector_4V clamped = V4Clamp(scaled, _zero, _one);
		Vector_4V weight = V4SelectFT(noFilter, clamped, alpha);
		Vector_4V weightZero = V4IsLessThanV(weight, tolerance);
		Vector_4V weightOne = V4IsGreaterThanV(weight, invTolerance);
		Vector_4V removeDest = V4And(noMerge, weightOne);

		// lerp vectors
		Vector_4V blend0 = V4Lerp(V4SplatX(weight), dest0, src0);
		Vector_4V blend1 = V4Lerp(V4SplatY(weight), dest1, src1);
		Vector_4V blend2 = V4Lerp(V4SplatZ(weight), dest2, src2);
		Vector_4V blend3 = V4Lerp(V4SplatW(weight), dest3, src3);

		// test for invalid
		Vector_4V srcInvalid0 = V4IsEqualIntV(src0, _invalid);
		Vector_4V srcInvalid1 = V4IsEqualIntV(src1, _invalid);
		Vector_4V srcInvalid2 = V4IsEqualIntV(src2, _invalid);
		Vector_4V srcInvalid3 = V4IsEqualIntV(src3, _invalid);
		Vector_4V destInvalid0 = V4IsEqualIntV(dest0, _invalid);
		Vector_4V destInvalid1 = V4IsEqualIntV(dest1, _invalid);
		Vector_4V destInvalid2 = V4IsEqualIntV(dest2, _invalid);
		Vector_4V destInvalid3 = V4IsEqualIntV(dest3, _invalid);

		// select results
		Vector_4V srcValue0 = V4Or(V4SplatX(weightZero), src0);
		Vector_4V srcValue1 = V4Or(V4SplatY(weightZero), src1);
		Vector_4V srcValue2 = V4Or(V4SplatZ(weightZero), src2);
		Vector_4V srcValue3 = V4Or(V4SplatW(weightZero), src3);
		Vector_4V destValue0 = V4Or(V4SplatX(removeDest), dest0);
		Vector_4V destValue1 = V4Or(V4SplatY(removeDest), dest1);
		Vector_4V destValue2 = V4Or(V4SplatZ(removeDest), dest2);
		Vector_4V destValue3 = V4Or(V4SplatW(removeDest), dest3);
		Vector_4V blendValue0 = V4SelectFT(srcInvalid0, blend0, destValue0);
		Vector_4V blendValue1 = V4SelectFT(srcInvalid1, blend1, destValue1);
		Vector_4V blendValue2 = V4SelectFT(srcInvalid2, blend2, destValue2);
		Vector_4V blendValue3 = V4SelectFT(srcInvalid3, blend3, destValue3);

		pDest[0] = V4SelectFT(destInvalid0, blendValue0, srcValue0);
		pDest[1] = V4SelectFT(destInvalid1, blendValue1, srcValue1);
		pDest[2] = V4SelectFT(destInvalid2, blendValue2, srcValue2);
		pDest[3] = V4SelectFT(destInvalid3, blendValue3, srcValue3);

		pFilter += 1;
		pDest += 4;
		pSrc += 4;
	}

	for(; countQuats; countQuats--)
	{
		Vector_4V dest0 = pDest[0], src0 = pSrc[0];
		Vector_4V dest1 = pDest[1], src1 = pSrc[1];
		Vector_4V dest2 = pDest[2], src2 = pSrc[2];
		Vector_4V dest3 = pDest[3], src3 = pSrc[3];

		// compute weigths
		Vector_4V filter = *pFilter;
		Vector_4V scaled = V4Scale(alpha, filter);
		Vector_4V clamped = V4Clamp(scaled, _zero, _one);
		Vector_4V weight = V4SelectFT(noFilter, clamped, alpha);
		Vector_4V weightZero = V4IsLessThanV(weight, tolerance);
		Vector_4V weightOne = V4IsGreaterThanV(weight, invTolerance);
		Vector_4V removeDest = V4And(noMerge, weightOne);

		// normalized lerp on each quaternions
		Vector_4V prepare0 = V4QuatPrepareSlerp(src0, dest0);
		Vector_4V prepare1 = V4QuatPrepareSlerp(src1, dest1);
		Vector_4V prepare2 = V4QuatPrepareSlerp(src2, dest2);
		Vector_4V prepare3 = V4QuatPrepareSlerp(src3, dest3);
		Vector_4V blend0 = V4QuatNlerp(V4SplatX(weight), prepare0, src0);
		Vector_4V blend1 = V4QuatNlerp(V4SplatY(weight), prepare1, src1);
		Vector_4V blend2 = V4QuatNlerp(V4SplatZ(weight), prepare2, src2);
		Vector_4V blend3 = V4QuatNlerp(V4SplatW(weight), prepare3, src3);

		// test for invalid
		Vector_4V srcInvalid0 = V4IsEqualIntV(src0, _invalid);
		Vector_4V srcInvalid1 = V4IsEqualIntV(src1, _invalid);
		Vector_4V srcInvalid2 = V4IsEqualIntV(src2, _invalid);
		Vector_4V srcInvalid3 = V4IsEqualIntV(src3, _invalid);
		Vector_4V destInvalid0 = V4IsEqualIntV(dest0, _invalid);
		Vector_4V destInvalid1 = V4IsEqualIntV(dest1, _invalid);
		Vector_4V destInvalid2 = V4IsEqualIntV(dest2, _invalid);
		Vector_4V destInvalid3 = V4IsEqualIntV(dest3, _invalid);

		// select results
		Vector_4V srcValue0 = V4Or(V4SplatX(weightZero), src0);
		Vector_4V srcValue1 = V4Or(V4SplatY(weightZero), src1);
		Vector_4V srcValue2 = V4Or(V4SplatZ(weightZero), src2);
		Vector_4V srcValue3 = V4Or(V4SplatW(weightZero), src3);
		Vector_4V destValue0 = V4Or(V4SplatX(removeDest), dest0);
		Vector_4V destValue1 = V4Or(V4SplatY(removeDest), dest1);
		Vector_4V destValue2 = V4Or(V4SplatZ(removeDest), dest2);
		Vector_4V destValue3 = V4Or(V4SplatW(removeDest), dest3);
		Vector_4V blendValue0 = V4SelectFT(srcInvalid0, blend0, destValue0);
		Vector_4V blendValue1 = V4SelectFT(srcInvalid1, blend1, destValue1);
		Vector_4V blendValue2 = V4SelectFT(srcInvalid2, blend2, destValue2);
		Vector_4V blendValue3 = V4SelectFT(srcInvalid3, blend3, destValue3);

		pDest[0] = V4SelectFT(destInvalid0, blendValue0, srcValue0);
		pDest[1] = V4SelectFT(destInvalid1, blendValue1, srcValue1);
		pDest[2] = V4SelectFT(destInvalid2, blendValue2, srcValue2);
		pDest[3] = V4SelectFT(destInvalid3, blendValue3, srcValue3);

		pFilter += 1;
		pDest += 4;
		pSrc += 4;
	}

	// blend all floats with lerp, 4 per iteration
	for(; countFloats; countFloats--)
	{
		Vector_4V dest = *pDest, src = *pSrc;

		Vector_4V filter = *pFilter;
		Vector_4V scaled = V4Scale(alpha, filter);
		Vector_4V clamped = V4Clamp(scaled, _zero, _one);
		Vector_4V weight = V4SelectFT(noFilter, clamped, alpha);
		Vector_4V weightZero = V4IsLessThanV(weight, tolerance);
		Vector_4V weightOne = V4IsGreaterThanV(weight, invTolerance);
		Vector_4V removeDest = V4And(noMerge, weightOne);
		Vector_4V blend = V4Lerp(weight, dest, src);

		Vector_4V srcInvalid = V4IsEqualIntV(src, _invalid);
		Vector_4V destInvalid = V4IsEqualIntV(dest, _invalid);

		Vector_4V srcValue = V4Or(weightZero, src);
		Vector_4V destValue = V4Or(removeDest, dest);
		Vector_4V blendValue = V4SelectFT(srcInvalid, blend, destValue);

		*pDest = V4SelectFT(destInvalid, blendValue, srcValue);

		pFilter += 1;
		pDest += 1;
		pSrc += 1;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_BLEND_FRAG