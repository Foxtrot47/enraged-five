//
// crmotiontree/nodeproxy.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRMOTIONTREE_NODEPROXY_H
#define CRMOTIONTREE_NODEPROXY_H

#include "node.h"
#include "observer.h"

namespace rage
{

// PURPOSE: Simulates graph like behavior in the motion tree
// The output from a node in another part of the tree becomes
// simulated "input" to this node
class crmtNodeProxy : public crmtNode
{
public:

	// PURPOSE: Default constructor
	crmtNodeProxy();

	// PURPOSE: Resource constructor
	crmtNodeProxy(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeProxy();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeProxy);

	// PURPOSE: Initialization
	// PARAMS: node - pointer to new proxy node, NULL for none.
	void Init(crmtNode* node=NULL);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper&) const;
#endif // CR_DEV

	// PURPOSE: Set the proxy node
	// PARAMS: node - pointer to new proxy node, NULL for none
	// NOTES: Internally node is held by an observer.
	void SetProxyNode(crmtNode* node);

	// PURPOSE: Get the proxy node
	// RETURNS: pointer to proxy node (can be NULL)
	crmtNode* GetProxyNode() const;

private:

	// PURPOSE: Specialized proxy observer
	class ProxyObserver : public crmtObserver
	{
	public:

		// PURPOSE: Reset message intercept override
		virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);
	};

	ProxyObserver m_Observer;
};

////////////////////////////////////////////////////////////////////////////////

inline crmtNode* crmtNodeProxy::GetProxyNode() const
{
	return m_Observer.GetNode();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRMOTIONTREE_NODEPROXY_H
