//
// crmotiontree/managerdrv.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRMOTIONTREE_MANAGERDRV_H
#define CRMOTIONTREE_MANAGERDRV_H

#include "manager.h"

namespace rage
{


// PURPOSE: Manager that interfaces with DrV application
class crmtManagerDrV : public crmtManager
{
public:

	// PURPOSE: Default constructor
	crmtManagerDrV();

	// PURPOSE: Helpful constructor that allows provision of motion tree
	// SEE ALSO: Init()
	crmtManagerDrV(crmtMotionTree& tree, const crmtObserver* location=NULL);

	// PURPOSE: Destructor
	virtual ~crmtManagerDrV();

	// PURPOSE: Resource constructor
	crmtManagerDrV(datResource&);

	DECLARE_PLACE(crmtManagerDrV);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initialization
	// PARAMS: tree - motion tree this manager is going to manage
	// location - identifies root node, if manager is not controlling entire tree
	virtual void Init(crmtMotionTree& tree, const crmtObserver* location=NULL);

	// PURPOSE: Shutdown the manager
	virtual void Shutdown();


	// TODO --- implement DrV interface layer here! :)

private:

};

} // namespace rage

#endif // CRMOTIONTREE_MANAGERDRV_H
