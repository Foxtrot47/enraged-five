//
// crmotiontree/requestaddsubtract.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTADDSUBTRACT_H
#define CRMOTIONTREE_REQUESTADDSUBTRACT_H

#include "request.h"

#include "atl/array.h"
#include "cranimation/framefilters.h"

namespace rage
{
	
// PURPOSE: Add/Subtract request
// Requests an add or subtract operation between two inputs.
class crmtRequestAddSubtract : public crmtRequestSource<2>
{
public:

	// PURPOSE: Default constructor
	crmtRequestAddSubtract();

	// PURPOSE: Initializing constructor - two new inputs
	// PARAMS: source0, source1 - generate nodes(s) that addsubtract node combines
	// weight - additive weight (-ve for subtract, +ve for add, default 1.0)
	crmtRequestAddSubtract(crmtRequest& source0, crmtRequest& source1, float weight=1.f);

	// PURPOSE: Initializing constructor - one new input, one existing
	// PARAMS: source - request that generates node(s) that will be added or subtracted
	// weight - additive weight (-ve for subtract, +ve for add, default 1.0)
	crmtRequestAddSubtract(crmtRequest& source, float weight=1.f);

	// PURPOSE: Destructor
	virtual ~crmtRequestAddSubtract();

	// PURPOSE: Initializing constructor - two new inputs
	// PARAMS: source0, source1 - generate nodes(s) that addsubtract node combines
	// weight - additive weight (-ve for subtract, +ve for add, default 1.0)
	void Init(crmtRequest& source0, crmtRequest& source1, float weight=1.f);

	// PURPOSE: Initializing constructor - one new input, one existing
	// PARAMS: source - request that generates node(s) that will be added or subtracted
	// weight - additive weight (-ve for subtract, +ve for add, default 1.0)
	void Init(crmtRequest& source, float weight=1.f);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Get the additive weight
	// RETURNS: Additive weight (-ve for subtract, +ve for add)
	float GetWeight() const;

	// PURPOSE: Get the frame filter for this blend
	// RETURNS: pointer to the frame filter, can be NULL.
	crFrameFilter* GetFilter() const;


	// PURPOSE: Set the additive weight
	// PARAMS: weight - additive weight (-ve for subtract, +ve for add)
	void SetWeight(float weight);

	// PURPOSE: Set the frame filter for this blend
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	float m_Weight;
	crFrameFilter* m_Filter;
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTADDSUBTRACT_H
