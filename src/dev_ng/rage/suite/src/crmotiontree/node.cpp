//
// crmotiontree/node.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "node.h"

#include "iterator.h"
#include "motiontree.h"
#include "nodeaddn.h"
#include "nodeaddsubtract.h"
#include "nodeanimation.h"
#include "nodeblend.h"
#include "nodeblendn.h"
#include "nodecapture.h"
#include "nodeclip.h"
#include "nodeexpression.h"
#include "nodeextrapolate.h"
#include "nodefactory.h"
#include "nodefilter.h"
#include "nodeframe.h"
#include "nodeidentity.h"
#include "nodeik.h"
#include "nodeinvalid.h"
#include "nodejointlimit.h"
#include "nodemerge.h"
#include "nodemergen.h"
#include "nodemirror.h"
#include "nodepm.h"
#include "nodepose.h"
#include "nodeproxy.h"
#include "observer.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

#if CRMT_NODE_2_DEF_MAP
PARAM(movedebug, "Enable node 2 def map");
atMap<const crmtNode *, const void *> g_crmtNode2DefMap;
#endif //CRMT_NODE_2_DEF_MAP

////////////////////////////////////////////////////////////////////////////////

crmtNode::crmtNode(crmtNode::eNodes nodeType)
: m_RefCount(0)
, m_NodeType(u8(nodeType))
, m_MotionTree(NULL)
, m_ParentNode(NULL)
, m_SiblingNode(NULL)
, m_FirstObserver(NULL)
, m_Factory(NULL)
, m_ChildNode(NULL)
#if CRMT_NODE_SIGNATURE
, m_Signature(0)
#endif
, m_Disabled(false)
, m_Silent(false)
{
	AssertMsg(nodeType != kNodeNone , "crmtNode::crmtNode - bad node construction, pass proper node type identifier to base node");
	m_Flags |= kFlagShutdown; // Node is in a shutdown state until PreInit is called
}

////////////////////////////////////////////////////////////////////////////////

crmtNode::crmtNode(datResource&)
{
	Assert(0);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::VirtualConstructFromPtr(datResource& rsc, crmtNode* base)
{
	// TODO --- watch if special value used to indicate motion tree is parent!
	Assert(base);

	eNodes nodeType = base->GetNodeType();

	const NodeTypeInfo* info = FindNodeTypeInfo(nodeType);
	AssertMsg(info , "crmtNode::VirtualConstructFromPtr - unknown node type, unable to fix up");

	NodePlaceRscFn* placeRscFn = info->GetPlaceRscFn();
	placeRscFn(base, rsc);
}

////////////////////////////////////////////////////////////////////////////////

crmtNode::~crmtNode()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::PreInit(crmtMotionTree& tree, eNodes nodeType, u16 defaultFlags)
{
	Assert(!m_MotionTree);

	m_MotionTree = &tree;
	m_NodeType = u8(nodeType);

	m_Flags = defaultFlags;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::Shutdown()
{
	ShuttingDown();

	RemoveObservers();

	m_ParentNode = NULL;
	m_SiblingNode = NULL;
	m_MotionTree = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::Update(float)
{
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNode::Dump(crmtDumper& dumper) const
{
	dumper.Outputf(0, "node", "%p", this);
	dumper.Outputf(0, "type", "'%s'", GetNodeTypeName());
	dumper.Outputf(0, "refs", "%d", m_RefCount);
	dumper.Outputf(0, "parent", "%p", m_ParentNode);
	dumper.Outputf(0, "sibling", "%p", m_SiblingNode);
	dumper.Outputf(0, "disable", "%d", IsDisabled());
	dumper.Outputf(0, "silent", "%d", IsSilent());
	dumper.Outputf(0, "skipfirstchild", "%d", SkipFirstChild());

	crmtObserver* observer = m_FirstObserver;
	while(observer)
	{
		crmtObserver* nextObserver = observer->m_NextObserver;

		observer->Dump(dumper);

		observer = nextObserver;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::Validate(crmtValidator& validator) const
{
	if(validator.GetMotionTree() != m_MotionTree)
	{
		validator.Reportf("bad motion tree pointer %p", m_MotionTree);
	}
}

////////////////////////////////////////////////////////////////////////////////

const char* crmtNode::GetNodeTypeName() const
{
	return GetNodeTypeInfo().GetNodeName();
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNode::Orphan()
{
	crmtNodeParent* parentNode = GetParent();
	if(parentNode)
	{
		crmtNode* child = parentNode->GetFirstChild();
		Assert(child);

		// eldest child
		if(child == this)
		{
			parentNode->m_ChildNode = m_SiblingNode;
		}
		else
		{
			// younger sibling
			while(child)
			{
				if(child->GetNextSibling() == this)
				{
					child->m_SiblingNode = m_SiblingNode;
					break;
				}

				child = child->m_SiblingNode;
			}

			Assert(child);
		}

		m_SiblingNode = NULL;
		m_ParentNode = NULL;

		Release();
	}
	else
	{
		Assert(m_MotionTree);
		if(m_MotionTree->m_RootNode == this)
		{
			Assert(!GetNextSibling());

			m_MotionTree->m_RootNode = NULL;
			Release();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::Replace(crmtNode& newNode, bool destroy)
{
	newNode.AddRef();
	newNode.Orphan();

	crmtNodeParent* parentNode = GetParent();
	if(parentNode)
	{
		parentNode->ReplaceChild(*this, newNode, destroy);
	}
	else
	{
		Assert(m_MotionTree);
		Assert(m_MotionTree->m_RootNode == this);

		Assert(!GetNextSibling());

		crmtMotionTree* mt = m_MotionTree;
		if(destroy)
		{
			Shutdown();
		}
		Release();

		newNode.AddRef();
		mt->m_RootNode = &newNode;
	}

	newNode.Release();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::AddObserver(crmtObserver& observer)
{
	observer.AddRef();

	if(observer.m_Node)
	{
		observer.m_Node->RemoveObserver(observer);
	}

	observer.m_Node = this;
	observer.m_NextObserver = m_FirstObserver;
	m_FirstObserver = &observer;

	ResetMessageIntercepts();

	observer.ReceiveMessage(crmtMessage(kMsgAttach));
}

////////////////////////////////////////////////////////////////////////////////

bool crmtNode::RemoveObserver(crmtObserver& observer)
{
	if(observer.m_Node == this)
	{
		crmtObserver** pprevObserver = &m_FirstObserver;
		crmtObserver* currObserver = m_FirstObserver;

		while(currObserver)
		{
			if(currObserver == &observer)
			{
				*pprevObserver = currObserver->m_NextObserver;
				currObserver->m_NextObserver = NULL;

				if(m_Flags & kFlagGenerateMsgDetach)
				{
					observer.ReceiveMessage(crmtMessage(kMsgDetach));
				}

				currObserver->m_Node = NULL;
				currObserver->Release();

				ResetMessageIntercepts();

				return true;
			}

			pprevObserver = &currObserver->m_NextObserver;
			currObserver = currObserver->m_NextObserver;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::RemoveObservers()
{
	crmtObserver* observer = m_FirstObserver;
	while(observer)
	{
		crmtObserver* nextObserver = observer->m_NextObserver;

		if(m_Flags & kFlagGenerateMsgDetach)
		{
			observer->ReceiveMessage(crmtMessage(kMsgDetach));
		}

		observer->m_NextObserver = NULL;
		observer->m_Node = NULL;
		observer->Release();

		observer = nextObserver;
	}

	m_FirstObserver = NULL;

	m_Flags &= ~kFlagGenerateMsgMask;
	m_Flags &= ~kFlagProxied;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::ResetMessageIntercepts()
{
	m_Flags &= ~kFlagGenerateMsgMask;
	m_Flags &= ~kFlagProxied;

	crmtInterceptor interceptor(*this);

	crmtObserver* observer = m_FirstObserver;
	while(observer)
	{
		observer->ResetMessageIntercepts(interceptor);

		observer = observer->m_NextObserver;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::RegisterMessageIntercept(u32 msgId)
{
	if(CRMT_UNPACK_MSG_ID_NODE_ID(msgId) == kNodeBase)
	{
		switch(msgId)
		{
		case kMsgPreUpdate:
			m_Flags |= kFlagGenerateMsgPreUpdate;
			break;
		case kMsgUpdate:
			m_Flags |= kFlagGenerateMsgUpdate;
			break;
		case kMsgPostUpdate:
			m_Flags |= kFlagGenerateMsgPostUpdate;
			break;
		case kMsgDetach:
			m_Flags |= kFlagGenerateMsgDetach;
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		sm_NodeTypeInfos.Resize(kNodeNum);
		for(int i=0; i<sm_NodeTypeInfos.GetCount(); ++i)
		{
			sm_NodeTypeInfos[i] = NULL;
		}

		// automatically register core nodes (ones that don't need exotic modules)
		crmtNodeAnimation::InitClass();
		crmtNodeBlend::InitClass();
		crmtNodeAddSubtract::InitClass();
		crmtNodeFilter::InitClass();
		crmtNodeMirror::InitClass();
		crmtNodeFrame::InitClass();
		crmtNodeBlendN::InitClass();
		crmtNodeExtrapolate::InitClass();
		crmtNodeCapture::InitClass();
		crmtNodeProxy::InitClass();
		crmtNodeAddN::InitClass();
		crmtNodeIdentity::InitClass();
		crmtNodeMerge::InitClass();
		crmtNodePose::InitClass();
		crmtNodeMergeN::InitClass();
		crmtNodeInvalid::InitClass();
		crmtNodeJointLimit::InitClass();

		// non-core nodes (may force pulling in of additional modules)
#if INIT_NON_CORE_NODES
		crmtNodeIk::InitClass();
		crmtNodeClip::InitClass();
		crmtNodePm::InitClass();
		crmtNodeExpression::InitClass();
#endif
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::ShutdownClass()
{
	sm_NodeTypeInfos.Reset();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode::NodeTypeInfo::NodeTypeInfo(crmtNode::eNodes nodeType, const char* nodeName, NodeCreateFn* createFn, NodePlaceFn* placeFn, NodePlaceRscFn placeRscFn, u32 size)
: m_NodeType(nodeType)
, m_NodeName(nodeName)
, m_CreateFn(createFn)
, m_PlaceFn(placeFn)
, m_PlaceRscFn(placeRscFn)
, m_Size(size)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNode::NodeTypeInfo::~NodeTypeInfo()
{
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::NodeTypeInfo::Register() const
{
	crmtNode::RegisterNodeTypeInfo(*this);
}

////////////////////////////////////////////////////////////////////////////////

const crmtNode::NodeTypeInfo* crmtNode::FindNodeTypeInfo(crmtNode::eNodes nodeType)
{
	// if build in node type, retrieve from first kNodeNum entries
	if(nodeType < kNodeNum)
	{
		AssertMsg(nodeType < sm_NodeTypeInfos.GetCount() , "crmtNode::FindNodeTypeInfo - built in node types not registered, missing crmtMotionTree::InitClass call?");
		return sm_NodeTypeInfos[nodeType];
	}
	else
	{
		// non-built in (custom) node type, search any remaining entries
		for(int i=kNodeNum; i<sm_NodeTypeInfos.GetCount(); ++i)
		{
			if(sm_NodeTypeInfos[i] && sm_NodeTypeInfos[i]->GetNodeType() == nodeType)
			{
				return sm_NodeTypeInfos[i];
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::ReleaseInternal() const
{
	Assert(m_RefCount == 0);

	if(m_Factory)
	{
		m_Factory->DeleteNode(this);
	}
	else
	{
		delete this;
	}
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtNode::CreateNodeFromNodeFactory(crmtMotionTree& tree, crmtNode::eNodes nodeType)
{
	return tree.m_NodeFactory ? tree.m_NodeFactory->NewNode(nodeType) : NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNode::RegisterNodeTypeInfo(const crmtNode::NodeTypeInfo& info)
{
	eNodes nodeType = info.GetNodeType();

	// if built in type, store in first kNodeNum entries (which should already be allocated)
	if(nodeType < kNodeNum)
	{
		AssertMsg(sm_NodeTypeInfos.GetCount() >= kNodeNum , "crmtNode::RegisterNodeTypeInfo - build in node type attempting to register before call to crmtMotionTree::InitClass");
		AssertMsg(!sm_NodeTypeInfos[nodeType] , "crmtNode::RegisterNodeTypeInfo - built in node type registered twice");

		sm_NodeTypeInfos[nodeType] = &info;
	}
	else
	{
		// non-built in (custom) node type, grow array and append
		AssertMsg(sm_NodeTypeInfos.GetCount() >= kNodeNum , "crmtNode::RegisterNodeTypeInfo - custom node type attempting to register before call to crmtMotionTree::InitClass");
		AssertMsg(!FindNodeTypeInfo(nodeType) , "crmtNode::RegisterNodeTypeInfo - custom node type registered twice");

		sm_NodeTypeInfos.Grow() = &info;
	}
}

////////////////////////////////////////////////////////////////////////////////

atArray<const crmtNode::NodeTypeInfo*> crmtNode::sm_NodeTypeInfos;

////////////////////////////////////////////////////////////////////////////////

bool crmtNode::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

crmtNodeParent::crmtNodeParent(crmtNode::eNodes nodeType)
: crmtNode(nodeType)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeParent::crmtNodeParent(datResource& rsc)
: crmtNode(rsc)
{
	Assert(0);
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeParent::~crmtNodeParent()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::Shutdown()
{
	ShuttingDown();

	RemoveChildren();

	crmtNode::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crmtNodeParent::Dump(crmtDumper& dumper) const
{
	crmtNode::Dump(dumper);

	dumper.Outputf(1, "numchildren", "%d [%d..%d]", GetNumChildren(), GetMinNumChildren(), GetMaxNumChildren());
	dumper.Outputf(1, "firstchild", "%p", m_ChildNode);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::Validate(crmtValidator& validator) const
{
	crmtNode::Validate(validator);

	const u32 numChildren = GetNumChildren();
	if(numChildren < GetMinNumChildren() || numChildren > GetMaxNumChildren())
	{
		validator.Reportf("invalid num children %d [%d..%d]", numChildren, GetMinNumChildren(), GetMaxNumChildren());
	}
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::AddChild(crmtNode& newNode, bool append)
{
	newNode.AddRef();
	newNode.Orphan();

	if(m_ChildNode)
	{
		if(append)
		{
			crmtNode* child = m_ChildNode;
			while(child)
			{
				if(!child->GetNextSibling())
				{
					child->m_SiblingNode = &newNode;

					newNode.m_ParentNode = this;
					break;
				}

				child = child->GetNextSibling();
			}
		}
		else
		{
			newNode.m_SiblingNode = m_ChildNode;
			m_ChildNode = &newNode;
			newNode.m_ParentNode = this;
		}
	}
	else
	{
		m_ChildNode = &newNode;
		newNode.m_ParentNode = this;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::RemoveChild(crmtNode& node, bool destroy)
{
	if(node.GetParent() == this)
	{
		node.AddRef();
		node.Orphan();

		if(destroy)
		{
			node.Shutdown();
		}
		node.Release();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::RemoveChild(u32 childIdx)
{
	crmtNode* node = GetChild(childIdx);
	if(node != NULL)
	{
		RemoveChild(*node);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::RemoveChildren()
{
	crmtNode* child = m_ChildNode;

	while(child)
	{
		crmtNode* nextChild = child->m_SiblingNode;

		child->Shutdown();
		child->Release();

		child = nextChild;
	}

	m_ChildNode = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::ReplaceChild(crmtNode& node, crmtNode& newNode, bool destroy)
{
	u32 childIdx = GetChildIndex(node);
	InsertChild(childIdx, newNode);
	RemoveChild(node, destroy);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::InsertChild(u32 childIdx, crmtNode& newNode)
{
	if(childIdx == 0)
	{
		AddChild(newNode, false);
	}
	else
	{
		crmtNode* elderSibling = GetChild(childIdx-1);
		if(elderSibling)
		{
			newNode.AddRef();
			newNode.Orphan();

			newNode.m_SiblingNode = elderSibling->m_SiblingNode;
			newNode.m_ParentNode = this;

			elderSibling->m_SiblingNode = &newNode;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::PromoteChild(crmtNode& node, bool destroy)
{
	node.AddRef();
	RemoveChild(node, false);

	if(m_ParentNode)
	{
		m_ParentNode->ReplaceChild(*this, node, destroy);
		node.Release();
	}
	else
	{
		Assert(m_MotionTree);
		Assert(m_MotionTree->m_RootNode == this);

		m_MotionTree->m_RootNode = &node;

		node.m_ParentNode = NULL;

		if(destroy)
		{
			Shutdown();
		}
		Release();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeParent::PromoteChild(u32 childIdx)
{
	crmtNode* node = GetChild(childIdx);
	if(node != NULL)
	{
		PromoteChild(*node);
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage