//
// crmotiontree/dependency_add.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_ADD_FRAG
#define MOTIONTREE_DEPENDENCY_ADD_FRAG

#include "cranimation/framedata.h"
#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"

using namespace rage;
using namespace Vec;

// dependency_add pseudo-code
//
// if(dest valid)
//		if(src valid)
//			dest = dest + src
//		else
//			dest = dest
// else
//		if(merge)
//			dest = src
//		else
//			dest = dest

SPUFRAG_DECL(bool, dependency_add, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_add, const sysDependency& dep)
{
	crmtDebug3("dependency_add");

	Vector_4V* RESTRICT pDest = static_cast<Vector_4V*>(dep.m_Params[0].m_AsPtr);
	const Vector_4V* RESTRICT pSrc = static_cast<const Vector_4V*>(dep.m_Params[1].m_AsConstPtr);
	const f32* RESTRICT pFilter = static_cast<const f32*>(dep.m_Params[2].m_AsConstPtr);
	float alphaFloat = dep.m_Params[3].m_AsFloat;
	unsigned int countVectors = RAGE_ALIGN(dep.m_Params[4].m_AsInt, 2);
	unsigned int countQuats = RAGE_ALIGN(dep.m_Params[5].m_AsInt, 2);
	unsigned int countFloats = RAGE_COUNT(dep.m_Params[6].m_AsInt, 2);
	bool mergeBool = dep.m_Params[7].m_AsBool;

	Vector_4V _zero = V4VConstant(V_ZERO);
	Vector_4V _invalid = V4VConstant(V_MASKXYZW);
	Vector_4V noMerge = V4IsEqualIntV(V4LoadScalar32IntoSplatted(mergeBool), _zero);
	Vector_4V noFilter = V4IsEqualIntV(V4LoadScalar32IntoSplatted(pFilter!=NULL), _zero);
	Vector_4V alpha = V4LoadScalar32IntoSplatted(alphaFloat);

	// if no filter, uses the src has dummy pointer
	pFilter = pFilter ? pFilter : reinterpret_cast<const f32*>(pSrc);

	for(; countVectors; countVectors--)
	{
		Vector_4V dest = *pDest, src = *pSrc;

		// compute blend
		Vector_4V filter = V4LoadScalar32IntoSplatted(*pFilter);
		Vector_4V scaled = V4Scale(alpha, filter);
		Vector_4V weight = V4SelectFT(noFilter, scaled, alpha);
		Vector_4V blend = V4AddScaled(dest, src, weight);

		// select result
		Vector_4V srcInvalid = V4IsEqualIntV(src, _invalid);
		Vector_4V destInvalid = V4IsEqualIntV(dest, _invalid);
		Vector_4V anyInvalid = V4Or(srcInvalid, destInvalid);
		Vector_4V srcValue = V4Or(noMerge, src);
		Vector_4V blendValue = V4SelectFT(anyInvalid, blend, dest);

		*pDest = V4SelectFT(destInvalid, blendValue, srcValue);

		pFilter += 1;
		pDest += 1;
		pSrc += 1;
	}

	for(; countQuats; countQuats--)
	{
		Vector_4V dest = *pDest, src = *pSrc;

		// compute weight
		Vector_4V filter = V4LoadScalar32IntoSplatted(*pFilter);
		Vector_4V scaled = V4Scale(alpha, filter);
		Vector_4V weight = V4SelectFT(noFilter, scaled, alpha);
		Vector_4V isSubstract = V4IsLessThanV(weight, _zero);
		Vector_4V absWeight = V4Abs(weight);

		// multiply quaternions, select between subtracted or added
		Vector_4V q = V4QuatScaleAngle(src, absWeight);
		Vector_4V add = V4QuatMultiply(q, dest);
		Vector_4V invQ = V4QuatInvertNormInput(q);
		Vector_4V substract = V4QuatMultiply(dest, invQ);
		Vector_4V blend = V4SelectFT(isSubstract, add, substract);

		// select result
		Vector_4V srcInvalid = V4IsEqualIntV(src, _invalid);
		Vector_4V destInvalid = V4IsEqualIntV(dest, _invalid);
		Vector_4V anyInvalid = V4Or(srcInvalid, destInvalid);
		Vector_4V srcValue = V4Or(noMerge, src);
		Vector_4V blendValue = V4SelectFT(anyInvalid, blend, dest);

		*pDest = V4SelectFT(destInvalid, blendValue, srcValue);

		pFilter += 1;
		pDest += 1;
		pSrc += 1;
	}

	// add all floats, 4 per iteration
	for(; countFloats; countFloats--)
	{
		Vector_4V dest = *pDest, src = *pSrc;

		Vector_4V filter = *reinterpret_cast<const Vector_4V*>(pFilter);
		Vector_4V scaled = V4Scale(alpha, filter);
		Vector_4V weight = V4SelectFT(noFilter, scaled, alpha);
		Vector_4V blend = V4AddScaled(dest, src, weight);
		
		Vector_4V srcInvalid = V4IsEqualIntV(src, _invalid);
		Vector_4V destInvalid = V4IsEqualIntV(dest, _invalid);
		Vector_4V anyInvalid = V4Or(srcInvalid, destInvalid);
		Vector_4V srcValue = V4Or(noMerge, src);
		Vector_4V blendValue = V4SelectFT(anyInvalid, blend, dest);

		*pDest = V4SelectFT(destInvalid, blendValue, srcValue);

		pFilter += 4;
		pDest += 1;
		pSrc += 1;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_ADD_FRAG