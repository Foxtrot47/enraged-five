//
// crmotiontree/dependency_compose.frag
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_COMPOSE_FRAG
#define MOTIONTREE_DEPENDENCY_COMPOSE_FRAG

#include "crmtdiag.h"
#include "composer.h"

#if __SPU
#include "composer.cpp"
#endif

using namespace rage;

///////////////////////////////////////////////////////////////////////

SPUFRAG_DECL(bool, dependency_compose, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_compose, const sysDependency& dep)
{
	crmtDebug3("dependency_compose");
	u8* scratchBuffer = static_cast<u8*>(dep.m_Params[0].m_AsPtr);
	const crmtComposerData& builderData = *static_cast<const crmtComposerData*>(dep.m_Params[1].m_AsConstPtr);
	const crFrameData& frameData = *static_cast<const crFrameData*>(dep.m_Params[2].m_AsConstPtr);
	crHeap* heap = static_cast<crHeap*>(dep.m_Params[3].m_AsPtr);
	const crmtNode* rootNode = static_cast<const crmtNode*>(dep.m_Params[4].m_AsConstPtr);
	unsigned int scratchSize = dep.m_DataSizes[0];

	crmtComposer composer(frameData, builderData, scratchBuffer, scratchSize);
	if(composer.Compose(rootNode))
	{
		return composer.Commit(heap, dep.m_Parents[sysDependency::kParentStrong], dep.m_Priority);
	}

	return true;
}

///////////////////////////////////////////////////////////////////////

#endif // MOTIONTREE_DEPENDENCY_COMPOSE_FRAG