//
// crmotiontree/updater.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "updater.h"

#include "node.h"

#include "system/cache.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

void crmtUpdater::Iterate(crmtNode& rootNode, float dt)
{
	crmtNode* node = &rootNode;
	crmtNode* stack[CRMT_ITERATE_BATCH];
	crmtNode** stack_top = stack;

	UpdateMsgPayload payload;
	payload.m_DeltaTime = dt;

	crmtMessage update(crmtNode::kMsgUpdate, &payload);
	crmtMessage postUpdate(crmtNode::kMsgPostUpdate, &payload);

	while(1)
	{
		PrefetchDC(node->GetFirstChild());

		node->AddRef();

		// pre-visit
		u32 flags = node->m_Flags;
#if CRMT_UPDATE_INFLUENCES
		if(flags & crmtNode::kFlagGenerateMsgPreUpdate)
		{
			node->MessageObservers(crmtMessage(crmtNode::kMsgPreUpdate, &payload));
		}
#endif

		if(Unlikely(flags & crmtNode::kFlagReceivePreUpdate))
		{
			node->PreUpdate(dt);
		}

		// iterate the eldest child
		crmtNode* nodeChild = node->GetFirstChild();
		if(nodeChild && Verifyf(stack_top < stack+CRMT_ITERATE_BATCH, "too many nodes on the stack!"))
		{
			*stack_top = node;
			stack_top++;
			node = nodeChild;
			continue;
		}

		// iterate the siblings
		while(1)
		{
			crmtNode* nodeSibling = node->GetNextSibling();
			PrefetchDC(nodeSibling);

			// visit
			if(Unlikely(flags & crmtNode::kFlagGenerateMsgUpdate))
			{
				node->MessageObservers(update);
			}

			node->SetDisabled(false);
			node->SetSilent(false);
			if(Likely(flags & crmtNode::kFlagReceiveUpdate))
			{
				node->Update(dt);
			}

			if(Unlikely(flags & crmtNode::kFlagGenerateMsgPostUpdate))
			{
				node->MessageObservers(postUpdate);
			}

			node->Release();

			// continue with siblings or parent
			if(Unlikely(nodeSibling))
			{
				node = nodeSibling;
				break;
			}
			else if(Likely(stack_top > stack))
			{
				stack_top--;
				node = *stack_top;
				flags = node->m_Flags;
				continue;
			}

			return;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

InfluenceStack::InfluenceStack()
: m_Depth(0)
{
}

////////////////////////////////////////////////////////////////////////////////

InfluenceStack::~InfluenceStack()
{
}

////////////////////////////////////////////////////////////////////////////////

void InfluenceStack::PushInfluence(float influence)
{
	FastAssert(m_Depth < CRMT_ITERATE_BATCH);
	m_Influences[m_Depth++] = influence;
}

////////////////////////////////////////////////////////////////////////////////

float InfluenceStack::PopInfluence()
{
	FastAssert(m_Depth != 0);
	return m_Influences[--m_Depth];
}

////////////////////////////////////////////////////////////////////////////////

float InfluenceStack::TopInfluence() const
{
	FastAssert(m_Depth != 0);
	return m_Influences[m_Depth-1];
}

////////////////////////////////////////////////////////////////////////////////

u32 InfluenceStack::GetInfluenceDepth() const
{
	return m_Depth;
}

////////////////////////////////////////////////////////////////////////////////

float InfluenceStack::CalcAbsoluteInfluence() const
{
	return CalcRelativeInfluence(0, m_Depth);
}

////////////////////////////////////////////////////////////////////////////////

float InfluenceStack::CalcRelativeInfluence(u32 depthMin, u32 depthMax) const
{
	Assert(depthMin <= depthMax);
	float influence = 1.f;
	for(u32 i=depthMin; i<depthMax; ++i)
	{
		influence *= m_Influences[i];
	}
	return influence;
}

////////////////////////////////////////////////////////////////////////////////

void InfluenceStack::Copy(const InfluenceStack& that)
{
	m_Depth = that.m_Depth;
	memcpy(m_Influences, that.m_Influences, m_Depth*sizeof(float));
}

////////////////////////////////////////////////////////////////////////////////

void InfluenceStack::PushNode(crmtNode& node)
{
	if(node.GetParent() && node.GetParent()->GetMaxNumChildren() > 1)
	{
		int childIdx = node.GetParent()->GetChildIndex(node);
		PushInfluence(node.GetParent()->GetChildInfluence(childIdx));
	}
	else
	{
		// TODO - have to push something, or Pop won't match
		// and can't optimize out based on parent, as parent pointer can change during update
		PushInfluence(1.f);
	}
}

////////////////////////////////////////////////////////////////////////////////

void InfluenceStack::PopNode(crmtNode&)
{
	// TODO - can't use parent pointer, as may be modified during update, so won't match Push
	//	if(node.GetParent() && node.GetParent()->GetMaxNumChildren() > 1)
	{
		PopInfluence();
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
