//
// crmotiontree/requestaddsubtract.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestaddsubtract.h"

#include "nodeaddsubtract.h"

#include "cranimation/framefilters.h"
#include "math/amath.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestAddSubtract::crmtRequestAddSubtract()
: m_Weight(0.f)
, m_Filter(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestAddSubtract::crmtRequestAddSubtract(crmtRequest& source0, crmtRequest& source1, float weight)
: m_Weight(0.f)
, m_Filter(NULL)
{
	Init(source0, source1, weight);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestAddSubtract::crmtRequestAddSubtract(crmtRequest& source, float weight)
: m_Weight(0.f)
, m_Filter(NULL)
{
	Init(source, weight);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestAddSubtract::~crmtRequestAddSubtract()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAddSubtract::Init(crmtRequest& source0, crmtRequest& source1, float weight)
{
	SetSource(&source0, 0);
	SetSource(&source1, 1);
	SetWeight(weight);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAddSubtract::Init(crmtRequest& source, float weight)
{
	SetSource(&source, 1);
	SetWeight(weight);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAddSubtract::Shutdown()
{
	SetFilter(NULL);

	crmtRequestSource<2>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestAddSubtract::GetWeight() const
{
	return m_Weight;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtRequestAddSubtract::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAddSubtract::SetWeight(float weight)
{
	m_Weight = weight;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestAddSubtract::SetFilter(crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	if(m_Filter)
	{
		m_Filter->Release();
	}
	m_Filter = filter;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestAddSubtract::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	if(HasValidSource())
	{
		crmtNodeAddSubtract* addNode = crmtNodeAddSubtract::CreateNode(tree);
		addNode->Init(m_Weight, m_Filter);

		GenerateSourceNodes(tree, node, addNode);

		return addNode;
	}
	else
	{
		// TODO --- could assume this case will just modify node's parameters (if it is an addsubtract node)
		return node;
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
