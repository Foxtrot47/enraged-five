//
// crmotiontree/dependency_proxy.frag
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_PROXY_FRAG
#define MOTIONTREE_DEPENDENCY_PROXY_FRAG

#include "crmtdiag.h"
#include "system/cache.h"
#include "system/dependency.h"

using namespace rage;

///////////////////////////////////////////////////////////////////////

SPUFRAG_DECL(bool, dependency_proxy, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_proxy, const sysDependency&)
{
	crmtDebug3("dependency_proxy");

	return true;
}

///////////////////////////////////////////////////////////////////////

#endif // MOTIONTREE_DEPENDENCY_PROXY_FRAG