//
// crmotiontree/requestmirror.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestmirror.h"

#include "nodemirror.h"

#include "cranimation/framefilters.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestMirror::crmtRequestMirror()
: m_Filter(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestMirror::crmtRequestMirror(crmtRequest& source, crFrameFilter* filter)
: m_Filter(NULL)
{
	Init(source, filter);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestMirror::crmtRequestMirror(crFrameFilter* filter)
: m_Filter(NULL)
{
	Init(filter);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestMirror::~crmtRequestMirror()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestMirror::Init(crmtRequest& source, crFrameFilter* filter)
{
	SetSource(&source, 0);
	SetFilter(filter);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestMirror::Init(crFrameFilter* filter)
{
	SetFilter(filter);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestMirror::Shutdown()
{
	SetFilter(NULL);

	crmtRequestSource<1>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crmtRequestMirror::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestMirror::SetFilter(crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	if(m_Filter)
	{
		m_Filter->Release();
	}
	m_Filter = filter;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestMirror::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeMirror* mirrorNode = crmtNodeMirror::CreateNode(tree);
	mirrorNode->Init(m_Filter);

	GenerateSourceNodes(tree, node, mirrorNode);

	return mirrorNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
