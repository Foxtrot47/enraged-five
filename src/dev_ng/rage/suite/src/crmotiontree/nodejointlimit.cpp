//
// crmotiontree/nodejointlimit.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "nodejointlimit.h"

#include "cranimation/frameiterators.h"
#include "crskeleton/jointdata.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtNodeJointLimit::crmtNodeJointLimit()
: crmtNodeParent(kNodeJointLimit)
, m_Filter(NULL)
, m_JointData(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeJointLimit::crmtNodeJointLimit(datResource& rsc)
: crmtNodeParent(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtNodeJointLimit::~crmtNodeJointLimit()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(crmtNodeJointLimit, crmtNode::kNodeJointLimit, 0);

////////////////////////////////////////////////////////////////////////////////

void crmtNodeJointLimit::Init(const crJointData& jointData, crFrameFilter* filter)
{
	SetJointData(jointData);
	SetFilter(filter);
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeJointLimit::Shutdown()
{
	ShuttingDown();
	if (m_JointData)
	{
		m_JointData->Release();
	}
	SetFilter(NULL);

	crmtNodeParent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeJointLimit::GetMinNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

u32 crmtNodeJointLimit::GetMaxNumChildren() const
{
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeJointLimit::SetJointData(const crJointData& jointData)
{
	jointData.AddRef();

	if (m_JointData)
	{
		m_JointData->Release();
	}
	m_JointData = &jointData;
}

////////////////////////////////////////////////////////////////////////////////

void crmtNodeJointLimit::SetFilter(crFrameFilter* filter)
{
	if(filter)
	{
		filter->AddRef();
	}
	if(m_Filter)
	{
		m_Filter->Release();
	}
	m_Filter = filter;	
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
