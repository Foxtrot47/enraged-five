//
// crmotiontree/dependency_normalize.frag
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOTIONTREE_DEPENDENCY_NORMALIZE_FRAG
#define MOTIONTREE_DEPENDENCY_NORMALIZE_FRAG

#include "crmotiontree/crmtdiag.h"
#include "system/dependency.h"
#include "vectormath/vectormath.h"

using namespace rage;
using namespace Vec;

SPUFRAG_DECL(bool, dependency_normalize, const sysDependency&);
SPUFRAG_IMPL(bool, dependency_normalize, const sysDependency& dep)
{
	crmtDebug3("dependency_normalize");

	Vector_4V* RESTRICT pDest = static_cast<Vector_4V*>(dep.m_Params[0].m_AsPtr);
	unsigned int countVectors = RAGE_ALIGN(dep.m_Params[1].m_AsInt, 2);
	unsigned int countQuats = RAGE_COUNT(dep.m_Params[2].m_AsInt, 2);

	Vector_4V _invalid = V4VConstant(V_MASKXYZW);

	// skip vectors
	pDest += countVectors;

	for(; countQuats; countQuats--)
	{
		Vector_4V dest0 = pDest[0];
		Vector_4V dest1 = pDest[1];
		Vector_4V dest2 = pDest[2];
		Vector_4V dest3 = pDest[3];

		// normalize quaternions
		Vector_4V norm0 = V4Normalize(dest0);
		Vector_4V norm1 = V4Normalize(dest1);
		Vector_4V norm2 = V4Normalize(dest2);
		Vector_4V norm3 = V4Normalize(dest3);

		// select between normalized or invalid
		Vector_4V invalid0 = V4IsEqualIntV(dest0, _invalid);
		Vector_4V invalid1 = V4IsEqualIntV(dest1, _invalid);
		Vector_4V invalid2 = V4IsEqualIntV(dest2, _invalid);
		Vector_4V invalid3 = V4IsEqualIntV(dest3, _invalid);

		pDest[0] = V4Or(invalid0, norm0);
		pDest[1] = V4Or(invalid1, norm1);
		pDest[2] = V4Or(invalid2, norm2);
		pDest[3] = V4Or(invalid3, norm3);

        pDest += 4;
	}

	return true;
}

#endif // MOTIONTREE_DEPENDENCY_NORMALIZE_FRAG