//
// crmotiontree/requestextrapolate.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRMOTIONTREE_REQUESTEXTRAPOLATE_H
#define CRMOTIONTREE_REQUESTEXTRAPOLATE_H

#include "request.h"

namespace rage
{

// PURPOSE: Extrapolate request
// Requests a motion tree construct an extrapolate node.
class crmtRequestExtrapolate : public crmtRequestSource<1>
{
public:

	// PURPOSE: Default constructor
	crmtRequestExtrapolate();

	// PURPOSE: Simple constructor, performs basic initialization - for extrapolating new nodes
	// PARAMS:
	// NOTES: See Init()
	crmtRequestExtrapolate(crmtRequest& source, float damping);

	// PURPOSE: Simple constructor, performs basic initialization - for extrapolating existing nodes
	// PARAMS:
	// NOTES: See Init()
	crmtRequestExtrapolate(float damping);

	// PURPOSE: Destructor
	virtual ~crmtRequestExtrapolate();

	// PURPOSE: Basic initializer - for extrapolating new nodes
	// PARAMS:
	// source - request that will generate new child node(s)
	// damping - damping to use during extrapolation stage (< 0 and >= 1, 1 indicates no damping)
	void Init(crmtRequest& source, float damping);

	// PURPOSE: Basic initializer - for extrapolating existing nodes
	// damping - damping to use during extrapolation stage (< 0 and >= 1, 1 indicates no damping)
	void Init(float damping);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Set damping value to use during extrapolation
	// PARAMS: damping - damping value (>0 and <=1, 1 indicates no damping)
	void SetDamping(float damping);

	// PURPOSE: Get damping value used during extrapolation
	// RETURNS: current damping value
	float GetDamping() const;


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	float m_Damping;
};

} // namespace rage

#endif //CRMOTIONTREE_REQUESTEXTRAPOLATE_H

