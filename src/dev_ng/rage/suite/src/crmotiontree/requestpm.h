//
// crmotiontree/requestpm.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTPM_H
#define CRMOTIONTREE_REQUESTPM_H

#include "request.h"

namespace rage
{

class crpmParameterizedMotion;


// PURPOSE: Parameterized motion request
// Requests a motion tree construct a parameterized motion playback node.
class crmtRequestPm : public crmtRequest
{
public:

	// PURPOSE: Default constructor
	crmtRequestPm();

	// PURPOSE: Simple constructor, performs basic initialization
	// PARAMS: pm - pointer to the parameterized motion instance to be used (can be NULL)
	crmtRequestPm(crpmParameterizedMotion* pm);

	// PURPOSE: Destructor
	virtual ~crmtRequestPm();

	// PURPOSE: Basic initializer
	// PARAMS: pm - pointer to the parameterized motion instance to be used (can be NULL)
	void Init(crpmParameterizedMotion* pm);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();


	// PURPOSE: Set parameterized motion to use
	// PARAMS: pm - pointer to parameterized motion instance to use (can be NULL)
	void SetParameterizedMotion(crpmParameterizedMotion* pm);

	// PURPOSE: Set playback rate
	// PARAMS: rate - parameterized motion playback rate (zero or greater, negative values not permitted)
	void SetRate(float rate);

	// PURPOSE: Set looping behavior
	// PARAMS: ignorePmLooping - override the parameterized motion's own looping status (default false)
	// forceLooping - force looping (only used if ignorePmLooping is true)
	void SetLooping(bool ignorePmLooping, bool forceLooping);


	// PURPOSE: Get parameterized motion to use
	// RETURNS: pointer to parameterized motion instance to use (can be NULL)
	crpmParameterizedMotion* GetParameterizedMotion() const;

	// PURPOSE: Get playback rate
	// RETURNS: Parameterized motion playback rate (zero or greater, negative values not permitted)
	float GetRate() const;

	// PURPOSE: Get the looping behavior and current status
	// PARAMS: outIgnorePmLooping - returns the "ignore the parameterized motion's own looping status" value.
	// outForceLooping - returns the "force looping (only used if ignorePmLooping is true)" value.
	// RETURNS: The net result of the two parameters, when combined with the parameterized motion's looping flag.
	bool GetLooping(bool& outIgnorePmLooping, bool& outForceLooping) const;


protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	crpmParameterizedMotion* m_Pm;
	float m_Rate;
	bool m_IgnorePmLooping;
	bool m_ForceLooping;
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTPM_H
