//
// crmotiontree/motiontreescheduler.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_MOTIONTREESCHEDULER_H
#define CRMOTIONTREE_MOTIONTREESCHEDULER_H

#include "system/ipc.h"

namespace rage
{

class crmtMotionTree;
struct sysDependency;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Manages update of motion trees
// Add motion tree to manager when ready to update.
// Tree will be assigned to a free internal worker thread,
// which may block waiting on tasks to complete
// Also manages composer task specific resources
class crmtMotionTreeScheduler
{
public:

	// PURPOSE: Default constructor
	crmtMotionTreeScheduler();

	// PURPOSE: Destructor
	~crmtMotionTreeScheduler();

	// PURPOSE: Initialization
	void Init();

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();

	// PURPOSE: Defines delegate that can optionally be executed during motion tree update
	// NOTES: Provides one payload parameter
	typedef void (*UpdateDelegate)(void*);

	// PURPOSE: Schedule a motion tree for update and/or compose
	// Schedule schedules both an update and compose, while ScheduleCompose only schedules a compose
	// PARAMS:
	// motionTree - motion tree to process
	// deltaTime - delta time, in seconds, to use during update
	// priority - importance of processing this tree (higher number is more important)
	// preDelegate, midDelegate, postDelegate
	//   - pointer to optional dependency called by worker thread before/after processing is started/finished
	// NOTES: Motion trees are added to a priority sorted queue.  When a worker
	// thread becomes available tree at the front of the queue is taken and processed.
	// If both update and compose were scheduled, then update is queued (with higher priority),
	// and once complete, compose is automatically entered into the queue to happen independently.
	// If false returned, tree will not be processed.  This is usually caused by
	// too many trees pending for processing simultaneously.  Wait and try again later.
	// If using delegates, ensure that any object being called by delegate
	// will persist and that code that is executed is *thread safe*, as execution will be
	// done by one of the worker threads at some later stage.
	void Schedule(crmtMotionTree& motionTree, float deltaTime, u32 priority=0, UpdateDelegate preDelegate=NULL, UpdateDelegate midDelegate=NULL, UpdateDelegate postDelegate=NULL, void* payload=NULL, bool update=true, bool compose=true, crmtMotionTree* parent=NULL);

	// PURPOSE: Test if processing on a particular motion tree has completed
	// PARAMS: motionTree - motion tree to check
	// updateOnly - only check for motion tree update, ignore any pending compose operations
	// RETURNS: true - processing complete, false - processing still pending
	// NOTES: This immediately returns a status.  To block on a particular
	// update completing don't poll this call, but use
	// WaitOnComplete() instead.
	bool IsComplete(const crmtMotionTree& motionTree, bool updateOnly=false);

	// PURPOSE: Wait on motion tree processing completing
	// PARAMS: motionTree - motion tree to check
	// elevatePriority - raise priority to encourage early processing by worker thread.
	// updateOnly - only check for motion tree update, ignore any pending compose operations
	// NOTES: Blocks on particular motion tree completing processing.
	// Can also encourage still pending motion tree to process as soon as possible
	// by elevating its priority (only works on trees still pending in the priority queue).
	// To simply check status, and then carry on doing other work,
	// use IsComplete() instead.
	void WaitOnComplete(crmtMotionTree& motionTree, bool elevatePriority=false, bool updateOnly=false);

	// PURPOSE: Wait on all processing pending in motion tree manager completing
	// NOTES: Blocks until all pending updates are complete.
	void WaitOnAllComplete();

private:

	// PURPOSE: Internal motion tree state
	enum { kStateIdle=0, kStateActive, kStatePending };

	// PURPOSE: Attach dependency to executing parent
	void Attach(sysDependency* dep, sysDependency* parent) const;

	// PURPOSE: Internal function, signal all threads inside the bitfield
	void SignalThreads(u32 threads);

	// PURPOSE: Internal function, wait for a signal
	void WaitThread(u32 threadIdx);

	// PURPOSE: Internal function, handles update task calls from dependency system
	static bool DependencyUpdateHandler(const sysDependency&);

	// PURPOSE: Internal function, handles recycle task calls from dependency system
	static bool DependencyRecycleHandler(const sysDependency&);

	// PURPOSE: Track number of active motion trees
	static const u32 sm_WaitOnAllFlag = 1u<<31;
	u32 m_NumActives;

	// PURPOSE: Semaphores per thread to wait on motion trees
	static const u32 sm_MaxNumThreads = 6;
	static __THREAD u32 sm_ThreadIdx;
	sysIpcSema m_Waits[sm_MaxNumThreads];
	u32 m_NumThreads;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_MOTIONTREESCHEDULER_H
