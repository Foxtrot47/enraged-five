//
// crmotiontree/nodeblendn.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEBLENDN_H
#define CRMOTIONTREE_NODEBLENDN_H

#include "node.h"
#include "request.h"

namespace rage
{

class crFrame;
class crFrameFilter;

////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Abstract base N input child nodes into a single output with weights and filters
class crmtNodeN : public crmtNodeParent
{
public:
	// PURPOSE: Default constructor
	crmtNodeN(eNodes nodeType);

	// PURPOSE: Resource constructor
	crmtNodeN(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeN();

	// PURPOSE: Initialization
	// filter - frame filter to use for this blend (default NULL)
	void Init(crFrameFilter* filter=NULL);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: Update node
	virtual void Update(float dt);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMinNumChildren() const;

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;

	// PURPOSE: override influence query
	virtual float GetChildInfluence(u32 childIdx) const;


	// PURPOSE: Set current weight for a particular child
	// PARAMS: childIdx - index of child node [0..(numChildren-1)]
	// weight - new weight percentage
	void SetWeight(u32 childIdx, float weight);

	// PURPOSE: Set weight delta for a particular child
	// PARAMS: childIdx - index of child node [0..(numChildren-1)]
	// weightRate - rate of change in weight percentage per second (can be +ve, -ve or zero)
	void SetWeightRate(u32 childIdx, float weightRate);

	// PURPOSE: Set the frame filter for this node
	// PARAMS: filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetFilter(crFrameFilter* filter);

	// PURPOSE: Set the input filter for a child
	// PARAMS:
	// childIdx - index of the filtered child
	// filter - pointer to the frame filter (can be NULL)
	// NOTES: filter will be reference counted
	void SetInputFilter(u32 childIdx, crFrameFilter* filter);

	// PURPOSE: Set child zero as destination in the n-way operation
	// PARAMS: zeroDest - use child zero as destination
	// NOTES: Changes the way input from first child behaves in n-way operation
	// First child becomes default result, with other inputs from
	// other children combined on to it (desirable behavior for layering)
	// Weight and input filter for first child will be ignored
	void SetZeroDestination(bool zeroDest);

	// PURPOSE: Get current weight for a particular child
	// PARAMS: childIdx - index of child node [0..(numChildren-1)]
	// RETURNS: Current weight value [0..1]
	// NOTES: This returns the raw, unmodified version of the the weight value
	float GetWeight(u32 childIdx) const;

	// PURPOSE: Get weight delta for a particular child
	// PARAMS: childIdx - index of child node [0..(numChildren-1)]
	// RETURNS: Current rate of change in weight percentage per second
	float GetWeightRate(u32 childIdx) const;

	// PURPOSE: Get the frame filter for this blend
	// RETURNS: pointer to the frame filter, can be NULL.
	crFrameFilter* GetFilter() const;

	// PURPOSE: Get the input filter of a child
	// PARAMS: childIdx - index of child node [0..(numChildren-1)]
	// RETURNS: pointer to the frame filter, can be NULL.
	crFrameFilter* GetInputFilter(u32 childIdx) const;

	// PURPOSE: Is child zero the destination in the n-way operation
	// RETURNS: true - child zero is destination, false - treated as normal source
	// NOTES: See SetZeroDestiniation for description
	bool IsZeroDestination() const;

	// PURPOSE: Adjust internal data to compensate for inserting a new node
	// PARAMS: childIdx - index of child node inserted [0..numChildren]
	// NOTES: Convenience, saves caller from having to manually shift through
	// repeated calls to get and set weights, weight rates etc
	void InsertData(u32 childIdx);

	// PURPOSE: Adjust internal data to compensate for removing a node
	// PARAMS: childIdx - index of child node removed [0..(numChildren-1)]
	// NOTES: Convenience, saves caller from having to manually shift through
	// repeated calls to get and set weights, weight rates etc
	void RemoveData(u32 childIdx);

	// PURPOSE: Set number of children
	void SetNumChildren(u32 n);

	// PURPOSE: Get number of children
	u32 GetNumChildren() const;

	// TODO --- add copy and swap data operations?


	// PURPOSE: Enumeration of messages
	enum
	{
		kMsgNZeroWeight = CRMT_PACK_MSG_ID(crmtNodeParent::kMsgEnd, kNodeN),

		kMsgEnd,
	};

	// PURPOSE: Maximum number of children N way nodes can simultaneously support.
	static const u32 sm_MaxChildren = 8;

	// PURPOSE: Internal structure for holding data related to child node's weights
	struct NData
	{
		// PURPOSE: Default constructor
		NData();

		// PURPOSE: Reset
		void Reset();

		float m_Weight;
		float m_WeightRate;

		crFrameFilter* m_InputFilter;
		// TODO --- modifier?
		// TODO --- destroy when complete?
	};

	// PURPOSE: Get internal data
	const NData* GetData() const;

protected:
	atRangeArray<NData, sm_MaxChildren> m_NData;
	crFrameFilter* m_Filter;
	bool m_ZeroDest;
	u32 m_NumChildren;
};


////////////////////////////////////////////////////////////////////////////////


// PURPOSE: Blend N input child nodes into a single output
class crmtNodeBlendN : public crmtNodeN
{
public:
	// PURPOSE: Default constructor
	crmtNodeBlendN();

	// PURPOSE: Resource constructor
	crmtNodeBlendN(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeBlendN();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeBlendN);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

private:

};

////////////////////////////////////////////////////////////////////////////////

inline float crmtNodeN::GetChildInfluence(u32 childIdx) const
{
	if(m_ZeroDest && !childIdx)
	{
		return 1.f;
	}
	return m_NData[childIdx].m_Weight;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crmtNodeN::IsZeroDestination() const
{
	return m_ZeroDest;
}

////////////////////////////////////////////////////////////////////////////////

inline const crmtNodeN::NData* crmtNodeN::GetData() const
{
	return &m_NData[0];
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crmtNodeN::GetNumChildren() const
{
	return m_NumChildren;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameFilter* crmtNodeN::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_NODEBLENDN_H
