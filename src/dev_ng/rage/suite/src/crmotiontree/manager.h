//
// crmotiontree/manager.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_MANAGER_H
#define CRMOTIONTREE_MANAGER_H

#include "atl/referencecounter.h"
#include "data/resource.h"
#include "data/struct.h"

namespace rage
{

class crmtMotionTree;
class crmtObserver;


// PURPOSE: Abstract base manager class
// Managers are wrappers for motion trees.
// They simplify the interface to the motion tree, by hiding the
// tree structure and presenting a more familiar paradigm to the
// end user instead.
class crmtManager
{
public:

	// PURPOSE: Default constructor
	crmtManager();

	// PURPOSE: Helpful constructor that allows provision of motion tree
	// SEE ALSO: Init()
	crmtManager(crmtMotionTree& tree, const crmtObserver* location=NULL);

	// PURPOSE: Destructor
	virtual ~crmtManager();
	
	// PURPOSE: Resource constructor
	crmtManager(datResource&);

	DECLARE_PLACE(crmtManager);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initialization
	// PARAMS: tree - motion tree this manager is going to manage
	// location - identifies root node, if manager is not controlling entire tree
	// NOTES: Will reference count motion tree, but external user
	// responsible for any resourcing.
	virtual void Init(crmtMotionTree& tree, const crmtObserver* location=NULL);

	// PURPOSE: Shutdown, release all dynamic allocation
	virtual void Shutdown();

	// PURPOSE: Reset, clear out all nodes represented by this manager
	virtual void Reset();

	// PURPOSE: Get the root node location
	// RETURNS: Pointer to observer which is attached to the root node (can be NULL)
	virtual const crmtObserver* GetRoot() const;

	// PURPOSE: Get the motion tree
	// RETURNS: Reference to the motion tree
	// NOTES: Asserts if called on a manager that has been shutdown, or not initialized
	crmtMotionTree& GetMotionTree() const;

private:
	crmtMotionTree* m_MotionTree;
};


// inlines

////////////////////////////////////////////////////////////////////////////////

inline crmtMotionTree& crmtManager::GetMotionTree() const
{
	FastAssert(m_MotionTree);
	return *m_MotionTree;
}

////////////////////////////////////////////////////////////////////////////////

inline const crmtObserver* crmtManager::GetRoot() const
{
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRMOTIONTREE_MANAGER_H
