//
// crmotiontree/nodeecapture.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//


#ifndef CRMOTIONTREE_NODECAPTURE_H
#define CRMOTIONTREE_NODECAPTURE_H

#include "node.h"

namespace rage
{

class crFrame;

// PURPOSE: Captures output from child node.
// Functionality somewhere between frame and extrapolate nodes.
// Captures output in frame if has child, if no child present
// supplies contents of frame to parent.
class crmtNodeCapture : public crmtNodeParent
{
public:

	// PURPOSE: Default constructor
	crmtNodeCapture();

	// PURPOSE: Resource constructor
	crmtNodeCapture(datResource&);

	// PURPOSE: Destructor
	virtual ~crmtNodeCapture();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(crmtNodeCapture);

	// PURPOSE: Initialization
	// PARAMS: frame - frame to use during capture
	void Init(crFrame* frame=NULL);

	// PURPOSE: Update node
	void Update(float dt);

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

	// PURPOSE: override maximum number of children this node can support
	virtual u32 GetMaxNumChildren() const;

	// PURPOSE: Refresh node when the creature changed
	virtual void Refresh();

	// PURPOSE: Set capture frame
	// PARAM: frame - pointer to an animation frame (can be NULL)
	void SetFrame(crFrame* frame);


	// PURPOSE: Get animation frame
	// RETURNS: animation frame (can be NULL)
	crFrame* GetFrame() const;

	// PURPOSE: Returns indices
	const crLock& GetIndices() const;

private:
	crLock m_Indices;
	crFrame* m_Frame;
	u32 m_FrameSignature;
};

////////////////////////////////////////////////////////////////////////////////

inline crFrame* crmtNodeCapture::GetFrame() const
{
	return m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

inline const crLock& crmtNodeCapture::GetIndices() const
{
	return m_Indices;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //CRMOTIONTREE_NODECAPTURE_H
