//
// crmotiontree/requestinvalid.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_REQUESTINVALID_H
#define CRMOTIONTREE_REQUESTINVALID_H

#include "request.h"

namespace rage
{
	
// PURPOSE: Invalid request
// Generates invalid degrees-of-freedom (aka no-op).
class crmtRequestInvalid : public crmtRequest
{
public:

	// PURPOSE: Default/Initializing constructor
	crmtRequestInvalid();

	// PURPOSE: Destructor
	virtual ~crmtRequestInvalid();

	// PURPOSE: Initializer
	void Init();

	// PURPOSE: Shutdown, free/release dynamic memory
	virtual void Shutdown();

protected:
	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
};

} // namespace rage

#endif // CRMOTIONTREE_REQUESTINVALID_H
