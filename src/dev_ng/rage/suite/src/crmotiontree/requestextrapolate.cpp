//
// crmotiontree/requestextrapolate.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "requestextrapolate.h"

#include "nodeextrapolate.h"

#include "cranimation/frame.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crmtRequestExtrapolate::crmtRequestExtrapolate()
: m_Damping(1.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestExtrapolate::crmtRequestExtrapolate(crmtRequest& source, float damping)
: m_Damping(1.f)
{
	Init(source, damping);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestExtrapolate::crmtRequestExtrapolate(float damping)
: m_Damping(1.f)
{
	Init(damping);
}

////////////////////////////////////////////////////////////////////////////////

crmtRequestExtrapolate::~crmtRequestExtrapolate()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestExtrapolate::Init(crmtRequest& source, float damping)
{
	SetSource(&source, 0);
	SetDamping(damping);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestExtrapolate::Init(float damping)
{
	SetDamping(damping);
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestExtrapolate::Shutdown()
{
	crmtRequestSource<1>::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crmtRequestExtrapolate::SetDamping(float damping)
{
	m_Damping = damping;
}

////////////////////////////////////////////////////////////////////////////////

float crmtRequestExtrapolate::GetDamping() const
{
	return m_Damping;
}

////////////////////////////////////////////////////////////////////////////////

crmtNode* crmtRequestExtrapolate::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	crmtNodeExtrapolate* extrapolateNode = crmtNodeExtrapolate::CreateNode(tree);
	Assert(extrapolateNode);

	extrapolateNode->Init(m_Damping);

	if(HasValidSource())
	{
		GenerateSourceNodes(tree, node, extrapolateNode);
	}

	return extrapolateNode;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage