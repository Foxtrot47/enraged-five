//
// crmotiontree/nodefactory.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRMOTIONTREE_NODEFACTORY_H
#define CRMOTIONTREE_NODEFACTORY_H

#define NODE_FACTORY_THREAD_SAFE (1)

#include "node.h"

#include "atl/pool.h"
#include "system/criticalsection.h"

namespace rage
{

class crmtNodeFactoryAllocator;

// PURPOSE: Node factory.
// Speeds up node allocation by allocates nodes from predefined pools of memory.
// Can be shared by multiple motion trees.


class crmtNodeFactory
{
public:

	// PURPOSE: Constructor
	crmtNodeFactory(const int sizes[], u8 numSizes);

	// PURPOSE: Destructor
	~crmtNodeFactory();

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();

	// PURPOSE: Create a pool of pages
	// PARAMS: numPages - number of pages in pool
	void CreatePagePool(u16 numPages);

	// PURPOSE: Allocate a new node of a particular type from a pool (or possibly dynamically allocate)
	// PARAMS: nodeType - type of node to allocate (see crmtNode::eNodes for enumeration of built in node types)
	// RETURNS: newly allocated node (from pool), will return NULL if allocation failed.
	// NOTES: Allocation can fail if there is no pool or pool for that node type is exhausted.
	crmtNode* NewNode(crmtNode::eNodes nodeType);

	// PURPOSE: Free a node allocated from a pool
	// PARAMS: node - node to free.
	// NOTES: Do not call more than once for a particular node.
	void DeleteNode(const crmtNode* node);

	// PURPOSE: Provides faster interface for batch deletes
	class Batch
	{
	public:

		// PURPOSE: Constructor - locks the batch critical section
		Batch(crmtNodeFactory& factory);

		// PURPOSE: Destructor - unlocks the batch critical section
		~Batch();

		// PURPOSE: Free a node allocated from a pool
		// PARAMS: Fast, bypasses thread safety checks
		void DeleteNode(const crmtNode* node);

	private:

		crmtNodeFactory* m_Factory;
	};

	friend class Batch;

protected:
	
	// PURPOSE: Internal version of node delete, with optional bypass of thread safety
	void DeleteNodeInternal(const crmtNode* node, bool unsafe);

private:

	static void InitNodeFactoryAllocator(crmtNodeFactory& factory, const int sizes[], u8 numSizes);
	static void DestroyNodeFactoryAllocator(crmtNodeFactory& factory);

	static void* Alloc(crmtNodeFactory& factory, const u32 size);
	static void Free(crmtNodeFactory& factory, void* p, const u32 size, bool unsafe);

	static u32 IndexToSize(const int index, const int sizes[]);
	static u32 SizeToIndex(const crmtNodeFactory& factory, const u32 size);

#if NODE_FACTORY_THREAD_SAFE
	sysCriticalSectionToken m_CsToken;
#endif // NODE_FACTORY_THREAD_SAFE

	atPoolBase* m_Pool;
	crmtNodeFactoryAllocator* m_Allocators;
	u32 m_NumAllocators;
	u32 m_MaxBytes;

	static const u32 kMaxAllocatorBytes = 512;
	static const u32 kChunkMemSize = 8192;
	u8 m_SizeToIndexTable[kMaxAllocatorBytes + 1];

	friend class crmtNodeFactoryAllocator;
};

////////////////////////////////////////////////////////////////////////////////

inline void crmtNodeFactory::DeleteNode(const crmtNode* node)
{
	DeleteNodeInternal(node, false);
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNodeFactory::Batch::Batch(crmtNodeFactory& factory)
: m_Factory(&factory)
{
#if NODE_FACTORY_THREAD_SAFE
	m_Factory->m_CsToken.Lock();
#endif // NODE_FACTORY_THREAD_SAFE
}

////////////////////////////////////////////////////////////////////////////////

inline crmtNodeFactory::Batch::~Batch()
{
#if NODE_FACTORY_THREAD_SAFE
	m_Factory->m_CsToken.Unlock();
#endif // NODE_FACTORY_THREAD_SAFE
}

////////////////////////////////////////////////////////////////////////////////

inline void crmtNodeFactory::Batch::DeleteNode(const crmtNode* node)
{
	Assert(node);
	Assert(node->m_Factory == m_Factory);

	m_Factory->DeleteNodeInternal(node, true);
}

////////////////////////////////////////////////////////////////////////////////

};  // namespace rage
#endif // CRMOTIONTREE_NODEFACTORY_H
