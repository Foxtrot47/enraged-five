//
// crstyletransfer/styleframe.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "styleframe.h"

#include "atl/array_struct.h"
#include "cranimation/frame.h"
#include "crskeleton/skeletondata.h"
#include "file/serialize.h"
#include "vectormath/classes.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////

crstAxisAngleRotation::crstAxisAngleRotation()
: m_axis(V_X_AXIS_WZERO)
, m_angle(V_ZERO)
{
}

////////////////////////////////////////////////////////////////////////////////

crstAxisAngleRotation::crstAxisAngleRotation(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crstAxisAngleRotation::~crstAxisAngleRotation()
{
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crstAxisAngleRotation);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crstAxisAngleRotation::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crstAxisAngleRotation);
	STRUCT_FIELD(m_axis);
	STRUCT_FIELD(m_angle);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::Serialize(datSerialize& s)
{
	// Version
	const int latestVersion = 1;

	int version = latestVersion;
	s << datLabel("AxisAngleVersion:") << version << datNewLine;

	if(version != latestVersion)
	{
		Errorf("crstAxisAngleRotation - attempting to load version '%d' (only '%d' supported)",
			version, latestVersion);
        return;
	}

	// Axis
	s << datLabel("Axis:") << m_axis << datNewLine;

	// Angle
	s << datLabel("Angle:") << m_angle << datNewLine;
}

////////////////////////////////////////////////////////////////////////////////

crstAxisAngleRotation::crstAxisAngleRotation(Vec3V_In axis, ScalarV_In angle)
{
	this->Set(axis, angle);
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::GetAxis(Vec3V_InOut axis) const
{
	axis = m_axis;
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::GetScaledAxis(Vec3V_InOut scaledAxis) const
{
	scaledAxis = Scale(m_axis, m_angle);
}

////////////////////////////////////////////////////////////////////////////////

ScalarV_Out crstAxisAngleRotation::GetAngle() const
{
	return m_angle;
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::SetAxis(Vec3V_In axis)
{
	m_axis = axis;
	NormalizeAxis();
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::SetAngle(ScalarV_In angle)
{
	m_angle = angle;
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::Set(Vec3V_In axis, ScalarV_In angle)
{
	m_axis = axis;
	NormalizeAxis();
	m_angle = angle;
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::Set(const crstAxisAngleRotation& toCopy)
{
	m_axis = toCopy.m_axis;
	m_angle = toCopy.m_angle;
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::Add(const crstAxisAngleRotation& otherRotation)
{
	Vec3V otherScaledAxis;
	otherRotation.GetScaledAxis(otherScaledAxis);

	this->m_axis = Scale(this->m_axis, this->m_angle);
	this->m_axis = this->m_axis + otherScaledAxis;

	this->m_angle = Mag(this->m_axis);
	NormalizeAxis();
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::Subtract(const crstAxisAngleRotation& otherRotation)
{
	Vec3V otherScaledAxis;
	otherRotation.GetScaledAxis(otherScaledAxis);

	this->m_axis = Scale(this->m_axis, m_angle);
	this->m_axis = this->m_axis - otherScaledAxis;

	this->m_angle = Mag(this->m_axis);
	NormalizeAxis();
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::Divide(float divisor)
{
	this->m_angle /= ScalarVFromF32(divisor);
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::Multiply(const crstAxisAngleRotation& otherRotation)
{
	Vec3V otherScaledAxis;
	otherRotation.GetScaledAxis(otherScaledAxis);

	this->m_axis = Scale(m_axis, m_angle);
	this->m_axis = Scale(m_axis, otherScaledAxis);

	this->m_angle = Mag(m_axis);
	NormalizeAxis();
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::Print() const
{
	Printf("Axis: (%f, %f, %f) Angle: %f\n", m_axis[0], m_axis[1], m_axis[2], m_angle.Getf());
}

////////////////////////////////////////////////////////////////////////////////

void crstAxisAngleRotation::NormalizeAxis()
{
	this->m_axis = Normalize(m_axis);
}

////////////////////////////////////////////////////////////////////////////////

crstStyleFrame::crstStyleFrame()
: m_moverOrient(),
  m_jointOrientations(0, 0),
  m_jointIDs(0, 0)
{
	// Timing Info
	m_timing = log(1.0f);

	// Root
	m_rootTranslation = Vec3V(V_ZERO);

	// Global
	m_moverX = 0;
	m_moverY = 0;
	m_moverZ = 0;

	m_deltaTime = 1;
}

////////////////////////////////////////////////////////////////////////////////

crstStyleFrame::crstStyleFrame(datResource& rsc)
: m_moverOrient(rsc)
, m_jointOrientations(rsc, true)
, m_jointIDs(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crstStyleFrame::crstStyleFrame(const crstStyleFrame& toCopy)
{
	this->Set(toCopy);
}

////////////////////////////////////////////////////////////////////////////////

crstStyleFrame::~crstStyleFrame()
{
	m_jointOrientations.clear();
	m_jointIDs.clear();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crstStyleFrame);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crstStyleFrame::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crstStyleFrame);
	STRUCT_FIELD(m_deltaTime);
	STRUCT_FIELD(m_timing);
	STRUCT_FIELD(m_rootTranslation);
	STRUCT_FIELD(m_moverX);
	STRUCT_FIELD(m_moverY);
	STRUCT_FIELD(m_moverZ);
	STRUCT_FIELD(m_moverOrient);
	STRUCT_FIELD(m_jointOrientations);
	STRUCT_FIELD(m_jointIDs);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::Serialize(datSerialize& s)
{
	// Version
	const int latestVersion = 6;

	int version = latestVersion;
	s << datLabel("StyleFrameVersion:") << version << datNewLine;

	if(version != latestVersion)
	{
		Errorf("crstStyleFrame - attempting to load version '%d' (only '%d' supported)",
			version, latestVersion);
        return;
	}

	// Timing
	s << datLabel("Timing:") << m_timing << datNewLine;

	// Root
	s << datLabel("RootTranslation:") << m_rootTranslation << datNewLine;

	// Mover
	s << datLabel("Mover:") << m_moverX <<  m_moverY << m_moverZ << datNewLine;
	m_moverOrient.Serialize(s);

	// Delta Time
	s << datLabel("DeltaTime:") << m_deltaTime << datNewLine;
	
	// Joint Orientations
	int numJoints = this->m_jointOrientations.size();
	s << datLabel("NumJoints:") << numJoints << datNewLine;

	for(int i = 0; i < numJoints; i++)
	{
		if(s.IsRead())
		{
			m_jointOrientations.PushAndGrow(crstAxisAngleRotation());
			m_jointIDs.PushAndGrow(0);
		}
		s << m_jointIDs[i];
		m_jointOrientations[i].Serialize(s);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::Set(const crstStyleFrame& toCopy)
{
	// Delta Time
	m_deltaTime = toCopy.m_deltaTime;

	// Timing Info
	m_timing = toCopy.m_timing;

	// Root Translation
	m_rootTranslation = toCopy.m_rootTranslation;

	// Global
	m_moverX = toCopy.m_moverX;
	m_moverY = toCopy.m_moverY;
	m_moverZ = toCopy.m_moverZ;
	m_moverOrient = toCopy.m_moverOrient;

	// Orientations
	m_jointOrientations.clear();
	m_jointIDs.clear();
	m_jointOrientations.Reset();
	m_jointOrientations.Resize(toCopy.GetNumJoints());
	m_jointIDs.Reset();
	m_jointIDs.Resize(toCopy.GetNumJoints());
	for(int i = 0; i < toCopy.GetNumJoints(); i++)
	{
		m_jointOrientations[i] = toCopy.m_jointOrientations[i];
		m_jointIDs[i] = toCopy.m_jointIDs[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::Set(const crFrame& animFrame,
						 const crSkeletonData& skelData, float deltaTime,
						 int upIndex /* = 1 */)
{
	// Some variables we'll use later
	Vec3V axis;
	ScalarV angle;

	// Clamp upIndex to [0,2]
	upIndex = Max(0, upIndex);
	upIndex = Min(2, upIndex);

	// Initialize Everything
	int numBones = skelData.GetNumBones();
	this->m_jointOrientations.Reset();
	this->m_jointOrientations.Resize(numBones);
	this->m_jointIDs.Reset();
	this->m_jointIDs.Resize(numBones);
	this->m_deltaTime = deltaTime ? deltaTime : 1;

	// Containers for later

	////// Global Stuff First
	TransformV moverSituation;
	animFrame.GetMoverSituation(moverSituation);
	QuatV orientation = moverSituation.GetRotation();
	Vec3V translation = moverSituation.GetPosition();

	// Get ground plane motion
	bool floorXSet = false;
	for(int i = 0; i < 3; i++)
	{
		if(i != upIndex)
		{
			if(floorXSet)
			{
				m_moverZ = translation[i]/this->m_deltaTime;
			}
			else
			{
				m_moverX = translation[i]/this->m_deltaTime;
				floorXSet = true;
			}
		}
		else
		{
			m_moverY = translation[i]/this->m_deltaTime;
		}
	}

	// Get mover rotation
	QuatVToAxisAngle(axis, angle, orientation);
	this->m_moverOrient.Set(axis, angle/ScalarVFromF32(this->m_deltaTime));

	////// Go through each bone and convert
	for(u16 boneIdx = 0; boneIdx < numBones; boneIdx++)
	{
		// Grab the bone id
		u16 boneID = 0;
		skelData.ConvertBoneIndexToId(boneIdx, boneID);

		// Get the rotation and translation components of the bone
		animFrame.GetBoneRotation(boneID, orientation);
		animFrame.GetBoneTranslation(boneID, translation);

		// Do orientation part
		QuatVToAxisAngle(axis, angle, orientation);

		this->m_jointOrientations[boneIdx].Set(axis, angle);
		this->m_jointIDs[boneIdx] = boneID;

		if(!boneIdx)
		{
			// We have the root

			// Translation Component
			this->m_rootTranslation = translation;
		}
	}

	// Timing - since there is no reference frames at the moment, we assume the
	//          timing is 1 to 1
	this->m_timing = log(1.0f);
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::SetTiming(float timing)
{
	this->m_timing = log(timing);
}

////////////////////////////////////////////////////////////////////////////////

float crstStyleFrame::GetAnimFrame(crFrame& animFrame,
								   int upIndex /* = 1 */) const
{
	// Some variables we'll use later
	Vec3V axis;
	ScalarV angle;

	// Clamp upIndex to [0,2]
	upIndex = Max(0, upIndex);
	upIndex = Min(2, upIndex);

	int numBones = this->GetNumJoints();

	// Containers for later
	QuatV orientation(V_IDENTITY);
	Vec3V location(V_ZERO);

	////// Global Stuff First

	// Translation
	bool floorXSet = false;
	for(int i = 0; i < 3; i++)
	{
		if(i != upIndex)
		{
			if(floorXSet)
			{
				location[i] = m_moverZ*this->m_deltaTime;
			}
			else
			{
				location[i] = m_moverX*this->m_deltaTime;
				floorXSet = true;
			}
		}
		else
		{
			location[i] = m_moverY*this->m_deltaTime;
		}
	}

	// Orientation
	m_moverOrient.GetAxis(axis);
	angle = m_moverOrient.GetAngle();
	orientation = QuatVFromAxisAngle(axis, angle*ScalarVFromF32(m_deltaTime));

	TransformV moverSituation = TransformV(orientation, location);
	animFrame.SetMoverSituation(moverSituation);

	////// Go through each bone and convert
	for(u16 boneIdx = 0; boneIdx < numBones; boneIdx++)
	{
		u16 boneID = m_jointIDs[boneIdx];

		// Orientation Component
		this->m_jointOrientations[boneIdx].GetAxis(axis);
		angle = this->m_jointOrientations[boneIdx].GetAngle();

		orientation = QuatVFromAxisAngle(axis, angle);

		animFrame.SetBoneRotation(boneID, orientation);

		if(!boneIdx)
		{
			// We have the root
			// Translation Component
			location = m_rootTranslation;
			animFrame.SetBoneTranslation(boneID, location);
		}
	}

	// Timing
	return exp(m_timing);
}

////////////////////////////////////////////////////////////////////////////////

float crstStyleFrame::GetTiming() const
{
	return exp(this->m_timing);
}

////////////////////////////////////////////////////////////////////////////////

float crstStyleFrame::GetDeltaTime() const
{
	return this->m_deltaTime;
}

////////////////////////////////////////////////////////////////////////////////

int crstStyleFrame::GetNumJoints() const
{
	return this->m_jointOrientations.size();
}

////////////////////////////////////////////////////////////////////////////////

bool crstStyleFrame::GetJointOrientation(int jointNum,
										 crstAxisAngleRotation& result) const
{
	if((jointNum >= 0) && (jointNum < this->GetNumJoints()))
	{
		result.Set(this->m_jointOrientations[jointNum]);
		return true;
	}
	else
	{
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::ToFeatureVector(VectorT<float>& featureVector,
									 const VectorT<float>& meanVector,
									 const VectorT<float>& varianceVector,
									 crstFeatureType featureType,
									 int auxFeatureIndex /* = 0 */,
									 float epsilon /* = 0.0000001f */) const
{
	// What type of feature was requested?
	switch(featureType)
	{
	case FEATURE_TIMING:
		{
			featureVector.Init(1, 0);
			featureVector[0] = this->m_timing;
		}
		break;
	case FEATURE_MOVER:
		{
			featureVector.Init(2, 0);
			featureVector[0] = this->m_moverX;
			featureVector[1] = this->m_moverZ;
		}
		break;
	case FEATURE_ROOT_OFFSET:
		{
			featureVector.Init(3, 0);
			featureVector[0] = this->m_rootTranslation[0];
			featureVector[1] = this->m_rootTranslation[1];
			featureVector[2] = this->m_rootTranslation[2];
		}
		break;
	case FEATURE_JOINT_ORIENTATION:
		{
			featureVector.Init(3, 0);
			Vec3V scaledAxis;
			m_jointOrientations[auxFeatureIndex].GetScaledAxis(scaledAxis);

			featureVector[0] = scaledAxis[0];
			featureVector[1] = scaledAxis[1];
			featureVector[2] = scaledAxis[2];
		}
		break;
	default:
		Errorf("crstStyleFrame - Cannot get feature.  Unknown feature type.");
		break;
	}

	// Normalize if provided with mean and variance values
	int dims = featureVector.GetSize();
	bool doShift = false;
	if((meanVector.GetSize() >= dims) && (varianceVector.GetSize() >= dims))
		doShift = true;

	for(short featureNum = 0; featureNum < dims; featureNum++)
	{
		if(doShift)
		{
			featureVector[featureNum] -= meanVector[featureNum];
			if(varianceVector[featureNum] > epsilon)
				featureVector[featureNum] /= sqrt(varianceVector[featureNum]);
			else
				featureVector[featureNum] /= sqrt(epsilon);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::FromFeatureVector(VectorT<float>& featureVector,
									   const VectorT<float>& meanVector,
									   const VectorT<float>& varianceVector,
									   crstFeatureType featureType,
									   int auxFeatureIndex /* = 0 */,
									   float epsilon /* = 0.0000001f */)
{
	// Denormalize if provided with mean and variance values
	int dims = featureVector.GetSize();
	bool doShift = false;
	if((meanVector.GetSize() >= dims) && (varianceVector.GetSize() >= dims))
		doShift = true;

	for(short featureNum = 0; featureNum < dims; featureNum++)
	{
		if(doShift)
		{
			if(varianceVector[featureNum] > epsilon)
				featureVector[featureNum] *= sqrt(varianceVector[featureNum]);
			else
				featureVector[featureNum] *= sqrt(epsilon);
			featureVector[featureNum] += meanVector[featureNum];
		}
	}
	
	// What type of feature was provided?
	switch(featureType)
	{
	case FEATURE_TIMING:
		{
			this->m_timing = featureVector[0];
		}
		break;
	case FEATURE_MOVER:
		{
			this->m_moverX = featureVector[0];
			this->m_moverZ = featureVector[1];
		}
		break;
	case FEATURE_ROOT_OFFSET:
		{
			this->m_rootTranslation[0] = featureVector[0];
			this->m_rootTranslation[1] = featureVector[1];
			this->m_rootTranslation[2] = featureVector[2];
		}
		break;
	case FEATURE_JOINT_ORIENTATION:
		{
			Vec3V scaledAxis;
			scaledAxis[0] = featureVector[0];
			scaledAxis[1] = featureVector[1];
			scaledAxis[2] = featureVector[2];

			ScalarV angle = Mag(scaledAxis);

			this->m_jointOrientations[auxFeatureIndex].Set(scaledAxis, angle);
		}
		break;
	default:
		Errorf("crstStyleFrame - Cannot set from feature.  Unknown feature type.");
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::BlendInto(const VectorT<float>& featureVector,
							   float blendTerm, crstFeatureType featureType,
							   int auxFeatureIndex /* = 0 */)
{
	if(!blendTerm) return;

	// Clamp Blend Term to [0.0f,1.0f]
	blendTerm = Min(1.0f, blendTerm);
	blendTerm = Max(0.0f, blendTerm);

	float oppositeBlend = 1.0f - blendTerm;
	
	// What type of feature are we blending?
	switch(featureType)
	{
	case FEATURE_TIMING:
		{
			this->m_timing = oppositeBlend*this->m_timing +
				blendTerm*featureVector[0];
		}
		break;
	case FEATURE_MOVER:
		{
			this->m_moverX = oppositeBlend*this->m_moverX +
				blendTerm*featureVector[0];
			this->m_moverZ = oppositeBlend*this->m_moverZ +
				blendTerm*featureVector[1];
		}
		break;
	case FEATURE_ROOT_OFFSET:
		{
			this->m_rootTranslation[0] =
				oppositeBlend*this->m_rootTranslation[0] +
				                  blendTerm*featureVector[0];
			this->m_rootTranslation[1] =
				oppositeBlend*this->m_rootTranslation[1] +	
				                  blendTerm*featureVector[1];
			this->m_rootTranslation[2] =
				oppositeBlend*this->m_rootTranslation[2] +	
				                  blendTerm*featureVector[2];
		}
		break;
	case FEATURE_JOINT_ORIENTATION:
		{
			Vec3V scaledAxis;
			m_jointOrientations[auxFeatureIndex].GetScaledAxis(scaledAxis);
			scaledAxis[0] = oppositeBlend*scaledAxis[0] +
				blendTerm*featureVector[0];
			scaledAxis[1] = oppositeBlend*scaledAxis[1] +
				blendTerm*featureVector[1];
			scaledAxis[2] = oppositeBlend*scaledAxis[2] +
				blendTerm*featureVector[2];

			ScalarV angle = Mag(scaledAxis);

			this->m_jointOrientations[auxFeatureIndex].Set(scaledAxis, angle);
		}
		break;
	default:
		Errorf("crstStyleFrame - Cannot blend feature.  Unknown feature type.");
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::Add(const crstStyleFrame& otherFrame)
{
	// Delta Time
	m_deltaTime += otherFrame.m_deltaTime;

	// Timing Info
	m_timing += otherFrame.m_timing;

	// Root Translation
	m_rootTranslation += otherFrame.m_rootTranslation;
	
	// Global
	m_moverX += otherFrame.m_moverX;
	m_moverY += otherFrame.m_moverY;
	m_moverZ += otherFrame.m_moverZ;
	m_moverOrient.Add(otherFrame.m_moverOrient);

	// Orientations
	for(int i = 0; i < this->GetNumJoints(); i++)
	{
		m_jointOrientations[i].Add(otherFrame.m_jointOrientations[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::Subtract(const crstStyleFrame& otherFrame)
{
	// Delta Time
	m_deltaTime -= otherFrame.m_deltaTime;

	// Timing Info
	m_timing -= otherFrame.m_timing;

	// Root Translation
	m_rootTranslation -= otherFrame.m_rootTranslation;
	
	// Global
	m_moverX -= otherFrame.m_moverX;
	m_moverY -= otherFrame.m_moverY;
	m_moverZ -= otherFrame.m_moverZ;
	m_moverOrient.Subtract(otherFrame.m_moverOrient);

	// Orientations
	for(int i = 0; i < this->GetNumJoints(); i++)
	{
		m_jointOrientations[i].Subtract(otherFrame.m_jointOrientations[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::Divide(float divisor)
{
	// Delta Time
	m_deltaTime /= divisor;

	// Timing Info
	m_timing /= divisor;

	// Root Translation
	m_rootTranslation /= ScalarVFromF32(divisor);
	
	// Global
	m_moverX /= divisor;
	m_moverY /= divisor;
	m_moverZ /= divisor;
	m_moverOrient.Divide(divisor);

	// Orientations
	for(int i = 0; i < this->GetNumJoints(); i++)
	{
		m_jointOrientations[i].Divide(divisor);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleFrame::Multiply(const crstStyleFrame& otherFrame)
{
	// Delta Time
	m_deltaTime *= otherFrame.m_deltaTime;

	// Timing Info
	m_timing = m_timing*otherFrame.m_timing;

	// Root Translation
	m_rootTranslation *= otherFrame.m_rootTranslation;
	
	// Global
	m_moverX *= otherFrame.m_moverX;
	m_moverY *= otherFrame.m_moverY;
	m_moverZ *= otherFrame.m_moverZ;
	m_moverOrient.Multiply(otherFrame.m_moverOrient);

	// Orientations
	for(int i = 0; i < this->GetNumJoints(); i++)
	{
		m_jointOrientations[i].Multiply(otherFrame.m_jointOrientations[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

#if !__NO_OUTPUT

void crstStyleFrame::Print() const
{
	Printf("Style Frame:\n");
	Printf("Timing: %f\n", this->m_timing);
	Printf("Mover: [%f %f %f] with Delta Time = %f and orientation ",
		this->m_moverX, this->m_moverY, this->m_moverZ, this->m_deltaTime);
	this->m_moverOrient.Print();
	this->m_rootTranslation.Print("Root Translation:");
	Printf("Joint Orientations:\n");
	for(int i = 0; i < this->GetNumJoints(); i++)
	{
		Printf("   (Index-%d ID-%d):  ", i, this->m_jointIDs[i]);
		this->m_jointOrientations[i].Print();
	}
}

#endif

////////////////////////////////////////////////////////////////////////////////

u16 crstStyleFrame::GetIDFromOrientIndex(int orientIndex)
{
	return m_jointIDs[orientIndex];
}
