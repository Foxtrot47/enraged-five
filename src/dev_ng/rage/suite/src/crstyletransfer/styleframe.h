//
// crstyletransfer/styleframe.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRSTYLETRANSFER_STYLEFRAME_H
#define CRSTYLETRANSFER_STYLEFRAME_H

#include "atl/array.h"
#include "vector/vectort.h"
#include "vectormath/vec3v.h"

namespace rage
{

// Forward Declarations
class datSerialize;
class crFrame;
class crSkeletonData;

// PURPOSE: Axis-Angle Representation of a Rotation
class crstAxisAngleRotation
{
public:
	// PURPOSE: Default constructor
	crstAxisAngleRotation();

	// PURPOSE: Resource constructor
	crstAxisAngleRotation(datResource&);

	// PURPOSE: Construct a new axis angle rotation with the
	//          specified axis and angle
	// PARAMS: axis - the axis for this rotation
	//         angle - the angle (in Radians) for this rotation
	crstAxisAngleRotation(Vec3V_In axis, ScalarV_In angle);

	// PURPOSE: Destructor
	~crstAxisAngleRotation();

	// PURPOSE: Placement
	DECLARE_PLACE(crstAxisAngleRotation);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Get the axis of the rotation
	// PARAMS: axis - a Vector3 to store the axis of rotation in
	void GetAxis(Vec3V_InOut axis) const;

	// PURPOSE: Get the axis of the rotation scaled by the angle (in radians)
	//          of the rotation
	// PARAMS: scaledAxis - a Vector3 to store the scaled axis of rotation in
	void GetScaledAxis(Vec3V_InOut scaledAxis) const;

	// PURPOSE: Get the angle of the rotation
	// RETURNS: float representing the angle (in Radians) of the rotation
	ScalarV_Out GetAngle() const;

	// PURPOSE: Set the axis of rotation
	// PARAMS: axis - the new axis for this rotation
	void SetAxis(Vec3V_In axis);

	// PURPOSE: Set the angle of rotation
	// PARAMS: angle - the new angle (in Radians) for this rotation
	void SetAngle(ScalarV_In angle);

	// PURPOSE: Set the axis and angle of rotation
	// PARAMS: axis - the new axis for this rotation
	//         angle - the new angle (in Radians) for this rotation
	void Set(Vec3V_In axis, ScalarV_In angle);

	// PURPOSE: Set the rotation from another rotation
	// PARAMS: toCopy - the axis angle rotation to copy
	void Set(const crstAxisAngleRotation& toCopy);

	// PURPOSE: Add another rotation to this one (equivalent to adding
	//          the scaled axis vectors of the two rotations)
	// PARAMS: otherRotation - the other rotation to add to this one
	void Add(const crstAxisAngleRotation& otherRotation);

	// PURPOSE: Subtract another rotation from this one (equivalent to
	//          subtracting the scaled axis vectors of the two rotations)
	// PARAMS: otherRotation - the other rotation to subtract from this one
	void Subtract(const crstAxisAngleRotation& otherRotation);

	// PURPOSE: Divide this rotation by the divisor (equivalent to dividing
	//          the angle by the divisor)
	// PARAMS: divisor - number to divide the rotation by
	void Divide(float divisor);

	// PURPOSE: Multiply another rotation by this one (equivalent to
	//          multiplying the DOFs of the scaled axis vectors of the two
	//          rotations)
	// PARAMS: otherRotation - the other rotation to multiply by this one
	void Multiply(const crstAxisAngleRotation& otherRotation);

	// PURPOSE:  Print information about this rotation to standard out
	void Print() const;

	// PURPOSE: Serialize the Style Frame to/from a file
	// PARAMS: datSerialize - the serialization object for the file
	void Serialize(datSerialize&);

private:
	void NormalizeAxis();

	// Rotation Data
	Vec3V m_axis;
	ScalarV m_angle;
};

// PURPOSE:  For style translation, a number of different types of animation
//           features can be related to one another.  This enumeration defines
//           the possible types of these features.
enum crstFeatureType
{
	// 1D: Log of the number of reference frames the frame corresponds to
	FEATURE_TIMING,
	// 2D:  Differential values for movement along the ground plane (2D)
	FEATURE_MOVER,
	// 3D:  The offset of the root from the mover
	FEATURE_ROOT_OFFSET,
	// 3D:  The local orientation of a particular joint, represented as a
	//      scaled axis-angle vector
	FEATURE_JOINT_ORIENTATION,
	FEATURE_NUM_TYPES
};

// PURPOSE: Contains a frame of animation represented as the
//          style transfer algorithm needs it to be
class crstStyleFrame
{
public:
	// PURPOSE: Default constructor
	crstStyleFrame();

	// PURPOSE: Resource constructor
	crstStyleFrame(datResource&);

	// PURPOSE: Construct a new style frame the
	//          is a copy of the supplied one
	// PARAMS: toCopy - the frame to copy
	crstStyleFrame(const crstStyleFrame& toCopy);

	// PURPOSE: Destructor
	~crstStyleFrame();

	// PURPOSE: Placement
	DECLARE_PLACE(crstStyleFrame);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Set this crstStyleFrame to be equivalent to
	//          the supplied crFrame
	// PARAMS:  animFrame - the crFrame containing the data
	//                      we want to convert into this style frame
	//          skelData - the skeleton data for the skeleton associated with
	//                     this animation frame
	//          deltaTime - amount of time represented by this frame (i.e.
	//                      sampling rate)
	//          upIndex - Defines which dimension is considered the up axis
	//                    (0 = x-axis, 1 = y-axis, and 2 = z-axis)
	void Set(const crFrame& animFrame, const crSkeletonData& skelData,
		float deltaTime, int upIndex = 1);

	// PURPOSE: Copy a specified style frame into this one
	// PARAMS: toCopy - the frame to copy
	void Set(const crstStyleFrame& toCopy);

	// PURPOSE: Set timing data for this frame
	// PARAMS: timing - float representing the number of 'normal' speed frames
	//          this frame should represent
	void SetTiming(float timing);

	// PURPOSE: Get a new crFrame representation of this crstStyleFrame
	// PARAMS: animFrame - crFrame to store the result in.  It is assumed
	//                     that this frame has already been initialized with
	//                     the correct DOFs
	//         upIndex - Defines which dimension is considered the up axis
	//                   (0 = x-axis, 1 = y-axis, and 2 = z-axis)
	// RETURNS: a float indicating the timing for this frame
	float GetAnimFrame(crFrame& animFrame, int upIndex = 1) const;

	// PURPOSE: Get timing data from this frame
	// RETURNS: float representing the number of 'normal' speed frames
	//          this frame now represents
	float GetTiming() const;

	// PURPOSE: Get delta time value for this frame
	// RETURNS: float for the sampling rate of this frame
	float GetDeltaTime() const;

	// PURPOSE: Get the number of joints in the skeleton that corresponds
	//          to this frame
	// RETURNS: int specifying number of joints in the skeleton
	int GetNumJoints() const;
	
	// PURPOSE: Get the joint orientation for specified joint
	// PARAMS: jointNum - the joint whose orientation is desired
	//         result - crstAxisAngleRotation for storing the orientation
	//                   of the joint
	// RETURNS: true or false depending on whether the method successfully
	//          found the joint orientation or not
	bool GetJointOrientation(int jointNum, crstAxisAngleRotation& result) const;

	// PURPOSE: Get a feature vector for this crstStyleFrame.  (If supplied
	//          with the means and variances of the features, the feature
	//          vector will be normalized).
	// PARAMS: featureVector - Container for holding the feature vector
	//         meanVector - the means for each feature.
	//         varianceVector - the variances for each feature.
	//         featureType - The type of feature vector desired:
	//                       See crstFeatureType enumeration.
	//         auxFeatureIndex - A FEATURE_JOINT_ORIENTATION feature needs an
	//                           index value to know which joint orientation is
	//                           desired.  This parameter is ignored for all
	//                           other feature types.
	//         epsilon - a variance of <= epsilon is considered 0
	// NOTE: If (meanVector.size() < # requested features) or
	//       (varianceVector.size() < # requested features), the feature vector
	//       will not be normalized
	void ToFeatureVector(VectorT<float>& featureVector,
		const VectorT<float>& meanVector, const VectorT<float>& varianceVector,
		crstFeatureType featureType, int auxFeatureIndex = 0,
		float epsilon = 0.0000001f) const;

	// PURPOSE: Set some DOFs of this frame from the specified feature vector.
	//          (If supplied with the means and variances of the features, the
	//          feature vector will be denormalized).
	// PARAMS: featureVector - the feature vector
	//         meanVector - the means for each feature.
	//         varianceVector - the variances for each feature.
	//         featureType - The type of feature vector desired:
	//                       See crstFeatureType enumeration.
	//         auxFeatureIndex - A FEATURE_JOINT_ORIENTATION feature needs an
	//                           index value to know which joint orientation is
	//                           desired.  This parameter is ignored for all
	//                           other feature types.
	//         epsilon - a variance of <= epsilon is considered 0
	// NOTE: If (meanVector.size() < featureVector.size()) or
	//       (varianceVector.size() < featureVector.size()), the feature vector
	//       will not be denormalized
	void FromFeatureVector(VectorT<float>& featureVector,
		const VectorT<float>& meanVector, const VectorT<float>& varianceVector,
		crstFeatureType featureType, int auxFeatureIndex = 0,
		float epsilon = 0.0000001f);

	// PURPOSE:  Blend a feature vector into this style frame using the
	//           supplied alpha value.
	// PARAMS:  featureVector - the feature vector to blend in
	//          blendTerm - the alpha value to apply to the feature vector.
	//                      this value will be clamped to the range [0,1]
	//         featureType - The type of feature vector desired:
	//                       See crstFeatureType enumeration.
	//         auxFeatureIndex - A FEATURE_JOINT_ORIENTATION feature needs an
	//                           index value to know which joint orientation is
	//                           desired.  This parameter is ignored for all
	//                           other feature types.
	void BlendInto(const VectorT<float>& featureVector, float blendTerm,
		crstFeatureType featureType, int auxFeatureIndex = 0);

	// PURPOSE: For each DOF, add the feature value from another frame to this
	//          one
	// PARAMS: otherFrame - the other frame to add to this one
	void Add(const crstStyleFrame& otherFrame);

	// PURPOSE: For each DOF, subtract the feature value from another frame
	//          from this one
	// PARAMS: otherFrame - the other frame to subtract from this one
	void Subtract(const crstStyleFrame& otherFrame);

	// PURPOSE: For each DOF, divide this frame by the divisor
	// PARAMS: divisor - number to divide the frame by
	void Divide(float divisor);

	// PURPOSE: For each DOF, multiply the feature value by the feature value
	//          of another frame
	// PARAMS: otherFrame - the other frame to multiply to this one
	void Multiply(const crstStyleFrame& otherFrame);

	// PURPOSE:  Print information about this style frame to standard out
	void Print() const;

	// PURPOSE: Serialize the Style Frame to/from a file
	// PARAMS: datSerialize - the serialization object for the file
	void Serialize(datSerialize&);

	// PURPOSE:  Get the id for the joint whose joint orientation is stored
	//           inside this style frame at a specified index.
	// PARAMS:  orientIndex - the queried joint index
	// RETURNS:  the joint id for the joint associated with the specified index
	u16 GetIDFromOrientIndex(int orientIndex);

private:
	// Delta Time for this Frame
	float m_deltaTime;

	// Timing Info
	float m_timing;

	// Root Info
	Vec3V m_rootTranslation;

	// Info About Mover
	float m_moverX;
	float m_moverY;
	float m_moverZ;
	crstAxisAngleRotation m_moverOrient;

	// Other Joints Info
	atArray< crstAxisAngleRotation > m_jointOrientations;
	atArray< u16 > m_jointIDs;
};

}; // namespace rage

#endif // CRSTYLETRANSFER_STYLEFRAME_H
