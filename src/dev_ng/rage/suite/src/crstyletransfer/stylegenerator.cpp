//
// crstyletransfer/stylegenerator.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "stylegenerator.h"

#include "style.h"
#include "styleframe.h"
#include "cranimation/animation.h"
#include "crskeleton/skeletondata.h"
#include "cranimation/frame.h"
#include "correspondencegenerator.h"
#include "mathext/subspaceid.h"
#include "cranimation/weightset.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////

crstStyleGenerator::crstStyleGenerator()
: m_SkeletonData(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crstStyleGenerator::crstStyleGenerator(crSkeletonData& skelData)
: m_SkeletonData(NULL)
{
	Init(skelData);
}

////////////////////////////////////////////////////////////////////////////////

crstStyleGenerator::~crstStyleGenerator()
{
	m_SkeletonData = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleGenerator::Init(const crSkeletonData& skelData)
{
	m_SkeletonData = &skelData;
}

////////////////////////////////////////////////////////////////////////////////

crstStyle* crstStyleGenerator::CreateStyle(const crAnimation& unstylizedAnim,
										   const crAnimation& stylizedAnim,
										   crWeightSet& weightSet,
										   int upIndex, short slopeLimit,
										   bool useIMW,
										   short imwIterations /* = 1 */,
										   float imwWeight /* = 100 */)
{
	crstStyle* newStyle = rage_new crstStyle();

	float unstylizedDuration = unstylizedAnim.GetDuration();
	float stylizedDuration = stylizedAnim.GetDuration();
	u32 unstylizedNumFrames = unstylizedAnim.GetNumInternalFrames();
	u32 stylizedNumFrames = stylizedAnim.GetNumInternalFrames();

	// Update at the maximum fps available in the original animations
	// (i.e. get the most from what we have)
	float deltaTime = Max(unstylizedDuration/unstylizedNumFrames,
		stylizedDuration/stylizedNumFrames);

	// Start by translating the entire unstylized animation into the new
	// representation
	atArray< crstStyleFrame* > unstylizedFrames;
	crstStyleFrame unstylizedMean = crstStyleFrame();
	crstStyleFrame unstylizedVariance = crstStyleFrame();
	this->ConvertAnimation(unstylizedFrames, unstylizedMean, unstylizedVariance,
		deltaTime, unstylizedAnim, upIndex);


	// Now, translate the entire stylized animation into the new representation
	atArray< crstStyleFrame* > stylizedFrames;
	crstStyleFrame stylizedMean = crstStyleFrame();
	crstStyleFrame stylizedVariance = crstStyleFrame();
	if(useIMW) this->ConvertAnimation(stylizedFrames, stylizedMean,
		stylizedVariance, deltaTime, stylizedAnim, upIndex);

	VectorT<float> repetitionVector;
	crstCorrespondenceGenerator correspondenceGenerator;
	correspondenceGenerator.Init(*this->m_SkeletonData);
	if(useIMW)
		correspondenceGenerator.DetermineCorrespondence(unstylizedFrames,
		stylizedFrames, slopeLimit, repetitionVector, weightSet, imwIterations,
		imwWeight);
	else correspondenceGenerator.DetermineCorrespondence(unstylizedAnim,
		stylizedAnim, slopeLimit, repetitionVector, weightSet, deltaTime);

	for(int i = 0; i < stylizedFrames.size(); i++)
	{
		delete(stylizedFrames[i]);
	}
	stylizedFrames.clear();

	this->ConvertAnimation(stylizedFrames, stylizedMean, stylizedVariance,
		deltaTime, stylizedAnim, repetitionVector, upIndex);

	// Solve for the new style
	atArray< crstLDS > LDSs;
	int numLDSs = this->m_SkeletonData->GetNumBones()+FEATURE_NUM_TYPES-1;
	const int bufferSize = 256;
	char buffer[bufferSize];
	crstFeatureType featuretype;
	int aux;
	for(int i = 0; i < numLDSs; i++)
	{
		LDSs.PushAndGrow(crstLDS());

		crstStyle::IndexToFeature(i, featuretype, aux);

		if((featuretype != FEATURE_JOINT_ORIENTATION) ||
			(weightSet.GetAnimWeight(aux)))
		{
			crstStyle::FeatureToString(featuretype, aux, buffer, bufferSize);

			Printf("Computing %s (%d/%d)\n", buffer, i+1, numLDSs);

			this->ComputeLDS(unstylizedFrames, unstylizedMean,
				unstylizedVariance, stylizedFrames, stylizedMean,
				stylizedVariance, featuretype, aux, LDSs[i]);
		}
	}
	newStyle->Set(LDSs, unstylizedMean, unstylizedVariance, stylizedMean,
		stylizedVariance);
	newStyle->m_storedSlopeLimit = slopeLimit;
	newStyle->m_storedDeltaTime = deltaTime;

	Printf("The order of the system is %d\n", newStyle->GetSystemOrder());
	
	// Cleanup
	for(int i = 0; i < unstylizedFrames.size(); i++)
	{
		delete(unstylizedFrames[i]);
	}
	for(int i = 0; i < stylizedFrames.size(); i++)
	{
		delete(stylizedFrames[i]);
	}

	return newStyle;
}

////////////////////////////////////////////////////////////////////////////////
bool crstStyleGenerator::ConvertAnimation(
	atArray< crstStyleFrame* >& translatedFrames, crstStyleFrame& framesMean,
    crstStyleFrame& framesVariance, float deltaTime,
	const crAnimation& originalAnim, int upIndex)
{
	VectorT<float> emptyRepVector;
	emptyRepVector.Init(0,0);
	return ConvertAnimation(translatedFrames, framesMean, framesVariance,
		deltaTime, originalAnim, emptyRepVector, upIndex);
}

////////////////////////////////////////////////////////////////////////////////

bool crstStyleGenerator::ConvertAnimation(
	atArray< crstStyleFrame* >& translatedFrames, crstStyleFrame& framesMean,
    crstStyleFrame& framesVariance, float deltaTime,
	const crAnimation& originalAnim, const VectorT<float>& repetitionVector,
	int upIndex)
{
	float animationDuration = originalAnim.GetDuration();
	crFrame* currentFrame = rage_new crFrame();
	crFrame* testFrame = rage_new crFrame();
	currentFrame->InitCreateAnimationDofs(originalAnim);
	testFrame->InitCreateAnimationDofs(originalAnim);

	float deltaMultiplier = 1;
	bool doRepetitions = false;
	if(repetitionVector.GetSize()) doRepetitions = true;
	short index = 0;
	// First, convert each of the frames and store their sum
	for(float currentTime = 0; currentTime <= animationDuration;
		currentTime += deltaMultiplier*deltaTime)
	{
		bool compositedSuccess = false;
		
		if(originalAnim.HasMoverTracks())
		{
			compositedSuccess = originalAnim.CompositeFrameWithMover(
				currentTime, deltaTime, *currentFrame);
		}
		else
		{
			compositedSuccess = originalAnim.CompositeFrame(currentTime,
				*currentFrame);
		}

		if(!compositedSuccess)
		{
			return false;
		}

		crstStyleFrame* translatedFrame = rage_new crstStyleFrame();
		translatedFrame->Set(*currentFrame, *this->m_SkeletonData, deltaTime, upIndex);
		translatedFrame->SetTiming(repetitionVector[index]);

		if(translatedFrames.size())
		{
			framesMean.Add(*translatedFrame);
		}
		else
		{
			framesMean.Set(*translatedFrame);
		}

		translatedFrames.PushAndGrow(translatedFrame);

		index++;
		if(doRepetitions && (index >= repetitionVector.GetSize())) break;
		if(doRepetitions) deltaMultiplier = repetitionVector[index];
	}

	delete(currentFrame);
	delete(testFrame);

	// Next, compute the mean and variance of the frames
	framesMean.Divide((float) translatedFrames.size());
	for(int i = 0; i < translatedFrames.size(); i++)
	{
		crstStyleFrame meanZeroFrame = crstStyleFrame(*translatedFrames[i]);
		meanZeroFrame.Subtract(framesMean);
		meanZeroFrame.Multiply(meanZeroFrame);
		if(i == 0)
		{
			framesVariance.Set(meanZeroFrame);
		}
		else
		{
			framesVariance.Add(meanZeroFrame);
		}
	}
	framesVariance.Divide((float) translatedFrames.size());

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleGenerator::ComputeLDS(
	const atArray< crstStyleFrame* >& unstylizedFrames,
	const crstStyleFrame& unstylizedMean,
	const crstStyleFrame& unstylizedVariance,
	const atArray< crstStyleFrame* >& stylizedFrames,
	const crstStyleFrame& stylizedMean, const crstStyleFrame& stylizedVariance,
	crstFeatureType outputfeaturetype, int outputAuxIndex, crstLDS& computedLDS)
{
	crstFeatureType inputfeaturetype;
	int inputAuxIndex;
	crstStyle::MapOutputToInput(outputfeaturetype, outputAuxIndex,
		inputfeaturetype, inputAuxIndex);

	short numInputSamples = (short) unstylizedFrames.size();
	short numOutputSamples = (short) stylizedFrames.size();

	VectorT<float> emptyArray;
	VectorT<float> unstylizedFeatureMean;
	unstylizedMean.ToFeatureVector(unstylizedFeatureMean, emptyArray,
		emptyArray, inputfeaturetype, inputAuxIndex);
	VectorT<float> unstylizedFeatureVariance;
	unstylizedVariance.ToFeatureVector(unstylizedFeatureVariance, emptyArray,
		emptyArray, inputfeaturetype, inputAuxIndex);

	VectorT<float> stylizedFeatureMean;
	stylizedMean.ToFeatureVector(stylizedFeatureMean, emptyArray, emptyArray,
		outputfeaturetype, outputAuxIndex);
	VectorT<float> stylizedFeatureVariance;
	stylizedVariance.ToFeatureVector(stylizedFeatureVariance, emptyArray,
		emptyArray, outputfeaturetype, outputAuxIndex);

	VectorT<float> featureVector;
	MatrixT<float> input;
	input.Init(numInputSamples, (short) unstylizedFeatureMean.GetSize(), 0);
	for(short i = 0; i < numInputSamples; i++)
	{
		unstylizedFrames[i]->ToFeatureVector(featureVector,
			unstylizedFeatureMean, unstylizedFeatureVariance, inputfeaturetype,
			inputAuxIndex);
		for(short j = 0; j < (short) featureVector.GetSize(); j++)
		{
			input[i][j] = featureVector[j];
		}
	}
	MatrixT<float> output;
	output.Init(numOutputSamples, (short) stylizedFeatureMean.GetSize(), 0);
	for(short i = 0; i < numOutputSamples; i++)
	{
		stylizedFrames[i]->ToFeatureVector(featureVector,
			stylizedFeatureMean, stylizedFeatureVariance, outputfeaturetype,
			outputAuxIndex);
		for(short j = 0; j < (short) featureVector.GetSize(); j++)
		{
			output[i][j] = featureVector[j];
		}
	}
#if !__NO_OUTPUT
	Printf("  Grabbed feature vectors\n");

	unstylizedFeatureVariance.Print("  Unstylized Variance");
	unstylizedFeatureMean.Print("  Unstylized Mean");
	stylizedFeatureVariance.Print("  Stylized Variance");
	stylizedFeatureMean.Print("  Stylized Mean");
#endif

	MatrixT<float> A;
	MatrixT<float> B;
	MatrixT<float> C;
	MatrixT<float> D;
	SubspaceIdentification::SubspaceIdentificationRobust(input, output, A, B,
		C, D, true);

#if !__NO_OUTPUT
	A.Print("A");
	B.Print("B");
	C.Print("C");
	D.Print("D");
#endif

	computedLDS.Set(A, B, C, D);
}

