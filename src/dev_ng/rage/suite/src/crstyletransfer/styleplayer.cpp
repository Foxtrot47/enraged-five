//
// crstyletransfer/styleplayer.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "styleplayer.h"
#include "style.h"

#include "crskeleton/skeletondata.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////

crstStylePlayer::crstStylePlayer()
: m_Style(NULL),
  m_SkeletonData(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crstStylePlayer::crstStylePlayer(crstStyle* style,
		const crSkeletonData& skelData,	int upIndex, bool disableHeuristic,
		int confidence, int preference)
: m_Style(NULL),
  m_SkeletonData(NULL)
{
	Init(style, skelData, upIndex, disableHeuristic, confidence, preference);
}

////////////////////////////////////////////////////////////////////////////////

crstStylePlayer::crstStylePlayer(datResource& rsc)
: m_styleState(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crstStylePlayer::~crstStylePlayer()
{
	m_Style = NULL;
	m_SkeletonData = NULL;
	m_styleState.Reset();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crstStylePlayer);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crstStylePlayer::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crstStylePlayer);
	STRUCT_FIELD(m_Style);
	STRUCT_FIELD(m_SkeletonData);
	STRUCT_FIELD(m_disableHeuristic);
	STRUCT_FIELD(m_confidence);
	STRUCT_FIELD(m_preference);
	STRUCT_FIELD(m_upIndex);
	STRUCT_FIELD(m_styleState);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crstStylePlayer::Init(crstStyle* style, const crSkeletonData& skelData,
		int upIndex, bool disableHeuristic, int confidence, int preference)
{
	m_Style = style;
	m_SkeletonData = &skelData;
	m_styleState.Init(style);
	m_disableHeuristic = disableHeuristic;
	m_confidence = confidence;
	m_preference = preference;
	m_upIndex = upIndex;
}

////////////////////////////////////////////////////////////////////////////////

void crstStylePlayer::Reset()
{
	m_styleState.Reset();
}

////////////////////////////////////////////////////////////////////////////////

float crstStylePlayer::Stylize(crFrame& inoutFrame, float deltaTime)
{
	crstStyleFrame styleFrame = crstStyleFrame();
	styleFrame.Set(inoutFrame, *this->m_SkeletonData, deltaTime, m_upIndex);

	if(m_styleState.m_globalBlendWeight)
		m_Style->Apply(styleFrame, *this->m_SkeletonData, m_styleState,
		m_disableHeuristic, m_confidence, m_preference);

	float timing = styleFrame.GetAnimFrame(inoutFrame, m_upIndex);

	float slopeLimit = (float) m_Style->GetSlopeLimit();
	timing = FPClamp(timing, 1.0f/slopeLimit, slopeLimit);

	return timing;
}

////////////////////////////////////////////////////////////////////////////////

float crstStylePlayer::GetBlendWeight() const
{
	return m_styleState.m_globalBlendWeight;
}

////////////////////////////////////////////////////////////////////////////////

void crstStylePlayer::SetBlendWeight(float weight)
{
	m_styleState.m_globalBlendWeight = FPClamp(weight, 0.0f, 1.0f);
}

////////////////////////////////////////////////////////////////////////////////


