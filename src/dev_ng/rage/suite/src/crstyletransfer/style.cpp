//
// crstyletransfer/style.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "style.h"

#include "atl/array_struct.h"
#include "file/serialize.h"
#include "file/stream.h"
#include "crskeleton/skeletondata.h"
#include "math/amath.h"
#include "mathext/linearalgebra.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////

crstLDS::crstLDS()
{
	m_A.Init(0,0,0);
	m_B.Init(0,0,0);
	m_C.Init(0,0,0);
	m_D.Init(0,0,0);
}

////////////////////////////////////////////////////////////////////////////////

crstLDS::crstLDS(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crstLDS::~crstLDS()
{
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crstLDS);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crstLDS::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crstLDS);
	STRUCT_FIELD(m_A);
	STRUCT_FIELD(m_B);
	STRUCT_FIELD(m_C);
	STRUCT_FIELD(m_D);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

bool crstLDS::Set(const MatrixT<float>& A, const MatrixT<float>& B,
				  const MatrixT<float>& C, const MatrixT<float>& D)
{
	// Check that the system order is consistent
	short systemOrder = A.GetCols();
	if((systemOrder != A.GetRows()) ||
	   (systemOrder != B.GetRows()) ||
	   (systemOrder != C.GetCols()))
	{
		Errorf("crstLDS - Cannot set LDS.  Inconsistent system order\n");
		return false;
	}

	// Check that the input dimensionality is consistent
	short inputDims = B.GetCols();
	if(inputDims != D.GetCols())
	{
		Errorf("crstLDS - Cannot set LDS.  Inconsistent number of input dimensions\n");
		return false;
	}

	// Check that the output dimensionality is consistent
	short outputDims = C.GetRows();
	if(outputDims != D.GetRows())
	{
		Errorf("crstLDS - Cannot set LDS.  Inconsistent number of output dimensions\n");
		return false;
	}

	// Set those matrices
	m_A = MatrixT<float>(A);
	m_B = MatrixT<float>(B);
	m_C = MatrixT<float>(C);
	m_D = MatrixT<float>(D);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crstLDS::Set(const crstLDS& toCopy)
{
	m_A = MatrixT<float>(toCopy.m_A);
	m_B = MatrixT<float>(toCopy.m_B);
	m_C = MatrixT<float>(toCopy.m_C);
	m_D = MatrixT<float>(toCopy.m_D);
}

////////////////////////////////////////////////////////////////////////////////

bool crstLDS::Apply(const VectorT<float>& input, VectorT<float>& output,
					VectorT<float>& state)
{
	// Check that the input and system dimensionality is consistent
	short inputDims = this->GetInputDims();
	short systemOrder = this->GetSystemOrder();
	if((input.GetSize() != inputDims) || (state.GetSize() != systemOrder))
	{
		Errorf("crstLDS - Cannot apply LDS.  Inconsistent dimensionality.\n");
		return false;
	}

	// Compute Output
	VectorT<float> temp1;
	m_C.Transform(state, temp1);
	m_D.Transform(input, output);
	output.Add(temp1);

	// Update State
	m_A.Transform(state, temp1);
	m_B.Transform(input, state);
	state.Add(temp1);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

short crstLDS::GetSystemOrder() const
{
	return this->m_A.GetRows();
}

////////////////////////////////////////////////////////////////////////////////

short crstLDS::GetInputDims() const
{
	return this->m_B.GetCols();
}

////////////////////////////////////////////////////////////////////////////////

void crstLDS::GetDeterministicZero(MatrixT<float>& result)
{
	MatrixT<float> temp1;
	MatrixT<float> temp2;
	PseudoInverse(m_D, temp1);
	m_B.Multiply(temp1, temp2);
	temp2.Multiply(m_C, temp1);
	m_A.Subtract(temp1, result);
}

////////////////////////////////////////////////////////////////////////////////

void crstLDS::Serialize(datSerialize& s)
{
	// Version
	const int latestVersion = 1;

	int version = latestVersion;
	s << datLabel("LDSVersion:") << version << datNewLine;

	if(version != latestVersion)
	{
		Errorf("crstLDS - attempting to load version '%d' (only '%d' supported)",
			version, latestVersion);
        return;
	}

	// System Matrices
	s << m_A << m_B << m_C << m_D;
}

////////////////////////////////////////////////////////////////////////////////

crstStyleState::crstStyleState()
{
	m_bodyBlendTerm = 1;
	m_states.clear();
	m_globalBlendWeight = 1;
}

////////////////////////////////////////////////////////////////////////////////

crstStyleState::crstStyleState(datResource& rsc)
: m_states(rsc, true)
{
}

////////////////////////////////////////////////////////////////////////////////

crstStyleState::crstStyleState(const crstStyle* style)
{
	this->Init(style);
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crstStyleState);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crstStyleState::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crstStyleState);
	STRUCT_FIELD(m_bodyBlendTerm);
	STRUCT_FIELD(m_states);
	STRUCT_FIELD(m_globalBlendWeight);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crstStyleState::Init(const crstStyle* style)
{
	m_bodyBlendTerm = 1;
	m_states.clear();
	m_globalBlendWeight = 1;

	for(int i = 0; i < style->GetNumLDSs(); i++)
	{
		m_states.PushAndGrow(
			VectorT<float>((short) style->GetSystemOrder(i), 0));
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyleState::Reset()
{
	m_bodyBlendTerm = 1;

	for(int i = 0; i < m_states.size(); i++)
	{
		m_states[i].Init(m_states[i].GetSize(), 0);
	}
}

////////////////////////////////////////////////////////////////////////////////

crstStyle::crstStyle()
{
	this->m_Name.Reset();
	this->m_storedSlopeLimit = 1;
	this->m_storedDeltaTime = 1;
	this->m_LDSs.clear();
}

////////////////////////////////////////////////////////////////////////////////

crstStyle::crstStyle(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crstStyle::crstStyle(const crstStyle& toCopy)
{
	this->m_unstylizedMean = crstStyleFrame(toCopy.m_unstylizedMean);
	this->m_unstylizedVariance = crstStyleFrame(toCopy.m_unstylizedVariance);
	this->m_stylizedMean = crstStyleFrame(toCopy.m_stylizedMean);
	this->m_stylizedVariance = crstStyleFrame(toCopy.m_stylizedVariance);

	this->m_LDSs.clear();

	for(short i = 0; i < toCopy.m_LDSs.size(); i++)
	{
		this->m_LDSs.PushAndGrow(crstLDS());
		m_LDSs[i].Set(toCopy.m_LDSs[i]);
	}

	this->m_Name = toCopy.m_Name;
	this->m_storedSlopeLimit = toCopy.m_storedSlopeLimit;
	this->m_storedDeltaTime = toCopy.m_storedDeltaTime;
}

////////////////////////////////////////////////////////////////////////////////

crstStyle::~crstStyle()
{
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crstStyle);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crstStyle::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crstStyle);
	STRUCT_FIELD(m_Name);
	STRUCT_FIELD(m_LDSs);
	STRUCT_FIELD(m_unstylizedMean);
	STRUCT_FIELD(m_unstylizedVariance);
	STRUCT_FIELD(m_stylizedMean);
	STRUCT_FIELD(m_stylizedVariance);
	STRUCT_FIELD(m_storedSlopeLimit);
	STRUCT_FIELD(m_storedDeltaTime);
	STRUCT_END();
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

bool crstStyle::Load(const char* filename)
{
	fiStream* f = ASSET.Open(filename, "style", false, true);
	if(!f)
	{
		Errorf("crstStyle - failed to open file '%s' for reading", filename);
		return false;
	}

	int magic = 0;
	f->ReadInt(&magic, 1);

	if(magic != 'LYTS')
	{
		Errorf("crstStyle - file '%s' is not a style file", filename);
		f->Close();
		return false;
	}

	bool success = fiSerializeFrom(f, *this);

	f->Close();

	return success;
}

////////////////////////////////////////////////////////////////////////////////

bool crstStyle::Save(const char* filename)
{
	fiStream* f = ASSET.Create(filename, "style");
	if(!f)
	{
		Errorf("crstStyle - failed to open file '%s' for writing", filename);
		return false;
	}

	int magic = 'LYTS';
	f->WriteInt(&magic, 1);

	const bool binary = false;
	bool success = fiSerializeTo(f, *this, binary);

	f->Close();

	return success;
}

////////////////////////////////////////////////////////////////////////////////

void crstStyle::Serialize(datSerialize& s)
{
	// Version
	const int latestVersion = 3;

	int version = latestVersion;
	s << datLabel("StyleVersion:") << version << datNewLine;

	if(version != latestVersion)
	{
		Errorf("crstStyle - attempting to load version '%d' (only '%d' supported)",
			version, latestVersion);
        return;
	}

	// Name
	char name[256];
	formatf(name, "%s", (const char*)m_Name);
	s << datLabel("Name:") << datString(name, 256) << datNewLine;

	if(s.IsRead())
	{
		this->SetName(name);
	}

	// Slope Limit
	s << datLabel("SlopeLimit:") << m_storedSlopeLimit << datNewLine;

	// Delta Time
	s << datLabel("DeltaTime:") << m_storedDeltaTime << datNewLine;

	// LDSs
	int numLDSs = this->m_LDSs.size();
	s << datLabel("NumLDSs:") << numLDSs << datNewLine;

	for(int i = 0; i < numLDSs; i++)
	{
		if(s.IsRead())
		{
			m_LDSs.PushAndGrow(crstLDS());
		}
		m_LDSs[i].Serialize(s);
	}
	
	// Style Frames
	this->m_unstylizedMean.Serialize(s);
	this->m_unstylizedVariance.Serialize(s);
	this->m_stylizedMean.Serialize(s);
	this->m_stylizedVariance.Serialize(s);
}

////////////////////////////////////////////////////////////////////////////////

const char* crstStyle::GetName() const
{
	return m_Name;
}

////////////////////////////////////////////////////////////////////////////////

void crstStyle::SetName(const char* name)
{
	m_Name.Set(name, (int)strlen(name), 0);
}

////////////////////////////////////////////////////////////////////////////////

void crstStyle::Set(const atArray< crstLDS >& LDSs,
					const crstStyleFrame& unstylizedMean,
					const crstStyleFrame& unstylizedVariance,
				    const crstStyleFrame& stylizedMean,
					const crstStyleFrame& stylizedVariance)
{
	this->m_unstylizedMean = crstStyleFrame(unstylizedMean);
	this->m_unstylizedVariance = crstStyleFrame(unstylizedVariance);
	this->m_stylizedMean = crstStyleFrame(stylizedMean);
	this->m_stylizedVariance = crstStyleFrame(stylizedVariance);

	this->m_LDSs.clear();

	for(short i = 0; i < LDSs.size(); i++)
	{
		this->m_LDSs.PushAndGrow(crstLDS());
		m_LDSs[i].Set(LDSs[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyle::Apply(crstStyleFrame& inOutFrame,
					  const crSkeletonData& skeletonData,
					  crstStyleState& styleState, bool disableHeuristic,
					  int confidence, int preference)
{
	VectorT<float> emptyArray;
	float maxBlendTerm = 0;
	// short worstfeature = -1;
	float maxBlendChange = .02f*60*inOutFrame.GetDeltaTime();

	// Go through each feature (#bones + timing)
	for(short LDSIndex = 0; LDSIndex < this->GetNumLDSs(); LDSIndex++)
	{
		// Check and make sure the feature vector is used
		if(!m_LDSs[LDSIndex].GetInputDims()) continue;

		// Map the current LDS index to a feature type and optional auxillary
		// value
		crstFeatureType inputfeaturetype, outputfeaturetype;
		int inputAuxIndex, outputAuxIndex;
		this->IndexToFeature(LDSIndex, outputfeaturetype, outputAuxIndex);

		// Map the current input feature to its corresponding output feature
		this->MapOutputToInput(outputfeaturetype, outputAuxIndex,
			inputfeaturetype, inputAuxIndex);

		// Grab the appropriate means and variances
		VectorT<float> unstylizedFeatureMean;
		m_unstylizedMean.ToFeatureVector(unstylizedFeatureMean, emptyArray,
			emptyArray, inputfeaturetype, inputAuxIndex);
		VectorT<float> unstylizedFeatureVariance;
		m_unstylizedVariance.ToFeatureVector(unstylizedFeatureVariance,
			emptyArray, emptyArray, inputfeaturetype, inputAuxIndex);

		VectorT<float> stylizedFeatureMean;
		m_stylizedMean.ToFeatureVector(stylizedFeatureMean, emptyArray,
			emptyArray, outputfeaturetype, outputAuxIndex);
		VectorT<float> stylizedFeatureVariance;
		m_stylizedVariance.ToFeatureVector(stylizedFeatureVariance, emptyArray,
			emptyArray, outputfeaturetype, outputAuxIndex);

		// If we are dealing with a joint orientation, remap index based on id
		if(outputfeaturetype == FEATURE_JOINT_ORIENTATION)
		{
			int tempAux = 0;
			skeletonData.ConvertBoneIdToIndex(
				m_unstylizedMean.GetIDFromOrientIndex(inputAuxIndex), tempAux);
			if(inputAuxIndex == outputAuxIndex)
			{
				outputAuxIndex = tempAux;
				inputAuxIndex = tempAux;
			}
			else
			{
				inputAuxIndex = tempAux;
				skeletonData.ConvertBoneIdToIndex(
					m_unstylizedMean.GetIDFromOrientIndex(outputAuxIndex),
					tempAux);
				outputAuxIndex = tempAux;
			}
		}

		// Get the input vector
		VectorT<float> input;
		inOutFrame.ToFeatureVector(input,
			unstylizedFeatureMean,
			unstylizedFeatureVariance, inputfeaturetype, inputAuxIndex);

		// Check the blend term
		float blendTerm = 0;
		if(!disableHeuristic) blendTerm = this->GetBlendTerm(input, confidence,
			preference);
		if(maxBlendTerm < blendTerm)
		{
			maxBlendTerm = blendTerm;
			// worstfeature = LDSIndex;
		}

		// Apply the LDS
		VectorT<float> output;
		m_LDSs[LDSIndex].Apply(input, output, styleState.m_states[LDSIndex]);

		// Get original feature vector for blending later
		VectorT<float> inputResultingFeatureVector;
		inOutFrame.ToFeatureVector(inputResultingFeatureVector, emptyArray,
			emptyArray, outputfeaturetype, outputAuxIndex);

		// Set out frame with output feature vector
		inOutFrame.FromFeatureVector(output, stylizedFeatureMean,
			stylizedFeatureVariance, outputfeaturetype, outputAuxIndex);

		// Blend in original feature vector
		float globalBlendWeight = 1.0f - styleState.m_globalBlendWeight;

		float localBlendWeight = 1.0f - styleState.m_bodyBlendTerm;
		if(disableHeuristic) localBlendWeight = 1.0f;
		globalBlendWeight = 1.0f -
			(styleState.m_globalBlendWeight*localBlendWeight);

		inOutFrame.BlendInto(inputResultingFeatureVector, globalBlendWeight,
			outputfeaturetype, outputAuxIndex);
	}

	// Determine the blend term for the next go around
	float blendChange = maxBlendTerm - styleState.m_bodyBlendTerm;
	if(abs(blendChange) > maxBlendChange)
	{
		maxBlendTerm = styleState.m_bodyBlendTerm +
			((blendChange < 0) ? -maxBlendChange : maxBlendChange);
	}
	styleState.m_bodyBlendTerm = maxBlendTerm;
}

////////////////////////////////////////////////////////////////////////////////

int crstStyle::GetSystemOrder() const
{
	int summedOrder = 0;
	for(short i = 0; i < this->m_LDSs.size(); i++)
	{
		summedOrder += m_LDSs[i].GetSystemOrder();
	}
	return summedOrder;
}

////////////////////////////////////////////////////////////////////////////////

int crstStyle::GetSystemOrder(int LDSIndex) const
{
	return m_LDSs[LDSIndex].GetSystemOrder();
}

////////////////////////////////////////////////////////////////////////////////

int crstStyle::GetNumLDSs() const
{
	return (int) this->m_LDSs.size();
}

////////////////////////////////////////////////////////////////////////////////

short crstStyle::GetSlopeLimit() const
{
	return this->m_storedSlopeLimit;
}

////////////////////////////////////////////////////////////////////////////////

float crstStyle::GetDeltaTime() const
{
	return this->m_storedDeltaTime;
}

////////////////////////////////////////////////////////////////////////////////

double crstStyle::ERF(double z, int numSignificantDigits /* = 12 */) const
{
	if (abs(z) > 2.2)
	{
		return 1.0 - ERFC(z, numSignificantDigits);
	}

	// Clamp numSignificantDigits to [1,15]
	numSignificantDigits = Min(15, numSignificantDigits);
	numSignificantDigits = Max(1, numSignificantDigits);

	double factor = 2.0/sqrt(PI);
	double sigDigitTest = pow(10.0, -numSignificantDigits);
	double zSquared = square(z);

	int stepNum = 1;
	double integral = z;
	double currentNumerator = z;
	// Calculate Sum over t of
	// +/-(z^(2t+1)/t!)/(2t+1) (+ if t is even)
	// until we have enough significant digits
	while(abs(currentNumerator/integral) > sigDigitTest)
	{
		currentNumerator *= zSquared/stepNum;
		double denom = (stepNum*2)+1;
		double term = currentNumerator/denom;
		if(stepNum%2) integral -= term;
		else integral += term;
		stepNum++;
	}

	return factor*integral;
}

////////////////////////////////////////////////////////////////////////////////

double crstStyle::ERFC(double z, int numSignificantDigits /* = 12 */) const
{
	if(abs(z) < 2.2)
	{
		return 1.0 - ERF(-z, numSignificantDigits);
	}

	if(z < 0)
	{
		return 2.0 - ERFC(-z, numSignificantDigits);
	}

	// Clamp numSignificantDigits to [1,15]
	numSignificantDigits = Min(15, numSignificantDigits);
	numSignificantDigits = Max(1, numSignificantDigits);

	double factor = 1.0/sqrt(PI);

	double numM2 = 1;
	double numM1 = z;
	double denM2 = z;
	double denM1 = z*z + 0.5;
	double termM2 = 0;
	double termM1 = numM1/denM1;
	double n = 1.0;
	while(abs((termM2-termM1)/termM1) > numSignificantDigits)
	{
		double numerator = numM2*n + numM1*z;
		numM2 = numM1;
		numM1 = numerator;
		double denominator = denM2*n + denM1*z;
		denM2 = denM1;
		denM1 = denominator;
		n += 0.5;
		termM2 = termM1;
		termM1 = numM1/numM2;
	}

	return factor*exp(-z*z)*termM1;
}

////////////////////////////////////////////////////////////////////////////////

float crstStyle::GetBlendTerm(const VectorT<float>& featureVector,
		int confidence,	int preference) const
{
	double factor = 1.0/(confidence*sqrt(2.0));
	double maxError = DBL_MIN;
	for(short i = 0; i < featureVector.GetSize(); i++)
	{
		double z = abs(featureVector[i]*factor);
		double error = ERF(z, 12);
		error = pow(error, preference);
		maxError = Max(maxError, error);
	}

	return (float) maxError;
}

////////////////////////////////////////////////////////////////////////////////

void crstStyle::FeatureToIndex(crstFeatureType featuretype, int aux, int& index)
{
	switch(featuretype)
	{
	case FEATURE_MOVER:
		index = 0;
		break;
	case FEATURE_ROOT_OFFSET:
		index = 1;
		break;
	case FEATURE_TIMING:
		index = 2;
		break;
	case FEATURE_JOINT_ORIENTATION:
		index = 3+aux;
		break;
	default:
		index = -1;
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyle::IndexToFeature(int index, crstFeatureType& featuretype,
							   int& aux)
{
	if(index == 0)
	{
		featuretype = FEATURE_MOVER;
		aux = 0;
	}
	else if(index == 1)
	{
		featuretype = FEATURE_ROOT_OFFSET;
		aux = 0;
	}
	else if(index == 2)
	{
		featuretype = FEATURE_TIMING;
		aux = 0;
	}
	else
	{
		featuretype = FEATURE_JOINT_ORIENTATION;
		aux = index-3;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstStyle::MapOutputToInput(crstFeatureType outputfeaturetype,
								 int outputAux,
								 crstFeatureType& inputfeaturetype,
								 int& inputAux)
{
	inputAux = outputAux;
	if(outputfeaturetype == FEATURE_TIMING)
	{
		inputfeaturetype = FEATURE_JOINT_ORIENTATION;
		inputAux = 0;
	}
	else inputfeaturetype = outputfeaturetype;
}

////////////////////////////////////////////////////////////////////////////////

void crstStyle::FeatureToString(crstFeatureType featuretype, int aux,
								char* buffer, int bufferSize)
{
	switch(featuretype)
	{
	case FEATURE_MOVER:
		formatf(buffer, bufferSize, "Mover");
		break;
	case FEATURE_ROOT_OFFSET:
		formatf(buffer, bufferSize, "Root Offset");
		break;
	case FEATURE_TIMING:
		formatf(buffer, bufferSize, "Timing");
		break;
	case FEATURE_JOINT_ORIENTATION:
		formatf(buffer, bufferSize, "Joint Orientation %d", aux);
		break;
	default:
		formatf(buffer, bufferSize, " ");
		break;
	}
}


