//
// crstyletransfer/styleplayer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRSTYLETRANSFER_STYLEPLAYER_H
#define CRSTYLETRANSFER_STYLEPLAYER_H

#include "styleframe.h"
#include "style.h"

#include "data/resource.h"
#include "data/struct.h"

namespace rage
{

// pre declarations
class crFrame;
class crSkeletonData;

// PURPOSE: Style player applies a style at runtime
// NOTES:  When applying a style, there are a few things to note:
//   * It's assumed that the animation the style is being applied to
//     has a mover.  The mover shouldn't just be a projection of the root
//     onto the floor; instead, the mover should be a smooth high-level
//     representation of the overall movement of the character
//   * A style does not stylize the mover orientation
//   * In most cases, layering styles on the same animation is a bad idea.
//     This can lead to unpredictable results.
//   * Styles can be very sensitive to variations in joint angles, even if
//     they don't alter the general pose/motion of the character
//   * Heuristic for determining when a style should be applied doesn't always
//     work very well.  It fails to recognize moments when a style shouldn't
//     be applied and introduces artifacts in cases where the style should be
//     applied.
class crstStylePlayer
{
public:

	// PURPOSE: Default constructor
	crstStylePlayer();

	// PURPOSE: Resource constructor
	crstStylePlayer(datResource&);

	// PURPOSE: Initializing constructor
	// PARAMS:  style - the style this player should apply
	//          skelData - the skeleton this player will apply the style to
	//          upIndex - Defines which dimension is considered the up axis
	//                    (0 = x-axis, 1 = y-axis, and 2 = z-axis)
	//          disableHeuristic - whether or not to disable the heuristic
	//                             blending
	//          confidence - confidence in the system.  Higher numbers mean you
	//                       have more confidence.  Expected to
	//                       be greater than 0.
	//          preference - preference for stylization.  Higher numbers mean
	//                       the heuristic should prefer to stylize the incoming
	//                       motion.
	// NOTE:  confidence and preference parameters are expected to be greater
	//        than 0.  For a mathematical definition of these parameters, see
	//        the original Siggraph 2005 Style Translation paper
	crstStylePlayer(crstStyle* style, const crSkeletonData& skelData,
		int upIndex=0, bool disableHeuristic=false, int confidence=20, int preference=2);

	// PURPOSE: Destructor
	~crstStylePlayer();

	// PURPOSE: Placement
	DECLARE_PLACE(crstStylePlayer);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Initializer
	// PARAMS: style - pointer to style data
	//         skelData - the skeleton this player will apply the style to
	//         upIndex - Defines which dimension is considered the up axis
	//                   (0 = x-axis, 1 = y-axis, and 2 = z-axis)
	//         disableHeuristic - whether or not to disable the heuristic
	//                            blending
	//         confidence - confidence in the system.  Higher numbers mean you
	//                      have more confidence.  Expected to
	//                      be greater than 0.
	//         preference - preference for stylization.  Higher numbers mean
	//                      the heuristic should prefer to stylize the incoming
	//                      motion.
	// NOTE:  confidence and preference parameters are expected to be greater
	//        than 0.  For a mathematical definition of these parameters, see
	//        the original Siggraph 2005 Style Translation paper
	void Init(crstStyle* style, const crSkeletonData& skelData,
		int upIndex=0, bool disableHeuristic=false, int confidence=20,
		int preference=2);

	// PURPOSE:  Reset the state of this player.
	void Reset();

	// PURPOSE: Stylize input data
	// PARAMS: inoutFrame - provide frame of unstylized data for stylizing
	//         deltaTime - the current sampling rate
	// RETURNS: suggested timing info.  The style suggests that standard delta
	//          time for the next iteration be divided by the returned value.
    float Stylize(crFrame& inoutFrame, float deltaTime);


	// PURPOSE: Get the current blend weight for this style.
	// RETURNS: Current blend weight [0.0...1.0]
	float GetBlendWeight() const;

	// PURPOSE: Sets the current blend weight for this style.
	// PARAMS:  weight - the new blend weight.  The weight will be clamped
	//                   to the range [0.0, 1.0].  0.0 means the style won't be
	//                   applied while 1.0 means the style will be fully
	//                   applied.
	void SetBlendWeight(float weight);

private:
	datRef<crstStyle> m_Style;
	datRef<const crSkeletonData> m_SkeletonData;
	bool m_disableHeuristic;
	int m_confidence;
	int m_preference;
	int m_upIndex;

	// Style State
	crstStyleState m_styleState;
};

}; // namespace rage

#endif // CRSTYLETRANSFER_STYLEPLAYER_H
