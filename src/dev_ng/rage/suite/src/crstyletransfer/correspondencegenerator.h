//
// crstyletransfer/correspondencegenerator.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRSTYLETRANSFER_CORRESPONDENCEGENERATOR_H
#define CRSTYLETRANSFER_CORRESPONDENCEGENERATOR_H

#include "atl/array.h"
#include "vector/matrixt.h"
#include "vector/vectort.h"

namespace rage
{

// pre-declarations
class crstStyleFrame;
class crWeightSet;
class crSkeletonData;
class crSkeleton;
class crAnimation;
class crFrame;

// PURPOSE: Generates timing information between two motions (offline use only)
class crstCorrespondenceGenerator
{
public:

	// PURPOSE: Default constructor
	crstCorrespondenceGenerator();

	// PURPOSE: Destructor
	~crstCorrespondenceGenerator();

	// PURPOSE: Initializer
	// PARAMS: skelData - skeleton data of character
	void Init(const crSkeletonData& skelData);

	// PURPOSE: Compute the time alignment that warps correspondingFrames to
	//          referenceFrames using simple joint angle alignment.
	// PARAMS:  referenceFrames - an array that holds the reference frames
	//          correspondingingFrames - an array that holds the corresponding
	//                                   frames
	//          slopeLimit - defines the range of values allowed in the
	//                       repetitionArray.  Each value in the repetitionArray
	//                       is required to be between 1/slopeLimit and
	//                       slopeLimit, inclusive.
	//          repetitionVector - the resulting time warp, represented as a
	//                            repetitionVector.  Item i of the array
	//                            provides a floating point represenation of the
	//                            number of frames in the corresponding motion
	//                            that should map to frame i of the reference
	//                            motion
	//          weightSet - When calculating the correspondence, only joints
	//                      with a weight > 0 are considered.
	//          imwIterations - Determines the number of iterations to run
	//          imwWeight - Weight used for smoothing
	// RETURNS: true - if the correspondence was found
	//          false - otherwise.
	bool DetermineCorrespondence(
		const atArray< crstStyleFrame* >& referenceFrames,
		const atArray< crstStyleFrame* >& correspondingFrames,
		short slopeLimit, VectorT<float>& repetitionVector,
		crWeightSet& weightSet, short imwIterations, float imwWeight);

	// PURPOSE: Compute the time alignment that warps frames in the
	//          correspondingAnim to frames in the referenceAnim using point
	//          cloud distance metric.
	// PARAMS:  referenceAnim - the reference animation
	//          correspondingingFrames - the corresponding animation
	//          slopeLimit - defines the range of values allowed in the
	//                       repetitionArray.  Each value in the repetitionArray
	//                       is required to be between 1/slopeLimit and
	//                       slopeLimit, inclusive.
	//          repetitionVector - the resulting time warp, represented as a
	//                            repetitionVector.  Item i of the array
	//                            provides a floating point represenation of the
	//                            number of frames in the corresponding motion
	//                            that should map to frame i of the reference
	//                            motion
	//          weightSet - When calculating the correspondence, only joints
	//                      with a weight > 0 are considered.
	//          deltaTime - the target sampling rate in seconds
	// RETURNS: true - if the correspondence was found
	//          false - otherwise.
	bool DetermineCorrespondence(
		const crAnimation& referenceAnim,
		const crAnimation& correspondingAnim,
		short slopeLimit, VectorT<float>& repetitionVector,
		crWeightSet& weightSet, float deltaTime);

private:
	MatrixT<float> m_distanceGrid;
	short m_slopeLimit;
	atArray< short > m_repetitionArray;
	MatrixT<float> m_A;
	MatrixT<float> m_B;
	MatrixT<float> m_WTranspose;
	VectorT<float> m_WTProduct;
	MatrixT<float> m_smoothness; // F and G in the paper
	MatrixT<float> m_smoothnessTProduct; // F^T*F and G^T*G in the paper
	const crSkeletonData* m_SkeletonData;

	// Methods used to determine a correspondence
	void Clear();
	void BuildSmoothnessTerms(short numReferenceFrames, float weight);
	void BuildDistanceGrid(const atArray< crstStyleFrame* >& referenceFrames,
		const atArray< crstStyleFrame* >& correspondingFrames,
		crWeightSet& weightSet);
	float ComputeCorrespondenceCost(const crstStyleFrame* referenceFrame,
		const crstStyleFrame* correspondingFrame, short frameNum,
		crWeightSet& weightSet) const;
	float ComputeCorrespondenceCostWithPointCloud(
		const crFrame& referenceAnimFrame,
		const crFrame& correspondingAnimFrame,
		crWeightSet& weightSet, crSkeleton& skeleton) const;
	float PerformDynamicProgramming();
	void BuildW();
	void BuildUandY(VectorT<float>& U, VectorT<float>& Y, int jointNum,
		short axisDOF, const atArray< crstStyleFrame* >& referenceFrames,
	    const atArray< crstStyleFrame* >& correspondingFrames) const;
	void FillAandB(const VectorT<float>& U, VectorT<float>& Y, int jointNum,
		short axisDOF);
	void PerformSpaceWarpStep(const atArray< crstStyleFrame* >& referenceFrames,
		const atArray< crstStyleFrame* >& correspondingFrames,
		crWeightSet& weightSet);
};

}; // namespace rage

#endif // CRSTYLETRANSFER_CORRESPONDENCEGENERATOR_H
