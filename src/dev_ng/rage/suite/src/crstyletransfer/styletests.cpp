//
// crstyletransfer/styletests.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "style.h"
#include <stdio.h>
#include <math/amath.h>
#include <system/timer.h>
#include <math/random.h>
#include <mathext/linearalgebra.h>
#include <mathext/subspaceid.h>

#if !__FINAL

using namespace rage;

// This file contains a number of tests (both automated and interactive) that
// can help evaluate the validity of the methods in crstLinearAlgebra.

bool testComputeHankelMatrix()
{
	short numSamples = 15;
	short numDims = 4;
	MatrixT<float> A = MatrixT<float>(numSamples, numDims);
	for(short i = 0; i < A.GetRows(); i++)
	{
		for(short j = 0; j < A.GetCols(); j++)
		{
			A[i][j] = i + ((float)j)/numDims;
		}
	}

	MatrixT<float> Hankel;
	short numBlockRows = (short) Min(numDims+1, (numSamples+1)/2);
	ComputeBlockHankel(A, numBlockRows,
		numSamples - 2*numBlockRows + 1, Hankel);

	for(short i = 0; i < Hankel.GetRows(); i++)
	{
		for(short j = 0; j < Hankel.GetCols(); j++)
		{
			short input = (i/numDims);
			short dim = i - input*numDims;
			input = input + j;
			float expectedValue = input + ((float)dim)/numDims;
			if(Hankel[i][j] != expectedValue)
			{
				return false;
			}
		}
	}

	return true;
}

bool testMatrixStacking()
{
	short numCols = 5;
	short numRows1 = 3;
	short numRows2 = 2;

	MatrixT<float> A = MatrixT<float>(numRows1, numCols);
	for(short i = 0; i < A.GetRows(); i++)
	{
		for(short j = 0; j < A.GetCols(); j++)
		{
			A[i][j] = (float) i*A.GetCols()+j;
		}
	}

	MatrixT<float> B = MatrixT<float>(numRows2, numCols);
	for(short i = 0; i < B.GetRows(); i++)
	{
		for(short j = 0; j < B.GetCols(); j++)
		{
			B[i][j] = (float) i*B.GetCols()+j+A.GetRows()*A.GetCols();
		}
	}

	MatrixT<float> Stacked;
	Stack(A, B, Stacked);

	for(short i = 0; i < Stacked.GetRows(); i++)
	{
		for(short j = 0; j < Stacked.GetCols(); j++)
		{
			float expectedValue = (float) i*Stacked.GetCols() + j;
			if(Stacked[i][j] != expectedValue)
			{
				return false;
			}
		}
	}

	return true;
}

bool testLQDecompositionInstance(const MatrixT<float>& originalA)
{
	Printf("    Testing Matrix of size %dx%d\n", originalA.GetRows(),
		originalA.GetCols());
	float epsilon = .000001f;
	MatrixT<float> L;
	MatrixT<float> Q;
	LQDecomposition(originalA, L, Q);


	// Check that A = LQ
	MatrixT<float> computedA;
	L.Multiply(Q, computedA);
	MatrixT<float> difference;
	originalA.Subtract(computedA, difference);
	float error = difference.NormSquared();
	if(error > epsilon)
	{
		Errorf("testLQDecompositionInstance - LQ does not equal A (sum-of-squared differences: %f)\n", error);
		return false;
	}

	// Check that Q is orthogonal
	MatrixT<float> Identity;
	MatrixT<float> QTranspose;
	Q.Transpose(QTranspose);
	Q.Multiply(QTranspose, Identity);
	error = 0;
	for(short i = 0; i < Identity.GetRows(); i++)
	{
		for(short j = 0; j < Identity.GetCols(); j++)
		{
			float expectedValue = 0;
			if(i == j) expectedValue = 1;
			error += square(Identity[i][j] - expectedValue);
		}
	}
	if(error > epsilon)
	{
		Errorf("testLQDecompositionInstance - Q is not Orthogonal\n");
		return false;
	}

	// Check that L is lower triangular
	for(short i = 0; i < L.GetRows(); i++)
	{
		for(short j = 0; j < L.GetCols(); j++)
		{
			if((i < j) && (L[i][j] > 0))
			{
				Errorf("testLQDecompositionInstance - L is not lower triangular\n");
				return false;
			}
		}
	}

	return true;
}

bool testLQDecomposition()
{
	int minNumDims = 2;
	int maxNumDims = 500;
	const int randSeed = ( int ) sysTimer::GetSystemMsTime();
    mthRandom randomNumberGenerator = mthRandom( randSeed );

	short squareDims = (short) randomNumberGenerator.GetRanged(minNumDims,
		maxNumDims);

	// Test a square matrix
	MatrixT<float> originalA;
	originalA.Init(squareDims, squareDims, 0);
	for(short i = 0; i < originalA.GetRows(); i++)
	{
		for(short j = 0; j < originalA.GetCols(); j++)
		{
			float randomDataValue = randomNumberGenerator.GetFloat();
			originalA[i][j] = randomDataValue;
		}
	}

	if(!testLQDecompositionInstance(originalA)) return false;

	short rectangleDims1 = (short) randomNumberGenerator.GetRanged(minNumDims,
		maxNumDims);
	short rectangleDims2 = (short) randomNumberGenerator.GetRanged(minNumDims,
		maxNumDims);

	while(rectangleDims1 == rectangleDims2)
	{
		rectangleDims2 = (short) randomNumberGenerator.GetRanged(minNumDims,
			maxNumDims);
	}

	// Test a rectangular matrix
	originalA.Init(rectangleDims1, rectangleDims2, 0);
	for(short i = 0; i < originalA.GetRows(); i++)
	{
		for(short j = 0; j < originalA.GetCols(); j++)
		{
			float randomDataValue = randomNumberGenerator.GetFloat();
			originalA[i][j] = randomDataValue;
		}
	}

	if(!testLQDecompositionInstance(originalA)) return false;

	// Test a second rectangular matrix where the dominate dimensionality is
	// the opposite of the previous test (i.e. we want to test a matrix that
	// has more rows than columns as well as one that has more columns than
	// rows)
	originalA.Init(rectangleDims2, rectangleDims1, 0);
	for(short i = 0; i < originalA.GetRows(); i++)
	{
		for(short j = 0; j < originalA.GetCols(); j++)
		{
			float randomDataValue = randomNumberGenerator.GetFloat();
			originalA[i][j] = randomDataValue;
		}
	}

	if(!testLQDecompositionInstance(originalA)) return false;

	return true;
}

bool runAutomatedTests()
{
	bool success = true;
	bool localsuccess;

	Printf("Testing Computation of Block Hankel Matrices\n");
	localsuccess = testComputeHankelMatrix();
	if(localsuccess) Printf("   Passed\n\n");
	else Printf("   Failed\n\n");
	success = success && localsuccess;

	Printf("Testing Matrix Stacking\n");
	localsuccess = testMatrixStacking();
	if(localsuccess) Printf("   Passed\n\n");
	else Printf("   Failed\n\n");
	success = success && localsuccess;

	Printf("Testing LQ Decomposition\n");
	localsuccess = testLQDecomposition();
	if(localsuccess) Printf("   Passed\n\n");
	else Printf("   Failed\n\n");
	success = success && localsuccess;

	if(success) Printf("All Tests Passed\n");
	else Printf("1 or more Tests Failed\n");
	return success;
}

void normalizeData(MatrixT<float>& input)
{
	for(short j = 0; j < input.GetCols(); j++)
	{
		float mean = 0;
		for(short i = 0; i < input.GetRows(); i++)
		{
			mean += input[i][j];
		}
		mean /= input.GetRows();

		float variance = 0;
		for(short i = 0; i < input.GetRows(); i++)
		{
			input[i][j] -= mean;
			variance += square(input[i][j]);
		}
		variance /= input.GetRows();

		if(variance > 0.00001f)
		{
			float sqrtVariance = sqrt(variance);
			for(short i = 0; i < input.GetRows(); i++)
			{
				input[i][j] /= sqrtVariance;
			}
		}
	}
}

void testSystemIdentification()
{
	short numSamples = 5000;
	const int randSeed = ( int ) sysTimer::GetSystemMsTime();
	mthRandom randomNumberGenerator = mthRandom( randSeed );

	MatrixT<float> inputs = MatrixT<float>(numSamples, 1);
	for(short i = 0; i < inputs.GetRows(); i++)
	{
		float randomValue = randomNumberGenerator.GetFloat();
		inputs[i][0] = randomValue;
	}

	normalizeData(inputs);

	MatrixT<float> originalA;
	originalA.Init(4, 4, 0);
	originalA[0][0] = originalA[0][1] = originalA[1][1] = originalA[3][2] = 0.603f;
	originalA[1][0] = originalA[2][2] = originalA[2][3] = originalA[3][3] = -0.603f;
	
	MatrixT<float> originalB;
	originalB.Init(4, 1, 0);
	originalB[0][0] = 0.9238f;
	originalB[1][0] = 2.7577f;
	originalB[2][0] = 4.3171f;
	originalB[3][0] = -2.6436f;

	MatrixT<float> originalC = MatrixT<float>(1, 4);
	originalC[0][0] = -0.5749f;
	originalC[0][1] = 1.0751f;
	originalC[0][2] = -0.5225f;
	originalC[0][3] = 0.1830f;

	MatrixT<float> originalD = MatrixT<float>(1, 1);
	originalD[0][0] = -0.7139f;

	crstLDS originalLDS;
	originalLDS.Set(originalA, originalB, originalC, originalD);

	MatrixT<float> outputs = MatrixT<float>(numSamples, 1);
	VectorT<float> state = VectorT<float>(originalLDS.GetSystemOrder(), 0);
	for(short i = 0; i < numSamples; i++)
	{
		VectorT<float> input = VectorT<float>(1, 0);
		input[0] = inputs[i][0];

		// Compute Output
		VectorT<float> output;
		originalLDS.Apply(input, output, state);

		outputs[i][0] = output[0];
	}

	//crstLinearAlgebra::PrintMatrix(inputs, "Inputs");
	//crstLinearAlgebra::PrintMatrix(outputs, "Outputs");

	MatrixT<float> A;
	MatrixT<float> B;
	MatrixT<float> C;
	MatrixT<float> D;
	SubspaceIdentification::SubspaceIdentificationRobust(inputs, outputs, A, B, C,
		D, true);

	A.Print("A");
	B.Print("B");
	C.Print("C");
	D.Print("D");

	crstLDS computedLDS;
	computedLDS.Set(A, B, C, D);

	MatrixT<float> originalZero;
	originalLDS.GetDeterministicZero(originalZero);

	MatrixT<float> computedZero;
	computedLDS.GetDeterministicZero(computedZero);

	originalZero.Print("Original Zero");
	computedZero.Print("Computed Zero");

	MatrixT<float> computedOutputs = MatrixT<float>(numSamples, 1);
	state.Init(computedLDS.GetSystemOrder(), 0);
	for(short i = 0; i < numSamples; i++)
	{
		VectorT<float> input = VectorT<float>(1, 0);
		input[0] = inputs[i][0];

		if(i < 200)
		{
			state.Print("State");
		}
		
		// Compute Output
		VectorT<float> output;
		computedLDS.Apply(input, output, state);

		computedOutputs[i][0] = output[0];

		if(i < 200)
		{
			Printf("Input: %f Actual Output: %f ComputedOutput: %f\n",
				inputs[i][0], outputs[i][0], computedOutputs[i][0]);
		}
	}

}

void testSystemIdentificationSmall()
{
	short numSamples = 5000;
	const int randSeed = ( int ) sysTimer::GetSystemMsTime();
	mthRandom randomNumberGenerator = mthRandom( randSeed );

	MatrixT<float> inputs = MatrixT<float>(numSamples, 1);
	for(short i = 0; i < inputs.GetRows(); i++)
	{
		float randomValue = randomNumberGenerator.GetFloat();
		inputs[i][0] = randomValue;
	}

	normalizeData(inputs);

	MatrixT<float> originalA;
	originalA.Init(1, 1, 0);
	originalA[0][0] = .949f;
	
	MatrixT<float> originalB;
	originalB.Init(1, 1, 0);
	originalB[0][0] = 1.8805f;

	MatrixT<float> originalC;
	originalC.Init(1, 1, 0);
	originalC[0][0] = .8725f;

	MatrixT<float> originalD;
	originalD.Init(1, 1, 0);
	originalD[0][0] = -2.0895f;

	crstLDS originalLDS;
	originalLDS.Set(originalA, originalB, originalC, originalD);

	MatrixT<float> outputs = MatrixT<float>(numSamples, 1);
	VectorT<float> state = VectorT<float>(originalLDS.GetSystemOrder(), 0);
	for(short i = 0; i < numSamples; i++)
	{
		VectorT<float> input = VectorT<float>(1, 0);
		input[0] = inputs[i][0];

		// Compute Output
		VectorT<float> output;
		originalLDS.Apply(input, output, state);

		outputs[i][0] = output[0];
	}

	//normalizeData(outputs);

	//crstLinearAlgebra::PrintMatrix(inputs, "Inputs");
	//crstLinearAlgebra::PrintMatrix(outputs, "Outputs");

	MatrixT<float> A;
	MatrixT<float> B;
	MatrixT<float> C;
	MatrixT<float> D;
	SubspaceIdentification::SubspaceIdentificationRobust(inputs, outputs, A, B, C,
		D, true);

	A.Print("A");
	B.Print("B");
	C.Print("C");
	D.Print("D");

	crstLDS computedLDS;
	computedLDS.Set(A, B, C, D);

	MatrixT<float> originalZero;
	originalLDS.GetDeterministicZero(originalZero);

	MatrixT<float> computedZero;
	computedLDS.GetDeterministicZero(computedZero);

	originalZero.Print("Original Zero");
	computedZero.Print("Computed Zero");

	MatrixT<float> computedOutputs = MatrixT<float>(numSamples, 1);
	state.Init(computedLDS.GetSystemOrder(), 0);
	for(short i = 0; i < numSamples; i++)
	{
		VectorT<float> input = VectorT<float>(1, 0);
		input[0] = inputs[i][0];

		if(i < 200)
		{
			state.Print("State");
		}

		// Compute Output
		VectorT<float> output;
		computedLDS.Apply(input, output, state);

		computedOutputs[i][0] = output[0];

		if(i < 200)
		{
			Printf("Input: %f Actual Output: %f ComputedOutput: %f\n",
				inputs[i][0], outputs[i][0], computedOutputs[i][0]);
		}
	}

}

#endif	// __FINAL
