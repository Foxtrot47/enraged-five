//
// crstyletransfer/stylegenerator.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRSTYLETRANSFER_STYLEGENERATOR_H
#define CRSTYLETRANSFER_STYLEGENERATOR_H

#include "atl/array.h"
#include "vector/vectort.h"
#include "styleframe.h"

namespace rage
{

// pre-declarations
class crAnimation;
class crSkeletonData;
class crstStyle;
class crFrame;
class crstLDS;
class crWeightSet;

// PURPOSE: Generates new styles (offline use only)
// NOTES:  There are a number of things to note when generating a style
//   * It's assumed that the training data has a mover.  The mover shouldn't
//     just be a projection of the root onto the floor; instead, the
//     mover should be a smooth high-level representation of the overall
//     movement of the character
//   * Our initial tests suggest that the training data for locomotion should
//     include several examples of left sharp turns, right sharp turns,
//     left smooth turns, right smooth turns, straight segments, starts and stops,
//     and turns on the spot.
//   * It is not advisable to create additional training data by interpolating
//     existing data as this will degrade the system.
//   * The training data consists of two training animations:  one in a neutral
//     style and one in the style you wish to learn.  The sequences should
//     consist of the same sequence of actions (with the same number of
//     footsteps), but can vary greatly in the details (e.g. timing, size of
//     steps, general configuration of the body, etc...)
class crstStyleGenerator
{
public:

	// PURPOSE: Default constructor
	crstStyleGenerator();

	// PURPOSE: Initializing constructor
	crstStyleGenerator(crSkeletonData& skelData);

	// PURPOSE: Destructor
	~crstStyleGenerator();

	// PURPOSE: Initializer
	// PARAMS: skelData - skeleton data of character
	void Init(const crSkeletonData& skelData);

	// PURPOSE: Create a new type of style
	// PARAMS: unstylizedAnim - the unstylized animation
	//         stylizedAnim - stylized verison of the animation
	//         weightSet - When calculating the style, only joints
	//                     with a weight > 0 are considered.
	//         upIndex - Defines which dimension is considered the up axis
	//                   (0 = x-axis, 1 = y-axis, and 2 = z-axis)
	//         slopeLimit - defines the maximum allowable slope during time
	//                      alignment.
	//         useIMW - if true, uses iterative motion warping to determine
	//                  time correspondence rather than point clouds
	//         imwIterations - only used for iterative motion warping.
	//                         Determines the number of iterations to run
	//         imwWeight - only used for iterative motion warping.
	//                     Weight used for smoothing
	// RETURNS: new style
	crstStyle* CreateStyle(const crAnimation& unstylizedAnim,
		const crAnimation& stylizedAnim, crWeightSet& weightSet,
		int upIndex, short slopeLimit, bool useIMW,
		short imwIterations = 1, float imwWeight = 100);

	// PURPOSE:  Convert an animation into an array of style frames.
	// PARAMS:  translatedFrames - the array to store the translated style
	//                             frames in
	//          framesMean - a style frame to store the mean of the translated
	//                       style frames in
	//          framesVariance - a style frame to store the variance of the
	//                           translated style frames in
	//          deltaTime - the time step between frames
	//          originalAnim - the original animation to convert
	//          repetitionVector - specifies multipliers for deltaTime at each
	//                             time step
	//          upIndex - Defines which dimension is considered the up axis
	//                    (0 = x-axis, 1 = y-axis, and 2 = z-axis)
	// RETURNS:  true if the function is able to translate all of the animation
	//                frames successfully
	//           false otherwise
	bool ConvertAnimation(atArray< crstStyleFrame* >& translatedFrames,
		crstStyleFrame& framesMean, crstStyleFrame& framesVariance,
		float deltaTime, const crAnimation& originalAnim,
		const VectorT<float>& repetitionVector, int upIndex);

	// PURPOSE:  Convert an animation into an array of style frames.
	// PARAMS:  translatedFrames - the array to store the translated style
	//                             frames in
	//          framesMean - a style frame to store the mean of the translated
	//                       style frames in
	//          framesVariance - a style frame to store the variance of the
	//                           translated style frames in
	//          deltaTime - the time step between frames
	//          originalAnim - the original animation to convert
	//          upIndex - Defines which dimension is considered the up axis
	//                    (0 = x-axis, 1 = y-axis, and 2 = z-axis)
	// RETURNS:  true if the function is able to translate all of the animation
	//                frames successfully
	//           false otherwise
	bool ConvertAnimation(atArray< crstStyleFrame* >& translatedFrames,
		crstStyleFrame& framesMean, crstStyleFrame& framesVariance,
		float deltaTime, const crAnimation& originalAnim, int upIndex);

private:
	const crSkeletonData* m_SkeletonData;

	// Computes and LDS for a single feature
	void ComputeLDS(const atArray< crstStyleFrame* >& unstylizedFrames,
		const crstStyleFrame& unstylizedMean,
		const crstStyleFrame& unstylizedVariance,
		const atArray< crstStyleFrame* >& stylizedFrames,
		const crstStyleFrame& stylizedMean,
		const crstStyleFrame& stylizedVariance,
		crstFeatureType outputfeaturetype, int outputAuxIndex,
		crstLDS& computedLDS);
};

}; // namespace rage

#endif // CRSTYLETRANSFER_STYLEGENERATOR_H
