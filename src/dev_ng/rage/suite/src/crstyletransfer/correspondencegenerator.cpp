//
// crstyletransfer/correspondencegenerator.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "correspondencegenerator.h"

#include "styleframe.h"

#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/weightset.h"
#include "crparameterizedmotion/pointcloud.h"
#include "crparameterizedmotion/pointcloudtransform.h"
#include "crskeleton/skeleton.h"
#include "mathext/linearalgebra.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////

crstCorrespondenceGenerator::crstCorrespondenceGenerator()
{
	Clear();

}

////////////////////////////////////////////////////////////////////////////////

crstCorrespondenceGenerator::~crstCorrespondenceGenerator()
{
	Clear();
}

////////////////////////////////////////////////////////////////////////////////

void crstCorrespondenceGenerator::Init(const crSkeletonData& skelData)
{
	m_SkeletonData = &skelData;
}

////////////////////////////////////////////////////////////////////////////////

void crstCorrespondenceGenerator::Clear()
{
	m_distanceGrid.Init(0,0,0);
	m_slopeLimit = 0;
	m_WTranspose.Init(0,0,0);
	m_WTProduct.Init(0,0);
	m_repetitionArray.clear();
	m_A.Init(0,0,0);
	m_B.Init(0,0,0);
	m_smoothness.Init(0,0,0);
	m_smoothnessTProduct.Init(0,0,0);
}

////////////////////////////////////////////////////////////////////////////////

bool crstCorrespondenceGenerator::DetermineCorrespondence(
	const atArray< crstStyleFrame* >& referenceFrames,
	const atArray< crstStyleFrame* >& correspondingFrames,
	short slopeLimit, VectorT<float>& repetitionVector,
	crWeightSet& weightSet, short imwIterations, float imwWeight)
{
	// Make sure it is possible to find a correspondence given the slopeLimit
	if((referenceFrames.size() > correspondingFrames.size()*slopeLimit) ||
	   (referenceFrames.size()*slopeLimit < correspondingFrames.size()))
	{
		Errorf("crstCorrespondenceGenerator - Cannot find correspondences between provided frames.  Slope limit too small.");
		return false;
	}

	m_slopeLimit = slopeLimit;

	// Initialize the space warp terms
	short numJoints = (short) referenceFrames[0]->GetNumJoints();
	m_A.Init((short) referenceFrames.size(), numJoints*3, 1);
	m_B.Init((short) referenceFrames.size(), numJoints*3, 0);
	
	Printf("Building Distance Grid\n");
	BuildDistanceGrid(referenceFrames, correspondingFrames, weightSet);

	Printf("Performing Time Warp\n");
	float error = PerformDynamicProgramming();
	if(error == -1) return false;
	Printf("   Correspondence error is %f\n", error/m_slopeLimit);

	// Do IMW
	for(int i = 1; i < imwIterations; i++)
	{
		if(i == 1)
		{
			Printf("Building smoothness terms\n");
			BuildSmoothnessTerms((short) referenceFrames.size(), imwWeight);
		}
		Printf("Performing Space Warp\n");
		this->PerformSpaceWarpStep(referenceFrames, correspondingFrames,
			weightSet);
		
		Printf("Building Distance Grid\n");
		BuildDistanceGrid(referenceFrames, correspondingFrames, weightSet);

		Printf("Performing Time Warp\n");
		float timeWarp = PerformDynamicProgramming();
		if(timeWarp == -1) return false;
		Printf("   Correspondence error is %f\n", timeWarp/m_slopeLimit);
	}

	// Fill in the repetition vector
	repetitionVector.Init((short) this->m_repetitionArray.size(), 1);
	for(short i = 0; i < this->m_repetitionArray.size(); i++)
	{
		repetitionVector[i] = this->m_repetitionArray[i]/(float) m_slopeLimit;
	}

#if !__NO_OUTPUT
	repetitionVector.Print("   Repetition Vector");
#endif

	// Clear out all those big matrices
	Clear();

	return true;
}

bool crstCorrespondenceGenerator::DetermineCorrespondence(
		const crAnimation& referenceAnim,
		const crAnimation& correspondingAnim,
		short slopeLimit, VectorT<float>& repetitionVector,
		crWeightSet& weightSet, float deltaTime)
{
	short numRefFrames = (short)(referenceAnim.GetDuration()/deltaTime) + 1;
	short numCorrFrames = (short)(correspondingAnim.GetDuration()/deltaTime) + 1;
	
	// Make sure it is possible to find a correspondence given the slopeLimit
	if((numRefFrames > numCorrFrames*slopeLimit) ||
	   (numRefFrames*slopeLimit < numCorrFrames))
	{
		Errorf("crstCorrespondenceGenerator - Cannot find correspondences between provided frames.  Slope limit too small.");
		return false;
	}

	m_slopeLimit = slopeLimit;

	Printf("Building Distance Grid\n");
	m_distanceGrid.Init(numRefFrames, numCorrFrames*m_slopeLimit, FLT_MAX);

	crSkeleton skeleton;
	skeleton.Init(*this->m_SkeletonData);
	crFrame* refFrame = rage_new crFrame();
	crFrame* corFrame = rage_new crFrame();
	refFrame->InitCreateAnimationDofs(referenceAnim);
	corFrame->InitCreateAnimationDofs(correspondingAnim);

	// Fill in the distance grid
	for(short i = 0; i < numRefFrames; i++)
	{
		if(!(i%100)) Printf("   Starting row %d\n", i);
		bool compositedSuccess = false;
		
		if(referenceAnim.HasMoverTracks())
		{
			compositedSuccess = referenceAnim.CompositeFrameWithMover(
				deltaTime*i, deltaTime, *refFrame);
		}
		else
		{
			compositedSuccess = referenceAnim.CompositeFrame(deltaTime*i,
				*refFrame);
		}

		if(!compositedSuccess)
		{
			return false;
		}

		// We don't actually have to fill in the whole grid.  Given the
		// slopelimit, there is only a limit region of the grid that we can
		// reach.  We only fill in that region.
		short rowsToGo = numRefFrames - i - 1;
		short start = (short) Max(i/this->m_slopeLimit,
			numCorrFrames-((rowsToGo+1)*this->m_slopeLimit));
		short end = (short) Min((i+1)*this->m_slopeLimit,
			numCorrFrames-(rowsToGo/this->m_slopeLimit));
		for(short j = start; j < end; j++)
		{
			compositedSuccess = false;

			if(correspondingAnim.HasMoverTracks())
			{
				compositedSuccess = correspondingAnim.CompositeFrameWithMover(
					deltaTime*j, deltaTime, *corFrame);
			}
			else
			{
				compositedSuccess = correspondingAnim.CompositeFrame(
					deltaTime*j, *corFrame);
			}

			if(!compositedSuccess)
			{
				return false;
			}

			float correspondenceCost =
				ComputeCorrespondenceCostWithPointCloud(*refFrame, *corFrame,
				weightSet, skeleton);

			for(short k = 0; k < m_slopeLimit; k++)
			{
				m_distanceGrid[i][j*m_slopeLimit+k] = correspondenceCost;
			}
		}
	}

	delete(refFrame);
	delete(corFrame);

	Printf("Performing Time Warp\n");
	float error = PerformDynamicProgramming();
	if(error == -1) return false;
	Printf("   Correspondence error is %f\n", error/m_slopeLimit);

	// Fill in the repetition vector
	repetitionVector.Init((short) this->m_repetitionArray.size(), 1);
	for(short i = 0; i < this->m_repetitionArray.size(); i++)
	{
		repetitionVector[i] = this->m_repetitionArray[i]/(float) m_slopeLimit;
	}

#if !__NO_OUTPUT
	repetitionVector.Print("   Repetition Vector");
#endif

	// Clear out all those big matrices
	Clear();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crstCorrespondenceGenerator::BuildSmoothnessTerms(short numReferenceFrames,
												float weight)
{
	// Finite difference estimate of first derivatives
	m_smoothness.Init(numReferenceFrames,numReferenceFrames,0);
	m_smoothness[0][0] =
		m_smoothness[numReferenceFrames-1][numReferenceFrames-2] = -1;
	m_smoothness[0][1] =
		m_smoothness[numReferenceFrames-1][numReferenceFrames-1] = 1;
	for(short i = 1;  i < numReferenceFrames-1; i++)
	{
			m_smoothness[i][i-1] = -.5f;
			m_smoothness[i][i+1] = .5f;
	}
	m_smoothness.Scale(weight);
	
	Printf("    Computing smoothness transpose terms\n");
	MatrixT<float> smoothnessTranspose;
	m_smoothness.Transpose(smoothnessTranspose);
	smoothnessTranspose.Multiply(m_smoothness, m_smoothnessTProduct);
}

////////////////////////////////////////////////////////////////////////////////

void crstCorrespondenceGenerator::BuildDistanceGrid(
	const atArray< crstStyleFrame* >& referenceFrames,
	const atArray< crstStyleFrame* >& correspondingFrames,
	crWeightSet& weightSet)
{
	short numRefFrames = (short) referenceFrames.size();
	short numCorrespondingFrames = (short) correspondingFrames.size();
	m_distanceGrid.Init(numRefFrames, numCorrespondingFrames*m_slopeLimit, 0);

	crSkeleton skeleton;
	skeleton.Init(*this->m_SkeletonData);

	// Fill in the distance grid
	for(short i = 0; i < numRefFrames; i++)
	{
		Printf("   Starting row %d\n", i);
		for(short j = 0; j < numCorrespondingFrames; j++)
		{
			float correspondenceCost =
				ComputeCorrespondenceCost(referenceFrames[i],
				                          correspondingFrames[j], i, weightSet);
			for(short k = 0; k < m_slopeLimit; k++)
			{
				m_distanceGrid[i][j*m_slopeLimit+k] = correspondenceCost;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

float crstCorrespondenceGenerator::ComputeCorrespondenceCost(
	const crstStyleFrame* referenceFrame,
	const crstStyleFrame* correspondingFrame, short frameNum,
	crWeightSet& weightSet) const
{
	int numJoints1 = referenceFrame->GetNumJoints();
	int numJoints2 = correspondingFrame->GetNumJoints();

	if(numJoints1 != numJoints2)
	{
		Errorf("crstCorrespondenceGenerator - Comparing frames with different numbers of joints\n");
	}

	int numJoints = Min(numJoints1, numJoints2);
	float errorSum = 0;
	for(int i = 0; i < numJoints; i++)
	{
		if(!weightSet.GetAnimWeight(i)) continue;
		crstAxisAngleRotation jointOrientation1;
		referenceFrame->GetJointOrientation(i, jointOrientation1);

		crstAxisAngleRotation jointOrientation2;
		correspondingFrame->GetJointOrientation(i, jointOrientation2);

		Vec3V scaledAxis1;
		jointOrientation1.GetScaledAxis(scaledAxis1);

		Vec3V scaledAxis2;
		jointOrientation2.GetScaledAxis(scaledAxis2);

		for(short j = 0; j < 3; j++)
		{
			// Get spatial warp values
			float a = m_A[frameNum][i*3+j];
			float b = m_B[frameNum][i*3+j];
			
			// Calculate error after applying the spatial warp terms
			errorSum += square((a*scaledAxis1[j]+b)-scaledAxis2[j]);
		}
	}

	return errorSum;
}

////////////////////////////////////////////////////////////////////////////////

float crstCorrespondenceGenerator::ComputeCorrespondenceCostWithPointCloud(
	const crFrame& referenceAnimFrame,
	const crFrame& correspondingAnimFrame, crWeightSet& weightSet,
	crSkeleton& skeleton) const
{
	crpmPointCloud referencePointCloud;
	referenceAnimFrame.Pose(skeleton);
	skeleton.Update();
	referencePointCloud.AddPose(skeleton, &weightSet);
	
	crpmPointCloud correspondingPointCloud;
	correspondingAnimFrame.Pose(skeleton);
	skeleton.Update();
	correspondingPointCloud.AddPose(skeleton, &weightSet);

	crpmPointCloudTransform pointCloudTransform;
	float distance = referencePointCloud.ComputeDistance(
		correspondingPointCloud, pointCloudTransform);
	return distance;
}

////////////////////////////////////////////////////////////////////////////////

float crstCorrespondenceGenerator::PerformDynamicProgramming()
{
	short repetitionLimit = square(m_slopeLimit);

	short numRows = m_distanceGrid.GetRows();
	short numCols = m_distanceGrid.GetCols();

	// Used to store the current minimum error accrued to reach the
	// corresponding cell in the distance grid.
	MatrixT<float> totalError;
	totalError.Init(numRows, numCols, FLT_MAX);

	// Used to store the current number of repetitions on that row needed to
	// reach the corresponding cell with the minimum error.
	MatrixT<float> repetitionValues;
	repetitionValues.Init(numRows, numCols, 1);

	for(short i = 0; i < numRows; i++)
	{
		if(!(i%100)) Printf("    Starting row %d out of %d\n", i, numRows);
		for(short j = 0; j < numCols; j++)
		{
			if(i == 0)
			{
				// [0][j]
				if(j == 0)
				{
					// [0][0]
					totalError[i][j] = m_distanceGrid[i][j];
				}
				else if(j < repetitionLimit)
				{
					// [0][<repetitionLimit]
					totalError[i][j] = totalError[i][j-1]+m_distanceGrid[i][j];
				}
				// Note:  if [0][>=repetitionLimit], cell is unreachable and
				// keeps the error value of FLT_MAX

				repetitionValues[i][j] = (float)(j+1);
			}
			else if(j != 0)
			{
				// [>0][>0]
				short maxRepetition = Min(j, repetitionLimit);

				// Check all possible "paths" to the previous row
				float additionalError = 0;
				short bestRepetition = 0;
				totalError[i][j] = totalError[i-1][j-1];
				for(short k = 1; k < maxRepetition; k++)
				{
					additionalError += m_distanceGrid[i][j-k];
					float candidateError =
						totalError[i-1][j-k-1]+additionalError;
					if(candidateError < totalError[i][j])
					{
						totalError[i][j] = candidateError;
						bestRepetition = k;
					}
				}
				
				// Add in the error for the current cell
				totalError[i][j] += m_distanceGrid[i][j];

				// Update the repetition values with the stored best path to
				// the previous row
				repetitionValues[i][j] = (float)(bestRepetition+1);
			}
			// Note:  if [>0][0], cell is unreachable and keeps the error value
			// of FLT_MAX
		}
	}

	// Fill the repetition array
	m_repetitionArray.clear();
	m_repetitionArray.Reserve(numRows);
	m_repetitionArray.Resize(numRows);
	short checkSum = 0;
	short currentCol = numCols - 1;
	for(short i = numRows-1; i >= 0; i--)
	{
		short repValue = (short) (repetitionValues[i][currentCol]);
		m_repetitionArray[i] = repValue;
		currentCol = currentCol - repValue;
		checkSum = checkSum + repValue;
	}

	// Sanity Check.  The sum of the values in the repetition array should be
	// equal to the number of columns in the distance grid.
	AssertMsg(checkSum == numCols, "crstCorrespondenceGenerator - error during dynamic programming.  Bad result.");

	return totalError[numRows-1][numCols-1];
}

////////////////////////////////////////////////////////////////////////////////

void crstCorrespondenceGenerator::BuildW()
{
	m_WTranspose.Init(m_distanceGrid.GetRows(), m_distanceGrid.GetCols(), 0);
	m_WTProduct.Init(m_WTranspose.GetRows(), 0);
	short currentRow = 0;
	for(short i = 0; i < m_repetitionArray.size(); i++)
	{
		short repetitionValue = m_repetitionArray[i];
		for(short j = 0; j < repetitionValue; j++)
		{
			m_WTranspose[i][currentRow] = 1;
			currentRow++;
		}
		m_WTProduct[i] = repetitionValue;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstCorrespondenceGenerator::BuildUandY(VectorT<float>& U,
	VectorT<float>& Y, int jointNum, short axisDOF,
	const atArray< crstStyleFrame* >& referenceFrames,
	const atArray< crstStyleFrame* >& correspondingFrames) const
{
	//Printf("    Computing Y\n");
	Y.Init((short) (correspondingFrames.size()*m_slopeLimit), 0);
	for(short i = 0; i < correspondingFrames.size(); i++)
	{
		crstAxisAngleRotation jointOrientation;
		correspondingFrames[i]->GetJointOrientation(jointNum, jointOrientation);
		Vec3V scaledAxis;
		jointOrientation.GetScaledAxis(scaledAxis);
		for(short j = 0; j < m_slopeLimit; j++)
		{
			Y[i*m_slopeLimit+j] = scaledAxis[axisDOF];
		}
	}

	//Printf("    Compute U\n");
	U.Init((short) referenceFrames.size(), 0);
	for(short i = 0; i < referenceFrames.size(); i++)
	{
		crstAxisAngleRotation jointOrientation;
		referenceFrames[i]->GetJointOrientation(jointNum, jointOrientation);
		Vec3V scaledAxis;
		jointOrientation.GetScaledAxis(scaledAxis);
		U[i] = scaledAxis[axisDOF];
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstCorrespondenceGenerator::FillAandB(const VectorT<float>& U,
	VectorT<float>& Y, int jointNum, short axisDOF)
{
	MatrixT<float> temp1;
	MatrixT<float> temp2;
	MatrixT<float> temp3;
	VectorT<float> tempVec1;

	VectorT<float> storedVec;
	storedVec = U;
	storedVec.Scale(m_WTProduct);
	MatrixT<float> stored;
	stored.MakeDiagonal(storedVec);

	// Top of LHS
	tempVec1 = storedVec;
	tempVec1.Scale(U);
	temp1.MakeDiagonal(tempVec1);
	temp1.Add(m_smoothnessTProduct, temp2);
	Append(temp2, stored, temp1);

	// Bottom of LHS
	temp2.MakeDiagonal(m_WTProduct);
	temp2.Add(m_smoothnessTProduct, temp3);
	Append(stored, temp3, temp2);

	// Define the LHS
	MatrixT<float> LHS;
	Stack(temp1, temp2, LHS);

	//Printf("    Computing RHS\n");
	VectorT<float> RHS;
	
	// Top of RHS
	temp1 = m_WTranspose;
	temp1.ScaleRows(U);

	// Define the RHS
	Stack(temp1, m_WTranspose, temp2);
	temp2.Transform(Y, RHS);
	
	// Solve
	bool success = GaussianElimination(LHS, RHS);

	// Extract Results
	if(success)
	{
		short numSamples = U.GetSize();
		for(short i = 0; i < numSamples; i++)
		{
			m_A[i][jointNum*3+axisDOF] = RHS[i];
			m_B[i][jointNum*3+axisDOF] = RHS[i+numSamples];
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crstCorrespondenceGenerator::PerformSpaceWarpStep(
	const atArray< crstStyleFrame* >& referenceFrames,
	const atArray< crstStyleFrame* >& correspondingFrames,
	crWeightSet& weightSet)
{
	Printf("Building W Terms\n");
	this->BuildW();

	int numJoints = referenceFrames[0]->GetNumJoints();
	VectorT<float> U;
	VectorT<float> Y;

	for(short i = 0; i < numJoints; i++)
	{
		if(!weightSet.GetAnimWeight(i)) continue;
		Printf("Doing space warp for joint %d out of %d...", i, numJoints);
		for(short j = 0; j < 3; j++)
		{
			Printf("Axis %d...", j);
			this->BuildUandY(U, Y, i, j, referenceFrames, correspondingFrames);
			this->FillAandB(U, Y, i, j);
		}
		Printf("\n");
	}
}
