//
// crstyletransfer/style.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRSTYLETRANSFER_STYLE_H
#define CRSTYLETRANSFER_STYLE_H

#include "styleframe.h"

#include "atl/string.h"
#include "data/resource.h"
#include "data/struct.h"
#include "vector/matrixt.h"
#include "vector/vectort.h"

namespace rage
{

// pre declarations
class datSerialize;
class crstStyleState;


// PURPOSE: Contains a linear dynamic system for a pair of feature vectors.
//          Describes a system of the form:
//             x_(t+1) = A*x_t + B*u_t
//             y_t = C*x_t + D*u_t
//          where u_t and y_t are the input and output values of the system at
//          time t, respectively, and x_t is a hidden state at time t.
class crstLDS
{
public:

	// PURPOSE: Default constructor
	crstLDS();

	// PURPOSE: Resource constructor
	crstLDS(datResource&);

	// PURPOSE: Destructor
	~crstLDS();

	// PURPOSE: Placement
	DECLARE_PLACE(crstLDS);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Sets the system matrices of this LDS with the value of the
	//          supplied ones.  After setting the system matrices, the state
	//          vector is reset.
	// PARAMS:  A - the dynamical system matrix (NxN)
	//          B - input matrix (NxM)
	//          C - output matrix (LxN)
	//          D - direct feedthrough matrix (LxM)
	// RETURNS: true if the dimensionalities of the matrices are appropriate
	//               (e.g. A and B have the same number of rows), therefore
	//               defining a valid LDS
	//          false otherwise (the LDS will remain unchanged)
	bool Set(const MatrixT<float>& A, const MatrixT<float>& B,
		const MatrixT<float>& C, const MatrixT<float>& D);

	// PURPOSE: Make this LDS an exact copy of another one.
	// PARAMS:  toCopy - the LDS to copy
	void Set(const crstLDS& toCopy);

	// PURPOSE: Applies this linear dynamic system to the input with the current
	//          state.
	// PARAMS: input - the input vector
	//         output - a vector to hold the output after the LDS has been
	//                  applied
	//         state - current state vector.  Is replaced with the value of the
	//                 state after applying the LDS.
	// RETURNS: true if the dimensionalities of the input and state vector match
	//               the dimensionalities required by this LDS, and, thus, the
	//               LDS can be applied
	//          false otherwise (the output and state vector will remain
	//                unchanged)
	bool Apply(const VectorT<float>& input, VectorT<float>& output,
		VectorT<float>& state);

	// PURPOSE: Accessor for the system order of this LDS (i.e. the size of the
	//          state vector)
	// RETURNS: The system order of this LDS.
	short GetSystemOrder() const;

	// PURPOSE: Accessor for the number of inputs for this LDS
	// RETURNS: The required number of inputs for this LDS.
	short GetInputDims() const;

	// PURPOSE:  Compute A - B*D^(-1)*C
	// PARAMS:  result - a matrix to store the result in
	void GetDeterministicZero(MatrixT<float>& result);

	// PURPOSE: Serialize the LDS to/from a file
	// PARAMS: datSerialize - the serialization object for the file
	void Serialize(datSerialize&);

private:
	MatrixT<float> m_A;
	MatrixT<float> m_B;
	MatrixT<float> m_C;
	MatrixT<float> m_D;
};


// PURPOSE: Contains a style
class crstStyle
{
	friend class crstStyleGenerator;

public:
	// PURPOSE: Default constructor
	crstStyle();

	// PURPOSE: Resource constructor
	crstStyle(datResource&);

	// PURPOSE: Create an exact copy of a style.
	// PARAMS:  toCopy - the style to copy
	crstStyle(const crstStyle& toCopy);

	// PURPOSE: Destructor
	~crstStyle();

	// PURPOSE: Placement
	DECLARE_PLACE(crstStyle);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// Offline resourcing version
	static const int RORC_VERSION = 1;

	// PURPOSE: Load the style from a file
	// PARAMS: filename - name of style file
	// RETURNS: true if the file was loaded without problem
	//          false otherwise
	bool Load(const char* filename);

	// PURPOSE: Save the style to a file
	// PARAMS: filename - name of destination file
	// RETURNS: true if the file was saved without problem
	//          false otherwise
	bool Save(const char* filename);

	// PURPOSE: Serialize the style to/from a file
	// PARAMS: datSerialize - the serialization object for the file
	void Serialize(datSerialize&);

	// PURPOSE: Get style name
	// RETURNS: string containing name of the style
	const char* GetName() const;

	// PURPOSE:  Set the style name
	// PARAMS:  name - the new name for the style
	void SetName(const char* name);

	// PURPOSE:  Set this Style with the provided LDSs, means, and variances
	// PARAMS:  LDSs - An array that holds one LDS for the global change of the
	//                 character's position and orientation, one for each bone,
	//                 and then one more for timing.
	//          unstylizedMean - The mean of the unstylized frames
	//          unstylizedVariance - the variance of the unstylized frames
	//          stylizedMean - the mean of the stylized frames
	//          stylizedVariance - the variance of the stylized frames
	void Set(const atArray< crstLDS >& LDSs,
		const crstStyleFrame& unstylizedMean,
		const crstStyleFrame& unstylizedVariance,
		const crstStyleFrame& stylizedMean,
		const crstStyleFrame& stylizedVariance);

	// PURPOSE:  Apply this style to an unstylized frame.
	// PARAMS:  inOutFrame - the unstylized frame of motion.  It is replaced
	//                       with the value of the stylized frame.
	//          skeletonData - the skeleton data associated with the frame of
	//                         motion
	//          styleState - the current state of the style
	//          disableHeuristic - whether or not to disable the heuristic
	//                             blending
	//          confidence - confidence in the system.  Higher numbers mean you
	//                       have more confidence.  Expected to
	//                       be greater than 0.
	//          preference - preference for stylization.  Higher numbers mean
	//                       the heuristic should prefer to stylize the incoming
	//                       motion.
	// NOTE:  confidence and preference parameters are expected to be greater
	//        than 0.  For a mathematical definition of these parameters, see
	//        the original Siggraph 2005 Style Translation paper
	void Apply(crstStyleFrame& inOutFrame, const crSkeletonData& skeletonData,
		crstStyleState& styleState, bool disableHeuristic, int confidence,
		int preference);

	// PURPOSE:  Get the order of the entire style system
	// RETURNS:  The order of the style system
	int GetSystemOrder() const;

	// PURPOSE:  Get the order of a single LDS in the style system
	// PARAMS:  LDSIndex - the index for the LDS whose system order is desired
	// RETURNS:  The order of LDS
	int GetSystemOrder(int LDSIndex) const;

	// PURPOSE:  Get the number of LDSs in this style system
	// RETURNS:  number of LDSs
	int GetNumLDSs() const;

	// PURPOSE:  Get the value of the timing slope limit used to create this
	//           style.
	// RETURNS:  slope limit at a short
	short GetSlopeLimit() const;

	// PURPOSE:  Get the delta time value that was used to train this style.
	// RETURNS:  slope limit as a float
	float GetDeltaTime() const;

protected:
	// The name of the style
	atString m_Name;

	// The linear dynamic systems that define the style
	atArray< crstLDS > m_LDSs;

	// The mean of all the unstylized frames the style was trained on
	crstStyleFrame m_unstylizedMean;

	// The variance of all the unstylized frames the style was trained on
	crstStyleFrame m_unstylizedVariance;

	// The mean of all the stylized frames the style was trained on
	crstStyleFrame m_stylizedMean;

	// The variance of all the stylized frames the style was trained on
	crstStyleFrame m_stylizedVariance;

	// The slope limit that was used during time alignment on the training
	// motions
	short m_storedSlopeLimit;

	// The delta time value that was used on the original unstylized training
	// data
	float m_storedDeltaTime;

	// numSignificantDigits should be between 1 and 15 inclusive (it will be
	// clamped to this range)
	double ERF(double z, int numSignificantDigits = 12) const;
	double ERFC(double z, int numSignificantDigits = 12) const;

	// Given the normalized feature vector, this method determines the
	// alpha value that should be applied to the original motion
	float GetBlendTerm(const VectorT<float>& featureVector, int confidence,
		int preference) const;

	// Convert to/from feature indices, feature types+auxillary values, or
	// strings
	static void FeatureToIndex(crstFeatureType featuretype, int aux,
		int& index);
	static void IndexToFeature(int index, crstFeatureType& featuretype,
		int& aux);
	static void MapOutputToInput(crstFeatureType outputfeaturetype,
		int outputAux, crstFeatureType& inputfeaturetype, int& inputAux);
	static void FeatureToString(crstFeatureType featuretype, int aux,
		char* buffer, int bufferSize);
};


// PURPOSE:  A simple container that holds state information about a style.
class crstStyleState
{
public:
	// PURPOSE:  Default Constructor
	crstStyleState();

	// PURPOSE: Resource constructor
	crstStyleState(datResource&);

	// PURPOSE:  Construct a new style state object, initializing the
	//           state objects for use with the specified style.
	// PARAMS:  style - the style the state is associated with
	crstStyleState(const crstStyle* style);

	// PURPOSE: Placement
	DECLARE_PLACE(crstStyleState);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE:  Initialize the state objects for use with the specified style.
	// PARAMS:  style - the style the state is associated with
	void Init(const crstStyle* style);

	// PURPOSE:  Reset the internal state objects.
	// NOTE:  Reset does not adjust the global blend weight for the style.
	void Reset();

	// A blend term for the entire body during style translation
	// (0.0-1.0, 0 means blend none of the original in, 1 means blend all of the
	//  original in)
	float m_bodyBlendTerm;

	// Current state vectors for each of the LDSs
	atArray< VectorT<float> > m_states;

	// Global Blend Weight for the style
	// (0.0-1.0 - 0 means don't apply the style, 1 means apply the style fully
	float m_globalBlendWeight;

};

}; // namespace rage

#endif // CRSTYLETRANSFER_STYLE_H
