Project cutfile

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\framework\tools\src\dcc\libs
IncludePath $(RAGE_3RDPARTY)\cli\libxml2-2.7.6\include

Files {
	cutfevent.cpp
	cutfevent.h
	cutfeventargs.cpp
	cutfeventargs.h
	cutfeventdef.cpp
	cutfeventdef.h
	cutfile.cpp
	cutfile.h
	cutfile2.cpp
	cutfile2.h
	cutfobject.cpp
	cutfobject.h
	cutfdefines.h
	cutfchannel.h
}
Custom {
	cutfile.dtx
}
Parse {	
	cutfevent
	cutfeventargs
	cutfeventdef
	cutfile
	cutfile2
	cutfobject
}