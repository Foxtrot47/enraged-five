<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
              generate="psoChecks">

  <const name="MAX_EXTRA_ROOM_CHARS" value="24"/>
  <const name="CUTSCENE_MAX_CONCAT" value="40"/>
  <const name="CUTSCENE_LONG_OBJNAMELEN" value="64"/>
  
	<structdef type="::rage::cutfCutsceneFile2"  version="1" onPreSave="PreSave" onPostSave="PostSave" onPreLoad="PreLoad" onPostLoad="PostLoad">
    <pad bytes="257"/> <!-- IsLoaded and SceneName -->
		<float name="m_fTotalDuration"/>
		<string name="m_cFaceDir" type="member" size="256"/>
		<array name="m_iCutsceneFlags" type="member" size="4">
			<u32 />
		</array>
		<Vector3 name="m_vOffset"/>
		<float name="m_fRotation"/>
		<Vector3 name="m_vTriggerOffset"/>
		<array name="m_pCutsceneObjects" type="atArray">
			<pointer type="::rage::cutfObject" policy="owner"/>
		</array>
		<array name="m_pCutsceneLoadEventList" type="atArray">
			<pointer type="::rage::cutfEvent" policy="owner"/>
		</array>
		<array name="m_pCutsceneEventList" type="atArray">
			<pointer type="::rage::cutfEvent" policy="owner"/>
		</array>
    <array name="m_pCutsceneEventArgsList" type="atArray">
      <pointer type="::rage::cutfEventArgs" policy="owner"/>
    </array>
    <struct name="m_attributes" type="::rage::parAttributeList"/>
    <pointer name="m_cutfAttributes" type="::rage::cutfAttributeList" policy="simple_owner"/>
		<s32 name="m_iRangeStart"/>
		<s32 name="m_iRangeEnd"/>
    <s32 name="m_iAltRangeEnd"/>
		<float name="m_fSectionByTimeSliceDuration"/>
		<float name="m_fFadeOutCutsceneDuration"/>
		<float name="m_fFadeInGameDuration"/>
		<Color32 name="m_fadeInColor"/>
    <int name="m_iBlendOutCutsceneDuration"/>
    <int name="m_iBlendOutCutsceneOffset"/>
    <float name="m_fFadeOutGameDuration"/>
		<float name="m_fFadeInCutsceneDuration"/>
		<Color32 name="m_fadeOutColor"/>
    <u32 name="m_DayCoCHours" init="2097088" />
		<array name="m_cameraCutList" type="atArray">
			<float />
		</array>
		<array name="m_sectionSplitList" type="atArray">
			<float />
		</array>
		<array name="m_concatDataList" type="atFixedArray" size="CUTSCENE_MAX_CONCAT">
			<struct type="::rage::cutfCutsceneFile2::SConcatData" />
		</array>
    <array name="m_discardFrameList" type="atArray">
      <struct type="::rage::cutfCutsceneFile2::SDiscardedFrameData" />
    </array>
	</structdef>

	<structdef type="::rage::cutfCutsceneFile2::SConcatData" simple="true">
		<string name="cSceneName" type="atHashString" />
		<Vector3 name="vOffset" />
		<float name="fStartTime" />
		<float name="fRotation" />
    <float name="fPitch" />
    <float name="fRoll" />
    <int name="iRangeStart" />
    <int name="iRangeEnd" />
    <bool name="bValidForPlayBack" />
	</structdef>

  <structdef type="::rage::cutfCutsceneFile2::SDiscardedFrameData" simple="true">
    <string name="cSceneName" type="atHashString" />
    <array name="frames" type="atArray">
      <int />
    </array>
  </structdef>

  <structdef type="::rage::cutfAttributeList" simple="true">
    <array name="m_Items" type="atArray">
      <pointer type="::rage::cutfAttribute" policy="owner"/>
    </array>
  </structdef>

  <structdef type="rage::cutfAttribute">
    <string name="m_Name" type="atFinalHashString"/>
  </structdef>

  <structdef type="rage::cutfAttributeInt" base="rage::cutfAttribute" name="cutf_int">
    <int name="m_Value"/>
  </structdef>

  <structdef type="rage::cutfAttributeFloat" base="rage::cutfAttribute" name="cutf_float">
    <float name="m_Value"/>
  </structdef>

  <structdef type="rage::cutfAttributeBool" base="rage::cutfAttribute" name="cutf_bool">
    <bool name="m_Value"/>
  </structdef>

  <structdef type="rage::cutfAttributeString" base="rage::cutfAttribute" name="cutf_string">
    <string name="m_Value" type="atString"/>
  </structdef>
  
</ParserSchema>