// 
// cutfile/cutfile2.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutfile2.h"
#include "cutfile2_parser.h"

#include "cutscene/cutsoptimisations.h"
#include "cutfchannel.h"
#include "cutfevent.h"
#include "cutfeventargs.h"
#include "cutfeventdef.h" 
#include "cutfile.h"
#include "cutfobject.h"

#include "atl/map.h"
#include "file/asset.h"
#include "file/default_paths.h"
#include "parser/manager.h"
#include "parser/visitorutils.h"
#include "vector/vector3.h"
#include "string/string.h"
#include "system/param.h"

#if RSG_DURANGO || (RSG_PC && defined(_MSC_VER) && (_MSC_VER >= 1700))	// VS2012+ only

// Durango/Windows specific hack to stop stable_sort from calling new instead of rage_new
// this is a duplicate/specialization of the get_temporary_buffer in xmemory file
namespace std{	
	template<> inline pair<rage::cutfEvent **, ptrdiff_t> get_temporary_buffer(ptrdiff_t _Count) _NOEXCEPT
	{	// get raw temporary buffer of up to _Count elements
		rage::cutfEvent **_Pbuf;
		if (_Count < 0)
			_Count = 0;
		else if (((size_t)(-1) / sizeof (rage::cutfEvent*) < _Count))
			_Xbad_alloc();	// report no memory

		for (_Pbuf = 0; 0 < _Count; _Count /= 2)
			if ((_Pbuf = (rage::cutfEvent **)operator rage_heap_new(
				(size_t)_Count * sizeof (rage::cutfEvent*))) != 0)
				break;

		return (pair<rage::cutfEvent**, ptrdiff_t>(_Pbuf, _Count));
	}	
}
#endif //RSG_DURANGO || RSG_PC...

CUTS_OPTIMISATIONS()



#if __TOOL
	#include "RsULog/ULogger.h"
#endif

#if __BANK || __TOOL
	PARAM(EnableCutsceneTuning,"Enables loading and saving of cutscene tune files, light files  from the cutscene tune directory");
	PARAM(EnableSubtitleTuning,"Allows subtitles to be hotloaded from the assets folder");
	PARAM(enablecutscenelightauthoring, "[cutscene] All authoring of cutscene lights such that the data can be saved out"); 
	PARAM(EnableCutSceneGameDataTuning, "Enables loading and saving of cutscene tune files, light files  from the cutscene tune directory"); 

	rage::fwFlags32 rage::CutfileNonParseData::m_FileTuningFlags; //PARAM_EnableCutsceneTuning.Get();
	rage::fwFlags32 rage::CutfileNonParseData::m_FileTuningStatusFlags;
	rage::fwFlags32 rage::CutfileNonParseData::m_FileGameDataTuningFlags; 
#endif // __BANK || __TOOL



RAGE_DEFINE_CHANNEL( Cutfile );

namespace rage {

cutfAttributeList::~cutfAttributeList()
{
	for(int i = 0; i < m_Items.GetCount(); i++)
	{
		delete m_Items[i];
	}
}

//##############################################################################

const cutfCutsceneFile2* cutfCutsceneFile2::sm_pCurrentCutfile = NULL;

cutfCutsceneFile2::cutfCutsceneFile2()
: m_pCutsceneFlagsBitSet( NULL )
, m_cutfAttributes(NULL)
{

    Clear();
}

cutfCutsceneFile2::~cutfCutsceneFile2()
{
    Clear();

    if ( m_pCutsceneFlagsBitSet != NULL )
    {
        delete m_pCutsceneFlagsBitSet;
        m_pCutsceneFlagsBitSet = NULL;
    }
}

void cutfCutsceneFile2::Clear()
{
    m_bIsLoaded = false;
    m_cSceneName[0] = 0;
    m_fTotalDuration = 0.0f;
    m_cFaceDir[0] = 0;
    
    m_fRotation = 0.0f;
	m_fPitch = 0.0f;
	m_fRoll = 0.0f; 
    m_iRangeStart = 0;
    m_iRangeEnd = 0;
	m_iAltRangeEnd = 0;
    m_fSectionByTimeSliceDuration = 4.0f;
	m_DayCoCHours = 2097088;
    
    m_fFadeOutCutsceneDuration = CUTSCENE_FADE_TIME;
    m_fFadeInGameDuration = CUTSCENE_FADE_TIME;
    m_fadeInColor.Setf( 0.0f, 0.0f, 0.0f );

    m_fFadeOutGameDuration = CUTSCENE_FADE_TIME;
    m_fFadeInCutsceneDuration = CUTSCENE_FADE_TIME;
    m_fadeOutColor.Setf( 0.0f, 0.0f, 0.0f );

	m_iBlendOutCutsceneDuration = 0;
	m_iBlendOutCutsceneOffset = 0;

    for ( int i = 0; i < 4; ++i )
    {
        m_iCutsceneFlags[i] = 0;
    }

    m_vOffset.Zero();
	m_vTriggerOffset.Zero();

    for ( int i = 0; i < m_pCutsceneObjects.GetCount(); ++i )
    {
        delete m_pCutsceneObjects[i];
    }

    m_pCutsceneObjects.Reset();

    for ( int i = 0; i < m_pCutsceneLoadEventList.GetCount(); ++i )
    {
        delete m_pCutsceneLoadEventList[i];
    }

    m_pCutsceneLoadEventList.Reset();

    for ( int i = 0; i < m_pCutsceneEventList.GetCount(); ++i )
    {
        delete m_pCutsceneEventList[i];
    }

    m_pCutsceneEventList.Reset();

    for ( int i = 0; i < m_pCutsceneEventArgsList.GetCount(); ++i )
    {
        if ( m_pCutsceneEventArgsList[i]->GetRef() == 0 )
        {
            delete m_pCutsceneEventArgsList[i];
        }
    }

    m_pCutsceneEventArgsList.Reset();

    m_attributes.Reset();
	delete m_cutfAttributes;
	m_cutfAttributes = NULL;

    m_cameraCutList.Reset();
    m_sectionSplitList.Reset();
    m_concatDataList.Reset();
	m_discardFrameList.Reset();
}

void cutfCutsceneFile2::ConvertToCutfAttributeList(parAttributeList& from, cutfAttributeList*& to)
{
	delete to;
	to = NULL;

	atArray<parAttribute>& allAttrs = from.GetAttributeArray();
	if (allAttrs.GetCount() > 0)
	{
		to = rage_new cutfAttributeList;

		for(int i = 0; i < allAttrs.GetCount(); i++)
		{
			parAttribute& attr = allAttrs[i];

			atFinalHashString nameHash(attr.GetName());

			switch(attr.GetType())
			{
			case parAttribute::INT64:
				{
					cutfAttributeInt* value = rage_new cutfAttributeInt;
					value->m_Value = attr.GetIntValue();
					value->m_Name = nameHash;
					to->m_Items.PushAndGrow(value);
				}
				break;
			case parAttribute::DOUBLE:
				{
					cutfAttributeFloat* value = rage_new cutfAttributeFloat;
					value->m_Value = attr.GetFloatValue();
					value->m_Name = nameHash;
					to->m_Items.PushAndGrow(value);
				}
				break;
			case parAttribute::BOOL:
				{
					cutfAttributeBool* value = rage_new cutfAttributeBool;
					value->m_Value = attr.GetBoolValue();
					value->m_Name = nameHash;
					to->m_Items.PushAndGrow(value);
				}
				break;
			case parAttribute::STRING:
				{
					cutfAttributeString* value = rage_new cutfAttributeString;
					value->m_Value = attr.GetStringValue();
					value->m_Name = nameHash;
					to->m_Items.PushAndGrow(value);
				}
				break;
			}
		}
	}
}

void cutfCutsceneFile2::ConvertToParAttributeList(cutfAttributeList*& from, parAttributeList& to)
{
	if (from)
	{
		to.Reset(); // Only modify the parAttributeList if a cutfAttributeList exists, otherwise we probably loaded an old file

		for(int i = 0; i < from->m_Items.GetCount(); i++)
		{
			cutfAttribute*& item = from->m_Items[i];
			const char* name = item->m_Name.GetCStr();
			switch(item->GetType())
			{
			case cutfAttribute::ATTR_INT:
				{
					cutfAttributeInt* value = smart_cast<cutfAttributeInt*>(item);
					to.AddAttribute(name, (s64)value->m_Value);
				}
				break;
			case cutfAttribute::ATTR_FLOAT:
				{
					cutfAttributeFloat* value = smart_cast<cutfAttributeFloat*>(item);
					to.AddAttribute(name, value->m_Value);
				}
				break;
			case cutfAttribute::ATTR_BOOL:
				{
					cutfAttributeBool* value = smart_cast<cutfAttributeBool*>(item);
					to.AddAttribute(name, value->m_Value);
				}
				break;
			case cutfAttribute::ATTR_STRING:
				{
					cutfAttributeString* value = smart_cast<cutfAttributeString*>(item);
					to.AddAttribute(name, value->m_Value);
				}
				break;
			case cutfAttribute::ATTR_NONE:
				Assertf(0, "Invalid type");
				break;
			}
		}
	}
}

void cutfCutsceneFile2::ConvertArgIndicesToPointers()
{
	sm_pCurrentCutfile = this;

	ConvertToParAttributeList(m_cutfAttributes, m_attributes);

	class Visitor : public parNonGenericVisitor
	{
	public:
		Visitor() {
			parInstanceVisitor::TypeMask typeMask(false);
			typeMask.Set(parMemberType::TYPE_STRUCT);
			SetMemberTypeMask(typeMask);
		}
		virtual ~Visitor() {}

		virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
		{
			if (metadata.IsSubclassOf<cutfEventArgs>())
			{
				cutfEventArgs* eventArgs = reinterpret_cast<cutfEventArgs*>(dataPtr);
				eventArgs->ConvertAttributesAfterLoad();
			}

			if (metadata.IsSubclassOf<cutfEventArgsList>())
			{
				cutfEventArgsList* eventArgs = reinterpret_cast<cutfEventArgsList*>(dataPtr);
				eventArgs->ConvertArgIndicesToPointers();
			}

			if (metadata.IsSubclassOf<cutfEvent>())
			{
				cutfEvent* event = reinterpret_cast<cutfEvent*>(dataPtr);
				event->ConvertArgIndicesToPointers();
			}

			if (metadata.IsSubclassOf<cutfObjectIdListEvent>())
			{
				cutfObjectIdListEvent* event = reinterpret_cast<cutfObjectIdListEvent*>(dataPtr);
				event->ConvertArgIndicesToPointers();
			}

			if (metadata.IsSubclassOf<cutfEventDef>())
			{
				cutfEventDef* eventDef = reinterpret_cast<cutfEventDef*>(dataPtr);
				eventDef->ConvertArgIndicesToPointers();
			}

			if (metadata.IsSubclassOf<cutfEventArgsDef>())
			{
				cutfEventArgsDef* eventArgsDef = reinterpret_cast<cutfEventArgsDef*>(dataPtr);
				eventArgsDef->ConvertAttributesAfterLoad();
			}

			if (metadata.IsSubclassOf<cutfObject>())
			{
				cutfObject* object = reinterpret_cast<cutfObject*>(dataPtr);
				object->ConvertAttributesAfterLoad();
			}


			parNonGenericVisitor::VisitStructure(dataPtr, metadata);
		}
	};

	Visitor v;
	v.Visit(*this);

	sm_pCurrentCutfile = NULL;
}


#if __BANK || __TOOL
void cutfCutsceneFile2::ConvertArgPointersToIndices()
{
	sm_pCurrentCutfile = this;

	ConvertToCutfAttributeList(m_attributes, m_cutfAttributes);

	class Visitor : public parNonGenericVisitor
	{
	public:
		Visitor() {
			parInstanceVisitor::TypeMask typeMask(false);
			typeMask.Set(parMemberType::TYPE_STRUCT);
			SetMemberTypeMask(typeMask);
		}
		virtual ~Visitor() {}

		virtual void VisitStructure(parPtrToStructure dataPtr, parStructure& metadata)
		{
			if (metadata.IsSubclassOf<cutfEventArgs>())
			{
				cutfEventArgs* eventArgs = reinterpret_cast<cutfEventArgs*>(dataPtr);
				eventArgs->ConvertAttributesForSave();
			}

			if (metadata.IsSubclassOf<cutfEventArgsList>())
			{
				cutfEventArgsList* eventArgs = reinterpret_cast<cutfEventArgsList*>(dataPtr);
				eventArgs->ConvertArgPointersToIndices();
			}

			if (metadata.IsSubclassOf<cutfEvent>())
			{
				cutfEvent* event = reinterpret_cast<cutfEvent*>(dataPtr);
				event->ConvertArgPointersToIndices();
			}

			if (metadata.IsSubclassOf<cutfObjectIdListEvent>())
			{
				cutfObjectIdListEvent* event = reinterpret_cast<cutfObjectIdListEvent*>(dataPtr);
				event->ConvertArgPointersToIndices();
			}

			if (metadata.IsSubclassOf<cutfEventDef>())
			{
				cutfEventDef* eventDef = reinterpret_cast<cutfEventDef*>(dataPtr);
				eventDef->ConvertArgPointersToIndices();
			}

			if (metadata.IsSubclassOf<cutfEventArgsDef>())
			{
				cutfEventArgsDef* eventArgsDef = reinterpret_cast<cutfEventArgsDef*>(dataPtr);
				eventArgsDef->ConvertAttributesForSave();
			}

			if (metadata.IsSubclassOf<cutfObject>())
			{
				cutfObject* object = reinterpret_cast<cutfObject*>(dataPtr);
				object->ConvertAttributesForSave();
			}

			parNonGenericVisitor::VisitStructure(dataPtr, metadata);
		}
	};

	Visitor v;
	v.Visit(*this);

	sm_pCurrentCutfile = NULL;
}


bool cutfCutsceneFile2::SaveFile( const char* pFilename, const char* pExtension )
{
    // determine the save type
    char cSaveFilename[RAGE_MAX_PATH];
    ASSET.FullWritePath( cSaveFilename, RAGE_MAX_PATH, pFilename, pExtension );
    const char* pFileExtension = ASSET.FindExtensionInPath( cSaveFilename );
    if ( pFileExtension == NULL )
    {
        pExtension = pFileExtension = PI_CUTSCENE_XMLFILE_EXT;
    }

    // in case a non-standard method of adding objects was used
    EnforceSequentialObjectIds();

#if !__FINAL
    if ( (strstr( pFileExtension, "tune" ) != NULL) )
    {
        cutfDisplayf( "About to save tune file '%s'.", cSaveFilename );
    }
    else
    {
        cutfDisplayf( "About to save cut file '%s'.", cSaveFilename );
    }
#endif // __FINAL

	ConvertArgPointersToIndices();

    ASSET.CreateLeadingPath( pFilename );

	sysMemStartDebug();

    bool bResult = false;
    if ( (strstr( pFileExtension, "xml" ) != NULL) || (strstr( pFileExtension, "tune" ) != NULL) || (strstr( pFileExtension, "sub" ) != NULL) )
    {
        bResult = PARSER.SaveObject( pFilename, pExtension ? pExtension : "", this, parManager::XML );
    }
    else if ( strstr( pFileExtension, "bin" ) != NULL )
    {
		bResult = PARSER.SaveObject( pFilename, pExtension ? pExtension : "", this, parManager::BINARY );
    }
#if __DEV
    else
    {
        cutfErrorf( "File extension '%s' not supported.", pFileExtension );
    }
#endif

	sysMemEndDebug();

    if ( bResult )
    {
        m_bIsLoaded = true;

        // store the scene name
        char cSceneName[RAGE_MAX_PATH];
        ASSET.RemoveExtensionFromPath( cSceneName, sizeof(cSceneName), pFilename );
        safecpy( m_cSceneName, ASSET.FileName( cSceneName ), sizeof(m_cSceneName) );

        cutfDisplayf( "Save success." );
    }
    
    return bResult;
}
#endif // __BANK || __TOOL

bool cutfCutsceneFile2::LoadFile( const char* pFilename, const char* pExtension )
{
    // determine the load type
    char cLoadFilename[RAGE_MAX_PATH];
    ASSET.FullReadPath( cLoadFilename, RAGE_MAX_PATH, pFilename, pExtension );
    const char* pFileExtension = ASSET.FindExtensionInPath( cLoadFilename );  
    if ( pFileExtension == NULL )
    {
        pExtension = pFileExtension = PI_CUTSCENE_XMLFILE_EXT;
    }

#if __DEV
    cutfDisplayf( "About to load cut file '%s'.", cLoadFilename );
#endif

    // Based on the extension (and later, a version number), determine which Parse function to use
    bool bResult = false;

#if HACK_GTA4
	if ( (strstr( pFileExtension, "xml" ) != NULL) || (strstr( pFileExtension, "bin" ) != NULL) 
        || (strstr( pFileExtension, "tune" ) != NULL) || (strstr( pFileExtension, "cut" ) != NULL) )
#else
	if ( (strstr( pFileExtension, "xml" ) != NULL) || (strstr( pFileExtension, "bin" ) != NULL) 
		|| (strstr( pFileExtension, "tune" ) != NULL)
#endif
	{
        parTree *pTree = PARSER.LoadTree( pFilename, pExtension ? pExtension : "" );
        if ( pTree )
        {
            bResult = ParseXml( pTree );
            delete pTree;
        }
#if __DEV
        else
        {
            cutfDisplayf( "Could not load tree." );
        }
#endif
    }
    else
    {
        fiStream* pStream = ASSET.Open( pFilename, pExtension );
        if ( pStream != NULL )
        {
			Quitf(ERR_GEN_CUT_1,"Dave compiled out support for old formats: %s.%s",pFilename,pExtension);
        }
#if __DEV
        else
        {
            cutfDisplayf( "Could not open file stream." );
        }
#endif
    }

#if __DEV
    cutfDisplayf( "Load %s.", bResult ? "success" : "failed" );
#endif

    if ( bResult )
    {
        // in case there was some manual tampering with the file
        EnforceSequentialObjectIds();

        // store the scene name
        char cSceneName[RAGE_MAX_PATH];
        ASSET.RemoveExtensionFromPath( cSceneName, sizeof(cSceneName), pFilename );
        safecpy( m_cSceneName, ASSET.FileName( cSceneName ), sizeof(m_cSceneName) );
    }

#if __BANK
	LoadAndReplaceLightsInCutfile(m_cSceneName);

	if(LoadSubtitleFile())
	{
		CutfileNonParseData::m_FileTuningStatusFlags.SetFlag(CutfileNonParseData::CUTSCENE_SUBTITLE_TUNING_FILE_LOADED);
	}

	if(LoadGameDataTuneFile())
	{
		CutfileNonParseData::m_FileTuningStatusFlags.SetFlag(CutfileNonParseData::CUTSCENE_GAMEDATA_TUNING_FILE_LOADED);
	}

#endif

    return bResult;
}

void cutfCutsceneFile2::InitExternallyLoadedFile(const char* sceneName, bool IsLoaded)
{
	SetSceneName(sceneName); 
	m_bIsLoaded = IsLoaded; 

#if __BANK
	LoadAndReplaceLightsInCutfile(m_cSceneName);
	if(LoadSubtitleFile())
	{
		CutfileNonParseData::m_FileTuningStatusFlags.SetFlag(CutfileNonParseData::CUTSCENE_SUBTITLE_TUNING_FILE_LOADED);
	}

	if(LoadGameDataTuneFile())
	{
		CutfileNonParseData::m_FileTuningStatusFlags.SetFlag(CutfileNonParseData::CUTSCENE_GAMEDATA_TUNING_FILE_LOADED);
	}

#endif
}

bool cutfCutsceneFile2::ParseXml( parTree* pTree )
{
#if __DEV
    cutfDisplayf( "About to parse xml." );
#endif

    Clear();

    bool bResult = false;

    // First, look for a version number  
    const parAttribute *pAttribute = pTree->GetRoot()->GetElement().FindAttribute( "v" );
    if ( pAttribute == NULL )
    {
		Quitf(ERR_GEN_CUT_2,"Dave compiled out support for old formats");
    }
    else
    {
        u32 iVersion = atoi( pAttribute->GetStringValue() );
        if ( iVersion == CUTSCENE_FILE_VERSION_NUMBER )
        {
            bResult = PARSER.LoadObject( pTree->GetRoot(), *this );           

            if ( bResult )
            {
				ConvertArgIndicesToPointers();

				// Minor Upgrade:  Draw Distance Events
				bResult = UpgradeDrawDistances();
            }
        }
        else
        {
            cutfErrorf( "Unable to parse cutfCutsceneFile2 version %d.  The current supported version is %d", 
                iVersion, CUTSCENE_FILE_VERSION_NUMBER );
            bResult = false;
        }
    }

    if ( bResult )
    {
        m_bIsLoaded = true;

#if __DEV
        cutfDisplayf( "Xml fully parsed." );
#endif
    }

    return bResult;
}

#if __BANK || __TOOL

const char * g_CutsceneTuneDirectory = "common:/non_final/cutscene/tune";
const char * g_CutsceneSubtitleDirectory = "assets:/cuts/!!cutsubs";
const char * g_SceneLightFileDirectory = RS_ASSETS "/cuts";
const char * g_CutsceneGameDataDirectory = "assets:/cuts/!!game_data/";

bool cutfCutsceneFile2::SaveTuneFile()
{
	if (!PARAM_EnableCutsceneTuning.Get())
		return false;

    char cFullPath[RAGE_MAX_PATH];
    sprintf( cFullPath, "%s/%s", g_CutsceneTuneDirectory, GetSceneName() );

    return SaveTuneFile( cFullPath );
}

bool cutfCutsceneFile2::SaveTuneFile( const char *pFilename )
{
	if (!PARAM_EnableCutsceneTuning.Get())
		return false;

    char cFullPath[RAGE_MAX_PATH];
    ASSET.RemoveExtensionFromPath( cFullPath, sizeof(cFullPath), pFilename );
    
    return SaveFile( cFullPath, PI_CUTSCENE_TUNEFILE_EXT );
}




bool cutfCutsceneFile2::LoadTuneFile()
{
	if (!PARAM_EnableCutsceneTuning.Get())
		return false;

    char cFullPath[RAGE_MAX_PATH];
    sprintf( cFullPath, "%s/%s", g_CutsceneTuneDirectory, GetSceneName() );

    if ( ASSET.Exists( cFullPath, PI_CUTSCENE_TUNEFILE_EXT ) )
    {
        return LoadTuneFile( cFullPath );
    }

    return true;
}

bool cutfCutsceneFile2::LoadTuneFile( const char *pFilename )
{
	if (!PARAM_EnableCutsceneTuning.Get())
		return false;

    // determine the load type
    const char *pExtension = PI_CUTSCENE_TUNEFILE_EXT;

    char cLoadFilename[RAGE_MAX_PATH];
    ASSET.FullReadPath( cLoadFilename, RAGE_MAX_PATH, pFilename, pExtension );
    const char* pFileExtension = ASSET.FindExtensionInPath( cLoadFilename );  
    if ( pFileExtension == NULL )
    {
        pExtension = pFileExtension = PI_CUTSCENE_TUNEFILE_EXT;
    }

#if __DEV
    cutfDisplayf( "About to load tune file '%s'.", cLoadFilename );
#endif

    // Based on the extension (and later, a version number), determine which Parse function to use
    bool bResult = false;
    if ( (strstr( pFileExtension, "xml" ) != NULL) || (strstr( pFileExtension, "bin" ) != NULL) 
        || (strstr( pFileExtension, "tune" ) != NULL) )
    {
        parTree *pTree = PARSER.LoadTree( pFilename, pExtension ? pExtension : "" );
        if ( pTree )
        {
            bResult = ParseTuneXml( pTree );
            delete pTree;
        }
#if __DEV
        else
        {
            cutfDisplayf( "Could not load tree." );
        }
#endif
    }
    else
    {
#if __DEV
        cutfDisplayf( "Could not open file stream." );
#endif
    }

#if __DEV
    cutfDisplayf( "Load %s.", bResult ? "success" : "failed" );
#endif

    return bResult;
}

bool cutfCutsceneFile2::ParseTuneXml( parTree *pTree )
{
    cutfCutsceneFile2 tuneCutfile;
    if ( !tuneCutfile.ParseXml( pTree ) )
    {
        return false;
    }

    // Copy the Depth of Field setting
    atBitSet &cutsceneFlags = const_cast<atBitSet &>( GetCutsceneFlags() );
    cutsceneFlags.Set( CUTSCENE_ENABLE_DEPTH_OF_FIELD_FLAG, tuneCutfile.GetCutsceneFlags().IsSet( CUTSCENE_ENABLE_DEPTH_OF_FIELD_FLAG ) );

    // remove the cut file's tunable items
    atArray<cutfObject *> fixupObjectList;
    FindObjectsOfType( CUTSCENE_FIXUP_MODEL_OBJECT_TYPE, fixupObjectList );
    for ( int i = 0; i < fixupObjectList.GetCount(); ++i )
    {
        RemoveObject( fixupObjectList[i] );
    }

    atArray<cutfObject *> hiddenObjectList;
    FindObjectsOfType( CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE, hiddenObjectList );
    for ( int i = 0; i < hiddenObjectList.GetCount(); ++i )
    {
        RemoveObject( hiddenObjectList[i] );
    }

    for ( int i = 0; i < m_pCutsceneLoadEventList.GetCount(); )
    {
        if ( m_pCutsceneLoadEventList[i]->GetEventId() == CUTSCENE_FIXUP_OBJECTS_EVENT )
        {
            RemoveLoadEvent( m_pCutsceneLoadEventList[i] );
        }
        else if ( m_pCutsceneLoadEventList[i]->GetEventId() == CUTSCENE_HIDE_OBJECTS_EVENT )
        {
            RemoveLoadEvent( m_pCutsceneLoadEventList[i] );
        }
        else
        {
            ++i;
        }
    }
   
    // add the tune file's items to the cut file
    SetRotation( tuneCutfile.GetRotation() );
    SetOffset( tuneCutfile.GetOffset() );

    if ( GetCutsceneFlags().IsSet( CUTSCENE_IS_SECTIONED_FLAG ) )
    {
        // If we're sectioned, our RangeEnd can get smaller, but not bigger
        if ( tuneCutfile.GetRangeEnd() <= GetRangeEnd() )
        {
            SetRangeEnd( tuneCutfile.GetRangeEnd() );
        }
        else
        {
            cutfWarningf( "The .cuttune's RangeEnd (%d) is greater than our RangeEnd (%d).  Unable to use .cuttune's value because we are sectioned.",
                tuneCutfile.GetRangeEnd(), GetRangeEnd() );
        }
    }
    else
    {
        SetRangeEnd( tuneCutfile.GetRangeEnd() );

        // Keep the total duration in sync
        SetTotalDuration( GetRangeEnd() / CUTSCENE_FPS );
    }

    if ( (tuneCutfile.GetRangeStart() >= 0) && (tuneCutfile.GetRangeStart() < GetRangeEnd() - 1) )
    {
        // our range cannot be smaller than 0, nor bigger than the range end - 1
        SetRangeStart( tuneCutfile.GetRangeStart() );
    }
    else
    {
        cutfWarningf( "The .cuttune's RangeStart (%d) is out of range (0 - %d).  Leaving it at %d.", 
            tuneCutfile.GetRangeStart(), GetRangeEnd(), GetRangeStart() );
    }

    atArray<cutfObject *> assetMgrObjectList;
    FindObjectsOfType( CUTSCENE_ASSET_MANAGER_OBJECT_TYPE, assetMgrObjectList );

    // this should never happen, but just in case
    s32 iAassetMgrObjectId = -1;
    if ( assetMgrObjectList.GetCount() == 0 )
    {
        cutfAssetManagerObject *pAssetMgrObject = rage_new cutfAssetManagerObject();
        AddObject( pAssetMgrObject );

        assetMgrObjectList.Grow() = pAssetMgrObject;        
    }

    iAassetMgrObjectId = assetMgrObjectList[0]->GetObjectId();

    fixupObjectList.Reset();
    tuneCutfile.FindObjectsOfType( CUTSCENE_FIXUP_MODEL_OBJECT_TYPE, fixupObjectList );
    if ( fixupObjectList.GetCount() > 0 )
    {
        atArray<s32> fixupObjectIdList;
        fixupObjectIdList.Reserve( fixupObjectList.GetCount() );
        
        for ( int i = 0; i < fixupObjectList.GetCount(); ++i )
        {
            cutfObject *pObject = fixupObjectList[i]->Clone();
            AddObject( pObject );

            fixupObjectIdList.Append() = pObject->GetObjectId();
        }

        AddLoadEvent( rage_new cutfObjectIdEvent( iAassetMgrObjectId, 0.0f, CUTSCENE_FIXUP_OBJECTS_EVENT, 
            rage_new cutfObjectIdListEventArgs( fixupObjectIdList ) ) );
    }

    hiddenObjectList.Reset();
    tuneCutfile.FindObjectsOfType( CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE, hiddenObjectList );
    if ( hiddenObjectList.GetCount() > 0 )
    {
        atArray<s32> hiddenObjectIdList;
        hiddenObjectIdList.Reserve( hiddenObjectList.GetCount() );

        for ( int i = 0; i < hiddenObjectList.GetCount(); ++i )
        {
            cutfObject *pObject = hiddenObjectList[i]->Clone();
            AddObject( pObject );

            hiddenObjectIdList.Grow() = pObject->GetObjectId();
        }

        AddLoadEvent( rage_new cutfObjectIdEvent( iAassetMgrObjectId, 0.0f, CUTSCENE_HIDE_OBJECTS_EVENT, 
            rage_new cutfObjectIdListEventArgs( hiddenObjectIdList ) ) );
    }

    atArray<cutfObject *> cameraObjectList;
    FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );   
    if ( cameraObjectList.GetCount() > 0 )
    {
        atArray<cutfObject *> tuneCameraObjectList;
        tuneCutfile.FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, tuneCameraObjectList );

        if ( tuneCameraObjectList.GetCount() > 0 )
        {
            atArray<cutfEvent *> cameraEventList;
            FindEventsForObjectIdOnly( cameraObjectList[0]->GetObjectId(), GetEventList(), cameraEventList );

            atArray<cutfEvent *> tuneCameraEventList;
            tuneCutfile.FindEventsForObjectIdOnly( tuneCameraObjectList[0]->GetObjectId(), GetEventList(), tuneCameraEventList );

            atArray<s32> cameraCutEventIndexList;
            for ( int i = 0; i < cameraEventList.GetCount(); ++i )
            {
                if ( cameraEventList[i]->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT )
                {
                    cameraCutEventIndexList.PushAndGrow( i );
                }
            }

            atArray<s32> tuneCameraCutEventIndexList;
            for ( int i = 0; i < tuneCameraEventList.GetCount(); ++i )
            {
                if ( tuneCameraEventList[i]->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT )
                {
                    tuneCameraCutEventIndexList.PushAndGrow( i );
                }
            }

            for ( int i = 0; (i < cameraCutEventIndexList.GetCount()) && (i < tuneCameraCutEventIndexList.GetCount()); ++i )
            {                
                cutfEvent *pTuneEvent = tuneCameraEventList[tuneCameraCutEventIndexList[i]];
                const cutfCameraCutEventArgs *pTuneCameraCutEventArgs = static_cast<const cutfCameraCutEventArgs *>( pTuneEvent->GetEventArgs() );

                cutfCameraCutEventArgs *pCameraCutEventArgs;

                cutfEvent *pEvent = cameraEventList[cameraCutEventIndexList[i]];
                cutfEventArgs *pEventArgs = const_cast<cutfEventArgs *>( pEvent->GetEventArgs() );
                if ( pEventArgs->GetType() == CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE )
                {
                    pCameraCutEventArgs = static_cast<cutfCameraCutEventArgs *>( pEventArgs );
                }
                else
                {
                    pEvent->SetEventArgs( NULL );
                    RemoveEventArgs( pEventArgs );

                    pCameraCutEventArgs = rage_new cutfCameraCutEventArgs();

                    pEvent->SetEventArgs( pCameraCutEventArgs );
                    AddEventArgs( pCameraCutEventArgs );
                }

                pCameraCutEventArgs->SetNearDrawDistance( pTuneCameraCutEventArgs->GetNearDrawDistance() );
                pCameraCutEventArgs->SetFarDrawDistance( pTuneCameraCutEventArgs->GetFarDrawDistance() );
				pCameraCutEventArgs->SetMapLodScale( pTuneCameraCutEventArgs->GetMapLodScale() );
            }
        }
    }

	atArray<cutfObjectIdEvent*> variationEventList;
	atArray<cutfObjectVariationEventArgs*> variationEventArgList;
	atArray<cutfVehicleVariationEventArgs*> VehicleVariationEventArgList; 
	atArray<cutfVehicleExtraEventArgs*> VehicleExtraEventArgList; 

	const atArray<cutfEvent *> &eventList = tuneCutfile.GetEventList();
	for(int i=0; i < eventList.GetCount(); ++i)
	{
		if(eventList[i]->GetEventId() == CUTSCENE_SET_VARIATION_EVENT)
		{
			cutfObjectIdEvent *pObjectVariationEvent = static_cast<cutfObjectIdEvent *>( eventList[i] );
			
			variationEventList.PushAndGrow(pObjectVariationEvent);
			//AddEvent(rage_new cutfObjectVariationEventArgs( pObjectVariationEventArgs->GetObjectId(), pObjectVariationEventArgs->GetComponent(), pObjectVariationEventArgs->GetDrawable(), pObjectVariationEventArgs->GetTexture()));
		}
	}

	// Bring in the cutfObjectVariationEventArgs events from the tune file and bosh them into our cut file
	// We have to rage_new a new event for some reason, if we don't and just pipe in the pointer then we get a crash in the parser :/
	// We need to find a way to fix this at some point
	const atArray<cutfEventArgs *> &eventArgsList = tuneCutfile.GetEventArgsList();
	for(int i=0; i < eventArgsList.GetCount(); ++i)
	{
		if(eventArgsList[i]->GetType() == CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE)
		{
			cutfObjectVariationEventArgs *pObjectVariationEventArgs = static_cast<cutfObjectVariationEventArgs *>( eventArgsList[i] );
			variationEventArgList.PushAndGrow(pObjectVariationEventArgs);
		}
		
		if (eventArgsList[i]->GetType() == CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE)
		{
			cutfVehicleVariationEventArgs *pVehicleVariationEventArgs = static_cast<cutfVehicleVariationEventArgs *>( eventArgsList[i] );
			VehicleVariationEventArgList.PushAndGrow(pVehicleVariationEventArgs);
		}
		
		if (eventArgsList[i]->GetType() == CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE)
		{
			cutfVehicleExtraEventArgs *pVehicleExtraEventArgs = static_cast<cutfVehicleExtraEventArgs *>( eventArgsList[i] );
			VehicleExtraEventArgList.PushAndGrow(pVehicleExtraEventArgs);
		}
	}

	for(int i=0; i < variationEventArgList.GetCount(); ++i)
	{
		cutfObjectVariationEventArgs* var = rage_new cutfObjectVariationEventArgs( variationEventArgList[i]->GetObjectId(), variationEventArgList[i]->GetComponent(), 
			variationEventArgList[i]->GetDrawable(), variationEventArgList[i]->GetTexture());
		cutfObjectIdEvent* var2 = rage_new cutfObjectIdEvent( variationEventList[i]->GetObjectId(), variationEventList[i]->GetTime(), variationEventList[i]->GetEventId(), var );

		Exporter_AddEventAndArgs( var2 );
	}

	for(int i=0; i < VehicleVariationEventArgList.GetCount(); ++i)
	{
		cutfVehicleVariationEventArgs* var = rage_new cutfVehicleVariationEventArgs(VehicleVariationEventArgList[i]->GetObjectId(), 
			VehicleVariationEventArgList[i]->GetBodyColour(), VehicleVariationEventArgList[i]->GetSecondaryBodyColour(), 
			VehicleVariationEventArgList[i]->GetSpecularBodyColour(), VehicleVariationEventArgList[i]->GetWheelTrimColour(), VehicleVariationEventArgList[i]->GetBodyColour5(), VehicleVariationEventArgList[i]->GetBodyColour6(), 
			VehicleVariationEventArgList[i]->GetLiveryId(), VehicleVariationEventArgList[i]->GetLivery2Id(), VehicleVariationEventArgList[i]->GetDirtLevel() );
		cutfObjectIdEvent* var2 = rage_new cutfObjectIdEvent( variationEventList[i]->GetObjectId(), variationEventList[i]->GetTime(), variationEventList[i]->GetEventId(), var );
		
		Exporter_AddEventAndArgs( var2 );
	}
	
	for(int i=0; i < VehicleExtraEventArgList.GetCount(); ++i)
	{
		cutfVehicleExtraEventArgs* var = rage_new cutfVehicleExtraEventArgs( VehicleExtraEventArgList[i]->GetObjectId(), VehicleExtraEventArgList[i]->GetBoneIdList());
		cutfObjectIdEvent* var2 = rage_new cutfObjectIdEvent( variationEventList[i]->GetObjectId(), variationEventList[i]->GetTime(), variationEventList[i]->GetEventId(), var );

		Exporter_AddEventAndArgs( var2 );
	}

    SortLoadEvents();
    SortEvents();

    return true;
}

bool cutfCutsceneFile2::LoadGameDataTuneFile()
{
	if (!PARAM_EnableCutSceneGameDataTuning.Get())
		return false;

	char cFullPath[RAGE_MAX_PATH];
	sprintf( cFullPath, "%s/%s", g_CutsceneGameDataDirectory, GetSceneName() );

	if ( ASSET.Exists( cFullPath, PI_CUTSCENE_XMLFILE_EXT ) )
	{
		return LoadGameDataTuneFile( cFullPath );
	}

	return false; 
}


bool cutfCutsceneFile2::LoadGameDataTuneFile( const char *pFilename )
{
	if (!PARAM_EnableCutSceneGameDataTuning.Get())
		return false;

	// determine the load type
	const char *pExtension = PI_CUTSCENE_XMLFILE_EXT;

	char cLoadFilename[RAGE_MAX_PATH];
	ASSET.FullReadPath( cLoadFilename, RAGE_MAX_PATH, pFilename, pExtension );
	const char* pFileExtension = ASSET.FindExtensionInPath( cLoadFilename );  
	if ( pFileExtension == NULL )
	{
		pExtension = pFileExtension = PI_CUTSCENE_XMLFILE_EXT;
	}

#if __DEV
	cutfDisplayf( "About to load subtitle file '%s'.", cLoadFilename );
#endif

	// Based on the extension (and later, a version number), determine which Parse function to use
	bool bResult = false;
	if ( (strstr( pFileExtension, "xml" ) != NULL) || (strstr( pFileExtension, "bin" ) != NULL) 
		|| (strstr( pFileExtension, "sub" ) != NULL) || (strstr( pFileExtension, "cutsub" ) != NULL))
	{
		parTree *pTree = PARSER.LoadTree( pFilename, pExtension ? pExtension : "" );
		if ( pTree )
		{
			bResult = ParseGameDataTuneXml( pTree );
			delete pTree;
		}
#if __DEV
		else
		{
			cutfDisplayf( "Could not load tree." );
		}
#endif
	}
	else
	{
#if __DEV
		cutfDisplayf( "Could not open file stream." );
#endif
	}

#if __DEV
	cutfDisplayf( "Load %s.", bResult ? "success" : "failed" );
#endif

	return bResult;
}


bool cutfCutsceneFile2::ParseGameDataTuneXml( parTree *pTree )
{
	cutfCutsceneFile2 gameDataCutFile;
	if ( !gameDataCutFile.ParseXml( pTree ) )
	{
		return false;
	}

	//if(m_FileGameDataTuningFlags.IsFlagSet(GAMEDATA_TUNE_FIRST_PERSON_BLENDOUT_EVENTS))
	{
		// remove the cut file's subtitle items
		atArray<cutfObject *> CutFileCameraObjects;
		atArray<cutfEvent *> CameraEventList; 

		 FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, CutFileCameraObjects );
		 //get the cameras event list
		for(int i=0; i< CutFileCameraObjects.GetCount() ; i++)
		{
			FindEventsForObjectIdOnly( CutFileCameraObjects[i]->GetObjectId(), GetEventList(), CameraEventList );
		}
		
		//remove the current blend out events
		for(int i=0; i< CameraEventList.GetCount() ; i++)
		{
			if(CameraEventList[i]->GetEventId() == CUTSCENE_FIRST_PERSON_BLENDOUT_CAMERA_EVENT || CameraEventList[i]->GetEventId() == CUTSCENE_FIRST_PERSON_CATCHUP_CAMERA_EVENT)
			{
				RemoveEvent(CameraEventList[i]); 
			}
		}

		atArray<cutfObject *> gameDateCutFileCameraObjects;
		atArray<cutfEvent *> gameDataCameraEventList; 

		//get the first person events from the game data file
		gameDataCutFile.FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, gameDateCutFileCameraObjects );

		for(int i=0; i< gameDateCutFileCameraObjects.GetCount() ; i++)
		{
			gameDataCutFile.FindEventsForObjectIdOnly( gameDateCutFileCameraObjects[i]->GetObjectId(), gameDataCutFile.GetEventList(), gameDataCameraEventList );
		}

		//remove the current blend out events
		for(int i=0; i< gameDataCameraEventList.GetCount() ; i++)
		{
			if(gameDataCameraEventList[i]->GetEventId() == CUTSCENE_FIRST_PERSON_BLENDOUT_CAMERA_EVENT || gameDataCameraEventList[i]->GetEventId() == CUTSCENE_FIRST_PERSON_CATCHUP_CAMERA_EVENT)
			{
				for(int j=0; j< CutFileCameraObjects.GetCount() ; j++)
				{
					cutfObjectIdEvent* pObjectEvent = static_cast<cutfObjectIdEvent *>( gameDataCameraEventList[i]->Clone() );
					pObjectEvent->SetObjectId(CutFileCameraObjects[j]->GetObjectId());
					
					//cutfObjectIdEventArgs* pEventArgs = dynamic_cast<cutfObjectIdEventArgs*>(pObjectEvent->GetEventArgs()->Clone());
					const cutfEventArgs* pArgs = gameDataCameraEventList[i]->GetEventArgs();

					if(pArgs)
					{
						cutfEventArgs* pNewArgs = pArgs->Clone(); 
						pObjectEvent->SetEventArgs(pNewArgs);
					}

					AddEvent( pObjectEvent );
				}
			}
		}
	}

	SortEvents();

	return true;
}

bool cutfCutsceneFile2::LoadSubtitleFile()
{
	if (!PARAM_EnableSubtitleTuning.Get())
		return false;

    char cFullPath[RAGE_MAX_PATH];
    sprintf( cFullPath, "%s/%s", g_CutsceneSubtitleDirectory, GetSceneName() );

    if ( ASSET.Exists( cFullPath, PI_CUTSCENE_SUBTITLEFILE_EXT ) )
    {
        return LoadSubtitleFile( cFullPath );
    }

    return true;
}

bool cutfCutsceneFile2::LoadSubtitleFile( const char *pFilename )
{
	if (!PARAM_EnableSubtitleTuning.Get())
		return false;

    // determine the load type
    const char *pExtension = PI_CUTSCENE_SUBTITLEFILE_EXT;

    char cLoadFilename[RAGE_MAX_PATH];
    ASSET.FullReadPath( cLoadFilename, RAGE_MAX_PATH, pFilename, pExtension );
    const char* pFileExtension = ASSET.FindExtensionInPath( cLoadFilename );  
    if ( pFileExtension == NULL )
    {
        pExtension = pFileExtension = PI_CUTSCENE_SUBTITLEFILE_EXT;
    }

#if __DEV
    cutfDisplayf( "About to load subtitle file '%s'.", cLoadFilename );
#endif

    // Based on the extension (and later, a version number), determine which Parse function to use
    bool bResult = false;
    if ( (strstr( pFileExtension, "xml" ) != NULL) || (strstr( pFileExtension, "bin" ) != NULL) 
        || (strstr( pFileExtension, "sub" ) != NULL) || (strstr( pFileExtension, "cutsub" ) != NULL))
    {
        parTree *pTree = PARSER.LoadTree( pFilename, pExtension ? pExtension : "" );
        if ( pTree )
        {
            bResult = ParseSubtitleXml( pTree );
            delete pTree;
        }
#if __DEV
        else
        {
            cutfDisplayf( "Could not load tree." );
        }
#endif
    }
    else
    {
#if __DEV
        cutfDisplayf( "Could not open file stream." );
#endif
    }

#if __DEV
    cutfDisplayf( "Load %s.", bResult ? "success" : "failed" );
#endif

    return bResult;
}

bool cutfCutsceneFile2::ParseSubtitleXml( parTree *pTree )
{
    cutfCutsceneFile2 subtitleCutfile;
    if ( !subtitleCutfile.ParseXml( pTree ) )
    {
        return false;
    }

    // remove the cut file's subtitle items
    atArray<cutfObject *> subtitleObjectList;
    FindObjectsOfType( CUTSCENE_SUBTITLE_OBJECT_TYPE, subtitleObjectList );
    for ( int i = 0; i < subtitleObjectList.GetCount(); ++i )
    {
        RemoveObject( subtitleObjectList[i] );
    }

    for ( int i = 0; i < m_pCutsceneLoadEventList.GetCount(); ++i )
    {
        if ( m_pCutsceneLoadEventList[i]->GetEventId() == CUTSCENE_LOAD_SUBTITLES_EVENT )
        {
            RemoveLoadEvent( m_pCutsceneLoadEventList[i] );
            break;
        }
    }

    subtitleObjectList.Reset();
    subtitleCutfile.FindObjectsOfType( CUTSCENE_SUBTITLE_OBJECT_TYPE, subtitleObjectList );
	for ( int i = 0; i < subtitleObjectList.GetCount(); ++i )
    {
        cutfObject *pSubtitleObject = subtitleObjectList[i]->Clone();
        AddObject( pSubtitleObject );

        atArray<cutfEvent *> subtitleEventList;

        subtitleCutfile.FindEventsForObjectIdOnly( subtitleObjectList[i]->GetObjectId(), subtitleCutfile.GetEventList(), subtitleEventList );
        for ( int j = 0; j < subtitleEventList.GetCount(); ++j )
        {
            cutfObjectIdEvent *pSubtitleEvent = static_cast<cutfObjectIdEvent *>( subtitleEventList[j]->Clone() );
            ChangeObjectId( pSubtitleEvent->GetObjectId(), pSubtitleObject->GetObjectId(), pSubtitleEvent );

            pSubtitleEvent->SetTime( pSubtitleEvent->GetTime());

            AddEvent( pSubtitleEvent );
        }
    }

    atArray<cutfObject *> assetMgrObjectList;
    FindObjectsOfType( CUTSCENE_ASSET_MANAGER_OBJECT_TYPE, assetMgrObjectList );

    // this should never happen, but just in case
    s32 iAassetMgrObjectId = -1;
    if ( assetMgrObjectList.GetCount() == 0 )
    {
        cutfAssetManagerObject *pAssetMgrObject = rage_new cutfAssetManagerObject();
        AddObject( pAssetMgrObject );
        
        assetMgrObjectList.Grow() = pAssetMgrObject;        
    }
    
    iAassetMgrObjectId = assetMgrObjectList[0]->GetObjectId();

    const atArray<cutfEvent *> &subtitleLoadEventList = subtitleCutfile.GetLoadEventList();
    for ( int i = 0; i < subtitleLoadEventList.GetCount(); ++i )
    {
        if ( subtitleLoadEventList[i]->GetEventId() == CUTSCENE_LOAD_SUBTITLES_EVENT )
        {
            cutfObjectIdEvent *pLoadSubtitlesEvent = static_cast<cutfObjectIdEvent *>( subtitleLoadEventList[i]->Clone() );
			ChangeObjectId( pLoadSubtitlesEvent->GetObjectId(), iAassetMgrObjectId, pLoadSubtitlesEvent );
            AddLoadEvent( pLoadSubtitlesEvent );
        }
    }

    SortLoadEvents();
    SortEvents();

    return true;
}

#if __BANK || __TOOL
bool cutfCutsceneFile2::LoadAndReplaceLightsInCutfile(const char* sceneName)
{
	if (!CutfileNonParseData::m_FileTuningFlags.IsFlagSet(CutfileNonParseData::CUTSCENE_LIGHT_TUNING_FILE))
		return false;

	atString atSceneName(sceneName); 
	atString cFullPath = GetAssetPathForScene(atSceneName); 

	cutfDisplayf( "LIGHT EDITING: Tuning enabled, attempting to replace the lights in the cutfile with the lights from %s", cFullPath.c_str() );

	if ( ASSET.Exists( cFullPath, PI_CUTSCENE_LIGHTFILE_EXT ) )
	{
		//Dont replace the the light data in the cutfile, just load the light data directly
		if(LoadAndParseLightData( cFullPath.c_str(), false ))
		{
			CutfileNonParseData::m_FileTuningStatusFlags.SetFlag(CutfileNonParseData::CUTSCENE_LIGHT_TUNING_FILE_LOADED);
			return true; 
		}
	}
	else
	{
		cutfDisplayf( "LIGHT EDITING: Failed to load file from :%s", cFullPath.c_str() );
	}
	
	return false;
}
#endif

bool cutfCutsceneFile2::LoadMaxLightFile(const char* sceneName)
{
	//formatf(cFullPath, RAGE_MAX_PATH, "%s/%s/%s", g_SceneLightFileDirectory, sceneName,"data");
	atString atSceneName(sceneName); 
	atString cFullPath = GetAssetPathForScene(atSceneName); 

	if ( ASSET.Exists( cFullPath, PI_CUTSCENE_LIGHTFILE_EXT ) )
	{
		//Dont replace the the light data in the cutfile, just load the light data directly
		return LoadAndParseLightData( cFullPath, true );
	}

	return false;
}

#if __BANK || __TOOL
bool cutfCutsceneFile2::LoadAndParseLightData( const char *pFilename, bool PreserveData)
{
	if (CutfileNonParseData::m_FileTuningFlags.IsFlagSet(CutfileNonParseData::CUTSCENE_LIGHT_TUNING_FILE) && !PARAM_enablecutscenelightauthoring.Get())
		return false;

	// determine the load type
	const char *pExtension = PI_CUTSCENE_LIGHTFILE_EXT;

	char cLoadFilename[RAGE_MAX_PATH];
	ASSET.FullReadPath( cLoadFilename, RAGE_MAX_PATH, pFilename, pExtension );
	const char* pFileExtension = ASSET.FindExtensionInPath( cLoadFilename );  
	if ( pFileExtension == NULL )
	{
		pExtension = pFileExtension = PI_CUTSCENE_LIGHTFILE_EXT;
	}

#if __DEV
	cutfDisplayf( "LIGHT EDITING: About to load light file '%s'.", cLoadFilename );
#endif

	// Based on the extension (and later, a version number), determine which Parse function to use
	bool bResult = false;
	if ( (strstr( pFileExtension, "xml" ) != NULL) || (strstr( pFileExtension, "bin" ) != NULL) 
		|| (strstr( pFileExtension, "sub" ) != NULL) || (strstr( pFileExtension, "light" ) != NULL ))
	{
		parTree *pTree = PARSER.LoadTree( pFilename, pExtension ? pExtension : "" );
		if ( pTree )
		{
			//this function removes all the current light objects and replaces them with the datalight.xml
			if(!PreserveData)
			{
				bResult = ParseLightXml( pTree );
			}
			else
			{
				bResult = ParseXml(pTree); 
			}
			delete pTree;
		}
#if __DEV
		else
		{
			cutfDisplayf( "LIGHT EDITING: Could not load tree from %s", cLoadFilename );
		}
#endif
	}
	else
	{
#if __DEV
		cutfDisplayf( "LIGHT EDITING: Could not open file stream: %s",  cLoadFilename);
#endif
	}

#if __DEV
	cutfDisplayf( "LIGHT EDITING: Load %s: %s", bResult ? "success" : "failed", cLoadFilename );
#endif

	return bResult;
}
#endif //__BANK

atString cutfCutsceneFile2::GetAssetPathForScene(const atString& sceneName, bool includeFileName )
{
	const char* facedir = GetFaceDirectory(); 

	if(facedir)
	{
		atString filePath(facedir); 

		if(stristr(filePath.c_str(), "dlc") != NULL)
		{
			atString filePath(facedir); 
			filePath.Lowercase(); 
			atArray<atString> stringResults; 

			if(stristr(filePath, "/cache/") != NULL)
			{
				filePath.Split(stringResults, "cache/"); 

				atString RootFile(stringResults[0]); 

#ifdef RS_BRANCHSUFFIX
				atVarString Path("%sassets_ng/cuts/%s/data", RootFile.c_str(),sceneName.c_str());
				atString AssetPath(Path); 
				return AssetPath; 
#else
				atVarString Path( "%sassets/cuts/%s/data", RootFile.c_str(), sceneName.c_str());
				atString AssetPath(Path); 
				return AssetPath; 
#endif
			}
			else
			{
				//atString TempSceneName(sceneName); 
				//TempSceneName.Lowercase(); 

				if(stristr(filePath.c_str(), "assets_ng") != NULL)
				{
					filePath.Split(stringResults, "assets_ng"); 
				}
				else
				{
					filePath.Split(stringResults, "assets"); 
				}
				

				atString RootFile(stringResults[0]); 

#ifdef RS_BRANCHSUFFIX
				if(includeFileName)
				{
					atVarString Path("%sassets_ng/cuts/%s/data", RootFile.c_str(),sceneName.c_str());
					atString AssetPath(Path); 
					return AssetPath; 
				}
				else
				{
					atVarString Path("%sassets_ng/cuts/%s", RootFile.c_str(),sceneName.c_str());
					atString AssetPath(Path); 
					return AssetPath; 
				}
#else
				if(includeFileName)
				{
					atVarString Path("%sassets/cuts/%s/data", RootFile.c_str(),sceneName.c_str());
					atString AssetPath(Path); 
					return AssetPath; 
				}
				else
				{
					atVarString Path("%sassets/cuts/%s", RootFile.c_str(),sceneName.c_str());
					atString AssetPath(Path); 
					return AssetPath; 
				}
#endif
			}
		}
	}

	if(includeFileName)
	{
		atVarString Path("%s/%s/data", g_SceneLightFileDirectory, sceneName.c_str());
		atString AssetPath(Path); 
		return AssetPath; 
	}
	else
	{
		atVarString Path("%s/", g_SceneLightFileDirectory);
		atString AssetPath(Path); 
		return AssetPath; 
	}
}



bool cutfCutsceneFile2::ParseLightXml( parTree *pTree )
{
	cutfCutsceneFile2 lightCutfile;
	if ( !lightCutfile.ParseXml( pTree ) )
	{
		return false;
	}

	// remove the cut file's light items
	atArray<cutfObject *> lightObjectList;
	FindObjectsOfType( CUTSCENE_LIGHT_OBJECT_TYPE, lightObjectList );
	FindObjectsOfType( CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE, lightObjectList );
	for ( int i = 0; i < lightObjectList.GetCount(); ++i )
	{
		RemoveObject( lightObjectList[i] );
	}

	lightObjectList.Reset();
	lightCutfile.FindObjectsOfType( CUTSCENE_LIGHT_OBJECT_TYPE, lightObjectList );
	lightCutfile.FindObjectsOfType( CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE, lightObjectList );
	for ( int i = 0; i < lightObjectList.GetCount(); ++i )
	{
		cutfObject *pLightObject = lightObjectList[i]->Clone();

		AddObject( pLightObject );

		atArray<cutfEvent*> eventList;
		lightCutfile.FindEventsForObjectIdOnly(lightObjectList[i]->GetObjectId(),lightCutfile.GetEventList(), eventList, true );
		lightCutfile.FindEventsForObjectIdOnly(lightObjectList[i]->GetObjectId(),lightCutfile.GetEventList(), eventList, false );

		for(int j=0; j < eventList.GetCount(); ++j)
		{
			cutfObjectIdEvent* pEvent = dynamic_cast<cutfObjectIdEvent*>(eventList[j]->Clone());
			if(pEvent)
			{
				if(pEvent->GetEventId() == CUTSCENE_SET_ANIM_EVENT || pEvent->GetEventId() == CUTSCENE_CLEAR_ANIM_EVENT)
				{
					cutfObjectIdEventArgs* pEventArgs = dynamic_cast<cutfObjectIdEventArgs*>(pEvent->GetEventArgs()->Clone());
					if(pEventArgs)
					{
						pEventArgs->SetObjectId(pLightObject->GetObjectId());
					}

					pEvent->SetObjectId(1); // Animation Manager
					pEvent->SetEventArgs(pEventArgs);
				}
				else if(pEvent->GetEventId() == CUTSCENE_SET_LIGHT_EVENT || pEvent->GetEventId() == CUTSCENE_CLEAR_LIGHT_EVENT)
				{
					pEvent->SetObjectId(pLightObject->GetObjectId());
				}

				AddEvent( pEvent );
			}
		}
	}

	atArray<cutfObject *> assetMgrObjectList;
	FindObjectsOfType( CUTSCENE_ASSET_MANAGER_OBJECT_TYPE, assetMgrObjectList );

	// this should never happen, but just in case
	if ( assetMgrObjectList.GetCount() == 0 )
	{
		cutfAssetManagerObject *pAssetMgrObject = rage_new cutfAssetManagerObject();
		AddObject( pAssetMgrObject );     
	}

	SortLoadEvents();
	SortEvents();

	return true;
}

#endif // __BANK || __TOOL

void cutfCutsceneFile2::AddObject( cutfObject *pObject )
{
    int iIndex = m_pCutsceneObjects.Find( pObject );
    if ( iIndex == -1 )
    {
        pObject->SetObjectId( m_pCutsceneObjects.GetCount() );
        m_pCutsceneObjects.Grow() = pObject;
    }
}

void cutfCutsceneFile2::RemoveObject( const cutfObject *pObject )
{
	int iIndex = 0;
	for ( ; iIndex < m_pCutsceneObjects.GetCount(); iIndex ++ )
	{
		if ( m_pCutsceneObjects[iIndex] == pObject )
		{
			break;
		}
	}
    if ( iIndex == m_pCutsceneObjects.GetCount() )
    {
        return;
    }

    // Remove the object from the event and event args lists
    RemoveObjectFromEventList( pObject->GetObjectId(), m_pCutsceneLoadEventList );
    RemoveObjectFromEventList( pObject->GetObjectId(), m_pCutsceneEventList );

    // Delete the object
    m_pCutsceneObjects.Delete( iIndex );
    delete pObject;

    // make sure all ids are sequential
    for ( int i = iIndex; i < m_pCutsceneObjects.GetCount(); ++i )
    {
        ChangeObjectId( m_pCutsceneObjects[i], i );
    }
}

void cutfCutsceneFile2::Exporter_AddEventAndArgs( cutfEvent* pEvent )
{
	bool bFoundEvent=false;
	for(int iEvent = 0; iEvent < m_pCutsceneEventList.GetCount(); ++iEvent)
	{
		if(*m_pCutsceneEventList[iEvent] == *pEvent)
		{
			bFoundEvent=true;
			break;
		}
	}

	if(!bFoundEvent)
	{
		m_pCutsceneEventList.Grow() = pEvent;

		if(pEvent->GetEventArgs())
		{
			bool bFoundEventArgs=false;
			for(int iEventArg=0; iEventArg < m_pCutsceneEventArgsList.GetCount(); ++iEventArg)
			{
				if(*m_pCutsceneEventArgsList[iEventArg] == *pEvent->GetEventArgs())
				{
					bFoundEventArgs=true;
				}
			}

			if(!bFoundEventArgs)
			{
				m_pCutsceneEventArgsList.Grow() = (cutfEventArgs*)pEvent->GetEventArgs();
			}
		}
	}
}

void cutfCutsceneFile2::AddEventArgs( cutfEventArgs *pEventArgs )
{
    if ( m_pCutsceneEventArgsList.Find( pEventArgs ) == -1 )
    {
        m_pCutsceneEventArgsList.Grow() = pEventArgs;
    }
}

void cutfCutsceneFile2::RemoveEventArgs( cutfEventArgs *pEventArgs )
{
    if ( pEventArgs->GetRef() == 0 )
    {
        int iIndex = m_pCutsceneEventArgsList.Find( pEventArgs );
        if ( iIndex != -1 )
        {
            delete m_pCutsceneEventArgsList[iIndex];
            m_pCutsceneEventArgsList.Delete( iIndex );
        }
    }
}

void cutfCutsceneFile2::GetCurrentSectionTimeList( atArray<float> &sectionTimeList ) const
{
    sectionTimeList.Reset();    

    if ( GetCutsceneFlags().IsSet( CUTSCENE_IS_SECTIONED_FLAG ) )
    {
        sectionTimeList.PushAndGrow( 0.0f );

        float fStartTime = ((float)GetRangeStart()) / CUTSCENE_FPS;
        float fEndTime = (((float)GetRangeEnd()) / CUTSCENE_FPS) - fStartTime;

        if ( GetCutsceneFlags().IsSet( CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG ) )
        {
            for ( int i = 0; i < m_cameraCutList.GetCount(); ++i )
            {
                // Zero-based Timeline: adjust for start time when anims have been sectioned
                float fCutTime = m_cameraCutList[i] - fStartTime;
                if ( (fCutTime > 0.0f) && (fCutTime < fEndTime) )
                {
                    sectionTimeList.PushAndGrow( fCutTime );
                }
            }
        }
        else if ( GetCutsceneFlags().IsSet( CUTSCENE_SECTION_BY_SPLIT_FLAG ) )
        {
            for ( int i = 0; i < m_sectionSplitList.GetCount(); ++i )
            {
                // Zero-based Timeline: adjust for start time when anims have been sectioned
                float fSplitTime = m_sectionSplitList[i] - fStartTime;
                if ( (fSplitTime > 0.0f) && (fSplitTime < fEndTime) )
                {
                    sectionTimeList.PushAndGrow( fSplitTime );
                }
            }
        }
        else if ( GetCutsceneFlags().IsSet( CUTSCENE_SECTION_BY_DURATION_FLAG ) )
        {
			//Changed the method to frames to avoid division by float and point error           
			float fTime = GetSectionByTimeSliceDuration();
            s32 CurrentFrame = (s32) GetSectionByTimeSliceDuration() * (s32) CUTSCENE_FPS; 
			s32 TargetFrame =  GetRangeEnd() - GetRangeStart(); 

			while (CurrentFrame < TargetFrame )
			{
				sectionTimeList.PushAndGrow( fTime );
				CurrentFrame += ((s32)GetSectionByTimeSliceDuration())* ((s32) CUTSCENE_FPS);
				fTime += GetSectionByTimeSliceDuration();
			}

        }
    }
}

void cutfCutsceneFile2::GetCurrentConcatDataList( atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT> &concatDataList ) const
{
    concatDataList.Reset();

    if ( m_concatDataList.GetCount() == 0 )
    {
        return;
    }

    float fOffset = 0.0f;
    if ( GetCutsceneFlags().IsSet( CUTSCENE_IS_SECTIONED_FLAG ) )
    {
        fOffset = GetRangeStart() / CUTSCENE_FPS;
    }
	//LOOK AT THIS AND MAKE SURE ITS CORRECT
		for ( int i = 0; i < m_concatDataList.GetCount(); ++i )
		{
			// Zero-based Timeline: adjust for start time when anims have been sectioned
			if ( i == 0 )
			{
				// preserve the name, but update the offset and rotation to actual
				concatDataList.Append() = SConcatData( m_concatDataList[i].cSceneName, 0.0f, GetOffset(), GetRotation(), GetPitch(), GetRoll(), m_concatDataList[i].iRangeStart, m_concatDataList[i].iRangeEnd  );
			}
			else
			{
				concatDataList.Append() = SConcatData( m_concatDataList[i].cSceneName, m_concatDataList[i].fStartTime - fOffset, 
					m_concatDataList[i].vOffset, m_concatDataList[i].fRotation, m_concatDataList[i].fPitch ,m_concatDataList[i].fRoll, m_concatDataList[i].iRangeStart, m_concatDataList[i].iRangeEnd );
			}
		}
}

void cutfCutsceneFile2::FindObjectsOfType( s32 iObjectType, atArray<cutfObject*> &pObjectList ) const
{
    for ( int i = 0; i < m_pCutsceneObjects.GetCount(); ++i )
    {        
        if ( m_pCutsceneObjects[i]->GetType() == iObjectType )
        {
            pObjectList.Grow() = m_pCutsceneObjects[i];
        }
    }
}

void cutfCutsceneFile2::FindObjectsOfType( s32 iObjectType, atArray<const cutfObject*> &pObjectList ) const
{
	for ( int i = 0; i < m_pCutsceneObjects.GetCount(); ++i )
	{        
		if ( m_pCutsceneObjects[i]->GetType() == iObjectType )
		{
			pObjectList.Grow() = m_pCutsceneObjects[i];
		}
	}
}


void cutfCutsceneFile2::FindEventsForObjectIdOnly( s32 iObjectId, const atArray<cutfEvent *>& pEventList, 
                                             atArray<cutfEvent *>& pObjectEventList, bool bSearchEventArgs ) const
{
    for ( int i = 0; i < pEventList.GetCount(); ++i )
    {        
        if ( pEventList[i]->GetType() == CUTSCENE_OBJECT_ID_EVENT_TYPE )
        {
            const cutfObjectIdEvent* pObjectIdEvent = static_cast<const cutfObjectIdEvent*>( pEventList[i] );
			if(!bSearchEventArgs)
			{
				if ( pObjectIdEvent->GetObjectId() == iObjectId )
				{
					pObjectEventList.Grow() = const_cast<cutfObjectIdEvent*>( pObjectIdEvent );
				}
			}
			else
			{
				const cutfEventArgs* eventArgs = pEventList[i]->GetEventArgs();
				if ( eventArgs )
				{
					s32 eventType = eventArgs->GetType();
					if(	eventType == CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE ||
						eventType == CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE ||
						eventType == CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE ||
						eventType == CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE ||
						eventType == CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE ||
						eventType == CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE)
					{
						const cutfObjectIdEventArgs* pObjectIdEventArgs = static_cast<const cutfObjectIdEventArgs*>( eventArgs );

						if ( pObjectIdEventArgs->GetObjectId() == iObjectId )
						{
							pObjectEventList.Grow() = const_cast<cutfObjectIdEvent*>( pObjectIdEvent );
						}
					}
				}
			}
        }
    }
}

void cutfCutsceneFile2::FindObjectIdEventsForObjectIdOnly( s32 iObjectId, const atArray<cutfEvent *>& pEventList, 
												  atArray<cutfEvent *>& pObjectEventList, bool bSearchForSpecifArgs, s32 eventArgsType ) const
{
	for ( int i = 0; i < pEventList.GetCount(); ++i )
	{        
		if ( pEventList[i]->GetType() == CUTSCENE_OBJECT_ID_EVENT_TYPE )
		{
			const cutfObjectIdEvent* pObjectIdEvent = static_cast<const cutfObjectIdEvent*>( pEventList[i] );
			if(!bSearchForSpecifArgs)
			{
				if ( pObjectIdEvent->GetObjectId() == iObjectId )
				{
					pObjectEventList.Grow() = const_cast<cutfObjectIdEvent*>( pObjectIdEvent );
				}
			}
			else
			{
				const cutfEventArgs* eventArgs = pEventList[i]->GetEventArgs();
				if ( eventArgs )
				{
					s32 eventType = eventArgs->GetType();
					if(	eventType == eventArgsType)
					{
						const cutfObjectIdEvent* pObjectIdEvent = static_cast<const cutfObjectIdEvent*>( pEventList[i] );

						if ( pObjectIdEvent->GetObjectId() == iObjectId )
						{
							pObjectEventList.Grow() = const_cast<cutfObjectIdEvent*>( pObjectIdEvent );
						}
					}
				}
			}
		}
	}
}

void cutfCutsceneFile2::FindEventsForObjectId( s32 iObjectId, const atArray<cutfEvent *>& pEventList, 
                                              atArray<cutfEvent *>& pObjectEventList ) const
{
    for ( int i = 0; i < pEventList.GetCount(); ++i )
    {
        if ( pEventList[i]->GetType() == CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE )
        {
            const cutfObjectIdListEvent *pObjectIdListEvent = static_cast<const cutfObjectIdListEvent*>( pEventList[i] );
            const atArray<s32> &objectIdList = pObjectIdListEvent->GetObjectIdList();

            int iIndex = objectIdList.Find( iObjectId );
            if ( iIndex != -1 )
            {
                pObjectEventList.Grow() = const_cast<cutfObjectIdListEvent *>( pObjectIdListEvent );
            }
        }
    }
}

void cutfCutsceneFile2::SortObjects()
{
	m_pCutsceneObjects.QSort( 0, -1, &ObjectsComparer);
}

void cutfCutsceneFile2::SortLoadEvents()
{
	// Use stable sort and not a qsort, if events are equal then qsort is not stable and cannot guarentee these events stay the same
	std::stable_sort(m_pCutsceneLoadEventList.begin(),m_pCutsceneLoadEventList.end(), &LoadEventsComparer );
}

void cutfCutsceneFile2::SortEvents()
{
    std::stable_sort(m_pCutsceneEventList.begin(),m_pCutsceneEventList.end(), &EventsComparer );
}

void cutfCutsceneFile2::GetUnusedEventArgs( atArray<cutfEventArgs *> &eventArgsList ) const
{
    for ( int i = 0; i < m_pCutsceneEventArgsList.GetCount(); ++i )
    {
        if ( m_pCutsceneEventArgsList[i]->GetRef() == 0 )
        {
            eventArgsList.Grow() = m_pCutsceneEventArgsList[i];
        }
    }
}

void cutfCutsceneFile2::CleanupUnusedEventArgs()
{
    atArray<cutfEventArgs *> unusedEventArgsList;
    GetUnusedEventArgs( unusedEventArgsList );

    for ( int i = 0; i < unusedEventArgsList.GetCount(); ++i )
    {
        RemoveEventArgs( unusedEventArgsList[i] );
    }
}

cutfCutsceneFile2* cutfCutsceneFile2::Clone() const
{
    cutfCutsceneFile2 *pCutfile = rage_new cutfCutsceneFile2();
    
    pCutfile->SetFaceDirectory( GetFaceDirectory() );
    pCutfile->SetFadeAtBeginningColor( GetFadeAtBeginningColor() );
    pCutfile->SetFadeAtEndColor( GetFadeAtEndColor() );
    pCutfile->SetFadeInCutsceneAtBeginningDuration( GetFadeInCutsceneAtBeginningDuration() );
    pCutfile->SetFadeInGameAtEndDuration( GetFadeInGameAtEndDuration() );
    pCutfile->SetFadeOutCutsceneAtEndDuration( GetFadeOutCutsceneAtEndDuration() );
    pCutfile->SetFadeOutGameAtBeginningDuration( GetFadeOutGameAtBeginningDuration() );
	pCutfile->SetBlendOutCutsceneDuration( GetBlendOutCutsceneDuration() );
	pCutfile->SetBlendOutCutsceneOffset( GetBlendOutCutsceneOffset() );
    pCutfile->SetOffset( GetOffset() );
    pCutfile->SetRangeEnd( GetRangeEnd() );
    pCutfile->SetRangeStart( GetRangeStart() );
    pCutfile->SetRotation( GetRotation() );
    pCutfile->SetSectionByTimeSliceDuration( GetSectionByTimeSliceDuration() );
    pCutfile->SetSceneName( GetSceneName() );
    pCutfile->SetTotalDuration( GetTotalDuration() );
	pCutfile->SetTriggerOffset( GetTriggerOffset() );
	pCutfile->SetDayCoCHours( GetDayCoCHours() );
    
    // scene attributes
    parAttributeList &attributes = const_cast<parAttributeList &>( pCutfile->GetAttributeList() );
    attributes.CopyFrom( m_attributes );
    
    // cutscene flags
    sysMemCpy( pCutfile->m_iCutsceneFlags, m_iCutsceneFlags, 4 * sizeof(u32) );

    // camera cuts
    atArray<float> &cameraCuts = const_cast<atArray<float> &>( pCutfile->GetCameraCutList() );
    cameraCuts.Reserve( m_cameraCutList.GetCount() );
    for ( int i = 0; i < m_cameraCutList.GetCount(); ++i )
    {
        cameraCuts.Append() = m_cameraCutList[i];
    }
    
    // section splits
    atArray<float> &sectionSplits = const_cast<atArray<float> &>( pCutfile->GetSectionSplitList() );
    sectionSplits.Reserve( m_sectionSplitList.GetCount() );
    for ( int i = 0; i < m_sectionSplitList.GetCount(); ++i )
    {
        sectionSplits.Append() = m_sectionSplitList[i];
    }

    // concat data list
    atFixedArray<SConcatData, CUTSCENE_MAX_CONCAT> &concatDataList 
        = const_cast<atFixedArray<SConcatData, CUTSCENE_MAX_CONCAT> &>( pCutfile->GetConcatDataList() );
    for ( int i = 0; i < m_concatDataList.GetCount(); ++i )
    {
        concatDataList.Append() = m_concatDataList[i];
    }

	// discard frame data list
	atArray<SDiscardedFrameData> &discardedFrameList 
		= const_cast<atArray<SDiscardedFrameData> &>( pCutfile->GetDiscardedFrameList() );
	discardedFrameList.Reserve( m_discardFrameList.GetCount() );
	for ( int i = 0; i < m_discardFrameList.GetCount(); ++i )
	{
		discardedFrameList.Append() = m_discardFrameList[i];
	}

    // event args
    pCutfile->m_pCutsceneEventArgsList.Reserve( m_pCutsceneEventArgsList.GetCount() );
    for ( int i = 0; i < m_pCutsceneEventArgsList.GetCount(); ++i )
    {
        pCutfile->m_pCutsceneEventArgsList.Append() = m_pCutsceneEventArgsList[i]->Clone();
    }

    // events   
    pCutfile->m_pCutsceneEventList.Reserve( m_pCutsceneEventList.GetCount() );
    for ( int i = 0; i < m_pCutsceneEventList.GetCount(); ++i )
    {
        pCutfile->m_pCutsceneEventList.Append() = m_pCutsceneEventList[i]->Clone();
    }

    // load events
    pCutfile->m_pCutsceneLoadEventList.Reserve( m_pCutsceneLoadEventList.GetCount() );
    for ( int i = 0; i < m_pCutsceneLoadEventList.GetCount(); ++i )
    {
        pCutfile->m_pCutsceneLoadEventList.Append() = m_pCutsceneLoadEventList[i]->Clone();
    }

    // objects
    pCutfile->m_pCutsceneObjects.Reserve( m_pCutsceneObjects.GetCount() );
    for ( int i = 0; i < m_pCutsceneObjects.GetCount(); ++i )
    {
        pCutfile->m_pCutsceneObjects.Append() = m_pCutsceneObjects[i]->Clone();
    }

    FixupClonedEventArgs( pCutfile->m_pCutsceneLoadEventList, pCutfile->GetEventArgsList() );
    FixupClonedEventArgs( pCutfile->m_pCutsceneEventList, pCutfile->GetEventArgsList() );

    return pCutfile;
}

const atString cutfCutsceneFile2::GetRealSceneName() const
{
	atString strFaceDir(GetFaceDirectory());

	atArray<atString> splitArray;
	strFaceDir.Split(splitArray, "\\");

	if(splitArray.GetCount() > 2)
	{
		return atString(splitArray[splitArray.GetCount()-2].c_str());
	}

	return atString(GetSceneName());
}
#if !__FINAL
void cutfCutsceneFile2::Concat( const cutfCutsceneFile2 &other, float previousDuration, bool overwrite )
{
#if __TOOL
	ULOGGER.SetProgressMessage("Appending '%s'", other.GetRealSceneName());
#endif

    int originalRangeEnd = GetRangeEnd();
    float originalTotalDuration = GetTotalDuration();
    int originalObjectCount = GetObjectList().GetCount();

    // Merge the scene attributes
    parAttributeList &mergedAttList = const_cast<parAttributeList &>( GetAttributeList() );
    mergedAttList.MergeFrom( other.GetAttributeList(), overwrite );

    // append the camera cuts
    const atArray<float> &cameraCuts = other.GetCameraCutList();
    m_cameraCutList.Grow() = originalTotalDuration; // put a cut at the concat point
    for ( int i = 0; i < cameraCuts.GetCount(); ++i )
    {
        // Check, just in case the concatenated cutscene has a camera cut at 0
        float fCameraCutTime = cameraCuts[i] + originalTotalDuration;
        if ( fCameraCutTime > m_cameraCutList[m_cameraCutList.GetCount() - 1] )
        {
            m_cameraCutList.Grow() = cameraCuts[i] + originalTotalDuration;
        }
    }

    // merge the flags.  this takes precedence over cutFile, except for the fades at the end of the scene
    atBitSet &mergedFlags = const_cast<atBitSet &>( GetCutsceneFlags() );
    mergedFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_IN_GAME_FLAG, other.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_IN_GAME_FLAG ) );
    mergedFlags.Set( cutfCutsceneFile2::CUTSCENE_FADE_OUT_FLAG, other.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_FADE_OUT_FLAG ) );
	mergedFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_BLENDOUT_CAMERA_FLAG, other.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_BLENDOUT_CAMERA_FLAG ) );
	mergedFlags.Set( cutfCutsceneFile2::CUTSCENE_USE_CATCHUP_CAMERA_FLAG, other.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_USE_CATCHUP_CAMERA_FLAG ) );

    // merge the section split list
    const atArray<float> &otherSectionSplits = other.GetSectionSplitList();
    m_sectionSplitList.Grow() = originalTotalDuration;  // put a section split at the concat point
    for ( int i = 0; i < otherSectionSplits.GetCount(); ++i )
    {
        float fSectionSplitTime = otherSectionSplits[i] + originalTotalDuration;
        if ( fSectionSplitTime > m_sectionSplitList[m_sectionSplitList.GetCount() - 1] )
        {
            m_sectionSplitList.Grow() = otherSectionSplits[i] + originalTotalDuration;
        }
    }    

    // merge other properties
    SetFadeAtEndColor( other.GetFadeAtEndColor() );
    SetFadeInGameAtEndDuration( other.GetFadeInGameAtEndDuration() );
    SetFadeOutCutsceneAtEndDuration( other.GetFadeOutCutsceneAtEndDuration() );
    SetRangeEnd( (other.GetCutsceneFlags().IsSet( cutfCutsceneFile2::CUTSCENE_IS_SECTIONED_FLAG ) 
        ? other.GetRangeEnd() - other.GetRangeStart() : other.GetRangeEnd()) + originalRangeEnd );
    SetTotalDuration( GetTotalDuration() + other.GetTotalDuration() );

	SetBlendOutCutsceneDuration(other.GetBlendOutCutsceneDuration());
	SetBlendOutCutsceneOffset(other.GetBlendOutCutsceneOffset());

    // On first concat
    if ( m_concatDataList.GetCount() == 0 )
    {
        atArray<cutfObject *> cameraObjectList;
        FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );

        if ( cameraObjectList.GetCount() > 0 )
        {
            atArray<cutfEvent *> cameraEventList;
            FindEventsForObjectIdOnly( cameraObjectList[0]->GetObjectId(), GetEventList(), cameraEventList );

            for ( int i = 0; i < cameraEventList.GetCount(); ++i )
            {
                // Change the name of the camera cuts
                if ( (cameraEventList[i]->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT) 
                    && (cameraEventList[i]->GetEventArgs() != NULL) 
                    && (cameraEventList[i]->GetEventArgs()->GetType() == CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE) )
                {
                    cutfCameraCutEventArgs *pCameraCutEventArgs 
                        = static_cast<cutfCameraCutEventArgs *>( const_cast<cutfEventArgs *>( cameraEventList[i]->GetEventArgs() ) );

                    char cCameraCutName[128];
                    sprintf( cCameraCutName, "%s - %s", GetSceneName(), pCameraCutEventArgs->GetName().GetCStr() );

                    pCameraCutEventArgs->SetName( cCameraCutName );
                }
            }
        }
    }    
    
    // clone the objects from cutFile that are not in this.  also, look for additional attributes on like objects.
    atMap<int, int> otherToThisObjectIdMap;
    CloneObjects( other, otherToThisObjectIdMap, overwrite );

    // Look for missing items in the other cutfile
    const atArray<cutfObject *> &objectList = GetObjectList();    
    const atArray<cutfObject *> &otherObjectList = other.GetObjectList();
    for ( int i = 0; i < originalObjectCount; ++i )
    {
        bool found = false;
        const cutfObject *pObject = objectList[i];

        for ( int j = 0; j < otherObjectList.GetCount(); ++j )
        {
            cutfObject *pOtherObject = otherObjectList[j];

            if ( ((pOtherObject->GetType() == CUTSCENE_ASSET_MANAGER_OBJECT_TYPE) && (pObject->GetType() == CUTSCENE_ASSET_MANAGER_OBJECT_TYPE))
                || ((pOtherObject->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE) && (pObject->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE)))
            {
				// Match the 'singletons'
				found = true;
            }
			else if((pOtherObject->GetType() == CUTSCENE_AUDIO_OBJECT_TYPE) && (pObject->GetType() == CUTSCENE_AUDIO_OBJECT_TYPE))
			{
				if ( GetCutsceneFlags().IsSet( CUTSCENE_USE_ONE_AUDIO_FLAG ))
				{
					// Match the 'singletons'
					found = true;
				}
			}
            else if ( (pOtherObject->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE) && (pObject->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE) )
            {
                // Another 'singleton' but the name must match
                if ( strcmpi( pOtherObject->GetDisplayName().c_str(), pObject->GetDisplayName().c_str() ) == 0 )
                {
                    // Matched by name and type
                    found = true;
                }
            }
            else if ( strcmpi( pOtherObject->GetDisplayName().c_str(), pObject->GetDisplayName().c_str() ) == 0 )
            {
                // The names match, and the type must also match
                if ( pOtherObject->GetType() == pObject->GetType() )
                {
                    switch ( pOtherObject->GetType() )
                    {
                    case CUTSCENE_MODEL_OBJECT_TYPE:
                        {
                            const cutfModelObject *pModelObject = static_cast<const cutfModelObject *>( pObject );
                            const cutfModelObject *pOtherModelObject = static_cast<const cutfModelObject *>( pOtherObject );
                            if ( pModelObject->GetModelType() == pOtherModelObject->GetModelType() )
                            {
                                // Matched by name, type and model type
                                found = true;
                            }
                        }
                        break;
                    case CUTSCENE_LIGHT_OBJECT_TYPE:
                        {
                            const cutfLightObject *pLightObject = static_cast<const cutfLightObject *>( pObject );
                            const cutfLightObject *pOtherLightObject = static_cast<const cutfLightObject *>( pOtherObject );
                            if ( pLightObject->GetLightType() == pOtherLightObject->GetLightType() )
                            {
                                // Matched by name, type and light type
                                found = true;
                            }
                        }
                        break;
                    default:
                        {
                            // Matched by name and type
                            found = true;
                        }
                        break;
                    }
                }
            }

            if ( found )
            {
                break;
            }
        }
    }

    // clone the events
    bool bNeedToAddAudioData;
    CloneEvents( other, otherToThisObjectIdMap, originalObjectCount, originalTotalDuration, bNeedToAddAudioData, previousDuration, overwrite );

  //  atArray<cutfObject *> audioObjectList;
  //  FindObjectsOfType( CUTSCENE_AUDIO_OBJECT_TYPE, audioObjectList );
  //  if ( audioObjectList.GetCount() > 0 )
  //  {
		//if ( GetCutsceneFlags().IsSet( CUTSCENE_USE_ONE_AUDIO_FLAG ))
		//{
		//	// If we're the audio object
		//	atArray<cutfEvent *> objectEventList;
		//	FindEventsForObjectIdOnly( audioObjectList[audioObjectList.GetCount()-1]->GetObjectId(), GetEventList(), objectEventList );

		//	for ( int i = objectEventList.GetCount() - 1; i >= 0; --i )
		//	{
		//		if ( (objectEventList[i]->GetEventId() == CUTSCENE_STOP_AUDIO_EVENT))
		//		{
		//			// move the Stop Audio event to the end
		//			objectEventList[i]->SetTime( GetTotalDuration() );
		//			break;
		//		}
		//	}
		//}
  //  }

    // Sort
    SortLoadEvents();
    SortEvents();
	
	atHashString SceneName = other.GetRealSceneName().c_str(); 

    SConcatData concatData( SceneName, originalTotalDuration, other.GetOffset(), other.GetRotation(), other.GetPitch(), other.GetRoll(),
		other.GetRangeStart(), other.GetRangeEnd() );
    m_concatDataList.Append() = concatData;

	if(other.GetDiscardedFrameList().GetCount())
	{
		SDiscardedFrameData discardedFrameData( other.GetDiscardedFrameList()[0].cSceneName, other.GetDiscardedFrameList()[0].frames);
		m_discardFrameList.PushAndGrow(discardedFrameData);
	}
}


void cutfCutsceneFile2::MergeFrom( const cutfCutsceneFile2 &other, bool overwrite )
{
    cutfDisplayf( "Merging '%s'", other.GetSceneName() );

    // Merge the scene attributes
    parAttributeList &mergedAttList = const_cast<parAttributeList &>( GetAttributeList() );
    mergedAttList.MergeFrom( other.GetAttributeList(), overwrite );

    int originalObjectCount = GetObjectList().GetCount();

    // clone the objects from cutFile that are not in this.  also, look for additional attributes on like objects.
    atMap<int, int> otherToThisObjectIdMap;
    CloneObjects( other, otherToThisObjectIdMap, overwrite );

    // clone the events
    bool bNeedToAddAudioData;
    CloneEvents( other, otherToThisObjectIdMap, originalObjectCount, 0.0f, bNeedToAddAudioData, overwrite );

    // Sort
    SortLoadEvents();
    SortEvents();
}
#endif

int cutfCutsceneFile2::GetEventArgsIndex( cutfEventArgs *pEventArgs )
{
	cutfAssertf( sm_pCurrentCutfile, "sm_pCurrentCutfile needs to be set before calling PARSER functions on the cutfCutsceneFile2 class." );
	return sm_pCurrentCutfile->m_pCutsceneEventArgsList.Find( pEventArgs );
}

cutfEventArgs* cutfCutsceneFile2::FindEventArgs( int iEventArgsIdx )
{
	cutfAssertf( sm_pCurrentCutfile, "sm_pCurrentCutfile needs to be set before calling PARSER functions on the cutfCutsceneFile2 class." );

	if ( (iEventArgsIdx >= 0) && (iEventArgsIdx < sm_pCurrentCutfile->m_pCutsceneEventArgsList.GetCount()) )
	{
		sm_pCurrentCutfile->m_pCutsceneEventArgsList[iEventArgsIdx]->AddRef();
		return const_cast<cutfEventArgs *>( sm_pCurrentCutfile->m_pCutsceneEventArgsList[iEventArgsIdx] );
	}

	return NULL;
}

bool cutfCutsceneFile2::UpgradeDrawDistances()
{
    atArray<cutfObject *> cameraObjectList;
    FindObjectsOfType( CUTSCENE_CAMERA_OBJECT_TYPE, cameraObjectList );
    if ( cameraObjectList.GetCount() > 0 )
    {
        atArray<cutfEvent *> cameraEventList;
        FindEventsForObjectIdOnly( cameraObjectList[0]->GetObjectId(), GetEventList(), cameraEventList );

        atArray<s32> drawDistanceEventIndexList;
        atArray<s32> cameraCutEventIndexList;

        for ( int i = 0; i < cameraEventList.GetCount(); ++i )
        {
            if ( cameraEventList[i]->GetEventId() == CUTSCENE_SET_DRAW_DISTANCE_EVENT )
            {
                drawDistanceEventIndexList.PushAndGrow( i );
            }
            else if ( cameraEventList[i]->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT )
            {
                cameraCutEventIndexList.PushAndGrow( i );
            }
        }

        // Transfer near and far draw distances to the Camera Cut events
        for ( int i = 0; (i < drawDistanceEventIndexList.GetCount()) && (i < cameraCutEventIndexList.GetCount()); ++i )
        {
            cutfCameraCutEventArgs *pCameraCutEventArgs;

            cutfEventArgs *pEventArgs = const_cast<cutfEventArgs *>( cameraEventList[cameraCutEventIndexList[i]]->GetEventArgs() );
            if ( pEventArgs->GetType() == CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE )
            {
                pCameraCutEventArgs = static_cast<cutfCameraCutEventArgs *>( pEventArgs );
            }
            else
            {
                cameraEventList[cameraCutEventIndexList[i]]->SetEventArgs( NULL );
                RemoveEventArgs( pEventArgs );

                pCameraCutEventArgs = rage_new cutfCameraCutEventArgs();

                cameraEventList[cameraCutEventIndexList[i]]->SetEventArgs( pCameraCutEventArgs );
                AddEventArgs( pCameraCutEventArgs );
            }

            const cutfTwoFloatValuesEventArgs *pDrawDistanceEventArgs = static_cast<const cutfTwoFloatValuesEventArgs *>( 
                cameraEventList[drawDistanceEventIndexList[i]]->GetEventArgs() );

            pCameraCutEventArgs->SetNearDrawDistance( pDrawDistanceEventArgs->GetFloat1() );
            pCameraCutEventArgs->SetFarDrawDistance( pDrawDistanceEventArgs->GetFloat2() );
        }

        // Remove all of the Set Draw Distance events
        for ( int i = 0; i < drawDistanceEventIndexList.GetCount(); ++i )
        {
            RemoveEvent( cameraEventList[drawDistanceEventIndexList[i]] );
        }
    }

    return true;
}

void cutfCutsceneFile2::FixupClonedEventArgs( atArray<cutfEvent *>& clonedEventList, 
                                             const atArray<cutfEventArgs *> &clonedEventArgsList ) const
{
    sm_pCurrentCutfile = this;

    for ( int i = 0; i < clonedEventList.GetCount(); ++i )
    {
        cutfEvent *pEvent = clonedEventList[i];
        if ( pEvent->GetEventArgs() != NULL )
        {
            // get the index of the cloned event args (they're in our list)
            int iIndex = m_pCutsceneEventArgsList.Find( const_cast<cutfEventArgs *>( pEvent->GetEventArgs() ) );
            cutfAssertf( iIndex != -1, "Unable to find the event args of type '%s'.", cutfEventArgs::GetTypeName( pEvent->GetEventArgs()->GetType() ) );

            // set the event args to the cloned instance (they're in the same order)
            pEvent->SetEventArgs( clonedEventArgsList[iIndex] );
        }
        else if ( pEvent->GetType() == CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE )
        {
            cutfObjectIdListEvent *pObjectIdListEvent = static_cast<cutfObjectIdListEvent *>( pEvent );

            atArray<cutfEventArgs *> &eventArgsList = const_cast<atArray<cutfEventArgs *> &>( pObjectIdListEvent->GetEventArgsList() );            
            for ( int j = 0; j < eventArgsList.GetCount(); ++j )
            {
                // get the index of the cloned event args (they're in our list)
                int iIndex = m_pCutsceneEventArgsList.Find( eventArgsList[j] );
                cutfAssertf( iIndex != -1, "Unable to find the event args of type '%s'.", cutfEventArgs::GetTypeName( eventArgsList[j]->GetType() ) );

                // set the event args to the cloned instance (they're in the same order)
                eventArgsList[j] = clonedEventArgsList[iIndex];
            }
        }
    }

    sm_pCurrentCutfile = NULL;
}

int cutfCutsceneFile2::ObjectsComparer( cutfObject* const* ppObject1, cutfObject* const* ppObject2 )
{
	return (*ppObject1)->Compare( *ppObject2 );
}

int cutfCutsceneFile2::EventsComparer( cutfEvent* ppEvent1, cutfEvent* ppEvent2 )
{
    return (ppEvent1)->Compare( ppEvent2 );
}

int cutfCutsceneFile2::LoadEventsComparer( cutfEvent* ppEvent1, cutfEvent* ppEvent2 )
{
	return (ppEvent1)->LoadCompare( ppEvent2 );
}

void cutfCutsceneFile2::AddEventToList( cutfEvent *pEvent, atArray<cutfEvent *> &eventList )
{
    eventList.Grow() = pEvent;

    if ( pEvent->GetEventArgs() )
    {
        AddEventArgs( const_cast<cutfEventArgs *>( pEvent->GetEventArgs() ) );
    }
    else if ( pEvent->GetType() == CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE )
    {
        cutfObjectIdListEvent *pObjectIdListEvent = static_cast<cutfObjectIdListEvent *>( pEvent );

        const atArray<cutfEventArgs *> &objectIdListEventArgs = pObjectIdListEvent->GetEventArgsList();
        for ( int i = 0; i < objectIdListEventArgs.GetCount(); ++i )
        {
            AddEventArgs( objectIdListEventArgs[i] );
        }
    }
}

void cutfCutsceneFile2::RemoveEventFromList( cutfEvent *pEvent, atArray<cutfEvent *> &eventList )
{
    for ( int i = 0; i < eventList.GetCount(); ++i )
    {
        if ( eventList[i] == pEvent )
        {
            atArray<cutfEventArgs *> eventArgsList;
            if ( pEvent->GetEventArgs() )
            {
                eventArgsList.Grow() = const_cast<cutfEventArgs *>( pEvent->GetEventArgs() );
            }
            else if ( pEvent->GetType() == CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE )
            {
                cutfObjectIdListEvent *pObjectIdListEvent = static_cast<cutfObjectIdListEvent *>( pEvent );

                const atArray<cutfEventArgs *> &objectIdListEventArgs = pObjectIdListEvent->GetEventArgsList();
                for ( int j = 0; j < objectIdListEventArgs.GetCount(); ++j )
                {
                    eventArgsList.Grow() = objectIdListEventArgs[j];
                }
            }

            delete eventList[i];
            eventList.Delete( i );

            for ( int j = 0; j < eventArgsList.GetCount(); ++j )
            {
                RemoveEventArgs( eventArgsList[j] );
            }

            break;
        }
    }
}

void cutfCutsceneFile2::RemoveObjectFromEventList( s32 iObjectId, atArray<cutfEvent *> &eventList )
{
    // Remove the events sent to this object
    atArray<cutfEvent *> objectEventList;
    FindEventsForObjectIdOnly( iObjectId, eventList, objectEventList );
    for ( int i = 0; i < objectEventList.GetCount(); ++i )
    {
        RemoveEventFromList( objectEventList[i], eventList );
    }

    objectEventList.Reset();
    FindEventsForObjectId( iObjectId, eventList, objectEventList );
    for ( int i = 0; i < objectEventList.GetCount(); ++i )
    {
        if ( objectEventList[i]->GetType() == CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE )
        {
            cutfObjectIdListEvent *pObjectIdListEvent = static_cast<cutfObjectIdListEvent *>( objectEventList[i] );

            cutfEventArgs *pEventArgs = pObjectIdListEvent->RemoveObjectId( iObjectId );

            if ( pObjectIdListEvent->GetObjectIdList().GetCount() == 0 )
            {
                // without any object ids, we don't need the event
                RemoveEventFromList( objectEventList[i], eventList );
            }
            else if ( pEventArgs != NULL )
            {
                RemoveEventArgs( pEventArgs );
            }
        }
    }

    // Remove this object's id from events args sent to other objects
    for ( int i = 0; i < eventList.GetCount(); )
    {
        if ( eventList[i]->GetEventArgs() != NULL )
        {
            cutfEventArgs *pEventArgs = RemoveObjectFromEventArgs( iObjectId, const_cast<cutfEventArgs *>( eventList[i]->GetEventArgs() ) );
            if ( pEventArgs != NULL )
            {
                // without event args, we don't need the event
                RemoveEventFromList( eventList[i], eventList );
            }
            else
            {
                ++i;
            }
        }
        else
        {
            ++i;
        }
    }
}

cutfEventArgs* cutfCutsceneFile2::RemoveObjectFromEventArgs( s32 iObjectId, cutfEventArgs *pEventArgs )
{
    switch ( pEventArgs->GetType() )
    {
    case CUTSCENE_EVENT_ARGS_LIST_TYPE:
        {
            cutfEventArgsList *pEventArgsList = static_cast<cutfEventArgsList *>( pEventArgs );
            
            atArray<cutfEventArgs *> &argsList = const_cast<atArray<cutfEventArgs *> &>( pEventArgsList->GetEventArgsList() );
            for ( int i = 0; i < argsList.GetCount(); )
            {
                cutfEventArgs *pArgs = RemoveObjectFromEventArgs( iObjectId, argsList[i] );
                if ( pArgs != NULL )
                {
                    argsList.Delete( i );
                    delete pArgs;
                }
                else
                {
                    ++i;
                }
            }

            if ( argsList.GetCount() == 0 )
            {
                return pEventArgs;
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE:
        {
            cutfObjectIdEventArgs *pObjectIdEventArgs = static_cast<cutfObjectIdEventArgs *>( pEventArgs );
            if ( pObjectIdEventArgs->GetObjectId() == iObjectId )
            {
                return pEventArgs;
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE:
        {
            cutfObjectIdNameEventArgs *pObjectIdNameEventArgs = static_cast<cutfObjectIdNameEventArgs *>( pEventArgs );
            if ( pObjectIdNameEventArgs->GetObjectId() == iObjectId )
            {
                return pEventArgs;
            }
        }
        break;
    case CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE:
        {
            cutfAttachmentEventArgs *pAttachmentEventArgs = static_cast<cutfAttachmentEventArgs *>( pEventArgs );
            if ( pAttachmentEventArgs->GetObjectId() == iObjectId )
            {
                return pEventArgs;
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE:
        {
            cutfObjectIdListEventArgs *pObjectIdListEventArgs = static_cast<cutfObjectIdListEventArgs *>( pEventArgs );
            atArray<s32> &objectIdlist = const_cast<atArray<s32> &>( pObjectIdListEventArgs->GetObjectIdList() );

            for ( int i = 0; i < objectIdlist.GetCount(); )
            {
                if ( objectIdlist[i] == iObjectId )
                {
                    objectIdlist.Delete( i );
                }
                else
                {
                    ++i;
                }
            }

            if ( objectIdlist.GetCount() == 0 )
            {
                return pEventArgs;
            }
        }
        break;
    }

    return NULL;
}

void cutfCutsceneFile2::EnforceSequentialObjectIds()
{
    for ( int i = 0; i < m_pCutsceneObjects.GetCount(); ++i )
    {
        if ( m_pCutsceneObjects[i]->GetObjectId() != i )
        {
            ChangeObjectId( m_pCutsceneObjects[i], i );
        }
    }
}

void cutfCutsceneFile2::ChangeObjectId( cutfObject *pObject, s32 iNewObjectId )
{
    // change the id in the events
    ChangeObjectId( pObject->GetObjectId(), iNewObjectId, m_pCutsceneLoadEventList );
    ChangeObjectId( pObject->GetObjectId(), iNewObjectId, m_pCutsceneEventList );

    // change the id
    pObject->SetObjectId( iNewObjectId );
}

void cutfCutsceneFile2::ChangeObjectId( s32 iObjectId, s32 iNewObjectId, const atArray<cutfEvent *> &eventList )
{
    // look for events that are dispatched to the original object id, or contains it somewhere in the event args
    for ( int i = 0; i < eventList.GetCount(); ++i )
    {
        ChangeObjectId( iObjectId, iNewObjectId, eventList[i] );
    }
}

void cutfCutsceneFile2::ChangeObjectId( s32 iObjectId, s32 iNewObjectId, cutfEvent *pEvent )
{
    if ( pEvent->GetEventArgs() != NULL )
    {
        ChangeObjectId( iObjectId, iNewObjectId, const_cast<cutfEventArgs *>( pEvent->GetEventArgs() ) );
    }

    switch ( pEvent->GetType() )
    {
    case CUTSCENE_OBJECT_ID_EVENT_TYPE:
        {
            cutfObjectIdEvent *pObjectIdEvent = static_cast<cutfObjectIdEvent *>( pEvent );
            if ( pObjectIdEvent->GetObjectId() == iObjectId )
            {
                pObjectIdEvent->SetObjectId( iNewObjectId );
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE:
        {
            cutfObjectIdListEvent *pObjectIdListEvent = static_cast<cutfObjectIdListEvent *>( pEvent );
            atArray<s32> &objectIdList = const_cast<atArray<s32> &>( pObjectIdListEvent->GetObjectIdList() );
            for ( int i = 0; i < objectIdList.GetCount(); ++i )
            {
                if ( objectIdList[i] == iObjectId )
                {
                    objectIdList[i] = iNewObjectId;
                }
            }

            const atArray<cutfEventArgs *> &eventArgsList = pObjectIdListEvent->GetEventArgsList();
            for ( int i = 0; i < eventArgsList.GetCount(); ++i )
            {
                ChangeObjectId( iObjectId, iNewObjectId, eventArgsList[i] );
            }
        }
        break;
    default:
        break;
    }
}

void cutfCutsceneFile2::ChangeObjectId( s32 iObjectId, s32 iNewObjectId, cutfEventArgs *pEventArgs )
{
    switch ( pEventArgs->GetType() )
    {
    case CUTSCENE_EVENT_ARGS_LIST_TYPE:
        {
            cutfEventArgsList *pEventArgsList = static_cast<cutfEventArgsList *>( pEventArgs );

            atArray<cutfEventArgs *> &argsList = const_cast<atArray<cutfEventArgs *> &>( pEventArgsList->GetEventArgsList() );
            for ( int i = 0; i < argsList.GetCount(); )
            {
                ChangeObjectId( iObjectId, iNewObjectId, argsList[i] );
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE:
	case CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE:
	case CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE:
	case CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE:
        {
            cutfObjectIdEventArgs *pObjectIdEventArgs = static_cast<cutfObjectIdEventArgs *>( pEventArgs );
            if ( pObjectIdEventArgs->GetObjectId() == iObjectId )
            {
                pObjectIdEventArgs->SetObjectId( iNewObjectId );
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE:
        {
            cutfObjectIdNameEventArgs *pObjectIdNameEventArgs = static_cast<cutfObjectIdNameEventArgs *>( pEventArgs );
            if ( pObjectIdNameEventArgs->GetObjectId() == iObjectId )
            {
                pObjectIdNameEventArgs->SetObjectId( iNewObjectId );
            }
        }
        break;
    case CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE:
        {
            cutfAttachmentEventArgs *pAttachmentEventArgs = static_cast<cutfAttachmentEventArgs *>( pEventArgs );
            if ( pAttachmentEventArgs->GetObjectId() == iObjectId )
            {
                pAttachmentEventArgs->SetObjectId( iNewObjectId );
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE:
        {
            cutfObjectIdListEventArgs *pObjectIdListEventArgs = static_cast<cutfObjectIdListEventArgs *>( pEventArgs );
            atArray<s32> &objectIdList = const_cast<atArray<s32> &>( pObjectIdListEventArgs->GetObjectIdList() );
            for ( int i = 0; i < objectIdList.GetCount(); ++i )
            {
                if ( objectIdList[i] == iObjectId )
                {
                    objectIdList[i] = iNewObjectId;
                }
            }
        }
        break;
    }
}

void cutfCutsceneFile2::ChangeObjectIds( const atMap<int, int> &oldToNewObjectId, const atArray<cutfEvent *> &eventList, bool bChangeEventArgs )
{
    // look for events that are dispatched to the original object id, or contains it somewhere in the event args
    for ( int i = 0; i < eventList.GetCount(); ++i )
    {
        ChangeObjectIds( oldToNewObjectId, eventList[i], bChangeEventArgs );
    }
}

void cutfCutsceneFile2::ChangeObjectIds( const atMap<int, int> &oldToNewObjectId, const atArray<cutfEventArgs *> &eventArgList )
{
	// look for events that are dispatched to the original object id, or contains it somewhere in the event args
	for ( int i = 0; i < eventArgList.GetCount(); ++i )
	{
		ChangeObjectIds( oldToNewObjectId, eventArgList[i] );
	}
}

void cutfCutsceneFile2::ChangeObjectIds( const atMap<int, int> &oldToNewObjectId, cutfEvent *pEvent, bool bChangeEventArgs )
{
    if ( pEvent->GetEventArgs() != NULL && bChangeEventArgs)
    {
        ChangeObjectIds( oldToNewObjectId, const_cast<cutfEventArgs *>( pEvent->GetEventArgs() ) );
    }

    switch ( pEvent->GetType() )
    {
    case CUTSCENE_OBJECT_ID_EVENT_TYPE:
        {
            cutfObjectIdEvent *pObjectIdEvent = static_cast<cutfObjectIdEvent *>( pEvent );
            const int *pNewObjectId = oldToNewObjectId.Access( pObjectIdEvent->GetObjectId() );
            if ( pNewObjectId != NULL )
            {
                pObjectIdEvent->SetObjectId( *pNewObjectId );
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE:
        {
            cutfObjectIdListEvent *pObjectIdListEvent = static_cast<cutfObjectIdListEvent *>( pEvent );
            atArray<s32> &objectIdList = const_cast<atArray<s32> &>( pObjectIdListEvent->GetObjectIdList() );
            for ( int i = 0; i < objectIdList.GetCount(); ++i )
            {
                const int *pNewObjectId = oldToNewObjectId.Access( objectIdList[i] );
                if ( pNewObjectId != NULL )
                {
                    objectIdList[i] = *pNewObjectId;
                }
            }

            const atArray<cutfEventArgs *> &eventArgsList = pObjectIdListEvent->GetEventArgsList();
            for ( int i = 0; i < eventArgsList.GetCount(); ++i )
            {
                ChangeObjectIds( oldToNewObjectId, eventArgsList[i] );
            }
        }
        break;
    default:
        break;
    }
}

void cutfCutsceneFile2::ChangeObjectIds( const atMap<int, int> &oldToNewObjectId, cutfEventArgs *pEventArgs )
{
    switch ( pEventArgs->GetType() )
    {
    case CUTSCENE_EVENT_ARGS_LIST_TYPE:
        {
            cutfEventArgsList *pEventArgsList = static_cast<cutfEventArgsList *>( pEventArgs );

            atArray<cutfEventArgs *> &argsList = const_cast<atArray<cutfEventArgs *> &>( pEventArgsList->GetEventArgsList() );
            for ( int i = 0; i < argsList.GetCount(); )
            {
                ChangeObjectIds( oldToNewObjectId, argsList[i] );
            }
        }
        break;
	case CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE:
	case CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE:
	case CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE:
	case CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE:
        {
            cutfObjectIdEventArgs *pObjectIdEventArgs = static_cast<cutfObjectIdEventArgs *>( pEventArgs );
            const int *pNewObjectId = oldToNewObjectId.Access( pObjectIdEventArgs->GetObjectId() );
            if ( pNewObjectId != NULL )
            {
                pObjectIdEventArgs->SetObjectId( *pNewObjectId );
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE:
        {
            cutfObjectIdNameEventArgs *pObjectIdNameEventArgs = static_cast<cutfObjectIdNameEventArgs *>( pEventArgs );
            const int *pNewObjectId = oldToNewObjectId.Access( pObjectIdNameEventArgs->GetObjectId() );
            if ( pNewObjectId != NULL )
            {
				pObjectIdNameEventArgs->SetObjectId( *pNewObjectId );
            }
        }
        break;
    case CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE:
        {
            cutfAttachmentEventArgs *pAttachmentEventArgs = static_cast<cutfAttachmentEventArgs *>( pEventArgs );
            const int *pNewObjectId = oldToNewObjectId.Access( pAttachmentEventArgs->GetObjectId() );
            if ( pNewObjectId != NULL )
            {
                pAttachmentEventArgs->SetObjectId( *pNewObjectId );
            }
        }
        break;
    case CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE:
        {
            cutfObjectIdListEventArgs *pObjectIdListEventArgs = static_cast<cutfObjectIdListEventArgs *>( pEventArgs );
            atArray<s32> &objectIdList = const_cast<atArray<s32> &>( pObjectIdListEventArgs->GetObjectIdList() );
            for ( int i = 0; i < objectIdList.GetCount(); ++i )
            {
                const int *pObjectId = oldToNewObjectId.Access( objectIdList[i] );
                if ( pObjectId != NULL )
                {
                    objectIdList[i] = *pObjectId;
                }
            }
        }
        break;
    }
}

void cutfCutsceneFile2::CloneObjects( const cutfCutsceneFile2 &other, atMap<int, int> &oldToNewObjectId, bool overwrite )
{
    const atArray<cutfObject *> &objectList = GetObjectList();
    int originalObjectCount = objectList.GetCount();

    const atArray<cutfObject *> &otherObjectList = other.GetObjectList();
    for ( int i = 0; i < otherObjectList.GetCount(); ++i )
    {
        bool found = false;
        const cutfObject *pOtherObject = otherObjectList[i];

        for ( int j = 0; j < originalObjectCount; ++j )
        {
            cutfObject *pObject = objectList[j];

            if ( ((pObject->GetType() == CUTSCENE_ASSET_MANAGER_OBJECT_TYPE) && (pOtherObject->GetType() == CUTSCENE_ASSET_MANAGER_OBJECT_TYPE))
                || ((pObject->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE) && (pOtherObject->GetType() == CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE)))
            {
				// Match the 'singletons'
				found = true;
            }
			else if((pObject->GetType() == CUTSCENE_AUDIO_OBJECT_TYPE) && (pOtherObject->GetType() == CUTSCENE_AUDIO_OBJECT_TYPE))
			{
				if ( GetCutsceneFlags().IsSet( CUTSCENE_USE_ONE_AUDIO_FLAG ))
				{
					// Match the 'singletons'
					found = true;
				}
			}
            else if ( (pObject->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE) && (pOtherObject->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE) )
            {
                // Another 'singleton' but the name must match
                if ( strcmpi( pObject->GetDisplayName().c_str(), pOtherObject->GetDisplayName().c_str() ) == 0 )
                {
                    // Matched by name and type
                    found = true;
                }
                else
                {
#if __TOOL
					atString strSceneName(other.GetRealSceneName());
					ULOGGER.SetProgressMessage("ERROR: (%s) Found a Camera Object, but it's name does not match the one we already have:  '%s' should be '%s'.",
						strSceneName.c_str(), pOtherObject->GetDisplayName().c_str(), pObject->GetDisplayName().c_str() );
#endif
                }
            }
			else if ( (pObject->GetType() == CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE && pOtherObject->GetType() == CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE)
				|| (pObject->GetType() == CUTSCENE_FIXUP_MODEL_OBJECT_TYPE && pOtherObject->GetType() == CUTSCENE_FIXUP_MODEL_OBJECT_TYPE) )
			{
				if ( stricmp( pOtherObject->GetDisplayName().c_str(), pObject->GetDisplayName().c_str() ) == 0 )
				{
					const cutfFindModelObject *pFindModelObject = static_cast<const cutfFindModelObject *>( pObject );
					const cutfFindModelObject *pFindModelOtherObject = static_cast<const cutfFindModelObject *>( pOtherObject );

					// Compare the objects position to make sure its unique, not name. The position will be absolute.
					if(pFindModelObject->GetPosition().IsEqual(pFindModelOtherObject->GetPosition()) == true)
					{
						found = true;
					}
				}
			}
            else if ( strcmpi( pObject->GetDisplayName().c_str(), pOtherObject->GetDisplayName().c_str() ) == 0 )
            {
                // The names match, and the type must also match
                if ( pObject->GetType() == pOtherObject->GetType() )
                {
                    switch ( pObject->GetType() )
                    {
                    case CUTSCENE_MODEL_OBJECT_TYPE:
                        {
                            const cutfModelObject *pModelObject = static_cast<const cutfModelObject *>( pObject );
                            const cutfModelObject *pOtherModelObject = static_cast<const cutfModelObject *>( pOtherObject );
                            if ( pModelObject->GetModelType() == pOtherModelObject->GetModelType() )
                            {
                                // Matched by name, type and model type
                                found = true;
                            }
                            else
                            {
#if __TOOL
								atString strSceneName(other.GetRealSceneName());
								ULOGGER.SetProgressMessage("ERROR: (%s) Found two Model Objects with the same name but different types:  '%s', type '%s' should be type '%s'.",
									strSceneName.c_str(), pOtherObject->GetDisplayName().c_str(), 
									pOtherModelObject->GetTypeName(), pObject->GetTypeName() );
#endif
                            }
                        }
                        break;
                    case CUTSCENE_LIGHT_OBJECT_TYPE:
                        {
                            const cutfLightObject *pLightObject = static_cast<const cutfLightObject *>( pObject );
                            const cutfLightObject *pOtherLightObject = static_cast<const cutfLightObject *>( pOtherObject );
                            if ( pLightObject->GetLightType() == pOtherLightObject->GetLightType() )
                            {
                                // Matched by name, type and light type
                                found = true;
                            }
                            else
                            {
#if __TOOL
								atString strSceneName(other.GetRealSceneName());
								ULOGGER.SetProgressMessage("ERROR: (%s) Found two Light Objects with the same name but different types:  '%s', type '%s' should be type '%s'.",
									strSceneName.c_str(), pOtherObject->GetDisplayName().c_str(), 
									pOtherLightObject->GetLightTypeName(), pLightObject->GetLightTypeName() );
#endif
                            }
                        }
                        break;
                    default:
                        {
                            // Matched by name and type
                            found = true;
                        }
                        break;
                    }
                }
            }

            if ( found )
            {
                oldToNewObjectId.Insert( pOtherObject->GetObjectId(), pObject->GetObjectId() );
                pObject->MergeFrom( *pOtherObject, overwrite );

                break;
            }
        }

        if ( !found )
        {
            cutfObject *pOtherObjectClone = pOtherObject->Clone();

            AddObject( pOtherObjectClone );
			oldToNewObjectId.Insert( pOtherObject->GetObjectId(), pOtherObjectClone->GetObjectId() );
        }
    }
}

#if  !__FINAL
void cutfCutsceneFile2::CloneEvents( const cutfCutsceneFile2 &other, const atMap<int, int> &oldToNewObjectId, int /*originalObjectCount*/, 
                                    float /*originalTotalDuration*/, bool &bNeedToAddAudioData, float previousDuration, bool /*overwrite*/ )
{
    bNeedToAddAudioData = !GetCutsceneFlags().IsSet( CUTSCENE_USE_ONE_AUDIO_FLAG );

    const atArray<cutfEvent *>& otherObjectLoadEventList = other.GetLoadEventList();

    const atArray<cutfEvent *>& otherObjectEventList = other.GetEventList();

    // clone and/or merge the load events           
    for ( int i = 0; i < otherObjectLoadEventList.GetCount(); ++i )
    {
        const cutfEvent *pOtherEvent = otherObjectLoadEventList[i];
        switch ( pOtherEvent->GetEventId() )
        {
		case CUTSCENE_LOAD_AUDIO_EVENT:
            {      
				// If we have overloaded audio and the part is marked to keep events we point the event to the overloaded 
				// audio object. Else we just add the event with its own object.
				if ( GetCutsceneFlags().IsSet( CUTSCENE_USE_ONE_AUDIO_FLAG ))
				{
					if ( other.GetCutsceneFlags().IsSet( CUTSCENE_USE_AUDIO_EVENTS_CONCAT_FLAG ))
					{
						cutfObjectIdEvent *pClonedEvent = dynamic_cast<cutfObjectIdEvent*>(pOtherEvent->Clone());
						pClonedEvent->SetTime(pClonedEvent->GetTime() + previousDuration);
						
						atArray<cutfObject*> audioObjectList;
						FindObjectsOfType(CUTSCENE_AUDIO_OBJECT_TYPE, audioObjectList );

						pClonedEvent->SetObjectId(audioObjectList[0]->GetObjectId());

						AddLoadEvent(pClonedEvent); 
					}
					  
				}
				else
				{
					cutfEvent *pClonedEvent = pOtherEvent->Clone();
					pClonedEvent->SetTime(pClonedEvent->GetTime() + previousDuration);
					ChangeObjectIds( oldToNewObjectId, pClonedEvent );
					AddLoadEvent(pClonedEvent);  
				}
            }
            break;
        case CUTSCENE_LOAD_MODELS_EVENT:
		case CUTSCENE_UNLOAD_MODELS_EVENT:
        case CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT:
        case CUTSCENE_LOAD_OVERLAYS_EVENT:
        case CUTSCENE_HIDE_OBJECTS_EVENT:
        case CUTSCENE_FIXUP_OBJECTS_EVENT:
        case CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT:
        case CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT:
		case CUTSCENE_LOAD_RAYFIRE_EVENT:
		case CUTSCENE_UNLOAD_RAYFIRE_EVENT:
            {
                cutfEvent *pClonedEvent = pOtherEvent->Clone();
                pClonedEvent->SetEventArgs( pClonedEvent->GetEventArgs()->Clone() );
                ChangeObjectIds( oldToNewObjectId, const_cast<cutfEventArgs *>( pClonedEvent->GetEventArgs() ) );
				
				pClonedEvent->SetTime(pClonedEvent->GetTime() + previousDuration);
				AddLoadEvent(pClonedEvent);
            }
            break;
        }
    }

	atMap<int, int>::ConstIterator iter = oldToNewObjectId.CreateIterator();
	for ( iter.Start(); !iter.AtEnd(); iter.Next() )
	{
		atArray<cutfEvent *> otherObjectLoadEventListCompare;
		other.FindEventsForObjectIdOnly( iter.GetKey(), other.GetEventList(), otherObjectLoadEventListCompare, false );

		// Hack this specifically for SET/CLEAR on animated lights, we dont want this to effect any other type 
		if(m_pCutsceneObjects[*oldToNewObjectId.Access(iter.GetKey())]->GetType() == CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE)
			other.FindEventsForObjectIdOnly( iter.GetKey(), other.GetEventList(), otherObjectLoadEventListCompare, true );

		bool bIsCamera = other.GetObjectList()[iter.GetKey()]->GetType() == CUTSCENE_CAMERA_OBJECT_TYPE;
		for ( int i = 0; i < otherObjectLoadEventListCompare.GetCount(); ++i )
		{
		    // Change the name of the camera cuts
		    if ( bIsCamera && (otherObjectEventList[i]->GetEventId() == CUTSCENE_CAMERA_CUT_EVENT) 
		        && (otherObjectEventList[i]->GetEventArgs() != NULL) 
		        && (otherObjectEventList[i]->GetEventArgs()->GetType() == CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE) )
		    {
		        cutfCameraCutEventArgs *pCameraCutEventArgs 
		            = static_cast<cutfCameraCutEventArgs *>( const_cast<cutfEventArgs *>( otherObjectEventList[i]->GetEventArgs() ) );

		        char cCameraCutName[128];
		        sprintf( cCameraCutName, "%s - %s", other.GetSceneName(), pCameraCutEventArgs->GetName().GetCStr() );

		        pCameraCutEventArgs->SetName( cCameraCutName );
		    }

			cutfEvent* pEvent = otherObjectLoadEventListCompare[i]->Clone();

			if(pEvent->GetEventId() == CUTSCENE_PLAY_AUDIO_EVENT ||
				pEvent->GetEventId() == CUTSCENE_STOP_AUDIO_EVENT)
			{
				if ( GetCutsceneFlags().IsSet( CUTSCENE_USE_ONE_AUDIO_FLAG ) )
				{
					if(!other.GetCutsceneFlags().IsSet( CUTSCENE_USE_AUDIO_EVENTS_CONCAT_FLAG ))
					{
						// Skip the events if we have overloaded audio and the part has not requested to keep them
						continue;
					}
					else
					{
						cutfObjectIdEvent *pEvent2 = dynamic_cast<cutfObjectIdEvent*>(pEvent);

						atArray<cutfObject*> audioObjectList;
						FindObjectsOfType(CUTSCENE_AUDIO_OBJECT_TYPE, audioObjectList );

						pEvent2->SetObjectId(audioObjectList[0]->GetObjectId());	
					}
				}
				else
				{
					pEvent->SetEventArgs( pEvent->GetEventArgs()->Clone() );
					ChangeObjectIds( oldToNewObjectId, pEvent );
				}
			}
			else
			{
				if(pEvent->GetEventId() == CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT)
				{
					cutfEventArgs* pEventArgs = pEvent->GetEventArgs()->Clone();
					cutfPlayParticleEffectEventArgs* pPlayArgs = dynamic_cast<cutfPlayParticleEffectEventArgs*>(pEventArgs);
					if(pPlayArgs)
					{
						const int *pNewObjectId = oldToNewObjectId.Access( pPlayArgs->GetAttachParentId() );
						if ( pNewObjectId != NULL )
						{
							pPlayArgs->SetAttachParentId(*pNewObjectId);
						}
					}

					pEvent->SetEventArgs( pEventArgs );
					ChangeObjectIds( oldToNewObjectId, pEvent );
				}
				else
				{
					if(pEvent->GetEventArgs() != NULL)
					{
						pEvent->SetEventArgs( pEvent->GetEventArgs()->Clone() );
					}

					ChangeObjectIds( oldToNewObjectId, pEvent );
				}
			}

			pEvent->SetTime( pEvent->GetTime() + previousDuration );
			AddEvent( pEvent );
		}
	}
}
#endif  //!__FINAL

void cutfCutsceneFile2::CloneEventList( atArray<cutfEvent *> &eventList, const atMap<int, int> &oldToNewObjectId, float originalTotalDuration )
{
    atMap<const cutfEventArgs *, cutfEventArgs *> oldToNewEventArgs;
    for ( int i = 0; i < eventList.GetCount(); ++i )
    {
        cutfEvent *pClonedEvent = eventList[i]->Clone();

        // Fix the time
        pClonedEvent->SetTime( pClonedEvent->GetTime() + originalTotalDuration );

        // Clone the event args, if any
        if ( eventList[i]->GetEventArgs() != NULL )
        {
            cutfEventArgs **ppClonedEventArgs = oldToNewEventArgs.Access( eventList[i]->GetEventArgs() );
            if ( ppClonedEventArgs != NULL )
            {
                // Clear the old event args so we can fix the ObjectIds (the event args have already been fixed)
                pClonedEvent->SetEventArgs( NULL );

                ChangeObjectIds( oldToNewObjectId, pClonedEvent );

                pClonedEvent->SetEventArgs( *ppClonedEventArgs );
            }
            else
            {
                cutfEventArgs *pClonedEventArgs = eventList[i]->GetEventArgs()->Clone();     
                pClonedEvent->SetEventArgs( pClonedEventArgs );

                // Fix the recipient's ObjectId and all of the event args
                ChangeObjectIds( oldToNewObjectId, pClonedEvent );
                
                oldToNewEventArgs.Insert( eventList[i]->GetEventArgs(), pClonedEventArgs );
            }
        }
        else
        {
            // Fix the recipient's ObjectId
            ChangeObjectIds( oldToNewObjectId, pClonedEvent );
        }

        // replace the event with the cloned event
        eventList[i] = pClonedEvent;
    }
}

void cutfCutsceneFile2::PreSave()
{
    cutfAssertf( sm_pCurrentCutfile == NULL, "We didn't clean up our last PARSER operation on the cutfCutsceneFile2 properly." );
    sm_pCurrentCutfile = this;
}

void cutfCutsceneFile2::PostSave( parTreeNode* /*pTreeNode*/ )
{
    cutfAssertf( sm_pCurrentCutfile != NULL, "We shouldn't be calling this function right now." );
    sm_pCurrentCutfile = NULL;
}

void cutfCutsceneFile2::PreLoad( parTreeNode* /*pTreeNode*/ )
{
    cutfAssertf( sm_pCurrentCutfile == NULL, "We didn't clean up our last PARSER operation on the cutfCutsceneFile2 properly." );
    sm_pCurrentCutfile = this;
}

void cutfCutsceneFile2::PostLoad()
{
    cutfAssertf( sm_pCurrentCutfile != NULL, "We shouldn't be calling this function right now." );
    sm_pCurrentCutfile = NULL;
}

//##############################################################################

} // namespace rage
