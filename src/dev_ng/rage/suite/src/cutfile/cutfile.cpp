// 
// cutfile/cutfile.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutfile.h"
#include "cutfile_parser.h"
#include "cutscene/cutsoptimisations.h"

#include "cutfchannel.h"
#include "file/asset.h"
#include "parser/manager.h"

CUTS_OPTIMISATIONS()

using namespace rage;

//##############################################################################
SEffectsDetails::SEffectsDetails() : iType( 0 ), SectionCreated( 0 ), iAttachedTo( 0 ), iStartTime( 0 ), iEndTime( 0 ), iBoneTag( 0 ), iEffectId( 0 )
{
	for (int i = 0; i < MAX_CUTSCENE_SECTIONS; ++i) { bActiveInThisSection[i] = false; }
	vPos.Zero();
	vDir.Zero();
	cOriginalEffectName[0] = cEffectName[0] = cEffectAnim[0] = 0;
	for (int i = 0; i < MAX_EVOLUTION_EFFECT_VALUES; ++i) { cEvoName[i][0] = 0; fEvoValue[i] = 0.0f; }
}

cutfCutsceneSection::cutfCutsceneSection()
: m_fRotation( 0.0f )
, m_fDuration( 0.0f )
, m_iModelCount( 0 )
, m_iVehicleCount( 0 )
, m_iVehicleRemovalCount( 0 )
, m_iAttachmentCount( 0 )
, m_iHiddenObjectCount( 0 )
, m_iLightCount( 0 )
, m_iManualSectionToUse( 0 )
, m_iManualSectionToUseForAudio( 0 )
, m_bManualSection( false )
, m_bUseRange( false )
, m_fRangeStart( 0.0f )
, m_fRangeEnd( 0.0f )
, m_iStaticSize( 0 )
, m_iFrameOffset( 0 )
{
    m_vOffset.Zero();

    m_cAudioTrackName[0] = 0;
    m_cCameraAnimName[0] = 0;
    m_cAnimDictName[0] = 0;

    for ( int i = 0; i < MAX_CUTSCENE_ENTITIES; ++i )
    {
        m_iModelSize[i] = 0;
    }
}

void cutfCutsceneSection::AddModel( const SModelDetails &model )
{
    if ( cutfVerifyf( m_iModelCount < MAX_CUTSCENE_ENTITIES, "Max number of entities per section (%d) hit!", MAX_CUTSCENE_ENTITIES ) )
    {
        m_models[m_iModelCount] = model;
        ++m_iModelCount;
    }
    else
    {
        m_models[m_iModelCount - 1] = model;
    }
}

void cutfCutsceneSection::AddVehicle( const SVehicleDetails &vehicle )
{
    if ( cutfVerifyf( m_iVehicleCount < MAX_CUTSCENE_VEHICLES, "Max number of vehicle details per section (%d) hit!", MAX_CUTSCENE_VEHICLES ) )
    {
        m_vehicles[m_iVehicleCount] = vehicle;
        ++m_iVehicleCount;
    }
    else
    {
        m_vehicles[m_iVehicleCount - 1] = vehicle;
    }
}

void cutfCutsceneSection::AddVehicleRemoval( const SVehicleRemoval &vehicleRemoval )
{
    if ( cutfVerifyf( m_iVehicleRemovalCount < MAX_CUTSCENE_VEHICLE_REMOVALS, 
        "Max number of vehicle removals per section (%d) hit!", MAX_CUTSCENE_VEHICLE_REMOVALS ) )
    {
        m_vehicleRemovals[m_iVehicleRemovalCount] = vehicleRemoval;
        ++m_iVehicleRemovalCount;
    }
    else
    {
        m_vehicleRemovals[m_iVehicleRemovalCount - 1] = vehicleRemoval;
    }
}

void cutfCutsceneSection::AddAttachment( const SAttachmentDetails &attachment )
{
    if ( cutfVerifyf( m_iAttachmentCount < MAX_CUTSCENE_ATTACHMENTS, "Max number of attachments per section (%d) hit!", MAX_CUTSCENE_ATTACHMENTS ) )
    {
        m_attachments[m_iAttachmentCount] = attachment;
        ++m_iAttachmentCount;
    }
    else
    {
        m_attachments[m_iAttachmentCount - 1] = attachment;
    }
}

void cutfCutsceneSection::AddHiddenObject( const SHiddenObjectDetails &hiddenObject )
{
    if ( cutfVerifyf( m_iHiddenObjectCount < MAX_HIDDEN_OBJS, "Max number of hidden objects per section (%d) hit!", MAX_HIDDEN_OBJS ) )
    {
        m_hiddenObjects[m_iHiddenObjectCount] = hiddenObject;
        ++m_iHiddenObjectCount;
    }
    else
    {
        m_hiddenObjects[m_iHiddenObjectCount - 1] = hiddenObject;
    }
}

void cutfCutsceneSection::AddLight( const SLightDetails &light )
{
    if ( cutfVerifyf( m_iLightCount < MAX_CUTSCENE_LIGHTS, "Max number of hidden objects per section (%d) hit!", MAX_CUTSCENE_LIGHTS ) )
    {
        m_lights[m_iLightCount] = light;
        ++m_iLightCount;
    }
    else
    {
        m_lights[m_iLightCount - 1] = light;
    }
}

//#############################################################################

cutfCutsceneFile::cutfCutsceneFile()
: m_bIsLoaded( false )
, m_iNameHash( 0 )
{
    m_cName[0] = 0;    

    Clear();
}

cutfCutsceneFile::~cutfCutsceneFile()
{
    Clear();
}

void cutfCutsceneFile::Clear()
{
    m_cMissionTextName[0] = 0;

    m_vPlayerStartingPos.Zero();
    m_iPlayerId = -1;

    m_iCutsceneFlags = 0;  // default not to fade in/out each section

    // particles work BETWEEN sections, so these are held outside any section data
    m_iPropCount = 0;
    m_iVariationCount = 0;
    m_iFixupObjectCount = 0;
    m_iCamcorderOverlayCount = 0;
    m_iDrawDistanceCount = 0;
    m_iEffectCount = 0;
    m_iBlockingBoundCount = 0;

    m_iSectionCount = 0;

    m_bNextSectionIsManual = true;
    m_bNextSectionIsManualForAnims = true;
    m_bNextSectionIsManualForAudio = true;

    m_fTotalDuration = 0.0f;

    for (int i = 0; i < MAX_CUTSCENE_SECTIONS; i++)  // init section ptr
    {
        if ( m_bIsLoaded && (m_pSections[i] != NULL) )
        {
            delete m_pSections[i];
        }

        m_pSections[i] = NULL;
    }

    // init any dd pointers:
    for (int i = 0; i < MAX_DRAW_DISTANCES; i++)  // init dd ptr
    {
        if ( m_bIsLoaded && (m_pDrawDistances[i] != NULL) )
        {
            delete m_pDrawDistances[i];
        }

        m_pDrawDistances[i] = NULL;
    }

    // init any effect pointers:
    for (int i = 0; i < MAX_CUTSCENE_EFFECTS; i++)  // init effect ptr
    {
        if ( m_bIsLoaded && (m_pEffects[i] != NULL) )
        {
            delete m_pEffects[i];
        }

        m_pEffects[i] = NULL;
    }

    for (int i = 0; i < MAX_CUTSCENE_BLOCKING_BOUNDS; i++)  
    {
        if ( m_bIsLoaded && (m_pBlockingBounds[i] != NULL) )
        {
            delete m_pBlockingBounds[i];
        }

        m_pBlockingBounds[i] = NULL;
    }

    ClearSubtitles();

    for (int i = 0; i < MAX_CAMCORDER_OVERLAYS; i++)  
    {
        if ( m_bIsLoaded && (m_pCamcorderOverlays[i] != NULL) )
        {
            delete m_pCamcorderOverlays[i];
        }

        m_pCamcorderOverlays[i] = NULL;
    }

    for (int i = 0; i < MAX_FIXUP_OBJS; i++)
    {
        if ( m_bIsLoaded && (m_pFixupObjects[i] != NULL) )
        {
            delete m_pFixupObjects[i];
        }

        m_pFixupObjects[i] = NULL;
    }

    for (int i = 0; i < MAX_CUTSCENE_VARIATIONS; i++)
    {
        if ( m_bIsLoaded && (m_pVariations[i] != NULL) )
        {
            delete m_pVariations[i];
        }

        m_pVariations[i] = NULL;
    }

    for (int i = 0; i < MAX_CUTSCENE_PROPS; i++)
    {
        if ( m_bIsLoaded && (m_pProps[i] != NULL) )
        {
            delete m_pProps[i];
        }

        m_pProps[i] = NULL;
    }

    m_bIsLoaded = false;

    m_iStartFrame = 0;

    for ( int i = 0; i < MAX_CUTSCENE_SECTIONS; ++i )
    {
        m_iSectionSplitFrames[i] = 0;
    }

    m_iSectionSplitFrameCount = 0;
    m_iEndFrame = 0;

    m_cExtraRoom[0] = 0;

    ResetExtraRoomPos();
}

#if __BANK || __TOOL
bool cutfCutsceneFile::SaveFile( const char* pFilename, const char* pExtension ) const
{
    // determine the save type
    char cSaveFilename[RAGE_MAX_PATH];
    ASSET.FullWritePath( cSaveFilename, RAGE_MAX_PATH, pFilename, pExtension );
    const char* pFileExtension = ASSET.FindExtensionInPath( cSaveFilename );
    if ( pFileExtension == NULL )
    {
        pExtension = pFileExtension = PI_CUTSCENE_XMLFILE_EXT;
    }

    if ( strstr( pFileExtension, "xml" ) != NULL )
    {
        return PARSER.SaveObject( pFilename, pExtension ? pExtension : "", this, parManager::XML );
    }
    else if ( strstr( pFileExtension, "bin" ) != NULL )
    {
        return PARSER.SaveObject( pFilename, pExtension ? pExtension : "", this, parManager::BINARY );
    }

    fiStream *pStream = ASSET.Create( pFilename, pExtension );
    if ( !pStream )
    {
        return false;
    }

    char cLine[1024];

    // CUTSCENE_HEADER
    strcpy( cLine, "[CUTSCENE_HEADER]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        sprintf( cLine, "%d\t", m_iStartFrame );

        for ( int i = 0; i < m_iSectionSplitFrameCount; ++i )
        {
            sprintf( cLine, "%s%d\t", cLine, m_iSectionSplitFrames[i] );
        }

        sprintf( cLine, "%s%d\n", cLine, m_iEndFrame );
        pStream->Write( cLine, (int)strlen( cLine ) );
    }
    strcpy( cLine, "[/CUTSCENE_HEADER]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // TEXT (Subtitles) [merge]
    strcpy( cLine, "[TEXT]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        for ( int i = 0; i < m_iSubtitleCount; ++i )
        {
            STextIdDetails *pSubtitle = m_pSubtitles[i];
            
            sprintf( cLine, "%d %d %s\n", pSubtitle->m_iTextStartTime, pSubtitle->m_iTextDuration, pSubtitle->m_cTextOutput );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }
    }
    strcpy( cLine, "[/TEXT]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // DRAW_DISTANCE [merge]
    strcpy( cLine, "[DRAW_DISTANCE]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        for ( int i = 0; i < m_iDrawDistanceCount; ++i )
        {
            SDrawDistanceDetails *pDrawDistance = m_pDrawDistances[i];

            sprintf( cLine, "%d %d %f %f\n", pDrawDistance->iStartTime, 0, pDrawDistance->fNearClip, pDrawDistance->fFarClip );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }
    }
    strcpy( cLine, "[/DRAW_DISTANCE]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // FIXUP [merge]
    strcpy( cLine, "[FIXUP]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        for ( int i = 0; i < m_iFixupObjectCount; ++i )
        {
            SFixupObjectDetails *pFixupObject = m_pFixupObjects[i];

            sprintf( cLine, "%f %f %f %s %f\n", pFixupObject->vPosition.x, pFixupObject->vPosition.y, pFixupObject->vPosition.z,
                pFixupObject->cObjectName, pFixupObject->fRadius );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }
    }
    strcpy( cLine, "[/FIXUP]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // FLAGS
    strcpy( cLine, "[FLAGS]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_FADE_BETWEEN_SECTIONS )
        {
            sprintf( cLine, "FADE_BETWEEN_SECTION\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_NO_VEHICLE_LIGHTS )
        {
            sprintf( cLine, "NO_VEHICLE_LIGHTS\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_SHORT_FADE_AT_END )
        {
            sprintf( cLine, "SHORT_FADE_OUT\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_LONG_FADE_AT_END )
        {
            sprintf( cLine, "LONG_FADE_OUT\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_DONT_FADE_IN )
        {
            sprintf( cLine, "FLAG_DONT_FADE_IN\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_USE_ONE_AUDIO )
        {
            sprintf( cLine, "FLAG_USE_ONE_AUDIO\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_MUTE_MUSIC_PLAYER )
        {
            sprintf( cLine, "FLAG_MUTE_MUSIC_PLAYER\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_LEAK_RADIO )
        {
            sprintf( cLine, "FLAG_LEAK_RADIO\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_NO_AMBIENT_LIGHTS )
        {
            sprintf( cLine, "NO_AMBIENT_LIGHTS\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_TRANSLATE_BONE_IDS )
        {
            sprintf( cLine, "TRANSLATE_BONE_IDS\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_AUTO_SECTION )
        {
            sprintf( cLine, "AUTO_SECTION\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }

        if ( m_iCutsceneFlags & CUTSCENE_FLAGS_INTERP_CAMERA )
        {
            sprintf( cLine, "INTERP_CAMERA\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }
    }
    strcpy( cLine, "[/FLAGS]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // BLOCKING_BOUNDS
    strcpy( cLine, "[BLOCKING_BOUNDS]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        for ( int i = 0; i < m_iBlockingBoundCount; ++i )
        {
            sprintf( cLine, "%f %f %f %f %f %f %f %f %f %f %f %f %f\n", 
                m_pBlockingBounds[i]->vCorners[0].x, m_pBlockingBounds[i]->vCorners[0].y, m_pBlockingBounds[i]->vCorners[0].z,
                m_pBlockingBounds[i]->vCorners[1].x, m_pBlockingBounds[i]->vCorners[1].y, m_pBlockingBounds[i]->vCorners[1].z,
                m_pBlockingBounds[i]->vCorners[2].x, m_pBlockingBounds[i]->vCorners[2].y, m_pBlockingBounds[i]->vCorners[2].z,
                m_pBlockingBounds[i]->vCorners[3].x, m_pBlockingBounds[i]->vCorners[3].y, m_pBlockingBounds[i]->vCorners[3].z,
                m_pBlockingBounds[i]->fHeight );

            pStream->Write( cLine, (int)strlen( cLine ) );
        }
    }
    strcpy( cLine, "[/BLOCKING_BOUNDS]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // CAMCORDER
    strcpy( cLine, "[CAMCORDER]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        for ( int i = 0; i < m_iCamcorderOverlayCount; ++i )
        {
            sprintf( cLine, "%d %d\n", m_pCamcorderOverlays[i]->iStartTime, m_pCamcorderOverlays[i]->iEndTime );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }
    }
    strcpy( cLine, "[/CAMCORDER]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // PLAYER_START [merge]
    strcpy( cLine, "[PLAYER_START]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        sprintf( cLine, "%f %f %f\n", m_vPlayerStartingPos.x, m_vPlayerStartingPos.y, m_vPlayerStartingPos.z );
        pStream->Write( cLine, (int)strlen( cLine ) );
    }
    strcpy( cLine, "[/PLAYER_START]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // PROPS [merge]
    strcpy( cLine, "[PROPS]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        for ( int i = 0; i < m_iPropCount; ++i )
        {
            SPropDetails *pProp = m_pProps[i];

            sprintf( cLine, "%d %d %d\n", pProp->parent, pProp->prop, pProp->anchor );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }
    }
    strcpy( cLine, "[/PROPS]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // VARIATION [merge]
    strcpy( cLine, "[VARIATION]\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );
    {
        for ( int i = 0; i < m_iVariationCount; ++i )
        {
            SVariationDetails *pVariation = m_pVariations[i];

            sprintf( cLine, "%d %d %d %d %d\n", pVariation->iId, pVariation->iComponentId, 
                pVariation->iDrawableId, pVariation->iTextureId, pVariation->iStartTime );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }
    }
    strcpy( cLine, "[/VARIATION]\n\n" );
    pStream->Write( cLine, (int)strlen( cLine ) );

    // Sections
    for ( int i = 0; i < m_iSectionCount; ++i )
    {
        cutfCutsceneSection *pSection = m_pSections[i];

        // SECTION_START
        strcpy( cLine, "[SECTION_START]\n\n" );
        pStream->Write( cLine, (int)strlen( cLine ) );
        {
            // MODELS
            strcpy( cLine, "[MODELS]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                for ( int j = 0; j < pSection->GetModelCount(); ++j )
                {
                    SModelDetails& model = pSection->GetModel( j );
                    if ( (int)strlen( model.cAnimName2 ) == 0 )
                    {
                        sprintf( cLine, "%d %s %s\n", model.iId, model.cModelName, model.cAnimName );
                    }
                    else
                    {
                        sprintf( cLine, "%d %s %s %s %d\n", model.iId, model.cModelName, model.cAnimName, model.cAnimName2, model.iFlags );
                    }

                    pStream->Write( cLine, (int)strlen( cLine ) );
                }
            }
            strcpy( cLine, "[/MODELS]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // COMPRESSION
            strcpy( cLine, "[COMPRESSION]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                for ( int j = 0; j < pSection->GetModelCount(); ++j )
                {
                    SModelDetails& model = pSection->GetModel( j );
    
                    if ( (int)strlen( model.cCompression ) > 0 )
                    {
                        sprintf( cLine, "%d %s\n", model.iId, model.cCompression );
                        pStream->Write( cLine, (int)strlen( cLine ) );
                    }
                }
            }
            strcpy( cLine, "[/COMPRESSION]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // ATTACHMENT
            strcpy( cLine, "[ATTACHMENT]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                for ( int j = 0; j < pSection->GetAttachmentCount(); ++j )
                {
                    SAttachmentDetails& attachment = pSection->GetAttachment( j );

                    sprintf( cLine,"%d %d %d", attachment.child, attachment.parent, attachment.boneid );
                    pStream->Write( cLine, (int)strlen( cLine ) );
                }
            }
            strcpy( cLine, "[/ATTACHMENT]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // LIGHTS
            strcpy( cLine, "[LIGHTS]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                for ( int j = 0; j < pSection->GetLightCount(); ++j )
                {
                    SLightDetails& light = pSection->GetLight( j );

                    sprintf( cLine, "%s\n", light.cLightName );
                    pStream->Write( cLine, (int)strlen( cLine ) );
                }
            }
            strcpy( cLine, "[/LIGHTS]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // EFFECTS
            strcpy( cLine, "[EFFECTS]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                for ( int j = 0; j < m_iEffectCount; ++j )
                {
                    SEffectsDetails *pEffect = m_pEffects[j];

                    if ( pEffect->SectionCreated == i )
                    {
                        if ( (pEffect->iType == CUTSCENE_PARTICLE_TYPE_ACTIVE) && ((int)strlen( pEffect->cEvoName[0] ) > 0) )
                        {
                            sprintf( cLine, "%s %d %d %d %d %f %f %f %f %f %f %f %s %f %s %f %s %f %s %f\n", 
                                pEffect->cOriginalEffectName, pEffect->iAttachedTo, pEffect->iBoneTag, pEffect->iStartTime, pEffect->iEndTime, 
                                pEffect->vPos.x, pEffect->vPos.y, pEffect->vPos.z, pEffect->vDir.x, pEffect->vDir.y, pEffect->vDir.z, 0.0f, 
                                pEffect->cEvoName[0], pEffect->fEvoValue[0], pEffect->cEvoName[1], pEffect->fEvoValue[1], 
                                pEffect->cEvoName[2], pEffect->fEvoValue[2], pEffect->cEvoName[3], pEffect->fEvoValue[3] );
                        }
                        else
                        {
                            sprintf( cLine, "%s %d %d %d %d %f %f %f %f %f %f %f\n", 
                                pEffect->cOriginalEffectName, pEffect->iAttachedTo, pEffect->iBoneTag, pEffect->iStartTime, pEffect->iEndTime, 
                                pEffect->vPos.x, pEffect->vPos.y, pEffect->vPos.z, pEffect->vDir.x, pEffect->vDir.y, pEffect->vDir.z, 0.0f );
                        }
                    }

                    pStream->Write( cLine, (int)strlen( cLine ) );
                }
            }
            strcpy( cLine, "[/EFFECTS]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // VEHICLE_DETAILS
            strcpy( cLine, "[VEHICLE_DETAILS]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                for ( int j = 0; j < pSection->GetVehicleCount(); ++j )
                {
                    SVehicleDetails& vehicle = pSection->GetVehicle( j );

                    sprintf( cLine, "%d %d %d %d %d %d\n", vehicle.iId, 
                        vehicle.iColour.GetRed(), vehicle.iColour.GetGreen(), vehicle.iColour.GetBlue(), vehicle.iColour.GetAlpha(), vehicle.iDirtLevel );
                    pStream->Write( cLine, (int)strlen( cLine ) );
                }
            }
            strcpy( cLine, "[/VEHICLE_DETAILS]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // VEHICLE_REMOVAL
            strcpy( cLine, "[VEHICLE_REMOVAL]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                for ( int j = 0; j < pSection->GetVehicleRemovalCount(); ++j )
                {
                    SVehicleRemoval& vehicleRemoval = pSection->GetVehicleRemoval( j );

                    sprintf( cLine, "%d %d\n", vehicleRemoval.iId, vehicleRemoval.iBone );
                    pStream->Write( cLine, (int)strlen( cLine ) );
                }
            }
            strcpy( cLine, "[/VEHICLE_REMOVAL]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // ORIENT (Rotation)
            strcpy( cLine, "[ORIENT]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                sprintf( cLine, "%f\n", pSection->GetRotation() );
                pStream->Write( cLine, (int)strlen( cLine ) );
            }
            strcpy( cLine, "[/ORIENT]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // REMOVE (Hidden Objects) [merge]
            strcpy( cLine, "[REMOVE]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                for ( int j = 0; j < pSection->GetHiddenObjectCount(); ++j )
                {
                    SHiddenObjectDetails& hiddenObject = pSection->GetHiddenObject( j );

                    sprintf( cLine, "%f %f %f %s %f\n", hiddenObject.vPosition.x, hiddenObject.vPosition.y, hiddenObject.vPosition.z, 
                        hiddenObject.cObjectName, hiddenObject.fRadius );
                    pStream->Write( cLine, (int)strlen( cLine ) );
                }
            }
            strcpy( cLine, "[/REMOVE]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // TIME [merge]
            strcpy( cLine, "[TIME]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                // deprecated: rexMBRage exports this tag empty (another tool could set an hour and minute), but the game does not load it.
            }
            strcpy( cLine, "[/TIME]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // OFFSET [merge]
            strcpy( cLine, "[OFFSET]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                Vector3 vOffset = pSection->GetOffset();

                sprintf( cLine, "%f %f %f\n", vOffset.x, vOffset.y, vOffset.z  );
                pStream->Write( cLine, (int)strlen( cLine ) );
            }
            strcpy( cLine, "[/OFFSET]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // DURATION [merge]
            strcpy( cLine, "[DURATION]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                sprintf( cLine, "%f\n", pSection->GetDuration()  );
                pStream->Write( cLine, (int)strlen( cLine ) );
            }
            strcpy( cLine, "[/DURATION]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // AUDIO [merge]
            strcpy( cLine, "[AUDIO]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                sprintf( cLine, "%s\n", pSection->GetAudioTrackName() );
                pStream->Write( cLine, (int)strlen( cLine ) );
            }
            strcpy( cLine, "[/AUDIO]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // ANIMRANGE [merge]
            strcpy( cLine, "[ANIMRANGE]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                sprintf( cLine, "range %s %f %f\n", pSection->UseRange() ? "on" : "off", pSection->GetRangeStart(), pSection->GetRangeEnd() );
                pStream->Write( cLine, (int)strlen( cLine ) );
            }
            strcpy( cLine, "[/ANIMRANGE]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // ANIM (Anim Dict)
            strcpy( cLine, "[ANIM]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                sprintf( cLine, "%s\n", pSection->GetClipDictName() );
                pStream->Write( cLine, (int)strlen( cLine ) );
            }
            strcpy( cLine, "[/ANIM]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );

            // CAMERA (Camera Animation)
            strcpy( cLine, "[CAMERA]\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
            {
                sprintf( cLine, "%s\n", pSection->GetCameraAnimName() );
                pStream->Write( cLine, (int)strlen( cLine ) );
            }
            strcpy( cLine, "[/CAMERA]\n\n" );
            pStream->Write( cLine, (int)strlen( cLine ) );
        }
        strcpy( cLine, "\n[SECTION_END]\n\n\n" );
        pStream->Write( cLine, (int)strlen( cLine ) );
    }

    pStream->Close();

    return true;
}
#endif // __BANK || __TOOL

bool cutfCutsceneFile::LoadFile( const char* pFilename, const char* pExtension )
{
    // determine the load type
    char cLoadFilename[RAGE_MAX_PATH];
    ASSET.FullReadPath( cLoadFilename, RAGE_MAX_PATH, pFilename, pExtension );
    const char* pFileExtension = ASSET.FindExtensionInPath( cLoadFilename );  
    if ( pFileExtension == NULL )
    {
        pExtension = pFileExtension = PI_CUTSCENE_XMLFILE_EXT;
    }

#if __DEV
    cutfDisplayf( "About to load %s file", pFileExtension );
#endif

    // Based on the extension (and later, a version number), determine which Parse function to use
    bool result = false;
    if ( (strstr( pFileExtension, "xml" ) != NULL) || (strstr( pFileExtension, "bin" ) != NULL) )
    {
        parTree *pTree = PARSER.LoadTree( pFilename, pExtension ? pExtension : "" );
        if ( pTree )
        {
            result = ParseXml( pTree );
            delete pTree;
        }
#if __DEV
        else
        {
            cutfDisplayf( "Could not load tree." );
        }
#endif
    }
    else
    {
        fiStream* pStream = ASSET.Open( pFilename, pExtension );
        if ( pStream != NULL )
        {
            u32 size = pStream->Size();

            char* pTempData = rage_new char[size];
            pStream->Read( pTempData, size );

            result = ParseData( pTempData, size );

            delete[] pTempData;
            pStream->Close();
        }
#if __DEV
        else
        {
            cutfDisplayf( "Could not open file stream." );
        }
#endif
    }

#if __DEV
    cutfDisplayf( "Load %s.", result ? "success" : "failed" );
#endif

    return result;
}

bool cutfCutsceneFile::ParseData( const char* pData, u32 /*size*/ )
{
#if __DEV
    cutfDisplayf( "About to parse data." );
#endif

    Clear();

    const char* p_cTempCurr = pData;	
    char cTempLine[1024];

    // new states:
    enum
    {
        LS_NOTHING,
        //		LS_GET_TIME,
        LS_GET_OFFSET,
        LS_GET_HEADER,
        LS_GET_DURATION,
        LS_GET_ANIM,
        LS_GET_MODELS,
        LS_GET_COMPRESSION, // NEW
        LS_GET_VEHICLE_DETAILS,
        LS_GET_VEHICLE_REMOVAL,
        LS_GET_ORIENT,  // NEW
        LS_GET_CAMCORDER,
        LS_GET_VARIATION,
        LS_REMOVE_MODELS,
        LS_FIXUP_MODELS,
        LS_GET_CAMERA,
        LS_GET_AUDIO,
        LS_GET_ANIMRANGE,
        LS_GET_TEXT,
        LS_SET_ATTACHMENTS,
        LS_SET_PROPS,
        LS_GET_FLAGS,
        LS_GET_LIGHTS,
        LS_GET_DRAW_DISTANCE,
        LS_GET_EFFECTS,
        LS_GET_BLOCKING_BOUNDS,
        //		LS_GET_WEATHER,
        LS_GET_PLAYER_STARTING_POS,
        LS_GET_MISSION_TEXT_NAME,
        LS_GET_EXTRA_ROOM,
        LS_MAX_LOAD_STATES
    };


    u32 loadState = LS_NOTHING;

    while ( (p_cTempCurr = GetNextLine( p_cTempCurr,cTempLine, 1024 )) != NULL )
    {
        // skip any comment lines:
        if ((cTempLine[0] == '\\') && (cTempLine[1] == '\\'))
        {
            continue;
        }

        if ((!strcmp(cTempLine,"[/OFFSET]")) ||
            (!strcmp(cTempLine,"[/CUTSCENE_HEADER]")) ||
            //			(!strcmp(cTempLine,"[/TIME]")) ||
            (!strcmp(cTempLine,"[/AUDIO]")) ||
            (!strcmp(cTempLine,"[/ANIMRANGE]")) ||
            (!strcmp(cTempLine,"[/DURATION]")) ||
            (!strcmp(cTempLine,"[/MODELS]")) ||
            (!strcmp(cTempLine,"[/COMPRESSION]")) ||
            (!strcmp(cTempLine,"[/VEHICLE_DETAILS]")) ||
            (!strcmp(cTempLine,"[/VEHICLE_REMOVAL]")) ||
            (!strcmp(cTempLine,"[/ORIENT]")) ||
            (!strcmp(cTempLine,"[/CAMCORDER]")) ||
            (!strcmp(cTempLine,"[/VARIATION]")) ||
            (!strcmp(cTempLine,"[/REMOVE]")) ||
            (!strcmp(cTempLine,"[/FIXUP]")) ||
            (!strcmp(cTempLine,"[/ATTACHMENT]")) ||
            (!strcmp(cTempLine,"[/LIGHTS]")) ||
            (!strcmp(cTempLine,"[/EFFECTS]")) ||
            (!strcmp(cTempLine,"[/DRAW_DISTANCE]")) ||
            (!strcmp(cTempLine,"[/BLOCKING_BOUNDS]")) ||
            (!strcmp(cTempLine,"[/PROPS]")) ||
            (!strcmp(cTempLine,"[/CAMERA]")) ||
            (!strcmp(cTempLine,"[/ANIM]")) ||
            (!strcmp(cTempLine,"[/FLAGS]")) ||
            (!strcmp(cTempLine,"[/PLAYER_START]")) ||
            (!strcmp(cTempLine,"[/MISSION_TEXT_NAME]")) ||
            (!strcmp(cTempLine,"[/EXTRA_ROOM]")) ||
            (!strcmp(cTempLine,"[/TEXT]")))
        {
            loadState = LS_NOTHING;
            continue;
        }

        switch(loadState)
        {
            //
            // set the current load state:
            //
        case LS_NOTHING:
            {
                if(!strcmp(cTempLine,"[SECTION_START]"))
                {
                    if ( !m_bNextSectionIsManual )  // if its not a manual section, then create the section here:
                    {
                        if ( !cutfVerifyf( m_pSections[m_iSectionCount] == NULL, "We didn't clean ourselves up properly last time." ) )
                        {
                            delete m_pSections[m_iSectionCount];
                        }

                        // create a new section
                        m_pSections[m_iSectionCount] = rage_new cutfCutsceneSection();
                    }

                    if ( m_bNextSectionIsManual )
                    {
                        m_bNextSectionIsManual = false;
                        if ( m_pSections[m_iSectionCount] )
                        {
                            m_pSections[m_iSectionCount]->SetManualSectionToUse( m_iSectionCount );
                        }
                    }
                    else
                    {
                        if ( m_pSections[m_iSectionCount] && m_pSections[m_iSectionCount - 1] )
                        {
                            m_pSections[m_iSectionCount]->SetManualSectionToUse( m_pSections[m_iSectionCount - 1]->GetManualSectionToUse() );
                        }
                    }

                    if ( m_bNextSectionIsManualForAudio )
                    {
                        m_bNextSectionIsManualForAudio = false;
                        if ( m_pSections[m_iSectionCount] )
                        {
                            m_pSections[m_iSectionCount]->SetManualSectionToUseForAudio( m_iSectionCount );
                        }
                    }
                    else
                    {
                        if ( m_pSections[m_iSectionCount] && m_pSections[m_iSectionCount - 1] )
                        {
                            m_pSections[m_iSectionCount]->SetManualSectionToUseForAudio( m_pSections[m_iSectionCount - 1]->GetManualSectionToUseForAudio() );
                        }
                    }

                    if ( m_bNextSectionIsManualForAnims )
                    {
                        m_bNextSectionIsManualForAnims = false;

                        if ( m_pSections[m_iSectionCount] )
                        {
                            m_pSections[m_iSectionCount]->SetIsManualSection( true );
                        }
                    }

                    loadState = LS_NOTHING;
                }
                if(!strcmp(cTempLine,"[SECTION_END]"))
                {
                    // set stuff that isn't declared in the cut file
                    /*					if (m_pSections[m_iSectionCount]->iWeather == -1)
                    m_pSections[m_iSectionCount]->iWeather = CWeather::WeatherTypeInList;

                    if (m_pSections[m_iSectionCount]->iHours != -1 && m_pSections[m_iSectionCount]->iMinutes != -1)
                    {
                    m_pSections[m_iSectionCount]->iHours = CClock::GetGameClockHours();
                    m_pSections[m_iSectionCount]->iMinutes = CClock::GetGameClockMinutes();
                    }
                    */

                    if ( cutfVerifyf( m_iSectionCount < MAX_CUTSCENE_SECTIONS, "Max number of cutscene sections (%d) hit!", MAX_CUTSCENE_SECTIONS ) )
                    {
                        // move onto the next section
                        m_iSectionCount++;  // increment the counter
                    }

                    loadState = LS_NOTHING;
                }
                if(!strcmp(cTempLine,"[OFFSET]"))
                {
                    loadState = LS_GET_OFFSET;
                }
                if(!strcmp(cTempLine,"[PLAYER_START]"))
                {
                    loadState = LS_GET_PLAYER_STARTING_POS;
                }
                if(!strcmp(cTempLine,"[MISSION_TEXT_NAME]"))
                {
                    loadState = LS_GET_MISSION_TEXT_NAME;
                }
                if(!strcmp(cTempLine,"[EXTRA_ROOM]"))
                {
                    loadState = LS_GET_EXTRA_ROOM;
                }
                if(!strcmp(cTempLine,"[CUTSCENE_HEADER]"))
                {
                    loadState = LS_GET_HEADER;
                }
                if(!strcmp(cTempLine,"[AUDIO]"))
                {
                    loadState = LS_GET_AUDIO;
                }
                /*
                if(!strcmp(cTempLine,"[TIME]"))
                {
                    loadState = LS_GET_TIME;
                }*/
                if ( !strcmp( cTempLine, "[ANIMRANGE]" ) )
                {
                    loadState = LS_GET_ANIMRANGE;
                }
                if(!strcmp(cTempLine,"[DURATION]"))
                {
                    loadState = LS_GET_DURATION;
                }
                if(!strcmp(cTempLine,"[ANIM]"))
                {
                    loadState = LS_GET_ANIM;
                }
                if(!strcmp(cTempLine,"[MODELS]"))
                {
                    loadState = LS_GET_MODELS;
                }
                if ( !strcmp( cTempLine,"[COMPRESSION]" ) )
                {
                    loadState = LS_GET_COMPRESSION;
                }
                if(!strcmp(cTempLine,"[VEHICLE_DETAILS]"))
                {
                    loadState = LS_GET_VEHICLE_DETAILS;
                }
                if(!strcmp(cTempLine,"[VEHICLE_REMOVAL]"))
                {
                    loadState = LS_GET_VEHICLE_REMOVAL;
                }
                if ( !strcmp( cTempLine, "[ORIENT]" ) )
                {
                    loadState = LS_GET_ORIENT;
                }
                if(!strcmp(cTempLine,"[CAMCORDER]"))
                {
                    loadState = LS_GET_CAMCORDER;
                }
                if(!strcmp(cTempLine,"[LIGHTS]"))
                {
                    loadState = LS_GET_LIGHTS;
                }
                if(!strcmp(cTempLine,"[EFFECTS]"))
                {
                    loadState = LS_GET_EFFECTS;
                }
                if(!strcmp(cTempLine,"[DRAW_DISTANCE]"))
                {
                    loadState = LS_GET_DRAW_DISTANCE;
                }
                if(!strcmp(cTempLine,"[BLOCKING_BOUNDS]"))
                {
                    loadState = LS_GET_BLOCKING_BOUNDS;
                }
                /*				if(!strcmp(cTempLine,"[WEATHER]"))
                {
                loadState = LS_GET_WEATHER;
                }*/
                if(!strcmp(cTempLine,"[VARIATION]"))
                {
                    loadState = LS_GET_VARIATION;
                }
                if(!strcmp(cTempLine,"[CAMERA]"))
                {
                    loadState = LS_GET_CAMERA;
                }
                if(!strcmp(cTempLine,"[TEXT]"))
                {
                    loadState = LS_GET_TEXT;
                }
                if(!strcmp(cTempLine,"[REMOVE]"))
                {
                    loadState = LS_REMOVE_MODELS;
                }
                if(!strcmp(cTempLine,"[FIXUP]"))
                {
                    loadState = LS_FIXUP_MODELS;
                }
                if(!strcmp(cTempLine,"[ATTACHMENT]"))
                {
                    loadState = LS_SET_ATTACHMENTS;
                }
                if(!strcmp(cTempLine,"[PROPS]"))
                {
                    loadState = LS_SET_PROPS;
                }
                if(!strcmp(cTempLine,"[FLAGS]"))
                {
                    loadState = LS_GET_FLAGS;
                }

                break;
            }
            /*
            //
            // game clock
            //
            case LS_GET_TIME:
            {
            sscanf(cTempLine,"%2d%*c%2d",&m_pSections[m_iSectionCount]->iHours, &m_pSections[m_iSectionCount]->iMinutes);
            break;
            }
            */
            //
            // flgs
            //
        case LS_GET_FLAGS:
            {
                char cFlagName[30];

                sscanf(cTempLine,"%s",cFlagName);

                if (!stricmp("FADE_BETWEEN_SECTION", cFlagName))  // turn on fades
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_FADE_BETWEEN_SECTIONS;

                if (!stricmp("NO_VEHICLE_LIGHTS", cFlagName))  // no vehicle headlights
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_NO_VEHICLE_LIGHTS;

                if (!stricmp("SHORT_FADE_OUT", cFlagName))  // longer fade at end
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_SHORT_FADE_AT_END;

                if (!stricmp("LONG_FADE_OUT", cFlagName))  // longer fade at end
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_LONG_FADE_AT_END;

                if (!stricmp("FLAG_DONT_FADE_OUT", cFlagName))  // turn off fade at start
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_DONT_FADE_CUTSCENE_OUT;

                if (!stricmp("FLAG_USE_ONE_AUDIO", cFlagName))  // scene uses one audio file
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_USE_ONE_AUDIO;

                if (!stricmp("FLAG_MUTE_MUSIC_PLAYER", cFlagName))  // mute music player
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_MUTE_MUSIC_PLAYER;

                if(!stricmp("FLAG_LEAK_RADIO", cFlagName))
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_LEAK_RADIO;

                if (!stricmp("NO_AMBIENT_LIGHTS", cFlagName))  // no ambient lights
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_NO_AMBIENT_LIGHTS;

                if (!stricmp("TRANSLATE_BONE_IDS", cFlagName))  // translate bone ids
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_TRANSLATE_BONE_IDS;

                if (!stricmp("AUTO_SECTION", cFlagName))  // auto-section
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_AUTO_SECTION;

                if (!stricmp("INTERP_CAMERA", cFlagName))  // interp camera
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_INTERP_CAMERA;

                /*
                if (!stricmp("CONTINUE_TIME", cFlagName))  // don't go back to original time
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_CONTINUE_TIME;

                if (!stricmp("CONTINUE_WEATHER", cFlagName))  // don't go back to original weather
                    m_iCutsceneFlags |= CUTSCENE_FLAGS_CONTINUE_WEATHER;
                */
                break;
            }

            //
            // cutscene header
            //
        case LS_GET_HEADER:
            {
                s32 iFrame = 0;

                // get the start frame
                sscanf( cTempLine, "%d", &iFrame );
                m_iStartFrame = iFrame;

                // get all of the section split frames
                m_iSectionSplitFrameCount = 0;
                char* cTok = strstr( cTempLine, "\t" );
                while ( cTok && *cTok )
                {
                    ++cTok;

                    if ( *cTok != 0 )
                    {
                        sscanf( cTok, "%d", &iFrame );

                        if ( cutfVerifyf( m_iSectionSplitFrameCount < MAX_CUTSCENE_SECTIONS, 
                             "Max number of cutscene section frames (%d) hit", MAX_CUTSCENE_SECTIONS ) )
                        {
                            m_iSectionSplitFrames[m_iSectionSplitFrameCount] = iFrame;
                            ++m_iSectionSplitFrameCount;
                        }

                        cTok = strstr( cTok, "\t" );
                    }
                }

                // the last one is our end frame, let's store it off separately
                if ( m_iSectionSplitFrameCount > 0 )
                {
                    m_iEndFrame = m_iSectionSplitFrames[m_iSectionSplitFrameCount - 1];
                    m_iSectionSplitFrames[m_iSectionSplitFrameCount - 1] = 0;
                    --m_iSectionSplitFrameCount;
                }

                // 1st section always is a manual section
                m_bNextSectionIsManual = true;

                m_bNextSectionIsManualForAnims = true;

                if ( m_iCutsceneFlags & CUTSCENE_FLAGS_USE_ONE_AUDIO )
                {
                    m_bNextSectionIsManualForAudio = false;
                }
                else
                {
                    m_bNextSectionIsManualForAudio = true;
                }

                if ( !cutfVerifyf( m_pSections[m_iSectionCount] == NULL, "We didn't clean ourselves up properly last time." ) )
                {
                    delete m_pSections[m_iSectionCount];
                }

                // create a new section here, so any global items are stored for this 'first' section:
                m_pSections[m_iSectionCount] = rage_new cutfCutsceneSection();

                break;
            }

            //
            // offset of the cutscene
            //
        case LS_GET_OFFSET:
            {
                if ( !cutfVerifyf( m_pSections[m_iSectionCount] != NULL, "We should have created a cutfCutsceneSection already." ) )
                {
                    delete m_pSections[m_iSectionCount];
                }

                sscanf(cTempLine,"%f %f %f",&m_pSections[m_iSectionCount]->GetOffset().x,&m_pSections[m_iSectionCount]->GetOffset().y,&m_pSections[m_iSectionCount]->GetOffset().z);
                break;
            }

        case LS_GET_PLAYER_STARTING_POS:
            {
                m_vPlayerStartingPos.Zero();

                sscanf( cTempLine, "%f %f %f", &m_vPlayerStartingPos.x, &m_vPlayerStartingPos.y, &m_vPlayerStartingPos.z );
                break;
            }

        case LS_GET_MISSION_TEXT_NAME:
            {
                char cMissionTextName[MISSION_NAME_LENGTH];
                cMissionTextName[0] = '\0';

                if ( (int)strlen( cTempLine ) < MISSION_NAME_LENGTH )
                {
                    sscanf( cTempLine, "%s", cMissionTextName );
                }

                SetMissionTextName( cMissionTextName );
                break;
            }

        case LS_GET_EXTRA_ROOM:
            {
                char cExtraRoom[MAX_EXTRA_ROOM_CHARS];
                cExtraRoom[0] = '\0';

                if ( cutfVerifyf( (int)strlen(cTempLine) < MAX_EXTRA_ROOM_CHARS, 
                    "Extra room name is longer than %d chars", MAX_EXTRA_ROOM_CHARS ) )
                {
                    sscanf(cTempLine,"%s",m_cExtraRoom);
                }

                SetExtraRoom( cExtraRoom );
                break;
            }

            //
            // offset of the cutscene
            //
        case LS_GET_DURATION:
            {
                float m_fDuration;
                sscanf(cTempLine,"%f",&m_fDuration);

                m_pSections[m_iSectionCount]->SetDuration( m_fDuration );

                m_fTotalDuration += m_pSections[m_iSectionCount]->GetDuration();
                break;
            }

            //
            // get the anim dict used for this section
            //
        case LS_GET_ANIM:
            {		
                char cAnimName[CUTSCENE_OBJNAMELEN];

                sscanf(cTempLine,"%s",cAnimName);

                m_pSections[m_iSectionCount]->SetAnimDictName( cAnimName );
                break;
            }

            //
            // get the models used & animation used
            //
        case LS_GET_MODELS:
            {		
                s32 iId;  // id - should start at 1 (NOT 0)
                char cName[CUTSCENE_OBJNAMELEN];
                char cAnimName[CUTSCENE_OBJNAMELEN];
                char cAnimName2[CUTSCENE_OBJNAMELEN];
                s32 iFlags = 0; // note that iFlags is optional

                cAnimName2[0] = '\0';

                sscanf( cTempLine, "%d %s %s %s %d", &iId, cName, cAnimName, cAnimName2, &iFlags );

                // create an empty slot to keep things in sync
                while ( (iId > 0) && (iId != (m_pSections[m_iSectionCount]->GetModel( m_pSections[m_iSectionCount]->GetModelCount() - 1 ).iId + 1)) )
                {                    
                    SModelDetails emtpyModel;

                    emtpyModel.iId = ( m_pSections[m_iSectionCount]->GetModel( m_pSections[m_iSectionCount]->GetModelCount() - 1 ).iId + 1 );
                    emtpyModel.bEmpty = true;  // flag as an empty slot
                    emtpyModel.bLoaded = false;
                    emtpyModel.cModelName[0] = '\0';
                    emtpyModel.cAnimName[0] = '\0';
                    emtpyModel.cAnimName2[0] = '\0';
                    emtpyModel.cCompression[0] = '\0';
                    emtpyModel.iFlags = 0;                    

                    m_pSections[m_iSectionCount]->AddModel( emtpyModel );
                }

                cutfAssertf( (int)strlen(cName) < CUTSCENE_OBJNAMELEN, 
                     "name of cutscene object must be less than %d chars", CUTSCENE_OBJNAMELEN );
                cutfAssertf( (int)strlen(cAnimName) < CUTSCENE_OBJNAMELEN, 
                     "name of cutscene animation must be less than %d chars", CUTSCENE_OBJNAMELEN );
                cutfAssertf( (int)strlen(cAnimName2) < CUTSCENE_OBJNAMELEN, 
                     "name of cutscene animation must be less than %d chars", CUTSCENE_OBJNAMELEN );

                SModelDetails model;

                safecpy( model.cModelName, cName, sizeof( model.cModelName ) );
                safecpy( model.cAnimName, cAnimName, sizeof( model.cAnimName ) );
                model.cCompression[0] = '\0';

                model.bEmpty = false;
                model.iId = iId;
                model.bLoaded = false;
                model.iFlags = iFlags;                

                if ( !strcmpi( cAnimName2, "null" ) || (cAnimName2[0] == '\0') )
                {
                    model.cAnimName2[0] = '\0';
                }
                else
                {
                    safecpy( model.cAnimName2, cAnimName2, sizeof( model.cAnimName2 ) );
                }

                if ( (m_iSectionCount == 0) && !strcmpi( cName, "player" ) )
                {
                    m_iPlayerId = iId;
                }

                m_pSections[m_iSectionCount]->AddModel( model );

                break;
            }

        case LS_GET_COMPRESSION:
            {
                s32 iId = 0;
                char cCompression[CUTSCENE_OBJNAMELEN];

                sscanf( cTempLine, "%d %s", &iId, cCompression );

                if ( (iId >= 0) && (iId < m_pSections[m_iSectionCount]->GetModelCount()) )
                {
                    SModelDetails& model = m_pSections[m_iSectionCount]->GetModel( iId );
                    safecpy( model.cCompression, cCompression, CUTSCENE_OBJNAMELEN );
                }

                break;
            }
            //
            // get the vehicle details (car colours etc)
            //
        case LS_GET_VEHICLE_DETAILS:
            {		
                s32 iId;
                s32 iColour1, iColour2, iColour3, iColour4, iDirtLevel;
                s32 iTexture = -1;  // default this to -1 just incase it doesnt exist in the cut file

                sscanf( cTempLine,"%d %d %d %d %d %d %d", &iId, &iColour1, &iColour2, &iColour3, &iColour4, &iDirtLevel, &iTexture );

                SVehicleDetails vehicle;

                vehicle.iId = iId;
                vehicle.iColour.Set( iColour1, iColour2, iColour3, iColour4 );
                vehicle.iDirtLevel = (u8)iDirtLevel;
                vehicle.iTexture = (s8)iTexture;

                m_pSections[m_iSectionCount]->AddVehicle( vehicle );

                break;
            }


            //
            // get the vehicle removal (ie removing car windows)
            //
        case LS_GET_VEHICLE_REMOVAL:
            {		
                s32 iId;
                s32 iBone;

                sscanf( cTempLine, "%d %d", &iId, &iBone );

                SVehicleRemoval vehicleRemoval;

                vehicleRemoval.iId = iId;
                vehicleRemoval.iBone = iBone;

                m_pSections[m_iSectionCount]->AddVehicleRemoval( vehicleRemoval );

                break;
            }

        case LS_GET_ORIENT:
            {
                float fRotation = 0.0f;

                sscanf( cTempLine, "%f", &fRotation );

                m_pSections[m_iSectionCount]->SetRotation( fRotation );

                break;
            }

            //
            // get the ped variation details
            //
        case LS_GET_VARIATION:
            {	
                s32 iId, iComponentId, iDrawableId, iTextureId, iStartTime;

                iStartTime = 0;  // initial values

                sscanf( cTempLine,"%d %d %d %d %d", &iId, &iComponentId, &iDrawableId, &iTextureId, &iStartTime );

                /*
                if ( iId == m_iPlayerId )
                {
                    if ( m_iSectionCount == 0 )
                    {
                        // setup player using stuff read from this line in the cut file.

                        // actually i think we are now just going to rely on level guys setting player variation now?
                    }
                }
                else*/
                // now the varitions are global we dont know the player id yet - so we just store and ignore it later on when they are set up
                {
                    SVariationDetails *pVariation = rage_new SVariationDetails;

                    pVariation->iId = iId;
                    pVariation->iComponentId = iComponentId;
                    pVariation->iDrawableId = iDrawableId;
                    pVariation->iTextureId = iTextureId;
                    pVariation->iStartTime = iStartTime;
                    pVariation->iSectionCreatedIn = m_iSectionCount;
                    pVariation->bIsSet = false;

                    AddVariation( pVariation );
                }

                break;
            }

            //
            // any models and their positions that must be removed for the scene
            //
        case LS_REMOVE_MODELS:
            {		
                char cObjectName[32];
                float fXPos,fYPos,fZPos,fRadius;

                fRadius = 1.5f;  // 1.5m as default

                sscanf( cTempLine,"%f %f %f %s %f",&fXPos, &fYPos, &fZPos, cObjectName, &fRadius );

                SHiddenObjectDetails hiddenObject;

                // hiddenObject.pHiddenEntity = NULL;
                hiddenObject.vPosition = Vector3(fXPos, fYPos, fZPos);
                hiddenObject.fRadius = fRadius;

                cutfAssertf( (int)strlen(cObjectName) < CUTSCENE_OBJNAMELEN, 
                     "name of hidden object must be less than %d chars", CUTSCENE_OBJNAMELEN );
                safecpy( hiddenObject.cObjectName, cObjectName, sizeof( hiddenObject.cObjectName ) );

                m_pSections[m_iSectionCount]->AddHiddenObject( hiddenObject );

                break;
            }


            //
            // any models and their positions that must be removed for the scene
            //
        case LS_FIXUP_MODELS:
            {		
                char cObjectName[32];
                float fXPos, fYPos, fZPos, fRadius;

                fRadius = 1.5f;  // 1.5m as default

                sscanf( cTempLine, "%f %f %f %s %f", &fXPos, &fYPos, &fZPos, cObjectName, &fRadius);

                SFixupObjectDetails *pFixupObject = rage_new SFixupObjectDetails;

                pFixupObject->vPosition = Vector3( fXPos, fYPos, fZPos );
                pFixupObject->fRadius = fRadius;

                cutfAssertf( (int)strlen(cObjectName) < CUTSCENE_OBJNAMELEN, 
                     "name of fixup object must be less than %d chars", CUTSCENE_OBJNAMELEN );
                safecpy( pFixupObject->cObjectName, cObjectName, CUTSCENE_OBJNAMELEN );

                AddFixupObject( pFixupObject );

                break;
            }



            //
            // find out what models will attach to others & where
            //
        case LS_SET_ATTACHMENTS:
            {		
                u32 parent, child, boneid;
                sscanf( cTempLine, "%d %d %d", &child, &parent, &boneid );

                SAttachmentDetails attachment;

                // -1 as we start at 1 in the model list (but obviously array starts at 0)
                attachment.child = child;
                attachment.parent = parent;
                attachment.boneid = boneid;

                m_pSections[m_iSectionCount]->AddAttachment( attachment );

                break;
            }

            //
            // find out what models will attach to others & where
            //
        case LS_SET_PROPS:
            {	
                u32 parent,prop,anchor;
                sscanf(cTempLine,"%d %d %d",&parent,&prop,&anchor);

                SPropDetails *pProp = rage_new SPropDetails;

                pProp->parent = parent;
                pProp->prop = prop;
                pProp->anchor = anchor;
                pProp->iSectionCreatedIn = m_iSectionCount;

                AddProp( pProp );

                break;
            }


            //
            // get the camera animations used - may not need this as we could just assume 1 camera each cutscene & have an assumed filename ie camera.anim
            //
        case LS_GET_CAMERA:
            {		
                char cAnimName[CUTSCENE_OBJNAMELEN];

                sscanf(cTempLine,"%s", cAnimName );

                // create and set up a cutscene object for this model
                m_pSections[m_iSectionCount]->SetCameraAnimName( cAnimName );
                break;
            }


            //
            // get the camera animations used - may not need this as we could just assume 1 camera each cutscene & have an assumed filename ie camera.anim
            //
        case LS_GET_AUDIO:
            {		
                char cAudioName[CUTSCENE_OBJNAMELEN];
                cAudioName[0] = '\0';

                sscanf( cTempLine, "%s", cAudioName );

                if ( cAudioName[0] != '\0')
                {
                    m_pSections[m_iSectionCount]->SetAudioTrackName( cAudioName );
                }

                break;
            }
        case LS_GET_ANIMRANGE:
            {
                char cRange[8];
                char cOnOff[8];
                float fRangeStart = 0.0f;
                float fRangeEnd = 0.0f;

                sscanf( cTempLine, "%s %s %f %f", cRange, cOnOff, &fRangeStart, &fRangeEnd );

                m_pSections[m_iSectionCount]->SetUseRange( strcmp( cOnOff, "on" ) == 0 );
                m_pSections[m_iSectionCount]->SetRangeStart( fRangeStart );
                m_pSections[m_iSectionCount]->SetRangeEnd( fRangeEnd );

                break;
            }

            //
            // text for the scene
            //
        case LS_GET_TEXT:
            {		
                s32 begin,length;
                char cName[TEXT_KEY_SIZE];

                sscanf( cTempLine, "%d %d %s", &begin, &length, cName );

                STextIdDetails *pSubtitle = rage_new STextIdDetails;

                cutfAssertf( (int)strlen(cName) < TEXT_KEY_SIZE,  "text id must be less than %d chars", TEXT_KEY_SIZE );
                safecpy( pSubtitle->m_cTextOutput, cName, TEXT_KEY_SIZE );

                pSubtitle->m_iTextStartTime = begin;
                pSubtitle->m_iTextDuration = length;
                pSubtitle->iManualSectionCreatedIn = m_iSectionCount;

                AddSubtitle( pSubtitle );

                break;
            }

            //
            // weather settings for this section
            //
            /*			case LS_GET_WEATHER:
            {	
            u32 t_iType;

            sscanf(cTempLine,"%d", &t_iType);

            // might be able to simplify this...
            m_pSections[m_iSectionCount]->iWeather = t_iType;
            break;
            }
            */
            //
            // lighting for this section
            //
        case LS_GET_LIGHTS:
            {	
                char cLightName[CUTSCENE_OBJNAMELEN];
                sscanf(cTempLine,"%s", cLightName);

                cutfAssertf( (int)strlen(cLightName) < CUTSCENE_OBJNAMELEN, 
                     "name of light animation must be less than %d chars", CUTSCENE_OBJNAMELEN );

                SLightDetails light;

                safecpy( light.cLightName, cLightName, sizeof( light.cLightName ) );
                
                m_pSections[m_iSectionCount]->AddLight( light );
                break;
            }

            //
            // blocking bounds
            //
        case LS_GET_BLOCKING_BOUNDS:
            {	
                Vector3 vCorners[4];
                float	fHeight;
                sscanf(cTempLine,"%f %f %f %f %f %f %f %f %f %f %f %f %f",  &vCorners[0].x,&vCorners[0].y,&vCorners[0].z,
                    &vCorners[1].x,&vCorners[1].y,&vCorners[1].z,
                    &vCorners[2].x,&vCorners[2].y,&vCorners[2].z,
                    &vCorners[3].x,&vCorners[3].y,&vCorners[3].z,
                    &fHeight);

                SBlockDetails *pBlockingBounds = rage_new SBlockDetails;

                pBlockingBounds->vCorners[0] = vCorners[0];
                pBlockingBounds->vCorners[1] = vCorners[1];
                pBlockingBounds->vCorners[2] = vCorners[2];
                pBlockingBounds->vCorners[3] = vCorners[3];
                pBlockingBounds->fHeight = fHeight;

                AddBlockingBounds( pBlockingBounds );

                break;
            }

            //
            // Draw Distance:
            //
        case LS_GET_CAMCORDER:
            {	
                s32 iStartTime = 0;
                s32 iEndTime = 0;

                sscanf( cTempLine, "%d %d", &iStartTime, &iEndTime );

                SCamcorderOverlay *pCamcorderOverlay = rage_new SCamcorderOverlay;

                pCamcorderOverlay->iStartTime = iStartTime;
                pCamcorderOverlay->iEndTime = iEndTime;

                AddCamcorderOverlay( pCamcorderOverlay );
                break;
            }

            //
            // Draw Distance:
            //
        case LS_GET_DRAW_DISTANCE:
            {	
                s32 iStartTime = 0;
                s32 iEndTime = 0;
                float fNearClip = -1.0f;
                float fFarClip = -1.0f;

                sscanf(cTempLine,"%d %d %f %f", &iStartTime, &iEndTime, &fNearClip, &fFarClip);

                SDrawDistanceDetails *pDrawDistance = rage_new SDrawDistanceDetails;

                pDrawDistance->iStartTime = iStartTime;
                // pDrawDistance->iEndTime = iEndTime;

                // adjust for old format: if we have only one float value, that is the old fDistance
                if ( fNearClip != -1.0f )
                {                    
                    if ( fFarClip != -1.0f )
                    {
                        pDrawDistance->fNearClip = fNearClip;
                        pDrawDistance->fFarClip = fFarClip;
                    }
                    else
                    {
                        pDrawDistance->fNearClip = CUTSCENE_DEFAULT_NEAR_CLIP;
                        pDrawDistance->fFarClip = fNearClip;
                    }
                }
                else
                {
                    pDrawDistance->fNearClip = CUTSCENE_DEFAULT_NEAR_CLIP;
                    pDrawDistance->fFarClip = CUTSCENE_DEFAULT_FAR_CLIP;
                }

                AddDrawDistance( pDrawDistance );

                break;
            }

            //
            // effects
            //
        case LS_GET_EFFECTS:
            {	
                s32 j;
                char cName[255];
                //	char cAudioName[CUTSCENE_EFFECTAUDIONAMELEN];
                s32 iStartTime = 0;
                s32 iEndTime = 0;
                Vector3 vPos(-99.0f, -99.0f, -99.0f);
                Vector3 vDir(-99.0f, -99.0f, -99.0f);
                float vDirW = 0.0f;
                s32 iBoneTag = -1;
                s32 iAttachmentId = 0;

                char cEvoName[MAX_EVOLUTION_EFFECT_VALUES][20];
                float fEvoValue[MAX_EVOLUTION_EFFECT_VALUES];

                for (j = 0; j < MAX_EVOLUTION_EFFECT_VALUES; j++)
                {
                    cEvoName[j][0] = '\0';
                    fEvoValue[j] = 0.0f;
                }

                //cAudioName[0] = '\0';

                sscanf( cTempLine,"%s %d %d %d %d %f %f %f %f %f %f %f %s %f %s %f %s %f %s %f", cName, &iAttachmentId, &iBoneTag, &iStartTime, &iEndTime, &vPos.x, &vPos.y, &vPos.z, &vDir.x, &vDir.y, &vDir.z, &vDirW, cEvoName[0], &fEvoValue[0], cEvoName[1], &fEvoValue[1], cEvoName[2], &fEvoValue[2], cEvoName[3], &fEvoValue[3] );

                // make sure the names are in lowercase:

                SEffectsDetails *pEffect = rage_new SEffectsDetails;

                pEffect->SectionCreated = m_iSectionCount;  // store the current section to use the effect                

                for (s32 iSectionCount = 0; iSectionCount < MAX_CUTSCENE_SECTIONS; iSectionCount++)
                {
                    pEffect->bActiveInThisSection[iSectionCount] = false;
                }

                if (iStartTime != iEndTime)
                {
                    // it is a start/stop anim:
                    pEffect->iType = CUTSCENE_PARTICLE_TYPE_ACTIVE;
                }
                else
                {
                    pEffect->iType = CUTSCENE_PARTICLE_TYPE_TRIGGER;
                }

                cutfAssertf( (int)strlen( cName ) < CUTSCENE_EFFECTNAMELEN * 2, 
                     "original effect name must be less than %d chars", CUTSCENE_EFFECTNAMELEN * 2 );
                safecpy( pEffect->cOriginalEffectName, cName, CUTSCENE_EFFECTNAMELEN * 2 );

                // copy the name of the effect over (getting rid of the 'fx_' at the start):
                for (s32 iCharCount = 3; iCharCount <= (s32)(int)strlen(cName); iCharCount++)
                {
                    if (cName[iCharCount] == ':')
                    {
                        pEffect->cEffectName[iCharCount-3] = '\0';

                        if ( pEffect->iType == CUTSCENE_PARTICLE_TYPE_ACTIVE )
                        {
                            // store the anim name:
                            for (s32 iCharCount2 = iCharCount+1; iCharCount2 <= (s32)(int)strlen(cName); iCharCount2++)
                            {
                                pEffect->cEffectAnim[iCharCount2-(iCharCount+1)] = cName[iCharCount2];
                            }

                            // we need to remove the section number here as we will append it later on
                            for (s32 iCharCount2 = (s32)(int)strlen(pEffect->cEffectAnim); iCharCount2 > 0; iCharCount2--)
                            {
                                if ( pEffect->cEffectAnim[iCharCount2] == '_' )
                                {
                                    pEffect->cEffectAnim[iCharCount2] = '\0';
                                }
                            }
                        }
                        else
                        {
                            pEffect->cEffectAnim[0] = '\0';
                        }

                        break;
                    }

                    pEffect->cEffectName[iCharCount-3] = cName[iCharCount];
                }

                // strcpy( pEffect->cEffectAudio, cAudioName );

                cutfAssertf( (int)strlen(pEffect->cEffectName) < CUTSCENE_EFFECTNAMELEN, 
                     "effect name must be less than %d chars", CUTSCENE_EFFECTNAMELEN );
                cutfAssertf( (int)strlen(pEffect->cEffectAnim) < CUTSCENE_EFFECTNAMELEN, 
                     "name of effect animation must be less than %d chars", CUTSCENE_EFFECTNAMELEN );
                // cutfAssertf( (int)strlen(pEffect->cEffectAudio) < CUTSCENE_EFFECTAUDIONAMELEN, 
                //	"name of effect audio must be less than %d chars", CUTSCENE_EFFECTAUDIONAMELEN );

                pEffect->iAttachedTo = iAttachmentId;  // model it is attached to
                pEffect->iBoneTag = iBoneTag;  // bone tag
                pEffect->iStartTime = iStartTime;  // start time
                pEffect->iEndTime = iEndTime;  // end time
                pEffect->vPos = vPos;  // position
                pEffect->vDir = vDir;  // direction
                pEffect->iEffectId = 0;  // negative number until we get an id

                // evolution effect properties:
                for (j = 0; j < MAX_EVOLUTION_EFFECT_VALUES; j++)
                {
                    AssertMsg( (int)strlen(cEvoName[j]) < 20, 
                        "Evolution name for an effect has too many characters (max is 20)!" );
                    AssertMsg( (fEvoValue[j] >= 0.0f) && (fEvoValue[j] <= 1.0f), 
                        "Evolution effect has a value out of range 0.0 to 1.0" );

                    safecpy( pEffect->cEvoName[j], cEvoName[j], 20 );
                    pEffect->fEvoValue[j] = fEvoValue[j];
                }

                AddEffect( pEffect );
                break;
            }
        }  // end of switch
    }  // end of while

    m_bIsLoaded = true;

#if __DEV
    Displayf("[CUTFILE] Data fully parsed." );
#endif

    return true;
}

bool cutfCutsceneFile::ParseXml( parTree* pTree )
{
#if __DEV
    cutfDisplayf( "About to parse xml." );
#endif

    Clear();

    bool result = PARSER.LoadObject( pTree->GetRoot(), *this );
    if ( result )
    {
        m_bIsLoaded = true;

#if __DEV
        Displayf("[CUTFILE] Xml fully parsed." );
#endif
    }

    return result;
}

/////////////////////////////////////////////////////////////////////////////////////
// NAME:	CCutsceneManager::GetNextLine()
// PURPOSE:	Reads a lines worth of text from a memory pointer and returns an advanced pointer
/////////////////////////////////////////////////////////////////////////////////////
const char* cutfCutsceneFile::GetNextLine( const char* p_cSource, char* p_cTarget, u32 uMax )
{
    //move the source onto the first proper character
    while(*p_cSource == '\n' || *p_cSource == '\r')
    {
        p_cSource++;
    }

    //if the first character is null, return null
    if(*p_cSource == '\0')
    {
        return NULL;
    }

    //copy over characters until we get a line end char
    while(*p_cSource != '\n' && *p_cSource != '\r' && *p_cSource != '\0')
    {
        *p_cTarget = *p_cSource;

        uMax--;

        AssertMsg( uMax > 0, "getnextline buffer too small" );

        p_cTarget++;
        p_cSource++;
    }

    *p_cTarget = '\0';

    return p_cSource;
}

void cutfCutsceneFile::AddSection( cutfCutsceneSection* pSection )
{
    if ( cutfVerifyf( m_iSectionCount < MAX_CUTSCENE_SECTIONS,  "Max number of sections (%d) hit!", MAX_CUTSCENE_SECTIONS ) )
    {
        m_pSections[m_iSectionCount] = pSection;
        ++m_iSectionCount;
    }
    else
    {
        m_pSections[m_iSectionCount - 1] = pSection;
    }
}

void cutfCutsceneFile::AddDrawDistance( SDrawDistanceDetails* pDrawDistance )
{
    if ( cutfVerifyf( m_iDrawDistanceCount < MAX_DRAW_DISTANCES,  "Max number of draw distances (%d) hit!", MAX_DRAW_DISTANCES ) )
    {
        m_pDrawDistances[m_iDrawDistanceCount] = pDrawDistance;
        ++m_iDrawDistanceCount;
    }
    else
    {
        m_pDrawDistances[m_iDrawDistanceCount - 1] = pDrawDistance;
    }
}

void cutfCutsceneFile::AddEffect( SEffectsDetails* pEffect )
{
    if ( cutfVerifyf( m_iEffectCount < MAX_CUTSCENE_EFFECTS,  "Max number of effects (%d) hit!", MAX_CUTSCENE_EFFECTS ) )
    {
        m_pEffects[m_iEffectCount] = pEffect;
        ++m_iEffectCount;
    }
    else
    {
        m_pEffects[m_iEffectCount - 1] = pEffect;
    }
}

void cutfCutsceneFile::AddBlockingBounds( SBlockDetails* pBlockingBounds )
{
    if ( cutfVerifyf( m_iBlockingBoundCount < MAX_CUTSCENE_BLOCKING_BOUNDS,  "Max number of blocking bounds (%d) hit!", MAX_CUTSCENE_BLOCKING_BOUNDS ) )
    {
        m_pBlockingBounds[m_iBlockingBoundCount] = pBlockingBounds;
        ++m_iBlockingBoundCount;
    }
    else
    {
        m_pBlockingBounds[m_iBlockingBoundCount - 1] = pBlockingBounds;
    }
}

void cutfCutsceneFile::AddSubtitle( STextIdDetails* pSubtitle )
{
    if ( cutfVerifyf( m_iSubtitleCount < MAX_TEXTOUTPUT,  "Max number of subtitles (%d) hit!", MAX_TEXTOUTPUT ) )
    {
        m_pSubtitles[m_iSubtitleCount] = pSubtitle;
        ++m_iSubtitleCount;
    }
    else
    {
        m_pSubtitles[m_iSubtitleCount - 1] = pSubtitle;
    }
}

void cutfCutsceneFile::ClearSubtitles()
{
    for (int i = 0; i < MAX_TEXTOUTPUT; i++)  
    {
        if ( m_bIsLoaded && (m_pSubtitles[i] != NULL) )
        {
            delete m_pSubtitles[i];
        }

        m_pSubtitles[i] = NULL;
    }

    m_iSubtitleCount = 0;
}

void cutfCutsceneFile::AddCamcorderOverlay( SCamcorderOverlay* pCamcorderOverlay )
{
    if ( cutfVerifyf( m_iCamcorderOverlayCount < MAX_CAMCORDER_OVERLAYS,  "Max number of camcorder overlays (%d) hit!", MAX_CAMCORDER_OVERLAYS ) )
    {
        m_pCamcorderOverlays[m_iCamcorderOverlayCount] = pCamcorderOverlay;
        ++m_iCamcorderOverlayCount;
    }
    else
    {
        m_pCamcorderOverlays[m_iCamcorderOverlayCount - 1] = pCamcorderOverlay;
    }
}

void cutfCutsceneFile::AddFixupObject( SFixupObjectDetails* pFixupObject )
{
    if ( cutfVerifyf( m_iFixupObjectCount < MAX_FIXUP_OBJS,  "Max number of fixup objects (%d) hit!", MAX_FIXUP_OBJS ) )
    {
        m_pFixupObjects[m_iFixupObjectCount] = pFixupObject;
        ++m_iFixupObjectCount;
    }
    else
    {
        m_pFixupObjects[m_iFixupObjectCount - 1] = pFixupObject;
    }
}

void cutfCutsceneFile::AddVariation( SVariationDetails* pVariation )
{
    if ( cutfVerifyf( m_iVariationCount < MAX_CUTSCENE_VARIATIONS,  "Max number of variations (%d) hit!", MAX_CUTSCENE_VARIATIONS ) )
    {
        m_pVariations[m_iVariationCount] = pVariation;
        ++m_iVariationCount;
    }
    else
    {
        m_pVariations[m_iVariationCount - 1] = pVariation;
    }
}

void cutfCutsceneFile::AddProp( SPropDetails* pProp )
{
    if ( cutfVerifyf( m_iPropCount < MAX_CUTSCENE_PROPS,  "Max number of props (%d) hit!", MAX_CUTSCENE_PROPS ) )
    {
        m_pProps[m_iPropCount] = pProp;
        ++m_iPropCount;
    }
    else
    {
        m_pProps[m_iPropCount - 1] = pProp;
    }
}

void rage::SDrawDistanceDetails::OnPostSetCallback( parTreeNode* /*pTreeNode*/, parMember* pMember, void* pInstance )
{
	SDrawDistanceDetails *pDrawDistance = static_cast<SDrawDistanceDetails *>( pInstance );
	if ( (pMember->GetNameHash() == atLiteralStringHash("fDistance")) ||
		(pMember->GetNameHash() == atLiteralStringHash("fFarClip") && pDrawDistance->bDistanceWasSet) )
	{
		pDrawDistance->bDistanceWasSet = true;
		pDrawDistance->fFarClip = pDrawDistance->fDistance;
	}
}