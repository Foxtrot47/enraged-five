// 
// cutfile/cutfile.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTFILE_CUTFILE_H
#define CUTFILE_CUTFILE_H

#include "cutfdefines.h"

#include "cutfchannel.h"

#include "parser/macros.h"
#include "string/stringhash.h"
#include "vector/color32.h"
#include "vector/vector3.h"

namespace rage
{
class parTree;
class parTreeNode;
class parMember;

// types of trigger effects
enum
{
    CUTSCENE_PARTICLE_TYPE_ACTIVE,
    CUTSCENE_PARTICLE_TYPE_TRIGGER
};

// types of attachment ids
enum
{
    CUTS_ATTACH_CHILDID = 0,
    CUTS_ATTACH_BONEID,
    MAX_CUTSCENE_ATTACH_IDS
};

//
enum eModelInformation
{
    OVERRIDE_ARM_ROLLBONES = (1<<0)
};

// holds the text id details:
struct SCamcorderOverlay
{
    SCamcorderOverlay()
        : iStartTime( 0 )
        , iEndTime( 0 )
    {

    }

    s32 iStartTime;
    s32 iEndTime;

    PAR_SIMPLE_PARSABLE;
};

// holds the text id details:
struct STextIdDetails
{
    STextIdDetails()
        : m_iTextStartTime( 0 )
        , m_iTextDuration( 0 )
        , iManualSectionCreatedIn( 0 )
    {
        m_cTextOutput[0] = 0;
    }

    s32 m_iTextStartTime;
    s32 m_iTextDuration;
    char m_cTextOutput[TEXT_KEY_SIZE];
    s32 iManualSectionCreatedIn;

    PAR_SIMPLE_PARSABLE;
};

// holds the prop details:
struct SPropDetails
{
    SPropDetails()
        : parent( 0 )
        , prop( 0 )
        , anchor( 0 )
        , iSectionCreatedIn( 0 )
    {

    }

    s32 parent;
    s32 prop;
    s32 anchor;
    s32 iSectionCreatedIn;

    PAR_SIMPLE_PARSABLE;
};

// holds the attachment details:
struct SAttachmentDetails
{
    SAttachmentDetails()
        : parent( 0 )
        , child( 0 )
        , boneid( 0 )
    {

    }

    s32 parent;
    s32 child;
    s32 boneid;

    PAR_SIMPLE_PARSABLE;
};

// holds variation details:
struct SVariationDetails
{
    SVariationDetails()
        : iId( 0 )
        , iComponentId( 0 )
        , iDrawableId( 0 )
        , iTextureId( 0 )
        , iStartTime( 0 )
        , iSectionCreatedIn( 0 )
        , bIsSet( false )
    {

    }

    s32 iId;
    s32 iComponentId;
    s32 iDrawableId;
    s32 iTextureId;

    s32 iStartTime;
    s32 iSectionCreatedIn;

    bool bIsSet;

    PAR_SIMPLE_PARSABLE;
};

// holds model details:
struct SModelDetails
{
    SModelDetails()
        : iId( 0 )
        , iFlags( 0 )
        , bLoaded( false )
        , bEmpty( true )
    {
        cModelName[0] = cAnimName[0] = cAnimName2[0] = cCompression[0] = 0;
    }

    s32 iId;
    char cModelName[CUTSCENE_OBJNAMELEN];   // NOTE: runtime needs to strlwr
    char cAnimName[CUTSCENE_OBJNAMELEN];    // NOTE: runtime needs to strlwr
    char cAnimName2[CUTSCENE_OBJNAMELEN];   // NOTE: runtime needs to strlwr
    s32 iFlags;
    char cCompression[CUTSCENE_OBJNAMELEN];
    bool bLoaded;
    bool bEmpty;

    PAR_SIMPLE_PARSABLE;
};

// holds vehicle details:
struct SVehicleDetails
{
    SVehicleDetails()
        : iId( 0 )
        , iDirtLevel( 0 )
    {
        iColour.Set( 0, 0, 0, 0 );
    }

    s32 iId;
    Color32 iColour;
    u8 iDirtLevel;
    s8 iTexture;

    PAR_SIMPLE_PARSABLE;
};

// holds vehicle details:
struct SVehicleRemoval
{
    SVehicleRemoval()
        : iId( 0 )
        , iBone( 0 )
    {

    }

    s32 iId;
    s32 iBone;

    PAR_SIMPLE_PARSABLE;
};

// holds FX details:
struct SEffectsDetails
{
    SEffectsDetails();

    s32 iType;
    s32 SectionCreated; // number of the section that this effect is created inside
        
    bool bActiveInThisSection[MAX_CUTSCENE_SECTIONS];  // store anim for each section so we can stream one in whilst another is playing
    s32 iAttachedTo;
    s32 iStartTime;
    s32 iEndTime;
    s32 iBoneTag;
    Vector3 vPos;
    Vector3 vDir;
    s32 iEffectId;
    char cOriginalEffectName[CUTSCENE_EFFECTNAMELEN * 2];
    char cEffectName[CUTSCENE_EFFECTNAMELEN];   // NOTE: runtime needs to strlwr
    char cEffectAnim[CUTSCENE_EFFECTNAMELEN];   // NOTE: runtime needs to strlwr
    //char cEffectAudio[CUTSCENE_EFFECTAUDIONAMELEN];   // NOTE: runtime needs to strlwr

    char cEvoName[MAX_EVOLUTION_EFFECT_VALUES][20]; // NOTE: runtime needs to strlwr
    float fEvoValue[MAX_EVOLUTION_EFFECT_VALUES];

    PAR_SIMPLE_PARSABLE;
};

struct SDrawDistanceDetails
{
    SDrawDistanceDetails()
        : iStartTime( 0 )
        , fNearClip( CUTSCENE_DEFAULT_NEAR_CLIP )
        , fFarClip( CUTSCENE_DEFAULT_FAR_CLIP )
    {

    }

    s32 iStartTime;
    //	s32 iEndTime;
    float fNearClip;
    float fFarClip;

    PAR_SIMPLE_PARSABLE;

private:
    // PURPOSE: Fixup the fDistance (deprecated) to be the new fFarCip
    // PARAMS:
    //    pTreeNode - the tree node containing the data
    //    pMember - the member that was just set
    //    pInstance - the instance of SDrawDistanceDetails
    static void OnPostSetCallback( parTreeNode* /*pTreeNode*/, parMember* pMember, void* pInstance );

    float fDistance;    // kept for backwards compatibility
    bool bDistanceWasSet;
};

// holds ped-block details:
struct SBlockDetails
{
    SBlockDetails()
        : fHeight( 0.0f )
    {
        for (int i = 0; i < 4; ++i) { vCorners[i].Zero(); }
    }

    Vector3 vCorners[4];	// the 4 extremes of the blocking box
    float	fHeight;	// the height of the blocking box

    PAR_SIMPLE_PARSABLE;
};

// holds light details:
struct SLightDetails
{
    SLightDetails()
    {
        cLightName[0] = 0;
    }

    char cLightName[CUTSCENE_OBJNAMELEN];   // NOTE: runtime needs to strlwr

    PAR_SIMPLE_PARSABLE;
};

// structure to hold the hidden objects:
struct SHiddenObjectDetails
{
    SHiddenObjectDetails()
        : fRadius( 0.0f )
    {
        vPosition.Zero();
        cObjectName[0] = 0;
    }

    Vector3 vPosition;
    char cObjectName[CUTSCENE_OBJNAMELEN];  // NOTE: runtime needs to strlwr
    float fRadius;
    // CEntity* pHiddenEntity;

    PAR_SIMPLE_PARSABLE;
};

// structure to hold the fixup objects:
struct SFixupObjectDetails
{
    SFixupObjectDetails()
        : fRadius( 0.0f )
    {
        vPosition.Zero();
        cObjectName[0] = 0;
    }

    Vector3 vPosition;
    char cObjectName[CUTSCENE_OBJNAMELEN];  // NOTE: runtime needs to strlwr
    float fRadius;

    PAR_SIMPLE_PARSABLE;
};

// for storing the original game settings so we can revert back at end of scene
/*struct SOriginalSettings
{
s32 iOriginalHours;
s32 iOriginalMinutes;
s32 iOriginalWeather;
};*/

//
// SECTION CLASS:
//
class cutfCutsceneSection
{
public:
    cutfCutsceneSection();

    // PURPOSE: Retrieve the world translation offset where the cut scene section takes place.
    // RETURNS: The world translation offset Vector3
    Vector3& GetOffset();

    // PURPOSE: Set the world translation offset where the cut scene section takes place.
    // PARAMS:
    //    offset - the world translation vector
    void SetOffset( const Vector3 &offset );

    // PURPOSE: Retrieve the correction rotation of this cut scene section.
    // RETURNS: The degrees rotation
    float GetRotation();
    
    // PURPOSE: Set the correction rotation of this cut scene section.
    // PARAMS:
    //    fRotation - the degrees rotation
    void SetRotation( float fRotation );

    // PURPOSE: Retrieve the length of this section
    // RETURNS: The length in milliseconds
    float GetDuration() const;

    // PURPOSE: Set the length of this section
    // PARAMS:
    //    fDuration - the length in milliseconds
    void SetDuration( float fDuration );

    // PURPOSE: Retrieve the length of this section
    // RETURNS: The length in seconds
    float GetDurationSeconds() const;

    // PURPOSE: Retrieve a model at the given index.
    // PARAMS:
    //    iModelIndex - the index of the model
    // RETURNS:  Model data
    // NOTES: This function will assert if iModelIndex is out of range.
    SModelDetails& GetModel( s32 iModelIndex );

    // PURPOSE: Adds a model to this section.
    // PARAMS: 
    //    model - the model to add (makes a copy)
    // NOTES: This function will assert if we have too many models.
    void AddModel( const SModelDetails &model );

    // PURPOSE: Retrieves the number of models that have been added to this section.
    // RETURNS: The count of models.
    s32 GetModelCount() const;

    // PURPOSE: Retrieves a vehicle at the given index.
    // PARAMS:
    //    iVehicleIndex - the index of the vehicle
    // RETURNS: Vehicle data
    // NOTES: This function will assert if iVehicleIndex is out of range.
    SVehicleDetails& GetVehicle( s32 iVehicleIndex );

    // PURPOSE: Adds a vehicle to this section.
    // PARAMS:
    //     vehicle - the vehicle to add (makes a copy)
    // NOTES: This function will assert if we have too many vehicles.
    void AddVehicle( const SVehicleDetails &vehicle );

    // PURPOSE: Retrieves the number of vehicles that have been added to this section.
    // RETURNS: The count of vehicles.
    s32 GetVehicleCount() const;

    // PURPOSE: Retrieves a vehicle removal at the given index.
    // PARAMS:
    //    iVehicleRemovalIndex - the index of the vehicle removal
    // RETURNS: Vehicle removal data
    // NOTES: This function will assert if iVehicleRemovalIndex is out of range.
    SVehicleRemoval& GetVehicleRemoval( s32 iVehicleRemovalIndex );

    // PURPOSE: Adds a vehicle removal to this section.
    // PARAMS:
    //     vehicleRemoval - the vehicle removal to add (makes a copy)
    // NOTES: This function will assert if we have too many vehicle removals.
    void AddVehicleRemoval( const SVehicleRemoval &vehicleRemoval );

    // PURPOSE: Retrieves the number of vehicle removals that have been added to this section.
    // RETURNS: The count of vehicle removals.
    s32 GetVehicleRemovalCount() const;

    // PURPOSE: Retrieves an attachment at the given index.
    // PARAMS:
    //    iAttachmentIndex - the index of the attachment
    // RETURNS: Attachment data
    // NOTES: This function will assert if iAttachmentIndex is out of range.
    SAttachmentDetails& GetAttachment( s32 iAttachmentIndex );
    
    // PURPOSE: Adds an attachment to this section.
    // PARAMS:
    //     attachment - the attachment to add (makes a copy)
    // NOTES: This function will assert if we have too many attachments.
    void AddAttachment( const SAttachmentDetails &attachment );

    // PURPOSE: Retrieves the number of attachments that have been added to this section.
    // RETURNS: The count of attachments.
    s32 GetAttachmentCount() const;

    // PURPOSE: Retrieves a hidden object at the given index.
    // PARAMS:
    //    iHiddenObjectIndex - the index of the hidden object
    // RETURNS: Hidden object data
    // NOTES: This function will assert if iHiddenObjectIndex is out of range.
    SHiddenObjectDetails& GetHiddenObject( s32 iHiddenObjectIndex );

    // PURPOSE: Adds a hidden object to this section.
    // PARAMS:
    //     hiddenObject - the hidden object to add (makes a copy)
    // NOTES: This function will assert if we have too many hidden objects.
    void AddHiddenObject( const SHiddenObjectDetails &hiddenObject );

    // PURPOSE: Retrieves the number of hidden objects that have been added to this section.
    // RETURNS: The count of hidden objects.
    s32 GetHiddenObjectCount() const;

    // PURPOSE: Retrieves a light at the given index.
    // PARAMS:
    //    iLightIndex - the index of the light
    // RETURNS: Light data
    // NOTES: This function will assert if iLightIndex is out of range.
    SLightDetails& GetLight( s32 iLightIndex );

    // PURPOSE: Adds a light to this section.
    // PARAMS:
    //     light - the light to add (makes a copy)
    // NOTES: This function will assert if we have too many lights.
    void AddLight( const SLightDetails &light );
    
    // PURPOSE: Retrieves the number of lights that have been added to this section.
    // RETURNS: The count of lights.
    s32 GetLightCount() const;
    
    // PURPOSE: Retrieves the name of the audio track for this section
    // RETURNS: The audio track name
    // NOTES:  The run-time may need to strlwr
    const char* GetAudioTrackName() const;

    // PURPOSE: Sets the audio track name for this section.
    // PARAMS:
    //    pAudioTrackName - the name of the audio track
    void SetAudioTrackName( const char* pAudioTrackName );

    // PURPOSE: Retrieves the name of the camera animation for this section
    // RETURNS: The camera animation name
    // NOTES:  The run-time may need to strlwr
    const char* GetCameraAnimName() const;

    // PURPOSE: Sets the camera animation name for this section.
    // PARAMS:
    //    pCameraName - the name of the audio track
    void SetCameraAnimName( const char* pCameraName );
    
    // PURPOSE: Retrieves the name of the animation dictionary for this section
    // RETURNS: The animation dictionary name
    // NOTES:  The run-time may need to strlwr
    const char* GetClipDictName() const;

    // PURPOSE: Sets the animation dictionary name for this section.
    // PARAMS:
    //    pAnimDictName - the name of the animation dictionary
    void SetAnimDictName( const char* pAnimDictName );

    // PURPOSE: Retrieves the index of the section to use when play back is manual.
    // RETURNS: The manual section to use.
    s32 GetManualSectionToUse() const;

    // PURPOSE: Sets the section to use when play back is manual.
    // PARAMS:
    //    iManualSection - the manual section to use.
    void SetManualSectionToUse( s32 iManualSection );

    // PURPOSE: Retrieves the index of the section to use for audio when play back is manual.
    // RETURNS: The manual section to use.
    s32 GetManualSectionToUseForAudio() const;

    // PURPOSE: Sets the section to use for audio when play back is manual.
    // PARAMS:
    //    iManualSectionAudio - the manual section to use.
    void SetManualSectionToUseForAudio( s32 iManualSectionAudio );

    // PURPOSE: Whether to use the manual section number.
    // RETURNS: true when the manual section number should be used
    bool IsManualSection() const;

    // PURPOSE: Sets whether to use the manual section number.
    // PARAMS:
    //    bManual - true to use the manual section number
    void SetIsManualSection( bool bManual );

    // PURPOSE: Retrieves whether the DCC should use the Start and End frames provided for export, or to use the span of the time line.
    // RETURNS: true to use the provided start and end frames
    bool UseRange() const;

    // PURPOSE: Sets whether the DCC should use the Start and End frames for export
    // PARAMS:
    //    bUseRange - true to use the provided start and end frames
    void SetUseRange( bool bUseRange );

    // PURPOSE: Retrieves the starting frame number for export from the DCC package.
    // RETURNS: The starting frame
    float GetRangeStart() const;

    // PURPOSE: Sets the starting frame number for export from the DCC package.
    // PARAMS:
    //    fRangeStart - the starting frame
    void SetRangeStart( float fRangeStart );

    // PURPOSE: Retrieves the ending frame number for export from the DCC package.
    // RETURNS: The ending frame
    float GetRangeEnd() const;

    // PURPOSE: Sets the ending frame number for export from the DCC package.
    // PARAMS:
    //    fRangeEnd - the ending frame
    void SetRangeEnd( float fRangeEnd );

    // PURPOSE: Retrieves the size of the model at the given index.
    // PARAMS:
    //    iModelIndex - the model index to get the size of
    // RETURNS: The size of the model at the given index.
    u32 GetModelSize( s32 iModelIndex ) const;

    // PURPOSE: Sets the size of the model at the given index.
    // PARAMS:
    //    iModelIndex - the index of the model
    //    iSize - the size of the model
    void SetModelSize( s32 iModelIndex, u32 iSize );

    // PURPOSE: Retrieves the static size of this section.
    // RETURNS: The static size of this section.
    u32 GetStaticSize() const;

    // PURPOSE: Sets the static size of this section.
    // PARAMS:
    //    iSize - the size
    void SetStaticSize( u32 iSize );

    // PURPOSE: Retrieves the frame offset of this section.  Basically, this is the start frame of the entire cut scene.
    // RETURNS: The frame offset
    // NOTES:  Used mainly for debugging and previewing purposes to help match run-time time codes with the scene in the DCC package.
    s32 GetFrameOffset() const;

    // PURPOSE: Sets the frame offset of this section.
    // PARAMS:
    //    iOffset - the frame offset
    void SetFrameOffset( s32 iOffset );

    PAR_SIMPLE_PARSABLE;

private:
    Vector3 m_vOffset;	// section offset
    float m_fRotation;  // section orientation

    float m_fDuration;	// duration of section - next section will start after this one
    // the next sections' data will stream in before this time also

    // Not used
    // s32 iWeather;

    // For the TIME section
    // s32 iHours;
    // s32 iMinutes;

    SModelDetails m_models[MAX_CUTSCENE_ENTITIES];
    SVehicleDetails m_vehicles[MAX_CUTSCENE_VEHICLES];
    SVehicleRemoval m_vehicleRemovals[MAX_CUTSCENE_VEHICLE_REMOVALS];
    SAttachmentDetails m_attachments[MAX_CUTSCENE_ATTACHMENTS];
    SHiddenObjectDetails m_hiddenObjects[MAX_HIDDEN_OBJS];
    SLightDetails m_lights[MAX_CUTSCENE_LIGHTS];

    char m_cAudioTrackName[CUTSCENE_OBJNAMELEN];
    char m_cCameraAnimName[CUTSCENE_OBJNAMELEN];
    char m_cAnimDictName[CUTSCENE_OBJNAMELEN];

    s32 m_iModelCount;
    s32 m_iVehicleCount;
    s32 m_iVehicleRemovalCount;
    s32 m_iAttachmentCount;
    s32 m_iHiddenObjectCount;
    s32 m_iLightCount;

    s32 m_iManualSectionToUse;
    s32 m_iManualSectionToUseForAudio;
    bool m_bManualSection;

    bool m_bUseRange;
    float m_fRangeStart;
    float m_fRangeEnd;

    u32 m_iModelSize[MAX_CUTSCENE_ENTITIES];
    u32 m_iStaticSize;
    s32 m_iFrameOffset;
};

inline Vector3& cutfCutsceneSection::GetOffset()
{
    return m_vOffset;
}

inline void cutfCutsceneSection::SetOffset( const Vector3 &offset )
{
    m_vOffset = offset;
}

inline float cutfCutsceneSection::GetRotation()
{
    return m_fRotation;
}

inline void cutfCutsceneSection::SetRotation( float fRotation )
{
    m_fRotation = fRotation;
}

inline float cutfCutsceneSection::GetDuration() const
{
    return m_fDuration;
}

inline float cutfCutsceneSection::GetDurationSeconds() const
{
    return m_fDuration / 1000.0f;
}

inline void cutfCutsceneSection::SetDuration( float fDuration )
{
    m_fDuration = fDuration;
}

inline SModelDetails& cutfCutsceneSection::GetModel( s32 iModelIndex )
{
    cutfAssertf( (iModelIndex >= 0) && (iModelIndex < m_iModelCount), "Out of Range" );
    return m_models[iModelIndex];
}

inline s32 cutfCutsceneSection::GetModelCount() const
{
    return m_iModelCount;
}

inline SVehicleDetails& cutfCutsceneSection::GetVehicle( s32 iVehicleIndex )
{
    cutfAssertf( (iVehicleIndex >= 0) && (iVehicleIndex < m_iVehicleCount), "Out of Range" );
    return m_vehicles[iVehicleIndex];
}

inline s32 cutfCutsceneSection::GetVehicleCount() const
{
    return m_iVehicleCount;
}

inline SVehicleRemoval& cutfCutsceneSection::GetVehicleRemoval( s32 iVehicleRemovalIndex )
{
    cutfAssertf( (iVehicleRemovalIndex >= 0) && (iVehicleRemovalIndex < m_iVehicleRemovalCount), "Out of Range" );
    return m_vehicleRemovals[iVehicleRemovalIndex];
}

inline s32 cutfCutsceneSection::GetVehicleRemovalCount() const
{
    return m_iVehicleRemovalCount;
}

inline SAttachmentDetails& cutfCutsceneSection::GetAttachment( s32 iAttachmentIndex )
{
    cutfAssertf( (iAttachmentIndex >= 0) && (iAttachmentIndex < m_iAttachmentCount), "Out of Range" );
    return m_attachments[iAttachmentIndex];
}

inline s32 cutfCutsceneSection::GetAttachmentCount() const
{
    return m_iAttachmentCount;
}

inline SHiddenObjectDetails& cutfCutsceneSection::GetHiddenObject( s32 iHiddenObjectIndex )
{
    cutfAssertf( (iHiddenObjectIndex >= 0) && (iHiddenObjectIndex < m_iHiddenObjectCount), "Out of Range" );
    return m_hiddenObjects[iHiddenObjectIndex];
}

inline s32 cutfCutsceneSection::GetHiddenObjectCount() const
{
    return m_iHiddenObjectCount;
}

inline SLightDetails& cutfCutsceneSection::GetLight( s32 iLightIndex )
{
    cutfAssertf( (iLightIndex >= 0) && (iLightIndex < m_iLightCount), "Out of Range" );
    return m_lights[iLightIndex];
}

inline s32 cutfCutsceneSection::GetLightCount() const
{
    return m_iLightCount;
}

inline const char* cutfCutsceneSection::GetAudioTrackName() const
{
    return m_cAudioTrackName;
}

inline void cutfCutsceneSection::SetAudioTrackName( const char* pAudioTrackName )
{
#if __DEV
    cutfAssertf( strlen(pAudioTrackName) < CUTSCENE_OBJNAMELEN, 
         "CUTSCENE: name of audio track must be less than %d chars", CUTSCENE_OBJNAMELEN );
#endif // __DEV
    safecpy( m_cAudioTrackName, pAudioTrackName, CUTSCENE_OBJNAMELEN );
}

inline const char* cutfCutsceneSection::GetCameraAnimName() const
{
    return m_cCameraAnimName;
}

inline void cutfCutsceneSection::SetCameraAnimName( const char* pAnimName )
{
#if __DEV
    cutfAssertf( strlen(pAnimName) < CUTSCENE_OBJNAMELEN, 
         "CUTSCENE: name of camera animation must be less than %d chars", CUTSCENE_OBJNAMELEN );
#endif // __DEV
    safecpy( m_cCameraAnimName, pAnimName, CUTSCENE_OBJNAMELEN );
}

inline const char* cutfCutsceneSection::GetClipDictName() const
{
    return m_cAnimDictName;
}  

inline void cutfCutsceneSection::SetAnimDictName( const char* pAnimDictName )
{
#if __DEV
    cutfAssertf( strlen(pAnimDictName) < CUTSCENE_OBJNAMELEN, 
         "CUTSCENE: name of cutscene animation dictionary must be less than %d chars", CUTSCENE_OBJNAMELEN );
#endif // __DEV
    safecpy( m_cAnimDictName, pAnimDictName, CUTSCENE_OBJNAMELEN );
}

inline s32 cutfCutsceneSection::GetManualSectionToUse() const
{
    return m_iManualSectionToUse;
}

inline void cutfCutsceneSection::SetManualSectionToUse( s32 iManualSection )
{
    m_iManualSectionToUse = iManualSection;
}

inline s32 cutfCutsceneSection::GetManualSectionToUseForAudio() const
{
    return m_iManualSectionToUseForAudio;
}

inline void cutfCutsceneSection::SetManualSectionToUseForAudio( s32 iManualSectionAudio )
{
    m_iManualSectionToUseForAudio = iManualSectionAudio;
}

inline bool cutfCutsceneSection::IsManualSection() const
{
    return m_bManualSection;
}

inline void cutfCutsceneSection::SetIsManualSection( bool bManual )
{
    m_bManualSection = bManual;
}

inline u32 cutfCutsceneSection::GetModelSize( s32 iModelIndex ) const
{
    cutfAssertf( (iModelIndex >= 0) && (iModelIndex < m_iModelCount), "Out of Range" );
    return m_iModelSize[iModelIndex];
}

inline bool cutfCutsceneSection::UseRange() const
{
    return m_bUseRange;
}

inline void cutfCutsceneSection::SetUseRange( bool bUseRange )
{
    m_bUseRange = bUseRange;
}

inline float cutfCutsceneSection::GetRangeStart() const
{
    return m_fRangeStart;
}

inline void cutfCutsceneSection::SetRangeStart( float fRangeStart )
{
    m_fRangeStart = fRangeStart;
}

inline float cutfCutsceneSection::GetRangeEnd() const
{
    return m_fRangeEnd;
}

inline void cutfCutsceneSection::SetRangeEnd( float fRangeEnd )
{
    m_fRangeEnd = fRangeEnd;
}

inline void cutfCutsceneSection::SetModelSize( s32 iModelIndex, u32 iSize )
{
    cutfAssertf( (iModelIndex >= 0) && (iModelIndex < m_iModelCount), "Out of Range" );
    m_iModelSize[iModelIndex] = iSize;
}

inline u32 cutfCutsceneSection::GetStaticSize() const
{
    return m_iStaticSize;
}

inline void cutfCutsceneSection::SetStaticSize( u32 iSize )
{
    m_iStaticSize = iSize;
}

inline s32 cutfCutsceneSection::GetFrameOffset() const
{
    return m_iFrameOffset;
}

inline void cutfCutsceneSection::SetFrameOffset( s32 iOffset )
{
    m_iFrameOffset = iOffset;
}

//#############################################################################

class cutfCutsceneFile
{
public:
    cutfCutsceneFile();
    ~cutfCutsceneFile();

    // PURPOSE: Clears all data associated with this cut scene file, deleting all allocated structures and classes.
    void Clear();

    // PURPOSE: Save the data to the .cut file.
    // PARAMS:
    //    pFilename - The name of the file to save to.
    //    pExtension - the extension to save as.  
    // RETURNS: true on success, otherwise false.
    // NOTES: Anything with "xml" or "bin" will save using the PARSER.  If no extension is provided, the default PI_CUTSCENE_XMLFILE_EXT is used.
#if __BANK || __TOOL
    bool SaveFile( const char* pFilename, const char* pExtension=NULL ) const;
#endif

    // PURPOSE: Load the data from the .cut file.
    // PARAMS:
    //    pFilename - The name of the file to load.
    //    pExtension - the extension to load as.
    // RETURNS: true on success, otherwise false.
    // NOTES: Anything with "xml" or "bin" will load using the PARSER.  If no extension is provided, the default PI_CUTSCENE_XMLFILE_EXT is used.
    bool LoadFile( const char* pFilename, const char* pExtension=NULL );

    // PURPOSE: Parses the data from the buffer.
    // PARAMS:
    //    pData - the data buffer to parse
    //    size - the length of data
    // RETURNS: true on success, otherwise false
    // NOTES: LoadFile executes this function, which is here for your convenience if you have an alternate method of loading files.
    bool ParseData( const char* pData, u32 size );

    // PURPOSE: Parse the data from the xml tree
    // PARAMS:
    //    pTree - the tree to parse
    // RETURNS: true on success, otherwise false.
    bool ParseXml( parTree *pTree );

    // PURPOSE: Indicates if the current data in this class was loaded from a file.
    // RETURNS: true if a file was loaded successfully
    bool IsLoaded() const;

    // PURPOSE: Retrieves the name of the cut scene.
    // RETURNS: The name of the cut scene.
    const char* GetName() const;

    // PURPOSE: Sets the name of the cut scene.
    // PARAMS:
    //    pName - the name of the cut scene
    void SetName( const char* pName );

    // PURPOSE: Retrieves the hash of the cut scene name.
    // RETURNS: The name hash
    // NOTES:
    s32 GetNameHash() const;

    // PURPOSE: Retrieves the total length of the cut scene.
    // RETURNS: The total length of the cut scene in milliseconds
    float GetTotalDuration() const;

    // PURPOSE: Sets the total length of the cut scene.
    // PARAMS:
    //    fTotalDuration - the total duration in milliseconds
    void SetTotalDuration( float fTotalDuration );

    // PURPOSE: Retrieves the total length of the cut scene.
    // RETURNS: The total length of the cut scene in seconds
    float GetTotalDurationSeconds() const;

    // PURPOSE: Retrieves the Model Id of the player.
    // RETURNS: The player Id
    s32 GetPlayerId() const;

    // PURPOSE: Sets the Model Id of the player.
    // PARAMS:
    //    iPlayerId - the player Id
    void SetPlayerId( s32 iPlayerId );

    // PURPOSE: Retrieves the cut scene flags, which includes settings for the DCC package and special run-time flags.
    // RETURNS: A bit-mask of flags.
    // NOTES:
    s32 GetCutsceneFlags() const;

    // PURPOSE: Sets the cut scene flags.
    // PARAMS:
    //    iCutsceneFlags - A bit-mask to set.
    void SetCutsceneFlags( s32 iCutsceneFlags );

    // PURPOSE: Retrieves the starting frame of the export from the DCC package.
    // RETURNS: The starting frame.
    s32 GetStartFrame() const;

    // PURPOSE: Sets the starting from of the export from the DCC package.
    // PARAMS:
    //    iStartFrame - the starting frame
    void SetStartFrame( s32 iStartFrame );

    // PURPOSE: Retrieves the frame on which a section split occurs for the given index
    // PARAMS:
    //    iSectionSplitFrameIndex - the index to retrieve
    // RETURNS: The frame on which a new section begins.
    s32 GetSectionSplitFrame( int iSectionSplitFrameIndex ) const;

    // PURPOSE: Adds a section split frame.
    // PARAMS:
    //    iSectionSplitFrame - the frame on which a new section begins.
    void AddSectionSplitFrame( int iSectionSplitFrame );

    // PURPOSE: Retrieves the number of splits added.
    // RETURNS: the count of section splits.
    s32 GetSectionSplitFrameCount() const;

    // PURPOSE: Retrieves the ending frame of the export from the DCC package.
    // RETURNS: The ending frame.
    s32 GetEndFrame() const;

    // PURPOSE: Sets the ending from of the export from the DCC package.
    // PARAMS:
    //    iEndFrame - the ending frame
    void SetEndFrame( s32 iEndFrame );

    // PURPOSE: Gets the index of the section that was called during playback.
    // RETURNS: The index of the called section.
    s32 GetSectionCalled() const;

    // PURPOSE: Sets the index of the section that was called during playback
    // PARAMS:
    //    iSectionCalled - the index of the called section
    void SetSectionCalled( s32 iSectionCalled );

    // PURPOSE: Retrieves the position in which the player should start.
    // RETURNS: A vector position.
    const Vector3& GetPlayerStartPosition() const;

    // PURPOSE: Sets teh position in which the player should start.
    // PARAMS:
    //    vPlayerStartPosition - the start position
    void SetPlayerStartPosition( const Vector3& vPlayerStartPosition );

    // PURPOSE: Retrieves the name of the associated mission.
    // RETURNS: the name of the associated mission
    // NOTES: For debugging purposes.
    const char* GetMissionTextName() const;

    // PURPOSE: Sets the name of the associated mission.
    // PARAMS:
    //    pMissionTextName - the mission name
    void SetMissionTextName( const char* pMissionTextName );

    // PURPOSE: Retrieves the section at the the given index.
    // PARAMS:
    //    iSection - the index of the section
    // RETURNS: Cut scene section data
    // NOTES: This function will assert if the iSection is out of range.
    cutfCutsceneSection* GetSection( s32 iSection ) const;
    
    // PURPOSE: Adds a section to this cut scene.
    // PARAMS:
    //    pSection - the section to add.  Clear() will delete the object for you.
    // NOTES: This function will assert if too many sections have been added.
    void AddSection( cutfCutsceneSection* pSection );
    
    // PURPOSE: Retrieves the number of sections that have been added to this cut scene.
    // RETURNS: The count of sections.
    s32 GetSectionCount() const;

    // PURPOSE: Retrieves the draw distance (clipping data) at the the given index.
    // PARAMS:
    //    iDrawDistanceIndex - the index of the draw distance
    // RETURNS: Draw distance data
    // NOTES: This function will assert if the iDrawDistanceIndex is out of range.
    SDrawDistanceDetails* GetDrawDistance( s32 iDrawDistanceIndex ) const;

    // PURPOSE: Adds a draw distance (clipping data) to this cut scene.
    // PARAMS:
    //    pDrawDistance - the draw distance to add.  Clear() will delete the object for you.
    // NOTES: This function will assert if too many draw distances have been added.
    void AddDrawDistance( SDrawDistanceDetails* pDrawDistance );

    // PURPOSE: Retrieves the number of draw distances that have been added to this cut scene.
    // RETURNS: The count of draw distances. 
    s32 GetDrawDistanceCount() const;

    // PURPOSE: Retrieves the effect at the the given index.
    // PARAMS:
    //    iEffectIndex - the index of the effect
    // RETURNS: Effects data
    // NOTES: This function will assert if the iEffectIndex is out of range.
    SEffectsDetails* GetEffect( s32 iEffectIndex ) const;

    // PURPOSE: Adds a effect to this cut scene.
    // PARAMS:
    //    pEffect - the effect to add.  Clear() will delete the object for you.
    // NOTES: This function will assert if too many effects have been added.
    void AddEffect( SEffectsDetails* pEffect );

    // PURPOSE: Retrieves the number of effects that have been added to this cut scene.
    // RETURNS: The count of effects.
    s32 GetEffectCount() const;

    // PURPOSE: Retrieves the blocking bounds at the the given index.
    // PARAMS:
    //    iBlockingBoundsIndex - the index of the blocking bounds
    // RETURNS: Blocking bounds data
    // NOTES: This function will assert if the iBlockingBoundsIndex is out of range.
    SBlockDetails* GetBlockingBounds( s32 iBlockingBoundsIndex ) const;

    // PURPOSE: Adds a blocking bound to this cut scene.
    // PARAMS:
    //    pBlockingBounds - the blocking bound to add.  Clear() will delete the object for you.
    // NOTES: This function will assert if too many blocking bounds have been added.
    void AddBlockingBounds( SBlockDetails* pBlockingBounds );

    // PURPOSE: Retrieves the number of blocking bounds that have been added to this cut scene.
    // RETURNS: The count of blocking bounds.
    s32 GetBlockingBoundsCount() const;

    // PURPOSE: Retrieves the subtitle at the the given index.
    // PARAMS:
    //    iSubtitleIndex - the index of the subtitle
    // RETURNS: Subtitle data
    // NOTES: This function will assert if the iSubtitleIndex is out of range.
    STextIdDetails* GetSubtitle( s32 iSubtitleIndex ) const;

    // PURPOSE: Adds a subtitle to this cut scene.
    // PARAMS:
    //    pSubtitle - the subtitle to add.  Clear() will delete the object for you.
    // NOTES: This function will assert if too many subtitles have been added.
    void AddSubtitle( STextIdDetails* pSubtitle );

    // PURPOSE: Clears the list of subtitles.
    void ClearSubtitles();

    // PURPOSE: Retrieves the number of subtitles that have been added to this cut scene.
    // RETURNS: The count of subtitles.
    s32 GetSubtitleCount() const;

    // PURPOSE: Retrieves the camcorder overlay at the the given index.
    // PARAMS:
    //    iCamcorderOverlayIndex - the index of the camcorder overlay
    // RETURNS: Camcorder overlay data
    // NOTES: This function will assert if the iCamcorderOverlayIndex is out of range.
    SCamcorderOverlay* GetCamcorderOverlay( s32 iCamcorderOverlayIndex ) const;

    // PURPOSE: Adds a camcorder overlay to this cut scene.
    // PARAMS:
    //    pCamcorderOverlay - the camcorder overlay to add.  Clear() will delete the object for you.
    // NOTES: This function will assert if too many camcorder overlays have been added.
    void AddCamcorderOverlay( SCamcorderOverlay* pCamcorderOverlay );

    // PURPOSE: Retrieves the number of camcorder overlays that have been added to this cut scene.
    // RETURNS: The count of camcorder overlays.
    s32 GetCamcorderOverlayCount() const;

    // PURPOSE: Retrieves the fixup object at the the given index.
    // PARAMS:
    //    iFixupObjectIndex - the index of the fixup object
    // RETURNS: Fixup object data
    // NOTES: This function will assert if the iFixupObjectIndex is out of range.
    SFixupObjectDetails* GetFixupObject( s32 iFixupObjectIndex ) const;

    // PURPOSE: Adds a fixup object to this cut scene.
    // PARAMS:
    //    pFixupObject - the fixup object to add.  Clear() will delete the object for you.
    // NOTES: This function will assert if too many fixup objects have been added.
    void AddFixupObject( SFixupObjectDetails* pFixupObject );

    // PURPOSE: Retrieves the number of fixup objects that have been added to this cut scene.
    // RETURNS: The count of fixup objects.
    s32 GetFixupObjectCount() const;

    // PURPOSE: Retrieves the variation at the the given index.
    // PARAMS:
    //    iVariationIndex - the index of the variation
    // RETURNS: Variation data
    // NOTES: This function will assert if the iVariationIndex is out of range.
    SVariationDetails* GetVariation( s32 iVariationIndex ) const;

    // PURPOSE: Adds a variation to this cut scene.
    // PARAMS:
    //    pVariation - the variation to add.  Clear() will delete the object for you.
    // NOTES: This function will assert if too many variations have been added.
    void AddVariation( SVariationDetails* pVariation );

    // PURPOSE: Retrieves the number of variations that have been added to this cut scene.
    // RETURNS: The count of variations.
    s32 GetVariationCount() const;

    // PURPOSE: Retrieves the prop at the the given index.
    // PARAMS:
    //    iPropIndex - the index of the prop
    // RETURNS: Prop data
    // NOTES: This function will assert if the iPropIndex is out of range.
    SPropDetails* GetProp( s32 iPropIndex ) const;

    // PURPOSE: Adds a prop to this cut scene.
    // PARAMS:
    //    pProp - the prop to add.  Clear() will delete the object for you.
    // NOTES: This function will assert if too many props have been added.
    void AddProp( SPropDetails* pProp );

    // PURPOSE: Retrieves the number of props that have been added to this cut scene.
    // RETURNS: The count of props.
    s32 GetPropCount() const;

    // PURPOSE: Retrieve the name of the "extra room" that is needed for this cut scene (North-specific).
    // RETURNS: The name of the "extra room".
    // NOTES: This is a North-specific setting.
    const char* GetExtraRoom() const;

    // PURPOSE: Set the name of the "extra room".
    // PARAMS:
    //    pExtraRoom - the name of the extra room.
    // NOTES: This is a North-specific setting.
    void SetExtraRoom( const char* pExtraRoom );

    // PURPOSE: Retrieve the position of the "extra room".
    // RETURNS: The vector position of the "extra room".
    // NOTES: This is a North-specific setting.
    const Vector3& GetExtraRoomPos() const;

    // PURPOSE: Set the position of the "extra room".
    // PARAMS:
    //    vExtraRoomPos - the vector position
    // NOTES: This is a North-specific setting.
    void SetExtraRoomPos( Vector3& vExtraRoomPos );

    // PURPOSE: Resets the "extra room" position to a default value.
    // NOTES: This is a North-specific setting.
    void ResetExtraRoomPos();

    PAR_SIMPLE_PARSABLE;

private:
    const char* GetNextLine( const char* p_cSource, char* p_cTarget, u32 uMax );

    bool m_bIsLoaded;

    char m_cName[CUTSCENE_NAME_LEN];
    s32 m_iNameHash;

    float m_fTotalDuration;
    s32 m_iPlayerId;
    s32 m_iCutsceneFlags;  // main cutscene flags

    Vector3 m_vPlayerStartingPos;
    char m_cMissionTextName[MISSION_NAME_LENGTH];

    cutfCutsceneSection* m_pSections[MAX_CUTSCENE_SECTIONS];	
    SDrawDistanceDetails *m_pDrawDistances[MAX_DRAW_DISTANCES];
    SEffectsDetails *m_pEffects[MAX_CUTSCENE_EFFECTS];
    SBlockDetails *m_pBlockingBounds[MAX_CUTSCENE_BLOCKING_BOUNDS];
    STextIdDetails *m_pSubtitles[MAX_TEXTOUTPUT];
    SCamcorderOverlay *m_pCamcorderOverlays[MAX_CAMCORDER_OVERLAYS];
    SFixupObjectDetails *m_pFixupObjects[MAX_FIXUP_OBJS];
    SVariationDetails *m_pVariations[MAX_CUTSCENE_VARIATIONS];
    SPropDetails *m_pProps[MAX_CUTSCENE_PROPS];

    s32 m_iSectionCount;
    s32 m_iVariationCount;
    s32 m_iPropCount;
    s32 m_iFixupObjectCount;
    s32 m_iCamcorderOverlayCount;
    s32 m_iDrawDistanceCount;
    s32 m_iEffectCount;
    s32 m_iBlockingBoundCount;
    s32 m_iSubtitleCount;

    // internal
    bool m_bNextSectionIsManual;
    bool m_bNextSectionIsManualForAnims;
    bool m_bNextSectionIsManualForAudio;

    char m_cExtraRoom[MAX_EXTRA_ROOM_CHARS];
    Vector3	m_vExtraRoomPos;

    s32 m_iStartFrame;
    s32 m_iSectionSplitFrames[MAX_CUTSCENE_SECTIONS];
    s32 m_iSectionSplitFrameCount;
    s32 m_iEndFrame;
};

inline bool cutfCutsceneFile::IsLoaded() const
{
    return m_bIsLoaded;
}

inline const char* cutfCutsceneFile::GetName() const
{
    return m_cName;
}

inline void cutfCutsceneFile::SetName( const char* pName )
{
#if __DEV
    cutfAssertf( strlen( pName ) < CUTSCENE_NAME_LEN,  "CUTSCENE: The scene name must be less than %d characters.", CUTSCENE_NAME_LEN );
#endif // __DEV
    safecpy( m_cName, pName, CUTSCENE_NAME_LEN );
    m_iNameHash = atStringHash( m_cName );
}

inline s32 cutfCutsceneFile::GetNameHash() const
{
    return m_iNameHash;
}

inline float cutfCutsceneFile::GetTotalDuration() const
{
    return m_fTotalDuration;
}

inline void cutfCutsceneFile::SetTotalDuration( float fTotalDuration )
{
    m_fTotalDuration = fTotalDuration;
}

inline float cutfCutsceneFile::GetTotalDurationSeconds() const
{
    return m_fTotalDuration / 1000.0f;
}

inline s32 cutfCutsceneFile::GetPlayerId() const
{
    return m_iPlayerId;
}

inline void cutfCutsceneFile::SetPlayerId( s32 iPlayerId )
{
    m_iPlayerId = iPlayerId;
}

inline s32 cutfCutsceneFile::GetCutsceneFlags() const
{
    return m_iCutsceneFlags;
}

inline void cutfCutsceneFile::SetCutsceneFlags( s32 iCutsceneFlags )
{
    m_iCutsceneFlags = iCutsceneFlags;
}

inline s32 cutfCutsceneFile::GetStartFrame() const
{
    return m_iStartFrame;
}

inline void cutfCutsceneFile::SetStartFrame( s32 iStartFrame )
{
    m_iStartFrame = iStartFrame;
}

inline s32 cutfCutsceneFile::GetSectionSplitFrame( int iSectionSplitFrameIndex ) const
{
    if ( cutfVerifyf( (iSectionSplitFrameIndex >= 0) && (iSectionSplitFrameIndex < MAX_CUTSCENE_SECTIONS), "Out of Range" ) )
    {
        return m_iSectionSplitFrames[iSectionSplitFrameIndex];
    }

    return -1;
}

inline void cutfCutsceneFile::AddSectionSplitFrame( int iSectionSplitFrame )
{
    if ( cutfVerifyf( (m_iSectionSplitFrameCount >= 0) && (m_iSectionSplitFrameCount < MAX_CUTSCENE_SECTIONS), "Out of Range" ) )
    {
        m_iSectionSplitFrames[m_iSectionSplitFrameCount] = iSectionSplitFrame;
        ++m_iSectionSplitFrameCount;
    }
}

inline s32 cutfCutsceneFile::GetSectionSplitFrameCount() const
{
    return m_iSectionSplitFrameCount;
}

inline s32 cutfCutsceneFile::GetEndFrame() const
{
    return m_iEndFrame;
}

inline void cutfCutsceneFile::SetEndFrame( s32 iEndFrame )
{
    m_iEndFrame = iEndFrame;
}

inline const Vector3& cutfCutsceneFile::GetPlayerStartPosition() const
{
    return m_vPlayerStartingPos;
}

inline void cutfCutsceneFile::SetPlayerStartPosition( const Vector3& vPlayerStartPosition )
{
    m_vPlayerStartingPos = vPlayerStartPosition;
}

inline const char* cutfCutsceneFile::GetMissionTextName() const
{
    return m_cMissionTextName;
}

inline void cutfCutsceneFile::SetMissionTextName( const char* pMissionTextName )
{
#if __DEV
    cutfAssertf( strlen( pMissionTextName ) < MISSION_NAME_LENGTH,  "CUTSCENE: The scene name must be less than %d characters.", MISSION_NAME_LENGTH );
#endif // 
    safecpy( m_cMissionTextName, pMissionTextName, MISSION_NAME_LENGTH );
}

inline cutfCutsceneSection* cutfCutsceneFile::GetSection( s32 iSection ) const
{
    if ( cutfVerifyf( (iSection >= 0) && (iSection < m_iSectionCount), "Out of Range" ) )
    {
        return m_pSections[iSection];
    }

    return NULL;
}

inline s32 cutfCutsceneFile::GetSectionCount() const
{
    return m_iSectionCount;
}

inline SDrawDistanceDetails* cutfCutsceneFile::GetDrawDistance( s32 iDrawDistanceIndex ) const
{
    if ( cutfVerifyf( (iDrawDistanceIndex >= 0) && (iDrawDistanceIndex < m_iDrawDistanceCount), "Out of Range" ) )
    {
        return m_pDrawDistances[iDrawDistanceIndex];
    }

    return NULL;
}

inline s32 cutfCutsceneFile::GetDrawDistanceCount() const
{
    return m_iDrawDistanceCount;
}

inline SEffectsDetails* cutfCutsceneFile::GetEffect( s32 iEffectIndex ) const
{
    if ( cutfVerifyf( (iEffectIndex >= 0) && (iEffectIndex < m_iEffectCount), "Out of Range" ) )
    {
        return m_pEffects[iEffectIndex];
    }

    return NULL;
}

inline s32 cutfCutsceneFile::GetEffectCount() const
{
    return m_iEffectCount;
}

inline SBlockDetails* cutfCutsceneFile::GetBlockingBounds( s32 iBlockingBoundsIndex ) const
{
    if ( cutfVerifyf( (iBlockingBoundsIndex >= 0) && (iBlockingBoundsIndex < m_iBlockingBoundCount), "Out of Range" ) )
    {
        return m_pBlockingBounds[iBlockingBoundsIndex];
    }

    return NULL;
}

inline s32 cutfCutsceneFile::GetBlockingBoundsCount() const
{
    return m_iBlockingBoundCount;
}

inline STextIdDetails* cutfCutsceneFile::GetSubtitle( s32 iSubtitleIndex ) const
{
    if ( cutfVerifyf( (iSubtitleIndex >= 0) && (iSubtitleIndex < m_iSubtitleCount), "Out of Range" ) )
    {
        return m_pSubtitles[iSubtitleIndex];
    }

    return NULL;
}

inline s32 cutfCutsceneFile::GetSubtitleCount() const
{
    return m_iSubtitleCount;
}

inline SCamcorderOverlay* cutfCutsceneFile::GetCamcorderOverlay( s32 iCamcorderOverlayIndex ) const
{
    if ( cutfVerifyf( (iCamcorderOverlayIndex >= 0) && (iCamcorderOverlayIndex < m_iCamcorderOverlayCount), "Out of Range" ) )
    {
        return m_pCamcorderOverlays[iCamcorderOverlayIndex];
    }

    return NULL;
}

inline s32 cutfCutsceneFile::GetCamcorderOverlayCount() const
{
    return m_iCamcorderOverlayCount;
}

inline SFixupObjectDetails* cutfCutsceneFile::GetFixupObject( s32 iFixupObjectIndex ) const
{
    if ( cutfVerifyf( (iFixupObjectIndex >= 0) && (iFixupObjectIndex < m_iFixupObjectCount), "Out of Range" ) )
    {
        return m_pFixupObjects[iFixupObjectIndex];
    }

    return NULL;
}

inline s32 cutfCutsceneFile::GetFixupObjectCount() const
{
    return m_iFixupObjectCount;
}

inline SVariationDetails* cutfCutsceneFile::GetVariation( s32 iVariationIndex ) const
{
    if ( cutfVerifyf( (iVariationIndex >= 0) && (iVariationIndex < m_iVariationCount), "Out of Range" ) )
    {
        return m_pVariations[iVariationIndex];
    }

    return NULL;
}

inline s32 cutfCutsceneFile::GetVariationCount() const
{
    return m_iVariationCount;
}

inline SPropDetails* cutfCutsceneFile::GetProp( s32 iPropIndex ) const
{
    if ( cutfVerifyf( (iPropIndex >= 0) && (iPropIndex < m_iPropCount), "Out of Range" ) )
    {
        return m_pProps[iPropIndex];
    }

    return NULL;
}

inline s32 cutfCutsceneFile::GetPropCount() const
{
    return m_iPropCount;
}

inline const char* cutfCutsceneFile::GetExtraRoom() const
{
    return m_cExtraRoom;
}

inline void cutfCutsceneFile::SetExtraRoom( const char* pExtraRoom )
{
#if __DEV
    cutfAssertf( strlen(pExtraRoom) < MAX_EXTRA_ROOM_CHARS, 
         "CUTSCENE: Extra room name is longer than %d chars", MAX_EXTRA_ROOM_CHARS );
#endif // __DEV
    safecpy( m_cExtraRoom, pExtraRoom, MAX_EXTRA_ROOM_CHARS );
}

inline const Vector3& cutfCutsceneFile::GetExtraRoomPos() const
{
    return m_vExtraRoomPos;
}

inline void cutfCutsceneFile::SetExtraRoomPos( Vector3& vExtraRoomPos )
{
    m_vExtraRoomPos = vExtraRoomPos;
}

inline void cutfCutsceneFile::ResetExtraRoomPos()
{
    m_vExtraRoomPos.Set( -1000.0f, -1000.0f, -1000.0);
}

}	// namespace rage

#endif // CUTFILE_CUTFILE_H
