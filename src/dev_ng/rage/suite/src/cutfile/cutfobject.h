// 
// cutfile/cutfobject.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTFILE_CUTFOBJECT_H 
#define CUTFILE_CUTFOBJECT_H 

#include "cutfdefines.h"

#include "cutfchannel.h"
#include "atl/string.h"
#include "string/string.h"
#include "parser/macros.h"
#include "parsercore/attribute.h"
#include "string/stringhash.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "atl/hashstring.h"

namespace rage {

class cutfAttributeList;

//##############################################################################

// PURPOSE: An enumeration of known cut scene object types
enum ECutsceneObjectType
{
    CUTSCENE_ASSET_MANAGER_OBJECT_TYPE,     // 1 object for loading the scene, models, overlays, subtitles, etc.
    CUTSCENE_OVERLAY_OBJECT_TYPE,           // 1 object for each overlay
    CUTSCENE_SCREEN_FADE_OBJECT_TYPE,       // 1 object for ALL screen fades
    CUTSCENE_SUBTITLE_OBJECT_TYPE,          // 1 object for each subtitle
    CUTSCENE_MODEL_OBJECT_TYPE,             // 1 object for each model
    CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE,      // 1 object for each hidden model
    CUTSCENE_FIXUP_MODEL_OBJECT_TYPE,       // 1 object for each fixup model
    CUTSCENE_PARTICLE_EFFECT_OBJECT_TYPE,   // 1 object for each particle effect
    CUTSCENE_BLOCKING_BOUNDS_OBJECT_TYPE,   // 1 object for each blocking bound
    CUTSCENE_LIGHT_OBJECT_TYPE,             // 1 object for each light
    CUTSCENE_CAMERA_OBJECT_TYPE,            // 1 object for each camera
    CUTSCENE_AUDIO_OBJECT_TYPE,             // 1 object for ALL audios
    CUTSCENE_EVENTS_OBJECT_TYPE,            // 1 object for all events
    CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE, // 1 object for all animation events
    CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE, // 1 object for each particle effect
    CUTSCENE_REMOVAL_BOUNDS_OBJECT_TYPE,    // 1 object for each removal bound
	CUTSCENE_RAYFIRE_OBJECT_TYPE,			// 1 object for each rayfire
	CUTSCENE_DECAL_OBJECT_TYPE, 
	CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE, 
    CUTSCENE_NUM_OBJECT_TYPES,              // custom object types should start with this id
};

// PURPOSE: An enumeration of known cut scene model types
enum ECutsceneModelType
{
    CUTSCENE_PED_MODEL_TYPE,
    CUTSCENE_VEHICLE_MODEL_TYPE,
    CUTSCENE_PROP_MODEL_TYPE,
	CUTSCENE_WEAPON_MODEL_TYPE,
    CUTSCENE_HIDDEN_MODEL_TYPE,
    CUTSCENE_FIXUP_MODEL_TYPE,
    CUTSCENE_NUM_MODEL_TYPES
};

enum ECutsceneLightType
{
    CUTSCENE_DIRECTIONAL_LIGHT_TYPE,
    CUTSCENE_POINT_LIGHT_TYPE,
    CUTSCENE_SPOT_LIGHT_TYPE,
    CUTSCENE_NUM_LIGHT_TYPES
};

enum ECutsceneLightFlag
{
	CUTSCENE_LIGHTFLAG_DONT_LIGHT_ALPHA = BIT0,
	CUTSCENE_LIGHTFLAG_CAST_STATIC_GEOM_SHADOWS = BIT7,
	CUTSCENE_LIGHTFLAG_CAST_DYNAMIC_GEOM_SHADOWS = BIT8,
	CUTSCENE_LIGHTFLAG_CALC_FROM_SUN = BIT9,
	CUTSCENE_LIGHTFLAG_ENABLE_BUZZING = BIT10,
	CUTSCENE_LIGHTFLAG_FORCE_BUZZING = BIT11,
	CUTSCENE_LIGHTFLAG_DRAW_VOLUME = BIT12,
	CUTSCENE_LIGHTFLAG_NO_SPECULAR = BIT13,
	CUTSCENE_LIGHTFLAG_BOTH_INTERIOR_AND_EXTERIOR = BIT14,
	CUTSCENE_LIGHTFLAG_CORONA_ONLY = BIT15,
	CUTSCENE_LIGHTFLAG_NOT_IN_REFLECTION = BIT16, 
	CUTSCENE_LIGHTFLAG_ONLY_IN_REFLECTION = BIT17,
	CUTSCENE_LIGHTFLAG_REFLECTOR = BIT18,
	CUTSCENE_LIGHTFLAG_DIFFUSER = BIT19,
	CUTSCENE_LIGHTFLAG_ADD_AMBIENT_LIGHT = BIT20,
	CUTSCENE_LIGHTFLAG_USE_TIMECYCLE_VALUES = BIT21,
	CUTSCENE_LIGHTFLAG_IS_CHARACTER_LIGHT = BIT22,
	CUTSCENE_LIGHTFLAG_CHARACTER_LIGHT_INTENSITY_AS_MULTIPLIER = BIT23,
	CUTSCENE_LIGHTFLAG_IS_PED_ONLY_LIGHT = BIT24
};

enum ECutsceneLightHourFlag
{
	CUTSCENE_LIGHTFLAG_HOURS_01 = BIT0,
	CUTSCENE_LIGHTFLAG_HOURS_02 = BIT1,
	CUTSCENE_LIGHTFLAG_HOURS_03 = BIT2,
	CUTSCENE_LIGHTFLAG_HOURS_04 = BIT3,
	CUTSCENE_LIGHTFLAG_HOURS_05 = BIT4,
	CUTSCENE_LIGHTFLAG_HOURS_06 = BIT5,
	CUTSCENE_LIGHTFLAG_HOURS_07 = BIT6,
	CUTSCENE_LIGHTFLAG_HOURS_08 = BIT7,
	CUTSCENE_LIGHTFLAG_HOURS_09 = BIT8,
	CUTSCENE_LIGHTFLAG_HOURS_10 = BIT9,
	CUTSCENE_LIGHTFLAG_HOURS_11 = BIT10,
	CUTSCENE_LIGHTFLAG_HOURS_12 = BIT11,
	CUTSCENE_LIGHTFLAG_HOURS_13 = BIT12,
	CUTSCENE_LIGHTFLAG_HOURS_14 = BIT13,
	CUTSCENE_LIGHTFLAG_HOURS_15 = BIT14,
	CUTSCENE_LIGHTFLAG_HOURS_16 = BIT15,
	CUTSCENE_LIGHTFLAG_HOURS_17 = BIT16,
	CUTSCENE_LIGHTFLAG_HOURS_18 = BIT17,
	CUTSCENE_LIGHTFLAG_HOURS_19 = BIT18,
	CUTSCENE_LIGHTFLAG_HOURS_20 = BIT19,
	CUTSCENE_LIGHTFLAG_HOURS_21 = BIT20,
	CUTSCENE_LIGHTFLAG_HOURS_22 = BIT21,
	CUTSCENE_LIGHTFLAG_HOURS_23 = BIT22,
	CUTSCENE_LIGHTFLAG_HOURS_24 = BIT23
};

enum ECutsceneAnimStreamingType
{
	CUTSCENE_ANIM_STREAMING_TYPE_NONE,
	CUTSCENE_NAMED_ANIMATED_OBJECT,
	CUTSCENE_NAMED_STREAMED_ANIMATED_OBJECT,
	CUTSCENE_ANIMATED_LIGHT_STREAMED_OBJECT 
};


//#define LIGHTFLAG_INTERIOR_ONLY					(1<<0)
//#define LIGHTFLAG_EXTERIOR_ONLY					(1<<1)
//#define LIGHTFLAG_CUTSCENE						(1<<2)
//#define LIGHTFLAG_VEHICLE						(1<<3)
//#define LIGHTFLAG_FX							(1<<4)
//#define LIGHTFLAG_TEXTURE_PROJECTION			(1<<5)
//#define LIGHTFLAG_CAST_SHADOWS					(1<<6)
//#define LIGHTFLAG_CAST_STATIC_GEOM_SHADOWS		(1<<7)
//#define LIGHTFLAG_CAST_DYNAMIC_GEOM_SHADOWS		(1<<8)
//#define LIGHTFLAG_CALC_FROM_SUN					(1<<9)
//#define LIGHTFLAG_ENABLE_BUZZING				(1<<10)
//#define LIGHTFLAG_FORCE_BUZZING					(1<<11)
//#define LIGHTFLAG_DRAW_VOLUME					(1<<12)
//#define LIGHTFLAG_NO_SPECULAR					(1<<13)
//#define LIGHTFLAG_BOTH_INTERIOR_AND_EXTERIOR	(1<<14)
//#define LIGHTFLAG_CORONA_ONLY					(1<<15)
//#define LIGHTFLAG_NOT_IN_REFLECTION				(1<<16)
//#define LIGHTFLAG_ONLY_IN_REFLECTION			(1<<17)
//#define LIGHTFLAG_USE_CULL_PLANE				(1<<18)
//#define LIGHTFLAG_USE_VOLUME_OUTER_COLOUR		(1<<19)

enum ECutsceneGenericWeaponType
{
	CUTSCENE_NO_GENERIC_WEAPON_TYPE,
	CUTSCENE_ONE_HANDED_GUN_GENERIC_WEAPON_TYPE,
	CUTSCENE_TWO_HANDED_GUN_GENERIC_WEAPON_TYPE,
	CUTSCENE_NUM_WEAPON_TYPES
};

// PURPOSE: An enumeration of known cut scene particle effect types
enum ECutsceneParticleEffectType
{
    CUTSCENE_ACTIVE_PARTICLE_EFFECT_TYPE,
    CUTSCENE_TRIGGER_PARTICLE_EFFECT_TYPE
};

enum ECutsceneOverlayType
{
	CUTSCENE_SCALEFORM_OVERLAY_TYPE,
	CUTSCENE_BINK_OVERLAY_TYPE
};

//##############################################################################

// PURPOSE: Base class for an item that is controlled and/or animated by a cut scene
class cutfObject
{
public:
    cutfObject();
    cutfObject( s32 iObjectId );
    cutfObject( s32 iObjectId, const parAttributeList& attributes );
    virtual ~cutfObject();

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    virtual s32 GetType() const = 0;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const = 0;

//#if !__FINAL
    // PURPOSE: Retrieves the display name of this object
    // RETURNS: The display name
    virtual const atString& GetDisplayName() const = 0;
//#endif

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const = 0;

	virtual void SetName(const char * pName) { m_displayName = pName; }

    // PURPOSE: Retrieves the unique id of this cutfObject.  cutfEvent and 
    //    cutsEntity will use this to match up events and receivers.
    // RETURNS: The id of this cutfObject.
    s32 GetObjectId() const;

    // PURPOSE: Set the unique object id of this cutfObject.
    // PARAMS:
    //    iObjectId - the object id to set
    // NOTES: Should only be called by tools
    void SetObjectId( s32 iObjectId );

    // PURPOSE: Retrieves the parAttributList so key-value pairs can be 
    //    added and accessed.
    // RETURNS: The attribute list.
    const parAttributeList& GetAttributeList() const;

	int Compare( const cutfObject *pObject ) const;

	virtual u32 GetAnimStreamingType() const { return CUTSCENE_ANIM_STREAMING_TYPE_NONE; }

    // PURPOSE: Brings all the attributes in the 'other' object into this object
    // PARAMS:
    //		other - The object to merge into this one
    //		overwrite - If true, attributes in 'other' will overwrite ones that are in this object.
    virtual void MergeFrom( const cutfObject &other, bool overwrite=true );

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

	void ConvertAttributesForSave();
	void ConvertAttributesAfterLoad();

    PAR_PARSABLE;

protected:
    s32 m_iObjectId;
//#if !__FINAL
    mutable atString m_displayName;
//#endif    
	parAttributeList m_attributeList;
	cutfAttributeList* m_cutfAttributes;
};

inline bool cutfObject::IsThisType( const char * /*pName*/ ) const
{
    return false;
}

inline s32 cutfObject::GetObjectId() const
{
    return m_iObjectId;
}

inline void cutfObject::SetObjectId( s32 iObjectId )
{
    m_iObjectId = iObjectId;
}

inline const parAttributeList& cutfObject::GetAttributeList() const
{
    return m_attributeList;
}

//##############################################################################

class cutfAssetManagerObject : public cutfObject
{
public:
    cutfAssetManagerObject();
    cutfAssetManagerObject( s32 iObjectId );
    cutfAssetManagerObject( s32 iObjectId, const parAttributeList& attributes );
    virtual ~cutfAssetManagerObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

//#if !__FINAL
	// PURPOSE: Retrieves the display name of this object
	// RETURNS: The display name
	virtual const atString& GetDisplayName() const;
//#endif

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;
};

inline s32 cutfAssetManagerObject::GetType() const
{
    return CUTSCENE_ASSET_MANAGER_OBJECT_TYPE;
}

inline const char* cutfAssetManagerObject::GetStaticTypeName()
{
    return "Asset Manager";
}

inline const char* cutfAssetManagerObject::GetTypeName() const
{
    return GetStaticTypeName();
}

//#if !__FINAL
inline const atString& cutfAssetManagerObject::GetDisplayName() const
{
    if ( m_displayName.GetLength() == 0 )
    {
        m_displayName.Set( "Asset Manager", 13, 0 );
    }
    
    return m_displayName;
}
//#endif

//##############################################################################

class cutfNamedObject : public cutfObject
{
public:
	cutfNamedObject();
	cutfNamedObject( s32 iObjectId, const char *pName );
	cutfNamedObject( s32 iObjectId, atHashString Name ); 
	cutfNamedObject( s32 iObjectId, const char *pName, const parAttributeList& attributes );
	cutfNamedObject( s32 iObjectId, atHashString pName, const parAttributeList& attributes );
    virtual ~cutfNamedObject() {}

    // PURPOSE: Gets the name of this object
    // RETURNS: The name of this object
    // NOTES: Models might be named something like "model1:Char", so this will return "model1:Char".
    atHashString GetName() const;

    // PURPOSE: Sets the name of this object
    // PARAMS:
    //    pName - the name of this object
    virtual void SetName( const char* pName );
	
	// PURPOSE: Sets the name of this object
	// PARAMS:
	//    pName - the name of this object
	virtual void SetName( atHashString Name); 
//#if !__FINAL
    // PURPOSE: Retrieves the display name of this object
    // RETURNS: The display name
    // NOTES: Models might be named something like "model1:Char", so this will return "model1".
    virtual const atString& GetDisplayName() const;
//#endif
    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const = 0;

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

    PAR_PARSABLE;

protected:
    atHashString m_cName; 
};

inline atHashString cutfNamedObject::GetName() const
{
    return m_cName;
}

//##############################################################################

class cutfNamedAnimatedObject : public cutfNamedObject
{
public:
	cutfNamedAnimatedObject();
	cutfNamedAnimatedObject( s32 iObjectId, const char *pName, u32 AnimBaseName );
	cutfNamedAnimatedObject( s32 iObjectId, atHashString Name, u32 AnimBaseName  ); 
	cutfNamedAnimatedObject( s32 iObjectId, const char *pName, u32 AnimBaseName, const parAttributeList& attributes );
	cutfNamedAnimatedObject( s32 iObjectId, atHashString pName, u32 AnimBaseName, const parAttributeList& attributes );
	virtual ~cutfNamedAnimatedObject() {}

	// PURPOSE: Gets the name of this object
	// RETURNS: The name of this object
	// NOTES: Models might be named something like "model1:Char", so this will return "model" used for streaming.
	u32 GetAnimStreamingBase() const;

	// PURPOSE: Sets the name of this object
	// PARAMS:
	//    pName - the name of this object
	void SetAnimStreamingBase(const char* Name); 

	virtual u32 GetAnimStreamingType() const { return CUTSCENE_NAMED_ANIMATED_OBJECT; }

	// PURPOSE: Creates a copy of this cut scene object, but with the given object id
	// PARAMS:
	//    iObjectId - the new object id to give the clone
	// RETURNS: A clone of this instance
	virtual cutfObject* Clone() const = 0;

	virtual bool operator == ( const cutfObject &other ) const;
	virtual bool operator != ( const cutfObject &other ) const;

	PAR_PARSABLE;

protected:
	u32 m_AnimStreamingBase; 
};

inline u32 cutfNamedAnimatedObject::GetAnimStreamingBase() const
{
	return m_AnimStreamingBase;
}


//##############################################################################

class cutfNamedStreamedObject : public cutfNamedObject
{
public:
	cutfNamedStreamedObject();
	cutfNamedStreamedObject( s32 iObjectId, const char* pName, const char* pStreamingName);
	cutfNamedStreamedObject( s32 iObjectId, atHashString Name, atHashString StreamingName); 
	cutfNamedStreamedObject( s32 iObjectId, const char *pName, const char* pStreamingName, const parAttributeList& attributes );
	cutfNamedStreamedObject( s32 iObjectId, atHashString Name, atHashString StreamingName, const parAttributeList& attributes );

	virtual ~cutfNamedStreamedObject() {}

	// PURPOSE: Gets the name of this object
	// RETURNS: The name of this object
	// NOTES: Models might be named something like "model1:Char", so this will return "model" used for streaming.
	atHashString GetStreamingName() const;

	// PURPOSE: Sets the name of this object
	// PARAMS:
	//    pName - the name of this object
	virtual void SetStreamingName( atHashString StreamingName);
	
	// PURPOSE: Sets the name of this object
	// PARAMS:
	//    Allow runtime to directly override the streaming name of the object
	virtual void OverrideStreamingName(atHashString StreamingName) { m_StreamingName = StreamingName; }

	// PURPOSE: Creates a copy of this cut scene object, but with the given object id
	// PARAMS:
	//    iObjectId - the new object id to give the clone
	// RETURNS: A clone of this instance
	virtual cutfObject* Clone() const = 0;

	virtual bool operator == ( const cutfObject &other ) const;
	virtual bool operator != ( const cutfObject &other ) const;

	PAR_PARSABLE;

protected:
	atHashString m_StreamingName; 
};

inline atHashString cutfNamedStreamedObject::GetStreamingName() const
{
	Assertf(m_StreamingName, "cutfObject %s has no streaming name, check the cut file", GetDisplayName().c_str());
	return m_StreamingName;
}

//##############################################################################

class cutfNamedAnimatedStreamedObject : public cutfNamedStreamedObject
{
public:
	cutfNamedAnimatedStreamedObject();
	cutfNamedAnimatedStreamedObject( s32 iObjectId, const char* pName, const char* pStreamingName, u32 AnimStreamingBase);
	cutfNamedAnimatedStreamedObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimStreamingBase); 
	cutfNamedAnimatedStreamedObject( s32 iObjectId, const char *pName, const char* pStreamingName, u32 AnimStreamingBase, const parAttributeList& attributes );
	cutfNamedAnimatedStreamedObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimStreamingBase, const parAttributeList& attributes );

	virtual ~cutfNamedAnimatedStreamedObject() {}

	// PURPOSE: Gets the name of this object
	// RETURNS: The name of this object
	// NOTES: Models might be named something like "model1:Char", so this will return "model" used for streaming.
	u32 GetAnimStreamingBase() const;
	
	// PURPOSE: Sets the name of this object
	// PARAMS:
	//    pName - the name of this object
	void SetAnimStreamingBase( const char* name);
	
	virtual u32 GetAnimStreamingType() const { return CUTSCENE_NAMED_STREAMED_ANIMATED_OBJECT; }

	// PURPOSE: Creates a copy of this cut scene object, but with the given object id
	// PARAMS:
	//    iObjectId - the new object id to give the clone
	// RETURNS: A clone of this instance
	virtual cutfObject* Clone() const = 0;

	virtual bool operator == ( const cutfObject &other ) const;
	virtual bool operator != ( const cutfObject &other ) const;

	PAR_PARSABLE;

protected:
	u32 m_AnimStreamingBase; 
};

inline u32 cutfNamedAnimatedStreamedObject::GetAnimStreamingBase() const
{
	return m_AnimStreamingBase;
}

//##############################################################################

class cutfFinalNamedObject : public cutfObject
{
public:
	cutfFinalNamedObject();
	cutfFinalNamedObject( s32 iObjectId, const char *pName );
	cutfFinalNamedObject( s32 iObjectId, const char *pName, const parAttributeList& attributes );
	virtual ~cutfFinalNamedObject() {}

	// PURPOSE: Gets the name of this object
	// RETURNS: The name of this object
	// NOTES: Models might be named something like "model1:Char", so this will return "model1:Char".
	const char* GetName() const;

	// PURPOSE: Sets the name of this object
	// PARAMS:
	//    pName - the name of this object
	virtual void SetName( const char* pName );
//#if !__FINAL
	// PURPOSE: Retrieves the display name of this object
	// RETURNS: The display name
	// NOTES: Models might be named something like "model1:Char", so this will return "model1".
	virtual const atString& GetDisplayName() const;
//#endif
	// PURPOSE: Creates a copy of this cut scene object, but with the given object id
	// PARAMS:
	//    iObjectId - the new object id to give the clone
	// RETURNS: A clone of this instance
	virtual cutfObject* Clone() const = 0;

	virtual bool operator == ( const cutfObject &other ) const;
	virtual bool operator != ( const cutfObject &other ) const;

	PAR_PARSABLE;

protected:
	atString m_cName;
};

inline const char* cutfFinalNamedObject::GetName() const
{
	return m_cName.c_str();
}

inline void cutfFinalNamedObject::SetName( const char* pName )
{
	if ( pName != NULL )
	{
		m_cName = pName; 
	}
	else
	{
		m_cName.Clear();
	}

//#if !__FINAL
	m_displayName.Clear();
//#endif
}

//##############################################################################

class cutfOverlayObject : public cutfFinalNamedObject
{
public:
    cutfOverlayObject();
    cutfOverlayObject( s32 iObjectId, const char* pName);
    cutfOverlayObject( s32 iObjectId, const char* pName, const parAttributeList& attributes );
	cutfOverlayObject( s32 iObjectId, const char* pName, const char* pRenderTarget, int iOverlayType, atHashString modelHashName, const parAttributeList& attributes );
    virtual ~cutfOverlayObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;
    
    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

	void SetOverlayType( u32 iOverlayType );
	u32 GetOverlayType() const;

	void SetRenderTargetName( const char* pRenderTargetName );
	const char* GetRenderTargetName() const;

	void SetModelHashName( atHashString modelHashName );
	atHashString GetModelHashName() const;

    PAR_PARSABLE;

protected:
	atString m_cRenderTargetName;
	u32 m_iOverlayType;
	atHashString m_modelHashName;
	
};

inline void cutfOverlayObject::SetModelHashName( atHashString modelHashName )
{
	m_modelHashName = modelHashName;
}

inline atHashString cutfOverlayObject::GetModelHashName() const
{
	return m_modelHashName;
}

inline void cutfOverlayObject::SetOverlayType( u32 iOverlayType )
{
	m_iOverlayType = iOverlayType;
}

inline u32 cutfOverlayObject::GetOverlayType() const
{
	return m_iOverlayType;
}

inline void cutfOverlayObject::SetRenderTargetName( const char* pRenderTargetName )
{
	if ( pRenderTargetName != NULL )
	{
		m_cRenderTargetName = pRenderTargetName; 
	}
	else
	{
		m_cRenderTargetName = "";
	}
}

inline const char* cutfOverlayObject::GetRenderTargetName() const
{
	return m_cRenderTargetName.c_str();
}

inline s32 cutfOverlayObject::GetType() const
{
    return CUTSCENE_OVERLAY_OBJECT_TYPE;
}

inline const char* cutfOverlayObject::GetStaticTypeName()
{
    return "Screen Overlay";
}

inline const char* cutfOverlayObject::GetTypeName() const
{
    return GetStaticTypeName();
}

//##############################################################################

class cutfScreenFadeObject : public cutfNamedObject
{
public:
    cutfScreenFadeObject();
    cutfScreenFadeObject( s32 iObjectId, const char *pName );
    cutfScreenFadeObject( s32 iObjectId, const char *pName, const parAttributeList& attributes );
	cutfScreenFadeObject( s32 iObjectId, atHashString Name, const parAttributeList& attributes );
    virtual ~cutfScreenFadeObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;
};

inline s32 cutfScreenFadeObject::GetType() const
{
    return CUTSCENE_SCREEN_FADE_OBJECT_TYPE;
}

inline bool cutfScreenFadeObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return strstr( lwrName, "screen fade" ) || strstr( lwrName, "screenfade" );
}

inline bool cutfScreenFadeObject::IsThisType( const char *pName ) const
{
    return cutfScreenFadeObject::StaticIsThisType( pName );
}

inline const char* cutfScreenFadeObject::GetStaticTypeName()
{
    return "Screen Fade";
}

inline const char* cutfScreenFadeObject::GetTypeName() const
{
    return GetStaticTypeName();
}

//##############################################################################

class cutfSubtitleObject : public cutfNamedObject
{
public:
    cutfSubtitleObject();
    cutfSubtitleObject( s32 iObjectId, const char *pName );
    cutfSubtitleObject( s32 iObjectId, const char *pName, const parAttributeList& attributes );
	cutfSubtitleObject( s32 iObjectId, atHashString Name, const parAttributeList& attributes );
    virtual ~cutfSubtitleObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;
};

inline s32 cutfSubtitleObject::GetType() const
{
    return CUTSCENE_SUBTITLE_OBJECT_TYPE;
}

inline const char* cutfSubtitleObject::GetStaticTypeName()
{
    return "Subtitle";
}

inline const char* cutfSubtitleObject::GetTypeName() const
{
    return GetStaticTypeName();
}

//##############################################################################

class cutfModelObject : public cutfNamedAnimatedStreamedObject
{
public:
    cutfModelObject();
    cutfModelObject( s32 iObjectId, const char *pName,const char *pStreamedName, u32 AnimBase, const char* pAnimExportCtrlSpecFile=NULL, const char *pAnimCompressionFile=NULL );
    cutfModelObject( s32 iObjectId, const char *pName, const char *pStreamedName,u32 AnimBase, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
        const parAttributeList& attributes );
	cutfModelObject( s32 iObjectId, atHashString pName, atHashString StreamedName, u32 AnimBase, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
		const parAttributeList& attributes );

	cutfModelObject( s32 iObjectId, atHashString pName, atHashString StreamedName, u32 AnimBase, atHashString pAnimExportCtrlSpecFile, atHashString pAnimCompressionFile, 
		const parAttributeList& attributes );

    virtual ~cutfModelObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Retrieves the model type.  Built-in types are given
    //    by the ECutsceneModelType enum.
    // RETURNS: The model type.
    virtual s32 GetModelType() const = 0;

    // PURPOSE: Gets the animation export control specification file
    // RETURNS: The animation export control specification file
    atHashString GetAnimExportCtrlSpecFile() const;

    // PURPOSE: Gets the default animation export control specification file.
    // RETURNS: The default animation export control specification file
    atHashString GetDefaultAnimExportCtrlSpecFile() const;
    
	atHashString GetFaceExportCtrlSpecFile() const;

	// PURPOSE: Sets the animation export control specification file
    // PARAMS:
    //    pAnimExportCtrlSpecFile - the animation export control specification file
    void SetAnimExportCtrlSpecFile( const char* pAnimExportCtrlSpecFile );
	
	void SetAnimExportCtrlSpecFile( atHashString pAnimExportCtrlSpecFile );

	void SetFaceExportCtrlSpecFile( const char* pAnimExportCtrlSpecFile );
	
	void SetFaceExportCtrlSpecFile( atHashString pAnimExportCtrlSpecFile );

    // PURPOSE: Gets the animation compression setting
    // RETURNS: The animation compression setting
    atHashString GetAnimCompressionFile() const;

    // PURPOSE: Gets the default animation compression setting
    // RETURNS: The default animation compression setting
    atHashString GetDefaultAnimCompression() const;

    // PURPOSE: Sets the animation compression
    // PARAMS:
    //    pAnimCompressionFile - the animation compression
    void SetAnimCompressionFile( const char* pAnimCompressionFile );

	// PURPOSE: Sets the animation compression
	// PARAMS:
	//    pAnimCompressionFile - the animation compression
	void SetAnimCompressionFile( atHashString pAnimCompressionFile );

	// PURPOSE: Retrieves the handle for the specific object.
	// RETURNS: The handle
	atHashString GetHandle() const;

	// PURPOSE: Set the specfic handle name on the object.
	// PARAMS:
	//    pHandle - the handle
	void SetHandle( atHashString Handle );

	void SetTypeFile( atHashString typeFile );

	atHashString GetTypeFile() const;


    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const = 0;

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

    PAR_PARSABLE;

protected:
    atHashString m_cAnimExportCtrlSpecFile;
	atHashString m_cFaceExportCtrlSpecFile;
    atHashString m_cAnimCompressionFile;
    
	mutable atHashString m_defaultAnimExportCtrlSpecFile;
    mutable atHashString m_defaultAnimCompression;
	
	atHashString m_cHandle;
	atHashString m_typeFile;
};

inline s32 cutfModelObject::GetType() const
{
    return CUTSCENE_MODEL_OBJECT_TYPE;
}

inline atHashString cutfModelObject::GetAnimExportCtrlSpecFile() const
{
	return m_cAnimExportCtrlSpecFile;

}

inline void cutfModelObject::SetAnimExportCtrlSpecFile( const char* pAnimExportCtrlSpecFile )
{
    if ( pAnimExportCtrlSpecFile != NULL )
    {
        m_cAnimExportCtrlSpecFile = pAnimExportCtrlSpecFile;
    }
    else
    {
        m_cAnimExportCtrlSpecFile.Clear();
    }
}

inline void cutfModelObject::SetAnimExportCtrlSpecFile( atHashString pAnimExportCtrlSpecFile )
{
	if ( pAnimExportCtrlSpecFile.GetHash() !=0 )
	{
		m_cAnimExportCtrlSpecFile = pAnimExportCtrlSpecFile;
	}
	else
	{
		m_cAnimExportCtrlSpecFile.Clear();
	}
}

inline atHashString cutfModelObject::GetFaceExportCtrlSpecFile() const
{
	return m_cFaceExportCtrlSpecFile;
}

inline void cutfModelObject::SetFaceExportCtrlSpecFile( const char* pFaceExportCtrlSpecFile )
{
	if ( pFaceExportCtrlSpecFile != NULL )
	{
		m_cFaceExportCtrlSpecFile = pFaceExportCtrlSpecFile;
	}
	else
	{
		m_cFaceExportCtrlSpecFile.Clear();
	}
}

inline void cutfModelObject::SetFaceExportCtrlSpecFile( atHashString pFaceExportCtrlSpecFile )
{
	if ( pFaceExportCtrlSpecFile.GetHash() != 0 )
	{
		m_cFaceExportCtrlSpecFile = pFaceExportCtrlSpecFile;
	}
	else
	{
		m_cFaceExportCtrlSpecFile.Clear();
	}
}

inline atHashString cutfModelObject::GetAnimCompressionFile() const 
{
	return m_cAnimCompressionFile;
}

inline void cutfModelObject::SetAnimCompressionFile( const char* pAnimCompressionFile )
{
    if ( pAnimCompressionFile != NULL )
    {
        m_cAnimCompressionFile = pAnimCompressionFile; 
    }
    else
    {
        m_cAnimCompressionFile.Clear();
    }
}

inline void cutfModelObject::SetAnimCompressionFile( atHashString pAnimCompressionFile )
{
	if ( pAnimCompressionFile.GetHash() != 0)
	{
		m_cAnimCompressionFile = pAnimCompressionFile; 
	}
	else
	{
		m_cAnimCompressionFile.Clear();
	}
}

inline atHashString cutfModelObject::GetHandle() const
{
	return m_cHandle;
}

inline void cutfModelObject::SetHandle( atHashString Handle )
{
	if ( Handle.GetHash() != 0 )
	{
		m_cHandle = Handle;
	}
	else
	{
		m_cHandle.Clear();
	}
}

//##############################################################################

class cutfPedModelObject : public cutfModelObject
{
public:
    cutfPedModelObject();
    cutfPedModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile=NULL, const char *pAnimCompressionFile=NULL, 
        bool bHasHeadAnimation=true, bool bFaceAndBodyAreMerged=false, bool bOverrideFaceAnimation=false, const char *pOverrideFaceAnimationFilename=NULL );
    cutfPedModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
        bool bHasHeadAnimation, bool bFaceAndBodyAreMerged, bool bOverrideFaceAnimation, const char *pOverrideFaceAnimationFilename, 
        const parAttributeList& attributes );
	cutfPedModelObject( s32 iObjectId, atHashString pName, atHashString StreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
		bool bHasHeadAnimation, bool bFaceAndBodyAreMerged, bool bOverrideFaceAnimation, const char *pOverrideFaceAnimationFilename, 
		const parAttributeList& attributes );

	cutfPedModelObject( s32 iObjectId, atHashString pName, atHashString StreamingName, u32 AnimBaseHash, atHashString pAnimExportCtrlSpecFile, atHashString pAnimCompressionFile, 
		bool bHasHeadAnimation, bool bFaceAndBodyAreMerged, bool bOverrideFaceAnimation, atHashString pOverrideFaceAnimationFilename, 
		const parAttributeList& attributes );
    virtual ~cutfPedModelObject() {}

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Retrieves the model type.  Built-in types are given
    //    by the ECutsceneModelType enum.
    // RETURNS: The model type.
    virtual s32 GetModelType() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    // PURPOSE: Returns whether this ped model has a head animation if 'Found Face' was set, or the 
    //    face anim is being overridden to a non-null, non-empty filename.
    // RETURNS: True if there is a head animation, otherwise false.
    // NOTE:  This does not check if the file actually exists.
    bool HasFaceAnimation() const;

    // PURPOSE: Retrieves the name of the face animation file, using the current settings to determine the name.
    //    If an override was specified, returns the override filename, otherwise returns "$(pFaceDir)\$(GetDisplayName())_face.anim".
    // PARAMS:
    //    pFaceDir - the directory containing the face animation files
    // RETURNS: The filename for the face animation
    atHashString GetFaceAnimationFilename( const char *pFaceDir ) const;


    // PURPOSE: Returns whether or not this ped model found a face animation when the cut file was saved.
    // RETURNS: True if there was a head animation, otherwise false.
    bool FoundDefaultFaceAnimation() const;

    // PURPOSE: Sets whether or not this ped model has a head animation with the default filename.
    // PARAMS:
    //    bfoundDefaultFaceAnimation - if it has a head animation
    void SetFoundDefaultFaceAnimation( bool bfoundDefaultFaceAnimation );

    // PURPOSE: Retrieves the name of the animation filename as it should be in the face directory.  It will
    //    be in the format "$(pFaceDir)\$(GetDisplayName())_face.anim".
    // PARAMS:
    //    pFaceDir - the directory containing the face animation files
    // RETURNS:  The filename for the default face animation
	atHashString GetDefaultFaceAnimationFilename( const char *pFaceDir ) const;

    // PURPOSE: Returns whether or not we are overriding the default face animation filename.
    // RETURNS: True if the default filename is being overridden, otherwise false.
    bool OverrideFaceAnimation() const;

    // PURPOSE: Sets the default face animation override.
    void SetOverrideFaceAnimation( bool bOverrideFaceAnimation );


    // PURPOSE: Retrieves the overridden face animation filename.
    // RETURNS:  The filename for the overridden face animation
   atHashString GetOverrideFaceAnimationFilename() const;


    // PURPOSE: Sets the face animation override filename.
    // PARAMS:
    //    pFilename - the name of the face animation file.
    void SetOverrideFaceAnimationFilename( const char *pFilename );


	// PURPOSE: Retrieves the face animation node name.
	// RETURNS:  The name of the face animation node in the scene
	atHashString GetFaceAnimationNodeName() const;

	// PURPOSE: Sets the face animation node name.
	// PARAMS:
	//    pNodeName - the name of the face animation node.
	void SetFaceAnimationNodeName( const char *pNodeName );

	// PURPOSE: Sets the face animation node name.
	// PARAMS:
	//    pNodeName - the name of the face animation node.
	void SetFaceAnimationNodeName( atHashString pNodeName );

	// PURPOSE: Retrieves the face attributes filename.
	// RETURNS:  The filename of the face attributes file. 
	atHashString GetFaceAttributesFilename() const;

	// PURPOSE: Sets the face attributes filename.
	// PARAMS:
	//    pFilename - the filename of the face attributes file. node.
	void SetFaceAttributesFilename( const char *pFilename );
	
	// PURPOSE: Sets the face attributes filename.
	// PARAMS:
	//    pFilename - the filename of the face attributes file. node.
	void SetFaceAttributesFilename( atHashString pFilename );

    // PURPOSE: Returns whether this ped model's face animation has been merged with its body animation
    // RETURNS: True if the face and body animations have been merged.
    bool FaceAndBodyAreMerged() const;

    // PURPOSE: Sets whether this ped model has a head animation.
    // PARAMS:
    //    bFaceAndBodyAreCombined - if it has a head animation
    void SetFaceAndBodyAreMerged( bool bFaceAndBodyAreMerged );

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

    PAR_PARSABLE;

protected:
    atHashString m_overrideFaceAnimationFilename;
    mutable atHashString m_defaultFaceAnimationFilename;
    
	bool m_bFoundFaceAnimation;
    bool m_bFaceAndBodyAreMerged;
    bool m_bOverrideFaceAnimation;

	atHashString m_faceAnimationNodeName;
	atHashString m_faceAttributesFilename;
};

inline bool cutfPedModelObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return strstr( lwrName, ":ped" ) || strstr( lwrName, ":skel_root" ) || strstr( lwrName, ":dummy" ) || strstr( lwrName, ":char" ) || strstr( lwrName, ":player" ) || strstr( lwrName, ":plyr" );
}

inline bool cutfPedModelObject::IsThisType( const char *pName ) const
{
    return cutfPedModelObject::StaticIsThisType( pName );
}

inline const char* cutfPedModelObject::GetStaticTypeName()
{
    return "Ped Model";
}

inline const char* cutfPedModelObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline s32 cutfPedModelObject::GetModelType() const
{
    return CUTSCENE_PED_MODEL_TYPE;
}

inline bool cutfPedModelObject::FoundDefaultFaceAnimation() const
{
    return m_bFoundFaceAnimation;
}

inline void cutfPedModelObject::SetFoundDefaultFaceAnimation( bool bfoundDefaultFaceAnimation )
{
    m_bFoundFaceAnimation = bfoundDefaultFaceAnimation;
    
    if ( !m_bFoundFaceAnimation )
    {
        m_bFaceAndBodyAreMerged = false;
    }
}

inline bool cutfPedModelObject::FaceAndBodyAreMerged() const
{
    return m_bFaceAndBodyAreMerged;
}

inline void cutfPedModelObject::SetFaceAndBodyAreMerged( bool bFaceAndBodyAreMerged )
{
    m_bFaceAndBodyAreMerged = bFaceAndBodyAreMerged;
}

inline bool cutfPedModelObject::OverrideFaceAnimation() const
{
    return m_bOverrideFaceAnimation;
}

inline void cutfPedModelObject::SetOverrideFaceAnimation( bool bOverrideFaceAnimation )
{
    m_bOverrideFaceAnimation = bOverrideFaceAnimation;
}

inline atHashString cutfPedModelObject::GetOverrideFaceAnimationFilename() const
{
    return m_overrideFaceAnimationFilename;
}

inline void cutfPedModelObject::SetOverrideFaceAnimationFilename( const char *pOverrideFaceAnimationFilename )
{
    if ( pOverrideFaceAnimationFilename != NULL )
    {
        m_overrideFaceAnimationFilename = pOverrideFaceAnimationFilename;
    }
    else
    {
        m_overrideFaceAnimationFilename.Clear();
    }
}

inline void cutfPedModelObject::SetFaceAnimationNodeName( const char *pNodeName )
{
	if ( pNodeName != NULL )
	{
		m_faceAnimationNodeName = pNodeName;
	}
	else
	{
		m_faceAnimationNodeName.Clear();
	}
}

inline void cutfPedModelObject::SetFaceAnimationNodeName( atHashString pNodeName )
{
	if ( pNodeName.GetHash() != 0 )
	{
		m_faceAnimationNodeName = pNodeName;
	}
	else
	{
		m_faceAnimationNodeName.Clear();
	}
}

inline atHashString cutfPedModelObject::GetFaceAnimationNodeName() const
{
#if !__FINAL
	return m_faceAnimationNodeName.GetCStr();
#else
	return m_faceAnimationNodeName;
#endif
}


inline atHashString cutfPedModelObject::GetFaceAttributesFilename() const
{
#if !__FINAL
	return m_faceAttributesFilename.GetCStr();
#else
	return m_faceAttributesFilename;
#endif
}

inline void cutfPedModelObject::SetFaceAttributesFilename( const char *pFilename )
{
	if ( pFilename != NULL )
	{
		m_faceAttributesFilename = pFilename;
	}
	else
	{
		m_faceAttributesFilename.Clear();
	}
}

inline void cutfPedModelObject::SetFaceAttributesFilename( atHashString pFilename )
{
	if ( pFilename.GetHash() != 0 )
	{
		m_faceAttributesFilename = pFilename;
	}
	else
	{
		m_faceAttributesFilename.Clear();
	}
}


//##############################################################################

class cutfVehicleModelObject : public cutfModelObject
{
public:
    cutfVehicleModelObject();
    cutfVehicleModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile=NULL, const char *pAnimCompressionFile=NULL );
    cutfVehicleModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
        const atArray<atString> &removeBoneNameList ); 
    cutfVehicleModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
        const atArray<atString> &removeBoneNameList, const parAttributeList& attributes );
	cutfVehicleModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
		const atArray<atString> &removeBoneNameList, const parAttributeList& attributes );
	cutfVehicleModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, atHashString pAnimExportCtrlSpecFile, atHashString pAnimCompressionFile, 
		const atArray<atString> &removeBoneNameList, const parAttributeList& attributes );
    virtual ~cutfVehicleModelObject();

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Retrieves the model type.  Built-in types are given
    //    by the ECutsceneModelType enum.
    // RETURNS: The model type.
    virtual s32 GetModelType() const;

    // PURPOSE: Retrieves the list of bone names to be made invisible
    // RETURNS: The list of bone names to make invisible.
    const atArray<atString>& GetRemoveBoneNameList() const;

	void SetCanApplyRealDamage(bool bValue);

	bool GetCanApplyRealDamage() const;


    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

    PAR_PARSABLE;

protected:
    atArray<atString> m_cRemoveBoneNameList;
	bool m_bCanApplyRealDamage;
};

inline bool cutfVehicleModelObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return strstr( lwrName, ":chassis" ) || strstr( lwrName, ":vehicle" ) || strstr( lwrName, ":veh" ) || strstr( lwrName, "vehicle" );
}

inline bool cutfVehicleModelObject::IsThisType( const char *pName ) const
{
    return cutfVehicleModelObject::StaticIsThisType( pName );
}

inline const char* cutfVehicleModelObject::GetStaticTypeName()
{
    return "Vehicle Model";
}

inline const char* cutfVehicleModelObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline s32 cutfVehicleModelObject::GetModelType() const
{
    return CUTSCENE_VEHICLE_MODEL_TYPE;
}

inline const atArray<atString>& cutfVehicleModelObject::GetRemoveBoneNameList() const
{
    return m_cRemoveBoneNameList;
}

inline bool cutfVehicleModelObject::GetCanApplyRealDamage() const
{
	return m_bCanApplyRealDamage;
}

inline void cutfVehicleModelObject::SetCanApplyRealDamage(bool bValue)
{
	m_bCanApplyRealDamage = bValue;
}

//##############################################################################

class cutfPropModelObject : public cutfModelObject
{
public:
    cutfPropModelObject();
    cutfPropModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile=NULL, const char *pAnimCompressionFile=NULL );
    cutfPropModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
        const parAttributeList& attributes );
	
	cutfPropModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
		const parAttributeList& attributes );
	

	cutfPropModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, atHashString pAnimExportCtrlSpecFile, atHashString pAnimCompressionFile, 
		const parAttributeList& attributes );
    virtual ~cutfPropModelObject() {}

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Retrieves the model type.  Built-in types are given
    //    by the ECutsceneModelType enum.
    // RETURNS: The model type.
    virtual s32 GetModelType() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;
};

inline bool cutfPropModelObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return strstr( lwrName, ":prop" ) || strstr( lwrName, ":object" ) || strstr( lwrName, ":obj" );
}

inline bool cutfPropModelObject::IsThisType( const char *pName ) const
{
    return cutfPropModelObject::StaticIsThisType( pName );
}

inline const char* cutfPropModelObject::GetStaticTypeName()
{
    return "Prop Model";
}

inline const char* cutfPropModelObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline s32 cutfPropModelObject::GetModelType() const
{
    return CUTSCENE_PROP_MODEL_TYPE;
}

//##############################################################################

class cutfWeaponModelObject : public cutfModelObject
{
public:
	cutfWeaponModelObject();
	cutfWeaponModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const u32 u32GenericWeaponType, const char* pAnimExportCtrlSpecFile=NULL, const char *pAnimCompressionFile=NULL );
	cutfWeaponModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const u32 u32GenericWeaponType, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
		const parAttributeList& attributes );

	cutfWeaponModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, const u32 u32GenericWeaponType, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile, 
		const parAttributeList& attributes );
	
	cutfWeaponModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, const u32 u32GenericWeaponType, atHashString pAnimExportCtrlSpecFile, atHashString pAnimCompressionFile, 
		const parAttributeList& attributes );
	virtual ~cutfWeaponModelObject() {}

	// PURPOSE: Examines the string for indications that it is an object
	//    of this type.
	// PARAMS:
	//    pName - the string to examine
	// RETURNS: true if it could be this type, otherwise false
	static bool StaticIsThisType( const char *pName );

	// PURPOSE: Examines the string for indications that it is an object
	//    of this type.
	// PARAMS:
	//    pName - the string to examine
	// RETURNS: true if it could be this type, otherwise false
	virtual bool IsThisType( const char *pName ) const;

	// PURPOSE: Retrieves the type name of this object
	// RETURNS: The type name
	static const char* GetStaticTypeName();

	// PURPOSE: Retrieves the type name of this object
	// RETURNS: The type name
	virtual const char* GetTypeName() const;

	// PURPOSE: Retrieves the model type.  Built-in types are given
	//    by the ECutsceneModelType enum.
	// RETURNS: The model type.
	virtual s32 GetModelType() const;

	// PURPOSE: Creates a copy of this cut scene object, but with the given object id
	// PARAMS:
	//    iObjectId - the new object id to give the clone
	// RETURNS: A clone of this instance
	virtual cutfObject* Clone() const;

	u32 GetGenericWeaponType() const { return m_GenericWeaponType; }

	void SetGenericWeaponType(const u32 weaponType) { m_GenericWeaponType = weaponType; }

	PAR_PARSABLE;

protected:
	u32 m_GenericWeaponType; 

};

inline bool cutfWeaponModelObject::StaticIsThisType( const char *pName )
{
	char lwrName[128];
	safecpy( lwrName, pName, sizeof(lwrName) );
	strlwr( lwrName );

	return strstr( lwrName, ":weapon" ) || strstr( lwrName, "W_")  || strstr( lwrName, "w_" );
}

inline bool cutfWeaponModelObject::IsThisType( const char *pName ) const
{
	return cutfWeaponModelObject::StaticIsThisType( pName );
}

inline const char* cutfWeaponModelObject::GetStaticTypeName()
{
	return "Weapon Model";
}

inline const char* cutfWeaponModelObject::GetTypeName() const
{
	return GetStaticTypeName();
}

inline s32 cutfWeaponModelObject::GetModelType() const
{
	return CUTSCENE_WEAPON_MODEL_TYPE;
}



//##############################################################################

class cutfFindModelObject : public cutfNamedObject
{
public:
    cutfFindModelObject();
    cutfFindModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius );
    cutfFindModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius, const parAttributeList& attributes );
	 cutfFindModelObject( s32 iObjectId, atHashString pName, const Vector3& vPos, float fRadius, const parAttributeList& attributes );
    virtual ~cutfFindModelObject() {}

    // PURPOSE: Retrieves the model type.  Built-in types are given
    //    by the ECutsceneModelType enum.
    // RETURNS: The model type.
    virtual s32 GetModelType() const = 0;

    // PURPOSE: Gets the approximate position of this object
    // RETURNS: The approximate position
    // NOTES:
    const Vector3& GetPosition() const;

    // PURPOSE: Sets the approximate position of this object
    // PARAMS:
    //    vPos - the approximate position
    void SetPosition( const Vector3& vPos );

    // PURPOSE: Gets the radius around the position in which the object may found
    // RETURNS: The radius around the position
    float GetRadius() const;

    // PURPOSE: Sets the radius around the position in which the object may found
    // PARAMS:
    //    fRadius - The radius around the position
    void SetRadius( float fRadius );

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

    PAR_PARSABLE;

protected:
    Vector3 m_vPosition;
    float m_fRadius;
};

inline const Vector3& cutfFindModelObject::GetPosition() const
{
    return m_vPosition;
}

inline void cutfFindModelObject::SetPosition( const Vector3& vPos )
{
    m_vPosition = vPos;
}

inline float cutfFindModelObject::GetRadius() const
{
    return m_fRadius;
}

inline void cutfFindModelObject::SetRadius( float fRadius )
{
    m_fRadius = fRadius;
}

//##############################################################################

class cutfHiddenModelObject : public cutfFindModelObject
{
public:
    cutfHiddenModelObject();
    cutfHiddenModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius );
    cutfHiddenModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius, const parAttributeList& attributes );
	  cutfHiddenModelObject( s32 iObjectId, atHashString pName, const Vector3& vPos, float fRadius, const parAttributeList& attributes );
    virtual ~cutfHiddenModelObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Retrieves the model type.  Built-in types are given
    //    by the ECutsceneModelType enum.
    // RETURNS: The model type.
    virtual s32 GetModelType() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;
};

inline s32 cutfHiddenModelObject::GetType() const
{
    return CUTSCENE_HIDDEN_MODEL_OBJECT_TYPE;
}

inline const char* cutfHiddenModelObject::GetStaticTypeName()
{
    return "Hidden Model";
}

inline const char* cutfHiddenModelObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline s32 cutfHiddenModelObject::GetModelType() const
{
    return CUTSCENE_HIDDEN_MODEL_TYPE;
}

//##############################################################################

class cutfFixupModelObject : public cutfFindModelObject
{
public:
    cutfFixupModelObject();
    cutfFixupModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius );
    cutfFixupModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius, const parAttributeList& attributes );
	cutfFixupModelObject( s32 iObjectId, atHashString pName, const Vector3& vPos, float fRadius, const parAttributeList& attributes );
    virtual ~cutfFixupModelObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Retrieves the model type.  Built-in types are given
    //    by the ECutsceneModelType enum.
    // RETURNS: The model type.
    virtual s32 GetModelType() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;
};

inline s32 cutfFixupModelObject::GetType() const
{
    return CUTSCENE_FIXUP_MODEL_OBJECT_TYPE;
}

inline const char* cutfFixupModelObject::GetStaticTypeName()
{
    return "Fixup Model";
}

inline const char* cutfFixupModelObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline s32 cutfFixupModelObject::GetModelType() const
{
    return CUTSCENE_FIXUP_MODEL_TYPE;
}

//##############################################################################

class cutfBlockingBoundsObject : public cutfNamedObject
{
public:
    cutfBlockingBoundsObject();
    cutfBlockingBoundsObject( s32 iObjectId, const char *pName, const Vector3 vCorners[4], float fHeight );
    cutfBlockingBoundsObject( s32 iObjectId, const char *pName, const Vector3 vCorners[4], float fHeight, const parAttributeList& attributes );
	 cutfBlockingBoundsObject( s32 iObjectId, atHashString Name, const Vector3 vCorners[4], float fHeight, const parAttributeList& attributes );
    virtual ~cutfBlockingBoundsObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    // PURPOSE: Get the position of the indexed corner
    // PARAMS:
    //    iIndex - the index of the corner to retrieve
    // RETURNS: the specified corner
    const Vector3& GetCorner( int iIndex ) const;

    // PURPOSE: Sets the position of the indexed corner
    // PARAMS:
    //    iIndex - the index of the corner to set
    //    vCorner - the position of the corner
    void SetCorner( int iIndex, const Vector3 &vCorner );

    // PURPOSE: Gets the height of the area
    // RETURNS: The height of the area
    float GetHeight() const;

    // PURPOSE: Sets the height of the area
    // PARAMS:
    //    fHeight - the height of the area
    void SetHeight( float fHeight );

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

    PAR_PARSABLE;

protected:
    Vector3 m_vCorners[4];
    float m_fHeight;
};

inline s32 cutfBlockingBoundsObject::GetType() const
{
    return CUTSCENE_BLOCKING_BOUNDS_OBJECT_TYPE;
}

inline bool cutfBlockingBoundsObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return strstr( lwrName, "blockbound" ) || strstr( lwrName, "blockingbound" );
}

inline bool cutfBlockingBoundsObject::IsThisType( const char *pName ) const
{
    return cutfBlockingBoundsObject::StaticIsThisType( pName );
}

inline const char* cutfBlockingBoundsObject::GetStaticTypeName()
{
    return "Blocking Bounds";
}

inline const char* cutfBlockingBoundsObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline const Vector3& cutfBlockingBoundsObject::GetCorner( int iIndex ) const
{
    cutfAssertf( (iIndex >= 0) && (iIndex < 4), "Out of Range" );
    return m_vCorners[iIndex];
}

inline void cutfBlockingBoundsObject::SetCorner( int iIndex, const Vector3& vCorner )
{
    if ( cutfVerifyf( (iIndex >= 0) && (iIndex < 4), "Out of Range" ) )
    {
		Vector3 vCornerCopy = vCorner;

		// Z-up the corner
		float y = vCornerCopy.y;
		vCornerCopy.x = -vCornerCopy.x;
		vCornerCopy.y = vCornerCopy.z;
		vCornerCopy.z = y;

        m_vCorners[iIndex] = vCornerCopy;
    }
}

inline float cutfBlockingBoundsObject::GetHeight() const
{
    return m_fHeight;
}

inline void cutfBlockingBoundsObject::SetHeight( float fHeight )
{
    m_fHeight = fHeight;
}

//##############################################################################

class cutfRemovalBoundsObject : public cutfBlockingBoundsObject
{
public:
    cutfRemovalBoundsObject();
    cutfRemovalBoundsObject( s32 iObjectId, const char *pName, const Vector3 vCorners[4], float fHeight );
    cutfRemovalBoundsObject( s32 iObjectId, const char *pName, const Vector3 vCorners[4], float fHeight, const parAttributeList& attributes );
    cutfRemovalBoundsObject( s32 iObjectId, atHashString Name, const Vector3 vCorners[4], float fHeight, const parAttributeList& attributes );
	virtual ~cutfRemovalBoundsObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;
};

inline s32 cutfRemovalBoundsObject::GetType() const
{
    return CUTSCENE_REMOVAL_BOUNDS_OBJECT_TYPE;
}

inline bool cutfRemovalBoundsObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return strstr( lwrName, "removebound" ) || strstr( lwrName, "removalbound" );
}

inline bool cutfRemovalBoundsObject::IsThisType( const char *pName ) const
{
    return cutfRemovalBoundsObject::StaticIsThisType( pName );
}

inline const char* cutfRemovalBoundsObject::GetStaticTypeName()
{
    return "Removal Bounds";
}

inline const char* cutfRemovalBoundsObject::GetTypeName() const
{
    return GetStaticTypeName();
}

//##############################################################################

class cutfLightObject : public cutfNamedObject
{
public:
    cutfLightObject();
    cutfLightObject( s32 iObjectId, const char *pName, s32 iLightType, s32 iLightProperty );
    cutfLightObject( s32 iObjectId, const char *pName, const Vector3& vPosition, const Vector3& vDirection, s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, const parAttributeList& attributes );
	cutfLightObject( s32 iObjectId, const char *pName, const Vector3& vPosition, const Vector3& vDirection, 
		s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
		const Vector4& vVolumeOuterColourAndIntensity, float fVolumeIntensity, float fVolumeSizeScale, float fCoronaSize,
		float fCoronaIntensity, float fCoronaZBias, s32 TextureDict, s32 TextureKey, u32 LightFlags, u32 HourFlags, u8 Flashiness, 
		float InnerConeAngle, float ExponentialFallOff, float ShadowBlur, const parAttributeList& attributes );
	cutfLightObject( s32 iObjectId, atHashString Name, const Vector3& vPosition, const Vector3& vDirection, 
		s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
		const Vector4& vVolumeOuterColourAndIntensity, float fVolumeIntensity, float fVolumeSizeScale, float fCoronaSize,
		float fCoronaIntensity, float fCoronaZBias, s32 TextureDict, s32 TextureKey, u32 LightFlags, u32 HourFlags, u8 Flashiness, 
		float InnerConeAngle, float ExponentialFallOff, float ShadowBlur, const parAttributeList& attributes );
	
	virtual ~cutfLightObject() {}

	enum eLightProperty
	{
		LP_NO_PROPERTY,
		LP_CASTS_SHADOWS,
		LP_ENVIRONMENT
	};

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Retrieves the type of this light.
    // RETURNS: The light type
    s32 GetLightType() const;

	// PURPOSE: Retrieves the property of this light.
	// RETURNS: The light property
	s32 GetLightProperty() const;

	// PURPOSE: Sets the property of this light.
	// PARAMS:
	//    iLightProperty - the light property
	void SetLightProperty( s32 iLightProperty );

    // PURPOSE: Sets the type of this light.
    // PARAMS:
    //    iLightType - the light type
    void SetLightType( s32 iLightType );

    // PURPOSE: Retrieves the light type name of this object
    // RETURNS: The light type name
    const char* GetLightTypeName() const;

    // PURPOSE: Retrieves the light type name of this object
    //    iLightType - the light type
    // RETURNS: The light type name
    static const char* GetStaticLightTypeName( s32 iLightType );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

	Vector3 GetLightPosition()						const { return m_vPosition; };
	Vector3 GetLightDirection()						const { return m_vDirection; };
	float GetLightFallOff()							const { return m_fFallOff; };
	float GetLightIntensity()						const { return m_fIntensity; };
	float GetLightConeAngle()						const { return m_fConeAngle; };
	Vector3 GetLightColour()						const { return m_vColour; };	
	u32 GetLightFlags()								const { return m_uLightFlags; };
	u32 GetLightHourFlags()							const { return m_uHourFlags; }; 
	bool GetLightFlag(ECutsceneLightFlag nFlag)		const { return (m_uLightFlags & nFlag) != 0; } 
	bool GetLightHourFlag(u32 nFlag)				const { return (m_uHourFlags & nFlag) != 0; } 
	float GetVolumeIntensity()						const { return m_fVolumeIntensity; };
	bool IsLightStatic()							const { return m_bStatic; };
	float GetVolumeSizeScale()						const { return m_fVolumeSizeScale; };
	Vector4 GetVolumeOuterColourAndIntensity()		const { return m_vVolumeOuterColourAndIntensity; }; 
	s32 GetTextureDict()							const { return m_TextureDictID; }
	s32 GetTextureKey()								const { return m_TextureKey; }
	float GetCoronaSize()							const { return m_fCoronaSize; }
	float GetCoronaIntensity()						const { return m_fCoronaIntensity; }
	float GetCoronaZBias()							const { return m_fCoronaZBias; };
	u8 GetFlashiness()								const { return m_uFlashiness; };
	float GetExponentialFallOff()					const { return m_fExponentialFallOff; };
	float GetInnerConeAngle()						const { return m_fInnerConeAngle; };
	float GetShadowBlur()							const { return m_fShadowBlur; };

	void SetLightPosition( const Vector3 &vPosition )			{ m_vPosition = vPosition; };
	void SetLightDirection( const Vector3 &vDirection )			{ m_vDirection = vDirection; };
	void SetLightFallOff( const float fFallOff )				{ m_fFallOff = fFallOff; };
	void SetLightConeAngle( const float fConeAngle )			{ m_fConeAngle = fConeAngle; };
	void SetLightStatic( bool bStatic )							{ m_bStatic = bStatic; };
	void SetLightColour( const Vector3& vColour )				{ m_vColour = vColour; };
	void SetLightIntensity( const float fIntensity )			{ m_fIntensity = fIntensity; };
	void SetLightFlags( ECutsceneLightFlag flags)				{ m_uLightFlags = flags; };
	void SetLightHourFlags( u32 flags)							{ m_uHourFlags = flags; }; 
	void SetLightFlag(u32 nFlag)								{ m_uLightFlags |= nFlag; };
	void SetLightHourFlag(u32 nFlag)							{ m_uHourFlags |= nFlag; }; 
	void SetVolumeIntensity(float fVolumeIntensity)				{ m_fVolumeIntensity = fVolumeIntensity; };
	void SetVolumeSizeScale(float fVolumeSizeScale)				{ m_fVolumeSizeScale = fVolumeSizeScale; };
	void SetVolumeOuterColourAndIntensity(const Vector4& VolumeOuterColourAndIntensity) { m_vVolumeOuterColourAndIntensity = VolumeOuterColourAndIntensity; };  
	void SetTextureDict(s32 Texture)							{  m_TextureDictID = Texture; };
	void SetTextureKey(s32 TextureDict)							{  m_TextureKey = TextureDict; };
	void SetCoronaSize(float coronaSize)						{  m_fCoronaSize = coronaSize; };
	void SetCoronaIntensity(float coronaIntensity)				{  m_fCoronaIntensity = coronaIntensity; };
	void SetCoronaZBias(float coronaZBias)						{  m_fCoronaZBias = coronaZBias; };
	void SetFlashiness(u8 flashiness)							{  m_uFlashiness = flashiness; };
	void SetExponentialFallOff(float ExpoFallOff)				{  m_fExponentialFallOff = ExpoFallOff; };
	void SetInnerConeAngle(float InnerConeAngle)				{  m_fInnerConeAngle = InnerConeAngle; };
	void SetShadowBlur(float ShadowBlur)						{  m_fShadowBlur = ShadowBlur; };


	void SetActivateTime(float fActivateTime) const { m_fActivateTime = fActivateTime; }
	float GetActivateTime() const { return m_fActivateTime; }

	PAR_PARSABLE;

protected:
   	Vector4 m_vVolumeOuterColourAndIntensity; 
	Vector3 m_vDirection;
	Vector3 m_vColour;
	Vector3 m_vPosition;
	
	float m_fIntensity;
	float m_fFallOff;
	float m_fConeAngle; 
	float m_fVolumeIntensity;
	float m_fVolumeSizeScale; 
	float m_fCoronaSize; 
	float m_fCoronaIntensity; 
	float m_fCoronaZBias; 
	float m_fInnerConeAngle; 
	float m_fExponentialFallOff; 
	float m_fShadowBlur;

	s32 m_iLightType;
	s32 m_iLightProperty;
	s32 m_TextureDictID; 
	s32 m_TextureKey; 
	s32 m_AttachParentId; 

	u32 m_uLightFlags; 
	u32 m_uHourFlags;
	
	u16 m_AttachBoneHash;
	
	bool m_bStatic; 

	u8 m_uFlashiness; 

	mutable float m_fActivateTime;
};

inline s32 cutfLightObject::GetType() const
{
    return CUTSCENE_LIGHT_OBJECT_TYPE;
}

inline s32 cutfLightObject::GetLightType() const
{
    return m_iLightType;
}

inline void cutfLightObject::SetLightType( s32 iLightType )
{
    m_iLightType = iLightType;
}

inline s32 cutfLightObject::GetLightProperty() const
{
	return m_iLightProperty;
}

inline void cutfLightObject::SetLightProperty( s32 iLightProperty )
{
	m_iLightProperty = iLightProperty;
}

inline const char* cutfLightObject::GetLightTypeName() const
{
    return cutfLightObject::GetStaticLightTypeName( GetLightType() );
}

inline const char* cutfLightObject::GetStaticLightTypeName( s32 iLightType )
{
    switch ( iLightType )
    {
    case CUTSCENE_DIRECTIONAL_LIGHT_TYPE:
        return "Directional Light";
    case CUTSCENE_POINT_LIGHT_TYPE:
        return "Point Light";
    case CUTSCENE_SPOT_LIGHT_TYPE:
        return "Spot Light";
    default:
        return "<Unknown>";
    }
}

inline bool cutfLightObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return (strstr( lwrName, "light" ) != NULL);
}

inline bool cutfLightObject::IsThisType( const char *pName ) const
{
    return cutfLightObject::StaticIsThisType( pName );
}

inline const char* cutfLightObject::GetStaticTypeName()
{
    return "Light";
}

inline const char* cutfLightObject::GetTypeName() const
{
    return GetStaticTypeName();
}

//##############################################################################

class cutfAnimatedLightObject : public cutfLightObject
{
public:
	cutfAnimatedLightObject();
	cutfAnimatedLightObject( s32 iObjectId, const char *pName, s32 iLightType, s32 iLightProperty, u32 AnimNameBase );
	cutfAnimatedLightObject( s32 iObjectId, const char *pName, const Vector3& vPosition, const Vector3& vDirection, s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, u32 AnimNameBase, const parAttributeList& attributes );
	cutfAnimatedLightObject( s32 iObjectId, const char *pName, const Vector3& vPosition, const Vector3& vDirection, 
		s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
		const Vector4& vVolumeOuterColourAndIntensity, float fVolumeIntensity, float fVolumeSizeScale, float fCoronaSize,
		float fCoronaIntensity, float fCoronaZBias, s32 TextureDict, s32 TextureKey, u32 LightFlags, u32 HourFlags, u8 Flashiness, 
		float InnerConeAngle, float ExponentialFallOff, float ShadowBlur, u32 AnimNameBase , const parAttributeList& attributes );
	cutfAnimatedLightObject( s32 iObjectId, atHashString Name, const Vector3& vPosition, const Vector3& vDirection, 
		s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
		const Vector4& vVolumeOuterColourAndIntensity, float fVolumeIntensity, float fVolumeSizeScale, float fCoronaSize,
		float fCoronaIntensity, float fCoronaZBias, s32 TextureDict, s32 TextureKey, u32 LightFlags, u32 HourFlags, u8 Flashiness, 
		float InnerConeAngle, float ExponentialFallOff, float ShadowBlur, u32 AnimNameBase ,const parAttributeList& attributes );
	
	virtual ~cutfAnimatedLightObject() {}
	
	// PURPOSE: Retrieves the type of this object so it can be cast
	//    to the appropriate derived type.  Built-in type ids are given
	//    by the ECutsceneObjectType enum.
	// RETURNS: The type.
	s32 GetType() const;

	// PURPOSE: Gets the name of this object
	// RETURNS: The name of this object
	// NOTES: Models might be named something like "model1:Char", so this will return "model" used for streaming.
	u32 GetAnimStreamingBase() const;

	// PURPOSE: Sets the name of this object
	// PARAMS:
	//    pName - the name of this object
	void SetAnimStreamingBase(const char* Name);

	virtual u32 GetAnimStreamingType() const { return CUTSCENE_ANIMATED_LIGHT_STREAMED_OBJECT; }

	// PURPOSE: Creates a copy of this cut scene object, but with the given object id
	// PARAMS:
	//    iObjectId - the new object id to give the clone
	// RETURNS: A clone of this instance
	virtual cutfObject* Clone() const;

	virtual bool operator == ( const cutfObject &other ) const;
	virtual bool operator != ( const cutfObject &other ) const;

	PAR_PARSABLE;

protected:
	u32 m_AnimStreamingBase; 
};

inline s32 cutfAnimatedLightObject::GetType() const
{
	return CUTSCENE_ANIMATED_LIGHT_OBJECT_TYPE;
}

inline u32 cutfAnimatedLightObject::GetAnimStreamingBase() const
{
	return m_AnimStreamingBase;
}

//##############################################################################

class cutfCameraObject : public cutfNamedAnimatedObject
{
public:
    cutfCameraObject();
    cutfCameraObject( s32 iObjectId, const char *pName, u32 AnimBaseHash, float fNearDrawDistance=-1.0f, float fFarDrawDistance=-1.0f );
    cutfCameraObject( s32 iObjectId, const char *pName, u32 AnimBaseHash, float fNearDrawDistance, float fFarDrawDistance, const parAttributeList& attributes );
	 cutfCameraObject( s32 iObjectId, atHashString Name, u32 AnimBaseHash, float fNearDrawDistance, float fFarDrawDistance, const parAttributeList& attributes );
    virtual ~cutfCameraObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Retrieves the near draw distance (clip plane) to be used before any 
    //    CUTSCENE_SET_DRAW_DISTANCE_EVENTs, if any.
    // RETURNS: The near draw distance.  -1 to use the game's default value.
    float GetNearDrawDistance() const;

    // PURPOSE: Sets the near draw distance (clip plane) to be used before any 
    //    CUTSCENE_SET_DRAW_DISTANCE_EVENTs, if any.
    // PARAMS:
    //    dist - the draw distance
    // NOTES: A negative number indicates that we should use the game's default value.
    void SetNearDrawDistance( float dist );

    // PURPOSE: Retrieves the far draw distance (clip plane) to be used before any 
    //    CUTSCENE_SET_DRAW_DISTANCE_EVENTs, if any.
    // RETURNS: The far draw distance.  -1 to use the game's default value.
    float GetFarDrawDistance() const;

    // PURPOSE: Sets the far draw distance (clip plane) to be used before any 
    //    CUTSCENE_SET_DRAW_DISTANCE_EVENTs, if any.
    // PARAMS:
    //    dist - the draw distance
    // NOTES: A negative number indicates that we should use the game's default value.
    void SetFarDrawDistance( float dist );

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;

private:
    float m_fNearDrawDistance;
    float m_fFarDrawDistance;
};

inline s32 cutfCameraObject::GetType() const
{
    return CUTSCENE_CAMERA_OBJECT_TYPE;
}

inline bool cutfCameraObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return (strstr( lwrName, "camera" ) != NULL) && (strstr( lwrName, "dof" ) == NULL) && !cutfScreenFadeObject::StaticIsThisType( pName );
}

inline bool cutfCameraObject::IsThisType( const char *pName ) const
{
    return cutfCameraObject::StaticIsThisType( pName );
}

inline const char* cutfCameraObject::GetStaticTypeName()
{
    return "Camera";
}

inline const char* cutfCameraObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline float cutfCameraObject::GetNearDrawDistance() const
{
    return m_fNearDrawDistance;
}

inline void cutfCameraObject::SetNearDrawDistance( float dist )
{
    m_fNearDrawDistance = dist;
}

inline float cutfCameraObject::GetFarDrawDistance() const
{
    return m_fFarDrawDistance;
}

inline void cutfCameraObject::SetFarDrawDistance( float dist )
{
    m_fFarDrawDistance = dist;
}

//##############################################################################

class cutfAudioObject : public cutfFinalNamedObject
{
public:
    struct SAudioData
    {
        SAudioData()
            : fStartOffset(0.0f)
        {
            cAudioName[0] = 0;
        }

        SAudioData( const char *pAudioName, float fOffset )
            : fStartOffset(fOffset)
        {
            safecpy( cAudioName, pAudioName, sizeof(cAudioName) );
        }

        char cAudioName[CUTSCENE_OBJNAMELEN];
        float fStartOffset;

        PAR_SIMPLE_PARSABLE;
    };

    cutfAudioObject();
    cutfAudioObject( s32 iObjectId, const char *pName, const float fOffset );
    cutfAudioObject( s32 iObjectId, const char *pName, const float fOffset, const parAttributeList& attributes );
	virtual ~cutfAudioObject() {}


    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

//#if !__FINAL
    // PURPOSE: Retrieves the display name of this object
    // RETURNS: The display name
    virtual const atString& GetDisplayName() const;
//#endif

	void SetOffset(const float fOffset);

	float GetOffset() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

    PAR_PARSABLE;

protected:
	float m_fOffset;
};

inline s32 cutfAudioObject::GetType() const
{
    return CUTSCENE_AUDIO_OBJECT_TYPE;
}

inline bool cutfAudioObject::IsThisType( const char *pName ) const
{
    return cutfAudioObject::StaticIsThisType( pName );
}

inline const char* cutfAudioObject::GetStaticTypeName()
{
    return "Audio";
}

inline const char* cutfAudioObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline void cutfAudioObject::SetOffset(const float fOffset)
{
	m_fOffset = fOffset;
}

inline float cutfAudioObject::GetOffset() const
{
	return m_fOffset;
}

//##############################################################################

class cutfEventObject : public cutfObject
{
public:
    cutfEventObject();
    cutfEventObject( s32 iObjectId );
    cutfEventObject( s32 iObjectId, const parAttributeList& attributes );
    virtual ~cutfEventObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

//#if !__FINAL
    // PURPOSE: Retrieves the display name of this object
    // RETURNS: The display name
    virtual const atString& GetDisplayName() const;
//#endif
    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;
};

inline s32 cutfEventObject::GetType() const
{
    return CUTSCENE_EVENTS_OBJECT_TYPE;
}

inline bool cutfEventObject::IsThisType( const char *pName ) const
{
    return cutfEventObject::StaticIsThisType( pName );
}

inline const char* cutfEventObject::GetStaticTypeName()
{
    return "Event";
}

inline const char* cutfEventObject::GetTypeName() const
{
    return GetStaticTypeName();
}

//#if !__FINAL
inline const atString& cutfEventObject::GetDisplayName() const
{
    if ( m_displayName.GetLength() == 0 )
    {
        m_displayName.Set( cutfEventObject::GetStaticTypeName(), (int)strlen( cutfEventObject::GetStaticTypeName() ), 0 );
    }
    
    return m_displayName;
}
//#endif
//##############################################################################

class cutfDecalObject : public cutfNamedStreamedObject
{
public:
	cutfDecalObject();
	cutfDecalObject(s32 iObjectId, const char *pName, const char *pStreamedName, u32 RenderId);
	cutfDecalObject(s32 iObjectId, const char *pName,  const char *pStreamedName, u32 RenderId, const parAttributeList& attributes);
	cutfDecalObject(s32 iObjectId, atHashString Name,  atHashString StreamedName, u32 RenderId, const parAttributeList& attributes);
	virtual ~cutfDecalObject() {}

	// PURPOSE: Retrieves the type of this object so it can be cast
	//    to the appropriate derived type.  Built-in type ids are given
	//    by the ECutsceneObjectType enum.
	// RETURNS: The type.
	s32 GetType() const;
	
	// PURPOSE: Examines the string for indications that it is an object
	//    of this type.
	// PARAMS:
	//    pName - the string to examine
	// RETURNS: true if it could be this type, otherwise false
	static bool StaticIsThisType( const char *pName );

	// PURPOSE: Examines the string for indications that it is an object
	//    of this type.
	// PARAMS:
	//    pName - the string to examine
	// RETURNS: true if it could be this type, otherwise false
	virtual bool IsThisType( const char *pName ) const;

	// PURPOSE: Retrieves the type name of this object
	// RETURNS: The type name
	static const char* GetStaticTypeName();

	// PURPOSE: Retrieves the type name of this object
	// RETURNS: The type name
	virtual const char* GetTypeName() const;
	
	// PARAMS:
	//    iObjectId - the new object id to give the clone
	// RETURNS: A clone of this instance
	virtual cutfObject* Clone() const;

	void SetRenderId(u32 RenderId);  

	u32 GetRenderId() const;

	 PAR_PARSABLE;

private:
	u32 m_RenderId; 
};

inline s32 cutfDecalObject::GetType() const
{
	return CUTSCENE_DECAL_OBJECT_TYPE;
}

inline void cutfDecalObject::SetRenderId(u32 RenderId) 
{
	m_RenderId = RenderId;
}

inline u32 cutfDecalObject::GetRenderId() const
{
	return m_RenderId; 
}

inline bool cutfDecalObject::StaticIsThisType( const char *pName )
{
	char lwrName[128];
	safecpy( lwrName, pName, sizeof(lwrName) );
	strlwr( lwrName );
	
	if( strstr(lwrName, ":decal" ) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

inline bool cutfDecalObject::IsThisType( const char *pName ) const
{
	return cutfDecalObject::StaticIsThisType( pName );
}

inline const char* cutfDecalObject::GetStaticTypeName()
{
	return "Decal";
}

inline const char* cutfDecalObject::GetTypeName() const
{
	return GetStaticTypeName();
}

//##############################################################################

// PURPOSE: This class is used for triggered particle effects which require no anim data
class cutfParticleEffectObject : public cutfNamedStreamedObject
{
public:
    cutfParticleEffectObject();
    cutfParticleEffectObject( s32 iObjectId, const char *pName, const char *pStreamedName, const atHashString athFxListHash);
    cutfParticleEffectObject( s32 iObjectId, const char *pName, const char *pStreamedName, const atHashString athFxListHash, const parAttributeList& attributes );
	cutfParticleEffectObject( s32 iObjectId, atHashString Name, atHashString StreamingName, const atHashString athFxListHash, const parAttributeList& attributes );
    
	virtual ~cutfParticleEffectObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;
	
	// PURPOSE: Sets the list the particle effect asset is stored in
	// PARAMS:
	void SetFxListHash(const char* ptrFxList);

	// PURPOSE: Get the list the particle effect asset is stored in
	// PARAMS:
	// RETURNS: at Hash string of the ptfx list
	atHashString GetFxListHash() const;

    virtual bool operator == ( const cutfObject &other ) const;
    virtual bool operator != ( const cutfObject &other ) const;

    PAR_PARSABLE;

protected:
    atHashString m_athFxListHash;
   
};

inline s32 cutfParticleEffectObject::GetType() const
{
    return CUTSCENE_PARTICLE_EFFECT_OBJECT_TYPE;
}

inline bool cutfParticleEffectObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return strstr( lwrName, "fx_" ) || strstr( lwrName, ":fx" ) || strstr( lwrName, ":effect" );
}

inline bool cutfParticleEffectObject::IsThisType( const char *pName ) const
{
    return cutfParticleEffectObject::StaticIsThisType( pName );
}

inline const char* cutfParticleEffectObject::GetStaticTypeName()
{
    return "Particle Effect";
}

inline const char* cutfParticleEffectObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline void cutfParticleEffectObject::SetFxListHash(const char* ptrFxList)
{
	m_athFxListHash = atHashString(ptrFxList);
}

inline atHashString cutfParticleEffectObject::GetFxListHash() const
{
	return m_athFxListHash;
}

//##############################################################################

class cutfAnimatedParticleEffectObject : public cutfNamedAnimatedStreamedObject
{
public:
    cutfAnimatedParticleEffectObject();
    cutfAnimatedParticleEffectObject( s32 iObjectId, const char *pName, const char *pStreamedName, u32 AnimStreamBase );
    cutfAnimatedParticleEffectObject( s32 iObjectId, const char *pName, const char *pStreamedName, u32 AnimStreamBase, const atHashString athFxListHash, const parAttributeList& attributes );
    cutfAnimatedParticleEffectObject( s32 iObjectId, atHashString Name, atHashString StreamedName, u32 AnimStreamBase, const atHashString athFxListHash, const parAttributeList& attributes );
	virtual ~cutfAnimatedParticleEffectObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;

    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

	// PURPOSE: Sets the list the particle effect asset is stored in
	// PARAMS:
	void SetFxListHash(const char* ptrFxList);

	// PURPOSE: Get the list the particle effect asset is stored in
	// PARAMS:
	// RETURNS: at Hash string of the ptfx list
	atHashString GetFxListHash() const;

    PAR_PARSABLE;

protected:
	atHashString m_athFxListHash;
};

inline s32 cutfAnimatedParticleEffectObject::GetType() const
{
    return CUTSCENE_ANIMATED_PARTICLE_EFFECT_OBJECT_TYPE;
}

inline bool cutfAnimatedParticleEffectObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return strstr( lwrName, ":fx" ) || strstr( lwrName, ":effect" );
}

inline bool cutfAnimatedParticleEffectObject::IsThisType( const char *pName ) const
{
    return cutfAnimatedParticleEffectObject::StaticIsThisType( pName );
}

inline const char* cutfAnimatedParticleEffectObject::GetStaticTypeName()
{
    return "Animated Particle Effect";
}

inline const char* cutfAnimatedParticleEffectObject::GetTypeName() const
{
    return GetStaticTypeName();
}

inline void cutfAnimatedParticleEffectObject::SetFxListHash(const char* ptrFxList)
{
	m_athFxListHash = atHashString(ptrFxList);
}

inline atHashString cutfAnimatedParticleEffectObject::GetFxListHash() const
{
	return m_athFxListHash;
}


//##############################################################################

class cutfAnimationManagerObject : public cutfObject
{
public:
    cutfAnimationManagerObject();
    cutfAnimationManagerObject( s32 iObjectId );
    cutfAnimationManagerObject( s32 iObjectId, const parAttributeList& attributes );
    virtual ~cutfAnimationManagerObject() {}

    // PURPOSE: Retrieves the type of this object so it can be cast
    //    to the appropriate derived type.  Built-in type ids are given
    //    by the ECutsceneObjectType enum.
    // RETURNS: The type.
    s32 GetType() const;

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    static bool StaticIsThisType( const char *pName );

    // PURPOSE: Examines the string for indications that it is an object
    //    of this type.
    // PARAMS:
    //    pName - the string to examine
    // RETURNS: true if it could be this type, otherwise false
    virtual bool IsThisType( const char *pName ) const;

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    static const char* GetStaticTypeName();

    // PURPOSE: Retrieves the type name of this object
    // RETURNS: The type name
    virtual const char* GetTypeName() const;
//#if !__FINAL
    // PURPOSE: Retrieves the display name of this object
    // RETURNS: The display name
    virtual const atString& GetDisplayName() const;
//#endif
    // PURPOSE: Creates a copy of this cut scene object, but with the given object id
    // PARAMS:
    //    iObjectId - the new object id to give the clone
    // RETURNS: A clone of this instance
    virtual cutfObject* Clone() const;

    PAR_PARSABLE;
};

inline s32 cutfAnimationManagerObject::GetType() const
{
    return CUTSCENE_ANIMATION_MANAGER_OBJECT_TYPE;
}

inline bool cutfAnimationManagerObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return (strstr( lwrName, "animmgr" ) != NULL) || (strstr( lwrName, "animmanager" ) != NULL) 
        || (strstr( lwrName, "animationmanager" ) != NULL);
}

inline bool cutfAnimationManagerObject::IsThisType( const char *pName ) const
{
    return cutfAnimationManagerObject::StaticIsThisType( pName );
}

inline const char* cutfAnimationManagerObject::GetStaticTypeName()
{
    return "Animation Manager";
}

inline const char* cutfAnimationManagerObject::GetTypeName() const
{
    return GetStaticTypeName();
}
//#if !__FINAL
inline const atString& cutfAnimationManagerObject::GetDisplayName() const
{
    if ( m_displayName.GetLength() == 0 )
    {
        m_displayName.Set( cutfAnimationManagerObject::GetStaticTypeName(), (int)strlen( cutfAnimationManagerObject::GetStaticTypeName() ), 0 );
    }

    return m_displayName;
}
//#endif
//##############################################################################

class cutfRayfireObject : public cutfNamedStreamedObject
{
public:
	cutfRayfireObject();
	cutfRayfireObject( s32 iObjectId, const char *pName, const char *pStreamingName, Vector3::Param vStartPosition, const parAttributeList& attributes);
	cutfRayfireObject( s32 iObjectId, atHashString Name, atHashString StreamingName, Vector3::Param vStartPosition, const parAttributeList& attributes);
	virtual ~cutfRayfireObject() {}

	s32 GetType() const;

	// PURPOSE: Set the position of the rayfire object
	// PARAMS:
	//    vStartPosition - the position of the rayfire object
	void SetStartPosition( Vector3::Param vStartPosition );

	// PURPOSE: Retrieve the position of the rayfire object
	// RETURNS: the position of the rayfire object
	Vector3 GetStartPosition() const ;

	// PURPOSE: Examines the string for indications that it is an object
	//    of this type.
	// PARAMS:
	//    pName - the string to examine
	// RETURNS: true if it could be this type, otherwise false
	static bool StaticIsThisType( const char *pName );

	// PURPOSE: Examines the string for indications that it is an object
	//    of this type.
	// PARAMS:
	//    pName - the string to examine
	// RETURNS: true if it could be this type, otherwise false
	virtual bool IsThisType( const char *pName ) const;

	// PURPOSE: Retrieves the type name of this object
	// RETURNS: The type name
	static const char* GetStaticTypeName();

	// PURPOSE: Retrieves the type name of this object
	// RETURNS: The type name
	virtual const char* GetTypeName() const;

	// PURPOSE: Creates a copy of this cut scene object, but with the given object id
	// PARAMS:
	//    iObjectId - the new object id to give the clone
	// RETURNS: A clone of this instance
	virtual cutfObject* Clone() const;

	PAR_PARSABLE;

protected:
	Vector3 m_vStartPosition;
};

inline s32 cutfRayfireObject::GetType() const
{
	return CUTSCENE_RAYFIRE_OBJECT_TYPE;
}

inline bool cutfRayfireObject::StaticIsThisType( const char *pName )
{
	char lwrName[128];
	safecpy( lwrName, pName, sizeof(lwrName) );
	strlwr( lwrName );

	return (strstr( lwrName, "des_" ) != NULL);
}

inline bool cutfRayfireObject::IsThisType( const char *pName ) const
{
	return cutfRayfireObject::StaticIsThisType( pName );
}

inline const char* cutfRayfireObject::GetStaticTypeName()
{
	return "Rayfire";
}

inline const char* cutfRayfireObject::GetTypeName() const
{
	return GetStaticTypeName();
}

//##############################################################################

} // namespace rage

#endif // CUTFILE_CUTFOBJECT_H 
