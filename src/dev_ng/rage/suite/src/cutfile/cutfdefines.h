// 
// cutfile/cutfdefines.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTFILE_CUTFDEFINES_H 
#define CUTFILE_CUTFDEFINES_H 

namespace rage {

//Debug properties
#define CUTSCENE_LIGHT_EVENT_LOGGING (0)
#define CUTSCENE_VISIBILITY_DEBUG (0)
#define CUTSCENE_VARIATION_DEBUG (0)

	//Scene Properties
#define CUTSCENE_FPS (30.0f)
#define CUTSCENE_MINIMUM_LENGTH_FRAMES (30)
#define CUTSCENE_LIGHT_EVENT_OFFSET (2.0f * (1.0f / CUTSCENE_FPS))

// Screen Fade times
#define CUTSCENE_FADE_TIME	        (0.8f)  // time in seconds for the fades in & out of cutscenes
#define CUTSCENE_DEFAULT_FADE_TIME	(0.4f)  // time in seconds for the force fades (skips) in & out of cutscenes
#define CUTSCENE_EXTENDED_FADE_TIME	(2.5f)  // some cutscenes require a longer fade at the end - defined by flag in cut file
#define CUTSCENE_SHORT_FADE_TIME	(0.25f) // some cutscenes require a very quick fade at the end - defined by flag in cut file

// Draw Distance
#define CUTSCENE_DEFAULT_NEAR_CLIP (0.15f)
#define CUTSCENE_DEFAULT_FAR_CLIP   (800.0f)

#define CUTSCENE_FILE_VERSION_NUMBER (1)

#define CUTSCENE_EXPORT_EVENTS_WITH_ZERO_BASE_TIME (0)

// lengths of strings:
#if HACK_GTA4
	#define CUTSCENE_NAME_LEN		(24)  // max number of chars for the cutscene name
#else
	#define CUTSCENE_NAME_LEN		(8)  // max number of chars for the cutscene name
#endif

#define CUTSCENE_OBJNAMELEN		(64)  // max number of chars in the object name
#define CUTSCENE_LONG_OBJNAMELEN (64)  // max number of chars in the object name
#define CUTSCENE_MAX_CONCAT     (40) // max number of cutscenes that can be concatenated together
#define CUTSCENE_EFFECTNAMELEN	(50)
#define CUTSCENE_EFFECTAUDIONAMELEN	(16)
#define MAX_EXTRA_ROOM_CHARS	(24)
#define MISSION_NAME_LENGTH	(8)
#define TEXT_KEY_SIZE		(16)

#define MAX_EVOLUTION_EFFECT_VALUES (4)
#define MAX_EVOLUTION_EFFECT_NAME_LEN 20

    // for cutscenes
// #define PI_CUTSCENE_FILE_EXT "cut"  
#define PI_CUTSCENE_XMLFILE_EXT "cutxml"
#define PI_CUTSCENE_BINFILE_EXT "cutbin"  
#define PI_CUTSCENE_TUNEFILE_EXT "cuttune"
#define PI_CUTSCENE_SUBTITLEFILE_EXT "cutsub"
#define PI_CUTSCENE_LIGHTFILE_EXT "lightxml"

    // max obj numbers:
#define MAX_CUTSCENE_OBJS		(40)  // max number of entities/objects setup & loaded in at one time in a cutscene
#define MAX_CUTSCENE_ENTITIES	(70)  // max number of objects in a section
#define MAX_CUTSCENE_VEHICLES	(5)  // max number of vehicle details we hold per section
#define MAX_BONES_TO_REMOVE		(10)
#define MAX_CUTSCENE_VEHICLE_REMOVALS	(MAX_CUTSCENE_VEHICLES*MAX_BONES_TO_REMOVE)
#define MAX_CUTSCENE_LIGHTS		(10)  // max number of animated lights in cutscene
#define MAX_CUTSCENE_EFFECTS	(50)  // max number of animated particle effects in cutscene
#define MAX_EVOLUTION_EFFECT_VALUES (4)
#define MAX_DRAW_DISTANCES		(100)  // max number of different draw distances in one whole scene
#define MAX_CUTSCENE_BLOCKING_BOUNDS	(10)  // max number of animated particle effects in cutscene
#define MAX_CUTSCENE_VARIATIONS	(100)  // max number of variations
#define MAX_CUTSCENE_ATTACHMENTS (1)  // max number of attachments - not sure if we are going to need this, so set to 1 instance for now
#define MAX_CUTSCENE_PROPS		(50)  // max number of props
#define MAX_HIDDEN_OBJS			(20)  // max hidden objects
#define MAX_FIXUP_OBJS			(50)  // max fixup objects
#define MAX_TEXTOUTPUT			(500)  // max number of text ids per cutscene
#define MAX_CAMCORDER_OVERLAYS	(30)

#define MAX_CUTSCENE_SECTIONS	(15)  // max number of sections in a cutscene
#define MINIMUM_CUTSCENE_SECTION_TIME (1) // minimum time a cutscene section should be (seconds)
#define MINIMUM_OLD_CUTSCENE_SECTION_TIME (3) // minimum time a cutscene section could be, before this was reduced for DLC data (seconds)

//
// flags for the cut file:
//
#define CUTSCENE_FLAGS_FADE_BETWEEN_SECTIONS	(1<<0)
#define CUTSCENE_FLAGS_NO_VEHICLE_LIGHTS		(1<<1)
#define CUTSCENE_FLAGS_LONG_FADE_AT_END			(1<<2)
#define CUTSCENE_FLAGS_FADE_IN_OUT_GAME_SCREEN	(1<<3)
#define CUTSCENE_FLAGS_DONT_FADE_IN	            (1<<4)
#define CUTSCENE_FLAGS_USE_ONE_AUDIO			(1<<5)
#define CUTSCENE_FLAGS_MUTE_MUSIC_PLAYER		(1<<6)
#define CUTSCENE_FLAGS_LEAK_RADIO				(1<<7)
#define CUTSCENE_FLAGS_DONT_FADE_CUTSCENE_OUT	(1<<8)
#define CUTSCENE_FLAGS_NO_AMBIENT_LIGHTS		(1<<9)
#define CUTSCENE_FLAGS_TRANSLATE_BONE_IDS       (1<<10)
#define CUTSCENE_FLAGS_AUTO_SECTION             (1<<11)
#define CUTSCENE_FLAGS_INTERP_CAMERA            (1<<12)
#define CUTSCENE_FLAGS_SHORT_FADE_AT_END        (1<<13)

} // namespace rage

#endif // CUTFILE_CUTFDEFINES_H 
