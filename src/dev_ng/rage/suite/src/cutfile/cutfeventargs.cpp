// 
// cutfile/cutfeventargs.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutfeventargs.h"

#include "cutscene/cutsoptimisations.h"
#include "cutfevent.h"
#include "cutfile2.h"   // must have this before the parser
#include "cutfeventargs_parser.h"

#include "parser/treenode.h"

CUTS_OPTIMISATIONS()

namespace rage {

//##############################################################################

atMap<s32, cutfEventArgs::EventArgsTypeNameFtor> cutfEventArgs::s_eventArgsTypeNameFtorMap;

const char* cutfEventArgs::c_cutsceneEventArgsTypeNames[] = 
{
    "Generic",
    "Event Args List",
    "Name",
    "Float Value",
    "Two Float Values",
    "Screen Fade",
    "Load Scene",
    "Object Id",
    "Object Id Name",
    "Attachment",
    "Object Id List",
	"Play Particle Effect",
	"Subtitle",
	"Camera Cut",	
	"Actor Variation",
	"Vehicle Variation",
	"Vehicle Extra",
	"Decal",
	"Cascade Shadow",
	"Bool Value",
	"Final Name",
	"Float Bool Value"
};

cutfEventArgs::cutfEventArgs()
: m_iRefCount( 0 )
, m_cutfAttributes(NULL)
{

}

cutfEventArgs::cutfEventArgs( const parAttributeList& attributes )
: m_iRefCount( 0 )
, m_cutfAttributes(NULL)
{
    m_attributeList.CopyFrom( attributes );
}

cutfEventArgs::~cutfEventArgs()
{
    delete m_cutfAttributes;
}

void cutfEventArgs::InitClass()
{    
    s_eventArgsTypeNameFtorMap.Kill();
}

void cutfEventArgs::ShutdownClass()
{
    s_eventArgsTypeNameFtorMap.Kill();
}

void cutfEventArgs::SetEventArgsTypeNameFunc( EventArgsTypeNameFtor ftor, s32 iFirstEventArgsType )
{
    if ( ftor != 0 )
    {
        EventArgsTypeNameFtor *pFtor = s_eventArgsTypeNameFtorMap.Access( iFirstEventArgsType );
        if ( pFtor == NULL  )
        {
            s_eventArgsTypeNameFtorMap.Insert( iFirstEventArgsType, ftor );
        }
    }
    else
    {
        s_eventArgsTypeNameFtorMap.Delete( iFirstEventArgsType );
    }
}

cutfEventArgs* cutfEventArgs::Clone() const
{
    return rage_new cutfEventArgs( m_attributeList );
}

const char* cutfEventArgs::GetTypeName( s32 iEventArgsType )
{
    if ( (iEventArgsType >= 0) && (iEventArgsType < CUTSCENE_NUM_EVENT_ARGS_TYPES) )
    {
        return c_cutsceneEventArgsTypeNames[iEventArgsType];
    }

    EventArgsTypeNameFtor ftor = MakeFunctorRet( &DefaultEventArgsTypeNameFtor );
    atMap<s32, EventArgsTypeNameFtor>::Iterator entry = s_eventArgsTypeNameFtorMap.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        if ( iEventArgsType >= entry.GetKey() )
        {
            ftor = entry.GetData();
        }
    }

    return ftor( iEventArgsType );
}

void cutfEventArgs::MergeFrom( const cutfEventArgs &other, bool overwrite )
{
    cutfAssertf( GetType() == other.GetType(), "Can only merge event args of the same type." );

    m_attributeList.MergeFrom( other.GetAttributeList(), overwrite );
}

bool cutfEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( this == &other )
    {
        return false;
    }

    if ( GetType() != other.GetType() )
    {
        return true;
    }

    const atArray<parAttribute> &attList = m_attributeList.GetAttributeArray();
    if ( attList.GetCount() != other.GetAttributeList().GetAttributeArray().GetCount() )
    {
        return true;
    }

    for ( int i = 0; i < attList.GetCount(); ++i )
    {
        const parAttribute *pAtt = other.GetAttributeList().FindAttribute( attList[i].GetName() );
        if ( pAtt == NULL )
        {
            return true;
        }

        if ( attList[i].GetType() != pAtt->GetType() )
        {
            return true;
        }

        switch ( attList[i].GetType() )
        {
        case parAttribute::BOOL:
            {
                if ( attList[i].GetBoolValue() != pAtt->GetBoolValue() )
                {
                    return true;
                }
            }
            break;
        case parAttribute::DOUBLE:
            {
                if ( attList[i].GetDoubleValue() != pAtt->GetDoubleValue() )
                {
                    return true;
                }
            }
            break;
        case parAttribute::INT64:
            {
                if ( attList[i].GetInt64Value() != pAtt->GetInt64Value() )
                {
                    return true;
                }
            }
            break;
        case parAttribute::STRING:
            {
                if ( strcmp( attList[i].GetStringValue(), pAtt->GetStringValue() ) != 0 )
                {
                    return true;
                }
            }
            break;
        }
    }

    return false;
}

void cutfEventArgs::ConvertAttributesForSave()
{
	cutfCutsceneFile2::ConvertToCutfAttributeList(m_attributeList, m_cutfAttributes);
}

void cutfEventArgs::ConvertAttributesAfterLoad()
{
	cutfCutsceneFile2::ConvertToParAttributeList(m_cutfAttributes, m_attributeList);
}

//##############################################################################

cutfEventArgsList::cutfEventArgsList()
: cutfEventArgs()
{

}

cutfEventArgsList::cutfEventArgsList( atArray<cutfEventArgs *> &pEventArgsList )
: cutfEventArgs()
{
    m_pEventArgsPtrList.Assume( pEventArgsList );
    for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
    {
        m_pEventArgsPtrList[i]->AddRef();
    }
}

cutfEventArgsList::cutfEventArgsList( atArray<cutfEventArgs *> &pEventArgsList, const parAttributeList& attributes )
: cutfEventArgs( attributes )
{
    m_pEventArgsPtrList.Assume( pEventArgsList );
    for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
    {
        m_pEventArgsPtrList[i]->AddRef();
    }
}

cutfEventArgsList::~cutfEventArgsList()
{
    for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
    {
        m_pEventArgsPtrList[i]->Release();
    }

    m_pEventArgsPtrList.Reset();
}

cutfEventArgs* cutfEventArgsList::Clone() const
{
    atArray<cutfEventArgs *> pEventArgsList;
    pEventArgsList.Reserve( m_pEventArgsPtrList.GetCount() );
    for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
    {
        pEventArgsList.Append() = m_pEventArgsPtrList[i];
    }

    return rage_new cutfEventArgsList( pEventArgsList, m_attributeList );
}

void cutfEventArgsList::MergeFrom( const cutfEventArgs &other, bool overwrite )
{
    cutfEventArgs::MergeFrom( other, overwrite );
	
	const cutfEventArgsList *pOtherEventArgsList = dynamic_cast<const cutfEventArgsList *>( &other );
   
	if(pOtherEventArgsList)
	{
		if ( overwrite )
		{
			for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
			{
				m_pEventArgsPtrList[i]->Release();
			}

			m_pEventArgsPtrList.Reset();
			m_pEventArgsPtrList.Reserve( pOtherEventArgsList->m_pEventArgsPtrList.GetCount() );

			for ( int i = 0; i < pOtherEventArgsList->m_pEventArgsPtrList.GetCount(); ++i )
			{
				pOtherEventArgsList->m_pEventArgsPtrList[i]->AddRef();
				m_pEventArgsPtrList.Append() = pOtherEventArgsList->m_pEventArgsPtrList[i];
			}
		}
		else
		{
			for ( int i = 0; i < pOtherEventArgsList->m_pEventArgsPtrList.GetCount(); ++i )
			{
				int indexOf = m_pEventArgsPtrList.Find( pOtherEventArgsList->m_pEventArgsPtrList[i] );
				if ( indexOf == -1 )
				{
					pOtherEventArgsList->m_pEventArgsPtrList[i]->AddRef();
					m_pEventArgsPtrList.Grow() = pOtherEventArgsList->m_pEventArgsPtrList[i];
				}
			}
		}
	}
}

bool cutfEventArgsList::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfEventArgsList::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfEventArgsList *pOtherEventArgsList = static_cast<const cutfEventArgsList *>( &other );
    const atArray<cutfEventArgs *> &otherEventArgsList = pOtherEventArgsList->GetEventArgsList();
    if ( m_pEventArgsPtrList.GetCount() != otherEventArgsList.GetCount() )
    {
        return true;
    }

    for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
    {
        const cutfEventArgs *pEventArgs = m_pEventArgsPtrList[i];
        const cutfEventArgs *pOtherEventArgs = NULL;
        for ( int j = 0; j < otherEventArgsList.GetCount(); ++j )
        {
            if ( (otherEventArgsList[j] == pEventArgs) || (*(otherEventArgsList[j]) == *pEventArgs) )
            {
                pOtherEventArgs = otherEventArgsList[j];
                break;
            }
        }

        if ( pOtherEventArgs == NULL )
        {
            return true;
        }
    }

    return false;
}

void cutfEventArgsList::PreLoad(parTreeNode* node)
{
	parTreeNode* oldArgs = node->FindChildWithName("pEventArgsList");
	if (oldArgs)
	{
		oldArgs->GetElement().SetName("iEventArgsIndexList", false);
		for(parTreeNode::ChildNodeIterator kid = oldArgs->BeginChildren(); kid != oldArgs->EndChildren(); ++oldArgs)
		{
			parTreeNode* kidNode = (*kid);
			int oldIdx = kidNode->GetElement().FindAttributeIntValue("ref", -1, true);
			kidNode->GetElement().AddAttribute("value", oldIdx, false);
		}
	}
}

void cutfEventArgsList::ConvertArgIndicesToPointers()
{
	m_pEventArgsPtrList.ResizeGrow(m_iEventArgsIndexList.GetCount());
	for(int i = 0; i < m_iEventArgsIndexList.GetCount(); i++)
	{
		m_pEventArgsPtrList[i] = cutfCutsceneFile2::FindEventArgs(m_iEventArgsIndexList[i]);
	}
}

void cutfEventArgsList::ConvertArgPointersToIndices()
{
	m_iEventArgsIndexList.ResizeGrow(m_pEventArgsPtrList.GetCount());
	for(int i = 0; i < m_pEventArgsPtrList.GetCount(); i++)
	{
		m_iEventArgsIndexList[i] = cutfCutsceneFile2::GetEventArgsIndex(m_pEventArgsPtrList[i]);
	}
}

//##############################################################################

cutfNameEventArgs::cutfNameEventArgs()
: cutfEventArgs()
{
}

cutfNameEventArgs::cutfNameEventArgs( const char* pName )
: cutfEventArgs()
{
    SetName( pName );
}

cutfNameEventArgs::cutfNameEventArgs( atHashString Name)
: cutfEventArgs()
{
	SetName( Name );
}

cutfNameEventArgs::cutfNameEventArgs( const char* pName, const parAttributeList& attributes )
: cutfEventArgs( attributes )
{
    SetName( pName );
}

cutfNameEventArgs::cutfNameEventArgs( atHashString Name, const parAttributeList& attributes )
: cutfEventArgs( attributes )
{
	SetName( Name );
}

cutfEventArgs* cutfNameEventArgs::Clone() const
{
    return rage_new cutfNameEventArgs( m_cName, m_attributeList );
}

bool cutfNameEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfNameEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfNameEventArgs *pOtherEventArgs = static_cast<const cutfNameEventArgs *>( &other );

    if ( GetName().GetHash() != pOtherEventArgs->GetName().GetHash())
    {
        return true;
    }

    return false;
}
//##############################################################################
cutfFinalNameEventArgs::cutfFinalNameEventArgs()
: cutfEventArgs()
{
}

cutfFinalNameEventArgs::cutfFinalNameEventArgs( const char* pName )
: cutfEventArgs()
{
	SetName( pName );
}

cutfFinalNameEventArgs::cutfFinalNameEventArgs( atString Name )
: cutfEventArgs()
{
	SetName( Name.c_str() );
}

cutfFinalNameEventArgs::cutfFinalNameEventArgs( atString Name, const parAttributeList& attributes )
: cutfEventArgs( attributes )
{
	SetName( Name.c_str() );
}

cutfFinalNameEventArgs::cutfFinalNameEventArgs( const char* pName, const parAttributeList& attributes )
: cutfEventArgs( attributes )
{
	SetName( pName );
}

cutfEventArgs* cutfFinalNameEventArgs::Clone() const
{
	return rage_new cutfFinalNameEventArgs( m_cName, m_attributeList );
}

bool cutfFinalNameEventArgs::operator == ( const cutfEventArgs &other ) const
{
	return (*this != other) == false;
}

bool cutfFinalNameEventArgs::operator != ( const cutfEventArgs &other ) const
{
	if ( cutfEventArgs::operator != ( other ) )
	{
		return true;
	}

	const cutfFinalNameEventArgs *pOtherEventArgs = static_cast<const cutfFinalNameEventArgs *>( &other );

	if ( strcmp( GetName(), pOtherEventArgs->GetName() ) != 0 )
	{
		return true;
	}

	return false;
}

//##############################################################################

cutfBoolValueEventArgs::cutfBoolValueEventArgs()
: cutfEventArgs()
, m_bValue( false)
{

}

cutfBoolValueEventArgs::cutfBoolValueEventArgs( bool bValue )
: cutfEventArgs()
, m_bValue( bValue )
{

}

cutfBoolValueEventArgs::cutfBoolValueEventArgs( bool bValue, const parAttributeList& attributes )
: cutfEventArgs( attributes )
, m_bValue( bValue )
{

}

cutfEventArgs* cutfBoolValueEventArgs::Clone() const
{
	return rage_new cutfBoolValueEventArgs( m_bValue, m_attributeList );
}

bool cutfBoolValueEventArgs::operator == ( const cutfEventArgs &other ) const
{
	return (*this != other) == false;
}

bool cutfBoolValueEventArgs::operator != ( const cutfEventArgs &other ) const
{
	if ( cutfEventArgs::operator != ( other ) )
	{
		return true;
	}

	const cutfBoolValueEventArgs *pOtherEventArgs = static_cast<const cutfBoolValueEventArgs *>( &other );
	if ( GetValue() != pOtherEventArgs->GetValue() )
	{
		return true;
	}

	return false;
}

//##############################################################################

cutfFloatBoolValueEventArgs::cutfFloatBoolValueEventArgs()
: cutfBoolValueEventArgs()
, m_fValue( 0.0f )
{

}

cutfFloatBoolValueEventArgs::cutfFloatBoolValueEventArgs( float fValue, bool bValue)
: cutfBoolValueEventArgs(bValue)
, m_fValue( fValue )
{

}

cutfFloatBoolValueEventArgs::cutfFloatBoolValueEventArgs( float fValue, bool bValue, const parAttributeList& attributes )
: cutfBoolValueEventArgs( bValue, attributes )
, m_fValue( fValue )
{

}

cutfEventArgs* cutfFloatBoolValueEventArgs::Clone() const
{
	return rage_new cutfFloatBoolValueEventArgs( m_fValue, m_bValue , m_attributeList );
}

bool cutfFloatBoolValueEventArgs::operator == ( const cutfEventArgs &other ) const
{
	return (*this != other) == false;
}

bool cutfFloatBoolValueEventArgs::operator != ( const cutfEventArgs &other ) const
{
	if ( cutfBoolValueEventArgs::operator != ( other ) )
	{
		return true;
	}


	const cutfFloatBoolValueEventArgs *pOtherEventArgs = static_cast<const cutfFloatBoolValueEventArgs *>( &other );
	if ( GetFloat1() != pOtherEventArgs->GetFloat1() )
	{
		return true;
	}

	return false;
}

//##############################################################################

cutfFloatValueEventArgs::cutfFloatValueEventArgs()
: cutfEventArgs()
, m_fValue( 0.0f )
{

}

cutfFloatValueEventArgs::cutfFloatValueEventArgs( float fValue )
: cutfEventArgs()
, m_fValue( fValue )
{

}

cutfFloatValueEventArgs::cutfFloatValueEventArgs( float fValue, const parAttributeList& attributes )
: cutfEventArgs( attributes )
, m_fValue( fValue )
{

}

cutfEventArgs* cutfFloatValueEventArgs::Clone() const
{
    return rage_new cutfFloatValueEventArgs( m_fValue, m_attributeList );
}

bool cutfFloatValueEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfFloatValueEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfFloatValueEventArgs *pOtherEventArgs = static_cast<const cutfFloatValueEventArgs *>( &other );
    if ( GetFloat1() != pOtherEventArgs->GetFloat1() )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfTwoFloatValuesEventArgs::cutfTwoFloatValuesEventArgs()
: cutfFloatValueEventArgs()
, m_fValue2( 0.0f )
{

}


cutfTwoFloatValuesEventArgs::cutfTwoFloatValuesEventArgs( float fValue1, float fValue2 )
: cutfFloatValueEventArgs( fValue1 )
, m_fValue2( fValue2 )
{

}

cutfTwoFloatValuesEventArgs::cutfTwoFloatValuesEventArgs( float fValue1, float fValue2, const parAttributeList& attributes )
: cutfFloatValueEventArgs( fValue1, attributes )
, m_fValue2( fValue2 )
{

}

cutfEventArgs* cutfTwoFloatValuesEventArgs::Clone() const
{
    return rage_new cutfTwoFloatValuesEventArgs( m_fValue,  m_fValue2, m_attributeList );
}

bool cutfTwoFloatValuesEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfTwoFloatValuesEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfFloatValueEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfTwoFloatValuesEventArgs *pOtherEventArgs = static_cast<const cutfTwoFloatValuesEventArgs *>( &other );
    if ( GetFloat2() != pOtherEventArgs->GetFloat2() )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfScreenFadeEventArgs::cutfScreenFadeEventArgs()
: cutfFloatValueEventArgs()
{
    // default to black
    m_color.Setf( 0, 0, 0, 1.0f );
}

cutfScreenFadeEventArgs::cutfScreenFadeEventArgs( float fDuration, const Color32 &color )
: cutfFloatValueEventArgs( fDuration )
, m_color( color )
{

}

cutfScreenFadeEventArgs::cutfScreenFadeEventArgs( float fDuration, const Color32 &color, const parAttributeList& attributes )
: cutfFloatValueEventArgs( fDuration, attributes )
, m_color( color )
{

}

cutfEventArgs* cutfScreenFadeEventArgs::Clone() const
{
    return rage_new cutfScreenFadeEventArgs( m_fValue, m_color, m_attributeList );
}

bool cutfScreenFadeEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfScreenFadeEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfFloatValueEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfScreenFadeEventArgs *pOtherEventArgs = static_cast<const cutfScreenFadeEventArgs *>( &other );
    if ( GetColor() != pOtherEventArgs->GetColor() )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfLoadSceneEventArgs::cutfLoadSceneEventArgs()
: cutfNameEventArgs()
, m_vOffset(VEC3_ZERO)
, m_fRotation( 0.0f )
, m_fPitch( 0.0f )
, m_fRoll( 0.0f )
{
}

cutfLoadSceneEventArgs::cutfLoadSceneEventArgs( const char* pCutsceneName, const Vector3& vOffset, float fRotation, float fPitch, float fRoll)
                                             
: cutfNameEventArgs( pCutsceneName )
, m_vOffset( vOffset )
, m_fRotation( fRotation )
, m_fPitch( fPitch ) 
, m_fRoll( fRoll )
{
}

cutfLoadSceneEventArgs::cutfLoadSceneEventArgs( const char* pCutsceneName, const Vector3& vOffset, float fRotation,  float fPitch, float fRoll,
                                               const parAttributeList& attributes )
                                               : cutfNameEventArgs( pCutsceneName, attributes )
                                               , m_vOffset( vOffset )
                                               , m_fRotation( fRotation )
											   , m_fPitch( fPitch ) 
											   , m_fRoll( fRoll )
{
}

cutfLoadSceneEventArgs::cutfLoadSceneEventArgs( atHashString pCutsceneName, const Vector3& vOffset, float fRotation,  float fPitch, float fRoll,
											    const parAttributeList& attributes )
											   : cutfNameEventArgs( pCutsceneName, attributes )
											   , m_vOffset( vOffset )
											   , m_fRotation( fRotation )	
											   , m_fPitch( fPitch ) 
											   , m_fRoll( fRoll )
{

}


cutfEventArgs* cutfLoadSceneEventArgs::Clone() const
{
    return rage_new cutfLoadSceneEventArgs( m_cName, m_vOffset, m_fRotation, m_fPitch, m_fRoll, m_attributeList );
}

bool cutfLoadSceneEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfLoadSceneEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfNameEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfLoadSceneEventArgs *pOtherEventArgs = static_cast<const cutfLoadSceneEventArgs *>( &other );
    if (GetOffset() != pOtherEventArgs->GetOffset()) 
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfObjectIdEventArgs::cutfObjectIdEventArgs()
: cutfEventArgs()
, m_iObjectId( -1 )
{

}

cutfObjectIdEventArgs::cutfObjectIdEventArgs( s32 iObjectId )
: cutfEventArgs()
, m_iObjectId( iObjectId )
{

}

cutfObjectIdEventArgs::cutfObjectIdEventArgs( s32 iObjectId, const parAttributeList& attributes )
: cutfEventArgs( attributes )
, m_iObjectId( iObjectId )
{

}

cutfEventArgs* cutfObjectIdEventArgs::Clone() const
{
    return rage_new cutfObjectIdEventArgs( m_iObjectId, m_attributeList );
}

bool cutfObjectIdEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfObjectIdEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfObjectIdEventArgs *pOtherEventArgs = static_cast<const cutfObjectIdEventArgs *>( &other );
    if ( GetObjectId() != pOtherEventArgs->GetObjectId() )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfObjectIdNameEventArgs::cutfObjectIdNameEventArgs()
: cutfObjectIdEventArgs()
{
}

cutfObjectIdNameEventArgs::cutfObjectIdNameEventArgs( s32 iObjectId, const char *pName )
: cutfObjectIdEventArgs( iObjectId )
{
    SetName( pName );
}

cutfObjectIdNameEventArgs::cutfObjectIdNameEventArgs( s32 iObjectId, const char *pName, const parAttributeList& attributes )
: cutfObjectIdEventArgs( iObjectId, attributes )
{
    SetName( pName );
}

cutfObjectIdNameEventArgs::cutfObjectIdNameEventArgs( s32 iObjectId, atHashString Name, const parAttributeList& attributes )
: cutfObjectIdEventArgs( iObjectId, attributes )
{
	SetName( Name );
}

cutfEventArgs* cutfObjectIdNameEventArgs::Clone() const
{
    return rage_new cutfObjectIdNameEventArgs( m_iObjectId, m_cName, m_attributeList );
}

bool cutfObjectIdNameEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfObjectIdNameEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfObjectIdEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfObjectIdNameEventArgs *pOtherEventArgs = static_cast<const cutfObjectIdNameEventArgs *>( &other );
    if ( GetName().GetHash() != pOtherEventArgs->GetName().GetHash() )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfObjectIdPartialHashEventArgs::cutfObjectIdPartialHashEventArgs()
: cutfObjectIdEventArgs()
, m_PartialHash(0)
{
	m_PartialHash = 0; 
}

cutfObjectIdPartialHashEventArgs::cutfObjectIdPartialHashEventArgs( s32 iObjectId, u32 partialHash)
: cutfObjectIdEventArgs( iObjectId )
, m_PartialHash(partialHash)
{

}

cutfObjectIdPartialHashEventArgs::cutfObjectIdPartialHashEventArgs( s32 iObjectId, u32 partialHash, const parAttributeList& attributes )
: cutfObjectIdEventArgs( iObjectId, attributes )
, m_PartialHash(partialHash)
{
}

cutfEventArgs* cutfObjectIdPartialHashEventArgs::Clone() const
{
	return rage_new cutfObjectIdPartialHashEventArgs( m_iObjectId, m_PartialHash, m_attributeList );
}

bool cutfObjectIdPartialHashEventArgs::operator == ( const cutfEventArgs &other ) const
{
	return (*this != other) == false;
}

bool cutfObjectIdPartialHashEventArgs::operator != ( const cutfEventArgs &other ) const
{
	if ( cutfObjectIdEventArgs::operator != ( other ) )
	{
		return true;
	}

	const cutfObjectIdPartialHashEventArgs *pOtherEventArgs = static_cast<const cutfObjectIdPartialHashEventArgs *>( &other );
	if ( GetPartialHash() != pOtherEventArgs->GetPartialHash() )
	{
		return true;
	}

	return false;
}

//##############################################################################

cutfAttachmentEventArgs::cutfAttachmentEventArgs()
: cutfObjectIdEventArgs()
{
}

cutfAttachmentEventArgs::cutfAttachmentEventArgs( s32 iObjectId, const char* pBoneName )
: cutfObjectIdEventArgs( iObjectId )
{
	SetBoneName( pBoneName );
}

cutfAttachmentEventArgs::cutfAttachmentEventArgs( s32 iObjectId, const char* pBoneName, const parAttributeList& attributes )
: cutfObjectIdEventArgs( iObjectId, attributes )
{
	SetBoneName( pBoneName );
}

cutfAttachmentEventArgs::cutfAttachmentEventArgs( s32 iObjectId, atHashString BoneName, const parAttributeList& attributes )
: cutfObjectIdEventArgs( iObjectId, attributes )
{
	SetBoneName( BoneName );
}

cutfEventArgs* cutfAttachmentEventArgs::Clone() const
{
    cutfAttachmentEventArgs *pAttachmentEventArgs = rage_new cutfAttachmentEventArgs( m_iObjectId, m_cBoneName, m_attributeList );


    return pAttachmentEventArgs;
}

bool cutfAttachmentEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfAttachmentEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfObjectIdEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfAttachmentEventArgs *pOtherEventArgs = static_cast<const cutfAttachmentEventArgs *>( &other );
    if ( GetBoneName().GetHash() != pOtherEventArgs->GetBoneName().GetHash())
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfObjectIdListEventArgs::cutfObjectIdListEventArgs()
: cutfEventArgs()
{

}

cutfObjectIdListEventArgs::cutfObjectIdListEventArgs( atArray<s32> &iObjectIdList )
: cutfEventArgs()
{
    m_iObjectIdList.Assume( iObjectIdList );
}

cutfObjectIdListEventArgs::cutfObjectIdListEventArgs( atArray<s32> &iObjectIdList, const parAttributeList& attributes )
: cutfEventArgs( attributes )
{
    m_iObjectIdList.Assume( iObjectIdList );
}

cutfObjectIdListEventArgs::~cutfObjectIdListEventArgs()
{
    
}

cutfEventArgs* cutfObjectIdListEventArgs::Clone() const
{
    atArray<s32> iObjectIdList;
    iObjectIdList.Reserve( m_iObjectIdList.GetCount() );
    for ( int i = 0; i < m_iObjectIdList.GetCount(); ++i )
    {
        iObjectIdList.Append() = m_iObjectIdList[i];
    }

    return rage_new cutfObjectIdListEventArgs( iObjectIdList, m_attributeList );
}

void cutfObjectIdListEventArgs::MergeFrom( const cutfEventArgs &other, bool overwrite )
{
    cutfEventArgs::MergeFrom( other, overwrite );

    const cutfObjectIdListEventArgs *pOtherObjectIdListEventArgs = dynamic_cast<const cutfObjectIdListEventArgs *>( &other );
    if ( overwrite )
    {
        m_iObjectIdList.Reset();
        m_iObjectIdList.Reserve( pOtherObjectIdListEventArgs->m_iObjectIdList.GetCount() );
        for ( int i = 0; i < pOtherObjectIdListEventArgs->m_iObjectIdList.GetCount(); ++i )
        {
            m_iObjectIdList.Append() = pOtherObjectIdListEventArgs->m_iObjectIdList[i];
        }
    }
    else
    {
        for ( int i = 0; i < pOtherObjectIdListEventArgs->m_iObjectIdList.GetCount(); ++i )
        {
            int indexOf = m_iObjectIdList.Find( pOtherObjectIdListEventArgs->m_iObjectIdList[i] );
            if ( indexOf == -1 )
            {
                m_iObjectIdList.Grow() = pOtherObjectIdListEventArgs->m_iObjectIdList[i];
            }
        }
    }
}

bool cutfObjectIdListEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfObjectIdListEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfObjectIdListEventArgs *pOtherEventArgs = static_cast<const cutfObjectIdListEventArgs *>( &other );

    const atArray<s32> &otherObjectIdList = pOtherEventArgs->GetObjectIdList();
    if ( m_iObjectIdList.GetCount() != otherObjectIdList.GetCount() )
    {
        return true;
    }

    for ( int i = 0; i < m_iObjectIdList.GetCount(); ++i )
    {
        int iObjectId = GetObjectIdList()[i];
        int iOtherObjectId = -1;
        for ( int j = 0; j < otherObjectIdList.GetCount(); ++j )
        {
            if ( otherObjectIdList[j] == iObjectId )
            {
                iOtherObjectId = otherObjectIdList[j];
                break;
            }
        }

        if ( iOtherObjectId == -1 )
        {
            return true;
        }
    }

    return false;
}

//##############################################################################

cutfPlayParticleEffectEventArgs::cutfPlayParticleEffectEventArgs()
: cutfEventArgs()
, m_iAttachParentId(-1)
, m_iAttachBoneHash(0)
{
}

cutfPlayParticleEffectEventArgs::cutfPlayParticleEffectEventArgs(s32 AttachParentId, u16 BoneHash, const Vector3& vOffset, const Vector4& vRotation )
                                 : cutfEventArgs()
                                 , m_iAttachParentId(AttachParentId)
                                 , m_iAttachBoneHash(BoneHash)
								 , m_vInitialBoneOffset(vOffset)
								 , m_vInitialBoneRotation(vRotation)
{

}

cutfPlayParticleEffectEventArgs::cutfPlayParticleEffectEventArgs(s32 AttachParentId, u16 BoneHash , const Vector3& vOffset, const Vector4& vRotation, const parAttributeList& attributes)
                                 : cutfEventArgs( attributes )
								 , m_iAttachParentId(AttachParentId)
								 , m_iAttachBoneHash(BoneHash)
								 , m_vInitialBoneOffset(vOffset)
								 , m_vInitialBoneRotation(vRotation)
{

}

cutfPlayParticleEffectEventArgs::~cutfPlayParticleEffectEventArgs()
{

}

cutfEventArgs* cutfPlayParticleEffectEventArgs::Clone() const
{

    return rage_new cutfPlayParticleEffectEventArgs( m_iAttachParentId, m_iAttachBoneHash,m_vInitialBoneOffset, m_vInitialBoneRotation,m_attributeList );
}

bool cutfPlayParticleEffectEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfPlayParticleEffectEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfPlayParticleEffectEventArgs *pOtherEventArgs = static_cast<const cutfPlayParticleEffectEventArgs *>( &other );
    if ( (GetAttachParentId() != pOtherEventArgs->GetAttachParentId()) 
        || (GetAttachBoneHash() != pOtherEventArgs->GetAttachBoneHash()) )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfTriggerLightEffectEventArgs::cutfTriggerLightEffectEventArgs()
: cutfEventArgs()
, m_iAttachParentId(-1)
, m_iAttachBoneHash(0)
, m_AttachedParentName("")
{
}

cutfTriggerLightEffectEventArgs::cutfTriggerLightEffectEventArgs(s32 AttachParentId, u16 BoneHash, atHashString AttachParentName )
: cutfEventArgs()
, m_iAttachParentId(AttachParentId)
, m_iAttachBoneHash(BoneHash)
, m_AttachedParentName(AttachParentName)
{

}

cutfTriggerLightEffectEventArgs::cutfTriggerLightEffectEventArgs(s32 AttachParentId, u16 BoneHash, atHashString AttachParentName, const parAttributeList& attributes)
: cutfEventArgs( attributes )
, m_iAttachParentId(AttachParentId)
, m_iAttachBoneHash(BoneHash)
, m_AttachedParentName(AttachParentName)
{
}

cutfTriggerLightEffectEventArgs::~cutfTriggerLightEffectEventArgs()
{
}

cutfEventArgs* cutfTriggerLightEffectEventArgs::Clone() const
{
	return rage_new cutfTriggerLightEffectEventArgs( m_iAttachParentId, m_iAttachBoneHash, m_AttachedParentName, m_attributeList );
}

bool cutfTriggerLightEffectEventArgs::operator == ( const cutfEventArgs &other ) const
{
	return (*this != other) == false;
}

bool cutfTriggerLightEffectEventArgs::operator != ( const cutfEventArgs &other ) const
{
	if ( cutfEventArgs::operator != ( other ) )
	{
		return true;
	}

	const cutfTriggerLightEffectEventArgs *pOtherEventArgs = static_cast<const cutfTriggerLightEffectEventArgs *>( &other );
	if ( (GetAttachParentId() != pOtherEventArgs->GetAttachParentId()) 
		|| (GetAttachBoneHash() != pOtherEventArgs->GetAttachBoneHash())
		|| (GetAttachParentHashString().GetHash() != pOtherEventArgs->GetAttachParentHashString().GetHash()))
	{
		return true;
	}

	return false;
}

//##############################################################################

cutfObjectVariationEventArgs::cutfObjectVariationEventArgs()
: cutfObjectIdEventArgs()
, m_iComponent ( 0 )
, m_iDrawable ( 0 )  
, m_iTexture ( 0 )  
{
}

cutfObjectVariationEventArgs::cutfObjectVariationEventArgs(s32 iObjectid, int iComponent, int iDrawable, int iTexture )
: cutfObjectIdEventArgs( iObjectid )
, m_iComponent ( iComponent )
, m_iDrawable ( iDrawable )  
, m_iTexture ( iTexture )  
{
}
cutfObjectVariationEventArgs::cutfObjectVariationEventArgs(s32 iObjectid, int iComponent, int iDrawable, int iTexture, const parAttributeList& attributes )
: cutfObjectIdEventArgs( iObjectid, attributes )
, m_iComponent ( iComponent )
, m_iDrawable ( iDrawable )  
, m_iTexture ( iTexture )  
{
}

cutfObjectVariationEventArgs::~cutfObjectVariationEventArgs()
{

}

cutfEventArgs* cutfObjectVariationEventArgs::Clone() const
{
	return rage_new cutfObjectVariationEventArgs(m_iObjectId, m_iComponent, m_iDrawable, m_iTexture);
}

bool cutfObjectVariationEventArgs::operator ==(const cutfEventArgs &other) const
{
	return (*this != other) == false; 
}

bool cutfObjectVariationEventArgs::operator != (const cutfEventArgs &other) const
{
	if ( cutfEventArgs::operator != ( other ) )
	{	
		return true;
	}

	const cutfObjectVariationEventArgs *pOtherEventArgs = static_cast<const cutfObjectVariationEventArgs *>( &other );

	if ((GetComponent() != pOtherEventArgs->GetComponent())
		|| (GetDrawable() != pOtherEventArgs->GetDrawable()) 
		|| (GetTexture() != pOtherEventArgs->GetTexture()) )
	{
		return true;
	}

	return false; 
}


//##############################################################################

cutfVehicleVariationEventArgs::cutfVehicleVariationEventArgs()
: cutfObjectIdEventArgs()
,m_iMainBodyColour ( 0 )
,m_iSecondBodyColour ( 0 )
,m_iSpecularColour  ( 0 )
,m_iWheelTrimColour ( 0 )
,m_iBodyColour5 ( 0 )
,m_iBodyColour6 ( 0 )
,m_iLivery ( -1 )
,m_iLivery2 ( -1 )
,m_fDirtLevel ( 0.0f )
{
}

cutfVehicleVariationEventArgs::cutfVehicleVariationEventArgs( s32 iObjectid,  int iBodyColour , int iSecondBodyColour , int iSpecColour, int iWheelTrimColour, int iBodyColour5, int iBodyColour6, int Liveryid, int Livery2id, float fDirtLevel )
: cutfObjectIdEventArgs(iObjectid)
,m_iMainBodyColour ( iBodyColour )
,m_iSecondBodyColour ( iSecondBodyColour )
,m_iSpecularColour  ( iSpecColour )
,m_iWheelTrimColour ( iWheelTrimColour )
,m_iBodyColour5 ( iBodyColour5 )
,m_iBodyColour6 ( iBodyColour6 )
,m_iLivery ( Liveryid )
,m_iLivery2 ( Livery2id )
,m_fDirtLevel ( fDirtLevel )
{
}

cutfVehicleVariationEventArgs::cutfVehicleVariationEventArgs( s32 iObjectid,  int iBodyColour , int iSecondBodyColour , int iSpecColour, int iWheelTrimColour, int iBodyColour5, int iBodyColour6, int Liveryid, int Livery2id, float fDirtLevel, const parAttributeList& attributes  )
: cutfObjectIdEventArgs(iObjectid, attributes)
,m_iMainBodyColour ( iBodyColour )
,m_iSecondBodyColour ( iSecondBodyColour )
,m_iSpecularColour  ( iSpecColour )
,m_iWheelTrimColour ( iWheelTrimColour )
,m_iBodyColour5 (iBodyColour5)
,m_iBodyColour6 (iBodyColour6)
,m_iLivery ( Liveryid )
,m_iLivery2 ( Livery2id )
,m_fDirtLevel ( fDirtLevel )
{
}

cutfVehicleVariationEventArgs::~cutfVehicleVariationEventArgs()
{

}

cutfEventArgs* cutfVehicleVariationEventArgs::Clone() const
{
	return rage_new cutfVehicleVariationEventArgs(m_iObjectId, m_iMainBodyColour, m_iSecondBodyColour, m_iSpecularColour, m_iWheelTrimColour, m_iBodyColour5, m_iBodyColour6, m_iLivery, m_iLivery2, m_fDirtLevel, m_attributeList);
}

bool cutfVehicleVariationEventArgs::operator ==(const cutfEventArgs &other) const
{
	return (*this != other) == false; 
}

bool cutfVehicleVariationEventArgs::operator != (const cutfEventArgs &other) const
{
	if ( cutfObjectIdEventArgs::operator != ( other ) )
	{	
		return true;
	}

	const cutfVehicleVariationEventArgs *pOtherEventArgs = static_cast<const cutfVehicleVariationEventArgs *>( &other );

	if ((GetBodyColour() != pOtherEventArgs->GetBodyColour())
		|| (GetSecondaryBodyColour() != pOtherEventArgs->GetSecondaryBodyColour()) 
		|| (GetSpecularBodyColour() != pOtherEventArgs->GetSpecularBodyColour())
		|| (GetWheelTrimColour() != pOtherEventArgs->GetWheelTrimColour())
		|| (GetBodyColour5() != pOtherEventArgs->GetBodyColour5())
		|| (GetBodyColour6() != pOtherEventArgs->GetBodyColour6())
		|| (GetLiveryId() != pOtherEventArgs->GetLiveryId())
		|| (GetLivery2Id() != pOtherEventArgs->GetLivery2Id())
		|| (GetDirtLevel() != pOtherEventArgs->GetDirtLevel()))
	{
		return true;
	}

	return false; 
}

//##############################################################################

cutfVehicleExtraEventArgs::cutfVehicleExtraEventArgs()
: cutfObjectIdEventArgs()
{
}

cutfVehicleExtraEventArgs::cutfVehicleExtraEventArgs( s32 iObjectid,  atArray<s32> &Extras)
: cutfObjectIdEventArgs(iObjectid)
{
	m_pExtraBoneIds.Assume(Extras);
}

cutfVehicleExtraEventArgs::cutfVehicleExtraEventArgs( s32 iObjectid,   atArray<s32> &Extras, const parAttributeList& attributes )
: cutfObjectIdEventArgs(iObjectid, attributes)
{
	m_pExtraBoneIds.Assume(Extras);
}

cutfVehicleExtraEventArgs::cutfVehicleExtraEventArgs( s32 iObjectid,   const atArray<s32> &Extras, const parAttributeList& attributes )
: cutfObjectIdEventArgs(iObjectid, attributes)
{
	m_pExtraBoneIds = Extras;
}


cutfEventArgs* cutfVehicleExtraEventArgs::Clone() const
{
	return rage_new cutfVehicleExtraEventArgs( m_iObjectId, m_pExtraBoneIds, m_attributeList );
}

cutfVehicleExtraEventArgs::~cutfVehicleExtraEventArgs()
{
}

bool cutfVehicleExtraEventArgs::operator ==(const cutfEventArgs &other) const
{
	return (*this != other) == false; 
}

bool cutfVehicleExtraEventArgs::operator != (const cutfEventArgs &other) const
{
	if ( cutfObjectIdEventArgs::operator != ( other ) )
	{	
		return true;
	}

	const cutfVehicleExtraEventArgs *pOtherBoneList = static_cast<const cutfVehicleExtraEventArgs *>( &other );

	if (pOtherBoneList == NULL)
	{
		return true; 
	}

	atArray<s32> otherBonesList; 
	otherBonesList = pOtherBoneList->GetBoneIdList();
	
	if ( m_pExtraBoneIds.GetCount() != otherBonesList.GetCount() )
	{
		return true;
	}

	for ( int i = 0; i < m_pExtraBoneIds.GetCount(); ++i )
	{
		s32 iBoneId = m_pExtraBoneIds[i];
		int iNumofItems = 0; 		

		for ( int j = 0; j < otherBonesList.GetCount(); ++j )
		{
			if ( otherBonesList[j] == iBoneId )
			{
				iNumofItems++;
			}
		}
		
		if(iNumofItems != 1)
		{
			return true;
		}
	}
	return false; 
}


//##############################################################################

cutfSubtitleEventArgs::cutfSubtitleEventArgs()
: cutfNameEventArgs()
, m_iLanguageID( -1 )
, m_iTransitionIn( -1 )
, m_fTransitionInDuration( 0.0f )
, m_iTransitionOut( -1 )
, m_fTransitionOutDuration( 0.0f )
, m_fSubtitleDuration( 0.0f )
{
}

cutfSubtitleEventArgs::cutfSubtitleEventArgs( const char* pIdentifier, int iLanguageID, 
                                             int iTransitionIn, float fTransitionInDuration, int iTransitionOut, float fTransitionOutDuration, float fSubtitleDuration )
                                             : cutfNameEventArgs( pIdentifier )
                                             , m_iLanguageID( iLanguageID )
                                             , m_iTransitionIn( iTransitionIn )
                                             , m_fTransitionInDuration( fTransitionInDuration )
                                             , m_iTransitionOut( iTransitionOut )
                                             , m_fTransitionOutDuration( fTransitionOutDuration )
											 , m_fSubtitleDuration ( fSubtitleDuration )
{
}

cutfSubtitleEventArgs::cutfSubtitleEventArgs( const char* pIdentifier, int iLanguageID, 
                                             int iTransitionIn, float fTransitionInDuration, int iTransitionOut, float fTransitionOutDuration, float fSubtitleDuration,
                                             const parAttributeList& attributes )
                                             : cutfNameEventArgs( pIdentifier, attributes )
                                             , m_iLanguageID( iLanguageID )
                                             , m_iTransitionIn( iTransitionIn )
                                             , m_fTransitionInDuration( fTransitionInDuration )
                                             , m_iTransitionOut( iTransitionOut )
                                             , m_fTransitionOutDuration( fTransitionOutDuration )
											 , m_fSubtitleDuration ( fSubtitleDuration )
{
}

cutfSubtitleEventArgs::cutfSubtitleEventArgs( atHashString pIdentifier, int iLanguageID, 
											 int iTransitionIn, float fTransitionInDuration, int iTransitionOut, float fTransitionOutDuration, float fSubtitleDuration,
											 const parAttributeList& attributes )
											 : cutfNameEventArgs( pIdentifier, attributes )
											 , m_iLanguageID( iLanguageID )
											 , m_iTransitionIn( iTransitionIn )
											 , m_fTransitionInDuration( fTransitionInDuration )
											 , m_iTransitionOut( iTransitionOut )
											 , m_fTransitionOutDuration( fTransitionOutDuration )
											 , m_fSubtitleDuration ( fSubtitleDuration )
{
}

cutfEventArgs* cutfSubtitleEventArgs::Clone() const
{
    return rage_new cutfSubtitleEventArgs( m_cName, m_iLanguageID, 
        m_iTransitionIn, m_fTransitionInDuration, m_iTransitionOut, m_fTransitionOutDuration, m_fSubtitleDuration,m_attributeList );
}

bool cutfSubtitleEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfSubtitleEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfNameEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfSubtitleEventArgs *pOtherEventArgs = static_cast<const cutfSubtitleEventArgs *>( &other );
    if ( (GetLanguageID() != pOtherEventArgs->GetLanguageID()) 
        || (GetTransitionIn() != pOtherEventArgs->GetTransitionIn()) 
        || (GetTransitionInDuration() != pOtherEventArgs->GetTransitionInDuration())
        || (GetTransitionOut() != pOtherEventArgs->GetTransitionOut()) 
        || (GetTransitionOutDuration() != pOtherEventArgs->GetTransitionOutDuration()) )
    {
        return true;
    }

    return false;
}

cutfCameraCutTimeOfDayDofModifier::cutfCameraCutTimeOfDayDofModifier()
:m_TimeOfDayFlags(0)
, m_DofStrengthModifier(0)
{

}

//##############################################################################

cutfCameraCutEventArgs::cutfCameraCutEventArgs()
: cutfNameEventArgs()
, m_fNearDrawDistance(-1.0f)
, m_fFarDrawDistance(-1.0f)
, m_fMapLodScale(-1.0f)
, m_ReflectionLodRangeStart(-1.0f)
, m_ReflectionLodRangeEnd(-1.0f)
, m_ReflectionSLodRangeStart(-1.0f)
, m_ReflectionSLodRangeEnd(-1.0f)
, m_LodMultHD(-1.0f)
, m_LodMultOrphanedHD(-1.0f)
, m_LodMultLod(-1.0f)
, m_LodMultSLod1(-1.0f)
, m_LodMultSLod2(-1.0f)
, m_LodMultSLod3(-1.0f)
, m_LodMultSLod4(-1.0f)
, m_WaterReflectionFarClip(-1.0f)
, m_SSAOLightInten(-1.0f)
, m_ExposurePush(0.0f)
, m_LightFadeDistanceMult(1.0f)
, m_LightShadowFadeDistanceMult(1.0f) 
, m_LightSpecularFadeDistMult(1.0f)
, m_LightVolumetricFadeDistanceMult(1.0f) 
, m_DirectionalLightMultiplier(-1.0f) 
, m_LensArtefactMultiplier(-1.0f) 
, m_BloomMax(-1.0f)
, m_DisableHighQualityDof(false)
, m_FreezeReflectionMap(false)
, m_DisableDirectionalLighting(false)
, m_AbsoluteIntensityEnabled(true)
{
    m_vPosition.Zero();
    m_vRotationQuaternion.Zero();
}

cutfCameraCutEventArgs::cutfCameraCutEventArgs( const char* pName, const Vector3 &vPosition, const Vector4 &vRotationQuaternion,
                                               float fNearDrawDistance, float fFarDrawDistance, float fMapLodScale, float ReflectionLodRangeStart,
											   float ReflectionLodRangeEnd, float ReflectionSLodRangeStart, float ReflectionSLodRangeEnd, 
											   float LodMultHD, float LodMultOrphanedHD, float LodMultLod, float LodMultSLod1, float LodMultSLod2,
											   float LodMultSLod3, float LodMultSLod4, float WaterReflectionFarClip, float SSAOLightInten, bool bDisableHighQualityDof,
											   bool FreezeReflectionMap, bool DisableDirectionalLighting, bool AbsoluteIntensityEnabled, 
											   float LightFadeDistanceMult, float LightShadowDistanceMult, float LightSpecularFadeDistMult, 
											   float LightVolumetricFadeDistanceMult, float DirectionalLightMultiplier, float LensArtefactMultiplier, float BloomMax)
: cutfNameEventArgs(pName)
, m_vPosition(vPosition)
, m_vRotationQuaternion(vRotationQuaternion)
, m_fNearDrawDistance(fNearDrawDistance)
, m_fFarDrawDistance(fFarDrawDistance)
, m_fMapLodScale(fMapLodScale)
, m_ReflectionLodRangeStart(ReflectionLodRangeStart)
, m_ReflectionLodRangeEnd(ReflectionLodRangeEnd)
, m_ReflectionSLodRangeStart(ReflectionSLodRangeStart)
, m_ReflectionSLodRangeEnd(ReflectionSLodRangeEnd)
, m_LodMultHD(LodMultHD)
, m_LodMultOrphanedHD(LodMultOrphanedHD)
, m_LodMultLod(LodMultLod)
, m_LodMultSLod1(LodMultSLod1)
, m_LodMultSLod2(LodMultSLod2)
, m_LodMultSLod3(LodMultSLod3)
, m_LodMultSLod4(LodMultSLod4)
, m_WaterReflectionFarClip(WaterReflectionFarClip)
, m_SSAOLightInten(SSAOLightInten)
, m_ExposurePush(0.0f)
, m_DisableHighQualityDof(bDisableHighQualityDof)
, m_FreezeReflectionMap(FreezeReflectionMap)
, m_DisableDirectionalLighting(DisableDirectionalLighting)
, m_AbsoluteIntensityEnabled(AbsoluteIntensityEnabled)
, m_LightFadeDistanceMult(LightFadeDistanceMult)
, m_LightShadowFadeDistanceMult(LightShadowDistanceMult)
, m_LightSpecularFadeDistMult(LightSpecularFadeDistMult)
, m_LightVolumetricFadeDistanceMult(LightVolumetricFadeDistanceMult)
, m_DirectionalLightMultiplier(DirectionalLightMultiplier) 
, m_LensArtefactMultiplier(LensArtefactMultiplier) 
, m_BloomMax(BloomMax)
{

}

cutfCameraCutEventArgs::cutfCameraCutEventArgs( const char* pName, const Vector3 &vPosition, const Vector4 &vRotationQuaternion, 
											   float fNearDrawDistance, float fFarDrawDistance, float fMapLodScale, float ReflectionLodRangeStart,
											   float ReflectionLodRangeEnd, float ReflectionSLodRangeStart, float ReflectionSLodRangeEnd, 
											   float LodMultHD, float LodMultOrphanedHD, float LodMultLod, float LodMultSLod1, float LodMultSLod2,
											   float LodMultSLod3, float LodMultSLod4, float WaterReflectionFarClip, float SSAOLightInten, 
											   bool bDisableHighQualityDof, bool FreezeReflectionMap, bool DisableDirectionalLighting, 
											   bool AbsoluteIntensityEnabled, float LightFadeDistanceMult, float LightShadowDistanceMult, 
											   float LightSpecularFadeDistMult, float LightVolumetricFadeDistanceMult, 
											   float DirectionalLightMultiplier, float LensArtefactMultiplier, float BloomMax, const parAttributeList& attributes)
: cutfNameEventArgs(pName, attributes)
, m_vPosition(vPosition)
, m_vRotationQuaternion(vRotationQuaternion)
, m_fNearDrawDistance(fNearDrawDistance)
, m_fFarDrawDistance(fFarDrawDistance)
, m_fMapLodScale(fMapLodScale)
, m_ReflectionLodRangeStart(ReflectionLodRangeStart)
, m_ReflectionLodRangeEnd(ReflectionLodRangeEnd)
, m_ReflectionSLodRangeStart(ReflectionSLodRangeStart)
, m_ReflectionSLodRangeEnd(ReflectionSLodRangeEnd)
, m_LodMultHD(LodMultHD)
, m_LodMultOrphanedHD(LodMultOrphanedHD)
, m_LodMultLod(LodMultLod)
, m_LodMultSLod1(LodMultSLod1)
, m_LodMultSLod2(LodMultSLod2)
, m_LodMultSLod3(LodMultSLod3)
, m_LodMultSLod4(LodMultSLod4)
, m_WaterReflectionFarClip(WaterReflectionFarClip)
, m_SSAOLightInten(SSAOLightInten)
, m_ExposurePush(0.0f)
, m_DisableHighQualityDof(bDisableHighQualityDof)
, m_FreezeReflectionMap(FreezeReflectionMap)
, m_DisableDirectionalLighting(DisableDirectionalLighting)
, m_AbsoluteIntensityEnabled(AbsoluteIntensityEnabled)
, m_LightFadeDistanceMult(LightFadeDistanceMult)
, m_LightShadowFadeDistanceMult(LightShadowDistanceMult)
, m_LightSpecularFadeDistMult(LightSpecularFadeDistMult)
, m_LightVolumetricFadeDistanceMult(LightVolumetricFadeDistanceMult)
, m_DirectionalLightMultiplier(DirectionalLightMultiplier) 
, m_LensArtefactMultiplier(LensArtefactMultiplier) 
, m_BloomMax(BloomMax)
{

}

cutfCameraCutEventArgs::cutfCameraCutEventArgs( atHashString pName, const Vector3 &vPosition, const Vector4 &vRotationQuaternion, 
											   float fNearDrawDistance, float fFarDrawDistance, float fMapLodScale, float ReflectionLodRangeStart,
											   float ReflectionLodRangeEnd, float ReflectionSLodRangeStart, float ReflectionSLodRangeEnd, 
											   float LodMultHD, float LodMultOrphanedHD, float LodMultLod, float LodMultSLod1, float LodMultSLod2,
											   float LodMultSLod3, float LodMultSLod4, float WaterReflectionFarClip, float SSAOLightInten,
											   const parAttributeList& attributes, float fExposurePush, bool bDisableHighQualityDof,
											   const cutfCameraCutCharacterLightParams& lightParams, bool FreezeReflectionMap, bool DisableDirectionalLighting,
											   bool AbsoluteIntensityEnabled, float LightFadeDistanceMult, float LightShadowDistanceMult, 
											   float LightSpecularFadeDistMult, float LightVolumetricFadeDistanceMult, 
											    float DirectionalLightMultiplier, float LensArtefactMultiplier, float BloomMax,
												const atArray<cutfCameraCutTimeOfDayDofModifier> &TimeOfDayCamDepthOfFieldModifiers)
											   : cutfNameEventArgs(pName, attributes)
											   , m_vPosition(vPosition)
											   , m_vRotationQuaternion(vRotationQuaternion)
											   , m_fNearDrawDistance(fNearDrawDistance)
											   , m_fFarDrawDistance(fFarDrawDistance)
											   , m_fMapLodScale(fMapLodScale)
											   , m_ReflectionLodRangeStart(ReflectionLodRangeStart)
											   , m_ReflectionLodRangeEnd(ReflectionLodRangeEnd)
											   , m_ReflectionSLodRangeStart(ReflectionSLodRangeStart)
											   , m_ReflectionSLodRangeEnd(ReflectionSLodRangeEnd)
											   , m_LodMultHD(LodMultHD)
											   , m_LodMultOrphanedHD(LodMultOrphanedHD)
											   , m_LodMultLod(LodMultLod)
											   , m_LodMultSLod1(LodMultSLod1)
											   , m_LodMultSLod2(LodMultSLod2)
											   , m_LodMultSLod3(LodMultSLod3)
											   , m_LodMultSLod4(LodMultSLod4)
											   , m_WaterReflectionFarClip(WaterReflectionFarClip)
											   , m_SSAOLightInten(SSAOLightInten)
											   , m_ExposurePush(fExposurePush)
											   , m_DisableHighQualityDof(bDisableHighQualityDof)
											   , m_CharacterLight(lightParams)
											   , m_FreezeReflectionMap(FreezeReflectionMap)
											   , m_DisableDirectionalLighting(DisableDirectionalLighting)
											   , m_AbsoluteIntensityEnabled(AbsoluteIntensityEnabled)
											   , m_LightFadeDistanceMult(LightFadeDistanceMult)
											   , m_LightShadowFadeDistanceMult(LightShadowDistanceMult)
											   , m_LightSpecularFadeDistMult(LightSpecularFadeDistMult)
											   , m_LightVolumetricFadeDistanceMult(LightVolumetricFadeDistanceMult)
											   , m_DirectionalLightMultiplier(DirectionalLightMultiplier) 
											   , m_LensArtefactMultiplier(LensArtefactMultiplier) 
											   , m_BloomMax(BloomMax)
{
	m_TimeOfDayDofModifers = TimeOfDayCamDepthOfFieldModifiers; 
}

cutfEventArgs* cutfCameraCutEventArgs::Clone() const
{
    return rage_new cutfCameraCutEventArgs( m_cName, m_vPosition, m_vRotationQuaternion, m_fNearDrawDistance, m_fFarDrawDistance, 
		m_fMapLodScale, m_ReflectionLodRangeStart, m_ReflectionLodRangeEnd, m_ReflectionSLodRangeStart, m_ReflectionSLodRangeEnd,
		m_LodMultHD, m_LodMultOrphanedHD, m_LodMultLod, m_LodMultSLod1, m_LodMultSLod2, m_LodMultSLod3, m_LodMultSLod4, 
		m_WaterReflectionFarClip, m_SSAOLightInten, m_attributeList, m_ExposurePush, m_DisableHighQualityDof, m_CharacterLight, m_FreezeReflectionMap,
		m_DisableDirectionalLighting, m_AbsoluteIntensityEnabled, m_LightFadeDistanceMult, m_LightShadowFadeDistanceMult, m_LightSpecularFadeDistMult, 
		m_LightVolumetricFadeDistanceMult, m_LightVolumetricFadeDistanceMult, m_DirectionalLightMultiplier, m_LensArtefactMultiplier, m_TimeOfDayDofModifers);
}

bool cutfCameraCutEventArgs::AddCoCModifier(u32 hourFlags, s32 dofStrengthModifier)
{
	for(int i =0; i < m_TimeOfDayDofModifers.GetCount(); i++)
	{
		for(int j =0 ; j < 24; j++)
		{
			if (m_TimeOfDayDofModifers[i].m_TimeOfDayFlags & (1 << j) && hourFlags & (1 << j))
			{
				return false; 
			}
		}
	}
	
	cutfCameraCutTimeOfDayDofModifier& TimeofDayDofOverride = m_TimeOfDayDofModifers.Grow(); 
	TimeofDayDofOverride.m_TimeOfDayFlags = hourFlags; 
	TimeofDayDofOverride.m_DofStrengthModifier = dofStrengthModifier; 

	return true; 
}

void cutfCameraCutEventArgs::RemoveCoCModifier(s32 index)
{
	if(index >= 0)
	{
		if(index < m_TimeOfDayDofModifers.GetCount())
		{
			m_TimeOfDayDofModifers.Delete(index); 
		}
	}
}


bool cutfCameraCutEventArgs::operator == ( const cutfEventArgs &other ) const
{
    return (*this != other) == false;
}

bool cutfCameraCutEventArgs::operator != ( const cutfEventArgs &other ) const
{
    if ( cutfNameEventArgs::operator != ( other ) )
    {
        return true;
    }

    const cutfCameraCutEventArgs *pOtherEventArgs = static_cast<const cutfCameraCutEventArgs *>( &other );
    if ( (GetPosition() != pOtherEventArgs->GetPosition()) 
        || (GetRotationQuaternion() != pOtherEventArgs->GetRotationQuaternion())
        || (GetNearDrawDistance() != pOtherEventArgs->GetNearDrawDistance())
        || (GetFarDrawDistance() != pOtherEventArgs->GetFarDrawDistance()) )
    {
        return true;
    }

    return false;
}

//#############################################################################
cutfDecalEventArgs::cutfDecalEventArgs()
:cutfEventArgs()
,m_vPosition(VEC3_ZERO)
,m_vRotation(Vector4::ZeroType)
,m_fWidth(0.0f)
,m_fHeight(0.0f)
,m_Colour(0, 0, 0, 0)
,m_fLifeTime(-1.0f)
{

}

cutfDecalEventArgs::cutfDecalEventArgs( const Vector3 &Position, const Vector4 &Rotation, 
									   float width, float height, const Color32 &color, float lifeTime)
:cutfEventArgs()
,m_vPosition(Position)
,m_vRotation(Rotation)
,m_fWidth(width)
,m_fHeight(height)
,m_Colour(color)
,m_fLifeTime(lifeTime)
{
}

cutfDecalEventArgs::cutfDecalEventArgs( const Vector3 &Position, const Vector4 &Rotation, 
									   float width, float height, const Color32 &color, float lifeTime, 
									   const parAttributeList& attributes )
:cutfEventArgs(attributes)
,m_vPosition(Position)
,m_vRotation(Rotation)
,m_fWidth(width)
,m_fHeight(height)
,m_Colour(color)
,m_fLifeTime(lifeTime)
{
}

cutfEventArgs* cutfDecalEventArgs::Clone() const
{
	return rage_new cutfDecalEventArgs( m_vPosition, m_vRotation, m_fWidth, m_fHeight, 
		m_Colour, m_fLifeTime, m_attributeList );
}



bool cutfDecalEventArgs::operator == ( const cutfEventArgs &other ) const
{
	return (*this != other) == false;
}


bool cutfDecalEventArgs::operator != (const cutfEventArgs &other) const
{
	if ( cutfEventArgs::operator != ( other ) )
	{	
		return true;
	}

	const cutfDecalEventArgs *pOther = static_cast<const cutfDecalEventArgs *>( &other );

	if (pOther == NULL)
	{
		return true; 
	}

	if ( pOther->GetPosition() != m_vPosition )
	{
		return true;
	}
	
	if ( pOther->GetRotationQuaternion() != m_vRotation )
	{
		return true;
	}
	
	if ( pOther->GetLifeTime() != m_fLifeTime )
	{
		return true;
	}

	if ( pOther->GetWidth() != m_fWidth )
	{
		return true;
	}
	
	if ( pOther->GetHeight() != m_fHeight )
	{
		return true;
	}

	if ( pOther->GetColor() != m_Colour )
	{
		return true;
	}
	return false; 
}

//#############################################################################

cutfCascadeShadowEventArgs::cutfCascadeShadowEventArgs()
:cutfEventArgs()
,m_position(VEC3_ZERO)
,m_radius(0.0f)
,m_interpTime(0.0f)
,m_cascadeIndex(-1)
,m_enabled(false)
,m_interpolateToDisabled(false)
{
}

cutfCascadeShadowEventArgs::cutfCascadeShadowEventArgs( const atHashString cameraCutHash, const Vector3 &Position,float radius, float interpTime, 
													s32 cascadeIndex, bool enbaled, bool interpDisabled)
:cutfEventArgs()
,m_cameraCutHashName(cameraCutHash)
,m_position(Position)
,m_radius(radius)
,m_interpTime(interpTime)
,m_cascadeIndex(cascadeIndex)
,m_enabled(enbaled)
,m_interpolateToDisabled(interpDisabled)
{
}

cutfCascadeShadowEventArgs::cutfCascadeShadowEventArgs(const atHashString cameraCutHash,  const Vector3 &position,float radius, float interpTime, 
													   s32 cascadeIndex, bool enbaled, bool interpDisabled, const parAttributeList& attributes )
:cutfEventArgs(attributes)
,m_cameraCutHashName(cameraCutHash)
,m_position(position)
,m_radius(radius)
,m_interpTime(interpTime)
,m_cascadeIndex(cascadeIndex)
,m_enabled(enbaled)
,m_interpolateToDisabled(interpDisabled)
{
}

cutfEventArgs* cutfCascadeShadowEventArgs::Clone() const
{
	return rage_new cutfCascadeShadowEventArgs( m_cameraCutHashName, m_position, m_radius, m_interpTime, 
		m_cascadeIndex, m_enabled, m_interpolateToDisabled, m_attributeList );
}


s32 cutfCascadeShadowEventArgs::GetType() const
{
	return CUTSCENE_CASCADE_SHADOW_EVENT_ARGS; 
}

void cutfCascadeShadowEventArgs::SetCascadeShadowIndex(s32 ShadowIndex)
{
	m_cascadeIndex = ShadowIndex; 
}

s32 cutfCascadeShadowEventArgs::GetCascadeShadowIndex() const
{
	return m_cascadeIndex; 
}

void cutfCascadeShadowEventArgs::SetIsEnabled(bool enabled)
{
	m_enabled = enabled; 
}

bool cutfCascadeShadowEventArgs::GetIsEnabled() const
{
	return m_enabled; 
}

void cutfCascadeShadowEventArgs::SetInterpDisabled(bool enabled)
{
	m_interpolateToDisabled = enabled; 
}

bool cutfCascadeShadowEventArgs::GetIsInterpDisabled() const
{
	return m_interpolateToDisabled; 
}

void cutfCascadeShadowEventArgs::SetRadius(float radius)
{
	m_radius = radius; 
}

float cutfCascadeShadowEventArgs::GetRadius() const
{
	return m_radius; 
}

void cutfCascadeShadowEventArgs::SetInterpTime(float interTime)
{
	m_interpTime = interTime; 
}

float cutfCascadeShadowEventArgs::GetInterpTime() const
{
	return m_interpTime; 
}

void cutfCascadeShadowEventArgs::SetPosition(const Vector3& Position)
{
	m_position = Position; 
}

const Vector3& cutfCascadeShadowEventArgs::GetPosition() const
{
	return m_position; 
}

void cutfCascadeShadowEventArgs::SetCameraCutHash(atHashString hash)
{
	m_cameraCutHashName = hash; 
}

atHashString cutfCascadeShadowEventArgs::GetCameraCutHash() const
{
	return m_cameraCutHashName; 
}




//#############################################################################

} // namespace rage
