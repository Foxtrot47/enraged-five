// 
// cutfile/testcutfile.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cutfevent.h"
#include "cutfeventargs.h"
#include "cutfile2.h"
#include "cutfobject.h"
#include "atl/array.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "system/param.h"

using namespace rage;

PARAM(loadcutfile,"[testcutfile] The cutfile to load.");
PARAM(savecutfile,"[testcutfile] The cutfile to save.");

static const char* c_pFile1 = "file1.cutxml";

int Main()
{
    INIT_PARSER;
    cutfEvent::InitClass();

    cutfCutsceneFile2 cutFile;

    const char* pLoadCutfile;
    if ( PARAM_loadcutfile.Get( pLoadCutfile ) )
    {
        if ( !cutFile.LoadFile( pLoadCutfile ) )
        {
            Displayf( "Load file error." );

            cutfEvent::ShutdownClass();
            SHUTDOWN_PARSER;
            return 1;
        }
    }
    else
    {
        // create one on the fly
        cutFile.SetTotalDuration( 10.0f );

        atBitSet &bitSet = const_cast<atBitSet &>( cutFile.GetCutsceneFlags() );
        bitSet.Set( cutfCutsceneFile2::CUTSCENE_FADE_IN_GAME_FLAG, true );
        bitSet.Set( cutfCutsceneFile2::CUTSCENE_FADE_OUT_GAME_FLAG, true );
        bitSet.Set( cutfCutsceneFile2::CUTSCENE_FADE_IN_FLAG, true );
        bitSet.Set( cutfCutsceneFile2::CUTSCENE_FADE_OUT_FLAG, true );
                
        cutfAssetManagerObject *pAssetMgrObj = rage_new cutfAssetManagerObject();
        cutFile.AddObject( pAssetMgrObj );

        // animation manager
        cutfAnimationManagerObject *pAnimMgrObj = rage_new cutfAnimationManagerObject();
        cutFile.AddObject( pAnimMgrObj );

        cutFile.AddLoadEvent( rage_new cutfObjectIdEvent( pAnimMgrObj->GetObjectId(), 0.0f, CUTSCENE_LOAD_ANIM_DICT_EVENT, 
            rage_new cutfNameEventArgs( "TestAnimDict" ) ) );

        // models
        atArray<s32> loadModelObjectIdList;
        for ( int i = 0; i < 3; ++i )
        {
            cutfObject *pObject = NULL;
            switch ( i )
            {
            case 0:
                {
                    pObject = rage_new cutfPedModelObject( 0, "TestPed" );
                }
                break;
            case 1:
                {
                    pObject = rage_new cutfVehicleModelObject( 0, "TestVehicle" );
                }
                break;
            case 2:
                {
                    pObject = rage_new cutfPropModelObject( 0, "TestProp" );
                }
                break;
            }

            if ( pObject != NULL )
            {
                cutFile.AddObject( pObject );
                loadModelObjectIdList.Grow() = pObject->GetObjectId();

                cutfEventArgs *pEventArgs = rage_new cutfObjectIdNameEventArgs( pObject->GetObjectId(), pObject->GetDisplayName().c_str() );
                cutFile.AddEvent( rage_new cutfObjectIdEvent( pAnimMgrObj->GetObjectId(), 0.0f,
                    CUTSCENE_SET_ANIM_EVENT, pEventArgs ) );
                cutFile.AddEvent( rage_new cutfObjectIdEvent( pAnimMgrObj->GetObjectId(), cutFile.GetTotalDuration(),
                    CUTSCENE_CLEAR_ANIM_EVENT, pEventArgs ) );
            }
        }

        // load the models
        cutFile.AddLoadEvent( rage_new cutfObjectIdEvent( pAssetMgrObj->GetObjectId(), 0.0f, CUTSCENE_LOAD_MODELS_EVENT, 
            rage_new cutfObjectIdListEventArgs( loadModelObjectIdList ) ) );

        // camera
        cutfObject *pObject = rage_new cutfCameraObject( 0, "Camera" );
        cutFile.AddObject( pObject );

        cutfEventArgs *pEventArgs = rage_new cutfObjectIdNameEventArgs( pObject->GetObjectId(), "Camera" );
        cutFile.AddEvent( rage_new cutfObjectIdEvent( pAnimMgrObj->GetObjectId(), 0.0f,
            CUTSCENE_SET_ANIM_EVENT, pEventArgs ) );
        cutFile.AddEvent( rage_new cutfObjectIdEvent( pAnimMgrObj->GetObjectId(), cutFile.GetTotalDuration(),
            CUTSCENE_CLEAR_ANIM_EVENT, pEventArgs ) );

        cutFile.SortLoadEvents();
        cutFile.SortEvents();
    }
    
    const char* pSaveCutfile;
    if ( !PARAM_savecutfile.Get( pSaveCutfile ) )
    {
        pSaveCutfile = c_pFile1;
    }

    if ( !cutFile.SaveFile( pSaveCutfile ) )
    {
        Displayf( "Save file error." );
        
        cutfEvent::ShutdownClass();
        SHUTDOWN_PARSER;
        return 1;
    }

    cutfEvent::ShutdownClass();
    SHUTDOWN_PARSER;
    return 0;
}
