// 
// cutfile/cutfeventdef.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutfeventdef.h"
#include "cutfeventdef_parser.h"
#include "cutscene/cutsoptimisations.h"

#include "cutfchannel.h"
#include "cutfile2.h"
#include "atl/map.h"
#include "parser/manager.h"

CUTS_OPTIMISATIONS()

namespace rage 
{

//##############################################################################

const cutfEventDefs* cutfEventDefs::sm_pCurrentEventDefs = NULL;

cutfEventDefs::cutfEventDefs()
{
    SetHeaderFilename( NULL );
    SetHeaderNamespace( NULL );
}

cutfEventDefs::cutfEventDefs( const char *pHeaderFilename, const char *pHeaderNamespace, atArray<cutfEventDef *> &eventDefList )
{
    SetHeaderFilename( pHeaderFilename );
    SetHeaderNamespace( pHeaderNamespace );

    for ( int i = 0; i < eventDefList.GetCount(); ++i )
    {
        AddEventDef( eventDefList[i] );
    }

    eventDefList.Reset();
}

cutfEventDefs::~cutfEventDefs()
{
    Clear();
}

void cutfEventDefs::Clear()
{
    SetHeaderFilename( NULL );
    SetHeaderNamespace( NULL );

    for ( int i = 0; i < m_eventDefList.GetCount(); ++i )
    {
        delete m_eventDefList[i];
    }

    m_eventDefList.Reset();

    for ( int i = 0; i < m_eventArgsDefList.GetCount(); ++i )
    {
        delete m_eventArgsDefList[i];
    }

    m_eventArgsDefList.Reset();
}

#if __BANK || __TOOL
bool cutfEventDefs::SaveFile( const char* pFilename, const char* pExtension )
{
    // determine the save type
    char cSaveFilename[RAGE_MAX_PATH];
    ASSET.FullWritePath( cSaveFilename, RAGE_MAX_PATH, pFilename, pExtension );
    const char* pFileExtension = ASSET.FindExtensionInPath( cSaveFilename );
    if ( pFileExtension == NULL )
    {
        pExtension = pFileExtension = "xml";
    }

    // in case a non-standard method of adding objects was used
    EnforceSequentialOffsets();

    sm_pCurrentEventDefs = this;

    bool bResult = false;
    if ( strstr( pFileExtension, "xml" ) != NULL )
    {
        bResult = PARSER.SaveObject( pFilename, pExtension ? pExtension : "", this, parManager::XML );
    }
    else if ( strstr( pFileExtension, "bin" ) != NULL )
    {
        bResult = PARSER.SaveObject( pFilename, pExtension ? pExtension : "", this, parManager::BINARY );
    }
#if __DEV
    else
    {
        cutfErrorf( "File extension '%s' not supported.", pFileExtension );
    }
#endif

    sm_pCurrentEventDefs = NULL;

    return bResult;
}
#endif

bool cutfEventDefs::LoadFile( const char* pFilename, const char* pExtension )
{
    // determine the load type
    char cLoadFilename[RAGE_MAX_PATH];
    ASSET.FullReadPath( cLoadFilename, RAGE_MAX_PATH, pFilename, pExtension );
    const char* pFileExtension = ASSET.FindExtensionInPath( cLoadFilename );  
    if ( pFileExtension == NULL )
    {
        pExtension = pFileExtension = "xml";
    }

#if __DEV
    cutfDisplayf( "About to load event defs file '%s'.", cLoadFilename );
#endif

    // Based on the extension (and later, a version number), determine which Parse function to use
    bool bResult = false;
    if ( (strstr( pFileExtension, "xml" ) != NULL) || (strstr( pFileExtension, "bin" ) != NULL) )
    {
        parTree *pTree = PARSER.LoadTree( pFilename, pExtension ? pExtension : "" );
        if ( pTree )
        {
            bResult = ParseXml( pTree );
            delete pTree;
        }
#if __DEV
        else
        {
            cutfDisplayf( "Could not load tree." );
        }
#endif
    }
    else
    {
#if __DEV
        cutfErrorf( "File extension '%s' not supported.", pFileExtension );
#endif
    }

#if __DEV
    cutfDisplayf( "Load %s.", bResult ? "success" : "failed" );
#endif

    if ( bResult )
    {
        // in case there was some manual tampering with the file
        EnforceSequentialOffsets();
    }

    return bResult;
}

bool cutfEventDefs::ParseXml( parTree *pTree )
{
#if __DEV
    cutfDisplayf( "About to parse xml." );
#endif

    Clear();

    bool bResult = false;

    sm_pCurrentEventDefs = this;

    bResult = PARSER.LoadObject( pTree->GetRoot(), *this );

    //sm_pCurrentEventDefs = NULL;

#if __DEV
    if ( bResult )
    {
        Displayf("[CUTFEVENTDEFS] Xml fully parsed." );
    }
#endif

    return bResult;
}

void cutfEventDefs::AddEventDef( cutfEventDef *pEventDef )
{
    int iIndex = m_eventDefList.Find( pEventDef );
    if ( iIndex == -1 )
    {
        pEventDef->SetEventIdOffset( m_eventDefList.GetCount() );
        m_eventDefList.Grow() = pEventDef;
    }

    // then only add the event args def if it's the first ref
    if ( pEventDef->GetEventArgsDef() != NULL )
    {
        iIndex = m_eventArgsDefList.Find( const_cast<cutfEventArgsDef *>( pEventDef->GetEventArgsDef() ) );
        if ( iIndex == -1 )
        {
            cutfEventArgsDef *pEventArgsDef = const_cast<cutfEventArgsDef *>( pEventDef->GetEventArgsDef() );
            pEventArgsDef->SetEventArgsTypeOffset( m_eventArgsDefList.GetCount() );
            m_eventArgsDefList.Grow() = const_cast<cutfEventArgsDef *>( pEventDef->GetEventArgsDef() );            
        }
    }
}

void cutfEventDefs::RemoveEventDef( cutfEventDef *pEventDef )
{
    RemoveEventDef( m_eventDefList.Find( pEventDef ) );
}

void cutfEventDefs::RemoveEventDef( const char *pName )
{
    for ( int i = 0; i < m_eventDefList.GetCount(); ++i )
    {
        if ( strcmp( m_eventDefList[i]->GetName(), pName ) == 0 )
        {
            RemoveEventDef( i );
            break;
        }
    }
}

void cutfEventDefs::RemoveEventDef( s32 iEventIdOffset )
{
    if ( (iEventIdOffset < 0) || (iEventIdOffset >= m_eventDefList.GetCount()) )
    {
        return;
    }

    // if someone has us as an opposite, we must remove the connection
    for ( int i = 0; i < m_eventDefList.GetCount(); ++i )
    {
        if ( m_eventDefList[i]->GetOppositeEventIdOffset() == m_eventDefList[iEventIdOffset]->GetEventIdOffset() )
        {
            m_eventDefList[i]->SetOppositeEventIdOffset( CUTSCENE_NO_OPPOSITE_EVENT );
        }
    }

    const cutfEventArgsDef *pEventArgsDef = m_eventDefList[iEventIdOffset]->GetEventArgsDef();

    delete m_eventDefList[iEventIdOffset];
    m_eventDefList.Delete( iEventIdOffset );

    // only remove and delete the event args def if it was the last one
    if ( (pEventArgsDef != NULL) && (pEventArgsDef->GetRef() == 0) )
    {
        int index = m_eventArgsDefList.Find( const_cast<cutfEventArgsDef *>( pEventArgsDef ) );
        if ( index == -1 )
        {
            return;
        }

        // delete the event args def
        delete m_eventArgsDefList[index];
        m_eventArgsDefList.Delete( index );
    }
}

const char* cutfEventDefs::GetEventDisplayName( s32 iEventId )
{
    int iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
    if ( (iEventIdOffset >= 0) && (iEventIdOffset < m_eventDefList.GetCount()) )
    {
        return m_eventDefList[iEventIdOffset]->GetName();
    }

    return "(unknown)";
}

s32 cutfEventDefs::GetEventIdRank( s32 iEventId )
{
    int iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
    if ( (iEventIdOffset >= 0) && (iEventIdOffset < m_eventDefList.GetCount()) )
    {
        return m_eventDefList[iEventIdOffset]->GetRank();
    }

    return CUTSCENE_LAST_EVENT_RANK;
}

s32 cutfEventDefs::GetOppositeEventId( s32 iEventId )
{
    int iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
    if ( (iEventIdOffset >= 0) && (iEventIdOffset < m_eventDefList.GetCount()) )
    {
        return m_eventDefList[iEventIdOffset]->GetOppositeEventId();
    }

    return CUTSCENE_NO_OPPOSITE_EVENT;
}

const char* cutfEventDefs::GetEventArgsTypeName( s32 iEventArgsType )
{
    int iEventArgsTypeOffset = iEventArgsType - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE;
    if ( (iEventArgsTypeOffset >= 0) && (iEventArgsTypeOffset < m_eventArgsDefList.GetCount()) )
    {
        return m_eventArgsDefList[iEventArgsTypeOffset]->GetName();
    }

    return "(unknown)";
}

int cutfEventDefs::GetEventArgsDefIndex( const cutfEventArgsDef *pEventArgsDef )
{
    cutfAssertf( sm_pCurrentEventDefs, "sm_pCurrentEventDefs needs to be set before calling PARSER functions on the cutfEventDefs class." );

   return sm_pCurrentEventDefs->m_eventArgsDefList.Find( const_cast<cutfEventArgsDef*>(pEventArgsDef) );
}

cutfEventArgsDef* cutfEventDefs::FindEventArgsDef( int iEventArgsDefIndex )
{
    cutfAssertf( sm_pCurrentEventDefs, "sm_pCurrentEventDefs needs to be set before calling PARSER functions on the cutfEventDefs class." );

    if ( (iEventArgsDefIndex >= 0) && (iEventArgsDefIndex < sm_pCurrentEventDefs->m_eventArgsDefList.GetCount()) )
    {
        sm_pCurrentEventDefs->m_eventArgsDefList[iEventArgsDefIndex]->AddRef();
        return const_cast<cutfEventArgsDef *>( sm_pCurrentEventDefs->m_eventArgsDefList[iEventArgsDefIndex] );
    }

    return NULL;
}

void cutfEventDefs::EnforceSequentialOffsets()
{
    atMap<s32, s32> oldToNewIdOffsetMap;
    for ( int i = 0; i < m_eventDefList.GetCount(); ++i )
    {
        oldToNewIdOffsetMap.Insert( m_eventDefList[i]->GetEventIdOffset(), i );

        m_eventDefList[i]->SetEventIdOffset( i );
    }

    for ( int i = 0; i < m_eventDefList.GetCount(); ++i )
    {
        if ( m_eventDefList[i]->GetOppositeEventIdOffset() >= 0 )
        {
            s32 *pNewIdOffset = oldToNewIdOffsetMap.Access( m_eventDefList[i]->GetOppositeEventIdOffset() );
            if ( pNewIdOffset != NULL )
            {
                m_eventDefList[i]->SetOppositeEventIdOffset( *pNewIdOffset );
            }
        }
    }

    for ( int i = 0; i < m_eventArgsDefList.GetCount(); ++i )
    {
        m_eventArgsDefList[i]->SetEventArgsTypeOffset( i );
    }
}

//##############################################################################

cutfEventDef::cutfEventDef()
: m_iEventIdOffset(-1)
, m_iRank(CUTSCENE_LAST_EVENT_RANK)
, m_iOppositeEventIdOffset(-1)
, m_iEventArgsDefIndex(-1)
, m_pEventArgsDefPtr(NULL)
{
    SetName( NULL );
}

cutfEventDef::cutfEventDef( s32 iEventIdOffset, const char *pName, s32 iRank, s32 iOppositeEventIdOffset, const cutfEventArgsDef *pEventArgsDef )
: m_iEventIdOffset(iEventIdOffset)
, m_iRank(iRank)
, m_iOppositeEventIdOffset(iOppositeEventIdOffset)
, m_iEventArgsDefIndex(-1)
, m_pEventArgsDefPtr(NULL)
{
    SetName( pName );
    SetEventArgsDef( pEventArgsDef );
}

cutfEventDef::~cutfEventDef()
{
    SetEventArgsDef( NULL );
}

void cutfEventDef::GetStaticEnumName( const char *pEventName, char *pBuffer, int iLen )
{
    char cEnumName[CUTSCENE_LONG_OBJNAMELEN];
    sprintf( cEnumName, "CUTSCENE_%s_EVENT", pEventName );
    strupr( cEnumName );

    int len = (int)strlen( cEnumName );
    for ( int i = 0; i < len; ++i )
    {
        if ( !isalnum( cEnumName[i] ) )
        {
            cEnumName[i] = '_';
        }
    }

    safecpy( pBuffer, cEnumName, iLen );
}

void cutfEventDef::SetEventArgsDef( const cutfEventArgsDef* pEventArgsDef )
{
    if ( m_pEventArgsDefPtr != NULL )
    {
        m_pEventArgsDefPtr->Release();
    }

    m_pEventArgsDefPtr = pEventArgsDef;

    if ( m_pEventArgsDefPtr )
    {
        m_pEventArgsDefPtr->AddRef();
    }
}

void cutfEventDef::PreLoad(parTreeNode* node)
{
	parTreeNode* oldArgs = node->FindChildWithName("pEventArgsDef");
	if (oldArgs)
	{
		oldArgs->GetElement().SetName("iEventArgsDefIndex");
		int oldIdx = oldArgs->GetElement().FindAttributeIntValue("ref", -1, true);
		oldArgs->GetElement().AddAttribute("value", oldIdx, false);
	}
}

void cutfEventDef::ConvertArgIndicesToPointers()
{
	m_pEventArgsDefPtr = cutfEventDefs::FindEventArgsDef(m_iEventArgsDefIndex);
}

void cutfEventDef::ConvertArgPointersToIndices()
{
	m_iEventArgsDefIndex = cutfEventDefs::GetEventArgsDefIndex(m_pEventArgsDefPtr);
}

//##############################################################################

cutfEventArgsDef::cutfEventArgsDef()
: m_iRefCount(0)
, m_cutfAttributes(NULL)
{
    SetName( NULL );
}

cutfEventArgsDef::cutfEventArgsDef( s32 iEventArgsTypeOffset, const char *pName )
: m_iEventArgsTypeOffset(iEventArgsTypeOffset)
, m_iRefCount(0)
, m_cutfAttributes(NULL)
{
    SetName( pName );
}

cutfEventArgsDef::cutfEventArgsDef( s32 iEventArgsTypeOffset, const char *pName, parAttributeList &attributeList )
: m_iEventArgsTypeOffset(iEventArgsTypeOffset)
, m_iRefCount(0)
, m_cutfAttributes(NULL)
{
    SetName( pName );
    m_attributeList.CopyFrom( attributeList );
}

cutfEventArgsDef::~cutfEventArgsDef()
{
	delete m_cutfAttributes;
}

bool cutfEventArgsDef::operator!=( const cutfEventArgsDef &other ) const
{
    if ( this == &other )
    {
        return false;
    }

    if ( strcmp( GetName(), other.GetName() ) != 0 )
    {
        return true;
    }

    const atArray<parAttribute> &attArray = GetAttributeList().GetAttributeArray();
    const atArray<parAttribute> &otherAttArray = other.GetAttributeList().GetAttributeArray();

    if ( attArray.GetCount() != otherAttArray.GetCount() )
    {
        return true;
    }

    for ( int i = 0; i < attArray.GetCount(); ++i )
    {
        if ( attArray[i].GetType() != otherAttArray[i].GetType() )
        {
            return true;
        }

        if ( strcmp( attArray[i].GetName(), otherAttArray[i].GetName() ) != 0 )
        {
            return true;
        }

        char stringValue[64];
        char otherStringValue[64];
        if ( strcmp( attArray[i].GetStringRepr( stringValue, sizeof(stringValue) ), 
            otherAttArray[i].GetStringRepr( otherStringValue, sizeof(otherStringValue) ) ) != 0 )
        {
            return true;
        }
    }

    return false;
}

void cutfEventArgsDef::ConvertAttributesForSave()
{
	cutfCutsceneFile2::ConvertToCutfAttributeList(m_attributeList, m_cutfAttributes);
}

void cutfEventArgsDef::ConvertAttributesAfterLoad()
{
	cutfCutsceneFile2::ConvertToParAttributeList(m_cutfAttributes, m_attributeList);
}

//##############################################################################

} // namespace rage
