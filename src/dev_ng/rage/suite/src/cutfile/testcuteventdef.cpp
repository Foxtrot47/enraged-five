// 
// /testcuteventdef.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "cutfile/cutfeventdef.h"
#include "parser/macros.h"

#include "system/param.h"

using namespace rage;

PARAM(loadfile,"[testcuteventdef] The cutscene event definition file to load.");
PARAM(savefile,"[testcuteventdef] The cutscene event definition file to save.");

static const char* c_pFile1 = "file1.xml";

int Main()
{
    INIT_PARSER;

    cutfEventDefs eventDefs;

    const char* pLoadFile;
    if ( PARAM_loadfile.Get( pLoadFile ) )
    {
        if ( !eventDefs.LoadFile( pLoadFile ) )
        {
            Displayf( "Load file error." );

            SHUTDOWN_PARSER;
            return 1;
        }
    }
    else
    {
        // create one on the fly
        eventDefs.SetHeaderFilename( "customCutEvents.h" );
        eventDefs.SetHeaderNamespace( "rage" );

        // create an event args def
        cutfEventArgsDef *pEventArgsDef1 = rage_new cutfEventArgsDef( 0, "BoolInt" );
        parAttributeList &atts1 = const_cast<parAttributeList &>( pEventArgsDef1->GetAttributeList() );
        atts1.AddAttribute( "BoolValue", true );
        atts1.AddAttribute( "IntValue", 4 );

        // create a start and a stop event that are the opposite of each other
        eventDefs.AddEventDef( rage_new cutfEventDef( 0, "TestStart", CUTSCENE_LAST_EVENT_RANK + 1, 1, pEventArgsDef1 ) );
        eventDefs.AddEventDef( rage_new cutfEventDef( 1, "TestEnd", CUTSCENE_LAST_EVENT_RANK, 0, pEventArgsDef1 ) );

        // create another event args def
        cutfEventArgsDef *pEventArgsDef2 = rage_new cutfEventArgsDef( 1, "FloatString" );
        parAttributeList &atts2 = const_cast<parAttributeList &>( pEventArgsDef2->GetAttributeList() );
        atts2.AddAttribute( "FloatValue", 1.5f );
        atts2.AddAttribute( "StringValue", "test" );

        // create a start event whose opposite is the previous instance
        eventDefs.AddEventDef( rage_new cutfEventDef( 2, "TestOppIsPrev", CUTSCENE_LAST_EVENT_RANK + 2, 
            CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS, pEventArgsDef2 ) );

        // create another event args def
        cutfEventArgsDef *pEventArgsDef3 = rage_new cutfEventArgsDef( 2, "TwoFloats" );
        parAttributeList &atts3 = const_cast<parAttributeList &>( pEventArgsDef3->GetAttributeList() );
        atts3.AddAttribute( "FloatValue1", 1.5f );
        atts3.AddAttribute( "FloatValue2", 3.26f );

        // create an event that doesn't have an opposite
        eventDefs.AddEventDef( rage_new cutfEventDef( 3, "TestNoOpp", CUTSCENE_LAST_EVENT_RANK + 3, 
            CUTSCENE_NO_OPPOSITE_EVENT, pEventArgsDef3 ) );

        // make sure that the operator== is working
        cutfEventArgsDef *pEventArgsDef4 = rage_new cutfEventArgsDef( 3, "TwoFloats" );
        parAttributeList &atts4 = const_cast<parAttributeList &>( pEventArgsDef4->GetAttributeList() );
        atts4.AddAttribute( "FloatValue1", 1.5f );
        atts4.AddAttribute( "FloatValue2", 3.26f );

        if ( *pEventArgsDef3 == *pEventArgsDef4 )
        {
            delete pEventArgsDef4;
            
            Displayf( "Successfully detected an event args def equality." );
        }
        else
        {
            delete pEventArgsDef4;

            Errorf( "Should have detected an event args def equality." );

            SHUTDOWN_PARSER;
            return 1;
        }

        // make sure the operator!= is working
        if ( *pEventArgsDef1 != *pEventArgsDef2 )
        {
            Displayf( "Successfully detected an event args def inequality." );
        }
        else
        {
            Errorf( "Should have detected an event args def inequality." );

            SHUTDOWN_PARSER;
            return 1;
        }
    }

    const char* pSaveFile;
    if ( !PARAM_savefile.Get( pSaveFile ) )
    {
        pSaveFile = c_pFile1;
    }

    if ( !eventDefs.SaveFile( pSaveFile ) )
    {
        Displayf( "Save file error." );

        SHUTDOWN_PARSER;
        return 1;
    }

    SHUTDOWN_PARSER;
    return 0;
}
