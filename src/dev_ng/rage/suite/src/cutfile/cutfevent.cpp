// 
// cutscene/cutsevent.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutfevent.h"

#include "cutscene/cutsoptimisations.h"
#include "cutfeventargs.h"
#include "cutfile2.h"   // must have this before the parser
#include "cutfevent_parser.h"

#include "diag/tracker.h"
#include "parser/treenode.h"
#include "system/nelem.h"

CUTS_OPTIMISATIONS()

namespace rage {

//##############################################################################

atMap<s32, cutfEvent::EventIdDisplayNameFtor> cutfEvent::s_eventIdDisplayNameFtorMap;
atMap<s32, cutfEvent::EventIdRankFtor> cutfEvent::s_eventIdRankFtorMap;
atMap<s32, cutfEvent::OppositeEventIdFtor> cutfEvent::s_oppositeEventIdFtorMap;

#if __BANK || __TOOL
const char* cutfEvent::c_cutsceneEventIdDisplayNames[] = 
{
    "Load Scene",
    "Unload Scene",
    "Load Anim Dict",
    "Unload Anim Dict",
    "Load Audio",
    "Unload Audio",
    "Load Models",
    "Unload Models",
    "Load Particle Effects",
    "Unload Particle Effects",
    "Load Overlays",
    "Unload Overlays",
    "Load Subtitles",
    "Unload Subtitles",
    "Hide Objects",
    "Show Objects",
    "Fixup Objects",
    "Revert Fixup Objects",
    "Add Blocking Bounds",
    "Remove Blocking Bounds",
    "Fade Out",
    "Fade In",
    "Set Anim",
    "Clear Anim",
    "Play Particle Effect",
    "Stop Particle Effect",
    "Show Overlay",
    "Hide Overlay",
    "Play Audio",
    "Stop Audio",
    "Show Subtitle",
    "Hide Subtitle",
    "Set Draw distance",
    "Set Attachment",
    "Set Variation",
    "Activate Blocking Bound",
    "Deactivate Blocking Bound",
    "Hide Hidden Object",
    "Show Hidden Object",
    "Fix Fixup Object",
    "Revert Fixup Object",
    "Add Removal Bounds",
    "Remove Removal Bounds",
    "Camera Cut",
    "Activate Removal Bound",
    "Deactivate Removal Bound",
	"Load Rayfire",
	"Unload Rayfire",
	"Enable DOF",
	"Disable DOF",
	"Catchup Camera",
	"BlendOut Camera",
	"Trigger Decal",
	"Remove Decal",
	"Cascade Shadow Bounds",
	"Cascade Shadow Enable Entity",
	"Cascade Shadow Set World Height Update",
	"Cascade Shadow Set Receiver Height Update",
	"Cascade Shadow Set Aircraft Mode",
	"Cascade Shadow Set Dynamic Depth Mode",
	"Cascade Shadow Set Fly Camera Mode",
	"Cascade Shadow Set Cascade Bounds HFOV",
	"Cascade Shadow Set Cascade Bounds VFOV",
	"Cascade Shadow Set Cascade Bounds Scale",
	"Cascade Shadow Set Entity Tracker Scale",
	"Cascade Shadow Set Split Z Expr Weight",
	"Cascade Shadow Set Dither Radius Scale",
	"Cascade Shadow Set World Height MinMax",
	"Cascade Shadow Set Receiver Height MinMax",
	"Cascade Shadow Set Depth Bias",
	"Cascade Shadow Set Slope Bias",
	"Cascade Shadow Set Shadow Sample Type",
	"Reset Adaption",
	"Cascade Shadow Set Dynamic Depth Value",
	"Set Light Event",
	"Clear Light Event",
	"Cascade Shadow Reset Cascade Shadows",
	"Start Replay Record",
	"Stop Replay Record",
	"First Person BlendOut Camera",
	"First Person CathUp Camera",
};
#endif
const s32 cutfEvent::c_oppositeEventIds[] =
{
    CUTSCENE_UNLOAD_SCENE_EVENT,
    CUTSCENE_LOAD_SCENE_EVENT,
    CUTSCENE_UNLOAD_ANIM_DICT_EVENT,
    CUTSCENE_LOAD_ANIM_DICT_EVENT,
    CUTSCENE_UNLOAD_AUDIO_EVENT,
    CUTSCENE_LOAD_AUDIO_EVENT,
    CUTSCENE_UNLOAD_MODELS_EVENT,
    CUTSCENE_LOAD_MODELS_EVENT,
    CUTSCENE_UNLOAD_PARTICLE_EFFECTS_EVENT,
    CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT,
    CUTSCENE_UNLOAD_OVERLAYS_EVENT,
    CUTSCENE_LOAD_OVERLAYS_EVENT,
    CUTSCENE_UNLOAD_SUBTITLES_EVENT,
    CUTSCENE_LOAD_SUBTITLES_EVENT,
    CUTSCENE_SHOW_OBJECTS_EVENT,
    CUTSCENE_HIDE_OBJECTS_EVENT,
    CUTSCENE_REVERT_FIXUP_OBJECTS_EVENT,
    CUTSCENE_FIXUP_OBJECTS_EVENT,
    CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT,
    CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT,

    CUTSCENE_NO_OPPOSITE_EVENT, // CUTSCENE_FADE_OUT_EVENT has no opposite
    CUTSCENE_NO_OPPOSITE_EVENT, // CUTSCENE_FADE_IN_EVENT has no opposite
    CUTSCENE_CLEAR_ANIM_EVENT,
    CUTSCENE_SET_ANIM_EVENT,
    CUTSCENE_STOP_PARTICLE_EFFECT_EVENT,
    CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT,
    CUTSCENE_HIDE_OVERLAY_EVENT,
    CUTSCENE_SHOW_OVERLAY_EVENT,
    CUTSCENE_STOP_AUDIO_EVENT,
    CUTSCENE_PLAY_AUDIO_EVENT,
    CUTSCENE_HIDE_SUBTITLE_EVENT,
    CUTSCENE_SHOW_SUBTITLE_EVENT,
    CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS, // CUTSCENE_SET_DRAW_DISTANCE_EVENT uses the previous instance of this event
    CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS, // CUTSCENE_SET_ATTACHMENT_EVENT uses the previous instance of this event
    CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS, // CUTSCENE_SET_VARIATION_EVENT uses the previous instance of this event
    CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT,
    CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT,
    CUTSCENE_SHOW_HIDDEN_OBJECT_EVENT,
    CUTSCENE_HIDE_HIDDEN_OBJECT_EVENT,
    CUTSCENE_REVERT_FIXUP_OBJECT_EVENT,
    CUTSCENE_FIX_FIXUP_OBJECT_EVENT,
    CUTSCENE_REMOVE_REMOVAL_BOUNDS_EVENT,
    CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT,
    CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS,   // CUTSCENE_CAMERA_CUT_EVENT uses the previous instance of this event
    CUTSCENE_DEACTIVATE_REMOVAL_BOUNDS_EVENT,
    CUTSCENE_ACTIVATE_REMOVAL_BOUNDS_EVENT, 
	CUTSCENE_UNLOAD_RAYFIRE_EVENT,
	CUTSCENE_LOAD_RAYFIRE_EVENT,
	CUTSCENE_DISABLE_DOF_EVENT,
	CUTSCENE_ENABLE_DOF_EVENT,
	CUTSCENE_NO_OPPOSITE_EVENT, //catchup cam
	CUTSCENE_NO_OPPOSITE_EVENT, //blendout
	CUTSCENE_REMOVE_DECAL_EVENT, 
	CUTSCENE_TRIGGER_DECAL_EVENT, 
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_ENABLE_CASCADE_SHADOW_BOUNDS_EVENT
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_UPDATE
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_UPDATE
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_AIRCRAFT_MODE
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_FLY_CAMERA_MODE
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_MINMAX
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_MINMAX
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_DEPTH_BIAS
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_SLOPE_BIAS
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_RESET_ADAPTION_EVENT
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE
	CUTSCENE_CLEAR_LIGHT_EVENT, 
	CUTSCENE_SET_LIGHT_EVENT,
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_CASCADE_SHADOWS_RESET_CASCADE_SHADOWS
	CUTSCENE_STOP_REPLAY_RECORD, //CUTSCENE_START_RELPLAY_RECORD
	CUTSCENE_START_REPLAY_RECORD, //CUTSCENE_STOP_RELPLAY_RECORD
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_FIRST_PERSON_BLENDOUT_CAMERA_EVENT
	CUTSCENE_NO_OPPOSITE_EVENT, //CUTSCENE_FIRST_PERSON_CATCHUP_CAMERA_EVENT
};

const s32 cutfEvent::c_cutsceneEventSortingRanks[] = 
{
    0,                                      // CUTSCENE_LOAD_SCENE_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK,        // CUTSCENE_UNLOAD_SCENE_EVENT
    9,                                      // CUTSCENE_LOAD_ANIM_DICT_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 9,    // CUTSCENE_UNLOAD_ANIM_DICT_EVENT
    1,                                      // CUTSCENE_LOAD_AUDIO_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 1,    // CUTSCENE_UNLOAD_AUDIO_EVENT
    2,                                      // CUTSCENE_LOAD_MODELS_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 2,    // CUTSCENE_UNLOAD_MODELS_EVENT
    3,                                      // CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 3,    // CUTSCENE_UNLOAD_PARTICLE_EFFECTS_EVENT
    4,                                      // CUTSCENE_LOAD_OVERLAYS_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 4,    // CUTSCENE_UNLOAD_OVERLAYS_EVENT
    5,                                      // CUTSCENE_LOAD_SUBTITLES_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 5,    // CUTSCENE_UNLOAD_SUBTITLES_EVENT
    6,                                      // CUTSCENE_HIDE_OBJECTS_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 6,    // CUTSCENE_SHOW_OBJECTS_EVENT
    7,                                      // CUTSCENE_FIXUP_OBJECTS_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 7,    // CUTSCENE_REVERT_FIXUP_OBJECTS_EVENT
    8,                                      // CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 8,    // CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT

    20,                                     // CUTSCENE_FADE_OUT_EVENT
    21,                                     // CUTSCENE_FADE_IN_EVENT
    23,                                     // CUTSCENE_SET_ANIM_EVENT
    22,                                     // CUTSCENE_CLEAR_ANIM_EVENT
    25,                                     // CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT
    24,                                     // CUTSCENE_STOP_PARTICLE_EFFECT_EVENT
    27,                                     // CUTSCENE_SHOW_OVERLAY_EVENT
    26,                                     // CUTSCENE_HIDE_OVERLAY_EVENT
    29,                                     // CUTSCENE_PLAY_AUDIO_EVENT
    28,                                     // CUTSCENE_STOP_AUDIO_EVENT
    31,                                     // CUTSCENE_SHOW_SUBTITLE_EVENT
    30,                                     // CUTSCENE_HIDE_SUBTITLE_EVENT
    32,                                     // CUTSCENE_SET_DRAW_DISTANCE_EVENT
    33,                                     // CUTSCENE_SET_ATTACHMENT_EVENT
    34,                                     // CUTSCENE_SET_VARIATION_EVENT
    36,                                     // CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT
    35,                                     // CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT
    38,                                     // CUTSCENE_HIDE_HIDDEN_OBJECT_EVENT
    37,                                     // CUTSCENE_SHOW_HIDDEN_OBJECT_EVENT
    39,                                     // CUTSCENE_FIX_FIXUP_OBJECT_EVENT
    38,                                     // CUTSCENE_REVERT_FIXUP_OBJECT_EVENT
    10,                                     // CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT
    CUTSCENE_LAST_UNLOAD_EVENT_RANK - 10,   // CUTSCENE_REMOVE_REMOVAL_BOUNDS_EVENT
    40,                                     // CUTSCENE_CAMERA_CUT_EVENT
    42,                                     // CUTSCENE_ACTIVATE_REMOVAL_BOUNDS_EVENT
    41,                                     // CUTSCENE_DEACTIVATE_REMOVAL_BOUNDS_EVENT
};



cutfEvent::cutfEvent()
: m_fTime( 0.0f )
, m_iEventId( -1 )
, m_iEventArgsIndex(-1)
, m_pEventArgsPtr( NULL )
, m_pChildEvents( NULL )
, m_StickyId(0)
, m_IsChild(false)
{

}

cutfEvent::cutfEvent( float fTime, s32 iEventId, cutfEventArgs* pEventArgs )
: m_fTime( fTime )
, m_iEventId( iEventId )
, m_iEventArgsIndex( -1 )
, m_pEventArgsPtr( pEventArgs )
, m_pChildEvents( NULL )
, m_StickyId(0)
, m_IsChild( false )
{
    if ( m_pEventArgsPtr )
    {
        m_pEventArgsPtr->AddRef();
    }
}

cutfEvent::cutfEvent( float fTime, s32 iEventId, bool IsChild, u32 StickyId, cutfEventArgs *pEventArgs )
	: m_fTime( fTime )
	, m_iEventId( iEventId )
	, m_iEventArgsIndex( -1 )
	, m_pEventArgsPtr( pEventArgs )
	, m_pChildEvents( NULL )
	, m_StickyId(StickyId)
	, m_IsChild(IsChild)
{
	if ( m_pEventArgsPtr )
	{
		m_pEventArgsPtr->AddRef();
	}
}

cutfEvent::~cutfEvent()
{
    if ( m_pEventArgsPtr )
    {
        m_pEventArgsPtr->Release();
        m_pEventArgsPtr = NULL;
    }

	if(m_pChildEvents)
	{
		delete m_pChildEvents; 
		m_pChildEvents = NULL; 
	}
}

void cutfEvent::InitClass()
{
    s_eventIdDisplayNameFtorMap.Kill();
    s_eventIdRankFtorMap.Kill();
    s_oppositeEventIdFtorMap.Kill();

    cutfEventArgs::InitClass();
}

void cutfEvent::ShutdownClass()
{
    s_eventIdDisplayNameFtorMap.Kill();
    s_eventIdRankFtorMap.Kill();
    s_oppositeEventIdFtorMap.Kill();

    cutfEventArgs::ShutdownClass();
}

void cutfEvent::ResetChildEvents()
{
	if(m_pChildEvents)
	{
		m_pChildEvents->m_ChildEvents.Reset(); 
	}
}

void cutfEvent::AddChildEventIndex(u32 Index)
{
	if(m_pChildEvents == NULL)
	{
		m_pChildEvents = rage_new cutfChildEvents(); 
	} 

	for(int i = 0; i < m_pChildEvents->m_ChildEvents.GetCount(); i++)
	{
		if(m_pChildEvents->m_ChildEvents[i] == Index)
		{
			return; 
		}
	}

	m_pChildEvents->m_ChildEvents.PushAndGrow(Index); 
}


void cutfEvent::SetEventIdDisplayNameFunc( EventIdDisplayNameFtor ftor, s32 iFirstEventId )
{
    if ( ftor != 0 )
    {
        EventIdDisplayNameFtor *pFtor = s_eventIdDisplayNameFtorMap.Access( iFirstEventId );
        if ( pFtor == NULL  )
        {
            s_eventIdDisplayNameFtorMap.Insert( iFirstEventId, ftor );
        }
    }
    else
    {
        s_eventIdDisplayNameFtorMap.Delete( iFirstEventId );
    }
}

void cutfEvent::SetEventIdRankFunc( EventIdRankFtor ftor, s32 iFirstEventId )
{
    if ( ftor != 0 )
    {
        EventIdRankFtor *pFtor = s_eventIdRankFtorMap.Access( iFirstEventId );
        if ( pFtor == NULL  )
        {
            s_eventIdRankFtorMap.Insert( iFirstEventId, ftor );
        }
    }
    else
    {
        s_eventIdRankFtorMap.Delete( iFirstEventId );
    }
}

void cutfEvent::SetOppositeEventIdFunc( OppositeEventIdFtor ftor, s32 iFirstEventId )
{
    if ( ftor != 0 )
    {
        OppositeEventIdFtor *pFtor = s_oppositeEventIdFtorMap.Access( iFirstEventId );
        if ( pFtor == NULL  )
        {
            s_oppositeEventIdFtorMap.Insert( iFirstEventId, ftor );
        }
    }
    else
    {
        s_oppositeEventIdFtorMap.Delete( iFirstEventId );
    }
}

#if __BANK || __TOOL
const atString& cutfEvent::GetLongDisplayName() const
{
    RAGE_TRACK( cutfEvent_GetLongDisplayName );

    if ( m_longDisplayName.GetLength() == 0 )
    {
        char cDisplayName[256];
        sprintf( cDisplayName, "%s: %s", GetTypeName(), GetDisplayName( GetEventId() ) );

        m_longDisplayName.Set( cDisplayName, (int)strlen( cDisplayName ), 0 );
    }
    
    return m_longDisplayName;
}
#endif

void cutfEvent::SetEventArgs( cutfEventArgs *pEventArgs )
{
    if ( m_pEventArgsPtr )
    {
        m_pEventArgsPtr->Release();
    }

    m_pEventArgsPtr = pEventArgs;

    if ( m_pEventArgsPtr )
    {
        m_pEventArgsPtr->AddRef();
    }
}

cutfEvent* cutfEvent::Clone() const
{
    return rage_new cutfEvent( m_fTime, m_iEventId, m_IsChild, m_StickyId, m_pEventArgsPtr );
}

#if __BANK || __TOOL
const char* cutfEvent::GetDisplayName( s32 iEventId )
{
	//Event ids display names must match number of events in enum ECutsceneEvent
	CompileTimeAssert(NELEM(c_cutsceneEventIdDisplayNames) == CUTSCENE_NUM_EVENTS);  
	
	if ( (iEventId >= 0) && (iEventId < CUTSCENE_NUM_EVENTS) )
    {
        return c_cutsceneEventIdDisplayNames[iEventId];
    }

    int iSmallestDifference = 9999;

    EventIdDisplayNameFtor ftor = MakeFunctorRet( &DefaultEventIdDisplayNameFtor );
    atMap<s32, EventIdDisplayNameFtor>::Iterator entry = s_eventIdDisplayNameFtorMap.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        if ( iEventId >= entry.GetKey() )
        {
            int iDifference = iEventId - entry.GetKey();
            if ( iDifference < iSmallestDifference )
            {
                ftor = entry.GetData();

                iSmallestDifference = iDifference;
            }
        }
    }

    return ftor( iEventId );
}
#endif

s32 cutfEvent::GetRank( s32 iEventId )
{
    if ( (iEventId >= 0) && (iEventId < CUTSCENE_NUM_EVENTS) )
    {
        return c_cutsceneEventSortingRanks[iEventId];
    }

    int iSmallestDifference = 9999;

    EventIdRankFtor ftor = MakeFunctorRet( &DefaultEventIdRankFtor );
    atMap<s32, EventIdRankFtor>::Iterator entry = s_eventIdRankFtorMap.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        if ( iEventId >= entry.GetKey() )
        {
            int iDifference = iEventId - entry.GetKey();
            if ( iDifference < iSmallestDifference )
            {
                ftor = entry.GetData();

                iSmallestDifference = iDifference;
            }
        }
    }

    return ftor( iEventId );
}

s32 cutfEvent::GetOppositeEventId( s32 iEventId )
{
	//oppposite events must match number of events in enum ECutsceneEvent
	CompileTimeAssert(NELEM(c_oppositeEventIds) == CUTSCENE_NUM_EVENTS);  
	
	if ( (iEventId >= 0) && (iEventId < CUTSCENE_NUM_EVENTS) )
    {
        return c_oppositeEventIds[iEventId];
    }

    int iSmallestDifference = 9999;

    OppositeEventIdFtor ftor = MakeFunctorRet( &DefaultOppositeEventIdFtor );
    atMap<s32, OppositeEventIdFtor>::Iterator entry = s_oppositeEventIdFtorMap.CreateIterator();
    for ( entry.Start(); !entry.AtEnd(); entry.Next() )
    {
        if ( iEventId >= entry.GetKey() )
        {
            int iDifference = iEventId - entry.GetKey();
            if ( iDifference < iSmallestDifference )
            {
                ftor = entry.GetData();

                iSmallestDifference = iDifference;
            }
        }
    }

    return ftor( iEventId );
}

int cutfEvent::LoadCompare( const cutfEvent *pEvent ) const
{
	if ( GetTime() < pEvent->GetTime() )
	{
		return 1;
	}

	return 0;
}

int cutfEvent::Compare( const cutfEvent *pEvent ) const
{
    if ( GetTime() < pEvent->GetTime() )
    {
        return 1;
    }

	return 0;
}

void cutfEvent::MergeFrom( const cutfEvent &other, bool overwrite )
{
    cutfAssertf( GetType() == other.GetType(), "Can only merge events of the same type." );

    if ( (m_pEventArgsPtr != NULL) && (other.GetEventArgs() != NULL) )
    {
        m_pEventArgsPtr->MergeFrom( *(other.GetEventArgs()), overwrite );
    }
}

bool cutfEvent::operator == ( const cutfEvent &other ) const
{
    return (*this != other) ==  false;
}

bool cutfEvent::operator != ( const cutfEvent &other ) const
{
    if ( this == &other )
    {
        return false;
    }

    if ( (GetEventId() != other.GetEventId())
        || (GetTime() != other.GetTime()) )
    {
        return true;
    }

    if ( (GetEventArgs() != NULL) && (other.GetEventArgs() != NULL) )
    {
        if ( *(GetEventArgs()) != *(other.GetEventArgs()) )
        {
            return true;
        }
    }
    else if ( ((GetEventArgs() == NULL) && (other.GetEventArgs() != NULL))
        || ((GetEventArgs() != NULL) && (other.GetEventArgs() == NULL)) )
    {
        return true;
    }

    return false;
}

void cutfEvent::PreLoad(parTreeNode* node)
{
	parTreeNode* oldArgs = node->FindChildWithName("pEventArgs");
	if (oldArgs)
	{
		oldArgs->GetElement().SetName("iEventArgsIndex");
		int oldIdx = oldArgs->GetElement().FindAttributeIntValue("ref", -1, true);
		oldArgs->GetElement().AddAttribute("value", oldIdx, false);
	}
}

void cutfEvent::ConvertArgIndicesToPointers()
{
	m_pEventArgsPtr = cutfCutsceneFile2::FindEventArgs(m_iEventArgsIndex);
}

void cutfEvent::ConvertArgPointersToIndices()
{
	m_iEventArgsIndex = cutfCutsceneFile2::GetEventArgsIndex(m_pEventArgsPtr);
}

//##############################################################################

cutfObjectIdEvent::cutfObjectIdEvent()
: cutfEvent()
, m_iObjectId( -1 )
{

}

cutfObjectIdEvent::cutfObjectIdEvent( s32 iObjectId, float fTime, s32 iEventId, cutfEventArgs* pEventArgs )
: cutfEvent( fTime, iEventId, pEventArgs )
, m_iObjectId( iObjectId )
{
}

cutfObjectIdEvent::cutfObjectIdEvent( s32 iObjectId, float fTime, s32 iEventId, bool IsChild, u32 StickyId, cutfEventArgs* pEventArgs )
	: cutfEvent( fTime, iEventId, IsChild, StickyId, pEventArgs )
	, m_iObjectId( iObjectId )
{
}


cutfEvent* cutfObjectIdEvent::Clone() const
{
    return rage_new cutfObjectIdEvent( m_iObjectId, m_fTime, m_iEventId, m_IsChild, m_StickyId, m_pEventArgsPtr );
}

bool cutfObjectIdEvent::operator == ( const cutfEvent &other ) const
{
    return (*this != other) ==  false;
}

bool cutfObjectIdEvent::operator != ( const cutfEvent &other ) const
{
    if ( cutfEvent::operator != ( other ) )
    {
        return true;
    }

    const cutfObjectIdEvent *pOtherObjectIdEvent = dynamic_cast<const cutfObjectIdEvent *>( &other );
    if ( GetObjectId() != pOtherObjectIdEvent->GetObjectId() )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfObjectIdListEvent::cutfObjectIdListEvent()
: cutfEvent()
{

}

cutfObjectIdListEvent::cutfObjectIdListEvent( atArray<s32> &iObjectIdList, float fTime, s32 iEventId, bool IsChild, u32 StickyId, cutfEventArgs* pEventArgs )
:cutfEvent( fTime, iEventId, IsChild, StickyId, pEventArgs )
{
    m_iObjectIdList.Assume( iObjectIdList );
}

cutfObjectIdListEvent::cutfObjectIdListEvent( atArray<s32> &iObjectIdList, float fTime, s32 iEventId, atArray<cutfEventArgs*> &eventArgsList )
: cutfEvent( fTime, iEventId, NULL )
{
    m_iObjectIdList.Assume( iObjectIdList );

    m_pEventArgsPtrList.Assume( eventArgsList );
    for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
    {
        m_pEventArgsPtrList[i]->AddRef();
    }
}

cutfObjectIdListEvent::cutfObjectIdListEvent( atArray<s32> &iObjectIdList, float fTime, s32 iEventId, bool IsChild, u32 StickyId, atArray<cutfEventArgs*> &eventArgsList )
	: cutfEvent( fTime, iEventId, IsChild, StickyId)
{
	m_iObjectIdList.Assume( iObjectIdList );

	m_pEventArgsPtrList.Assume( eventArgsList );
	for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
	{
		m_pEventArgsPtrList[i]->AddRef();
	}
}

cutfObjectIdListEvent::~cutfObjectIdListEvent()
{
    for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
    {
        m_pEventArgsPtrList[i]->Release();
    }

    m_pEventArgsPtrList.Reset();
}

void cutfObjectIdListEvent::AddObjectId( s32 iObjectId, cutfEventArgs *pEventArgs )
{
    if ( (m_pEventArgsPtr == NULL) && (pEventArgs != NULL) )
    {
        // keep the two lists in sync
        while ( m_pEventArgsPtrList.GetCount() != m_iObjectIdList.GetCount() )
        {
            m_pEventArgsPtrList.Grow() = NULL;
        }

        m_pEventArgsPtrList.Grow() = pEventArgs;
        pEventArgs->AddRef();
    }

    m_iObjectIdList.Grow() = iObjectId;
}

cutfEventArgs* cutfObjectIdListEvent::RemoveObjectId( s32 iObjectId )
{
    int iIndex = m_iObjectIdList.Find( iObjectId );
    if ( iIndex != -1 )
    {
        m_iObjectIdList.Delete( iIndex );

        if ( iIndex < m_pEventArgsPtrList.GetCount() )
        {
            cutfEventArgs *pEventArgs = m_pEventArgsPtrList[iIndex];
            m_pEventArgsPtrList[iIndex]->Release();
            m_pEventArgsPtrList.Delete( iIndex );

            return pEventArgs;
        }
    }

    return NULL;
}

cutfEvent* cutfObjectIdListEvent::Clone() const
{
    atArray<s32> iObjectIdList;
    iObjectIdList.Reserve( m_iObjectIdList.GetCount() );
    for ( int i = 0; i < m_iObjectIdList.GetCount(); ++i )
    {
        iObjectIdList.Append() = m_iObjectIdList[i];
    }

    if ( m_pEventArgsPtr != NULL )
    {
        return rage_new cutfObjectIdListEvent( iObjectIdList, m_fTime, m_iEventId, m_IsChild, m_StickyId, m_pEventArgsPtr );
    }
    else
    {
        atArray<cutfEventArgs *> pEventArgsList;
        pEventArgsList.Reserve( m_pEventArgsPtrList.GetCount() );
        for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
        {
            pEventArgsList.Append() = m_pEventArgsPtrList[i];
        }

        return rage_new cutfObjectIdListEvent( iObjectIdList, m_fTime, m_iEventId, m_IsChild, m_StickyId, pEventArgsList );
    }
}

void cutfObjectIdListEvent::MergeFrom( const cutfEvent &other, bool overwrite )
{
    cutfAssertf( GetType() == other.GetType(), "Can only merge events of the same type." );

    const cutfObjectIdListEvent *pOtherObjectIdListEvent = dynamic_cast<const cutfObjectIdListEvent *>( &other );
    
    if ( overwrite )
    {
        m_iObjectIdList.Reset();

        SetEventArgs( const_cast<cutfEventArgs *>( other.GetEventArgs() ) );

        for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
        {
            m_pEventArgsPtrList[i]->Release();
        }

        m_pEventArgsPtrList.Reset();

        for ( int i = 0; i < pOtherObjectIdListEvent->m_iObjectIdList.GetCount(); ++i )
        {
            AddObjectId( pOtherObjectIdListEvent->m_iObjectIdList[i], pOtherObjectIdListEvent->m_pEventArgsPtrList[i] );
        }
    }
    else
    {
        for ( int i = 0; i < pOtherObjectIdListEvent->m_iObjectIdList.GetCount(); ++i )
        {
            int indexOf = m_iObjectIdList.Find( pOtherObjectIdListEvent->m_iObjectIdList[i] );
            if ( indexOf == -1 )
            {
                AddObjectId( pOtherObjectIdListEvent->m_iObjectIdList[i], pOtherObjectIdListEvent->m_pEventArgsPtrList[i] );
            }
            else if ( pOtherObjectIdListEvent->m_pEventArgsPtrList[i] != NULL )
            {
                if ( m_pEventArgsPtrList[indexOf] == NULL )
                {
                    pOtherObjectIdListEvent->m_pEventArgsPtrList[i]->AddRef();
                    m_pEventArgsPtrList[indexOf] = pOtherObjectIdListEvent->m_pEventArgsPtrList[i];
                }
                else
                {
                    m_pEventArgsPtrList[indexOf]->MergeFrom( *(pOtherObjectIdListEvent->m_pEventArgsPtrList[i]), overwrite );
                }
            }
        }
    }
}

bool cutfObjectIdListEvent::operator == ( const cutfEvent &other ) const
{
    return (*this != other) ==  false;
}

bool cutfObjectIdListEvent::operator != ( const cutfEvent &other ) const
{
    if ( cutfEvent::operator != ( other ) )
    {
        return true;
    }

    const cutfObjectIdListEvent *pOtherObjectIdListEvent = dynamic_cast<const cutfObjectIdListEvent *>( &other );

    const atArray<s32> &otherObjectIdList = pOtherObjectIdListEvent->GetObjectIdList();
    if ( m_iObjectIdList.GetCount() != otherObjectIdList.GetCount() )
    {
        return true;
    }

    for ( int i = 0; i < m_iObjectIdList.GetCount(); ++i )
    {
        int iObjectId = GetObjectIdList()[i];
        int iOtherObjectId = -1;
        for ( int j = 0; j < otherObjectIdList.GetCount(); ++j )
        {
            if ( otherObjectIdList[j] == iObjectId )
            {
                iOtherObjectId = otherObjectIdList[j];
                break;
            }
        }

        if ( iOtherObjectId == -1 )
        {
            return true;
        }
    }

    const atArray<cutfEventArgs *> &otherEventArgsList = pOtherObjectIdListEvent->GetEventArgsList();
    if ( m_pEventArgsPtrList.GetCount() != otherEventArgsList.GetCount() )
    {
        return true;
    }

    for ( int i = 0; i < m_pEventArgsPtrList.GetCount(); ++i )
    {
        const cutfEventArgs *pEventArgs = m_pEventArgsPtrList[i];
        const cutfEventArgs *pOtherEventArgs = NULL;
        for ( int j = 0; j < otherEventArgsList.GetCount(); ++j )
        {
            if ( (otherEventArgsList[j] == pEventArgs) || (*(otherEventArgsList[j]) == *pEventArgs) )
            {
                pOtherEventArgs = otherEventArgsList[j];
                break;
            }
        }

        if ( pOtherEventArgs == NULL )
        {
            return true;
        }
    }

    return false;
}

void cutfObjectIdListEvent::PreLoad(parTreeNode* node)
{
	parTreeNode* oldArgs = node->FindChildWithName("pEventArgsList");
	if (oldArgs)
	{
		oldArgs->GetElement().SetName("iEventArgsIndexList", false);
		for(parTreeNode::ChildNodeIterator kid = oldArgs->BeginChildren(); kid != oldArgs->EndChildren(); ++oldArgs)
		{
			parTreeNode* kidNode = (*kid);
			int oldIdx = kidNode->GetElement().FindAttributeIntValue("ref", -1, true);
			kidNode->GetElement().AddAttribute("value", oldIdx, false);
		}
	}
}

void cutfObjectIdListEvent::ConvertArgIndicesToPointers()
{
	m_pEventArgsPtrList.ResizeGrow(m_iEventArgsIndexList.GetCount());
	for(int i = 0; i < m_iEventArgsIndexList.GetCount(); i++)
	{
		m_pEventArgsPtrList[i] = cutfCutsceneFile2::FindEventArgs(m_iEventArgsIndexList[i]);
	}
}

void cutfObjectIdListEvent::ConvertArgPointersToIndices()
{
	m_iEventArgsIndexList.ResizeGrow(m_pEventArgsPtrList.GetCount());
	for(int i = 0; i < m_pEventArgsPtrList.GetCount(); i++)
	{
		m_iEventArgsIndexList[i] = cutfCutsceneFile2::GetEventArgsIndex(m_pEventArgsPtrList[i]);
	}
}

//##############################################################################

} // namespace rage
