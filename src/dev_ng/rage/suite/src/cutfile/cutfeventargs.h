// 
// cutfile/cutfeventargs.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTFILE_CUTFEVENTARGS_H 
#define CUTFILE_CUTFEVENTARGS_H 

#include "cutfobject.h"

#include "atl/functor.h"
#include "atl/map.h"
#include "parser/macros.h"
#include "parsercore/attribute.h"
#include "vector/color32.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

namespace rage {

class cutfAttributeList;

enum ECutsceneEventArgsType
{
    CUTSCENE_GENERIC_EVENT_ARGS_TYPE,
    CUTSCENE_EVENT_ARGS_LIST_TYPE,
    CUTSCENE_NAME_EVENT_ARGS_TYPE,
    CUTSCENE_FLOAT_VALUE_EVENT_ARGS_TYPE,
    CUTSCENE_TWO_FLOAT_VALUES_EVENT_ARGS_TYPE,
    CUTSCENE_SCREEN_FADE_EVENT_ARGS_TYPE,
    CUTSCENE_LOAD_SCENE_EVENT_ARGS_TYPE,
    CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE,
    CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE,
    CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE,
    CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE,
    CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT_ARGS_TYPE,
    CUTSCENE_SUBTITLE_EVENT_ARGS_TYPE,
    CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE,
	CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE,
	CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE, 
	CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE,
	CUTSCENE_DECAL_EVENT_ARGS, 
	CUTSCENE_CASCADE_SHADOW_EVENT_ARGS, 
	CUTSCENE_BOOL_VALUE_EVENT_ARGS_TYPE, 
	CUTSCENE_FINAL_NAME_EVENT_ARGS_TYPE, 
	CUTSCENE_FLOAT_BOOL_EVENT_ARGS_TYPE,
	CUTSCENE_TRIGGER_LIGHT_EVENT_ARGS_TYPE, 
	CUTSCENE_OBJECT_ID_PARTIAL_HASH_EVENT_ARGS_TYPE,
    CUTSCENE_NUM_EVENT_ARGS_TYPES,

    // highest rage cutfile "built in" event args
    CUTSCENE_EVENT_ARGS_TYPE_LAST_RESERVED_FOR_CUTFILE_USE_ONLY = 0x7f,

    // first rage cutscene "built in" event args
    CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY = 0x80,  // The events defined in cutscene/cutseventargs.h will start with this enum.

    // highest rage cutscene "built in" event args
    CUTSCENE_EVENT_ARGS_TYPE_LAST_RESERVED_FOR_RUNTIME_USE_ONLY = 0xff,

    // first project specific event args
    CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE = 0x100
};

//##############################################################################

inline const char* DefaultEventArgsTypeNameFtor( s32 /*iEventArgsType*/ )
{
    return "(unknown)";
}

//##############################################################################

// PURPOSE: Base class for cut scene event args that are dispatched to cut 
// scene entities.
class cutfEventArgs
{
public:
    typedef Functor1Ret<const char*, s32> EventArgsTypeNameFtor;

    cutfEventArgs();
    cutfEventArgs( const parAttributeList& attributes );
    virtual ~cutfEventArgs();

    // PURPOSE: Initializes the static members of this class.
    static void InitClass();

    // PURPOSE: Shuts down the static members of this class.
    static void ShutdownClass();

    // PURPOSE: This function will set the EventArgsTypeNameFtor which is required if
    //    a project has defined its own event args.  The functor takes in the event args type
    //    a returns its display name.
    // PARAMS:
    //    ftor - the functor
    //    iFirstEventArgsType - the first event args type that should call this functor
    // NOTES: The functor will only be called for event args types that are greater
    //    than or equal to CUTSCENE_EVENT_ARGS_FIRST_RESERVED_FOR_PROJECT_USE.
    static void SetEventArgsTypeNameFunc( EventArgsTypeNameFtor ftor, s32 iFirstEventArgsType=CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE );

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the name of this data type.
    // RETURNS: The type name.
    const char* GetTypeName() const;

    // PURPOSE: Retrieves the current reference counter value.
    // RETURNS: The current number of references
    int GetRef() const;

    // PURPOSE: Increment reference counter.
    void AddRef() const;

    // PURPOSE: Decrements the reference counter.
    // RETURNS: Returns the new reference count.  This instance will NOT be deleted as it 
    //    will be owned by the cufCutsceneFile2 object which will handle the deletion.
    int Release() const;

    // PURPOSE: Retrieves the parAttributList so key-value pairs can be 
    //    added and accessed.
    // RETURNS: The attribute list.
    const parAttributeList& GetAttributeList() const;

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    // PURPOSE: Retrieves the name of the event args.
    // PARAMS:
    //    iEventArgsType - the event args type to get the display name of
    // RETURNS: The display name of the given event args type.
    // NOTES: This will be used when editing the events in various tools.
    static const char* GetTypeName( s32 iEventArgsType );

    // PURPOSE: Brings all the attributes in the 'other' list into this event args
    // PARAMS:
    //		other - The event args to merge into this one
    //		overwrite - If true, attributes in 'other' will overwrite ones that are in this event args.
    virtual void MergeFrom( const cutfEventArgs &other, bool overwrite=true );

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

	void ConvertAttributesForSave();
	void ConvertAttributesAfterLoad();

    PAR_PARSABLE;

protected:
    mutable u16 m_iRefCount;
    parAttributeList m_attributeList;
	cutfAttributeList* m_cutfAttributes;

private:
    static atMap<s32, EventArgsTypeNameFtor> s_eventArgsTypeNameFtorMap;

    static const char* c_cutsceneEventArgsTypeNames[]; // for the static GetTypeName function
};

inline int cutfEventArgs::GetRef() const
{ 
    return m_iRefCount; 
}

inline void cutfEventArgs::AddRef() const
{ 
    ++m_iRefCount; 
}

inline int cutfEventArgs::Release() const
{ 
    if ( m_iRefCount > 0 )
    {
        --m_iRefCount;
    }

    return m_iRefCount; 
}

inline s32 cutfEventArgs::GetType() const
{
    return CUTSCENE_GENERIC_EVENT_ARGS_TYPE;
}

inline const char* cutfEventArgs::GetTypeName() const
{
    return GetTypeName( GetType() );
}

inline const parAttributeList& cutfEventArgs::GetAttributeList() const
{
    return m_attributeList;
}

//##############################################################################

// PURPOSE: An Event args class that is a list of event args.  Typically, this 
//    is paired with cutfObjectIdListEvent (but not always, so be careful).
class cutfEventArgsList : public cutfEventArgs
{
public:
    cutfEventArgsList();
    cutfEventArgsList( atArray<cutfEventArgs *> &pEventArgsList );
    cutfEventArgsList( atArray<cutfEventArgs *> &pEventArgsList, const parAttributeList& attributes  );
    virtual ~cutfEventArgsList();

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the list of event args
    // RETURNS: The list of event args
    const atArray<cutfEventArgs *>& GetEventArgsList() const;

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    // PURPOSE: Brings all the attributes in the 'other' list into this event args
    // PARAMS:
    //		other - The event args to merge into this one
    //		overwrite - If true, attributes in 'other' will overwrite ones that are in this event args.
    virtual void MergeFrom( const cutfEventArgs &other, bool overwrite=true );

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

	void PreLoad(parTreeNode* node);
	void ConvertArgPointersToIndices();
	void ConvertArgIndicesToPointers();

    PAR_PARSABLE;

protected:
	atArray<int> m_iEventArgsIndexList; /* for serialization, don't use at runtime */
    atArray<cutfEventArgs *> m_pEventArgsPtrList;
};

inline s32 cutfEventArgsList::GetType() const
{
    return CUTSCENE_EVENT_ARGS_LIST_TYPE;
}

inline const atArray<cutfEventArgs *>& cutfEventArgsList::GetEventArgsList() const
{
    return m_pEventArgsPtrList;
}

//##############################################################################

// PURPOSE: Event args for sending a string.  Used by CUTSCENE_LOAD_ANIM_DICT_EVENT,
//    CUTSCENE_LOAD_AUDIO_EVENT, CUTSCENE_UNLOAD_AUDIO_EVENT, CUTSCENE_PLAY_AUDIO_EVENT,
//    and CUTSCENE_STOP_AUDIO_EVENT.
class cutfNameEventArgs : public cutfEventArgs
{
public:
    cutfNameEventArgs();
    cutfNameEventArgs( const char* pName );
	cutfNameEventArgs( atHashString Name );
    cutfNameEventArgs( const char* pName, const parAttributeList& attributes );
	cutfNameEventArgs( atHashString Name, const parAttributeList& attributes );

    virtual ~cutfNameEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Gets the name
    // RETURNS: The name
    atHashString GetName() const;

    // PURPOSE: Sets the name
    // PARAMS:
    //    pName - the name
    void SetName( const char* pName );
	
	// PURPOSE: Sets the name
	// PARAMS:
	//    pName - the name
	void SetName( atHashString pName );

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    atHashString m_cName;
};

inline s32 cutfNameEventArgs::GetType() const
{
    return CUTSCENE_NAME_EVENT_ARGS_TYPE;
}

inline atHashString cutfNameEventArgs::GetName() const
{
    return m_cName;
}

inline void cutfNameEventArgs::SetName( const char* pName )
{
    if ( pName != NULL )
    {
        m_cName = pName; 
    }
    else
    {
        m_cName.Clear();
    }
}

inline void cutfNameEventArgs::SetName( atHashString Name )
{
	if ( Name.GetHash() != 0 )
	{
		m_cName = Name; 
	}
	else
	{
		m_cName.Clear();
	}
}


//##############################################################################

// PURPOSE: For sending a string which you need in final builds
class cutfFinalNameEventArgs : public cutfEventArgs
{
public:
	cutfFinalNameEventArgs();
	cutfFinalNameEventArgs( const char* pName );
	cutfFinalNameEventArgs( atString pName );
	cutfFinalNameEventArgs( const char* pName, const parAttributeList& attributes );
	cutfFinalNameEventArgs( atString Name, const parAttributeList& attributes );
	virtual ~cutfFinalNameEventArgs() {}

	// PURPOSE: Retrieves the type of this event args for easy casting
	// RETURNS: An id representing the type
	virtual s32 GetType() const;

	// PURPOSE: Gets the name
	// RETURNS: The name
	const char* GetName() const;

	// PURPOSE: Sets the name
	// PARAMS:
	//    pName - the name
	void SetName( const char* pName );

	// PURPOSE: Creates a copy of this cut scene event args
	// RETURNS: A clone of this instance
	virtual cutfEventArgs* Clone() const;

	virtual bool operator == ( const cutfEventArgs &other ) const;
	virtual bool operator != ( const cutfEventArgs &other ) const;

	PAR_PARSABLE;

protected:
	atString m_cName;
};

inline s32 cutfFinalNameEventArgs::GetType() const
{
	return CUTSCENE_FINAL_NAME_EVENT_ARGS_TYPE;
}

inline const char* cutfFinalNameEventArgs::GetName() const
{
	return m_cName.c_str();
}

inline void cutfFinalNameEventArgs::SetName( const char* pName )
{
	if ( pName != NULL )
	{
		m_cName = pName; 
	
	}
	else
	{
		m_cName.Reset(); 
	}
}


//##############################################################################

// PURPOSE: Event args for sending a Bool value.  Used by CUTSCENE_FADE_IN_EVENT.
class cutfBoolValueEventArgs : public cutfEventArgs
{
public:
	cutfBoolValueEventArgs();
	cutfBoolValueEventArgs( bool bValue );
	cutfBoolValueEventArgs( bool bValue, const parAttributeList& attributes );
	virtual ~cutfBoolValueEventArgs() {}

	// PURPOSE: Retrieves the type of this event args for easy casting
	// RETURNS: An id representing the type
	virtual s32 GetType() const;

	// PURPOSE: Gets the bool value
	// RETURNS: The Bool value
	// NOTES:
	bool GetValue() const;

	// PURPOSE: Sets the bool value
	// PARAMS:
	//    fValue - the bool value
	void SetValue( bool bValue );

	// PURPOSE: Creates a copy of this cut scene event args
	// RETURNS: A clone of this instance
	virtual cutfEventArgs* Clone() const;

	virtual bool operator == ( const cutfEventArgs &other ) const;
	virtual bool operator != ( const cutfEventArgs &other ) const;

	PAR_PARSABLE;

protected:
	bool m_bValue;
};

inline s32 cutfBoolValueEventArgs::GetType() const
{
	return CUTSCENE_BOOL_VALUE_EVENT_ARGS_TYPE;
}

inline bool cutfBoolValueEventArgs::GetValue() const
{
	return m_bValue;
}

inline void cutfBoolValueEventArgs::SetValue( bool bValue )
{
	m_bValue = bValue;
}

//##############################################################################

// PURPOSE: Event args for sending a float value.  Used by CUTSCENE_FADE_IN_EVENT.
class cutfFloatBoolValueEventArgs : public cutfBoolValueEventArgs
{
public:
	cutfFloatBoolValueEventArgs();
	cutfFloatBoolValueEventArgs( float fValue , bool bValue);
	cutfFloatBoolValueEventArgs( float fValue, bool bValue, const parAttributeList& attributes );
	virtual ~cutfFloatBoolValueEventArgs() {}

	// PURPOSE: Retrieves the type of this event args for easy casting
	// RETURNS: An id representing the type
	virtual s32 GetType() const;

	// PURPOSE: Gets the float value
	// RETURNS: The float value
	// NOTES:
	float GetFloat1() const;

	// PURPOSE: Sets the float value
	// PARAMS:
	//    fValue - the float value
	void SetFloat1( float fValue );

	// PURPOSE: Creates a copy of this cut scene event args
	// RETURNS: A clone of this instance
	virtual cutfEventArgs* Clone() const;

	virtual bool operator == ( const cutfEventArgs &other ) const;
	virtual bool operator != ( const cutfEventArgs &other ) const;

	PAR_PARSABLE;

protected:
	float m_fValue;
};

inline s32 cutfFloatBoolValueEventArgs::GetType() const
{
	return CUTSCENE_FLOAT_BOOL_EVENT_ARGS_TYPE;
}

inline float cutfFloatBoolValueEventArgs::GetFloat1() const
{
	return m_fValue;
}

inline void cutfFloatBoolValueEventArgs::SetFloat1( float fValue )
{
	m_fValue = fValue;
}

//##############################################################################

// PURPOSE: Event args for sending a float value.  Used by CUTSCENE_FADE_IN_EVENT.
class cutfFloatValueEventArgs : public cutfEventArgs
{
public:
    cutfFloatValueEventArgs();
    cutfFloatValueEventArgs( float fValue );
    cutfFloatValueEventArgs( float fValue, const parAttributeList& attributes );
    virtual ~cutfFloatValueEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Gets the float value
    // RETURNS: The float value
    // NOTES:
    float GetFloat1() const;

    // PURPOSE: Sets the float value
    // PARAMS:
    //    fValue - the float value
    void SetFloat1( float fValue );

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    float m_fValue;
};

inline s32 cutfFloatValueEventArgs::GetType() const
{
    return CUTSCENE_FLOAT_VALUE_EVENT_ARGS_TYPE;
}

inline float cutfFloatValueEventArgs::GetFloat1() const
{
    return m_fValue;
}

inline void cutfFloatValueEventArgs::SetFloat1( float fValue )
{
    m_fValue = fValue;
}

//##############################################################################

// PURPOSE: Event args for sending 2 float values.  Used by 
//    CUTSCENE_SET_DRAW_DISTANCE_EVENT.
class cutfTwoFloatValuesEventArgs : public cutfFloatValueEventArgs
{
public:
    cutfTwoFloatValuesEventArgs();
    cutfTwoFloatValuesEventArgs( float fValue1, float fValue2 );
    cutfTwoFloatValuesEventArgs( float fValue1, float fValue2, const parAttributeList& attributes );
    virtual ~cutfTwoFloatValuesEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Gets the second float value
    // RETURNS: The float value
    // NOTES:
    float GetFloat2() const;

    // PURPOSE: Sets the second float value
    // PARAMS:
    //    fValue - the float value
    void SetFloat2( float fValue );

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    float m_fValue2;
};

inline s32 cutfTwoFloatValuesEventArgs::GetType() const
{
    return CUTSCENE_TWO_FLOAT_VALUES_EVENT_ARGS_TYPE;
}

inline float cutfTwoFloatValuesEventArgs::GetFloat2() const
{
    return m_fValue2;
}

inline void cutfTwoFloatValuesEventArgs::SetFloat2( float fValue )
{
    m_fValue2 = fValue;
}

//##############################################################################

// PURPOSE: Event args for CUTSCENE_FADE_OUT_EVENT.
class cutfScreenFadeEventArgs : public cutfFloatValueEventArgs
{
public:
    cutfScreenFadeEventArgs();
    cutfScreenFadeEventArgs( float fDuration, const Color32 &color );
    cutfScreenFadeEventArgs( float fDuration, const Color32 &color, const parAttributeList& attributes );
    virtual ~cutfScreenFadeEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Gets the color that the screen should fade to
    // RETURNS: The color
    const Color32& GetColor() const;

    // PURPOSE: Sets the color that the screen should fade to
    // PARAMS:
    //    vColor - the color
    void SetColor( const Color32 &color );

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    Color32 m_color;
};

inline s32 cutfScreenFadeEventArgs::GetType() const
{
    return CUTSCENE_SCREEN_FADE_EVENT_ARGS_TYPE;
}

inline const Color32& cutfScreenFadeEventArgs::GetColor() const
{
    return m_color;
}

inline void cutfScreenFadeEventArgs::SetColor( const Color32 &color )
{
    m_color = color;
}

//##############################################################################

// PURPOSE: Event args for CUTSCENE_LOAD_SCENE_EVENT.
class cutfLoadSceneEventArgs : public cutfNameEventArgs
{
public:
    cutfLoadSceneEventArgs();
    cutfLoadSceneEventArgs( const char* pCutsceneName, const Vector3& offset, float fRotation, float fPitch, float fRoll);
    cutfLoadSceneEventArgs( const char* pCutsceneName, const Vector3& offset, float fRotation, float fPitch, float fRoll,
		const parAttributeList& attributes );
	cutfLoadSceneEventArgs( atHashString pCutsceneName, const Vector3& offset, float fRotation, float fPitch, float fRoll,
		const parAttributeList& attributes );
    virtual ~cutfLoadSceneEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Gets the offset for the scene in the game world
    // RETURNS: The offset
    const Vector3& GetOffset() const;

    // PURPOSE: Sets the offset for the scene in the game world
    // PARAMS:
    //    vOffset - the offset
    void SetOffset( const Vector3 &vOffset );

    // PURPOSE: Gets the rotation for the scene in the game world
    // RETURNS: The rotation
    float GetRotation() const;

    // PURPOSE: Sets the rotation for the scene in the game world
    // PARAMS:
    //    vRotation - the rotation
    void SetRotation( float fRotation );

	// PURPOSE: Gets the rotation for the scene in the game world
	// RETURNS: The rotation
	float GetRoll() const;

	// PURPOSE: Sets the rotation for the scene in the game world
	// PARAMS:
	//    vRotation - the rotation
	void SetRoll( float fRoll );

	// PURPOSE: Gets the rotation for the scene in the game world
	// RETURNS: The rotation
	float GetPitch() const;

	// PURPOSE: Sets the rotation for the scene in the game world
	// PARAMS:
	//    vRotation - the rotation
	void SetPitch( float fPitch );

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    Vector3 m_vOffset;
    float m_fRotation;
	float m_fPitch; 
	float m_fRoll; 
};

inline s32 cutfLoadSceneEventArgs::GetType() const
{
    return CUTSCENE_LOAD_SCENE_EVENT_ARGS_TYPE;
}

inline const Vector3& cutfLoadSceneEventArgs::GetOffset() const
{
    return m_vOffset;
}

inline void cutfLoadSceneEventArgs::SetOffset( const Vector3 &vOffset )
{
    m_vOffset = vOffset;
}

inline float cutfLoadSceneEventArgs::GetRotation() const
{
    return m_fRotation;
}

inline void cutfLoadSceneEventArgs::SetRotation( float fRotation )
{
    m_fRotation = fRotation;
}

inline float cutfLoadSceneEventArgs::GetRoll() const
{
	return m_fRoll;
}

inline void cutfLoadSceneEventArgs::SetRoll( float fRoll )
{
	m_fRoll = fRoll;
}

inline float cutfLoadSceneEventArgs::GetPitch() const
{
	return m_fPitch;
}

inline void cutfLoadSceneEventArgs::SetPitch( float fPitch )
{
	m_fPitch = fPitch;
}

//##############################################################################

// PURPOSE: Event args for sending an object id.
class cutfObjectIdEventArgs : public cutfEventArgs
{
public:
    cutfObjectIdEventArgs();
    cutfObjectIdEventArgs( s32 iObjectId );
    cutfObjectIdEventArgs( s32 iObjectId, const parAttributeList& attributes );
    virtual ~cutfObjectIdEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Gets the object id
    // RETURNS: The object id
    s32 GetObjectId() const;

    // PURPOSE: Sets the object id
    // PARAMS:
    //    iObjectId - The object id
    void SetObjectId( s32 iObjectId );

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    s32 m_iObjectId;
};

inline s32 cutfObjectIdEventArgs::GetType() const
{
    return CUTSCENE_OBJECT_ID_EVENT_ARGS_TYPE;
}

inline s32 cutfObjectIdEventArgs::GetObjectId() const
{
    return m_iObjectId;
}

inline void cutfObjectIdEventArgs::SetObjectId( s32 iObjectId )
{
    m_iObjectId = iObjectId;
}

//##############################################################################

// PURPOSE: Event args for sending an object id and a string.  Used by 
class cutfObjectIdNameEventArgs : public cutfObjectIdEventArgs
{
public:
    cutfObjectIdNameEventArgs();
    cutfObjectIdNameEventArgs( s32 iObjectId, const char *pName );
    cutfObjectIdNameEventArgs( s32 iObjectId, const char *pName, const parAttributeList& attributes );
	cutfObjectIdNameEventArgs( s32 iObjectId, atHashString Name, const parAttributeList& attributes );
    virtual ~cutfObjectIdNameEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Gets the name
    // RETURNS: The name
    atHashString GetName() const;

    // PURPOSE: Sets the name
    // PARAMS:
    //    pName - the name
    void SetName( const char* pName );

	// PURPOSE: Sets the name
	// PARAMS:
	//    pName - the name
	void SetName( atHashString Name );

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    atHashString m_cName;
};

inline s32 cutfObjectIdNameEventArgs::GetType() const
{
    return CUTSCENE_OBJECT_ID_NAME_EVENT_ARGS_TYPE;
}

inline atHashString cutfObjectIdNameEventArgs::GetName() const
{
    return m_cName;
}

inline void cutfObjectIdNameEventArgs::SetName( const char* pName )
{
    if ( pName != NULL )
    {
        m_cName = pName; 
    }
    else
    {
        m_cName.Clear(); 
    }
}

inline void cutfObjectIdNameEventArgs::SetName( atHashString Name )
{
	if ( Name.GetHash() != 0 )
	{
		m_cName = Name; 
	}
	else
	{
		m_cName.Clear(); 
	}
}


//##############################################################################

// PURPOSE: Event args for sending an object id and a partial hash.  Used by 
class cutfObjectIdPartialHashEventArgs : public cutfObjectIdEventArgs
{
public:
	cutfObjectIdPartialHashEventArgs();
	cutfObjectIdPartialHashEventArgs( s32 iObjectId, u32 PartialHash);
	cutfObjectIdPartialHashEventArgs( s32 iObjectId, u32 PartialHash, const parAttributeList& attributes );
	virtual ~cutfObjectIdPartialHashEventArgs() {}

	// PURPOSE: Retrieves the type of this event args for easy casting
	// RETURNS: An id representing the type
	virtual s32 GetType() const;

	// PURPOSE: Gets the name
	// RETURNS: The name
	u32 GetPartialHash() const;

	// PURPOSE: Sets the name
	// PARAMS:
	//    pName - the name
	void SetPartialHash( u32 partialHash );

	// PURPOSE: Creates a copy of this cut scene event args
	// RETURNS: A clone of this instance
	virtual cutfEventArgs* Clone() const;

	virtual bool operator == ( const cutfEventArgs &other ) const;
	virtual bool operator != ( const cutfEventArgs &other ) const;

	PAR_PARSABLE;

protected:
	u32 m_PartialHash; 
};

inline s32 cutfObjectIdPartialHashEventArgs::GetType() const
{
	return CUTSCENE_OBJECT_ID_PARTIAL_HASH_EVENT_ARGS_TYPE;
}

inline u32 cutfObjectIdPartialHashEventArgs::GetPartialHash() const
{
	return m_PartialHash;
}

inline void cutfObjectIdPartialHashEventArgs::SetPartialHash( u32 partialHash )
{
	m_PartialHash = partialHash; 
}


//##############################################################################

// PURPOSE: Event args for CUTSCENE_SET_ATTACHMENT_EVENT.
class cutfAttachmentEventArgs : public cutfObjectIdEventArgs
{
public:
    cutfAttachmentEventArgs();
    cutfAttachmentEventArgs( s32 iObjectId, const char* pBoneName );
    cutfAttachmentEventArgs( s32 iObjectId, const char* pBoneName, const parAttributeList& attributes );
	cutfAttachmentEventArgs( s32 iObjectId, atHashString BoneName, const parAttributeList& attributes );
    virtual ~cutfAttachmentEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Gets the name of the bone to attach to
    // RETURNS: The bone name
    atHashString GetBoneName() const;

    // PURPOSE: Gets the name of the bone to attach to
    // PARAMS:
    //    pBoneName - The bone name
    void SetBoneName( const char* pBoneName );

	// PURPOSE: Gets the name of the bone to attach to
	// PARAMS:
	//    pBoneName - The bone name
	void SetBoneName( atHashString BoneName );

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    atHashString m_cBoneName;
};

inline s32 cutfAttachmentEventArgs::GetType() const
{
    return CUTSCENE_ATTACHMENT_EVENT_ARGS_TYPE;
}

inline atHashString cutfAttachmentEventArgs::GetBoneName() const
{
    return m_cBoneName;
}

inline void cutfAttachmentEventArgs::SetBoneName( const char* pBoneName )
{
    if ( pBoneName != NULL )
    {
        m_cBoneName = pBoneName; 
    }
    else
    {
        m_cBoneName.Clear(); 
    }
}

inline void cutfAttachmentEventArgs::SetBoneName( atHashString BoneName )
{
	if ( BoneName.GetHash() != 0 )
	{
		m_cBoneName = BoneName; 
	}
	else
	{
		m_cBoneName.Clear(); 
	}
}


//##############################################################################

// PURPOSE: Event args for sending a list of object ids.  Used by
//    CUTSCENE_LOAD_MODELS_EVENT, CUTSCENE_UNLOAD_MODELS_EVENT,
//    CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT, CUTSCENE_UNLOAD_PARTICLE_EFFECTS_EVENT,
//    CUTSCENE_LOAD_OVERLAYS_EVENT, CUTSCENE_UNLOAD_OVERLAYS_EVENT,
//    CUTSCENE_LOAD_SUBTITLES_EVENT, CUTSCENE_UNLOAD_SUBTITLES_EVENT,
//    CUTSCENE_HIDE_OBJECTS_EVENT, CUTSCENE_SHOW_OBJECTS_EVENT,
//    CUTSCENE_FIXUP_OBJECTS_EVENT, CUTSCENE_REVERT_FIXUP_OBJECTS_EVENT,
//    CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT, and CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT.
//    CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT, and CUTSCENE_REMOVE_REMOVAL_BOUNDS_EVENT.
class cutfObjectIdListEventArgs : public cutfEventArgs
{
public:
    cutfObjectIdListEventArgs();
    cutfObjectIdListEventArgs( atArray<s32> &iObjectIdList );
    cutfObjectIdListEventArgs( atArray<s32> &iObjectIdList, const parAttributeList& attributes );
    virtual ~cutfObjectIdListEventArgs();

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the list of object ids
    // RETURNS: The object ids
    const atArray<s32>& GetObjectIdList() const;

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    // PURPOSE: Brings all the attributes in the 'other' list into this event args
    // PARAMS:
    //		other - The event args to merge into this one
    //		overwrite - If true, attributes in 'other' will overwrite ones that are in this event args.
    virtual void MergeFrom( const cutfEventArgs &other, bool overwrite=true );

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    atArray<s32> m_iObjectIdList;
};

inline s32 cutfObjectIdListEventArgs::GetType() const
{
    return CUTSCENE_OBJECT_ID_LIST_EVENT_ARGS_TYPE;
}

inline const atArray<s32>& cutfObjectIdListEventArgs::GetObjectIdList() const
{
    return m_iObjectIdList;
}

//##############################################################################

struct SEvolutionParticleEffectParam
{
    SEvolutionParticleEffectParam();

    char cName[MAX_EVOLUTION_EFFECT_NAME_LEN];
    float fValue;

    PAR_SIMPLE_PARSABLE;
};

// PURPOSE: Event args for CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT.
class cutfPlayParticleEffectEventArgs : public cutfEventArgs
{
public:
    cutfPlayParticleEffectEventArgs();
    cutfPlayParticleEffectEventArgs(s32 AttachParentId, u16 BoneHash, const Vector3& vOffset, const Vector4& vRotation);
    cutfPlayParticleEffectEventArgs(s32 AttachParentId, u16 BoneHash, const Vector3& vOffset, const Vector4& vRotation, const parAttributeList& attributes);
    virtual ~cutfPlayParticleEffectEventArgs();

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;


    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;
	
	// PURPOSE: Sets the model id we are attached to
	// RETURNS: The model id
	s32 GetAttachParentId() const;

	// PURPOSE: Sets the model id we are attached to
	// PARAMS:
	//    iAttachModelId - the model id
	void SetAttachParentId( s32 iAttachModelId );

	// PURPOSE: Gets the hash value of the bone attachment
	// RETURNS: The nashed bone name
	u16 GetAttachBoneHash() const;

	// PURPOSE: Set Attach Parent Id
	// PARAMS: Bone tag hash
	void SetAttachBoneHash( u16 iHash );
	
	// PURPOSE: Sets the bone offset is bone space. 
	// PARAMS:Local offset in bone space 
	void SetInitialOffset(const Vector3& BoneOffset); 

	// PURPOSE: Gets the bone offset is bone space. 
	// RETURNS: Bone initial offset
	Vector3 GetInitialOffset() const;

	// PURPOSE: Sets the bone rotation is bone space. 
	// PARAMS:Local rotation in bone space 
	void SetInitialRotation(const Vector4& BoneRotation); 

	// PURPOSE: Gets the bone rotation is bone space. 
	// RETURNS: Bone initial rotation
	Vector4 GetInitialRotation() const;
	

    PAR_PARSABLE;

protected:
	Vector4 m_vInitialBoneRotation; 
	Vector3 m_vInitialBoneOffset; 

	s32 m_iAttachParentId; 
	u16 m_iAttachBoneHash;

};

inline s32 cutfPlayParticleEffectEventArgs::GetType() const
{
    return CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT_ARGS_TYPE;
}

inline s32 cutfPlayParticleEffectEventArgs::GetAttachParentId() const
{
    return m_iAttachParentId;
}

inline void cutfPlayParticleEffectEventArgs::SetAttachParentId( s32 iAttachModelId )
{
    m_iAttachParentId = iAttachModelId;
}

inline u16 cutfPlayParticleEffectEventArgs::GetAttachBoneHash() const
{
    return m_iAttachBoneHash;
}

inline void cutfPlayParticleEffectEventArgs::SetAttachBoneHash( u16 iHash )
{
    m_iAttachBoneHash = iHash;
}


inline void cutfPlayParticleEffectEventArgs::SetInitialOffset(const Vector3& BoneOffset)
{
	m_vInitialBoneOffset = BoneOffset; 
}

inline Vector3 cutfPlayParticleEffectEventArgs::GetInitialOffset() const
{
	return m_vInitialBoneOffset; 
}

inline void cutfPlayParticleEffectEventArgs::SetInitialRotation(const Vector4& BoneOffset)
{
	m_vInitialBoneRotation = BoneOffset; 
}

inline Vector4 cutfPlayParticleEffectEventArgs::GetInitialRotation() const
{
	return m_vInitialBoneRotation; 
}

//##############################################################################

// PURPOSE: Event args for CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT.
class cutfTriggerLightEffectEventArgs : public cutfEventArgs
{
public:
	cutfTriggerLightEffectEventArgs();
	cutfTriggerLightEffectEventArgs(s32 AttachParentId, u16 BoneHash, atHashString AttachParentName);
	cutfTriggerLightEffectEventArgs(s32 AttachParentId, u16 BoneHash, atHashString AttachParentName, const parAttributeList& attributes);
	virtual ~cutfTriggerLightEffectEventArgs();

	// PURPOSE: Retrieves the type of this event args for easy casting
	// RETURNS: An id representing the type
	virtual s32 GetType() const;


	// PURPOSE: Creates a copy of this cut scene event args
	// RETURNS: A clone of this instance
	virtual cutfEventArgs* Clone() const;

	virtual bool operator == ( const cutfEventArgs &other ) const;
	virtual bool operator != ( const cutfEventArgs &other ) const;

	// PURPOSE: Sets the model id we are attached to
	// RETURNS: The model id
	s32 GetAttachParentId() const;

	// PURPOSE: Sets the model id we are attached to
	// PARAMS:
	//    iAttachModelId - the model id
	void SetAttachParentId( s32 iAttachModelId );

	// PURPOSE: Sets the model name we are attached to
	// PARAMS:
	//    pName - the model name
	void SetAttachParentName( const char* pName );

#if !__FINAL
	// PURPOSE: Gets the model name we are attached to
	// RETURNS: The model name
	const char* GetAttachParentName() const;
#endif
	
	atHashString GetAttachParentHashString() const;

	// PURPOSE: Gets the hash value of the bone attachment
	// RETURNS: The hashed bone name
	u16 GetAttachBoneHash() const;

	// PURPOSE: Set Attach Parent Id
	// PARAMS: Bone tag hash
	void SetAttachBoneHash( u16 iHash );

	PAR_PARSABLE;

protected:
	s32 m_iAttachParentId; 
	u16 m_iAttachBoneHash;
	atHashString m_AttachedParentName;

};

inline s32 cutfTriggerLightEffectEventArgs::GetType() const
{
	return CUTSCENE_TRIGGER_LIGHT_EVENT_ARGS_TYPE;
}

inline s32 cutfTriggerLightEffectEventArgs::GetAttachParentId() const
{
	return m_iAttachParentId;
}

inline void cutfTriggerLightEffectEventArgs::SetAttachParentId( s32 iAttachModelId )
{
	m_iAttachParentId = iAttachModelId;
}

inline u16 cutfTriggerLightEffectEventArgs::GetAttachBoneHash() const
{
	return m_iAttachBoneHash;
}

inline void cutfTriggerLightEffectEventArgs::SetAttachBoneHash( u16 iHash )
{
	m_iAttachBoneHash = iHash;
}

inline void cutfTriggerLightEffectEventArgs::SetAttachParentName( const char* pName )
{
	m_AttachedParentName = pName;
}

inline atHashString cutfTriggerLightEffectEventArgs::GetAttachParentHashString() const 
{
	return m_AttachedParentName;
}

#if !__FINAL
inline const char* cutfTriggerLightEffectEventArgs::GetAttachParentName() const
{
	return m_AttachedParentName.GetCStr();
}
#endif
//##############################################################################


class cutfObjectVariationEventArgs : public cutfObjectIdEventArgs
{
public:
	cutfObjectVariationEventArgs(); 
	cutfObjectVariationEventArgs( s32 iObjectid,  int iComponent, int iDrawable, int iTexture );
	cutfObjectVariationEventArgs( s32 iObjectid, int iComponent, int iDrawable, int iTexture, const parAttributeList& attributes );
	virtual ~cutfObjectVariationEventArgs();

	virtual s32 GetType() const; 
	
	
	// PURPOSE: Set the component
	void SetComponent(int iComponent); 
	
	// PURPOSE: Set the component
	void SetDrawable(int iDrawable); 
	
	// PURPOSE: Set the component
	void SetTexture(int iTexture); 

	// PURPOSE: Gets component to change
	// RETURNS: returns a component
	int GetComponent() const; 
	
	// PURPOSE: Gets drawable to change
	// RETURNS: returns a component
	int GetDrawable() const; 
	
	// PURPOSE: Gets texture to change
	// RETURNS: returns a component
	int GetTexture() const; 

	// PURPOSE: Creates a copy of this cut scene event args
	// RETURNS: A clone of this instance
	virtual cutfEventArgs* Clone() const;

	virtual bool operator == ( const cutfEventArgs &other ) const;
	virtual bool operator != ( const cutfEventArgs &other ) const;

	PAR_PARSABLE; 

protected: 
	int m_iComponent; 

	int m_iDrawable; 
	
	int m_iTexture; 
};

inline s32 cutfObjectVariationEventArgs::GetType() const
{
	return CUTSCENE_ACTOR_VARIATION_EVENT_ARGS_TYPE;
}

inline void cutfObjectVariationEventArgs::SetComponent(int iComponent)
{
	m_iComponent = iComponent;
}

inline int cutfObjectVariationEventArgs::GetComponent() const
{
	return m_iComponent;
}

inline void cutfObjectVariationEventArgs::SetDrawable(int iDrawable)
{
	m_iDrawable = iDrawable;
}


inline int cutfObjectVariationEventArgs::GetDrawable() const
{
	return m_iDrawable;
}

inline void cutfObjectVariationEventArgs::SetTexture(int iTexture)
{
	m_iTexture = iTexture;
}

inline int cutfObjectVariationEventArgs::GetTexture() const
{
	return m_iTexture;
}

//##############################################################################

// PURPOSE: Event args for CUTSCENE_SET_VARIATION_EVENT .

class cutfVehicleVariationEventArgs : public cutfObjectIdEventArgs
{
public:
	cutfVehicleVariationEventArgs(); 
	cutfVehicleVariationEventArgs( s32 iObjectid,  int iBodyColour , int iSecondBodyColour , int iSpecColour, int iWheelTrimColour, int iBodyColour5, int iBodyColour6, int Liveryid, int Livery2id, float fDirtLevel );
	cutfVehicleVariationEventArgs( s32 iObjectid,  int iBodyColour , int iSecondBodyColour , int iSpecColour, int iWheelTrimColour, int iBodyColour5, int iBodyColour6, int Liveryid, int Livery2id, float fDirtLevel, const parAttributeList& attributes );
	virtual ~cutfVehicleVariationEventArgs();

	virtual s32 GetType() const; 

	// PURPOSE: Set the component
	void SetBodyColour(int uColour); 

	// PURPOSE: Set the component
	void SetSecondaryBodyColour(int uColour) ; 

	// PURPOSE: Set the component
	void SetSpecularColour(int uColour); 

	// PURPOSE: Gets component to change
	// RETURNS: returns a component
	void SetWheelTrimColour(int uColour); 

	// PURPOSE: Set the component
	void SetBodyColour5(int uColour); 

	// PURPOSE: Set the component
	void SetBodyColour6(int uColour); 

	// PURPOSE: Gets component to change
	// RETURNS: returns a component
	void SetLiveryId(int iLivery); 

	// PURPOSE: Sets component to change
	void SetLivery2Id(int iLivery); 

	// PURPOSE: Gets component to change
	// RETURNS: returns a component
	void SetDirtLevel(float iDirtLevel); 

	//PURPOSE: Get the body colour 
	int	GetBodyColour() const; 

	//PURPOSE: Get the body colour 
	int	GetSecondaryBodyColour() const;

	//PURPOSE: Get the body colour 
	int	GetSpecularBodyColour() const;

	//PURPOSE: Get the body colour 
	int	GetWheelTrimColour() const;

	//PURPOSE: Get the body colour 
	int	GetBodyColour5() const;

	//PURPOSE: Get the body colour 
	int	GetBodyColour6() const;

	//PURPOSE: Get the livery
	int	GetLiveryId() const;

	//PURPOSE: Get the 2nd livery
	int	GetLivery2Id() const;

	//PURPOSE: Get the dirt level; 
	float	GetDirtLevel() const; 

	// PURPOSE: Creates a copy of this cut scene event args
	// RETURNS: A clone of this instance
	virtual cutfEventArgs* Clone() const;
	
	virtual bool operator == ( const cutfEventArgs &other ) const;
	virtual bool operator != ( const cutfEventArgs &other ) const;

	PAR_PARSABLE; 

protected: 
	int							m_iMainBodyColour; 
	int							m_iSecondBodyColour; 
	int							m_iSpecularColour; 
	int							m_iWheelTrimColour;
	int							m_iBodyColour5;
	int							m_iBodyColour6;
	int							m_iLivery;
	int							m_iLivery2;
	float						m_fDirtLevel;  
};

inline s32 cutfVehicleVariationEventArgs::GetType() const
{
	return CUTSCENE_VEHICLE_VARIATION_EVENT_ARGS_TYPE;
}

inline void cutfVehicleVariationEventArgs::SetBodyColour(int iColour)
{
	m_iMainBodyColour = iColour;
}

inline int cutfVehicleVariationEventArgs::GetBodyColour() const
{
	return m_iMainBodyColour;
}

inline void cutfVehicleVariationEventArgs::SetSecondaryBodyColour(int iColour)
{
	m_iSecondBodyColour = iColour;
}

inline int cutfVehicleVariationEventArgs::GetSecondaryBodyColour() const
{
	return m_iSecondBodyColour;
}


inline void cutfVehicleVariationEventArgs::SetSpecularColour(int iColour)
{
	m_iSpecularColour = iColour;
}

inline int cutfVehicleVariationEventArgs::GetSpecularBodyColour() const
{
	return m_iSpecularColour;
}


inline void cutfVehicleVariationEventArgs::SetWheelTrimColour(int iColour)
{
	m_iWheelTrimColour = iColour;
}

inline int cutfVehicleVariationEventArgs::GetWheelTrimColour() const
{
	return m_iWheelTrimColour;
}

inline void cutfVehicleVariationEventArgs::SetBodyColour5(int iColour)
{
	m_iBodyColour5 = iColour;
}

inline int cutfVehicleVariationEventArgs::GetBodyColour5() const
{
	return m_iBodyColour5;
}

inline void cutfVehicleVariationEventArgs::SetBodyColour6(int iColour)
{
	m_iBodyColour6 = iColour;
}

inline int cutfVehicleVariationEventArgs::GetBodyColour6() const
{
	return m_iBodyColour6;
}

inline void cutfVehicleVariationEventArgs::SetLiveryId(int iLiveryId)
{
	m_iLivery = iLiveryId;
}


inline int cutfVehicleVariationEventArgs::GetLiveryId() const
{
	return m_iLivery;
}

inline void cutfVehicleVariationEventArgs::SetLivery2Id(int iLivery2Id)
{
	m_iLivery2 = iLivery2Id;
}

inline int cutfVehicleVariationEventArgs::GetLivery2Id() const
{
	return m_iLivery2;
}

inline void cutfVehicleVariationEventArgs::SetDirtLevel(float fDirtLevel)
{
	m_fDirtLevel = fDirtLevel;
}

inline float cutfVehicleVariationEventArgs::GetDirtLevel() const
{
	return m_fDirtLevel;
}

//##############################################################################


class cutfVehicleExtraEventArgs : public cutfObjectIdEventArgs
{
public:
	cutfVehicleExtraEventArgs(); 
	cutfVehicleExtraEventArgs( s32 iObjectid,  atArray<s32> &Extras );
	cutfVehicleExtraEventArgs( s32 iObjectid,  const atArray<s32> &Extras, const parAttributeList& attributes );
	cutfVehicleExtraEventArgs( s32 iObjectid,  atArray<s32> &Extras, const parAttributeList& attributes );
	virtual ~cutfVehicleExtraEventArgs();
	
	virtual s32 GetType() const; 
	
	//Set the id of the bone to remove
	const atArray<s32>&  GetBoneIdList() const; 

	atArray<s32>&  GetBoneIdList() ; 

	//set a new bone list
	void SetBoneIdList(atArray<s32> &NewBoneList); 
	
	virtual cutfEventArgs* Clone() const;

	virtual bool operator == ( const cutfEventArgs &other ) const;
	virtual bool operator != ( const cutfEventArgs &other ) const;

	PAR_PARSABLE; 

protected:
	atArray<s32> m_pExtraBoneIds; 
	
};

inline s32 cutfVehicleExtraEventArgs::GetType() const
{
	return CUTSCENE_VEHICLE_EXTRA_EVENT_ARGS_TYPE; 
}


inline const atArray<s32>& cutfVehicleExtraEventArgs::GetBoneIdList() const
{
	return m_pExtraBoneIds;
}

inline atArray<s32>& cutfVehicleExtraEventArgs::GetBoneIdList()
{
	return m_pExtraBoneIds;
}

inline void cutfVehicleExtraEventArgs::SetBoneIdList(atArray<s32> &NewBoneList)
{
	m_pExtraBoneIds.Reset(); 
	
	m_pExtraBoneIds.Assume(NewBoneList); 
}


//##############################################################################

// PURPOSE: Event args for CUTSCENE_SHOW_SUBTITLE_EVENT and CUTSCENE_HIDE_SUBTITLE_EVENT.
class cutfSubtitleEventArgs : public cutfNameEventArgs
{
public:
    cutfSubtitleEventArgs();
    cutfSubtitleEventArgs( const char* pIdentifier, int languageID=-1, 
        int iTransitionIn=-1, float fTransitionInDuration=0.0f, int iTransitionOut=-1, float fTransitionOutDuration=0.0f, float fSubtitleDuration=0.0f );
    cutfSubtitleEventArgs( const char* pIdentifier, int languageID, 
        int iTransitionIn, float fTransitionInDuration, int iTransitionOut, float fTransitionOutDuration, float fSubtitleDuration, const parAttributeList& attributes );
	cutfSubtitleEventArgs( atHashString pIdentifier, int languageID, 
		int iTransitionIn, float fTransitionInDuration, int iTransitionOut, float fTransitionOutDuration, float fSubtitleDuration, const parAttributeList& attributes );
    virtual ~cutfSubtitleEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Gets the language ID.
    // RETURNS: The language ID which can be cast to txtLanguage::eLanguage.
    int GetLanguageID() const;

    // PURPOSE: Sets the language ID.
    // PARAMS:
    //    languageID - the language ID which can be cast from txtLanguage::eLanguage.
    void SetLanguageID( int languageID );

    // PURPOSE: Gets the type of transition in to perform. 
    // RETURNS: The index of the project-specific transition.  Anything less than zero should signify "none".
    int GetTransitionIn() const;

    // PURPOSE: Sets the type of transition in to perform.
    // PARAMS:
    //    iTransition - the index of the project-specific transition.  Anything less than zero should signify "none".
    void SetTransitionIn( int iTransition );

    // PURPOSE: Gets the duration of the transition in
    // RETURNS: The duration in seconds.
    float GetTransitionInDuration() const;

    // PURPOSE: Sets the duration of the transition in
    // PARAMS:
    //    fTransitionDuration - the duration in seconds
    void SetTransitionInDuration( float fTransitionDuration );

    // PURPOSE: Gets the type of transition out to perform. 
    // RETURNS: The index of the project-specific transition.  Anything less than zero should signify "none".
    int GetTransitionOut() const;

    // PURPOSE: Sets the type of transition out to perform.
    // PARAMS:
    //    iTransition - the index of the project-specific transition.  Anything less than zero should signify "none".
    void SetTransitionOut( int iTransition );

    // PURPOSE: Gets the duration of the transition out
    // RETURNS: The duration out seconds.
    float GetTransitionOutDuration() const;

    // PURPOSE: Sets the duration of the transition out
    // PARAMS:
    //    fTransitionDuration - the duration out seconds
    void SetTransitionOutDuration( float fTransitionDuration );
	
	// PURPOSE: Sets the length the subtitle should be displayed.
	// PARAMS:fDuration - length in seconds
	//   
	void SetSubtitleDuration( float fDuration );
	
	// PURPOSE: Get the duration that the subtitle should be displayed.
	// RETURNS: The label of the new subtitle. 
	float GetSubtitleDuration() const;

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    int m_iLanguageID;
    int m_iTransitionIn;
    float m_fTransitionInDuration;
    int m_iTransitionOut;
    float m_fTransitionOutDuration;
	float m_fSubtitleDuration; 

};

inline s32 cutfSubtitleEventArgs::GetType() const
{
    return CUTSCENE_SUBTITLE_EVENT_ARGS_TYPE;
}

inline int cutfSubtitleEventArgs::GetLanguageID() const
{
    return m_iLanguageID;
}

inline void cutfSubtitleEventArgs::SetLanguageID( int languageID )
{
    m_iLanguageID = languageID;
}

inline int cutfSubtitleEventArgs::GetTransitionIn() const
{
    return m_iTransitionIn;
}

inline void cutfSubtitleEventArgs::SetTransitionIn( int iTransition )
{
    m_iTransitionIn = iTransition;
}

inline float cutfSubtitleEventArgs::GetTransitionInDuration() const
{
    return m_fTransitionInDuration;
}

inline void cutfSubtitleEventArgs::SetTransitionInDuration( float fTransitionDuration )
{
    m_fTransitionInDuration = fTransitionDuration;
}

inline int cutfSubtitleEventArgs::GetTransitionOut() const
{
    return m_iTransitionOut;
}

inline void cutfSubtitleEventArgs::SetTransitionOut( int iTransition )
{
    m_iTransitionOut = iTransition;
}

inline float cutfSubtitleEventArgs::GetTransitionOutDuration() const
{
    return m_fTransitionOutDuration;
}

inline void cutfSubtitleEventArgs::SetTransitionOutDuration( float fTransitionDuration )
{
    m_fTransitionOutDuration = fTransitionDuration;
}

inline void cutfSubtitleEventArgs::SetSubtitleDuration(float fDuration ) 
{
	m_fSubtitleDuration = fDuration;  
}

inline float cutfSubtitleEventArgs::GetSubtitleDuration() const
{
	return m_fSubtitleDuration;
}

 
//#############################################################################

struct cutfCameraCutCharacterLightParams
{
	cutfCameraCutCharacterLightParams()
		: m_bUseTimeCycleValues(true)
		, m_fIntensity(0.0f)
	{
		m_vDirection = Vector3(0.0f, 1.0f, 0.0f);
		m_vColour.Zero();
	}

	virtual ~cutfCameraCutCharacterLightParams()
	{

	}

	void Reset()
	{
		m_bUseTimeCycleValues = true;
		m_bUseAsIntensityModifier  = false; 
		m_vDirection = Vector3(0.0f, 1.0f, 0.0f);
		m_vColour.Zero();
		m_fIntensity = 0.0f;
		m_fNightIntensity = 0.0f; 
	}

	bool m_bUseTimeCycleValues;
	bool m_bUseAsIntensityModifier ; 
	Vector3 m_vDirection;
	Vector3 m_vColour;
	float m_fIntensity;
	float m_fNightIntensity; 

	PAR_PARSABLE;
};

class cutfCameraCutTimeOfDayDofModifier
{
public:
	cutfCameraCutTimeOfDayDofModifier(); 

	virtual ~cutfCameraCutTimeOfDayDofModifier() {}

	PAR_PARSABLE;

public:
	u32 m_TimeOfDayFlags; 
	s32 m_DofStrengthModifier; 

};

class cutfCameraCutEventArgs : public cutfNameEventArgs
{
public:
    cutfCameraCutEventArgs();

	cutfCameraCutEventArgs( const char* pName, const Vector3 &vPosition, const Vector4 &vRotationQuaternion, 
		float fNearDrawDistance=-1.0f, float fFarDrawDistance=-1.0f, float fMapLodScale=-1.0f, float ReflectionLodRangeStart=-1.0f,
		float ReflectionLodRangeEnd=-1.0f, float ReflectionSLodRangeStart=-1.0f, float ReflectionSLodRangeEnd=-1.0f, 
		float LodMultHD=-1.0f, float LodMultOrphanedHD=-1.0f, float LodMultLod=-1.0f, float LodMultSLod1=-1.0f, float LodMultSLod2=-1.0f,
		float LodMultSLod3=-1.0f, float LodMultSLod4=-1.0f, float WaterReflectionFarClip=-1.0f, float SSAOLightInten=-1.0f, bool bDisableHighQualityDofOverride=false,
		bool FreezeReflectionMap = false, bool DisableDirectionalLighting = false,  bool AbsoluteIntensityEnabled = true, 
		float LightFadeDistanceMult = 1.0f, float LightShadowDistanceMult = 1.0f, float LightSpecularFadeDistMult = 1.0f, 
		float LightVolumetricFadeDistanceMult = 1.0f, float DirectionalLightMultiplier = -1.0f, float LensArtefactMultiplier = -1.0f, float BloomMax = -1.0f);
    
	cutfCameraCutEventArgs( const char* pName, const Vector3 &vPosition, const Vector4 &vRotationQuaternion, 
		float fNearDrawDistance, float fFarDrawDistance, float fMapLodScale, float ReflectionLodRangeStart,
		float ReflectionLodRangeEnd, float ReflectionSLodRangeStart, float ReflectionSLodRangeEnd, 
		float LodMultHD, float LodMultOrphanedHD, float LodMultLod, float LodMultSLod1, float LodMultSLod2,
		float LodMultSLod3, float LodMultSLod4, float WaterReflectionFarClip, float SSAOLightIntenconst, bool bDisableHighQualityDofOverride, 
		bool FreezeReflectionMap, bool DisableDirectionalLighting, bool AbsoluteIntensityEnabled, 
		float LightFadeDistanceMult, float LightShadowDistanceMult, float LightSpecularFadeDistMult, 
		float LightVolumetricFadeDistanceMult, float DirectionalLightMultiplier, float LensArtefactMultiplier, float BloomMax, const parAttributeList& attributes );


	cutfCameraCutEventArgs( atHashString Name, const Vector3 &vPosition, const Vector4 &vRotationQuaternion, 
		float fNearDrawDistance, float fFarDrawDistance, float fMapLodScale, float ReflectionLodRangeStart,
		float ReflectionLodRangeEnd, float ReflectionSLodRangeStart, float ReflectionSLodRangeEnd, 
		float LodMultHD, float LodMultOrphanedHD, float LodMultLod, float LodMultSLod1, float LodMultSLod2,
		float LodMultSLod3, float LodMultSLod4, float WaterReflectionFarClip, float SSAOLightIntenconst, const parAttributeList& attributes,
		float fExposurePush, bool bDisableHighQualityDofOverride, const cutfCameraCutCharacterLightParams& lightParams, bool FreezeReflectionMap, 
		bool DisableDirectionalLighting, bool AbsoluteIntensityEnabled, float LightFadeDistanceMult, float LightShadowDistanceMult, 
		float LightSpecularFadeDistMult, float LightVolumetricFadeDistanceMult, float DirectionalLightMultiplier, float LensArtefactMultiplier, float BloomMax,
		const atArray<cutfCameraCutTimeOfDayDofModifier> &TimeOfDayCamDepthOfFieldModifiers);

    virtual ~cutfCameraCutEventArgs() {}

    // PURPOSE: Retrieves the type of this event args for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    const Vector3& GetPosition() const;

    void SetPosition( const Vector3 &vPosition );

    const Vector4& GetRotationQuaternion() const;

    void SetRotationQuaternion( const Vector4 &vRotationQuaternion );

    // PURPOSE: Retrieves the near draw distance (clip plane) to be used before any 
    //    CUTSCENE_SET_DRAW_DISTANCE_EVENTs, if any.
    // RETURNS: The near draw distance.  -1 to use the game's default value.
    float GetNearDrawDistance() const;

    // PURPOSE: Sets the near draw distance (clip plane) to be used before any 
    //    CUTSCENE_SET_DRAW_DISTANCE_EVENTs, if any.
    // PARAMS:
    //    fNearDrawDistance - the draw distance
    // NOTES: A negative number indicates that we should use the game's default value.
    void SetNearDrawDistance( float fNearDrawDistance );

    // PURPOSE: Retrieves the far draw distance (clip plane) to be used before any 
    //    CUTSCENE_SET_DRAW_DISTANCE_EVENTs, if any.
    // RETURNS: The far draw distance.  -1 to use the game's default value.
    float GetFarDrawDistance() const;

    // PURPOSE: Sets the far draw distance (clip plane) to be used before any 
    //    CUTSCENE_SET_DRAW_DISTANCE_EVENTs, if any.
    // PARAMS:
    //    fFarDrawDistance - the draw distance
    // NOTES: A negative number indicates that we should use the game's default value.
    void SetFarDrawDistance( float fFarDrawDistance );

	// PURPOSE: Retrieves the map lod scale
	// RETURNS: The map lod scale.  -1 to use the game's default value.
	float GetMapLodScale() const;

	// PURPOSE: Sets the map lod scale
	// PARAMS:
	//    fMapLodScale - the map lod scale
	// NOTES: A negative number indicates that we should use the game's default value.
	void SetMapLodScale( float fMapLodScale );
	
	float	GetReflectionLodRangeStart() const; 
	void	SetReflectionLodRangeStart(float ReflectionLodRangeStart); 

	float	GetReflectionLodRangeEnd()const;
	void	SetReflectionLodRangeEnd(float ReflectionLodRangeEnd);

	float	GetReflectionSLodRangeStart()const;
	void	SetReflectionSLodRangeStart(float ReflectionSLodRangeStart);

	float	GetReflectionSLodRangeEnd()const;
	void 	SetReflectionSLodRangeEnd(float ReflectionSLodRangeEnd);

	float	GetLodMultHD()const; 
	void	SetLodMultHD(float LodMultHD); 

	float	GetLodMultOrphanHD()const;
	void	SetLodMultOrphanHD(float LodMultOrphanHD);

	float	GetLodMultLod()const; 
	void	SetLodMultLod(float LodMultLod); 

	float	GetLodMultSlod1()const;
	void	SetLodMultSlod1(float LodMultSlod1);

	float	GetLodMultSlod2()const;
	void	SetLodMultSlod2(float LodMultSlod2);

	float	GetLodMultSlod3()const;
	void	SetLodMultSlod3(float LodMultSlod3);

	float	GetLodMultSlod4()const;
	void	SetLodMultSlod4(float LodMultSlod4);

	float	GetWaterReflectionFarClip()const;
	void	SetWaterReflectionFarClip(float WaterReflectionFarClip);

	float	GetLightSSAOInten()const;
	void	SetLightSSAOInten(float LightSSAOInten);

	float	GetExposurePush() const;
	void	SetExposurePush(float ExposurePush);

	bool	GetDisableHighQualityDof() const;
	void	SetDisableHighQualityDof(bool bDisableHighQualityDof);
	
	bool	IsReflectionMapFrozen() const;
	void	SetReflectionMapFrozen(bool FreezeReflectionMap); 

	bool	IsDirectionalLightDisabled() const;
	void	SetDirectionalLightDisabled(bool DisableDirectionalLighting); 
	
	bool	IsAbsoluteIntensityEnabled() const;
	void	SetAbsoluteIntensityEnabled(bool AbsoluteIntensityEnabled);

	float	GetLightFadeDistanceMult() const;
	void	SetLightFadeDistanceMult(float LightFadeDistanceMult);
	
	float	GetLightShadowFadeDistanceMult() const;
	void	SetLightShadowFadeDistanceMult(float LightShadowDistanceMult);

	float	GetLightSpecularFadeDistMult() const;
	void	SetLightSpecularFadeDistMult(float LightSpecularFadeDistMult);

	float	GetLightVolumetricFadeDistanceMult() const;
	void	SetLightVolumetricFadeDistanceMult(float LightVolumetricFadeDistanceMult); 

	float	GetDirectionalLightMultiplier() const;
	void	SetDirectionalLightMultiplier(float DirectionalLightMultiplier); 

	float	GetLensArtefactMultiplier() const;
	void	SetLensArtefactMultiplier(float LensArtefactMultiplier); 

	float	GetBloomMax() const;
	void	SetBloomMax(float BloomMax); 

	const cutfCameraCutCharacterLightParams& GetCharacterLightParams() const { return m_CharacterLight; }
	void	SetCharacterLightParams(const cutfCameraCutCharacterLightParams& params) { m_CharacterLight = params; }

	bool AddCoCModifier(u32 hourFlags, s32 dofStrengthModifier);

	void RemoveCoCModifier(s32 index); 

	atArray<cutfCameraCutTimeOfDayDofModifier>&  GetCoCModifierList() ; 

	const atArray<cutfCameraCutTimeOfDayDofModifier>&  GetCoCModifierList() const; 

    // PURPOSE: Creates a copy of this cut scene event args
    // RETURNS: A clone of this instance
    virtual cutfEventArgs* Clone() const;

    virtual bool operator == ( const cutfEventArgs &other ) const;
    virtual bool operator != ( const cutfEventArgs &other ) const;

    PAR_PARSABLE;

protected:
    Vector3 m_vPosition;
    Vector4 m_vRotationQuaternion;
    float m_fNearDrawDistance;
    float m_fFarDrawDistance;
	float m_fMapLodScale;
	float m_ReflectionLodRangeStart;
	float m_ReflectionLodRangeEnd;
	float m_ReflectionSLodRangeStart;
	float m_ReflectionSLodRangeEnd;
	float m_LodMultHD;
	float m_LodMultOrphanedHD;
	float m_LodMultLod;
	float m_LodMultSLod1;
	float m_LodMultSLod2;
	float m_LodMultSLod3;
	float m_LodMultSLod4;
	float m_WaterReflectionFarClip;
	float m_SSAOLightInten;
	float m_ExposurePush;
	float m_LightFadeDistanceMult; 
	float m_LightShadowFadeDistanceMult; 
	float m_LightSpecularFadeDistMult; 
	float m_LightVolumetricFadeDistanceMult; 
	float m_DirectionalLightMultiplier; 
	float m_LensArtefactMultiplier; 
	float m_BloomMax; 

	bool m_DisableHighQualityDof;
	bool m_FreezeReflectionMap; 
	bool m_DisableDirectionalLighting; 
	bool m_AbsoluteIntensityEnabled; 

	cutfCameraCutCharacterLightParams m_CharacterLight;
	atArray<cutfCameraCutTimeOfDayDofModifier> m_TimeOfDayDofModifers; 
};

inline s32 cutfCameraCutEventArgs::GetType() const
{
    return CUTSCENE_CAMERA_CUT_EVENT_ARGS_TYPE;
}

inline const Vector3& cutfCameraCutEventArgs::GetPosition() const
{
    return m_vPosition;
}

inline void cutfCameraCutEventArgs::SetPosition( const Vector3 &vPosition )
{
    m_vPosition = vPosition;
}

inline const Vector4& cutfCameraCutEventArgs::GetRotationQuaternion() const
{
    return m_vRotationQuaternion;
}

inline void cutfCameraCutEventArgs::SetRotationQuaternion( const Vector4 &vRotationQuaternion )
{
    m_vRotationQuaternion = vRotationQuaternion;
}

inline float cutfCameraCutEventArgs::GetNearDrawDistance() const
{
    return m_fNearDrawDistance;
}

inline void cutfCameraCutEventArgs::SetNearDrawDistance( float fNearDrawDistance )
{
    m_fNearDrawDistance = fNearDrawDistance;
}

inline float cutfCameraCutEventArgs::GetFarDrawDistance() const
{
    return m_fFarDrawDistance;
}

inline void cutfCameraCutEventArgs::SetFarDrawDistance( float fFarDrawDistance )
{
    m_fFarDrawDistance = fFarDrawDistance;
}

inline float cutfCameraCutEventArgs::GetMapLodScale() const
{
	return m_fMapLodScale;
}

inline void cutfCameraCutEventArgs::SetMapLodScale( float fMapLodScale )
{
	m_fMapLodScale = fMapLodScale;
}

inline float cutfCameraCutEventArgs::GetReflectionLodRangeStart() const
{
	return m_ReflectionLodRangeStart;
}

inline void cutfCameraCutEventArgs::SetReflectionLodRangeStart( float ReflectionLodRangeStart )
{
	m_ReflectionLodRangeStart = ReflectionLodRangeStart;
}

inline float cutfCameraCutEventArgs::GetReflectionLodRangeEnd() const
{
	return m_ReflectionLodRangeEnd;
}

inline void cutfCameraCutEventArgs::SetReflectionLodRangeEnd( float ReflectionLodRangeEnd )
{
	m_ReflectionLodRangeEnd = ReflectionLodRangeEnd;
}

inline float cutfCameraCutEventArgs::GetReflectionSLodRangeStart() const
{
	return m_ReflectionSLodRangeStart;
}

inline void cutfCameraCutEventArgs::SetReflectionSLodRangeStart( float ReflectionSLodRangeStart )
{
	m_ReflectionSLodRangeStart = ReflectionSLodRangeStart;
}

inline float cutfCameraCutEventArgs::GetReflectionSLodRangeEnd() const
{
	return m_ReflectionSLodRangeEnd;
}

inline void cutfCameraCutEventArgs::SetReflectionSLodRangeEnd( float ReflectionSLodRangeEnd )
{
	m_ReflectionSLodRangeEnd = ReflectionSLodRangeEnd;
}

inline float cutfCameraCutEventArgs::GetLodMultHD() const
{
	return m_LodMultHD;
}

inline void cutfCameraCutEventArgs::SetLodMultHD( float LodMultHD )
{
	m_LodMultHD = LodMultHD;
}

inline float cutfCameraCutEventArgs::GetLodMultOrphanHD() const
{
	return m_LodMultOrphanedHD;
}

inline void cutfCameraCutEventArgs::SetLodMultOrphanHD( float LodMultOrphanHD )
{
	m_LodMultOrphanedHD = LodMultOrphanHD;
}

inline float cutfCameraCutEventArgs::GetLodMultLod() const
{
	return m_LodMultLod;
}

inline void cutfCameraCutEventArgs::SetLodMultLod( float LodMultLod )
{
	m_LodMultLod = LodMultLod;
}

inline float cutfCameraCutEventArgs::GetLodMultSlod1() const
{
	return m_LodMultSLod1;
}

inline void cutfCameraCutEventArgs::SetLodMultSlod1( float LodMultSlod1 )
{
	m_LodMultSLod1 = LodMultSlod1;
}

inline float cutfCameraCutEventArgs::GetLodMultSlod2() const
{
	return m_LodMultSLod2;
}

inline void cutfCameraCutEventArgs::SetLodMultSlod2( float LodMultSlod2 )
{
	m_LodMultSLod2 = LodMultSlod2;
}

inline float cutfCameraCutEventArgs::GetLodMultSlod3() const
{
	return m_LodMultSLod3;
}

inline void cutfCameraCutEventArgs::SetLodMultSlod3( float LodMultSlod3 )
{
	m_LodMultSLod3 = LodMultSlod3;
}

inline float cutfCameraCutEventArgs::GetLodMultSlod4() const
{
	return m_LodMultSLod4;
}

inline void cutfCameraCutEventArgs::SetLodMultSlod4( float LodMultSlod4 )
{
	m_LodMultSLod4 = LodMultSlod4;
}

inline float cutfCameraCutEventArgs::GetWaterReflectionFarClip() const
{
	return m_WaterReflectionFarClip;
}

inline void cutfCameraCutEventArgs::SetWaterReflectionFarClip( float WaterReflectionFarClip )
{
	m_WaterReflectionFarClip = WaterReflectionFarClip;
}

inline float cutfCameraCutEventArgs::GetLightSSAOInten() const
{
	return m_SSAOLightInten;
}

inline void cutfCameraCutEventArgs::SetLightSSAOInten( float LightSSAOInten )
{
	m_SSAOLightInten = LightSSAOInten;
}

inline float cutfCameraCutEventArgs::GetExposurePush() const
{
	return m_ExposurePush;
}

inline void cutfCameraCutEventArgs::SetExposurePush( float ExposurePush )
{
	m_ExposurePush = ExposurePush;
}

inline bool cutfCameraCutEventArgs::GetDisableHighQualityDof() const
{
	return m_DisableHighQualityDof;
}

inline void cutfCameraCutEventArgs::SetDisableHighQualityDof(bool bDisableHighQualityDof)
{
	m_DisableHighQualityDof = bDisableHighQualityDof;
}

inline bool cutfCameraCutEventArgs::IsReflectionMapFrozen() const 
{
	return m_FreezeReflectionMap; 
}

inline void cutfCameraCutEventArgs::SetReflectionMapFrozen(bool FreezeReflectionMap)
{
	m_FreezeReflectionMap = FreezeReflectionMap; 
}

inline bool cutfCameraCutEventArgs::IsDirectionalLightDisabled() const 
{
	return m_DisableDirectionalLighting; 
}

inline void cutfCameraCutEventArgs::SetDirectionalLightDisabled(bool DisableDirectionalLighting)
{
	m_DisableDirectionalLighting = DisableDirectionalLighting; 
}

inline float cutfCameraCutEventArgs::GetLightFadeDistanceMult() const
{
	return m_LightFadeDistanceMult;
}

inline void cutfCameraCutEventArgs::SetLightFadeDistanceMult( float LightFadeDistanceMult )
{
	m_LightFadeDistanceMult = LightFadeDistanceMult;
}

inline float cutfCameraCutEventArgs::GetLightShadowFadeDistanceMult() const
{
	return m_LightShadowFadeDistanceMult;
}

inline void cutfCameraCutEventArgs::SetLightShadowFadeDistanceMult( float LightShadowDistanceMult )
{
	m_LightShadowFadeDistanceMult = LightShadowDistanceMult;
}

inline float cutfCameraCutEventArgs::GetLightSpecularFadeDistMult() const
{
	return m_LightSpecularFadeDistMult;
}

inline void cutfCameraCutEventArgs::SetLightSpecularFadeDistMult( float LightSpecularFadeDistMult )
{
	m_LightSpecularFadeDistMult = LightSpecularFadeDistMult;
}

inline float cutfCameraCutEventArgs::GetLightVolumetricFadeDistanceMult() const
{
	return m_LightVolumetricFadeDistanceMult;
}

inline void cutfCameraCutEventArgs::SetLightVolumetricFadeDistanceMult( float LightVolumetricFadeDistanceMult )
{
	m_LightVolumetricFadeDistanceMult = LightVolumetricFadeDistanceMult;
}

inline bool cutfCameraCutEventArgs::IsAbsoluteIntensityEnabled() const 
{
	return m_AbsoluteIntensityEnabled; 
}

inline void cutfCameraCutEventArgs::SetAbsoluteIntensityEnabled(bool AbsoluteIntensityEnabled)
{
	m_AbsoluteIntensityEnabled = AbsoluteIntensityEnabled; 
}

inline atArray<cutfCameraCutTimeOfDayDofModifier>&  cutfCameraCutEventArgs::GetCoCModifierList() 
{
	return m_TimeOfDayDofModifers; 
}

inline const atArray<cutfCameraCutTimeOfDayDofModifier>&  cutfCameraCutEventArgs::GetCoCModifierList() const
{
		return m_TimeOfDayDofModifers; 
}

inline void cutfCameraCutEventArgs::SetDirectionalLightMultiplier( float DirectionalLightMultiplier )
{
	m_DirectionalLightMultiplier = DirectionalLightMultiplier;
}

inline float cutfCameraCutEventArgs::GetDirectionalLightMultiplier() const
{
	return m_DirectionalLightMultiplier;
}

inline void cutfCameraCutEventArgs::SetLensArtefactMultiplier( float LensArtefactMultiplier )
{
	m_LensArtefactMultiplier = LensArtefactMultiplier;
}

inline float cutfCameraCutEventArgs::GetLensArtefactMultiplier() const
{
	return m_LensArtefactMultiplier;
}

inline void cutfCameraCutEventArgs::SetBloomMax( float BloomMax )
{
	m_BloomMax = BloomMax;
}

inline float cutfCameraCutEventArgs::GetBloomMax() const
{
	return m_BloomMax;
}

//##############################################################################

class cutfDecalEventArgs : public cutfEventArgs
{
public:
	cutfDecalEventArgs(); 
	cutfDecalEventArgs( const Vector3 &Position, const Vector4 &Rotation, float width, float height, const Color32 &color, float lifeTime);
	cutfDecalEventArgs( const Vector3 &Position, const Vector4 &Rotation, float width, float height, const Color32 &color, float lifeTime, const parAttributeList& attributes );
	virtual ~cutfDecalEventArgs(){}

	virtual s32 GetType() const; 

	const Vector3& GetPosition() const; 
	void SetPosition(const Vector3& vPos); 

	const Vector4& GetRotationQuaternion() const;
	void SetRotationQuaternion( const Vector4 &vRotationQuaternion );
	
	float GetHeight() const;
	void SetHeight( float height );
	
	float GetWidth() const;
	void SetWidth( float width );

	const Color32& GetColor() const;
	void SetColor( const Color32 &color );

	void SetLifeTime( float lifeTime );
	float GetLifeTime() const;
	
	virtual cutfEventArgs* Clone() const;

	virtual bool operator == ( const cutfEventArgs &other ) const;
	virtual bool operator != ( const cutfEventArgs &other ) const;

	PAR_PARSABLE; 
private: 
	Vector3 m_vPosition; 
	Vector4 m_vRotation;
	float m_fWidth; 
	float m_fHeight; 
	Color32 m_Colour; 
	float m_fLifeTime;

};

inline s32 cutfDecalEventArgs::GetType() const
{
	return CUTSCENE_DECAL_EVENT_ARGS; 
}

inline const Vector3& cutfDecalEventArgs::GetPosition() const
{
	return m_vPosition;
}

inline void cutfDecalEventArgs::SetPosition(const Vector3& vPos)
{
	m_vPosition = vPos;
}

inline const Vector4& cutfDecalEventArgs::GetRotationQuaternion() const
{
	return m_vRotation;
}

inline void cutfDecalEventArgs::SetRotationQuaternion(const Vector4& vRotation)
{
	m_vRotation = vRotation;
}

inline float cutfDecalEventArgs::GetHeight() const
{
	return m_fHeight;
}

inline void cutfDecalEventArgs::SetHeight(float height)
{
	m_fHeight = height;
}

inline float cutfDecalEventArgs::GetWidth() const
{
	return m_fWidth;
}

inline void cutfDecalEventArgs::SetWidth(float width)
{
	m_fWidth = width;
}

inline const Color32& cutfDecalEventArgs::GetColor() const
{
	return m_Colour;
}

inline void cutfDecalEventArgs::SetColor(const Color32 &color)
{
	m_Colour = color;
}

inline void cutfDecalEventArgs::SetLifeTime(float lifeTime)
{
	m_fLifeTime = lifeTime;
}

inline float cutfDecalEventArgs::GetLifeTime() const
{
	return m_fLifeTime;
}

class cutfCascadeShadowEventArgs : public cutfEventArgs
{
public:
	cutfCascadeShadowEventArgs(); 
	
	cutfCascadeShadowEventArgs( const atHashString cameraCutHash, const Vector3 &Position,float radius, float interpTime, 
		s32 cascadeIndex, bool enabled, bool interpDisabled);

	cutfCascadeShadowEventArgs( const atHashString cameraCutHash, const Vector3 &Position,float radius, float interpTime, 
		s32 cascadeIndex, bool enabled, bool interpDisabled, const parAttributeList& attributes );

	virtual s32 GetType() const; 
	
	void SetCascadeShadowIndex(s32 ShadowIndex); 

	s32 GetCascadeShadowIndex() const; 
	
	void SetIsEnabled(bool enabled); 
	
	bool GetIsEnabled() const;
	
	void SetInterpDisabled(bool enabled); 
	
	bool GetIsInterpDisabled() const;
	
	void SetRadius(float radius); 
	
	float GetRadius() const; 
	
	void SetPosition(const Vector3& position);

	const Vector3& GetPosition() const;

	void SetInterpTime(float interpTime); 
	
	float GetInterpTime() const; 

	atHashString GetCameraCutHash() const; 

	void SetCameraCutHash(atHashString hash); 
	
	virtual cutfEventArgs* Clone() const;

	PAR_PARSABLE; 

private: 
	atHashString m_cameraCutHashName; 

	Vector3 m_position;

	float m_radius;
	float m_interpTime; 

	s32 m_cascadeIndex;
	
	bool m_enabled; 
	bool m_interpolateToDisabled; 
	
};

//##############################################################################

} // namespace rage

#endif // CUTFILE_CUTFEVENTARGS_H 
