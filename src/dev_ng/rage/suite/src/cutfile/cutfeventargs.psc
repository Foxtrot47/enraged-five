<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
              generate="psoChecks">
  
	<structdef type="::rage::cutfEventArgs">
		<pad bytes="4"/> <!-- u16 m_iRefCount, 2 bytes pad -->
			<struct name="m_attributeList" type="::rage::parAttributeList"/>
      <pointer name="m_cutfAttributes" type="::rage::cutfAttributeList" policy="simple_owner"/>
  </structdef>

	<structdef type="::rage::cutfEventArgsList" base="::rage::cutfEventArgs" onPreLoad="PreLoad">
			<array name="m_iEventArgsIndexList" type="atArray">
		        <s32/>
			</array>
	</structdef>

	<structdef type="::rage::cutfNameEventArgs" base="::rage::cutfEventArgs">
    <string name="m_cName" type="atHashString"/>
	</structdef>

  <structdef type="::rage::cutfFinalNameEventArgs" base="::rage::cutfEventArgs">
    <string name="m_cName" type="atString" />
  </structdef>
  
	<structdef type="::rage::cutfFloatValueEventArgs" base="::rage::cutfEventArgs">
			<float name="m_fValue"/>
	</structdef>

	<structdef type="::rage::cutfTwoFloatValuesEventArgs" base="::rage::cutfFloatValueEventArgs">
			<float name="m_fValue2"/>
	</structdef>

  <structdef type="::rage::cutfBoolValueEventArgs" base="::rage::cutfEventArgs">
    <bool name="m_bValue"/>
  </structdef>

  <structdef type="::rage::cutfFloatBoolValueEventArgs" base="::rage::cutfBoolValueEventArgs">
    <float name="m_fValue"/>
  </structdef>
  
  <structdef type="::rage::cutfScreenFadeEventArgs" base="::rage::cutfFloatValueEventArgs">
			<Color32 name="m_color"/>
	</structdef>

	<structdef type="::rage::cutfLoadSceneEventArgs" base="::rage::cutfNameEventArgs">
			<Vector3 name="m_vOffset"/>
			<float name="m_fRotation"/>
      <float name="m_fPitch"/>
      <float name="m_fRoll"/>
	</structdef>

	<structdef type="::rage::cutfObjectIdEventArgs" base="::rage::cutfEventArgs">
			<s32 name="m_iObjectId"/>
	</structdef>

	<structdef type="::rage::cutfObjectIdNameEventArgs" base="::rage::cutfObjectIdEventArgs">
    <string name="m_cName" type="atHashString"/>
	</structdef>

  <structdef type="::rage::cutfObjectIdPartialHashEventArgs" base="::rage::cutfObjectIdEventArgs">
    <u32 name="m_PartialHash" init="0"/>
  </structdef>
  
	<structdef type="::rage::cutfAttachmentEventArgs" base="::rage::cutfObjectIdEventArgs">
    <string name="m_cBoneName" type="atHashString"/>
	</structdef>

	<structdef type="::rage::cutfObjectIdListEventArgs" base="::rage::cutfEventArgs">
			<array name="m_iObjectIdList" type="atArray">
					<s32 />
			</array>
	</structdef>

	<structdef type="::rage::cutfPlayParticleEffectEventArgs" base="::rage::cutfEventArgs">
    <Vector4 name="m_vInitialBoneRotation" init="0.0f,0.0f,0.0f,1.0f" />
    <Vector3 name="m_vInitialBoneOffset" init="0.0f,0.0f,0.0f" />
    <s32 name="m_iAttachParentId" init="-1" />
    <u16 name="m_iAttachBoneHash" init="0" />
	</structdef>

	<structdef type="::rage::cutfObjectVariationEventArgs" base="::rage::cutfObjectIdEventArgs">
			<int name="m_iComponent" init="0"/>
			<int name="m_iDrawable" init="0"/>
			<int name="m_iTexture" init="0"/>
	</structdef>

	<structdef type="::rage::cutfVehicleVariationEventArgs" base="::rage::cutfObjectIdEventArgs">
			<int name="m_iMainBodyColour" init="0"/>
			<int name="m_iSecondBodyColour" init="0"/>
			<int name="m_iSpecularColour" init="0"/>
			<int name="m_iWheelTrimColour" init="0"/>
			<int name="m_iBodyColour5" init="0"/>
			<int name="m_iLivery" init="0"/>
			<int name="m_iLivery2" init="0"/>
			<float name="m_fDirtLevel" init="0.0"/>
	</structdef>

	<structdef type="::rage::cutfVehicleExtraEventArgs" base="::rage::cutfObjectIdEventArgs">
			<array name="m_pExtraBoneIds" type="atArray">
		<s32/> 
			</array>
	</structdef>

	<structdef type="::rage::cutfSubtitleEventArgs" base="::rage::cutfNameEventArgs">
			<int name="m_iLanguageID" init="-1"/>
			<int name="m_iTransitionIn" init="-1"/>
			<float name="m_fTransitionInDuration" init="0.0f"/>
			<int name="m_iTransitionOut" init="-1"/>
			<float name="m_fTransitionOutDuration" init="0.0f"/>
      <float name="m_fSubtitleDuration" init="0.0f"/>
	</structdef>

  <structdef type="::rage::cutfCameraCutCharacterLightParams">
    <bool name="m_bUseTimeCycleValues" init="false"/>
    <Vector3 name="m_vDirection" min="-1.0f" max="1.0f" init="0.0f,1.0f,0.0f"/>
    <Vector3 name="m_vColour" min="0.0f" max="1.0f" init="0.0f,0.0f,0.0f"/>
    <float name="m_fIntensity" min="0.0f" max="64.0f" init="0.0f"/>
  </structdef>

  <structdef type="::rage::cutfCameraCutTimeOfDayDofModifier">
    <u32 name="m_TimeOfDayFlags" init="0"/>
    <s32 name="m_DofStrengthModifier" min="-15" max="15" init="0"/>
  </structdef>

  <structdef type="::rage::cutfCameraCutEventArgs" base="::rage::cutfNameEventArgs">
		<Vector3 name="m_vPosition"/>
		<Vector4 name="m_vRotationQuaternion"/>
		<float name="m_fNearDrawDistance" init="-1.0f"/>
		<float name="m_fFarDrawDistance" init="-1.0f"/>
    <float name="m_fMapLodScale" init="-1.0f"/>
    <float name="m_ReflectionLodRangeStart" init="-1.0f"/>
    <float name="m_ReflectionLodRangeEnd" init="-1.0f"/>
    <float name="m_ReflectionSLodRangeStart" init="-1.0f"/>
    <float name="m_ReflectionSLodRangeEnd" init="-1.0f"/>
    <float name="m_LodMultHD" init="-1.0f"/>
    <float name="m_LodMultOrphanedHD" init="-1.0f"/>
    <float name="m_LodMultLod" init="-1.0f"/>
    <float name="m_LodMultSLod1" init="-1.0f"/>
    <float name="m_LodMultSLod2" init="-1.0f"/>
    <float name="m_LodMultSLod3" init="-1.0f"/>
    <float name="m_LodMultSLod4" init="-1.0f"/>
    <float name="m_WaterReflectionFarClip" init="-1.0f"/>
    <float name="m_SSAOLightInten" init="-1.0f"/>
    <float name="m_ExposurePush" init="0.0f"/>
	  <float name="m_LightFadeDistanceMult" init="1.0f"/>
	  <float name="m_LightShadowFadeDistanceMult" init="1.0f"/>
	  <float name="m_LightSpecularFadeDistMult" init="1.0f"/>
	  <float name="m_LightVolumetricFadeDistanceMult" init="1.0f"/>
    <float name="m_DirectionalLightMultiplier" init="-1.0f"/>
    <float name="m_LensArtefactMultiplier" init="-1.0f"/>
    <float name="m_BloomMax" init="-1.0f"/>
    <bool name="m_DisableHighQualityDof" init="false"/>
    <bool name="m_FreezeReflectionMap" init="false"/>
    <bool name="m_DisableDirectionalLighting" init="false"/>
	  <bool name="m_AbsoluteIntensityEnabled" init="false"/>
    <struct name="m_CharacterLight" type="::rage::cutfCameraCutCharacterLightParams"/>
    <array name="m_TimeOfDayDofModifers" type="atArray">
      <struct type="::rage::cutfCameraCutTimeOfDayDofModifier"/>
    </array>
  </structdef>

  <structdef type="::rage::cutfDecalEventArgs" base="::rage::cutfEventArgs">
    <Vector3 name="m_vPosition" init="0.0f, 0.0f, 0.0f"/>
    <Vector4 name="m_vRotation"/>
    <float name="m_fWidth" init="0.0f"/>
    <float name="m_fHeight" init="0.0f"/>
    <Color32 name="m_Colour"/>
    <float name="m_fLifeTime" init="-1.0f"/>
  </structdef>

  <structdef type="::rage::cutfCascadeShadowEventArgs" base="::rage::cutfEventArgs">
    <string name="m_cameraCutHashName" type="atHashString"/>
    <Vector3 name="m_position" init="0.0f, 0.0f, 0.0f"/>
    <float name="m_radius" init="0.0f"/>
    <float name="m_interpTime" init="0.0f"/>
    <s32 name="m_cascadeIndex" init="-1"/>
    <bool name="m_enabled" init="false"/>
    <bool name="m_interpolateToDisabled" init="false"/>
  </structdef>

  <structdef type="::rage::cutfTriggerLightEffectEventArgs" base="::rage::cutfEventArgs">
    <s32 name="m_iAttachParentId" init="-1" />
    <u16 name="m_iAttachBoneHash" init="0" />
    <string name="m_AttachedParentName" type="atHashString"/>
  </structdef>
  
</ParserSchema>
