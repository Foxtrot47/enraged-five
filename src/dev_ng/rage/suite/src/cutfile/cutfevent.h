// 
// cutscene/cutsevent.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTFILE_CUTFEVENT_H 
#define CUTFILE_CUTFEVENT_H 

#include "atl/array.h"
#include "atl/functor.h"
#include "atl/map.h"
#include "atl/string.h"
#include "parser/macros.h"

namespace rage {

class cutfCutsceneFile2;
class cutfEventArgs;
class parTreeNode;

// PURPOSE: An enumeration of known cut scene event types
enum ECutsceneEventType
{
    CUTSCENE_EVENT_TYPE,
    CUTSCENE_OBJECT_ID_EVENT_TYPE,
    CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE,
    CUTSCENE_NUM_EVENT_TYPES,               // custom event types should with this type
};

// PURPOSE: An enumeration of known cut scene event ids
enum ECutsceneEvent
{
    // Loading events
    CUTSCENE_LOAD_SCENE_EVENT,              // Tell the game to get ready for the cut scene using the offset and extra room info.  cutfLoadSceneEventArgs
    CUTSCENE_UNLOAD_SCENE_EVENT,            // Unload the scene
    CUTSCENE_LOAD_ANIM_DICT_EVENT,          // Load an animation dictionary.  cutfNameEventArgs
    CUTSCENE_UNLOAD_ANIM_DICT_EVENT,        // Unload the animation dictionary.
    CUTSCENE_LOAD_AUDIO_EVENT,              // Load/preseek the audio.  cutfNameEventArgs
    CUTSCENE_UNLOAD_AUDIO_EVENT,            // Unload the audio.  cutfNameEventArgs
    CUTSCENE_LOAD_MODELS_EVENT,             // Load the models needed for the scene.  cutfObjectIdListEventArgs
    CUTSCENE_UNLOAD_MODELS_EVENT,           // Unload the models that are no longer needed for the scene.  cutfObjectIdListEventArgs
    CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT,   // Load the particle effects.  cutfObjectIdListEventArgs
    CUTSCENE_UNLOAD_PARTICLE_EFFECTS_EVENT, // Unload the particle effects.  cutfObjectIdListEventArgs
    CUTSCENE_LOAD_OVERLAYS_EVENT,           // Load the screen overlays.  cutfObjectIdListEventArgs
    CUTSCENE_UNLOAD_OVERLAYS_EVENT,         // Unload the overlays.  cutfObjectIdListEventArgs
    CUTSCENE_LOAD_SUBTITLES_EVENT,          // Load the subtitles.  cutfNameEventArgs
    CUTSCENE_UNLOAD_SUBTITLES_EVENT,        // Unload the subtitles.  cutfNameEventArgs
    CUTSCENE_HIDE_OBJECTS_EVENT,            // Hide the specified objects.  cutfObjectIdListEventArgs
    CUTSCENE_SHOW_OBJECTS_EVENT,            // Unhide the specified objects.  cutfObjectIdListEventArgs
    CUTSCENE_FIXUP_OBJECTS_EVENT,           // Fixup the specified physics objects.  cutfObjectIdListEventArgs
    CUTSCENE_REVERT_FIXUP_OBJECTS_EVENT,    // Undo the fixup to the specified physics objects.  cutfObjectIdListEventArgs
    CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT,     // Add the blocking bounds.  cutfObjectIdListEventArgs
    CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT,  // Remove the blocking bounds.    cutfObjectIdListEventArgs
        
    // Playback events
    CUTSCENE_FADE_OUT_EVENT,                // Fade out to black.  cutfScreenFadeEventArgs
    CUTSCENE_FADE_IN_EVENT,                 // Fade in from black.  cutfScreenFadeEventArgs
    CUTSCENE_SET_ANIM_EVENT,                // Set the model to play the given animation.  cutfObjectIdNameEventArgs
    CUTSCENE_CLEAR_ANIM_EVENT,              // Clear the animation from the model.  cutfObjectIdNameEventArgs
    CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT,    // Trigger the particle effect.  cutfPlayParticleEffectEventArgs
    CUTSCENE_STOP_PARTICLE_EFFECT_EVENT,    // Stop the particle effect.  cutfPlayParticleEffectEventArgs
    CUTSCENE_SHOW_OVERLAY_EVENT,            // Show the overlay.  cutfEventArgs
    CUTSCENE_HIDE_OVERLAY_EVENT,            // Hide the overlay.  cutfEventArgs
    CUTSCENE_PLAY_AUDIO_EVENT,              // Play audio.  cutfNameEventArgs
    CUTSCENE_STOP_AUDIO_EVENT,              // Stop audio.  cutfNameEventArgs
    CUTSCENE_SHOW_SUBTITLE_EVENT,           // Show the subtitle.  cutfSubtitleEventArgs
    CUTSCENE_HIDE_SUBTITLE_EVENT,           // Hide the subtitle.  cutfSubtitleEventArgs
    CUTSCENE_SET_DRAW_DISTANCE_EVENT,       // Set the draw distance.  cutfTwoFloatValuesEventArgs.  Data to CUTSCENE_CAMERA_CUT_EVENT.  In practice, this event will only be fired when editing the Draw Distances with the Rag Widgets.
    CUTSCENE_SET_ATTACHMENT_EVENT,          // Sets up a parent-child relationship between two model objects, connected at a specified bone.  cutfAttachmentEventArgs
    CUTSCENE_SET_VARIATION_EVENT,           // Set a ped variation.  Technically, this is considered a custom GTA event, but is included for backwards compatibility.  ???
    CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT,    // Activate the blocking bounds.  cutfEventArgs
    CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT,  // Deactivate the blocking bounds.  cutfEventArgs
    CUTSCENE_HIDE_HIDDEN_OBJECT_EVENT,      // Hide the hidden object.  cutfEventArgs
    CUTSCENE_SHOW_HIDDEN_OBJECT_EVENT,      // Show the hidden object.  cutfEventArgs
    CUTSCENE_FIX_FIXUP_OBJECT_EVENT,        // Fix the fixup object.  cutfEventArgs
    CUTSCENE_REVERT_FIXUP_OBJECT_EVENT,     // Revert the fixup object.  cutfEventArgs

    // New Load events
    CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT,      // Add the removal bounds.  cutfObjectIdListEventArgs
    CUTSCENE_REMOVE_REMOVAL_BOUNDS_EVENT,   // Remove the removal bounds.    cutfObjectIdListEventArgs

    // New Playback events
    CUTSCENE_CAMERA_CUT_EVENT,              // A camera cut.  cutfCameraCutEventArgs
    CUTSCENE_ACTIVATE_REMOVAL_BOUNDS_EVENT,     // Activate the removal bounds.  cutfEventArgs
    CUTSCENE_DEACTIVATE_REMOVAL_BOUNDS_EVENT,   // Deactivate the removal bounds.  cutfEventArgs

	CUTSCENE_LOAD_RAYFIRE_EVENT,			// Load the rayfire's needed for the scene.  
	CUTSCENE_UNLOAD_RAYFIRE_EVENT,

	CUTSCENE_ENABLE_DOF_EVENT,			// CUTSCENE_ENABLE_DEPTH_OF_FIELD_EVENT
	CUTSCENE_DISABLE_DOF_EVENT,			// CUTSCENE_DISABLE_DEPTH_OF_FIELD_EVENT

	CUTSCENE_CATCHUP_CAMERA_EVENT,

	CUTSCENE_BLENDOUT_CAMERA_EVENT,
	
	CUTSCENE_TRIGGER_DECAL_EVENT,    // Trigger the decal
	CUTSCENE_REMOVE_DECAL_EVENT,    // Remove a decal
	
	CUTSCENE_ENABLE_CASCADE_SHADOW_BOUNDS_EVENT,

	CUTSCENE_CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER, 
	CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_UPDATE, 
	CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_UPDATE, 
	CUTSCENE_CASCADE_SHADOWS_SET_AIRCRAFT_MODE, 
	CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE, 
	CUTSCENE_CASCADE_SHADOWS_SET_FLY_CAMERA_MODE, 

	CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV, 
	CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV,
	CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE, 
	CUTSCENE_CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE, 
	CUTSCENE_CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT,
	CUTSCENE_CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE, 
	
	CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_MINMAX,
	CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_MINMAX, 
	CUTSCENE_CASCADE_SHADOWS_SET_DEPTH_BIAS, 
	CUTSCENE_CASCADE_SHADOWS_SET_SLOPE_BIAS, 
	CUTSCENE_CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE, 
		
	CUTSCENE_RESET_ADAPTION_EVENT, 
	CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE,
	
	CUTSCENE_SET_LIGHT_EVENT,
	CUTSCENE_CLEAR_LIGHT_EVENT, 
	
	CUTSCENE_CASCADE_SHADOWS_RESET_CASCADE_SHADOWS, //Dont forget to add an opposite event

	CUTSCENE_START_REPLAY_RECORD,  // No args
	CUTSCENE_STOP_REPLAY_RECORD,  // No args

	CUTSCENE_FIRST_PERSON_BLENDOUT_CAMERA_EVENT,

	CUTSCENE_FIRST_PERSON_CATCHUP_CAMERA_EVENT, 

    CUTSCENE_NUM_EVENTS,

    // highest rage cutfile "built in" event
    CUTSCENE_EVENT_LAST_RESERVED_FOR_CUTFILE_USE_ONLY = 0x7f,

    // first rage cutscene "built in" event
    CUTSCENE_EVENT_FIRST_RESERVED_FOR_RUNTIME_USE_ONLY = 0x80,  // The events defined in cutscene/cutsevent.h will start with this enum.

    // highest rage cutscene "built in" event
    CUTSCENE_EVENT_LAST_RESERVED_FOR_RUNTIME_USE_ONLY = 0xff,

    // first project specific event
    CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE = 0x100
};

// PURPOSE: This is the rank number of the last unload event for sorting 
//    purposes.  Other unload events are negative offsets from this.
#define CUTSCENE_LAST_UNLOAD_EVENT_RANK 65535

// PURPOSE: This is the rank number of the last playback event for sorting
//    purposes.  Built-in playback events are less than this, and 
//    project-specific events ranks should start with this.
#define CUTSCENE_LAST_EVENT_RANK 255

// PURPOSE: When scrubbing or playing back in reverse, or when turning a load 
//    event into an unload event, this is the opposite event id that signals 
//    the run time that we should look for the previous instance of the event 
//    id to dispatch.  Obviously, if we're in the unload situation, no event
//    will be dispatched.
#define CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS -1

// PURPOSE: When scrubbing or playing back in reverse, or when turning a load 
//    event into an unload event, this is the opposite event id that signals 
//    the run time that we have no opposite event to dispatch.
#define CUTSCENE_NO_OPPOSITE_EVENT -2

//##############################################################################

inline const char* DefaultEventIdDisplayNameFtor( s32 /*iEventId*/ )
{
    return "(unknown)";
}

inline s32 DefaultEventIdRankFtor( s32 /*iEventId*/ )
{
    return CUTSCENE_LAST_EVENT_RANK;
}

inline s32 DefaultOppositeEventIdFtor( s32 /*iEventId*/ )
{
    return CUTSCENE_NO_OPPOSITE_EVENT;
};

//##############################################################################

class cutfChildEvents
{
public:
	atArray<u32> m_ChildEvents; 
	PAR_SIMPLE_PARSABLE;
};


// PURPOSE: A class that contains the timing and information to execute a cut
//  scene event.
class cutfEvent
{
public:
    typedef Functor1Ret<const char*, s32> EventIdDisplayNameFtor;
    typedef Functor1Ret<s32, s32> EventIdRankFtor;
    typedef Functor1Ret<s32, s32> OppositeEventIdFtor;

    cutfEvent();
    cutfEvent( float fTime, s32 iEventId, cutfEventArgs *pEventArgs=NULL );
	cutfEvent( float fTime, s32 iEventId, bool IsChild, u32 StickyId, cutfEventArgs *pEventArgs=NULL );
    virtual ~cutfEvent();

    // PURPOSE: Initializes the static members of this class.
    static void InitClass();

    // PURPOSE: Shuts down the static members of this class.
    static void ShutdownClass();

    // PURPOSE: This function will set the EventIdDisplayNameFtor which is required if
    //    a project has defined its own event ids.  The functor takes in the event id
    //    a returns its display name.
    // PARAMS:
    //    ftor - the functor
    //    iFirstEventId - the first event id that should call this functor.
    // NOTES: The functor will only be called for event ids that are greater
    //    than or equal to CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE.
    static void SetEventIdDisplayNameFunc( EventIdDisplayNameFtor ftor, s32 iFirstEventId=CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE );

    // PURPOSE: This function will add a EventIdRankFtor which is required if
    //    a project has defined its own event ids.  The functor takes in the event id
    //    a returns its rank number, which should start at CUTSCENE_LAST_EVENT_RANK.
    // PARAMS:
    //    ftor - the functor
    //    iFirstEventId - the first event id that should call this functor.
    // NOTES: The functor will only be called for event ids that are greater
    //    than or equal to CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE.
    static void SetEventIdRankFunc( EventIdRankFtor ftor, s32 iFirstEventId=CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE );

    // PURPOSE: This function will set the OppositeEventIdFtor which is required if
    //    a project has defined its own events ids.  The functor takes in the event id
    //    a returns its opposite event id, CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS, or
    //    CUTSCENE_NO_OPPOSITE_EVENT.
    // PARAMS:
    //    ftor - the functor
    //    iFirstEventId - the first event id that should call this functor.
    // NOTES: The functor will only be called for event ids that are greater
    //    than or equal to CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE.
    static void SetOppositeEventIdFunc( OppositeEventIdFtor ftor, s32 iFirstEventId=CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE );

    // PURPOSE: Retrieves the type of this event for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the type name of this data type.
    // RETURNS: The type name.
    virtual const char* GetTypeName() const;

#if __BANK || __TOOL
    // PURPOSE: Retrieves the display name of the event id.
    // RETURNS: The event id display name.
    const char* GetDisplayName() const;


    // PURPOSE: Retrieves the display name of the event type and event id.
    // RETURNS: The event display name.
    const atString& GetLongDisplayName() const;
#endif

    // PURPOSE: Retrieves the time during the cut scene that this event should be dispatched.
    // RETURNS: Time in seconds.
    float GetTime() const;

    // PURPOSE: Sets the time during the cut scene that this event should be dispatched.
    // PARAMS:
    //    fTime - time in seconds
    void SetTime( float fTime );

    // PURPOSE: Retrieves the event id that describes this event.  Built-in events ids are given
    //    by the ECutsceneEvent enum.
    // RETURNS: The event id.
    s32 GetEventId() const;

    // PURPOSE: Retrieves the event id that describes the opposite of this event.
    // RETURNS: The event id.  If it returns CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS, the opposite 
    //    event is the previous occurrence of the event id in the time line.  If it returns 
    //    CUTSCENE_NO_OPPOSITE_EVENT, no opposite event shall be dispatched.
    // NOTES:  This will be used in 2 ways: 1) to turn the "Load Events" into the "Unload Events".
    //    2) To help with scrubbing during playback.
    s32 GetOppositeEventId() const;

    // PURPOSE: Retrieves the sorting rank for the given event id.
    // RETURNS: the sorting rank.
    // NOTES:  This will be used to sort the event lists in chronological and rank order.
    s32 GetRank() const;

    // PURPOSE: Retrieves the event args that go with this event.
    // RETURNS: The event args.
    const cutfEventArgs* GetEventArgs() const;

    // PURPOSE: Sets the event args, deleting the old ones, if needed
    // PARAMS:
    //    pEventArgs - the event args
    void SetEventArgs( cutfEventArgs* pEventArgs );

    // PURPOSE: Compares this event to another event for purposes of sorting by time and event id.
    // PARAMS:
    //    pEvent - the event to compare to.
    // RETURNS: -1 if this event should come before pEvent, 1 if after, and 0 otherwise
    int Compare( const cutfEvent *pEvent ) const;

	// PURPOSE: Compares this event to another event for purposes of sorting by time.
	// PARAMS:
	//    pEvent - the event to compare to.
	// RETURNS: 1 or 0
	int LoadCompare( const cutfEvent *pEvent ) const;

    // PURPOSE: Creates a copy of this cut scene event
    // RETURNS: A clone of this instance
    virtual cutfEvent* Clone() const;

#if __BANK || __TOOL
    // PURPOSE: Retrieves the name of the event id.
    // PARAMS:
    //    iEventId - the event id to get the display name of
    // RETURNS: The display name of the given event id.
    // NOTES: This will be used when editing the events in various tools.
    static const char* GetDisplayName( s32 iEventId );
#endif
    // PURPOSE: Retrieves the event id that describes the opposite of the event.
    // PARAMS:
    //    iEventId - the event id to get the opposite of
    // RETURNS: The event id.  If it returns CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS, the opposite 
    //    event is the previous occurrence of the event id in the time line.  If it returns 
    //    CUTSCENE_NO_OPPOSITE_EVENT, no opposite event shall be dispatched.
    // NOTES:  This will be used in 2 ways: 1) to turn the "Load Events" into the "Unload Events".
    //    2) To help with scrubbing during playback.
    static s32 GetOppositeEventId( s32 iEventId );

    // PURPOSE: Retrieves the sorting rank for the given event id.
    // PARAMS:
    //    iEventId - the event id to get the rank of
    // RETURNS: the sorting rank.
    // NOTES:  This will be used to sort the event lists in chronological and rank order.
    static s32 GetRank( s32 iEventId );

    // PURPOSE: Brings all the attributes in the 'other' event into this event
    // PARAMS:
    //		other - The event to merge into this one
    //		overwrite - If true, attributes in 'other' will overwrite ones that are in this event.
    virtual void MergeFrom( const cutfEvent &other, bool overwrite=true );

    virtual bool operator == ( const cutfEvent &other ) const;
    virtual bool operator != ( const cutfEvent &other ) const;

	void PreLoad(parTreeNode* node);
	void ConvertArgPointersToIndices();
	void ConvertArgIndicesToPointers();

	const atArray<u32>& GetChildEvents() const { return m_pChildEvents->m_ChildEvents; }

	void AddChildEventIndex(u32 Index); 

	bool HasChildren() const { return m_pChildEvents && m_pChildEvents->m_ChildEvents.GetCount() > 0;  }

	u32 GetStickyId() const { return m_StickyId; }

	void SetStickyEventId(u32 stickyEventId) { m_StickyId = stickyEventId; } 

	bool IsChild() const { return m_IsChild; }

	void SetIsChild(bool IsChild) { m_IsChild = IsChild; }

	void ResetChildEvents(); 

    PAR_PARSABLE;

protected:
    mutable atString m_longDisplayName;
    float m_fTime;
    s32 m_iEventId;
	s32 m_iEventArgsIndex; /* for serialization, don't use at runtime */
	cutfEventArgs *m_pEventArgsPtr;
	cutfChildEvents* m_pChildEvents; 
	u32 m_StickyId; 
	bool m_IsChild; 

private:
    static atMap<s32, EventIdDisplayNameFtor> s_eventIdDisplayNameFtorMap;
    static atMap<s32, EventIdRankFtor> s_eventIdRankFtorMap;
    static atMap<s32, OppositeEventIdFtor> s_oppositeEventIdFtorMap;

#if __BANK || __TOOL
    static const char* c_cutsceneEventIdDisplayNames[]; // for the static GetDisplayName function
#endif 
	static const s32 c_oppositeEventIds[];          // for the base OppositeEventId function
    static const s32 c_cutsceneEventSortingRanks[]; // for the base Compare function
};

inline s32 cutfEvent::GetType() const
{
    return CUTSCENE_EVENT_TYPE;
}

inline const char* cutfEvent::GetTypeName() const
{
    return "Event";
}

#if __BANK || __TOOL
inline const char* cutfEvent::GetDisplayName() const
{

	return GetDisplayName( GetEventId() );
}
#endif

inline float cutfEvent::GetTime() const
{
    return m_fTime;
}

inline void cutfEvent::SetTime( float fTime )
{
    m_fTime = fTime;
}

inline s32 cutfEvent::GetEventId() const
{
    return m_iEventId;
}

inline s32 cutfEvent::GetOppositeEventId() const
{
    return GetOppositeEventId( m_iEventId );
}

inline s32 cutfEvent::GetRank() const
{
    return GetRank( m_iEventId );
}

inline const cutfEventArgs* cutfEvent::GetEventArgs() const
{
    return m_pEventArgsPtr;
}

//##############################################################################

class cutfObjectIdEvent : public cutfEvent
{
public:
    cutfObjectIdEvent();
    cutfObjectIdEvent( s32 iObjectId, float fTime, s32 iEventId, cutfEventArgs* pEventArgs=NULL );
	cutfObjectIdEvent( s32 iObjectId, float fTime, s32 iEventId, bool IsChild, u32 StickyId, cutfEventArgs* pEventArgs=NULL );
    virtual ~cutfObjectIdEvent() {}

    // PURPOSE: Retrieves the type of this event for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the type name of this data type.
    // RETURNS: The type name.
    virtual const char* GetTypeName() const;

    // PURPOSE: Retrieves the object id that is the target of this event
    // RETURNS: The object id.
    s32 GetObjectId() const;

    // PURPOSE: Sets the object id that is the target of this event
    // PARAMS:
    //    iObjectId - the object id
    void SetObjectId( s32 iObjectId );

    // PURPOSE: Creates a copy of this cut scene event
    // RETURNS: A clone of this instance
    virtual cutfEvent* Clone() const;

    virtual bool operator == ( const cutfEvent &other ) const;
    virtual bool operator != ( const cutfEvent &other ) const;

    PAR_PARSABLE;

protected:
    s32 m_iObjectId;
};

inline s32 cutfObjectIdEvent::GetType() const
{
    return CUTSCENE_OBJECT_ID_EVENT_TYPE;
}

inline const char* cutfObjectIdEvent::GetTypeName() const
{
    return "Object Id Event";
}

inline s32 cutfObjectIdEvent::GetObjectId() const
{
    return m_iObjectId;
}

inline void cutfObjectIdEvent::SetObjectId( s32 iObjectId )
{
    m_iObjectId = iObjectId;
}

//##############################################################################

class cutfObjectIdListEvent : public cutfEvent
{
public:
    cutfObjectIdListEvent();
	cutfObjectIdListEvent( atArray<s32> &iObjectIdList, float fTime, s32 iEventId, bool IsChild, u32 StickyId, cutfEventArgs* pEventArgs=NULL );
    cutfObjectIdListEvent( atArray<s32> &iObjectIdList, float fTime, s32 iEventId, atArray<cutfEventArgs*> &eventArgsList );
	cutfObjectIdListEvent( atArray<s32> &iObjectIdList, float fTime, s32 iEventId, bool IsChild, u32 StickyId, atArray<cutfEventArgs*> &eventArgsList );
    virtual ~cutfObjectIdListEvent();

    // PURPOSE: Retrieves the type of this event for easy casting
    // RETURNS: An id representing the type
    virtual s32 GetType() const;

    // PURPOSE: Retrieves the type name of this data type.
    // RETURNS: The type name.
    virtual const char* GetTypeName() const;

    // PURPOSE: Retrieves the list of object ids that are the target of this event
    // RETURNS: The object ids.
    const atArray<s32>& GetObjectIdList() const;

    // PURPOSE: Retrieves the event args that go with each object id
    // RETURNS: The event args
    const atArray<cutfEventArgs *>& GetEventArgsList() const;

    // PURPOSE: Adds the object id and event args, if provided.
    // PARAMS:
    //    iObjectId - the object id to add
    //    pEventArgs - the object's event args
    // RETURNS:
    // NOTES:  If event args are provided, but the main Event Args have already been set,
    //    the object's event args will be ignored.
    void AddObjectId( s32 iObjectId, cutfEventArgs *pEventArgs=NULL );

    // PURPOSE: Removes the object id from the Object Id List.  If the object has event
    //    args, its reference counter will be decremented.  Returns the object's event args, 
    //    if any, so that they can be deleted.
    // PARAMS:
    //    iObjectId - the object id to remove
    // RETURNS: 
    cutfEventArgs* RemoveObjectId( s32 iObjectId );

    // PURPOSE: Creates a copy of this cut scene event
    // RETURNS: A clone of this instance
    virtual cutfEvent* Clone() const;

    // PURPOSE: Brings all the attributes in the 'other' event into this event
    // PARAMS:
    //		other - The event to merge into this one
    //		overwrite - If true, attributes in 'other' will overwrite ones that are in this event.
    virtual void MergeFrom( const cutfEvent &other, bool overwrite=true );

    virtual bool operator == ( const cutfEvent &other ) const;
    virtual bool operator != ( const cutfEvent &other ) const;

	void PreLoad(parTreeNode* node);
	void ConvertArgPointersToIndices();
	void ConvertArgIndicesToPointers();

    PAR_PARSABLE;

protected:
    atArray<s32> m_iObjectIdList;
    atArray<s32> m_iEventArgsIndexList;/* for serialization, don't use at runtime */
	atArray<cutfEventArgs*> m_pEventArgsPtrList;
};

inline s32 cutfObjectIdListEvent::GetType() const
{
    return CUTSCENE_OBJECT_ID_LIST_EVENT_TYPE;
}

inline const char* cutfObjectIdListEvent::GetTypeName() const
{
    return "Object Id List Event";
}

inline const atArray<s32>& cutfObjectIdListEvent::GetObjectIdList() const
{
    return m_iObjectIdList;
}

inline const atArray<cutfEventArgs*>& cutfObjectIdListEvent::GetEventArgsList() const
{
    return m_pEventArgsPtrList;
}

//##############################################################################

} // namespace rage

#endif // CUTFILE_CUTFEVENT_H 
