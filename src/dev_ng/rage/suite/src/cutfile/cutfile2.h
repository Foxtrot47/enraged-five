// 
// cutfile/cutfile2.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTFILE_CUTFILE2_H 
#define CUTFILE_CUTFILE2_H 

#include "cutfdefines.h"

#include "atl/array.h"
#include "atl/binmap.h"
#include "atl/map.h"
#include "atl/string.h"
#include "fwutil/Flags.h"
#include "parser/macros.h"
#include "parsercore/attribute.h"
#include "paging/base.h"
#include "string/stringhash.h"
#include "vector/color32.h"
#include "vector/vector3.h"
#include "string/stringutil.h"

namespace rage {

class atUserBitSet;
class cutfCutsceneFile;
class cutfObject;
class cutfEvent;
class cutfEventArgs;
class parTree;
class cutfAttributeList;
class cutfAttribute;

#if __BANK || __TOOL
class CutfileNonParseData
{
public:
	CutfileNonParseData() {}; 


	static fwFlags32 m_FileTuningFlags; 
	static fwFlags32 m_FileTuningStatusFlags; 
	static fwFlags32 m_FileGameDataTuningFlags; 

	enum ECutsceneTuningFiles
	{
		CUTSCENE_LIGHT_TUNING_FILE = BIT0,
		CUTSCENE_SUBTITLE_TUNING_FILE = BIT1,
		CUTSCENE_GAMEDATA_TUNING_FILE = BIT2
	};

	enum ECutsceneTuningLoadStatus
	{
		CUTSCENE_LIGHT_TUNING_FILE_LOADED = BIT0,
		CUTSCENE_SUBTITLE_TUNING_FILE_LOADED = BIT1,
		CUTSCENE_GAMEDATA_TUNING_FILE_LOADED = BIT2
	};

	enum EGameDataTuningFlags
	{
		GAMEDATA_TUNE_FIRST_PERSON_BLENDOUT_EVENTS = BIT0
	};


};
#endif //__BANK || __TOOL


class cutfCutsceneFile2 : public datBase
{ 
public: 
    enum ECutsceneFlags
    {
        // Fade flags for the editor
        CUTSCENE_FADE_IN_GAME_FLAG,             // Set when we should fade in the game when the cut scene has ended
        CUTSCENE_FADE_OUT_GAME_FLAG,            // Set when we should fade the game out before starting playback
        CUTSCENE_FADE_IN_FLAG,                  // Set when we should fade in the cut scene when we start playback
        CUTSCENE_FADE_OUT_FLAG,                 // Set when we should fade out the cut scene when nearing the end
        CUTSCENE_SHORT_FADE_OUT_FLAG,           // Set when we should do a shorter than normal fade out when nearing the end
        CUTSCENE_LONG_FADE_OUT_FLAG,            // Set when we should do a longer than normal fade out when nearing the end
        CUTSCENE_FADE_BETWEEN_SECTIONS_FLAG,    // Set when we should fade in/out between sections

        // Lights
        CUTSCENE_NO_AMBIENT_LIGHTS_FLAG,        // Set when the ambient lights should be turned off for the duration of the cut scene
        CUTSCENE_NO_VEHICLE_LIGHTS_FLAG,        // Set when the vehicle lights should be turned off for the duration of the cut scene

		// Audio
        CUTSCENE_USE_ONE_AUDIO_FLAG,            // Set when planning to concatenate cutscenes and there will be only 1 audio track for the entire scene
        CUTSCENE_MUTE_MUSIC_PLAYER_FLAG,        // Set when game music should be muted for the entire cut scene
        CUTSCENE_LEAK_RADIO_FLAG,               // Set when it is ok for the game music (from the radio) can play through the cut scene

        // Misc editor flags
        CUTSCENE_TRANSLATE_BONE_IDS_FLAG,       // Set when the bone ids need to be translated using the spec file during export  
        CUTSCENE_INTERP_CAMERA_FLAG,            // Set when we should interpolate between camera cuts during export

        // Sectioning flags
        CUTSCENE_IS_SECTIONED_FLAG,             // Indicates when the data contained in the file (and the animations) have been sectioned
        CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG,   // Set when we should segment based on the camera cuts
        CUTSCENE_SECTION_BY_DURATION_FLAG,      // Set when we should segment based on a specified duration for each section
        CUTSCENE_SECTION_BY_SPLIT_FLAG,         // Set when we should segment on the camera's SectionSplit attribute keys

        CUTSCENE_USE_PARENT_SCALE_FLAG,         // Set when exported models should factor in their parent node's scale

        CUTSCENE_USE_ONE_SCENE_ORIENTATION_FLAG,// Set when planning to concatenate cutscenes and we should use only the first scene's orientation

        CUTSCENE_ENABLE_DEPTH_OF_FIELD_FLAG,    // Set when the depth of field camera settings are ready for prime time

		CUTSCENE_STREAM_PROCESSED,

		CUTSCENE_USE_STORY_MODE_FLAG,

		CUTSCENE_USE_IN_GAME_DOF_START_FLAG,
		CUTSCENE_USE_IN_GAME_DOF_END_FLAG,

		CUTSCENE_USE_CATCHUP_CAMERA_FLAG,
		CUTSCENE_USE_BLENDOUT_CAMERA_FLAG,
		
		CUTSCENE_PART_FLAG,
		CUTSCENE_INTERNAL_CONCAT_FLAG,
		CUTSCENE_EXTERNAL_CONCAT_FLAG,
		CUTSCENE_USE_AUDIO_EVENTS_CONCAT_FLAG,
		CUTSCENE_USE_IN_GAME_DOF_START_SECOND_CUT_FLAG,

        CUTSCENE_NUM_FLAGS,                     // Custom flags should start here
    };

	struct SDiscardedFrameData
	{
		SDiscardedFrameData()
		{
		}

		SDiscardedFrameData(atHashString SceneName)
		{
			cSceneName = SceneName;
		}

		SDiscardedFrameData(atHashString SceneName, const atArray<int>& frames_)
		{
			cSceneName = SceneName;
			frames = frames_;
		}

		atHashString cSceneName;
		atArray<int> frames;
		PAR_SIMPLE_PARSABLE;
	};

    struct SConcatData
    {
        SConcatData()
            : fStartTime(0.0f)
            , fRotation(0.0f)
			, fPitch(0.0f)
			, fRoll(0.0f)
			, iRangeStart(0)
			, iRangeEnd(0)
			, bValidForPlayBack(true)
        {
            vOffset.Zero();
        }

        SConcatData( atHashString SceneName, float startTime, const Vector3 &offset, float rotation, float pitch, float roll, int rangeStart, int rangeEnd )
            : fStartTime(startTime)
            , vOffset(offset)
            , fRotation(rotation)
			, fPitch(pitch)
			, fRoll(roll)
			, iRangeStart(rangeStart)
			, iRangeEnd(rangeEnd)
			, bValidForPlayBack(true)
        {
            cSceneName = SceneName;
        }

        atHashString cSceneName;
        Vector3 vOffset;
        float fStartTime;
        float fRotation;
		float fPitch; 
		float fRoll;
		int iRangeStart;
		int iRangeEnd;
		bool bValidForPlayBack; 
        PAR_SIMPLE_PARSABLE;
    };

    cutfCutsceneFile2();
    virtual ~cutfCutsceneFile2(); 

    // PURPOSE: Clears all data associated with this cut scene file, deleting all allocated structures and classes.
    void Clear();

#if __BANK || __TOOL
    // PURPOSE: Save the data to the cut file.
    // PARAMS:
    //    pFilename - The name of the file to save to.
    //    pExtension - the extension to save as. 
    // RETURNS: true on success, otherwise false.
    // NOTES: Anything with "xml" or "bin" will save using the PARSER.  If no extension is provided, the default PI_CUTSCENE_XMLFILE_EXT is used.
    bool SaveFile( const char* pFilename, const char* pExtension=NULL );
#endif // __BANK || __TOOL

    // PURPOSE: Load the data from the cut file.
    // PARAMS:
    //    pFilename - The name of the file to load.
    //    pExtension - the extension to load as.
    // RETURNS: true on success, otherwise false.
    // NOTES: Anything with "xml" or "bin" will load using the PARSER.  If no extension is provided, the default PI_CUTSCENE_XMLFILE_EXT is used.
    bool LoadFile( const char* pFilename, const char* pExtension=NULL );

	// PURPOSE: Allow externally loaded cut file to be initialised.
	// PARAMS:
	//    pFilename - Scene name without extenstions
	//    pExtension - Sets the scene is loaded.
	// RETURNS: 
	// NOTES: Assumes that the file is already loaded. 
	void InitExternallyLoadedFile(const char* sceneName, bool IsLoaded); 

    // PURPOSE: Parses the data from the buffer.
    // PARAMS:
    //    pData - the data buffer to parse
    //    size - the length of data
    // RETURNS: true on success, otherwise false
    // NOTES: LoadFile executes this function, which is here for your convenience if you have an alternate method of loading files.
    bool ParseData( const char* pData, u32 size );

    // PURPOSE: Parse the data from the xml tree
    // PARAMS:
    //    pTree - the tree to parse
    // RETURNS: true on success, otherwise false.
    bool ParseXml( parTree *pTree );

#if __BANK || __TOOL
    // PURPOSE: Saves the current data as the tune data to the tune file for the currently loaded scene
    // RETURNS: true on success, otherwise false.
    bool SaveTuneFile();

    // PURPOSE: Saves the current data as the tune data to the give file
    // PARAMS:
    //    pFilename - the filename of the tune file.  The extension will be forced to PI_CUTSCENE_TUNEFILE_EXT.
    // RETURNS: true on success, otherwise false.
    bool SaveTuneFile( const char *pFilename );

	// PURPOSE: Loads the tune data for the current scene.
	// RETURNS: true on success, otherwise false.
	bool LoadGameDataTuneFile();

	// PURPOSE: Loads the tune data from the given file.
	// PARAMS:
	//    pFilename - the filename of the tune file.
	// RETURNS: true on success, otherwise false.
	bool LoadGameDataTuneFile( const char *pFilename );

	// PURPOSE: Parse the data from the xml tree
	// PARAMS:
	//    pTree - the tree to parse
	// RETURNS: true on success, otherwise false.
	bool ParseGameDataTuneXml( parTree *pTree );

	// PURPOSE: Loads the light data for the current scene.
	// RETURNS: true on success, otherwise false.
	bool LoadAndReplaceLightsInCutfile(const char* sceneName);

	// PURPOSE: Loads the light data from the given file.
	// PARAMS:
	//    pFilename - the filename of the subtitle file.
	// RETURNS: true on success, otherwise false.
	bool LoadAndParseLightData( const char *pFilename, bool PreserveData = false);

	atString GetAssetPathForScene(const atString& sceneName, bool includeFileName = true); 


    // PURPOSE: Loads the tune data for the current scene.
    // RETURNS: true on success, otherwise false.
    bool LoadTuneFile();

    // PURPOSE: Loads the tune data from the given file.
    // PARAMS:
    //    pFilename - the filename of the tune file.
    // RETURNS: true on success, otherwise false.
    bool LoadTuneFile( const char *pFilename );

    // PURPOSE: Parse the data from the xml tree
    // PARAMS:
    //    pTree - the tree to parse
    // RETURNS: true on success, otherwise false.
    bool ParseTuneXml( parTree *pTree );

    // PURPOSE: Loads the subtitle data for the current scene.
    // RETURNS: true on success, otherwise false.
    bool LoadSubtitleFile();

    // PURPOSE: Loads the subtitle data from the given file.
    // PARAMS:
    //    pFilename - the filename of the subtitle file.
    // RETURNS: true on success, otherwise false.
    bool LoadSubtitleFile( const char *pFilename );

    // PURPOSE: Parse the data from the xml tree
    // PARAMS:
    //    pTree - the tree to parse
    // RETURNS: true on success, otherwise false.
    bool ParseSubtitleXml( parTree *pTree );

	bool LoadMaxLightFile(const char* sceneName); 

	// PURPOSE: Parse the data from the xml tree
	// PARAMS:
	//    pTree - the tree to parse
	// RETURNS: true on success, otherwise false.
	bool ParseLightXml( parTree *pTree );

#endif // __BANK || __TOOL

    // PURPOSE: Indicates if the current data in this class was loaded from a file.
    // RETURNS: true if a file was loaded successfully
    bool IsLoaded() const;

    // PURPOSE: Retrieves the name of the scene.  This is always the name of the file that was loaded
    //    by the call to LoadFile or saved by the call to SaveFile
    // PARAMS: The scene name.
    const char* GetSceneName() const;

	// PURPOSE: Retrieves the real name of the scene. This is retrieved from the face dir, the GetSceneName() will
	//	  only return the name of the cutxml file.
	const atString GetRealSceneName() const;

    // PURPOSE: Sets the name of the scene.
    // PARAMS:
    //    pSceneName - the name of the scene.  Typically, the file name without the extension.
    // NOTES:  Normally, the scene name is set by LoadFile(...) and SaveFile(...) but certain situations, 
    //    such as loading from a memory stream, may require the user to fix the name with this function manually.
    void SetSceneName( const char *pSceneName );

    // PURPOSE: Retrieves the total length of the cut scene.
    // RETURNS: The total length of the cut scene in seconds
    float GetTotalDuration() const;

    // PURPOSE: Sets the total length of the cut scene.
    // PARAMS:
    //    fTotalDuration - the total duration in seconds
    void SetTotalDuration( float fTotalDuration );

    // PURPOSE: Retrieve the directory where the unresourced face animations live.
    // RETURNS: The face directory.
    // NOTES: The only time this is used is when the run-time wants to load face
    //  animations to create dual animations on the fly.
    const char* GetFaceDirectory() const;

    // PURPOSE: Set the directory where the unresourced face animations live.
    // PARAMS:
    //    pFaceDir - the face directory
    void SetFaceDirectory( const char* pFaceDir );

    // PURPOSE: Retrieve the bit set that contains the cut scene flags
    // RETURNS: The bit set for cut scene flags
    const atBitSet& GetCutsceneFlags() const;

    // PURPOSE: Retrieve the world translation offset where the cut scene section takes place.
    // RETURNS: The world translation offset Vector3
    const Vector3& GetOffset() const;

    // PURPOSE: Set the world translation offset where the cut scene section takes place.
    // PARAMS:
    //    offset - the world translation vector
    void SetOffset( const Vector3 &offset );

	// PURPOSE: Retrieve the world translation offset where the cut scene section takes place.
	// RETURNS: The world translation offset Vector3
	const Vector3& GetTriggerOffset() const;

	// PURPOSE: Set the world translation offset where the cut scene section takes place.
	// PARAMS:
	//    offset - the world translation vector
	void SetTriggerOffset( const Vector3 &offset );

    // PURPOSE: Retrieve the correction rotation of this cut scene section.
    // RETURNS: The degrees rotation
    float GetRotation() const;

    // PURPOSE: Set the correction rotation of this cut scene section.
    // PARAMS:
    //    fRotation - the degrees rotation
    void SetRotation( float fRotation );

	// PURPOSE: Retrieve the correction rotation of this cut scene section.
	// RETURNS: The degrees rotation
	float GetPitch() const;

	// PURPOSE: Set the correction rotation of this cut scene section.
	// PARAMS:
	//    fRotation - the degrees rotation
	void SetPitch( float fPitch );

	// PURPOSE: Retrieve the correction rotation of this cut scene section.
	// RETURNS: The degrees rotation
	float GetRoll() const;

	// PURPOSE: Set the correction rotation of this cut scene section.
	// PARAMS:
	//    fRotation - the degrees rotation
	void SetRoll( float fRoll );

    // PURPOSE: Retrieves the starting frame number for export from the DCC package.
    // RETURNS: The starting frame
    s32 GetRangeStart() const;

    // PURPOSE: Sets the starting frame number for export from the DCC package.
    // PARAMS:
    //    iRangeStart - the starting frame
    void SetRangeStart( s32 iRangeStart );

	// PURPOSE: Sets the ending frame number for export from the DCC package.
	// PARAMS:
	//    iRangeStart - the starting frame
	void SetRangeEnd( s32 iRangeEnd );

    // PURPOSE: Retrieves the ending frame number for export from the DCC package.
    // RETURNS: The ending frame
    s32 GetRangeEnd() const;

	// PURPOSE: Sets the ending frame number for export from the DCC package. This will be the last known end range when exporting normally.
	// PARAMS:
	//    iRangeEnd - the ending frame
	void SetAltRangeEnd( s32 iRangeEnd );

	// PURPOSE: Retrieves the ending frame number for export from the DCC package. This will be the last known end range when exporting normally.
	// RETURNS: The ending frame
	s32 GetAltRangeEnd() const;

    // PURPOSE: Retrieves the duration of each section when the sectioning method is by time.
    // RETURNS: The section duration in seconds.
    // NOTES: This is only used if the cut scene flag CUTSCENE_SECTION_BY_DURATION_FLAG is set.
    //    The last section will be equal to or less than this length of time.
    float GetSectionByTimeSliceDuration() const;

	int GetSectionMethod();

    // PURPOSE: Sets the duration of each section when the sectioning method is by time.
    // PARAMS:
    //    fDuration - the duration in seconds
    // NOTES: The last section will be equal to or less than this length of time.
    void SetSectionByTimeSliceDuration( float fDuration );

    // PURPOSE: Retrieves the duration of the fade in game at the end of the scene.
    // RETURNS: The duration in seconds
    // NOTES: The fade will only be executed if the cut scene flag CUTSCENE_FADE_IN_GAME_FLAG is set
    float GetFadeInGameAtEndDuration() const;
    
    // PURPOSE: Sets the duration of the fade in game at the end of the scene.
    // PARAMS:
    //    fDuration - The duration in seconds
    void SetFadeInGameAtEndDuration( float fDuration );

    // PURPOSE: Retrieves the duration of the fade out cutscene at the end of the scene.
    // RETURNS: The duration in seconds
    // NOTES: The fade will only be executed if the cut scene flag CUTSCENE_FADE_OUT_FLAG is set
    float GetFadeOutCutsceneAtEndDuration() const;

    // PURPOSE: Sets the duration of the fade out cutscene at the end of the scene.
    // PARAMS:
    //    fDuration - The duration in seconds
    void SetFadeOutCutsceneAtEndDuration( float fDuration );

    // PURPOSE: Retrieves the color that the fade in game fades from at the end of the scene.
    // RETURNS: the fade color
    const Color32& GetFadeAtEndColor() const;

    // PURPOSE: Sets the color that the fade in game fades from at the end of the scene.
    // PARAMS:
    //    color - the fade color
    void SetFadeAtEndColor( const Color32 &color );

    // PURPOSE: Retrieves the duration of the fade out game at the beginning of the scene.
    // RETURNS: The duration in seconds
    // NOTES: The fade will only be executed if the cut scene flag CUTSCENE_FADE_OUT_GAME_FLAG is set
    float GetFadeOutGameAtBeginningDuration() const;

    // PURPOSE: Sets the duration of the fade out game at the beginning of the scene.
    // PARAMS:
    //    fDuration - The duration in seconds
    void SetFadeOutGameAtBeginningDuration( float fDuration );

    // PURPOSE: Retrieves the duration of the fade in cutscene at the beginning of the scene.
    // RETURNS: The duration in seconds
    // NOTES: The fade will only be executed if the cut scene flag CUTSCENE_FADE_OUT_GAME_FLAG is set
    float GetFadeInCutsceneAtBeginningDuration() const;

    // PURPOSE: Sets the duration of the fade in cutscene at the beginning of the scene.
    // PARAMS:
    //    fDuration - The duration in seconds
    void SetFadeInCutsceneAtBeginningDuration( float fDuration );

	// PURPOSE: Sets the duration of the blend out game at the end of the scene.
	// PARAMS:
	//    fDuration - The duration in seconds
	void SetBlendOutCutsceneDuration( int iDuration );

	// PURPOSE: Retrieves the duration of the blend in cutscene at the end of the scene.
	// RETURNS: The duration in frames
	int GetBlendOutCutsceneDuration() const;

	// PURPOSE: Sets the offset of the blend out game at the end of the scene.
	// PARAMS:
	//    fOffset - The offset in frames
	void SetBlendOutCutsceneOffset( int iOffset );

	// PURPOSE: Retrieves the offset of the blend in cutscene at the end of the scene.
	// RETURNS: The offset in seconds
	int GetBlendOutCutsceneOffset() const;

    // PURPOSE: Retrieves the color that the fade out game fades to at the beginning of the scene.
    // RETURNS: the fade color
    const Color32& GetFadeAtBeginningColor() const;

    // PURPOSE: Sets the color that the fade out game fades to at the beginning of the scene.
    // PARAMS:
    //    color - the fade color
    void SetFadeAtBeginningColor( const Color32 &color );

	void SetDayCoCHours(u32 DayTimes) { m_DayCoCHours = DayTimes; }

	u32 GetDayCoCHours() const { return m_DayCoCHours; }

    // PURPOSE: Gets the attribute list for the scene
    // RETURNS: The attribute list
    const parAttributeList& GetAttributeList() const;

    // PURPOSE: Retrieves the entire list of cut scene objects
    // RETURNS: The list of objects
    const atArray<cutfObject *>& GetObjectList() const;

    // PURPOSE: Retrieves the list of cut scene load events
    // RETURNS: The list of load events
    const atArray<cutfEvent *>& GetLoadEventList() const;

    // PURPOSE: Retrieves the list of cut scene events
    // RETURNS: The list of events
    const atArray<cutfEvent *>& GetEventList() const;

    // PURPOSE: Retrieves the list of cut scene events args
    // RETURNS: The list of event args
    const atArray<cutfEventArgs *>& GetEventArgsList() const;

    // PURPOSE: Adds the object to the Object list.
    // PARAMS:
    //    pObject - the object to add
    // NOTES: The object's id will be changed to match it's position in the list.
    void AddObject( cutfObject *pObject );

    // PURPOSE: Removes the object from the list and deletes it.  Also removes
    //    all of its events and event args.
    // PARAMS:
    //    pObject - the object to remove
    // NOTES: The remaining objects' ids will be updated so that they match
    //    their positions in the list.
    void RemoveObject( const cutfObject *pObject );

    // PURPOSE: Adds the event to the Load Event List.  If there are EventArgs
    //    and they are not already in the Event Args List, it will be added.
    // PARAMS:
    //    pEvent - the event to add
    void AddLoadEvent( cutfEvent *pEvent );

    // PURPOSE: Adds the event to the Event List.  If there are EventArgs
    //    and they are not already in the Event Args List, it will be added.
    // PARAMS:
    //    pEvent - the event to add
    void AddEvent( cutfEvent *pEvent );

    // PURPOSE: Removes the event from the Load Event List and deletes it.
    //    If there are EventArgs, and there are no more references to it,
    //    they will be removed from the Event Args List and deleted as well.
    // PARAMS:
    //    pEvent - the event to remove
    void RemoveLoadEvent( cutfEvent *pEvent );

    // PURPOSE: Removes the event from the Event List and deletes it.
    //    If there are EventArgs, and there are no more references to it,
    //    they will be removed from the Event Args List and deleted as well.
    // PARAMS:
    //    pEvent - the event to remove
    void RemoveEvent( cutfEvent *pEvent );

    // PURPOSE: Adds an event args to the list if it isn't in there already.
    // PARAMS:
    //    pEventArgs - the event args to add.
    void AddEventArgs( cutfEventArgs *pEventArgs );

	// PURPOSE: Adds an event and its args to the list if it isn't in there already.
	// NOTE: This is an exporter only function, it assumes the lists contain pointers as we need to do an equality check on them
	//		we cannot use the original functions as they use Find() and this doesnt work with pointers as we need to compare the de-reffed instances.
	// PARAMS:
	//    pEvent - the event to add.
	void Exporter_AddEventAndArgs( cutfEvent* pEvent );

    // PURPOSE: Removes and deletes the event args if its reference count is zero.
    // PARAMS:
    //    pEventArgs - the event args to remove.
    void RemoveEventArgs( cutfEventArgs *pEventArgs );

    // PURPOSE: Retrieves the list of times on which a camera cut occurs.
    // RETURNS: A list of times in seconds.
    const atArray<float>& GetCameraCutList() const;
    
    // PURPOSE: Retrieves the list of times on which the SectionSplit property has key-frames
    // RETURNS: A list of times in seconds.
    const atArray<float>& GetSectionSplitList() const;

	const atArray<SDiscardedFrameData>& GetDiscardedFrameList() const;

    // PURPOSE: Retrieves the list of concat data
    // RETURNS: The list of concat data
    const atFixedArray<SConcatData, CUTSCENE_MAX_CONCAT>& GetConcatDataList() const;

    // PURPOSE: Fills the array with the times that a section split occurs, based on the current settings.
    //    Includes 0.0f for the first section.
    // PARAMS:
    //    sectionTimeList - the list to fill
    // NOTES: The list will be empty if no splitting has occurred.
    void GetCurrentSectionTimeList( atArray<float> &sectionTimeList ) const;

    // PURPOSE: Fills the array with the concat data, based on the current settings.  Includes an entry for
    //    the first section at time 0.
    // PARAMS:
    //    concatDataList - the list to fill
    // NOTES: The list will be empty if no concatenation has occurred.
    void GetCurrentConcatDataList( atFixedArray<SConcatData, CUTSCENE_MAX_CONCAT> &concatDataList ) const;

    // PURPOSE: Finds all objects with the given type id
    // PARAMS:
    //    iObjectType - the type id to locate
    //    pObjectList - the list to populate
    void FindObjectsOfType( s32 iObjectType, atArray<cutfObject*> &pObjectList ) const;
	
	// PURPOSE: Finds all objects with the given type id
	// PARAMS:
	//    iObjectType - the type id to locate
	//    pObjectList - the list to populate
	void FindObjectsOfType( s32 iObjectType, atArray<const cutfObject*> &pObjectList ) const;

    // PURPOSE: Finds all events in the list that are dispatched to the given object id as part of a cutfObjectIdEvent.
    // PARAMS:
    //    iObjectId - the object id to locate
    //    pEventList - the list of events to search
    //    pObjectEventList - the list to populate
    void FindEventsForObjectIdOnly( s32 iObjectId, const atArray<cutfEvent *>& pEventList, atArray<cutfEvent *>& pObjectEventList, bool bSearchEventArgs=false ) const;

    // PURPOSE: Finds all events in the list that are dispatched to the given object id as part of a cutfObjectIdListEvent.
    // PARAMS:
    //    iObjectId - the object id to locate
    //    pEventList - the list of events to search
    //    pObjectEventList - the list to populate
    void FindEventsForObjectId( s32 iObjectId, const atArray<cutfEvent *>& pEventList, atArray<cutfEvent *>& pObjectEventList ) const;

	// PURPOSE: Finds all events in the list that are dispatched to the given object id as part of a cutfObjectIdListEvent.
	// PARAMS:
	//    iObjectId - the object id to locate
	//    pEventList - the list of events to search
	//    pObjectEventList - the list to populate
	//	  bSearchForSpecifcArgs - check for specific event args
	//	  eventargs- event args you want to check against 
	void FindObjectIdEventsForObjectIdOnly(  s32 iObjectId, const atArray<cutfEvent *>& pEventList, 
		atArray<cutfEvent *>& pObjectEventList, bool bSearchForSpecifArgs, s32 eventArgsType  ) const;

    // PURPOSE: Sorts the load events based on time and event type.
    // NOTES:  This will be executed by Upgrade(), and possibly by rexMBRage.
    virtual void SortLoadEvents();

    // PURPOSE: Sorts the events based on time and event type.
    // NOTES:  This will be executed by Upgrade(), and possibly by rexMBRage.
    virtual void SortEvents();

	virtual void SortObjects();

    // PURPOSE: Retrieves the number of cutfEventArgs whose ref count is zero.
    // PARAMS:
    //    eventArgsList - the list to fill with the unused args.
    void GetUnusedEventArgs( atArray<cutfEventArgs *> &eventArgsList ) const;

    // PURPOSE: Deletes all cutfEventArgs whose ref count is zero.
    void CleanupUnusedEventArgs();

    // PURPOSE: Clones this instance
    // RETURNS: A clone of this instance
    virtual cutfCutsceneFile2* Clone() const;

#if  !__FINAL
    // PURPOSE: Merges the 'other' cutfile on the end of this one.
    // PARAMS:
    //    other - the cutfile to append to this one
    //	  overwrite - If true, attributes in 'other' will overwrite ones that are in this event.
    // NOTES: The properties of this cutfile take precedence over the other cutfile except 
    //    where the end is concerned (fade out cutscene, fade in game, range end, etc.).
    //    Animation names in the new cutfile will be appended with "A", "B", etc. to differentiate
    //    them from their sources.
    virtual void Concat( const cutfCutsceneFile2 &other, float previousDuration, bool overwrite=true );

    // PURPOSE: Merges the 'other' cutfile on top of this one.
    // PARAMS:
    //    other - the cutfile to merge onto this one
    //	  overwrite - If true, attributes in 'other' will overwrite ones that are in this event.
    // NOTES: The properties of this cutfile take precedence over the other cutfile except 
    //    where the end is concerned (fade out cutscene, fade in game, range end, etc.).
    //    Animation names in the new cutfile will be appended with "A", "B", etc. to differentiate
    //    them from their sources.
    // NOTES:  This function can and will fall over if some objects have the same name.  Intended for situations where there is no overlap
    //    between the two files.
    virtual void MergeFrom( const cutfCutsceneFile2 &other, bool overwrite=true );
#endif
    // PURPOSE: Using the current cutfile, returns the index (as a string) cutfEventArgs in the EventArgsList.
    // PARAMS:
    //    pEventArgs - the referenced cutfEventArgs.
    // RETURNS: The string index.  "-1" if there were none or the index could not be determined.
	static int GetEventArgsIndex( cutfEventArgs *pEventArgs );

    // PURPOSE: Using the current cutfile, returns the cutfEventArgs that that string represents.
    // PARAMS:
    //    pEventArgsIndexString - the index (as a string) of the event that owns the referenced
    //      cutfEventArgs.
    // RETURNS: The cutfEventArgs.  NULL if there were none, or it wasn't found.
	static cutfEventArgs* FindEventArgs( int iEventArgsIndex );

    // PURPOSE: Upgrades the Draw Distance Events, putting the clipping planes in the Camera Cut Event.
    // RETURNS: true on success, otherwise false.
    bool UpgradeDrawDistances();

	// PURPOSE: Changes all event recipients from the old ObjectId to the new one
	// PARAMS:
	//    oldToNewObjectId - the map of old to new ObjectIds
	//    eventList - the event list to search
	//	  bChangeEventArgs - if oldToNewObjectId contains swaps then set this to false and change the object ids on the args seperate
	//						otherwise all kinds of badness happens
	void ChangeObjectIds( const atMap<int, int> &oldToNewObjectId, const atArray<cutfEvent *> &eventList, bool bChangeEventArgs=true );

	// PURPOSE: Changes all eventarg recipients from the old ObjectId to the new one
	// PARAMS:
	//    oldToNewObjectId - the map of old to new ObjectIds
	//    eventArgList - the eventarg list to search
	void ChangeObjectIds( const atMap<int, int> &oldToNewObjectId, const atArray<cutfEventArgs *> &eventArgList );

	// PURPOSE: Removes the object (or rather, its id) from all events args referenced by
	//    events in the event list.
	// PARAMS:
	//    iObjectId - the object id to remove
	//    eventList - the event list to remove the object id from
	void RemoveObjectFromEventList( s32 iObjectId, atArray<cutfEvent *> &eventList );

	void ConvertArgIndicesToPointers();
	void ConvertArgPointersToIndices();

	static void ConvertToParAttributeList(cutfAttributeList*& from, parAttributeList& to);
	static void ConvertToCutfAttributeList(parAttributeList& from, cutfAttributeList*& to);

    PAR_PARSABLE; 

protected: 
    // PURPOSE: Returns the number of cut scene flags.
    // RETURNS: The number of cut scene flags.
    // NOTES: Override this if you define your own custom flags.
    virtual u16 GetCutsceneFlagsCount() const;

    // PURPOSE: The event cloning process does not involve the event args, so we must fixup
    //    the cloned events by making them point to the manually closed event args.
    // PARAMS:
    //    clonedEventList - the list of cloned events
    //    clonedEventArgsList - the list of cloned event args
    void FixupClonedEventArgs( atArray<cutfEvent *>& clonedEventList, const atArray<cutfEventArgs *> &clonedEventArgsList ) const;
  
	static int ObjectsComparer( cutfObject* const* ppObject1, cutfObject* const* ppObject2 );

    // PURPOSE: Standard comparer function for sorting the events
    // PARAMS:
    //    pEvent1
    //    pEvent2
    // RETURNS: 0 if equal. -1 if pEvent1 should come before pEvent2. 1, otherwise.
    static int EventsComparer( cutfEvent* ppEvent1, cutfEvent* ppEvent2 );

	// PURPOSE: Standard comparer function for sorting the events
	// PARAMS:
	//    pEvent1
	//    pEvent2
	// RETURNS: 0 if equal. -1 if pEvent1 should come before pEvent2. 1, otherwise.
	static int LoadEventsComparer( cutfEvent* ppEvent1, cutfEvent* ppEvent2 );
    
    // PURPOSE: Adds the event to the specified list.  If the event has event args, it
    //    are added to the Event Args List if it is not there already.
    // PARAMS:
    //    pEvent - the event to add
    //    eventList - the list to add to
    void AddEventToList( cutfEvent *pEvent, atArray<cutfEvent *> &eventList );

    // PURPOSE: Removes the event from the specified list and deletes it.  If the event has 
    //    event args, and its reference count is zero, the event args will be removed and
    //    deleted, too.
    // PARAMS:
    //    pEvent - the event to remove
    //    eventList - the list to remove from
    void RemoveEventFromList( cutfEvent *pEvent, atArray<cutfEvent *> &eventList );

    // PURPOSE: Removes the object (or rather, its id) from the event args.
    // PARAMS:
    //    iObjectId - the object id to remove
    //    pEventArgs - the event args to remove the object id from
    // RETURNS: non-NULL if the args can be deleted.
    cutfEventArgs* RemoveObjectFromEventArgs( s32 iObjectId, cutfEventArgs *pEventArgs );

    // PURPOSE: Ensures that each object's id is the same as its index in the Object List.
    void EnforceSequentialObjectIds();

    // PURPOSE: Changes the object's id and all of its references.
    // PARAMS:
    //    pObject - the object to change the id of
    //    iNewObjectId - the new id to give the object
    void ChangeObjectId( cutfObject *pObject, s32 iNewObjectId );

    // PURPOSE: Finds the events in the list that reference iObjectId, and changes them to iNewObjectId.
    // PARAMS:
    //    iObjectId - the id of the object to change
    //    iNewObjectId - the new id to give the object
    //    eventList - the event list to search
    void ChangeObjectId( s32 iObjectId, s32 iNewObjectId, const atArray<cutfEvent *> &eventList );

    // PURPOSE: If the event references iObjectId, changes it to iNewObjectId.
    // PARAMS:
    //    iObjectId - the id of the object to change
    //    iNewObjectId - the new id to give the object
    //    pEvent - the event to look at
    void ChangeObjectId( s32 iObjectId, s32 iNewObjectId, cutfEvent *pEvent );

    // PURPOSE: If the event args references iObjectId, changes it to iNewObjectId.
    // PARAMS:
    //    iObjectId - the id of the object to change
    //    iNewObjectId - the new id to give the object
    //    pEventArgs - the event args to look at
    void ChangeObjectId( s32 iObjectId, s32 iNewObjectId, cutfEventArgs *pEventArgs );

    // PURPOSE: If the event references an ObjectId in the map, changes it to the new ObjectId.
    // PARAMS:
    //    oldToNewObjectId - the map of old to new ObjectIds
    //    pEvent - the event to look at
	//	  bChangeEventArgs - if oldToNewObjectId contains swaps then set this to false and change the object ids on the args separate
	//						otherwise all kinds of badness happens
    void ChangeObjectIds( const atMap<int, int> &oldToNewObjectId, cutfEvent *pEvent, bool bChangeEventArgs=true );

    // PURPOSE: If the event args references an ObjectId in the map, changes it to the new ObjectId.
    // PARAMS:
    //    oldToNewObjectId - the map of old to new ObjectIds
    //    pEventArgs - the event args to look at
    void ChangeObjectIds( const atMap<int, int> &oldToNewObjectId, cutfEventArgs *pEventArgs );

    // PURPOSE: Clones the objects from the other cutfile.  Adds the new object Ids into the map for later translation
    // PARAMS: 
    //    other - the cutfile to clone the objects from
    //    oldToNewObjectId - the map of old to new object Ids
    //    overwrite - true to overwrite object attributes with those found in the other cutfile.
    void CloneObjects( const cutfCutsceneFile2 &other, atMap<int, int> &oldToNewObjectId, bool overwrite=true );

#if  !__FINAL
    // PURPOSE: Clones the events and load events from the other cutfile, applying a time offset to the events.
    // PARAMS: 
    //    other - the cutfile to clone the objects from
    //    oldToNewObjectId - the map of old to new object Ids
    //    originalObjectCount - the original number of objects (before CloneObjects was called)
    //    originalTotalDuration - the original duration of this cutscene
    //    bNeedToAddAudioData - out parameter indicating when we need to add an SAudioData struct.
    //    overwrite - true to overwrite event args attributes with those found in the other cutfile.
    // RETURNS: 
    // NOTES: 
    void CloneEvents( const cutfCutsceneFile2 &other, const atMap<int, int> &oldToNewObjectId, 
        int originalObjectCount, float originalTotalDuration, bool &bNeedToAddAudioData, float previousDuration, bool overwrite=true );
#endif

    // PURPOSE: Clones the events in the list, including their event args.  Adds originalTotalDuration to the dispatch times.  
    //    Changes all ObjectIds to the new ids using the provided map.
    // PARAMS:
    //    eventList - the events to clone.  The cloned events are placed back into this array.
    //    oldToNewObjectId - the map of old to new ObjectIds
    //    originalTotalDuration - the time to add to the cloned event dispatch times
    void CloneEventList( atArray<cutfEvent *> &eventList, const atMap<int, int> &oldToNewObjectId, float originalTotalDuration );

    bool m_bIsLoaded; 
    char m_cSceneName[256];
    float m_fTotalDuration;

    char m_cFaceDir[256];

    u32 m_iCutsceneFlags[4];
    mutable atUserBitSet* m_pCutsceneFlagsBitSet;

    Vector3 m_vOffset; 
    float m_fRotation; 
	float m_fPitch;
	float m_fRoll;

	Vector3 m_vTriggerOffset;  

    atArray<cutfObject *> m_pCutsceneObjects;

    atArray<cutfEvent *> m_pCutsceneLoadEventList; 
    atArray<cutfEvent *> m_pCutsceneEventList; 
    atArray<cutfEventArgs *> m_pCutsceneEventArgsList;

    parAttributeList m_attributes; 
	cutfAttributeList* m_cutfAttributes;

    s32 m_iRangeStart;
    s32 m_iRangeEnd;
	s32 m_iAltRangeEnd;
    float m_fSectionByTimeSliceDuration;
    
    float m_fFadeOutCutsceneDuration;
    float m_fFadeInGameDuration;
    Color32 m_fadeInColor;

	int m_iBlendOutCutsceneDuration;
	int m_iBlendOutCutsceneOffset;

    float m_fFadeOutGameDuration;
    float m_fFadeInCutsceneDuration;
    Color32 m_fadeOutColor;

	u32 m_DayCoCHours; 

    atArray<float> m_cameraCutList;
    atArray<float> m_sectionSplitList;
    atFixedArray<SConcatData, CUTSCENE_MAX_CONCAT> m_concatDataList;
	atArray<SDiscardedFrameData> m_discardFrameList;

private:
    // PURPOSE: This function is called by the parser on PreSave to set sm_pCurrentCutfile to this instance.
    void PreSave();

    // PURPOSE: This function is called by the parser on PostSave to clear sm_pCurrentCutfile.
    void PostSave( parTreeNode *pTreeNode );

    // PURPOSE: This function is called by the parser on PreLoad to set sm_pCurrentCutfile to this instance.
    void PreLoad( parTreeNode *pTreeNode );

    // PURPOSE: This function is called by the parser on PostLoad to clear sm_pCurrentCutfile.
    void PostLoad();
	
    static const cutfCutsceneFile2 *sm_pCurrentCutfile;  // for the cutfEventArgs referencing
}; 

/////////////////////////////////
// These classes are really similar to sveNode. Thought about moving those down into rage
// but it's not clear that they belong in rage yet. 
class cutfAttributeList
{
public:
	~cutfAttributeList();
	atArray<cutfAttribute*> m_Items;
	PAR_SIMPLE_PARSABLE;
};

class cutfAttribute : public datBase
{
public:
	enum Type { ATTR_NONE, ATTR_INT, ATTR_FLOAT, ATTR_BOOL, ATTR_STRING };
	virtual Type GetType() { return ATTR_NONE; }
	atFinalHashString m_Name;
	PAR_PARSABLE;
};

class cutfAttributeInt : public cutfAttribute
{
public:
	int m_Value;
	virtual Type GetType() { return ATTR_INT; }
	PAR_PARSABLE;
};

class cutfAttributeFloat : public cutfAttribute
{
public:
	float m_Value;
	virtual Type GetType() { return ATTR_FLOAT; }
	PAR_PARSABLE;
};

class cutfAttributeBool : public cutfAttribute
{
public:
	bool m_Value;
	virtual Type GetType() { return ATTR_BOOL; }
	PAR_PARSABLE;
};

class cutfAttributeString : public cutfAttribute
{
public:
	atString m_Value;
	virtual Type GetType() { return ATTR_STRING; }
	PAR_PARSABLE;
};

inline bool cutfCutsceneFile2::IsLoaded() const
{
    return m_bIsLoaded;
}

inline const char* cutfCutsceneFile2::GetSceneName() const
{
    return m_cSceneName;
}

inline void cutfCutsceneFile2::SetSceneName( const char *pSceneName )
{
    safecpy( m_cSceneName, pSceneName, sizeof(m_cSceneName) );
}

inline float cutfCutsceneFile2::GetTotalDuration() const
{
    return m_fTotalDuration;
}

inline void cutfCutsceneFile2::SetTotalDuration( float fTotalDuration )
{
    m_fTotalDuration = fTotalDuration;
}

inline const char* cutfCutsceneFile2::GetFaceDirectory() const
{
    return m_cFaceDir;
}

inline void cutfCutsceneFile2::SetFaceDirectory( const char* pFaceDir )
{
    if ( pFaceDir != NULL )
    {
        safecpy( m_cFaceDir, pFaceDir, sizeof(m_cFaceDir) );
    }
    else
    {
        m_cFaceDir[0] = 0;
    }
}

inline const atBitSet& cutfCutsceneFile2::GetCutsceneFlags() const
{
    if ( m_pCutsceneFlagsBitSet == NULL )
    {
        m_pCutsceneFlagsBitSet = rage_new atUserBitSet( const_cast<u32 *>( m_iCutsceneFlags ), GetCutsceneFlagsCount() );
    }

    return *m_pCutsceneFlagsBitSet;
}

inline const Vector3& cutfCutsceneFile2::GetOffset() const
{
    return m_vOffset;
}

inline void cutfCutsceneFile2::SetOffset( const Vector3 &offset )
{
    m_vOffset = offset;
}

inline const Vector3& cutfCutsceneFile2::GetTriggerOffset() const
{
	return m_vTriggerOffset;
}

inline void cutfCutsceneFile2::SetTriggerOffset( const Vector3 &offset )
{
	m_vTriggerOffset = offset;
}

inline float cutfCutsceneFile2::GetRotation() const
{
    return m_fRotation;
}

inline void cutfCutsceneFile2::SetRotation( float fRotation )
{
    m_fRotation = fRotation;
}

inline float cutfCutsceneFile2::GetPitch() const
{
	return m_fPitch;
}

inline void cutfCutsceneFile2::SetPitch( float pitch )
{
	m_fPitch = pitch;
}

inline float cutfCutsceneFile2::GetRoll() const
{
	return m_fRoll;
}

inline void cutfCutsceneFile2::SetRoll( float roll )
{
	m_fRoll = roll;
}


inline s32 cutfCutsceneFile2::GetRangeStart() const
{
    return m_iRangeStart;
}

inline void cutfCutsceneFile2::SetRangeStart( s32 iRangeStart )
{
    m_iRangeStart = iRangeStart;
}

inline s32 cutfCutsceneFile2::GetRangeEnd() const
{
    return m_iRangeEnd;
}

inline void cutfCutsceneFile2::SetAltRangeEnd( s32 iRangeEnd )
{
	m_iAltRangeEnd = iRangeEnd;
}

inline s32 cutfCutsceneFile2::GetAltRangeEnd() const
{
	return m_iAltRangeEnd;
}

inline void cutfCutsceneFile2::SetRangeEnd( s32 iRangeEnd )
{
    m_iRangeEnd = iRangeEnd;
}

inline float cutfCutsceneFile2::GetSectionByTimeSliceDuration() const
{
    return m_fSectionByTimeSliceDuration;
}

inline int cutfCutsceneFile2::GetSectionMethod()
{
	//if ( GetCutsceneFlags().IsSet( CUTSCENE_IS_SECTIONED_FLAG ) )
	{
		if ( GetCutsceneFlags().IsSet( CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG ) )
		{
			return CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG;
		}
		else if( GetCutsceneFlags().IsSet( CUTSCENE_SECTION_BY_DURATION_FLAG ) )
		{
			return CUTSCENE_SECTION_BY_DURATION_FLAG;
		}
		else if( GetCutsceneFlags().IsSet( CUTSCENE_SECTION_BY_SPLIT_FLAG ) )
		{
			return CUTSCENE_SECTION_BY_SPLIT_FLAG;
		}
	}

	return 0;
}

inline void cutfCutsceneFile2::SetSectionByTimeSliceDuration( float fDuration )
{
    m_fSectionByTimeSliceDuration = fDuration;
}

inline float cutfCutsceneFile2::GetFadeInGameAtEndDuration() const
{
    return m_fFadeInGameDuration;
}

inline void cutfCutsceneFile2::SetFadeInGameAtEndDuration( float fDuration )
{
    m_fFadeInGameDuration = fDuration;
}

inline float cutfCutsceneFile2::GetFadeOutCutsceneAtEndDuration() const
{
    return m_fFadeOutCutsceneDuration;
}

inline void cutfCutsceneFile2::SetFadeOutCutsceneAtEndDuration( float fDuration )
{
    m_fFadeOutCutsceneDuration = fDuration;
}

inline const Color32& cutfCutsceneFile2::GetFadeAtEndColor() const
{
    return m_fadeInColor;
}

inline void cutfCutsceneFile2::SetFadeAtEndColor( const Color32 &color )
{
    m_fadeInColor = color;
}

inline float cutfCutsceneFile2::GetFadeOutGameAtBeginningDuration() const
{
    return m_fFadeOutGameDuration;
}

inline void cutfCutsceneFile2::SetFadeOutGameAtBeginningDuration( float fDuration )
{
    m_fFadeOutGameDuration = fDuration;
}

inline float cutfCutsceneFile2::GetFadeInCutsceneAtBeginningDuration() const
{
    return m_fFadeInCutsceneDuration;
}

inline void cutfCutsceneFile2::SetFadeInCutsceneAtBeginningDuration( float fDuration )
{
    m_fFadeInCutsceneDuration = fDuration;
}

inline const Color32& cutfCutsceneFile2::GetFadeAtBeginningColor() const
{
    return m_fadeOutColor;
}

inline void cutfCutsceneFile2::SetFadeAtBeginningColor( const Color32 &color )
{
    m_fadeOutColor = color;
}



inline int cutfCutsceneFile2::GetBlendOutCutsceneDuration() const
{
	return m_iBlendOutCutsceneDuration;
}

inline void cutfCutsceneFile2::SetBlendOutCutsceneDuration( int iDuration )
{
	m_iBlendOutCutsceneDuration = iDuration;
}

inline int cutfCutsceneFile2::GetBlendOutCutsceneOffset() const
{
	return m_iBlendOutCutsceneOffset;
}

inline void cutfCutsceneFile2::SetBlendOutCutsceneOffset( int iOffset )
{
	m_iBlendOutCutsceneOffset = iOffset;
}


inline const parAttributeList& cutfCutsceneFile2::GetAttributeList() const
{
    return m_attributes;
}

inline const atArray<cutfObject *>& cutfCutsceneFile2::GetObjectList() const
{
    return m_pCutsceneObjects;
}

inline const atArray<cutfEvent *>& cutfCutsceneFile2::GetLoadEventList() const
{
    return m_pCutsceneLoadEventList;
}

inline const atArray<cutfEvent *>& cutfCutsceneFile2::GetEventList() const
{
    return m_pCutsceneEventList;
}

inline const atArray<cutfEventArgs *>& cutfCutsceneFile2::GetEventArgsList() const
{
    return m_pCutsceneEventArgsList;
}

inline void cutfCutsceneFile2::AddLoadEvent( cutfEvent *pEvent )
{
    AddEventToList( pEvent, m_pCutsceneLoadEventList );
}

inline void cutfCutsceneFile2::RemoveLoadEvent( cutfEvent *pEvent )
{
    RemoveEventFromList( pEvent, m_pCutsceneLoadEventList );
}

inline void cutfCutsceneFile2::AddEvent( cutfEvent *pEvent )
{
    AddEventToList( pEvent, m_pCutsceneEventList );
}

inline void cutfCutsceneFile2::RemoveEvent( cutfEvent *pEvent )
{
    RemoveEventFromList( pEvent, m_pCutsceneEventList );
}

inline const atArray<float>& cutfCutsceneFile2::GetCameraCutList() const
{
    return m_cameraCutList;
}

inline const atArray<cutfCutsceneFile2::SDiscardedFrameData>& cutfCutsceneFile2::GetDiscardedFrameList() const
{
	return m_discardFrameList;
}

inline const atArray<float>& cutfCutsceneFile2::GetSectionSplitList() const
{
    return m_sectionSplitList;
}

inline const atFixedArray<cutfCutsceneFile2::SConcatData, CUTSCENE_MAX_CONCAT>& cutfCutsceneFile2::GetConcatDataList() const
{
    return m_concatDataList;
}

inline u16 cutfCutsceneFile2::GetCutsceneFlagsCount() const
{
    return CUTSCENE_NUM_FLAGS;
}

//#############################################################################

} // namespace rage

#endif // CUTFILE_CUTFILE2_H 
