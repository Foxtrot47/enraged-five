<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
              generate="psoChecks">

  <const name="TEXT_KEY_SIZE" value="16"/>
  <const name="CUTSCENE_OBJNAMELEN" value="64"/>
  <const name="CUTSCENE_EFFECTNAMELEN * 2" value="100"/>
  <const name="CUTSCENE_EFFECTNAMELEN" value="50"/>
  <const name="MAX_EVOLUTION_EFFECT_VALUES" value="4"/>
  <const name="MAX_CUTSCENE_ENTITIES" value="70"/>
  <const name="MAX_CUTSCENE_VEHICLES" value="5"/>
  <const name="MAX_CUTSCENE_VEHICLE_REMOVALS" value="50"/>
  <const name="MAX_CUTSCENE_ATTACHMENTS" value="1"/>
  <const name="MAX_HIDDEN_OBJS" value="20"/>
  <const name="MAX_CUTSCENE_LIGHTS" value="10"/>
  <const name="CUTSCENE_NAME_LEN" value="24"/>
  <const name="MISSION_NAME_LENGTH" value="8"/>
  <const name="MAX_CUTSCENE_SECTIONS" value="15"/>
  <const name="MAX_DRAW_DISTANCES" value="100"/>
  <const name="MAX_CUTSCENE_EFFECTS" value="50"/>
  <const name="MAX_CUTSCENE_BLOCKING_BOUNDS" value="10"/>
  <const name="MAX_TEXTOUTPUT" value="500"/>
  <const name="MAX_CAMCORDER_OVERLAYS" value="30"/>
  <const name="MAX_FIXUP_OBJS" value="50"/>
  <const name="MAX_CUTSCENE_VARIATIONS" value="100"/>
  <const name="MAX_CUTSCENE_PROPS" value="50"/>
  <const name="MAX_EXTRA_ROOM_CHARS" value="24"/>

  <structdef type="::rage::SCamcorderOverlay" simple="true">
		<s32 name="iStartTime"/>
		<s32 name="iEndTime"/>
	</structdef>

	<structdef type="::rage::STextIdDetails" simple="true">
		<s32 name="m_iTextStartTime"/>
		<s32 name="m_iTextDuration"/>
		<string name="m_cTextOutput" type="member" size="TEXT_KEY_SIZE"/>
		<s32 name="iManualSectionCreatedIn"/>
	</structdef>

	<structdef type="::rage::SPropDetails" simple="true">
		<s32 name="parent"/>
		<s32 name="prop"/>
		<s32 name="anchor"/>
		<s32 name="iSectionCreatedIn"/>
	</structdef>

	<structdef type="::rage::SAttachmentDetails" simple="true">
		<s32 name="parent"/>
		<s32 name="child"/>
		<s32 name="boneid"/>
	</structdef>

	<structdef type="::rage::SVariationDetails" simple="true">
		<s32 name="iId"/>
		<s32 name="iComponentId"/>
		<s32 name="iDrawableId"/>
		<s32 name="iTextureId"/>
		<s32 name="iStartTime"/>
		<s32 name="iSectionCreatedIn"/>
	</structdef>

	<structdef type="::rage::SModelDetails" simple="true">
		<s32 name="iId"/>
		<string name="cModelName" type="member" size="CUTSCENE_OBJNAMELEN"/>
		<string name="cAnimName" type="member" size="CUTSCENE_OBJNAMELEN"/>
		<string name="cAnimName2" type="member" size="CUTSCENE_OBJNAMELEN"/>
		<s32 name="iFlags"/>
		<string name="cCompression" type="member" size="CUTSCENE_OBJNAMELEN"/>
		<bool name="bEmpty"/>
	</structdef>

	<structdef type="::rage::SVehicleDetails" simple="true">
		<s32 name="iId"/>
		<Color32 name="iColour"/>
		<u8 name="iDirtLevel"/>
		<s8 name="iTexture"/>
	</structdef>

	<structdef type="::rage::SVehicleRemoval" simple="true">
		<s32 name="iId"/>
		<s32 name="iBone"/>
	</structdef>

	<structdef type="::rage::SEffectsDetails" simple="true">
		<s32 name="iType"/>
		<s32 name="SectionCreated"/>
		<s32 name="iAttachedTo"/>
		<s32 name="iStartTime"/>
		<s32 name="iEndTime"/>
		<s32 name="iBoneTag"/>
		<Vector3 name="vPos"/>
		<Vector3 name="vDir"/>
		<s32 name="iEffectId"/>
		<string name="cOriginalEffectName" type="member" size="CUTSCENE_EFFECTNAMELEN * 2"/>
		<string name="cEffectName" type="member" size="CUTSCENE_EFFECTNAMELEN"/>
		<string name="cEffectAnim" type="member" size="CUTSCENE_EFFECTNAMELEN"/>
		<array name="cEvoName" type="member" size="MAX_EVOLUTION_EFFECT_VALUES">
			<string type="member" size="20"/>
		</array>
		<array name="fEvoValue" type="member" size="MAX_EVOLUTION_EFFECT_VALUES">
			<float/>
		</array>
	</structdef>

	<structdef type="::rage::SDrawDistanceDetails" simple="true" onPostSet="::rage::SDrawDistanceDetails::OnPostSetCallback">
		<s32 name="iStartTime"/>
		<float name="fNearClip"/>
		<float name="fFarClip"/>
		<float name="fDistance" />
	</structdef>

	<structdef type="::rage::SBlockDetails" simple="true">
		<array name="vCorners" type="member" size="4">
			<Vector3/>
		</array>
		<float name="fHeight"/>
	</structdef>

	<structdef type="::rage::SLightDetails" simple="true">
		<string name="cLightName" type="member" size="CUTSCENE_OBJNAMELEN"/>
	</structdef>

	<structdef type="::rage::SHiddenObjectDetails" simple="true">
		<Vector3 name="vPosition"/>
		<string name="cObjectName" type="member" size="CUTSCENE_OBJNAMELEN"/>
		<float name="fRadius"/>
	</structdef>

	<structdef type="::rage::SFixupObjectDetails" simple="true">
		<Vector3 name="vPosition"/>
		<string name="cObjectName" type="member" size="CUTSCENE_OBJNAMELEN"/>
		<float name="fRadius"/>
	</structdef>

	<structdef type="::rage::cutfCutsceneSection" simple="true">
		<Vector3 name="m_vOffset"/>
		<float name="m_fRotation"/>
		<float name="m_fDuration"/>
		<array name="m_models" type="member" size="MAX_CUTSCENE_ENTITIES">
			<struct type="::rage::SModelDetails"/>
		</array>
		<array name="m_vehicles" type="member" size="MAX_CUTSCENE_VEHICLES">
			<struct type="::rage::SVehicleDetails"/>
		</array>
		<array name="m_vehicleRemovals" type="member" size="MAX_CUTSCENE_VEHICLE_REMOVALS">
			<struct type="::rage::SVehicleRemoval"/>
		</array>
		<array name="m_attachments" type="member" size="MAX_CUTSCENE_ATTACHMENTS">
			<struct type="::rage::SAttachmentDetails"/>
		</array>
		<array name="m_hiddenObjects" type="member" size="MAX_HIDDEN_OBJS">
			<struct type="::rage::SHiddenObjectDetails"/>
		</array>
		<array name="m_lights" type="member" size="MAX_CUTSCENE_LIGHTS">
			<struct type="::rage::SLightDetails"/>
		</array>
		<string name="m_cAudioTrackName" type="member" size="CUTSCENE_OBJNAMELEN"/>
		<string name="m_cCameraAnimName" type="member" size="CUTSCENE_OBJNAMELEN"/>
		<string name="m_cAnimDictName" type="member" size="CUTSCENE_OBJNAMELEN"/>
		<s32 name="m_iModelCount"/>
		<s32 name="m_iVehicleCount"/>
		<s32 name="m_iVehicleRemovalCount"/>
		<s32 name="m_iAttachmentCount"/>
		<s32 name="m_iHiddenObjectCount"/>
		<s32 name="m_iLightCount"/>
		<s32 name="m_iManualSectionToUse"/>
		<s32 name="m_iManualSectionToUseForAudio"/>
		<bool name="m_bManualSection"/>
		<bool name="m_bUseRange"/>
		<float name="m_fRangeStart"/>
		<float name="m_fRangeEnd"/>
		<array name="m_iModelSize" type="member" size="MAX_CUTSCENE_ENTITIES">
			<u32/>
		</array>
		<u32 name="m_iStaticSize"/>
		<s32 name="m_iFrameOffset"/>
	</structdef>

	<structdef type="::rage::cutfCutsceneFile" simple="true">
    <pad bytes="1"/>  <!-- isLoaded-->
		<string name="m_cName" type="member" size="CUTSCENE_NAME_LEN"/>
		<s32 name="m_iNameHash"/>
		<float name="m_fTotalDuration"/>
		<s32 name="m_iPlayerId"/>
		<s32 name="m_iCutsceneFlags"/>
		<Vector3 name="m_vPlayerStartingPos"/>
		<string name="m_cMissionTextName" type="member" size="MISSION_NAME_LENGTH"/>
		<array name="m_pSections" type="member" size="MAX_CUTSCENE_SECTIONS">
			<pointer type="::rage::cutfCutsceneSection" policy="owner"/>
		</array>
		<array name="m_pDrawDistances" type="member" size="MAX_DRAW_DISTANCES">
			<pointer type="::rage::SDrawDistanceDetails" policy="owner"/>
		</array>
		<array name="m_pEffects" type="member" size="MAX_CUTSCENE_EFFECTS">
			<pointer type="::rage::SEffectsDetails" policy="owner"/>
		</array>
		<array name="m_pBlockingBounds" type="member" size="MAX_CUTSCENE_BLOCKING_BOUNDS">
			<pointer type="::rage::SBlockDetails" policy="owner"/>
		</array>
		<array name="m_pSubtitles" type="member" size="MAX_TEXTOUTPUT">
			<pointer type="::rage::STextIdDetails" policy="owner"/>
		</array>
		<array name="m_pCamcorderOverlays" type="member" size="MAX_CAMCORDER_OVERLAYS">
			<pointer type="::rage::SCamcorderOverlay" policy="owner"/>
		</array>
		<array name="m_pFixupObjects" type="member" size="MAX_FIXUP_OBJS">
			<pointer type="::rage::SFixupObjectDetails" policy="owner"/>
		</array>
		<array name="m_pVariations" type="member" size="MAX_CUTSCENE_VARIATIONS">
			<pointer type="::rage::SVariationDetails" policy="owner"/>
		</array>
		<array name="m_pProps" type="member" size="MAX_CUTSCENE_PROPS">
			<pointer type="::rage::SPropDetails" policy="owner"/>
		</array>
		<s32 name="m_iSectionCount"/>
		<s32 name="m_iVariationCount"/>
		<s32 name="m_iPropCount"/>
		<s32 name="m_iFixupObjectCount"/>
		<s32 name="m_iCamcorderOverlayCount"/>
		<s32 name="m_iDrawDistanceCount"/>
		<s32 name="m_iEffectCount"/>
		<s32 name="m_iBlockingBoundCount"/>
		<s32 name="m_iSubtitleCount"/>
		<string name="m_cExtraRoom" type="member" size="MAX_EXTRA_ROOM_CHARS"/>
		<Vector3 name="m_vExtraRoomPos"/>
		<s32 name="m_iStartFrame"/>
		<array name="m_iSectionSplitFrames" type="member" size="MAX_CUTSCENE_SECTIONS">
			<s32/>
		</array>
		<s32 name="m_iSectionSplitFrameCount"/>
		<s32 name="m_iEndFrame"/>
	</structdef>

</ParserSchema>