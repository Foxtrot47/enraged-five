// 
// cutfile/cutfeventdef.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef CUTFILE_CUTFEVENTDEF_H 
#define CUTFILE_CUTFEVENTDEF_H 

#include "cutfdefines.h"
#include "cutfevent.h"
#include "cutfeventargs.h"
#include "atl/array.h"
#include "file/limits.h"
#include "parser/macros.h"

namespace rage 
{

class cutfEventDef;
class cutfEventArgsDef;
class parTree;

//##############################################################################

// PURPOSE: This class holds a collection of cutfEventDefs and is used to save
//    and load them.
class cutfEventDefs
{
public:
    cutfEventDefs();
    cutfEventDefs( const char *pHeaderFilename, const char *pHeaderNamespace, atArray<cutfEventDef *> &eventDefList );
    virtual ~cutfEventDefs();

    // PURPOSE: Clears all data associated with this cut scene file, deleting all allocated structures and classes.
    void Clear();

    // PURPOSE: Save the data to the .xml file.
    // PARAMS:
    //    pFilename - The name of the file to save to.
    //    pExtension - the extension to save as. 
    // RETURNS: true on success, otherwise false.
#if __BANK || __TOOL
    bool SaveFile( const char* pFilename, const char* pExtension=NULL );
#endif

    // PURPOSE: Load the data from the .cut file.
    // PARAMS:
    //    pFilename - The name of the file to load.
    //    pExtension - the extension to load as.
    // RETURNS: true on success, otherwise false.
    bool LoadFile( const char* pFilename, const char* pExtension=NULL );

    // PURPOSE: Parses the data from the buffer.
    // PARAMS:
    //    pData - the data buffer to parse
    //    size - the length of data
    // RETURNS: true on success, otherwise false
    // NOTES: LoadFile executes this function, which is here for your convenience if you have an alternate method of loading files.
    bool ParseData( const char* pData, u32 size );

    // PURPOSE: Parse the data from the xml tree
    // PARAMS:
    //    pTree - the tree to parse
    // RETURNS: true on success, otherwise false.
    bool ParseXml( parTree *pTree );

    // PURPOSE: Retrieves the filename of the header file that will be created from the data in this structure.
    // RETURNS: The header filename.
    const char* GetHeaderFilename() const;

    // PURPOSE: Sets the filename of the header file that will be created from the data in this structure.
    // PARAMS:
    //    pFilename - the header filename
    void SetHeaderFilename( const char *pFilename );

    // PURPOSE: Retrieves the name of the namespace that will go in the header.
    // RETURNS: The namespace
    const char* GetHeaderNamespace() const;

    // PURPOSE: Sets the name of the namespace that will go in the header.
    // PARAMS:
    //    pNamespace - the namespace
    void SetHeaderNamespace( const char *pNamespace );

    // PURPOSE: Adds the event def to the list.
    // PARAMS:
    //    pEventDef - the event def to add
    // NOTES: The cutfEventArgsDef is managed by this call as well.  If it is the first reference, it will
    //    be added to our internal list.
    void AddEventDef( cutfEventDef *pEventDef );

    // PURPOSE: Removes the event def from the list and deletes it.  Also removes
    //    all of its events args defs and their event arg defs.
    // PARAMS:
    //    pEventDef - the event def to remove
    void RemoveEventDef( cutfEventDef *pEventDef );

    // PURPOSE: Removes the event def from the list and deletes it.  Also removes
    //    all of its events args defs and their event arg defs.
    // PARAMS:
    //    pName - the name of the event def to remove
    void RemoveEventDef( const char *pName );

    // PURPOSE: Removes the event def from the list and deletes it.  Also removes
    //    all of its events args defs and their event arg defs.
    // PARAMS:
    //    iEventIdOffset - the event id offset of the event def to remove
    void RemoveEventDef( s32 iEventIdOffset );

    // PURPOSE: Retrieve the number of event defs in the internal list.
    // RETURNS: the count
    s32 GetEventDefCount() const;

    // PURPOSE: Retrieve the event def at the specified index.
    // PARAMS:
    //    iIndex - the index of the event def
    // RETURNS: the event def, NULL if iIndex is out of range
    const cutfEventDef* GetEventDef( s32 iIndex ) const;

    // PURPOSE: Retrieve the number of event args defs in the internal list.
    // RETURNS: the count
    s32 GetEventArgsDefCount() const;

    // PURPOSE: Retrieve the event args def at the specified index.
    // PARAMS:
    //    iIndex - the index of the event args def
    // RETURNS: the event args def, NULL if iIndex is out of range
    const cutfEventArgsDef* GetEventArgsDef( s32 iIndex ) const;

    // PURPOSE: Determines the display name for the given event id.  This function should be passed into 
    //    cutfEvent::SetEventIdDisplayNameFunc(..).
    // PARAMS:
    //    iEventId - the event id
    // RETURNS: The event id's name.  If the event id is not found, returns "(unknown)".
    const char* GetEventDisplayName( s32 iEventId );

    // PURPOSE: Determines the rank of the given event id.  This function should be passed into 
    //    cutfEvent::SetEventIdRankFunc(..).
    // PARAMS:
    //    iEventId - the event id
    // RETURNS: The event id's rank.  If the event id is not found, returns CUTSCENE_LAST_EVENT_RANK.
    s32 GetEventIdRank( s32 iEventId );

    // PURPOSE: Determines the opposite event id of the given event id.  This function should be passed into
    //    cutfEvent::SetEventIdRankFunc(..).
    // PARAMS:
    //    iEventId - the event id
    // RETURNS: The event id's opposite.  If the event id is not found, returns CUTSCENE_NO_OPPOSITE_EVENT.
    s32 GetOppositeEventId( s32 iEventId );

    // PURPOSE: Determines the display name for the given event args type.  This function should be passed into 
    //    cutfEventArgs::SetEventArgsTypeNameFunc(..).
    // PARAMS:
    //    iEventArgsType - the event args type id
    // RETURNS: The event arg type's name.  If the event args type is not found, returns "(unknown)".
    const char* GetEventArgsTypeName( s32 iEventArgsType );

    // PURPOSE: Using the current cutfEventDefs, returns the index (as a string) of cutfEventArgsDef in the EventArgsDefList.
    // PARAMS:
    //    pEventArgsDef - the referenced pEventArgsDef.
    // RETURNS: The string index.  "-1" if there were none or the index could not be determined.
    static int GetEventArgsDefIndex( const cutfEventArgsDef *pEventArgsDef );

    // PURPOSE: Using the current cutfEventDefs, returns the cutfEventArgsDef that that string represents.
    // PARAMS:
    //    pEventArgsDefIndexString - the index (as a string) of the cutfEventArgsDef in the EventArgsDefList.
    // RETURNS: The cutfEventArgs.  NULL if there were none, or it wasn't found.
    static cutfEventArgsDef* FindEventArgsDef( int iEventArgsDefIndex );

    PAR_PARSABLE;

private:
    // PURPOSE: Ensures that each event def's and event args def's offset are the same as their index in its containing List.
    void EnforceSequentialOffsets();

    char m_cHeaderFilename[RAGE_MAX_PATH];
    char m_cHeaderNamespace[CUTSCENE_LONG_OBJNAMELEN];
    
    atArray<cutfEventDef *> m_eventDefList;
    atArray<cutfEventArgsDef *> m_eventArgsDefList;
    
    static const cutfEventDefs *sm_pCurrentEventDefs;  // for the cutfEventArgsDef referencing
};

inline const char* cutfEventDefs::GetHeaderFilename() const
{
    return m_cHeaderFilename;
}

inline void cutfEventDefs::SetHeaderFilename( const char *pFilename )
{
    if ( pFilename != NULL )
    {
        safecpy( m_cHeaderFilename, pFilename, sizeof(m_cHeaderFilename) );
    }
    else
    {
        m_cHeaderFilename[0] = 0;
    }
}

inline const char* cutfEventDefs::GetHeaderNamespace() const
{
    return m_cHeaderNamespace;
}

inline void cutfEventDefs::SetHeaderNamespace( const char *pNamespace )
{
    if ( pNamespace != NULL )
    {
        safecpy( m_cHeaderNamespace, pNamespace, sizeof(m_cHeaderNamespace) );
    }
    else
    {
        m_cHeaderNamespace[0] = 0;
    }
}

inline s32 cutfEventDefs::GetEventDefCount() const
{
    return m_eventDefList.GetCount();
}

inline const cutfEventDef* cutfEventDefs::GetEventDef( s32 iIndex ) const
{
    if ( (iIndex >= 0) && (iIndex < m_eventDefList.GetCount()) )
    {
        return m_eventDefList[iIndex];
    }

    return NULL;
}

inline s32 cutfEventDefs::GetEventArgsDefCount() const
{
    return m_eventArgsDefList.GetCount();
}

inline const cutfEventArgsDef* cutfEventDefs::GetEventArgsDef( s32 iIndex ) const
{
    if ( (iIndex >= 0) && (iIndex < m_eventArgsDefList.GetCount()) )
    {
        return m_eventArgsDefList[iIndex];
    }

    return NULL;
}

//##############################################################################

// PURPOSE: This class holds the data that describes a project-specific event id
//     and its event args, if any.
class cutfEventDef 
{
public:
    cutfEventDef();
    cutfEventDef( s32 iEventIdOffset, const char *pName, s32 iRank=CUTSCENE_LAST_EVENT_RANK, 
        s32 iOppositeEventIdOffset=CUTSCENE_NO_OPPOSITE_EVENT, const cutfEventArgsDef *pEventArgsDef=NULL );   
    virtual ~cutfEventDef();

    // PURPOSE: Retrieves the event id by adding CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE to the event id offset.
    // RETURNS: The event id.
    s32 GetEventId() const;

    // PURPOSE: Sets the event id offset by subtracting CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE.  This should 
    //    be greater than or equal to CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE for that reason.
    // PARAMS:
    //    iEventId - the event id
    void SetEventId( s32 iEventId );

    // PURPOSE: Retrieves the event id offset, which should be this object's index in cutfEventDefs's array.
    // RETURNS: The event id.
    s32 GetEventIdOffset() const;

    // PURPOSE: Sets the event id offset.  This should be this object's index in cutfEventDefs's array.
    // PARAMS:
    //    iEventIdOffset - the event id offset
    void SetEventIdOffset( s32 iEventIdOffset );

    // PURPOSE: Retrieves the name of this event.
    // RETURNS: The name
    const char* GetName() const;

    // PURPOSE: Sets the name of this event.  This will be used for display purposes and to generate
    //    the name of the enum as follows, replacing any illegal characters with underscores:  
    //    CUTSCENE_%s_EVENT
    // PARAMS:
    //    pName - the name
    void SetName( const char* pName );

    // PURPOSE: Retrieves the name of the enumeration into the buffer
    // PARAMS:
    //    pBuffer - the text buffer to write to
    //    iLen - the length of the buffer
    void GetEnumName( char *pBuffer, int iLen ) const;

    // PURPOSE: Retrieves the name of the enumeration into the buffer
    // PARAMS:
    //    pEventName - the name of the event
    //    pBuffer - the text buffer to write to
    //    iLen - the length of the buffer
    static void GetStaticEnumName( const char *pEventName, char *pBuffer, int iLen );

    // PURPOSE: Retrieves the rank of this event (for sorting purposes).
    // RETURNS: The rank.
    s32 GetRank() const;

    // PURPOSE: Sets the rank of this event (for sorting purposes).
    // PARAMS:
    //    iRank - the rank
    // NOTES: Typically, you will want to start with CUTSCENE_LAST_EVENT_RANK.  In a situation where 
    //    you create events that are opposites, the "stop" event should have a lower rank than the 
    //    "start" event.
    void SetRank( s32 iRank );

    // PURPOSE: Retrieves the event id that describes the opposite of this event by adding 
    //    CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE to the opposite evnet id offset.
    // RETURNS: The opposite event id.
    s32 GetOppositeEventId() const;

    // PURPOSE: Sets the opposite event id for this event by subtracting CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE
    //    from the opposite event id offset.  CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS 
    //    will cause the run time to find the previous occurrence of the event id in the time line.  
    //    If the opposite id is CUTSCENE_NO_OPPOSITE_EVENT, no opposite event shall be dispatched.
    // PARAMS:
    //    iOppositeEventId - the opposite event id
    // NOTES:  This will be used in 2 ways: 1) to turn the "Load Events" into the "Unload Events".
    //    2) To help with scrubbing during playback.
    void SetOppositeEventId( s32 iOppositeEventId );

    // PURPOSE: Retrieves the event id offset that describes the opposite of this event.
    // RETURNS: The event id.
    s32 GetOppositeEventIdOffset() const;

    // PURPOSE: Sets the opposite event id offset for this event.  CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS 
    //    will cause the run time to find the previous occurrence of the event id in the time line.  
    //    If the opposite id is CUTSCENE_NO_OPPOSITE_EVENT, no opposite event shall be dispatched.
    // PARAMS:
    //    iOppositeEventIdOffset - the opposite event id
    // NOTES:  This will be used in 2 ways: 1) to turn the "Load Events" into the "Unload Events".
    //    2) To help with scrubbing during playback.
    void SetOppositeEventIdOffset( s32 iOppositeEventIdOffset );

    // PURPOSE: Retrieve the cutfEventArgsDef
    // PARAMS: The event args def
    const cutfEventArgsDef* GetEventArgsDef() const;

    // PURPOSE: Sets the cutfEventArgsDef
    // PARAMS:
    //   pEventArgsDef - the event args def
    void SetEventArgsDef( const cutfEventArgsDef* pEventArgsDef );

	void PreLoad(parTreeNode* node);
	void ConvertArgPointersToIndices();
	void ConvertArgIndicesToPointers();

    PAR_PARSABLE;

private:
    s32 m_iEventIdOffset;
    char m_cName[CUTSCENE_LONG_OBJNAMELEN];
    s32 m_iRank;
    s32 m_iOppositeEventIdOffset;
	s32 m_iEventArgsDefIndex; /* for serialization, don't use at runtime */
    const cutfEventArgsDef* m_pEventArgsDefPtr;
};

inline s32 cutfEventDef::GetEventId() const
{
    return CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE + m_iEventIdOffset;
}

inline void cutfEventDef::SetEventId( s32 iEventId )
{
    m_iEventIdOffset = iEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
}

inline s32 cutfEventDef::GetEventIdOffset() const
{
    return m_iEventIdOffset;
}

inline void cutfEventDef::SetEventIdOffset( s32 iEventIdOffset )
{
    m_iEventIdOffset = iEventIdOffset;
}

inline const char* cutfEventDef::GetName() const
{
    return m_cName;
}

inline void cutfEventDef::SetName( const char* pName )
{
    if ( pName != NULL )
    {
        safecpy( m_cName, pName, sizeof(m_cName) );
    }
    else
    {
        m_cName[0] = 0;
    }
}

inline void cutfEventDef::GetEnumName( char *pBuffer, int iLen ) const
{
    cutfEventDef::GetStaticEnumName( GetName(), pBuffer, iLen );
}

inline s32 cutfEventDef::GetRank() const
{
    return m_iRank;
}

inline void cutfEventDef::SetRank( s32 iRank )
{
    m_iRank = iRank;
}

inline s32 cutfEventDef::GetOppositeEventId() const
{
    if ( m_iOppositeEventIdOffset >= 0 )
    {
        return CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE + m_iOppositeEventIdOffset;
    }

    // return CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS or CUTSCENE_NO_OPPOSITE_EVENT
    return m_iOppositeEventIdOffset;
}

inline void cutfEventDef::SetOppositeEventId( s32 iOppositeEventId )
{
    if ( iOppositeEventId >= 0 )
    {
        m_iOppositeEventIdOffset = iOppositeEventId - CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE;
    }
    else
    {
        // CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS or CUTSCENE_NO_OPPOSITE_EVENT
        m_iOppositeEventIdOffset = iOppositeEventId;
    }
}

inline s32 cutfEventDef::GetOppositeEventIdOffset() const
{
    return m_iOppositeEventIdOffset;
}

inline void cutfEventDef::SetOppositeEventIdOffset( s32 iOppositeEventIdOffset )
{
    m_iOppositeEventIdOffset = iOppositeEventIdOffset;
}

inline const cutfEventArgsDef* cutfEventDef::GetEventArgsDef() const
{
    return m_pEventArgsDefPtr;
}

//##############################################################################

// PURPOSE: This class holds a parAttributeList that, together, describe an 
//    event args class.
class cutfEventArgsDef
{
public:
    cutfEventArgsDef();
    cutfEventArgsDef( s32 iEventArgsTypeOffset, const char *pName );
    cutfEventArgsDef( s32 iEventArgsTypeOffset, const char *pName, parAttributeList &attributeList );
    virtual ~cutfEventArgsDef();

    // PURPOSE: Retrieves the event args type by adding CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE to 
    //    the event args type offset.
    // RETURNS: The event id.
    s32 GetEventArgsType() const;

    // PURPOSE: Sets the event args type offset by subtracting CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE.
    //    This should be greater than or equal to CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE for that reason.
    // PARAMS:
    //    iEventArgsType - the event args type
    void SetEventArgsType( s32 iEventArgsType );

    // PURPOSE: Retrieves the event id offset, which should be this object's index in cutfEventDefs's array.
    // RETURNS: The event id.
    s32 GetEventArgsTypeOffset() const;

    // PURPOSE: Sets the event args type offset.  This should be this object's index in cutfEventargsDefs's array.
    // PARAMS:
    //    iEventArgsTypeOffset - the event args type offset
    void SetEventArgsTypeOffset( s32 iEventArgsTypeOffset );

    // PURPOSE: Retrieves the current reference counter value.
    // RETURNS: The current number of references
    int GetRef() const;

    // PURPOSE: Increment reference counter.
    void AddRef() const;

    // PURPOSE: Decrements the reference counter.
    // RETURNS: Returns the new reference count.  This instance will NOT be deleted as it 
    //    will be owned by the cufCutsceneFile2 object which will handle the deletion.
    int Release() const;

    // PURPOSE: Retrieves the name of this event args.
    // RETURNS: The name
    const char* GetName() const;

    // PURPOSE: Sets the name of this event args.  This will be used for display purposes and to generate
    //    the name of the class as follows, replacing any illegal characters with underscores:  
    //    Cutscene%sCustomEventArgs.
    // PARAMS:
    //    pName - the name
    void SetName( const char* pName );

    // PURPOSE: Retrieves the parAttributList so key-value pairs can be 
    //    added and accessed.  The key is the name of the attribute and the value will 
    //    be the default value when a new instance of the event args is created.
    // RETURNS: The attribute list.
    const parAttributeList& GetAttributeList() const;

    // PURPOSE: Override the equals comparison operator.  We don't want to have multiple copies
    //    of the same cutfEventArgsDef in cutfEventDefs.
    // PARAMS:
    //    other - the cutfEventArgsDef to compare
    // RETURNS: true if they contain the same data, otherwise false.
    bool operator==( const cutfEventArgsDef &other ) const;

    // PURPOSE: Override the not equals comparison operator.  We don't want to have multiple copies
    //    of the same cutfEventArgsDef in cutfEventDefs.
    // PARAMS:
    //    other - the cutfEventArgsDef to compare
    // RETURNS: true if they contain the same data, otherwise false.
    bool operator!=( const cutfEventArgsDef &other ) const;
    
	void ConvertAttributesForSave();
	void ConvertAttributesAfterLoad();

	PAR_PARSABLE;

private:
    s32 m_iEventArgsTypeOffset;
    mutable u16 m_iRefCount;
    char m_cName[CUTSCENE_LONG_OBJNAMELEN];
    parAttributeList m_attributeList;
	cutfAttributeList* m_cutfAttributes;
};

inline s32 cutfEventArgsDef::GetEventArgsType() const
{
    return CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE + m_iEventArgsTypeOffset;
}

inline void cutfEventArgsDef::SetEventArgsType( s32 iEventArgsType )
{
    m_iEventArgsTypeOffset = iEventArgsType - CUTSCENE_EVENT_ARGS_TYPE_FIRST_RESERVED_FOR_PROJECT_USE;
}

inline s32 cutfEventArgsDef::GetEventArgsTypeOffset() const
{
    return m_iEventArgsTypeOffset;
}

inline void cutfEventArgsDef::SetEventArgsTypeOffset( s32 iEventArgsTypeOffset )
{
    m_iEventArgsTypeOffset = iEventArgsTypeOffset;
}

inline int cutfEventArgsDef::GetRef() const
{ 
    return m_iRefCount; 
}

inline void cutfEventArgsDef::AddRef() const
{ 
    ++m_iRefCount; 
}

inline int cutfEventArgsDef::Release() const
{ 
    if ( m_iRefCount > 0 )
    {
        --m_iRefCount;
    }

    return m_iRefCount; 
}

inline const char* cutfEventArgsDef::GetName() const
{
    return m_cName;
}

inline void cutfEventArgsDef::SetName( const char* pName )
{
    if ( pName != NULL )
    {
        safecpy( m_cName, pName, sizeof(m_cName) );
    }
    else
    {
        m_cName[0] = 0;
    }
}

inline const parAttributeList& cutfEventArgsDef::GetAttributeList() const
{
    return m_attributeList;
}

inline bool cutfEventArgsDef::operator==( const cutfEventArgsDef &other ) const
{
    return operator!=(other) == false;
}

//##############################################################################

} // namespace rage

#endif // CUTFILE_CUTFEVENTDEF_H 
