// 
// cutfile/cutfobject.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cutfobject.h"
#include "cutfobject_parser.h"
#include "cutscene/cutsoptimisations.h"

#include "cutfile2.h"

#include "diag/tracker.h"
#include "file/asset.h"
#include "atl/map.h"

CUTS_OPTIMISATIONS()

namespace rage {

//##############################################################################

cutfObject::cutfObject()
: m_iObjectId( -1 )
, m_cutfAttributes(NULL)
{

}

cutfObject::cutfObject( s32 iObjectId )
: m_iObjectId( iObjectId )
, m_cutfAttributes(NULL)
{

}

cutfObject::cutfObject( s32 iObjectId, const parAttributeList& attributes )
: m_iObjectId( iObjectId )
, m_cutfAttributes(NULL)
{
    m_attributeList.CopyFrom( attributes );
}

cutfObject::~cutfObject() 
{
	delete m_cutfAttributes;
}

void cutfObject::MergeFrom( const cutfObject &other, bool overwrite )
{
    cutfAssertf( GetType() == other.GetType(), "Can only merge objects of the same type." );

    m_attributeList.MergeFrom( other.GetAttributeList(), overwrite );
}

int cutfObject::Compare( const cutfObject *pObject ) const
{
	if ( GetObjectId() < pObject->GetObjectId() )
	{
		return -1;
	}
	else if ( GetObjectId() > pObject->GetObjectId() )
	{
		return 1;
	}
	else if ( GetObjectId() == pObject->GetObjectId() )
	{
		return 0;
	}

	return 0;
}

bool cutfObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfObject::operator != ( const cutfObject &other ) const
{
    if ( this == &other )
    {
        return false;
    }

    if ( (GetType() != other.GetType())
        || (strcmp( GetDisplayName().c_str(), other.GetDisplayName().c_str() ) != 0) )
    {
        return true;
    }

    const atArray<parAttribute> &attList = m_attributeList.GetAttributeArray();
    if ( attList.GetCount() != other.GetAttributeList().GetAttributeArray().GetCount() )
    {
        return true;
    }

    for ( int i = 0; i < attList.GetCount(); ++i )
    {
        const parAttribute *pAtt = other.GetAttributeList().FindAttribute( attList[i].GetName() );
        if ( pAtt == NULL )
        {
            return true;
        }

        if ( attList[i].GetType() != pAtt->GetType() )
        {
            return true;
        }

        switch ( attList[i].GetType() )
        {
        case parAttribute::BOOL:
            {
                if ( attList[i].GetBoolValue() != pAtt->GetBoolValue() )
                {
                    return true;
                }
            }
            break;
        case parAttribute::DOUBLE:
            {
                if ( attList[i].GetDoubleValue() != pAtt->GetDoubleValue() )
                {
                    return true;
                }
            }
            break;
        case parAttribute::INT64:
            {
                if ( attList[i].GetInt64Value() != pAtt->GetInt64Value() )
                {
                    return true;
                }
            }
            break;
        case parAttribute::STRING:
            {
                if ( strcmp( attList[i].GetStringValue(), pAtt->GetStringValue() ) != 0 )
                {
                    return true;
                }
            }
            break;
        }
    }

    return false;
}

void cutfObject::ConvertAttributesForSave()
{
	cutfCutsceneFile2::ConvertToCutfAttributeList(m_attributeList, m_cutfAttributes);
}

void cutfObject::ConvertAttributesAfterLoad()
{
	cutfCutsceneFile2::ConvertToParAttributeList(m_cutfAttributes, m_attributeList);
}

//##############################################################################

cutfAssetManagerObject::cutfAssetManagerObject()
: cutfObject()
{

}

cutfAssetManagerObject::cutfAssetManagerObject( s32 iObjectId )
: cutfObject( iObjectId )
{

}

cutfAssetManagerObject::cutfAssetManagerObject( s32 iObjectId, const parAttributeList& attributes )
: cutfObject( iObjectId, attributes )
{

}

cutfObject* cutfAssetManagerObject::Clone() const
{
    return rage_new cutfAssetManagerObject( m_iObjectId, m_attributeList );
}


//##############################################################################

cutfNamedObject::cutfNamedObject()
: cutfObject()
{
}

cutfNamedObject::cutfNamedObject( s32 iObjectId, atHashString pName )
: cutfObject( iObjectId )
{
	SetName( pName );
}

cutfNamedObject::cutfNamedObject( s32 iObjectId, const char *pName )
: cutfObject( iObjectId )
{
	SetName( pName );
}

cutfNamedObject::cutfNamedObject( s32 iObjectId, const char *pName, const parAttributeList& attributes )
: cutfObject( iObjectId, attributes )
{
	SetName( pName );
}

cutfNamedObject::cutfNamedObject( s32 iObjectId, atHashString Name, const parAttributeList& attributes )
: cutfObject( iObjectId, attributes )
{
	SetName( Name );
}


void cutfNamedObject::SetName( const char* pName )
{
	if ( pName != NULL )
	{
		m_cName = pName; 

	}
	else
	{
		m_cName.Clear(); 
	}

//#if !__FINAL
	m_displayName.Clear();
//#endif

}

void cutfNamedObject::SetName( atHashString Name )
{
	if ( Name.GetHash() !=0 )
	{
		m_cName = Name; 

	}
	else
	{
		m_cName.Clear(); 
	}

//#if !__FINAL
	m_displayName.Clear();
//#endif

}

bool cutfNamedObject::operator == ( const cutfObject &other ) const
{
	return (*this != other) == false;
}

bool cutfNamedObject::operator != ( const cutfObject &other ) const
{
	if ( cutfObject::operator !=( other ) )
	{
		return true;
	}

	const cutfNamedObject *pOtherNamedObject = dynamic_cast<const cutfNamedObject *>( &other );
	if ( GetName().GetHash() != pOtherNamedObject->GetName().GetHash())
	{
		return true;
	}

	return false;
}


//#if! __FINAL
const atString& cutfNamedObject::GetDisplayName() const
{
	RAGE_TRACK( cutfNamedObject_GetDisplayName );
#if !__FINAL
	if ( m_displayName.GetLength() == 0 && m_cName.TryGetCStr())
	{
		char cDisplayName[CUTSCENE_OBJNAMELEN];
		safecpy( cDisplayName, m_cName.GetCStr(), sizeof(cDisplayName) );

		char *pToken = strtok( cDisplayName, ":" );
		if ( pToken )
		{
			m_displayName.Set( pToken, (int)strlen( pToken ), 0 );
		}
		else
		{
			m_displayName.Set( m_cName.GetCStr(), (int)strlen( m_cName.GetCStr() ), 0 );
		}
	}
#endif

	return m_displayName;
}

//##############################################################################
cutfNamedAnimatedObject::cutfNamedAnimatedObject()
:cutfNamedObject()
{
}

cutfNamedAnimatedObject::cutfNamedAnimatedObject( s32 iObjectId, const char* pName, u32 AnimBaseName)
: cutfNamedObject( iObjectId, pName )
, m_AnimStreamingBase(AnimBaseName)
{
}

cutfNamedAnimatedObject::cutfNamedAnimatedObject( s32 iObjectId, atHashString Name, u32 AnimBaseName )
: cutfNamedObject( iObjectId, Name )
,  m_AnimStreamingBase(AnimBaseName)
{
}

cutfNamedAnimatedObject::cutfNamedAnimatedObject( s32 iObjectId, const char *pName, u32 AnimBaseName, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, pName, attributes )
,  m_AnimStreamingBase(AnimBaseName)
{
}

cutfNamedAnimatedObject::cutfNamedAnimatedObject( s32 iObjectId, atHashString Name, u32 AnimBaseName, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, Name, attributes )
,  m_AnimStreamingBase(AnimBaseName)
{
}

bool cutfNamedAnimatedObject::operator == ( const cutfObject &other ) const
{
	return (*this != other) == false;
}

bool cutfNamedAnimatedObject::operator != ( const cutfObject &other ) const
{
	if ( cutfNamedObject::operator !=( other ) )
	{
		return true;
	}

	const cutfNamedAnimatedObject *pOtherNamedObject = dynamic_cast<const cutfNamedAnimatedObject *>( &other );
	if ( GetAnimStreamingBase() != pOtherNamedObject->GetAnimStreamingBase())
	{
		return true;
	}

	return false;
}


void cutfNamedAnimatedObject::SetAnimStreamingBase(const char* Name)
{
	m_AnimStreamingBase = atPartialStringHash(Name); 
}


//##############################################################################

cutfNamedStreamedObject::cutfNamedStreamedObject()
:cutfNamedObject()
{
}

cutfNamedStreamedObject::cutfNamedStreamedObject( s32 iObjectId, const char* pName, const char* pStreamingName)
: cutfNamedObject( iObjectId, pName )
, m_StreamingName(pStreamingName)
{
}

cutfNamedStreamedObject::cutfNamedStreamedObject( s32 iObjectId, atHashString Name, atHashString StreamingName )
: cutfNamedObject( iObjectId, Name )
, m_StreamingName(StreamingName)
{
}

cutfNamedStreamedObject::cutfNamedStreamedObject( s32 iObjectId, const char *pName, const char* pStreamingName, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, pName, attributes )
, m_StreamingName(pStreamingName)
{
}

cutfNamedStreamedObject::cutfNamedStreamedObject( s32 iObjectId, atHashString Name, atHashString pStreamingName, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, Name, attributes )
, m_StreamingName(pStreamingName)
{
}


inline void cutfNamedStreamedObject::SetStreamingName( atHashString StreamingName )
{
#if !__FINAL
	char cModelName[CUTSCENE_OBJNAMELEN];
	cModelName[0] = 0;
	strncpy(cModelName, StreamingName.GetCStr(), strlen(StreamingName.GetCStr()));

	cModelName[GetDisplayName().GetLength()] = '\0';

	char *pChar = strrchr(cModelName, '~'); 
	char *pSecondChar = strrchr(cModelName, '^'); 
	if(pChar != NULL){ *pChar = '\0'; }
	if(pSecondChar != NULL){ *pSecondChar = '\0';} 

	m_StreamingName.SetFromString(cModelName);
#else
	m_StreamingName = StreamingName; 
#endif
//#if !__FINAL
	m_displayName.Clear();
//#endif
}

bool cutfNamedStreamedObject::operator == ( const cutfObject &other ) const
{
	return (*this != other) == false;
}

bool cutfNamedStreamedObject::operator != ( const cutfObject &other ) const
{
	if ( cutfNamedObject::operator !=( other ) )
	{
		return true;
	}

	const cutfNamedStreamedObject *pOtherNamedObject = dynamic_cast<const cutfNamedStreamedObject *>( &other );
	if ( GetStreamingName().GetHash() != pOtherNamedObject->GetStreamingName().GetHash())
	{
		return true;
	}

	return false;
}


//##############################################################################


cutfNamedAnimatedStreamedObject::cutfNamedAnimatedStreamedObject()
:cutfNamedStreamedObject()
{
}

cutfNamedAnimatedStreamedObject::cutfNamedAnimatedStreamedObject( s32 iObjectId, const char* pName, const char* pStreamingName, u32 AnimStreamingBase)
: cutfNamedStreamedObject( iObjectId, pName, pStreamingName )
, m_AnimStreamingBase(AnimStreamingBase)
{
}

cutfNamedAnimatedStreamedObject::cutfNamedAnimatedStreamedObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimStreamingBase )
: cutfNamedStreamedObject( iObjectId, Name, StreamingName )
, m_AnimStreamingBase(AnimStreamingBase)
{
}

cutfNamedAnimatedStreamedObject::cutfNamedAnimatedStreamedObject( s32 iObjectId, const char *pName, const char* pStreamingName, u32 AnimStreamingBase, const parAttributeList& attributes )
: cutfNamedStreamedObject( iObjectId, pName, pStreamingName, attributes )
, m_AnimStreamingBase(AnimStreamingBase)
{
}

cutfNamedAnimatedStreamedObject::cutfNamedAnimatedStreamedObject( s32 iObjectId, atHashString Name, atHashString pStreamingName, u32 AnimStreamingBase, const parAttributeList& attributes )
: cutfNamedStreamedObject( iObjectId, Name, pStreamingName, attributes )
,  m_AnimStreamingBase(AnimStreamingBase)
{
}

bool cutfNamedAnimatedStreamedObject::operator == ( const cutfObject &other ) const
{
	return (*this != other) == false;
}

void cutfNamedAnimatedStreamedObject::SetAnimStreamingBase(const char* Name)
{
	m_AnimStreamingBase = atPartialStringHash(Name); 
}

bool cutfNamedAnimatedStreamedObject::operator != ( const cutfObject &other ) const
{
	if ( cutfNamedStreamedObject::operator !=( other ) )
	{
		return true;
	}

	const cutfNamedAnimatedStreamedObject *pOtherNamedObject = dynamic_cast<const cutfNamedAnimatedStreamedObject *>( &other );
	if ( GetAnimStreamingBase() != pOtherNamedObject->GetAnimStreamingBase())
	{
		return true;
	}

	return false;
}


//##############################################################################
cutfFinalNamedObject::cutfFinalNamedObject()
: cutfObject()
{
}

cutfFinalNamedObject::cutfFinalNamedObject( s32 iObjectId, const char *pName )
: cutfObject( iObjectId )
{
    SetName( pName );
}

cutfFinalNamedObject::cutfFinalNamedObject( s32 iObjectId, const char *pName, const parAttributeList& attributes )
: cutfObject( iObjectId, attributes )
{
    SetName( pName );
}

//#if! __FINAL
const atString& cutfFinalNamedObject::GetDisplayName() const
{
    RAGE_TRACK( cutfNamedObject_GetDisplayName );

    if ( m_displayName.GetLength() == 0 )
    {
        char cDisplayName[CUTSCENE_OBJNAMELEN];
        safecpy( cDisplayName, m_cName.c_str(), sizeof(cDisplayName) );

        char *pToken = strtok( cDisplayName, ":" );
        if ( pToken )
        {
            m_displayName.Set( pToken, (int)strlen( pToken ), 0 );
        }
        else
        {
            m_displayName.Set( m_cName, (int)strlen( m_cName ), 0 );
        }
    }

    return m_displayName;
}
//#endif

bool cutfFinalNamedObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfFinalNamedObject::operator != ( const cutfObject &other ) const
{
    if ( cutfObject::operator !=( other ) )
    {
        return true;
    }

    const cutfFinalNamedObject *pOtherNamedObject = dynamic_cast<const cutfFinalNamedObject *>( &other );
    if ( strcmp( GetName(), pOtherNamedObject->GetName() ) != 0 )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfOverlayObject::cutfOverlayObject()
: cutfFinalNamedObject()
{
	m_iOverlayType = 0;
	m_modelHashName = "";
}

cutfOverlayObject::cutfOverlayObject( s32 iObjectId, const char* pName)
: cutfFinalNamedObject( iObjectId, pName)
{
	m_iOverlayType = 0;
	m_modelHashName = "";
}

cutfOverlayObject::cutfOverlayObject( s32 iObjectId, const char* pName, const parAttributeList& attributes )
: cutfFinalNamedObject( iObjectId, pName, attributes )
{
	m_iOverlayType = 0;
	m_modelHashName = "";
}

cutfOverlayObject::cutfOverlayObject( s32 iObjectId, const char* pName, const char* pRenderTarget, int iOverlayType, atHashString modelHashName, const parAttributeList& attributes )
: cutfFinalNamedObject( iObjectId, pName, attributes ),
m_iOverlayType(iOverlayType),
m_modelHashName(modelHashName)
{
	SetRenderTargetName(pRenderTarget);
}

cutfObject* cutfOverlayObject::Clone() const
{
    return rage_new cutfOverlayObject( m_iObjectId, m_cName, m_cRenderTargetName, m_iOverlayType, m_modelHashName, m_attributeList );
}

//##############################################################################

cutfScreenFadeObject::cutfScreenFadeObject()
: cutfNamedObject()
{

}

cutfScreenFadeObject::cutfScreenFadeObject( s32 iObjectId, const char *pName )
: cutfNamedObject( iObjectId, pName )
{

}

cutfScreenFadeObject::cutfScreenFadeObject( s32 iObjectId, const char *pName, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, pName, attributes )
{

}

cutfScreenFadeObject::cutfScreenFadeObject( s32 iObjectId, atHashString Name, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, Name, attributes )
{

}

cutfObject* cutfScreenFadeObject::Clone() const
{
   
	return rage_new cutfScreenFadeObject( m_iObjectId, m_cName, m_attributeList );
}

//##############################################################################



cutfSubtitleObject::cutfSubtitleObject()
: cutfNamedObject()
{

}

cutfSubtitleObject::cutfSubtitleObject( s32 iObjectId, const char *pName )
: cutfNamedObject( iObjectId, pName )
{

}

cutfSubtitleObject::cutfSubtitleObject( s32 iObjectId, const char *pName, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, pName, attributes )
{

}

cutfSubtitleObject::cutfSubtitleObject( s32 iObjectId, atHashString Name, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, Name, attributes )
{

}

cutfObject* cutfSubtitleObject::Clone() const
{
    return rage_new cutfSubtitleObject( m_iObjectId, m_cName, m_attributeList );
}

//##############################################################################

cutfModelObject::cutfModelObject()
: cutfNamedAnimatedStreamedObject()
{
}

cutfModelObject::cutfModelObject( s32 iObjectId, const char *pName, const char* pStreamedName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile )
: cutfNamedAnimatedStreamedObject( iObjectId, pName, pStreamedName, AnimBaseHash )
{
    SetAnimExportCtrlSpecFile( pAnimExportCtrlSpecFile );
    SetAnimCompressionFile( pAnimCompressionFile );
}

cutfModelObject::cutfModelObject( s32 iObjectId, const char *pName, const char* pStreamedName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile,  
                                 const parAttributeList& attributes )
: cutfNamedAnimatedStreamedObject( iObjectId, pName, pStreamedName, AnimBaseHash, attributes )
{
    SetAnimExportCtrlSpecFile( pAnimExportCtrlSpecFile );
    SetAnimCompressionFile( pAnimCompressionFile );
}

cutfModelObject::cutfModelObject( s32 iObjectId, atHashString Name, atHashString StreamedName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, const char *pAnimCompressionFile,  
								 const parAttributeList& attributes )
								 : cutfNamedAnimatedStreamedObject( iObjectId, Name, StreamedName, AnimBaseHash, attributes )
{
	SetAnimExportCtrlSpecFile( pAnimExportCtrlSpecFile );
	SetAnimCompressionFile( pAnimCompressionFile );
}

cutfModelObject::cutfModelObject( s32 iObjectId, atHashString Name, atHashString StreamedName, u32 AnimBaseHash, atHashString pAnimExportCtrlSpecFile, atHashString pAnimCompressionFile,  
								 const parAttributeList& attributes )
								 : cutfNamedAnimatedStreamedObject( iObjectId, Name, StreamedName, AnimBaseHash, attributes )
{
	SetAnimExportCtrlSpecFile( pAnimExportCtrlSpecFile );
	SetAnimCompressionFile( pAnimCompressionFile );
}

atHashString cutfModelObject::GetDefaultAnimExportCtrlSpecFile() const
{
    RAGE_TRACK( cutfModelObject_GetDefaultAnimExportCtrlSpecFile );

    if ( m_defaultAnimExportCtrlSpecFile.GetHash() == 0 )
    {
        char cDefaultSpecFilename[CUTSCENE_LONG_OBJNAMELEN];
        sprintf( cDefaultSpecFilename, "%s_Default_Spec.xml", GetTypeName() );

        int len = (int)strlen( cDefaultSpecFilename );
        for ( int i = 0; i < len; ++i )
        {
            if ( isspace( cDefaultSpecFilename[i] ) )
            {
                cDefaultSpecFilename[i] = '_';
            }
        }

		m_defaultAnimExportCtrlSpecFile = cDefaultSpecFilename; 
    }

    return m_defaultAnimExportCtrlSpecFile;
}

atHashString cutfModelObject::GetDefaultAnimCompression() const
{
    RAGE_TRACK( cutfModelObject_GetDefaultAnimCompression );

    if ( m_defaultAnimCompression.GetHash() == 0 )
    {
        char cDefaultCompressFilename[CUTSCENE_LONG_OBJNAMELEN];
        sprintf( cDefaultCompressFilename, "%s_Default_Compress.txt", GetTypeName() );

        int len = (int)strlen( cDefaultCompressFilename );
        for ( int i = 0; i < len; ++i )
        {
            if ( isspace( cDefaultCompressFilename[i] ) )
            {
                cDefaultCompressFilename[i] = '_';
            }
        }

        //m_defaultAnimCompression.Set( cDefaultCompressFilename, (int)strlen( cDefaultCompressFilename ), 0 );
		m_defaultAnimCompression = cDefaultCompressFilename; 
    }

    return m_defaultAnimCompression;
}


void cutfModelObject::SetTypeFile( atHashString typeFile )
{
	m_typeFile = typeFile;
}

atHashString cutfModelObject::GetTypeFile() const
{
	return m_typeFile;
}

bool cutfModelObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfModelObject::operator != ( const cutfObject &other ) const
{
    if ( cutfNamedObject::operator !=( other ) )
    {
        return true;
    }

    const cutfModelObject *pOtherModelObject = dynamic_cast<const cutfModelObject *>( &other );
    if ( (GetModelType() != pOtherModelObject->GetModelType())
		|| (GetAnimCompressionFile().GetHash() != pOtherModelObject->GetAnimCompressionFile())
		|| (GetAnimExportCtrlSpecFile().GetHash() != pOtherModelObject->GetAnimExportCtrlSpecFile().GetHash()))
    {
        return true;
    }

    return false;
}

//##############################################################################


cutfPedModelObject::cutfPedModelObject()
: cutfModelObject()
, m_bFoundFaceAnimation(false)
, m_bFaceAndBodyAreMerged(false)
, m_bOverrideFaceAnimation(false)
, m_overrideFaceAnimationFilename("")
, m_faceAnimationNodeName("")
, m_faceAttributesFilename("")
{

}

cutfPedModelObject::cutfPedModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
                                       const char *pAnimCompressionFile, bool bHasHeadAnimation, bool bFaceAndBodyAreMerged, 
                                       bool bOverrideFaceAnimation, const char *pFaceAnimationFilename )
: cutfModelObject( iObjectId, pName, pStreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile )
, m_bFoundFaceAnimation(bHasHeadAnimation)
, m_bFaceAndBodyAreMerged(bFaceAndBodyAreMerged)
, m_bOverrideFaceAnimation(bOverrideFaceAnimation)
, m_overrideFaceAnimationFilename(pFaceAnimationFilename)
{

}

cutfPedModelObject::cutfPedModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
                                       const char *pAnimCompressionFile, bool bHasHeadAnimation, bool bFaceAndBodyAreMerged, 
                                       bool bOverrideFaceAnimation, const char *pFaceAnimationFilename, const parAttributeList& attributes )
: cutfModelObject( iObjectId, pName, pStreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes )
, m_bFoundFaceAnimation(bHasHeadAnimation)
, m_bFaceAndBodyAreMerged(bFaceAndBodyAreMerged)
, m_bOverrideFaceAnimation(bOverrideFaceAnimation)
, m_overrideFaceAnimationFilename(pFaceAnimationFilename)
{

}

cutfPedModelObject::cutfPedModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
									   const char *pAnimCompressionFile, bool bHasHeadAnimation, bool bFaceAndBodyAreMerged, 
									   bool bOverrideFaceAnimation, const char *pFaceAnimationFilename, const parAttributeList& attributes )
									   : cutfModelObject( iObjectId, Name, StreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes )
									   , m_bFoundFaceAnimation(bHasHeadAnimation)
									   , m_bFaceAndBodyAreMerged(bFaceAndBodyAreMerged)
									   , m_bOverrideFaceAnimation(bOverrideFaceAnimation)
									   , m_overrideFaceAnimationFilename(pFaceAnimationFilename)
									   , m_faceAnimationNodeName("")
									   , m_faceAttributesFilename("")
{

}

cutfPedModelObject::cutfPedModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, atHashString pAnimExportCtrlSpecFile, 
									   atHashString pAnimCompressionFile, bool bHasHeadAnimation, bool bFaceAndBodyAreMerged, 
									   bool bOverrideFaceAnimation, atHashString pFaceAnimationFilename, const parAttributeList& attributes )
									   : cutfModelObject( iObjectId, Name, StreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes )
									   , m_bFoundFaceAnimation(bHasHeadAnimation)
									   , m_bFaceAndBodyAreMerged(bFaceAndBodyAreMerged)
									   , m_bOverrideFaceAnimation(bOverrideFaceAnimation)
									   , m_overrideFaceAnimationFilename(pFaceAnimationFilename)
{

}



cutfObject* cutfPedModelObject::Clone() const
{    
    cutfPedModelObject *pPedModelObject = rage_new cutfPedModelObject( m_iObjectId, m_cName, m_StreamingName, m_AnimStreamingBase,m_cAnimExportCtrlSpecFile, m_cAnimCompressionFile, 
        m_bFoundFaceAnimation, m_bFaceAndBodyAreMerged, m_bOverrideFaceAnimation, m_overrideFaceAnimationFilename, m_attributeList );
	pPedModelObject->SetFaceAnimationNodeName( m_faceAnimationNodeName );
	pPedModelObject->SetFaceExportCtrlSpecFile( m_cFaceExportCtrlSpecFile );
	pPedModelObject->SetFaceAttributesFilename( m_faceAttributesFilename );
	pPedModelObject->SetHandle( GetHandle() );
	pPedModelObject->SetTypeFile( GetTypeFile() ); 
    return pPedModelObject;
}


bool cutfPedModelObject::HasFaceAnimation() const
{
    return FoundDefaultFaceAnimation() || (OverrideFaceAnimation() && (m_overrideFaceAnimationFilename.GetHash() > 0));
}


atHashString cutfPedModelObject::GetFaceAnimationFilename( const char *pFaceDir ) const
{
    if ( OverrideFaceAnimation() )
    {
        return m_overrideFaceAnimationFilename;
    }
    else
    {
        return GetDefaultFaceAnimationFilename( pFaceDir );
    }
}


atHashString cutfPedModelObject::GetDefaultFaceAnimationFilename( const char *pFaceDir ) const
{
    RAGE_TRACK( cutfPedModelObject_GetDefaultFaceAnimationFilename );

    char cName[CUTSCENE_LONG_OBJNAMELEN];
    safecpy( cName, GetDisplayName().c_str(), sizeof(cName) );

    int len = (int)strlen( cName );
    for ( int i = 0; i < len; ++i )
    {
        if ( isspace( cName[i] ) )
        {
            cName[i] = '_';
        }
    }

    char cFilename[RAGE_MAX_PATH];
    sprintf( cFilename, "%s\\%s_face", pFaceDir, cName );

    if ( ASSET.Exists( cFilename, "clip" ) )
    {
        safecat( cFilename, ".clip" );
    }
    else
    {
        safecat( cFilename, ".anim" );
    }

    m_defaultFaceAnimationFilename = cFilename;
    return m_defaultFaceAnimationFilename;
}


bool cutfPedModelObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfPedModelObject::operator != ( const cutfObject &other ) const
{
    if ( cutfModelObject::operator !=( other ) )
    {
        return true;
    }

    const cutfPedModelObject *pOtherPedModelObject = dynamic_cast<const cutfPedModelObject *>( &other );
    if ( (FoundDefaultFaceAnimation() != pOtherPedModelObject->FoundDefaultFaceAnimation())
        || (FaceAndBodyAreMerged() != pOtherPedModelObject->FaceAndBodyAreMerged()) 
        || (OverrideFaceAnimation() != pOtherPedModelObject->OverrideFaceAnimation())
        || (OverrideFaceAnimation() && pOtherPedModelObject->OverrideFaceAnimation() && (m_overrideFaceAnimationFilename != pOtherPedModelObject->m_overrideFaceAnimationFilename)) )
    {
        return true;
    }
    
    return false;
}

//##############################################################################


cutfVehicleModelObject::cutfVehicleModelObject()
: cutfModelObject()
{

}

cutfVehicleModelObject::cutfVehicleModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
                                               const char *pAnimCompressionFile )
: cutfModelObject( iObjectId, pName, pStreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile )
{

}

cutfVehicleModelObject::cutfVehicleModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
                                               const char *pAnimCompressionFile, const atArray<atString> &removeBoneNameList )
: cutfModelObject( iObjectId, pName, pStreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile )
{
    m_cRemoveBoneNameList.Reserve( removeBoneNameList.GetCount() );
    for ( int i = 0; i < removeBoneNameList.GetCount(); ++i )
    {
        m_cRemoveBoneNameList.Append() = removeBoneNameList[i];
    }
}

cutfVehicleModelObject::cutfVehicleModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
                                               const char *pAnimCompressionFile, const atArray<atString> &removeBoneNameList, 
                                               const parAttributeList& attributes )
: cutfModelObject( iObjectId, pName, pStreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes )
{
    m_cRemoveBoneNameList.Reserve( removeBoneNameList.GetCount() );
    for ( int i = 0; i < removeBoneNameList.GetCount(); ++i )
    {
        m_cRemoveBoneNameList.Append() = removeBoneNameList[i];
    }
}

cutfVehicleModelObject::cutfVehicleModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
											   const char *pAnimCompressionFile, const atArray<atString> &removeBoneNameList, 
											   const parAttributeList& attributes )
											   : cutfModelObject( iObjectId, Name, StreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes )
{
	m_cRemoveBoneNameList.Reserve( removeBoneNameList.GetCount() );
	for ( int i = 0; i < removeBoneNameList.GetCount(); ++i )
	{
		m_cRemoveBoneNameList.Append() = removeBoneNameList[i];
	}
}

cutfVehicleModelObject::cutfVehicleModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, atHashString pAnimExportCtrlSpecFile, 
											   atHashString pAnimCompressionFile, const atArray<atString> &removeBoneNameList, 
											   const parAttributeList& attributes )
											   : cutfModelObject( iObjectId, Name, StreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes )
{
	m_cRemoveBoneNameList.Reserve( removeBoneNameList.GetCount() );
	for ( int i = 0; i < removeBoneNameList.GetCount(); ++i )
	{
		m_cRemoveBoneNameList.Append() = removeBoneNameList[i];
	}
}




cutfVehicleModelObject::~cutfVehicleModelObject()
{
    for ( int i = 0; i < m_cRemoveBoneNameList.GetCount(); ++i )
    {
        m_cRemoveBoneNameList[i].Clear();
    }
}

cutfObject* cutfVehicleModelObject::Clone() const
{
    cutfVehicleModelObject *pVehicleModelObject = rage_new cutfVehicleModelObject( m_iObjectId, m_cName, m_StreamingName, m_AnimStreamingBase, m_cAnimExportCtrlSpecFile, m_cAnimCompressionFile, 
        m_cRemoveBoneNameList, m_attributeList );
	pVehicleModelObject->SetHandle( GetHandle() );
	pVehicleModelObject->SetTypeFile( GetTypeFile() );
    return pVehicleModelObject;
}

bool cutfVehicleModelObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfVehicleModelObject::operator != ( const cutfObject &other ) const
{
    if ( cutfModelObject::operator !=( other ) )
    {
        return true;
    }

    const cutfVehicleModelObject *pOtherVehicleModelObject = dynamic_cast<const cutfVehicleModelObject *>( &other );
    const atArray<atString> &otherRemoveBoneNameList = pOtherVehicleModelObject->GetRemoveBoneNameList();
    if ( m_cRemoveBoneNameList.GetCount() != otherRemoveBoneNameList.GetCount() )
    {
        return true;
    }

    for ( int i = 0; i < m_cRemoveBoneNameList.GetCount(); ++i )
    {
        if ( strcmp( m_cRemoveBoneNameList[i].c_str(), otherRemoveBoneNameList[i].c_str() ) != 0 )
        {
            return true;
        }
    }

    return false;
}

//##############################################################################


cutfPropModelObject::cutfPropModelObject()
: cutfModelObject()
{

}

cutfPropModelObject::cutfPropModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
                                         const char *pAnimCompressionFile )
: cutfModelObject( iObjectId, pName, pStreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile )
{
    
}

cutfPropModelObject::cutfPropModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
                                         const char *pAnimCompressionFile, const parAttributeList& attributes )
: cutfModelObject( iObjectId, pName, pStreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes )
{
    
}

cutfPropModelObject::cutfPropModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, const char* pAnimExportCtrlSpecFile, 
										 const char *pAnimCompressionFile, const parAttributeList& attributes )
										 : cutfModelObject( iObjectId, Name, StreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes )
{

}

cutfPropModelObject::cutfPropModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, atHashString pAnimExportCtrlSpecFile, 
										 atHashString pAnimCompressionFile, const parAttributeList& attributes )
										 : cutfModelObject( iObjectId, Name, StreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes )
{

}


cutfObject* cutfPropModelObject::Clone() const
{
    cutfPropModelObject *pPropModelObject = rage_new cutfPropModelObject( m_iObjectId, m_cName, m_StreamingName, m_AnimStreamingBase, m_cAnimExportCtrlSpecFile, m_cAnimCompressionFile, m_attributeList );
	pPropModelObject->SetHandle( GetHandle() );
	pPropModelObject->SetTypeFile( GetTypeFile() );
    return pPropModelObject;
}

//##############################################################################


cutfWeaponModelObject::cutfWeaponModelObject()
: cutfModelObject(),
m_GenericWeaponType(0)
{

}

cutfWeaponModelObject::cutfWeaponModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const u32 u32GenericWeaponType, const char* pAnimExportCtrlSpecFile, 
										 const char *pAnimCompressionFile )
										 : cutfModelObject( iObjectId, pName, pStreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile ),
										 m_GenericWeaponType(u32GenericWeaponType)
{

}

cutfWeaponModelObject::cutfWeaponModelObject( s32 iObjectId, const char *pName, const char *pStreamingName, u32 AnimBaseHash, const u32 u32GenericWeaponType, const char* pAnimExportCtrlSpecFile, 
										 const char *pAnimCompressionFile, const parAttributeList& attributes )
										 : cutfModelObject( iObjectId, pName, pStreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes ),
										 m_GenericWeaponType(u32GenericWeaponType)
{

}


cutfWeaponModelObject::cutfWeaponModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, const u32 u32GenericWeaponType, const char* pAnimExportCtrlSpecFile, 
											 const char *pAnimCompressionFile, const parAttributeList& attributes )
											 : cutfModelObject( iObjectId, Name, StreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes ),
											 m_GenericWeaponType(u32GenericWeaponType)
{

}

cutfWeaponModelObject::cutfWeaponModelObject( s32 iObjectId, atHashString Name, atHashString StreamingName, u32 AnimBaseHash, const u32 u32GenericWeaponType, atHashString pAnimExportCtrlSpecFile, 
											 atHashString pAnimCompressionFile, const parAttributeList& attributes )
											 : cutfModelObject( iObjectId, Name, StreamingName, AnimBaseHash, pAnimExportCtrlSpecFile, pAnimCompressionFile, attributes ),
											 m_GenericWeaponType(u32GenericWeaponType)
{

}


cutfObject* cutfWeaponModelObject::Clone() const
{
	cutfWeaponModelObject *pWeaponModelObject = rage_new cutfWeaponModelObject( m_iObjectId, m_cName, m_StreamingName, m_AnimStreamingBase, m_GenericWeaponType, m_cAnimExportCtrlSpecFile, m_cAnimCompressionFile, m_attributeList );
	pWeaponModelObject->SetHandle( GetHandle() );
	pWeaponModelObject->SetTypeFile( GetTypeFile() );
	return pWeaponModelObject;
}


//##############################################################################



cutfFindModelObject::cutfFindModelObject()
: cutfNamedObject()
, m_fRadius( 0.0f )
{
    m_vPosition.Zero();
}

cutfFindModelObject::cutfFindModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius )
: cutfNamedObject( iObjectId, pName )
, m_vPosition( vPos )
, m_fRadius( fRadius )
{

}

cutfFindModelObject::cutfFindModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, pName, attributes )
, m_vPosition( vPos )
, m_fRadius( fRadius )
{

}

cutfFindModelObject::cutfFindModelObject( s32 iObjectId, atHashString pName, const Vector3& vPos, float fRadius, const parAttributeList& attributes )
: cutfNamedObject( iObjectId, pName, attributes )
, m_vPosition( vPos )
, m_fRadius( fRadius )
{

}

bool cutfFindModelObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfFindModelObject::operator != ( const cutfObject &other ) const
{
    if ( cutfNamedObject::operator !=( other ) )
    {
        return true;
    }

    const cutfFindModelObject *pOtherFindModelObject = dynamic_cast<const cutfFindModelObject *>( &other );
    if ( (GetModelType() != pOtherFindModelObject->GetModelType())
        || (GetPosition() != pOtherFindModelObject->GetPosition())
        || (GetRadius() != pOtherFindModelObject->GetRadius()) )
    {
        return true;
    }

    return false;
}

//##############################################################################


cutfHiddenModelObject::cutfHiddenModelObject()
: cutfFindModelObject()
{

}

cutfHiddenModelObject::cutfHiddenModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius )
: cutfFindModelObject( iObjectId, pName, vPos, fRadius )
{

}

cutfHiddenModelObject::cutfHiddenModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius, 
                                             const parAttributeList& attributes )
: cutfFindModelObject( iObjectId, pName, vPos, fRadius, attributes )
{

}

cutfHiddenModelObject::cutfHiddenModelObject( s32 iObjectId, atHashString pName, const Vector3& vPos, float fRadius, 
											 const parAttributeList& attributes )
											 : cutfFindModelObject( iObjectId, pName, vPos, fRadius, attributes )
{

}

cutfObject* cutfHiddenModelObject::Clone() const
{
    return rage_new cutfHiddenModelObject( m_iObjectId, m_cName, m_vPosition, m_fRadius, m_attributeList );
}

//##############################################################################

cutfFixupModelObject::cutfFixupModelObject()
: cutfFindModelObject()
{

}

cutfFixupModelObject::cutfFixupModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius )
: cutfFindModelObject( iObjectId, pName, vPos, fRadius )
{

}

cutfFixupModelObject::cutfFixupModelObject( s32 iObjectId, const char *pName, const Vector3& vPos, float fRadius,
                                           const parAttributeList& attributes )
: cutfFindModelObject( iObjectId, pName, vPos, fRadius, attributes )
{

}

cutfFixupModelObject::cutfFixupModelObject( s32 iObjectId, atHashString pName, const Vector3& vPos, float fRadius,
										   const parAttributeList& attributes )
										   : cutfFindModelObject( iObjectId, pName, vPos, fRadius, attributes )
{

}

cutfObject* cutfFixupModelObject::Clone() const
{
    return rage_new cutfFixupModelObject( m_iObjectId, m_cName, m_vPosition, m_fRadius, m_attributeList );
}

//##############################################################################

cutfBlockingBoundsObject::cutfBlockingBoundsObject()
: cutfNamedObject()
, m_fHeight( 0.0f )
{
    for ( int i = 0; i < 4; ++i )
    {
        m_vCorners[i].Zero();
    }
}

cutfBlockingBoundsObject::cutfBlockingBoundsObject( s32 iObjectId, const char *pName, const Vector3 vCorners[4], float fHeight )
: cutfNamedObject( iObjectId, pName )
, m_fHeight( fHeight )
{
    for ( int i = 0; i < 4; ++i )
    {
        m_vCorners[i] = vCorners[i];
    }
}

cutfBlockingBoundsObject::cutfBlockingBoundsObject( s32 iObjectId, const char *pName, const Vector3 vCorners[4], float fHeight,
                                                   const parAttributeList& attributes )
: cutfNamedObject( iObjectId, pName, attributes )
, m_fHeight( fHeight )
{
    for ( int i = 0; i < 4; ++i )
    {
        m_vCorners[i] = vCorners[i];
    }
}

cutfBlockingBoundsObject::cutfBlockingBoundsObject( s32 iObjectId, atHashString pName, const Vector3 vCorners[4], float fHeight,
												   const parAttributeList& attributes )
												   : cutfNamedObject( iObjectId, pName, attributes )
												   , m_fHeight( fHeight )
{
	for ( int i = 0; i < 4; ++i )
	{
		m_vCorners[i] = vCorners[i];
	}
}

cutfObject* cutfBlockingBoundsObject::Clone() const
{
    return rage_new cutfBlockingBoundsObject( m_iObjectId, m_cName, m_vCorners, m_fHeight, m_attributeList );
}

bool cutfBlockingBoundsObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfBlockingBoundsObject::operator != ( const cutfObject &other ) const
{
    if ( cutfNamedObject::operator != ( other ) )
    {
        return true;
    }

    const cutfBlockingBoundsObject *pOtherBlockingBoundsObject = dynamic_cast<const cutfBlockingBoundsObject *>( &other );
    for ( int i = 0; i < 4; ++i )
    {
        if ( GetCorner( i ) != pOtherBlockingBoundsObject->GetCorner( i ) )
        {
            return true;
        }
    }

    if ( GetHeight() != pOtherBlockingBoundsObject->GetHeight() )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfRemovalBoundsObject::cutfRemovalBoundsObject()
: cutfBlockingBoundsObject()
{

}

cutfRemovalBoundsObject::cutfRemovalBoundsObject( s32 iObjectId, const char *pName, const Vector3 vCorners[4], float fHeight )
: cutfBlockingBoundsObject( iObjectId, pName, vCorners, fHeight )
{

}

cutfRemovalBoundsObject::cutfRemovalBoundsObject( s32 iObjectId, const char *pName, const Vector3 vCorners[4], float fHeight,
                                                 const parAttributeList& attributes )
                                                 : cutfBlockingBoundsObject( iObjectId, pName, vCorners, fHeight, attributes )
{

}

cutfRemovalBoundsObject::cutfRemovalBoundsObject( s32 iObjectId, atHashString Name, const Vector3 vCorners[4], float fHeight,
												 const parAttributeList& attributes )
												 : cutfBlockingBoundsObject( iObjectId, Name, vCorners, fHeight, attributes )
{

}

cutfObject* cutfRemovalBoundsObject::Clone() const
{
    return rage_new cutfRemovalBoundsObject( m_iObjectId, m_cName, m_vCorners, m_fHeight, m_attributeList );
}

//##############################################################################

cutfLightObject::cutfLightObject()
: cutfNamedObject()
, m_vVolumeOuterColourAndIntensity(1.f, 1.f, 1.f, 1.f)
, m_vDirection(0.f,0.f,0.f)
, m_vColour(0.f,0.f,0.f)
, m_vPosition(0.f,0.f,0.f)
, m_fIntensity(0.f)
, m_fFallOff(0.f)
, m_fConeAngle(0.f)
, m_fVolumeIntensity(0.f)
, m_fVolumeSizeScale(0.f)
, m_fCoronaSize(0.f)
, m_fCoronaIntensity(0.f)
, m_fCoronaZBias(0.f)
, m_iLightType(CUTSCENE_SPOT_LIGHT_TYPE)
, m_iLightProperty(LP_NO_PROPERTY)
, m_TextureDictID(0)
, m_AttachParentId(-1)
, m_TextureKey(0)
, m_uLightFlags(0)
, m_uHourFlags(16777215)
, m_AttachBoneHash(0)
, m_uFlashiness(0)
, m_fActivateTime(-1.0f)
{

}

cutfLightObject::cutfLightObject( s32 iObjectId, const char *pName, s32 iLightType, s32 iLightProperty )
: cutfNamedObject( iObjectId, pName )
, m_vVolumeOuterColourAndIntensity(1.f, 1.f, 1.f, 1.f)
, m_vDirection(0.f,0.f,0.f)
, m_vColour(0.f,0.f,0.f)
, m_vPosition(0.f,0.f,0.f)
, m_fIntensity(0.f)
, m_fFallOff(0.f)
, m_fConeAngle(0.f)
, m_fVolumeIntensity(0.f)
, m_fVolumeSizeScale(0.f)
, m_fCoronaSize(0.f)
, m_fCoronaIntensity(0.f)
, m_fCoronaZBias(0.f)
, m_iLightType(iLightType)
, m_iLightProperty(iLightProperty)
, m_TextureDictID(0)
, m_AttachParentId(-1)
, m_TextureKey(0)
, m_uLightFlags(0)
, m_uHourFlags(16777215)
, m_AttachBoneHash(0)
, m_uFlashiness(0)
, m_fInnerConeAngle(0.f)
, m_fExponentialFallOff(0.f)
, m_fActivateTime(-1.0f)
, m_fShadowBlur(0.0f)
{
}

cutfLightObject::cutfLightObject( s32 iObjectId, const char *pName, const Vector3& vPosition, const Vector3& vDirection, 
	s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
	const parAttributeList& attributes )
: cutfNamedObject( iObjectId, pName, attributes )
, m_vVolumeOuterColourAndIntensity(1.f, 1.f, 1.f, 1.f)
, m_vDirection(vDirection)
, m_vColour(vColour)
, m_vPosition(vPosition)
, m_fIntensity(fIntensity)
, m_fFallOff(fFallOff)
, m_fConeAngle(fConeAngle)
, m_fVolumeIntensity(0.f)
, m_fVolumeSizeScale(0.f)
, m_fCoronaSize(0.f)
, m_fCoronaIntensity(0.f)
, m_fCoronaZBias(0.f)
, m_iLightType(iLightType)
, m_iLightProperty(iLightProperty)
, m_TextureDictID(0)
, m_TextureKey(0)
, m_uLightFlags(0)
, m_uHourFlags(16777215)
, m_uFlashiness(0)
, m_fInnerConeAngle(0.f)
, m_fExponentialFallOff(0.f)
, m_fActivateTime(-1.0f)
, m_fShadowBlur(0.0f)
{
}

cutfLightObject::cutfLightObject( s32 iObjectId, const char *pName, const Vector3& vPosition, const Vector3& vDirection, 
 s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
 const Vector4& vVolumeOuterColourAndIntensity, float fVolumeIntensity, float fVolumeSizeScale, float fCoronaSize,
 float fCoronaIntensity, float fCoronaZBias, s32 TextureDict, s32 TextureKey, u32 LightFlags, u32 HourFlags, u8 Flashiness, 
 float InnerConeAngle, float ExponentialFallOff, float ShadowBlur, const parAttributeList& attributes )
 : cutfNamedObject( iObjectId, pName, attributes )
 , m_vVolumeOuterColourAndIntensity(vVolumeOuterColourAndIntensity)
 , m_vDirection(vDirection)
 , m_vColour(vColour)
 , m_vPosition(vPosition)
 , m_fIntensity(fIntensity)
 , m_fFallOff(fFallOff)
 , m_fConeAngle(fConeAngle)
 , m_fVolumeIntensity(fVolumeIntensity)
 , m_fVolumeSizeScale(fVolumeSizeScale)
 , m_fCoronaSize(fCoronaSize)
 , m_fCoronaIntensity(fCoronaIntensity)
 , m_fCoronaZBias(fCoronaZBias)
 , m_iLightType(iLightType)
 , m_iLightProperty(iLightProperty)
 , m_TextureDictID(TextureDict)
 , m_TextureKey(TextureKey)
 , m_uLightFlags(LightFlags)
 , m_uHourFlags(HourFlags)
 , m_uFlashiness(Flashiness)
 , m_fInnerConeAngle(InnerConeAngle)
 , m_fExponentialFallOff(ExponentialFallOff)
 , m_fActivateTime(-1.0f)
 , m_fShadowBlur(ShadowBlur)
{
}

cutfLightObject::cutfLightObject( s32 iObjectId, atHashString Name, const Vector3& vPosition, const Vector3& vDirection, 
 s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
const Vector4& vVolumeOuterColourAndIntensity, float fVolumeIntensity, float fVolumeSizeScale, float fCoronaSize,
 float fCoronaIntensity, float fCoronaZBias, s32 TextureDict, s32 TextureKey, u32 LightFlags, u32 HourFlags, u8 Flashiness, 
 float InnerConeAngle, float ExponentialFallOff, float ShadowBlur, const parAttributeList& attributes )
 : cutfNamedObject( iObjectId, Name, attributes )
 , m_vVolumeOuterColourAndIntensity(vVolumeOuterColourAndIntensity)
 , m_vDirection(vDirection)
 , m_vColour(vColour)
 , m_vPosition(vPosition)
 , m_fIntensity(fIntensity)
 , m_fFallOff(fFallOff)
 , m_fConeAngle(fConeAngle)
 , m_fVolumeIntensity(fVolumeIntensity)
 , m_fVolumeSizeScale(fVolumeSizeScale)
 , m_fCoronaSize(fCoronaSize)
 , m_fCoronaIntensity(fCoronaIntensity)
 , m_fCoronaZBias(fCoronaZBias)
 , m_iLightType(iLightType)
 , m_iLightProperty(iLightProperty)
 , m_TextureDictID(TextureDict)
 , m_TextureKey(TextureKey)
 , m_uLightFlags(LightFlags)
 , m_uHourFlags(HourFlags)
 , m_uFlashiness(Flashiness)
 , m_fInnerConeAngle(InnerConeAngle)
 , m_fExponentialFallOff(ExponentialFallOff)
 , m_fActivateTime(-1.0f)
 , m_fShadowBlur(ShadowBlur)
{
}




cutfObject* cutfLightObject::Clone() const
{
    return rage_new cutfLightObject( m_iObjectId, m_cName, m_vPosition, m_vDirection, m_iLightType, m_iLightProperty, m_vColour, m_fIntensity,
		m_fConeAngle, m_fFallOff, m_vVolumeOuterColourAndIntensity, m_fVolumeIntensity, m_fVolumeSizeScale, m_fCoronaSize,
		m_fCoronaIntensity, m_fCoronaZBias, m_TextureDictID, m_TextureKey, m_uLightFlags, m_uHourFlags, m_uFlashiness, m_fInnerConeAngle,
		m_fExponentialFallOff, m_fShadowBlur, m_attributeList );
}

bool cutfLightObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfLightObject::operator != ( const cutfObject &other ) const
{
    if ( cutfNamedObject::operator != ( other ) )
    {
        return true;
    }

    const cutfLightObject *pOtherLightObject = dynamic_cast<const cutfLightObject *>( &other );
    if ( GetLightType() != pOtherLightObject->GetLightType() )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfAnimatedLightObject::cutfAnimatedLightObject()
: cutfLightObject()
{

}

cutfAnimatedLightObject::cutfAnimatedLightObject( s32 iObjectId, const char *pName, s32 iLightType, s32 iLightProperty, u32 AnimNameBase)
: cutfLightObject( iObjectId, pName, iLightType, iLightProperty)
, m_AnimStreamingBase(AnimNameBase)
{
}

cutfAnimatedLightObject::cutfAnimatedLightObject( s32 iObjectId, const char *pName, const Vector3& vPosition, const Vector3& vDirection, 
								 s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
								 u32 AnimNameBase, const parAttributeList& attributes )
: cutfLightObject( iObjectId, pName, vPosition, vDirection, iLightType, iLightProperty, vColour,  fIntensity,  fConeAngle, fFallOff, attributes )
, m_AnimStreamingBase(AnimNameBase)
{		
}

cutfAnimatedLightObject::cutfAnimatedLightObject( s32 iObjectId, const char *pName, const Vector3& vPosition, const Vector3& vDirection, 
								 s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
								 const Vector4& vVolumeOuterColourAndIntensity, float fVolumeIntensity, float fVolumeSizeScale, 
								 float fCoronaSize, float fCoronaIntensity, float fCoronaZBias, s32 TextureDict, s32 TextureKey, 
								 u32 LightFlags, u32 HourFlags, u8 Flashiness, float InnerConeAngle, float ExponentialFallOff, 
								 float ShadowBlur, u32 AnimNameBase, const parAttributeList& attributes )
 : cutfLightObject( iObjectId, pName, vPosition, vDirection, iLightType, iLightProperty, vColour, fIntensity, fConeAngle, fFallOff, 
 vVolumeOuterColourAndIntensity, fVolumeIntensity, fVolumeSizeScale, fCoronaSize, fCoronaIntensity, 
 fCoronaZBias,TextureDict, TextureKey, LightFlags, HourFlags, Flashiness, InnerConeAngle, ExponentialFallOff, ShadowBlur, attributes)
 , m_AnimStreamingBase(AnimNameBase)
{
}

cutfAnimatedLightObject::cutfAnimatedLightObject( s32 iObjectId, atHashString pName, const Vector3& vPosition, const Vector3& vDirection, 
												 s32 iLightType, s32 iLightProperty, const Vector3& vColour, float fIntensity, float fConeAngle, float fFallOff, 
												 const Vector4& vVolumeOuterColourAndIntensity, float fVolumeIntensity, float fVolumeSizeScale, 
												 float fCoronaSize, float fCoronaIntensity, float fCoronaZBias, s32 TextureDict, s32 TextureKey, 
												 u32 LightFlags, u32 HourFlags, u8 Flashiness, float InnerConeAngle, float ExponentialFallOff, 
												 float fShadowBlur, u32 AnimNameBase, const parAttributeList& attributes )
 : cutfLightObject( iObjectId, pName, vPosition, vDirection, iLightType, iLightProperty, vColour, fIntensity, fConeAngle, fFallOff, 
 vVolumeOuterColourAndIntensity, fVolumeIntensity, fVolumeSizeScale, fCoronaSize, fCoronaIntensity, 
 fCoronaZBias,TextureDict, TextureKey, LightFlags, HourFlags, Flashiness, InnerConeAngle, ExponentialFallOff, fShadowBlur, attributes)
 , m_AnimStreamingBase(AnimNameBase)
{
}

cutfObject* cutfAnimatedLightObject::Clone() const
{
	return rage_new cutfAnimatedLightObject( m_iObjectId, m_cName, m_vPosition, m_vDirection, m_iLightType, m_iLightProperty, m_vColour, m_fIntensity,
		m_fConeAngle, m_fFallOff, m_vVolumeOuterColourAndIntensity, m_fVolumeIntensity, m_fVolumeSizeScale, m_fCoronaSize,
		m_fCoronaIntensity, m_fCoronaZBias, m_TextureDictID, m_TextureKey, m_uLightFlags, m_uHourFlags, m_uFlashiness, m_fInnerConeAngle,
		m_fExponentialFallOff, m_fShadowBlur, m_AnimStreamingBase, m_attributeList );
}


void cutfAnimatedLightObject::SetAnimStreamingBase(const char* Name)
{
	m_AnimStreamingBase = atPartialStringHash(Name); 
}



bool cutfAnimatedLightObject::operator == ( const cutfObject &other ) const
{
	return (*this != other) == false;
}

bool cutfAnimatedLightObject::operator != ( const cutfObject &other ) const
{
	if ( cutfLightObject::operator != ( other ) )
	{
		return true;
	}

	const cutfAnimatedLightObject *pOtherLightObject = dynamic_cast<const cutfAnimatedLightObject *>( &other );
	if ( GetLightType() != pOtherLightObject->GetLightType() )
	{
		return true;
	}

	return false;
}


//##############################################################################

cutfCameraObject::cutfCameraObject()
: cutfNamedAnimatedObject()
, m_fNearDrawDistance(-1.0f)
, m_fFarDrawDistance(-1.0f)
{

}

cutfCameraObject::cutfCameraObject( s32 iObjectId, const char *pName, u32 AnimBaseHash, float fNearDrawDistance, float fFarDrawDistance )
: cutfNamedAnimatedObject( iObjectId, pName, AnimBaseHash )
, m_fNearDrawDistance(fNearDrawDistance)
, m_fFarDrawDistance(fFarDrawDistance)
{

}

cutfCameraObject::cutfCameraObject( s32 iObjectId, const char *pName, u32 AnimBaseHash, float fNearDrawDistance, float fFarDrawDistance,
                                   const parAttributeList& attributes )
: cutfNamedAnimatedObject( iObjectId, pName, AnimBaseHash, attributes )
, m_fNearDrawDistance(fNearDrawDistance)
, m_fFarDrawDistance(fFarDrawDistance)
{

}

cutfCameraObject::cutfCameraObject( s32 iObjectId, atHashString pName, u32 AnimBaseHash, float fNearDrawDistance, float fFarDrawDistance,
								   const parAttributeList& attributes )
								   : cutfNamedAnimatedObject( iObjectId, pName, AnimBaseHash, attributes )
								   , m_fNearDrawDistance(fNearDrawDistance)
								   , m_fFarDrawDistance(fFarDrawDistance)
{

}

cutfObject* cutfCameraObject::Clone() const
{
    return rage_new cutfCameraObject( m_iObjectId, m_cName, m_AnimStreamingBase, m_fNearDrawDistance, m_fFarDrawDistance, m_attributeList );
}

//##############################################################################

cutfAudioObject::cutfAudioObject()
: cutfFinalNamedObject(),
m_fOffset(0)
{

}

cutfAudioObject::cutfAudioObject( s32 iObjectId, const char *pName, const float fOffset )
: cutfFinalNamedObject( iObjectId, pName ),
m_fOffset(fOffset)
{
}

cutfAudioObject::cutfAudioObject( s32 iObjectId, const char *pName, const float fOffset, const parAttributeList& attributes )
: cutfFinalNamedObject( iObjectId, pName, attributes ),
m_fOffset(fOffset)
{
}

bool cutfAudioObject::StaticIsThisType( const char *pName )
{
    const char *pExtension = ASSET.FindExtensionInPath( pName );
    if ( pExtension == NULL )
    {
        return false;
    }

    return (stricmp( pExtension, ".wav" ) == 0) || (stricmp( pExtension, ".aif" ) == 0) || (stricmp( pExtension, ".aiff" ) == 0)
        || (stricmp( pExtension, ".aifc" ) == 0) || (stricmp( pExtension, ".au" ) == 0) || (stricmp( pExtension, ".snd" ) == 0)
        || (stricmp( pExtension, ".ulw" ) == 0) || (stricmp( pExtension, ".mp3" ) == 0) || (stricmp( pExtension, ".mov" ) == 0)
        || (stricmp( pExtension, ".qt" ) == 0) || (stricmp( pExtension, ".avi" ) == 0) || (stricmp( pExtension, ".vfw" ) == 0)
        || (stricmp( pExtension, ".mpeg" ) == 0) || (stricmp( pExtension, ".mp2" ) == 0) || (stricmp( pExtension, ".mp4" ) == 0)
        || (stricmp( pExtension, ".mpg4" ) == 0) || (stricmp( pExtension, ".sd2" ) == 0) || (stricmp( pExtension, ".gsm" ) == 0);
}

const atString& cutfAudioObject::GetDisplayName() const
{
    RAGE_TRACK( cutfAudioObject_GetDisplayName );

    if ( m_displayName.GetLength() == 0 )
    {
        char cDisplayName[CUTSCENE_OBJNAMELEN];
        safecpy( cDisplayName, m_cName.c_str(), sizeof(cDisplayName) );

        const char *pExtension = ASSET.FindExtensionInPath( cDisplayName );
        if ( pExtension != NULL )
        {
            *(const_cast<char *>( pExtension )) = 0;
        }

        m_displayName.Set( cDisplayName, (int)strlen( cDisplayName ), 0 );
    }

    return m_displayName;
}

cutfObject* cutfAudioObject::Clone() const
{
    return rage_new cutfAudioObject( m_iObjectId, m_cName, m_fOffset, m_attributeList );
}

bool cutfAudioObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfAudioObject::operator != ( const cutfObject &other ) const
{
    if ( cutfFinalNamedObject::operator != ( other ) )
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfEventObject::cutfEventObject()
: cutfObject()
{

}

cutfEventObject::cutfEventObject( s32 iObjectId )
: cutfObject( iObjectId )
{

}

cutfEventObject::cutfEventObject( s32 iObjectId, const parAttributeList& attributes )
: cutfObject( iObjectId, attributes )
{

}

bool cutfEventObject::StaticIsThisType( const char *pName )
{
    char lwrName[128];
    safecpy( lwrName, pName, sizeof(lwrName) );
    strlwr( lwrName );

    return (strstr( lwrName, "event" ) != NULL) || (strstr( lwrName, "evt" ) != NULL) 
        || (strstr( lwrName, "evnt" ) != NULL);
}

cutfObject* cutfEventObject::Clone() const
{
    return rage_new cutfEventObject( m_iObjectId, m_attributeList );
}

//##############################################################################

cutfDecalObject::cutfDecalObject()
: cutfNamedStreamedObject()
, 	m_RenderId(0)
{
}

cutfDecalObject::cutfDecalObject(s32 iObjectId, const char *pName, const char *pStreamedName,u32 RenderId)
: cutfNamedStreamedObject(iObjectId,pName, pStreamedName)
, m_RenderId(RenderId)
{
}

cutfDecalObject::cutfDecalObject(s32 iObjectId, const char *pName, const char *pStreamedName,u32 RenderId, const parAttributeList& attributes)
: cutfNamedStreamedObject(iObjectId, pName, pStreamedName, attributes)
, m_RenderId(RenderId)
{
}

cutfDecalObject::cutfDecalObject(s32 iObjectId, atHashString Name, atHashString StreamingName,u32 RenderId, const parAttributeList& attributes)
: cutfNamedStreamedObject(iObjectId, Name, StreamingName, attributes)
, m_RenderId(RenderId)
{
}

cutfObject* cutfDecalObject::Clone() const
{
	cutfDecalObject *pParticleEffectObject = rage_new cutfDecalObject( m_iObjectId, m_cName, m_StreamingName, m_RenderId, m_attributeList);

	return pParticleEffectObject;
}

//##############################################################################

cutfParticleEffectObject::cutfParticleEffectObject()
: cutfNamedStreamedObject()
{
}

cutfParticleEffectObject::cutfParticleEffectObject( s32 iObjectId, const char *pName, const char *pStreamingName, const atHashString athFxListHash)
                                                   : cutfNamedStreamedObject( iObjectId, pName, pStreamingName )
												   , m_athFxListHash(athFxListHash)
{
}

cutfParticleEffectObject::cutfParticleEffectObject( s32 iObjectId, const char *pName,  const char *pStreamingName, const atHashString athFxListHash, const parAttributeList& attributes )
                                                   : cutfNamedStreamedObject( iObjectId, pName, pStreamingName, attributes )
												   , m_athFxListHash(athFxListHash)
{
}

cutfParticleEffectObject::cutfParticleEffectObject( s32 iObjectId, atHashString Name,  atHashString StreamingName, const atHashString athFxListHash, const parAttributeList& attributes )
: cutfNamedStreamedObject( iObjectId, Name, StreamingName, attributes )
, m_athFxListHash(athFxListHash)
{
}


cutfObject* cutfParticleEffectObject::Clone() const
{
    cutfParticleEffectObject *pParticleEffectObject 
        = rage_new cutfParticleEffectObject( m_iObjectId, m_cName, m_StreamingName, m_athFxListHash, m_attributeList );

    return pParticleEffectObject;
}

bool cutfParticleEffectObject::operator == ( const cutfObject &other ) const
{
    return (*this != other) == false;
}

bool cutfParticleEffectObject::operator != ( const cutfObject &other ) const
{
    if ( cutfNamedStreamedObject::operator != ( other ) )
    {
        return true;
    }

    const cutfParticleEffectObject *pOtherParticleEffectObject = dynamic_cast<const cutfParticleEffectObject *>( &other );
    if (GetFxListHash() != pOtherParticleEffectObject->GetFxListHash())
    {
        return true;
    }

    return false;
}

//##############################################################################

cutfAnimatedParticleEffectObject::cutfAnimatedParticleEffectObject()
: cutfNamedAnimatedStreamedObject()
{
}

cutfAnimatedParticleEffectObject::cutfAnimatedParticleEffectObject( s32 iObjectId, const char *pName, const char *pStreamedName, u32 AnimStreamBase )
                                                   : cutfNamedAnimatedStreamedObject( iObjectId, pName, pStreamedName, AnimStreamBase )
{
}

cutfAnimatedParticleEffectObject::cutfAnimatedParticleEffectObject( s32 iObjectId, const char *pName, const char *pStreamedName, u32 AnimStreamBase, const atHashString athFxListHash,const parAttributeList& attributes )
                                                   : cutfNamedAnimatedStreamedObject( iObjectId, pName, pStreamedName, AnimStreamBase, attributes )
{
    m_athFxListHash = athFxListHash;
}

cutfAnimatedParticleEffectObject::cutfAnimatedParticleEffectObject( s32 iObjectId, atHashString Name, atHashString StreamedName, u32 AnimStreamBase, const atHashString athFxListHash,const parAttributeList& attributes )
: cutfNamedAnimatedStreamedObject( iObjectId, Name, StreamedName, AnimStreamBase, attributes )
{
	m_athFxListHash = athFxListHash;
}


cutfObject* cutfAnimatedParticleEffectObject::Clone() const
{
    return rage_new cutfAnimatedParticleEffectObject( m_iObjectId, m_cName, m_StreamingName, m_AnimStreamingBase, m_athFxListHash, m_attributeList );
}

//##############################################################################

cutfAnimationManagerObject::cutfAnimationManagerObject()
: cutfObject()
{

}

cutfAnimationManagerObject::cutfAnimationManagerObject( s32 iObjectId )
: cutfObject( iObjectId )
{

}

cutfAnimationManagerObject::cutfAnimationManagerObject( s32 iObjectId, const parAttributeList& attributes )
: cutfObject( iObjectId, attributes )
{

}

cutfObject* cutfAnimationManagerObject::Clone() const
{
    return rage_new cutfAnimationManagerObject( m_iObjectId, m_attributeList );
}

//##############################################################################

cutfRayfireObject::cutfRayfireObject()
: cutfNamedStreamedObject()
{

}

cutfRayfireObject::cutfRayfireObject( s32 iObjectId, const char *pName, const char *pStreamingName, Vector3::Param vStartPosition, const parAttributeList& attributes)
: cutfNamedStreamedObject(iObjectId, pName, pStreamingName, attributes)
, m_vStartPosition(vStartPosition)
{
}

cutfRayfireObject::cutfRayfireObject( s32 iObjectId, atHashString Name, atHashString StreamingName, Vector3::Param vStartPosition, const parAttributeList& attributes)
: cutfNamedStreamedObject(iObjectId, Name, StreamingName, attributes)
, m_vStartPosition(vStartPosition)
{
}

cutfObject* cutfRayfireObject::Clone() const
{
    return rage_new cutfRayfireObject( m_iObjectId, m_cName, m_StreamingName, m_vStartPosition, m_attributeList);
}

Vector3 cutfRayfireObject::GetStartPosition() const
{
	return m_vStartPosition;
}

void cutfRayfireObject::SetStartPosition(Vector3::Param vStartPosition)
{
	m_vStartPosition = vStartPosition;
}

//##############################################################################

} // namespace rage
