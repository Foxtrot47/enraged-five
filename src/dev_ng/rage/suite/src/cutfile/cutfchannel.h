// 
// cutfile/cutfchannel.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
// Generated by makechannel.bat, do not modify. 
// 

#ifndef CUTFILE_CUTFCHANNEL_H 
#define CUTFILE_CUTFCHANNEL_H 

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(Cutfile)

#define cutfAssertf(cond,fmt,...)			RAGE_ASSERTF(Cutfile,cond,fmt,##__VA_ARGS__)
#define cutfVerifyf(cond,fmt,...)			RAGE_VERIFYF(Cutfile,cond,fmt,##__VA_ARGS__)
#define cutfErrorf(fmt,...)					RAGE_ERRORF(Cutfile,fmt,##__VA_ARGS__)
#define cutfWarningf(fmt,...)				RAGE_WARNINGF(Cutfile,fmt,##__VA_ARGS__)
#define cutfDisplayf(fmt,...)				RAGE_DISPLAYF(Cutfile,fmt,##__VA_ARGS__)
#define cutfDebugf1(fmt,...)					RAGE_DEBUGF1(Cutfile,fmt,##__VA_ARGS__)
#define cutfDebugf2(fmt,...)					RAGE_DEBUGF2(Cutfile,fmt,##__VA_ARGS__)
#define cutfDebugf3(fmt,...)					RAGE_DEBUGF3(Cutfile,fmt,##__VA_ARGS__)
#define cutfLogf(severity,fmt,...)			RAGE_LOGF(Cutfile,fmt,##__VA_ARGS__)
#define cutfCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,Cutfile,severity,fmt,##__VA_ARGS__)

#endif // CUTFILE_CUTFCHANNEL_H 
