<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
              generate="psoChecks">

  <const name="::rage::RAGE_MAX_PATH" value="256"/>
  <const name="CUTSCENE_LONG_OBJNAMELEN" value="64"/>

  <structdef type="::rage::cutfEventDefs" >
		<string name="m_cHeaderFilename" type="member" size="::rage::RAGE_MAX_PATH"/>
		<string name="m_cHeaderNamespace" type="member" size="CUTSCENE_LONG_OBJNAMELEN"/>
		<array name="m_eventDefList" type="atArray">
			<pointer type="::rage::cutfEventDef" policy="owner"/>
		</array>
    <array name="m_eventArgsDefList" type="atArray">
      <pointer type="::rage::cutfEventArgsDef" policy="owner"/>
    </array>
  </structdef>
	
	<structdef type="::rage::cutfEventDef" onPreLoad="PreLoad" >
		<s32 name="m_iEventIdOffset"/>
		<string name="m_cName" type="member" size="CUTSCENE_LONG_OBJNAMELEN"/>
		<s32 name="m_iRank"/>
		<s32 name="m_iOppositeEventIdOffset"/>
    <s32 name="m_iEventArgsDefIndex"/>
	</structdef>
	
	<structdef type="::rage::cutfEventArgsDef" >
		<s32 name="m_iEventArgsTypeOffset"/>
		<string name="m_cName" type="member" size="CUTSCENE_LONG_OBJNAMELEN"/>
		<struct name="m_attributeList" type="::rage::parAttributeList"/>
    <pointer name="m_cutfAttributes" type="::rage::cutfAttributeList" policy="simple_owner"/>
  </structdef>
	
</ParserSchema>