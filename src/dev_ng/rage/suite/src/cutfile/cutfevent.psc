<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="::rage::cutfChildEvents">
    <array name="m_ChildEvents" type="atArray">
      <u32 />
    </array>
  </structdef>

  <structdef type="::rage::cutfEvent" onPreLoad="PreLoad">
    <pad bytes="8"/><!-- atString m_longDisplayName -->
    <float name="m_fTime"/>
    <s32 name="m_iEventId"/>
    <s32 name="m_iEventArgsIndex"/>
    <pad bytes="4"/><!-- cutfEventArgs *m_pEventArgsPtr; -->
    <pointer name="m_pChildEvents" type="::rage::cutfChildEvents" policy="owner"/>
    <u32 name="m_StickyId"/>
    <bool name="m_IsChild"/>
  </structdef>
 
	<structdef type="::rage::cutfObjectIdEvent" base="::rage::cutfEvent">
		<s32 name="m_iObjectId"/>
	</structdef>
	
	<structdef type="::rage::cutfObjectIdListEvent" base="::rage::cutfEvent" onPreLoad="PreLoad">
		<array name="m_iObjectIdList" type="atArray">
			<s32 />
		</array>
		<array name="m_iEventArgsIndexList" type="atArray">
      		<s32 />
		</array>
	</structdef>

</ParserSchema>