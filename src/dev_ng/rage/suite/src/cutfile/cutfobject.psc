<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
              generate="psoChecks">

  <structdef type="::rage::cutfObject" constructable="false">
			<s32 name="m_iObjectId"/>
			<pad bytes="8"/>  <!-- atString m_displayName -->
			<struct name="m_attributeList" type="::rage::parAttributeList"/>
    <pointer name="m_cutfAttributes" type="::rage::cutfAttributeList" policy="simple_owner"/>
  </structdef>

	<structdef type="::rage::cutfAssetManagerObject" base="::rage::cutfObject">
	</structdef>

	<structdef type="::rage::cutfNamedObject" base="::rage::cutfObject" constructable="false">
    <string name="m_cName" type="atHashString"/>
	</structdef>

  <structdef type="::rage::cutfNamedStreamedObject" base="::rage::cutfNamedObject" constructable="false">
    <string name="m_StreamingName" type="atHashString"/>
  </structdef>

  <structdef type="::rage::cutfNamedAnimatedStreamedObject" base="::rage::cutfNamedStreamedObject" constructable="false">
    <u32 name="m_AnimStreamingBase" init="0"/>
  </structdef>

  <structdef type="::rage::cutfNamedAnimatedObject" base="::rage::cutfNamedObject" constructable="false">
    <u32 name="m_AnimStreamingBase" init="0"/>
  </structdef>
  
  <structdef type="::rage::cutfFinalNamedObject" base="::rage::cutfObject" constructable="false">
    <string name="m_cName" type="atString"/>
  </structdef>
  
	<structdef type="::rage::cutfOverlayObject" base="::rage::cutfFinalNamedObject">
    <string name="m_cRenderTargetName" type="atString"/>
    <u32 name="m_iOverlayType" />
    <string name="m_modelHashName" type="atHashString"/>
  </structdef>

	<structdef type="::rage::cutfScreenFadeObject" base="::rage::cutfNamedObject">
	</structdef>

	<structdef type="::rage::cutfSubtitleObject" base="::rage::cutfNamedObject">
	</structdef>

	<structdef type="::rage::cutfModelObject" base="::rage::cutfNamedAnimatedStreamedObject" constructable="false">
			<string name="m_cAnimExportCtrlSpecFile" type="atHashString"/>
		  <string name="m_cFaceExportCtrlSpecFile" type="atHashString"/>
			<string name="m_cAnimCompressionFile" type="atHashString"/>
		<pad bytes="8"/> <!-- atHashString m_defaultAnimExportCtrlSpecFile, m_defaultAnimCompression -->
      <string name="m_cHandle" type="atHashString"/>
      <string name="m_typeFile" type="atHashString"/>
  </structdef>

	<structdef type="::rage::cutfPedModelObject" base="::rage::cutfModelObject">
    <string name="m_overrideFaceAnimationFilename" type="atHashString"/>
	<pad bytes="4"/> <!-- atHashString m_defaultFaceAnimationFilename -->
    <bool name="m_bFoundFaceAnimation"/>
    <bool name="m_bFaceAndBodyAreMerged"/>
    <bool name="m_bOverrideFaceAnimation"/>
    <string name="m_faceAnimationNodeName" type="atHashString"/>
		<string name="m_faceAttributesFilename" type="atHashString"/>
	</structdef>

	<structdef type="::rage::cutfVehicleModelObject" base="::rage::cutfModelObject">
			<array name="m_cRemoveBoneNameList" type="atArray">
					<string type="atString"/>
			</array>
      <bool name="m_bCanApplyRealDamage"/>
	</structdef>

	<structdef type="::rage::cutfPropModelObject" base="::rage::cutfModelObject">
	</structdef>

  <structdef type="::rage::cutfWeaponModelObject" base="::rage::cutfModelObject">
    <u32 name="m_GenericWeaponType" />
  </structdef>
  
	<structdef type="::rage::cutfFindModelObject" base="::rage::cutfNamedObject" constructable="false">
			<Vector3 name="m_vPosition"/>
			<float name="m_fRadius"/>
	</structdef>

	<structdef type="::rage::cutfHiddenModelObject" base="::rage::cutfFindModelObject">
	</structdef>

	<structdef type="::rage::cutfFixupModelObject" base="::rage::cutfFindModelObject">
	</structdef>

	<structdef type="::rage::cutfBlockingBoundsObject" base="::rage::cutfNamedObject">
			<array name="m_vCorners" type="member" size="4">
					<Vector3 />
			</array>
			<float name="m_fHeight"/>
	</structdef>

	<structdef type="::rage::cutfRemovalBoundsObject" base="::rage::cutfBlockingBoundsObject">
	</structdef>

	<structdef type="::rage::cutfLightObject" base="::rage::cutfNamedObject">
    <pad bytes="16"/>  <!-- m_vVolumeOuterColorAndIntensity -->
    <Vector3 name="m_vDirection" init="0.0f,0.0f,0.0f" />
    <Vector3 name="m_vColour" init="0.0f,0.0f,0.0f" />
    <Vector3 name="m_vPosition" init="0.0f,0.0f,0.0f" />
    <float name="m_fIntensity" init="0.0f" />
    <float name="m_fFallOff" init="0.0f" />
    <float name="m_fConeAngle" init="0.0f" />
    <float name="m_fVolumeIntensity" init="0.0f" />
    <float name="m_fVolumeSizeScale" init="0.0f" />
    <float name="m_fCoronaSize" init="0.0f" />
    <float name="m_fCoronaIntensity" init="0.0f" />
    <float name="m_fCoronaZBias" init="0.0f" />
    <float name="m_fInnerConeAngle" init="0.0f" />
    <float name="m_fExponentialFallOff" init="0.0f" />
    <float name="m_fShadowBlur" init="0.0f"/>
    <s32 name="m_iLightType" init="2" />
    <s32 name="m_iLightProperty" init="0"/>
    <s32 name="m_TextureDictID" init="0" />
    <s32 name="m_TextureKey" init="0" />
    <pad bytes="4"/> <!-- <s32 name="m_AttachParentId"/>-->
    <u32 name="m_uLightFlags" init="0" />
    <u32 name="m_uHourFlags" init="16777215"/>
    <pad bytes="2"/> <!-- u16 m_AttachBoneHash; -->
    <bool name="m_bStatic" init="false" />
    <pad bytes="5"/> <!-- u8 m_uFlashiness; 	float m_fActivateTime; -->
  </structdef>

  <structdef type="::rage::cutfAnimatedLightObject" base="::rage::cutfLightObject">
    <u32 name="m_AnimStreamingBase" init="0"/>
  </structdef>
  
  <structdef type="::rage::cutfCameraObject" base="::rage::cutfNamedAnimatedObject">
		<float name="m_fNearDrawDistance"/>
		<float name="m_fFarDrawDistance"/>
	</structdef>

	<structdef type="::rage::cutfAudioObject" base="::rage::cutfFinalNamedObject">
    <float name="m_fOffset"/>
	</structdef>

	<structdef type="::rage::cutfEventObject" base="::rage::cutfObject">
	</structdef>

	<structdef type="::rage::cutfParticleEffectObject" base="::rage::cutfNamedStreamedObject">
    <string name="m_athFxListHash" type="atHashString"/>
	</structdef>

  <structdef type="::rage::cutfAnimatedParticleEffectObject" base="::rage::cutfNamedAnimatedStreamedObject">
    <string name="m_athFxListHash" type="atHashString"/>
	</structdef>
  
  <structdef type="::rage::cutfAnimationManagerObject" base="::rage::cutfObject">
	</structdef>

	<structdef type="::rage::cutfRayfireObject" base="::rage::cutfNamedStreamedObject">
			<Vector3 name="m_vStartPosition" init="0.0f,0.0f,0.0f" />
	</structdef>

  <structdef type="::rage::cutfDecalObject" base="::rage::cutfNamedStreamedObject">
    <u32 name="m_RenderId"/>
  </structdef>

</ParserSchema>
