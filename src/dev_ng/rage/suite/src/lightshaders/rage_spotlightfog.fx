//
//	PURPOSE
//		Example water / reflection shader
//

#include "../../../base/src/shaderlib/rage_common.fxh"
#include "../../../base/src/shaderlib/rage_skin.fxh"
#include "../../../base/src/shaderlib/rage_fastLight.fxh"

struct VS_OUTPUT
{
	float4	Pos:POSITION;
	
	float4	posX : TEXCOORD1;
	float4	posY : TEXCOORD2;
	float4	posZ : TEXCOORD3;
	
	float	fade: TEXCOORD4;

	float stepSize: TEXCOORD5;
};

 
float4	LightPosition : LightPosition;
float4  LightColor : LightColor;
float4	LightDirection : LightDirection;

float4	LightVolume : LightVolume;

		
		
float4x4 LightMatrix : LightMatrix;
float4x4 LightInvMatrix: LightInvMatrix;

float VolConeIntersection( float3 worldPos , float4x4 VolConeWorldInverseTranspose, out float3 resPos  ) 
{
	resPos = worldPos;
	float3 E = gViewInverse[3].xyz;
	float3 Dir = normalize(gViewInverse[3].xyz - worldPos);
	float3 D;

	E = mul(float4(E, 1.0f), VolConeWorldInverseTranspose).xyz;
	D = normalize(mul(float4(Dir, 0.0f), VolConeWorldInverseTranspose).xyz);
	
	float A = (D.x * D.x) + (D.y * D.y) - (D.z * D.z);
	float B = (2.0f * E.x * D.x) + (2.0f * E.y * D.y) - (2.0f * E.z * D.z);
	float C = (E.x * E.x) + (E.y * E.y) - (E.z * E.z);


	float B2 = B * B;
	float AC4 = 4.0f * A * C;

	if(AC4 > B2) //no real solutions to quadratic equation
	{
		//p1 = worldPos;
		return 0.0f;
	}
	else
	{
		float t1 = (-B + sqrt(B2 - AC4)) / (2*A);
		float t2 = (-B - sqrt(B2 - AC4)) / (2*A);

		float tMin = min(t1, t2);
		float tMax = max(t1, t2);
		
		//return float4(tMin, tMin, tMin, 1.0f);
		//return float4(tMax, tMax, tMax, 1.0f);

		float zMin = E.z + (tMin * D.z);
		float zMax = E.z + (tMax * D.z);

		float isect[2] = {0.0f, 0.0f};
		float zisect[2] = {0.0f, 0.0f};
		int numIntersections = 0;

		//if(tMin >= 0.0f)
		{
			if(zMin > 0.0f)
			{
				if(zMin > 1.0f)
				{
					tMin = (1.0f - E.z) / D.z;
					zMin = E.z + tMin * D.z;
				}

				isect[0] = tMin;
				zisect[0] = zMin;
				numIntersections++;
			}
		}

		//if(tMax >= 0.0f)
		{
			if(zMax > 0.0f)
			{
				if(zMax > 1.0f)
				{
					tMax = (1.0f - E.z) / D.z;
					zMax = E.z + tMax * D.z;
				}

				if(numIntersections == 1)
				{
					isect[1] = tMax;
					zisect[1] = zMax;
				}
				else
				{
					isect[0] = tMax;
					zisect[0] = zMax;
				}
				numIntersections++;
			}
		}


		if(numIntersections == 1)
		{
			isect[1] = (1.0f - E.z) / D.z;
			zisect[1] = E.z + (isect[1] * D.z);
		}

		float3 p1 = E + isect[0] * D;
		float3 p2 = E + isect[1] * D;

		//resPos = worldPos.xyz + Dir.xyz * isect[0];
		float3 v = /*saturate*/(p2 - p1);
		float dist = length(v);

		return dist;
	}
}
 float3 CalculateSpotLightPosition( float3 pos )
 {
	pos.y +=LightVolume.z;
	pos.xz *= pos.y;
	return mul( float4( pos.xzy, 1.0f), (float4x4)LightMatrix ).xyz;
 }
VS_OUTPUT vs_main( rageVertexInputBump IN) 
{
	VS_OUTPUT		OUT;
	
	float width;
	float3 dir;
	float3 spotPos = CalculateSpotLightPosition( IN.pos);
	OUT.Pos = mul( float4( spotPos.xyz, 1.0f), gWorldViewProj);
	spotPos  = mul( float4(spotPos, 1.0f),(float4x4) gWorld).xyz;
	
	float3 view = -normalize( gViewInverse[3].xyz - spotPos );
	
	float3 resPos = spotPos;
	width = VolConeIntersection( spotPos , LightInvMatrix, resPos);
	
	spotPos = resPos;
	float4 steps = float4(0.5, 1.5, 2.5, 3.5 ) * 0.25f;
	
	steps *= width ;
	OUT.stepSize = 1.0f;//width * 0.25f;
	OUT.posX = spotPos.xxxx + steps * view.xxxx;
	OUT.posY = spotPos.yyyy + steps * view.yyyy;
	OUT.posZ = spotPos.zzzz + steps * view.zzzz;
	
	float4 dX;
	float4 dY;
	float4 dZ;
	
	dX = LightPosition.xxxx - OUT.posX;
	dY = LightPosition.yyyy - OUT.posY;
	dZ= LightPosition.zzzz - OUT.posZ;
	OUT.posX = dX;
	OUT.posY = dY;
	OUT.posZ = dZ;
	OUT.fade = ( 1.0f - IN.pos.y ) * 16.0f;
	return OUT;
}
float integrateAccross4( float4 vals, float density )  // performs fast trapiziodal integration for volume lighting starts at w
{
	vals *= density;
	float4 inverseVals = 1.0f - vals;
	
	vals.z = vals.z  + inverseVals.z * vals.w;
	vals.y = vals.y  + inverseVals.y * vals.z;
	vals.x = vals.x  + inverseVals.x * vals.y;
	
	return vals.x;
}
float4 CalculateIntensitySpotFastLight42( float4 dX, float4 dY, float4 dZ, float range, float3 lightPos,
											float3 spotDir, float SpotFadeScale, float  SpotFadeOffset )
{   
	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions	
	float4 recip = rsqrt(dist);		// 4 instructions should be paired on 360

	float4 atten = range;	// 1 instruct
	float4 light = saturate( atten * recip * recip );	
	
	float4 fallOff = dX * -spotDir.xxxx;
	fallOff = dY * -spotDir.yyyy + fallOff;
	fallOff = dZ * -spotDir.zzzz + fallOff;
	fallOff *=recip;
	float4 spotInten = saturate( fallOff * SpotFadeScale + SpotFadeOffset );
	light = max( spotInten * light , 0);
	return light;
}
float4 CalculateIntensitySpotFastLight43( float4 dist, float range,float4 fallOff  )
{   			// 6 instructions	
	float4 recip = 1.0f/dist;		// 4 instructions should be paired on 360
	float4 light = saturate( range * recip * recip );	
	float4 spotInten = saturate( fallOff );
	light = max( spotInten * light , 0);
	return light;
}
float4 ps_main(VS_OUTPUT IN ) : COLOR 
{

	float4 inten4 = CalculateIntensitySpotFastLight42( IN.posX, IN.posY, IN.posZ, LightColor.w, LightPosition.xyz, 
											LightDirection.xyz, 
											LightDirection.w, LightPosition.w );


	float opacity = integrateAccross4( inten4 ,  LightVolume.x * IN.stepSize ) * IN.fade * LightVolume.y;
	float colScale = saturate(opacity - 1.0f ) + 1.0f;  // stop clamping due to alpha
	return  float4(  LightColor.xyz * colScale, opacity );
}

float4 ps_debug(VS_OUTPUT IN ) : COLOR 
{
	return  float4(  LightColor.xyz, LightVolume.x );
}

//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
      ZENABLE = true;
      ZWriteEnable = false;
  	  CullMode = CW;
      ALPHATESTENABLE = false;
      ALPHABLENDENABLE = true;
 #if __XENON
	  HighPrecisionBlendEnable = true;
#endif

	  BlendOp = Add;
      SrcBlend = SRCALPHA;
      DestBlend = INVSRCALPHA;
      //DestBlend = ONE;
      
      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

technique unlit_draw
{
   pass p0
   {
      ZENABLE = true;
      ZWriteEnable = false;
  	  CullMode = CW;
      ALPHATESTENABLE = false;
      ALPHABLENDENABLE = true;
 #if __XENON
	  HighPrecisionBlendEnable = true;
#endif

	  BlendOp = Add;
      SrcBlend = SRCALPHA;
      DestBlend = INVSRCALPHA;
      //DestBlend = ONE;
      
      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}

technique drawDebug
{
   pass p0
   {
      ZENABLE = true;
      ZWriteEnable = false;
  	  CullMode = CW;
      ALPHATESTENABLE = false;
      ALPHABLENDENABLE = true;
 #if __XENON
	  HighPrecisionBlendEnable = true;
#endif

	  BlendOp = Add;
      SrcBlend = SRCALPHA;
      DestBlend = INVSRCALPHA;
      //DestBlend = ONE;
      
      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_debug();
   }
}
