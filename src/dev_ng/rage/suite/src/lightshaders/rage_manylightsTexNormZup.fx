
//-----------------------------------------------------
// Rage Diffuse
//


//#define USE_MAX_ATTENUATION
//#include "rage_manylights.fxh"


//#define USE_MANYSHADOWS
#define USE_LAYERS

#include "rage_manylightsTex.fxh"
#include "rage_approxfilllights.fxh"

#pragma constant 155

float2 AmbientOcclusionScale: AmbientOcclusionScale = { 1.2f, 0.1f };
float BounceStrength : BounceStrength = 1.0f;

float	DisplayLightingOnly : DisplayLightingOnly = 0.0f;


#define EnvironmentStrength	manylights_GroundColor.w
#define DirectStrength	manylights_SkyColor.w


// Ambient Occlusion
float		Metallic
<
	string UIName = "Metallic";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = 0.0f;


float		Bumpiness
<
	string UIName = "Bumpiness";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 4.0;
> = 2.0f;

float		DiffuseIntensity
<
	string UIName = "Diffuse Intensity";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 10.0;
> = 0.5f;

float		SpecularIntensity
<
	string UIName = "Specular Intensity";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = 0.5f;

float		SpecularExponent
<
	string UIName = "Specular Exponent";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 128.0;
> = 24.0f;

#define USE_ANISOTROPIC


BeginSampler(sampler, NormaMapTex, NormaMapSampler, NormaMapTex)
	string UIName="Bump Texture";
    string TextureOutputFormats = "PC=COMP_NRM_DXT5";
ContinueSampler(sampler, NormaMapTex, NormaMapSampler, NormaMapTex)
#ifdef USE_ANISOTROPIC
	MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC; 
	MaxAnisotropy = 4;
#else 
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
#endif
EndSharedSampler;

BeginSampler(sampler, ColorTexure, DiffuseSampler, ColorTexure)
	string UIName="Color Texture";
ContinueSampler(sampler, ColorTexure, DiffuseSampler, ColorTexure)
#ifdef USE_ANISOTROPIC
    MIPFILTER = LINEAR;
    MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC; 
	MaxAnisotropy = 4;
#else 
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
#endif
EndSharedSampler;


struct VertexOutput 
{
    float4 Pos        : POSITION;
    float4 WorldPos	  : TEXCOORD0;
    float4 Normal      : TEXCOORD2;
    float3 Tangent		: TEXCOORD3;
    float3 Binormal		: TEXCOORD4;
    float2 Texcoord0	: TEXCOORD5;
	float3 fillLight 	: TEXCOORD6;
	float2 SortTexCoord :TEXCOORD9;	
};

#define MAKE_GLASS

float4 GetLighting( float3 worldPos, float3 normal,  float3 diffuseColour, float3 ambient )
{
	Irradiance rad;
	rad = manylights_GetLocalLights( worldPos, normal,  manylights_GetGridPositionXY(worldPos), SpecularExponent * 4 );
	rad.ambient = ambient;

#ifdef MAKE_GLASS
	float3 V = normalize( worldPos.xyz  - gViewInverse[3].xyz).xyz;
	float3 R = reflect( V, normal.xyz); // float3(  0.7f, 0.0f, 0.7f);//

	
	
	float3 sunDir = -float3( sin( EnvironmentStrength * 3.142), 0.0f, cos( EnvironmentStrength  * 3.142) );//-normalize( float3( 1.7f,0.0f, 0.7f) );
	float3 sunCol = float3( 1.0f, 1.0f, 0.7f) * 2.0f;
/*
	float3 viewPos = worldPos.xyz + R * 60.0f;
	float3 hieghtOff = ( viewPos.z - 40.0f ) / 100.0f ;
	hieghtOff =saturate( hieghtOff  * 3.0f);
	float3 col = lerp( float3( 0.3, 0.3, 0.2) * 0.5f , float3( 0.65f, 0.75f,1.20f) * 1.0f , hieghtOff ) * 0.6f;
	*/

	rad.ambient = lerp( float3( 0.3, 0.3, 0.4) * 0.7f, float3( 0.65f, 0.75f,1.20f) * 1.0f , normal.z * 0.5f + 0.5f) * 0.6f;

	// add sunSpec
	float3 sun = ragePow( dot( sunDir, R), 30.0f) * sunCol * 2.0f;// * saturate( dot( normal.xyz, sunDir)*16.0f;

	float3 vis = rayhgt_TraceShadowRay(worldPos, R, 60.0f  );

	float3 bentNormal = R;
	bentNormal.z += worldPos.z / 100.0f;
	float3 sky = reflect_reflectionSkyHatZup( GlobalEnvironmentMapSampler, normalize(bentNormal) ); //float3( 0.65f, 0.75f,1.20f) * 1.0f 
	float3 col = lerp( float3( 0.3, 0.3, 0.2) * 0.5f, sky, vis.x ) * 0.6f;
	//col += sun * vis.x;
	rad.ambient = float3( 0.65f, 0.75f,1.20f)  * 1.0f + normal.z * 0.2f;
//	col *=0.1f;
	//rad.specular * 2.0f 
//	return float4( col, fresnelSlick( abs(dot( V, normal)), 0.98f ) );
	float3 sunDiff = saturate( dot( -sunDir, normal.xyz )) * sunCol  * rayhgt_TraceShadowRay(worldPos + normal, -sunDir, 60.0f  );
	rad.diffuse = sunDiff;
	float3 spec = col * 2.0f;//(rad.specular) * 3.0f + col;
	float3 diff = (rad.diffuse + rad.ambient ) * diffuseColour.xyz * DiffuseIntensity * 3.0;

	float Metallic = 0.4f;;
	float3 SpecCol =(1.0f - Metallic )+ diffuseColour.xyz * Metallic ;

	float3 res = diff * 0.2f;
	res += SpecCol * spec * fresnelSlick( abs(dot( V, normal)), 0.96f ) *2.0f;

	R.z += worldPos.z / 100.0f;
	//res = reflect_reflectionSkyHatZup( GlobalEnvironmentMapSampler, normalize(R) );
	return  float4( res , 1.0f);
	

	//vis = rayhgt_GetHieght( worldPos );
	//float blurRange = 0.001f;
	//vis = rayhgt_GetGreaterThanHieght( worldPos.xyz , blurRange);
	return float4( vis.xxx , 1.0f);

#else
	
	float3 SpecCol =(1.0f - Metallic )+ diffuseColour.xyz * Metallic ;

	return  float4( (rad.diffuse + rad.ambient ) * diffuseColour.xyz * DiffuseIntensity * 3.0 + (rad.specular) * SpecCol, 1.0f);
#endif

}
	
//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos =float4( IN.pos.xyz,1.0f);// mul(float4(IN.pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal.xyz = normalize(mul(IN.normal.xyz, (float3x3)gWorld));
	OUT.Normal.w = 	IN.diffuse.w;
	OUT.Texcoord0 = IN.texCoord0;
	
     
	float3 normal = OUT.Normal.xyz;
	float3 binorm = rageComputeBinormal(IN.normal, IN.tangent);
	OUT.Tangent = normalize(mul(IN.tangent.xyz, (float3x3)gWorld));
	OUT.Binormal = normalize(mul(binorm, (float3x3)gWorld));

	OUT.SortTexCoord= 0.0f;//

	OUT.fillLight.xyz = IN.diffuse.xyz;
    return OUT;
}
float3 GetNormal( VertexOutput IN )
{
	float3 norm = tex2D( NormaMapSampler, IN.Texcoord0).xyz * 2.0f - 1.0f;// UncompressNormalMapShaderX2( NormaMapSampler, IN.Texcoord0);
	norm.xy = norm.xy* Bumpiness * 0.25f;
	norm = normalize( norm );

	//return IN.Normal.xyz;
	float3 normWorldSpace = normalize( IN.Normal.xyz * norm.z  + IN.Tangent.xyz * norm.x + IN.Binormal.xyz * norm.y );
	return normWorldSpace;
}
//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS( VertexOutput IN ) : COLOR0
{
	//return float4(manylights_GetGridPositionXY( IN.WorldPos.xyz).yyy, 1.0f);
	float3 norm = GetNormal( IN);
	float3 fill = approxfill_bouncePlaneXY( norm,  IN.fillLight, IN.WorldPos.xyz, 20.0f );

	float3 texCol = tex2D( DiffuseSampler, IN.Texcoord0.xy ).xyz;
	if ( DirectStrength >1.0f )
	{
		texCol = 0.7f;
	}
	float4 col= GetLighting( IN.WorldPos.xyz, norm , texCol, fill * EnvironmentStrength );
	col.xyz *= col.w;

	
	col.w =GlobalTransparency;
	//col.w = 1.0f;
	//col.xyz = reflect_GetHieght( IN.WorldPos.xyz );
//	return col.w;
	//if ( DirectStrength >2.0f )
	//{	
	//	col = IN.fillLight.xyz;
	//}
	return col;
}

technique drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
       // AlphaBlendEnable = false; 
        ZwriteEnable = true;    
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique draw
{
#ifdef MAKE_GLASS
	    pass p0 
    {
        AlphaTestEnable = false;
       // AlphaBlendEnable = false;
        ZwriteEnable = true;   
        ZEnable = true;
        
		CullMode = CW;
        VertexShader = compile VERTEXSHADER VS();
		 PixelShader  = compile PIXELSHADER PS();

    }
	
#else
    pass p0 
    {
        AlphaTestEnable = false;
        //AlphaBlendEnable = false;
        ZwriteEnable = true;   
        ZEnable = true;
        
		CullMode = CW;
        VertexShader = compile VERTEXSHADER VS();
		 PixelShader  = compile PIXELSHADER PS();

    }
#endif

}

technique unlit_drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
       // AlphaBlendEnable = false; 
        ZwriteEnable = true;    
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_draw
{
#ifdef MAKE_GLASS
	    pass p0 
    {
        AlphaTestEnable = false;
       // AlphaBlendEnable = false;
        ZwriteEnable = true;   
        ZEnable = true;
        
		CullMode = CW;
        VertexShader = compile VERTEXSHADER VS();
		 PixelShader  = compile PIXELSHADER PS();

    }
	
#else
    pass p0 
    {
        AlphaTestEnable = false;
        //AlphaBlendEnable = false;
        ZwriteEnable = true;   
        ZEnable = true;
        
		CullMode = CW;
        VertexShader = compile VERTEXSHADER VS();
		 PixelShader  = compile PIXELSHADER PS();

    }
#endif

}
