//-----------------------------------------------------
// Rage Diffuse
//

// Get the pong globals
 #define _SORT_
 #define DISABLE_RAGE_LIGHTING
#define NO_SKINNING
#include "../../../base/src/shaderlib/rage_common.fxh"



#ifdef _SORT_
//#define USE_HL2_LIGHTING	
//#define USE_UPBOUNCE_LIGHTING
//#define USE_BOUNCE_TEXTURE
//#define USE_BOUNCE_TEXTURE_PERVERTEX

#define USE_SIMPLE_ENVLIGHTING
//---------------------------------------------------------------
// SHARED CONSTANTS for Manylights
//------------------------------------------------------------------

#define USE_COMPRESSED 1
#define MAX_SORT_LIGHTS 10
#define MAX_FILLSORT_LIGHTS	10

#include "rage_manyshadows.fxh"

CBSHARED BeginConstantBufferPagedDX10(LightShadow,b6)
shared float4 gLightPositions[ MAX_SORT_LIGHTS ] : LightPositions;
shared float4  gLightColors[ MAX_SORT_LIGHTS] : LightColors;

#if !USE_COMPRESSED
shared float4  gLightDirections[ MAX_SORT_LIGHTS ]: LightDirections;
#endif

shared float3	LightSortScale : LightSortScale;
shared float3	LightSortBias : LightSortBias;

shared float4		manylights_SkyColor : manylights_SkyColor;
shared float4		manylights_GroundColor : manylights_GroundColor;
EndConstantBufferDX10(LightShadow)

float3 LightPositions( int i )
{
	return gLightPositions[i].xyz;
}
#if USE_COMPRESSED
float3 LightDirections( int i )
{
	return frac( gLightColors[i].xyz ) * 2.0f - 1.0f;
}
float3 LightColors( int i )
{
	return ( gLightColors[i].xyz - frac( gLightColors[i].xyz ) ) ;
}

#else
float3 LightDirections( int i )
{
	return gLightDirections[i].xyz ;
}
float3 LightColors( int i )
{
	return gLightColors[i].xyz;
}

#endif
float Atten( int i )
{
	return gLightColors[i].w;
}

BeginSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
ContinueSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSharedSampler;

//----------------------------------------------------------------------

#include "../../../../rage/base/src/shaderlib/rage_shadow_variance.fxh"

#include "../../../../rage/base/src/shaderlib/rage_utility.fxh"
#include "../../../../rage/base/src/shaderlib/rage_halflife2_lighting.fxh"
#include "../../../../rage/base/src/shaderlib/rage_specular.fxh"

float4	FillLightPositions[ MAX_FILLSORT_LIGHTS ] : FillLightPositions;
float4  FillLightColors[ MAX_FILLSORT_LIGHTS ] : FillLightColors;
float4  FillLightDirections[ MAX_FILLSORT_LIGHTS ]: FillLightDirections;

// layer controls
float2 AmbientOcclusionScale: AmbientOcclusionScale = { 1.2f, 0.1f };
float BounceStrength : BounceStrength = 1.0f;

float	DisplayLightingOnly : DisplayLightingOnly = 0.0f;

#define EnvironmentStrength	manylights_GroundColor.w
#define DirectStrength	manylights_SkyColor.w

float	BounceFallOff : BounceFallOff = 1.0f/10.0f;

// Ambient Occlusion
float2	gAmbientOcclusionFadeIn : AmbientOcclusionFadeIn;


float		FresnelRollOff
<
	string UIName = "FresnelRollOff";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = 0.9f;

float		Metallic
<
	string UIName = "Metallic";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = 0.0f;


float		Bumpiness
<
	string UIName = "Bumpiness";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 4.0;
> = 2.0f;

float		DiffuseIntensity
<
	string UIName = "Diffuse Intensity";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 10.0;
> = 0.5f;

float		SpecularIntensity
<
	string UIName = "Specular Intensity";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = 0.5f;

float		SpecularExponent
<
	string UIName = "Specular Exponent";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 128.0;
> = 24.0f;


// Decompression
float3		PositionBias: PositionBias;
float3		PositionScale: PositionScale;

// Directional
//float3		DirectionDir : DirectionDir;
//float3		DirectionCol : DirectionCol;

//float4 ShadowMatCompX[ MAX_SORT_LIGHTS];

float2 GetSpotTexCoords( float4 MatComp, float3 dir , float3 spotDir, float offy )
{
	float2 res;
	float2 offset = float2( MatComp.w, offy );
	float3 matY  = cross( spotDir, MatComp.xyz );  // 2 instructions
	res.x = dot( MatComp.xyz, dir.xyz );		//1	
	res.y = dot( matY.xyz, dir.xyz );			//	2
	res.xy = res.xy / ( dot( dir, spotDir ) ) + offset;  // 6 instructions
	return res.xy;
}
#endif

BeginSampler(sampler, PositionTexture, PositionSampler, PositionTexture)
ContinueSampler(sampler, PositionTexture, PositionSampler, PositionTexture)
	AddressU  = CLAMP;
    AddressV  = CLAMP;
    AddressW  = CLAMP;
    MIPFILTER = NONE;
    MINFILTER = POINT;
	MAGFILTER = POINT; 
EndSharedSampler;


BeginSampler(sampler, ColorT, ColorSampler, ColorT)
ContinueSampler(sampler, ColorT, ColorSampler, ColorT)
	AddressU  = CLAMP;
    AddressV  = CLAMP;
    AddressW  = CLAMP;
    MIPFILTER = NONE;
    MINFILTER = POINT;
	MAGFILTER = POINT; 
EndSharedSampler;


BeginSampler(sampler, HeightTex, HeightSampler, HeightTex)
	string UIName="Height Texture";
    string TextureOutputFormats = "PC=L8";
ContinueSampler(sampler, HeightTex, HeightSampler, HeightTex)
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
EndSharedSampler;

BeginSampler(sampler, ColorTexure, DiffuseSampler, ColorTexure)
	string UIName="Color Texture";
ContinueSampler(sampler, ColorTexure, DiffuseSampler, ColorTexure)
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
EndSharedSampler;



BeginSampler(sampler2D,FillLightSortTexture, FillLightSortSampler,FillLightSortTexture)
ContinueSampler(sampler2D,FillLightSortTexture, FillLightSortSampler,FillLightSortTexture)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSampler;

struct VertexOutput 
{
    float4 Pos        : DX_POS;
    float4 WorldPos	  : TEXCOORD0;
    float4 Normal      : TEXCOORD2;
    float3 Tangent		: TEXCOORD3;
    float3 Binormal		: TEXCOORD4;
    float2 Texcoord0	: TEXCOORD5;
    
#ifdef   USE_HL2_LIGHTING
    float3	HLL1 : TEXCOORD6;
    float3	HLL2 : TEXCOORD7;
    float3	HLL3 : TEXCOORD8;
#endif
   
#ifdef USE_UPBOUNCE_LIGHTING
	float3  HLL1 : TEXCOORD6; 
#endif

#if defined( USE_BOUNCE_TEXTURE ) || defined( USE_BOUNCE_PERVERTEX )
	float3  occ : TEXCOORD6; 
#endif

	float2 SortTexCoord :TEXCOORD9;
	
};
	


BeginSampler(sampler2D,LightSortTexture2, LightSortSampler2,LightSortTexture2)
ContinueSampler(sampler2D,LightSortTexture2, LightSortSampler2,LightSortTexture2)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		MipMapLodBias = 0;
EndSampler;


float2 GetSpotTexCoordsOld( int i , float4 pos )
{
	float2 res;
	res.x = dot( ShadowMatrixX[i], pos );
	res.y = dot( ShadowMatrixY[i], pos );
	return res;
}

float3 DirectionalLighting( float3 normal , float occ )
{
	return lerp( manylights_GroundColor.xyz, manylights_SkyColor.xyz, normal.y * 0.5 + 0.5) * occ;
}


//-----------------------  Reading values from textures

#if __XENON
float4 GetTextureStreamLightPositions( float2 uv , float3 pos, out float4 ldirX, out float4 ldirY, out float4 ldirZ, out float4 atten )
{
	float4 lPosX;
    float4 lPosY;
    float4 lPosZ;
    asm
    {
        tfetch2D lPosX, uv, PositionSampler, UseComputedLOD = false, OffsetX = 0.5
        tfetch2D lPosY, uv, ColorSampler, UseComputedLOD = false, OffsetX = 0.5
        tfetch2D lPosZ, uv, PositionSampler, UseComputedLOD = false, OffsetX = 1.5
        tfetch2D atten, uv, ColorSampler, UseComputedLOD = false, OffsetX = 1.5
    };
	//pos = PositionScale  * pos + PositionBias;
	lPosX = lPosX * PositionScale.x + PositionBias.x;  // this is extra expense
	lPosY = lPosY * PositionScale.y + PositionBias.y;
	lPosZ = lPosZ * PositionScale.z + PositionBias.z;
	atten *= 1024.0f;  // 
	//atten *= 4.0f;
	
	//atten /= PositionScale.x;
	
	ldirX = lPosX - pos.x;
	ldirY = lPosY - pos.y;
	ldirZ = lPosZ - pos.z;

	float4 dist = ldirX * ldirX;
	dist = ldirY * ldirY + dist;
	dist = ldirZ * ldirZ + dist;
	
	//dist /= PositionScale.x;
	return dist;	
}
float4 GetTextureStreamLightSpot( float2 uv , float4 ldirX, float4 ldirY,  float4 ldirZ, float4 recip, out float4 spot )
{
	float4 SDirX;
    float4 SDirY;
    float4 SDirZ;
    
    float4 PenScale;
    float4 PenBias;
	asm
    {
        tfetch2D SDirX, uv, ColorSampler, UseComputedLOD = false, OffsetX = 5.5
        tfetch2D SDirY, uv, ColorSampler, UseComputedLOD = false, OffsetX = 6.5
    //    tfetch2D SDirZ, uv, ColorSampler, UseComputedLOD = false, OffsetX = 7.5   
        
        tfetch2D PenScale, uv, PositionSampler, UseComputedLOD = false, OffsetX = 2.5
        tfetch2D PenBias, uv, PositionSampler, UseComputedLOD = false, OffsetX = 3.5
     };
     
      SDirX = SDirX * 2.0f - 1.0f;	// don't like this  will  use signed format to fix this 
     SDirY = SDirY * 2.0f - 1.0f;
     SDirZ = SDirZ * 2.0f - 1.0f;
     
     SDirZ = -SDirX * SDirX + 1.0f;  // 1.0f - x *x - y*y
     SDirZ = -SDirY * SDirY + SDirZ;
     
     spot = ldirX * SDirX;
     spot = ldirY * SDirY + spot;
     spot = ldirZ * SDirZ + spot;
     spot *= recip;
     PenScale *= 1024.0f;			// don't like this either
     PenBias = -PenBias * 1024.0f + 1.0f;
     float4 resSpot = saturate( spot * PenScale + PenBias );
     
	 return  resSpot;	 
}
float4 GetTextureShadow( float4 worldPos, float2 uv, float4 dist, float4 spot )
{
	float4 lightIndices;
	float4 shadBias;
	float4 shadScale;
	asm
    {
        tfetch2D shadScale, uv, PositionSampler, UseComputedLOD = false, OffsetX = 4.5
        tfetch2D shadBias, uv, PositionSampler, UseComputedLOD = false, OffsetX = 5.5
        tfetch2D lightIndices, uv, LightSortSampler, UseComputedLOD = false, OffsetX = 0.5
     };

	float4 invW;
	float4 shadTex1;
	float4 shadTex2;
	invW = 1.0f/spot;
		
	shadTex1.xy = GetSpotTexCoordsOld( lightIndices.x ,worldPos );
	shadTex1.zw = GetSpotTexCoordsOld( lightIndices.y ,worldPos );
	shadTex2.xy = GetSpotTexCoordsOld( lightIndices.z ,worldPos );
	shadTex2.zw = GetSpotTexCoordsOld( lightIndices.w ,worldPos );
	
	shadTex1.xyzw *= invW.xxyy;
	shadTex2.xyzw *= invW.zzww;
	
	dist = dist * shadScale - shadBias;
	
	float4 shadFade  = saturate( spot * MaxShadowFade.x + MaxShadowFade.y);
	float4 shad =shadowVariance_CompareDepthBy4Precscaled( shadTex1, shadTex2, dist, ShadowAtlasSampler, 0.0001f );

	return shad * shadFade;
}
float3 GetTextureColors( float2 uv , float4 colors )
{
	float4 lColR;
    float4 lColG;
    float4 lColB;
    
    // need to make deal with negative colors
    asm
    {
		tfetch2D lColR, uv, ColorSampler, UseComputedLOD = false, OffsetX = 2.5
        tfetch2D lColG, uv, ColorSampler, UseComputedLOD = false, OffsetX = 3.5
        tfetch2D lColB, uv, ColorSampler, UseComputedLOD = false, OffsetX = 4.5
     };
     
	 float3 col= float3(	dot(lColR, colors ),
					dot( lColG, colors ),
					dot( lColB, colors ) );
	 
	return col; 
};

float3 TextureLight( float2 gridUV, float3 worldPos, float3 normal )
{
	float4 ldirX;
    float4 ldirY;
    float4 ldirZ;
    float4 atten;
	float4 dist = GetTextureStreamLightPositions( gridUV, worldPos, ldirX, ldirY, ldirZ, atten );
	
	float4 recipDist = rsqrt( dist );
	float4 light = ldirX * normal.x;
	light = ldirY * normal.y + light;
	light = ldirZ * normal.z + light;
	light *=   atten * recipDist  * recipDist * recipDist ;
	
	float4 spot;
	light = saturate( light* GetTextureStreamLightSpot( gridUV , ldirX, ldirY, ldirZ, recipDist, spot )  );
	
	float4 shadow = GetTextureShadow(float4(worldPos, 1.0f), gridUV, dist * recipDist, spot );
	return GetTextureColors( gridUV , light * shadow) ;
}

//------------- end

#endif



float4 MaxDiff( float4 t1, float4 t2 )
{
	float4 res;
	float4 res1;
	res1.xy = max( abs(ddx( t1.xy)), abs(ddy(t1.xy)) );
	res1.zw = max( abs(ddx( t1.zw)), abs(ddy(t1.zw)) );
	float4 res2;
	res2.xy = max( abs(ddx( t2.xy)), abs(ddy(t2.xy)));
	res2.zw = max( abs(ddx( t2.zw)), abs(ddy(t2.zw)) );
	res.xy = max( res1.xy, res1.zw );
	res.zw = max( res2.xy, res2.zw );
	return res;
}
float4 GetShadows( float3 V, float4 worldPos, float3 normal , float4 lightIndices, out float4 diffuse, out float4 spec, out float4 bounce  )
{

	float4 attenuation;
	float4 bias;
	float4 eplision;	
	float4 distToLight;
	
	float4 dist2;
	float4 recipDist;
	float4 inten;
	float3 ldir1 = LightPositions( lightIndices.x ).xyz - worldPos.xyz;
	float3 ldir2 = LightPositions( lightIndices.y ).xyz - worldPos.xyz;
	float3 ldir3 = LightPositions( lightIndices.z ).xyz - worldPos.xyz;
	float3 ldir4 = LightPositions( lightIndices.w ).xyz - worldPos.xyz;
		
	
	float4 spot;
	spot.x = -dot( LightDirections( lightIndices.x).xyz, ldir1 );
	spot.y = -dot( LightDirections( lightIndices.y).xyz, ldir2 );
	spot.z = -dot( LightDirections( lightIndices.z).xyz, ldir3 );
	spot.w = -dot( LightDirections( lightIndices.w).xyz, ldir4 );

	
	// 4 distance and normalize instruction
	distToLight.x = dot( ldir1, ldir1 );
	recipDist.x = rsqrt( distToLight.x );
	ldir1 *= recipDist.x;
	
	distToLight.y = dot( ldir2, ldir2 );
	recipDist.y = rsqrt( distToLight.y );
	ldir2 *= recipDist.y;
	
	distToLight.z  = dot( ldir3, ldir3 );
	recipDist.z = rsqrt( distToLight.z  );
	ldir3 *= recipDist.z;
	
	distToLight.w = dot( ldir4, ldir4 );
	recipDist.w = rsqrt( distToLight.w );
	ldir4 *= recipDist.w;
	
	float4 atten;
	
	distToLight *= recipDist;
	
	
	atten.x =Atten(  lightIndices.x);//LightColors( lightIndices.x).w; 
	atten.y =Atten(  lightIndices.y);//LightColors( lightIndices.y).w;
	atten.z =Atten(  lightIndices.z);//LightColors( lightIndices.z).w;	
	atten.w =Atten(  lightIndices.w); //LightColors( lightIndices.w).w;
	
	
	float4 shad =1.0f;// manyshadows_GetShadows( worldPos, lightIndices, distToLight, spot, recipDist  );

	spot *= recipDist;
	inten.x = spot.x * ShadowSettings[ lightIndices.x].z + ShadowSettings[ lightIndices.x ].w;
	inten.y = spot.y * ShadowSettings[ lightIndices.y].z + ShadowSettings[ lightIndices.y ].w;
	inten.z = spot.z * ShadowSettings[ lightIndices.z].z + ShadowSettings[ lightIndices.z ].w;
	inten.w = spot.w * ShadowSettings[ lightIndices.w].z + ShadowSettings[ lightIndices.w ].w;
	
	inten = saturate( inten);
	atten = saturate( atten * recipDist * recipDist );
	
	
	spec = specSpecularBlinn( SpecularExponent, normal, -V, ldir1, ldir2, ldir3, ldir4 );
	
	
	diffuse = float4( saturate( dot( ldir1, normal ) ),
							 saturate( dot( ldir2, normal ) ),
							 saturate( dot( ldir3, normal ) ),
							 saturate( dot( ldir4, normal ) ) );
							 
	
	// calculate bounce amount from directionals
#ifdef LIGHT_BOUNCE	
	float3 upCol = GroundColor * ( -normal.y + 0.75 );
	
	float4 lheight = float4( LightPositions( lightIndices.x ).y,
								LightPositions( lightIndices.y ).y, 
								LightPositions( lightIndices.z ).y,
								LightPositions( lightIndices.w ).y );
	
	lheight += worldPos.yyyy + sqrt(distToLightBounce.xyzw);
	lheight *= lheight;
	
	float4 wrapAmount = 0.75f;
	float4 wrapScale = 1.0f/1.75f;
	
	float3 normal2 = normal;
	normal2.y = -normal2.y;
	bounce = float4( dot( -LightDirections[ lightIndices.x].xyz, normal2 ) ,
							 dot( -LightDirections[ lightIndices.x].xyz, normal2 ),
							 dot( -LightDirections[ lightIndices.x].xyz, normal2 ),
							  dot( -LightDirections[ lightIndices.x].xyz, normal2 ));
	
	float4 bounceDist = ( atten2/ lheight );
	bounce = bounceDist* ( saturate( (bounce +  0.75f) /1.75f) );
#else
	bounce = 0;
#endif
	
	return  atten * shad * inten;
}

float4 LightSortingGetLights( float3 worldPos, float3 normal,  float4 lightIndices, float3 diffuseColour )
{
	float4 diffuse;
	float4 spec;
	float4 bounce;
#if !__XENON
	lightIndices = float4( 0.0f, 1.0f, 2.0f, 3.0f );	// always do first four lights if is pc
#endif
	
	float3 V = normalize( worldPos.xyz  - gViewInverse[3].xyz);
	float4 shadows = GetShadows( V , float4( worldPos, 1.0f), normal, lightIndices, diffuse, spec, bounce );
	

	shadows *= DirectStrength * 2.0f/255.0f; // for compression
	diffuse *= shadows;
	spec *= shadows;
	
	float3 c1 = LightColors( lightIndices.x).xyz;
	float3 c2 = LightColors( lightIndices.y).xyz;
	float3 c3 = LightColors( lightIndices.z).xyz;
	float3 c4 = LightColors( lightIndices.w).xyz;
	
	float3 dcol =  diffuse.x * c1 + 
			diffuse.y * c2 + 
			diffuse.z * c3 + 
			diffuse.w * c4;
			
	float3 scol =   spec.x * c1 + 
			spec.y * c2 + 
			spec.z * c3 + 
			spec.w * c4;
	
	
#ifdef USE_SIMPLE_ENVLIGHTING
	dcol += DirectionalLighting( normal, EnvironmentStrength  ) ;
#endif		
	float fresnel =1.0f;//= saturate( fresnelSlick( abs(dot( -V, normal )), FresnelRollOff ) );
	float3 SpecCol =(1.0f - Metallic )+ diffuseColour.xyz * Metallic ;

	float3 col = dcol * diffuseColour.xyz * DiffuseIntensity * 3.0 + scol * SpecularIntensity * fresnel * SpecCol;
		
	return float4( col, 1.0f );
}
	

void CalculateHL2BounceLighting( float3 worldPos, float3 n, float3 t, float3 b, float4 lightIndices, out float3 c1, out float3 c2, out float3 c3 )
{
	// transform HL2 vectors into world space
	float3 v1 = HalfLifeRed;
	float3 v2 = HalfLifeGreen;
	float3 v3 = HalfLifeBlue;
	
	
	v1 = v1.x * t + v1.y * b + v1.z * n;
	v2 = v2.x * t + v2.y * b + v2.z * n;
	v3 = v3.x * t + v3.y * b + v3.z * n;
	
	
	float3 lit1 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.x ], FillLightDirections[ lightIndices.x], FillLightColors[ lightIndices.x] );
	float3 lit2 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.y ], FillLightDirections[ lightIndices.y], FillLightColors[ lightIndices.y] );
	float3 lit3 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.z ], FillLightDirections[ lightIndices.z], FillLightColors[ lightIndices.z] );
	float3 lit4 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.w ], FillLightDirections[ lightIndices.w], FillLightColors[ lightIndices.w] );
	
	c1 = lit1.x * FillLightColors[ lightIndices.x].xyz + lit2.x * FillLightColors[ lightIndices.y].xyz + lit3.x * FillLightColors[ lightIndices.z].xyz + lit4.x * FillLightColors[ lightIndices.w].xyz;
	c2 = lit1.y * FillLightColors[ lightIndices.x].xyz + lit2.y * FillLightColors[ lightIndices.y].xyz + lit3.y * FillLightColors[ lightIndices.z].xyz + lit4.y * FillLightColors[ lightIndices.w].xyz;
	c3 = lit1.z * FillLightColors[ lightIndices.x].xyz + lit2.z * FillLightColors[ lightIndices.y].xyz + lit3.z * FillLightColors[ lightIndices.z].xyz + lit4.z * FillLightColors[ lightIndices.w].xyz;	
	
	c1 = c1 * BounceStrength + DirectionalLighting( v1 , EnvironmentStrength );
	c2 = c2 * BounceStrength + DirectionalLighting( v2 , EnvironmentStrength);
	c3 = c3 * BounceStrength + DirectionalLighting( v3 , EnvironmentStrength);
}
void CalculateUpBounceLighting( float3 worldPos, float3 n, float3 t, float3 b, float4 lightIndices, out float3 c1 )
{
	// transform HL2 vectors into world space
	float3 v1 = n;//normalize( n + float3( 0.0f, -0.5f, 0.0f ) );
	float3 v2 = HalfLifeGreen;
	float3 v3 = HalfLifeBlue;

	float3 lit1 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.x ], FillLightDirections[ lightIndices.x], FillLightColors[ lightIndices.x] );
	float3 lit2 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.y ], FillLightDirections[ lightIndices.y], FillLightColors[ lightIndices.y] );
	float3 lit3 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.z ], FillLightDirections[ lightIndices.z], FillLightColors[ lightIndices.z] );
	float3 lit4 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.w ], FillLightDirections[ lightIndices.w], FillLightColors[ lightIndices.w] );
	
	c1 = lit1.x * FillLightColors[ lightIndices.x].xyz + lit2.x * FillLightColors[ lightIndices.y].xyz + lit3.x * FillLightColors[ lightIndices.z].xyz + lit4.x * FillLightColors[ lightIndices.w].xyz;
	
	c1 = c1 * BounceStrength + DirectionalLighting( v1 , EnvironmentStrength );
	
}
//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos = mul(float4(IN.pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal.xyz = normalize(mul(IN.normal.xyz, (float3x3)gWorld));
	OUT.Normal.w = 	IN.diffuse.x * gAmbientOcclusionFadeIn.x + gAmbientOcclusionFadeIn.y;
	OUT.Texcoord0 = IN.texCoord0;
	
     
	float3 normal = OUT.Normal.xyz;
	//float3 binorm = rageComputeBinormal( IN.normal, IN.tangent);
	//OUT.Tangent = normalize(mul( IN.tangent.xyz, (float3x3)gWorld));
	//OUT.Binormal = normalize(mul(binorm, (float3x3)gWorld));

	float3 binorm = rageComputeBinormal(IN.normal, IN.tangent);
	OUT.Tangent = normalize(mul(IN.tangent.xyz, (float3x3)gWorld));
	OUT.Binormal = normalize(mul(binorm, (float3x3)gWorld));


#ifdef USE_HL2_LIGHTING
	float4 SortTexCoord = float4( OUT.WorldPos.xz* LightSortScale.xz + LightSortBias.xz, 0.0f, 0.0f );
	float4 Indices =  tex2Dlod( FillLightSortSampler, SortTexCoord ) * 255.0f + 0.5f;
	
	CalculateHL2BounceLighting( OUT.WorldPos.xyz, OUT.Normal.xyz, OUT.Tangent.xyz, OUT.Binormal.xyz, Indices, OUT.HLL1.xyz, OUT.HLL2.xyz, OUT.HLL3.xyz );
	
	float occ = saturate( IN.diffuse.x * AmbientOcclusionScale.x + AmbientOcclusionScale.y );
	
	OUT.HLL1 = OUT.HLL1 * occ.xxx;
	OUT.HLL2 = OUT.HLL2 * occ.xxx;
	OUT.HLL3= OUT.HLL3 * occ.xxx;
	
#endif	

#ifdef USE_UPBOUNCE_LIGHTING
	float4 SortTexCoord = float4( OUT.WorldPos.xz* LightSortScale.xz + LightSortBias.xz, 0.0f, 0.0f );
	float4 Indices =  tex2Dlod( FillLightSortSampler, SortTexCoord ) * 255.0f + 0.5f;
	
	CalculateUpBounceLighting( OUT.WorldPos.xyz, OUT.Normal.xyz, OUT.Tangent.xyz, OUT.Binormal.xyz, Indices, OUT.HLL1.xyz );
	
	float occ = saturate( IN.diffuse.x * AmbientOcclusionScale.x + AmbientOcclusionScale.y );
	
	OUT.HLL1 = OUT.HLL1 * occ.xxx;
	
#endif	

#ifdef USE_BOUNCE_TEXTURE
	OUT.occ =CalculateRealTimeGIsaturate(  );IN.diffuse.x * AmbientOcclusionScale.x + AmbientOcclusionScale.y );
#endif

	OUT.SortTexCoord= OUT.WorldPos.xz* LightSortScale.xz + LightSortBias.xz;	

    return OUT;
}

float3 GNormal(sampler2D HeightSam, float2 TexCoord,  float Bump)
{   	
#if __XENON
//return GenerateNormalFromHeightMapFast( HeightSam, TexCoord, 1.0f/256.0f, Bump);

	float4 pos;
	asm
	{
		tfetch2D	pos.x___, TexCoord, HeightSam, OffsetX=0.5, OffsetY = 0.0
		tfetch2D	pos._x__, TexCoord, HeightSam, OffsetX=0.0, OffsetY = 0.5
		tfetch2D	pos.__x_, TexCoord, HeightSam, OffsetX=-0.5, OffsetY = 0.0
		tfetch2D	pos.___x, TexCoord, HeightSam, OffsetX=0.0, OffsetY = -0.5
   	};
   	pos *= Bump;
   	
   	// return this normal -> we might have to transform it into world space
   	return float3( pos.zw - pos.xy, 1.0f );
#else
	return GenerateNormalFromHeightMapFast( HeightSam, TexCoord, 1.0f/256.0f, Bump);
#endif
}


float3 GetNormal( VertexOutput IN, out float normOcc, out float3 bounceCol )
{
	float3 norm = GNormal( HeightSampler, IN.Texcoord0, Bumpiness);
	normOcc = 1.0f;// norm.z;
	
	float3 normWorldSpace = normalize( IN.Normal.xyz * norm.z  + IN.Tangent.xyz * norm.x + IN.Binormal.xyz * norm.y );
	
#ifdef USE_HL2_LIGHTING
	bounceCol =  GetHL2Color( norm ,IN.HLL1, IN.HLL2, IN.HLL3 );	
#else

#ifdef USE_UPBOUNCE_LIGHTING
float3 newNormal =  float3( 0.0f, -4.0f, 0.0f )	+ IN.Tangent.xyz;
	newNormal = normalize( newNormal );
	bounceCol =  IN.HLL1 * ( dot( newNormal, normWorldSpace ) + 0.75 ) / 1.75f;
	
#else
	bounceCol = 1.0f;
#endif
#endif
	
	return normWorldSpace;
}

float ApplyFormFactor( float offset, float3 v,float3  normal , float fallOff )
{
	float res =  saturate( fallOff/ ( offset ) );
	res *= dot(  v, normal ) * ( -normal.y + 1.0 )/2.0f;
	return res;
}	

float3 CalculateRealTimeGI(  VertexOutput IN, float3 normal   )
{	
	// Calculate Approximate region of most bounce 
	float3 bounceNormal =normalize( IN.Normal.xyz + float3( 0.0f, -0.75f, 0.0f ) );
	bounceNormal.y = min( bounceNormal.y, -0.01f);
	
	// Collide with ground plane
	float offset = IN.WorldPos.y / -bounceNormal.y;
	float3 groundHitPos = IN.WorldPos.xyz + offset * bounceNormal;
	
	// Get intensity at that point
	float2 SortTexCoord2 = groundHitPos.xz* LightSortScale.xz + LightSortBias.xz;	
	float3 bounceGround =  tex2D( LightSortSampler2, SortTexCoord2 ).xyz;

	// Apply form factor 	
#ifdef USE_BOUNCE_TEXTURE
	bounceGround *= IN.occ * 3.0f * ApplyFormFactor( offset, bounceNormal, normal , 7.0f );
#endif
	return bounceGround;
}

//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS( VertexOutput IN) : COLOR
{
	float normOcc;
	float3 bounceCol;
	float3 normal = GetNormal( IN, normOcc, bounceCol );
	float3 diff = tex2D( DiffuseSampler, IN.Texcoord0 ).xyz;
	
	IN.SortTexCoord= IN.WorldPos.xz* LightSortScale.xz + LightSortBias.xz;	

	float4 Indices =  tex2D( LightSortSampler, IN.SortTexCoord ) * 255.0f + 0.5f;
	float4 direct =  LightSortingGetLights( IN.WorldPos.xyz,  normal ,  Indices, diff )   ;
	
#ifdef  USE_HL2_LIGHTING
	direct.xyz += bounceCol * diff;
#endif
#ifdef USE_UPBOUNCE_LIGHTING
	direct.xyz += bounceCol * diff * BounceStrength;
#endif
#ifdef USE_BOUNCE_TEXTURE
	float3 upCol = CalculateRealTimeGI(  IN, normal );
	direct.xyz += saturate( upCol) * diff * BounceStrength;
#endif

	return float4( direct );
}

technique drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false; 
        ZwriteEnable = true;    
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ZwriteEnable = true;   
        ZEnable = true;
        
		CullMode = CW;
        VertexShader = compile VERTEXSHADER VS();
		PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false; 
        ZwriteEnable = true;    
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ZwriteEnable = true;   
        ZEnable = true;
        
		CullMode = CW;
        VertexShader = compile VERTEXSHADER VS();
		PixelShader  = compile PIXELSHADER PS();
    }
}
