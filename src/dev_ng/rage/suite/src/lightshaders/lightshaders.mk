SHELL = $(WINDIR)\system32\cmd.exe

ifeq 	($(PLATFORM),psn)
SHADERDIR = psn
SHADEREXT = cgx
else
ifeq 	($(PLATFORM),xenon)
SHADERDIR = fxl_final
SHADEREXT = fxc
else
SHADERDIR	= win32_30
SHADERDIR_30ATI9 = win32_30_atidx9
SHADERDIR_30ATI10 = win32_30_atidx10
SHADERDIR_30NV9 = win32_30_nvdx9
SHADERDIR_30NV10 = win32_30_nvdx10
SHADERDIR_40ATI10 = win32_40_atidx10
SHADERDIR_40NV10 = win32_40_nvdx10
SHADEREXT = fxc
endif
endif

ifeq    ($(RAGE_ASSET_ROOT),)
badEnv:
	@echo $$(RAGE_ASSET_ROOT) env variable is not defined.
	@exit 1
endif

RAGE_DIRECTORY = $(subst \,/,$(RAGE_DIR))

MAKEDEP_PATH = $(subst \,/,$(RS_TOOLSROOT)\bin\coding\makedep.exe)

MAKESHADER_PATH = $(subst \,/,$(RS_TOOLSROOT)\script\coding\shaders\makeshader.bat)

SHADERPATH = $(subst \,/,$(RAGE_ASSET_ROOT)\prt\ManyLights\lib\)

SHADERPATHDOS = $(subst /,\,$(SHADERPATH))

all: alltargets

clean:
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR)/*.$(SHADEREXT))
	if exist $(SHADERDIR)\*.d del $(SHADERDIR)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI9)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI9)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30ATI9)\*.d del $(SHADERDIR_30ATI9)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30ATI10)\*.d del $(SHADERDIR_30ATI10)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV9)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV9)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30NV9)\*.d del $(SHADERDIR_30NV9)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30NV10)\*.d del $(SHADERDIR_30NV10)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_40ATI10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_40ATI10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_40ATI10)\*.d del $(SHADERDIR_40ATI10)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_40NV10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_40NV10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_40NV10)\*.d del $(SHADERDIR_40NV10)\*.d

# FLAGS = -useATGCompiler
FLAGS = -noPerformanceDump -quiet

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_manylightsTexNormZup.$(SHADEREXT)

-include $(SHADERDIR)/rage_manylightsTexNormZup.d

$(SHADERPATH)$(SHADERDIR)/rage_manylightsTexNormZup.$(SHADEREXT): rage_manylightsTexNormZup.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_manylightsTexNormZup.$(SHADEREXT) rage_manylightsTexNormZup.fx $(SHADERDIR)/rage_manylightsTexNormZup.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_manylightsTexNormZup.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsTexNormZup.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_manylightsTexNormZup.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsTexNormZup.$(SHADEREXT): rage_manylightsTexNormZup.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsTexNormZup.$(SHADEREXT) rage_manylightsTexNormZup.fx $(SHADERDIR_30ATI9)/rage_manylightsTexNormZup.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_manylightsTexNormZup.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTexNormZup.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_manylightsTexNormZup.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTexNormZup.$(SHADEREXT): rage_manylightsTexNormZup.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTexNormZup.$(SHADEREXT) rage_manylightsTexNormZup.fx $(SHADERDIR_30ATI10)/rage_manylightsTexNormZup.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_manylightsTexNormZup.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTexNormZup.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsTexNormZup.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_manylightsTexNormZup.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsTexNormZup.$(SHADEREXT): rage_manylightsTexNormZup.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsTexNormZup.$(SHADEREXT) rage_manylightsTexNormZup.fx $(SHADERDIR_30NV9)/rage_manylightsTexNormZup.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_manylightsTexNormZup.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTexNormZup.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_manylightsTexNormZup.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTexNormZup.$(SHADEREXT): rage_manylightsTexNormZup.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTexNormZup.$(SHADEREXT) rage_manylightsTexNormZup.fx $(SHADERDIR_30NV10)/rage_manylightsTexNormZup.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_manylightsTexNormZup.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTexNormZup.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTexNormZup.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_manylightsTexNormZup.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTexNormZup.$(SHADEREXT): rage_manylightsTexNormZup.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTexNormZup.$(SHADEREXT) rage_manylightsTexNormZup.fx $(SHADERDIR_40ATI10)/rage_manylightsTexNormZup.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_manylightsTexNormZup.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTexNormZup.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTexNormZup.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_manylightsTexNormZup.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTexNormZup.$(SHADEREXT): rage_manylightsTexNormZup.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTexNormZup.$(SHADEREXT) rage_manylightsTexNormZup.fx $(SHADERDIR_40NV10)/rage_manylightsTexNormZup.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_manylightsTexNormZup.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTexNormZup.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_manylights.$(SHADEREXT)

-include $(SHADERDIR)/rage_manylights.d

$(SHADERPATH)$(SHADERDIR)/rage_manylights.$(SHADEREXT): rage_manylights.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_manylights.$(SHADEREXT) rage_manylights.fx $(SHADERDIR)/rage_manylights.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_manylights.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylights.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_manylights.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylights.$(SHADEREXT): rage_manylights.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylights.$(SHADEREXT) rage_manylights.fx $(SHADERDIR_30ATI9)/rage_manylights.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_manylights.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylights.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_manylights.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylights.$(SHADEREXT): rage_manylights.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylights.$(SHADEREXT) rage_manylights.fx $(SHADERDIR_30ATI10)/rage_manylights.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_manylights.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylights.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylights.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_manylights.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylights.$(SHADEREXT): rage_manylights.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylights.$(SHADEREXT) rage_manylights.fx $(SHADERDIR_30NV9)/rage_manylights.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_manylights.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylights.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_manylights.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylights.$(SHADEREXT): rage_manylights.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylights.$(SHADEREXT) rage_manylights.fx $(SHADERDIR_30NV10)/rage_manylights.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_manylights.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylights.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylights.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_manylights.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylights.$(SHADEREXT): rage_manylights.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylights.$(SHADEREXT) rage_manylights.fx $(SHADERDIR_40ATI10)/rage_manylights.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_manylights.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylights.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylights.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_manylights.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylights.$(SHADEREXT): rage_manylights.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylights.$(SHADEREXT) rage_manylights.fx $(SHADERDIR_40NV10)/rage_manylights.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_manylights.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylights.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_manylightsTex.$(SHADEREXT)

-include $(SHADERDIR)/rage_manylightsTex.d

$(SHADERPATH)$(SHADERDIR)/rage_manylightsTex.$(SHADEREXT): rage_manylightsTex.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_manylightsTex.$(SHADEREXT) rage_manylightsTex.fx $(SHADERDIR)/rage_manylightsTex.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_manylightsTex.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsTex.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_manylightsTex.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsTex.$(SHADEREXT): rage_manylightsTex.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsTex.$(SHADEREXT) rage_manylightsTex.fx $(SHADERDIR_30ATI9)/rage_manylightsTex.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_manylightsTex.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTex.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_manylightsTex.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTex.$(SHADEREXT): rage_manylightsTex.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTex.$(SHADEREXT) rage_manylightsTex.fx $(SHADERDIR_30ATI10)/rage_manylightsTex.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_manylightsTex.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTex.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsTex.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_manylightsTex.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsTex.$(SHADEREXT): rage_manylightsTex.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsTex.$(SHADEREXT) rage_manylightsTex.fx $(SHADERDIR_30NV9)/rage_manylightsTex.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_manylightsTex.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTex.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_manylightsTex.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTex.$(SHADEREXT): rage_manylightsTex.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTex.$(SHADEREXT) rage_manylightsTex.fx $(SHADERDIR_30NV10)/rage_manylightsTex.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_manylightsTex.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTex.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTex.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_manylightsTex.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTex.$(SHADEREXT): rage_manylightsTex.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTex.$(SHADEREXT) rage_manylightsTex.fx $(SHADERDIR_40ATI10)/rage_manylightsTex.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_manylightsTex.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTex.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTex.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_manylightsTex.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTex.$(SHADEREXT): rage_manylightsTex.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTex.$(SHADEREXT) rage_manylightsTex.fx $(SHADERDIR_40NV10)/rage_manylightsTex.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_manylightsTex.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTex.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_manylightsTexNorm.$(SHADEREXT)

-include $(SHADERDIR)/rage_manylightsTexNorm.d

$(SHADERPATH)$(SHADERDIR)/rage_manylightsTexNorm.$(SHADEREXT): rage_manylightsTexNorm.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_manylightsTexNorm.$(SHADEREXT) rage_manylightsTexNorm.fx $(SHADERDIR)/rage_manylightsTexNorm.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_manylightsTexNorm.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsTexNorm.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_manylightsTexNorm.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsTexNorm.$(SHADEREXT): rage_manylightsTexNorm.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsTexNorm.$(SHADEREXT) rage_manylightsTexNorm.fx $(SHADERDIR_30ATI9)/rage_manylightsTexNorm.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_manylightsTexNorm.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTexNorm.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_manylightsTexNorm.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTexNorm.$(SHADEREXT): rage_manylightsTexNorm.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTexNorm.$(SHADEREXT) rage_manylightsTexNorm.fx $(SHADERDIR_30ATI10)/rage_manylightsTexNorm.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_manylightsTexNorm.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsTexNorm.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsTexNorm.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_manylightsTexNorm.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsTexNorm.$(SHADEREXT): rage_manylightsTexNorm.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsTexNorm.$(SHADEREXT) rage_manylightsTexNorm.fx $(SHADERDIR_30NV9)/rage_manylightsTexNorm.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_manylightsTexNorm.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTexNorm.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_manylightsTexNorm.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTexNorm.$(SHADEREXT): rage_manylightsTexNorm.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTexNorm.$(SHADEREXT) rage_manylightsTexNorm.fx $(SHADERDIR_30NV10)/rage_manylightsTexNorm.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_manylightsTexNorm.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsTexNorm.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTexNorm.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_manylightsTexNorm.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTexNorm.$(SHADEREXT): rage_manylightsTexNorm.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTexNorm.$(SHADEREXT) rage_manylightsTexNorm.fx $(SHADERDIR_40ATI10)/rage_manylightsTexNorm.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_manylightsTexNorm.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsTexNorm.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTexNorm.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_manylightsTexNorm.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTexNorm.$(SHADEREXT): rage_manylightsTexNorm.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTexNorm.$(SHADEREXT) rage_manylightsTexNorm.fx $(SHADERDIR_40NV10)/rage_manylightsTexNorm.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_manylightsTexNorm.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsTexNorm.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_manylightsShadow.$(SHADEREXT)

-include $(SHADERDIR)/rage_manylightsShadow.d

$(SHADERPATH)$(SHADERDIR)/rage_manylightsShadow.$(SHADEREXT): rage_manylightsShadow.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_manylightsShadow.$(SHADEREXT) rage_manylightsShadow.fx $(SHADERDIR)/rage_manylightsShadow.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_manylightsShadow.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsShadow.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_manylightsShadow.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsShadow.$(SHADEREXT): rage_manylightsShadow.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manylightsShadow.$(SHADEREXT) rage_manylightsShadow.fx $(SHADERDIR_30ATI9)/rage_manylightsShadow.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_manylightsShadow.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsShadow.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_manylightsShadow.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsShadow.$(SHADEREXT): rage_manylightsShadow.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsShadow.$(SHADEREXT) rage_manylightsShadow.fx $(SHADERDIR_30ATI10)/rage_manylightsShadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_manylightsShadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manylightsShadow.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsShadow.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_manylightsShadow.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsShadow.$(SHADEREXT): rage_manylightsShadow.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manylightsShadow.$(SHADEREXT) rage_manylightsShadow.fx $(SHADERDIR_30NV9)/rage_manylightsShadow.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_manylightsShadow.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsShadow.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_manylightsShadow.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsShadow.$(SHADEREXT): rage_manylightsShadow.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsShadow.$(SHADEREXT) rage_manylightsShadow.fx $(SHADERDIR_30NV10)/rage_manylightsShadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_manylightsShadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manylightsShadow.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsShadow.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_manylightsShadow.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsShadow.$(SHADEREXT): rage_manylightsShadow.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsShadow.$(SHADEREXT) rage_manylightsShadow.fx $(SHADERDIR_40ATI10)/rage_manylightsShadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_manylightsShadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manylightsShadow.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsShadow.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_manylightsShadow.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsShadow.$(SHADEREXT): rage_manylightsShadow.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsShadow.$(SHADEREXT) rage_manylightsShadow.fx $(SHADERDIR_40NV10)/rage_manylightsShadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_manylightsShadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manylightsShadow.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_spotshadow.$(SHADEREXT)

-include $(SHADERDIR)/rage_spotshadow.d

$(SHADERPATH)$(SHADERDIR)/rage_spotshadow.$(SHADEREXT): rage_spotshadow.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_spotshadow.$(SHADEREXT) rage_spotshadow.fx $(SHADERDIR)/rage_spotshadow.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_spotshadow.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spotshadow.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_spotshadow.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spotshadow.$(SHADEREXT): rage_spotshadow.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spotshadow.$(SHADEREXT) rage_spotshadow.fx $(SHADERDIR_30ATI9)/rage_spotshadow.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_spotshadow.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spotshadow.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_spotshadow.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spotshadow.$(SHADEREXT): rage_spotshadow.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spotshadow.$(SHADEREXT) rage_spotshadow.fx $(SHADERDIR_30ATI10)/rage_spotshadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_spotshadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spotshadow.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_spotshadow.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_spotshadow.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_spotshadow.$(SHADEREXT): rage_spotshadow.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_spotshadow.$(SHADEREXT) rage_spotshadow.fx $(SHADERDIR_30NV9)/rage_spotshadow.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_spotshadow.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spotshadow.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_spotshadow.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_spotshadow.$(SHADEREXT): rage_spotshadow.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spotshadow.$(SHADEREXT) rage_spotshadow.fx $(SHADERDIR_30NV10)/rage_spotshadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_spotshadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spotshadow.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spotshadow.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_spotshadow.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spotshadow.$(SHADEREXT): rage_spotshadow.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spotshadow.$(SHADEREXT) rage_spotshadow.fx $(SHADERDIR_40ATI10)/rage_spotshadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_spotshadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spotshadow.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spotshadow.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_spotshadow.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_spotshadow.$(SHADEREXT): rage_spotshadow.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spotshadow.$(SHADEREXT) rage_spotshadow.fx $(SHADERDIR_40NV10)/rage_spotshadow.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_spotshadow.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spotshadow.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_spotlightfog.$(SHADEREXT)

-include $(SHADERDIR)/rage_spotlightfog.d

$(SHADERPATH)$(SHADERDIR)/rage_spotlightfog.$(SHADEREXT): rage_spotlightfog.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_spotlightfog.$(SHADEREXT) rage_spotlightfog.fx $(SHADERDIR)/rage_spotlightfog.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_spotlightfog.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spotlightfog.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_spotlightfog.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spotlightfog.$(SHADEREXT): rage_spotlightfog.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_spotlightfog.$(SHADEREXT) rage_spotlightfog.fx $(SHADERDIR_30ATI9)/rage_spotlightfog.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_spotlightfog.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spotlightfog.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_spotlightfog.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spotlightfog.$(SHADEREXT): rage_spotlightfog.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spotlightfog.$(SHADEREXT) rage_spotlightfog.fx $(SHADERDIR_30ATI10)/rage_spotlightfog.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_spotlightfog.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_spotlightfog.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_spotlightfog.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_spotlightfog.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_spotlightfog.$(SHADEREXT): rage_spotlightfog.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_spotlightfog.$(SHADEREXT) rage_spotlightfog.fx $(SHADERDIR_30NV9)/rage_spotlightfog.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_spotlightfog.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spotlightfog.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_spotlightfog.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_spotlightfog.$(SHADEREXT): rage_spotlightfog.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spotlightfog.$(SHADEREXT) rage_spotlightfog.fx $(SHADERDIR_30NV10)/rage_spotlightfog.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_spotlightfog.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_spotlightfog.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spotlightfog.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_spotlightfog.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spotlightfog.$(SHADEREXT): rage_spotlightfog.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spotlightfog.$(SHADEREXT) rage_spotlightfog.fx $(SHADERDIR_40ATI10)/rage_spotlightfog.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_spotlightfog.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_spotlightfog.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spotlightfog.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_spotlightfog.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_spotlightfog.$(SHADEREXT): rage_spotlightfog.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spotlightfog.$(SHADEREXT) rage_spotlightfog.fx $(SHADERDIR_40NV10)/rage_spotlightfog.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_spotlightfog.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_spotlightfog.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_manyemissive.$(SHADEREXT)

-include $(SHADERDIR)/rage_manyemissive.d

$(SHADERPATH)$(SHADERDIR)/rage_manyemissive.$(SHADEREXT): rage_manyemissive.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_manyemissive.$(SHADEREXT) rage_manyemissive.fx $(SHADERDIR)/rage_manyemissive.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_manyemissive.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manyemissive.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_manyemissive.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manyemissive.$(SHADEREXT): rage_manyemissive.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_manyemissive.$(SHADEREXT) rage_manyemissive.fx $(SHADERDIR_30ATI9)/rage_manyemissive.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_manyemissive.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manyemissive.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_manyemissive.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manyemissive.$(SHADEREXT): rage_manyemissive.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manyemissive.$(SHADEREXT) rage_manyemissive.fx $(SHADERDIR_30ATI10)/rage_manyemissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_manyemissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_manyemissive.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manyemissive.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_manyemissive.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_manyemissive.$(SHADEREXT): rage_manyemissive.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_manyemissive.$(SHADEREXT) rage_manyemissive.fx $(SHADERDIR_30NV9)/rage_manyemissive.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_manyemissive.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manyemissive.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_manyemissive.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_manyemissive.$(SHADEREXT): rage_manyemissive.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manyemissive.$(SHADEREXT) rage_manyemissive.fx $(SHADERDIR_30NV10)/rage_manyemissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_manyemissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_manyemissive.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manyemissive.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_manyemissive.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manyemissive.$(SHADEREXT): rage_manyemissive.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manyemissive.$(SHADEREXT) rage_manyemissive.fx $(SHADERDIR_40ATI10)/rage_manyemissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_manyemissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_manyemissive.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manyemissive.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_manyemissive.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_manyemissive.$(SHADEREXT): rage_manyemissive.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manyemissive.$(SHADEREXT) rage_manyemissive.fx $(SHADERDIR_40NV10)/rage_manyemissive.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_manyemissive.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_manyemissive.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_neon.$(SHADEREXT)

-include $(SHADERDIR)/rage_neon.d

$(SHADERPATH)$(SHADERDIR)/rage_neon.$(SHADEREXT): rage_neon.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_neon.$(SHADEREXT) rage_neon.fx $(SHADERDIR)/rage_neon.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_neon.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_neon.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_neon.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_neon.$(SHADEREXT): rage_neon.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_neon.$(SHADEREXT) rage_neon.fx $(SHADERDIR_30ATI9)/rage_neon.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_neon.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_neon.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_neon.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_neon.$(SHADEREXT): rage_neon.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_neon.$(SHADEREXT) rage_neon.fx $(SHADERDIR_30ATI10)/rage_neon.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_neon.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_neon.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_neon.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_neon.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_neon.$(SHADEREXT): rage_neon.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_neon.$(SHADEREXT) rage_neon.fx $(SHADERDIR_30NV9)/rage_neon.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_neon.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_neon.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_neon.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_neon.$(SHADEREXT): rage_neon.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_neon.$(SHADEREXT) rage_neon.fx $(SHADERDIR_30NV10)/rage_neon.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_neon.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_neon.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_neon.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_neon.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_neon.$(SHADEREXT): rage_neon.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_neon.$(SHADEREXT) rage_neon.fx $(SHADERDIR_40ATI10)/rage_neon.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_neon.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_neon.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_neon.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_neon.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_neon.$(SHADEREXT): rage_neon.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_neon.$(SHADEREXT) rage_neon.fx $(SHADERDIR_40NV10)/rage_neon.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_neon.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_neon.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)preload.list

$(SHADERPATH)preload.list: preload.list
	if not exist $(SHADERPATHDOS)* mkdir $(SHADERPATHDOS)
	if exist $(SHADERPATHDOS)preload.list del /f $(SHADERPATHDOS)preload.list
	type preload.list > $(SHADERPATHDOS)preload.list

alltargets: $(TARGETS)
	echo DONE > $(SHADERDIR).dummy
