//-----------------------------------------------------
// PURPOSE: Manylights is a forward rendering system for dealing with lots of lights.
//
//#define USE_MANYSHADOWS



 #define DISABLE_RAGE_LIGHTING
#define NO_SKINNING
#include "../../../../rage/base/src/shaderlib/rage_common.fxh"
#include "../../../../rage/base/src/shaderlib/rage_specular.fxh"


#define USE_EIGHT_PER_PIXEL		1


#ifdef USE_MANYSHADOWS
#include "rage_manyshadows.fxh"

BeginSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
ContinueSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSharedSampler;

#endif

BeginSharedSampler(sampler2D,HeightTexture, HeightSampler,HeightTexture, s7)
ContinueSharedSampler(sampler2D,HeightTexture, HeightSampler,HeightTexture, s7)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		MipMapLodBias = 0;
EndSharedSampler;


BeginSharedSampler(sampler2D,GlobalEnvironmentMap, GlobalEnvironmentMapSampler,GlobalEnvironmentMap, s6)
ContinueSharedSampler(sampler2D,GlobalEnvironmentMap, GlobalEnvironmentMapSampler,GlobalEnvironmentMap, s6)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		MipMapLodBias = 0;
EndSharedSampler;


CBSHARED BeginConstantBufferPagedDX10(LightShadow,b6)
//USE_LAYERS
shared float3	LightSortScale : LightSortScale REGISTER2(ps,c65);
shared float3	LightSortBias : LightSortBias  REGISTER2(ps,c66);
// Decompression
//shared float3		PositionBias: PositionBias REGISTER2(ps,c67);
//shared float3		PositionScale: PositionScale REGISTER2(ps,c68);
shared float3		PositionBias: PositionBias REGISTER2(ps,c30);
shared float3		PositionScale: PositionScale REGISTER2(ps,c31);

// ambient 
shared float4		manylights_SkyColor : manylights_SkyColor REGISTER2(ps,c62) = { 0.02f, 0.02f, 0.2f,1.0f};
shared float4		manylights_GroundColor : manylights_GroundColor  REGISTER2(ps,c61)  = { 0.05f, 0.05f, 0.0f, 1.0f};
EndConstantBufferDX10(LightShadow)

struct Irradiance
{
	float3 diffuse;
	float3 specular;
	float3 ambient;
};

BeginSharedSampler(sampler, PositionTexture, PositionSampler, PositionTexture, s11)
ContinueSharedSampler(sampler, PositionTexture, PositionSampler, PositionTexture, s11)
	AddressU  = CLAMP;
    AddressV  = CLAMP;
    AddressW  = CLAMP;
    MIPFILTER = NONE;
    MINFILTER = POINT;
	MAGFILTER = POINT; 
EndSharedSampler;

BeginSharedSampler(sampler, ColorT, ColorSampler, ColorT, s12)
ContinueSharedSampler(sampler, ColorT, ColorSampler, ColorT, s12)
	AddressU  = CLAMP;
    AddressV  = CLAMP;
    AddressW  = CLAMP;
    MIPFILTER = NONE;
    MINFILTER = POINT;
	MAGFILTER = POINT; 
EndSharedSampler;

BeginSharedSampler(sampler, PositionTextureFill, PositionSamplerFill, PositionTextureFill, s14)
ContinueSharedSampler(sampler, PositionTextureFill, PositionSamplerFill, PositionTextureFill, s14)
	AddressU  = CLAMP;
    AddressV  = CLAMP;
    AddressW  = CLAMP;
    MIPFILTER = NONE;
    MINFILTER = POINT;
	MAGFILTER = POINT; 
EndSharedSampler;


BeginSharedSampler(sampler, ColorTFill, ColorSamplerFill, ColorTFill, s15)
ContinueSharedSampler(sampler, ColorTFill, ColorSamplerFill, ColorTFill, s15)
	AddressU  = CLAMP;
    AddressV  = CLAMP;
    AddressW  = CLAMP;
    MIPFILTER = NONE;
    MINFILTER = POINT;
	MAGFILTER = POINT; 
EndSharedSampler;

#ifdef USE_LAYERS

BeginSharedSampler(sampler2D,CellDividerTexture, CellDividerSampler,CellDividerTexture,  s13)
ContinueSharedSampler(sampler2D,CellDividerTexture, CellDividerSampler,CellDividerTexture,  s13)
	AddressU  = CLAMP;
    AddressV  = CLAMP;
    AddressW  = CLAMP;
    MIPFILTER = POINT;
    MINFILTER = POINT;
	MAGFILTER = POINT; 
EndSharedSampler;


// PURPOSE : Using the cellDiverSampler it selects with grid to use
//
// NOTES: Since it's using a texture altas, it's meant return the y compoent
//  for a texture lookup into the final grid
//
float manylights_SelectLayer( float2 gridUV, float height, float layerScale )
{
	height = height * LightSortScale.z + LightSortBias.z;
	float4 heights = tex2D( CellDividerSampler, gridUV.xy );
	float4 d = (height > heights );
	return dot( d, layerScale );
}

#endif

float4 GetTextureStreamLightPositions( sampler2D PosSampler, sampler2D ColSampler, float2 uv , float3 pos, out float4 ldirX, out float4 ldirY, out float4 ldirZ, out float4 atten )
{
	float4 lPosX;
    float4 lPosY;
    float4 lPosZ;
#if __XENON
    asm
    {
        tfetch2D lPosX, uv, PosSampler, UseComputedLOD = false, OffsetX = 0.5
        tfetch2D lPosY, uv, ColSampler, UseComputedLOD = false, OffsetX = 0.5
        tfetch2D lPosZ, uv, PosSampler, UseComputedLOD = false, OffsetX = 1.5
        tfetch2D atten, uv, ColSampler, UseComputedLOD = false, OffsetX = 1.5
    };
#else
		lPosX = 0.0f;
		lPosY = 0.0f;
		lPosZ = 0.0f;
		atten = 0.0f;
#endif
	
	lPosX = lPosX * PositionScale.x + PositionBias.x;  // this is extra expense
	lPosY = lPosY * PositionScale.y + PositionBias.y;
	lPosZ = lPosZ * PositionScale.z + PositionBias.z;

	atten *= 1024.0f; 
	
	ldirX = lPosX - pos.x;
	ldirY = lPosY - pos.y;
	ldirZ = lPosZ - pos.z;

	float4 dist = ldirX * ldirX;
	dist = ldirY * ldirY + dist;
	dist = ldirZ * ldirZ + dist;
	return dist;	
}
float4 GetTextureStreamLightSpotFill( sampler2D PosSampler, sampler2D ColSampler, float2 uv , float4 ldirX, float4 ldirY,  float4 ldirZ, float4 recipDist )
{
	float4 SDirX;
    float4 SDirY;
    float4 SDirZ;
    
    float4 PenScale;
    float4 PenBias;
#if __XENON
	asm
    {
        tfetch2D SDirX, uv, ColSampler, UseComputedLOD = false, OffsetX = 5.5
        tfetch2D SDirY, uv, ColSampler, UseComputedLOD = false, OffsetX = 6.5
         tfetch2D SDirZ, uv, ColSampler, UseComputedLOD = false, OffsetX = 7.5
        
        tfetch2D PenScale, uv, PosSampler, UseComputedLOD = false, OffsetX = 2.5
        tfetch2D PenBias, uv, PosSampler, UseComputedLOD = false, OffsetX = 3.5
     };
#else
		SDirX = 0.0f;
		SDirY = 0.0f;
		SDirZ = 0.0f;
		PenScale = 0.0f;
		PenBias = 0.0f;
#endif
     SDirX = SDirX * 2.0f - 1.0f;	// don't like this  will  use signed format to fix this 
     SDirY = SDirY * 2.0f - 1.0f;
     SDirZ = SDirZ * 2.0f - 1.0f;
         
     float4 spot = ldirX * SDirX;
     spot = ldirY * SDirY + spot;
     spot = ldirZ * SDirZ + spot;
     
   //  PenScale *= ;			// don't like this either
     PenBias =   1.0f -PenBias  * 128.0f;
     float4 resSpot = saturate( spot * PenScale * 128.0f * recipDist + PenBias );
     
	 return  resSpot;	 
}
float4 GetTextureStreamLightSpot( sampler2D PosSampler, sampler2D ColSampler, float2 uv , float4 ldirX, float4 ldirY,  float4 ldirZ, out float4 spot, float4 recipDist )
{
	float4 SDirX;
    float4 SDirY;
    float4 SDirZ;
    
    float4 PenScale;
    float4 PenBias;
#if __XENON
	asm
    {
        tfetch2D SDirX, uv, ColSampler, UseComputedLOD = false, OffsetX = 5.5
        tfetch2D SDirY, uv, ColSampler, UseComputedLOD = false, OffsetX = 6.5
         tfetch2D SDirZ, uv, ColSampler, UseComputedLOD = false, OffsetX = 7.5
        
        tfetch2D PenScale, uv, PosSampler, UseComputedLOD = false, OffsetX = 2.5
        tfetch2D PenBias, uv, PosSampler, UseComputedLOD = false, OffsetX = 3.5
     };
#else
		SDirX = 0.0f;
		SDirY = 0.0f;
		SDirZ = 0.0f;
		PenScale = 0.0f;
		PenBias = 0.0f;
#endif
     SDirX = SDirX * 2.0f - 1.0f;	// don't like this  will  use signed format to fix this 
     SDirY = SDirY * 2.0f - 1.0f;
     SDirZ = SDirZ * 2.0f - 1.0f;
         
     spot = ldirX * SDirX;
     spot = ldirY * SDirY + spot;
     spot = ldirZ * SDirZ + spot;
     
   //  PenScale *= ;			// don't like this either
     PenBias =   1.0f -PenBias  * 128.0f;
     float4 resSpot = saturate( spot * PenScale * 128.0f * recipDist + PenBias );
     
	 return  resSpot;	 
}
void GetTextureStreamColors( sampler2D ColSampler, float2 uv , out float4 cR, out float4 cG,out float4 cB )
{
	float4 lColR;
    float4 lColG;
    float4 lColB;
    
    // need to make deal with negative colors
 #if __XENON
    asm
    {
		tfetch2D lColR, uv, ColSampler, UseComputedLOD = false, OffsetX = 2.5
        tfetch2D lColG, uv, ColSampler, UseComputedLOD = false, OffsetX = 3.5
        tfetch2D lColB, uv, ColSampler, UseComputedLOD = false, OffsetX = 4.5
     };
 #else
		lColR = 0.0f;
		lColG = 0.0f;
		lColB = 0.0f;
#endif    
	cR = lColR;
	cG = lColG;
	cB = lColB;
};
float3 TextureLightFill( float2 gridUV, float3 worldPos, float3 normal )
{
	float4 ldirX;
    float4 ldirY;
    float4 ldirZ;
    float4 atten;
	float4 dist = GetTextureStreamLightPositions( PositionSamplerFill, ColorSamplerFill, gridUV, worldPos, ldirX, ldirY, ldirZ, atten );	
	float4 recipDist = rsqrt( dist );

	float4 light = ldirX * normal.x;
	light = ldirY * normal.y + light;
	light = ldirZ * normal.z + light;
	light *=   atten * recipDist  * recipDist * recipDist;
	light *= saturate( GetTextureStreamLightSpotFill( PositionSamplerFill, ColorSamplerFill, gridUV , ldirX, ldirY, ldirZ, recipDist)  );
	
	// apply colors	
	float4 red;
	float4 green;
	float4 blue;
	
	GetTextureStreamColors( ColorSamplerFill, gridUV, red, green, blue );
	return float3( dot(red, light ), dot( green, light ), dot( blue, light ) );
}
Irradiance TextureLight( float2 gridUV, float3 worldPos, float3 normal, float specExp )
{
#if USE_EIGHT_PER_PIXEL
	float3 fourFill = TextureLightFill( gridUV, worldPos.xyz, normal.xyz );
#endif

	float4 ldirX;
    float4 ldirY;
    float4 ldirZ;
    float4 atten;
	float4 dist = GetTextureStreamLightPositions( PositionSampler, ColorSampler, gridUV, worldPos, ldirX, ldirY, ldirZ, atten );
	
	float4 recipDist = rsqrt( dist );
	dist *= recipDist;
	
	
	float4 spot;
	float4 spotAtten = saturate( GetTextureStreamLightSpot( PositionSampler, ColorSampler, gridUV , ldirX, ldirY, ldirZ,  spot , recipDist)  );
	
	ldirX *=recipDist;
	ldirY *=recipDist;
	ldirZ *=recipDist;
	
	float4 light = ldirX * normal.x;
	light = ldirY * normal.y + light;
	light = ldirZ * normal.z + light;
	light *=  spotAtten* atten * recipDist  * recipDist;
	
	float4 shadow = 1.0f;	
#if 0
#if !__XENON
	float4 Indices = float4( 0.0f, 1.0f, 2.0f, 3.0f );
#else
	float4 Indices;
	asm
    {
        tfetch2D Indices, gridUV, LightSortSampler, UseComputedLOD = false, OffsetX =0.5
     };  
     Indices =  Indices * 255.0f + 0.5f;
#endif

	float4 shadMask = (Indices > MAX_SHADOW_LIGHTS) ? float4(1.0f,1.0f,1.0f,1.0f) : float4(0.0f,0.0f,0.0f,0.0f);
	
	Indices = min( Indices, MAX_SHADOW_LIGHTS );
	
	shadow = manyshadows_GetShadows( float4(worldPos,1.0f), Indices.xyzw, dist, spot, recipDist );
	shadow = saturate( shadMask + shadow );
#endif
	
	float3 V = -normalize( worldPos.xyz  - gViewInverse[3].xyz).xyz;
	float4 spec = specSpecularBlin4( specExp, normal.xyz, V.xyz, ldirX.xyzw, ldirY,ldirZ );

	// apply colors
	float4 diff =  shadow * light;// float4( 1.0f,0.0f,0.0f,0.0f);
	spec *= diff;
	
	float4 red;
	float4 green;
	float4 blue;
	
	GetTextureStreamColors( ColorSampler, gridUV, red, green, blue );
	
	Irradiance rad;
	rad.ambient = 0;
	rad.specular = float3( dot(red, spec ), dot( green, spec ),	dot( blue, spec ) );
	rad.diffuse =float3( dot(red, diff ), dot( green, diff ),	dot( blue, diff ) );

#if USE_EIGHT_PER_PIXEL
	rad.diffuse += fourFill;
#endif
	rad.specular  *=5;
	return rad;
}
// PURPOSE : get the grid position for the manylights in a xz plane world ( Y UP )
//
float2 manylights_GetGridPositonXZ( float3 WorldPos )
{
	return WorldPos.xz* LightSortScale.xz + LightSortBias.xz;	
}
// PURPOSE : get the grid position for the manylights in a xy plane world ( Z UP )
//
float2 manylights_GetGridPositionXY( float3 WorldPos)
{ 
	float2 pos = WorldPos.xy* LightSortScale.xy + LightSortBias.xy;	
#ifdef USE_LAYERS
	pos.y = pos.y * 1.0/3.0 +  manylights_SelectLayer( pos, WorldPos.z , 1.0f/3.0) ;//;; // could place in w componet of LightSortScale
	//pos.y = manylights_SelectLayer( pos, WorldPos.z , 1.0f/3.0f) + 0.5f /( 64.0f * 3.0f);
#endif
	pos.x =(int)( pos * 64.0f  - 1.5f);
	pos.x /=64.0f;
	return pos;
}
float3 manylights_GetCellColor(  float2 gridUV)
{
	float4 red;
	float4 green;
	float4 blue;
	
	GetTextureStreamColors( ColorSampler, gridUV, red, green, blue );
	float4	diff = float4(1.0f,0.0f,0.0f,0.0f);
	return float3( dot(red, diff ), dot( green, diff ),	dot( blue, diff ) );
}
Irradiance manylights_GetLocalLights( float3 worldPos, float3 normal,  float2 gridPosition, float  specExp )
{	
	return TextureLight( gridPosition, worldPos, normal, specExp );	
}


shared float GlobalTransparency : GlobalTransparency = 1.0f;

float rayhgt_GetHieght( float3 worldPos )
{
	return 1.0 - tex2D( HeightSampler, (worldPos.xy* LightSortScale.xy + LightSortBias.xy ) * float2( -1.0f,-1.0f) + float2( 1.0f + 0.5f/128.0f,1.0f + 0.5f/128.0f) ).x;
}
float rayhgt_GetGreaterThanHieght( float3 worldPos, float OneOverRange ) 
{
	worldPos.xyz = worldPos.xyz* LightSortScale.xyz + LightSortBias.xyz;
	worldPos.xyz = worldPos.xyz * float3( -1.0f,-1.0f, -1.0f) + float3( 1.0f,1.0f,1.0f) + float3( 0.5f/128.0f,0.5f/128.0f, 0.0f) ;

	return saturate(  1.0 - ( worldPos.z - tex2D( HeightSampler, worldPos.xy ).x  ) * OneOverRange );
}
float rayhgt_TraceShadowRay( float3 worldPos, float3 R, float dist  )
{
	float blurRange = 1.0f/0.02f;
	float4 vis = float4( rayhgt_GetGreaterThanHieght( worldPos.xyz + R * dist * 0.2, blurRange),
			rayhgt_GetGreaterThanHieght( worldPos.xyz + R * dist * 0.4, blurRange ) ,
			rayhgt_GetGreaterThanHieght( worldPos.xyz + R * dist * 0.6, blurRange ) ,
			rayhgt_GetGreaterThanHieght( worldPos.xyz + R * dist * 0.9, blurRange ) );
	return  min( vis.x, min( vis.y, min( vis.z, vis.w ) ) );
}


float3 reflect_reflectionSkyHatZup( sampler2D envMap, float3 R )
{
	float2 Tex =  R.xy * 0.47 + 0.5;
	return tex2D( envMap, Tex ).xyz;//  * saturate( (R.z + 0.75f) * 2.0f);
}
