//-----------------------------------------------------
// PURPOSE: Manyshadows uses a shadow atlas to allow picking a selection of different shadows per pixel
//
// NOTES: currently optimized to for use with 4 lights at a time.
//
//

#include "../../../../rage/base/src/shaderlib/rage_shadow_variance.fxh"

#define MAX_SHADOW_LIGHTS 29

CBSHARED BeginConstantBufferPagedDX10(ShadowFade,b8)
shared float2	MaxShadowFade : MaxShadowFade REGISTER2(ps,c67);

shared float4  ShadowMatrixX[ MAX_SHADOW_LIGHTS] : manylights_ShadowMatrixX REGISTER2(ps,c68);
shared float4  ShadowMatrixY[ MAX_SHADOW_LIGHTS] : manylights_ShadowMatrixY REGISTER2(ps,c97);
shared float4  ShadowSettings[ MAX_SHADOW_LIGHTS] : manylights_ShadowSettings REGISTER2(ps,c126);
EndConstantBufferDX10(ShadowFade)

BeginSharedSampler( sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9)
ContinueSharedSampler(sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9 )
	AddressU = CLAMP;
	AddressV = CLAMP;
	AddressW = CLAMP;	
	MIPFILTER = LINEAR;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
EndSharedSampler;


float2 manyshadows_GetSpotTexCoords( int i , float4 pos )
{
	float2 res;
	res.x = dot( ShadowMatrixX[i], pos );
	res.y = dot( ShadowMatrixY[i], pos );
	return res;
}
float4 manyshadows_ApplyShadowScaleBias(float4 Indices, float4 dist )
{
	float4 res;
	float4 v;
	v.xy = ShadowSettings[Indices.x].xy;
	v.zw = ShadowSettings[Indices.y].xy;
	
	v /=10.0f;
	res.xy = dist.xy * v.xz + v.yw;
	
	
	v.xy = ShadowSettings[Indices.z].xy;
	v.zw = ShadowSettings[Indices.w].xy;
	v /=10.0f;
	res.zw = dist.zw * v.xz + v.yw;
	
	return res;
}
// PURPOSE: Finds the shadowing for four manylights given their distance from the light
// and the spot angle.
// 
float4 manyshadows_GetShadows( float4 worldPos, float4 Indices, float4 dist, float4 spot, float4 recipDist  )
{
	float4 invW;
	float4 shadTexCoord1;
	float4 shadTexCoord2;
	invW = 1.0f/spot;
	spot *=recipDist;
	
	shadTexCoord1.xy = manyshadows_GetSpotTexCoords( Indices.x ,worldPos);
	shadTexCoord1.zw = manyshadows_GetSpotTexCoords( Indices.y ,worldPos);
	shadTexCoord2.xy = manyshadows_GetSpotTexCoords( Indices.z ,worldPos);
	shadTexCoord2.zw = manyshadows_GetSpotTexCoords( Indices.w ,worldPos);
	
	shadTexCoord1.xyzw *= invW.xxyy;
	shadTexCoord2.xyzw *= invW.zzww;
	float4 newdist = manyshadows_ApplyShadowScaleBias( Indices, dist );
	
	
	float4 shadFade  = saturate( spot * MaxShadowFade.x + MaxShadowFade.y);
	float4 shad = shadowVariance_CompareDepthBy4Precscaled( shadTexCoord1, shadTexCoord2, newdist, ShadowAtlasSampler, 0.0001f );
	//shad =  saturate( shad * shad +  shadFade);
	//return tex2D(ShadowAtlasSampler, shadTexCoord1.xy ).yyyy ;
	//return shad;
	return saturate( shad * shad +  shadFade);
}


