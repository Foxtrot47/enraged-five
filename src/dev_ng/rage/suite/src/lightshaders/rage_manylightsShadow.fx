//-----------------------------------------------------
// Rage Diffuse
//

// Get the pong globals
 #define _SORT_
#define NO_SKINNING

#include "../../../base/src/shaderlib/rage_shadow_variance.fxh"
#include "../../../base/src/shaderlib/rage_common.fxh"

#ifndef _SORT_
#include "../../../base/src/shaderlib/rage_fastLight.fxh"
#endif


#ifdef _SORT_

#define MAX_SORT_LIGHTS 29

//float4	FastLightKdTreeSplit[1] : FastLightKdTreeSplit;

shared float4  ShadowMatrixX[ MAX_SORT_LIGHTS] : manylights_ShadowMatrixX REGISTER2(ps,c68);
shared float4  ShadowMatrixY[ MAX_SORT_LIGHTS] : manylights_ShadowMatrixY REGISTER2(ps,c97);
shared float4  ShadowSettings[ MAX_SORT_LIGHTS] : manylights_ShadowSettings REGISTER2(ps,c126);

float4	LightPositions[ MAX_SORT_LIGHTS ] : LightPositions;
float4  LightColors[ MAX_SORT_LIGHTS] : LightColors;
float4  LightDirections[ MAX_SORT_LIGHTS ]: LightDirections;

// float4	FillLightPositions[ MAX_SORT_LIGHTS ] : FillLightPositions;
// float4  FillLightColors[ MAX_SORT_LIGHTS ] : FillLightColors;
// float4  FillLightDirections[ MAX_SORT_LIGHTS  ]: FillLightDirections;


//float4	FastLightKdTreeLeafValues[1]: FastLightKdTreeLeafValues;  
//float4	FastLightKdTreeVSLeafValues[1]: FastLightKdTreeVSLeafValues;
float3	LightSortScale : LightSortScale;
float3	LightSortBias : LightSortBias;

float2 ShadowTexScale : ShadowTexScale;

float2		MaxShadowFade : MaxShadowFade;

float		Bumpiness : Bumpiness  = 1.0f;

// Decompression
float3		PositionBias: PositionBias;
float3		PositionScale: PositionScale;

// Directional
float3		DirectionDir : DirectionDir;
float3		DirectionCol : DirectionCol;

float3		SkyColor 
<
	string UIName = "Sky Color";
	string UIWidget = "Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = { 0.1f, 0.5f, 0.5f};

float3		GroundColor
<
	string UIName = "Ground Color";
	string UIWidget = "Color";
	float UIMin = 0.0;
	float UIMax = 1.0;
> = { 0.1f, 0.1f, 0.0f};

// Ambient Occlusion
float2	gAmbientOcclusionFadeIn : AmbientOcclusionFadeIn;

//float4 ShadowMatCompX[ MAX_SORT_LIGHTS];

float2 GetSpotTexCoords( float4 MatComp, float3 dir , float3 spotDir, float offy )
{
	float2 res;
	float2 offset = float2( MatComp.w, offy );
	float3 matY  = cross( spotDir, MatComp.xyz );  // 2 instructions
	res.x = dot( MatComp.xyz, dir.xyz );		//1	
	res.y = dot( matY.xyz, dir.xyz );			//	2
	res.xy = res.xy / ( dot( dir, spotDir ) ) + offset;  // 6 instructions
	return res.xy;
}

#endif





BeginSharedSampler( sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9)
ContinueSharedSampler(sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9 )
	AddressU = CLAMP;
	AddressV = CLAMP;
	AddressW = CLAMP;	
	MIPFILTER = LINEAR;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
EndSharedSampler;


BeginSampler(sampler, PositionTexture, PositionSampler, PositionTexture)
ContinueSampler(sampler, PositionTexture, PositionSampler, PositionTexture)
	AddressU  = CLAMP;
    AddressV  = CLAMP;
    AddressW  = CLAMP;
    MIPFILTER = NONE;
    MINFILTER = POINT;
	MAGFILTER = POINT; 
EndSharedSampler;


BeginSampler(sampler, ColorT, ColorSampler, ColorT)
ContinueSampler(sampler, ColorT, ColorSampler, ColorT)
	AddressU  = CLAMP;
    AddressV  = CLAMP;
    AddressW  = CLAMP;
    MIPFILTER = NONE;
    MINFILTER = POINT;
	MAGFILTER = POINT; 
EndSharedSampler;


BeginSampler(sampler, BumpTexture, BumpSampler, BumpTexture)
	string UIName="Bump Map Texture";
    string TextureOutputFormats = "PC=A8R8G8B8";
ContinueSampler(sampler, BumpTexture, BumpSampler, BumpTexture)
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
EndSharedSampler;

struct VertexOutput 
{
    float4 Pos        : POSITION;
    float4 WorldPos	  : TEXCOORD0;
    float4 Normal      : TEXCOORD1;
    float3 Tangent		: TEXCOORD2;
    float3 Binormal		: TEXCOORD3;
    float4 Color		: TEXCOORD4;
    float2 Texcoord0	: TEXCOORD5;
   
};
	
BeginSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
ContinueSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSharedSampler;

BeginSampler(sampler2D,LightSortTexture2, LightSortSampler2,LightSortTexture2)
ContinueSampler(sampler2D,LightSortTexture2, LightSortSampler2,LightSortTexture2)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSampler;



BeginSampler(sampler2D,FillLightSortTexture, FillLightSortSampler,FillLightSortTexture)
ContinueSampler(sampler2D,FillLightSortTexture, FillLightSortSampler,FillLightSortTexture)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSampler;

float3 GetVertexStreamLight( int index )
{
#if __XENON
	float4 pos;
	asm 
	{
		vfetch	pos, index, texcoord0;
	};
	return pos.xyz/100.0f;
#else
	return float3( 1.0f, 1.0f, 0.0f );
#endif
}

float2 GetSpotTexCoordsOld( int i , float4 pos )
{
	float2 res;
	res.x = dot( ShadowMatrixX[i], pos );
	res.y = dot( ShadowMatrixY[i], pos );
	return res;
}
float4 SlickApproxPhong( float4 t )
{
	const float  n = 32.0f;
	const float n1 = ( 1.0f - n );
	return saturate( t / ( n1  * t + n ) );
}

float3 DirectionalLighting( float3 normal , float occ )
{
	return 0.1f;
	//return (saturate( dot( -DirectionDir, normal) * DirectionCol )
	//			+ lerp( GroundColor, SkyColor, normal.y * 0.5 + 0.5) )  * occ ;
}
#if __XENON
float4 GetTextureStreamLightPositions( float2 uv , float3 pos, out float4 ldirX, out float4 ldirY, out float4 ldirZ, out float4 atten )
{
	float4 lPosX;
    float4 lPosY;
    float4 lPosZ;
    asm
    {
        tfetch2D lPosX, uv, PositionSampler, UseComputedLOD = false, OffsetX = 0.5
        tfetch2D lPosY, uv, ColorSampler, UseComputedLOD = false, OffsetX = 0.5
        tfetch2D lPosZ, uv, PositionSampler, UseComputedLOD = false, OffsetX = 1.5
        tfetch2D atten, uv, ColorSampler, UseComputedLOD = false, OffsetX = 1.5
    };
	//pos = PositionScale  * pos + PositionBias;
	lPosX = lPosX * PositionScale.x + PositionBias.x;  // this is extra expense
	lPosY = lPosY * PositionScale.y + PositionBias.y;
	lPosZ = lPosZ * PositionScale.z + PositionBias.z;
	atten *= 1024.0f;  // 
	//atten *= 4.0f;
	
	//atten /= PositionScale.x;
	
	ldirX = lPosX - pos.x;
	ldirY = lPosY - pos.y;
	ldirZ = lPosZ - pos.z;

	float4 dist = ldirX * ldirX;
	dist = ldirY * ldirY + dist;
	dist = ldirZ * ldirZ + dist;
	
	//dist /= PositionScale.x;
	return dist;	
}
float4 GetTextureStreamLightSpot( float2 uv , float4 ldirX, float4 ldirY,  float4 ldirZ, float4 recip, out float4 spot )
{
	float4 SDirX;
    float4 SDirY;
    float4 SDirZ;
    
    float4 PenScale;
    float4 PenBias;
	asm
    {
        tfetch2D SDirX, uv, ColorSampler, UseComputedLOD = false, OffsetX = 5.5
        tfetch2D SDirY, uv, ColorSampler, UseComputedLOD = false, OffsetX = 6.5
        tfetch2D SDirZ, uv, ColorSampler, UseComputedLOD = false, OffsetX = 7.5   
        
        tfetch2D PenScale, uv, PositionSampler, UseComputedLOD = false, OffsetX = 2.5
        tfetch2D PenBias, uv, PositionSampler, UseComputedLOD = false, OffsetX = 3.5
     };
     
      SDirX = SDirX * 2.0f - 1.0f;	// don't like this  will  use signed format to fix this 
     SDirY = SDirY * 2.0f - 1.0f;
     SDirZ = SDirZ * 2.0f - 1.0f;
     
//     SDirZ = -SDirX * SDirX + 1.0f;  // 1.0f - x *x - y*y
//     SDirZ = -SDirY * SDirY + SDirZ;
     
     spot = ldirX * SDirX;
     spot = ldirY * SDirY + spot;
     spot = ldirZ * SDirZ + spot;
     spot *= recip;
     PenScale *= 1024.0f;			// don't like this either
     PenBias = -PenBias * 1024.0f + 1.0f;
     float4 resSpot = saturate( spot * PenScale + PenBias );
     
	 return  resSpot;	 
}
float4 GetTextureShadow( float4 worldPos, float2 uv, float4 dist, float4 spot )
{
	float4 lightIndices;
	float4 shadBias;
	float4 shadScale;
	asm
    {
        tfetch2D shadScale, uv, PositionSampler, UseComputedLOD = false, OffsetX = 4.5
        tfetch2D shadBias, uv, PositionSampler, UseComputedLOD = false, OffsetX = 5.5
        tfetch2D lightIndices, uv, LightSortSampler, UseComputedLOD = false, OffsetX = 0.5
     };

	float4 invW;
	float4 shadTex1;
	float4 shadTex2;
	invW = 1.0f/spot;
		
	shadTex1.xy = GetSpotTexCoordsOld( lightIndices.x ,worldPos );
	shadTex1.zw = GetSpotTexCoordsOld( lightIndices.y ,worldPos );
	shadTex2.xy = GetSpotTexCoordsOld( lightIndices.z ,worldPos );
	shadTex2.zw = GetSpotTexCoordsOld( lightIndices.w ,worldPos );
	
	shadTex1.xyzw *= invW.xxyy;
	shadTex2.xyzw *= invW.zzww;
	
	dist = dist * shadScale - shadBias;
	
	float4 shadFade  = saturate( spot * MaxShadowFade.x + MaxShadowFade.y);
	float4 shad =shadowVariance_CompareDepthBy4Precscaled( shadTex1, shadTex2, dist, ShadowAtlasSampler, 0.0001f );

	return shad * shadFade;
}
float3 GetTextureColors( float2 uv , float4 colors )
{
	float4 lColR;
    float4 lColG;
    float4 lColB;
    
    // need to make deal with negative colors
    asm
    {
		tfetch2D lColR, uv, ColorSampler, UseComputedLOD = false, OffsetX = 2.5
        tfetch2D lColG, uv, ColorSampler, UseComputedLOD = false, OffsetX = 3.5
        tfetch2D lColB, uv, ColorSampler, UseComputedLOD = false, OffsetX = 4.5
     };
     
	 float3 col= float3(	dot(lColR, colors ),
					dot( lColG, colors ),
					dot( lColB, colors ) );
	 
	return col; 
};

float4 Specular3( float t, float3 M, float4 ldirX, float4 ldirY, float4 ldirZ )
{
	float4 spec =  ldirX * M.x;
	spec =  ldirY * M.y + spec;
	spec =  ldirY * M.z + spec;
	return SlickApproxPhong( spec );	
}
//	
float3 TextureLight( float2 gridUV, float3 worldPos, float3 normal )
{
	float4 ldirX;
    float4 ldirY;
    float4 ldirZ;
    float4 atten;
	float4 dist = GetTextureStreamLightPositions( gridUV, worldPos, ldirX, ldirY, ldirZ, atten );
	
	float4 recipDist = rsqrt( dist );
	float4 light = ldirX * normal.x;
	light = ldirY * normal.y + light;
	light = ldirZ * normal.z + light;
	light *=  atten * recipDist  * recipDist * recipDist;
	
	float4 spot;
	light = saturate( light* GetTextureStreamLightSpot( gridUV , ldirX, ldirY, ldirZ, recipDist, spot )  );
	
	float4 shadow = GetTextureShadow(float4(worldPos, 1.0f), gridUV, dist * recipDist, spot );
	return GetTextureColors( gridUV , light * shadow) ;
}
#endif


#if !__XENON
float3 CalculateDiffuseFastLight4All( float3 worldPos, float3 normal )
{
	float3 res = 0;
	return res;
}
//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos = mul(float4(IN.pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal.xyz = normalize(mul(IN.normal.xyz, (float3x3)gWorld));
	OUT.Normal.w = IN.diffuse.x * gAmbientOcclusionFadeIn.x + gAmbientOcclusionFadeIn.y;
      
    float3 binorm = rageComputeBinormal( IN.normal, IN.tangent);
	OUT.Tangent = normalize(mul(  IN.tangent.xyz, (float3x3)gWorld));
	OUT.Binormal = normalize(mul(binorm, (float3x3)gWorld));
	
	OUT.Texcoord0 = IN.texCoord0;

    // output texture coordinate
     	
  //  PreCalculateDirections( FastLight4GetConstants(2), OUT.WorldPos.xyz, OUT.lightDX, OUT.lightDY,  OUT.lightDZ );
  //  PreCalculateDirections( FastLight4GetConstants(3), OUT.WorldPos.xyz, OUT.lightDX2, OUT.lightDY2,  OUT.lightDZ2 );
 #ifndef _SORT_
	SH2Color sh;
	sh.red = 0;
	sh.green = 0;
	sh.blue = 0;
	GET_SH2_COLOR( sh )
#endif
	OUT.Color = IN.diffuse;
    return OUT;
}
#else
float3 CalculateSpot( float3 pos, float3 normal, float4 lpos1, float4 col1, float4 ldir )
{
	float3 d1 = lpos1.xyz - pos.xyz;
	float dist = dot( d1, d1 );
	float recipicol = rsqrt(dist);		// 4 instructions should be paired on 360
	float3 dir =  d1 * recipicol;

	//float atten = saturate(  col1.w /sqrt(dist)  );	// 1 instruct
	float atten = saturate(  col1.w * recipicol );	// 1 instruct now quadratic
	
	float light =saturate( dot( dir.xyz, normal.xyz ) );
	light *= atten;
	float spot = dot( ldir.xyz, -dir.xyz );
	light *= saturate(  spot * ldir.w  + lpos1.w  );
	return col1.xyz * light.x;
}

float GetSpotIntensity( float3 normal, float3 dir ,  float recipDist,  float spotDot, float4 ldir , float4 lpos1)
{

	return saturate(  spotDot * ldir.w  + lpos1.w  );;
}
float3 CalculateDiffuse( float3 pos, float3 normal, float3 lpos1, float4 col1 )
{   
	float3 d1 = lpos1 - pos;
	float dist = dot( d1, d1 );
	float recipicol = rsqrt(dist);		// 4 instructions should be paired on 360
	float3 ldir =  d1 * recipicol;

	//float atten = saturate(  col1.w /sqrt(dist)  );	// 1 instruct
	float atten = saturate(  col1.w / dist  );	// 1 instruct now quadratic
	
	float light =saturate( dot( ldir.xyz, normal.xyz ) );
	light *= atten;
	
	return col1.xyz * light.x;
}
float3 CalculateSpecular( float3 reflectedEye, float3 pos, float3 normal, float3 lpos1, float4 col1 )
{   
	float3 d1 = lpos1 - pos;
	float dist = dot( d1, d1 );
	float recipicol = rsqrt(dist);		// 4 instructions should be paired on 360
	float3 ldir =  d1 * recipicol;

	//float atten = saturate(  col1.w /sqrt(dist)  );	// 1 instruct
	float atten = saturate(  col1.w / dist  );	// 1 instruct now quadratic
	float2 light;
	
	light.x =dot( ldir.xyz, normal.xyz );
	light.y = saturate( pow( dot( ldir.xyz, reflectedEye.xyz ),128.0 ) )* ( light.x > 0.0f );
	light.x= saturate( light.x * atten );
	light.y *=saturate( atten * 8.0f);
	
	return col1.xyz * light.x + col1.xyz * light.y ;
}

float3 GetReflectedView( float3 pos, float3 normal )
{
	float3 view = -gViewInverse[2].xyz;// 
	//float3 view = normalize( pos  - gViewInverse[3].xyz);
	
	return reflect( view, normal );
}

float4 Specular( float t, float3 M, float3 ldir1, float3 ldir2, float3 ldir3, float3 ldir4 )
{
	float4 spec;
	spec.x =  saturate( dot( M, ldir1 ) );
	spec.y =  saturate(dot( M, ldir2 ) );
	spec.z =  saturate(dot( M, ldir3 ) );
	spec.w =  saturate(dot( M, ldir4 ) );

return SlickApproxPhong( spec );	
//	return saturate( pow( spec, t ) );
}



float4 GetShadows( float4 worldPos, float3 normal , float4 lightIndices )
{
	float4 shadTex1;
	float4 shadTex2;
	float4 shadTexW;
	
	float4 attenuation;
	float4 bias;
	float4 eplision;	
	float4 distToLight;
	
	float4 dist2;
	float4 recipDist;
	float4 inten;
	float3 ldir1 = LightPositions[ lightIndices.x ].xyz - worldPos.xyz;
	float3 ldir2 = LightPositions[ lightIndices.y ].xyz - worldPos.xyz;
	float3 ldir3 = LightPositions[ lightIndices.z ].xyz - worldPos.xyz;
	float3 ldir4 = LightPositions[ lightIndices.w ].xyz - worldPos.xyz;
		
	float4 spotWDist1;
	float4 spotWDist2;
	spotWDist1.x = dot( LightDirections[ lightIndices.x], -ldir1 );
	spotWDist1.z = dot( LightDirections[ lightIndices.y], -ldir2 );
	spotWDist2.x = dot( LightDirections[ lightIndices.z], -ldir3 );
	spotWDist2.z = dot( LightDirections[ lightIndices.w], -ldir4 );
	float4 invW;
	invW.xy = 1.0f/spotWDist1.xz;
	invW.zw = 1.0f/spotWDist2.xz;
	
	shadTex1.xy = GetSpotTexCoordsOld( lightIndices.x ,worldPos);
	shadTex1.zw = GetSpotTexCoordsOld( lightIndices.y ,worldPos );
	shadTex2.xy = GetSpotTexCoordsOld( lightIndices.z ,worldPos );
	shadTex2.zw = GetSpotTexCoordsOld( lightIndices.w ,worldPos);
	
	shadTex1.xyzw *= invW.xxyy;
	shadTex2.xyzw *= invW.zzww;
	
	
	// 4 distance and normalize instruction
	spotWDist1.y = dot( ldir1, ldir1 );
	recipDist.x = rsqrt( spotWDist1.y );
	ldir1 *= recipDist.x;
	
	spotWDist1.w = dot( ldir2, ldir2 );
	recipDist.y = rsqrt( spotWDist1.w );
	ldir2 *= recipDist.y;
	
	spotWDist2.y  = dot( ldir3, ldir3 );
	recipDist.z = rsqrt( spotWDist2.y  );
	ldir3 *= recipDist.z;
	
	spotWDist2.w = dot( ldir4, ldir4 );
	recipDist.w = rsqrt( spotWDist2.w );
	ldir4 *= recipDist.w;
	
	
	float4 atten;
	
	spotWDist1 *= recipDist.xxyy;
	spotWDist2 *= recipDist.zzww;
	
	float4 shadFade;
	
	shadFade.xy = saturate( spotWDist1.xz * MaxShadowFade.x + MaxShadowFade.y);
	shadFade.zw = saturate( spotWDist2.xz * MaxShadowFade.x + MaxShadowFade.y);
	
	float4 intenDist1;
	float4 intenDist2;
	
	atten.x =LightColors[ lightIndices.x].w; 
	atten.y =LightColors[ lightIndices.y].w;
	atten.z =LightColors[ lightIndices.z].w;	
	atten.w =LightColors[ lightIndices.w].w;
	
	
	spotWDist1.xy = spotWDist1.xy * ShadowSettings[ lightIndices.x].zx + ShadowSettings[ lightIndices.x ].wy;
	spotWDist1.zw = spotWDist1.zw * ShadowSettings[ lightIndices.y].zx + ShadowSettings[ lightIndices.y ].wy;
	spotWDist2.xy = spotWDist2.xy * ShadowSettings[ lightIndices.z].zx + ShadowSettings[ lightIndices.z ].wy;
	spotWDist2.zw = spotWDist2.zw * ShadowSettings[ lightIndices.w].zx + ShadowSettings[ lightIndices.w ].wy;
	
	atten = saturate( atten * recipDist * recipDist );
	
	distToLight.xy = spotWDist1.yw;
	distToLight.zw = spotWDist2.yw;
	
	inten.xy = saturate( spotWDist1.xz );
	inten.zw = saturate( spotWDist2.xz );
	


	float4 shad =shadowVariance_CompareDepthBy4Precscaled( shadTex1, shadTex2, distToLight, ShadowAtlasSampler, 0.0001f );
	
	// fade shadows out at extreme angles to allow for simplified point light shadows
	shad =  saturate( shad * shad +  shadFade);
	
	float3 M = GetReflectedView( worldPos.xyz, normal );
	float4 spec = Specular( 16.0f, M, ldir1, ldir2, ldir3, ldir4 );
	float4 diffuse = float4( saturate( dot( ldir1, normal ) ),
							 saturate( dot( ldir2, normal ) ),
							 saturate( dot( ldir3, normal ) ),
							 saturate( dot( ldir4, normal ) ) );
	
	
	return  ( diffuse + spec ) * atten * shad * inten;
	return  inten * atten * shad; //  *
}
float2 CalculateSpot2( float3 pos, float3 normal, float4 lpos1, float4 lpos2,  float4 ldir, float4 ldir2 , float2 atten, float4 penumbra)
{
	float3 d1 =  pos.xyz - lpos1.xyz;
	float3 d2 =  pos.xyz - lpos2.xyz;
	float2 dist;
	dist.x = dot( d1, d1 );
	dist.y = dot( d2, d2 );
	float2 recipicol = rsqrt( dist );		// 4 instructions should be paired on 360
	
	atten = saturate( atten * recipicol * recipicol  );	// 1 instruct now quadratic
	d1 *= recipicol.x;
	d2 *= recipicol.y;
	
	float2 light;
	light.x =saturate( dot( -d1.xyz, normal.xyz ) );
	light.y  =saturate( dot( -d2.xyz, normal.xyz ) );
	light *= atten;
	
	float2 spot;
	spot.x = dot( ldir.xyz, d1.xyz );
	spot.y = dot( ldir2.xyz, d2.xyz );
	
	light *= saturate(  spot * penumbra.xy  + penumbra.zw  );
	return light;
}

float4 GetLights( float3 worldPos, float3 normal,  float4 lightIndices, float4 Indices, float occ )
{
//	normal = float3( 0.0f, 1.0f, 0.0f );
	float4 shadows = GetShadows( float4( worldPos, 1.0f), normal, lightIndices );
	
	float3 col =  shadows.x * LightColors[ lightIndices.x].xyz + 
			shadows.y * LightColors[ lightIndices.y].xyz + 
			shadows.z * LightColors[ lightIndices.z].xyz + 
			shadows.w * LightColors[ lightIndices.w].xyz;
	
	
	col.xyz += DirectionalLighting( normal, occ );
	
	return float4( col.xyz, 1.0f);	
	
	/*float2 val = CalculateSpot2( worldPos, normal, 
				LightPositions[ Indices.x ], LightPositions[ Indices.y ],  
				LightDirections[ Indices.x ], LightDirections[ Indices.y ], 
				float2( LightColors[ Indices.x ].w, LightColors[ Indices.y ].w),
				float4( LightDirections[ Indices.x ].w, LightDirections[ Indices.y ].w,
						LightPositions[ Indices.x ].w, LightPositions[ Indices.y ].w ));
				
	col.xyz += LightColors[ Indices.x ].xyz * val.x + LightColors[ Indices.y ].xyz * val.y ; 
	return float4( col.xyz, 1.0f);	*/
//	return shadows;
	/*float3 col = CalculateSpot(  worldPos, normal, 	LightPositions[ lightIndices.x ], LightColors[ lightIndices.x], LightDirections[ lightIndices.x] ) * shadows.x
			+  CalculateSpot( worldPos, normal, LightPositions[ lightIndices.y ], LightColors[ lightIndices.y], LightDirections[ lightIndices.y] ) * shadows.y
			+	CalculateSpot(  worldPos, normal, LightPositions[ lightIndices.z ], LightColors[ lightIndices.z] , LightDirections[ lightIndices.z])* shadows.z
			+ 	CalculateSpot(  worldPos, normal, LightPositions[ lightIndices.w ], LightColors[ lightIndices.w], LightDirections[ lightIndices.w] )* shadows.w;
			
	return float4( col, 1.0f);*/
}
	

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos = mul(float4(IN.pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal.xyz = normalize(mul(IN.normal.xyz, (float3x3)gWorld));
	OUT.Normal.w = 	IN.diffuse.x * gAmbientOcclusionFadeIn.x + gAmbientOcclusionFadeIn.y;
	OUT.Texcoord0 = IN.texCoord0;
	
     
    
	float3 worldPos = OUT.WorldPos;
	float3 normal = OUT.Normal;
	    float3 binorm = rageComputeBinormal( IN.normal, IN.tangent);
	OUT.Tangent = normalize(mul( IN.tangent.xyz, gWorld));
	OUT.Binormal = normalize(mul(binorm, gWorld));

	// PreCalculateDirections( FastLight4GetConstants(2), OUT.WorldPos, OUT.lightDX, OUT.lightDY,  OUT.lightDZ );
#ifndef _SORT_
	SH2Color	sh;
	sh.red = 0;
	sh.green = 0;
	sh.blue = 0;
	
	sh = CalculateSH2DiffuseFastLight4( OUT.WorldPos, FastLight4GetConstants(0), sh );
	GET_SH2_COLOR(sh)
#endif

OUT.Color = IN.diffuse;//col;
//OUT.Color.xyz =  GetVertexStreamLight( 0);

    return OUT;
}

#endif
#define TEX_SORT


float3 GetNormal( VertexOutput IN, out float normOcc )
{
	float3 norm = tex2D( BumpSampler, IN.Texcoord0 ).xyz * 2.0 - 1.0f;
	
	normOcc = norm.z;
	return normalize( IN.Normal.xyz * norm.z  + IN.Tangent.xyz * norm.x + IN.Binormal.xyz * norm.y );
}


float4 PSFast4( VertexOutput IN ) 
{
	float normOcc;
	float3 normal = GetNormal( IN, normOcc );// normalize( IN.Normal.xyz );//

#ifdef TEX_SORT
	float2 SortTexCoord = IN.WorldPos.xz* LightSortScale.xz  + LightSortBias.xz ;
	
	float4 Indices =  tex2D( LightSortSampler, SortTexCoord ) * 255.0f + 0.5f;
//	float4 Indices2 =  tex2D( LightSortSampler2, SortTexCoord ) * 255.0f + 0.5f;

#if __XENON
	return float4( GetLights( IN.WorldPos.xyz,  normal ,  Indices, Indices, IN.Color.x * normOcc ) * IN.Color.w ) * 0.82f;
#endif
	return float4( 1.0f, 0.0f, 1.0f, 0.0f );
#endif

}
//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS( VertexOutput IN ) : COLOR
{
	return PSFast4( IN );
}

technique drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false; 
        ZwriteEnable = true;    
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ZwriteEnable = true;   
        ZEnable = true;
        
		CullMode = CW;
        VertexShader = compile VERTEXSHADER VS();
#if __XENON
		 PixelShader  = compile PIXELSHADER PS();
#else
        PixelShader  = compile PIXELSHADER PS();
#endif
    }
}

technique unlit_drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false; 
        ZwriteEnable = true;    
        ZEnable = true;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ZwriteEnable = true;   
        ZEnable = true;
        
		CullMode = CW;
        VertexShader = compile VERTEXSHADER VS();
#if __XENON
		 PixelShader  = compile PIXELSHADER PS();
#else
        PixelShader  = compile PIXELSHADER PS();
#endif
    }
}
