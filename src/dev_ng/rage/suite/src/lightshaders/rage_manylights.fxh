//-----------------------------------------------------
// PURPOSE: Manylights is a forward rendering system for dealing with lots of lights.
//

 #define DISABLE_RAGE_LIGHTING
#define NO_SKINNING
#include "../../../../rage/base/src/shaderlib/rage_common.fxh"


#define USE_COMPRESSED 1
#define MAX_SORT_LIGHTS 29

#ifndef MAX_FILLSORT_LIGHTS
#define MAX_FILLSORT_LIGHTS	55
#endif

//---------------------------------------------------------------
// SHARED CONSTANTS for Manylights
//------------------------------------------------------------------

CBSHARED BeginConstantBufferPagedDX10(LightAttributes,b5)
#ifdef USE_MAX_ATTENUATION
shared float4	gLightAttenuations[ MAX_SORT_LIGHTS ] : LightAttenuations REGISTER2(ps,c30 );
#endif

shared float4	gLightPositions[ MAX_SORT_LIGHTS ] : LightPositions  REGISTER2(ps,c155 );
shared float4  gLightColors[ MAX_SORT_LIGHTS] : LightColors  REGISTER2(ps, c184 );

#if !USE_COMPRESSED
shared float4  gLightDirections[ MAX_SORT_LIGHTS ]: LightDirections;
#endif
EndConstantBufferDX10(LightAttributes)

CBSHARED BeginConstantBufferPagedDX10(LightShadow,b6)
shared float3	LightSortScale : LightSortScale REGISTER2(ps,c65);
shared float3	LightSortBias : LightSortBias  REGISTER2(ps,c66);
shared float2	MaxShadowFade : MaxShadowFade REGISTER2(ps,c67);

shared float4		manylights_SkyColor : manylights_SkyColor REGISTER2(ps,c62) = { 0.02f, 0.02f, 0.2f,1.0f};
shared float4		manylights_GroundColor : manylights_GroundColor  REGISTER2(ps,c61)  = { 0.05f, 0.05f, 0.0f, 1.0f};
EndConstantBufferDX10(LightShadow)

float3 LightPositions( int i )
{
	return gLightPositions[i].xyz;
}
#if USE_COMPRESSED
float3 LightDirections( int i )
{
	return frac( gLightColors[i].xyz ) * 2.0f - 1.0f;
}
float3 LightColors( int i )
{
	return ( gLightColors[i].xyz - frac( gLightColors[i].xyz ) ) ;
}

#else
float3 LightDirections( int i )
{
	return gLightDirections[i].xyz ;
}
float3 LightColors( int i )
{
	return gLightColors[i].xyz;
}

#endif
float Atten( int i )
{
	return gLightColors[i].w;
}

BeginSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
ContinueSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSharedSampler;

BeginSharedSampler( sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9)
ContinueSharedSampler(sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9 )
	AddressU = CLAMP;
	AddressV = CLAMP;
	AddressW = CLAMP;	
	MIPFILTER = LINEAR;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
EndSharedSampler;

//----------------- 
//   Shadows Atlas
//----------------------

#ifdef USE_SHADOW_ATLAS

#include "../../../../rage/base/src/shaderlib/rage_shadow_variance.fxh"

CBSHARED BeginConstantBufferPagedDX10(ShadowFade,b8)
shared float2	MaxShadowFade : MaxShadowFade REGISTER2(ps,c67);

shared float4  ShadowMatrixX[ MAX_SORT_LIGHTS] : manylights_ShadowMatrixX REGISTER2(ps,c68);
shared float4  ShadowMatrixY[ MAX_SORT_LIGHTS] : manylights_ShadowMatrixY REGISTER2(ps,c97);
shared float4  ShadowSettings[ MAX_SORT_LIGHTS] : manylights_ShadowSettings REGISTER2(ps,c126);
EndConstantBufferDX10(ShadowFade)

BeginSharedSampler( sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9)
ContinueSharedSampler(sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9 )
	AddressU = CLAMP;
	AddressV = CLAMP;
	AddressW = CLAMP;	
	MIPFILTER = LINEAR;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
EndSharedSampler;


float2 GetSpotTexCoords( int i , float4 pos )
{
	float2 res;
	res.x = dot( ShadowMatrixX[i], pos );
	res.y = dot( ShadowMatrixY[i], pos );
	return res;
}

float4 GetShadows( float4 Indices, float4 worldPos, float4 distToLight )
{
	shadTex1.xy = GetSpotTexCoords( lightIndices.x ,worldPos);
	shadTex1.zw = GetSpotTexCoords( lightIndices.y ,worldPos);
	shadTex2.xy = GetSpotTexCoords( lightIndices.z ,worldPos);
	shadTex2.zw = GetSpotTexCoords( lightIndices.w ,worldPos);
	
	distToLight *=
	
	float4 shad =shadowVariance_CompareDepthBy4Precscaled( shadTex1, shadTex2, distToLight, ShadowAtlasSampler, 0.0001f );
}

#endif

//----------------------------------------------------------------------


#include "../../../../rage/base/src/shaderlib/rage_utility.fxh"
#include "../../../../rage/base/src/shaderlib/rage_halflife2_lighting.fxh"
#include "../../../../rage/base/src/shaderlib/rage_specular.fxh"

#ifdef USE_FILL_LIGHTS
float4	FillLightPositions[ MAX_FILLSORT_LIGHTS ] : FillLightPositions;
float4  FillLightColors[ MAX_FILLSORT_LIGHTS ] : FillLightColors;
float4  FillLightDirections[ MAX_FILLSORT_LIGHTS ]: FillLightDirections;

BeginSampler(sampler2D,FillLightSortTexture, FillLightSortSampler,FillLightSortTexture)
ContinueSampler(sampler2D,FillLightSortTexture, FillLightSortSampler,FillLightSortTexture)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSampler;


#endif

// Decompression
float3		PositionBias: PositionBias;
float3		PositionScale: PositionScale;


	


BeginSampler(sampler2D,LightSortTexture2, LightSortSampler2,LightSortTexture2)
ContinueSampler(sampler2D,LightSortTexture2, LightSortSampler2,LightSortTexture2)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = LINEAR;
		MINFILTER = LINEAR;
		MAGFILTER = LINEAR; 
		MipMapLodBias = 0;
EndSampler;


float3 DirectionalLighting( float3 normal , float occ )
{
	return lerp( manylights_GroundColor.xyz, manylights_SkyColor.xyz, normal.y * 0.5 + 0.5) * occ;
}


float4 MaxDiff( float4 t1, float4 t2 )
{
	float4 res;
	float4 res1;
	res1.xy = max( abs(ddx( t1.xy)), abs(ddy(t1.xy)) );
	res1.zw = max( abs(ddx( t1.zw)), abs(ddy(t1.zw)) );
	float4 res2;
	res2.xy = max( abs(ddx( t2.xy)), abs(ddy(t2.xy)));
	res2.zw = max( abs(ddx( t2.zw)), abs(ddy(t2.zw)) );
	res.xy = max( res1.xy, res1.zw );
	res.zw = max( res2.xy, res2.zw );
	return res;
}
float4 GetShadows( float3 V, float4 worldPos, float3 normal , float4 lightIndices, out float4 diffuse, out float4 spec, float specExp  )
{
	float4 shadTex1;
	float4 shadTex2;
	float4 shadTexW;
	
	float4 attenuation;
	float4 bias;
	float4 eplision;	
	float4 distToLight;
	
	float4 dist2;
	float4 recipDist;
	float4 inten;
	float3 ldir1 = LightPositions( lightIndices.x ).xyz - worldPos.xyz;
	float3 ldir2 = LightPositions( lightIndices.y ).xyz - worldPos.xyz;
	float3 ldir3 = LightPositions( lightIndices.z ).xyz - worldPos.xyz;
	float3 ldir4 = LightPositions( lightIndices.w ).xyz - worldPos.xyz;
		
	float4 spotWDist1;
	float4 spotWDist2;
	spotWDist1.x = -dot( LightDirections( lightIndices.x).xyz, ldir1 );
	spotWDist1.z = -dot( LightDirections( lightIndices.y).xyz, ldir2 );
	spotWDist2.x = -dot( LightDirections( lightIndices.z).xyz, ldir3 );
	spotWDist2.z = -dot( LightDirections( lightIndices.w).xyz, ldir4 );
	float4 invW;
	invW.xy = 1.0f/spotWDist1.xz;
	invW.zw = 1.0f/spotWDist2.xz;
	
	shadTex1.xy = GetSpotTexCoords( lightIndices.x ,worldPos);
	shadTex1.zw = GetSpotTexCoords( lightIndices.y ,worldPos );
	shadTex2.xy = GetSpotTexCoords( lightIndices.z ,worldPos );
	shadTex2.zw = GetSpotTexCoords( lightIndices.w ,worldPos);
	
	shadTex1.xyzw *= invW.xxyy;
	shadTex2.xyzw *= invW.zzww;
	
	
	// 4 distance and normalize instruction
	spotWDist1.y = dot( ldir1, ldir1 );
	recipDist.x = rsqrt( spotWDist1.y );
	ldir1 *= recipDist.x;
	
	spotWDist1.w = dot( ldir2, ldir2 );
	recipDist.y = rsqrt( spotWDist1.w );
	ldir2 *= recipDist.y;
	
	spotWDist2.y  = dot( ldir3, ldir3 );
	recipDist.z = rsqrt( spotWDist2.y  );
	ldir3 *= recipDist.z;
	
	spotWDist2.w = dot( ldir4, ldir4 );
	recipDist.w = rsqrt( spotWDist2.w );
	ldir4 *= recipDist.w;
	
	float4 atten;
	
	spotWDist1 *= recipDist.xxyy;
	spotWDist2 *= recipDist.zzww;
	
	float4 shadFade;
	
	shadFade.xy = saturate( spotWDist1.xz * MaxShadowFade.x + MaxShadowFade.y);
	shadFade.zw = saturate( spotWDist2.xz * MaxShadowFade.x + MaxShadowFade.y);
	
	float4 intenDist1;
	float4 intenDist2;
	
#ifdef USE_MAX_ATTENUATION
	float4 atten2;
	atten2.xy = saturate( gLightAttenuations[ lightIndices.x ].xz * spotWDist1.yy + gLightAttenuations[ lightIndices.x ].yw );
	atten2.zw = saturate( gLightAttenuations[ lightIndices.y ].xz * spotWDist1.ww + gLightAttenuations[ lightIndices.y ].yw );
	atten.xy =  saturate(atten2.xz - atten2.yw );
	
	atten2.xy = saturate( gLightAttenuations[ lightIndices.z ].xz * spotWDist2.yy + gLightAttenuations[ lightIndices.z ].yw );
	atten2.zw = saturate( gLightAttenuations[ lightIndices.w ].xz * spotWDist2.ww + gLightAttenuations[ lightIndices.w ].yw );
	atten.zw = saturate(atten2.xz - atten2.yw );
	
#else
	atten.x =Atten(  lightIndices.x);//LightColors( lightIndices.x).w; 
	atten.y =Atten(  lightIndices.y);//LightColors( lightIndices.y).w;
	atten.z =Atten(  lightIndices.z);//LightColors( lightIndices.z).w;	
	atten.w =Atten(  lightIndices.w); //LightColors( lightIndices.w).w;
	atten = saturate( atten * recipDist * recipDist );
#endif
	
	spotWDist1.xy = spotWDist1.xy * ShadowSettings[ lightIndices.x].zx + ShadowSettings[ lightIndices.x ].wy;
	spotWDist1.zw = spotWDist1.zw * ShadowSettings[ lightIndices.y].zx + ShadowSettings[ lightIndices.y ].wy;
	spotWDist2.xy = spotWDist2.xy * ShadowSettings[ lightIndices.z].zx + ShadowSettings[ lightIndices.z ].wy;
	spotWDist2.zw = spotWDist2.zw * ShadowSettings[ lightIndices.w].zx + ShadowSettings[ lightIndices.w ].wy;
	

	distToLight.xy = spotWDist1.yw;
	distToLight.zw = spotWDist2.yw;
	
	inten.xy = saturate( spotWDist1.xz );
	inten.zw = saturate( spotWDist2.xz );
	float4 shad =shadowVariance_CompareDepthBy4Precscaled( shadTex1, shadTex2, distToLight, ShadowAtlasSampler, 0.0001f );
		
	// fade shadows out at extreme angles to allow for simplified point light shadows
	shad =  saturate( shad * shad +  shadFade);
	
	spec =  specSpecularBlinn( specExp, normal, -V, ldir1, ldir2, ldir3, ldir4 );
	
	
	diffuse = float4( saturate( dot( ldir1, normal ) ),
							 saturate( dot( ldir2, normal ) ),
							 saturate( dot( ldir3, normal ) ),
							 saturate( dot( ldir4, normal ) ) );
							 
	
	return  atten * shad * inten;
}

struct Irradiance
{
	float3 diffuse;
	float3 specular;
	float3 ambient;
};

// PURPOSE : get the grid position for the manylights in a xz plane world ( Y UP )
//
float2 manylights_GetGridPositonXZ( float3 WorldPos )
{
	return WorldPos.xz* LightSortScale.xz + LightSortBias.xz;	
}

// PURPOSE : get the grid position for the manylights in a xy plane world ( Z UP )
//
float2 manylights_GetGridPositionXY( float3 WorldPos)
{ 
	return WorldPos.xy* LightSortScale.xy + LightSortBias.xy;	
}


float3 manylights_GetCellColor(  float2 gridPosition)
{
	float4 Indices =  tex2D( LightSortSampler, gridPosition) * 255.0f + 0.5f;
	return LightColors( Indices.x).xyz;
}

Irradiance manylights_GetLocalLightsIndices( float3 worldPos, float3 normal,  float4 lightIndices, float  specExp )
{
	float4 diffuse;
	float4 spec;
	float4 bounce;
#if !__XENON
	lightIndices = float4( 0.0f, 1.0f, 2.0f, 3.0f );	// always do first four lights if is pc
#endif
		
	float3 V = normalize( worldPos.xyz  - gViewInverse[3].xyz);
	float4 shadows = GetShadows( V , float4( worldPos, 1.0f), normal, lightIndices, diffuse, spec, specExp );
	shadows *= 2.0f/255.0f;
	diffuse *= shadows;
	spec *= shadows;
	
	
	float3 c1 =  LightColors( lightIndices.x).xyz;
	float3 c2 = LightColors( lightIndices.y).xyz;
	float3 c3 = LightColors( lightIndices.z).xyz;
	float3 c4 = LightColors( lightIndices.w).xyz;
	
	Irradiance rad;
	rad.ambient = 0;
	
	rad.diffuse =  diffuse.x * c1 + 
			diffuse.y * c2 + 
			diffuse.z * c3 + 
			diffuse.w * c4;
			
	rad.specular =   spec.x * c1 + 
			spec.y * c2 + 
			spec.z * c3 + 
			spec.w * c4;
	
	return rad;
}
	
// PURPOSE : Calculates local lighting using the manylights system
//
// RETURNS : Irradanice which has the diffuse and specular compoents separately to allow for
//			 easier per shader control.
//
Irradiance manylights_GetLocalLights( float3 worldPos, float3 normal,  float2 GridPosition, float specExp )
{	
	float4 Indices =  tex2D( LightSortSampler, GridPosition) * 255.0f + 0.5f;
	return manylights_GetLocalLightsIndices( worldPos, normal,  Indices, specExp );
}


#if USE_FILL_LIGHTS
void precompute_CalculateHL2BounceLighting( float3 worldPos, float3 n, float3 t, float3 b, float4 lightIndices, out float3 c1, out float3 c2, out float3 c3 )
{
	// transform HL2 vectors into world space
	float3 v1 = HalfLifeRed;
	float3 v2 = HalfLifeGreen;
	float3 v3 = HalfLifeBlue;
	
	
	v1 = v1.x * t + v1.y * b + v1.z * n;
	v2 = v2.x * t + v2.y * b + v2.z * n;
	v3 = v3.x * t + v3.y * b + v3.z * n;
	
	
	float3 lit1 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.x ], FillLightDirections[ lightIndices.x], FillLightColors[ lightIndices.x] );
	float3 lit2 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.y ], FillLightDirections[ lightIndices.y], FillLightColors[ lightIndices.y] );
	float3 lit3 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.z ], FillLightDirections[ lightIndices.z], FillLightColors[ lightIndices.z] );
	float3 lit4 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.w ], FillLightDirections[ lightIndices.w], FillLightColors[ lightIndices.w] );
	
	c1 = lit1.x * FillLightColors[ lightIndices.x].xyz + lit2.x * FillLightColors[ lightIndices.y].xyz + lit3.x * FillLightColors[ lightIndices.z].xyz + lit4.x * FillLightColors[ lightIndices.w].xyz;
	c2 = lit1.y * FillLightColors[ lightIndices.x].xyz + lit2.y * FillLightColors[ lightIndices.y].xyz + lit3.y * FillLightColors[ lightIndices.z].xyz + lit4.y * FillLightColors[ lightIndices.w].xyz;
	c3 = lit1.z * FillLightColors[ lightIndices.x].xyz + lit2.z * FillLightColors[ lightIndices.y].xyz + lit3.z * FillLightColors[ lightIndices.z].xyz + lit4.z * FillLightColors[ lightIndices.w].xyz;	
	
	c1 = c1 * BounceStrength + DirectionalLighting( v1 , EnvironmentStrength );
	c2 = c2 * BounceStrength + DirectionalLighting( v2 , EnvironmentStrength);
	c3 = c3 * BounceStrength + DirectionalLighting( v3 , EnvironmentStrength);
}
void precompute_CalculateUpBounceLighting( float3 worldPos, float3 n, float3 t, float3 b, float4 lightIndices, out float3 c1 )
{
	// transform HL2 vectors into world space
	float3 v1 = n;//normalize( n + float3( 0.0f, -0.5f, 0.0f ) );
	float3 v2 = HalfLifeGreen;
	float3 v3 = HalfLifeBlue;

	
	float3 lit1 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.x ], FillLightDirections[ lightIndices.x], FillLightColors[ lightIndices.x] );
	float3 lit2 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.y ], FillLightDirections[ lightIndices.y], FillLightColors[ lightIndices.y] );
	float3 lit3 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.z ], FillLightDirections[ lightIndices.z], FillLightColors[ lightIndices.z] );
	float3 lit4 = CalculateSpotHL2(  n, worldPos, v1, v2, v3, FillLightPositions[ lightIndices.w ], FillLightDirections[ lightIndices.w], FillLightColors[ lightIndices.w] );
	
	c1 = lit1.x * FillLightColors[ lightIndices.x].xyz + lit2.x * FillLightColors[ lightIndices.y].xyz + lit3.x * FillLightColors[ lightIndices.z].xyz + lit4.x * FillLightColors[ lightIndices.w].xyz;
	
	c1 = c1 * BounceStrength + DirectionalLighting( v1 , EnvironmentStrength );
	
}
#endif
/*
float gi_ApplyFormFactor( float offset, float3 v, float3  normal , float fallOff )
{
	return saturate( fallOff / offset ) * dot(  v, normal ) * ( 1.0 -normal.y )/2.0f;
}	

float3 gi_CalculateRealTimeGI(  VertexOutput IN, float3 normal, sampler2D radianceSampler   )
{	
	// Calculate Approximate region of most bounce 
	float3 bounceNormal =normalize( IN.Normal.xyz + float3( 0.0f, -0.75f, 0.0f ) );
	bounceNormal.y = min( bounceNormal.y, -0.01f);
	
	// Collide with ground plane
	float offset = IN.WorldPos.y / -bounceNormal.y;
	float3 groundHitPos = IN.WorldPos.xyz + offset * bounceNormal;
	
	// Get intensity at that point
	float2 SortTexCoord2 = groundHitPos.xz* LightSortScale.xz + LightSortBias.xz;	
	float3 bounceGround =  tex2D( radianceSampler, SortTexCoord2 ).xyz;

	// Apply form factor 	
	bounceGround *= IN.occ * 3.0f * ApplyFormFactor( offset, bounceNormal, normal , 7.0f );
	return bounceGround;
}
*/

