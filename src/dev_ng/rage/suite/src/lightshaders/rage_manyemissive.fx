//-----------------------------------------------------
// Rage Diffuse
//

// Get the pong globals
 #define _SORT_
#define NO_SKINNING
#include "../../../base/src/shaderlib/rage_common.fxh"
//---------------------------------------------------------------
// SHARED CONSTANTS for Manylights
//------------------------------------------------------------------

#define USE_COMPRESSED 1
#define MAX_SORT_LIGHTS 29
#define MAX_FILLSORT_LIGHTS	55


shared float4	gLightPositions[ MAX_SORT_LIGHTS ] : LightPositions  REGISTER2(ps,c155 );
shared float4  gLightColors[ MAX_SORT_LIGHTS] : LightColors  REGISTER2(ps, c184 );

#if !USE_COMPRESSED
shared float4  gLightDirections[ MAX_SORT_LIGHTS ]: LightDirections;
#endif

shared float3	LightSortScale : LightSortScale REGISTER2(ps,c65);
shared float3	LightSortBias : LightSortBias  REGISTER2(ps,c66);

float3 LightPositions( int i )
{
	return gLightPositions[i].xyz;
}
#if USE_COMPRESSED
float3 LightDirections( int i )
{
	return frac( gLightColors[i].xyz ) * 2.0f - 1.0f;
}
float3 LightColors( int i )
{
	return ( gLightColors[i].xyz - frac( gLightColors[i].xyz ) ) ;
}

#else
float3 LightDirections( int i )
{
	return gLightDirections[i].xyz ;
}
float3 LightColors( int i )
{
	return gLightColors[i].xyz;
}

#endif
float Atten( int i )
{
	return gLightColors[i].w;
}

BeginSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
ContinueSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSharedSampler;


BeginSharedSampler( sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9)
ContinueSharedSampler(sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9 )
	AddressU = CLAMP;
	AddressV = CLAMP;
	AddressW = CLAMP;	
	MIPFILTER = LINEAR;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
EndSharedSampler;


//----------------------------------

struct VertexOutput 
{
    float4 Pos        : POSITION;
    float4 Color		: TEXCOORD1;
    float3 worldPos		: TEXCOORD2;
};


float		Intensity
<
	string UIName = "Intensity";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 5.0;
> = 1.0f;




#if !__XENON
float3 CalculateDiffuseFastLight4All( float3 worldPos, float3 normal )
{
	float3 res = 0;
	return res;
}
//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
  
    OUT.Color = IN.diffuse;
    return OUT;
}

float3 GetClosestColor( float3 worldPos, float4 lightIndices )	{ return 1.0f; }

#else
float3 GetClosestColor( float3 worldPos, float4 lightIndices )
{
	float d1 = distance( worldPos, LightPositions( lightIndices.x ).xyz );
	float d2 = distance( worldPos, LightPositions( lightIndices.y ).xyz );
	float d3 = distance( worldPos, LightPositions( lightIndices.z ).xyz );
   	float d4 = distance( worldPos, LightPositions( lightIndices.w ).xyz );

	float res = d1 < d2 ? lightIndices.x : lightIndices.y;
	float d = min( d1, d2 );
	res = d3 < d ?  lightIndices.z : res;
	d = min( d3, d );
	res = d4 < d ?  lightIndices.w : res;

	return LightColors( res).xyz;
}


	
//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.worldPos = mul(float4(IN.pos,1), gWorld);
    
    return OUT;
}

#endif



//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS( VertexOutput IN ) : COLOR
{
	float2 SortTexCoord = IN.worldPos.xz* LightSortScale.xz + LightSortBias.xz;
    float4 Indices =  tex2D( LightSortSampler, SortTexCoord) * 255.0f + 0.5f;
	float4 Color = float4( GetClosestColor( IN.worldPos , Indices  ) * 1.5f * Intensity , 1.0f);
	return Color;
}

technique drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;    
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;    
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}
