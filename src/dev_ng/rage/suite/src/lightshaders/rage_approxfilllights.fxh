//-----------------------------------------------------
// PURPOSE: Approx fillights has approximation to fill lights for rendering night time
//	or indoor environments.
//

/*
float gi_ApplyFormFactor( float offset, float3 v, float3  normal , float fallOff )
{
	return saturate( fallOff / offset ) * dot(  v, normal ) * ( 1.0 -normal.y )/2.0f;
}	

float3 gi_CalculateRealTimeGI(  VertexOutput IN, float3 normal, sampler2D radianceSampler   )
{	
	// Calculate Approximate region of most bounce 
	float3 bounceNormal =normalize( IN.Normal.xyz + float3( 0.0f, -0.75f, 0.0f ) );
	bounceNormal.y = min( bounceNormal.y, -0.01f);
	
	// Collide with ground plane
	float offset = IN.WorldPos.y / -bounceNormal.y;
	float3 groundHitPos = IN.WorldPos.xyz + offset * bounceNormal;
	
	// Get intensity at that point
	float2 SortTexCoord2 = groundHitPos.xz* LightSortScale.xz + LightSortBias.xz;	
	float3 bounceGround =  tex2D( radianceSampler, SortTexCoord2 ).xyz;

	// Apply form factor 	
	bounceGround *= IN.occ * 3.0f * ApplyFormFactor( offset, bounceNormal, normal , 7.0f );
	return bounceGround;
}
*/

// PURPOSE: treat the colour as coming from a plane at a certain height
//
float3 approxfill_bouncePlaneXY( float3 normal,  float3 color, float3 position, float h )
{
	float3 dir =  position.z > h ?  float3( 0.0f, 0.0f, -1  ) : float3( 0.0f, 0.0f, 1  ) ;
	float bf = saturate( (dot( normal, dir ) + 1.2f) * 0.5f );
	return   bf * color;
}
	

