//-----------------------------------------------------
// Rage Diffuse
//

// Get the pong globals
//
#define NO_SKINNING

#include "../../../base/src/shaderlib/rage_fastLight.fxh"
#include "../../../base/src/shaderlib/rage_common.fxh"
#include "../../../base/src/shaderlib/rage_shadow_variance.fxh"

#ifndef NO_SKINNING
#include "../../../base/src/shaderlib/rage_skin.fxh"
#endif

#define USE_VARIANCE_SHADOWS		1



BeginSampler(sampler, Shadow1Texture, Shadow1Sampler, Shadow1Texture)
ContinueSampler(sampler, Shadow1Texture, Shadow1Sampler, Shadow1Texture)
#if !__PS3
     AddressU  = BORDER;
     AddressV  = BORDER;
     AddressW  = BORDER;
     BorderColor = 0xffffffff;
#endif    
#if USE_VARIANCE_SHADOWS
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
#else
    MIPFILTER = POINT;
    MINFILTER = POINT;
    MAGFILTER = POINT;
#endif
EndSharedSampler;

BeginSampler(sampler, Shadow2Texture, Shadow2Sampler, Shadow2Texture)
ContinueSampler(sampler, Shadow2Texture, Shadow2Sampler, Shadow2Texture)
#if !__PS3
     AddressU  = BORDER;
     AddressV  = BORDER;
     AddressW  = BORDER;
     BorderColor = 0xffffffff;
#endif    
 #if USE_VARIANCE_SHADOWS
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
#else
    MIPFILTER = POINT;
    MINFILTER = POINT;
    MAGFILTER = POINT;
#endif
EndSharedSampler;

BeginSampler(sampler, Shadow3Texture, Shadow3Sampler, Shadow3Texture)
ContinueSampler(sampler, Shadow3Texture, Shadow3Sampler, Shadow3Texture)
#if !__PS3
     AddressU  = BORDER;
     AddressV  = BORDER;		
     AddressW  = BORDER;
     BorderColor = 0xffffffff;
#endif    
    #if USE_VARIANCE_SHADOWS
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
#else
    MIPFILTER = POINT;
    MINFILTER = POINT;
    MAGFILTER = POINT;
#endif
EndSharedSampler;

BeginSampler(sampler, Shadow4Texture, Shadow4Sampler, Shadow4Texture)
ContinueSampler(sampler, Shadow4Texture, Shadow4Sampler, Shadow4Texture)
#if !__PS3
     AddressU  = BORDER;
     AddressV  = BORDER;
     AddressW  = BORDER;
     BorderColor = 0xffffffff;
#endif    
  #if USE_VARIANCE_SHADOWS
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
#else
    MIPFILTER = POINT;
    MINFILTER = POINT;
    MAGFILTER = POINT;
#endif
EndSharedSampler;

BeginSampler(sampler, Shadow5Texture, Shadow5Sampler, Shadow5Texture)
ContinueSampler(sampler, Shadow5Texture, Shadow5Sampler, Shadow5Texture)
#if !__PS3
     AddressU  = BORDER;
     AddressV  = BORDER;
     AddressW  = BORDER;
     BorderColor = 0xffffffff;
#endif    
  #if USE_VARIANCE_SHADOWS
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
#else
    MIPFILTER = POINT;
    MINFILTER = POINT;
    MAGFILTER = POINT;
#endif
EndSharedSampler;

BeginSampler(sampler, Shadow6Texture, Shadow6Sampler, Shadow6Texture)
ContinueSampler(sampler, Shadow6Texture, Shadow6Sampler, Shadow6Texture)
#if !__PS3
     AddressU  = BORDER;
     AddressV  = BORDER;
     AddressW  = BORDER;
     BorderColor = 0xffffffff;
#endif    
  #if USE_VARIANCE_SHADOWS
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
#else
    MIPFILTER = POINT;
    MINFILTER = POINT;
    MAGFILTER = POINT;
#endif
EndSharedSampler;

BeginSampler(sampler, Shadow7Texture, Shadow7Sampler, Shadow7Texture)
ContinueSampler(sampler, Shadow7Texture, Shadow7Sampler, Shadow7Texture)
#if !__PS3
     AddressU  = BORDER;
     AddressV  = BORDER;
     AddressW  = BORDER;
     BorderColor = 0xffffffff;
#endif    
  #if USE_VARIANCE_SHADOWS
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
#else
    MIPFILTER = POINT;
    MINFILTER = POINT;
    MAGFILTER = POINT;
#endif
EndSharedSampler;

BeginSampler(sampler, Shadow8Texture, Shadow8Sampler, Shadow8Texture)
ContinueSampler(sampler, Shadow8Texture, Shadow8Sampler, Shadow8Texture)
#if !__PS3
     AddressU  = BORDER;
     AddressV  = BORDER;
     AddressW  = BORDER;
     BorderColor = 0xffffffff;
#endif    
  #if USE_VARIANCE_SHADOWS
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
    MAGFILTER = LINEAR;
#else
    MIPFILTER = POINT;
    MINFILTER = POINT;
    MAGFILTER = POINT;
#endif
EndSharedSampler;
#define USE_PERVERTEX_LIGHTS		0
#define USE_8_LIGHTS				1


struct VertexOutput 
{
    float4 Pos        : POSITION;
    float4 WorldPos	  : TEXCOORD0;
    float4 Normal      : TEXCOORD1;
   
    
    // have 8 texcoords
    float4 shadowCoord0 : TEXCOORD2;
    float4 shadowCoord1 : TEXCOORD3;
    float4 shadowCoord2 : TEXCOORD4;
    
    float4 shadowCoord3 : TEXCOORD8;
    
    float4 shadowCoord20 : TEXCOORD5;
    float4 shadowCoord21 : TEXCOORD6;
    float4 shadowCoord22 : TEXCOORD7;
    
    float4 shadowCoord23 : TEXCOORD9;
    
 #if ( __XENON && USE_PERVERTEX_LIGHTS )
    float4	lightDirX : TEXCOORD8;
    float4	lightDirY : TEXCOORD9;
    float4	lightDirZ : TEXCOORD10;
    float4	lightDirAtten : TEXCOORD11;
    float4  lightAngle : TEXCOORD12;
 #endif
};




float4		FastSpotDirection[ 12] : FastSpotDirection;
float4		FastSpotFadeScale[4]: FastSpotFadeScale;
float4		FastSpotFadeOffset[4] : FastSpotFadeOffset ;
float4		FastSpotDropOff[4] : FastSpotDropOff;
float4x4	ShadowMatrix[8] : ShadowMatrix;
float2		MaxShadowFade : MaxShadowFade;

// variance shadow mapping 

	

// bool	gUseVarianceShadowMaps : UseVarianceShadowMapsx4 = true;
float4 fLightVSMEpsilonx4[4] : LightVSMEpsilonx4;
float4 fDepthBiasx4[4] : LightVSMBiasx4;
float4 gOneOverLightAttenuationEndx4[4]: gOneOverLightAttenuationEndx4;

// Ambient Occlusion
float2	gAmbientOcclusionFadeIn : AmbientOcclusionFadeIn;

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

 float4 ShadowValue( float3 worldPos )
 {
	// fetch pixel depth from the shadow map
	// this is also from the point of view from the light camera
	
	float4 pixDepth;
	float4 pos0 = mul( float4(worldPos,1.0f), ShadowMatrix[0] );
	pos0.xyz *= 1.0  / pos0.w; 
	pixDepth.x =  pos0.z; 
	
	float4 pos1 = mul( float4(worldPos,1.0f), ShadowMatrix[1] );
	pos1.xyz *=  1.0 / pos1.w; 
	pixDepth.y =  pos1.z; 
	
	float4 pos2 = mul( float4(worldPos,1.0f), ShadowMatrix[2] );
	pos2.xyz *=  1.0  / pos2.w; 
	pixDepth.z =  pos2.z; 
	
	float4 pos3 = mul( float4(worldPos,1.0f), ShadowMatrix[3] );
	pos3.xyz *=  1.0  / pos3.w; 
	pixDepth.w =  pos3.z; 
	
	float4 depth;
	depth.x = tex2D( Shadow1Sampler, pos0.xy ).x;  // change to 2D proj maybe	
	depth.y = tex2D( Shadow2Sampler, pos1.xy ).x;  // change to 2D proj maybe	
	depth.z = tex2D( Shadow3Sampler, pos2.xy ).x;  // change to 2D proj maybe	
	depth.w = tex2D( Shadow4Sampler, pos3.xy ).x;  // change to 2D proj maybe	
	
	// get the pixel z bias value
	return (pixDepth >= depth);
}
float4 ShadowVSMValue( float3 worldPos, float4 lightDist )
{
	float4 pos0 = mul( float4(worldPos,1.0f), ShadowMatrix[0] );
	float4 pos1 = mul( float4(worldPos,1.0f), ShadowMatrix[1] );
	float4 pos2 = mul( float4(worldPos,1.0f), ShadowMatrix[2] );
	float4 pos3 = mul( float4(worldPos,1.0f), ShadowMatrix[3] );
	
	return 1.0f;//CompareDepthVarianceShadowBy4( pos0, pos1, pos2, pos3, lightDist);
}
float4 ShadowVSMValueTex( VertexOutput IN, float4 lightDist, float4 recipW  )
{
	IN.shadowCoord0 *= recipW.xxyy;
	IN.shadowCoord1 *= recipW.zzww;
	
	return shadowVariance_CompareDepthBy4( IN.shadowCoord0, IN.shadowCoord1, lightDist,
											Shadow1Sampler, Shadow2Sampler, Shadow3Sampler,Shadow4Sampler,
											gOneOverLightAttenuationEndx4[0], fDepthBiasx4[0], fLightVSMEpsilonx4[0] );
}


//------------------- vertex spot lights -- converts them to directional lights
void SetupVertexSpotLights( float3 pos, out float4 lightDirX, out float4 lightDirY, out float4 lightDirZ, out float4 lightDirAtten, out float4 lightAngle )
{
    FastLight4 lgt = FastLight4GetConstants(1);
    lightDirX = lgt.posX - pos.xxxx;
	lightDirY = lgt.posY - pos.yyyy;	
	lightDirZ = lgt.posZ - pos.zzzz;
	
	float4 dist = lightDirX * lightDirX;
	dist = lightDirY * lightDirY + dist;
	dist = lightDirZ * lightDirZ + dist;				// 6 instructions	
	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct

	lightDirAtten = sqrt( dist );
	float4 recipAtten= 1.0f / lightDirAtten;
	
	lightDirX *= recipAtten;
	lightDirY *= recipAtten;
	lightDirZ *= recipAtten;
	
    lightAngle = lightDirX * -FastSpotDirection[3 + 0 ];
	lightAngle = lightDirY * -FastSpotDirection[3 + 1 ] + lightAngle;
	lightAngle = lightDirZ * -FastSpotDirection[3 + 2 ] + lightAngle;
	lightAngle = lightAngle * FastSpotFadeScale[1] +  FastSpotFadeOffset[1];	
	
	lightDirX *= atten;
	lightDirY *= atten;
	lightDirZ *= atten;
}

float4 CalculatePerVertexSpotLights( float3 normal, float4 lightDirX, float4 lightDirY, float4 lightDirZ, float4 lightAngle )
{
	float4 light = normal.x * lightDirX;
	light = normal.y * lightDirY + light;
	light = saturate( normal.z * lightDirZ + light );
	return saturate( light * lightAngle );
}	
 
//----------------------------------------
#ifndef NO_SKINNING

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VSskin(rageSkinVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;
	rageSkinMtx boneMtx = ComputeSkinMtx( IN.blendindices, IN.weight );
    float3 pos = rageSkinTransform(IN.pos, boneMtx);
    
    // output position value
    OUT.Pos = mul(float4(pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos =  mul(float4(pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal.xyz = normalize(mul(IN.normal.xyz, (float3x3)gWorld));
	OUT.Normal.w = 0.0f;//IN.diffuse.x * gAmbientOcclusionFadeIn.x + gAmbientOcclusionFadeIn.y;
      
    // output texture coordinate
	//OUT.Color = IN.diffuse;
	
	float4 shadow1  = mul( OUT.WorldPos, ShadowMatrix[0] );
	float4 shadow2  = mul( OUT.WorldPos, ShadowMatrix[1] );
	float4 shadow3  = mul( OUT.WorldPos, ShadowMatrix[2] );
	float4 shadow4  = mul( OUT.WorldPos, ShadowMatrix[3] );
	
	OUT.shadowCoord0.xy  = shadow1.xy;
	OUT.shadowCoord0.zw  = shadow2.xy;
	
	OUT.shadowCoord1.xy  = shadow3.xy;
	OUT.shadowCoord1.zw  = shadow4.xy;
	
    OUT.shadowCoord2.x  = shadow1.w;
    OUT.shadowCoord2.y  = shadow2.w;
    OUT.shadowCoord2.z  = shadow3.w;
    OUT.shadowCoord2.w  = shadow4.w;
    
    OUT.shadowCoord3.x  = shadow1.z;
    OUT.shadowCoord3.y  = shadow2.z;
    OUT.shadowCoord3.z  = shadow3.z;
    OUT.shadowCoord3.w  = shadow4.z;

    
    float4 shadow5  = mul( OUT.WorldPos, ShadowMatrix[4] );
	float4 shadow6  = mul( OUT.WorldPos, ShadowMatrix[5] );
	float4 shadow7  = mul( OUT.WorldPos, ShadowMatrix[6] );
	float4 shadow8  = mul( OUT.WorldPos, ShadowMatrix[7] );
	
	OUT.shadowCoord20.xy  = shadow5.xy;
	OUT.shadowCoord20.zw  = shadow6.xy;
	
	OUT.shadowCoord21.xy  = shadow7.xy;
	OUT.shadowCoord21.zw  = shadow8.xy;
	
    OUT.shadowCoord22.x  = shadow5.w;
    OUT.shadowCoord22.y  = shadow6.w;
    OUT.shadowCoord22.z  = shadow7.w;
    OUT.shadowCoord22.w  = shadow8.w;
    
	OUT.shadowCoord23.x  = shadow5.z;
    OUT.shadowCoord23.y  = shadow6.z;
    OUT.shadowCoord23.z  = shadow7.z;
    OUT.shadowCoord23.w  = shadow8.z;
    
 #if ( __XENON && USE_PERVERTEX_LIGHTS )
	SetupVertexSpotLights( OUT.WorldPos, OUT.lightDirX, OUT.lightDirY, OUT.lightDirZ, OUT.lightDirAtten, OUT.lightAngle );
 #endif

	 
    return OUT;
}
#endif
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos =  mul(float4(IN.pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal.xyz = normalize(mul(IN.normal.xyz, (float3x3)gWorld));
	OUT.Normal.w = IN.diffuse.x * gAmbientOcclusionFadeIn.x + gAmbientOcclusionFadeIn.y;
      
    // output texture coordinate
	//OUT.Color = IN.diffuse;
	
	float4 shadow1  = mul( OUT.WorldPos, ShadowMatrix[0] );
	float4 shadow2  = mul( OUT.WorldPos, ShadowMatrix[1] );
	float4 shadow3  = mul( OUT.WorldPos, ShadowMatrix[2] );
	float4 shadow4  = mul( OUT.WorldPos, ShadowMatrix[3] );
	
	OUT.shadowCoord0.xy  = shadow1.xy;
	OUT.shadowCoord0.zw  = shadow2.xy;
	
	OUT.shadowCoord1.xy  = shadow3.xy;
	OUT.shadowCoord1.zw  = shadow4.xy;
	
    OUT.shadowCoord2.x  = shadow1.w;
    OUT.shadowCoord2.y  = shadow2.w;
    OUT.shadowCoord2.z  = shadow3.w;
    OUT.shadowCoord2.w  = shadow4.w;
    
        OUT.shadowCoord3.x  = shadow1.z;
    OUT.shadowCoord3.y  = shadow2.z;
    OUT.shadowCoord3.z  = shadow3.z;
    OUT.shadowCoord3.w  = shadow4.z;
    
    OUT.shadowCoord3 = OUT.shadowCoord3;
    
    float4 shadow5  = mul( OUT.WorldPos, ShadowMatrix[4] );
	float4 shadow6  = mul( OUT.WorldPos, ShadowMatrix[5] );
	float4 shadow7  = mul( OUT.WorldPos, ShadowMatrix[6] );
	float4 shadow8  = mul( OUT.WorldPos, ShadowMatrix[7] );
	
	OUT.shadowCoord20.xy  = shadow5.xy;
	OUT.shadowCoord20.zw  = shadow6.xy;
	
	OUT.shadowCoord21.xy  = shadow7.xy;
	OUT.shadowCoord21.zw  = shadow8.xy;
	
    OUT.shadowCoord22.x  = shadow5.w;
    OUT.shadowCoord22.y  = shadow6.w;
    OUT.shadowCoord22.z  = shadow7.w;
    OUT.shadowCoord22.w  = shadow8.w;
    
   	OUT.shadowCoord23.x  = shadow5.z;
    OUT.shadowCoord23.y  = shadow6.z;
    OUT.shadowCoord23.z  = shadow7.z;
    OUT.shadowCoord23.w  = shadow8.z;
    
    OUT.shadowCoord23 = OUT.shadowCoord23;
 #if ( __XENON && USE_PERVERTEX_LIGHTS )
	SetupVertexSpotLights( OUT.WorldPos, OUT.lightDirX, OUT.lightDirY, OUT.lightDirZ, OUT.lightDirAtten, OUT.lightAngle );
 #endif

	 
    return OUT;
}
float4 CalculateDistance4( float3 pos, FastLight4 lgt, out float4 dX, out float4 dY, out float4 dZ , out float4 recipDist )
{  

	dX = lgt.posX - pos.xxxx;
	dY = lgt.posY - pos.yyyy;
	dZ = lgt.posZ - pos.zzzz;

	float4 dist = dX * dX;
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions	

	recipDist = rsqrt( dist );
	return dist * recipDist;
 } 
float4 CalculateDistance4Combined( float3 pos, float4 pX, float4 pY,float4 pZ, out float4 dX, out float4 dY, out float4 dZ , out float4 recipDist, out float4 inv,
									float4 val  )
{  
float4 dist;
#if __XENON
 asm
 {
	add dX, pX , -pos.xxxx
		+ rcp inv.x, val.x
	add dY, pY , -pos.yyyy
		+ rcp inv.y, val.y
	add dZ, pZ , -pos.yyyy
		+ rcp inv.z, val.z
		
	mul dist, dX , dX
	  + rcp inv.w, val.w
};
#endif
	dist = dY * dY + dist;
	dist = dZ * dZ + dist;				// 6 instructions	

	recipDist = rsqrt( dist );
	return dist * recipDist;
 }  
 
 float4 CalculateSpot4Approx( float4 spotDist, float4 spotX, float4 spotY, float4 scale, float4 bias, 
								float4 spotDirX, float4 spotDirY, float4 spotDirZ, float4 oneOverRangeSq , float3 normal)
 {
	float4 atten = saturate( ( spotDist * oneOverRangeSq  ) + 1 );	// 1 instruct
	float4 fallOff = -spotX * spotX;
	fallOff += -spotY * spotY + 1.0;
	
	float4 light = spotDirX * normal.xxxx;
	light = spotDirY * normal.yyyy + light;
	light = saturate( spotDirZ * normal.zzzz + light );
	
	return light *  saturate( fallOff ) * atten;
 }
 
 float4 SlickApprox( float4 t, float4 n )
 {
	float4 demon = n - n* t + t;
	return t * ( 1.0f/demon );
 }
 
 float4 CalculateSpot4( float3 normal, FastLight4 lgt, float4 dX, float4 dY, float4 dZ, float4 dist, float4 recip,
														float4 spotDirX, float4 spotDirY, float4 spotDirZ,
														float4 SpotFadeScale, float4 SpotFadeOffset, float4 spotDropOff,
														float4 shadows	 )
 {
	float4 atten = saturate( ( dist * -lgt.OneOverRangeSq ) + 1 );	// 1 instruct

	float4 light = dX * normal.xxxx;
	light = dY * normal.yyyy + light;
	light = dZ * normal.zzzz + light;
	light *= atten; // 4 instructions
	light = saturate( light * recip );	
	
	float4 fallOff = dX * -spotDirX;  // could be tex lookup with projection matrix ?? save 6 - 8 instructions per 4 lights
	fallOff = dY * -spotDirY + fallOff;
	fallOff = dZ * -spotDirZ + fallOff;
	fallOff *=recip;
	
	// apply shadows only 
	shadows = saturate( shadows + saturate(fallOff * MaxShadowFade.x + MaxShadowFade.y) );
	
	float4 dropOff = SlickApprox( fallOff, spotDropOff );//exp( fallOff );//pow( fallOff, spotDropOff);
	float4 spotInten = saturate( fallOff * SpotFadeScale + SpotFadeOffset );
	light = saturate( spotInten * light * dropOff);	
	return light * shadows;
}
	
float4 PerVertexLights( VertexOutput IN, float3 norm, FastLight4 lgt  ) 
{
 #if ( __XENON && USE_PERVERTEX_LIGHTS )
	float4 invW = 1.0f / IN.shadowCoord22;
	IN.shadowCoord20 *= invW.xxyy;
	IN.shadowCoord21 *= invW.zzww;

	float4 perVershadows  =shadowVariance_CompareDepthBy4( IN.shadowCoord20, IN.shadowCoord21,  IN.lightDirAtten,
															Shadow5Sampler, Shadow6Sampler, Shadow7Sampler,Shadow8Sampler,
															gOneOverLightAttenuationEndx4[1], fDepthBiasx4[1], fLightVSMEpsilonx4[1] );
	
	float4 light = CalculatePerVertexSpotLights( norm.xyz, IN.lightDirX, IN.lightDirY, IN.lightDirZ, IN.lightAngle );
	//return perVershadows;
	
	light *=perVershadows;
	float3	col = float3( dot( lgt.colR, light  ),
						   dot( lgt.colG, light  ),
							dot( lgt.colB, light  ) );	// 3
	
	return float4(  col, 1.0f );
#else
	return 1;
#endif
}

float4 Calcuate4Lights( float4 zDist, float3 norm, VertexOutput IN, FastLight4 lgt, float4 shadowCoord0, float4 shadowCoord1, float4 shadowCoord2,
							sampler shad1, sampler shad2, sampler shad3, sampler shad4,
							float4 ShadAttenuation, float4 ShadBias, float4 ShadEplision,
							float4 spotDirectionX, float4 spotDirectionY, float4 spotDirectionZ, float4 SpotScale, float4 SpotOffset, float4 SpotDropOff )						
{							
	float4 dX;
	float4 dY;
	float4 dZ;
	float4 recipDist;
	float4 invW;
	
	//
	invW = 1.0f/ shadowCoord2;
	float4 distLight =CalculateDistance4( IN.WorldPos.xyz, lgt, dX, dY, dZ, recipDist );
//	distLight = zDist;
	shadowCoord0 *= invW.xxyy;
	shadowCoord1 *= invW.zzww;
	
	float4 shadows = shadowVariance_CompareDepthBy4( shadowCoord0, shadowCoord1,  distLight,
													shad1, shad2, shad3,shad4,
													ShadAttenuation, ShadBias, ShadEplision );
	float4 light = CalculateSpot4( norm, lgt, dX, dY, dZ, distLight, recipDist,
									 spotDirectionX, spotDirectionY, spotDirectionZ, SpotScale, SpotOffset, SpotDropOff, shadows );
			 
	light = light * IN.Normal.w ;
	float3 col = float3( dot( lgt.colR, light  ),
						   dot( lgt.colG, light  ),
							dot( lgt.colB, light  ) );	// 3
	
	return float4(col, 1.0f);
}
float4 PSFast4( VertexOutput IN ) 
{
	
		
	float3 norm = normalize( IN.Normal.xyz );
	
	float4 col = 0;
	
	//float4 spotDropOff = 32.0f;
	
 #if  ( __XENON && USE_PERVERTEX_LIGHTS )

	FastLight4 lgt2 = FastLight4GetConstants(1);
	col = PerVertexLights( IN, norm, lgt2) ;
#else

#if USE_8_LIGHTS
	col += Calcuate4Lights( IN.shadowCoord23, norm, IN, FastLight4GetConstants(1), IN.shadowCoord20, IN.shadowCoord21, IN.shadowCoord22,
							Shadow5Sampler, Shadow6Sampler, Shadow7Sampler,Shadow8Sampler,
							gOneOverLightAttenuationEndx4[1], fDepthBiasx4[1], fLightVSMEpsilonx4[1],
							FastSpotDirection[3],FastSpotDirection[4],FastSpotDirection[5], FastSpotFadeScale[1], FastSpotFadeOffset[1], FastSpotDropOff[1] );
#endif

#endif


	col += Calcuate4Lights( IN.shadowCoord3, norm, IN, FastLight4GetConstants(0), IN.shadowCoord0, IN.shadowCoord1, IN.shadowCoord2,
							Shadow1Sampler, Shadow2Sampler, Shadow3Sampler,Shadow4Sampler,
							gOneOverLightAttenuationEndx4[0], fDepthBiasx4[0], fLightVSMEpsilonx4[0],
							FastSpotDirection[0],FastSpotDirection[1],FastSpotDirection[2], FastSpotFadeScale[0], FastSpotFadeOffset[0], FastSpotDropOff[0]  );
												
	
	return col;
}
//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS( VertexOutput IN ) : COLOR
{
	return PSFast4( IN );
}

technique drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;   
        
        ZEnable = true;
        ZWriteEnable = true; 
 #ifndef NO_SKINNING
        VertexShader = compile VERTEXSHADER VSskin();
 #else
         VertexShader = compile VERTEXSHADER VS();
 #endif
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ZEnable = true;
        ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;   
        
        ZEnable = true;
        ZWriteEnable = true; 
 #ifndef NO_SKINNING
        VertexShader = compile VERTEXSHADER VSskin();
 #else
         VertexShader = compile VERTEXSHADER VS();
 #endif
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        ZEnable = true;
        ZWriteEnable = true;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}
