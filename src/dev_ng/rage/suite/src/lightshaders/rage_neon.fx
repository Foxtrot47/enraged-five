//-----------------------------------------------------
// Rage neon shader
//
#define NO_SKINNING

#include "../../../base/src/shaderlib/rage_diffuse_sampler.fxh"
#include "../../../base/src/shaderlib/rage_common.fxh"
#define USE_MANYSHADOWS
#include "rage_manylightsTex.fxh"

struct VertexOutput 
{
    float4 Pos        : POSITION;
	float2 TexCoord		:TEXCOORD2;
};

float		IntensityScale
<
	string UIName = "Intensity Scale ";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 5.0;
> = 1.0f;

float		IntensityBias
<
	string UIName = "Intensity Bias";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 5.0;
> = 0.1f;


struct VertexInput
{
	float3 pos : POSITION;
	float2 texCoord0: TEXCOORD0;
};

//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(VertexInput IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
  
    OUT.TexCoord = IN.texCoord0;
    return OUT;
}



//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS( VertexOutput IN ) : COLOR
{
	return float4( tex2D( DiffuseSampler, IN.TexCoord).xyz * IntensityScale + IntensityBias, 1.0f);
}

technique drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
       // AlphaBlendEnable = false;    
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
      //  AlphaBlendEnable = false;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

