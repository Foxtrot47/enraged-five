//-----------------------------------------------------
// Rage Diffuse
//

// Get the pong globals
 #define _SORT_
#define NO_SKINNING
#include "../../../base/src/shaderlib/rage_common.fxh"

#ifndef _SORT_
#include "../../../base/src/shaderlib/rage_fastLight.fxh"
#endif


#ifdef _SORT_
float4	FastLightKdTreeSplit[16] : FastLightKdTreeSplit;
float4	LightPositions[32] : LightPositions;
float4  LightColors[32] : LightColors;
float4  LightDirections[32]: LightDirections;
float4	FastLightKdTreeLeafValues[16]: FastLightKdTreeLeafValues;
float4	FastLightKdTreeVSLeafValues[16]: FastLightKdTreeVSLeafValues;
float3	LightSortScale : LightSortScale;
float3	LightSortBias : LightSortBias;

#endif

struct VertexOutput 
{
    float4 Pos        : POSITION;
    float4 WorldPos	  : TEXCOORD0;
    float3 Normal      : TEXCOORD2;
    float4 Color		: TEXCOORD9;
   
 
//    DECLARE_SH2_COLOR
   // float4 lightDX	: TEXCOORD3;
    //float4 lightDY	: TEXCOORD4;
    //float4 lightDZ	: TEXCOORD5;
    
    //float4 lightDX2	: TEXCOORD6;
    //float4 lightDY2	: TEXCOORD7;
    //float4 lightDZ2	: TEXCOORD8;
};

//AddSphericalLight2ndOrder( float3 col, float3 dir, SH2Color sh )

BeginSharedSampler( sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9)
ContinueSharedSampler(sampler, ShadowAtlasTexture, ShadowAtlasSampler, ShadowAtlasTexture, s9 )
	AddressU = CLAMP;
	AddressV = CLAMP;
	AddressW = CLAMP;	
	MIPFILTER = LINEAR;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR; 
EndSharedSampler;



BeginSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
ContinueSharedSampler(sampler2D,LightSortTexture, LightSortSampler,LightSortTexture, s8)
		AddressU = CLAMP;
		AddressV = CLAMP;
		AddressW = CLAMP;
		MIPFILTER = POINT;
		MINFILTER = POINT;
		MAGFILTER = POINT; 
		MipMapLodBias = 0;
EndSharedSampler;


#ifdef _SORT_

//-------------------- Sorting Code
void SwapMax( float4 a, float4 b, out float4 ra, out float4 rb )  // 3 instructions
{
	ra =  min( a, b );
	rb =  max( a, b );//sel ?   a : b;  // rotate rb
}
float4 SortAscending( float4 a, float4 b  )
{	
	float4 ta = min( a, b );
	float4 tb = max( a, b);
	
	a = min( ta, tb.yzwx );
	b = max( ta, tb.yzwx);
	
	ta = min( a, b.yzwx );
	tb = max( a, b.yzwx);
	
	a = min( ta, tb.yzwx );
	
	return a;
}
float4 sort16( float4 A, float4 B, float4 C,  float4 D)
{
	float4 ra = SortAscending( A, B  );
	float4 rb = SortAscending( C, D);
	return SortAscending( ra, rb );
}
float4 sortApprox16( float4 A, float4 B, float4 C,  float4 D)
{
	float4 ta;
	float4 tb;
	A = min( A, B );	// insertion sort
	C = min( C, D );
	return SortAscending( A, C );
}
float4 encodeIndices( float4 offset, float4 values )
{
	return ((int4) ( values.xyzw  * 0.001)) + offset.xyzw ;
}
float4 decodeIndices(  float4 values )
{
	return( frac( values.xyzw ) *16.0f );
}
//-----------------------------------------
//------------------------ distance calculating code
#define MAX_LIGHT_CONSTS	4

float4	Light4PosX[ MAX_LIGHT_CONSTS] : Light4PosX;
float4	Light4PosY[ MAX_LIGHT_CONSTS] : Light4PosY;
float4	Light4PosZ[ MAX_LIGHT_CONSTS] : Light4PosZ;

float4 computeCost( float3 pos, float4 X, float4 Y,float4 Z )
{
	float4 dX = X.xyzw - pos.xxxx;
		float4 dZ = Z.xyzw - pos.zzzz;
	float4 dist = dX.xyzw * dX.xyzw;
	float4 dY = Y.xyzw - pos.yyyy;
	dist.xyzw = dY.xyzw * dY.xyzw + dist.xyzw;
	dist.xyzw = dZ.xyzw * dZ.xyzw + dist.xyzw;			// 4
	return dist.xyzw;
}
float4 computeCostCly( float3 pos, float4 X, float4 Z )
{
	float4 dX = X.xyzw - pos.xxxx;
	float4 dZ = Z.xyzw - pos.zzzz;
	float4 dist = dX.xyzw * dX.xyzw;
	dist.xyzw = dZ.xyzw * dZ.xyzw + dist.xyzw;			// 4
	return dist.xyzw;
}
void CalculateCost( float3 pos, out float4 A, out float4 B, out float4 C, out float4 D )
{
	A = computeCostCly( pos, Light4PosX[0], Light4PosZ[0] );
	A = encodeIndices(  float4( 0.0f, 1.0f, 2.0f, 3.0f )/16.0f, A );
	
	B = computeCostCly( pos, Light4PosX[1],  Light4PosZ[1] );
	B = encodeIndices(  float4( 4.0f, 5.0f, 6.0f, 7.0f )/16.0f, B );
	
	C = computeCostCly( pos, Light4PosX[2], Light4PosZ[2] );
	C = encodeIndices( float4( 8.0f, 9.0f, 10.0f, 11.0f )/16.0f, C );
	
	D = computeCostCly( pos, Light4PosX[3], Light4PosZ[3] );
	D = encodeIndices(  float4( 12.0f, 13.0f, 14.0f, 15.0f )/16.0f, D );
}
float4 SortApprox( float3 pos  )
{
	float4 A = computeCostCly( pos, Light4PosX[0], Light4PosZ[0] );
	A = encodeIndices(  float4( 0.0f, 1.0f, 2.0f, 3.0f )/16.0f, A );
	
	float4 B = computeCostCly( pos, Light4PosX[1], Light4PosZ[1] );
	B = encodeIndices(  float4( 4.0f, 5.0f, 6.0f, 7.0f )/16.0f, B );
	
	A = min( A, B );
	
	float4 C = computeCostCly( pos, Light4PosX[2], Light4PosZ[2] );
	C = encodeIndices( float4( 8.0f, 9.0f, 10.0f, 11.0f )/16.0f, C );
	
	float4 D = computeCostCly( pos, Light4PosX[3], Light4PosZ[3] );
	D = encodeIndices(  float4( 12.0f, 13.0f, 14.0f, 15.0f )/16.0f, D );
	C = min( C, D );
	
	return SortAscending( A, C );
}
float4 SortApprox12( float3 pos  )
{
	float4 A = computeCostCly( pos, Light4PosX[0], Light4PosZ[0] );
	A = encodeIndices(  float4( 0.0f, 1.0f, 2.0f, 3.0f )/16.0f, A );
	
	float4 B = computeCostCly( pos, Light4PosX[1], Light4PosZ[1] );
	B = encodeIndices(  float4( 4.0f, 5.0f, 6.0f, 7.0f )/16.0f, B );
	
	A = min( A, B );
	
	float4 C = computeCostCly( pos, Light4PosX[2], Light4PosZ[2] );
	C = encodeIndices( float4( 8.0f, 9.0f, 10.0f, 11.0f )/16.0f, C );	
	return SortAscending( A, C );
}

float4 Select4ClosestLights( float3 pos )
{
	float4 res = SortApprox( pos );
	return  decodeIndices( res );	
}

float4 Test()
{
	float4 test = float4( 10.0f, 600.0, 300.0f, 10.0 );
	test = encodeIndices(0, test );
	return decodeIndices( test );
}

//-------------------
#endif

#if !__XENON
float3 CalculateDiffuseFastLight4All( float3 worldPos, float3 normal )
{
	float3 res = 0;
	return res;
}
//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos = mul(float4(IN.pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal = normalize(mul(IN.normal.xyz, (float3x3)gWorld));
      
    // output texture coordinate
     	
  //  PreCalculateDirections( FastLight4GetConstants(2), OUT.WorldPos.xyz, OUT.lightDX, OUT.lightDY,  OUT.lightDZ );
  //  PreCalculateDirections( FastLight4GetConstants(3), OUT.WorldPos.xyz, OUT.lightDX2, OUT.lightDY2,  OUT.lightDZ2 );
 #ifndef _SORT_
	SH2Color sh;
	sh.red = 0;
	sh.green = 0;
	sh.blue = 0;
	GET_SH2_COLOR( sh )
#endif
	OUT.Color = IN.diffuse;
    return OUT;
}
#else
float3 CalculateSpot( float3 pos, float3 normal, float4 lpos1, float4 col1, float4 ldir )
{
	float3 d1 = lpos1.xyz - pos.xyz;
	float dist = dot( d1, d1 );
	float recipicol = rsqrt(dist);		// 4 instructions should be paired on 360
	float3 dir =  d1 * recipicol;

	//float atten = saturate(  col1.w /sqrt(dist)  );	// 1 instruct
	float atten = saturate(  col1.w / dist  );	// 1 instruct now quadratic
	
	float light =saturate( dot( dir.xyz, normal.xyz ) );
	light *= atten;
	float spot = dot( ldir.xyz, -dir.xyz );
	light *= saturate(  spot * ldir.w  + lpos1.w  );
	return col1.xyz * light.x;
}

float3 CalculateDiffuse( float3 pos, float3 normal, float3 lpos1, float4 col1 )
{   
	float3 d1 = lpos1 - pos;
	float dist = dot( d1, d1 );
	float recipicol = rsqrt(dist);		// 4 instructions should be paired on 360
	float3 ldir =  d1 * recipicol;

	//float atten = saturate(  col1.w /sqrt(dist)  );	// 1 instruct
	float atten = saturate(  col1.w / dist  );	// 1 instruct now quadratic
	
	float light =saturate( dot( ldir.xyz, normal.xyz ) );
	light *= atten;
	
	return col1.xyz * light.x;
}
float3 CalculateSpecular( float3 reflectedEye, float3 pos, float3 normal, float3 lpos1, float4 col1 )
{   
	float3 d1 = lpos1 - pos;
	float dist = dot( d1, d1 );
	float recipicol = rsqrt(dist);		// 4 instructions should be paired on 360
	float3 ldir =  d1 * recipicol;

	//float atten = saturate(  col1.w /sqrt(dist)  );	// 1 instruct
	float atten = saturate(  col1.w / dist  );	// 1 instruct now quadratic
	float2 light;
	
	light.x =dot( ldir.xyz, normal.xyz );
	light.y = saturate( pow( dot( ldir.xyz, reflectedEye.xyz ),128.0 ) )* ( light.x > 0.0f );
	light.x= saturate( light.x * atten );
	light.y *=saturate( atten * 8.0f);
	
	return col1.xyz * light.x + col1.xyz * light.y ;
}
#ifdef _SORT_
int FastLgtGetBatchToUse( float3 pos )
{
	// look up into a kdtree
		float3 cmp = pos.xzz > FastLightKdTreeSplit[0].xyz;
		float4 split = cmp.x ? float4( 2.0f, 0.0f, 1.0f , 1.0f) : float4( 0.0f,1.0f, 0.0f, 1.0f) ;
		split.yz *= cmp.yz; 
		split.w = FastLightKdTreeSplit[0].w;
		float idx =dot(  split, 1.0f );
		
		// second level
		// look up into a kdtree
		cmp = pos.xzz > FastLightKdTreeSplit[idx].xyz;
		split = cmp.x ? float4( 2.0f, 0.0f, 1.0f, 1.0f ) : float4( 0.0f,1.0f, 0.0f, 1.0f) ;
		split.yz *= cmp.yz;
		split.w =  FastLightKdTreeSplit[idx].w;
		return dot(  split, 1.0f );
}
#endif

float3 GetLights( float3 worldPos, float3 normal,  float4 lightIndices )
{
	 return CalculateSpot(  worldPos, normal, 	LightPositions[ lightIndices.x ], LightColors[ lightIndices.x], LightDirections[ lightIndices.x] ) 
			+  CalculateSpot( worldPos, normal, LightPositions[ lightIndices.y ], LightColors[ lightIndices.y], LightDirections[ lightIndices.y] ) 
			+	CalculateSpot(  worldPos, normal, LightPositions[ lightIndices.z ], LightColors[ lightIndices.z] , LightDirections[ lightIndices.z]) 
			+ 	CalculateSpot(  worldPos, normal, LightPositions[ lightIndices.w ], LightColors[ lightIndices.w], LightDirections[ lightIndices.w] ) ;
}
	
	
//------------------------------------------------------
// Vertex Shader
//------------------------------------------------------
VertexOutput VS(rageVertexInputBump IN) 
{
    VertexOutput OUT = (VertexOutput)0;

    // output position value
    OUT.Pos = mul(float4(IN.pos, 1.0), gWorldViewProj);
    
    // send world position to pixel shader
    OUT.WorldPos = mul(float4(IN.pos,1), gWorld);
    
    // create tangent space system
	OUT.Normal = normalize(mul(IN.normal.xyz, (float3x3)gWorld));
     
    
	float3 worldPos = OUT.WorldPos;
	float3 normal = OUT.Normal;
	
	// PreCalculateDirections( FastLight4GetConstants(2), OUT.WorldPos, OUT.lightDX, OUT.lightDY,  OUT.lightDZ );
#ifndef _SORT_
	SH2Color	sh;
	sh.red = 0;
	sh.green = 0;
	sh.blue = 0;
	
	sh = CalculateSH2DiffuseFastLight4( OUT.WorldPos, FastLight4GetConstants(0), sh );
	GET_SH2_COLOR(sh)
#endif

OUT.Color = IN.diffuse;//col;
#ifdef _SORT_
	//int i = FastLgtGetBatchToUse( worldPos );
	//int4 lightIndices = FastLightKdTreeVSLeafValues[i];
	//float3 col =	CalculateDiffuse( worldPos, normal, 	LightPositions[ lightIndices.x ], LightColors[ lightIndices.x] ) + //float4( 1.0f, 0.0f, 1.0f, 0.1f) )  + 
	//				CalculateDiffuse( worldPos, normal, LightPositions[ lightIndices.y ], LightColors[ lightIndices.y] ) +
	//				CalculateDiffuse( worldPos, normal, LightPositions[ lightIndices.z ], LightColors[ lightIndices.z] ) + //LightColors[ idx] );
	//				CalculateDiffuse( worldPos, normal, LightPositions[ lightIndices.w ], LightColors[ lightIndices.w] );
	

	int i = FastLgtGetBatchToUse( worldPos );
	float4 lightIndices2 = FastLightKdTreeVSLeafValues[i];
	
	OUT.Color.w = IN.diffuse.x;
	OUT.Color.xyz = GetLights( worldPos, normal, lightIndices2);
#endif
	
    return OUT;
}

#ifdef _SORT_

	
float3 CalculateDiffuseFastLight4All( float3 worldPos, float3 normal )
{
	float3 Eye = normalize( worldPos.xyz - gViewInverse[3].xyz );
	float3 M = reflect( normal, Eye );	// calculate the eye reflection amount  // 3

	//return TestSelect4ClosestLights( worldPos );
	int i  = FastLgtGetBatchToUse( worldPos ); //Select4ClosestLights( worldPos);
	float4 lightIndices = FastLightKdTreeLeafValues[i];
	//return dot( lightIndices, 0.1);
	
	
	return GetLights( worldPos, normal, lightIndices );
}
#endif
#endif
#define TEX_SORT


float4 PSFast4( VertexOutput IN ) 
{


				
#ifdef TEX_SORT
	//return float4( IN.WorldPos.xz* LightSortScale.xz + LightSortBias.xz , 0.0f, 1.0f );
	float2 SortTexCoord = IN.WorldPos.xz* LightSortScale.xz + LightSortBias.xz;
	//SortTexCoord.y = 1.0f - SortTexCoord.y;//
	float4 Indices =  tex2D( LightSortSampler, SortTexCoord ) * 255.0f + 0.5f;

#if __XENON
	//return float4( LightColors[Indices.x ].xyz , 1.0f);
	return float4( GetLights( IN.WorldPos.xyz,  normalize( IN.Normal.xyz ) ,  Indices )  * IN.Color.w, 1.0f);
#endif
	return float4( 1.0f, 0.0f, 1.0f, 0.0f );
#endif
	//return float4( CalculateDiffuseFastLight4( IN.WorldPos.xyz, normalize( IN.Normal.xyz ) , ImportanceSortLights( IN.WorldPos.xyz )	), 1.0f);
	//return lerp( float4( 0.2f, 1.0f, 0.0f, 1.0f ), float4( 0.1, 0.3f, 1.0f, 1.0f), ( IN.Normal.y + 1.0f ) * 0.5 );
#ifdef _SORT_
	return float4( (CalculateDiffuseFastLight4All( IN.WorldPos.xyz, normalize( IN.Normal.xyz ) ) + IN.Color.xyz) * IN.Color.w, 1.0f);
#else

	float3 Eye = normalize( IN.WorldPos.xyz - gViewInverse[3].xyz );
	float3 spec1;
	float3 spec2 = 0.0f;
	float3 spec3;
	float3 Normal = normalize( IN.Normal );
	
	float3 M = reflect( Eye, Normal );	// calculate the eye reflection amount  // 3
//	float3 col = CalculateDiffuseSpecularFastLight4( IN.WorldPos.xyz, Normal, M, 64.0f, FastLight4Constants(), spec1 );
//	col +=	CalculateDiffuseSpecularFastLight4( IN.WorldPos.xyz, Normal,  M, 64.0f,FastLight4GetConstants( 1), spec2 );
//	col +=	CalculateDiffuseSpecularFastLight4( IN.WorldPos.xyz, Normal,  M, 64.0f,FastLight4GetConstants( 2), spec3 );

	float3 col = CalculateDiffuseSpecularFastLight4( IN.WorldPos.xyz, Normal,  M, 64.0f, FastLight4GetConstants(0), spec1 );
	 col += CalculateDiffuseFastLight4( IN.WorldPos.xyz, Normal, FastLight4GetConstants(1) );
	 
	 // add ambient
	SH2Color	sh;
	SET_SH2_COLOR(sh)
	float4 visSH = ConvertDirToSH2( Normal );
	col = CalculateSphericalLight2ndOrder( visSH, sh );
	
	
	
	//col += CalculateDiffuseDirectional( Normal, IN.lightDX, IN.lightDY, IN.lightDZ, FastLight4GetConstants(2)  );
	//col += CalculateDiffuseDirectional( Normal, IN.lightDX2, IN.lightDY2, IN.lightDZ2, FastLight4GetConstants(3)  );

	// col += CalculateDiffuseFastLight4( IN.WorldPos.xyz, Normal, FastLight4GetConstants(2) );
	//FastLight4GetConstants( 2)
	return float4( col + spec1  , 1.0f); //float4( col * 0.5 + ( spec1 + spec2 + spec3) * 0.3, 1.0f );
#endif
}
//------------------------------------------------------
// Pixel Shader
//------------------------------------------------------
float4 PS( VertexOutput IN ) : COLOR
{
	return PSFast4( IN );
}

technique drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;    
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_drawskinned
{
    pass p0
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;    
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}

technique unlit_draw
{
    pass p0 
    {
        AlphaTestEnable = false;
        AlphaBlendEnable = false;
        VertexShader = compile VERTEXSHADER VS();
        PixelShader  = compile PIXELSHADER PS();
    }
}
