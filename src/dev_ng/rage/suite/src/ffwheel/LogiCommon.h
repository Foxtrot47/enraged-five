/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#ifndef _LOGI_COMMON_H_INCLUDED_
#define _LOGI_COMMON_H_INCLUDED_

#include "input/input.h"

#if HAVE_FFWHEEL

//#include <sys/types.h>
#include "liblgFF.h"

typedef rage::u64 LG_ULONG;

//////////////////////////////
// Do not change the following. The last 4 bits are always reserved for dpad.
#if __PS3
const LG_ULONG LG_DPAD_UP    = (0x1000000000000000ll);
const LG_ULONG LG_DPAD_DOWN  = (0x2000000000000000ll);
const LG_ULONG LG_DPAD_RIGHT = (0x4000000000000000ll);
const LG_ULONG LG_DPAD_LEFT  = (0x8000000000000000ll);
#else
const LG_ULONG LG_DPAD_UP    = (0x1000000000000000);
const LG_ULONG LG_DPAD_DOWN  = (0x2000000000000000);
const LG_ULONG LG_DPAD_RIGHT = (0x4000000000000000);
const LG_ULONG LG_DPAD_LEFT  = (0x8000000000000000);
#endif
//////////////////////////////

#define SAFE_DELETE(p) if (p) { delete p; p = NULL; }

#endif

#endif // _LOGI_COMMON_H_INCLUDED_
