/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#ifndef LOGI_GLOBALS_H_INCLUDED_
#define LOGI_GLOBALS_H_INCLUDED_

#include "input/input.h"

#if HAVE_FFWHEEL

#include <sys/types.h>
#include <stdio.h>
#include <assert.h>

namespace LogitechEZWheelWrapper
{
#if __PS3
#ifdef DEBUG
#define LOGITRACE(format, args...) printf(format, ## args)
#else
#define LOGITRACE(format, args...)
#endif
#else
#ifdef DEBUG
#define LOGITRACE(format, args...) printf(format, ## args)
#else
#define LOGITRACE __noop
#endif
#endif

#ifdef DEBUG
#define LOGIASSERT(test) assert(test)
#else
#define LOGIASSERT(test)
#endif

    // Re-define buttons and periodic types to make wrapper more cross platform with Gamecube and PC
#define LG_BUTTON_BUTTON0   LGBUTTON_BUTTON0
#define LG_BUTTON_BUTTON1   LGBUTTON_BUTTON1
#define LG_BUTTON_BUTTON2   LGBUTTON_BUTTON2
#define LG_BUTTON_BUTTON3   LGBUTTON_BUTTON3
#define LG_BUTTON_BUTTON4   LGBUTTON_BUTTON4
#define LG_BUTTON_BUTTON5   LGBUTTON_BUTTON5
#define LG_BUTTON_BUTTON6   LGBUTTON_BUTTON6
#define LG_BUTTON_BUTTON7   LGBUTTON_BUTTON7
#define LG_BUTTON_BUTTON8   LGBUTTON_BUTTON8
#define LG_BUTTON_BUTTON9   LGBUTTON_BUTTON9
#define LG_BUTTON_BUTTON10  LGBUTTON_BUTTON10
#define LG_BUTTON_BUTTON11  LGBUTTON_BUTTON11
#define LG_BUTTON_BUTTON12  LGBUTTON_BUTTON12
#define LG_BUTTON_BUTTON13  LGBUTTON_BUTTON13
#define LG_BUTTON_BUTTON14  LGBUTTON_BUTTON14
#define LG_BUTTON_BUTTON15  LGBUTTON_BUTTON15

#define LG_BUTTON_CROSS     LGBUTTON_CROSS
#define LG_BUTTON_SQUARE    LGBUTTON_SQUARE
#define LG_BUTTON_CIRCLE    LGBUTTON_CIRCLE
#define LG_BUTTON_TRIANGLE  LGBUTTON_TRIANGLE
#define LG_BUTTON_START     LGBUTTON_START
#define LG_BUTTON_SELECT    LGBUTTON_SELECT
#define LG_BUTTON_L1        LGBUTTON_L1
#define LG_BUTTON_R1        LGBUTTON_R1
#define LG_BUTTON_L2        LGBUTTON_L2
#define LG_BUTTON_R2        LGBUTTON_R2
#define LG_BUTTON_L3        LGBUTTON_L3
#define LG_BUTTON_R3        LGBUTTON_R3
#define LG_BUTTON_SHIFTER_FORWARD LGBUTTON_SHIFTER_FORWARD
#define LG_BUTTON_SHIFTER_BACKWARD LGBUTTON_SHIFTER_BACKWARD

#define LG_TYPE_SINE             LGTYPE_SINE
#define LG_TYPE_SQUARE           LGTYPE_SQUARE
#define LG_TYPE_TRIANGLE         LGTYPE_TRIANGLE
#define LG_TYPE_SAWTOOTHUP       LGTYPE_SAWTOOTHUP
#define LG_TYPE_SAWTOOTHDOWN     LGTYPE_SAWTOOTHDOWN

    // define types
    typedef rage::s32 LG_INT;
    typedef rage::u32 LG_UINT;
    typedef rage::s64 LG_LONG;
    typedef float LG_FLOAT;
    typedef bool LG_BOOL;
    typedef void LG_VOID;
    typedef rage::u8 LG_UCHAR;

    ///////////////////////////////////
    // variables that may be changed //
    ///////////////////////////////////
    const LG_INT LG_MAX_CONTROLLERS = 4;
    const LG_INT LG_COLLISION_EFFECT_DURATION = 150; // in milliseconds, for side and frontal collisions
    const LG_INT LG_FRONTAL_COLLISION_PERIOD = 75; // milliseconds
    const LG_INT LG_FRONTAL_COLLISION_FADE_TIME = 20; // milliseconds
    const LG_INT LG_DIRT_ROAD_PERIOD = 65; // milliseconds
    const LG_INT LG_BUMPY_ROAD_PERIOD = 100; // milliseconds    

    //////////////////////////////////////////
    // variables that should not be changed //
    //////////////////////////////////////////
    const LG_INT LG_MIN_PERCENTAGE = -100;
    const LG_INT LG_MAX_PERCENTAGE = 100;
    const LG_INT LG_8_BIT_RANGE_MIN = -255;
    const LG_INT LG_8_BIT_RANGE_MAX = 255;
    const LG_INT LG_NON_LIN_MAX_OUTPUT = 32767; // Max and Min output resulting from the method "GenerateNonLinValues()"
    const LG_INT LG_NON_LIN_MIN_OUTPUT = -32767;
    const LG_UINT LG_UNSIGNED_16_BIT_RANGE_MAX = 65535;
    const LG_INT LG_SIGNED_16_BIT_RANGE_MAX = 32767;
    const LG_INT LG_SIGNED_16_BIT_RANGE_MIN = -32767;
    const LG_INT LG_LOOKUP_TABLE_SIZE = 1024;
}

#endif

#endif // LOGI_GLOBALS_H_INCLUDED_
