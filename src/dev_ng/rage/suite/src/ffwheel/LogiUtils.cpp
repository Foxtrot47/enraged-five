/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#include "LogiUtils.h"

#if HAVE_FFWHEEL

using namespace LogitechEZWheelWrapper;

LG_INT Utils::FromPercentage(const LG_INT percentage, const LG_INT minPercentage, const LG_INT maxPercentage, const LG_INT minOutput, const LG_INT maxOutput)
{
    LG_INT percentage_ = percentage;

    if (percentage < minPercentage)
    {
        LOGITRACE("WARNING: input percentage (%d) smaller than min percentage (%d). Clamping value.\n", percentage, minPercentage);
        percentage_ = minPercentage;
    }

    if (percentage > maxPercentage)
    {
        LOGITRACE("WARNING: input percentage (%d) bigger than max percentage (%d). Clamping value.\n", percentage, maxPercentage);
        percentage_ = maxPercentage;
    }

    LG_FLOAT var1_ = static_cast<LG_FLOAT>(maxOutput - minOutput);
    LG_FLOAT var2_ = static_cast<LG_FLOAT>(percentage_ - minPercentage) / static_cast<LG_FLOAT>(maxPercentage - minPercentage);
    LG_INT output_ = static_cast<LG_INT>(var1_ * var2_) + minOutput;

    return output_;
}

#endif
