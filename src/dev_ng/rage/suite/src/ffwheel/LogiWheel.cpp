/****h* EZ.Wheel.Wrapper/Wheel [6.01]
* NAME
*  EZ Wheel Wrapper
* COPYRIGHT
*  The Logitech EZ Wheel Wrapper, including all accompanying
*  documentation, is protected by intellectual property laws. All
*  rights not expressly granted by Logitech are reserved.
* PURPOSE
*  The EZ Wheel Wrapper is an addition to the Logitech USB Devices
*  Library (force feedback SDK). It enables to dramatically shorten
*  development time and improve implementation quality for the
*  Logitech USB force feedback wheel support.  The wrapper comes with
*  a very intuitive and easy to use interface which enables to read
*  the wheel's axes and buttons, and also to create all the force
*  feedback effects that are necessary for a good and complete
*  implementation. See the following files to get started:
*    - readme.txt: tells you how to get started
*    - SampleInGameImplementation.cpp: shows line by line how to use
*    the EZ Wheel Wrapper's interface to do a complete implementation
*    for Logitech force feedback wheels in your game.
*    - PS3TestApp.cpp: demonstrates force feedback effects. Just
*    compile, and plug in a Logitech USB wheel. See usage at top of
*    PS3TestApp.cpp.
* For more details see the force feedback SDK documentation which is
* part of the Logitech USB Devices Library package.
* AUTHOR
*   Christophe Juncker
* CREATION DATE
*   10/10/2002

* MODIFICATION HISTORY
*   v6.01 - 10/2007
*     - re-wrote wrapper from scratch as a PS3 specific
*     version. Changed all parameters that apply to use percentages
*     instead of absolutes. Added correct player numbering support to
*     go with a Sony firmware update so that at any time the port
*     numbering of the wheels is correct.
*   v5.01 - 10/2006
*     - made PS3 version, sharing much of the code with PS2.
*   v4.01 - 03/2006
*     - added clutch and gated shifter support
*   v3.03 - 09/2005
*     - added definable types
*   v3.02 - 09/2004 (tom)
*     - added softstop
*   v3.01 - 01/2004
*     - added multiturn methods
*     - put all error messages between #ifdef's
*     - added global variables file (LogiGlobals.h)
*   v2.01 - 08/2003
*     - eliminated DPad commands and merged them into button commands
*     - made asynchronous methods more effective in order to optimize
*     processor usage
*     - made filenames more unique to avoid name clashes
*     - made definition names more unique to avoid clashes
*     - added version display during initialization
*     - updated SampleInGameImplementation.cpp
*     - added Optimization_Tricks.txt to show how to best take
*     advantage of asynchronous methods
*     - eliminated autocalibration force method because it should not
*     be used with Driving Force Pro
*     - updated documentation in doc folder
*   v1.2 - 06/2003
*     - enhanced enumeration process to use minimal processing time
*     when no wheels are plugged in
*******
*/

#include "LogiWheel.h"

#if HAVE_FFWHEEL

#include "LogiController.h"
#include <string.h>
#include <time.h>

#include "math/amath.h"

#if __XENON
#define QUANTIZE(param,value) ((value) * int((param)/float(value)))
#else
#define QUANTIZE(param,value) (param)
#endif

using namespace LogitechEZWheelWrapper;

/****f* EZ.Wheel.Wrapper/Wheel()
* NAME
*  Wheel() -- Constructor.
* FUNCTION
*  Does necessary initialization of Logitech library.
******
*/
Wheel::Wheel()
{
#ifdef __SNC__
#pragma diag_suppress 552
#endif
    LG_INT result_ = -1;

    lgDevInitParams initParams_;
    memset(&initParams_, 0, sizeof(initParams_));

    initParams_.MaxDevices = LG_MAX_CONTROLLERS;
    initParams_.MaxEffects = LG_MAX_CONTROLLERS * 16;

    if(LGFAILED(result_ = lgDevInit(&initParams_)))
    {
        LOGITRACE("ERROR: Failed to initialize wheel library. Return code is 0x%08x\n", result_);
    }
}

Wheel::~Wheel()
{
    lgDevDeInit();
}

/****f* EZ.Wheel.Wrapper/ReadAll()
* NAME
*  LG_VOID ReadAll() -- Read all connected Logitech USB wheels.
* FUNCTION
*  Enumerate wheels if necessary.  Read all connected wheels. Assign
*  or update port numbers correctly.
* SEE ALSO
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::ReadAll()
{
    m_controllerManager.Update();
}

/****f* EZ.Wheel.Wrapper/IsConnected(LG_INT.port_no)
* NAME
*  LG_BOOL IsConnected(LG_INT port_no) -- Check if a wheel is
*  connected at the specified port number.
* INPUTS
*  port_no: number of the port of the wheel that we want to check.
* RETURN VALUE
*  true if a Logitech wheel is connected at the specified port
*  number, false otherwise.
* SEE ALSO
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::IsConnected(const LG_INT port_no)
{
    if (port_no < 0 || port_no >= LG_MAX_CONTROLLERS)
        return false;

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL != controller_)
        return true;

    return false;
}

/****f* EZ.Wheel.Wrapper/GetPosition(LG_INT.port_no)
* NAME
*  lgDevPosition* GetPosition(LG_INT port_no) -- Get current
*  positional information.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* RETURN VALUE
*  Pointer to a lgDevPosition structure that contains the info about
*  the wheel's current values for axes, buttons and DPAD.
* SEE ALSO
*  liblgFF.pdf in SDK documentation to see details about the
*  lgDevPosition structure.
*  SampleInGameImplementation.cpp to see an example.
******
*/
lgDevPosition* Wheel::GetPosition(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return NULL;

    return controller_->GetPosition();
}

/****f* EZ.Wheel.Wrapper/ButtonTriggered(LG_INT.port_no,LG_ULONG.buttonMask)
* NAME
*  LG_BOOL ButtonTriggered(LG_INT port_no, LG_ULONG buttonMask) --
*  Check if a button has been triggered.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  buttonMask: the mask of the button that we want to check.
*              Possible masks are:
*              Wheel class A:
*              LG_BUTTON_BUTTON0 through LG_BUTTON_BUTTON15
*              Wheel class B:
*              LG_BUTTON_SQUARE, LG_BUTTON_CROSS, LG_BUTTON_CIRCLE,
*              LG_BUTTON_TRIANGLE, LG_BUTTON_START, LG_BUTTON_SELECT,
*              LG_BUTTON_L1, LG_BUTTON_R1, LG_BUTTON_L2, LG_BUTTON_R2,
*              LG_DPAD_UP, LG_DPAD_DOWN, LG_DPAD_RIGHT, LG_DPAD_LEFT.
* RETURN VALUE
*  true if specified button has been triggered.
* SEE ALSO
*  ButtonReleased(LG_INT.port_no,LG_ULONG.buttonMask)
*  ButtonIsPressed(LG_INT.port_no,LG_ULONG.buttonMask)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::ButtonTriggered(const LG_INT port_no, const LG_ULONG buttonMask)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->ButtonTriggered(buttonMask);
}

/****f* EZ.Wheel.Wrapper/ButtonReleased(LG_INT.port_no,LG_ULONG.buttonMask)
* NAME
*  LG_BOOL ButtonReleased(LG_INT port_no, LG_ULONG buttonMask) --
*  Check if a button has been released.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  buttonMask: the mask of the button that we want to check.
*              Possible masks are:
*              Wheel class A:
*              LG_BUTTON_BUTTON0 through LG_BUTTON_BUTTON15
*              Wheel class B:
*              LG_BUTTON_SQUARE, LG_BUTTON_CROSS, LG_BUTTON_CIRCLE,
*              LG_BUTTON_TRIANGLE, LG_BUTTON_START, LG_BUTTON_SELECT,
*              LG_BUTTON_L1, LG_BUTTON_R1, LG_BUTTON_L2, LG_BUTTON_R2,
*              LG_DPAD_UP, LG_DPAD_DOWN, LG_DPAD_RIGHT, LG_DPAD_LEFT.
* RETURN VALUE
*  true if specified button has been released.
* SEE ALSO
*  ButtonTriggered(LG_INT.port_no,LG_ULONG.buttonMask)
*  ButtonIsPressed(LG_INT.port_no,LG_ULONG.buttonMask)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::ButtonReleased(const LG_INT port_no, const LG_ULONG buttonMask)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->ButtonReleased(buttonMask);
}

/****f* EZ.Wheel.Wrapper/ButtonIsPressed(LG_INT.port_no,LG_ULONG.buttonMask)
* NAME
*  LG_BOOL ButtonIsPressed(LG_INT port_no, LG_ULONG buttonMask) --
*  Check if a button is being pressed.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  buttonMask: the mask of the button that we want to check.
*              Possible masks are:
*              Wheel class A:
*              LG_BUTTON_BUTTON0 through LG_BUTTON_BUTTON15
*              Wheel class B:
*              LG_BUTTON_SQUARE, LG_BUTTON_CROSS, LG_BUTTON_CIRCLE,
*              LG_BUTTON_TRIANGLE, LG_BUTTON_START, LG_BUTTON_SELECT,
*              LG_BUTTON_L1, LG_BUTTON_R1, LG_BUTTON_L2, LG_BUTTON_R2,
*              LG_DPAD_UP, LG_DPAD_DOWN, LG_DPAD_RIGHT, LG_DPAD_LEFT.
* RETURN VALUE
*  true if specified button is being pressed.
* SEE ALSO
*  ButtonReleased(LG_INT.port_no,LG_ULONG.buttonMask)
*  ButtonTriggered(LG_INT.port_no,LG_ULONG.buttonMask)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::ButtonIsPressed(const LG_INT port_no, const LG_ULONG buttonMask)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->ButtonIsPressed(buttonMask);
}

/****f* EZ.Wheel.Wrapper/WheelAMenuIsPressed(LG_INT.port_no,DPadDirection.direction)
* NAME
*  LG_BOOL WheelAMenuIsPressed(LG_INT port_no,DPadDirection direction)
*  -- Check a class A wheel for axes input to mimic menu navigation.
* FUNCTION
*  Since class A wheels don't have a DPAD to navigate menus, pedals
*  and wheel movement can be used instead. Specifically wheel
*  movements will be used to check for left and right direction, and
*  accelerator and brake will be used for up and down direction.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  direction: the mask of the direction of the wheel that we want to
*             check.
*             Possible masks are:
*             LG_MENU_UP, LG_MENU_DOWN, LG_MENU_RIGHT, LG_MENU_LEFT.
* RETURN VALUE
*  true if specified direction is being pressed.
* SEE ALSO
*  WheelAMenuTriggered(LG_INT.port_no,DPadDirection.direction)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::WheelAMenuIsPressed(const LG_INT port_no, const MenuDirection direction)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->WheelAMenuIsPressed(direction);
}

/****f* EZ.Wheel.Wrapper/WheelAMenuTriggered(LG_INT.port_no,DPadDirection.direction)
* NAME
*  LG_BOOL WheelAMenuIsPressed(LG_INT port_no,DPadDirection direction)
*  -- Check a class A wheel for axes input to mimic menu navigation.
* FUNCTION
*  Since class A wheels don't have a DPAD to navigate menus, pedals
*  and wheel movement can be used instead. Specifically wheel
*  movements will be used to check for left and right direction, and
*  accelerator and brake will be used for up and down direction.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  direction: the mask of the direction of the wheel that we want to
*             check.
*             Possible masks are:
*             LG_MENU_UP, LG_MENU_DOWN, LG_MENU_RIGHT, LG_MENU_LEFT.
* RETURN VALUE
*  true if specified direction has been triggered.
* SEE ALSO
*  WheelAMenuIsPressed(LG_INT.port_no,DPadDirection.direction)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::WheelAMenuTriggered(const LG_INT port_no, const MenuDirection direction)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->WheelAMenuTriggered(direction);
}

/****f* EZ.Wheel.Wrapper/IsWheelClassA(LG_INT.port_no)
* NAME
*  LG_BOOL IsWheelClassA(LG_INT port_no) -- Check if wheel is a class
*  A wheel.
* FUNCTION
*  Class A wheels have no DPAD and no playstation specific shape
*  buttons. Instead they only have simple buttons, typically two
*  paddles and 4 or 6 buttons on the front plate.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* RETURN VALUE
*  true if wheel is class A.
* SEE ALSO
*  IsWheelClassB(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::IsWheelClassA(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->IsWheelClassA();
}

/****f* EZ.Wheel.Wrapper/IsWheelClassB(LG_INT.port_no)
* NAME
*  LG_BOOL IsWheelClassB(LG_INT port_no) -- Check if wheel is a class
*  B wheel.
* FUNCTION
*  Class B wheels have a DPAD and playstation specific buttons such as
*  CROSS, CIRCLE, TRIANGLE, SQUARE, Start and Select.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* RETURN VALUE
*  true if wheel is class B.
* SEE ALSO
*  IsWheelClassA(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::IsWheelClassB(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->IsWheelClassB();
}

/****f* EZ.Wheel.Wrapper/IsMultiturnCapable(LG_INT.port_no)
* NAME
*  LG_BOOL IsMultiturnCapable(LG_INT port_no) -- Check if a wheel can
*  do multiturn.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* RETURN VALUE
*  true if wheel can do multiturn, false otherwise.
* SEE ALSO
*  IsMultiturnEnabled(LG_INT.port_no)
*  SetMultiturn(LG_INT.port_no,LG_BOOL.multiturn)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::IsMultiturnCapable(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->HasMultiturn();
}

/****f* EZ.Wheel.Wrapper/IsMultiturnEnabled(LG_INT.port_no)
* NAME
*  LG_BOOL IsMultiturnEnabled(LG_INT port_no) -- Check if a wheel is
*  currently in multiturn mode.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* RETURN VALUE
*  true if wheel is currently in multiturn mode, false otherwise.
* SEE ALSO
*  IsMultiturnCapable(LG_INT.port_no)
*  SetMultiturn(LG_INT.port_no,LG_BOOL.multiturn)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::IsMultiturnEnabled(const LG_INT UNUSED_PARAM(port_no))
{
    return false;
}

/****f* EZ.Wheel.Wrapper/SetMultiturn(LG_INT.port_no,LG_BOOL.multiturn)
* NAME
*  LG_INT SetMultiturn(LG_INT port_no, LG_BOOL multiturn) -- Set wheel
*  into multiturn or single turn mode.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  multiturn: if true, wheel will be set to multiturn mode. If false,
*             it will be set to singleturn mode.
* RETURN VALUE
*  LG_SUCCESS if successful.
* SEE ALSO
*  IsMultiturnCapable(LG_INT.port_no)
*  IsMultiturnEnabled(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_INT Wheel::SetMultiturn(const LG_INT port_no, const LG_BOOL multiturn)
{
    // in addition of setting the multiturn, we also need to keep
    // track at the controllerManager level of which wheel is in
    // multiturn and which is not, so that when re-enumerating because
    // for example one of 2 wheels has been unplugged, we can reset
    // the wheel that was not unplugged to its previous multiturn
    // state.

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return LG_ERROR;

    LG_INT ret_ = LG_ERROR;

    if (true == multiturn)
    {
        if (LGSUCCEEDED(ret_ = controller_->SetMultiturn(LG_TRUE)))
        {
            m_controllerManager.SetMultiturnFlag(port_no, true);
        }
    }
    else if (false == multiturn)
    {
        if (LGSUCCEEDED(ret_ = controller_->SetMultiturn(LG_FALSE)))
        {
            m_controllerManager.SetMultiturnFlag(port_no, false);
        }
    }

    return ret_;
}

/****f* EZ.Wheel.Wrapper/IsClutchPresent(LG_INT.port_no)
* NAME
*  LG_BOOL IsClutchPresent(LG_INT port_no) -- Check if a wheel has a
*  clutch pedal.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* RETURN VALUE
*  true if wheel has a clutch pedal, false otherwise.
* SEE ALSO
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::IsClutchPresent(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->HasClutch();
}

/****f* EZ.Wheel.Wrapper/IsGatedShifterPresent(LG_INT.port_no)
* NAME
*  LG_BOOL IsGatedShifterPresent(LG_INT port_no) -- Check if a wheel has a
*  gated shifter.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* RETURN VALUE
*  true if wheel has a gated shifter, false otherwise.
* SEE ALSO
*  IsGatedShifterSet(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::IsGatedShifterPresent(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->HasGatedShifter();
}

/****f* EZ.Wheel.Wrapper/IsGatedShifterSet(LG_INT.port_no)
* NAME
*  LG_BOOL IsGatedShifterSet(LG_INT port_no) -- Check if a wheel's
*  gated shifter is currently set to gated shifter mode or sequential
*  shifter mode.
* FUNCTION
*  The G25's gated shifter can be set to either use all 7 gears (gated
*  shifter mode) or to be used as a sequential shifter which mimics
*  the function of other wheels such as the Driving Force Pro. This
*  function enables the game to see which mode the shifter is
*  currently set to.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* RETURN VALUE
*  true if wheel's gated shifter is in gated shifter mode.
*  false if wheel's gated shifter is in sequential shifter mode.
* SEE ALSO
*  IsGatedShifterPresent(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::IsGatedShifterSet(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    return controller_->IsGatedShifterSet();
}

/****f* EZ.Wheel.Wrapper/GenerateNonLinValues(LG_INT.port_no,LG_INT.nonLinCoeff)
* NAME
*  LG_VOID GenerateNonLinValues(LG_INT port_no, LG_INT nonLinCoeff) --
*  Generate non-linear values for the wheel's axis.
* FUNCTION
*  This method is particularly aimed at gaming wheels that are not
*  multiturn capable. The game should check if the connected wheel is
*  a multiturn capable wheel or not. If the wheel is multiturn capable
*  and is set to multiturn mode then the GenerateNonLinValues method
*  should probably not be used. For all other cases the following
*  applies.
*  Gaming wheels have very different behavior from real steering
*  wheels. The reason is that they only do up to 3 quarters of a turn
*  lock to lock, compared to about 3 turns for a real car. This
*  directly affects the steering ratio (15:1 to 20:1 for a real car,
*  but only 4:1 for a gaming wheel!).  Because of this very short
*  steering ratio, the gaming wheel will feel highly sensitive which
*  may make game play very difficult. Especially it may be difficult
*  to drive in a straight line at speed (tendency to swerve back and
*  forth).  One way to get around this problem is to use a sensitivity
*  curve.  This is a curve that defines the sensitivity of the wheel
*  depending on speed. This type of curve is usually used for game
*  pads to make up for their low physical range. The result of
*  applying such a curve is that at high speed the car's wheels will
*  physically turn less than if the car is moving very slowly. For
*  example the car's wheels may turn 60 degrees lock to lock at low
*  speed but only 10 degrees lock to lock at higher speeds. If you
*  calculate the resulting steering ratio for 10 degrees lock to lock
*  you find that if you use a steering wheel that turns 180 degrees
*  lock to lock the ratio is equal to 180/10 = 18, which corresponds
*  to a real car's steering ratio.
*  If the sensitivity curve has been implemented for
*  the wheel, adding a non-linear curve probably is not necessary. But
*  you may find that even after applying a sensitivity curve, the car
*  still feels a little twitchy on a straight line when driving
*  fast. This may be because in your game you need more than 10
*  degrees lock to lock even at high speeds. Or maybe the car is
*  moving at very high speeds where even a normal steering ratio is
*  not good enough to eliminate high sensitivity.
*  The best way at this point is to add a non-linear curve on top of
*  the sensitivity curve.  The effect of the non-linear curve is that
*  around center position the wheel will be less sensitive. Yet at
*  locked position left or right the car's wheels will turn the same
*  amount of degrees as without the non-linear response
*  curve. Therefore the car will become more controllable on a
*  straight line and game-play will be improved.
*  A non-linear curve can also be used on its own, without the speed
*  sensitive adjustments.
*  This method lets you define a non-linearity coefficient which will
*  determine how strongly non-linear the curve will be. When running
*  the method it will generate a mapping table in the form of an
*  array. For each of the 1024 entries in this array there will be a
*  corresponding non-linear value which can be used as the wheel's
*  axis position instead of the raw SDK value.

* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  nonLinCoeff: value representing how much non-linearity should be
*               applied. Range is -100 to 100. 0 = linear curve. 100 =
*               maximum non-linear curve making wheel less responsive
*               around center. -100 = maximum non-linear curve making
*               wheel more sensitive around center.
* SEE ALSO
*  GetNonLinearValue(LG_INT.port_no,LG_INT.inputValue)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::GenerateNonLinValues(const LG_INT port_no, const LG_INT nonLinCoeff)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL != controller_)
        controller_->GenerateNonLinValues(nonLinCoeff);
}

/****f* EZ.Wheel.Wrapper/GetNonLinearValue(LG_INT.port_no,LG_INT.inputValue)
* NAME
*  LG_INT GetNonLinearValue(LG_INT port_no, LG_INT inputValue) -- Get
*  non-linear value.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  inputValue: current wheel axis value (between -32767 and 32767).
* RETURN VALUE
*  Corresponding non-linear value (between -32767 and 32767) based on
*  nonLinCoeff set when calling GenerateNonLinValues().
* SEE ALSO
*  GenerateNonLinValues(LG_INT.port_no,LG_INT.nonLinCoeff)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_INT Wheel::GetNonLinearValue(const LG_INT port_no, const LG_INT inputValue)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return inputValue;

    return controller_->GetNonLinearValue(inputValue);
}

/****f* EZ.Wheel.Wrapper/IsPlaying(LG_INT.port_no,ForceType.forceType)
* NAME
*  LG_BOOL IsPlaying(LG_INT port_no, ForceType forceType) -- Check if
*  specified force is currently playing.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  forceType: force that we want to check. Possible forces are:
*             LG_FORCE_SPRING, LG_FORCE_CONSTANT, LG_FORCE_DAMPER,
*             LG_FORCE_SIDE_COLLISION, LG_FORCE_FRONTAL_COLLISION,
*             LG_FORCE_DIRT_ROAD, LG_FORCE_BUMPY_ROAD,
*             LG_FORCE_SLIPPERY_ROAD, LG_FORCE_SURFACE_EFFECT,
*             LG_FORCE_SOFTSTOP, LG_FORCE_CAR_AIRBORNE.
* RETURN VALUE
*  true if force is currently playing, false otherwise.
* SEE ALSO
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_BOOL Wheel::IsPlaying(const LG_INT port_no, const ForceType forceType)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return false;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return false;

    return forceManager_->IsPlaying(forceType);
}

/****f* EZ.Wheel.Wrapper/PlaySpringForce(LG_INT.port_no,LG_INT.offsetPercentage,LG_INT.saturationPercentage,LG_INT.coefficientPercentage)
* NAME
*  LG_VOID PlaySpringForce(LG_INT port_no, LG_INT offsetPercentage,
*  LG_INT saturationPercentage, LG_INT coefficientPercentage) -- Play
*  spring force.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  offsetPercentage: specifies how much offset there is between spring
*                    center force and wheel center position. Valid
*                    range is -100 to 100.
*  saturationPercentage: saturation of the spring. Valid range is 0 to
*                        100.
*  coefficientPercentage: coefficient of the spring. Valid range is
*                         -100 to 100.
* SEE ALSO
*  StopSpringForce(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlaySpringForce(const LG_INT port_no, const LG_INT offsetPercentage, const LG_INT saturationPercentage, const LG_INT coefficientPercentage)
{
	LG_INT saturationPercentage_ = QUANTIZE(saturationPercentage,5);
	LG_INT coefficientPercentage_ = QUANTIZE(coefficientPercentage,5);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->PlaySpringForce(offsetPercentage, saturationPercentage_, coefficientPercentage_);
}

/****f* EZ.Wheel.Wrapper/StopSpringForce(LG_INT.port_no)
* NAME
*  LG_VOID StopSpringForce(LG_INT port_no) -- Stop spring force.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  PlaySpringForce(LG_INT.port_no,LG_INT.offsetPercentage,LG_INT.saturationPercentage,LG_INT.coefficientPercentage)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::StopSpringForce(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->StopSpringForce();
}

/****f* EZ.Wheel.Wrapper/PlayConstantForce(LG_INT.port_no,LG_INT.strengthPercentage)
* NAME
*  LG_VOID PlayConstantForce(LG_INT port_no, LG_INT
*  strengthPercentage) -- Play constant force.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  strengthPercentage: valid range is -100 to 100. A negative strength
*                      reverses the direction.
* SEE ALSO
*  StopConstantForce(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlayConstantForce(const LG_INT port_no, const LG_INT strengthPercentage)
{
	LG_INT strengthPercentage_ = QUANTIZE(strengthPercentage,20);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->PlayConstantForce(strengthPercentage_);
}

/****f* EZ.Wheel.Wrapper/StopConstantForce(LG_INT.port_no)
* NAME
*  LG_VOID StopConstantForce(LG_INT port_no) -- Stop constant force.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  PlayConstantForce(LG_INT.port_no,LG_INT.strengthPercentage)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::StopConstantForce(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->StopConstantForce();
}

/****f* EZ.Wheel.Wrapper/PlayDamperForce(LG_INT.port_no,LG_INT.strengthPercentage)
* NAME
*  LG_VOID PlayDamperForce(LG_INT port_no, LG_INT strengthPercentage)
*  -- Play damper force.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  strengthPercentage: valid range is -100 to 100. A negative strength
*                      creates a negative damper which makes the wheel
*                      feel "slippery".
* SEE ALSO
*  StopDamperForce(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlayDamperForce(const LG_INT port_no, const LG_INT strengthPercentage)
{
	LG_INT strengthPercentage_ = QUANTIZE(strengthPercentage,5);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->PlayDamperForce(strengthPercentage_);
}

/****f* EZ.Wheel.Wrapper/StopDamperForce(LG_INT.port_no)
* NAME
*  LG_VOID StopDamperForce(LG_INT port_no) -- Stop damper force.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  PlayDamperForce(LG_INT.port_no,LG_INT.strengthPercentage)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::StopDamperForce(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->StopDamperForce();
}

/****f* EZ.Wheel.Wrapper/PlaySideCollisionForce(LG_INT.port_no,LG_INT.strengthPercentage)
* NAME
*  LG_VOID PlaySideCollisionForce(LG_INT port_no, LG_INT
*  strengthPercentage) -- Play side collision force.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  strengthPercentage: valid range is -100 to 100. A negative strength
*                      reverses the direction.
* SEE ALSO
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlaySideCollisionForce(const LG_INT port_no, const LG_INT strengthPercentage)
{
	LG_INT strengthPercentage_ = QUANTIZE(strengthPercentage,5);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

#if __PS3
	static long timeAtSideCollision_ = 0;
	static LG_INT combinedStrengthPercentage_ = 0;

	long currentTime_ = clock();

	if (currentTime_ - timeAtSideCollision_ > LG_COLLISION_EFFECT_DURATION * 1000)
	{
		combinedStrengthPercentage_ = strengthPercentage_;
		timeAtSideCollision_ = currentTime_;
	}
	else
	{
		combinedStrengthPercentage_ = rage::Clamp(combinedStrengthPercentage_ + strengthPercentage_, 0, 100);
	}

	forceManager_->PlaySideCollisionForce(combinedStrengthPercentage_);
#else
    forceManager_->PlaySideCollisionForce(strengthPercentage_);
#endif
}

/****f* EZ.Wheel.Wrapper/PlayFrontalCollisionForce(LG_INT.port_no,LG_INT.strengthPercentage)
* NAME
*  LG_VOID PlayFrontalCollisionForce(LG_INT port_no, LG_INT
*  strengthPercentage) -- Play frontal collision force.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  strengthPercentage: valid range is 0 to 100.
* SEE ALSO
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlayFrontalCollisionForce(const LG_INT port_no, const LG_INT strengthPercentage)
{
	LG_INT strengthPercentage_ = QUANTIZE(strengthPercentage,5);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

#if __PS3
	static long timeAtFrontalCollision_ = 0;
	static LG_INT combinedStrengthPercentage_ = 0;

	long currentTime_ = clock();

	if (currentTime_ - timeAtFrontalCollision_ > LG_COLLISION_EFFECT_DURATION * 1000)
	{
		combinedStrengthPercentage_ = strengthPercentage_;
		timeAtFrontalCollision_ = currentTime_;
	}
	else
	{
		combinedStrengthPercentage_ = rage::Clamp(combinedStrengthPercentage_ + strengthPercentage_, 0, 100);
	}

	forceManager_->PlayFrontalCollisionForce(combinedStrengthPercentage_);
#else
    forceManager_->PlayFrontalCollisionForce(strengthPercentage_);
#endif
}

/****f* EZ.Wheel.Wrapper/PlayDirtRoadEffect(LG_INT.port_no,LG_INT.strengthPercentage)
* NAME
*  LG_VOID PlayDirtRoadEffect(LG_INT port_no, LG_INT
*  strengthPercentage) -- Play dirt road effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  strengthPercentage: valid range is 0 to 100.
* SEE ALSO
*  StopDirtRoadEffect(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlayDirtRoadEffect(const LG_INT port_no, const LG_INT strengthPercentage)
{
	LG_INT strengthPercentage_ = QUANTIZE(strengthPercentage,5);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->PlayDirtRoadEffect(strengthPercentage_);
}

/****f* EZ.Wheel.Wrapper/StopDirtRoadEffect(LG_INT.port_no)
* NAME
*  LG_VOID StopDirtRoadEffect(LG_INT port_no) -- Stop dirt road
*  effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  PlayDirtRoadEffect(LG_INT.port_no,LG_INT.strengthPercentage)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::StopDirtRoadEffect(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->StopDirtRoadEffect();
}

/****f* EZ.Wheel.Wrapper/PlayBumpyRoadEffect(LG_INT.port_no,LG_INT.strengthPercentage)
* NAME
*  LG_VOID PlayBumpyRoadEffect(LG_INT port_no, LG_INT
*  strengthPercentage) -- Play bumpy road effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  strengthPercentage: valid range is 0 to 100.
* SEE ALSO
*  StopBumpyRoadEffect(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlayBumpyRoadEffect(const LG_INT port_no, const LG_INT strengthPercentage)
{
	LG_INT strengthPercentage_ = QUANTIZE(strengthPercentage,5);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->PlayBumpyRoadEffect(strengthPercentage_);
}

/****f* EZ.Wheel.Wrapper/StopBumpyRoadEffect(LG_INT.port_no)
* NAME
*  LG_VOID StopBumpyRoadEffect(LG_INT port_no) -- Stop bumpy road
*  effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  PlayBumpyRoadEffect(LG_INT.port_no,LG_INT.strengthPercentage)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::StopBumpyRoadEffect(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->StopBumpyRoadEffect();
}

/****f* EZ.Wheel.Wrapper/PlaySlipperyRoadEffect(LG_INT.port_no,LG_INT.strengthPercentage)
* NAME
*  LG_VOID PlaySlipperyRoadEffect(LG_INT port_no, LG_INT
*  strengthPercentage) -- Play slippery road effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  strengthPercentage: valid range is 0 to 100. The higher the number,
*                      the more slippery the force will feel.
* SEE ALSO
*  StopSlipperyRoadEffect(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlaySlipperyRoadEffect(const LG_INT port_no, const LG_INT strengthPercentage)
{
	LG_INT strengthPercentage_ = QUANTIZE(strengthPercentage,5);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->PlaySlipperyRoadEffect(strengthPercentage_);
}

/****f* EZ.Wheel.Wrapper/StopSlipperyRoadEffect(LG_INT.port_no)
* NAME
*  LG_VOID StopSlipperyRoadEffect(LG_INT port_no) -- Stop slippery
*  road effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  PlaySlipperyRoadEffect(LG_INT.port_no,LG_INT.strengthPercentage)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::StopSlipperyRoadEffect(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->StopSlipperyRoadEffect();
}

/****f* EZ.Wheel.Wrapper/PlaySurfaceEffect(LG_INT.port_no,LG_INT.type,LG_INT.strengthPercentage,LG_INT.period)
* NAME
*  LG_VOID PlaySurfaceEffect(LG_INT port_no, LG_INT type, LG_INT
*  strengthPercentage, LG_INT period) -- Play surface effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  type: possible types are:
*        LG_TYPE_SINE, LG_TYPE_SQUARE, LG_TYPE_TRIANGLE,
*        LG_TYPE_SAWTOOTHUP, LG_TYPE_SAWTOOTHDOWN
*  strengthPercentage: valid range is 0 to 100.
*  period: duration for one full cycle of the periodic function
*          measured in milliseconds. A good range of values for the
*          period is 20 ms (sand) to 120 ms (wooden bridge or
*          cobblestones).
* SEE ALSO
*  StopSurfaceEffect(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlaySurfaceEffect(const LG_INT port_no, const LG_INT type, const LG_INT strengthPercentage, const LG_INT period)
{
	LG_INT strengthPercentage_ = QUANTIZE(strengthPercentage,5);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->PlaySurfaceEffect(type, strengthPercentage_, period);
}

/****f* EZ.Wheel.Wrapper/StopSurfaceEffect(LG_INT.port_no)
* NAME
*  LG_VOID StopSurfaceEffect(LG_INT port_no) -- Stop surface effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  PlaySurfaceEffect(LG_INT.port_no,LG_INT.type,LG_INT.strengthPercentage,LG_INT.period)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::StopSurfaceEffect(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->StopSurfaceEffect();
}

/****f* EZ.Wheel.Wrapper/PlayCarAirborne(LG_INT.port_no)
* NAME
*  LG_VOID PlayCarAirborne(LG_INT port_no) -- Play car airborne
*  effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  StopCarAirborne(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlayCarAirborne(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->PlayCarAirborne();
}

/****f* EZ.Wheel.Wrapper/StopCarAirborne(LG_INT.port_no)
* NAME
*  LG_VOID StopCarAirborne(LG_INT port_no) -- Stop car airborne
*  effect.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  PlayCarAirborne(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::StopCarAirborne(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->StopCarAirborne();
}

/****f* EZ.Wheel.Wrapper/PlaySoftstopForce(LG_INT.port_no,LG_INT.deadbandPercentage)
* NAME
*  LG_VOID PlaySoftstopForce(LG_INT port_no, LG_INT
*  deadbandPercentage) -- Play soft stop force.
* FUNCTION
*  Used to emulate the ends of a wheel's usable range.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  deadbandPercentage: defines how large the force deadband should be,
*                      which corresponds to the range of the steering
*                      wheel. For example if a multiturn wheel is
*                      connected but game only uses half of its range,
*                      game can play a soft stop force with a deadband
*                      of 50%.
* SEE ALSO
*  StopSoftstopForce(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::PlaySoftstopForce(const LG_INT port_no, const LG_INT deadbandPercentage)
{
	LG_INT deadbandPercentage_ = QUANTIZE(deadbandPercentage,5);

    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->PlaySoftstopForce(deadbandPercentage_);
}

/****f* EZ.Wheel.Wrapper/StopSoftstopForce(LG_INT.port_no)
* NAME
*  LG_VOID StopSoftstopForce(LG_INT port_no) -- Stop soft stop force.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* SEE ALSO
*  PlaySoftstopForce(LG_INT.port_no,LG_INT.deadbandPercentage)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_VOID Wheel::StopSoftstopForce(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return;

    ForceManager* forceManager_ = controller_->GetForceManager();

    if (NULL == forceManager_)
        return;

    forceManager_->StopSoftstopForce();
}

/****f* EZ.Wheel.Wrapper/SetOverallForceGain(LG_INT.port_no,LG_INT.strengthPercentage)
* NAME
*  LG_INT SetOverallForceGain(LG_INT port_no, LG_INT
*  strengthPercentage) -- Set overall force gain..
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
*  strengthPercentage: valid range is 0 to 100.
* RETURN VALUE
*  LG_SUCCESS if successful.
* SEE ALSO
*  GetOverallForceGainPercentage(LG_INT.port_no)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_INT Wheel::SetOverallForceGain(const LG_INT port_no, const LG_INT strengthPercentage)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return LG_ERROR;

    return controller_->SetOverallForceGain(strengthPercentage);
}

/****f* EZ.Wheel.Wrapper/GetOverallForceGainPercentage(LG_INT.port_no)
* NAME
*  LG_VOID GetOverallForceGainPercentage(LG_INT port_no, LG_INT
*  strengthPercentage) -- Get overall force gain.
* INPUTS
*  port_no: number of the port to which the concerned wheel is
*           connected.
* RETURN VALUE
*  Overall force gain percentage (between 0 and 100).
* SEE ALSO
*  SetOverallForceGain(LG_INT.port_no,LG_INT.strengthPercentage)
*  SampleInGameImplementation.cpp to see an example.
******
*/
LG_INT Wheel::GetOverallForceGainPercentage(const LG_INT port_no)
{
    Controller* controller_ = m_controllerManager.GetController(port_no);

    if (NULL == controller_)
        return LG_MAX_PERCENTAGE;

    return controller_->GetOverallForceGainPercentage();
}

#endif
