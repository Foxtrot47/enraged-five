/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#ifndef LOGI_CONTROLLER_H_INCLUDED_
#define LOGI_CONTROLLER_H_INCLUDED_

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiGlobals.h"
#include "LogiCommon.h"
#include "LogiForceManager.h"

namespace LogitechEZWheelWrapper
{
    // conversion table between DPAD number(0 - 7) and bits corresponding to which
    // of the four buttons are being pressed
    // Left bit corresponds to button up, 2nd from the left to button right,
    // 3rd from the left to button down, and the last bit to button left
    const LG_INT DPAD_POSITION[8][4] =
    {{1, 0, 0, 0},
    {1, 1, 0, 0},
    {0, 1, 0, 0},
    {0, 1, 1, 0},
    {0, 0, 1, 0},
    {0, 0, 1, 1},
    {0, 0, 0, 1},
    {1, 0, 0, 1}};

    typedef enum
    {
        LG_LOCAL_DPAD_UP, LG_LOCAL_DPAD_RIGHT, LG_LOCAL_DPAD_DOWN, LG_LOCAL_DPAD_LEFT
    } DPadDirection;

    typedef enum
    {
        LG_MENU_UP, LG_MENU_DOWN, LG_MENU_RIGHT, LG_MENU_LEFT
    } MenuDirection;

    class Controller
    {
    public:
        Controller();
        ~Controller();

        LG_INT Read();
        lgDevPosition* GetPosition();

        LG_BOOL ButtonTriggered(const LG_ULONG buttonMask);
        LG_BOOL ButtonReleased(const LG_ULONG buttonMask);
        LG_BOOL ButtonIsPressed(const LG_ULONG buttonMask);
        LG_BOOL WheelAMenuIsPressed(const MenuDirection direction);
        LG_BOOL WheelAMenuTriggered(const MenuDirection direction);
        LG_BOOL IsWheelClassA();
        LG_BOOL IsWheelClassB();

        LG_VOID GenerateNonLinValues(const LG_INT nonLinCoeff);
        LG_INT GetNonLinearValue(const LG_INT inputValue);

        LG_INT SetOverallForceGain(const LG_INT strengthPercentage);
        LG_INT GetOverallForceGainPercentage();

        LG_INT IsMultiturnEnabled();
        LG_INT SetMultiturn(const LG_INT value);
        LG_BOOL IsGatedShifterSet();

        LG_INT GetHandle() { return m_handle; }
        LG_VOID SetHandle(const LG_INT handle);

        LG_INT GetClass() { return m_class; }
        LG_VOID SetClass(const LG_INT deviceClass) {m_class = deviceClass; }

        LG_BOOL HasMultiturn() { return m_hasMultiturn; }
        LG_VOID SetMultiturnFlag(const LG_BOOL hasMultiturn) { m_hasMultiturn = hasMultiturn; }

        LG_BOOL HasGatedShifter() { return m_hasGatedShifter; }
        LG_VOID SetGatedShifterFlag(const LG_BOOL hasGatedShifter) { m_hasGatedShifter = hasGatedShifter; }

        LG_BOOL HasClutch() { return m_hasClutch; }
        LG_VOID SetClutchFlag(const LG_BOOL hasClutch) { m_hasClutch = hasClutch; }

        LG_INT GetPortNbr() { return m_portNbr; }
        LG_VOID SetPortNbr(const LG_INT portNbr) { m_portNbr = portNbr; }

        ForceManager* GetForceManager() { return &m_forceManager; }

    private:
        LG_INT m_handle;
        LG_INT m_class;
        LG_BOOL m_hasMultiturn;
        LG_INT m_isMultiturnEnabled;
        LG_BOOL m_hasGatedShifter;
        LG_BOOL m_hasClutch;
        LG_INT m_portNbr;
        LG_INT m_overallForceGainPercentage;
        LG_INT m_nonLinearWheel[LG_LOOKUP_TABLE_SIZE];
        LG_INT m_nonLinearCoefficient;

        lgDevPosition m_position;
        lgDevPosition m_positionLast;

        ForceManager m_forceManager;

        LG_VOID Init();

        LG_BOOL DPadIsPressed(const LG_INT direction);
        LG_BOOL DPadTriggered(const LG_INT direction);
        LG_BOOL DPadReleased(const LG_INT direction);

        LG_FLOAT CalculateNonLinValue(const LG_INT tableIndex, const LG_INT nonLinearCoeff, const LG_INT minOutput, const LG_INT maxOutput);
    };
}

#endif

#endif // LOGI_CONTROLLER_H_INCLUDED_
