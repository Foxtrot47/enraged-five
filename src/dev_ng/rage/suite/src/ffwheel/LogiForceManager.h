/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#ifndef LOGI_FORCE_MANAGER_H_INCLUDED_
#define LOGI_FORCE_MANAGER_H_INCLUDED_

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiForce.h"
#include <vector>

namespace LogitechEZWheelWrapper
{
    class ForceManager
    {
    public:
        ForceManager();
        ~ForceManager();

        LG_BOOL IsPlaying(const ForceType forceType);
        LG_VOID PlaySpringForce(const LG_INT offsetPercentage, const LG_INT saturationPercentage, const LG_INT coefficientPercentage);
        LG_VOID StopSpringForce();
        LG_VOID PlayConstantForce(const LG_INT strengthPercentage);
        LG_VOID StopConstantForce();
        LG_VOID PlayDamperForce(const LG_INT strengthPercentage);
        LG_VOID StopDamperForce();
        LG_VOID PlaySideCollisionForce(const LG_INT strengthPercentage);
        LG_VOID PlayFrontalCollisionForce(const LG_INT strengthPercentage);
        LG_VOID PlayDirtRoadEffect(const LG_INT strengthPercentage);
        LG_VOID StopDirtRoadEffect();
        LG_VOID PlayBumpyRoadEffect(const LG_INT strengthPercentage);
        LG_VOID StopBumpyRoadEffect();
        LG_VOID PlaySlipperyRoadEffect(const LG_INT strengthPercentage);
        LG_VOID StopSlipperyRoadEffect();
        LG_VOID PlaySurfaceEffect(const LG_INT type, const LG_INT strengthPercentage, const LG_UINT period);
        LG_VOID StopSurfaceEffect();
        LG_VOID PlayCarAirborne();
        LG_VOID StopCarAirborne();
        LG_VOID PlaySoftstopForce(const LG_INT deadbandPercentage);
        LG_VOID StopSoftstopForce();

        LG_VOID SetWheelHandle(const LG_INT handle) { m_wheelHandle = handle; }

    private:
        LG_INT m_wheelHandle;
        std::vector<Force> m_force;
        LG_BOOL m_isAirborne;

        LG_VOID Init();
        LG_BOOL ForceHasChanged(const ForceType forceType, const lgDevForceEffect* newForceEffect);
        LG_VOID PlayEffect(const ForceType forceType, lgDevForceEffect* newForceEffect, const LG_BOOL isCollisionForce = false);
    };
}

#endif

#endif // LOGI_FORCE_MANAGER_H_INCLUDED_
