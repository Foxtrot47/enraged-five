/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#include "LogiForce.h"

#if HAVE_FFWHEEL

using namespace LogitechEZWheelWrapper;

Force::Force()
{
    Init();
}

Force::~Force()
{

}

LG_VOID Force::Init()
{
    m_effectID = LG_INVALID_EFFECTID;
    m_isPlaying = false;
    m_forceType = LG_FORCE_NONE;
    m_wasPlaying = false;
    m_previousEffectType = 0xff;
}

LG_INT Force::Download(const LG_INT device, lgDevForceEffect& forceEffect)
{
    if (LG_INVALID_DEVICE == device)
    {
        LOGITRACE("ERROR: trying to download force effect buf device handle is NULL\n");
        return LG_ERROR;
    }

    if (m_effectID != LG_INVALID_EFFECTID)
    {
        // Destroy force effect or otherwise a different effect ID will be assigned which is bad
        LOGITRACE("WARNING: Can't assign new effectID over old one. Destroying old force.\n");
        Destroy();
    }

    LG_INT ret_ = lgDevDownloadForceEffect(device, &m_effectID, &forceEffect);
    if(LGFAILED(ret_))
    {
        LOGITRACE("ERROR: lgDevDownloadForceEffect returned %d\n", ret_);
        m_effectID = LG_INVALID_EFFECTID;
    }
    else
    {
        m_forceEffect = forceEffect;
    }

    return ret_;
}

LG_INT Force::Update(lgDevForceEffect& forceEffect)
{
    if (m_effectID == LG_INVALID_EFFECTID)
    {
        // Destroy force effect or otherwise a different effect ID will be assigned which is bad
        LOGITRACE("WARNING: trying to update force but effect ID is invalid\n");
        return LG_ERROR;
    }

    LG_INT ret_ = lgDevUpdateForceEffect(m_effectID, &forceEffect);
    if(LGFAILED(ret_))
    {
        LOGITRACE("ERROR: lgDevUpdateForceEffect returned %d\n", ret_);
        m_effectID = LG_INVALID_EFFECTID;
		m_isPlaying = false;
    }
    else
    {
        m_forceEffect = forceEffect;
    }

    return ret_;
}

LG_INT Force::Start()
{
    LG_INT ret_ = LG_SUCCESS;

    if (LG_INVALID_EFFECTID == m_effectID)
    {
        LOGITRACE("ERROR: trying to start force but effect ID is invalid\n");
        return LG_ERROR;
    }

    if (LGFAILED(ret_ = lgDevStartForceEffect(m_effectID)))
    {
        LOGITRACE("ERROR: failed to start force\n");
		m_effectID = LG_INVALID_EFFECTID;
		m_isPlaying = false;
    }
    else
    {
        m_isPlaying = true;
    }

    return ret_;
}

LG_INT Force::Stop()
{
    LG_INT ret_ = LG_SUCCESS;

    if (LG_INVALID_EFFECTID == m_effectID)
    {
        LOGITRACE("ERROR: trying to stop force but effect ID is NULL\n");
        return LG_ERROR;
    }

    if (LGFAILED(ret_ = lgDevStopForceEffect(m_effectID)))
    {
        LOGITRACE("ERROR: failed to stop force\n");
    }
    else
    {
        m_isPlaying = false;
    }

    return ret_;
}

LG_INT Force::Destroy()
{
    LG_INT ret_ = LG_SUCCESS;

    if (LG_INVALID_EFFECTID == m_effectID)
    {
        LOGITRACE("ERROR: trying to destroy force but effect ID is invalid\n");
        return LG_ERROR;
    }

    if (LGFAILED(ret_ = lgDevDestroyForceEffect(m_effectID)))
    {
        LOGITRACE("ERROR: failed to destroy force\n");
    }
    else
    {
        m_isPlaying = false;
        m_effectID = LG_INVALID_EFFECTID;
    }

    return ret_;
}

#endif
