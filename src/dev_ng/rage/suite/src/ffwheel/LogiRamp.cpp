/*
The Logitech EZ Wheel Wrapper, including all acompanying documentation, 
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiRamp.h"
#include <string.h>

LogiRamp::LogiRamp()
{
}

LogiRamp::~LogiRamp()
{
}

int LogiRamp::DownloadForce(int channel, int forceNumber, int &handle, int duration, int startDelay, int magnitudeStart,
                            int magnitudeEnd, int direction)
{
    lgDevForceEffect force;
    int ret = LG_SUCCESS;

    if (EffectID[channel][forceNumber] != LG_INVALID_EFFECTID)
    {
        // Destroy force effect or otherwise a different effect ID will be assigned which is bad
#ifdef _DEBUG
        printf("WARNING: Can't assign new effectID over old one (channel %d, force number %d). Destroying old force.\n", channel, forceNumber);
#endif
        Destroy(channel, forceNumber);
    }

    if (handle != LG_INVALID_DEVICE)
    {
        memset(&force, 0, sizeof(lgDevForceEffect));

        force.Type = LGTYPE_RAMP;
        force.Duration = duration;
        force.StartDelay = startDelay;
        force.p.ramp.MagnitudeStart = magnitudeStart;
        force.p.ramp.MagnitudeEnd = magnitudeEnd;
        force.p.ramp.Direction = direction;

#ifdef _DEBUG
        printf("Downloading ramp force effect on channel %d\n", channel);
#endif
        ret = lgDevDownloadForceEffect(handle, &EffectID[channel][forceNumber], &force);
        if(LGFAILED(ret))
        {
#ifdef _DEBUG_BASIC
            printf("ERROR: DownloadForce(ramp force) on channel %d returned %d\n", channel, ret);
#endif
            EffectID[channel][forceNumber] = LG_INVALID_EFFECTID;
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: Trying to download a ramp force to channel %d but wheel has not been opened.\n", channel);
#endif
    }
    return ret;
}

int LogiRamp::UpdateForce(int channel, int forceNumber, int duration, int startDelay, int magnitudeStart, int magnitudeEnd, int direction)
{
    lgDevForceEffect force;
    int ret = LG_SUCCESS;

    memset(&force, 0, sizeof(lgDevForceEffect));

    force.Type = LGTYPE_RAMP;
    force.Duration = duration;
    force.StartDelay = startDelay;
    force.p.ramp.MagnitudeStart = magnitudeStart;
    force.p.ramp.MagnitudeEnd = magnitudeEnd;
    force.p.ramp.Direction = direction;

#ifdef _DEBUG
    printf("Updating ramp force effect on channel %d\n", channel);
#endif

    //mjc lgDevASync(LGASYNC_MODE_WAIT, &ret);
    lgDevUpdateForceEffect(EffectID[channel][forceNumber], &force);
    //lgDevASync(LGASYNC_MODE_WAIT, &ret);

    if(LGFAILED(ret))
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: UpdateForce(ramp force) on channel %d returned %d\n", channel, ret);
#endif
        EffectID[channel][forceNumber] = LG_INVALID_EFFECTID;
    }

    return ret;
}

#endif // HAVE_FFWHEEL
