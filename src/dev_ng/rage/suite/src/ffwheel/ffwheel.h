#ifndef INPUT_FFWHEEL_H
#define INPUT_FFWHEEL_H

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiWheel.h"
#include "liblgff.h"

#include "diag/output.h"
#include "bank/bank.h"
#include "math/amath.h"

namespace rage {

// One to one correspondence between wheels and enumerated channels
#define IO_FF_MAX_WHEELS	LogitechEZWheelWrapper::LG_MAX_CONTROLLERS

class ioFFButtonState{
public:
	ioFFButtonState(){IsDown=false,Pressed=false,Released=false;}
	bool IsDown;
	bool Pressed;	//Up Last Frame Down this frame
	bool Released;	//Down last frame Up this frame
};

class ioFFWheel
{
public:

	enum eFFEffects
	{
		kSpring=0,
		kJerkCW,
		kJerkCCW,
		kConstF,
		kNumEffects,
	};

	enum eButtons
	{
		kCross,
		kSquare,
		kCircle,
		kTriangle,
		kStart,
		kSelect,
		kL1,
		kR1,
		kL2,
		kR2,
		kL3,
		kR3,
		kDPadUp,
		kDPadDown,
		kDPadRight,
		kDPadLeft,
		kNumButtonValues,
		// Aliases
		kRDown=kCross,
		kRLeft=kSquare,
		kRRight=kCircle,
		kRUp=kTriangle,
		kLDown=kDPadDown,
		kLLeft=kDPadLeft,
		kLRight=kDPadRight,
		kLUp=kDPadUp,
	};

	ioFFWheel();
	~ioFFWheel(){};

	static void LoadDrivers();
	static void UnloadDrivers();
	static void InitClass();
	static void ShutdownClass();
	static void StopAllEffectsForAllWheels();
	static void ResetAll();
	static void UpdateAll();
	static void SetState();
	static void SetThreadCPU(int threadCPU);
	
	static ioFFWheel s_FFWheels[IO_FF_MAX_WHEELS];
#if __DEV
	static ioFFWheel& GetFFWheel(int i) {if((i<0)||(i>=IO_FF_MAX_WHEELS)){AssertMsg(0,"Get FFwheel passed an invalid index!"); return s_FFWheels[0];} return s_FFWheels[i];}
#else
	static ioFFWheel& GetFFWheel(int i) { return s_FFWheels[i]; }
#endif
	static int FindFFWheelIndexFromPadIndex(int i);
	static LogitechEZWheelWrapper::Wheel* GetLGWheels() { return s_LGWheels; }
	static LogitechEZWheelWrapper::Wheel* s_LGWheels;
	
	static rage::u64 s_LGButtonDefines[kNumButtonValues];
	static rage::u64 s_DSButtonDefines[kNumButtonValues];	// Dual Shock buttons
	
	static int s_NumDevices;
	static bool s_DriverPresent;
	static bool s_CanPlayFeedback;
	
	bool m_Enabled;
	int m_DeviceIndex;
	lgDevPosition m_Position;
	ioFFButtonState m_ButtonState[kNumButtonValues];
	int m_LastWheelPosition;
	int m_AngWheelVelocity;

	unsigned int m_ButtonBits;
	unsigned int m_LastButtonBits;

	unsigned int m_DualshockButtons;

	float m_WheelRange;	//[0..1]

	bool IsEnabled() const { return m_Enabled; }
	void SetEnabled(bool bValue) { m_Enabled = bValue; }

	void StopAllFFEffects();
	bool ReDownload(eFFEffects kType);
	bool IsConnected() const { return (s_LGWheels)?s_LGWheels->IsConnected(m_DeviceIndex):false; }
	bool IsFFB() const { return (s_LGWheels)?(s_LGWheels->IsWheelClassB(m_DeviceIndex)||s_LGWheels->IsWheelClassA(m_DeviceIndex)):false; }
	bool IsGatedShifter() const;	// { return false; }
	
	unsigned int GetButtons() const { return m_ButtonBits; }

	bool GetButton(eButtons i) const { return (m_ButtonBits & s_LGButtonDefines[i]) == s_LGButtonDefines[i]; }
	const ioFFButtonState& GetButtonState(eButtons i) const { return m_ButtonState[i]; }

	float GetSteer() const { return m_Position.X1; }
	float GetAccel() const { return m_Position.Accelerator; }
	float GetBrake() const { return m_Position.Brake; }
	float GetNormSteer() const;	// [-1..1]
	float GetNormAccel() const;	// { return m_Position.Accelerator * (1.0f / 65535); }	// [0..1]
	float GetNormBrake() const;	// { return m_Position.Brake * (1.0f / 65535); }	// [-1..1]
	bool GetClutch() const; 
	int GetGear() const;
	int FFWheelAngVelocity() const {return m_Position.X1-m_LastWheelPosition;}  //- is CCW + is CW

	unsigned int GetDualshockButtons() const { return m_DualshockButtons; }

	void SetWheelRange(float range);		// [0..1]
	float GetWheelRange() const { return m_WheelRange; }

#if __BANK
	static void AddBankWidgets(rage::bkBank& B);
	static bool CreatedBank;
#endif 

protected:
	bool Update();
	void Reset();

};

};	// rage

#endif

#endif
