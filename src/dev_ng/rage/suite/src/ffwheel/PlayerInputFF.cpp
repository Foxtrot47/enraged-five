#include "playerinputff.h"

#include "input/input.h"
#if HAVE_WHEEL
#include "ffwheel/ffwheel.h"
#endif

#include "veh_dyna/chassis.h"


void vehPlayerInputFF::UpdateFFWheel() 
{
#if HAVE_WHEEL
#if 0 /// example code only
	const float OneOver256 = 1.0f/256.0f;
	const float OneOver128 = 1.0f/128.0f;
	if (GetIndex()<0) return; // no input index yet! 
	ioFFWheel * W = &ioFFWheel::GetFFWheel(GetIndex());
	if(!W->Connected())return;

	float Brake,Throttle,Handbrake;

	if(W->ButtonState[ioFFWheel::kCross].WasJustPressed)
			m_pSim->SetReverse(false);
	if(W->ButtonState[ioFFWheel::kTriangle].WasJustPressed)
			m_pSim->SetReverse(true);

	if(m_pSim->GetReverse())
		Throttle = (W->ButtonState[ioFFWheel::kTriangle].IsDown)?(1.0f):(0.0f);
	else
		Throttle = (W->ButtonState[ioFFWheel::kCross].IsDown)?(1.0f):(0.0f);

	Brake    = (W->ButtonState[ioFFWheel::kSquare].IsDown)?(1.0f):(0.0f);
	Handbrake= (W->ButtonState[ioFFWheel::kCircle].IsDown)?(1.0f):(0.0f);

	Throttle += W->m_Position.Accelerator*OneOver256;
	Brake    += W->m_Position.Brake*OneOver256;

	if(W->ButtonState[ioFFWheel::kRightPaddle].WasPressed)
		m_pSim->Upshift();

	if(W->ButtonState[ioFFWheel::kLeftPaddle].WasPressed)
		m_pSim->Downshift();

	Throttle = Clamp(Throttle,0.0f,1.0f);
	Brake    = Clamp(Brake,0.0f,1.0f);
	
	m_pSim->SetSteer((W->m_Position.X1-128)*OneOver128);
	m_pSim->SetThrottle(Throttle);
	m_pSim->SetBrake(Brake);
	m_pSim->SetHandbrake(Handbrake);
#endif
#endif
}

void vehPlayerInputFF::UpdateFFWheelEffects()
{
#if HAVE_WHEEL
	static float sOldSuspensionValue[2]={0.0f,0.0f};
	if(GetIndex()<0)return;
	ioFFWheel * W = &ioFFWheel::GetFFWheel(GetIndex());
	if(!W->Connected())return;

	//Here we can do all sorts of cool effect type stuff
	//like setting spring saturation based on car velocity
	//using the texture bumpiness to shake the wheel
	//collisions to jerk the wheel
	//centripetal force to apply constant forces to the wheel
	//sink depth increases resistance -centering spring

	int SForce=int(m_pSim->GetVelocity().Mag()*W->SpringForceModifier);
	SForce=175-SForce;
	if(SForce<0)SForce=0;
	SForce=Clamp(SForce,0,175);
	ioFFEffect &SF=W->GetAndChangeEffect(ioFFWheel::kSpring);
	SF.ForceEffect.p.condition[0].CoefficientNeg=
	SF.ForceEffect.p.condition[0].CoefficientPos=SForce;
//	SF.ForceEffect.p.condition[0].Offset=offset;
	W->ReDownload(ioFFWheel::kSpring);

	vehWheel *rwheel = m_pSim->GetWheel(1);			// get a quick accessor for the wheel
	vehWheel *lwheel = m_pSim->GetWheel(0);           // get a quick accessor for the wheel
	float SuspensionValue;
	if(rwheel->GetSuspensionValue() < 0.0f) {							// is the right shock extended?
		Assert(rwheel->GetTune()->GetSuspensionExtent() && "Suspension Extent value is 0!");
		SuspensionValue = (rwheel->GetSuspensionValue() / rwheel->GetTune()->GetSuspensionExtent()) * 125;	// yep, normalize to the SuspensionExtent
	} else {
		Assert(rwheel->GetTune()->GetSuspensionLimit() && "Suspension Extent value is 0!");
		SuspensionValue = (rwheel->GetSuspensionValue() / rwheel->GetTune()->GetSuspensionLimit()) * 125; 	// nope, normalize to the SuspensionLimit
	}
	if(rwheel->GetSuspensionValue() < 0.0f) {							// is the left shock extended?
		Assert(rwheel->GetTune()->GetSuspensionExtent() && "Suspension Extent value is 0!");
		SuspensionValue += (lwheel->GetSuspensionValue() / lwheel->GetTune()->GetSuspensionExtent()) * 125;	// yep, normalize to the SuspensionExtent
	} else {
		Assert(rwheel->GetTune()->GetSuspensionLimit() && "Suspension Extent value is 0!");
		SuspensionValue += (lwheel->GetSuspensionValue() / lwheel->GetTune()->GetSuspensionLimit()) * 125; 	// nope, normalize to the SuspensionLimit
	}
	float SuspensionForce = sOldSuspensionValue[GetIndex()] - SuspensionValue;		// get only the delta, so the wheel only turns on ups and downs
	sOldSuspensionValue[GetIndex()] = SuspensionValue;
	float Force = ((rwheel->GetForceLat()+lwheel->GetForceLat()) * (1.0f/150.0f));
	Force=Clamp(Force,-255.0f,255.0f);
	Force += SuspensionForce;
	Force=Clamp(Force,-255.0f,255.0f);

/*	float Jerk = Damage->GetLastDamage() * (1.0f/60.0f);
	if(Jerk > m_Jerk) {
		m_Jerk=Clamp(Jerk,0.0f,600.0f);
		Damage->GetLastDamage() = 0.0f;
	}
	if(m_Jerk) {
		m_Jerk -=100.0f;
		if(m_Jerk < 0.0f) m_Jerk = 0.0f;
		Force += m_Jerk;
		Force=Clamp(Force,-255.0f,255.0f);
	}
*/
  ioFFEffect &CF=W->GetAndChangeEffect(ioFFWheel::kConstF);
	CF.ForceEffect.p.ramp.MagnitudeStart=CF.ForceEffect.p.ramp.MagnitudeEnd=(int)(Force);
	W->ReDownload(ioFFWheel::kConstF);
	if(
		(!m_pSim->GetWheel(0)->IsOnGround()) &&
		(!m_pSim->GetWheel(1)->IsOnGround())
	)
	{
		W->StopFFEffect(ioFFWheel::kConstF);
		W->StopFFEffect(ioFFWheel::kSpring);
	}
	else {
		W->StartFFEffect(ioFFWheel::kConstF);
		W->StartFFEffect(ioFFWheel::kSpring);
	}
#endif
}

