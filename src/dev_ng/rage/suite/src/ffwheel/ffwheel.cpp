
#include "ffwheel.h"

#if HAVE_FFWHEEL

#if __PPU
#include <cell/usbd/libusbd.h>
#endif // __PPU
#include "data/marker.h"
#include "system/timer.h"
#include "grprofile/pix.h"
#include "math/amath.h"

#include "LogiWheel.h"
#include "liblgff.h"

#include "input/pad.h"

namespace rage {


#define PRINT_STATUS 0

using namespace LogitechEZWheelWrapper;

Wheel* ioFFWheel::s_LGWheels = 0;
ioFFWheel ioFFWheel::s_FFWheels[IO_FF_MAX_WHEELS];
bool	  ioFFWheel::s_DriverPresent = false; 
bool	  ioFFWheel::s_CanPlayFeedback = true;

// Logi buttons
rage::u64 ioFFWheel::s_LGButtonDefines[kNumButtonValues] =
{
	//same order as the eButtons enum in ffwheel.h
	LG_BUTTON_CROSS,
	LG_BUTTON_SQUARE,
	LG_BUTTON_CIRCLE	| LG_BUTTON_BUTTON5,
	LG_BUTTON_TRIANGLE	| LG_BUTTON_BUTTON7,
	LG_BUTTON_START		| LG_BUTTON_BUTTON6,
	LG_BUTTON_SELECT	| LG_BUTTON_BUTTON4,
	LG_BUTTON_L1		| LG_BUTTON_BUTTON0,
	LG_BUTTON_R1		| LG_BUTTON_BUTTON1,
	LG_BUTTON_L2		| LG_BUTTON_BUTTON2,
	LG_BUTTON_R2		| LG_BUTTON_BUTTON3,
	LG_BUTTON_L3,
	LG_BUTTON_R3,
	LG_DPAD_UP,
	LG_DPAD_DOWN,
	LG_DPAD_RIGHT,
	LG_DPAD_LEFT,
};

// Dual shock buttons
rage::u64 ioFFWheel::s_DSButtonDefines[kNumButtonValues] =
{
	//match the order of buttons in ioFFWheel::s_LGButtonDefines
	ioPad::RDOWN,
	ioPad::RLEFT,
	ioPad::RRIGHT,
	ioPad::RUP,
	ioPad::START,
	ioPad::SELECT,
	ioPad::L1,
	ioPad::R1,
	ioPad::L2,
	ioPad::R2,
	ioPad::L3,
	ioPad::R3,
	ioPad::LUP,
	ioPad::LDOWN,
	ioPad::LRIGHT,
	ioPad::LLEFT,
};

int ioFFWheel::s_NumDevices=0;

/*
 *	LoadDrivers() - Call this first, before InitClass()
 */
void ioFFWheel::LoadDrivers()
{
	RAGE_FUNC();

#if	0 //__PSX2
	// load usb driver
	if(!iopManager::LoadModule("usbd"))
	{
		s_DriverPresent = false;
		return;
	}

	// load wingman devices usb driver
	if(!iopManager::LoadModule("lgdevW"))
	{
		s_DriverPresent = false;
		return;
	}

#elif __PPU

	int r;
	if ((r = cellUsbdInit()) != CELL_OK) {
	    Errorf("Error: cellUsbdInit error (0x%x)", r);
		s_DriverPresent = false;
		return;
	}

#endif

	s_DriverPresent = true;
}

/*
*	UnloadDrivers() - Call this first, before InitClass()
*/
void ioFFWheel::UnloadDrivers()
{
#if	0 //__PSX2
	StopAllEffectsForAllWheels();

	// load wingman devices usb driver
	int safety=0;
	while(!iopManager::UnloadModule("lgdevW"))	// Wait
	{
		if(++safety>30)
			return;
	}

	s_DriverPresent = true;
#elif __PS3
	cellUsbdEnd();
	s_DriverPresent = false;
#endif
}


/*
 *	SetThreadCPU()
 *	Assign cpu thread index
 */
void ioFFWheel::SetThreadCPU(int XENON_ONLY(threadCPU))
{
#if __XENON
	lgDevSetThreadCPU( threadCPU );
#endif
}

/*
 *	InitClass()
 *	Allocate and initialize the class
 */
void ioFFWheel::InitClass()
{
	RAGE_FUNC();

	Assert(s_LGWheels == 0);

	s_CanPlayFeedback=true;

	if(s_DriverPresent==false)
	{
		Quitf("ioFFWheel::InitClass(): Drivers not loaded - Call LoadDrivers() first");
		return;
	}

	s_LGWheels = rage_new LogitechEZWheelWrapper::Wheel();

	s_LGWheels->ReadAll();	// Enumerate devices

	for (int index = 0; index < LG_MAX_CONTROLLERS; index++)
	{
		s_LGWheels->GenerateNonLinValues(index, 15); // Needs to be done after first ReadAll
	}

	//s_LGWheels->GetPosition(0).POV0 = 0xFF;
	//s_LGWheels->Position[1].POV0 = 0xFF;

//	SetState();
}

void ioFFWheel::ShutdownClass()
{
	delete s_LGWheels;
	s_LGWheels = 0;
	s_DriverPresent = false;

	lgDevDeInit();
}

void ioFFWheel::ResetAll()
{
	for(int i=0;i<IO_FF_MAX_WHEELS;i++)
		s_FFWheels[i].Reset();
}

void ioFFWheel::SetState()
{
}

void ioFFWheel::UpdateAll()
{
	if(!s_DriverPresent)
		return;

	PIXBegin(0, "ioFFWheel::UpdateAll");

#if __XENON
	lgDev360Update();
#endif // __XENON

	SetState();		// Setup

	if (s_LGWheels)
		s_LGWheels->ReadAll();

	for(int i=0;i<IO_FF_MAX_WHEELS;i++)
		s_FFWheels[i].Update();

#if __PS3
	// Shove wheel values into the 'dead' pads
	for(int i=0;i<s_NumDevices;i++)
	{
		ioFFWheel& wheel = ioFFWheel::GetFFWheel(i);
		if(wheel.IsConnected())
		{
			ioPad& pad = ioPad::GetPad(i);

			pad.SetButtons( wheel.GetDualshockButtons() );
			pad.SetAxis( 0, (u8)((1.0f + wheel.GetNormSteer()) * 127) );
			pad.SetAxis( 3, (u8)((1.0f + wheel.GetNormBrake()-wheel.GetNormAccel()) * 127) );
		}
	}
#endif

	PIXEnd();
}

void ioFFWheel::StopAllEffectsForAllWheels()
{
	for(int i=0;i<IO_FF_MAX_WHEELS;i++)
		s_FFWheels[i].StopAllFFEffects();
}

// the libraries on PS3 and 360 map the wheel in one of the controller indices, so look that index up
int ioFFWheel::FindFFWheelIndexFromPadIndex(int pad)
{
	if (s_LGWheels)
	{
		if (s_LGWheels->IsConnected(pad))
		{
			return pad;
		}
	}

	return -1;
}

ioFFWheel::ioFFWheel()
{
	Assert(m_DeviceIndex>=0 && m_DeviceIndex<IO_FF_MAX_WHEELS);
	m_DeviceIndex=s_NumDevices++;
	m_Enabled = true;
	m_WheelRange=1.0f;

	Reset();
}

void ioFFWheel::SetWheelRange(float range)		// [0..1]
{
	Assert(range > 0.0f);
	if(s_LGWheels && s_LGWheels->IsConnected(m_DeviceIndex) && s_LGWheels->IsMultiturnEnabled(m_DeviceIndex))
	{
		m_WheelRange = range;
	}
	else
	{
		m_WheelRange = Clamp(range,0.0f,1.0f);
	}
//	m_WheelRange = Clamp(range,0.0f,1.0f);
//	if(s_LGWheels && s_LGWheels->IsConnected(m_DeviceIndex) && s_LGWheels->GetMultiturn(m_DeviceIndex))
//	{
//		m_WheelRange *= 0.3f;		// 30 % range for 900 degree wheel (270 deg)
//	}
	//Displayf("ioFFWheel::GetWheelRange() %f",m_WheelRange);
}	


void ioFFWheel::Reset()
{
	int i;

	for(i=0; i<kNumButtonValues; i++)
	{
		m_ButtonState[i].IsDown = false;
		m_ButtonState[i].Pressed = false;
		m_ButtonState[i].Released = false;
	}

	m_ButtonBits = 0;
	m_LastButtonBits = 0;

	m_LastWheelPosition = 0;
	m_AngWheelVelocity = 0;

	m_DualshockButtons = 0;
}

bool ioFFWheel::Update()
{
	if(s_LGWheels->IsConnected(m_DeviceIndex)==false)
	{
		return false;
	}

	m_LastWheelPosition=m_Position.X1;
	m_LastButtonBits=m_ButtonBits;

	m_Position = *s_LGWheels->GetPosition(m_DeviceIndex);

	m_ButtonBits = 0;	// Reset bitmask
	//m_ButtonBits = m_Position.Buttons;
	m_DualshockButtons = 0;

    {
		// Update button state
		for(int i=0;i<kNumButtonValues;i++)
		{
			m_ButtonState[i].IsDown = s_LGWheels->ButtonIsPressed(m_DeviceIndex,s_LGButtonDefines[i]);
			m_ButtonState[i].Pressed = s_LGWheels->ButtonTriggered(m_DeviceIndex,s_LGButtonDefines[i]);
			m_ButtonState[i].Released = s_LGWheels->ButtonReleased(m_DeviceIndex,s_LGButtonDefines[i]);

			// Build bitmask
			if(m_ButtonState[i].IsDown)
				m_ButtonBits |= s_LGButtonDefines[i];

			// Dualshock ioPad buttons
			if(m_ButtonState[i].IsDown)
				m_DualshockButtons |= s_DSButtonDefines[i];
		}

		m_AngWheelVelocity=m_LastWheelPosition-m_Position.X1;

#if PRINT_STATUS
		// only print data which is new....
        if(LGSTATUS_OLD_DATA != m_Position.Status)
        {
			Displayf("FFWheel %d",m_DeviceIndex);

			Displayf("Wheel: %d, Accel: %d, Brake:  %d, Buttons (0x%08x), Dualshock (0x%08x)",
                m_Position.X1, m_Position.Accelerator, m_Position.Brake, m_ButtonBits, m_DualshockButtons);
            if(LGBUTTON_LEFT_PADDLE & m_Position.Buttons)
                Displayf("Left paddle (B0)\n");
            if(LGBUTTON_RIGHT_PADDLE & m_Position.Buttons)
                Displayf("Right paddle (B1)\n");
            if(LGBUTTON_BUTTON2 & m_Position.Buttons)
                Displayf("Button2 \n");
            if(LGBUTTON_BUTTON3 & m_Position.Buttons)
                Displayf("Button3 \n");
            if(LGBUTTON_BUTTON4 & m_Position.Buttons)
                Displayf("Button4 \n");
            if(LGBUTTON_BUTTON5 & m_Position.Buttons)
                Displayf("Button5 \n");
            Displayf("\n");

        }
#endif
	}
	return true;
}

float ioFFWheel::GetNormSteer() const	// [-1..1]
{
	float fSteer;

	if(s_LGWheels && s_LGWheels->IsConnected(m_DeviceIndex) && s_LGWheels->IsMultiturnEnabled(m_DeviceIndex))
	{	
		// 180 deg range for 900 degree wheel
		const int kRange = int((180 / 900.0f) * 31500);	//32768
		fSteer = m_Position.X1 * (1.0f / kRange);	
	}
	else
	{
		// Full range
		fSteer = m_Position.X1 * (1.0f / 31500);	//32768
	}

	fSteer /= m_WheelRange;
	fSteer = Clamp(fSteer,-1.0f,1.0f);

	return fSteer;
}

float ioFFWheel::GetNormAccel() const
{
	return Clamp(m_Position.Accelerator * (1.0f / 63000), -1.0f, 1.0f);
}

float ioFFWheel::GetNormBrake() const
{
	return Clamp(m_Position.Brake * (1.0f / 63000), -1.0f, 1.0f);
}

bool ioFFWheel::GetClutch() const
{
	return (s_LGWheels?s_LGWheels->GetPosition(m_DeviceIndex)->Clutch > 0.5f*63000 : false); 
	//return false;
}

int ioFFWheel::GetGear() const
{
	return (s_LGWheels?s_LGWheels->GetPosition(m_DeviceIndex)->Gear : 0); 
	//return 0;
}

bool ioFFWheel::IsGatedShifter() const 
{ 
	return (s_LGWheels?s_LGWheels->IsGatedShifterSet(m_DeviceIndex):false); 
}


void ioFFWheel::StopAllFFEffects()
{
	Assert(m_DeviceIndex>=0 && m_DeviceIndex<LogitechEZWheelWrapper::LG_MAX_CONTROLLERS);

	LogitechEZWheelWrapper::Wheel * W = ioFFWheel::GetLGWheels();
	if(W->IsConnected(m_DeviceIndex))
	{
		W->StopBumpyRoadEffect(m_DeviceIndex);
		W->StopDirtRoadEffect(m_DeviceIndex);
		W->StopSlipperyRoadEffect(m_DeviceIndex);
		W->StopSurfaceEffect(m_DeviceIndex);
		W->StopCarAirborne(m_DeviceIndex);

		W->StopConstantForce(m_DeviceIndex);
		W->StopDamperForce(m_DeviceIndex);
		//W->StopSoftstopForce(m_DeviceIndex);
		//W->StopSpringForce(m_DeviceIndex);

		W->PlaySpringForce(m_DeviceIndex,0,70,70);
	}
}
};	// rage

#endif

