/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#ifndef WHEEL_H_INCLUDED_
#define WHEEL_H_INCLUDED_

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiGlobals.h"
#include "LogiCommon.h"
#include "LogiControllerManager.h"

namespace LogitechEZWheelWrapper
{
    class Wheel
    {
    public:
        Wheel();
        ~Wheel();

        LG_VOID ReadAll();

        LG_BOOL IsConnected(const LG_INT port_no);
        lgDevPosition* GetPosition(const LG_INT port_no);
        LG_BOOL ButtonTriggered(const LG_INT port_no, const LG_ULONG buttonMask);
        LG_BOOL ButtonReleased(const LG_INT port_no, const LG_ULONG buttonMask);
        LG_BOOL ButtonIsPressed(const LG_INT port_no, const LG_ULONG buttonMask);
        LG_BOOL WheelAMenuIsPressed(const LG_INT port_no, const MenuDirection direction);
        LG_BOOL WheelAMenuTriggered(const LG_INT port_no, const MenuDirection direction);
        LG_BOOL IsWheelClassA(const LG_INT port_no);
        LG_BOOL IsWheelClassB(const LG_INT port_no);
        LG_BOOL IsMultiturnCapable(const LG_INT port_no);
        LG_BOOL IsMultiturnEnabled(const LG_INT port_no);
        LG_INT SetMultiturn(const LG_INT port_no, const LG_BOOL multiturn);
        LG_BOOL IsClutchPresent(const LG_INT port_no);
        LG_BOOL IsGatedShifterPresent(const LG_INT port_no);
        LG_BOOL IsGatedShifterSet(const LG_INT port_no);

        LG_VOID GenerateNonLinValues(const LG_INT port_no, const LG_INT nonLinCoeff);
        LG_INT GetNonLinearValue(const LG_INT port_no, const LG_INT inputValue);

        LG_BOOL IsPlaying(const LG_INT port_no, const ForceType forceType);
        LG_VOID PlaySpringForce(const LG_INT port_no, const LG_INT offsetPercentage, const LG_INT saturationPercentage, const LG_INT coefficientPercentage);
        LG_VOID StopSpringForce(const LG_INT port_no);
        LG_VOID PlayConstantForce(const LG_INT port_no, const LG_INT strengthPercentage);
        LG_VOID StopConstantForce(const LG_INT port_no);
        LG_VOID PlayDamperForce(const LG_INT port_no, const LG_INT strengthPercentage);
        LG_VOID StopDamperForce(const LG_INT port_no);
        LG_VOID PlaySideCollisionForce(const LG_INT port_no, const LG_INT strengthPercentage);
        LG_VOID PlayFrontalCollisionForce(const LG_INT port_no, const LG_INT strengthPercentage);
        LG_VOID PlayDirtRoadEffect(const LG_INT port_no, const LG_INT strengthPercentage);
        LG_VOID StopDirtRoadEffect(const LG_INT port_no);
        LG_VOID PlayBumpyRoadEffect(const LG_INT port_no, const LG_INT strengthPercentage);
        LG_VOID StopBumpyRoadEffect(const LG_INT port_no);
        LG_VOID PlaySlipperyRoadEffect(const LG_INT port_no, const LG_INT strengthPercentage);
        LG_VOID StopSlipperyRoadEffect(const LG_INT port_no);
        LG_VOID PlaySurfaceEffect(const LG_INT port_no, const LG_INT type, const LG_INT strengthPercentage, const LG_INT period);
        LG_VOID StopSurfaceEffect(const LG_INT port_no);
        LG_VOID PlayCarAirborne(const LG_INT port_no);
        LG_VOID StopCarAirborne(const LG_INT port_no);
        LG_VOID PlaySoftstopForce(const LG_INT port_no, const LG_INT deadbandPercentage);
        LG_VOID StopSoftstopForce(const LG_INT port_no);

        LG_INT SetOverallForceGain(const LG_INT port_no, const LG_INT strengthPercentage);
        LG_INT GetOverallForceGainPercentage(const LG_INT port_no);

    private:
        ControllerManager m_controllerManager; 
    };
}

#endif

#endif // WHEEL_H_INCLUDED_
