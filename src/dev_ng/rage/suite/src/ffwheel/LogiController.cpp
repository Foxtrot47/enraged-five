/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#include "LogiController.h"

#if HAVE_FFWHEEL

#include "LogiUtils.h"
#include <string.h>

using namespace LogitechEZWheelWrapper;

Controller::Controller()
{
    Init();
}

Controller::~Controller()
{

}

LG_VOID Controller::Init()
{
    m_handle = -1;
    m_class = -1;
    m_hasMultiturn = false;
    m_isMultiturnEnabled = -1;
    m_hasGatedShifter = false;
    m_hasClutch = false;
    m_portNbr = -1;
    m_overallForceGainPercentage = -1;
    m_nonLinearCoefficient = 0;

    memset(&m_position, 0, sizeof(m_position));
    memset(&m_positionLast, 0, sizeof(m_positionLast));

    m_position.POV0 = 255;
    m_position.POV1 = 255;
    m_positionLast.POV0 = 255;
    m_positionLast.POV1 = 255;

    GenerateNonLinValues(m_nonLinearCoefficient);
}

LG_INT Controller::Read()
{
    if (-1 == m_handle)
    {
        LOGITRACE("ERROR: trying to read wheel but handle is NULL\n");
        return LG_ERROR;
    }

    m_positionLast = m_position;

    LG_INT ret_ = lgDevRead(m_handle, LGREADMODE_IMMEDIATE, &m_position);

    return ret_;
}

lgDevPosition* Controller::GetPosition()
{
    return &m_position;
}

LG_INT Controller::IsMultiturnEnabled()
{
    if (-1 == m_handle)
    {
        LOGITRACE("ERROR: trying to check multiturn but wheel handle is NULL\n");
        return false;
    }

    if (-1 == m_isMultiturnEnabled)
    {
        LG_INT value_ = -1;
        if (LGSUCCEEDED(lgDevGetDeviceProperty(m_handle, LGPROP_MULTITURN, &value_)))
        {
            if (1 == value_)
                return 1;
        }

        return 0;
    }

    return m_isMultiturnEnabled;
}

LG_INT Controller::SetMultiturn(const LG_INT value)
{
    if (-1 == m_handle)
    {
        LOGITRACE("ERROR: trying to set multiturn but wheel handle is NULL\n");
        return LG_ERROR;
    }

    LG_INT ret_ = LG_SUCCESS;

    if (LGSUCCEEDED(ret_ = lgDevSetDeviceProperty(m_handle, LGPROP_MULTITURN, value)))
    {
        m_isMultiturnEnabled = value;
    }
    else
    {
        LOGITRACE("ERROR: failed to set multiturn to %d for handle 0x%x\n", value, m_handle);
    }

    return ret_;
}

LG_BOOL Controller::IsGatedShifterSet()
{
    LG_INT value_ = -1;
    if (LGSUCCEEDED(lgDevGetDeviceProperty(m_handle, LGPROP_GATED_SHIFTER, &value_)))
    {
        if (value_)
            return true;
    }
    else
    {
        LOGITRACE("ERROR getting device gated shifter property for handle 0x%x\n", m_handle);
    }

    return false;
}

LG_VOID Controller::SetHandle(const LG_INT handle) 
{ 
    m_handle = handle; 
    m_forceManager.SetWheelHandle(handle);
}

LG_BOOL Controller::ButtonTriggered(const LG_ULONG buttonMask)
{
    switch (buttonMask)
    {
    case LG_DPAD_UP:
        if (DPadTriggered(LG_LOCAL_DPAD_UP))
        {
            return true;
        }
        break;
    case LG_DPAD_DOWN:
        if (DPadTriggered(LG_LOCAL_DPAD_DOWN))
        {
            return true;
        }
        break;
    case LG_DPAD_RIGHT:
        if (DPadTriggered(LG_LOCAL_DPAD_RIGHT))
        {
            return true;
        }
        break;
    case LG_DPAD_LEFT:
        if (DPadTriggered(LG_LOCAL_DPAD_LEFT))
        {
            return true;
        }
        break;
    }

    return ( !(m_positionLast.Buttons & buttonMask) &&
        (m_position.Buttons & buttonMask));
}

LG_BOOL Controller::ButtonReleased(const LG_ULONG buttonMask)
{
    switch (buttonMask)
    {
    case LG_DPAD_UP:
        if (DPadReleased(LG_LOCAL_DPAD_UP))
        {
            return true;
        }
        break;
    case LG_DPAD_DOWN:
        if (DPadReleased(LG_LOCAL_DPAD_DOWN))
        {
            return true;
        }
        break;
    case LG_DPAD_RIGHT:
        if (DPadReleased(LG_LOCAL_DPAD_RIGHT))
        {
            return true;
        }
        break;
    case LG_DPAD_LEFT:
        if (DPadReleased(LG_LOCAL_DPAD_LEFT))
        {
            return true;
        }
        break;
    }

    return ((m_positionLast.Buttons & buttonMask) &&
        !(m_position.Buttons & buttonMask));
}

LG_BOOL Controller::ButtonIsPressed(const LG_ULONG buttonMask)
{
    switch (buttonMask)
    {
    case LG_DPAD_UP:
        if (DPadIsPressed(LG_LOCAL_DPAD_UP))
        {
            return true;
        }
        break;
    case LG_DPAD_DOWN:
        if (DPadIsPressed(LG_LOCAL_DPAD_DOWN))
        {
            return true;
        }
        break;
    case LG_DPAD_RIGHT:
        if (DPadIsPressed(LG_LOCAL_DPAD_RIGHT))
        {
            return true;
        }
        break;
    case LG_DPAD_LEFT:
        if (DPadIsPressed(LG_LOCAL_DPAD_LEFT))
        {
            return true;
        }
        break;
    }

	return (m_position.Buttons & buttonMask) ? true : false;
}

LG_BOOL Controller::WheelAMenuIsPressed(const MenuDirection direction)
{
    // only do this for class A wheels
    if (LGCLASS_WHEEL_A != m_class)
        return false;

    switch (direction)
    {
    case LG_MENU_UP:
        // Range is 0 to LG_UNSIGNED_16_BIT_RANGE_MAX. Let's trigger when half of the range is reached
        if (m_position.Accelerator > (LG_UNSIGNED_16_BIT_RANGE_MAX / 2))
            return true;
        break;
    case LG_MENU_DOWN:
        if (m_position.Brake > (LG_UNSIGNED_16_BIT_RANGE_MAX / 2))
            return true;
        break;

        // Range is 0 to 32767. Trigger when half is reached = about 15000
    case LG_MENU_RIGHT:
        if (m_position.X1 > (LG_SIGNED_16_BIT_RANGE_MAX / 2))
            return true;
        break;
    case LG_MENU_LEFT:
        if (m_position.X1 < (LG_SIGNED_16_BIT_RANGE_MIN / 2))
            return true;
        break;
    default:
        LOGIASSERT(0);
    }

    return false;
}

LG_BOOL Controller::WheelAMenuTriggered(const MenuDirection direction)
{
    // only do this for class A wheels
    if (LGCLASS_WHEEL_A != m_class)
        return false;

    switch (direction)
    {
    case LG_MENU_UP:
        if ((m_position.Accelerator > (LG_UNSIGNED_16_BIT_RANGE_MAX / 2)) && (m_positionLast.Accelerator < (LG_UNSIGNED_16_BIT_RANGE_MAX / 2)))
            return true;
        break;
    case LG_MENU_DOWN:
        if ((m_position.Brake > (LG_UNSIGNED_16_BIT_RANGE_MAX / 2)) && (m_positionLast.Brake < (LG_UNSIGNED_16_BIT_RANGE_MAX / 2)))
            return true;
        break;
    case LG_MENU_RIGHT:
        if ((m_position.X1 > (LG_SIGNED_16_BIT_RANGE_MAX / 2)) && (m_positionLast.X1 < (LG_SIGNED_16_BIT_RANGE_MAX / 2)))
            return true;
        break;
    case LG_MENU_LEFT:
        if ((m_position.X1 < (LG_SIGNED_16_BIT_RANGE_MIN / 2)) && (m_positionLast.X1 > (LG_SIGNED_16_BIT_RANGE_MIN / 2)))
            return true;
        break;
    default:
        LOGIASSERT(0);
    }

    return false;
}

LG_BOOL Controller::IsWheelClassA()
{
    return (m_class == LGCLASS_WHEEL_A);
}

LG_BOOL Controller::IsWheelClassB()
{
    return (m_class == LGCLASS_WHEEL_B);
}

LG_BOOL Controller::DPadIsPressed(const LG_INT direction)
{
    if (255 == m_position.POV0)
        return false;

    switch (direction)
    {
    case LG_LOCAL_DPAD_UP:
        if (DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_UP])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_RIGHT:
        if (DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_RIGHT])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_DOWN:
        if (DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_DOWN])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_LEFT:
        if (DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_LEFT])
        {
            return true;
        }
        break;
    default:
        LOGIASSERT(0);
    }
    return false;

}

LG_BOOL Controller::DPadTriggered(const LG_INT direction)
{
    if (m_position.POV0 == 255)
    {
        return false;
    }

    switch (direction)
    {
    case LG_LOCAL_DPAD_UP:
        if (1 == DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_UP]
        && 1 != DPAD_POSITION[m_positionLast.POV0][LG_LOCAL_DPAD_UP])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_RIGHT:
        if (1 == DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_RIGHT]
        && 1 != DPAD_POSITION[m_positionLast.POV0][LG_LOCAL_DPAD_RIGHT])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_DOWN:
        if (1 == DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_DOWN]
        && 1 != DPAD_POSITION[m_positionLast.POV0][LG_LOCAL_DPAD_DOWN])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_LEFT:
        if (1 == DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_LEFT]
        && 1 != DPAD_POSITION[m_positionLast.POV0][LG_LOCAL_DPAD_LEFT])
        {
            return true;
        }
        break;
    default:
        LOGIASSERT(0);
    }

    return false;

}

LG_BOOL Controller::DPadReleased(const LG_INT direction)
{
    switch (direction)
    {
    case LG_LOCAL_DPAD_UP:
        if (1 != DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_UP]
        && 1 == DPAD_POSITION[m_positionLast.POV0][LG_LOCAL_DPAD_UP])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_RIGHT:
        if (1 != DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_RIGHT]
        && 1 == DPAD_POSITION[m_positionLast.POV0][LG_LOCAL_DPAD_RIGHT])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_DOWN:
        if (1 != DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_DOWN]
        && 1 == DPAD_POSITION[m_positionLast.POV0][LG_LOCAL_DPAD_DOWN])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_LEFT:
        if (1 != DPAD_POSITION[m_position.POV0][LG_LOCAL_DPAD_LEFT]
        && 1 == DPAD_POSITION[m_positionLast.POV0][LG_LOCAL_DPAD_LEFT])
        {
            return true;
        }
        break;
    default:
        LOGIASSERT(0);
    }

    return false;
}

LG_INT Controller::SetOverallForceGain(const LG_INT strengthPercentage)
{
    if (-1 == m_handle)
    {
        LOGITRACE("ERROR: trying to set overall force gain but wheel handle is NULL\n");
        return LG_ERROR;
    }

    LG_INT overallGain_ = Utils::FromPercentage(strengthPercentage, 0, LG_MAX_PERCENTAGE, 0, LG_8_BIT_RANGE_MAX);

    LG_INT ret_ = lgDevSetDeviceProperty(m_handle, LGPROP_OVERALLFORCEGAIN, overallGain_);
    if (LGFAILED(ret_))
    {
        LOGITRACE("ERROR: Failed to set overall force gain. Error is 0x%x\n", ret_);
    }
    else
    {
        m_overallForceGainPercentage = strengthPercentage;
    }

    return ret_;
}

LG_INT Controller::GetOverallForceGainPercentage()
{
    if (-1 == m_handle)
    {
        LOGITRACE("ERROR: trying to get overall force gain but wheel handle is NULL\n");
        return LG_MAX_PERCENTAGE;
    }

    LG_INT overallForceGain_ = LG_MAX_PERCENTAGE;

    if (-1 == m_overallForceGainPercentage)
    {
#ifdef __SNC__
#pragma diag_suppress 552
#endif

        LG_INT ret_ = LG_SUCCESS;
        if (LGFAILED(ret_  = lgDevGetDeviceProperty(m_handle, LGPROP_OVERALLFORCEGAIN, &overallForceGain_)))
        {
            LOGITRACE("ERROR: Get overall force gain failed (0x%x)\n", ret_);
        }
        else
        {
            return static_cast<LG_INT>((static_cast<LG_FLOAT>(overallForceGain_) / static_cast<LG_FLOAT>(LGMAGNITUDE_MAX)) * static_cast<LG_FLOAT>(LG_MAX_PERCENTAGE));
        }
    }
    else
    {
        return m_overallForceGainPercentage;
    }

    return LG_MAX_PERCENTAGE;
}

// nonLinCoeff between 0 and 100. 0 = linear, 100 = maximum mon-linear.
LG_VOID Controller::GenerateNonLinValues(const LG_INT nonLinCoeff)
{
    if (nonLinCoeff == m_nonLinearCoefficient)
        return;

    m_nonLinearCoefficient = nonLinCoeff;

    // Populate lookup table
    for (LG_INT ii = 0; ii < LG_LOOKUP_TABLE_SIZE - 1; ii++)
    {
        m_nonLinearWheel[ii] = static_cast<LG_INT>(CalculateNonLinValue(ii, m_nonLinearCoefficient, LG_NON_LIN_MIN_OUTPUT, LG_NON_LIN_MAX_OUTPUT));
    }

    // Let's use 10 bits for reading wheel axis, which means 1024 counts. 0 - 1023 gives 511.5 as center position. We need a TRUE center
    // so let's use the range of 0 to 1022 and just define 1023 as equal to 1022.
    m_nonLinearWheel[LG_LOOKUP_TABLE_SIZE - 1] = m_nonLinearWheel[LG_LOOKUP_TABLE_SIZE - 2];
}

///////////////////////////////////////////////////////////////////////
// Method: calculateNonLinearValue(LG_INT tableIndex, LG_INT nonLinearCoeff,
// LG_FLOAT physicsMinInput, LG_FLOAT physicsMaxInput)
//     Method calculates a non-linear output value from a linear
//     input value.
//
// Arguments: 
//  inputValue: must be between 0 and LG_LOOKUP_TABLE_SIZE. 
//
//  nonLinearCoeff: non-linearity coefficient which must be a
//      value between 0 and 100.
//      0 corresponds to a completely linear response curve and 100
//      to a heavily non-linear response curve.
//
//  minOutput and maxOutput: minimum and maximum
//      numbers that you want as output in your lookup table. For
//      example if your physics engine takes -1000 to 1000 as input
//      you may specify those values here.
//
// Returns: floating number which has a value between minOutput
//      and maxOutput and which reflects the chosen
//      non-linearity curve.
///////////////////////////////////////////////////////////////////////
LG_FLOAT Controller::CalculateNonLinValue(const LG_INT tableIndex, const LG_INT nonLinearCoeff, const LG_INT minOutput, const LG_INT maxOutput)
{
    // Let's use 10 bits for reading wheel axis, which means 1024 counts. 0 - 1023 gives 511.5 as center position. We need a TRUE center
    // so let's use the range of 0 to 1022 and just define 1023 as equal to 1022.
    LG_INT MaxLookupTableInput_ = 1022;  // These values correspond to the
    // wheel's position values.
    LG_INT MinLookupTableInput_ = 0;
    LG_FLOAT outputValue_;

    // In order to center our curve on the X axis let's calculate the
    // center offset value
    LG_FLOAT centerOffset_ = (LG_FLOAT)(MaxLookupTableInput_ -
        MinLookupTableInput_) / 2;

    // Calculate maximum on x axis for the centered curve
    LG_FLOAT centeredCurveMax_ = MaxLookupTableInput_ - centerOffset_;

    // Normalize non-linear coefficient
    LG_FLOAT nonLinearCoeffNormalized_ = (LG_FLOAT)nonLinearCoeff/100;

    // Normalize input value
    outputValue_=((LG_FLOAT)(tableIndex - centerOffset_))/centeredCurveMax_;

    // Apply a cubical curve
    outputValue_=(((maxOutput - minOutput)/2)*((1.0f-nonLinearCoeffNormalized_)*outputValue_+(nonLinearCoeffNormalized_)
        *(outputValue_*outputValue_*outputValue_))) + ((maxOutput + minOutput)/2);

    return outputValue_;
}

LG_INT Controller::GetNonLinearValue(const LG_INT inputValue)
{
    if (inputValue < LG_NON_LIN_MIN_OUTPUT)
    {
        return static_cast<LG_INT>(LG_NON_LIN_MIN_OUTPUT);
    }

    if (inputValue > LG_NON_LIN_MAX_OUTPUT)
    {
        return static_cast<LG_INT>(LG_NON_LIN_MAX_OUTPUT);
    }

    LG_FLOAT middleNumber_ = static_cast<LG_FLOAT>(LG_LOOKUP_TABLE_SIZE - 1) / 2.0f;
    LG_INT index_ = (LG_INT)(((middleNumber_ * (LG_FLOAT)inputValue) / LG_NON_LIN_MAX_OUTPUT) + middleNumber_);

    return m_nonLinearWheel[index_];
}

#endif
