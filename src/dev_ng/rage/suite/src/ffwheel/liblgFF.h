/*

   liblgFF.h - version 1.04.008

   Library definition for liblgFF.a

   Part of liblgFF for PLAYSTATION(R)3

   Copyright 2006-2008 Logitech. All rights reserved.

   The Logitech Force Feedback SDK, including all acompanying
   documentation, is protected by intellectual property laws.  All use
   of the Logitech Force Feedback SDK is subject to the License
   Agreement found in the "liblgFF_license_agreement.pdf" file and in
   the Reference Manual.  All rights not expressly granted by Logitech
   are reserved.

*/

#ifndef _LIBLGFF_H_INCLUDED_
#define _LIBLGFF_H_INCLUDED_

//#include <sys/types.h>

#if defined(_LANGUAGE_C_PLUS_PLUS)||defined(__cplusplus)||defined(c_plusplus)
extern "C" {
#endif


// defines the maximum number of devices supported by the library
#define LGDEV_MAX_DEVICES 4


//************************************************************************
// lgDevInitParams
//************************************************************************

// Defines the library initialization parameters.
typedef struct lgDevInitParams
{
    // Maximum devices supported by the library. Pass in 0 to use
    // library default value (2).
    rage::u32 MaxDevices;

    // Maximum effects supported for all devices added together. Pass
    // in 0 to use library default value (16).
    rage::u32 MaxEffects;

    // Thread priority for the library internal threads. Pass in 0 to
    // use library default value (2000).
    rage::u32 ThreadPriority;

    // Custom memory allocators. Pass in NULL in either of the fields
    // to use standard C-runtime library malloc()/free().
    void* (*pfnMemAlloc)(size_t);
    void (*pfnMemFree)(void *);

} lgDevInitParams;


//************************************************************************
// lgDevPosition
//************************************************************************

// Device positional information is returned in this structure.
// Pass in the address to lgDevRead(). Examine either the value of member
// 'Status' or the return value of lgDevRead for successful completion.

// The ranges for X1, Y1, X2, Y2 and Twist have been expanded to 16 bit
// signed. Notice that the range is symmetrical around 0 (32767 counts
// each way) yielding a total range of 65535 (2^16 - 1).

typedef struct lgDevPosition
{
    // one of the LGSTATUS_* flags below
    rage::u16 Status;

    // -32767..32767, neutral position at 0, lower values mean moving
    // to the right, higher values mean moving to the left
    rage::s16 X1;

    // -32767..32767, neutral position at 0, lower values mean moving
    // away from the user, higher values mean moving towards the user.
    // Wheels: combined pedal information, neutral at 0, lower
    // values mean gas pedal being pressed, higher values mean brake
    // pedal being pressed.
    rage::s16 Y1;

    // -32767..32767, neutral position at 0, lower values mean moving
    // to the right, higher values mean moving to the left
    rage::s16 X2;

    // -32767..32767, neutral position at 0, lower values mean moving
    // away from the user, higher values mean moving towards the user.
    rage::s16 Y2;

    // 0..65535, no neutral position, higher values mean more throttle
    // is applied.
    rage::u16 Throttle;

    // -32767..32767, neutral position at 0, lower values mean rotating
    // counterclockwise, higher values mean rotating clockwise
    rage::s16 Twist;

    // 0..65535, neutral position at 0, higher values mean more gas
    // pedal pressed
    rage::u16 Accelerator;

    // 0..65535, neutral position at 0, higher values mean more brake
    // pedal pressed
    rage::u16 Brake;

    // 0..65535, neutral position at 0, higher values mean more clutch
    // pedal pressed
    rage::u16 Clutch;

    // Point Of View, 0xff,0..7, neutral position at 0xff, directions
    // coded 45 degree segments, starting from straight north,
    // clockwise.
    rage::u8 POV0;
    rage::u8 POV1;

    // Gated shifter position: -1..6, neutral gear at 0, -1 is reverse gear
    rage::s8 Gear;

    // Position of the shifter handle (0..255), only available on some
    // wheels
    rage::u8 ShifterHandleX; // 0 = left of the box, 255 = right of the box
    rage::u8 ShifterHandleY; // 0 = bottom of the box, 255 = top of the box

    // Relative change of Dial control in number of ticks since last
    // poll, range -128..+127
    rage::s8 Dial;

    // bitmask of several different device-specific states, such as
    // "pedals connected", see LGMISCSTATE_* below
    rage::u32 MiscState;

    // bitmask, bit 0 == button 0, combination of the LGBUTTON_*
    // constants below
    rage::u64 Buttons;

} lgDevPosition;


// lgDevPosition.Status:
#define LGSTATUS_OK                         (0x00)
#define LGSTATUS_OLD_DATA                   (0x01)
#define LGSTATUS_ERROR                      (0xff)


// lgDevPosition.Buttons:

    // friendly name for some specific buttons
#define LGBUTTON_TRIGGER                    (0x0000000000000001ll)
#define LGBUTTON_LEFT_PADDLE                (0x0000000000000001ll)
#define LGBUTTON_RIGHT_PADDLE               (0x0000000000000002ll)

    // regular numbering
#define LGBUTTON_BUTTON0                    (0x0000000000000001ll)
#define LGBUTTON_BUTTON1                    (0x0000000000000002ll)
#define LGBUTTON_BUTTON2                    (0x0000000000000004ll)
#define LGBUTTON_BUTTON3                    (0x0000000000000008ll)
#define LGBUTTON_BUTTON4                    (0x0000000000000010ll)
#define LGBUTTON_BUTTON5                    (0x0000000000000020ll)
#define LGBUTTON_BUTTON6                    (0x0000000000000040ll)
#define LGBUTTON_BUTTON7                    (0x0000000000000080ll)
#define LGBUTTON_BUTTON8                    (0x0000000000000100ll)
#define LGBUTTON_BUTTON9                    (0x0000000000000200ll)
#define LGBUTTON_BUTTON10                   (0x0000000000000400ll)
#define LGBUTTON_BUTTON11                   (0x0000000000000800ll)
#define LGBUTTON_BUTTON12                   (0x0000000000001000ll)
#define LGBUTTON_BUTTON13                   (0x0000000000002000ll)
#define LGBUTTON_BUTTON14                   (0x0000000000004000ll)
#define LGBUTTON_BUTTON15                   (0x0000000000008000ll)

    // PLAYSTATION(R)3 regular buttons
#define LGBUTTON_CROSS                      (0x0000000000010000ll)
#define LGBUTTON_SQUARE                     (0x0000000000020000ll)
#define LGBUTTON_CIRCLE                     (0x0000000000040000ll)
#define LGBUTTON_TRIANGLE                   (0x0000000000080000ll)
#define LGBUTTON_START                      (0x0000000000100000ll)
#define LGBUTTON_SELECT                     (0x0000000000200000ll)
#define LGBUTTON_L1                         (0x0000000000400000ll)
#define LGBUTTON_R1                         (0x0000000000800000ll)
#define LGBUTTON_L2                         (0x0000000001000000ll)
#define LGBUTTON_R2                         (0x0000000002000000ll)
#define LGBUTTON_L3                         (0x0000000004000000ll)
#define LGBUTTON_R3                         (0x0000000008000000ll)

#define LGBUTTON_SHIFTER_FORWARD            (0x0000000010000000ll)
#define LGBUTTON_SHIFTER_BACKWARD           (0x0000000020000000ll)
#define LGBUTTON_ENTER                      (0x0000000040000000ll)
#define LGBUTTON_ROCKER_PLUS                (0x0000000080000000ll)
#define LGBUTTON_ROCKER_MINUS               (0x0000000100000000ll)
#define LGBUTTON_HORN                       (0x0000000200000000ll)

// various states
#define LGMISCSTATE_EXTERNAL_PEDALS_CONNECTED         (0x00000001)
#define LGMISCSTATE_GATED_SHIFTER                     (0x00000002)
#define LGMISCSTATE_SHIFTER_PUSHED_DOWN               (0x00000004)


//************************************************************************
// lgDevDeviceDesc
//************************************************************************

typedef struct lgDevDeviceDesc {
    rage::u32 DevClass;
    rage::u32 AdditionalControls;
    rage::u32 Flags;
    rage::u8 Topology[8];
    int ExistingHandle;
    // "Port Number" of this device in the CellPadInfo structure where
    // status[i] == CELL_PAD_STATUS_LDD_CONNECTED (aka 5, aka "Custom
    // Controller is connected")
    rage::u32 CellPadPortNo;
} lgDevDeviceDesc;


// lgDevDeviceDesc.DevClass:
// the class of a particular device
#define LGCLASS_WHEEL_A                     (0x00000011)
#define LGCLASS_WHEEL_B                     (0x00000012)
#define LGCLASS_JOYSTICK_A                  (0x00000021)
#define LGCLASS_JOYSTICK_B                  (0x00000022)

// lgDevDeviceDesc.AdditionalControls:
#define LGAC_X2Y2                           (0x00000001)
#define LGAC_THROTTLE                       (0x00000002)
#define LGAC_TWIST                          (0x00000004)
#define LGAC_POV0                           (0x00000008)
#define LGAC_POV1                           (0x00000010)
#define LGAC_B_SHAPE                        (0x00000020)
#define LGAC_B_STARTSELECT                  (0x00000040)
#define LGAC_B_L1R1                         (0x00000080)
#define LGAC_B_L2R2                         (0x00000100)
#define LGAC_B_L3R3                         (0x00000200)
#define LGAC_B_SHIFTER                      (0x00000400)
#define LGAC_CLUTCH                         (0x00000800)
#define LGAC_GATED_SHIFTER                  (0x00001000)
#define LGAC_SHIFTER_HANDLE_POSITION        (0x00002000)
#define LGAC_B_ROCKERS                      (0x00004000)
#define LGAC_DIAL                           (0x00008000)
#define LGAC_B_ENTER                        (0x00010000)
#define LGAC_B_HORN                         (0x00020000)
#define LGAC_B_PSBUTTON                     (0x00040000)

// lgDevDeviceDesc.Flags:

    // if this flag is set, then the attached device support playback
    // of force feedback effects
#define LGFLAG_FORCEFEEDBACK                (0x00000001)

    // if this flag is set, then the centering spring, which keeps the
    // device centered, can be turned off by software
#define LGFLAG_CENTERINGDISABLEABLE         (0x00000002)

    // if this flag is set, then the device is capable of an extended
    // multiturn mode (invoke lgSetProperty() to activate this special
    // mode)
#define LGFLAG_MULTITURN                    (0x00000004)

    // if this flag is set, then the device is capable of reporting
    // "pedals connected" vs. "disconnected" state (in lgPosition.MiscState)
#define LGFLAG_PEDALSTATE_AVAILABLE         (0x00000008)


//************************************************************************
// properties
//************************************************************************

// values: LG_TRUE, LG_FALSE, Get/Set
#define LGPROP_CENTERINGENABLED             (0x00000002)

// values: 0..255, Get/Set
#define LGPROP_OVERALLFORCEGAIN             (0x00000003)

// values: LG_TRUE, LG_FALSE, Get/Set
#define LGPROP_MULTITURN                    (0x00000004)

// values: LG_TRUE, LG_FALSE, Get/Set (only if LGAC_B_SHIFTER is
// present)
#define LGPROP_REPORT_SEPARATE_SHIFTERS     (0x00000005)

// values: LG_TRUE, LG_FALSE, Get only
#define LGPROP_GATED_SHIFTER                (0x00000006)

// values: 0..CELL_MAX_PADS-1, Get only
#define LGPROP_CELLPAD_PORT_NUMBER          (0x00000007)

// values: LG_TRUE, LG_FALSE, Get/Set (default LG_FALSE) - be VERY
// VERY careful with this, you will have to check for external pedal
// presence for the original Driving Force wheel
#define LGPROP_REPORT_DRIVING_FORCE_PADDLES_AS_B0_AND_B1 (0x00000009)


#define LG_TRUE                             (1==1)
#define LG_FALSE                            (!LG_TRUE)


//************************************************************************
// lgDevForceEffect et. al.
//************************************************************************

typedef struct lgDevEnvelopeParams {
    rage::u32 AttackTime;       // in milliseconds (1E-3 seconds)
    rage::u32 AttackLevel;      // 0..255
    rage::u32 FadeTime;         // in milliseconds (1E-3 seconds)
    rage::u32 FadeLevel;        // 0..255
} lgDevEnvelopeParams;

typedef struct lgDevConstantForceParams {
    int  Magnitude;        // -255..0..255
    rage::u32 Direction;        // 0..359 degrees, 0 == straight north,
                                   // clockwise
    lgDevEnvelopeParams envelope;
} lgDevConstantForceParams;

typedef struct lgDevRampForceParams {
    int  MagnitudeStart;   // -255..0..255 - starting magnitude
    int  MagnitudeEnd;     // -255..0..255 - ending magnitude
    rage::u32 Direction;        // 0..359 degrees, 0 == straight north,
                               // clockwise
} lgDevRampForceParams;

typedef struct lgDevPeriodicForceParams {
    rage::u32 Magnitude; // 0..255
    rage::u32 Direction; // 0..359 degrees, 0 == straight north,
                        // clockwise
    rage::u32 Period;    // in milliseconds (1E-3 seconds)
    rage::u32 Phase;     // 0..359 degree, 0 == no phase shift
    int  Offset;    // -255..0..255, 0 == no offset
    lgDevEnvelopeParams envelope;
} lgDevPeriodicForceParams;

typedef struct lgDevConditionForceParams {
    int  Offset;           // -32767..0..32767, 0 = no offset
    rage::u32 Deadband;         // 0..65535
    rage::u32 SaturationNeg;    // 0..255, left or upper side
    rage::u32 SaturationPos;    // 0..255, right or lower side
    int  CoefficientNeg;   // -255..0..255, left or upper side
    int  CoefficientPos;   // -255..0..255, right or lower side
} lgDevConditionForceParams;

typedef struct lgDevForceEffect {
    rage::u8  Type;       // see below
    rage::u32 Duration;   // in milliseconds (1E-3 seconds), or
                         // LGDURATION_INFINITE
    rage::u32 StartDelay; // in milliseconds (1E-3 seconds)
    union {
        struct lgDevConstantForceParams constant;
        struct lgDevRampForceParams ramp;
        struct lgDevPeriodicForceParams periodic;
        struct lgDevConditionForceParams condition[2];
    } p;
} lgDevForceEffect;

// lgDevForceEffect.Type:
    // constant
#define LGTYPE_CONSTANT                     (0x00)
    // ramp
#define LGTYPE_RAMP                         (0x01)
    // periodics
#define LGTYPE_SINE                         (0x02)
#define LGTYPE_SQUARE                       (0x03)
#define LGTYPE_TRIANGLE                     (0x04)
#define LGTYPE_SAWTOOTHUP                   (0x05)
#define LGTYPE_SAWTOOTHDOWN                 (0x06)
// conditions
#define LGTYPE_SPRING                       (0x07)
#define LGTYPE_DAMPER                       (0x08)

// lgDevForceEffect.Duration:
#define LGDURATION_INFINITE                 (0xffffffff)

// diverse.Magnitude:
#define LGMAGNITUDE_MAX                     (255)


//************************************************************************
// Miscellaneous
//************************************************************************


// an invalid device value
#define LG_INVALID_DEVICE                   (-1)

// an invalid effectid value
#define LG_INVALID_EFFECTID                 (-1)

// maximum of effects that can be passed to the Multi.. calls
#define LG_MAX_MULTIEFFECTS                 (16)

// maximum of reads that can be passed to the MultiRead calls
#define LG_MAX_MULTIREAD                    (4)

// lgDevRead/lgDevARead modes
#define LGREADMODE_BUFFERED                 (0)
#define LGREADMODE_IMMEDIATE                (1)

// lgDevEnumHint hints
#define LGHINT_NOENUMNEEDED                 (0)
#define LGHINT_ENUMNEEDED                   (1)

// lgDevSetSysutilMapping stuff
typedef struct lgDevSysutilMapEntry {
    rage::u16 lgDevButton;     // LGBUTTON_BUTTON0 .. LGBUTTON_BUTTON15
    rage::u8  CellPadByte;      // CELL_PAD_BTN_OFFSET_DIGITAL1/2
    rage::u8  CellPadBit;       // CELL_PAD_CTRL_*
} lgDevSysutilMapEntry;



//************************************************************************
// Prototypes
//************************************************************************

int lgDevInit(const lgDevInitParams *initparams);
int lgDevDeInit(void);

int lgDevSetSysutilMapping(const lgDevSysutilMapEntry *entries);

int lgDevEnumHint(int *hint);
int lgDevEnumerate(int index, lgDevDeviceDesc *description);

int lgDevOpen(int index, int *device);
int lgDevClose(int device);

int lgDevGetDeviceProperty(int device, int property, int *value);
int lgDevSetDeviceProperty(int device, int property, int value);

int lgDevRead(int device, int readmode, lgDevPosition *position);
int lgDevReadMulti(int devicecount, const int *devices, const int *readmodes, lgDevPosition *positions);

int lgDevDownloadForceEffect(int device, int *effectid, const lgDevForceEffect *forceeffect);
int lgDevUpdateForceEffect(int effectid, const lgDevForceEffect *forceeffect);
int lgDevStartForceEffect(int effectid);
int lgDevStartForceEffectMulti(int numeffects, const int *effectids);
int lgDevStopForceEffect(int effectid);
int lgDevStopForceEffectMulti(int numeffects, const int *effectids);
int lgDevDestroyForceEffect(int effectid);

int lgDevSetThreadCPU(int threadCPU);
int lgDev360Update();

//************************************************************************
// Error codes
//************************************************************************

#define LG_SUCCESS                          (0x00000000)

#define LG_ERROR                            (0x80000000)

#define LGERR_NO_MORE_DEVICES               (0x80000002)

#define LGERR_INVALID_DEVICETYPE            (0x80000003)

#define LGERR_INVALID_PARAMETER             (0x80000004)

#define LGERR_ALREADY_OPENED                (0x80000005)

#define LGERR_DEVICE_LOST                   (0x80000006)

#define LGERR_OUT_OF_MEMORY                 (0x80000007)

#define LGERR_ALREADY_INITIALIZED           (0x80000008)

#define LGERR_NOT_INITIALIZED               (0x80000009)

#define LGERR_USBD_REGISTRATION_ERROR       (0x80000010)



// quick test against error condition
#define LGSUCCEEDED(x)                      (0 == ((x) & LG_ERROR))
#define LGFAILED(x)                         (0 != ((x) & LG_ERROR))


#if defined(_LANGUAGE_C_PLUS_PLUS)||defined(__cplusplus)||defined(c_plusplus)
}
#endif

#endif // _LIBLGFF_H_INCLUDED_

//** end of liblgFF.h ***************************************************
