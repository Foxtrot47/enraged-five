
#include "input/input.h"

#if HAVE_FFWHEEL

/****h* EZ.Wheel.Wrapper/LGWheels [3.01]
* NAME
*   EZ Wheel Wrapper
* COPYRIGHT
*   The Logitech EZ Wheel Wrapper, including all acompanying documentation, is protected by intellectual property laws. All rights not
*   expressly granted by Logitech are reserved.
* PURPOSE
*   The EZ Wheel Wrapper is an addition to the Logitech USB Devices Library (force feedback SDK on ps2-pro.com). It enables to dramatically
*   shorten development time and improve implementation quality for the Logitech USB force feedback wheel support.
*   The wrapper comes with a very intuitive and easy to use interface which enables to read the wheel's axes and buttons, and also to create all
*   the force feedback effects that are necessary for a good and complete implementation. See the following files to get started:
*       - readme.txt: tells you how to get started
*       - Sample_In-game_Implementation.cpp: shows line by line how to use the EZ Wheel Wrapper's interface to do a complete implementation for
*         Logitech force feedback wheels in your game.
*       - main.cpp: demonstrates force feedback effects. Just compile, run and plug in a Logitech USB wheel. See usage at top of main.cpp.
*   For more details see the force feedback SDK documentation which is part of the Logitech USB Devices Library package (liblgdev.pdf,
*   WheelProgramming.pfd).
* AUTHOR
*   Christophe Juncker
* CREATION DATE
*   10/10/2002
* MODIFICATION HISTORY
*   v3.02 - 09/2004 (tom)
*       - added softstop
*   v3.01 - 01/2004
*       - added multiturn methods
*       - put all error messages between #ifdef's
*       - added global variables file (LogiGlobals.h)
*   v2.01 - 08/2003
*       - eliminated DPad commands and merged them into button commands
*       - made asynchronous methods more effective in order to optimize processor usage
*       - made filenames more unique to avoid name clashes
*       - made definition names more unique to avoid clashes
*       - added version display during initialization
*       - updated Sample_In-game_Implementation.cpp
*       - added Optimization_Tricks.txt to show how to best take advantage of asynchronous methods
*       - eliminated autocalibration force method because it should not be used with Driving Force Pro
*       - updated documentation in doc folder
*       
*   v1.2 - 06/2003
*       - enhanced enumeration process to use minimal processing time when no wheels are plugged in
*******
*/

/****v* EZ.Wheel.Wrapper/LGWheels::Position
 * NAME
 *  lgDevPosition Position[LG_MAX_CHANNELS] -- contains position information for connected Logitech USB wheels.
 * PURPOSE
 *  Contains information about all axis and button values of connected Logitech USB wheels. See lgDevPosition in Logitech SDK's liblgdev.pdf 
 *  documentation file to see all details about available axes and buttons.
 * SEE ALSO
 *  LGWheels::ReadAll
 *  SDK docs: lgDevPosition in liblgdev.pdf
 ******
 */

/****v* EZ.Wheel.Wrapper/LGWheels::NonLinearWheel
 * NAME
 *  int NonLinearWheel[LG_MAX_CHANNELS][256] -- Lookup table that contains non-linear values for the wheel's axis reading.
 * PURPOSE
 *  Offer a set of non-linear values corresponding to wheel axis input values. If the non-linear values are used instead of the wheel's axis
 *  values the wheel will feel less / more sensitive around center position. This is not necessary for the multiturn wheel where you can use the wheel's
 *	raw values and directly translate them into your physics engine.
 * SEE ALSO
 *  LGWheels::GenerateNonLinValues
 ******
 */

#include "LGWheels.h"
#include <string.h>

/****f* EZ.Wheel.Wrapper/LGWheels::LGWheels
 * NAME
 *  LGWheels::LGWheels -- Constructor
 * FUNCTION
 *  Loads necessary IRX files and does necessary initialization.
 ******
 */
LGWheels::LGWheels()
{
    InitVars();
    // EZ Wheel Wrapper version number. Please do not remove. You may remove all other ones by commenting out _DEBUG_BASIC.
    printf("EZ Wheel Wrapper v3.02\n");
}

LGWheels::~LGWheels()
{
}

void LGWheels::InitVars()
{
    // re-initialize variables
    for (int ii = 0; ii < LG_MAX_FORCES; ii++)
    {
        for (int jj = 0; jj < LG_MAX_CHANNELS; jj++)
        {
            condition.EffectID[jj][ii] = LG_INVALID_EFFECTID;
            condition.Playing[jj][ii] = false;
            constant.EffectID[jj][ii] = LG_INVALID_EFFECTID;
            constant.Playing[jj][ii] = false;
            periodic.EffectID[jj][ii] = LG_INVALID_EFFECTID;
            periodic.Playing[jj][ii] = false;
            ramp.EffectID[jj][ii] = LG_INVALID_EFFECTID;
            ramp.Playing[jj][ii] = false;
        }
    }

    for (int ii = 0; ii < LG_MAX_CHANNELS; ii++)
    {
        damperWasPlaying[ii] = false;
        springWasPlaying[ii] = false;
        IsAirborne[ii] = false;

        for (int jj = 0; jj < LG_NUMBER_FORCE_EFFECTS; jj++)
        {
            wasPlayingBeforeAirborne[ii][jj] = false;
        }
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::ReadAll
 * NAME
 *  void LGWheels::ReadAll() -- Read all connected Logitech USB wheels
 *FUNCTION
 *  Enumerate wheels if necessary.
 *  Read all connected wheels and populate LGWheels::Position structure.
 *  Automatically assign channel numbers according to the wheels' positions in the USB tree. Channel 0 corresponds to the first wheel in the USB
 *  tree. Channel 1 to the second.
 * SEE ALSO
 *  LGWheels::Position
 *  liblgdev.pdf part of the SDK documentation to find out more about the USB tree.
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::ReadAll()
{
    GetState();
    for (int ii = 0; ii < LG_MAX_CHANNELS; ii++)
    {
        if (IsConnected(ii))
            ReadPort(ii);
    }
}

void LGWheels::GetState()
{
    if (wheels.EnumIsNeeded())
    {
#ifdef _DEBUG_BASIC
        printf("devices changed - re-enumerating\n");
#endif
        wheels.DestroyDevices();

        InitVars();
        wheels.EnumerateDevices();
    }
}

void LGWheels::ReadPort(int channel)
{
    wheels.ReadPort(channel);
    memcpy(&Position, wheels.Position, sizeof(Position));
}

void LGWheels::StopForce(int channel, int forceType)
{
    switch (forceType)
    {
    case LG_SPRING:
        if (IsPlaying(channel, LG_SPRING))
        {
            condition.Stop(channel, LG_CONDITION_0);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop spring force but none is currently playing.\n");
#endif
        }
        break;
    case LG_SOFTSTOP:
        if (IsPlaying(channel, LG_SOFTSTOP))
        {
            condition.Stop(channel, LG_CONDITION_3);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop softstop force but none is currently playing.\n");
#endif
        }
        break;
    case LG_CONSTANT:
        if (IsPlaying(channel, LG_CONSTANT))
        {
            constant.Stop(channel, LG_CONSTANT_0);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop constant force but none is currently playing.\n");
#endif
        }
        break;
    case LG_DAMPER:
        if (IsPlaying(channel, LG_DAMPER))
        {
            condition.Stop(channel, LG_CONDITION_1);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop damper force but none is currently playing.\n");
#endif
        }
        break;
    case LG_SIDE_COLLISION:
        if (IsPlaying(channel, LG_SIDE_COLLISION))
        {
            constant.Stop(channel, LG_CONSTANT_1);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop side collision force but none is currently playing.\n");
#endif
        }
        break;
    case LG_FRONTAL_COLLISION:
        if (IsPlaying(channel, LG_FRONTAL_COLLISION))
        {
            periodic.Stop(channel, LG_PERIODIC_0);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop frontal collision force but none is currently playing.\n");
#endif
        }
        break;
    case LG_DIRT_ROAD:
        if (IsPlaying(channel, LG_DIRT_ROAD))
        {
            periodic.Stop(channel, LG_PERIODIC_1);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop dirt road effect but none is currently playing.\n");
#endif
        }
        break;
    case LG_BUMPY_ROAD:
        if (IsPlaying(channel, LG_BUMPY_ROAD))
        {
            periodic.Stop(channel, LG_PERIODIC_2);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop bumpy road effect but none is currently playing.\n");
#endif
        }
        break;
    case LG_SLIPPERY_ROAD:
        if (IsPlaying(channel, LG_SLIPPERY_ROAD))
        {
            condition.Stop(channel, LG_CONDITION_2);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop slippery road effect but none is currently playing.\n");
#endif
        }

        // check if damper had been playing before. If such is the case, start it again. This would happen in case
        // the damper effect was stopped by the slippery surface effect. Indeed we only want one effect of type damper
        // to be played at any time.
        if (damperWasPlaying[channel])
        {
            PlayDamperForce(channel, DamperForceParams[channel].coefficient);
            condition.Playing[channel][LG_CONDITION_1] = true;
            damperWasPlaying[channel] = false;
        }

        // check if spring had been playing before. If such is the case, start it again. This would happen in case
        // the spring effect was stopped by the slippery surface effect.
        // to be played at any time.
        if (springWasPlaying[channel])
        {
            PlaySpringForce(channel, SpringForceParams[channel].offset, SpringForceParams[channel].saturation,
                            SpringForceParams[channel].coefficient);
            condition.Playing[channel][LG_CONDITION_0] = true;
            springWasPlaying[channel] = false;
        }

        break;
    case LG_SURFACE_EFFECT:
        if (IsPlaying(channel, LG_SURFACE_EFFECT))
        {
            periodic.Stop(channel, LG_PERIODIC_3);
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop surface effect but none is currently playing.\n");
#endif
        }
        break;
    case LG_CAR_AIRBORNE:
        if (IsPlaying(channel, LG_CAR_AIRBORNE))
        {
            IsAirborne[channel] = false;

            if (wasPlayingBeforeAirborne[channel][LG_SPRING] == true)
            {
                PlaySpringForce(channel, SpringForceParams[channel].offset, SpringForceParams[channel].saturation,
                                SpringForceParams[channel].coefficient);
            }
            if (wasPlayingBeforeAirborne[channel][LG_CONSTANT] == true)
            {
                PlayConstantForce(channel, ConstantForceParams[channel].magnitude, ConstantForceParams[channel].direction);
            }
            if (wasPlayingBeforeAirborne[channel][LG_DAMPER] == true)
            {
                PlayDamperForce(channel, DamperForceParams[channel].coefficient);
            }
            if (wasPlayingBeforeAirborne[channel][LG_DIRT_ROAD] == true)
            {
                PlayDirtRoadEffect(channel, DirtRoadParams[channel].magnitude);
            }
            if (wasPlayingBeforeAirborne[channel][LG_BUMPY_ROAD] == true)
            {
                PlayBumpyRoadEffect(channel, BumpyRoadParams[channel].magnitude);
            }
            if (wasPlayingBeforeAirborne[channel][LG_SLIPPERY_ROAD] == true)
            {
                PlaySlipperyRoadEffect(channel, SlipperyRoadParams[channel].magnitude);
            }
            if (wasPlayingBeforeAirborne[channel][LG_SURFACE_EFFECT] == true)
            {
                PlaySurfaceEffect(channel, SurfaceEffectParams[channel].type, SurfaceEffectParams[channel].magnitude,
                                  SurfaceEffectParams[channel].period);
            }

            // re-initialize variables
            for (int jj = 0; jj < LG_NUMBER_FORCE_EFFECTS; jj++)
            {
                wasPlayingBeforeAirborne[channel][jj] = false;
            }
        } else
        {
#ifdef _DEBUG
            printf("WARNING: trying to stop car airborne effect but none is currently playing.\n");
#endif
        }
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::IsConnected
 * NAME
 *  bool LGWheels::IsConnected(int channel) -- Check if a wheel is connected at the specified channel.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * RETURN VALUE 
 *  TRUE if a Logitech wheel is connected.
 * SEE ALSO
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
bool LGWheels::IsConnected(int channel)
{
    return wheels.IsConnected(channel);
}

/****f* EZ.Wheel.Wrapper/LGWheels::IsPlaying
 * NAME
 *  bool LGWheels::IsPlaying(int channel, int forceType) -- Check if a certain force effect is currently playing.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  forceType   - the type of the force that we want to check to see if it is playing.
 *                Possible types are: LG_SPRING, LG_CONSTANT, LG_DAMPER, LG_SIDE_COLLISION, LG_FRONTAL_COLLISION, LG_DIRT_ROAD, LG_BUMPY_ROAD, LG_SLIPPERY_ROAD,
 *                LG_SURFACE_EFFECT, LG_CAR_AIRBORNE.
 * RETURN VALUE 
 *  TRUE if the force is playing
 * SEE ALSO
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
bool LGWheels::IsPlaying(int channel, int forceType)
{
    switch (forceType)
    {
    case LG_SPRING:
        if (condition.Playing[channel][LG_CONDITION_0])
        {
            return true;
        }
        break;
    case LG_SOFTSTOP:
        if (condition.Playing[channel][LG_CONDITION_3])
        {
            return true;
        }
        break;
    case LG_CONSTANT:
        if (constant.Playing[channel][LG_CONSTANT_0])
        {
            return true;
        }
        break;
    case LG_DAMPER:
        if (condition.Playing[channel][LG_CONDITION_1])
        {
            return true;
        }
        break;
    case LG_SIDE_COLLISION:
        if (constant.Playing[channel][LG_CONSTANT_1])
        {
            return true;
        }
        break;
    case LG_FRONTAL_COLLISION:
        if (periodic.Playing[channel][LG_PERIODIC_0])
        {
            return true;
        }
        break;
    case LG_DIRT_ROAD:
        if (periodic.Playing[channel][LG_PERIODIC_1])
        {
            return true;
        }
        break;
    case LG_BUMPY_ROAD:
        if (periodic.Playing[channel][LG_PERIODIC_2])
        {
            return true;
        }
        break;
    case LG_SLIPPERY_ROAD:
        if (condition.Playing[channel][LG_CONDITION_2])
        {
            return true;
        }
        break;
    case LG_SURFACE_EFFECT:
        if (periodic.Playing[channel][LG_PERIODIC_3])
        {
            return true;
        }
        break;
    case LG_CAR_AIRBORNE:
        if (IsAirborne[channel] == true)
        {
            return true;
        }
        break;
    }
    return false;
}

/****f* EZ.Wheel.Wrapper/LGWheels::ButtonTriggered
 * NAME
 *  bool LGWheels::ButtonTriggered(int channel, int buttonMask) -- Check if a certain button was triggered.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  buttonMask  - the mask of the button that we want to check.
 *                Possible masks are:
 *                Wheel class A: LG_BUTTON_BUTTON0 through LG_BUTTON_BUTTON15
 *                Wheel class B: LG_BUTTON_SQUARE, LG_BUTTON_CROSS, LG_BUTTON_CIRCLE, LG_BUTTON_TRIANGLE, LG_BUTTON_START, LG_BUTTON_SELECT, LG_BUTTON_L1,
 *                LG_BUTTON_R1, LG_BUTTON_L2, LG_BUTTON_R2, LG_DPAD_UP, LG_DPAD_DOWN, LG_DPAD_RIGHT, LG_DPAD_LEFT.
 * RETURN VALUE 
 *  TRUE if the button was triggered
 * SEE ALSO
 *  LGWheels::ButtonIsPressed
 *  LGWheels::ButtonReleased
 *  SDK docs: liblgdev.pdf to see button assignments for Wheel class A.
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
bool LGWheels::ButtonTriggered(int channel, int buttonMask)
{

    return wheels.ButtonTriggered(channel, buttonMask);
}

/****f* EZ.Wheel.Wrapper/LGWheels::ButtonReleased
 * NAME
 *  bool LGWheels::ButtonReleased(int channel, int buttonMask) -- Check if a certain button was released.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  buttonMask  - the mask of the button that we want to check.
 *                Possible masks are:
 *                Wheel class A: LG_BUTTON_BUTTON0 through LG_BUTTON_BUTTON15
 *                Wheel class B: LG_BUTTON_SQUARE, LG_BUTTON_CROSS, LG_BUTTON_CIRCLE, LG_BUTTON_TRIANGLE, LG_BUTTON_START, LG_BUTTON_SELECT, LG_BUTTON_L1,
 *                LG_BUTTON_R1, LG_BUTTON_L2, LG_BUTTON_R2, LG_DPAD_UP, LG_DPAD_DOWN, LG_DPAD_RIGHT, LG_DPAD_LEFT.
 * RETURN VALUE 
 *  TRUE if the button was released.
 * SEE ALSO
 *  LGWheels::ButtonIsPressed
 *  LGWheels::ButtonTriggered
 *  SDK docs: liblgdev.pdf to see button assignments for Wheel class A.
 ******
 */
bool LGWheels::ButtonReleased(int channel, int buttonMask)
{
    return wheels.ButtonReleased(channel, buttonMask);
}

/****f* EZ.Wheel.Wrapper/LGWheels::ButtonIsPressed
 * NAME
 *  bool LGWheels::ButtonIsPressed(int channel, int buttonMask) -- Check if a certain button is being pressed.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  buttonMask  - the mask of the button that we want to check.
 *                Possible masks are:
 *                Wheel class A: LG_BUTTON_BUTTON0 through LG_BUTTON_BUTTON15
 *                Wheel class B: LG_BUTTON_SQUARE, LG_BUTTON_CROSS, LG_BUTTON_CIRCLE, LG_BUTTON_TRIANGLE, LG_BUTTON_START, LG_BUTTON_SELECT, LG_BUTTON_L1,
 *                LG_BUTTON_R1, LG_BUTTON_L2, LG_BUTTON_R2, LG_DPAD_UP, LG_DPAD_DOWN, LG_DPAD_RIGHT, LG_DPAD_LEFT.
 * RETURN VALUE 
 *  TRUE if the button is being pressed.
 * SEE ALSO
 *  LGWheels::ButtonTriggered
 *  LGWheels::ButtonReleased
 *  SDK docs: liblgdev.pdf to see button assignments for Wheel class A.
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
bool LGWheels::ButtonIsPressed(int channel, int buttonMask)
{
    return wheels.ButtonIsPressed(channel, buttonMask);
}

/****f* EZ.Wheel.Wrapper/LGWheels::WheelAMenuIsPressed
 * NAME
 *  bool LGWheels::WheelAMenuIsPressed(int channel, int direction) -- Check if the user wants to navigate the menu using the wheel's right and left
 *  motion as well as the accelerator and brake pedals.
 * FUNCTION
 *  Enable menu navigation with class A wheels  which do not have the DPad.
 *  The user may navigate left or right by turning the wheel left or right.
 *  The user may also navigate up by pressing the accelerator pedal and down by pressing the brake pedal.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  direction   - the mask of the direction of the wheel that we want to check.
 *                Possible masks are: LG_MENU_UP, LG_MENU_DOWN, LG_MENU_RIGHT, LG_MENU_LEFT.
 * RETURN VALUE 
 *  TRUE if the direction intended by the user is being given (wheel, accelerator or brake pedal are over a certain threshold).
 * NOTES
 *  This function is only valid for class A wheels (class B wheels may navigate the menus using the built-in DPad).
 * SEE ALSO
 *  LGWheels::WheelAMenuTriggered
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
bool LGWheels::WheelAMenuIsPressed(int channel, int direction)
{
    return wheels.WheelAMenuIsPressed(channel, direction);
}

/****f* EZ.Wheel.Wrapper/LGWheels::WheelAMenuTriggered
 * NAME
 *  bool LGWheels::WheelAMenuTriggered(int channel, int direction) -- Check if the user wants to navigate the menu using the wheel's right and left
 *  motion as well as the accelerator and brake pedals.
 * FUNCTION
 *  Enable menu navigation with class A wheels which do not have the DPad.
 *  The user may navigate left or right by turning the wheel left or right.
 *  The user may also navigate up by pressing the accelerator pedal and down by pressing the brake pedal.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  direction   - the mask of the direction of the wheel that we want to check.
 *                Possible masks are: LG_MENU_UP, LG_MENU_DOWN, LG_MENU_RIGHT, LG_MENU_LEFT.
 * RETURN VALUE 
 *  TRUE if the direction intended by the user was triggered. (wheel, accelerator or brake pedal went over a certain threshold).
 * NOTES
 *  This function is only valid for class A wheels (class B wheels may navigate the menus using the built-in DPad).
 * SEE ALSO
 *  LGWheels::WheelAMenuIsPressed
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
bool LGWheels::WheelAMenuTriggered(int channel, int direction)
{
    return wheels.WheelAMenuTriggered(channel, direction);
}

/****f* EZ.Wheel.Wrapper/LGWheels::IsWheelClassA
 * NAME
 *  bool LGWheels::IsWheelClassA(int channel) -- Check if we have a class A wheel (no game pad type buttons available).
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * RETURN VALUE 
 *  TRUE if the connected wheel is a class A wheel.
 * SEE ALSO
 *  LGWheels::IsWheelClassB
 ******
 */
bool LGWheels::IsWheelClassA(int channel)
{
    return wheels.IsWheelClassA(channel);
}

/****f* EZ.Wheel.Wrapper/LGWheels::IsWheelClassB
 * NAME
 *  bool LGWheels::IsWheelClassB(int channel) -- Check if we have a class B wheel (game pad type buttons available on the wheel).
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * RETURN VALUE 
 *  TRUE if the connected wheel is a class B wheel.
 * SEE ALSO
 *  LGWheels::IsWheelClassA
 ******
 */
bool LGWheels::IsWheelClassB(int channel)
{
    return wheels.IsWheelClassB(channel);
}


/****f* EZ.Wheel.Wrapper/LGWheels::IsMultiturnCapable
 * NAME
 *  bool LGWheels::IsMultiturnCapable(int channel) -- Check if the connected wheel is multiturn capable.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * RETURN VALUE 
 *  TRUE if the connected wheel is multiturn capable.
 * SEE ALSO
 *  LGWheels::IsMultiturnEnabled
 *  LGWheels::SetMultiturn
 ******
 */
bool LGWheels::IsMultiturnCapable(int channel)
{
    return wheels.IsMultiturnCapable(channel);
}

/****f* EZ.Wheel.Wrapper/LGWheels::IsMultiturnEnabled
 * NAME
 *  bool LGWheels::IsMultiturnEnabled(int channel) -- Check if the connected wheel has multiturn enabled.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * RETURN VALUE 
 *  TRUE if multiturn is enabled for the connected wheel.
 * SEE ALSO
 *  LGWheels::IsMultiturnCapable
 *  LGWheels::SetMultiturn
 ******
 */
bool LGWheels::IsMultiturnEnabled(int channel)
{
    return wheels.IsMultiturnEnabled(channel);
}

/****f* EZ.Wheel.Wrapper/LGWheels::SetMultiturn
 * NAME
 *  bool LGWheels::SetMultiturn(int channel, int value) -- set multiturn mode on or off.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  value       - LG_TRUE to enable multiturn, LG_FALSE to disable multiturn.
 * SEE ALSO
 *  LGWheels::IsMultiturnCapable
 *  LGWheels::IsMultiturnEnabled
 ******
 */
void LGWheels::SetMultiturn(int channel, int value)
{
    wheels.SetMultiturn(channel, value);
}

/****f* EZ.Wheel.Wrapper/LGWheels::GetMultiturn
 * NAME
 *  int LGWheels::GetMultiturn(int channel) -- get multiturn mode on or off (based on last set value)
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::IsMultiturnCapable
 *  LGWheels::IsMultiturnEnabled
 *  LGWheels::SetMultiturn
 ******
 */
int LGWheels::GetMultiturn(int channel)
{
    return wheels.GetMultiturn(channel);
}

// mjc
int LGWheels::GetDeviceNumber(int channel)
{
    return wheels.GetDeviceNumber(channel);
}

/****f* EZ.Wheel.Wrapper/LGWheels::SetOverallForceGain
 * NAME
 *  void LGWheels::SetOverallForceGain(int channel, int value) -- Set overall gain of force feedback.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  value       - value to which the gain should be set. Range is 0 - 255. 0 gives minimum force gain which will result in not feeling any forces
 *                at all even if they are playing. 255 gives maximum force gain.
 * SEE ALSO
 *  LGWheels::GetOverallForceGain
 ******
 */
void LGWheels::SetOverallForceGain(int channel, int value)
{
    force.SetOverallForceGain(wheels.WheelHandles[channel], value);
}

/****f* EZ.Wheel.Wrapper/LGWheels::GetOverallForceGain
 * NAME
 *  int LGWheels::GetOverallForceGain(int channel) -- Get overall gain of force feedback.
 * INPUTS
 *  channel     - number of the channel of the wheel that we want to check.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * RETURN VALUE
 *  Value between 0 and 255 corresponding to the overall gain that is currently set.
 * SEE ALSO
 *  LGWheels::SetOverallForceGain
 ******
 */
int LGWheels::GetOverallForceGain(int channel)
{
    return force.GetOverallForceGain(wheels.WheelHandles[channel]);
}

/****f* EZ.Wheel.Wrapper/LGWheels::GenerateNonLinValues
 * NAME
 *  void LGWheels::GenerateNonLinValues(int channel, int nonLinCoeff) -- Generate non-linear values for the wheel's axis.
 * FUNCTION
 *	This method is aimed at all gaming wheels that are not multiturn capable. The game should check if the connected wheel is a
 *	multiturn capable wheel or not. If the wheel is multiturn capable and is set to multiturn mode then the GenerateNonLinValues method should not
 *	be used. For all other cases the following applies.
 *	Gaming wheels have very different behavior from real steering wheels. The reason is that they only do up to 3/4ths of a turn lock to lock, compared
 *	to about 3 turns for a real car. This directly affects the steering ratio (15:1 to 20:1 for a real car, but only 4:1 for a gaming wheel!).
 *	Because of this very short steering ratio, the gaming wheel will feel highly sensitive which may make game play very difficult. Especially it
 *	may be difficult to drive in a straight line at speed (tendency to swerve back and forth). 
 *	One way to get around this problem is to use a sensitivity curve.
 *	This is a curve that defines the sensitivity of the wheel depending on speed. This type of curve is usually used for game pads to make up for
 *	their low physical range. The result of applying such a curve is that at high speed the car's wheels will physically turn less than if the car
 *	is moving very slowly. For example the car's wheels may turn 60 degrees lock to lock at low speed but only 10 degrees lock to lock at higher
 *	speeds. If you calculate the resulting steering ratio for 10 degrees lock to lock you find that if you use a steering wheel that turns 180
 *	degrees lock to lock the ratio is equal to 180/10 = 18, which corresponds to a real car's steering ratio.
 *	If the sensitivity curve has been implemented for the wheel, adding a non-linear curve probably is not necessary. But you may find that even
 *	after applying a sensitivity curve, the car still feels a little twitchy on a straight line when driving fast. This may be because in your game
 *	you need more than 10 degrees lock to lock even at high speeds. Or maybe the car is moving at very high speeds where even a normal steering
 *	ratio is not good enough to eliminate high sensitivity.
 *	The best way at this point is to add a non-linear curve on top of the sensitivity curve.
 *	The effect of the non-linear curve is that around center position the wheel will be less sensitive. Yet at locked position left or right the
 *	car's wheels will turn the same amount of degrees as without the non-linear response curve. Therefore the car will become more controllable on
 *	a straight line and game-play will be improved.
 *	This method lets you define a non-linearity coefficient which will determine how strongly non-linear the curve will be. When running the method
 *	it will generate a mapping table in the form of an array (LGWheels::NonLinearWheel). For each of the 256 entries in this array there will be a
 *	corresponding non-linear value which can be used as the wheel's axis position instead of the raw value which figures in the LGWheels::Position
 *	structure.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  nonLinCoeff - value representing how much non-linearity should be applied. Range is -100 to 100. 0 = linear curve, 100/-100 = maximum non-linear 
 *                curve.
 * SEE ALSO
 *  LGWheels::NonLinearWheel
 *  LGWheels::Position
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::GenerateNonLinValues(int channel, int nonLinCoeff)
{
    wheels.GenerateNonLinValues(channel, nonLinCoeff);
    memcpy(&NonLinearWheel, &wheels.NonLinearWheel, sizeof(NonLinearWheel));
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlaySpringForce
 * NAME
 *  void LGWheels::PlaySpringForce(int channel, int offset, int saturation, int coefficient) -- Play the spring force.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  offset      - Specifies the center of the spring force effect.
 *                Valid range is -255 to 255. Specifying 0 centers the spring. Any values outside this range are silently clamped.
 *  saturation  - Specify the level of saturation of the spring force effect. The saturation stays constant after a certain deflection from the
 *                center of the spring. It is comparable to a magnitude.
 *                Valid ranges are 0 to 255. Any value higher than 255 is silently clamped.
 *  coefficient - Specify the slope of the effect strength increase relative to the amount of deflection from the center of the condition. Higher
 *                values mean that the saturation level is reached sooner.
 *                Valid ranges are -255 to 255. Any value outside the valid range is silently clamped.
 * SEE ALSO
 *  LGWheels::StopSpringForce
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::PlaySpringForce(int channel, int offset, int saturation, int coefficient)
{
    int ret = LG_SUCCESS;

    // only play spring force if slippery road effect is not playing (slippery effect is a damper with negative coefficient and takes
    // precedence on the spring)
    if (IsPlaying(channel, LG_SLIPPERY_ROAD))
        return;

    //if playing
    //	check if same parameters
    //	if same, do nothing
    //	if different, update
    //if NOT playing
    //	if never been downloaded
    //		download and start
    //	else if has been downloaded before
    //		check parameters
    //		if same
    //			start force
    //		else if not same
    //			update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsAirborne[channel] == false)
        {
            if (IsPlaying(channel, LG_SPRING))
            {
                if (SameSpringForceParams(channel, offset, saturation, coefficient))
                {
                    // Do nothing
                } else
                {
                    ret = condition.UpdateForce(channel, LG_CONDITION_0, LGTYPE_SPRING, LGDURATION_INFINITE, 0, offset, 0, saturation,
                                                saturation, coefficient, coefficient);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        SpringForceParams[channel].offset = offset;
                        SpringForceParams[channel].saturation = saturation;
                        SpringForceParams[channel].coefficient = coefficient;
                    }
                }
            } else // force is not playing
            {
                // If force has never been downloaded before, download and start it
                if (condition.EffectID[channel][LG_CONDITION_0] == LG_INVALID_EFFECTID)
                {
                    ret = condition.DownloadForce(channel, LG_CONDITION_0, wheels.WheelHandles[channel], LGTYPE_SPRING, LGDURATION_INFINITE,
                                                  0, offset, 0, saturation, saturation, coefficient, coefficient);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        SpringForceParams[channel].offset = offset;
                        SpringForceParams[channel].saturation = saturation;
                        SpringForceParams[channel].coefficient = coefficient;
                    }
                    condition.Start(channel, LG_CONDITION_0);
                } else
                    // The force has been downloaded before but is not playing
                {
                    if (SameSpringForceParams(channel, offset, saturation, coefficient))
                    {
                        condition.Start(channel, LG_CONDITION_0);
                    } else
                    {
                        ret = condition.UpdateForce(channel, LG_CONDITION_0, LGTYPE_SPRING, LGDURATION_INFINITE, 0, offset, 0, saturation,
                                                    saturation, coefficient, coefficient);
                        if (!LGFAILED(ret))
                        {
                            // Fill parameter structure which enables us for next commands to check if we have new
                            // parameters or not
                            SpringForceParams[channel].offset = offset;
                            SpringForceParams[channel].saturation = saturation;
                            SpringForceParams[channel].coefficient = coefficient;
                        }
                        condition.Start(channel, LG_CONDITION_0);
                    }
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::StopSpringForce
 * NAME
 *  void LGWheels::StopSpringForce(int channel) -- stop the spring force.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::PlaySpringForce
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::StopSpringForce(int channel)
{
    StopForce(channel, LG_SPRING);
}

bool LGWheels::SameSpringForceParams(int channel, int offset, int saturation, int coefficient)
{
    return ((SpringForceParams[channel].offset == offset) && (SpringForceParams[channel].saturation == saturation)
            && (SpringForceParams[channel].coefficient == coefficient));
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlaySoftstopForce
 * NAME
 *  void LGWheels::PlaySoftstopForce(int channel, int deadband, int saturation, int coefficient) -- Play a spring force that acts like a soft stop on range-limited wheels.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  deadband     - Specifies the deadband the softstop force effect.
 *                Valid range is 0 to 65535. Specifying 0 leaves zero deadband, 64K max deadband.
 *				  A range in between limits to the appropriate value (i.e., deadband = 32K for a 900 degree wheel results
 *				  in (32K/64K) * 900 degree = 450 degree stops.
 *  saturation  - Specify the level of saturation of the spring force effect. The saturation stays constant after a certain deflection from the
 *                center of the spring. It is comparable to a magnitude.
 *                Valid ranges are 0 to 255. Any value higher than 255 is silently clamped.
 *  coefficient - Specify the slope of the effect strength increase relative to the amount of deflection from the center of the condition. Higher
 *                values mean that the saturation level is reached sooner.
 *                Valid ranges are -255 to 255. Any value outside the valid range is silently clamped.
 * SEE ALSO
 *  LGWheels::StopSoftstopForce
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::PlaySoftstopForce(int channel, int deadband, int saturation, int coefficient)
{
    int ret = LG_SUCCESS;

    // play all the time (no exceptions for slippery road)

    //if playing
    //	check if same parameters
    //	if same, do nothing
    //	if different, update
    //if NOT playing
    //	if never been downloaded
    //		download and start
    //	else if has been downloaded before
    //		check parameters
    //		if same
    //			start force
    //		else if not same
    //			update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsPlaying(channel, LG_SOFTSTOP))
        {
            if (SameSoftstopForceParams(channel, deadband, saturation, coefficient))
            {
                // Do nothing
            } else
            {
				ret = condition.UpdateForce(channel, LG_CONDITION_3, LGTYPE_SPRING, LGDURATION_INFINITE, 0, 0, deadband, saturation,
                                            saturation, coefficient, coefficient);
                if (!LGFAILED(ret))
                {
                    // Fill parameter structure which enables us for next commands to check if we have new
                    // parameters or not
                    SoftstopForceParams[channel].deadband = deadband;
                    SoftstopForceParams[channel].saturation = saturation;
                    SoftstopForceParams[channel].coefficient = coefficient;
                }
            }
        } else // force is not playing
        {
            // If force has never been downloaded before, download and start it
            if (condition.EffectID[channel][LG_CONDITION_3] == LG_INVALID_EFFECTID)
            {
                ret = condition.DownloadForce(channel, LG_CONDITION_3, wheels.WheelHandles[channel], LGTYPE_SPRING, LGDURATION_INFINITE,
											0, 0, deadband, saturation, saturation, coefficient, coefficient);
                if (!LGFAILED(ret))
                {
                    // Fill parameter structure which enables us for next commands to check if we have new
                    // parameters or not
                    SoftstopForceParams[channel].deadband = deadband;
                    SoftstopForceParams[channel].saturation = saturation;
                    SoftstopForceParams[channel].coefficient = coefficient;
                }
                condition.Start(channel, LG_CONDITION_3);
            } else
                // The force has been downloaded before but is not playing
            {
                if (SameSoftstopForceParams(channel, deadband, saturation, coefficient))
                {
                    condition.Start(channel, LG_CONDITION_3);
                } else
                {
					ret = condition.UpdateForce(channel, LG_CONDITION_3, LGTYPE_SPRING, LGDURATION_INFINITE, 0, 0, deadband, saturation,
                                                saturation, coefficient, coefficient);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        SoftstopForceParams[channel].deadband = deadband;
                        SoftstopForceParams[channel].saturation = saturation;
                        SoftstopForceParams[channel].coefficient = coefficient;
                    }
                    condition.Start(channel, LG_CONDITION_3);
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::StopSoftstopForce
 * NAME
 *  void LGWheels::StopSoftStopForce(int channel) -- stop the "softstop" spring force.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::PlaySoftstopForce
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::StopSoftstopForce(int channel)
{
    StopForce(channel, LG_SOFTSTOP);
}

bool LGWheels::SameSoftstopForceParams(int channel, int deadband, int saturation, int coefficient)
{
    return ((SoftstopForceParams[channel].deadband == deadband) && (SoftstopForceParams[channel].saturation == saturation)
            && (SoftstopForceParams[channel].coefficient == coefficient));
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlayConstantForce
 * NAME
 *  void LGWheels::PlayConstantForce(int channel, int magnitude, int direction) -- Play the Constant force.
 * FUNCTION
 *  Tie the steering wheel to the car's physics engine via a vector force. This will give us a centering spring effect, a sliding effect, a feeling
 *  for the car's inertia, and depending on the physics engine it should also give side collisions (wheel jerks in the opposite way of the wall the
 *  car just touched).
 *  The vector force should be deduced from the lateral force measured at the front tires. This vector force should be 0 when at a stop
 *  or driving straight. When driving through a turn or when driving on a banked surface the vector force should have a magnitude that grows in a
 *  proportional way, as well as a direction.
 *  The trick now is to update direction and magnitude of our constant force in every frame to reflect the vector force from the physics engine.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  magnitude   - Specifies the magnitude of the constant force effect. A negative value reverses the direction of the force. It is equivalent of
 *                setting the positive value as magnitude and adding 180 to the direction member.
 *                Valid ranges for magnitudes are -255 to 255. Any values outside the valid range are silently clamped.
 *  direction   - Specifies the direction of the constant force effect in degrees. A value of 90 will produce a force pushing counter-clockwise.
 *                A value of 270 will make the wheel turn clockwise.
 *                Valid ranges for directions are 0 to 359. Any values outside the valid range are silently modulo'd by 360.
 * SEE ALSO
 *  LGWheels::StopConstantForce
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::PlayConstantForce(int channel, int magnitude, int direction)
{
    int ret = LG_SUCCESS;

    //if playing
    //	check if same parameters
    //	if same, do nothing
    //	if different, update
    //if NOT playing
    //	if never been downloaded
    //		download and start
    //	else if has been downloaded before
    //		check parameters
    //		if same
    //			start force
    //		else if not same
    //			update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsAirborne[channel] == false)
        {
            if (IsPlaying(channel, LG_CONSTANT))
            {
                if (SameConstantForceParams(channel, magnitude, direction))
                {
                    // Do nothing
                } else
                {
                    ret = constant.UpdateForce(channel, LG_CONSTANT_0, LGDURATION_INFINITE, 0, magnitude, direction, 0, 0, 0, 0);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        ConstantForceParams[channel].magnitude = magnitude;
                        ConstantForceParams[channel].direction = direction;
                    }
                }
            } else // force is not playing
            {
                // If force has never been downloaded before, download and start it
                if (constant.EffectID[channel][LG_CONSTANT_0] == LG_INVALID_EFFECTID)
                {
                    ret = constant.DownloadForce(channel, LG_CONSTANT_0, wheels.WheelHandles[channel], LGDURATION_INFINITE, 0, magnitude,
                                                 direction, 0, 0, 0, 0);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        ConstantForceParams[channel].magnitude = magnitude;
                        ConstantForceParams[channel].direction = direction;
                    }
                    constant.Start(channel, LG_CONSTANT_0);
                } else
                    // The force has been downloaded before but is not playing
                {
                    if (SameConstantForceParams(channel, magnitude, direction))
                    {
                        constant.Start(channel, LG_CONSTANT_0);
                    } else
                    {
                        ret = constant.UpdateForce(channel, LG_CONSTANT_0, LGDURATION_INFINITE, 0, magnitude, direction, 0, 0, 0, 0);
                        if (!LGFAILED(ret))
                        {
                            // Fill parameter structure which enables us for next commands to check if we have new
                            // parameters or not
                            ConstantForceParams[channel].magnitude = magnitude;
                            ConstantForceParams[channel].direction = direction;
                        }
                        constant.Start(channel, LG_CONSTANT_0);
                    }
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::StopConstantForce
 * NAME
 *  void LGWheels::StopConstantForce(int channel) -- stop the constant force.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::PlayConstantForce
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::StopConstantForce(int channel)
{
    StopForce(channel, LG_CONSTANT);
}

bool LGWheels::SameConstantForceParams(int channel, int magnitude, int direction)
{
    return ((ConstantForceParams[channel].magnitude == magnitude) && (ConstantForceParams[channel].direction == direction));
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlayDamperForce
 * NAME
 *  void LGWheels::PlayDamperForce(int channel, int coefficient) -- Play the damper force.
 * FUNCTION
 *  Simulate surfaces that are hard to turn on (mud, car at a stop) or slippery surfaces (snow, ice).
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  coefficient - Specify the slope of the effect strength increase relative to the amount of deflection from the center of the condition. Higher
 *                values mean that the saturation level is reached sooner.
 *                Valid ranges are -255 to 255. Any value outside the valid range is silently clamped. -255 simulates a very slipper effect, +255 makes
 *                the wheel very hard to turn, simulating the car at a stop or mud.
 * SEE ALSO
 *  LGWheels::StopDamperForce
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::PlayDamperForce(int channel, int coefficient)
{
    int ret = LG_SUCCESS;

    // only play damper force if slippery road effect is not playing (slippery effect is a damper with negative coefficient and takes
    // precedence on this damper)
    if (IsPlaying(channel, LG_SLIPPERY_ROAD))
        return;

    //if playing
    //	check if same parameters
    //	if same, do nothing
    //	if different, update
    //if NOT playing
    //	if never been downloaded
    //		download and start
    //	else if has been downloaded before
    //		check parameters
    //		if same
    //			start force
    //		else if not same
    //			update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsAirborne[channel] == false)
        {
            if (IsPlaying(channel, LG_DAMPER))
            {
                if (SameDamperForceParams(channel, coefficient))
                {
                    // Do nothing
                } else
                {
                    ret = condition.UpdateForce(channel, LG_CONDITION_1, LGTYPE_DAMPER, LGDURATION_INFINITE, 0, 0, 0, 255, 255, coefficient,
                                                coefficient);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        DamperForceParams[channel].coefficient = coefficient;
                    }
                }
            } else // force is not playing
            {
                // If force has never been downloaded before, download and start it
                if (condition.EffectID[channel][LG_CONDITION_1] == LG_INVALID_EFFECTID)
                {
                    ret = condition.DownloadForce(channel, LG_CONDITION_1, wheels.WheelHandles[channel], LGTYPE_DAMPER, LGDURATION_INFINITE,
                                                  0, 0, 0, 255, 255, coefficient, coefficient);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        DamperForceParams[channel].coefficient = coefficient;
                    }
                    condition.Start(channel, LG_CONDITION_1);
                } else
                    // The force has been downloaded before but is not playing
                {
                    if (SameDamperForceParams(channel, coefficient))
                    {
                        condition.Start(channel, LG_CONDITION_1);
                    } else
                    {
                        ret = condition.UpdateForce(channel, LG_CONDITION_1, LGTYPE_DAMPER, LGDURATION_INFINITE, 0, 0, 0, 255, 255,
                                                    coefficient, coefficient);
                        if (!LGFAILED(ret))
                        {
                            // Fill parameter structure which enables us for next commands to check if we have new
                            // parameters or not
                            DamperForceParams[channel].coefficient = coefficient;
                        }
                        condition.Start(channel, LG_CONDITION_1);
                    }
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::StopDamperForce
 * NAME
 *  void LGWheels::StopDamperForce(int channel) -- stop the damper force.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::PlayDamperForce
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::StopDamperForce(int channel)
{
    StopForce(channel, LG_DAMPER);
}

bool LGWheels::SameDamperForceParams(int channel, int coefficient)
{
    return (DamperForceParams[channel].coefficient == coefficient);
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlaySideCollisionForce
 * NAME
 *  void LGWheels::PlaySideCollisionForce(int channel, int magnitude, int direction) -- Play a side collision force.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  magnitude   - Specifies the magnitude of the side collision force effect. A negative value reverses the direction of the force. It is
 *                equivalent of setting the positive value as magnitude and adding 180 to the direction member.
 *                Valid ranges for magnitudes are -255 to 255. Any values outside the valid range are silently clamped.
 *  direction   - Specifies the direction of the side collision force effect in degrees. A value of 90 will produce a force pushing
 *                counter-clockwise. A value of 270 will make the wheel turn clockwise.
 *                Valid ranges for directions are 0 to 359. Any values outside the valid range are silently modulo'd by 360.
 * NOTE
 *  If you are already using a constant force tied to a vector force from the physics engine, then you may not need to add side collisions since
 *  depending on your physics engine the side collisions may automatically be taken care of by the constant force.
 * SEE ALSO
 *  LGWheels::PlayConstantForce
 ******
 */
void LGWheels::PlaySideCollisionForce(int channel, int magnitude, int direction)
{
    int ret = LG_SUCCESS;

    // This guy is a bit different: even if we have the same parameters, we start the force.

    //if playing
    //	check if same parameters
    //	if same, start force
    //	if different, update and start
    //if NOT playing
    //	if never been downloaded
    //		download and start
    //	else if has been downloaded before
    //		check parameters
    //		if same
    //			start force
    //		else if not same
    //			update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsPlaying(channel, LG_SIDE_COLLISION))
        {
            if (SameSideCollisionForceParams(channel, magnitude, direction))
            {
                constant.Start(channel, LG_CONSTANT_1);
            } else
            {
                ret = constant.UpdateForce(channel, LG_CONSTANT_1, 150, 0, magnitude, direction, 0, 0, 0, 0);
                if (!LGFAILED(ret))
                {
                    // Fill parameter structure which enables us for next commands to check if we have new
                    // parameters or not
                    SideCollisionParams[channel].magnitude = magnitude;
                    SideCollisionParams[channel].direction = direction;
                }
                constant.Start(channel, LG_CONSTANT_1);
            }
        } else // force is not playing
        {
            // If force has never been downloaded before, download and start it
            if (constant.EffectID[channel][LG_CONSTANT_1] == LG_INVALID_EFFECTID)
            {
                ret = constant.DownloadForce(channel, LG_CONSTANT_1, wheels.WheelHandles[channel], 150, 0, magnitude, direction, 0, 0, 0, 0);
                if (!LGFAILED(ret))
                {
                    // Fill parameter structure which enables us for next commands to check if we have new
                    // parameters or not
                    SideCollisionParams[channel].magnitude = magnitude;
                    SideCollisionParams[channel].direction = direction;
                }
                constant.Start(channel, LG_CONSTANT_1);
            } else
                // The force has been downloaded before but is not playing
            {
                if (SameSideCollisionForceParams(channel, magnitude, direction))
                {
                    constant.Start(channel, LG_CONSTANT_1);
                } else
                {
                    ret = constant.UpdateForce(channel, LG_CONSTANT_1, 150, 0, magnitude, direction, 0, 0, 0, 0);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        SideCollisionParams[channel].magnitude = magnitude;
                        SideCollisionParams[channel].direction = direction;
                    }
                    constant.Start(channel, LG_CONSTANT_1);
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

bool LGWheels::SameSideCollisionForceParams(int channel, int magnitude, int direction)
{
    return ((SideCollisionParams[channel].magnitude == magnitude) && (SideCollisionParams[channel].direction == direction));
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlayFrontalCollisionForce
 * NAME
 *  void LGWheels::PlayFrontalCollisionForce(int channel, int magnitude) -- Play a frontal collision force.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  magnitude   - Specifies the magnitude of the frontal collision force effect.
 *                Valid ranges for Magnitude are 0 to 255. Values higher than 255 are silently clamped.
 * SEE ALSO
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::PlayFrontalCollisionForce(int channel, int magnitude)
{
    int ret = LG_SUCCESS;

    // This guy also is a bit different (like side collision): even if we have the same parameters, we start the force.

    //if playing
    //	check if same parameters
    //	if same, start force
    //	if different, update and start
    //if NOT playing
    //	if never been downloaded
    //		download and start
    //	else if has been downloaded before
    //		check parameters
    //		if same
    //			start force
    //		else if not same
    //			update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsPlaying(channel, LG_FRONTAL_COLLISION))
        {
            if (SameFrontalCollisionForceParams(channel, magnitude))
            {
                periodic.Start(channel, LG_PERIODIC_0);
            } else
            {
                ret = periodic.UpdateForce(channel, LG_PERIODIC_0, LGTYPE_SQUARE, 150, 0, magnitude, 90, 75, 0, 0, 0, 20, 0, 0);
                if (!LGFAILED(ret))
                {
                    // Fill parameter structure which enables us for next commands to check if we have new
                    // parameters or not
                    FrontalCollisionParams[channel].magnitude = magnitude;
                }
                periodic.Start(channel, LG_PERIODIC_0);
            }
        } else // force is not playing
        {
            // If force has never been downloaded before, download and start it
            if (periodic.EffectID[channel][LG_PERIODIC_0]  == LG_INVALID_EFFECTID)
            {
                ret = periodic.DownloadForce(channel, LG_PERIODIC_0, wheels.WheelHandles[channel], LGTYPE_SQUARE, 150, 0, magnitude,
                                             90, 75, 0, 0, 0, 20, 0, 0);
                if (!LGFAILED(ret))
                {
                    // Fill parameter structure which enables us for next commands to check if we have new
                    // parameters or not
                    FrontalCollisionParams[channel].magnitude = magnitude;
                }
                periodic.Start(channel, LG_PERIODIC_0);
            } else
                // The force has been downloaded before but is not playing
            {
                if (SameFrontalCollisionForceParams(channel, magnitude))
                {
                    periodic.Start(channel, LG_PERIODIC_0);
                } else
                {
                    ret = periodic.UpdateForce(channel, LG_PERIODIC_0, LGTYPE_SQUARE, 150, 0, magnitude, 90, 75, 0, 0, 0, 20, 0, 0);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        FrontalCollisionParams[channel].magnitude = magnitude;
                    }
                    periodic.Start(channel, LG_PERIODIC_0);
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

bool LGWheels::SameFrontalCollisionForceParams(int channel, int magnitude)
{
    return (FrontalCollisionParams[channel].magnitude == magnitude);
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlayDirtRoadEffect
 * NAME
 *  void LGWheels::PlayDirtRoadEffect(int channel, int magnitude) -- Play a surface effect that feels like driving on a dirt road.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  magnitude   - Specifies the magnitude of the dirt road effect.
 *                Valid ranges for Magnitude are 0 to 255. Values higher than 255 are silently clamped.
 * SEE ALSO
 *  LGWheels::StopDirtRoadEffect
 ******
 */
void LGWheels::PlayDirtRoadEffect(int channel, int magnitude)
{
    int ret = LG_SUCCESS;

    //if playing
    //	check if same parameters
    //	if same, do nothing
    //	if different, update
    //if NOT playing
    //	if never been downloaded
    //		download and start
    //	else if has been downloaded before
    //		check parameters
    //		if same
    //			start force
    //		else if not same
    //			update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsAirborne[channel] == false)
        {
            if (IsPlaying(channel, LG_DIRT_ROAD))
            {
                if (SameDirtRoadEffectParams(channel, magnitude))
                {
                    // Do nothing
                } else
                {
                    ret = periodic.UpdateForce(channel, LG_PERIODIC_1, LGTYPE_SINE, LGDURATION_INFINITE, 0, magnitude, 90, 65, 0, 0, 0, 0, 0, 0);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        DirtRoadParams[channel].magnitude = magnitude;
                    }
                }
            } else // force is not playing
            {
                // If force has never been downloaded before, download and start it
                if (periodic.EffectID[channel][LG_PERIODIC_1] == LG_INVALID_EFFECTID)
                {
                    ret = periodic.DownloadForce(channel, LG_PERIODIC_1, wheels.WheelHandles[channel], LGTYPE_SINE, LGDURATION_INFINITE,
                                                 0, magnitude, 90, 65, 0, 0, 0, 0, 0, 0);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        DirtRoadParams[channel].magnitude = magnitude;
                    }
                    periodic.Start(channel, LG_PERIODIC_1);
                } else
                    // The force has been downloaded before but is not playing
                {
                    if (SameDirtRoadEffectParams(channel, magnitude))
                    {
                        periodic.Start(channel, LG_PERIODIC_1);
                    } else
                    {
                        ret = periodic.UpdateForce(channel, LG_PERIODIC_1, LGTYPE_SINE, LGDURATION_INFINITE, 0, magnitude, 90, 65,
                                                   0, 0, 0, 0, 0, 0);
                        if (!LGFAILED(ret))
                        {
                            // Fill parameter structure which enables us for next commands to check if we have new
                            // parameters or not
                            DirtRoadParams[channel].magnitude = magnitude;
                        }
                        periodic.Start(channel, LG_PERIODIC_1);
                    }
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::StopDirtRoadEffect
 * NAME
 *  void LGWheels::StopDirtRoadEffect(int channel) -- stop the dirt road effect.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::PlayDirtRoadEffect
 ******
 */
void LGWheels::StopDirtRoadEffect(int channel)
{
    StopForce(channel, LG_DIRT_ROAD);
}

bool LGWheels::SameDirtRoadEffectParams(int channel, int magnitude)
{
    return (DirtRoadParams[channel].magnitude == magnitude);
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlayBumpyRoadEffect
 * NAME
 *  void LGWheels::PlayBumpyRoadEffect(int channel, int magnitude) -- Play a surface effect that feels like driving on a bumpy road (like on
 *  cobblestones for example).
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  magnitude   - Specifies the magnitude of the bumpy road effect.
 *                Valid ranges for Magnitude are 0 to 255. Values higher than 255 are silently clamped.
 * SEE ALSO
 *  LGWheels::StopBumpyRoadEffect
 ******
 */
void LGWheels::PlayBumpyRoadEffect(int channel, int magnitude)
{
    int ret = LG_SUCCESS;

    //if playing
    //	check if same parameters
    //	if same, do nothing
    //	if different, update
    //if NOT playing
    //	if never been downloaded
    //		download and start
    //	else if has been downloaded before
    //		check parameters
    //		if same
    //			start force
    //		else if not same
    //			update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsAirborne[channel] == false)
        {
            if (IsPlaying(channel, LG_BUMPY_ROAD))
            {
                if (SameBumpyRoadEffectParams(channel, magnitude))
                {
                    // Do nothing
                } else
                {
                    ret = periodic.UpdateForce(channel, LG_PERIODIC_2, LGTYPE_SQUARE, LGDURATION_INFINITE, 0, magnitude, 90, 100, 0, 0, 0, 0, 0, 0);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        BumpyRoadParams[channel].magnitude = magnitude;
                    }
                }
            } else // force is not playing
            {
                // If force has never been downloaded before, download and start it
                if (periodic.EffectID[channel][LG_PERIODIC_2] == LG_INVALID_EFFECTID)
                {
                    ret = periodic.DownloadForce(channel, LG_PERIODIC_2, wheels.WheelHandles[channel], LGTYPE_SQUARE, LGDURATION_INFINITE,
                                                 0, magnitude, 90, 100, 0, 0, 0, 0, 0, 0);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        BumpyRoadParams[channel].magnitude = magnitude;
                    }
                    periodic.Start(channel, LG_PERIODIC_2);
                } else
                    // The force has been downloaded before but is not playing
                {
                    if (SameBumpyRoadEffectParams(channel, magnitude))
                    {
                        periodic.Start(channel, LG_PERIODIC_2);
                    } else
                    {
                        ret = periodic.UpdateForce(channel, LG_PERIODIC_2, LGTYPE_SQUARE, LGDURATION_INFINITE, 0, magnitude, 90, 100,
                                                   0, 0, 0, 0, 0, 0);
                        if (!LGFAILED(ret))
                        {
                            // Fill parameter structure which enables us for next commands to check if we have new
                            // parameters or not
                            BumpyRoadParams[channel].magnitude = magnitude;
                        }
                        periodic.Start(channel, LG_PERIODIC_2);
                    }
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::StopBumpyRoadEffect
 * NAME
 *  void LGWheels::StopBumpyRoadEffect(int channel) -- stop the bumpy road effect.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::PlayBumpyRoadEffect
 ******
 */
void LGWheels::StopBumpyRoadEffect(int channel)
{
    StopForce(channel, LG_BUMPY_ROAD);
}

bool LGWheels::SameBumpyRoadEffectParams(int channel, int magnitude)
{
    return (BumpyRoadParams[channel].magnitude == magnitude);
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlaySlipperyRoadEffect
 * NAME
 *  void LGWheels::PlaySlipperyRoadEffect(int channel, int magnitude) -- Play a slippery road effect (snow, ice).
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  magnitude   - Specifies the magnitude of the slippery road effect.
 *                Valid ranges for magnitude are 0 to 255. 255 corresponds to the most slippery effect.
 * SEE ALSO
 *  LGWheels::StopSlipperyRoadEffect
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::PlaySlipperyRoadEffect(int channel, int magnitude)
{
    int ret = LG_SUCCESS;

    // check if there was a damper playing when making the current call. If there was, stop the damper and remember
    // to restart it after slippery road effect is stopped
    if (IsPlaying(channel, LG_DAMPER))
    {
        StopDamperForce(channel);
        condition.Playing[channel][LG_CONDITION_1] = false;
        damperWasPlaying[channel] = true;
    }

    // check if there was a spring playing when making the current call. If there was, stop the spring and remember
    // to restart it after slippery road effect is stopped
    if (IsPlaying(channel, LG_SPRING))
    {
        StopSpringForce(channel);
        condition.Playing[channel][LG_CONDITION_0] = false;
        springWasPlaying[channel] = true;
    }

    //if playing
    //	check if same parameters
    //	if same, do nothing
    //	if different, update
    //if NOT playing
    //	if never been downloaded
    //		download and start
    //	else if has been downloaded before
    //		check parameters
    //		if same
    //			start force
    //		else if not same
    //			update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsAirborne[channel] == false)
        {
            if (IsPlaying(channel, LG_SLIPPERY_ROAD))
            {
                if (SameSlipperyRoadEffectParams(channel, magnitude))
                {
                    // Do nothing
                } else
                {
                    ret = condition.UpdateForce(channel, LG_CONDITION_2, LGTYPE_DAMPER, LGDURATION_INFINITE, 0, 0, 0, (int)255,
                                                (int)255, (int)-magnitude, (int)-magnitude);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        SlipperyRoadParams[channel].magnitude = magnitude;
                    }
                }
            } else // force is not playing
            {
                // If force has never been downloaded before, download and start it
                if (condition.EffectID[channel][LG_CONDITION_2] == LG_INVALID_EFFECTID)
                {
                    ret = condition.DownloadForce(channel, LG_CONDITION_2, wheels.WheelHandles[channel], LGTYPE_DAMPER,
                                                  LGDURATION_INFINITE, 0, 0, 0, (int)255, (int)255, (int)-magnitude, (int)-magnitude);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        SlipperyRoadParams[channel].magnitude = magnitude;
                    }
                    condition.Start(channel, LG_CONDITION_2);
                } else
                    // The force has been downloaded before but is not playing
                {
                    if (SameSlipperyRoadEffectParams(channel, magnitude))
                    {
                        condition.Start(channel, LG_CONDITION_2);
                    } else
                    {
                        ret = condition.UpdateForce(channel, LG_CONDITION_2, LGTYPE_DAMPER, LGDURATION_INFINITE, 0, 0, 0, (int)255,
                                                    (int)255, (int)-magnitude, (int)-magnitude);
                        if (!LGFAILED(ret))
                        {
                            // Fill parameter structure which enables us for next commands to check if we have new
                            // parameters or not
                            SlipperyRoadParams[channel].magnitude = magnitude;
                        }
                        condition.Start(channel, LG_CONDITION_2);
                    }
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::StopSlipperyRoadEffect
 * NAME
 *  void LGWheels::StopSlipperyRoadEffect(int channel) -- stop the slippery road effect.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::PlaySlipperyRoadEffect
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::StopSlipperyRoadEffect(int channel)
{
    StopForce(channel, LG_SLIPPERY_ROAD);
}

bool LGWheels::SameSlipperyRoadEffectParams(int channel, int magnitude)
{
    return (SlipperyRoadParams[channel].magnitude == magnitude);
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlaySurfaceEffect
 * NAME
 *  void LGWheels::PlaySurfaceEffect(int channel, int type, int magnitude, int period) -- Play any type of rumble to simulate surface effect.
 * INPUTS
 *  type        - Specifies the type of force effect.
 *                Can be one of the following values: LG_TYPE_SINE, LG_TYPE_SQUARE, LG_TYPE_TRIANGLE, LG_TYPE_SAWTOOTHUP, LG_TYPE_SAWTOOTHDOWN.
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 *  magnitude   - Specifies the magnitude of the surface effect.
 *                Valid ranges for Magnitude are 0 to 255. Values higher than 255 are silently clamped.
 *  period      - Specifies the period of the periodic force effect. The value is the duration for one full cycle of the periodic function measured
 *                in milliseconds (0.001 seconds). A good range of values for the period is 20 ms (sand) to 120 ms (wooden bridge or cobblestones).
 * SEE ALSO
 *  LGWheels::StopSurfaceEffect
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::PlaySurfaceEffect(int channel, int type, int magnitude, int period)
{
    int ret = LG_SUCCESS;

    //if playing
    //  check if same parameters
    //  if same, do nothing
    //  if different, update
    //if NOT playing
    //  if never been downloaded
    //          download and start
    //  else if has been downloaded before
    //          check parameters
    //          if same
    //                  start force
    //          else if not same
    //                  update and start force
    //

    if (wheels.IsConnected(channel)) // only process in case a wheel is connected and has been opened
    {
        if (IsAirborne[channel] == false)
        {
            if (IsPlaying(channel, LG_SURFACE_EFFECT))
            {
                if (SameSurfaceEffectParams(channel, type, magnitude, period))
                {
                    // Do nothing
                } else
                {
                    if (type != SurfaceEffectParams[channel].type)
                    {
                        periodic.Destroy(channel, LG_PERIODIC_3);
                        ret = periodic.DownloadForce(channel, LG_PERIODIC_3, wheels.WheelHandles[channel], type, LGDURATION_INFINITE, 0,
                                                     magnitude, 90, period, 0, 0, 0, 0, 0, 0);
                        periodic.Start(channel, LG_PERIODIC_3);

                    } else
                    {
                        ret = periodic.UpdateForce(channel, LG_PERIODIC_3, type, LGDURATION_INFINITE, 0, magnitude, 90, period, 0, 0, 0, 0, 0, 0);
                    }

                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        SurfaceEffectParams[channel].type = type;
                        SurfaceEffectParams[channel].magnitude = magnitude;
                        SurfaceEffectParams[channel].period = period;
                    }
                }
            } else // force is not playing
            {
                // If force has never been downloaded before, download and start it
                if (periodic.EffectID[channel][LG_PERIODIC_3] == LG_INVALID_EFFECTID)
                {
                    ret = periodic.DownloadForce(channel, LG_PERIODIC_3, wheels.WheelHandles[channel], type, LGDURATION_INFINITE, 0,
                                                 magnitude, 90, period, 0, 0, 0, 0, 0, 0);
                    if (!LGFAILED(ret))
                    {
                        // Fill parameter structure which enables us for next commands to check if we have new
                        // parameters or not
                        SurfaceEffectParams[channel].type = type;
                        SurfaceEffectParams[channel].magnitude = magnitude;
                        SurfaceEffectParams[channel].period = period;
                    }
                    periodic.Start(channel, LG_PERIODIC_3);
                } else
                    // The force has been downloaded before but is not playing
                {
                    if (SameSurfaceEffectParams(channel, type, magnitude, period))
                    {
                        periodic.Start(channel, LG_PERIODIC_3);
                    } else
                    {
                        if (type != SurfaceEffectParams[channel].type)
                        {
                            periodic.Destroy(channel, LG_PERIODIC_3);
                            ret = periodic.DownloadForce(channel, LG_PERIODIC_3, wheels.WheelHandles[channel], type, LGDURATION_INFINITE,
                                                         0, magnitude, 90, period, 0, 0, 0, 0, 0, 0);
                        } else
                        {
                            ret = periodic.UpdateForce(channel, LG_PERIODIC_3, type, LGDURATION_INFINITE, 0, magnitude, 90, period,
                                                       0, 0, 0, 0, 0, 0);
                        }

                        if (!LGFAILED(ret))
                        {
                            // Fill parameter structure which enables us for next commands to check if we have new
                            // parameters or not
                            SurfaceEffectParams[channel].type = type;
                            SurfaceEffectParams[channel].magnitude = magnitude;
                            SurfaceEffectParams[channel].period = period;
                        }
                        periodic.Start(channel, LG_PERIODIC_3);
                    }
                }
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::StopSurfaceEffect
 * NAME
 *  void LGWheels::StopSurfaceEffect(int channel) -- stop the surface effect.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::PlaySurfaceEffect
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::StopSurfaceEffect(int channel)
{
    StopForce(channel, LG_SURFACE_EFFECT);
}

bool LGWheels::SameSurfaceEffectParams(int channel, int type, int magnitude, int period)
{
    return ((SurfaceEffectParams[channel].type == type) && (SurfaceEffectParams[channel].magnitude == magnitude)
            && (SurfaceEffectParams[channel].period == period));
}

/****f* EZ.Wheel.Wrapper/LGWheels::PlayCarAirborne
 * NAME
 *  void LGWheels::PlayCarAirborne(int channel) -- Play an effect that simulates a car that is airborne or where the front wheels do not touch the
 *  ground.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::StopCarAirborne
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::PlayCarAirborne(int channel)
{
    // only process in case a wheel is connected and has been opened and car isn't in the air already
    if (wheels.IsConnected(channel))
    {
        if (IsAirborne[channel] == false)
        {
            IsAirborne[channel] = true;

            if (IsPlaying(channel, LG_SPRING))
            {
                StopSpringForce(channel);
                wasPlayingBeforeAirborne[channel][LG_SPRING] = true;
            }
            if (IsPlaying(channel, LG_CONSTANT))
            {
                StopConstantForce(channel);
                wasPlayingBeforeAirborne[channel][LG_CONSTANT] = true;
            }
            if (IsPlaying(channel, LG_DAMPER))
            {
                StopDamperForce(channel);
                wasPlayingBeforeAirborne[channel][LG_DAMPER] = true;
            }
            if (IsPlaying(channel, LG_DIRT_ROAD))
            {
                StopDirtRoadEffect(channel);
                wasPlayingBeforeAirborne[channel][LG_DIRT_ROAD] = true;
            }
            if (IsPlaying(channel, LG_BUMPY_ROAD))
            {
                StopBumpyRoadEffect(channel);
                wasPlayingBeforeAirborne[channel][LG_BUMPY_ROAD] = true;
            }
            if (IsPlaying(channel, LG_SLIPPERY_ROAD))
            {
                StopSlipperyRoadEffect(channel);
                wasPlayingBeforeAirborne[channel][LG_SLIPPERY_ROAD] = true;
            }
            if (IsPlaying(channel, LG_SURFACE_EFFECT))
            {
                StopSurfaceEffect(channel);
                wasPlayingBeforeAirborne[channel][LG_SURFACE_EFFECT] = true;
            }
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: trying to play a force on channel %d but no wheel opened.\n", channel);
#endif
    }
}

/****f* EZ.Wheel.Wrapper/LGWheels::StopCarAirborne
 * NAME
 *  void LGWheels::StopCarAirborne(int channel) -- stop the car airborne effect and resume any forces that were playing before the car was airborne.
 * INPUTS
 *  channel     - number of the channel to which the concerned wheel is connected.
 *                Channel 0 corresponds to the first wheel connected on the USB tree. Channel 1 to the second wheel.
 * SEE ALSO
 *  LGWheels::PlayCarAirborne
 *  Sample_In-game_Implementation.cpp to see an example.
 ******
 */
void LGWheels::StopCarAirborne(int channel)
{
    StopForce(channel, LG_CAR_AIRBORNE);
}

/*********/
#endif // HAVE_FFWHEEL
