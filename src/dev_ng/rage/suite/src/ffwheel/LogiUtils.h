/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#ifndef LOGI_UTILS_H_INCLUDED_
#define LOGI_UTILS_H_INCLUDED_

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiGlobals.h"

namespace LogitechEZWheelWrapper
{
    class Utils
    {
    public:
        static LG_INT FromPercentage(const LG_INT percentage, const LG_INT minPercentage, const LG_INT maxPercentage, const LG_INT minOutput, const LG_INT maxOutput);
    };
}

#endif

#endif // LOGI_UTILS_H_INCLUDED_
