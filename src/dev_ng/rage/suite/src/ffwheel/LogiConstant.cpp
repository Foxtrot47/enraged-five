/*
The Logitech EZ Wheel Wrapper, including all acompanying documentation, 
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiConstant.h"
#include <string.h>

LogiConstant::LogiConstant()
{
}

LogiConstant::~LogiConstant()
{
}

int LogiConstant::DownloadForce(int channel, int forceNumber, int &handle, int duration, int startDelay, int magnitude,
                                int direction, int attackTime, int fadeTime, int attackLevel, int fadeLevel)
{
    lgDevForceEffect force;
    int ret = LG_SUCCESS;

    if (EffectID[channel][forceNumber] != LG_INVALID_EFFECTID)
    {
        // Destroy force effect or otherwise a different effect ID will be assigned which is bad
#ifdef _DEBUG
        printf("WARNING: Can't assign new effectID over old one (channel %d, force number %d). Destroying old force.\n", channel, forceNumber);
#endif
        Destroy(channel, forceNumber);
    }

    if (handle != LG_INVALID_DEVICE)
    {
        memset(&force, 0, sizeof(lgDevForceEffect));

        force.Type = LGTYPE_CONSTANT;
        force.Duration = duration;
        force.StartDelay = startDelay;
        force.p.constant.Magnitude = magnitude;
        force.p.constant.Direction = direction;
        force.p.constant.envelope.AttackTime = attackTime;
        force.p.constant.envelope.AttackLevel = attackLevel;
        force.p.constant.envelope.FadeTime = fadeTime;
        force.p.constant.envelope.FadeLevel = fadeLevel;

#ifdef _DEBUG
        printf("Downloading constant force effect on channel %d\n", channel);
#endif
        ret = lgDevDownloadForceEffect(handle, &EffectID[channel][forceNumber], &force);
        if(LGFAILED(ret))
        {
#ifdef _DEBUG_BASIC
            printf("ERROR: DownloadForce(constant force) on channel %d returned %d\n", channel, ret);
#endif
            EffectID[channel][forceNumber] = LG_INVALID_EFFECTID;
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: Trying to download a constant force to channel %d but wheel has not been opened.\n", channel);
#endif
    }
    return ret;
}

int LogiConstant::UpdateForce(int channel, int forceNumber, int duration, int startDelay, int magnitude, int direction,
                              int attackTime, int fadeTime, int attackLevel, int fadeLevel)
{
    lgDevForceEffect force;
    int ret = LG_SUCCESS;

    memset(&force, 0, sizeof(lgDevForceEffect));

    force.Type = LGTYPE_CONSTANT;
    force.Duration = duration;
    force.StartDelay = startDelay;
    force.p.constant.Magnitude = magnitude;
    force.p.constant.Direction = direction;
    force.p.constant.envelope.AttackTime = attackTime;
    force.p.constant.envelope.AttackLevel = attackLevel;
    force.p.constant.envelope.FadeTime = fadeTime;
    force.p.constant.envelope.FadeLevel = fadeLevel;

#ifdef _DEBUG
    printf("Updating constant force effect on channel %d\n", channel);
#endif

    //mjc lgDevASync(LGASYNC_MODE_WAIT, &ret);
    lgDevUpdateForceEffect(EffectID[channel][forceNumber], &force);
    //lgDevASync(LGASYNC_MODE_WAIT, &ret);

    if(LGFAILED(ret))
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: UpdateForce(constant force) on channel %d returned %d\n", channel, ret);
#endif
        EffectID[channel][forceNumber] = LG_INVALID_EFFECTID;
    }

    return ret;
}

#endif // HAVE_FFWHEEL
