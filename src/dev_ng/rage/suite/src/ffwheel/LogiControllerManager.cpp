/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#include "LogiControllerManager.h"

#if HAVE_FFWHEEL

#include "LogiCommon.h"
#include <string.h>

using namespace LogitechEZWheelWrapper;

ControllerManager::ControllerManager()
{
    Init();
}

ControllerManager::~ControllerManager()
{
    DeleteAllControllers();
}

LG_VOID ControllerManager::Init()
{
    m_enumHint = LGHINT_ENUMNEEDED;

    for (LG_INT ii = 0; ii < LG_MAX_CONTROLLERS; ii++)
    {
        m_controller[ii] = NULL;
        m_multiturnEnabled[ii] = false;
        m_previousConnectionStatus[ii] = false;
    }
#if __PS3
    memset(&m_lastPadInfo, 0, sizeof(m_lastPadInfo));
#endif
}

LG_BOOL ControllerManager::EnumIsNeeded()
{
    lgDevEnumHint(&m_enumHint);

    if (LGHINT_ENUMNEEDED == m_enumHint)
        return true;

    return false;
}

LG_VOID ControllerManager::EnumerateDevices()
{
    DeleteAllControllers();

    lgDevDeviceDesc description_;
    LG_INT index_ = 0;
    LG_INT portNbr_ = 0;

    while(LGSUCCEEDED(lgDevEnumerate(index_, &description_)))
    {
#if __XENON
		// Let's discard joysticks cause this wrapper is for wheels only
		if (/*LGCLASS_JOYSTICK_A == description_.DevClass
			||*/ LGCLASS_JOYSTICK_B == description_.DevClass)
		{
			++portNbr_;
			++index_;
			continue;
		}
#else
		// Let's discard joysticks cause this wrapper is for wheels only
		if (LGCLASS_JOYSTICK_A == description_.DevClass
			|| LGCLASS_JOYSTICK_B == description_.DevClass)
		{
			++index_;
			continue;
		}
#endif
        if(portNbr_ < LG_MAX_CONTROLLERS)
        {
            LG_INT handle_ = -1;
            if (LGFAILED(lgDevOpen(index_, &handle_)))
            {
                LOGITRACE("ERROR: failed to open device for index %d, handle 0x%.8x\n", index_, handle_);
                return;
            }

            m_controller[portNbr_] = rage_new Controller();

            if (NULL == m_controller[portNbr_])
            {
                LOGITRACE("ERROR: m_controller[%d] is NULL\n", portNbr_);
                return;
            }

            m_controller[portNbr_]->SetHandle(handle_);
            m_controller[portNbr_]->SetClass(description_.DevClass);

            if (description_.Flags & LGFLAG_MULTITURN)
                m_controller[portNbr_]->SetMultiturnFlag(true);

            if (description_.AdditionalControls & LGAC_CLUTCH)
                m_controller[portNbr_]->SetClutchFlag(true);

            if (description_.AdditionalControls & LGAC_GATED_SHIFTER)
                m_controller[portNbr_]->SetGatedShifterFlag(true);

#if __XENON
			m_controller[portNbr_]->SetPortNbr(portNbr_);
#else
			m_controller[portNbr_]->SetPortNbr(description_.CellPadPortNo);
#endif

            // disable centering spring
            if (LGFAILED(lgDevSetDeviceProperty(handle_, LGPROP_CENTERINGENABLED, false)))
                LOGITRACE("ERROR: failed to disable centering spring for handle 0x%x\n", handle_);
        }

        ++portNbr_;
        ++index_;
    }
}

LG_VOID ControllerManager::DeleteAllControllers()
{
    for (LG_INT ii = 0; ii < LG_MAX_CONTROLLERS; ii++)
    {
        if (NULL != m_controller[ii])
        {
            lgDevClose(m_controller[ii]->GetHandle());
            SAFE_DELETE(m_controller[ii]);
        }
    }
}

// arrange controllers based on port_no
LG_VOID ControllerManager::ReOrderControllers()
{
    // Update port numbers
    for (LG_INT ii = 0; ii < LG_MAX_CONTROLLERS; ii++)
    {
        if (NULL == m_controller[ii])
            continue;

        LG_INT port_no_ = -1;
        if (LGFAILED(lgDevGetDeviceProperty(m_controller[ii]->GetHandle(), LGPROP_CELLPAD_PORT_NUMBER, &port_no_)))
            LOGITRACE("ERROR: failed to do lgDevGetDeviceProperty for handle 0x%x\n", m_controller[ii]->GetHandle());

        m_controller[ii]->SetPortNbr(port_no_);
    }

    // rearrange

    Controller* tempController_[LG_MAX_CONTROLLERS];
    // First make copy of all controller pointers
    for (LG_INT ii = 0; ii < LG_MAX_CONTROLLERS; ii++)
    {
        tempController_[ii] = m_controller[ii];
        m_controller[ii] = NULL;
    }

    // shuffle controllers based on port_no
    for (LG_INT ii = 0; ii < LG_MAX_CONTROLLERS; ii++)
    {
        if (NULL == tempController_[ii])
            continue;

        LG_INT portNbr_ = tempController_[ii]->GetPortNbr();

        if (-1 == portNbr_)
        {
            LOGITRACE("ERROR: trying to re-arrange but controller port_no = -1 for controller %d\n", ii);
            continue;
        }
        else
        {
            m_controller[portNbr_] = tempController_[ii];
        }
    }
}

LG_VOID ControllerManager::ReadAll()
{
    for (LG_INT ii = 0; ii < LG_MAX_CONTROLLERS; ii++)
    {
        if (NULL != m_controller[ii])
            m_controller[ii]->Read();
    }
}

Controller* ControllerManager::GetController(const LG_INT ctrlNbr)
{
    return m_controller[ctrlNbr];
}

LG_VOID ControllerManager::Update()
{
    // Check if wheen re-enumeration is needed
    if (EnumIsNeeded())
    {
        EnumerateDevices();
#if !__XENON
        ReOrderControllers();
#endif

        // if wheel got unplugged, reset its multiturn flag to false
        for (LG_INT ii = 0; ii < LG_MAX_CONTROLLERS; ii++)
        {
            if (NULL == m_controller[ii] && true == m_previousConnectionStatus[ii])
            {
                SetMultiturnFlag(ii, false);
            }
        }

        // Reset wheels to multiturn where needed
        for (LG_INT ii = 0; ii < LG_MAX_CONTROLLERS; ii++)
        {
            if (true == GetMultiturnFlag(ii))
            {
                if (NULL != m_controller[ii])
                {
                    m_controller[ii]->SetMultiturn(LG_TRUE);
                }
            }
        }

        // Update previousConnectionStatus
        for (LG_INT ii = 0; ii < LG_MAX_CONTROLLERS; ii++)
        {
            if (NULL == m_controller[ii])
            {
                m_previousConnectionStatus[ii] = false;
            }
            else
            {
                m_previousConnectionStatus[ii] = true;
            }
        }
    }

#if __PS3
    // Check if re-ordering is needed (pad port nbr has changed)
    CellPadInfo2 padInfo_;
    cellPadGetInfo2(&padInfo_);
    if(PadChanged(m_lastPadInfo, padInfo_))
    {
        // pad configuration has changed, let's find out what is
        // going on
        LG_BOOL needsReOrdering_ = false;
        //LOGITRACE("pad configuration changed:\n");
        for(LG_UINT ii = 0; ii < padInfo_.max_connect; ii++)
        {
            //LOGITRACE("  pad %d: status = %d [VID:%04x PID:%04x]: ",
                //ii, padInfo_.status[ii], padInfo_.vendor_id[ii], padInfo_.product_id[ii]);
            switch(padInfo_.device_type[ii])
            {
            case 0:
                //LOGITRACE("no controller connected\n");
                break;
            case 1:
                //LOGITRACE("cellPad controller connected\n");
                break;
            case 5:
                //LOGITRACE("liblgFF controller connected\n");
                needsReOrdering_ = true;
                break;
            default:
                //LOGITRACE("Warning: unknown controller connected.\n");
                break;
            }
        }
        if(needsReOrdering_)
        {
            ReOrderControllers();
        }
        // process pad changes
        m_lastPadInfo = padInfo_;
    }
#endif

    ReadAll();
}

//////// Helpers to find out when the pad configuration has changed
#if __PS3
LG_BOOL ControllerManager::PadChanged(const CellPadInfo2 &padInfo1, const CellPadInfo2 &padInfo2, const LG_INT index)
{
    return (padInfo1.port_status[index] != padInfo2.port_status[index]) ||
        (padInfo1.device_type[index] != padInfo2.device_type[index]) ||
        (padInfo1.device_capability[index] != padInfo2.device_capability[index]);
}

LG_BOOL ControllerManager::PadChanged(const CellPadInfo2 &padInfo1, const CellPadInfo2 &padInfo2)
{
    for(LG_UINT ii = 0; ii < padInfo2.max_connect; ii++)
    {
        if(PadChanged(padInfo1, padInfo2, ii))
        {
            return true;
        }
    }

    return false;
}
#endif

#endif

