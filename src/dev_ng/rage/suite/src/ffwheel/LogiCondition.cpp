/*
The Logitech EZ Wheel Wrapper, including all acompanying documentation, 
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiCondition.h"
#include <string.h>

LogiCondition::LogiCondition()
{
}

LogiCondition::~LogiCondition()
{
}

int LogiCondition::DownloadForce(int channel, int forceNumber, int &handle, int type, int duration, int startDelay, int offset,
                                 int deadband, int satNeg, int satPos, int coeffNeg, int coeffPos)
{
    lgDevForceEffect force;
    int ret = LG_SUCCESS;

    if (EffectID[channel][forceNumber] != LG_INVALID_EFFECTID)
    {
        // Destroy force effect or otherwise a different effect ID will be assigned which is bad
#ifdef _DEBUG
        printf("WARNING: Can't assign new effectID over old one (channel %d, force number %d). Destroying old force.\n", channel, forceNumber);
#endif
        Destroy(channel, forceNumber);
    }

    if (handle != LG_INVALID_DEVICE)
    {
        memset(&force, 0, sizeof(lgDevForceEffect));

        force.Type = (unsigned char)type;
        force.Duration = duration;
        force.StartDelay = startDelay;
        force.p.condition[0].Offset = offset;
        force.p.condition[0].Deadband = deadband;
        force.p.condition[0].SaturationNeg = satNeg;
        force.p.condition[0].SaturationPos = satPos;
        force.p.condition[0].CoefficientNeg = coeffNeg;
        force.p.condition[0].CoefficientPos = coeffPos;
        // make the Y axis be the same as X (only for joysticks)
        force.p.condition[1] = force.p.condition[0];

#ifdef _DEBUG
        printf("Downloading condition force effect on channel %d\n", channel);
#endif
        ret = lgDevDownloadForceEffect(handle, &EffectID[channel][forceNumber], &force);
        if(LGFAILED(ret))
        {
#ifdef _DEBUG_BASIC
            printf("ERROR: DownloadForce(condition force) on channel %d returned %d\n", channel, ret);
#endif
            EffectID[channel][forceNumber] = LG_INVALID_EFFECTID;
        }
    } else
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: Trying to download a condition force to channel %d but wheel has not been opened.\n", channel);
#endif
    }
    return ret;
}

int LogiCondition::UpdateForce(int channel, int forceNumber, int type, int duration, int startDelay, int offset, int deadband,
                               int satNeg, int satPos, int coeffNeg, int coeffPos)
{
    lgDevForceEffect force;
    int ret = LG_SUCCESS;

    memset(&force, 0, sizeof(lgDevForceEffect));

    force.Type = (unsigned char)type;
    force.Duration = duration;
    force.StartDelay = startDelay;
    force.p.condition[0].Offset = offset;
    force.p.condition[0].Deadband = deadband;
    force.p.condition[0].SaturationNeg = satNeg;
    force.p.condition[0].SaturationPos = satPos;
    force.p.condition[0].CoefficientNeg = coeffNeg;
    force.p.condition[0].CoefficientPos = coeffPos;
    // make the Y axis be the same as X (only for joysticks)
    force.p.condition[1] = force.p.condition[0];

#ifdef _DEBUG
    printf("Updating condition force effect on channel %d\n", channel);
#endif

    //mjc lgDevASync(LGASYNC_MODE_WAIT, &ret);
    lgDevUpdateForceEffect(EffectID[channel][forceNumber], &force);
    //lgDevASync(LGASYNC_MODE_WAIT, &ret);

    if(LGFAILED(ret))
    {
#ifdef _DEBUG_BASIC
        printf("ERROR: UpdateForce(condition force) on channel %d returned %d\n", channel, ret);
#endif
        EffectID[channel][forceNumber] = LG_INVALID_EFFECTID;
    }

    return ret;
}

#endif // HAVE_FFWHEEL
