/*
The Logitech EZ Wheel Wrapper, including all acompanying documentation, 
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiWheels.h"
#include "system/memory.h"
#include "system/new.h"
#include "diag/output.h"
#include <string.h>
#include "system/ipc.h"
using namespace rage;

LogiWheels::LogiWheels()
{
    InitVars();
    InitlgDevLibrary();
}

LogiWheels::~LogiWheels()
{
}

void LogiWheels::InitVars()
{
    memset(&Position, 0, sizeof(Position));
    memset(&PositionLast, 0, sizeof(PositionLast));
    memset(&NonLinearWheel, 0, sizeof(NonLinearWheel));

    for (int channel = 0; channel < LG_MAX_CHANNELS; ++channel)
    {
        WheelHandles[channel] = LG_INVALID_DEVICE;
        deviceNumber[channel] = 0;
        validController[channel] = false;
        wheelClass[channel] = LG_ERROR;
        multiturnCapable[channel] = false;
        multiturnCache[channel] = -1;
        m_hasGatedShifter[channel] = false;
        m_hasClutch[channel] = false;
        Position[channel].POV0 = 255;
        Position[channel].POV1 = 255;
        PositionLast[channel].POV0 = 255;
        PositionLast[channel].POV1 = 255;
    }

    enumhint = LGHINT_ENUMNEEDED;
    numDevices = 0;
}

static void * mallocWrapper(size_t s)
{
	char * pmem = rage_new char[s];
	return (void*)pmem;
}

static void freeWrapper(void * pmem)
{
	delete [] (char*)pmem;
}

////////////////////////////////////////////////////////////////////////////
// Method: InitlgDevLibrary(): initializes Logitech library and variables //
////////////////////////////////////////////////////////////////////////////
void LogiWheels::InitlgDevLibrary()
{
    // init wingman devices rpc interface
	lgDevInitParams initParams;
	memset(&initParams, 0, sizeof(initParams));
	initParams.pfnMemAlloc = &mallocWrapper;
	initParams.pfnMemFree = &freeWrapper;

    unsigned int errorCode = lgDevInit(&initParams);
	int retry=0;
	while (errorCode == LGERR_USBD_REGISTRATION_ERROR && ++retry<10)
	{
		Printf("lgDevInit returned error %x - Retrying\n", errorCode);
		rage::sysIpcSleep(10);
		errorCode = lgDevInit(NULL);
	}

    // init our own structures...
    for(int ii = 0; ii < LG_MAX_CHANNELS; ii++)
    {
        validController[ii] = false;
        WheelHandles[ii] = LG_INVALID_DEVICE;
		deviceNumber[ii] = 0;
    }
    numDevices = 0;
}

unsigned int LogiWheels::GetDeviceNumber(unsigned char *Topology)
{
    unsigned int deviceNumber = Topology[0];
#if __PPU
	int i;
    // for some strange reason, Topology[0] is the wrong way
    // around. Topology[0] gives which USB port the device is
    // hooked up to on the PlayStation2 unit - the top port
    // is 2, the bottom 1.
    // Noteworthy detail: the Tool has the opposite wiring;
    // i.e., the ports are as follows:
    // Tool: Top = 1, Bottom = 2
    // Debug/Retail: Top = 2, Bottom = 1
    //
    // Let's reverse this to make it work right with debugs/retails:
    deviceNumber = (2 == deviceNumber) ? 1 : ((1 == deviceNumber) ? 2 : deviceNumber);
    for(i = 1; i < 8; i++)
    {
        // the following will fail if you find a hub
        // with more than 16 ports. good luck.
        // (and by the way, by default, USBD only
        // supports up to 8 ports on a hub)
        deviceNumber <<= 4;
        deviceNumber |= (Topology[i] & 0x0f);
    }
#endif // __PPU
    return deviceNumber;
}

// If function returns true, it means devices have been destroyed.
bool LogiWheels::EnumIsNeeded()
{
    lgDevEnumHint(&enumhint);

    if (enumhint == LGHINT_ENUMNEEDED)
    {
        return true;
    }

    return false;
}

void LogiWheels::ReadPort(int channel)
{
    int ret;

    // which channel did we read?
    lgDevPosition *readbuff = &Position[channel];

    // do asynchronous read if controller exists
    if(validController[channel])
    {
        // now we have the position report, do something with it:
        if(readbuff->Status == LGSTATUS_OK)
        {
            //Printf("Wheel%d: %d, Buttons %08x\n", channel, readbuff->X1, readbuff->Buttons);
        }
        PositionLast[channel] = Position[channel];
        // and submit a new read
        //mjc lgDevASync(LGASYNC_MODE_WAIT, &ret);
        ret = lgDevRead(WheelHandles[channel], LGREADMODE_IMMEDIATE, readbuff);
        //lgDevASync(LGASYNC_MODE_WAIT, &ret);

        if(LGFAILED(ret))
        {
#ifdef _DEBUG_BASIC
            Printf("ERROR: ReadPort on channel %d returned %d\n", channel, ret);
#endif
        }
    }
}

void LogiWheels::EnumerateDevices()
{
    int openeddevs=0;
    int unused=0;
    int index=0;
    // to keep track of which index is what
    unsigned int deviceIndex[LG_MAX_CHANNELS];
	memset(deviceIndex, 0, sizeof(deviceNumber));

    lgDevDeviceDesc description;

	int errorCode = 0;
    while(LGSUCCEEDED(errorCode=lgDevEnumerate(index, &description)))
    {
        if(index < LG_MAX_CHANNELS) // no more than 2 devices....
        {
            lgDevOpen(index, &WheelHandles[openeddevs]);
#ifdef _DEBUG_BASIC
            Printf("Found and opened Logitech USB device. Type: ");
#endif

            // disable centering spring
            lgDevSetDeviceProperty(WheelHandles[openeddevs], LGPROP_CENTERINGENABLED, LG_FALSE);

            if(description.DevClass == LGCLASS_WHEEL_A)
            {
#ifdef _DEBUG_BASIC
                Printf("Wheel class A\n");
#endif
                wheelClass[openeddevs] = LGCLASS_WHEEL_A;
                validController[openeddevs] = true;

                // calculate the "device number"
                deviceNumber[openeddevs] = GetDeviceNumber(description.Topology);
                deviceIndex[openeddevs] = openeddevs;

                openeddevs++;
            }
            else if(description.DevClass == LGCLASS_WHEEL_B)
            {
#ifdef _DEBUG_BASIC
                Printf("Wheel class B\n");
#endif
                wheelClass[openeddevs] = LGCLASS_WHEEL_B;
                validController[openeddevs] = true;

                // calculate the "device number"
                deviceNumber[openeddevs] = GetDeviceNumber(description.Topology);
                deviceIndex[openeddevs] = openeddevs;

                if (description.Flags & LGFLAG_MULTITURN)
                {
                    multiturnCapable[openeddevs] = true;
                }

                // Check for clutch
                if (LGAC_CLUTCH & description.AdditionalControls)
                {

                    m_hasClutch[openeddevs] = true;
                }

                // Check for gated shifter
                if (LGAC_GATED_SHIFTER & description.AdditionalControls)
                {
                    m_hasGatedShifter[openeddevs] = true;
                }

                openeddevs++;
            }
            else if(description.DevClass == LGCLASS_JOYSTICK_A)
            {
#ifdef _DEBUG_BASIC
                Printf("Joystick A\n");
#endif
                lgDevClose(WheelHandles[openeddevs]);
                WheelHandles[openeddevs] = LG_INVALID_DEVICE;
#ifdef _DEBUG_BASIC
                Printf("No good...close it...\n");
#endif
                unused++;
            }
            else if(description.DevClass == LGCLASS_JOYSTICK_B)
            {
#ifdef _DEBUG_BASIC
                Printf("Joystick B\n");
#endif
                lgDevClose(WheelHandles[openeddevs]);
                WheelHandles[openeddevs] = LG_INVALID_DEVICE;
#ifdef _DEBUG_BASIC
                Printf("No good...close it...\n");
#endif
                unused++;
            }
        }
        else
        {
#ifdef _DEBUG_BASIC
            Printf("More than enough Logitech USB devices !\n");
#endif
            unused++;
        }

        index++;
    }
	Printf("enum error code %x\n", errorCode);

    // Remember number of useful devices that are connected
    numDevices = openeddevs;

    // player number assignment:
    if(numDevices > 0)
    {
        int i;
        // sort our players according to the device number:
        for(i = 0; i < numDevices - 1; i++)
        {
            for(int j = i + 1; j < numDevices; j++)
            {
                if(deviceNumber[i] > deviceNumber[j])
                {
                    // Need to reverse things
                    unsigned int tempnum = deviceNumber[i];
                    deviceNumber[i] = deviceNumber[j];
                    deviceNumber[j] = tempnum;
                    unsigned int tempindex = deviceIndex[i];
                    deviceIndex[i] = deviceIndex[j];
                    deviceIndex[j] = tempindex;
                    unsigned int tempHandle = WheelHandles[i];
                    WheelHandles[i] = WheelHandles[j];
                    WheelHandles[j] = tempHandle;
                    unsigned int tempClass = wheelClass[i];
                    wheelClass[i] = wheelClass[j];
                    wheelClass[j] = tempClass;
                    bool tempController = validController[i];
                    validController[i] = validController[j];
                    validController[j] = tempController;
                    bool tempMultiturnCapable = multiturnCapable[i];
                    multiturnCapable[i] = multiturnCapable[j];
                    multiturnCapable[j] = tempMultiturnCapable;
                    bool tempHasClutch_ = m_hasClutch[i];
                    m_hasClutch[i] = m_hasClutch[j];
                    m_hasClutch[j] = tempHasClutch_;
                    bool tempHasGatedShifter_ = m_hasGatedShifter[i];
                    m_hasGatedShifter[i] = m_hasGatedShifter[j];
                    m_hasGatedShifter[j] = tempHasGatedShifter_;
                    int tempMultiturnCache = multiturnCache[i];
                    multiturnCache[i] = multiturnCache[j];
                    multiturnCache[j] = tempMultiturnCache;
                }
            }
        }
        // now, we have deviceNumber[0..numDevices-1] being the device numbers,
        // and deviceIndex[0..numDevices-1] being their indices into devices[]
    }

    // make sure the rest of our variables contain valid voidness
    for(int i = numDevices; i < LG_MAX_CHANNELS; i++)
    {
        validController[i] = false;
        WheelHandles[i] = LG_INVALID_DEVICE;
        deviceNumber[i] = 0;
    }

#ifdef _DEBUG_BASIC
    Printf("Total opened Logitech wheels: %d\n", numDevices);
#endif
}

void LogiWheels::DestroyDevices()
{
#ifdef _DEBUG_BASIC
    Printf("Destroying all Logitech Devices !\n");
#endif

    // delete old devices
    for(int i = 0; i < LG_MAX_CHANNELS; i++)
    {
        if(WheelHandles[i] != LG_INVALID_DEVICE)
        {
            lgDevClose(WheelHandles[i]);
			WheelHandles[i] = LG_INVALID_DEVICE;
        }
    }

    // Re-init all variables
    InitVars();
}

//////////////////////////////////////////////////////////////////////////////////
// Method: ButtonIsPressed(int channel, int buttonMask): Find out for a 		//
// certain channel if a certain button is pressed being pressed.	//
//										//
// Parameters: channel: the number of the channel the current device is	//
//		     connected to.						//
//	       buttonMask: the mask corresponding to a specific button.	//
//										//
// Returns: true if the button is currently pressed down			//
//////////////////////////////////////////////////////////////////////////////////
bool LogiWheels::ButtonIsPressed(int channel, int buttonMask)
{
    //mjc lgDevASync(LGASYNC_MODE_WAIT, NULL);

    switch(buttonMask)
    {
    case LG_DPAD_UP:
        if (DPadIsPressed(channel, LG_LOCAL_DPAD_UP))
        {
            return true;
        }
        break;
    case LG_DPAD_DOWN:
        if (DPadIsPressed(channel, LG_LOCAL_DPAD_DOWN))
        {
            return true;
        }
        break;
    case LG_DPAD_RIGHT:
        if (DPadIsPressed(channel, LG_LOCAL_DPAD_RIGHT))
        {
            return true;
        }
        break;
    case LG_DPAD_LEFT:
        if (DPadIsPressed(channel, LG_LOCAL_DPAD_LEFT))
        {
            return true;
        }
        break;
    }

    return ((Position[channel].Buttons & buttonMask)!=0);
}

//////////////////////////////////////////////////////////////////////////////////
// Method: ButtonTriggered(int channel, int button_mask): Find out for a           //
// certain channel or channel if a certain button has just been triggered.         //
//                                                                              //
// Parameters: channel: the number of the channel the current device is    //
//                   connected to.                                              //
//             button_mask: the mask corresponding to a specific button.        //
//                                                                              //
// Returns: true if the button has just been triggered				//
//////////////////////////////////////////////////////////////////////////////////
bool LogiWheels::ButtonTriggered(int channel, int buttonMask)
{
    //mjc lgDevASync(LGASYNC_MODE_WAIT, NULL);

    switch(buttonMask)
    {
    case LG_DPAD_UP:
        if (DPadTriggered(channel, LG_LOCAL_DPAD_UP))
        {
            return true;
        }
        break;
    case LG_DPAD_DOWN:
        if (DPadTriggered(channel, LG_LOCAL_DPAD_DOWN))
        {
            return true;
        }
        break;
    case LG_DPAD_RIGHT:
        if (DPadTriggered(channel, LG_LOCAL_DPAD_RIGHT))
        {
            return true;
        }
        break;
    case LG_DPAD_LEFT:
        if (DPadTriggered(channel, LG_LOCAL_DPAD_LEFT))
        {
            return true;
        }
        break;
    }

    return ( !(PositionLast[channel].Buttons & buttonMask) &&
             (Position[channel].Buttons & buttonMask));
}

bool LogiWheels::ButtonReleased(int channel, int buttonMask)
{
    //mjc lgDevASync(LGASYNC_MODE_WAIT, NULL);

    switch(buttonMask)
    {
    case LG_DPAD_UP:
        if (DPadReleased(channel, LG_LOCAL_DPAD_UP))
        {
            return true;
        }
        break;
    case LG_DPAD_DOWN:
        if (DPadReleased(channel, LG_LOCAL_DPAD_DOWN))
        {
            return true;
        }
        break;
    case LG_DPAD_RIGHT:
        if (DPadReleased(channel, LG_LOCAL_DPAD_RIGHT))
        {
            return true;
        }
        break;
    case LG_DPAD_LEFT:
        if (DPadReleased(channel, LG_LOCAL_DPAD_LEFT))
        {
            return true;
        }
        break;
    }

    return ( (PositionLast[channel].Buttons & buttonMask) &&
             !(Position[channel].Buttons & buttonMask));
}

bool LogiWheels::DPadIsPressed(int channel, int direction)
{
    //mjc lgDevASync(LGASYNC_MODE_WAIT, NULL);

    if (Position[channel].POV0 == 255)
        return false;

    switch (direction)
    {
    case LG_LOCAL_DPAD_UP:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_UP])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_RIGHT:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_RIGHT])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_DOWN:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_DOWN])
        {
            return true;
        }
        break;
    case LG_LOCAL_DPAD_LEFT:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_LEFT])
        {
            return true;
        }
        break;
    }
    return false;
}

bool LogiWheels::DPadTriggered(int channel, int direction)
{
    //mjc lgDevASync(LGASYNC_MODE_WAIT, NULL);

    if (Position[channel].POV0 == 255)
        return false;

    switch (direction)
    {
    case LG_LOCAL_DPAD_UP:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_UP] == 1)
        {
            if (dpadPosition[PositionLast[channel].POV0][LG_LOCAL_DPAD_UP] != 1)
                return true;
        }
        break;
    case LG_LOCAL_DPAD_RIGHT:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_RIGHT] == 1)
        {
            if (dpadPosition[PositionLast[channel].POV0][LG_LOCAL_DPAD_RIGHT] != 1)
                return true;
        }
        break;
    case LG_LOCAL_DPAD_DOWN:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_DOWN] == 1)
        {
            if (dpadPosition[PositionLast[channel].POV0][LG_LOCAL_DPAD_DOWN] != 1)
                return true;
        }
        break;
    case LG_LOCAL_DPAD_LEFT:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_LEFT] == 1)
        {
            if (dpadPosition[PositionLast[channel].POV0][LG_LOCAL_DPAD_LEFT] != 1)
                return true;
        }
        break;
    }
    return false;
}

bool LogiWheels::DPadReleased(int channel, int direction)
{
    //mjc lgDevASync(LGASYNC_MODE_WAIT, NULL);

    switch (direction)
    {
    case LG_LOCAL_DPAD_UP:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_UP] != 1)
        {
            if (dpadPosition[PositionLast[channel].POV0][LG_LOCAL_DPAD_UP] == 1)
                return true;
        }
        break;
    case LG_LOCAL_DPAD_RIGHT:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_RIGHT] != 1)
        {
            if (dpadPosition[PositionLast[channel].POV0][LG_LOCAL_DPAD_RIGHT] == 1)
                return true;
        }
        break;
    case LG_LOCAL_DPAD_DOWN:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_DOWN] != 1)
        {
            if (dpadPosition[PositionLast[channel].POV0][LG_LOCAL_DPAD_DOWN] == 1)
                return true;
        }
        break;
    case LG_LOCAL_DPAD_LEFT:
        if (dpadPosition[Position[channel].POV0][LG_LOCAL_DPAD_LEFT] != 1)
        {
            if (dpadPosition[PositionLast[channel].POV0][LG_LOCAL_DPAD_LEFT] == 1)
                return true;
        }
        break;
    }

    return false;
}

bool LogiWheels::WheelAMenuIsPressed(int channel, int direction)
{
    //mjc lgDevASync(LGASYNC_MODE_WAIT, NULL);
    switch (direction)
    {
    case LG_MENU_UP:
        // Range is 0 to 65535. Let's trigger when half of the range is reached
        if (Position[channel].Accelerator > 30000)
            return true;
        break;
    case LG_MENU_DOWN:
        if (Position[channel].Brake > 30000)
            return true;
        break;

        // Range is 0 to 32767. Trigger when half is reached = about 15000
    case LG_MENU_RIGHT:
        if (Position[channel].X1 > 15000)
            return true;
        break;
    case LG_MENU_LEFT:
        if (Position[channel].X1 < -15000)
            return true;
        break;
    }
    return false;
}

bool LogiWheels::WheelAMenuTriggered(int channel, int direction)
{
    //mjc lgDevASync(LGASYNC_MODE_WAIT, NULL);

    switch (direction)
    {
    case LG_MENU_UP:
        if ((Position[channel].Accelerator > 30000) && (PositionLast[channel].Accelerator < 30000))
            return true;
        break;
    case LG_MENU_DOWN:
        if ((Position[channel].Brake > 30000) && (PositionLast[channel].Brake < 30000))
            return true;
        break;
    case LG_MENU_RIGHT:
        if ((Position[channel].X1 > 15000) && (PositionLast[channel].X1 < 15000))
            return true;
        break;
    case LG_MENU_LEFT:
        if ((Position[channel].X1 < -15000) && (PositionLast[channel].X1 > -15000))
            return true;
        break;
    }
    return false;
}


bool LogiWheels::IsConnected(int channel)
{
    if (validController[channel])
    {
        return true;
    }
    return false;
}

bool LogiWheels::IsWheelClassA(int channel)
{
    return (wheelClass[channel] == LGCLASS_WHEEL_A);
}

bool LogiWheels::IsWheelClassB(int channel)
{
    return (wheelClass[channel] == LGCLASS_WHEEL_B);
}

bool LogiWheels::IsMultiturnCapable(int channel)
{
    return multiturnCapable[channel];
}

bool LogiWheels::IsMultiturnEnabled(int channel)
{
    int value = 0;
    if (LGSUCCEEDED(lgDevGetDeviceProperty(WheelHandles[channel], LGPROP_MULTITURN, &value)))
    {
        if (value != 0)
            return true;
    }
    else
    {
#ifdef _DEBUG_BASIC
        Printf("ERROR getting device multiturn property\n");
#endif
    }
    return false;
}

void LogiWheels::SetMultiturn(int channel, int value)
{
    if (LGSUCCEEDED(lgDevSetDeviceProperty(WheelHandles[channel], LGPROP_MULTITURN, value)))
    {
		multiturnCache[channel] = value;
    }
    else
    {
#ifdef _DEBUG_BASIC
        Printf("ERROR: could not change enable/disable multiturn\n");
#endif
    }
}

int LogiWheels::GetMultiturn(int channel)
{
	if(multiturnCache[channel] == -1)
	{
		return IsMultiturnEnabled(channel);
	}
	return multiturnCache[channel];
}

int LogiWheels::GetDeviceNumber(int channel)
{
	return deviceNumber[channel];
}

bool LogiWheels::IsClutchPresent(int channel)
{
    return m_hasClutch[channel];
}

bool LogiWheels::IsGatedShifterPresent(int channel)
{
    return m_hasGatedShifter[channel];
}

bool LogiWheels::IsGatedShifterSet(int channel)
{
    int value = 0;
    if (LGSUCCEEDED(lgDevGetDeviceProperty(WheelHandles[channel], LGPROP_GATED_SHIFTER, &value)))
    {
        if (value != 0)
            return true;
    }
    else
    {
#ifdef _DEBUG_BASIC
        Printf("ERROR getting device gated shifter property\n");
#endif
    }
    return false;
}


// nonLinCoeff between -100 and 100. 0 = linear, 100/-100 = maximum mon-linear.
void LogiWheels::GenerateNonLinValues(int channel, int nonLinCoeff)
{
    int ii;

    // Populate lookup table
    for (ii = 0; ii < 256; ii++)
    {
        NonLinearWheel[channel][ii] = (int)CalculateNonLinValue(ii, nonLinCoeff, nonLinMinOutput, nonLinMaxOutput);
    }
}

///////////////////////////////////////////////////////////////////////
// Method: calculateNonLinearValue(int inputValue, int nonLinearCoeff,
// float physicsMinInput, float physicsMaxInput)
//	   Method calculates a non-linear output value from a linear
//	   input value that corresponds to the Logitech wheel output.
//
// Arguments: inputValue: must be between 0 and 255. This corresponds
//		 directly to the wheel's position values.
//
//	     nonLinearCoeff: non-linearity coefficient which must be a
//	     value between -100 and 100.
//		0 corresponds to a completely linear response curve,
//      100 to a heavily non-linear response curve resulting in less
//      sensitivity around center, and -100 to a heavily non-linear 
//      response curve resulting in more sensitivity around center,
//
//	      physicsMinInput and physicsMaxInput: minimum and maximum
//		numbers that you want as output in your lookup table. For
//		example if your physics engine takes -1000 to 1000 as input
//		you may specify those values here.
//
// Returns: floating number which has a value between physicsMinInput
//		and physicsMaxInput and which reflects the chosen
//		non-linearity curve.
///////////////////////////////////////////////////////////////////////
float LogiWheels::CalculateNonLinValue(int inputValue, int nonLinearCoeff, float physicsMinInput, float physicsMaxInput)
{
    int MaxLookupTableInput = 255;  // These values correspond to the
    // wheel's position values.
    int MinLookupTableInput = 0;
    float outputValue;

    // In order to center our curve on the X axis let's calculate the
    // center offset value
    float centerOffset = (float)(MaxLookupTableInput -
                                 MinLookupTableInput) / 2;

    // Calculate maximum on x axis for the centered curve
    float centeredCurveMax = MaxLookupTableInput - centerOffset;

    // Normalize non-linear coefficient
    float nonLinearCoeffNormalized = (float)nonLinearCoeff/100;

    // Normalize input value
    outputValue=((float)(inputValue - centerOffset))/centeredCurveMax;

    // Apply a cubical curve
    outputValue=(((physicsMaxInput - physicsMinInput)/2)*((1.0f-nonLinearCoeffNormalized)*outputValue+(nonLinearCoeffNormalized)
                                                          *(outputValue*outputValue*outputValue))) + ((physicsMaxInput + physicsMinInput)/2);
    return outputValue;
}

#endif // HAVE_FFWHEEL

