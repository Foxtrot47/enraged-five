// 
// ffwheel/liblgFF_xenon.cpp 
// 
// This is a Xenon wrapper for the Logitech function calls
// Allows us to use the Logitech EZ Wheel wrapper classes without too many Xenon specific changes
// The setup of both systems is so darn similar that I suspect Logitech was involved with the implementation of the 360 wheel classes.
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if __XENON

#pragma comment(lib, "Xffb.lib")
#include "diag/output.h"
#include "system/xtl.h"
#include "system/ipc.h"
#include "system/messagequeue.h"
#include "grprofile/pix.h"
#include <Xffb.h>
#include "liblgFF.h"

#define MAX_USERS (4)		// number of users (max number of players)
#define MAX_EFFECT_HANDLES (64)
#define EFFECT_MESSAGE_QUEUE_SIZE (32)

using namespace rage;

// define the message that we can use to send force effects from the main game thread to the ff worker thread (some of the 360 ff calls can take a while)
struct lgDev360EffectMessage {
	enum {
		eFFMSG_SetEEffect,
		eFFMSG_UpdateEEffect,
		eFFMSG_EffectOperationStart,
		eFFMSG_EffectOperationStop,
		eFFMSG_Sync,					// Finish processing all outstanding ff requests (if we wait on the main thread after issuing this request then we know there are no currently processing ff requests
		eFFMSG_Quit,
	} m_Action;
	int m_EffectId;
	lgDevForceEffect m_ForceEffect;
};

static int s_ThreadCPU = 2;
static bool s_Initialized = false;
static HANDLE s_InputNotifyListener;
static sysIpcThreadId s_WorkerThread;
static int s_MaxEffectIndex[MAX_USERS];
static int s_EffectHandles[MAX_USERS][MAX_EFFECT_HANDLES];
static bool s_NeedEnumeration = false;
static sysMessageQueue<lgDev360EffectMessage,EFFECT_MESSAGE_QUEUE_SIZE,true> s_FFRequests;	// queue to send events from game to wheel


// forward declaration
static void lgDev360ThreadWorker(void*);



// wait for any outstanding forcefeedback messages to get processed by the other thread
static void WaitForAllOutstandingFFJobs()
{
	// ugh, just push enough messages to fill the queue (and the Push is set to block if full)
	// we shouldn't be having to do this in game fame (defeats the object of having it in a queue)
	lgDev360EffectMessage r;
	for(int i=0;i<EFFECT_MESSAGE_QUEUE_SIZE;i++)
	{
		r.m_Action = lgDev360EffectMessage::eFFMSG_Sync;
		s_FFRequests.Push(r);
	}
}

// Must set thread cpu before system initialization
int lgDevSetThreadCPU(int threadCPU)
{
	if(s_Initialized)
		return LG_ERROR;

	s_ThreadCPU = threadCPU;

	return LG_SUCCESS;
}

int lgDevInit(const lgDevInitParams *UNUSED_PARAM(initparams))
{
	Assert(s_Initialized==false);
	memset(s_MaxEffectIndex, 0, sizeof(s_MaxEffectIndex));
	s_InputNotifyListener = XNotifyCreateListener(XNOTIFY_SYSTEM);
	s_WorkerThread = sysIpcCreateThread(lgDev360ThreadWorker,0,sysIpcMinThreadStackSize,PRIO_BELOW_NORMAL,"[RAGE] ForceFeedback",s_ThreadCPU);

	s_NeedEnumeration = true;
	s_Initialized = true;
	return LG_SUCCESS;
}

int lgDevDeInit(void)
{
	Assert(s_Initialized==true);
	CloseHandle(s_InputNotifyListener);
	s_InputNotifyListener = NULL;
	s_Initialized = false;
	s_NeedEnumeration = false;
	return LG_SUCCESS;
}

int lgDevEnumHint(int * hint)
{
	//Assert(0 && "lgDevEnumHint not supported on Xenon");
	Assert(hint);
	if (!s_Initialized)
		return LG_ERROR;

	// mjc - need to only return this if device has been plugged/unplugged, or if not yet initialised in this library
	if (s_NeedEnumeration==true)
	{
		*hint = LGHINT_ENUMNEEDED;
	}
	else
	{
		*hint=LGHINT_NOENUMNEEDED;
	}
	return LG_SUCCESS;
}

// Find the nth wheel within the list of connected controllers
//   (0 is the first wheel that is plugged in - even if it is the fourth connected game controller)
//	 Outputs the input capabilities for the controller and the userIndex (port number) of the controller
bool FindNthWheelController(int wheelIndex, XINPUT_CAPABILITIES * pxCapabilities, DWORD * dwpUserIndex)
{
	int numValidWheelsFound = 0;
	for(int i=0;i<XUSER_MAX_COUNT ;i++)
	{
		DWORD result = XInputGetCapabilities(i,0,pxCapabilities);
		if ((result == ERROR_SUCCESS) &&
			(pxCapabilities->Type == XINPUT_DEVTYPE_GAMEPAD) &&
			(pxCapabilities->SubType == XINPUT_DEVSUBTYPE_WHEEL) &&
			((pxCapabilities->Flags & XINPUT_CAPS_FFB_SUPPORTED) != 0))
		{
			if (numValidWheelsFound == wheelIndex)
			{
				// found the wheel we want
				*dwpUserIndex = i;
				return true;
			}
			else
				numValidWheelsFound++;
		}
	}
	return false;

}

int lgDevEnumerate(int index, lgDevDeviceDesc *description)
{
	Assert(description);

	// cap the max number of users/devices
	if (index>=MAX_USERS)
		return LGERR_NO_MORE_DEVICES;

	// may need to split this flag into a flag per device 
	s_NeedEnumeration = false;

	WaitForAllOutstandingFFJobs();

	/*
	XINPUT_CAPABILITIES xCapabilities;
	DWORD dwUserIndex;
	if (FindNthWheelController(index, &xCapabilities, &dwUserIndex))
	{
		// found the wheel we want
		description->DevClass = LGCLASS_WHEEL_B;
		description->AdditionalControls = 0; // mjc - which flags to set here?
		description->Flags = LGFLAG_FORCEFEEDBACK|LGFLAG_CENTERINGDISABLEABLE;
		memset(description->Topology, 0, sizeof(description->Topology));
		description->ExistingHandle = LG_INVALID_DEVICE;

		return LG_SUCCESS;
	}
	*/

	XINPUT_CAPABILITIES xCapabilities;
	DWORD result = XInputGetCapabilities(index,0,&xCapabilities);
	if ((result == ERROR_SUCCESS) &&
		(xCapabilities.Type == XINPUT_DEVTYPE_GAMEPAD) &&
		(xCapabilities.SubType == XINPUT_DEVSUBTYPE_WHEEL) &&
		((xCapabilities.Flags & XINPUT_CAPS_FFB_SUPPORTED) != 0))
	{
		// found a valid wheel
		description->DevClass = LGCLASS_WHEEL_B;
		description->AdditionalControls = 0; // mjc - which flags to set here?
		description->Flags = LGFLAG_FORCEFEEDBACK|LGFLAG_CENTERINGDISABLEABLE;
		memset(description->Topology, 0, sizeof(description->Topology));
		description->Topology[0] = char( index+1 );	// device index
		description->ExistingHandle = LG_INVALID_DEVICE;
		return LG_SUCCESS;
	}
	else if ((result == ERROR_SUCCESS) &&
		(xCapabilities.Type == XINPUT_DEVTYPE_GAMEPAD) &&
		(xCapabilities.SubType == XINPUT_DEVSUBTYPE_WHEEL) /*&&
		((xCapabilities.Flags & XINPUT_CAPS_FFB_SUPPORTED) != 0)*/)
	{
		// found a valid wheel
		description->DevClass = LGCLASS_JOYSTICK_A;	// DriveFX
		description->AdditionalControls = 0; // mjc - which flags to set here?
		description->Flags = 0;
		memset(description->Topology, 0, sizeof(description->Topology));
		description->Topology[0] = char( index+1 );	// device index
		description->ExistingHandle = LG_INVALID_DEVICE;
		return LG_SUCCESS;
	}
	else
	{
		// not a valid wheel, but we want to return something still (so the library thinks there is a devide here...)
		description->DevClass = LGCLASS_JOYSTICK_B;
		description->AdditionalControls = 0; // mjc - which flags to set here?
		description->Flags = 0;
		memset(description->Topology, 0, sizeof(description->Topology));
		description->Topology[0] = char( index+1 );	// device index
		description->ExistingHandle = LG_INVALID_DEVICE;
		return LG_SUCCESS;
	}

	//return LG_ERROR;
	//return LGERR_NO_MORE_DEVICES;
}


int lgDevOpen(int index, int * device)
{
	Assert(index < MAX_USERS);
	Assert(device);

	WaitForAllOutstandingFFJobs();

	XINPUT_CAPABILITIES xCapabilities;
	DWORD dwUserIndex = index;
	DWORD result = XInputGetCapabilities(index,0,&xCapabilities);
	if ((result == ERROR_SUCCESS) &&
		(xCapabilities.Type == XINPUT_DEVTYPE_GAMEPAD) &&
		(xCapabilities.SubType == XINPUT_DEVSUBTYPE_WHEEL) &&
		((xCapabilities.Flags & XINPUT_CAPS_FFB_SUPPORTED) != 0))
	{
		// Force-feedback MaxIndex, MaxSimultaneousEffects (XINPUT_FF_DEVICE_INFO) may not be available for 2205 ms after xCapabilities
		Sleep( 100 );

		XINPUT_FF_DEVICE_INFO xDeviceInfo;
		if (XInputFFGetDeviceInfo(dwUserIndex, &xDeviceInfo) != ERROR_SUCCESS)
			return LG_ERROR;

		s_MaxEffectIndex[index] = xDeviceInfo.MaxIndex;
		if (s_MaxEffectIndex[index] > MAX_EFFECT_HANDLES)
			s_MaxEffectIndex[index] = MAX_EFFECT_HANDLES;

		memset(s_EffectHandles[index], 0, sizeof(s_EffectHandles[index]));

		if (XInputFFResetDevice(dwUserIndex, NULL) == ERROR_SUCCESS)
		{
			*device = dwUserIndex;
			return LG_SUCCESS;
		}
		else
			return LG_ERROR;
	}
	else if ((result == ERROR_SUCCESS) &&
		(xCapabilities.Type == XINPUT_DEVTYPE_GAMEPAD) &&
		(xCapabilities.SubType == XINPUT_DEVSUBTYPE_WHEEL) /*&&
		((xCapabilities.Flags & XINPUT_CAPS_FFB_SUPPORTED) != 0)*/)
	{
		s_MaxEffectIndex[index] = 0;

		memset(s_EffectHandles[index], 0, sizeof(s_EffectHandles[index]));

		*device = dwUserIndex;
		return LG_SUCCESS;

	}
	else
	{
		// not a wheel - may be a pad - may not be connected
		s_MaxEffectIndex[index] = 0;
		return LG_ERROR;
	}

}

int lgDevClose(int device)
{
	// do we need to do anything here - certainly mark device for needing reopening...
	if (device<0)
		return LG_ERROR;

	WaitForAllOutstandingFFJobs();

	// for now do a device reset
	if (XInputFFResetDevice(device, NULL) == ERROR_SUCCESS)
		return LG_SUCCESS;
	else
		return LG_ERROR;
}

int lgDevGetDeviceProperty(int UNUSED_PARAM(device), int property, int * value)
{
	Assert(value);
	switch(property) {
	case LGPROP_MULTITURN:
		*value = 0;	// not a 900degree wheel on 360
		break;
	default:
		*value = 0;
		break;
	}
	return LG_SUCCESS;
}

int lgDevSetDeviceProperty(int UNUSED_PARAM(device), int UNUSED_PARAM(property), int UNUSED_PARAM(value))
{
	return LG_SUCCESS;
}

int lgDevRead(int device, int UNUSED_PARAM(readmode), lgDevPosition *position)
{
	Assert(position);

	//int queueCount = s_FFRequests.m_Count;
	//if(queueCount)
	//	Displayf("FF Queue Count: %d",queueCount);

	// should we read the controller in here - or use the main pad_xenon code?
	XINPUT_STATE xControllerState;
	if (XInputGetState(device, &xControllerState) == ERROR_SUCCESS)
	{
		position->X1 = xControllerState.Gamepad.sThumbLX;
		position->Y1 = (short(xControllerState.Gamepad.bLeftTrigger)<<7) - (short(xControllerState.Gamepad.bRightTrigger)<<7);
		position->Accelerator = short(xControllerState.Gamepad.bRightTrigger)<<8;
		position->Brake = short(xControllerState.Gamepad.bLeftTrigger)<<8;
		return LG_SUCCESS;
	}
	return LG_ERROR;
}

int lgDevReadMulti(int UNUSED_PARAM(devicecount), int *UNUSED_PARAM(devices), int *UNUSED_PARAM(readmodes), lgDevPosition *UNUSED_PARAM(positions))
{
	Assert(0 && "lgDevReadMulti not supported on Xenon");
	return LG_SUCCESS;
}



// Convert from a logitech effect to a 360 effect
static void ConvertForceEffect(const lgDevForceEffect * const forceeffect, int iEffectIndex/*not in the logitech struct*/, XINPUT_FF_EFFECT & xEffect )
{
	memset(&xEffect, 0, sizeof(xEffect));
	xEffect.EffectIndex = iEffectIndex;

	switch(forceeffect->Type) {
	case LGTYPE_CONSTANT:
		xEffect.EffectType = XINPUT_FFEFFECT_CONSTANT;
		break;
	case LGTYPE_RAMP:
		xEffect.EffectType = XINPUT_FFEFFECT_RAMP;
		break;
	case LGTYPE_SINE:
		xEffect.EffectType = XINPUT_FFEFFECT_PERIODIC_SINE;
		break;
	case LGTYPE_SQUARE:
		xEffect.EffectType = XINPUT_FFEFFECT_PERIODIC_SQUARE;
		break;
	case LGTYPE_TRIANGLE:
		xEffect.EffectType = XINPUT_FFEFFECT_PERIODIC_TRIANGLE;
		break;
	case LGTYPE_SAWTOOTHUP:
		xEffect.EffectType = XINPUT_FFEFFECT_PERIODIC_SAWTOOTHUP;
		break;
	case LGTYPE_SAWTOOTHDOWN:
		xEffect.EffectType = XINPUT_FFEFFECT_PERIODIC_SAWTOOTHDOWN;
		break;
	case LGTYPE_SPRING:
		xEffect.EffectType = XINPUT_FFEFFECT_CONDITION_SPRING;
		break;
	case LGTYPE_DAMPER:
		xEffect.EffectType = XINPUT_FFEFFECT_CONDITION_DAMPER;
		break;
	}

	xEffect.Axes = XINPUT_FFAXIS_X;
	xEffect.Gain = 255;									// not in the logitech library?
	xEffect.Duration = forceeffect->Duration;			// in ms
	xEffect.StartDelay = forceeffect->StartDelay;		// in ms
	xEffect.LoopCount = 1; // ???

	lgDevEnvelopeParams envelope;
	memset(&envelope, 0, sizeof(envelope));	// some effects will not set an envelope

	switch(forceeffect->Type) {
	case LGTYPE_CONSTANT:
		envelope = forceeffect->p.constant.envelope;
		xEffect.ConstantForce.Magnitude = -(forceeffect->p.constant.Magnitude>>1);	// -128to128 range on Xbox, -256to255 on logitech
		//xEffect.ConstantForce.Magnitude = forceeffect->p.constant.Magnitude>>1;	// -128to128 range on Xbox, -256to255 on logitech
		//if (forceeffect->p.constant.Direction < 180)
		//	xEffect.ConstantForce.Magnitude = -xEffect.ConstantForce.Magnitude;
		//what to do with forceeffect->p.constant.Direction
		break;
	case LGTYPE_RAMP:
		xEffect.RampForce.Start = forceeffect->p.ramp.MagnitudeStart>>1;	// -128to128 range on Xbox, -256to255 on logitech
        xEffect.RampForce.End = forceeffect->p.ramp.MagnitudeEnd>>1;		// -128to128 range on Xbox, -256to255 on logitech
		//what to do with forceeffect->p.ramp.Direction
		//no envelope
		break;
	case LGTYPE_SINE:
	case LGTYPE_SQUARE:
	case LGTYPE_TRIANGLE:
	case LGTYPE_SAWTOOTHUP:
	case LGTYPE_SAWTOOTHDOWN:
		envelope = forceeffect->p.periodic.envelope;
		xEffect.PeriodicForce.Magnitude = forceeffect->p.periodic.Magnitude>>1;	// -128to128 range on Xbox, -256to255 on logitech
        xEffect.PeriodicForce.Offset = forceeffect->p.periodic.Offset>>1;		// -128to128 range on Xbox, -256to255 on logitech
		xEffect.PeriodicForce.Phase = forceeffect->p.periodic.Phase>>1;			// -128to128 range on Xbox, -256to255 on logitech
		xEffect.PeriodicForce.Period = forceeffect->p.periodic.Period;			// in ms
		//what to do with forceeffect->p.periodic.Direction
		break;
	case LGTYPE_SPRING:
	case LGTYPE_DAMPER:
		for(int i=0;i<2;i++)	// 2 condition forces on logitech (3 on xbox)
		{
			xEffect.ConditionForce[i].CenterOffset = forceeffect->p.condition[i].Offset >>8;				// -128to128 range on Xbox, -32768to32767 on logitech
			xEffect.ConditionForce[i].PositiveCoefficient = forceeffect->p.condition[i].CoefficientPos>>1;	// -128to128 range on Xbox, -256to255 on logitech
			xEffect.ConditionForce[i].NegativeCoefficient = forceeffect->p.condition[i].CoefficientNeg>>1;	// -128to128 range on Xbox, -256to255 on logitech
			xEffect.ConditionForce[i].PositiveSaturation = forceeffect->p.condition[i].SaturationPos;		// 0to255 on both platforms
			xEffect.ConditionForce[i].NegativeSaturation = forceeffect->p.condition[i].SaturationNeg;		// 0to255 on both platforms
			xEffect.ConditionForce[i].Deadband = forceeffect->p.condition[i].Deadband>>8;					// 0to255 range on Xbox, 0to65535 on logitech
		}
		//no envelope
		break;
	default:
		Assert(0);
		break;
	}
		
	// times in ms and levels in 0-255 range
	xEffect.Envelope.AttackLevel = envelope.AttackLevel;
	xEffect.Envelope.FadeLevel = envelope.FadeLevel;
	xEffect.Envelope.AttackTime = envelope.AttackTime;
	xEffect.Envelope.FadeTime = envelope.FadeTime;
}






int lgDevDownloadForceEffect(int device, int *effectid, const lgDevForceEffect *forceeffect)
{
	// find a free effect handle slot
	Assert(device>=0);
	*effectid = 0;
	for(int i=0;i<s_MaxEffectIndex[device];i++)
	{
		if (s_EffectHandles[device][i] == 0)
		{
			*effectid = (i+1) | (device<<8);		// pack device number in top 8 bits of effectid
			s_EffectHandles[device][i] = i+1;
			break;
		}
	}
	if (*effectid == 0)
		return LGERR_OUT_OF_MEMORY;

	lgDev360EffectMessage r;
	r.m_Action = lgDev360EffectMessage::eFFMSG_SetEEffect;
	r.m_EffectId = *effectid;
	r.m_ForceEffect = *forceeffect;
	if(s_FFRequests.Push(r) == NULL)
		return LGERR_OUT_OF_MEMORY;

	return LG_SUCCESS;
}

int lgDevUpdateForceEffect(int effectid, const lgDevForceEffect *forceeffect)
{
	if (effectid<=0)
		return LGERR_INVALID_PARAMETER;

	lgDev360EffectMessage r;
	r.m_Action = lgDev360EffectMessage::eFFMSG_UpdateEEffect;
	r.m_EffectId = effectid;
	r.m_ForceEffect = *forceeffect;
	if(s_FFRequests.Push(r) == NULL)
		return LGERR_OUT_OF_MEMORY;

	return LG_SUCCESS;
}

int lgDevStartForceEffect(int effectid)
{
	if (effectid<=0)
		return LGERR_INVALID_PARAMETER;

	lgDev360EffectMessage r;
	r.m_Action = lgDev360EffectMessage::eFFMSG_EffectOperationStart;
	r.m_EffectId = effectid;
	if(s_FFRequests.Push(r) == NULL)
		return LGERR_OUT_OF_MEMORY;

	return LG_SUCCESS;
}

int lgDevStartForceEffectMulti(int UNUSED_PARAM(numeffects), int *UNUSED_PARAM(effectids))
{
	Assert(0 && "lgDevStartForceEffectMulti not supported on Xenon");
	return LG_SUCCESS;
}

int lgDevStopForceEffect(int effectid)
{
	if (effectid<=0)
		return LGERR_INVALID_PARAMETER;

	lgDev360EffectMessage r;
	r.m_Action = lgDev360EffectMessage::eFFMSG_EffectOperationStop;
	r.m_EffectId = effectid;
	if(s_FFRequests.Push(r) == NULL)
		return LGERR_OUT_OF_MEMORY;

	return LG_SUCCESS;
}

int lgDevStopForceEffectMulti(int UNUSED_PARAM(numeffects), int *UNUSED_PARAM(effectids))
{
	Assert(0 && "lgDevStopForceEffectMulti not supported on Xenon");
	return LG_SUCCESS;
}

int lgDevDestroyForceEffect(int effectid)
{
	if (effectid<=0)
		return LGERR_INVALID_PARAMETER;

	lgDevStopForceEffect(effectid);

	Assert(s_EffectHandles[0][effectid-1] == effectid);
	s_EffectHandles[0][effectid-1] = 0;

	return LG_SUCCESS;
}




int lgDev360Update()
{
	DWORD dwNotificationId = 0;
    ULONG_PTR ulParam;
	while (XNotifyGetNext(s_InputNotifyListener, 0, &dwNotificationId, &ulParam) != 0)
	{
		switch(dwNotificationId)
		{
		case XN_SYS_INPUTDEVICESCHANGED:
			// Controller connected or disconnected from system
			break;
		case XN_SYS_INPUTDEVICECONFIGCHANGED:
			{
				// possibly the power was restored/removed - possibly we called reset on the device
				// ulParam is the user index for the controller
				s_NeedEnumeration = true;
			}
			break;
		case XN_SYS_UI:
			{
				// Displaying or hiding the system interface
				DWORD dwUIDisplaying = (DWORD)ulParam;
				if (dwUIDisplaying)
				{
					// shutdown all ff effects
				}
				else
				{
					// restart all ff effects
				}
			}
			break;
		default:
			break;
		}
	}
	return LG_SUCCESS;
}




/*
 * We create a thread to service Force Feedback requests on 360 (may need to migrate this code to PS3 also)
 */
#if __XENON
#pragma warning(disable: 4702)		// Suppress bogus "unreachable code" warning reported by LTCG builds.
#endif

DECLARE_THREAD_FUNC(lgDev360ThreadWorker) {
	if (ptr) ptr = 0;	// shut compiler up
	for (;;) {

		PIXBegin(0, "360ThreadWorker");

		lgDev360EffectMessage r = s_FFRequests.Pop();			// make a copy of the request because it might get stomped on by the game thread!
		if (r.m_Action == lgDev360EffectMessage::eFFMSG_Quit)	// quit request
			break;
		const int effectId = (r.m_EffectId & 0xff);				// actual effectid (bottom 8 bits of effectid)
		const DWORD dwUserIndex = (r.m_EffectId >> 8);			// device (packed into top 8 bits of effectid)
		DWORD dwError = ERROR_SUCCESS;

		switch(r.m_Action)
		{
		case lgDev360EffectMessage::eFFMSG_SetEEffect:
			{
				XINPUT_FF_EFFECT xEffect;
				ConvertForceEffect( &r.m_ForceEffect, effectId, xEffect );
				dwError = XInputFFSetEffect(dwUserIndex, &xEffect, 1, NULL);
				Assert(dwError == ERROR_SUCCESS);
			}
			break;
		case lgDev360EffectMessage::eFFMSG_UpdateEEffect:
			{
				XINPUT_FF_EFFECT xEffect;
				ConvertForceEffect( &r.m_ForceEffect, effectId, xEffect );
				dwError = XInputFFUpdateEffect(dwUserIndex, &xEffect, XINPUT_FFFLAG_EFFECT|XINPUT_FFFLAG_ENVELOPE|XINPUT_FFFLAG_PARAMETERS, NULL);
				Assert(dwError == ERROR_SUCCESS);
			}
			break;
		case lgDev360EffectMessage::eFFMSG_EffectOperationStart:
			{
				XINPUT_FF_EFFECT_OPERATION xOperation;
				xOperation.EffectIndex = effectId;
				xOperation.Operation = XINPUT_FFOPERATION_START;
				dwError = XInputFFEffectOperation(dwUserIndex, &xOperation, 1, XINPUT_FFOPERATION_STOP, NULL);	// what is the native operation?  For now turn it off
				Assert(dwError == ERROR_SUCCESS);
			}
			break;
		case lgDev360EffectMessage::eFFMSG_EffectOperationStop:
			{
				XINPUT_FF_EFFECT_OPERATION xOperation;
				xOperation.EffectIndex = effectId;
				xOperation.Operation = XINPUT_FFOPERATION_STOP;
				dwError = XInputFFEffectOperation(dwUserIndex, &xOperation, 1, XINPUT_FFOPERATION_STOP, NULL);	// what is the native operation?  For now turn it off
				Assert(dwError == ERROR_SUCCESS);
			}
			break;
		case lgDev360EffectMessage::eFFMSG_Sync:
			// do nothing - the mere act of processing the request performs the sync!
			break;
		default:
			break;
		}

		PIXEnd();
	}
}

#if __XENON
#pragma warning(error: 4702)
#endif






//namespace rage {



//} // namespace rage


#endif // __XENON

