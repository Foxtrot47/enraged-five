/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#ifndef LOGI_CONTROLLER_MANAGER_H_INCLUDED_
#define LOGI_CONTROLLER_MANAGER_H_INCLUDED_

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiGlobals.h"
#include "LogiController.h"

#if __PS3
#include <cell/pad.h>
#endif

namespace LogitechEZWheelWrapper
{
    class ControllerManager
    {
    public:
        ControllerManager();
        ~ControllerManager();

        Controller* GetController(const LG_INT ctrlNbr);

        // keep track separately of multiturn status in order to reset it in case enumeration happens.
        LG_VOID SetMultiturnFlag(const LG_INT ctrlNbr, const LG_BOOL multiturnEnabled) { m_multiturnEnabled[ctrlNbr] = multiturnEnabled; }
        LG_BOOL GetMultiturnFlag(const LG_INT ctrlNbr) { return m_multiturnEnabled[ctrlNbr]; }

        LG_VOID Update();

    private:
        Controller* m_controller[LG_MAX_CONTROLLERS];
        LG_BOOL m_previousConnectionStatus[LG_MAX_CONTROLLERS];
        LG_INT m_enumHint;
        LG_BOOL m_multiturnEnabled[LG_MAX_CONTROLLERS]; // keep track of multiturn so it can be reset if enumeration happens

        LG_VOID Init();
        LG_BOOL EnumIsNeeded();
        LG_VOID EnumerateDevices();
        LG_VOID DeleteAllControllers();
        LG_VOID ReOrderControllers();
        LG_VOID ReadAll();

#if __PS3
		CellPadInfo2 m_lastPadInfo;
        LG_BOOL PadChanged(const CellPadInfo2 &padInfo1, const CellPadInfo2 &padInfo2, const LG_INT index);
        LG_BOOL PadChanged(const CellPadInfo2 &padInfo1, const CellPadInfo2 &padInfo2);
#endif

    };
}

#endif

#endif // LOGI_CONTROLLER_MANAGER_H_INCLUDED_
