#ifndef VEH_PLAYERINPUTFF
#define VEH_PLAYERINPUTFF

#include "veh_dyna/playerinput.h"

class vehPlayerInputFF: public vehPlayerInput {
public:
	friend class	vehInputTune;
public:
	void	UpdateFFWheel();
	void    UpdateFFWheelEffects();
};

#endif
