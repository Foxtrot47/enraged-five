/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#include "LogiForceManager.h"

#if HAVE_FFWHEEL

#include "LogiUtils.h"
#include <string.h>

#if __PS3
#define LG_COLLISION_START_DELAY 200
#elif __XENON
#define LG_COLLISION_START_DELAY 0
#else
#define LG_COLLISION_START_DELAY 0
#endif

using namespace LogitechEZWheelWrapper;

ForceManager::ForceManager()
{
    Init();
}

ForceManager::~ForceManager()
{

}

LG_VOID ForceManager::Init()
{
    m_wheelHandle = LG_INVALID_DEVICE;
    m_isAirborne = false;

    m_force.clear();

    for (LG_INT ii = 0; ii < LG_NUMBER_FORCE_EFFECTS; ii++)
    {
        Force force_;
        force_.SetForceType(static_cast<ForceType>(ii));
        m_force.push_back(force_);
    }
}

LG_BOOL ForceManager::IsPlaying(const ForceType forceType)
{
    if (LG_FORCE_CAR_AIRBORNE == forceType)
    {
        return m_isAirborne;
    }

    return m_force[forceType].IsPlaying();
}

LG_VOID ForceManager::PlaySpringForce(const LG_INT offsetPercentage, const LG_INT saturationPercentage, const LG_INT coefficientPercentage)
{
    if (offsetPercentage < LG_MIN_PERCENTAGE || offsetPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlaySpringForce offsetPercentage is out of bounds (%d)\n", offsetPercentage);
    if (saturationPercentage < 0 || saturationPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlaySpringForce saturationPercentage is out of bounds (%d)\n", saturationPercentage);
    if (coefficientPercentage < LG_MIN_PERCENTAGE || coefficientPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlaySpringForce coefficientPercentage is out of bounds (%d)\n", coefficientPercentage);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlaySpringForce but wheel handle is NULL\n");
        return;
    }

    if (m_isAirborne)
        return;

    // only play spring force if slippery road effect is not playing (slippery effect is a damper with negative coefficient and takes
    // precedence on the spring)
    if (IsPlaying(LG_FORCE_SLIPPERY_ROAD))
        return;

    LG_INT offset_ = Utils::FromPercentage(offsetPercentage, LG_MIN_PERCENTAGE, LG_MAX_PERCENTAGE, LG_SIGNED_16_BIT_RANGE_MIN, LG_SIGNED_16_BIT_RANGE_MAX);
    LG_INT saturation_ = Utils::FromPercentage(saturationPercentage, 0, LG_MAX_PERCENTAGE, 0, LG_8_BIT_RANGE_MAX);
    LG_INT coefficient_ = Utils::FromPercentage(coefficientPercentage, LG_MIN_PERCENTAGE, LG_MAX_PERCENTAGE, LG_8_BIT_RANGE_MIN, LG_8_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

    newEffect_.Type = LGTYPE_SPRING;
    newEffect_.Duration = LGDURATION_INFINITE;
    newEffect_.p.condition[0].Offset = offset_;
    newEffect_.p.condition[0].SaturationNeg = saturation_;
    newEffect_.p.condition[0].SaturationPos = saturation_;
    newEffect_.p.condition[0].CoefficientNeg = coefficient_;
    newEffect_.p.condition[0].CoefficientPos = coefficient_;
    newEffect_.p.condition[1] = newEffect_.p.condition[0];

    PlayEffect(LG_FORCE_SPRING, &newEffect_);
}

LG_VOID ForceManager::StopSpringForce()
{
	if(m_force[LG_FORCE_SPRING].IsPlaying())
	{
		m_force[LG_FORCE_SPRING].Stop();
	}
}

LG_VOID ForceManager::PlayConstantForce(const LG_INT strengthPercentage)
{
    if (strengthPercentage < LG_MIN_PERCENTAGE || strengthPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlayConstantForce strengthPercentage is out of bounds (%d)\n", strengthPercentage);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlayConstantForce but wheel handle is NULL\n");
        return;
    }

    if (m_isAirborne)
        return;

    // calculate magnitude and create new force effect
    LG_INT magnitude_ = Utils::FromPercentage(strengthPercentage, LG_MIN_PERCENTAGE, LG_MAX_PERCENTAGE, LG_8_BIT_RANGE_MIN, LG_8_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

    newEffect_.Type = LGTYPE_CONSTANT;
    newEffect_.Duration = LGDURATION_INFINITE;
    newEffect_.p.constant.Magnitude = magnitude_;
    newEffect_.p.constant.Direction = 90;

    PlayEffect(LG_FORCE_CONSTANT, &newEffect_);
}

LG_VOID ForceManager::StopConstantForce()
{
	//if(m_force[LG_FORCE_CONSTANT].IsPlaying())
	{
		m_force[LG_FORCE_CONSTANT].Stop();
	}
}

LG_VOID ForceManager::PlayDamperForce(const LG_INT strengthPercentage)
{
    if (strengthPercentage < LG_MIN_PERCENTAGE || strengthPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlayDamperForce strengthPercentage is out of bounds (%d)\n", strengthPercentage);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlayDamperForce but wheel handle is NULL\n");
        return;
    }

    if (m_isAirborne)
        return;

    // only play spring force if slippery road effect is not playing (slippery effect is a damper with negative coefficient and takes
    // precedence on the spring)
    if (IsPlaying(LG_FORCE_SLIPPERY_ROAD))
        return;

    LG_INT coefficient_ = Utils::FromPercentage(strengthPercentage, LG_MIN_PERCENTAGE, LG_MAX_PERCENTAGE, LG_8_BIT_RANGE_MIN, LG_8_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

    newEffect_.Type = LGTYPE_DAMPER;
    newEffect_.Duration = LGDURATION_INFINITE;
    newEffect_.p.condition[0].SaturationNeg = LGMAGNITUDE_MAX;
    newEffect_.p.condition[0].SaturationPos = LGMAGNITUDE_MAX;
    newEffect_.p.condition[0].CoefficientNeg = coefficient_;
    newEffect_.p.condition[0].CoefficientPos = coefficient_;
    newEffect_.p.condition[1] = newEffect_.p.condition[0];

    PlayEffect(LG_FORCE_DAMPER, &newEffect_);
}

LG_VOID ForceManager::StopDamperForce()
{
	if(m_force[LG_FORCE_DAMPER].IsPlaying())
	{
		m_force[LG_FORCE_DAMPER].Stop();
	}
}

LG_VOID ForceManager::PlaySideCollisionForce(const LG_INT strengthPercentage)
{
    if (strengthPercentage < LG_MIN_PERCENTAGE || strengthPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlaySideCollisionForce strengthPercentage is out of bounds (%d)\n", strengthPercentage);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlaySideCollisionForce but wheel handle is NULL\n");
        return;
    }

    LG_INT magnitude_ = Utils::FromPercentage(strengthPercentage, LG_MIN_PERCENTAGE, LG_MAX_PERCENTAGE, LG_8_BIT_RANGE_MIN, LG_8_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

	newEffect_.StartDelay = LG_COLLISION_START_DELAY;
    newEffect_.Type = LGTYPE_CONSTANT;
    newEffect_.Duration = LG_COLLISION_EFFECT_DURATION;
    newEffect_.p.constant.Magnitude = magnitude_;
    newEffect_.p.constant.Direction = 90;

    PlayEffect(LG_FORCE_SIDE_COLLISION, &newEffect_);	//, true);
}

LG_VOID ForceManager::PlayFrontalCollisionForce(const LG_INT strengthPercentage)
{
    if (strengthPercentage < 0 || strengthPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlayFrontalCollisionForce strengthPercentage is out of bounds (%d)\n", strengthPercentage);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlayFrontalCollisionForce but wheel handle is NULL\n");
        return;
    }

    LG_INT magnitude_ = Utils::FromPercentage(strengthPercentage, 0, LG_MAX_PERCENTAGE, 0, LG_8_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

	newEffect_.StartDelay = LG_COLLISION_START_DELAY;
    newEffect_.Type = LGTYPE_SQUARE;
    newEffect_.Duration = LG_COLLISION_EFFECT_DURATION;
    newEffect_.p.periodic.Magnitude = magnitude_;
    newEffect_.p.periodic.Period = LG_FRONTAL_COLLISION_PERIOD;
    newEffect_.p.periodic.Direction = 90;
    newEffect_.p.periodic.envelope.FadeTime = LG_FRONTAL_COLLISION_FADE_TIME;

    PlayEffect(LG_FORCE_FRONTAL_COLLISION, &newEffect_, true);
}

LG_VOID ForceManager::PlayDirtRoadEffect(const LG_INT strengthPercentage)
{
    if (strengthPercentage < 0 || strengthPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlayDirtRoadEffect strengthPercentage is out of bounds (%d)\n", strengthPercentage);

    LOGIASSERT(strengthPercentage >= 0 && strengthPercentage <= LG_MAX_PERCENTAGE);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlayDirtRoadEffect but wheel handle is NULL\n");
        return;
    }

    if (m_isAirborne)
        return;

    LG_INT magnitude_ = Utils::FromPercentage(strengthPercentage, 0, LG_MAX_PERCENTAGE, 0, LG_8_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

    newEffect_.Type = LGTYPE_SINE;
    newEffect_.Duration = LGDURATION_INFINITE;
    newEffect_.p.periodic.Magnitude = magnitude_;
    newEffect_.p.periodic.Period = LG_DIRT_ROAD_PERIOD;
    newEffect_.p.periodic.Direction = 90;

    PlayEffect(LG_FORCE_DIRT_ROAD, &newEffect_);
}

LG_VOID ForceManager::StopDirtRoadEffect()
{
	if(m_force[LG_FORCE_DIRT_ROAD].IsPlaying())
	{
		m_force[LG_FORCE_DIRT_ROAD].Stop();
	}
}

LG_VOID ForceManager::PlayBumpyRoadEffect(const LG_INT strengthPercentage)
{
    if (strengthPercentage < 0 || strengthPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlayBumpyRoadEffect strengthPercentage is out of bounds (%d)\n", strengthPercentage);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlayBumpyRoadEffect but wheel handle is NULL\n");
        return;
    }

    if (m_isAirborne)
        return;

    LG_INT magnitude_ = Utils::FromPercentage(strengthPercentage, 0, LG_MAX_PERCENTAGE, 0, LG_8_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

    newEffect_.Type = LGTYPE_SQUARE;
    newEffect_.Duration = LGDURATION_INFINITE;
    newEffect_.p.periodic.Magnitude = magnitude_;
    newEffect_.p.periodic.Period = LG_BUMPY_ROAD_PERIOD;
    newEffect_.p.periodic.Direction = 90;

    PlayEffect(LG_FORCE_BUMPY_ROAD, &newEffect_);
}

LG_VOID ForceManager::StopBumpyRoadEffect()
{
	if(m_force[LG_FORCE_BUMPY_ROAD].IsPlaying())
	{
		m_force[LG_FORCE_BUMPY_ROAD].Stop();
	}
}

LG_VOID ForceManager::PlaySlipperyRoadEffect(const LG_INT strengthPercentage)
{
    if (strengthPercentage < 0 || strengthPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlaySlipperyRoadEffect strengthPercentage is out of bounds (%d)\n", strengthPercentage);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlaySlipperyRoadEffect but wheel handle is NULL\n");
        return;
    }

    // check if there was a damper playing when making the current call. If there was, stop the damper and remember
    // to restart it after slippery road effect is stopped
    if (m_force[LG_FORCE_DAMPER].IsPlaying())
    {
        m_force[LG_FORCE_DAMPER].Stop();
        m_force[LG_FORCE_DAMPER].SetWasPlayingFlag(true);
    }

    // check if there was a spring playing when making the current call. If there was, stop the spring and remember
    // to restart it after slippery road effect is stopped
    if (m_force[LG_FORCE_SPRING].IsPlaying())
    {
        m_force[LG_FORCE_SPRING].Stop();
        m_force[LG_FORCE_SPRING].SetWasPlayingFlag(true);
    }

    if (m_isAirborne)
        return;

    LG_INT coefficient_ = Utils::FromPercentage(strengthPercentage, 0, LG_MAX_PERCENTAGE, 0, LG_8_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

    newEffect_.Type = LGTYPE_DAMPER;
    newEffect_.Duration = LGDURATION_INFINITE;
    newEffect_.p.condition[0].SaturationNeg = LGMAGNITUDE_MAX;
    newEffect_.p.condition[0].SaturationPos = LGMAGNITUDE_MAX;
    newEffect_.p.condition[0].CoefficientNeg = -coefficient_;
    newEffect_.p.condition[0].CoefficientPos = -coefficient_;
    newEffect_.p.condition[1] = newEffect_.p.condition[0];

    PlayEffect(LG_FORCE_SLIPPERY_ROAD, &newEffect_);
}

LG_VOID ForceManager::StopSlipperyRoadEffect()
{
    m_force[LG_FORCE_SLIPPERY_ROAD].Stop();

    // check if damper had been playing before. If such is the case, start it again. This would happen in case
    // the damper effect was stopped by the slippery surface effect. Indeed we only want one effect of type damper
    // to be played at any time.
    if (m_force[LG_FORCE_DAMPER].WasPlaying())
    {
        m_force[LG_FORCE_DAMPER].Start();
        m_force[LG_FORCE_DAMPER].SetWasPlayingFlag(false);
    }

    // check if spring had been playing before. If such is the case, start it again. This would happen in case
    // the spring effect was stopped by the slippery surface effect.
    // to be played at any time.
    if (m_force[LG_FORCE_SPRING].WasPlaying())
    {
        m_force[LG_FORCE_SPRING].Start();
        m_force[LG_FORCE_SPRING].SetWasPlayingFlag(false);
    }
}

LG_VOID ForceManager::PlaySurfaceEffect(const LG_INT type, const LG_INT strengthPercentage, const LG_UINT period)
{
    if (strengthPercentage < 0 || strengthPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlaySurfaceEffect strengthPercentage is out of bounds (%d)\n", strengthPercentage);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlaySurfaceEffect but wheel handle is NULL\n");
        return;
    }

    if (m_isAirborne)
        return;

    LG_INT magnitude_ = Utils::FromPercentage(strengthPercentage, 0, LG_MAX_PERCENTAGE, 0, LG_8_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

	newEffect_.Type = (rage::u8)type;
    newEffect_.Duration = LGDURATION_INFINITE;
    newEffect_.p.periodic.Magnitude = magnitude_;
    newEffect_.p.periodic.Period = period;
    newEffect_.p.periodic.Direction = 90;

    PlayEffect(LG_FORCE_SURFACE_EFFECT, &newEffect_);
}

LG_VOID ForceManager::StopSurfaceEffect()
{
	if(m_force[LG_FORCE_SURFACE_EFFECT].IsPlaying())
	{
		m_force[LG_FORCE_SURFACE_EFFECT].Stop();
	}
}

LG_VOID ForceManager::PlayCarAirborne()
{
    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlayCarAirborne but wheel handle is NULL\n");
        return;
    }

    // only process in case car isn't in the air already
    if (true == m_isAirborne)
        return;

    for (LG_INT ii = 0; ii < LG_NUMBER_FORCE_EFFECTS; ii++)
    {
        if (m_force[ii].IsPlaying())
        {
            m_force[ii].Stop();
            m_force[ii].SetWasPlayingFlag(true);
        }
    }

    m_isAirborne = true;
}

LG_VOID ForceManager::StopCarAirborne()
{
    if (false == m_isAirborne)
        return;

    for (LG_INT ii = 0; ii < LG_NUMBER_FORCE_EFFECTS; ii++)
    {
        if (m_force[ii].WasPlaying())
        {
            m_force[ii].Start();
            m_force[ii].SetWasPlayingFlag(false);
        }
    }

    m_isAirborne = false;
}

LG_VOID ForceManager::PlaySoftstopForce(const LG_INT deadbandPercentage)
{
    if (deadbandPercentage < 0 || deadbandPercentage > LG_MAX_PERCENTAGE)
        LOGITRACE("Warning: PlaySoftstopForce deadbandPercentage is out of bounds (%d)\n", deadbandPercentage);

    if (LG_INVALID_DEVICE == m_wheelHandle)
    {
        LOGITRACE("ERROR: trying to do PlaySoftstopForce but wheel handle is NULL\n");
        return;
    }

    LG_INT deadband_ = Utils::FromPercentage(deadbandPercentage, 0, LG_MAX_PERCENTAGE, 0, LG_UNSIGNED_16_BIT_RANGE_MAX);

    lgDevForceEffect newEffect_;
    memset(&newEffect_, 0, sizeof(newEffect_));

    newEffect_.Type = LGTYPE_SPRING;
    newEffect_.Duration = LGDURATION_INFINITE;
    newEffect_.p.condition[0].SaturationNeg = LGMAGNITUDE_MAX;
    newEffect_.p.condition[0].SaturationPos = LGMAGNITUDE_MAX;
    newEffect_.p.condition[0].CoefficientNeg = LGMAGNITUDE_MAX;
    newEffect_.p.condition[0].CoefficientPos = LGMAGNITUDE_MAX;
    newEffect_.p.condition[0].Deadband = deadband_;
    newEffect_.p.condition[1] = newEffect_.p.condition[0];

    PlayEffect(LG_FORCE_SOFTSTOP, &newEffect_);
}

LG_VOID ForceManager::StopSoftstopForce()
{
	if(m_force[LG_FORCE_SOFTSTOP].IsPlaying())
	{
	   m_force[LG_FORCE_SOFTSTOP].Stop();
	}
}

LG_BOOL ForceManager::ForceHasChanged(const ForceType forceType, const lgDevForceEffect* newForceEffect)
{
    if (!newForceEffect)
    {
        LOGITRACE("ERROR: new force effect is NULL\n");
        return true;
    }

    // Get current force effect
    lgDevForceEffect* forceEffect_ = m_force[forceType].GetForceEffect();
    if (!forceEffect_)
    {
        LOGITRACE("ERROR: GetForceEffect returned NULL\n");
        return true;
    }

    switch (forceType)
    {
    case LG_FORCE_SPRING:
    case LG_FORCE_DAMPER:
    case LG_FORCE_SLIPPERY_ROAD:
        if (newForceEffect->p.condition[0].Offset == forceEffect_->p.condition[0].Offset
            && newForceEffect->p.condition[0].SaturationNeg == forceEffect_->p.condition[0].SaturationNeg
            && newForceEffect->p.condition[0].SaturationPos == forceEffect_->p.condition[0].SaturationPos
            && newForceEffect->p.condition[0].CoefficientNeg == forceEffect_->p.condition[0].CoefficientNeg
            && newForceEffect->p.condition[0].CoefficientPos == forceEffect_->p.condition[0].CoefficientPos)
            return false;
        break;
    case LG_FORCE_CONSTANT:
        if (newForceEffect->p.constant.Magnitude == forceEffect_->p.constant.Magnitude
            && newForceEffect->p.constant.Direction == forceEffect_->p.constant.Direction)
            return false;
        break;
    case LG_FORCE_SIDE_COLLISION:
    case LG_FORCE_FRONTAL_COLLISION:
        return true; // always return true for collision effects
    case LG_FORCE_DIRT_ROAD:
    case LG_FORCE_BUMPY_ROAD:
    case LG_FORCE_SURFACE_EFFECT:
        if (newForceEffect->Type == forceEffect_->Type
            && newForceEffect->p.periodic.Magnitude == forceEffect_->p.periodic.Magnitude
            && newForceEffect->p.periodic.Period == forceEffect_->p.periodic.Period)
            return false;
        break;
    case LG_FORCE_SOFTSTOP:
        if (newForceEffect->p.condition[0].Deadband == forceEffect_->p.condition[0].Deadband)
            return false;
        break;
    case LG_FORCE_CAR_AIRBORNE:
        return false;
    default:
        LOGIASSERT(0);
    }

    return true;
}

LG_VOID ForceManager::PlayEffect(const ForceType forceType, lgDevForceEffect* newForceEffect, const LG_BOOL isCollisionForce)
{
    // if lgDevForceEffect type differs
    //   if downloaded before
    //     destroy
    //   download, start
    // else if lgDevForceEffect type does not differ
    //   if playing
    //     if parameters differ
    //       update
    //     if collision force
    //       start
    //   else if not playing
    //     if not downloaded before
    //       download
    //     else if parameters differ
    //       update
    //     start


    if (!newForceEffect)
    {
        LOGITRACE("Trying to play effect but newForceEffect is NULL\n");
        return;
    }

	LG_INT ret_ = LG_SUCCESS;
    if (m_force[forceType].GetPreviousEffectType() != newForceEffect->Type)
    {
        if (LG_INVALID_EFFECTID != m_force[forceType].GetEffectID())
        {
            m_force[forceType].Destroy();
        }

        ret_ = m_force[forceType].Download(m_wheelHandle, *newForceEffect);
		if (ret_ == LG_SUCCESS)
		{
			m_force[forceType].Start();
			m_force[forceType].SetPreviousEffectType(newForceEffect->Type);
		}
    }
    else
    {
        if (IsPlaying(forceType))
        {
            if (ForceHasChanged(forceType, newForceEffect))
            {
                ret_ = m_force[forceType].Update(*newForceEffect);
            }

            if ((ret_ == LG_SUCCESS) && isCollisionForce)
            {
                m_force[forceType].Start();
            }
        }
        else
        {
            if (LG_INVALID_EFFECTID == m_force[forceType].GetEffectID())
            {
                ret_ = m_force[forceType].Download(m_wheelHandle, *newForceEffect);
            }
            else if (ForceHasChanged(forceType, newForceEffect))
            {
               ret_ =  m_force[forceType].Update(*newForceEffect);
            }

			if (ret_ == LG_SUCCESS)
			{
				m_force[forceType].Start();
				m_force[forceType].SetPreviousEffectType(newForceEffect->Type);
			}
        }
    }
}

#endif
