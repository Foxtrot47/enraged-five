/*
The Logitech EZ Wheel Wrapper, including all accompanying documentation,
is protected by intellectual property laws. All rights
not expressly granted by Logitech are reserved.
*/

#ifndef LOGI_FORCE_H_INCLUDED_
#define LOGI_FORCE_H_INCLUDED_

#include "input/input.h"

#if HAVE_FFWHEEL

#include "LogiGlobals.h"
#include "liblgFF.h"

namespace LogitechEZWheelWrapper
{
    typedef enum
    {
        LG_FORCE_NONE = -1,
        LG_FORCE_SPRING,
        LG_FORCE_CONSTANT,
        LG_FORCE_DAMPER,
        LG_FORCE_SIDE_COLLISION,
        LG_FORCE_FRONTAL_COLLISION,
        LG_FORCE_DIRT_ROAD,
        LG_FORCE_BUMPY_ROAD,
        LG_FORCE_SLIPPERY_ROAD,
        LG_FORCE_SURFACE_EFFECT,
        LG_NUMBER_FORCE_EFFECTS, LG_FORCE_SOFTSTOP, LG_FORCE_CAR_AIRBORNE
    } ForceType;

    class Force
    {
    public:
        Force();
        ~Force();

        LG_INT Download(const LG_INT device, lgDevForceEffect& forceEffect);
        LG_INT Update(lgDevForceEffect& forceEffect);
        LG_INT Start();
        LG_INT Stop();
        LG_INT Destroy();

        LG_BOOL IsPlaying() { return m_isPlaying; }
        LG_VOID SetPlaying(const LG_BOOL isPlaying) { m_isPlaying = isPlaying; }

        LG_VOID SetForceType(const ForceType forceType) { m_forceType = forceType; }

        lgDevForceEffect* GetForceEffect() { return &m_forceEffect; }

        LG_INT GetEffectID() { return m_effectID; }

        LG_BOOL WasPlaying() { return m_wasPlaying; }
        LG_VOID SetWasPlayingFlag(const LG_BOOL wasPlaying) { m_wasPlaying = wasPlaying; }

        LG_UCHAR GetPreviousEffectType() { return m_previousEffectType; }
        LG_VOID SetPreviousEffectType(LG_UCHAR previousEffectType) { m_previousEffectType = previousEffectType; }

    private:
        LG_INT m_effectID;
        lgDevForceEffect m_forceEffect;
        LG_BOOL m_isPlaying;
        ForceType m_forceType;
        LG_BOOL m_wasPlaying;
        LG_UCHAR m_previousEffectType;

        LG_VOID Init();
    };
}

#endif

#endif // LOGI_FORCE_H_INCLUDED_
