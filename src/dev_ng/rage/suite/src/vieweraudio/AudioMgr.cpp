// 
// vieweraudio/AudioMgr.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "audiomgr.h"

#include "audioengine/controller.h"
#include "audioengine/categorymanager.h"
#include "audioengine/engine.h"
#include "audioengine/environment.h"
#include "audioengine/engineutil.h"

#include "system/param.h"
#include "system/timemgr.h"
#include "system/simpleallocator.h"


#if __WIN32PC
#define DEFAULT_AUDIO_HEAP_SIZE (256 * 1024)
#else
#define DEFAULT_AUDIO_HEAP_SIZE (32 * 1024)
#endif

using namespace rage;
PARAM(latestaudio, "Point audio path to asset staging area.");
PARAM(audiomemory, "Size of the heap memory for the audio system in MB");

namespace rage
{
	XPARAM(noaudio);
	XPARAM(noaudiothread);
	XPARAM(rave);
};


audMetadataManager		vwrAudioManager::m_GameObjectMetadata;

audController*			vwrAudioManager::sm_pAudController = NULL;
audCategoryController*	vwrAudioManager::m_pBaseController;
sysMemAllocator*	vwrAudioManager::sm_AudioAllocator = NULL;
const char*				vwrAudioManager::sm_BankPath = NULL;


Matrix34				vwrAudioManager::sm_ActiveMatrix = M34_IDENTITY;
f32						vwrAudioManager::sm_fCurTimeActual = 0.0f;
u32						vwrAudioManager::sm_uiCurTimeInMS = 0;

bool					vwrAudioManager::sm_bIsAudioPaused = false;

void vwrAudioManager::StartUserUpdateFunction()
{
	//	Set the game specific audio thread update
	g_AudioEngine.SetUserGameUpdateFunction(&vwrAudioManager::UpdateAudioThread);
}

void vwrAudioManager::StopUserUpdateFunction()
{
	//	Kill the game update function
	g_AudioEngine.SetUserGameUpdateFunction(NULL);
}

bool vwrAudioManager::InitClass()
{
	if (PARAM_noaudio.Get())
		PARAM_noaudiothread.Set("");

	char szPathBuffer[256] = {0};
	const char* audioPath = NULL;
	const char* latestAudioParam = NULL;
	if (!PARAM_latestaudio.Get(latestAudioParam))
	{
#if __XENON
		audioPath = "$/xbox360/audio/Config/";
		sm_BankPath = "$/xbox360/audio/sfx/";
#elif __WIN32PC
		audioPath = "$/pc/audio/Config/";
		sm_BankPath = "$/pc/audio/sfx/";
#elif __PS3
		audioPath = "$/ps3/audio/Config/";
		sm_BankPath = "$/ps3/audio/sfx/";
#endif
	}
	else
	{
		if (latestAudioParam[0] == '\0')
			latestAudioParam = "audio";

		audioPath   = szPathBuffer;
#if __XENON
		formatf(szPathBuffer, sizeof(szPathBuffer), "$/../../audio/dev/runtime/xbox360/config/", latestAudioParam);
		sm_BankPath = "$/../../audio/dev/runtime/xbox360/sfx/";
#elif __WIN32PC
		formatf(szPathBuffer, sizeof(szPathBuffer), "$/../../audio/dev/runtime/pc/config/", latestAudioParam);
		sm_BankPath = "$/../../audio/dev/runtime/pc/sfx/";
#elif __PS3
		formatf(szPathBuffer, sizeof(szPathBuffer), "$/../../audio/dev/runtime/ps3/config/", latestAudioParam);
		sm_BankPath = "$/../../audio/dev/runtime/ps3/sfx/";
#endif
	}

	int audioHeapSize = DEFAULT_AUDIO_HEAP_SIZE;
	PARAM_audiomemory.Get(audioHeapSize);
	audioHeapSize *= 1024;	//	Convert to kilobytes

	//	Use only one kilobyte if noaudiothread is on
	if (PARAM_noaudiothread.Get())
		audioHeapSize = 1024;

	//#if __BANK
	//	sm_TotalAudioMemory = audioHeapSize;
	//	CreateBank();
	//#endif


	sm_AudioAllocator = rage_new sysMemSimpleAllocator(audioHeapSize, MEMTYPE_COUNT+1);

	sm_pAudController = rage_new audController;

	Displayf("Initializing audio engine using assets in \"%s\"", audioPath);
	ASSET.PushFolder(audioPath);
	if (!InitializeAudio(*sm_pAudController, sm_AudioAllocator, sysMemAllocator::GetCurrent().GetAllocator(0), audioPath))
	{
		return false;
	}
	ASSET.PopFolder();

	//	We define our own game object metadata now
	InitGameMetadata(audioPath);


	//sm_pDefaultReverbParams = (const sdAudio::ReverbParameters*)GetGameObject("REVERB_DEFAULT");


	// reserve a bucket for streaming sounds
	//audWaveSlotManager::SetBucketIdForStreamingSounds(audSound::GetStaticPool().GetNextReservedBucketId());

	//	Initialize the category manager
	//audCategoryControllerManager::Instantiate();

	//	Instantiate the waveslot managers

	//	Initialize the dynamic mixer
	//	mcDynamicMixer::Instantiate();

	g_AudioEngine.GetEnvironment().InitReverbCurves();

	//Assign occlusion mgr ptr to the controller
	/*sm_pOcclusionMgr = rage_new rdrOcclusionMgr;
	sm_pOcclusionMgr->Init();
	sm_pAudController->SetOcclusionGroupManager(sm_pOcclusionMgr);

	sm_FlashEntity = rage_new rdrFlashAudEntity;
	sm_FlashEntity->Init();
	sm_AmbienceAudioEntity = rage_new rdrAmbienceAudioEntity;
	AMBIENCEAUDIO->Init();
	sm_PedAudioManager = rage_new rdrPedAudioManager;
	sm_PedAudioManager->Init();
	sm_ConversationAudioEntity = rage_new rdrConversationAudioEntity;
	CONVERSATIONAUDIO->Init();
	sm_WaterAudioEnitity = rage_new rdrWaterAudioEntity;
	WATERAUDIO->Init();
	sm_pCutsceneAudioEntity = rage_new mcCutsceneAudEntity;
	sm_pCutsceneAudioEntity->Init();
	sm_pAudioEnvironment = rage_new rdrAudioEnvironment;
	sm_pAudioEnvironment->Init();
	g_SpeechManager.Init();
	audSpeechAudioEntity::InitClass();*/


	//AUDIOENGINE.GetEnvironment()->SetDirectionalMicParameters(0.707f, -0.701f, -1.5f);

	// Set flash hook
	/*swfFILE::PlaySound = rdrAudioManager::PlayFlashSound;

	sm_pActiveReverbParams = (const sdAudio::ReverbParameters*)GetGameObject("REVERB_DEFAULT");
	Assert(sm_pActiveReverbParams);

	sm_pPriorityParams = (const sdAudio::PriorityParameters*)GetGameObject("GLOBAL_PRIORITIES");
	Assert(sm_pPriorityParams);*/

	//	Begin updating effects
//	g_AudioEngine.GetEffectManager().StartUpdatingEffects();

	//	Call this after we start updating the effects!
//	InitRouteEffects();

	g_AudioEngine.SetVolumeListenerMatrix(sm_ActiveMatrix, 0, 1.0f);
	g_AudioEngine.SetPanningListenerMatrix(sm_ActiveMatrix, 0, 1.0f);

	//	Make sure we kill the rest of of our 4 listeners we don't care about
	for (u32 i = 1; i < g_maxListeners; ++i)
	{
		g_AudioEngine.SetVolumeListenerMatrix(sm_ActiveMatrix, i, 0.0f);
		g_AudioEngine.SetPanningListenerMatrix(sm_ActiveMatrix, i, 0.0f);
	};

	//rage::audSpeechSound::InitClass();

	sm_ActiveMatrix.Identity();

	//#if __BANK
	//	AddWidgets();
	//#endif

	//	MC4 has this in the game's main update loop, and I can't remember why, so we may need to move this
	StartUserUpdateFunction();

	//m_pBaseController = AUDCATEGORYCONTROLLERMANAGER.CreateController("BASE");

	return true;
}

void vwrAudioManager::InitGameMetadata(const char* UNUSED_PARAM(configPath))
{
	/*char metadataFileName[256];
	formatf(metadataFileName, sizeof(metadataFileName), "%sgame.dat", configPath);
	AssertVerify(m_GameObjectMetadata.Init("GameObjects", metadataFileName, true, GAMEOBJECTS_SCHEMA_VERSION));*/
}

void vwrAudioManager::ShutdownClass()
{
	StopUserUpdateFunction();

	//	Destroy the dynamic mixer
	//	mcDynamicMixer::Destroy();

	//	Destroy the category manager
	//audCategoryControllerManager::Destroy();

	g_AudioEngine.Shutdown();

	if (sm_pAudController)
	{
		delete sm_pAudController;
		sm_pAudController = NULL;
	}

	if (sm_pAudController)
	{
		delete sm_AudioAllocator;
		sm_AudioAllocator = NULL;
	}
}

void vwrAudioManager::Reset()
{

}

void vwrAudioManager::Update(const Matrix34& listenerMtx, bool bPaused)
{
	if (PARAM_noaudio.Get())
		return;
//
//#if __BANK
//	{
//		float fGameRate = 1.0f / s_TimerGameThread.GetTime();
//		s_TimerGameThread.Reset();
//		Approach(s_fAvgGameRate, fGameRate, Max(1.0f, fabs(fGameRate-s_fAvgGameRate)), TIME.GetUnwarpedRealtimeSeconds());
//	}
//	s_TimerUpdate.Reset();
//#endif

	sm_ActiveMatrix = listenerMtx;

	if (bPaused)
	{
		g_AudioEngine.GetSoundManager().Pause(0);
		//DYNAMICMIXER.TriggerState(sdAudio::DM_PAUSE);
	}
	else
	{
		g_AudioEngine.GetSoundManager().UnPause(0);
		//DYNAMICMIXER.DeTriggerState(sdAudio::DM_PAUSE);
	}

	//	Update camera position
	g_AudioEngine.SetVolumeListenerMatrix(sm_ActiveMatrix);
	g_AudioEngine.SetPanningListenerMatrix(sm_ActiveMatrix);

	//UpdateListenerEnvironment();

	/*BANK_ONLY( if (sm_bUpdateNearbyProps) )
		UpdateNearbyProps();*/

	//	We need the actual time independent of frame rate, but warped for the most accurate Doppler effect during "Zone"
	sm_fCurTimeActual += TIME.GetWarpedRealtimeSeconds();
	sm_uiCurTimeInMS = u32(sm_fCurTimeActual * 1000.0f);

	//	Update the controllers
	sm_pAudController->PreUpdate(g_AudioEngine.GetTimeInMilliseconds());
	sm_pAudController->Update(g_AudioEngine.GetTimeInMilliseconds());

	g_AudioEngine.CommitGameSettings(sm_uiCurTimeInMS);

#if __BANK
	//	If Rave is running, we'll need to catch our bus effects updates once per frame
	/*if (PARAM_rave.Get())
		UpdateRouteEffects();*/
#endif

//#if __BANK
//	{
//		float fTimeInMs = s_TimerUpdate.GetMsTime();
//		Approach(s_fAvgUpdateTimeMs, fTimeInMs, Max(1.0f, fabs(fTimeInMs-s_fAvgUpdateTimeMs)), TIME.GetUnwarpedRealtimeSeconds());
//	}
//	sm_AudioMemoryUsed	= sm_AudioAllocator->GetMemoryUsed(0);
//	sm_AudioMemoryFree	= sm_AudioAllocator->GetMemoryAvailable();
//#if __XENON
//	if (audDriverXenon::GetPhysicalVoiceAllocator())
//	{
//		sm_stVoiceAllocUsed	= audDriverXenon::GetPhysicalVoiceAllocator()->GetMemoryUsed(0);
//		sm_stVoiceAllocFree	= audDriverXenon::GetPhysicalVoiceAllocator()->GetMemoryAvailable();
//		sm_stVoiceAllocTotal = sm_stVoiceAllocUsed + sm_stVoiceAllocFree;
//	}
//#endif	//	__XENON
//#endif	//	__BANK
}

void vwrAudioManager::UpdateAudioThread()
{
//#if __BANK
//	{
//		float fAudioRate = 1.0f / s_TimerAudioThread.GetTime();
//		s_TimerAudioThread.Reset();
//		Approach(s_fAvgAudioRate, fAudioRate, Max(1.0f, fabs(fAudioRate-s_fAvgAudioRate)), TIME.GetUnwarpedRealtimeSeconds());
//	}
//#endif

	//AUDCATEGORYCONTROLLERMANAGER.Update();
	g_AudioEngine.GetCategoryManager().CommitCategorySettings();
	//sm_pOcclusionMgr->FullyUpdateOcclusionGroups(AUDIOENGINE.GetTimeInMilliseconds());
}