// 
// vieweraudio/AudioMgr.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef VIEWERAUDIO_AUDIOMGR_H
#define VIEWERAUDIO_AUDIOMGR_H

#include "audioengine\metadatamanager.h"

namespace rage
{
	class Matrix34;
	class audController;
	class sysMemAllocator;
	class audSound;
	class audCategoryController;
	class audMetadataManager;
}

using namespace rage;

class vwrAudioManager
{
public:
	static void StartUserUpdateFunction();
	static void StopUserUpdateFunction();

	static bool InitClass();
	static void InitGameMetadata(const char* configPath);
	static void ShutdownClass();
	static void Reset();

	static void Update(const Matrix34& listenerMtx, bool bPaused);
	static void UpdateAudioThread();

	//static void InitRouteEffects();
	//static void UpdateRouteEffects();

	//static audMetadataManager&	GetGameMetadataManager()	{ return m_GameObjectMetadata; }
	static __forceinline void* GetGameObject(const unsigned int hashValue)						{ return m_GameObjectMetadata.GetObjectMetadataPtr(hashValue); }
	static __forceinline void* GetGameObject(const char* objectName)						{ return m_GameObjectMetadata.GetObjectMetadataPtr(objectName); }

	static bool IsAudioPaused() { return sm_bIsAudioPaused; }
	static void Pause();
	static void UnPause();
	static void MuteAll();
	static void UnMuteAll();

private:
	static audMetadataManager			m_GameObjectMetadata;
	static audCategoryController*		m_pBaseController;

	static const char*					sm_BankPath;

	static rage::audController*			sm_pAudController;
	static rage::sysMemAllocator*	sm_AudioAllocator;

	static Matrix34						sm_ActiveMatrix;
	static rage::f32					sm_fCurTimeActual;
	static rage::u32					sm_uiCurTimeInMS;

	static bool							sm_bIsAudioPaused;
};

#endif //VIEWERAUDIO_AUDIOMGR_H
