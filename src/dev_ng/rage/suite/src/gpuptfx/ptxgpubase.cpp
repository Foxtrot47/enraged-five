// 
// rmptfx/ptxgpuBase.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 


#if !__RESOURCECOMPILER

// includes
#include "ptxgpubase.h"
#include "grcore/image.h"
#include "grcore/quads.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/shader.h"
#include "system/task.h"
#include "rmptfx/ptxeffectinst.h"
#include "pheffects/wind.h"
#include "profile/timebars.h"

#if __D3D11
#include "grcore/d3dwrapper.h"
#endif //__D3D11

// optimisations
//RMPTFX_OPTIMISATIONS()
//OPTIMISATIONS_OFF()
#if RSG_PC
NOSTRIP_PARAM(nogpuparticles,"Disable gpu particle effect update/render");
#endif

// namespace
namespace rage {
extern bool NIY;

// externally declared variables
extern u32 g_AllowVertexBufferVramLocks;

#if RAIN_UPDATE_CPU
	bool ptxgpuBase::sm_bRainUpdateOnCpu = false;
	#if __BANK
	bool ptxgpuBase::sm_bBankRainUpdateOnCpu = true;
	#endif
#endif


// global functions
#if (__D3D11 || RSG_ORBIS)

grcVertexBuffer* CreateInstancedVertexBuffer(int channel)
{
	// These represent a mini strip making a quad.
	ALIGNAS(16) Vector2 QuadVerts[4] = 
	{
		Vector2(1.0f, 0.0f), 
		Vector2(0.0f, 0.0f), 
		Vector2(1.0f, 1.0f), 
		Vector2(0.0f, 1.0f)
	};

	grcFvf fvf0;
	fvf0.SetTextureChannel(channel, true, grcFvf::grcdsFloat2);
	fvf0.SetPosChannel(false);

	// Create the vertex buffer.
#if RSG_PC
	grcVertexBuffer* pVB = grcVertexBuffer::CreateWithData(4, fvf0, grcsBufferCreate_NeitherReadNorWrite, (const void *)&QuadVerts[0]);
#else // RSG_PC
	grcVertexBuffer* pVB = grcVertexBuffer::CreateWithData(4, fvf0, (const void *)&QuadVerts[0]);
#endif // RSG_PC

	return pVB;
}

#else //__D3D11 || RSG_ORBIS

grcVertexBuffer* CreateInstancedVertexBuffer(int repeatAmount, int channel, int maxAmount)
{
	Vector2 quadVerts[4] = 
	{
		Vector2(1.0f, 0.0f), 
		Vector2(0.0f, 0.0f), 
		Vector2(0.0f, 1.0f),
		Vector2(1.0f, 1.0f) 
	};

	grcFvf fvf0;
	fvf0.SetTextureChannel(channel, true, grcFvf::grcdsFloat2);
	fvf0.SetPosChannel(false);

	const bool bReadWrite = true;
	const bool bDynamic = false;
	grcVertexBuffer* pVB = grcVertexBuffer::Create(__WIN32PC ? maxAmount*repeatAmount : repeatAmount, fvf0, bReadWrite, bDynamic, NULL);
	{
		grcVertexBufferEditor editor(pVB);

#if __WIN32PC
		for (int i=0; i<maxAmount; ++i)
#endif
		{
			for (int j=0; j<repeatAmount; j++)
			{
				int index = WIN32PC_ONLY(i*repeatAmount +) j;
				editor.SetUV(index, channel, quadVerts[j], false);
			}
		}
	}
	pVB->MakeReadOnly();

	return pVB;
}

#endif //__D3D11 || RSG_OBRIS

/********************************************************************************************************************************************/
/* Class ptxgpuShader																														*/
/********************************************************************************************************************************************/

ptxgpuShader::~ptxgpuShader()
{
	ClearTextureReferences();
	delete m_pShader;
}

bool ptxgpuShader::Init(const char* name)
{
	m_pShader = grmShaderFactory::GetInstance().Create();
	AssertReturn(m_pShader);

	if (!m_pShader->Load(name))
	{
		AssertReturn(0);
	}

	m_isActive = false;

#if __D3D11 || RSG_ORBIS
	m_positionDestSurfacePosID = m_pShader->LookupVar("ParticlePos"); 
	m_positionDestSurfaceVelID = m_pShader->LookupVar("ParticleVel"); 
#if !__FINAL
	if((m_positionDestSurfacePosID == grcevNONE) || (m_positionDestSurfaceVelID == grcevNONE))
	{
		char ErrorMsg[1024];
		sprintf(ErrorMsg, "Particle shader %s is missing position textures!", m_pShader->GetName());
		grcErrorf("%s",ErrorMsg);
	}
#endif //!__FINAL
#endif //__D3D11

	return true;
}

void ptxgpuShader::FullScreenBlit() 
{
	Color32 white(1.0f,1.0f,1.0f,1.0f);

	if (BeginDraw()) 
	{
		grcDrawSingleQuadf(-1.0f, 1.0f, 1.0f, -1.0f, 0.1f, 0.0f, 0.0f, 1.0f, 1.0f, white);
	}
	EndDraw();
}

bool ptxgpuShader::BeginDraw()
{
	ASSERT_ONLY(m_prevFunc = ATSTRINGHASH("BeginDraw", 0x2F1DF8BE));

	AssertReturn(m_pShader);
	if (m_pShader->BeginDraw(grmShader::RMC_DRAW, true, m_techniqueId))
	{
		m_pShader->Bind(m_pass);
		m_isActive = true;

		return true;
	}

	return false;
}

void ptxgpuShader::EndDraw()
{ 
	Assert(m_prevFunc == ATSTRINGHASH("BeginDraw", 0x2F1DF8BE));
	ASSERT_ONLY(m_prevFunc = ATSTRINGHASH("EndDraw", 0x47C80D2A));

	if (m_isActive)
	{
		m_pShader->UnBind();
	}

	m_pShader->EndDraw(); 
	ClearTextureReferences();
	m_isActive = false;
} 

void ptxgpuShader::SetTechnique(const char* name)
{
	m_techniqueId = m_pShader->LookupTechnique(name);
}

u32 ptxgpuShader::GetHashName() const
{	
	return atStringHash(m_pShader->GetName()); 
}

#if __BANK
void ptxgpuShader::SetTechniqueFromList()
{
	m_techniqueId = m_pShader->GetTechniqueByIndex(m_techIdx);

}
#endif

#if __BANK
# if EFFECT_PRESERVE_STRINGS
void ptxgpuShader::AddWidgets(bkBank& bank)
{
	const char* techniqueNames[64];
	int cnt = m_pShader->GetTechniqueCount();

	for (int i=0; i<cnt; i++)
	{
		grcEffectTechnique tech = m_pShader->GetTechniqueByIndex(i);
		techniqueNames[i] = m_pShader->GetTechniqueName(tech);
	}

	bank.AddCombo("Shader Technique", &m_techIdx, cnt, techniqueNames, datCallback(MFA(ptxgpuShader::SetTechniqueFromList), this));
}
# else
void ptxgpuShader::AddWidgets(bkBank&) { }
# endif
#endif


#if EFFECT_PRESERVE_STRINGS
const char *ptxgpuShader::GetTechniqueName() const
{ 
	if(m_techniqueId != grcetNONE)
	{
		return m_pShader->GetTechniqueName(m_techniqueId);
	}
	return "Unspecified";
}
#endif

#if __D3D11 || RSG_ORBIS
void ptxgpuShader::SetPositionTextures(grcTexture* pPos, grcTexture* pVel)
{
	m_pShader->SetVar(m_positionDestSurfacePosID, pPos);
	m_pShader->SetVar(m_positionDestSurfaceVelID, pVel);
}
#endif// __D3D11

/********************************************************************************************************************************************/
/* Class ptxgpuRenderShaderBase.																											*/
/********************************************************************************************************************************************/


ptxgpuRenderBaseShader::ptxgpuRenderBaseShader(const char *pName) : ptxgpuShader(pName)
{
#if __D3D11 || RSG_ORBIS
	m_ParticleTextureSizeID = m_pShader->LookupVar("ParticleTextureSize");
#endif //__D3D11
}

ptxgpuRenderBaseShader::~ptxgpuRenderBaseShader()
{

}


// Sets the particle texture size.
void ptxgpuRenderBaseShader::SetParticleTextureSize(u32 Width, u32 Height)
{
#if __D3D11 || RSG_ORBIS
	Assert(m_pShader);
	Vector4 V((float)Width, (float)Height, 1.0f/(float)Width, 1.0f/(float)Height);
	m_pShader->SetVar(m_ParticleTextureSizeID, V);
#else
	(void)Width;
	(void)Height;
#endif //__D3D11
}

/********************************************************************************************************************************************/
/* Class ptxgpuUpdateBaseShader																												*/
/********************************************************************************************************************************************/

void ptxgpuUpdateBaseShader::SetupShader()
{
	m_gravityWindTimeStepCutID = m_pShader->LookupVar("GravityWindTimeStepCut");
	m_boxForwardID = m_pShader->LookupVar("BoxForward");
	m_boxRightID = m_pShader->LookupVar("BoxRight");
	m_boxCentrePosID = m_pShader->LookupVar("BoxCentrePos");
	m_refParticlePosID = m_pShader->LookupVar("RefParticlePos");
	m_offsetParticlePosID = m_pShader->LookupVar("OffsetParticlePos");

#if !(__D3D11 || RSG_ORBIS)
	m_positionDestSurfacePosID = m_pShader->LookupVar("ParticlePos"); 
	m_positionDestSurfaceVelID = m_pShader->LookupVar("ParticleVel"); 
#endif // !__D3D11

	m_resetParticlesID = m_pShader->LookupVar("ResetParticles");

	m_heightMapID = m_pShader->LookupVar("ParticleCollisionTexture"); 
	m_noiseTexID = m_pShader->LookupVar("NoiseTexture"); 

	m_heightMapMtxID = m_pShader->LookupVar("DepthViewProjMtx");

	// wind related
	m_windBaseVelID = m_pShader->LookupVar("WindBaseVel");
	m_windGustVelID = m_pShader->LookupVar("WindGustVel");
	m_windVarianceVelID = m_pShader->LookupVar("WindVarianceVel");
	m_windMultParamsID = m_pShader->LookupVar("WindMultParams");

#if PTXGPU_USE_WIND_DISTURBANCE
	m_windDistFieldXYTexID = m_pShader->LookupVar("DisturbanceFieldXY");
	m_windDistFieldZTexID = m_pShader->LookupVar("DisturbanceFieldZ");
#endif
}

void ptxgpuUpdateBaseShader::SetWindVariables(WindType_e windType) 
{
	Vec4V vGlobalWindBaseVel		= Vec4V(WIND.GetBaseVelocity(windType));
	Vec4V vGlobalWindGustVel		= Vec4V(WIND.GetGustVelocity(windType));
	Vec4V vGlobalWindVarianceVel	= Vec4V(WIND.GetGlobalVariationVelocity(windType));			

	// pack some scalar data on spare w components
	vGlobalWindBaseVel.SetW(WIND.GetSpeed(windType));
	vGlobalWindVarianceVel.SetW(WIND.GetPositionalVariationMult(windType));

	Vec4V vWindMultParams(  WIND.GetGlobalVariationSinPosMult(windType),
							WIND.GetGlobalVariationSinTimeMult(windType),	
							WIND.GetGlobalVariationCosPosMult(windType),
							WIND.GetGlobalVariationCosTimeMult(windType));

	m_pShader->SetVar(m_windBaseVelID, vGlobalWindBaseVel);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gWindBaseVel = vGlobalWindBaseVel;
#endif

	m_pShader->SetVar(m_windGustVelID, vGlobalWindGustVel);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gWindGustVel = vGlobalWindGustVel;
#endif

	m_pShader->SetVar(m_windVarianceVelID, vGlobalWindVarianceVel);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gWindVarianceVel = vGlobalWindVarianceVel;
#endif

	m_pShader->SetVar(m_windMultParamsID, vWindMultParams);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gWindMultParams = vWindMultParams;
#endif
}

void ptxgpuUpdateBaseShader::SetHeightMapTransformMtx(const Matrix44& DepthViewProjMtx)
{
	m_pShader->SetVar( m_heightMapMtxID, DepthViewProjMtx );
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gDepthViewProjMtx = DepthViewProjMtx;
#endif
}

void ptxgpuUpdateBaseShader::SetResetParticles(bool shouldReset)
{
	if (shouldReset)
	{
		m_pShader->SetVar( m_resetParticlesID, 1.0f );
		#if RAIN_UPDATE_CPU
			m_CpuUpdateParams.m_gResetParticles = 1.0f;
		#endif
	}
	else
	{
		m_pShader->SetVar( m_resetParticlesID, 0.0f );
		#if RAIN_UPDATE_CPU
			m_CpuUpdateParams.m_gResetParticles = 0.0f;
		#endif
	}
}

void ptxgpuUpdateBaseShader::SetEmitterTransform(const Matrix44& trans, const Vector3 &boxCentreOffset, const Vector3 &refPosition, const Vector3 &posOffset)
{
	Vector3 boxUp;
	Vector3 boxForward;
	Vector3 boxRight;
	if (m_instanceId == GROUND_SYSTEM_ID)
	{
		// use the camera matrix but only in the xy plane
		boxUp = Vector3(0.0f, 0.0f, 1.0f);
		boxForward = -trans.c.GetVector3() * Vector3(1.0f, 1.0f, 0.0f);
		boxRight = trans.a.GetVector3() * Vector3(1.0f, 1.0f, 0.0f);
	}
	else
	{
		// use the full camera matrix
		boxUp = trans.b.GetVector3();
		boxForward = -trans.c.GetVector3();
		boxRight = trans.a.GetVector3();
	}

	boxForward.Normalize();
	boxRight.Normalize();
	Vector3 boxCentrePos = trans.d.GetVector3() + (boxCentreOffset.x * boxRight) + (boxCentreOffset.y * boxForward) + (boxCentreOffset.z * boxUp);

	m_pShader->SetVar(m_boxForwardID, boxForward);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gBoxForward = boxForward;
#endif

	m_pShader->SetVar(m_boxRightID, boxRight);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gBoxRight = boxRight;
#endif

	m_pShader->SetVar(m_boxCentrePosID, boxCentrePos);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gBoxCentrePos = boxCentrePos;
#endif

	m_pShader->SetVar(m_refParticlePosID, refPosition);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gRefParticlePos = refPosition;
#endif

	m_pShader->SetVar(m_offsetParticlePosID, posOffset);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gOffsetParticlePos = posOffset;
#endif
}


void ptxgpuUpdateBaseShader::SetPositionAndVelocityTextures(grcTexture* pPos, grcTexture* pVel)
{
#if !(__D3D11 || RSG_ORBIS)
	m_pShader->SetVar(m_positionDestSurfacePosID, pPos);
	m_pShader->SetVar(m_positionDestSurfaceVelID, pVel);
#else // !__D3D11
	SetPositionTextures(pPos, pVel);
#endif // !__D3D11
}

#if RAIN_UPDATE_CPU
//
//
//
//
bool ptxgpuUpdateBaseShader::SetPositionAndVelocitySrc(grcTexture* pPos, grcTexture* pVel)
{
	Assert(pPos);
	Assert(pVel);

	if(!pPos->LockRect(0, 0, m_CpuUpdateParams.m_ParticlePosLock[0], grcsRead))
	{
		Assertf(0, "Error locking Pos src!");
		return false;
	}

	if(!pVel->LockRect(0, 0, m_CpuUpdateParams.m_ParticleVelLock[0], grcsRead))
	{
		Assertf(0, "Error locking Vel src!");
		return false;
	}

	return true;
}

//
//
//
//
void ptxgpuUpdateBaseShader::UnSetPositionAndVelocitySrc(grcTexture* pPos, grcTexture* pVel)
{
	Assert(pPos);
	Assert(pVel);

	pPos->UnlockRect(m_CpuUpdateParams.m_ParticlePosLock[0]);
	sysMemSet(&m_CpuUpdateParams.m_ParticlePosLock[0], 0x00, sizeof(grcTextureLock));

	pVel->UnlockRect(m_CpuUpdateParams.m_ParticleVelLock[0]);
	sysMemSet(&m_CpuUpdateParams.m_ParticleVelLock[0], 0x00, sizeof(grcTextureLock));
}

//
//
//
//
bool ptxgpuUpdateBaseShader::SetPositionAndVelocityDst(grcTexture* pPos, grcTexture* pVel)
{
	Assert(pPos);
	Assert(pVel);

	if(!pPos->LockRect(0, 0, m_CpuUpdateParams.m_ParticlePosLock[1], grcsWrite))
	{
		Assertf(0, "Error locking Pos dst!");
		return false;
	}

	if(!pVel->LockRect(0, 0, m_CpuUpdateParams.m_ParticleVelLock[1], grcsWrite))
	{
		Assertf(0, "Error locking Vel dst!");
		return false;
	}

	return true;
}

//
//
//
//
void ptxgpuUpdateBaseShader::UnSetPositionAndVelocityDst(grcTexture* pPos, grcTexture* pVel)
{
	Assert(pPos);
	Assert(pVel);

	pPos->UnlockRect(m_CpuUpdateParams.m_ParticlePosLock[1]);
	sysMemSet(&m_CpuUpdateParams.m_ParticlePosLock[1], 0x00, sizeof(grcTextureLock));

	pVel->UnlockRect(m_CpuUpdateParams.m_ParticleVelLock[1]);
	sysMemSet(&m_CpuUpdateParams.m_ParticleVelLock[1], 0x00, sizeof(grcTextureLock));
}
#endif //RAIN_UPDATE_CPU...

//
// Note: on PSN this method must be called twice, like that:
//
// ...
// ...
// ...
// ptxgpuUpdateBaseShader::SetHeightMap(map);
// ...
// ptxgpuUpdateBaseShader::Update(...);
// ...
// ptxgpuUpdateBaseShader::SetHeightMap(NULL);
// ...
// ...
// ...
//
void ptxgpuUpdateBaseShader::SetHeightMap(const grcTexture* pHeightMap)
{
	m_pShader->SetVar(m_heightMapID, pHeightMap);
}

#if RAIN_UPDATE_CPU
bool ptxgpuUpdateBaseShader::SetHeightMapSrc(const grcTexture* pHeightMap)
{
	if(pHeightMap)
	{
		if(!pHeightMap->LockRect(0, 0, m_CpuUpdateParams.m_HeightMapTextureLock, grcsRead))
		{
			Assertf(0, "Error locking HeightMap!");
			return false;
		}
	}

	return true;
}


void ptxgpuUpdateBaseShader::UnSetHeightMapSrc(const grcTexture* pHeightMap)
{
	if(pHeightMap)
	{
		pHeightMap->UnlockRect(m_CpuUpdateParams.m_HeightMapTextureLock);
		sysMemSet(&m_CpuUpdateParams.m_HeightMapTextureLock, 0x00, sizeof(grcTextureLock));
	}
}
#endif//RAIN_UPADTE_CPU...

void ptxgpuUpdateBaseShader::SetNoiseMap(const grcTexture* pNoiseMap)
{	
	m_pShader->SetVar(m_noiseTexID, pNoiseMap);
}

#if PTXGPU_USE_WIND_DISTURBANCE
void ptxgpuUpdateBaseShader::SetWindDisturbanceTextures(grcTexture* pWindDistXY, grcTexture* pWindDistZ)
{
	m_pShader->SetVar(m_windDistFieldXYTexID, pWindDistXY);
	m_pShader->SetVar(m_windDistFieldZTexID, pWindDistZ);
}
#endif // PTXGPU_USE_WIND_DISTURBANCE

//
//
//
//
static inline grcFvf::grcDataSize GetRainFvfDataSize()
{
	return grcFvf::grcdsFloat4;
}

/********************************************************************************************************************************************/
/* Class ptxgpuBase.																														*/
/********************************************************************************************************************************************/

const int ptxgpuBase::m_numSurfaces = 2; // Always use 2 surfaces

grcBlendStateHandle			ptxgpuBase::ms_exitUpdateBlendState;
grcDepthStencilStateHandle	ptxgpuBase::ms_exitUpdateDepthStencilState;
grcRasterizerStateHandle	ptxgpuBase::ms_exitUpdateRasterizerState;

grcBlendStateHandle			ptxgpuBase::ms_exitRenderBlendState;
grcDepthStencilStateHandle	ptxgpuBase::ms_exitRenderDepthStencilState;
grcRasterizerStateHandle	ptxgpuBase::ms_exitRenderRasterizerState;

grcBlendStateHandle			ptxgpuBase::ms_updateBlendState;
grcDepthStencilStateHandle	ptxgpuBase::ms_updateDepthStencilState;
grcRasterizerStateHandle	ptxgpuBase::ms_updateRasterizerState;

grcBlendStateHandle			ptxgpuBase::ms_renderBlendState;
grcDepthStencilStateHandle	ptxgpuBase::ms_renderDepthStencilState;
grcRasterizerStateHandle	ptxgpuBase::ms_renderRasterizerState;

void ptxgpuBase::InitRenderStateBlocks()
{
	// exit state blocks
	ms_exitUpdateRasterizerState = grcStateBlock::RS_NoBackfaceCull;
	ms_exitRenderRasterizerState = grcStateBlock::RS_NoBackfaceCull;

	grcBlendStateDesc defaultExitStateB;
	defaultExitStateB.BlendRTDesc[0].BlendEnable = 1;
	defaultExitStateB.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	defaultExitStateB.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_SRCALPHA;
	ms_exitUpdateBlendState = grcStateBlock::CreateBlendState(defaultExitStateB);
	ms_exitRenderBlendState = ms_exitUpdateBlendState;

	grcDepthStencilStateDesc defaultExitStateDS;
	defaultExitStateDS.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	defaultExitStateDS.DepthWriteMask = 0;
	ms_exitUpdateDepthStencilState = grcStateBlock::CreateDepthStencilState(defaultExitStateDS);
	ms_exitRenderDepthStencilState = ms_exitUpdateDepthStencilState;

	// state blocks for update
	ms_updateRasterizerState = grcStateBlock::RS_NoBackfaceCull;
	ms_updateBlendState = grcStateBlock::BS_Default;

	grcDepthStencilStateDesc updateStateDS;
	updateStateDS.DepthFunc = grcRSV::CMP_ALWAYS;
	updateStateDS.DepthWriteMask = 0;
	updateStateDS.DepthEnable = 0;
	ms_updateDepthStencilState = grcStateBlock::CreateDepthStencilState(updateStateDS);

	// state blocks for rendering
	ms_renderRasterizerState = grcStateBlock::RS_NoBackfaceCull;

	grcBlendStateDesc renderBlendStateDesc;
	renderBlendStateDesc.BlendRTDesc[0].RenderTargetWriteMask = grcRSV::COLORWRITEENABLE_ALL;
	renderBlendStateDesc.BlendRTDesc[0].BlendEnable = 1;
	renderBlendStateDesc.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	renderBlendStateDesc.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_SRCALPHA;
	renderBlendStateDesc.IndependentBlendEnable = 1;
	ms_renderBlendState = grcStateBlock::CreateBlendState(renderBlendStateDesc);


	grcDepthStencilStateDesc renderStateDSDesc;
	renderStateDSDesc.DepthEnable = 1;
	renderStateDSDesc.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	renderStateDSDesc.DepthWriteMask = 0;
	ms_renderDepthStencilState = grcStateBlock::CreateDepthStencilState(renderStateDSDesc);
}


extern u32 g_AllowVertexBufferVramLocks;

RETURNCHECK( grcDevice::Result)  ptxgpuBase::CreateVertexBuffers( int numParticles )
{
	m_dummyVdecl = NULL;
		
	SETTOZERO( m_dummyVertexBuffer);

#if !(__D3D11 || RSG_ORBIS)
	grcFvf::grcDataSize dataSize = GetRainFvfDataSize();
#endif // !(__D3D11 || RSG_ORBIS)

	g_AllowVertexBufferVramLocks++;

	#if RAIN_UPDATE_USES_STR_HEAP
		// nothing to do now
	#else
	#if !(__D3D11 || RSG_ORBIS)
		CreateDummyVertexBuffers();
	#endif //!__D3D11
	#endif

#if __D3D11 || RSG_ORBIS
	(void)numParticles;
	grcVertexElement elements[1] =
	{
		// The texture coordinates of each corner of the quad.
		grcVertexElement( 0, grcVertexElement::grcvetTexture, 0, grcFvf::GetDataSizeFromType(grcFvf::grcdsFloat2), grcFvf::grcdsFloat2 ),
	};
	m_pInstanceVertexBuffer = CreateInstancedVertexBuffer(0);
	m_dummyVdecl = AssertRetVal( GRCDEVICE.CreateVertexDeclaration( elements, NELEM( elements ) ) );

#elif __WIN32PC && !__D3D11
	(void)numParticles;
	// TODO:- Create a big buffer of mini quad-strips all glued together with degenerates plus the uv look up for each vertex to mimic the DX11 version.
	// DX9 version is not currently implemented.
	(void)dataSize;
	m_pInstanceVertexBuffer = NULL;
	m_dummyVdecl = NULL;
#else

	grcVertexElement elements[3] =
	{
		grcVertexElement( 0, grcVertexElement::grcvetTexture, 0, grcFvf::GetDataSizeFromType(dataSize) , dataSize,grcFvf::grcsfmDivide, 4 ),
		grcVertexElement( 1, grcVertexElement::grcvetTexture, 1,  grcFvf::GetDataSizeFromType(dataSize) , dataSize,grcFvf::grcsfmDivide, 4 ),
		grcVertexElement( 2, grcVertexElement::grcvetTexture, 2, grcFvf::GetDataSizeFromType(grcFvf::grcdsFloat2), grcFvf::grcdsFloat2 , grcFvf::grcsfmModulo, 4 )
		// TODO : CreateInstancedVertexElement( 4 );
	};
	m_pInstanceVertexBuffer = CreateInstancedVertexBuffer( 4, NELEM( elements ), numParticles );
	m_dummyVdecl = AssertRetVal( GRCDEVICE.CreateVertexDeclaration( elements, NELEM( elements ) ) );

#endif	

	g_AllowVertexBufferVramLocks--;

	return 0;
}

bool ptxgpuBase::InitParticles()
{
// DX9 version not implemented.
#if !(__WIN32PC && !__D3D11) && !RSG_ORBIS

	u8* pData[m_numSurfaces]={NULL};
	rage::grcTextureLock lock[m_numSurfaces];

	for(int i=0; i<m_numSurfaces; i++ )
	{
#if !__D3D11
		m_destSurfaces[i]->LockRect(0,0,lock[i], grcsRead | grcsWrite | grcsAllowVRAMLock);
#else //!__D3D11
		m_Surfaces[0].m_Textures[i]->LockRect(0, 0, lock[i], grcsRead | grcsWrite);
#endif //!__D3D11
		Assert( lock[i].Base );
		pData[i] = (u8*)lock[i].Base;
	}
	int height = lock[0].Height;
	
	// Initialize the particle data
	// Zero means that the particle is dead, so we will just re-spawn all particles with random values
	for(int i = 0; i < m_numSurfaces; i++)
	{
		memset(pData[i], 0, height * lock[i].Pitch);
	}

	for(int i=0; i< m_numSurfaces; i++)
	{
#if !__D3D11
		m_destSurfaces[i]->UnlockRect(lock[i]);
#else //!__D3D11
		m_Surfaces[0].m_Textures[i]->UnlockRect(lock[i]);
#endif //!__D3D11
	}

#if __D3D11
	// Copy the initial state into the 2nd texture.
	for(int Idx=0; Idx<2; Idx++)
	{
		rage::grcTextureLock Src, Dest;
		m_Surfaces[0].m_Textures[Idx]->LockRect(0, 0, Src, grcsRead);
		m_Surfaces[1].m_Textures[Idx]->LockRect(0, 0, Dest, grcsWrite);

		char *pSrc = (char *)Src.Base;
		char *pDest = (char *)Dest.Base;

		for(int i=0; i < height; i++)
		{
			memcpy(pDest, pSrc, (Src.BitsPerPixel>>3)*Src.Width);
			pSrc += Src.Pitch;
			pDest += Dest.Pitch;
		}
		m_Surfaces[1].m_Textures[Idx]->UnlockRect(Dest);
		m_Surfaces[0].m_Textures[Idx]->UnlockRect(Src);
	}
#endif //_D3D11

#endif //!(__WIN32PC && !__D3D11) && !RSG_ORBIS

	return true;
}

ptxgpuBase::ptxgpuBase()
{
	m_isSimulationPaused = false;
	m_instanceId = 0xff;

	m_pInstanceVertexBuffer = 0;

#if !(__D3D11 || RSG_ORBIS)
	SETTOZERO(m_destSurfaces);
#else //!__D3D11
	SETTOZERO(m_Surfaces);
#endif //!__D3D11

	m_dummyVdecl = NULL;

	SETTOZERO( m_dummyVertexBuffer );

#if RAIN_UPDATE_USES_STR_HEAP
	StreamingMemoryInit();
#endif

#if __BANK
	m_disableRendering = false;
#endif

#if RAIN_UPDATE_CPU
	m_RainUpdateCpuTaskHandle = 0;
#endif
}

ptxgpuBase::~ptxgpuBase()
{
}

#if RAIN_UPDATE_CPU_HEIGHTMAP
const u32 COLLISION_MAP_WIDTH = 128;
const u32 COLLISION_MAP_HEIGHT= 128;
static float sm_defaultHeightMap[COLLISION_MAP_WIDTH*COLLISION_MAP_HEIGHT] = {0};
#endif

bool ptxgpuBase::Init(unsigned int instanceId, int numParticles, ptxgpuUpdateBaseShader* pUpdateShader, ptxgpuRenderBaseShader* pRenderShader, grcRenderTarget* UNUSED_PARAM(pParentTarget), bool UNUSED_PARAM(useHitPlane))
{
	// Create the static textures
	PTXGPU_USE_WIND_DISTURBANCE_ONLY(CreateWindDisturbanceTextures();)
	CreateNoiseTexture();

	m_instanceId			= instanceId;
	m_numParticles			= numParticles;
	m_isSimulationPaused	= false;
	AssertReturn( pUpdateShader );
	AssertReturn( pRenderShader );

	m_textureWidth  = 128;
    m_textureHeight = numParticles / m_textureWidth;
	m_pUpdateShader = pUpdateShader;
	m_pRenderShader = pRenderShader;
	m_pRenderShader->SetParticleTextureSize(m_textureWidth, m_textureHeight);

#if RAIN_UPDATE_USES_STR_HEAP
	SETTOZERO(m_destSurfaces);
	// Do nothing!
#else

#if !(__D3D11 || RSG_ORBIS)
	SETTOZERO(m_destSurfaces);
	CreateDestSurfaces(NULL, 0 );
#else //!__D3D11
	SETTOZERO(m_Surfaces);
	CreateTexturesAndRenderTargets();
#endif // !__D3D11

	if ( !InitParticles() )
	{
		return false;
	}
#endif // RAIN_UPDATE_USES_STR_HEAP

#if !(__D3D11 || RSG_ORBIS)
	m_pUpdateShader->SetPositionAndVelocityTextures(m_destSurfaces[0], m_destSurfaces[1]);
#endif //!__D3D11

	m_pUpdateShader->SetNoiseMap( ms_pNoiseTexture );
	PTXGPU_USE_WIND_DISTURBANCE_ONLY(m_pUpdateShader->SetWindDisturbanceTextures(ms_pWindDisturbanceTexture[0], ms_pWindDisturbanceTexture[1]);)
	
	if ( CreateVertexBuffers( numParticles ) != 0 )
	{
		return false;
	}

	m_gravity = 0.0f;
	m_windInfluence = 0.0f;
	m_timeControl = 1.0f;
	m_RefPosition = Vector3(0.0f, 0.0f, 0.0f);

	// value to randomize the particles without requiring a complex setup.
	Randomize();

#if RAIN_UPDATE_CPU
	m_pUpdateShader->ResetCpuUpdateParams();

	#if RAIN_UPDATE_CPU_HEIGHTMAP
		for(u32 i=0; i<COLLISION_MAP_WIDTH*COLLISION_MAP_HEIGHT; i++)
		{
			sm_defaultHeightMap[i] = 10000.0f;
		}
	#endif
#endif

	return true;
}

void ptxgpuBase::Shutdown()
{	
	// free the assets 
	m_pUpdateShader = 0;
	m_pRenderShader = 0;


#if !(__D3D11 || RSG_ORBIS)
	DeleteDestSurfaces(NULL, 0);
	DeleteDummyVertexBuffers();
#else
	DeleteTexturesAndRenderTargets();
#endif //__D3D11

	// Delete the static textures
	PTXGPU_USE_WIND_DISTURBANCE_ONLY(DeleteWindDisturbanceTextures();)
	LastSafeRelease(ms_pNoiseTexture);

	if(m_dummyVdecl)
	{
		LastSafeRelease(m_dummyVdecl);
		m_dummyVdecl = NULL;
	}

	if(m_pInstanceVertexBuffer)
	{
		delete m_pInstanceVertexBuffer;	
		m_pInstanceVertexBuffer = NULL;
	}
}

#if RAIN_UPDATE_CPU
	static void RainUpdateCpuTask(sysTaskParameters &params);
	#if __DEV
		static dev_bool sm_bDropRunAsync	= true;
		static dev_bool sm_bMistRunAsync	= true;
		static dev_bool sm_bGroundRunAsync	= true;
	#endif
#endif

//
//
//
//
bool ptxgpuBase::Update(grcViewport* pViewport, float UNUSED_PARAM(deltaTime), WindType_e UNUSED_PARAM(windType), float UNUSED_PARAM(ratioToRender), float UNUSED_PARAM(cutValue), bool resetParticles)
{	
	int i;

#if RSG_PC
	if (PARAM_nogpuparticles.Get())
		return false;
#endif

#if RAIN_UPDATE_USES_STR_HEAP
	if(m_RainHeap.GetState() != RAINHEAP_STATE_ALLOCATED)
	{	
		// don't update if VRam buffers not allocated
		return(false);
	}
#endif

	if (!pViewport)
	{
		return false;
	}
	
	m_pUpdateShader->SetResetParticles(resetParticles);


#if RAIN_UPDATE_CPU

	if(sm_bRainUpdateOnCpu	BANK_ONLY(&& sm_bBankRainUpdateOnCpu))
	{
		/////////////////
		// CPU Update: //
		/////////////////
		const u32 DestIndex = m_SurfaceIndex ^ 0x1;

		bool bRunAsync = true;
		#if __DEV
			if((!sm_bDropRunAsync) && (this->m_instanceId==DROP_SYSTEM_ID))
			{
				bRunAsync = false;
			}

			if((!sm_bMistRunAsync) && (this->m_instanceId==MIST_SYSTEM_ID))
			{
				bRunAsync = false;
			}

			if((!sm_bGroundRunAsync) && (this->m_instanceId==GROUND_SYSTEM_ID))
			{
				bRunAsync = false;
			}
		#endif

		if(bRunAsync && m_RainUpdateCpuTaskHandle)	// Late Wait (#2) (Main Wait (#1) in early in Render()): run async - wait for prev instance
		{
			sysTaskManager::Wait( m_RainUpdateCpuTaskHandle );
			m_RainUpdateCpuTaskHandle = 0;

			// Unset up previous the destination (write) textures.
			GetUpdateShader()->UnSetPositionAndVelocityDst(m_Surfaces[m_SurfaceIndex].m_Textures[0],		m_Surfaces[m_SurfaceIndex].m_Textures[1]);	// write only
		}


		// Set up the source textures.
		GetUpdateShader()->SetPositionAndVelocitySrc(m_Surfaces[m_SurfaceIndex].m_Textures[0],	m_Surfaces[m_SurfaceIndex].m_Textures[1]);
		// Set the destination targets.
		GetUpdateShader()->SetPositionAndVelocityDst(m_Surfaces[DestIndex].m_Textures[0],		m_Surfaces[DestIndex].m_Textures[1]);

		// run update on CPU:
		Assertf(m_RainUpdateCpuTaskHandle==0, "RainUpdateCpu: Update task already running!");


		ptxcpuUpdateBaseParams *pUpdateParams = GetUpdateShader()->GetCpuUpdateParamsCopy();
		pUpdateParams->m_gInstanceId	= this->m_instanceId;			// DROP/MIST/GROUND_SYSTEM_ID
		pUpdateParams->m_gNumParticles	= this->m_numParticles;

		sysTaskParameters params;
		params.UserData[0].asPtr = pUpdateParams;
		m_RainUpdateCpuTaskHandle = rage::sysTaskManager::Create(TASK_INTERFACE(RainUpdateCpuTask), params);
	

		if((!bRunAsync) && m_RainUpdateCpuTaskHandle)	// run in sync
		{
			sysTaskManager::Wait( m_RainUpdateCpuTaskHandle );
			m_RainUpdateCpuTaskHandle = 0;

			// Unset up the source and destination textures.
			GetUpdateShader()->UnSetPositionAndVelocitySrc(m_Surfaces[m_SurfaceIndex].m_Textures[0],m_Surfaces[m_SurfaceIndex].m_Textures[1]);	// read-only
			GetUpdateShader()->UnSetPositionAndVelocityDst(m_Surfaces[DestIndex].m_Textures[0],		m_Surfaces[DestIndex].m_Textures[1]);		// write-only
		}
		else
		{
			// Unset up the source (read-only) textures.
			GetUpdateShader()->UnSetPositionAndVelocitySrc(m_Surfaces[m_SurfaceIndex].m_Textures[0],m_Surfaces[m_SurfaceIndex].m_Textures[1]);	// read-only
		}

		// "Flip" buffers.
		m_SurfaceIndex = DestIndex;
	}
	else
#endif //RAIN_UPDATE_CPU...
	{
		/////////////////
		// GPU Update: //
		/////////////////
		grcStateBlock::SetRasterizerState(ms_updateRasterizerState);
		grcStateBlock::SetBlendState(ms_updateBlendState);
		grcStateBlock::SetDepthStencilState(ms_updateDepthStencilState);

	#if __D3D11 || RSG_ORBIS
		const u32 DestIndex = m_SurfaceIndex ^ 0x1;

		// Set up the source textures.
	#if RSG_ORBIS
		GetUpdateShader()->SetPositionAndVelocityTextures(m_Surfaces[m_SurfaceIndex].m_RTs[0], m_Surfaces[m_SurfaceIndex].m_RTs[1]);
	#else
		GetUpdateShader()->SetPositionAndVelocityTextures(m_Surfaces[m_SurfaceIndex].m_Textures[0], m_Surfaces[m_SurfaceIndex].m_Textures[1]);
	#endif

		// Set the destination targets.
		const grcRenderTarget *Color[grcmrtColorCount] = { NULL };

		for(i=0; i<2; i++)
		{
			Color[i] = m_Surfaces[DestIndex].m_RTs[i];
		}
		grcTextureFactory::GetInstance().LockMRT(Color, NULL);

		// "Flip" buffers.
		m_SurfaceIndex = DestIndex;
	#endif //#if __D3D11 || RSG_ORBIS...

	#if !(__WIN32PC && !__D3D11)
	#if RSG_PC && NV_SUPPORT
		PF_PUSH_TIMEBAR("BeginRscRendering");
		bool bUseNVSLIApi = GRCDEVICE.UsingMultipleGPUs() && (GRCDEVICE.GetManufacturer() == NVIDIA && GRCDEVICE.IsUsingNVidiaAPI());
		if (bUseNVSLIApi) 
		{
			GRCDEVICE.Clear(true, Color32(0x00000000),false, 0.0f, false, 0xffffffff);
			for (int i = 0; i < 2; i++)
				GRCDEVICE.BeginRscRendering(m_Surfaces[DestIndex].m_RTs[i]);
		}
		PF_POP_TIMEBAR();
	#endif

		// DX9 is not implemented.
		GetUpdateShader()->FullScreenBlit();

	#if RSG_PC && NV_SUPPORT
		if (bUseNVSLIApi) 
		{
			for (int i = 0; i < 2; i++)
				GRCDEVICE.EndRscRendering(m_Surfaces[DestIndex].m_RTs[i]);
		}
	#endif
	#endif

		grcResolveFlags	clearFlags;
		clearFlags.ClearColor = true;
		clearFlags.ClearDepthStencil = true;

	#if __D3D11 || RSG_ORBIS
		grcTextureFactory::GetInstance().UnlockMRT();
	#else	
		(void)i;
		// DX9 version not implemented.
		AssertMsg(NIY, "ptxgpuBase::Update()...DX9 version not implemented.");
	#endif
		grcStateBlock::SetRasterizerState(ms_exitUpdateRasterizerState);
		grcStateBlock::SetBlendState(ms_exitUpdateBlendState);
		grcStateBlock::SetDepthStencilState(ms_exitUpdateDepthStencilState);
	}

	return true;
}

bool ptxgpuBase::Render(const grcViewport* UNUSED_PARAM(pViewport), Vec3V_In UNUSED_PARAM(vCamVel), float UNUSED_PARAM(fMaxLife), float UNUSED_PARAM(camPosZ), float UNUSED_PARAM(groundPosZ), ptxRenderSetup* pRenderSetup, float ratioToRender BANK_ONLY(, Vec4V_In UNUSED_PARAM(vCamAngleLimits)))
{
#if RSG_PC
	if (PARAM_nogpuparticles.Get())
		return false;
#endif

#if __BANK
	if (m_disableRendering)
	{
		return true;
	}
#endif

#if RAIN_UPDATE_USES_STR_HEAP
	if(m_RainHeap.GetState() != RAINHEAP_STATE_ALLOCATED)
	{	
		// don't render if VRam buffers not allocated
		return(true);
	}
#endif

	//Displayf("ptxgpuBase::Render: BEGIN (%u, 0x%X) ============================================", m_gcmContextHeapId,pLabelStart);

#if RAIN_UPDATE_CPU
	if(sm_bRainUpdateOnCpu	BANK_ONLY(&& sm_bBankRainUpdateOnCpu))
	{
		bool bRunAsync = true;
		#if __DEV
			if((!sm_bDropRunAsync) && (this->m_instanceId==DROP_SYSTEM_ID))
			{
				bRunAsync = false;
			}

			if((!sm_bMistRunAsync) && (this->m_instanceId==MIST_SYSTEM_ID))
			{
				bRunAsync = false;
			}

			if((!sm_bGroundRunAsync) && (this->m_instanceId==GROUND_SYSTEM_ID))
			{
				bRunAsync = false;
			}
		#endif

		if(bRunAsync && m_RainUpdateCpuTaskHandle)	// Early wait #1 for run async - wait for prev instance
		{
			sysTaskManager::Wait( m_RainUpdateCpuTaskHandle );
			m_RainUpdateCpuTaskHandle = 0;

			// Unset up the destination (write) textures.
			GetUpdateShader()->UnSetPositionAndVelocityDst(m_Surfaces[m_SurfaceIndex].m_Textures[0],		m_Surfaces[m_SurfaceIndex].m_Textures[1]);
		}
	}
#endif //RAIN_UPDATE_CPU...


	grcStateBlock::SetRasterizerState(ms_renderRasterizerState);
	grcStateBlock::SetBlendState(ms_renderBlendState);
	grcStateBlock::SetDepthStencilState(ms_renderDepthStencilState);
#if RSG_ORBIS
	m_pRenderShader->SetPositionTextures(m_Surfaces[m_SurfaceIndex].m_RTs[0], m_Surfaces[m_SurfaceIndex].m_RTs[1]);
#elif __D3D11
	// During rendering on DX11 we look up particle attributes directly from the GPU update textures. SV_InstanceID is used
	// to calculate uv coords.
	m_pRenderShader->SetPositionTextures(m_Surfaces[m_SurfaceIndex].m_Textures[0], m_Surfaces[m_SurfaceIndex].m_Textures[1]);
#endif //__D3D11

	if (m_pRenderShader->BeginDraw())
	{
		int numToRender = (int)((float)m_numParticles*ratioToRender);

		if(numToRender >0)
		{
		#if __D3D11 || RSG_ORBIS

			GRCDEVICE.SetVertexDeclaration(m_dummyVdecl);

			// Set the quad corner UV instancing vertex buffer.
			GRCDEVICE.SetStreamSource(0, *m_pInstanceVertexBuffer, 0, m_pInstanceVertexBuffer->GetVertexStride());

			// Render particles.
			GRCDEVICE.DrawInstancedPrimitive(drawTriStrip, 4, numToRender, 0, 0);

		#else
			// DX9 version is not implemented.
			AssertMsg(NIY, "ptxgpuBase::Render()...DX9 version not implemented.");
		#endif 
		}
	}
	m_pRenderShader->EndDraw();

	grcStateBlock::SetRasterizerState(ms_exitRenderRasterizerState);
	grcStateBlock::SetBlendState(ms_exitRenderBlendState);
	grcStateBlock::SetDepthStencilState(ms_exitRenderDepthStencilState);

	for (int i=0; i<m_numSurfaces; i++)
	{
		GRCDEVICE.ClearStreamSource(i);
	}

	if (m_pInstanceVertexBuffer)
	{
		GRCDEVICE.ClearStreamSource(m_numSurfaces);
	}

	GRCDEVICE.SetVertexDeclaration(NULL);

	if (pRenderSetup)
	{
		pRenderSetup->PostDraw(NULL, NULL);
	}

	return true;
}


#if !(__D3D11 || RSG_ORBIS)
//
//
//
//
void ptxgpuBase::CreateDestSurfaces(u8 * UNUSED_PARAM(pHeapMemPtr), u32 UNUSED_PARAM(HeapMemSize), grcRenderTarget* )
{
	grcTextureFactory::CreateParams params;
	params.PoolID = kRTPoolIDAuto;
	params.AllocateFromPoolOnCreate = true;
	params.HasParent = false;
	params.Parent = 0;
	params.Multisample = 0;
	params.UseFloat = true;
	params.Format = grctfA32B32G32R32F;
	params.InLocalMemory = true; // Put this in VRAM
	int bitDepth = 128;
#if __WIN32PC
	params.Lockable = true;
#endif		

	for ( int i =0; i < m_numSurfaces; i++ )
	{
		m_destSurfaces[i] = AssertRetVal( grcTextureFactory::GetInstance().CreateRenderTarget( "GPU particle Position XY", grcrtPermanent, m_textureWidth, m_textureHeight, bitDepth, &params) );
	}
}

//
//
//
//
void ptxgpuBase::DeleteDestSurfaces(u8 *UNUSED_PARAM(pHeapMemPtr), u32 UNUSED_PARAM(HeapMemSize))
{
	for(int i=0; i<m_numSurfaces; i++)
	{
		if(m_destSurfaces[i])
		{
			for ( int i = 0; i < m_numSurfaces; i++ )
			{
				LastSafeRelease( m_destSurfaces[i] );
			}
		}
	}
}

#else //!__D3D11

void ptxgpuBase::CreateTexturesAndRenderTargets()
{
	int i;
	grcTextureFactory::TextureCreateParams TextureParams(grcTextureFactory::TextureCreateParams::SYSTEM, 
		grcTextureFactory::TextureCreateParams::LINEAR,	grcsRead|grcsWrite, NULL, 
		grcTextureFactory::TextureCreateParams::RENDERTARGET, 
		grcTextureFactory::TextureCreateParams::MSAA_NONE);

#if RSG_ORBIS
	grcTextureFactory::CreateParams params;
	params.UseFloat		= true;
	params.HasParent	= true; 
	params.Parent		= NULL;
	params.IsSRGB		= true;
	params.Multisample = 0;
	params.Format = grctfA32B32G32R32F;
	params.MipLevels = 1;
#endif //RSG_ORBIS

	// Create the double buffered textures/render targets.
	for(i=0; i<2; i++)
	{
		int j;

		for(j=0; j<m_numSurfaces; j++)
		{
			char ResourceName[1024];
			sprintf(ResourceName, "[%d:%d]ptxgpuBase:", i, j);

	#if EFFECT_PRESERVE_STRINGS
			if(m_pUpdateShader)
			{
				strcat(ResourceName, m_pUpdateShader->GetImp()->GetName());
				strcat(ResourceName, ":");
				strcat(ResourceName, m_pUpdateShader->GetTechniqueName());
			}
			strcat(ResourceName, " | ");
			if(m_pRenderShader)
			{
				strcat(ResourceName, m_pRenderShader->GetImp()->GetName());
				strcat(ResourceName, ":");
				strcat(ResourceName, m_pRenderShader->GetTechniqueName());
			}
	#endif //EFFECT_PRESERVE_STRINGS

#if RSG_ORBIS
			m_Surfaces[i].m_RTs[j] = grcTextureFactory::GetInstance().CreateRenderTarget(ResourceName, grcrtPermanent, m_textureWidth, m_textureHeight,
				grcTextureFactory::GetBitsPerPixel(params.Format), &params);
#else
			// Create textures...
			BANK_ONLY(grcTexture::SetCustomLoadName(ResourceName);)
			m_Surfaces[i].m_Textures[j] = grcTextureFactory::GetInstance().Create(m_textureWidth, m_textureHeight, grctfA32B32G32R32F, NULL, 1U /*numMips*/, &TextureParams);
			BANK_ONLY(grcTexture::SetCustomLoadName(NULL);)
			//...And render targets using the textures.
			m_Surfaces[i].m_RTs[j] = grcTextureFactory::GetInstance().CreateRenderTarget(ResourceName, m_Surfaces[i].m_Textures[j]->GetTexturePtr());
#endif
		}
	}
	// Reset the double buffering.
	m_SurfaceIndex = 0;
}

void ptxgpuBase::DeleteTexturesAndRenderTargets()
{
	int i;

	for(i=0; i<2; i++)
	{
		int j;

		for(j=0; j<m_numSurfaces; j++)
		{
			// Delete render targets...
			if(m_Surfaces[i].m_RTs[j])
			{
				LastSafeRelease(m_Surfaces[i].m_RTs[j]);
				m_Surfaces[i].m_RTs[j] = NULL;
			}
			// ...And textures.
			if(m_Surfaces[i].m_Textures[j])
			{
				LastSafeRelease(m_Surfaces[i].m_Textures[j]);
				m_Surfaces[i].m_Textures[j] = NULL;
			}
		}
	}
}

#endif //!__D3D11

#if !(__D3D11 || RSG_ORBIS)
//
//
//
//
void ptxgpuBase::CreateDummyVertexBuffers()
{
	grcFvf::grcDataSize dataSize = GetRainFvfDataSize();

	// Create vertex buffer	
	for ( int i = 0; i < m_numSurfaces ; i++ )
	{
		grcFvf fvf0;
		fvf0.SetTextureChannel( 0, true, dataSize);
		fvf0.SetPosChannel( false );
		grcTextureLock lock;
		Assert(m_destSurfaces[i]);
		m_destSurfaces[i]->LockRect( 0,0, lock, grcsRead | grcsAllowVRAMLock );
		const bool bReadWrite = false;
		const bool bDynamic = false;
		m_dummyVertexBuffer[i] =  grcVertexBuffer::Create( m_numParticles, fvf0, bReadWrite, bDynamic, lock.Base );
		m_destSurfaces[i]->UnlockRect(lock);
	}
}

//
//
//
//
void ptxgpuBase::DeleteDummyVertexBuffers()
{
	for(int i = 0; i < 4; i++ )
	{
		if(m_dummyVertexBuffer[i])
		{
			delete m_dummyVertexBuffer[i];
			m_dummyVertexBuffer[i] = NULL;
		}
	}	
}
#endif //!__D3D11

#if PTXGPU_USE_WIND_DISTURBANCE
grcTexture* ptxgpuBase::ms_pWindDisturbanceTexture[2] = {NULL, NULL};

void ptxgpuBase::CreateWindDisturbanceTextures()
{
	// have these been allocated already?
	if (ms_pWindDisturbanceTexture[0] != NULL)
	{
		Assert(ms_pWindDisturbanceTexture[1] != NULL);
		return;
	}

	const int DISTURBANCE_TEX_WIDTH = 256;
	const int DISTURBANCE_TEX_HEIGHT = 32;

	// texture 1
	grcImage* pResult = grcImage::Create(DISTURBANCE_TEX_WIDTH, DISTURBANCE_TEX_HEIGHT, 1, grcImage::G16R16, grcImage::STANDARD,0, 0);
	Assert(pResult);

	grcTextureFactory::TextureCreateParams params(grcTextureFactory::TextureCreateParams::SYSTEM, grcTextureFactory::TextureCreateParams::LINEAR);
	grcTexture* pDisturbanceTex = rage::grcTextureFactory::GetInstance().Create(pResult, &params);
	Assert(pDisturbanceTex);

	LastSafeRelease(pResult);

	ms_pWindDisturbanceTexture[0] = pDisturbanceTex;

	// texture 2
	pResult = grcImage::Create(DISTURBANCE_TEX_WIDTH, DISTURBANCE_TEX_HEIGHT, 1, grcImage::G16R16, grcImage::STANDARD,0, 0);
	Assert(pResult);

	pDisturbanceTex = rage::grcTextureFactory::GetInstance().Create(pResult, &params);
	Assert(pDisturbanceTex);

	LastSafeRelease(pResult);

	ms_pWindDisturbanceTexture[1] = pDisturbanceTex;
}

void ptxgpuBase::DeleteWindDisturbanceTextures()
{
	LastSafeRelease(ms_pWindDisturbanceTexture[0]);
	LastSafeRelease(ms_pWindDisturbanceTexture[1]);
}

// temporary workaround to shift and compress the full s16 range to positive numbers
// as at the moment there is no support to set a texture sampler to return signed values.
// it is assumed that the actual range for signed values is [-16383,16383] to be able 
// to comfortably shift numbers into the [0, 32767] without aliasing.
static inline s16 S16ToUnsignedRange(s16 x)
{
	Assert(x <= 16383 && x >= -16383);
	// mapping function fully developed for clarity
	const s16 MIN_USHORT = 0;
	const s16 MAX_USHORT = 32767;
	const s16 MIN_SSHORT = -16383;
	const s16 MAX_SSHORT = 16383;
	s16 result = MIN_USHORT + (x - MIN_SSHORT) * (MAX_USHORT-MIN_USHORT)/(MAX_SSHORT-MIN_SSHORT);
	return result;
}

void ptxgpuBase::UpdateWindDisturbanceTextures() 
{
	Assert(ms_pWindDisturbanceTexture[0] != NULL && ms_pWindDisturbanceTexture[1] != NULL);

	// lock the texture used to store the disturbance field data
	grcTextureLock lockTex1, lockTex2;
	ms_pWindDisturbanceTexture[0]->LockRect(0,0,lockTex1);
	ms_pWindDisturbanceTexture[1]->LockRect(0,0,lockTex2);

	Assert(lockTex1.Base != NULL && lockTex2.Base != NULL);
	s16* pTex1Data = (s16*)lockTex1.Base;
	s16* pTex2Data = (s16*)lockTex2.Base;

	// get access to the disturbance field data
	const u32 distFieldSizeX	= WIND.GetDisturbanceFieldWidth();
	const u32 distFieldSizeY	= WIND.GetDisturbanceFieldHeight();
	const u32 distFieldSizeZ	= WIND.GetDisturbanceFieldDepth();

	phWindField& windField = WIND.GetDisturbanceField();

	for (u32 z=0; z<distFieldSizeZ; z++)
	{
		for (u32 y=0; y<distFieldSizeY; y++)
		{
			for (u32 x=0; x<distFieldSizeX; x++)
			{
				u32 dstX = x + (z*distFieldSizeX);
				u32 dstY = y * (distFieldSizeX*distFieldSizeZ);
				u32 dstIdx = 2*(dstX+dstY);

				phWindPoint& windPoint = windField.GetData(x, y, z);

				pTex1Data[dstIdx+0] = S16ToUnsignedRange(windPoint.GetVelX());
				pTex1Data[dstIdx+1] = S16ToUnsignedRange(windPoint.GetVelY());
				pTex2Data[dstIdx+0] = S16ToUnsignedRange(windPoint.GetVelZ());
				pTex2Data[dstIdx+1] = 0;
			}			
		}
	}

	ms_pWindDisturbanceTexture[0]->UnlockRect(lockTex1);
	ms_pWindDisturbanceTexture[1]->UnlockRect(lockTex2);
}
#endif // PTXGPU_USE_WIND_DISTURBANCE

grcTexture* ptxgpuBase::ms_pNoiseTexture = NULL;
void ptxgpuBase::CreateNoiseTexture()
{
	// Create once
	if(ms_pNoiseTexture)
	{
		return;
	}

	const int NOISETEXSIZE = 128;

	grcImage* pResult = grcImage::Create(NOISETEXSIZE, NOISETEXSIZE, 1, grcImage::A8R8G8B8, grcImage::STANDARD,0, 0);
	Assert(pResult);

	for (int y = 0; y < pResult->GetHeight(); y++)
	{
		for (int x = 0; x < pResult->GetWidth(); x++)
		{
			Color32 c = Color32( rand()  & 0xFF, rand() & 0xFF, rand() & 0xFF, rand() & 0xFF);

			pResult->SetPixel(x, y, c.GetColor());
		}
	}

#if __D3D11
	grcTextureFactory::TextureCreateParams::Memory_t textureMemory = grcTextureFactory::TextureCreateParams::VIDEO;
#else
	grcTextureFactory::TextureCreateParams::Memory_t textureMemory = grcTextureFactory::TextureCreateParams::SYSTEM;
#endif
	grcTextureFactory::TextureCreateParams params(textureMemory, grcTextureFactory::TextureCreateParams::LINEAR);
	BANK_ONLY(grcTexture::SetCustomLoadName("Ptx NoiseTexture");)
	ms_pNoiseTexture = rage::grcTextureFactory::GetInstance().Create(pResult, &params);
	BANK_ONLY(grcTexture::SetCustomLoadName(NULL);)
	Assert(ms_pNoiseTexture);

	LastSafeRelease(pResult);
}

void ptxgpuBase::CalculateTransformedBoundBox(const Matrix34& mtx, Vector3& min, Vector3& max)
{
	Assert(min.IsGreaterThanV(max).IsZero());

	Vector3 pts[8] = 
	{ 
		Vector3(min.x, min.y, min.z),
		Vector3(max.x, min.y, min.z),
		Vector3(min.x, max.y, min.z),
		Vector3(min.x, min.y, max.z),
		Vector3(max.x, max.y, min.z),
		Vector3(min.x, max.y, max.z),
		Vector3(max.x, min.y, max.z),
		Vector3(max.x, max.y, max.z)
	};

	Vector3 avec;
	mtx.Transform( pts[0], avec);
	Vector3	scMax = avec;
	Vector3 scMin = avec;

	for (int i=1; i<8; i++)
	{
		mtx.Transform(pts[i], avec);
		scMin.Min(scMin, avec);
		scMax.Max(scMax, avec);
	}

	min = scMin;
	max = scMax;
}

void ptxgpuBase::Randomize()
{
	m_initialRandomize = 1024 * 2;
}

#if __BANK
void ptxgpuBase::AddWidgets(bkBank& bank)
{
	bank.PushGroup("GPU Particle System");
		bank.AddToggle("Disable Rendering", &m_disableRendering);
		bank.AddSlider("Gravity", &m_gravity, -64.0f, 64.0f, 0.1f);
		bank.AddSlider("Wind Influence", &m_windInfluence, 0.0f, 30.0f, 0.1f);
		bank.AddSlider("Time Control", &m_timeControl, 0.0f, 10.0f, 0.01f);
	bank.PopGroup();
}
#endif


#if RAIN_UPDATE_CPU

///////////////////////////////////////////////////////////////////////////////
// DepthCompareHeightMap
// Return: x = 1 - no collison, -1 - collision
//         y = collision position Z
#if RAIN_UPDATE_CPU_HEIGHTMAP
static __forceinline void DepthCompareHeightMap(Vector2& out,	float x,float y,float z,
										 const Matrix44& gDepthViewProjMtx,
										 float *ParticleCollisionPtr)
{	
//	float4 pos = mul(float4(ptxPosition.xyz, 1.0f), gDepthViewProjMtx);
//	pos.y = 1.0f - pos.y;
//	float2 depth;
//	depth.xy = fixupDepth(tex2D(ParticleCollisionSampler, pos.xy).xx);
//	depth.x = (pos.z<depth.x) ? 1.0 : -1.0f;
//	return depth;
	Vector4 pos;
	Vector4 ptxPosition(x,y,z,1.0f);
	gDepthViewProjMtx.Transform(ptxPosition, pos);
	pos.y = 1.0f - pos.y;


	s32 ix = s32( Clamp(pos.x, 0.0f, 1.0f) * (COLLISION_MAP_WIDTH-1));
	s32 iy = s32( Clamp(pos.y, 0.0f, 1.0f) * (COLLISION_MAP_HEIGHT-1));

Vector2& depth = out;
	depth.x = depth.y = ParticleCollisionPtr[ iy*(COLLISION_MAP_WIDTH) + ix ]; // 128x128 grctfR32f
	depth.x = (pos.z<depth.x) ? 1.0f : -1.0f;
}


static __forceinline void DepthCompareHeightMap(Vec4V& outX, Vec4V& outY,
										 const Vec4V& ptxPositionX,const Vec4V& ptxPositionY, const Vec4V& ptxPositionZ,
										 const Mat44V& depthViewProjMtxA, const Mat44V& depthViewProjMtxB, const Mat44V& depthViewProjMtxC, const Mat44V& depthViewProjMtxD,
										 float* ParticleCollisionPtr)
{	
//	float4 pos = mul(float4(ptxPosition.xyz, 1.0f), gDepthViewProjMtx);
//	pos.y = 1.0f - pos.y;
//	float2 depth;
//	depth.xy = fixupDepth(tex2D(ParticleCollisionSampler, pos.xy).xx);
//	depth.x = (pos.z<depth.x) ? 1.0 : -1.0f;
//	return depth;

const Vec4V constOne(1.0f, 1.0f, 1.0f, 1.0f);
const Vec4V constMinusOne(-1.0f, -1.0f, -1.0f, -1.0f);
const Vec4V constCOLLISION_MAP_WIDTH_1(float(COLLISION_MAP_WIDTH-1),float(COLLISION_MAP_WIDTH-1),float(COLLISION_MAP_WIDTH-1),float(COLLISION_MAP_WIDTH-1));
const Vec4V constCOLLISION_MAP_HEIGHT_1(float(COLLISION_MAP_HEIGHT-1), float(COLLISION_MAP_HEIGHT-1), float(COLLISION_MAP_HEIGHT-1), float(COLLISION_MAP_HEIGHT-1));

	//Vector4 ptxPosition(x,y,z,1.0f);
	//gDepthViewProjMtx.Transform(ptxPosition, pos);
	Vec4V posX, posY, posZ;
	const Vec4V& depthViewProjMtxAX = depthViewProjMtxA.GetCol0ConstRef();
	const Vec4V& depthViewProjMtxAY = depthViewProjMtxA.GetCol1ConstRef();
	const Vec4V& depthViewProjMtxAZ = depthViewProjMtxA.GetCol2ConstRef();
	//const Vec4V& depthViewProjMtxAW = depthViewProjMtxA.GetCol3ConstRef();
	const Vec4V& depthViewProjMtxBX = depthViewProjMtxB.GetCol0ConstRef();
	const Vec4V& depthViewProjMtxBY = depthViewProjMtxB.GetCol1ConstRef();
	const Vec4V& depthViewProjMtxBZ = depthViewProjMtxB.GetCol2ConstRef();
	//const Vec4V& depthViewProjMtxBW = depthViewProjMtxB.GetCol3ConstRef();
	const Vec4V& depthViewProjMtxCX = depthViewProjMtxC.GetCol0ConstRef();
	const Vec4V& depthViewProjMtxCY = depthViewProjMtxC.GetCol1ConstRef();
	const Vec4V& depthViewProjMtxCZ = depthViewProjMtxC.GetCol2ConstRef();
	//const Vec4V& depthViewProjMtxCW = depthViewProjMtxC.GetCol3ConstRef();
	const Vec4V& depthViewProjMtxDX = depthViewProjMtxD.GetCol0ConstRef();
	const Vec4V& depthViewProjMtxDY = depthViewProjMtxD.GetCol1ConstRef();
	const Vec4V& depthViewProjMtxDZ = depthViewProjMtxD.GetCol2ConstRef();
	//const Vec4V& depthViewProjMtxDW = depthViewProjMtxD.GetCol3ConstRef();

	posX = ptxPositionX*depthViewProjMtxAX + ptxPositionY*depthViewProjMtxBX + ptxPositionZ*depthViewProjMtxCX + depthViewProjMtxDX;
	posY = ptxPositionX*depthViewProjMtxAY + ptxPositionY*depthViewProjMtxBY + ptxPositionZ*depthViewProjMtxCY + depthViewProjMtxDY;
	posZ = ptxPositionX*depthViewProjMtxAZ + ptxPositionY*depthViewProjMtxBZ + ptxPositionZ*depthViewProjMtxCZ + depthViewProjMtxDZ;

	posY = constOne - posY;

	//s32 ix = s32( Clamp(pos.x, 0.0f, 1.0f) * (COLLISION_MAP_WIDTH-1));
	//s32 iy = s32( Clamp(pos.y, 0.0f, 1.0f) * (COLLISION_MAP_HEIGHT-1));
	const Vec4V _xf = Vec4V(Vec::V4Saturate(posX.GetIntrin128())) * constCOLLISION_MAP_WIDTH_1;
	const Vec4V _yf = Vec4V(Vec::V4Saturate(posY.GetIntrin128())) * constCOLLISION_MAP_HEIGHT_1;
	const Vec4V ix  = Vec4V(Vec::V4FloatToIntRaw<0>(_xf.GetIntrin128()));
	const Vec4V iy  = Vec4V(Vec::V4FloatToIntRaw<0>(_yf.GetIntrin128()));

Vec4V& depthX = outX;
Vec4V& depthY = outY;
	const s32	ix0 = ix.GetXi();
	const s32	iy0 = iy.GetXi();
	const s32	ix1 = ix.GetYi();
	const s32	iy1 = iy.GetYi();
	const s32	ix2 = ix.GetZi();
	const s32	iy2 = iy.GetZi();
	const s32	ix3 = ix.GetWi();
	const s32	iy3 = iy.GetWi();

	//depth.x = depth.y = ParticleCollisionPtr[ iy*(COLLISION_MAP_WIDTH*2) + ix*2 ]; // 128x128 grctfD32FS8 / DXGI_FORMAT_R32G8X24_TYPELESS
	const float depth0 = ParticleCollisionPtr[ iy0*(COLLISION_MAP_WIDTH) + ix0 ]; // 128x128 grctfR32f
	const float depth1 = ParticleCollisionPtr[ iy1*(COLLISION_MAP_WIDTH) + ix1 ]; // 128x128 grctfR32f
	const float depth2 = ParticleCollisionPtr[ iy2*(COLLISION_MAP_WIDTH) + ix2 ]; // 128x128 grctfR32f
	const float depth3 = ParticleCollisionPtr[ iy3*(COLLISION_MAP_WIDTH) + ix3 ]; // 128x128 grctfR32f
	Vec::V4Set(depthX.GetIntrin128Ref(), depth0, depth1, depth2, depth3);
	depthY = depthX;

	//depth.x = (pos.z<depth.x) ? 1.0f : -1.0f;
	const Vec4V select0 = Vec4V(Vec::V4IsLessThanV(posZ.GetIntrin128(), depthX.GetIntrin128()));
	depthX = Vec4V(Vec::V4SelectFT(select0.GetIntrin128(), constMinusOne.GetIntrin128(), constOne.GetIntrin128()));
}
#else
static __forceinline void DepthCompareHeightMap(Vector2& out,	float /*x*/,float /*y*/,float /*z*/,
										 const Matrix44& /*gDepthViewProjMtx*/,
										 float* /*ParticleCollisionPtr*/)
{	
	out.x = 1.0f;
	out.y = 1.0f;
}

static __forceinline void DepthCompareHeightMap(Vec4V& outX, Vec4V& outY,
										 const Vec4V& /*x*/,const Vec4V& /*y*/, const Vec4V& /*z*/,
										 const Mat44V& /*gDepthViewProjMtx*/,
										 float* /*ParticleCollisionPtr*/)
{	
	Vec::V4Set(outX.GetIntrin128Ref(), 1.0f, 1.0f, 1.0f, 1.0f);
	Vec::V4Set(outY.GetIntrin128Ref(), 1.0f, 1.0f, 1.0f, 1.0f);
}
#endif


///////////////////////////////////////////////////////////////////////////////
// ptxgpuGetPositionalWindVariance
static __forceinline void ptxgpuGetPositionalWindVariance(Vector3& Out,	const Vector4& /*ptxPosition*/, mthRandom& noiseTexSampler,
																	const Vector4& gWindBaseVel, const Vector4& gWindVarianceVel)
{
	//float2 texLookup = ptxPosition.xy;
	//float3 randDir = tex2D(NoiseTexSampler, (texLookup.xy/16.0f)+texCoord.xy).xyz;
Vector3& randDir = Out;
	randDir.x = noiseTexSampler.GetFloat();
	randDir.y = noiseTexSampler.GetFloat();
	randDir.z = noiseTexSampler.GetFloat();
	
	//randDir = (randDir * 2.0f.xxx) - 1.0f.xxx;
	randDir.x = (randDir.x * 2.0f) - 1.0f;
	randDir.y = (randDir.y * 2.0f) - 1.0f;
	randDir.z = (randDir.z * 2.0f) - 1.0f;
	//randDir = normalize(randDir);
	randDir.Normalize();

	//randDir.xyz *= gWindBaseVel.www;
	randDir.x *= gWindBaseVel.w;
	randDir.y *= gWindBaseVel.w;
	randDir.z *= gWindBaseVel.w;

	//randDir.xyz *= gWindVarianceVel.www;
	randDir.x *= gWindVarianceVel.w;
	randDir.y *= gWindVarianceVel.w;
	randDir.z *= gWindVarianceVel.w;
}

static __forceinline void ptxgpuGetPositionalWindVariance(Vec4V& OutX,Vec4V& OutY,Vec4V& OutZ,
																	mthRandom& noiseTexSampler,
																	const Vec4V& gWindBaseVelW, const Vec4V& gWindVarianceVelW)
{
const Vec4V constTwo(2.0f,2.0f,2.0f,2.0f);
const Vec4V constOne(1.0f,1.0f,1.0f,1.0f);

	//float2 texLookup = ptxPosition.xy;
	//float3 randDir = tex2D(NoiseTexSampler, (texLookup.xy/16.0f)+texCoord.xy).xyz;
Vec4V& randDirX = OutX;
Vec4V& randDirY = OutY;
Vec4V& randDirZ = OutZ;
	randDirX = noiseTexSampler.Get4FloatsV();
	randDirY = noiseTexSampler.Get4FloatsV();
	randDirZ = noiseTexSampler.Get4FloatsV();
	
	//randDir = (randDir * 2.0f.xxx) - 1.0f.xxx;
	randDirX = (randDirX * constTwo) - constOne;
	randDirY = (randDirY * constTwo) - constOne;
	randDirZ = (randDirZ * constTwo) - constOne;

	//randDir = normalize(randDir);
	Vec4V _randDirSqr = randDirX*randDirX + randDirY*randDirY + randDirZ*randDirZ;
	Vec4V _invLenRandDir = Vec4V(Vec::V4InvSqrt(_randDirSqr.GetIntrin128()));
	randDirX *= _invLenRandDir;
	randDirY *= _invLenRandDir;
	randDirZ *= _invLenRandDir;

	//randDir.xyz *= gWindBaseVel.www;
	randDirX *= gWindBaseVelW;
	randDirY *= gWindBaseVelW;
	randDirZ *= gWindBaseVelW;

	//randDir.xyz *= gWindVarianceVel.www;
	randDirX *= gWindVarianceVelW;
	randDirY *= gWindVarianceVelW;
	randDirZ *= gWindVarianceVelW;
}

///////////////////////////////////////////////////////////////////////////////
// ptxgpuGetLocalWindVel
static __forceinline void ptxgpuGetLocalWindVel(Vector3& Out,	const Vector4& ptxPosition, mthRandom& noiseTexSampler,
						   								const Vector4& gGravityWindTimeStepCut,
														const Vector4& gWindBaseVel, const Vector4& gWindGustVel, const Vector4& gWindVarianceVel, const Vector4& gWindMultParams)
{
	//windVel.xyz = gWindBaseVel.xyz;
Vector3& windVel = Out;
	windVel.x = gWindBaseVel.x;
	windVel.y = gWindBaseVel.y;
	windVel.z = gWindBaseVel.z;

	// add on gusts
	//windVel.xyz += gWindGustVel.xyz;
	windVel.x += gWindGustVel.x;
	windVel.y += gWindGustVel.y;
	windVel.z += gWindGustVel.z;

	// add on global variance (on a sine wave)
	//float3 gustMult = float3(0.0f,0.0f,1.0f);
	//gustMult.xy = (ptxPosition.xy*gWindMultParams.xz) + (gGravityWindTimeStepCut.zz*gWindMultParams.yw);
	//gustMult.xy	= sin(gustMult.xy);
	//gustMult.xy = (gustMult.xy+1.0f.xx)*(0.5f.xx);
	Vector3 gustMult(0.0f,0.0f,1.0f);
	gustMult.x = (ptxPosition.x*gWindMultParams.x) + (gGravityWindTimeStepCut.z*gWindMultParams.y);
	gustMult.y = (ptxPosition.y*gWindMultParams.z) + (gGravityWindTimeStepCut.z*gWindMultParams.w);
	gustMult.x	= Sinf(gustMult.x);
	gustMult.y	= Sinf(gustMult.y);
	gustMult.x = (gustMult.x+1.0f)*0.5f;
	gustMult.y = (gustMult.y+1.0f)*0.5f;

	//windVel.xyz += gWindVarianceVel.xyz*gustMult.xyz;
	windVel.x += gWindVarianceVel.x*gustMult.x;
	windVel.y += gWindVarianceVel.y*gustMult.y;
	windVel.z += gWindVarianceVel.z*gustMult.z;

	// add on positional variance
	//windVel.xyz += ptxgpuGetPositionalWindVariance(ptxPosition, texCoord);
Vector3 randDir;
	ptxgpuGetPositionalWindVariance(randDir, ptxPosition, noiseTexSampler, gWindBaseVel, gWindVarianceVel);
	windVel.x += randDir.x;
	windVel.y += randDir.y;
	windVel.z += randDir.z;
}

static __forceinline void ptxgpuGetLocalWindVel(Vec4V& OutX, Vec4V& OutY, Vec4V& OutZ,
														const Vec4V& ptxPositionX,const Vec4V& ptxPositionY,const Vec4V& /*ptxPositionZ*/,
														mthRandom& noiseTexSampler,
						   								const Vec4V& gGravityWindTimeStepCutZ,
														const Vec4V& gWindBaseVelX,const Vec4V& gWindBaseVelY,const Vec4V& gWindBaseVelZ,const Vec4V& gWindBaseVelW,
														const Vec4V& gWindGustVelX,const Vec4V& gWindGustVelY,const Vec4V& gWindGustVelZ,const Vec4V& /*gWindGustVelW*/,
														const Vec4V& gWindVarianceVelX,const Vec4V& gWindVarianceVelY,const Vec4V& gWindVarianceVelZ,const Vec4V& gWindVarianceVelW,
														const Vec4V& gWindMultParamsX,const Vec4V& gWindMultParamsY,const Vec4V& gWindMultParamsZ,const Vec4V& gWindMultParamsW)
{
const Vec4V constOne(1.0f,1.0f,1.0f,1.0f);
const Vec4V constHalf(0.5f,0.5f,0.5f,0.5f);

	//windVel.xyz = gWindBaseVel.xyz;
Vec4V& windVelX = OutX;
Vec4V& windVelY = OutY;
Vec4V& windVelZ = OutZ;
	windVelX = gWindBaseVelX;
	windVelY = gWindBaseVelY;
	windVelZ = gWindBaseVelZ;

	// add on gusts
	//windVel.xyz += gWindGustVel.xyz;
	windVelX += gWindGustVelX;
	windVelY += gWindGustVelY;
	windVelZ += gWindGustVelZ;

	// add on global variance (on a sine wave)
	//float3 gustMult = float3(0.0f,0.0f,1.0f);
	//gustMult.xy = (ptxPosition.xy*gWindMultParams.xz) + (gGravityWindTimeStepCut.zz*gWindMultParams.yw);
	//gustMult.xy	= sin(gustMult.xy);
	//gustMult.xy = (gustMult.xy+1.0f.xx)*(0.5f.xx);
	Vec4V gustMultX,gustMultY;
	gustMultX = (ptxPositionX*gWindMultParamsX) + (gGravityWindTimeStepCutZ*gWindMultParamsY);
	gustMultY = (ptxPositionY*gWindMultParamsZ) + (gGravityWindTimeStepCutZ*gWindMultParamsW);
	gustMultX = Vec4V(Vec::V4Sin(gustMultX.GetIntrin128()));
	gustMultY = Vec4V(Vec::V4Sin(gustMultY.GetIntrin128()));
	gustMultX = (gustMultX+constOne)*constHalf;
	gustMultY = (gustMultY+constOne)*constHalf;

	//windVel.xyz += gWindVarianceVel.xyz*gustMult.xyz;
	windVelX += gWindVarianceVelX*gustMultX;
	windVelY += gWindVarianceVelY*gustMultY;
	windVelZ += gWindVarianceVelZ;

	// add on positional variance
	//windVel.xyz += ptxgpuGetPositionalWindVariance(ptxPosition, texCoord);
Vec4V randDirX,randDirY,randDirZ;
	ptxgpuGetPositionalWindVariance(randDirX,randDirY,randDirZ,	
										noiseTexSampler, gWindBaseVelW, gWindVarianceVelW);
	windVelX += randDirX;
	windVelY += randDirY;
	windVelZ += randDirZ;
}

///////////////////////////////////////////////////////////////////////////////
// ptxgpuApplyWindInfluence
static __forceinline void ptxgpuApplyWindInfluence(Vector3 &Out,	const Vector4& ptxPosition, const Vector4& ptxVelocity, mthRandom& noiseTexSampler,
															const Vector4& gGravityWindTimeStepCut,
															const Vector4& gWindBaseVel, const Vector4& gWindGustVel, const Vector4& gWindVarianceVel, const Vector4& gWindMultParams)
{
	Vector3 vAirVel;
	ptxgpuGetLocalWindVel(vAirVel,
						ptxPosition, noiseTexSampler, gGravityWindTimeStepCut, gWindBaseVel, gWindGustVel, gWindVarianceVel, gWindMultParams);

	// calculate the difference between the particle and air velocity
	//float3 vDeltaVel = vAirVel.xyz - ptxVelocity.xyz;
	Vector3& vDeltaVel = Out;
	vDeltaVel.x = vAirVel.x - ptxVelocity.x;
	vDeltaVel.y = vAirVel.y - ptxVelocity.y;
	vDeltaVel.z = vAirVel.z - ptxVelocity.z;

	// determine how quickly the particle velocity should match the wind velocity
	// if running at 30fps when a wind influence of 30 would match it instantly i.e. (delta mult = 1.0f)
	// a wind influence of 1 would mean it would take 1 second to match the wind velocity (delta mult = 1/30)
	//float3 vDeltaMult = gGravityWindTimeStepCut.yyy * gGravityWindTimeStepCut.zzz;
	//vDeltaMult = saturate(vDeltaMult);
	float vDeltaMult = gGravityWindTimeStepCut.y * gGravityWindTimeStepCut.z;
	vDeltaMult = Clamp(vDeltaMult, 0.0f, 1.0f);

	// apply the scale
	vDeltaVel.x *= vDeltaMult;
	vDeltaVel.y *= vDeltaMult;
	vDeltaVel.z *= vDeltaMult;
}


static __forceinline void ptxgpuApplyWindInfluence(Vec4V &OutX,Vec4V &OutY,Vec4V &OutZ,
															const Vec4V& ptxPositionX,const Vec4V& ptxPositionY,const Vec4V& ptxPositionZ,
															const Vec4V& ptxVelocityX,const Vec4V& ptxVelocityY,const Vec4V& ptxVelocityZ,
															mthRandom& noiseTexSampler,
															const Vec4V& /*gGravityWindTimeStepCutX*/,const Vec4V& gGravityWindTimeStepCutY,const Vec4V& gGravityWindTimeStepCutZ,const Vec4V& /*gGravityWindTimeStepCutW*/,
															const Vec4V& gWindBaseVelX,const Vec4V& gWindBaseVelY,const Vec4V& gWindBaseVelZ,const Vec4V& gWindBaseVelW,
															const Vec4V& gWindGustVelX,const Vec4V& gWindGustVelY,const Vec4V& gWindGustVelZ,const Vec4V& gWindGustVelW,
															const Vec4V& gWindVarianceVelX,const Vec4V& gWindVarianceVelY,const Vec4V& gWindVarianceVelZ,const Vec4V& gWindVarianceVelW,
															const Vec4V& gWindMultParamsX,const Vec4V& gWindMultParamsY,const Vec4V& gWindMultParamsZ,const Vec4V& gWindMultParamsW)
{
	Vec4V vAirVelX,vAirVelY,vAirVelZ;
	ptxgpuGetLocalWindVel(vAirVelX,vAirVelY,vAirVelZ,
								ptxPositionX,ptxPositionY,ptxPositionZ,
								noiseTexSampler,
								gGravityWindTimeStepCutZ,
								gWindBaseVelX,gWindBaseVelY,gWindBaseVelZ,gWindBaseVelW,
								gWindGustVelX,gWindGustVelY,gWindGustVelZ,gWindGustVelW,
								gWindVarianceVelX,gWindVarianceVelY,gWindVarianceVelZ,gWindVarianceVelW,
								gWindMultParamsX,gWindMultParamsY,gWindMultParamsZ,gWindMultParamsW);

	// calculate the difference between the particle and air velocity
	//float3 vDeltaVel = vAirVel.xyz - ptxVelocity.xyz;
	Vec4V& vDeltaVelX = OutX;
	Vec4V& vDeltaVelY = OutY;
	Vec4V& vDeltaVelZ = OutZ;
	vDeltaVelX = vAirVelX - ptxVelocityX;
	vDeltaVelY = vAirVelY - ptxVelocityY;
	vDeltaVelZ = vAirVelZ - ptxVelocityZ;

	// determine how quickly the particle velocity should match the wind velocity
	// if running at 30fps when a wind influence of 30 would match it instantly i.e. (delta mult = 1.0f)
	// a wind influence of 1 would mean it would take 1 second to match the wind velocity (delta mult = 1/30)
	//float3 vDeltaMult = gGravityWindTimeStepCut.yyy * gGravityWindTimeStepCut.zzz;
	//vDeltaMult = saturate(vDeltaMult);
	Vec4V vDeltaMult = gGravityWindTimeStepCutY * gGravityWindTimeStepCutZ;	// CONST!
	vDeltaMult = Vec4V(Vec::V4Saturate(vDeltaMult.GetIntrin128()));

	// apply the scale
	vDeltaVelX *= vDeltaMult;
	vDeltaVelY *= vDeltaMult;
	vDeltaVelZ *= vDeltaMult;
}

static __forceinline float _ceilf(float f)
{
	float i;
	float frac = Modf(f, &i);
	if(frac > 0.0f)
	{
		return i+1.0f;
	}

	return i;
}

static __forceinline void _V4Ceilf(Vec4V& v)
{
	const float cx = FPCeil(v.GetXf());
	const float cy = FPCeil(v.GetYf());
	const float cz = FPCeil(v.GetZf());
	const float cw = FPCeil(v.GetWf());
	Vec::V4Set(v.GetIntrin128Ref(), cx, cy, cz, cw);
}

// Pack the seed into the range 0-255 and the normalize the initial life into the range <epsilon>-1/maxLife
static __forceinline float PackVelW(float maxLife, float initLife, float seed)
{
	float initLifeNorm = initLife / maxLife;
	return _ceilf(255.0f * initLifeNorm) + seed;
}

static __forceinline void PackVelW(Vec4V &out,
							 const Vec4V& maxLife, const Vec4V& initLife, const Vec4V& seed)
{
	//float initLifeNorm = initLife / maxLife;
	//return _ceilf(255.0f * initLifeNorm) + seed;
	Vec4V initLifeNorm = initLife / maxLife;
	const Vec4V const255(255.0f, 255.0f, 255.0f, 255.0f);
	initLifeNorm *= const255;
	_V4Ceilf(initLifeNorm);
	out = initLifeNorm + seed;
}


// Unpack the seed and initial life values
static __forceinline void UnPackVelW(float packedFloat, float maxLife, float& initLife, float& seed)
{
	seed = packedFloat - Floorf(packedFloat);		//seed = frac(packedFloat);
	float initLifeNorm = (packedFloat - seed) / 255.0f;
	initLife = initLifeNorm * maxLife;
}

// Unpack the seed and initial life values
static __forceinline void UnPackVelW(const Vec4V& packedFloat, const Vec4V& maxLife, Vec4V& initLife, Vec4V& seed)
{
	//seed = frac(packedFloat);
	const float packedFloatX = packedFloat.GetXf();
	const float packedFloatY = packedFloat.GetYf();
	const float packedFloatZ = packedFloat.GetZf();
	const float packedFloatW = packedFloat.GetWf();
	const float seedX = packedFloatX - FPFloor(packedFloatX);
	const float seedY = packedFloatY - FPFloor(packedFloatY);
	const float seedZ = packedFloatZ - FPFloor(packedFloatZ);
	const float seedW = packedFloatW - FPFloor(packedFloatW);
	seed.SetX(seedX);
	seed.SetY(seedY);
	seed.SetZ(seedZ);
	seed.SetW(seedW);

	Vec4V initLifeNorm;
	const Vec4V const1_255(1.0f/255.0f, 1.0f/255.0f, 1.0f/255.0f, 1.0f/255.0f);
	initLifeNorm = (packedFloat - seed) * const1_255;
	initLife = initLifeNorm * maxLife;
}


//
///////////////////////////////////////////////////////////////////////////////
// GPUPTFX_UPDATE_DROP.FX - gpu drop particle (raindrop, snowdrop etc)
///////////////////////////////////////////////////////////////////////////////
//
// VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
// PixelShader  = compile PIXELSHADER PS_ptxgpuUpdateDrop()	PS4_TARGET(FMT_32_ABGR);
//
static void _RainUpdateCpuTaskDrop(ptxcpuUpdateBaseParams *params, u32 _width, u32 _height)
{
#define PTXGPU_USE_WIND						(1)

const u32 RAIN_UPDATE_WIDTH = _width;	//128;
const u32 RAIN_UPDATE_HEIGHT= _height;	//128;

///////////////////////////////////////////////////////////////////////////////
// SHADER VARIABLES 
///////////////////////////////////////////////////////////////////////////////
static dev_bool bUseOpt = true;

if(bUseOpt)	// opt
{
	const Vec4V		constZero	(0.0f, 0.0f, 0.0f, 0.0f);
	const Vec4V		constHalf	(0.5f, 0.5f, 0.5f, 0.5f);
	const Vec4V		const255	(255.0f, 255.0f, 255.0f, 255.0f);
	const Vec4V		const0_004	(0.004f, 0.004f, 0.004f, 0.004f);

	const Mat44V	_gDepthViewProjMtx		= RC_MAT44V(params->m_gDepthViewProjMtx);
	const Vec4V		_gDepthViewProjMtxAX	= Vec4V(Vec::V4SplatX(_gDepthViewProjMtx.GetCol0Intrin128()));
	const Vec4V		_gDepthViewProjMtxAY	= Vec4V(Vec::V4SplatY(_gDepthViewProjMtx.GetCol0Intrin128()));
	const Vec4V		_gDepthViewProjMtxAZ	= Vec4V(Vec::V4SplatZ(_gDepthViewProjMtx.GetCol0Intrin128()));
	const Vec4V		_gDepthViewProjMtxAW	= Vec4V(Vec::V4SplatW(_gDepthViewProjMtx.GetCol0Intrin128()));
	const Mat44V	gDepthViewProjMtxA(_gDepthViewProjMtxAX, _gDepthViewProjMtxAY, _gDepthViewProjMtxAZ, _gDepthViewProjMtxAW);

	const Vec4V		_gDepthViewProjMtxBX	= Vec4V(Vec::V4SplatX(_gDepthViewProjMtx.GetCol1Intrin128()));
	const Vec4V		_gDepthViewProjMtxBY	= Vec4V(Vec::V4SplatY(_gDepthViewProjMtx.GetCol1Intrin128()));
	const Vec4V		_gDepthViewProjMtxBZ	= Vec4V(Vec::V4SplatZ(_gDepthViewProjMtx.GetCol1Intrin128()));
	const Vec4V		_gDepthViewProjMtxBW	= Vec4V(Vec::V4SplatW(_gDepthViewProjMtx.GetCol1Intrin128()));
	const Mat44V	gDepthViewProjMtxB(_gDepthViewProjMtxBX, _gDepthViewProjMtxBY, _gDepthViewProjMtxBZ, _gDepthViewProjMtxBW);

	const Vec4V		_gDepthViewProjMtxCX	= Vec4V(Vec::V4SplatX(_gDepthViewProjMtx.GetCol2Intrin128()));
	const Vec4V		_gDepthViewProjMtxCY	= Vec4V(Vec::V4SplatY(_gDepthViewProjMtx.GetCol2Intrin128()));
	const Vec4V		_gDepthViewProjMtxCZ	= Vec4V(Vec::V4SplatZ(_gDepthViewProjMtx.GetCol2Intrin128()));
	const Vec4V		_gDepthViewProjMtxCW	= Vec4V(Vec::V4SplatW(_gDepthViewProjMtx.GetCol2Intrin128()));
	const Mat44V	gDepthViewProjMtxC(_gDepthViewProjMtxCX, _gDepthViewProjMtxCY, _gDepthViewProjMtxCZ, _gDepthViewProjMtxCW);

	const Vec4V		_gDepthViewProjMtxDX	= Vec4V(Vec::V4SplatX(_gDepthViewProjMtx.GetCol3Intrin128()));
	const Vec4V		_gDepthViewProjMtxDY	= Vec4V(Vec::V4SplatY(_gDepthViewProjMtx.GetCol3Intrin128()));
	const Vec4V		_gDepthViewProjMtxDZ	= Vec4V(Vec::V4SplatZ(_gDepthViewProjMtx.GetCol3Intrin128()));
	const Vec4V		_gDepthViewProjMtxDW	= Vec4V(Vec::V4SplatW(_gDepthViewProjMtx.GetCol3Intrin128()));
	const Mat44V	gDepthViewProjMtxD(_gDepthViewProjMtxDX, _gDepthViewProjMtxDY, _gDepthViewProjMtxDZ, _gDepthViewProjMtxDW);

	const Vector3	_gBoxForward			= params->m_gBoxForward;
	const Vec4V		gBoxForwardX(_gBoxForward.x, _gBoxForward.x, _gBoxForward.x, _gBoxForward.x);
	const Vec4V		gBoxForwardY(_gBoxForward.y, _gBoxForward.y, _gBoxForward.y, _gBoxForward.y);
	const Vec4V		gBoxForwardZ(_gBoxForward.z, _gBoxForward.z, _gBoxForward.z, _gBoxForward.z);

	// Use a constant up direction
	//float3 boxUp = normalize(cross(gBoxRight, gBoxForward));
	const Vector3	_gBoxRight				= params->m_gBoxRight;
	const Vec4V		gBoxRightX(_gBoxRight.x,_gBoxRight.x,_gBoxRight.x,_gBoxRight.x);
	const Vec4V		gBoxRightY(_gBoxRight.y,_gBoxRight.y,_gBoxRight.y,_gBoxRight.y);
	const Vec4V		gBoxRightZ(_gBoxRight.z,_gBoxRight.z,_gBoxRight.z,_gBoxRight.z);

	const Vector3	_gBoxCentrePos			= params->m_gBoxCentrePos;
	const Vec4V		gBoxCentrePosX(_gBoxCentrePos.x,_gBoxCentrePos.x,_gBoxCentrePos.x,_gBoxCentrePos.x);
	const Vec4V		gBoxCentrePosY(_gBoxCentrePos.y,_gBoxCentrePos.y,_gBoxCentrePos.y,_gBoxCentrePos.y);
	const Vec4V		gBoxCentrePosZ(_gBoxCentrePos.z,_gBoxCentrePos.z,_gBoxCentrePos.z,_gBoxCentrePos.z);

	Vector3 _boxUp;
	_boxUp.Cross(_gBoxRight, _gBoxForward);
	_boxUp.Normalize();
	const Vec4V		boxUpX(_boxUp.x, _boxUp.x, _boxUp.x, _boxUp.x);
	const Vec4V		boxUpY(_boxUp.y, _boxUp.y, _boxUp.y, _boxUp.y);
	const Vec4V		boxUpZ(_boxUp.z, _boxUp.z, _boxUp.z, _boxUp.z);

	// Transforming particles from local to worls space
	const Vec4V		_gRefParticlePos		= RC_VEC4V(params->m_gRefParticlePos);
	const Vec4V		gRefParticlePosX		= Vec4V(Vec::V4SplatX(_gRefParticlePos.GetIntrin128()));
	const Vec4V		gRefParticlePosY		= Vec4V(Vec::V4SplatY(_gRefParticlePos.GetIntrin128()));
	const Vec4V		gRefParticlePosZ		= Vec4V(Vec::V4SplatZ(_gRefParticlePos.GetIntrin128()));

	const Vec4V		_gOffsetParticlePos		= RC_VEC4V(params->m_gOffsetParticlePos);
	const Vec4V		gOffsetParticlePosX		= Vec4V(Vec::V4SplatX(_gOffsetParticlePos.GetIntrin128()));
	const Vec4V		gOffsetParticlePosY		= Vec4V(Vec::V4SplatY(_gOffsetParticlePos.GetIntrin128()));
	const Vec4V		gOffsetParticlePosZ		= Vec4V(Vec::V4SplatZ(_gOffsetParticlePos.GetIntrin128()));

	const Vec4V		_gGravityWindTimeStepCut= RC_VEC4V(params->m_gGravityWindTimeStepCut);	// x: Gravity, y: Wind Influence, z: TimeStep, w: 0 after camera cut, 1 otherwise
	const Vec4V		gGravityWindTimeStepCutX= Vec4V(Vec::V4SplatX(_gGravityWindTimeStepCut.GetIntrin128()));
	const Vec4V		gGravityWindTimeStepCutY= Vec4V(Vec::V4SplatY(_gGravityWindTimeStepCut.GetIntrin128()));
	const Vec4V		gGravityWindTimeStepCutZ= Vec4V(Vec::V4SplatZ(_gGravityWindTimeStepCut.GetIntrin128()));
	const Vec4V		gGravityWindTimeStepCutW= Vec4V(Vec::V4SplatW(_gGravityWindTimeStepCut.GetIntrin128()));

	const float		_gResetParticles		= params->m_gResetParticles;
	const Vec4V		gResetParticles(_gResetParticles,_gResetParticles,_gResetParticles,_gResetParticles);

	const Vec4V		gResetParticlesGreaterThanZeroSel = Vec4V(Vec::V4IsGreaterThanV(gResetParticles.GetIntrin128(),constZero.GetIntrin128()));

	// emitter settings
	const Vector3	_gBoxSize				= params->m_gBoxSize;
	const Vec4V		gBoxSizeX(_gBoxSize.x, _gBoxSize.x, _gBoxSize.x, _gBoxSize.x);
	const Vec4V		gBoxSizeY(_gBoxSize.y, _gBoxSize.y, _gBoxSize.y, _gBoxSize.y);
	const Vec4V		gBoxSizeZ(_gBoxSize.z, _gBoxSize.z, _gBoxSize.z, _gBoxSize.z);
	const Vec4V		gInvBoxSizeX(1.0f/_gBoxSize.x, 1.0f/_gBoxSize.x, 1.0f/_gBoxSize.x, 1.0f/_gBoxSize.x);
	const Vec4V		gInvBoxSizeY(1.0f/_gBoxSize.y, 1.0f/_gBoxSize.y, 1.0f/_gBoxSize.y, 1.0f/_gBoxSize.y);
	const Vec4V		gInvBoxSizeZ(1.0f/_gBoxSize.z, 1.0f/_gBoxSize.z, 1.0f/_gBoxSize.z, 1.0f/_gBoxSize.z);

	const Vec4V		_gLifeMinMaxClampToGround= RC_VEC4V(params->m_gLifeMinMaxClampToGround);
	const Vec4V		gLifeMinMaxClampToGroundX= Vec4V(Vec::V4SplatX(_gLifeMinMaxClampToGround.GetIntrin128()));
	const Vec4V		gLifeMinMaxClampToGroundY= Vec4V(Vec::V4SplatY(_gLifeMinMaxClampToGround.GetIntrin128()));
	const Vec4V		gLifeMinMaxClampToGroundZ= Vec4V(Vec::V4SplatZ(_gLifeMinMaxClampToGround.GetIntrin128()));

	const Vec3V		_gVelocityMin			= RC_VEC3V(params->m_gVelocityMin);
	const Vec4V		gVelocityMinX			= Vec4V(Vec::V4SplatX(_gVelocityMin.GetIntrin128()));
	const Vec4V		gVelocityMinY			= Vec4V(Vec::V4SplatY(_gVelocityMin.GetIntrin128()));
	const Vec4V		gVelocityMinZ			= Vec4V(Vec::V4SplatZ(_gVelocityMin.GetIntrin128()));

	const Vec3V		_gVelocityMax			= RC_VEC3V(params->m_gVelocityMax);
	const Vec4V		gVelocityMaxX			= Vec4V(Vec::V4SplatX(_gVelocityMax.GetIntrin128()));
	const Vec4V		gVelocityMaxY			= Vec4V(Vec::V4SplatY(_gVelocityMax.GetIntrin128()));
	const Vec4V		gVelocityMaxZ			= Vec4V(Vec::V4SplatZ(_gVelocityMax.GetIntrin128()));

	// wind related
	const Vec4V		_gWindBaseVel			= params->m_gWindBaseVel;		// xyz: WindBaseVel, w: GameWindSpeed
	const Vec4V		gWindBaseVelX			= Vec4V(Vec::V4SplatX(_gWindBaseVel.GetIntrin128()));
	const Vec4V		gWindBaseVelY			= Vec4V(Vec::V4SplatY(_gWindBaseVel.GetIntrin128()));
	const Vec4V		gWindBaseVelZ			= Vec4V(Vec::V4SplatZ(_gWindBaseVel.GetIntrin128()));
	const Vec4V		gWindBaseVelW			= Vec4V(Vec::V4SplatW(_gWindBaseVel.GetIntrin128()));

	const Vec4V		_gWindGustVel			= params->m_gWindGustVel;		// xyz: WindGustVel, w: unused
	const Vec4V		gWindGustVelX			= Vec4V(Vec::V4SplatX(_gWindGustVel.GetIntrin128()));
	const Vec4V		gWindGustVelY			= Vec4V(Vec::V4SplatY(_gWindGustVel.GetIntrin128()));
	const Vec4V		gWindGustVelZ			= Vec4V(Vec::V4SplatZ(_gWindGustVel.GetIntrin128()));
	const Vec4V		gWindGustVelW			= Vec4V(Vec::V4SplatW(_gWindGustVel.GetIntrin128()));

	const Vec4V		_gWindVarianceVel		= params->m_gWindVarianceVel;	// xyz: WindVarianceVel, w: GameWindPositionalVarMult
	const Vec4V		gWindVarianceVelX		= Vec4V(Vec::V4SplatX(_gWindVarianceVel.GetIntrin128()));
	const Vec4V		gWindVarianceVelY		= Vec4V(Vec::V4SplatY(_gWindVarianceVel.GetIntrin128()));
	const Vec4V		gWindVarianceVelZ		= Vec4V(Vec::V4SplatZ(_gWindVarianceVel.GetIntrin128()));
	const Vec4V		gWindVarianceVelW		= Vec4V(Vec::V4SplatW(_gWindVarianceVel.GetIntrin128()));

	const Vec4V		_gWindMultParams		= params->m_gWindMultParams;	// x: GameWindGlobalVarSinePosMult, y: GameWindGlobalVarSineTimeMult
																			// z: GameWindGlobalVarCosinePosMult, w: GameWindGlobalVarCosineTimeMult
	const Vec4V		gWindMultParamsX		= Vec4V(Vec::V4SplatX(_gWindMultParams.GetIntrin128()));
	const Vec4V		gWindMultParamsY		= Vec4V(Vec::V4SplatY(_gWindMultParams.GetIntrin128()));
	const Vec4V		gWindMultParamsZ		= Vec4V(Vec::V4SplatZ(_gWindMultParams.GetIntrin128()));
	const Vec4V		gWindMultParamsW		= Vec4V(Vec::V4SplatW(_gWindMultParams.GetIntrin128()));

///////////////////////////////////////////////////////////////////////////////
// SAMPLERS
///////////////////////////////////////////////////////////////////////////////
	const Vec::Vector_4V*	gpParticlePosSrc = (Vec::Vector_4V*)params->m_ParticlePosLock[0].Base;
	Assert(gpParticlePosSrc);
	const Vec::Vector_4V*	gpParticleVelSrc = (Vec::Vector_4V*)params->m_ParticleVelLock[0].Base;
	Assert(gpParticleVelSrc);

	Vec::Vector_4V*			gpParticlePosDst = (Vec::Vector_4V*)params->m_ParticlePosLock[1].Base;
	Assert(gpParticlePosDst);
	Vec::Vector_4V*			gpParticleVelDst = (Vec::Vector_4V*)params->m_ParticleVelLock[1].Base;
	Assert(gpParticleVelDst);

#if RAIN_UPDATE_CPU_HEIGHTMAP
	float*			gpParticleCollisionSrc = (float*)params->m_HeightMapTextureLock.Base;
	Assert(gpParticleCollisionSrc);
#else
	float*			gpParticleCollisionSrc = NULL;
#endif

mthRandom	NoiseTexSampler;
	NoiseTexSampler.SetFullSeed(sysTimer::GetTicks());


Vec::Vector_4V*	pParticlePosSrc = (Vec::Vector_4V*)gpParticlePosSrc;
Vec::Vector_4V*	pParticleVelSrc = (Vec::Vector_4V*)gpParticleVelSrc;
Vec::Vector_4V*	pParticlePosDst = (Vec::Vector_4V*)gpParticlePosDst;
Vec::Vector_4V*	pParticleVelDst = (Vec::Vector_4V*)gpParticleVelDst;


	const s32 _count = RAIN_UPDATE_HEIGHT*RAIN_UPDATE_WIDTH;
	for(s32 xy=0; xy<_count; xy+=4)
	{
			///////////////////////////////////////////////////////////////////////////////
			// PS_ptxgpuUpdateDrop

			// get the particle position and velocity 
			Vec4V ptxPosition0,ptxPosition1,ptxPosition2,ptxPosition3;
			Vec4V ptxVelocity0,ptxVelocity1,ptxVelocity2,ptxVelocity3;
			ptxPosition0.SetIntrin128( *(pParticlePosSrc+0) );
			ptxVelocity0.SetIntrin128( *(pParticleVelSrc+0) );
			ptxPosition1.SetIntrin128( *(pParticlePosSrc+1) );
			ptxVelocity1.SetIntrin128( *(pParticleVelSrc+1) );
			ptxPosition2.SetIntrin128( *(pParticlePosSrc+2) );
			ptxVelocity2.SetIntrin128( *(pParticleVelSrc+2) );
			ptxPosition3.SetIntrin128( *(pParticlePosSrc+3) );
			ptxVelocity3.SetIntrin128( *(pParticleVelSrc+3) );
			pParticlePosSrc += 4;
			pParticleVelSrc += 4;

			Vec4V ptxPositionX, ptxPositionY, ptxPositionZ, ptxPositionW;
			Vec4V ptxVelocityX, ptxVelocityY, ptxVelocityZ, ptxVelocityW;
			Vec4V t1, t2;
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxPosition0.GetIntrin128(), ptxPosition1.GetIntrin128()));		// X0X1Y0Y1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxPosition2.GetIntrin128(), ptxPosition3.GetIntrin128()));		// X2X3Y2Y3
			ptxPositionX = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X0X1X2X3
			ptxPositionY = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// Y0Y1Y2Y3
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxPosition0.GetIntrin128(), ptxPosition1.GetIntrin128()));		// Z0Z1W0W1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxPosition2.GetIntrin128(), ptxPosition3.GetIntrin128()));		// Z2Z3W2W3
			ptxPositionZ = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// Z0Z1Z2Z2
			ptxPositionW = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// W0W1W2W3

			t1 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxVelocity0.GetIntrin128(), ptxVelocity1.GetIntrin128()));		// X0X1Y0Y1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxVelocity2.GetIntrin128(), ptxVelocity3.GetIntrin128()));		// X2X3Y2Y3
			ptxVelocityX = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X0X1X2X3
			ptxVelocityY = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// Y0Y1Y2Y3
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxVelocity0.GetIntrin128(), ptxVelocity1.GetIntrin128()));		// Z0Z1W0W1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxVelocity2.GetIntrin128(), ptxVelocity3.GetIntrin128()));		// Z2Z3W2W3
			ptxVelocityZ = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// Z0Z1Z2Z2
			ptxVelocityW = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// W0W1W2W3

			// Transform the position to world space
			//ptxPosition.xyz += gRefParticlePos + gOffsetParticlePos;
			ptxPositionX = ptxPositionX + gRefParticlePosX + gOffsetParticlePosX;
			ptxPositionY = ptxPositionY + gRefParticlePosY + gOffsetParticlePosY;
			ptxPositionZ = ptxPositionZ + gRefParticlePosZ + gOffsetParticlePosZ;


			// Position W contains the current particle life value
			Vec4V curLife = Vec4V(Vec::V4Abs(ptxPositionW.GetIntrin128()));

			// unpack the current and max life
			Vec4V initLife;
			Vec4V seed;
			UnPackVelW(ptxVelocityW, gLifeMinMaxClampToGroundY, initLife, seed);

			// update the particle life
			curLife = curLife - gGravityWindTimeStepCutZ;


			// check if the particle is dead
			//if (curLife<=0.0f || gResetParticles>0.0f)
			const Vec4V newSelect0 = Vec4V(Vec::V4IsLessThanOrEqualV(curLife.GetIntrin128(),	constZero.GetIntrin128()));
			const Vec4V newSelect  = Vec4V(Vec::V4Or(newSelect0.GetIntrin128(),					gResetParticlesGreaterThanZeroSel.GetIntrin128()));
			{
				// the particle is dead - we need to emit a new one
				Vec4V newPtxPositionX, newPtxPositionY, newPtxPositionZ;
				Vec4V newPtxVelocityX, newPtxVelocityY, newPtxVelocityZ;
				Vec4V newCurLife, newInitLife, newSeed;

				// get some random data
				Vec4V fRandom1X, fRandom1Y, fRandom1Z, fRandom1W;
				Vec4V fRandom2X, fRandom2Y, fRandom2Z;
				fRandom1X = NoiseTexSampler.Get4FloatsV();
				fRandom2X = NoiseTexSampler.Get4FloatsV();
				fRandom1Y = NoiseTexSampler.Get4FloatsV();
				fRandom2Y = NoiseTexSampler.Get4FloatsV();
				fRandom1Z = NoiseTexSampler.Get4FloatsV();
				fRandom2Z = NoiseTexSampler.Get4FloatsV();
				fRandom1W = NoiseTexSampler.Get4FloatsV();

				// position
				//float3 emitRange = fRandom1.wzy-0.5f;
				Vec4V emitRangeX, emitRangeY, emitRangeZ;
				emitRangeX = fRandom1W - constHalf;
				emitRangeY = fRandom1Z - constHalf;
				emitRangeZ = fRandom1Y - constHalf;
		
				//ptxPosition.xyz = gBoxCentrePos;
				//ptxPosition.xyz += gBoxRight * gBoxSize.x * emitRange.x;
				//ptxPosition.xyz += gBoxForward * gBoxSize.y * emitRange.y;
				//ptxPosition.xyz += boxUp * gBoxSize.z * emitRange.z;
				newPtxPositionX = gBoxCentrePosX;
				newPtxPositionY = gBoxCentrePosY;
				newPtxPositionZ = gBoxCentrePosZ;
				newPtxPositionX += gBoxRightX * gBoxSizeX * emitRangeX;
				newPtxPositionY += gBoxRightY * gBoxSizeX * emitRangeX;
				newPtxPositionZ += gBoxRightZ * gBoxSizeX * emitRangeX;
				newPtxPositionX += gBoxForwardX * gBoxSizeY * emitRangeY;
				newPtxPositionY += gBoxForwardY * gBoxSizeY * emitRangeY;
				newPtxPositionZ += gBoxForwardZ * gBoxSizeY * emitRangeY;
				newPtxPositionX += boxUpX * gBoxSizeZ * emitRangeZ;
				newPtxPositionY += boxUpY * gBoxSizeZ * emitRangeZ;
				newPtxPositionZ += boxUpZ * gBoxSizeZ * emitRangeZ;

				// velocity 
				//ptxVelocity.xyz = gVelocityMin + (gVelocityMax.xyz-gVelocityMin)*fRandom2.xyz;
				newPtxVelocityX = gVelocityMinX + (gVelocityMaxX-gVelocityMinX)*fRandom2X;
				newPtxVelocityY = gVelocityMinY + (gVelocityMaxY-gVelocityMinY)*fRandom2Y;
				newPtxVelocityZ = gVelocityMinZ + (gVelocityMaxZ-gVelocityMinZ)*fRandom2Z;

				// life
				newCurLife	= gLifeMinMaxClampToGroundX + (gLifeMinMaxClampToGroundY-gLifeMinMaxClampToGroundX)*fRandom1W;
				newInitLife	= newCurLife;

				// collision state and texture id
				// seed = saturate(fRandom1.x - 0.004); // Seed can't be one
				newSeed = Vec4V(Vec::V4Saturate((fRandom1X-const0_004).GetIntrin128ConstRef()));

				ptxPositionX = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxPositionX.GetIntrin128(), newPtxPositionX.GetIntrin128()));
				ptxPositionY = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxPositionY.GetIntrin128(), newPtxPositionY.GetIntrin128()));
				ptxPositionZ = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxPositionZ.GetIntrin128(), newPtxPositionZ.GetIntrin128()));

				ptxVelocityX = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxVelocityX.GetIntrin128(), newPtxVelocityX.GetIntrin128()));
				ptxVelocityY = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxVelocityY.GetIntrin128(), newPtxVelocityY.GetIntrin128()));
				ptxVelocityZ = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxVelocityZ.GetIntrin128(), newPtxVelocityZ.GetIntrin128()));

				curLife		 = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), curLife.GetIntrin128(),		newCurLife.GetIntrin128()));
				initLife	 = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), initLife.GetIntrin128(),		newInitLife.GetIntrin128()));
				seed		 = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), seed.GetIntrin128(),			newSeed.GetIntrin128()));
			}

			// update the particle position
			//ptxPosition.xyz += ptxVelocity.xyz * gGravityWindTimeStepCut.zzz;
			ptxPositionX += ptxVelocityX * gGravityWindTimeStepCutZ;
			ptxPositionY += ptxVelocityY * gGravityWindTimeStepCutZ;
			ptxPositionZ += ptxVelocityZ * gGravityWindTimeStepCutZ;

			// keep the particle inside the box
			//float3 boxToPtx = ptxPosition.xyz - gBoxCentrePos;
			Vec4V boxToPtxX, boxToPtxY, boxToPtxZ;
			boxToPtxX = ptxPositionX - gBoxCentrePosX;
			boxToPtxY = ptxPositionY - gBoxCentrePosY;
			boxToPtxZ = ptxPositionZ - gBoxCentrePosZ;


			//float dotRight = dot(gBoxRight, boxToPtx);
			const Vec4V dotRight = gBoxRightX*boxToPtxX + gBoxRightY*boxToPtxY + gBoxRightZ*boxToPtxZ;
			//float dotForward = dot(gBoxForward, boxToPtx);
			const Vec4V dotForward = gBoxForwardX*boxToPtxX + gBoxForwardY*boxToPtxY + gBoxForwardZ*boxToPtxZ;
			//float dotUp = dot(boxUp, boxToPtx);
			const Vec4V dotUp = boxUpX*boxToPtxX + boxUpY*boxToPtxY + boxUpZ*boxToPtxZ;
	

			// TODO: This code causes pops as the particle can changes position while fully opaque
 			//ptxPosition.xyz -= gBoxRight * gBoxSize.x * ceil((dotRight/gBoxSize.x)-0.5f);
 			//ptxPosition.xyz -= gBoxForward * gBoxSize.y * ceil((dotForward/gBoxSize.y)-0.5f);
 			//ptxPosition.xyz -= boxUp * gBoxSize.z * ceil((dotUp/gBoxSize.z)-0.5f);
			Vec4V ceilDotRightBoxSizeX = (dotRight*gInvBoxSizeX) - constHalf;
			_V4Ceilf(ceilDotRightBoxSizeX);
 			ptxPositionX -= gBoxRightX * gBoxSizeX * ceilDotRightBoxSizeX;
 			ptxPositionY -= gBoxRightY * gBoxSizeX * ceilDotRightBoxSizeX;
 			ptxPositionZ -= gBoxRightZ * gBoxSizeX * ceilDotRightBoxSizeX;

			Vec4V ceilDotForwardBoxSizeY = (dotForward*gInvBoxSizeY) - constHalf;
			_V4Ceilf(ceilDotForwardBoxSizeY);
			ptxPositionX -= gBoxForwardX * gBoxSizeY * ceilDotForwardBoxSizeY;
			ptxPositionY -= gBoxForwardY * gBoxSizeY * ceilDotForwardBoxSizeY;
			ptxPositionZ -= gBoxForwardZ * gBoxSizeY * ceilDotForwardBoxSizeY;

			Vec4V ceilDotUpBoxSizeZ = (dotUp*gInvBoxSizeZ) - constHalf;
			_V4Ceilf(ceilDotUpBoxSizeZ);
			ptxPositionX -= boxUpX * gBoxSizeZ * ceilDotUpBoxSizeZ;
			ptxPositionY -= boxUpY * gBoxSizeZ * ceilDotUpBoxSizeZ;
			ptxPositionZ -= boxUpZ * gBoxSizeZ * ceilDotUpBoxSizeZ;

			// get the depth info for the new particle position (after keeping the particle within the box)
			Vec4V depthInfo2X, depthInfo2Y;
			DepthCompareHeightMap(depthInfo2X, depthInfo2Y,
									ptxPositionX,ptxPositionY,ptxPositionZ,
									gDepthViewProjMtxA,gDepthViewProjMtxB,gDepthViewProjMtxC,gDepthViewProjMtxD,
									gpParticleCollisionSrc);


			// update the particle position (v += g * dt)
			//ptxVelocity.xyz += float3(0.0f, 0.0f, gGravityWindTimeStepCut.x) * gGravityWindTimeStepCut.zzz;
			ptxVelocityZ += gGravityWindTimeStepCutX * gGravityWindTimeStepCutZ;

		#if PTXGPU_USE_WIND
			// update the particle's velocity with wind influence
			//ptxVelocity.xyz += ptxgpuApplyWindInfluence(ptxPosition.xyz, ptxVelocity.xyz, Input.texCoord0.xy);
			Vec4V windInfluenceX, windInfluenceY, windInfluenceZ;
			ptxgpuApplyWindInfluence(windInfluenceX,windInfluenceY, windInfluenceZ,
														ptxPositionX,ptxPositionY,ptxPositionZ,
														ptxVelocityX,ptxVelocityY,ptxVelocityZ,
														NoiseTexSampler,
														gGravityWindTimeStepCutX,gGravityWindTimeStepCutY,gGravityWindTimeStepCutZ,gGravityWindTimeStepCutW,
														gWindBaseVelX,gWindBaseVelY,gWindBaseVelZ,gWindBaseVelW,
														gWindGustVelX,gWindGustVelY,gWindGustVelZ,gWindGustVelW,
														gWindVarianceVelX,gWindVarianceVelY,gWindVarianceVelZ,gWindVarianceVelW,
														gWindMultParamsX,gWindMultParamsY,gWindMultParamsZ,gWindMultParamsW);
			ptxVelocityX += windInfluenceX;
			ptxVelocityY += windInfluenceY;
			ptxVelocityZ += windInfluenceZ;
		#endif


			// Transform particle position back to local space
			//ptxPosition.xyz -= gRefParticlePos;
			ptxPositionX -= gRefParticlePosX;
			ptxPositionY -= gRefParticlePosY;
			ptxPositionZ -= gRefParticlePosZ;
	
			// Store the current particle life value along with the collision info in position W
			// We saturate curLife it died and became negative
			ptxPositionW = curLife * depthInfo2X;
	
			// pack the current and max life
			PackVelW(ptxVelocityW,		gLifeMinMaxClampToGroundY, initLife, seed);

			// pack the position and velocity
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxPositionX.GetIntrin128(), ptxPositionY.GetIntrin128()));		// X0Y0X1Y1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxPositionZ.GetIntrin128(), ptxPositionW.GetIntrin128()));		// Z0W0Z1W1
			ptxPosition0 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X0Y0Z0W0
			ptxPosition1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X1Y1Z1W1
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxPositionX.GetIntrin128(), ptxPositionY.GetIntrin128()));		// X2Y2X3Y3
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxPositionZ.GetIntrin128(), ptxPositionW.GetIntrin128()));		// Z2W2Z3W3
			ptxPosition2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X2Y2Z2W2
			ptxPosition3 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X3Y3Z3W3

			t1 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxVelocityX.GetIntrin128(), ptxVelocityY.GetIntrin128()));		// X0Y0X1Y1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxVelocityZ.GetIntrin128(), ptxVelocityW.GetIntrin128()));		// Z0W0Z1W1
			ptxVelocity0 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X0Y0Z0W0
			ptxVelocity1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X1Y1Z1W1
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxVelocityX.GetIntrin128(), ptxVelocityY.GetIntrin128()));		// X2Y2X3Y3
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxVelocityZ.GetIntrin128(), ptxVelocityW.GetIntrin128()));		// Z2W2Z3W3
			ptxVelocity2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X2Y2Z2W2
			ptxVelocity3 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X3Y3Z3W3

			*(pParticlePosDst+0) = ptxPosition0.GetIntrin128();
			*(pParticleVelDst+0) = ptxVelocity0.GetIntrin128();
			*(pParticlePosDst+1) = ptxPosition1.GetIntrin128();
			*(pParticleVelDst+1) = ptxVelocity1.GetIntrin128();
			*(pParticlePosDst+2) = ptxPosition2.GetIntrin128();
			*(pParticleVelDst+2) = ptxVelocity2.GetIntrin128();
			*(pParticlePosDst+3) = ptxPosition3.GetIntrin128();
			*(pParticleVelDst+3) = ptxVelocity3.GetIntrin128();
			pParticlePosDst += 4;
			pParticleVelDst += 4;
	}//for(s32 y=0; y<RAIN_UPDATE_HEIGHT; y++)...
}
else // opt
{
	const Matrix44	gDepthViewProjMtx		= params->m_gDepthViewProjMtx;
	const Vector3	gBoxForward				= params->m_gBoxForward;
	const Vector3	gBoxRight				= params->m_gBoxRight;
	const Vector3	gBoxCentrePos			= params->m_gBoxCentrePos;

	// Use a constant up direction
	//float3 boxUp = normalize(cross(gBoxRight, gBoxForward));
	Vector3 boxUp;
	boxUp.Cross(gBoxRight, gBoxForward);
	boxUp.Normalize();

	// Transforming particles from local to worls space
	const Vector3	gRefParticlePos			= params->m_gRefParticlePos;
	const Vector3	gOffsetParticlePos		= params->m_gOffsetParticlePos;

	const Vector4	gGravityWindTimeStepCut	= params->m_gGravityWindTimeStepCut;	// x: Gravity, y: Wind Influence, z: TimeStep, w: 0 after camera cut, 1 otherwise

	const float		gResetParticles		= params->m_gResetParticles;

	// emitter settings
	const Vector3	gBoxSize				= params->m_gBoxSize;
	const Vector3	gLifeMinMaxClampToGround= params->m_gLifeMinMaxClampToGround;
	const Vector3	gVelocityMin			= params->m_gVelocityMin;
	const Vector3	gVelocityMax			= params->m_gVelocityMax;

	// wind related
	const Vector4	gWindBaseVel			= RC_VECTOR4(params->m_gWindBaseVel);		// xyz: WindBaseVel, w: GameWindSpeed
	const Vector4	gWindGustVel			= RC_VECTOR4(params->m_gWindGustVel);		// xyz: WindGustVel, w: unused
	const Vector4	gWindVarianceVel		= RC_VECTOR4(params->m_gWindVarianceVel);	// xyz: WindVarianceVel, w: GameWindPositionalVarMult
	const Vector4	gWindMultParams			= RC_VECTOR4(params->m_gWindMultParams);	// x: GameWindGlobalVarSinePosMult, y: GameWindGlobalVarSineTimeMult
																						// z: GameWindGlobalVarCosinePosMult, w: GameWindGlobalVarCosineTimeMult


///////////////////////////////////////////////////////////////////////////////
// SAMPLERS
///////////////////////////////////////////////////////////////////////////////
	const Vector4*	gpParticlePosSrc = (Vector4*)params->m_ParticlePosLock[0].Base;
	Assert(gpParticlePosSrc);
	const Vector4*	gpParticleVelSrc = (Vector4*)params->m_ParticleVelLock[0].Base;
	Assert(gpParticleVelSrc);

	Vector4*		gpParticlePosDst = (Vector4*)params->m_ParticlePosLock[1].Base;
	Assert(gpParticlePosDst);
	Vector4*		gpParticleVelDst = (Vector4*)params->m_ParticleVelLock[1].Base;
	Assert(gpParticleVelDst);

#if RAIN_UPDATE_CPU_HEIGHTMAP
	float*			gpParticleCollisionSrc = (float*)params->m_HeightMapTextureLock.Base;
	Assert(gpParticleCollisionSrc);
#else
	float*			gpParticleCollisionSrc = NULL;
#endif

mthRandom	NoiseTexSampler;
	NoiseTexSampler.SetFullSeed(sysTimer::GetTicks());


Vector4*	pParticlePosSrc = (Vector4*)gpParticlePosSrc;
Vector4*	pParticleVelSrc = (Vector4*)gpParticleVelSrc;
Vector4*	pParticlePosDst = (Vector4*)gpParticlePosDst;
Vector4*	pParticleVelDst = (Vector4*)gpParticleVelDst;

	const s32 _count = RAIN_UPDATE_HEIGHT*RAIN_UPDATE_WIDTH;
	for(s32 xy=0; xy<_count; xy++)
	{
		///////////////////////////////////////////////////////////////////////////////
		// PS_ptxgpuUpdateDrop

		// get the particle position and velocity 
		Vector4 ptxPosition = *(pParticlePosSrc++);
		Vector4 ptxVelocity = *(pParticleVelSrc++);

		// Transform the position to world space
		//ptxPosition.xyz += gRefParticlePos + gOffsetParticlePos;
		ptxPosition.x += gRefParticlePos.x + gOffsetParticlePos.x;
		ptxPosition.y += gRefParticlePos.y + gOffsetParticlePos.y;
		ptxPosition.z += gRefParticlePos.z + gOffsetParticlePos.z;

		// Position W contains the current particle life value
		float curLife = fabs(ptxPosition.w);

		// unpack the current and max life
		float initLife;
		float seed;
		UnPackVelW(ptxVelocity.w, gLifeMinMaxClampToGround.y, initLife, seed);

	    // update the particle life
		curLife -= gGravityWindTimeStepCut.z;

		// check if the particle is dead
		if (curLife<=0.0f || gResetParticles>0.0f)
		{
			// the particle is dead - we need to emit a new one

			// get some random data
			Vector4 fRandom1, fRandom2;
			fRandom1.x = NoiseTexSampler.GetFloat();
			fRandom2.x = NoiseTexSampler.GetFloat();
			fRandom1.y = NoiseTexSampler.GetFloat();
			fRandom2.y = NoiseTexSampler.GetFloat();
			fRandom1.z = NoiseTexSampler.GetFloat();
			fRandom2.z = NoiseTexSampler.GetFloat();
			fRandom1.w = NoiseTexSampler.GetFloat();
			fRandom2.w = NoiseTexSampler.GetFloat();
	

			// position
			//float3 emitRange = fRandom1.wzy-0.5f;
			Vector3 emitRange(fRandom1.w-0.5f, fRandom1.z-0.5f, fRandom1.y-0.5f);
			
			//ptxPosition.xyz = gBoxCentrePos;
			//ptxPosition.xyz += gBoxRight * gBoxSize.x * emitRange.x;
			//ptxPosition.xyz += gBoxForward * gBoxSize.y * emitRange.y;
			//ptxPosition.xyz += boxUp * gBoxSize.z * emitRange.z;
			ptxPosition.x = gBoxCentrePos.x;
			ptxPosition.y = gBoxCentrePos.y;
			ptxPosition.z = gBoxCentrePos.z;

			ptxPosition.x += gBoxRight.x * gBoxSize.x * emitRange.x;
			ptxPosition.y += gBoxRight.y * gBoxSize.x * emitRange.x;
			ptxPosition.z += gBoxRight.z * gBoxSize.x * emitRange.x;
			ptxPosition.x += gBoxForward.x * gBoxSize.y * emitRange.y;
			ptxPosition.y += gBoxForward.y * gBoxSize.y * emitRange.y;
			ptxPosition.z += gBoxForward.z * gBoxSize.y * emitRange.y;
			ptxPosition.x += boxUp.x * gBoxSize.z * emitRange.z;
			ptxPosition.y += boxUp.y * gBoxSize.z * emitRange.z;
			ptxPosition.z += boxUp.z * gBoxSize.z * emitRange.z;


			// velocity 
			//ptxVelocity.xyz = gVelocityMin + (gVelocityMax.xyz-gVelocityMin)*fRandom2.xyz;
			ptxVelocity.x = gVelocityMin.x + (gVelocityMax.x-gVelocityMin.x)*fRandom2.x;
			ptxVelocity.y = gVelocityMin.y + (gVelocityMax.y-gVelocityMin.y)*fRandom2.y;
			ptxVelocity.z = gVelocityMin.z + (gVelocityMax.z-gVelocityMin.z)*fRandom2.z;

			// life
			curLife = gLifeMinMaxClampToGround.x + (gLifeMinMaxClampToGround.y-gLifeMinMaxClampToGround.x)*fRandom1.w;
			initLife = curLife;

			// collision state and texture id
			seed = Clamp(fRandom1.x - 0.004f, 0.0f, 1.0f); // Seed can't be one
		}

	    // update the particle position
		//ptxPosition.xyz += ptxVelocity.xyz * gGravityWindTimeStepCut.zzz;
		ptxPosition.x += ptxVelocity.x * gGravityWindTimeStepCut.z;
		ptxPosition.y += ptxVelocity.y * gGravityWindTimeStepCut.z;
		ptxPosition.z += ptxVelocity.z * gGravityWindTimeStepCut.z;

		// keep the particle inside the box
		//float3 boxToPtx = ptxPosition.xyz - gBoxCentrePos;
		Vector3 boxToPtx;
		boxToPtx.x = ptxPosition.x - gBoxCentrePos.x;
		boxToPtx.y = ptxPosition.y - gBoxCentrePos.y;
		boxToPtx.z = ptxPosition.z - gBoxCentrePos.z;

		//float dotRight = dot(gBoxRight, boxToPtx);
		float dotRight = gBoxRight.Dot(boxToPtx);
		//float dotForward = dot(gBoxForward, boxToPtx);
		float dotForward = gBoxForward.Dot(boxToPtx);
		//float dotUp = dot(boxUp, boxToPtx);
		float dotUp = boxUp.Dot(boxToPtx);
	
		// TODO: This code causes pops as the particle can changes position while fully opaque
 		//ptxPosition.xyz -= gBoxRight * gBoxSize.x * ceil((dotRight/gBoxSize.x)-0.5f);
 		//ptxPosition.xyz -= gBoxForward * gBoxSize.y * ceil((dotForward/gBoxSize.y)-0.5f);
 		//ptxPosition.xyz -= boxUp * gBoxSize.z * ceil((dotUp/gBoxSize.z)-0.5f);

 		ptxPosition.x -= gBoxRight.x * gBoxSize.x * _ceilf((dotRight/gBoxSize.x)-0.5f);
 		ptxPosition.y -= gBoxRight.y * gBoxSize.x * _ceilf((dotRight/gBoxSize.x)-0.5f);
 		ptxPosition.z -= gBoxRight.z * gBoxSize.x * _ceilf((dotRight/gBoxSize.x)-0.5f);

		ptxPosition.x -= gBoxForward.x * gBoxSize.y * _ceilf((dotForward/gBoxSize.y)-0.5f);
		ptxPosition.y -= gBoxForward.y * gBoxSize.y * _ceilf((dotForward/gBoxSize.y)-0.5f);
		ptxPosition.z -= gBoxForward.z * gBoxSize.y * _ceilf((dotForward/gBoxSize.y)-0.5f);

		ptxPosition.x -= boxUp.x * gBoxSize.z * _ceilf((dotUp/gBoxSize.z)-0.5f);
		ptxPosition.y -= boxUp.y * gBoxSize.z * _ceilf((dotUp/gBoxSize.z)-0.5f);
		ptxPosition.z -= boxUp.z * gBoxSize.z * _ceilf((dotUp/gBoxSize.z)-0.5f);

		// get the depth info for the new particle position (after keeping the particle within the box)
		Vector2 depthInfo2;
		DepthCompareHeightMap(depthInfo2,		ptxPosition.x,ptxPosition.y,ptxPosition.z,
												gDepthViewProjMtx,
												gpParticleCollisionSrc);

	    // update the particle position (v += g * dt)
		//ptxVelocity.xyz += float3(0.0f, 0.0f, gGravityWindTimeStepCut.x) * gGravityWindTimeStepCut.zzz;
		ptxVelocity.z += gGravityWindTimeStepCut.x * gGravityWindTimeStepCut.z;

	#if PTXGPU_USE_WIND
		// update the particle's velocity with wind influence
		//ptxVelocity.xyz += ptxgpuApplyWindInfluence(ptxPosition.xyz, ptxVelocity.xyz, Input.texCoord0.xy);
		Vector3 windInfluence;
		ptxgpuApplyWindInfluence(windInfluence,		ptxPosition, ptxVelocity, NoiseTexSampler,
													gGravityWindTimeStepCut, gWindBaseVel, gWindGustVel, gWindVarianceVel, gWindMultParams);
		ptxVelocity.x += windInfluence.x;
		ptxVelocity.y += windInfluence.y;
		ptxVelocity.z += windInfluence.z;
	#endif

		// Transform particle position back to local space
		//ptxPosition.xyz -= gRefParticlePos;
		ptxPosition.x -= gRefParticlePos.x;
		ptxPosition.y -= gRefParticlePos.y;
		ptxPosition.z -= gRefParticlePos.z;
	
		// Store the current particle life value along with the collision info in position W
		// We saturate curLife it died and became negative
		ptxPosition.w = curLife * depthInfo2.x;
	
		// pack the current and max life
		ptxVelocity.w = PackVelW(gLifeMinMaxClampToGround.y, initLife, seed);

		// pack the position and velocity
		*(pParticlePosDst++) = ptxPosition;
		*(pParticleVelDst++) = ptxVelocity;
	}//for(s32 y=0; y<RAIN_UPDATE_HEIGHT; y++)...
}// !opt...

}// end of _RainUpdateCpuTaskDrop()...


//
///////////////////////////////////////////////////////////////////////////////
// GPUPTFX_UPDATE_GROUND.FX - gpu ground particle (raindrop, snowdrop etc)
///////////////////////////////////////////////////////////////////////////////
//
// VertexShader = compile VERTEXSHADER VS_ragePassThroughNoXformTexOnly();
// PixelShader  = compile PIXELSHADER PS_ptxgpuUpdateGround()	PS4_TARGET(FMT_32_ABGR);
//
static void _RainUpdateCpuTaskGround(ptxcpuUpdateBaseParams *params, u32 _width, u32 _height)
{
#define PTXGPU_USE_WIND						(1)

const u32 RAIN_UPDATE_WIDTH = _width;	//128;
const u32 RAIN_UPDATE_HEIGHT= _height;	//128;

///////////////////////////////////////////////////////////////////////////////
// SHADER VARIABLES 
///////////////////////////////////////////////////////////////////////////////
static dev_bool bUseOpt = true;

if(bUseOpt)	// opt
{
	const Vec4V		constZero	(0.0f, 0.0f, 0.0f, 0.0f);
	const Vec4V		constOne	(1.0f, 1.0f, 1.0f, 1.0f);
	const Vec4V		constHalf	(0.5f, 0.5f, 0.5f, 0.5f);
	const Vec4V		const255	(255.0f, 255.0f, 255.0f, 255.0f);
	const Vec4V		const0_004	(0.004f, 0.004f, 0.004f, 0.004f);
	const Vec4V		const0_1	(0.1f, 0.1f, 0.1f, 0.1f);
	const Vec4V		const0_01	(0.01f, 0.01f, 0.01f, 0.01f);
	const Vec4V		const0_05	(0.05f, 0.05f, 0.05f, 0.05f);
	const Vec4V		constSelOne	(V_MASKXYZW);	// 0xffffffff	0xffffffff	0xffffffff	0xffffffff
	const Vec4V		constSelZero(V_ZERO);		// 0x00000000	0x00000000	0x00000000	0x00000000

	const Mat44V	_gDepthViewProjMtx		= RC_MAT44V(params->m_gDepthViewProjMtx);
	const Vec4V		_gDepthViewProjMtxAX	= Vec4V(Vec::V4SplatX(_gDepthViewProjMtx.GetCol0Intrin128()));
	const Vec4V		_gDepthViewProjMtxAY	= Vec4V(Vec::V4SplatY(_gDepthViewProjMtx.GetCol0Intrin128()));
	const Vec4V		_gDepthViewProjMtxAZ	= Vec4V(Vec::V4SplatZ(_gDepthViewProjMtx.GetCol0Intrin128()));
	const Vec4V		_gDepthViewProjMtxAW	= Vec4V(Vec::V4SplatW(_gDepthViewProjMtx.GetCol0Intrin128()));
	const Mat44V	gDepthViewProjMtxA(_gDepthViewProjMtxAX, _gDepthViewProjMtxAY, _gDepthViewProjMtxAZ, _gDepthViewProjMtxAW);

	const Vec4V		_gDepthViewProjMtxBX	= Vec4V(Vec::V4SplatX(_gDepthViewProjMtx.GetCol1Intrin128()));
	const Vec4V		_gDepthViewProjMtxBY	= Vec4V(Vec::V4SplatY(_gDepthViewProjMtx.GetCol1Intrin128()));
	const Vec4V		_gDepthViewProjMtxBZ	= Vec4V(Vec::V4SplatZ(_gDepthViewProjMtx.GetCol1Intrin128()));
	const Vec4V		_gDepthViewProjMtxBW	= Vec4V(Vec::V4SplatW(_gDepthViewProjMtx.GetCol1Intrin128()));
	const Mat44V	gDepthViewProjMtxB(_gDepthViewProjMtxBX, _gDepthViewProjMtxBY, _gDepthViewProjMtxBZ, _gDepthViewProjMtxBW);

	const Vec4V		_gDepthViewProjMtxCX	= Vec4V(Vec::V4SplatX(_gDepthViewProjMtx.GetCol2Intrin128()));
	const Vec4V		_gDepthViewProjMtxCY	= Vec4V(Vec::V4SplatY(_gDepthViewProjMtx.GetCol2Intrin128()));
	const Vec4V		_gDepthViewProjMtxCZ	= Vec4V(Vec::V4SplatZ(_gDepthViewProjMtx.GetCol2Intrin128()));
	const Vec4V		_gDepthViewProjMtxCW	= Vec4V(Vec::V4SplatW(_gDepthViewProjMtx.GetCol2Intrin128()));
	const Mat44V	gDepthViewProjMtxC(_gDepthViewProjMtxCX, _gDepthViewProjMtxCY, _gDepthViewProjMtxCZ, _gDepthViewProjMtxCW);

	const Vec4V		gInvDepthViewProjMtxCZ	= Vec4V(Vec::V4InvertPrecise(_gDepthViewProjMtxCZ.GetIntrin128()));

	const Vec4V		_gDepthViewProjMtxDX	= Vec4V(Vec::V4SplatX(_gDepthViewProjMtx.GetCol3Intrin128()));
	const Vec4V		_gDepthViewProjMtxDY	= Vec4V(Vec::V4SplatY(_gDepthViewProjMtx.GetCol3Intrin128()));
	const Vec4V		_gDepthViewProjMtxDZ	= Vec4V(Vec::V4SplatZ(_gDepthViewProjMtx.GetCol3Intrin128()));
	const Vec4V		_gDepthViewProjMtxDW	= Vec4V(Vec::V4SplatW(_gDepthViewProjMtx.GetCol3Intrin128()));
	const Mat44V	gDepthViewProjMtxD(_gDepthViewProjMtxDX, _gDepthViewProjMtxDY, _gDepthViewProjMtxDZ, _gDepthViewProjMtxDW);

	const Vec4V		gDepthViewProjMtxDZ		= _gDepthViewProjMtxDZ;	

	const Vector3	_gBoxForward			= params->m_gBoxForward;
	const Vec4V		gBoxForwardX(_gBoxForward.x, _gBoxForward.x, _gBoxForward.x, _gBoxForward.x);
	const Vec4V		gBoxForwardY(_gBoxForward.y, _gBoxForward.y, _gBoxForward.y, _gBoxForward.y);
	const Vec4V		gBoxForwardZ(_gBoxForward.z, _gBoxForward.z, _gBoxForward.z, _gBoxForward.z);

	// Use a constant up direction
	//float3 boxUp = normalize(cross(gBoxRight, gBoxForward));
	const Vector3	_gBoxRight				= params->m_gBoxRight;
	const Vec4V		gBoxRightX(_gBoxRight.x,_gBoxRight.x,_gBoxRight.x,_gBoxRight.x);
	const Vec4V		gBoxRightY(_gBoxRight.y,_gBoxRight.y,_gBoxRight.y,_gBoxRight.y);
	const Vec4V		gBoxRightZ(_gBoxRight.z,_gBoxRight.z,_gBoxRight.z,_gBoxRight.z);

	const Vector3	_gBoxCentrePos			= params->m_gBoxCentrePos;
	const Vec4V		gBoxCentrePosX(_gBoxCentrePos.x,_gBoxCentrePos.x,_gBoxCentrePos.x,_gBoxCentrePos.x);
	const Vec4V		gBoxCentrePosY(_gBoxCentrePos.y,_gBoxCentrePos.y,_gBoxCentrePos.y,_gBoxCentrePos.y);
	const Vec4V		gBoxCentrePosZ(_gBoxCentrePos.z,_gBoxCentrePos.z,_gBoxCentrePos.z,_gBoxCentrePos.z);

	// Use a constant up direction
	const Vec4V		boxUpX(0.0f, 0.0f, 0.0f, 0.0f);
	const Vec4V		boxUpY(0.0f, 0.0f, 0.0f, 0.0f);
	const Vec4V		boxUpZ(1.0f, 1.0f, 1.0f, 1.0f);

	// Transforming particles from local to worls space
	const Vec4V		_gRefParticlePos		= RC_VEC4V(params->m_gRefParticlePos);
	const Vec4V		gRefParticlePosX		= Vec4V(Vec::V4SplatX(_gRefParticlePos.GetIntrin128()));
	const Vec4V		gRefParticlePosY		= Vec4V(Vec::V4SplatY(_gRefParticlePos.GetIntrin128()));
	const Vec4V		gRefParticlePosZ		= Vec4V(Vec::V4SplatZ(_gRefParticlePos.GetIntrin128()));

	const Vec4V		_gOffsetParticlePos		= RC_VEC4V(params->m_gOffsetParticlePos);
	const Vec4V		gOffsetParticlePosX		= Vec4V(Vec::V4SplatX(_gOffsetParticlePos.GetIntrin128()));
	const Vec4V		gOffsetParticlePosY		= Vec4V(Vec::V4SplatY(_gOffsetParticlePos.GetIntrin128()));
	const Vec4V		gOffsetParticlePosZ		= Vec4V(Vec::V4SplatZ(_gOffsetParticlePos.GetIntrin128()));

	const Vec4V		_gGravityWindTimeStepCut= RC_VEC4V(params->m_gGravityWindTimeStepCut);	// x: Gravity, y: Wind Influence, z: TimeStep, w: 0 after camera cut, 1 otherwise
	const Vec4V		gGravityWindTimeStepCutX= Vec4V(Vec::V4SplatX(_gGravityWindTimeStepCut.GetIntrin128()));
	const Vec4V		gGravityWindTimeStepCutY= Vec4V(Vec::V4SplatY(_gGravityWindTimeStepCut.GetIntrin128()));
	const Vec4V		gGravityWindTimeStepCutZ= Vec4V(Vec::V4SplatZ(_gGravityWindTimeStepCut.GetIntrin128()));
	const Vec4V		gGravityWindTimeStepCutW= Vec4V(Vec::V4SplatW(_gGravityWindTimeStepCut.GetIntrin128()));

	const Vec4V		gGravityWindTimeStepCutWZeroSel = Vec4V(Vec::V4IsEqualV(gGravityWindTimeStepCutW.GetIntrin128(), constZero.GetIntrin128()));	// const!

	const float		_gResetParticles		= params->m_gResetParticles;
	const Vec4V		gResetParticles(_gResetParticles,_gResetParticles,_gResetParticles,_gResetParticles);

	// emitter settings
	const Vector3	_gBoxSize				= params->m_gBoxSize;
	const Vec4V		gBoxSizeX(_gBoxSize.x, _gBoxSize.x, _gBoxSize.x, _gBoxSize.x);
	const Vec4V		gBoxSizeY(_gBoxSize.y, _gBoxSize.y, _gBoxSize.y, _gBoxSize.y);
	const Vec4V		gBoxSizeZ(_gBoxSize.z, _gBoxSize.z, _gBoxSize.z, _gBoxSize.z);
	const Vec4V		gInvBoxSizeX(1.0f/_gBoxSize.x, 1.0f/_gBoxSize.x, 1.0f/_gBoxSize.x, 1.0f/_gBoxSize.x);
	const Vec4V		gInvBoxSizeY(1.0f/_gBoxSize.y, 1.0f/_gBoxSize.y, 1.0f/_gBoxSize.y, 1.0f/_gBoxSize.y);
	const Vec4V		gInvBoxSizeZ(1.0f/_gBoxSize.z, 1.0f/_gBoxSize.z, 1.0f/_gBoxSize.z, 1.0f/_gBoxSize.z);

	const Vec4V		_gLifeMinMaxClampToGround= RC_VEC4V(params->m_gLifeMinMaxClampToGround);
	const Vec4V		gLifeMinMaxClampToGroundX= Vec4V(Vec::V4SplatX(_gLifeMinMaxClampToGround.GetIntrin128()));
	const Vec4V		gLifeMinMaxClampToGroundY= Vec4V(Vec::V4SplatY(_gLifeMinMaxClampToGround.GetIntrin128()));
	const Vec4V		gLifeMinMaxClampToGroundZ= Vec4V(Vec::V4SplatZ(_gLifeMinMaxClampToGround.GetIntrin128()));

	const Vec4V		gLifeMinMaxClampToGroundZMoreThanZeroSel = Vec4V(Vec::V4IsGreaterThanV(gLifeMinMaxClampToGroundZ.GetIntrin128(), constZero.GetIntrin128()));

	const Vec3V		_gVelocityMin			= RC_VEC3V(params->m_gVelocityMin);
	const Vec4V		gVelocityMinX			= Vec4V(Vec::V4SplatX(_gVelocityMin.GetIntrin128()));
	const Vec4V		gVelocityMinY			= Vec4V(Vec::V4SplatY(_gVelocityMin.GetIntrin128()));
	const Vec4V		gVelocityMinZ			= Vec4V(Vec::V4SplatZ(_gVelocityMin.GetIntrin128()));

	const Vec3V		_gVelocityMax			= RC_VEC3V(params->m_gVelocityMax);
	const Vec4V		gVelocityMaxX			= Vec4V(Vec::V4SplatX(_gVelocityMax.GetIntrin128()));
	const Vec4V		gVelocityMaxY			= Vec4V(Vec::V4SplatY(_gVelocityMax.GetIntrin128()));
	const Vec4V		gVelocityMaxZ			= Vec4V(Vec::V4SplatZ(_gVelocityMax.GetIntrin128()));

	// wind related
	const Vec4V		_gWindBaseVel			= params->m_gWindBaseVel;		// xyz: WindBaseVel, w: GameWindSpeed
	const Vec4V		gWindBaseVelX			= Vec4V(Vec::V4SplatX(_gWindBaseVel.GetIntrin128()));
	const Vec4V		gWindBaseVelY			= Vec4V(Vec::V4SplatY(_gWindBaseVel.GetIntrin128()));
	const Vec4V		gWindBaseVelZ			= Vec4V(Vec::V4SplatZ(_gWindBaseVel.GetIntrin128()));
	const Vec4V		gWindBaseVelW			= Vec4V(Vec::V4SplatW(_gWindBaseVel.GetIntrin128()));

	const Vec4V		_gWindGustVel			= params->m_gWindGustVel;		// xyz: WindGustVel, w: unused
	const Vec4V		gWindGustVelX			= Vec4V(Vec::V4SplatX(_gWindGustVel.GetIntrin128()));
	const Vec4V		gWindGustVelY			= Vec4V(Vec::V4SplatY(_gWindGustVel.GetIntrin128()));
	const Vec4V		gWindGustVelZ			= Vec4V(Vec::V4SplatZ(_gWindGustVel.GetIntrin128()));
	const Vec4V		gWindGustVelW			= Vec4V(Vec::V4SplatW(_gWindGustVel.GetIntrin128()));

	const Vec4V		_gWindVarianceVel		= params->m_gWindVarianceVel;	// xyz: WindVarianceVel, w: GameWindPositionalVarMult
	const Vec4V		gWindVarianceVelX		= Vec4V(Vec::V4SplatX(_gWindVarianceVel.GetIntrin128()));
	const Vec4V		gWindVarianceVelY		= Vec4V(Vec::V4SplatY(_gWindVarianceVel.GetIntrin128()));
	const Vec4V		gWindVarianceVelZ		= Vec4V(Vec::V4SplatZ(_gWindVarianceVel.GetIntrin128()));
	const Vec4V		gWindVarianceVelW		= Vec4V(Vec::V4SplatW(_gWindVarianceVel.GetIntrin128()));

	const Vec4V		_gWindMultParams		= params->m_gWindMultParams;	// x: GameWindGlobalVarSinePosMult, y: GameWindGlobalVarSineTimeMult
																			// z: GameWindGlobalVarCosinePosMult, w: GameWindGlobalVarCosineTimeMult
	const Vec4V		gWindMultParamsX		= Vec4V(Vec::V4SplatX(_gWindMultParams.GetIntrin128()));
	const Vec4V		gWindMultParamsY		= Vec4V(Vec::V4SplatY(_gWindMultParams.GetIntrin128()));
	const Vec4V		gWindMultParamsZ		= Vec4V(Vec::V4SplatZ(_gWindMultParams.GetIntrin128()));
	const Vec4V		gWindMultParamsW		= Vec4V(Vec::V4SplatW(_gWindMultParams.GetIntrin128()));



///////////////////////////////////////////////////////////////////////////////
// SAMPLERS
///////////////////////////////////////////////////////////////////////////////
	const Vec::Vector_4V*	gpParticlePosSrc = (Vec::Vector_4V*)params->m_ParticlePosLock[0].Base;
	Assert(gpParticlePosSrc);
	const Vec::Vector_4V*	gpParticleVelSrc = (Vec::Vector_4V*)params->m_ParticleVelLock[0].Base;
	Assert(gpParticleVelSrc);

	Vec::Vector_4V*			gpParticlePosDst = (Vec::Vector_4V*)params->m_ParticlePosLock[1].Base;
	Assert(gpParticlePosDst);
	Vec::Vector_4V*			gpParticleVelDst = (Vec::Vector_4V*)params->m_ParticleVelLock[1].Base;
	Assert(gpParticleVelDst);

#if RAIN_UPDATE_CPU_HEIGHTMAP
	float*			gpParticleCollisionSrc = (float*)params->m_HeightMapTextureLock.Base;
	Assert(gpParticleCollisionSrc);
#else
	float*			gpParticleCollisionSrc = NULL;
#endif

mthRandom	NoiseTexSampler;
	NoiseTexSampler.SetFullSeed(sysTimer::GetTicks());


Vec::Vector_4V*	pParticlePosSrc = (Vec::Vector_4V*)gpParticlePosSrc;
Vec::Vector_4V*	pParticleVelSrc = (Vec::Vector_4V*)gpParticleVelSrc;
Vec::Vector_4V*	pParticlePosDst = (Vec::Vector_4V*)gpParticlePosDst;
Vec::Vector_4V*	pParticleVelDst = (Vec::Vector_4V*)gpParticleVelDst;

	const s32 _count = RAIN_UPDATE_HEIGHT*RAIN_UPDATE_WIDTH;
	for(s32 xy=0; xy<_count; xy+=4)
	{
			///////////////////////////////////////////////////////////////////////////////
			// PS_ptxgpuUpdateGround

			// get the particle position and velocity 
			Vec4V ptxPosition0,ptxPosition1,ptxPosition2,ptxPosition3;
			Vec4V ptxVelocity0,ptxVelocity1,ptxVelocity2,ptxVelocity3;
			ptxPosition0.SetIntrin128( *(pParticlePosSrc+0) );
			ptxVelocity0.SetIntrin128( *(pParticleVelSrc+0) );
			ptxPosition1.SetIntrin128( *(pParticlePosSrc+1) );
			ptxVelocity1.SetIntrin128( *(pParticleVelSrc+1) );
			ptxPosition2.SetIntrin128( *(pParticlePosSrc+2) );
			ptxVelocity2.SetIntrin128( *(pParticleVelSrc+2) );
			ptxPosition3.SetIntrin128( *(pParticlePosSrc+3) );
			ptxVelocity3.SetIntrin128( *(pParticleVelSrc+3) );
			pParticlePosSrc += 4;
			pParticleVelSrc += 4;

			Vec4V ptxPositionX, ptxPositionY, ptxPositionZ, ptxPositionW;
			Vec4V ptxVelocityX, ptxVelocityY, ptxVelocityZ, ptxVelocityW;
			Vec4V t1, t2;
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxPosition0.GetIntrin128(), ptxPosition1.GetIntrin128()));		// X0X1Y0Y1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxPosition2.GetIntrin128(), ptxPosition3.GetIntrin128()));		// X2X3Y2Y3
			ptxPositionX = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X0X1X2X3
			ptxPositionY = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// Y0Y1Y2Y3
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxPosition0.GetIntrin128(), ptxPosition1.GetIntrin128()));		// Z0Z1W0W1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxPosition2.GetIntrin128(), ptxPosition3.GetIntrin128()));		// Z2Z3W2W3
			ptxPositionZ = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// Z0Z1Z2Z2
			ptxPositionW = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// W0W1W2W3

			t1 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxVelocity0.GetIntrin128(), ptxVelocity1.GetIntrin128()));		// X0X1Y0Y1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxVelocity2.GetIntrin128(), ptxVelocity3.GetIntrin128()));		// X2X3Y2Y3
			ptxVelocityX = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X0X1X2X3
			ptxVelocityY = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// Y0Y1Y2Y3
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxVelocity0.GetIntrin128(), ptxVelocity1.GetIntrin128()));		// Z0Z1W0W1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxVelocity2.GetIntrin128(), ptxVelocity3.GetIntrin128()));		// Z2Z3W2W3
			ptxVelocityZ = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// Z0Z1Z2Z2
			ptxVelocityW = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// W0W1W2W3


			// Transform the position to world space
			//ptxPosition.xyz += gRefParticlePos + gOffsetParticlePos;
			ptxPositionX += gRefParticlePosX + gOffsetParticlePosX;
			ptxPositionY += gRefParticlePosY + gOffsetParticlePosY;
			ptxPositionZ += gRefParticlePosZ + gOffsetParticlePosZ;

			Vec4V ptxStartPositionX, ptxStartPositionY, ptxStartPositionZ;
			ptxStartPositionX = ptxPositionX;
			ptxStartPositionY = ptxPositionY;
			ptxStartPositionZ = ptxPositionZ;
		
			// Position W contains the current particle life value
			Vec4V curLife;
			curLife = ptxPositionW;

			// unpack the current and max life
			Vec4V initLife;
			Vec4V seed;
			UnPackVelW(ptxVelocityW, gLifeMinMaxClampToGroundY, initLife, seed);

			// update the particle life
			curLife -= gGravityWindTimeStepCutZ;


			// check if the particle is dead
			//if (curLife <= 0.0f)
			const Vec4V newSelect = Vec4V(Vec::V4IsLessThanOrEqualV(curLife.GetIntrin128(),	constZero.GetIntrin128()));
			{
				// the particle is dead - we need to emit a new one
				Vec4V newPtxPositionX, newPtxPositionY, newPtxPositionZ;
				Vec4V newPtxVelocityX, newPtxVelocityY, newPtxVelocityZ;
				Vec4V newCurLife, newInitLife, newSeed;

				// get some random data
				Vec4V fRandom1X, fRandom1Y, fRandom1Z, fRandom1W;
				Vec4V fRandom2X, fRandom2Y, fRandom2Z;
				fRandom1X = NoiseTexSampler.Get4FloatsV();
				fRandom2X = NoiseTexSampler.Get4FloatsV();
				fRandom1Y = NoiseTexSampler.Get4FloatsV();
				fRandom2Y = NoiseTexSampler.Get4FloatsV();
				fRandom1Z = NoiseTexSampler.Get4FloatsV();
				fRandom2Z = NoiseTexSampler.Get4FloatsV();
				fRandom1W = NoiseTexSampler.Get4FloatsV();

				// position
				//float3 emitRange = fRandom1.wzy-0.5f;
				Vec4V emitRangeX, emitRangeY, emitRangeZ;
				emitRangeX = fRandom1W - constHalf;
				emitRangeY = fRandom1Z - constHalf;
				emitRangeZ = fRandom1Y - constHalf;

				//ptxPosition.xyz = gBoxCentrePos;
				//ptxPosition.xyz += gBoxRight * gBoxSize.x * emitRange.x;
				//ptxPosition.xyz += gBoxForward * gBoxSize.y * emitRange.y;
				//ptxPosition.xyz += boxUp * gBoxSize.z * emitRange.z;
				newPtxPositionX = gBoxCentrePosX;
				newPtxPositionY = gBoxCentrePosY;
				newPtxPositionZ = gBoxCentrePosZ;
				newPtxPositionX += gBoxRightX * gBoxSizeX * emitRangeX;
				newPtxPositionY += gBoxRightY * gBoxSizeX * emitRangeX;
				newPtxPositionZ += gBoxRightZ * gBoxSizeX * emitRangeX;
				newPtxPositionX += gBoxForwardX * gBoxSizeY * emitRangeY;
				newPtxPositionY += gBoxForwardY * gBoxSizeY * emitRangeY;
				newPtxPositionZ += gBoxForwardZ * gBoxSizeY * emitRangeY;
				//newPtxPositionX += boxUpX * gBoxSizeZ * emitRangeZ;		// boxUp = (0.0f, 0.0f, 1.0f)
				//newPtxPositionY += boxUpY * gBoxSizeZ * emitRangeZ;
				newPtxPositionZ += boxUpZ * gBoxSizeZ * emitRangeZ;

				// fix to ground level
				Vec4V depthInfo1X, depthInfo1Y;
				DepthCompareHeightMap(depthInfo1X, depthInfo1Y,
										ptxPositionX,ptxPositionY,ptxPositionZ,
										gDepthViewProjMtxA,gDepthViewProjMtxB,gDepthViewProjMtxC,gDepthViewProjMtxD,
										gpParticleCollisionSrc);

				//float collisionZ = (depthInfo1.y - (gDepthViewProjMtx._43))/(gDepthViewProjMtx._33);
				Vec4V collisionZ;
				collisionZ = (depthInfo1Y - gDepthViewProjMtxDZ) * (gInvDepthViewProjMtxCZ);
				newPtxPositionZ = collisionZ;

				// velocity 
				//ptxVelocity.xyz = gVelocityMin + (gVelocityMax.xyz-gVelocityMin)*fRandom2.xyz;
				newPtxVelocityX = gVelocityMinX + (gVelocityMaxX-gVelocityMinX)*fRandom2X;
				newPtxVelocityY = gVelocityMinY + (gVelocityMaxY-gVelocityMinY)*fRandom2Y;
				newPtxVelocityZ = gVelocityMinZ + (gVelocityMaxZ-gVelocityMinZ)*fRandom2Z;

				// life
				//curLife = gLifeMinMaxClampToGround.x + (gLifeMinMaxClampToGround.y-gLifeMinMaxClampToGround.x)*fRandom1.w;
				//initLife = max(curLife, 0.0);
				newCurLife	= gLifeMinMaxClampToGroundX + (gLifeMinMaxClampToGroundY-gLifeMinMaxClampToGroundX)*fRandom1W;
				newInitLife = Vec4V(Vec::V4Max(newCurLife.GetIntrin128(), constZero.GetIntrin128()));

				// seed
				//seed = saturate(fRandom1.x - 0.004); // Seed can't be one
				newSeed = Vec4V(Vec::V4Saturate((fRandom1X - const0_004).GetIntrin128())); // Seed can't be one


				ptxPositionX = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxPositionX.GetIntrin128(), newPtxPositionX.GetIntrin128()));
				ptxPositionY = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxPositionY.GetIntrin128(), newPtxPositionY.GetIntrin128()));
				ptxPositionZ = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxPositionZ.GetIntrin128(), newPtxPositionZ.GetIntrin128()));

				ptxVelocityX = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxVelocityX.GetIntrin128(), newPtxVelocityX.GetIntrin128()));
				ptxVelocityY = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxVelocityY.GetIntrin128(), newPtxVelocityY.GetIntrin128()));
				ptxVelocityZ = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxVelocityZ.GetIntrin128(), newPtxVelocityZ.GetIntrin128()));

				curLife		 = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), curLife.GetIntrin128(),		newCurLife.GetIntrin128()));
				initLife	 = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), initLife.GetIntrin128(),		newInitLife.GetIntrin128()));
				seed		 = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), seed.GetIntrin128(),			newSeed.GetIntrin128()));
			}

			// update the particle position
			//ptxPosition.xyz += ptxVelocity.xyz * gGravityWindTimeStepCut.zzz;
			ptxPositionX += ptxVelocityX * gGravityWindTimeStepCutZ;
			ptxPositionY += ptxVelocityY * gGravityWindTimeStepCutZ;
			ptxPositionZ += ptxVelocityZ * gGravityWindTimeStepCutZ;

			// keep the particle inside the box
			Vec4V ptxPosPreBoxX, ptxPosPreBoxY, ptxPosPreBoxZ;
			ptxPosPreBoxX = ptxPositionX;
			ptxPosPreBoxY = ptxPositionY;
			ptxPosPreBoxZ = ptxPositionZ;

			Vec4V boxToPtxX, boxToPtxY, boxToPtxZ;
			boxToPtxX = ptxPositionX - gBoxCentrePosX;
			boxToPtxY = ptxPositionY - gBoxCentrePosY;
			boxToPtxZ = ptxPositionZ - gBoxCentrePosZ;

			//float dotRight		= gBoxRight.Dot(boxToPtx);
			const Vec4V dotRight	= gBoxRightX*boxToPtxX + gBoxRightY*boxToPtxY + gBoxRightZ*boxToPtxZ;
			//float dotForward		= gBoxForward.Dot(boxToPtx);
			const Vec4V dotForward	= gBoxForwardX*boxToPtxX + gBoxForwardY*boxToPtxY + gBoxForwardZ*boxToPtxZ;

	 		//ptxPosition.xyz -= gBoxRight * gBoxSize.x * ceil((dotRight/gBoxSize.x)-0.5f);
			Vec4V ceilDotRightBoxSizeX = (dotRight*gInvBoxSizeX) - constHalf;
			_V4Ceilf(ceilDotRightBoxSizeX);
			ptxPositionX -= gBoxRightX * gBoxSizeX * ceilDotRightBoxSizeX;
			ptxPositionY -= gBoxRightY * gBoxSizeX * ceilDotRightBoxSizeX;
	 		ptxPositionZ -= gBoxRightZ * gBoxSizeX * ceilDotRightBoxSizeX;

			//ptxPosition.xyz -= gBoxForward * gBoxSize.y * ceil((dotForward/gBoxSize.y)-0.5f);
			Vec4V ceilDotForwardBoxSizeY = (dotForward * gInvBoxSizeY) - constHalf;
			_V4Ceilf(ceilDotForwardBoxSizeY);
			ptxPositionX -= gBoxForwardX * gBoxSizeY * ceilDotForwardBoxSizeY;
			ptxPositionY -= gBoxForwardY * gBoxSizeY * ceilDotForwardBoxSizeY;
			ptxPositionZ -= gBoxForwardZ * gBoxSizeY * ceilDotForwardBoxSizeY;

			//float3 ptxPosMove = ptxPosition.xyz - ptxPosPreBox;
			Vec4V ptxPosMoveX, ptxPosMoveY, ptxPosMoveZ;
			ptxPosMoveX = ptxPositionX - ptxPosPreBoxX;
			ptxPosMoveY = ptxPositionY - ptxPosPreBoxY;
			ptxPosMoveZ = ptxPositionZ - ptxPosPreBoxZ;


			//bool ptxMoved = false;
			Vec4V ptxMoved = constSelZero;
			//if (dot(ptxPosMove.xyz, ptxPosMove.xyz) > 0.01)
			const Vec4V dotPtxPosMove = ptxPosMoveX*ptxPosMoveX + ptxPosMoveY*ptxPosMoveY + ptxPosMoveZ*ptxPosMoveZ;
			const Vec4V dotPtxPosMoveSelect = Vec4V(Vec::V4IsGreaterThanV(dotPtxPosMove.GetIntrin128(), const0_01.GetIntrin128()));
			{
				// During normal play - restore life to initial value
				// This will force a fade in and help avoid a pop
				// For camera cuts we basically don't want to do anything.
				// curLife += (initLife - curLife) * gGravityWindTimeStepCut.w;
				Vec4V newCurLife = curLife;
				newCurLife += (initLife - curLife) * gGravityWindTimeStepCutW;
		
				curLife	= Vec4V(Vec::V4SelectFT(dotPtxPosMoveSelect.GetIntrin128(), curLife.GetIntrin128(),		newCurLife.GetIntrin128()));
				//ptxMoved = true;
				ptxMoved= Vec4V(Vec::V4SelectFT(dotPtxPosMoveSelect.GetIntrin128(), ptxMoved.GetIntrin128(),	constSelOne.GetIntrin128()));
			}

			// get the depth info for the new particle position (after keeping the particle within the box)
			Vec4V depthInfo2X, depthInfo2Y;
			DepthCompareHeightMap(depthInfo2X, depthInfo2Y,
										ptxPositionX,ptxPositionY,ptxPositionZ,
										gDepthViewProjMtxA,gDepthViewProjMtxB,gDepthViewProjMtxC,gDepthViewProjMtxD,
										gpParticleCollisionSrc);


		//float collisionZ = (depthInfo2.y - (gDepthViewProjMtx._43))/(gDepthViewProjMtx._33);
		const Vec4V collisionZ = (depthInfo2Y - gDepthViewProjMtxDZ) * (gInvDepthViewProjMtxCZ);
		
		//if (gLifeMinMaxClampToGround.z>0.0f)
		//{
		//	float collisionZ = (depthInfo2.y - (gDepthViewProjMtx._43))/(gDepthViewProjMtx._33);
		//	ptxPosition.z = collisionZ;
		//}
		ptxPositionZ = Vec4V(Vec::V4SelectFT(gLifeMinMaxClampToGroundZMoreThanZeroSel.GetIntrin128(), ptxPositionZ.GetIntrin128(), collisionZ.GetIntrin128()));
		//else if (depthInfo2.x < 0.1f || ptxMoved)
		{
			const Vec4V newSelect0 = Vec4V(Vec::V4IsLessThanV(depthInfo2X.GetIntrin128(), const0_1.GetIntrin128()));
			const Vec4V newSelect1 = Vec4V(Vec::V4Or(newSelect0.GetIntrin128(), ptxMoved.GetIntrin128()));
			const Vec4V newSelect  = Vec4V(Vec::V4SelectFT(gLifeMinMaxClampToGroundZMoreThanZeroSel.GetIntrin128(), newSelect1.GetIntrin128(), constSelZero.GetIntrin128())); 
			Vec4V	newPtxPositionX = ptxPositionX,
					newPtxPositionY = ptxPositionY,
					newPtxPositionZ = ptxPositionZ,
					newCurLife		= curLife;


			Vec4V cascadeSel = constSelOne;

			//if(gGravityWindTimeStepCut.w == 0.0)
			//{
			//	ptxPosition.z = collisionZ;
			//}
			newPtxPositionZ = Vec4V(Vec::V4SelectFT(gGravityWindTimeStepCutWZeroSel.GetIntrin128(), newPtxPositionZ.GetIntrin128(), collisionZ.GetIntrin128()));
			cascadeSel		= Vec4V(Vec::V4SelectFT(gGravityWindTimeStepCutWZeroSel.GetIntrin128(), cascadeSel.GetIntrin128(),		constSelZero.GetIntrin128()));
			
			//else if (depthInfo2.y <= 0.0f)
			//{
			//	// kill the particle if it's below the entire height map (below a z of zero)
			//	curLife = 0.0f;
			//}
			const Vec4V depthInfo2yLessThanOrEqualZeroSel0 = Vec4V(Vec::V4IsLessThanOrEqualV(depthInfo2Y.GetIntrin128(),			constZero.GetIntrin128()));
			const Vec4V depthInfo2yLessThanOrEqualZeroSel  = Vec4V(Vec::V4And(depthInfo2yLessThanOrEqualZeroSel0.GetIntrin128(),	cascadeSel.GetIntrin128()));
			newCurLife = Vec4V(Vec::V4SelectFT(depthInfo2yLessThanOrEqualZeroSel.GetIntrin128(), newCurLife.GetIntrin128(), constZero.GetIntrin128()));
			cascadeSel = Vec4V(Vec::V4SelectFT(depthInfo2yLessThanOrEqualZeroSel.GetIntrin128(), cascadeSel.GetIntrin128(), constSelZero.GetIntrin128()));

			//else if (collisionZ - ptxPosition.z > 1.0f)
			//{
			//	// reset the particle position if it's hit a barrier that's too high
			//	ptxPosition.xyz = ptxStartPosition;
			//}
			const Vec4V collZMinusPosZ = collisionZ - ptxPositionZ;
			const Vec4V collZMinusPosZGreaterOneSel0 = Vec4V(Vec::V4IsGreaterThanV(collZMinusPosZ.GetIntrin128(),		constOne.GetIntrin128()));
			const Vec4V collZMinusPosZGreaterOneSel  = Vec4V(Vec::V4And(collZMinusPosZGreaterOneSel0.GetIntrin128(),	cascadeSel.GetIntrin128()));
			newPtxPositionX = Vec4V(Vec::V4SelectFT(collZMinusPosZGreaterOneSel.GetIntrin128(), newPtxPositionX.GetIntrin128(), ptxStartPositionX.GetIntrin128()));
			newPtxPositionY = Vec4V(Vec::V4SelectFT(collZMinusPosZGreaterOneSel.GetIntrin128(), newPtxPositionY.GetIntrin128(), ptxStartPositionY.GetIntrin128()));
			newPtxPositionZ = Vec4V(Vec::V4SelectFT(collZMinusPosZGreaterOneSel.GetIntrin128(), newPtxPositionZ.GetIntrin128(), ptxStartPositionZ.GetIntrin128()));
			cascadeSel		= Vec4V(Vec::V4SelectFT(collZMinusPosZGreaterOneSel.GetIntrin128(), cascadeSel.GetIntrin128(),		constSelZero.GetIntrin128()));

			//else
			//{
			//	// Let the particle raise gradually
			//	ptxPosition.z = FPMin(ptxPosition.z + 0.05f, collisionZ);
			//}
			const Vec4V minPtxPosZCollZ = Vec4V(Vec::V4Min((ptxPositionZ+const0_05).GetIntrin128(), collisionZ.GetIntrin128()));
			newPtxPositionZ = Vec4V(Vec::V4SelectFT(cascadeSel.GetIntrin128(), newPtxPositionZ.GetIntrin128(), minPtxPosZCollZ.GetIntrin128()));

			// final select:
			ptxPositionX = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxPositionX.GetIntrin128(), newPtxPositionX.GetIntrin128()));
			ptxPositionY = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxPositionY.GetIntrin128(), newPtxPositionY.GetIntrin128()));
			ptxPositionZ = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), ptxPositionZ.GetIntrin128(), newPtxPositionZ.GetIntrin128()));
			curLife		 = Vec4V(Vec::V4SelectFT(newSelect.GetIntrin128(), curLife.GetIntrin128(),		newCurLife.GetIntrin128()));
		}

		// update the particle position (v += g * dt)
		//ptxVelocity.xyz += float3(0.0f, 0.0f, gGravityWindTimeStepCut.x) * gGravityWindTimeStepCut.zzz;
		ptxVelocityZ += gGravityWindTimeStepCutX * gGravityWindTimeStepCutZ;

		#if PTXGPU_USE_WIND
			// update the particle's velocity with wind influence
			//ptxVelocity.xyz += ptxgpuApplyWindInfluence(ptxPosition.xyz, ptxVelocity.xyz, Input.texCoord0.xy);
			Vec4V windInfluenceX, windInfluenceY, windInfluenceZ;
			ptxgpuApplyWindInfluence(windInfluenceX,windInfluenceY, windInfluenceZ,
														ptxPositionX,ptxPositionY,ptxPositionZ,
														ptxVelocityX,ptxVelocityY,ptxVelocityZ,
														NoiseTexSampler,
														gGravityWindTimeStepCutX,gGravityWindTimeStepCutY,gGravityWindTimeStepCutZ,gGravityWindTimeStepCutW,
														gWindBaseVelX,gWindBaseVelY,gWindBaseVelZ,gWindBaseVelW,
														gWindGustVelX,gWindGustVelY,gWindGustVelZ,gWindGustVelW,
														gWindVarianceVelX,gWindVarianceVelY,gWindVarianceVelZ,gWindVarianceVelW,
														gWindMultParamsX,gWindMultParamsY,gWindMultParamsZ,gWindMultParamsW);
			ptxVelocityX += windInfluenceX;
			ptxVelocityY += windInfluenceY;
			ptxVelocityZ += windInfluenceZ;
		#endif

			// Transform particle position back to local space
			//ptxPosition.xyz -= gRefParticlePos;
			ptxPositionX -= gRefParticlePosX;
			ptxPositionY -= gRefParticlePosY;
			ptxPositionZ -= gRefParticlePosZ;
	
			// Position W contains the current particle life value
			ptxPositionW = curLife;
	
			// pack the current and max life
			//ptxVelocity.w = PackVelW(gLifeMinMaxClampToGround.y, initLife, seed);
			PackVelW(ptxVelocityW,		gLifeMinMaxClampToGroundY, initLife, seed);

			// pack the position and velocity
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxPositionX.GetIntrin128(), ptxPositionY.GetIntrin128()));		// X0Y0X1Y1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxPositionZ.GetIntrin128(), ptxPositionW.GetIntrin128()));		// Z0W0Z1W1
			ptxPosition0 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X0Y0Z0W0
			ptxPosition1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X1Y1Z1W1
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxPositionX.GetIntrin128(), ptxPositionY.GetIntrin128()));		// X2Y2X3Y3
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxPositionZ.GetIntrin128(), ptxPositionW.GetIntrin128()));		// Z2W2Z3W3
			ptxPosition2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X2Y2Z2W2
			ptxPosition3 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X3Y3Z3W3

			t1 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxVelocityX.GetIntrin128(), ptxVelocityY.GetIntrin128()));		// X0Y0X1Y1
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::X2,Vec::Y1,Vec::Y2>(ptxVelocityZ.GetIntrin128(), ptxVelocityW.GetIntrin128()));		// Z0W0Z1W1
			ptxVelocity0 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X0Y0Z0W0
			ptxVelocity1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X1Y1Z1W1
			t1 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxVelocityX.GetIntrin128(), ptxVelocityY.GetIntrin128()));		// X2Y2X3Y3
			t2 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::Z2,Vec::W1,Vec::W2>(ptxVelocityZ.GetIntrin128(), ptxVelocityW.GetIntrin128()));		// Z2W2Z3W3
			ptxVelocity2 = Vec4V(Vec::V4PermuteTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Y2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X2Y2Z2W2
			ptxVelocity3 = Vec4V(Vec::V4PermuteTwo<Vec::Z1,Vec::W1,Vec::Z2,Vec::W2>(t1.GetIntrin128(), t2.GetIntrin128()));					// X3Y3Z3W3

			*(pParticlePosDst+0) = ptxPosition0.GetIntrin128();
			*(pParticleVelDst+0) = ptxVelocity0.GetIntrin128();
			*(pParticlePosDst+1) = ptxPosition1.GetIntrin128();
			*(pParticleVelDst+1) = ptxVelocity1.GetIntrin128();
			*(pParticlePosDst+2) = ptxPosition2.GetIntrin128();
			*(pParticleVelDst+2) = ptxVelocity2.GetIntrin128();
			*(pParticlePosDst+3) = ptxPosition3.GetIntrin128();
			*(pParticleVelDst+3) = ptxVelocity3.GetIntrin128();
			pParticlePosDst += 4;
			pParticleVelDst += 4;
	}//for(s32 xy=0; xy<_count; xy+=4)...
}
else // opt
{
	const Matrix44	gDepthViewProjMtx		= params->m_gDepthViewProjMtx;
	const Vector3	gBoxForward				= params->m_gBoxForward;
	const Vector3	gBoxRight				= params->m_gBoxRight;
	const Vector3	gBoxCentrePos			= params->m_gBoxCentrePos;

	// Use a constant up direction
	const Vector3	boxUp(0.0f, 0.0f, 1.0f);

	// Transforming particles from local to worls space
	const Vector3	gRefParticlePos			= params->m_gRefParticlePos;
	const Vector3	gOffsetParticlePos		= params->m_gOffsetParticlePos;

	const Vector4	gGravityWindTimeStepCut	= params->m_gGravityWindTimeStepCut;	// x: Gravity, y: Wind Influence, z: TimeStep, w: 0 after camera cut, 1 otherwise

	// emitter settings
	const Vector3	gBoxSize				= params->m_gBoxSize;
	const Vector3	gLifeMinMaxClampToGround= params->m_gLifeMinMaxClampToGround;
	const Vector3	gVelocityMin			= params->m_gVelocityMin;
	const Vector3	gVelocityMax			= params->m_gVelocityMax;

	// wind related
	const Vector4	gWindBaseVel			= RC_VECTOR4(params->m_gWindBaseVel);		// xyz: WindBaseVel, w: GameWindSpeed
	const Vector4	gWindGustVel			= RC_VECTOR4(params->m_gWindGustVel);		// xyz: WindGustVel, w: unused
	const Vector4	gWindVarianceVel		= RC_VECTOR4(params->m_gWindVarianceVel);	// xyz: WindVarianceVel, w: GameWindPositionalVarMult
	const Vector4	gWindMultParams			= RC_VECTOR4(params->m_gWindMultParams);	// x: GameWindGlobalVarSinePosMult, y: GameWindGlobalVarSineTimeMult
																						// z: GameWindGlobalVarCosinePosMult, w: GameWindGlobalVarCosineTimeMult


///////////////////////////////////////////////////////////////////////////////
// SAMPLERS
///////////////////////////////////////////////////////////////////////////////
	const Vector4*	gpParticlePosSrc = (Vector4*)params->m_ParticlePosLock[0].Base;
	Assert(gpParticlePosSrc);
	const Vector4*	gpParticleVelSrc = (Vector4*)params->m_ParticleVelLock[0].Base;
	Assert(gpParticleVelSrc);

	Vector4*		gpParticlePosDst = (Vector4*)params->m_ParticlePosLock[1].Base;
	Assert(gpParticlePosDst);
	Vector4*		gpParticleVelDst = (Vector4*)params->m_ParticleVelLock[1].Base;
	Assert(gpParticleVelDst);

#if RAIN_UPDATE_CPU_HEIGHTMAP
	float*			gpParticleCollisionSrc = (float*)params->m_HeightMapTextureLock.Base;
	Assert(gpParticleCollisionSrc);
#else
	float*			gpParticleCollisionSrc = NULL;
#endif

mthRandom	NoiseTexSampler;
	NoiseTexSampler.SetFullSeed(sysTimer::GetTicks());


Vector4*	pParticlePosSrc = (Vector4*)gpParticlePosSrc;
Vector4*	pParticleVelSrc = (Vector4*)gpParticleVelSrc;
Vector4*	pParticlePosDst = (Vector4*)gpParticlePosDst;
Vector4*	pParticleVelDst = (Vector4*)gpParticleVelDst;

	const s32 _count = RAIN_UPDATE_HEIGHT*RAIN_UPDATE_WIDTH;
	for(s32 xy=0; xy<_count; xy++)
	{
		///////////////////////////////////////////////////////////////////////////////
		// PS_ptxgpuUpdateGround

		// get the particle position and velocity 
		Vector4 ptxPosition = *(pParticlePosSrc++);
		Vector4 ptxVelocity = *(pParticleVelSrc++);

		// Transform the position to world space
		//ptxPosition.xyz += gRefParticlePos + gOffsetParticlePos;
		ptxPosition.x += gRefParticlePos.x + gOffsetParticlePos.x;
		ptxPosition.y += gRefParticlePos.y + gOffsetParticlePos.y;
		ptxPosition.z += gRefParticlePos.z + gOffsetParticlePos.z;

		Vector3 ptxStartPosition;
		ptxStartPosition.x = ptxPosition.x;
		ptxStartPosition.y = ptxPosition.y;
		ptxStartPosition.z = ptxPosition.z;
		
		// Position W contains the current particle life value
		float curLife = ptxPosition.w;
	
		// unpack the current and max life
		float initLife;
		float seed;
		UnPackVelW(ptxVelocity.w, gLifeMinMaxClampToGround.y, initLife, seed);

		// update the particle life
		curLife -= gGravityWindTimeStepCut.z;

		// check if the particle is dead
		if (curLife <= 0.0f)
		{
			// the particle is dead - we need to emit a new one

			// get some random data
			Vector4 fRandom1, fRandom2;
			fRandom1.x = NoiseTexSampler.GetFloat();
			fRandom2.x = NoiseTexSampler.GetFloat();
			fRandom1.y = NoiseTexSampler.GetFloat();
			fRandom2.y = NoiseTexSampler.GetFloat();
			fRandom1.z = NoiseTexSampler.GetFloat();
			fRandom2.z = NoiseTexSampler.GetFloat();
			fRandom1.w = NoiseTexSampler.GetFloat();
			fRandom2.w = NoiseTexSampler.GetFloat();

			// position
			//float3 emitRange = fRandom1.wzy-0.5f;
			Vector3 emitRange;
			emitRange.x = fRandom1.w - 0.5f;
			emitRange.y = fRandom1.z - 0.5f;
			emitRange.z = fRandom1.y - 0.5f;

			//ptxPosition.xyz = gBoxCentrePos;
			ptxPosition.x = gBoxCentrePos.x;
			ptxPosition.y = gBoxCentrePos.y;
			ptxPosition.z = gBoxCentrePos.z;

			//ptxPosition.xyz += gBoxRight * gBoxSize.x * emitRange.x;
			ptxPosition.x += gBoxRight.x * gBoxSize.x * emitRange.x;
			ptxPosition.y += gBoxRight.y * gBoxSize.x * emitRange.x;
			ptxPosition.z += gBoxRight.z * gBoxSize.x * emitRange.x;

			//ptxPosition.xyz += gBoxForward * gBoxSize.y * emitRange.y;
			ptxPosition.x += gBoxForward.x * gBoxSize.y * emitRange.y;
			ptxPosition.y += gBoxForward.y * gBoxSize.y * emitRange.y;
			ptxPosition.z += gBoxForward.z * gBoxSize.y * emitRange.y;
			
			//ptxPosition.xyz += boxUp * gBoxSize.z * emitRange.z;
			//ptxPosition.x += boxUp.x * gBoxSize.z * emitRange.z;
			//ptxPosition.y += boxUp.y * gBoxSize.z * emitRange.z;
			ptxPosition.z += boxUp.z * gBoxSize.z * emitRange.z;

			// fix to ground level
			Vector2 depthInfo1;
			DepthCompareHeightMap(depthInfo1, ptxPosition.x, ptxPosition.y, ptxPosition.z,
											gDepthViewProjMtx,
											gpParticleCollisionSrc);

			//float collisionZ = (depthInfo1.y - (gDepthViewProjMtx._43))/(gDepthViewProjMtx._33);
			float collisionZ = (depthInfo1.y - (gDepthViewProjMtx.d.z))/(gDepthViewProjMtx.c.z);
			ptxPosition.z = collisionZ;

			// velocity 
			//ptxVelocity.xyz = gVelocityMin + (gVelocityMax.xyz-gVelocityMin)*fRandom2.xyz;
			ptxVelocity.x = gVelocityMin.x + (gVelocityMax.x-gVelocityMin.x)*fRandom2.x;
			ptxVelocity.y = gVelocityMin.y + (gVelocityMax.y-gVelocityMin.y)*fRandom2.y;
			ptxVelocity.z = gVelocityMin.z + (gVelocityMax.z-gVelocityMin.z)*fRandom2.z;
		
			// life
			curLife = gLifeMinMaxClampToGround.x + (gLifeMinMaxClampToGround.y-gLifeMinMaxClampToGround.x)*fRandom1.w;
			initLife = FPMax(curLife, 0.0f);

			// seed
			seed = FPClamp(fRandom1.x - 0.004f, 0.0f, 1.0f); // Seed can't be one
		}

	    // update the particle position
		//ptxPosition.xyz += ptxVelocity.xyz * gGravityWindTimeStepCut.zzz;
		ptxPosition.x += ptxVelocity.x * gGravityWindTimeStepCut.z;
		ptxPosition.y += ptxVelocity.y * gGravityWindTimeStepCut.z;
		ptxPosition.z += ptxVelocity.z * gGravityWindTimeStepCut.z;

		// keep the particle inside the box
		Vector3 ptxPosPreBox;
		ptxPosPreBox.x = ptxPosition.x;
		ptxPosPreBox.y = ptxPosition.y;
		ptxPosPreBox.z = ptxPosition.z;

		Vector3 boxToPtx;
		boxToPtx.x = ptxPosition.x - gBoxCentrePos.x;
		boxToPtx.y = ptxPosition.y - gBoxCentrePos.y;
		boxToPtx.z = ptxPosition.z - gBoxCentrePos.z;

		float dotRight	= gBoxRight.Dot(boxToPtx);
		float dotForward= gBoxForward.Dot(boxToPtx);
	
	 	//ptxPosition.xyz -= gBoxRight * gBoxSize.x * ceil((dotRight/gBoxSize.x)-0.5f);
	 	ptxPosition.x -= gBoxRight.x * gBoxSize.x * FPCeil((dotRight/gBoxSize.x)-0.5f);
	 	ptxPosition.y -= gBoxRight.y * gBoxSize.x * FPCeil((dotRight/gBoxSize.x)-0.5f);
	 	ptxPosition.z -= gBoxRight.z * gBoxSize.x * FPCeil((dotRight/gBoxSize.x)-0.5f);

		//ptxPosition.xyz -= gBoxForward * gBoxSize.y * ceil((dotForward/gBoxSize.y)-0.5f);
		ptxPosition.x -= gBoxForward.x * gBoxSize.y * FPCeil((dotForward/gBoxSize.y)-0.5f);
		ptxPosition.y -= gBoxForward.y * gBoxSize.y * FPCeil((dotForward/gBoxSize.y)-0.5f);
		ptxPosition.z -= gBoxForward.z * gBoxSize.y * FPCeil((dotForward/gBoxSize.y)-0.5f);

		//float3 ptxPosMove = ptxPosition.xyz - ptxPosPreBox;
		Vector3 ptxPosMove;
		ptxPosMove.x = ptxPosition.x - ptxPosPreBox.x;
		ptxPosMove.y = ptxPosition.y - ptxPosPreBox.y;
		ptxPosMove.z = ptxPosition.z - ptxPosPreBox.z;

		bool ptxMoved = false;
		//if (dot(ptxPosMove.xyz, ptxPosMove.xyz) > 0.01)
		if (ptxPosMove.Dot(ptxPosMove) > 0.01f)
		{
			// During normal play - restore life to initial value
			// This will force a fade in and help avoid a pop
			// For camera cuts we basically don't want to do anything.
			curLife += (initLife - curLife) * gGravityWindTimeStepCut.w;
		
			ptxMoved = true;
		}

		// get the depth info for the new particle position (after keeping the particle within the box)
		Vector2 depthInfo2;
		DepthCompareHeightMap(depthInfo2, ptxPosition.x, ptxPosition.y, ptxPosition.z,
											gDepthViewProjMtx,
											gpParticleCollisionSrc);

		if (gLifeMinMaxClampToGround.z>0.0f)
		{
			//float collisionZ = (depthInfo2.y - (gDepthViewProjMtx._43))/(gDepthViewProjMtx._33);
			float collisionZ = (depthInfo2.y - (gDepthViewProjMtx.d.z))/(gDepthViewProjMtx.c.z);
			ptxPosition.z = collisionZ;
		}
		else if (depthInfo2.x < 0.1f || ptxMoved)
		{
			//float collisionZ = (depthInfo2.y - (gDepthViewProjMtx._43))/(gDepthViewProjMtx._33);
			float collisionZ = (depthInfo2.y - (gDepthViewProjMtx.d.z))/(gDepthViewProjMtx.c.z);

			if(gGravityWindTimeStepCut.w == 0.0)
			{
				ptxPosition.z = collisionZ;
			}
			else if (depthInfo2.y <= 0.0f)
			{
				// kill the particle if it's below the entire height map (below a z of zero)
				curLife = 0.0f;
			}
			else if (collisionZ - ptxPosition.z > 1.0f)
			{
				// reset the particle position if it's hit a barrier that's too high
				//ptxPosition.xyz = ptxStartPosition;
				ptxPosition.x = ptxStartPosition.x;
				ptxPosition.y = ptxStartPosition.y;
				ptxPosition.z = ptxStartPosition.z;
			}
			else
			{
				// Let the particle raise gradually
				ptxPosition.z = FPMin(ptxPosition.z + 0.05f, collisionZ);
			}
		}

	    // update the particle position (v += g * dt)
		//ptxVelocity.xyz += float3(0.0f, 0.0f, gGravityWindTimeStepCut.x) * gGravityWindTimeStepCut.zzz;
		ptxVelocity.z += gGravityWindTimeStepCut.x * gGravityWindTimeStepCut.z;

	#if PTXGPU_USE_WIND
		// update the particle's velocity with wind influence
		//ptxVelocity.xyz += ptxgpuApplyWindInfluence(ptxPosition.xyz, ptxVelocity.xyz, Input.texCoord0.xy);
		Vector3 windInfluence;
		ptxgpuApplyWindInfluence(windInfluence,		ptxPosition, ptxVelocity, NoiseTexSampler,
													gGravityWindTimeStepCut, gWindBaseVel, gWindGustVel, gWindVarianceVel, gWindMultParams);
		ptxVelocity.x += windInfluence.x;
		ptxVelocity.y += windInfluence.y;
		ptxVelocity.z += windInfluence.z;
	#endif

		// Transform particle position back to local space
		//ptxPosition.xyz -= gRefParticlePos;
		ptxPosition.x -= gRefParticlePos.x;
		ptxPosition.y -= gRefParticlePos.y;
		ptxPosition.z -= gRefParticlePos.z;
	
		// Position W contains the current particle life value
		ptxPosition.w = curLife;
	
		// pack the current and max life
		ptxVelocity.w = PackVelW(gLifeMinMaxClampToGround.y, initLife, seed);

		// pack the position and velocity
		*(pParticlePosDst++) = ptxPosition;
		*(pParticleVelDst++) = ptxVelocity;
	}//for(s32 y=0; y<RAIN_UPDATE_HEIGHT; y++)...
}// !opt...

}// end of _RainUpdateCpuTaskGround()...


//
//
//
//
static void RainUpdateCpuTask(sysTaskParameters &_params)
{
	ptxcpuUpdateBaseParams *UpdateParams = (ptxcpuUpdateBaseParams*)_params.UserData[0].asPtr; 
	Assert(UpdateParams);

	if(!UpdateParams)
		return;

	PIXBegin(1, "RainUpdateCpuTask");


	if(UpdateParams->m_HeightMapTextureLock.Base==NULL)
	{
		UpdateParams->m_HeightMapTextureLock.Base = &sm_defaultHeightMap[0];	// fallback in case no heightmap was locked
	}

	u32 updateWidth=0, updateHeight=0;
	switch(UpdateParams->m_gInstanceId)
	{
		case(DROP_SYSTEM_ID):
		{
			updateWidth = updateHeight = 128;
			Assert(updateWidth*updateHeight == UpdateParams->m_gNumParticles);
			_RainUpdateCpuTaskDrop(UpdateParams, updateWidth, updateHeight);
		}
		break;

		case(MIST_SYSTEM_ID):
		{
			updateWidth = updateHeight = 16;
			Assert(updateWidth*updateHeight == UpdateParams->m_gNumParticles);
			_RainUpdateCpuTaskDrop(UpdateParams, updateWidth, updateHeight);
		}
		break;

		case(GROUND_SYSTEM_ID):
		{
			updateWidth = updateHeight = 64;
			Assert(updateWidth*updateHeight == UpdateParams->m_gNumParticles);
			_RainUpdateCpuTaskGround(UpdateParams, updateWidth, updateHeight);
		}
		break;
	};


	PIXEnd();
}// end of RainUpdateCpuTask()...
#endif //RAIN_UPDATE_CPU...


#if RAIN_UPDATE_USES_STR_HEAP

const u32 nRainMainRamHeapSize	= (4*128) * 1024;
const u32 nRainVRamHeapSize		= 0;

static u8 *s_MainRamAlloc=0;
static u32 s_MainRamAllocSize=0;
static u8 *s_VRamAlloc=0;
static u32 s_VRamAllocSize=0;
ptxgpuBase::CRainStrHeap::CRainStrHeap(datResource &rsc)
{
	const datResourceMap &map = rsc.GetMap();
	s_MainRamAlloc	= (u8*)map.Chunks[0].DestAddr;	// should be the virtual pointer, remember it somewhere
	s_MainRamAllocSize= map.Chunks[0].Size;

	s_VRamAlloc		= (u8*)map.Chunks[1].DestAddr;	// should be the phys pointer, remember it somewhere
	s_VRamAllocSize	= map.Chunks[1].Size;
}

ptxgpuBase::CRainStrHeap::~CRainStrHeap()
{
	// pgBaseAllocated takes care of cleaning up.
}

/*
	TODO:
	When using dynamic allocatations from streaming heap:
	Allocate/FreeStreamingHeapBuffers() must be called EVERY frame from update thread
	(no matter if there's anything to really update or not), as this is the way to inform
	the rain system to grab (and fail silently if memory not available)
	or release (after doing rendering) memory buffers from the streaming heap.

Example:

void CPtFxGPU::Update(float deltaTime, bool bStrHeapFreeMem)
{	
	CPtFxGPU& ptFxGPUInterface = g_vfxWeather.GetPtFxGPUInterface();
	if(!ptFxGPUInterface.m_InitSuccess)
		return;

	// a bit hacky
	if(bStrHeapFreeMem)
	{
	#if RAIN_UPDATE_USES_STR_HEAP
		ptFxGPUInterface.m_ptxGPUParticle.FreeStreamingHeapBuffers();
	#endif
		return;	// nothing else to do - exit!
	}
	else
	{
	#if RAIN_UPDATE_USES_STR_HEAP
		ptFxGPUInterface.m_ptxGPUParticle.AllocateStreamingHeapBuffers();
	#endif
	}
	....
}
*/
//
//
// called by PtFxGPU when straming heap buffers required:
//
void ptxgpuBase::AllocateStreamingHeapBuffers()
{
	// only allowed from UpdateThread:
//	rdrThreadHelpers::VerifyCalledFromThread(rdrThreadHelpers::GetThread(kThreadMain), "ptxgpuBase::AllocateRainVRamBuffers()");

	// if buffers empty - try to allocate
	switch(m_RainHeap.GetState())
	{
		case(RAINHEAP_STATE_FREE):
		{
			m_RainHeap.SetState(RAINHEAP_STATE_ALLOCATING);
			m_RainStrHeap.ScheduleAndPlace(-10002.0f);
		}
		break;

		case(RAINHEAP_STATE_ALLOCATING):
		{
			if(m_RainStrHeap.GetState()==pgStreamableBase::RESIDENT)
			{
				// Mr Dave Etherton explains the trick himself:
				// "IOW if you tell it to allocate 512k virtual and 512k phys,
				// the GetObject pointer you get back will be to the virtual part,
				// containing your CRainStrHeap object at the start.
				// internally it contains a compressed map that remembers both the virtual
				// and physical parts so the memory can be properly reclaimed
				// well, as long as you copy the header aside after it's become resident,
				// and restore it before you call Unschedule, you should be okay
				// ugly as fuck, but i can't think of a reason why it wouldn't work"
					
				// HACK: save allocated CRainStrHeap object from streaming after it's allocated!
				// (don't want to allocate more than 512KB MainRam, as it allocates in power-of-2 chunks)
				if(nRainMainRamHeapSize)
					sysMemCpy(m_RainStrHeapCopy, (u8*)m_RainStrHeap.GetObject(), sizeof(CRainStrHeap));

				// 1. this is Virtual (MainRam) part of allocation:
				//m_RainHeap.SetMainRamHeapPtr( (u8*)m_RainStrHeap.GetObject() );	
				if(nRainMainRamHeapSize)
				{
					m_RainHeap.SetMainRamHeapPtr( s_MainRamAlloc );
					Assert(m_RainHeap.GetMainRamHeapPtr());
					Displayf("Rain: Allocated m_RainHeap.GetMainRamHeapPtr()=0x%X, allocSize: %d, wanted: %d",m_RainHeap.GetMainRamHeapPtr(),s_MainRamAllocSize,nRainMainRamHeapSize);
					Displayf("Rain: m_RainStrHeap.GetObject()=0x%X", (u8*)m_RainStrHeap.GetObject());	
				}

				if(nRainMainRamHeapSize)
				{
					// allocate MainRam Buffes now!
					CreateDestSurfaces(m_RainHeap.GetMainRamHeapPtr(), nRainMainRamHeapSize);
					InitParticles();

					// 360: this is called from UpdateThread, but is safe to do so, as Update() & Render() won't run until RAINHEAP_STATE_ALLOCATED is set
					m_updateShader->SetPositionAndVelocityTextures( m_destSurfaces[0], 
																	m_destSurfaces[1] );
				}
	
				CreateDummyVertexBuffers();

				m_RainHeap.SetState(RAINHEAP_STATE_ALLOCATED);
			}
			else
			{
				Displayf("Rain: Waiting for Rain VRam temp heap to become resident (%x).",m_RainStrHeap.GetState());
			}
		}
		break;

		case(RAINHEAP_STATE_ALLOCATED):
			break;
		case(RAINHEAP_STATE_FREEING):
			break;
		default:
			Assert(false);	// invalid state!
			break;
	}// end of switch(m_RainHeap.GetState()...

}// end of ptxgpuBase::AllocateStreamingHeapBuffers()...

//
//
// called by PtFxGPU when streaming heap buffers no longer needed:
//
void ptxgpuBase::FreeStreamingHeapBuffers()
{
	// only allowed from UpdateThread:
//	rdrThreadHelpers::VerifyCalledFromThread(rdrThreadHelpers::GetThread(kThreadMain), "ptxgpuBase::FreeRainVRamBuffers()");

	switch(m_RainHeap.GetState())
	{
		case(RAINHEAP_STATE_FREE):
			break;

		case(RAINHEAP_STATE_ALLOCATING):
		{
			m_RainHeap.SetState(RAINHEAP_STATE_FREE); // stop allocating immediately if there's no rain already
		}
		break;

		case(RAINHEAP_STATE_ALLOCATED):
		{
			m_RainHeap.SetFreeingFrameCounter(4);	// 4 frames delay
			m_RainHeap.SetState(RAINHEAP_STATE_FREEING);
		}
		break;

		case(RAINHEAP_STATE_FREEING):
		{
			m_RainHeap.m_freeingFrameCounter--;
			if(!m_RainHeap.GetFreeingFrameCounter())
			{
				if(nRainMainRamHeapSize)
				{
					DeleteDestSurfaces(m_RainHeap.GetMainRamHeapPtr(), nRainMainRamHeapSize);
					m_RainHeap.SetMainRamHeapPtr(NULL);

					// 360: this is called from UpdateThread, but is safe to do so, as Update() & Render() won't run until RAINHEAP_STATE_ALLOCATED is set
					m_updateShader->SetPositionAndVelocityTextures(NULL, NULL, NULL, NULL);
				}
				
				DeleteDummyVertexBuffers();
				Displayf("Rain: Freed streamable heap");

				// HACK: restore CRainStrHeap object where streaming expects it!
				if(nRainMainRamHeapSize)
					sysMemCpy((u8*)m_RainStrHeap.GetObject(), m_RainStrHeapCopy, sizeof(CRainStrHeap));

				m_RainStrHeap.Unschedule();

				m_RainHeap.SetState(RAINHEAP_STATE_FREE);
			}
		}
		break;
		
		default:
			Assert(false);	// invalid state!
			break;
	} // end of m_RainHeap.GetState()...

}// end of ptxgpuBase::FreeStreamingHeapBuffers()..


//
//
// to be called in ptxgpuBase::ptxgpuBase():
//
bool ptxgpuBase::StreamingMemoryInit()
{
	m_RainStrHeap.InitMem(nRainMainRamHeapSize, /*to be aligned to 128bytes*/nRainVRamHeapSize, TRACKING_BUCKET_PARTICLES);

	sysMemSet(&m_RainHeap, 0x00, sizeof(m_RainHeap));
	m_RainHeap.SetState(RAINHEAP_STATE_FREE);
	return true;
}

//
//
// to be called in ptxgpuBase::~ptxgpuBase():
//
bool ptxgpuBase::StreamingMemoryShutdown()
{
	if(m_RainStrHeap.GetState()==pgStreamableBase::NONRESIDENT)
	{
		m_RainStrHeap.Shutdown();
	}
	else
	{
		Displayf("\n Error closing RainStrHeap!");
	}

	return true;
}
#endif // RAIN_UPDATE_USES_STR_HEAP...

} // namespace rage

#endif	// !__RESOURCECOMPILER


