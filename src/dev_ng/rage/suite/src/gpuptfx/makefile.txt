Project gpuptfx
IncludePath $(RAGE_DIR)\base\src
Files {
	ptxgpubase.cpp
	ptxgpubase.h
	ptxgpudrop.cpp
	ptxgpudrop.h
}
Parse {
	ptxgpudrop
}
