// 
// rmptfx/ptxgpudrop.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
//

#ifndef RMPTFX_PTXGPUDROP_H 
#define RMPTFX_PTXGPUDROP_H 


// includes
#include "gpuptfx/ptxgpubase.h"


// namespace
namespace rage {


// forward declarations
class grcTexture;
class grcRenderTarget;
class grcViewport;


// classes

// PURPOSE
//  emitter settings for a gpu drop particle
struct ptxgpuDropEmitterSettings : public ptxgpuParseClass<ptxgpuDropEmitterSettings>
{ 
	// public functions
public:

	// PURPOSE
	//  constructor
	ptxgpuDropEmitterSettings();

	// PURPOSE
	//  interpolates between two drop emitter settings
	// PARAMS
	//  settings1 - the emitter settings to interpolate from
	//  settings2 - the emitter settings to interpolate to 
	//  t - how far to interpolate between the two (0.0->1.0)
	void Interpolate(const ptxgpuDropEmitterSettings& settings1, const ptxgpuDropEmitterSettings& settings2, float t);

	// PURPOSE
	//  interpolates to another drop emitter settings
	// PARAMS
	//  settings - the emitter settings to interpolate to
	//  t - how far to interpolate between the two (0.0->1.0)
	void Interpolate(const ptxgpuDropEmitterSettings& settings, float t);

	// PURPOSE
	//  calculates an approximate bounding volume around the effect
	//  this is approximate as the GPU does the particle calculate so it is only a guess
	void CalculateBound(float gravity, Vector3& min, Vector3& max);

	// public variables
public:

	Vector3 boxCentreOffset;			// the centre position of the box relative to the camera (z is always straight up)
	Vector3 boxSize;					// the size of the box in the x, y and z axes

	Vector2 lifeMinMax;					// the min and max values of the life of a drop particle, 
	Vector3 velocityMin;				// the minimum velocity of a drop particle
	Vector3 velocityMax;				// the maximum velocity of a drop particle
	bool clampToGround;					// whether to always clamp to the ground position
	bool disableParticleOnCollision; 	// if true, stops rendering particle after it collides with height map. If false, continues to render particle after it collides with height map

	PAR_PARSABLE;
};

// PURPOSE
//  render settings for a gpu drop particle
struct ptxgpuDropRenderSettings : public ptxgpuParseClass<ptxgpuDropRenderSettings>
{    
	// public functions
public:

	// PURPOSE
	//  constructor
	ptxgpuDropRenderSettings();

	// PURPOSE
	//  virtual destructor
	virtual ~ptxgpuDropRenderSettings() {Shutdown();}

	// PURPOSE
	//  releases any graphical resources
	void Shutdown();

	// PURPOSE
	//  interpolates between two drop render settings
	// PARAMS
	//  settings1 - the render settings to interpolate from
	//  settings2 - the render settings to interpolate to 
	//  t - how far to interpolate between the two (0.0->1.0)
	void Interpolate(const ptxgpuDropRenderSettings& settings1, const ptxgpuDropRenderSettings& settings2, float t);

	// PURPOSE
	//  interpolates to another drop render settings
	// PARAMS
	//  settings - the render settings to interpolate to 
	//  t - how far to interpolate between the two (0.0->1.0)
	void Interpolate(const ptxgpuDropRenderSettings& settings, float t);

	// public variables
public:
	grcTextureHandle pBackBufferTexture;			// handle to the back buffer for semi-transparency tricks
	grcTextureHandle pTexture;						// handle to the drop texture
	grcTextureHandle pDistotionTexture;				// handle to the drop distortion Texture for semi-transparency tricks
	Vector4	textureRowsColsStartEnd;				// xy: the number of rows and columns in the drop texture, zw: the normal start and end frame of the animation
	Vector4	textureAnimRateScaleOverLifeStart2End2;	// x: animation rate, y: whether to scale over life, zw: the alternative start and end frame of the animation
	Vector4 sizeMinMax;								// the min and max width and height of the drop particle
	Vector4 colour;									// the colour of the drop particle
	Vector2 fadeInOut;								// the time that a drop particle takes to fade in and out
	Vector2 fadeNearFar;							// the distances over which the particles fade as they get nearer or closer to the front and back of the emitter box
	Vector4 fadeGrdOffLoHi;							// 
	Vector2	rotSpeedMinMax;							// the min and max rotation speed of the drop particle
	Vector3 directionalZOffsetMinMax;				// x: non zero if the drop particle orientates itself in the velocity direction yz: min and max for random z offset in position
	Vector3 dirVelAddCamSpeedMinMaxMult;			// the min and max cam speeds and the mutliplier that gets applied when calculating the directional velocity add for making particles angle into the camera
	float	edgeSoftness;							// Particle softness factor
	Vector2 backgroundDisplacement;
	float	particleColorPercentage;
	float	backgroundDistortionVisibilityPercentage;
	float	backgroundDistortionAlphaBooster;
	float	backgroundDistortionAmount;
	int		backgroundDistortionBlurLevel;
	float	localLightsMultiplier;
	float	lightIntensityMultiplier;
	float	directionalLightShadowAmount;
	PAR_SIMPLE_PARSABLE;
};


// PURPOSE
//  update shader for the gpu particle drops
class ptxgpuDropUpdateShader : public ptxgpuUpdateBaseShader
{
public:

	// PURPOSE
	//  constructor
	ptxgpuDropUpdateShader(unsigned int id, const char* name) : ptxgpuUpdateBaseShader(id, name) 
	{ 
		SetupShader(); 
	}

	// PURPOSE
	//  sets the emitter for the update pass with the following render passes
	// PARAMS
	//  emitter - emitter settings
	//  gravity - downward gravity force
	//  windInfluence - how much the particles are influenced by the wind
	//  timeStep - frame delta
	//  cutValue - 0.0f on cut frame, otherwise 1.0f
	void SetEmitter(ptxgpuDropEmitterSettings& emitter, float gravity, float windInfluence, float timeStep, float cutValue);


	// protected functions
protected:

	// PURPOSE
	//  sets up the shader variables ids
	void SetupShader();


	// private variables
private:

	// shader variable ids
	grcEffectVar m_boxSizeID;
	grcEffectVar m_lifeMinMaxClampToGroundID;
	grcEffectVar m_velocityMinID;
	grcEffectVar m_velocityMaxID;
};

// PURPOSE
//  render shader for the gpu particle drops
class ptxgpuDropRenderShader : public ptxgpuRenderBaseShader
{	
	// public functions
public:

	// PURPOSE
	//  constructor
	ptxgpuDropRenderShader(const char* name) : ptxgpuRenderBaseShader(name) {SetupShader();}

	// PURPOSE
	//  sets the shader variables with values in the render settings
	// PARAMS
	//  renderSettings - the render settings to use to set this shader's variables
	void SetRenderSettings(const ptxgpuDropRenderSettings& renderSettings, Vec3V_In vDirVelAdd, float fMaxLife, const Vector3& refParticlePos, float camPosZ, float basePosZ BANK_ONLY(, Vec4V_In vCamAngleLimits));


	// protected functions
protected:

	// PURPOSE
	//  sets up the shader variables ids
	void SetupShader();


	// private variables
private:

	// shader variable ids
	grcEffectVar m_backBufferTextureID;
	grcEffectVar m_textureID;
	grcEffectVar m_distorionTextureID;
	grcEffectVar m_textureRowsColsStartEndID;
	grcEffectVar m_textureAnimRateScaleOverLifeStart2End2ID;
	grcEffectVar m_sizeMinRangeID;
	grcEffectVar m_colourID;
	grcEffectVar m_fadeInOutID;
	grcEffectVar m_fadeNearFarID;
	grcEffectVar m_fadeZBaseLoHiID;
	grcEffectVar m_rotSpeedMinRangeID;
	grcEffectVar m_directionalZOffsetMinRangeID;
	grcEffectVar m_DirectionalVelocityAddID;
	grcEffectVar m_edgeSoftnessID;
	grcEffectVar m_maxLifeID;
	grcEffectVar m_refParticlePosID;
	grcEffectVar m_particleColorPercentageID;
	grcEffectVar m_backgroundDistortionVisibilityPercentageID;
	grcEffectVar m_backgroundDistortionAlphaBoosterID;
	grcEffectVar m_backgroundDistortionAmountID;
	grcEffectVar m_localLightsMultiplierID;
	grcEffectVar m_directionalLightShadowAmountID;
	grcEffectVar m_lightIntensityMultId;
	BANK_ONLY(grcEffectVar m_camAngleLimitsID;)

};

// PURPOSE
//  drop implementation of a gpu particle
class ptxgpuDrop : public ptxgpuBase
{
	// public functions
public:

	// PURPOSE
	//  updates the gpu particle system
	//  this should not be called in normal game update phase but just before rendering the main scene
	//  a viewport is passed to allow for multi threaded rendering
	// PARAMS
	//  pViewport - pointer to the viewport to render the particles in
	//  deltaTime - current frame delta 
	//  cutValue - 0.0f on camera cut, otherwise 1.0f
	bool Update(grcViewport* pViewport,	float deltaTime, WindType_e windType, float ratioToRender, float cutValue, bool resetParticles);

	// PURPOSE
	//  renders the gpu particle system
	//	this should be in the transparent rendering portion of the frame
	// PARAMS
	//  pViewport - current viewport
	//  vCamVel - Camera velocity
	//  fMaxLife - Maximum particle life span
	//  pRenderSetup - render setup as in rmptfx to allow for setting game specific
	//  ratioToRender - ratio of particles to render (0.0f - no particles;  1.0f - all particles)
	virtual bool Render(const grcViewport* pViewport, Vec3V_In vCamVel, float fMaxLife, float camPosZ, float groundPosZ, ptxRenderSetup* pRenderSetup=NULL, float ratioToRender=1.0f BANK_ONLY(, Vec4V_In vCamAngleLimits = Vec4V(V_Y_AXIS_WONE)));
	
	// PURPOSE
	//  get the axis aligned bounding box that contains all the active emitters
	// PARAMS
	//  viewport - current viewport
	//  emitterMin - minimum emitter bound position in world space
	//  emitterMax - maximum emitter bound position in world space
	void GetEmitterBounds(Mat44V_ConstRef camMatrix, Vec3V_Ref emitterMin, Vec3V_Ref emitterMax);

#if __BANK
	void RenderDebug(const grcViewport& viewport, const Color32& color);
#endif // __BANK

	// PURPOSE
	//  sets the emitter to use with this particle system
	void SetEmitterSettings(const ptxgpuDropEmitterSettings& emitterSettings) {m_emitterSettings = emitterSettings;}
	void SetRenderSettings(const ptxgpuDropRenderSettings& renderSettings) {m_renderSettings = renderSettings;}

	// PURPOSE
	ptxgpuDropUpdateShader* GetUpdateShader() {return (ptxgpuDropUpdateShader*)m_pUpdateShader;}	
	ptxgpuDropRenderShader* GetRenderShader() {return (ptxgpuDropRenderShader*)m_pRenderShader;}


	// private variables
private:

	ptxgpuDropRenderSettings m_renderSettings;
    ptxgpuDropEmitterSettings m_emitterSettings;
};


} // namespace rage


#endif // RMPTFX_PTXGPUDROP_H 
