// 
// rmptfx/ptxgpuBase.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
//

#ifndef RMPTFX_PTXGPUBASE_H 
#define RMPTFX_PTXGPUBASE_H 


// defines
#define PTXGPU_USE_WIND_DISTURBANCE				(0)

#define PTXGPU_NUM_BUFFERS						(2)

#if PTXGPU_USE_WIND_DISTURBANCE
	#define PTXGPU_USE_WIND_DISTURBANCE_ONLY(x) x
#else
	#define PTXGPU_USE_WIND_DISTURBANCE_ONLY(x)
#endif


#define	RAIN_UPDATE_USES_STR_HEAP				(0)	// if 1, then Rain uses dynamically allocated buffers in streaming heap (in VRam), rather than statically allocated ones
#define	RAIN_UPDATE_CPU							(1 && RSG_PC)	// if 1, then RainUpdate is executed on CPU (rather than GPU)
#define RAIN_UPDATE_CPU_HEIGHTMAP				(1 && RAIN_UPDATE_CPU)

// includes
#include "atl/referencecounter.h"
#include "bank/bkmgr.h"
#include "file/limits.h"
#include "grcore/device.h"
#include "grcore/effect_typedefs.h"
#include "grcore/stateblock.h"
#include "grcore/texture.h"
#include "parser/manager.h"
#include "pheffects/wind.h"
#include "system/codecheck.h"
#include "system/task.h"
#include "system/timer.h"
#include "vector/vector3.h"
#include "vector/vector4.h"


// namespace
namespace rage {

enum ePtxGpuSystemId
{ 
	DROP_SYSTEM_ID = 0, 
	MIST_SYSTEM_ID, 
	GROUND_SYSTEM_ID,
	GPU_SYSTEMS_COUNT,
};

// forward declarations
class grcViewport;
class grmShader;
class ptxRenderSetup;


// classes

// PURPOSE
//   this adds windows style load, save and saveAs methods and widgets to a parseable class as done by the Parser
// REMARKS 
//   the class is derived from this using the curiously recurring template pattern and then
//   will have the load, save and save as functionality. Uses a compile time assert to force that the class
//   is parseable
template<class T>
class ptxgpuParseClass : public datBase
{
	// public functions
public:

	// PURPOSE
	//  constructor
	ptxgpuParseClass()
	{ 
		CompileTimeAssert(sizeof(T::parser_IsInheritable()) == sizeof(bool));
#if __BANK
		m_filename[0] = '\0'; 
#endif
	}

	// PURPOSE
	//  stores the filename for subsequent saves and loads the object using the parser
	void Load(const char* filename) 
	{
#if __BANK
		safecpy(m_filename, filename, RAGE_MAX_PATH);
#endif
		T* ptr = const_cast<T*>((const T*)this);
		PARSER.LoadObject(filename, "xml", *ptr);
	}

	// PURPOSE
	//  adds the widgets of the object and load/save and save as buttons if selected
	// PARAMS
	//  bank - current bank
	//  addLoadButtons - if true will add load/ save and saveAs widgets
#if __BANK
	void AddWidgets(bkBank& bank, bool addLoadButtons=false)
	{
		PARSER.AddWidgets(bank, (T*)this);
		if (addLoadButtons)
		{
			bank.AddButton("Load", datCallback(MFA(T::OnLoad), this));
			bank.AddButton("Save", datCallback(MFA(T::OnSave), this));
			bank.AddButton("SaveAs", datCallback(MFA(T::OnSaveAs), this));
		}
	}

	// PURPOSE
	//  saves the file using the given filename and the parser
	void Save(const char* fileName)
	{
		safecpy(m_filename, fileName, RAGE_MAX_PATH);
		PARSER.SaveObject(fileName, "xml", (T*)this);
	}

	// private functions
private:
	// PURPOSE
	//  widget callback which will bring up a file menu to select which file you want to load
	void OnLoad()
	{
		char fullPresetPath[RAGE_MAX_PATH];
		memset(fullPresetPath, 0, RAGE_MAX_PATH);

		if (BANKMGR.OpenFile(fullPresetPath, RAGE_MAX_PATH, "*.xml", false, "xml files"))
		{	
			Load(fullPresetPath);
		}
	}

	// PURPOSE
	//  widget callback which if will save if filename is known from previous 
	//  loading/saving or else it will bring up a save as menu
	void OnSave()
	{
		if (m_filename[0]=='\0')	
		{
			// do save as first time
			OnSaveAs();
		}
		else
		{
			Save(m_filename);
		}
	}

	// PURPOSE : widget callback which will bring up a dialog menu of where to save the file, blah blah
	void OnSaveAs()
	{
		char fullPresetPath[RAGE_MAX_PATH];
		memset(fullPresetPath, 0, RAGE_MAX_PATH);

		if( BANKMGR.OpenFile(fullPresetPath, RAGE_MAX_PATH, "*.xml", true, "xml files"))
		{
			Save(fullPresetPath);		
		}
	}

	// private variables
private:

	char m_filename[RAGE_MAX_PATH];


#endif // __BANK
};

// PURPOSE
//   base class to manage interface for shaders at a slightly higher level
// <GROUP gpuParticle>
class ptxgpuShader : public atReferenceCounter
{
	// public functions
protected:

	// creates a gpuShader wrapping the given shader name
	ptxgpuShader(const char* name) 
		: m_techniqueId(grcetNONE)
		, m_pass(0)
		BANK_ONLY(, m_techIdx(0))
	{ 
		Init(name); 
	}

public:
	bool BeginDraw();												// call before rendering with this shader
	void EndDraw();													// call after rendering with this shader
	void SetTechnique(const char* name);							// sets the technique for all subsequent frames
	void SetPass(int pass)					{ m_pass = pass; }		// sets the pass for all subsequent calls
	u32 GetHashName() const;
	void FullScreenBlit();											// renders a full screen quad with this shader
	grmShader* GetImp() {return m_pShader;}

#if __BANK
	void AddWidgets(bkBank& bank);
#endif
#if __D3D11 || RSG_ORBIS
	void SetPositionTextures(grcTexture* pPosXY, grcTexture* pPosZW);
#endif// __D3D11

	// protected functions
protected:

	bool Init(const char* name);
	
	virtual ~ptxgpuShader();
	virtual void ClearTextureReferences() {}
	virtual void SetupShader() = 0;

	// private functions
private:
#if __BANK
	void SetTechniqueFromList();
#endif

#if EFFECT_PRESERVE_STRINGS
public:
	const char *GetTechniqueName() const;
#endif //EFFECT_PRESERVE_STRINGS

	// protected variables
protected:
	grcEffectTechnique m_techniqueId;
	int m_pass;
	grmShader* m_pShader;
#if __D3D11 || RSG_ORBIS
	// We require these textures at both update and render time on DX11.
	grcEffectVar m_positionDestSurfacePosID;
	grcEffectVar m_positionDestSurfaceVelID;
#endif //__D3D11

	// private variables
private:
	ASSERT_ONLY(u32	m_prevFunc);			// for checked function order
	bool m_isActive;
	BANK_ONLY(int	m_techIdx);
};

// Base class for render shaders.
class ptxgpuRenderBaseShader : public ptxgpuShader
{
public:
	ptxgpuRenderBaseShader(const char *pName);
	~ptxgpuRenderBaseShader();

public:
	// Sets the particle texture size.
	void SetParticleTextureSize(u32 Width, u32 Height);

#if __D3D11 || RSG_ORBIS
private:
	grcEffectVar m_ParticleTextureSizeID;
#endif //__D3D11
};

#if RAIN_UPDATE_CPU
//
//
//
//
class ptxcpuUpdateBaseParams
{
public:
	Matrix44	m_gDepthViewProjMtx;//
	Vector3		m_gBoxForward;//
	Vector3		m_gBoxRight;//
	Vector3		m_gBoxCentrePos;//

	// Transforming particles from local to worls space
	Vector3		m_gRefParticlePos;//
	Vector3		m_gOffsetParticlePos;//

	Vector4		m_gGravityWindTimeStepCut;//	// x: Gravity, y: Wind Influence, z: TimeStep, w: 0 after camera cut, 1 otherwise

	// emitter settings
	Vector3		m_gBoxSize;//
	Vector3  	m_gLifeMinMaxClampToGround;//
	Vector3		m_gVelocityMin;//
	Vector3		m_gVelocityMax;//

	// wind related
	Vec4V		m_gWindBaseVel;//			// xyz: WindBaseVel, w: GameWindSpeed
	Vec4V		m_gWindGustVel;//			// xyz: WindGustVel, w: unused
	Vec4V		m_gWindVarianceVel;//		// xyz: WindVarianceVel, w: GameWindPositionalVarMult
	Vec4V		m_gWindMultParams;//		// x: GameWindGlobalVarSinePosMult, y: GameWindGlobalVarSineTimeMult
											// z: GameWindGlobalVarCosinePosMult, w: GameWindGlobalVarCosineTimeMult

	grcTextureLock	m_ParticlePosLock[2];//			// 0=src, 1=dst
	grcTextureLock	m_ParticleVelLock[2];//			// 0=src, 1=dst

	grcTextureLock	m_HeightMapTextureLock;

	float		m_gResetParticles;//
	u32			m_gInstanceId;
	u32			m_gNumParticles;
};
#endif //RAIN_UPDATE_CPU...


// PURPOSE
//  update shader for the gpu particle drops
class ptxgpuUpdateBaseShader : public ptxgpuShader
{
public:

	ptxgpuUpdateBaseShader(unsigned int id, const char* name) : ptxgpuShader(name) 
	{ 
		m_instanceId = id;
	}

	// PURPOSE
	//  sets the emitter bounding box values
	void SetEmitterTransform(const Matrix44& trans, const Vector3 &boxCentreOffset, const Vector3 &refPosition, const Vector3 &posOffset);

	// PURPOSE
	//  sets the actual position and velocity textures, on PS3/windows only two of these textures will be used
	void SetPositionAndVelocityTextures(grcTexture* pPos, grcTexture* pVel);
	
#if RAIN_UPDATE_CPU
	// PURPOSE
	//  sets the actual position and velocity textures
	bool SetPositionAndVelocitySrc(grcTexture* pPos, grcTexture* pVel);

	// PURPOSE
	//  sets the actual position and velocity targets
	bool SetPositionAndVelocityDst(grcTexture* pPos, grcTexture* pVel);

	// PURPOSE
	//  unsets the position and velocity textures
	void UnSetPositionAndVelocitySrc(grcTexture* pPos, grcTexture* pVel);

	// PURPOSE
	//  unsets the actual position and velocity targets
	void UnSetPositionAndVelocityDst(grcTexture* pPos, grcTexture* pVel);
#endif

	// PURPOSE
	//  sets the collision map to be used against the particles
	void SetHeightMap(const grcTexture* pHeightMap);

#if RAIN_UPDATE_CPU
	// PURPOSE
	//  sets the collision map to be used against the particles
	bool SetHeightMapSrc(const grcTexture* pHeightMap);

	// PURPOSE
	//  sets the collision map to be used against the particles
	void UnSetHeightMapSrc(const grcTexture* pHeightMap);
#endif

	// PURPOSE
	//  sets the transform matrix of the collision map
	void SetHeightMapTransformMtx(const Matrix44& depthViewProjMtx);

	// PURPOSE
	//  sets the noise map to be used for random calculations in the pixel shader
	void SetNoiseMap(const grcTexture* pNoiseMap);

	// PURPOSE
	//  sets the flag for whether or not the shader should reest the particles
	void SetResetParticles(bool shouldReset);

	// PURPOSE
	//  sets wind related variables
	void SetWindVariables(WindType_e windType);

	// PURPOSE
	//  sets wind disturbance field textures
	PTXGPU_USE_WIND_DISTURBANCE_ONLY(void SetWindDisturbanceTextures(grcTexture* pWindDistXY, grcTexture* pWindDistZ);)

	// PURPOSE
	//
	void SetupShader();

#if RAIN_UPDATE_CPU
	void						ResetCpuUpdateParams()					{ sysMemSet(&m_CpuUpdateParams, 0x00, sizeof(m_CpuUpdateParams));	}
//	ptxcpuUpdateBaseParams*		GetCpuUpdateParams()					{ return &m_CpuUpdateParams;										}
	ptxcpuUpdateBaseParams*		GetCpuUpdateParamsCopy()				{ sysMemCpy(&m_CpuUpdateParamsCopy, &m_CpuUpdateParams, sizeof(m_CpuUpdateParams)); return &m_CpuUpdateParamsCopy;	}
#endif

protected:

	// settings
	grcEffectVar m_gravityWindTimeStepCutID;
	grcEffectVar m_boxForwardID;
	grcEffectVar m_boxRightID;
	grcEffectVar m_boxCentrePosID;
	grcEffectVar m_refParticlePosID;
	grcEffectVar m_offsetParticlePosID;
	grcEffectVar m_resetParticlesID;				

#if !(__D3D11 || RSG_ORBIS)
	grcEffectVar m_positionDestSurfacePosID;
	grcEffectVar m_positionDestSurfaceVelID;
#endif //!__D3D11

	grcEffectVar m_heightMapID;
	grcEffectVar m_heightMapMtxID;

	grcEffectVar m_noiseTexID;

	// wind related 
	grcEffectVar m_windBaseVelID;		
	grcEffectVar m_windGustVelID;		
	grcEffectVar m_windVarianceVelID;	
	grcEffectVar m_windMultParamsID;

#if PTXGPU_USE_WIND_DISTURBANCE
	grcEffectVar m_windDistFieldXYTexID;
	grcEffectVar m_windDistFieldZTexID;
#endif // PTXGPU_USE_WIND_DISTURBANCE

	unsigned int m_instanceId;

#if RAIN_UPDATE_CPU
	ptxcpuUpdateBaseParams		m_CpuUpdateParams;
	ptxcpuUpdateBaseParams		m_CpuUpdateParamsCopy;		// instanced "working copy" for task
#endif
};

// PURPOSE
//  manages a simple GPU particle system
//  currently only setup for one at a time
// REMARKS
//  the gpu particle system works by using two render phases. The first one updates the position
//  and velocity values of the particle system by rendering to four textures ( 2 on PC / PS3 ) and reusing the
//  previous frames values. This allows all the physics to be calculated on the GPU. The second phase does
//  the rendering of the particles
// <FLAG Component>
// <GROUP gpuParticle>
class ptxgpuBase
{
	// public functions
public:

	ptxgpuBase();
	virtual ~ptxgpuBase();

	// PURPOSE
	//  initialize the gpu particle system and resources
	// PARAMS
	//  instanceId	 - unique id to identify this instance
	//  numParticles - maximum number of particles to render
	//  updateShader - shader which controls the updating phase
	//  renderShader - shader which controls the rendering phase
	//  pParentTarget - (optional) used to specify EDRAM offset of render targets on 360
	//  useHitPlane - 
	virtual bool Init(unsigned int instanceId, int numParticles, ptxgpuUpdateBaseShader* pUpdateShader, ptxgpuRenderBaseShader* pRenderShader, grcRenderTarget* pParentTarget = NULL, bool useHitPlane=false);

	// PURPOSE
	//  updates the particle system. This should not be called in normal game update phase but in
	//  a before rendering the main scene. Viewport is passed to allow for multi threaded rendering
	// PARAMS
	//  pViewport - pointer to the viewport to render the particles in
	//  deltaTime - current frame delta 
	//  windType - the wind type (air or water)
	virtual bool Update(grcViewport* pViewport,	float deltaTime, WindType_e windType, float ratioToRender, float cutValue, bool resetParticles) = 0;

	// PURPOSE
	//  will render with the given settings, this should be in the transparent rendering portion of the frame
	// PARAMS
	//  pViewport - current viewport
	//  vCamVel - Camera velocity
	//  fMaxLife - Maximum life of a particle
	//  pRenderSetup - render setup as in rmptfx to allow for setting game specific
	//  ratioToRender - ratio of particles to render (0.0f - no particles;  1.0f - all particles)
	virtual bool Render(const grcViewport* pViewport, Vec3V_In vCamVel, float fMaxLife, float camPosZ, float groundPosZ, ptxRenderSetup* pRenderSetup=NULL, float ratioToRender=1.0f BANK_ONLY(, Vec4V_In vCamAngleLimits = Vec4V(V_Y_AXIS_WONE))) = 0;

	// PURPOSE
	//  release all resources
	void Shutdown();

	// PURPOSE
	//  sets the gravity force
	void SetGravity(float gravity) {m_gravity = gravity;}	

	// PURPOSE
	//  sets the wind influence
	void SetWindInfluence(float windInfluence) {m_windInfluence = windInfluence;}	

	// PURPOSE
	int GetNumParticles() const {return m_numParticles;}

	void PauseSimulation() {m_isSimulationPaused = true;}

	// PURPOSE
	virtual ptxgpuUpdateBaseShader* GetUpdateShader() {return m_pUpdateShader;}

	// PURPOSE
	virtual ptxgpuShader* GetRenderShader() {return m_pRenderShader;}

	static	void InitRenderStateBlocks();

#if __BANK 
	// PURPOSE
	void AddWidgets( bkBank& bank );
#endif
#if RAIN_UPDATE_CPU
	static bool									CpuRainUpdateEnabled()		{ return sm_bRainUpdateOnCpu;	}
	static void									EnableCpuRainUpdate(bool b)	{ sm_bRainUpdateOnCpu = b;		}

	static bool									sm_bRainUpdateOnCpu;		// main switch
	#if __BANK
		static bool								sm_bBankRainUpdateOnCpu;	// __BANK-only switch
	#endif
#else
	static bool									CpuRainUpdateEnabled()		{ return false;	}
	static void									EnableCpuRainUpdate(bool)	{ /*do nothing*/}
#endif

	static void InitScheduler()					{ /*do nothing*/};

	bool IsDropInstance() const { return m_instanceId == DROP_SYSTEM_ID; }
	bool IsMistInstance() const { return m_instanceId == MIST_SYSTEM_ID; }
	bool IsGroundInstance() const { return m_instanceId == GROUND_SYSTEM_ID; }

	PTXGPU_USE_WIND_DISTURBANCE_ONLY(static void UpdateWindDisturbanceTextures();)

private:

	// PURPOSE
	//  will randomize the postion of the particles over the space of several frames
	//  this means the the cpu and shader can be decoupled.
	//  but it does mean that the gpu particle system cannot be displayed over these frames 
	void Randomize();


#if RAIN_UPDATE_USES_STR_HEAP
	void AllocateStreamingHeapBuffers();
	void FreeStreamingHeapBuffers();
#else
	void AllocateStreamingHeapBuffers()	{/*do nothing*/};
	void FreeStreamingHeapBuffers()		{/*do nothing*/};
#endif

	// protected functions
protected:

	bool							InitParticles();
	RETURNCHECK(grcDevice::Result)	CreateVertexBuffers(int numParticles);

#if PTXGPU_USE_WIND_DISTURBANCE
	static void						CreateWindDisturbanceTextures();
	static void						DeleteWindDisturbanceTextures();
#endif // PTXGPU_USE_WIND_DISTURBANCE

	static void						CreateNoiseTexture();

	static void						CalculateTransformedBoundBox(const Matrix34& mtx, Vector3& min, Vector3& max);

#if !(__D3D11 || RSG_ORBIS)
	void							CreateDestSurfaces(u8 *pHeapMemPtr, u32 HeapMemSize, grcRenderTarget* pParentTarget = NULL);
	void							DeleteDestSurfaces(u8 *pHeapMemPtr, u32 HeapMemSize);
#else //!__D3D11
	void							CreateTexturesAndRenderTargets();
	void							DeleteTexturesAndRenderTargets();
#endif //!__D3D11

#if !(__D3D11 || RSG_ORBIS)
	void							CreateDummyVertexBuffers();
	void							DeleteDummyVertexBuffers();
#endif //!__D3D11

	// protected variables
protected:

	int							m_numParticles;

	ptxgpuUpdateBaseShader*		m_pUpdateShader;
	ptxgpuRenderBaseShader*		m_pRenderShader;

#if __D3D11 || RSG_ORBIS
	typedef struct TargetsAndTextures
	{
		grcRenderTarget*			m_RTs[2];
		grcTexture *				m_Textures[2];
	} TargetsAndTextures;
	u32							m_SurfaceIndex;
	TargetsAndTextures			m_Surfaces[2];
#else //__D3D11
	grcRenderTarget*			m_destSurfaces[2];
#endif // __D3D11

	grcVertexBuffer*			m_pInstanceVertexBuffer;

	grcVertexBuffer*			m_dummyVertexBuffer[2];
	static grcTexture*			ms_pNoiseTexture;

	grcVertexDeclaration*		m_dummyVdecl;

	float						m_timeControl;
	float						m_updateDelta;
	int							m_initialRandomize;
	float						m_gravity;
	float						m_windInfluence;
	Vector3						m_RefPosition;
	bool						m_isSimulationPaused;

	unsigned int				m_instanceId;

	// private variables
private:
	
	static const int			m_numSurfaces;
	int							m_textureWidth;
	int							m_textureHeight;

#if __BANK
	bool						m_disableRendering;
#endif

#if RAIN_UPDATE_CPU
	sysTaskHandle				m_RainUpdateCpuTaskHandle;
#endif

#if RAIN_UPDATE_USES_STR_HEAP
	enum {
		RAINHEAP_STATE_FREE	= 0,
		RAINHEAP_STATE_ALLOCATING,
		RAINHEAP_STATE_ALLOCATED,
		RAINHEAP_STATE_FREEING
	};
	struct CRainHeap
	{
		void	SetState(u8 s)					{ m_state=s;									}
		u8		GetState()						{ return m_state;								}
		void	SetVRamHeapPtr(u8 *p)			{ m_pVRamHeap = (u8*)((u32(p)+127)&(~127));		} // align128
		u8*		GetVRamHeapPtr()				{ return m_pVRamHeap;							}
		void	SetMainRamHeapPtr(u8 *p)		{ m_pMainRamHeap = (u8*)((u32(p)+127)&(~127));	} // align128
		u8*		GetMainRamHeapPtr()				{ return m_pMainRamHeap;						}
		void	SetFreeingFrameCounter(u8 c)	{ m_freeingFrameCounter=c;						}
		u8 		GetFreeingFrameCounter()		{ return m_freeingFrameCounter;					}

		u8 *m_pVRamHeap;		// allocated heap ptr (NULL when invalid)
		u8 *m_pMainRamHeap;		// allocated heap ptr (NULL when invalid)
		u8 m_state;				// current state of the heap (RAINHEAP_STATE_...);
		u8 m_freeingFrameCounter;
		u8 m_dummy[2];
	};
	CRainHeap	m_RainHeap;
			
	struct CRainStrHeap: public pgBaseAllocated
	{ 
		CRainStrHeap(datResource&);
		~CRainStrHeap();
		IMPLEMENT_PLACE_INLINE(CRainStrHeap);
		int Release() { delete this; return 0; }
	};
	rage::pgStreamable<CRainStrHeap>	m_RainStrHeap;

	// HACK: space for keeping copy of streamed object after it's scheduled (allocated), it needs to be restored before Unschedule():
	char								m_RainStrHeapCopy[sizeof(CRainStrHeap)];

	bool	StreamingMemoryInit();
	bool	StreamingMemoryShutdown();
#endif // RAIN_UPDATE_USES_STR_HEAP...

public:

	static grcTexture*					ms_pWindDisturbanceTexture[2];

	static grcBlendStateHandle			ms_exitUpdateBlendState;
	static grcDepthStencilStateHandle	ms_exitUpdateDepthStencilState;
	static grcRasterizerStateHandle		ms_exitUpdateRasterizerState;
	
	static grcBlendStateHandle			ms_exitRenderBlendState;
	static grcDepthStencilStateHandle	ms_exitRenderDepthStencilState;
	static grcRasterizerStateHandle		ms_exitRenderRasterizerState;

	static grcBlendStateHandle			ms_updateBlendState;
	static grcDepthStencilStateHandle	ms_updateDepthStencilState;
	static grcRasterizerStateHandle		ms_updateRasterizerState;

	static grcBlendStateHandle			ms_renderBlendState;
	static grcDepthStencilStateHandle	ms_renderDepthStencilState;
	static grcRasterizerStateHandle		ms_renderRasterizerState;

	static grcDepthStencilStateHandle GetDepthStencilState()							{return ms_renderDepthStencilState;}
	static void SetDepthStencilState(grcDepthStencilStateHandle depthStencilState)		{ms_renderDepthStencilState = depthStencilState;}
};

} // namespace rage


#endif // RMPTFX_PTXGPUBASE_H 
