<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxgpuDropEmitterSettings">
	<Vector3 name="boxCentreOffset" min="-64.0f" max="64.0f" step="0.1f"/>
	<Vector3 name="boxSize" min="-256.0f" max="256.0f" step="0.1f"/>
	<Vector2 name="lifeMinMax" min="0.0f" max="64.0f" step="0.1f"/>
	<Vector3 name="velocityMin" min="-64.0f" max="64.0f" step="0.1f"/>
	<Vector3 name="velocityMax" min="-64.0f" max="64.0f" step="0.1f"/>
  <bool name="clampToGround"/>
</structdef>

<structdef type="rage::ptxgpuDropRenderSettings">
	<Vector4 name="textureRowsColsStartEnd" min="0.0f" max="100.0f" step="1.0f"/>
  <Vector4 name="textureAnimRateScaleOverLifeStart2End2" min="0.0f" max="100.0f" step="1.0f"/>
	<Vector4 name="sizeMinMax" min="0.0f" max="10.0f" step="0.1f"/>
	<Vector4 name="colour" min="0.0f" max="32.0f" step="0.1f"/>
	<Vector2 name="fadeInOut" min="0.0f" max="5.0f" step="0.1f"/>
	<Vector2 name="fadeNearFar" min="0.0f" max="1000.0f" step="1.0f"/>
  <Vector4 name="fadeGrdOffLoHi" min="-100.0f" max="100.0f" step="0.1f"/>
	<Vector2 name="rotSpeedMinMax" min="-64.0f" max="64.0f" step="0.1f"/>
	<Vector3 name="directionalZOffsetMinMax" min="-2.0f" max="2.0f" step="0.1f"/>
	<Vector3 name="dirVelAddCamSpeedMinMaxMult" min="0.0f" max="100.0f" step="0.05f"/>
  <float name="edgeSoftness" min="0.0f" max="1000.0f" step="0.05f"/>
  <float name= "particleColorPercentage" min="0.0f" max="1.0f" step="0.005f"/>
  <float name= "backgroundDistortionVisibilityPercentage" min="0.0f" max="1.0f" step="0.005f"/>
  <float name= "backgroundDistortionAlphaBooster" min="0.0f" max="100.0f" step="0.05f"/>
  <float name= "backgroundDistortionAmount" min="0.0f" max="1000.0f" step="0.05f"/>
  <int name= "backgroundDistortionBlurLevel" min="0" max="4" step="1"/>
  <float name= "localLightsMultiplier" min="0.0f" max="5.0f" step="0.05f"/>
  <float name= "directionalLightShadowAmount" min="0.0f" max="1.0f" step="0.001f" init="0.0f"/>
</structdef>

</ParserSchema>
