// 
// rmptfx/ptxgpudrop.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if !__RESOURCECOMPILER
#include "bank/bank.h"
#include "gpuptfx/ptxgpudrop.h"
#include "gpuptfx/ptxgpudrop_parser.h"
#include "grcore/indexbuffer.h"
#include "grcore/quads.h"
#include "grcore/texture.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grcore/viewport.h"
#include "grmodel/shaderfx.h"
#include "grmodel/modelfactory.h"
#include "math/random.h"
#include "rmptfx/ptxeffectinst.h"
#include "system/nelem.h"

#include "grcore/debugdraw.h"
#include "vector/colors.h"

// Crap - need these for the vertexDeclaration release
#if __WIN32
#include "system/xtl.h"
#include "system/d3d9.h"
#endif // __WIN32

//RMPTFX_OPTIMISATIONS()
//OPTIMISATIONS_OFF()


namespace rage {


// ptxgpuDropEmitterSettings

ptxgpuDropEmitterSettings::ptxgpuDropEmitterSettings()	
:	boxCentreOffset(0.0f, 10.0f, 0.0f), 
	boxSize(15.0f, 15.0f, 15.0f), 
	lifeMinMax(5.0f, 10.0f), 
	velocityMin(-0.5f, 0.5f, -1.0f), 
	velocityMax(-0.5f, 0.5f, 0.0f),
	clampToGround(false)
{
}

void ptxgpuDropEmitterSettings::Interpolate(const ptxgpuDropEmitterSettings& settings1, const ptxgpuDropEmitterSettings& settings2, float t)
{
	boxCentreOffset.x = settings1.boxCentreOffset.x + (t*(settings2.boxCentreOffset.x-settings1.boxCentreOffset.x));
	boxCentreOffset.y = settings1.boxCentreOffset.y + (t*(settings2.boxCentreOffset.y-settings1.boxCentreOffset.y));
	boxCentreOffset.z = settings1.boxCentreOffset.z + (t*(settings2.boxCentreOffset.z-settings1.boxCentreOffset.z));

	boxSize.x = settings1.boxSize.x + (t*(settings2.boxSize.x-settings1.boxSize.x));
	boxSize.y = settings1.boxSize.y + (t*(settings2.boxSize.y-settings1.boxSize.y));
	boxSize.z = settings1.boxSize.z + (t*(settings2.boxSize.z-settings1.boxSize.z));

	lifeMinMax.x = settings1.lifeMinMax.x + (t*(settings2.lifeMinMax.x-settings1.lifeMinMax.x));
	lifeMinMax.y = settings1.lifeMinMax.y + (t*(settings2.lifeMinMax.y-settings1.lifeMinMax.y));

	velocityMin.x = settings1.velocityMin.x + (t*(settings2.velocityMin.x-settings1.velocityMin.x));
	velocityMin.y = settings1.velocityMin.y + (t*(settings2.velocityMin.y-settings1.velocityMin.y));
	velocityMin.z = settings1.velocityMin.z + (t*(settings2.velocityMin.z-settings1.velocityMin.z));

	velocityMax.x = settings1.velocityMax.x + (t*(settings2.velocityMax.x-settings1.velocityMax.x));
	velocityMax.y = settings1.velocityMax.y + (t*(settings2.velocityMax.y-settings1.velocityMax.y));
	velocityMax.z = settings1.velocityMax.z + (t*(settings2.velocityMax.z-settings1.velocityMax.z));

	clampToGround = settings1.clampToGround || settings2.clampToGround;
}

void ptxgpuDropEmitterSettings::Interpolate(const ptxgpuDropEmitterSettings& settings, float t)
{
	boxCentreOffset.x = boxCentreOffset.x + (t*(settings.boxCentreOffset.x-boxCentreOffset.x));
	boxCentreOffset.y = boxCentreOffset.y + (t*(settings.boxCentreOffset.y-boxCentreOffset.y));
	boxCentreOffset.z = boxCentreOffset.z + (t*(settings.boxCentreOffset.z-boxCentreOffset.z));

	boxSize.x = boxSize.x + (t*(settings.boxSize.x-boxSize.x));
	boxSize.y = boxSize.y + (t*(settings.boxSize.y-boxSize.y));
	boxSize.z = boxSize.z + (t*(settings.boxSize.z-boxSize.z));

	lifeMinMax.x = lifeMinMax.x + (t*(settings.lifeMinMax.x-lifeMinMax.x));
	lifeMinMax.y = lifeMinMax.y + (t*(settings.lifeMinMax.y-lifeMinMax.y));

	velocityMin.x =	velocityMin.x + (t*(settings.velocityMin.x-velocityMin.x));
	velocityMin.y =	velocityMin.y + (t*(settings.velocityMin.y-velocityMin.y));
	velocityMin.z =	velocityMin.z + (t*(settings.velocityMin.z-velocityMin.z));

	velocityMax.x =	velocityMax.x + (t*(settings.velocityMax.x-velocityMax.x));
	velocityMax.y =	velocityMax.y + (t*(settings.velocityMax.y-velocityMax.y));
	velocityMax.z =	velocityMax.z + (t*(settings.velocityMax.z-velocityMax.z));

	clampToGround = settings.clampToGround;
}


void ptxgpuDropEmitterSettings::CalculateBound(float gravity, Vector3& min, Vector3& max)
{
	// allow for velocity over life
	Vector3 speed = velocityMin + Vector3(0.0f, 0.0f, gravity);
	Vector3 dirMax = speed + velocityMax;
	Vector3 dirMin = speed - velocityMax;

	float life = (lifeMinMax.x + lifeMinMax.y);

	Vector3 potentialMin = boxCentreOffset + dirMin*life;
	Vector3 potentialMax = boxCentreOffset + boxSize + dirMax*life;

	min.Min(boxCentreOffset, potentialMin);
	max.Max(boxCentreOffset+boxSize, potentialMax);
}


// ptxgpuDropRenderSettings

ptxgpuDropRenderSettings::ptxgpuDropRenderSettings() 
:	textureRowsColsStartEnd(1.0f, 1.0f, 0.0f, 0.0f),
	textureAnimRateScaleOverLifeStart2End2(0.0f, 0.0f, 0.0f, 0.0f),
	sizeMinMax(0.1f, 0.1f, 0.1f, 0.1f),
	colour(1.0f, 1.0f, 1.0f, 0.5f),
	fadeInOut(FLT_MIN, FLT_MIN),
	fadeNearFar(FLT_MIN, 1000.0f),
	fadeGrdOffLoHi(0.0f, -50.0f, 50.0f, 100.0f),
	rotSpeedMinMax(0.0f, 0.0f),
	directionalZOffsetMinMax(0.0f, 0.0f, 0.0f),
	dirVelAddCamSpeedMinMaxMult(0.0f, 0.0f, 0.0f),
	edgeSoftness(1.0f),
	particleColorPercentage(1.0f),
	backgroundDistortionVisibilityPercentage(0.0f),
	backgroundDistortionAlphaBooster(1.0f),
	backgroundDistortionAmount(1.0f),
	backgroundDistortionBlurLevel(0),
	localLightsMultiplier(0.8f),
	lightIntensityMultiplier(0.8f),
	directionalLightShadowAmount(0.0f),
	pDistotionTexture(NULL),
	pTexture(NULL),
	pBackBufferTexture(NULL)
{
}

void ptxgpuDropRenderSettings::Interpolate(const ptxgpuDropRenderSettings& settings1, const ptxgpuDropRenderSettings& settings2, float t)
{
	// check some settings are equal as we can't blend between these
	Assertf(settings1.pTexture==settings2.pTexture, "can't interpolate between 2 different diffuse textures");
	Assertf(settings1.pDistotionTexture==settings2.pDistotionTexture, "can't interpolate between 2 different distortion textures");
	Assertf(settings1.textureRowsColsStartEnd.x==settings2.textureRowsColsStartEnd.x && settings1.textureRowsColsStartEnd.y==settings2.textureRowsColsStartEnd.y, "can't interpolate between 2 textures with a different number of row and colums");
	Assertf(settings1.directionalZOffsetMinMax.x==settings2.directionalZOffsetMinMax.x, "can't interpolate between 2 different directional settings");

	// copy over these settings
	pTexture = settings1.pTexture;
	pDistotionTexture = settings1.pDistotionTexture;
	textureRowsColsStartEnd = settings1.textureRowsColsStartEnd;
	textureAnimRateScaleOverLifeStart2End2 = settings1.textureAnimRateScaleOverLifeStart2End2;
	directionalZOffsetMinMax = settings1.directionalZOffsetMinMax;

	// interpolate the remaining settings
	sizeMinMax.x = settings1.sizeMinMax.x + (t*(settings2.sizeMinMax.x-settings1.sizeMinMax.x));
	sizeMinMax.y = settings1.sizeMinMax.y + (t*(settings2.sizeMinMax.y-settings1.sizeMinMax.y));
	sizeMinMax.z = settings1.sizeMinMax.z + (t*(settings2.sizeMinMax.z-settings1.sizeMinMax.z));
	sizeMinMax.w = settings1.sizeMinMax.w + (t*(settings2.sizeMinMax.w-settings1.sizeMinMax.w));

	colour.x = settings1.colour.x + (t*(settings2.colour.x-settings1.colour.x));
	colour.y = settings1.colour.y + (t*(settings2.colour.y-settings1.colour.y));
	colour.z = settings1.colour.z + (t*(settings2.colour.z-settings1.colour.z));
	colour.w = settings1.colour.w + (t*(settings2.colour.w-settings1.colour.w));

	// Never set fade values to zero to avoid NaNs in the shader
	fadeInOut.x = Max(FLT_MIN, settings1.fadeInOut.x + (t*(settings2.fadeInOut.x-settings1.fadeInOut.x)));
	fadeInOut.y = Max(FLT_MIN, settings1.fadeInOut.y + (t*(settings2.fadeInOut.y-settings1.fadeInOut.y)));

	fadeNearFar.x = Max(FLT_MIN, settings1.fadeNearFar.x + (t*(settings2.fadeNearFar.x-settings1.fadeNearFar.x)));
	fadeNearFar.y = Max(FLT_MIN, settings1.fadeNearFar.y + (t*(settings2.fadeNearFar.y-settings1.fadeNearFar.y)));

	rotSpeedMinMax.x = settings1.rotSpeedMinMax.x + (t*(settings2.rotSpeedMinMax.x-settings1.rotSpeedMinMax.x));
	rotSpeedMinMax.y = settings1.rotSpeedMinMax.y + (t*(settings2.rotSpeedMinMax.y-settings1.rotSpeedMinMax.y));

	dirVelAddCamSpeedMinMaxMult.x = settings1.dirVelAddCamSpeedMinMaxMult.x + (t*(settings2.dirVelAddCamSpeedMinMaxMult.x-settings1.dirVelAddCamSpeedMinMaxMult.x));
	dirVelAddCamSpeedMinMaxMult.y = settings1.dirVelAddCamSpeedMinMaxMult.y + (t*(settings2.dirVelAddCamSpeedMinMaxMult.y-settings1.dirVelAddCamSpeedMinMaxMult.y));
	dirVelAddCamSpeedMinMaxMult.z = settings1.dirVelAddCamSpeedMinMaxMult.z + (t*(settings2.dirVelAddCamSpeedMinMaxMult.z-settings1.dirVelAddCamSpeedMinMaxMult.z));

	particleColorPercentage = settings1.particleColorPercentage + (t*(settings2.particleColorPercentage-settings1.particleColorPercentage));
	backgroundDistortionVisibilityPercentage = settings1.backgroundDistortionVisibilityPercentage + (t*(settings2.backgroundDistortionVisibilityPercentage-settings1.backgroundDistortionVisibilityPercentage));

	backgroundDistortionAlphaBooster = settings1.backgroundDistortionAlphaBooster + (t*(settings2.backgroundDistortionAlphaBooster-settings1.backgroundDistortionAlphaBooster));
	backgroundDistortionAmount = settings1.backgroundDistortionAmount + (t*(settings2.backgroundDistortionAmount-settings1.backgroundDistortionAmount));
	backgroundDistortionBlurLevel = settings1.backgroundDistortionBlurLevel + ((int)t*(settings2.backgroundDistortionBlurLevel-settings1.backgroundDistortionBlurLevel));
	localLightsMultiplier = settings1.localLightsMultiplier + (t*(settings2.localLightsMultiplier-settings1.localLightsMultiplier));
	directionalLightShadowAmount = settings1.directionalLightShadowAmount + (t*(settings2.directionalLightShadowAmount-settings1.directionalLightShadowAmount));
}

void ptxgpuDropRenderSettings::Interpolate(const ptxgpuDropRenderSettings& settings, float t)
{
	// We disable those asserts : while valid, we're using this in a specific settings which shouldn't create any issues.
	// check some settings are equal as we can't blend between these
	//Assertf(pTexture==settings.pTexture, "can't interpolate between 2 different diffuse textures");
	//Assertf(textureRowsColsAnim.x==settings2.textureRowsColsAnim.x && textureRowsColsAnim.y==settings.textureRowsColsAnim.y, "can't interpolate between 2 textures with a different number of row and colums");
	//Assertf(directionalZOffsetMinMax.x==settings.directionalZOffsetMinMax.x, "can't interpolate between 2 different directional settings");

	// interpolate the remaining settings
	sizeMinMax.x = sizeMinMax.x + (t*(settings.sizeMinMax.x-sizeMinMax.x));
	sizeMinMax.y = sizeMinMax.y + (t*(settings.sizeMinMax.y-sizeMinMax.y));
	sizeMinMax.z = sizeMinMax.z + (t*(settings.sizeMinMax.z-sizeMinMax.z));
	sizeMinMax.w = sizeMinMax.w + (t*(settings.sizeMinMax.w-sizeMinMax.w));

	colour.x = colour.x + (t*(settings.colour.x-colour.x));
	colour.y = colour.y + (t*(settings.colour.y-colour.y));
	colour.z = colour.z + (t*(settings.colour.z-colour.z));
	colour.w = colour.w + (t*(settings.colour.w-colour.w));

	fadeInOut.x = fadeInOut.x + (t*(settings.fadeInOut.x-fadeInOut.x));
	fadeInOut.y = fadeInOut.y + (t*(settings.fadeInOut.y-fadeInOut.y));

	fadeNearFar.x = fadeNearFar.x + (t*(settings.fadeNearFar.x-fadeNearFar.x));
	fadeNearFar.y = fadeNearFar.y + (t*(settings.fadeNearFar.y-fadeNearFar.y));

	rotSpeedMinMax.x = rotSpeedMinMax.x + (t*(settings.rotSpeedMinMax.x-rotSpeedMinMax.x));
	rotSpeedMinMax.y = rotSpeedMinMax.y + (t*(settings.rotSpeedMinMax.y-rotSpeedMinMax.y));

	dirVelAddCamSpeedMinMaxMult.x = dirVelAddCamSpeedMinMaxMult.x + (t*(settings.dirVelAddCamSpeedMinMaxMult.x-dirVelAddCamSpeedMinMaxMult.x));
	dirVelAddCamSpeedMinMaxMult.y = dirVelAddCamSpeedMinMaxMult.y + (t*(settings.dirVelAddCamSpeedMinMaxMult.y-dirVelAddCamSpeedMinMaxMult.y));
	dirVelAddCamSpeedMinMaxMult.z = dirVelAddCamSpeedMinMaxMult.z + (t*(settings.dirVelAddCamSpeedMinMaxMult.z-dirVelAddCamSpeedMinMaxMult.z));

	particleColorPercentage = particleColorPercentage + (t*(settings.particleColorPercentage-particleColorPercentage));
	backgroundDistortionVisibilityPercentage = backgroundDistortionVisibilityPercentage + (t*(settings.backgroundDistortionVisibilityPercentage-backgroundDistortionVisibilityPercentage));

	backgroundDistortionAlphaBooster = backgroundDistortionAlphaBooster + (t*(settings.backgroundDistortionAlphaBooster-backgroundDistortionAlphaBooster));
	backgroundDistortionAmount = backgroundDistortionAmount + (t*(settings.backgroundDistortionAmount-backgroundDistortionAmount));
	backgroundDistortionBlurLevel = backgroundDistortionBlurLevel + ((int)t*(settings.backgroundDistortionBlurLevel-backgroundDistortionBlurLevel));
	localLightsMultiplier = localLightsMultiplier + (t*(settings.localLightsMultiplier-localLightsMultiplier));
	directionalLightShadowAmount = directionalLightShadowAmount + (t*(settings.directionalLightShadowAmount-directionalLightShadowAmount));
}

void ptxgpuDropRenderSettings::Shutdown()
{
	if (pTexture && !pTexture->Release()) 
	{
		pTexture = NULL;
	}

	if(pDistotionTexture && !pDistotionTexture->Release())
	{
		pDistotionTexture = NULL;
	}
}


// ptxgpuDropUpdateShader

void ptxgpuDropUpdateShader::SetupShader()
{
	Assert(m_pShader);

	ptxgpuUpdateBaseShader::SetupShader();

	m_boxSizeID = m_pShader->LookupVar("BoxSize"); 

	m_lifeMinMaxClampToGroundID = m_pShader->LookupVar("LifeMinMaxClampToGround"); 
	m_velocityMinID = m_pShader->LookupVar("VelocityMin"); 
	m_velocityMaxID = m_pShader->LookupVar("VelocityMax"); 
}

void ptxgpuDropUpdateShader::SetEmitter(ptxgpuDropEmitterSettings& emitter, float gravity, float windInfluence, float timeStep, float cutValue)
{
	m_pShader->SetVar(m_gravityWindTimeStepCutID, Vector4(gravity, windInfluence, timeStep, cutValue));
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gGravityWindTimeStepCut.Set(gravity, windInfluence, timeStep, cutValue);
#endif

	m_pShader->SetVar(m_boxSizeID, emitter.boxSize);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gBoxSize = emitter.boxSize;
#endif

	m_pShader->SetVar(m_lifeMinMaxClampToGroundID, Vector3(emitter.lifeMinMax.x, emitter.lifeMinMax.y, emitter.clampToGround ? 1.0f : 0.0f));
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gLifeMinMaxClampToGround.Set(emitter.lifeMinMax.x, emitter.lifeMinMax.y, emitter.clampToGround ? 1.0f : 0.0f);
#endif

	m_pShader->SetVar(m_velocityMinID, emitter.velocityMin);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gVelocityMin = emitter.velocityMin;
#endif

	m_pShader->SetVar(m_velocityMaxID, emitter.velocityMax);
#if RAIN_UPDATE_CPU
	m_CpuUpdateParams.m_gVelocityMax = emitter.velocityMax;
#endif
}

// ptxgpuDropRenderShader

void ptxgpuDropRenderShader::SetupShader()
{
	m_backBufferTextureID = m_pShader->LookupVar("BackBufferTexture");
	m_textureID = m_pShader->LookupVar("DiffuseTex");
	m_distorionTextureID = m_pShader->LookupVar("DistortionTexture");
	m_textureRowsColsStartEndID = m_pShader->LookupVar("TextureRowsColsStartEnd");
	m_textureAnimRateScaleOverLifeStart2End2ID = m_pShader->LookupVar("TextureAnimRateScaleOverLifeStart2End2");
	m_sizeMinRangeID = m_pShader->LookupVar("SizeMinRange");
	m_colourID = m_pShader->LookupVar("Colour");
	m_fadeInOutID = m_pShader->LookupVar("FadeInOut");
	m_fadeNearFarID = m_pShader->LookupVar("FadeNearFar");
	m_fadeZBaseLoHiID = m_pShader->LookupVar("FadeZBaseLoHi");
	m_rotSpeedMinRangeID = m_pShader->LookupVar("RotSpeedMinRange");
	m_directionalZOffsetMinRangeID = m_pShader->LookupVar("DirectionalZOffsetMinRange");
	m_DirectionalVelocityAddID = m_pShader->LookupVar("DirectionalVelocityAdd");
	m_edgeSoftnessID = m_pShader->LookupVar("EdgeSoftness", false); // Only used by ground particles
	m_maxLifeID = m_pShader->LookupVar("MaxLife");
	m_refParticlePosID = m_pShader->LookupVar("RefParticlePos");
	m_particleColorPercentageID = m_pShader->LookupVar("ParticleColorPercentage");
	m_backgroundDistortionVisibilityPercentageID = m_pShader->LookupVar("BackgroundDistortionVisibilityPercentage");
	m_backgroundDistortionAlphaBoosterID = m_pShader->LookupVar("BackgroundDistortionAlphaBooster");
	m_backgroundDistortionAmountID = m_pShader->LookupVar("BackgroundDistortionAmount");
	m_localLightsMultiplierID = m_pShader->LookupVar("LocalLightsMultiplier");
	m_directionalLightShadowAmountID = m_pShader->LookupVar("DirectionalLightShadowAmount");
	m_lightIntensityMultId = m_pShader->LookupVar("LightIntensityMult");
	BANK_ONLY(m_camAngleLimitsID = m_pShader->LookupVar("CamAngleLimits");)
}

void ptxgpuDropRenderShader::SetRenderSettings(const ptxgpuDropRenderSettings& renderSettings, Vec3V_In vDirVelAdd, float fMaxLife, const Vector3& refParticlePos, float camPosZ, float groundPosZ BANK_ONLY(, Vec4V_In vCamAngleLimits))
{
	m_pShader->SetVar(m_backBufferTextureID, renderSettings.pBackBufferTexture);

	m_pShader->SetVar(m_textureID, renderSettings.pTexture);
	m_pShader->SetVar(m_distorionTextureID, renderSettings.pDistotionTexture);
	m_pShader->SetVar(m_textureRowsColsStartEndID, renderSettings.textureRowsColsStartEnd);
	m_pShader->SetVar(m_textureAnimRateScaleOverLifeStart2End2ID, renderSettings.textureAnimRateScaleOverLifeStart2End2);

	// Pass the size in a more GPU friendly version
	Vec4V vSizeMinRange = Vec4V(renderSettings.sizeMinMax.x, renderSettings.sizeMinMax.y, renderSettings.sizeMinMax.z - renderSettings.sizeMinMax.x, renderSettings.sizeMinMax.w - renderSettings.sizeMinMax.y);
	m_pShader->SetVar(m_sizeMinRangeID, vSizeMinRange);

	// Convert color to linear space
	Vector4 colour = Vector4(renderSettings.colour.x * renderSettings.colour.x, renderSettings.colour.y * renderSettings.colour.y, renderSettings.colour.z * renderSettings.colour.z, renderSettings.colour.w);
	m_pShader->SetVar(m_colourID, colour);

	// fade over life of particle
	const float smallFloat = 1.0e-5f;
	Assertf(renderSettings.fadeInOut.x>=0.0f, "GPU fade in is negative");
	Assertf(renderSettings.fadeInOut.y>=0.0f, "GPU fade out is negative");
	Vec2V fadeInOut = Vec2V(1.0f/(renderSettings.fadeInOut.x+smallFloat), 1.0f/(renderSettings.fadeInOut.y+smallFloat));
	m_pShader->SetVar(m_fadeInOutID, fadeInOut);
	
	// fade over distance from camera
	Assertf(renderSettings.fadeNearFar.x>=0.0f, "GPU fade near is negative");
	Assertf(renderSettings.fadeNearFar.y>=0.0f, "GPU fade far is negative");
	Vec2V vFadeNearFar(1.0f/(renderSettings.fadeNearFar.x+smallFloat), 1.0f/(renderSettings.fadeNearFar.y+smallFloat));
	m_pShader->SetVar(m_fadeNearFarID, vFadeNearFar);

	// fade over height
	float basePosZ = camPosZ;
	if (renderSettings.fadeGrdOffLoHi.x>0.0f)
	{
		basePosZ = groundPosZ;
	}
	basePosZ += renderSettings.fadeGrdOffLoHi.y;
	Assertf(renderSettings.fadeGrdOffLoHi.z>=0.0f, "GPU fade lo is negative");
	Assertf(renderSettings.fadeGrdOffLoHi.w>=0.0f, "GPU fade hi is negative");
	Vec3V vFadePosZ(basePosZ, 1.0f/(renderSettings.fadeGrdOffLoHi.z+smallFloat), 1.0f/(renderSettings.fadeGrdOffLoHi.w+smallFloat));
	m_pShader->SetVar(m_fadeZBaseLoHiID, vFadePosZ);

	Vec2V vRotSpeedMinRange = Vec2V(renderSettings.rotSpeedMinMax.x, renderSettings.rotSpeedMinMax.y - renderSettings.rotSpeedMinMax.x);
	m_pShader->SetVar(m_rotSpeedMinRangeID, vRotSpeedMinRange);

	// Pass the offset in a more GPU friendly version
	Vec3V vDirZOffsetMinRange = Vec3V(renderSettings.directionalZOffsetMinMax.x, renderSettings.directionalZOffsetMinMax.y * 0.5f, (renderSettings.directionalZOffsetMinMax.z - renderSettings.directionalZOffsetMinMax.y) * 0.5f);
	m_pShader->SetVar(m_directionalZOffsetMinRangeID, vDirZOffsetMinRange);
	m_pShader->SetVar(m_DirectionalVelocityAddID, vDirVelAdd);
	if(m_edgeSoftnessID != grcevNONE)
	{
		m_pShader->SetVar(m_edgeSoftnessID, renderSettings.edgeSoftness);
	}
	m_pShader->SetVar(m_maxLifeID, fMaxLife);
	m_pShader->SetVar(m_refParticlePosID, refParticlePos);

	m_pShader->SetVar(m_particleColorPercentageID, renderSettings.particleColorPercentage);
	m_pShader->SetVar(m_backgroundDistortionVisibilityPercentageID, renderSettings.backgroundDistortionVisibilityPercentage);

	m_pShader->SetVar(m_backgroundDistortionAlphaBoosterID, renderSettings.backgroundDistortionAlphaBooster);
	m_pShader->SetVar(m_backgroundDistortionAmountID, renderSettings.backgroundDistortionAmount);
	m_pShader->SetVar(m_localLightsMultiplierID, renderSettings.localLightsMultiplier);
	m_pShader->SetVar(m_directionalLightShadowAmountID, renderSettings.directionalLightShadowAmount);
	m_pShader->SetVar(m_lightIntensityMultId, renderSettings.lightIntensityMultiplier);

	BANK_ONLY(m_pShader->SetVar(m_camAngleLimitsID, vCamAngleLimits);)

}


// ptxgpuDrop

bool ptxgpuDrop::Update(grcViewport* pViewport, float deltaTime, WindType_e windType, float ratioToRender, float cutValue, bool resetParticles)
{
	if (!pViewport)
	{
		return false;
	}
	AssertReturn(m_numParticles);

	if (m_initialRandomize)
	{
		m_initialRandomize >>= 1;
		deltaTime *= (float)m_initialRandomize;
	}
	m_updateDelta = deltaTime;

	ptxgpuDropEmitterSettings emit = m_emitterSettings;

	Matrix44 cam = RCC_MATRIX44(pViewport->GetCameraMtx44());

	
	// Calculate the new offset value for the particles local space
	Vector3 pos = cam.d.GetVector3() + cam.c.GetVector3() * (emit.boxCentreOffset.y);
	Vector3 offPosition =  pos - m_RefPosition;
	Vector3 boxHalfSize = 0.5f * emit.boxSize;
	offPosition = offPosition / boxHalfSize;
	offPosition.x = floorf(offPosition.x) * boxHalfSize.x;
	offPosition.y = floorf(offPosition.y) * boxHalfSize.y;
	offPosition.z = floorf(offPosition.z) * boxHalfSize.z;
	m_RefPosition += offPosition; // Update the reference position

	GetUpdateShader()->SetEmitterTransform(cam, emit.boxCentreOffset, m_RefPosition, -offPosition);

	GetUpdateShader()->SetEmitter(emit, m_gravity, m_windInfluence, m_updateDelta*m_timeControl, cutValue);

	GetUpdateShader()->SetWindVariables(windType);

	ptxgpuBase::Update(pViewport, deltaTime, windType, ratioToRender, cutValue, resetParticles);

	return true;
}

bool ptxgpuDrop::Render(const grcViewport* pViewport, Vec3V_In vCamVel, float fMaxLife, float camPosZ, float groundPosZ, ptxRenderSetup* pRenderSetup, float ratioToRender BANK_ONLY(, Vec4V_In vCamAngleLimits))

{
	AssertReturn(m_numParticles);

	if (m_initialRandomize)
	{
		return true;
	}

	// calculate the directional velocity add based on the current camera velocity
	float camSpeedMin = m_renderSettings.dirVelAddCamSpeedMinMaxMult.x;
	float camSpeedMax = m_renderSettings.dirVelAddCamSpeedMinMaxMult.y;
	float camSpeedMult = m_renderSettings.dirVelAddCamSpeedMinMaxMult.z;
	float camSpeed = Mag(vCamVel).Getf();
	Vec3V vDirVelAdd = Vec3V(V_ZERO);
	if (camSpeed>camSpeedMin)
	{
		float dirSpeedMax = (camSpeedMax - camSpeedMin) * camSpeedMult;
		float dirSpeed = camSpeed-camSpeedMin;
		if (dirSpeed>dirSpeedMax)
		{
			dirSpeed = dirSpeedMax;
		}

		vDirVelAdd = Normalize(vCamVel);
		vDirVelAdd = -vDirVelAdd * ScalarVFromF32(dirSpeed);
	}

	GetRenderShader()->SetRenderSettings(m_renderSettings, vDirVelAdd, fMaxLife, m_RefPosition, camPosZ, groundPosZ BANK_ONLY(, vCamAngleLimits));

	ptxgpuBase::Render(pViewport, vCamVel, fMaxLife, camPosZ, groundPosZ, pRenderSetup, ratioToRender);

	return true;
}

void ptxgpuDrop::GetEmitterBounds(Mat44V_ConstRef camMatrix, Vec3V_Ref emitterMin, Vec3V_Ref emitterMax)
{
	// Create a transformation for the box size into worls space
	const Vec3V boxUp = Vec3V(ScalarV(V_ZERO), ScalarV(V_ZERO), ScalarV(V_ONE));
	Vec3V boxForward = -camMatrix.GetCol2().GetXYZ() * Vec3V(ScalarV(V_ONE), ScalarV(V_ONE), ScalarV(V_ZERO));
	boxForward = NormalizeSafe(boxForward, Vec3V(V_ZERO));
	Vec3V boxRight = camMatrix.GetCol0().GetXYZ() * Vec3V(ScalarV(V_ONE), ScalarV(V_ONE), ScalarV(V_ZERO));
	boxRight = NormalizeSafe(boxRight, Vec3V(V_ZERO));
	Vec3V boxCenterOffset = RCC_VEC3V(m_emitterSettings.boxCentreOffset);
	Vec3V boxCentrePos = camMatrix.GetCol3().GetXYZ() + (boxCenterOffset.GetX() * boxRight) + (boxCenterOffset.GetY() * boxForward) + (boxCenterOffset.GetZ() * boxUp);

	// Need to split the bound into two halfs
	Vec3V boxHalfSize = RCC_VEC3V(m_emitterSettings.boxSize) * ScalarV(V_HALF);

	emitterMin = boxCentrePos - boxHalfSize;
	emitterMax = boxCentrePos + boxHalfSize;
}

#if __BANK
void ptxgpuDrop::RenderDebug(const grcViewport& viewport, const Color32& color)
{
	// Create a transformation for the box size into worls space
	Mat44V_ConstRef cam = viewport.GetCameraMtx44();
	const Vec3V boxUp = Vec3V(ScalarV(V_ZERO), ScalarV(V_ZERO), ScalarV(V_ONE));
	Vec3V boxForward = -cam.GetCol2().GetXYZ() * Vec3V(ScalarV(V_ONE), ScalarV(V_ONE), ScalarV(V_ZERO));
	boxForward = Normalize(boxForward);
	Vec3V boxRight = cam.GetCol0().GetXYZ() * Vec3V(ScalarV(V_ONE), ScalarV(V_ONE), ScalarV(V_ZERO));
	boxRight = Normalize(boxRight);
	Vec3V boxCenterOffset = RCC_VEC3V(m_emitterSettings.boxCentreOffset);
	Vec3V boxCentrePos = cam.GetCol3().GetXYZ() + (boxCenterOffset.GetX() * boxRight) + (boxCenterOffset.GetY() * boxForward) + (boxCenterOffset.GetZ() * boxUp);

	// Build a matrix out of the transformation
	Mat34V boxTrans;
	boxTrans.SetCol0(boxRight);
	boxTrans.SetCol1(boxForward);
	boxTrans.SetCol2(boxUp);
	boxTrans.SetCol3(boxCentrePos);

	// Need to split the bound into two halfs
	Vec3V boxHalfSize = RCC_VEC3V(m_emitterSettings.boxSize) * ScalarV(V_HALF);

	// Draw the emitter bounds
	grcDebugDraw::BoxOriented(-boxHalfSize, boxHalfSize, boxTrans, color, false);
}
#endif // __BANK

} // namespace rage

#endif	// !__RESOURCECOMPILER
