// 
// event/runner.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "runner.h"

#include "data/struct.h"

namespace rage {

evtRunner::evtRunner()
{
}

evtRunner::evtRunner(datResource& rsc)
: m_Parameters(rsc)
{
}

IMPLEMENT_PLACE(evtRunner);

#if __DECLARESTRUCT
void evtRunner::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(evtRunner);

	STRUCT_FIELD(m_Parameters);

	STRUCT_END();
}
#endif // __DECLARESTRUCT

} // namespace rage
