// 
// event/updatedata.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_UPDATEDATA_H
#define EVENT_UPDATEDATA_H

namespace rage {

class evtRunner;

// PURPOSE: A block of data that the event types recieve during their Start(), Update() and Stop() calls.
class evtUpdateData {
public:
	evtUpdateData();

	// PURPOSE: Phase in the timeline where the update began
	float m_GlobalPhaseBegin;

	// PURPOSE: Phase in the timeline where the update ended
	float m_GlobalPhaseEnd;

	// PURPOSE: Where, relative to this event, the update began
	float m_LocalPhaseBegin;

	// PURPOSE: Where, relative to this event, the update ended
	float m_LocalPhaseEnd;

	// PURPOSE: The object that is causing this event
	evtRunner* m_EventRunner;

	// PURPOSE: A spot that types can read from and write to, to preserve info between start, update, stop calls
	mutable void* m_RunnerInstanceData;
};

}

#endif

