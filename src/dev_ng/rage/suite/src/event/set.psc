<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef name="EventList" type="::rage::evtSet" version="1" onPostLoad="PostLoad" onPreSave="PreSave">
	<array name="m_Instances" type="atArray">
		<pointer type="::rage::evtInstance" policy="owner" addGroupWidget="false" onUnknownType="::rage::evtSet::AddUnknownType"/>
	</array>
</structdef>

</ParserSchema>