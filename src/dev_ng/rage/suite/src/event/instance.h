// 
// event/instance.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_INSTANCE_H
#define EVENT_INSTANCE_H

#include "atl/bitset.h"
#include "data/base.h"
#include "parser/macros.h"

namespace rage {

class bkGroup;
class evtEditorWidget;
class evtSet;
class evtType;

class evtInstance : public datBase {
public:
	evtInstance();
	evtInstance(const evtInstance& other);
	evtInstance(datResource& rsc);

	DECLARE_PLACE(evtInstance);

#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	const evtInstance& operator=(const evtInstance& other);

	float GetMinT() const {return m_MinT;}
	float GetMaxT() const {return m_MaxT;}

	void SetMinT(float minT) {m_MinT=minT;}
	void SetMaxT(float maxT) {m_MaxT=maxT;}

	evtType* GetType() const {return m_EventType;}
	void SetType(evtType* t);

	u32 GetTypeHash() const {return m_EventTypeHash;}

	virtual const char* ToString() const {return NULL;}

	bool CanPlayForwards() const {return m_Flags.IsSet(PLAY_FORWARD);}
	bool CanPlayBackwards() const {return m_Flags.IsSet(PLAY_BACKWARD);}
	bool CanPlayBlendingIn() const {return m_Flags.IsSet(PLAY_BLENDING_IN);}
	bool CanPlayBlendingOut() const {return m_Flags.IsSet(PLAY_BLENDING_OUT);}

	void SetPlayForwards(bool flag) {m_Flags.Set(PLAY_FORWARD, flag);}
	void SetPlayBackwards(bool flag) {m_Flags.Set(PLAY_BACKWARD, flag);}
	void SetPlayBlendingIn(bool flag) {m_Flags.Set(PLAY_BLENDING_IN, flag);}
	void SetPlayBlendingOut(bool flag) {m_Flags.Set(PLAY_BLENDING_OUT, flag);}

	int GetPriority() const {return m_Priority;}
	void SetPriority(int priority) {FastAssert(priority >= 0 && priority < MAX_INST_PRIORITY); m_Priority = (u8)priority;}

	int GetTrackNumber() const {return m_TrackNumber;}
	void SetTrackNumber(int trackNumber) {FastAssert(trackNumber >= 0 && trackNumber < MAX_TRACK_NUMBER); m_TrackNumber = (u8)trackNumber;}

#if __BANK
	bkGroup* GetWidgetGroup() const {return m_WidgetGroup;}
	void SetWidgetGroup(bkGroup* gp) { m_WidgetGroup = gp;}

	void SetEditorWidget(evtEditorWidget* widget) { m_EditorWidget = widget;}

	void SetSet( evtSet* set) { m_Set = set; }

	void PreAddWidgets(bkBank& bank);
	void PostAddWidgets(bkBank& bank);
#endif

	enum Flags {
		PLAY_FORWARD		= 0,
		PLAY_BACKWARD		= 1,
		PLAY_BLENDING_IN	= 2,
		PLAY_BLENDING_OUT	= 3,
	};

protected:
	enum {
		MAX_TRACK_NUMBER		= 255,
		MAX_INST_PRIORITY		= 255
	};

	virtual void Copy(const evtInstance& other);

	friend class evtTypeManager;

	float m_MinT, m_MaxT;
	evtType* m_EventType;
	u32 m_EventTypeHash;
	atFixedBitSet16 m_Flags;
	u8 m_TrackNumber;
	u8 m_Priority;
	bkGroup* m_WidgetGroup;
	evtEditorWidget* m_EditorWidget;
	// HACK: evtInstance is not always part of a set, this is so
	//	we can delete evtInstance's from the array.
	evtSet* m_Set;

	void UpdateEditor();

	PAR_PARSABLE;
};

} // namespace rage

#endif
