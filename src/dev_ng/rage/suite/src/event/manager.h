// 
// event/manager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

#include "atl/array.h"
#include "atl/functor.h"
#include "atl/singleton.h"
#include "event/type.h"

namespace rage {

class evtInstance;
class evtParamManager;

class evtTypeManager {
public:	
	evtTypeManager(u16 instanceDataMapSize);

	evtParamManager& GetParamManager();

	enum CollisionEnum 
	{
		ONCOLLIDE_ERROR,
		ONCOLLIDE_REPLACE,
		ONCOLLIDE_SKIP,
	};

	void RegisterEventType(const char* group, const char* name, evtType* type, CollisionEnum onCollide);

	static evtType* FindEventTypeFromName(const char*);
	static evtType* FindEventTypeFromHash(u32);
	static const char* FindEventTypeName(evtType*);

	int GetNumTypes() const;
	const char* const* GetEventNameArray() const;

	~evtTypeManager();

#if __BANK
	void AddEventTypes(class evtEditorWidget& widget);
#endif


	template<typename _Type, typename _Instance>
	void RegisterTypeInstancePair(const char* group, const char* name, CollisionEnum onCollide = ONCOLLIDE_ERROR)
	{
		evtType* type = rage_new _Type;
		type->template Init<_Instance>();
		RegisterEventType(group, name, type, onCollide);
	}

protected:
	atArray<evtType*> m_EventTypes;
	atArray<const char *> m_EventNames;
	evtParamManager* m_ParamManager;
};

inline evtParamManager& evtTypeManager::GetParamManager()
{
	return *m_ParamManager;
}

typedef atSingleton<evtTypeManager> evtTypeManagerSingleton;

#define EVENTMGR ::rage::evtTypeManagerSingleton::InstanceRef()

#define INIT_EVENTMGR ::rage::evtTypeManagerSingleton::Instantiate(u16(256))

#define INIT_EVENTMGR_SIZE(X) ::rage::evtTypeManagerSingleton::Instantiate(X)

#define SHUTDOWN_EVENTMGR ::rage::evtTypeManagerSingleton::Destroy()

#define EVENTPARAM ::rage::evtTypeManagerSingleton::InstanceRef().GetParamManager()

}

#endif
