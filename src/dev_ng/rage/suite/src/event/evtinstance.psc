<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef name="EventInstance" type="::rage::evtInstance" version="1" onPreAddWidgets="PreAddWidgets" onPostAddWidgets="PostAddWidgets">
	<float name="m_MinT" min="0.0f" max="1000000.0f" hideWidgets="true" onWidgetChanged="UpdateEditor"/>
	<float name="m_MaxT" min="0.0f" max="1000000.0f" hideWidgets="true" onWidgetChanged="UpdateEditor"/>
	<pointer name="m_EventType" type="::rage::evtType" policy="external_named" toString="rage::evtTypeManager::FindEventTypeName" fromString="rage::evtTypeManager::FindEventTypeFromName" hideWidgets="true"/>
	<u32 name="m_EventTypeHash" hideWidgets="true"/>
	<u16 name="m_Flags" hideWidgets="true"/>
	<u8 name="m_TrackNumber" init="0" hideWidgets="true"/>
	<u8 name="m_Priority" init="0"/>
</structdef>

</ParserSchema>