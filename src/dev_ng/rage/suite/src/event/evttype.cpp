// 
// event/type.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "type.h"

#include "instance.h"
#include "updatedata.h"
#include "parser/manager.h"

using namespace rage;


evtInstance* evtType::CreateNewInstance()
{
	evtInstance* inst = m_InstanceFactory(); 
	inst->SetType(this); 
	return inst;
}

void evtType::CreateInstanceFromResource(datResource& rsc, evtInstance& inst)
{
	m_InstanceResourceFactory(inst, rsc);
	inst.SetType(this); 
}

void evtType::LookupParameterIds()
{
}
