// 
// event/timeline.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_TIMELINE_H
#define EVENT_TIMELINE_H

#include "atl/array.h"
#include "parser/macros.h"

namespace rage {

class evtInstance;
class evtType;
class parTreeNode;

// PURPOSE: An evtTimeline is a collection of event instances that each have start and stop times.
// NOTES:
//   An evtPlayer can play the timeline, and events in the timeline will be started, updated and
//   stopped based on the player's current position.
//   An evtTimeline may be shared by a number of evtPlayers. For example there might be an evtTimline
//   corresponding to a "walk" animation, and each character would own a player that plays the same
//   timline.

class evtTimeline {	
public:

	evtTimeline();
	~evtTimeline();

	void AddEvent(evtInstance* newEvt);
	void RemoveEvent(evtInstance* evt);

	int GetNumInstances() const {return m_Instances.GetCount();}
	const evtInstance* GetInstance(int i) const {return m_Instances[i];}

	const evtInstance* GetMaxTimeInstance(int i) const {
		FastAssert(IsSorted());
		return m_Instances[m_MaxTimeOrder[i]];
	}

	void SortByTime(bool force = false);

	bool IsSorted() const {return m_SortStatus;}

	// For when we're trying to load an unregistered type
	static evtInstance* AddUnknownType(parTreeNode*);

	void PostLoad();

#if __BANK
	void SetMaxTime(float max) const;
	void SetCurrentTime(float t) const;
	void AddWidgets(class bkBank& bank);

	bool CheckTrackTypes();
	void SetTrackType(int track, evtType* type);
	evtType* GetTrackType(int track) const;

	int GetLastModifiedFrame() const {return m_LastModifiedFrame;}
#endif

	static void PostLoadCB(evtTimeline* tl) {tl->PostLoad();}

protected:
	atArray<evtInstance*> m_Instances;
	atArray<int> m_MaxTimeOrder;
	bool m_SortStatus;

#if __BANK
	class evtEditorWidget* m_EditorWidget;

	enum {
		MAX_TRACKS = 20
	};
	atRangeArray<evtType*, MAX_TRACKS> m_TrackTypes;
	mutable int m_LastModifiedFrame;
#endif

	PAR_SIMPLE_PARSABLE;
};



}

#endif

