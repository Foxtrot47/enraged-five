// 
// event/set.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_SET_H
#define EVENT_SET_H

#include "atl/array.h"
#include "data/base.h"
#include "data/callback.h"
#include "data/struct.h"
#include "parser/macros.h"

namespace rage {

class bkGroup;
class evtInstance;
class evtType;
class parTreeNode;

//
// PURPOSE: An evtSet is a set of events that should all be started, stopped and updated un unison.
// NOTES:
//   This might be used to signal to a number of systems that some discrete event has occured (for example
//   all the event instances in an event set might be triggered when an object breaks)
//   A number of event runners may all share the same event set.
//
class evtSet : datBase {	
public:

	evtSet();
	evtSet(const evtSet& other);
	evtSet(datResource& rsc);
	~evtSet();

	DECLARE_PLACE(evtSet);

	const evtSet& operator=(const evtSet& other);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	void AddEvent(evtInstance* newEvt);
	void RemoveEvent(evtInstance* evt);

	int GetNumInstances() const {return m_Instances.GetCount();}
	const evtInstance* GetInstance(int i) const {return m_Instances[i];}
	evtInstance* GetInstance(int i) {return m_Instances[i];}

	// For when we're trying to load an unregistered type
	static evtInstance* AddUnknownType(parTreeNode*);

    void PreSave();
	void PostLoad();

#if __BANK
	void SetMaxTime(float max) const;
	void SetCurrentTime(float t) const;
	void AddWidgets(class bkBank& bank);
#endif

	static void PostLoadCB(evtSet* tl) {tl->PostLoad();}

	void DeleteEventCbThunk(bkBank& bank, CallbackData* data);
protected:
	atArray<datOwner<evtInstance> > m_Instances;

	enum {
		MAX_TRACKS = 20
	};

	void CreateEventCb();
	void DeleteEventCb(CallbackData* data);

	int m_NewInstanceType;
	bkBank*  m_Bank;
	bkGroup* m_Group;

	PAR_SIMPLE_PARSABLE;
};

} // EVENT_SET_H

#endif

