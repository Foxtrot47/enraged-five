//
// event/editorwidget.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef EVENT_EDITORWIDGET_H
#define EVENT_EDITORWIDGET_H

#if __BANK

#include "bank/widget.h"

namespace rage {
class evtEditorWidget : public bkWidget {
	friend class evtInstance;
	friend class evtTimeline;
	friend class evtTypeManager;

protected:
	static void InitClass();

	evtEditorWidget(class bkBank& bank,class evtTimeline& timeLine,const char *title,const char *memo, const char *fillColor=NULL);
	~evtEditorWidget();

	static int GetStaticGuid() { return BKGUID('e','v','n','t'); }
	int GetGuid() const;

	void SetMaxTime(float time);
	void SetCurrentTime(float time);

	void AddEventType(const char* eventType);
	void AddEventInstance(class evtInstance& inst);
	void RemoveEventInstance(class evtInstance& inst);
	void UpdateEventInstance(class evtInstance& inst);
    void AssociateEventInstance( class evtInstance& inst, s32 remote );
    void AddTrack( const char *pTrackName, s32 trackNumber );

	int DrawLocal(int x,int y);

	void Update();
	static void RemoteHandler(const bkRemotePacket& p);
	
protected:
	void RemoteCreate();
	void RemoteUpdate();

#if __WIN32PC
	void WindowCreate();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM) {return 0;}
#endif

	evtTimeline& m_Timeline;
	bkBank& m_Bank;
    int m_numTracksCreated;

private:
	evtEditorWidget();
	const evtEditorWidget& operator=(evtEditorWidget&) {return *this;}
};

}	// namespace rage

#endif

#endif
