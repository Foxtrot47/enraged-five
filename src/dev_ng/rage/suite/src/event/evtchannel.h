// 
// event/evtchannel.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_EVTCHANNEL_H
#define EVENT_EVTCHANNEL_H

#include "diag/channel.h"

// DOM-IGNORE-BEGIN
RAGE_DECLARE_CHANNEL(Event)

#define evtAssertf(cond,fmt,...)			RAGE_ASSERTF(Event,cond,fmt,##__VA_ARGS__)
#define evtVerifyf(cond,fmt,...)			RAGE_VERIFYF(Event,cond,fmt,##__VA_ARGS__)
#define evtErrorf(fmt,...)					RAGE_ERRORF(Event,fmt,##__VA_ARGS__)
#define evtWarningf(fmt,...)				RAGE_WARNINGF(Event,fmt,##__VA_ARGS__)
#define evtDisplayf(fmt,...)				RAGE_DISPLAYF(Event,fmt,##__VA_ARGS__)
#define evtDebugf1(fmt,...)					RAGE_DEBUGF1(Event,fmt,##__VA_ARGS__)
#define evtDebugf2(fmt,...)					RAGE_DEBUGF2(Event,fmt,##__VA_ARGS__)
#define evtDebugf3(fmt,...)					RAGE_DEBUGF3(Event,fmt,##__VA_ARGS__)
#define evtLogf(severity,fmt,...)			RAGE_LOGF(Event,severity,fmt,##__VA_ARGS__)
#define evtCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,Event,severity,fmt,##__VA_ARGS__)
// DOM-IGNORE-END

#endif

