// 
// event/type.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_TYPE_H
#define EVENT_TYPE_H

#include "atl/array.h"
#include "atl/creator.h"
#include "atl/functor.h"
#include "atl/singleton.h"

namespace rage {

class evtInstance;
class evtUpdateData;

class evtType {
public:
	virtual ~evtType() { }
	virtual void Start(const evtInstance& , const evtUpdateData& ) {}
	virtual void Update(const evtInstance& , const evtUpdateData& ) {}
	virtual void Stop(const evtInstance& , const evtUpdateData& ) {}
	virtual void Modified(evtInstance&) {}

	const char* GetName() const {return m_Name;}
	const char* GetGroup() const {return m_Group;}

	evtInstance* CreateNewInstance();
	void CreateInstanceFromResource(datResource& rsc, evtInstance& inst);

	template<typename _Instance>
	void Init() {
		evtInstance*(*factory)() = &atPtrCreator<evtInstance, _Instance>;
		m_InstanceFactory = MakeFunctorRet(factory);
		void(*resourceFactory)(evtInstance&, datResource&) = &atPlaceCreator<evtInstance, _Instance, datResource&>;
		m_InstanceResourceFactory = MakeFunctor(resourceFactory);

		LookupParameterIds();
	}

	void SetFactory(Functor0Ret<evtInstance*> fun) {m_InstanceFactory = fun;}

protected:
	virtual void LookupParameterIds();

	friend class evtTypeManager;

	char m_Group[32];
	char m_Name[32];
	Functor0Ret<evtInstance*> m_InstanceFactory;
	Functor2<evtInstance&, datResource&> m_InstanceResourceFactory;
};


} // namespace rage
#endif

