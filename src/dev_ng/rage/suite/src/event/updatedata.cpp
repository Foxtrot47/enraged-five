// 
// event/updatedata.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "updatedata.h"

using namespace rage;

evtUpdateData::evtUpdateData()
: m_GlobalPhaseBegin(0.0f)
, m_GlobalPhaseEnd(0.0f)
, m_LocalPhaseBegin(0.0f)
, m_LocalPhaseEnd(0.0f)
, m_EventRunner(NULL)
, m_RunnerInstanceData(NULL)
{
}
