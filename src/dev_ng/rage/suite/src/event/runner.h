// 
// event/runner.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_RUNNER_H
#define EVENT_RUNNER_H

#include "param.h"

namespace rage {

// PURPOSE: An evtRunner is a class that can call start, stop, and update functions for events.
// A special class is needed here so that the event type can associate an instance with the
// object that started it.
class evtRunner
{
public:
	evtRunner();
	evtRunner(datResource& rsc);

	DECLARE_PLACE(evtRunner);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	evtParamList* GetParamList() {return &m_Parameters;}

protected:
	evtParamList m_Parameters;
};

} // namespace rage

#endif

