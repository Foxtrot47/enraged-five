// 
// event/params.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_PARAM_H
#define EVENT_PARAM_H

#include "manager.h"

#include "atl/any.h"
#include "atl/array.h"
#include "atl/map.h"

namespace rage {

typedef int evtParamId;

class evtParamScheme;

//PURPOSE
//  Maintain the global list of event parameters, so that players and types can coordinate
//  on what IDs to use. This prevents parameters from having to be looked up by name all
//  the time.
//NOTES
//  Created automatically by the evtTypeManager
class evtParamManager
{
public:	
	static const u16 MAX_NUM_PARAM_TYPES = 128; // The total number of unique parameters for all events
	static const evtParamId PARAM_NOT_FOUND; // The ID returned when a parameter doesn't really exist
	static const int MAX_PARAM_NAME_LENGTH = 128; // The maximum nme length for a parameter

    //PURPOSE
    //  Create an event parameter manager.
	evtParamManager();

    //PURPOSE
    //  Register the intent to use a particular parameter, allocating a slot if the parameter is not
    //  already registered.
    //NOTES
    //  Parameters are reference counted, so you must UnregisterParam once for each RegisterParam
    //SEE ALSO
    //  UnregisterParam
	evtParamId RegisterParam(const char* name);

    //PURPOSE
    //  Tell the system that we are done working with a parameter, so that it can reallocate that slot
    //  once no one is using it any more.
    //NOTES
    //  Parameters are reference counted, so you must UnregisterParam once for each RegisterParam
    //SEE ALSO
    //  RegisterParam
    void UnregisterParam(evtParamId param);
	evtParamId GetParamId(const char* name);

private:
    typedef atMap<ConstString, evtParamId> IdMap;	// TODO: Consider u32 for a hash instead here
    IdMap m_Params; // A map from parameter names to ids

    evtParamId m_UnusedParams[MAX_NUM_PARAM_TYPES]; // The set of as-yet unallocated parameter ids
    u16 m_UsedParamRefs[MAX_NUM_PARAM_TYPES]; // The number of references to each allocated parameter
    const IdMap::Entry* m_UsedMapEntries[MAX_NUM_PARAM_TYPES]; // A map from param id to map entries, for unregistration purposes
	int m_NumParams; // The total number of parameters currently allocated
};

inline evtParamId evtParamManager::RegisterParam(const char* name)
{
	char normalized[MAX_PARAM_NAME_LENGTH];
	StringNormalize(normalized, name, MAX_PARAM_NAME_LENGTH);

	if (evtParamId* slot = m_Params.Access(normalized))
	{
        // Take out a reference to the parameter we allocate
		++m_UsedParamRefs[*slot];							
		
		return *slot;
	}
	else
	{
		FastAssert(m_NumParams < MAX_NUM_PARAM_TYPES);

        // Get one of the unused parameters from this array
		int newParam = m_UnusedParams[m_NumParams];		   
		
		// The one we get better not have a reference already!
        FastAssert(m_UsedParamRefs[newParam] == 0);				  
		
		// Set the reference to 1 because now we own it
        m_UsedParamRefs[newParam] = 1;
													   
        // Put the parameter into the map so we can find it later when players are created
        const IdMap::Entry& entry = m_Params.Insert(ConstString(normalized), newParam);				  
		
		// Record the map entry so we can reclaim it when the ref count goes to zero later
        m_UsedMapEntries[newParam] = &entry;

		// We now have a new param
		++m_NumParams;

		return newParam;
	}
}

//PURPOSE
//  A parameter list is an array of parameters. Each active event runner maintains one of these.
//NOTES
//  The array of atAny's is dense, so there is only one entry for each parameter supported by a
//  particular player class. Each parameter for a runner is called a slot. Since the global array of
//  parameter ids is much larger than the number of slots, an map from parameter ids to slot numbers
//  is held in the evtParamScheme. Since this map is the same for all parameter lists for a particular
//  player class, it is shared between them.
//SEE ALSO
//  evtParamRunner, evtParamScheme
class evtParamList
{
public:
	evtParamList();
	evtParamList(datResource& rsc);
	~evtParamList();

	DECLARE_PLACE(evtParamList);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	atAny& GetParam(evtParamId param);
	bool ParamExists(evtParamId param);

	int GetNumParams()
	{
		return m_Params.GetCount();
	}

	void Init(const evtParamScheme& scheme);

protected:
	atArray<atAny> m_Params;
	const evtParamScheme* m_Scheme;
};

//PURPOSE
//  A special parameter list that is a template for all parameter list created of a particular runner type.
//NOTES
//  1. The parameter schemes are stored in static members of the event players. They are a template for the
//     parameter lists that each instance of that player will need. When evtParamList::Init is called, a param
//     scheme is supplied which inits the number and types of all the parameters.
//  2. The parameter scheme also stores a mapping from the total set of parameters ids to the parameters for this
//     particular scheme. So even though there may be over a hundred parameter ids, each event player only needs
//     to have space for the parameters it intends to support.
//SEE ALSO
//  evtRunner, evtParamList
class evtParamScheme : public evtParamList
{
public:
    //PURPOSE
    //  Add support of a parameter to a particular scheme.
    //NOTES
    //  AddParam part of the system for binding the parameters by name to the global parameter ids. It also
    //  fills in the scheme-specific table that maps from global ids to parameter slots.
	template <class _Type>
		evtParamId AddParam(const char* name)
	{
		if (m_ParamIdToSlotNum.GetCapacity() == 0)
		{
			m_ParamIdToSlotNum.Resize(evtParamManager::MAX_NUM_PARAM_TYPES);

			for (int param = 0; param < evtParamManager::MAX_NUM_PARAM_TYPES; ++param)
			{
				m_ParamIdToSlotNum[param] = evtParamManager::PARAM_NOT_FOUND;
			}
		}

		evtParamId id = EVENTPARAM.RegisterParam(name);
		m_ParamIdToSlotNum[id] = m_Params.GetCount();

		m_Params.Grow() = _Type();

		return id;
	}

    //PURPOSE
    //  Unregister all the parameters used by this scheme.
    //NOTES
    //  When a parameter scheme is retired, this decrements the ref count the event manager maintains for
    //  each parameter at the global scope.
	void DeleteParams()
	{
		// If our capacity is zero we never had any params
		if (m_ParamIdToSlotNum.GetCapacity() != 0)
		{
		    // For each param slot...
	        for (int param = 0; param < evtParamManager::MAX_NUM_PARAM_TYPES; ++param)
	        {
				// ...that is actually in use... 
	            if (m_ParamIdToSlotNum[param] != evtParamManager::PARAM_NOT_FOUND)
	            {								
					// ...unregister the param
	                EVENTPARAM.UnregisterParam(param);
	            }
	        }
		}
		
		m_ParamIdToSlotNum.Reset();
		m_Params.Reset();
	}

    //PURPOSE
    //  Find out what slot number a global parameter id corresponds to for this scheme.
    //NOTES
    //  The only operation supported for the parameter lists that are the main users of parameter schemes,
    //  this function is how a global param id turns into a slot number for this scheme.
	int TranslateFromIdToSlot(evtParamId id) const
	{
		return m_ParamIdToSlotNum[id];
	}

private:
	atArray<int> m_ParamIdToSlotNum;
};

}

#endif // EVENT_PARAM_H
