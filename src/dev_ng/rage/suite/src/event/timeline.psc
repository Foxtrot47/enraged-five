<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef name="EventTimeline" type="::rage::evtTimeline" version="1" onPostLoad="PostLoad">
	<array name="m_Instances" type="atArray">
		<pointer type="::rage::evtInstance" policy="owner" addGroupWidget="false" onUnknownType="::rage::evtTimeline::AddUnknownType"/>
	</array>
</structdef>

</ParserSchema>