// 
// event/player.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "player.h"

#include "evtchannel.h"
#include "instance.h"
#include "timeline.h"
#include "type.h"
#include "updatedata.h"

#include "atl/array_struct.h"
#include "atl/map.h"
#include "bank/bank.h"
#include "data/struct.h"
#include "event/set.h"
#include "math/amath.h"
#include "paging/rscbuilder.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"
#include "vector/vector3.h"

// Enable for detailed profile breakdown of event usage
#if 0
#define EVT_PF_START(X) PF_START(X)
#define EVT_PF_STOP(X) PF_STOP(X)
#define EVT_PF_FUNC(X) PF_FUNC(X)
#else
#define EVT_PF_START(X)
#define EVT_PF_STOP(X)
#define EVT_PF_FUNC(X)
#endif

namespace rage {

evtPlayer::InstanceDataMap* evtPlayer::s_InstanceDataMap = NULL;

class crSkeleton;
class rmcDrawable;
class phInst;

namespace evtProfile {
	PF_PAGE(EventPlayerPage,"Event Player");
	PF_GROUP(Events);
	PF_LINK(EventPlayerPage, Events);

	PF_TIMER(Update, Events);
	PF_TIMER(Start, Events);
	PF_TIMER(Stop, Events);
	PF_TIMER(InstUpdate, Events);
	PF_TIMER(InstStart, Events);
	PF_TIMER(InstStop, Events);

	PF_VALUE_INT(NumEvents, Events);
}
}

namespace rage {

using namespace rage::evtProfile;

evtPlayer::evtPlayer()
: m_Timeline(NULL)
, m_Eventset(NULL)
, m_LastT(0.0f)
, m_LastDirection(DIR_PAUSE)
, m_Playing(false)
, m_ControlTimelinePlayhead(false)
, m_Pad0(0)
, m_Pad1(0)
{
}

evtPlayer::evtPlayer(datResource& )
{
}

evtPlayer::~evtPlayer()
{
	Stop(m_LastT, true);
}

IMPLEMENT_PLACE(evtPlayer);

#if __DECLARESTRUCT
datSwapper_ENUM(evtPlayer::Direction)

void evtPlayer::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(evtPlayer);

	evtRunner::DeclareStruct(s);
	STRUCT_FIELD_VP(m_Timeline);
	STRUCT_FIELD_VP(m_Eventset);
	STRUCT_FIELD(m_LastT);
	STRUCT_FIELD(m_LastDirection);
	STRUCT_FIELD(m_StartInstance);
	STRUCT_FIELD(m_LastKnownModification);
	STRUCT_FIELD(m_Playing);
	STRUCT_FIELD(m_ControlTimelinePlayhead);
	STRUCT_FIELD(m_Pad0);
	STRUCT_FIELD(m_Pad1);

	STRUCT_END();
}
#endif // __DECLARESTRUCT

void evtPlayer::UpdateStartIndex(Direction dir, float startT, float )
{
	if (m_Timeline)
	{
		switch(dir)
		{
		case DIR_PAUSE:
			// do nothing
			break;
		case DIR_FORWARD:
			// move StartInstance to the number of the first instance that starts before startT and continues beyond startT
			for(; m_StartInstance < m_Timeline->GetNumInstances(); m_StartInstance++) {
				const evtInstance* inst = m_Timeline->GetInstance(m_StartInstance);
				if (inst->GetMaxT() >= startT) {
					break;
				}
			}
			break;
		case DIR_BACKWARD:
			// move StartInstance to the number of the first instance that starts before startT and continues beyond startT
			for(; m_StartInstance < m_Timeline->GetNumInstances(); m_StartInstance++) {
				const evtInstance* inst = m_Timeline->GetMaxTimeInstance(m_StartInstance);
				if (inst->GetMinT() <= startT) {
					break;
				}
			}
			break;
		}
	}
}

void evtPlayer::SetTimeline(evtTimeline& timeline, bool controlPlayhead)
{
	if (m_Playing) {
		Stop(m_LastT);
	}
	evtAssertf(m_Eventset == NULL, "Can't set a timeline for a player that's using an event set");
	m_Timeline = &timeline;
	m_LastT = 0.0f;
	m_LastDirection = DIR_FORWARD;
	m_StartInstance = 0;
#if __BANK
	m_LastKnownModification = -1;
#endif
	m_ControlTimelinePlayhead = controlPlayhead;
}

void evtPlayer::SetSet(evtSet& set)
{
	if (m_Playing) {
		Stop(m_LastT);
	}
	evtAssertf(m_Timeline == NULL, "Can't set an event set for a player that's using a timeline");
	m_Eventset = &set;
	m_LastT = 0.0f;
	m_LastDirection = DIR_FORWARD;
	m_StartInstance = 0;
#if __BANK
	m_LastKnownModification = -1;
#endif
	m_ControlTimelinePlayhead = false;
}

void evtPlayer::Start(float startT, bool forward)
{
	EVT_PF_FUNC(Start);

	// If the player has already been started, stop it
	if (m_Playing) {
		Stop(m_LastT);
	}
	m_Playing = true;

	m_LastT = startT;
	m_LastDirection = forward ? DIR_FORWARD : DIR_BACKWARD;

	m_StartInstance = 0;
	UpdateStartIndex(m_LastDirection, startT, startT);

	evtUpdateData upData;

	upData.m_GlobalPhaseBegin = startT;
	upData.m_GlobalPhaseEnd = startT;
	upData.m_EventRunner = this;

	// Start any active items

	if (m_Eventset)
	{
		upData.m_GlobalPhaseBegin = 0.0f;
		upData.m_GlobalPhaseEnd = 0.0f;

		for(int i = 0; i < m_Eventset->GetNumInstances(); i++) {
			const evtInstance* inst = m_Eventset->GetInstance(i);
            
            if (inst->GetType() == NULL) {
                continue;
            }
			
            StartInstance(*inst, upData);
		}
	}
	else
	{
		evtAssertf(m_Timeline, "This player doesn't have an event set or a timeline");
		evtAssertf(m_Timeline->IsSorted(), "The timeline events are out of order and need to be sorted before playing");

		for(int i = m_StartInstance; i < m_Timeline->GetNumInstances(); i++) {
			const evtInstance* inst = NULL;

			if (m_LastDirection == DIR_FORWARD) {
				inst = m_Timeline->GetInstance(i);
			}
			else {
				inst = m_Timeline->GetMaxTimeInstance(i);
			}

			if (inst->GetType() == NULL) {
				continue;
			}

			float minT = inst->GetMinT();
			float maxT = inst->GetMaxT();

			if ((m_LastDirection == DIR_FORWARD && minT > startT) ||
				(m_LastDirection == DIR_BACKWARD && maxT < startT)) {
				break; 
			}

			if ((m_LastDirection == DIR_FORWARD && inst->CanPlayForwards()) ||
				(m_LastDirection == DIR_BACKWARD && inst->CanPlayBackwards())) {
				if (minT < startT && maxT > startT) {
					upData.m_LocalPhaseBegin = upData.m_LocalPhaseEnd = ClampRange(startT, minT, maxT);
					StartInstance(*inst, upData);
				}
			}
		}
	}
}

void evtPlayer::Stop(float endT, bool bSupressUpdate)
{
	EVT_PF_FUNC(Stop);

	if (!m_Playing) {
		return;
	}

	if (!bSupressUpdate && (endT != m_LastT))
	{
		Update(endT);
	}

	evtUpdateData upData;

	upData.m_GlobalPhaseBegin = endT;
	upData.m_GlobalPhaseEnd = endT;
	upData.m_EventRunner = this;

	// Stop any active items
	if (m_Eventset)
	{
		upData.m_GlobalPhaseBegin = 1.0f;
		upData.m_GlobalPhaseEnd = 1.0f;
		
		for(int i = 0; i < m_Eventset->GetNumInstances(); i++) {
			const evtInstance* inst = m_Eventset->GetInstance(i);

            if (inst->GetType() == NULL) {
                continue;
            }

			StopInstance(*inst, upData);
		}
	}
	else if (m_Timeline)
	{
		for(int i = m_StartInstance; i < m_Timeline->GetNumInstances(); i++) {
			const evtInstance* inst = NULL;

			if (m_LastDirection == DIR_FORWARD) {
				inst = m_Timeline->GetInstance(i);
			}
			else {
				inst = m_Timeline->GetMaxTimeInstance(i);
			}
			if (inst->GetType() == NULL) {
				continue;
			}

			float minT = inst->GetMinT();
			float maxT = inst->GetMaxT();

			if ((m_LastDirection == DIR_FORWARD && minT > endT) ||
				(m_LastDirection == DIR_BACKWARD && maxT < endT)) {
				break; 
			}

			if ((m_LastDirection == DIR_FORWARD && inst->CanPlayForwards()) ||
				(m_LastDirection == DIR_BACKWARD && inst->CanPlayBackwards())) {
				if (minT < endT && maxT > endT) {
					upData.m_LocalPhaseBegin = upData.m_LocalPhaseEnd = ClampRange(endT, minT, maxT);
					StopInstance(*inst, upData);
				}
			}
		}
	}

	m_Playing = false;
}

void evtPlayer::StartUpdateStop()
{
	//evtAssertf(m_Eventset);
	if(NULL==m_Eventset)
		return;

	for(int i = 0; i < m_Eventset->GetNumInstances(); i++)
	{
		evtUpdateData upData;

		upData.m_GlobalPhaseBegin = 0.0f;
		upData.m_GlobalPhaseEnd = 0.0f;
		upData.m_EventRunner = this;
		upData.m_RunnerInstanceData = NULL;

		const evtInstance* inst = m_Eventset->GetInstance(i);

        evtType* type = inst->GetType();

        if (type)
        {
		    EVT_PF_START(InstStart);
		    type->Start(*inst, upData);
		    EVT_PF_STOP(InstStart);

		    EVT_PF_START(InstUpdate);
		    type->Update(*inst, upData);
		    EVT_PF_STOP(InstUpdate);
    		
		    EVT_PF_START(InstStop);
		    type->Stop(*inst, upData);
		    EVT_PF_STOP(InstStop);
        }
	}
}

void evtPlayer::Update(float newT, bool looping /* = false */ )
{
	EVT_PF_FUNC(Update);

#if __BANK
	// While editing, the user might have moved the minT, maxT points past thr current play area,
	// causing us to miss start and stop events. Stop the playback and also make sure any events that
	// were active last frame get stopped (even if they're no longer under the playhead)
	if (m_Timeline && m_LastKnownModification != m_Timeline->GetLastModifiedFrame())
	{
		m_LastKnownModification = m_Timeline->GetLastModifiedFrame();
		StopLastFrameInstances();
	}
#endif
 
	Direction direction = DIR_PAUSE;
	if (newT < m_LastT) {
		if (looping && m_LastDirection == DIR_FORWARD) 
		{
			// do the easy thing for now. The real solution involes not recursing first of all and 
			// making sure that we _dont_ stop and start any events that begin at 0.0 and end at 1.0,
			// cause those should be continuous.
			Update(1.0f, false);
			Stop(1.0f);
			Start(0.0f, true);
			Update(newT, false);
			return;
		}
		else
		{
			direction = DIR_BACKWARD;
		}
	}
	else if (newT > m_LastT) {
		if (looping && m_LastDirection == DIR_BACKWARD)
		{
			Update(0.0f, false);
			Stop(0.0f);
			Start(1.0f, true);
			Update(newT, false);
			return;
		}
		else
		{
			direction = DIR_FORWARD;
		}
	}

	if (!m_Playing) {
		Start(newT, direction == DIR_BACKWARD ? false : true);
	}
	m_Playing = true;

	// If we changed directions, stop the player and restart it.
	if (direction != DIR_PAUSE) {
		if (direction != m_LastDirection) {
			Stop(m_LastT);
			Start(m_LastT, direction == DIR_FORWARD);
		}
	}

	evtUpdateData upData;

	upData.m_GlobalPhaseBegin = m_LastT;
	upData.m_GlobalPhaseEnd = newT;
	upData.m_EventRunner = this;

	if (m_Eventset)
	{
		upData.m_GlobalPhaseBegin = 1.0f;
		upData.m_GlobalPhaseEnd = 1.0f;

		for(int i = 0; i < m_Eventset->GetNumInstances(); i++) {
			const evtInstance* inst = m_Eventset->GetInstance(i);

            if (inst->GetType() == NULL) {
                continue;
            }

			UpdateInstance(*inst, upData);
		}
	}
	else
	{
		UpdateStartIndex(direction, m_LastT, newT);

		bool done = false;
		int i = m_StartInstance;
		for(i = m_StartInstance; !done && i < m_Timeline->GetNumInstances(); i++) {

			const evtInstance* inst = NULL;
			if (m_LastDirection == DIR_FORWARD) {
				inst = m_Timeline->GetInstance(i);
			}
			else {
				inst = m_Timeline->GetMaxTimeInstance(i);
			}

			if (inst->GetType() == NULL) {
				continue;
			}

			float minT = inst->GetMinT();
			float maxT = inst->GetMaxT();
			float phaseBegin = m_LastT;
			float phaseEnd = newT;

			switch(direction) {
			case DIR_FORWARD: 
				{
					if (minT > phaseEnd) {
						done = true;
						break;
					}
					if (inst->CanPlayForwards())
					{
						if (minT == maxT) { // Special case for 0 duration events.
							if (minT > phaseBegin && minT <= phaseEnd) { // If the 0 duration event is exactly on a frame boundary, play it in the earlier frame.
								upData.m_LocalPhaseBegin = 0.0f;
								upData.m_LocalPhaseEnd = 1.0f;
								StartInstance(*inst, upData);
								UpdateInstance(*inst, upData);
								StopInstance(*inst, upData);
							}
						}
						else {
							upData.m_LocalPhaseBegin = ClampRange(phaseBegin, minT, maxT);
							upData.m_LocalPhaseEnd = ClampRange(phaseEnd, minT, maxT);

							if (minT >= phaseBegin && minT < phaseEnd) {
								StartInstance(*inst, upData);
							}
							if (maxT > phaseBegin && minT < phaseEnd) {
								UpdateInstance(*inst, upData);
							}
							if (maxT <= phaseEnd && maxT > phaseBegin) {
								StopInstance(*inst, upData);
							}
						}
					}
				}
				break;
			case DIR_PAUSE:
				{
					if ((m_LastDirection == DIR_FORWARD && minT > phaseEnd) ||
						(m_LastDirection == DIR_BACKWARD && maxT < phaseBegin)) {
						done = true;
						break;
					}
					// call update on any active events. Don't start or stop any events.
					if (maxT > phaseBegin && minT < phaseEnd) {
						upData.m_LocalPhaseBegin = ClampRange(phaseBegin, minT, maxT);
						upData.m_LocalPhaseEnd = ClampRange(phaseEnd, minT, maxT);
						if ((m_LastDirection == DIR_FORWARD && inst->CanPlayForwards()) ||
							(m_LastDirection == DIR_BACKWARD && inst->CanPlayBackwards())) {
								UpdateInstance(*inst, upData);
							}
					}
				}
				break;
			case DIR_BACKWARD:
				{
					if (maxT < phaseEnd) {
						done = true;
						break;
					}
					if (inst->CanPlayBackwards())
					{
						if (minT == maxT) { // Special case for 0 duration events.
							if (minT < phaseBegin && minT >= phaseEnd) { // If the 0 duration event is exactly on a frame boundary, play it in the earlier frame.
								upData.m_LocalPhaseBegin = 0.0f;
								upData.m_LocalPhaseEnd = 1.0f;
								StartInstance(*inst, upData);
								UpdateInstance(*inst, upData);
								StopInstance(*inst, upData);
							}
						}
						else {
							upData.m_LocalPhaseBegin = 1.0f - ClampRange(phaseBegin, minT, maxT);
							upData.m_LocalPhaseEnd = 1.0f - ClampRange(phaseEnd, minT, maxT);

							if (maxT <= phaseBegin && maxT > phaseEnd) {
								StartInstance(*inst, upData);
							}
							if (minT < phaseBegin && maxT > phaseEnd) {
								UpdateInstance(*inst, upData);
							}
							if (minT >= phaseEnd && minT < phaseBegin) {
								StopInstance(*inst, upData);
							}
						}
					}
				}
				break;
			}

		}
	}

	m_LastT = newT;
	if (direction != DIR_PAUSE) {
		m_LastDirection = direction;
	}
	
#if __BANK
	if (m_ControlTimelinePlayhead)
	{
		m_Timeline->SetCurrentTime(m_LastT);
	}
#endif

	PF_SET(NumEvents, s_InstanceDataMap->m_Map.GetNumUsed());
}

void evtPlayer::StartInstance(const evtInstance& inst, evtUpdateData& upData)
{
	// Add inst to the list of active instances
	InstanceDataMap::Map::Entry& entry = s_InstanceDataMap->Insert(PlayerInstancePair(this, &inst));
	upData.m_RunnerInstanceData = NULL;
	EVT_PF_START(InstStart);
	inst.GetType()->Start(inst, upData);
	EVT_PF_STOP(InstStart);
	entry.data = upData.m_RunnerInstanceData;
}

void evtPlayer::UpdateInstance(const evtInstance& inst, evtUpdateData& upData)
{
	// Find the instance data in the list of active instances
	void** runnerInstanceData = s_InstanceDataMap->Access(PlayerInstancePair(this, &inst));

	// If the instance data wasn't found, this is probably an instance that was added while
	// the player was alreade playing. In which case, start this one up!
	if (runnerInstanceData == NULL)
	{
		InstanceDataMap::Map::Entry& entry = s_InstanceDataMap->Insert(PlayerInstancePair(this, &inst));
		runnerInstanceData = &entry.data;
		evtAssertf(runnerInstanceData, "Couldn't find the instance user data for newly started instance");
	}

	upData.m_RunnerInstanceData = *runnerInstanceData;

	EVT_PF_START(InstUpdate);
	inst.GetType()->Update(inst, upData);
	EVT_PF_STOP(InstUpdate);
	*runnerInstanceData = upData.m_RunnerInstanceData;
}

void evtPlayer::StopInstance(const evtInstance& inst, evtUpdateData& upData)
{
	// Find the instance data in the list of active instances
	PlayerInstancePair playerInstancePair(this, &inst);
	void** runnerInstanceData = s_InstanceDataMap->Access(playerInstancePair);
	evtAssertf(runnerInstanceData, "Couldn't find the instance data for a stopping instance");

	upData.m_RunnerInstanceData = *runnerInstanceData;

	EVT_PF_START(InstStop);
	inst.GetType()->Stop(inst, upData);
	EVT_PF_STOP(InstStop);

	s_InstanceDataMap->Delete(playerInstancePair);
}

void evtPlayer::StopLastFrameInstances()
{
	evtUpdateData upData;
	upData.m_GlobalPhaseBegin = m_LastT;
	upData.m_GlobalPhaseEnd = m_LastT;
	upData.m_LocalPhaseBegin = 0.0f;
	upData.m_LocalPhaseEnd = 1.0f;
	upData.m_EventRunner = this;

	const evtInstance* instanceArray = NULL;
	int numInstances = 0;

	if (m_Eventset)
	{
		instanceArray = m_Eventset->GetInstance(0);
		numInstances = m_Eventset->GetNumInstances();
	}
	else if (m_Timeline)
	{
		instanceArray = m_Timeline->GetInstance(0);
		numInstances = m_Timeline->GetNumInstances();
	}

	for(int i = 0; i < numInstances; ++i)
	{
		const evtInstance* inst = instanceArray + i;
		void** runnerInstanceData = s_InstanceDataMap->Access(PlayerInstancePair(this, inst));

		upData.m_RunnerInstanceData = *runnerInstanceData;
		inst->GetType()->Stop(*inst, upData);

		s_InstanceDataMap->Delete(PlayerInstancePair(this, inst));
	}

	m_Playing = false;
}

evtParamId evtAnimPlayer::s_AnimSpeed;
evtParamId evtAnimPlayer::s_AnimBlendAmount;
evtParamId evtAnimPlayer::s_AnimBlendSpeed;
evtParamId evtAnimPlayer::s_Position;
evtParamId evtAnimPlayer::s_Skeleton;
evtParamId evtAnimPlayer::s_Drawable;

IMPLEMENT_EVENT_PLAYER(evtAnimPlayer)

void evtAnimPlayer::RegisterPlayerClass()
{
	s_AnimSpeed = s_ParameterScheme.AddParam<float>("anim speed"); // PARAM_ANIM_SPEED
	s_AnimBlendAmount = s_ParameterScheme.AddParam<float>("anim blend amount"); // PARAM_ANIM_BLEND_AMOUNT
	s_AnimBlendSpeed = s_ParameterScheme.AddParam<float>("anim blend speed"); // PARAM_ANIM_BLEND_SPEED
	s_Position = s_ParameterScheme.AddParam<Vector3>("position"); // PARAM_ANIM_ROOT_POSITION
	s_Skeleton = s_ParameterScheme.AddParam<const crSkeleton*>("skeleton pointer"); // PARAM_ANIM_SKELETON
	s_Drawable = s_ParameterScheme.AddParam<rmcDrawable*>("drawable pointer"); // PARAM_DRAWABLE
}

void evtAnimPlayer::UnregisterPlayerClass()
{
	s_ParameterScheme.DeleteParams();
}

} // namespace rage
