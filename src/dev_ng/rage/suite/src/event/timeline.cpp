// 
// event/timeline.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "timeline.h"


#include "editorwidget.h"
#include "evtchannel.h"
#include "instance.h"
#include "manager.h"

#include "bank/bank.h"
#include "parser/treenode.h"
#include "parsercore/attribute.h"
#include "system/timemgr.h"

#include <algorithm>

#include "timeline_parser.h"

using namespace rage;

evtTimeline::evtTimeline()
: m_SortStatus(false)
#if __BANK
, m_EditorWidget(NULL)
#endif
{
#if __BANK
	for(int i = 0; i < MAX_TRACKS; i++) {
		m_TrackTypes[i] = NULL;
	}
#endif
}

evtTimeline::~evtTimeline()
{
	for(int i = 0; i < m_Instances.GetCount(); i++) {
		delete m_Instances[i];
	}
}

void evtTimeline::AddEvent(evtInstance* newEvt)
{
	evtAssertf(newEvt->GetMinT() <= newEvt->GetMaxT(), "Bad time values min=%f, max=%f", newEvt->GetMinT(), newEvt->GetMaxT());
#if __BANK
	if (m_TrackTypes[newEvt->GetTrackNumber()] == NULL) {
		m_TrackTypes[newEvt->GetTrackNumber()] = newEvt->GetType();
	}
	else {
		evtAssertf(m_TrackTypes[newEvt->GetTrackNumber()] == newEvt->GetType(), "The track type and instance type dont match. Instance is %s, track number %d", newEvt->GetType()->GetName(), newEvt->GetTrackNumber());
	}
#endif
	m_Instances.Grow() = newEvt;
	m_MaxTimeOrder.Grow() = m_Instances.GetCount()-1;
	m_SortStatus = false;
}

void evtTimeline::RemoveEvent(evtInstance* evt)
{
	m_Instances.DeleteMatches(evt);
	m_MaxTimeOrder.Resize(m_Instances.GetCount());
	m_SortStatus = false;
}

struct StartCompare {
	inline bool operator()(evtInstance* evt1, evtInstance* evt2) {
		return evt1->GetMinT() < evt2->GetMinT();
	}
};

struct StopCompare {
	inline bool operator()(int evt1, int evt2) {
		return m_Instances[evt1]->GetMaxT() > m_Instances[evt2]->GetMaxT();
	}
	StopCompare(evtInstance** instances) : m_Instances(instances) {
	}
	evtInstance** m_Instances;
};


void evtTimeline::SortByTime(bool force)
{
	if (!force && m_SortStatus == true) {
		return;
	}
	if (m_MaxTimeOrder.GetCount() != m_Instances.GetCount()) {
		m_MaxTimeOrder.Reset();
		m_MaxTimeOrder.Resize(m_Instances.GetCount());
	}

	// Reset the max index list
	for(int i = 0; i < m_MaxTimeOrder.GetCount(); i++) {
		m_MaxTimeOrder[i] = i;
	}

	if (m_Instances.GetCount() > 0) {
		std::sort(&m_Instances[0], &m_Instances[0] + m_Instances.GetCount(), StartCompare());
		std::sort(&m_MaxTimeOrder[0], &m_MaxTimeOrder[0] + m_MaxTimeOrder.GetCount(), StopCompare(&m_Instances[0]));
	}
	m_SortStatus = true;

#if __BANK
	m_LastModifiedFrame = TIME.GetFrameCount();
#endif
}

evtInstance* evtTimeline::AddUnknownType(parTreeNode* OUTPUT_ONLY(node))
{
	evtWarningf("Couldn't find registered type %s", node->GetElement().FindAttribute("type")->GetStringValue());
	evtInstance* evt = rage_new evtInstance;
	return evt;
}

void evtTimeline::PostLoad()
{
	SortByTime();
#if __BANK
	for(int i = 0; i < m_Instances.GetCount(); i++) {
		if (m_TrackTypes[m_Instances[i]->GetTrackNumber()] == NULL) {
			m_TrackTypes[m_Instances[i]->GetTrackNumber()] = m_Instances[i]->GetType();
		}
	}
	evtAssertf(CheckTrackTypes(), "One of the instances is on a track with the wrong type");
#endif
}

#if __BANK
void evtTimeline::SetTrackType(int track, evtType* type)
{
	m_TrackTypes[track] = type;
	evtAssertf(CheckTrackTypes(), "One of the instances is on a track with the wrong type");
}

bool evtTimeline::CheckTrackTypes()
{
	for(int i = 0; i < m_Instances.GetCount(); i++) {
		if (m_TrackTypes[m_Instances[i]->GetTrackNumber()] != m_Instances[i]->GetType()) {
			return false;
		}
	}
	return true;
}

evtType* evtTimeline::GetTrackType(int index) const
{
	return m_TrackTypes[index];
}

void evtTimeline::SetMaxTime(float t) const
{
    if ( m_EditorWidget )
    {
        m_EditorWidget->SetMaxTime(t);
    }
}

void evtTimeline::SetCurrentTime(float t) const
{
    if ( m_EditorWidget )
    {
        m_EditorWidget->SetCurrentTime(t);
    }
}

void evtTimeline::AddWidgets(bkBank& bank)
{
	evtAssertf(m_EditorWidget==NULL, "Editor widget hasn't been created yet");
	m_EditorWidget=rage_new evtEditorWidget(bank,*this,"Event Editor","Timeline editor for editing events");
	bank.AddWidget(*m_EditorWidget);

	// send instance information to widget:
	for(int i = 0; i < m_Instances.GetCount(); i++) {
		if (m_Instances[i])
		{
			m_EditorWidget->AddEventInstance(*m_Instances[i]);
			m_Instances[i]->SetEditorWidget(m_EditorWidget);
		}
	}

	EVENTMGR.AddEventTypes(*m_EditorWidget);
}

#endif
