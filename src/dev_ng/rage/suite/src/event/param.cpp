// 
// event/params.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "param.h"

#include "evtchannel.h"
#include "instance.h"
#include "player.h"

#include "atl/array_struct.h"
#include "diag/tracker.h"
#include "parser/manager.h"
#include "paging/rscbuilder.h"
#include "qa/qa.h"
#include "system/miniheap.h"
#include <algorithm>

namespace rage {

const evtParamId evtParamManager::PARAM_NOT_FOUND = -1;

evtParamList::evtParamList()
{
}

evtParamList::evtParamList(datResource& rsc)
: m_Params(rsc, true)
{
}

evtParamList::~evtParamList()
{
}

#if __DECLARESTRUCT
void evtParamList::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(evtParamList);

	// NOTE: We assume that these parameters have been created in temp memory,
	// so we delete them here. If we didn't, this is a memory leak.
	// TODO: If there is a way to see if a pointer is inside resource memory,
	// we should use it here and assert if m_Parameters.GetElements() *is* inside
	// the resource heap.
	sysMemStartTemp();
	m_Params.Reset();
	sysMemEndTemp();

	STRUCT_FIELD(m_Params);
	m_Scheme = NULL;	// This is repaired in the rsc ctor
	STRUCT_FIELD_VP(m_Scheme);

	STRUCT_END();
} 
#endif // __DECLARESTRUCT

atAny& evtParamList::GetParam(evtParamId param)
{
	evtAssertf(param != evtParamManager::PARAM_NOT_FOUND, "Parameter not known to the event manager");
	evtAssertf(m_Params.GetCount() > 0, "Parameter list empty, was CreateParameterList called on the player?");
	int slot = m_Scheme->TranslateFromIdToSlot(param);
	evtAssertf(slot < m_Params.GetCount(), "Slot index is invalid for param ID %d", param);
	evtAssertf(slot != evtParamManager::PARAM_NOT_FOUND, "Parameter not present on parameter list");
	return m_Params[slot];
}

bool evtParamList::ParamExists(evtParamId param)
{
	if(evtParamManager::PARAM_NOT_FOUND ==param)
		return false;
	int slot = m_Scheme->TranslateFromIdToSlot(param);

	if(evtParamManager::PARAM_NOT_FOUND == slot)
		return false;

	return true;
}

void evtParamList::Init(const evtParamScheme& scheme)
{
	RAGE_TRACK(evtParamList);

	evtAssertf(m_Params.GetCount() == 0, "Can't init a non-empty param list");

	if (m_Params.GetCount() == scheme.m_Params.GetCount())
	{
		m_Params.Resize(0);
	}
	else
	{
		m_Params.Reserve(scheme.m_Params.GetCount());
	}

	for (int param = 0; param < scheme.m_Params.GetCount(); ++param)
	{
		m_Params.Append() = scheme.m_Params[param];
	}

	m_Scheme = &scheme;
}

evtParamManager::evtParamManager()
	: m_NumParams(0)
{
	m_Params.Recompute(MAX_NUM_PARAM_TYPES);

    sysMemSet(m_UsedParamRefs, 0, sizeof(m_UsedParamRefs));
    for (int param = 0; param < MAX_NUM_PARAM_TYPES; ++param)
    {
        m_UnusedParams[param] = param;
    }
}

evtParamId evtParamManager::GetParamId(const char* name)
{
	char normalized[MAX_PARAM_NAME_LENGTH];
	StringNormalize(normalized, name, MAX_PARAM_NAME_LENGTH);

	if (evtParamId* slot = m_Params.Access(normalized))
	{
		return *slot;
	}
	else
	{
		return PARAM_NOT_FOUND;
	}
}

void evtParamManager::UnregisterParam(evtParamId param)
{
	// We had better not dip under zero params!
	evtAssertf(m_NumParams > 0, "No more params to unregister");

	// Decrement the ref on this param
	if (--m_UsedParamRefs[param] == 0)
	{								  
		// If the ref went to zero, we have one less param
		--m_NumParams;									  

		// This param is now unused, so put it on the unused array
		m_UnusedParams[m_NumParams] = param;

		// Sort the unused params to we get the smallest available slot later
		std::sort(m_UnusedParams+ m_NumParams, m_UnusedParams + MAX_NUM_PARAM_TYPES - 1);

		// Reclaim the map entry so we leave no trace
		m_Params.Delete(m_UsedMapEntries[param]->key);
	}
}

#if __QA

class evtQA1Player : public evtPlayer
{
public:
    evtQA1Player();
    evtQA1Player(datResource& rsc);

    void CreateParameterList();

    static void RegisterPlayerClass();
    static void UnregisterPlayerClass();

private:
    static evtParamScheme s_ParameterScheme;

    static evtParamId s_AnimSpeed;
    static evtParamId s_AnimBlendAmount;
    static evtParamId s_AnimBlendSpeed;
    static evtParamId s_Matrix;
};

evtParamId evtQA1Player::s_AnimSpeed;
evtParamId evtQA1Player::s_AnimBlendAmount;
evtParamId evtQA1Player::s_AnimBlendSpeed;
evtParamId evtQA1Player::s_Matrix;

IMPLEMENT_EVENT_PLAYER(evtQA1Player)

void evtQA1Player::RegisterPlayerClass()
{
    s_AnimSpeed       = s_ParameterScheme.AddParam<float>("anim speed"); // PARAM_ANIM_SPEED
    s_AnimBlendAmount = s_ParameterScheme.AddParam<float>("anim blend amount"); // PARAM_ANIM_BLEND_AMOUNT
    s_AnimBlendSpeed  = s_ParameterScheme.AddParam<float>("anim blend speed"); // PARAM_ANIM_BLEND_SPEED
    s_Matrix		  = s_ParameterScheme.AddParam<u8>("matrix"); // PARAM_MATRIX
}

void evtQA1Player::UnregisterPlayerClass()
{
    s_ParameterScheme.DeleteParams();
}

class evtQA2Player : public evtPlayer
{
public:
    evtQA2Player();
    evtQA2Player(datResource& rsc);

    void CreateParameterList();

    static void RegisterPlayerClass();
    static void UnregisterPlayerClass();

private:
    static evtParamScheme s_ParameterScheme;

    static evtParamId s_AnimSpeed;
    static evtParamId s_Component;
    static evtParamId s_Part;
};

evtParamId evtQA2Player::s_AnimSpeed;
evtParamId evtQA2Player::s_Component;
evtParamId evtQA2Player::s_Part;

IMPLEMENT_EVENT_PLAYER(evtQA2Player)

void evtQA2Player::RegisterPlayerClass()
{
    s_AnimSpeed = s_ParameterScheme.AddParam<float>("anim speed"); // PARAM_ANIM_SPEED
    s_Component	= s_ParameterScheme.AddParam<int>("component");
    s_Part		= s_ParameterScheme.AddParam<int>("part");
}

void evtQA2Player::UnregisterPlayerClass()
{
    s_ParameterScheme.DeleteParams();
}

class qa_EvtParamRegisterUnregister : public qaItem
{
public:

    void Init()
    {
	    INIT_PARSER;
	    INIT_EVENTMGR;

        evtAnimPlayer::RegisterPlayerClass();
            evtAnimPlayer::UnregisterPlayerClass();

        evtQA1Player::RegisterPlayerClass();
            evtAnimPlayer::RegisterPlayerClass();
                evtQA1Player::UnregisterPlayerClass();
            evtQA2Player::RegisterPlayerClass();
                evtAnimPlayer::UnregisterPlayerClass();
            evtQA2Player::UnregisterPlayerClass();

        evtAnimPlayer::RegisterPlayerClass();
            evtQA2Player::RegisterPlayerClass();
                evtQA2Player::UnregisterPlayerClass();
            evtQA1Player::RegisterPlayerClass();
                evtQA2Player::RegisterPlayerClass();
                    evtAnimPlayer::UnregisterPlayerClass();
                evtQA1Player::UnregisterPlayerClass();
            evtQA2Player::UnregisterPlayerClass();
    }

    void Shutdown()
    {
        SHUTDOWN_EVENTMGR;
        SHUTDOWN_PARSER;
    }

    void Update( qaResult& result );
};

void qa_EvtParamRegisterUnregister::Update( qaResult& result )
{
    TST_PASS;
}

#endif // __QA

} // namespace rage

#if __QA

using namespace rage;

QA_ITEM_FAMILY( qa_EvtParamRegisterUnregister, (), () );

QA_ITEM( qa_EvtParamRegisterUnregister, (), qaResult::PASS_OR_FAIL );

#endif // __QA

