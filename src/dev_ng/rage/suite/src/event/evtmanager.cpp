// 
// event/manager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "manager.h"

#include "editorwidget.h"
#include "evtchannel.h"
#include "instance.h"
#include "param.h"
#include "player.h"
#include "type.h"

#include "system/param.h"

RAGE_DEFINE_CHANNEL(Event)

using namespace rage;

evtTypeManager::evtTypeManager(u16 instanceDataMapSize)
: m_ParamManager(rage_new evtParamManager)
{
	evtPlayer::InitInstanceDataMap(instanceDataMapSize);

#if __BANK
	evtEditorWidget::InitClass();
#endif
}

evtTypeManager::~evtTypeManager()
{
	for(int i = 0; i < m_EventTypes.GetCount(); i++) {
		delete m_EventTypes[i];
	}
	delete m_ParamManager;
	m_ParamManager = NULL;

	evtPlayer::ShutdownInstanceDataMap();
}

evtType* evtTypeManager::FindEventTypeFromName(const char* str)
{
#if __RESOURCECOMPILER
	if (!evtTypeManagerSingleton::IsInstantiated())
	{
		Warningf("No event manager instantiated!");
		return NULL;
	}
#endif
	for(int i = 0; i < EVENTMGR.m_EventTypes.GetCount(); i++) {
		if (!strcmp(EVENTMGR.m_EventTypes[i]->m_Name, str)) {
			return EVENTMGR.m_EventTypes[i];
		}
	}
	evtErrorf("Couldn't find an event type named \"%s\"", str);
	return NULL;
}

evtType* evtTypeManager::FindEventTypeFromHash(u32 hash)
{
	for(int i = 0; i < EVENTMGR.m_EventTypes.GetCount(); i++) {
		if (atHash_const_char(EVENTMGR.m_EventTypes[i]->m_Name) == hash) {
			return EVENTMGR.m_EventTypes[i];
		}
	}
	evtErrorf("Couldn't find an event type with hash %d", hash);
	return NULL;
}

const char* evtTypeManager::FindEventTypeName(evtType* evt)
{
	if (evt != NULL) {
		return evt->m_Name;
	}
	else {
		return NULL;
	}
}

int evtTypeManager::GetNumTypes() const
{
	return m_EventTypes.GetCount();
}

const char* const* evtTypeManager::GetEventNameArray() const
{
	return &m_EventNames[0];
}

void evtTypeManager::RegisterEventType(const char* group, const char* name, evtType* type, CollisionEnum onCollide)
{
	for(int i = 0; i < m_EventTypes.GetCount(); i++) {
		if (!strcmp(m_EventTypes[i]->m_Name, name)) {
			switch(onCollide)
			{
			case ONCOLLIDE_ERROR:
				evtErrorf("An event named %s has already been registered", name);
				return;
			case ONCOLLIDE_SKIP:
				evtDebugf1("Skipping event %s, since one has already been registered", name);
				return;
			case ONCOLLIDE_REPLACE:
				evtDebugf1("Replacing event %s with a new one", name);
				delete m_EventTypes[i];
				m_EventTypes.DeleteFast(i);
				m_EventNames.DeleteFast(i);
				i--;
				break;
			}
		}
	}

	strcpy(type->m_Group, group);
	strcpy(type->m_Name, name);
	m_EventTypes.Grow() = type;
	m_EventNames.Grow() = type->m_Name;
}

#if __BANK
void evtTypeManager::AddEventTypes(class evtEditorWidget& widget)
{
	for (int i=0;i<m_EventTypes.GetCount();i++)
		widget.AddEventType(m_EventTypes[i]->m_Name);
}
#endif

