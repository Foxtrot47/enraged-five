// 
// event/player.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_PLAYER_H
#define EVENT_PLAYER_H

#include "runner.h"

#include "atl/array.h"

namespace rage {

class evtInstance;
class evtSet;
class evtTimeline;
class evtUpdateData;

class evtPlayer : public evtRunner {
public:
	evtPlayer();
	evtPlayer(datResource& rsc);
	~evtPlayer();

	DECLARE_PLACE(evtPlayer);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	// PURPOSE: Call to start the event player.
	// PARAMS: 
	//		startT - The parametric value for where to start the playback.
	//		forward - Will we be playing forward or backward? If this is incorrect, some
	//			forward only or backward only events will be incorrectly started and stopped before the first update.
	void Start(float startT, bool forward=true);

	// PURPOSE: Update the event player.
	// PARAMS:
	//		newT - The new parametric value for where along the timeline we are now.
	//		looping - If true, continue in the current direction and loop around, instead of changing directions
	void Update(float newT, bool looping=false);

	// PURPOSE: Stops the event player.
	// PARAMS:
	//		stopT - The final parameteric value for the position along the timeline.
	void Stop(float stopT, bool bSupressUpdate=false);

	// PURPOSE: Start, update, and stop the event player all in one function call.
	// NOTES:
	//  - This is the equivalent of calling Start(0.0f); Update(0.0f); Stop(0.0f);
	//        but without having to allocation instance data in the player itself.
	//  - This function only works on players that have been assigned an event set.
	void StartUpdateStop();

	// PURPOSE: Sets which timeline to use to read events from.
	void SetTimeline(evtTimeline& timeline, bool controlPlayhead = false);

	// PURPOSE: Sets which set to use to read events from.
	void SetSet(evtSet& set);

	//	Accessor for event set
	const evtSet* GetEventSet() const { return m_Eventset; }

	enum Direction {
		DIR_FORWARD,
		DIR_PAUSE,
		DIR_BACKWARD
	};

	static void InitInstanceDataMap(u16 size)
	{
		Assert(s_InstanceDataMap == NULL);
		s_InstanceDataMap = rage_new InstanceDataMap(size);
	}

	static void ShutdownInstanceDataMap()
	{
		delete s_InstanceDataMap;
		s_InstanceDataMap = NULL;
	}

protected:
	void StartInstance(const evtInstance& inst, evtUpdateData& upData);
	void UpdateInstance(const evtInstance& inst, evtUpdateData& upData);
	void StopInstance(const evtInstance& inst, evtUpdateData& upData);

	void StopLastFrameInstances();

	enum {
		MAX_SIMULTANEOUS_EVENTS = 40,
	};

	void UpdateStartIndex(Direction dir, float startT, float endT);

	const evtTimeline*	m_Timeline;
	const evtSet*		m_Eventset;

	float m_LastT;
	Direction m_LastDirection;
	int m_StartInstance; // where to start, when scanning over the list of instances

	int m_LastKnownModification;

	bool m_Playing;
	bool m_ControlTimelinePlayhead;

	u8 m_Pad0, m_Pad1;

	struct PlayerInstancePair
	{
		PlayerInstancePair(const evtPlayer* thePlayer, const evtInstance* theInstance)
			: player(thePlayer)
			, instance(theInstance)
		{
		}

		bool operator==(const PlayerInstancePair& other) const
		{
			return player == other.player && instance == other.instance;
		}

		const evtPlayer* player;
		const evtInstance* instance;
	};

	typedef atSimplePooledMapType<PlayerInstancePair, void*> InstanceDataMap;

	static InstanceDataMap* s_InstanceDataMap;
	friend unsigned atHash(const PlayerInstancePair x);
};

inline unsigned atHash(const evtPlayer::PlayerInstancePair x)
{
	return unsigned(uptr(x.player) ^ (uptr(x.instance) >> 4));
}

#define IMPLEMENT_EVENT_PLAYER(X) \
evtParamScheme X::s_ParameterScheme; \
\
X::X() \
{ \
} \
\
X::X(datResource& rsc) \
	: evtPlayer(rsc) \
{ \
	m_Parameters.Init(s_ParameterScheme); \
} \
\
void X::CreateParameterList() \
{ \
	Assertf(m_Parameters.GetNumParams() == 0, #X "::CreateParameterList was called already"); \
	Assertf(s_ParameterScheme.GetNumParams() != 0, #X "::RegisterPlayerClass() has not been called"); \
\
	if (pgRscBuilder::IsBuilding())	sysMemStartTemp();	\
	m_Parameters.Init(s_ParameterScheme); \
	if (pgRscBuilder::IsBuilding())	sysMemEndTemp();	\
}

class evtAnimPlayer : public evtPlayer
{
public:
	evtAnimPlayer();
	evtAnimPlayer(datResource& rsc);

	void CreateParameterList();

	atAny& GetAnimSpeed() { return m_Parameters.GetParam(s_AnimSpeed); }
	atAny& GetAnimBlendAmout() { return m_Parameters.GetParam(s_AnimBlendAmount); }
	atAny& GetAnimBlendSpeed() { return m_Parameters.GetParam(s_AnimBlendSpeed); }
	atAny& GetPosition() { return m_Parameters.GetParam(s_Position); }
	atAny& GetSkeleton() { return m_Parameters.GetParam(s_Skeleton); }
	atAny& GetDrawable() { return m_Parameters.GetParam(s_Drawable); }

	static void RegisterPlayerClass();
	static void UnregisterPlayerClass();

private:
	static evtParamScheme s_ParameterScheme;

	static evtParamId s_AnimSpeed;
	static evtParamId s_AnimBlendAmount;
	static evtParamId s_AnimBlendSpeed;
	static evtParamId s_Position;
	static evtParamId s_Skeleton;
	static evtParamId s_Drawable;
};

} // namespace rage

#endif
