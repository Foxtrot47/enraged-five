// 
// event/evtinstance.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "instance.h"

#include "editorwidget.h"
#include "manager.h"
#include "set.h"

#include "evtinstance_parser.h"

#include "atl/map.h"
#include "bank/bank.h"
#include "data/safestruct.h"
#include "data/struct.h"
#include "string/stringhash.h"

namespace rage {

evtInstance::evtInstance() 
: m_MinT(0.0f)
, m_MaxT(0.0f)
, m_EventType(NULL)
, m_TrackNumber(0)
, m_Priority(0)
#if __BANK
, m_WidgetGroup(NULL)
, m_EditorWidget(NULL)
, m_Set(NULL)
#endif
{
	m_Flags.Set(PLAY_FORWARD);
	m_Flags.Set(PLAY_BACKWARD);
	m_Flags.Set(PLAY_BLENDING_IN);
	m_Flags.Set(PLAY_BLENDING_OUT);
}

evtInstance::evtInstance(datResource& )
{
	m_EventType = EVENTMGR.FindEventTypeFromHash(m_EventTypeHash);
}

evtInstance::evtInstance(const evtInstance& other)
{
	Copy(other);
}

IMPLEMENT_PLACE(evtInstance);

#if __DECLARESTRUCT
void evtInstance::DeclareStruct(datTypeStruct &s)
{
	m_EventType = NULL;

	SSTRUCT_BEGIN(evtInstance)

	SSTRUCT_FIELD(evtInstance, m_MinT)
	SSTRUCT_FIELD(evtInstance, m_MaxT)
	SSTRUCT_IGNORE(evtInstance, m_EventType)
	SSTRUCT_FIELD(evtInstance, m_EventTypeHash)
	SSTRUCT_FIELD(evtInstance, m_Flags)
	SSTRUCT_FIELD(evtInstance, m_TrackNumber)
	SSTRUCT_FIELD(evtInstance, m_Priority)
	SSTRUCT_FIELD_VP(evtInstance, m_WidgetGroup)
	SSTRUCT_FIELD_VP(evtInstance, m_EditorWidget)
	SSTRUCT_FIELD_VP(evtInstance, m_Set)

	SSTRUCT_END(evtInstance)
}
#endif // __DECLARESTRUCT

const evtInstance& evtInstance::operator=(const evtInstance& other)
{
	Copy(other);

	return *this;
}

void evtInstance::Copy(const evtInstance& other)
{
	m_MinT = other.m_MinT;
	m_MaxT = other.m_MaxT;
	m_EventType = other.m_EventType;
	m_EventTypeHash = other.m_EventTypeHash;
	m_Flags = other.m_Flags;
	m_TrackNumber = other.m_TrackNumber;
	m_Priority = other.m_Priority;

#if __BANK
	m_WidgetGroup = other.m_WidgetGroup;
	m_EditorWidget = other.m_EditorWidget;
#endif // __BANK
}

void evtInstance::SetType(evtType* t)
{	
	m_EventType = t;
	m_EventTypeHash = atHash_const_char(t->GetName());
}

#if __BANK
void evtInstance::PreAddWidgets(bkBank& bank)
{
	char name[ 128 ];
	strcpy( name, m_EventType->GetName() );
	strcat( name, " Event" );

	bkGroup* newGrp = bank.PushGroup(name, false);
	SetWidgetGroup(newGrp);

	m_Set->DeleteEventCbThunk( bank, (rage::CallbackData*)this );
}

void evtInstance::PostAddWidgets(bkBank& bank)
{
	bank.PopGroup();
}

void evtInstance::UpdateEditor()
{
	if (m_EditorWidget)
		m_EditorWidget->UpdateEventInstance(*this);
}

#endif

} // namespace rage

