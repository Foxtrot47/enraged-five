// 
// event/list.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "set.h"

#include "editorwidget.h"
#include "evtchannel.h"
#include "instance.h"
#include "manager.h"

#include "atl/array_struct.h"
#include "bank/bank.h"
#include "data/resourcehelpers.h"
#include "data/struct.h"
#include "parser/manager.h"
#include "parsercore/attribute.h"
#include "system/timemgr.h"

#include <algorithm>

#include "set_parser.h"

using namespace rage;

evtSet::evtSet()
: m_NewInstanceType(0)
{
	m_Bank = NULL;
	m_Group = NULL;
}

evtSet::evtSet(const evtSet& other)
{
	*this = other;
}

const evtSet& evtSet::operator=(const evtSet& other)
{
	m_Instances.Reserve(other.m_Instances.GetCount());

	for (int inst = 0; inst < other.m_Instances.GetCount(); ++inst)
	{
		evtInstance* newInst;
        if (other.m_Instances[inst]->GetType())
        {
            newInst = other.m_Instances[inst]->GetType()->CreateNewInstance();
        }
        else
        {
            newInst = rage_new evtInstance;
        }
		*newInst = *other.m_Instances[inst];
		m_Instances.Append() = newInst;
	}

	m_NewInstanceType = other.m_NewInstanceType;
	m_Bank = other.m_Bank;
	m_Group = other.m_Group;

	return *this;
}

evtSet::evtSet(datResource& rsc)
: m_Instances(rsc)
, m_NewInstanceType(0)
{
	m_Bank = NULL;

	for (int inst = 0; inst < m_Instances.GetCount(); ++inst)
	{
		rsc.PointerFixup(m_Instances[inst].ptr);
		evtType* type = EVENTMGR.FindEventTypeFromHash(m_Instances[inst]->GetTypeHash());
		evtAssertf(type, "Event type '%d' not registered with the event system", m_Instances[inst]->GetTypeHash());
		type->CreateInstanceFromResource(rsc, *m_Instances[inst]);
	}
}

IMPLEMENT_PLACE(evtSet);

evtSet::~evtSet()
{
	for(int i = 0; i < m_Instances.GetCount(); i++) {
		delete m_Instances[i];
	}
}

#if __DECLARESTRUCT
void evtSet::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(evtSet);

	STRUCT_FIELD(m_Instances);
	STRUCT_FIELD(m_NewInstanceType);
	STRUCT_FIELD_VP(m_Bank);
	STRUCT_FIELD_VP(m_Group);

	STRUCT_END();
}
#endif // __DECLARESTRUCT

void evtSet::AddEvent(evtInstance* newEvt)
{
	m_Instances.Grow() = newEvt;
}

void evtSet::RemoveEvent(evtInstance* evt)
{
	for (int i=0; i<m_Instances.GetCount(); )
	{
		if (m_Instances[i] == evt)
			m_Instances.Delete(i);
		else
			++i;
	}
}

evtInstance* evtSet::AddUnknownType(parTreeNode* OUTPUT_ONLY(node))
{
	evtWarningf("Couldn't find registered type %s", node->GetElement().FindAttribute("type")->GetStringValue());
	evtInstance* evt = rage_new evtInstance;
	return evt;
}

void evtSet::PreSave()
{
    for (int i=0; i<m_Instances.GetCount(); ++i)
    {
        evtAssertf(m_Instances[i]->GetType(), "Event of unknown type cannot be saved, because that would destroy prior tuning values");
    }
}

void evtSet::PostLoad()
{
}

#if __BANK
void evtSet::DeleteEventCbThunk(bkBank& bank, CallbackData* data)
{
	bank.AddButton( "Delete", datCallback(MFA1(evtSet::DeleteEventCb), this, data ), "Delete this event" );
}

void evtSet::CreateEventCb()
{
	m_Bank->SetCurrentGroup(*m_Group);

	evtType* type=EVENTMGR.FindEventTypeFromName(EVENTMGR.GetEventNameArray()[m_NewInstanceType]);
	evtAssertf(type, "Can't find type for event named %s", EVENTMGR.GetEventNameArray()[m_NewInstanceType]);

	evtInstance* inst = type->CreateNewInstance();
	inst->SetMinT(0.0f);
	inst->SetMaxT(1.0f);
	inst->SetTrackNumber(m_Instances.GetCount());

	inst->SetSet( this );

	AddEvent(inst);

	PARSER.AddWidgets(*m_Bank, inst);

	m_Bank->UnSetCurrentGroup(*m_Group);
}

void evtSet::DeleteEventCb(CallbackData* data)
{
	evtInstance* inst = reinterpret_cast< evtInstance* >( data );

	bkGroup* group = inst->GetWidgetGroup();	
	group->Destroy();

	RemoveEvent( inst );
}

void evtSet::AddWidgets(bkBank& bank)
{
	m_Bank = &bank;

	m_Group = bank.PushGroup("Event Set");

	if (int numTypes = EVENTMGR.GetNumTypes())
	{
		bank.AddCombo("New Type",
					  &m_NewInstanceType,
					  numTypes,
					  (const char**)EVENTMGR.GetEventNameArray(),
					  NullCB,
					  "The type to create when the Add Event button is pressed");
	}

	bank.AddButton("Create Event", datCallback(MFA(evtSet::CreateEventCb), this), "Create an event, set New Type to control the type");

	for(int i = 0; i < m_Instances.GetCount(); i++)
	{
		PARSER.AddWidgets(*m_Bank, m_Instances[i].ptr);
	}

	bank.PopGroup();
}

#endif
