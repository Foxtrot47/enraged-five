//
// event/editorwidget.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "editorwidget.h"

#include "evtchannel.h"
#include "instance.h"
#include "manager.h"
#include "timeline.h"
#include "type.h"

#include "bank/group.h"
#include "bank/packet.h"
#include "bank/pane.h"
#include "parser/manager.h"
#include "string/string.h"

using namespace rage;

void evtEditorWidget::InitClass()
{
	bkRemotePacket::AddType(GetStaticGuid(),RemoteHandler);
}

evtEditorWidget::evtEditorWidget(bkBank& bank,evtTimeline& timeLine,const char *title,const char *memo, const char *fillColor) :
bkWidget(NullCallback,title,memo,fillColor),
m_Timeline(timeLine),
m_Bank(bank),
m_numTracksCreated(0)
{
}



evtEditorWidget::~evtEditorWidget() {
}


int evtEditorWidget::GetGuid() const { return GetStaticGuid(); }


int evtEditorWidget::DrawLocal(int x,int y) 
{
	Drawf( x, y, m_Title );
	return bkWidget::DrawLocal(x,y);
}

enum 
{
	EVENT_INST_ADD=bkRemotePacket::USER+0,
	EVENT_INST_REMOVE=bkRemotePacket::USER+1,
	EVENT_INST_ASSOCIATE=bkRemotePacket::USER+2,
	EVENT_CURRENT_TIME=bkRemotePacket::USER+3,
	EVENT_MAX_TIME=bkRemotePacket::USER+4,
	EVENT_TYPE_ADD=bkRemotePacket::USER+5,
	EVENT_INST_UPDATE=bkRemotePacket::USER+6,
	EVENT_TRACK_ASSOCIATE=bkRemotePacket::USER+7
};

void evtEditorWidget::SetMaxTime(float time)
{
	if (!bkRemotePacket::IsConnectedToRag())
	{
		return;
	}
	bkRemotePacket p;
	p.Begin(EVENT_MAX_TIME,GetStaticGuid(),this);
	p.Write_float(0.0f);
	p.Write_float(time);
	p.Write_float(0.0f);
	p.Write_float(time);
	p.Send();
}

void evtEditorWidget::SetCurrentTime(float time)
{
	if (!bkRemotePacket::IsConnectedToRag())
	{
		return;
	}
	bkRemotePacket p;
	p.Begin(EVENT_CURRENT_TIME,GetStaticGuid(),this);
	p.Write_float(time);
	p.Send();
}

void evtEditorWidget::AddEventType(const char* eventType)
{
	if (!bkRemotePacket::IsConnectedToRag())
	{
		return;
	}
	bkRemotePacket p;
	p.Begin(EVENT_TYPE_ADD,GetStaticGuid(),this);
	p.Write_const_char(eventType);
	p.Send();
}

void evtEditorWidget::AddEventInstance(evtInstance& inst)
{
	if (!bkRemotePacket::IsConnectedToRag())
	{
		return;
	}

    // make sure our tracks have been created
    while ( m_numTracksCreated < inst.GetTrackNumber() + 1 )
    {
        evtType *pType = m_Timeline.GetTrackType( m_numTracksCreated );
        AddTrack( pType->GetName(), m_numTracksCreated );        
    }

	bkRemotePacket p;
	p.Begin(EVENT_INST_ADD,GetStaticGuid(),this);
	p.Write_pointer(&inst);
	evtAssertf(inst.GetType(), "This instance has no type - was the type/instance pair registered right?");
	p.Write_const_char(inst.GetType()->GetName());
	p.Write_const_char(inst.ToString());
	p.Write_float(inst.GetMinT());
	p.Write_float(inst.GetMaxT());
	p.Write_s32(inst.GetTrackNumber());
	p.WriteWidget(inst.GetWidgetGroup());
	p.Send();
}

void evtEditorWidget::RemoveEventInstance(evtInstance& inst)
{
	if (!bkRemotePacket::IsConnectedToRag())
	{
		return;
	}
	bkRemotePacket p;
	p.Begin(EVENT_INST_REMOVE,GetStaticGuid(),this);
	p.Write_pointer(&inst);
	p.Send();
}

void evtEditorWidget::UpdateEventInstance(evtInstance& inst)
{
	if (!bkRemotePacket::IsConnectedToRag())
	{
		return;
	}
	bkRemotePacket p;
	p.Begin(EVENT_INST_UPDATE,GetStaticGuid(),this);
	p.Write_pointer(&inst);
	p.Write_const_char(inst.ToString());
	p.Write_float(inst.GetMinT());
	p.Write_float(inst.GetMaxT());
	p.Send();
}

void evtEditorWidget::AssociateEventInstance( evtInstance& inst, s32 remote )
{
    if (!bkRemotePacket::IsConnectedToRag())
    {
        return;
    }

	bkRemotePacket p;
	p.Begin( EVENT_INST_ASSOCIATE, GetStaticGuid(), this );
	p.Write_pointer( &inst );
	p.Write_s32( remote );
	p.WriteWidget( inst.GetWidgetGroup() );
	p.Send();
}

void evtEditorWidget::AddTrack( const char *pTrackName, s32 trackNumber )
{
    bkRemotePacket p;
    p.Begin( EVENT_TRACK_ASSOCIATE, GetStaticGuid(), this );
    p.Write_const_char( pTrackName );
    p.Write_s32( trackNumber );
    p.Send();

    ++m_numTracksCreated;
}

void evtEditorWidget::RemoteCreate() {
	if (!bkRemotePacket::IsConnectedToRag())
	{
		return;
	}
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool(IsReadOnly());
	p.Send();
}


void evtEditorWidget::RemoteUpdate() {
	if (!bkRemotePacket::IsConnectedToRag())
	{
		return;
	}
	//bkRemotePacket p;
	//p.Begin(bkRemotePacket::CHANGED,GetStaticGuid());
	//p.Write_bkWidget(bkRemotePacket::LocalToRemote(*this));
	//p.Write_const_char(m_String);
	//p.Send();
}

#if __WIN32PC
#include "system/xtl.h"

void evtEditorWidget::WindowCreate() 
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{
		GetPane()->AddLastWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX );
	}
}
#endif // __WIN32PC

void evtEditorWidget::RemoteHandler(const bkRemotePacket& packet) {
	switch (packet.GetCommand())
	{
	case bkRemotePacket::CREATE:
		{
			Quitf("Remote creation not supported yet.");
		}
		break;

	// remotely added instances:
	case EVENT_INST_ADD:
		{
			packet.Begin();
			evtEditorWidget* widget = packet.ReadWidget<evtEditorWidget>();
			if (!widget) return;
			s32 hashCode=packet.Read_s32();
			float minT=packet.Read_float();
			float maxT=packet.Read_float();
			s32 trackNumber=packet.Read_s32();

			evtType* newType = widget->m_Timeline.GetTrackType(trackNumber);
			evtInstance* inst = newType->CreateNewInstance();
			inst->SetMinT(minT);
			inst->SetMaxT(maxT);
			inst->SetTrackNumber(trackNumber);
			widget->m_Timeline.AddEvent(inst);
			widget->m_Timeline.SortByTime(true);
			packet.End();

			inst->SetEditorWidget(widget);
			PARSER.AddWidgets(widget->m_Bank, inst);

			// send a response to tell the widget to associate the local widget with a remote widget:
            widget->AssociateEventInstance( *inst, hashCode );
		}
		break;

	// remotely removed instances:
	case EVENT_INST_REMOVE:
		{
			packet.Begin();
			evtEditorWidget* widget = packet.ReadWidget<evtEditorWidget>();
			if (!widget) return;
            evtInstance* inst = (evtInstance*)packet.Read_pointer();
            packet.End();
			
            // send a response to tell the widget to go ahead and delete the inst
            widget->RemoveEventInstance( *inst );

			widget->m_Timeline.RemoveEvent(inst);
			widget->m_Timeline.SortByTime(true);
			evtDisplayf("local: %p ptr: %p",widget,inst);
		}
		break;

	case EVENT_INST_UPDATE:
		{
			packet.Begin();
			evtEditorWidget* widget = packet.ReadWidget<evtEditorWidget>();
			if (!widget) return;
			evtInstance* inst = (evtInstance*)packet.Read_pointer();
			float minT=packet.Read_float();
			float maxT=packet.Read_float();
			evtDisplayf("old minT: %f maxT: %f new minT: %f maxT: %f",inst->GetMinT(),inst->GetMaxT(),minT,maxT);
			inst->SetMinT(minT);
			inst->SetMaxT(maxT);
			widget->m_Timeline.SortByTime(true);
			
			packet.End();
		}
		break;

	case EVENT_TRACK_ASSOCIATE:
		{
			packet.Begin();
			evtEditorWidget* widget = packet.ReadWidget<evtEditorWidget>();
			if (!widget) return;
			const char* eventTypeName=packet.Read_const_char();
			s32 track=packet.Read_s32();
			evtType* type=EVENTMGR.FindEventTypeFromName(eventTypeName);
			evtAssertf(type, "Can't find an event type named %s, was it registered", eventTypeName);
			widget->m_Timeline.SetTrackType(track,type);
			packet.End();

            // send a response to the widget to go ahead and create the track
            widget->AddTrack( eventTypeName, track );
		}
		break;
	}
}

void evtEditorWidget::Update() {
}


#endif
