// 
// event/resourceversions.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EVENT_RESOURCEVERSIONS_H
#define EVENT_RESOURCEVERSIONS_H

namespace rage {

enum
{
	evtResourceBaseVersion = 4,
};

} // namespace rage

#endif // EVENT_RESOURCEVERSIONS_H
