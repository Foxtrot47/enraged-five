// 
// fragment/manager.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "manager.h"

// frag
#include "cachemanager.h"
#include "drawable.h"
#include "fragmentconfig.h"
#include "instance.h"
#include "resourceversions.h"
#include "tune.h"
#include "type.h"
#include "typegroup.h"

// RAGE
#include "bank/bank.h"
#include "bank/combo.h"
#include "cloth_config.h"
#include "cloth/environmentcloth.h"
#include "cloth/charactercloth.h"
#include "breakableglass/glassmanager.h"
#include "breakableglass/bgdrawable.h"
#include "data/resourcehelpers.h"
#include "diag/output.h"
#include "diag/tracker.h"
#include "file/asset.h"
#include "file/token.h"
#include "paging/rscbuilder.h"
#include "phbound/boundcomposite.h"
#include "physics/levelnew.h"
#include "grprofile/drawmanager.h"
#include "grprofile/ekg.h"
#include "profile/profiler.h"
#include "system/memory.h"
#include "system/alloca.h"
#include "system/param.h"
#include "system/memmanager.h"
#include "vector/matrix34.h"
#include "vector/quantize.h"
#include "system/taskwaitthread.h"
#include "system/memvisualize.h"

FRAGMENT_OPTIMISATIONS()

RAGE_DEFINE_CHANNEL(Fragment)

#if (__XENON || __PS3)
#define FRAG_CACHE_NODES 13000
#else
#define FRAG_CACHE_NODES 30000
#endif

namespace rage {

#if __PAGING
PARAM(regenfrags,"[fragment] Regenerate paging data for fragment types");
PARAM(regenfragdistricts,"[fragment] Regenerate paging data for fragment types, and all districs that have fragment objects");
#endif

PARAM(props,"[fragment] Force all container-based fragment types to be of the same type");
PARAM(nodisappearwhendead, "[fragment] Disable fragments from disappearing when dead (for debugging)");
#if __BANK
PARAM(nofraglatching,"[fragment] don't allow anything to latch (car doors)");
#endif // __BANK

PFD_DECLARE_GROUP_ON(Fragment);
PFD_DECLARE_ITEM(DetachInterest,Color32(128, 128, 255),Fragment);
PFD_DECLARE_ITEM(Locators,Color32(255,255,255),Fragment);
PFD_DECLARE_ITEM_COLOR(LocatorColor,Fragment, Color32(255,255,255));
PFD_DECLARE_ITEM(FragmentNames,Color32(255,255,255),Fragment);
PFD_DECLARE_ITEM_SLIDER(FragmentProfileDrawRange, Color32(255,255,255), Fragment, 50, 1000, 0.01f);
PFD_DECLARE_ITEM_SLIDER(FragmentNameDistanceOpacity, Color32(255,255,255), Fragment, 0, 1, 0.01f);
PFD_DECLARE_ITEM_ON(CurrentHealth,Color32(255,255,255),Fragment);
PFD_DECLARE_ITEM(Skeletons,Color32(255,255,255),Fragment);
PFD_DECLARE_ITEM_ON(FragmentHighlight,Color32(255,255,255),Fragment);

namespace fragStats
{
	EXT_PF_TIMER(WalkInstances);
	EXT_PF_TIMER(TimeSlice);
}

using namespace fragStats;

fragManager*			  fragManager::sm_Instance = NULL;
fragCacheAllocator* fragManager::sm_CacheAllocator = NULL;

#if __ASSERT
int fragManager::sm_PhysicsInstancesAdded = 0;
#endif

const char* fragManager::MakeTypeMapName(const char *pFileName, char *pBuffer, int iSizeofBuffer)
{
	Assert(pFileName);
	Assert(pBuffer);
	Assert(iSizeofBuffer);
	if (!(iSizeofBuffer && pBuffer && pFileName))
	{
		return pFileName ? pFileName : "InvalidFile";
	}

	const char *pEnd = pFileName;
	if (m_UseBaseNames)
	{
		pEnd += strlen(pFileName);
		while (pEnd > pFileName)
		{
			if (*pEnd == '/' || *pEnd=='\\')
			{	
				++pEnd;
				break;
			}
			--pEnd;
		}
	}
#if __ASSERT
	else
	{
		AssertMsg(pFileName[0] == '$', "Asset filenames must start with '$'");
	}
#endif
	Assert(strlen(pEnd)<FRAG_TYPE_SHORT_NAME_LEN);
	StringNormalize(pBuffer, pEnd, iSizeofBuffer);
	return pBuffer;
}

// Init
void fragManager::CreateAllocator()
{
	if (!sm_CacheAllocator)
	{
#if !(__PS3 || __XENON) || __TOOL || __RESOURCECOMPILER
		void* ptr = MEMMANAGER.GetFragMemory();
		if (!ptr)
		{
			sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
			Assert(pAllocator);

			USE_MEMBUCKET(MEMBUCKET_PHYSICS);
			ptr = pAllocator->Allocate(FRAG_HEAP_SIZE, 16, MEMTYPE_GAME_VIRTUAL);
			sysMemManager::GetInstance().SetFragMemory(ptr);
		}
#endif	
		sm_CacheAllocator = rage_new fragCacheAllocator(MEMMANAGER.GetFragMemory(), FRAG_HEAP_SIZE, FRAG_CACHE_NODES);

#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && MEMVISUALIZE.HasFrag())
			diagTracker::GetCurrent()->InitHeap("Frag Cache", MEMMANAGER.GetFragMemory(), FRAG_HEAP_SIZE);
#endif
	}
}

fragManager::fragManager(UserDataInit callback,
						 InsertFragInstFunctor insertFragInst,
						 RemoveFragInstFunctor removeFragInst,
						 InitFragInstFunctor initFragInst,
						 DrawCallbackFunctor drawCallback,
						 const ClothInterface* clothInterface,
						 ConvertEntityClassFunctor convertEntityClass,
						 u32 numCacheEntries,
						 u16 /*numTypeStubs*/,
						 TypePagedInFunctor pagedInCallback,
						 TypePagingOutFunctor pagingOutCallback,
                         bool bSkipCacheMgrInit
						 )
	: m_CacheEntryTypeFlags(BIT(1))
	, m_CacheEntryIncludeFlags(0xffffffff)
	, m_DefaultTypeFlags(phArchetype::DEFAULT_TYPE)
	, m_DefaultIncludeFlags(0xffffffff)
	, m_UseRecyclingSphere(false)
	, m_PreventBreakingBecauseOfFramerate(false)
	, m_GlobalNoRevive(false)
	, m_UseBaseNames(false)
	, m_PartsBrokeOffFunc(MakeFunctor(&DefaultPartsBrokeOffFunc))
	, m_UserDataCallback(callback)
	, m_InsertFragInst(insertFragInst)
	, m_RemoveFragInst(removeFragInst)
	, m_InitFragInst(initFragInst)
	, m_DrawCallback(drawCallback)
	, m_ConvertEntityClass(convertEntityClass)
	, m_TypePagedIn(pagedInCallback)
	, m_TypePagingOut(pagingOutCallback)
{
	Assert(sm_Instance == NULL);
	sm_Instance = this;

	fragTypeGroup::SetNoDisappearWhenDead(PARAM_nodisappearwhendead.Get());

	if(clothInterface)
	{
		m_ClothInterface = *clothInterface;
	}

	m_CacheMgr = rage_new fragCacheManager(numCacheEntries);

	// Frag cache allocator
	CreateAllocator();

	//allocate the hash and the cache...
	if(!bSkipCacheMgrInit)
		m_CacheMgr->Init(NULL);

#if __BANK
	fragTypeGroup::sm_NoLatching = PARAM_nofraglatching.Get();
#endif // __BANK
}

int fragManager::ComputeGlassCrackTypeForGTA5(const rage::fragInst::Collision& /*collision*/)
{
	return 1;
}


fragManager::~fragManager()
{
	m_CacheMgr->Shutdown();
	delete m_CacheMgr;
	delete sm_CacheAllocator;
	sm_CacheAllocator = NULL;
    sm_Instance = NULL;
}

void fragManager::InitCacheMgr(fragInst** cacheBackingInsts)
{
	m_CacheMgr->Init(cacheBackingInsts);
}

void fragManager::SetPartsBrokeOffFunc(PartsBrokeOffFunc func)
{
	m_PartsBrokeOffFunc = func;
}

void fragManager::DefaultPartsBrokeOffFunc(fragInst* oldInst,
										   fragInst::ComponentBits& brokenGroups,
										   fragInst* newInst)
{
	oldInst->PartsBrokeOff(brokenGroups, newInst);
}

void fragManager::InitializeUserData(phInst* instance)
{
	m_UserDataCallback(instance);
}

void fragManager::Reset()
{
	//reset the cache...
	m_CacheMgr->Reset();

/*	//reset all the instances back to their starting locations
	TypeMapType::Iterator type = m_TypeMap.CreateIterator();
	for (type.Start(); !type.AtEnd(); type.Next())
	{
		fragInst*	nextInst;

		for (fragInst* inst = type.GetData()->m_ClientsHead.GetFirst(); inst; inst = nextInst)
		{
			nextInst = type.GetData()->m_ClientsHead.GetLinkFromObj(inst)->pNext;

			if (!inst->GetCached())
			{
				inst->Reset();
			}
		}
	}
*/
#if __ASSERT
	Displayf("fragManager::Reset, %d instances added", sm_PhysicsInstancesAdded);
#endif // __ASSERT
}

void fragManager::SetInterestFrustum(const grcViewport& viewport, const Matrix34& interestMatrix)
{
#if __PFDRAW
	if (!PFD_DetachInterest.WillDraw())
#endif
	{
		m_InterestShaft.Set(viewport, interestMatrix);
		m_InterestMatrix = MATRIX34_TO_MAT34V(interestMatrix);
		m_ViewportWidth = viewport.GetWidth();
		m_ViewportTanHFOV = viewport.GetTanHFOV();
	}
}
void fragManager::SimUpdate(float timeSlice)
{
#if __PFDRAW && __DEV
	if (PFD_DetachInterest.Begin())
	{
		bool oldLighting = grcLighting(false);
		m_InterestShaft.DrawDebug();
		grcLighting(oldLighting);
		PFD_DetachInterest.End();
	}
#endif

	PF_FUNC(TimeSlice);

	if (fragTune::IsInstantiated())
	{	
		m_PreventBreakingBecauseOfFramerate = FRAGTUNE->GetBreakingFrameRateLimit() >= 1.0f / timeSlice;
	}
	else
	{
		m_PreventBreakingBecauseOfFramerate = false;
	}

	/*for each type we have, if it has any clients, make sure to touch the underlying data...
	having a client means that some instance has been activated by its movable, and wants to interact 
	with the world...
	*/
#if 0	// Disabled, see comments by the "WalkInstances" loop below. /FF
	static u32 s_TouchRotate = 0;
	const u32 TOUCH_ROTATE_MASK = 0x7;
	
	++s_TouchRotate;
	if (s_TouchRotate > TOUCH_ROTATE_MASK)
	{
		s_TouchRotate = 0;
	}
#endif

	FRAGCACHEMGR->SimUpdate(timeSlice);
}

void fragManager::Update(float timeSlice, int simUpdates)
{
	FRAGCACHEMGR->Update(timeSlice,simUpdates);
}

#if __BANK

void fragManager::AddWidgets(bkBank &bank)
{
	bank.PushGroup("Fragment Manager");
	bank.AddToggle("Frag Cache Heap Debug",&s_bFragCacheHeapDebug);
	Assert(m_CacheMgr != NULL);
	m_CacheMgr->AddWidgets(bank);
	bank.PopGroup();
}
#endif // __BANK

#if __PFDRAW
void fragManager::ProfileDraw()
{
#if __BANK
	if (fragTune::IsInstantiated() && FRAGTUNE->GetTypeBeingTuned())
#endif // __BANK
	{
		m_CacheMgr->ProfileDraw();
	}
}
#endif // __PFDRAW

void fragManager::SetInterpretSpecialFlagFunc (InterpretSpecialFlagFunc func)
{
	m_InterpretSpecialFlagFunc = func;
}

void fragManager::SetInterpretSpecialFlagForIncludesFunc (InterpretSpecialFlagForIncludesFunc func)
{
	m_InterpretSpecialFlagForIncludesFunc = func;
}

void fragManager::InterpretSpecialFlag(const char* specialFlagText)
{
	if (m_InterpretSpecialFlagFunc.IsBound())
	{
		AssertMsg(m_SpecialTypeFlags.GetCount(), ("Trying to Interpret a special flag without any allocated storage. Call AddNewStorageForSpecialFlags before you start interpreting flags."));
		m_SpecialTypeFlags[(m_SpecialTypeFlags.GetCount()-1)] |= m_InterpretSpecialFlagFunc(specialFlagText);
	}

	if (m_InterpretSpecialFlagForIncludesFunc.IsBound())
	{
		AssertMsg(m_SpecialIncludeFlags.GetCount(), ("Trying to Interpret a special flag without any allocated storage. Call AddNewStorageForSpecialFlags before you start interpreting flags."));
		u32 flag = m_InterpretSpecialFlagForIncludesFunc(specialFlagText);
		//Assertf(flag, "Unknown bound flag: (%s)", specialFlagText);
		m_SpecialIncludeFlags[(m_SpecialIncludeFlags.GetCount()-1)] |= flag;
	}
}

void fragManager::AddNewStorageForSpecialFlags()
{
	if (m_InterpretSpecialFlagFunc.IsBound())
	{
		m_SpecialTypeFlags.Grow();
		m_SpecialTypeFlags[(m_SpecialTypeFlags.GetCount()-1)] = 0;
	}

	if (m_InterpretSpecialFlagForIncludesFunc.IsBound())
	{
		m_SpecialIncludeFlags.Grow();
		m_SpecialIncludeFlags[(m_SpecialIncludeFlags.GetCount()-1)] = 0;
	}
}


rmcDrawable* fragManager::GetRenderedDrawableForFragType(const fragType* type)
{
	if (m_GetRenderedDrawableForFragType.IsBound())
	{
		return m_GetRenderedDrawableForFragType(type);
	}
	else
	{
		return type->GetCommonDrawable();
	}
}

} // namespace rage
