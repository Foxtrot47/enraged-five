// 
// fragment/typegroup.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "typegroup.h"

// fragment
#include "cache.h"
#include "eventplayer.h"
#include "drawable.h"
#include "tune.h"
#include "typechild.h"

// RAGE
#include "bank/group.h"
#include "breakableglass/crackstemplate.h"
#include "file/token.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "event/set.h"
#include "parser/manager.h"
#include "pharticulated/articulatedcollider.h"
#include "phbound/boundcomposite.h"
#include "fragment/typephysicslod.h"

FRAGMENT_OPTIMISATIONS()

namespace rage {

EXT_PFD_DECLARE_ITEM(CurrentHealth);

bool fragTypeGroup::sm_NoDisappearWhenDead = false;

#if __BANK
bool fragTypeGroup::sm_NoLatching = false;
#endif // __BANK

IMPLEMENT_PLACE(fragTypeGroup);

fragTypeGroup::fragTypeGroup(f32 strength, 
							 f32 forceTransmissionScaleUp, 
							 f32 forceTransmissionScaleDown,
							 f32 jointStiffness,
							 f32 maxSoftAngle1,
							 f32 minSoftAngle1,
							 f32 maxSoftAngle2,
							 f32 maxSoftAngle3,
							 f32 rotationSpeed,
							 f32 rotationStrength,
                             f32 restoringStrength,
                             f32 restoringMaxTorque,
							 f32 latchStrength,
							 u8  numChildren,
							 u8  numChildGroups, 
							 u8  childIndex, 
							 u8  parentIndex, 
							 u8  childGroupsPointersIndex,
							 u8  flags,
							 f32 minDamageForce,
							 f32 damageHealth,
							 f32 weaponHealth,
							 f32 weaponScale,
							 f32 meleeScale,
							 f32 vehicleScale,
							 f32 pedScale,
							 f32 ragdollScale,
							 f32 explosionScale,
							 f32 objectScale,
							 f32 pedInvMassScale)
	: 
#if ENABLE_FRAGMENT_EVENTS
	  m_DeathEventset(NULL)
	, m_DeathEventPlayer(NULL) ,
#endif // ENABLE_FRAGMENT_EVENTS
     m_Strength(strength)
	, m_ForceTransmissionScaleUp(forceTransmissionScaleUp)
	, m_ForceTransmissionScaleDown(forceTransmissionScaleDown)
	, m_JointStiffness(jointStiffness)
	, m_MinSoftAngle1(minSoftAngle1)
	, m_MaxSoftAngle1(maxSoftAngle1)
	, m_MaxSoftAngle2(maxSoftAngle2)
	, m_MaxSoftAngle3(maxSoftAngle3)
	, m_RotationSpeed(rotationSpeed)
	, m_RotationStrength(rotationStrength)
    , m_RestoringStrength(restoringStrength)
    , m_RestoringMaxTorque(restoringMaxTorque)
	, m_LatchStrength(latchStrength)
    , m_TotalUndamagedMass(0.0f)
    , m_TotalDamagedMass(0.0f)
	, m_ChildGroupsPointersIndex(childGroupsPointersIndex)
	, m_ParentGroupPointerIndex(parentIndex)
	, m_ChildIndex(childIndex)
	, m_NumChildren(numChildren)
	, m_NumChildGroups(numChildGroups)
	, m_Flags(flags)
	, m_MinDamageForce(minDamageForce)
	, m_DamageHealth(damageHealth)
	, m_WeaponHealth(weaponHealth)
	, m_WeaponScale(weaponScale)
	, m_MeleeScale(meleeScale)
	, m_VehicleScale(vehicleScale)
	, m_PedScale(pedScale)
	, m_RagdollScale(ragdollScale)
	, m_ExplosionScale(explosionScale)
	, m_ObjectScale(objectScale)
	, m_PedInvMassScale(pedInvMassScale)
	, m_PresetApplied(0)
	, m_GlassModelAndType(0xff)
	, m_GlassPaneModelInfoIndex(0)
{
}

#if ENABLE_FRAGMENT_EVENTS
fragTypeGroup::fragTypeGroup(datResource& rsc)
    : m_DeathEventset(rsc)
    , m_DeathEventPlayer(rsc)
{
	if (m_DeathEventPlayer)
	{
		m_DeathEventPlayer->CreateParameterList();
		Assert(m_DeathEventset);
		m_DeathEventPlayer->SetSet(*m_DeathEventset);
	}
}
#else // ENABLE_FRAGMENT_EVENTS
fragTypeGroup::fragTypeGroup(datResource& UNUSED_PARAM(rsc))
{
}
#endif // ENABLE_FRAGMENT_EVENTS

fragTypeGroup::~fragTypeGroup()
{
#if ENABLE_FRAGMENT_EVENTS
	if (m_DeathEventPlayer)
	{
		m_DeathEventPlayer->Stop(0.0f, true);	//TMS: Stop player before destroying the set
		delete m_DeathEventPlayer;
	}
	delete m_DeathEventset;
#endif // ENABLE_FRAGMENT_EVENTS
}

#if __DECLARESTRUCT
void fragTypeGroup::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(fragTypeGroup);
#if ENABLE_FRAGMENT_EVENTS
    STRUCT_FIELD(m_DeathEventset);
    STRUCT_FIELD(m_DeathEventPlayer);
#else // ENABLE_FRAGMENT_EVENTS
	STRUCT_CONTAINED_ARRAY(m_Pad0);
#endif // ENABLE_FRAGMENT_EVENTS
	STRUCT_FIELD(m_Strength);
	STRUCT_FIELD(m_ForceTransmissionScaleUp);
	STRUCT_FIELD(m_ForceTransmissionScaleDown);
	STRUCT_FIELD(m_JointStiffness);
	STRUCT_FIELD(m_MinSoftAngle1);
	STRUCT_FIELD(m_MaxSoftAngle1);
	STRUCT_FIELD(m_MaxSoftAngle2);
	STRUCT_FIELD(m_MaxSoftAngle3);
	STRUCT_FIELD(m_RotationSpeed);
	STRUCT_FIELD(m_RotationStrength);
    STRUCT_FIELD(m_RestoringStrength);
    STRUCT_FIELD(m_RestoringMaxTorque);
	STRUCT_FIELD(m_LatchStrength);
    STRUCT_FIELD(m_TotalUndamagedMass);
    STRUCT_FIELD(m_TotalDamagedMass);
	STRUCT_FIELD(m_ChildGroupsPointersIndex);
	STRUCT_FIELD(m_ParentGroupPointerIndex);
	STRUCT_FIELD(m_ChildIndex);
	STRUCT_FIELD(m_NumChildren);
	STRUCT_FIELD(m_NumChildGroups);
	STRUCT_FIELD(m_GlassModelAndType);
	STRUCT_FIELD(m_GlassPaneModelInfoIndex);
	STRUCT_FIELD(m_Flags);
	STRUCT_FIELD(m_MinDamageForce);
	STRUCT_FIELD(m_DamageHealth);
	STRUCT_FIELD(m_WeaponHealth);
	STRUCT_FIELD(m_WeaponScale);
	STRUCT_FIELD(m_VehicleScale);
	STRUCT_FIELD(m_PedScale);
	STRUCT_FIELD(m_RagdollScale);
	STRUCT_FIELD(m_ExplosionScale);
	STRUCT_FIELD(m_ObjectScale);
	STRUCT_FIELD(m_PedInvMassScale);
	STRUCT_FIELD(m_PresetApplied);

#if FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH > 0
	STRUCT_CONTAINED_ARRAY(m_DebugName);
#endif
	STRUCT_FIELD(m_MeleeScale);

	STRUCT_END();
}
#endif // __DECLARESTRUCT


void fragTypeGroup::Set(fragTypeGroup& other)
{
    m_Strength						= other.m_Strength;
    m_ForceTransmissionScaleUp		= other.m_ForceTransmissionScaleUp;
    m_ForceTransmissionScaleDown	= other.m_ForceTransmissionScaleDown;
    m_JointStiffness				= other.m_JointStiffness;
    m_MinSoftAngle1					= other.m_MinSoftAngle1;
    m_MaxSoftAngle1					= other.m_MaxSoftAngle1;
    m_MaxSoftAngle2					= other.m_MaxSoftAngle2;
    m_MaxSoftAngle3					= other.m_MaxSoftAngle3;
    m_RotationSpeed					= other.m_RotationSpeed;
    m_RotationStrength				= other.m_RotationStrength;
    m_RestoringStrength             = other.m_RestoringStrength;
    m_RestoringMaxTorque            = other.m_RestoringMaxTorque;
    m_LatchStrength					= other.m_LatchStrength;
	
	bool madeOfGlass = GetMadeOfGlass(); // Save the made of glass flag from the pre-tuned frag
	m_Flags							= other.m_Flags;
	SetMadeOfGlass(madeOfGlass);
    
	m_MinDamageForce				= other.m_MinDamageForce;
    m_DamageHealth					= other.m_DamageHealth;
	m_WeaponHealth					= other.m_WeaponHealth;

	m_WeaponScale					= other.m_WeaponScale;
	m_MeleeScale					= other.m_MeleeScale;
	m_VehicleScale					= other.m_VehicleScale;
	m_PedScale						= other.m_PedScale;
	m_RagdollScale					= other.m_RagdollScale;
	m_ExplosionScale				= other.m_ExplosionScale;
	m_ObjectScale					= other.m_ObjectScale;

	m_PedInvMassScale				= other.m_PedInvMassScale;

	m_PresetApplied					= other.m_PresetApplied;

#if ENABLE_FRAGMENT_EVENTS
    if (other.m_DeathEventset)
    {
		if (m_DeathEventPlayer == NULL)
		{
			m_DeathEventPlayer = rage_new fragCollisionEventPlayer;
			m_DeathEventPlayer->CreateParameterList();
			Assert(m_DeathEventset == NULL);
			m_DeathEventset = rage_new evtSet;
			m_DeathEventPlayer->SetSet(*m_DeathEventset);
		}
        *m_DeathEventset                = *other.m_DeathEventset;
    }
#endif // ENABLE_FRAGMENT_EVENTS
}


#if __BANK && __PFDRAW
void fragTypeGroup::DrawPhys(int currentLOD, 
							 const Matrix34&		matrix,
							 fragHierarchyInst*		hierInst,
							 const fragType*		type,
							 bool					childHighlight,
                             bool                   selectHighlight,
                             bool                   ignoreBank,
                             unsigned int           bankIsForGroupIndex)
{
	u32 childIndex = GetChildFragmentIndex();

	// Draw some physics m_Bounds for all the extant children
	if (childHighlight || selectHighlight)
	{
        if (selectHighlight)
        {
            grcColor(Color_PaleGreen);
        }
		else
		{
			grcColor(Color_IndianRed1);
		}

		for (int child = 0; child < GetNumChildren(); ++child)
		{
			fragTypeChild *curChild = type->GetPhysics(currentLOD)->GetChild(childIndex + child);
			int groupIndex = curChild->GetOwnerGroupPointerIndex();
			bool isGroupDamaged = hierInst && hierInst->groupInsts[groupIndex].IsDamaged();
			fragDrawable* toDraw = isGroupDamaged ? curChild->GetDamagedEntity() : curChild->GetUndamagedEntity();

			if (toDraw && toDraw->GetBound())
			{
				Matrix34 worldMatrix(matrix);
				if (hierInst)
				{
					const phBoundComposite& compositeBound = *hierInst->compositeBound;
					worldMatrix.DotFromLeft(RCC_MATRIX34(compositeBound.GetCurrentMatrix(childIndex + child)));
				}
				else
				{
					worldMatrix.DotFromLeft(RCC_MATRIX34(type->GetPhysics(currentLOD)->GetCompositeBounds()->GetCurrentMatrix(childIndex + child)));
				}
				toDraw->GetBound()->Draw(RCC_MAT34V(worldMatrix));
			}
		}

		if (PFD_CurrentHealth.WillDraw() && hierInst)
		{
			fragTypeChild* child = type->GetPhysics(currentLOD)->GetChild(childIndex);
			int groupIndex = child->GetOwnerGroupPointerIndex();
			if (hierInst->groupBroken->IsClear(groupIndex))
			{
				float damageHealth = hierInst->groupInsts[groupIndex].damageHealth;

				Vector3 drawPos;
				matrix.Transform(RCC_VECTOR3(type->GetPhysics(currentLOD)->GetCompositeBounds()->GetCurrentMatrix(childIndex).GetCol3ConstRef()), drawPos);

				grcColor(Color_white);

				if (m_WeaponHealth <= 0)
				{
					grcDrawLabelf(drawPos, "D: %.0f", damageHealth);
				}
				else
				{
					float weaponHealth = hierInst->groupInsts[groupIndex].weaponHealth;
					grcDrawLabelf(drawPos, "D: %.0f\nW: %.0f", damageHealth, weaponHealth);
				}
			}
		}
	}

	// Ask all our child groups to draw
	if (m_NumChildGroups > 0)
	{
		for (u32 groupIndex = 0; groupIndex < m_NumChildGroups; groupIndex++)
		{
            u32 typeGroupIndex = m_ChildGroupsPointersIndex + groupIndex;
			// Cull the groups that don't exist *before* we call Draw on them.
			if(hierInst == NULL || hierInst->groupBroken->IsClear(typeGroupIndex))
			{
				type->GetPhysics(currentLOD)->GetGroup(typeGroupIndex)->DrawPhys(currentLOD,
					matrix,
					hierInst,
					type,
					childHighlight,
					bankIsForGroupIndex == typeGroupIndex,
					ignoreBank && bankIsForGroupIndex != typeGroupIndex,
					bankIsForGroupIndex);
			}
		}
	}
}
#endif // __BANK

void fragTypeGroup::ComputeAndSetTotalUndamagedMass(const fragPhysicsLOD* lod)
{
    m_TotalUndamagedMass = ComputeTotalUndamagedMass(lod);
}

float fragTypeGroup::ComputeTotalUndamagedMass(const fragPhysicsLOD* lod)
{
    float totalUndamagedMass = 0.0f;

    for (u8 childIndex = 0; childIndex < m_NumChildren; ++childIndex)
    {
        fragTypeChild& child = *lod->GetChild(childIndex + m_ChildIndex);

        float childUndamagedMass = child.GetUndamagedMass();
        totalUndamagedMass += childUndamagedMass;
    }

    return Max(0.001f, totalUndamagedMass);
}

void fragTypeGroup::SetTotalUndamagedMass(int currentLOD, float fMass, fragType* type)
{
	float fOriginalMass = ComputeTotalUndamagedMass(type->GetPhysics(currentLOD));
	float fMassRatio = fMass / fOriginalMass;

	for (u8 childIndex = 0; childIndex < m_NumChildren; ++childIndex)
	{
		fragTypeChild& child = *type->GetPhysics(currentLOD)->GetChild(childIndex + m_ChildIndex);
		child.SetUndamagedMass(fMassRatio * child.GetUndamagedMass());
	}

	m_TotalUndamagedMass = fMass;
}

#if ENABLE_FRAGMENT_EVENTS
void fragTypeGroup::TriggerAllBreakFromRootEvents(int currentLOD, const fragType* type, crSkeleton* skeleton, fragInst* oldInst, fragInst* newInst, atBitSet* groupBroken)
{
    u8 breakChildIndex = GetChildFragmentIndex();
    Vector3 breakPosition(FLT_MAX, FLT_MAX, FLT_MAX);

    for (int childIndex = 0; childIndex < GetNumChildren(); ++childIndex)
    {
        fragTypeChild* breakChild = type->GetPhysics(currentLOD)->GetChild(breakChildIndex + childIndex);
        Assert(breakChild);

        const evtSet* set = breakChild->GetBreakFromRootEventset();
        if (set && set->GetNumInstances() > 0)
        {
			Matrix34 breakMatrix;
            int parentGroupIndex = GetParentGroupPointerIndex();
			if(parentGroupIndex != 0xFF)
			{
				fragTypeGroup* parentGroup = type->GetPhysics(currentLOD)->GetGroup(parentGroupIndex);
				fragTypeChild* parentChild = type->GetPhysics(currentLOD)->GetChild(parentGroup->GetChildFragmentIndex());
				int breakBone = type->GetBoneIndexFromID(parentChild->GetBoneID());
				if (breakBone != -1)
				{
					crSkeleton* newSkeleton = oldInst->GetSkeleton();
					Assert(newSkeleton != NULL);
					newSkeleton->GetGlobalMtx(breakBone, RC_MAT34V(breakMatrix));
					breakPosition.Set(breakMatrix.d);
				}
			}
			else
			{
				breakMatrix = RCC_MATRIX34(oldInst->GetMatrix());
				breakPosition.Set(RCC_VECTOR3(oldInst->GetPosition()));
			}

            // Couldn't give a dog a bone, pick a point as close to the parent as possible on the child's bounding box
            if (breakPosition.x == FLT_MAX)
            {
                phBound* bound = newInst->GetArchetype()->GetBound();
                breakPosition = VEC3V_TO_VECTOR3(bound->GetBoundingBoxMin());
                RCC_MATRIX34(newInst->GetMatrix()).Transform(breakPosition);
            }

			Assert(breakChild->GetBreakFromRootEventPlayer());
            fragBreakEventPlayer& player = *breakChild->GetBreakFromRootEventPlayer();

            player.GetPosition() = RCC_VEC3V(breakPosition);	
            player.GetMatrix() = RCC_MAT34V(breakMatrix);
            player.GetInst() = (phInst*)oldInst;
            player.GetFragInst() = oldInst;
            player.GetComponent() = (int)GetChildFragmentIndex();
            player.GetOtherInst() = (phInst*)newInst;

            player.StartUpdateStop();
        }
    }

    for (int groupIndex = 0; groupIndex < GetNumChildGroups(); ++groupIndex)
    {
        int globalGroupIndex = GetChildGroupsPointersIndex() + groupIndex;
        if (groupBroken->IsClear(globalGroupIndex))
        {
            fragTypeGroup* group = type->GetPhysics(currentLOD)->GetGroup(globalGroupIndex);
            group->TriggerAllBreakFromRootEvents(currentLOD, type, skeleton, oldInst, newInst, groupBroken);
        }
    }
}
#endif // ENABLE_FRAGMENT_EVENTS

#if MESH_LIBRARY
void fragTypeGroup::LoadASCII(fiAsciiTokenizer& asciiTok, 
							  crSkeletonData* skeletonData,
							  grmShaderGroup* shaderGroup,
								
							  u8* nextGroupIndex, 
							  datOwner<fragTypeGroup>* groups, 
							  u8* numChildren, 
							  fragTypeChild** children, 
								
							  u8 parentIndex)
{
	u8		ourHierIndex;
	u8		numGroupsInGroup = 0;
	u8		childGroupsPointersIndex;
	u8		numSelfChildren = 0;
	
	fragTypeGroup* group;
	u8 childFragmentIndex = 0xff;
	f32 strength = 100.0f;
	f32 forceTransmissionScaleUp = 0.25f;
	f32 forceTransmissionScaleDown = 0.25f;
	f32 jointStiffness = 0.0f;
	f32 minSoftAngle1 = -1.0f;
	f32 maxSoftAngle1 = 1.0f;
	f32 maxSoftAngle2 = 1.0f;
	f32 maxSoftAngle3 = 1.0f;
	f32 rotationSpeed = 0.0f;
	f32 rotationStrength = 0.0f;
    f32 restoringStrength = 0.0f;
    f32 restoringMaxTorque = 0.0f;
	f32 latchStrength = 0.0f;
	// bool jointLatched = false;
	u8 flags = 0;
	u8 glassTypeIndex = 0;
	f32 minDamageForce = 100.0f;
	f32 damageHealth = 1000.0f;
	f32 weaponHealth = 0.0f;
	f32 weaponScale = 1.0f;
	f32 meleeScale = 1.0f;
	f32 vehicleScale = 1.0f;
	f32 pedScale = 1.0f;
	f32 ragdollScale = 1.0f;
	f32 explosionScale = 1.0f;
	f32 objectScale = 1.0f;
	f32 pedInvMassScale = 1.0f;
	int preset = -1;

	fragTypeChild::s_Flags = 0;

	char name[fiBaseTokenizer::PUSH_BUFFER_SIZE];
	name[0] = '\0';
	asciiTok.GetToken(name,sizeof(name));

	Assert(numChildren);

	ourHierIndex = *nextGroupIndex;
	*nextGroupIndex = ourHierIndex + 1;
	group = groups[ourHierIndex] = rage_new fragTypeGroup;

#if FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH > 0
	strncpy(group->m_DebugName, name, FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH);
	group->m_DebugName[FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH - 1] = '\0';
#endif

	//if we have any child groups, this will be the first one's index...
	childGroupsPointersIndex = *nextGroupIndex;

    bool madeFakeChildren = false;
    int firstChild = -1;

	bool meleeScaleSupplied = false;

	//there was a fragment group token - we're responsible for allocating one and filling it in...

	//need to find an open brace...
	asciiTok.CheckIToken("{", true);
	while(1)
	{
		if (asciiTok.CheckIToken("}", false))
		{
			//all done!
			break;
		}
		
		//from here on, we'll look for useful tokens...
		if (asciiTok.CheckIToken("Strength", false))
		{
			asciiTok.CheckIToken("Strength", true);
			strength = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("ForceTransmissionScaleUp", false))
		{
			asciiTok.CheckIToken("ForceTransmissionScaleUp", true);
			forceTransmissionScaleUp = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("ForceTransmissionScaleDown", false))
		{
			asciiTok.CheckIToken("ForceTransmissionScaleDown", true);
			forceTransmissionScaleDown = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("JointStiffness", false))
		{
			asciiTok.CheckIToken("JointStiffness", true);
			jointStiffness = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("MinSoftAngle1", false))
		{
			asciiTok.CheckIToken("MinSoftAngle1", true);
			minSoftAngle1 = asciiTok.GetFloat();
            minSoftAngle1 = Max(-1.0f, minSoftAngle1);
            minSoftAngle1 = Min( 1.0f, minSoftAngle1);
		}
		else if (asciiTok.CheckIToken("MaxSoftAngle1", false))
		{
			asciiTok.CheckIToken("MaxSoftAngle1", true);
			maxSoftAngle1 = asciiTok.GetFloat();
            maxSoftAngle1 = Max(-1.0f, maxSoftAngle1);
            maxSoftAngle1 = Min( 1.0f, maxSoftAngle1);
		}
		else if (asciiTok.CheckIToken("MaxSoftAngle2", false))
		{
			asciiTok.CheckIToken("MaxSoftAngle2", true);
			maxSoftAngle2 = asciiTok.GetFloat();
            maxSoftAngle2 = Max(-1.0f, maxSoftAngle2);
            maxSoftAngle2 = Min( 1.0f, maxSoftAngle2);
		}
		else if (asciiTok.CheckIToken("MaxSoftAngle3", false))
		{
			asciiTok.CheckIToken("MaxSoftAngle3", true);
			maxSoftAngle3 = asciiTok.GetFloat();
            maxSoftAngle3 = Max(-1.0f, maxSoftAngle3);
            maxSoftAngle3 = Min( 1.0f, maxSoftAngle3);
		}
		else if (asciiTok.CheckIToken("RotationSpeed", false))
		{
			asciiTok.CheckIToken("RotationSpeed", true);
			rotationSpeed = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("RotationStrength", false))
		{
			asciiTok.CheckIToken("RotationStrength", true);
			rotationStrength = asciiTok.GetFloat();
		}
        else if (asciiTok.CheckIToken("RestoringStrength", false))
        {
            asciiTok.CheckIToken("RestoringStrength", true);
            restoringStrength = asciiTok.GetFloat();
        }
        else if (asciiTok.CheckIToken("RestoringMaxTorque", false))
        {
            asciiTok.CheckIToken("RestoringMaxTorque", true);
            restoringMaxTorque = asciiTok.GetFloat();
        }
		else if (asciiTok.CheckIToken("LatchStrength", false))
		{
			asciiTok.CheckIToken("LatchStrength", true);
			latchStrength = asciiTok.GetFloat();
		}
#if ENABLE_FRAGMENT_EVENTS
        else if (asciiTok.CheckIToken("deathEvents", false))
        {
            asciiTok.CheckIToken("deathEvents", true);
            asciiTok.CheckToken("all");
            asciiTok.CheckToken("{");
            PARSER.LoadObjectPtr(asciiTok.GetStream(), (evtSet*&)group->m_DeathEventset);
            asciiTok.CheckToken("}");

			Assert(group->m_DeathEventPlayer == NULL);

			group->m_DeathEventPlayer = rage_new fragCollisionEventPlayer;
			group->m_DeathEventPlayer->CreateParameterList();
			group->m_DeathEventPlayer->SetSet(*group->m_DeathEventset);
        }
#endif // ENABLE_FRAGMENT_EVENTS
		else if (asciiTok.CheckIToken("group", false))
		{
			asciiTok.CheckIToken("group", true);

            if (!madeFakeChildren)
            {
                MakeFakeChildren(firstChild, children, numChildren, numSelfChildren);
                madeFakeChildren = true;
            }
			
			//uTempGroupIndex = *nextGroupIndex;
			fragTypeGroup::LoadASCII(asciiTok, 
									 skeletonData,
									 shaderGroup,
												
									 nextGroupIndex, 
									 groups, 
									 numChildren, 
									 children, 
												
									 ourHierIndex);

			numGroupsInGroup++;
		}
		else if (asciiTok.CheckIToken("JointLatched", false))
		{
			asciiTok.CheckIToken("JointLatched", true);
			asciiTok.GetInt(); // jointLatched = asciiTok.GetInt() != 0;
		}
		else if (asciiTok.CheckIToken("child", false))
		{
			asciiTok.CheckIToken("child", true);

			if (childFragmentIndex == 0xff)
			{
				childFragmentIndex = *numChildren;
			}

            if (firstChild == -1)
            {
			    firstChild = *numChildren;
            }

			int boneID = 0;

			if (skeletonData)
			{
				if (const crBoneData* bone = skeletonData->FindBoneData(name))
				{
					boneID = bone->GetBoneId();
				}
				else // Try replacing the first underscore with a colon, that might work
				{
					if (char* underscore = strchr(name, '_'))
					{
						*underscore = ':';
					}

					if (const crBoneData* bone = skeletonData->FindBoneData(name))
					{
						boneID = bone->GetBoneId();
					}
				}
			}

			children[*numChildren] = fragTypeChild::LoadASCII(asciiTok,
															  shaderGroup,
															  ourHierIndex,
															  //boneIndex,
															  boneID);

			Assert(children[*numChildren]);

			++(*numChildren);
			++numSelfChildren;
		}
		else if (asciiTok.CheckIToken("damagedChild", false))
		{
			asciiTok.CheckIToken("damagedChild", true);

			children[childFragmentIndex]->LoadDamagedASCII(asciiTok, shaderGroup);
		}
		else if (asciiTok.CheckIToken("DisappearsWhenDead", false))
		{
			asciiTok.CheckIToken("DisappearsWhenDead", true);

			flags |= FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD;
		}
		else if (asciiTok.CheckIToken("MadeOfGlass", false))
		{
			asciiTok.CheckIToken("MadeOfGlass", true);

			flags |= FRAG_GROUP_FLAG_MADE_OF_GLASS;
		}
		else if (asciiTok.CheckIToken("DamageWhenBroken", false))
		{
			asciiTok.CheckIToken("DamageWhenBroken", true);

			flags |= FRAG_GROUP_FLAG_DAMAGE_WHEN_BROKEN;
		}
		else if (asciiTok.CheckIToken("DoesntAffectVehicles", false))
		{
			asciiTok.CheckIToken("DoesntAffectVehicles", true);

			flags |= FRAG_GROUP_FLAG_DOESNT_AFFECT_VEHICLES;
		}
		else if (asciiTok.CheckIToken("HasCloth", false))
		{
			asciiTok.CheckIToken("HasCloth", true);

			flags |= FRAG_GROUP_FLAG_HAS_CLOTH;
		}
		else if (asciiTok.CheckIToken("DoesntPushVehiclesDown", false))
		{
			asciiTok.CheckIToken("DoesntPushVehiclesDown", true);

			flags |= FRAG_GROUP_FLAG_DOESNT_PUSH_VEHICLES_DOWN;
		}
		else if (asciiTok.CheckIToken("GlassType", false))
		{
			asciiTok.CheckIToken("GlassType", true);
			char glassTypeName[255];
			asciiTok.GetToken(glassTypeName, sizeof(glassTypeName));
			bgCracksTemplate* pCracksTemplate;
			const char* szErrMsg;
			if (Verifyf(bgCracksTemplate::IsInitialized(pCracksTemplate, &szErrMsg),
				"Can't parse glass type; breakableglass library not initialized: %s\n", szErrMsg))
			{
				const atArray<bgGlassTypeConfig>& glassTypes = pCracksTemplate->GetGlassTypes();
				for (glassTypeIndex = 0; glassTypeIndex < glassTypes.GetCount(); glassTypeIndex++)
				{
					if (0 == stricmp(glassTypeName,glassTypes[glassTypeIndex].GetTypeName()))
						break;
				}
				if (!Verifyf(glassTypeIndex<glassTypes.GetCount(), "Breakable glass pane type %s not found!  Defaulting to %s.\n", glassTypeName, glassTypes[0].GetTypeName().c_str()))
				{
					glassTypeIndex = 0;
				}
			}
		}
		else if (asciiTok.CheckIToken("minDamageForce", false))
		{
			asciiTok.CheckIToken("minDamageForce", true);

			minDamageForce = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("damageHealth", false))
		{
			asciiTok.CheckIToken("damageHealth", true);

			damageHealth = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("weaponHealth", false))
		{
			asciiTok.CheckIToken("weaponHealth", true);

			weaponHealth = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("weaponScale", false))
		{
			asciiTok.CheckIToken("weaponScale", true);

			weaponScale = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("meleeScale", false))
		{
			asciiTok.CheckIToken("meleeScale", true);

			meleeScaleSupplied = true;
			meleeScale = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("vehicleScale", false))
		{
			asciiTok.CheckIToken("vehicleScale", true);

			vehicleScale = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("pedScale", false))
		{
			asciiTok.CheckIToken("pedScale", true);

			pedScale = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("ragdollScale", false))
		{
			asciiTok.CheckIToken("ragdollScale", true);

			ragdollScale = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("explosionScale", false))
		{
			asciiTok.CheckIToken("explosionScale", true);

			explosionScale = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("objectScale", false))
		{
			asciiTok.CheckIToken("objectScale", true);

			objectScale = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("pedInvMassScale", false))
		{
			asciiTok.CheckIToken("pedInvMassScale", true);

			pedInvMassScale = asciiTok.GetFloat();
		}
		else if (asciiTok.CheckIToken("preset", false))
		{
			asciiTok.CheckIToken("preset", true);

			char presetName[256];
			asciiTok.GetToken(presetName, sizeof(presetName));

			if (fragTune::IsInstantiated())
			{
				preset = FRAGTUNE->FindPresetByName(presetName);
			}
		}
		else
		{
			char tokenBuffer[256];
			asciiTok.GetToken(tokenBuffer, 256);
			Warningf("Found unknown token '%s' in group.  Ignoring.", tokenBuffer);
		}
	}
	//skip the ending brace...
	asciiTok.CheckIToken("}", true);

    if (!madeFakeChildren)
    {
        MakeFakeChildren(firstChild, children, numChildren, numSelfChildren);
    }
	
	//we either need to have a child or some groups...
	Assert(childFragmentIndex != 0xFF || numGroupsInGroup);

	// Add back in any flags that the child might have picked up while it was parsing.
	flags |= fragTypeChild::s_Flags;

	if(!meleeScaleSupplied)
	{
		// Copy the weapon scale if no melee scale was supplied
		meleeScale = weaponScale;
	}
	
	group->Set(strength, 
			   forceTransmissionScaleUp, 
			   forceTransmissionScaleDown,
			   jointStiffness,
			   minSoftAngle1,
			   maxSoftAngle1,
			   maxSoftAngle2,
			   maxSoftAngle3,
			   rotationSpeed,
			   rotationStrength,
               restoringStrength,
               restoringMaxTorque,
			   latchStrength,
			   numSelfChildren,
			   numGroupsInGroup, 
			   childFragmentIndex, 
			   parentIndex, 
			   childGroupsPointersIndex,
			   flags,
			   glassTypeIndex,
			   minDamageForce,
			   damageHealth,
			   weaponHealth,
			   weaponScale,
			   meleeScale,
			   vehicleScale,
			   pedScale,
			   ragdollScale,
			   explosionScale,
			   objectScale,
			   pedInvMassScale);

	if (fragTune::IsInstantiated())
	{
		FRAGTUNE->ApplyBreakPresetToGroup(*group, preset);
	}
}
#endif

void fragTypeGroup::MakeFakeChildren(int firstChild, fragTypeChild** children, u8* numChildren, u8& numSelfChildren)
{
    if (firstChild != -1)
    {
        // If a child had more than one bound, make some fake groups and children to hold them
        if (fragDrawable* undamagedDrawable = children[firstChild]->GetUndamagedEntity())
        {
            int numExtra = undamagedDrawable->GetNumExtraBounds();

            // If there is a damaged drawable, make a number of extra children based on the max count in
            // either the damaged and undamaged entities.
            fragDrawable* damagedDrawable = children[firstChild]->GetDamagedEntity();
            if (damagedDrawable)
            {
                numExtra = Max(numExtra, damagedDrawable->GetNumExtraBounds());
            }

            for (u8 numSubChildren = 0; numSubChildren < numExtra; ++numSubChildren)
            {
                children[*numChildren] = rage_new fragTypeChild(
					children[firstChild]->GetBoneID(),
					NULL, NULL,
                    children[firstChild]->GetUndamagedMass(),
                    children[firstChild]->GetDamagedMass(),
                    children[firstChild]->GetOwnerGroupPointerIndex(),
                    0);
                    //children[firstChild]->GetBoneIndexDeprecated());

                fragDrawable* newUndamagedDrawable = rage_new fragDrawable;
                children[*numChildren]->m_UndamagedEntity = newUndamagedDrawable;

                if (numSubChildren < undamagedDrawable->GetNumExtraBounds())
                {
                    newUndamagedDrawable->SetBound(undamagedDrawable->GetExtraBound(numSubChildren));

                    //take ownership of the extra bound by zeroing out the original pointer
                    undamagedDrawable->SetExtraBound(numSubChildren, NULL);

                    newUndamagedDrawable->SetBoundMatrix(undamagedDrawable->GetExtraBoundMatrix(numSubChildren));
                }
                else
                {
                    // In this case, we have more bounds in the damaged version than the undamaged one,
                    // which means we will have some NULL's in the undamaged version of the model. Rather
                    // than try to track down all the places were we are assuming there is an undamaged
                    // entity, we are creating one but having its bound be NULL.
                    newUndamagedDrawable->SetBound(NULL);
                }

                if (damagedDrawable && numSubChildren < damagedDrawable->GetNumExtraBounds())
                {
                    fragDrawable* newDamagedDrawable = rage_new fragDrawable;
                    newDamagedDrawable->SetBound(damagedDrawable->GetExtraBound(numSubChildren));

                    //take ownership of the extra bound by zeroing out the original pointer
                    damagedDrawable->SetExtraBound(numSubChildren, NULL);

                    newDamagedDrawable->SetBoundMatrix(damagedDrawable->GetExtraBoundMatrix(numSubChildren));

                    children[*numChildren]->m_DamagedEntity = newDamagedDrawable;
                }

                ++(*numChildren);
                ++numSelfChildren;
            }
        }
    }
}

#if __BANK
void fragTypeGroup::SaveASCII(fiAsciiTokenizer& T, datOwner<fragTypeGroup>* groups, fragTypeChild** children)
{
	T.StartLine();	T.PutDelimiter("group "); T.PutDelimiter(GetDebugName()); T.PutDelimiter("\t");
	T.StartBlock();

#if ENABLE_FRAGMENT_EVENTS
    if (m_DeathEventset && m_DeathEventset->GetNumInstances() > 0)
    {
        T.StartLine(); T.PutDelimiter("deathEvents all\t");
        T.StartBlock();

        PARSER.SaveObject(T.GetStream(), (evtSet*)m_DeathEventset);

        T.EndLine();
        T.EndBlock();
    }
#endif // ENABLE_FRAGMENT_EVENTS

	T.StartLine();	T.PutDelimiter("strength\t");					T.Put(m_Strength);							T.EndLine();
	T.StartLine();	T.PutDelimiter("forceTransmissionScaleUp\t");	T.Put(m_ForceTransmissionScaleUp);			T.EndLine();
	T.StartLine();	T.PutDelimiter("forceTransmissionScaleDown\t");	T.Put(m_ForceTransmissionScaleDown);		T.EndLine();
	T.StartLine();	T.PutDelimiter("jointStiffness\t");				T.Put(m_JointStiffness);					T.EndLine();
	T.StartLine();	T.PutDelimiter("minSoftAngle1\t");				T.Put(m_MinSoftAngle1);						T.EndLine();
	T.StartLine();	T.PutDelimiter("maxSoftAngle1\t");				T.Put(m_MaxSoftAngle1);						T.EndLine();
	T.StartLine();	T.PutDelimiter("maxSoftAngle2\t");				T.Put(m_MaxSoftAngle2);						T.EndLine();
	T.StartLine();	T.PutDelimiter("maxSoftAngle3\t");				T.Put(m_MaxSoftAngle3);						T.EndLine();
	T.StartLine();	T.PutDelimiter("rotationSpeed\t");				T.Put(m_RotationSpeed);						T.EndLine();
	T.StartLine();	T.PutDelimiter("rotationStrength\t");			T.Put(m_RotationStrength);					T.EndLine();
    T.StartLine();	T.PutDelimiter("restoringStrength\t");			T.Put(m_RestoringStrength);					T.EndLine();
    T.StartLine();	T.PutDelimiter("restoringMaxTorque\t");			T.Put(m_RestoringMaxTorque);				T.EndLine();
	T.StartLine();	T.PutDelimiter("latchStrength\t");				T.Put(m_LatchStrength);						T.EndLine();

	if (GetDisappearsWhenDead())
	{
		T.StartLine();	T.PutDelimiter("disappearsWhenDead\t");		T.EndLine();
	}
	if (GetMadeOfGlass())
	{
		T.StartLine();	T.PutDelimiter("madeOfGlass\t");			T.EndLine();
	}
	if (GetDamageWhenBroken())
	{
		T.StartLine();	T.PutDelimiter("damageWhenBroken\t");		T.EndLine();
	}
	if (GetDoesntAffectVehicles())
	{
		T.StartLine();	T.PutDelimiter("doesntAffectVehicles\t");	T.EndLine();
	}
	if (GetDoesntPushVehiclesDown())
	{
		T.StartLine();	T.PutDelimiter("doesntPushVehiclesDown\t");	T.EndLine();
	}
	T.StartLine();	T.PutDelimiter("minDamageForce\t");				T.Put(m_MinDamageForce);					T.EndLine();
	T.StartLine();	T.PutDelimiter("damageHealth\t");				T.Put(m_DamageHealth);						T.EndLine();
	T.StartLine();	T.PutDelimiter("weaponHealth\t");				T.Put(m_WeaponHealth);						T.EndLine();

	T.StartLine();	T.PutDelimiter("weaponScale\t");				T.Put(m_WeaponScale);						T.EndLine();
	T.StartLine();	T.PutDelimiter("meleeScale\t");					T.Put(m_MeleeScale);						T.EndLine();
	T.StartLine();	T.PutDelimiter("vehicleScale\t");				T.Put(m_VehicleScale);						T.EndLine();
	T.StartLine();	T.PutDelimiter("pedScale\t");					T.Put(m_PedScale);							T.EndLine();
	T.StartLine();	T.PutDelimiter("ragdollScale\t");				T.Put(m_RagdollScale);						T.EndLine();
	T.StartLine();	T.PutDelimiter("explosionScale\t");				T.Put(m_ExplosionScale);					T.EndLine();
	T.StartLine();	T.PutDelimiter("objectScale\t");				T.Put(m_ObjectScale);						T.EndLine();

	T.StartLine();	T.PutDelimiter("pedInvMassScale\t");			T.Put(m_PedInvMassScale);					T.EndLine();

	const char* presetName = "<none>";
	if (fragTune::IsInstantiated())
	{
		presetName = FRAGTUNE->GetNameForPreset(m_PresetApplied - 1);
	}

	T.StartLine();	T.PutDelimiter("preset\t");						T.Put(presetName);							T.EndLine();

	if (m_ChildIndex != 0xFF)
	{
		for (int childIndex = 0; childIndex < m_NumChildren; ++childIndex)
		{
			children[m_ChildIndex + childIndex]->SaveASCII(T);
		}
	}

	for (int group = 0; group < m_NumChildGroups; ++group)
	{
		groups[m_ChildGroupsPointersIndex + group]->SaveASCII(T, groups, children);
	}

	T.EndBlock();
}
#endif // __BANK

} // namespace rage
