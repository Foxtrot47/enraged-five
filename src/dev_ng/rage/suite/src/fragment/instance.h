// 
// fragment/instance.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_INSTANCE_H
#define FRAGMENT_INSTANCE_H

#include "fragmentconfig.h"

// RAGE
#include "atl/dlistsimple.h"
#include "data/struct.h"
#include "fragment/type.h"
#include "paging/ref.h"
#include "physics/levelbase.h"
#include "physics/instbreakable.h"
#include "spatialdata/sphere.h"
#include "vector/quantize.h"
#include "vector/vector4.h"


// TODO: apparently virtual void Update is empty and no class override it, so can be removed
// I'm  #if it for now, in case something comes up 
// svetli
#define		USE_VIRTUAL_UPDATE			0	
#define ENABLE_PRERENDER_BOUNDS_UPDATE (1)

namespace rage {

template <int _T ,typename _BT> class atFixedBitSet;
class BreakStruct;
class crSkeleton;
struct fragBreakDamage;
class fragCacheEntry;
class fragCacheKey;
struct fragFindBreakInfo;
struct fragHierarchyInst;
class phArticulatedBody;
class phArticulatedCollider;
class phLevel;

/*
  PURPOSE
	A subclass of the physics instance for fragment instances. This class does all the computation
	of determining when to break, by overriding the virtual functions of the phInstBreakable.

  NOTES
	The fragInst is only twice as big as a phInst by itself, 128 bytes instead of 64. It is intended
	to be used for large number of props, etc. throughout a large level. Most of the working data
	other than the matrix is in the fragType to which the fragInst is connected.

	When you construct a fragInst, you must pass it either a fragType or a fragTypeStub. Which one
	you choose controls which of two streaming modes you are using this fragInst in. You use a fragType
	if you have taken care of loading the actual type data yourself. If you pass in a fragTypeStub,
	then the fragInst will automatically take care of not drawing and not being physical until the 
	associated fragType pages in.

	After construction, you will eventually want to call fragInst::Insert. That tells the system to 
	add it to the physics level and the movable system for drawing. (If the fragType data is not ready
	yet, then it will wait until it is).

	The other thing that can happen to fragInst's is that they can be "cached". This means that they
	are assigned a fragCacheEntry, which is a very large piece of data assigned to fragInst's that
	are activated. It contains the necessary additional information that goes into an object after
	the user interacts with it, e.g. parts that have broken off, a skeleton, damage for each part.

  SEE ALSO
    fragType, fragTypeStub, Movable_World, fragCacheEntry
<FLAG Component>
*/

class fragInst : public phInstBreakable
{
friend class fragCacheEntry;
friend class fragManager;
public:

	enum
	{
		PH_FRAG_INST = PH_INST_RAGE_LAST + 1,
		PH_INST_GDF_LAST = PH_FRAG_INST
	};

	enum ePhysicsLOD
	{
		RAGDOLL_LOD_HIGH = 0,
		RAGDOLL_LOD_MEDIUM,
		RAGDOLL_LOD_LOW,
		PHYSICS_LOD_MAX
	};

	// PURPOSE: Passed into CollisionDamage to inform derived classes of damage being taken from a collision
	struct Collision
	{
		Vec3V		position;
		Vec3V		normal;
		Vec3V		relativeVelocity;
		float		relativeSpeed;
		float		damage;
		int			component;
		int			part;
		phInst*		otherInst;
		int			otherComponent;
		int			otherPart;
#if ENABLE_FRAGMENT_EVENTS
		bool		bApplyEvents:1;
#endif // ENABLE_FRAGMENT_EVENTS

		Collision() 
#if ENABLE_FRAGMENT_EVENTS
			: bApplyEvents(true)
#endif // ENABLE_FRAGMENT_EVENTS
		{}
	};

	DECLARE_PLACE(fragInst);
    fragInst();
	fragInst(const fragType* type, const Matrix34& matrix, u32 guid = 0);
	fragInst(const fragType* type, Mat34V_In pose, u32 guid = 0);
	fragInst(datResource& rsc);
	~fragInst();

    // PURPOSE:
    //   Set the type of a fragInst after construction
    // NOTES:
    //   - Can only be done if the type has not already been set (the default constructor must be used)
    //   - fragTypes are used directly if you are handling the streaming of the type data yourself
    void SetType(fragType* type);

	// PURPOSE:
	//   Exchanges the type of a fragInst for another equivalent one.
	void ExchangeType(fragType* type);

    // PURPOSE:
    //   Sets the reset matrix of the instance
    // NOTES
    //   - Also called the "original matrix", this is where the instance will return to when Reset is called.
    //   - Normally set in the constructor, this function allows the reset matrix to be changed after creation.
    void SetResetMatrix(const Matrix34& resetMatrix);

    // PURPOSE:
    //   Sets the GUID of the instance, for cache identification purposes.
    // NOTES:
    //   - Cannot be set on instances that are already Inserted, since by then their GUID has been used by the cache manager already.
    void SetGuid(u32 guid);

	// PURPOSE:
	//   Start drawing this fragInst, and insert it into the physics level.
    // PARAMS:
    //   searchCache - Look for a cache entry with a matching GUID, in case we are re-inserting an inst that has
    //                 a cache entry outstanding already. That avoids this scenario:  1. Break lamp post. 2. Drive
    //                 around block 3. Observe both broken lamp post on the ground AND a healed lamp post standing
    //                 up straight
	// NOTES:
	//   If this inst was constructed with a fragTypeStub, Insert also adds this instance to its
	//   fragTypeStub's client list, meaning that it will get notified if the associated fragType pages
	//   in or out. The instance will not actually be added to the level until its fragType also gets paged in.
	virtual bool Insert(bool searchCache = true);

	// PURPOSE:
	//   The reverse of Insert, to stop drawing this instance and delete it from the physics level.
	// NOTES:
	//   Also removes this instance from the cache if necessary.
	virtual void Remove();

	// This will ensure that this instance is in fact in the fragment cache.  Do not call it on an already cached instance (it will assert on that).
	// This is necessary in case you want to get an object into the cache without activating it, such as when you want to manually animate the bounds of the instance.
	// If you are also going to activate it, you can still just call ActivateWhenHit().  That will take care of getting it into the cache as well.
	virtual fragCacheEntry *PutIntoCache();

	// PURPOSE: Remove the instance from the cache and put it back where it was constructed
	virtual void Reset();

	// PURPOSE: Hook this instance to a cache entry, should only be called from fragCacheEntry
	virtual void SetCacheEntry(fragCacheEntry* entry);

	// PURPOSE: Get the cache entry if this instance is in the cache
	// NOTES:
	//   The most interesting part of a cache entry is in the fragHierarchyInst, which is obtained
	//   by calling GetHierInst().
	fragCacheEntry* GetCacheEntry() const;

	// PURPOSE: Returns true if this instance is in the cache
	bool GetCached() const;

	// PURPOSE: Returns true if this instance has a cache entry and that cache entry has been initialized.
	// NOTE: Use this if you want to safely access data in the cache entry. Using GetCached only tells if
	// a cache entry has been allocated for this instance, not that it has been initialized and
	// ready for use.
	bool IsCacheEntryReady() const;

	// PURPOSE: Find the articulated link index of a component, even if the collider is inactive
	int GetLinkIndexFromComponent(int componentIndex) const;

	// PURPOSE: Switches from a simplified collider back to articulated
	void ForceArticulatedColliderMode();

	// PURPOSE: Switches from an articulated collider with one part to a phCollider
	void CheckSimpleColliderMode();

	// Returns whether or not Insert() has been called since object creation or Remove().
	bool GetInserted() const;

	// Some handy body and articulated collider accessors
	bool HasArticulatedBody() const;
	phArticulatedBody * GetArticulatedBody(); 
	bool HasArticulatedCollider() const;
	phArticulatedCollider * GetArticulatedCollider();

	inline fragPhysicsLOD * GetTypePhysics() { return m_Type->GetPhysics(m_CurrentLOD); }
	inline fragPhysicsLOD * GetTypePhysics() const { return m_Type->GetPhysics(m_CurrentLOD); }

	// PURPOSE:
	//   Returns true if any parts of the instance are broken off, or the instance is
	//   broken off from the ground.
    // NOTES:
    //   Also returns true for any object that is not attached to the ground via tuning, i.e. MinMoveForce == zero
	bool GetBroken() const;
	void SetBroken(bool broken);
	
	// PURPOSE:
	//   Return true if the user has chosen "no revive" for this instance
	// SEE ALSO:
	//   SetNoRevive
	bool GetNoRevive() const;

	// PURPOSE:
	//   Tell this instance how to behave when its cache entry is recycled
	// PARAMS:
	//   noRevive - if true, this instance will remove itself from the level, when it is aborted from the cache
	//              if false, this instance will reset itself to its unbroken starting state, when it is aborted from the cache
	// NOTES:
	//  - The default "no revive" behavior is false, so instances will reset back to their initialization
	//    state when they are recycled.
	//  - If you choose turn on "no revive" by calling this function, you can later Reset that instance back to its
	//    initial, unbroken but extant state.
	// SEE ALSO:
	//   GetNoRevive
	void SetNoRevive(bool noRevive = true);

    void SetManualSkeletonUpdate(bool manualUpdate = true);
    bool GetManualSkeletonUpdate() const;

    void SetDead(bool manualUpdate = true);
    bool GetDead() const;

	//PURPOSE: Enable/disable activation of this instance. Default setting comes from type data unless this function is called
	void SetDisableActivation(bool bDisable);

	//PURPOSE: Enable/disable breaking of this instance. Default setting comes from type data unless this function is called
	void SetDisableBreakable(bool bDisable);

	//PURPOSE: Enable/disable damaging of this instance. Default setting comes from type data unless this function is called
	void SetDisableDamage(bool bDisable);

	// PURPOSE:Reset disabling of activation to the default state of the fragType
	void ResetDisableActivation();

	// PURPOSE: Reset disabling of breaking to the default state of the fragType
	void ResetDisableBreakable();

	// PURPOSE: Reset disabling of damage to the default state of the fragType
	void ResetDisableDamage();

	// PURPOSE: Determines if the user has overridden the activation disabled flag
	// RETURN: true if the flag was overridden
	bool IsActivationFlagOverridden() const;

	// PURPOSE: Determines if the user has explicitly prevented this fragment from activating or if the type has disabled activation
	// RETURN: true if activation is disabled
	bool IsActivationDisabled() const;

	// PURPOSE: Determines if the user has overridden the breaking disabled flag
	// RETURN: true if the flag was overridden
	bool IsBreakingFlagOverridden() const;

	// PURPOSE: Determines if the user has explicitly prevented this fragment from breaking or if the type has disabled breaking
	// RETURN: true if breaking is disabled
	bool IsBreakingDisabled() const;

	// PURPOSE: Determines if the user has overridden the damage disabled flag
	// RETURN: true if the flag was overridden
	bool IsDamageFlagOverridden() const;

	// PURPOSE: Determines if the user has explicitly prevented this fragment from damaging or if the type has disabled damaging
	// RETURN: true if damaging is disabled
	bool IsDamageDisabled() const;

#if ENABLE_FRAG_OPTIMIZATION
	void SetDisableCacheOnInsertion(bool disabled);

	bool IsCacheOnInsertionDisabled() const;
#endif

	bool IsBoundsPrerenderUpdateEnabled() const;
	void SetBoundsPrerenderUpdate(bool bEnable = true);

	bool IsBoundsPrerenderCopyLastMatricesEnabled() const;
	void SetBoundsPrerenderCopyLastMatrices(bool bEnable = true);

	// PURPOSE:
	//   Set/retrieve the state that the instance will have when it is Insert()ed.
	// NOTES:
	//  - This is *NOT* returning the current state of the object in the physics level.
	//  - GetFixed() and SetFixed() have been removed.  Use these functions in the obvious way to obtain the same functionality.
	void SetInsertionState(phLevelBase::eObjectState insertionState);
	phLevelBase::eObjectState GetInsertionState() const;

	// PURPOSE: Returns true if any parts of the instance are broken off
	bool GetPartBroken() const;
	bool GetGroupBroken(int groupIndex) const;
	bool GetChildBroken(int childIndex) const;

	int GetComponentFromBoneIndex(s32 boneIndex) const;
	int GetControllingComponentFromBoneIndex(s32 boneIndex) const;
	int GetGroupFromBoneIndex(s32 boneIndex) const;
	bool GetBoneMatrix(Matrix34& result, s32 boneIndex, bool lastMatrix);

	// PURPOSE:
	//   Propagate an impulse magnitude up and down the object hierarchy, filling out an array of fragBreakDamage's specifying the amount of impulse that
	//     was felt for each group.  Note that the calling object is const and so is not modified at all.
	// PARAMS:
	// mag - a magnitude that correlates to the strength of the impulse applied.  What exactly this magnitude represents depends on the value of fromWhere
	//   (see the comments below for more info on what fromWhere tells us). If sourceGroup >= currentGroup then the mag represents the impulse that has
	//   already reached currentGroup. If sourceGroup < currentGroup then mag is the amount of impulse that was felt by our parent.
	// tempBreakDamage - the array of fragBreakDamage's that we wanted filled out.
	// currentGroup - the group currently processing the impulse
	// sourceGroup - the group which propagated the impulse to currentGroup. This should be set equal to currentGroup for the initial call. If
	//   sourceGroup < currentGroup then sourceGroup is the parent of currentGroup. If sourceGroup > currentGroup  then sourceGroup is a child of currentGroup
	void PropagateAndAccumulateBreakingImpulse(f32 mag, 
											   fragBreakDamage* tempBreakDamage, 
											   u8 currentGroup, 
											   u8 sourceGroup) const;

	// PURPOSE:
	//   A utility function, causes this instance to break part off, so that everything above
	//   a particular component in the hierarchy is separated from everything below.
	// PARAMS:
	//   component - The component which will be separated into a new inst, along with all of its children
	// RETURNS: The inst representing the part that was broken off, or NULL if no instance was created.
	// NOTES:
	//   1. This operation will fail if the fragment cache is full, or if a strength of -1 is specified
	//      for the group that is being asked to break.
	//   2. The normal breaking procedure is followed, so events associated with the break will be triggered.
	//   3. This is only to be used for mid-tree breaks, not breaking off the whole entry (use BreakOffRootInstead)
	fragInst* BreakOffAbove(int component);
	fragInst* BreakOffAboveGroup(int groupIndex);

	// PURPOSE:
	//  Break the fragment off at the root, not creating a new cache entry
	void BreakOffRoot();

	// PURPOSE: Delete the given group and all child groups, recursively
	// PARAMS:
	//  groupIndex - index of group to delete
	// NOTES:
	//  1. At the moment we don't support deleting the whole object because game-side code will crash later. 
	//      If you pass in a group that would result in us deleting all groups, we will assert and do nothing.
	//  2. Passing in an already broken group is acceptable, we simply wont do anything. 
	void DeleteAbove(int childIndex);
	void DeleteAboveGroup(int groupIndex);

	void RestoreAbove(int childIndex);
	void RestoreAboveGroup(int groupIndex);

	void DamageAllGroups();
	void ReduceGroupDamageHealth(int groupIndex, float newHealth, Vec3V_In position, Vec3V_In relativeVelocity FRAGMENT_EVENT_PARAM(bool applyEvents = true));
	void DamageGroupByComponent(int component FRAGMENT_EVENT_PARAM(bool applyEvents = true));
	void IncreaseGroupDamageHealth(int groupIndex, float newHealth);
	void FixGroupByComponent(int component);
	void SetGroupDamageHealth(int groupIndex, float newHealth);

	void ShatterGlassOnBone(int boneIndex, Vec3V_In position, Vec3V_In impact, int glassCrackType);

	// PURPOSE:
	//   A utility function, causes this instance to break its latch, so that it starts being articulated.
	//   This only works if the relevant joint has a non-zero latch strength.
	// PARAMS:
	//   component - The component that has the latched joint
	// NOTES:
	//   1. This operation will fail if the fragment cache is full, or if a strength of -1 is specified
	//      for the group that is being asked to break.
	//   2. The normal breaking procedure is followed, so events associated with the break will be triggered.
	void OpenLatchAbove(int component);

	// PURPOSE:
	//   A utility function, causes this instance to close its latch, so that it stops being articulated.
	//   This only works if the relevant joint has a non-zero latch strength, and the latch is currently open.
	// PARAMS:
	//   component - The component that has the unlatched joint
	// NOTES:
	//   1. This operation will fail if the fragment cache is full, or if a strength of -1 is specified
	//      for the group that is being asked to break.
	void CloseLatchAbove(int component);

    // PURPOSE:
    //   Returns the fragment instance this one broke off from.
	// NOTES:
	//   If this instance hasn't broken, GetParent will return this instance.
    fragInst* GetParent();

	// This instance needs to be assigned a cache entry into order to be drawn.
	virtual bool ShouldBeInCacheToDraw() const;

	// Return the culling sphere for rendering
	const spdSphere& GetXFormSphere() const;

	// PURPOSE: Return the globally unique ID of this instance
	// SEE ALSO: m_Guid
	u32 GetGuid() const;
	void SetBounds(const Vector4& bounds);
	void SetBoundSphere(const Vector4& boundSphere);

	const Vector3& GetOrgPos() const;

	/*retuns a [0, 1] value, where 0 means has no impact, and 1 means has a lot of impact...invMaxDistance is one over
	the maximum distance(the distance at which there is no impact).  The observer matrices represent a list of things 
	and their orientations(user cameras) to use for angle tests.  They are assume to be localToWorld matrices, 
	such that multiplying (0, 0, 1) by each matrix will get a view direction in world space.  If bUseDistanceOnly is true, 
	then there are no angle tests, else the distance impact is scaled by a factor of the angle off the front(Z) vector.  The 
	return value is the highest impact value for each matrix supplied...
	*/
	f32 GetImpact(f32 invMaxDistance, bool useDistanceOnly, const Matrix34** observerMats, u32 numObserverMats);

	const fragType* GetType() const;

	// Return the art asset ID of this instance if there is one
	// NOTES: really more of a type-related function, but we need the ability to change the
	// behavior of this based on logic in the fragmentnm module
	virtual int GetARTAssetID() const;

	u32 GetPackedEulers() const;

	// PURPOSE: Convert the packed Euler angles to the position and orientation matrix for this instance.
	// PARAMS:
	//	matrix - reference into which to write the position and orientation matrix
	void GetMatrixFromEulers (Matrix34& matrix) const;

	// Called externally (usually through the functor registered with the fragManager) to draw the instance
	bool AddToDrawList();

	// Copy the matrices over from the bounds to the rendering skeleton, using the necessary attachments
	void PoseSkeletonFromBounds();

	// Copy the matrices over from the rendering skeleton to the bounds, using the necessary attachments
	virtual void PoseBoundsFromSkeleton(bool onlyAdjustForInternalMotion, bool updateLastBoundsMatrices, bool fullBvhRebuild = true, s8 numFragsToDisable = 0, const s8 *pFragsToDisable = NULL);

	void PoseBoundsFromSkeleton (const crSkeleton& skeleton, phBoundComposite& bound);

	// Copy the matrices over from the bounds to the articulated body, using the necessary attachments
	void PoseArticulatedBodyFromBounds(s8 numFragsToDisable = 0, const s8 *pFragsToDisable = NULL);
	void PoseArticulatedBodyFromBounds (const phBoundComposite& bound, phArticulatedBody& body);

	// Copy the matrices over from the articulated body to the bounds, using the necessary attachments
	void PoseBoundsFromArticulatedBody();

    // Copy the matrices from the articulated body to the skeleton, using the necessary attachments
    void PoseSkeletonFromArticulatedBody();

	// PURPOSE: Set muscle target angles from the given frame.
	// PARAMS:
	//	animFrame - a frame that will be the target pose for the articulated body's muscles
	//  savedAnimPose - a frame that will be used to preserve the current pose once the function is finished setting the muscles.
	//           This used to be allocated and deallocated within this function, but that's very inefficient.
	// NOTES:	This can be used to make an articulated body attempt to follow an animation while reacting to collisions.
	//  Many factors will determine how accurate an active pose will be:
	//	1) Collisions w/ environment and other external forces like gravity.
	//	2) Joint stiffness and damping parameters.  These can be controlled on a joint level or with SetStiffnessWithNMValue.
	//	3) How close the core kinematic rig and physics rig match up.  Simplifying physics rigs, joint DOFs, and restricting joint limits
	//     will all detract from an accurate active pose.
	//  If erratic behavior occurs, try reducing muscle angle strength on the offending joints.
	void SetMusclesFromFrame (const crFrame& animFrame, crFrame& savedAnimPose);

	// PURPOSE: Set muscle target angles from the given posed skeleton.
	// PARAMS:
	//	skeleton - a posed skeleton that will be the target pose for the articulated body's muscles
    //  useAlternativeLinkIndexSelection - used for vehicles, joint child link index does not match required child component index
	// NOTES:	This can be used to make an articulated body attempt to follow an animation while reacting to collisions.
	//  Many factors will determine how accurate an active pose will be:
	//	1) Collisions w/ environment and other external forces like gravity.
	//	2) Joint stiffness and damping parameters.  These can be controlled on a joint level or with SetStiffnessWithNMValue.
	//  If erratic behavior occurs, try reducing muscle angle strength on the offending joints.
    // useAlternativeLinkIndexSelection was added to deal with vehicles in GTA, the original code was finding incorrect link indices. 
    // This doesn't assume that all components are linked
	void SetMusclesFromSkeleton (const crSkeleton& skeleton, bool useAlternativeLinkIndexSelection = false);

	virtual void SetCurrentPhysicsLOD(ePhysicsLOD set);
	inline ePhysicsLOD GetCurrentPhysicsLOD() const { return m_CurrentLOD; } 

	void RemoveArticulatedBodyFromCacheEntry();

	// Updates a deferred skeleton update simple form used for multithreaded physics impl
	virtual void SyncSimpleSkeletonUpdate();

	// Updates a deferred skeleton update articulated form used for multithreaded physics impl
	virtual void SyncSkeletonToArticulatedBody(bool updateLocals = false);
#if __SPU
	void SyncSkeletonToArticulatedBodySpu(crSkeleton* skeleton);
#endif // __SPU

	// Updates a deferred skeleton update Natural Motion form used for multithreaded physics impl
	virtual void SyncSkeletonNMToArticulatedBody(){/*no-op*/}

	// Debugging function
	void PrintBoneMatrices() const;

	//this generates a unique key that can be hashed for fragment caching...
	void MakeKey(fragCacheKey* cacheKey);

	// Hide drawables and remove the group from the physics level, disabling impacts as necessary.
	// returns false if this inst is now dead and removed from the physics level
	bool HideDeadGroup(int groupIndex, phContactIterator *impacts);

	// PURPOSE: Called when this instance's fragType gets paged in.
	// NOTES:
	//   This never gets called unless we've previously been Insert()ed, so the instance will also
	//   be added to the physics level at this time.
	// 
	//   If overridden by a derived class, this class's version should be called, or the instances
	//   will never be Insert'ed, etc.
	virtual void ReportTypePagedIn();

	// PURPOSE: This function gets called after the object has been paged in.
	virtual void	PagedIn() { ReportTypePagedIn(); }

	// PURPOSE: Called when this instance's fragType pages out.
	// NOTES:
	//   If overridden by a derived class, this class's version should be called, or the instances
	//   will never be Insert'ed, etc.
	virtual void ReportTypePagedOut();

	// PURPOSE: This function gets called after the object has been paged out.
	virtual void	PagedOut() { ReportTypePagedOut(); }

	// *** The next section has functions that are overridden in derived classes for various effects ***

	// This is overridden in derived classes right before an instance loses its cache entry
	virtual void LosingCacheEntry();

	// This is overridden in derived classes right after an instances loses its cache entry
	virtual void LostCacheEntry();

	// This is overridden to return a pointer to the skeleton needed for this instance
	virtual crSkeleton* GetSkeleton() const;

#if HACK_GTA4
	// Added for Gta render thread on spu
	// Our implementation is in SPU compiled game code.
	//
	// Not sure how this should be maintained as far as rage\dev goes?
	crSkeleton* GetSkeletonSpu() const;
#endif

#if USE_VIRTUAL_UPDATE
	// Called once per frame during fragManager::Update, on instances active in the fragment cache
	virtual void Update();
#endif

	// Called whenever a part breaks off, to let the instance react
	typedef atFixedBitSet<phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS> ComponentBits;
	virtual void PartsBrokeOff(const ComponentBits& brokenGroups, phInst* newInst);
	virtual void PartsRestored(const ComponentBits& restoredGroups);

	// Called whenever a part dies, i.e. its health hits 0.
	virtual void PartDied(int groupIndex, const Collision& damageMsg);

	// PURPOSE: Called whenever a part takes damage
	// NOTES: Any derived classes should call back to this class, or normal damage won't occur
	virtual void CollisionDamage(const Collision& msg, phContactIterator *impacts);

	// PURPOSE: Just apply damage (no collision)
	void ApplyDamage(int component, float mag, Vec3V_In position, Vec3V_In relativeVelocity, float &healthPrior, float &healthLeft FRAGMENT_EVENT_PARAM(bool bApplyEvents=true));

	// *** the next section has functions inherited from phInstBreakable, to hook into breaking, etc.

	virtual bool IsBreakable(phInst* otherInst) const;
	virtual bool ShouldFindImpacts(const phInst* otherInst) const;
	virtual void PreComputeImpacts(phContactIterator impacts);
    virtual void NotifyImpulse(const Vector3& impulse, const Vector3& position, int component, int element, float breakScale=1.0f);
	virtual void NotifyForce(const Vector3& force, const Vector3& position, int component, int element, float breakScale=1.0f);

	virtual void ReportMovedBySim();
	virtual int GetClassType() const;
	virtual void NotifyOutOfWorld();
	virtual void NotifyCouldntGetCacheEntry() const;

    virtual phInst* PrepareForActivation(phCollider** colliderToUse, phInst* otherInst, const phConstraintBase * constraint);
	virtual bool PrepareForDeactivation(bool colliderIsManagedBySim, bool forceDeactivate);

	virtual bool FindBreakStrength(const Vector3* componentImpulses,
								   const Vector4* componentPositions,
								   float* breakStrength,
								   phBreakData* breakData) const;

	virtual int BreakApart(phInstBreakable** breakableInstList, 
						   int* numBreakInsts,
						   phInst** brokenInstList, 
						   int componentA, int componentB, phInst*& pInstanceA, phInst*& pInstanceB,
						   const phBreakData& breakData);


	virtual bool IsGroupBreakable(int nGroupIndex)const;

#if HACK_GTA4 // __SPU defines were added for Gta because we're compiling fragment\instance.h on spu for drawable/skeleton update jobs
#if !__SPU
#define COMPILING_BREAK_APART 1
#else 
#define COMPILING_BREAK_APART 0
#endif
#else
#define COMPILING_BREAK_APART 1
#endif

#if COMPILING_BREAK_APART
	virtual phInst* BreakApart(const phBreakData& breakData);
#endif

	// <COMBINE phInst::ApplyForce>
	virtual void ApplyForce (const Vector3& force, const Vector3& position, int component=0, int element=0, float breakScale=1.0f);

#if __DEBUGLOG
	virtual void DebugReplay() const;
#endif

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct& s);
#endif

	f32 GetDamageHealth() const
	{
		return m_DamageHealth;
	}

	//NOTE: This function may have problems with complex (cached) types.
	void SetDamageHealth(f32 fHealth)
	{
		m_DamageHealth = fHealth;
	}

	void SetActivateOnHit(bool activateonhit=true);
	bool ShouldActivateOnHit() const;

	void ZeroSkeletonMatricesByGroup(crSkeleton* skeleton, crSkeleton* damagedSkeleton);

	void PrepareForReuse();

protected:
	// Some notes on the archetype pointer of a fragInst:
	// When a fragInst's fragType is not paged in, its archetype will be NULL (regardless of whether it is in the cache or not).
	// When a fragInst's fragType is paged in and it is not in the cache, the archetype of its fragType will be used.
	// When a fragInst's fragType is paged in and it is in the cache, the archetype of its cache entry will be used.
	void SetInserted(bool active);	

	// fragInst::BreakApart() will call this on the new instance that is created for a broken piece.
	// This can be used to make any additional changes that you would like to make to broken-off pieces.
	// As an example, a vehicle might want to clear the vehicle flag on the archetype of a broken piece that came from a vehicle
	//   since we probably don't want that broken piece to be considered a 'vehicle' itself.
	virtual void PrepareBrokenInst(fragInst *brokenInst) const;

	void BreakEffects(fragCacheEntry* reinsert[2], const fragFindBreakInfo* findBreakInfo);

	// Reset the user editable bitstates to their original type specified settings
	void SetDefaultBitStates();

	enum
	{
		FRAG_INSTANCE_STATE_INSERTED	= 1 << 0, // Set if this instance has been ordered to be inserted
		FRAG_INSTANCE_STATE_BROKEN		= 1 << 1, // Set if this instance isn't attached to the ground
		FRAG_INSTANCE_NO_REVIVE			= 1 << 2, // The user sets this, to prevent revive when cache entries recycle
		FRAG_INSTANCE_MANUAL_UPDATE		= 1 << 3, // Update skeleton matrices, instead of during ReportMovedBySim
		FRAG_INSTANCE_NO_ARTIC_BODY		= 1 << 4, // Don't allocate an articulated body for this instance (internal use)
		FRAG_INSTANCE_STATE_DEAD		= 1 << 5, // This object disappeared because it took too much damage
		FRAG_INSTANCE_BROKE_AND_REMOVED = 1 << 6, // This object broke into nothing and was removed, so it will be re-inserted on reset (internal use)
		FRAG_INSTANCE_DISABLE_ACTIVATION= 1 << 7, // Cannot be activated (copied from type data, modified by user)
		FRAG_INSTANCE_DISABLE_ACTIVATION_OVERRIDE= 1 << 8, // The user has overridden FRAG_INSTANCE_DISABLE_ACTIVATION
		FRAG_INSTANCE_DISABLE_BREAKING	= 1 << 9, // Cannot be broken (copied from type data, modified by user)
		FRAG_INSTANCE_DISABLE_BREAKING_OVERRIDE	= 1 << 10, // The user has overridden FRAG_INSTANCE_DISABLE_BREAKING_OVERRIDE
		FRAG_INSTANCE_ACTIVATE_ON_HIT	= 1 << 11, // Activate on hit
		FRAG_INSTANCE_DISABLE_DAMAGE	= 1 << 12, // Cannot be damaged (copied from type data, modified by user)
		FRAG_INSTANCE_DISABLE_DAMAGE_OVERRIDE= 1 << 13, // The user has overridden FRAG_INSTANCE_DISABLE_DAMAGE_OVERRIDE
		FRAG_INSTANCE_DISABLE_PRERENDER_BOUNDS_UPDATE = 1 << 14, // Disable the bounds syncing to skeleton update from the composer at prerender
		FRAG_INSTANCE_DISABLE_PRERENDER_BOUNDS_COPY_LAST_MATRICES = 1 << 15, // Disable copying last matrices when bounds syncing at prerender
#if ENABLE_FRAG_OPTIMIZATION
		FRAG_INSTANCE_DISABLE_CACHE_ON_INSERTION= 1 << 16 // The user has overridden FRAG_INSTANCE_DISABLE_CACHE_ON_INSERTION
#endif
	};
	
	//some flags, telling whether we're inserted, broken, etc.
	unsigned int				m_BitStates;

	//if this isn't NULL, we're hooked up to this cache entry
	datRef<fragCacheEntry>		m_CacheEntry;
	
	//simple fragments use this - complex fragments keep their healths in their fragCacheEntry (in childInsts)
	f32							m_DamageHealth; 

	//this is the type of the instance
	pgRef<const fragType>		m_Type;

	//this gets set(typically) based on an update from the physics system...
	mutable spdSphere			m_InstanceXFormSphere;

	//this is the original position component of the instance matrix - we use this for resetting...
	Vector3						m_OrgPos;

	phLevelBase::eObjectState m_InsertionState;

	// PURPOSE: a globally unique ID for this fragInst
	// NOTES:
	//   If the type for this instance pages out and back in again, and the cache entry that matches this
	//   GUID has not been reused, then that cache entry will simply be reactivated. That means that the
	//   lamppost, etc. will still be knocked over when the player finds it again.
	u32							m_Guid;

	//this is used when restoring the original orientation to an object - the original matrix also gets stuffed from this(at regen time)...
	u32							m_PackedEulers;

	void*						m_Pad;

	// The current LOD of the articulated body and composite bounds
	ePhysicsLOD					m_CurrentLOD;

	datPadding<12>				m_Pad2;

	void SetBit(bool bit, u32 mask);
};

inline void fragInst::SetBit(bool bit, u32 mask)
{
	if (bit)
	{
		m_BitStates |= mask;
	}
	else
	{
		m_BitStates &= ~mask;
	}
}

inline void fragInst::SetCacheEntry(fragCacheEntry* entry)
{
	m_CacheEntry = entry;
}

inline fragCacheEntry* fragInst::GetCacheEntry() const
{
	return m_CacheEntry; 
}

inline bool fragInst::GetCached() const
{
	return m_CacheEntry != NULL;
}

inline bool fragInst::GetInserted() const
{
	return (m_BitStates & FRAG_INSTANCE_STATE_INSERTED) != 0;
}

inline void fragInst::SetInserted(bool active)
{
	SetBit(active, FRAG_INSTANCE_STATE_INSERTED);
}

inline bool fragInst::ShouldActivateOnHit() const
{
	return (m_BitStates & FRAG_INSTANCE_ACTIVATE_ON_HIT) != 0;
}

inline void fragInst::SetActivateOnHit(bool active)
{
	SetBit(active, FRAG_INSTANCE_ACTIVATE_ON_HIT);
}

inline bool fragInst::GetBroken() const
{
	return (m_BitStates & FRAG_INSTANCE_STATE_BROKEN) != 0; 
}

inline void fragInst::SetBroken(bool broken)
{
	SetBit(broken, FRAG_INSTANCE_STATE_BROKEN);
}

inline bool fragInst::GetNoRevive() const
{
    return (m_BitStates & FRAG_INSTANCE_NO_REVIVE) != 0; 
}

inline void fragInst::SetNoRevive(bool noRevive)
{
    SetBit(noRevive, FRAG_INSTANCE_NO_REVIVE);
}

inline bool fragInst::GetManualSkeletonUpdate() const
{
    return (m_BitStates & FRAG_INSTANCE_MANUAL_UPDATE) != 0; 
}

inline void fragInst::SetManualSkeletonUpdate(bool manualSkeletonUpdate)
{
    SetBit(manualSkeletonUpdate, FRAG_INSTANCE_MANUAL_UPDATE);
}

inline bool fragInst::GetDead() const
{
    return (m_BitStates & FRAG_INSTANCE_STATE_DEAD) != 0; 
}

inline void fragInst::SetDead(bool manualSkeletonUpdate)
{
    SetBit(manualSkeletonUpdate, FRAG_INSTANCE_STATE_DEAD);
}

#if ENABLE_FRAG_OPTIMIZATION
inline bool fragInst::IsCacheOnInsertionDisabled() const
{
	return (m_BitStates & FRAG_INSTANCE_DISABLE_CACHE_ON_INSERTION) != 0;
}

inline void fragInst::SetDisableCacheOnInsertion(bool disabled)
{
	SetBit(disabled, FRAG_INSTANCE_DISABLE_CACHE_ON_INSERTION);
}
#endif

inline void fragInst::SetInsertionState(phLevelBase::eObjectState insertionState)
{
	m_InsertionState = insertionState;
}

inline phLevelBase::eObjectState fragInst::GetInsertionState() const
{
	return m_InsertionState;
}

inline const spdSphere& fragInst::GetXFormSphere() const	
{
	return m_InstanceXFormSphere;
}

inline u32 fragInst::GetGuid() const
{
	return m_Guid; 
}

inline void fragInst::SetBoundSphere(const Vector4& boundSphere)
{
	m_InstanceXFormSphere.SetV4(RCC_VEC4V(boundSphere));
}

inline const Vector3& fragInst::GetOrgPos() const
{
	return m_OrgPos; 
}

__forceinline const fragType* fragInst::GetType() const
{
	return m_Type;
}

inline u32 fragInst::GetPackedEulers() const
{
	return m_PackedEulers; 
}

inline int fragInst::GetClassType() const
{
	return PH_FRAG_INST;
}

inline void fragInst::PrepareBrokenInst(fragInst *UNUSED_PARAM(brokenInst)) const
{
}

inline bool fragInst::IsBoundsPrerenderUpdateEnabled() const
{
	return ENABLE_PRERENDER_BOUNDS_UPDATE && (m_BitStates & FRAG_INSTANCE_DISABLE_PRERENDER_BOUNDS_UPDATE) == 0; 
}

inline void fragInst::SetBoundsPrerenderUpdate(bool bEnable)
{
	SetBit(!bEnable, FRAG_INSTANCE_DISABLE_PRERENDER_BOUNDS_UPDATE);
}

inline bool fragInst::IsBoundsPrerenderCopyLastMatricesEnabled() const
{
	return ENABLE_PRERENDER_BOUNDS_UPDATE && (m_BitStates & FRAG_INSTANCE_DISABLE_PRERENDER_BOUNDS_COPY_LAST_MATRICES) == 0; 
}

inline void fragInst::SetBoundsPrerenderCopyLastMatrices(bool bEnable)
{
	SetBit(!bEnable, FRAG_INSTANCE_DISABLE_PRERENDER_BOUNDS_COPY_LAST_MATRICES);
}

} // namespace rage

#endif // FRAG_INSTANCE_H
