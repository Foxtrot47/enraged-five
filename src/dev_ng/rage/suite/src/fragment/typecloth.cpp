// 
// fragment/typecloth.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cloth_config.h"
#include "typecloth.h"

namespace rage
{

fragTypeEnvCloth::fragTypeEnvCloth(class datResource& rsc) 
: m_Cloth(rsc)
{
}

IMPLEMENT_PLACE( fragTypeEnvCloth );

#if __DECLARESTRUCT
void fragTypeEnvCloth::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(fragTypeEnvCloth);
	STRUCT_FIELD(m_Cloth);
	STRUCT_END();
}
#endif

fragTypeCharCloth::fragTypeCharCloth(class datResource& rsc) 
: m_Cloth(rsc)
{
}

IMPLEMENT_PLACE( fragTypeCharCloth );

#if __DECLARESTRUCT
void fragTypeCharCloth::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(fragTypeCharCloth);
	STRUCT_FIELD(m_Cloth);
	STRUCT_END();
}
#endif

}
