// 
// fragment/animation.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "animation.h"

#include "atl/array.h"

#if __BANK
	#include "bank/bank.h"
#endif


#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "file/token.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "data/resourcehelpers.h"

//////////////////////////////////////////

namespace rage {

fragAnimation::fragAnimation()
	: BoneIndices(NULL)
	, BoneCount(0)
	, PhaseLastFrame(0.0f)
	, AutoPlay(false)
	, AffectsOnlyRootNodes(false)
	, Pad0(0)
	, Pad1(0)
{
}

fragAnimation::~fragAnimation()
{
	delete Animation;
	delete BoneIndices;
}

void fragAnimation::LoadSetup( fiAsciiTokenizer& tok)
{
	char buffer[256];
	tok.GetToken( buffer, sizeof( buffer ) );
	Name = buffer;
	BoneCount = tok.GetInt();
	BoneIndices = rage_new int[ BoneCount ];
	tok.GetDelimiter( "rootbones" );
	for( int a = 0; a < BoneCount; a++ )
		BoneIndices[ a ] = tok.GetInt();
	AutoPlay = tok.CheckToken( "autoplay" );
	tok.CheckToken( "hide" ); // ignored by game right now...used by maya UI
	AffectsOnlyRootNodes = tok.CheckToken( "rootsonly" );
}

fragAnimation::fragAnimation(datResource &rsc)
	: Animation(rsc)
	, Name(rsc)
{
	rsc.PointerFixup(BoneIndices);
}

IMPLEMENT_PLACE(fragAnimation);

#if __DECLARESTRUCT
void fragAnimation::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(fragAnimation);

	STRUCT_FIELD(Animation); // This should't be VP, but the animation can't be swapped yet...
	STRUCT_DYNAMIC_ARRAY(BoneIndices,BoneCount);
	STRUCT_FIELD(BoneCount);
	STRUCT_FIELD(Name);
	STRUCT_FIELD(PhaseLastFrame);
	STRUCT_FIELD(AutoPlay);
	STRUCT_FIELD(AffectsOnlyRootNodes);
	STRUCT_FIELD(Pad0);
	STRUCT_FIELD(Pad1);

	STRUCT_END();
}
#endif // __DECLARESTRUCT

} // namespace rage
