// 
// fragment/manager.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_MANAGER_H
#define FRAGMENT_MANAGER_H

// fragment
#include "instance.h"
#include "fragment/fragshaft.h"

// RAGE
#include "atl/functor.h"
#include "atl/pool.h"
#include "atl/map.h"
#include "atl/delegate.h"
#include "data/base.h"
#include "phbound/boundsphere.h"
#include "physics/archetype.h"
#include "string/stringhash.h"

namespace rage {

class bkBank;
class bgDrawable;
class characterCloth;
class ConstString;
class environmentCloth;
class fiAsciiTokenizer;
class fragCacheManager;
class fragCacheAllocator;
class fragInst;
class grmMatrixSet;
class grmShader;
class phGlassInst;
class phImpact;
class phInst;
class phCollider;
class phInstBreakable;
class rmcDrawable;
template <class TKey> class SimpleHash;

BANK_ONLY(extern bool s_bFragCacheHeapDebug);

#define		USE_PRE_ALLOC_MEMORY			(!( RSG_PC ) && !__RESOURCECOMPILER)

/*
  PURPOSE
	The manager of the fragment types, this class handles the creation of resource data for each
	fragment type, and manages the loading of that data into memory. It maintains a hash table of
	types by name, for quick retrieval in game. It also handles the management of the fragContainer's,
	for convenient maintenance of the arrays of instances that flow into the streaming world.
	
<FLAG Component>
*/
class fragManager : public datBase
{
public:
	friend class fragTuneStruct;

	// PURPOSE
	//   The purpose of these functors, which are passed to the fragManager constructor,
	//   is to communicate with the rendering system.
	typedef Functor1 <phInst*> UserDataInit;
	typedef Functor1 <fragInst*> InsertFragInstFunctor;
	typedef Functor1 <fragInst*> RemoveFragInstFunctor;
	typedef Functor1 <fragInst*> InitFragInstFunctor;
	typedef Functor1 <fragType&> TypePagedInFunctor;
	typedef Functor1 <fragType&> TypePagingOutFunctor;

	
	typedef Functor5Ret <bool,
						 const rmcDrawable&,
						 const Matrix34&,
						 const crSkeleton*,
						 grmMatrixSet*,
						 float> DrawCallbackFunctor;

	typedef atDelegate<bool (const bgDrawable&, const float *transforms, int numTransforms)> BreakableGlassDrawCallbackFunctor;
	typedef atDelegate<int ()> ComputeGlassCrackTypeCallbackFunctor;
	typedef atDelegate<rmcDrawable* (const fragType*)> GetRenderedDrawableForFragTypeFunctor;

	// PURPOSE: a collection of functions for game code to control the behavior and appearance of cloth
	class ClothInterface
	{
	public:
		typedef atDelegate<environmentCloth* (Mat34V_In , const environmentCloth* , bool, bool )>	EnvClothInstanceFunctor;
		typedef atDelegate<void (environmentCloth* , const fragType* )> EnvClothRemoveFunctor;

		EnvClothInstanceFunctor		m_InstanceEnvironmentCloth;
		EnvClothRemoveFunctor		m_RemoveEnvironmentCloth;

		ClothInterface()
		{
			m_InstanceEnvironmentCloth.Reset();
			m_RemoveEnvironmentCloth.Reset();
		}

		ClothInterface(const ClothInterface& clthInterface) 
			: 	m_InstanceEnvironmentCloth(clthInterface.m_InstanceEnvironmentCloth)
			,	m_RemoveEnvironmentCloth(clthInterface.m_RemoveEnvironmentCloth)
		{
		}

		const ClothInterface& operator= (const fragManager::ClothInterface& clthInterface)
		{
			m_InstanceEnvironmentCloth	= clthInterface.m_InstanceEnvironmentCloth;
			m_RemoveEnvironmentCloth	= clthInterface.m_RemoveEnvironmentCloth;
			return clthInterface;
		}
	};

	typedef Functor1Ret <int, const char*> ConvertEntityClassFunctor;

	fragManager(UserDataInit callback = UserDataInit::NullFunctor(),
				InsertFragInstFunctor insertFragInst = InsertFragInstFunctor::NullFunctor(),
				RemoveFragInstFunctor removeFragInst = RemoveFragInstFunctor::NullFunctor(),
				InitFragInstFunctor initFragInst = RemoveFragInstFunctor::NullFunctor(),
				DrawCallbackFunctor drawCallback = MakeFunctorRet(ReturnFalse),
				const ClothInterface* clothInterface = NULL,
				ConvertEntityClassFunctor convertEntityClass = MakeFunctorRet(ReturnEntityClassZero),
				u32 numCacheEntries = 64,
				u16 numTypeStubs = 128,
				TypePagedInFunctor pagedInCallback = TypePagedInFunctor::NullFunctor(),
				TypePagingOutFunctor pagingOutCallback = TypePagingOutFunctor::NullFunctor(),
                bool bSkipCacheMgrInit = false
				);
	virtual ~fragManager();

	void InitCacheMgr(fragInst** cacheBackingInsts);

	void Reset(); //called when the game resets - all assets are about to be trashed...
	
	void SetGlobalNoRevive(bool globalNoRevive = true);
	bool GetGlobalNoRevive() const;

	void SetInterestFrustum(const grcViewport& viewport, const Matrix34& interestMatrix);
	const Matrix34& GetInterestMatrix() const;
	bool GetInterestShaft_IsVisibleSphere(const spdSphere& sphere) const;
	int GetViewportWidth() const;
	float GetViewportTanHFOV() const;

	void SetUseRecyclingSphere(bool b);
 	bool GetUseRecyclingSphere() const;
 	void SetRecyclingSphere(const spdSphere& sphere);
 	const spdSphere& GetRecyclingSphere() const;

	// Functions for controlling the archetype flags of "loose" cache entries
	void SetCacheEntryTypeFlags(u32 flags);
	u32 GetCacheEntryTypeFlags() const;
	void SetCacheEntryIncludeFlags(u32 flags);
	u32 GetCacheEntryIncludeFlags() const;

	void SetDefaultTypeFlags(u32 flags);
	u32 GetDefaultTypeFlags() const;
	const atArray<u32>& GetSpecialTypeFlags() const;
	void ResetSpecialTypeFlags();
	const atArray<u32>& GetSpecialIncludeFlags() const;
	void ResetSpecialIncludeFlags();
	void SetDefaultIncludeFlags(u32 flags);
	u32 GetDefaultIncludeFlags() const;

	typedef atDelegate<u32 (const char*)> InterpretSpecialFlagFunc;
	typedef atDelegate<u32 (const char*)> InterpretSpecialFlagForIncludesFunc;

	void SetInterpretSpecialFlagFunc (InterpretSpecialFlagFunc func);
	void SetInterpretSpecialFlagForIncludesFunc (InterpretSpecialFlagForIncludesFunc func);
	void AddNewStorageForSpecialFlags();
	void InterpretSpecialFlag(const char* specialFlagText);

	typedef Functor3 <fragInst*,
					  fragInst::ComponentBits&,
					  fragInst*> PartsBrokeOffFunc;

	void SetPartsBrokeOffFunc(PartsBrokeOffFunc func);
	PartsBrokeOffFunc GetPartsBrokeOffFunc();
	static void DefaultPartsBrokeOffFunc(fragInst* oldInst,
										 fragInst::ComponentBits& brokenGroups,
									 	 fragInst* newInst);

	bool BreakingPreventedBecauseOfFramerate() const;

	int ComputeGlassCrackTypeForGTA5(const fragInst::Collision& collision);

	void SetBreakableGlassDrawCallback(BreakableGlassDrawCallbackFunctor cb);
	void SetComputeGlassCrackTypeCallback(ComputeGlassCrackTypeCallbackFunctor cd);
	ComputeGlassCrackTypeCallbackFunctor GetComputeGlassCrackTypeCallback();

	void SetGetRenderedDrawableForFragTypeCallback(GetRenderedDrawableForFragTypeFunctor cb);
	rmcDrawable* GetRenderedDrawableForFragType(const fragType* type);

	//will return NULL until InitAndSetupArchetypes has been called...
	static void CreateAllocator();
	static fragManager* GetFragManager();
	static fragCacheManager* GetFragCacheManager();
	static fragCacheAllocator* GetFragCacheAllocator();

	void InitializeUserData(phInst* instance);
	
	void InsertFragInst(fragInst* instance);
	void RemoveFragInst(fragInst* instance);
	void InitFragInst(fragInst* instance);
	void DrawCallback(const rmcDrawable& toDraw,
					  const Matrix34& matrix,
					  const crSkeleton* skeleton,
					  grmMatrixSet* sharedMs,
					  float dist);

	environmentCloth* InstanceEnvironmentCloth(Mat34V_In phinstMat, const environmentCloth* pEnvClothType, bool activateOnHit);
	void RemoveEnvironmentCloth(environmentCloth* pEnvCloth, const fragType* pFragType);

	int ConvertEntityClass(const char *name);

	static void* ReturnZero(void*);
	static int ReturnEntityClassZero(const char*);
	static bool ReturnFalse(const rmcDrawable&, const Matrix34&, const crSkeleton*, grmMatrixSet*, float);

	void NotifyPagedIn(fragType &type)		{ m_TypePagedIn(type); }
	void NotifyPagingOut(fragType &type)	{ m_TypePagingOut(type); }
	
	// PURPOSE: Perform some updating on the fragment system as a whole
	// PARAMS:
	//	fpsActual - The actual current frames per second (not warped time)
	// NOTES:
	//  1. The only purpose of the timeStep parameter is to disable breaking in the  
	//     next frame if the FPS drops below the number set in the global tuning value
	//     BreakingFrameRateLimit.
	//  2. This function also Touches all the underlying fragment type assets,
	//     causing them to page in, if you are using the optional paging system
	//  3. Update also calls Update on the fragment cache manager, which allows
	//     cached instances to receive Update callbacks.
	void Update(float timeStep, int simUpdates);
	void SimUpdate(float timeStep);

	//Switch between long and short file names for the mapping
	void SetUseBaseNamesForID(bool bUseBaseNames);

#if __BANK
	void AddWidgets(bkBank &bank);
#endif // __BANK

	PF_DRAW_ONLY(void ProfileDraw();)

#if __ASSERT
	static int sm_PhysicsInstancesAdded;
#endif

	//Given a file name as input create a name usable to identify the typestub
	const char* MakeTypeMapName(const char *pFileName, char *pBuffer, int iSizeofBuffer);

protected:

	fragShaft							m_InterestShaft;
	Mat34V								m_InterestMatrix;
	//Vec4V								m_InterestPlanes[6]; // TODO -- replace m_InterestShaft with these planes, or spdFrustum
	int									m_ViewportWidth;
	float								m_ViewportTanHFOV;

	spdSphere							m_RecyclingSphere;

	fragCacheManager*					m_CacheMgr;

	u32									m_CacheEntryTypeFlags;
	u32									m_CacheEntryIncludeFlags;

	u32									m_DefaultTypeFlags;
	atArray< u32 >						m_SpecialTypeFlags;
	u32									m_DefaultIncludeFlags;
	atArray< u32 >						m_SpecialIncludeFlags;

	bool								m_UseRecyclingSphere;
	bool								m_PreventBreakingBecauseOfFramerate;
	bool								m_GlobalNoRevive;
	bool								m_UseBaseNames;
	PartsBrokeOffFunc					m_PartsBrokeOffFunc;

	UserDataInit						m_UserDataCallback; // Callback for application to set up user data

	InterpretSpecialFlagFunc			m_InterpretSpecialFlagFunc;
	InterpretSpecialFlagForIncludesFunc m_InterpretSpecialFlagForIncludesFunc;

	InsertFragInstFunctor				m_InsertFragInst;
	RemoveFragInstFunctor				m_RemoveFragInst;
	InitFragInstFunctor					m_InitFragInst;
	DrawCallbackFunctor					m_DrawCallback;
	ClothInterface						m_ClothInterface;
	ComputeGlassCrackTypeCallbackFunctor m_ComputeGlassCrackTypeCallback;
	GetRenderedDrawableForFragTypeFunctor m_GetRenderedDrawableForFragType;

	ConvertEntityClassFunctor			m_ConvertEntityClass;
	TypePagedInFunctor					m_TypePagedIn;
	TypePagingOutFunctor				m_TypePagingOut;
	
	static fragManager*					sm_Instance;
	static fragCacheAllocator*			sm_CacheAllocator;
};

#define FRAGMGR fragManager::GetFragManager()
#define FRAGCACHEMGR fragManager::GetFragCacheManager()
#define FRAGCACHEALLOCATOR fragManager::GetFragCacheAllocator()

inline void fragManager::SetGlobalNoRevive(bool globalNoRevive)
{
	m_GlobalNoRevive = globalNoRevive;
}

inline bool fragManager::GetGlobalNoRevive() const
{
	return m_GlobalNoRevive;
}

inline void fragManager::SetUseBaseNamesForID(bool bUseBaseNames)
{
	m_UseBaseNames = bUseBaseNames;
}

inline const Matrix34& fragManager::GetInterestMatrix() const
{
	return RCC_MATRIX34(m_InterestMatrix);
}

inline bool fragManager::GetInterestShaft_IsVisibleSphere(const spdSphere& sphere) const
{
	return m_InterestShaft.IsVisible(sphere);

	// TODO
	/*
	const Vec3V   sphereCenter = sphere.GetCenter();
	const ScalarV sphereRadius = sphere.GetRadius();

	for (int i = 0; i < 6; i++)
	{
		const Vec4V plane = m_InterestPlanes[i];

		if (IsGreaterThanAll(Dot(sphereCenter, plane.GetXYZ()) - plane.GetW(), sphereRadius))
		{
			return false;
		}
	}

	return true;
	*/
}

inline int fragManager::GetViewportWidth() const
{
	return m_ViewportWidth;
}

inline float fragManager::GetViewportTanHFOV() const
{
	return m_ViewportTanHFOV;
}

inline void fragManager::SetCacheEntryTypeFlags(u32 flags)
{
	m_CacheEntryTypeFlags = flags;
}

inline u32 fragManager::GetCacheEntryTypeFlags() const
{
	return m_CacheEntryTypeFlags;
}

inline void fragManager::SetCacheEntryIncludeFlags(u32 flags)
{
	m_CacheEntryIncludeFlags = flags;
}

inline u32 fragManager::GetCacheEntryIncludeFlags() const
{
	return m_CacheEntryIncludeFlags;
}

inline void fragManager::SetDefaultTypeFlags(u32 flags)
{
	m_DefaultTypeFlags = flags;
}

inline u32 fragManager::GetDefaultTypeFlags() const
{
	return m_DefaultTypeFlags;
}

inline const atArray<u32>& fragManager::GetSpecialTypeFlags() const
{
	return m_SpecialTypeFlags;
}

inline const atArray<u32>& fragManager::GetSpecialIncludeFlags() const
{
	return m_SpecialIncludeFlags;
}

inline void fragManager::ResetSpecialTypeFlags()
{
	m_SpecialTypeFlags.clear();
}

inline void fragManager::ResetSpecialIncludeFlags()
{
	m_SpecialIncludeFlags.clear();
}

inline void fragManager::SetDefaultIncludeFlags(u32 flags)
{
	m_DefaultIncludeFlags = flags;
}

inline u32 fragManager::GetDefaultIncludeFlags() const
{
	return m_DefaultIncludeFlags;
}

inline fragManager::PartsBrokeOffFunc fragManager::GetPartsBrokeOffFunc()
{
	return m_PartsBrokeOffFunc;
}

inline bool fragManager::BreakingPreventedBecauseOfFramerate() const
{
	return m_PreventBreakingBecauseOfFramerate;
}

inline void fragManager::SetComputeGlassCrackTypeCallback(ComputeGlassCrackTypeCallbackFunctor cb)
{
	m_ComputeGlassCrackTypeCallback = cb;
}

inline void fragManager::SetGetRenderedDrawableForFragTypeCallback(GetRenderedDrawableForFragTypeFunctor cb)
{
	m_GetRenderedDrawableForFragType = cb;
}

inline fragManager::ComputeGlassCrackTypeCallbackFunctor fragManager::GetComputeGlassCrackTypeCallback()
{
	return m_ComputeGlassCrackTypeCallback;
}

inline fragManager* fragManager::GetFragManager()
{
	return sm_Instance; 
}

inline fragCacheManager* fragManager::GetFragCacheManager()
{
	return GetFragManager()->m_CacheMgr; 
}

inline fragCacheAllocator* fragManager::GetFragCacheAllocator()
{
	return GetFragManager()->sm_CacheAllocator; 
}

inline void fragManager::InsertFragInst(fragInst* instance)
{
	m_InsertFragInst(instance);
}

inline void fragManager::RemoveFragInst(fragInst* instance)
{
	m_RemoveFragInst(instance);
}

inline void fragManager::InitFragInst(fragInst* instance)
{
	return m_InitFragInst(instance);
}

inline void fragManager::DrawCallback(const rmcDrawable& toDraw,
									  const Matrix34& matrix,
									  const crSkeleton *skeleton,
									  grmMatrixSet* sharedMs,
									  float dist)
{
	m_DrawCallback(toDraw, matrix, skeleton, sharedMs, dist);
}


inline environmentCloth* fragManager::InstanceEnvironmentCloth(Mat34V_In phinstMat, const environmentCloth* pEnvClothType, bool activateOnHit)
{
	if(m_ClothInterface.m_InstanceEnvironmentCloth.IsBound())
		return m_ClothInterface.m_InstanceEnvironmentCloth(phinstMat, pEnvClothType, activateOnHit, false);
	return NULL;
}

inline void fragManager::RemoveEnvironmentCloth(environmentCloth* pEnvClothInst, const fragType* pFragType)
{
	if(m_ClothInterface.m_RemoveEnvironmentCloth.IsBound())
		m_ClothInterface.m_RemoveEnvironmentCloth(pEnvClothInst, pFragType);
}


inline int fragManager::ConvertEntityClass(const char *name)
{
	return m_ConvertEntityClass(name);
}

inline void* fragManager::ReturnZero(void*)
{
	return 0;
}

inline int fragManager::ReturnEntityClassZero(const char *)
{
	return 0;
}

inline bool fragManager::ReturnFalse(const rmcDrawable&, const Matrix34&, const crSkeleton *, grmMatrixSet *, float)
{
	return false;
}

inline void fragManager::SetUseRecyclingSphere(bool b)
{
	m_UseRecyclingSphere = b;
}

inline bool fragManager::GetUseRecyclingSphere() const
{
	return m_UseRecyclingSphere;
}

inline void fragManager::SetRecyclingSphere(const spdSphere& sphere)
{
	m_RecyclingSphere = sphere;
}

inline const spdSphere& fragManager::GetRecyclingSphere() const
{
	return m_RecyclingSphere;
}

} // namespace rage

#endif // FRAG_MANAGER_H
