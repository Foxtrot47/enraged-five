@@Overview

* Origin Of Module Name *
The module name, "fragment" refers to the fragmentable objects it handles.

* Overview *
The fragment module deals with objects that break apart. The core purpose of the
module is to manage the broken parts after a fragmentable instance breaks. After a
fragmentable object breaks into two pieces, both of those pieces need to be kept track
of, so that they can be drawn at their correct location, and with the correct parts 
broken off.

Peripheral to this purpose is the specification of the structure of the fragmentable
objects, which is kept in the fragType. It has a structure of fragTypeChild's and
fragTypeGroup's that mimic the structure of the fragmentable object in the game. It
holds other information such as the breaking strength of the structure.

Several other features are available for fragment objects. Each breakable piece also
has an array of damage regions, each of which has a health value that can be passed to
rendering for mesh deformation or texture modification. When a health value reaches
zero, that piece can be swapped for a damaged version, which can also take more damage
for further mesh deformation and texture adjustment.

Fragmenting objects can be animated with a skeleton, which optionally can control child
fragment parts. If an animation doesn't control child fragment types, it has no physical
effect. If it does control a fragment child, the animation will move the bound parts
associated with that child and its children. That child can then break off in response
to sufficient force.

Fragment parts can also be animated by physics. The skeleton can be set up with joint
limits on a part, which will then rotate in response to forces. The part can be held
still by a latch (think of a car door), which can be broken to trigger the physical
simulation. The entire part and all of its children can, of course, still break off.

All of these features are tunable in-game. An intuitive interface allows the game
designer to modify the breaking strengths, health values, and other properties of
fragment types. As they use the tuning system, the part they are working on is
highlighted in the game.

Yet another optional feature is streaming. If you call FRAGMGR.GetType(), you get a fragTypeStub,
which can be used to create a fragInst which will activate itself when the type data actually
pages in. If you don't want to use the built in paging, you can call fragType::Load, which
will load the type data directly from disk. When you Insert a fragInst directly from a
fragType, it starts functioning immediately.

@@Movable_World
<title Movable World>

In order to be drawn, fragment instances communicate with an
external system that manages the renderable world. The
\purpose of this is to allow some form of culling to be done
using some sort of spatial partitioning. The purpose of all
the functors is to allow the fragment system to allocate new
movables in the renderable world when an object breaks into two.

Any world system that implements the necessary interface is
sufficient. There are two examples of such a world at present,
fragWorld in base\samples\sample_physics, and rmworld in
suite\src.

See Also
<link rage::fragManager::AddToDrawBucket@const rmcDrawable&@const Matrix34*@const crSkeleton*@float, fragManager::AddToDrawBucket Method>,
<link rage::fragManager::AddToDrawList@void*, fragManager::AddToDrawList Method>,
<link rage::fragManager::DeleteMovable@void*, fragManager::DeleteMovable Method>,
<link rage::fragManager::GetBoundingSphere@void*, fragManager::GetBoundingSphere Method>,
<link rage::fragManager::InsertMovable@void*, fragManager::InsertMovable Method>,
<link rage::fragManager::NewMovable@fragInst*, fragManager::NewMovable Method>,
<link rage::fragManager::RemoveMovable@void*, fragManager::RemoveMovable Method>,
<link rage::fragManager::UpdateMovable@void*, fragManager::UpdateMovable Method>

@@Cache
<title Cache>

The fragment system needs to be able to obtain variable sized pieces of memory, depending on the type and tuning
data the artists and designers create. A fragment type with twelve breakable parts, seven articulated parts, and 
eighteen events requires a lot more state data than a simple fragment type that just has one breakable part.
In order to obtain that memory, the fragment module uses a two-level system called the cache.

The top level is the cache entry level. Whenever a fragInst needs to go into the cache, a single cache entry is
allocated to that instance. The cache entry is like a shell for the entry data, since most of the data is
down at the lower level in the cache pool blocks.

When cache entries are not available, old cache entries are recycled. The system attempts to find a cache
entry that is offscreen. If that is not available, it will pick an entry that is as small as possible on
screen.

Each cache entry uses one or more cache pool blocks, which are the lower level of the cache system. The
cache pool blocks are obtained from a set of pools that are created by the user when by calling
AddCachePool. Each call specifies the size of a pool entry and the number of entries. As many
pools can be created as desired, which can be of any number of elements of any size. A separate pool
is created by each call to AddCachePool. When a cache pool block is needed, the fragment system requests
one from the pool that contains blocks of the appropriate size.

When tuning cache pool strategies, the most useful tool is to turn on some debug information available, which
is a compile switch. Edit cacheheap.cpp and change the value of FRAG_CACHE_HEAP_DEBUG to 1. A bunch of spew
will result showing the allocations that are being made and the space being wasted.

There is a large variety of different objects that are allocted in the cache pools. Some are pretty obvious,
like crSkeleton's and phBound's, but there are also evtPlayer's, crAnimFrame's, and even classes that are
defined outside of RAGE at the project level. So, rather than try to somehow compute the total amount of
cache space needed for particular fragment type, we just measure it.

The fragType contains a member that tracks the "estimated" size of the cache entries that have been created
from that fragType. When a cache entry is initialized, the first allocation it attempts to make is at least
as big as that estimated size. That way, a type requiring a large cache entry doesn't consume a lot of
smaller blocks, if a larger block was available that was appropriate. If the cache entry does overflow
the estimated size, it will take as small a block as available to satisfy the current request.

However, the very first cache entry created from a freshly generated fragType won't have any estimate of
the size. There are too many different systems that make allocations in cache entries to make a reliable
guess at the size that will be needed, so we don't even try. Instead, the recommendation is that the resource
builder for fragment types should create a fragInst of the type and put it in the cache. Then when the game
loads it will already have a good estimate of the size needed. Later tunings can cause this estimate to be
wrong, but only the first cache entry created after that will be horribly wrong.

If the cache pools run completely dry, main memory will start being allocated. There is a lot of waste when
this happens, because each allocation has headers that take up extra space. Fragmentation of the game heap
can result, because the cache entry will hang on to that memory until the cache entry is recycled. So, it is
a good idea to configure your cache pools carefully and make sure the system has good estimated cache
sizes to work with.

@@Components
Here's a table listing the primary components of this module.

##BEGIN COMMAND-LINE-SECTION is automatically generated - do not edit this section!
@@Command Line Options
<TABLE>
Parameter Name                In File       Description                                                                             
----------------------------  ------------  --------------------------------------------------------------------------------------  
fragcachewarn                 instance.cpp  How many times to warn if fragment cache was full when an entry was needed              
fragdebug                     instance.cpp  Print debugging information to the terminal about fragments                             
fragdebug2                    instance.cpp  Print debugging information to the terminal about fragments                             
FragGlobalMaxDrawingDistance  tune.cpp      force the global max drawing distance to the passed in value                            
fragresdir                    typestub.cpp  Directory that contains all the pre-built fragment resources, i.e. WCK/XECK files.      
nofragarticulation            cache.cpp     Don't allow fragment objects to become articulated                                      
nofragbreak                   instance.cpp  Don't allow fragment objects to break                                                   
props                         manager.cpp   Force all container-based fragment types to be of the same type                         
regenfragdistricts            manager.cpp   Regenerate paging data for fragment types, and all districs that have fragment objects  
regenfrags                    manager.cpp   Regenerate paging data for fragment types                                               
</TABLE>
##END COMMAND-LINE-SECTION

@@Working Examples
##BEGIN SAMPLES-SECTION is automatically generated - do not edit this section!
@@Basic fragments Sample
<GROUP Working Examples>

This sample shows how to load a fragment type, and create
a fragment instance from it.
The sample is broken into several steps:
1. Initialize the physics sample manager and create a demo world
2. Initialize the fragment manager
3. Load a fragment type
4. Instantiate the instance
5. Insert the instance
6. Clean up

* Initialize the physics sample manager and create a demo world *
This is actually several steps rolled into one, but these steps are common to
all RAGE physics, so they're not repeated here. If you need help setting up RAGE
physics, have a squiz at base/samples/sample_physics/sample_basic.sln
<CODE>


physicsSampleManager::InitClient();
m_Demos.AddDemo(new phDemoWorld("basic frag sample"));
m_Demos.GetCurrentWorld()->Init(1000);
m_Demos.GetCurrentWorld()->Activate();
m_Demos.GetCurrentWorld()->ConstructTerrainPlane(false);
m_Demos.SetCurrentDemo(0);

</CODE>


* Initialize the fragment manager *
The fragment manager is responsible for a couple of features we're not actually
using in this sample. For one, it handles paging of type data, and in this sample we
are just going to load the type data directly from disk. The other is that it forwards
draw calls from a drawing system to the fragment instances, and in this sample we're
not going to draw the fragments themselves. (You will only see their physical forms
due to physics debug drawing. Nevertheless, we have to create one because there's a fair
bit of code that expects us to.
<CODE>


new fragManager;
FRAGMGR->AddCachePool(100000, 32);

</CODE>


* Load a fragment type *
All we have to do is call fragType::Load(file) with the location of the entity.type
file we're interested in. If we were using the fragment manager's paging system, we would
have instead called FRAGMGR->GetType(), which would have given us a fragTypeStub instead
of a while fragType. The stub doesn't hold the data, but will inform any instances we create
with it when the data becomes available.
<CODE>


const char* file = "$/fragments/HotDogRed_rex/entity.type";
PARAM_file.Get(file);

m_Type = fragType::Load(file);
Assert(m_Type);

</CODE>


* Instantiate the instance *
We create a fragInst, passing it the type data that we loaded. If we had only a fragTypeStub
at this point, the constructor would look the same. The matrix passed in is the initial position
of the instance, and also where it will return to when Reset.
<CODE>


m_Inst = new fragInst(m_Type, M34_IDENTITY);

</CODE>


* Insert the instance *
Inserting the instance causes it to try to insert into the physics world. If it doesn't have
type data at this point because it was create with a fragTypeStub whose type hasn't paged in
yet, it will wait to perform the insert as soon as the type comes in. In this sample, it is
inserted immediately.

If we were using fragment drawing, Insert would also cause the instance to create a "movable"
in the drawing system through the callbacks registered with the fragment manager. That could be
used, to hook into some PVS mechanism such as an octree or BSP. In this sample, that doesn't
happen because we didn't register any functors with the fragment manager.
<CODE>


m_Inst->Insert(false);
}

virtual void ShutdownClient()
{
</CODE>


* Clean up *
Removing the instance deletes it from the physics world, and would also remove it from the
graphics culling world if we were using one.
<CODE>


m_Inst->Remove();

// We have to reset the fragment manager before we delete the type, because otherwise the fragment
// cache could be holding onto references to our type data.
FRAGMGR->Reset();

// Then we delete the instance and the type, and
// dismiss the other systems in the reverse order we created them.
delete m_Inst;
delete m_Type;

delete FRAGMGR;

physicsSampleManager::ShutdownClient();
}

protected:
virtual const char* GetSampleName() const
{
return "sample_fragment/sample_basic";
}

private:
fragType*					m_Type;
fragInst*					m_Inst;
};

} // namespace ragesamples

// main application
int Main()
{
{
ragesamples::fragmentSampleManager samplePhysics;
samplePhysics.Init();

samplePhysics.UpdateLoop();

samplePhysics.Shutdown();
}

return 0;
}





</CODE>


@@Fragment small cache demonstration Sample
<GROUP Working Examples>

This sample loads a fragment type, and creates a circle of instances of that type. The fragment cache
is deliberately set to a small number of entries to demonstrate the behavior of the system when cache
entries start to run out.
NOTES:
- This is a good sample to try out the "DetachInterest" debug drawing. To use this, choose a camera position,
then click the DetachInterest widget in the Fragment group. The current camera position is frozen at that
position, as far as the fragment manager is aware. The position of the camera is also marked by debug
drawing of its frustum at that time. You can then move the camera away, and observe, for example, that the
system will prefer to recycle cache entries outside of that frustum.


##END SAMPLES-SECTION is automatically generated - do not edit this section!

