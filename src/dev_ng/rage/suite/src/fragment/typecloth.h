// 
// fragment/typecloth.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_CLOTHTYPE_H
#define FRAGMENT_CLOTHTYPE_H

#include "fragment/cloth_config.h"

#include "cloth/clothbridgesimgfx.h"
#include "cloth/environmentcloth.h"
#include "cloth/charactercloth.h"

namespace rage {

class fragTypeEnvCloth
{
public:
	fragTypeEnvCloth(){}
	fragTypeEnvCloth(class datResource& rsc);

	DECLARE_PLACE(fragTypeEnvCloth);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	environmentCloth m_Cloth;
};

class fragTypeCharCloth
{
public:
	fragTypeCharCloth(){}
	fragTypeCharCloth(class datResource& rsc);

	DECLARE_PLACE(fragTypeCharCloth);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	characterCloth m_Cloth;
};


} // namespace rage


#endif // ENVIRONMENT_CLOTH_H
