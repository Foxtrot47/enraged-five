// 
// fragment/locatorhandle.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_LOCATORHANDLE_H
#define FRAGMENT_LOCATORHANDLE_H

#include "drawable.h"

namespace rage {

/*
PURPOSE
	To cheaply and quickly identify a location on a fragment instance. They are
	used to find the seats of vehicles, the location of particle effects, etc.

*/
class fragLocatorHandle
{
public:
	explicit fragLocatorHandle() : m_Ptr(NULL)
	{	}

	explicit fragLocatorHandle(const fragDrawable::Locator& loc) : m_Ptr(&loc)
	{	}

	bool IsValid() const
	{	return m_Ptr != NULL;	}

	void Reset()
	{	m_Ptr = NULL;	}

	void Set(const fragDrawable::Locator* loc)
	{ m_Ptr = loc; }

	const fragDrawable::Locator& Dereference() const
	{	FastAssert(m_Ptr);
		return *m_Ptr;
	}

protected:
	const fragDrawable::Locator* m_Ptr;
};

} // namespace rage

#endif // FRAG_LOCATORHANDLE_H
