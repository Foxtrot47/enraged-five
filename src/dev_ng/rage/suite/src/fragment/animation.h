// 
// fragment/animation.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_ANIMATION_H
#define FRAGMENT_ANIMATION_H

#include "data/base.h"
#include "string/string.h"
#include "cranimation/animation.h"

namespace rage {

/*
  PURPOSE
	Owns a crAnimation, for handling of animated parts.

 NOTES
    Animated parts can optionally control a bound part. If they do, that part can be
	broken off if its force limit is exceeded.

<FLAG Component>
*/
class fragAnimation
{
public:
	friend class fragDrawable; 
	friend class sagVehBonyModel;
	friend class sagVehicle;

	fragAnimation();

	fragAnimation(class datResource&);
	DECLARE_PLACE(fragAnimation);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	~fragAnimation();


	void LoadSetup( class fiAsciiTokenizer& tok );

	datOwner<crAnimation> Animation;   // owned by this object
	int *BoneIndices;
	int BoneCount;
	ConstString Name;
	float PhaseLastFrame;
	bool AutoPlay, AffectsOnlyRootNodes;
	u8 Pad0, Pad1;
};

} // namespace rage

#endif // FRAG_ANIMATION_H
