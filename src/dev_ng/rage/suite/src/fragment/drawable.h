// 
// fragment/drawable.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_DRAWABLE_H
#define FRAGMENT_DRAWABLE_H

#ifndef RMCORE_DRAWABLE_H
#include "rmcore/drawable.h"
#endif

#ifndef ATL_BINTREE_H
#include "atl/bintree.h"
#endif

#ifndef ATL_STRING_H
#include "atl/string.h"
#endif

#include "atl/array_struct.h"
#include "data/resource.h"

namespace rage {

class fragAnimation;
class fragLocatorHandle;
class phBound;

/*
  PURPOSE
	Handles the loading of the drawing and bounds data for each piece of a fragment type.
	
  NOTES
    Each fragTypeChild owns one fragDrawable. The fragDrawable also loads other data,
	such as "locators" which are used to indicate the positions of the characters
	in vehicle seats, the entry and exit positions from every door, the position of particle
	effects, etc.
	
<FLAG Component>
*/
class fragDrawable : public rmcDrawable {
public:
	enum {
		eLoadStructural=1,
		eLoadGraphical=2
	};

	struct Locator
	{
		Vector3 Offset;
		Vector3 Eulers;
		int BoneIndex;
		u32 Pad0, Pad1, Pad2;

		Locator() : Offset(0.0f,0.0f,0.0f), Eulers(0.0f,0.0f,0.0f), BoneIndex(-1), Pad0(0), Pad1(0), Pad2(0) {}
		Locator( datResource& UNUSED_PARAM(rsc) ) {}

		// PURPOSE: Construct this locator's global matrix, transforming it by
		//	the parent bone (if any), or the root bone if there is no parent
		//	bone.
		// PARAMETERS:
		//	outMtx - The output matrix
		//	skel - The posed parent skeleton
		void ComputeGlobalMtx(Matrix34 &outMtx, const crSkeleton &skel) const;

		// PURPOSE: Construct this locator's local matrix, transforming it by
		//	the parent bone (if any).
		// PARAMETERS:
		//	outMtx - The output matrix
		//	skel - The posed parent skeleton
		void ComputeLocalMtx(Matrix34 &outMtx, const crSkeleton &skel) const;

		// PURPOSE: Construct this locator's matrix, transforming it by an
		//	optional parent matrix.
		// PARAMETERS:
		//	outMtx - The output matrix
		//	pParentMtx - The optional parent matrix.
		void ConstructMtx(Matrix34 &outMtx, const Matrix34 *pParentMtx=0) const;

#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct &s)
		{
			STRUCT_BEGIN(fragDrawable::LocatorData);
			STRUCT_FIELD(Offset);
			STRUCT_FIELD(Eulers);
			STRUCT_FIELD(BoneIndex);
			STRUCT_FIELD(Pad0);
			STRUCT_FIELD(Pad1);
			STRUCT_FIELD(Pad2);
			STRUCT_END();
		}
#endif // __DECLARESTRUCT
	};

	fragDrawable();
	~fragDrawable();
	fragDrawable(class datResource&);
	fragDrawable(class datResource&, grmShaderGroup*);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

	DECLARE_PLACE(fragDrawable);

	rmcTypeFileParser *RegisterParserFunctions(int loadConfig, rmcTypeFileParser* parser = 0);
	bool Load(const char *basename = "entity", rmcTypeFileParser *parser = 0, bool configParser = true);
	bool Load(fiTokenizer &T, rmcTypeFileParser *parser = 0, bool configParser = true);

	void LoadAnimations(rmcTypeFileCbData *data);
	void LoadBoneTags(rmcTypeFileCbData *data);
	void LoadBound(rmcTypeFileCbData *data);
	void LoadLocators(rmcTypeFileCbData *data);
	void LoadSkeletonType(rmcTypeFileCbData *data);

	void SetLoadSkeleton(bool loadSkeleton = true)	{ m_LoadSkeleton = loadSkeleton; }

	void LoadSharedSkeletonData(const char* filename, crSkeletonData*& sharedSkelData);

	phBound* GetBound() const						{ return m_Bound; }
	void SetBound(phBound* bound)					{ m_Bound = bound; }
	const Matrix34& GetBoundMatrix() const			{ return m_BoundMatrix; }
	void SetBoundMatrix(const Matrix34& matrix)		{ m_BoundMatrix = matrix; }
	const char* GetSkeletonTypeName() const			{ return m_SkeletonTypeName; }

	int GetNumExtraBounds() const					{ return m_NumExtraBounds; }
	phBound* GetExtraBound(int i) const				{ return m_ExtraBounds[i]; }
	void SetExtraBound(int i, phBound* bound)		{ m_ExtraBounds[i] = bound; }
	atArray<datOwner<phBound> >& GetExtraBounds()	{ return m_ExtraBounds; }
	const Matrix34& GetExtraBoundMatrix(int i) const{ return m_ExtraBoundsMatrices[i]; }

	virtual const Locator* GetLocator( const char* name ) const;

	int GetAnimCount() const						{ return m_Animations.GetCount(); }
	fragAnimation* GetAnimation(int index) const	{ return m_Animations[index]; }

	void GetBoundingBox(Vector3& boxMin, Vector3& boxMax) const { m_LodGroup.GetBoundingBox(boxMin, boxMax); }

	// This is a workaround for the fact that rmcDrawable thinks that
	// it owns the skeleton data, which is not always true. In Agent,
	// for entities sharing skeleton data, the entity type manager clears
	// the skeleton data pointer before invoking the resource constructor,
	// and sets it to point to the shared data afterwards.
	void ClearSkeletonDataPtrBeforePageIn()							{ m_SkeletonData = NULL; }
	void SetSkeletonDataPtrAfterPageIn(crSkeletonData &skelData)	{ m_SkeletonData = &skelData; }

#if __PFDRAW
	// PURPOSE : Performs profile drawing for the drawable.
	//
	// PARAMS : pSkel - Skeleton to use for skinned drawables (can be NULL)
	//			transformMat - Transformation matrix to use for drawables that have no skeleton available
	//			lodLevel - lod level of the drawable to perform the profile drawing for.
	virtual void ProfileDraw(const crSkeleton *pSkel, const Matrix34* transformMat = NULL, int lodLevel = 0) const;
#endif

	bool				CloneShaderGroup();
	grmShaderGroup*		GetClonedShaderGroupPtr()						{ return m_ClonedShaderGroup; }

private:
	class LocatorData
	{
	public:
		typedef atBinTree<ConstString,Locator> LocatorTree;
		typedef LocatorTree::ConstIterator Iterator;
		LocatorData() { }

		~LocatorData() { m_Locators.DeleteAll(); }

		LocatorData(class datResource& rsc);

#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

		void AddLocator( const char *name, const Locator& info )	{ m_Locators.Insert( name, info ); }
		const Locator* GetLocator( const char *name ) const { return m_Locators.Access( name ); }

		int GetLocatorCount() const { return m_Locators.GetNumNodes(); }
		Iterator CreateIterator() const { return m_Locators.CreateIterator(); }

	private:
		LocatorTree m_Locators;
	};

	void AddAnim(const char *animName, int rootBoneCount, int* rootBoneIndices, bool rootBonesOnly);

	Matrix34					m_BoundMatrix;
	datOwner<phBound>			m_Bound;
	atArray<datOwner<phBound> >	m_ExtraBounds;
	Matrix34*					m_ExtraBoundsMatrices;
	u16							m_NumExtraBounds;
	bool						m_LoadSkeleton;
	ATTR_UNUSED u8				m_Pad0;
	LocatorData					m_Locators;
	ConstString					m_SkeletonTypeName; // HACK: m_SkeletonType's aren't resourcified/paged yet, so just store string in the fragDrawable resource and look it up from static table

	// NOTE: These are only used for vehicles, which have very limited numbers of animations
	typedef atArray<datOwner<fragAnimation> > AnimArray;
	AnimArray					m_Animations;
	grmShaderGroup				*m_ClonedShaderGroup;	// cloned shadergroup of original drawable
};

} // namespace rage

#endif // FRAG_DRAWABLE_H

