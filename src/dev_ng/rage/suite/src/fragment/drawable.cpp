// 
// fragment/drawable.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "animation.h"
#include "drawable.h"
#include "manager.h"

#include "atl/bintree_rsc.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "data/resourcehelpers.h"
#include "file/token.h"
#include "phbound/bound.h"
#include "grprofile/drawmanager.h"
#include "rmcore/typefileparser.h"
#include "system/memory.h"

namespace rage {

EXT_PFD_DECLARE_ITEM(Locators);
EXT_PFD_DECLARE_ITEM_COLOR(LocatorColor);

fragDrawable::fragDrawable()
	: m_Bound(NULL)
	, m_ExtraBoundsMatrices(NULL)
	, m_NumExtraBounds(0)
	, m_LoadSkeleton(true)
	, m_ClonedShaderGroup(NULL)
	, m_Pad0(0)
{
	m_BoundMatrix.Identity();
}


fragDrawable::~fragDrawable() 
{
	if (m_Bound && phConfig::IsRefCountingEnabled())
	{
		m_Bound->Release();
	}
	else
	{
		delete m_Bound;
	}

	if (m_NumExtraBounds)
	{
		if (phConfig::IsRefCountingEnabled())
		{
			for (int bound = 0; bound < m_NumExtraBounds; ++bound)
			{
				if (m_ExtraBounds[bound])
				{
					m_ExtraBounds[bound]->Release();
				}
			}
		}
		else
		{
			for (int bound = 0; bound < m_NumExtraBounds; ++bound)
			{
				delete m_ExtraBounds[bound];
			}
		}
	}
	delete [] m_ExtraBoundsMatrices;

	for (int anim = 0; anim < m_Animations.GetCount(); ++anim)
	{
		delete m_Animations[anim];
	}

	if (m_ClonedShaderGroup)
	{
#if !__TOOL
		sysMemContainerData temp;
		temp.m_Base =  m_ClonedShaderGroup;
		temp.m_Size = m_ClonedShaderGroup->GetContainerSize();
		sysMemContainer shaderContainer(temp);
		shaderContainer.BeginShutdown();
#endif
		delete m_ClonedShaderGroup;
		m_ClonedShaderGroup = NULL;
#if !__TOOL
		shaderContainer.EndShutdown();
#endif
	}
}


const fragDrawable::Locator* fragDrawable::GetLocator(const char* name) const
{
	const Locator* locator = m_Locators.GetLocator( name );
	return locator;
}

#if MESH_LIBRARY
void fragDrawable::LoadAnimations(rmcTypeFileCbData *data)
{
	fiTokenizer *T = data->m_T;
	T->MatchToken("{");
	T->MatchToken("Count");
	int animCount = T->GetInt();

	m_Animations.Reserve(animCount);
	
	char animName[64];
	for (int i = 0; i < animCount; ++i)
	{
		T->MatchToken( "animation" );
		T->GetToken(animName, sizeof(animName));
		int rootBoneCount = T->GetInt();
		T->MatchToken( "rootbones" );
		int* rootBoneIndices = (int*)alloca( sizeof( int ) * rootBoneCount );
		for( int a = 0; a < rootBoneCount; a++ )
			rootBoneIndices[ a ] = T->GetInt();
		T->CheckToken( "autoplay" );  // ignore if there...shouldn't be (same as level format)
		T->CheckToken( "hide" );	  // ignore if there...shouldn't be (same as level format)
		bool rootBonesOnly = T->CheckToken( "rootsonly" );
		AddAnim( animName, rootBoneCount, rootBoneIndices, rootBonesOnly );
	}
	T->MatchToken("}");
}

void fragDrawable::AddAnim(const char *animName, int rootBoneCount, int* rootBoneIndices, bool rootBonesOnly)
{
	fragAnimation* anim = rage_new fragAnimation;
	anim->Animation = rage_new crAnimation;
	AssertVerify( anim->Animation->Load( animName )); 
//	anim->Animation->LoadFX( anim );
	anim->Name = animName;
	anim->BoneCount = rootBoneCount;
	anim->BoneIndices = rage_new int[ rootBoneCount ];
	for( int a = 0; a < rootBoneCount; a++ )
		anim->BoneIndices[ a ] = rootBoneIndices[ a ];
	anim->AffectsOnlyRootNodes = rootBonesOnly;

	m_Animations.Append() = anim;
}
#endif // MESH_LIBRARY

void fragDrawable::LoadBoneTags(rmcTypeFileCbData *data)
{
	Locator loc;

	loc.BoneIndex = data->m_T->GetInt();

	m_Locators.AddLocator(data->m_EntityName, loc);
}

#define MAX_NUM_EXTRA_BOUNDS 64

void fragDrawable::LoadBound(rmcTypeFileCbData *data)
{
	phBound** bound = &m_Bound.ptr;
	Matrix34* matrix = &m_BoundMatrix;

	if (*bound)
	{
		Assertf(m_NumExtraBounds < MAX_NUM_EXTRA_BOUNDS, "Too many bounds on a particular part (max %d)", MAX_NUM_EXTRA_BOUNDS);

		if (m_ExtraBounds.GetCapacity() == 0)
		{
			Assert(m_NumExtraBounds == 0);
			m_ExtraBounds.Resize(MAX_NUM_EXTRA_BOUNDS);
			m_ExtraBoundsMatrices = rage_new Matrix34[MAX_NUM_EXTRA_BOUNDS];
		}

		bound = &m_ExtraBounds[m_NumExtraBounds].ptr;
		matrix = &m_ExtraBoundsMatrices[m_NumExtraBounds];
		++m_NumExtraBounds;
	}

	fiTokenizer *T = data->m_T;
	char buff[RAGE_MAX_PATH];
	T->GetToken(buff, sizeof(buff)); 

	if(stricmp(buff,"none"))
	{
		*bound = phBound::Load(buff);
	}

	// If specified, load position
	if (T->CheckIToken("<", false))
	{
		T->CheckIToken("<");

		matrix->d.x = T->GetFloat();
		matrix->d.y = T->GetFloat();
		matrix->d.z = T->GetFloat();

		T->CheckIToken(">");
	}
	else
	{
		// No position was specified, default to the origin.
		matrix->d.Zero();
	}

	// If specified, load orientation
	if (T->CheckIToken("<", false))
	{
		T->CheckIToken("<");

		Quaternion quat;
		quat.x = T->GetFloat();
		quat.y = T->GetFloat();
		quat.z = T->GetFloat();
		quat.w = T->GetFloat();
		quat.Normalize();

		matrix->FromQuaternion(quat);

		T->CheckIToken(">");
	}
	else
	{
		// No orientation was specified, default to the identity orientation.
		matrix->Identity3x3();
	}

	// If specified, load extra flags
	if (T->CheckIToken("[", false))
	{
		sysMemStartTemp();
		FRAGMGR->AddNewStorageForSpecialFlags();
		sysMemEndTemp();

		T->CheckIToken("[");
		bool done = false;
		do
		{
			char boundFlag[256];
			T->GetToken(boundFlag, sizeof(boundFlag));

			FRAGMGR->InterpretSpecialFlag(boundFlag);

			// Skip the comma if applicable.
			if (T->CheckIToken(",", false))
			{
				T->CheckIToken(",");
			}

			if (T->CheckIToken("]", false))
			{
				T->CheckIToken("]");
				done = true;
			}
		} while (!done);


	}
}


void fragDrawable::LoadLocators(rmcTypeFileCbData *data)
{
	Locator loc;

	data->m_T->MatchToken( "boneindex" );
	loc.BoneIndex = data->m_T->GetInt();
	data->m_T->MatchToken( "offset" );
	data->m_T->GetVector( loc.Offset );
	data->m_T->MatchToken( "eulers" );
	data->m_T->GetVector( loc.Eulers );

	if (data->m_T->CheckToken( "[", false ) )
	{
		data->m_T->CheckToken( "[" );

		while ( data->m_T->CheckToken( "]", false ) == false )
		{
			// Currently, we're just reading in the keys and values, and ignoring them
			const int MAX_KEY_AND_VALUE_LENGTH = 256;
			char keyValue[MAX_KEY_AND_VALUE_LENGTH];
			data->m_T->GetToken( keyValue, MAX_KEY_AND_VALUE_LENGTH );
		}

		data->m_T->CheckToken( "]" );
	}

	m_Locators.AddLocator( data->m_EntityName, loc );
}


void fragDrawable::LoadSkeletonType(rmcTypeFileCbData *data)
{
	fiTokenizer *T = data->m_T;
	char buff[RAGE_MAX_PATH];
	T->GetToken(buff, sizeof(buff)); 

	if(stricmp(buff,"none"))
	{
/*		Assert(!m_SkeletonType);

		ASSET.PushFolder("$/tune/animation");
		m_SkeletonType = sagSkeletonType::CreateSkeletonType(buff);
		ASSET.PopFolder();

		Assert(m_SkeletonType);
		*/

		m_SkeletonTypeName = buff;
	}
}

void fragDrawable::Locator::ComputeGlobalMtx(Matrix34 &outMtx, const crSkeleton &skel) const
{
	int boneCount = skel.GetBoneCount();
	Assertf(BoneIndex >= 0 && BoneIndex < boneCount, "Trying to ComputeGlobalMtx with a locator that has an invalid BoneIndex (%d)", BoneIndex);
	int boneIdx = Clamp(BoneIndex, 0, boneCount - 1);
	Matrix34 boneMtx;
	skel.GetGlobalMtx(boneIdx, RC_MAT34V(boneMtx));
	ConstructMtx(outMtx, &boneMtx);
}

void fragDrawable::Locator::ComputeLocalMtx(Matrix34 &outMtx, const crSkeleton &skel) const
{
	ConstructMtx(outMtx, (BoneIndex>0 && BoneIndex<(int)skel.GetBoneCount()) ? &RCC_MATRIX34(skel.GetLocalMtx(BoneIndex)) : 0);
}

void fragDrawable::Locator::ConstructMtx(Matrix34 &outMtx, const Matrix34 *pParentMtx) const
{
	outMtx.FromEulersXYZ(Eulers);
	if( pParentMtx )
	{
		pParentMtx->Transform(Offset, outMtx.d);
		outMtx.Dot3x3(*pParentMtx);
	}
	else
	{
		outMtx.d.Set(Offset);
	}
}


#if __PFDRAW
void fragDrawable::ProfileDraw(const crSkeleton *pSkel, const Matrix34* transformMat, int lodLevel) const
{
	rmcDrawable::ProfileDraw(pSkel, transformMat, lodLevel);

	if (PFD_Locators.Begin())
	{
		bool oldLighting = grcLighting(false);

		grcWorldIdentity();

		for (LocatorData::Iterator it = m_Locators.CreateIterator(); it; ++it)
		{
			Matrix34 locator;

			if( pSkel )
				it.GetData().ComputeGlobalMtx(locator, *pSkel);
			else
				it.GetData().ConstructMtx(locator, transformMat);
			
			const float SIZE = 0.5f;
			grcDrawAxis(SIZE, locator);

			grcColor(Color32(255,255,255));
			PFD_Locators.Draw2dText(locator.d, it.GetKey());
		}

		grcLighting(oldLighting);

		PFD_Locators.End();
	}
}
#endif // __PFDRAW

#if MESH_LIBRARY
bool fragDrawable::Load(const char *basename, rmcTypeFileParser *parser, bool configParser)
{
	return rmcDrawable::Load(basename, parser, configParser);
}

rmcTypeFileParser *fragDrawable::RegisterParserFunctions(int loadConfig, rmcTypeFileParser* parser)
{
	static rmcTypeFileParser defaultParser;
	if ( !parser )
	{
		parser = &defaultParser;
		parser->Reset();
	}

	parser->RegisterLoader( "bound", "all", datCallback(MFA1(fragDrawable::LoadBound), this, 0, true));
	parser->RegisterLoader( "locator", "all", datCallback(MFA1(fragDrawable::LoadLocators), this, 0, true) );
	parser->RegisterLoader( "bonetag", "all", datCallback(MFA1(fragDrawable::LoadBoneTags), this, 0, true) );

	if (loadConfig & eLoadGraphical)
	{
		parser->RegisterLoader( "shadinggroup", "shadinggroup", datCallback(MFA1(rmcDrawable::LoadShader), this, 0, true) );
		parser->RegisterLoader( "lodgroup", "mesh", datCallback(MFA1(rmcDrawable::LoadMesh), this, 0, true) );
		parser->RegisterLoader( "edge", "edge", datCallback(MFA1(rmcDrawable::LoadEdgeModel), this, 0, true) );
	}

	if (loadConfig & eLoadStructural)
	{
		parser->RegisterLoader( "animation", "anim", datCallback(MFA1(fragDrawable::LoadAnimations), this, 0, true) );
		parser->RegisterLoader( "skeletontype", "all", datCallback(MFA1(fragDrawable::LoadSkeletonType), this, 0, true) );
		if (m_LoadSkeleton)
		{
			parser->RegisterLoader( "skel", "skel", datCallback(MFA1(rmcDrawable::LoadSkel), this, 0, true) );
			parser->RegisterLoader( "skel", "properties", datCallback(MFA1(rmcDrawable::LoadSkelProperties), this, 0, true) );
		}
	}

	return parser;
}

bool fragDrawable::Load(fiTokenizer &T, rmcTypeFileParser *parser, bool configParser)
{	//:bLoadBaseDataSet
	//	This flag indicates whether to load the structural information (locators / skeletontype
	static rmcTypeFileParser defaultParser;
	if ( !parser )
	{
		parser = &defaultParser;
		parser->Reset();
	}
	
	phBound* bound = NULL;

	if ( configParser )
	{
		RegisterParserFunctions(eLoadGraphical|eLoadStructural , parser);
	}

	parser->SetWarningSpew(false);

	// Call the base class Load(), while we handle any callbacks.
	bool retval = rmcDrawable::Load(T, parser, false);

#if __PAGING && !__FINAL
//	if (retval)
//	{
//		int count = GetShaderGroupCount();
//		for (int i=0 ; i<count ; i++)
//		{
//			rmcDrawable::GetLodGroup().SetRemapInfo(GetShaderGroup(i));
//		}
//	}
#endif	

	if( bound )
	{
		m_Bound = bound;
	}
	
	return retval;
}

void fragDrawable::LoadSharedSkeletonData(const char* filename, crSkeletonData*& sharedSkelData)
{
	Assert( !m_SkeletonData );

	if( sharedSkelData )
	{
		m_SkeletonData = sharedSkelData;
		m_SkeletonData->AddRef();

		int version;

		sysMemStartTemp();
		crSkeletonData* tempSkelData = crSkeletonData::AllocateAndLoad(filename, &version);
		Assert(tempSkelData);
		sysMemEndTemp();

		sysMemStartTemp();
		delete tempSkelData;
		sysMemEndTemp();
	}
	else
	{
		int version;
		m_SkeletonData = crSkeletonData::AllocateAndLoad(filename, &version);
		Assert(m_SkeletonData);

		sharedSkelData = m_SkeletonData;
	}
}
#endif		// MESH_LIBRARY


IMPLEMENT_PLACE(fragDrawable)

#if 0
void fragDrawable::UnPlace(fragDrawable &rThis)
{
	//all we do is thunk to the rmcDrawable.  Bounds don't get one(and if they did, it would be as complicated as the current resource construction)...
	rmcDrawable::UnPlace(rThis);
}
#endif

fragDrawable::fragDrawable(datResource &rsc) :
	rmcDrawable(rsc),
	m_ExtraBounds(rsc, true),
	m_Locators(rsc),
	m_SkeletonTypeName(rsc),
	m_Animations(rsc, true)
{
	rsc.PointerFixup(m_ExtraBoundsMatrices);
}

fragDrawable::fragDrawable(datResource &rsc, grmShaderGroup* shaderGroup) :
	rmcDrawable(rsc, shaderGroup),
	m_ExtraBounds(rsc, true),
	m_Locators(rsc),
	m_SkeletonTypeName(rsc),
	m_Animations(rsc)
{
	rsc.PointerFixup(m_ExtraBoundsMatrices);
}


#if __DECLARESTRUCT
void fragDrawable::DeclareStruct(datTypeStruct &s)
{
	rmcDrawable::DeclareStruct(s);

	STRUCT_BEGIN(fragDrawable);
	STRUCT_FIELD(m_BoundMatrix);
	STRUCT_FIELD(m_Bound);
	STRUCT_FIELD(m_ExtraBounds);
	STRUCT_DYNAMIC_ARRAY(m_ExtraBoundsMatrices, m_NumExtraBounds);
	STRUCT_FIELD(m_NumExtraBounds);
	STRUCT_FIELD(m_LoadSkeleton);
	STRUCT_FIELD(m_Pad0);
	STRUCT_FIELD(m_Locators);
	STRUCT_FIELD(m_SkeletonTypeName);
	STRUCT_FIELD(m_Animations);
	STRUCT_FIELD_VP(m_ClonedShaderGroup);

	STRUCT_END();
}
#endif // __DECLARESTRUCT

fragDrawable::LocatorData::LocatorData(datResource& rsc) : m_Locators(rsc)
{
}

#if __DECLARESTRUCT
void fragDrawable::LocatorData::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(fragDrawable::LocatorData);
	STRUCT_FIELD(m_Locators);
	STRUCT_END();
}
#endif // __DECLARESTRUCT


//
//
// duplicates drawable's main shaderGroup into m_ClonedShaderGroup:
//
bool fragDrawable::CloneShaderGroup()
{
	if(GetClonedShaderGroupPtr())
	{
		return true;	// already cloned
	}

	if(this->m_ShaderGroup)
	{
		m_ClonedShaderGroup = rage_aligned_new(16) grmShaderGroup( this->m_ShaderGroup );
		return true;
	}

	return false;
}


} // namespace rage
