// 
// fragment/event.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_EVENT_H
#define FRAGMENT_EVENT_H

#include "event/player.h"

namespace rage {

class fragCollisionEventPlayer : public evtPlayer
{
public:
	fragCollisionEventPlayer();
	fragCollisionEventPlayer(datResource& rsc);

	void CreateParameterList();

	atAny& GetPosition()				{ return m_Parameters.GetParam(s_Position); }
	atAny& GetNormal()					{ return m_Parameters.GetParam(s_Normal); }
	atAny& GetRelativeVelocity()		{ return m_Parameters.GetParam(s_RelativeVelocity); }
	atAny& GetRelativeSpeed()			{ return m_Parameters.GetParam(s_RelativeSpeed); }
	atAny& GetDamage()					{ return m_Parameters.GetParam(s_Damage); }
	atAny& GetNormalizedHealthPrior()	{ return m_Parameters.GetParam(s_NormalizedHealthPrior); }
	atAny& GetNormalizedHealthLeft()	{ return m_Parameters.GetParam(s_NormalizedHealthLeft); }
	atAny& GetInst()					{ return m_Parameters.GetParam(s_Inst); }
    atAny& GetFragInst()				{ return m_Parameters.GetParam(s_FragInst); }
	atAny& GetComponent()				{ return m_Parameters.GetParam(s_Component); }
	atAny& GetPart()					{ return m_Parameters.GetParam(s_Part); }
	atAny& GetOtherInst()				{ return m_Parameters.GetParam(s_OtherInst); }
	atAny& GetOtherComponent()			{ return m_Parameters.GetParam(s_OtherComponent); }
	atAny& GetOtherPart()				{ return m_Parameters.GetParam(s_OtherPart); }
    atAny& GetImpactIterator()			{ return m_Parameters.GetParam(s_ImpactIterator); }

	static void RegisterPlayerClass();
	static void UnregisterPlayerClass();

private:
	static evtParamScheme s_ParameterScheme;

	static evtParamId s_Position;
	static evtParamId s_Normal;
	static evtParamId s_RelativeVelocity;
	static evtParamId s_RelativeSpeed;
	static evtParamId s_Damage;
	static evtParamId s_NormalizedHealthPrior;
	static evtParamId s_NormalizedHealthLeft;
	static evtParamId s_Inst;
    static evtParamId s_FragInst;
	static evtParamId s_Component;
	static evtParamId s_Part;
	static evtParamId s_OtherInst;
	static evtParamId s_OtherComponent;
	static evtParamId s_OtherPart;
    static evtParamId s_ImpactIterator;
};

class fragBreakEventPlayer : public evtPlayer
{
public:
	fragBreakEventPlayer();
	fragBreakEventPlayer(datResource& rsc);

	void CreateParameterList();

    atAny& GetPosition()			{ return m_Parameters.GetParam(s_Position); }
    atAny& GetMatrix()    			{ return m_Parameters.GetParam(s_Matrix); }
	atAny& GetInst()				{ return m_Parameters.GetParam(s_Inst); }
    atAny& GetFragInst()			{ return m_Parameters.GetParam(s_FragInst); }
	atAny& GetComponent()			{ return m_Parameters.GetParam(s_Component); }
	atAny& GetOtherInst()			{ return m_Parameters.GetParam(s_OtherInst); }

	static void RegisterPlayerClass();
	static void UnregisterPlayerClass();

private:
	static evtParamScheme s_ParameterScheme;

    static evtParamId s_Position;
    static evtParamId s_Matrix;
	static evtParamId s_Inst;
    static evtParamId s_FragInst;
	static evtParamId s_Component;
	static evtParamId s_OtherInst;
};

class fragContinuousEventPlayer : public evtPlayer
{
public:
	fragContinuousEventPlayer();
	fragContinuousEventPlayer(datResource& rsc);

	void CreateParameterList();

	atAny& GetMatrix()		{ return m_Parameters.GetParam(s_Matrix); }
	atAny& GetInst()		{ return m_Parameters.GetParam(s_Inst); }
    atAny& GetFragInst()	{ return m_Parameters.GetParam(s_FragInst); }

	static void RegisterPlayerClass();
	static void UnregisterPlayerClass();

private:
	static evtParamScheme s_ParameterScheme;

	static evtParamId s_Matrix;
	static evtParamId s_Inst;
    static evtParamId s_FragInst;
};

} // namespace rage

#endif // FRAGMENT_EVENT_H
