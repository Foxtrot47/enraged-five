// 
// fragment/cache.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_CACHE_H
#define FRAGMENT_CACHE_H

#include "cacheheap.h"
#include "cloth_config.h"
#include "fragmentconfig.h"
#include "instance.h"
#include "manager.h"
#include "type.h"

#include "atl/hashsimple.h"
#if __ASSERT
#include "phbound/boundcomposite.h"
#include "crskeleton/skeleton.h"
#endif // __ASSERT
#include "physics/archetype.h"
#include "vector/quantize.h"
#include "paging/ref.h"
#include "paging/array.h"

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4201)
#endif

#define FRAGMENT_LAZY_ARTICULATED_CACHE 1

#define DEFAULT_INSTABILITY_PREVENTION_FRAMES 10

namespace rage {

class atBitSet;
class bgBreakable;
class bgDrawable;
class fragAnimation;
class grcInstanceData;
class phArticulatedBody;
class phArticulatedBodyPart;
class phArticulatedCollider;
class phBoundComposite;
class phGlass;
class phGlassInst;
class phInstBehavior;
class phJoint;
class fragContinuousEventPlayer;
struct fragGroupInst;
class environmentCloth;
class characterCloth;
class sysTaskWaitThread;
typedef s16 bgGlassHandle;
typedef atDelegate<void (bgGlassHandle)> bgGlassImpactFunc;

// Structure used by fragCacheEntry once the fragType that it corresponds to has been paged in.
// In fragCacheEntry, it is part of a union with another structure that contains information that is
//   stored until the time when fragType is done paging in.
struct fragHierarchyInst
{
    //the fragment instance this cache entry's inst was broken off from
    phHandle            parentInst;

	//there's one of these for each child in the archetype...
	fragGroupInst*      groupInsts;

	// size of the groupInsts array
	int					groupSize;

    //this gets marked "true" if the damaged drawable needs to be drawn
    bool                anyGroupDamaged;

	//for each group above, indicates if it actually exists in the hierarchy...
	// NOTE: Whenever this bitset is modified you must also call fragCacheEntry::UpdateIsFurtherBreakable
	atBitSet*           groupBroken;

#if ENABLE_FRAGMENT_EVENTS
	//indicates that this child does NOT want to have Update of its continuous event player called...
	atBitSet*			childOmitEventUpdate;
#endif // ENABLE_FRAGMENT_EVENTS

	//a pointer to the composite bound, actually m_CompositeBound of the cache entry
	phBoundComposite*   compositeBound;

    //matrices used to draw the children
    crSkeleton*         skeleton;

    //matrices used to draw the children when they are damaged
    crSkeleton*         damagedSkeleton;

    // Set this to switch to drawing one of the "extra" drawables instead of the main one
    int                 drawExtraDrawable;

	// This is created in fragCacheEntry::InitInternal() iff the fragment is found to have at least one bone with rotation limits.
	phArticulatedBody*  body;

	// The index of the child that corresponds to the root link of the articulated body
	int rootLinkChildIndex;

	// If there is one, the articulated collider owned by this object. The body is allowed to be NULL if the object doesn't have any links yet
	phArticulatedCollider*	articulatedCollider;

#if ENABLE_FRAGMENT_EVENTS
	// These are event players for "continuous events", which are running whenever the object exists
	fragContinuousEventPlayer**		continuousPlayers;
#endif // ENABLE_FRAGMENT_EVENTS
	
	// A pointer to an animation that an fragAnimEventInstance might be playing
	fragAnimation*		animation;

	// The type that was being pointed to when the above animation pointer was grabbed, so we can make sure it didn't move
	pgRef<const fragType>		type;

	// The current phase of an animation that an fragAnimEventInstance might be playing
	float				animPhase;

    // Then length of time since the parent cache entry was first initialized
    float               age;

	// Joints that are currently latched
	atBitSet*			latchedJoints;

    // Root latch (for constrained colliders) is broken
    bool                rootLatchBroken;

	// Attachment matrices copied into cache entry, for dynamic link attachments
	pgArray<Matrix34>	*linkAttachment;

	pgRef<environmentCloth> envCloth;

	// Breakable glass
	struct GlassInfo
	{
		Mat34V matrix;
		s16 handle;
		u16 groupIndex;
	};
	GlassInfo* glassInfos;
	int numGlassInfos;


	void SetInstBehavior( phInstBehavior* ptr )
	{
		behavior = ptr;
	}

	phInstBehavior* GetInstBehavior() const
	{
		return behavior;
	}

protected:	
	phInstBehavior*		behavior;	// If there is an inst behavior that goes with this cache entry, it goes here

};

/*
PURPOSE
	The fragCacheEntry contains a fragment instance, plus extra instance data for objects that
	the user has interacted with. 

NOTES
	The fragment cache entry holds state data about which parts of the object have broken off or
	taken damage, and the extra matrices for articulated objects. 

	The fragObjects inserted in the cache can contain physics instances derived from fragInst, but
	they must be the same size as fragInst because they coexist in the fragment cache. When a cache
	entry is initialized, placement rage_new writes the correct virtual pointer into the instance.
<FLAG Component>
*/
class fragCacheEntry
{
	friend class fragCacheManager;
public:
	fragCacheEntry();
	~fragCacheEntry();

	// PURPOSE: Initialize a cache entry
	// NOTES:
	//    Called when a fragInst is being put into the cache
	void Init(const fragType* type,
			  const Matrix34& matrix,
			  fragInst* inst,
			  u16 groupRootIndex = 0xFF, 
			  const crSkeleton* skeleton = NULL,
			  fragCacheEntry* entrySrc = 0);

	// PURPOSE: Called when a type belonging to a cache entry pages in, to complete initialization
	void FinishInit(); 

#if FRAGMENT_LAZY_ARTICULATED_CACHE
    void LazyArticulatedInit();
#endif // FRAGMENT_LAZY_ARTICULATED_CACHE

	// PURPOSE: De-initialize a cache entry
	// NOTES:
	//    Called when a fragInst's cache entry is being taken away
	void UnInit();
	bool IsInit() const;
	void Update(float timeSlice, int simUpdates);
	void ExchangeType(const fragType* type);

	//returns the new mass of the whole(but does not apply that mass) - returns 0 if this has an orphaned root group...
	f32 AdjustForLostGroups(const fragInst::ComponentBits& groupsBreaking);
	fragHierarchyInst* GetHierInst();
	const fragHierarchyInst* GetHierInst() const;

	// Called when switching physics LOD without disrupting the skeleton which other systems rely on
	void ReloadPhysicsData();
	void RemovePhysicsData();
	void RemoveArticulatedBody();
	void DeleteLink(phArticulatedBodyPart *link);
	void DeleteJoint(phJoint *joint);

	// PURPOSE: Set the skeleton data
	// Must have the same number of bones, the same hierarchy and the same boneids as the original skeletondata	
	void SetSkeletonData(const crSkeletonData& skelData);

	// PURPOSE: Copies the skeleton data
	// Must have the same number of bones, the same hierarchy and the same boneids as the original skeletondata	
	void CopySkeletonData(const crSkeletonData& skelData);

#if __ASSERT
	void CheckVelocities();
#endif
	void UpdateSavedVelocityArraySizes(bool zeroVels = false);
#if __PS3
	void ReloadDmaPlan();
#endif

	void CloneBoundParts();
	void DecloneBoundParts();

	fragInst* GetInst();
	const fragInst* GetInst() const;


	// PURPOSE: Get the index of the component that controls the given bone
	// NOTES: This is just an array lookup and should always return an index between 0 and the number of components
	int GetControllingComponentIndexFromBoneIndex(u32 boneIndex) const;

	// PURPOSE: Get the index of the component that this bone is directly attached to
	// NOTES: If no child is directly connected to this bone, this will return -1
	int GetComponentIndexFromBoneIndex(u32 boneIndex) const;

	inline int * GetBoneIndexToComponentMap() const { return m_BoneIndexToComponentMap; }
	inline int * GetComponentToBoneIndexMap() const { return m_ComponentToBoneIndexMap; }
	f32 GetMass(fragHierarchyInst* hierInst);

	//sets the mass for the whole object - seems like there should be a helper function in physics for this, since each part has a different mass...
	void SetMass(f32 fMass);

	// calculate the composite bound's center of gravity
	void CalcCenterOfGravity();

	bool IsGroupUnbroken(int groupIndex) const;
	bool IsGroupBroken(int groupIndex) const;
	bool IsGroupDamaged(int groupIndex) const;
	bool IsGroupLatched(int groupIndex) const;

	//sets this object's sequence number to the current one, making it ineligible for deletion from the cache this frame...
	void SetSequenceNumber();
	u32 GetSequenceNumber() const;

	bool IsFurtherBreakable() const;

	// This should be called whenever fragHierarchyInst::groupsBroken changes
	void UpdateIsFurtherBreakable();

	phBoundComposite* GetBound();
	const phBoundComposite* GetBound() const;
	
	phArchetypeDamp* GetPhysArchetype();

	fragCacheBlockList m_Blocks;

	bool IsUsingBackingInst() const;

	void SetLinkAttachmentRecurseChildren(const fragTypeGroup& group, const Matrix34& linkAttachment);
	void ResetLinkAttachmentRecurseChildren(const fragTypeGroup& group);

	// PURPOSE: Safely recompute all of a links information
	// PARAMS:
	//   groupInLinkIndex - the index of a group in this link
	// NOTES:
	//   To maintain realistic physics this function should be called whenever the mass, angular inertia, or center of mass
	//     of this link changes. 
	//   This function will add numerical error into the system. Calling it every frame is probably a bad idea (for speed reasons as well).
	void RecomputeLink(u8 groupInLinkIndex);

	// PURPOSE: Transform a joint matrix. 
	// PARAMS:
	//   linkFromJointRotation - rotation from the joint to the link , transposed. It will be set to the new rotation from the joint to the link, transposed
	//   linkToJoint - the position of the joint in link space, it will be set to the new position of the joint in link space
	//   linkFromOldLink - matrix that transforms from the old link space to the new link space 
	// NOTE: This shouldn't need to be it's own function, but the joint interface is a little messy
	static void TransformJoint(Mat33V_InOut linkFromJointRotation, Vec3V_InOut linkToJoint, Mat34V_In linkFromOldLink);

	void ShatterGlassOnBone(int groupIndex, int boneIndex, Vec3V_In position, Vec3V_In impact, int glassCrackType);
	void PurgeBreakableGlassInfos(const fragInst::ComponentBits& groups);
	void AllocateGlassInfo();
	void CreateBreakableGlassOnBone(int groupIndex, int boneIndex, bgGlassImpactFunc impactFunc);

#if ENABLE_FRAGMENT_EVENTS
	void StartEvents();
	void StopEvents();
	bool PlayEventsAtStart();
	void SetPlayEventsAtStart(bool value);
	bool IsPlayingEvents();
#endif // ENABLE_FRAGMENT_EVENTS
	void SetFoundAttachment (bool found=true);
	void SetActivateOnHit(bool use=true);
	bool ShouldActivateOnHit() const;
	bool FoundAttachment () const;
	bool HasClonedBounds () const;

	void SetIncreaseMinStiffnessOnActivation(bool set) { m_IncreaseMinStiffnessOnActivation = set; } 

	void SetShuoldUseInstanceToArtColliderOffsetMat(bool set) { m_ShouldUseInstanceToArtColliderOffsetMat = set; } 
	bool IsIntendingToUseInstanceToArtColliderOffsetMat() const { return m_ShouldUseInstanceToArtColliderOffsetMat; } 

	void ActivateInstabilityPrevention(s8 frames = DEFAULT_INSTABILITY_PREVENTION_FRAMES) { m_InstabilityPreventionFrames = frames; m_InstabilityPreventionInitialized = false; }
	bool IsPreventingInstability() { return m_InstabilityPreventionInitialized && m_InstabilityPreventionFrames >= 0; }

	static void SetAttachmentIncludeFlags (u32 flags) { sm_AttachmentIncludeFlags = flags; }
	static void SetAttachmentTypeFlags (u32 flags) { sm_AttachmentTypeFlags = flags; }

	void ResetSelfCollisions();

#ifdef GTA_REPLAY_RAGE
	static void SuppressClothCreation(bool suppress) { sm_SuppressClothCreation = suppress; }
#endif

private:

	void UninitAllGlass();

	// Update breakable glass - call only while render thread is idle
	void UpdateAge(float timeStep);
#if ENABLE_FRAGMENT_EVENTS
	void UpdateEvents();
#endif // ENABLE_FRAGMENT_EVENTS
	void UpdateGlassBreakables(float timeStep);

	void InitializeInstabilityPrevention();
	void UpdateInstabilityPrevention(int simUpdates);
	void ClearInstabilityPrevention();

	bool WillBeArticulated() const;
	bool WillBeSimpleArticulated() const;
    bool WillBeConstrained() const;

	void InitInternal(u16 groupRootIndex, const crSkeleton* skeleton, fragCacheEntry* entrySrc = NULL);

	void InitBitsets(fragHierarchyInst* hierSrc);
	void InitBound(fragHierarchyInst* hierSrc);
	void InitEnvCloth(fragHierarchyInst* hierSrc);
	void UninitEnvCloth();

	void InitSkeleton();
	void InitSkeleton2(fragHierarchyInst* hierSrc, const crSkeleton* skeleton);
	void InitComponentAndBoneIndexMaps();
	void InitPhysics(fragHierarchyInst* hierSrc, u16 groupIndex, bool cloneParts);
	void InitArticulated(fragHierarchyInst* hierSrc);
	void RefreshArticulated();
	void InitConstrained();
	void InitSelfCollisions();
#if ENABLE_FRAGMENT_EVENTS
	void InitEvents();
#endif // ENABLE_FRAGMENT_EVENTS
	void AddChildMassAndAngInertia (float& mass, Vector3& angInertia, Vector3& cgOffset, const fragTypeChild* child, int childIndex, bool damaged=false);

	// PURPOSE: Get the center of mass, angular inertia, and mass of a fragTypeChild in instance space
	// PARAMS:
	//   childIndex - index of the child to get the mass properties of
	//   centerOfGravity - will be set to the center of gravity of the child, in instance space
	//   angularInertia - will be set to the angularInertia of the child about its center of gravity, rotated into instance space
	//   mass - will be set to the mass of the child
	void ComputeChildMassProperties(u8 childIndex, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass);

	void GetParentGroupAndBone(int childIndex, const int linkForBone[], int& outParentGroupIndex, int& outParentBoneIndex) const;


	// PURPOSE: Allocate the articulated body, first articulated link, and the articulated collider if it doesn't
	//          already exist. Then initialize them. 
	// PARAMS:
	//   rootLinkInertia - the angular inertia of the root link
	//   rootLinkMass - the mass of the root link 
	// NOTES: The root link always lined up with the instance, since we don't want to move the instance during initialization, 
	//        the root link orientation is assumed to be the same as the instance. The position is dependent on whatever the 
	//        CG offset of the composite bound currently is. Prior to calling this function the user should set the bound CG
	//        offset to the root link CG in instance space. 
	void InitArticulatedRoot(Vec3V_In rootLinkInertia, ScalarV_In rootLinkMass);

	void InitArticulatedBody(phArticulatedBodyPart& rootLink);

	// PURPOSE: Convert a groupIndex-to-linkIndex table into the articulated colliders componentIndex-to-linkIndex table
	//          The collider doesn't care about groups, so it's faster for it to just map directly from component to link.
	// PARAMS:
	//   groupIndexToLinkIndex - table of link indices where a group index can be used as an index into the table
	void InitComponentIndexToLinkIndexTable(const u8* groupIndexToLinkIndex);
public:


	// PURPOSE: Allocate, initialize and add a new joint and link
	// PARAMS:
	//   parentLinkIndex - the link index of the new link's parent
	//   linkToWorldRotation - the rotation of the link in world space
	//   linkPositionInstance - the position of the link, in instance space (instance space is the same
	//                            orientation as the root link)
	//   linkAngularInertia - the angular inertia of the link
	//   linkMass - the mass of the link
	//   firstGroupIndexOfChildLink - index of the first group of the child link
	//   handleCacheMemory - if true, this will explicitly create a cache heap to allocate from.
	// RETURN:
	//   A reference to the new joint
	phJoint& AddJointAndLink(	const u8 parentLinkIndex,
								Mat33V_In linkToWorldRotation,
								Vec3V_In linkPositionInstance,
								Vec3V_In linkAngularInertia,
								ScalarV_In linkMass,
								const int firstGroupIndexOfChildLink,
								bool handleCacheMemory = false);

	void SetFixedArticulatedRoot(bool fixedArticulatedRoot);
	bool HasFixedArticulatedRoot() const;
private:
	// PURPOSE: Allocate and initialize a new link
	// PARAMS:
	//   linkToWorldRotation - the rotation of the link in world space
	//   rootLinkToLinkInstance - the vector from the root link, to this link, in instance space (instance space is the same
	//                            orientation as the root link)
	//   linkAngularInertia - the angular inertia of the link
	//   linkMass - the mass of the link
	// RETURN: pointer to newly allocated/initialized articulated body part
	// NOTES:
	//   If the linkAngularInertia is too small it and the mass will be scaled.
	//   This doesn't add the link to the body
	phArticulatedBodyPart* CreateArticulatedLink(Mat33V_In linkToWorldRotation, Vec3V_In rootLinkToLinkInstance);

	void InitArticulatedLinkInertia(int linkIndex, Vec3V_In linkAngularInertia, ScalarV_In linkMass DEV_ONLY(,u8 groupIndex));

	phJoint* CreateArticulatedJoint(const crBoneData& bone,
									const fragTypeGroup& tuningGroup,
									u8 childLinkIndex,
									u8 parentLinkIndex,
									Vec3V_In rootLinkToJointInstance,
									Mat33V_In boneToInstanceRotation);

	u32                             m_SequenceNumber;

	u32                             m_Flags;

	phArchetypeDamp                 m_PhysDamp;

	fragInst*		                m_BackingInst;
	phBoundComposite*               m_CompositeBound;
	fragInst*						m_Inst;					// Pointer to the *actual* fragInst that we're using.  That may or may not be our m_BackingInst.

	int*							m_BoneIndexToComponentMap;
	int*							m_ComponentToBoneIndexMap;

	const crSkeleton*   m_InitSkeleton;
	u16                 m_InitGroupIndex;

	DLinkSimple<fragCacheEntry> link;

	enum Flags
	{
		INITIALIZED				= BIT(0),
		STREAMING_IN			= BIT(1),
#if ENABLE_FRAGMENT_EVENTS
		PLAY_EVENTS_AT_START	= BIT(2),
		IS_PLAYING_EVENTS		= BIT(3),
#endif // ENABLE_FRAGMENT_EVENTS
		FOUND_ATTACHMENT		= BIT(4),
		ACTIVATE_ON_HIT			= BIT(5),
		CLONED_BOUND_PARTS		= BIT(6),
		FURTHER_BREAKABLE		= BIT(7),
		ART_FIXED_ROOT          = BIT(8)
	};

	fragHierarchyInst       m_HierInst; 

#if ENABLE_FRAGMENT_EVENTS
	// An array of the children that are playing events
	int*					m_ChildrenWithEvents;
	int						m_NumChildrenWithEvents;
#endif // ENABLE_FRAGMENT_EVENTS

	s8 m_InstabilityPreventionFrames;

	bool m_InstabilityPreventionInitialized : 1;
	bool m_ShouldUseInstanceToArtColliderOffsetMat : 1;
	bool m_IncreaseMinStiffnessOnActivation : 1;

	static u32 sm_AttachmentIncludeFlags;
	static u32 sm_AttachmentTypeFlags;

public:
	BANK_ONLY(static bool sm_DisableCompositeBvhGeneration;)

private:

	void SetInit(bool bInitialized);
	bool IsStreamingIn() const;
	void SetStreamingIn(bool streamingIn);

#ifdef GTA_REPLAY_RAGE
	static bool sm_SuppressClothCreation;
#endif
};

/*
  PURPOSE
    The class stored in a hash table by the cache manager, for quickly locating fragment instances
    that had been interacted with, then paged out, and paged back in.
    
*/
class fragCacheKey
{
public:
    fragCacheKey(){m_RefCount = 0;}
    fragCacheKey(u32 guid);

	u32 GetGuid() const;
    
    u32 GenerateHash() const;
    bool IsEqual(const fragCacheKey* compKey) const;
    
    u16 GetRefCount();
    void IncRefCount();
    void DecRefCount();

private:
    //these two form a GUID that forms the basis for our hash...
    u32     m_Guid;
    
    /*each time a fragment goes into the cache, it (and all its progeny) set this so that as long as 
    any of them are alive, this structure stays around keeping fragments from getting loaded.  Because 
    multiple fragments can reference the same key, we cannot get from a key to a particular cache 
    entry...
    */
    u16     m_RefCount;
};

inline fragCacheKey::fragCacheKey(u32 guid)
{
    m_Guid = guid;
    m_RefCount = 0;
}

inline u32 fragCacheKey::GetGuid() const
{
    return m_Guid;
}

inline u32 fragCacheKey::GenerateHash() const
{
    u32     hash;
    
    hash = m_Guid;
    hash = SimpleHash<fragCacheKey>::SampleHash(hash);
    
    return hash;
}

inline bool fragCacheKey::IsEqual(const fragCacheKey* compKey) const
{
    return m_Guid == compKey->m_Guid;
}

inline u16 fragCacheKey::GetRefCount() 
{
    return m_RefCount; 
}

inline void fragCacheKey::IncRefCount()
{
    ++m_RefCount;
}

inline void fragCacheKey::DecRefCount()
{
    FastAssert(m_RefCount);
    --m_RefCount;
}
struct fragGroupInst
{
	bool IsDamaged() const { return damageHealth < 0; }
	f32     damageHealth;
	f32		weaponHealth;
};

inline fragInst* fragCacheEntry::GetInst()
{
    return m_Inst;
}

inline const fragInst* fragCacheEntry::GetInst() const
{
	return m_Inst;
}

inline int fragCacheEntry::GetControllingComponentIndexFromBoneIndex(u32 boneIndex) const
{
	Assertf(boneIndex < m_HierInst.skeleton->GetBoneCount(), "Bone index %i is not valid. Only %i bones on this skeleton.",boneIndex,m_HierInst.skeleton->GetBoneCount());
	return m_BoneIndexToComponentMap[boneIndex];
}
inline int fragCacheEntry::GetComponentIndexFromBoneIndex(u32 boneIndex) const
{
	int controllingComponentIndex = GetControllingComponentIndexFromBoneIndex(boneIndex);
	Assert(controllingComponentIndex < m_CompositeBound->GetNumBounds());

	// If the component this bone is attached to isn't attached to the bone, then the bone doesn't
	//   own the component. 
	if(controllingComponentIndex != -1 && m_ComponentToBoneIndexMap[controllingComponentIndex] == (s32)boneIndex)
	{
		return controllingComponentIndex;
	}
	else
	{
		return -1;
	}
}

inline bool fragCacheEntry::IsGroupUnbroken(int groupIndex) const
{
	return m_HierInst.groupBroken->IsClear(groupIndex);
}

inline bool fragCacheEntry::IsGroupBroken(int groupIndex) const
{
	return m_HierInst.groupBroken->IsSet(groupIndex);
}
inline bool fragCacheEntry::IsGroupDamaged(int groupIndex) const
{
	return m_HierInst.groupInsts[groupIndex].IsDamaged();
}
inline bool fragCacheEntry::IsGroupLatched(int groupIndex) const
{
	return m_HierInst.latchedJoints && m_HierInst.latchedJoints->IsSet(groupIndex);
}

inline bool fragCacheEntry::IsInit() const
{
    return (m_Flags & INITIALIZED) != 0;
}

inline fragHierarchyInst* fragCacheEntry::GetHierInst()
{
	return &m_HierInst;
}

inline const fragHierarchyInst* fragCacheEntry::GetHierInst() const
{
	return &m_HierInst;
}

inline u32 fragCacheEntry::GetSequenceNumber() const
{
    return m_SequenceNumber;
}

inline phBoundComposite* fragCacheEntry::GetBound()
{
    return m_CompositeBound;
}

inline const phBoundComposite* fragCacheEntry::GetBound() const
{
    return m_CompositeBound;
}

inline phArchetypeDamp* fragCacheEntry::GetPhysArchetype()
{
	return &m_PhysDamp;
}

inline void fragCacheEntry::SetInit(bool initialized)
{
    if (initialized)
    {
        m_Flags |= INITIALIZED;
    }
    else
    {
        m_Flags &= ~INITIALIZED;
    }
}

inline bool fragCacheEntry::IsStreamingIn() const
{
    return (m_Flags & STREAMING_IN) != 0;
}

inline void fragCacheEntry::SetStreamingIn(bool streamingIn)
{
    if (streamingIn)
    {
        m_Flags |= STREAMING_IN;
    }
    else
    {
        m_Flags &= ~STREAMING_IN;
    }
}

#if __WIN32
#pragma warning(pop)
#endif

inline void fragCacheEntry::SetActivateOnHit(bool value)
{
	if (value) 
	{
		m_Flags |= ACTIVATE_ON_HIT;
	}
	else 
	{
		m_Flags &= ~ACTIVATE_ON_HIT;
	}
}

inline bool fragCacheEntry::ShouldActivateOnHit() const
{
	return (m_Flags & ACTIVATE_ON_HIT) != 0;
}

#if ENABLE_FRAGMENT_EVENTS
inline void fragCacheEntry::SetPlayEventsAtStart(bool value)
{
	if (value) 
	{
		m_Flags |= PLAY_EVENTS_AT_START;
	}
	else 
	{
		m_Flags &= ~PLAY_EVENTS_AT_START;
	}

}

inline bool fragCacheEntry::PlayEventsAtStart()
{
	return (m_Flags & PLAY_EVENTS_AT_START) != 0;
}

inline bool fragCacheEntry::IsPlayingEvents()
{
	return (m_Flags & IS_PLAYING_EVENTS) != 0;
}
#endif // ENABLE_FRAGMENT_EVENTS

inline void fragCacheEntry::SetFoundAttachment (bool found)
{
	if (found)
	{
		m_Flags |= FOUND_ATTACHMENT;
	}
	else
	{
		m_Flags &= ~FOUND_ATTACHMENT;
	}
}

inline bool fragCacheEntry::FoundAttachment () const
{
	return (m_Flags & FOUND_ATTACHMENT) != 0;
}

inline bool fragCacheEntry::HasClonedBounds () const
{
	return (m_Flags & CLONED_BOUND_PARTS) != 0;
}

inline bool fragCacheEntry::IsFurtherBreakable() const
{
	return (m_Flags & FURTHER_BREAKABLE) != 0;
}

inline bool fragCacheEntry::HasFixedArticulatedRoot() const
{ 
	return (m_Flags & ART_FIXED_ROOT) != 0; 
}

inline crSkeleton* GetSkeletonFromFragInst(const fragInst* pFragInst)
{
	if(pFragInst->GetCached() && pFragInst->GetType())
	{
		// If we're in the cache AND our type has been paged in let's use that skeleton.
		fragCacheEntry* entry = pFragInst->GetCacheEntry();
		fragHierarchyInst* hierInst = entry->GetHierInst();

		return hierInst->skeleton;
	}

	// Sorry folks, no skeleton here.
	return NULL;
}

} // namespace rage

#endif // FRAG_CACHE_H
