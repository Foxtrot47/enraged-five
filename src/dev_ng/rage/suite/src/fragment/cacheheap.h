// 
// fragment/cacheheap.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_CACHEHEAP_H
#define FRAGMENT_CACHEHEAP_H

#include "atl/slistsimple.h"
#include "atl/map.h"
#include "atl/pool.h"
#include "grcore/device.h"
#include "system/memory.h"

#define FRAG_MAX_OVERAGE (3 << 20)
#define FRAG_CACHE_HEAP_DEBUG (!__FINAL)

#if FRAG_CACHE_HEAP_DEBUG
#define FRAG_CACHE_HEAP_DEBUG_ONLY(x) x
#else
#define FRAG_CACHE_HEAP_DEBUG_ONLY(x)
#endif

#define FRAG_CACHE_POP_REUSE_MAX (6 << 20)

namespace rage {

class fragCacheEntry;

// Frag Cache
struct fragCacheBlock
{
	void* ptr;
	SLinkSimple<fragCacheBlock> link;
};

typedef atSimplePooledMapType<void*, fragCacheBlock*, sizeof(void*)> fragCacheBlockHashTable;
typedef SLListSimple<SLIST_SIMPLE_INIT(fragCacheBlock, link)> fragCacheBlockList;
extern void fragMemStartCacheHeapFunc(fragCacheEntry* entry);

FRAG_CACHE_HEAP_DEBUG_ONLY(typedef void (*StreamingInterestsCallback)();)

// PURPOSE
//   Begin redirecting all memory allocations to grow-only heap in a particular block of memory.
// PARAMS
//   expectedSize - attempt to get a cache block of this size or larger
// NOTES
//   These calls will NOT nest.
#define fragMemStartCacheHeap() fragMemStartCacheHeapFunc(this)

// PURPOSE
//   Restores memory allocator to previous location
// RETURNS
//   Returns amount of heap remaining.
extern size_t fragMemEndCacheHeap();

class fragCacheAllocator : public sysMemAllocator
{
	friend class fragCacheEntry;
	friend void fragMemStartCacheHeapFunc(fragCacheEntry* entry);
	friend size_t fragMemEndCacheHeap();

private:
	sysMemAllocator* m_pAllocator;
	atPool<fragCacheBlock> m_pool;
	fragCacheBlockHashTable m_hashTable;

#if FRAG_CACHE_HEAP_DEBUG
	int m_peakUsed;
	int m_totalUsed;
	static StreamingInterestsCallback m_streamingInterestsCallback;
#endif

private:
	fragCacheBlock* AllocateBlock(size_t size, size_t align = 16, int heapIndex = 0);
	void FreeBlock(fragCacheBlock* block);

	inline sysMemAllocator* GetBackupAllocator() const
	{ 
		return sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL); 
	}

#if FRAG_CACHE_HEAP_DEBUG && !__TOOL && !__RESOURCECOMPILER
	void OutOfMemory();
#endif

public:
	fragCacheAllocator();
	fragCacheAllocator(void* ptr, size_t bytes, size_t size);
	virtual ~fragCacheAllocator();

	// PURPOSE:	Init the pool and heap
	void Init(void* const ptr, const size_t bytes, const size_t size);

	virtual void* Allocate(size_t size, size_t align, int heapIndex = 0);
	virtual void* TryAllocate(size_t size, size_t align, int heapIndex = 0) { return Allocate(size, align, heapIndex); }
	void* ExternalAllocate(size_t size, size_t align, int heapIndex = 0);

	virtual void Free(const void* ptr);
	void ExternalFree(const void* ptr);

	virtual bool IsTallied() { return false; }
	virtual bool IsValidPointer(const void* ptr) const;
	BANK_ONLY(virtual bool IsValidUsedPointer(const void *ptr) const;)
	inline bool IsValidHeaderPointer(const void* ptr) const {return m_hashTable.Access(const_cast<void*>(ptr)) != NULL;}

	// Accessors
	virtual void* GetHeapBase() const { return m_pAllocator->GetHeapBase(); }
	virtual size_t GetHeapSize() const { return m_pAllocator->GetHeapSize(); }
	virtual size_t GetMemoryAvailable() { return m_pAllocator->GetMemoryAvailable(); }

#if FRAG_CACHE_HEAP_DEBUG
	virtual size_t GetMemoryUsed(int UNUSED_PARAM(bucket) = -1 /*all buckets*/) { return static_cast<size_t>(m_totalUsed); }
#else
	virtual size_t GetMemoryUsed(int bucket = -1 /*all buckets*/) { return m_pAllocator->GetMemoryUsed(bucket); }
#endif

	virtual size_t GetSize(const void* ptr) const;
	virtual size_t GetSizeWithOverhead(const void* ptr) const;
	virtual size_t GetLargestAvailableBlock() {return m_pAllocator->GetLargestAvailableBlock(); }
	void SanityCheck()
	{
		m_pAllocator->SanityCheck();
		GetBackupAllocator()->SanityCheck();
	}

#if FRAG_CACHE_HEAP_DEBUG
	virtual size_t GetHighWaterMark(bool reset);
	atPool<fragCacheBlock>* GetPool() { return &m_pool;}
	static void SetStreamingInterestsCallback(StreamingInterestsCallback callback) { m_streamingInterestsCallback = callback; }
#endif

#if __WIN32PC
	virtual bool IsBuildingResource() const 
	{
		// lame-ass hack for detecting a resource compiler
		return !GRCDEVICE.IsCreated();
	}
#endif
};
}

#endif // FRAGMENT_CACHEHEAP_H