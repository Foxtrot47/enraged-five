// 
// fragment/tune.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_TUNE_H
#define FRAGMENT_TUNE_H

#include "fragmentconfig.h"

#include "atl/map.h"
#include "atl/singleton.h"
#include "data/base.h"
#include "input/mapper.h"
#include "parsercore/stream.h"	// Necessary for gcc 3.4
#include "parser/tree.h"	// Necessary for gcc 3.4
#include "parser/macros.h"
#include "system/param.h"
#include "pheffects/tune.h"
#include "physics/instbreakable.h"
#include "fragment/cloth_config.h"
#include "paging/ref.h"

// Defines for damping tuning
#define MAX_NUM_DAMPING_CHILDREN 3
#define MAX_DAMPING 10.0f

namespace rage {

class fragInst;
class fragType;
class fragTypeChild;
class fragTypeGroup;
class bkBank;
class bkCombo;
class bkGroup;
class grcViewport;

////////////////////////////////////////////////////////////////////////////////

/*
  PURPOSE
	Allows in game tuning of fragment properties through widgets.
	
  NOTES
	There are several "global" variables that allow tuning of every fragment type simultaneously,
	as well as widgets for each type. As fragment types are modified, the part you're modifying
	is highlighted with debug drawing on all the fragment instances of that type in the game.

	<FLAG Component>
*/

class fragTuneBreakPreset;

class fragTuneStruct : public datBase
{
public:
#if HACK_GTA4
	int   GetBreakImpulseSourceTypeFlags() const;
#endif
	float GetGlobalMaxDrawingDistance() const;
	float GetGlobalForceTransmissionScaleDown() const;
	float GetGlobalForceTransmissionScaleUp() const;
	float GetGlobalRootBreakHealthScale() const;
	float GetGlobalBreakHealthScale() const;
	float GetGlobalDamageHealthScale() const;
	float GetBreakingFrameRateLimit() const;
	bool  GetEnableCompleteImpulsePropagation() const;
	bool  GetAggressiveAssetTouching() const;

	bool  GetFragDebug1() const;
	bool  GetFragDebug2() const;

	bool  GetAllowArticulation() const;
	bool  GetAllowSimplifiedColliders() const;

	float GetCollisionDamageScale() const;
	float GetCollisionDamageMinThreshold() const;
	float GetCollisionDamageMaxThreshold() const;
	float GetLodDistanceBias() const;

	void  Load(const char* filename=NULL);
	void  SetForcedTuningPath(const char* forcedPath);
	const char* GetForcedTuningPath();

	void Update(const grcViewport* viewport = NULL, bool updateMouseSelection=true);

	bkBank* GetBank();

	void ApplyBreakPresetToGroup(fragTypeGroup& group, int preset);
	int FindPresetByName(const char* presetName);
	const char* GetNameForPreset(int preset);

#if __BANK
	// PURPOSE:
	//	Set the ComboSelection member to match the type associated with the specified frag.
	void SetType(fragType* selectedType, int componentSelection = 0, fragInst* selectedInst = NULL);

	void SaveArchetypeCallback(fragType* type);
	void SetToDefault(fragType* type);
	void SetToDefaultByMass(fragType* type);
	void RepeatFromChild(int index);
	void CopyPasteToAllGroups(int index);
	void CopyFromGroup(int index);
	void PasteToGroup(int index);
	void PasteToGroupsRecursive(int index);
	void SelectAllFields();
	void DeselectAllFields();
	void AdjustRootCGOffset(fragType* type);
	void RestoreComputedCG(fragType* type);
	void AdjustGravityFactor(fragType* type);
	void AdjustBuoyancyFactor(fragType* type);
	void AdjustChildMass(fragTypeGroup* group);
    void AdjustGroupMass(fragTypeGroup* group);
	void AdjustDamageHealth(int groupIndex);
	void AdjustInstanceDamageHealth(int index);
	void AdjustWeaponHealth(int groupIndex);
	void AdjustInstanceWeaponHealth(int index);
    void AdjustTypeMass(fragType* type);
	void AdjustMinMoveForce(fragType* type);
	void ApplyArchetypeBreakPresetCallback(fragType* type);
	void ResetGroupPresetCallback(fragTypeGroup* group);
	void ApplyGroupBreakPresetCallback(int groupIndex);
	void ApplyGroupBreakPresetRecursiveCallback(int groupIndex);
#if ENABLE_FRAGMENT_EVENTS
	void CreateTypeCollisionEventsCallback(fragType* type);
	void CreateGroupDeathEventsCallback(fragTypeGroup* group);
	void CreateChildCollisionEventsCallback(fragTypeChild* child);
	void CreateChildBreakEventsCallback(fragTypeChild* child);
	void CreateChildBreakFromRootEventsCallback(fragTypeChild* child);
#endif // ENABLE_FRAGMENT_EVENTS

	void AddWidgets(bkBank& bank);
	void DestroyWidgets(bkBank &);
	void AddArchetypeWidgets(fragType* type);
    void RemoveArchetypeWidgets(fragType* type);
	bkGroup* GetWidgetsForType(const fragType* type);
	fragType* GetTypeBeingTuned();
	fragType* GetTypeHovered();
    int GetSelectedComponent();
	void RefreshTypeList();
	void AddSelfCollision(fragType* type);
	void CheckForRemoveSelfCollision(fragType* type);

	void ResetInstance();
	void PutIntoCache();
	void BreakGroup(int group);
	void DeleteAboveGroup(int group);
	void RestoreAboveGroup(int group);
	void LatchGroup(int group);
	void UnlatchGroup(int group);

	void Save(const char* filename=NULL);
	void UpdateMouseSelection(const grcViewport* viewport, u32 probeIncludeFlags = (u32) -1, u32 probeExcludeFlags = 0);
	bool IsDebugDrawSelectedFragEnabled() const;

	static void ResetAudioType(fragType* type);

	typedef atDelegate<void (void*)> UpdateEntityFunctor;
	static void SetUpdateEntityFunctor(UpdateEntityFunctor updateEntityFunctor);

	typedef atDelegate<void (char*,fragType*)> PreSaveEntityFunctor;
	static void SetPreSaveEntityFunctor(PreSaveEntityFunctor preSaveCallback);

	typedef atDelegate<void (void*)> SelectEntityFunctor;
	static void SetSelectEntityFunctor(SelectEntityFunctor selectEntityFunctor);
#endif

protected:

	virtual ~fragTuneStruct();
	fragTuneStruct();

#if __BANK
    void SelectFromTypeComboCb();
    void RegenerateTuningWidgets();
	void AddTuningWidgetsToBank(fragType* type, int componentSelection);
	virtual void AddWidgetsToBank(fragType* type, int componentSelection);
	void AddGroupWidgets(bkBank& bank, int index, fragType* type);
	bool LeftClickSelectEnabled;
	bool InstanceTuningEnabled;
	bool SelectWholeTypeEnabled;
	bool DebugDrawSelectedFrag;
	bool WakeAllAffectedFragsOnEdit;
#endif // __BANK

	float LodDistanceBias;
#if __BANK
	struct DampingNode
	{
		DampingNode();
		float ComputeMaximumScalingAllowed();
		float ComputeChildRatioAverage();
		float ComputeChildAverage();
		void UpdateFromChildren();
		void SetAverage(float average);
		void Update();
		void UpdateAverage();
		void SetChild(int childIndex, DampingNode& node);
		void Initialize(float initialValue);
		void InitializeFromLeaves();

		DampingNode* Parent;
		DampingNode* Children[MAX_NUM_DAMPING_CHILDREN];
		float Ratio;
		float Average;
	};
	DampingNode Damping;
	DampingNode LinearDamping;
	DampingNode LinearSpeedDamping;
	DampingNode LinearSpeedXDamping,LinearSpeedYDamping,LinearSpeedZDamping;
	DampingNode LinearVelocityDamping;
	DampingNode LinearVelocityXDamping,LinearVelocityYDamping,LinearVelocityZDamping;
	DampingNode LinearVelocitySquaredDamping;
	DampingNode LinearVelocitySquaredXDamping,LinearVelocitySquaredYDamping,LinearVelocitySquaredZDamping;
	DampingNode AngularDamping;
	DampingNode AngularSpeedDamping;
	DampingNode AngularSpeedXDamping,AngularSpeedYDamping,AngularSpeedZDamping;
	DampingNode AngularVelocityDamping;
	DampingNode AngularVelocityXDamping,AngularVelocityYDamping,AngularVelocityZDamping;
	DampingNode AngularVelocitySquaredDamping;
	DampingNode AngularVelocitySquaredXDamping,AngularVelocitySquaredYDamping,AngularVelocitySquaredZDamping;

	void AdjustDamping(DampingNode* dampingNode);
#endif // __BANK

#if HACK_GTA4
	int BreakImpulseSourceTypeFlags;
#endif
	float GlobalMaxDrawingDistance;
	float GlobalForceTransmissionScaleDown;
	float GlobalForceTransmissionScaleUp;
	float GlobalRootBreakHealthScale;
	float GlobalBreakHealthScale;
	float GlobalDamageHealthScale;
	float BreakingFrameRateLimit;
	bool EnableCompleteImpulsePropagation;
	bool  AggressiveAssetTouching;

	bool FragDebug1;
	bool FragDebug2;
	bool AllowArticulation;
	bool AllowSimplifiedColliders;

	float CollisionDamageScale;
	float CollisionDamageMinThreshold;
	float CollisionDamageMaxThreshold;

	float ReferenceMassForDefaultTuning;

	atArray<fragTuneBreakPreset> m_BreakPresets;

	const char* ForcedTuningPath;

	bkBank* Bank;
	bkGroup* TypeBank;
	bkCombo* TypeCombo;

    float TypeTotalUndamagedMass;
	int TypeBreakPresetSelection;

	sysCriticalSectionToken m_TunerCriticalSectionToken;
	int ComboSelection;
	pgRef<fragType> CurrentFragType;
	pgRef<fragType> HoverFragType;
	u16 CurrentFragInstLevelIndex;
	u16 CurrentFragInstGenerationId;
	bool CurrentFragInstCached;
	int TruncateCount;
	int NumComboItems;
	int MaxNumComboItems;
    int ComponentSelection;

	fragType** ComboTypes;

	ioMapper m_Mapper;
    ioValue m_SelectUnderMouse;
	ioValue m_MouseButton;

    int m_PressedMouseX;
    int m_PressedMouseY;

	bool m_RecreateWidgets;
	bool m_RegenerateTuningWidgets;

	float m_DamageHealthProxies[phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS];
	float m_WeaponHealthProxies[phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS];

#if __BANK
	static UpdateEntityFunctor sm_UpdateEntityFunctor;
	static SelectEntityFunctor sm_SelectEntityFunctor;
	static PreSaveEntityFunctor sm_PreSaveEntityFunctor;
#endif // __BANK

	PAR_PARSABLE;
};

typedef	atSingleton<fragTuneStruct>	fragTune;
#define FRAGTUNE (fragTune::InstancePtr())

#if __DEV
XPARAM(FragGlobalMaxDrawingDistance);
#endif

inline int fragTuneStruct::GetBreakImpulseSourceTypeFlags() const
{
	return BreakImpulseSourceTypeFlags;
}

inline float fragTuneStruct::GetGlobalMaxDrawingDistance() const
{
#if __DEV
	static float maxDistance;
	static bool overrideDistance=PARAM_FragGlobalMaxDrawingDistance.Get(maxDistance);
	if (overrideDistance)
		return maxDistance;
	else
#endif
	return GlobalMaxDrawingDistance;
}

inline float fragTuneStruct::GetGlobalForceTransmissionScaleDown() const
{
	return GlobalForceTransmissionScaleDown;
}

inline float fragTuneStruct::GetGlobalForceTransmissionScaleUp() const
{
	return GlobalForceTransmissionScaleUp;
}

inline float fragTuneStruct::GetGlobalRootBreakHealthScale() const
{
	return GlobalRootBreakHealthScale;	
}

inline float fragTuneStruct::GetGlobalBreakHealthScale() const
{
	return GlobalBreakHealthScale;
}

inline float fragTuneStruct::GetGlobalDamageHealthScale() const
{
	return GlobalDamageHealthScale;	
}

inline float fragTuneStruct::GetBreakingFrameRateLimit() const
{
	return BreakingFrameRateLimit;
}

inline bool fragTuneStruct::GetEnableCompleteImpulsePropagation() const
{
	return EnableCompleteImpulsePropagation;
}

inline bool fragTuneStruct::GetAggressiveAssetTouching() const
{
	return AggressiveAssetTouching;	
}

inline bool fragTuneStruct::GetFragDebug1() const
{
	return FragDebug1;
}

inline bool fragTuneStruct::GetFragDebug2() const
{
	return FragDebug2;
}

inline bool fragTuneStruct::GetAllowArticulation() const
{
	return AllowArticulation;
}

inline bool fragTuneStruct::GetAllowSimplifiedColliders() const
{
	return AllowSimplifiedColliders;
}

inline float fragTuneStruct::GetCollisionDamageScale() const
{
	return CollisionDamageScale;	
}

inline float fragTuneStruct::GetCollisionDamageMinThreshold() const
{
	return CollisionDamageMinThreshold;	
}

inline float fragTuneStruct::GetCollisionDamageMaxThreshold() const
{
	return CollisionDamageMaxThreshold;	
}

inline float fragTuneStruct::GetLodDistanceBias() const
{
	return LodDistanceBias;	
}

inline void fragTuneStruct::SetForcedTuningPath(const char* forcedPath)
{
	ForcedTuningPath = forcedPath;
}

inline const char* fragTuneStruct::GetForcedTuningPath()
{
	return ForcedTuningPath;
}

inline bkBank* fragTuneStruct::GetBank()
{
	return Bank; 
}

#if __BANK
inline fragType* fragTuneStruct::GetTypeBeingTuned()
{
	if(ComboSelection && ComboTypes[ComboSelection-1] == CurrentFragType)
	{
		return CurrentFragType;
	}

	return NULL;
}

inline fragType* fragTuneStruct::GetTypeHovered()
{
	return HoverFragType;
}

inline int fragTuneStruct::GetSelectedComponent()
{
    return ComponentSelection;
}

inline bool fragTuneStruct::IsDebugDrawSelectedFragEnabled() const
{ 
	return DebugDrawSelectedFrag; 
}

#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

class fragTuneBreakPreset : public datBase
{
public:
	fragTuneBreakPreset();

	ConstString m_Name;
	float		m_Strength;
	float		m_ForceTransmissionUp;
	float		m_ForceTransmissionDown;
	float		m_MinDamageForce;
	float		m_DamageHealth;
	float		m_WeaponHealth;
	float		m_WeaponScale;
	float		m_MeleeScale;
	float		m_VehicleScale;
	float		m_PedScale;
	float		m_RagdollScale;
	float		m_ExplosionScale;
	float		m_ObjectScale;

	PAR_PARSABLE;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // #ifndef FRAG_TUNE_H
