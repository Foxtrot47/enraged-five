// 
// fragment/typephysicslod.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "typephysicslod.h"

#include "animation.h"
#include "drawable.h"
#include "fragmentconfig.h"
#include "typechild.h"
#include "typegroup.h"

#include "pharticulated/articulatedbody.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "data/resourcehelpers.h"

FRAGMENT_OPTIMISATIONS()

namespace rage {

#if !__SPU

IMPLEMENT_PLACE(fragPhysicsLODGroup);

#if __DECLARESTRUCT

void fragPhysicsLODGroup::DeclareStruct(datTypeStruct &s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(fragPhysicsLODGroup, pgBase)
		SSTRUCT_FIELD(fragPhysicsLODGroup, m_HighLOD)
		SSTRUCT_FIELD(fragPhysicsLODGroup, m_MediumLOD)
		SSTRUCT_FIELD(fragPhysicsLODGroup, m_LowLOD)
		SSTRUCT_END(fragPhysicsLODGroup)
}
#endif // __DECLARESTRUCT

fragPhysicsLODGroup::fragPhysicsLODGroup (datResource &rsc) :
	m_HighLOD(rsc)
  , m_MediumLOD(rsc)
  , m_LowLOD(rsc)
{
}

fragPhysicsLODGroup::fragPhysicsLODGroup()
{
	// The existence of at least one LOD is required for fragTypes 
	m_HighLOD = rage_new fragPhysicsLOD;
	m_MediumLOD = NULL;
	m_LowLOD = NULL;
}

fragPhysicsLODGroup::~fragPhysicsLODGroup()
{
	if (m_HighLOD)
		delete m_HighLOD;
	if (m_MediumLOD)
		delete m_MediumLOD;
	if (m_LowLOD)
		delete m_LowLOD;
}

// deep copy constructor
fragPhysicsLODGroup::fragPhysicsLODGroup(fragPhysicsLODGroup *copyLODGroup) :
	m_HighLOD(NULL)
  , m_MediumLOD(NULL)
  , m_LowLOD(NULL)
{
	if (copyLODGroup->m_HighLOD)
		m_HighLOD = rage_new fragPhysicsLOD(copyLODGroup->m_HighLOD);
	if (copyLODGroup->m_MediumLOD)
		m_MediumLOD = rage_new fragPhysicsLOD(copyLODGroup->m_MediumLOD);
	if (copyLODGroup->m_LowLOD)
		m_LowLOD = rage_new fragPhysicsLOD(copyLODGroup->m_LowLOD);
}

IMPLEMENT_PLACE(fragPhysicsLOD);

#if __DECLARESTRUCT
void fragPhysicsLOD::DeclareStruct(datTypeStruct &s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(fragPhysicsLOD, pgBase)
		SSTRUCT_IGNORE(fragPhysicsLOD, m_Pad00)
		SSTRUCT_FIELD(fragPhysicsLOD, m_SmallestAngInertia)
		SSTRUCT_FIELD(fragPhysicsLOD, m_LargestAngInertia)
		SSTRUCT_FIELD(fragPhysicsLOD, m_MinMoveForce)
		SSTRUCT_FIELD(fragPhysicsLOD, m_BodyType)
#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
		SSTRUCT_DYNAMIC_ARRAY(fragPhysicsLOD, m_MinBreakingImpulses, m_NumChildren)
#else // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
		SSTRUCT_IGNORE(fragPhysicsLOD, m_Pad2)
#endif // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
		SSTRUCT_FIELD(fragPhysicsLOD, m_RootCGOffset)
		SSTRUCT_FIELD(fragPhysicsLOD, m_OriginalRootCGOffset)
		SSTRUCT_FIELD(fragPhysicsLOD, m_UnbrokenCGOffset)
		SSTRUCT_CONTAINED_ARRAY_COUNT(fragPhysicsLOD, m_DampingConstant, phArchetypeDamp::NUM_DAMP_TYPES)
		SSTRUCT_DYNAMIC_ARRAY(fragPhysicsLOD, m_GroupNames, m_NumGroups)
		SSTRUCT_DYNAMIC_ARRAY(fragPhysicsLOD, m_Groups, m_NumGroups)
		SSTRUCT_DYNAMIC_ARRAY(fragPhysicsLOD, m_Children, m_NumChildren)
		SSTRUCT_FIELD(fragPhysicsLOD, m_PhysDampUndamaged)
		SSTRUCT_FIELD(fragPhysicsLOD, m_PhysDampDamaged)
		SSTRUCT_FIELD(fragPhysicsLOD, m_CompositeBounds)
#if !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
		SSTRUCT_DYNAMIC_ARRAY(fragPhysicsLOD, m_UndamagedAngInertia, m_NumChildren)
		SSTRUCT_DYNAMIC_ARRAY(fragPhysicsLOD, m_DamagedAngInertia, m_NumChildren)
#endif // !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
		SSTRUCT_FIELD(fragPhysicsLOD, m_LinkAttachments)
		SSTRUCT_DYNAMIC_ARRAY(fragPhysicsLOD, m_SelfCollisionA, m_MaxNumSelfCollisions)
		SSTRUCT_DYNAMIC_ARRAY(fragPhysicsLOD, m_SelfCollisionB, m_MaxNumSelfCollisions)
		SSTRUCT_FIELD(fragPhysicsLOD, m_NumSelfCollisions)
		SSTRUCT_FIELD(fragPhysicsLOD, m_MaxNumSelfCollisions)
		SSTRUCT_FIELD(fragPhysicsLOD, m_NumGroups)
		SSTRUCT_FIELD(fragPhysicsLOD, m_RootGroupCount)
		SSTRUCT_FIELD(fragPhysicsLOD, m_NumRootDamageRegions)
		SSTRUCT_FIELD(fragPhysicsLOD, m_NumBonyChildren)
		SSTRUCT_FIELD(fragPhysicsLOD, m_NumChildren)
		SSTRUCT_IGNORE(fragPhysicsLOD, m_Pad1)
		SSTRUCT_END(fragPhysicsLOD)
}
#endif // __DECLARESTRUCT

fragPhysicsLOD::fragPhysicsLOD (datResource &rsc) :
	  m_PhysDampUndamaged(rsc)
	, m_PhysDampDamaged(rsc)
	, m_BodyType(rsc)
{
	if (!rsc.IsDefragmentation())
	{
		if (m_NumRootDamageRegions < 1)
		{
			m_NumRootDamageRegions = 1;
		}
	}

	ObjectFixup(rsc, m_Groups, m_NumGroups);

	rsc.PointerFixup(m_GroupNames);
	for (int group = 0; group < m_NumGroups; ++group)
	{
		rsc.PointerFixup(m_GroupNames[group]);
	}
	m_GroupNames[m_NumGroups] = s_RemoveSelfCollisionString;

#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	rsc.PointerFixup(m_MinBreakingImpulses);
#endif // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	rsc.PointerFixup(m_SelfCollisionA);
	rsc.PointerFixup(m_SelfCollisionB);
}

fragPhysicsLOD::fragPhysicsLOD()
	: m_SmallestAngInertia(0.0f)
	, m_LargestAngInertia(0.0f)
	, m_GroupNames(NULL)
	, m_Groups(NULL)
	, m_Children(NULL)
	, m_PhysDampUndamaged(NULL)
	, m_PhysDampDamaged(NULL)
	, m_CompositeBounds(NULL)
#if !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	, m_UndamagedAngInertia(NULL)
	, m_DamagedAngInertia(NULL)
#endif // !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	, m_LinkAttachments(NULL)
#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	, m_MinBreakingImpulses(NULL)
#endif // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	, m_SelfCollisionA(NULL)
	, m_SelfCollisionB(NULL)
	, m_NumSelfCollisions(0)
	, m_MaxNumSelfCollisions(0)
	, m_NumGroups(0)
	, m_NumChildren(0)
	, m_RootGroupCount(0)
	, m_NumRootDamageRegions(1)
	, m_NumBonyChildren(0)
	, m_MinMoveForce(0.0f)
	, m_BodyType(NULL)
{
	m_RootCGOffset.Set(ORIGIN);
	m_OriginalRootCGOffset.Set(ORIGIN);
	m_UnbrokenCGOffset.Set(ORIGIN);

	m_DampingConstant[phArchetypeDamp::LINEAR_C].Set  (0.02f, 0.02f, 0.02f);
	m_DampingConstant[phArchetypeDamp::LINEAR_V].Set  (0.02f, 0.02f, 0.02f);
	m_DampingConstant[phArchetypeDamp::LINEAR_V2].Set (0.01f, 0.01f, 0.01f);
	m_DampingConstant[phArchetypeDamp::ANGULAR_C].Set (0.02f, 0.02f, 0.02f);
	m_DampingConstant[phArchetypeDamp::ANGULAR_V].Set (0.02f, 0.02f, 0.02f);
	m_DampingConstant[phArchetypeDamp::ANGULAR_V2].Set(0.01f, 0.01f, 0.01f);

	Assert(m_PhysDampUndamaged == NULL);
	Assert(m_PhysDampDamaged == NULL);
}

// deep copy constructor
fragPhysicsLOD::fragPhysicsLOD(fragPhysicsLOD *copyLOD)
{
	Assert(copyLOD);

	// basic types
	m_SmallestAngInertia = copyLOD->m_SmallestAngInertia;
	m_LargestAngInertia = copyLOD->m_LargestAngInertia;
	m_RootCGOffset = copyLOD->m_RootCGOffset;
	m_OriginalRootCGOffset = copyLOD->m_OriginalRootCGOffset;
	m_UnbrokenCGOffset = copyLOD->m_UnbrokenCGOffset;
	m_NumSelfCollisions = copyLOD->m_NumSelfCollisions;
	m_MaxNumSelfCollisions = copyLOD->m_MaxNumSelfCollisions;
	m_NumGroups = copyLOD->m_NumGroups;
	m_NumChildren = copyLOD->m_NumChildren;
	m_RootGroupCount = copyLOD->m_RootGroupCount; 
	m_NumRootDamageRegions = copyLOD->m_NumRootDamageRegions;
	m_NumBonyChildren = copyLOD->m_NumBonyChildren;
	m_MinMoveForce = copyLOD->m_MinMoveForce;
	for (int i=0; i<phArchetypeDamp::NUM_DAMP_TYPES; i++)
		m_DampingConstant[i] = copyLOD->m_DampingConstant[i];

	m_SelfCollisionA = rage_new u8[m_NumSelfCollisions];
	m_SelfCollisionB = rage_new u8[m_NumSelfCollisions];
	for (int i=0; i<m_NumSelfCollisions; i++)
	{
		m_SelfCollisionA[i] = copyLOD->m_SelfCollisionA[i];
		m_SelfCollisionB[i] = copyLOD->m_SelfCollisionB[i];
	}
	m_GroupNames = rage_new const char*[m_NumGroups + 1];
	for (int i=0; i<m_NumGroups; i++)
		m_GroupNames[i] = copyLOD->m_Groups[i]->GetDebugName();
	m_GroupNames[m_NumGroups] = "";

	// bounds
	m_CompositeBounds = rage_new phBoundComposite;
	m_CompositeBounds->Init(copyLOD->m_CompositeBounds->GetNumBounds(), true);
	m_CompositeBounds->Copy(copyLOD->m_CompositeBounds);

	// archetypes
	m_PhysDampUndamaged = m_PhysDampDamaged = NULL;
	if (copyLOD->m_PhysDampUndamaged)
	{
		m_PhysDampUndamaged = rage_new phArchetypeDamp;
		m_PhysDampUndamaged->SetBound(m_CompositeBounds);
		m_PhysDampUndamaged->CopyData(copyLOD->m_PhysDampUndamaged);
	}
	if (copyLOD->m_PhysDampDamaged)
	{
		m_PhysDampDamaged = rage_new phArchetypeDamp;
		m_PhysDampDamaged->SetBound(m_CompositeBounds);
		m_PhysDampDamaged->CopyData(copyLOD->m_PhysDampDamaged);
	}

	// groups
	m_Groups = rage_new datOwner<fragTypeGroup>[m_NumGroups];
	for (int i=0; i<m_NumGroups; i++)
	{
		m_Groups[i] = rage_new fragTypeGroup;
		*(m_Groups[i].ptr) = *(copyLOD->m_Groups[i].ptr);
	}

	// children
	m_Children = rage_new datOwner<fragTypeChild>[m_NumChildren];
	for (int i=0; i<m_NumChildren; i++)
	{
		m_Children[i] = rage_new fragTypeChild;
		*(m_Children[i].ptr) = *(copyLOD->m_Children[i].ptr);

		// fragDrawables
		if (copyLOD->m_Children[i]->GetUndamagedEntity())
		{
			fragDrawable *fd = rage_new fragDrawable;
			*fd = *(copyLOD->m_Children[i]->GetUndamagedEntity());
			m_Children[i]->SetUndamagedEntity(fd);
		}
		if (copyLOD->m_Children[i]->GetDamagedEntity())
		{
			fragDrawable *fd = rage_new fragDrawable;
			*fd = *copyLOD->m_Children[i]->GetDamagedEntity();
			m_Children[i]->SetDamagedEntity(fd);
		}
	}

	// simple(er) arrays
#if !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	m_UndamagedAngInertia = rage_new Vector3[m_NumChildren];
	m_DamagedAngInertia = rage_new Vector3[m_NumChildren];
#endif // !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD

#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	m_MinBreakingImpulses = rage_new float[m_NumChildren];
#endif // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES

	m_LinkAttachments = pgArray<Matrix34>::Create(m_NumChildren);
	for (int i=0; i<m_NumChildren; i++)
	{
		SetUndamagedAngInertia(i,copyLOD->GetUndamagedAngInertia(i));
		SetDamagedAngInertia(i,copyLOD->GetDamagedAngInertia(i));
#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
		m_MinBreakingImpulses[i] = copyLOD->m_MinBreakingImpulses[i];
#endif // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
		m_LinkAttachments->GetElement(i).Set(copyLOD->m_LinkAttachments->GetElement(i));
	}
	// articulated body type - TODO !!
	//m_BodyType = NULL;
	//if(copyLOD->m_BodyType)
	//	m_BodyType = rage_new phArticulatedBodyType(copyLOD->m_BodyType);
}


fragPhysicsLOD::~fragPhysicsLOD()
{
	if (m_BodyType)
		delete m_BodyType;

	delete [] m_SelfCollisionB;
	delete [] m_SelfCollisionA;

#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	delete [] m_MinBreakingImpulses;
#endif // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES

	for (int child = 0; child < m_NumChildren; ++child)
	{
		delete m_Children[child];
	}
	delete [] m_Children;

	delete [] m_GroupNames;
	if (m_PhysDampDamaged && phConfig::IsRefCountingEnabled())
	{
		// Damaged bounds have two references and don't get deleted unless we release one of them here
		m_PhysDampDamaged->GetBound()->Release();

		if (m_PhysDampDamaged->Release())
		{
			AssertMsg(false,"fragPhysicsLOD::m_PhysDampDamaged didn't release properly.");
			delete m_PhysDampDamaged;
		}
	}
	else
	{
		delete m_PhysDampDamaged;
	}

	if (m_PhysDampUndamaged && phConfig::IsRefCountingEnabled())
	{
		if (m_PhysDampUndamaged->Release())
		{
			AssertMsg(false,"fragPhysicsLOD::m_PhysDampUndamaged didn't release properly.");
			delete m_PhysDampUndamaged;
		}

	}
	else
	{
		delete m_PhysDampUndamaged;
	}
#if !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	delete [] m_UndamagedAngInertia;
	delete [] m_DamagedAngInertia;
#endif // !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	delete m_LinkAttachments;
	if (m_CompositeBounds && phConfig::IsRefCountingEnabled())
	{
		if (m_CompositeBounds->Release())
		{
			AssertMsg(false,"fragPhysicsLOD::m_PhysCompositeBounds didn't release properly");
			delete m_CompositeBounds;
		}
	}
	else
	{
		delete m_CompositeBounds;
	}
	for (int group = 0; group < m_NumGroups; ++group)
	{
		delete m_Groups[group];
	}
	delete [] m_Groups;
}


void fragPhysicsLODGroup::CreateLOD(int index)
{
	switch(index)
	{
	case 0:
		m_HighLOD = rage_new fragPhysicsLOD;
		break;
	case 1:
		m_MediumLOD = rage_new fragPhysicsLOD;
		break;
	case 2:
		m_LowLOD = rage_new fragPhysicsLOD;
		break;
	default:
		Assertf(0, "fragPhysicsLODGroup::CreateLOD - unhandled LOD index requested - %d", index);
	}
}

#endif  // !__SPU


const fragPhysicsLOD * fragPhysicsLODGroup::GetLODByIndex(int index) const
{
	switch(index)
	{
	case 0:
		return m_HighLOD;
	case 1:
		return m_MediumLOD;
	case 2:
		return m_LowLOD;
	default:
		//Assertf(0, "fragPhysicsLODGroup::GetLODByIndex - unhandled LOD index requested - %d", index);
		break;
	}
	return NULL;
}

fragPhysicsLOD * fragPhysicsLODGroup::GetLODByIndex(int index)
{
	switch(index)
	{
	case 0:
		return m_HighLOD;
	case 1:
		return m_MediumLOD;
	case 2:
		return m_LowLOD;
	default:
		//Assertf(0, "fragPhysicsLODGroup::GetLODByIndex - unhandled LOD index requested - %d", index);
		break;
	}
	return NULL;
}

void fragPhysicsLODGroup::SetLOD(int index, fragPhysicsLOD *lod)
{
	switch(index)
	{
	case 0:
		m_HighLOD = lod;
		break;
	case 1:
		m_MediumLOD = lod;
		break;
	case 2:
		m_LowLOD = lod;
		break;
	default:
		Assertf(0, "fragPhysicsLODGroup::SetLOD - unhandled LOD index - %d", index);
	}
}


void fragPhysicsLOD::ComputeUndamagedChildMassProperties(ScalarV_Ptr childMasses, Vec3V_Ptr childAngularInertias, Vec3V_Ptr childCGs) const
{
	ComputeUndamagedChildMassProperties(0,GetNumChildren(),childMasses,childAngularInertias,childCGs);
}
void fragPhysicsLOD::ComputeDamagedChildMassProperties(ScalarV_Ptr childMasses, Vec3V_Ptr childAngularInertias, Vec3V_Ptr childCGs) const
{
	ComputeDamagedChildMassProperties(0,GetNumChildren(),childMasses,childAngularInertias,childCGs);
}
void fragPhysicsLOD::ComputeUndamagedChildMassProperties(u8 firstChildIndex, u8 endChildIndex, ScalarV_Ptr childMasses, Vec3V_Ptr childAngularInertias, Vec3V_Ptr childCGs) const
{
	for(u8 childIndex = firstChildIndex; childIndex < endChildIndex; ++childIndex)
	{
		u8 userArrayIndex = childIndex-firstChildIndex;
		ComputeUndamagedChildMassProperties(childIndex,childCGs[userArrayIndex],childAngularInertias[userArrayIndex],childMasses[userArrayIndex]);
	}
}
void fragPhysicsLOD::ComputeDamagedChildMassProperties(u8 firstChildIndex, u8 endChildIndex, ScalarV_Ptr childMasses, Vec3V_Ptr childAngularInertias, Vec3V_Ptr childCGs) const
{
	for(u8 childIndex = firstChildIndex; childIndex < endChildIndex; ++childIndex)
	{
		u8 userArrayIndex = childIndex-firstChildIndex;
		ComputeDamagedChildMassProperties(childIndex,childCGs[userArrayIndex],childAngularInertias[userArrayIndex],childMasses[userArrayIndex]);
	}
}

void fragPhysicsLOD::SetUndamagedChildMassProperties(ScalarV_ConstPtr childMassses, Vec3V_ConstPtr childAngularInertias)
{
	SetUndamagedChildMassProperties(0,GetNumChildren(),childMassses,childAngularInertias);
}

Vec3V_Out AlignAngularInertiaMatrixVector(Vec3V_In v)
{
	return Vec3V(IsGreaterThan(Abs(v),Vec3V(V_HALF))) & Vec3V(V_ONE);
}
void AlignAngularInertiaMatrix(Mat33V_In rotationMatrix, Mat33V_InOut alignedRotationMatrix)
{
	alignedRotationMatrix.SetCol0(AlignAngularInertiaMatrixVector(rotationMatrix.GetCol0()));
	alignedRotationMatrix.SetCol1(AlignAngularInertiaMatrixVector(rotationMatrix.GetCol1()));
	alignedRotationMatrix.SetCol2(AlignAngularInertiaMatrixVector(rotationMatrix.GetCol2()));
}

void fragPhysicsLOD::SetUndamagedChildMassProperties(u8 firstChildIndex, u8 endChildIndex, ScalarV_ConstPtr childMassses, Vec3V_ConstPtr childAngularInertias)
{
	const phBoundComposite* compositeBounds = GetCompositeBounds();
	for(u8 childIndex = firstChildIndex; childIndex < endChildIndex; ++childIndex)
	{
		u8 userArrayIndex = childIndex-firstChildIndex;
		fragTypeChild& child = *GetChild(childIndex);
		Assert(IsGreaterThanOrEqualAll(childMassses[userArrayIndex],ScalarV(V_ZERO)));
		Assert(IsGreaterThanOrEqualAll(childAngularInertias[userArrayIndex],Vec3V(V_ZERO)));
		child.SetUndamagedMass(childMassses[userArrayIndex].Getf());

		const Mat33V instanceFromChild = compositeBounds->GetCurrentMatrix(childIndex).GetMat33ConstRef();
		const Vec3V currentAngularInertia = VECTOR3_TO_VEC3V(GetUndamagedAngInertia(childIndex));
		const Vec3V currentRotatedAngularInertia = phMathInertia::RotateAngularInertia(instanceFromChild,currentAngularInertia);
		const Vec3V instanceSpaceScale = InvScaleSafe(childAngularInertias[userArrayIndex],currentRotatedAngularInertia,Vec3V(V_ZERO));
		if(IsChildAngularInertiaScalable(childIndex).Getb())
		{
			// We have the instance-space scale, before we apply it to the child-space angular inertia we need an exactly 90 degree rotation matrix
			Mat33V alignedInstanceFromChild;
			AlignAngularInertiaMatrix(instanceFromChild,alignedInstanceFromChild);
			Vec3V childSpaceScale = UnTransformOrtho(alignedInstanceFromChild,instanceSpaceScale);
			SetUndamagedAngInertia(childIndex,VEC3V_TO_VECTOR3(Scale(currentAngularInertia,childSpaceScale)));
		}
		else
		{
			// This child isn't allowed to have non-uniform angular inertia scale in other spaces. Make sure that the angular inertia
			//   is only scaling uniformly. 
			if(Verifyf(IsCloseAll(instanceSpaceScale,Vec3V(instanceSpaceScale.GetX()),Vec3V(V_FLT_SMALL_3)),"Trying to modify the angular inertia of a child with non-90-degree rotations. Child: %i, Group: ('%s', %i), Fragment: '%s'",childIndex,GetGroup(child.GetOwnerGroupPointerIndex())->GetDebugName(),child.GetOwnerGroupPointerIndex(),GetArchetype()->GetFilename()))
			{
				SetUndamagedAngInertia(childIndex,VEC3V_TO_VECTOR3(Scale(currentAngularInertia,instanceSpaceScale.GetX())));
			}
		}

#if __ASSERT
		const Vec3V expectedAngularInertia = childAngularInertias[userArrayIndex];
		const Vec3V newAngularInertia = phMathInertia::RotateAngularInertia(instanceFromChild,VECTOR3_TO_VEC3V(GetUndamagedAngInertia(childIndex)));
		const Vec3V tolerance = Max(Scale(Vec3V(V_FLT_SMALL_1), ScalarV( 3.0f )),Scale(expectedAngularInertia,Vec3V(V_FLT_SMALL_3)));
		Assertf(IsCloseAll(expectedAngularInertia,newAngularInertia,tolerance), "fragPhysicsLOD::SetUndamagedChildMassProperties wasn't able to set the expected angular inertia. Child: %i, Group: ('%s', %i), Fragment: '%s'",childIndex,GetGroup(child.GetOwnerGroupPointerIndex())->GetDebugName(),child.GetOwnerGroupPointerIndex(),GetArchetype()->GetFilename());
#endif // __ASSERT
	}
}

BoolV_Out fragPhysicsLOD::SetUndamagedGroupMassAndAngularInertia(u8 groupIndex, ScalarV_In newGroupMass, Vec3V_In newGroupAngularInertia)
{
	// Find out what children we're modifying
	const fragTypeGroup& group = *GetGroup(groupIndex);
	u8 firstChildIndex = group.GetChildFragmentIndex();
	u8 numChildrenInGroup = group.GetNumChildren();
	u8 endChildIndex = firstChildIndex + numChildrenInGroup;

	fragChildBits angularInertiaScalableChildren;
	for(u8 childIndex = firstChildIndex; childIndex < endChildIndex; ++childIndex)
	{
		if(IsChildAngularInertiaScalable(childIndex).Getb())
		{
			// Bitset starts at the first child in the group
			angularInertiaScalableChildren.Set(childIndex-firstChildIndex);
		}
#if __DEV
		else
		{
			// Let the user know if one of the children in this group has unscalable angular inertia. 
			fragWarningf("fragPhysicsLOD::SetUndamagedGroupMassAndAngularInertia - Unable to scale the angular inertia of child %i on group ('%s', %i) of fragment '%s'",childIndex,group.GetDebugName(),groupIndex,GetArchetype()->GetFilename());
		}
#endif // __DEV
	}

	// Get the mass properties
	ScalarV_Ptr childMasses = Alloca(ScalarV,numChildrenInGroup);
	Vec3V_Ptr childAngularInertias = Alloca(Vec3V,numChildrenInGroup);
	Vec3V_Ptr childCGs = Alloca(Vec3V, numChildrenInGroup);
	ComputeUndamagedChildMassProperties(firstChildIndex,endChildIndex,childMasses,childAngularInertias,childCGs);

	// Scale the masses to the target sum, and scale the angular inertias uniformly alongside it
	phMathInertia::MassAccumulatorSimple groupMassAccumulator;
	phMathInertia::ComputeCombinedMassAndCenterOfGravity(numChildrenInGroup,childMasses,childCGs,groupMassAccumulator);
	phMathInertia::ScaleMassesAndAngularInertiasToTargetSum(numChildrenInGroup,childMasses,childAngularInertias,newGroupMass,phMathInertia::MassAccumulatorSimple());

	// Compute how much angular inertia we aren't allowed to scale in this group
	phMathInertia::MassAccumulatorNotBitSet<fragChildBits> unscalableMassAccumulator(angularInertiaScalableChildren);
	unscalableMassAccumulator.SetCenterOfGravity(groupMassAccumulator.GetCenterOfGravity());
	phMathInertia::ComputeCombinedAngularInertia(numChildrenInGroup,childMasses,childAngularInertias,childCGs,unscalableMassAccumulator);
	const Vec3V newScalableAngularInertia = Subtract(newGroupAngularInertia,unscalableMassAccumulator.GetAngularInertia());

	// Scale the angular inertias of scalable children to the target scalable angular inertia, so that their scalable angular inertia plus the unscalable angular inertia is equal to the user's expected angular inertia
	BoolV wasAngularInertiaScaled = phMathInertia::ScaleAngularInertiasToTargetSum(numChildrenInGroup,childMasses,childAngularInertias,childCGs,groupMassAccumulator.GetCenterOfGravity(),newScalableAngularInertia,phMathInertia::MassAccumulatorBitSet<fragChildBits>(angularInertiaScalableChildren));

	// Update the mass properties on the fragment
	SetUndamagedChildMassProperties(firstChildIndex,endChildIndex,childMasses,childAngularInertias);
	GetGroup(groupIndex)->ComputeAndSetTotalUndamagedMass(this);

	return wasAngularInertiaScaled;
}

BoolV_Out fragPhysicsLOD::SetUndamagedGroupTreeMassAndAngularInertia(u8 groupIndex, ScalarV_In newGroupTreeMass, Vec3V_In newGroupTreeAngularInertia)
{
	// Find out what children we're modifying
	fragGroupBits groupsInSubtree;
	GetGroupsAbove(groupIndex,groupsInSubtree);
	fragChildBits childrenInSubtree;
	ConvertGroupBitsetToChildBitset(groupsInSubtree,childrenInSubtree);

	// Not all groups have scalable angular inertia, only scale those without
	fragChildBits angularInertiaScalableChildren;
	GetChildrenWithScalableAngularInertia(angularInertiaScalableChildren);
	fragChildBits angularInertiaScalableChildrenInSubtree(childrenInSubtree);
	angularInertiaScalableChildrenInSubtree.Intersect(angularInertiaScalableChildren);

	// Allocate temporary arrays to store the mass
	u8 numChildren = GetNumChildren();
	ScalarV_Ptr childMasses = Alloca(ScalarV,numChildren);
	Vec3V_Ptr childAngularInertias = Alloca(Vec3V,numChildren);
	Vec3V_Ptr childCGs = Alloca(Vec3V, numChildren);
	ComputeUndamagedChildMassProperties(childMasses,childAngularInertias,childCGs);

#if __DEV
	// Let the user know if one of the children in this tree has unscalable angular inertia. 
	for(u8 childIndex = 0; childIndex < numChildren; ++childIndex)
	{
		if(angularInertiaScalableChildrenInSubtree.IsClear(childIndex) && childrenInSubtree.IsSet(childIndex))
		{
			u8 groupIndex = GetChild(childIndex)->GetOwnerGroupPointerIndex();
			fragWarningf("fragPhysicsLOD::SetUndamagedGroupTreeMassAndAngularInertia - Unable to scale the angular inertia of child %i on group ('%s', %i) of fragment '%s'",childIndex,GetGroup(groupIndex)->GetDebugName(),groupIndex,GetArchetype()->GetFilename());
		}
	}
#endif // __DEV

	// Scale the masses to the target sum, and scale the angular inertias uniformly alongside it
	phMathInertia::MassAccumulatorBitSet<fragChildBits> subtreeMassAccumulator(childrenInSubtree);
	phMathInertia::ComputeCombinedMassAndCenterOfGravity(numChildren,childMasses,childCGs,subtreeMassAccumulator);
	phMathInertia::ScaleMassesAndAngularInertiasToTargetSum(numChildren,childMasses,childAngularInertias,newGroupTreeMass,subtreeMassAccumulator);

	// Compute how much angular inertia we aren't allowed to scale in this group subtree
	fragChildBits angularInertiaUnscalableChildrenInSubtree(childrenInSubtree);
	angularInertiaUnscalableChildrenInSubtree.IntersectNegate(angularInertiaScalableChildren);
	phMathInertia::MassAccumulatorBitSet<fragChildBits> unscalableMassAccumulator(angularInertiaUnscalableChildrenInSubtree);
	unscalableMassAccumulator.SetCenterOfGravity(subtreeMassAccumulator.GetCenterOfGravity());
	phMathInertia::ComputeCombinedAngularInertia(numChildren,childMasses,childAngularInertias,childCGs,unscalableMassAccumulator);
	const Vec3V newScalableAngularInertia = Subtract(newGroupTreeAngularInertia,unscalableMassAccumulator.GetAngularInertia());

	// Scale the angular inertias of scalable children to the target scalable angular inertia, so that their scalalbe angular inertia plus the unscalable angular inertia is equal to the user's expected angular inertia
	BoolV wasAngularInertiaScaled = phMathInertia::ScaleAngularInertiasToTargetSum(numChildren,childMasses,childAngularInertias,childCGs,subtreeMassAccumulator.GetCenterOfGravity(),newScalableAngularInertia,phMathInertia::MassAccumulatorBitSet<fragChildBits>(angularInertiaScalableChildrenInSubtree));

	// Update the mass properties on the fragment
	SetUndamagedChildMassProperties(childMasses,childAngularInertias);
	u8 numGroups = GetNumChildGroups();
	for(u8 subtreeGroupIndex = groupIndex; subtreeGroupIndex < numGroups; ++subtreeGroupIndex)
	{
		if(groupsInSubtree.IsSet(subtreeGroupIndex))
		{
			GetGroup(subtreeGroupIndex)->ComputeAndSetTotalUndamagedMass(this);
		}
	}
	return wasAngularInertiaScaled;
}

void fragPhysicsLOD::SetNumSelfCollisions(u8 numSelf)
{
	if (numSelf > m_MaxNumSelfCollisions)
	{
		// NOTE: memory leak here, not sure what to do because we don't know if we
		// allocated it ourselves or it is from a resource heap
		u8* newSelfA = rage_new u8[numSelf];
		u8* newSelfB = rage_new u8[numSelf];

		memcpy(newSelfA, m_SelfCollisionA, sizeof(u8) * GetNumSelfCollisions());
		memcpy(newSelfB, m_SelfCollisionB, sizeof(u8) * GetNumSelfCollisions());

		m_SelfCollisionA = newSelfA;
		m_SelfCollisionB = newSelfB;

		m_MaxNumSelfCollisions = numSelf;
	}

	m_NumSelfCollisions = numSelf;
}

void fragPhysicsLOD::SetSelfCollision(int i, u8 a, u8 b)
{
	Assert(i < GetNumSelfCollisions());
	Assertf(a < GetNumChildGroups() && b < GetNumChildGroups(), "Self collision being set <%d, %d> on type with only %d group(s).", a, b, GetNumChildGroups());

	m_SelfCollisionA[i] = a;
	m_SelfCollisionB[i] = b;
}

bool fragPhysicsLOD::AddSelfCollision(const char* a, const char* b)
{
	int indexA = -1, indexB = -1;

	for (int group = 0; group < GetNumChildGroups(); ++group)
	{
		if (stricmp(GetGroup(group)->GetDebugName(), a) == 0)
		{
			indexA = group;
		}
		else if (stricmp(GetGroup(group)->GetDebugName(), b) == 0)
		{
			indexB = group;
		}
	}

	if (Verifyf(indexA != -1 && indexB != -1, "Can't find self collision pair %s, %s (type %s), was one of those groups deleted?", a, b, GetArchetype() ? GetArchetype()->GetFilename() : NULL))
	{
		++m_NumSelfCollisions;
		SetSelfCollision(m_NumSelfCollisions - 1, (u8)indexA, (u8)indexB);
		return true;
	}
	else
	{
		return false;
	}
}

void fragPhysicsLOD::SetPhysicsArchetypeMassFromChildren()
{
	float mass = 0.0f;
	float damagedMass = 0.0f;
	fragTypeChild** child = GetAllChildren();
	for (u32 childIndex = 0; childIndex < GetNumChildren(); ++childIndex, ++child)
	{
		mass += (*child)->GetUndamagedMass();
		damagedMass += (*child)->GetDamagedMass();
	}
	GetArchetype()->SetMassOnly(mass);
	if (GetArchetype(true))
	{
		GetArchetype(true)->SetMassOnly(damagedMass);
	}
}


void fragPhysicsLOD::ComputeGroupTotalMasses()
{
	for (int group = 0; group < GetNumChildGroups(); ++group)
	{
		GetGroup(group)->ComputeAndSetTotalUndamagedMass(this);
	}
}

void fragPhysicsLOD::ComputeAngularInertiaLimits()
{
	u8 numChildren = GetNumChildren();
	Vec3V maxAngularInertia(V_ZERO);
	for (u32 childIndex = 0; childIndex < numChildren; ++childIndex)
	{
		maxAngularInertia = Max(maxAngularInertia,VECTOR3_TO_VEC3V(GetUndamagedAngInertia(childIndex)));
		maxAngularInertia = Max(maxAngularInertia,VECTOR3_TO_VEC3V(GetDamagedAngInertia(childIndex)));
	}
	ScalarV maxAngularInertiaElement = MaxElement(maxAngularInertia);

	m_SmallestAngInertia = maxAngularInertiaElement.Getf()*ANG_INERTIA_MAX_RATIO;
	SetLargestAngInertia(maxAngularInertiaElement.Getf());
}

void fragPhysicsLOD::ComputeChildAngularInertias()
{
	u8 numChildren = GetNumChildren();
	for(u8 childIndex = 0; childIndex < numChildren; ++childIndex)
	{
		fragTypeChild& child = *GetChild(childIndex);
		if(child.GetUndamagedEntity() && child.GetUndamagedEntity()->GetBound())
		{
			SetUndamagedAngInertia(childIndex,VEC3V_TO_VECTOR3(child.GetUndamagedEntity()->GetBound()->GetComputeAngularInertia(child.GetUndamagedMass())));
		}
		else
		{
			SetUndamagedAngInertia(childIndex,VEC3V_TO_VECTOR3(Vec3V(V_ZERO)));
		}

		if(child.GetDamagedEntity() && child.GetDamagedEntity()->GetBound())
		{
			SetDamagedAngInertia(childIndex,VEC3V_TO_VECTOR3(child.GetDamagedEntity()->GetBound()->GetComputeAngularInertia(child.GetDamagedMass())));
		}
		else
		{
			SetDamagedAngInertia(childIndex,VEC3V_TO_VECTOR3(Vec3V(V_ZERO)));
		}
	}
}

void fragPhysicsLOD::ClampAngularInertia(Vec3V_InOut angularInertia, ScalarV_InOut mass)
{
	ScalarV smallestAngularInertiaComponent = MinElement(angularInertia);
	ScalarV smallestAllowedAngularInertiaComponent = ScalarVFromF32(GetSmallestAngInertia());

	if(IsLessThanAll(smallestAngularInertiaComponent, smallestAllowedAngularInertiaComponent))
	{
		ScalarV largestAngularInertiaComponent = MaxElement(angularInertia);
		ScalarV largestAllowedAngularInertiaComponent = ScalarVFromF32(GetLargestAngInertia());

		ScalarV inertiaScale = InvScale(smallestAllowedAngularInertiaComponent, smallestAngularInertiaComponent);
		
		if(IsGreaterThanAll(Scale(inertiaScale,largestAngularInertiaComponent), largestAllowedAngularInertiaComponent))
		{
			inertiaScale = Scale(inertiaScale, InvScale(largestAngularInertiaComponent,largestAllowedAngularInertiaComponent));
		}

		if(IsGreaterThanAll(inertiaScale, ScalarV(V_ONE)))
		{
			angularInertia = Scale(angularInertia, inertiaScale);
			mass = Scale(mass, inertiaScale);
		}
	}
}

float fragPhysicsLOD::GetTotalUndamagedMass() const
{
	return GetArchetype()->GetMass();
}

Vector3 fragPhysicsLOD::GetTotalUndamagedAngInertia() const
{
	return GetArchetype()->GetAngInertia();
}


int fragPhysicsLOD::FindGroupIndexFromName(const char* groupName) const
{
	for(int groupIndex = 0; groupIndex < GetNumChildGroups(); ++groupIndex)
	{
		if(stricmp(GetGroup(groupIndex)->GetDebugName(), groupName) == 0)
		{
			return groupIndex;
		}
	}

	return -1;
}

void fragPhysicsLOD::ApplyDamping(phArchetypeDamp* archetype) const
{
	if (archetype)
	{
		for (int dampType = 0; dampType < phArchetypeDamp::NUM_DAMP_TYPES; ++dampType)
		{
			archetype->ActivateDamping(dampType, GetDampingConstant(dampType));
		}
	}
}

bool fragPhysicsLOD::IsAllGlass() const
{
	for (int groupIndex = 0; groupIndex < GetNumChildGroups(); ++groupIndex)
	{
		if (!GetGroup(groupIndex)->GetMadeOfGlass())
		{
			return false;
		}
	}

	return true;
}


void fragPhysicsLOD::ComputeDamagedChildMassProperties(u8 childIndex, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass) const
{
	ComputeDamagedChildMassProperties(childIndex,GetCompositeBounds()->GetCurrentMatrix(childIndex),centerOfGravity,angularInertia,mass);
}
void fragPhysicsLOD::ComputeUndamagedChildMassProperties(u8 childIndex, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass) const
{
	ComputeUndamagedChildMassProperties(childIndex,GetCompositeBounds()->GetCurrentMatrix(childIndex),centerOfGravity,angularInertia,mass);
}

void fragPhysicsLOD::ComputeDamagedChildMassProperties(u8 childIndex, Mat34V_In instanceFromChild, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass) const
{
	const fragTypeChild& child = *GetChild(childIndex);
	mass = ScalarVFromF32(child.GetDamagedMass());
	if(child.GetDamagedEntity() && child.GetDamagedEntity()->GetBound())
	{
		centerOfGravity = Transform(instanceFromChild,child.GetDamagedEntity()->GetBound()->GetCGOffset());
		angularInertia = phMathInertia::RotateAngularInertia(instanceFromChild.GetMat33ConstRef(), VECTOR3_TO_VEC3V(GetDamagedAngInertia(childIndex)));
	}
	else
	{
		centerOfGravity = Vec3V(V_ZERO);
		angularInertia = Vec3V(V_ZERO);
	}
}

void fragPhysicsLOD::ComputeUndamagedChildMassProperties(u8 childIndex, Mat34V_In instanceFromChild, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass) const
{
	const fragTypeChild& child = *GetChild(childIndex);
	mass = ScalarVFromF32(child.GetUndamagedMass());
	if(child.GetUndamagedEntity() && child.GetUndamagedEntity()->GetBound())
	{
		centerOfGravity = Transform(instanceFromChild,child.GetUndamagedEntity()->GetBound()->GetCGOffset());
		angularInertia = phMathInertia::RotateAngularInertia(instanceFromChild.GetMat33ConstRef(), VECTOR3_TO_VEC3V(GetUndamagedAngInertia(childIndex)));
	}
	else
	{
		centerOfGravity = Vec3V(V_ZERO);
		angularInertia = Vec3V(V_ZERO);
	}
}

} // namespace rage 
