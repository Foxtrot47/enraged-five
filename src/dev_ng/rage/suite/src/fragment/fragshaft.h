// 
// fragshaft.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FRAGSHAFT_H
#define FRAGSHAFT_H

#include "grcore/viewport.h"
#include "vector/vector3.h"
#include "spatialdata/aabb.h"
#include "spatialdata/plane.h"
#include "spatialdata/sphere.h"

namespace rage {

class spdSphere;

class fragViewport
{
public:
	fragViewport() :
		m_ZClipNear(0.0f),
		m_ZClipFar(0.0f),
		m_TanHFOV(0.0f),
		m_TanVFOV(0.0f),
		m_InvPixelWidth(1.0f),
		m_InvPixelHeight(1.0f)
	{}

	void Set(const grcViewport &viewport)
	{
		m_ZClipNear      = viewport.GetNearClip();
		m_ZClipFar       = viewport.GetFarClip();
		m_TanHFOV        = viewport.GetTanHFOV();
		m_TanVFOV        = viewport.GetTanVFOV();
		m_InvPixelWidth  = 1.0f / static_cast<float>(viewport.GetWindow().GetWidth());
		m_InvPixelHeight = 1.0f / static_cast<float>(viewport.GetWindow().GetHeight());
	}

	float m_ZClipNear;
	float m_ZClipFar;
	float m_TanHFOV;
	float m_TanVFOV;
	float m_InvPixelWidth;
	float m_InvPixelHeight;
};

class fragApical
{
public:
	fragApical() : m_X1(0.0f), m_X2(0.0f), m_Y1(0.0f), m_Y2(0.0f), m_Matrix(M34_IDENTITY) {}

	const Matrix34& GetMatrix() { return m_Matrix; }

protected:
	float        m_X1;
	float        m_Y1;
	float        m_X2;
	float        m_Y2;
	Matrix34     m_Matrix;
	fragViewport m_Viewport;
};

class fragShaft : public fragApical
{
public:
	enum
	{
		PLANE_NEAR,
		PLANE_FAR,
		PLANE_LEFT,
		PLANE_RIGHT,
		PLANE_TOP,
		PLANE_BOTTOM,
		NUMOF_PLANES, // = 6
		NUMOF_CORNERS = 8,
	};

	fragShaft();

	void Set(const grcViewport &viewport, const Matrix34 &mat);

	void SetX1(float x1);
	void SetX2(float x2);
	void SetY1(float y1);
	void SetY2(float y2);

	bool IsVisible(const spdSphere &sphere) const;

	void DrawDebug(bool solid = false, bool depthTest = false) const;

private:
	bool     m_IsOrtho;
	spdPlane m_LocalPlanes[NUMOF_PLANES];
	spdPlane m_WorldPlanes[NUMOF_PLANES];

#if __DEV
	void UpdateCornersOrtho();
	void UpdateCorners();

	Vector3 m_WorldCorners[NUMOF_CORNERS];
#endif // __DEV
};

} // namespace rage

#endif // FRAGSHAFT_H

