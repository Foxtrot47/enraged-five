// 
// fragshaft.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// (copied from spatialdata/shaft.cpp)
// 

#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"
#if __DEV
#include "grcore/im.h"
#include "grcore/stateblock.h"
#endif // __DEV
#include "fragment/fragshaft.h"

using namespace rage;

fragShaft::fragShaft() : m_IsOrtho(false)
{
	for (int i = 0; i < NUMOF_PLANES; i++)
	{
		m_LocalPlanes[i].Invalidate();
		m_WorldPlanes[i].Invalidate();
	}

#if __DEV
	for (int i = 0; i < NUMOF_CORNERS; i++)
	{
		m_WorldCorners[i].Zero();
	}
#endif // __DEV
}

void fragShaft::Set(const grcViewport &viewport, const Matrix34 &mat)
{
	m_IsOrtho = !viewport.IsPerspective();
	fragApical::m_Viewport.Set(viewport);
	m_Matrix = mat;

	for (int i = 0; i < NUMOF_PLANES; i++)
	{
		m_LocalPlanes[i].Transform(m_Matrix, m_WorldPlanes[i]);
	}

	const Vec3V _zAxis		= Vec3V(V_Z_AXIS_WZERO);
	const Vec3V _negZAxis	= Negate( Vec3V(V_Z_AXIS_WZERO) );

	m_LocalPlanes[PLANE_NEAR].SetPair( Vector3(0.0f, 0.0f, -viewport.GetNearClip()),VEC3V_TO_VECTOR3(_zAxis),	m_Matrix, m_WorldPlanes[PLANE_NEAR] );
	m_LocalPlanes[PLANE_FAR ].SetPair( Vector3(0.0f, 0.0f, -viewport.GetFarClip()) ,VEC3V_TO_VECTOR3(_negZAxis),m_Matrix, m_WorldPlanes[PLANE_FAR] );

	if (m_IsOrtho)
	{
		const Vec3V _xAxis		= Vec3V(V_X_AXIS_WZERO);
		const Vec3V _negXAxis	= Negate( Vec3V(V_X_AXIS_WZERO) );
		const Vec3V _yAxis		= Vec3V(V_Y_AXIS_WZERO);
		const Vec3V _negYAxis	= Negate( Vec3V(V_Y_AXIS_WZERO) );

		const Vec4V _lrtbV = viewport.GetLRTB();
		Vector3 _lrtb_L, _lrtb_R, _lrtb_T, _lrtb_B;
		const Vector3 _zero = VEC3V_TO_VECTOR3( Vec3V(V_ZERO) );
		_lrtb_L = _zero; 
		_lrtb_R = _zero; 
		_lrtb_T = _zero;
		_lrtb_B = _zero;
		RC_VEC3V(_lrtb_L).SetX( _lrtbV.GetX() );
		RC_VEC3V(_lrtb_R).SetX( _lrtbV.GetY() );
		RC_VEC3V(_lrtb_T).SetY( _lrtbV.GetZ() );
		RC_VEC3V(_lrtb_B).SetY( _lrtbV.GetW() );

		m_LocalPlanes[PLANE_LEFT  ].SetPair( _lrtb_L, VEC3V_TO_VECTOR3(_negXAxis)	,m_Matrix, m_WorldPlanes[PLANE_LEFT]	);
		m_LocalPlanes[PLANE_RIGHT ].SetPair( _lrtb_R, VEC3V_TO_VECTOR3(_xAxis)		,m_Matrix, m_WorldPlanes[PLANE_RIGHT]	);
		m_LocalPlanes[PLANE_TOP   ].SetPair( _lrtb_T, VEC3V_TO_VECTOR3(_yAxis)		,m_Matrix, m_WorldPlanes[PLANE_TOP]		);
		m_LocalPlanes[PLANE_BOTTOM].SetPair( _lrtb_B, VEC3V_TO_VECTOR3(_negYAxis)	,m_Matrix, m_WorldPlanes[PLANE_BOTTOM]	);

#if __DEV
		const Matrix34 &camMtx = m_Matrix;

		Vector3 left	= VEC3V_TO_VECTOR3( Scale( VECTOR3_TO_VEC3V(camMtx.a), SplatX( _lrtbV ) ) );
		Vector3 right	= VEC3V_TO_VECTOR3( Scale( VECTOR3_TO_VEC3V(camMtx.a), SplatY( _lrtbV ) ) );
		Vector3 top		= VEC3V_TO_VECTOR3( Scale( VECTOR3_TO_VEC3V(camMtx.b), SplatZ( _lrtbV ) ) );
		Vector3 bottom	= VEC3V_TO_VECTOR3( Scale( VECTOR3_TO_VEC3V(camMtx.b), SplatW( _lrtbV ) ) );
		Vector3 front = camMtx.c * -viewport.GetNearClip();
		Vector3 back = camMtx.c * -viewport.GetFarClip();

		m_WorldCorners[0] = camMtx.d + front + top    + left;
		m_WorldCorners[1] = camMtx.d + front + bottom + left;
		m_WorldCorners[2] = camMtx.d + front + top    + right;
		m_WorldCorners[3] = camMtx.d + front + bottom + right;
		m_WorldCorners[4] = camMtx.d + back  + top    + left;
		m_WorldCorners[5] = camMtx.d + back  + bottom + left;
		m_WorldCorners[6] = camMtx.d + back  + top    + right;
		m_WorldCorners[7] = camMtx.d + back  + bottom + right;
#endif // __DEV
	}
	else
	{
		float x = (float)viewport.GetWindow().GetX();
		float y = (float)viewport.GetWindow().GetY();
		float w = (float)viewport.GetWindow().GetWidth();
		float h = (float)viewport.GetWindow().GetHeight();

		SetX1(x);
		SetX2(x + w);
		SetY1(y);
		SetY2(y + h);
	}

#if __DEV
	UpdateCorners();
#endif // __DEV
}

void fragShaft::SetX1(float x1)
{
	m_X1 = x1;

	float nearF = m_Viewport.m_ZClipNear;
	float width = 2.0f * nearF * m_Viewport.m_TanHFOV;
	float fx1 = x1 * m_Viewport.m_InvPixelWidth;
	float localx1 = (fx1 * width) - (width * 0.5f);

	Vector3 p;
	Vector3 n;

	p.Set(localx1, 0.0f, -nearF);
	if (m_IsOrtho)
	{
		n = VEC3V_TO_VECTOR3( Vec3V(V_X_AXIS_WZERO) );
	}
	else
	{
		n.Cross(p, VEC3V_TO_VECTOR3( Negate(Vec3V(V_Y_AXIS_WZERO)) ) );
		n.Normalize();
	}

	spdPlane &local = m_LocalPlanes[PLANE_LEFT];
	spdPlane &world = m_WorldPlanes[PLANE_LEFT];

	local.SetPair( p, n, m_Matrix, world );
}

void fragShaft::SetX2(float x2)
{
	m_X2 = x2;

	float near_ = m_Viewport.m_ZClipNear;
	float width = 2.0f * near_ * m_Viewport.m_TanHFOV;
	float fx2 = x2 * m_Viewport.m_InvPixelWidth;
	float localx2 = (fx2 * width) - (width * 0.5f);

	Vector3 p(localx2, 0.0f, -near_);
	Vector3 n;
	if (m_IsOrtho)
	{
		n = VEC3V_TO_VECTOR3( Negate(Vec3V(V_X_AXIS_WZERO)) );
	}
	else
	{
		n.Cross(p, VEC3V_TO_VECTOR3(Vec3V(V_Y_AXIS_WZERO)) );
		n.Normalize();
	}

	spdPlane &local = m_LocalPlanes[PLANE_RIGHT];
	spdPlane &world = m_WorldPlanes[PLANE_RIGHT];

	local.SetPair(p,n,m_Matrix, world);
}

void fragShaft::SetY1(float y1)
{
	m_Y1 = y1;

	float near_ = m_Viewport.m_ZClipNear;
	float height = 2.0f * near_ * m_Viewport.m_TanVFOV;
	float fy1 = y1 * m_Viewport.m_InvPixelHeight;
	float localy1 = (fy1 * height) - (height * 0.5f);

	Vector3 p(0.0f, -localy1, -near_);
	Vector3 n;

	if (m_IsOrtho)
	{
		n = VEC3V_TO_VECTOR3( Negate(Vec3V(V_Y_AXIS_WZERO)) );
	}
	else
	{
		n.Cross(p, VEC3V_TO_VECTOR3( Negate(Vec3V(V_X_AXIS_WZERO)) ));
		n.Normalize();
	}

	spdPlane &local = m_LocalPlanes[PLANE_TOP];
	spdPlane &world = m_WorldPlanes[PLANE_TOP];

	local.SetPair(p, n, m_Matrix, world);
}

void fragShaft::SetY2(float y2)
{
	m_Y2 = y2;

	float near_ = m_Viewport.m_ZClipNear;
	float height = 2.0f * near_ * m_Viewport.m_TanVFOV;
	float fy2 = y2 * m_Viewport.m_InvPixelHeight;
	float localy2 = (fy2 * height) - (height * 0.5f);

	Vector3 p(0.0f, -localy2, -near_);
	Vector3 n;

	if (m_IsOrtho)
	{
		n = VEC3V_TO_VECTOR3( Vec3V(V_Y_AXIS_WZERO) );
	}
	else
	{
		n.Cross(p, VEC3V_TO_VECTOR3( Vec3V(V_X_AXIS_WZERO) ) );
		n.Normalize();
	}

	spdPlane &local = m_LocalPlanes[PLANE_BOTTOM];
	spdPlane &world = m_WorldPlanes[PLANE_BOTTOM];

	local.SetPair(p, n, m_Matrix, world);
}

bool fragShaft::IsVisible(const spdSphere &sphere) const
{
	const Vector3 center = VEC3V_TO_VECTOR3(sphere.GetCenter());

	for (int i = 0; i < NUMOF_PLANES; i++)
	{
		float d = m_WorldPlanes[i].DistanceToPlane(center);

		if (d > sphere.GetRadiusf())
		{
			return false;
		}
	}

	return true;
}

#if __DEV

void fragShaft::UpdateCornersOrtho()
{
	Matrix34 cameraMatrix(m_Matrix);

	Vector3 camPos(cameraMatrix.d);
	Vector3 camDir(-cameraMatrix.c); 
	Vector3 camA(cameraMatrix.a);
	Vector3 camB(cameraMatrix.b);

	camDir.Normalize();
	camA.Normalize();
	camB.Normalize();

	Vector2 camViewWidth;  
	Vector2 camViewHeight; 

	camViewWidth.Set(m_Viewport.m_TanHFOV * m_Viewport.m_ZClipFar, m_Viewport.m_TanHFOV * m_Viewport.m_ZClipFar);
	camViewHeight.Set(m_Viewport.m_TanVFOV * m_Viewport.m_ZClipFar, m_Viewport.m_TanVFOV * m_Viewport.m_ZClipFar);

	Vector3 nearCenter(camPos); nearCenter.AddScaled(camDir, m_Viewport.m_ZClipNear);
	Vector3 farCenter(camPos); farCenter.AddScaled(camDir, m_Viewport.m_ZClipFar);
	Vector3 leftNear(camA); leftNear.Scale(camViewWidth.x);
	Vector3 upNear(-camB); upNear.Scale(camViewHeight.x);
	Vector3 leftFar(camA); leftFar.Scale(camViewWidth.y);
	Vector3 upFar(-camB); upFar.Scale(camViewHeight.y);

	for(int i = 0; i < 4; i++)
	{
		m_WorldCorners[i].Set(nearCenter);
		m_WorldCorners[i + 4].Set(farCenter);
	}

	m_WorldCorners[0].Add( leftNear);  m_WorldCorners[0].Add( upNear);
	m_WorldCorners[1].Add(-leftNear);  m_WorldCorners[1].Add( upNear);
	m_WorldCorners[2].Add(-leftNear);  m_WorldCorners[2].Add(-upNear);
	m_WorldCorners[3].Add( leftNear);  m_WorldCorners[3].Add(-upNear);

	m_WorldCorners[4].Add( leftFar);  m_WorldCorners[4].Add( upFar);
	m_WorldCorners[5].Add(-leftFar);  m_WorldCorners[5].Add( upFar);
	m_WorldCorners[6].Add(-leftFar);  m_WorldCorners[6].Add(-upFar);
	m_WorldCorners[7].Add( leftFar);  m_WorldCorners[7].Add(-upFar);
}

void fragShaft::UpdateCorners(void)
{
	if (m_IsOrtho)
	{
		UpdateCornersOrtho();
	}
	else
	{
		float x1 = m_X1;
		float y1 = m_Y1;
		float x2 = m_X2;
		float y2 = m_Y2;

		float nearClip = m_Viewport.m_ZClipNear;
		float farClip = m_Viewport.m_ZClipFar;

		float width = 2.0f * nearClip * m_Viewport.m_TanHFOV;
		float height = 2.0f * nearClip * m_Viewport.m_TanVFOV;

		float fx1 = x1 * m_Viewport.m_InvPixelWidth;
		float fy1 = y1 * m_Viewport.m_InvPixelHeight;
		float fx2 = x2 * m_Viewport.m_InvPixelWidth;
		float fy2 = y2 * m_Viewport.m_InvPixelHeight;
		float localx1 = (fx1 * width) - (width * 0.5f);
		float localy1 = (fy1 * height) - (height * 0.5f);
		float localx2 = (fx2 * width) - (width * 0.5f);
		float localy2 = (fy2 * height) - (height * 0.5f);

		Vector3 localCorners[4] = {
			Vector3(localx1, -localy1, -nearClip),
			Vector3(localx1, -localy2, -nearClip),
			Vector3(localx2, -localy1, -nearClip),
			Vector3(localx2, -localy2, -nearClip),
		};

		const int cornerIndexes[NUMOF_CORNERS] = {0, 4, 3, 7, 1, 5, 2, 6};
		BoolV success;
		Vector3 pos = m_Matrix.d; // Ray origin.
		Vector3 corner; // Ray direction.

		for (int i = 0; i < 4; i++)
		{
			corner.Set(localCorners[i]);
			corner.Normalize();
			m_Matrix.Transform3x3(corner);

			int nearCorner = cornerIndexes[i * 2];
			int farCorner = cornerIndexes[(i * 2)+1];

			success = BoolV(V_TRUE);
			success &= m_WorldPlanes[PLANE_NEAR].IntersectRayV( VECTOR3_TO_VEC3V(pos), VECTOR3_TO_VEC3V(corner), RC_VEC3V(m_WorldCorners[ nearCorner ]) );
			success &= m_WorldPlanes[PLANE_FAR].IntersectRayV( VECTOR3_TO_VEC3V(pos), VECTOR3_TO_VEC3V(corner), RC_VEC3V(m_WorldCorners[ farCorner ]) );

			if ( !IsTrue( success ) )
			{
				m_WorldCorners[nearCorner] = pos + (corner * nearClip);
				m_WorldCorners[farCorner] = pos + (corner * farClip);
			}
		}
	}
}

void fragShaft::DrawDebug(bool DEV_ONLY(solid), bool DEV_ONLY(depthTest)) const
{
	grcBindTexture(NULL);
	// Note - DSS_TestOnly implies CMP_LESS; there is DSS_TestOnly_LessEqual if that's what you really need here.
	grcStateBlock::SetStates(grcStateBlock::RS_Default,depthTest?grcStateBlock::DSS_TestOnly:grcStateBlock::DSS_IgnoreDepth,grcStateBlock::BS_Default);

	u32 previousColor = grcCurrentColor;

	const Vector3 *corners = m_WorldCorners;

	if (solid)
	{
		Vector3 polygon[4];

		polygon[0] = corners[0];
		polygon[1] = corners[1];
		polygon[2] = corners[2];
		polygon[3] = corners[3];
		grcDrawPolygon(polygon, 4, NULL, true, Color32(0.8f, 0.0f, 0.0f, 0.8f));

		polygon[0] = corners[0];
		polygon[1] = corners[4];
		polygon[2] = corners[6];
		polygon[3] = corners[2];
		grcDrawPolygon(polygon, 4, NULL, true, Color32(0.8f, 0.8f, 0.0f, 0.8f));

		polygon[0] = corners[0];
		polygon[1] = corners[4];
		polygon[2] = corners[5];
		polygon[3] = corners[1];
		grcDrawPolygon(polygon, 4, NULL, true, Color32(0.8f, 0.0f, 0.0f, 0.8f));

		polygon[0] = corners[1];
		polygon[1] = corners[5];
		polygon[2] = corners[7];
		polygon[3] = corners[3];
		grcDrawPolygon(polygon, 4, NULL, true, Color32(0.8f, 0.0f, 0.8f, 0.8f));

		polygon[0] = corners[2];
		polygon[1] = corners[6];
		polygon[2] = corners[7];
		polygon[3] = corners[3];
		grcDrawPolygon(polygon, 4, NULL, true, Color32(0.0f, 0.8f, 0.8f, 0.8f));

		polygon[0] = corners[4];
		polygon[1] = corners[5];
		polygon[2] = corners[6];
		polygon[3] = corners[7];
		grcDrawPolygon(polygon, 4, NULL, true, Color32(0.0f, 0.0f, 0.8f, 0.8f));
	}

	Vector3 color;
	color.Set(1.0f, 1.0f, 1.0f);

	if (m_IsOrtho)
	{
		grcDrawLine(corners[0], corners[4], color); 
		grcDrawLine(corners[1], corners[5], color); 
		grcDrawLine(corners[2], corners[6], color); 
		grcDrawLine(corners[3], corners[7], color); 
	}
	else
	{
		grcDrawLine(m_Matrix.d, corners[4], color); 
		grcDrawLine(m_Matrix.d, corners[5], color); 
		grcDrawLine(m_Matrix.d, corners[6], color); 
		grcDrawLine(m_Matrix.d, corners[7], color); 
	}

	color.Set(0.0f, 0.5f, 1.0f);
	grcDrawLine(corners[0], corners[1], color);
	grcDrawLine(corners[1], corners[2], color);
	grcDrawLine(corners[2], corners[3], color);
	grcDrawLine(corners[3], corners[0], color);
	grcDrawLine(corners[4], corners[5], color);
	grcDrawLine(corners[5], corners[6], color);
	grcDrawLine(corners[6], corners[7], color);
	grcDrawLine(corners[7], corners[4], color);
	grcDrawLine(corners[0], corners[1], color);
	grcDrawLine(corners[1], corners[2], color);
	grcDrawLine(corners[2], corners[3], color);

	grcColor(Color32(previousColor));
}

#endif // __DEV
