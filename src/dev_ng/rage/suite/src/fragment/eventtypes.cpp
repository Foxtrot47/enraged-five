// 
// fragment/eventtypes.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.  
// 

#include "eventtypes.h"

#include "animation.h"
#include "cache.h"
#include "drawable.h"
#include "eventtypes_parser.h"
#include "instance.h"
#include "type.h"
#include "typechild.h"
#include "typegroup.h"

#include "breakableglass/glassmanager.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "crskeleton/skeleton.h"
#include "data/safestruct.h"
#include "event/player.h"
#include "event/runner.h"
#include "event/updatedata.h"
#include "grmodel/matrixset.h"
#include "math/random.h"
#include "system/timemgr.h"

namespace rage {

fragSnuffEventInstance::fragSnuffEventInstance()
	: m_CrossedHealth(-2.0f)
	, m_ChanceOfSnuff(1.0f)
	, m_SnuffComponent(-1)
{
}

void fragSnuffEventInstance::Copy(const evtInstance& other)
{
	Assert(dynamic_cast<const fragSnuffEventInstance*>(&other));
	const fragSnuffEventInstance& typedOther = static_cast<const fragSnuffEventInstance&>(other);

	evtInstance::Copy(typedOther);

	m_CrossedHealth = typedOther.m_CrossedHealth;
	m_ChanceOfSnuff = typedOther.m_ChanceOfSnuff;
	m_SnuffComponent = typedOther.m_SnuffComponent;
}

#if __DECLARESTRUCT
void fragSnuffEventInstance::DeclareStruct(datTypeStruct &s)
{
	evtInstance::DeclareStruct(s);

	SSTRUCT_BEGIN_BASE(fragSnuffEventInstance, evtInstance)

	SSTRUCT_FIELD(fragSnuffEventInstance, m_CrossedHealth)
	SSTRUCT_FIELD(fragSnuffEventInstance, m_ChanceOfSnuff)
	SSTRUCT_FIELD(fragSnuffEventInstance, m_SnuffComponent)

	SSTRUCT_END(fragSnuffEventInstance)
}
#endif // __DECLARESTRUCT

fragSnuffEventType::fragSnuffEventType()
	: m_NormalizedHealthPrior(evtParamManager::PARAM_NOT_FOUND)
	, m_NormalizedHealthLeft(evtParamManager::PARAM_NOT_FOUND)
	, m_Inst(evtParamManager::PARAM_NOT_FOUND)
	, m_BreakComponent(evtParamManager::PARAM_NOT_FOUND)
{
}

void fragSnuffEventType::LookupParameterIds()
{
	m_NormalizedHealthPrior = EVENTPARAM.GetParamId("health prior");
	m_NormalizedHealthLeft = EVENTPARAM.GetParamId("health left");
	m_Inst = EVENTPARAM.GetParamId("fragment instance pointer");
	m_BreakComponent = EVENTPARAM.GetParamId("component");
}

#if ENABLE_FRAGMENT_EVENTS
void fragSnuffEventType::Start(const evtInstance& inst, const evtUpdateData& data)
{
	bool trigger = true;

	//Grab an instance and go
	const fragSnuffEventInstance& realInst = static_cast<const fragSnuffEventInstance&>(inst);

	// See if we're going to trigger
	if (realInst.m_CrossedHealth >= -1.0f)
	{
		if (m_NormalizedHealthPrior != evtParamManager::PARAM_NOT_FOUND &&
			m_NormalizedHealthLeft != evtParamManager::PARAM_NOT_FOUND)
		{
			float normalizedHealthPrior = atAnyCast<float>(data.m_EventRunner->GetParamList()->GetParam(m_NormalizedHealthPrior));
			float normalizedHealthLeft = atAnyCast<float>(data.m_EventRunner->GetParamList()->GetParam(m_NormalizedHealthLeft));

			if (realInst.m_CrossedHealth < normalizedHealthLeft || realInst.m_CrossedHealth >= normalizedHealthPrior)
			{
				trigger = false;
			}
		}
	}

	if (realInst.m_ChanceOfSnuff < 1.0f)
	{
		float elapsed = TIME.GetUnpausedElapsedTime();
		mthRandom rand(*reinterpret_cast<int*>(&elapsed));

		if (rand.GetFloat() > realInst.m_ChanceOfSnuff)
		{
			trigger = false;
		}
	}

	if (trigger)
	{
		int snuffComponent;
		if (realInst.m_SnuffComponent != -1 || m_BreakComponent == evtParamManager::PARAM_NOT_FOUND)
		{
			snuffComponent = realInst.m_SnuffComponent;
		}
		else
		{
			if (data.m_EventRunner->GetParamList()->ParamExists(m_BreakComponent))
			{
				snuffComponent = atAnyCast<int>(data.m_EventRunner->GetParamList()->GetParam(m_BreakComponent));
			}
			else
			{
				snuffComponent = 0;
			}
		}

		if (snuffComponent < 0)
		{ 
			snuffComponent = 0;
		}

		fragInst* inst = atAnyCast<fragInst*>(data.m_EventRunner->GetParamList()->GetParam(m_Inst));
		Assert(inst);
		Assert(inst->GetType());
		int groupIndex = inst->GetTypePhysics()->GetChild(snuffComponent)->GetOwnerGroupPointerIndex();

		SnuffGroup(*inst, groupIndex);
	}
}

void fragSnuffEventType::SnuffGroup(fragInst& inst, int groupIndex)
{
	if (fragCacheEntry* entry = inst.GetCacheEntry())
	{
		fragTypeGroup* group = inst.GetTypePhysics()->GetGroup(groupIndex);
		Assert(group);

		fragHierarchyInst* hierInst = entry->GetHierInst();

		int lastChild = group->GetNumChildren() + group->GetChildFragmentIndex();
		for (int child = group->GetChildFragmentIndex(); child < lastChild; ++child)
		{
			// We've got a fragment child ...
			if (fragContinuousEventPlayer* player = hierInst->continuousPlayers[child])
			{
				player->Stop(1.0f);
				hierInst->continuousPlayers[child] = NULL;
			}
		}
	}
}
#endif // ENABLE_FRAGMENT_EVENTS

fragDamageEventInstance::fragDamageEventInstance()
: m_CrossedHealth(-2.0f)
, m_ChanceOfDamage(1.0f)
, m_DamageComponent(-1)
, m_DamageHealth(1.0f)
{
}

void fragDamageEventInstance::Copy(const evtInstance& other)
{
	Assert(dynamic_cast<const fragDamageEventInstance*>(&other));
	const fragDamageEventInstance& typedOther = static_cast<const fragDamageEventInstance&>(other);

	evtInstance::Copy(typedOther);

	m_CrossedHealth = typedOther.m_CrossedHealth;
	m_ChanceOfDamage = typedOther.m_ChanceOfDamage;
	m_DamageComponent = typedOther.m_DamageComponent;
	m_DamageHealth = typedOther.m_DamageHealth;
}

#if __DECLARESTRUCT
void fragDamageEventInstance::DeclareStruct(datTypeStruct &s)
{
	evtInstance::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(fragDamageEventInstance, evtInstance)

	SSTRUCT_FIELD(fragDamageEventInstance, m_CrossedHealth)
	SSTRUCT_FIELD(fragDamageEventInstance, m_ChanceOfDamage)
	SSTRUCT_FIELD(fragDamageEventInstance, m_DamageComponent)
	SSTRUCT_FIELD(fragDamageEventInstance, m_DamageHealth)

	SSTRUCT_END(fragDamageEventInstance)
}
#endif // __DECLARESTRUCT

fragDamageEventType::fragDamageEventType()
	: m_NormalizedHealthPrior(evtParamManager::PARAM_NOT_FOUND)
	, m_NormalizedHealthLeft(evtParamManager::PARAM_NOT_FOUND)
	, m_Inst(evtParamManager::PARAM_NOT_FOUND)
	, m_OtherInst(evtParamManager::PARAM_NOT_FOUND)
	, m_BreakComponent(evtParamManager::PARAM_NOT_FOUND)
	, m_Position(evtParamManager::PARAM_NOT_FOUND)
{
}

void fragDamageEventType::LookupParameterIds()
{
	m_NormalizedHealthPrior = EVENTPARAM.GetParamId("health prior");
	m_NormalizedHealthLeft = EVENTPARAM.GetParamId("health left");
	m_Inst = EVENTPARAM.GetParamId("fragment instance pointer");
	m_OtherInst = EVENTPARAM.GetParamId("other instance pointer");
	m_BreakComponent = EVENTPARAM.GetParamId("component");
	m_Position = EVENTPARAM.GetParamId("position");
}

#if ENABLE_FRAGMENT_EVENTS
void fragDamageEventType::Start(const evtInstance& inst, const evtUpdateData& data)
{
	bool trigger = true;

	//Grab an instance and go
	const fragDamageEventInstance& realInst = static_cast<const fragDamageEventInstance&>(inst);

	// See if we're going to trigger
	if (realInst.m_CrossedHealth >= -1.0f)
	{
		if (m_NormalizedHealthPrior != evtParamManager::PARAM_NOT_FOUND &&
			m_NormalizedHealthLeft != evtParamManager::PARAM_NOT_FOUND)
		{
			float normalizedHealthPrior = atAnyCast<float>(data.m_EventRunner->GetParamList()->GetParam(m_NormalizedHealthPrior));
			float normalizedHealthLeft = atAnyCast<float>(data.m_EventRunner->GetParamList()->GetParam(m_NormalizedHealthLeft));

			if (realInst.m_CrossedHealth < normalizedHealthLeft || realInst.m_CrossedHealth >= normalizedHealthPrior)
			{
				trigger = false;
			}
		}
	}

	if (realInst.m_ChanceOfDamage < 1.0f)
	{
		float elapsed = TIME.GetUnpausedElapsedTime();
		mthRandom rand(*reinterpret_cast<int*>(&elapsed));

		if (rand.GetFloat() > realInst.m_ChanceOfDamage)
		{
			trigger = false;
		}
	}

	if (trigger)
	{
		int damageComponent;
		if (realInst.m_DamageComponent != -1 || m_BreakComponent == evtParamManager::PARAM_NOT_FOUND)
		{
			damageComponent = realInst.m_DamageComponent;
		}
		else
		{
			damageComponent = atAnyCast<int>(data.m_EventRunner->GetParamList()->GetParam(m_BreakComponent));
		}

		if (damageComponent < 0)
		{ 
			damageComponent = 0;
		}

		fragInst* inst = atAnyCast<fragInst*>(data.m_EventRunner->GetParamList()->GetParam(m_Inst));
		fragInst* otherInst = atAnyCast<fragInst*>(data.m_EventRunner->GetParamList()->GetParam(m_OtherInst));
		Vec3V position = atAnyCast<Vec3V>(data.m_EventRunner->GetParamList()->GetParam(m_Position));

		Assert(damageComponent >= 0);
		Assert(inst->GetType());
		Assert(damageComponent < inst->GetTypePhysics()->GetNumChildren());

		fragInst::Collision msg;
		msg.component = damageComponent;
		msg.damage = realInst.m_DamageHealth;
		msg.position = position;
		msg.otherInst = otherInst;

		msg.normal = Vec3V(V_Y_AXIS_WZERO);
		msg.relativeVelocity = Vec3V(V_ZERO);
		msg.relativeSpeed = 0.0f;
		msg.part = 0;
		msg.otherComponent = 0;
		msg.otherPart = 0;

		inst->CollisionDamage(msg, NULL);
	}
}
#endif // ENABLE_FRAGMENT_EVENTS

fragAnimEventInstance::fragAnimEventInstance()
: m_Speed(1.0f)
, m_Start(0.0f)
, m_Looping(false)
, m_End(-1.0f)
{
	m_AnimName[0] = '\0';
    memset(pad, 0, sizeof(pad));
}

void fragAnimEventInstance::Copy(const evtInstance& other)
{
	Assert(dynamic_cast<const fragAnimEventInstance*>(&other));
	const fragAnimEventInstance& typedOther = static_cast<const fragAnimEventInstance&>(other);

	evtInstance::Copy(typedOther);

	strncpy(m_AnimName, typedOther.m_AnimName, 64);
	m_Speed = typedOther.m_Speed;
	m_Start = typedOther.m_Start;
	m_End = typedOther.m_End;
    m_Looping = typedOther.m_Looping;
}

#if __DECLARESTRUCT
void fragAnimEventInstance::DeclareStruct(datTypeStruct &s)
{
	evtInstance::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(fragAnimEventInstance, evtInstance)

	SSTRUCT_CONTAINED_ARRAY(fragAnimEventInstance, m_AnimName)
	SSTRUCT_FIELD(fragAnimEventInstance, m_Speed)
	SSTRUCT_FIELD(fragAnimEventInstance, m_Start)
	SSTRUCT_FIELD(fragAnimEventInstance, m_End)
    SSTRUCT_FIELD(fragAnimEventInstance, m_Looping)
    SSTRUCT_CONTAINED_ARRAY(fragAnimEventInstance, pad)

	SSTRUCT_END(fragAnimEventInstance)
}
#endif // __DECLARESTRUCT

fragAnimEventType::fragAnimEventType()
	: m_Inst(evtParamManager::PARAM_NOT_FOUND)
{
}

void fragAnimEventType::LookupParameterIds()
{
	m_Inst = EVENTPARAM.GetParamId("fragment instance pointer");
}

#if ENABLE_FRAGMENT_EVENTS
void fragAnimEventType::Start(const evtInstance& evtInst, const evtUpdateData& data)
{
	const fragAnimEventInstance& realInst = static_cast<const fragAnimEventInstance&>(evtInst);

	char normalized[64];
	StringNormalize(normalized, realInst.m_AnimName, 64);

	if (fragInst* inst = atAnyCast<fragInst*>(data.m_EventRunner->GetParamList()->GetParam(m_Inst)))
	{
		if (/*crSkeleton* skeleton = */ inst->GetSkeleton())
		{
			fragCacheEntry* entry = inst->GetCacheEntry();
			Assert(entry);
			fragHierarchyInst* hierInst = entry->GetHierInst();

			hierInst->type = inst->GetType();
			if (hierInst->type)
			{
				fragDrawable* drawable = hierInst->type->GetCommonDrawable();
				int animCount = drawable->GetAnimCount();

				for (int animIndex = 0; animIndex < animCount; ++animIndex)
				{
					fragAnimation* anim = drawable->GetAnimation(animIndex);
					Assert(anim);

					if (stricmp(normalized, anim->Name) == 0)
					{
						hierInst->animation = anim;

						// Play the animation
						hierInst->animPhase = realInst.m_Start;

                        if (realInst.m_Looping)
                        {
                            float end = anim->Animation->GetDuration();

                            if (realInst.m_End >= realInst.m_Start && realInst.m_End < anim->Animation->GetDuration())
                            {
                                end = realInst.m_End;
                            }

                            hierInst->animPhase = g_ReplayRand.GetRanged(realInst.m_Start, end);
                        }

						if (anim->Animation->GetDuration() < realInst.m_Start)
						{	
							hierInst->animPhase = anim->Animation->GetDuration();

							Warningf("Animation event '%s', start time %f is past the end of the animation %f",
								realInst.m_AnimName,
								realInst.m_Start,
								anim->Animation->GetDuration());
						}

#if 0
						crFrame* frame = hierInst->type->GetAnimFrame();
						Assert(frame);
						anim->Animation->CompositeFrame(hierInst->animPhase, *frame);
						frame->Pose(*skeleton);
#endif
					}
				}

				if (hierInst->animation == NULL)
				{
					Warningf("Animation '%s' was requested by a fragAnimEventInstance but was not found in the type '%s'", normalized, inst->GetType()->GetBaseName());
				}
			}
		}
	}
}

void fragAnimEventType::Update(const evtInstance& evtInst, const evtUpdateData& data)
{
	const fragAnimEventInstance& realInst = static_cast<const fragAnimEventInstance&>(evtInst);

	if (fragInst* inst = atAnyCast<fragInst*>(data.m_EventRunner->GetParamList()->GetParam(m_Inst)))
	{
		diagDebugLog(diagDebugLogPhysics, 'ETin', &inst);

		if (/*crSkeleton* skeleton = */ inst->GetSkeleton())
		{
			//diagDebugLog(diagDebugLogPhysics, 'ETsk', &skeleton);

			fragCacheEntry* entry = inst->GetCacheEntry();
			Assert(entry);
			fragHierarchyInst* hierInst = entry->GetHierInst();

			//diagDebugLog(diagDebugLogPhysics, 'EThI', &hierInst);
			diagDebugLog(diagDebugLogPhysics, 'ETT1', &hierInst->type);

			// I'm kind of being brave and keeping a pointer to the animation for efficiency, so check
			// to make sure the type is still where we think it is or we have to search for the animation again
			if (hierInst->type != inst->GetType())
			{
				hierInst->type = inst->GetType();

				diagDebugLog(diagDebugLogPhysics, 'ETT2', &hierInst->type);

				char normalized[64];
				StringNormalize(normalized, realInst.m_AnimName, 64);

				fragDrawable* drawable = hierInst->type->GetCommonDrawable();
				int animCount = drawable->GetAnimCount();

				for (int animIndex = 0; animIndex < animCount; ++animIndex)
				{
					fragAnimation* anim = drawable->GetAnimation(animIndex);
					Assert(anim);

					if (stricmp(normalized, anim->Name) == 0)
					{
						hierInst->animation = anim;
					}
				}
			}

			diagDebugLog(diagDebugLogPhysics, 'ETan', &hierInst->animation);

            if (hierInst->animation)
            {
				hierInst->animPhase += TIME.GetSeconds() * realInst.m_Speed;

				diagDebugLog(diagDebugLogPhysics, 'ETP1', &hierInst->animPhase);

			    float end = hierInst->animation->Animation->GetDuration();

			    // If the end is after the start and before the duration, use it
			    if (realInst.m_End >= realInst.m_Start && realInst.m_End < hierInst->animation->Animation->GetDuration())
			    {
				    end = realInst.m_End;
			    }

			    // End of animation
			    if (hierInst->animPhase > end)
			    {
				    if (realInst.m_Looping)
				    {
					    // Loop back to start
					    float length = end - realInst.m_Start;
					    length = Max(TIME.GetSeconds(), length);
					    hierInst->animPhase -= length;
				    }
				    else
				    {
					    // Freeze at end
					    hierInst->animPhase = end;
				    }
			    }

				diagDebugLog(diagDebugLogPhysics, 'ETP2', &hierInst->animPhase);

#if 0
				crFrame* frame = hierInst->type->GetAnimFrame();
				Assert(frame);
				hierInst->animation->Animation->CompositeFrame(hierInst->animPhase, *frame);
				frame->Pose(*skeleton);
				skeleton->Update();
				inst->ReportMovedBySim();
				inst->PoseBoundsFromSkeleton(true, true);
#endif
			}
		}
	}
}
#endif // ENABLE_FRAGMENT_EVENTS

fragPaneFrameBrokenEventInstance::fragPaneFrameBrokenEventInstance()
: m_GroupIndex(0)
, m_FrameFlags(0)
, m_CrackType(0)
, m_BrokenEventFlags(0)
{
}

void fragPaneFrameBrokenEventInstance::Copy(const evtInstance& other)
{
	Assert(dynamic_cast<const fragPaneFrameBrokenEventInstance*>(&other));
	const fragPaneFrameBrokenEventInstance& typedOther = static_cast<const fragPaneFrameBrokenEventInstance&>(other);

	evtInstance::Copy(typedOther);

	m_GroupIndex = typedOther.m_GroupIndex;
	m_FrameFlags = typedOther.m_FrameFlags;
	m_CrackType = typedOther.m_CrackType;
	m_BrokenEventFlags = typedOther.m_BrokenEventFlags;
}

#if __DECLARESTRUCT
void fragPaneFrameBrokenEventInstance::DeclareStruct(datTypeStruct &s)
{
	evtInstance::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(fragPaneFrameBrokenEventInstance, evtInstance)

		SSTRUCT_FIELD(fragPaneFrameBrokenEventInstance, m_GroupIndex)
		SSTRUCT_FIELD(fragPaneFrameBrokenEventInstance, m_FrameFlags)
		SSTRUCT_FIELD(fragPaneFrameBrokenEventInstance, m_CrackType)
		SSTRUCT_FIELD(fragPaneFrameBrokenEventInstance, m_BrokenEventFlags)

	SSTRUCT_END(fragPaneFrameBrokenEventInstance)
}
#endif // __DECLARESTRUCT

fragPaneFrameBrokenEventType::fragPaneFrameBrokenEventType()
: m_FragInst(evtParamManager::PARAM_NOT_FOUND)
{
}

void fragPaneFrameBrokenEventType::LookupParameterIds()
{
	m_FragInst = EVENTPARAM.GetParamId("fragment instance pointer");
}

class BreakFrameCallback
{
public:
	BreakFrameCallback(int crackType, u32 frameFlags, Vec3V_In worldImpactDir) :
	   m_CrackType(crackType)
	  ,m_FrameFlags(frameFlags)
	  ,m_WorldImpactDir(worldImpactDir)
	{
	}

	bgGlassImpactFunc GetGlassImpactCallback()
	{
	   bgGlassImpactFunc impactFunc;
	   impactFunc.Bind(this, &rage::BreakFrameCallback::BreakFrame);
	   return impactFunc;
	}
private:
	void BreakFrame(bgGlassHandle in_handle)
	{
		bgGlassManager::BreakFrame(in_handle, m_CrackType, m_FrameFlags, m_WorldImpactDir);
	}
	int m_CrackType;
	u32 m_FrameFlags;
	Vec3V m_WorldImpactDir;
};

#if ENABLE_FRAGMENT_EVENTS
void fragPaneFrameBrokenEventType::Start(const evtInstance& eventInst, const evtUpdateData& data)
{
	fragInst* pFragInst = atAnyCast<fragInst*>(data.m_EventRunner->GetParamList()->GetParam(m_FragInst));
	fragCacheEntry* entry = pFragInst->GetCacheEntry();
	if (!Verifyf(entry, "fragPaneFrameBrokenEventType playing on fragInst without cache entry!"))
	{
		return;
	}

	fragHierarchyInst* hierInst = entry->GetHierInst();
	const fragPaneFrameBrokenEventInstance& frameBrokenEventInst = static_cast<const fragPaneFrameBrokenEventInstance&>(eventInst);

	fragTypeGroup *group = pFragInst->GetTypePhysics()->GetGroup(frameBrokenEventInst.m_GroupIndex);
	fragTypeChild *child = pFragInst->GetTypePhysics()->GetChild(group->GetChildFragmentIndex());
	// if the group is already broken, look for the corresponding glass item in the fragHierarchyInst
	float minDamage = child->GetDamagedEntity() ? -group->GetDamageHealth() : 0.f;
	if (hierInst->groupInsts[frameBrokenEventInst.m_GroupIndex].damageHealth < minDamage)
	{
		// pane already broken: find the corresponding breakable if it still exists...
		for (int glassIndex = 0; glassIndex < hierInst->numGlassInfos; glassIndex++)
		{
			fragHierarchyInst::GlassInfo& glassInfo = hierInst->glassInfos[glassIndex];
			if (glassInfo.groupIndex == frameBrokenEventInst.m_GroupIndex)
			{
				RAGE_TRACK(BreakableGlass);
				if (bgGlassHandle_Invalid != glassInfo.handle)
				{
					bgGlassManager::BreakFrame(glassInfo.handle, frameBrokenEventInst.m_CrackType, frameBrokenEventInst.m_FrameFlags, Vec3V(V_Z_AXIS_WZERO));
					// This is just to play some audio...
				}
			}
		}
	}
	else
	{
		// The target pane is not yet broken; break it now
		hierInst->groupInsts[frameBrokenEventInst.m_GroupIndex].damageHealth = minDamage - 0.01f;
		BreakFrameCallback breakFrame(frameBrokenEventInst.m_CrackType, frameBrokenEventInst.m_FrameFlags, Vec3V(V_Z_AXIS_WZERO));
		entry->CreateBreakableGlassOnBone(frameBrokenEventInst.m_GroupIndex, pFragInst->GetType()->GetBoneIndexFromID(child->GetBoneID()), breakFrame.GetGlassImpactCallback());
		// @NOTE: ideally, we'd have an impact list to pass in here...
		pFragInst->HideDeadGroup(frameBrokenEventInst.m_GroupIndex, NULL);
		// @TODO: probably ought to generate a fragInst::Collision msg here and use it to play death events
		fragInst::Collision msg;
		pFragInst->PartDied(frameBrokenEventInst.m_GroupIndex, msg);
		pFragInst->PoseSkeletonFromArticulatedBody();
	}
}
#endif // ENABLE_FRAGMENT_EVENTS

} //namespace rage
