// 
// fragment/cachemanager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_CACHEMANAGER_H
#define FRAGMENT_CACHEMANAGER_H

#include "cache.h"

namespace rage {

#if __BANK
struct FragmentDebugData
{
	enum {FRAG_NONE, FRAG_PED, FRAG_VEHICLE};

	int m_size;
	atString m_name;
	const fragCacheEntry* m_frag;
	const void* m_object;
	Vector3 m_velocity;
	u32 m_type : 31;
	u32 m_reuse : 1;
};
#endif

/*
  PURPOSE
	Manages a pool of fragCacheEntry's for active fragObject instances.
	
  NOTES
	Objects have to be in
	the fragment cache before they can be articulated or damaged, or parts broken off of them.
	
<FLAG Component>
*/
class fragCacheManager
#if !__SPU
	: public datBase
#endif
{
public:
	fragCacheManager(u32 numCacheEntries);

	//see if we can get new cache entries...
	u32 CanGetNewCacheEntries(u32 num);
	u32 GetNumFreeCacheEntries() {return m_MaxNumCacheEntries - m_NumLiveCacheEntries;}

	//this will find a cache entry to evict(if possible), and evict it, passing the now unused entry back...
	fragCacheEntry* GetNewCacheEntry();
	void ReleaseCacheEntry(fragCacheEntry* entry);
	void Update(float timeSlice, int simUpdates);
	void SimUpdate(float timeSlice);
	u32 GetSeqNum();

	static fragCacheEntry* GetEntryFromInst(const fragInst* inst);

	SimpleHash<fragCacheKey>* GetFragHash() const;

	void TuneDamageHealth(fragType* type, int index);
	void TuneWeaponHealth(fragType* type, int index);

	void Init(fragInst** cacheBackingInsts);
	void Shutdown();
	void Reset();

	void Clear();
#if HACK_GTA4
	void ClearCloth();
#endif

#if __BANK
	void AddWidgets(bkBank &bank);
	void PopulateFragmentArray(atArray<FragmentDebugData>& array) const;
	int GetFramentSize() const;
	int GetFramentCount() const;
#endif

#if !__FINAL
	void DumpCacheEntries() const;
#endif

	PF_DRAW_ONLY(void ProfileDraw();)

private:
	u32     m_MaxNumCacheEntries;
	u32		m_NumLiveCacheEntries;
	u32		m_CacheSequenceNumber;

	/*a linked list of live cache entries...There are FRAG_MAX_NUM_CACHED total(defined in the cpp file), but 
	only the latter m_NumLiveCacheEntries are in use, the first FRAG_MAX_NUM_CACHED - m_NumLiveCacheEntries 
	entries are in free...
	*/
	DLListSimple<DLIST_SIMPLE_INIT(fragCacheEntry, link)>	m_CacheHead;

	fragCacheEntry*											m_CacheHeadOrg; //we call delete on this!
	
	SimpleHash<fragCacheKey>*								m_FragCacheHash;

    fragInst*                                               m_OwnedCacheBackingInsts;
	
};

inline u32 fragCacheManager::GetSeqNum()
{
	return m_CacheSequenceNumber;
}

inline fragCacheEntry* fragCacheManager::GetEntryFromInst(const fragInst* inst)
{
	FastAssert(inst->GetCached());

	return inst->GetCacheEntry();
}
	
inline SimpleHash<fragCacheKey>* fragCacheManager::GetFragHash() const
{
	return m_FragCacheHash;
}

} // namespace rage

#endif // FRAG_CACHEMANAGER_H
