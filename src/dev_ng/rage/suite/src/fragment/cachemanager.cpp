// 
// fragment/cachemanager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cachemanager.h" 
#include "typechild.h"
#include "typegroup.h"
#include "breakableglass/glassmanager.h"
#include "cloth_config.h"
#include "cloth/environmentcloth.h"
#include "cloth/charactercloth.h"
#include "physics/simulator.h"
#include "diag/output.h"

namespace rage {

fragCacheManager::fragCacheManager(u32 numCacheEntries)
	: m_MaxNumCacheEntries(numCacheEntries)
	, m_CacheSequenceNumber(1)
    , m_FragCacheHash(NULL)
	, m_OwnedCacheBackingInsts(NULL)
{
}

void fragCacheManager::Init(fragInst** cacheBackingInsts)
{
    fragAssertf(m_FragCacheHash == NULL, "fragCacheManager::Init has already been called once.");

	m_FragCacheHash = rage_new SimpleHash<fragCacheKey>;
	m_FragCacheHash->Init(m_MaxNumCacheEntries);
	
	m_NumLiveCacheEntries = 0;
	m_CacheHeadOrg = rage_new fragCacheEntry[m_MaxNumCacheEntries];
	m_CacheHead.SetAndInitPool(m_CacheHeadOrg, m_MaxNumCacheEntries);

    if (cacheBackingInsts == NULL)
    {
        m_OwnedCacheBackingInsts = rage_new fragInst[m_MaxNumCacheEntries];
    }

	for (u32 entry = 0; entry < m_MaxNumCacheEntries; ++entry)
	{
        if (cacheBackingInsts)
        {
            m_CacheHeadOrg[entry].m_BackingInst = cacheBackingInsts[entry];
        }
        else
        {
            m_CacheHeadOrg[entry].m_BackingInst = &m_OwnedCacheBackingInsts[entry];
        }

		FRAGMGR->InitializeUserData(m_CacheHeadOrg[entry].m_BackingInst);
	}
}

void fragCacheManager::Shutdown()
{
	delete [] m_OwnedCacheBackingInsts;
	delete [] m_CacheHeadOrg;
	delete m_FragCacheHash;
}

void fragCacheManager::Reset()
{
	fragInst** cacheEntriesToReinsert = Alloca(fragInst*, m_NumLiveCacheEntries);
	int numToReinsert = 0;

	while (m_NumLiveCacheEntries > 0)
	{
		fragCacheEntry* toDelete = m_CacheHead.GetLast();
		if (toDelete->GetInst()->ShouldBeInCacheToDraw() && !toDelete->IsUsingBackingInst())
		{
			cacheEntriesToReinsert[numToReinsert++] = toDelete->GetInst();
		}

		ReleaseCacheEntry(toDelete);
	}

	m_CacheHead.Reset();
	m_CacheHead.SetAndInitPool(m_CacheHeadOrg, m_MaxNumCacheEntries);

	for (int i = 0; i < numToReinsert; ++i)
	{
		cacheEntriesToReinsert[i]->PutIntoCache();

		if (!cacheEntriesToReinsert[i]->IsInLevel())
		{
			if(cacheEntriesToReinsert[i]->GetInsertionState() == phLevelBase::OBJECTSTATE_INACTIVE)
			{
				PHSIM->AddInactiveObject(cacheEntriesToReinsert[i]);
			}
			else if(cacheEntriesToReinsert[i]->GetInsertionState() == phLevelBase::OBJECTSTATE_FIXED)
			{
				PHSIM->AddFixedObject(cacheEntriesToReinsert[i]);
			}
			else if(cacheEntriesToReinsert[i]->GetInsertionState() == phLevelBase::OBJECTSTATE_ACTIVE)
            {
				PHSIM->AddActiveObject(cacheEntriesToReinsert[i]);
			}
            else
            {
                Assert(cacheEntriesToReinsert[i]->GetInsertionState() == phLevelBase::OBJECTSTATE_NONEXISTENT);
            }
		}
	}
}


void fragCacheManager::Clear()
{
	while (m_NumLiveCacheEntries > 0)
	{
		fragCacheEntry* toDelete = m_CacheHead.GetLast();
		//Assert(toDelete->GetInst()->GetLevelIndex() != phInst::INVALID_INDEX);
		toDelete->GetInst()->SetLevelIndex(phInst::INVALID_INDEX);
		ReleaseCacheEntry(toDelete);
	}

	m_CacheHead.Reset();
	m_CacheHead.SetAndInitPool(m_CacheHeadOrg, m_MaxNumCacheEntries);
}


/*for networking, maybe two lists - one for all locally owned entries, and one for all 
non locally owned entries.  When someone tells me to add, maybe steal from the local list, and delete 
from there...If everyone does that, it should self cull, I think...
*/
u32 fragCacheManager::CanGetNewCacheEntries(u32 num)
{
    fragAssertf(m_FragCacheHash != NULL, "fragCacheManager::Init has not been called.");

	u32 numAvail = m_MaxNumCacheEntries - m_NumLiveCacheEntries;
	if (numAvail >= num)
	{
		return numAvail;
	}
	
	fragCacheEntry* curEntry = m_CacheHead.GetLast();

	for (u32 entry = 0; entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
	{
		if (curEntry->GetSequenceNumber() < m_CacheSequenceNumber - 1 &&
			!curEntry->GetInst()->ShouldBeInCacheToDraw())
		{
			numAvail++;
			if (numAvail >= num)
			{
				return numAvail;
			}
		}
	}
	
	return numAvail;
}

fragCacheEntry* fragCacheManager::GetNewCacheEntry()
{
	fragCacheEntry* result;
	
	//first, see if we have a free entry...
	if (m_NumLiveCacheEntries < m_MaxNumCacheEntries)
	{
		//cool - just pop one from here...
		result = m_CacheHead.GetFirst();
		Assert(result);
		
		//move the object to the end, since it's in use now...
		m_CacheHead.Remove(result);
		m_CacheHead.AddToTail(result);
		
		++m_NumLiveCacheEntries;
		
		result->SetSequenceNumber();
		
		return result;
	}

	if(!Verifyf(!PHSIM->IsUpdateRunning(),"Could not allocate cache entry because we have none available and are unable to remove any at this time."))
	{
		return NULL;
	}
	
	/*we need to try to evict an entry - we will search through all the used entries and pick the one that has the least 
	impact...
	*/

	result = NULL;
	fragCacheEntry* curEntry = m_CacheHead.GetLast();
	Assert(curEntry);


	if (FRAGMGR->GetUseRecyclingSphere())
	{
		const spdSphere& sphere = FRAGMGR->GetRecyclingSphere();
		ScalarV leastImpact = ScalarV(V_FLT_MAX);
		ScalarV recyclingSphereRadius = sphere.GetRadius();

		for (u32 entry = 0; entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
		{
			if (curEntry->GetSequenceNumber() < m_CacheSequenceNumber - 1 &&
				!curEntry->GetInst()->ShouldBeInCacheToDraw())
			{
				const ScalarV radiusSum = recyclingSphereRadius + curEntry->GetInst()->GetXFormSphere().GetRadius();
				const ScalarV impact = radiusSum*radiusSum - MagSquared(sphere.GetCenter() - curEntry->GetInst()->GetXFormSphere().GetCenter());

				if (IsLessThanAll(impact, ScalarV(V_ZERO)))
				{
					result = curEntry;
					break;
				}
				else if (IsLessThanAll(impact, leastImpact))
				{
					result = curEntry;
					leastImpact = impact;
				}
			}
		}
	}
	else
	{
		for (u32 entry = 0; !result && entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
		{
			if (curEntry->GetSequenceNumber() < m_CacheSequenceNumber - 1 &&
				!curEntry->GetInst()->ShouldBeInCacheToDraw())
			{
				// Look for something off-screen
				if (!FRAGMGR->GetInterestShaft_IsVisibleSphere(curEntry->GetInst()->GetXFormSphere()))
				{
					result = curEntry;
				}
			}
		}

		if (!result)
		{
			curEntry = m_CacheHead.GetLast();
			const Matrix34* camMat = &FRAGMGR->GetInterestMatrix();

			float leastImpact = FLT_MAX;
			for (u32 entry = 0; entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
			{
				if (curEntry->GetSequenceNumber() < m_CacheSequenceNumber - 1 &&
					!curEntry->GetInst()->ShouldBeInCacheToDraw())
				{
					float impact = curEntry->GetInst()->GetImpact(1.0f / 300.0f, false, &camMat, 1);

					if (impact < leastImpact)
					{
						leastImpact = impact;
						result = curEntry;
					}
				}
			}
		}
	}

	if (result)
	{
		//kill the entry we're giving back...
		result->UnInit();
		
		result->SetSequenceNumber();
	}

	return result;
}

void fragCacheManager::ReleaseCacheEntry(fragCacheEntry* entry)
{
	Assert(entry);
	entry->UnInit();
	m_CacheHead.Remove(entry);
	m_CacheHead.AddToHead(entry);
	
	--m_NumLiveCacheEntries;
	Assert(m_NumLiveCacheEntries <= m_MaxNumCacheEntries);
}

void fragCacheManager::SimUpdate(float UNUSED_PARAM(timeSlice))
{
	fragAssertf(m_FragCacheHash != NULL, "fragCacheManager::Init has not been called.");

	bgGlassManager::SetViewportData(
		RCC_VEC3V(FRAGMGR->GetInterestMatrix().d),
		FRAGMGR->GetViewportWidth(),
		FRAGMGR->GetViewportTanHFOV());

	++m_CacheSequenceNumber;
}

void fragCacheManager::Update(float timeSlice, int simUpdates)
{
    fragAssertf(m_FragCacheHash != NULL, "fragCacheManager::Init has not been called.");

	fragCacheEntry* curEntry = m_CacheHead.GetLast();
	Assert(curEntry);
	PrefetchDC(curEntry);
	fragCacheEntry* nextEntry = NULL;
	nextEntry = m_CacheHead.GetPrev(curEntry);
	if (nextEntry)
	{
		PrefetchDC(nextEntry);
	}

	u32 entry = 0;
	while (entry < m_NumLiveCacheEntries)
	{
		Assert(curEntry);
		Assert(curEntry->IsInit());	// it should have been init by the time it gets an update..
		curEntry->Update(timeSlice, simUpdates);

		++entry;
		curEntry = nextEntry;

		if (nextEntry)
		{
			nextEntry = m_CacheHead.GetPrev(nextEntry);
			PrefetchDC(nextEntry);
		}
	}
}

void fragCacheManager::TuneDamageHealth(fragType* type, int index)
{
	fragCacheEntry* curEntry = m_CacheHead.GetLast();
	Assert(curEntry);

	for (u32 entry = 0; entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
	{
		if (curEntry->GetInst()->GetType() == type && curEntry->GetInst())
		{
			curEntry->GetHierInst()->groupInsts[index].damageHealth = curEntry->GetInst()->GetTypePhysics()->GetGroup(index)->GetDamageHealth();
		}
	}
}

void fragCacheManager::TuneWeaponHealth(fragType* type, int index)
{
	fragCacheEntry* curEntry = m_CacheHead.GetLast();
	Assert(curEntry);

	for (u32 entry = 0; entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
	{
		if (curEntry->GetInst()->GetType() == type)
		{
			curEntry->GetHierInst()->groupInsts[index].weaponHealth = curEntry->GetInst()->GetTypePhysics()->GetGroup(index)->GetWeaponHealth();
		}
	}
}

#if !__FINAL
void fragCacheManager::DumpCacheEntries() const
{
#if FRAG_CACHE_HEAP_DEBUG
	const fragCacheEntry* curEntry = m_CacheHead.GetLast();
	Assert(curEntry);

	size_t totalUsedMemory = 0;

	Displayf("*********BEGIN DUMP FRAG CACHE ENTRIES*********");
	Displayf("Index, Name, Size (bytes)");
	for (u32 entry = 0; entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
	{
		const fragInst* inst = curEntry->GetInst();
		if (inst)
		{
			if (const fragType* type = inst->GetType())
			{
				size_t usedMemory = 0;
				for (fragCacheBlockList::ConstIterator it(curEntry->m_Blocks.GetFirst()); it.IsValid(); it.Next())
				{
					const fragCacheBlock* block = it.Item();
					usedMemory += FRAGCACHEALLOCATOR->GetSizeWithOverhead(block->ptr);
				}

				totalUsedMemory += usedMemory;
				Displayf("%4d: %s, %" SIZETFMT "d", entry, type->GetBaseName(), usedMemory);
			}
		}
	}
	Displayf("*********END DUMP FRAG CACHE ENTRIES*********");
	Displayf("Total used: %" SIZETFMT "d", totalUsedMemory);
#endif
}
#endif

#if __BANK
void fragCacheManager::AddWidgets(bkBank& bank)
{
	// Nothing to do just yet.
	bank.AddButton("Dump Frag Cache Entries", datCallback(MFA(fragCacheManager::DumpCacheEntries), this));
}

int fragCacheManager::GetFramentCount() const
{
	int totalFrags = 0;
	const fragCacheEntry* curEntry = m_CacheHead.GetLast();
	Assert(curEntry);

	for (u32 entry = 0; entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
	{
		const fragInst* inst = curEntry->GetInst();
		if (inst)
		{
			if (inst->GetType())
			{
				for (fragCacheBlockList::ConstIterator it(curEntry->m_Blocks.GetFirst()); it.IsValid(); it.Next())
					totalFrags++;
			}
		}
	}

	return totalFrags;
}

int fragCacheManager::GetFramentSize() const
{
	int totalMemory = 0;
	const fragCacheEntry* curEntry = m_CacheHead.GetLast();
	Assert(curEntry);

	for (u32 entry = 0; entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
	{
		const fragInst* inst = curEntry->GetInst();
		if (inst)
		{
			if (inst->GetType())
			{
				for (fragCacheBlockList::ConstIterator it(curEntry->m_Blocks.GetFirst()); it.IsValid(); it.Next())
				{
					const fragCacheBlock* block = it.Item();
					totalMemory += (int) FRAGCACHEALLOCATOR->GetSizeWithOverhead(block->ptr);
				}
			}
		}
	}

	return totalMemory;
}

void fragCacheManager::PopulateFragmentArray(atArray<FragmentDebugData>& array) const
{
	const fragCacheEntry* pEntry = m_CacheHead.GetLast();
	Assert(pEntry);

	Vector3 velocity(Vector3::ZeroType);
	for (u32 i = 0; i < m_NumLiveCacheEntries; ++i, pEntry = m_CacheHead.GetPrev(pEntry))
	{
		const fragInst* pInst = pEntry->GetInst();
		if (pInst)
		{
			if (const fragType* pType = pInst->GetType())
			{
				int usedMemory = 0;
				for (fragCacheBlockList::ConstIterator it(pEntry->m_Blocks.GetFirst()); it.IsValid(); it.Next())
				{
					const fragCacheBlock* block = it.Item();
					usedMemory += (int) FRAGCACHEALLOCATOR->GetSizeWithOverhead(block->ptr);
				}

				atString name(pType->GetBaseName());
				FragmentDebugData data = { usedMemory, name, pEntry, NULL, velocity, FragmentDebugData::FRAG_NONE, false };
				array.Push(data);
			}
		}
	}
}
#endif

#if __PFDRAW
void fragCacheManager::ProfileDraw()
{
	fragCacheEntry* curEntry = m_CacheHead.GetLast();

	for (u32 entry = 0; entry < m_NumLiveCacheEntries; ++entry, curEntry = m_CacheHead.GetPrev(curEntry))
	{
		if (fragInst* inst = curEntry->GetInst())
		{
			const fragType* type = inst->GetType();
			type->ProfileDraw(inst->GetCurrentPhysicsLOD(), RCC_MATRIX34(inst->GetMatrix()), FRAGMGR->GetInterestMatrix().d, inst->GetSkeleton(), curEntry->GetHierInst());
		}
	}
}
#endif // __PFDRAW

} // namespace rage

