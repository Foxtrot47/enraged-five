// 
// fragment/instance.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "instance.h"

// fragment
#include "cache.h"
#include "cachemanager.h"
#include "drawable.h"
#include "eventplayer.h"
#include "tune.h"
#include "typechild.h"
#include "typegroup.h"

// RAGE
#include "atl/bitset.h"
#include "bank/bank.h"
#include "breakableglass/glassmanager.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "data/safestruct.h"
#include "diag/output.h"
#include "event/set.h"
#include "pharticulated/articulatedcollider.h"
#include "phbound/boundcomposite.h"
#include "phcore/phmath.h"
#include "phsolver/contactmgr.h"
#include "physics/collider.h"
#include "physics/colliderdispatch.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "physics/sleep.h"
#include "physics/broadphase.h"
#include "profile/profiler.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "vector/quaternion.h"
#include "vectormath/legacyconvert.h"
#include "math/angmath.h"

// Used for active pose
#include "cranimation/framefilters.h"

#include "cloth/environmentcloth.h"
#include "cloth/charactercloth.h"

#define FRAGINST_TRANSFERVELOCITIES 1

#define FRAGINST_USE_NEW_REPLACE_INSTANCE_COMPONENTS	1

// Set the frame rates to use for testing whether a force can break an object. Object breaking is done through
// impulses, so breaking with forces would depend on the frame rate if the actual frame time was used.
#define DEFAULT_FRAME_RATE	30.0f
#define DEFAULT_FRAME_TIME	1.0f/DEFAULT_FRAME_RATE

FRAGMENT_OPTIMISATIONS()

namespace rage {

namespace fragStats
{
	EXT_PF_TIMER(BreakApart);
	EXT_PF_TIMER(PoseBoundsFromSkeleton);
}

using namespace fragStats;

PARAM(fragcachewarn, "[fragment] How many times to warn if fragment cache was full when an entry was needed");

#if __BANK == 0
#define PARAM_FRAGDEBUG_1_GET false
#define PARAM_FRAGDEBUG_2_GET false
#else // __FINAL
#define PARAM_FRAGDEBUG_1_GET (fragTune::IsInstantiated() && FRAGTUNE->GetFragDebug1() && FRAGTUNE->GetTypeBeingTuned() == type)
#define PARAM_FRAGDEBUG_2_GET (fragTune::IsInstantiated() && FRAGTUNE->GetFragDebug2() && FRAGTUNE->GetTypeBeingTuned() == type)
#endif // __FINAL

bool fragInst::HasArticulatedBody() const 
{ 
	return GetCached() && GetCacheEntry() && GetCacheEntry()->GetHierInst() && GetCacheEntry()->GetHierInst()->body; 
}

phArticulatedBody * fragInst::GetArticulatedBody() 
{ 
	return (HasArticulatedBody() ? GetCacheEntry()->GetHierInst()->body : NULL); 
}

bool fragInst::HasArticulatedCollider() const 
{ 
	return GetCached() && GetCacheEntry() && GetCacheEntry()->GetHierInst() && GetCacheEntry()->GetHierInst()->articulatedCollider; 
}

phArticulatedCollider * fragInst::GetArticulatedCollider() 
{ 
	return (HasArticulatedCollider() ? GetCacheEntry()->GetHierInst()->articulatedCollider : NULL); 
}

bool fragInst::Insert(bool searchCache/*= true*/)
{
	Assert(!GetInserted());

	if (searchCache && m_Guid != 0)
	{
		fragCacheKey searchKey(m_Guid);

		//looks long, but actually should generate minimal code outside of ::Search...
		if (FRAGCACHEMGR->GetFragHash()->Search(&searchKey))
		{
			//we've been cached - bail!
			return false;
		}
	}

	// We shouldn't be in the level yet.
	Assert(!IsInLevel());

	// Record that we have been asked to be inserted.  This is below the above bail out because it returns false there.
	//   From here on we are going to return true.
	SetInserted(true);

	FRAGMGR->InsertFragInst(this);

	const fragType* type = GetType();

	// There used to be a check here that the type has at least one active part in its composite, and if not it would set the insertion state
	// to OBJECTSTATE_NONEXISTENT. But props that are only a single cloth piece can have no active bound parts here, and one is added later.
	// So this check is done in ReportTypePagedIn now, after the bound is created.
	if (type && GetInsertionState()!=phLevelBase::OBJECTSTATE_NONEXISTENT)
	{
		if (!GetTypePhysics()->GetCompositeBounds()->CanBecomeActive())
		{
			SetInstFlag(phInst::FLAG_NEVER_ACTIVATE, true);
		}

		// Our fragType already exists!
		ReportTypePagedIn();

		// ReportTypePagedIn() should have inserted us.
		// Nope, we might have failed to insert due to the level being full (or more likely out of managed colliders)
// 		Assert(IsInLevel() == (GetInsertionState()!=phLevelBase::OBJECTSTATE_NONEXISTENT));
#if __ASSERT
		if (IsInLevel())
		{
		++fragManager::sm_PhysicsInstancesAdded;
		}
#endif
	}
	else
	{
		// Our fragType does not exist yet!
		Assert(GetArchetype() == NULL);
	}

	return true;
}

IMPLEMENT_PLACE(fragInst)

fragInst::fragInst()
	: m_InsertionState(phLevelBase::OBJECTSTATE_INACTIVE)
    , m_BitStates(0)
    , m_CacheEntry(NULL)
    , m_DamageHealth(0.0f)
    , m_Type(NULL)
	, m_OrgPos(ORIGIN)
    , m_Guid(0)
	, m_CurrentLOD(RAGDOLL_LOD_HIGH)
{
	FRAGMGR->InitFragInst(this);

	m_InstanceXFormSphere.SetV4(Vec4V(V_ZERO));
	m_Type = NULL;

	Vector3 eulers;
	M34_IDENTITY.ToEulersZYX(eulers);
	m_PackedEulers = PackEulersTo32(eulers);

	// Unpack the matrix that was just packed, before setting it in the instance, to avoid roundoff errors causing replay divergence.
	Matrix34 unpackedMatrix;
	GetMatrixFromEulers(unpackedMatrix);
	SetMatrix(RCC_MAT34V(unpackedMatrix));
}

fragInst::fragInst(const fragType* type, const Matrix34& matrix, u32 guid)
	: m_InsertionState(phLevelBase::OBJECTSTATE_INACTIVE)
    , m_BitStates(0)
    , m_CacheEntry(NULL)
    , m_DamageHealth(0.0f)
    , m_Type(NULL)
	, m_OrgPos(matrix.d)
    , m_Guid(guid)
	, m_CurrentLOD(RAGDOLL_LOD_HIGH)
{
	FRAGMGR->InitFragInst(this);

	Vector3 eulers;
	matrix.ToEulersZYX(eulers);
	m_PackedEulers = PackEulersTo32(eulers);

	// Unpack the matrix that was just packed, before setting it in the instance, to avoid roundoff errors causing replay divergence.
	Matrix34 unpackedMatrix;
	GetMatrixFromEulers(unpackedMatrix);
	SetMatrix(RCC_MAT34V(unpackedMatrix));

	m_Type = type;

	if (type)
	{
		// [SPHERE-OPTIMISE]
		m_InstanceXFormSphere.SetV4(Vec4V(
			GetTypePhysics()->GetCompositeBounds()->GetCentroidOffset(),
			GetTypePhysics()->GetCompositeBounds()->GetRadiusAroundCentroidV()
		));
		m_InstanceXFormSphere.TransformSphereScaled( RCC_MAT34V(unpackedMatrix) );
	}

	SetDefaultBitStates();
}


fragInst::fragInst(const fragType* type, Mat34V_In pose, u32 guid)
: m_InsertionState(phLevelBase::OBJECTSTATE_INACTIVE)
, m_BitStates(0)
, m_CacheEntry(NULL)
, m_DamageHealth(0.0f)
, m_Type(NULL)
, m_Guid(guid)
, m_CurrentLOD(RAGDOLL_LOD_HIGH)
{
	m_OrgPos.Set(VEC3V_TO_VECTOR3(pose.GetCol3()));
	FRAGMGR->InitFragInst(this);

	Vector3 eulers;
	MAT34V_TO_MATRIX34(pose).ToEulersZYX(eulers);
	m_PackedEulers = PackEulersTo32(eulers);

	// Unpack the matrix that was just packed, before setting it in the instance, to avoid roundoff errors causing replay divergence.
	Matrix34 unpackedMatrix;
	GetMatrixFromEulers(unpackedMatrix);
	SetMatrix(RCC_MAT34V(unpackedMatrix));

	m_Type = type;

	if (type)
	{
		// [SPHERE-OPTIMISE]
		m_InstanceXFormSphere.SetV4(Vec4V(
			GetTypePhysics()->GetCompositeBounds()->GetCentroidOffset(),
			GetTypePhysics()->GetCompositeBounds()->GetRadiusAroundCentroidV()
		));
		m_InstanceXFormSphere.TransformSphereScaled( RCC_MAT34V(unpackedMatrix) );
	}

	SetDefaultBitStates();
}


fragInst::fragInst(datResource& rsc)
	: phInstBreakable(rsc)
	, m_BitStates(0)
    , m_InsertionState(phLevelBase::OBJECTSTATE_INACTIVE)
	, m_CacheEntry(NULL)
	, m_CurrentLOD(RAGDOLL_LOD_HIGH)
{
	FRAGMGR->InitFragInst(this);

	Assert(m_Type);
	// [SPHERE-OPTIMISE]
	m_InstanceXFormSphere.SetV4(Vec4V(
		GetTypePhysics()->GetCompositeBounds()->GetCentroidOffset(),
		GetTypePhysics()->GetCompositeBounds()->GetRadiusAroundCentroidV()
	));
	m_InstanceXFormSphere.TransformSphereScaled( GetMatrix() );

	SetDefaultBitStates();
}

void fragInst::SetType(fragType* type)
{
    Assert(m_Type == NULL);
    Assert(type);
    Assert(!GetInserted());

	m_Type = type;

	if (GetArchetype() && GetArchetype()->GetBound())
	{
		// [SPHERE-OPTIMISE]
		m_InstanceXFormSphere.SetV4(Vec4V(
			GetArchetype()->GetBound()->GetCentroidOffset(),
			GetArchetype()->GetBound()->GetRadiusAroundCentroidV()
		));
	}
	else
	{
		m_InstanceXFormSphere.SetV4( RCC_VEC4V(m_Type->GetBoundingSphere()) );
	}

	m_InstanceXFormSphere.TransformSphereScaled( GetMatrix() );

	SetDefaultBitStates();
}

void fragInst::ExchangeType(fragType* type)
{
	fragCacheEntry* cacheEntry = GetCacheEntry();

	if (cacheEntry)
	{
		cacheEntry->ExchangeType(type);	
	}
	
	m_Type = type;

	SetDefaultBitStates();
}

void fragInst::SetDisableActivation(bool bDisable)
{
	m_BitStates |= FRAG_INSTANCE_DISABLE_ACTIVATION_OVERRIDE;
	if (bDisable)
	{
		m_BitStates |= FRAG_INSTANCE_DISABLE_ACTIVATION;
	}
	else
	{
		m_BitStates &= ~FRAG_INSTANCE_DISABLE_ACTIVATION;
	}
}

void fragInst::SetDisableBreakable(bool bDisable)
{
	m_BitStates |= FRAG_INSTANCE_DISABLE_BREAKING_OVERRIDE;
	if (bDisable)
	{
		m_BitStates |= FRAG_INSTANCE_DISABLE_BREAKING;
	}
	else
	{
		m_BitStates &= ~FRAG_INSTANCE_DISABLE_BREAKING;
	}
}

void fragInst::SetDisableDamage(bool bDisable)
{
	m_BitStates |= FRAG_INSTANCE_DISABLE_DAMAGE_OVERRIDE;
	if(bDisable)
	{
		m_BitStates |= FRAG_INSTANCE_DISABLE_DAMAGE;
	}
	else
	{
		m_BitStates &= ~FRAG_INSTANCE_DISABLE_DAMAGE;
	}
}

void fragInst::ResetDisableActivation()
{
	m_BitStates &= ~FRAG_INSTANCE_DISABLE_ACTIVATION_OVERRIDE;
}

void fragInst::ResetDisableBreakable()
{
	m_BitStates &= ~FRAG_INSTANCE_DISABLE_BREAKING_OVERRIDE;
}

void fragInst::ResetDisableDamage()
{
	m_BitStates &= ~FRAG_INSTANCE_DISABLE_DAMAGE_OVERRIDE;
}

bool fragInst::IsActivationFlagOverridden() const
{
	return (m_BitStates & FRAG_INSTANCE_DISABLE_ACTIVATION_OVERRIDE) != 0;
}

bool fragInst::IsActivationDisabled() const
{
	// Check if the user specified an override befeore checking the type
	if(IsActivationFlagOverridden())
	{
		if (m_BitStates & FRAG_INSTANCE_DISABLE_ACTIVATION)
		{
			return true;
		}
	}
	else if (GetType()->IsActivationDisabled())
	{
		return true;
	}
	return false;
}

bool fragInst::IsBreakingFlagOverridden() const
{
	return (m_BitStates & FRAG_INSTANCE_DISABLE_BREAKING_OVERRIDE) != 0;
}

bool fragInst::IsBreakingDisabled() const
{
	// Check if the user specified an override befeore checking the type
	if(IsBreakingFlagOverridden())
	{
		if (m_BitStates & FRAG_INSTANCE_DISABLE_BREAKING)
		{
			return true;
		}
	}
	else if (GetType()->IsBreakingDisabled())
	{
		return true;
	}
	return false;
}

bool fragInst::IsDamageFlagOverridden() const
{
	return (m_BitStates & FRAG_INSTANCE_DISABLE_DAMAGE_OVERRIDE) != 0;
}

bool fragInst::IsDamageDisabled() const
{
	if(IsDamageFlagOverridden())
	{
		if(m_BitStates & FRAG_INSTANCE_DISABLE_DAMAGE)
		{
			return true;
		}
	}
	/*else if(GetType()->IsDamageDisabled()) // Not going to implement a fragType::IsDamageDisabled until it's necessary
	{
		return true;
	}*/
	return false;
}

void fragInst::SetDefaultBitStates()
{
	m_BitStates &= ~(FRAG_INSTANCE_DISABLE_ACTIVATION_OVERRIDE | FRAG_INSTANCE_DISABLE_BREAKING_OVERRIDE | FRAG_INSTANCE_DISABLE_DAMAGE_OVERRIDE);
}

void fragInst::SetResetMatrix(const Matrix34& resetMatrix)
{
    m_OrgPos = resetMatrix.d;

    Vector3 eulers;
    resetMatrix.ToEulersZYX(eulers);
    m_PackedEulers = PackEulersTo32(eulers);
}

void fragInst::SetGuid(u32 guid)
{
    Assert(!GetInserted());
    m_Guid = guid;
}

void fragInst::SetBounds(const Vector4& bounds)
{
	m_InstanceXFormSphere.SetV4(Vec4V(m_InstanceXFormSphere.GetCenter(), ScalarV(bounds.w)));

	phBound* bound = GetArchetype() ? GetArchetype()->GetBound() : NULL;
	if (bound && IsLessThanOrEqualAll(bound->GetRadiusAroundCentroidV(),ScalarV(V_ZERO)))
	{
		// The bound hasn't been initialized, so give it a radius before this object is put in the level.
		bound->SetRadiusAroundCentroid(ScalarVFromF32(bounds.w));
	}
	else
	{

	}
}

fragInst::~fragInst()
{
	Assert(!GetCached());	//TMS: Really shouldn't be cached
	Assert(!GetInserted());
}

struct fragBreakDamage
{
	f32		tempBreakHealth;
	//f32		fTempDamageHealth;
};

struct fragFindBreakInfo
{
	Vec3V	impulse;
	Vec4V	position;

	u8		breakGroup; //if set to 0xFF, then this is a root break, else the group that is to break...
	bool	breakLatch; //if true, this is a latch break
	u8		pad[2];

	//this has as many elements as the number of groups in the instance...
	//f32		afTempBreakHealths[];
	fragBreakDamage	tempBreakDamage[];
};

bool fragInst::GetPartBroken() const
{
	return GetCached() && GetCacheEntry()->GetHierInst()->groupBroken->AreAnySet();
}

bool fragInst::GetGroupBroken(int groupIndex) const
{
	return GetCached() && GetCacheEntry()->GetHierInst()->groupBroken->IsSet(groupIndex);
}

bool fragInst::GetChildBroken(int childIndex) const
{
	return GetCached() && GetCacheEntry()->GetHierInst()->groupBroken->IsSet(GetTypePhysics()->GetChild(childIndex)->GetOwnerGroupPointerIndex());
}

int fragInst::GetComponentFromBoneIndex(s32 boneIndex) const
{
	// TODO: The caller should probably handle the negative bone index case, this should just be an assert.
	if(boneIndex >= 0)
	{
		if(const fragCacheEntry* entry = GetCacheEntry())
		{
			FastAssert(entry->IsInit());
			return entry->GetComponentIndexFromBoneIndex(boneIndex);
		}
		else
		{
			return GetType()->GetComponentFromBoneIndex(GetCurrentPhysicsLOD(), boneIndex);
		}
	}
	else
	{
		return -1;
	}
}

int fragInst::GetControllingComponentFromBoneIndex(s32 boneIndex) const
{
	// TODO: The caller should probably handle the negative bone index case, this should just be an assert.
	if(boneIndex >= 0)
	{
		if(const fragCacheEntry* entry = GetCacheEntry())
		{
			FastAssert(entry->IsInit());
			return entry->GetControllingComponentIndexFromBoneIndex(boneIndex);
		}
		else
		{
			return GetType()->GetControllingComponentFromBoneIndex(GetCurrentPhysicsLOD(), boneIndex);
		}
	}
	else
	{
		return -1;
	}
}

int fragInst::GetGroupFromBoneIndex(s32 boneIndex) const
{
	// TODO: The caller should probably handle the negative bone index case, this should just be an assert.
	if(boneIndex >= 0)
	{
		if(const fragCacheEntry* entry = GetCacheEntry())
		{
			FastAssert(entry->IsInit());
			int childIndex = entry->GetComponentIndexFromBoneIndex(boneIndex);
			if(childIndex >= 0)
			{
				return GetTypePhysics()->GetChild(childIndex)->GetOwnerGroupPointerIndex();
			}
			else
			{
				// If the bone isn't directly connected to a child let the user know by returning
				//   a group index of -1
				return -1;
			}
		}
		else
		{
			return GetType()->GetGroupFromBoneIndex(GetCurrentPhysicsLOD(), boneIndex);
		}
	}
	else
	{
		return -1;
	}
}

bool fragInst::GetBoneMatrix(Matrix34& result, s32 boneIndex, bool lastMatrix)
{
	if(GetType()==NULL)
		return false;

	result = RCC_MATRIX34(GetMatrix());	// ANIM_NEW_VEC_LIB
	int component = GetType()->GetComponentFromBoneIndex(GetCurrentPhysicsLOD(), boneIndex);	

	if(component < 0 || component > GetTypePhysics()->GetNumChildren())
		return false;

	phBoundComposite* boundComposite = dynamic_cast<phBoundComposite*>(GetArchetype()->GetBound());

	if(boundComposite==NULL)
		return false;

	if(lastMatrix)
		result = RCC_MATRIX34(boundComposite->GetLastMatrix(component));
	else
		result = RCC_MATRIX34(boundComposite->GetCurrentMatrix(component));

	// got component matrix, but we need the bone matrix, so get the transformation matrix between the two
	fragTypeChild* child = GetTypePhysics()->GetChild(component);
	Matrix34 transformationMatrix = child->GetUndamagedEntity()->GetBoundMatrix();

	// and un-transform it to get into local bone space
	transformationMatrix.Transpose();
	result.DotFromLeft(transformationMatrix);

	return true;
}

/*RECURSIVE function that calculates the breaking forces for a fragment hierarchy...*/
// This function potentially propagates the forces (and health damage) both up the hierarchy and down the hierarchy (to all of its child groups)
//   depending on 
void fragInst::PropagateAndAccumulateBreakingImpulse (f32 mag, fragBreakDamage* tempBreakDamage, u8 currentGroup, u8 sourceGroup) const
{
	// The transmission scale for any link between groups is on the child node only.
	//   When transmitting force from child to parent, the impulse is: parent impulse = child impulse * child transmission up
	//   When transmitting force from parent to child, the impulse is: child impulse = parent impulse * child transmission down
	const fragHierarchyInst *hierInst = GetCached() ? GetCacheEntry()->GetHierInst() : NULL;
	fragTypeGroup* fragHitGroup = GetTypePhysics()->GetGroup(currentGroup);
	Assert(hierInst == NULL || hierInst->groupBroken->IsClear(currentGroup));

	if(sourceGroup < currentGroup)
	{
		// If this impulse came from a parent, scale by the child's downward force transmission
		mag *= fragHitGroup->GetForceTransmissionScaleDown();
		if (fragTune::IsInstantiated())
		{
			mag *= FRAGTUNE->GetGlobalForceTransmissionScaleDown();
		}
	}
	else
	{
		// If the impulse came from a child, the impulse hasn't reached our parent yet so propagate the force upwards
		u8 parentGroup = fragHitGroup->GetParentGroupPointerIndex();
		if(parentGroup != 0xFF && (hierInst == NULL || hierInst->groupBroken->IsClear(parentGroup)))
		{
			float magUpwards = mag * fragHitGroup->GetForceTransmissionScaleUp();
			if (fragTune::IsInstantiated())
			{
				magUpwards *= FRAGTUNE->GetGlobalForceTransmissionScaleUp();
			}
			PropagateAndAccumulateBreakingImpulse(magUpwards, tempBreakDamage, parentGroup, currentGroup);
		}
	}

	// Apply impulse to the current group
	tempBreakDamage[currentGroup].tempBreakHealth += mag;

#if __BANK
	if(fragTune::IsInstantiated() && !FRAGTUNE->GetEnableCompleteImpulsePropagation())
	{
		// Previous implementation
		if(sourceGroup <= currentGroup)
		{
			// If this impulse came from a parent, or this is the initial group, apply the impulse to all
			//   of this group's children.
			// NOTE: This implementation will not propagate the impulse to all groups
			for (u8 groupOffset = 0; groupOffset < fragHitGroup->GetNumChildGroups(); ++groupOffset)
			{
				u8 childGroup = fragHitGroup->GetChildGroupsPointersIndex() + groupOffset;
				if (hierInst == NULL || hierInst->groupBroken->IsClear(childGroup))
				{
					PropagateAndAccumulateBreakingImpulse(mag, tempBreakDamage, childGroup, currentGroup);
				}
			}
		}
	}
	else
#endif
	{
		// No matter where the impulse came from, propagate it to all the children groups except for the source group
		//   since that group already processed the impulse.
		for (u8 groupOffset = 0; groupOffset < fragHitGroup->GetNumChildGroups(); ++groupOffset)
		{
			u8 childGroup = fragHitGroup->GetChildGroupsPointersIndex() + groupOffset;
			if (childGroup != sourceGroup && (hierInst == NULL || hierInst->groupBroken->IsClear(childGroup)))
			{
				PropagateAndAccumulateBreakingImpulse(mag, tempBreakDamage, childGroup, currentGroup);
			}
		}
	}
}


fragInst* fragInst::BreakOffAbove(int component)
{
	return BreakOffAboveGroup(GetTypePhysics()->GetChild(component)->GetOwnerGroupPointerIndex());
}

fragInst* fragInst::BreakOffAboveGroup(int groupIndex)
{
	fragInst* newInst = NULL;

	if(!Verifyf(groupIndex < GetTypePhysics()->GetNumChildGroups(), "Group %d doesn't exist on fragInst %s.", groupIndex, GetArchetype()->GetFilename()))
	{
		return NULL;
	}

	if (FRAGCACHEMGR->CanGetNewCacheEntries(1))
	{
		if (fragCacheEntry* entry = GetCacheEntry())
		{
			if (!Verifyf(entry->GetHierInst()->groupBroken->IsClear(groupIndex), "Group %d is already broken off! Aborting...", groupIndex))
			{
				return NULL;
			}
		}

		phBreakData findBreakInfoRaw;
		fragFindBreakInfo& findBreakInfo = reinterpret_cast<fragFindBreakInfo&>(findBreakInfoRaw);
		// We could convert groupIndex from 0 to 0xFF but the idea of the function is to return the newly broken off part. This is a bit weird
		//   if the new part is the same as the original part. 
		Assertf(groupIndex != 0, "BreakOffAboveGroup is not for breaking off the whole fragment, use BreakOffRoot instead.");
		findBreakInfo.impulse = Vec3V(V_ZERO);
		findBreakInfo.position = Vec4V(V_ZERO);
		findBreakInfo.breakGroup = (u8)groupIndex;
		findBreakInfo.breakLatch = false;

		newInst = static_cast<fragInst*>(BreakApart(findBreakInfoRaw));
		Assertf(newInst != this, "fragInst::BreakOffAboveGroup shouldn't be used for root breaks.");
	}
	else
	{
		NotifyCouldntGetCacheEntry();
	}

	return newInst;
}

void fragInst::BreakOffRoot()
{
	if (FRAGCACHEMGR->CanGetNewCacheEntries(1))
	{
		phBreakData findBreakInfo;
		reinterpret_cast<fragFindBreakInfo*>(&findBreakInfo)->breakGroup = 0xFF;
		reinterpret_cast<fragFindBreakInfo*>(&findBreakInfo)->breakLatch = false;
		BreakApart(findBreakInfo);
	}
	else
	{
		NotifyCouldntGetCacheEntry();
	}
}

void fragInst::DeleteAbove(int childIndex)
{
	DeleteAboveGroup(GetTypePhysics()->GetChild(childIndex)->GetOwnerGroupPointerIndex());
}

void fragInst::DeleteAboveGroup(int groupIndex)
{
	// Get a cache entry if we don't have one
	if(!GetCached())
	{
		PutIntoCache();
	}

	if(fragCacheEntry* entry = GetCacheEntry())
	{
		const fragPhysicsLOD* physicsLOD = GetTypePhysics();
		u8 rootGroupIndex = physicsLOD->GetChild(entry->GetHierInst()->rootLinkChildIndex)->GetOwnerGroupPointerIndex();
		Assertf(entry->IsGroupUnbroken(rootGroupIndex),"The root group shouldn't be broken. Root Group: %i, File Name: %s",groupIndex,GetArchetype()->GetFilename());
		// NOTE: We should be able to remove the instance if the user tries to delete the whole fragment tree but that will crash in game side code later on. 
		if(entry->IsGroupUnbroken(groupIndex) && Verifyf(groupIndex > rootGroupIndex,"Deleting this group means there will be no more groups in the cache entry. Group: %i, Root Group: %i, Filename: %s",groupIndex,rootGroupIndex,GetArchetype()->GetFilename()))
		{
			// Find all of the groups in subtree that the given group is the root of
			fragInst::ComponentBits groupsBreaking;
			physicsLOD->GetGroupsAbove((u8)groupIndex,groupsBreaking);

			// Clear out any pieces of glass in these groups
			entry->PurgeBreakableGlassInfos(groupsBreaking);

			// Clear already broken groups
			for(int breakingGroupIndex = groupIndex; breakingGroupIndex < physicsLOD->GetNumChildGroups(); breakingGroupIndex++)
			{
				groupsBreaking.Set(breakingGroupIndex, groupsBreaking.IsSet(breakingGroupIndex) && entry->IsGroupUnbroken(breakingGroupIndex));
			}
			Assert(groupsBreaking.AreAnySet());

			// Delete the subtree and update the cache entry
			float newRootMass = entry->AdjustForLostGroups(groupsBreaking);
			entry->SetMass(newRootMass);
			PHLEVEL->UpdateObjectLocationAndRadius(GetLevelIndex(), (Mat34V_Ptr)(NULL));

			// Let the user know that some groups were erased
			FRAGMGR->GetPartsBrokeOffFunc()(this,groupsBreaking,NULL);

			// Remove all contacts with the deleted groups
			fragInst::ComponentBits childrenBreaking;
			physicsLOD->ConvertGroupBitsetToChildBitset(groupsBreaking,childrenBreaking);
			PHSIM->GetContactMgr()->RemoveAllContactsWithInstance(this, childrenBreaking);
		}
	}
}

void fragInst::RestoreAbove(int childIndex)
{
	RestoreAboveGroup(GetTypePhysics()->GetChild(childIndex)->GetOwnerGroupPointerIndex());
}

void fragInst::RestoreAboveGroup(int groupIndex)
{
	// Get a cache entry if we don't have one
	if(!GetCached())
	{
		PutIntoCache();
	}

	if(fragCacheEntry* entry = GetCacheEntry())
	{
		const fragPhysicsLOD* physicsLOD = GetTypePhysics();
		u8 rootGroupIndex = physicsLOD->GetChild(entry->GetHierInst()->rootLinkChildIndex)->GetOwnerGroupPointerIndex();
		Assertf(entry->IsGroupUnbroken(rootGroupIndex),"The root group shouldn't be broken. Root Group: %i, File Name: %s",groupIndex,GetArchetype()->GetFilename());

		if(entry->IsGroupBroken(groupIndex) && Verifyf(groupIndex > rootGroupIndex,"Cannot restore the root group. Group: %i, Root Group: %i, Filename: %s",groupIndex,rootGroupIndex,GetArchetype()->GetFilename()))
		{
			// Find all of the groups in subtree that the given group is the root of
			fragInst::ComponentBits groupsRestoring;
			physicsLOD->GetGroupsAbove((u8)groupIndex,groupsRestoring);
			fragInst::ComponentBits groupsRestoringMask;
			groupsRestoringMask.SetAll();
			groupsRestoringMask.IntersectNegate(groupsRestoring);
			// Restore the subtree and update the cache entry
			fragHierarchyInst* hierInst = entry->GetHierInst();
			hierInst->groupBroken->Intersect(groupsRestoringMask);
			entry->UpdateIsFurtherBreakable();

			fragMemStartCacheHeapFunc(entry);

			// Loop over each of the groups and check to see if there's anything that we need to do
			const fragPhysicsLOD *physLOD = GetTypePhysics();
			const int numGroups = physLOD->GetNumChildGroups();
			for(int groupIndex = 0; groupIndex < numGroups; ++groupIndex)
			{
				const fragTypeGroup &typeGroup = *physLOD->GetGroup(groupIndex);
				const int numChildrenInGroup = typeGroup.GetNumChildren();
				const int firstChildIndexInGroup = typeGroup.GetChildFragmentIndex();

				if(groupsRestoring.IsSet(groupIndex))
				{
					for(int childInGroupIndex = 0; childInGroupIndex < numChildrenInGroup; ++childInGroupIndex)
					{
						// Restore skeleton
						const int childIndex = childInGroupIndex + firstChildIndexInGroup;
						fragTypeChild* child = physicsLOD->GetChild(childIndex);
						int nBoneIndex = GetType()->GetBoneIndexFromID(child->GetBoneID());
						if(nBoneIndex > -1 && GetSkeleton())
						{
							GetSkeleton()->PartialReset(nBoneIndex);
						}
						// Restore bounds
						if(entry->GetBound()->GetBound(childIndex) == NULL)
						{
							if(GetTypePhysics() && GetTypePhysics()->GetCompositeBounds())
							{
								phBoundComposite *pOriginalBound = GetTypePhysics()->GetCompositeBounds();
								if(pOriginalBound->GetBound(childIndex))
								{
									phLevelNew::CompositeBoundSetBoundThreadSafe(*entry->GetBound(), childIndex, pOriginalBound->GetBound(childIndex)->Clone());
									if(pOriginalBound->GetCurrentMatrices() && entry->GetBound()->GetCurrentMatrices())
										entry->GetBound()->SetCurrentMatrix(childIndex, pOriginalBound->GetCurrentMatrix(childIndex));
									if(pOriginalBound->GetLastMatrices() && entry->GetBound()->GetLastMatrices())
										entry->GetBound()->SetLastMatrix(childIndex, pOriginalBound->GetLastMatrix(childIndex));
									if(pOriginalBound->GetTypeAndIncludeFlags() && entry->GetBound()->GetTypeAndIncludeFlags())
									{
										entry->GetBound()->SetTypeFlags(childIndex, pOriginalBound->GetTypeFlags(childIndex));
										entry->GetBound()->SetIncludeFlags(childIndex, pOriginalBound->GetIncludeFlags(childIndex));
									}
								}
							}
						}
					}
				}
			}
			fragMemEndCacheHeap();

			PoseBoundsFromSkeleton(false, true);

			// TODO: Restore articulated body

			// Compute mass
			fragInst::ComponentBits allZeros;
			allZeros.Reset();
			float newRootMass = entry->AdjustForLostGroups(allZeros);
			entry->SetMass(newRootMass);


			PHLEVEL->UpdateObjectLocationAndRadius(GetLevelIndex(), (Mat34V_Ptr)(NULL));

			PartsRestored(groupsRestoring);
		}
	}
}

void fragInst::DamageAllGroups()
{
	int numGroups = GetTypePhysics()->GetNumChildGroups();
	for(int groupIndex = 0; groupIndex < numGroups; ++groupIndex)
	{
		fragCacheEntry* entry = GetCacheEntry();
		if(entry == NULL || (!entry->IsGroupDamaged(groupIndex) && !entry->IsGroupBroken(groupIndex)))
		{
			ReduceGroupDamageHealth(groupIndex,-1.0f,Vec3V(V_ZERO),Vec3V(V_ZERO));
		}
	}
}

void fragInst::ReduceGroupDamageHealth(int groupIndex, float newHealth, Vec3V_In position, Vec3V_In relativeVelocity FRAGMENT_EVENT_PARAM(bool applyEvents))
{
	const fragType* type = GetType();
	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	if(!GetCached() && type->GetNeedsCacheEntryToActivate())
	{
		PutIntoCache();

		if (!GetCached())
		{
			// No cache entry was available
			return;
		}
	}

	if(IsDamageDisabled())
	{
		return;
	}

	float healthPrior;
	if (!GetCached())
	{
		// Usually we must be a simple fragment to get here, but it's possible that we're not a simple fragment but we couldn't be put into the
		//   cache because we couldn't get activated (not enough active objects) or we ran out of cache entries.
		fragTypeChild* child = type->GetRootChild();
		if (!child)
		{
			child = physicsLOD->GetChild(0);
		}
		Assert(child != NULL || PHLEVEL->IsInactive(GetLevelIndex()));
		int groupIndex = child->GetOwnerGroupPointerIndex();

		if (groupIndex != 0xFF)
		{
			fragTypeGroup *group = physicsLOD->GetGroup(groupIndex);
			if(child != NULL)
			{
				float minDamage = 0.0f;			   
				if (child->GetDamagedEntity())
				{
					// Think this was added for gta4 because we got a crash here once
					if ((m_DamageHealth >= 0) && (newHealth < 0.0f) && physicsLOD->GetArchetype(true))
					{
						// Set the bounds to damaged
						if (phArchetype* damagedArchetype = physicsLOD->GetArchetype(true))
						{
							SetArchetype(damagedArchetype);
						}
					}
					minDamage = -group->GetDamageHealth();
				}

				healthPrior = m_DamageHealth;
				m_DamageHealth = Max(minDamage, newHealth);

				if (m_DamageHealth <= minDamage)
				{
					m_DamageHealth = minDamage;

					if (PHSIM->IsSafeToDelete() &&
						(group->GetDisappearsWhenDead() || group->GetMadeOfGlass()))
					{
						// Apply DisappearsWhenDead, but only if we are here because we are not cachable
						if (physicsLOD->GetNumChildGroups() == 1)
						{
							PHSIM->DeleteObject(GetLevelIndex());

							SetDead();
#if ENABLE_FRAGMENT_EVENTS
							// Play death events
							if (applyEvents)
							{
								const evtSet* set = group->GetDeathEventset();
								if (set && set->GetNumInstances() > 0)
								{
									Assert(group->GetDeathEventPlayer());
									fragCollisionEventPlayer& player = *group->GetDeathEventPlayer();

									// 								player.GetPosition() = msg.position;	
									// 								player.GetNormal() = msg.normal;
									// 								player.GetRelativeVelocity() = msg.relativeVelocity;
									// 								player.GetRelativeSpeed() = msg.relativeSpeed;
									// 								player.GetDamage() = msg.damage;
									player.GetNormalizedHealthPrior() = healthPrior;
									player.GetNormalizedHealthLeft() = m_DamageHealth;
									player.GetFragInst() = this;
									player.GetInst() = (phInst*)this;
									player.GetComponent() = 0;
									player.GetPart() = 0;
									// 								player.GetOtherInst() = msg.otherInst;
									// 								player.GetOtherComponent() = msg.otherComponent;
									// 								player.GetOtherPart() = msg.otherPart;
									// 								player.GetImpactIterator() = impacts ? *impacts : phContactIterator();

									player.StartUpdateStop();
								}
							}
#endif // ENABLE_FRAGMENT_EVENTS
						}
					}
				}
			}
		}
	}
	else
	{
		fragTypeGroup* group = physicsLOD->GetGroup(groupIndex);
		fragCacheEntry* entry = GetCacheEntry();
		fragHierarchyInst* hierInst = entry->GetHierInst();
		fragTypeChild* child = physicsLOD->GetChild(group->GetChildFragmentIndex());
		healthPrior = hierInst->groupInsts[groupIndex].damageHealth;

#if __ASSERT
		if( !entry->IsGroupUnbroken(groupIndex) )
		{
			fragWarningf( "fragInst being made to damage a group that has already been broken off. Inst: %p  -  LevelIndex: %d  -  groupIndex: %d", this, this->GetLevelIndex(), groupIndex);
		}
#endif
		bool poseSkeleton = false;

		if ( entry->IsGroupUnbroken(groupIndex) &&
			PHSIM->IsSafeToDelete() &&
			physicsLOD->IsGroupDamageable(groupIndex))
		{
			float minDamage = child->GetDamagedEntity() ? -group->GetDamageHealth() : 0.0f;
			hierInst->groupInsts[groupIndex].damageHealth = Max(minDamage, newHealth);

			if (healthPrior >= 0 && newHealth < 0.0f)
			{
				if (type->GetDamagedDrawable())
				{
					hierInst->anyGroupDamaged = true;
				}

				poseSkeleton = true;

				// If any of the children in this group have a damaged entity then the first one will.  Only enter the block below if there will be at least one bound
				//   remaining from this group.  If that is not the case, then the group will be going away and that will be handled in the HideDeadGroup() call below.
				if(child->GetDamagedEntity())
				{
					fragInst *entryInst = entry->GetInst();
					phBoundComposite *entryBound = entry->GetBound();
					Assert(entryInst->GetArchetype()->GetBound() == entryBound);
					ComponentBits removedChildren;
					for (int groupChildIndex = 0; groupChildIndex < group->GetNumChildren(); ++groupChildIndex)
					{
						int childIndex = group->GetChildFragmentIndex() + groupChildIndex;
						fragTypeChild* groupChild = physicsLOD->GetChild(childIndex);
						if (groupChild->GetDamagedEntity())
						{
							phLevelNew::CompositeBoundSetBoundThreadSafe(*entryBound,childIndex,groupChild->GetDamagedEntity()->GetBound());
							Assert(groupChild->GetDamagedEntity()->GetBound() != NULL);
							// The entity's bound matrix will tell us how that bound is posed relative to its controlling bone.  In order to reflect any changes
							//   to the skeleton that the client might have made we will consult the current skeleton to see where that bone is (in world space),
							//   and then work backward from there.
							const crSkeleton *skeleton = GetSkeleton();
							Assert(skeleton != NULL);
							Mat34V globalMtx;
							skeleton->GetGlobalMtx(type->GetBoneIndexFromID(groupChild->GetBoneID()), globalMtx);

							Mat34V damagedMtx;
							Transform(damagedMtx, globalMtx, RCC_MAT34V(groupChild->GetDamagedEntity()->GetBoundMatrix()));
							UnTransformOrtho(damagedMtx, GetMatrix(), damagedMtx);

							entryBound->SetCurrentMatrix(childIndex, damagedMtx);
							entryBound->SetLastMatrix(childIndex, damagedMtx);
						}
						else
						{
							entryBound->SetBound(childIndex, NULL);
							removedChildren.Set(childIndex);
						}
					}
					PHSIM->GetContactMgr()->RemoveAllContactsWithInstance(this,removedChildren);

					entry->SetMass(entry->GetMass(hierInst));
					PHLEVEL->UpdateObjectLocationAndRadius(entryInst->GetLevelIndex(), (Mat34V_Ptr)(NULL));
				}
			}

			if (healthPrior > minDamage)
			{
				if (newHealth <= minDamage)
				{
					if (group->GetMadeOfGlass())
					{
						int glassCrackType = 0;
						fragManager::ComputeGlassCrackTypeCallbackFunctor computeCrackTypeCallback =
							FRAGMGR->GetComputeGlassCrackTypeCallback();

						if (computeCrackTypeCallback.IsBound())
						{
							glassCrackType = computeCrackTypeCallback();
						}
						entry->ShatterGlassOnBone(groupIndex, type->GetBoneIndexFromID(physicsLOD->GetChild(group->GetChildFragmentIndex())->GetBoneID()), position, relativeVelocity, glassCrackType);
					}

					if (group->GetMadeOfGlass() || group->GetDisappearsWhenDead())
					{
						HideDeadGroup(groupIndex, NULL);

#if ENABLE_FRAGMENT_EVENTS
						// Play death events
						if (applyEvents)
						{
							const evtSet* set = group->GetDeathEventset();
							if (set && set->GetNumInstances() > 0)
							{
								Assert(group->GetDeathEventPlayer());
								fragCollisionEventPlayer& player = *group->GetDeathEventPlayer();

								// 						player.GetPosition() = msg.position;	
								// 						player.GetNormal() = msg.normal;
								// 						player.GetRelativeVelocity() = msg.relativeVelocity;
								// 						player.GetRelativeSpeed() = msg.relativeSpeed;
								player.GetDamage() = healthPrior + 1.0f;
								player.GetNormalizedHealthPrior() = healthPrior;
								player.GetNormalizedHealthLeft() = newHealth;
								player.GetFragInst() = this;
								player.GetInst() = (phInst*)this;
								player.GetComponent() = group->GetChildFragmentIndex();
								player.GetPart() = groupIndex;
								// 						player.GetOtherInst() = msg.otherInst;
								// 						player.GetOtherComponent() = msg.otherComponent;
								// 						player.GetOtherPart() = msg.otherPart;
								// 						player.GetImpactIterator() = impacts ? *impacts : phContactIterator();

								player.StartUpdateStop();
							}
						}
#endif // ENABLE_FRAGMENT_EVENTS

						poseSkeleton = true;
					}
				}
			}
		}

		if (poseSkeleton)
		{
			// Pose skeletons to reflect new damaged parts
			PoseSkeletonFromArticulatedBody();
		}
	}
}

void fragInst::DamageGroupByComponent(int component FRAGMENT_EVENT_PARAM(bool applyEvents))
{
	Assert(component < GetTypePhysics()->GetNumChildren());

	fragTypeChild* child = GetTypePhysics()->GetChild(component);
	int groupIndex = child->GetOwnerGroupPointerIndex();
	fragTypeGroup* group = GetTypePhysics()->GetGroup(groupIndex);

	ReduceGroupDamageHealth(groupIndex, -group->GetDamageHealth(), Vec3V(V_ZERO), Vec3V(V_ZERO) FRAGMENT_EVENT_PARAM(applyEvents));
}

void fragInst::IncreaseGroupDamageHealth(int groupIndex, float newHealth)
{
	const fragType* type = GetType();
	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	if(!GetCached() && type->GetNeedsCacheEntryToActivate())
	{
		PutIntoCache();

		if (!GetCached())
		{
			// No cache entry was available
			return;
		}
	}

	if (!GetCached())
	{
		m_DamageHealth = newHealth;

		fragTypeChild* child = type->GetRootChild();
		if (!child)
		{
			child = physicsLOD->GetChild(0);
		}
		Assert(child != NULL || PHLEVEL->IsInactive(GetLevelIndex()));

		if (child)
		{
			int groupIndex = child->GetOwnerGroupPointerIndex();

			if (groupIndex != 0xFF)
			{
				fragTypeGroup *group = physicsLOD->GetGroup(groupIndex);

				if ((m_DamageHealth < 0) && (newHealth >= 0.0f))
				{
					// Set the bounds to damaged
					SetArchetype(physicsLOD->GetArchetype(false));
				}

				float minDamage = 0.0f;			   
				if (child->GetDamagedEntity())
				{
					minDamage = -group->GetDamageHealth();
				}

				if (m_DamageHealth <= minDamage && newHealth > minDamage)
				{
					if (PHSIM->IsSafeToDelete() &&
						(group->GetDisappearsWhenDead() || group->GetMadeOfGlass()))
					{
						// Undo DisappearsWhenDead, but only if we are here because we are not cachable
						if (physicsLOD->GetNumChildGroups() == 1)
						{
							PHSIM->AddInactiveObject(this);

							SetDead(false);
						}
					}
				}

				m_DamageHealth = Min(group->GetDamageHealth(), newHealth);
			}
		}
	}
	else
	{
		fragTypeGroup* group = physicsLOD->GetGroup(groupIndex);
		fragTypeChild* child = physicsLOD->GetChild(group->GetChildFragmentIndex());
		fragCacheEntry* entry = GetCacheEntry();
		fragHierarchyInst* hierInst = entry->GetHierInst();
		bool poseSkeleton = false;

		float minDamage = 0.0f;			   
		if (child->GetDamagedEntity())
		{
			minDamage = -group->GetDamageHealth();
		}

		if (PHSIM->IsSafeToDelete() && 
			(child->GetDamagedEntity() || group->GetDisappearsWhenDead() || group->GetMadeOfGlass()))
		{
			float healthPrior = hierInst->groupInsts[groupIndex].damageHealth;

			if ((healthPrior <= minDamage && newHealth > minDamage) ||
				(healthPrior <= 0.0f && newHealth > 0.0f))
			{
				poseSkeleton = true;

				for (int groupChildIndex = 0; groupChildIndex < group->GetNumChildren(); ++groupChildIndex)
				{
					int childIndex = group->GetChildFragmentIndex() + groupChildIndex;
					fragTypeChild* groupChild = physicsLOD->GetChild(childIndex);

					// The entity's bound matrix will tell us how that bound is posed relative to its controlling bone.  In order to reflect any changes
					//   to the skeleton that the client might have made we will consult the current skeleton to see where that bone is (in world space),
					//   and then work backward from there.
					const crSkeleton *skeleton = GetSkeleton();
					Assert(skeleton != NULL);
					Mat34V globalMtx;
					skeleton->GetGlobalMtx(type->GetBoneIndexFromID(groupChild->GetBoneID()), globalMtx);

					if (groupChild->GetDamagedEntity() && newHealth < 0.0f)
					{
						entry->GetBound()->SetBound(childIndex, groupChild->GetDamagedEntity()->GetBound());
						Assert(groupChild->GetDamagedEntity()->GetBound() != NULL);
						Mat34V damagedMtx;
						Transform(damagedMtx, globalMtx, RCC_MAT34V(groupChild->GetDamagedEntity()->GetBoundMatrix()));
						UnTransformOrtho(damagedMtx, GetMatrix(), damagedMtx);
						entry->GetBound()->SetCurrentMatrix(childIndex, damagedMtx);
						entry->GetBound()->SetLastMatrix(childIndex, damagedMtx);
					}
					else
					{
						entry->GetBound()->SetBound(childIndex, groupChild->GetUndamagedEntity()->GetBound());
						Assert(groupChild->GetUndamagedEntity()->GetBound() != NULL);
						Mat34V undamagedMtx;
						Transform(undamagedMtx, globalMtx, RCC_MAT34V(groupChild->GetUndamagedEntity()->GetBoundMatrix()));
						UnTransformOrtho(undamagedMtx, GetMatrix(), undamagedMtx);
						entry->GetBound()->SetCurrentMatrix(childIndex, undamagedMtx);
						entry->GetBound()->SetLastMatrix(childIndex, undamagedMtx);
					}
				}

				hierInst->groupBroken->Set(groupIndex, false);
				entry->UpdateIsFurtherBreakable();

				entry->SetMass(entry->GetMass(hierInst));
				PHLEVEL->UpdateObjectLocationAndRadius(entry->GetInst()->GetLevelIndex(), (Mat34V_Ptr)(NULL));
			}

			hierInst->groupInsts[groupIndex].damageHealth = Min(group->GetDamageHealth(), newHealth);
		}

		if (poseSkeleton)
		{
			// Pose skeletons to reflect new damaged parts
			PoseSkeletonFromArticulatedBody();
		}
	}
}

void fragInst::FixGroupByComponent(int component)
{
	Assert(component < GetTypePhysics()->GetNumChildren());

	fragTypeChild* child = GetTypePhysics()->GetChild(component);
	int groupIndex = child->GetOwnerGroupPointerIndex();
	fragTypeGroup* group = GetTypePhysics()->GetGroup(groupIndex);

	IncreaseGroupDamageHealth(groupIndex, group->GetDamageHealth());
}

void fragInst::SetGroupDamageHealth(int groupIndex, float newHealth)
{
	const fragType* type = GetType();
	if(!GetCached() && type->GetNeedsCacheEntryToActivate())
	{
		PutIntoCache();

		if (!GetCached())
		{
			// No cache entry was available
			return;
		}
	}

	float damageHealth = m_DamageHealth;

	if (GetCached())
	{
		fragCacheEntry* entry = GetCacheEntry();
		fragHierarchyInst* hierInst = entry->GetHierInst();
		damageHealth = hierInst->groupInsts[groupIndex].damageHealth;
	}

	if (newHealth > damageHealth)
	{
		IncreaseGroupDamageHealth(groupIndex, newHealth);
	}
	else if (newHealth < damageHealth)
	{
		ReduceGroupDamageHealth(groupIndex, newHealth, Vec3V(V_ZERO), Vec3V(V_ZERO) FRAGMENT_EVENT_PARAM(true));
	}
}

void fragInst::ShatterGlassOnBone(int boneIndex, Vec3V_In position, Vec3V_In impact, int glassCrackType)
{
	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	if (fragCacheEntry* entry = GetCacheEntry())
	{
		int groupIndex;
		for (groupIndex = 0; groupIndex < physicsLOD->GetNumChildGroups(); ++groupIndex)
		{
			fragTypeGroup& group = *physicsLOD->GetGroup(groupIndex);
			if (group.GetMadeOfGlass())
			{
				fragTypeChild& child = *physicsLOD->GetChild(group.GetChildFragmentIndex());

				int glassBoneIndex = GetType()->GetBoneIndexFromID(child.GetBoneID());
				if (glassBoneIndex == boneIndex)
				{
					break;
				}
			}
		}

		if (groupIndex < physicsLOD->GetNumChildGroups())
		{
			entry->ShatterGlassOnBone(groupIndex, boneIndex, position, impact, glassCrackType);
		}
	}
}

void fragInst::OpenLatchAbove(int component)
{
	if (FRAGCACHEMGR->CanGetNewCacheEntries(1))
	{
		if (component < GetTypePhysics()->GetNumChildren())
		{
			fragTypeGroup* group = GetTypePhysics()->GetGroup(GetTypePhysics()->GetChild(component)->GetOwnerGroupPointerIndex());

			while (group->GetLatchStrength() == 0.0f)
			{
				int groupIndex = group->GetParentGroupPointerIndex();

				if (groupIndex != 0xff)
				{
					group = GetTypePhysics()->GetGroup(groupIndex);
				}
				else
				{
					return;
				}
			}

			phBreakData findBreakInfo;
			reinterpret_cast<fragFindBreakInfo*>(&findBreakInfo)->breakGroup = GetTypePhysics()->GetChild(component)->GetOwnerGroupPointerIndex();
			reinterpret_cast<fragFindBreakInfo*>(&findBreakInfo)->breakLatch = true;

			BreakApart(findBreakInfo);
		}
	}
	else
	{
		NotifyCouldntGetCacheEntry();
	}
}


void fragInst::CloseLatchAbove(int component)
{
	if (fragCacheEntry* entry = GetCacheEntry())
	{
		int groupIndex = GetTypePhysics()->GetChild(component)->GetOwnerGroupPointerIndex();
		fragTypeGroup* group = GetTypePhysics()->GetGroup(groupIndex);

		if (Verifyf(group->GetLatchStrength() != 0, "Can't latch child %i/group (%i, %s) on %s.",component,groupIndex,group->GetDebugName(),GetArchetype()->GetFilename()) &&
			Verifyf(GetType()->WillBeArticulated(groupIndex),"Can't latch non-articulated child %i/group (%i, %s) on %s.",component,groupIndex,group->GetDebugName(),GetArchetype()->GetFilename()))
		{
			fragHierarchyInst* hierInst = entry->GetHierInst();
			phArticulatedCollider* collider = hierInst->articulatedCollider;
			int linkIndex = collider->GetLinkFromComponent(component);
			
			// Don't crash if the user supplies a valid component that can't be latched. 
			if (Verifyf(hierInst->latchedJoints, "Trying to latch a joint on a cache entry %s that has no initially latched joints.",GetArchetype()->GetFilename()) && 
				Verifyf(hierInst->latchedJoints->IsClear(groupIndex), "Trying to latch already latched child %i/group %i on %s.",component,groupIndex,GetArchetype()->GetFilename()))
			{
				collider->GetBody()->EnsureVelocitiesFullyPropagated();

				if(linkIndex != 0) // The link could be removed as the component had been broken off ealier, we just need to reset the latch bitset if that's the case
				{
					// Modify the mass of the root link
					ScalarV oldMass = hierInst->body->GetMass(0);
					phArticulatedBodyPart* newLink = &hierInst->body->GetLink(linkIndex);
					hierInst->body->SetMassOnly(0, oldMass + hierInst->body->GetMass(linkIndex));

					phJoint* joint = &hierInst->body->GetJoint(linkIndex - 1);

					// need to remove the link from the collider Before we remove it from the body, or it will assert
					collider->RemoveLink(linkIndex);

					hierInst->body->RemoveChild(linkIndex);

					// Delete the link and joint now that body doesn't own them
					entry->DeleteLink(newLink);
					entry->DeleteJoint(joint);
				}

				entry->UpdateSavedVelocityArraySizes();
				collider->GetBody()->ResetPropagationVelocities();
#if __ASSERT
				entry->CheckVelocities();
#endif

				PS3_ONLY(entry->ReloadDmaPlan());
				hierInst->articulatedCollider->FindJointLimitDofs(); 

				int parentChild = GetTypePhysics()->GetGroup(group->GetParentGroupPointerIndex())->GetChildFragmentIndex();
				Matrix34 latchedMatrix;
				GetTypePhysics()->ComputeInstanceFromLinkMatrix(parentChild,RC_MAT34V(latchedMatrix));
				entry->SetLinkAttachmentRecurseChildren(*group, latchedMatrix);

				// Mark this group as latched
				hierInst->latchedJoints->Set(groupIndex);
			}
		}
	}
}

fragInst* fragInst::GetParent()
{
    if (fragCacheEntry* entry = GetCacheEntry())
    {
		//if (entry->GetHierInst()->parentInst.IsValid()) <== should be this, but we need CL5079214 from RDR before we have IsValid
		if (entry->GetHierInst()->parentInst.GetLevelIndexAndGenerationId() != 0xffffffff)
		{
			return static_cast<fragInst*>(PHLEVEL->GetInstance(entry->GetHierInst()->parentInst));
		}
    }

    return this;
}

PARAM(nofragbreak,"[fragment] Don't allow fragment objects to break");

// Using the impulses that would be applied if the object had no breakability, fill out a phBreakData (actually a fragFindBreakInfo) telling what kind of
//   breaking would occur.
bool fragInst::FindBreakStrength(const Vector3 * XENON_ONLY(RESTRICT) componentImpulses,
								 const Vector4 * XENON_ONLY(RESTRICT) componentPositions,
	float* breakStrength,
	phBreakData* applyData) const
{
	if (PARAM_nofragbreak.Get())
	{
		return false;
	}

	const fragType* type = GetType();
	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	Assert(type);

	//Have we been tuned unbreakable (or do we have a per instance unbreakable flag
	//override)
	if (IsBreakingDisabled())
	{
		return false;
	}


	fragFindBreakInfo* findBreakInfo = reinterpret_cast<fragFindBreakInfo*>(applyData->BreakData);

	/*
	only allow a group to break off at the root iff the number of valid children it has is less than the 
	number of total active fragments.  This will allow a groups to break off from the root until the last one, 
	which would just waste resources to break off from the root...
	*/

	/*
	if not broken, call RootBreakingForce and see if it will break.  If it will, then it always breaks first.
	Store that info.

	Else, call ImpactBreakModifyInst, storing off any rage_new info into applyData.  Find the weakest and if there is 
	a break, return the breaking force and wait for the call to break, else modify local data and return false.
	*/

	// If we are a 'simple' fragment then our job is much simpler than otherwise, so we special-case it here.
	if (!GetCached() && !type->GetNeedsCacheEntryToActivate())
	{
		//we're a simple fragment, so we will break into ourselves...
		findBreakInfo->breakGroup = 0xFF;

		Assert(!GetBroken());

		if (physicsLOD->GetMinMoveForce() >= 0.0f)
		{
			// Determine the amount of breaking impulse reaching the root.  Because we're a simple fragment (one group) this is pretty straightforward
			//   to do - simply add up the magnitudes of all the impulses.
			float magTotal = 0.0f;
			for (int part = 0; part < static_cast<phBoundComposite*>(GetArchetype()->GetBound())->GetNumBounds(); ++part)
			{
				magTotal += componentImpulses[part].Mag();
				findBreakInfo->impulse = VECTOR3_TO_VEC3V(componentImpulses[part]);
				findBreakInfo->position = VECTOR4_TO_VEC4V(componentPositions[part]);
			}

			if (fragTune::IsInstantiated())
			{
				magTotal *= FRAGTUNE->GetGlobalRootBreakHealthScale();
			}

			if(magTotal >= physicsLOD->GetMinMoveForce())
			{
				// We will break as a result of these impulses.
				*breakStrength = physicsLOD->GetMinMoveForce() / magTotal;

				if (PARAM_FRAGDEBUG_1_GET)
				{
					Displayf(TGreen"*** Root breaking force %f exceeds MinMoveForce %f, broken off!", magTotal, physicsLOD->GetMinMoveForce());
				}

				return true;
			}
			else
			{
				if (PARAM_FRAGDEBUG_2_GET)
				{
					Displayf(TGreen"Root breaking force %f less than MinMoveForce %f, no root break", magTotal, physicsLOD->GetMinMoveForce());
				}
			}
		}

		// No break will occur.  
		return false;
	}

	// If we got here, it means that we're a complex fragment...
	fragCacheEntry*	curEntry = GetCacheEntry();
	fragHierarchyInst* hierInst = curEntry != NULL ? curEntry->GetHierInst() : NULL;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Check to ensure a few things are in order before we go on with further breaking calculations.

	// I could be more precise about how many active objects we really need but I decided it wouldn't be worth the
	//   extra cost of actually checking the active state of this object (which requires a pointer dereference).
	if (!PHLEVEL->CheckAddActiveObjects(2))
	{
		return false;
	}

	// Before we do any more, ensure that we have enough room for however many entries we need.
	if(curEntry != NULL)
	{
		// We don't want this entry to be considered as 'reclaimable' to the CanGetNewCacheEntries() call below.
		curEntry->SetSequenceNumber();
	}
	const u32 kNumNewCacheEntriesNeeded = curEntry == NULL ? 2 : 1;
	if (FRAGCACHEMGR->CanGetNewCacheEntries(kNumNewCacheEntriesNeeded) < kNumNewCacheEntriesNeeded)
	{
		NotifyCouldntGetCacheEntry();
	}
	//
	////////////////////////////////////////////////////////////////////////////////////////////////////

	if(FRAGMGR->BreakingPreventedBecauseOfFramerate())
	{
		return false;
	}

	// Make sure findBreakInfo->tempBreakDamage is zeroed out.
	Assert(phInstBreakable::BREAK_DATA_MAX_NUM_BYTES >= sizeof(f32) * physicsLOD->GetNumChildGroups() * 2);
	memset(findBreakInfo->tempBreakDamage, 0x00, sizeof(f32) * physicsLOD->GetNumChildGroups() * 2);

	// Go through all of the impulses for each component and submit the magnitudes that correspond to breakable groups to PropagateAndAccumulateBreakingImpulse to fill out
	//   findBreakInfo->tempBreakDamage and find out how much breaking impulse each breakable part is experiencing.
	float magTotal = 0.0f;
	const int numChildren = physicsLOD->GetNumChildren();
	Assert(numChildren <= static_cast<phBoundComposite*>(GetArchetype()->GetBound())->GetNumBounds());

	for (int childIndex = 0; childIndex < numChildren; ++childIndex)
	{
		if (componentImpulses[childIndex].IsNonZero())
		{
			float magnitude = componentImpulses[childIndex].Mag();
			findBreakInfo->impulse = VECTOR3_TO_VEC3V(componentImpulses[childIndex]);
			findBreakInfo->position = VECTOR4_TO_VEC4V(componentPositions[childIndex]);
			// We shouldn't have impulses for bound components in groups that don't exist ... maybe this can happen when disappears when dead is set on
			//   a group?
			int groupIndex = physicsLOD->GetChild(childIndex)->GetOwnerGroupPointerIndex();
			if(hierInst == NULL || hierInst->groupBroken->IsClear(groupIndex))
			{
				if (fragTune::IsInstantiated())
				{
					magnitude *= FRAGTUNE->GetGlobalBreakHealthScale();
				}

				magTotal += magnitude;

				// Send this breaking impulse magnitude up and down the hierarchy.
				u8 groupIndex = physicsLOD->GetChild(childIndex)->GetOwnerGroupPointerIndex();
				PropagateAndAccumulateBreakingImpulse(magnitude, findBreakInfo->tempBreakDamage, groupIndex, groupIndex);
			}
		}
	}

	// Look for the possibility of a root break.  For some reason we always take the root break if there is one even if another break would have been
	//   achieved more easily.
	if ((!GetBroken() || (curEntry && curEntry->GetHierInst()->body && curEntry->GetHierInst()->body->RootIsFixed())) &&
		physicsLOD->GetMinMoveForce() >= 0.0f)
	{
		const fragTypeGroup *rootGroup = physicsLOD->GetGroup(0);
		const float rootBreakingForce = findBreakInfo->tempBreakDamage[0].tempBreakHealth * rootGroup->GetForceTransmissionScaleUp();

		if(rootBreakingForce >= physicsLOD->GetMinMoveForce())
		{
			findBreakInfo->breakGroup = 0xFF;
			*breakStrength = physicsLOD->GetMinMoveForce() / rootBreakingForce;

			return true;
		}
	}

	// Find out if this instance has a single root and that root has already broken off.  It it does, we don't want to allow that group to be
	//   the group that breaks off (it doesn't make sense to break free if you're already free and it prevents legitimate breaks that could happen).
	int loneBrokenRootGroupIndex = 0xFF;
	if(GetBroken())		// No point in looking if nothing has broken free.
	{
		// Without a cache entry (and likewise a hierInst) this is pretty easy to check.
		if(hierInst == NULL && physicsLOD->GetRootGroupCount() == 1)
		{
			Assert(!GetPartBroken());
			loneBrokenRootGroupIndex = 0;
		}
		else
		{
			const int numGroups = physicsLOD->GetNumChildGroups();
			const int numRootGroups = physicsLOD->GetRootGroupCount();
			int groupUpperLimit = numGroups;
			for(int groupIndex = 0; groupIndex < groupUpperLimit; ++groupIndex)
			{
				if(hierInst->groupBroken->IsClear(groupIndex))
				{
					// This group is still present, is it one of the (possibly multiple) root groups of the type?
					if(groupIndex < physicsLOD->GetRootGroupCount())
					{
						// We're one of the root groups in the type and we're potentially a lone broken root group.
						if(loneBrokenRootGroupIndex == 0xFF)
						{
							// This is the first time we've encountered a broken root group ...
							loneBrokenRootGroupIndex = groupIndex;
							groupUpperLimit = numRootGroups;
						}
						else
						{
							// Doh, this is the second time we've found a broken root group so there isn't a *lone* broken root group.
							loneBrokenRootGroupIndex = 0xFF;
							break;
						}
					}
					else
					{
						// This is not a root group of the type, but it may be the root group of a broken off piece.  Fortunately there can only
						//   be one of these.
						const fragTypeGroup *curGroup = physicsLOD->GetGroup(groupIndex);
						Assert(curGroup != NULL);
						const int parentGroupIndex = curGroup->GetParentGroupPointerIndex();
						Assert(parentGroupIndex != 0xFF);
						if(hierInst->groupBroken->IsSet(parentGroupIndex))
						{
							loneBrokenRootGroupIndex = groupIndex;
							break;
						}
					}
				}
			}
		}
	}

	// Find out if the root has a latch that could potentially be broken.
	int limitDof;
	Vector3 constrainDir, rotMin, rotMax;
	const bool rootLinkConstraint  = type->GetRootLinkConstraint(RCC_MATRIX34(GetMatrix()), limitDof, constrainDir, rotMin, rotMax);
	const bool rootLatchBreakable = hierInst && rootLinkConstraint && !hierInst->rootLatchBroken;

	bool mightHaveLatches = type->GetHasAnyArticulatedParts();
	bool checkLatchGroups = curEntry && curEntry->GetHierInst()->latchedJoints;

	u8 firstBrokenIndex = 0xFF;
	f32 biggestRatio = 0.0f;
	bool biggestRatioIsLatch = false;
	// Check each group to see which, if any, would be the first to break.
	int numChildGroups = physicsLOD->GetNumChildGroups();
	for (int group = 0; group < numChildGroups; ++group)
	{
		const fragTypeGroup *curGroup = physicsLOD->GetGroup(group);
		FastAssert(curGroup != NULL);

		// Only check those groups that exist and have a non-zero breaking health (which is poorly named but refers to the amount of breaking impulse that
		//   was applied to that group) and a non-negative breaking strength (meaning that they're tuned to be breakable).
		// TODO: An improvement would be to check if the strength is >= 0.0f and the health health >= the strength.  This would save some extra trips into
		//   the loop which would save some divisions and some comparisons (no need to check if the breakRatio is >= 1.0f).
		if (findBreakInfo->tempBreakDamage[group].tempBreakHealth > 0.0f && (curGroup->GetStrength() >= 0.0f || curGroup->GetLatchStrength()>=0.0f))
        {
			// If we had breaking impulse propagated to use we'd better exist!  If this fails, then there's probably a problem with ImpactModifyBreakInst().
			Assert(hierInst == NULL || hierInst->groupBroken->IsClear(group));

			float breakRatio = 0.0f;
			if(group != loneBrokenRootGroupIndex && IsGroupBreakable(group))
			{
				float strength = curGroup->GetStrength();
				if (strength > SMALL_FLOAT)
				{
					float strength = physicsLOD->GetGroup(group)->GetStrength();
					if (strength > SMALL_FLOAT)
					{
						breakRatio = findBreakInfo->tempBreakDamage[group].tempBreakHealth / strength;
					}
					else if (strength < 0.0f)
					{
						breakRatio = 0.0f;
					}
				}
				else if (strength >= 0.0f)
				{
					breakRatio = FLT_MAX;
				}
			}
#if HACK_GTA4	// NEEDS_RAGE_VERIFY - Justin
				// This was a change added to the Gta branch by me (sandy) with the comment
				// "Additional fix for articulated fragments breaking into new articulated parts."
				// Not long after Justin made a change to stop articulated fragments breaking into new articulated parts
				//
				// Need to check with Justin if the change should be brought across to rage\dev, or ditched
				else
				{
					breakRatio = 0.0f;
				}
#endif

			float latchBreakRatio = 0.0f;
			
			// If we have a latched joint bitset allocated, only attempt to unlatch if this group is marked as latched. 
			// If we don't have a latched joint bitset we might be in simplified collider mode, in that case check that the group will be
			//   articulated and is initially latched. Otherwise we might get stuck always trying to unlatch a group that can't be unlatched. 
			if ((rootLatchBreakable && group == 0) || 
				(mightHaveLatches && ((checkLatchGroups && hierInst->latchedJoints->IsSet(group)) || (!checkLatchGroups && curGroup->IsInitiallyLatched() && type->WillBeArticulated(group)))))
			{
				float latchStrength = curGroup->GetLatchStrength();
				if (latchStrength > SMALL_FLOAT)
				{
					latchBreakRatio = findBreakInfo->tempBreakDamage[group].tempBreakHealth / latchStrength;
				}
				else if (latchStrength >= 0.0f)
				{
					latchBreakRatio = FLT_MAX;
				}
			}

			if (PARAM_FRAGDEBUG_2_GET && (curEntry == NULL || hierInst->groupBroken->IsClear(group)))
			{
				Displayf(TRed"fragObject %s break point %d (str %f) got impulse %f",
					GetType()->GetTuneName(),
					group,
				curGroup->GetStrength(),
					findBreakInfo->tempBreakDamage[group].tempBreakHealth);
			}

			if (latchBreakRatio > 1.0f)
			{
				//this latch broke, but did it break first?
				if (latchBreakRatio > biggestRatio)
				{
					//first so far...
					firstBrokenIndex = static_cast<u8>(group);
					biggestRatio = breakRatio;
					biggestRatioIsLatch = true;
				}
			}

			if (breakRatio > 1.0f)
			{
				//this group broke, but did it break first?
				if (breakRatio > biggestRatio)
				{
					//first so far...
					firstBrokenIndex = static_cast<u8>(group);
					biggestRatio = breakRatio;
					biggestRatioIsLatch = false;
				}
			}
		}
	}

	//see if we broke somewhere...
	if (firstBrokenIndex != 0xFF)
	{
		if (PARAM_FRAGDEBUG_1_GET)
		{
			if (biggestRatioIsLatch)
			{
				Displayf(TRed"*** fragObject %s latch %d broken (ratio %f)!",
					GetType()->GetTuneName(),
					firstBrokenIndex,
					biggestRatio);
			}
			else
			{
				Displayf(TRed"*** fragObject %s break point %d broken off (ratio %f)!",
					GetType()->GetTuneName(),
					firstBrokenIndex,
					biggestRatio);
			}
		}

		if (biggestRatioIsLatch)
		{
			*breakStrength = physicsLOD->GetGroup(firstBrokenIndex)->GetLatchStrength() / magTotal;
		}
		else
		{
			*breakStrength = physicsLOD->GetGroup(firstBrokenIndex)->GetStrength() / magTotal;
		}

		const fragTypeGroup* breakingGroup = physicsLOD->GetGroup(firstBrokenIndex);
		const int parentGroupIndex = breakingGroup->GetParentGroupPointerIndex();
		Assert((parentGroupIndex == 0xFF) == firstBrokenIndex < physicsLOD->GetRootGroupCount());
		const bool isRootGroup = (parentGroupIndex == 0xFF);
		bool loneRootGroup = isRootGroup;
		if(isRootGroup && physicsLOD->GetRootGroupCount() > 1)
		{
			// Let's see if we're the last root group around.
			int survivingRootGroupCount = 0;
			for(int curGroupIndex = physicsLOD->GetRootGroupCount() - 1; survivingRootGroupCount < 2 && curGroupIndex >= 0; --curGroupIndex)
			{
				survivingRootGroupCount += (hierInst == NULL || hierInst->groupBroken->IsClear(curGroupIndex)) ? 1 : 0;
			}
			loneRootGroup = survivingRootGroupCount < 2;
		}

		// If the only remaining group is trying to break off from its parent, force this to become a root break instead.
		if (loneRootGroup && !breakingGroup->GetMadeOfGlass())
		{
			firstBrokenIndex = 0xFF;
		}

		findBreakInfo->breakLatch = biggestRatioIsLatch;
		findBreakInfo->breakGroup = firstBrokenIndex;
		return true;
	}

	if (PARAM_FRAGDEBUG_2_GET)
	{
		Displayf(TRed"(No breaks, though)");
	}

	//no break...
	return false;
}

bool fragInst::ShouldFindImpacts(const phInst* otherInst) const
{
	// Set the sequence number so that the cache entry isn't stolen in the middle of the physics update.
	if (fragCacheEntry* pCacheEntry = GetCacheEntry())
	{
		pCacheEntry->SetSequenceNumber();

		bool isFixedRoot = pCacheEntry->HasFixedArticulatedRoot();
		// Optimization sanity check
		Assert(isFixedRoot == (pCacheEntry->GetHierInst()->body && pCacheEntry->GetHierInst()->body->RootIsFixed() && GetTypePhysics()->GetMinMoveForce() != 0.0f));

		if (isFixedRoot)
		{
			if (PHLEVEL->IsFixed(otherInst->GetLevelIndex()))
			{
				return false;
			}
		}
	}
	return phInstBreakable::ShouldFindImpacts(otherInst);
}

void fragInst::PreComputeImpacts(phContactIterator impacts)
{
	if (GetCached())
	{
		GetCacheEntry()->SetSequenceNumber();
	}

    phInstBreakable::PreComputeImpacts(impacts);
}

void fragInst::NotifyImpulse(const Vector3& impulse, const Vector3& position, int component, int UNUSED_PARAM(element), float breakScale)
{
	// Check was added to fix a crash in gta4 when driving into a fence and smashing it apart
	// I believe the fence did have bDissapearsWhenDead on some of the groups, so might have been related to that
	if(!IsInLevel())
		return;

	Assert(GetArchetype() != NULL);

	// We allow impulses to be applied to a component that's not present but we certainly don't want to try and apply damage to a group that's not around any more.
	const phBoundComposite *compositeBound = reinterpret_cast<const phBoundComposite *>(GetArchetype()->GetBound());
	if(component >= compositeBound->GetMaxNumBounds())
	{
		return;
	}
	if(compositeBound->GetBound(component) == NULL)
	{
		return;
	}

    Collision collisionMsg;
    collisionMsg.position = RCC_VEC3V(position);
    collisionMsg.component = component;
    collisionMsg.otherComponent = 0;
    collisionMsg.part = 0;
    collisionMsg.otherPart = 0;

	collisionMsg.relativeVelocity = Vec3V(V_ZERO);
	collisionMsg.relativeSpeed = 0.0f;

    Vector3 otherLocalVelocity(ORIGIN);

    collisionMsg.normal = Normalize(RCC_VEC3V(impulse));

	// This code appears to have disappeared from rage, so seems like CollisionDamage will never get called correctly
	// Looks like a rage bug and should be put back into head of rage\dev
	//
	// Without this HACK_GTA4 code the relative speed will be zero and no damage will be done.

	// Gather the relative speed and relative velocity, into the surface, at the point of contact.
    collisionMsg.relativeVelocity = VECTOR3_TO_VEC3V(impulse);
    collisionMsg.relativeSpeed = impulse.Mag();

#if __DEBUGLOG
	if (PHSIM->GetCollider(GetLevelIndex()))
	{
		PHSIM->GetCollider(GetLevelIndex())->DebugReplay();
	}

	diagDebugLog(diagDebugLogPhysics, 'fIim', &impulse);
	diagDebugLog(diagDebugLogPhysics, 'fIpo', &position);
	diagDebugLog(diagDebugLogPhysics, 'fIco', &component);
#endif

    collisionMsg.damage = impulse.Mag() * breakScale;
    collisionMsg.otherInst = NULL;

    CollisionDamage(collisionMsg, NULL);
    
#if HACK_GTA4
    // CollisionDamage can remove us from the level, so do this check again
    if(!IsInLevel())
		return;
#endif
    
}

void fragInst::NotifyForce(const Vector3& force, const Vector3& position, int component, int element, float breakScale)
{
	Vector3 impulse(force);
	impulse.Scale(DEFAULT_FRAME_TIME);

	NotifyImpulse(impulse, position, component, element, breakScale);
}

// returns true if the instance is still alive
bool fragInst::HideDeadGroup(int groupIndex, phContactIterator *impacts)
{
	fragTypeGroup *group = GetTypePhysics()->GetGroup(groupIndex);
	fragCacheEntry* entry = GetCacheEntry();
	Assert(entry);
	fragHierarchyInst* hierInst = entry->GetHierInst();

	const int parentGroupIndex = group->GetParentGroupPointerIndex();
	const bool isRootGroup = (parentGroupIndex == 0xFF);
	const bool parentGroupIsPresent = !isRootGroup && hierInst->groupBroken->IsClear(parentGroupIndex);

	// Determine if this group is the root of the existing fragment tree
	bool entireTreeUnderGroup = !parentGroupIsPresent || isRootGroup;
	for (int rootGroupIndex = 0; rootGroupIndex < GetTypePhysics()->GetRootGroupCount(); ++rootGroupIndex)
	{
		if(rootGroupIndex != groupIndex && !GetGroupBroken(rootGroupIndex))
		{
			// There is another root group that hasn't been broken so our group can't be the root
			entireTreeUnderGroup = false;
			break;
		}
	}

	if(entireTreeUnderGroup && group->GetNumChildGroups() == 1)
	{
		// We can delete this group and keep the structure of the entire tree
		// If we don't break the child groups off we need to at least set it as broken so it's not floating
		if(!GetBroken())
		{
			SetBroken(true);
			if (PHLEVEL->IsInactive(GetLevelIndex()))
			{
				PHSIM->ActivateObject(this);
			}
		}
	}
	else
	{
		// This group is going to be disappearing so lets break off any child groups that might still be in existence.
		for(int childGroupOffset = 0; childGroupOffset < group->GetNumChildGroups(); ++childGroupOffset)
		{
			int childGroupIndex = group->GetChildGroupsPointersIndex() + childGroupOffset;
			if(hierInst->groupBroken->IsClear(childGroupIndex))
			{
				// If we can't break the object, just delete it
				if(!IsBreakable(NULL) || BreakOffAboveGroup(childGroupIndex) == NULL)
				{
					DeleteAboveGroup(childGroupIndex);
				}
			}

			fragAssert(hierInst->groupBroken->IsSet(childGroupIndex));
		}
	}


	// We need to do this after we do the breaking above because the breaking won't happen if the parent is already cleared out.
	hierInst->groupBroken->Set(groupIndex, true);
	entry->UpdateIsFurtherBreakable();

	Assert(GetArchetype()->GetBound() == entry->GetBound());
	ComponentBits removedChildren;
	for (int childIndex = 0; childIndex < group->GetNumChildren(); ++childIndex)
	{
		const int componentIndex = childIndex + group->GetChildFragmentIndex();
		entry->GetBound()->SetBound(componentIndex, NULL);
		removedChildren.Set(componentIndex);
	}
	PHSIM->GetContactMgr()->RemoveAllContactsWithInstance(this,removedChildren);

	Assert(entry->GetInst() == this);

	const float newMass = entry->GetMass(hierInst);
	if(newMass > 0.0f)
	{
		entry->SetMass(newMass);
		PHLEVEL->UpdateObjectLocationAndRadius(entry->GetInst()->GetLevelIndex(), (Mat34V_Ptr)(NULL));
	}
	else
	{
		if(impacts != NULL)
		{
			impacts->Reset();
			for(; !impacts->AtEnd(); ++(*impacts))
			{
				impacts->DisableImpact();
			}
		}
		PHSIM->DeleteObject(entry->GetInst()->GetLevelIndex());
		SetDead();
		return false;
	}
	return true;
}

void fragInst::ApplyDamage(int component, float mag, Vec3V_In position, Vec3V_In relativeVelocity, float &healthPrior, float &healthLeft FRAGMENT_EVENT_PARAM(bool bApplyEvents))
{
	const fragType* type = GetType();

	// It's valid to have a component index that's greater than the
	// number of children - mostly in case of cloth. So early out here without
	// asserting. /MAK
	if (component >= GetTypePhysics()->GetNumChildren())
	{
		return;
	}

	if (fragTune::IsInstantiated())
	{
		mag *= FRAGTUNE->GetGlobalDamageHealthScale();
	}

	// If we're going to take damage and we're the kind of instance that needs a cache entry we'd better make sure we get one so that things work properly.
	// Under Bullet, it seems that the only way this could happen is if we ran out of cache entries because we would have already activated by this point if it
	//   it was possible.  Without Bullet, activation comes later in the process and so this is very important.
	if(!GetCached() && type->GetNeedsCacheEntryToActivate())
	{
		PutIntoCache();

		if (!GetCached())
		{
			// No cache entry was available
			return;
		}
	}

	if (!GetCached())
	{
		// Usually we must be a simple fragment to get here, but it's possible that we're not a simple fragment but we couldn't be put into the
		//   cache because we couldn't get activated (not enough active objects) or we ran out of cache entries.
		fragTypeChild* child = type->GetRootChild();
		if (!child)
		{
			child = GetTypePhysics()->GetChild(0);
		}
		Assert(child != NULL || PHLEVEL->IsInactive(GetLevelIndex()));
		int groupIndex = child->GetOwnerGroupPointerIndex();

        if (groupIndex != 0xFF)
        {
		    fragTypeGroup *group = GetTypePhysics()->GetGroup(groupIndex);
		    if(child != NULL)
		    {
			    healthPrior = healthLeft = m_DamageHealth;

			    if (mag > group->GetMinDamageForce())
			    {
				    //mag /= group->GetDamageHealth();

				    if (PARAM_FRAGDEBUG_1_GET && m_DamageHealth >= 0.0f)
				    {
					    Displayf(TBlue"fragObject damage health was %f, lost %f...",
						    m_DamageHealth,
						    mag);

					    if (mag > m_DamageHealth)
					    {
						    Displayf(TBlue"   ...and became damaged!");
					    }
					    else
					    {
						    Displayf(TBlue"   ...but remains undamaged");
					    }
				    }

					ReduceGroupDamageHealth(groupIndex, m_DamageHealth - mag, position, relativeVelocity FRAGMENT_EVENT_PARAM(bApplyEvents));

				    healthLeft = m_DamageHealth;
                }
			}
		}
	}
	else
	{
		Assert(component < GetTypePhysics()->GetNumChildren());

		fragCacheEntry* entry = GetCacheEntry();
		fragHierarchyInst* hierInst = entry->GetHierInst();
		fragTypeChild* child = GetTypePhysics()->GetChild(component);

		int groupIndex = child->GetOwnerGroupPointerIndex();
		fragTypeGroup *group = GetTypePhysics()->GetGroup(groupIndex);
		healthPrior = healthLeft = hierInst->groupInsts[groupIndex].damageHealth;

		if (mag > group->GetMinDamageForce())
		{
			//mag /= group->GetDamageHealth();
			if (PARAM_FRAGDEBUG_1_GET && healthLeft >= 0.0f)
			{
				Displayf(TBlue"fragObject part %d damage health was %f, lost %f...",
					component,
					healthLeft,
					mag);

				if (mag > healthLeft)
				{
					Displayf(TBlue"   ...and became damaged!");
				}
				else
				{
					Displayf(TBlue"   ...but remains undamaged");
				}
			}

			ReduceGroupDamageHealth(groupIndex, healthPrior - mag, position, relativeVelocity FRAGMENT_EVENT_PARAM(bApplyEvents));
		}
	}
}

void fragInst::CollisionDamage(const Collision& msg, phContactIterator * FRAGMENT_EVENT_ONLY(impacts))
{
	float healthPrior = 1.0f;
	float healthLeft = 1.0f;
	ApplyDamage(msg.component, msg.damage, msg.position, msg.relativeVelocity, healthPrior, healthLeft FRAGMENT_EVENT_PARAM(msg.bApplyEvents));

#if ENABLE_FRAGMENT_EVENTS
	if (msg.bApplyEvents)
	{
		const fragType* type = GetType();
		const evtSet* set = type->GetCollisionEventset();
		if (set && set->GetNumInstances() > 0)
		{
			Assert(type->GetCollisionEventPlayer());
			fragCollisionEventPlayer& player = *type->GetCollisionEventPlayer();

			player.GetPosition() = msg.position;	
			player.GetNormal() = msg.normal;
			player.GetRelativeVelocity() = msg.relativeVelocity;
			player.GetRelativeSpeed() = msg.relativeSpeed;
			player.GetDamage() = msg.damage;
			player.GetNormalizedHealthPrior() = healthPrior;
			player.GetNormalizedHealthLeft() = healthLeft;
			player.GetFragInst() = this;
			player.GetInst() = (phInst*)this;
			player.GetComponent() = msg.component;
			player.GetPart() = msg.part;
			player.GetOtherInst() = msg.otherInst;
			player.GetOtherComponent() = msg.otherComponent;
			player.GetOtherPart() = msg.otherPart;
            player.GetImpactIterator() = impacts ? *impacts : phContactIterator();

			player.StartUpdateStop();
		}
	}
#endif // ENABLE_FRAGMENT_EVENTS
}

void fragInst::CheckSimpleColliderMode()
{ 
	phArticulatedCollider *collider = GetCacheEntry() && GetCacheEntry()->GetHierInst() && 
		GetCacheEntry()->GetHierInst()->articulatedCollider ? GetCacheEntry()->GetHierInst()->articulatedCollider : NULL;
	if (collider)
	{
		if (collider->GetDenyColliderSimplificationFrames() == 0)
		{
			if (FRAGTUNE->GetAllowSimplifiedColliders() &&
				collider->IsArticulated() &&
				collider->GetBody()->GetNumBodyParts() == 1 &&
				!collider->GetBody()->RootIsFixed())
			{
				collider->SetType(phCollider::TYPE_RIGID_BODY);
				// We're switching from articulated to rigid which uses a different contact format - let's clear out those contacts just to be safe.
				PHSIM->GetContactMgr()->ResetWarmStartAllContactsWithInstance(this);
#if __PS3
				GetCacheEntry()->ReloadDmaPlan();
#endif
			}

#if __BANK
			// If simplified colliders have been disabled - switch back to articulated
			if (!FRAGTUNE->GetAllowSimplifiedColliders() && collider->CanBeArticulated() && !collider->IsArticulated())
			{
				collider->SetType(phCollider::TYPE_ARTICULATED_BODY);

				// Reset the root CG offset now that the collider is articulated. We can't fully support CG offsets different than the type offset. 
				// At the moment, an offset articulated CG will mean the bounds attached to the root also get offset. 
				GetArchetype()->GetBound()->SetCGOffset(VECTOR3_TO_VEC3V(GetTypePhysics()->GetRootCGOffset()));
				collider->SetColliderMatrixFromInstance();

				// We're switching from articulated to rigid which uses a different contact format - let's clear out those contacts just to be safe.
				PHSIM->GetContactMgr()->ResetWarmStartAllContactsWithInstance(this);

				// This collider is articulated, so add it as a self overlapping pair the physics level's broadphase culler.
				// The physics level's ActivateObject above adds broadphase overlapping pairs, but not self-overlapping pairs for articualted bodies.
				PHLEVEL->GetBroadPhase()->addOverlappingPair((u16)GetLevelIndex(),(u16)GetLevelIndex());
#if __PS3
				GetCacheEntry()->ReloadDmaPlan();
#endif
				collider->FindJointLimitDofs();
			}
#endif
		}
		else
		{
			collider->SetDenyColliderSimplificationFrames(collider->GetDenyColliderSimplificationFrames()-1);
		}
	}
}

void fragInst::ForceArticulatedColliderMode()
{
	if (fragCacheEntry* entry = GetCacheEntry())
	{
		// If already articulated, no need to do anything else
		if (entry->GetHierInst() && entry->GetHierInst()->articulatedCollider && entry->GetHierInst()->articulatedCollider->IsArticulated())
			return;

		entry->LazyArticulatedInit();
	
		fragHierarchyInst* hierInst = entry->GetHierInst();
		phArticulatedCollider* collider = hierInst->articulatedCollider;

		if (collider && !collider->IsArticulated() && collider->CanBeArticulated() && Verifyf(entry->GetHierInst()->body, "fragCacheEntry::LazyArticulated did not create a body. fragInst::ForceArticulatedColliderMode will not be able to make this collider articulated."))
		{
			collider->SetType(phCollider::TYPE_ARTICULATED_BODY);

			// Reset the root CG offset now that the collider is articulated. We can't fully support CG offsets different than the type offset. 
			// At the moment, an offset articulated CG will mean the bounds attached to the root also get offset. 
			GetArchetype()->GetBound()->SetCGOffset(VECTOR3_TO_VEC3V(GetTypePhysics()->GetRootCGOffset()));
			collider->SetColliderMatrixFromInstance();
			
			// We're switching from articulated to rigid which uses a different contact format - let's clear out those contacts just to be safe.
			PHSIM->GetContactMgr()->ResetWarmStartAllContactsWithInstance(this);

			// This collider is articulated, so add it as a self overlapping pair the physics level's broadphase culler.
			// The physics level's ActivateObject above adds broadphase overlapping pairs, but not self-overlapping pairs for articualted bodies.
			PHLEVEL->GetBroadPhase()->addOverlappingPair((u16)GetLevelIndex(),(u16)GetLevelIndex());
#if __PS3
			GetCacheEntry()->ReloadDmaPlan();
#endif
			collider->FindJointLimitDofs();
		}
	}
}

#if USE_VIRTUAL_UPDATE
void fragInst::Update()
{
	// We don't need to do anything here any more.  If the skeleton gets changed and we need to update the bounds then SkeletonWasUpdated() should get called.  If the instance
	// matrix and/or articulated body get changed then ReportMovedBySim() should get called.
}
#endif

void fragInst::PartsBrokeOff(const ComponentBits& UNUSED_PARAM(brokenGroups), phInst* UNUSED_PARAM(newInst))
{
}

void fragInst::PartsRestored(const ComponentBits& UNUSED_PARAM(restoredGroups))
{
}

void fragInst::PartDied(int UNUSED_PARAM(groupIndex), const Collision& UNUSED_PARAM(damageMsg))
{
}

bool fragInst::IsBreakable(phInst* UNUSED_PARAM(otherInst)) const 
{
	if(!GetBroken())
	{
		return true;
	}

	//see if we're a cached fragment...
	if(!GetCached())
	{
		//in this case, we're a simple fragment, and we know we can't break any more...
		return false;
	}

	if(IsBreakingDisabled())
	{
		return false;
	}

	return GetCacheEntry()->IsFurtherBreakable();
}

bool fragInst::IsGroupBreakable(int UNUSED_PARAM(nGroupIndex)) const
{
	return true;
}


#if 0
void fragInst::ProjectileHit(const projImpactInfo& impact)
{
	if (!PHLEVEL->GetFrozenActiveState()) // Can't apply impulses during simulator update
	{
		Vector3		impulse;

		impulse = impact.Projectile->GetVelocity();
		impulse *= impact.Projectile->GetVelocity().InvMag() * impact.Damage->GetBangerImpulse();

		PHSIM->ApplyImpulse(GetLevelIndex(), impulse, impact.Position, impact.component);

		if (PHLEVEL->LegitLevelIndex(GetLevelIndex()) && impact.Damage->GetBangerHitPoints() > 0.0f)
		{
			sagMsgPhysCollision collisionMsg;

			GetVelocity(collisionMsg.relativeVelocity);
			collisionMsg.relativeVelocity -= impact.Projectile->GetVelocity();
			collisionMsg.relativeSpeed = collisionMsg.relativeVelocity.Mag();
			collisionMsg.damage = impact.Damage->GetBangerHitPoints();
			collisionMsg.component = impact.component;
			collisionMsg.part = 0;
			collisionMsg.otherInst = NULL;
			collisionMsg.otherComponent = 0;
			collisionMsg.otherPart = 0;
			collisionMsg.position = impact.Position;
			collisionMsg.normal = impact.Normal;

			TakeDamage(collisionMsg);
		}
	}
}
#endif

void fragInst::Remove()
{
#if !__FINAL
	AssertMsg(!phInst::AreDeletionsLocked() || !ShouldBeInCacheToDraw(), "A fragment is being removed outside the safe zone - this can crash the game because the renderer requires the cache entry that is about to be released.");
#endif // !__FINAL

#if ENABLE_PHYSICS_LOCK
	// I don't really think this is necessary.
	PHLOCK_SCOPEDWRITELOCK;
#endif	// ENABLE_PHYSICS_LOCK

	if (GetCached())
	{
		FRAGCACHEMGR->ReleaseCacheEntry(GetCacheEntry());
	}

	if (GetType())
	{
		Matrix34 reloadMat;
		GetMatrixFromEulers(reloadMat);
		// [SPHERE-OPTIMISE]
		m_InstanceXFormSphere.SetV4(Vec4V(
			GetTypePhysics()->GetCompositeBounds()->GetCentroidOffset(),
			GetTypePhysics()->GetCompositeBounds()->GetRadiusAroundCentroidV()
		));
		m_InstanceXFormSphere.TransformSphereScaled( RCC_MAT34V(reloadMat) );
	}

	FRAGMGR->RemoveFragInst(this);

	//we're in the physics world - get out...
	if (IsInLevel())
	{

		PHSIM->DeleteObject(GetLevelIndex());

#if __ASSERT
		--fragManager::sm_PhysicsInstancesAdded;
#endif
	}

	SetArchetype(NULL);

	SetInserted(false);
}


fragCacheEntry *fragInst::PutIntoCache()
{
	Assert(!GetCached());
	Assert(GetType());

	// If the number of children is zero, this is a simple fragment and shouldn't go into the cache.
	Assert(GetTypePhysics()->GetNumChildren() != 0);

	fragCacheEntry* newEntry = FRAGCACHEMGR->GetNewCacheEntry();

	if (newEntry == NULL)
	{
		NotifyCouldntGetCacheEntry();

		return NULL;
	}

	newEntry->SetActivateOnHit( ShouldActivateOnHit() );

	// We got a cache entry so now let's initialize it.
	const crSkeleton* skeleton = GetSkeleton();

	newEntry->Init(
		m_Type,
		RCC_MATRIX34(GetMatrix()),
		this,
		0xFF,
		skeleton);

    newEntry->GetHierInst()->parentInst = PHLEVEL->GetHandle(this);

	Assert(newEntry->GetInst() == this);
	return newEntry;
}

void fragInst::Reset()
{
	//AssertMsg(GetInserted(), "fragInst::Reset cannot be called on an instance that isn't Inserted");
#if ENABLE_PHYSICS_LOCK
	// I don't really think this is necessary.
	PHLOCK_SCOPEDWRITELOCK;
#endif	// ENABLE_PHYSICS_LOCK

	//Reset the user editable flags
	SetDefaultBitStates();

    if (m_BitStates & FRAG_INSTANCE_BROKE_AND_REMOVED)
	{
		// This instance was broken and removed, so re-insert it.
		Insert();
		SetBit(false,FRAG_INSTANCE_BROKE_AND_REMOVED);
	}

	if (GetCached())
	{
		fragCacheEntry* entry = GetCacheEntry();
		bool usingBackingInst = entry->IsUsingBackingInst();
		FRAGCACHEMGR->ReleaseCacheEntry(entry);

		if (ShouldBeInCacheToDraw() && !usingBackingInst)
		{
			PutIntoCache();
		}

		if(PHLEVEL && IsInLevel())
		{
			PHLEVEL->UpdateObjectLocationAndRadius(GetLevelIndex(),(Mat34V_Ptr)(NULL));
			ReportMovedBySim();
		}
	}
	else
	{
		Matrix34 originalMatrix;
		GetMatrixFromEulers(originalMatrix);
		m_DamageHealth = 0.0f;
		if (GetType())
		{
			if (GetTypePhysics()->GetAllGroups())
			{
				if (GetTypePhysics()->GetGroup(0))
				{
					m_DamageHealth = GetTypePhysics()->GetGroup(0)->GetDamageHealth();
				}
			}
		}
		// Just because this instance is registered with this fragType doesn't mean 
		// that it's been inserted in the physics level (particularly if the fragType
		//   hasn't been paged in yet).
		if (GetInserted() && !IsInLevel())
		{
			if (GetType() && GetInsertionState()!=phLevelBase::OBJECTSTATE_NONEXISTENT)
			{
				// Our fragType exists!
				ReportTypePagedIn();
			}
			if (GetType() && GetInsertionState() == phLevelBase::OBJECTSTATE_NONEXISTENT) // let's keep original matrix if it's not inserted in the level, it's probably not messed up, could gfxOnly prop
			{
				originalMatrix = RCC_MATRIX34(GetMatrix());
			}
		}
		if(PHLEVEL && IsInLevel())
		{
			if (PHLEVEL->IsActive(GetLevelIndex()))
			{
				PHSIM->DeactivateObject(this,true);
			}
		}

		SetMatrixNoZeroAssert(RCC_MAT34V(originalMatrix));

		if (GetType())
		{
			// [SPHERE-OPTIMISE]
			m_InstanceXFormSphere.SetV4(Vec4V(
				GetTypePhysics()->GetCompositeBounds()->GetCentroidOffset(),
				GetTypePhysics()->GetCompositeBounds()->GetRadiusAroundCentroidV()
			));
			m_InstanceXFormSphere.TransformSphereScaled( RCC_MAT34V(originalMatrix) );

			if(GetTypePhysics()->GetMinMoveForce() != 0.0f)
			{
				SetBroken(false);
			}
		}

		if(PHLEVEL && IsInLevel())
		{
			PHLEVEL->UpdateObjectLocationAndRadius(GetLevelIndex(),(Mat34V_Ptr)(NULL));
			ReportMovedBySim();
		}
	}

    SetDead(false);
}

bool fragInst::IsCacheEntryReady() const
{
	return GetCached() && GetCacheEntry()->IsInit();
}


int fragInst::GetLinkIndexFromComponent(int componentIndex) const
{
	if(const fragCacheEntry* entry = GetCacheEntry())
	{
		if(const phArticulatedCollider* pArticulatedCollider = entry->GetHierInst()->articulatedCollider)
		{
			if(pArticulatedCollider->IsArticulated())
			{
				return pArticulatedCollider->GetLinkFromComponent(componentIndex);
			}
		}
	}
	return 0;
}


void fragInst::ReportMovedBySim ()
{
    if (!GetManualSkeletonUpdate())
    {
		// We only want to update the skeleton if our collider is articulated
		if(fragCacheEntry* entry = GetCacheEntry())
		{
			if(entry->GetHierInst()->articulatedCollider && entry->GetHierInst()->articulatedCollider->IsArticulated())
			{
				PoseSkeletonFromArticulatedBody();
			}
		}
    }
}


void fragInst::PoseSkeletonFromArticulatedBody()
{
	// We're here because the we have been physically moved.  That means that our instance/collider/composite bound matrices are correct and, if it exists
	//   (and is active?), our articulated body is correct too.
	// Really, this function says "your instance matrix and/or articulated body might have been changed, do any extra stuff to you need to do to handle this".

	//update the position our movable is using...
	if (GetArchetype() && GetArchetype()->GetBound())
	{
		// [SPHERE-OPTIMISE]
		m_InstanceXFormSphere.SetV4(Vec4V(
			GetArchetype()->GetBound()->GetCentroidOffset(),
			GetArchetype()->GetBound()->GetRadiusAroundCentroidV()
		));
		m_InstanceXFormSphere.TransformSphereScaled( GetMatrix() );
	}

#if 0
	// The only thing that's probably off is our skeleton.
	// It seems like we SHOULD be able to just call this but, in the case of the articulated body it appears that our bounds have not yet been updated.
	// Also, this would be overkill when we're not an articulated body.
	// TODO: Look into this.
	PoseSkeletonFromBounds();
#else

    fragCacheEntry* entry = GetCacheEntry();
	fragHierarchyInst* hierInst = entry ? entry->GetHierInst() : NULL;
	Assert(!GetCached() || hierInst != NULL);

	// Copy the skeleton matrices for bones that don't have a bound part
	crSkeleton* skeleton = GetSkeleton();

	if(!GetCached() || hierInst->body == NULL || !PHLEVEL->IsActive(GetLevelIndex()))
	{
		// We're not yet in the cache or we're not articulated.  In either case, our skeleton update is going to come simply from recalculating the skeleton's matrices using
		//   a new root bone position.
		// If we don't have a skeleton, then there's nothing extra that we have to do.
		if (skeleton)
		{
			skeleton->Update();

			if (entry)
			{
                crSkeleton* damagedSkeleton = entry->GetHierInst()->damagedSkeleton;
                Matrix34* damagedMatrices = NULL;

                if (damagedSkeleton)
                {
                    damagedMatrices = reinterpret_cast<Matrix34*>(damagedSkeleton->GetObjectMtxs());
                    sysMemCpy(damagedMatrices, reinterpret_cast<Matrix34*>(skeleton->GetObjectMtxs()), sizeof(Matrix34) * skeleton->GetSkeletonData().GetNumBones());
                }

				ZeroSkeletonMatricesByGroup(skeleton, damagedSkeleton);
			}  
		}
	}
	else
	{
		//moved the impl into its own funciton.
		SyncSkeletonToArticulatedBody();
	}
#endif
}

void fragInst::SyncSimpleSkeletonUpdate()
{
	//need to consider the possibility the object was evicted from the level between updates.
	if (!IsInLevel() || !PHLEVEL->LegitLevelIndex(GetLevelIndex()))
	{
		return;
	}

	fragCacheEntry* entry = GetCacheEntry();
	fragHierarchyInst* hierInst = entry ? entry->GetHierInst() : NULL;

	Assert(!GetCached() || hierInst != NULL);

	if (hierInst==NULL)
	{
		return;
	}

	// Copy the skeleton matrices for bones that don't have a bound part
	crSkeleton* skeleton = GetSkeleton();

	if(!GetCached() || hierInst->body == NULL || !PHLEVEL->IsActive(GetLevelIndex()))
	{
		// We're not yet in the cache or we're not articulated.  In either case, our skeleton update is going to come simply from recalculating the skeleton's matrices using
		//   a new root bone position.
		// If we don't have a skeleton, then there's nothing extra that we have to do.
		if (skeleton)
		{
			skeleton->Update();

			if (entry)
			{
				ZeroSkeletonMatricesByGroup(skeleton, NULL);
			}  
		}
	}
	else
	{
		//shouldn't be updating this object with this call if we don't meet the above conditions.
		AssertMsg(0,"Tried to sync simple of skeleton which requires articulated syncing" );
	}
}

void fragInst::SyncSkeletonToArticulatedBody(bool )
{
	// Not sure why this check is necessary
	if (!IsInLevel())
	{
		return;
	}

	// It doesn't make sense to call this function without a cache entry and an articulated body
	if (fragCacheEntry* entry = GetCacheEntry())
	{
		fragHierarchyInst& hierInst = *entry->GetHierInst();
		if (const phArticulatedBody* body = hierInst.body)
		{
			const fragPhysicsLOD* physicsLOD = GetTypePhysics();
			const phArticulatedCollider& articulatedCollider = *hierInst.articulatedCollider;
			const Mat34V* linksFromComponents = (const Mat34V*)(hierInst.linkAttachment ? &hierInst.linkAttachment->GetElement(0) : physicsLOD->GetLinkAttachmentMatrices());
			crSkeleton& skeleton = *hierInst.skeleton;
			Mat34V* instanceFromBones = skeleton.GetObjectMtxs();
			Mat34V* parentBonesFromBones = skeleton.GetLocalMtxs();
			const s16* parentIndices = skeleton.GetSkeletonData().GetParentIndices();

			// Ensure that the instance and collider are lined up. If this asserts then the bone matrices will be incorrect. 
			// The assert will only fire if somebody calls this function after moving the instance or collider without 
			//   calling phCollider::SetInstanceMatrixFromCollider or SetColliderMatrixFromInstance. 
			ASSERT_ONLY(articulatedCollider.ValidateInstanceMatrixAlignedWithCollider();)

			// The first object matrix always matches the local
			instanceFromBones[0] = parentBonesFromBones[0];

			const int numBones = skeleton.GetBoneCount();
			if(numBones > 1)
			{
				// Precompute the instance-from-world matrix and root link position
				Mat34V instanceFromWorld;
				InvertTransformOrtho(instanceFromWorld,GetMatrix());
				const Vec3V rootLinkPosition = articulatedCollider.GetPosition();

				bool anyGroupsBroken = hierInst.groupBroken->AreAnySet();

				int nextBoneIndex = 1;
				int nextChildIndex = entry->GetComponentIndexFromBoneIndex(nextBoneIndex);
				int nextLinkIndex = nextChildIndex > -1 ? articulatedCollider.GetLinkFromComponent(nextChildIndex) : 0;
				for(int boneIndex = nextBoneIndex, childIndex = nextChildIndex, linkIndex = nextLinkIndex; 
						boneIndex < numBones;
						boneIndex = nextBoneIndex, childIndex = nextChildIndex, linkIndex = nextLinkIndex)
				{
					// Prefetch the next bone matrices
					nextBoneIndex = boneIndex + 1;
					if(nextBoneIndex < numBones)
					{
						PrefetchObject(&instanceFromBones[nextBoneIndex]);
						PrefetchObject(&parentBonesFromBones[nextBoneIndex]);
						nextChildIndex = entry->GetComponentIndexFromBoneIndex(nextBoneIndex);
						if(nextChildIndex > -1)
						{
							nextLinkIndex = articulatedCollider.GetLinkFromComponent(nextChildIndex);
							if(nextLinkIndex > 0)
							{
								// Grab the next link and component matrices
								PrefetchObject(&body->GetLink(nextLinkIndex).GetMatrixRef());
								PrefetchObject(&physicsLOD->GetChild(nextChildIndex)->GetUndamagedEntity()->GetBoundMatrix());
								PrefetchObject(&linksFromComponents[nextChildIndex]);
							}
						}
					}

					Mat34V_Ref instanceFromBone = instanceFromBones[boneIndex];
					Mat34V_ConstRef instanceFromParentBone = instanceFromBones[parentIndices[boneIndex]];
					Mat34V_Ref parentBoneFromBone = parentBonesFromBones[boneIndex];

					// Is this bone part of the physically articulated body?
					if(childIndex > -1)
					{
						// Since the root link shouldn't be moving in instance space we shouldn't have to update those bone matrices. This check also lets us
						//   avoid overwriting wheel matrices in gta.
						if(linkIndex > 0)
						{
							const fragTypeChild& child = *physicsLOD->GetChild(childIndex);
							// Prevent cache miss in GetOwnerGroupPointerIndex in common case where nothing is broken
							if (!anyGroupsBroken || hierInst.groupBroken->IsClear(child.GetOwnerGroupPointerIndex()))
							{
								// Compute the instance-from-link matrix
								// linkMatrix 3x3 is the transpose of the world-from-link rotation and the position is relative to the root link in world space
								Mat34V_ConstRef linkMatrix = body->GetLink(linkIndex).GetMatrixRef();
								Mat34V worldFromLink;
								Transpose3x3(worldFromLink,linkMatrix);
								worldFromLink.SetCol3(Add(linkMatrix.GetCol3(),rootLinkPosition));
								Mat34V instanceFromLink;
								Transform(instanceFromLink,instanceFromWorld,worldFromLink);

								// Using the bone-from-component and link-from-component matrices we can find the link-from-bone matrix
								Mat34V_ConstRef linkFromComponent = linksFromComponents[childIndex];
								Mat34V componentFromBone;
								// NOTE: Why don't we check for damaged groups and use the damaged entity bound matrix?
								//       Do damaged models just not work on articulated props?
								InvertTransformOrtho(componentFromBone,RCC_MAT34V(child.GetUndamagedEntity()->GetBoundMatrix()));
								Mat34V linkFromBone;
								Transform(linkFromBone,linkFromComponent,componentFromBone);

								// Combine the instance-from-link and link-from-bone to create the instance-from-bone (object matrix of the skeleton)
								Transform(instanceFromBone,instanceFromLink,linkFromBone);
								Assertf(instanceFromBone.IsOrthonormal3x3(ScalarVFromF32(REJUVENATE_ERROR_NEW_VEC)), "Setting non-orthnormal object matrix on bone %i of '%s'.",boneIndex,GetArchetype()->GetFilename());

								// Update the matrix between this bone and its parent if the parent is valid
								if(IsZeroAll(instanceFromParentBone.GetCol0()) == 0)
								{
									UnTransformOrtho(parentBoneFromBone,instanceFromParentBone,instanceFromBone);
									Assertf(parentBoneFromBone.IsOrthonormal3x3(ScalarVFromF32(REJUVENATE_ERROR_NEW_VEC)), "Setting non-orthnormal local matrix on bone %i of '%s'.",boneIndex,GetArchetype()->GetFilename());
								}
							}
						}
					}
					else
					{
						// This group isn't articulated so it shouldn't have moved relative to its parent bone. The index of the parent
						//   is always greater than the index of the child so we've already processed the parent. 
						// NOTE: This can zero the object matrix of this bone
						Transform(instanceFromBone,instanceFromParentBone,parentBoneFromBone);
					}
				}
			}

			ZeroSkeletonMatricesByGroup(&skeleton, hierInst.damagedSkeleton);
		}
	}
}

void fragInst::ZeroSkeletonMatricesByGroup(crSkeleton* skeleton, crSkeleton* damagedSkeleton)
{
    // Zero out skeleton matrices for broken-off parts
	// Only proceed if ragdoll is damagable.  Currently, human ragdolls are not damageable.
	if (GetType()->GetARTAssetID() >= 0)
		return;

    if (fragCacheEntry* entry = GetCacheEntry())
    {
		fragPhysicsLOD* lod = GetTypePhysics();
        fragHierarchyInst* hierInst = entry->GetHierInst();

        const atBitSet& brokenGroups = *hierInst->groupBroken;
		const fragGroupInst* groupInsts = hierInst->groupInsts;
		const int* boneIndexToComponentMap = entry->GetBoneIndexToComponentMap();
		Assert(boneIndexToComponentMap);
		
		bool bCheckGroups = brokenGroups.AreAnySet();
		if(!bCheckGroups && damagedSkeleton)
		{
			int numChildGroups = lod->GetNumChildGroups();
			for(int i = 0; i < numChildGroups; i++)
			{
				if(groupInsts[i].IsDamaged())
				{
					bCheckGroups = true;
					break;
				}
			}
		}

		int numBones = skeleton->GetBoneCount();
		bool anyGroupDamaged = false;
		
		if(bCheckGroups)
		{
			Mat34V* skeletonMatrices = skeleton->GetObjectMtxs();

			for (int boneIndex = 0; boneIndex < numBones; ++boneIndex)
			{
				int component = boneIndexToComponentMap[boneIndex];

				if (component >= 0)
				{
					fragTypeChild* child = lod->GetChild(component);
					int groupIndex = child->GetOwnerGroupPointerIndex();

					if (brokenGroups.IsSet(groupIndex) ||
						(damagedSkeleton && groupInsts[groupIndex].IsDamaged()))
					{
						skeletonMatrices[boneIndex] = Mat34V(V_ZERO);
						anyGroupDamaged = true;
					}
				}
			}
		}

		if (damagedSkeleton)
		{
			Mat34V* damagedSkeletonMatrices = damagedSkeleton->GetObjectMtxs();

			for (int boneIndex = 0; boneIndex < numBones; ++boneIndex)
			{
				int component = boneIndexToComponentMap[boneIndex];

				if (component >= 0)
				{
					fragTypeChild* child = lod->GetChild(component);
					int groupIndex = child->GetOwnerGroupPointerIndex();

					if (brokenGroups.IsSet(groupIndex) || !groupInsts[groupIndex].IsDamaged())
					{
						damagedSkeletonMatrices[boneIndex] = Mat34V(V_ZERO);
					}
				}
			}
		}

        if (GetType()->GetDamagedDrawable())
        {
            hierInst->anyGroupDamaged = anyGroupDamaged;
        }
    }
}

void fragInst::PrepareForReuse()
{
	Assert(!IsInLevel());
	m_BitStates = 0;
	SetInstFlags(0);
}

void fragInst::NotifyOutOfWorld()
{
#if 1 // Some experimental functionality
	phInstBreakable::NotifyOutOfWorld();
#else
	Errorf("fragObject found itself out of world, so is resetting itself!");

	const_cast<fragInst*>(this)->Reset();	   
#endif
}

static int s_CacheWarnCount = 0;
static int s_CacheWarnSequenceNumber = 0;

void fragInst::NotifyCouldntGetCacheEntry() const
{
	int maxWarnCount = 256;
	PARAM_fragcachewarn.Get(maxWarnCount);

    int sequenceNumber = FRAGCACHEMGR->GetSeqNum();
    if (sequenceNumber > s_CacheWarnSequenceNumber)
    {
        s_CacheWarnSequenceNumber = sequenceNumber;

	    if (s_CacheWarnCount < maxWarnCount)
	    {
		    ++s_CacheWarnCount;

		    Displayf("Fragment cache entry could not be allocated for instance of type '%s' at <%f %f %f>", GetType()->GetBaseName(), RCC_MATRIX34(GetMatrix()).d.x, RCC_MATRIX34(GetMatrix()).d.y, RCC_MATRIX34(GetMatrix()).d.z);
	    }
    }
}

void fragInst::PoseSkeletonFromBounds()
{
	if(const fragType* type = GetType())
	{
		const fragPhysicsLOD* physicsLOD = GetTypePhysics();
		crSkeleton *skeleton = GetSkeleton();
		if(skeleton == NULL)
		{
			return;
		}

		Assert(GetArchetype()->GetBound()->GetType() == phBound::COMPOSITE);
		phBoundComposite *compositeBound = static_cast<rage::phBoundComposite *>(GetArchetype()->GetBound());

		fragHierarchyInst* hierInst = GetCached() ? GetCacheEntry()->GetHierInst() : NULL;

		// If we're cached and have a skeleton then that skeleton should have some bones. (otherwise we shouldn't have created it)
		Assert(!GetCached() || hierInst->skeleton->GetSkeletonData().GetNumBones() > 0);
		Assert(!GetCached() || hierInst->skeleton->GetBoneCount() > 0);

		atFixedBitSet<FRAG_MAX_NUM_BONES> matrixUpdated;

		// We need to fix our skeleton up to match the bounds.
		int numChildren = physicsLOD->GetNumChildren();
		int curBoneIndex = 1;
		for (int childIndex = 0; childIndex < numChildren; ++childIndex)
		{
			// Update all of the bones from after the previous one up to the one that goes with the current fragTypeChild.
			// This works because the fragTypeChilds are sorted according to increasing bone index.
			fragTypeChild* child = physicsLOD->GetChild(childIndex);
			int curChildBoneIndex = type->GetBoneIndexFromID(child->GetBoneID());

			// If this child has the same bone as the previous child, undo the increment
			if (curBoneIndex > curChildBoneIndex)
			{
				--curBoneIndex;
			}

			Assert(curChildBoneIndex >= curBoneIndex || curChildBoneIndex == 0);
			for ( ; curBoneIndex < curChildBoneIndex; ++curBoneIndex)
			{
				// skeleton->GetGlobalMtx(curBoneIndex) = (parent's local -> world) x (child's local -> parent's local) = (child's local -> world)
				RC_MATRIX34(skeleton->GetObjectMtx(curBoneIndex)).Dot(RCC_MATRIX34(skeleton->GetLocalMtx(curBoneIndex)), RCC_MATRIX34(skeleton->GetObjectMtx(skeleton->GetSkeletonData().GetParentIndex(curBoneIndex))));
			}

			const int curGroupIndex = child->GetOwnerGroupPointerIndex();
			if (!GetCached() || hierInst->groupBroken->IsClear(curGroupIndex))		// If we're not cached, then nothing can be broken.
			{
				if (matrixUpdated.IsClear(curChildBoneIndex))
				{
					matrixUpdated.Set(curChildBoneIndex);

					// Update this bone to correspond to the bound part.
					// hierInst->skeleton->GetGlobalMtx(curChildBoneIndex) = (instance -> world) x (frag part/bone -> instance) = frag part/bone -> world

					Matrix34& boneMatrix = RC_MATRIX34(skeleton->GetObjectMtx(curChildBoneIndex));
					boneMatrix.FastInverse(child->GetUndamagedEntity()->GetBoundMatrix());
					boneMatrix.Dot(RCC_MATRIX34(compositeBound->GetCurrentMatrix(childIndex)));
				}
			}
			else
			{
				RC_MATRIX34(skeleton->GetObjectMtx(curChildBoneIndex)).Zero();
			}

			curBoneIndex = curChildBoneIndex + 1;
		}

		// Finish up by taking care of any bones that come after the last bone to which a fragTypeChild is mapped.
		int numBones = skeleton->GetSkeletonData().GetNumBones();
		for ( ; curBoneIndex < numBones; ++curBoneIndex)
		{
			RC_MATRIX34(skeleton->GetObjectMtx(curBoneIndex)).Dot(RCC_MATRIX34(skeleton->GetLocalMtx(curBoneIndex)),RCC_MATRIX34(skeleton->GetObjectMtx(skeleton->GetSkeletonData().GetParentIndex(curBoneIndex))));
		}

		if (hierInst)
		{
			crSkeleton* damagedSkeleton = hierInst->damagedSkeleton;
			Matrix34* damagedMatrices = NULL;

			if (damagedSkeleton)
			{
				damagedMatrices = reinterpret_cast<Matrix34*>(damagedSkeleton->GetObjectMtxs());
				sysMemCpy(damagedMatrices, skeleton->GetObjectMtxs(), sizeof(Matrix34) * skeleton->GetSkeletonData().GetNumBones());
			}

			ZeroSkeletonMatricesByGroup(skeleton, damagedSkeleton);
		}
	}
}

void fragInst::PoseBoundsFromSkeleton(bool onlyAdjustForInternalMotion, bool updateLastBoundsMatrices, bool fullBvhRebuild, s8 numFragsToDisable, const s8 *pFragsToDisable)
{
	PF_FUNC(PoseBoundsFromSkeleton);

	// Could/should this be an assert?  If we're not cached, we shouldn't be messing with the bounds.
	if (fragCacheEntry* entry = GetCacheEntry())
	{
		// Copy the last matrices from the current matrices so that collisions will know where we just were
		// NOTE: this means we'd better be calling this function only once per frame
		if (updateLastBoundsMatrices)
			entry->GetBound()->UpdateLastMatricesFromCurrent();

		const fragPhysicsLOD* physicsLOD = GetTypePhysics();

		const crSkeleton* skeleton = GetSkeleton();
		Assert(skeleton != NULL);		// We use it below and don't seem to check it then ...

		int* componentToBoneIndexMap = entry->GetComponentToBoneIndexMap();
		Assert(componentToBoneIndexMap);

		// Update the composite bounds according to the skeleton and copy the skeleton matrices that we got into our cache entry's skeleton.
		// Go through each fragTypeChild/bound component ...

		int numChildren = physicsLOD->GetNumChildren();

		for (int childIndex = 0; childIndex < numChildren; ++childIndex)
		{
			if (childIndex + 1 < numChildren)
			{
				const fragTypeChild* nextChild = physicsLOD->GetChild(childIndex + 1);
				const int nextGroupIndex = nextChild->GetOwnerGroupPointerIndex();
				PrefetchDC(&entry->GetHierInst()->groupInsts[nextGroupIndex]);
				PrefetchObject(&nextChild->GetUndamagedEntity()->GetBoundMatrix());
				u32 nextBoneIndex = componentToBoneIndexMap[childIndex + 1];
				PrefetchObject(&skeleton->GetObjectMtx(nextBoneIndex));
			}
			// ... and check if it's still attached.
			const fragTypeChild *curChild = physicsLOD->GetChild(childIndex);
			
			// Determin if the bounds should be update from the skeleton
			bool bUpdateBoundFromSkeleton = true;
			for(int i = 0; i < numFragsToDisable; i ++)
			{
				if(pFragsToDisable[i] == childIndex)
				{
					bUpdateBoundFromSkeleton = false;
					break;
				}
			}

			const int curGroupIndex = curChild->GetOwnerGroupPointerIndex();
			if (entry->GetHierInst()->groupBroken->IsClear(curGroupIndex) && bUpdateBoundFromSkeleton)
			{
				// This is fragTypeChild/bound component still attached.
				fragTypeChild* child = physicsLOD->GetChild(childIndex);
				u32 boneIndex = componentToBoneIndexMap[childIndex];

				Mat34V instanceFromBone = skeleton->GetObjectMtx(boneIndex);

				// If the bone matrix has been zeroed, don't bring it into the bound
				if(Likely(!IsEqualAll(instanceFromBone.GetCol0(),Vec3V(V_ZERO))))
				{
					fragDrawable* currentDrawable = child->GetUndamagedEntity();

					fragDrawable* damagedEntity = child->GetDamagedEntity();
					if (Unlikely(damagedEntity && entry->GetHierInst()->groupInsts[curGroupIndex].IsDamaged()))
					{
						currentDrawable = damagedEntity;
					}

					Mat34V_ConstRef boneFromComponent = RCC_MAT34V(currentDrawable->GetBoundMatrix());
					Mat34V instanceFromComponent;
					Transform(instanceFromComponent,instanceFromBone,boneFromComponent);
					entry->GetBound()->SetCurrentMatrix(childIndex, instanceFromComponent);

#if __ASSERT
					// If this object has broken off from the root object then this assert doesn't mean much. 
					if(!GetGroupBroken(0))
					{
						// don't expect any ped bones to move more than 14m from the skeleton root bone
						// slightly gta specific assert :-)
						Vec3V rootPos = skeleton->GetObjectMtx(0).d();
						Vector3 offset = VEC3V_TO_VECTOR3(instanceFromComponent.GetCol3() - rootPos);
						float assertRad = Max(3.0f, 3.0f * entry->GetBound()->GetRadiusAroundCentroid());
						Assertf(offset.Mag2() < assertRad*assertRad, "fragInst::PoseBoundsFromSkeleton - %s unexpected large offset comp%d bone%d (%f,%f,%f). Centroid radius %f.", GetType()->GetBaseName(), childIndex, boneIndex, VEC3V_ARGS(instanceFromComponent.GetCol3()), entry->GetBound()->GetRadiusAroundCentroid());
					}
#endif
				}
			}
		}

        /*Vector3 centroidOffset = GetTypePhysics()->GetCompositeBounds()->GetCentroidOffset();
        Mat34V objectTransform;
        type->GetSkeletonData()->ComputeObjectTransform(0, objectTransform);
        objectTransform.UnTransform(centroidOffset);
        skeleton->GetGlobalMtx(0).Transform(centroidOffset);
        RCC_MATRIX34(GetMatrix()).UnTransform(centroidOffset);
        entry->GetBound()->SetCentroidOffset(centroidOffset);*/

		phBoundComposite *bnd = entry->GetBound();

		// MIGRATE_FIXME - the fixed true value passed into this can now be drived by the argument
		bnd->CalculateCompositeExtents(onlyAdjustForInternalMotion);

		//Revisit this later
		//Updating the physics level should really NOT be done from here -- instead the calling code should handle it itself
		//since not every call to PoseBoundsFromSkeelton would require a level update
		if(IsInLevel())
		{
			const phArchetype* pArch = GetArchetype();
			if(pArch)
			{
				const phBound* pInstBound = pArch->GetBound();
				if(pInstBound == bnd)
				{
					const int levelIndex = GetLevelIndex();
					PHLEVEL->UpdateObjectLocationAndRadius(levelIndex, (Mat34V_Ptr)(NULL));

					if(bnd->HasBVHStructure())
					{
						if(fullBvhRebuild)
						{
							PHLEVEL->RebuildCompositeBvh(levelIndex);
						}
						else
						{
							PHLEVEL->UpdateCompositeBvh(levelIndex);
						}
					}
				}
			}
		}
		else
		{
			bnd->UpdateBvh(fullBvhRebuild);
		}
	}
}


void fragInst::PoseBoundsFromSkeleton (const crSkeleton& skeleton, phBoundComposite& bound)
{
	PF_FUNC(PoseBoundsFromSkeleton);

	if (fragCacheEntry* entry = GetCacheEntry())
	{
		int numChildren = GetTypePhysics()->GetNumChildren();

		// Update the composite bounds according to the skeleton and copy the skeleton matrices that we got into our cache entry's skeleton.
		// Go through each fragTypeChild/bound component ...
		for (int childIndex = 0; childIndex < numChildren; ++childIndex)
		{
			// ... and check if it's still attached.
			const fragTypeChild *curChild = GetTypePhysics()->GetChild(childIndex);
			const int curGroupIndex = curChild->GetOwnerGroupPointerIndex();
			if (entry->GetHierInst()->groupBroken->IsClear(curGroupIndex))
			{
				// This is fragTypeChild/bound component still attached.
				fragTypeChild* child = GetTypePhysics()->GetChild(childIndex);

				// Grab the matrix of the bound relative to the bone
				Matrix34 currentMatrix = child->GetUndamagedEntity()->GetBoundMatrix();

				if (entry->GetHierInst()->groupInsts[curGroupIndex].IsDamaged())
				{
					if (fragDrawable* damagedEntity = child->GetDamagedEntity())
					{
						currentMatrix = damagedEntity->GetBoundMatrix();
					}
				}

				s16 boneIndex = (s16) GetType()->GetBoneIndexFromID(child->GetBoneID());
				Assert(boneIndex >= 0);

				// Multiply by the bone's world-space matrix to get the bound in world space
				Matrix34 globalMtx;
				skeleton.GetGlobalMtx(boneIndex, RC_MAT34V(globalMtx));
				currentMatrix.Dot(globalMtx);

				// Bring the world space bound into the current fragInst's coordinates (wrong if the skeleton is from another instance)
				currentMatrix.DotTranspose(RCC_MATRIX34(GetMatrix()));

				// Resulting bounds are stored relative to fragInst's coordinate system.
				bound.SetCurrentMatrix(childIndex,RCC_MAT34V(currentMatrix));
			}
		}
	}
}


void fragInst::PoseArticulatedBodyFromBounds(s8 numFragsToDisable /*= 0*/, const s8 *pFragsToDisable /*= NULL*/)
{
	if(!GetCached())
	{
		return;
	}

	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	fragCacheEntry* entry = GetCacheEntry();
	fragHierarchyInst* hierInst = entry->GetHierInst();
	Assert(hierInst->body != NULL);

	phArticulatedCollider* articulatedCollider = hierInst->articulatedCollider;
	phBoundComposite *compositeBound = static_cast<phBoundComposite *>(GetArchetype()->GetBound());

	atFixedBitSet<phArticulatedBodyType::MAX_NUM_LINKS> linksPosed;

#if FRAGINST_TRANSFERVELOCITIES
	Vector3 instanceLinearVelocity;
	PHSIM->GetObjectVelocity(*this, instanceLinearVelocity);
	Vector3 instanceAngularVelocity;
	PHSIM->GetObjectAngVelocity(*this, instanceAngularVelocity);
#endif

	// Pose the articulated collider from the instance
	articulatedCollider->SetColliderMatrixFromInstanceRigid();

	for (u8 groupIndex = 0; groupIndex < physicsLOD->GetNumChildGroups(); ++groupIndex)
	{
		if (!entry->IsGroupBroken(groupIndex))
		{
			u8 childIndex = physicsLOD->GetGroup(groupIndex)->GetChildFragmentIndex();
			Assertf(compositeBound->GetBound(childIndex) != NULL, "Frag child (%i) on unbroken group (%i) doesn't have bound on composite.", childIndex, groupIndex);
			int linkIndex = articulatedCollider->GetLinkFromComponent(childIndex);

			if (linksPosed.IsClear(linkIndex))
			{
				linksPosed.Set(linkIndex);

				// Find the instance from link matrix
				Mat34V_ConstRef instanceFromComponent = compositeBound->GetCurrentMatrix(childIndex);
				Mat34V instanceFromLink;
				if(articulatedCollider->GetLinkAttachmentMatrices())
				{
					Mat34V componentFromLink;
					InvertTransformOrtho(componentFromLink, RCC_MAT34V(articulatedCollider->GetLinkAttachmentMatrices()[childIndex]));
					Transform(instanceFromLink, instanceFromComponent, componentFromLink);
				}
				else
				{
					// If there are no link attachment matrices, then the link orientation lines up with
					//   the components and the position is just the bound's CG
					instanceFromLink.Set3x3(instanceFromComponent.GetMat33());
					instanceFromLink.SetCol3(Transform(instanceFromComponent,compositeBound->GetBound(childIndex)->GetCGOffset()));
				}

				// Find the world from link matrix
				Mat34V_ConstRef worldFromInstance = GetMatrix();
				Mat34V worldFromLink;
				Transform(worldFromLink, worldFromInstance, instanceFromLink);

				// Transpose the orientation and make the position relative to the root link (which should be the collider position)
				Mat34V linkMatrix;
				Transpose3x3(linkMatrix, worldFromLink);
				linkMatrix.SetCol3(Subtract(worldFromLink.GetCol3(), articulatedCollider->GetPosition()));
				phArticulatedBodyPart& link = hierInst->body->GetLink(linkIndex);
				link.SetMatrix(RCC_MATRIX34(linkMatrix));

#if FRAGINST_TRANSFERVELOCITIES
				bool skipFrag = false;
				for(s8 fragIndex = 0; fragIndex < numFragsToDisable; fragIndex ++)
				{
					if(pFragsToDisable[fragIndex] == childIndex)
					{
						skipFrag = true;
						break;
					}
				}
				if(skipFrag)
				{
					continue;
				}

				// Set the velocities of the links according to the current and previous bound positions.
				// Note that this now takes into consideration both the internal motion of the instance and the motion that the instance might have as a whole.
				// Perhaps these calculations should actually be in fragInst::GetExternallyControlledVelocity() and we should just call that here ... I'll have to think about that.

				// Find the difference in position of the CG of from the last frame to the current frame.
				// Note that this assumes that the CG of a link is the same as the CG of the upper-most fragment part that is part of that link.  While not quite correct,
				//   this is an error that is inherited from the way links are created so in that sense it's correct.
				// linearVelocity = (Mcurr . CG - Mlast . CG) * (1 / frameTime) = ([Mcurr - Mlast] . CG) * (1 / frameTime)
				Matrix34 deltaPosition;
				deltaPosition.Subtract(RCC_MATRIX34(compositeBound->GetCurrentMatrix(childIndex)), RCC_MATRIX34(compositeBound->GetLastMatrix(childIndex)));
				Vector3 linearVelocity;
				deltaPosition.Transform(VEC3V_TO_VECTOR3(compositeBound->GetBound(childIndex)->GetCGOffset()), linearVelocity);

				// Find the transformation that takes this bound part from its previous position to its current position.  Then convert it to a quaternion and find the angular change that
				//   results from that transformation.
				// lastToCurrent = (Mlast ^ -1) x (Mcurr)
				Matrix34 lastToCurrent;
				lastToCurrent.DotTranspose(RCC_MATRIX34(compositeBound->GetCurrentMatrix(childIndex)), RCC_MATRIX34(compositeBound->GetLastMatrix(childIndex)));
				Quaternion tempQuat;
				lastToCurrent.ToQuaternion(tempQuat);
				Vector3 angularVelocity;
				float angle;
				tempQuat.ToRotation(angularVelocity,angle);
				angularVelocity.Scale(CanonicalizeAngle(angle));

				// Assuming constant velocities, find the velocities that correspond to displacements.
				linearVelocity.Scale(TIME.GetInvSeconds());
				angularVelocity.Scale(TIME.GetInvSeconds());

				// Transform the local bound part velocities into world space.
				RCC_MATRIX34(GetMatrix()).Transform3x3(linearVelocity);
				RCC_MATRIX34(GetMatrix()).Transform3x3(angularVelocity);

				// Add in the instance motion.
				linearVelocity.Add(instanceLinearVelocity);
				angularVelocity.Add(instanceAngularVelocity);

				hierInst->body->SetVelocities(linkIndex, RCC_VEC3V(linearVelocity), RCC_VEC3V(angularVelocity));
#endif
			}
		}
	}
}


void fragInst::PoseArticulatedBodyFromBounds (const phBoundComposite& bound, phArticulatedBody& body)
{
	if(!GetCached())
	{
		return;
	}

	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	fragCacheEntry* entry = GetCacheEntry();
	fragHierarchyInst* hierInst = entry->GetHierInst();
	Assert(hierInst->body != NULL);
	bool linkPosed[phArticulatedBodyType::MAX_NUM_LINKS];
	memset(linkPosed, 0, phArticulatedBodyType::MAX_NUM_LINKS * sizeof(bool));

	for (int childIndex = 0; childIndex < physicsLOD->GetNumChildren(); ++childIndex)
	{
		const fragTypeChild *curChild = physicsLOD->GetChild(childIndex);
		const int curGroupIndex = curChild->GetOwnerGroupPointerIndex();
		if (hierInst->groupBroken->IsClear(curGroupIndex))
		{
			int linkNum = hierInst->articulatedCollider->GetLinkFromComponent(childIndex);

			if (!linkPosed[linkNum])
			{
				linkPosed[linkNum] = true;
				// Attachment needs to be the childFromLink matrix, the physics lod link attachment matrices are linkFromChild,
				//   so just do an ortho invert. 
				Matrix34 attachment;
				InvertTransformOrtho(RC_MAT34V(attachment),RCC_MAT34V(physicsLOD->GetLinkAttachmentMatrices()[childIndex]));

				Matrix34 linkWorld;
				linkWorld.Dot(attachment, RCC_MATRIX34(bound.GetCurrentMatrix(childIndex)));
				linkWorld.Dot(RCC_MATRIX34(GetMatrix()));

				// Set the link's angular and linear positions.
				phArticulatedBodyPart& link = body.GetLink(linkNum);

				// The link position is supposed to be relative to the collider position.
				linkWorld.d.Subtract(RCC_VECTOR3(hierInst->articulatedCollider->GetPosition()));
				link.SetPosition(linkWorld.d);

				// The link matrix stays in world space, but we have to transpose it relative to the way we keep it.
				linkWorld.Transpose();
				link.SetOrientation(linkWorld);
			}
		}
	}
}


void fragInst::PoseBoundsFromArticulatedBody()
{
	if(fragCacheEntry* entry = GetCacheEntry())
	{
		const fragPhysicsLOD* physicsLOD = GetTypePhysics();
		fragHierarchyInst* hierInst = entry->GetHierInst();

		phArticulatedBody* body = hierInst->body;
		Assert(body != NULL);
		if (!body)
			return;

		int numChildren = physicsLOD->GetNumChildren();
		for (int childIndex = 0; childIndex < numChildren; ++childIndex)
		{
			// Find out to what link this fragTypeChild maps.
			int linkIndex = hierInst->articulatedCollider->GetLinkFromComponent(childIndex);
			if (linkIndex >= 0)
			{
				AssertMsg(MAT34V_TO_MATRIX34(body->GetLink(linkIndex).GetMatrix()).IsOrthonormal(),("fragInst::PoseBoundsFromArticulatedBody() has a non-orthonormal matrix."));

				// Again, we have to get this link's local space to instance space transform in this wonky way due to the way matrices are stored in phArticulatedLinks.
				Matrix34 linkToInstance;
				linkToInstance.Transpose(MAT34V_TO_MATRIX34(body->GetLink(linkIndex).GetMatrix()));
				linkToInstance.d.Set(body->GetLink(linkIndex).GetPosition());
				linkToInstance.d.Add(RCC_MATRIX34(GetMatrix()).d);
				linkToInstance.DotTranspose(RCC_MATRIX34(GetMatrix()));

				// attachment = frag part -> link
				Matrix34 attachment = physicsLOD->GetLinkAttachmentMatrices()[childIndex];

				// fragToInstance = (link -> instance) x (frag part -> link) = frag part -> instance
				Matrix34 fragToInstance;
				fragToInstance.Dot(attachment, linkToInstance);

				entry->GetBound()->SetCurrentMatrix(childIndex, RCC_MAT34V(fragToInstance));

				AssertMsg(fragToInstance.IsOrthonormal(),("fragInst::PoseBoundsFromArticulatedBody() has a non-orthonormal matrix."));
			}
		}
	}
}


void fragInst::SetMusclesFromFrame (const crFrame& animFrame, crFrame& savedAnimPose)
{
	// Get the fragInst's body
	Assert(GetCacheEntry());
	phArticulatedBody * pBody = GetCacheEntry()->GetHierInst()->body;
	if (pBody == NULL) {  // Check that there is a body !!
		AssertMsg(0, "fragInst::SetMusclesFromFrame() is trying to pose a non-simulating articulated body.");
		return;
	}

	// Pose a frame of the fragInst (as a way of saving it's pose)
	crSkeleton &skeleton = *GetSkeleton();
	savedAnimPose.InversePose(skeleton);

	// Pose the skeleton
	animFrame.Pose(skeleton);
	skeleton.Update();

	// Set muscles from the world space skeleton
	SetMusclesFromSkeleton(skeleton);

	// Revert back to old skeleton
	savedAnimPose.Pose(skeleton);

	// World-space cleanup (might not be needed)
	skeleton.Update();					
	SyncSkeletonToArticulatedBody();	// Used to avoid reset to zero pose when simulation ends
}


void fragInst::SetMusclesFromSkeleton (const crSkeleton& skeleton, bool useAlternativeLinkIndexSelection)
{
	// Get the fragInst's body and set the drive state
	if (!IsInLevel())
		return;
	Assert(GetCacheEntry());
	fragCacheEntry* entry = GetCacheEntry();
	if (!entry)
		return;
	fragHierarchyInst* hierInst = entry->GetHierInst();
	if (hierInst==NULL || !GetCached())
		return;
	
	phArticulatedBody * pBody = GetCacheEntry()->GetHierInst()->body;
	if (pBody == NULL) {  // Check that there is a body !!
		AssertMsg(false, ("fragInst::SetMusclesFromSkeleton() is trying to pose a non-simulating articulated body."));
		return;
	}
	phArticulatedBody& body = *pBody;

	body.SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);

	Mat34V worldParent;
	Mat34V worldChild; 
	Mat34V boundMatrixParent;
	Mat34V boundMatrixChild;
	Mat34V attachParent;
	Mat34V attachChild;
    int parentIndex;
    int numJoints = body.GetNumJoints();
    for (int jointIndex=0; jointIndex<numJoints; jointIndex++)
    {
        phJoint& joint = body.GetJoint(jointIndex);
        // search for the indices of the links pointed to by joint (would ideally be a more efficient method)
        int childIndex = parentIndex = -1;

        if(useAlternativeLinkIndexSelection)
        {
            Assert(hierInst->compositeBound);
            int componentCount = hierInst->compositeBound->GetNumBounds();

            Assert(hierInst->articulatedCollider);
            const phArticulatedCollider *articulatedCollider = hierInst->articulatedCollider;

            for(int componentIndex = 0; componentIndex < componentCount && (childIndex == -1 || parentIndex == -1); componentIndex++) 
            {
                // Find out to what link this fragTypeChild maps.
                int linkIndex = articulatedCollider->GetLinkFromComponent(componentIndex);

                if(linkIndex >= 0)
                {
                    if (&body.GetLink(linkIndex) == joint.GetChildLink(pBody)) 
                    {
                        childIndex = componentIndex;
                    } 
                    else if (parentIndex == -1 && &body.GetLink(linkIndex) == joint.GetParentLink(pBody)) //only get the earliest valid link
                    {
                        parentIndex = componentIndex;
                    } 
                }
            }
        }
        else
        {   
            childIndex = joint.GetChildLinkIndex();
            parentIndex = joint.GetParentLinkIndex();
        }
		// From the link indices, get the fragInstChild, from which you can get a bone index, from which to get the bone matrix.
		Matrix34 worldParentMtx;
        s32 parentBoneIndex = GetType()->GetBoneIndexFromID(GetTypePhysics()->GetChild(parentIndex)->GetBoneID());
        skeleton.GetGlobalMtx(parentBoneIndex, RC_MAT34V(worldParentMtx));

		Matrix34 worldParentChildMtx;
        s32 childBoneIndex = GetType()->GetBoneIndexFromID(GetTypePhysics()->GetChild(childIndex)->GetBoneID());
        skeleton.GetGlobalMtx(childBoneIndex, RC_MAT34V(worldParentChildMtx));

		worldParent = RCC_MAT34V(worldParentMtx);
		worldChild = RCC_MAT34V(worldParentChildMtx); 

		// Compute the link positions
		boundMatrixParent = RCC_MAT34V(GetTypePhysics()->GetChild(parentIndex)->GetUndamagedEntity()->GetBoundMatrix());
		boundMatrixChild = RCC_MAT34V(GetTypePhysics()->GetChild(childIndex)->GetUndamagedEntity()->GetBoundMatrix());

		if (hierInst->linkAttachment)
		{
			attachParent = RCC_MAT34V(hierInst->linkAttachment->GetElement(parentIndex));
			attachChild = RCC_MAT34V(hierInst->linkAttachment->GetElement(childIndex));
		}
		else
		{
			attachParent = RCC_MAT34V(GetTypePhysics()->GetLinkAttachmentMatrices()[parentIndex]);
			attachChild = RCC_MAT34V(GetTypePhysics()->GetLinkAttachmentMatrices()[childIndex]);
		}

		Transpose3x3(attachParent, attachParent);
		Transpose3x3(attachChild, attachChild);
		Transform3x3(boundMatrixParent, boundMatrixParent, attachParent);
		Transform3x3(boundMatrixChild, boundMatrixChild, attachChild);
		Transform3x3(worldParent, worldParent, boundMatrixParent);
		Transform3x3(worldChild, worldChild, boundMatrixChild);
		Transpose3x3(worldParent, worldParent);
		Transpose3x3(worldChild, worldChild);

		// Get the joint orientation w/ respect parent and child links
		Mat34V jointLocalToParent = RCC_MAT34V(joint.GetOrientationParent()); 
		Matrix34 matTemp = joint.GetOrientationChild();
		Mat34V jointLocalToChild = RCC_MAT34V(matTemp);

		switch (joint.GetJointType())
		{
			case phJoint::JNT_1DOF:
			{
				phJoint1Dof& joint1Dof = *static_cast<phJoint1Dof*>(&joint);
				Vec3V JointAxis( SplatZ(jointLocalToChild.GetCol0()), SplatZ(jointLocalToChild.GetCol1()), SplatZ(jointLocalToChild.GetCol2()) );
				JointAxis = UnTransform3x3Ortho( worldChild, JointAxis );		// Joint axis, global coords
				Vec3V ChildAxis( SplatX(jointLocalToChild.GetCol0()), SplatX(jointLocalToChild.GetCol1()), SplatX(jointLocalToChild.GetCol2()) );
				ChildAxis = UnTransform3x3Ortho( worldChild, ChildAxis );		// Child x-axis, global coords
				Vec3V ParentAxis( SplatX(jointLocalToParent.GetCol0()), SplatX(jointLocalToParent.GetCol1()), SplatX(jointLocalToParent.GetCol2()) );
				ParentAxis = UnTransform3x3Ortho( worldParent, ParentAxis );	// Parent x-axis, global coords
				ScalarV v_x = Dot( ChildAxis, ParentAxis );	// Dot between Parent POINTING axis and Child pointing axis
				ParentAxis = Cross( JointAxis, ParentAxis );					// Parent y-axis, global coords
				ScalarV v_y = Dot( ChildAxis, ParentAxis );	// Dot between Parent UP axis and Child pointing axis
				ScalarV targetAngle = Arctan2( v_y, v_x );
				joint1Dof.SetMuscleTargetAngle(targetAngle);
				joint1Dof.SetMuscleTargetSpeed(0.0f);
				break;
			}

			case phJoint::JNT_3DOF:
			{
				phJoint3Dof& joint3Dof = *static_cast<phJoint3Dof*>(&joint);
				Mat34V jointWorld;
				Transform3x3(jointWorld, jointLocalToParent, worldParent);
				Mat44V axesAndAngles = geomVectors::ComputeLeanAndTwistAngles(
					worldParent, worldChild, jointWorld, jointLocalToChild, false);
				Vec3V leanAxis = axesAndAngles.GetCol0().GetXYZ();
				float currentLeanAngle = axesAndAngles.GetCol3().GetXf();
				float twistAngle = axesAndAngles.GetCol3().GetZf();
				Mat34V m_JointPoseWorld = worldParent;
				RC_MATRIX34(m_JointPoseWorld).Dot3x3(RCC_MATRIX34(jointLocalToParent));
				Mat34V jointTranspose;
				Transpose3x3(jointTranspose,m_JointPoseWorld);
				Vec3V lean1Axis = jointTranspose.GetCol1();
				Vec3V lean2Axis = -jointTranspose.GetCol0();
				ScalarV leanMtx22a = Dot(leanAxis,lean1Axis);
				ScalarV leanMtx22c = Dot(leanAxis,lean2Axis);
				Vec3V targetAngles = Vec3V(Scale(ScalarVFromF32(currentLeanAngle),leanMtx22a),
					Scale(ScalarVFromF32(currentLeanAngle),leanMtx22c),ScalarVFromF32(twistAngle));	
				joint3Dof.SetMuscleTargetAngle(RCC_VECTOR3(targetAngles));
				joint3Dof.SetLean1TargetSpeed(0.f);
				joint3Dof.SetLean2TargetSpeed(0.f);
				joint3Dof.SetTwistTargetSpeed(0.f);
				break;
			}
		}
	}
}


void fragInst::SetCurrentPhysicsLOD(ePhysicsLOD newLOD) 
{ 
	if(!Verifyf(PHSIM->IsSafeToModifyManifolds(), "fragInst::SetCurrentPhysicsLOD - Don't try to change physics lods during a callback."))
	{
		return;
	}

	bool inlevel = PHLEVEL->IsInLevel(GetLevelIndex());
	if(inlevel && !Verifyf(PHLEVEL->IsInactive(GetLevelIndex()), "fragInst::SetCurrentPhysicsLOD - Changing physics lods while active isn't currently supported."))
	{
		return;
	}

	if (GetType()->GetPhysics(newLOD) && m_CurrentLOD != newLOD)
	{
		// set the new LOD
		m_CurrentLOD = newLOD; 

		// Remove active contacts
		PHSIM->GetContactMgr()->RemoveAllContactsWithInstance(this);

		// Remove active constraints, if they haven't already been removed
		PHCONSTRAINT->RemoveActiveConstraints(this);

		// need to do more if there's a cache entry
		fragCacheEntry* cacheEntry = GetCacheEntry();
		if (cacheEntry)
		{
			Displayf("Switching ragdoll LOD to %d.", (int) m_CurrentLOD);

			cacheEntry->ReloadPhysicsData();

			ASSERT_ONLY(cacheEntry->GetBound()->CheckCachedMinMaxConsistency());
		}
		DEV_ONLY(PHSIM->CheckForManifoldReferencesToInst(this,true,true);)
	}
}


void fragInst::RemoveArticulatedBodyFromCacheEntry()
{
	fragCacheEntry* cacheEntry = GetCacheEntry();
	if (cacheEntry)
	{
		cacheEntry->RemoveArticulatedBody();
	}
}


void fragInst::PrintBoneMatrices() const
{
	crSkeleton *skeleton = GetSkeleton();
	Assert(skeleton != NULL);

	int numChildren = GetTypePhysics()->GetNumChildren();
	for(int childIndex = 0; childIndex < numChildren; ++childIndex)
	{
		fragTypeChild *typeChild = GetTypePhysics()->GetChild(childIndex);
		int boneIndex = GetType()->GetBoneIndexFromID(typeChild->GetBoneID());
		Printf("Bone: %d", boneIndex);
		Matrix34 globalMtx;
		skeleton->GetGlobalMtx(boneIndex, RC_MAT34V(globalMtx));
		globalMtx.Print();
	}
}


phInst* fragInst::PrepareForActivation (phCollider** collider, phInst* otherInst, const phConstraintBase * constraint)
{
	const fragType* type = GetType();
	if (!Verifyf(type, "fragInst::m_Type is NULL, out of memory?"))
	{
		return NULL;
	}

	Assert(!IsInLevel() || PHLEVEL->IsInactive(GetLevelIndex()));

	if (!GetBroken())
	{
		// We're still 'attached' to the world.  No activation for us until we get broken off.
		return NULL;
	}

	//Have we got flags that indicate we can't be activated (either override flags at
	//the instance level OR flags at the type level if no override).
	if (IsActivationDisabled())
	{
		return NULL;
	}

	//see if this is a simple fragment or not...
	if (!type->GetNeedsCacheEntryToActivate())
	{
		// This is a simple fragment - we can simply allow activation without any further action.
		return this;
	}

	if(!GetCached())
	{
		// This instance is not yet in the cache, and it can't be activated until it's in the cache.
		// There used to be a check here whether this is from a collision, and if so, it ensured that the other
		// object in the collision wouldn't get freed from the cache by a request for a new cache entry.
		// Get this instance into the cache.
		fragCacheEntry *newEntry = PutIntoCache();
		if(newEntry == NULL)
		{
			// We will not break, nor will we modify, since there were no cache entries available.
			return NULL;
		}
	}

    fragCacheEntry* entry = GetCacheEntry();
	fragHierarchyInst* hierInst = entry->GetHierInst();
	Assert(hierInst);

#if FRAGMENT_LAZY_ARTICULATED_CACHE
    if ((m_BitStates & FRAG_INSTANCE_NO_ARTIC_BODY) == 0)
    {
		if (!hierInst->articulatedCollider || hierInst->articulatedCollider->GetType() != phCollider::TYPE_RIGID_BODY)
		{
			entry->LazyArticulatedInit();
		}
    }
#endif // FRAGMENT_LAZY_ARTICULATED_CACHE

    // If our behavior says not to activate us, then don't activate us!
	phInstBehavior* behavior = hierInst->GetInstBehavior();
	if( behavior && !behavior->ActivateWhenHit() )
	{
		return NULL;
	}

	bool bBodyAlreadyPosed = entry->GetInst() && entry->GetInst()->GetTypePhysics() && entry->GetInst()->GetTypePhysics()->GetBodyType();

	// Move PoseArticulatedBodyFromBounds to after the collider because
	// the link attachment matrices are set relative to the collider
	if (hierInst->body && !bBodyAlreadyPosed)
	{
		PoseArticulatedBodyFromBounds();
	}

	if (collider)
	{
		if (hierInst->articulatedCollider)
		{
			*collider = hierInst->articulatedCollider;

			// Specifically the call to collider->Freeze was added for gta to make sure any velocity
			// from the previous time this collider was used has been correctly reset before activation
			if (!bBodyAlreadyPosed)
			{
				(*collider)->Freeze();
			}

			(*collider)->SetColliderMatrixFromInstance();
			if((*collider)->GetSleep())
				(*collider)->GetSleep()->Reset();
		}
	}

	// Move PoseArticulatedBodyFromBounds to after the collider because
	// the link attachment matrices are set relative to the collider
	if (hierInst->body && !bBodyAlreadyPosed)
	{
		PoseArticulatedBodyFromBounds();
	}

	Assert(GetBroken());
	SetBroken(true);

	return phInst::PrepareForActivation(collider, otherInst, constraint);
}


bool fragInst::PrepareForDeactivation(bool UNUSED_PARAM(colliderIsManagedBySim), bool UNUSED_PARAM(forceDeactivate))
{
#if !HACK_GTA4
	// This was added since gta4 and its screwing up our car doors
	// Better solution might be to fix the function CloseLatchAbove so that it updates local skeleton matrix for component being latched
	// because problem is that door has moved since last time local skeleton matrix was updated, and when it's latched that matrix
	// starts to be used for updating skeleton global matrices again (wasn't being used while link was active

	if(GetSkeleton() != NULL && GetType()->GetHasAnyArticulatedParts())
	{
		// On deactivation it's a good idea to re-pose the local skeleton matrices such that they're in sync with the current global skeleton matrices and
		//   instance matrix.
		GetSkeleton()->InverseUpdate();
	}
#endif
	return true;
}

//returns the number still collidable...
int fragInst::BreakApart( phInstBreakable** breakableInstList, 
	int* numBreakInsts, //num collidable and breakable...
	phInst** brokenInstList, 
	int componentA, int componentB, phInst*& pInstanceA, phInst*& pInstanceB,
	const phBreakData& breakData)
{
	PF_FUNC(BreakApart);

	// This used to just assert and continue on, but if it is an invalid level index many bad things will follow
	// as it is used as an array[-1] lookup in many places, including for writes.
	// While we still need to understand why this is happening, we have to at least prevent the crash by returning early
	// until that underlying cause is understood. -ecosky
	if(!Verifyf(PHLEVEL->LegitLevelIndex(GetLevelIndex()), "%d", GetLevelIndex()))
	{
		return 0;
	}


	const fragFindBreakInfo* findBreakInfo = reinterpret_cast<const fragFindBreakInfo*>(&breakData);

	const fragType* type = GetType();
	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	Assert(type);

	if (findBreakInfo->breakGroup != 0xFF)
	{
		if (fragCacheEntry* entry = GetCacheEntry())
		{
			if (entry->GetHierInst()->groupBroken->IsSet(findBreakInfo->breakGroup))
			{
				// This group is already broken. That happens when the pre-break impetus causes the broken 
				// part to disappear when dead.
				*brokenInstList = this;
				return 1;
			}
		}
	}

	//remove ourselves from the first list - we may or may not add ourselves back...
	//bHavePostBreakMotion = false;
	RemoveFromInstList(breakableInstList, this, numBreakInsts);

	// TODO: This really looks like it could use some cleanup/simplification.  We're calling SetBroken(true) twice,
	//   and fragInst::PrepareForActivation() ALSO calls SetBroken(true) again.
	if (!GetCached() && !type->GetNeedsCacheEntryToActivate())
	{
		//this is a simple fragment, so we always break into ourselves...
		Assert(!GetBroken());
		SetBroken(true);
		if (PHLEVEL->IsInactive(GetLevelIndex()))
		{
			PHSIM->ActivateObject(this);
		}
		SetBroken(true);
		*brokenInstList = this;

#if ENABLE_FRAGMENT_EVENTS
		//trigger events we have been set up with
		fragTypeChild* breakChild = physicsLOD->GetChild(0);
		Assert(breakChild);

		const evtSet* set = breakChild->GetBreakEventset();
		if (set && set->GetNumInstances() > 0)
		{
			Assert(breakChild->GetBreakEventPlayer());
			fragBreakEventPlayer& player = *breakChild->GetBreakEventPlayer();

			player.GetPosition() = GetPosition();	
			player.GetMatrix() = GetMatrix();
			player.GetFragInst() = this;
			player.GetInst() = (phInst*) this;
			player.GetComponent() = 0;
			player.GetOtherInst() = (phInst*)this;

			player.StartUpdateStop();
		}
#endif // ENABLE_FRAGMENT_EVENTS

		//apply any damage...
		Assert(type->GetRootChild() || physicsLOD->GetChild(0));
		//damageHealth -= findBreakInfo->aTempBreakDamage[0].fTempDamageHealth;

		return 1;
	}

	if (!GetCached())
	{
		PutIntoCache();
	}


	bool entryHasContacts[2] = { false, false };
	if (fragCacheEntry* entry = GetCacheEntry())
	{
		/*//make sure we're not replaced this frame(unless we actually are killed here and released back to the pool)...
		fragCacheManager::GetEntryFromInst(this)->SetSequenceNumber();*/

		if (findBreakInfo->breakGroup == 0xff)
		{
			//we might have just done a root break, because we had breakGroup == 0xFF, or we broke off
			//all the parts we had left. either way, our job is a whole lot simpler than otherwise...

#if ENABLE_FRAGMENT_EVENTS
	        // Trigger break above events for all the root groups still attached
	        atBitSet* groupBroken = entry->GetHierInst()->groupBroken;
	        for (int groupIndex = 0; groupIndex < physicsLOD->GetRootGroupCount(); ++groupIndex)
	        {
	            fragTypeGroup* rootGroup = physicsLOD->GetGroup(groupIndex);
	            rootGroup->TriggerAllBreakFromRootEvents(m_CurrentLOD, type, GetSkeleton(), this, this, groupBroken);
	        }
#endif // ENABLE_FRAGMENT_EVENTS

	        SetBroken(true);

			//if the object is inactive(for fragment, we're inactive unless/until we break) - make the object active now...
			if (PHLEVEL->IsInactive(GetLevelIndex()))
			{
				PHSIM->ActivateObject(this);
			}

			if (!findBreakInfo->breakLatch)
			{
				if (phArticulatedBody* body = entry->GetHierInst()->body)
				{
					if (body->RootIsFixed())
					{
						entry->SetFixedArticulatedRoot(false);
					}
				}
			}

			//go see if we're further breakable - we're further breakable if more than one group exists...
			if (entry->IsFurtherBreakable())
			{
				//add to the list of further breakables...
				breakableInstList[*numBreakInsts] = this;
				*numBreakInsts = *numBreakInsts + 1;
			}

			//whether or not we're further breakable, we obviously still have contacts...
			*brokenInstList = this;
			for (int groupIndex = 0; groupIndex < physicsLOD->GetRootGroupCount(); ++groupIndex)
			{
				fragTypeGroup* rootGroup = physicsLOD->GetGroup(groupIndex);
				if(rootGroup->GetDamageWhenBroken() && entry->IsGroupUnbroken(groupIndex))
				{
					ReduceGroupDamageHealth(groupIndex, -rootGroup->GetDamageHealth(), Vec3V(V_ZERO), Vec3V(V_ZERO) FRAGMENT_EVENT_PARAM(true));
				}
			}
			return 1;
		}
		else
		{
			fragCacheEntry* reinsert[2] = { NULL, NULL };

			//compute as if we have a full-on break
			BreakEffects(reinsert, findBreakInfo);

			if (reinsert[0])
			{
				bool isInstA = false;
				int childIndex = -1;

				if (pInstanceA == this)
				{
					childIndex = componentA;
					isInstA = true;
				}
				else if (pInstanceB == this)
				{
					childIndex = componentB;
					isInstA = false;
				}	

				if(childIndex >= 0)
				{
					//one of none of the two re-inserts maps to this component
					const fragTypeChild *curChild = physicsLOD->GetChild(childIndex);
					const int curGroupIndex = curChild->GetOwnerGroupPointerIndex();
					if (reinsert[0]->GetHierInst()->groupBroken->IsClear(curGroupIndex))
					{
						//it better not exist in the other one!
						Assert(reinsert[1] == NULL || reinsert[1]->GetHierInst()->groupBroken->IsSet(curGroupIndex));

						entryHasContacts[0] = true;

						//component numbers always stay the same...
						if (isInstA)
						{
							Assert( pInstanceA );
							pInstanceA = reinsert[0]->GetInst();
						}
						else
						{
							Assert( pInstanceB );
							pInstanceB = reinsert[0]->GetInst();
						}
					}
					else if (reinsert[1])
					{
						if (reinsert[1]->GetHierInst()->groupBroken->IsClear(curGroupIndex))
						{
							entryHasContacts[1] = true;
							if (isInstA)
							{
								Assert( pInstanceA );
								pInstanceA = reinsert[1]->GetInst();
							}
							else
							{
								Assert( pInstanceB );
								pInstanceB = reinsert[1]->GetInst();
							}
						}
						else
						{
							if (isInstA)
							{
								Assert( pInstanceA );
								pInstanceA = reinsert[0]->GetInst();
							}
							else
							{
								Assert( pInstanceB );
								pInstanceB = reinsert[0]->GetInst();
							}
						}
					}
					else
					{
						if (isInstA)
						{
							Assert( pInstanceA );
							pInstanceA = reinsert[0]->GetInst();
						}
						else
						{
							Assert( pInstanceB );
							pInstanceB = reinsert[0]->GetInst();
						}
					}
				}

				//see if each object is further breakable...
				for (int entry = 0; entry < 2; ++entry)
				{
					if (entryHasContacts[entry])
					{
						//add it to the list of things that can still participate in collision...
						*brokenInstList++ = reinsert[entry]->GetInst();

						if (reinsert[entry]->IsFurtherBreakable())
						{
							//add to the list of further breakables...
							breakableInstList[*numBreakInsts] = reinsert[entry]->GetInst();
							*numBreakInsts = *numBreakInsts + 1;
						}
					}
				}
			}
		}
	}

	//return the number of still collidable objects...	
	return !!entryHasContacts[0] + !!entryHasContacts[1];
}

phInst* fragInst::BreakApart(const phBreakData& breakData)
{
	PF_FUNC(BreakApart);

	// This check was added as a hopeful fix for crash we'd seen in this function
	// Might be an idea to at least have an assert here to check if this code is still needed?
	//
	// Need to see what the rage guys think
	Assert(PHLEVEL->LegitLevelIndex(GetLevelIndex()));

	const fragType* type = GetType();
	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	Assert(type);

	if (!GetCached())
	{
		if(!type->GetNeedsCacheEntryToActivate())
		{
			//this is a simple fragment, so we always break into ourselves...
			Assert(!GetBroken());
			SetBroken(true);
			if (PHLEVEL->IsInactive(GetLevelIndex()))
			{
				phCollider *newCollider = PHSIM->ActivateObject(this);
				PHSIM->GetContactMgr()->UpdateColliderInManifoldsWithInstance(this, newCollider);
			}
			SetBroken(true);

#if ENABLE_FRAGMENT_EVENTS
			//trigger events we have been set up with
			fragTypeChild* breakChild = physicsLOD->GetChild(0);
			Assert(breakChild);

			const evtSet* set = breakChild->GetBreakEventset();
			if (set && set->GetNumInstances() > 0)
			{
				Assert(breakChild->GetBreakEventPlayer());
				fragBreakEventPlayer& player = *breakChild->GetBreakEventPlayer();

				player.GetPosition() = GetPosition();
				player.GetMatrix() = GetMatrix();
				player.GetFragInst() = this;
				player.GetInst() = (phInst*) this;
				player.GetComponent() = 0;
				player.GetOtherInst() = (phInst*)this;

				player.StartUpdateStop();
			}
#endif // ENABLE_FRAGMENT_EVENTS

			//apply any damage...
			Assert(type->GetRootChild() || physicsLOD->GetChild(0));
			//damageHealth -= findBreakInfo->aTempBreakDamage[0].fTempDamageHealth;

			return this;
		}
		else
		{
			PutIntoCache();
		}
	}

	phInst* newInst = NULL;

	if (fragCacheEntry* entry = GetCacheEntry())
	{
		//if we're breaking off at the root, skip BreakEffects
		const fragFindBreakInfo* findBreakInfo = reinterpret_cast<const fragFindBreakInfo*>(&breakData);

		//we might have just done a root break, because we had breakGroup == 0xFF, or we broke off
		//all the parts we had left. either way, our job is a whole lot simpler than otherwise...
		if (findBreakInfo->breakGroup == 0xFF)
		{
#if ENABLE_FRAGMENT_EVENTS
			// Trigger break above events for all the root groups still attached
			atBitSet* groupBroken = entry->GetHierInst()->groupBroken;
			for (int groupIndex = 0; groupIndex < physicsLOD->GetRootGroupCount(); ++groupIndex)
			{
				fragTypeGroup* rootGroup = physicsLOD->GetGroup(groupIndex);
				rootGroup->TriggerAllBreakFromRootEvents(m_CurrentLOD, type, GetSkeleton(), this, this, groupBroken);
			}
#endif // ENABLE_FRAGMENT_EVENTS

			SetBroken(true);

			//if the object is inactive(for fragment, we're inactive unless/until we break) - make the object active now...
			if (PHLEVEL->IsInactive(GetLevelIndex()))
			{
				phCollider *newCollider = PHSIM->ActivateObject(this);
				PHSIM->GetContactMgr()->UpdateColliderInManifoldsWithInstance(this, newCollider);
			}

			if (!findBreakInfo->breakLatch)
			{
				if (phArticulatedBody* body = entry->GetHierInst()->body)
				{
					if (body->RootIsFixed())
					{
						entry->SetFixedArticulatedRoot(false);
					}
				}
			}

			for (int groupIndex = 0; groupIndex < physicsLOD->GetRootGroupCount(); ++groupIndex)
			{
				fragTypeGroup* rootGroup = physicsLOD->GetGroup(groupIndex);
				if(rootGroup->GetDamageWhenBroken() && entry->IsGroupUnbroken(groupIndex))
				{
					ReduceGroupDamageHealth(groupIndex, -rootGroup->GetDamageHealth(), Vec3V(V_ZERO), Vec3V(V_ZERO) FRAGMENT_EVENT_PARAM(true));
				}
			}

			newInst = this;
		}
		else
		{
			fragCacheEntry* reinsert[2] = { NULL, NULL };

			//compute as if we have a full-on break
			BreakEffects(reinsert, findBreakInfo);

			if (reinsert[0])
			{
				newInst = reinsert[0]->GetInst();
			}
		}
	}

	return newInst;
}

bool HasChildWithCloth(fragTypeGroup* pFragGroup, const fragPhysicsLOD* pFragPhysicsLOD)
{
	Assert( pFragGroup );
	Assert( pFragPhysicsLOD );

	bool hasCloth = pFragGroup->GetHasCloth();

	u8 numChildGroups = pFragGroup->GetNumChildGroups();
	if( numChildGroups )
	{
		u8 firstChildGroup = pFragGroup->GetChildGroupsPointersIndex();
		Assert( firstChildGroup < 0xff );
		for (int childGroupIndex = 0; childGroupIndex < numChildGroups; ++childGroupIndex)
		{
			fragTypeGroup* pChildGroup = pFragPhysicsLOD->GetGroup(firstChildGroup + childGroupIndex);
			Assert( pChildGroup );
			hasCloth |= HasChildWithCloth(pChildGroup, pFragPhysicsLOD);
		}
	}
		
	return hasCloth;
}

void fragInst::BreakEffects(fragCacheEntry* reinsert[2], const fragFindBreakInfo* findBreakInfo)
{
	const fragType* type = GetType();
	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	Assert(type);

	fragCacheEntry* entry = GetCacheEntry();

	// Once we are breaking we are definitely not allowed to lose our cache entry this frame
	entry->SetSequenceNumber();
	fragHierarchyInst* hierInst = entry->GetHierInst();

	// Find all of the groups breaking
	ComponentBits groupsBreaking;
	physicsLOD->GetGroupsAbove(findBreakInfo->breakGroup,groupsBreaking);

	// Find the children that are breaking
	ComponentBits childrenBreaking;
	physicsLOD->ConvertGroupBitsetToChildBitset(groupsBreaking,childrenBreaking);

	//make sure we're got at least one fragment child somewhere in the new hierarchy...
	bool anyLeft = false;
	for (int curGroupIndex = 0; !anyLeft && curGroupIndex < physicsLOD->GetNumChildGroups(); ++curGroupIndex)
	{
		if (groupsBreaking.IsClear(curGroupIndex) && entry->GetHierInst()->groupBroken->IsClear(curGroupIndex))
		{
			anyLeft = true;
		}
	}

	Assert(findBreakInfo->breakGroup != 0xFF);
	fragTypeGroup* breakGroup = physicsLOD->GetGroup(findBreakInfo->breakGroup);
	Assert(breakGroup);
	u8 breakChildIndex = breakGroup->GetChildFragmentIndex();

	if (breakGroup->GetMadeOfGlass())
	{
		int glassCrackType = 0;
		fragManager::ComputeGlassCrackTypeCallbackFunctor computeCrackTypeCallback =
			FRAGMGR->GetComputeGlassCrackTypeCallback();

		if (computeCrackTypeCallback.IsBound())
		{
			glassCrackType = computeCrackTypeCallback();
		}

		entry->ShatterGlassOnBone(findBreakInfo->breakGroup, type->GetBoneIndexFromID(physicsLOD->GetChild(breakGroup->GetChildFragmentIndex())->GetBoneID()), findBreakInfo->position.GetXYZ() / findBreakInfo->position.GetW(), findBreakInfo->impulse, glassCrackType);
		HideDeadGroup(findBreakInfo->breakGroup, 0);
		return;
	}

	if (!anyLeft)
	{
		//if we're breaking off all, then we're breaking off none
		// Now groupBroken takes on a slightly different meaning.
		// In fact, this should never be able to happen now and will result in an infinite loop in the breaking system and should be changed to an assert.
		return;
	}

	// Propagate all velocities and reset the incremental base (to prevent a crash due to the base being higher than the number of links)
	if (hierInst && hierInst->body)
		hierInst->body->EnsureVelocitiesFullyPropagated();

	if (findBreakInfo->breakLatch)
	{
		// Sync up the collider to the instance matrix. 
		// If the collider is active the instance should be lined up. If it's inactive we should go with the instance position. 
		if(hierInst->articulatedCollider)
		{
			hierInst->articulatedCollider->SetColliderMatrixFromInstance();
		}

		// Ensure not in simple collider mode
		ForceArticulatedColliderMode();

		// We seem to be getting NULL joints back here at least in network games... possibly because we come through
		// here again after NULLing the latchedJoints entry below? At least protect from crashing.
		if(	!Verifyf(type->WillBeArticulated(findBreakInfo->breakGroup), "Trying to unlatch non-articulated group on '%s'", physicsLOD->GetArchetype()->GetFilename()) ||
			!Verifyf(hierInst->latchedJoints && hierInst->latchedJoints->IsSet(findBreakInfo->breakGroup), "Trying to break latch when already unlatched: breakGroup=%u", findBreakInfo->breakGroup))
		{
			return;
		}
		// And zero out the pointer here so we don't get confused later.
		hierInst->latchedJoints->Clear(findBreakInfo->breakGroup);

		// Compute the instance-from-link matrix for the soon-to-be parent link
		u8 parentLinkIndex = (u8)hierInst->articulatedCollider->GetLinkFromComponent(breakChildIndex);
		u8 childIndex = physicsLOD->GetGroup(findBreakInfo->breakGroup)->GetChildFragmentIndex();

		// Update the link attachment matrices so we can find the new link
		// NOTE: This should actually be completely recalculating the link, and the parent link CG
		//       and updating both of their linkAttachment matrices, not just resetting them. 
		entry->ResetLinkAttachmentRecurseChildren(*breakGroup);

		// Now that the link attachment matrices are updated, we can find the position and orientation of the new link
		Mat34V componentFromNewLink;
		InvertTransformOrtho(componentFromNewLink, RCC_MAT34V(hierInst->articulatedCollider->GetLinkAttachmentMatrices()[childIndex]));
		Mat34V_ConstRef instanceFromComponent = hierInst->compositeBound->GetCurrentMatrix(childIndex);
		Mat34V instanceFromNewLink;
		Transform(instanceFromNewLink, instanceFromComponent, componentFromNewLink);

		Mat34V worldFromNewLink;
		Transform(worldFromNewLink, GetMatrix(), instanceFromNewLink);

		u8 numChildren = physicsLOD->GetNumChildren();

		// Get the undamaged mass properties from each child
		ScalarV_Ptr childMasses = Alloca(ScalarV,numChildren);
		Vec3V_Ptr childAngularInertias = Alloca(Vec3V,numChildren);
		Vec3V_Ptr childCentersOfGravity = Alloca(Vec3V,numChildren);
		physicsLOD->ComputeUndamagedChildMassProperties(childMasses,childAngularInertias,childCentersOfGravity);

		// Find out which children are unlatching and not broken
		fragGroupBits groupsUnlatching;
		physicsLOD->GetGroupsAbove(findBreakInfo->breakGroup,groupsUnlatching);
		fragChildBits childrenUnlatching;
		physicsLOD->ConvertGroupBitsetToChildBitset(groupsUnlatching,childrenUnlatching);
		fragChildBits childrenAlreadyBroken;
		physicsLOD->ConvertGroupBitsetToChildBitset(*hierInst->groupBroken,childrenAlreadyBroken);
		childrenUnlatching.IntersectNegate(childrenAlreadyBroken);

		phMathInertia::MassAccumulatorBitSet<fragChildBits> linkMassAccumulator(childrenUnlatching);
		phMathInertia::ComputeCombinedMassAngularInertiaAndCenterOfGravity(numChildren,childMasses,childAngularInertias,childCentersOfGravity,linkMassAccumulator);
		
		// Compute the mass of the new part, this should really include the whole mass of the group.
		phJoint& newJoint = entry->AddJointAndLink(	parentLinkIndex,
													worldFromNewLink.GetMat33ConstRef(),
													instanceFromNewLink.GetCol3(),
													linkMassAccumulator.GetAngularInertia(),
													linkMassAccumulator.GetMass(),
													findBreakInfo->breakGroup,
													true);

		entry->UpdateSavedVelocityArraySizes();
		hierInst->body->ResetPropagationVelocities();
#if __ASSERT
		entry->CheckVelocities();
#endif
		u8 newLinkIndex = newJoint.GetChildLinkIndex();

		// Add it to the body and set the joint's child index
		PS3_ONLY(hierInst->articulatedCollider->RegenerateCoreDmaPlan());

		// Modify the mass of the root link
		ScalarV oldMass = hierInst->body->GetMass(0);
		hierInst->body->SetMassOnly(0, oldMass - hierInst->body->GetMass(newLinkIndex));

		// Compute some values in the articulated body to apologize for the fact that we just changed its structure
		hierInst->body->CalculateInertias();
		hierInst->articulatedCollider->FindJointLimitDofs();

		// Go through the children and assign their "link for component" map entries in the collider
//		fragTypeGroup* breakGroup = GetTypePhysics()->GetGroup(findBreakInfo->breakGroup);
		u8 firstChildGroup = breakGroup->GetChildGroupsPointersIndex();
		u8 numChildGroups = breakGroup->GetNumChildGroups();

		Assert(hierInst->articulatedCollider->IsArticulated());
		phArticulatedCollider* articulatedCollider = hierInst->articulatedCollider;

		// For the first iteration on the children, get our children first
		fragTypeGroup* childGroup = breakGroup;
		for (int childGroupIndex = 0; childGroupIndex < numChildGroups + 1; ++childGroupIndex)
		{
			u8 breakChildIndex = childGroup->GetChildFragmentIndex();
			u8 numChildren = childGroup->GetNumChildren();
			for (int childIndex = 0; childIndex < numChildren; ++childIndex)
			{
				articulatedCollider->SetLinkForComponent(breakChildIndex + childIndex, newLinkIndex);
			}

			// Set the childGroup at the bottom here, so that the second iteration covers the first child group
			if(childGroupIndex < numChildGroups)
			{
				childGroup = physicsLOD->GetGroup(firstChildGroup + childGroupIndex);
			}
		}

		PoseArticulatedBodyFromBounds();

		return;
	}

	// Do not allow any breaking at all if the user has explicitly forced this fragment to not break
	if(IsBreakingDisabled())
	{
		return;
	}

	if(Verifyf(PHSIM->AllowBreaking(),"Trying to break '%s' when the simulator isn't allowing breaking",GetArchetype()->GetFilename()))
	{
		//grab a new cache entry that we'll break into...
		fragCacheEntry* newEntry = FRAGCACHEMGR->GetNewCacheEntry();
		bool wereManifoldsTransferred = false;
		if (newEntry)
		{
			// Have to pose the skeleton from the bounds before using it to init the new cache entry, because otherwise
			// last frames' joint positions will be used to compute the local matrices of the new cache entry
			PoseSkeletonFromArticulatedBody();
	
			//init the entry from the root one...
			newEntry->Init(m_Type,RCC_MATRIX34(GetMatrix()),NULL,findBreakInfo->breakGroup,NULL,entry);
			newEntry->GetHierInst()->parentInst = entry->GetHierInst()->parentInst;
	
			// Make sure this new instance is active and set up properly.
			PrepareBrokenInst(newEntry->GetInst());
			newEntry->GetInst()->SetBroken(true);
			newEntry->GetInst()->SetInsertionState(phLevelBase::OBJECTSTATE_ACTIVE);
			newEntry->GetInst()->Insert();
			fragHierarchyInst* newHierInst = newEntry->GetHierInst();
			fragInst* newInst = newEntry->GetInst();
			reinsert[0] = newEntry;
	
			if (newEntry->GetInst()->IsInLevel())
			{
				//give the new instance the right amount of motion, if the root object is actually active...
				if (PHLEVEL->IsActive(GetLevelIndex()) && PHLEVEL->IsActive(newEntry->GetInst()->GetLevelIndex()))
				{
					Vector3 centerOfMass;
					Vector3	velocity;
					Vector3	angVelocity;
	
					FindPostBreakMotion(centerOfMass,velocity,angVelocity);
	
					newEntry->GetInst()->ThrowBrokenPart(newEntry->GetInst(),centerOfMass,velocity,angVelocity);
				}
				PHLEVEL->UpdateObjectLocation(newEntry->GetInst()->GetLevelIndex());
	
				// We made a new instance and put it in the level so we need to transfer manifolds with the breaking components to the new instance
				wereManifoldsTransferred = true;
				PHSIM->GetContactMgr()->ReplaceInstanceComponents(this, childrenBreaking, newEntry->GetInst());

				// Copy glass info across to the new cache entry
				for(int glassInfoIndex = 0; glassInfoIndex < hierInst->numGlassInfos; ++glassInfoIndex)
				{
					fragHierarchyInst::GlassInfo& glassInfo = hierInst->glassInfos[glassInfoIndex];
					if(groupsBreaking.IsSet(glassInfo.groupIndex) && glassInfo.handle != bgGlassHandle_Invalid)
					{
						// Don't allocate unless we know glass is transferring
						newEntry->AllocateGlassInfo();

						fragHierarchyInst::GlassInfo& newGlassInfo = newHierInst->glassInfos[newHierInst->numGlassInfos++];

						// Deep copy the glass info and clear the handle on the original entry so we don't delete it
						newGlassInfo = glassInfo;
						bgGlassManager::TransferBreakableGlassOwner(&glassInfo.handle, &newGlassInfo.handle, newInst);

						GLASS_RECORDER_ONLY(if(glassRecordingRage::IsActive()) { glassRecordingRage::Get()->OnTransferGlass(entry->GetInst(), newEntry->GetInst(), glassInfo.groupIndex); })
					}
				}
			}
	
		
			bool hasCloth = HasChildWithCloth(breakGroup, physicsLOD);
// NOTE: transfer cloth
			if( hasCloth )
			{
				environmentCloth* pEnvCloth = hierInst->envCloth;
				if( pEnvCloth )
				{
					pEnvCloth->SetAttachedToFrame( RCC_MATRIX34(newEntry->GetInst()->GetMatrix()) );

					fragHierarchyInst* pNewHierInst = newEntry->GetHierInst();
					Assert( pNewHierInst );

					phClothVerletBehavior* pClothBehavior = (phClothVerletBehavior*)pEnvCloth->GetBehavior();
					Assert( pClothBehavior );
					pClothBehavior->SetInstance( *newEntry->GetInst() );

					pNewHierInst->envCloth = pEnvCloth;							
					hierInst->envCloth = NULL;
				}
			}

	#if ENABLE_FRAGMENT_EVENTS
		    // If the root instance is still attached to the root, then this means that means that the connection between the piece that's breaking off
			//   and the root is getting severed now.  Since that is the case, we want to trigger all of the 'break from root' events on the piece that's
			//   breaking off.
		    if (!GetBroken())
		    {
		        atBitSet* groupBroken = entry->GetHierInst()->groupBroken;
		        breakGroup->TriggerAllBreakFromRootEvents(m_CurrentLOD, type, GetSkeleton(), this, newEntry->GetInst(), groupBroken);
		    }
	#endif // ENABLE_FRAGMENT_EVENTS
	
			// While this instance still has the breaking groups, find out if this is a mid link break
			// If it is a mid link break we need to recompute the cg, mass, angular inertia and link
			//   attachment matrices of this link. 
			bool midLinkBreak = false;
			u8 parentGroupIndex = breakGroup->GetParentGroupPointerIndex();
			phArticulatedCollider* articulatedCollider = entry->GetHierInst()->articulatedCollider;
			if(articulatedCollider && entry->GetHierInst()->body)
			{
				if(parentGroupIndex != 0xFF)
				{
					u8 childInParentGroupIndex = physicsLOD->GetGroup(parentGroupIndex)->GetChildFragmentIndex();
					if(articulatedCollider->GetLinkFromComponent(childInParentGroupIndex) == articulatedCollider->GetLinkFromComponent(breakChildIndex))
					{
						midLinkBreak = true;
					}
				}
			}
	
			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Adjust the root instance for its loss of the group(s).
	
			// Find the new mass.
			float fNewRootMass = entry->AdjustForLostGroups(groupsBreaking);
	
			//if we lost all our parts, we should have returned up above before making a new cache entry
			Assert(fNewRootMass > 0.0f); 
	
			//compute new mass, radius, etc.
			// Really this does far more than simply setting a new mass.  SetMass(), combined with AdjustForLostGroups(), seem to handle most of what's
			//   necessary for updating things physically when group(s) are lost.
			entry->SetMass(fNewRootMass);
			PHLEVEL->UpdateObjectLocationAndRadius(GetLevelIndex(), (Mat34V_Ptr)(NULL));
	
			if(midLinkBreak)
			{
				// latched joints rely on link attachment matrices not being changed
				if(entry->GetHierInst()->latchedJoints == NULL)
				{
					// Now that this instance no longer own the breaking groups, it is safe to recompute the link of the parent
					//   of the group breaking. 
					entry->RecomputeLink(parentGroupIndex);
				}
			}
			//
			////////////////////////////////////////////////////////////////////////////////////////////////////
	
			reinsert[1] = entry;
	
	#if ENABLE_FRAGMENT_EVENTS
			Vector3 breakPosition(FLT_MAX, FLT_MAX, FLT_MAX);
	
			for (int childIndex = 0; childIndex < breakGroup->GetNumChildren(); ++childIndex)
			{
				fragTypeChild* breakChild = physicsLOD->GetChild(breakChildIndex + childIndex);
				Assert(breakChild);
	
				const evtSet* set = breakChild->GetBreakEventset();
				if (set && set->GetNumInstances() > 0)
				{
					Matrix34 breakMatrix;
					const bool kAttachEventToBrokenPiece = false;
					if(!kAttachEventToBrokenPiece)
					{
						// We're going to attach the player to the skeleton of the bone of the parent group from which we're breaking in the instance from
						//   which the breaking is occurring.
						int parentGroupIndex = breakGroup->GetParentGroupPointerIndex();
						if(parentGroupIndex != 0xFF)
						{
							fragTypeGroup *parentGroup = physicsLOD->GetGroup(parentGroupIndex);
							fragTypeChild *parentChild = physicsLOD->GetChild(parentGroup->GetChildFragmentIndex());
							int parentBoneIndex = type->GetBoneIndexFromID(parentChild->GetBoneID());
							if(parentBoneIndex != -1)
							{
								crSkeleton *skeleton = GetSkeleton();
								Assert(skeleton != NULL);
								skeleton->GetGlobalMtx(parentBoneIndex, RC_MAT34V(breakMatrix));
								breakPosition.Set(breakMatrix.d);
							}
						}
					}
					else
					{
						// For the event player position and matrix we're going to try and point them to the global matrix in the skeleton of the bone that
						//   corresponds to the group that is breaking off, *in the new instance*.  Note that we really don't want to point to anything in the
						//   skeleton of the original instance because those matrices could get zero'd out later if something breaks off.
						fragTypeChild* breakingChild = physicsLOD->GetChild(breakGroup->GetChildFragmentIndex());
						int breakBone = type->GetBoneIndexFromID(breakingChild->GetBoneID());
						if (breakBone != -1)
						{
							crSkeleton *skeleton = newEntry->GetInst()->GetSkeleton();
							Assert(skeleton != NULL);
							skeleton->GetGlobalMtx(breakBone, RC_MAT34V(breakMatrix));
							breakPosition.Set(breakMatrix.d);
						}
					}
	
				    // Couldn't give a dog a bone, pick a point as close to the parent as possible on the child's bounding box
				    if (breakPosition.x == FLT_MAX)
				    {
					    phInst* newInst = newEntry->GetInst();
					    phBound* bound = newInst->GetArchetype()->GetBound();
					    breakPosition = VEC3V_TO_VECTOR3(bound->GetBoundingBoxMin());
						RC_VEC3V(breakPosition) = Transform( newInst->GetMatrix(), RCC_VEC3V(breakPosition) );
				    }
	
					Assert(breakChild->GetBreakEventPlayer());
				    fragBreakEventPlayer& player = *breakChild->GetBreakEventPlayer();
	
				    player.GetPosition() = RCC_VEC3V(breakPosition);
					player.GetMatrix() = RCC_MAT34V(breakMatrix);
				    player.GetFragInst() = this;
		            player.GetInst() = (phInst*)this;
				    player.GetComponent() = (int)breakGroup->GetChildFragmentIndex();
				    player.GetOtherInst() = (phInst*)newEntry->GetInst();
	
				    player.StartUpdateStop();
				}
			}
	#endif // ENABLE_FRAGMENT_EVENTS

			FRAGMGR->GetPartsBrokeOffFunc()(this,groupsBreaking,newEntry->GetInst());
	
			fragTypeGroup* breakGroup = physicsLOD->GetGroup(findBreakInfo->breakGroup);
			if(breakGroup->GetDamageWhenBroken())
			{
				// If flagged for damage when broken, damage the fragment last. Otherwise it can be destroyed if Disappear when dead is set and
				//   then the fragment becomes invalid.
				Collision collisionMsg;
				collisionMsg.position = Vec3V(V_ZERO);
				collisionMsg.component = breakGroup->GetChildFragmentIndex();
				collisionMsg.otherComponent = 0;
				collisionMsg.part = 0;
				collisionMsg.otherPart = 0;
				collisionMsg.relativeVelocity = Vec3V(V_ZERO);
				collisionMsg.relativeSpeed = 0.0f;
				collisionMsg.normal = Vec3V(V_ZERO);
				collisionMsg.relativeVelocity = Vec3V(V_ZERO);
				collisionMsg.otherInst = NULL;
	
				// Apply enough damage so that the final health is the negative of the group's damage health
				collisionMsg.damage = newEntry->GetHierInst()->groupInsts[findBreakInfo->breakGroup].damageHealth + breakGroup->GetDamageHealth();
				newInst->CollisionDamage(collisionMsg, NULL);
			}
	
			if (entry && entry->GetHierInst()&& entry->GetHierInst()->body)
			{
				entry->UpdateSavedVelocityArraySizes();
				entry->GetHierInst()->body->ResetPropagationVelocities();
	#if __ASSERT
				entry->CheckVelocities();
	#endif
			}
			if (newEntry && newEntry->GetHierInst() && newEntry->GetHierInst()->body)
			{
				newEntry->UpdateSavedVelocityArraySizes();
				newEntry->GetHierInst()->body->ResetPropagationVelocities();
	#if __ASSERT
				newEntry->CheckVelocities();
	#endif
			}
		}
	
		// If we weren't able to transfer the manifolds just erase them on the original instance
		if(!wereManifoldsTransferred)
		{
			PHSIM->GetContactMgr()->RemoveAllContactsWithInstance(this, childrenBreaking);
		}
	}
}


void fragInst::ApplyForce (const Vector3& force, const Vector3& position, int component, int element, float breakScale)
{
	phInst::ApplyForce(force,position,component,element,breakScale);
}


void fragInst::ReportTypePagedIn()
{
	/*
	Core to this code is that we assume that a type stub will never be moved out from under us, and the 
	type loading code asserts this.  As such, we are safe in assuming that if we get here, we told the 
	type that we're a client, but at that time, we were unable to add to the physics system because 
	the entity and bounds we need didn't exist yet...
	*/

	// We shouldn't be here unless we've been requested to be inserted.
	Assert(GetInserted());

	// We shouldn't actually be in the level until our type is paged in.
	Assert(!IsInLevel());
	Assert(GetType());
	Assert(GetTypePhysics()->GetArchetype());

	if(GetTypePhysics()->GetMinMoveForce() == 0.0f || GetType()->GetHasAnyArticulatedParts())
	{
		SetBroken(true);
	}

	if (!GetCached())
	{
		SetArchetype(GetTypePhysics()->GetArchetype());
	}

	SetBounds(GetType()->GetBoundingSphere());

	// If we've already been put in the cache, but our type hasn't been paged in yet, we need to make sure that
	//   our cache entry gets set up first before we try and update our bounds or anything.
	if (GetCached())
	{
		GetCacheEntry()->FinishInit();
	}
    else
    {
        m_DamageHealth = GetTypePhysics()->GetGroup(0)->GetDamageHealth();
    }

	Assert(GetArchetype()->GetBound());

	if(GetInsertionState() == phLevelBase::OBJECTSTATE_INACTIVE)
	{
		PHSIM->AddInactiveObject(this);
	}
	else if(GetInsertionState() == phLevelBase::OBJECTSTATE_FIXED)
	{
		PHSIM->AddFixedObject(this);
	}
	else if(GetInsertionState() == phLevelBase::OBJECTSTATE_ACTIVE)
	{
		PHSIM->AddActiveObject(this);
	}

	// This should come after we set the archetype because, oh, I don't know, maybe we would like to use it in here?
	// This needs to be done after inserting because otherwise we don't update the skeleton, etc. in here
	ReportMovedBySim();

	if(!GetCached() && ShouldBeInCacheToDraw() ENABLE_FRAG_OPTIMIZATION_ONLY(&& !IsCacheOnInsertionDisabled()))
	{
		PutIntoCache();
	}

#if !__RESOURCECOMPILER && __ASSERT && USE_CLOTH_PHINST
	// If this is environment cloth, make sure it has a bound.
	if (GetType()->GetNumEnvCloths()>0)
	{
		const phBound* bound  = GetArchetype()->GetBound();
		Assertf(bound,"Found cloth object without physics bounds, of type '%s'.", GetType()->GetBaseName());
		if (bound && bound->GetType()==phBound::COMPOSITE)
		{
			const phBoundComposite* compositeBound = static_cast<const phBoundComposite*>(bound);
			int numActiveBounds =  compositeBound->GetNumActiveBounds();
#if !__NO_OUTPUT
			if(!numActiveBounds)
			{
				clothErrorf("[Most likely run out of streaming memory] Found cloth object without physics bounds, of type '%s'.", GetType()->GetBaseName());
			}
#endif
		}
	}
#endif // !__RESOURCECOMPILER && __ASSERT

#if __ASSERT
	++fragManager::sm_PhysicsInstancesAdded;
#endif
}

void fragInst::ReportTypePagedOut()
{
	if (GetCached())
	{
		FRAGCACHEMGR->ReleaseCacheEntry(GetCacheEntry());
	}

//	FRAGMGR->RemoveMovable(m_MovableHandle);

	//we're in the physics world - get out...
	if (IsInLevel())
	{

		PHSIM->DeleteObject(GetLevelIndex());

#if __ASSERT
		--fragManager::sm_PhysicsInstancesAdded;
#endif
	}

	SetArchetype(NULL);
}



bool fragInst::ShouldBeInCacheToDraw() const
{
	const fragType* type = GetType();
	Assert(type);

	if (type->GetNumEnvCloths()>0 || type->GetNumCharCloths()>0)
	{
		return true;
	}

#if ENABLE_FRAGMENT_EVENTS
	const fragPhysicsLOD* physicsLOD = GetTypePhysics();
	for (int child = 0; child < physicsLOD->GetNumChildren(); ++child)
	{
		if (evtSet* events = physicsLOD->GetChild(child)->GetContinuousEventset())
		{
			if (events->GetNumInstances() > 0)
			{
				return true;
			}
		}
	}
#endif // ENABLE_FRAGMENT_EVENTS

	return false;
}

/*this needs some work since it knows nothing about the current FOV - smaller FOV should make distance less of a factor 
and make angle more of a factor...Something to tweak later if anyone complains...
*/
f32 fragInst::GetImpact(f32 invMaxDistance, 
	bool useDistanceOnly, 
	const Matrix34** worldToViewMats, 
	u32 numObserverMats)
{
	Assert(numObserverMats);
	float impact = 0.0f;
	for (u32 matrix = 0; matrix < numObserverMats; ++matrix, ++worldToViewMats)
	{
		Vector3		tempVec;

		//xform our current position into view space...
		(*worldToViewMats)->Transform(VEC3V_TO_VECTOR3(m_InstanceXFormSphere.GetCenter()), tempVec);

		//compute the distance impact...
		float tempMag = tempVec.Mag();
		float temp = tempMag * invMaxDistance;
		temp = (temp > 1.0f) ? 1.0f : temp;
		temp = 1.0f - temp;

		if (useDistanceOnly == false)
		{
			//compute the angular impact - really just a modification of the normalized z component of the vector...
			temp *= (((-tempVec.z / tempMag) + 1.0f) * .25f) + .5f; //in front stays 1.0f, and behind becomes .5f...
		}

		if (temp > impact)
		{
			impact = temp;
		}
	}

	return impact / m_InstanceXFormSphere.GetRadiusf();
}

int fragInst::GetARTAssetID() const
{
	return -1;
}

void fragInst::MakeKey(fragCacheKey* cacheKey)
{
	Assert(cacheKey);
	*cacheKey = fragCacheKey(m_Guid);
}

bool fragInst::AddToDrawList()
{
	if (const fragType* type = GetType())
	{
		if (GetCached())
		{
			fragCacheEntry* entry = GetCacheEntry();

#if ENABLE_FRAG_OPTIMIZATION
			type->Draw(m_CurrentLOD, RCC_MATRIX34(GetMatrix()), GetSkeleton(), NULL, entry->GetHierInst());
#else
			type->Draw(m_CurrentLOD, RCC_MATRIX34(GetMatrix()), GetSkeleton(), entry->GetHierInst());
#endif			
		}
		else if (!GetDead())
		{
#if ENABLE_FRAG_OPTIMIZATION
			type->Draw(m_CurrentLOD, RCC_MATRIX34(GetMatrix()), GetSkeleton());
#else
			type->Draw(m_CurrentLOD, RCC_MATRIX34(GetMatrix()), GetSkeleton(), NULL);
#endif				
		}
	}

	return true;
}


void fragInst::LosingCacheEntry()
{
}

void fragInst::LostCacheEntry()
{
	const fragType* type = GetType();
	if (type)
	{
		if (GetTypePhysics()->GetMinMoveForce()!=0.0f && !type->GetHasAnyArticulatedParts())
		{
			// Restore this fragment instance to the unbroken state.
			SetBroken(false);
		}

		if (type->GetNumEnvCloths()==1 && GetTypePhysics()->GetNumChildren()==1)
		{
			// Reset this cloth fragment instance. If this isn't done, then it will be positioned incorrectly when it streams back in.
			// Cloth keeps its instance position at its centroid while updating, but it doesn't necessarily start at its centroid.
			Reset();
		}
	}
}

//void fragInst::DebugBounds()
//{
//	phBoundComposite* bound = static_cast<phBoundComposite*>(GetArchetype()->GetBound());
//	Assert(bound->GetType() == phBound::COMPOSITE);
//
//	Matrix34 currentMatrix;
//	Matrix34 lastMatrix;
//
//	Matrix34 currMtx = RCC_MATRIX34(GetMatrix());
//	Matrix34 lastMtx = RCC_MATRIX34(PHSIM->GetLastInstanceMatrix(this));
//
//	static float maxDist = 0.15f;
//
//	for (int part = 0; part < GetTypePhysics()->GetNumChildren(); ++part)
//	{
//		currentMatrix.Dot(RCC_MATRIX34(bound->GetCurrentMatrix(part)), currMtx);
//		currentMatrix.a.w = 0.0f;
//		currentMatrix.b.w = 0.0f;
//		currentMatrix.c.w = 0.0f;
//		currentMatrix.d.w = 1.0f;
//		lastMatrix.Dot(RCC_MATRIX34(bound->GetLastMatrix(part)), lastMtx);
//		lastMatrix.a.w = 0.0f;
//		lastMatrix.b.w = 0.0f;
//		lastMatrix.c.w = 0.0f;
//		lastMatrix.d.w = 1.0f;
//
//		float dist = currentMatrix.d.Dist(lastMatrix.d);
//		if (dist > maxDist)
//		{
//			currMtx = RCC_MATRIX34(GetMatrix());
//		}
//	}
//}

crSkeleton* fragInst::GetSkeleton() const
{
	return GetSkeletonFromFragInst(this);
}

void fragInst::GetMatrixFromEulers (Matrix34& matrix) const
{
	// Get the Euler angles.
	Vector3 eulers;
	UnpackEulersFrom32(eulers,GetPackedEulers());

	Mat34V pose;
	Mat34VFromEulersZYX(pose, VECTOR3_TO_VEC3V(eulers), VECTOR3_TO_VEC3V(m_OrgPos));
	matrix.Set(pose.GetCol0Intrin128(), pose.GetCol1Intrin128(), pose.GetCol2Intrin128(), pose.GetCol3Intrin128());
}

#if __DECLARESTRUCT
datSwapper_ENUM(phLevelBase::eObjectState);

void fragInst::DeclareStruct(datTypeStruct& s)
{
	phInstBreakable::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(fragInst, phInstBreakable)
	SSTRUCT_FIELD(fragInst, m_BitStates)
	SSTRUCT_FIELD(fragInst, m_CacheEntry)
	SSTRUCT_FIELD(fragInst, m_DamageHealth)
	SSTRUCT_FIELD(fragInst, m_Type)
	SSTRUCT_FIELD(fragInst, m_InstanceXFormSphere)
	SSTRUCT_FIELD(fragInst, m_OrgPos)
	SSTRUCT_FIELD(fragInst, m_InsertionState)
	SSTRUCT_FIELD(fragInst, m_Guid)
	SSTRUCT_FIELD(fragInst, m_PackedEulers)
	SSTRUCT_FIELD_VP(fragInst, m_Pad)
	SSTRUCT_FIELD_AS(fragInst, m_CurrentLOD, int)
	SSTRUCT_IGNORE(fragInst, m_Pad2)
	SSTRUCT_END(fragInst);
}
#endif

#if __DEBUGLOG
void fragInst::DebugReplay() const
{
	phInstBreakable::DebugReplay();
	diagDebugLog(diagDebugLogPhysics, 'fIBS', &m_BitStates);
	diagDebugLog(diagDebugLogPhysics, 'fIDH', &m_DamageHealth);
	diagDebugLog(diagDebugLogPhysics, 'fITy', &m_Type);
	diagDebugLog(diagDebugLogPhysics, 'fIXS', &m_InstanceXFormSphere);
	diagDebugLog(diagDebugLogPhysics, 'fIOP', &m_OrgPos);
	diagDebugLog(diagDebugLogPhysics, 'fIIS', &m_InsertionState);
	diagDebugLog(diagDebugLogPhysics, 'fIGu', &m_Guid);
	diagDebugLog(diagDebugLogPhysics, 'fIPE', &m_PackedEulers);
}
#endif

} // namespace rage

#ifndef _CPPRTTI
type_info dummy_type_info;
#endif
