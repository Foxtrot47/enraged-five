// 
// fragment/typechild.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
//

#ifndef FRAGMENT_TYPECHILD_H
#define FRAGMENT_TYPECHILD_H

#include "eventplayer.h"
#include "fragmentconfig.h"

#include "data/base.h"
#include "data/resource.h"
#include "data/struct.h"
#include "vector/matrix34.h"

namespace rage {

class crSkeletonData;
class evtSet;
class fiAsciiTokenizer;
class fragDrawable;
class grmShaderGroup;
struct rmcTypeFileCbData;

/*
  PURPOSE
	Holds type data related to one atomic piece of a fragment type.
	
  NOTES
    Simple fragType's have only one of these, complex fragType's have one of these
	for each piece they can break into. fragTypeChild's are linked together in a complex
	type by fragTypeGroup's.

	<FLAG Component>
*/
class fragTypeChild : datBase
{
friend class fragManager;
friend class fragType;
friend class fragTypeGroup;
friend class fragTypeStub;
friend class fragTuneStruct;
friend class fragPhysicsLOD;
public:
	DECLARE_PLACE(fragTypeChild);
	void Place(datResource& rsc, grmShaderGroup* shaderGroup);
	void UnPlace();
	
	fragTypeChild(	u16 boneID = 0,
					fragDrawable* undamagedEntity = NULL, 
					fragDrawable* damagedEntity = NULL, 
					f32 undamagedMass = -1.0f, 
					f32 damagedMass = -1.0f, 
					u8 parentIndex = 0xFF,
					u8 flags = 0);
	
	fragTypeChild(datResource& rsc);
	fragTypeChild(datResource& rsc, grmShaderGroup* shaderGroup);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	~fragTypeChild();
	void Set(fragTypeChild& other FRAGMENT_EVENT_PARAM(bool setEvents = true));
	
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	Vec3V_Out GetUndamagedAngularInertia() const;
	Vec3V_Out GetDamagedAngularInertia() const;
	void SetUndamagedAngularInertia(Vec3V_In undamagedAngularInertia);
	void SetDamagedAngularInertia(Vec3V_In damagedAngularInertia);
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD

	f32* GetUndamagedMassPtr();
	f32* GetDamagedMassPtr();

	f32 GetUndamagedMass() const;
	f32 GetDamagedMass() const;
	void SetUndamagedMass(f32 fMass);
	void SetDamagedMass(f32 fMass);

	u8 GetOwnerGroupPointerIndex() const;
	u16 GetBoneID () const;
#if ENABLE_FRAGMENT_EVENTS
	evtSet* GetContinuousEventset() const;
	evtSet* GetCollisionEventset() const;
	fragCollisionEventPlayer* GetCollisionEventPlayer();
	evtSet* GetBreakEventset() const;
	fragBreakEventPlayer* GetBreakEventPlayer();
    evtSet* GetBreakFromRootEventset() const;
    fragBreakEventPlayer* GetBreakFromRootEventPlayer();
#endif // ENABLE_FRAGMENT_EVENTS

	fragDrawable* GetUndamagedEntity() const;
	fragDrawable* GetDamagedEntity() const;

	void SetUndamagedEntity(fragDrawable* set) { m_UndamagedEntity = set; }
	void SetDamagedEntity(fragDrawable* set) { m_DamagedEntity = set; }

#if __BANK
	void SaveASCII(fiAsciiTokenizer& pAsciiTok);	
#endif

	static fragTypeChild* LoadASCII(fiAsciiTokenizer& asciiTok, 
									grmShaderGroup* shaderGroup,
									u8 parentIndex,
									int boneID);

	void LoadDamagedASCII(fiAsciiTokenizer& asciiTok, 
						  grmShaderGroup* shaderGroup);

#if ENABLE_FRAGMENT_EVENTS
	void SetBreakEventset(const evtSet* src);

	void SetBreakFromRootEventset(const evtSet* src);
#endif // ENABLE_FRAGMENT_EVENTS

	// Used to pass information back to the group that might be loading us.
	static u8 s_Flags;

private:
	float ComputeMassFromVolume(fragDrawable& drawable);

	// We keep this around even if the damage is not stored in the child any more just so that old tune files can still load.
	// Their implementations will not do anything.
	void LoadDisappearsWhenDead(rmcTypeFileCbData* data);
	void LoadDamagePassesThrough(rmcTypeFileCbData* data);
	void LoadMinDamageForce(rmcTypeFileCbData* data);
	void LoadDamageHealth(rmcTypeFileCbData* data);

	void LoadPristineMass(rmcTypeFileCbData* data);
	void LoadDamagedMass(rmcTypeFileCbData* data);
	void LoadParentBoneID(rmcTypeFileCbData* data);
	void LoadParentBoneName(rmcTypeFileCbData* data);
#if ENABLE_FRAGMENT_EVENTS
	void LoadContinuousEventset(rmcTypeFileCbData* data);
	void LoadCollisionEventset(rmcTypeFileCbData* data);
	void LoadBreakEventset(rmcTypeFileCbData* data);
	void LoadBreakFromRootEventset(rmcTypeFileCbData* data);
#endif // ENABLE_FRAGMENT_EVENTS

	// Extra four bytes here; don't forget about the v-table pointer (this class derived from datBase).
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	Vec4V m_UndamagedAngularInertiaXYZMassW;
	Vec4V m_DamagedAngularInertiaXYZMassW;
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	f32				 m_UndamagedMass;
	f32				 m_DamagedMass;
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD

	// Index in the fragType to the group that owns us...
	u8				 m_OwnerGroupPointerIndex;
	// Not really needed anymore since we don't have any flags any more.  Leaving around just in case it makes a comeback (we wouldn't save any memory
	//   anyway due to alignment, and we've already got some unused padding bytes).
	u8				 m_Flags;

	// The bone of the main entity's skeleton this child follows, 0 if it's parented to the entity itself
	u16				 m_BoneID;

#	if __64BIT
		u8              m_PadX[12];
#	endif

	u8				 m_Pad0[64];
	u8				 m_Pad1[64];

	fragDrawable*	 m_UndamagedEntity;
	fragDrawable*	 m_DamagedEntity;

#if ENABLE_FRAGMENT_EVENTS
	datOwner<evtSet> m_ContinuousEventset;
	datOwner<evtSet> m_CollisionEventset;
	datOwner<evtSet> m_BreakEventset;
    datOwner<evtSet> m_BreakFromRootEventset;

	datOwner<fragCollisionEventPlayer> m_CollisionEventPlayer;
	datOwner<fragBreakEventPlayer> m_BreakEventPlayer;
    datOwner<fragBreakEventPlayer> m_BreakFromRootEventPlayer;
#else // ENABLE_FRAGMENT_EVENTS
	u8 m_Pad2[sizeof(datOwner<int>)*7];
#endif // ENABLE_FRAGMENT_EVENTS

#define FRAG_TYPE_CHILD_PADDING_NEEDED 12

#if FRAG_TYPE_CHILD_PADDING_NEEDED > 0
	u8 pad[FRAG_TYPE_CHILD_PADDING_NEEDED];
#endif

#if __RESOURCECOMPILER
public:
	static class fragType* s_currentLoadingFragType;
#endif // __RESOURCECOMPILER
};

#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
inline Vec3V_Out fragTypeChild::GetUndamagedAngularInertia() const
{
	return m_UndamagedAngularInertiaXYZMassW.GetXYZ();
}

inline Vec3V_Out fragTypeChild::GetDamagedAngularInertia() const
{
	return m_DamagedAngularInertiaXYZMassW.GetXYZ();
}

inline void fragTypeChild::SetUndamagedAngularInertia(Vec3V_In undamagedAngularInertia)
{
	m_UndamagedAngularInertiaXYZMassW.SetXYZ(undamagedAngularInertia);
}

inline void fragTypeChild::SetDamagedAngularInertia(Vec3V_In damagedAngularInertia)
{
	m_DamagedAngularInertiaXYZMassW.SetXYZ(damagedAngularInertia);
}
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD

inline f32* fragTypeChild::GetUndamagedMassPtr()
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	return &m_UndamagedAngularInertiaXYZMassW[3];
#else
	return &m_UndamagedMass;
#endif
}

inline f32* fragTypeChild::GetDamagedMassPtr()
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	return &m_DamagedAngularInertiaXYZMassW[3];
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	return &m_DamagedMass;
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
}

inline f32 fragTypeChild::GetUndamagedMass() const
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	return m_UndamagedAngularInertiaXYZMassW.GetWf();
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	return m_UndamagedMass;
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
}

inline f32 fragTypeChild::GetDamagedMass() const
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	return m_DamagedAngularInertiaXYZMassW.GetWf();
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	return m_DamagedMass;
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
}

inline void fragTypeChild::SetUndamagedMass(f32 fMass)
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	m_UndamagedAngularInertiaXYZMassW.SetWf(fMass);
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	m_UndamagedMass = fMass;
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
}

inline void fragTypeChild::SetDamagedMass(f32 fMass)
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	m_DamagedAngularInertiaXYZMassW.SetWf(fMass);
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	m_DamagedMass = fMass;
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
}

inline u8 fragTypeChild::GetOwnerGroupPointerIndex() const
{
	return m_OwnerGroupPointerIndex;
}

inline u16 fragTypeChild::GetBoneID () const
{
	return m_BoneID;
}

#if ENABLE_FRAGMENT_EVENTS
inline evtSet* fragTypeChild::GetContinuousEventset() const
{
	return m_ContinuousEventset;
}

inline evtSet* fragTypeChild::GetCollisionEventset() const
{
	return m_CollisionEventset;
}

inline fragCollisionEventPlayer* fragTypeChild::GetCollisionEventPlayer()
{
	return m_CollisionEventPlayer;
}

inline evtSet* fragTypeChild::GetBreakEventset() const
{
	return m_BreakEventset;
}

inline evtSet* fragTypeChild::GetBreakFromRootEventset() const
{
    return m_BreakFromRootEventset;
}

inline fragBreakEventPlayer* fragTypeChild::GetBreakEventPlayer()
{
	return m_BreakEventPlayer;
}

inline fragBreakEventPlayer* fragTypeChild::GetBreakFromRootEventPlayer()
{
    return m_BreakFromRootEventPlayer;
}
#endif // ENABLE_FRAGMENT_EVENTS

// Should this be const?
inline fragDrawable* fragTypeChild::GetUndamagedEntity() const
{
	return m_UndamagedEntity;
}

// Should this be const?
inline fragDrawable* fragTypeChild::GetDamagedEntity() const
{
	return m_DamagedEntity;
}

} // namespace rage

#endif // FRAG_TYPE_CHILD_H
