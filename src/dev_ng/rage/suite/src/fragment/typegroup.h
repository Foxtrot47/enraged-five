// 
// fragment/typegroup.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_TYPEGROUP_H
#define FRAGMENT_TYPEGROUP_H

// fragment
#include "eventplayer.h"
#include "fragmentconfig.h"

// RAGE
#include "data/resource.h"
#include "data/struct.h"
#include "math/amath.h"

namespace rage {

class atBitSet;
class bkGroup;
class crSkeleton;
class crSkeletonData;
class evtSet;
class fiAsciiTokenizer;
struct fragHierarchyInst;
class fragTypeChild;
class grmShaderGroup; 
class phInst;
class Matrix34;
class fragInst;
class fragManager;
class fragTypeStub;
class fragType;
class fragTuneStruct;
class fragPhysicsLOD;

#define FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH 40

/*
  PURPOSE
	fragTypeGroup's are tied together in a tree structure, and the links between them
	are the breaking points of the fragment instances.
	
  NOTES
    Each fragTypeGroup owns one or more fragTypeChild's, which contain the drawable and physical
	properties of the breakable piece it controls.

	<FLAG Component>
*/
class fragTypeGroup
{
friend class fragManager;
friend class fragTypeStub;
friend class fragType;
friend class fragTuneStruct;
public:
	
	DECLARE_PLACE(fragTypeGroup);

	fragTypeGroup(f32 strength = 3.0f, 
		f32 forceTransmissionScaleUp = 0.2f, 
		f32 forceTransmissionScaleDown = 0.2f, 
		f32 jointStiffness = 0.0f,
		f32 minSoftAngle1 = -1.0f,
		f32 maxSoftAngle1 = 1.0f,
		f32 maxSoftAngle2 = 1.0f,
		f32 maxSoftAngle4 = 1.0f,
		f32 rotationSpeed = 0.0f,
		f32 rotationStrength = 0.0f,
        f32 restoringStrength = 0.0f,
        f32 restoringMaxTorque = 0.0f,
		f32 latchStrength = 0.0f,
		u8  numChildren = 1,
		u8  numChildGroups = 0, 
		u8  childIndex = 0xFF, 
		u8  parentIndex = 0xFF, 
		u8  childGroupsPointersIndex = 0xFF,
		u8  flags = 0,
		f32	minDamageForce = 0.0f,
		f32	damageHealth = 0.0f,
		f32 weaponHealth = 0.0f,
		f32 weaponScale = 1.0f,
		f32 meleeScale = 1.0f,
		f32 vehicleScale = 1.0f,
		f32 pedScale = 1.0f,
		f32 ragdollScale = 1.0f,
		f32 explosionScale = 1.0f,
		f32 objectScale = 1.0f,
		f32 pedInvMassScale = 1.0f);

	fragTypeGroup(datResource& rsc);

    ~fragTypeGroup();

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

	void Set(f32 strength = 100.0f, 
			 f32 forceTransmissionScaleUp = 0.2f, 
			 f32 forceTransmissionScaleDown = 0.2f, 
			 f32 jointStiffness = 0.0f,
			 f32 minSoftAngle1 = -1.0f,
			 f32 maxSoftAngle1 = 1.0f,
			 f32 maxSoftAngle2 = 1.0f,
			 f32 maxSoftAngle3 = 1.0f,
			 f32 rotationSpeed = 0.0f,
			 f32 rotationStrength = 0.0f,
             f32 restoringStrength = 0.0f,
             f32 restoringMaxTorque = 0.0f,
			 f32 latchStrength = 0.0f,
			 u8  numChildren = 1,
			 u8  numChildGroups = 0, 
			 u8  childIndex = 0xFF, 
			 u8  parentIndex = 0xFF, 
			 u8  childGroupsPointersIndex = 0xFF,
			 u8  flags = 0,
			 u8  glassTypeIndex = 0,
			 f32 minDamageForce = 0.0f,
			 f32 damageHealth = 0.0f,
			 f32 weaponHealth = 0.0f,
			 f32 weaponScale = 1.0f,
			 f32 meleeScale = 1.0f,
			 f32 vehicleScale = 1.0f,
			 f32 pedScale = 1.0f,
			 f32 ragdollScale = 1.0f,
			 f32 explosionScale = 1.0f,
			 f32 objectScale = 1.0f,
			 f32 pedInvMassScale = 1.0f);

	void Set(fragTypeGroup& other);

	void SetGlassTypeIndex(int paneType);
	u8 GetGlassTypeIndex() const;
	void SetGlassPaneModelInfoIndex(u8 modelInfoIndex);
	u8 GetGlassPaneModelInfoIndex() const;
	bool IsLegacyGlass() const;

	int GetLegacyGlassModelIndex() const;
	int GetLegacyGlassGeomIndex() const;

	f32 GetStrength() const;
	void SetStrength(f32 fStrength);
	u8  GetParentGroupPointerIndex() const;
	f32 GetForceTransmissionScaleUp() const;
	void SetForceTransmissionScaleUp(f32 fScale);
	f32 GetForceTransmissionScaleDown() const;
	void SetForceTransmissionScaleDown(f32 fScale);
	f32 GetJointStiffness() const;
	void SetJointStiffness(f32 fStiffness);
	f32 GetMinSoftAngle1() const;
	f32 GetMaxSoftAngle1() const;
	f32 GetMaxSoftAngle2() const;
	f32 GetMaxSoftAngle3() const;
	f32 GetJointRotationSpeed() const;
	f32 GetJointRotationStrength() const;
    f32 GetJointRestoringStrength() const;
    f32 GetJointRestoringMaxTorque() const;
	f32 GetLatchStrength() const;
	void SetLatchStrength(f32 fLatchStrength);
	bool IsInitiallyLatched() const;
	u8  GetChildFragmentIndex() const;
	u8  GetNumChildren() const;
	u8  GetNumChildGroups() const;
	u8  GetChildGroupsPointersIndex() const;
	bool GetDisappearsWhenDead() const;
	bool GetMadeOfGlass() const;
	bool GetDamageWhenBroken() const;
	bool GetDoesntAffectVehicles() const;
	bool GetDoesntPushVehiclesDown() const;
	bool GetHasCloth() const;
	void SetMadeOfGlass(bool madeOfGlass);
	f32	GetMinDamageForce() const;
	f32	GetDamageHealth() const;
	const f32& GetWeaponHealth() const;
	const f32& GetWeaponScale() const;
	const f32& GetMeleeScale() const;
	const f32& GetVehicleScale() const;
	const f32& GetPedScale() const;
	void SetPedScale(f32 fPedScale);
	const f32& GetRagdollScale() const;
	const f32& GetExplosionScale() const;
	const f32& GetObjectScale() const;
	const f32& GetPedInvMassScale() const;

#if ENABLE_FRAGMENT_EVENTS
    evtSet* GetDeathEventset() const;
    fragCollisionEventPlayer* GetDeathEventPlayer();
#endif // ENABLE_FRAGMENT_EVENTS

	const char* GetDebugName() const;

    void ComputeAndSetTotalUndamagedMass(const fragPhysicsLOD* lod);
    float ComputeTotalUndamagedMass(const fragPhysicsLOD* lod);
	void SetTotalUndamagedMass(int currentLOD, float fMass, fragType* type);
	float GetTotalUndamagedMass() const;

#if ENABLE_FRAGMENT_EVENTS
    void TriggerAllBreakFromRootEvents(int currentLOD, const fragType* type, crSkeleton* skeleton, fragInst* oldInst, fragInst* newInst, atBitSet* groupBroken);
#endif // ENABLE_FRAGMENT_EVENTS

#if __BANK
	void SaveASCII(fiAsciiTokenizer& asciiTok, datOwner<fragTypeGroup>* groups, fragTypeChild** children);
#endif

	static void LoadASCII(fiAsciiTokenizer& asciiTok, 
						  crSkeletonData* skeletonData,
						  grmShaderGroup* shaderGroup,
									
						  u8* nextGroupIndex, 
						  datOwner<fragTypeGroup>* groups, 
						  u8* numChildren, 
						  fragTypeChild** children,
									
						  u8 parentIndex);

#if __BANK
	void DrawPhys(int currentLOD,
		const Matrix34& matrix,
		fragHierarchyInst* hierInst,
		const fragType* type,
		bool childHighlight,
		bool selectHighlight,
		bool ignoreBank,
		unsigned int bankIsForGroupIndex);
#endif // __BANK

	static void SetNoDisappearWhenDead(bool noDisappearWhenDead) { sm_NoDisappearWhenDead = noDisappearWhenDead; }

private:
    static void MakeFakeChildren(int firstChild, fragTypeChild** children, u8* numChildren, u8& numSelfChildren);
#if ENABLE_FRAGMENT_EVENTS
    datOwner<evtSet> m_DeathEventset;
    datOwner<fragCollisionEventPlayer> m_DeathEventPlayer;
#else // ENABLE_FRAGMENT_EVENTS
	u8 m_Pad0[sizeof(datOwner<int>)*2];
#endif // ENABLE_FRAGMENT_EVENTS

	// Magnitude of force required to separate this group from its parent
	f32		m_Strength;
	
	// This determines how much of a force applied to us gets passed on to our parent.  Before we send force up the chain, 
	// we first scale by this value...
	f32		m_ForceTransmissionScaleUp;
	
	// This determines how much of a force applied to us from our parent applies to us.  For each force that gets directly 
	// applied to us, we pass it unmodified down to all children.  For each for we get from our parent, we scale by this 
	// value before we do anything with the value...
	f32		m_ForceTransmissionScaleDown;

	// The articulated stiffness of the joint where this group is attached, if it becomes articulated
	f32		m_JointStiffness;
	
	// The the minimum soft angle limit for 1 DOF joints or the first limit for 3 DOF joints
	f32		m_MinSoftAngle1;

	// The the maximum soft angle limit for 1 DOF joints or the first limit for 3 DOF joints
	f32		m_MaxSoftAngle1;

	// The the second maximum soft angle limit for 3 DOF joints
	f32		m_MaxSoftAngle2;

	// The the third maximum soft angle limit for 3 DOF joints
	f32		m_MaxSoftAngle3;

	// The speed this articulated joint attached here will attempt to achieve using a muscle
	f32		m_RotationSpeed;

	// The strength this articulated joint will use to try to match the rotation speed
	f32		m_RotationStrength;

	// The strength with with this articulated joint will try to reach the zero pose
	f32		m_RestoringStrength;

	// The maximum torque this articulated joint can use to return to the zero pose
	f32		m_RestoringMaxTorque;

	// The breaking strength of the latch on this joint, if there is one
	f32     m_LatchStrength;

    // The total mass of all child fragments, undamaged
    f32     m_TotalUndamagedMass;

    // The total mass of all child fragments, damaged
    f32     m_TotalDamagedMass;

	// Index in the fragType to the start of the list of child groups we own - 0xFF if we own no groups...
	u8		m_ChildGroupsPointersIndex;
	
	// Index in the fragType to the group that owns us - 0xFF if the root owns us...
	u8		m_ParentGroupPointerIndex;
	
	// Index in the fragType to the child we own - 0xFF if we don't own one...
	u8		m_ChildIndex;

	// The number of children we own, starting with m_ChildIndex
	u8		m_NumChildren;
	
	// The number of child groups we own, starting with child groups pointers index
	u8		m_NumChildGroups;

	u8		m_GlassModelAndType;        // now unused; was: upper bits are glass type index; lower are geometry index
	u8		m_GlassPaneModelInfoIndex;  // was geometry index

	// Uhmm ... flags.  See StateBits below 
	u8		m_Flags;

	// Related to damage and health.  Not much in the way of standards yet for what these values represent.
	f32		m_MinDamageForce;
	f32		m_DamageHealth;
	f32		m_WeaponHealth;

	f32		m_WeaponScale;
	f32		m_VehicleScale;
	f32		m_PedScale;
	f32		m_RagdollScale;
	f32		m_ExplosionScale;
	f32		m_ObjectScale;

	// Scaling values for the group's inverse mass when colliding with the given type of object
	// An inverse mass scale of 0 means the group is immovable, 1 means no difference.
	f32     m_PedInvMassScale; 

	int		m_PresetApplied;

	enum StateBits
	{
		FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD	  = 1 << 0, // When health reaches zero, this group disappears
		FRAG_GROUP_FLAG_MADE_OF_GLASS			  = 1 << 1, // This group is made out of glass and will shatter when broken
		FRAG_GROUP_FLAG_DAMAGE_WHEN_BROKEN		  = 1 << 2, // When this group breaks off its parent, it will become damaged
		FRAG_GROUP_FLAG_DOESNT_AFFECT_VEHICLES    = 1 << 3, // When colliding with vehicles, the vehicle is treated as infinitely massive
		FRAG_GROUP_FLAG_DOESNT_PUSH_VEHICLES_DOWN = 1 << 4,
		FRAG_GROUP_FLAG_HAS_CLOTH				  = 1 << 5, // This group has the cloth ( can't have more than one cloth per fragment )
	};

#if FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH > 0
	char    m_DebugName[FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH];
#endif
	f32		m_MeleeScale;

	static const int kNumGlassModelBits = 5;
	static const int kMaxGlassModels = 1 << kNumGlassModelBits;
	static const u8 kGlassModelMask = kMaxGlassModels - 1;
	static const int kNumGlassTypeBits = 8 - kNumGlassModelBits;
	static const int kMaxGlassTypes = 1 << kNumGlassTypeBits;
	static const u8 kGlassTypeMask = (kMaxGlassTypes - 1) << kNumGlassModelBits;
	static bool sm_NoDisappearWhenDead;
#if __BANK
	static bool sm_NoLatching;
#endif // __BANK
};

inline void fragTypeGroup::Set(f32 strength,
							   f32 forceTransmissionScaleUp,
							   f32 forceTransmissionScaleDown,
							   f32 jointStiffness,
							   f32 minSoftAngle1,
							   f32 maxSoftAngle1,
							   f32 maxSoftAngle2,
							   f32 maxSoftAngle3,
							   f32 rotationSpeed,
							   f32 rotationStrength,
                               f32 restoringStrength,
                               f32 restoringMaxTorque,
							   f32 latchStrength,
							   u8  numChildren,
							   u8  numChildGroups,
							   u8  childIndex,
							   u8  parentIndex, 
							   u8  childGroupsPointersIndex,
							   u8  flags,
							   u8  glassTypeIndex,
							   f32 minDamageForce,
							   f32 damageHealth,
							   f32 weaponHealth,
							   f32 weaponScale,
							   f32 meleeScale,
							   f32 vehicleScale,
							   f32 pedScale,
							   f32 ragdollScale,
							   f32 explosionScale,
							   f32 objectScale,
							   f32 pedInvMassScale)
{
	m_Strength						= strength;
	m_ForceTransmissionScaleUp		= forceTransmissionScaleUp;
	m_ForceTransmissionScaleDown	= forceTransmissionScaleDown;
	m_JointStiffness				= jointStiffness;
	m_MinSoftAngle1					= minSoftAngle1;
	m_MaxSoftAngle1					= maxSoftAngle1;
	m_MaxSoftAngle2					= maxSoftAngle2;
	m_MaxSoftAngle3					= maxSoftAngle3;
	m_RotationSpeed					= rotationSpeed;
	m_RotationStrength				= rotationStrength;
    m_RestoringStrength             = restoringStrength;
    m_RestoringMaxTorque            = restoringMaxTorque;
	m_LatchStrength					= latchStrength;
	m_NumChildren					= numChildren;
	m_NumChildGroups				= numChildGroups;
	m_ChildIndex					= childIndex;
	m_ParentGroupPointerIndex		= parentIndex;
	m_ChildGroupsPointersIndex		= childGroupsPointersIndex;
	m_Flags							= flags;
	m_MinDamageForce				= minDamageForce;
	m_DamageHealth					= damageHealth;
	m_WeaponHealth					= weaponHealth;
	m_WeaponScale					= weaponScale;
	m_MeleeScale					= meleeScale;
	m_VehicleScale					= vehicleScale;
	m_PedScale						= pedScale;
	m_RagdollScale					= ragdollScale;
	m_ExplosionScale				= explosionScale;
	m_ObjectScale					= objectScale;
	m_PedInvMassScale				= pedInvMassScale;

	SetGlassTypeIndex(glassTypeIndex);
}

// This should be called only at resource-time before model-load
inline void fragTypeGroup::SetGlassTypeIndex(int glassTypeIndex)
{
	// a bit of a hack here: use the pane model info index 
	// to hold this value temporarily during resourcing
	m_GlassPaneModelInfoIndex = (u8)glassTypeIndex;
}

// should be called only at runtime and only for legacy fragments
inline int fragTypeGroup::GetLegacyGlassModelIndex() const
{
	return m_GlassModelAndType & kGlassModelMask;
}

// more legacy support
inline int fragTypeGroup::GetLegacyGlassGeomIndex() const
{
	return m_GlassPaneModelInfoIndex;
}

inline u8 fragTypeGroup::GetGlassTypeIndex() const
{
	u8 glassTypeIndex;
	// This is going to be a bit confusing... sorry...
	if (!IsLegacyGlass())
	{
		// resource-time only usage for current fragments
		glassTypeIndex = m_GlassPaneModelInfoIndex;
	}
	else
	{
		// runtime only usage for legacy fragments
		glassTypeIndex =  m_GlassModelAndType >> kNumGlassModelBits;
	}
	return glassTypeIndex;
}

inline void fragTypeGroup::SetGlassPaneModelInfoIndex(u8 modelInfoIndex)
{
	m_GlassPaneModelInfoIndex = modelInfoIndex;
}

inline u8 fragTypeGroup::GetGlassPaneModelInfoIndex() const
{
	return m_GlassPaneModelInfoIndex;
}

inline bool fragTypeGroup::IsLegacyGlass() const
{
	return m_GlassModelAndType != 0xff;
}

inline f32 fragTypeGroup::GetStrength() const
{
	return m_Strength;
}

inline void fragTypeGroup::SetStrength(f32 fStrength)
{
	m_Strength = fStrength;
}

inline u8 fragTypeGroup::GetParentGroupPointerIndex() const
{
	return m_ParentGroupPointerIndex;
}

inline f32 fragTypeGroup::GetForceTransmissionScaleUp() const
{
	return m_ForceTransmissionScaleUp; 
}

inline void fragTypeGroup::SetForceTransmissionScaleUp(f32 fScale)
{
	m_ForceTransmissionScaleUp = fScale;
}

inline f32 fragTypeGroup::GetForceTransmissionScaleDown() const
{
	return m_ForceTransmissionScaleDown; 
}

inline void fragTypeGroup::SetForceTransmissionScaleDown(f32 fScale)
{
	m_ForceTransmissionScaleDown = fScale;
}

inline f32 fragTypeGroup::GetJointStiffness() const
{
	return m_JointStiffness; 
}

inline void fragTypeGroup::SetJointStiffness(f32 fStiffness)
{
	m_JointStiffness = fStiffness;
}

inline f32 fragTypeGroup::GetMinSoftAngle1() const
{
	return m_MinSoftAngle1; 
}

inline f32 fragTypeGroup::GetMaxSoftAngle1() const
{
	return m_MaxSoftAngle1; 
}

inline f32 fragTypeGroup::GetMaxSoftAngle2() const
{
	return m_MaxSoftAngle2; 
}

inline f32 fragTypeGroup::GetMaxSoftAngle3() const
{
	return m_MaxSoftAngle3; 
}

inline f32 fragTypeGroup::GetJointRotationSpeed() const
{
	return m_RotationSpeed; 
}

inline f32 fragTypeGroup::GetJointRotationStrength() const
{
	return m_RotationStrength; 
}

inline f32 fragTypeGroup::GetJointRestoringStrength() const
{
    return m_RestoringStrength;
}

inline f32 fragTypeGroup::GetJointRestoringMaxTorque() const
{
    return m_RestoringMaxTorque;
}

inline f32 fragTypeGroup::GetLatchStrength() const
{
#if __BANK
	return sm_NoLatching ? 0 : m_LatchStrength;
#else // __BANK
	return m_LatchStrength; 
#endif // __BANK
}

inline void fragTypeGroup::SetLatchStrength(f32 fLatchStrength)
{
	m_LatchStrength = fLatchStrength;
}

inline bool fragTypeGroup::IsInitiallyLatched() const
{
	return GetLatchStrength() == -1.0f || GetLatchStrength() > 0.0f;
}

inline u8 fragTypeGroup::GetChildFragmentIndex() const
{
	return m_ChildIndex;
}

inline u8 fragTypeGroup::GetNumChildren() const
{
	return m_NumChildren;
}

inline u8 fragTypeGroup::GetNumChildGroups() const
{
	return m_NumChildGroups;
}

inline u8 fragTypeGroup::GetChildGroupsPointersIndex() const
{
	return m_ChildGroupsPointersIndex;
}

inline bool fragTypeGroup::GetDisappearsWhenDead() const
{
	return (m_Flags & FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD) && !sm_NoDisappearWhenDead;
}

inline bool fragTypeGroup::GetMadeOfGlass() const
{
	return (m_Flags & FRAG_GROUP_FLAG_MADE_OF_GLASS) != 0;
}

inline bool fragTypeGroup::GetDamageWhenBroken() const
{
	return (m_Flags & FRAG_GROUP_FLAG_DAMAGE_WHEN_BROKEN) != 0;
}

inline bool fragTypeGroup::GetDoesntAffectVehicles() const 
{ 
	return (m_Flags & FRAG_GROUP_FLAG_DOESNT_AFFECT_VEHICLES) != 0;
}

inline bool fragTypeGroup::GetDoesntPushVehiclesDown() const 
{ 
	return (m_Flags & FRAG_GROUP_FLAG_DOESNT_PUSH_VEHICLES_DOWN) != 0;
}

inline bool fragTypeGroup::GetHasCloth() const 
{ 
	return (m_Flags & FRAG_GROUP_FLAG_HAS_CLOTH) != 0;
}


inline void fragTypeGroup::SetMadeOfGlass(bool madeOfGlass)
{
	if (madeOfGlass)
	{
		m_Flags |= FRAG_GROUP_FLAG_MADE_OF_GLASS;
	}
	else
	{
		m_Flags &= ~(u8(FRAG_GROUP_FLAG_MADE_OF_GLASS));
	}
}

inline f32 fragTypeGroup::GetMinDamageForce() const
{
	return m_MinDamageForce;
}

inline f32 fragTypeGroup::GetDamageHealth() const
{
	return m_DamageHealth;
}

inline const f32& fragTypeGroup::GetWeaponHealth() const
{
	return m_WeaponHealth;
}

inline const f32& fragTypeGroup::GetWeaponScale() const
{
	return m_WeaponScale;
}

inline const f32& fragTypeGroup::GetMeleeScale() const
{
	return m_MeleeScale;
}

inline const f32& fragTypeGroup::GetVehicleScale() const
{
	return m_VehicleScale;
}

inline const f32& fragTypeGroup::GetPedScale() const
{
	return m_PedScale;
}

inline void fragTypeGroup::SetPedScale(f32 fPedScale)
{
	m_PedScale = fPedScale;
}

inline const f32& fragTypeGroup::GetRagdollScale() const
{
	return m_RagdollScale;
}

inline const f32& fragTypeGroup::GetExplosionScale() const
{
	return m_ExplosionScale;
}

inline const f32& fragTypeGroup::GetObjectScale() const
{
	return m_ObjectScale;
}

inline const f32& fragTypeGroup::GetPedInvMassScale() const
{
	return m_PedInvMassScale;
}

#if ENABLE_FRAGMENT_EVENTS
inline evtSet* fragTypeGroup::GetDeathEventset() const
{
    return m_DeathEventset;
}

inline fragCollisionEventPlayer* fragTypeGroup::GetDeathEventPlayer()
{
    return m_DeathEventPlayer;
}
#endif // ENABLE_FRAGMENT_EVENTS

inline const char* fragTypeGroup::GetDebugName() const
{
#if FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH > 0
	return m_DebugName;
#else
	return "";
#endif
}

inline float fragTypeGroup::GetTotalUndamagedMass() const
{
	return m_TotalUndamagedMass;
}

} // namespace rage

#endif // FRAG_TYPE_GROUP_H
