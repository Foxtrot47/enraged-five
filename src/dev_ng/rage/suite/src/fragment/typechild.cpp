// 
// fragment/typechild.cpp 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "typechild.h"
#if __RESOURCECOMPILER
#include "type.h"
#endif // __RESOURCECOMPILER

// RAGE
#include "drawable.h"
#include "eventplayer.h"

#include "crskeleton/skeletondata.h"
#include "data/resourcehelpers.h"
#include "event/player.h"
#include "event/set.h"
#include "file/token.h"
#include "parser/manager.h"
#include "phbound/bound.h"
#include "physics/archetype.h"
#include "rmcore/typefileparser.h"

FRAGMENT_OPTIMISATIONS()

namespace rage {

	// Used to pass information back to the group that might be loading us.
	u8 fragTypeChild::s_Flags = 0;

fragTypeChild::fragTypeChild(u16 boneID,
							 fragDrawable* undamagedEntity, 
							 fragDrawable* damagedEntity, 
							 f32 undamagedMass, 
							 f32 damagedMass, 
							 u8 parentIndex,
							 u8 flags)
	:
#if !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	m_UndamagedMass(undamagedMass)
	, m_DamagedMass(damagedMass),
#endif // !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	 m_OwnerGroupPointerIndex(parentIndex)
	, m_Flags(flags)
	, m_BoneID(boneID)
	//, m_BoneIndexDeprecated(boneIndex)
	, m_UndamagedEntity(undamagedEntity)
	, m_DamagedEntity(damagedEntity)
#if ENABLE_FRAGMENT_EVENTS
	, m_ContinuousEventset(NULL)
	, m_CollisionEventset(NULL)
	, m_BreakEventset(NULL)
    , m_BreakFromRootEventset(NULL)
	, m_CollisionEventPlayer(NULL)
	, m_BreakEventPlayer(NULL)
	, m_BreakFromRootEventPlayer(NULL)
#endif // ENABLE_FRAGMENT_EVENTS
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	SetUndamagedAngularInertia(Vec3V(V_ZERO));
	SetDamagedAngularInertia(Vec3V(V_ZERO));
	SetUndamagedMass(undamagedMass);
	SetDamagedMass(damagedMass);
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD

#if FRAG_TYPE_CHILD_PADDING_NEEDED > 0
	memset(pad, 0, sizeof(pad));
#endif
}


IMPLEMENT_PLACE(fragTypeChild);

void fragTypeChild::Place(datResource& rsc, grmShaderGroup* shaderGroup)
{
	::new (this) fragTypeChild(rsc, shaderGroup);
}

fragTypeChild::fragTypeChild(datResource&)
{
	AssertMsg(false, "Don't call, just required to compile datOwner::datOwner()!");
}

#ifdef __SNC__
#pragma diag_suppress 285	// NULL reference is not allowed.
#endif

fragTypeChild::fragTypeChild(datResource& rsc, grmShaderGroup* shaderGroup)
#if ENABLE_FRAGMENT_EVENTS
	: m_ContinuousEventset(rsc)
	, m_CollisionEventset(rsc)
	, m_BreakEventset(rsc)
    , m_BreakFromRootEventset(rsc)
	, m_CollisionEventPlayer(rsc)
	, m_BreakEventPlayer(rsc)
    , m_BreakFromRootEventPlayer(rsc)
#endif // ENABLE_FRAGMENT_EVENTS
{
#if ENABLE_FRAGMENT_EVENTS
	if (m_CollisionEventPlayer)
	{
		m_CollisionEventPlayer->CreateParameterList();
		Assert(m_CollisionEventset);
		m_CollisionEventPlayer->SetSet(*m_CollisionEventset);
	}

	if (m_BreakEventPlayer)
	{
		m_BreakEventPlayer->CreateParameterList();
		Assert(m_BreakEventset);
		m_BreakEventPlayer->SetSet(*m_BreakEventset);
	}

	if (m_BreakFromRootEventPlayer)
	{
		m_BreakFromRootEventPlayer->CreateParameterList();
		Assert(m_BreakFromRootEventset);
		m_BreakFromRootEventPlayer->SetSet(*m_BreakFromRootEventset);
	}
#endif // ENABLE_FRAGMENT_EVENTS

	// In here, we don't want the bounds to be resource constructed, because that is handled by
	// the phArchetype owned by the fragType. So, we pull the bound pointer out, fix it up ourselves,
	// zero it in the entity, and then put it back.
	if (m_DamagedEntity == m_UndamagedEntity)
	{
		if (m_UndamagedEntity)
		{
			rsc.PointerFixup(m_UndamagedEntity);
			phBound* bound = m_UndamagedEntity->GetBound();
			m_UndamagedEntity->SetBound(NULL);
			::new ((void*)m_UndamagedEntity) fragDrawable(rsc, shaderGroup);
			if (bound)
			{
				m_UndamagedEntity->SetBound((phBound*)((char*)bound + rsc.GetFixup(bound)));
			}
		}

		m_DamagedEntity = m_UndamagedEntity;
	}
	else
	{
		if (m_UndamagedEntity)
		{
			rsc.PointerFixup(m_UndamagedEntity);
			phBound* bound = m_UndamagedEntity->GetBound();
			m_UndamagedEntity->SetBound(NULL);
			crSkeletonData* sd = m_UndamagedEntity->GetSkeletonData();
			m_UndamagedEntity->SetSkeletonDataPtrAfterPageIn(*(crSkeletonData*)NULL);
			::new ((void*)m_UndamagedEntity) fragDrawable(rsc, shaderGroup);
			if (bound)
			{
				m_UndamagedEntity->SetBound((phBound*)((char*)bound + rsc.GetFixup(bound)));
				m_UndamagedEntity->SetSkeletonDataPtrAfterPageIn(*sd);
			}
		}

		if (m_DamagedEntity)
		{
			rsc.PointerFixup(m_DamagedEntity);
			phBound* bound = m_DamagedEntity->GetBound();
			m_DamagedEntity->SetBound(NULL);
			crSkeletonData* sd = m_DamagedEntity->GetSkeletonData();
			m_DamagedEntity->SetSkeletonDataPtrAfterPageIn(*(crSkeletonData*)NULL);
			::new ((void*)m_DamagedEntity) fragDrawable(rsc, shaderGroup);
			m_DamagedEntity->SetSkeletonDataPtrAfterPageIn(*sd);
			if (bound)
			{
				m_DamagedEntity->SetBound((phBound*)((char*)bound + rsc.GetFixup(bound)));
			}
		}
	}
}



#if __DECLARESTRUCT
void fragTypeChild::DeclareStruct(datTypeStruct &s)
{
	STRUCT_BEGIN(fragTypeChild);
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	STRUCT_FIELD(m_UndamagedAngularInertiaXYZMassW);
	STRUCT_FIELD(m_DamagedAngularInertiaXYZMassW);
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	STRUCT_FIELD(m_UndamagedMass);
	STRUCT_FIELD(m_DamagedMass);
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD

	STRUCT_FIELD(m_OwnerGroupPointerIndex);
	STRUCT_FIELD(m_Flags);
	STRUCT_FIELD(m_BoneID);
	//STRUCT_FIELD(m_BoneIndexDeprecated);

#	if __64BIT
		STRUCT_CONTAINED_ARRAY(m_PadX);
#	endif
	STRUCT_CONTAINED_ARRAY(m_Pad0);
	STRUCT_CONTAINED_ARRAY(m_Pad1);

	fragDrawable* drawable = m_UndamagedEntity;
	phBound* bound = drawable->GetBound();
	drawable->SetBound(NULL);
	drawable->SetShaderGroup(NULL);
	s.AddField((size_t)&m_UndamagedEntity-(size_t)this,sizeof(m_UndamagedEntity));
	if (m_UndamagedEntity) datSwapper(*m_UndamagedEntity); datSwapper((void*&)(m_UndamagedEntity));
	datSwapper((void*&)bound);
	drawable->SetBound(bound);

	if (m_DamagedEntity)
	{
		drawable = m_DamagedEntity;
		phBound* bound = drawable->GetBound();
		drawable->SetBound(NULL);
		drawable->SetShaderGroup(NULL);
		if (m_DamagedEntity) datSwapper(*m_DamagedEntity);
		datSwapper((void*&)bound);
		drawable->SetBound(bound);
	}

	s.AddField((size_t)&m_DamagedEntity-(size_t)this,sizeof(m_DamagedEntity));
	datSwapper((void*&)(m_DamagedEntity));

#if ENABLE_FRAGMENT_EVENTS
	STRUCT_FIELD(m_ContinuousEventset);
	STRUCT_FIELD(m_CollisionEventset);
	STRUCT_FIELD(m_BreakEventset);
    STRUCT_FIELD(m_BreakFromRootEventset);
	STRUCT_FIELD(m_CollisionEventPlayer);
	STRUCT_FIELD(m_BreakEventPlayer);
    STRUCT_FIELD(m_BreakFromRootEventPlayer);
#else // ENABLE_FRAGMENT_EVENTS
	STRUCT_CONTAINED_ARRAY(m_Pad2);
#endif // ENABLE_FRAGMENT_EVENTS

#if FRAG_TYPE_CHILD_PADDING_NEEDED > 0
	STRUCT_CONTAINED_ARRAY(pad);
#endif

	STRUCT_END();
}
#endif // __DECLARESTRUCT

fragTypeChild::~fragTypeChild()
{
	// We don't own the shader groups!
	if (m_DamagedEntity)
	{
		m_DamagedEntity->SetShaderGroup(NULL);
		m_DamagedEntity->SetSkeletonDataPtrAfterPageIn(*(crSkeletonData*)NULL);
		delete m_DamagedEntity;
	}

	if (m_UndamagedEntity)
	{
		m_UndamagedEntity->SetShaderGroup(NULL);
		m_UndamagedEntity->SetSkeletonDataPtrAfterPageIn(*(crSkeletonData*)NULL);
		delete m_UndamagedEntity;
	}

#if ENABLE_FRAGMENT_EVENTS
	if (m_CollisionEventPlayer)
	{
		m_CollisionEventPlayer->Stop(0.0f, true);
		delete m_CollisionEventPlayer;
	}

	if (m_BreakEventPlayer)
	{
		m_BreakEventPlayer->Stop(0.0f, true);
		delete m_BreakEventPlayer;
	}

	if (m_BreakFromRootEventPlayer)
	{
		m_BreakFromRootEventPlayer->Stop(0.0f, true);
		delete m_BreakFromRootEventPlayer;
	}

	delete m_ContinuousEventset;
	delete m_CollisionEventset;
	delete m_BreakEventset;
    delete m_BreakFromRootEventset;
#endif // ENABLE_FRAGMENT_EVENTS
}

#if ENABLE_FRAGMENT_EVENTS
void fragTypeChild::SetBreakEventset(const evtSet* breakEventset)
{
	if (breakEventset)
	{
		if (m_BreakEventPlayer == NULL)
		{
			m_BreakEventPlayer = rage_new fragBreakEventPlayer;
			m_BreakEventPlayer->CreateParameterList();
			Assert(m_BreakEventset == NULL);
			m_BreakEventset = rage_new evtSet;
			m_BreakEventPlayer->SetSet(*m_BreakEventset);
		}
		*m_BreakEventset = *breakEventset;
	}
}

void fragTypeChild::SetBreakFromRootEventset(const evtSet* breakEventset)
{
	if (breakEventset)
	{
		if (m_BreakFromRootEventPlayer == NULL)
		{
			m_BreakFromRootEventPlayer = rage_new fragBreakEventPlayer;
			m_BreakFromRootEventPlayer->CreateParameterList();
			Assert(m_BreakFromRootEventset == NULL);
			m_BreakFromRootEventset = rage_new evtSet;
			m_BreakFromRootEventPlayer->SetSet(*m_BreakFromRootEventset);
		}
		*m_BreakFromRootEventset = *breakEventset;
	}
}
#endif // ENABLE_FRAGMENT_EVENTS

void fragTypeChild::Set(fragTypeChild& other FRAGMENT_EVENT_PARAM(bool setEvents))
{
	if (other.GetUndamagedMass() > 0.0f)
	{
		SetUndamagedMass(other.GetUndamagedMass());
	}
	if (other.GetDamagedMass()	> 0.0f)
	{
		SetDamagedMass(other.GetDamagedMass());
	}
	m_Flags					= other.m_Flags;

#if ENABLE_FRAGMENT_EVENTS
    if (setEvents)
    {
	    if (other.m_ContinuousEventset)
	    {
		    if(m_ContinuousEventset == NULL)
		    {
			    m_ContinuousEventset = rage_new evtSet;
		    }
		    *m_ContinuousEventset = *other.m_ContinuousEventset;
	    }
    	
	    if (other.m_CollisionEventset)
	    {
			if (m_CollisionEventPlayer == NULL)
			{
				m_CollisionEventPlayer = rage_new fragCollisionEventPlayer;
				m_CollisionEventPlayer->CreateParameterList();
				Assert(m_CollisionEventset == NULL);
			    m_CollisionEventset = rage_new evtSet;
				m_CollisionEventPlayer->SetSet(*m_CollisionEventset);
			}
		    *m_CollisionEventset = *other.m_CollisionEventset;
	    }

	    if (other.m_BreakEventset)
	    {
			if (m_BreakEventPlayer == NULL)
			{
				m_BreakEventPlayer = rage_new fragBreakEventPlayer;
				m_BreakEventPlayer->CreateParameterList();
				Assert(m_BreakEventset == NULL);
			    m_BreakEventset = rage_new evtSet;
				m_BreakEventPlayer->SetSet(*m_BreakEventset);
			}
		    *m_BreakEventset = *other.m_BreakEventset;
	    }

        if (other.m_BreakFromRootEventset)
        {
			if (m_BreakFromRootEventPlayer == NULL)
			{
				m_BreakFromRootEventPlayer = rage_new fragBreakEventPlayer;
				m_BreakFromRootEventPlayer->CreateParameterList();
				Assert(m_BreakFromRootEventset == NULL);
			    m_BreakFromRootEventset = rage_new evtSet;
				m_BreakFromRootEventPlayer->SetSet(*m_BreakFromRootEventset);
			}
            *m_BreakFromRootEventset = *other.m_BreakFromRootEventset;
        }
    }
#endif // ENABLE_FRAGMENT_EVENTS
}

void fragTypeChild::UnPlace()
{
//	ObjectUnPlace(m_UndamagedEntity); // TODO: drawables don't need unplace anymore?
//	ObjectUnPlace(m_DamagedEntity);
}

void fragTypeChild::LoadDisappearsWhenDead(rmcTypeFileCbData* OUTPUT_ONLY(data))
{
	// This is pretty hacky, but it's only temporary until we stop trying to support old tune files.  Basically, we're relying on the assumption that
	//   fragTypeGroup::FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD is 1.
	s_Flags |= 1;
	Warningf("Found old 'DisappearsWhenDead' tuning value in child tune data for fragment '%s'.  Please re-save tune file.", data->m_T->GetName());
}

void fragTypeChild::LoadDamagePassesThrough(rmcTypeFileCbData* OUTPUT_ONLY(data))
{
	Warningf("Found old 'DamagePassesThrough' tuning value in child tune data for fragment '%s'.  Please re-save tune file.", data->m_T->GetName());
}

void fragTypeChild::LoadMinDamageForce(rmcTypeFileCbData* OUTPUT_ONLY(data))
{
	Warningf("Found old 'MinDamageForce' tuning value in child tune data for fragment '%s'.  Please re-save tune file.", data->m_T->GetName());
}

void fragTypeChild::LoadDamageHealth(rmcTypeFileCbData* OUTPUT_ONLY(data))
{
	Warningf("Found old 'DamageHealth' tuning value in child tune data for fragment '%s'.  Please re-save tune file.", data->m_T->GetName());
}

void fragTypeChild::LoadPristineMass(rmcTypeFileCbData* data)
{
	SetUndamagedMass(data->m_T->GetFloat());
}

void fragTypeChild::LoadDamagedMass(rmcTypeFileCbData* data)
{
	SetDamagedMass(data->m_T->GetFloat());
}

//void fragTypeChild::LoadParentBoneIndexDeprecated(rmcTypeFileCbData* data)
//{
//	Assign(m_BoneIndexDeprecated, data->m_T->GetInt());
//}

void fragTypeChild::LoadParentBoneID(rmcTypeFileCbData* data)
{
	Assign(m_BoneID, data->m_T->GetInt());
}

#if ENABLE_FRAGMENT_EVENTS
void fragTypeChild::LoadContinuousEventset(rmcTypeFileCbData* data)
{
	data->m_T->CheckToken("{");
	PARSER.LoadObjectPtr(data->m_T->GetStream(), (evtSet*&)m_ContinuousEventset);
	data->m_T->CheckToken("}");
}

void fragTypeChild::LoadCollisionEventset(rmcTypeFileCbData* data)
{
	data->m_T->CheckToken("{");
	PARSER.LoadObjectPtr(data->m_T->GetStream(), (evtSet*&)m_CollisionEventset);
	data->m_T->CheckToken("}");

	Assert(m_CollisionEventPlayer == NULL);
	
	m_CollisionEventPlayer = rage_new fragCollisionEventPlayer;
	m_CollisionEventPlayer->CreateParameterList();
	m_CollisionEventPlayer->SetSet(*m_CollisionEventset);
}

void fragTypeChild::LoadBreakEventset(rmcTypeFileCbData* data)
{
	data->m_T->CheckToken("{");
	PARSER.LoadObjectPtr(data->m_T->GetStream(), (evtSet*&)m_BreakEventset);
	data->m_T->CheckToken("}");

	Assert(m_BreakEventPlayer == NULL);
	
	m_BreakEventPlayer = rage_new fragBreakEventPlayer;
	m_BreakEventPlayer->CreateParameterList();
	m_BreakEventPlayer->SetSet(*m_BreakEventset);
}

void fragTypeChild::LoadBreakFromRootEventset(rmcTypeFileCbData* data)
{
    data->m_T->CheckToken("{");
    PARSER.LoadObjectPtr(data->m_T->GetStream(), (evtSet*&)m_BreakFromRootEventset);
    data->m_T->CheckToken("}");

	Assert(m_BreakFromRootEventPlayer == NULL);
	m_BreakFromRootEventPlayer = rage_new fragBreakEventPlayer;
	m_BreakFromRootEventPlayer->CreateParameterList();
    m_BreakFromRootEventPlayer->SetSet(*m_BreakFromRootEventset);
}
#endif // ENABLE_FRAGMENT_EVENTS

#if MESH_LIBRARY
fragTypeChild* fragTypeChild::LoadASCII(fiAsciiTokenizer& asciiTok, 
										grmShaderGroup* shaderGroup,
										u8 parentIndex,
										//int boneIndex,
										int boneID)
{
#if __RESOURCECOMPILER
	strcpy(phBound::s_currentFragChildBoneName, "");

	if (s_currentLoadingFragType)
	{
		const crSkeletonData* sd = s_currentLoadingFragType->GetCommonDrawable()->GetSkeletonData();

		if (sd)
		{
			const int boneIndex = s_currentLoadingFragType->GetBoneIndexFromID((u16)boneID);

			if (boneIndex >= 0 && boneIndex < sd->GetNumBones())
			{
				strcpy(phBound::s_currentFragChildBoneName, sd->GetBoneData(boneIndex)->GetName());
			}
		}
	}
#endif // __RESOURCECOMPILER

	fragTypeChild* child = rage_new fragTypeChild;
	//Assign(child->m_BoneIndexDeprecated, boneIndex);
	Assign(child->m_BoneID, boneID);
	child->m_OwnerGroupPointerIndex = parentIndex;

	child->m_UndamagedEntity = rage_new fragDrawable;
	child->m_UndamagedEntity->SetShaderGroup(shaderGroup);

	rmcTypeFileParser parser;

	// Eventually this block can go away, if/when we stop supporting the loading of old tune files.
	parser.RegisterLoader( "DisappearsWhenDead", "all", datCallback(MFA1(fragTypeChild::LoadDisappearsWhenDead), child, 0, true) );
	parser.RegisterLoader( "DamagePassesThrough", "all", datCallback(MFA1(fragTypeChild::LoadDamagePassesThrough), child, 0, true) );
	parser.RegisterLoader( "MinDamageForce", "all", datCallback(MFA1(fragTypeChild::LoadMinDamageForce), child, 0, true) );
	parser.RegisterLoader( "DamageHealth", "all", datCallback(MFA1(fragTypeChild::LoadDamageHealth), child, 0, true) );

	parser.RegisterLoader( "PristineMass", "all", datCallback(MFA1(fragTypeChild::LoadPristineMass), child, 0, true) );
	parser.RegisterLoader( "DamagedMass", "all", datCallback(MFA1(fragTypeChild::LoadDamagedMass), child, 0, true) );

	// Bone indices are deprecated
	//parser.RegisterLoader( "ParentBoneIndex", "all", datCallback(MFA1(fragTypeChild::LoadParentBoneIndexDeprecated), child, 0, true) );

	parser.RegisterLoader( "ParentBoneID", "all", datCallback(MFA1(fragTypeChild::LoadParentBoneID), child, 0, true) );
#if ENABLE_FRAGMENT_EVENTS
	parser.RegisterLoader( "continuousEvents", "all", datCallback(MFA1(fragTypeChild::LoadContinuousEventset), child, 0, true) );
	parser.RegisterLoader( "collisionEvents", "all", datCallback(MFA1(fragTypeChild::LoadCollisionEventset), child, 0, true) );
	parser.RegisterLoader( "breakEvents", "all", datCallback(MFA1(fragTypeChild::LoadBreakEventset), child, 0, true) );
    parser.RegisterLoader( "BreakFromRootEvents", "all", datCallback(MFA1(fragTypeChild::LoadBreakFromRootEventset), child, 0, true) );
#endif // ENABLE_FRAGMENT_EVENTS

	parser.RegisterLoader( "bound", "all", datCallback(MFA1(fragDrawable::LoadBound), child->m_UndamagedEntity, 0, true) );
	parser.RegisterLoader( "lodgroup", "all", datCallback(MFA1(rmcDrawable::LoadMesh), child->m_UndamagedEntity, 0, true) );
	parser.RegisterLoader( "edge", "all", datCallback(MFA1(rmcDrawable::LoadEdgeModel), child->m_UndamagedEntity, 0, true) );

	parser.ProcessTypeFile(static_cast<fiTokenizer&>(asciiTok), true);

	//Assert(boneIndex == child->m_BoneIndex);

	if (child->GetUndamagedMass() < 0.0f)
	{
		if (float volumeMass = child->ComputeMassFromVolume(*child->GetUndamagedEntity()))
		{
			child->SetUndamagedMass(volumeMass);
			child->SetDamagedMass(volumeMass);
		}
		else
		{
			child->SetUndamagedMass(1.0f);
			child->SetDamagedMass(1.0f);
		}
	}

#if ENABLE_FRAGMENT_EVENTS
	if(child->m_ContinuousEventset == NULL)
	{
		child->m_ContinuousEventset = rage_new evtSet;
	}
#endif // ENABLE_FRAGMENT_EVENTS

	return child;
}

void fragTypeChild::LoadDamagedASCII(fiAsciiTokenizer& asciiTok, 
									 grmShaderGroup* shaderGroup)
{
	char name[fiBaseTokenizer::PUSH_BUFFER_SIZE];
	name[0] = '\0';
	asciiTok.GetToken(name,sizeof(name));

	rmcTypeFileParser parser;

	// If there are more than one damaged children, ignore the second and further ones
	if (m_DamagedEntity == NULL)
	{
		m_DamagedEntity = rage_new fragDrawable;
		m_DamagedEntity->SetShaderGroup(shaderGroup);

		parser.RegisterLoader( "bound", "all", datCallback(MFA1(fragDrawable::LoadBound), m_DamagedEntity, 0, true) );
		parser.RegisterLoader( "lodgroup", "all", datCallback(MFA1(rmcDrawable::LoadMesh), m_DamagedEntity, 0, true) );
		parser.RegisterLoader( "edge", "all", datCallback(MFA1(rmcDrawable::LoadEdgeModel), m_DamagedEntity, 0, true) );
	}

	parser.ProcessTypeFile(static_cast<fiTokenizer&>(asciiTok), true);

	if (GetUndamagedMass() < 0.0f)
	{
		if (float volumeMass = ComputeMassFromVolume(*GetDamagedEntity()))
		{
			SetDamagedMass(volumeMass);
		}
		else
		{
			SetDamagedMass(1.0f);
		}
	}
}
#endif

float fragTypeChild::ComputeMassFromVolume(fragDrawable& drawable)
{
	float volume = 0.0f;

	if (drawable.GetBound())
	{
		volume = drawable.GetBound()->GetVolume();
	}

	if (volume < 0.0f)
	{
#if __RESOURCECOMPILER		// Tired of this making ragebuilder look like it failed
		Warningf("Bound part has volume %f!", volume);
#else
		Errorf("Bound part has volume %f!", volume);
#endif
		volume = fabs(volume);
	}

	//Displayf("Mass %f", volume * phArchetypePhys::GetDefaultDensity() * 1000.0f);
	return Max(1.0f, volume * phArchetypePhys::GetDefaultDensity() * 1000.0f);
}

#if __BANK
void fragTypeChild::SaveASCII(fiAsciiTokenizer& T)
{
	T.StartLine();	T.PutDelimiter("child\t");
	T.StartBlock();
	T.StartLine();	T.PutDelimiter("pristineMass all\t");   T.Put(GetUndamagedMass());  T.EndLine();
	T.StartLine();	T.PutDelimiter("damagedMass all\t");    T.Put(GetDamagedMass());    T.EndLine();

#if ENABLE_FRAGMENT_EVENTS
	if (m_ContinuousEventset && m_ContinuousEventset->GetNumInstances() > 0)
	{
		T.StartLine(); T.PutDelimiter("continuousEvents all\t");
		T.StartBlock();

		PARSER.SaveObject(T.GetStream(), (evtSet*)m_ContinuousEventset);

		T.EndLine();
		T.EndBlock();
	}

	if (m_CollisionEventset && m_CollisionEventset->GetNumInstances() > 0)
	{
		T.StartLine(); T.PutDelimiter("collisionEvents all\t");
		T.StartBlock();

		PARSER.SaveObject(T.GetStream(), (evtSet*)m_CollisionEventset);

		T.EndLine();
		T.EndBlock();
	}

	if (m_BreakEventset && m_BreakEventset->GetNumInstances() > 0)
	{
		T.StartLine(); T.PutDelimiter("breakEvents all\t");
		T.StartBlock();

		PARSER.SaveObject(T.GetStream(), (evtSet*)m_BreakEventset);

		T.EndLine();
		T.EndBlock();
	}

    if (m_BreakFromRootEventset && m_BreakFromRootEventset->GetNumInstances() > 0)
    {
        T.StartLine(); T.PutDelimiter("BreakFromRootEvents all\t");
        T.StartBlock();

        PARSER.SaveObject(T.GetStream(), (evtSet*)m_BreakFromRootEventset);

        T.EndLine();
        T.EndBlock();
    }
#endif // ENABLE_FRAGMENT_EVENTS

	T.EndBlock();
}
#endif // __BANK

#if __RESOURCECOMPILER
fragType* fragTypeChild::s_currentLoadingFragType = NULL;
#endif // __RESOURCECOMPILER

} // namespace rage
