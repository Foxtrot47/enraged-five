<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::fragTuneBreakPreset" constructable="true">
	<string name="m_Name" type="ConstString"/>
	<float name="m_Strength" min="100.0f" max="1000.0f" step="0.5f"/>
	<float name="m_ForceTransmissionUp" min="0.25f" max="1.0f" step="0.1f"/>
	<float name="m_ForceTransmissionDown" min="0.25f" max="1.0f" step="0.1f"/>
	<float name="m_MinDamageForce" min="100.0f" max="1000.0f" step="1.0f"/>
	<float name="m_DamageHealth" min="1000.0f" max="10000.0f" step="1.0f"/>
	<float name="m_WeaponHealth" min="0.0f" max="10000.0f" step="1.0f"/>
	<float name="m_WeaponScale" min="1.0f" max="10.0f" step="0.1f"/>
	<float name="m_MeleeScale" min="1.0f" max="10.0f" step="0.1f"/>
	<float name="m_VehicleScale" min="1.0f" max="10.0f" step="0.1f"/>
	<float name="m_PedScale" min="1.0f" max="10.0f" step="0.1f"/>
	<float name="m_RagdollScale" min="1.0f" max="10.0f" step="0.1f"/>
	<float name="m_ExplosionScale" min="1.0f" max="10.0f" step="0.1f"/>
	<float name="m_ObjectScale" min="1.0f" max="10.0f" step="0.1f"/>
</structdef>

<structdef type="::rage::fragTuneStruct" constructable="false">
  <float name="GlobalMaxDrawingDistance" min="0.0f" max="10000.0f" step="10.0f"/>
  <float name="GlobalForceTransmissionScaleDown" min="0.0f" max="10.0f" step="0.01f"/>
  <float name="GlobalForceTransmissionScaleUp" min="0.0f" max="10.0f" step="0.01f"/>
  <float name="GlobalRootBreakHealthScale" min="0.0f" max="1000000.0f" step="0.001f"/>
  <float name="GlobalBreakHealthScale" min="0.0f" max="1000000.0f" step="0.005f"/>
  <float name="GlobalDamageHealthScale" min="0.0f" max="10.0f" step="0.005f"/>
  <float name="BreakingFrameRateLimit" min="0.0f" max="60.0f" step="5.0f"/>
  <bool name="EnableCompleteImpulsePropagation"/>
  <bool name="AggressiveAssetTouching"/>
  <bool name="AllowArticulation"/>
  <bool name="AllowSimplifiedColliders"/>
  <bool name="FragDebug1" description="Spew information to the TTY to explain breaking and damage decisions being made at runtime"/>
  <bool name="FragDebug2" description="Spew EVEN MORE information to the TTY to explain breaking and damage decisions being made at runtime"/>
  <!--
// HACK_GTA4	// EUGENE_APPROVED
// Don't want the collision damage scale to have any effect for gta because it distorts the values the props guys are tuning
// So just put the limits very high
-->
  <float name="CollisionDamageScale" min="0.0f" max="10000.0f" step="0.005f"/>
  <float name="CollisionDamageMinThreshold" min="0.0f" max="10000.0f" step="0.005f"/>
  <float name="CollisionDamageMaxThreshold" min="0.0f" max="10000.0f" step="5.0f"/>
  <float name="ReferenceMassForDefaultTuning" min="0.0f" max="1000000.0f" step="5.0f"/>

  <array name="m_BreakPresets" type="atArray" hideWidgets="true">
	<struct type="::rage::fragTuneBreakPreset"/>
  </array>

</structdef>

</ParserSchema>