// 
// fragment/meshcache.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "meshcache.h"

#include "grmodel/model.h"
#include "rmcore/lodgroup.h"
#include "rmcore/typefileparser.h"

#if MESH_LIBRARY

namespace rage {
	fragMeshCache::~fragMeshCache()
	{
		sysMemStartTemp();
		int meshCount=m_meshes.GetCount();
		for (int meshIndex=0; meshIndex<meshCount; meshIndex++)
		{
			delete m_meshes[meshIndex];
		}
		m_meshes.Reset();
		SetShaderGroup(NULL);
		sysMemEndTemp();
	}

	void fragMeshCache::SetShaderGroup(grmShaderGroup* in_pShaderGroup)
	{
		m_Drawable.SetShaderGroup(in_pShaderGroup);
	}

	void fragMeshCache::LoadDrawables(rmcTypeFileCbData* data)
	{
		sysMemStartTemp();
		if (stricmp(data->m_EntityName, "mesh") == 0)
		{
			InstallMeshLoaderHook();
			m_Drawable.LoadMesh(data);
			RemoveMeshLoaderHook();
		}
		sysMemEndTemp();
	}

	bool fragMeshCache::CacheMesh(mshMesh& in_mesh, const char*)
	{
		sysMemStartTemp();
		m_meshes.Grow() = &in_mesh;
		sysMemEndTemp();
		return false; // don't delete the mesh!
	}

	void fragMeshCache::InstallMeshLoaderHook()
	{
		grmModel::MeshLoaderHookParams meshLoaderHook;
		meshLoaderHook.Reset<fragMeshCache, &fragMeshCache::CacheMesh>(this);
		grmModel::SetMeshLoaderHook(meshLoaderHook);
	}

	void fragMeshCache::RemoveMeshLoaderHook()
	{
		grmModel::RemoveMeshLoaderHook();
	}


	mshMaterial* fragMeshCache::GetMshMaterial(int targetLod, int targetModelIndex, int targetGeomIndex) const
	{
		int modelIndex = 0;
		const rmcLodGroup& lodGroup = m_Drawable.GetLodGroup();
		for (int lodIndex = 0; lodIndex <= targetLod; lodIndex++)
		{
			if (!lodGroup.ContainsLod(lodIndex))
				continue;
			const rmcLod& lod = m_Drawable.GetLodGroup().GetLod(lodIndex);
			if (lodIndex == targetLod)
			{
				return &m_meshes[modelIndex + targetModelIndex]->GetMtl(targetGeomIndex);
			}
			else
			{
				modelIndex += lod.GetCount();
			}
		}
		return NULL;
	}

	// return true if at least one vertex in the material is weighted more than 50% to the specified bone
	bool fragMeshCache::IsSkinnedMeshUsingBone(mshMaterial* pMaterial, int boneIndex)
	{
		int vertexCount = pMaterial->GetVertexCount();

		for (int vertexIndex = 0; vertexIndex < vertexCount; ++vertexIndex)
		{
			const mshBinding& binding = pMaterial->GetVertex(vertexIndex).Binding;
			for (int i=0; i<mshMaxMatricesPerVertex; i++)
			{
				if ((binding.IsPassThrough == false) &&
					(binding.Wgt[i] > 0.5f) &&
					(binding.Mtx[i] == boneIndex))
				{
					return true;
				}
			}
		}
		return false;
	}

	bool fragMeshCache::GetModelAndGeomIndexFromBoneIndex(int lodIndex, int boneIndex, int& out_ModelIndex, int& out_GeomIndex) const
	{
		if (!m_Drawable.GetLodGroup().ContainsLod(lodIndex))
		{
			return false;
		}

		const rmcLod& lod = m_Drawable.GetLodGroup().GetLod(lodIndex);
		int modelCount = lod.GetCount();
		int modelIndex;
		for (modelIndex = 0; modelIndex < modelCount; ++modelIndex)
		{
			const grmModel* model = lod.GetModel(modelIndex);

			if (model->GetSkinFlag())
			{
				int geomCount = model->GetGeometryCount();
				for (int geomIndex = 0; geomIndex < geomCount; geomIndex++)
				{
					mshMaterial* pMshMaterial = GetMshMaterial(lodIndex, modelIndex, geomIndex);
					if (IsSkinnedMeshUsingBone(pMshMaterial, boneIndex))
					{
						out_ModelIndex = modelIndex;
						out_GeomIndex = geomIndex;
						return true;
					}
				}
			}
			else
			{
				// no skinning; just return the first geometry of the first model to use the bone
				if (model->GetMatrixIndex() == boneIndex)
				{
					out_ModelIndex = modelIndex;
					out_GeomIndex = 0;
					return true;
				}
			}
		}
		return false;
	}


} // namespace rage

#endif		// MESH_LIBRARY

