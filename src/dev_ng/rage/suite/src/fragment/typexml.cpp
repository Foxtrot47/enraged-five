// 
// fragment/typexml.cpp 
//  
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "cloth_config.h"
#include "drawable.h"
#include "manager.h"
#include "type.h"
#include "typechild.h"
#include "typecloth.h"
#include "typegroup.h"

#include "cranimation/frame.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "diag/output.h"
#include "grmodel/matrixset.h"
#include "parser/manager.h"
#include "phbound/boundbox.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"

#include "pharticulated/joint.h"
#include "pharticulated/joint1dof.h"
#include "pharticulated/joint3dof.h"
#include "pharticulated/prismaticjoint.h"
#include "pharticulated/bodypart.h"
#include "pharticulated/articulatedbody.h"
#include "fragment/typephysicslod.h"

namespace rage {

static fragType::JointParams *m_JointParams = NULL;

inline bool IsWhiteSpace(char c) {
	return (c == ' ' || c == '\t' || c == '\n' || c == '\r' || c == '\f' || c == '\v' || c == '"');
}

template< typename T, typename U >
T*
StringStrip( T *dest, const U *src, int destSize )
{
    FastAssert( dest && src );
    FastAssert( destSize > 0 );
    size_t ch;

    T* result = dest;

    while( --destSize > 0 && ( ch = *src++ ) != '\0' )
    {
        if( ch >= 'A' && ch <= 'Z' )
            ch += 32;
        else if ( ch == '\\' )
            ch = '/';

		if (!IsWhiteSpace((char)ch))
		{
			*dest++ = T( ch );
		}
    }

    *dest = '\0';

    return result;
}

#if MESH_LIBRARY
void fragType::LoadWithXML(const char* typeFile, const char* nmXmlFile)
{
	sysMemStartTemp();
	parTree* nmTree = PARSER.LoadTree(nmXmlFile, "xml");
	sysMemEndTemp();

	Matrix34* bones = NULL;
	crSkeletonData* skeletonData = NULL;

	fiSafeStream typeStream = ASSET.Open(typeFile, "type", true, true);

	if (typeStream == NULL)
	{
		return;
	}

	fragType::SetLoadingFragType(this);

	char uniqueName[RAGE_MAX_PATH];
	StringNormalize(uniqueName, typeFile, sizeof(uniqueName));

	char* lastSlash = strrchr(uniqueName, '/');
	AssertMsg(lastSlash, "You have to call fragType::Load with at least one parent directory in the argument, or it won't know what tune file to look for");
	*lastSlash = '\0';

	char normalizedFile[RAGE_MAX_PATH];
	strcpy(normalizedFile, uniqueName);

	fiAsciiTokenizer typeTok;

	Displayf(TBlue"Regenerating fragment type %s", uniqueName);

	typeTok.Init(typeFile, typeStream);

	char srcDir[256];
	ASSET.RemoveNameFromPath(srcDir,sizeof(srcDir),typeFile);
	ASSET.PushFolder(srcDir);

	m_TuneName = strcpy(rage_new char[strlen(uniqueName) + 1], uniqueName);

	m_CommonDrawable = rage_new fragDrawable;

	rmcTypeFileParser parser;
	RegisterLoaders(parser, true);
	parser.RegisterLoader( "lodgroup", "all", datCallback(MFA1(fragType::LoadCommonDrawables), this, 0, true) );
	parser.ProcessTypeFile(static_cast<fiTokenizer&>(typeTok), true);

	parTreeNode* nmRoot = nmTree->GetRoot();
	Assertf(strcmp(nmRoot->GetElement().GetName(), "LODGroup") == 0, "fragType::LoadWithXML - %s needs to be wrapped in an LOD group in the format ""<LODGroup>"".", nmXmlFile);
	
	// Set NM Agent ID
	parTreeNode* NMAgentID = nmRoot->FindChildWithName("NMAgent");
	Assertf(NMAgentID, "fragType::LoadWithXML - %s needs to be fixed up with the NM agent ID in the format ""<NMAgent>0</NMAgent>"".", nmXmlFile);
	s8 artID = (s8) atoi(NMAgentID->GetData());
	SetARTAssetID(artID);

	// if the following is false then don't load any physics data, probably because it's going to share physics data
	// with another fragType once loaded
	bool bLoadingPhysics = nmRoot->GetChild()->GetSibling() ? true : false;
	if (bLoadingPhysics)
	{
		m_PhysicsLODGroup = rage_new fragPhysicsLODGroup();
	}
	else
	{
		SetNeedsCacheEntryToActivate();

		bones = NULL;
		u32 numBones = 0;
		skeletonData = GetCommonDrawable()->GetSkeletonData();

		if (skeletonData)
		{
			numBones = skeletonData->GetNumBones();
			AssertMsg(numBones <= FRAG_MAX_NUM_BONES , "Maximum number of bones for a fragment type exceeded!");

			sysMemStartTemp();
			crSkeleton* tempSkeleton = rage_new crSkeleton();

			tempSkeleton->Init(*skeletonData, NULL);
			sysMemEndTemp();

			m_SharedMatrixSet = NULL;
#if __RESOURCECOMPILER
			if (ms_bCreateMatrixSet)
#endif // __RESOURCECOMPILER
				m_SharedMatrixSet = grmMatrixSet::Create(numBones);

			// Allocate some matrices and store the authored global matrices in there
			bones = Alloca(Matrix34, numBones);
			for (int bone = 0; bone < skeletonData->GetNumBones(); ++bone)
			{
				tempSkeleton->GetGlobalMtx(bone, RC_MAT34V(bones[bone]));
			}

			sysMemStartTemp();
			delete tempSkeleton;
			sysMemEndTemp();
		}

		if (m_CommonDrawable->GetLodGroup().GetCullRadius() > 0.0f)
		{
			m_BoundingSphere.SetVector3(m_CommonDrawable->GetLodGroup().GetCullSphere());
			m_BoundingSphere.w = m_CommonDrawable->GetLodGroup().GetCullRadius();
		}
		else
			AssertMsg(0, "GetCullRadius() is negative");

		//////////////////////////////////////////////////////////////////////////

		// Since this is not going through normal drawable loading code, gotta call set shader group to get
		//	appropriate info passed down..
		if (&m_CommonDrawable->GetShaderGroup())
		{
			m_CommonDrawable->SetShaderGroup(&m_CommonDrawable->GetShaderGroup());
		}

		// load cloth in another pass, since lodgroups may have not have been loaded in the first pass.

		parser.Reset();
		parser.RegisterLoader( "cloth", "all", datCallback(MFA1(fragType::LoadEnvCloth), this, 0, true) );

		typeTok.Reset();
		parser.ProcessTypeFile(static_cast<fiTokenizer&>(typeTok), true);

		//TODO: look for a better way to store the cloth while loading.  This is a little hacky, JGG		
		const int envClothCount = m_EnvCloth.GetCount();
		if( envClothCount )
		{
			fragTypeEnvCloth** temp = Alloca(fragTypeEnvCloth*, envClothCount);
			for(int i=0; i<envClothCount; ++i)
				temp[i] = m_EnvCloth[i];

			sysMemStartTemp();
			m_EnvCloth.Reset();
			sysMemEndTemp();

			m_EnvCloth.Reserve(envClothCount);

			for(int i=0; i<envClothCount; ++i)
				m_EnvCloth.Push(temp[i]);
		}
		else
		{
			sysMemStartTemp();
			m_EnvCloth.Reset();
			sysMemEndTemp();
		}
	}

	// Loop for each LOD in the XML
	for(parTreeNode* nmChild = nmRoot->GetChild()->GetSibling(); nmChild; nmChild = nmChild->GetSibling())
	{
		Assert(strcmp(nmChild->GetElement().GetName(), "PhysicsCharacter") == 0);

		// Get LOD number
		parTreeNode* currentLODNode = nmChild->FindChildWithName("LOD");
		Assertf(currentLODNode, "fragType::LoadWithXML - %s needs to be fixed up with an LOD number in the format ""<LOD>0</LOD>"".", nmXmlFile);
		s8 currentLOD = (s8) atoi(currentLODNode->GetData());
		Assertf(currentLOD >= 0 && currentLOD < 3, "fragType::LoadWithXML - %s contains an unhandled LOD number of %d", nmXmlFile, currentLOD);
		bool firstPass = currentLOD == 0;

		// Set the current LOD to be processed
		if (currentLOD == 0)
			Assert(GetPhysicsLODGroup()->GetLODByIndex(currentLOD) != NULL);  // The high LOD should be created by default 
		else
		{
			Assert(GetPhysicsLODGroup()->GetLODByIndex(currentLOD) == NULL);
			GetPhysicsLODGroup()->CreateLOD(currentLOD);
		}

		// Load the joints, limits, and bounds
		if (!LoadPhysicsRigFromXML(currentLOD, nmChild->GetChild()->GetSibling()->GetChild()))
		{
			break;
		}

		// Load self collisions
		LoadSelfCollisionsFromXML(currentLOD, nmChild->GetChild()->GetSibling()->GetSibling());

		//////////////////////////////////////////////////////////////////////////

		//allocate the composite bounds stuff...
		GetPhysics(currentLOD)->m_CompositeBounds = rage_new phBoundComposite();
		GetPhysics(currentLOD)->m_LinkAttachments = pgArray<Matrix34>::Create(GetPhysics(currentLOD)->GetNumChildren());

		GetPhysics(currentLOD)->GetCompositeBounds()->Init(GetPhysics(currentLOD)->GetNumChildren());

		for (int matrix = 0; matrix < GetPhysics(currentLOD)->GetNumChildren(); ++matrix)
		{
			GetPhysics(currentLOD)->GetCompositeBounds()->SetCurrentMatrix(matrix, RCC_MAT34V(GetPhysics(currentLOD)->GetChild(matrix)->GetUndamagedEntity()->GetBoundMatrix()));
			GetPhysics(currentLOD)->GetLinkAttachmentArray()->GetElement(matrix).Set(M34_IDENTITY);
		}

		/*since we don't have everything we need yet for the bounds, we'll defer initialization to load time - This is 
		unfortunate because the inertia calculation is not the cheapest thing in the world.  So, each time we load an 
		archetype, we'll get a little bit if a hit to set up the physics archetype, which we'll also get each time 
		something breaks into pieces...
		*/
		GetPhysics(currentLOD)->m_PhysDampUndamaged = rage_new phArchetypeDamp;
		GetPhysics(currentLOD)->GetArchetype()->AddRef(); // Add a reference, since we reference the archetype by virtue of owning it
		GetPhysics(currentLOD)->GetArchetype()->SetFilename(GetBaseName());

		GetPhysics(currentLOD)->GetArchetype()->SetGravityFactor(m_GravityFactor);
		GetPhysics(currentLOD)->GetArchetype()->SetBuoyancyFactor(m_BuoyancyFactor);

#if !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
		//precompute the angular inertia vectors for all child bangers, so we don't have to compute them at load-time and run-time
		GetPhysics(currentLOD)->m_DamagedAngInertia = rage_new Vector3[GetPhysics(currentLOD)->GetNumChildren()];
		GetPhysics(currentLOD)->m_UndamagedAngInertia = rage_new Vector3[GetPhysics(currentLOD)->GetNumChildren()];
#endif // !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD

		if (firstPass)  // Only required once
		{
			SetNeedsCacheEntryToActivate();

			bones = NULL;
			u32 numBones = 0;
			skeletonData = GetCommonDrawable()->GetSkeletonData();

			if (skeletonData)
			{
				numBones = skeletonData->GetNumBones();
				AssertMsg(numBones <= FRAG_MAX_NUM_BONES , "Maximum number of bones for a fragment type exceeded!");

				int limitDof;
				Vector3 constrainDir, rotMin, rotMax;
				if (GetRootLinkConstraint(M34_IDENTITY, limitDof, constrainDir, rotMin, rotMax))
				{
					SetNeedsCacheEntryToActivate();
				}


				sysMemStartTemp();
				crSkeleton* tempSkeleton = rage_new crSkeleton();

				tempSkeleton->Init(*skeletonData, NULL);
				sysMemEndTemp();

				m_SharedMatrixSet = NULL;
#if __RESOURCECOMPILER
				if (ms_bCreateMatrixSet)
#endif // __RESOURCECOMPILER
					m_SharedMatrixSet = grmMatrixSet::Create(numBones);

				// Allocate some matrices and store the authored global matrices in there
				bones = Alloca(Matrix34, numBones);
				for (int bone = 0; bone < skeletonData->GetNumBones(); ++bone)
				{
					tempSkeleton->GetGlobalMtx(bone, RC_MAT34V(bones[bone]));
				}

				sysMemStartTemp();
				delete tempSkeleton;
				sysMemEndTemp();
			}
		}

		// Assign the skeleton data to all the children
		if (skeletonData)
		{
			for (int childIndex = 0; childIndex < GetPhysics(currentLOD)->GetNumChildren(); ++childIndex)
			{
				fragDrawable* undamaged = GetPhysics(currentLOD)->GetChild(childIndex)->GetUndamagedEntity();
				Assert(undamaged);
				undamaged->SetSkeletonDataPtrAfterPageIn(*skeletonData);

				if (fragDrawable* damaged = GetPhysics(currentLOD)->GetChild(childIndex)->GetDamagedEntity())
				{
					damaged->SetSkeletonDataPtrAfterPageIn(*skeletonData);
				}
			}
		}

		fragTypeChild** pChild = GetPhysics(currentLOD)->GetAllChildren();

		for (u32 child = 0; child < GetPhysics(currentLOD)->GetNumChildren(); ++child, ++pChild)
		{
			// Position bounds to be the same as their controlling bones.
			if (bones)
			{
				Assert(skeletonData != NULL);
				Matrix34 mtx;        //hs Matrix34 mtx(bones[boneIndex]);
				mtx.Set(RCC_MATRIX34(GetPhysics(currentLOD)->GetCompositeBounds()->GetCurrentMatrix(child)));
				mtx.Dot(bones[ GetBoneIndexFromID((*pChild)->GetBoneID()) ]);
				GetPhysics(currentLOD)->GetCompositeBounds()->SetCurrentMatrix(child, RCC_MAT34V(mtx));
				GetPhysics(currentLOD)->m_NumBonyChildren++;
			}
			phBound* undamagedBound = (*pChild)->m_UndamagedEntity->GetBound();
			GetPhysics(currentLOD)->GetCompositeBounds()->SetBound(child, undamagedBound);

			// Use the undamaged bound to compute the undamaged angular inertia from the undamaged mass.
			GetPhysics(currentLOD)->SetUndamagedAngInertia(child,VEC3V_TO_VECTOR3(undamagedBound->GetComputeAngularInertia((*pChild)->GetUndamagedMass())));
		}

		//////////////////////////////////////////////////////////////////////////

		GetPhysics(currentLOD)->SetPhysicsArchetypeMassFromChildren();

		GetPhysics(currentLOD)->GetCompositeBounds()->CalcCenterOfBound();
		GetPhysics(currentLOD)->GetCompositeBounds()->CalculateCompositeExtents();
		GetPhysics(currentLOD)->GetCompositeBounds()->UpdateBvh(true);
		GetPhysics(currentLOD)->GetArchetype()->SetBound(GetPhysics(currentLOD)->GetCompositeBounds());

		bool missingBound = false;
		float* masses = Alloca(float, GetPhysics(currentLOD)->GetNumChildren());
		Vector3* angularInertias = Alloca(Vector3, GetPhysics(currentLOD)->GetNumChildren());
		for (u32 childIndex = 0; childIndex < GetPhysics(currentLOD)->GetNumChildren(); ++childIndex)
		{
			const fragTypeChild* child = GetPhysics(currentLOD)->GetChild(childIndex);
			masses[childIndex] = child->GetUndamagedMass();
			angularInertias[childIndex] = GetPhysics(currentLOD)->GetUndamagedAngInertia(childIndex);

			if (child->GetUndamagedEntity()->GetBound() == NULL)
			{
				missingBound = true;
			}
		}

		if (!missingBound) // We are allowed to be missing bounds if we are loading a type file, in which case we need to skip this
		{
			GetPhysics(currentLOD)->GetArchetype()->ComputeCompositeAngInertia(masses, (Vec3V*)angularInertias);
		}

		if ( sm_UseFragManagerDefaultsInLoad )
		{
			GetPhysics(currentLOD)->GetArchetype()->SetTypeFlags(FRAGMGR->GetDefaultTypeFlags());
			GetPhysics(currentLOD)->GetArchetype()->SetIncludeFlags(FRAGMGR->GetDefaultIncludeFlags());
		}
		else
		{

			GetPhysics(currentLOD)->GetArchetype()->SetTypeFlags(phArchetype::DEFAULT_TYPE);
			GetPhysics(currentLOD)->GetArchetype()->SetIncludeFlags(0xffffffff);
		}
		const atArray< u32 >& specialFlags = FRAGMGR->GetSpecialTypeFlags();
		u32 mergedFlags = 0;
		for (int sf = 0; sf < specialFlags.GetCount(); sf++)
		{
			mergedFlags |= specialFlags[sf];
		}

		GetPhysics(currentLOD)->GetArchetype()->SetTypeFlags((mergedFlags|GetPhysics(currentLOD)->GetArchetype()->GetTypeFlags()));
		//since these are gatherd by the bound loading reset them now 
		FRAGMGR->ResetSpecialTypeFlags();

		//////////////////////////////////////////////////////////////////////////


		// load cloth in another pass, since lodgroups may have not have been loaded in the first pass.
		if (firstPass)  // Only required once
		{
			parser.Reset();

			parser.RegisterLoader( "cloth", "all", datCallback(MFA1(fragType::LoadEnvCloth), this, 0, true) );
//			parser.RegisterLoader( "charcloth", "all", datCallback(MFA1(fragType::LoadCharCloth), this, 0, true) );

			typeTok.Reset();
			parser.ProcessTypeFile(static_cast<fiTokenizer&>(typeTok), true);

			//TODO: look for a better way to store the cloth while loading.  This is a little hacky, JGG		
			const int envClothCount = m_EnvCloth.GetCount();
			if( envClothCount )
			{
				fragTypeEnvCloth** temp = Alloca(fragTypeEnvCloth*, envClothCount);
				for(int i=0; i<envClothCount; ++i)
					temp[i] = m_EnvCloth[i];

				sysMemStartTemp();
				m_EnvCloth.Reset();
				sysMemEndTemp();

				m_EnvCloth.Reserve(envClothCount);

				for(int i=0; i<envClothCount; ++i)
					m_EnvCloth.Push(temp[i]);
			}
			else
			{
				sysMemStartTemp();
				m_EnvCloth.Reset();
				sysMemEndTemp();
			}
		}


		if (firstPass)  // Only required once
		{
			if (m_CommonDrawable->GetLodGroup().GetCullRadius() > 0.0f)
			{
				m_BoundingSphere.SetVector3(m_CommonDrawable->GetLodGroup().GetCullSphere());
				m_BoundingSphere.w = m_CommonDrawable->GetLodGroup().GetCullRadius();
			}
			else
			{
				if (m_RootChild)
				{
					m_BoundingSphere.SetVector3(m_RootChild->m_UndamagedEntity->GetLodGroup().GetCullSphere());
					m_BoundingSphere.w = m_RootChild->m_UndamagedEntity->GetLodGroup().GetCullRadius();
				}
				else
				{
					m_BoundingSphere.SetVector3(GetPhysics(currentLOD)->GetChild(GetPhysics(currentLOD)->GetGroup(0)->GetChildFragmentIndex())
					->GetUndamagedEntity()->GetLodGroup().GetCullSphere());
					m_BoundingSphere.w = GetPhysics(currentLOD)->GetChild(GetPhysics(currentLOD)->GetGroup(0)->GetChildFragmentIndex())
					->GetUndamagedEntity()->GetLodGroup().GetCullRadius();
				}
			}
		}
		
#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
		GetPhysics(currentLOD)->m_MinBreakingImpulses = rage_new float[GetPhysics(currentLOD)->GetNumChildren()];
		ComputeMinBreakingImpulses(currentLOD, GetPhysics(currentLOD)->m_MinBreakingImpulses, NULL);
#endif // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES

		GetPhysics(currentLOD)->m_GroupNames = rage_new const char*[GetPhysics(currentLOD)->GetNumChildGroups() + 1];
		for (int group = 0; group < GetPhysics(currentLOD)->GetNumChildGroups(); ++group)
		{
			GetPhysics(currentLOD)->m_GroupNames[group] = GetPhysics(currentLOD)->GetGroup(group)->GetDebugName();
			GetPhysics(currentLOD)->GetGroup(group)->ComputeAndSetTotalUndamagedMass(GetPhysics(currentLOD));
		}
		GetPhysics(currentLOD)->m_GroupNames[GetPhysics(currentLOD)->GetNumChildGroups()] = "";

		if (firstPass)  // Only required once
		{
			//////////////////////////////////////////////////////////////////////////

			// Since this is not going through normal drawable loading code, gotta call set shader group to get
			//	appropriate info passed down..
			if (&m_CommonDrawable->GetShaderGroup())
			{
				m_CommonDrawable->SetShaderGroup(&m_CommonDrawable->GetShaderGroup());
			}
		}

		InitArticulatedLODManagerFromXMLData(currentLOD);

		sysMemStartTemp();
		if (m_JointParams)
			delete [] m_JointParams;
		sysMemEndTemp();
	}

	sysMemStartTemp();
	delete nmTree;
	sysMemEndTemp();

	fragType::SetLoadingFragType(NULL);

	ASSET.PopFolder();
}
#endif

void fragType::InitArticulatedLODManagerFromXMLData(int currentLOD)
{
	// Create the body type for the current LOD
	GetPhysics(currentLOD)->m_BodyType = rage_new phArticulatedBodyType;
	phArticulatedBodyType *bodyType = GetPhysics(currentLOD)->GetBodyType();

	//int i1 = sizeof(phArticulatedBodyType);i1; // 208
	//int i2 = sizeof(phArticulatedBodyPartType);i2; // 32
	//int i3 = sizeof(phJoint1DofType);i3; // 36
	//int i4 = sizeof(phJoint1DofCoreType);i4; // 192
	//int i5 = sizeof(phJoint3DofType);i5; // 64
	//int i6 = sizeof(phJoint3DofCoreType);i6; // 208
	//int i7 = sizeof(fragType);i7; // 368 

	CompileTimeAssert(sizeof(phJoint3DofType) >= sizeof(phJoint1DofType));
	CompileTimeAssert(sizeof(phJoint3DofType) >= sizeof(phPrismaticJointType));

	// Allocate the part and joint types
	bodyType->m_ResourcedAngInertiaXYZmassW = rage_new Vec4V[GetPhysics(currentLOD)->GetNumChildren()];
	bodyType->m_phJointTypes = rage_new datOwner<phJointType>[GetPhysics(currentLOD)->GetNumChildren()-1];

	if (GetPhysics(currentLOD)->GetNumChildren() > 0)
	{
		SetHasAnyArticulatedParts();
	}

	for (int jointIndex = 0; jointIndex < GetPhysics(currentLOD)->GetNumChildren() - 1; ++jointIndex)
	{
		const fragType::JointParams& param = m_JointParams[jointIndex];

		if (param.dof == 1)
		{
			bodyType->m_phJointTypes[jointIndex] = rage_new phJoint1DofType();
			bodyType->GetJointTypes(jointIndex)->SetJointType(phJoint::JNT_1DOF);
		}
		else
		{
			bodyType->m_phJointTypes[jointIndex] = rage_new phJoint3DofType();
			bodyType->GetJointTypes(jointIndex)->SetJointType(phJoint::JNT_3DOF);
		}
	}

	sysMemStartTemp();

	// Create a temporary AB instance for the convenience of using current functions to initialize the type data
	phArticulatedBody *body = rage_new phArticulatedBody(bodyType);

	phArticulatedBodyPart* rootPart = rage_new phArticulatedBodyPart();
	body->AddRoot(*rootPart);
	body->SetMassAndAngInertia(0, GetPhysics(currentLOD)->GetChild(0)->GetUndamagedMass(), GetPhysics(currentLOD)->GetUndamagedAngInertia(0));
	bodyType->m_ResourcedAngInertiaXYZmassW[0] = Vec4V(VECTOR3_TO_VEC3V(GetPhysics(currentLOD)->GetUndamagedAngInertia(0)), ScalarVFromF32(GetPhysics(currentLOD)->GetChild(0)->GetUndamagedMass()));
	const Matrix34& boundMatrix = RCC_MATRIX34(GetPhysics(currentLOD)->GetCompositeBounds()->GetCurrentMatrix(0));
	Matrix34 matrix;
	matrix.Transpose(boundMatrix);
	matrix.d = boundMatrix.d;
	rootPart->SetMatrix(matrix);

	for (int jointIndex = 0; jointIndex < GetPhysics(currentLOD)->GetNumChildren() - 1; ++jointIndex)
	{
		phArticulatedBodyPart* part = rage_new phArticulatedBodyPart();
		bodyType->m_ResourcedAngInertiaXYZmassW[jointIndex + 1] = Vec4V(VECTOR3_TO_VEC3V(GetPhysics(currentLOD)->GetUndamagedAngInertia(jointIndex + 1)), ScalarVFromF32(GetPhysics(currentLOD)->GetChild(jointIndex + 1)->GetUndamagedMass()));
		const Matrix34& boundMatrix = RCC_MATRIX34(GetPhysics(currentLOD)->GetCompositeBounds()->GetCurrentMatrix(jointIndex + 1));
		matrix.Transpose(boundMatrix);
		matrix.d = boundMatrix.d;
		part->SetMatrix(matrix);

		const fragType::JointParams& param = m_JointParams[jointIndex];
		Vec3V worldAxisPos = Transform(RCC_MAT34V(boundMatrix),param.axisPos);
		Vec3V worldAxisDir = Transform3x3(RCC_MAT34V(boundMatrix),param.fixedAxisDir);

		phJointType *jointType = bodyType->GetJointTypes(jointIndex);

		// Set default stiffness
		jointType->m_DefaultStiffness = param.stiffness;

		if (jointType->m_JointType == phJoint::JNT_1DOF)
		{
			phJoint1Dof* joint = rage_new phJoint1Dof(static_cast<phJoint1DofType*>(jointType));
			body->AddChild(param.parent, *joint, *part);
			joint->SetAxis(body, 
				RCC_VECTOR3(worldAxisPos),
				RCC_VECTOR3(worldAxisDir),
				param.limit1min,
				param.limit1max);
			
		}
		else if (jointType->m_JointType == phJoint::JNT_3DOF)
		{
			float midLean1Limit = 0.5f*(param.limit1max-param.limit1min);
			float midLean1 =      0.5f*(param.limit1max+param.limit1min);
			float midLean2Limit = 0.5f*(param.limit2max-param.limit2min);
			float midLean2 =      0.5f*(param.limit2max+param.limit2min);
			float midTwistLimit = 0.5f*(param.limit3max-param.limit3min);
			float midTwist =      0.5f*(param.limit3max+param.limit3min);

			float tsInX = 0.0f;
			float tsInY = tanf(midLean1/4.f);
			float tsInZ = tanf(midLean2/4.f);

			float b = 2/(1 + tsInY*tsInY + tsInZ*tsInZ);
			float c = 2/(1 + tsInX*tsInX);

			rage::Quaternion quatIn;
			quatIn.x = (b-1)*(c-1);
			quatIn.y = tsInX*(b-1)*c;
			quatIn.z = b*(c*tsInX*tsInY+(c-1)*tsInZ);
			quatIn.w = b*(c*tsInX*tsInZ-(c-1)*tsInY);

			rage::Quaternion quaternion(quatIn.z, quatIn.w, quatIn.y, quatIn.x);
			rage::Quaternion quatChild;
			quatChild.FromRotation(rage::Vector3(0,0,1), midTwist);

			// Here we negate the leanDir due to data from xml being opposite of that expected.
			Vec3V negLeanDir(param.fixedLeanDir);
			negLeanDir = Negate(negLeanDir);
			Vec3V worldLeanDir = Transform3x3(RCC_MAT34V(boundMatrix),negLeanDir);

			// Get the lean axis
			Vector3 worldLeanAxis;
			worldLeanAxis.Cross(RCC_VECTOR3(worldLeanDir), RCC_VECTOR3(worldAxisDir));

			phJoint3Dof* joint = rage_new phJoint3Dof(static_cast<phJoint3DofType*>(jointType));
			body->AddChild(param.parent, *joint, *part);

			joint->SetAxis(body,
				RCC_VECTOR3(worldAxisPos),
				RCC_VECTOR3(worldAxisDir),
				worldLeanAxis,
				midLean1Limit,
				midLean2Limit,
				midTwistLimit,
				quaternion,
				quatChild);
#ifdef USE_SOFT_LIMITS
			Assert(param.limit1soft == param.limit2soft && param.limit1soft == param.limit3soft);
			joint->GetCore().SetSoftLimitRatio(param.limit1soft);
#endif
		}

		body->SetMassAndAngInertia(jointIndex + 1, GetPhysics(currentLOD)->GetChild(jointIndex + 1)->GetUndamagedMass(), GetPhysics(currentLOD)->GetUndamagedAngInertia(jointIndex + 1));
	}

	// Set the number of joints.  This usually gets set automatically during resourcing but when loaded without resourcing,
	// like from a tool like viewfragnm, this wasn't getting set previously.
	bodyType->m_NumJoints = static_cast<u8>(bodyType->GetNumBodyParts() - 1);

	// delete the AB instance data
	delete body;

	sysMemEndTemp();
}

bool fragType::LoadPhysicsRigFromXML(int currentLOD, parTreeNode* root)
{	
	// traverse to load bounds and allocate type data
	fragTypeChild* children[255];
	fragTypeGroup* groups[255];		
	LoadBoundsFromXML(currentLOD, 0, root, children, groups);
	GetPhysics(currentLOD)->m_Children = rage_new datOwner<fragTypeChild>[GetPhysics(currentLOD)->GetNumChildren()];
	memcpy(GetPhysics(currentLOD)->m_Children, children, GetPhysics(currentLOD)->GetNumChildren() * sizeof(fragTypeChild*));
	GetPhysics(currentLOD)->m_Groups = rage_new datOwner<fragTypeGroup>[GetPhysics(currentLOD)->GetNumChildGroups()];
	memcpy(GetPhysics(currentLOD)->m_Groups, groups, GetPhysics(currentLOD)->GetNumChildGroups() * sizeof(fragTypeGroup*));

	sysMemStartTemp();
	m_JointParams = rage_new JointParams[GetPhysics(currentLOD)->GetNumChildGroups()];
	sysMemEndTemp();

	GetPhysics(currentLOD)->m_RootGroupCount = GetPhysics(currentLOD)->GetNumChildGroups();

	// traverse to load joints and their limits
	int jointIndex = -1;
	if (!LoadJointsFromXML(currentLOD, 0, root, jointIndex, -1))
	{
		sysMemStartTemp();
		if (m_JointParams)
			delete [] m_JointParams;
		sysMemEndTemp();
		
		return false;
	}

	// wrap-up
	GetPhysics(currentLOD)->GetChild(GetPhysics(currentLOD)->GetNumChildren() - 1)->m_OwnerGroupPointerIndex = (u8)GetPhysics(currentLOD)->GetNumChildren() - 1;
	GetPhysics(currentLOD)->GetGroup(GetPhysics(currentLOD)->GetNumChildren() - 1)->m_ChildIndex = (u8)GetPhysics(currentLOD)->GetNumChildren() - 1;

	return true;
}

void fragType::LoadBound(int currentLOD, parTreeNode* boundNode, fragTypeChild** children, fragTypeGroup** groups)
{
	children[GetPhysics(currentLOD)->GetNumChildren()] = rage_new fragTypeChild;
	fragTypeChild& newChild = *children[GetPhysics(currentLOD)->GetNumChildren()];
	newChild.m_UndamagedEntity = rage_new fragDrawable;

	groups[GetPhysics(currentLOD)->GetNumChildren()] = rage_new fragTypeGroup;
	fragTypeGroup& newGroup = *groups[GetPhysics(currentLOD)->GetNumChildren()];
	newGroup.m_Strength = -1.0f;
	newGroup.m_NumChildGroups = 0;
	newGroup.m_NumChildren = 1;
	newGroup.m_ParentGroupPointerIndex = 0xff;

	// load bound's world matrix
	parTreeNode* worldNode = boundNode->FindChildWithName("worldFrame");
	Assert(worldNode);
	Mat34V matrix(V_ZERO);
	LoadMatrixFromXML_ConvertToLeftHanded(worldNode, matrix);   
	//LoadMatrixFromXML_New(worldNode, matrix);   

	if (strcmp(boundNode->GetChild()->GetSibling()->GetSibling()->GetData(), "Capsule") == 0)
	{
		parTreeNode* nameNode = boundNode->FindChildWithName("name");
		Assert(nameNode);
		parTreeNode* radiusNode = boundNode->FindChildWithName("radius");
		Assert(radiusNode);
		parTreeNode* heightNode = boundNode->FindChildWithName("length");
		Assert(heightNode);
		parTreeNode* massNode = boundNode->FindChildWithName("mass");
		Assert(massNode);

		StringStrip(newGroup.m_DebugName, nameNode->GetData(), FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH);
		float radius = (float)atof(radiusNode->GetData());
		float length = (float)atof(heightNode->GetData());
		float mass = (float)atof(massNode->GetData());

		phBoundCapsule* capsule = rage_new phBoundCapsule();
		capsule->SetCapsuleSize(radius, length);
		newChild.m_UndamagedEntity->SetBound(capsule);
		newChild.SetUndamagedMass(mass);

		// Flip capsules 180 degrees
		Matrix34 fixupMat = RCC_MATRIX34(matrix);
		fixupMat.RotateLocalAxis(PI, 2);
		newChild.GetUndamagedEntity()->SetBoundMatrix(fixupMat);	
	}
	else if (strcmp(boundNode->GetChild()->GetSibling()->GetSibling()->GetData(), "Box") == 0)
	{
		parTreeNode* nameNode = boundNode->FindChildWithName("name");
		Assert(nameNode);
		parTreeNode* heightNode = boundNode->FindChildWithName("height");
		Assert(heightNode);
		parTreeNode* lengthNode = boundNode->FindChildWithName("length");
		Assert(lengthNode);
		parTreeNode* widthNode = boundNode->FindChildWithName("width");
		Assert(widthNode);
		parTreeNode* massNode = boundNode->FindChildWithName("mass");
		Assert(massNode);		

		StringStrip(newGroup.m_DebugName, nameNode->GetData(), FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH);
		float height = (float)atof(heightNode->GetData());
		float length = (float)atof(lengthNode->GetData());
		float width = (float)atof(widthNode->GetData());
		float mass = (float)atof(massNode->GetData());

		phBoundBox* box = rage_new phBoundBox();
		Vector3 size(width, height, length);
		box->SetBoxSize(RCC_VEC3V(size));
		newChild.m_UndamagedEntity->SetBound(box);
		newChild.SetUndamagedMass(mass);

		// Flip box 90 degrees and fix-up extents
		Matrix34 fixupMat = RCC_MATRIX34(matrix);
		fixupMat.RotateLocalAxis(-PI/2, 0);
		newChild.GetUndamagedEntity()->SetBoundMatrix(fixupMat);	
	}
	else
	{
		Errorf("Unrecognized bound type '%s'.", boundNode->GetElement().GetName());
	}

	Assert(GetPhysics(currentLOD)->GetNumChildren() < 255);
	GetPhysics(currentLOD)->m_NumChildren++;
	GetPhysics(currentLOD)->m_NumGroups++;
}

bool fragType::LoadJoint(int currentLOD, parTreeNode* joint, parTreeNode* limit, int &jointIndex, int parentIndex)
{
	// Update indices
	jointIndex++;
	GetPhysics(currentLOD)->GetChild(jointIndex)->m_OwnerGroupPointerIndex = (u8)jointIndex;
	GetPhysics(currentLOD)->GetGroup(jointIndex)->m_ChildIndex = (u8)jointIndex;

	// Setup limit if any
	if (limit)
	{
		JointParams& params = m_JointParams[jointIndex-1];

		params.parent = parentIndex;

		//parTreeNode* limitMatrix = limit->FindChildWithName("localFrame");
		//parTreeNode* limitMatrix = limit->FindChildWithName("worldFixedFrame");
		parTreeNode* limitMatrix = limit->FindChildWithName("worldMovingFrame");

		parTreeNode* positionx = limitMatrix->FindChildWithName("trans_x");
		parTreeNode* positiony = limitMatrix->FindChildWithName("trans_y");
		parTreeNode* positionz = limitMatrix->FindChildWithName("trans_z");
		Vec3V position((float)atof(positionx->GetData()),
							(float)atof(positiony->GetData()),
							(float)atof(positionz->GetData()));

		parTreeNode* axis1x = limitMatrix->FindChildWithName("xAxis_x");
		parTreeNode* axis1y = limitMatrix->FindChildWithName("xAxis_y");
		parTreeNode* axis1z = limitMatrix->FindChildWithName("xAxis_z");
		Vec3V axisDir(0.0f + 1.0f*(float)atof(axis1x->GetData()),
						   0.0f + 1.0f*(float)atof(axis1y->GetData()),
						   0.0f + 1.0f*(float)atof(axis1z->GetData()));

		parTreeNode* orthAxis1X = limitMatrix->FindChildWithName("yAxis_x");
		parTreeNode* orthAxis1Y = limitMatrix->FindChildWithName("yAxis_y");
		parTreeNode* orthAxis1Z = limitMatrix->FindChildWithName("yAxis_z");
		Vec3V leanDir(0.0f + 1.0f*(float)atof(orthAxis1X->GetData()),
						   0.0f + 1.0f*(float)atof(orthAxis1Y->GetData()),
						   0.0f + 1.0f*(float)atof(orthAxis1Z->GetData()));

		//if (jointIndex == 0)
		//{
		//	parTreeNode* positionx = limit->FindChildWithName("axisposition.X");
		//	parTreeNode* positiony = limit->FindChildWithName("axisposition.Y");
		//	parTreeNode* positionz = limit->FindChildWithName("axisposition.Z");
		//	position = Vec3V((float)atof(positionx->GetData()),
		//		(float)atof(positiony->GetData()),
		//		(float)atof(positionz->GetData()));

		//	parTreeNode* axis1x = limit->FindChildWithName("axisdirection.X");
		//	parTreeNode* axis1y = limit->FindChildWithName("axisdirection.Y");
		//	parTreeNode* axis1z = limit->FindChildWithName("axisdirection.Z");
		//	axisDir = Vec3V(0.0f + 1.0f*(float)atof(axis1x->GetData()),
		//		0.0f + 1.0f*(float)atof(axis1y->GetData()),
		//		0.0f + 1.0f*(float)atof(axis1z->GetData()));
		//}

		// Need to multiply limitMatrix by the parent's bound matrix to get the link-space limit matrix
		Mat34V limitMat(V_ZERO);
		Mat34V limitFromBound(V_ZERO);
		char *boneName = joint->FindChildWithName("name")->GetData(); 
		char boundName[128];
		strcpy(boundName, boneName);
		strcat(boundName, "PhysicsBody");

		int groupIndex = GetPhysics(currentLOD)->FindGroupIndexFromName(boundName);
		Mat34V boundMat = RCC_MAT34V(GetPhysics(currentLOD)->GetChild(groupIndex)->GetUndamagedEntity()->GetBoundMatrix());
		LoadMatrixFromXML_New(limitMatrix, limitMat);
		//LoadMatrixFromXML_ConvertToLeftHanded(limitMatrix, limitMat);

		//InvertTransformOrtho(limitMat, limitMat);
		InvertTransformOrtho(boundMat, boundMat);

		//if (jointIndex != 0)
		//{
			position = Transform(boundMat, position);
			axisDir = Transform3x3(boundMat, axisDir);
			leanDir = Transform3x3(boundMat, leanDir);

			params.axisPos = position;
			params.fixedAxisDir = axisDir;
			params.fixedLeanDir = leanDir;
		//}

		Transform(limitFromBound, limitMat, boundMat);

		if (strcmp(limit->GetChild()->GetSibling()->GetSibling()->GetData(), "BallSocket") == 0)
		{
			params.dof = 3;

			// TODO - Decide on a method for setting 3dof joint types.  Will default for original joint types for now.
			// 0 for original-style limit
			// 1 for inclusion-exclusion cones limit

			// Default to off until we devise a way to mark the joints in the XML file - probably need Endorphin support
			params.dof3Type = 0;

			if (params.dof3Type == 1)
			{
				// Get the moving frame offset
				limitMatrix = limit->FindChildWithName("worldMovingFrame");
				axis1x = limitMatrix->FindChildWithName("xAxis_x");
				axis1y = limitMatrix->FindChildWithName("xAxis_y");
				axis1z = limitMatrix->FindChildWithName("xAxis_z");
				axisDir = Vec3V((float)atof(axis1x->GetData()), (float)atof(axis1y->GetData()), (float)atof(axis1z->GetData()));
				orthAxis1X = limitMatrix->FindChildWithName("yAxis_x");
				orthAxis1Y = limitMatrix->FindChildWithName("yAxis_y");
				orthAxis1Z = limitMatrix->FindChildWithName("yAxis_z");
				leanDir = Vec3V((float)atof(orthAxis1X->GetData()), (float)atof(orthAxis1Y->GetData()), (float)atof(orthAxis1Z->GetData()));
				axisDir = Transform3x3(boundMat, axisDir);
				leanDir = Transform3x3(boundMat, leanDir);
				params.movingAxisDir = axisDir;
				params.movingLeanDir = leanDir;
			}

			//if (jointIndex == 0)
			//{
			//	parTreeNode* orthAxis1X = limit->FindChildWithName("leandirection.X");
			//	parTreeNode* orthAxis1Y = limit->FindChildWithName("leandirection.Y");
			//	parTreeNode* orthAxis1Z = limit->FindChildWithName("leandirection.Z");
			//	Vec3V leanDir(0.0f + 1.0f*(float)atof(orthAxis1X->GetData()),
			//		0.0f + 1.0f*(float)atof(orthAxis1Y->GetData()),
			//		0.0f + 1.0f*(float)atof(orthAxis1Z->GetData()));

			//	params.fixedLeanDir = leanDir;
			//}

			// get limit angles
			//parTreeNode* lean1_angle = limit->FindChildWithName("swing1Angle");
			//Assert(lean1_angle);
			//params.limit1max = (float)atof(lean1_angle->GetData());
			//params.limit1min = -params.limit1max;

			//parTreeNode* lean2_angle = limit->FindChildWithName("swing2Angle");
			//Assert(lean2_angle);
			//params.limit2max = (float)atof(lean2_angle->GetData());
			//params.limit2min = -params.limit2max;

			//parTreeNode* twist_angle = limit->FindChildWithName("twistAngle");
			//Assert(twist_angle);
			//params.limit3max = (float)atof(twist_angle->GetData()) / 2.0f;
			//params.limit3min = -params.limit3max;

			//parTreeNode* offset = limit->FindChildWithName("twistOffset");
			//Assert(offset);
			//params.limit3max += (float)atof(offset->GetData());
			//params.limit3min += (float)atof(offset->GetData());

			//if (jointIndex == 0 || jointIndex == 3)
			//{
				parTreeNode* lean1_angle_max = limit->FindChildWithName("maxfirstleanangle");
				parTreeNode* lean1_angle_min = limit->FindChildWithName("minfirstleanangle");
				Assert(lean1_angle_max);
				Assert(lean1_angle_min);
				params.limit1max = (float)atof(lean1_angle_max->GetData());
				params.limit1min = (float)atof(lean1_angle_min->GetData());

				parTreeNode* lean2_angle_max = limit->FindChildWithName("maxsecondleanangle");
				parTreeNode* lean2_angle_min = limit->FindChildWithName("minsecondleanangle");
				Assert(lean2_angle_max);
				Assert(lean2_angle_min);
				params.limit2max = (float)atof(lean2_angle_max->GetData());
				params.limit2min = (float)atof(lean2_angle_min->GetData());

				parTreeNode* twist_angle_max = limit->FindChildWithName("maxtwistangle");
				parTreeNode* twist_angle_min = limit->FindChildWithName("mintwistangle");
				Assert(twist_angle_max);
				Assert(twist_angle_min);
				params.limit3max = (float)atof(twist_angle_max->GetData());
				params.limit3min = (float)atof(twist_angle_min->GetData());
			//}

			params.limit1soft = 1.0f;
			params.limit2soft = 1.0f;
			params.limit3soft = 1.0f;
			params.stiffness = 0.825f;
		}
		else if (strcmp(limit->GetChild()->GetSibling()->GetSibling()->GetData(), "Hinge") == 0)
		{
			params.dof = 1;

			parTreeNode* angle_max = limit->FindChildWithName("maxangle");
			parTreeNode* angle_min = limit->FindChildWithName("minangle");
			Assert(angle_max);
			Assert(angle_min);
			params.limit1max = (float)atof(angle_max->GetData());
			params.limit1min = (float)atof(angle_min->GetData());

			//parTreeNode* angle = limit->FindChildWithName("twistAngle");
			//Assert(angle);
			//params.limit1max = (float)atof(angle->GetData()) / 2.0f;
			//params.limit1min = -params.limit1max;

			//parTreeNode* offset = limit->FindChildWithName("twistOffset");
			//Assert(offset);
			//params.limit1max += (float)atof(offset->GetData());
			//params.limit1min += (float)atof(offset->GetData());

			params.limit1soft = 1.0f;
			params.stiffness = 0.825f;
		}
	}

	// Set Bone Index
	char *boneName = joint->FindChildWithName("name")->GetData();
	char boundName[128];
	strcpy(boundName, boneName);
	strcat(boundName, "PhysicsBody");

	int groupIndex = GetPhysics(currentLOD)->FindGroupIndexFromName(boundName);
	Assert(groupIndex != -1);
	if (groupIndex == -1)
	{
		Errorf("Group index is -1.  Unrecognized bound name '%s'.  The skeleton has become incompatible with the physics rig.", boundName);
		return false;
	}

	const crBoneData* boneData = m_CommonDrawable->GetSkeletonData()->FindBoneData(boneName);
	Assert(boneData);
	if (!boneData)
	{
		Errorf("boneData is NULL.  Unrecognized bound name '%s'.  The skeleton has become incompatible with the physics rig.", boundName);
		return false;
	}
	GetPhysics(currentLOD)->GetChild(groupIndex)->m_BoneID = (s16)boneData->GetBoneId();
	//GetPhysics(currentLOD)->GetChild(groupIndex)->m_BoneIndexDeprecated = (s16)boneData->GetIndex();

	// Modify bound matrix from world-bound to bone-bound by transforming by bone-world
	parTreeNode* worldNode = joint->FindChildWithName("worldFrame");
	Assert(worldNode);
	Mat34V boneFromInst(V_IDENTITY);
	LoadMatrixFromXML_New(worldNode, boneFromInst);
	InvertTransformOrtho(boneFromInst, boneFromInst);
	Mat34V instFromBound = RCC_MAT34V(GetPhysics(currentLOD)->GetChild(groupIndex)->GetUndamagedEntity()->GetBoundMatrix());
	Mat34V boneFromBound;
	Transform(boneFromBound, boneFromInst, instFromBound);
	GetPhysics(currentLOD)->GetChild(groupIndex)->GetUndamagedEntity()->SetBoundMatrix(RCC_MATRIX34(boneFromBound));

	return true;
}

bool fragType::LoadJointsFromXML(int currentLOD, parTreeNode* parentJoint, parTreeNode* joint, int &jointIndex, int parentIndex)
{
	// debug
	//char * parent_name = 0;
	//if (parentJoint)
	//	parent_name = parentJoint->GetChild()->GetData();

	//*************
	// setup joint and limit
	//*************
	char * joint_name = joint->GetChild()->GetData();
	//const crBoneData* boneData = m_CommonDrawable->GetSkeletonData()->FindBoneData(joint_name);
	//Assert(boneData);
	//int bIndex = (s16)boneData->GetIndex();
	//joint_name=joint_name;
	char * limit_name = 0;
	parTreeNode* limit = joint;
	bool aLimit = false;
	while (limit->GetSibling()) 
	{
		if (strcmp(limit->GetSibling()->GetElement().GetName(), "PhysicsJointLimit") == 0)
		{
			limit_name = limit->GetSibling()->GetChild()->GetData();

			// shave off after underscore then compare to joint name
			if (strncmp(joint_name, limit_name, strlen(joint_name)) == 0)
			{
				aLimit = true;
				break;
			}
		}
		limit=limit->GetSibling();
	}
	if (!aLimit)
		limit = 0;
	else
		limit = limit->GetSibling();
	if (!LoadJoint(currentLOD, joint, limit, jointIndex, parentIndex))
		return false;


	//*************
	// load joint child
	//*************
	parTreeNode* sibling = joint->GetSibling();
	if (sibling->GetChild())
	{
		if (strcmp(sibling->GetChild()->GetElement().GetName(), "PhysicsJoint") == 0)
		{
			if (!LoadJointsFromXML(currentLOD, joint, sibling->GetChild(), jointIndex, jointIndex))
				return false;
		}
	}

	//*************
	// load joint sibling
	//*************
	sibling = sibling->GetSibling();
	if (sibling)
	{
		if (strcmp(sibling->GetElement().GetName(), "PhysicsJoint") == 0)
		{
			if (!LoadJointsFromXML(currentLOD, parentJoint, sibling, jointIndex, parentIndex))
				return false;
		}
		else
		{
			sibling = sibling->GetSibling();
			while (sibling)
			{
				if (strcmp(sibling->GetElement().GetName(), "PhysicsJoint") == 0)
				{
					if (!LoadJointsFromXML(currentLOD, parentJoint, sibling, jointIndex, parentIndex))
						return false;
				}
				sibling = sibling->GetSibling();
			}
		}
	}

	return true;
}

void fragType::LoadBoundsFromXML(int currentLOD, parTreeNode* parentJoint, parTreeNode* joint, fragTypeChild** children, fragTypeGroup** groups)
{
	Assert(strcmp(joint->GetElement().GetName(), "PhysicsJoint") == 0);

	//*************
	// setup bound
	//*************
	parTreeNode* node = joint->GetSibling()->GetChild();
	while (node->GetSibling() && strcmp(node->GetChild()->GetElement().GetName(), "PhysicsBody") != 0)
		node=node->GetSibling();
	parTreeNode* boundNode = node->GetChild();
	Assert(strcmp(boundNode->GetElement().GetName(), "PhysicsBody") == 0);
	LoadBound(currentLOD, boundNode, children, groups);

	//*************
	// load joint child
	//*************
	parTreeNode* sibling = joint->GetSibling();
	if (sibling->GetChild())
	{
		if (strcmp(sibling->GetChild()->GetElement().GetName(), "PhysicsJoint") == 0)
			LoadBoundsFromXML(currentLOD, joint, sibling->GetChild(), children, groups);
	}

	//*************
	// load joint sibling
	//*************
	sibling = sibling->GetSibling();
	if (sibling)
	{
		if (strcmp(sibling->GetElement().GetName(), "PhysicsJoint") == 0)
			LoadBoundsFromXML(currentLOD, parentJoint, sibling, children, groups);
		else
		{
			sibling = sibling->GetSibling();
			while (sibling)
			{
				if (strcmp(sibling->GetElement().GetName(), "PhysicsJoint") == 0)
					LoadBoundsFromXML(currentLOD, parentJoint, sibling, children, groups);
				sibling = sibling->GetSibling();
			}
		}
	}
}


void fragType::LoadMatrixFromXML_New(const parTreeNode* matrixNode, Mat34V& matrix)
{
	parTreeNode* element = matrixNode->GetChild();

	for (int c = 0; c < 4; c++)
	{
		matrix.SetElemf(0, c, (float)atof(element->GetData()));
		element = element->GetSibling();
		matrix.SetElemf(1, c, (float)atof(element->GetData()));
		element = element->GetSibling();
		matrix.SetElemf(2, c, (float)atof(element->GetData()));
		element = element->GetSibling();
	}
}

void fragType::LoadMatrixFromXML_ConvertToLeftHanded(const parTreeNode* matrixNode, Mat34V& matrix)
{
	parTreeNode* element = matrixNode->GetChild();

	// X goes in as normal
	matrix.SetElemf(0, 0, (float)atof(element->GetData()));
	element = element->GetSibling();
	matrix.SetElemf(1, 0, (float)atof(element->GetData()));			
	element = element->GetSibling();
	matrix.SetElemf(2, 0, (float)atof(element->GetData()));			
	element = element->GetSibling();

	// Y goes to third row and gets negated
	matrix.SetElemf(0, 2, (float)atof(element->GetData()) * -1.0f);
	element = element->GetSibling();
	matrix.SetElemf(1, 2, (float)atof(element->GetData()) * -1.0f);			
	element = element->GetSibling();
	matrix.SetElemf(2, 2, (float)atof(element->GetData()) * -1.0f);			
	element = element->GetSibling();

	// Z goes to second row
	matrix.SetElemf(0, 1, (float)atof(element->GetData()));
	element = element->GetSibling();
	matrix.SetElemf(1, 1, (float)atof(element->GetData()));			
	element = element->GetSibling();
	matrix.SetElemf(2, 1, (float)atof(element->GetData()));			
	element = element->GetSibling();
	
	// TRANS goes in as normal
	matrix.SetElemf(0, 3, (float)atof(element->GetData()));
	element = element->GetSibling();
	matrix.SetElemf(1, 3, (float)atof(element->GetData()));
	element = element->GetSibling();
	matrix.SetElemf(2, 3, (float)atof(element->GetData()));
}

void fragType::LoadMatrixFromXML(const parTreeNode* matrixNode, Mat34V& matrix)
{
	parTreeNode* element = matrixNode->GetChild();

	for (int column = 0; column < 4; column++)
	{
		matrix.SetElemf(0, column, (float)atof(element->GetData()));
		element = element->GetSibling();
		matrix.SetElemf(1, column, (float)atof(element->GetData()));
		element = element->GetSibling();
		matrix.SetElemf(2, column, (float)atof(element->GetData()));
		element = element->GetSibling();
		element = element->GetSibling();
	}
}

int fragType::LoadPartsFromXML(int currentLOD, parTreeNode* partNode, int& jointIndex)
{
	parTreeNode* volumeNameNode = partNode->FindChildWithName("volume_name");
	Assert(volumeNameNode);
	char groupName[FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH];
	StringStrip(groupName, volumeNameNode->GetData(), FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH);

	int groupIndex = GetPhysics(currentLOD)->FindGroupIndexFromName(groupName);
	Assert(groupIndex != -1);

	// Put the child/group in the top position, so that after this is done 
	// the parts are ordered the same way as the joints
	SwapEm(GetPhysics(currentLOD)->m_Groups[groupIndex], GetPhysics(currentLOD)->m_Groups[jointIndex]);
	SwapEm(GetPhysics(currentLOD)->m_Children[groupIndex], GetPhysics(currentLOD)->m_Children[jointIndex]);

	GetPhysics(currentLOD)->GetChild(jointIndex)->m_OwnerGroupPointerIndex = (u8)jointIndex;
	GetPhysics(currentLOD)->GetGroup(jointIndex)->m_ChildIndex = (u8)jointIndex;

	parTreeNode* worldNode = partNode->FindChildWithName("world");
	Assert(worldNode);
	Mat34V matrix;
	LoadMatrixFromXML(worldNode, matrix);
	GetPhysics(currentLOD)->GetChild(jointIndex)->GetUndamagedEntity()->SetBoundMatrix(RCC_MATRIX34(matrix));

	parTreeNode* massNode = partNode->FindChildWithName("mass");
	Assert(massNode);
	GetPhysics(currentLOD)->GetChild(jointIndex)->SetUndamagedMass((float)atof(massNode->GetData()));

	parTreeNode* node = massNode;
	while (node &&
		strcmp(node->GetElement().GetName(), "nm3dof") != 0 &&
		strcmp(node->GetElement().GetName(), "nmHinge") != 0)
	{
		node = node->GetSibling();
	}

	int parentJointIndex = jointIndex;
	++jointIndex;

	while (node &&
		(strcmp(node->GetElement().GetName(), "nm3dof") == 0 ||
		 strcmp(node->GetElement().GetName(), "nmHinge") == 0))
	{
		parTreeNode* jointNode = node;
		parTreeNode* childPartNode = jointNode->FindChildWithName("Part");

		if (childPartNode)
		{
			int childJointIndex = LoadPartsFromXML(currentLOD, childPartNode, jointIndex);

			JointParams& params = m_JointParams[childJointIndex - 1];

			params.parent = parentJointIndex;

			parTreeNode* positionNode = jointNode->FindChildWithName("position1");
			Assert(positionNode);
			parTreeNode* positionx = positionNode->FindChildWithName("x");
			Assert(positionx);
			parTreeNode* positiony = positionNode->FindChildWithName("y");
			Assert(positiony);
			parTreeNode* positionz = positionNode->FindChildWithName("z");
			Assert(positionz);

			Vec3V localPosition((float)atof(positionx->GetData()),
								(float)atof(positiony->GetData()),
								(float)atof(positionz->GetData()));

			params.axisPos = localPosition;

			parTreeNode* orthogonal_axis1 = jointNode->FindChildWithName("orthogonal_axis1");
			Assert(orthogonal_axis1);

			parTreeNode* orthAxis1X = orthogonal_axis1->FindChildWithName("x");
			Assert(orthAxis1X);
			parTreeNode* orthAxis1Y = orthogonal_axis1->FindChildWithName("y");
			Assert(orthAxis1Y);
			parTreeNode* orthAxis1Z = orthogonal_axis1->FindChildWithName("z");
			Assert(orthAxis1Z);

			Vec3V localLeanDir(	(float)atof(orthAxis1X->GetData()),
								(float)atof(orthAxis1Y->GetData()),
								(float)atof(orthAxis1Z->GetData()));
			params.fixedLeanDir = localLeanDir;

			parTreeNode* axis1 = jointNode->FindChildWithName("axis1");
			Assert(axis1);

			parTreeNode* axis1x = axis1->FindChildWithName("x");
			Assert(axis1x);
			parTreeNode* axis1y = axis1->FindChildWithName("y");
			Assert(axis1y);
			parTreeNode* axis1z = axis1->FindChildWithName("z");
			Assert(axis1z);

			Vec3V localAxisDir(	(float)atof(axis1x->GetData()),
				(float)atof(axis1y->GetData()),
								(float)atof(axis1z->GetData()));
			params.fixedAxisDir = localAxisDir;

			if (strcmp(jointNode->GetElement().GetName(), "nm3dof") == 0)
			{
				params.dof = 3;

				parTreeNode* max_lean2_angle = jointNode->FindChildWithName("max_lean2_angle");
				Assert(max_lean2_angle);
				params.limit2max = (float)atof(max_lean2_angle->GetData());

				parTreeNode* min_lean2_angle = jointNode->FindChildWithName("min_lean2_angle");
				Assert(min_lean2_angle);
				params.limit2min = (float)atof(min_lean2_angle->GetData());

				parTreeNode* max_lean1_angle = jointNode->FindChildWithName("max_lean1_angle");
				Assert(max_lean1_angle);
				params.limit1max = (float)atof(max_lean1_angle->GetData());

				parTreeNode* min_lean1_angle = jointNode->FindChildWithName("min_lean1_angle");
				Assert(min_lean1_angle);
				params.limit1min = (float)atof(min_lean1_angle->GetData());

				parTreeNode* min_twist_angle = jointNode->FindChildWithName("min_twist_angle");
				Assert(min_twist_angle);
				params.limit3min = (float)atof(min_twist_angle->GetData());

				parTreeNode* max_twist_angle = jointNode->FindChildWithName("max_twist_angle");
				Assert(max_twist_angle);
				params.limit3max = (float)atof(max_twist_angle->GetData());

				parTreeNode* soft_limit_l1 = jointNode->FindChildWithName("soft_limit_l1");
				Assert(soft_limit_l1);
				params.limit1soft = (float)atof(soft_limit_l1->GetData());

				parTreeNode* soft_limit_l2 = jointNode->FindChildWithName("soft_limit_l2");
				Assert(soft_limit_l2);
				params.limit2soft = (float)atof(soft_limit_l2->GetData());

				parTreeNode* soft_limit_twist = jointNode->FindChildWithName("soft_limit_twist");
				Assert(soft_limit_twist);
				params.limit3soft = (float)atof(soft_limit_twist->GetData());

				parTreeNode* stiffness = jointNode->FindChildWithName("stiffness");
				Assert(stiffness);
				params.stiffness = 0.95f - powf(0.25f, (float)atof(stiffness->GetData()));
			}
			else if (strcmp(jointNode->GetElement().GetName(), "nmHinge") == 0)
			{
				params.dof = 1;

				parTreeNode* min_angle = jointNode->FindChildWithName("min_angle");
				Assert(min_angle);
				params.limit1max = -(float)atof(min_angle->GetData());

				parTreeNode* max_angle = jointNode->FindChildWithName("max_angle");
				Assert(max_angle);
				params.limit1min = -(float)atof(max_angle->GetData());

				parTreeNode* soft_limit = jointNode->FindChildWithName("soft_limit");
				Assert(soft_limit);
				params.limit1soft = (float)atof(soft_limit->GetData());

				parTreeNode* stiffness = jointNode->FindChildWithName("stiffness");
				Assert(stiffness);
				params.stiffness = 0.95f - powf(0.25f, (float)atof(stiffness->GetData()));
			}
		}

		node = node->GetSibling();
	}

	return parentJointIndex;
}

void RemoveLeadingUnderscores(char* buf)
{
	int len = (int)strlen(buf);
	int idx = 0;

	while(idx < len && buf[idx] == '_')
		idx++;

	memmove(&buf[0], &buf[idx], len - idx +1);
}

void fragType::LoadSelfCollisionsFromXML(int currentLOD, parTreeNode* collisionSets)
{
	Assert(strcmp(collisionSets->GetElement().GetName(), "CollisionSets") == 0);

	// Allocate an array to use as a lookup matrix where the groupIndices are on the 
	// horizontal and vertical axis.  Later we will only look at half of this matrix,
	// pairs where column > row, since the other half is a duplicate.
	const u8  numGrps     = GetPhysics(currentLOD)->GetNumChildGroups();
	const int numGrpsSqrd = numGrps * numGrps;
	u8* pairMatrix        = Alloca(u8, numGrpsSqrd);
	memset(pairMatrix, 1, numGrpsSqrd * sizeof(u8));
	int numCollidingPairs = (numGrpsSqrd - numGrps) / 2;

	parTreeNode* curCollSetNode = NULL;

	// Iterate over the CollisionSet nodes in the file converting the string names to group indices,
	// and filling in the lookup matrix.  Also keep track of the number of elements cleared so we will 
	// know how many pairs there are total later for allocating the self collision arrays.
	while( (curCollSetNode = collisionSets->FindChildWithName("CollisionSet", curCollSetNode)) != NULL )
	{
		sysMemStartTemp();
		atArray<int> curCollSet;
		sysMemEndTemp();
		parTreeNode* curItem = NULL;

		// Read in the group names, and convert to indices.
		while( (curItem = curCollSetNode->FindChildWithName("item", curItem)) != NULL )
		{
			const char* data = curItem->GetData();
			char groupName[FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH];
			StringStrip(groupName, data, FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH);
			int groupIndex = GetPhysics(currentLOD)->FindGroupIndexFromName(groupName);
			Assert(groupIndex != -1);
			sysMemStartTemp();
			curCollSet.PushAndGrow(groupIndex);
			sysMemEndTemp();
		}

		// Fill out the lookup matrix.
		for(int a = 0; a < curCollSet.GetCount() - 1; ++a)
		{
			for(int b = a + 1; b < curCollSet.GetCount(); ++b)
			{
				int groupIdxA = curCollSet[a];
				int groupIdxB = curCollSet[b];

				// Here we swap so that we only use the half of the matrix where col > row.
				if(groupIdxA > groupIdxB)
				{
					int tmp   = groupIdxA;
					groupIdxA = groupIdxB;
					groupIdxB = tmp;
				}

				// Here we only decrement the counter if the element has not been cleared 
				// yet so that duplicate pairs in the sets don't throw off our count.
				if(*(pairMatrix + groupIdxA * numGrps + groupIdxB) == 1)
				{
					*(pairMatrix + groupIdxA * numGrps + groupIdxB) = 0;
					numCollidingPairs--;
				}
			}
		}

		sysMemStartTemp();
		curCollSet.Reset();
		sysMemEndTemp();
	}

	// Allocate the self collision pair arrays.
	Assert(numCollidingPairs < 255);
	GetPhysics(currentLOD)->SetNumSelfCollisions((u8)numCollidingPairs);
	GetPhysics(currentLOD)->SetNumSelfCollisions(0);

	// Loop over the half of the matrix with col > row and add the pairs 
	// that are set to the self collision arrays.
	for(int i = 0; i < numGrpsSqrd; ++i)
	{
		int row = i / numGrps;
		int col = i % numGrps;

		if(col > row && pairMatrix[i] == 1)
		{
			int numSelfCollisions = GetPhysics(currentLOD)->m_NumSelfCollisions;
			GetPhysics(currentLOD)->m_NumSelfCollisions++;
			GetPhysics(currentLOD)->SetSelfCollision(numSelfCollisions, (u8)row, (u8)col);
		}
	}
}

} // namespace rage
