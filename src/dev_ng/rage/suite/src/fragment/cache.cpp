//
// fragment/cache.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
// 

#include "cache.h"

// fragment
#include "cacheheap.h"
#include "cachemanager.h"
#include "drawable.h"
#include "eventplayer.h"
#include "eventtypes.h"
#include "manager.h"
#include "tune.h"
#include "type.h"
#include "typechild.h"
#include "typegroup.h"

// RAGE
#include "breakableglass/bgdrawable.h"
#include "breakableglass/breakable.h"
#include "breakableglass/crackstemplate.h"
#include "cranimation/frame.h"
#include "crskeleton/jointdata.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "data/resource.h"
#include "diag/tracker.h"
#include "event/player.h"
#include "event/set.h"
#include "grmodel/geometry.h"
#include "grmodel/shader.h"
#include "physics/colliderdispatch.h"
#include "phbound/support.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "phbound/OptimizedBvh.h"
#include "phcore/phmath.h"
#include "phglass/glass.h"
#include "phglass/glassinstance.h"
#include "physics/constraints.h"
#include "phsolver/contactmgr.h"
#include "physics/levelnew.h"
#include "physics/shapetest.h"
#include "physics/simulator.h"
#include "physics/sleep.h"
#include "profile/profiler.h"
#include "grprofile/ekg.h"  // pfEKGMgr
#include "system/miniheap.h"
#include "system/param.h"
#include "system/typeinfo.h"

#include "fragment/typephysicslod.h"
#include "pharticulated/articulatedbody.h"
#include "pharticulated/prismaticjoint.h"

#include "cloth/environmentcloth.h"
#include "cloth/charactercloth.h"
#include "fragment/typecloth.h"
#include "grrope/grroperender.h"
#include "pheffects/clothverletinst.h"

#include "system/taskwaitthread.h"

PARAM(nocloth, "[fragment] disable cloth from being created");

//#pragma optimize("", off)

const float UNBROKEN_AGE = 0.25f;
const float DEFAULT_JOINT_STIFFNESS = 0.5f;
const float DEFAULT_JOINT_MIN_STIFFNESS = 0.1f;

FRAGMENT_OPTIMISATIONS()

namespace rage {

namespace fragStats
{
	PF_PAGE(FragPage,"Frag");

	PF_GROUP(CacheInitGroup);
	PF_LINK(FragPage, CacheInitGroup);

	PF_TIMER(InitInternal, CacheInitGroup);
	PF_TIMER(SetMass, CacheInitGroup);

	PF_GROUP(InstanceGroup);
	PF_LINK(FragPage, InstanceGroup);
	PF_TIMER(BreakApart, InstanceGroup);
	PF_TIMER(PoseBoundsFromSkeleton, InstanceGroup);
	PF_TIMER(WalkInstances, InstanceGroup);
	PF_TIMER(TimeSlice, InstanceGroup);
}

using namespace fragStats;

u32 fragCacheEntry::sm_AttachmentIncludeFlags = INCLUDE_FLAGS_ALL;
u32 fragCacheEntry::sm_AttachmentTypeFlags = TYPE_FLAGS_ALL;

#ifdef GTA_REPLAY_RAGE
bool fragCacheEntry::sm_SuppressClothCreation = false;
#endif

BANK_ONLY(bool fragCacheEntry::sm_DisableCompositeBvhGeneration = false;)

fragCacheEntry::fragCacheEntry()
	: 
#if ENABLE_FRAGMENT_EVENTS
	m_Flags(PLAY_EVENTS_AT_START)
#else // ENABLE_FRAGMENT_EVENTS
	m_Flags(0)
#endif // ENABLE_FRAGMENT_EVENTS
	, m_BackingInst(NULL)
	, m_CompositeBound(NULL)
	, m_Inst(NULL)
	, m_ShouldUseInstanceToArtColliderOffsetMat(true)
	, m_IncreaseMinStiffnessOnActivation(false)
	, m_BoneIndexToComponentMap(NULL)
	, m_ComponentToBoneIndexMap(NULL)
	, m_InstabilityPreventionFrames(-1)
	, m_InstabilityPreventionInitialized(false)
{
	m_PhysDamp.AddRef(); // Add a reference, since we reference the archetype by virtue of owning it
}

fragCacheEntry::~fragCacheEntry()
{
	if (IsInit())
	{
		UnInit();
	}
	m_PhysDamp.Release(false);
}

#if __PS3
void fragCacheEntry::ReloadDmaPlan()
{
	const fragType* type = m_Inst->GetType();
	Assert(type);
	Assert(IsInit());
	Assert(m_HierInst.articulatedCollider && m_HierInst.articulatedCollider->GetDmaPlan());

	// We allocate all the active fragment instance data from a chunk of memory inside the cache entry, using the miniHeap
	fragMemStartCacheHeap();

	// Remove the old dma plan
	sysDmaPlan *dmaPlan = m_HierInst.articulatedCollider->GetDmaPlan();
	delete dmaPlan;
	
	// Add a new one
	if (m_HierInst.articulatedCollider->IsArticulated())
	{
		dmaPlan = rage_new phArticulatedCollider::DmaPlan;
		dmaPlan->Initialize();
		m_HierInst.articulatedCollider->GenerateCoreDmaPlan(*dmaPlan);
	}
	else
	{
		dmaPlan = rage_new phCollider::DmaPlan;
		dmaPlan->Initialize();
		m_HierInst.articulatedCollider->GenerateDmaPlan(*dmaPlan);
	}

	// Stop special allocator
	fragMemEndCacheHeap();
}
#endif

void fragCacheEntry::RemovePhysicsData()
{
	Assert(m_Inst);
	Assert(m_Inst->IsInLevel());

	if (m_HierInst.body)
	{
		delete m_HierInst.body;
		m_HierInst.body = NULL;
		Assert(m_HierInst.articulatedCollider);
		m_HierInst.articulatedCollider->RemoveFromActiveList();
		delete m_HierInst.articulatedCollider->GetSleep();
		m_HierInst.articulatedCollider->SetSleep(NULL);
		if (!m_HierInst.articulatedCollider->GetOwnsSelfCollisionElements())
		{
			m_HierInst.articulatedCollider->GetSelfCollisionPairsArrayRefA().Reset(false);
			m_HierInst.articulatedCollider->GetSelfCollisionPairsArrayRefB().Reset(false);
		}
		PS3_ONLY(delete m_HierInst.articulatedCollider->GetDmaPlan());
		delete m_HierInst.articulatedCollider;
		m_HierInst.articulatedCollider = NULL;
	}

	if (m_BoneIndexToComponentMap)
	{
		delete[] m_BoneIndexToComponentMap;
		m_BoneIndexToComponentMap = NULL;
	}

	if (m_ComponentToBoneIndexMap)
	{
		delete [] m_ComponentToBoneIndexMap;
		m_ComponentToBoneIndexMap = NULL;
	}

	if (m_HierInst.linkAttachment)
	{
		delete m_HierInst.linkAttachment;
		m_HierInst.linkAttachment = NULL;
	}

	phInstBehavior* behavior = m_HierInst.GetInstBehavior();
	if( behavior )
	{
		PHSIM->RemoveInstBehavior( *behavior );
	}

	UninitEnvCloth();
	UninitAllGlass();

#if ENABLE_FRAGMENT_EVENTS
	for (int eventChildIndex = 0; eventChildIndex < m_NumChildrenWithEvents; ++eventChildIndex)
	{
		// We've got a fragment child ...
		int child = m_ChildrenWithEvents[eventChildIndex];
		if (fragContinuousEventPlayer* player = m_HierInst.continuousPlayers[child])
		{
			player->Stop(1.0f);
			m_HierInst.continuousPlayers[child] = NULL;
		}
	}
	delete [] m_ChildrenWithEvents;
	m_ChildrenWithEvents = NULL;
	m_NumChildrenWithEvents = 0;
#endif // ENABLE_FRAGMENT_EVENTS

	// Revert back to the archetype in the fragType if our fragType is paged in.
	Assert(&m_PhysDamp == m_Inst->GetArchetype());
	m_Inst->SetArchetype(m_Inst->GetTypePhysics()->GetArchetype());
	Assert(m_PhysDamp.GetRefCount() == 1);

	m_PhysDamp.SetBound(NULL);
	phLevelNew::CompositeBoundRemoveBoundsThreadSafe(*m_CompositeBound);
	m_HierInst.compositeBound = NULL;
	while(m_CompositeBound->Release()) {}
	m_CompositeBound = NULL;

	delete [] m_HierInst.groupInsts;
	m_HierInst.groupInsts = NULL;
	m_HierInst.groupSize = 0;
	delete m_HierInst.groupBroken;
	m_HierInst.groupBroken = NULL;

#if ENABLE_FRAGMENT_EVENTS
	delete m_HierInst.childOmitEventUpdate;
	m_HierInst.childOmitEventUpdate = NULL;
	delete [] m_HierInst.continuousPlayers;
	m_HierInst.continuousPlayers = NULL;
#endif // ENABLE_FRAGMENT_EVENTS

	//m_HierInst.parentInst.Reset(); <== better, but need CL5079214 on RDR branch before we have Reset
	m_HierInst.parentInst.SetLevelIndexAndGenerationId(0xffffffff);
	m_HierInst.SetInstBehavior( NULL );
	Assert(m_HierInst.latchedJoints == NULL);
	m_HierInst.rootLatchBroken = false;
	m_HierInst.drawExtraDrawable = -1;
	m_HierInst.glassInfos = NULL;
	m_HierInst.numGlassInfos = 0;
}

void fragCacheEntry::DeleteLink(phArticulatedBodyPart *link)
{
	Assert(m_Inst);
	Assert(m_Inst->IsInLevel());
#if __ASSERT
	const fragType* type = m_Inst->GetType();
	Assert(type != NULL);
#endif

	fragMemStartCacheHeap();
	delete link;
	fragMemEndCacheHeap();
}

void fragCacheEntry::DeleteJoint(phJoint *joint)
{
	Assert(m_Inst);
	Assert(m_Inst->IsInLevel());
#if __ASSERT
	const fragType* type = m_Inst->GetType();
	Assert(type != NULL);
#endif

	fragMemStartCacheHeap();
	delete joint->m_Type;
	delete joint;
	fragMemEndCacheHeap();
}

void fragCacheEntry::SetSkeletonData(const crSkeletonData& skelData)
{
	Assert(m_Inst);
	Assert(m_Inst->IsInLevel());

	if (GetHierInst()->skeleton)
	{
		GetHierInst()->skeleton->SetSkeletonData(skelData);
	}
}

void fragCacheEntry::CopySkeletonData(const crSkeletonData& skelData)
{
	Assert(m_Inst);
	Assert(m_Inst->IsInLevel());

	if (GetHierInst()->skeleton)
	{
		GetHierInst()->skeleton->CopySkeletonData(skelData);
	}
}

void fragCacheEntry::RemoveArticulatedBody()
{
	Assert(m_Inst);
	Assert(m_Inst->IsInLevel());
#if __ASSERT
	const fragType* type = m_Inst->GetType();
	Assert(type != NULL);
#endif

	if (PHLEVEL->IsActive(m_Inst->GetLevelIndex()))
	{
		// deactivate - makes the LOD swap cleaner
		PHSIM->DeactivateObject(m_Inst->GetLevelIndex());
	}

	// We allocate all the active fragment instance data from a chunk of memory inside the cache entry, using the miniHeap
	fragMemStartCacheHeap();

	if (m_HierInst.body)
	{
		delete m_HierInst.body;
		m_HierInst.body = NULL;
		Assert(m_HierInst.articulatedCollider);
		m_HierInst.articulatedCollider->RemoveFromActiveList();
		delete m_HierInst.articulatedCollider->GetSleep();
		m_HierInst.articulatedCollider->SetSleep(NULL);
		if (!m_HierInst.articulatedCollider->GetOwnsSelfCollisionElements())
		{
			m_HierInst.articulatedCollider->GetSelfCollisionPairsArrayRefA().Reset(false);
			m_HierInst.articulatedCollider->GetSelfCollisionPairsArrayRefB().Reset(false);
		}
		PS3_ONLY(delete m_HierInst.articulatedCollider->GetDmaPlan());
		delete m_HierInst.articulatedCollider;
		m_HierInst.articulatedCollider = NULL;
	}

	// Stop special allocator
	fragMemEndCacheHeap();
}

void fragCacheEntry::UpdateSavedVelocityArraySizes(bool zeroVels)
{
	if (m_HierInst.articulatedCollider)
	{		
		int oldSize = m_HierInst.articulatedCollider->m_SavedLinearVelocities.GetCount(); 
		int newSize = m_HierInst.articulatedCollider->GetBody()->GetNumBodyParts(); 

		bool resize = newSize != oldSize;

		if (resize)
		{
			fragMemStartCacheHeap();
		}

		m_HierInst.articulatedCollider->UpdateSavedVelocityArraySizes(zeroVels);

		if (resize)
		{
			fragMemEndCacheHeap();
		}
	}
}

ASSERT_ONLY(extern const char* g_BoundNameAssertContext;)

void fragCacheEntry::CloneBoundParts()
{
	if (!(m_Flags & CLONED_BOUND_PARTS))
	{
		m_Flags |= CLONED_BOUND_PARTS;

		fragMemStartCacheHeap();

		ASSERT_ONLY(g_BoundNameAssertContext = m_Inst->GetType()->GetTuneName());
		m_CompositeBound->CloneParts();
		ASSERT_ONLY(g_BoundNameAssertContext = NULL);

		fragMemEndCacheHeap();
	}
}

void fragCacheEntry::DecloneBoundParts()
{
	if (m_Flags & CLONED_BOUND_PARTS)
	{
		m_Flags &= ~CLONED_BOUND_PARTS;

		// We allocate all the active fragment instance data from a chunk of memory inside the cache entry, using the miniHeap
		fragMemStartCacheHeap();

		const fragPhysicsLOD* physics = m_Inst->GetTypePhysics();
		phLevelNew::CompositeBoundSetActiveBoundsThreadSafe(*m_CompositeBound,*physics->GetCompositeBounds());
		m_CompositeBound->CalculateCompositeExtents(false);
		if(m_CompositeBound->HasBVHStructure())
		{
			if(m_Inst->IsInLevel())
			{
				PHLEVEL->RebuildCompositeBvh(m_Inst->GetLevelIndex());
			}
			else
			{
				m_CompositeBound->UpdateBvh(true);
			}
		}

		fragMemEndCacheHeap();
	}
}

void fragCacheEntry::ReloadPhysicsData()
{
#if __ASSERT
	const fragType* type = m_Inst->GetType();
	Assert(type);
#endif

	Assert(IsInit());

#if ENABLE_PHYSICS_LOCK
	g_GlobalPhysicsLock.WaitAsWriter();
#endif	// ENABLE_PHYSICS_LOCK

	bool isActive = PHLEVEL->IsActive(m_Inst->GetLevelIndex());
	if (isActive)
	{
		// deactivate - makes the LOD swap cleaner
		PHSIM->DeactivateObject(m_Inst->GetLevelIndex());
	}

	// We allocate all the active fragment instance data from a chunk of memory inside the cache entry, using the miniHeap
	fragMemStartCacheHeap();

	// Clear out physics data - leave the skeleton
	RemovePhysicsData();

	// Re-initialize (presumably with a different physics LOD)
	InitBitsets(NULL);
	InitBound(NULL);
	InitComponentAndBoneIndexMaps();
	m_Inst->PoseBoundsFromSkeleton(false, true);
	m_CompositeBound->UpdateLastMatricesFromCurrent();
	InitPhysics(NULL,0xFF, (m_Flags & CLONED_BOUND_PARTS) != 0);
#if __RESOURCECOMPILER
	InitEnvCloth(NULL);
#endif

	// With no source cache entry InitPhysics will just read the composite extents from the type, instead of recomputing them. 
	m_CompositeBound->CalculateCompositeExtents();
	m_CompositeBound->UpdateBvh(true);
	

	InitConstrained();
#if ENABLE_FRAGMENT_EVENTS
	InitEvents();
#endif // ENABLE_FRAGMENT_EVENTS

	// Stop special allocator
	fragMemEndCacheHeap();

#if !__RESOURCECOMPILER
	InitEnvCloth(NULL);
#endif

	phInstBehavior* behavior = m_HierInst.GetInstBehavior();
	if( behavior )
	{
		PHSIM->AddInstBehavior( *behavior );
	}

	if (isActive)
	{
		// reactivate here is object was active at the beginning of this function
		PHSIM->ActivateObject(m_Inst->GetLevelIndex());
	}

#if ENABLE_PHYSICS_LOCK
	g_GlobalPhysicsLock.ReleaseAsWriter();
#endif	// ENABLE_PHYSICS_LOCK

	Assert(IsInit());
}

void fragCacheEntry::SetMass(f32 mass)
{
	PF_FUNC(SetMass);
	// You should never be setting a negative mass and instead of setting a mass of zero you probably just be removing the instance from the level instead.
	Assert(mass > 0.0f);
	Assert(m_Inst);

	//THIS STUFF IS EXPENSIVE!!!
	//all of these use really big hammers to do their job - we should be able to pre-calc more if(when!) these three turn out to be too slow...
	Assert(m_Inst);
	fragHierarchyInst* hierInst = GetHierInst();

	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();

	Assert(m_CompositeBound);

	if (m_HierInst.body || WillBeArticulated()) // Don't move the CG if we're articulated, otherwise we'll shift
	{
	}
	else if (!WillBeConstrained())
	{
		CalcCenterOfGravity();
#if HACK_GTA4	//	KEEP as a HACK_GTA4 change on rage/dev
		// Think the effect of this code is mainly to enforce the RootCgOffset specified in the fragType
		// for non-articulated instances, even when the fragment has broken apart.
		// Only usually has an effect for vehicles in gta, because we rarely set the cg offset for props
		//
		// Think we would want to take this code across to rage\dev, can't see any good reason not to?
		Vector3 cgOffsetTemp = VEC3V_TO_VECTOR3(m_CompositeBound->GetCGOffset());
		float fMassRatio = mass / physicsLOD->GetTotalUndamagedMass();

		Vector3 vecCgDelta = physicsLOD->GetRootCGOffset() - physicsLOD->GetOriginalRootCGOffset();
		cgOffsetTemp += vecCgDelta * fMassRatio;

		m_CompositeBound->SetCGOffset(RCC_VEC3V(cgOffsetTemp));
#endif //HACK_GTA4
	}

	if (hierInst->age < UNBROKEN_AGE)
	{
		Vector3 cgOffset = VEC3V_TO_VECTOR3(m_CompositeBound->GetCGOffset());
		cgOffset.Add(physicsLOD->GetUnbrokenCGOffset());
		m_CompositeBound->SetCGOffset(RCC_VEC3V(cgOffset));
	}

	m_CompositeBound->CalcCenterOfBound();
	m_CompositeBound->CalculateCompositeExtents();

	if (m_Inst->IsInLevel())
	{
		if(m_CompositeBound->HasBVHStructure())
		{
			PHLEVEL->RebuildCompositeBvh(m_Inst->GetLevelIndex());
		}
		if (phCollider* collider = PHSIM->GetCollider(m_Inst))
		{
			collider->SetColliderMatrixFromInstance();
		}
	}
	else
	{
		m_CompositeBound->UpdateBvh(true);
	}

	m_PhysDamp.SetMassOnly(mass);

	float totalMass = 0.0f;
	Vector3 totalAngInertia(VEC3_ZERO);
	Vector3 cgOffset(VEC3V_TO_VECTOR3(m_CompositeBound->GetCGOffset()));
// NOTE: composite bound on the broken piece might have less children than what is says in the physicsLOD ( i got this case with the parasol 01 ) - svetli
	for (int child = 0; child < m_CompositeBound->GetNumBounds() /*physicsLOD->GetNumChildren()*/; ++child)
	{
		if (m_CompositeBound->GetBound(child) != NULL)
		{
			const fragTypeChild *curTypeChild = physicsLOD->GetChild(child);
			int groupIndex = curTypeChild->GetOwnerGroupPointerIndex();
			if (hierInst->groupBroken->IsClear(groupIndex))
			{
				//accumulate some new mass...
				// Only switch to use the damaged mass and angular inertia if we have a damaged entity.  If there's no damaged entity then who knows what
				//   values are in there.
				if (hierInst->groupInsts[groupIndex].IsDamaged() && curTypeChild->GetDamagedEntity() != NULL)
				{
					AddChildMassAndAngInertia(totalMass, totalAngInertia, cgOffset, curTypeChild, child, true);
				}
				else
				{
					AddChildMassAndAngInertia(totalMass, totalAngInertia, cgOffset, curTypeChild, child, false);
				}
			}
		}
	}

	if (totalMass > FLT_MIN)
	{
		m_PhysDamp.SetAngInertia(totalAngInertia);
	}

    // If necessary, propagate our changes into the current collider
    if (GetInst()->IsInLevel())
    {
        if (phCollider* collider = PHSIM->GetCollider(GetInst()))
        {
            // Load the mass and inertial from the archetype into the collider
            collider->InitInertia();
        }
    }
}

f32 fragCacheEntry::GetMass(fragHierarchyInst* hierInst)
{
	f32 mass = 0.0f;

	Assert(m_Inst);

	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();

	for (int child = 0; child < physicsLOD->GetNumChildren(); ++child)
	{
		const fragTypeChild *curTypeChild = physicsLOD->GetChild(child);
		int groupIndex = curTypeChild->GetOwnerGroupPointerIndex();
		if (hierInst->groupBroken->IsClear(groupIndex))
		{
			//accumulate some new mass...
			if (hierInst->groupInsts[groupIndex].IsDamaged() && curTypeChild->GetDamagedEntity() != NULL)
			{
				mass += physicsLOD->GetChild(child)->GetDamagedMass();
			}
			else
			{
				mass += physicsLOD->GetChild(child)->GetUndamagedMass();
			}
		}
	}

	return mass;
}

void fragCacheEntry::CalcCenterOfGravity()
{
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	float* masses = Alloca(float, physicsLOD->GetNumChildren());
	for (u32 childIndex = 0; childIndex < physicsLOD->GetNumChildren(); ++childIndex)
	{
		const fragTypeChild* child  = physicsLOD->GetChild(childIndex);
		int groupIndex = child->GetOwnerGroupPointerIndex();
		if (m_HierInst.groupBroken->IsClear(groupIndex))
		{
			if (m_HierInst.groupInsts[groupIndex].IsDamaged() && child->GetDamagedEntity() != NULL)
			{
				masses[childIndex] = child->GetDamagedMass();
			}
			else
			{
				masses[childIndex] = child->GetUndamagedMass();
			}
		}
	}

	m_CompositeBound->CalcCenterOfGravity(masses);
}

f32 fragCacheEntry::AdjustForLostGroups(const fragInst::ComponentBits& groupsBreaking)
{
	Assert(m_Inst);
	fragHierarchyInst* hierInst = GetHierInst();
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();

	// Apply the breaking bits that we computed using RecursiveGroupLose
	hierInst->groupBroken->Union(groupsBreaking);
	UpdateIsFurtherBreakable();

	//remove any articulated body parts that are not longer needed
	bool linkIsNeeded[phArticulatedBodyType::MAX_NUM_LINKS];
	memset(linkIsNeeded, 0, sizeof(bool) * phArticulatedBodyType::MAX_NUM_LINKS);

	if (m_HierInst.body)
	{
		m_HierInst.body->EnsureVelocitiesFullyPropagated();

		// Iterate through all of the groups and check to see if they're still around.  If they are, record that the link that corresponds to them is still
		//   needed.
		for (int curGroupIndex = physicsLOD->GetNumChildGroups() - 1; curGroupIndex > 0; --curGroupIndex)
		{
			if (hierInst->groupBroken->IsClear(curGroupIndex))
			{
				const fragTypeGroup *curGroup = physicsLOD->GetGroup(curGroupIndex);
				int firstChildIndex = curGroup->GetChildFragmentIndex();
				int curLinkIndex = hierInst->articulatedCollider->GetLinkFromComponent(firstChildIndex);

				linkIsNeeded[curLinkIndex] = true;
			}
		}

		bool reconfiguredBody = false;

		for (int linkIndex = m_HierInst.body->GetNumJoints(); linkIndex > 0; --linkIndex)
		{
			if (!linkIsNeeded[linkIndex])
			{
				m_HierInst.articulatedCollider->RemoveLink(linkIndex);
				m_HierInst.body->RemoveChild(linkIndex);
				UpdateSavedVelocityArraySizes();
#if __ASSERT
				CheckVelocities();
#endif
				reconfiguredBody = true;
			}
		}

		if (reconfiguredBody)
		{
#if __PS3
			m_HierInst.articulatedCollider->RegenerateCoreDmaPlan();
#endif
			m_HierInst.articulatedCollider->FindJointLimitDofs();
		}
	}

	//go null out the composite bounds where children are not valid anymore...
	float mass = 0.0f;

	/*const */fragInst *instance = GetInst();
	Assert(!instance->IsInLevel() || instance->GetArchetype()->GetBound() == m_CompositeBound);

	// Loop over each of the groups and check to see if there's anything that we need to do
	const fragPhysicsLOD *physLOD = instance->GetTypePhysics();
	const int numGroups = physLOD->GetNumChildGroups();
	for(int groupIndex = 0; groupIndex < numGroups; ++groupIndex)
	{
		const fragTypeGroup &typeGroup = *physLOD->GetGroup(groupIndex);
		const int numChildrenInGroup = typeGroup.GetNumChildren();
		const int firstChildIndexInGroup = typeGroup.GetChildFragmentIndex();

		const bool groupIsPresent = !hierInst->groupBroken->IsSet(groupIndex);
		const bool groupJustBrokeOff = groupsBreaking.IsSet(groupIndex);

		for(int childInGroupIndex = 0; childInGroupIndex < numChildrenInGroup; ++childInGroupIndex)
		{
			const int childIndex = childInGroupIndex + firstChildIndexInGroup;

			if(groupJustBrokeOff)
			{
				// This is one of the groups that has newly broken off.  Let's set the components to NULL and clear out any manifolds that might have referred to
				//   those components.
				if (m_Flags & CLONED_BOUND_PARTS)
					fragMemStartCacheHeap();

				phLevelNew::CompositeBoundRemoveBoundThreadSafe(*m_CompositeBound,childIndex);

				if (m_Flags & CLONED_BOUND_PARTS)
					fragMemEndCacheHeap();

				// NOTE: Don't clear manifolds here or do anything like that.  We don't know why this group has been removed (perhaps it's due to breaking) so
				//   let the calling code handle that.
			}

			const fragTypeChild &typeChild = *physLOD->GetChild(childIndex);
			if(groupIsPresent)
			{
				// Whether it just broke off or not, it's still here, so let's count its mass.
				if (hierInst->groupInsts[groupIndex].IsDamaged() && typeChild.GetDamagedEntity() != NULL)
				{
					mass += typeChild.GetDamagedMass();
				}
				else
				{
					mass += typeChild.GetUndamagedMass();
				}
			}
		}
	}

	Assertf( mass > 0.0f, "mass > 0.0f on fragment '%s'", physLOD->GetArchetype()->GetFilename() );

#if ENABLE_FRAGMENT_EVENTS
	// Stop any event players on any of the children
	for (int eventChildIndex = 0; eventChildIndex < m_NumChildrenWithEvents; ++eventChildIndex)
	{
		// We've got a fragment child ...
		int child = m_ChildrenWithEvents[eventChildIndex];
		if (fragContinuousEventPlayer* player = m_HierInst.continuousPlayers[child])
		{
			const fragTypeChild *curChild = physicsLOD->GetChild(child);
			const int curGroupIndex = curChild->GetOwnerGroupPointerIndex();
			if(m_HierInst.groupBroken->IsSet(curGroupIndex))
			{
				player->Stop(1.0f);
				m_HierInst.continuousPlayers[child] = NULL;
			}
		}
	}
#endif // ENABLE_FRAGMENT_EVENTS

	if (m_HierInst.skeleton)
	{
        m_Inst->ZeroSkeletonMatricesByGroup(m_HierInst.skeleton, m_HierInst.damagedSkeleton);
	}

	return mass;
}

class ModelGeomPair
{
public:

	int m_ModelIdx;
	int m_GeomIdx;

};

int ModelGeomPairSort( const ModelGeomPair* a, const ModelGeomPair* b )
{
	return (a->m_ModelIdx - b->m_ModelIdx);
}

void fragCacheEntry::UnInit()
{
	PHINST_SCOPED_DISABLE_VALIDATE_POSITION; // We probably shouldn't be setting weird instance positions here but it's not causing any known issues and is spammy. 

	fragCacheKey removeKey;

	Assert(!PHSIM->IsUpdateRunning());
	Assert(IsInit() || IsStreamingIn());
	Assert(m_Inst);
	SetFoundAttachment(false);

	// Set pointers to articulated body type data to NULL.  This has the effect of removing references and 
	// stops defragmentation from crashing the system.
	if (m_HierInst.body)
	{
		m_HierInst.body->NullOutTypeDataPointers();
	}

	if (m_HierInst.articulatedCollider)
	{
		m_HierInst.articulatedCollider->SetLinkAttachment(NULL);
		m_HierInst.articulatedCollider->RemoveFromActiveList();
	}

	phInstBehavior* behavior = m_HierInst.GetInstBehavior();
	if( behavior )
	{
		PHSIM->RemoveInstBehavior( *behavior );
	}

	UninitEnvCloth();
	UninitAllGlass();

#if ENABLE_FRAGMENT_EVENTS
	for (int eventChildIndex = 0; eventChildIndex < m_NumChildrenWithEvents; ++eventChildIndex)
	{
		// We've got a fragment child ...
		int child = m_ChildrenWithEvents[eventChildIndex];
		if (fragContinuousEventPlayer* player = m_HierInst.continuousPlayers[child])
		{
			player->Stop(1.0f);
			m_HierInst.continuousPlayers[child] = NULL;
		}
	}
#endif // ENABLE_FRAGMENT_EVENTS

	m_BoneIndexToComponentMap = NULL;
	m_ComponentToBoneIndexMap = NULL;

    if (m_HierInst.damagedSkeleton)
    {
        m_HierInst.damagedSkeleton->UnInit();
    }

	if (m_HierInst.skeleton)
    {
	    m_HierInst.skeleton->UnInit();
    }

	m_Inst->SetCacheEntry(NULL);
	if (m_Inst == m_BackingInst)
	{
		m_BackingInst->Remove();
		m_BackingInst->m_Type = NULL;
	}
	else
	{
		if (m_Inst->GetNoRevive() || FRAGMGR->GetGlobalNoRevive())
		{
			if(m_Inst->IsInLevel())
			{
				PHSIM->DeleteObject(m_Inst->GetLevelIndex());
				m_Inst->SetArchetype(NULL);
				m_PhysDamp.SetBound(NULL);
				m_Inst->SetDead(true);
			}
			m_PhysDamp.SetBound(NULL);
		}
		else
		{
			m_Inst->Reset();
			Assertf(m_Inst == NULL || !m_Inst->IsInLevel() || PHSIM->GetCollider(m_Inst) == NULL, "fragCacheEntry::UnInit The inst is not deactivated, and has reference to deleted collider. Game will likely to crash");
			m_Inst->LosingCacheEntry();

			// Revert back to the archetype in the fragType if our fragType is paged in.
			Assert(m_Inst);
			
			const fragType* type = m_Inst->GetType();
			Assert(type != NULL || !m_Inst->IsInLevel());
			m_Inst->SetArchetype(type != NULL ? m_Inst->GetTypePhysics()->GetArchetype() : NULL);
			m_PhysDamp.SetBound(NULL);

			PHLEVEL->UpdateObjectLocationAndRadius(m_Inst->GetLevelIndex(), (Mat34V_Ptr)(NULL));

			m_Inst->LostCacheEntry();
		}
	}

	if (m_Flags & CLONED_BOUND_PARTS)
		fragMemStartCacheHeap();

	phLevelNew::CompositeBoundRemoveBoundsThreadSafe(*m_CompositeBound);

	if (m_Flags & CLONED_BOUND_PARTS)
		fragMemEndCacheHeap();

	m_PhysDamp.SetBound(NULL);

	fragCacheBlock* block = m_Blocks.GetFirst();
	while (block)
	{
		fragCacheBlock* nextBlock = m_Blocks.Remove(block);
		FRAGCACHEALLOCATOR->FreeBlock(block);
		block = nextBlock;
	}
#if ENABLE_FRAGMENT_EVENTS
	m_Flags = PLAY_EVENTS_AT_START;
#else // ENABLE_FRAGMENT_EVENTS
	m_Flags = 0;
#endif // ENABLE_FRAGMENT_EVENTS

	// The line above does the following all at once:
	//SetInit(false);
	//SetStreamingIn(false);

	m_Inst = NULL;

	m_InstabilityPreventionFrames = -1;
	m_InstabilityPreventionInitialized = false;
}

void fragCacheEntry::SetSequenceNumber()
{
	m_SequenceNumber = FRAGCACHEMGR->GetSeqNum();
}

void fragCacheEntry::UpdateIsFurtherBreakable()
{
	int breakableCount = 0;

	int count = m_Inst->GetTypePhysics()->GetNumChildGroups();
	for (int curGroupIndex = 0; curGroupIndex < count; ++curGroupIndex)
	{
		if (m_HierInst.groupBroken->IsClear(curGroupIndex))
		{
			//we can be further broken apart if we have two or more children left attached to us
			if(breakableCount > 0)
			{
				m_Flags |= FURTHER_BREAKABLE;
				return;
			}

			++breakableCount;
		}
	}

	// We didn't find two breakable groups
	m_Flags &= ~FURTHER_BREAKABLE;
}

bool fragCacheEntry::IsUsingBackingInst() const
{
	return m_Inst == m_BackingInst;
}

bool fragCacheEntry::WillBeArticulated() const
{
	Assert(m_Inst);
	const fragType* type = m_Inst->GetType();
	fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();

	if (type->GetARTAssetID() != -1 || physicsLOD->GetBodyType())
	{
		return false;
	}

	// For this cache entry to be articulated it must have at least one joint
	if(fragDrawable* drawable = type->GetCommonDrawable())
	{
		const crSkeletonData* skeletonData = drawable->GetSkeletonData();
		const crJointData* jointData = drawable->GetJointData();

		if (skeletonData && jointData)
		{
			u8 numGroups = physicsLOD->GetNumChildGroups();
			// Start at group 1 since it can't have a joint with its non-existent parent
			for(u8 groupIndex = 1; groupIndex < numGroups; ++groupIndex)
			{
				const fragTypeGroup& group = *physicsLOD->GetGroup(groupIndex);
				u8 parentGroupIndex = group.GetParentGroupPointerIndex();
				// For there to be a joint we need a non-broken parent and child group.
				if(parentGroupIndex != 0xFF && IsGroupUnbroken(groupIndex) && IsGroupUnbroken(parentGroupIndex))
				{
					int boneIndex = m_ComponentToBoneIndexMap[group.GetChildFragmentIndex()];
					bool hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits;
					if(jointData->HasLimits(*skeletonData->GetBoneData(boneIndex), hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits))	
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}

bool fragCacheEntry::WillBeSimpleArticulated() const
{
	Assert(m_Inst);
	const fragType* type = m_Inst->GetType();
	fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();

	if (type->GetARTAssetID() != -1 || physicsLOD->GetBodyType() || physicsLOD->GetMinMoveForce() != 0)
	{
		return false;
	}

	// For this cache entry to be simple articulated it must have at least one joint and all of its joints
	//   must be latched. 
	bool foundOne = false;
	if(fragDrawable* drawable = type->GetCommonDrawable())
	{
		const crSkeletonData* skeletonData = drawable->GetSkeletonData();
		const crJointData* jointData = drawable->GetJointData();

		if (skeletonData && jointData)
		{
			u8 numGroups = physicsLOD->GetNumChildGroups();
			// Start at group 1 since it can't have a joint with its non-existent parent
			for(u8 groupIndex = 1; groupIndex < numGroups; ++groupIndex)
			{
				const fragTypeGroup& group = *physicsLOD->GetGroup(groupIndex);
				u8 parentGroupIndex = group.GetParentGroupPointerIndex();
				// For there to be a joint we need a non-broken parent and child group.
				if(parentGroupIndex != 0xFF && !IsGroupBroken(groupIndex) && !IsGroupBroken(parentGroupIndex))
				{
					int boneIndex = m_ComponentToBoneIndexMap[group.GetChildFragmentIndex()];
					bool hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits;
					if(jointData->HasLimits(*skeletonData->GetBoneData(boneIndex), hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits))	
					{
						// there is a joint on this cache entry
						foundOne = true;

						if(!group.IsInitiallyLatched())
						{
							// there is at least one unlatched joint, this cache entry can't be simple articulated
							return false;
						}
					}
				}
			}
		}
	}

	return foundOne;
}

bool fragCacheEntry::WillBeConstrained() const
{
    const fragType* type = m_Inst->GetType();
    int limitDof;
    Vector3 constrainDir, rotMin, rotMax;
    bool rootLinkConstraint = type->GetRootLinkConstraint(RCC_MATRIX34(m_Inst->GetMatrix()), limitDof, constrainDir, rotMin, rotMax);
    if (rootLinkConstraint && m_HierInst.groupBroken->IsClear(0))
    {
        return true;
    }
    else
    {
        return false;
    }
}

inline void fragCacheEntry::InitBitsets(fragHierarchyInst* hierSrc)
{
	Assert(m_Inst);
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	int damageCount = physicsLOD->GetNumChildGroups();// + type->GetNumRootDamageRegions() - 1;

	// Allocate memory for some parts of our fragHierarchyInst.
	m_HierInst.groupInsts = rage_new fragGroupInst[damageCount];
	m_HierInst.groupSize = damageCount;
	m_HierInst.groupBroken = rage_new atBitSet;
#if ENABLE_FRAGMENT_EVENTS
	m_HierInst.childOmitEventUpdate = rage_new atBitSet;
#endif // ENABLE_FRAGMENT_EVENTS

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Set up the groupBroken, childBroken, and childInsts members of our fragHierarchyInst.
	if (hierSrc)
	{
		// This cache entry is being initialized as a copy of another one, so let's
		//   initialize the instance data from the supplied hierInst ...

		*m_HierInst.groupBroken = *hierSrc->groupBroken;
#if ENABLE_FRAGMENT_EVENTS
		*m_HierInst.childOmitEventUpdate = *hierSrc->childOmitEventUpdate;
#endif // ENABLE_FRAGMENT_EVENTS
		memcpy(m_HierInst.groupInsts, hierSrc->groupInsts, sizeof(fragGroupInst) * damageCount);
        m_HierInst.anyGroupDamaged = hierSrc->anyGroupDamaged;
	}
	else
	{
		// This cache entry is brand new and needs to be initialized from the fragType.

		m_HierInst.groupBroken->Init(physicsLOD->GetNumChildGroups());
#if ENABLE_FRAGMENT_EVENTS
		m_HierInst.childOmitEventUpdate->Init(physicsLOD->GetNumChildren());
#endif // ENABLE_FRAGMENT_EVENTS
        m_HierInst.anyGroupDamaged = false;

		for (int entry = 0; entry < damageCount; ++entry)
		{
			m_HierInst.groupInsts[entry].damageHealth = physicsLOD->GetGroup(entry)->GetDamageHealth();
			m_HierInst.groupInsts[entry].weaponHealth = physicsLOD->GetGroup(entry)->GetWeaponHealth();
		}
	}

	UpdateIsFurtherBreakable();
}

inline void fragCacheEntry::InitBound(fragHierarchyInst* hierSrc)
{
	Assert(m_Inst);
	
	const fragType* type = m_Inst->GetType();
	Assert(type);

	// Get our composite bound set up to use our local set of (currently unset) bound pointers but the original bound's local matrix set.
	phBoundComposite* orgBounds = m_Inst->GetTypePhysics()->GetCompositeBounds();
    if (hierSrc)
    {
        orgBounds = hierSrc->compositeBound;
    }
	m_CompositeBound = rage_new phBoundComposite;
	m_HierInst.compositeBound = m_CompositeBound;
	m_CompositeBound->AddRef(); // Increment the reference, since it's owned by the mini heap

	int maxNumBounds = orgBounds->GetMaxNumBounds();
	int numBounds = orgBounds->GetNumBounds();
	
	m_CompositeBound->Init( maxNumBounds , true);
	m_CompositeBound->SetCGOffset(orgBounds->GetCGOffset());

	// Copy the set of bounds into our phBoundComposite..
	phLevelNew::CompositeBoundSetBoundsThreadSafe(*m_CompositeBound,*orgBounds);

#if HACK_GTA4
	m_CompositeBound->SetNumBounds( numBounds );
#endif

	if (hierSrc)
	{
		m_CompositeBound->CopyMatricesFromComposite(*hierSrc->compositeBound);
	}
	else
	{
		m_CompositeBound->CopyMatricesFromComposite(*m_Inst->GetTypePhysics()->GetCompositeBounds());
	}

	m_CompositeBound->UpdateLastMatricesFromCurrent();


	if( orgBounds->GetTypeAndIncludeFlags() )
	{
		m_CompositeBound->AllocateTypeAndIncludeFlags();
		m_CompositeBound->SetTypeAndIncludeFlags( orgBounds->GetTypeAndIncludeFlags() );
	}
	else
	{
		// This change requires our changes to phBoundComposite as well.
		// With the original code, composite bounds would end up deleting type and include flags
		// that were owned by other bounds (see comments in boundcomposite.h)
		if(type->GetAllocateTypeAndIncludeFlags())
		{
			m_CompositeBound->AllocateTypeAndIncludeFlags();
		}
		if(m_Inst->GetTypePhysics()->GetCompositeBounds()->GetTypeAndIncludeFlags())
		{
			m_CompositeBound->SetTypeAndIncludeFlags(m_Inst->GetTypePhysics()->GetCompositeBounds()->GetTypeAndIncludeFlags());
		}
	}

	// We want this bound to have a valid BVH structure allocated but we don't want to be bothered with building it we're just going to make a change to it (and
	//  thus rebuild it) later on.  So copy the one that we have now and, if we make changes later on then we'll rebuild it at that time.
	BANK_ONLY(if(!sm_DisableCompositeBvhGeneration))
	{
		const phOptimizedBvh *existingBvhStructure = orgBounds->GetBVHStructure();
		if(existingBvhStructure != NULL)
		{
			m_CompositeBound->AllocateAndCopyBvhStructure(*existingBvhStructure);
#if __ASSERT
			// Composites should always have 1 subtree header
			int numSubOrig = existingBvhStructure->GetNumSubtreeHeaders();
			int numUsedSubOrig = existingBvhStructure->GetNumUsedSubtreeHeaders();
			int numSubNew = m_CompositeBound->GetBVHStructure()->GetNumSubtreeHeaders();
			int numUsedSubNew = m_CompositeBound->GetBVHStructure()->GetNumUsedSubtreeHeaders();
			Assertf(numSubOrig==1 && numUsedSubOrig==1 && numSubNew==1 && numUsedSubNew==1, "Composite BVH mismatch on '%s' (%i,%i)->(%i,%i)",GetInst()->GetType()->GetTuneName(),numSubOrig,numUsedSubOrig,numSubNew,numUsedSubNew);
#endif
		}
	}
}

inline void fragCacheEntry::InitSkeleton()
{
	Assert(m_Inst);
	const fragType* type = m_Inst->GetType();
	Assert(type);

	m_HierInst.skeleton = NULL;
	m_HierInst.damagedSkeleton = NULL;

	int numBones = 0;
	crSkeletonData* sd = NULL;
	if(fragDrawable* drawable = type->GetCommonDrawable())
	{
		sd = drawable->GetSkeletonData();

		if (sd)
		{
			numBones = sd->GetNumBones();
		}
	}

	if (numBones > 0)
	{
		// We've got bones! (in our drawable's skeleton data)  We need to create a skeleton for this cache entry for this instance.
		m_HierInst.skeleton = rage_new crSkeleton();
		m_HierInst.skeleton->Init(*sd, &m_Inst->GetMatrix());

        if (type->GetDamagedDrawable())
        {
            m_HierInst.damagedSkeleton = rage_new crSkeleton();
            m_HierInst.damagedSkeleton->Init(*sd, &m_Inst->GetMatrix());
        }
	}
}

inline void fragCacheEntry::InitSkeleton2(fragHierarchyInst* hierSrc, const crSkeleton* skeleton)
{
	// TODO: This function seems unnecessary. We should be able to pass the old skeleton into the first skeleton init. 
	//       In the case of a source skeleton without a source entry we also need to pose the bounds, but that could
	//         be done in InitBounds. 
	if (m_HierInst.skeleton)
	{
		const crSkeletonData* sd = &m_HierInst.skeleton->GetSkeletonData();
		Assert(sd);

		int numBones = sd->GetNumBones();

		if (hierSrc)
		{
			sysMemCpy(m_HierInst.skeleton->GetObjectMtxs(), hierSrc->skeleton->GetObjectMtxs(), numBones*sizeof(Matrix34));
			sysMemCpy(m_HierInst.skeleton->GetLocalMtxs(), hierSrc->skeleton->GetLocalMtxs(), numBones*sizeof(Matrix34));

			if (m_HierInst.damagedSkeleton)
			{
				sysMemCpy(m_HierInst.damagedSkeleton->GetObjectMtxs(), hierSrc->damagedSkeleton->GetObjectMtxs(), numBones*sizeof(Matrix34));
				sysMemCpy(m_HierInst.damagedSkeleton->GetLocalMtxs(), hierSrc->damagedSkeleton->GetLocalMtxs(), numBones*sizeof(Matrix34));
			}
		}
		else if (skeleton)
		{
			// We're initializing from a passed-in skeleton.  First copy the skeleton
			memcpy(m_HierInst.skeleton->GetObjectMtxs(), skeleton->GetObjectMtxs(), numBones * sizeof(Matrix34));
			Assert(skeleton != m_HierInst.skeleton);

            if (m_HierInst.damagedSkeleton)
            {
                memcpy(m_HierInst.damagedSkeleton->GetObjectMtxs(), skeleton->GetObjectMtxs(), numBones * sizeof(Matrix34));
            }

			const bool onlyAdjustForInternalMotion = false;
			m_Inst->PoseBoundsFromSkeleton(onlyAdjustForInternalMotion, true);
			m_CompositeBound->UpdateLastMatricesFromCurrent();
		}
	}
}

inline void fragCacheEntry::InitPhysics(fragHierarchyInst* hierSrc, u16 groupIndex, bool cloneParts)
{
	//set up the physics archetype...
	Assert(m_Inst);
	
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();

	// m_PhysDamp is not created from scratch each time a cache entry is initialized.
	m_PhysDamp.SetBound(m_CompositeBound);
	m_PhysDamp.CopyData(physicsLOD->GetArchetype());

#if __DEV
	// Make a copy of the type name because the type might move in memory during defragmentation
	const char* filenameFromType = physicsLOD->GetArchetype()->GetFilename();
	size_t filenameLength = strlen(filenameFromType) + 1;
	char* filenameCopy = rage_new char[filenameLength];
	strncpy(filenameCopy, filenameFromType, filenameLength);
	m_PhysDamp.SetFilename(filenameCopy);
#endif // __DEV

#if HACK_GTA4
	// Let's give this thing some damping.
	physicsLOD->ApplyDamping(&m_PhysDamp);
#endif

	m_HierInst.body = NULL;
	m_HierInst.articulatedCollider = NULL;
#if ENABLE_FRAGMENT_EVENTS
	m_HierInst.continuousPlayers = NULL;
#endif // ENABLE_FRAGMENT_EVENTS
	m_HierInst.SetInstBehavior( NULL );
    m_HierInst.animation = NULL;
	m_HierInst.linkAttachment = NULL;

#if ENABLE_FRAGMENT_EVENTS
	m_HierInst.continuousPlayers = rage_new fragContinuousEventPlayer*[physicsLOD->GetNumChildren()];
	sysMemSet(m_HierInst.continuousPlayers, 0, sizeof(fragContinuousEventPlayer*) * physicsLOD->GetNumChildren());
#endif // ENABLE_FRAGMENT_EVENTS

	if(hierSrc)
	{
		float mass = 0.0f;

		fragInst::ComponentBits brokenGroups;
		physicsLOD->GetGroupsNotAbove((u8)groupIndex,brokenGroups);
		mass = AdjustForLostGroups(brokenGroups);

		if (mass == 0.0f)
		{
			//if this happened, then we lost no groups from the source - so, just grab the mass as it stands now...
			mass = GetMass(&m_HierInst);
		}	

		if (cloneParts)
		{
			m_Flags |= CLONED_BOUND_PARTS;

			m_CompositeBound->CloneParts();
		}
		else
		{
			// Multiple places will override bounds on composites without cloning all bounds. We cannot let those stray bounds be referenced by
			//   broken off fragments since they'll be destroyed once the parent is destroyed. 
			for(int boundIndex = 0; boundIndex < m_CompositeBound->GetNumBounds(); ++boundIndex)
			{
				if(m_CompositeBound->GetBound(boundIndex) != NULL && m_CompositeBound->GetBound(boundIndex) != physicsLOD->GetCompositeBounds()->GetBound(boundIndex))
				{
					m_CompositeBound->SetBound(boundIndex, m_CompositeBound->GetBound(boundIndex)->Clone());
				}
			}
		}

		Assert(mass > 0.0f);
		SetMass(mass);
	}
	else
	{
		if (cloneParts)
		{
			m_Flags |= CLONED_BOUND_PARTS;

			m_CompositeBound->CloneParts();
		}

		// The call to SetMass in this function was setting up the composite extents for this bound. The extents
		//   should just be copied in InitBounds, but entries with a source don't know about broken bounds yet,
		//   since they're lost in InitPhysics. 
		// Maybe we should merge InitPhysics and InitBounds?
		m_CompositeBound->CopyCompositeExtents(*physicsLOD->GetCompositeBounds());		
	}

	// Wait until here to swap out the archetype pointer, because it's fully formed at this point
	sysMemStartTemp(); // This may delete some stuff in main memory, so activate the main game heap
	m_Inst->SetArchetype(&m_PhysDamp);
	sysMemEndTemp();

	if (m_Inst->IsInLevel())
	{
		// Let's inform the physics level so that it can ensure that the object is placed properly.
		PHLEVEL->UpdateObjectLocationAndRadius(m_Inst->GetLevelIndex(), (Mat34V_Ptr)(NULL));
	}
}


void fragCacheEntry::InitEnvCloth(fragHierarchyInst* hierSrc)
{
#if !__GAMETOOL
	Assert(m_Inst);
	const fragType* type = m_Inst->GetType();
	Assert(type);
	
	if (!hierSrc && type->GetNumEnvCloths() > 0 && !PARAM_nocloth.Get() 
#ifdef GTA_REPLAY_RAGE
	&& sm_SuppressClothCreation	== false
#endif
	)
	{
		Assert( m_HierInst.envCloth == NULL );

		RAGE_TRACK(EnvCloth);

		const environmentCloth* pEnvClothType = &type->GetTypeEnvCloth(0)->m_Cloth;
		const bool bActivateOnHit = ShouldActivateOnHit();
		environmentCloth* pEnvClothInst = FRAGMGR->InstanceEnvironmentCloth( m_Inst->GetMatrix(), pEnvClothType, bActivateOnHit );
		if( pEnvClothInst )
		{
			phClothVerletBehavior* pClothBehavior = (phClothVerletBehavior*)pEnvClothInst->GetBehavior();
			Assert( pClothBehavior );
			pClothBehavior->SetInstance( *m_Inst );
			if( !pClothBehavior->IsMotionSeparated() )
			{
				m_HierInst.SetInstBehavior( pClothBehavior );
			}
			m_HierInst.envCloth = pEnvClothInst;
		}
	}
#else
	(void)hierSrc;
#endif
}

void fragCacheEntry::UninitEnvCloth()
{
	if( !m_HierInst.envCloth )
		return;

	Assert( m_Inst );
	FRAGMGR->RemoveEnvironmentCloth( m_HierInst.envCloth, m_Inst->m_Type );
	m_HierInst.envCloth = NULL;
}


PARAM(nofragarticulation,"[fragment] Don't allow fragment objects to become articulated");


void fragCacheEntry::AddChildMassAndAngInertia (float& mass, Vector3& angInertia, Vector3& cgOffset, const fragTypeChild* child, int childIndex, bool damaged)
{
	Assert(m_Inst);
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	
	float childMass = damaged ? child->GetDamagedMass() : child->GetUndamagedMass();
	mass += childMass;

	Vector3 childAngInertia(ORIGIN);

	fragDrawable* childEntity = damaged ? child->GetDamagedEntity() : child->GetUndamagedEntity();
	if(childEntity && childEntity->GetBound())
	{
		Vector3 childCgOffset;
		RCC_MATRIX34(m_CompositeBound->GetCurrentMatrix(childIndex)).Transform(VEC3V_TO_VECTOR3(childEntity->GetBound()->GetCGOffset()), childCgOffset);
		childCgOffset.Subtract(cgOffset);
		Vector3 childPosSquared;
		childPosSquared.Multiply(childCgOffset,childCgOffset);

		childAngInertia.Set(childPosSquared.y+childPosSquared.z,childPosSquared.x+childPosSquared.z,childPosSquared.x+childPosSquared.y);
		childAngInertia.Scale(childMass);
	}

	Vector3 childTypeAngInertia = damaged ? physicsLOD->GetDamagedAngInertia(childIndex) : physicsLOD->GetUndamagedAngInertia(childIndex);
	Matrix34 absMatrix = RCC_MATRIX34(m_CompositeBound->GetCurrentMatrix(childIndex));
	absMatrix.Abs();
	absMatrix.Transform3x3(childTypeAngInertia);
	childAngInertia.Add(childTypeAngInertia);
	angInertia.Add(childAngInertia);
}

void fragCacheEntry::ComputeChildMassProperties(u8 childIndex, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass)
{
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	Mat34V instanceFromComponent = m_HierInst.compositeBound->GetCurrentMatrix(childIndex);
	if(IsGroupDamaged(physicsLOD->GetChild(childIndex)->GetOwnerGroupPointerIndex()))
	{
		physicsLOD->ComputeDamagedChildMassProperties(childIndex,instanceFromComponent,centerOfGravity,angularInertia,mass);
	}
	else
	{
		physicsLOD->ComputeUndamagedChildMassProperties(childIndex,instanceFromComponent,centerOfGravity,angularInertia,mass);
	}
}

void fragCacheEntry::GetParentGroupAndBone(int childIndex, const int linkForBone[], int& outParentGroupIndex, int& outParentBoneIndex) const
{
	Assert(m_Inst);
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	
	fragTypeChild* child = physicsLOD->GetChild(childIndex);
	Assert(child);
	int curControllingBoneIndex = m_Inst->GetType()->GetBoneIndexFromID(child->GetBoneID());

	// As we work our way up the tree, parentGroupIndex will track which fragTypeGroup we're currently at.
	outParentGroupIndex = child->GetOwnerGroupPointerIndex();
	int nextParentGroupIndex = physicsLOD->GetGroup(outParentGroupIndex)->GetParentGroupPointerIndex();

	// parentBone will be the bone of the first parent fragTypeChild that we come to that is controlled by a bone different than ours and which bone is also already mapped to a link.
	// We will default this to bone 0 which means that it is controlled by the instance as a whole and no
	outParentBoneIndex = -1;
	if (nextParentGroupIndex != 0xFF)
	{
		do
		{
			// Move up one level
			outParentGroupIndex = nextParentGroupIndex;

			// Let's find out what bone is controlling this group's fragTypeChild.
			int groupChild = physicsLOD->GetGroup(outParentGroupIndex)->GetChildFragmentIndex();
			int groupChildBone = m_Inst->GetType()->GetBoneIndexFromID(physicsLOD->GetChild(groupChild)->GetBoneID());

			// Is this bone both different from ours AND already mapping to a link?
			if (groupChildBone != curControllingBoneIndex && linkForBone[groupChildBone] != -1)
			{
				outParentBoneIndex = groupChildBone;
			}
			else
			{
				nextParentGroupIndex = physicsLOD->GetGroup(nextParentGroupIndex)->GetParentGroupPointerIndex();
			}
		}
		while (outParentBoneIndex == -1 &&
			outParentGroupIndex != 0 &&
			outParentGroupIndex != 0xFF &&
			nextParentGroupIndex != 0xFF &&
			m_HierInst.groupBroken->IsClear(nextParentGroupIndex));
	}

	if (outParentBoneIndex == -1)
	{
		outParentBoneIndex = 0;
	}
}


void fragCacheEntry::SetLinkAttachmentRecurseChildren(const fragTypeGroup& group, const Matrix34& linkAttachment)
{
	Assert(m_Inst);
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	const phBoundComposite* compositeBound = physicsLOD->GetCompositeBounds();
	

    // Go through all the children of this group, and make the passed-in matrix the link attachment of the lot

	phArticulatedCollider* articulatedCollider = m_HierInst.articulatedCollider;

	if (m_HierInst.linkAttachment == NULL)
	{
		m_HierInst.linkAttachment = pgArray<Matrix34>::Create(physicsLOD->GetNumChildren());
		memcpy(m_HierInst.linkAttachment->GetElements(), physicsLOD->GetLinkAttachmentMatrices(), sizeof(Matrix34) * physicsLOD->GetNumChildren());
		articulatedCollider->SetLinkAttachment(m_HierInst.linkAttachment);
	}

	for (int childIndex = 0; childIndex < group.GetNumChildren(); ++childIndex)
	{
		Matrix34& linkMatrix = m_HierInst.linkAttachment->GetElement(childIndex + group.GetChildFragmentIndex());
		linkMatrix = RCC_MATRIX34(compositeBound->GetCurrentMatrix(childIndex + group.GetChildFragmentIndex()));
		linkMatrix.DotTranspose(linkAttachment);
	}

	for (int groupIndex = 0; groupIndex < group.GetNumChildGroups(); ++groupIndex)
	{
		SetLinkAttachmentRecurseChildren(*physicsLOD->GetGroup(groupIndex + group.GetChildGroupsPointersIndex()), linkAttachment);
	}
}


void fragCacheEntry::ResetLinkAttachmentRecurseChildren(const fragTypeGroup& group)
{
	Assert(m_Inst);
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	
	// Go through all the children of this group, and set their link attachments back to default
	phArticulatedCollider* articulatedCollider = m_HierInst.articulatedCollider;

	if (m_HierInst.linkAttachment == NULL)
	{
		m_HierInst.linkAttachment = pgArray<Matrix34>::Create(physicsLOD->GetNumChildren());
		memcpy(m_HierInst.linkAttachment->GetElements(), physicsLOD->GetLinkAttachmentMatrices(), sizeof(Matrix34) * physicsLOD->GetNumChildren());
		articulatedCollider->SetLinkAttachment(m_HierInst.linkAttachment);
	}

	for (int childIndex = 0; childIndex < group.GetNumChildren(); ++childIndex)
	{
		Matrix34& linkMatrix = m_HierInst.linkAttachment->GetElement(childIndex + group.GetChildFragmentIndex());

		linkMatrix = physicsLOD->GetLinkAttachmentMatrices()[childIndex + group.GetChildFragmentIndex()];
	}

	for (int groupIndex = 0; groupIndex < group.GetNumChildGroups(); ++groupIndex)
	{
		ResetLinkAttachmentRecurseChildren(*physicsLOD->GetGroup(groupIndex + group.GetChildGroupsPointersIndex()));
	}
}

void fragCacheEntry::TransformJoint(Mat33V_InOut linkFromJointRotation, Vec3V_InOut linkToJoint, Mat34V_In linkFromOldLink)
{
	// Get the old link-from-joint matrix (joint orientations are transposed)
	Mat34V oldLinkFromJoint;
	Transpose(oldLinkFromJoint.GetMat33Ref(), linkFromJointRotation);
	oldLinkFromJoint.SetCol3(linkToJoint);

	// Transform the joint matrix into the new link's space
	Mat34V linkFromJoint;
	Transform(linkFromJoint, linkFromOldLink, oldLinkFromJoint);

	// Tranpose the final matrix so the user doesn't have to
	Transpose(linkFromJointRotation, linkFromJoint.GetMat33ConstRef());
	linkToJoint = linkFromJoint.GetCol3();
}

void fragCacheEntry::RecomputeLink(u8 groupInLinkIndex)
{
	fragAssertf(!IsGroupBroken(groupInLinkIndex), "fragCacheEntry::RecomputeLink - given group (%i) is broken on this cache entry.", groupInLinkIndex);
	// Store some local variables
	phArticulatedCollider* articulatedCollider = m_HierInst.articulatedCollider;
	phArticulatedBody* body = m_HierInst.body;
	phBoundComposite* compositeBound = m_HierInst.compositeBound;
	fragAssertf(articulatedCollider && body, "fragCacheEntry::RecomputeLink requires an articulated collider and body.");
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	const u8 numGroups = physicsLOD->GetNumChildGroups();
	const u8 numChildren = physicsLOD->GetNumChildren();
	u8 childInLinkIndex = physicsLOD->GetGroup(groupInLinkIndex)->GetChildFragmentIndex();
	u8 linkIndex = (u8)articulatedCollider->GetLinkFromComponent(childInLinkIndex);
	fragAssertf(linkIndex < body->GetNumBodyParts(), "fragCacheEntry::RecomputeLink - given group (%i) is mapped to non-existant link (%i).", groupInLinkIndex, linkIndex);

	// Get the link's center of gravity, angular inertia, and total mass
	// Store the child mass information for the second pass
	Vec3V linkCenterOfGravity(V_ZERO);
	Vec3V linkAngularInertia(V_ZERO);
	ScalarV linkMass(V_ZERO);
	Vec3V* childCenterOfGravity = Alloca(Vec3V,numChildren);
	Vec3V* childAngularInertia = Alloca(Vec3V,numChildren);
	ScalarV* childMass = Alloca(ScalarV,numChildren);
	for(u8 groupIndex = 0; groupIndex < numGroups; ++groupIndex)
	{
		const fragTypeGroup& group = *physicsLOD->GetGroup(groupIndex);
		u8 firstChildIndex = group.GetChildFragmentIndex();
		if(!IsGroupBroken(groupIndex) && (u8)articulatedCollider->GetLinkFromComponent(firstChildIndex) == linkIndex)
		{
			u8 lastChildIndex = firstChildIndex + group.GetNumChildren();
			for(u8 childIndex = firstChildIndex; childIndex < lastChildIndex; ++childIndex)
			{
				ComputeChildMassProperties(childIndex, childCenterOfGravity[childIndex], childAngularInertia[childIndex], childMass[childIndex]);
				linkCenterOfGravity = AddScaled(linkCenterOfGravity, childCenterOfGravity[childIndex], childMass[childIndex]);
				linkAngularInertia = Add(linkAngularInertia, childAngularInertia[childIndex]);
				linkMass = Add(linkMass, childMass[childIndex]);
			}
		}
	}
	linkCenterOfGravity = InvScaleSafe(linkCenterOfGravity, linkMass, Vec3V(V_ZERO));

	// If we're modifying the root link and the root group still exists, make sure to add in the user defined offset
	if(linkIndex == 0 && !IsGroupBroken(0))
	{
		linkCenterOfGravity = Add(linkCenterOfGravity, Subtract(VECTOR3_TO_VEC3V(physicsLOD->GetRootCGOffset()), VECTOR3_TO_VEC3V(physicsLOD->GetOriginalRootCGOffset())));
	}

	// We need to update the link attachment matrices. If the hierInst doesn't own any, allocate and copy from the type
	if(m_HierInst.linkAttachment == NULL)
	{
		m_HierInst.linkAttachment = pgArray<Matrix34>::Create(numChildren);
		sysMemCpy(m_HierInst.linkAttachment->GetElements(), physicsLOD->GetLinkAttachmentMatrices(), sizeof(Matrix34)*numChildren);
		articulatedCollider->SetLinkAttachment(m_HierInst.linkAttachment);
	}

	phArticulatedBodyPart& link = body->GetLink(linkIndex);

	// Store the old world-from-link matrix
	Mat34V worldFromOldLink;
	Transpose3x3(worldFromOldLink, link.GetMatrix());
	worldFromOldLink.SetCol3(Add(VECTOR3_TO_VEC3V(link.GetPosition()), articulatedCollider->GetPosition()));

	// Compute the link-from-instance and link-from-world matrices
	Mat34V_ConstRef worldFromInstance = m_Inst->GetMatrix();
	Vec3V linkPositionWorld = Transform(worldFromInstance, linkCenterOfGravity);
	Mat34V linkFromWorld;
	Mat34V linkFromInstance;
	if(linkIndex == 0)
	{
		// If this is the root link matrix, align it to the instance matrix
		Transpose3x3(linkFromWorld, worldFromInstance);
		linkFromWorld.SetCol3(Transform3x3(linkFromWorld,Negate(linkPositionWorld)));
		linkFromInstance.SetIdentity3x3();
		linkFromInstance.SetCol3(Negate(UnTransformOrtho(worldFromInstance,linkPositionWorld)));
	}
	else
	{
		// We don't really care what the link rotation is if it's not the root, just keep the old one
		linkFromWorld.Set3x3(link.GetMatrix().GetMat33ConstRef());
		linkFromWorld.SetCol3(Transform3x3(linkFromWorld,Negate(linkPositionWorld)));
		Transform(linkFromInstance, linkFromWorld, worldFromInstance);
	}

	// Add in the angular inertia gained from the child offset from the link's center of gravity
	// Compute the link attachment matrices
	for(u8 groupIndex = 0; groupIndex < numGroups; ++groupIndex)
	{
		const fragTypeGroup& group = *physicsLOD->GetGroup(groupIndex);
		u8 firstChildIndex = group.GetChildFragmentIndex();
		if(!IsGroupBroken(groupIndex) && (u8)articulatedCollider->GetLinkFromComponent(firstChildIndex) == linkIndex)
		{
			u8 lastChildIndex = firstChildIndex + group.GetNumChildren();
			for(u8 childIndex = firstChildIndex; childIndex < lastChildIndex; ++childIndex)
			{
				// Accumulate angular inertia in the link caused by child translation from the link's CG
				linkAngularInertia = Add(linkAngularInertia, phMathInertia::ComputeTranslationAngularInertia(Subtract(childCenterOfGravity[childIndex], linkCenterOfGravity), childMass[childIndex]));

				// Compute the linkFromChild matrix (link attachment matrix) for this child
				Mat34V_ConstRef instanceFromChild = compositeBound->GetCurrentMatrix(childIndex);
				Mat34V_Ref linkFromChild = RC_MAT34V(m_HierInst.linkAttachment->GetElement(childIndex));
				Transform(linkFromChild, linkFromInstance, instanceFromChild);		
			}
		}
	}

	// Transform the link's angular inertia from instance space into the link's space
	linkAngularInertia = phMathInertia::RotateAngularInertia(linkFromInstance.GetMat33ConstRef(), linkAngularInertia);
	body->SetMassAndAngInertia(linkIndex, linkMass, linkAngularInertia);

	// Shift the link's position
	Vec3V currentRootLinkPositionWorld = articulatedCollider->GetPosition();
	Vec3V currentRootLinktoLinkWorld = Subtract(linkPositionWorld, currentRootLinkPositionWorld);
	if(linkIndex == 0)
	{
		// Set the CG offset of the bound, this represents the root link CG in instance space
		compositeBound->SetCGOffset(Negate(linkFromInstance.GetCol3()));

		// Set the collider matrix from the instance because the collider matrix IS the root 
		//   link matrix, which we just modified.
		link.SetMatrix(MAT34V_TO_MATRIX34(Mat34V(linkFromWorld.GetMat33ConstRef(), Vec3V(V_ZERO))));
		articulatedCollider->SetColliderMatrixFromInstanceRigid();

		// Since the other link positions are relative to the root link, if we're moving
		//   the root link we need to move the other links by the opposite amount. Otherwise
		//   they will just implicitly move with the root link.
		for(u8 linkIndex = 1; linkIndex < body->GetNumBodyParts(); ++linkIndex)
		{
			phArticulatedBodyPart& link = body->GetLink(linkIndex);
			link.SetPosition(VEC3V_TO_VECTOR3(Subtract(VECTOR3_TO_VEC3V(link.GetPosition()), currentRootLinktoLinkWorld)));
		}
	}
	else
	{
		link.SetPosition(RCC_VECTOR3(currentRootLinktoLinkWorld));
	}

	// Transform all of the joint information related to this link from the old link space into the new link space
	// We shouldn't have to rotate the joint orientations if this isn't a root link, but I don't think adding
	//   more branches is worth it.
	Mat34V linkFromOldLink;
	Transform(linkFromOldLink, linkFromWorld, worldFromOldLink);
	for(u8 jointIndex = 0; jointIndex < body->GetNumJoints(); ++jointIndex)
	{
		phJoint& joint = body->GetJoint(jointIndex);
		if(joint.GetParentLinkIndex() == linkIndex)
		{
			joint.m_Type = joint.m_Type->Clone();
			TransformJoint(joint.GetType().m_OrientParent.GetMat33Ref(), joint.GetType().m_OrientParent.GetCol3Ref(), linkFromOldLink);
		}
		else if(joint.GetChildLinkIndex() == linkIndex)
		{
			fragAssertf(linkIndex != 0, "The root link shouldn't be a child in a joint.");
			joint.m_Type = joint.m_Type->Clone();

			// TODO: 1. Put the child orientation matrix on the base type
			//       2. Make the already-3x4 orientation matrix store the joint position, rename it to something more suitable
			switch(joint.GetJointType())
			{
				case phJoint::JNT_1DOF:
				{
					phJoint1DofType& jointType = static_cast<phJoint1DofType&>(joint.GetType());
					TransformJoint(jointType.m_OrientChild.GetMat33Ref(), jointType.m_OrientChild.GetCol3Ref(), linkFromOldLink);
					break;
				}
				case phJoint::JNT_3DOF:
				{
					phJoint3DofType& jointType = static_cast<phJoint3DofType&>(joint.GetType());
					TransformJoint(jointType.m_OrientChild.GetMat33Ref(), jointType.m_OrientChild.GetCol3Ref(), linkFromOldLink);
					break;
				}
				case phJoint::PRISM_JNT:
				{
					phPrismaticJointType& jointType = static_cast<phPrismaticJointType&>(joint.GetType());
					TransformJoint(jointType.m_OrientChild.GetMat33Ref(), jointType.m_OrientChild.GetCol3Ref(), linkFromOldLink);
					break;
				}
			}
		}
	}

	// TODO: Write a CalculateInertias that only updates a single link, this 
	//       call is really slow.
	// Let the body know that the link inertia changed
	body->CalculateInertias();

	// When we do CalculateInertias, we have to re-do FindJointLimitDofs because changing the inertias invalidates the Jacobeans
	articulatedCollider->FindJointLimitDofs();
}

void fragCacheEntry::InitComponentIndexToLinkIndexTable(const u8* groupIndexToLinkIndex)
{
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	for(u8 groupIndex = 0; groupIndex < physicsLOD->GetNumChildGroups(); ++groupIndex)
	{
		if(!IsGroupBroken(groupIndex))
		{
			// Map all the children in this group to this group's link
			const fragTypeGroup& group = *physicsLOD->GetGroup(groupIndex);
			u8 firstChildIndex = group.GetChildFragmentIndex();
			u8 lastChildIndex = firstChildIndex + group.GetNumChildren();
			for(u8 childIndex = firstChildIndex; childIndex < lastChildIndex; ++childIndex)
			{
				m_HierInst.articulatedCollider->SetLinkForComponent(childIndex, groupIndexToLinkIndex[groupIndex]);
			}
		}
	}
}

void fragCacheEntry::InitArticulatedBody(phArticulatedBodyPart& rootLink)
{
	// Let's create our new phArticulatedBody ...
	m_HierInst.body = rage_new phArticulatedBody;
	m_HierInst.body->AddRoot(rootLink);

	// ... and create our new phArticulatedCollider and set it up to use our new body.
	phArticulatedCollider* articulatedCollider = m_HierInst.articulatedCollider;

	if (articulatedCollider)
	{
		articulatedCollider->PreInit(phArticulatedBodyType::MAX_NUM_JOINT_DOFS, m_Inst->GetTypePhysics()->GetCompositeBounds()->GetMaxNumBounds());
	}
	else
	{
		articulatedCollider = rage_new phArticulatedCollider(phArticulatedBodyType::MAX_NUM_JOINT_DOFS, m_Inst->GetTypePhysics()->GetCompositeBounds()->GetMaxNumBounds());
	}
	m_HierInst.articulatedCollider = articulatedCollider;
	m_HierInst.articulatedCollider->SetBody(m_HierInst.body);
	m_HierInst.articulatedCollider->ZeroLinksForComponents(m_Inst->GetTypePhysics()->GetNumChildren());
	m_HierInst.articulatedCollider->SetInstance(m_Inst);
	m_HierInst.articulatedCollider->SetLinkAttachment(m_HierInst.linkAttachment ? m_HierInst.linkAttachment : m_Inst->GetTypePhysics()->GetLinkAttachmentArray());

	m_HierInst.articulatedCollider->SetMaxSpeed(m_Inst->GetTypePhysics()->GetArchetype()->GetMaxSpeed());
	m_HierInst.articulatedCollider->SetMaxAngSpeed(m_Inst->GetTypePhysics()->GetArchetype()->GetMaxAngSpeed());

	// Need to copy linear damping values across from fragType to articulated body
	// because linear damping (only linear) is performed as part of the articulated body
	// update (as well as part of the articulated collider update, where it also does constant and v2 damping)
	//
	// For rage\dev I think this should either be made the default behaviour
	// OR the articulated body shouldn't be doing any damping itself

	// NOTE: these don't get used for anything, but set them for users' sake
	float totalMass = m_Inst->GetTypePhysics()->GetTotalUndamagedMass();
	m_HierInst.articulatedCollider->SetInertia(ScalarVFromF32(totalMass).GetIntrin128(), m_PhysDamp.GetAngInertia());
}

void fragCacheEntry::InitArticulatedRoot(Vec3V_In rootLinkInertia, ScalarV_In rootLinkMass)
{
	const fragType* type = m_Inst->GetType();
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();

	// Create the root link. The root link and instance rotation always line up. Since we don't want to move the instance during initialization,
	//   we need to ensure that the root link starts with the same orientation as the instance. The position of a link is the world space vector
	//   from the root link to that link. Since this is the root link, that vector is zero.
	phArticulatedBodyPart* rootLink = CreateArticulatedLink(m_Inst->GetMatrix().GetMat33ConstRef(), Vec3V(V_ZERO));
	InitArticulatedBody(*rootLink);
	InitArticulatedLinkInertia(0, rootLinkInertia, rootLinkMass DEV_ONLY(,0));

	// Set the collider matrix from the instance
	// This assumes that the bound CG is set up
	m_HierInst.articulatedCollider->SetColliderMatrixFromInstanceRigid();

	// Set all the link attachments for parts that have latched joints
	for (int groupIndex = 0; groupIndex < physicsLOD->GetNumChildGroups(); ++groupIndex)
	{
		fragTypeGroup* group = physicsLOD->GetGroup(groupIndex);
		float latchStrength = group->GetLatchStrength();
		if (type->HasMotionLimits(groupIndex) && (latchStrength == -1.0f || latchStrength > 0.0f))
		{
			int parentGroup = group->GetParentGroupPointerIndex();
			if (!fragVerifyf(parentGroup != 0xff, "Latched joint with no parent in type %s", type->GetTuneName()))
			{
				parentGroup = 0;
			}
			
			int parentChild = physicsLOD->GetGroup(parentGroup)->GetChildFragmentIndex();

			Matrix34 latchedMatrix;
			physicsLOD->ComputeInstanceFromLinkMatrix(parentChild,RC_MAT34V(latchedMatrix));
			SetLinkAttachmentRecurseChildren(*group, latchedMatrix);
		}
	}

#if HACK_GTA4
	// If we didn't allocate any link attachment matrices above then allocate them now
	if (m_HierInst.linkAttachment == NULL && type->GetForceAllocateLinkAttachments())
	{
		m_HierInst.linkAttachment = pgArray<Matrix34>::Create(physicsLOD->GetNumChildren());
		memcpy(m_HierInst.linkAttachment->GetElements(), physicsLOD->GetLinkAttachmentMatrices(), sizeof(Matrix34) * physicsLOD->GetNumChildren());
		m_HierInst.articulatedCollider->SetLinkAttachment(m_HierInst.linkAttachment);
	}
#endif
}

void fragCacheEntry::SetFixedArticulatedRoot(bool fixedArticulatedRoot)
{
	FastAssert(m_HierInst.body);
	m_HierInst.body->FixRoot(fixedArticulatedRoot);
	if(fixedArticulatedRoot)
	{
		m_Flags |= ART_FIXED_ROOT;
	}
	else
	{
		m_Flags &= ~ART_FIXED_ROOT;
	}
}

phJoint& fragCacheEntry::AddJointAndLink(	const u8 parentLinkIndex,
											Mat33V_In linkToWorldRotation,
											Vec3V_In linkPositionInstance,
											Vec3V_In linkAngularInertia,
											ScalarV_In linkMass,
											const int firstGroupIndexOfChildLink,
											bool handleCacheMemory)
{
	const fragTypeGroup& firstGroupInChildLink = *m_Inst->GetTypePhysics()->GetGroup(firstGroupIndexOfChildLink);
	fragAssertf(m_HierInst.latchedJoints == NULL || m_HierInst.latchedJoints->IsClear(firstGroupInChildLink.GetParentGroupPointerIndex()), "Adding links to latched links is not supported.");

	Vec3V rootLinkPositionInstance = m_HierInst.compositeBound->GetCGOffset();
	Vec3V rootLinkToLinkInstance = Subtract(linkPositionInstance,rootLinkPositionInstance);


	if (handleCacheMemory)
		fragMemStartCacheHeap();

	u8 childLinkIndex = (u8)m_HierInst.body->GetNumBodyParts();
	phArticulatedBodyPart& newLink = *CreateArticulatedLink(linkToWorldRotation,rootLinkToLinkInstance);

	// Add the new link to the body temporarily
	// We need to add the link because when creating the joint, the SetAxis functions read the link matrices from the body.
	m_HierInst.body->GetType().SetNumBodyParts(childLinkIndex+1);
	m_HierInst.body->SetBodyPart(childLinkIndex, &newLink);

	// Create the actual joint using the first group in the child link as the tuning group
	
	// Either copy the given joint or create a new one
	// Compute the skeleton matrix and initialize the joint
	u8 childIndex = firstGroupInChildLink.GetChildFragmentIndex();
	const fragTypeChild& child = *m_Inst->GetTypePhysics()->GetChild(childIndex);
	int boneIndex = m_Inst->GetType()->GetBoneIndexFromID(child.GetBoneID());
	// Compute the bone-from-instance matrix of the controlling bone of the new latch

	fragDrawable* childDrawable;
	if (child.GetDamagedEntity() && IsGroupDamaged(firstGroupIndexOfChildLink))
	{
		childDrawable = child.GetDamagedEntity();
	}
	else
	{
		childDrawable = child.GetUndamagedEntity();
	}

	Mat34V_ConstRef instanceFromComponent = m_HierInst.compositeBound->GetCurrentMatrix(childIndex);
	Mat34V componentFromBone;
	InvertTransformOrtho(componentFromBone, RCC_MAT34V(childDrawable->GetBoundMatrix()));
	Mat34V instanceFromBone;
	Transform(instanceFromBone, instanceFromComponent, componentFromBone);

	Vec3V rootLinkToBoneInstance = Subtract(instanceFromBone.GetCol3(), rootLinkPositionInstance);

	const crBoneData& bone = *m_Inst->GetType()->GetSkeletonData().GetBoneData(boneIndex);

	phJoint& newJoint = *CreateArticulatedJoint(bone, firstGroupInChildLink, childLinkIndex, parentLinkIndex, rootLinkToBoneInstance, instanceFromBone.GetMat33ConstRef());

	// Remove the temporarily added child link
	m_HierInst.body->SetBodyPart(childLinkIndex, NULL);
	m_HierInst.body->GetType().SetNumBodyParts(childLinkIndex);

	// Finished creating and initializing the new joint.
	m_HierInst.body->AddChild(parentLinkIndex, newJoint, newLink);
	InitArticulatedLinkInertia(childLinkIndex,linkAngularInertia,linkMass DEV_ONLY(,(u8)firstGroupIndexOfChildLink));

	if (handleCacheMemory)
		fragMemEndCacheHeap();

	return newJoint;
}

void fragCacheEntry::InitArticulatedLinkInertia(int  linkIndex,
															 Vec3V_In linkAngularInertia,
															 ScalarV_In linkMass
															 DEV_ONLY(,u8 groupIndex))
{
	// Clamp the angular inertia in case it is too small
	ScalarV clampedLinkMass = linkMass;
	Vec3V clampedLinkAngularInertia = linkAngularInertia;
	m_Inst->GetTypePhysics()->ClampAngularInertia(clampedLinkAngularInertia, clampedLinkMass);
#if __DEV
	if(!IsEqualAll(clampedLinkAngularInertia,linkAngularInertia))
	{
		const fragPhysicsLOD* physicsLOD = GetInst()->GetTypePhysics();
		fragWarningf("Link '%s' on fragment '%s' has had its mass and angular inertia clamped.",physicsLOD->GetGroup(groupIndex)->GetDebugName(),physicsLOD->GetArchetype()->GetFilename());
		fragWarningf("\tSmallest Allowed Angular Inertia: %f",physicsLOD->GetSmallestAngInertia());
		fragWarningf("\tMass: %5.3f -> %5.3f",linkMass.Getf(),clampedLinkMass.Getf());
		fragWarningf("\tAngular Inertia: <%5.3f, %5.3f, %5.3f> -> <%5.3f, %5.3f, %5.3f>",VEC3V_ARGS(linkAngularInertia),VEC3V_ARGS(clampedLinkAngularInertia));
	}
#endif // __DEV

	m_HierInst.body->SetMassAndAngInertia(linkIndex, clampedLinkMass, clampedLinkAngularInertia);
}

phArticulatedBodyPart* fragCacheEntry::CreateArticulatedLink(Mat33V_In linkToWorldRotation,
														   Vec3V_In rootLinkToLinkInstance)
{
	// The controlling bone has rotation limits and it's not already mapped to a link (body part).
	// Scale the angular inertia and mass by the same amount so that no angular inertia component is smaller than the
	// smallest allowed, unless the required scale factor makes another angular inertia component larger than the
	// largest allowed, and in that case scale them all by the same amount to make the largest one the largest allowed.
	phArticulatedBodyPart* newLink = rage_new phArticulatedBodyPart;

	// Transpose the user's matrix to get the bodypart rotation. 
	// The position of the bodypart is is the offset from the root link in world space.
	Mat34V linkToWorldTranspose;
	Transpose(linkToWorldTranspose.GetMat33Ref(), linkToWorldRotation);
	linkToWorldTranspose.SetCol3(Transform3x3(m_Inst->GetMatrix(),rootLinkToLinkInstance));
	newLink->SetMatrix(RCC_MATRIX34(linkToWorldTranspose));

	return newLink;
}

phJoint* fragCacheEntry::CreateArticulatedJoint(const crBoneData& bone,
												const fragTypeGroup& tuningGroup,
												u8 childLinkIndex,
												u8 parentLinkIndex,
												Vec3V_In rootLinkToJointInstance,
												Mat33V_In boneToInstanceRotation)
{
	const fragType* type = m_Inst->GetType();

	Mat33V instanceToWorldRotation = m_Inst->GetMatrix().GetMat33();
	Vec3V rootLinkToJointWorld = Multiply(instanceToWorldRotation, rootLinkToJointInstance);

	const crJointData* jointData = type->GetCommonDrawable()->GetJointData();
	Assert(jointData);

	bool hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits;
#if HACK_GTA4
	// Again, not sure if this is a deliberate gta change, or just a change that never made it across from dev
	AssertVerify(type->WillBeArticulated(bone, *jointData, hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits));
#else
	AssertVerify(type->HasMotionLimits(bone, hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits));
#endif


	// TODO: darchard - With new joint limit format, due to the conversion from the old bonedata limits if there is one limits, there will now be 3.
	//	This code should really check to make sure that there's actually a range in the givin limits here.
	phJoint* newJoint = NULL;
	int limitTDof = (hasXTLimits ? 1 : 0) +
		(hasYTLimits ? 1 : 0) +
		(hasZTLimits ? 1 : 0);

	if (limitTDof)
	{
		phPrismaticJoint* newPrismaticJoint = rage_new phPrismaticJoint;
		newJoint = newPrismaticJoint;
		fragAssertf(limitTDof == 1, "Only 1 DOF translational joints are allowed at this time");

		Vector3 axis;
		float minValue = 0.0f;
		float maxValue = 0.0f;

		Vec3V minTrans, maxTrans;
		AssertVerify(jointData->GetTranslationLimits(bone, minTrans, maxTrans));

		if (hasXTLimits)
		{
			axis = XAXIS;
			minValue = minTrans.GetXf();
			maxValue = maxTrans.GetXf();
		}
		else if (hasYTLimits)
		{
			axis = YAXIS;
			minValue = minTrans.GetYf();
			maxValue = maxTrans.GetYf();
		}
		else
		{
			Assert(hasZTLimits);
			axis = ZAXIS;
			minValue = minTrans.GetZf();
			maxValue = maxTrans.GetZf();
		}

		// Transform the axis from bone to world space
		axis = VEC3V_TO_VECTOR3(Multiply(instanceToWorldRotation, Multiply(boneToInstanceRotation, RCC_VEC3V(axis))));

		float restoringStrength = tuningGroup.GetJointRestoringStrength();
		float restoringMaxTorque = tuningGroup.GetJointRestoringMaxTorque();
		if (restoringStrength != 0.0f && restoringMaxTorque != 0.0f)
		{
			newPrismaticJoint->SetDriveState(phJoint::DRIVE_STATE_ANGLE);
			newPrismaticJoint->SetMusclePositionStrength(restoringStrength);
			newPrismaticJoint->SetMaxMuscleForce(restoringMaxTorque);
		}

		newPrismaticJoint->m_Type->m_ParentLinkIndex = parentLinkIndex;
		newPrismaticJoint->m_Type->m_ChildLinkIndex = childLinkIndex; 
		newPrismaticJoint->SetAxis(m_HierInst.body, RCC_VECTOR3(rootLinkToJointWorld), axis, minValue, maxValue);
	}
	else
	{
		int limitDof = (hasXLimits ? 1 : 0) +
			(hasYLimits ? 1 : 0) +
			(hasZLimits ? 1 : 0);

		Assert(limitDof > 0 && limitDof <= 3);

		float twistAngle = 0.0f;

		float targetSpeed = tuningGroup.GetJointRotationSpeed();
		float speedStrength = tuningGroup.GetJointRotationStrength();
		float restoringStrength = tuningGroup.GetJointRestoringStrength();
		float restoringMaxTorque = tuningGroup.GetJointRestoringMaxTorque();

		switch (limitDof)
		{
		case 1:
			{
				Vector3 axis;
				float minAngle = -PI;
				float maxAngle = PI;

				Vec3V minRot, maxRot;
				AssertVerify(jointData->ConvertToEulers(bone, minRot, maxRot));

				if (hasXLimits)
				{
					axis = XAXIS;
					minAngle = minRot.GetXf();
					maxAngle = maxRot.GetXf();
				}
				else if (hasYLimits)
				{
					axis = YAXIS;
					minAngle = minRot.GetYf();
					maxAngle = maxRot.GetYf();
				}
				else
				{
					axis = ZAXIS;
					minAngle = minRot.GetZf();
					maxAngle = maxRot.GetZf();
				}

				// Transform the axis from bone to world space
				axis = VEC3V_TO_VECTOR3(Multiply(instanceToWorldRotation, Multiply(boneToInstanceRotation, RCC_VEC3V(axis))));

				phJoint1Dof* new1DofJoint = rage_new phJoint1Dof;
				new1DofJoint->m_Type->m_ParentLinkIndex = parentLinkIndex;
				new1DofJoint->m_Type->m_ChildLinkIndex = childLinkIndex; 
				newJoint = new1DofJoint;
				new1DofJoint->SetAxis(m_HierInst.body, RCC_VECTOR3(rootLinkToJointWorld),
					axis,
					minAngle,
					maxAngle);

				float minSoft = tuningGroup.GetMinSoftAngle1();
				float maxSoft = tuningGroup.GetMaxSoftAngle1();

				minSoft = Max(-1.0f, minSoft);
				minSoft = Min( 1.0f, minSoft);
				maxSoft = Max(-1.0f, maxSoft);
				maxSoft = Min( 1.0f, maxSoft);

				if (minSoft > maxSoft)
				{
					minSoft = (minSoft + maxSoft) * 0.5f;
					maxSoft = minSoft;
				}

				float halfAngleRange = (maxAngle - minAngle) * 0.49f;
				minSoft *= halfAngleRange;
				maxSoft *= halfAngleRange;
#ifdef USE_SOFT_LIMITS
				float avgAngle = (maxAngle + minAngle) * 0.5f;
				minSoft += avgAngle;
				maxSoft += avgAngle;
				new1DofJoint->SetSoftAngleLimits(minSoft, maxSoft);
#endif

				bool speedDrive = false;
				if (speedStrength != 0.0f)
				{
					speedDrive = true;
					new1DofJoint->SetDriveState(phJoint::DRIVE_STATE_SPEED);
					new1DofJoint->SetMuscleTargetSpeed(targetSpeed);
					new1DofJoint->SetMuscleSpeedStrength(speedStrength);
				}

				if (restoringStrength != 0.0f && restoringMaxTorque != 0.0f)
				{
					if (speedDrive)
					{
						new1DofJoint->SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
					}
					else
					{
						new1DofJoint->SetDriveState(phJoint::DRIVE_STATE_ANGLE);
					}

					new1DofJoint->SetMuscleAngleStrength(restoringStrength);
					new1DofJoint->SetMaxMuscleTorque(restoringMaxTorque);
				}
#ifdef USE_SOFT_LIMITS
				if (restoringStrength != 0.0f && restoringMaxTorque == 0.0f)
				{
					new1DofJoint->SetSoftLimitStrength(restoringStrength);
				}
#endif
			}
			break;

		case 3:
			{
				Vec3V minRot, maxRot;
				AssertVerify(jointData->ConvertToEulers(bone, minRot, maxRot));

				twistAngle = (maxRot.GetZf() - minRot.GetZf()) * 0.5f;
				if (twistAngle >= PI)
				{
					twistAngle = FLT_MAX;
				}
				// fall through
			}
		case 2:
			{
				phJoint3Dof* new3DofJoint = rage_new phJoint3Dof;
				newJoint = new3DofJoint;

				Vector3 axis;
				Vector3 leanAxis;
				float minFirstLean = -PI;
				float maxFirstLean = PI;
				float minSecondLean = -PI;
				float maxSecondLean = PI;

				Vec3V minRot, maxRot;
				AssertVerify(jointData->ConvertToEulers(bone, minRot, maxRot));

				if (limitDof==3 || !hasZLimits)
				{
					axis = ZAXIS;
					leanAxis = YAXIS;
					maxFirstLean = maxRot.GetXf();
					minFirstLean = minRot.GetXf();
					maxSecondLean = maxRot.GetYf();
					minSecondLean = minRot.GetYf();
				}
				else if (!hasXLimits)
				{
					axis = XAXIS;
					leanAxis = YAXIS;
					maxFirstLean = maxRot.GetZf();
					minFirstLean = minRot.GetZf();
					maxSecondLean = maxRot.GetYf();
					minSecondLean = minRot.GetYf();
				}
				else
				{
					Assert(!hasYLimits);
					axis = YAXIS;
					leanAxis = XAXIS;
					maxFirstLean = maxRot.GetZf();
					minFirstLean = minRot.GetZf();
					maxSecondLean = maxRot.GetXf();
					minSecondLean = minRot.GetXf();
				}

				// Transform the axis from bone to instance space
				axis = VEC3V_TO_VECTOR3(Multiply(boneToInstanceRotation, RCC_VEC3V(axis)));
				leanAxis = VEC3V_TO_VECTOR3(Multiply(boneToInstanceRotation, RCC_VEC3V(leanAxis)));

				float firstTilt = (maxFirstLean + minFirstLean) * 0.5f;

				Vector3 secondLeanAxis = leanAxis;
				secondLeanAxis.Cross(axis);

				Matrix34 tiltMatrix(M34_IDENTITY);
				tiltMatrix.MakeRotateUnitAxis(secondLeanAxis, firstTilt);

				tiltMatrix.Transform3x3(axis);
				tiltMatrix.Transform3x3(leanAxis);

				float firstLean = (maxFirstLean - minFirstLean) * 0.5f;
				float secondLean = (maxSecondLean - minSecondLean) * 0.5f;

				if (twistAngle < 0.01f || firstLean >= PI || secondLean >= PI)
				{
					twistAngle = 0.01f;

					// Twist limit is meaningless without some lean limits
					if (firstLean >= PI)
					{
						firstLean = PI * 0.9f;
					}

					if (secondLean >= PI)
					{
						secondLean = PI * 0.9f;
					}
				}

				// Transform the axis from instance to world space
				axis = VEC3V_TO_VECTOR3(Multiply(instanceToWorldRotation, RCC_VEC3V(axis)));
				leanAxis = VEC3V_TO_VECTOR3(Multiply(instanceToWorldRotation, RCC_VEC3V(leanAxis)));
				//orientation.Transform3x3(secondLeanAxis); // Not needed below here
				axis = VEC3V_TO_VECTOR3(UnTransformOrtho(boneToInstanceRotation, RCC_VEC3V(axis)));
				leanAxis = VEC3V_TO_VECTOR3(UnTransformOrtho(boneToInstanceRotation, RCC_VEC3V(leanAxis)));

				Vector3 meanEulers;
				meanEulers.x = -Abs(minRot.GetZf() + maxRot.GetZf()) * 0.5f;//0.0f;//(bone->GetRotationMin().GetXf() + bone->GetRotationMax().GetXf()) * 0.5f;
				meanEulers.y = -Abs(minRot.GetYf() + maxRot.GetYf()) * 0.5f;
				meanEulers.z = -Abs(minRot.GetXf() + maxRot.GetXf()) * 0.5f;

				Quaternion quaternion;
				quaternion.FromEulers(meanEulers);

				Quaternion initialQuaterion;
				initialQuaterion.FromMatrix34(MAT34V_TO_MATRIX34(Mat34V(boneToInstanceRotation, Vec3V(V_ZERO))));
				quaternion.Dot(initialQuaterion);

				new3DofJoint->m_Type->m_ParentLinkIndex = parentLinkIndex;
				new3DofJoint->m_Type->m_ChildLinkIndex = childLinkIndex; 

				new3DofJoint->SetAxis(m_HierInst.body, RCC_VECTOR3(rootLinkToJointWorld),
					axis,
					leanAxis,
					firstLean,
					secondLean,
					twistAngle,
					quaternion);

				float maxSoft1 = tuningGroup.GetMaxSoftAngle1();
				float maxSoft2 = tuningGroup.GetMaxSoftAngle2();
				float maxSoftTwist = tuningGroup.GetMaxSoftAngle3();

				maxSoft1 = Min(1.0f, maxSoft1);
				maxSoft2 = Min(1.0f, maxSoft2);
				maxSoftTwist = Min(1.0f, maxSoftTwist);
#ifdef USE_SOFT_LIMITS
				maxSoft1 *= firstLean;
				maxSoft2 *= secondLean;
				maxSoftTwist *= twistAngle;
				new3DofJoint->SetSoftLimitRatio(maxSoft1 / firstLean);
#endif
				bool speedDrive = false;
				if (speedStrength != 0.0f)
				{
					speedDrive = true;
					new3DofJoint->SetDriveState(phJoint::DRIVE_STATE_SPEED);
					new3DofJoint->SetLean1TargetSpeed(targetSpeed);
					new3DofJoint->SetLean2TargetSpeed(targetSpeed);
					new3DofJoint->SetTwistTargetSpeed(targetSpeed);
					Vector3 muscleSpeedStrength;
					muscleSpeedStrength.Set(speedStrength);
					new3DofJoint->SetMuscleSpeedStrength(muscleSpeedStrength);
				}

				if (restoringStrength != 0.0f && restoringMaxTorque != 0.0f)
				{
					if (speedDrive)
					{
						new3DofJoint->SetDriveState(phJoint::DRIVE_STATE_ANGLE_AND_SPEED);
					}
					else
					{
						new3DofJoint->SetDriveState(phJoint::DRIVE_STATE_ANGLE);
					}

					new3DofJoint->SetMuscleAngleStrength(Vector3(restoringStrength, restoringStrength, restoringStrength));
					new3DofJoint->SetMaxMuscleTorque(Vector3(restoringMaxTorque, restoringMaxTorque, restoringMaxTorque));
				}
			}
		}
	}

	newJoint->SetStiffness(tuningGroup.GetJointStiffness());
	return newJoint;
}

void fragCacheEntry::RefreshArticulated()
{
	Assert(m_Inst);

	m_HierInst.articulatedCollider->SetReferenceFrameVelocity(Vec3V(V_ZERO).GetIntrin128ConstRef());
	m_HierInst.articulatedCollider->SetReferenceFrameAngularVelocity(Vec3V(V_ZERO).GetIntrin128ConstRef());
	m_HierInst.articulatedCollider->SetReducedLinearDampingFrames(0);
	
	m_HierInst.articulatedCollider->TemporarilyDisableSelfCollision(-1);

	m_HierInst.body->ResetAllJointLimitAdjustments();

	if (m_Inst->GetTypePhysics()->GetBodyType())
	{
		// Reset joint values
		if (m_IncreaseMinStiffnessOnActivation)
		{
			m_HierInst.body->SetBodyMinimumStiffness(phArticulatedBody::sm_EmergencyMinStiffness);
			m_HierInst.body->SetStiffness(phArticulatedBody::sm_EmergencyMinStiffness);
			m_IncreaseMinStiffnessOnActivation = false;
		}
		else
		{
			m_HierInst.body->SetBodyMinimumStiffness(DEFAULT_JOINT_MIN_STIFFNESS);
			m_HierInst.body->SetStiffness(DEFAULT_JOINT_STIFFNESS);
		}
		m_HierInst.body->SetDriveState(phJoint::DRIVE_STATE_FREE);
		m_HierInst.body->SetEffectorsToZeroPose();

		m_HierInst.body->Freeze();

		UpdateInstabilityPrevention(1);

		if (IsIntendingToUseInstanceToArtColliderOffsetMat())
		{
			// Set the offset from inst matrix to collider matrix.  The collider matrix is assumed to be the transpose of
			// the link root (see phArticulatedCollider::MoveColliderWithRootLink()).  Currently, the root bound defines
			// that relationship.  More generally, the link attachment matrix would need to get factored in.
			Matrix34 offsetMat = RCC_MATRIX34(static_cast<phBoundComposite*>(m_Inst->GetArchetype()->GetBound())->GetCurrentMatrix(0));
			ReOrthonormalize3x3(RC_MAT34V(offsetMat), RCC_MAT34V(offsetMat));
			if ( IsFiniteAll(RC_MAT34V(offsetMat).GetCol0()) == 0 )
			{
				fragAssertf(0, "fragCacheEntry::RefreshArticulated() - bad root bound matrix");
				RC_MAT34V(offsetMat).SetIdentity3x3();
				RC_MAT34V(offsetMat).SetCol3(Vec3V(V_ZERO));
			}
			m_HierInst.articulatedCollider->SetOffsetMatrix(offsetMat);
			m_HierInst.articulatedCollider->SetUsingInstanceToArtColliderOffsetMat(true);

			// Check the inst matrix and set the collider matrix from it
			Mat34V instMat = m_Inst->GetMatrix();
			ReOrthonormalize3x3(instMat, instMat);
			if ( IsFiniteAll(instMat.GetCol0()) == 0 )
			{
				fragAssertf(0, "fragCacheEntry::RefreshArticulated() - bad inst matrix");
				instMat.SetIdentity3x3();
				instMat.SetCol3(Vec3V(V_ZERO));
			}
			m_Inst->SetMatrix(instMat);
			m_HierInst.articulatedCollider->SetColliderMatrixFromInstanceRigid();

			m_Inst->PoseArticulatedBodyFromBounds();
			m_HierInst.body->RejuvenateMatrices();

			m_Inst->SyncSkeletonToArticulatedBody();

			m_HierInst.body->CalculateInertias();
		}
		else
		{
			fragAssertf(0, "Not using collider-inst offset matrix");
			m_HierInst.articulatedCollider->SetUsingInstanceToArtColliderOffsetMat(false);

			// Determine the new inst matrix
			Matrix34 linkRoot = RCC_MATRIX34(static_cast<phBoundComposite*>(m_Inst->GetArchetype()->GetBound())->GetCurrentMatrix(0));
			linkRoot.Dot(MAT34V_TO_MATRIX34(m_Inst->GetMatrix()));

			// Adjust bounds accordingly 
			((phBoundComposite*)m_Inst->GetArchetype()->GetBound())->AdjustToNewInstMatrix(m_Inst->GetMatrix(), RCC_MAT34V(linkRoot));

			// Adjust skeleton accordingly
			m_HierInst.skeleton->Update();
			Matrix34 ragdollToAnimMtx;
			ragdollToAnimMtx.FastInverse(linkRoot);
			ragdollToAnimMtx.DotFromLeft(MAT34V_TO_MATRIX34(m_Inst->GetMatrix()));
			m_HierInst.skeleton->Transform(MATRIX34_TO_MAT34V(ragdollToAnimMtx));
			m_HierInst.skeleton->InverseUpdate();

			// Set the inst matrix
			m_Inst->SetMatrix(MATRIX34_TO_MAT34V(linkRoot));

			// Update collider Matrix
			m_HierInst.articulatedCollider->SetColliderMatrixFromInstance();

			// Update articulated body off of bounds and correct collider matrix
			m_Inst->PoseArticulatedBodyFromBounds();

			// Reset Forces
			m_HierInst.articulatedCollider->Freeze();
		}
	}

	if (m_HierInst.body)
	{
		UpdateSavedVelocityArraySizes(true);
#if __ASSERT
		CheckVelocities();
#endif

		// This is normally done while performing collider update, but we might not get an update before we want to apply impulses
		m_HierInst.articulatedCollider->InitAccumJointImpulse();
		m_HierInst.articulatedCollider->FindJointLimitDofs();
	}
}

#if __ASSERT
void fragCacheEntry::CheckVelocities()
{
	if (!(GetHierInst() && GetHierInst()->articulatedCollider && GetHierInst()->body))
		return;

	phArticulatedCollider *collider = GetHierInst()->articulatedCollider;
	phArticulatedBody *body = GetHierInst()->body;

	int numBodyParts = body->GetNumBodyParts();
	fragAssertf(body->m_IncrementalBase < numBodyParts, "body->m_IncrementalBase is %d, numBodyParts is %d", body->m_IncrementalBase, numBodyParts);
	fragAssertf(body->m_PartVelocities.GetCount() == numBodyParts, "body->m_PartVelocities.GetCount() is %d, numBodyParts is %d", body->m_PartVelocities.GetCount(), numBodyParts);
	fragAssertf(body->m_VelocitiesToPropUp.GetCount() == numBodyParts, "body->m_VelocitiesToPropUp.GetCount() is %d, numBodyParts is %d", body->m_VelocitiesToPropUp.GetCount(), numBodyParts);
	fragAssertf(collider->m_SavedLinearVelocities.GetCount() == numBodyParts, "collider->m_SavedLinearVelocities.GetCount() is %d, numBodyParts is %d", collider->m_SavedLinearVelocities.GetCount(), numBodyParts);
	fragAssertf(collider->m_SavedAngularVelocities.GetCount() == numBodyParts, "collider->m_SavedAngularVelocities.GetCount() is %d, numBodyParts is %d", collider->m_SavedAngularVelocities.GetCount(), numBodyParts);

	for (int i=0; i<numBodyParts; i++)
	{
		fragAssertf(body->m_PartVelocities[i]==body->m_PartVelocities[i], "index is %d, numBodyParts is %d", i, numBodyParts);
		fragAssertf(IsLessThanAll(body->m_PartVelocities[i].omega,Vec3V(V_FLT_LARGE_6)), "index is %d, numBodyParts is %d", i, numBodyParts);
		fragAssertf(body->m_VelocitiesToPropUp[i]==body->m_VelocitiesToPropUp[i], "index is %d, numBodyParts is %d", i, numBodyParts);
		fragAssertf(IsLessThanAll(body->m_VelocitiesToPropUp[i].omega,Vec3V(V_FLT_LARGE_6)), "index is %d, numBodyParts is %d", i, numBodyParts);
		fragAssertf(IsFiniteAll(collider->m_SavedLinearVelocities[i]), "index is %d, numBodyParts is %d", i, numBodyParts);
		fragAssertf(IsFiniteAll(collider->m_SavedAngularVelocities[i]), "index is %d, numBodyParts is %d", i, numBodyParts);
		fragAssertf(IsLessThanAll(collider->m_SavedLinearVelocities[i],Vec3V(V_FLT_LARGE_6)), "index is %d, numBodyParts is %d", i, numBodyParts);
		fragAssertf(IsLessThanAll(collider->m_SavedAngularVelocities[i],Vec3V(V_FLT_LARGE_6)), "index is %d, numBodyParts is %d", i, numBodyParts);
	}
	for (int i=0; i<numBodyParts-1; i++)
	{
		Assertf(body->GetJoint(i).m_Type->m_ParentLinkIndex == body->m_Type->GetJointParentIndex(i), "index is %d, numBodyParts is %d", i, numBodyParts);
		Assertf(body->GetJoint(i).m_Type->m_ChildLinkIndex == i+1, "index is %d, numBodyParts is %d", i, numBodyParts);
		Assertf(body->GetJoint(i).m_Type->m_ParentLinkIndex < (u8) numBodyParts && body->GetJoint(i).m_Type->m_ChildLinkIndex < (u8) numBodyParts, "index is %d, numBodyParts is %d", i, numBodyParts);
		Assertf(body->GetJoint(i).GetVelocityToPropDown()==body->GetJoint(i).GetVelocityToPropDown(), "index is %d, numBodyParts is %d", i, numBodyParts);
		Assertf(IsLessThanAll(body->GetJoint(i).GetVelocityToPropDown().omega,Vec3V(V_FLT_LARGE_6)), "index is %d, numBodyParts is %d", i, numBodyParts);
	}
}
#endif

void fragCacheEntry::InitComponentAndBoneIndexMaps()
{
	const fragType* type = m_Inst->GetType();
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	const crSkeletonData& skeletonData = type->GetSkeletonData();
	int numBones = m_HierInst.skeleton->GetBoneCount();
	int numChildren = physicsLOD->GetNumChildren();

	// Create the bone->component and component->bone maps
	m_BoneIndexToComponentMap = rage_new int[numBones];	
	m_ComponentToBoneIndexMap = rage_new int[numChildren];	
	sysMemSet(m_BoneIndexToComponentMap, -1, sizeof(m_BoneIndexToComponentMap[0])*numBones);

	// Find the bone index of each child
	for(int childIndex = 0; childIndex < numChildren; childIndex++)
	{
		int boneIndex;
		fragVerifyf(skeletonData.ConvertBoneIdToIndex(physicsLOD->GetChild(childIndex)->GetBoneID(),boneIndex),"ConvertBoneIdToIndex failed on component %i with bone id %i.", childIndex, physicsLOD->GetChild(childIndex)->GetBoneID());
		Assert(boneIndex < numBones && boneIndex >= 0);
		m_ComponentToBoneIndexMap[childIndex] = boneIndex;
		if(m_BoneIndexToComponentMap[boneIndex] == -1)
		{
			m_BoneIndexToComponentMap[boneIndex] = childIndex;
		}
	}

	// Find the component index of any bone that doesn't have a direct link with a child
	for (int boneIndex = 0; boneIndex < numBones; ++boneIndex)
	{
		int componentIndex = m_BoneIndexToComponentMap[boneIndex];
		if (componentIndex == -1)
		{
			int parentBoneIndex = skeletonData.GetParentIndex(boneIndex);

			while (parentBoneIndex >= 0 && componentIndex < 0)
			{
				componentIndex = m_BoneIndexToComponentMap[parentBoneIndex];
				parentBoneIndex = skeletonData.GetParentIndex(parentBoneIndex);
			}

			m_BoneIndexToComponentMap[boneIndex] = componentIndex;
		}
	}
}

inline void fragCacheEntry::InitArticulated(fragHierarchyInst* hierSrc)
{
	// This is where we check for articulation and create and set up both the phArticulatedBody and phArticulatedCollider if we find it.
	if(
#if __BANK
		fragTune::IsInstantiated() && !FRAGTUNE->GetAllowArticulation() )
#else // __BANK
		PARAM_nofragarticulation.Get())
#endif // __BANK
	{
		return;
	}

	const fragType* type = const_cast<fragType*>(m_Inst->GetType());
	const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
	const phBoundComposite* typeBound = physicsLOD->GetCompositeBounds();
	Assert(type);
	Assert(m_Inst);
	Assert(m_HierInst.latchedJoints == NULL);


	if (phArticulatedBodyType* resourcedBodyType = m_Inst->GetTypePhysics()->GetBodyType())
	{
		// In V, the physicsLOD data for NM agents is not defragable, so it's safe to just pass a pointer to it here
		// (only would be unsafe due to the DMA plan which stores this pointer and won't get regenerated due to defragmentation TODO - fix that).
		// Animals still get defragged so CLone them here (until we fix the bug where DMA plans don't get their pointer to AB type data updated on defrag).
		phArticulatedBodyType* bodyType = NULL;
		if (m_Inst->GetARTAssetID() >= 0)
			bodyType = resourcedBodyType;
		else
			bodyType = resourcedBodyType->Clone();

		m_HierInst.body = rage_new phArticulatedBody(bodyType);
		m_HierInst.body->PreInit(physicsLOD->GetNumChildren());
		phArticulatedBodyPart* rootPart = rage_new phArticulatedBodyPart();

		const Matrix34& boundMatrix = RCC_MATRIX34(typeBound->GetCurrentMatrix(0));
		Matrix34 matrix;
		matrix.Transpose(boundMatrix);
		matrix.d = boundMatrix.d;
		rootPart->SetMatrix(matrix);

		m_HierInst.body->AddRoot(*rootPart);
		m_HierInst.body->SetMassAndAngInertia(0, bodyType->GetResourcedAngInertiaXYZmassW(0));

		for (int jointIndex = 0; jointIndex < physicsLOD->GetNumChildren() - 1; ++jointIndex)
		{
			phArticulatedBodyPart* part = rage_new phArticulatedBodyPart();

			const Matrix34& boundMatrix = RCC_MATRIX34(typeBound->GetCurrentMatrix(jointIndex + 1));
			matrix.Transpose(boundMatrix);
			matrix.d = boundMatrix.d;
			part->SetMatrix(matrix);

			phJointType *jointType = bodyType->GetJointTypes(jointIndex);

			if (jointType->m_JointType == phJoint::JNT_1DOF)
			{
				phJoint1Dof* joint = rage_new phJoint1Dof(static_cast<phJoint1DofType*>(jointType));

				// Set link pointers and default stiffness
				joint->SetStiffness(jointType->m_DefaultStiffness);
				m_HierInst.body->AddChild(jointType->m_ParentLinkIndex, *joint, *part);
			}
			else if (jointType->m_JointType == phJoint::JNT_3DOF)
			{
				phJoint3Dof* joint = rage_new phJoint3Dof(static_cast<phJoint3DofType*>(jointType));

				// Set link pointers and default stiffness
				joint->SetStiffness(jointType->m_DefaultStiffness);
				m_HierInst.body->AddChild(jointType->m_ParentLinkIndex, *joint, *part);		
			}

			m_HierInst.body->SetMassAndAngInertia(jointIndex + 1, bodyType->GetResourcedAngInertiaXYZmassW(jointIndex + 1));
		}

		m_HierInst.articulatedCollider = rage_new phArticulatedCollider(phArticulatedBodyType::MAX_NUM_JOINT_DOFS, 32);
		m_HierInst.articulatedCollider->SetARTAssetID(type->GetARTAssetID());
		m_HierInst.articulatedCollider->SetLOD(m_Inst->GetCurrentPhysicsLOD());

		m_HierInst.articulatedCollider->SetLargeTurnsTriggerPushCollisions(false);

		// Reset joint values
		if (m_IncreaseMinStiffnessOnActivation)
		{
			m_HierInst.body->SetBodyMinimumStiffness(phArticulatedBody::sm_EmergencyMinStiffness);
			m_HierInst.body->SetStiffness(phArticulatedBody::sm_EmergencyMinStiffness);
			m_IncreaseMinStiffnessOnActivation = false;
		}
		else
		{
			m_HierInst.body->SetBodyMinimumStiffness(DEFAULT_JOINT_MIN_STIFFNESS);
			m_HierInst.body->SetStiffness(DEFAULT_JOINT_STIFFNESS);
		}
		m_HierInst.body->SetDriveState(phJoint::DRIVE_STATE_FREE);
		m_HierInst.body->SetEffectorsToZeroPose();

		UpdateInstabilityPrevention(1);

		// Set the offset from inst matrix to collider matrix.  The collider matrix is assumed to be the transpose of
		// the link root (see phArticulatedCollider::MoveColliderWithRootLink()).  Currently, the root bound defines
		// that relationship.  More generally, the link attachment matrix would need to get factored in.
		Matrix34 offsetMat = RCC_MATRIX34(static_cast<phBoundComposite*>(m_Inst->GetArchetype()->GetBound())->GetCurrentMatrix(0));
		ReOrthonormalize3x3(RC_MAT34V(offsetMat), RCC_MAT34V(offsetMat));
		if ( IsFiniteAll(RC_MAT34V(offsetMat).GetCol0()) == 0 )
		{
			fragAssertf(0, "fragCacheEntry::RefreshArticulated() - bad root bound matrix");
			RC_MAT34V(offsetMat).SetIdentity3x3();
			RC_MAT34V(offsetMat).SetCol3(Vec3V(V_ZERO));
		}
		m_HierInst.articulatedCollider->SetOffsetMatrix(offsetMat);
		m_HierInst.articulatedCollider->SetUsingInstanceToArtColliderOffsetMat(true);

		m_HierInst.articulatedCollider->SetBody(m_HierInst.body);
		m_HierInst.articulatedCollider->SetInstance(m_Inst);
		m_HierInst.articulatedCollider->UpdateSavedVelocityArraySizes(true);
		m_HierInst.body->Freeze();
#if __ASSERT
		CheckVelocities();
#endif

		// Check the inst matrix and set the collider matrix from it
		Mat34V instMat = m_Inst->GetMatrix();
		ReOrthonormalize3x3(instMat, instMat);
		if ( IsFiniteAll(instMat.GetCol0()) == 0 )
		{
			fragAssertf(0, "fragCacheEntry::RefreshArticulated() - bad inst matrix");
			instMat.SetIdentity3x3();
			instMat.SetCol3(Vec3V(V_ZERO));
		}
		m_Inst->SetMatrix(instMat);
		m_HierInst.articulatedCollider->SetColliderMatrixFromInstanceRigid();

		m_Inst->PoseArticulatedBodyFromBounds();
		m_HierInst.body->RejuvenateMatrices();
		m_HierInst.body->CalculateInertias();

		// This is normally done while performing collider update, but we might not get an update before we want to apply impulses
		m_HierInst.articulatedCollider->InitAccumJointImpulse();
		m_HierInst.articulatedCollider->FindJointLimitDofs();
		m_HierInst.articulatedCollider->InitInertia();
		m_HierInst.body->ResetPropagationVelocities();

		m_Inst->SyncSkeletonToArticulatedBody();

		phSleep* sleep = rage_new phSleep(m_HierInst.articulatedCollider);
		m_HierInst.articulatedCollider->SetSleep(sleep);

		// Initialize the max linear and angular velocities for ragdolls
		m_HierInst.articulatedCollider->SetMaxSpeed(phArticulatedBody::sm_RagdollMaxLinearSpeed);
		m_HierInst.articulatedCollider->SetMaxAngSpeed(phArticulatedBody::sm_RagdollMaxAngularSpeed);
		

#if __PS3
		sysDmaPlan* dmaPlan = rage_new phArticulatedCollider::DmaPlan;
		m_HierInst.articulatedCollider->GenerateCoreDmaPlan(*dmaPlan);
#endif

		return;
	}

	if (type->GetCommonDrawable() == NULL || type->GetCommonDrawable()->GetSkeletonData() == NULL)
	{
		return;
	}

	u8 rootGroupIndex = physicsLOD->GetChild(m_HierInst.rootLinkChildIndex)->GetOwnerGroupPointerIndex();

#if __ASSERT
	fragAssertf(hierSrc == NULL || !IsGroupBroken(rootGroupIndex), "Root group (%i) is broken on this cache entry.", rootGroupIndex);
	for(u8 groupIndex = 0; groupIndex < rootGroupIndex; ++groupIndex)
	{
		fragAssertf(IsGroupBroken(groupIndex), "There is a non-broken group (%i) before the root group index (%i).", groupIndex, rootGroupIndex);
	}
#endif // __ASSERT

	m_HierInst.rootLinkChildIndex = physicsLOD->GetGroup(rootGroupIndex)->GetChildFragmentIndex();

	u8 numGroups = physicsLOD->GetNumChildGroups();
	u8 numChildren = physicsLOD->GetNumChildren();

	// Allocate temporary maps for group->link and link->group
	u8* groupIndexToLinkIndex = Alloca(u8,numChildren);
	u8 linkIndexToFirstGroupIndex[phArticulatedBodyType::MAX_NUM_LINKS];
	u8 numLinks;
	u8 numLatchedJoints;
	type->MapGroupsToLinks(0,groupIndexToLinkIndex,linkIndexToFirstGroupIndex,numLinks,numLatchedJoints,m_HierInst.groupBroken, hierSrc ? &(hierSrc->latchedJoints) : NULL, false);

	// we need to create the articulated body and collider if we have latches, even if there aren't any joints
	if(numLatchedJoints + numLinks > 1)
	{
		// if we have a source, we want to just copy all of that information
		if(hierSrc)
		{
			if(hierSrc->latchedJoints)
			{
				// Copy the source's latched joints
				m_HierInst.latchedJoints = rage_new atBitSet(*hierSrc->latchedJoints);
			}

			// Copy the link attachment matrices over from the source
			Assert(m_HierInst.linkAttachment == NULL);
			m_HierInst.linkAttachment = pgArray<Matrix34>::Create(numChildren);
			sysMemCpy(m_HierInst.linkAttachment->GetElements(), hierSrc->articulatedCollider->GetLinkAttachmentMatrices(), sizeof(Matrix34)*numChildren);

			// Create the new root and articulated body
			u8 sourceRootLinkIndex = (u8)hierSrc->articulatedCollider->GetLinkFromComponent(physicsLOD->GetGroup(rootGroupIndex)->GetChildFragmentIndex());
			const phArticulatedBodyPart& sourceRootLink = hierSrc->body->GetLink(sourceRootLinkIndex);
			phArticulatedBodyPart* newRootLink = rage_new phArticulatedBodyPart(sourceRootLink);
			InitArticulatedBody(*newRootLink);
			m_HierInst.body->SetMassAndAngInertia(m_HierInst.body->GetNumBodyParts()-1, hierSrc->body->GetMass(sourceRootLinkIndex), 
				hierSrc->body->GetAngInertia(sourceRootLinkIndex));

			// Go through all the children and copy over the matching link and joint from the source
			for(u8 linkIndex = 1; linkIndex < numLinks; ++linkIndex)
			{
				u8 sourceLinkIndex = (u8)hierSrc->articulatedCollider->GetLinkFromComponent(physicsLOD->GetGroup(linkIndexToFirstGroupIndex[linkIndex])->GetChildFragmentIndex());
				const phArticulatedBodyPart& sourceLink = hierSrc->body->GetLink(sourceLinkIndex);
				phArticulatedBodyPart* newLink = rage_new phArticulatedBodyPart(sourceLink);

				// Since we only joints and links are always added as pairs (other than the root link)
				//   we can find the joint whose child is a given link by subtracting one from the link index.
				u8 sourceJointIndex = sourceLinkIndex - 1;
				phJoint& sourceJoint = hierSrc->body->GetJoint(sourceJointIndex);
				Assert(sourceJoint.GetChildLinkIndex() == sourceLinkIndex);

				// TODO: Write a nice clone function for joints
				phJoint* newJoint = NULL;
				switch(sourceJoint.GetJointType())
				{
				case phJoint::JNT_1DOF:
					{
						newJoint = rage_new phJoint1Dof(static_cast<phJoint1Dof&>(sourceJoint));
						break;
					}
				case phJoint::JNT_3DOF:
					{
						newJoint = rage_new phJoint3Dof(static_cast<phJoint3Dof&>(sourceJoint));
						break;
					}
				case phJoint::PRISM_JNT:
					{
						newJoint = rage_new phPrismaticJoint(static_cast<phPrismaticJoint&>(sourceJoint));
						break;
					}
				default:
					{
						fragAssertf(false, "Unrecognized joint type on joint to copy.");
					}
				}
				newJoint->m_Type = sourceJoint.GetType().Clone();

				u8 parentGroupIndex = physicsLOD->GetGroup(linkIndexToFirstGroupIndex[linkIndex])->GetParentGroupPointerIndex();
				Assert(parentGroupIndex != 0xFF);
				m_HierInst.body->AddChild(groupIndexToLinkIndex[parentGroupIndex], *newJoint, *newLink);
				m_HierInst.body->SetMassAndAngInertia(m_HierInst.body->GetNumBodyParts()-1, hierSrc->body->GetMass(sourceLinkIndex), 
					hierSrc->body->GetAngInertia(sourceLinkIndex));
			}

			// We have all of the links and joints now, but the root link doesn't line up with the instance, and the
			//   links have offsets from the old root link.
			// Setting the articulated collider position to the old root link position and then calling recompute link
			//   will safely "move" the new link from the old links position to the current root link position, while
			//   re-orienting it with the instance. 
			// This will also make sure that the mass, angular inertia and CG of the group is correct, which it likely isn't
			//   after breaking.
			InitComponentIndexToLinkIndexTable(groupIndexToLinkIndex);
			m_HierInst.articulatedCollider->SetPosition(hierSrc->articulatedCollider->GetPosition().GetIntrin128());
			RecomputeLink(rootGroupIndex);

			// If this is breaking during the physics update we need to add the collider to the active array. If this
			//   isn't during the physics update then it shouldn't matter because the active array is cleared before
			//   each physics update.
			m_HierInst.articulatedCollider->AddToActiveArray();
		}
		else
		{
			// We don't have a source, just initialize from the pre-processed data
			// Create and clear the arrays of link mass properties
			Vec3V linkAngularInertia[phArticulatedBodyType::MAX_NUM_LINKS];
			ScalarV linkMass[phArticulatedBodyType::MAX_NUM_LINKS];
			sysMemZeroBytes<sizeof(linkAngularInertia)>(linkAngularInertia);
			sysMemZeroBytes<sizeof(linkMass)>(linkMass);

			// Allocate arrays for the child mass properties
			// These are required for two separate group passes, so we might as well store the information
			Vec3V* childCenterOfGravity = Alloca(Vec3V, numChildren);
			Vec3V* childAngularInertia = Alloca(Vec3V, numChildren);
			ScalarV* childMass = Alloca(ScalarV, numChildren);

			if(numLatchedJoints > 0)
			{
				m_HierInst.latchedJoints = rage_new atBitSet(numGroups);
			}

			// Find the total mass and partial angular inertia of each link
			// Store the child mass properties for future use
			for(u8 groupIndex = rootGroupIndex; groupIndex < numGroups; ++groupIndex)
			{
				if(!IsGroupBroken(groupIndex))
				{
					// Loop through the children of this group, adding their mass, center of gravity, and angular inertia to this link's total
					const fragTypeGroup& group = *physicsLOD->GetGroup(groupIndex);
					bool groupDamaged = IsGroupDamaged(groupIndex);
					u8 firstChildIndex = group.GetChildFragmentIndex();
					u8 lastChildIndex = firstChildIndex + group.GetNumChildren();
					for(u8 childIndex = firstChildIndex; childIndex < lastChildIndex; ++childIndex)
					{
						u8 linkIndex = groupIndexToLinkIndex[groupIndex];
						if(groupDamaged)
						{
							physicsLOD->ComputeDamagedChildMassProperties(childIndex, childCenterOfGravity[childIndex], childAngularInertia[childIndex], childMass[childIndex]);
						}
						else
						{
							physicsLOD->ComputeUndamagedChildMassProperties(childIndex, childCenterOfGravity[childIndex], childAngularInertia[childIndex], childMass[childIndex]);
						}
						linkMass[linkIndex] = Add(linkMass[linkIndex],childMass[childIndex]);
						linkAngularInertia[linkIndex] = Add(linkAngularInertia[linkIndex], childAngularInertia[childIndex]);
					}

					// It's possible for the group to be marked as initially latched even with no latched joints
					if(m_HierInst.latchedJoints && group.IsInitiallyLatched() && type->WillBeArticulated(groupIndex))
					{
						m_HierInst.latchedJoints->Set(groupIndex);
					}
				}
			}

			// We aren't breaking off of anything, so we can use the pre-processed data
			Vec3V linkCenterOfGravity[phArticulatedBodyType::MAX_NUM_LINKS];
			linkCenterOfGravity[0] = VECTOR3_TO_VEC3V(physicsLOD->GetRootCGOffset());
			for(u8 linkIndex = 1; linkIndex < numLinks; ++linkIndex)
			{
				linkCenterOfGravity[linkIndex] = physicsLOD->ComputeLinkPositionInInstance(physicsLOD->GetGroup(linkIndexToFirstGroupIndex[linkIndex])->GetChildFragmentIndex());
			}

			// Find the total angular inertia of each link
			for(u8 groupIndex = rootGroupIndex; groupIndex < numGroups; ++groupIndex)
			{
				if(!IsGroupBroken(groupIndex))
				{
					// Loop through this group's children
					// Add the each child's angular inertia to the total link angular inertia
					const fragTypeGroup& group = *physicsLOD->GetGroup(groupIndex);
					u8 linkIndex = groupIndexToLinkIndex[groupIndex];
					u8 firstChildIndex = group.GetChildFragmentIndex();
					u8 lastChildIndex = firstChildIndex + group.GetNumChildren();
					for(u8 childIndex = firstChildIndex; childIndex < lastChildIndex; ++childIndex)
					{
						// Add the angular inertia caused by translation away from the total center of gravity
						linkAngularInertia[linkIndex] = Add(linkAngularInertia[linkIndex], phMathInertia::ComputeTranslationAngularInertia(Subtract(childCenterOfGravity[childIndex], linkCenterOfGravity[linkIndex]), childMass[childIndex]));
					}
				}
			}
			m_HierInst.compositeBound->SetCGOffset(linkCenterOfGravity[0]);

			// Create the articulated body, articulated collider, and initial link
			InitArticulatedRoot(linkAngularInertia[0], linkMass[0]);

			// Add the joints and links to the body
			for(u8 linkIndex = 1; linkIndex < numLinks; ++linkIndex)
			{
				u8 groupIndex = linkIndexToFirstGroupIndex[linkIndex];
				const fragTypeGroup& group = *physicsLOD->GetGroup(groupIndex);
				u8 parentGroupIndex = group.GetParentGroupPointerIndex();
				u8 parentLinkIndex = groupIndexToLinkIndex[parentGroupIndex];
				AddJointAndLink(parentLinkIndex, m_Inst->GetMatrix().GetMat33ConstRef(), linkCenterOfGravity[linkIndex], linkAngularInertia[linkIndex], linkMass[linkIndex], groupIndex);
			}

			InitComponentIndexToLinkIndexTable(groupIndexToLinkIndex);

			// Calculate the inertias of the body
			// If there was a source entry this would be taken care of in RecomputeLink
			m_HierInst.body->CalculateInertias();
		}

		m_HierInst.articulatedCollider->UpdateSavedVelocityArraySizes(true);
		m_HierInst.body->Freeze();
#if __ASSERT
		CheckVelocities();
#endif

		if (m_HierInst.articulatedCollider->GetSleep() == NULL)
		{
			phSleep* sleep = rage_new phSleep(m_HierInst.articulatedCollider);
			m_HierInst.articulatedCollider->SetSleep(sleep);
		}

		// TODO: Move this stuff into InitArticulatedBody
		if(rootGroupIndex == 0)
		{
			// Only fix the root if the root is intact
			float minMoveForce = physicsLOD->GetMinMoveForce();
			if (minMoveForce != 0.0f)
			{
				SetFixedArticulatedRoot(true);
				m_HierInst.body->CalculateInertias();
			}
		}

		// This is normally done while performing collider update, but we might not get an update before we want to apply impulses
		m_HierInst.articulatedCollider->InitAccumJointImpulse();
		m_HierInst.articulatedCollider->FindJointLimitDofs();

		// Calculate the mass and angular inertia of the whole articulated body. The angular inertia would only be valid until the links start moving.
		Vec3V totalAngularInertia(V_ZERO);
		ScalarV totalMass(V_ZERO);
		for(u8 linkIndex = 0; linkIndex < numLinks; ++linkIndex)
		{
			const phArticulatedBodyPart& link = m_HierInst.body->GetLink(linkIndex);
			Vec3V translationAngularInertia = phMathInertia::ComputeTranslationAngularInertia(UnTransformOrtho(m_HierInst.articulatedCollider->GetMatrix().GetMat33ConstRef(), VECTOR3_TO_VEC3V(link.GetPosition())), m_HierInst.body->GetMass(linkIndex));
			
			Mat33V instanceFromWorldRotation;
			Transpose(instanceFromWorldRotation, m_Inst->GetMatrix().GetMat33ConstRef());
			Mat33V worldFromLinkRotation;
			Transpose(worldFromLinkRotation, link.GetMatrix().GetMat33ConstRef());
			Mat33V instanceFromLinkRotation;
			Multiply(instanceFromLinkRotation, instanceFromWorldRotation, worldFromLinkRotation);
			Vec3V rotationAngularInertia = phMathInertia::RotateAngularInertia(instanceFromLinkRotation, m_HierInst.body->GetAngInertia(linkIndex));

			totalAngularInertia = Add(totalAngularInertia, Add(translationAngularInertia, rotationAngularInertia));
			totalMass = Add(totalMass, m_HierInst.body->GetMass(linkIndex));
		}
		m_HierInst.articulatedCollider->SetInertia(totalMass.GetIntrin128(), totalAngularInertia.GetIntrin128());

#if __DEV
		if(hierSrc == NULL && !m_HierInst.groupBroken->AreAnySet() && !m_HierInst.anyGroupDamaged)
		{
			ScalarV tolerance = Max(ScalarV(V_FLT_SMALL_2),Scale(totalMass,ScalarV(V_FLT_SMALL_4)));
			if(!IsCloseAll(ScalarVFromF32(physicsLOD->GetArchetype()->GetMass()),totalMass,tolerance))
			{
				fragWarningf("Mass on pristine cache entry (%5.3f) doesn't match type's mass (%5.3f) on '%s'. Tolerance: %f",totalMass.Getf(),physicsLOD->GetArchetype()->GetMass(),physicsLOD->GetArchetype()->GetFilename(),tolerance.Getf());
			}
			if(!IsCloseAll(VECTOR3_TO_VEC3V(physicsLOD->GetArchetype()->GetAngInertia()),totalAngularInertia,Vec3V(tolerance)))
			{
				fragWarningf("Angular Inertia on pristine cache entry <%5.3f, %5.3f, %5.3f> doesn't match type's angular inertia <%5.3f, %5.3f, %5.3f> on '%s'. Tolerance: %f", VEC3V_ARGS(totalAngularInertia),VEC3V_ARGS(VECTOR3_TO_VEC3V(physicsLOD->GetArchetype()->GetAngInertia())),physicsLOD->GetArchetype()->GetFilename(),tolerance.Getf());
			}
		}
#endif // __DEV

#if HACK_GTA4 // Probably want on DEV branch, testing in Agent first
		m_PhysDamp.SetMassOnly(totalMass.Getf());
		m_PhysDamp.SetAngInertia(RCC_VECTOR3(totalAngularInertia));
#else // HACK_GTA4
		m_PhysDamp.SetMassOnly(m_HierInst.body->GetLink(0)->GetMass().Getf());
		m_PhysDamp.SetAngInertia(RCC_VECTOR3(m_HierInst.body->GetLink(0)->GetAngInertia()));
#endif // HACK_GTA4
		m_HierInst.articulatedCollider->InitInertia();

		if (FRAGTUNE->GetAllowSimplifiedColliders() &&
			m_HierInst.body->GetNumBodyParts() == 1 &&
			!m_HierInst.body->RootIsFixed())
		{
			if (m_HierInst.articulatedCollider->GetType() != phCollider::TYPE_RIGID_BODY)
			{
				m_HierInst.articulatedCollider->SetType(phCollider::TYPE_RIGID_BODY);
#if __PS3
				sysDmaPlan* dmaPlan = rage_new phCollider::DmaPlan;
				dmaPlan->Initialize();
				m_HierInst.articulatedCollider->GenerateDmaPlan(*dmaPlan);
#endif
			}
		}
		else
		{
#if __PS3
			sysDmaPlan* dmaPlan = rage_new phArticulatedCollider::DmaPlan;
			m_HierInst.articulatedCollider->GenerateCoreDmaPlan(*dmaPlan);
#endif
		}

#if __ASSERT && 0
		// Some sanity checking. If any of these fail, weird stuff is going to happen right after activation.
		fragAssertf(IsCloseAll(m_Inst->GetMatrix().GetMat33ConstRef(), m_HierInst.articulatedCollider->GetMatrix().GetMat33ConstRef(), Vec3V(V_FLT_SMALL_3)), "Collider and instance rotation isn't the same at the end of fragCacheEntry::InitArticulated.");
		Mat33V worldFromRootLinkRotation;
		Transpose(worldFromRootLinkRotation, m_HierInst.body->GetLink(0).GetMatrix().GetMat33ConstRef());
		fragAssertf(IsCloseAll(worldFromRootLinkRotation, m_HierInst.articulatedCollider->GetMatrix().GetMat33ConstRef(), Vec3V(V_FLT_SMALL_3)), "Collider and root link rotation isn't the same at the end of fragCacheEntry::InitArticulated.");
		fragAssertf(IsCloseAll(VECTOR3_TO_VEC3V(m_HierInst.body->GetLink(0).GetPosition()), Vec3V(V_ZERO), Vec3V(V_FLT_SMALL_3)), "Root link position isn't zero at end of fragCacheEntry::InitArticulated.");

		Mat34V worldFromInstanceOld = m_Inst->GetMatrix();
		m_HierInst.articulatedCollider->SetInstanceMatrixFromCollider();
		Mat34V worldFromInstanceNew = m_Inst->GetMatrix();
		fragAssertf(IsCloseAll(worldFromInstanceNew,worldFromInstanceOld, Vec3V(V_FLT_SMALL_3)), 
				"Instance matrix is changing during breaking!"
				"\nOld Matrix: "
				"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
				"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
				"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
				"\nNew Matrix: "
				"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
				"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
				"\n\t%5.3f, %5.3f, %5.3f, %5.3f",
				worldFromInstanceOld.GetCol0().GetXf(), worldFromInstanceOld.GetCol1().GetXf(), worldFromInstanceOld.GetCol2().GetXf(), worldFromInstanceOld.GetCol3().GetXf(),
				worldFromInstanceOld.GetCol0().GetYf(), worldFromInstanceOld.GetCol1().GetYf(), worldFromInstanceOld.GetCol2().GetYf(), worldFromInstanceOld.GetCol3().GetYf(),
				worldFromInstanceOld.GetCol0().GetZf(), worldFromInstanceOld.GetCol1().GetZf(), worldFromInstanceOld.GetCol2().GetZf(), worldFromInstanceOld.GetCol3().GetZf(),
				worldFromInstanceNew.GetCol0().GetXf(), worldFromInstanceNew.GetCol1().GetXf(), worldFromInstanceNew.GetCol2().GetXf(), worldFromInstanceNew.GetCol3().GetXf(),
				worldFromInstanceNew.GetCol0().GetYf(), worldFromInstanceNew.GetCol1().GetYf(), worldFromInstanceNew.GetCol2().GetYf(), worldFromInstanceNew.GetCol3().GetYf(),
				worldFromInstanceNew.GetCol0().GetZf(), worldFromInstanceNew.GetCol1().GetZf(), worldFromInstanceNew.GetCol2().GetZf(), worldFromInstanceNew.GetCol3().GetZf());

		for(u8 groupIndex = rootGroupIndex; groupIndex < numGroups; ++groupIndex)
		{
			if(!IsGroupBroken(groupIndex))
			{
				// Map all the children in this group to this group's link
				const fragTypeGroup& group = *m_Inst->GetTypePhysics()->GetGroup(groupIndex);
				u8 firstChildIndex = group.GetChildFragmentIndex();
				u8 lastChildIndex = firstChildIndex + group.GetNumChildren();
				for(u8 childIndex = firstChildIndex; childIndex < lastChildIndex; ++childIndex)
				{
					u8 linkIndex = groupIndexToLinkIndex[groupIndex];
					Mat34V instanceFromChildNew;
					instanceFromChildNew = m_HierInst.body->GetLink(linkIndex).GetMatrix();

					// Transpose the body part matrix, to go from the articulated body's column vectors to Rage's row vectors.
					Transpose3x3( instanceFromChildNew, instanceFromChildNew );

					if (m_HierInst.articulatedCollider->GetLinkAttachmentMatrices())
					{
						const Matrix34& linkAttachment = (m_HierInst.articulatedCollider->GetLinkAttachmentMatrices()[childIndex]);
						RC_MATRIX34(instanceFromChildNew).DotFromLeft(linkAttachment);
					}
					else
					{
						Vector3 centroidOffset;
						centroidOffset = VEC3V_TO_VECTOR3(m_HierInst.compositeBound->GetBound(childIndex)->GetCentroidOffset());
						RC_MATRIX34(instanceFromChildNew).Transform3x3(centroidOffset);
						RC_MATRIX34(instanceFromChildNew).d.Subtract(centroidOffset);
					}

					// Add the collider position, to make the body part matrix in world space.
					instanceFromChildNew.SetCol3( instanceFromChildNew.GetCol3() + m_HierInst.articulatedCollider->GetMatrix().GetCol3() );

					// Transform the body part matrix into the instance's coordinate system.
					RC_MATRIX34(instanceFromChildNew).DotTranspose(RCC_MATRIX34(m_Inst->GetMatrix()));

					Mat34V instanceFromChildOld;
					if(hierSrc)
					{
						instanceFromChildOld = hierSrc->compositeBound->GetCurrentMatrix(childIndex);
					}
					else
					{
						// If the user is setting component matrices of this inst before initializing it (car wheels in GTA5)
						//   then we will end up overwriting their changes. This means we test against the original instance-from-child
						//   matrix.
						Transform(instanceFromChildOld, physicsLOD->GetCompositeBounds()->GetCurrentMatrix(childIndex));
					}
					fragAssertf(IsCloseAll(instanceFromChildNew,instanceFromChildOld, Vec3V(V_FLT_SMALL_3)), 
						"Component matrix %i on link %i is changing during breaking!"
						"\nOld Matrix: "
						"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
						"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
						"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
						"\nNew Matrix: "
						"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
						"\n\t%5.3f, %5.3f, %5.3f, %5.3f"
						"\n\t%5.3f, %5.3f, %5.3f, %5.3f",
						childIndex, linkIndex,
						instanceFromChildOld.GetCol0().GetXf(), instanceFromChildOld.GetCol1().GetXf(), instanceFromChildOld.GetCol2().GetXf(), instanceFromChildOld.GetCol3().GetXf(),
						instanceFromChildOld.GetCol0().GetYf(), instanceFromChildOld.GetCol1().GetYf(), instanceFromChildOld.GetCol2().GetYf(), instanceFromChildOld.GetCol3().GetYf(),
						instanceFromChildOld.GetCol0().GetZf(), instanceFromChildOld.GetCol1().GetZf(), instanceFromChildOld.GetCol2().GetZf(), instanceFromChildOld.GetCol3().GetZf(),
						instanceFromChildNew.GetCol0().GetXf(), instanceFromChildNew.GetCol1().GetXf(), instanceFromChildNew.GetCol2().GetXf(), instanceFromChildNew.GetCol3().GetXf(),
						instanceFromChildNew.GetCol0().GetYf(), instanceFromChildNew.GetCol1().GetYf(), instanceFromChildNew.GetCol2().GetYf(), instanceFromChildNew.GetCol3().GetYf(),
						instanceFromChildNew.GetCol0().GetZf(), instanceFromChildNew.GetCol1().GetZf(), instanceFromChildNew.GetCol2().GetZf(), instanceFromChildNew.GetCol3().GetZf());
				}
			}
		}
#endif // __ASSERT
	}
}


inline void fragCacheEntry::InitConstrained()
{
	// Check to see if this should become a constrained collider
	Assert(m_Inst);
	
	const fragType* type = m_Inst->GetType();
	Assert(type);

	int limitDof;
	Vector3 constrainDir, rotMin, rotMax;
	bool rootLinkConstraint = type->GetRootLinkConstraint(RCC_MATRIX34(m_Inst->GetMatrix()), limitDof, constrainDir, rotMin, rotMax);
	Vector3 constrainDirGlobal = constrainDir;
	RCC_MATRIX34(m_Inst->GetMatrix()).Transform3x3(constrainDirGlobal);
	if (rootLinkConstraint && m_HierInst.groupBroken->IsClear(0))
	{
		// The root link is constrained.  Depending on whether or not there is further articulation in the object, we're either going to add a constraint to
		//   the root link (if it is articulated) or make it a constrained collider (if it's not articulated).
		if (m_HierInst.articulatedCollider || m_HierInst.GetInstBehavior() )
		{
			phConstraintSpherical::Params constraint;
			constraint.instanceA = m_Inst;
			constraint.worldPosition = m_Inst->GetPosition();
			PHCONSTRAINT->Insert(constraint);
		}
		else
		{
			// Find the soft rotation limits.
			fragTypeChild *rootChild = m_Inst->GetTypePhysics()->GetChild(0);
			const fragTypeGroup &rootGroup = *m_Inst->GetTypePhysics()->GetGroup(rootChild->GetOwnerGroupPointerIndex());
			Vector3 softRotationLimitMin(ORIGIN),softRotationLimitMax(ORIGIN);
			if (limitDof == 2)
			{
				// This is a 2-dof joint.
				if(constrainDir.x == 0.0f)
				{
					softRotationLimitMin.x = rootGroup.GetMinSoftAngle1();
					softRotationLimitMax.x = rootGroup.GetMaxSoftAngle1();
					if(constrainDir.y == 0.0f)
					{
						softRotationLimitMin.y = -rootGroup.GetMaxSoftAngle2();
						softRotationLimitMax.y = rootGroup.GetMaxSoftAngle2();
					}
					else
					{
						softRotationLimitMin.z = -rootGroup.GetMaxSoftAngle2();
						softRotationLimitMax.z = rootGroup.GetMaxSoftAngle2();
					}
				}
				else
				{
					softRotationLimitMin.Set(0.0f, rootGroup.GetMinSoftAngle1(), -rootGroup.GetMaxSoftAngle2());
					softRotationLimitMax.Set(0.0f, rootGroup.GetMaxSoftAngle1(), rootGroup.GetMaxSoftAngle2());
				}
			}
			else if (limitDof == 3)
			{
				// With 3-dof joints we just consider the angles to x, y and z limits respectively.
				softRotationLimitMin.Set(-rootGroup.GetMaxSoftAngle1(), -rootGroup.GetMaxSoftAngle2(), -rootGroup.GetMaxSoftAngle3());
				softRotationLimitMax.Set(rootGroup.GetMaxSoftAngle1(), rootGroup.GetMaxSoftAngle2(), rootGroup.GetMaxSoftAngle3());
			}

			// Increase the maximum angular speed.
			m_PhysDamp.SetMaxAngSpeed(4.0f*DEFAULT_MAX_ANG_SPEED);

			Scalar rotationLimitMin = rotMin.DotV(constrainDir);
			Scalar rotationLimitMax = rotMax.DotV(constrainDir);
			// Special case of 1dof intended to be freely rotating about that axis
			if (limitDof == 1 && (rotationLimitMax.x < rotationLimitMin.x))
			{
				phConstraintHinge::Params constraint;
				constraint.instanceA = m_Inst;
				constraint.worldAnchor = m_Inst->GetPosition();
				constraint.worldAxis = RCC_VEC3V(constrainDirGlobal);
				PHCONSTRAINT->Insert(constraint);
			}
			else
			{
				switch (limitDof)
				{
					case 1:
					{
						phConstraintHinge::Params constraint;
						constraint.instanceA = m_Inst;
						constraint.worldAnchor = m_Inst->GetPosition();
						constraint.worldAxis = RCC_VEC3V(constrainDirGlobal);
						constraint.minLimit = rotationLimitMin.x;
						constraint.maxLimit = rotationLimitMax.x;
						constraint.minSoftLimit = rootGroup.GetMinSoftAngle1() * rotationLimitMin.x;
						constraint.maxSoftLimit = rootGroup.GetMaxSoftAngle1() * rotationLimitMin.y;
						constraint.softLimitStrength = rootGroup.GetJointRestoringStrength();
						PHCONSTRAINT->Insert(constraint);
						break;
					}

					case 2:
					case 3:
					{
						phConstraintSpherical::Params constraint;
						constraint.instanceA = m_Inst;
						constraint.worldPosition = m_Inst->GetPosition();

						// limits not yet implemented for spherical constraints
 						//constraint.limitCenter = constrainDirGlobal;
 						//constraint.limit = rotMax;
						//constraint.softLimit = softRotationLimitMin;
						//constraint.softLimitScale = rootGroup.GetJointRestoringStrength();
						break;
					}

					default:
					{
						break;
					}
				}
			}
		}
	}
}

#if ENABLE_FRAGMENT_EVENTS
inline void fragCacheEntry::InitEvents()
{
	Assert(m_Inst);
	
	const fragType* type = m_Inst->GetType();
	Assert(type);

	m_Flags &= ~IS_PLAYING_EVENTS;

	int numChildren = m_Inst->GetTypePhysics()->GetNumChildren();
	m_ChildrenWithEvents = rage_new int[numChildren];
	m_NumChildrenWithEvents = 0;

	for (int childIndex = 0; childIndex < numChildren; ++childIndex)
	{
		// We've got a fragment child ...
		const fragTypeChild *curChild = m_Inst->GetTypePhysics()->GetChild(childIndex);
		const int curGroupIndex = curChild->GetOwnerGroupPointerIndex();
		if (m_HierInst.groupBroken->IsClear(curGroupIndex))
		{
			fragTypeChild* child = m_Inst->GetTypePhysics()->GetChild(childIndex);
			Assert(child);

			evtSet* continuousEvents = child->GetContinuousEventset();
			if (continuousEvents && continuousEvents->GetNumInstances() > 0)
			{
				m_ChildrenWithEvents[m_NumChildrenWithEvents++] = childIndex;

				fragContinuousEventPlayer* player = rage_new fragContinuousEventPlayer;
				m_HierInst.continuousPlayers[childIndex] = player;

				Matrix34 drawingMatrix = RCC_MATRIX34(m_Inst->GetMatrix());

                crSkeleton* skeleton = m_HierInst.skeleton;

                if (type->GetDamagedDrawable() && m_HierInst.damagedSkeleton &&
                    m_HierInst.groupInsts[curGroupIndex].IsDamaged()
                    )
                {
                    skeleton = m_HierInst.damagedSkeleton;
                }

				if (skeleton)
				{
					skeleton->GetGlobalMtx(type->GetBoneIndexFromID(m_Inst->GetTypePhysics()->GetChild(childIndex)->GetBoneID()), RC_MAT34V(drawingMatrix));
				}

				player->CreateParameterList();

				player->GetMatrix() = RCC_MAT34V(drawingMatrix);
				player->GetFragInst() = m_Inst;
                player->GetInst() = (phInst*)m_Inst;
				//					player->evtParamList()->GetValue(fragContinuousEventPlayer::PARAM_INSTANCE_PTR).SetValue(m_Inst);
				//					player->evtParamList()->GetValue(fragContinuousEventPlayer::PARAM_PART_NUM).SetValue(childIndex);
				//					player->evtParamList()->GetValue(fragContinuousEventPlayer::PARAM_PART_ANGLE).SetValue(0.0f);

				player->SetSet(*continuousEvents);
				//player->Start(0.0f);
			}
		}
	}
}
#endif // ENABLE_FRAGMENT_EVENTS

inline void fragCacheEntry::InitSelfCollisions()
{
	Assert(m_Inst);

	// High LOD humans can copy the collision set stored in the NM manager, which happens in fragInstNM::PrepareForActivation()
	if (m_Inst->GetType()->GetARTAssetID() >= 0 && m_Inst->GetCurrentPhysicsLOD() == fragInst::RAGDOLL_LOD_HIGH)
	{
		return;
	}

	if (m_HierInst.body)
	{
		const fragPhysicsLOD* physicsLOD = m_Inst->GetTypePhysics();
		phArticulatedCollider* collider = m_HierInst.articulatedCollider;

		Assert(collider->GetSelfCollisionPairsArrayRefA().GetCapacity() == 0);
		Assert(collider->GetSelfCollisionPairsArrayRefB().GetCapacity() == 0);
		collider->GetSelfCollisionPairsArrayRefA().Reserve(256);
		collider->GetSelfCollisionPairsArrayRefB().Reserve(256);
		collider->SetOwnsSelfCollisionElements(true);

		// Loop over all the self collisions and set them on the collider
		for (int sc = 0; sc < physicsLOD->GetNumSelfCollisions(); ++sc)
		{
			if (physicsLOD->GetSelfCollisionA(sc) < physicsLOD->GetNumChildGroups() &&
				physicsLOD->GetSelfCollisionB(sc) < physicsLOD->GetNumChildGroups())
			{
				fragTypeGroup* groupA = physicsLOD->GetGroup(physicsLOD->GetSelfCollisionA(sc));
				fragTypeGroup* groupB = physicsLOD->GetGroup(physicsLOD->GetSelfCollisionB(sc));

				if (groupA != groupB)
				{
					// Loop over all the groups of each self collision, and set every child of
					// each to collide with every child of the other
					for (u8 gai = 0; gai < groupA->GetNumChildren(); ++gai)
					{
						for (u8 gbi = 0; gbi < groupB->GetNumChildren(); ++gbi)
						{
							collider->SetPartsCanCollide(groupA->GetChildFragmentIndex() + gai, groupB->GetChildFragmentIndex() + gbi, true, false);
						}
					}
				}
			}
		}
		rage::phArticulatedCollider::FinalizeSettingPartsCanCollide(collider->GetSelfCollisionPairsArrayRefA(), collider->GetSelfCollisionPairsArrayRefB());
	}
}

void fragCacheEntry::ResetSelfCollisions()
{
	// We allocate all the active fragment instance data from a chunk of memory inside the cache entry, using the miniHeap
	fragMemStartCacheHeap();

	if (m_HierInst.articulatedCollider)
	{
		m_HierInst.articulatedCollider->GetSelfCollisionPairsArrayRefA().Reset();
		m_HierInst.articulatedCollider->GetSelfCollisionPairsArrayRefB().Reset();
	}

	// Stop special allocator
	fragMemEndCacheHeap();
}

void fragCacheEntry::Init(
						  const fragType*		type,
						  const Matrix34&       matrix,
						  fragInst*				inst,
						  u16                   groupIndex,
						  const crSkeleton*     skeleton,
						  fragCacheEntry*		entrySrc)
{
	Assert(this);
	Assert(!IsInit());
	Assert(!IsStreamingIn());
	Assert(m_Inst == NULL);
	SetStreamingIn(true);

	if (inst)
	{
		m_Inst = inst;

		// If they give us an inst, it had better be consistent with the other parameters they passed us.
		Assert(inst->m_Type == type);
		//Assert(inst->GetType() == NULL || inst->GetArchetype() == inst->GetType()->GetArchetype(currentLOD));

		// In this case, all setting of archetype flags is
	}
	else
	{
		// If hierSrc != NULL, then we're breaking off from a user object.

		// Reset the data in the inst that the user may have set for the previous cache entry
		m_BackingInst->PrepareForReuse();

		m_Inst = m_BackingInst;

		m_BackingInst->SetMatrix(RCC_MAT34V(matrix));

		m_BackingInst->m_InstanceXFormSphere.SetV4(Vec4V(RCC_VEC3V(matrix.d), m_BackingInst->m_InstanceXFormSphere.GetRadius()));
		m_BackingInst->m_Type = type;

		Vector3 eulers;
		matrix.ToEulersZYX(eulers);

		m_BackingInst->m_PackedEulers = PackEulersTo32(eulers);
	}

	// Make sure we don't get recycled right away
	SetSequenceNumber();

	// I'm not dead yet!
	m_Inst->SetDead(false);

	m_ShouldUseInstanceToArtColliderOffsetMat = true;

	m_IncreaseMinStiffnessOnActivation = false;

	m_BoneIndexToComponentMap = NULL;
	m_ComponentToBoneIndexMap = NULL;

	if (m_Inst->GetType() != NULL)
	{
		FastAssert(m_Inst->GetTypePhysics()->GetNumChildren() > 0);
		FastAssert(m_Inst->GetTypePhysics()->GetNumChildGroups() > 0);
		FastAssert(m_Inst->GetType()->GetSkeletonData().GetNumBones() > 0);

		// Our fragType is paged in, so let's finish off initialization.
		InitInternal(groupIndex, skeleton, entrySrc);
		// At this point, our archetype has got the type and include flags that come along with this fragType.
		// That not too bad but, if we're breaking, it would be even better to inherit the flags of the archetype
		//   of the instance from which we're breaking (in case it's cached and they had made any local changes
		//   to it).  Beyond that, it would be even better to give the instance from which we are breaking a chance
		//   to make further modifications to the archetype flags of this instance (for example, a vehicle might
		//   want to clear the flag that says that a given object is a vehicle since the broken piece shouldn't be
		//   considered a vehicle).  We can't do this right now because we don't have any way of getting at our
		//   source instance.
	}
	else
	{
		// Our fragType isn't paged in yet so we'll have to delay full initialization until it is.
		Assert(entrySrc == NULL); // We can only delay initialization if we're creating a pristine fragment

		m_InitGroupIndex = groupIndex;
		m_InitSkeleton = skeleton;
	}

	// Tell our instance who it's cache entry is.
	m_Inst->SetCacheEntry(this);
}

#define FRAG_REPORT_CACHE_INIT 0

void fragCacheEntry::InitInternal(u16					groupIndex,
								  const crSkeleton*		skeleton,
								  fragCacheEntry*		entrySrc)
{
	PF_FUNC(InitInternal);

	const fragType* type = m_Inst->GetType();

#if FRAG_REPORT_CACHE_INIT
	if (type)
	{
		fragDisplayf("Allocating cache entry for instance of type '%s'", type->GetBaseName());
	}
#endif // FRAG_REPORT_CACHE_INIT

	// We can't be initializing from both another cache entry AND a separate skeleton (the fragHierarchyInst has a skeleton that we will use).
	Assert(skeleton == NULL || entrySrc == NULL);

	if (IsStreamingIn())
	{
		// The data for this fragment cache entry is now available for us to use, let's set ourselves up.
		Assert(!IsInit());

		m_HierInst.envCloth = NULL;
		//m_HierInst.parentInst.Reset(); <== better, but need CL5079214 on RDR branch before we have Reset
		m_HierInst.parentInst.SetLevelIndexAndGenerationId(0xffffffff);
		m_HierInst.SetInstBehavior( NULL );
		m_HierInst.latchedJoints = NULL;
        m_HierInst.rootLatchBroken = false;
        m_HierInst.drawExtraDrawable = -1;
        m_HierInst.damagedSkeleton = NULL;
		m_HierInst.glassInfos = NULL;
		m_HierInst.numGlassInfos = 0;
		m_HierInst.rootLinkChildIndex = m_Inst->GetTypePhysics()->GetGroup(groupIndex == 0xFF ? 0 : groupIndex)->GetChildFragmentIndex();

		fragHierarchyInst* hierSrc = entrySrc ? entrySrc->GetHierInst() : NULL;
        m_HierInst.age = hierSrc ? hierSrc->age : 0.0f;

#if ENABLE_FRAGMENT_EVENTS
		// These need to be initialized before InitPhysics because it might call AdjustForLostGroups which depends on them
		m_NumChildrenWithEvents = 0;
		m_ChildrenWithEvents = NULL;
#endif // ENABLE_FRAGMENT_EVENTS

		// We allocate all the active fragment instance data from a chunk of memory inside the cache entry, using the miniHeap
		fragMemStartCacheHeap();

		InitSkeleton();
		InitBitsets(hierSrc);
		InitBound(hierSrc);
		InitSkeleton2(hierSrc, skeleton);
		InitComponentAndBoneIndexMaps();
		InitPhysics(hierSrc, groupIndex, entrySrc ? (entrySrc->m_Flags & CLONED_BOUND_PARTS) != 0 : false);		

#if __RESOURCECOMPILER
		InitEnvCloth(hierSrc);
#endif

#if FRAGMENT_LAZY_ARTICULATED_CACHE
        if (!(hierSrc && hierSrc->body))
		{
			if (WillBeSimpleArticulated() && FRAGTUNE->GetAllowSimplifiedColliders())
			{
				phArticulatedCollider* articulatedCollider = rage_new phArticulatedCollider;
				articulatedCollider->SetType(phCollider::TYPE_RIGID_BODY);
				articulatedCollider->SetInstanceAndReset(m_Inst);
				phSleep* sleep = rage_new phSleep(articulatedCollider);
				articulatedCollider->SetSleep(sleep);
#if __PS3
				sysDmaPlan* dmaPlan = rage_new phCollider::DmaPlan;
				dmaPlan->Initialize();
				articulatedCollider->GenerateDmaPlan(*dmaPlan);
#endif // __PS3
				m_HierInst.articulatedCollider = articulatedCollider;
			}
		}
		else
#endif // FRAGMENT_LAZY_ARTICULATED_CACHE
        {
            InitArticulated(hierSrc);
            InitSelfCollisions();
        }

		InitConstrained();
#if ENABLE_FRAGMENT_EVENTS
		InitEvents();
#endif // ENABLE_FRAGMENT_EVENTS

		size_t totalSize = fragMemEndCacheHeap();
#if !__RESOURCECOMPILER
		InitEnvCloth(hierSrc);
#endif
		type->ReportCacheSize(totalSize);

#if ENABLE_FRAGMENT_EVENTS
		if (m_Flags & PLAY_EVENTS_AT_START)
		{
			StartEvents();
		}
#endif // ENABLE_FRAGMENT_EVENTS

		phInstBehavior* behavior = m_HierInst.GetInstBehavior();
		if( behavior && (m_Inst->GetLevelIndex() != phInst::INVALID_INDEX) )
		{ 							
			PHSIM->AddInstBehavior( *behavior );
		}

		//ask fragment inst stub to make us a unique key...
		fragCacheKey insertKey;
		m_Inst->MakeKey(&insertKey);

		//search for the key first, since there may be others based off the same original instance...
		fragCacheKey* searchKey = FRAGCACHEMGR->GetFragHash()->Search(&insertKey);

		if (searchKey == NULL)
		{
			searchKey = FRAGCACHEMGR->GetFragHash()->Insert(&insertKey);
			Assert(searchKey); //we should never fail to insert a new key...
		}

		//indicate that there is now one(more) owner...
		searchKey->IncRefCount();

		SetStreamingIn(false);
		SetInit(true);
	}
}

void fragCacheEntry::FinishInit()
{
	InitInternal(m_InitGroupIndex, m_InitSkeleton);
}

#if FRAGMENT_LAZY_ARTICULATED_CACHE
void fragCacheEntry::LazyArticulatedInit()
{
	if (m_HierInst.body == NULL)
    {
        fragMemStartCacheHeap();

		InitArticulated(NULL);
		InitSelfCollisions();

        size_t totalSize = fragMemEndCacheHeap();
		const fragType* type = m_Inst->GetType();
        type->ReportArticulatedCacheSize(totalSize - type->GetEstimatedCacheSize());
    }
	else
	{
		RefreshArticulated();
	}
}
#endif // FRAGMENT_LAZY_ARTICULATED_CACHE

class HitBreakableGlassCallback
{
public:
	HitBreakableGlassCallback(Vec3V_In worldImpactPos, Vec3V_In worldImpactImpulse, int glassCrackType)
		: m_worldImpactPos(worldImpactPos)
		, m_worldImpactImpulse(worldImpactImpulse)
		  ,m_glassCrackType(glassCrackType)
	  {
	  }

	  bgGlassImpactFunc GetGlassImpactFunc() const
	  {
		  bgGlassImpactFunc impactFunc;
		  impactFunc.Bind(this, &rage::HitBreakableGlassCallback::HitGlass);
		  return impactFunc;
	  }

private:
	void HitGlass(bgGlassHandle in_handle)
	{
		bgGlassManager::HitGlass(in_handle, m_glassCrackType, m_worldImpactPos, m_worldImpactImpulse);
	}

	Vec3V m_worldImpactPos;
	Vec3V m_worldImpactImpulse;
	int m_glassCrackType;
};

void fragCacheEntry::ShatterGlassOnBone(int groupIndex, int boneIndex, Vec3V_In position, Vec3V_In impact, int glassCrackType)
{
	HitBreakableGlassCallback hitGlassFunc(position, impact, glassCrackType);
	CreateBreakableGlassOnBone(groupIndex, boneIndex, hitGlassFunc.GetGlassImpactFunc());

	GLASS_RECORDER_ONLY(if(glassRecordingRage::IsActive()) { glassRecordingRage::Get()->OnCreateBreakableGlass(GetInst(), groupIndex, boneIndex, position, impact, glassCrackType); })
}

void fragCacheEntry::PurgeBreakableGlassInfos(const fragInst::ComponentBits& groups)
{
	// Clean up any glass infos attached to broken groups
	for(int glassInfoIndex = 0; glassInfoIndex < m_HierInst.numGlassInfos; ++glassInfoIndex)
	{
		if(groups.IsSet(m_HierInst.glassInfos[glassInfoIndex].groupIndex))
		{
			bgGlassHandle handle = m_HierInst.glassInfos[glassInfoIndex].handle;
			if (bgGlassHandle_Invalid != handle)
			{
				bgGlassManager::DestroyBreakableGlass(handle);
			}
			m_HierInst.glassInfos[glassInfoIndex].handle = bgGlassHandle_Invalid;
		}
	}
}

static const int MAX_NUM_ACTIVE_GLASS_PIECES_PER_INST = 32;
void fragCacheEntry::AllocateGlassInfo()
{
	if (m_HierInst.glassInfos == NULL)
	{
		m_HierInst.glassInfos = rage_new fragHierarchyInst::GlassInfo[MAX_NUM_ACTIVE_GLASS_PIECES_PER_INST];
	}
}
void fragCacheEntry::CreateBreakableGlassOnBone(int groupIndex, int boneIndex, bgGlassImpactFunc impactFunc)
{
	Assert(m_Inst);
	

	// Make sure we have a high LOD in our common drawable
	const fragType* type = m_Inst->GetType();
	const fragTypeGroup& group = *m_Inst->GetTypePhysics()->GetGroup(groupIndex);

	RAGE_TRACK(BreakableGlass);
	AllocateGlassInfo();
	// First look for old glass info to recycle
	int glassInfoIndex;
	for (glassInfoIndex = 0; glassInfoIndex < m_HierInst.numGlassInfos; glassInfoIndex++)
	{
		if (m_HierInst.glassInfos[glassInfoIndex].handle == bgGlassHandle_Invalid)
		{
			break;
		}
	}
	if (glassInfoIndex == MAX_NUM_ACTIVE_GLASS_PIECES_PER_INST)
	{
		fragWarningf("fragCacheEntry has no available glass info slot to break glass into\n");
		return;
	}
	if (glassInfoIndex == m_HierInst.numGlassInfos)
	{
		// not recycling, so bump the 'in use' count
		m_HierInst.numGlassInfos++;
	}

	fragHierarchyInst::GlassInfo& glassInfo = m_HierInst.glassInfos[glassInfoIndex];
	glassInfo.handle = bgGlassHandle_Invalid;
	glassInfo.groupIndex = u16(groupIndex);

	Mat34V glassMatrix;
	m_HierInst.skeleton->GetGlobalMtx(boneIndex, glassMatrix);
	Assert(RCC_MATRIX34(glassMatrix).IsOrthonormal());
	Mat34V glassAttachment = *m_HierInst.skeleton->GetParentMtx();
	Assert(RCC_MATRIX34(glassAttachment).IsOrthonormal());
	UnTransformOrtho(glassInfo.matrix, glassAttachment, glassMatrix);
	Assert(RCC_MATRIX34(glassInfo.matrix).IsOrthonormal());

	bgPaneModelInfoBase* pModelInfo;
	pModelInfo = type->GetAllGlassPaneModelInfos()[group.GetGlassPaneModelInfoIndex()];

	const fragTypeChild* child = m_Inst->GetTypePhysics()->GetChild(group.GetChildFragmentIndex());
	const phMaterialMgr::Id materialId = child->GetUndamagedEntity()->GetBound()->GetMaterialId(0);
	bgGetInstanceDataFunc getInstanceDataFunc;
	getInstanceDataFunc.Bind(const_cast<fragType*>(m_Inst->GetType()), &rage::fragType::GetInstanceDataForShader);
	bgGlassManager::CreateBreakableGlass(
		&glassInfo.handle,
		glassMatrix,
		*pModelInfo,
		getInstanceDataFunc,
		m_Inst,
		materialId,
		child->GetUndamagedMass(),
		group.GetChildFragmentIndex());

	if (bgGlassHandle_Invalid != glassInfo.handle)
	{
		impactFunc(glassInfo.handle);
	}
}


void fragCacheEntry::UninitAllGlass()
{
	// if we never had any glass, no need to do anything
	if (NULL == m_HierInst.glassInfos)
	{
		Assert(0 == m_HierInst.numGlassInfos);
		return;
	}

	for (int glassIndex = 0; glassIndex < m_HierInst.numGlassInfos; ++glassIndex)
	{
		bgGlassHandle handle = m_HierInst.glassInfos[glassIndex].handle;
		if (bgGlassHandle_Invalid != handle)
		{
			bgGlassManager::DestroyBreakableGlass(handle);
		}
	}	

	delete [] m_HierInst.glassInfos;
	m_HierInst.glassInfos = NULL;
	m_HierInst.numGlassInfos = 0;
}

#if ENABLE_FRAGMENT_EVENTS
void fragCacheEntry::StartEvents()
{
	Assert(m_Inst);

	m_Flags |= IS_PLAYING_EVENTS;

	for (int eventChildIndex = 0; eventChildIndex < m_NumChildrenWithEvents; ++eventChildIndex)
	{
		// We've got a fragment child ...
		int childIndex = m_ChildrenWithEvents[eventChildIndex];
		const fragTypeChild *curChild = m_Inst->GetTypePhysics()->GetChild(childIndex);
		const int curGroupIndex = curChild->GetOwnerGroupPointerIndex();
		if (m_HierInst.groupBroken->IsClear(curGroupIndex))
		{
			fragTypeChild* child = m_Inst->GetTypePhysics()->GetChild(childIndex);
			Assert(child);

			evtSet* continuousEvents = child->GetContinuousEventset();
			if (continuousEvents && continuousEvents->GetNumInstances() > 0)
			{
				m_HierInst.continuousPlayers[childIndex]->Start(0.0f);
			}
		}
	}
}

void fragCacheEntry::StopEvents()
{
	Assert(m_Inst);

	m_Flags = m_Flags & ~IS_PLAYING_EVENTS;

	for (int eventChildIndex = 0; eventChildIndex < m_NumChildrenWithEvents; ++eventChildIndex)
	{
		// We've got a fragment child ...
		int childIndex = m_ChildrenWithEvents[eventChildIndex];
		const fragTypeChild *curChild = m_Inst->GetTypePhysics()->GetChild(childIndex);
		const int curGroupIndex = curChild->GetOwnerGroupPointerIndex();
		if (m_HierInst.groupBroken->IsClear(curGroupIndex))
		{
			fragTypeChild* child = m_Inst->GetTypePhysics()->GetChild(childIndex);
			Assert(child);

			evtSet* continuousEvents = child->GetContinuousEventset();
			if (continuousEvents && continuousEvents->GetNumInstances() > 0)
			{
				m_HierInst.continuousPlayers[childIndex]->Stop(1.0f);
			}
		}
	}
}
#endif // ENABLE_FRAGMENT_EVENTS


__forceinline void fragCacheEntry::UpdateAge(float timeStep)
{
	const fragType* type = m_Inst->GetType();
	fragAssertf(type, "fragType is 0x00000000, most likely is not streamed in yet ?!");

	if (m_HierInst.age < UNBROKEN_AGE && m_HierInst.age + timeStep >= UNBROKEN_AGE && type && !m_Inst->GetTypePhysics()->GetUnbrokenCGOffset().IsZero() && m_Inst->IsInLevel())
	{
		m_HierInst.age += timeStep;

		Vector3 cgOffset = VEC3V_TO_VECTOR3(m_CompositeBound->GetCGOffset());
		cgOffset.Subtract(m_Inst->GetTypePhysics()->GetUnbrokenCGOffset());
		m_CompositeBound->SetCGOffset(RCC_VEC3V(cgOffset));

		if (phCollider* collider = PHSIM->GetCollider(m_Inst))
		{
			collider->SetColliderMatrixFromInstance();
		}

		// Get and set the mass so as to fix the inertia
		float mass = GetMass(&m_HierInst);
		Assert(mass > 0.0f);
		SetMass(mass);
	}
	else
	{
		m_HierInst.age += timeStep;
	}
}

#if ENABLE_FRAGMENT_EVENTS
__forceinline void fragCacheEntry::UpdateEvents()
{
	if (m_Flags & IS_PLAYING_EVENTS)
	{
		Assert(m_Inst);
		

		const fragType* type = m_Inst->GetType();
		fragAssertf(type, "fragType is 0x00000000, most likely is not streamed in yet ?!");

		if (type)
		{
// WIP: supplying angle information to event system
//		if (m_HierInst.collider != NULL && m_HierInst.body == NULL)
//		{
//			if (fragContinuousEventPlayer* rootPlayer = m_HierInst.continuousPlayers[0])
//			{
//				phConstrainedCollider* collider = static_cast<phConstrainedCollider*>(m_HierInst.collider);
//				Quaternion orientation;
//				float angle = collider->GetRotation(orientation);
//				rootPlayer->GetAngle() = angle;
//			}
//		}

			for (int eventChildIndex = 0; eventChildIndex < m_NumChildrenWithEvents; ++eventChildIndex)
			{
				int childIndex = m_ChildrenWithEvents[eventChildIndex];
				if (eventChildIndex + 1 < m_NumChildrenWithEvents)
				{
					if (fragContinuousEventPlayer* continuousPlayer = m_HierInst.continuousPlayers[m_ChildrenWithEvents[eventChildIndex + 1]])
					{
						PrefetchDC(continuousPlayer);
					}
				}

				// We've got a fragment child ...
				if (fragContinuousEventPlayer* player = m_HierInst.continuousPlayers[childIndex])
				{
					Matrix34 drawingMatrix = RCC_MATRIX34(m_Inst->GetMatrix());

					if (crSkeleton* skeleton = m_HierInst.skeleton)
					{
						skeleton->GetGlobalMtx(type->GetBoneIndexFromID(m_Inst->GetTypePhysics()->GetChild(childIndex)->GetBoneID()), RC_MAT34V(drawingMatrix));
					}

					player->GetMatrix() = RCC_MAT34V(drawingMatrix);

					if (!(m_HierInst.childOmitEventUpdate->IsSet(childIndex)))
					{
	#if __ASSERT
						const evtSet* events = player->GetEventSet();
						for (int eventIndex = 0; eventIndex < events->GetNumInstances(); ++ eventIndex)
						{
							const fragSnuffEventInstance* snuffEvent = dynamic_cast<const fragSnuffEventInstance*>(events->GetInstance(eventIndex));
							fragAssertf(!snuffEvent, "You can't put a snuff event on a continuous event player...you probably want it on a collision event or a break event");
						}
	#endif // __ASSERT

						player->Update(0.0f);
					}
				}
			}
		}
	}
}
#endif // ENABLE_FRAGMENT_EVENTS

__forceinline void fragCacheEntry::UpdateGlassBreakables(float timeStep)
{
	int numGlassInfos = m_HierInst.numGlassInfos;
	if (numGlassInfos > 0)
	{
		const fragType* type = m_Inst->GetType();
		fragAssertf(type, "fragType is 0x00000000, most likely is not streamed in yet ?!");
		if( type )
		{
			Mat34V glassAttachment;
			const Mat34V* pGlassAttachment = m_HierInst.skeleton->GetParentMtx();
			Assert(pGlassAttachment);
			glassAttachment = *pGlassAttachment;

			for (int glassIndex = 0; glassIndex < numGlassInfos; ++glassIndex)
			{
				fragHierarchyInst::GlassInfo& glassInfo = m_HierInst.glassInfos[glassIndex];
				if (bgGlassHandle_Invalid != glassInfo.handle)
				{
					Mat34V glassMatrix;
					Transform(glassMatrix, glassAttachment, glassInfo.matrix);
					Assert(RCC_MATRIX34(glassMatrix).IsOrthonormal());
					bgGlassManager::UpdateBreakableGlass(
						glassInfo.handle,
						glassMatrix,
						timeStep);
				}
			}
		}
	}
}


void fragCacheEntry::Update (float timeStep, int simUpdates)
{
	fragAssert(m_Inst);
	//#if __DEV // Making this safety check apply to release builds, to improve stability
	if(!m_Inst)
	{
		return;
	}
	//#endif

	fragAssertf(m_Inst->GetCacheEntry() == this, "%p/%p", m_Inst->GetCacheEntry(), this);
	fragAssert(IsInit());
	fragAssert(m_PhysDamp.GetBound()->GetType() == phBound::COMPOSITE);

#if USE_VIRTUAL_UPDATE
	m_Inst->Update();
#endif

	UpdateInstabilityPrevention(simUpdates);

	UpdateAge(timeStep);
#if ENABLE_FRAGMENT_EVENTS
	UpdateEvents();
#endif // ENABLE_FRAGMENT_EVENTS
	UpdateGlassBreakables(timeStep);
}


void fragCacheEntry::UpdateInstabilityPrevention(int simUpdates)
{
	// Update instability prevention
	if (m_Inst && m_HierInst.body && m_InstabilityPreventionFrames >= 0)
	{
		m_InstabilityPreventionFrames -= (s8)simUpdates;

		// If the prevention period has ended, revert to default values
		if (m_InstabilityPreventionFrames < 0)
		{
			ClearInstabilityPrevention();
		}
		else if (!m_InstabilityPreventionInitialized)
		{
			InitializeInstabilityPrevention();
			m_InstabilityPreventionInitialized =  true;
		}
	}
}

void fragCacheEntry::InitializeInstabilityPrevention()
{
	static float minStiff = 0.8f;
	m_HierInst.body->SetBodyMinimumStiffness(minStiff);
	m_HierInst.body->SetStiffness(minStiff);
	m_Inst->SetInstFlag(phInst::FLAG_OPTIONAL_ITERATIONS, true);
}

void fragCacheEntry::ClearInstabilityPrevention()
{
	m_HierInst.body->SetBodyMinimumStiffness(0.1f);
	m_Inst->SetInstFlag(phInst::FLAG_OPTIONAL_ITERATIONS, false);
}

void fragCacheEntry::ExchangeType(const fragType* type)
{
	Assert(m_Inst);
	

	// skeleton
	crSkeletonData* sd = NULL;
	if (m_HierInst.skeleton) {
		if (fragDrawable* drawable = type->GetCommonDrawable()) {
			sd = drawable->GetSkeletonData();

			if (sd) {
				// We've got bones and we have data! (in our drawable's skeleton data).  We need to tell it to use a different
				// skeleton's data, even though its the same damn thing.
				m_HierInst.skeleton->Init(*sd, m_HierInst.skeleton->GetParentMtx());
			}
		}
	}

	// compositebound
	if (m_HierInst.compositeBound) {
		phBoundComposite* orgBounds = m_Inst->GetTypePhysics()->GetCompositeBounds();

		// Copy the set of bounds into our phBoundComposite..
		phLevelNew::CompositeBoundSetActiveBoundsThreadSafe(*m_HierInst.compositeBound,*orgBounds);
		m_HierInst.compositeBound->CalculateCompositeExtents(false);

		if(m_CompositeBound->HasBVHStructure())
		{
			if(m_Inst->IsInLevel())
			{
				PHLEVEL->RebuildCompositeBvh(m_Inst->GetLevelIndex());
			}
			else
			{
				m_HierInst.compositeBound->UpdateBvh(true);
			}
		}
	}

	// articulatedcollider
	if (m_HierInst.articulatedCollider && m_HierInst.body) {
		m_HierInst.articulatedCollider->SetLinkAttachment(m_Inst->GetTypePhysics()->GetLinkAttachmentArray());
	}

	// animation
	// 	if (m_HierInst.animation)
	// 	StringNormalize(normalized, realInst.m_AnimName, 64);
	//
	// 	fragDrawable* drawable = m_HierInst.type->GetCommonDrawable();
	// 	int animCount = drawable->GetAnimCount();
	//
	// 	for (int animIndex = 0; animIndex < animCount; ++animIndex)
	// 	{
	// 		fragAnimation* anim = drawable->GetAnimation(animIndex);
	// 		Assert(anim);
	//
	// 		if (stricmp(normalized, anim->Name) == 0)
	// 		{
	// 			hierInst->animation = anim;
	// 		}
	// 	}

	// type
	m_HierInst.type = type;
}

} // namespace rage
