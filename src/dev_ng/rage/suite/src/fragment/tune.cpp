// 
// fragment/tune.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tune.h"

// fragment
#include "cachemanager.h"
#include "drawable.h"
#include "eventplayer.h"
#include "manager.h"
#include "tune_parser.h"
#include "type.h"
#include "typechild.h"
#include "typegroup.h"
#include "typecloth.h"

// RAGE
#include "bank/bank.h"
#include "bank/combo.h"
#include "crskeleton/skeletondata.h"
#include "diag/output.h"
#include "event/manager.h"
#include "event/set.h"
#include "file/asset.h"
#include "file/default_paths.h"
#include "file/remote.h"
#include "input/mouse.h"
#include "input/keys.h"
#include "parser/manager.h"
#include "phcore/segment.h"
#include "physics/collider.h"
#include "physics/colliderdispatch.h"
#include "physics/intersection.h"
#include "physics/iterator.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/exec.h"

#include <functional>

#define DEBUG_FRAG_TUNE_STRUCT 0
#define DEBUG_DISPLAY_COMBO_NAME 0

FRAGMENT_OPTIMISATIONS()

namespace rage {

#if __DEV
	PARAM(FragGlobalMaxDrawingDistance,"[fragment] force the global max drawing distance to the passed in value");
#endif

//XPARAM( fraggeo );


#if __BANK
static fragTypeGroup s_CopiedGroup;
static bool s_CopiedArticulated = false;
static bool s_CopiedArticulatedPrismatic = false;
static bool s_CopiedArticulated1DOF = false;
static bool s_CopiedArticulated3DOF = false;

static bool s_CopyGroupStrength = true;
static bool s_CopyGroupForceTransmissionScaleUp = true;
static bool s_CopyGroupForceTransmissionScaleDown = true;
static bool s_CopyGroupJointStiffness = true;
static bool s_CopyGroupMinSoftAngle1 = true;
static bool s_CopyGroupMaxSoftAngle1 = true;
static bool s_CopyGroupMaxSoftAngle2 = true;
static bool s_CopyGroupMaxSoftAngle3 = true;
static bool s_CopyGroupRotationSpeed = true;
static bool s_CopyGroupRotationStrength = true;
static bool s_CopyGroupRestoringStrength = true;
static bool s_CopyGroupRestoringMaxTorque = true;
static bool s_CopyGroupLatchStrength = true;
static bool s_CopyGroupDisappearsWhenDeadFlag = true;
static bool s_CopyGroupDamageWhenBrokenFlag = true;
static bool s_CopyDoesntAffectVehicles = true;
static bool s_CopyDoesntPushVehiclesDown = true;
static bool s_CopyGroupMinDamageForce = true;
static bool s_CopyGroupDamageHealth = true;
static bool s_CopyGroupWeaponHealth = true;
static bool s_CopyGroupWeaponScale = true;
static bool s_CopyGroupMeleeScale = true;
static bool s_CopyGroupVehicleScale = true;
static bool s_CopyGroupPedScale = true;
static bool s_CopyGroupRagdollScale = true;
static bool s_CopyGroupExplosionScale = true;
static bool s_CopyGroupObjectScale = true;
static bool s_CopyGroupPedInvMassScale = true;
static bool s_CopyGroupPresetApplied = true;
#endif // __BANK

XPARAM(nofragarticulation);
PARAM(nofragsimplification,"[fragment] Don't allow articulated fragment objects to become rigid");
PARAM(fragdebug,"[fragment] Print debugging information to the terminal about fragments");
PARAM(fragdebug2,"[fragment] Print debugging information to the terminal about fragments");

////////////////////////////////////////////////////////////////////////////////

fragTuneBreakPreset::fragTuneBreakPreset() : m_Name(""), m_Strength(-1.0f), m_ForceTransmissionUp(0.1f), m_ForceTransmissionDown(0.1f) {}

////////////////////////////////////////////////////////////////////////////////

fragTuneStruct::fragTuneStruct()
	: LodDistanceBias(0.0f)
	, GlobalMaxDrawingDistance(250.0f)
	, GlobalForceTransmissionScaleDown(1.0f)
	, GlobalForceTransmissionScaleUp(1.0f)
	, GlobalRootBreakHealthScale(0.1f)
	, GlobalBreakHealthScale(0.008f)
	, GlobalDamageHealthScale(2.0f)
	, BreakingFrameRateLimit(30.0f)
	, EnableCompleteImpulsePropagation(true)
	, AggressiveAssetTouching(false)
	, AllowArticulation(true)
	, AllowSimplifiedColliders(true)
	, FragDebug1(false)
	, FragDebug2(false)
	, CollisionDamageScale(1.0f)
	, CollisionDamageMinThreshold(0.1f)
	, CollisionDamageMaxThreshold(0.5f)

	, ReferenceMassForDefaultTuning(100.0f)
	, ForcedTuningPath("$/tune/types/fragments")
	, Bank(NULL)
	, TypeBank(NULL)
	, TypeCombo(NULL)
    , TypeTotalUndamagedMass(0.0f)
	, TypeBreakPresetSelection(0)

	, ComboSelection(0)
	, TruncateCount(0)
	, NumComboItems(0)
	, MaxNumComboItems(2048)
    , ComponentSelection(0)
	, CurrentFragType(NULL)
	, HoverFragType(NULL)
	, CurrentFragInstLevelIndex(phInst::INVALID_INDEX)
	, CurrentFragInstGenerationId(phInst::INVALID_INDEX)
	, ComboTypes(NULL)
	, m_RecreateWidgets(false)
	, m_RegenerateTuningWidgets(false)
#if __BANK
	, LeftClickSelectEnabled(false)
	, InstanceTuningEnabled(true)
	, SelectWholeTypeEnabled(false)
	, DebugDrawSelectedFrag(true)
	, WakeAllAffectedFragsOnEdit(true)
#endif
{
	m_Mapper.Reset();
	m_Mapper.Map(IOMS_KEYBOARD,		KEY_LMENU,				m_SelectUnderMouse);
	m_Mapper.Map(IOMS_MOUSE_BUTTON, ioMouse::MOUSE_LEFT,	m_MouseButton);

	int fragDebug1;
	if (PARAM_fragdebug.Get(fragDebug1))
	{
		FragDebug1 = (fragDebug1 != 0);
	}

	int fragDebug2;
	if (PARAM_fragdebug.Get(fragDebug2))
	{
		FragDebug2 = (fragDebug2 != 0);
	}

	NumComboItems = 0;
	ComboSelection = 0;
	ComboTypes = rage_new fragType*[MaxNumComboItems];

	for(int i=0; i < phInstBreakable::MAX_NUM_BREAKABLE_COMPONENTS; i++)
	{
		m_DamageHealthProxies[i] = 0.0f;
		m_WeaponHealthProxies[i] = 0.0f;
	}

	if (PARAM_nofragarticulation.Get())
	{
		AllowArticulation = false;
	}

	if (PARAM_nofragsimplification.Get())
	{
		AllowSimplifiedColliders = false;
	}

#if __BANK
	Damping.SetChild(0,LinearDamping);
	LinearDamping.SetChild(0,LinearSpeedDamping);
	LinearSpeedDamping.SetChild(0,LinearSpeedXDamping);
	LinearSpeedDamping.SetChild(1,LinearSpeedYDamping);
	LinearSpeedDamping.SetChild(2,LinearSpeedZDamping);
	LinearDamping.SetChild(1,LinearVelocityDamping);
	LinearVelocityDamping.SetChild(0,LinearVelocityXDamping);
	LinearVelocityDamping.SetChild(1,LinearVelocityYDamping);
	LinearVelocityDamping.SetChild(2,LinearVelocityZDamping);
	LinearDamping.SetChild(2,LinearVelocitySquaredDamping);
	LinearVelocitySquaredDamping.SetChild(0,LinearVelocitySquaredXDamping);
	LinearVelocitySquaredDamping.SetChild(1,LinearVelocitySquaredYDamping);
	LinearVelocitySquaredDamping.SetChild(2,LinearVelocitySquaredZDamping);
	Damping.SetChild(1,AngularDamping);
	AngularDamping.SetChild(0,AngularSpeedDamping);
	AngularSpeedDamping.SetChild(0,AngularSpeedXDamping);
	AngularSpeedDamping.SetChild(1,AngularSpeedYDamping);
	AngularSpeedDamping.SetChild(2,AngularSpeedZDamping);
	AngularDamping.SetChild(1,AngularVelocityDamping);
	AngularVelocityDamping.SetChild(0,AngularVelocityXDamping);
	AngularVelocityDamping.SetChild(1,AngularVelocityYDamping);
	AngularVelocityDamping.SetChild(2,AngularVelocityZDamping);
	AngularDamping.SetChild(2,AngularVelocitySquaredDamping);
	AngularVelocitySquaredDamping.SetChild(0,AngularVelocitySquaredXDamping);
	AngularVelocitySquaredDamping.SetChild(1,AngularVelocitySquaredYDamping);
	AngularVelocitySquaredDamping.SetChild(2,AngularVelocitySquaredZDamping);
#endif // __BANK
}

fragTuneStruct::~fragTuneStruct()
{
	for (int i=0; i<NumComboItems; i++)
	{
		SAFE_REMOVE_KNOWN_REF(ComboTypes[i]);
	}
	delete [] ComboTypes;
}

void fragTuneStruct::Load(const char* filename)
{
	if (filename)
	{
		PARSER.LoadObject(filename, "xml", *this);
	}
	else
	{
		ASSET.PushFolder(ForcedTuningPath);
		PARSER.LoadObject("fragment", "xml", *this);
		ASSET.PopFolder();
	}
}

class BaseNamePred : public std::binary_function<const fragType*, const fragType*, bool>
{
public:
	bool operator()(const fragType* left, const fragType* right) const
	{
		return strcmp(left->GetBaseName(), right->GetBaseName()) < 0;
	}
};

#if __BANK
void fragTuneStruct::UpdateMouseSelection(const grcViewport* viewport, u32 probeIncludeFlags, u32 probeExcludeFlags)
{
	if(!LeftClickSelectEnabled)
	{
		HoverFragType = 0;
		return;
	}

    if (viewport == NULL)
    {
        viewport = grcViewport::GetCurrent();
    }

    if (viewport == NULL)
    {
        // Can't get a good viewport.
        return;
    }

	if ((LeftClickSelectEnabled || m_SelectUnderMouse.IsDown()) && m_MouseButton.IsPressed())
    {
        m_PressedMouseX = ioMouse::GetX();
        m_PressedMouseY = ioMouse::GetY();
    }

	// Determine the type under the cursor, regardless of if we are selecting it, so that we 
	// can render the 'hover' indicator on it.
	Vector3 mouseScreen, mouseFar;
	//please remove this const_cast after we pick up the proper version of rage.
	const_cast<grcViewport*>(viewport)->ReverseTransform(	static_cast<float>(ioMouse::GetX()),
		static_cast<float>(ioMouse::GetY()),
		RC_VEC3V(mouseScreen),
		RC_VEC3V(mouseFar));

	Vector3 direction;
	direction.Subtract(mouseFar, mouseScreen);
	direction.Normalize();

	Vector3 segA, segB;
	segA = mouseScreen;
	segB.AddScaled(mouseScreen, direction, 1000.0f);
	phSegment segment;
	segment.Set(segA, segB);

	phIntersection isect;
	HoverFragType = NULL;
	if (PHLEVEL->TestProbe(segment, &isect, 0, probeIncludeFlags, (u32)-1, phLevelBase::STATE_FLAGS_ALL, 1, probeExcludeFlags) > 0)
	{
		// A direct line of sight probe at the mouse icon location hit something.
		Assert(isect.GetInstance() && isect.GetInstance()->GetLevelIndex()!=phInst::INVALID_INDEX);

		if (fragInst* inst = dynamic_cast<fragInst*>(isect.GetInstance()))
		{
			HoverFragType = const_cast<fragType*>(inst->GetType());
			if (m_MouseButton.IsReleased())
			{
				if (m_PressedMouseX == ioMouse::GetX() && m_PressedMouseY == ioMouse::GetY())
				{
					SetType(HoverFragType, isect.GetComponent(), inst);
				}
			}
		}
	}
}

void fragTuneStruct::SetType(fragType* selectedType, int componentSelection, fragInst* selectedInst)
{
	int currentLOD = 0;

	if (selectedType)
	{
		if (selectedInst && sm_SelectEntityFunctor.IsBound())
		{
			sm_SelectEntityFunctor(selectedInst->GetUserData());
		}

		fragType** found = std::lower_bound(&ComboTypes[0], &ComboTypes[NumComboItems], selectedType, BaseNamePred());
		ComboSelection = static_cast<int>(found - ComboTypes + 1);

		CurrentFragType = selectedType;

		Vector3 linearSpeedDamping = CurrentFragType->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::LINEAR_C];
		Vector3 linearVelocityDamping = CurrentFragType->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::LINEAR_V];
		Vector3 linearVelocitySquaredDamping = CurrentFragType->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::LINEAR_V2];
		Vector3 angularSpeedDamping = CurrentFragType->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::ANGULAR_C];
		Vector3 angularVelocityDamping = CurrentFragType->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::ANGULAR_V];
		Vector3 angularVelocitySquaredDamping = CurrentFragType->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::ANGULAR_V2];

		LinearSpeedXDamping.Initialize(linearSpeedDamping.x);
		LinearSpeedYDamping.Initialize(linearSpeedDamping.y);
		LinearSpeedZDamping.Initialize(linearSpeedDamping.z);
		LinearVelocityXDamping.Initialize(linearVelocityDamping.x);
		LinearVelocityYDamping.Initialize(linearVelocityDamping.y);
		LinearVelocityZDamping.Initialize(linearVelocityDamping.z);
		LinearVelocitySquaredXDamping.Initialize(linearVelocitySquaredDamping.x);
		LinearVelocitySquaredYDamping.Initialize(linearVelocitySquaredDamping.y);
		LinearVelocitySquaredZDamping.Initialize(linearVelocitySquaredDamping.z);
		AngularSpeedXDamping.Initialize(angularSpeedDamping.x);
		AngularSpeedYDamping.Initialize(angularSpeedDamping.y);
		AngularSpeedZDamping.Initialize(angularSpeedDamping.z);
		AngularVelocityXDamping.Initialize(angularVelocityDamping.x);
		AngularVelocityYDamping.Initialize(angularVelocityDamping.y);
		AngularVelocityZDamping.Initialize(angularVelocityDamping.z);
		AngularVelocitySquaredXDamping.Initialize(angularVelocitySquaredDamping.x);
		AngularVelocitySquaredYDamping.Initialize(angularVelocitySquaredDamping.y);
		AngularVelocitySquaredZDamping.Initialize(angularVelocitySquaredDamping.z);
		Damping.InitializeFromLeaves();

		ComponentSelection = componentSelection;
		if(SelectWholeTypeEnabled || ComponentSelection > selectedType->GetPhysics(currentLOD)->GetNumChildren())
		{
			ComponentSelection = 0;
		}

		if(selectedInst)
		{
			Assertf(selectedInst->GetLevelIndex() != phInst::INVALID_INDEX, "fragTuneStruct::SetType: is not in the level");
			u16 levelIndexA = selectedInst->GetLevelIndex();
			CurrentFragInstLevelIndex = levelIndexA;
			CurrentFragInstGenerationId = PHLEVEL->GetGenerationID(levelIndexA);
		}
		else
		{
			CurrentFragInstLevelIndex = phInst::INVALID_INDEX;
			CurrentFragInstGenerationId = phInst::INVALID_INDEX;
		}

		if(ComboTypes[ComboSelection-1] != selectedType)
		{
			RefreshTypeList();
		}

		m_RecreateWidgets = true; 
	}
	else
	{
		// If it reaches this point, the selection is for a type that no longer exists.
		ComboSelection = 0;
		ComponentSelection = 0;
		CurrentFragType = NULL;
		CurrentFragInstLevelIndex = phInst::INVALID_INDEX;
		CurrentFragInstGenerationId = phInst::INVALID_INDEX;

		if ( TypeBank)
		{	
			TypeBank->Destroy();
		}
	}
}

#endif

void fragTuneStruct::Update(const grcViewport*  BANK_ONLY(viewport), bool BANK_ONLY(updateMouseSelection))
{
	sysCriticalSection criticalSection(m_TunerCriticalSectionToken);

#if DEBUG_DISPLAY_COMBO_NAME
	const char* name = "(none)";
	if((ComboSelection > 0) && ComboTypes[ComboSelection-1])
	{
		name = ComboTypes[ComboSelection-1]->GetBaseName();
	}
	const char* name2 = "(none)";
	if(CurrentFragType)
	{
		name2 = CurrentFragType->GetBaseName();
	}
	Displayf("fragTuneStruct: ComboTypes[%d]=%08x '%s' component=%d CurrentFragType=%08x '%s'", ComboSelection-1, name, ComponentSelection, CurrentFragType, name2);
#endif

#if __BANK
	int currentLOD = 0;

	m_Mapper.Update();

	if(updateMouseSelection)
	{
		UpdateMouseSelection(viewport);
	}

	if (m_RegenerateTuningWidgets)
	{
		m_RegenerateTuningWidgets = false;
		RegenerateTuningWidgets();
	}

	if (InstanceTuningEnabled && CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (!PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			CurrentFragInstLevelIndex = phInst::INVALID_INDEX;
			CurrentFragInstGenerationId = phInst::INVALID_INDEX;
			m_RecreateWidgets = true;
		}
		else
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			if (fragCacheEntry* entry = selectedInst->GetCacheEntry())
			{
				fragGroupInst* groupInsts = entry->GetHierInst()->groupInsts;
				for (int groupIndex = 0; groupIndex < selectedInst->GetType()->GetPhysics(currentLOD)->GetNumChildGroups(); ++groupIndex)
				{
					m_DamageHealthProxies[groupIndex] = groupInsts[groupIndex].damageHealth;
					m_WeaponHealthProxies[groupIndex] = groupInsts[groupIndex].weaponHealth;
				}
			}
			else
			{
				for (int groupIndex = 0; groupIndex < selectedInst->GetType()->GetPhysics(currentLOD)->GetNumChildGroups(); ++groupIndex)
				{
					m_DamageHealthProxies[groupIndex] = selectedInst->GetType()->GetPhysics(currentLOD)->GetGroup(groupIndex)->GetDamageHealth();
					m_WeaponHealthProxies[groupIndex] = selectedInst->GetType()->GetPhysics(currentLOD)->GetGroup(groupIndex)->GetWeaponHealth();
				}
			}

			if (CurrentFragInstCached != selectedInst->GetCached())
			{
				m_RecreateWidgets = true;

				CurrentFragInstCached = selectedInst->GetCached();
			}
		}
	}

	if (m_RecreateWidgets)
	{
		m_RecreateWidgets = false;

		if ( TypeBank)
		{	
			TypeBank->Destroy();
		}

		RegenerateTuningWidgets();
	}
#endif // __BANK
}

void fragTuneStruct::ApplyBreakPresetToGroup(fragTypeGroup& group, int preset)
{
	if (preset >= 0 && preset < m_BreakPresets.GetCount())
	{
		fragTuneBreakPreset& breakPreset = m_BreakPresets[preset];

		group.m_Strength = breakPreset.m_Strength;
		group.m_ForceTransmissionScaleUp = breakPreset.m_ForceTransmissionUp;
		group.m_ForceTransmissionScaleDown = breakPreset.m_ForceTransmissionDown;
		group.m_MinDamageForce = breakPreset.m_MinDamageForce;
		group.m_DamageHealth = breakPreset.m_DamageHealth;
		group.m_WeaponHealth = breakPreset.m_WeaponHealth;
		group.m_WeaponScale = breakPreset.m_WeaponScale;
		group.m_MeleeScale = breakPreset.m_MeleeScale;
		group.m_VehicleScale = breakPreset.m_VehicleScale;
		group.m_PedScale = breakPreset.m_PedScale;
		group.m_RagdollScale = breakPreset.m_RagdollScale;
		group.m_ExplosionScale = breakPreset.m_ExplosionScale;
		group.m_ObjectScale = breakPreset.m_ObjectScale;

		group.m_PresetApplied = preset + 1;
	}
}

int fragTuneStruct::FindPresetByName(const char* presetName)
{
	for (int i = 0; i < m_BreakPresets.GetCount(); ++i)
	{
		if (stricmp(presetName, m_BreakPresets[i].m_Name) == 0)
		{
			return i;
		}
	}

	return -1;
}

const char* fragTuneStruct::GetNameForPreset(int preset)
{
	if (preset >= 0 && preset < m_BreakPresets.GetCount())
	{
		return m_BreakPresets[preset].m_Name;
	}
	else
	{
		return "<none>";
	}
}


#if __BANK

void fragTuneStruct::ApplyArchetypeBreakPresetCallback(fragType* type)
{
	int currentLOD = 0;
	for (int groupIndex = 0; groupIndex < type->GetPhysics(currentLOD)->GetNumChildGroups(); ++groupIndex)
	{
		fragTypeGroup& group = *type->GetPhysics(currentLOD)->GetGroup(groupIndex);
		
		ApplyBreakPresetToGroup(group, TypeBreakPresetSelection);
	}
}

void fragTuneStruct::ResetGroupPresetCallback(fragTypeGroup* group)
{
	group->m_PresetApplied = 0;
}

void fragTuneStruct::SaveArchetypeCallback(fragType* type)
{
	char assetPath[RAGE_MAX_PATH]={ 0};
	if(sm_PreSaveEntityFunctor.IsBound())
	{
		sm_PreSaveEntityFunctor(assetPath,type);
	}
	if(assetPath[0]==0)
		ASSET.PushFolder(ForcedTuningPath);
	else
		ASSET.PushFolder(assetPath);

	bool saved = false;

	char filename[RAGE_MAX_PATH];
	strcpy(filename, type->GetBaseName());
	ASSET.CreateLeadingPath( filename );

	char fullPath[RAGE_MAX_PATH];
	ASSET.FullPath(fullPath, RAGE_MAX_PATH, filename, "tune");

	const fiDevice *device = fiDevice::GetDevice(fullPath,false);
	if (device != NULL)
	{
		char path[RAGE_MAX_PATH];
		device->FixRelativeName(path, RAGE_MAX_PATH, fullPath);

		char perforceCommand[512];
		formatf(perforceCommand, sizeof(perforceCommand), RS_TOOLSROOT "\\script\\misc\\p4_add_or_edit_frag_tune_file.bat %s", path);
		sysExec(perforceCommand);
	}

	if ( fiStream* S = ASSET.Create( filename, "tune" ) )
	{
		fiAsciiTokenizer T;
		T.Init( filename, S );

		type->SaveASCII( T );

		S->Close();

		saved = true;
	}
#if __ASSERT
	else
	{
		char errorMessage[512];
		formatf(errorMessage, sizeof(errorMessage), "Unable to save the tune file '%s.tune'. Make sure it is checked out in Perforce.", filename);
		fiRemoteShowMessageBox(errorMessage, "Tune file is read-only", MB_ICONERROR | MB_OK | MB_TOPMOST, IDOK);
	}
#endif // __ASSERT

	if ( false != saved )
	{
		Displayf(TBlue"Successfully wrote '%s/%s'.", ForcedTuningPath, filename);

		// Attempt to delete the resource file, so you won't reload next time with the tunings lost
		char resourceFile[RAGE_MAX_PATH];
		formatf(resourceFile, "$/fragments/%s", type->GetBaseName());

		char fullPath[256];
		ASSET.FullPath(fullPath, RAGE_MAX_PATH, resourceFile, "pck");

		int resourcesDeleted = 0;

		if (const fiDevice* device = fiDevice::GetDevice(fullPath, false))
		{
			ASSET.FullPath(fullPath, RAGE_MAX_PATH, resourceFile, "xeck");
			resourcesDeleted += device->Delete(fullPath) ? 1 : 0;

			ASSET.FullPath(fullPath, RAGE_MAX_PATH, resourceFile, "wck");
			resourcesDeleted += device->Delete(fullPath) ? 1 : 0;

			ASSET.FullPath(fullPath, RAGE_MAX_PATH, resourceFile, "pck");
			resourcesDeleted += device->Delete(fullPath) ? 1 : 0;

			ASSET.FullPath(fullPath, RAGE_MAX_PATH, resourceFile, "pspck");
			resourcesDeleted += device->Delete(fullPath) ? 1 : 0;

			ASSET.FullPath(fullPath, RAGE_MAX_PATH, resourceFile, "psnck");
			resourcesDeleted += device->Delete(fullPath) ? 1 : 0;
		}

		Displayf(TBlue"%d resource files deleted", resourcesDeleted);
	}
	else
	{
		Errorf("Unable to save %s/%s, check file permissions", ForcedTuningPath, filename);
	}
	
	ASSET.PopFolder();
}

void fragTuneStruct::SetToDefaultByMass(fragType* type)
{
	int currentLOD = 0;

	float mass = type->GetPhysics(currentLOD)->GetTotalUndamagedMass();
	float ratio = mass / ReferenceMassForDefaultTuning;
	for (int groupIndex = 0; groupIndex < type->GetPhysics(currentLOD)->GetNumChildGroups(); ++groupIndex)
	{
		fragTypeGroup& group = *type->GetPhysics(currentLOD)->GetGroup(groupIndex);

		group.m_MinDamageForce	= 100.0f * ratio;
		group.m_DamageHealth	= 1000.0f * ratio;
		group.m_PresetApplied   = 0;
	}

	for (int groupIndex = 0; groupIndex < type->GetPhysics(currentLOD)->GetNumChildGroups(); ++groupIndex)
	{
		fragTypeGroup& group = *type->GetPhysics(currentLOD)->GetGroup(groupIndex);

		group.m_Strength					= 3.0f * ratio;
		group.m_PresetApplied				= 0;
	}
}

void fragTuneStruct::RepeatFromChild(int index)
{
	int currentLOD = 0;

	fragType* type = GetTypeBeingTuned();
	fragTypeChild& specialChild = *type->GetPhysics(currentLOD)->GetChild(index);

	for (int childIndex = 0; childIndex < type->GetPhysics(currentLOD)->GetNumChildren(); ++childIndex)
	{
		if (index != childIndex)
		{
			fragTypeChild& child = *type->GetPhysics(currentLOD)->GetChild(childIndex);
			child.SetUndamagedMass(specialChild.GetUndamagedMass());
			child.SetDamagedMass(specialChild.GetDamagedMass());
			child.m_Flags			= specialChild.m_Flags;
		}
	}
}

void fragTuneStruct::CopyPasteToAllGroups(int index)
{
	CopyFromGroup(index);

	fragType* type = GetTypeBeingTuned();
	for (int groupIndex = 0; groupIndex < type->GetPhysics(0)->GetNumChildGroups(); ++groupIndex)
	{
		if (index != groupIndex)
		{
			PasteToGroup(groupIndex);
		}
	}
}

void fragTuneStruct::CopyFromGroup(int index)
{
	s_CopiedArticulated = false;
	s_CopiedArticulatedPrismatic = false;
	s_CopiedArticulated1DOF = false;
	s_CopiedArticulated3DOF = false;

	fragType* type = GetTypeBeingTuned();
	s_CopiedGroup = *type->GetPhysics(0)->GetGroup(index);
#if ENABLE_FRAGMENT_EVENTS
	s_CopiedGroup.m_DeathEventPlayer = NULL;
	s_CopiedGroup.m_DeathEventset = NULL;
#endif // ENABLE_FRAGMENT_EVENTS
	fragTypeGroup& specialGroup = s_CopiedGroup;

	if (const crSkeletonData* sd = type->GetCommonDrawable()->GetSkeletonData())
	{
		const crBoneData* bone = sd->GetBoneData(type->GetBoneIndexFromID(type->GetPhysics(0)->GetChild(specialGroup.GetChildFragmentIndex())->GetBoneID()));
		const crJointData* jd = type->GetCommonDrawable()->GetJointData();
		bool hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits;
		if (jd != NULL && type->HasMotionLimits(*bone, *jd, hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits))
		{
			s_CopiedArticulated = true;

			int limitTDof = (hasXTLimits ? 1 : 0) +
				(hasYTLimits ? 1 : 0) +
				(hasZTLimits ? 1 : 0);

			if (limitTDof)
			{
				s_CopiedArticulatedPrismatic = true;
			}
			else
			{
				int limitDof = (hasXLimits ? 1 : 0) +
					(hasYLimits ? 1 : 0) +
					(hasZLimits ? 1 : 0);

				if (limitDof == 1)
				{
					s_CopiedArticulated1DOF = true;
				}
				else
				{
					s_CopiedArticulated3DOF = true;
				}
			}
		}
	}
}

void fragTuneStruct::PasteToGroup(int groupIndex)
{
	fragType* type = GetTypeBeingTuned();

	fragTypeGroup& specialGroup = s_CopiedGroup;

	fragTypeGroup& group = *type->GetPhysics(0)->GetGroup(groupIndex);

	if (s_CopyGroupStrength)						group.m_Strength					= specialGroup.m_Strength;
	if (s_CopyGroupForceTransmissionScaleUp)		group.m_ForceTransmissionScaleUp	= specialGroup.m_ForceTransmissionScaleUp;
	if (s_CopyGroupForceTransmissionScaleDown)		group.m_ForceTransmissionScaleDown	= specialGroup.m_ForceTransmissionScaleDown;
	if (s_CopyGroupDisappearsWhenDeadFlag)
	{
		if (specialGroup.m_Flags & fragTypeGroup::FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD)
		{
			group.m_Flags |= fragTypeGroup::FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD;
		}
		else
		{
			group.m_Flags &= ~fragTypeGroup::FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD;
		}
	}
	if (s_CopyGroupDamageWhenBrokenFlag)
	{
		if (specialGroup.m_Flags & fragTypeGroup::FRAG_GROUP_FLAG_DAMAGE_WHEN_BROKEN)
		{
			group.m_Flags |= fragTypeGroup::FRAG_GROUP_FLAG_DAMAGE_WHEN_BROKEN;
		}
		else
		{
			group.m_Flags &= ~fragTypeGroup::FRAG_GROUP_FLAG_DAMAGE_WHEN_BROKEN;
		}
	}
	if (s_CopyDoesntAffectVehicles)
	{
		if (specialGroup.m_Flags & fragTypeGroup::FRAG_GROUP_FLAG_DOESNT_AFFECT_VEHICLES)
		{
			group.m_Flags |= fragTypeGroup::FRAG_GROUP_FLAG_DOESNT_AFFECT_VEHICLES;
		}
		else
		{
			group.m_Flags &= ~fragTypeGroup::FRAG_GROUP_FLAG_DOESNT_AFFECT_VEHICLES;
		}
	}
	if (s_CopyDoesntPushVehiclesDown)
	{
		if (specialGroup.m_Flags & fragTypeGroup::FRAG_GROUP_FLAG_DOESNT_PUSH_VEHICLES_DOWN)
		{
			group.m_Flags |= fragTypeGroup::FRAG_GROUP_FLAG_DOESNT_PUSH_VEHICLES_DOWN;
		}
		else
		{
			group.m_Flags &= ~fragTypeGroup::FRAG_GROUP_FLAG_DOESNT_PUSH_VEHICLES_DOWN;
		}
	}
	if (s_CopyGroupMinDamageForce)					group.m_MinDamageForce				= specialGroup.m_MinDamageForce;
	if (s_CopyGroupDamageHealth)					group.m_DamageHealth				= specialGroup.m_DamageHealth;
	if (s_CopyGroupWeaponHealth)					group.m_WeaponHealth				= specialGroup.m_WeaponHealth;
	if (s_CopyGroupWeaponScale)						group.m_WeaponScale					= specialGroup.m_WeaponScale;
	if (s_CopyGroupMeleeScale)						group.m_MeleeScale					= specialGroup.m_MeleeScale;
	if (s_CopyGroupVehicleScale)					group.m_VehicleScale				= specialGroup.m_VehicleScale;
	if (s_CopyGroupPedScale)						group.m_PedScale					= specialGroup.m_PedScale;
	if (s_CopyGroupRagdollScale)					group.m_RagdollScale				= specialGroup.m_RagdollScale;
	if (s_CopyGroupExplosionScale)					group.m_ExplosionScale				= specialGroup.m_ExplosionScale;
	if (s_CopyGroupObjectScale)						group.m_ObjectScale					= specialGroup.m_ObjectScale;
	if (s_CopyGroupPedInvMassScale)					group.m_PedInvMassScale				= specialGroup.m_PedInvMassScale;

	if (s_CopyGroupPresetApplied)
	{
		ApplyBreakPresetToGroup(group, specialGroup.m_PresetApplied - 1);
	}

	// Only copy the other data over if the joint type of the target group matches the special group
	if (s_CopiedArticulated)
	{
		if (const crSkeletonData* sd = type->GetCommonDrawable()->GetSkeletonData())
		{
			const crBoneData* bone = sd->GetBoneData(type->GetBoneIndexFromID(type->GetPhysics(0)->GetChild(group.GetChildFragmentIndex())->GetBoneID()));
			const crJointData* jd = type->GetCommonDrawable()->GetJointData();
			bool hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits;
			if (jd != NULL && type->HasMotionLimits(*bone, *jd, hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits))
			{
				if (s_CopyGroupLatchStrength)					group.m_LatchStrength				= specialGroup.m_LatchStrength;

				int limitTDof = (hasXTLimits ? 1 : 0) +
					(hasYTLimits ? 1 : 0) +
					(hasZTLimits ? 1 : 0);

				if (limitTDof)
				{
					if (s_CopiedArticulatedPrismatic)
					{
						if (s_CopyGroupJointStiffness)					group.m_JointStiffness				= specialGroup.m_JointStiffness;
					}
				}
				else
				{
					int limitDof = (hasXLimits ? 1 : 0) +
						(hasYLimits ? 1 : 0) +
						(hasZLimits ? 1 : 0);

					if (limitDof == 1)
					{
						if (s_CopiedArticulated1DOF)
						{
							if (s_CopyGroupJointStiffness)					group.m_JointStiffness				= specialGroup.m_JointStiffness;
							if (s_CopyGroupMinSoftAngle1)					group.m_MinSoftAngle1				= specialGroup.m_MinSoftAngle1;
							if (s_CopyGroupMaxSoftAngle1)					group.m_MaxSoftAngle1				= specialGroup.m_MaxSoftAngle1;
							if (s_CopyGroupRotationSpeed)					group.m_RotationSpeed				= specialGroup.m_RotationSpeed;
							if (s_CopyGroupRotationStrength)				group.m_RotationStrength			= specialGroup.m_RotationStrength;
							if (s_CopyGroupRestoringStrength)				group.m_RestoringStrength			= specialGroup.m_RestoringStrength;
							if (s_CopyGroupRestoringMaxTorque)				group.m_RestoringMaxTorque			= specialGroup.m_RestoringMaxTorque;
						}
					}
					else
					{
						if (s_CopiedArticulated3DOF)
						{
							if (s_CopyGroupJointStiffness)					group.m_JointStiffness				= specialGroup.m_JointStiffness;
							if (s_CopyGroupMaxSoftAngle1)					group.m_MaxSoftAngle1				= specialGroup.m_MaxSoftAngle1;
							if (s_CopyGroupMaxSoftAngle2)					group.m_MaxSoftAngle2				= specialGroup.m_MaxSoftAngle2;
							if (s_CopyGroupMaxSoftAngle3)					group.m_MaxSoftAngle3				= specialGroup.m_MaxSoftAngle3;
							if (s_CopyGroupRestoringStrength)				group.m_RestoringStrength			= specialGroup.m_RestoringStrength;
							if (s_CopyGroupRestoringMaxTorque)				group.m_RestoringMaxTorque			= specialGroup.m_RestoringMaxTorque;
						}
					}
				}
			}
		}
	}
}

void fragTuneStruct::PasteToGroupsRecursive(int index)
{
	fragType* type = GetTypeBeingTuned();

	PasteToGroup(index);

	fragTypeGroup* group = type->GetPhysics(0)->GetGroup(index);

	for (int childIndex = 0; childIndex < group->GetNumChildGroups(); ++childIndex)
	{
		PasteToGroupsRecursive(group->GetChildGroupsPointersIndex() + childIndex);
	}
}

void fragTuneStruct::SelectAllFields()
{
	s_CopyGroupStrength = true;
	s_CopyGroupForceTransmissionScaleUp = true;
	s_CopyGroupForceTransmissionScaleDown = true;
	s_CopyGroupJointStiffness = true;
	s_CopyGroupMinSoftAngle1 = true;
	s_CopyGroupMaxSoftAngle1 = true;
	s_CopyGroupMaxSoftAngle2 = true;
	s_CopyGroupMaxSoftAngle3 = true;
	s_CopyGroupRotationSpeed = true;
	s_CopyGroupRotationStrength = true;
	s_CopyGroupRestoringStrength = true;
	s_CopyGroupRestoringMaxTorque = true;
	s_CopyGroupLatchStrength = true;
	s_CopyGroupDisappearsWhenDeadFlag = true;
	s_CopyGroupDamageWhenBrokenFlag = true;
	s_CopyDoesntAffectVehicles = true;
	s_CopyDoesntPushVehiclesDown = true;
	s_CopyGroupMinDamageForce = true;
	s_CopyGroupDamageHealth = true;
	s_CopyGroupWeaponHealth = true;
	s_CopyGroupWeaponScale = true;
	s_CopyGroupMeleeScale = true;
	s_CopyGroupVehicleScale = true;
	s_CopyGroupPedScale = true;
	s_CopyGroupRagdollScale = true;
	s_CopyGroupExplosionScale = true;
	s_CopyGroupObjectScale = true;
	s_CopyGroupPedInvMassScale = true;
	s_CopyGroupPresetApplied = true;
}

void fragTuneStruct::DeselectAllFields()
{
	s_CopyGroupStrength = false;
	s_CopyGroupForceTransmissionScaleUp = false;
	s_CopyGroupForceTransmissionScaleDown = false;
	s_CopyGroupJointStiffness = false;
	s_CopyGroupMinSoftAngle1 = false;
	s_CopyGroupMaxSoftAngle1 = false;
	s_CopyGroupMaxSoftAngle2 = false;
	s_CopyGroupMaxSoftAngle3 = false;
	s_CopyGroupRotationSpeed = false;
	s_CopyGroupRotationStrength = false;
	s_CopyGroupRestoringStrength = false;
	s_CopyGroupRestoringMaxTorque = false;
	s_CopyGroupLatchStrength = false;
	s_CopyGroupDisappearsWhenDeadFlag = false;
	s_CopyGroupDamageWhenBrokenFlag = false;
	s_CopyDoesntAffectVehicles = false;
	s_CopyDoesntPushVehiclesDown = false;
	s_CopyGroupMinDamageForce = false;
	s_CopyGroupDamageHealth = false;
	s_CopyGroupWeaponHealth = false;
	s_CopyGroupWeaponScale = false;
	s_CopyGroupMeleeScale = false;
	s_CopyGroupVehicleScale = false;
	s_CopyGroupPedScale = false;
	s_CopyGroupRagdollScale = false;
	s_CopyGroupExplosionScale = false;
	s_CopyGroupObjectScale = false;
	s_CopyGroupPedInvMassScale = false;
	s_CopyGroupPresetApplied = false;
}

void fragTuneStruct::AdjustRootCGOffset(fragType* type)
{
	int currentLOD = 0;
	const Vec3V newRootCenterOfGravity = VECTOR3_TO_VEC3V(type->GetPhysics(currentLOD)->GetRootCGOffset());
	type->ComputeMassProperties(currentLOD,&newRootCenterOfGravity);
}

void fragTuneStruct::RestoreComputedCG(fragType* type)
{
	int currentLOD = 0;
	const Vec3V originalRootCenterOfGravity = VECTOR3_TO_VEC3V(type->GetPhysics(currentLOD)->GetOriginalRootCGOffset());
	type->ComputeMassProperties(currentLOD,&originalRootCenterOfGravity);
}
#if __BANK
void fragTuneStruct::AdjustDamping(DampingNode* dampingNode)
{
	fragType* type = CurrentFragType;
	int currentLOD = 0;
	dampingNode->Update();
	type->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::LINEAR_C] = Vector3(LinearSpeedXDamping.Average,LinearSpeedYDamping.Average,LinearSpeedZDamping.Average);
	type->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::LINEAR_V] = Vector3(LinearVelocityXDamping.Average,LinearVelocityYDamping.Average,LinearVelocityZDamping.Average);
	type->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::LINEAR_V2] = Vector3(LinearVelocitySquaredXDamping.Average,LinearVelocitySquaredYDamping.Average,LinearVelocitySquaredZDamping.Average);
	type->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::ANGULAR_C] = Vector3(AngularSpeedXDamping.Average,AngularSpeedYDamping.Average,AngularSpeedZDamping.Average);
	type->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::ANGULAR_V] = Vector3(AngularVelocityXDamping.Average,AngularVelocityYDamping.Average,AngularVelocityZDamping.Average);
	type->GetPhysics(currentLOD)->m_DampingConstant[phArchetypeDamp::ANGULAR_V2] = Vector3(AngularVelocitySquaredXDamping.Average,AngularVelocitySquaredYDamping.Average,AngularVelocitySquaredZDamping.Average);
	type->GetPhysics(currentLOD)->ApplyDamping(type->GetPhysics(currentLOD)->GetArchetype());
}

fragTuneStruct::DampingNode::DampingNode()
{
	memset(Children,0,sizeof(Children));
	Parent = NULL;
	Ratio = 0;
	Average = 0;
}
float fragTuneStruct::DampingNode::ComputeChildRatioAverage()
{
	int numChildren = 0;
	float total = 0;
	for(int i = 0; i < MAX_NUM_DAMPING_CHILDREN; ++i)
	{
		if(Children[i])
		{
			numChildren++;
			total += Children[i]->Ratio;
		}
	}
	return total*InvertSafe((float)numChildren,0);
}
float fragTuneStruct::DampingNode::ComputeChildAverage()
{
	int numChildren = 0;
	float total = 0;
	for(int i = 0; i < MAX_NUM_DAMPING_CHILDREN; ++i)
	{
		if(Children[i])
		{
			numChildren++;
			total += Children[i]->Average;
		}
	}
	return total*InvertSafe((float)numChildren,0);
}

void fragTuneStruct::DampingNode::UpdateFromChildren()
{
	// Update the ratios of all the children of this node
	for(int i = 0; i < MAX_NUM_DAMPING_CHILDREN; ++i)
	{
		if(Children[i])
		{
			Children[i]->Ratio = Children[i]->Average;
		}
	}
	// Compute the average from the children
	Average = ComputeChildAverage();

	// Propagate upwards
	if(Parent)
	{
		Parent->UpdateFromChildren();
	}
}

void fragTuneStruct::DampingNode::SetAverage(float average)
{
	Average = average;
	float childRatioAverage = ComputeChildRatioAverage();
	
	// Set each child's average to this nodes average multiplied by the percent of the ratio the child is
	for(int i = 0; i < MAX_NUM_DAMPING_CHILDREN; ++i)
	{
		if(Children[i])
		{
			Children[i]->SetAverage(Average * (childRatioAverage == 0 ? 1 : Children[i]->Ratio / childRatioAverage));
		}
	}
}

float fragTuneStruct::DampingNode::ComputeMaximumScalingAllowed()
{
	// Find the largest child average
	float maximumChildAverage = 0;
	for(int i = 0; i < MAX_NUM_DAMPING_CHILDREN; ++i)
	{
		if(Children[i])
		{
			maximumChildAverage = Max(maximumChildAverage,Children[i]->Average);
		}
	}

	// Find the scaling that would take the maximum child average to the limit
	float maximumScaling = (maximumChildAverage == 0 ? FLT_MAX : MAX_DAMPING/maximumChildAverage);

	// Use the smallest maximum scaling allowed
	for(int i = 0; i < MAX_NUM_DAMPING_CHILDREN; ++i)
	{
		if(Children[i])
		{
			maximumScaling = Min(maximumScaling,Children[i]->ComputeMaximumScalingAllowed());
		}
	}
	return maximumScaling;
}
void fragTuneStruct::DampingNode::Update()
{
	// Save the average which was set
	float targetAverage = Average;

	// Find the old average
	Average = ComputeChildAverage();

	// If the old average was 0, it should be save to just set the average to the target
	// Otherwise, scale the old average by the minimum of the scaling the user wants, and the maximum scaling allowed
	float newAverage = (Average == 0 ? targetAverage : Average*Min(targetAverage/Average,ComputeMaximumScalingAllowed()));
	SetAverage(newAverage);

	// Update ratios and averages up the tree to the root
	if(Parent)
	{
		Parent->UpdateFromChildren();
	}
	else
	{
		Ratio = Average;
	}
}

void fragTuneStruct::DampingNode::SetChild(int childIndex, DampingNode& node)
{
	Assert(childIndex < MAX_NUM_DAMPING_CHILDREN);
	Children[childIndex] = &node;
	node.Parent = this;
}

void fragTuneStruct::DampingNode::Initialize(float initialValue)
{
	Average = Ratio = initialValue;
}

void fragTuneStruct::DampingNode::InitializeFromLeaves()
{
	for(int i = 0; i < MAX_NUM_DAMPING_CHILDREN; ++i)
	{
		if(Children[i])
		{
			Children[i]->InitializeFromLeaves();
		}
	}
	float average = ComputeChildAverage();
	if(average != 0)
	{
		Initialize(average);
	}
}
#endif // __BANK

void fragTuneStruct::AdjustGravityFactor(fragType* type)
{
	int currentLOD = 0;

	float newGravityFactor = type->m_GravityFactor;

	if(type->GetPhysics(currentLOD)->GetArchetype(true))
	{
		type->GetPhysics(currentLOD)->GetArchetype(true)->SetGravityFactor(newGravityFactor);
	}
	if(type->GetPhysics(currentLOD)->GetArchetype())
	{
		type->GetPhysics(currentLOD)->GetArchetype()->SetGravityFactor(newGravityFactor);
	}

	// Also, change the archetype gravity for all current instances
#if ENABLE_PHYSICS_LOCK
	phIterator iterator(phIterator::PHITERATORLOCKTYPE_WRITELOCK);
#else	// ENABLE_PHYSICS_LOCK
	phIterator iterator;
#endif	// ENABLE_PHYSICS_LOCK
	iterator.InitCull_All();
	iterator.SetStateIncludeFlags(phLevelBase::STATE_FLAG_ACTIVE | phLevelBase::STATE_FLAG_INACTIVE);
	for(u16 index = iterator.GetFirstLevelIndex(PHLEVEL); index != phInst::INVALID_INDEX; index = iterator.GetNextLevelIndex(PHLEVEL))
	{
		fragInst* inst = dynamic_cast<fragInst*>(PHLEVEL->GetInstance(index));
		if(inst && inst->GetType() == type)
		{
			phArchetypePhys* archPhys = dynamic_cast<phArchetypePhys*>(inst->GetArchetype());
			if(archPhys)
			{
				archPhys->SetGravityFactor(newGravityFactor);
				if(WakeAllAffectedFragsOnEdit && !PHLEVEL->IsActive(index))
				{
					PHSIM->ActivateObject(index); // Activate to allow changes to take an effect
				}
			}
		}
	}
}

void fragTuneStruct::AdjustBuoyancyFactor(fragType* type)
{
	int currentLOD = 0;

	float newBuoyancyFactor = type->m_BuoyancyFactor;

	if(type->GetPhysics(currentLOD)->GetArchetype(true))
	{
		type->GetPhysics(currentLOD)->GetArchetype(true)->SetBuoyancyFactor(newBuoyancyFactor);
	}
	if(type->GetPhysics(currentLOD)->GetArchetype())
	{
		type->GetPhysics(currentLOD)->GetArchetype()->SetBuoyancyFactor(newBuoyancyFactor);
	}

	// Also, change the archetype buoyancy for all current instances
#if ENABLE_PHYSICS_LOCK
	phIterator iterator(phIterator::PHITERATORLOCKTYPE_WRITELOCK);
#else	// ENABLE_PHYSICS_LOCK
	phIterator iterator;
#endif	// ENABLE_PHYSICS_LOCK
	iterator.InitCull_All();
	iterator.SetStateIncludeFlags(phLevelBase::STATE_FLAG_ACTIVE | phLevelBase::STATE_FLAG_INACTIVE);
	for(u16 index = iterator.GetFirstLevelIndex(PHLEVEL); index != phInst::INVALID_INDEX; index = iterator.GetNextLevelIndex(PHLEVEL))
	{
		fragInst* inst = dynamic_cast<fragInst*>(PHLEVEL->GetInstance(index));
		if(inst && inst->GetType() == type)
		{
			phArchetypePhys* archPhys = dynamic_cast<phArchetypePhys*>(inst->GetArchetype());
			if(archPhys)
			{
				archPhys->SetBuoyancyFactor(newBuoyancyFactor);
				if(WakeAllAffectedFragsOnEdit && !PHLEVEL->IsActive(index))
				{
					PHSIM->ActivateObject(index); // Activate to allow changes to take an effect
				}
			}
		}
	}
}

void fragTuneStruct::AdjustChildMass(fragTypeGroup* UNUSED_PARAM(group))
{
	// Update the whole fragment since the mass on the child was changed by RAG
	int currentLOD = 0;
    fragType* type = GetTypeBeingTuned();
	type->GetPhysics(currentLOD)->ComputeChildAngularInertias();
	type->ComputeMassProperties(currentLOD);
	TypeTotalUndamagedMass = type->GetPhysics(currentLOD)->GetArchetype()->GetMass();
}

void fragTuneStruct::AdjustGroupMass(fragTypeGroup* group)
{
	// Scale the damaged and undamaged mass of each child by how much the group scaled
	// Should this scale the damaged mass?
	int currentLOD = 0;
    fragType* type = GetTypeBeingTuned();
    float totalUndamagedMass = group->ComputeTotalUndamagedMass(type->GetPhysics(currentLOD));
    float massRatio = group->m_TotalUndamagedMass / totalUndamagedMass;
    for (u8 childIndex = 0; childIndex < group->m_NumChildren; ++childIndex)
    {
        fragTypeChild& child = *type->GetPhysics(currentLOD)->GetChild(childIndex + group->m_ChildIndex);
        child.SetUndamagedMass(child.GetUndamagedMass() * massRatio);
        child.SetDamagedMass(child.GetDamagedMass() * massRatio);
	}

	// Update the whole fragment 
	type->GetPhysics(currentLOD)->ComputeChildAngularInertias();
	type->ComputeMassProperties(currentLOD);
	TypeTotalUndamagedMass = type->GetPhysics(currentLOD)->GetArchetype()->GetMass();
}

void fragTuneStruct::ApplyGroupBreakPresetCallback(int groupIdx)
{
	int currentLOD = 0;

	fragType* type = GetTypeBeingTuned();
	fragTypeGroup& group = *type->GetPhysics(currentLOD)->GetGroup(groupIdx);
	
	ApplyBreakPresetToGroup(group, group.m_PresetApplied - 1);
}

void fragTuneStruct::ApplyGroupBreakPresetRecursiveCallback(int groupIndex)
{
	int currentLOD = 0;

	fragType* type = GetTypeBeingTuned();
	fragTypeGroup& group = *type->GetPhysics(currentLOD)->GetGroup(groupIndex);

	ApplyBreakPresetToGroup(group, group.m_PresetApplied - 1);

	for (int childIndex = 0; childIndex < group.GetNumChildGroups(); ++childIndex)
	{
		ApplyGroupBreakPresetRecursiveCallback(group.GetChildGroupsPointersIndex() + childIndex);
	}
}

#if ENABLE_FRAGMENT_EVENTS
void fragTuneStruct::CreateTypeCollisionEventsCallback(fragType* type)
{
	type->m_CollisionEventPlayer = rage_new fragCollisionEventPlayer;
	type->m_CollisionEventPlayer->CreateParameterList();
	type->m_CollisionEventset = rage_new evtSet;
	type->m_CollisionEventPlayer->SetSet(*type->m_CollisionEventset);

	m_RecreateWidgets = true;
}

void fragTuneStruct::CreateGroupDeathEventsCallback(fragTypeGroup* group)
{
	group->m_DeathEventPlayer = rage_new fragCollisionEventPlayer;
	group->m_DeathEventPlayer->CreateParameterList();
	group->m_DeathEventset = rage_new evtSet;
	group->m_DeathEventPlayer->SetSet(*group->m_DeathEventset);

	m_RecreateWidgets = true;
}

void fragTuneStruct::CreateChildCollisionEventsCallback(fragTypeChild* child)
{
	child->m_CollisionEventPlayer = rage_new fragCollisionEventPlayer;
	child->m_CollisionEventPlayer->CreateParameterList();
	child->m_CollisionEventset = rage_new evtSet;
	child->m_CollisionEventPlayer->SetSet(*child->m_CollisionEventset);

	m_RecreateWidgets = true;
}

void fragTuneStruct::CreateChildBreakEventsCallback(fragTypeChild* child)
{
	child->m_BreakEventPlayer = rage_new fragBreakEventPlayer;
	child->m_BreakEventPlayer->CreateParameterList();
	child->m_BreakEventset = rage_new evtSet;
	child->m_BreakEventPlayer->SetSet(*child->m_BreakEventset);

	m_RecreateWidgets = true;
}

void fragTuneStruct::CreateChildBreakFromRootEventsCallback(fragTypeChild* child)
{
	child->m_BreakFromRootEventPlayer = rage_new fragBreakEventPlayer;
	child->m_BreakFromRootEventPlayer->CreateParameterList();
	child->m_BreakFromRootEventset = rage_new evtSet;
	child->m_BreakFromRootEventPlayer->SetSet(*child->m_BreakFromRootEventset);

	m_RecreateWidgets = true;
}
#endif // ENABLE_FRAGMENT_EVENTS

void fragTuneStruct::AdjustDamageHealth(int groupIndex)
{
	int currentLOD = 0;

	fragType* type = GetTypeBeingTuned();
	fragTypeGroup* group = type->GetPhysics(currentLOD)->GetGroup(groupIndex);
	group->m_PresetApplied = 0;

	FRAGMGR->GetFragCacheManager()->TuneDamageHealth(type, groupIndex);
}

void fragTuneStruct::AdjustWeaponHealth(int groupIndex)
{
	int currentLOD = 0;

	fragType* type = GetTypeBeingTuned();
	fragTypeGroup* group = type->GetPhysics(currentLOD)->GetGroup(groupIndex);
	group->m_PresetApplied = 0;

	FRAGMGR->GetFragCacheManager()->TuneWeaponHealth(type, groupIndex);
}

void fragTuneStruct::AdjustTypeMass(fragType* type)
{
	int currentLOD = 0;

	ScalarV newMass(TypeTotalUndamagedMass);
	type->ScaleUndamagedChildren(currentLOD, &newMass, NULL, fragChildBits(true), NULL);

	// Also, change the archetype mass for all current instances
#if ENABLE_PHYSICS_LOCK
	phIterator iterator(phIterator::PHITERATORLOCKTYPE_WRITELOCK);
#else	// ENABLE_PHYSICS_LOCK
	phIterator iterator;
#endif	// ENABLE_PHYSICS_LOCK
	iterator.InitCull_All();
	iterator.SetStateIncludeFlags(phLevelBase::STATE_FLAG_ACTIVE | phLevelBase::STATE_FLAG_INACTIVE);
	for(u16 index = iterator.GetFirstLevelIndex(PHLEVEL); index != phInst::INVALID_INDEX; index = iterator.GetNextLevelIndex(PHLEVEL))
	{
		fragInst* inst = dynamic_cast<fragInst*>(PHLEVEL->GetInstance(index));
		if(inst && inst->GetType() == type)
		{
			phArchetypePhys* archPhys = dynamic_cast<phArchetypePhys*>(inst->GetArchetype());
			if(archPhys)
			{
				archPhys->SetMass(TypeTotalUndamagedMass);
				archPhys->CalcInvMass();
				archPhys->CalcAngInertia();
				archPhys->CalcInvAngInertia();
			}

			if(PHLEVEL->IsActive(index))
			{
				phCollider* collider = (phCollider*)(PHLEVEL->GetUserData(index));
				// The collider makes copies of our mass and such; if we're changing it we need to let it know
				collider->InitInertia();
				// In addition we need to recalculate the momenta with the new masses, which occurs via our setting the same velocities
				collider->SetVelocity(collider->GetVelocity().GetIntrin128());
				collider->SetAngVelocity(collider->GetAngVelocity().GetIntrin128());
			}
			else if(WakeAllAffectedFragsOnEdit)
			{
				// Activate to allow changes to take an effect if the object was previously inactive
				PHSIM->ActivateObject(index);
			}
		}
	}
}

void fragTuneStruct::AdjustMinMoveForce(fragType* type)
{
	int currentLOD = 0;

	// We unfortunately cannot limit our looping to just active or inactive objects because the SetBroken call
	//	needs to be made on all objects of this type, regardless.
	if(type->GetPhysics(currentLOD)->m_MinMoveForce == 0.0f) // Wake up all current instances all current instances if the frag is now movable
	{
#if ENABLE_PHYSICS_LOCK
		phIterator iterator(phIterator::PHITERATORLOCKTYPE_WRITELOCK);
#else	// ENABLE_PHYSICS_LOCK
		phIterator iterator;
#endif	// ENABLE_PHYSICS_LOCK
		iterator.InitCull_All();
		iterator.SetStateIncludeFlags(phLevelBase::STATE_FLAG_ACTIVE | phLevelBase::STATE_FLAG_INACTIVE);
		for(u16 index = iterator.GetFirstLevelIndex(PHLEVEL); index != phInst::INVALID_INDEX; index = iterator.GetNextLevelIndex(PHLEVEL))
		{
			fragInst* inst = dynamic_cast<fragInst*>(PHLEVEL->GetInstance(index));
			if(inst && inst->GetType() == type)
			{
				inst->SetBroken(true); // The frag is no longer guaranteed to be stuck to the world (Without this, the following activate fails)
				if(WakeAllAffectedFragsOnEdit && !PHLEVEL->IsActive(index))
				{
					PHSIM->ActivateObject(index); // Activate to allow changes to take an effect
				}
			}
		}
	}
	else if(type->GetPhysics(currentLOD)->m_MinMoveForce < 0.0f) // Reset all to immobile
	{
#if ENABLE_PHYSICS_LOCK
		phIterator iterator(phIterator::PHITERATORLOCKTYPE_WRITELOCK);
#else	// ENABLE_PHYSICS_LOCK
		phIterator iterator;
#endif	// ENABLE_PHYSICS_LOCK
		iterator.InitCull_All();
		iterator.SetStateIncludeFlags(phLevelBase::STATE_FLAG_ACTIVE | phLevelBase::STATE_FLAG_INACTIVE);
		for(u16 index = iterator.GetFirstLevelIndex(PHLEVEL); index != phInst::INVALID_INDEX; index = iterator.GetNextLevelIndex(PHLEVEL))
		{
			fragInst* inst = dynamic_cast<fragInst*>(PHLEVEL->GetInstance(index));
			if(inst && inst->GetType() == type)
			{
				inst->SetBroken(false); // The frag should be immobile now
				if(WakeAllAffectedFragsOnEdit && PHLEVEL->IsActive(index))
				{
					PHSIM->DeactivateObject(index);
				}
			}
		}
	}
}

void fragTuneStruct::AddGroupWidgets(bkBank& bank, int index, fragType* type)
{
	int currentLOD = 0;

	Assert(type);
	fragTypeGroup* group = type->GetPhysics(currentLOD)->GetGroup(index);
	Assert(group);

	char nameString[FRAG_TYPE_GROUP_DEBUG_NAME_LENGTH + 20];

	if (group->GetNumChildren() == 1)
	{
		formatf(nameString, "%s (%d)", group->GetDebugName(), group->GetChildFragmentIndex());
	}
	else
	{
		formatf(nameString, "%s (%d-%d)", group->GetDebugName(), group->GetChildFragmentIndex(), group->GetChildFragmentIndex() + group->GetNumChildren() - 1);
	}

	bank.PushGroup(nameString, true);

	bank.AddSlider("Strength",						&group->m_Strength,						-1.0f, 1000000.0f, 0.1f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "The for needed to break a group from its parent");

	bank.AddSlider("ForceTransmissionScaleUp",		&group->m_ForceTransmissionScaleUp,		0.0f,     100.0f,  0.01f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "The amount of breaking force transferred from a child to its parent.");
	bank.AddSlider("ForceTransmissionScaleDown",	&group->m_ForceTransmissionScaleDown,	0.0f,     100.0f,  0.01f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "The amount of breaking force transferred from a parent down to its child.");

    bank.AddSlider("Total Group Mass",              &group->m_TotalUndamagedMass,           0.001f, 100000.0f,  1.0f * group->m_NumChildren,  datCallback(MFA2(fragTuneStruct::AdjustGroupMass), this, group), "The sum of the masses of all the children");

	bank.AddToggle("DisappearsWhenDead",			&group->m_Flags, fragTypeGroup::FRAG_GROUP_FLAG_DISAPPEARS_WHEN_DEAD, NullCB, "When the health of this group becomes zero, cause it to disappear (particularly useful when a particle effect triggers at the same time)");
	bank.AddToggle("DamageWhenBroken",				&group->m_Flags, fragTypeGroup::FRAG_GROUP_FLAG_DAMAGE_WHEN_BROKEN, NullCB, "When this group breaks off from its parent it will become damaged");
	bank.AddToggle("DoesntAffectVehicles",			&group->m_Flags, fragTypeGroup::FRAG_GROUP_FLAG_DOESNT_AFFECT_VEHICLES, NullCB, "When a vehicle touches this group, the vehicle is treated as infinitely massive");
	bank.AddToggle("DoesntPushVehiclesDown",		&group->m_Flags, fragTypeGroup::FRAG_GROUP_FLAG_DOESNT_PUSH_VEHICLES_DOWN, NullCB, "Downward collision normals with this prop and a vehicle will be flattened if the prop is not broken");

	bank.AddSlider("MinDamageForce",				&group->m_MinDamageForce,				0.0f,    1000.0f,  0.1f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "The minimum force necessary to cause any health loss");

	void* groupIndexContext = (void*)(ptrdiff_t)index;
	bank.AddSlider("DamageHealth",				&group->m_DamageHealth,				0.0f,   10000.0f,  1.0f, datCallback(MFA1(fragTuneStruct::AdjustDamageHealth), this, groupIndexContext), "The total amount of damage that can be absorbed before the part becomes damaged");

	fragInst* selectedInst = NULL;
	fragCacheEntry* entry = NULL;

	if (InstanceTuningEnabled &&
		CurrentFragInstLevelIndex != phInst::INVALID_INDEX &&
		PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
	{
		selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

		entry = selectedInst->GetCacheEntry();
	}

	if (selectedInst)
	{
		bank.AddSlider("* DamageHealth",		&m_DamageHealthProxies[index], -10000.0f, 10000.0f,  1.0f, datCallback(MFA1(fragTuneStruct::AdjustInstanceDamageHealth), this, groupIndexContext), "The current damage health of the instance being tuned");
		if (entry)
		{
			m_DamageHealthProxies[index] = entry->GetHierInst()->groupInsts[index].damageHealth;
		}
		else
		{
			m_DamageHealthProxies[index] = group->GetDamageHealth();
		}
	}

	bank.AddSlider("WeaponHealth",				&group->m_WeaponHealth,				0.0f,   10000.0f,  1.0f, datCallback(MFA1(fragTuneStruct::AdjustWeaponHealth), this, groupIndexContext), "The total amount of damage from weapons that can be absorbed before the part can break");

	if (selectedInst)
	{
		bank.AddSlider("* WeaponHealth",		&m_WeaponHealthProxies[index], -10000.0f, 10000.0f,  1.0f, datCallback(MFA1(fragTuneStruct::AdjustInstanceWeaponHealth), this, groupIndexContext), "The current weapon health of the instance being tuned");
		if (entry)
		{
			m_WeaponHealthProxies[index] = entry->GetHierInst()->groupInsts[index].weaponHealth;
		}
		else
		{
			m_WeaponHealthProxies[index] = group->GetWeaponHealth();
		}
	}

	bank.AddButton("Save (whole type)", datCallback(MFA1(fragTuneStruct::SaveArchetypeCallback), this, type), "Save fragment tuning data into .tune file");

	bank.PushGroup("Source Scales", true);
	bank.AddSlider("Weapon Scale",		&group->m_WeaponScale,		0.0f,     10.0f,  0.1f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "A factor to multiply both breaking and damage impulses by when coming from weapons.");
	bank.AddSlider("Melee Scale",		&group->m_MeleeScale,		0.0f,     10.0f,  0.1f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "A factor to multiply both breaking and damage impulses by when coming from melee.");
	bank.AddSlider("Vehicle Scale",		&group->m_VehicleScale,		0.0f,     10.0f,  0.1f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "A factor to multiply both breaking and damage impulses by when coming from vehicles.");
	bank.AddSlider("Ped Scale",			&group->m_PedScale,			0.0f,     10.0f,  0.1f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "A factor to multiply both breaking and damage impulses by when coming from peds.");
	bank.AddSlider("Ragdoll Scale",		&group->m_RagdollScale,		0.0f,     10.0f,  0.1f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "A factor to multiply both breaking and damage impulses by when coming from ragdolls.");
	bank.AddSlider("Explosion Scale",	&group->m_ExplosionScale,	0.0f,     10.0f,  0.1f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "A factor to multiply both breaking and damage impulses by when coming from explosions.");
	bank.AddSlider("Object Scale",		&group->m_ObjectScale,		0.0f,     10.0f,  0.1f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "A factor to multiply both breaking and damage impulses by when coming from other objects.");
	bank.AddSlider("Ped Inv Mass Scale",&group->m_PedInvMassScale,	0.0f,     1.0f,  0.05f, datCallback(MFA1(fragTuneStruct::ResetGroupPresetCallback), this, group), "Affects how heavy this group is when interacting with peds. 1-Normal, 0-Not movable at all.");
	bank.PopGroup();

	if (selectedInst)
	{
		bank.AddButton("* Break", datCallback(MFA1(fragTuneStruct::BreakGroup), this, groupIndexContext));
		bank.AddButton("* Latch", datCallback(MFA1(fragTuneStruct::LatchGroup), this, groupIndexContext));
		bank.AddButton("* Unlatch", datCallback(MFA1(fragTuneStruct::UnlatchGroup), this, groupIndexContext));
		bank.AddButton("* Delete Above", datCallback(MFA1(fragTuneStruct::DeleteAboveGroup), this, groupIndexContext));
		bank.AddButton("* Restore Above", datCallback(MFA1(fragTuneStruct::RestoreAboveGroup), this, groupIndexContext));
	}

	if(m_BreakPresets.GetCount())
	{
		int numBreakPresets = m_BreakPresets.GetCount();

		const char** breakPresetNames = rage_new const char*[numBreakPresets + 1];
		breakPresetNames[0] = "<none>";
		for (int i = 0; i < numBreakPresets; ++i)
		{
			breakPresetNames[i + 1] = m_BreakPresets[i].m_Name;
		}

		bank.AddCombo("Preset", &group->m_PresetApplied, numBreakPresets + 1, breakPresetNames, datCallback(MFA1(fragTuneStruct::ApplyGroupBreakPresetCallback), this, groupIndexContext), "Select the break preset to apply to the fragment group");

		bank.AddButton("Apply Preset To All Subgroups", datCallback(MFA1(fragTuneStruct::ApplyGroupBreakPresetRecursiveCallback), this, groupIndexContext));

		delete [] breakPresetNames;
	}

#if ENABLE_FRAGMENT_EVENTS
	if (evtTypeManagerSingleton::IsInstantiated())
	{
		if (group->m_DeathEventset)
		{
			bank.PushGroup("Death Events - group", false, "Events that fire when DisappearsWhenDead takes effect and the group disappears");
			PARSER.AddWidgets(bank, (evtSet*)group->m_DeathEventset);
			group->m_DeathEventset->AddWidgets(bank);
			bank.PopGroup();
		}
		else
		{
			bank.AddButton("Death Events - group", datCallback(MFA1(fragTuneStruct::CreateGroupDeathEventsCallback), this, group));
		}
	}
#endif // ENABLE_FRAGMENT_EVENTS

	if (const crSkeletonData* sd = type->GetCommonDrawable()->GetSkeletonData())
	{
		const crBoneData* bone = sd->GetBoneData(type->GetBoneIndexFromID(type->GetPhysics(currentLOD)->GetChild(group->GetChildFragmentIndex())->GetBoneID()));
		const crJointData* jd = type->GetCommonDrawable()->GetJointData();
        bool hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits;
		if (jd != NULL && type->HasMotionLimits(*bone, *jd, hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits))
		{
            bank.AddSlider("Joint Latch Strength",				&group->m_LatchStrength,			   -1.0f, 1000000.0f, 10.0f);

            int limitTDof = (hasXTLimits ? 1 : 0) +
                            (hasYTLimits ? 1 : 0) +
                            (hasZTLimits ? 1 : 0);

            if (limitTDof)
            {
                AssertMsg(limitTDof == 1, "Only 1 DOF translational joints are allowed at this time");

                bank.PushGroup("Prismatic Joint", false, "This fragment group will become a 1 degree of freedom sliding joint (a drawer or shock)");
                bank.AddSlider("Stiffness",						&group->m_JointStiffness,				0.0f,		1.0f,  0.01f, NullCB, "The resistance to rotation of the joint");
#if 0 // Disabled because these don't seem to work right now
                bank.AddSlider("RestoringStrength",				&group->m_RestoringStrength,		    0.0f,	 1000.0f,  1.0f, NullCB, "The speed with which to attempt to return to the zero pose");
                bank.AddSlider("RestoringMaxForce",				&group->m_RestoringMaxTorque,		    0.0f,	 1000.0f,  1.0f, NullCB, "The maximum force to use to drive the joint to zero pose");
#endif
                bank.PopGroup();
            }
            else
            {
			    int limitDof = (hasXLimits ? 1 : 0) +
						    (hasYLimits ? 1 : 0) +
						    (hasZLimits ? 1 : 0);

			    if (limitDof == 1)
			    {
				    bank.PushGroup("1 DOF Joint", false, "This fragment group will become a 1 degree of freedom joint (a hinge)");
				    bank.AddSlider("Stiffness",						&group->m_JointStiffness,				0.0f,		1.0f,  0.01f, NullCB, "The resistance to rotation of the joint");
				    bank.AddSlider("Min Soft Angle",				&group->m_MinSoftAngle1,			   -1.0f,		1.0f,  0.01f, NullCB, "The angle where the soft limit will kick in, pushing the joint back to this angle");
				    bank.AddSlider("Max Soft Angle",				&group->m_MaxSoftAngle1,			   -1.0f,		1.0f,  0.01f, NullCB, "The angle where the soft limit will kick in, pushing the joint back to this angle");
				    bank.AddSlider("RotationSpeed",					&group->m_RotationSpeed,			 -100.0f,	  100.0f,  1.0f, NullCB, "The target rotation speed of this joint, a force will drive the joint to reach this rotation");
				    bank.AddSlider("RotationStrength",				&group->m_RotationStrength,			 -100.0f,	  100.0f,  1.0f, NullCB, "The amount of force to use to drive the joint to the above rotation speed");
                    bank.AddSlider("RestoringStrength",				&group->m_RestoringStrength,		    0.0f,	 1000.0f,  1.0f, NullCB, "The speed with which to attempt to return to the zero pose");
                    bank.AddSlider("RestoringMaxTorque",			&group->m_RestoringMaxTorque,		    0.0f,	 1000.0f,  1.0f, NullCB, "The maximum amount of torque to be applied to return tot he zero pose");
				    bank.PopGroup();
			    }
			    else
			    {
				    bank.PushGroup("3 DOF Joint", false);
				    bank.AddSlider("Stiffness",						&group->m_JointStiffness,				0.0f,		1.0f,  0.01f, NullCB, "The resistance to rotation of the joint");
				    bank.AddSlider("Max Soft Angle 1",				&group->m_MaxSoftAngle1,			   -1.0f,		1.0f,  0.01f, NullCB, "The angle where the soft limit will kick in, pushing the joint back to this angle");
				    bank.AddSlider("Max Soft Angle 2",				&group->m_MaxSoftAngle2,			   -1.0f,		1.0f,  0.01f, NullCB, "The angle where the soft limit will kick in, pushing the joint back to this angle");
				    bank.AddSlider("Max Soft Twist Angle",			&group->m_MaxSoftAngle3,			   -1.0f,		1.0f,  0.01f, NullCB, "The angle where the soft limit will kick in, pushing the joint back to this angle");
					bank.AddSlider("RotationStrength",				&group->m_RotationStrength,			    0.0f,	  100.0f,  1.0f, NullCB, "The amount of force to use to drive the joint to the above rotation speed");
                    bank.AddSlider("RestoringStrength",				&group->m_RestoringStrength,		    0.0f,	 1000.0f,  1.0f, NullCB, "The speed with which to attempt to return to the zero pose");
                    bank.AddSlider("RestoringMaxTorque",			&group->m_RestoringMaxTorque,		    0.0f,	 1000.0f,  1.0f, NullCB, "The maximum amount of torque to be applied to return tot he zero pose");
				    bank.PopGroup();
			    }
            }
		}
	}

	void* context = (void*)(ptrdiff_t)index;

	Bank->PushGroup("Copy/Paste", false);
	Bank->AddButton("Copy, Paste To All Groups", datCallback(MFA1(fragTuneStruct::CopyPasteToAllGroups), this, context), "Copy the tunings from this group into all the other groups");
	Bank->AddButton("Copy From Group", datCallback(MFA1(fragTuneStruct::CopyFromGroup), this, context), "Copy the tunings from this group to apply to other groups via paste");
	Bank->AddButton("Paste To Group", datCallback(MFA1(fragTuneStruct::PasteToGroup), this, context), "Copy the tunings from the copied group to this group");
	Bank->AddButton("Paste To All Subgroups", datCallback(MFA1(fragTuneStruct::PasteToGroupsRecursive), this, context), "Copy the tunings from the copied group to this group");
	Bank->AddSeparator();
	Bank->AddButton("Select All", datCallback(MFA1(fragTuneStruct::SelectAllFields), this), "Select all fields for copying");
	Bank->AddButton("Deselect All", datCallback(MFA1(fragTuneStruct::DeselectAllFields), this), "Select no fields for copying");
	Bank->AddSeparator();
	Bank->AddToggle("Strength", &s_CopyGroupStrength);
	Bank->AddToggle("ForceTransmissionScaleUp", &s_CopyGroupForceTransmissionScaleUp);
	Bank->AddToggle("ForceTransmissionScaleDown", &s_CopyGroupForceTransmissionScaleDown);
	Bank->AddToggle("DisappearsWhenDead", &s_CopyGroupDisappearsWhenDeadFlag);
	Bank->AddToggle("DamageWhenBroken", &s_CopyGroupDamageWhenBrokenFlag);
	Bank->AddToggle("DoesntAffectVehicles", &s_CopyDoesntAffectVehicles);
	Bank->AddToggle("DoesntPushVehiclesDown", &s_CopyDoesntPushVehiclesDown);
	Bank->AddToggle("MinDamageForce", &s_CopyGroupMinDamageForce);
	Bank->AddToggle("Damage Health", &s_CopyGroupDamageHealth);
	Bank->AddToggle("Weapon Health", &s_CopyGroupWeaponHealth);
	Bank->AddToggle("Weapon Scale", &s_CopyGroupWeaponScale);
	Bank->AddToggle("Melee Scale", &s_CopyGroupMeleeScale);
	Bank->AddToggle("Vehicle Scale", &s_CopyGroupVehicleScale);
	Bank->AddToggle("Ped Scale", &s_CopyGroupPedScale);
	Bank->AddToggle("Ragdoll Scale", &s_CopyGroupRagdollScale);
	Bank->AddToggle("Explosion Scale", &s_CopyGroupExplosionScale);
	Bank->AddToggle("Object Scale", &s_CopyGroupObjectScale);
	Bank->AddToggle("Ped Inv Mass Scale", &s_CopyGroupPedInvMassScale);
	Bank->AddToggle("Preset", &s_CopyGroupPresetApplied);

	if (const crSkeletonData* sd = type->GetCommonDrawable()->GetSkeletonData())
	{
		const crBoneData* bone = sd->GetBoneData(type->GetBoneIndexFromID(type->GetPhysics(currentLOD)->GetChild(group->GetChildFragmentIndex())->GetBoneID()));
		const crJointData* jd = type->GetCommonDrawable()->GetJointData();
		bool hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits;
		if (jd != NULL && type->HasMotionLimits(*bone, *jd, hasXLimits, hasYLimits, hasZLimits, hasXTLimits, hasYTLimits, hasZTLimits))
		{
			Bank->AddToggle("LatchStrength", &s_CopyGroupLatchStrength);

			int limitTDof = (hasXTLimits ? 1 : 0) +
				(hasYTLimits ? 1 : 0) +
				(hasZTLimits ? 1 : 0);

			if (limitTDof)
			{
				AssertMsg(limitTDof == 1, "Only 1 DOF translational joints are allowed at this time");

				bank.PushGroup("Prismatic Joint", false, "This fragment group will become a 1 degree of freedom sliding joint (a drawer or shock)");
				Bank->AddToggle("JointStiffness", &s_CopyGroupJointStiffness);
#if 0 // Disabled because these don't seem to work right now
				Bank->AddToggle("RestoringStrength", &s_CopyGroupRestoringStrength);
				Bank->AddToggle("RestoringMaxTorque", &s_CopyGroupRestoringMaxTorque);
#endif
				bank.PopGroup();
			}
			else
			{
				int limitDof = (hasXLimits ? 1 : 0) +
					(hasYLimits ? 1 : 0) +
					(hasZLimits ? 1 : 0);

				if (limitDof == 1)
				{
					bank.PushGroup("1 DOF Joint", false, "This fragment group will become a 1 degree of freedom joint (a hinge)");
					Bank->AddToggle("Stiffness", &s_CopyGroupJointStiffness);
					Bank->AddToggle("Min Soft Angle", &s_CopyGroupMinSoftAngle1);
					Bank->AddToggle("Max Soft Angle", &s_CopyGroupMaxSoftAngle1);
					Bank->AddToggle("RotationSpeed", &s_CopyGroupRotationSpeed);
					Bank->AddToggle("RotationStrength", &s_CopyGroupRotationStrength);
					Bank->AddToggle("RestorinStrength", &s_CopyGroupRestoringStrength);
					Bank->AddToggle("RestoringMaxTorque", &s_CopyGroupRestoringMaxTorque);
					bank.PopGroup();
				}
				else
				{
					bank.PushGroup("3 DOF Joint", false);
					Bank->AddToggle("Stiffness", &s_CopyGroupJointStiffness);
					Bank->AddToggle("Max Soft Angle 1", &s_CopyGroupMaxSoftAngle1);
					Bank->AddToggle("Max Soft Angle 2", &s_CopyGroupMaxSoftAngle2);
					Bank->AddToggle("Max Soft Twist Angle", &s_CopyGroupMaxSoftAngle3);
					Bank->AddToggle("RestoringStrength", &s_CopyGroupRestoringStrength);
					Bank->AddToggle("RestoringMaxTorque", &s_CopyGroupRestoringMaxTorque);
					bank.PopGroup();
				}
			}
		}
	}

	bank.PopGroup();

	fragTypeChild* firstChild = type->GetPhysics(currentLOD)->GetChild(group->GetChildFragmentIndex());

	// Only put frag children there if there is more than one, or it has a damaged entity
	if (group->GetNumChildren() > 1 || firstChild->GetDamagedEntity())
	{
		if (group->GetNumChildren() > 1)
		{
			bank.PushGroup("children");
		}
		else
		{
			bank.PushGroup("child");
		}

		for (int childIndex = 0; childIndex < group->GetNumChildren(); ++childIndex)
		{
			if (group->GetNumChildren() > 1)
			{
				char childName[256];
				sprintf(childName, "Child %d", childIndex + group->GetChildFragmentIndex());

				Bank->PushGroup(childName,false);
			}

			fragTypeChild* child = type->GetPhysics(currentLOD)->GetChild(group->GetChildFragmentIndex() + childIndex);

			context = (void*)(ptrdiff_t)(group->GetChildFragmentIndex() + childIndex);
	
			Bank->AddButton("Copy To Other Children", datCallback(MFA1(fragTuneStruct::RepeatFromChild), this, context), "Copy the tunings from this child into all the other children");
			if (child->GetDamagedEntity())
			{
				bank.AddSlider("UndamagedMass",  child->GetUndamagedMassPtr(),  0.0f, 1000000.0f, 1.0f, datCallback(MFA1(fragTuneStruct::AdjustChildMass), this, group), "The mass of this part of the object before it is damaged");
				bank.AddSlider("DamagedMass",    child->GetDamagedMassPtr(),    0.0f, 1000000.0f, 1.0f, NullCB, "The mass of this part of the object after it is damaged");
			}
			else
			{
				bank.AddSlider("Mass",  child->GetUndamagedMassPtr(),  0.0f, 1000000.0f, 1.0f, datCallback(MFA1(fragTuneStruct::AdjustChildMass), this, group), "The mass of this part of the object");
			}

#if ENABLE_FRAGMENT_EVENTS
			if (evtTypeManagerSingleton::IsInstantiated())
			{
				if (child->m_ContinuousEventset)
				{
					bank.PushGroup("Continuous Events - child", false, "Events that will be started when the prop is created, and stopped when it is destroyed. They will stay attached to this part of the object.");
					PARSER.AddWidgets(bank, (evtSet*)child->m_ContinuousEventset);
					child->m_ContinuousEventset->AddWidgets(bank);
					bank.PopGroup();
				}

				if (child->m_CollisionEventset)
				{
					bank.PushGroup("Collision Events - child", false, "One-shot events that will be fired when this part of the object is involved in a collision");
					PARSER.AddWidgets(bank, (evtSet*)child->m_CollisionEventset);
					child->m_CollisionEventset->AddWidgets(bank);
					bank.PopGroup();
				}
				else
				{
					bank.AddButton("Collision Events - child", datCallback(MFA1(fragTuneStruct::CreateChildCollisionEventsCallback), this, child));
				}

				if (child->m_BreakEventset)
				{
					bank.PushGroup("Break Events - child", false, "One-shot events that will be fired when this part of the object breaks off from its parent");
					PARSER.AddWidgets(bank, (evtSet*)child->m_BreakEventset);
					child->m_BreakEventset->AddWidgets(bank);
					bank.PopGroup();
				}
				else
				{
					bank.AddButton("Break Events - child", datCallback(MFA1(fragTuneStruct::CreateChildBreakEventsCallback), this, child));
				}

				if (child->m_BreakFromRootEventset)
				{
					bank.PushGroup("Break From Root Events - child", false, "One-shot events that will be fired when this part of the object breaks off from the root, i.e. any break point between this part and the root breaks");
					PARSER.AddWidgets(bank, (evtSet*)child->m_BreakFromRootEventset);
					child->m_BreakFromRootEventset->AddWidgets(bank);
					bank.PopGroup();
				}
				else
				{
					bank.AddButton("Break From Root Events - child", datCallback(MFA1(fragTuneStruct::CreateChildBreakFromRootEventsCallback), this, child));
				}
			}
#endif // ENABLE_FRAGMENT_EVENTS

			if (group->GetNumChildren() > 1)
			{
				// Only pop Child X if we pushed it
				Bank->PopGroup();
			}
		}

		bank.PopGroup();
	}

	u32 numChildGroups = group->GetNumChildGroups();
	u32 globalChild = group->GetChildGroupsPointersIndex();

	for (u32 localChild = 0; localChild < numChildGroups; ++localChild, ++globalChild)
	{
		AddGroupWidgets(bank, globalChild, type);
	}

	bank.PopGroup();
}

void fragTuneStruct::AddTuningWidgetsToBank(fragType* type, int componentSelection)
{
	if(type)
	{
		TypeBank = Bank->PushGroup(type->GetBaseName(), false);

		AddWidgetsToBank(type, componentSelection);

		Bank->PopGroup();
	}
}

void fragTuneStruct::ResetAudioType(fragType* type)
{
	if (fragType::GetAudioDestructCB())
		fragType::GetAudioDestructCB()(type);
	fragType::GetAudioConstructCB()(type);
}

fragTuneStruct::UpdateEntityFunctor fragTuneStruct::sm_UpdateEntityFunctor;

void fragTuneStruct::SetUpdateEntityFunctor(UpdateEntityFunctor updateEntityFunctor)
{
	sm_UpdateEntityFunctor = updateEntityFunctor;
}

fragTuneStruct::SelectEntityFunctor fragTuneStruct::sm_SelectEntityFunctor;

void fragTuneStruct::SetSelectEntityFunctor(SelectEntityFunctor selectEntityFunctor)
{
	sm_SelectEntityFunctor = selectEntityFunctor;
}

fragTuneStruct::PreSaveEntityFunctor fragTuneStruct::sm_PreSaveEntityFunctor;

void fragTuneStruct::SetPreSaveEntityFunctor(PreSaveEntityFunctor preSaveCB)
{
	sm_PreSaveEntityFunctor = preSaveCB;
}

void fragTuneStruct::AddWidgetsToBank(fragType* type, int componentSelection)
{
	int currentLOD = 0;

	Bank->AddButton("Save", datCallback(MFA1(fragTuneStruct::SaveArchetypeCallback), this, type), "Save fragment tuning data into .tune file");


    if (componentSelection > 0)
    {
        Bank->AddButton("Show whole type", datCallback(MFA(fragTuneStruct::SelectFromTypeComboCb), this), "Display all the widgets for this fragment type");
    }
    else
    {
		if(m_BreakPresets.GetCount())
		{
			Bank->PushGroup("Breaking Presets", false);
			int numBreakPresets = m_BreakPresets.GetCount();

			const char** breakPresetNames = rage_new const char*[numBreakPresets];
			for (int i = 0; i < numBreakPresets; ++i)
			{
				breakPresetNames[i] = m_BreakPresets[i].m_Name;
			}

			Bank->AddCombo("Type", &TypeBreakPresetSelection, numBreakPresets, breakPresetNames, NullCallback, "Select the break preset to apply to the fragment type");
			Bank->AddButton("Apply Preset To All Groups", datCallback(MFA1(fragTuneStruct::ApplyArchetypeBreakPresetCallback), this, type));

			delete [] breakPresetNames;
			
			Bank->PopGroup();
		}


	    //Bank->AddButton("Become default times mass", datCallback(MFA1(fragTuneStruct::SetToDefaultByMass), this, type), "Generate initial guess at strengths and healths scaled by mass");

        Bank->AddSlider("Total Type Mass", &TypeTotalUndamagedMass, 0.001f, 100000.0f,  1.0f * type->GetPhysics(currentLOD)->GetNumChildren(),  datCallback(MFA1(fragTuneStruct::AdjustTypeMass), this, type), "The sum of the masses of all the children");

		if (!type->GetPhysics(currentLOD)->IsAllGlass()) // Hid min move force if type is all glass, so we can force it to be minus one
		{
			Bank->AddSlider("MinMoveForce", &type->GetPhysics(currentLOD)->m_MinMoveForce, -1.0f, 10000.0f, 10.0f, datCallback(MFA1(fragTuneStruct::AdjustMinMoveForce), this, type), "The impulse needed to start the object moving");
		}
	    //Bank->AddSlider("NM Asset", &type->m_ARTAssetID, -1, 127, 1, NullCB, "If the game code is set up to handle it, connect this fragment type to this NaturalMotion asset.");
        //Bank->AddSlider("Cache Size", (float *)&type->m_EstimatedCacheSize, 0, 10000000, 0, NullCB, "The estimated memory for the prop when it is in the cache (active).");

	    Bank->PushGroup("Root CG Offset", false);
		Bank->AddButton("Restore computed CG", datCallback(MFA1(fragTuneStruct::RestoreComputedCG), this, type), "Restore the CG computed from the bounds");
		Bank->AddSlider("Root CG Offset", &type->GetPhysics(currentLOD)->m_RootCGOffset, -100.0f, 100.0f, 0.1f,datCallback(MFA1(fragTuneStruct::AdjustRootCGOffset), this, type), "Move the center of gravity of the root link");
	    Bank->PopGroup();

		Bank->PushGroup("Unbroken tuning", false);
	    Bank->AddSlider("Unbroken CG Offset", &type->GetPhysics(currentLOD)->m_UnbrokenCGOffset, -100.0f, 100.0f, 0.1f, NullCB, "CG offset for the prop before it breaks, influencing its motion from the initial breaking impact");
		Bank->AddSlider("Unbroken Elasticity", &type->m_UnbrokenElasticity, 0.0f, 8.0f, 0.1f, NullCB, "Elasticity for the prop before it breaks, influencing its motion from the initial breaking impact");
		Bank->PopGroup();

		//
		// This will not do anything unless the game itself utilizes m_UnbrokenElasticity:
		//
		Bank->AddSlider("Gravity Factor", &type->m_GravityFactor, -10.0f, 10.0f, 0.01f, datCallback(MFA1(fragTuneStruct::AdjustGravityFactor), this, type), "Make object fall faster or slower");
		Bank->AddSlider("Buoyancy Factor", &type->m_BuoyancyFactor, 0.0f, 10.0f, 0.01f, datCallback(MFA1(fragTuneStruct::AdjustBuoyancyFactor), this, type), "Make object more or less buoyant");

		Bank->PushGroup("Damping", false, "Additional drag on the objects, causing them to slow down or stop.");
		{
			Bank->AddSlider("Average Damping", &Damping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &Damping), "Controls the average of all damping values.");
			Bank->PushGroup("Linear Damping", false, "Damps the linear velocity of objects.");
			{
				Bank->AddSlider("Average Linear Damping", &LinearDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearDamping), "Controls the average of all linear damping values.");
				Bank->PushGroup("Linear Speed Damping", false, "Slows down objects traveling at low speeds");
				{
					Bank->AddSlider("Average Linear Speed Damping", &LinearSpeedDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearSpeedDamping), "Controls the average of all linear speed damping values.");
					Bank->AddSlider("Linear Speed Damping X", &LinearSpeedXDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearSpeedXDamping), "");
					Bank->AddSlider("Linear Speed Damping Y", &LinearSpeedYDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearSpeedYDamping), "");
					Bank->AddSlider("Linear Speed Damping Z", &LinearSpeedZDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearSpeedZDamping), "");
				}
				Bank->PopGroup();
				Bank->PushGroup("Linear Velocity Damping", false, "Slows down objects traveling at medium speeds");
				{
					Bank->AddSlider("Average Linear Velocity Damping", &LinearVelocityDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearVelocityDamping), "Controls the average of all linear velocity damping values.");
					Bank->AddSlider("Linear Velocity Damping X", &LinearVelocityXDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearVelocityXDamping), "");
					Bank->AddSlider("Linear Velocity Damping Y", &LinearVelocityYDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearVelocityYDamping), "");
					Bank->AddSlider("Linear Velocity Damping Z", &LinearVelocityZDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearVelocityZDamping), "");
				}
				Bank->PopGroup();
				Bank->PushGroup("Linear Velocity Squared Damping", "Slows down objects traveling at high speeds");
				{
					Bank->AddSlider("Average Linear Velocity Squared Damping", &LinearVelocitySquaredDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearVelocitySquaredDamping), "Controls the average of all linear velocity squared damping values");
					Bank->AddSlider("Linear Velocity Squared Damping X", &LinearVelocitySquaredXDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearVelocitySquaredXDamping), "");
					Bank->AddSlider("Linear Velocity Squared Damping Y", &LinearVelocitySquaredYDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearVelocitySquaredYDamping), "");
					Bank->AddSlider("Linear Velocity Squared Damping Z", &LinearVelocitySquaredZDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &LinearVelocitySquaredZDamping), "");
				}
				Bank->PopGroup();
			}
			Bank->PopGroup();
			Bank->PushGroup("Angular Damping", false, "Damps the angular velocity of objects.");
			{
				Bank->AddSlider("Average Angular Damping", &AngularDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularDamping), "Controls the average of all angular damping values.");
				Bank->PushGroup("Angular Speed Damping", false, "Slows down objects rotating at low speeds");
				{
					Bank->AddSlider("Average Angular Speed Damping", &AngularSpeedDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularSpeedDamping), "Controls the average of all angular speed damping values.");
					Bank->AddSlider("Angular Speed Damping X", &AngularSpeedXDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularSpeedXDamping), "");
					Bank->AddSlider("Angular Speed Damping Y", &AngularSpeedYDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularSpeedYDamping), "");
					Bank->AddSlider("Angular Speed Damping Z", &AngularSpeedZDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularSpeedZDamping), "");
				}
				Bank->PopGroup();
				Bank->PushGroup("Angular Velocity Damping", false, "Slows down objects rotating at medium speeds");
				{
					Bank->AddSlider("Average Angular Velocity Damping", &AngularVelocityDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularVelocityDamping), "Controls the average of all angular velocity damping values.");
					Bank->AddSlider("Angular Velocity Damping X", &AngularVelocityXDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularVelocityXDamping), "");
					Bank->AddSlider("Angular Velocity Damping Y", &AngularVelocityYDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularVelocityYDamping), "");
					Bank->AddSlider("Angular Velocity Damping Z", &AngularVelocityZDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularVelocityZDamping), "");
				}
				Bank->PopGroup();
				Bank->PushGroup("Angular Velocity Squared Damping", "Slows down objects rotating at high speeds");
				{
					Bank->AddSlider("Average Angular Velocity Squared Damping", &AngularVelocitySquaredDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularVelocitySquaredDamping), "Controls the average of all angular velocity squared damping values");
					Bank->AddSlider("Angular Velocity Squared Damping X", &AngularVelocitySquaredXDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularVelocitySquaredXDamping), "");
					Bank->AddSlider("Angular Velocity Squared Damping Y", &AngularVelocitySquaredYDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularVelocitySquaredYDamping), "");
					Bank->AddSlider("Angular Velocity Squared Damping Z", &AngularVelocitySquaredZDamping.Average, 0.0f, MAX_DAMPING, 0.01f,datCallback(MFA1(fragTuneStruct::AdjustDamping), this, &AngularVelocitySquaredZDamping), "");
				}
				Bank->PopGroup();
			}
			Bank->PopGroup();
		}
	    Bank->PopGroup();
	    //Bank->AddToggle("Attach Rope At Bottom", &type->m_AttachBottomEnd, NullCB, "Shoot a probe out from the bottom of the rope to find something to hook on to");
		Bank->AddToggle("Disable activation", &type->m_Flags, fragType::DISABLE_ACTIVATION, NullCB, "Prevent the fragment from being activated on collision, can be overridden on a per instance basis");
		Bank->AddToggle("Disable breaking", &type->m_Flags, fragType::DISABLE_BREAKING, NullCB, "Prevent the fragment from being broken on collision, can be overridden on a per instance basis");
		//Bank->AddToggle("Clone Bound Parts", &type->m_Flags, fragType::CLONE_BOUND_PARTS_IN_CACHE, NullCB, "When initializing the cache entry, make a copy of all the bounds instead of referencing them from the type. Only use this if game code is going to be modifying the bounds.");

#if ENABLE_FRAGMENT_EVENTS
		if (evtTypeManagerSingleton::IsInstantiated())
		{
			if (type->m_CollisionEventset)
			{
				Bank->PushGroup("Collision Events - type", true, "Collision events which are tied to collisions anywhere on the object");
				PARSER.AddWidgets(*Bank, (evtSet*)type->m_CollisionEventset);
				type->m_CollisionEventset->AddWidgets(*Bank);
				Bank->PopGroup();
			}
			else
			{
				Bank->AddButton("Collision Events - type", datCallback(MFA(fragTuneStruct::CreateTypeCollisionEventsCallback), this));
			}
		}
#endif // ENABLE_FRAGMENT_EVENTS

		if (InstanceTuningEnabled)
		{
			Bank->AddButton("* Reset", datCallback(MFA1(fragTuneStruct::ResetInstance), this));
			Bank->AddButton("* Put Into Cache", datCallback(MFA1(fragTuneStruct::PutIntoCache), this));
		}
    }

	if (type->GetPhysics(currentLOD)->GetNumChildren()>0)
	{
        if (componentSelection > 0)
        {
            fragTypeChild* child = type->GetPhysics(currentLOD)->GetChild(componentSelection);
            int groupIndex = child->GetOwnerGroupPointerIndex();
            AddGroupWidgets(*Bank, groupIndex, type);
        }
        else
        {
		    u32 numGroups = type->GetPhysics(currentLOD)->GetRootGroupCount();

		    for (u32 groupIndex = 0; groupIndex < numGroups; ++groupIndex)
		    {
			    AddGroupWidgets(*Bank, groupIndex, type);
		    }
        }
	}
	else if (fragTypeChild* rootChild = type->GetRootChild())
	{
		if (rootChild->GetDamagedEntity())
		{
			Bank->AddSlider("UndamagedMass",  rootChild->GetUndamagedMassPtr(),  0.0f, 1000000.0f, 0.1f, datCallback(MFA1(fragTuneStruct::AdjustChildMass), this, NULL), "The mass of the object before it is damaged");
			Bank->AddSlider("DamagedMass",    rootChild->GetDamagedMassPtr(),    0.0f, 1000000.0f, 0.1f, NullCB, "The mass of the object after it is damaged");
		}
		else
		{
			Bank->AddSlider("Mass",  rootChild->GetUndamagedMassPtr(),  0.0f, 1000000.0f, 0.1f, datCallback(MFA1(fragTuneStruct::AdjustChildMass), this, NULL), "The mass of the object");
		}
	}

    if (componentSelection == 0)
    {
	    Bank->AddButton("Add Self Collision", datCallback(MFA1(fragTuneStruct::AddSelfCollision), this, type), "Create a new self collision pair");

	    for (int sc = 0; sc < type->GetPhysics(currentLOD)->GetNumSelfCollisions(); ++sc)
	    {
		    Bank->AddCombo("from", &type->GetPhysics(currentLOD)->m_SelfCollisionA[sc], type->GetPhysics(currentLOD)->GetNumChildGroups() + 1, type->GetPhysics(currentLOD)->GetGroupNames(), datCallback(MFA1(fragTuneStruct::CheckForRemoveSelfCollision), this, type), "Half of a collision pair, select a part here and another part in the widget just below, and self collisions will occur between those two parts. To remove the pair, select (remove self collision) on both halves of the pair.");
		    Bank->AddCombo("to", &type->GetPhysics(currentLOD)->m_SelfCollisionB[sc], type->GetPhysics(currentLOD)->GetNumChildGroups() + 1, type->GetPhysics(currentLOD)->GetGroupNames(), datCallback(MFA1(fragTuneStruct::CheckForRemoveSelfCollision), this, type), "Half of a collision pair, select a part here and another part in the widget just above, and self collisions will occur between those two parts. To remove the pair, select (remove self collision) on both halves of the pair.");
	    }

        TypeTotalUndamagedMass = type->GetPhysics(currentLOD)->GetTotalUndamagedMass();
    }

	if (fragType::GetAudioConstructCB())
	{
		Bank->AddButton("Reset Audio Type", datCallback(CFA1(fragTuneStruct::ResetAudioType), type));
	}

	// PSS TO DO: if you want, add buttons here for exporting all attached particles as (1) a particle script or (2) as a fire script
}

void fragTuneStruct::AddArchetypeWidgets(fragType* type)
{
	sysCriticalSection criticalSection(m_TunerCriticalSectionToken);
	if(Verifyf(type,"Can't add NULL fragType to fragTuneStruct"))
	{
		if (NumComboItems < MaxNumComboItems)
		{
			ComboTypes[NumComboItems] = type;
			SAFE_ADD_KNOWN_REF(ComboTypes[NumComboItems]);
			++NumComboItems;
		}
		else
		{
			Assertf(false,"Trying to add to many fragments to the combo drop down list. Not adding %s.", type->GetTuneName());
		}
	}
}

void fragTuneStruct::RemoveArchetypeWidgets(fragType* type)
{
	sysCriticalSection criticalSection(m_TunerCriticalSectionToken);
#if DEBUG_FRAG_TUNE_STRUCT
	Displayf("fragTuneStruct::RemoveArchetypeWidgets %s", type->GetBaseName());
#endif

	if(!(Verifyf(type,"Can't remove NULL fragType from fragTuneStruct")))
	{
		return;
	}

    if (NumComboItems > 0)
    {
		ASSERT_ONLY(bool bFound = false;)
        for (int i = 0; i < NumComboItems; ++i)
        {
            // find our combo
            if ( ComboTypes[i] == type )
            {
				ASSERT_ONLY(bFound = true;)
#if DEBUG_FRAG_TUNE_STRUCT
				Displayf("fragTuneStruct::RemoveArchetypeWidgets %s @ combo index %d", type->GetBaseName(), i);
#endif
                for (int j = i; j < NumComboItems-1; ++j)
                {
                    // shift trailing combos down
					SAFE_REMOVE_KNOWN_REF(ComboTypes[j]);
                    ComboTypes[j] = ComboTypes[j+1];
					SAFE_ADD_KNOWN_REF(ComboTypes[j]);
                }
                
                --NumComboItems;
				SAFE_REMOVE_KNOWN_REF(ComboTypes[NumComboItems]);

				// Ensure this fragType is no longer selected.
				if(type == CurrentFragType)
				{
					SetType(0, ComponentSelection);
				}
				else 
				{
					if(ComboSelection > i)
						--ComboSelection;
					if(TypeCombo)
					{
						const char** comboNames = Alloca(const char*, NumComboItems+1);
						comboNames[0] = "No selection";
						for (int i = 0; i < NumComboItems; ++i)
							comboNames[i+1] = ComboTypes[i]->GetBaseName();

						TypeCombo->UpdateCombo("Type", &ComboSelection, NumComboItems+1, comboNames, datCallback(MFA(fragTuneStruct::SelectFromTypeComboCb), this), "Select the type you want to tune");
					}
				}

                break;
            }
        }
		Assertf(bFound,"Failed to find type %s in fragTuneStruct",type->GetBaseName());
    }
}

bkGroup* fragTuneStruct::GetWidgetsForType(const fragType* type)
{
	if (ComboSelection <= NumComboItems && ComboTypes[ComboSelection-1] == type)
	{
		return TypeBank;
	}
	else
	{
		return NULL;
	}
}

void fragTuneStruct::AddSelfCollision(fragType* type)
{
	int currentLOD = 0;

	Assert(type);

	int numSelfCollisions = type->GetPhysics(currentLOD)->GetNumSelfCollisions();
	Assert(numSelfCollisions < 255);
	type->GetPhysics(currentLOD)->SetNumSelfCollisions((u8)numSelfCollisions + 1);
	type->GetPhysics(currentLOD)->SetSelfCollision(numSelfCollisions, (u8)0, (u8)0);

	m_RecreateWidgets = true;
}

void fragTuneStruct::CheckForRemoveSelfCollision(fragType* type)
{
	int currentLOD = 0;

	Assert(type);

	int numSelfCollisions = type->GetPhysics(currentLOD)->GetNumSelfCollisions();

	int foundPairToRemove = numSelfCollisions;
	for (int sc = 0; sc < numSelfCollisions && foundPairToRemove == numSelfCollisions; ++sc)
	{
		if (type->GetPhysics(currentLOD)->m_SelfCollisionA[sc] == type->GetPhysics(currentLOD)->GetNumChildGroups() &&
			type->GetPhysics(currentLOD)->m_SelfCollisionB[sc] == type->GetPhysics(currentLOD)->GetNumChildGroups())
		{
			foundPairToRemove = sc;
		}
	}

	if (foundPairToRemove < numSelfCollisions)
	{
		for (int sc = foundPairToRemove; sc < numSelfCollisions - 1; ++sc)
		{
			type->GetPhysics(currentLOD)->m_SelfCollisionA[sc] = type->GetPhysics(currentLOD)->m_SelfCollisionA[sc + 1];
			type->GetPhysics(currentLOD)->m_SelfCollisionB[sc] = type->GetPhysics(currentLOD)->m_SelfCollisionB[sc + 1];
		}

		type->GetPhysics(currentLOD)->SetNumSelfCollisions((u8)numSelfCollisions - 1);

		m_RecreateWidgets = true;
	}
}

void fragTuneStruct::ResetInstance()
{
	if (CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			selectedInst->Reset();

			if (sm_UpdateEntityFunctor.IsBound())
			{
				sm_UpdateEntityFunctor(selectedInst->GetUserData());
			}
		}
	}
}

void fragTuneStruct::PutIntoCache()
{
	if (CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			if (!selectedInst->GetCached())
			{
				selectedInst->PutIntoCache();
			}
		}
	}
}

void fragTuneStruct::BreakGroup(int groupIndex)
{
	if (CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			if(groupIndex == 0)
			{
				selectedInst->BreakOffRoot();
			}
			else
			{
				selectedInst->BreakOffAboveGroup(groupIndex);
			}
		}
	}
}

void fragTuneStruct::DeleteAboveGroup(int groupIndex)
{
	if (CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			selectedInst->DeleteAboveGroup(groupIndex);
		}
	}
}

void fragTuneStruct::RestoreAboveGroup(int groupIndex)
{
	if (CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			selectedInst->RestoreAboveGroup(groupIndex);
		}
	}
}

void fragTuneStruct::LatchGroup(int groupIndex)
{
	if (CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			selectedInst->CloseLatchAbove(groupIndex);
		}
	}
}

void fragTuneStruct::UnlatchGroup(int groupIndex)
{
	if (CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			selectedInst->OpenLatchAbove(groupIndex);
		}
	}
}

void fragTuneStruct::AdjustInstanceDamageHealth(int groupIndex)
{
	if (CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			if (!selectedInst->GetCached())
			{
				selectedInst->PutIntoCache();
			}

			selectedInst->SetGroupDamageHealth(groupIndex, m_DamageHealthProxies[groupIndex]);
		}
	}
}

void fragTuneStruct::AdjustInstanceWeaponHealth(int groupIndex)
{
	if (CurrentFragInstLevelIndex != phInst::INVALID_INDEX)
	{
		if (PHLEVEL->IsLevelIndexGenerationIDCurrent(CurrentFragInstLevelIndex, CurrentFragInstGenerationId))
		{
			fragInst* selectedInst = static_cast<fragInst*>(PHLEVEL->GetInstance(CurrentFragInstLevelIndex));

			if (!selectedInst->GetCached())
			{
				selectedInst->PutIntoCache();
			}

			if (fragCacheEntry* entry = selectedInst->GetCacheEntry())
			{
				entry->GetHierInst()->groupInsts[groupIndex].weaponHealth = m_WeaponHealthProxies[groupIndex];
			}
		}
	}
}

void fragTuneStruct::AddWidgets( bkBank &bank)
{
	Bank = &bank;

	bank.PushGroup("Global", false);
	bank.AddButton("Save", datCallback(MFA(fragTuneStruct::Save), this));
	bank.AddSlider("LOD distance", &LodDistanceBias, 0.0, 10000.0f, 1.0f);
	PARSER.AddWidgets(bank, this);
	bank.AddToggle("No Latching",&fragTypeGroup::sm_NoLatching);
	bank.AddToggle("Disable Composite BVH Generation", &fragCacheEntry::sm_DisableCompositeBvhGeneration);
	bank.PopGroup();

	bank.AddButton("Refresh List", datCallback(MFA(fragTuneStruct::RefreshTypeList), this));
	bank.AddToggle("Left click selects frag type", &LeftClickSelectEnabled, NullCallback, "Enable this if you want to click to select frag types.");
	bank.AddToggle("* Instance tuning", &InstanceTuningEnabled, NullCallback, "Enable this if you want to tune the instance you clicked in.");
	bank.AddToggle("Select whole type when clicking", &SelectWholeTypeEnabled, NullCallback, "Enable this to select the entire type when tuning fragments.");
	bank.AddToggle("Debug draw selected frag", &DebugDrawSelectedFrag, NullCallback, "Enable this if you want the selected frag to have it's bounds rendered.");
	bank.AddToggle("Wake all affected objects on edit", &WakeAllAffectedFragsOnEdit, NullCallback, "Enable this if you want all frags of the selected type to activate whenever a tuning change is made below.");
	TruncateCount = bank.GetNumWidgets();
	RefreshTypeList(); // not needed the first time around, but if a selection is made and frag type drawing is turned on before a soft reset this will prevent a crash.
}

void fragTuneStruct::RefreshTypeList()
{
	sysCriticalSection criticalSection(m_TunerCriticalSectionToken);

	//fragType* selectedType = ComboTypes[ComboSelection];
	fragType* selectedType = CurrentFragType;

	for (int i=0; i<NumComboItems; i++)
	{
		SAFE_REMOVE_KNOWN_REF(ComboTypes[i]);
	}
	std::sort(&ComboTypes[0], &ComboTypes[NumComboItems], BaseNamePred());
	for (int i=0; i<NumComboItems; i++)
	{
		SAFE_ADD_KNOWN_REF(ComboTypes[i]);
	}

	if (CurrentFragType)
	{
		fragType** found = std::lower_bound(&ComboTypes[0], &ComboTypes[NumComboItems], selectedType, BaseNamePred());
		if(*found == selectedType)
		{
			ComboSelection = static_cast<int>(found - ComboTypes + 1);
		}
		else
		{
			ComboSelection = 0;
		}
	}

	Bank->Truncate(TruncateCount);

	const char** comboNames = rage_new const char*[NumComboItems+1];
	comboNames[0] = "No selection";
	for (int i = 0; i < NumComboItems; ++i)
	{
		comboNames[i+1] = ComboTypes[i]->GetBaseName();
	}

	TypeCombo = Bank->AddCombo("Type", &ComboSelection, NumComboItems+1, comboNames, datCallback(MFA(fragTuneStruct::SelectFromTypeComboCb), this), "Select the type you want to tune");

	delete [] comboNames;

	if (CurrentFragType)
	{
		RegenerateTuningWidgets();
	}
}

void fragTuneStruct::SelectFromTypeComboCb()
{
	ComponentSelection = 0;
	if ((ComboSelection > 0) && (ComboSelection <= NumComboItems))
	{
		if (CurrentFragType != ComboTypes[ComboSelection-1])
		{
			SetType(ComboTypes[ComboSelection-1]);
		}
	}
	else
	{
		CurrentFragType = 0;
		CurrentFragInstLevelIndex = phInst::INVALID_INDEX;
		CurrentFragInstGenerationId = phInst::INVALID_INDEX;
	}
    RegenerateTuningWidgets();
}

void fragTuneStruct::RegenerateTuningWidgets()
{
    if (ComboSelection <= NumComboItems)
    {
        Bank->Truncate(TruncateCount + 1);
        AddTuningWidgetsToBank(GetTypeBeingTuned(), ComponentSelection);
    }
}

void fragTuneStruct::DestroyWidgets(bkBank &/*ignored*/)
{
	Bank = NULL;
	TypeCombo = NULL;
}

void fragTuneStruct::Save(const char* filename)
{
	if (filename)
	{
		PARSER.SaveObject(filename, "xml", this);
	}
	else
	{
		ASSET.PushFolder(ForcedTuningPath);
		PARSER.SaveObject("fragment", "xml", this);
		ASSET.PopFolder();
	}
}

#endif // __BANK

} // namespace rage

