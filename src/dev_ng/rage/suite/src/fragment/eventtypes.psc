<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef name="fragSnuffEvent" type="::rage::fragSnuffEventInstance" base="::rage::evtInstance">
<float name="m_CrossedHealth" min="-2.0f" max="1.0f" step="0.05f"/>
<float name="m_ChanceOfSnuff" min="0.0f" max="1.0f" step="0.05f"/>
<int name="m_SnuffComponent" min="-1" max="128"/>
</structdef>

<structdef name="fragDamageEvent" type="::rage::fragDamageEventInstance" base="::rage::evtInstance">
<float name="m_CrossedHealth" min="-2.0f" max="1.0f" step="0.05f"/>
<float name="m_ChanceOfDamage" min="0.0f" max="1.0f" step="0.05f"/>
<int name="m_DamageComponent" min="0" max="128"/>
<float name="m_DamageHealth" min="0.0f" max="2.0f"/>
</structdef>

<structdef name="fragAnimEvent" type="::rage::fragAnimEventInstance" base="::rage::evtInstance">
<string name="m_AnimName" type="member" size="64"/>
<float name="m_Speed" min="0.0f" max="100.0f" step="0.05f"/>
<float name="m_Start" min="0.0f" max="1000.0f" step="0.033333333333333333333f"/>
<float name="m_End" min="-1.0f" max="1000.0f" step="0.0333333333333333333333f"/>
<bool name="m_Looping"/>
</structdef>

<structdef name="fragPaneFrameBrokenEvent" type="::rage::fragPaneFrameBrokenEventInstance" base="::rage::evtInstance">
<u8 name="m_GroupIndex" min="0" max="255" step="1"/>
<u8 name="m_FrameFlags" min="0" max="15" step="1"/>
<u8 name="m_CrackType" min="0" max="16" step="1"/>
<u8 name="m_BrokenEventFlags" min="0" max="255" step="1" hideWidgets="true"/>
</structdef>

</ParserSchema>