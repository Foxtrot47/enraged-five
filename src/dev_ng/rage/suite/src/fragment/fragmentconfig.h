// 
// fragment/fragmentconfig.h 
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 
#ifndef FRAGMENT_CONFIG_H
#define FRAGMENT_CONFIG_H

#include "diag/channel.h"

#define FRAGMENT_OPTIMISATIONS_OFF	0

#if FRAGMENT_OPTIMISATIONS_OFF
#define FRAGMENT_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else	//FRAGMENT_OPTIMISATIONS_OFF
#define FRAGMENT_OPTIMISATIONS()
#endif	//FRAGMENT_OPTIMISATIONS_OFF

#define ENABLE_FRAGMENT_EVENTS (1)
#define ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES (1)

#if ENABLE_FRAGMENT_EVENTS
#define FRAGMENT_EVENT_PARAM(X) , X
#define FRAGMENT_EVENT_ONLY(X) X
#else // ENABLE_FRAGMENT_EVENTS
#define FRAGMENT_EVENT_PARAM(X)
#define FRAGMENT_EVENT_ONLY(X)
#endif // ENABLE_FRAGMENT_EVENTS

#define FRAGMENT_STORE_ANG_INERTIA_IN_CHILD (0)

// DOM-IGNORE-BEGIN
RAGE_DECLARE_CHANNEL(Fragment)

#define fragAssert(cond)					RAGE_ASSERT(Fragment,cond)
#define fragAssertf(cond,fmt,...)			RAGE_ASSERTF(Fragment,cond,fmt,##__VA_ARGS__)
#define fragFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(Fragment,cond,fmt,##__VA_ARGS__)
#define fragVerifyf(cond,fmt,...)			RAGE_VERIFYF(Fragment,cond,fmt,##__VA_ARGS__)
#define fragErrorf(fmt,...)					RAGE_ERRORF(Fragment,fmt,##__VA_ARGS__)
#define fragWarningf(fmt,...)				RAGE_WARNINGF(Fragment,fmt,##__VA_ARGS__)
#define fragDisplayf(fmt,...)				RAGE_DISPLAYF(Fragment,fmt,##__VA_ARGS__)
#define fragDebugf1(fmt,...)				RAGE_DEBUGF1(Fragment,fmt,##__VA_ARGS__)
#define fragDebugf2(fmt,...)				RAGE_DEBUGF2(Fragment,fmt,##__VA_ARGS__)
#define fragDebugf3(fmt,...)				RAGE_DEBUGF3(Fragment,fmt,##__VA_ARGS__)
#define fragLogf(severity,fmt,...)			RAGE_LOGF(Fragment,severity,fmt,##__VA_ARGS__)
#define fragCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,Fragment,severity,fmt,##__VA_ARGS__)
// DOM-IGNORE-END

#endif // FRAGMENT_CONFIG_H
