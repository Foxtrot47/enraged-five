// 
// fragment/typephysicslod.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_TYPEPHYSICSLOD_H
#define FRAGMENT_TYPEPHYSICSLOD_H

#include "typechild.h"
#include "typegroup.h"

#include "atl/bitset.h"
#include "data/struct.h"
#include "data/safestruct.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "phbound/boundcomposite.h"
#include "physics/archetype.h"
#include "paging/base.h"
#include "paging/array.h"

#define PHYSICS_LOD_GROUP_SHARING 1

namespace rage {

class phArticulatedBodyType;
class fragPhysicsLOD;
class phBoundComposite;

#define FRAG_MAX_NUM_CHILDREN 256
#define FRAG_MAX_NUM_GROUPS 256

typedef atFixedBitSet<FRAG_MAX_NUM_CHILDREN> fragChildBits;
typedef atFixedBitSet<FRAG_MAX_NUM_GROUPS> fragGroupBits;

#if HACK_GTA4	// keep as a gta change only
const float ANG_INERTIA_MAX_RATIO = 1.0e-4f;
#else
const float ANG_INERTIA_MAX_RATIO = 1.0e-3f;
#endif

static const char s_RemoveSelfCollisionString[] = "(remove collision pair)";

// This class holds physics type data (that used to be scattered across the fragType) for one or more levels of detail
class fragPhysicsLODGroup : public pgBase
{
public:

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	DECLARE_PLACE(fragPhysicsLODGroup);

	fragPhysicsLODGroup();
	fragPhysicsLODGroup(fragPhysicsLODGroup *copyLODGroup); // deep copy constructor
	fragPhysicsLODGroup(datResource &rsc);
	~fragPhysicsLODGroup();

	void CreateLOD(int index);
	fragPhysicsLOD * GetLODByIndex(int index);
	const fragPhysicsLOD * GetLODByIndex(int index) const; // const version
	void SetLOD(int index, fragPhysicsLOD *lod);

protected:

	datOwner<fragPhysicsLOD>	m_HighLOD;
	datOwner<fragPhysicsLOD>	m_MediumLOD;
	datOwner<fragPhysicsLOD>	m_LowLOD;
};


// This class holds physics type data for a single LOD
class fragPhysicsLOD : public pgBase
{
	friend class fragType;
	friend class fragTuneStruct;

public:

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif

	DECLARE_PLACE(fragPhysicsLOD);

	fragPhysicsLOD();
	fragPhysicsLOD(fragPhysicsLOD *copyLOD); // deep copy constructor
	fragPhysicsLOD(datResource &rsc);
	~fragPhysicsLOD();

	fragTypeChild** GetAllChildren() const;
	fragTypeChild** GetChildren() const;
	fragTypeChild* GetChild(int index) const;
	phBoundComposite* GetCompositeBounds() const;
	const Vector3* GetDampingConstant() const;
	Vector3& GetDampingConstant(int index);
	const Vector3& GetDampingConstant(int index) const;
	const char** GetGroupNames() const;
	datOwner<fragTypeGroup>* GetAllGroups() const;
	datOwner<fragTypeGroup> GetGroup(int index) const;
	float GetLargestAngInertia() const;
	void SetLargestAngInertia(float set);
	const Matrix34* GetLinkAttachmentMatrices() const;
	pgArray<Matrix34>* GetLinkAttachmentArray() const;
	Vec3V_Out ComputeLinkPositionInInstance(int childIndex) const;
	void ComputeInstanceFromLinkMatrix(int childIndex, Mat34V_InOut instanceFromLink) const;
#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	const float* GetMinBreakingImpulses() const;
#endif // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	f32 GetMinMoveForce() const;
	u8 GetRootGroupCount() const;
	u8 GetNumChildren() const;
	u8 GetNumBonyChildren() const;
	u8 GetNumChildGroups() const;
	u8 GetNumRootDamageRegions() const;
	phArchetypeDamp* GetArchetype(bool damagedPhys = false) const;
	int GetNumSelfCollisions() const;
	u8 GetSelfCollisionA(int i) const;
	u8 GetSelfCollisionB(int i) const;
	Vector3 GetRootCGOffset() const;
	Vector3 GetOriginalRootCGOffset() const;
	Vector3 GetUnbrokenCGOffset() const;
	float GetSmallestAngInertia() const;
	void SetSmallestAngInertia( float smallestAngInertia );
	Vector3 GetUndamagedAngInertia(u32 child) const;
	Vector3 GetDamagedAngInertia(u32 child) const;
	void SetUndamagedAngInertia(u32 child, Vector3::Vector3Param inertia);
	void SetDamagedAngInertia(u32 child, Vector3::Vector3Param inertia);
	void SetMinMoveForce(float minMoveForce) { m_MinMoveForce = minMoveForce; }
	datOwner<phArticulatedBodyType> GetBodyType() { return m_BodyType; } 

	float GetTotalUndamagedMass() const;
	Vector3 GetTotalUndamagedAngInertia() const;

	// PURPOSE: Returns the group index for the group with name groupName, or return -1 if not found.
	int FindGroupIndexFromName(const char* groupName) const;

	void SetPhysicsArchetypeMassFromChildren();

	void ComputeGroupTotalMasses();

	// PURPOSE: Compute the angular inertia limits on this LOD based on the damaged and undamaged angular inertias
	void ComputeAngularInertiaLimits();

	// PURPOSE: Compute the angular inertia of each child using its mass and bound's mass distribution
	void ComputeChildAngularInertias();

	// PURPOSE: Ensure the angular inertia is not too small in any component. If it is too small, the
	//          angular inertia and mass will be scaled by the same value until the angular inertia is acceptable.
	// PARAMS:
	//   angularInertia - The angular inertia to clamp, this parameter will be modified by the function if it is too small.
	//   mass - The mass that goes along with the angular inertia, it will scale by whatever the angularInertia is scaled by.
	void ClampAngularInertia(Vec3V_InOut angularInertia, ScalarV_InOut mass);

	void SetNumSelfCollisions(u8 numSelf);
	void SetSelfCollision(int i, u8 a, u8 b);
	bool AddSelfCollision(const char* a, const char* b);

	void SetDamping(int type, Vector3::Param damping);
	void ApplyDamping(phArchetypeDamp* archetype) const;

	bool IsAllGlass() const;

	bool IsGroupDamageable(int groupIndex) const;
	bool IsChildsGroupDamageable(int childIndex) const;

	// PURPOSE: Compute the damaged/undamaged CG, angular inertia, and mass of a child
	// PARAMS:
	//   childIndex - the child to get the information from
	//   instanceFromChild - the component matrix of the child, if not supplied this will be the original component matrix
	//   centerOfGravity - out parameter filled with the CG of the child in instance space
	//   angularInertia - out parameter filled with the rotated angular inertia of the child in instance space
	//   mass -  the mass of the child in instance space
	// NOTE:
	//   The angular inertia will only be rotated, it doesn't include the translational angular inertia since you hopefully don't know the composite CG
	//     without knowing all the child CGs. 
	void ComputeDamagedChildMassProperties(u8 childIndex, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass) const;
	void ComputeUndamagedChildMassProperties(u8 childIndex, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass) const;
	void ComputeDamagedChildMassProperties(u8 childIndex, Mat34V_In instanceFromChild, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass) const;
	void ComputeUndamagedChildMassProperties(u8 childIndex, Mat34V_In instanceFromChild, Vec3V_InOut centerOfGravity, Vec3V_InOut angularInertia, ScalarV_InOut mass) const;

	// PUPOSE: Fill the given arrays with the mass properties of the undamaged children
	// PARAMS:
	//   childMasses - array that will be filled with the masses of the children
	//   childAngularInertias - array that will be filled with the angular inertias of the children in instance space (using the original component matrix)
	//   childCGs - array that will be filled with the centers of gravity of each child in instance space (using the original component matrix)
	void ComputeUndamagedChildMassProperties(ScalarV_Ptr childMasses, Vec3V_Ptr childAngularInertias, Vec3V_Ptr childCGs) const;
	void ComputeDamagedChildMassProperties(ScalarV_Ptr childMasses, Vec3V_Ptr childAngularInertias, Vec3V_Ptr childCGs) const;

	// PUPOSE: Fill the given arrays with the mass properties of the undamaged children in the given range
	// PARAMS:
	//   firstChildIndex - index of the first child to get information on (childMasses[0] corresponds to the mass of this child)
	//   endChildIndex - one past the last child index in the array
	//   childMasses - array that will be filled with the masses of the children
	//   childAngularInertias - array that will be filled with the angular inertias of the children in instance space (using the original component matrix)
	//   childCGs - array that will be filled with the centers of gravity of each child in instance space (using the original component matrix)
	void ComputeUndamagedChildMassProperties(u8 firstChildIndex, u8 endChildIndex, ScalarV_Ptr childMasses, Vec3V_Ptr childAngularInertias, Vec3V_Ptr childCGs) const;
	void ComputeDamagedChildMassProperties(u8 firstChildIndex, u8 endChildIndex, ScalarV_Ptr childMasses, Vec3V_Ptr childAngularInertias, Vec3V_Ptr childCGs) const;


	// PUPOSE: Update all of the child masses/angular inertias from the given arrays
	// PARAMS:
	//   childMasses - masses of the children 
	//   childAngularInertias - angular inertias of the children in instance space 
	void SetUndamagedChildMassProperties(ScalarV_ConstPtr childMassses, Vec3V_ConstPtr childAngularInertias);

	// PUPOSE: Update the child masses/angular inertias of the given range from the given arrays
	// PARAMS:
	//   firstChildIndex - index of the first child to get information on (childMasses[0] corresponds to the mass of this child)
	//   endChildIndex - one past the last child index in the array
	//   childMasses - masses of the children 
	//   childAngularInertias - angular inertias of the children in instance space 
	void SetUndamagedChildMassProperties(u8 firstChildIndex, u8 endChildIndex, ScalarV_ConstPtr childMassses, Vec3V_ConstPtr childAngularInertias);

	// PURPOSE: Set the mass/angular inertia of the children in this group so that their totals match the given totals
	// PARAMS:
	//   groupIndex - index of the group
	//   newGroupMass - the new total group mass
	//   newGroupAngluarInertia - the new angular inertia of the group
	// RETURN: False if the angular inertia couldn't be set
	// NOTE: For this function to work properly the given angular inertia must be greater than the total translated angular inertia. That's
	//         the angular inertia that comes from being offset from the combined center of mass. If this happens we will set the angular 
	//         inertia as low as possible, and return false
	BoolV_Out SetUndamagedGroupMassAndAngularInertia(u8 groupIndex, ScalarV_In newGroupMass, Vec3V_In newGroupAngularInertia);

	// PURPOSE: Set the mass/angular inertia of the children in this group subtree so that their totals match the given totals
	// PARAMS:
	//   groupIndex - index of the group at the head of the subtree
	//   newGroupMass - the new total group subtree mass
	//   newGroupAngluarInertia - the new angular inertia of the group subtree
	// RETURN: False if the angular inertia couldn't be set
	// NOTE: For this function to work properly the given angular inertia must be greater than the total translated angular inertia. That's
	//         the angular inertia that comes from being offset from the combined center of mass. If this happens we will set the angular 
	//         inertia as low as possible, and return false
	BoolV_Out SetUndamagedGroupTreeMassAndAngularInertia(u8 groupIndex, ScalarV_In newGroupTreeMass, Vec3V_In newGroupTreeAngularInertia);

	// PURPOSE: Determine if the angular inertia of an object with the given rotation matrix is scalable
	// PARAMS: 
	//   rotationMatrix - the rotation matrix of the object
	// RETURN: true if the angular inertia is scalable, false otherwise
	static BoolV_Out IsAngularInertiaScalable(Mat33V_In rotationMatrix)
	{ 
		// The angular inertia is scalable if the rotation matrix is made of 90-degree rotations.
		// As long as two columns have a largest absolute element of ~1 then this should be a 90-degree matrix. 
		return IsClose(Min(MaxElement(Abs(rotationMatrix.GetCol0())),MaxElement(Abs(rotationMatrix.GetCol1()))),ScalarV(V_ONE),ScalarV(V_FLT_SMALL_3)); 
	}

	// PURPOSE: Determine if a child has a scalable angular inertia
	// PARAMS:
	//  childIndex - index of the child
	// RETURN: true if the child's angular inertia is scalable, false otherwise
	BoolV_Out IsChildAngularInertiaScalable(u8 childIndex) const { return IsAngularInertiaScalable(GetCompositeBounds()->GetCurrentMatrix(childIndex).GetMat33ConstRef()); }

	// PURPOSE: Get a bitset filled with the children that have a scalable angular inertia
	// PARAMS:
	//  childBits - out parameter bitset filled with true for child bits with scalable angular inertia
	// NOTES:
	//  A child's angular inertia can't be scaled if it has a non-90-degree rotation matrix. This is because our
	//    angular inertia estimation system can't handle that
	template <class childBitsType>
	void GetChildrenWithScalableAngularInertia(childBitsType& childBits) const
	{
		u8 numChildren = GetNumChildren();
		for(u8 childIndex = 0; childIndex < numChildren; ++childIndex)
		{
			childBits.Set(childIndex,IsChildAngularInertiaScalable(childIndex).Getb());
		}
	}

	// PURPOSE: Convert a map from group index->X to child index->X
	// PARAMS:
	//   groupMap - the group index->X map
	//   childMap - out parameter where each index will be filled with whatever the corresponding child's group has in the groupMap
	template <typename IndexType>
	void ConvertGroupMapToChildMap(const IndexType* groupMap, IndexType* childMap) const
	{
		Assert(groupMap && childMap);
		u8 numChildren = GetNumChildren();
		for(u8 childIndex = 0; childIndex < numChildren; ++childIndex)
		{
			childMap[childIndex] = groupMap[GetChild(childIndex)->GetOwnerGroupPointerIndex()];
		}
	}

	// PURPOSE: Convert a bitset of groups into a bitset of children
	// PARAMS: 
	//  groupBits - bitset representing groups
	//  childBits - out parameter where each bit will be set if it's owning group's bit was set
	template <class groupBitsType, class childBitsType>
	void ConvertGroupBitsetToChildBitset(const groupBitsType& groupBits, childBitsType& childBits) const
	{
		Assert(!childBits.AreAnySet());
		u8 numChildren = GetNumChildren();
		for(u8 childIndex = 0; childIndex < numChildren; ++childIndex)
		{
			childBits.Set(childIndex,groupBits.IsSet(GetChild(childIndex)->GetOwnerGroupPointerIndex()));
		}
	}

	// PURPOSE: Set all bits in a bitset that correspond to groups in the subtree which the given group is a root of
	// PARAMS:
	//  rootGroupIndex - the root group of the subtree
	//  groupBits - out parameter filled with groups in the subtree (must be cleared before passing to function)
	template <class groupBitsType>
	void GetGroupsAbove(u8 rootGroupIndex, groupBitsType& groupBits) const
	{
		Assert(!groupBits.AreAnySet());
		u8 numGroups = GetNumChildGroups();
		groupBits.Set(rootGroupIndex);
		// Loop over all the groups starting from the first possible child group of the root group. If the root group is one of 
		//   the whole fragment's root groups make sure we start on the first non-root group, otherwise the parent index will be 0xFF
		for(u8 groupIndex = Max((u8)(rootGroupIndex + 1),GetRootGroupCount()); groupIndex < numGroups; ++groupIndex)
		{
			groupBits.Set(groupIndex,groupBits.IsSet(GetGroup(groupIndex)->GetParentGroupPointerIndex()));
		}
	}

	// PURPOSE: Set all bits in a bitset that correspond to groups not in the subtree which the given group is a root of
	// PARAMS:
	//  rootGroupIndex - the root group of the subtree
	//  groupBits - out parameter filled with groups that aren't in the root group's subtree
	template <class groupBitsType>
	void GetGroupsNotAbove(u8 rootGroupIndex, groupBitsType& groupBits) const
	{
		groupBits.SetAll();
		u8 numGroups = GetNumChildGroups();
		groupBits.Clear(rootGroupIndex);
		// Loop over all the groups starting from the first possible child group of the root group. If the root group is one of 
		//   the whole fragment's root groups make sure we start on the first non-root group, otherwise the parent index will be 0xFF
		for(u8 groupIndex = Max((u8)(rootGroupIndex + 1),GetRootGroupCount()); groupIndex < numGroups; ++groupIndex)
		{
			groupBits.Set(groupIndex,groupBits.IsSet(GetGroup(groupIndex)->GetParentGroupPointerIndex()));
		}
	}

protected:

	// A consolidation of physics type data that used to live in the fragType
	datPadding<4>        m_Pad00;

	float				m_SmallestAngInertia;
	float				m_LargestAngInertia;

	f32					m_MinMoveForce;

	// The articulated body type
	datOwner<phArticulatedBodyType> m_BodyType;

#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	float*				m_MinBreakingImpulses;
#else // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
	datPadding<sizeof(float*)>  m_Pad2;
#endif

	Vector3 m_RootCGOffset;
	Vector3 m_OriginalRootCGOffset;

	Vector3 m_UnbrokenCGOffset;

	Vector3 m_DampingConstant[phArchetypeDamp::NUM_DAMP_TYPES];

	const char** m_GroupNames;

	datOwner<fragTypeGroup>*	m_Groups;

	// The children 
	datOwner<fragTypeChild>*	m_Children; //there are m_NumChildren of these...

	// The archetype in a fragType is used only when an instance's fragType is paged in and it is not in the cache.
	// See the fragInst class definition for more information regarding archetypes and fragInsts.
	datOwner<phArchetypeDamp>	m_PhysDampUndamaged;
	datOwner<phArchetypeDamp>	m_PhysDampDamaged;

	//used if m_NumChildren is set...
	datRef<phBoundComposite>	m_CompositeBounds;

#if !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	//for complex fragments, we need to store precomputed angular inertia values for each part
	Vector3*			m_UndamagedAngInertia;
	Vector3*			m_DamagedAngInertia;
#endif // !FRAGMENT_STORE_ANG_INERTIA_IN_CHILD

	// The link attachment of a fragment part is the *initial* position of the center of gravity of the top-most fragment part that is part of the link of which that fragment part is a part.
	// Another way to state this is that this is the *initial* link CG -> *initial* instance for the link of which this fragment part is a part.
	// As far as I can tell the orientation part of the link attachment will always be identity.
	datOwner< pgArray<Matrix34> > m_LinkAttachments;

	u8*					m_SelfCollisionA;
	u8*					m_SelfCollisionB;

	u8					m_NumSelfCollisions;
	u8					m_MaxNumSelfCollisions;
	u8					m_NumGroups;

	u8					m_RootGroupCount; //the first uRootGroupCount of ppGroups are the root groups...

	// The number of damageable regions of the root child
	u8					m_NumRootDamageRegions;
	// The number of children controlled by skeleton bones
	u8					m_NumBonyChildren;

	// Total number of children...
	u8					m_NumChildren;

#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	datPadding<5>		m_Pad1;
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	datPadding<13>       m_Pad1;
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
};


inline fragTypeChild** fragPhysicsLOD::GetAllChildren() const
{
	return (fragTypeChild**) m_Children;
}

inline fragTypeChild* fragPhysicsLOD::GetChild(int index) const
{
	TrapGE(index,m_NumChildren);
	TrapLT(index,0);
	return m_Children[index].ptr;
}


inline bool fragPhysicsLOD::IsGroupDamageable(int groupIndex) const
{
	const fragTypeGroup* group = GetGroup(groupIndex);
	return group->GetDamageHealth() != 0.0f && 
			(GetChild(group->GetChildFragmentIndex())->GetDamagedEntity() || group->GetDisappearsWhenDead() || group->GetMadeOfGlass());
}

inline bool fragPhysicsLOD::IsChildsGroupDamageable(int childIndex) const
{
	return IsGroupDamageable(GetChild(childIndex)->GetOwnerGroupPointerIndex());
}

inline phBoundComposite* fragPhysicsLOD::GetCompositeBounds() const
{
	return m_CompositeBounds.ptr;
}

inline Vector3 fragPhysicsLOD::GetUndamagedAngInertia(u32 child) const
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	return VEC3V_TO_VECTOR3(GetChild(child)->GetUndamagedAngularInertia());
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	TrapGE(child,(u32)m_NumChildren);
	return m_UndamagedAngInertia[child];
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
}

inline Vector3 fragPhysicsLOD::GetDamagedAngInertia(u32 child) const
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	return VEC3V_TO_VECTOR3(GetChild(child)->GetDamagedAngularInertia());
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	TrapGE(child,(u32)m_NumChildren);
	return m_DamagedAngInertia[child];
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
}

inline void fragPhysicsLOD::SetDamagedAngInertia(u32 child, Vector3::Vector3Param inertia)
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	GetChild(child)->SetDamagedAngularInertia(RCC_VEC3V(inertia));
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	TrapGE(child,(u32)m_NumChildren);
	m_DamagedAngInertia[child] = inertia;
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
}

inline void fragPhysicsLOD::SetUndamagedAngInertia(u32 child, Vector3::Vector3Param inertia)
{
#if FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	GetChild(child)->SetUndamagedAngularInertia(RCC_VEC3V(inertia));
#else // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
	TrapGE(child,(u32)m_NumChildren);
	m_UndamagedAngInertia[child] = inertia;
#endif // FRAGMENT_STORE_ANG_INERTIA_IN_CHILD
}

inline const Vector3* fragPhysicsLOD::GetDampingConstant() const
{
	return m_DampingConstant;
}

inline Vector3& fragPhysicsLOD::GetDampingConstant(int index)
{
	TrapGE(index,phArchetypeDamp::NUM_DAMP_TYPES);
	TrapLT(index,0);
	return m_DampingConstant[index];
}

inline const Vector3& fragPhysicsLOD::GetDampingConstant(int index) const
{
	TrapGE(index,phArchetypeDamp::NUM_DAMP_TYPES);
	TrapLT(index,0);
	return m_DampingConstant[index];
}

inline const char** fragPhysicsLOD::GetGroupNames() const
{
	return m_GroupNames;
}

inline datOwner<fragTypeGroup>* fragPhysicsLOD::GetAllGroups() const
{
	return m_Groups;
}

inline datOwner<fragTypeGroup> fragPhysicsLOD::GetGroup(int index) const
{
	TrapGE(index,m_NumGroups);
	TrapLT(index,0);
	return m_Groups[index];
}

inline float fragPhysicsLOD::GetLargestAngInertia() const
{
	return m_LargestAngInertia;
}

inline void fragPhysicsLOD::SetLargestAngInertia(float set) 
{
	m_LargestAngInertia = set;
}

inline const Matrix34* fragPhysicsLOD::GetLinkAttachmentMatrices() const
{
	return m_LinkAttachments->GetElements();
}

inline pgArray<Matrix34> *fragPhysicsLOD::GetLinkAttachmentArray() const
{
	return m_LinkAttachments;
}

inline Vec3V_Out fragPhysicsLOD::ComputeLinkPositionInInstance(int childIndex) const
{
	TrapGE(childIndex,GetNumChildren());
	TrapLT(childIndex,0);
	// link position in instance space = child position in instance space - child position in link space (link and instance space have the same orientation)
	return Subtract(GetCompositeBounds()->GetCurrentMatrix(childIndex).GetCol3(),RCC_MAT34V(GetLinkAttachmentMatrices()[childIndex]).GetCol3());
}

inline void fragPhysicsLOD::ComputeInstanceFromLinkMatrix(int childIndex, Mat34V_InOut instanceFromLink) const
{
	instanceFromLink.Set3x3(Mat33V(V_IDENTITY));
	instanceFromLink.SetCol3(ComputeLinkPositionInInstance(childIndex));
}

#if ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES
inline const float* fragPhysicsLOD::GetMinBreakingImpulses() const
{
	return m_MinBreakingImpulses;
}
#endif // ENABLE_FRAGMENT_MIN_BREAKING_IMPULSES

inline f32 fragPhysicsLOD::GetMinMoveForce() const
{
	return m_MinMoveForce;
}

inline u8 fragPhysicsLOD::GetRootGroupCount() const
{
	return m_RootGroupCount;
}

inline u8 fragPhysicsLOD::GetNumChildren() const
{
	return m_NumChildren;
}

inline u8 fragPhysicsLOD::GetNumBonyChildren() const
{
	return m_NumBonyChildren;
}

inline u8 fragPhysicsLOD::GetNumChildGroups() const
{
	return m_NumGroups;
}

inline u8 fragPhysicsLOD::GetNumRootDamageRegions() const
{
	FastAssert(m_NumRootDamageRegions >= 1);
	return m_NumRootDamageRegions;
}

inline phArchetypeDamp* fragPhysicsLOD::GetArchetype(bool damagedPhys) const
{
	return damagedPhys ? m_PhysDampDamaged : m_PhysDampUndamaged;
}

inline int fragPhysicsLOD::GetNumSelfCollisions() const
{
	return m_NumSelfCollisions;
}

inline u8 fragPhysicsLOD::GetSelfCollisionA(int i) const
{
	FastAssert(i < m_NumSelfCollisions && i >= 0);
	return m_SelfCollisionA[i];
}

inline u8 fragPhysicsLOD::GetSelfCollisionB(int i) const
{
	FastAssert(i < m_NumSelfCollisions && i >= 0);
	return m_SelfCollisionB[i];
}

inline Vector3 fragPhysicsLOD::GetRootCGOffset() const
{
	return m_RootCGOffset;
}

inline Vector3 fragPhysicsLOD::GetOriginalRootCGOffset() const
{
	return m_OriginalRootCGOffset;
}

inline Vector3 fragPhysicsLOD::GetUnbrokenCGOffset() const
{
	return m_UnbrokenCGOffset;
}

inline float fragPhysicsLOD::GetSmallestAngInertia() const
{
	return m_SmallestAngInertia;
}

inline void fragPhysicsLOD::SetSmallestAngInertia( float smallestAngInertia )
{
	m_SmallestAngInertia = smallestAngInertia;
}

inline void fragPhysicsLOD::SetDamping(int type, Vector3::Param damping)
{
	FastAssert(type >= 0 && type < phArchetypeDamp::NUM_DAMP_TYPES);
	m_DampingConstant[type] = damping;
}

}  // namespace rage

#endif // ARTICULATED_CHARACTER_LOD_H

