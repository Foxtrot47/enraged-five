// 
// fragment/event.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "eventplayer.h"

#include "atl/any.h"
#include "paging/rscbuilder.h"
#include "physics/contactiterator.h"

namespace rage {

class fragInst;
class phInst;

evtParamId fragCollisionEventPlayer::s_Position;
evtParamId fragCollisionEventPlayer::s_Normal;
evtParamId fragCollisionEventPlayer::s_RelativeVelocity;
evtParamId fragCollisionEventPlayer::s_RelativeSpeed;
evtParamId fragCollisionEventPlayer::s_Damage;
evtParamId fragCollisionEventPlayer::s_NormalizedHealthPrior;
evtParamId fragCollisionEventPlayer::s_NormalizedHealthLeft;
evtParamId fragCollisionEventPlayer::s_Inst;
evtParamId fragCollisionEventPlayer::s_FragInst;
evtParamId fragCollisionEventPlayer::s_Component;
evtParamId fragCollisionEventPlayer::s_Part;
evtParamId fragCollisionEventPlayer::s_OtherInst;
evtParamId fragCollisionEventPlayer::s_OtherComponent;
evtParamId fragCollisionEventPlayer::s_OtherPart;
evtParamId fragCollisionEventPlayer::s_ImpactIterator;

IMPLEMENT_EVENT_PLAYER(fragCollisionEventPlayer);

void fragCollisionEventPlayer::RegisterPlayerClass()
{
	s_Position				= s_ParameterScheme.AddParam<Vec3V>("position");
	s_Normal				= s_ParameterScheme.AddParam<Vec3V>("normal");
	s_RelativeVelocity		= s_ParameterScheme.AddParam<Vec3V>("relative velocity");
	s_RelativeSpeed			= s_ParameterScheme.AddParam<float>("relative speed");
	s_Damage				= s_ParameterScheme.AddParam<float>("damage");
	s_NormalizedHealthPrior	= s_ParameterScheme.AddParam<float>("health prior");
	s_NormalizedHealthLeft	= s_ParameterScheme.AddParam<float>("health left");
    s_Inst					= s_ParameterScheme.AddParam<phInst*>("instance pointer");
    s_FragInst				= s_ParameterScheme.AddParam<fragInst*>("fragment instance pointer");
	s_Component				= s_ParameterScheme.AddParam<int>("component");
	s_Part					= s_ParameterScheme.AddParam<int>("part");
	s_OtherInst				= s_ParameterScheme.AddParam<phInst*>("other instance pointer");
	s_OtherComponent		= s_ParameterScheme.AddParam<int>("other component");
	s_OtherPart				= s_ParameterScheme.AddParam<int>("other part");
    s_ImpactIterator		= s_ParameterScheme.AddParam<phContactIterator>("impact iterator");
}

void fragCollisionEventPlayer::UnregisterPlayerClass()
{
	s_ParameterScheme.DeleteParams();
}

evtParamId fragBreakEventPlayer::s_Position;
evtParamId fragBreakEventPlayer::s_Matrix;
evtParamId fragBreakEventPlayer::s_FragInst;
evtParamId fragBreakEventPlayer::s_Inst;
evtParamId fragBreakEventPlayer::s_Component;
evtParamId fragBreakEventPlayer::s_OtherInst;

IMPLEMENT_EVENT_PLAYER(fragBreakEventPlayer);

void fragBreakEventPlayer::RegisterPlayerClass()
{
	s_Position			= s_ParameterScheme.AddParam<Vec3V>("position");
    s_Matrix			= s_ParameterScheme.AddParam<Mat34V>("matrix");
    s_Inst				= s_ParameterScheme.AddParam<phInst*>("instance pointer");
    s_FragInst			= s_ParameterScheme.AddParam<fragInst*>("fragment instance pointer");
	s_Component			= s_ParameterScheme.AddParam<int>("component");
	s_OtherInst			= s_ParameterScheme.AddParam<phInst*>("other instance pointer");
}

void fragBreakEventPlayer::UnregisterPlayerClass()
{
	s_ParameterScheme.DeleteParams();
}

evtParamId fragContinuousEventPlayer::s_Matrix;
evtParamId fragContinuousEventPlayer::s_Inst;
evtParamId fragContinuousEventPlayer::s_FragInst;

IMPLEMENT_EVENT_PLAYER(fragContinuousEventPlayer)

void fragContinuousEventPlayer::RegisterPlayerClass()
{
	s_Matrix		= s_ParameterScheme.AddParam<Mat34V>("matrix"); // PARAM_MATRIX
	s_Inst			= s_ParameterScheme.AddParam<phInst*>("instance pointer");
    s_FragInst		= s_ParameterScheme.AddParam<fragInst*>("fragment instance pointer");
}

void fragContinuousEventPlayer::UnregisterPlayerClass()
{
	s_ParameterScheme.DeleteParams();
}

} // namespace rage
