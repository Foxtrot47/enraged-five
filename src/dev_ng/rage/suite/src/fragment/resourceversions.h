//
// fragment/resourceversions.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef FRAGMENT_RESOURCEVERSIONS_H
#define FRAGMENT_RESOURCEVERSIONS_H

#include "event/resourceversions.h"
#include "phcore/resourceversions.h"
#include "grcore/resourceversions.h"
#include "pheffects/cloth_verlet.h"
#include "cloth/environmentcloth.h"
#include "cloth/clothcontroller.h"

namespace rage {

enum
{
	rvFragObject = 61 + rmcResourceBaseVersion + phResourceBaseVersion + evtResourceBaseVersion + phVerletCloth::RORC_VERSION + environmentCloth::RORC_VERSION + clothController::RORC_VERSION,
};

} // namespace rage

#endif // FRAG_RESOURCE_VERSIONS
