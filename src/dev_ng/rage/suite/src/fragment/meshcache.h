// 
// fragment/meshcache.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_MESHCACHE_H 
#define FRAGMENT_MESHCACHE_H 

#include "atl/array.h"
#include "mesh/mesh.h"
#include "rmcore/drawable.h"

namespace rage {
	class grmShaderGroup;
	struct rmcTypeFileCbData;

	// cache of platform-independent mesh data used during resourcing
	class fragMeshCache : public datBase
	{
	public:
		fragMeshCache();
		~fragMeshCache();

		void LoadDrawables(rmcTypeFileCbData* data);

		const rmcDrawable& GetDrawable() const;

#if MESH_LIBRARY
		mshMaterial* GetMshMaterial(int targetLod, int targetModelIndex, int targetGeomIndex) const;
#endif
		bool GetModelAndGeomIndexFromBoneIndex(int lodIndex, int boneIndex, int& out_ModelIndex, int& out_GeomIndex) const;

		void SetShaderGroup(grmShaderGroup* in_pShaderGroup);

	private:
		void InstallMeshLoaderHook();

		void RemoveMeshLoaderHook();
#if MESH_LIBRARY
		static bool IsSkinnedMeshUsingBone(mshMaterial* pMaterial, int boneIndex);
#endif
		bool CacheMesh(mshMesh& in_mesh, const char*);

		fragMeshCache(const fragMeshCache&); // prevent accidental pass-by-value
		fragMeshCache& operator = (const fragMeshCache&);

		rmcDrawable m_Drawable;
		atArray<mshMesh*> m_meshes;
	};

	inline fragMeshCache::fragMeshCache()
	{
	}

	inline const rmcDrawable& fragMeshCache::GetDrawable() const
	{
		return m_Drawable;
	}

} // namespace rage

#endif // FRAGMENT_MESHCACHE_H 
