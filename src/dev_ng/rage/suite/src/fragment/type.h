// 
// fragment/type.h  
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FRAGMENT_TYPE_H
#define FRAGMENT_TYPE_H

#include "eventplayer.h"
#include "fragmentconfig.h"

#include "data/resource.h"
#include "data/struct.h"
#include "mesh/mesh.h"
#include "paging/base.h"
#include "paging/array.h"
#include "physics/archetype.h"
#include "fragment/typephysicslod.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "rmcore/typefileparser.h"

#include "resourceversions.h"

#ifndef ATL_DELEGATE_H
	#include "atl/delegate.h"
#endif

#define FRAG_TYPE_SHORT_NAME_LEN	1023
#define FRAG_MAX_NUM_BONES		2048

#define ENABLE_FRAG_OPTIMIZATION (1 && (__PS3 || __XENON))
#define DISABLE_FRAG_OPTIMIZATION !(ENABLE_FRAG_OPTIMIZATION)

#if ENABLE_FRAG_OPTIMIZATION
#define ENABLE_FRAG_OPTIMIZATION_ONLY(x) x
#define DISABLE_FRAG_OPTIMIZATION_ONLY(x)
#else
#define ENABLE_FRAG_OPTIMIZATION_ONLY(x)
#define DISABLE_FRAG_OPTIMIZATION_ONLY(x) x
#endif

namespace rage {

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4201)
#endif

class atBitSet;
class bgPaneModelInfoBase;
class bkGroup;
class crBoneData;
class crFrame;
class crSkeleton;
class grmMatrixSet;
class evtSet;
class fiAsciiTokenizer;
class fragDrawable;
struct fragHierarchyInst;
class fragMeshCache;
class fragTypeChild;
class fragTypeGroup;
class grcInstanceData;
class Matrix34;
class phArchetypeDamp;
class phBound;
class phBoundComposite;
class phJoint;
struct rmcTypeFileCbData;
class fragTypeEnvCloth;
class fragTypeCharCloth;
class parTreeNode;
class parTree;

/*
  PURPOSE
	Holds the bulk of the data associated with a fragObject, and is usually shared among many instances.
	
  NOTES
	Every fragObject has a fragType, although typically many fragObjects will be of a particular
	fragType. The fragType defines the total configuration and disposition of an instance of a
	fragment object as it is spawned in the world. It contains all the data necessary for drawing
	a particular fragObject instance, and inserting it into the physics level.

	A fragType does NOT contain any data that is unique to a fragObject instance. For example, it
	does not contain a particular spawning point. It also does not contain active instance data,
	which is only reserved for a fragObject instance once it has been physically interacted with:
	hit by a physical object or a projectile, for example. Active instance data is held in the
	fragCacheEntry, a pool of which is owned by the fragCacheManager.

	A fragType can be of two basic sorts: simple and complex. A simple fragType consists of just
	one fragTypeChild, and is really just a damagable object. It has a particular strength by which
	it is affixed to the "ground", and a health which, when exceeded, triggers substitution with
	a "damaged" version of its model and bounds.

	A complex fragType consists of an array of fragTypeGroups and fragTypeChilds. Each fragTypeChild
	is just like a simple fragType. The fragTypeGroups are linked together in a hierarchy and
	have an attachment strength, much like the simple fragType's attachment to the "ground", which
	determines how much force is needed to break a fragObject into pieces. Each fragTypeGroup
	essentially specifies a link between two fragTypeChilds.

<FLAG Component>
*/
class fragType : public pgBase
{

friend class fragTypeStub;
friend class fragManager;
friend class fragTuneStruct;
public:
	typedef atDelegate<void (fragType*, datResource&)> fragTypePlacementFunc;
	static const float FRAG_PRIORITY_SCALE;
	static const int RORC_VERSION = rvFragObject;

#if __RESOURCECOMPILER
	static bool ms_bCreateMatrixSet;
#endif // __RESOURCECOMPILER

	struct JointParams
	{
		JointParams()
			: dof(0),
			dof3Type(0)
		{ }

#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct &s)
		{
			STRUCT_BEGIN(JointParams);

			STRUCT_FIELD(dof);
			STRUCT_FIELD(dof3Type);
			STRUCT_FIELD(parent);
			STRUCT_FIELD(limit1min);
			STRUCT_FIELD(limit1max);
			STRUCT_FIELD(limit1soft);
			STRUCT_FIELD(limit2min);
			STRUCT_FIELD(limit2max);
			STRUCT_FIELD(limit2soft);
			STRUCT_FIELD(limit3min);
			STRUCT_FIELD(limit3max);
			STRUCT_FIELD(limit3soft);
			STRUCT_FIELD(stiffness);
			STRUCT_IGNORE(pad);
			STRUCT_FIELD(axisPos);
			STRUCT_FIELD(fixedAxisDir);
			STRUCT_FIELD(fixedLeanDir);
			STRUCT_FIELD(movingAxisDir);
			STRUCT_FIELD(movingLeanDir);

			STRUCT_END();
		}
#endif // __DECLARESTRUCT

		int dof;
		int dof3Type;
		int parent;
		float limit1min;
		float limit1max;
		float limit1soft;
		float limit2min;
		float limit2max;
		float limit2soft;
		float limit3min;
		float limit3max;
		float limit3soft;
		float stiffness;
		datPadding<12> pad;
		Vec3V axisPos;
		Vec3V fixedAxisDir;
		Vec3V fixedLeanDir;
		Vec3V movingAxisDir;
		Vec3V movingLeanDir;
	};

	DECLARE_PLACE(fragType);
	void UnPlace();
	void Delete(){};
	int Release() const { delete this; return 0; }

	fragType(int classID = -1);
	fragType(datResource& rsc);
	virtual ~fragType();

	fragPhysicsLODGroup * GetPhysicsLODGroup() { return m_PhysicsLODGroup; }
	const fragPhysicsLODGroup * GetPhysicsLODGroup() const { return m_PhysicsLODGroup; }
	void SetPhysicsLODGroup(fragPhysicsLODGroup *set) { m_PhysicsLODGroup = set; }
	inline fragPhysicsLOD * GetPhysics(int lod) { return m_PhysicsLODGroup ? m_PhysicsLODGroup->GetLODByIndex(lod) : NULL; }
	inline fragPhysicsLOD * GetPhysics(int lod) const { return m_PhysicsLODGroup ? m_PhysicsLODGroup->GetLODByIndex(lod) : NULL; }
	
	//
	// This will not do anything unless the game itself utilizes m_UnbrokenElasticity:
	//
	float GetUnbrokenElasticity() const;

	// PURPOSE: Map all of the groups to their links
	//   currentLOD - the LOD to map with
	//   groupIndexToLinkIndex - this must be non-null. It will be filled with the link index at each group index
	//   linkIndexToFirstGroupIndex - this must be non-null. It will be filled with the index of the first group in the link, at each link index
	//   numLinks - out parameter filled with the number of links. It will included latched links if mapToLatchedLinks is false.
	//   numLatchedLinks - out parameter filled with number of links. It will be non-zero only if mapToLatchedLinks is true
	//   brokenGroups - pointer to bitset where being true at a group index means the group is broken, and won't be mapped to anything. If NULL, it is assumed no groups are broken
	//   latchedGroups - pointer to pointer to bitset where being true at a group index means the group is latched. If the pointer to pointer is NULL, we will go by whether the group is
	//                   initially latched. If the pointer is valid, but it points at NULL pointer, we will assume no groups are latched
	//   mapToLatchedLinks - if this is true, we will treat latched links as unlatched. 
	// NOTE:
	//   Eventually we should be getting rid of mapToLatchedLinks because we should never be mapping that. All of the calculations when a link unlatches should be done at runtime. 
	void MapGroupsToLinks(int currentLOD, u8* groupIndexToLinkIndex, u8* linkIndexToFirstGroupIndex, u8& numLinks, u8& numLatchedJoints, atBitSet* brokenGroups = NULL, atBitSet** latchedGroups = NULL, bool mapToLatchedLinks = true) const;

	// PURPOSE: Compute all the mass properties of this type based on the component matrices in the composite and the child masses
	//          This should be called after messing with the mass or initial matrix of fragTypeChildren. It will ensure that all the fragType
	//            information is up-to-date.
	// PARAMS:
	//   currentLOD - the LOD to compute the mass properties of
	//   rootLinkCenterOfGravityOverride - if a pointer is given, this value will be used instead of the computed root link center of gravity. 
	//                                     The root link CG will be put into the composite and archetypes center of gravity. 
	// RETURN:
	//   true - this type has at least one joint, or latched link (it can be articulated). 
	// NOTES:
	//   - This will overwrite the user-set m_RootCGOffset with either rootLinkCenterOfGravityOverride if it's set or the actual root link CG
	bool ComputeMassProperties(int currentLOD, Vec3V_ConstPtr rootLinkCenterOfGravityOverride = NULL);

	// PURPOSE: Scale the mass and/or angular inertia of the undamaged child groups to a target amount
	// PARAMS:
	//  currentLOD - lod to scale the children of (only needed for call to ComputeMassProperties)
	//  newTotalMass - pointer to new total mass of the fragment. The angular inertia of the children will scale uniformly by the same amount that the mass scales
	//	newTotalAngularInertia - pointer to new total angular inertia.
	//  unscalableChildren - children that cannot have their mass/angular inertia scaled
	//  rootLinkCenterOfGravityOverride - if a pointer is given, this value will be used instead of the computed root link center of gravity. 
	// RETURN: True if the given mass properties were valid, false if they weren't. 
	//         An example of invalid properties would be have unscalable child masses add up to 100 and trying to set the total mass to 50. 
	// NOTES:
	//  -This will handle recomputing the mass properties of the fragment through ComputeMassProperties
	//  -If the target mass/angular inertia is invalid we will scale it as low as possible, which means children with 0 mass/angular inertia in some components
	BoolV_Out ScaleUndamagedChildren(int currentLOD, ScalarV_ConstPtr newTotalMass, Vec3V_ConstPtr newTotalAngularInertia, const fragChildBits& scalableChildren, Vec3V_ConstPtr rootLinkCenterOfGravityOverride = NULL);

#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct &s);
#endif // __DECLARESTRUCT

#if BASE_DEBUG_NAME
	virtual const char *GetDebugName(char * /*buffer*/, size_t /*bufferSize*/) const		{ return GetBaseName(); }
#endif // BASE_DEBUG_NAME


	bool GetAttachBottomEnd() const;
	void SetAttachBottomEnd(bool attach=true);
	size_t GetEstimatedCacheSize() const;
    size_t GetEstimatedArticulatedCacheSize() const;
	void ReportCacheSize(size_t size) const;
    void ReportArticulatedCacheSize(size_t size) const;
	fragTypeChild* GetRootChild() const;
	fragDrawable* GetCommonDrawable() const;
	fragDrawable* GetClothDrawable() const;
    fragDrawable* GetDamagedDrawable() const;
    int GetNumExtraDrawables() const;
    fragDrawable* GetExtraDrawable(int index) const;
    const char* GetExtraDrawableName(int index) const;
	const char* GetTuneName() const;
	const char* GetBaseName() const;
	bool GetNeedsCacheEntryToActivate() const;
	int GetEntityClass() const;
	bool GetHasAnyArticulatedParts() const;
	bool GetIsUserModified() const;
#if HACK_GTA4	// EUGENE_APPROVED
	// Three new flags added for Gta
	// Want them taken across to rage\dev
	bool GetForceArticulatedDamping() const;
	bool GetForceLoadCommonDrawable() const;
	bool GetForceAllocateLinkAttachments() const;
#endif
	bool GetAllocateTypeAndIncludeFlags() const;
    bool GetSkinned() const;
	bool IsModelSkinned() const;

#if ENABLE_FRAG_OPTIMIZATION
	void Draw(int currentLOD, const Matrix34& matrix, const crSkeleton* skeleton = NULL, grmMatrixSet* sharedMatrixSetOverride = NULL, fragHierarchyInst* instanceInfo = NULL) const;
	void Draw(int currentLOD, const Matrix34& matrix, Vector3::Vector3Param poi, const crSkeleton* ms = NULL, grmMatrixSet* sharedMatrixSetOverride = NULL, fragHierarchyInst* instanceInfo = NULL) const;
#else
	void Draw(int currentLOD, const Matrix34& matrix, const crSkeleton* skeleton = NULL, fragHierarchyInst* instanceInfo = NULL) const;
	void Draw(int currentLOD, const Matrix34& matrix, Vector3::Vector3Param poi, const crSkeleton* ms = NULL, fragHierarchyInst* instanceInfo = NULL) const;
#endif	

#if __BANK && __PFDRAW
	void ProfileDrawHover(fragDrawable* toDraw, const Matrix34& transformMat) const;
	void ProfileDraw(int currentLOD, const Matrix34& matrix, const crSkeleton* skeleton = NULL, fragHierarchyInst* instanceInfo = NULL) const;
	void ProfileDraw(int currentLOD, const Matrix34& matrix, Vector3::Vector3Param poi, const crSkeleton* skeleton = NULL, fragHierarchyInst* instanceInfo = NULL) const;
#endif
	u32 GetShaderBucketMask(int currentLOD) const;

	const Vector4& GetBoundingSphere() const;

	void SetUserData(void* userData);
	void* GetUserData() const;

	int GetBoneIndexFromID(u16 boneID) const;
	int GetComponentFromBoneIndex(int currentLOD, s32 nBoneIndex) const;
	int GetControllingComponentFromBoneIndex(int currentLOD, s32 nBoneIndex) const;
	int GetGroupFromBoneIndex(int currentLOD, s32 nBoneIndex) const;

	void SetGlassAttachmentBone(int bone);
	int GetGlassAttachmentBone() const;

#if HACK_GTA4
	static bool WillBeArticulated(const crBoneData& bone,
		const crJointData& jointData,
		bool& outXLimits,
		bool& outYLimits,
		bool& outZLimits,
		bool& outXTLimits,
		bool& outYTLimits,
		bool& outZTLimits);
	bool WillBeArticulated(int groupIndex) const;
#endif
	// PURPOSE: Tell if the given bone has any motion limits.
	// PARAMS:
	//	outXLimits - output to tell whether there are any rotation limits about the x axis
	//	outYLimits - output to tell whether there are any rotation limits about the y axis
	//	outZLimits - output to tell whether there are any rotation limits about the z axis
	//	outXTLimits - output to tell whether there are any translation limits along the x axis
	//	outYTLimits - output to tell whether there are any translation limits along the y axis
	//	outZTLimits - output to tell whether there are any translation limits along the z axis
	// RETURN:	true if the given bone has any motion limits, false if it does not
	static bool HasMotionLimits (const crBoneData& bone, const crJointData& jointData, bool& outXLimits, bool& outYLimits, bool& outZLimits, bool& outXTLimits, bool& outYTLimits, bool& outZTLimits);

	bool HasMotionLimits (int groupIndex) const;

	bool GetRootLinkConstraint(const Matrix34& instMatrix,
							   int& outLimitDof,
							   Vector3& outConstrainDir,
							   Vector3& outRotMin,
							   Vector3& outRotMax) const;
	int ComputeDamageRegion(int currentLOD, Vector3::Param position) const;
	void ComputeMinBreakingImpulses(int currentLOD, float* minBreakingImpulses, const atBitSet* groupBroken, phJoint** latchedJoints = NULL) const;

	int GetARTAssetID() const;
	void SetARTAssetID(int assetID);
	crSkeleton* GetInitialSkeleton();
	const crSkeleton* GetInitialSkeleton() const;
	crSkeleton* GetSharedSkeleton() const;
	grmMatrixSet* GetSharedMatrixSet() const;
	const crSkeletonData& GetSkeletonData() const;

#if ENABLE_FRAGMENT_EVENTS
	const evtSet* GetCollisionEventset() const;
	fragCollisionEventPlayer* GetCollisionEventPlayer() const;
#endif // ENABLE_FRAGMENT_EVENTS

	template <class T>
	static T* Load(const char* typeFile)
	{
		sysMemStartTemp();
		T *tuningType = rage_new T;
		sysMemEndTemp();

		T *pFragType = (T*)Load(typeFile,tuningType,rage_new T);

		sysMemStartTemp();
		delete tuningType;
		sysMemEndTemp();

		return pFragType;
	}

	template <class T>
	static T* LoadXML(const char* typeFile, const char* nmXmlFile = NULL)
	{
		T* pFragType = rage_new T;
		pFragType->LoadWithXML(typeFile,nmXmlFile);
		return pFragType;
	}

	//You should use the template version instead
	static fragType* Load(const char* typeFile,
						fragType *tuningType = NULL,
						fragType *tempType = NULL);

	bool LoadASCII(fiAsciiTokenizer& asciiTok,
				   const char* archetypeName,
				   bool needsEntity = true,
				   fragMeshCache* meshCache = NULL);

	static fragType* LoadWithTuning(fiAsciiTokenizer& asciiTok,
									fiAsciiTokenizer* tuningTok,
									const char* name,
									fragType *tuningType = NULL,
									fragType *tempType = NULL);

#if MESH_LIBRARY
	void LoadWithXML(const char* typeFile, const char* nmXmlFile = NULL);
#endif
	int LoadPartsFromXML(int currentLOD, parTreeNode* rigid, int& jointIndex);
	void LoadMatrixFromXML(const parTreeNode* matrixNode, Mat34V& matrix);

	// Support for custom script
	bool LoadPhysicsRigFromXML(int currentLOD, parTreeNode* rig);
	void LoadBoundsFromXML(int currentLOD, parTreeNode* parentJoint, parTreeNode* child, fragTypeChild** children, fragTypeGroup** groups);
	bool LoadJointsFromXML(int currentLOD, parTreeNode* parentJoint, parTreeNode* child, int &jointIndex, int parentIndex);
	void LoadBound(int currentLOD, parTreeNode* bound, fragTypeChild** children, fragTypeGroup** groups);
	bool LoadJoint(int currentLOD, parTreeNode* joint, parTreeNode* limit, int &jointIndex, int parentIndex);
	void LoadSelfCollisionsFromXML(int currentLOD, parTreeNode* root);
	void LoadMatrixFromXML_New(const parTreeNode* matrixNode, Mat34V& matrix);
	void LoadMatrixFromXML_ConvertToLeftHanded(const parTreeNode* matrixNode, Mat34V& matrix);

	void InitArticulatedLODManagerFromXMLData(int currentLOD);

	static inline fragType* GetLoadingFragType() { return sm_LoadingFragType; }
	static inline void SetLoadingFragType(fragType *value) { sm_LoadingFragType = value; }

	void ApplyTuningData(const fragType* tuningType, const fragMeshCache* meshCache);
#if ENABLE_FRAGMENT_EVENTS
	void AutoGenerateBreakEvents(int currentLOD, fragTypeChild& child, const fragMeshCache& mshmeshCache, evtSet* eventSet);
#endif // ENABLE_FRAGMENT_EVENTS

#if __BANK
	void SaveASCII(fiAsciiTokenizer& asciiTok);
#endif
	void SaveTree( int currentLOD, parTree& tree );

	bool IsBreakable() const;
	bool IsBreakingDisabled() const;
	bool IsActivationDisabled() const;

	void CopyEstimatedCacheSizes(const fragType *copyFrom);

#if HACK_GTA4	// EUGENE_APPROVED
	// Three new flags added for Gta
	// Want them taken across to rage\dev
	void SetForceArticulatedDamping();
	void SetForceLoadCommonDrawable(bool loadCommonDrawable = true);
	void SetForceAllocateLinkAttachments();
#endif
	void SetAllocateTypeAndIncludeFlags();
	void SetIsUserModified();

	int GetNumEnvCloths () const;
	const fragTypeEnvCloth* GetTypeEnvCloth (int clothIndex) const;
	fragTypeEnvCloth* GetTypeEnvCloth (int clothIndex);
#if HACK_GTA4
	void AddEnvCloth(fragTypeEnvCloth* envCloth);
	void ClearEnvCloths();
#endif

	int GetNumCharCloths () const;
	const fragTypeCharCloth* GetTypeCharCloth (int clothIndex) const;
	fragTypeCharCloth* GetTypeCharCloth (int clothIndex);
#if HACK_GTA4
	void AddCharCloth(fragTypeCharCloth* charCloth);
	void ClearCharCloths();
#endif

	datOwner<bgPaneModelInfoBase>* GetAllGlassPaneModelInfos() const;
	int GetNumGlassPaneModelInfos() const;

	void SetARTAssetID(s8 set) { m_ARTAssetID = set; }

	// Callback used by breakable glass to get instance data for the specified shader
	// Invoked in render thread only.
	const grcInstanceData& GetInstanceDataForShader(int in_shaderIndex);

	int				GetClientClassID() const		{ return m_ClientClassID; }
	static void		SetPlacementFunction(const fragTypePlacementFunc& placeFunc);
	static void		DefaultPlace(fragType *that,datResource &rsc);

	static bool IsProfileDrawAutomatic() { return sm_bProfileDrawAutomatic; }
	static void SetProfileDrawAutomatic(bool mode) { sm_bProfileDrawAutomatic = mode; }
	static void	SetUseFragManagerDefaultsInLoad(bool enable);
	static void	SetAudioConstructCB(Functor1<fragType*> fn) { sm_AudioConstructCB = fn; }
	static void	SetAudioDestructCB(Functor1<fragType*> fn) { sm_AudioDestructCB = fn; }
	static Functor1<fragType*> GetAudioConstructCB() { return sm_AudioConstructCB; }
	static Functor1<fragType*> GetAudioDestructCB() { return sm_AudioDestructCB; }

#if __RESOURCECOMPILER
	struct RagebuilderParams
	{
		RagebuilderParams() : lodSkeleton(false) {}
		bool lodSkeleton;
	};

	typedef atFunctor1<void, RagebuilderParams*> RagebuilderHook;
	static void SetRagebuilderHook(RagebuilderHook functor) { sm_ragebuilderHook = functor; }
#endif

protected:
#if MESH_LIBRARY
	void		RegisterLoaders(rmcTypeFileParser& parser, bool ignoreFragments = false);
#endif
#if __BANK
	void		WriteASCII(fiAsciiTokenizer& asciiTok);
#endif

	void		SetClientClassID(int classId)	{ m_ClientClassID = classId; }

protected:
	void LoadEntityClass(rmcTypeFileCbData *data);
	void LoadMinDamageForce(rmcTypeFileCbData* data);
	void LoadDamageHealth(rmcTypeFileCbData* data);
	void LoadRootChild(rmcTypeFileCbData* data);
	void LoadRootGroup(rmcTypeFileCbData* data);
	void LoadRootDamageRegions(rmcTypeFileCbData* data);
	void LoadMinMoveForce(rmcTypeFileCbData* data);
	void LoadAttachBottomEnd(rmcTypeFileCbData* data);
	void LoadDisableBreaking(rmcTypeFileCbData* data);
	void LoadDisableActivation(rmcTypeFileCbData* data);
	void LoadEstimatedCacheSize(rmcTypeFileCbData* data);
    void LoadEstimatedArticulatedCacheSize(rmcTypeFileCbData* data);
#if HACK_GTA4	// EUGENE_APPROVED
	// New flag added for gta (to load combined skinned and non-skinned fragment)
	// Want to take across to rage\dev
	void LoadForceLoadCommonDrawable(rmcTypeFileCbData* data);
#endif
	void LoadRootCGOffset(rmcTypeFileCbData* data);
	void LoadUnbrokenCGOffset(rmcTypeFileCbData* data);

	//
	// This will not do anything unless the game itself utilizes m_UnbrokenElasticity:
	//
	void LoadUnbrokenElasticity(rage::rmcTypeFileCbData* data);
	void LoadDampingLinearC(rmcTypeFileCbData* data);
	void LoadDampingLinearV(rmcTypeFileCbData* data);
	void LoadDampingLinearV2(rmcTypeFileCbData* data);
	void LoadDampingAngularC(rmcTypeFileCbData* data);
	void LoadDampingAngularV(rmcTypeFileCbData* data);
	void LoadDampingAngularV2(rmcTypeFileCbData* data);
	void LoadSelfCollisionCount(rmcTypeFileCbData* data);
	void LoadSelfCollision(rmcTypeFileCbData* data);
	void LoadARTAssetID(rmcTypeFileCbData* data);
#if ENABLE_FRAGMENT_EVENTS
    void LoadCollisionEventset(rmcTypeFileCbData* data);
#endif // ENABLE_FRAGMENT_EVENTS
#if MESH_LIBRARY
	void LoadEnvCloth(rmcTypeFileCbData* data);
	void LoadEnvClothCustomBound(rmcTypeFileCbData* data);
    void LoadCommonDrawables(rmcTypeFileCbData* data);
	void LoadClothDrawable(rmcTypeFileCbData* data);	
	void CreateClothDrawable(rmcTypeFileCbData* data);
#endif
	void LoadGravityFactor(rmcTypeFileCbData* data);
	void LoadBuoyancyFactor(rmcTypeFileCbData* data);

	static void RemapGroups(datOwner<fragTypeGroup>* const hierGroupList, 
							datOwner<fragTypeGroup>* linearGroupList, 
							u8 numGroupsTotal, 
							fragTypeChild** const childListOrg,
							fragTypeChild** childListRemapped,
							u8* remapIndices);

#if HACK_GTA4
	public:
#endif
	void SetNeedsCacheEntryToActivate();
#if HACK_GTA4
	protected:
#endif

	void SetHasAnyArticulatedParts();
	void SetFlags(u16 bits, bool set);

	void FindGlassParts(const fragMeshCache& meshCache);

protected:

	enum
	{
		NEEDS_CACHE_ENTRY_TO_ACTIVATE	= 1 << 0,
		HAS_ANY_ARTICULATED_PARTS		= 1 << 1,
		UNUSED							= 1 << 2,
		CLONE_BOUND_PARTS_IN_CACHE		= 1 << 3,
		ALLOCATE_TYPE_AND_INCLUDE_FLAGS	= 1 << 4,
#if HACK_GTA4	// EUGENE_APPROVED
		// Three new flags added for gta
		// Want to take them across to rage\dev, but need more bits for flags for them to fit
		FORCE_ARTICULATED_DAMPING		= 1 << 5,	// gta needs this
		FORCE_LOAD_COMMON_DRAWABLE		= 1 << 6,	// gta needs this
		FORCE_ALLOCATE_LINK_ATTACHMENTS	= 1 << 7,	// Ensures link attachment matrices are allocated in cache entry
#endif		
		BECOME_ROPE						= 1 << 10,  //Some nasty RDR2 hack.
		IS_USER_MODIFIED				= 1 << 11,   // flag to help the user keep track of fragments they modified. We can find a non-fragment solution if necessary but this was the easiest solution. 
		DISABLE_ACTIVATION				= 1 << 12,	//Disables activation on instances until the user enables
		DISABLE_BREAKING				= 1 << 13	//Disables activation on instances until the user enables
	};

	datPadding<8>       m_Pad0;

	Vector4 m_BoundingSphere;

    fragDrawable* m_CommonDrawable; // Contains data common to all the parts of the fragment type, the shader groups, etc.
    datOwner<fragDrawable>* m_ExtraDrawables;
    const char** m_ExtraDrawableNames;
    int m_NumExtraDrawables;
    int m_DamagedDrawable;

	// The mesh for this is used when undamaged, and the bound is used when there are no children...
	fragTypeChild*	m_RootChild;

	const char* m_TuneName;

	// cloth
	atArray< datOwner<fragTypeEnvCloth> > m_EnvCloth;
	atArray< datOwner<fragTypeCharCloth> > m_CharCloth;

	void*				m_UserData;

#if ENABLE_FRAGMENT_EVENTS
	datOwner<evtSet>					m_CollisionEventset;
	datOwner<fragCollisionEventPlayer>	m_CollisionEventPlayer;
#else // ENABLE_FRAGMENT_EVENTS
	datPadding<sizeof(datOwner<int>)*2,u8> m_Pad9;
#endif // ENABLE_FRAGMENT_EVENTS

	datPadding<1, u32> m_Pad6;
	datPadding<1, u32> m_Pad7;
	datPadding<1, u32> m_Pad8;

	datOwner<grmMatrixSet>	m_SharedMatrixSet;

	mutable size_t		m_EstimatedCacheSize;
	mutable size_t		m_EstimatedArticulatedCacheSize;

    u8					m_EntityClass;
	s8					m_ARTAssetID;
	bool				m_AttachBottomEnd;
	datPadding<1>       m_Pad1;
	u16					m_Flags;
	datPadding<2>       m_Pad2;
	int					m_ClientClassID;	

    //
	// This will not do anything unless the game itself utilizes m_UnbrokenElasticity:
	//
	f32					m_UnbrokenElasticity;
	f32					m_GravityFactor;
	f32					m_BuoyancyFactor;

	u8					m_GlassAttachmentBone;
	u8                  m_NumGlassPaneModelInfos;
	datPadding<2>       m_Pad3;
	datOwner<bgPaneModelInfoBase>*         m_GlassPaneModelInfos;

	datPadding<1, u32>	m_Pad5;

	fragPhysicsLODGroup*	m_PhysicsLODGroup;

	datOwner<fragDrawable>	m_CommonClothDrawable;
	datPadding<4>       m_Pad4;

	PAD_FOR_GCC_X64_8;

	static fragTypePlacementFunc	sm_PlacementNewFunc;
	static fragType* sm_LoadingFragType;
	static bool sm_bProfileDrawAutomatic;

	static bool sm_UseFragManagerDefaultsInLoad;

	// Called in constructor
	static Functor1<fragType*> sm_AudioConstructCB;

	// Called in destructor
	static Functor1<fragType*> sm_AudioDestructCB;

#if __RESOURCECOMPILER
	static RagebuilderHook sm_ragebuilderHook;

public:
	static atArray<int>* ms_pSkinnedBones;
	static bool ms_canSortBones;
#endif // __RESOURCECOMPILER
};

inline bool fragType::GetAttachBottomEnd() const
{
	return m_AttachBottomEnd;
}

inline void fragType::SetAttachBottomEnd(bool attach)
{
	m_AttachBottomEnd = attach;
}

inline size_t fragType::GetEstimatedCacheSize() const
{
	return m_EstimatedCacheSize;
}

inline size_t fragType::GetEstimatedArticulatedCacheSize() const
{
    return m_EstimatedArticulatedCacheSize;
}

inline void fragType::ReportCacheSize(size_t size) const
{
	m_EstimatedCacheSize = size;
}

inline void fragType::ReportArticulatedCacheSize(size_t size) const
{
    m_EstimatedArticulatedCacheSize = size;
}

inline fragTypeChild* fragType::GetRootChild() const
{
	return m_RootChild;
}

inline fragDrawable* fragType::GetCommonDrawable() const
{
	return m_CommonDrawable;
}

inline fragDrawable* fragType::GetClothDrawable() const
{
	return m_CommonClothDrawable;
}

inline fragDrawable* fragType::GetDamagedDrawable() const
{
    return m_DamagedDrawable == -1 ? NULL : m_ExtraDrawables[m_DamagedDrawable];
}

inline int fragType::GetNumExtraDrawables() const
{
    return m_NumExtraDrawables;
}

inline fragDrawable* fragType::GetExtraDrawable(int index) const
{
    FastAssert(index <= m_NumExtraDrawables);
    return m_ExtraDrawables[index];
}

inline const char* fragType::GetExtraDrawableName(int index) const
{
    FastAssert(index <= m_NumExtraDrawables);
    return m_ExtraDrawableNames[index];
}

inline const char* fragType::GetTuneName() const
{
	return m_TuneName;
}

inline const char* fragType::GetBaseName() const
{
	if (m_TuneName)
	{
		if (const char* baseName = strrchr(m_TuneName, '/'))
		{
			return baseName + 1;
		}
	}

	return m_TuneName;
}

inline int fragType::GetEntityClass() const
{
	return m_EntityClass;
}

inline void fragType::SetFlags(u16 bits, bool set)
{
	if (set)
	{
		m_Flags |= bits;
	}
	else
	{
		m_Flags &= ~bits;
	}
}

inline bool fragType::IsBreakingDisabled() const
{
	return (m_Flags & DISABLE_BREAKING) != 0;
}

inline bool fragType::IsActivationDisabled() const
{
	return (m_Flags & DISABLE_ACTIVATION) != 0;
}

inline void fragType::CopyEstimatedCacheSizes(const fragType *copyFrom)
{
	m_EstimatedCacheSize = copyFrom->m_EstimatedCacheSize;
	m_EstimatedArticulatedCacheSize = copyFrom->m_EstimatedArticulatedCacheSize;
}

inline bool fragType::GetNeedsCacheEntryToActivate() const
{
	return (m_Flags & NEEDS_CACHE_ENTRY_TO_ACTIVATE) != 0;
}

inline bool fragType::GetHasAnyArticulatedParts() const
{
	return (m_Flags & HAS_ANY_ARTICULATED_PARTS) != 0;
}

inline bool fragType::GetIsUserModified() const
{
	return (m_Flags & IS_USER_MODIFIED) != 0;
}

#if HACK_GTA4	// EUGENE_APPROVED
// Added these two flags for gta
// Want to keep them and move across to rage\dev
inline bool fragType::GetForceArticulatedDamping() const
{
	return (m_Flags & FORCE_ARTICULATED_DAMPING) != 0;
}

inline bool fragType::GetForceLoadCommonDrawable() const
{
	return (m_Flags & FORCE_LOAD_COMMON_DRAWABLE) != 0;
}

inline void fragType::SetForceLoadCommonDrawable(bool loadCommonDrawable)
{
	if (loadCommonDrawable)
	{
		m_Flags |= FORCE_LOAD_COMMON_DRAWABLE;
	}
	else
	{
		m_Flags &= ~FORCE_LOAD_COMMON_DRAWABLE;
	}
}

inline bool fragType::GetForceAllocateLinkAttachments() const
{
	return (m_Flags & FORCE_ALLOCATE_LINK_ATTACHMENTS) != 0;
}
#endif //HACK_GTA4

inline bool fragType::GetAllocateTypeAndIncludeFlags() const
{
	return (m_Flags & ALLOCATE_TYPE_AND_INCLUDE_FLAGS) != 0;
}

inline const Vector4& fragType::GetBoundingSphere() const
{
	return m_BoundingSphere;
}

//
// This will not do anything unless the game itself utilizes m_UnbrokenElasticity:
//
inline float fragType::GetUnbrokenElasticity() const
{
	return m_UnbrokenElasticity;
}

inline void fragType::SetUserData(void* userData)
{
	m_UserData = userData;
}

inline void* fragType::GetUserData() const
{
	return m_UserData;
}

inline void fragType::SetGlassAttachmentBone(int bone)
{
	Assert(bone >= 0 && bone < 256);
	m_GlassAttachmentBone = (u8)bone;
}

inline int fragType::GetGlassAttachmentBone() const
{
	return m_GlassAttachmentBone;
}

inline int fragType::GetARTAssetID() const
{
	return m_ARTAssetID;
}

inline void fragType::SetARTAssetID(int assetID)
{
	m_ARTAssetID = (s8)assetID;
}

inline grmMatrixSet* fragType::GetSharedMatrixSet() const
{
	return m_SharedMatrixSet;
}

#if ENABLE_FRAGMENT_EVENTS
inline const evtSet* fragType::GetCollisionEventset() const
{
	return m_CollisionEventset;
}

inline fragCollisionEventPlayer* fragType::GetCollisionEventPlayer() const
{
	return m_CollisionEventPlayer;
}
#endif // ENABLE_FRAGMENT_EVENTS

inline void fragType::SetUseFragManagerDefaultsInLoad(bool enable)
{
	sm_UseFragManagerDefaultsInLoad = enable;
}

inline int fragType::GetNumEnvCloths () const
{
	return m_EnvCloth.GetCount();
}

inline fragTypeEnvCloth* fragType::GetTypeEnvCloth(int clothIndex)
{
	return m_EnvCloth[clothIndex];
}

inline const fragTypeEnvCloth* fragType::GetTypeEnvCloth (int clothIndex) const
{
	return m_EnvCloth[clothIndex];
}

inline fragTypeCharCloth* fragType::GetTypeCharCloth (int clothIndex)
{
	return m_CharCloth[clothIndex];
}

inline const fragTypeCharCloth* fragType::GetTypeCharCloth (int clothIndex) const
{
	return m_CharCloth[clothIndex];
}

inline int fragType::GetNumCharCloths () const
{
	return m_CharCloth.GetCount();
}

inline datOwner<bgPaneModelInfoBase>* fragType::GetAllGlassPaneModelInfos() const
{
	return m_GlassPaneModelInfos;
}

inline int fragType::GetNumGlassPaneModelInfos() const
{
	return m_NumGlassPaneModelInfos;
}


#if __WIN32
#pragma warning(pop)
#endif

} // namespace rage

#endif // FRAG_TYPE_H
