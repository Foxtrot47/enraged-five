// 
// fragment/cacheheap.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "cacheheap.h"

#include "diag/output.h"
#include "fragment/cache.h"
#include "fragment/cachemanager.h"
#include "fragment/fragmentconfig.h"
#include "system/criticalsection.h"
#include "system/simpleallocator.h"
#include "system/tls.h"

// Defines
#if __SPU
#define FRAG_HEAP_CRITICAL_SECTION
#else
#define FRAG_HEAP_CRITICAL_SECTION //sysCriticalSectionToken s_MemFragToken;
#endif


namespace rage {

// The current cache entry capturing allocations
static fragCacheBlockList* s_HeapCacheBlocks = NULL;

#if FRAG_CACHE_HEAP_DEBUG
bool s_bFragCacheHeapDebug = false;
//static fragCacheEntry* s_HeapCacheEntry;
StreamingInterestsCallback fragCacheAllocator::m_streamingInterestsCallback = NULL;
#endif

fragCacheAllocator::fragCacheAllocator() : m_pAllocator(NULL)
{
#if FRAG_CACHE_HEAP_DEBUG
	m_peakUsed = 0;
	m_totalUsed = 0;
#endif
}

fragCacheAllocator::fragCacheAllocator(void* ptr, size_t bytes, size_t size) : m_pAllocator(NULL)
{
	Init(ptr, bytes, size);

#if FRAG_CACHE_HEAP_DEBUG
	m_peakUsed = 0;
	m_totalUsed = 0;
#endif
}

fragCacheAllocator::~fragCacheAllocator()
{
	delete m_pAllocator;
}

void fragCacheAllocator::Init(void* ptr, size_t bytes, size_t size)
{
	fragAssert(!m_pAllocator && !m_pool.IsInitialized());
	fragAssert(ptr && bytes && size);

	if (!m_pAllocator)
		delete m_pAllocator;

	m_pAllocator = rage_new sysMemSimpleAllocator(ptr, (int) bytes, sysMemSimpleAllocator::HEAP_FRAG, false);
	m_pAllocator->SetQuitOnFail(false);
	m_hashTable.Init(static_cast<u16>(size));
	m_pool.Init(size);
}

fragCacheBlock* fragCacheAllocator::AllocateBlock(size_t size, size_t align /*= 16*/, int heapIndex /*= 0*/)
{
	ASSERT_ONLY(if (sysMemVerifyMainThread) sysMemVerifyMainThread();)

	FRAG_HEAP_CRITICAL_SECTION;
	fragAssert(size && align && s_HeapCacheBlocks);	

	// Block	
#if FRAG_CACHE_HEAP_DEBUG && !__TOOL && !__RESOURCECOMPILER
	static bool sDumpedCacheEntries = false;
	if (!m_pool.GetNoOfFreeSpaces() && !sDumpedCacheEntries)
	{
		fragManager::GetFragCacheManager()->DumpCacheEntries();
		sDumpedCacheEntries = true;
	}
#endif

	fragCacheBlock* block = m_pool.New();
	if (!block)
		return NULL;

	// Memory
	USE_MEMBUCKET(MEMBUCKET_PHYSICS);

	// Small allocations should go to smallocator
#if !__TOOL && !__RESOURCECOMPILER
	block->ptr = NULL;
	if (size > sysSmallocator::MAX_SMALLOCATOR_SIZE)
#endif
	{
		block->ptr = m_pAllocator->Allocate(size, align, heapIndex);
	}		

	if (!block->ptr)
	{
//#if FRAG_CACHE_HEAP_DEBUG
//		// get name of frag type out of current cache entry so we can display a model name in warning
//		const char* typeName = NULL;
//		if(s_HeapCacheEntry && s_HeapCacheEntry->GetInst())
//			typeName = s_HeapCacheEntry->GetInst()->GetType()->GetTuneName();
//		fragWarningf("%s Allocating %" SIZETFMT "d byte fragment cache block from main game heap, increase size of cache pools to prevent this!", typeName, size);
//#endif
		block->ptr = GetBackupAllocator()->Allocate(size, align, heapIndex);
		if (!block->ptr)
		{
			m_pool.Delete(block);
			return NULL;
		}
	}

#if FRAG_CACHE_HEAP_DEBUG
	m_totalUsed += (int) GetSizeWithOverhead(block->ptr);
	if (m_totalUsed > m_peakUsed)
		m_peakUsed = m_totalUsed;

#if !__TOOL && !__RESOURCECOMPILER && !USE_SPARSE_MEMORY
	if (m_totalUsed > (int) (m_pAllocator->GetHeapSize() + FRAG_MAX_OVERAGE))
	{
		if(!sDumpedCacheEntries)
		{
			if (m_streamingInterestsCallback)
				m_streamingInterestsCallback();

			fragManager::GetFragCacheManager()->DumpCacheEntries();
			sDumpedCacheEntries = true;
		}
		else
		{
			fragWarningf("Exceeded max frag cache entries by %d KB!", FRAG_MAX_OVERAGE >> 10);
		}

		fragAssertf(false, "Exceeded max frag cache entries by %d KB! Dumping frag cache entries to TTY. Open a B* to Eric J Anderson, CC Klaas, and attach the TTY to the bug. Please enter a separate bug for each instance!", FRAG_MAX_OVERAGE >> 10);		
	}		
#endif
#endif

	m_hashTable.Insert(block->ptr, block);
	s_HeapCacheBlocks->AddToHead(block);

	return block;
}

void* fragCacheAllocator::Allocate(size_t size, size_t align, int heapIndex /*= 0*/)
{
	ASSERT_ONLY(if (sysMemVerifyMainThread) sysMemVerifyMainThread();)

	fragAssert(size && align);
	fragCacheBlock* block = AllocateBlock(size, align, heapIndex);
	return block ? block->ptr : NULL;
}

void* fragCacheAllocator::ExternalAllocate(size_t size, size_t align, int heapIndex /*= 0*/)
{
	ASSERT_ONLY(if (sysMemVerifyMainThread) sysMemVerifyMainThread();)

	fragAssert(size && align);

	// Small allocations should go to smallocator
	void* ptr = NULL;
#if !__TOOL && !__RESOURCECOMPILER	
	if (size > sysSmallocator::MAX_SMALLOCATOR_SIZE)
#endif
	{
		ptr = m_pAllocator->Allocate(size, align, heapIndex);
	}		

	if (!ptr)
		ptr = GetBackupAllocator()->Allocate(size, align, heapIndex);

#if FRAG_CACHE_HEAP_DEBUG
	if (ptr)
	{
		m_totalUsed += (int) GetSizeWithOverhead(ptr);
		if (m_totalUsed > m_peakUsed)
			m_peakUsed = m_totalUsed;
	}	
#endif

	return ptr;
}

void fragCacheAllocator::FreeBlock(fragCacheBlock* block)
{
	ASSERT_ONLY(if (sysMemVerifyMainThread) sysMemVerifyMainThread();)

	FRAG_HEAP_CRITICAL_SECTION;
	fragAssert(m_pool.IsInPool(block));

	// Memory	
	void* ptr = block->ptr;
	fragAssert(IsValidPointer(ptr) && m_hashTable.Access(const_cast<void*>(ptr)));
	m_hashTable.Delete(const_cast<void*>(ptr));

	// Debug
#if FRAG_CACHE_HEAP_DEBUG
	m_totalUsed -= (int) GetSizeWithOverhead(ptr);

	if (pgBase::IsMemoryTracked(const_cast<void*>(ptr), GetSize(ptr)))
		Quitf("Frag Cache nuking tracked memory");

	// Write a fragment specific pattern to memory
	IF_DEBUG_MEMORY_FILL_N(memset((void**)ptr, 0xEF, GetSize(ptr)), DMF_FRAGCACHE);
#endif

	// Data
	if (m_pAllocator->IsValidPointer(ptr))
	{
		m_pAllocator->Free(const_cast<void*>(ptr));
	}			
	else
	{
		GetBackupAllocator()->Free(ptr);
	}

	// Pool
	m_pool.Delete(block);
}

void fragCacheAllocator::Free(const void* ptr)
{
	ASSERT_ONLY(if (sysMemVerifyMainThread) sysMemVerifyMainThread();)

	FRAG_HEAP_CRITICAL_SECTION;	
	fragAssert(IsValidPointer(ptr) && m_hashTable.Access(const_cast<void*>(ptr)));	

	// Block
	fragCacheBlock* block = *m_hashTable.Access(const_cast<void*>(ptr));
	fragAssert(block && m_pool.IsInPool(block) && ptr == block->ptr);

	s_HeapCacheBlocks->Remove(block);

	// Memory
	m_hashTable.Delete(const_cast<void*>(ptr));

	// Debug
#if FRAG_CACHE_HEAP_DEBUG
	m_totalUsed -= (int) GetSizeWithOverhead(ptr);
	m_totalUsed = (m_totalUsed > 0) ? m_totalUsed : 0;

	// Write a fragment specific pattern to memory
	IF_DEBUG_MEMORY_FILL_N(memset((void**)ptr, 0xEF, GetSize(ptr)), DMF_FRAGCACHE);
#endif

	// Data
	if (m_pAllocator->IsValidPointer(ptr))
		m_pAllocator->Free(const_cast<void*>(ptr));
	else
		GetBackupAllocator()->Free(ptr);

	// Pool
	m_pool.Delete(block);
}

void fragCacheAllocator::ExternalFree(const void* ptr)
{
	ASSERT_ONLY(if (sysMemVerifyMainThread) sysMemVerifyMainThread();)

	fragAssert(IsValidPointer(ptr));

	// Debug
#if FRAG_CACHE_HEAP_DEBUG
	m_totalUsed -= (int) GetSizeWithOverhead(ptr);
	m_totalUsed = (m_totalUsed > 0) ? m_totalUsed : 0;

	if (pgBase::IsMemoryTracked(const_cast<void*>(ptr), GetSize(ptr)))
		Quitf("Frag Cache nuking tracked memory");

	// Write a fragment specific pattern to memory
	IF_DEBUG_MEMORY_FILL_N(memset((void**)ptr, 0xEF, GetSize(ptr)), DMF_FRAGCACHE);
#endif

	// Data
	if (m_pAllocator->IsValidPointer(ptr))
		m_pAllocator->Free(const_cast<void*>(ptr));
	else
		GetBackupAllocator()->Free(ptr);
}

bool fragCacheAllocator::IsValidPointer(const void* ptr) const
{
	return m_pAllocator->IsValidPointer(ptr) || GetBackupAllocator()->IsValidPointer(ptr);
}

#if __BANK
bool fragCacheAllocator::IsValidUsedPointer(const void *ptr) const
{
	return ((sysMemSimpleAllocator*) m_pAllocator)->IsValidUsedPointer(ptr) || ((sysMemSimpleAllocator*) GetBackupAllocator())->IsValidUsedPointer(ptr);
}
#endif

size_t fragCacheAllocator::GetSize(const void* ptr) const
{
	FRAG_HEAP_CRITICAL_SECTION;
	fragAssert(IsValidPointer(ptr));

	if (m_pAllocator->IsValidPointer(ptr))
		return m_pAllocator->GetSize(ptr);

	return GetBackupAllocator()->GetSize(ptr);
}

size_t fragCacheAllocator::GetSizeWithOverhead(const void* ptr) const
{	
	FRAG_HEAP_CRITICAL_SECTION;
	fragAssert(IsValidPointer(ptr));

	if (m_pAllocator->IsValidPointer(ptr))
		return m_pAllocator->GetSizeWithOverhead(ptr);

	return GetBackupAllocator()->GetSizeWithOverhead(ptr);
}

#if FRAG_CACHE_HEAP_DEBUG
size_t fragCacheAllocator::GetHighWaterMark(bool reset)
{
	size_t result = m_peakUsed;
	if (reset)
		m_peakUsed = (int) GetMemoryUsed();

	return result;
}
#endif

// Global
#ifndef __THREAD
#define __THREAD
#endif // __THREAD

static __THREAD sysMemAllocator* s_PrevAllocator;
static __THREAD sysMemAllocator* s_PrevContainer;
static __THREAD int s_FragMemRecursionCount = 0;

void fragMemStartCacheHeapFunc(fragCacheEntry* entry)
{
	ASSERT_ONLY(if (sysMemVerifyMainThread) sysMemVerifyMainThread();)

	FRAG_HEAP_CRITICAL_SECTION;
	fragAssert(entry);

	if (s_HeapCacheBlocks == NULL)
	{
		//FRAG_CACHE_HEAP_DEBUG_ONLY(s_HeapCacheEntry = entry;)
		s_HeapCacheBlocks = &entry->m_Blocks;
		s_PrevAllocator = &sysMemAllocator::GetCurrent();
		s_PrevContainer = &sysMemAllocator::GetContainer();

		fragCacheAllocator* pFragAllocator = fragManager::GetFragCacheAllocator();
		sysMemAllocator::SetCurrent(*pFragAllocator);
		sysMemAllocator::SetContainer(*pFragAllocator);
	}
	else
	{
		// Ensure we don't change which cache entry we're allocating for. We could support that with a stack but it seems unnecessary. 
		FastAssert(&entry->m_Blocks == s_HeapCacheBlocks); 

		s_FragMemRecursionCount++;
	}
}

size_t fragMemEndCacheHeap()
{
	ASSERT_ONLY(if (sysMemVerifyMainThread) sysMemVerifyMainThread();)

	// We're always returning 0 in non-bank builds...
	// I think the fragType depends on this for cache size estimation, maybe that's no longer needed. 
	size_t bytes = 0;

	FRAG_HEAP_CRITICAL_SECTION;

#if FRAG_CACHE_HEAP_DEBUG	
	fragCacheAllocator* pFragAllocator = fragManager::GetFragCacheAllocator();

	for (fragCacheBlockList::Iterator it(s_HeapCacheBlocks->GetFirst()); it.IsValid(); it.Next())
	{
		fragCacheBlock* block = it.Item();		
		bytes += pFragAllocator->GetSizeWithOverhead(block->ptr);
	}

	if(s_bFragCacheHeapDebug)
		fragDisplayf("Used %" SIZETFMT "d", bytes);
#endif

	if (s_FragMemRecursionCount == 0)
	{
		FastAssert(s_PrevAllocator && s_PrevContainer);
		sysMemAllocator::SetContainer(*s_PrevContainer);
		sysMemAllocator::SetCurrent(*s_PrevAllocator);
		s_PrevAllocator = NULL;
		s_PrevContainer = NULL;

		s_HeapCacheBlocks = NULL;
	}
	else
	{
		s_FragMemRecursionCount--;
	}

	return bytes;
}

} // namespace rage
