#include "tmplugin.h"
#include <stdio.h>

#ifdef __RADWIN__
#include <windows.h>
#pragma comment(lib,"opengl32")
#pragma comment(lib,"glu32")
#endif

#include <gl/gl.h>
#include <gl/glu.h>

#include <map>
#include <string>

static char const * kpIdentifier = "Rage Blob";
static TmPluginImport s_pi_imp;

const float kHEIGHT = 512.0f;

static float kX, kY, kW, kScale;
static char  s_name[ 1024 ];
static TmI32 s_sequence, s_frame;
static TmI32 startFrame = 0;
static TmI32 numFrames = 0;

typedef unsigned char u8;
typedef unsigned int u32;

using std::map;
using std::string;

////////////////////////////////////////////////////////////////////////////////////////////////////

class BlobBuffer
{
public:
    BlobBuffer(const void * buffer, int bufferSize)
    : m_Buffer(static_cast<const u8*>(buffer))
    , m_BufferSize(bufferSize)
    , m_BufferPosition(0)
    , m_SwapEndian(false)
    {
        m_SwapEndian = ReadU32() != 0x01234567;
    }
    inline u32 ReadU32()
    {
        return Read<u32>();
    }
    
    inline float ReadFloat()
    {
        return Read<float>();
    }
    
    template<typename T> T Read()
    {
        if((m_BufferSize - m_BufferPosition) < sizeof(T))
        {
            m_BufferPosition = m_BufferSize;
            // might want to replace with Default<T>()
            return 0;
        }
        else
        {
            union
            {
                T value;
                u8  bytes[sizeof(T)];
            } valueBytes;
            if(m_SwapEndian)
            {
                int i = sizeof(T);
                do
                {
                    --i;
                    valueBytes.bytes[i] = m_Buffer[m_BufferPosition++];
                } while(i != 0);
            }
            else
            {
                for(int i = 0; i < sizeof(T); ++i)
                {
                    valueBytes.bytes[i] = m_Buffer[m_BufferPosition++];
                }
            }
            return valueBytes.value;
        }
    }
    const char* ReadString()
    {
        const char * value = reinterpret_cast<const char*>(&m_Buffer[m_BufferPosition]);
        const char * valueCheck = value;
        while(*valueCheck != '\0' && m_BufferPosition != m_BufferSize)
        {
            ++m_BufferPosition;
            ++valueCheck;
        }
        if(m_BufferPosition == m_BufferSize)
        {
            return m_Buffer[m_BufferPosition - 1] == '\0' ? value : NULL;
        }
        else
        {
            ++m_BufferPosition;
            return value;
        }
    }
private:
    const u8 * m_Buffer;
    const int m_BufferSize;
    int m_BufferPosition;
    bool m_SwapEndian;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

static
int
sInit(void* hwnd,TmPluginImport* pi_imp)
{
    s_pi_imp = *pi_imp;
    return 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static
void
sShutdown( void )
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static
int
sBeginSetup( TmI32 const kStartFrame, TmI32 const kNumFrames )
{
    startFrame = kStartFrame;
    numFrames = kNumFrames;
    return 1; // If you return 0 the visualizer will stop calling you
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static
int 
sFrameSetup( TmI32 const kFrame, TmI32 const kFrameIndex, TmI32 const kNumBlobs,
            TmI32 const kSequence, char const * kpName, 
            float const _kX, float const _kY, float const _kW, 
            float const kCyclesToPixelsScale )

{
    return 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static
int 
sBlobSetup( TmI32 const kFrame, TmI32 const kFrameIndex,
           TmI32 const kBlobIndex,
           TmU64 const kBlobTime, 
           int const kNumChunks, 
           void **blobChunks,
           int const kChunkSizes[] )
{
    return 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static
void 
sFrameDraw( TmI32 const kFrame, TmI32 const kFrameIndex, TmI32 const kNumBlobs,
           TmI32 const kSequence, char const * kpName, float const _kX, float const _kY, float const _kW, float const kCyclesToPixelsScale )
{
    kX = _kX;
    kY = _kY;
    kW = _kW;
    s_sequence = kSequence;
    s_frame = kFrame;
    strcpy_s( s_name, sizeof( s_name ), kpName );
    kScale = kCyclesToPixelsScale;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static
void
sBeginDraw( TmI32 const kStartFrame, TmI32 const kNumFrames )
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static
float
sBlobDraw( TmI32 const kFrame, TmI32 const kFrameIndex,
          TmI32 const kBlobIndex,
          TmU64 const kBlobTime, 
          int const kNumChunks, 
          void **blob_chunks,
          int const kChunkSizes[] )
{
    const float	kSX = kScale * kBlobTime;
    
    glBegin(GL_QUADS);
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex2f(kX, kY);
    glVertex2f(kX + kW, kY);
    glColor3f(0.5f, 0.5f, 0.5f);
    glVertex2f(kX + kW, kY + kHEIGHT);
    glVertex2f(kX, kY + kHEIGHT);
    glEnd();
    
    glRasterPos2d( kX, kY + 128.0f );
    
    BlobBuffer blobBuffer(blob_chunks[0], kChunkSizes[0]);
    
    const char * blobType = blobBuffer.ReadString();
    if(strcmp(blobType, "rendersummary:1") == 0)
    {    
        u32 numTimings = blobBuffer.ReadU32();
        
        char buffer[1024];
        const float MIN_WIDTH_FOR_GPU_TIMINGS = 128.0f;
        if(kW > MIN_WIDTH_FOR_GPU_TIMINGS)
        {
            for(int timing = 0; timing < numTimings; ++timing)
            {
                const char * timingName = blobBuffer.ReadString();
                float timingValue = blobBuffer.ReadFloat();
                u32 entityCount = blobBuffer.ReadU32();
                sprintf_s(buffer, sizeof(buffer), "%s: %.2f ms (%u entities)", timingName, timingValue, entityCount);
                float x0 = kX + 16.0f;
                float y0 = kY + 16.0f + timing*16.0f;
                float x1 = x0 + 100000.0f * kScale * timingValue;
                float y1 = y0 + 10.0f;
                glColor3f(0.3f, 0.3f, 0.3f);
                glRectf(x0-1, y0-1, x1+2, y1+2);
                glColor3f(0.7f, 1.0f, 0.7f);
                glRectf(x0, y0, x1, y1);
                s_pi_imp.draw_text( buffer, x0, y0, 0.0f, 0.0f, 0.0f );
            }
        }
    }

    // Return how much vertical space we used
    return kHEIGHT;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static
void
sEndDraw( TmI32 const kStartFrame, TmI32 const kNumFrames )
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
extern "C"
{
__declspec(dllexport)
int
vpEntry( TmPluginInfo* pi )
{
    if ( pi->pi_size != sizeof( *pi ) )
        return 0;
    if ( pi->pi_version != TM_PLUGIN_VERSION )
        return 0;
    pi->pi_identifier = kpIdentifier;

    pi->pi_init = sInit;
    pi->pi_shutdown = sShutdown;
    pi->pi_begin_setup = sBeginSetup;
    pi->pi_frame_setup = sFrameSetup;
    pi->pi_blob_setup  = sBlobSetup;

    pi->pi_begin_draw = sBeginDraw;
    pi->pi_blob_draw = sBlobDraw;
    pi->pi_frame_draw = sFrameDraw;
    pi->pi_end_draw = sEndDraw;

    return 1;

}
}
