#include <stdio.h>
#include <stdarg.h>

#include <xtl.h>

#include <telemetry.h>
#include <tmxgpu.h>

#define TMXGPU_ENABLE_SYNC 0x1
#define MAX_QUERY_COUNT     32

struct TmxGpuContextXenon
{
	HTELEMETRY cx;
	TmxGpuFunctions api;

	struct Query
	{
		IDirect3DPerfCounters9* preQuery;
		IDirect3DPerfCounters9* postQuery;
		const char* name; // NULL name means the query is free to use
	};

	IDirect3DDevice9* device;
	TmU64 options;

	int queryCount;
	int queryCurrent;

	Query queries[MAX_QUERY_COUNT];

	TmI64 gpuToCpuTimeOffset;
	TmF64 gpuToCpuTimeRatio;

	void tick()
	{
		if (!cx) return;

		resolveAllQueries();

		plotPercentageCounter(D3DCOUNTER_FRAME_GPU_IDLE_PERCENT, "GPU/Idle");

		plotPercentageCounter(D3DCOUNTER_PRIMITIVES_CULLED_PERCENT, "GPU/Xbox 360/Primitives/Culled");
		plotPercentageCounter(D3DCOUNTER_PRIMITIVES_CLIPPED_PERCENT, "GPU/Xbox 360/Primitives/Clipped");
		plotPercentageCounter(D3DCOUNTER_PRIMITIVES_VISIBLE_PERCENT, "GPU/Xbox 360/Primitives/Visible");

		plotCounter(D3DCOUNTER_BANDWIDTH_SYSTEM, "GPU/Xbox 360/Bandwidth in GBps/System");
		plotCounter(D3DCOUNTER_BANDWIDTH_INDICES, "GPU/Xbox 360/Bandwidth in GBps/Index");
		plotCounter(D3DCOUNTER_BANDWIDTH_TEXTURE, "GPU/Xbox 360/Bandwidth in GBps/Texture");
		plotCounter(D3DCOUNTER_BANDWIDTH_VERTEX, "GPU/Xbox 360/Bandwidth in GBps/Vertex");
		plotCounter(D3DCOUNTER_BANDWIDTH_RESOLVE, "GPU/Xbox 360/Bandwidth in GBps/Resolve");
		plotCounter(D3DCOUNTER_BANDWIDTH_MEMEXPORT, "GPU/Xbox 360/Bandwidth in GBps/Memory export");
	}

	void reset()
	{
		if (!cx) return;

		resolveAllQueries();

		for (int i = 0; i < queryCount; i++) {
			queries[i].preQuery->Release();
			queries[i].postQuery->Release();
		}

		cx = NULL;
		device = NULL;
	}

	void enable(TmxGpuOption option, int value)
	{
		if (!cx) return;

		if (option == TMXGPU_SYNC) {
			resolveAllQueries();

			if (value)
				options |= TMXGPU_ENABLE_SYNC;
			else
				options &= ~TMXGPU_ENABLE_SYNC;
		}
	}

	void drawCallBegin(const char *nameFormat, va_list *args)
	{
		if (!cx) return;

		Query *query = makeQuery();
		d3dQueryIssue(query->preQuery);

		char buffer[1024];
		vsprintf_s(buffer, 1024, nameFormat, *args);
		query->name = tmDynamicString(cx, buffer);
	}

	void drawCallEnd()
	{
		if (!cx) return;

		d3dQueryIssue(queries[queryCurrent].postQuery);
		maybeResolveQuery(&queries[queryCurrent]);
	}

	TmU64 convertTime(TmU64 gpuTimestamp)
	{
		return gpuToCpuTimeOffset + TmU64(TmF64(gpuTimestamp) * gpuToCpuTimeRatio);
	}

	void d3dQueryIssue(IDirect3DPerfCounters9* query)
	{
		device->QueryPerfCounters(query, 0);
	}

	TmU64 d3dQueryWait(IDirect3DPerfCounters9* query)
	{
		D3DPERFCOUNTER_VALUES values = {0};
		query->BlockUntilNotBusy();
		query->GetValues(&values, 0, NULL);
		return values.CP[0].QuadPart;
	}

	TmU64 d3dQueryIssueAndWait(IDirect3DPerfCounters9* query)
	{
		d3dQueryIssue(query);
		return d3dQueryWait(query);
	}


	void resolveQuery(Query* query)
	{
		if (!query->name)
			return;

		TmU64 preTime = d3dQueryWait(query->preQuery);
		TmU64 postTime = d3dQueryWait(query->postQuery);

		tmBeginTimeSpanAt(cx, 1, 0, convertTime(preTime), "Draw call: %s", query->name);
		tmEndTimeSpanAt(cx, 1, 0, convertTime(postTime), "Draw call: %s", query->name);

		query->name = NULL;
	}

	void maybeResolveQuery(Query* query)
	{
		if (options & TMXGPU_ENABLE_SYNC)
			resolveQuery(query);
	}

	void resolveAllQueries()
	{
		for (int i = 0; i < queryCount; i++)
			resolveQuery(&queries[i]);
	}

	Query *makeQuery()
	{
		queryCurrent++;
		if (queryCurrent == queryCount)
			queryCurrent = 0;

		Query *query = &queries[queryCurrent];
		if (query->name)
			resolveQuery(query);

		return query;
	}

	void calibrateTiming()
	{
		Query *query = makeQuery();

		const TmU64 kStartCpuTimestamp = tmFastTime();
		const TmU64 kStartGpuTimestamp = d3dQueryIssueAndWait(query->preQuery);

		Sleep(100);

		const TmU64 kEndCpuTimestamp = tmFastTime();
		const TmU64 kEndGpuTimestamp = d3dQueryIssueAndWait(query->preQuery);

		// Compare CPU and GPU timestamps to find the offset and frequency ratio.
		gpuToCpuTimeRatio = TmF64(kEndCpuTimestamp - kStartCpuTimestamp) / TmF64(kEndGpuTimestamp - kStartGpuTimestamp);
		gpuToCpuTimeOffset = kStartCpuTimestamp - TmU64(kStartGpuTimestamp * gpuToCpuTimeRatio);
	}

	void plotCounter(D3DCOUNTER counter, const char *name)
	{
		tmPlot(cx, TMPT_NONE, 0, device->GetCounter(counter), name);
	}

	void plotPercentageCounter(D3DCOUNTER counter, const char *name)
	{
		tmPlot(cx, TMPT_PERCENTAGE_DIRECT, 0, device->GetCounter(counter) / 100.0f, name);
	}
};

extern "C" {

#define BEGIN_WRAPPER(rettype, func, ...) static rettype func (TmxGpuContext* cx_, ##__VA_ARGS__) { TmxGpuContextXenon* cx = (TmxGpuContextXenon*) cx_;
#define END_WRAPPER }

BEGIN_WRAPPER(void, tick)
	cx->tick();
END_WRAPPER

BEGIN_WRAPPER(void, reset)
	cx->reset();
END_WRAPPER

BEGIN_WRAPPER(void, enable, TmxGpuOption option, int value)
	cx->enable(option, value);
END_WRAPPER

BEGIN_WRAPPER(void, drawCallBegin, const char *nameFormat, ...)
	va_list args;
	va_start(args, nameFormat);
	cx->drawCallBegin(nameFormat, &args);
	va_end(args);
END_WRAPPER

BEGIN_WRAPPER(void, drawCallEnd)
	cx->drawCallEnd();
END_WRAPPER

#undef END_WRAPPER
#undef BEGIN_WRAPPER

TmxGpuErrorCode tmxGpuInitXenon(TmxGpuContext* dest, HTELEMETRY tmcx, void *device, TmU32 flags, int queryCount, void *queryResources)
{
	TmxGpuContextXenon* cx = (TmxGpuContextXenon*) dest;

	if (!tmcx) {
		cx->cx = NULL;
		return TMXERR_GPU_INVALID_CONTEXT;
	}

	if (!device)
		return TMXERR_GPU_INVALID_DEVICE;

	if (queryCount > 2*MAX_QUERY_COUNT || queryCount % 2 != 0)
		return TMXERR_GPU_INSUFFICIENT_RESOURCES;
	
	cx->api.tick = tick;
	cx->api.reset = reset;
	cx->api.enable = enable;
	cx->api.drawCallBegin = drawCallBegin;
	cx->api.drawCallEnd = drawCallEnd;

	cx->cx = tmcx;
	cx->device = (D3DDevice*) device;
	cx->options = 0;

	cx->queryCurrent = 0;
	cx->queryCount = queryCount / 2;
	if (queryResources) {
		for (int i = 0; i < cx->queryCount; i++) {
			cx->queries[i].preQuery = ((IDirect3DPerfCounters9**) queryResources)[2*i + 0];
			cx->queries[i].postQuery = ((IDirect3DPerfCounters9**) queryResources)[2*i + 1];
			cx->queries[i].name = NULL;
		}
	} else {
		for (int i = 0; i < cx->queryCount; i++) {
			if (cx->device->CreatePerfCounters(&cx->queries[i].preQuery, 1) != S_OK)
				return TMXERR_GPU_INSUFFICIENT_RESOURCES;
			if (cx->device->CreatePerfCounters(&cx->queries[i].postQuery, 1) != S_OK)
				return TMXERR_GPU_INSUFFICIENT_RESOURCES;
			cx->queries[i].name = NULL;
		}
	}

	if (flags & TMXGPU_INIT_QUERIES) {
		cx->device->EnablePerfCounters(TRUE);

		D3DPERFCOUNTER_EVENTS events;
		ZeroMemory(&events, sizeof(D3DPERFCOUNTER_EVENTS));
		events.CP[0] = GPUPE_CP_COUNT;
		cx->device->SetPerfCounterEvents(&events, 0);
	}

	cx->calibrateTiming();

	return TMX_OK;
}

}