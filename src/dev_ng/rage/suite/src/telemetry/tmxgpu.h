#ifndef TMXGPU_H
#define TMXGPU_H

#ifndef EXPGROUP
#define EXPGROUP(x)
#endif
#ifndef EXPAPI
#define EXPAPI
#endif
#ifndef EXPTYPE
#define EXPTYPE
#endif

#ifndef NTELEMETRY

#ifdef __cplusplus
extern "C" {
#endif

EXPGROUP(TMXGPU)

EXPTYPE typedef enum TmxGpuErrorCode {
	TMX_OK								= 0x0000, // Success.
	TMXERR_GPU_INVALID_CONTEXT          = 0x0001, // The specified Telemetry context was invalid.
	TMXERR_GPU_ALREADY_INITIALIZED		= 0x0002, // The system was already initialized. If you want to reinitialize with a new device, call $tmxGpuReset first. 
	TMXERR_GPU_INVALID_DEVICE			= 0x0003, // The device pointer does not correspond to a valid graphics device.
	TMXERR_GPU_INSUFFICIENT_RESOURCES	= 0x0004  // Not enough graphics resources (e.g. performance counters and event queries) available for profiling.
} TmxGpuErrorCode;
/*
GPU error code
*/

EXPTYPE typedef enum TmxGpuInitFlag {
	TMXGPU_INIT_QUERIES					= 0x0001  // Initialize query-related rendering state (otherwise you must do it yourself).
} TmxGpuInitFlag;
/*
GPU initialization flag
*/

EXPTYPE typedef enum TmxGpuOption {
	TMXGPU_SYNC							= 0x0001  // Synchronous draw call timing.
} TmxGpuOption;
/*
GPU option
*/

struct TmxGpuContext;

struct TmxGpuFunctions
{
	void (*enable)(TmxGpuContext *cx, TmxGpuOption option, int value);
	void (*tick)(TmxGpuContext *cx);
	void (*reset)(TmxGpuContext *cx);
	void (*drawCallBegin)(TmxGpuContext *cx, const char *nameFormat, ...);
	void (*drawCallEnd)(TmxGpuContext *cx);
};

#define TMX_GPU_CALL(fn, gcx, ...) do { TmxGpuContext* gcx_ = (gcx); if (gcx_ && gcx_->cx) gcx_->func.fn(gcx_, ##__VA_ARGS__); } while(0)

EXPTYPE struct TmxGpuContext
{
	HTELEMETRY cx;
	TmxGpuFunctions func;
	char data[4096];
};
/*
TMXGPU context. Internal use only. Don't modify.
*/

EXPAPI TmxGpuErrorCode tmxGpuInitD3D9(TmxGpuContext* dest, HTELEMETRY tmcx, void* device, TmU32 flags, int queryCount, void *queryResources);
/*
Initialize the Telemetry GPU profiler on Direct3D 9.

$:dest the TMXGPU context to initialize
$:tmcx a valid Telemetry context
$:device a pointer to the D3D9 device
$:flags bitwise OR of $TmxGpuInitFlag flags
$:queryCount number of timestamp/performance counter queries to use (must be an even number less than or equal to 64).
$:queryResources pointer to array of size queryCount with pointers to query resources to use. NULL means Telemetry should allocate its own resources (not possible on PS3).
$:returns some $TmxGpuErrorCode return code if it works

If you lose your rendering device, you must reinitialize the Telemetry GPU
profiler with the new device after first calling $tmxGpuReset.
*/

EXPAPI TmxGpuErrorCode tmxGpuInitD3D10(TmxGpuContext* dest, HTELEMETRY tmcx, void* device, TmU32 flags, int queryCount, void *queryResources);
/*
Initialize the Telemetry GPU profiler on Direct3D 10.

$:dest the TMXGPU context to initialize
$:tmcx a valid Telemetry context
$:device a pointer to the D3D10 device context
$:flags bitwise OR of $TmxGpuInitFlag flags
$:queryCount number of timestamp/performance counter queries to use (must be an even number).
$:queryResources pointer to array of size queryCount with pointers to query resources to use. NULL means Telemetry should allocate its own resources (not possible on PS3).
$:returns some $TmxGpuErrorCode return code if it works
*/

EXPAPI TmxGpuErrorCode tmxGpuInitD3D11(TmxGpuContext* dest, HTELEMETRY tmcx, void* device, TmU32 flags, int queryCount, void *queryResources);
/*
Initialize the Telemetry GPU profiler on Direct3D 11.

$:dest the TMXGPU context to initialize
$:tmcx a valid Telemetry context
$:device a pointer to the D3D11 device context
$:flags bitwise OR of $TmxGpuInitFlag flags
$:queryCount number of timestamp/performance counter queries to use (must be an even number).
$:queryResources pointer to array of size queryCount with pointers to query resources to use. NULL means Telemetry should allocate its own resources (not possible on PS3).
$:returns some $TmxGpuErrorCode return code if it works
*/

EXPAPI TmxGpuErrorCode tmxGpuInitXenon(TmxGpuContext* dest, HTELEMETRY tmcx, void* device, TmU32 flags, int queryCount, void *queryResources);
/*
Initialize the Telemetry GPU profiler on Xenon (Xbox 360).

$:dest the TMXGPU context to initialize
$:tmcx a valid Telemetry context
$:device a pointer to the D3D device
$:flags bitwise OR of $TmxGpuInitFlag flags
$:queryCount number of timestamp/performance counter queries to use (must be an even number).
$:queryResources pointer to array of size queryCount with pointers to query resources to use. NULL means Telemetry should allocate its own resources (not possible on PS3).
$:returns some $TmxGpuErrorCode return code if it works
*/

EXPAPI TmxGpuErrorCode tmxGpuInitPS3(TmxGpuContext* dest, HTELEMETRY tmcx, void* device, TmU32 flags, int queryCount, void *queryResources);
/*
Initialize the Telemetry GPU profiler on PS3.

$:dest the TMXGPU context to initialize
$:tmcx a valid Telemetry context
$:device a pointer to a graphics device
$:flags bitwise OR of $TmxGpuInitFlag flags
$:queryCount number of timestamp/performance counter queries to use (must be an even number).
$:queryResources pointer to array of size queryCount with pointers to query resources to use. NULL means Telemetry should allocate its own resources (not possible on PS3).
$:returns some $TmxGpuErrorCode return code if it works
*/

EXPAPI TmxGpuErrorCode tmxGpuInitWiiu(TmxGpuContext* dest, HTELEMETRY tmcx, void* device, TmU32 flags, int queryCount, void *queryResources);
/*
Initialize the Telemetry GPU profiler on Wii-U.

$:dest the TMXGPU context to initialize
$:tmcx a valid Telemetry context
$:device a pointer to a graphics device
$:flags bitwise OR of $TmxGpuInitFlag flags
$:queryCount number of timestamp/performance counter queries to use (must be an even number).
$:queryResources pointer to array of size queryCount with pointers to query resources to use. NULL means Telemetry should allocate its own resources (not possible on PS3).
$:returns some $TmxGpuErrorCode return code if it works
*/

EXPAPI TmxGpuErrorCode tmxGpuInitOpenGL(TmxGpuContext* dest, HTELEMETRY tmcx, void* device, TmU32 flags, int queryCount, void *queryResources);
/*
Initialize the Telemetry GPU profiler on OpenGL.

$:dest the TMXGPU context to initialize
$:tmcx a valid Telemetry context
$:device a pointer to a graphics device
$:flags bitwise OR of $TmxGpuInitFlag flags
$:queryCount number of timestamp/performance counter queries to use (must be an even number).
$:queryResources pointer to array of size queryCount with GLints of OpenGL query ids to use. NULL means Telemetry should allocate its own resources (not possible on PS3).
$:returns some $TmxGpuErrorCode return code if it works
*/

#define tmxGpuEnable(cx, option, value) TMX_GPU_CALL(enable, cx, option, value)

#if 0
EXPAPI void tmxGpuEnable(TmxGpuContext* cx, TmxGpuOption option, int value);
/*
Enable/disable a TMX GPU option.

$:cx the TMXGPU context
$:option the option to modify. One of the $TmxGpuOption constants.
$:value the new value for the option. Must be 0 (disable) or 1 (enable).
*/
#endif

#define tmxGpuTick(cx) TMX_GPU_CALL(tick, cx)

#if 0
EXPAPI void tmxGpuTick(TmxGpuContext* cx);
/*
Tick the Telemetry GPU profiler.

$:cx the TMXGPU context

Call this at the end of every frame, typically right after a Present() call.
*/
#endif

#define tmxGpuReset(cx) TMX_GPU_CALL(reset, cx)

#if 0
EXPAPI void tmxGpuReset(TmxGpuContext* cx);
/*
Reset the Telemetry GPU profiler.

$:cx the TMXGPU context

Call this when shutting down, or after losing a device prior to reinitialization.
*/
#endif

#define tmxGpuDrawCallBegin(cx, ...) TMX_GPU_CALL(drawCallBegin, cx, ##__VA_ARGS__)

#if 0
EXPAPI void tmxGpuDrawCallBegin(TmxGpuContext* cx, const char *nameFormat, ...);
/*
Mark the beginning of a draw call for Telemetry GPU profiling.

$:cx the TMXGPU context
$:name the name of the draw call for use in the visualizer

This must be matched by a corresponding $tmxGpuDrawCallEnd. It is currently illegal
to nest $tmxGpuDrawCallBegin and $tmxGpuDrawCallEnd pairs.
*/
#endif

#define tmxGpuDrawCallEnd(cx) TMX_GPU_CALL(drawCallEnd, cx)

#if 0
EXPAPI void tmxGpuDrawCallEnd(TmxGpuContext* cx);
/*
Mark the end of a draw call for Telemetry GPU profiling.

$:cx the TMXGPU context
*/
#endif

#ifdef __cplusplus
}
#endif

#undef EXPGROUP
#define EXPGROUP()
EXPGROUP()
#undef EXPGROUP

#else

#define tmxGpuInitD3D9(dest, tmcx, device, flags, queryCount, queryResource)	TMX_OK
#define tmxGpuInitD3D10(dest, tmcx, device, flags, queryCount, queryResource)	TMX_OK
#define tmxGpuInitD3D11(dest, tmcx, device, flags, queryCount, queryResource)	TMX_OK
#define tmxGpuInitPS3(dest, tmcx, device, flags, queryCount, queryResource)		TMX_OK
#define tmxGpuInitXenon(dest, tmcx, device, flags, queryCount, queryResource)	TMX_OK
#define tmxGpuReset(cx)					do {} while(0)
#define tmxGpuTick(cx)					do {} while(0)
#define tmxGpuEnable(cx, name)			do {} while(0)
#define tmxGpuDrawCallBegin(cx, name)	do {} while(0)
#define tmxGpuDrawCallEnd(cx)			do {} while(0)

#endif

#endif //TMXGPU_H