#include <stdio.h>
#include <stdarg.h>

#include <d3d9.h>

#include <telemetry.h>
#include <tmxgpu.h>

#define TMXGPU_ENABLE_SYNC 0x1
#define MAX_QUERY_COUNT     32

struct TmxGpuContextD3D9
{
	HTELEMETRY cx;
	TmxGpuFunctions api;

	struct Query
	{
		IDirect3DQuery9* preQuery;
		IDirect3DQuery9* postQuery;
		const char* name; // NULL name means the query is free to use
	};

	IDirect3DDevice9* device;

	TmU64 options;

	TmI64 gpuToCpuTimeOffset;
	TmF64 gpuToCpuTimeRatio;

	int queryCount;
	int queryCurrent;
	Query queries[MAX_QUERY_COUNT];

	void tick()
	{
		if (!cx) return;

		resolveAllQueries();
	}

	void reset()
	{
		if (!cx) return;

		resolveAllQueries();

		for (int i = 0; i < queryCount; i++) {
			queries[i].preQuery->Release();
			queries[i].postQuery->Release();
		}

		cx = NULL;
		device = NULL;
	}

	void enable(TmxGpuOption option, int value)
	{
		if (!cx) return;

		if (option == TMXGPU_SYNC) {
			resolveAllQueries();

			if (value)
				options |= TMXGPU_ENABLE_SYNC;
			else
				options &= ~TMXGPU_ENABLE_SYNC;
		}
	}

	void drawCallBegin(const char *nameFormat, va_list *args)
	{
		if (!cx) return;

		Query *query = makeQuery();
		d3dQueryIssue(query->preQuery);

		char buffer[1024];
		vsprintf_s(buffer, 1024, nameFormat, *args);
		query->name = tmDynamicString(cx, buffer);
	}

	void drawCallEnd()
	{
		if (!cx) return;

		d3dQueryIssue(queries[queryCurrent].postQuery);
		maybeResolveQuery(&queries[queryCurrent]);
	}

	TmU64 convertTime(TmU64 gpuTimestamp)
	{
		return gpuToCpuTimeOffset + TmU64(TmF64(gpuTimestamp) * gpuToCpuTimeRatio);
	}

	void d3dQueryIssue(IDirect3DQuery9* query)
	{
		query->Issue(D3DISSUE_END);
	}

	TmU64 d3dQueryWait(IDirect3DQuery9* query)
	{
		TmU64 timestamp;
		HRESULT hr;
		do {
			hr = query->GetData(&timestamp, sizeof(timestamp), D3DGETDATA_FLUSH);
			if (hr == D3DERR_DEVICELOST)
				return 0;
		} while (hr != S_OK);
		return timestamp;
	}

	TmU64 d3dQueryIssueAndWait(IDirect3DQuery9* query)
	{
		d3dQueryIssue(query);
		return d3dQueryWait(query);
	}

	void resolveQuery(Query* query)
	{
		if (!query->name)
			return;

		TmU64 preTime = d3dQueryWait(query->preQuery);
		TmU64 postTime = d3dQueryWait(query->postQuery);

		// Check for lost device.
		if (preTime == 0 && postTime == 0)
			return;

		tmBeginTimeSpanAt(cx, 1, 0, convertTime(preTime), "Draw call: %s", query->name);
		tmEndTimeSpanAt(cx, 1, 0, convertTime(postTime), "Draw call: %s", query->name);

		query->name = NULL;
	}

	void maybeResolveQuery(Query* query)
	{
		if (options & TMXGPU_ENABLE_SYNC)
			resolveQuery(query);
	}

	void resolveAllQueries()
	{
		for (int i = 0; i < queryCount; i++)
			resolveQuery(&queries[i]);
	}

	Query *makeQuery()
	{
		queryCurrent++;
		if (queryCurrent == queryCount)
			queryCurrent = 0;

		Query *query = &queries[queryCurrent];
		if (query->name)
			resolveQuery(query);

		return query;
	}

	void calibrateTiming()
	{
		Query *query = makeQuery();

		const TmU64 kStartCpuTimestamp = tmFastTime();
		const TmU64 kStartGpuTimestamp = d3dQueryIssueAndWait(query->preQuery);

		Sleep(100);

		const TmU64 kEndCpuTimestamp = tmFastTime();
		const TmU64 kEndGpuTimestamp = d3dQueryIssueAndWait(query->preQuery);

		// Compare CPU and GPU timestamps to find the offset and frequency ratio.
		gpuToCpuTimeRatio = TmF64(kEndCpuTimestamp - kStartCpuTimestamp) / TmF64(kEndGpuTimestamp - kStartGpuTimestamp);
		gpuToCpuTimeOffset = kStartCpuTimestamp - TmU64(kStartGpuTimestamp * gpuToCpuTimeRatio);
	}
};

extern "C" {

#define BEGIN_WRAPPER(rettype, func, ...) static rettype func (TmxGpuContext* cx_, ##__VA_ARGS__) { TmxGpuContextD3D9* cx = (TmxGpuContextD3D9*) cx_;
#define END_WRAPPER }

BEGIN_WRAPPER(void, tick)
	cx->tick();
END_WRAPPER

BEGIN_WRAPPER(void, reset)
	cx->reset();
END_WRAPPER

BEGIN_WRAPPER(void, enable, TmxGpuOption option, int value)
	cx->enable(option, value);
END_WRAPPER

BEGIN_WRAPPER(void, drawCallBegin, const char *nameFormat, ...)
	va_list args;
	va_start(args, nameFormat);
	cx->drawCallBegin(nameFormat, &args);
	va_end(args);
END_WRAPPER

BEGIN_WRAPPER(void, drawCallEnd)
	cx->drawCallEnd();
END_WRAPPER

#undef END_WRAPPER
#undef BEGIN_WRAPPER
	
TmxGpuErrorCode tmxGpuInitD3D9(TmxGpuContext *dest, HTELEMETRY tmcx, void *device, TmU32, int queryCount, void* queryResources)
{
	TmxGpuContextD3D9* cx = (TmxGpuContextD3D9*) dest;

	if (!tmcx) {
		cx->cx = NULL;
		return TMXERR_GPU_INVALID_CONTEXT;
	}

	if (!device)
		return TMXERR_GPU_INVALID_DEVICE;

	if (queryCount > 2*MAX_QUERY_COUNT || queryCount % 2 != 0)
		return TMXERR_GPU_INSUFFICIENT_RESOURCES;

	cx->api.tick = tick;
	cx->api.reset = reset;
	cx->api.enable = enable;
	cx->api.drawCallBegin = drawCallBegin;
	cx->api.drawCallEnd = drawCallEnd;

	cx->cx = tmcx;
	cx->device = (IDirect3DDevice9*) device;
	cx->options = 0;

	cx->queryCurrent = 0;
	cx->queryCount = queryCount / 2;
	if (queryResources) {
		for (int i = 0; i < cx->queryCount; i++) {
			cx->queries[i].preQuery = ((IDirect3DQuery9**) queryResources)[2*i + 0];
			cx->queries[i].postQuery = ((IDirect3DQuery9**) queryResources)[2*i + 1];
			cx->queries[i].name = NULL;
		}
	} else {
		for (int i = 0; i < cx->queryCount; i++) {
			if (cx->device->CreateQuery(D3DQUERYTYPE_TIMESTAMP, &cx->queries[i].preQuery) != D3D_OK)
				return TMXERR_GPU_INSUFFICIENT_RESOURCES;
			if (cx->device->CreateQuery(D3DQUERYTYPE_TIMESTAMP, &cx->queries[i].postQuery) != D3D_OK)
				return TMXERR_GPU_INSUFFICIENT_RESOURCES;
			cx->queries[i].name = NULL;
		}
	}

	cx->calibrateTiming();

	return TMX_OK;
}
}