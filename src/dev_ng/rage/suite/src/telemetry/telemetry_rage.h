#ifndef TELEMETRY_TELEMETRY_RAGE_H 
#define TELEMETRY_TELEMETRY_RAGE_H 

#include "profile/telemetry.h"

#if USE_TELEMETRY

namespace rage
{
	void TelemetryInit();
	void TelemetryShutdown();
	bool TelemetryStart(const char * appName, const unsigned int arenaSize);
	void TelemetryStop();
	void TelemetryPause();
	void TelemetryResume();
	void TelemetryUpdate();
}
#else

#define NTELEMETRY 1

#define TelemetryInit()
#define TelemetryShutdown()
#define TelemetryStart(...) true
#define TelemetryStop()
#define TelemetryPause()
#define TelemetryResume()
#define TelemetryUpdate()
#endif // USE_TELEMETRY

#endif // TELEMETRY_TELEMETRY_RAGE_H