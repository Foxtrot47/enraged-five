#include "telemetry/telemetry_rage.h"

#if USE_TELEMETRY

#define USE_DEBUG_TELEMETRY (0 && RSG_PC)

#if USE_DEBUG_TELEMETRY
#if RSG_PS3
#pragma comment(lib, "libTelemetryPS3c")
#elif RSG_XENON
#pragma comment(lib, "telemetry360c.lib")
#elif RSG_CPU_X64
#pragma comment(lib,"telemetry64.link.lib")
#elif RSG_CPU_X86
#pragma comment(lib,"telemetry32.link.lib")
#elif RSG_DURANGO
#pragma comment(lib,"telemetrydurangod.lib")
#elif RSG_ORBIS
#pragma comment(lib,"libTelemetryOrbisc.a")
#endif // RSG_ORBIS

#else // !USE_DEBUG_TELEMETRY

#if RSG_PS3
#pragma comment(lib, "libTelemetryPS3")
#elif RSG_XENON
#pragma comment(lib, "telemetry360.lib")
#elif (RSG_DURANGO) && (defined _DEBUG)
#pragma comment(lib,"telemetrydurangod.lib")
#elif RSG_DURANGO
#pragma comment(lib,"telemetrydurango.lib")
#elif RSG_ORBIS
#pragma comment(lib,"libTelemetryOrbis.a")
#elif RSG_CPU_X64
#pragma comment(lib,"telemetry64.link.lib")
#elif RSG_CPU_X86
#pragma comment(lib,"telemetry32.link.lib")
#endif // platform
#endif // USE_DEBUG_TELEMETRY

#if RSG_DURANGO || RSG_PC
// It appears that turning this on (so, Durango only, for now) can cause thread starvation in rare cases, 
// which in turn can cause giant stalls (on the order of ~0.5 - 4 seconds) in the game. See B* 1926754 and 
// CL 5965309 for more details.
#define TELEMETRY_SAFECONTEXT ( 1 )
#else 
#define TELEMETRY_SAFECONTEXT ( 0 )
#endif // RSG_DURANGO

// Currently, on Durango, as the render thread is THAT FAR behind, it exposes a threading issue
// when terminating a Telemetry grab. So I've protected it (only) on Durango for now with a . 
#if TELEMETRY_SAFECONTEXT
#define TELEMETRY_SAFECONTEXT_ONLY (...)	__VA_ARGS__
#define TELEMETRY_SAFECONTEXT_AUTOLOCK		sysCriticalSection lock( g_telemetryContextToken );
#else
#define TELEMETRY_SAFECONTEXT_ONLY (...)
#define TELEMETRY_SAFECONTEXT_AUTOLOCK
#endif // TELEMETRY_SAFECONTEXT


#include "file/tcpip.h"
#include "profile/telemetry.h"
#include "profile/timebars.h"
#include "system/memory.h"
#include "system/new.h"
#include "system/timemgr.h"
#include "system/threadregistry.h"

// This is Telemetry's 'secret' codeword for next-gen kits. It'll probably disappear in a future SDK.
#if RSG_ORBIS
#define _SEKRIT2 1
#elif RSG_DURANGO
#define _SEKRIT 1
#endif 

#include "telemetry/telemetry.h"
#include "system/param.h"

#define tmFreeEx(cx,ptr, file, line) TMCHECKCONTEXT_VA(cx,tmCoreFree,(cx,ptr,file,line, &tm_fmt))

#if RSG_PC || RSG_DURANGO
	#define TM_ALLOCATE(size) sysMemVirtualAllocate(size);
	#define TM_DEALLOCATE(ptr) sysMemVirtualFree(ptr);
#else
	#define TM_ALLOCATE(size) rage_new TmU8[size];
	#define TM_DEALLOCATE(ptr) delete[] ptr;
#endif

namespace rage
{
	PARAM(tmHost, "Telemetry Host Server IP Format ##.##.##.##");

#if TELEMETRY_SAFECONTEXT
static sysCriticalSectionToken g_telemetryContextToken;
#endif // TELEMETRY_SAFECONTEXT

static bool g_Initialised = false;
static TmU8 * g_TelemetryArena = NULL;
static HTELEMETRY g_TelemetryContext = NULL;

static TmMessageFlag g_MessageConversion[] = {
	TMMF_SEVERITY_LOG,
	TMMF_SEVERITY_WARNING,
	TMMF_SEVERITY_ERROR,
};

static TmZoneFlag ZoneFlagFromZoneType(ProfileZoneType zoneType)
{
	switch(zoneType)
	{
	case PZONE_NORMAL:
		return TMZF_NONE;
	case PZONE_IDLE:
		return TMZF_IDLE;
	case PZONE_STALL:
		return TMZF_STALL;
	default:
		return TMZF_NONE;
	}
}

static void SetThreadNameCb( void* pThreadId, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	sysIpcThreadId threadId = *(sysIpcThreadId*)pThreadId;

	va_list vl;
	va_start(vl, nameFormat);
	tmThreadName(g_TelemetryContext, (const TmI32)(ORBIS_ONLY((const TmI64)) WIN32PC_ONLY((const TmI64))threadId), nameFormat, TM_VA_LIST, vl);
	va_end(vl);
}

static void AllocCb(const void * ptr, u32 size, const char *file, unsigned int line, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	tmAllocEx(g_TelemetryContext, file, line, ptr, size, TM_VA_LIST, nameFormat, vl);
	va_end(vl);
}

static void FreeCb(const void * ptr, const char * file, unsigned int line)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	tmFreeEx(g_TelemetryContext, ptr, file, line);
}

static void StartZoneCb(ProfileZoneType zoneType, const char * file, unsigned int line, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	tmEnterEx(g_TelemetryContext, NULL, 0, 0, file, line, ZoneFlagFromZoneType(zoneType), TM_VA_LIST, nameFormat, vl );
	va_end(vl);
}

static void EndZoneCb(const char * file, unsigned int line)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	tmLeaveEx(g_TelemetryContext, 0, 0, file, line);
}

static void TryLockCb(const void* ptr, const char * file, unsigned int line, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	tmTryLockEx( g_TelemetryContext, 0, 0, file, line, ptr, nameFormat, vl ); 
	va_end(vl);
}

static void EndTryLockCb(LockResultType eResult, const void* ptr, const char * file, unsigned int line)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	CompileTimeAssert((u32)LRT_SUCCESS == (u32)TMLR_SUCCESS);
	CompileTimeAssert((u32)LRT_FAILED == (u32)TMLR_FAILED);
	CompileTimeAssert((u32)LRT_TIMEOUT == (u32)TMLR_TIMEOUT);
	tmEndTryLockEx( g_TelemetryContext, 0, file, line, ptr, (TmLockResult)eResult); 
}

static void LockCb(const void* ptr, const char * file, unsigned int line, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	tmSetLockStateEx( g_TelemetryContext, file, line, ptr, TMLS_LOCKED, nameFormat, vl );
	va_end(vl);
}

static void UnlockCb(const void* ptr, const char * file, unsigned int line, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	tmSetLockStateEx( g_TelemetryContext, file, line, ptr, TMLS_RELEASED, nameFormat, vl );
	va_end(vl);
}

static void PlotMemoryCb(u32 value, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	tmPlotU32(g_TelemetryContext, TMPT_MEMORY, TMPF_NONE, value, TM_VA_LIST, nameFormat, &vl);
	va_end(vl);
}

static void PlotMillisecondsCb(float value, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	tmPlot(g_TelemetryContext, TMPT_TIME_MS, TMPF_FRAMETIME, value, TM_VA_LIST, nameFormat, &vl);
	va_end(vl);
}

static void PlotFloatCb(float value, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	tmPlot(g_TelemetryContext, TMPT_NONE, TMPF_NONE, value, TM_VA_LIST, nameFormat, &vl);
	va_end(vl);
}

static void AddBlobCb(const CBlobBuffer & blob, const char * pluginName, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	tmBlob(g_TelemetryContext, blob.GetData(), blob.GetDataSizeInBytes(), pluginName, TM_VA_LIST, nameFormat, &vl);
	va_end(vl);
}

static void MessageCb(u32 severity, const char * nameFormat, ...)
{
	TELEMETRY_SAFECONTEXT_AUTOLOCK;

	va_list vl;
	va_start(vl, nameFormat);
	char buffer[1024];
	vsprintf(buffer, nameFormat, vl);
	va_end(vl);

	tmMessage(g_TelemetryContext, g_MessageConversion[severity], "%s", tmDynamicString(g_TelemetryContext, buffer));
}

void TelemetryInit()
{
	// prevent warning
#if (defined __RADNT__ || defined __RADLINUX__) && !RSG_DURANGO
	tmLoadTelemetry(1);  // Set up checked DLLs on Windows (ignored on other platforms)
#endif
	g_Initialised = (tmStartup() == TM_OK) ? true : false;

	//@@: location RAGE_TELEMETRYINIT
	TelemetryCallbacks callbacks = { NULL };
	callbacks.SetThreadName = SetThreadNameCb;
	callbacks.Alloc = AllocCb;
	callbacks.Free = FreeCb;
	callbacks.StartZone = StartZoneCb;
	callbacks.EndZone = EndZoneCb;
	callbacks.TryLock = TryLockCb;
	callbacks.EndTryLock = EndTryLockCb;
	callbacks.Lock = LockCb;
	callbacks.Unlock = UnlockCb;
	callbacks.PlotMemory = PlotMemoryCb;
	callbacks.PlotMilliseconds = PlotMillisecondsCb;
	callbacks.PlotFloat = PlotFloatCb;
	callbacks.AddBlob = AddBlobCb;
	callbacks.Message = MessageCb;
	SetTelemetryCallbacks(callbacks);
}

void TelemetryShutdown()
{
	Assert(g_Initialised);

	if(g_Initialised)
	{
		TelemetryStop();

		g_Initialised = false;

		if(g_TelemetryArena)
		{
			TM_DEALLOCATE(g_TelemetryArena);
			g_TelemetryArena = NULL;
		}
		tmShutdown();
	}
}

bool TelemetryStart(const char * appName, const unsigned int arenaSize)
{
	Assert(g_Initialised);

	if(g_Initialised)
	{
		TELEMETRY_SAFECONTEXT_AUTOLOCK;

		if(g_TelemetryContext || g_TelemetryArena)
		{
			return false;
		}

		g_TelemetryArena = ( TmU8* )TM_ALLOCATE(arenaSize);

		HTELEMETRY TelemetryContext = NULL;

		if(tmInitializeContext(&TelemetryContext, (void*)g_TelemetryArena, arenaSize) != TM_OK)
		{
			TM_DEALLOCATE(g_TelemetryArena);
			g_TelemetryArena = NULL;
			return false;
		}

		const char * buildInfo = __DATE__ __TIME__;
 		const char * server = fiDeviceTcpIp::GetLocalHost();
		// Use this to force Telemetry collection to remote IP address.
		PARAM_tmHost.Get(server);

		const int timeout = 1000;

		TmErrorCode eRetVal = tmOpen(TelemetryContext, appName, buildInfo, server, TMCT_TCP, TELEMETRY_DEFAULT_PORT, TMOF_DEFAULT, timeout);
		if (eRetVal != TM_OK)
		{
			tmShutdownContext(TelemetryContext);
			
			TM_DEALLOCATE(g_TelemetryArena);
			g_TelemetryArena = NULL;
			Errorf("Telemetry Failed to Open - Error Code - %d", eRetVal);
			return false;
		}
		g_TelemetryContext = TelemetryContext;
		tmEnable(g_TelemetryContext, TMO_OUTPUT_DEBUG_INFO, USE_DEBUG_TELEMETRY);
		tmEnable(g_TelemetryContext, TMO_RECORD_CALLSTACKS, USE_DEBUG_TELEMETRY);
		tmEnable(g_TelemetryContext, TMO_SUPPORT_CONTEXT_SWITCHES, USE_DEBUG_TELEMETRY);

#if THREAD_REGISTRY
		sysThreadRegistry::AdjustPriosBasedOnParams();
#endif
		return true;
	}
	else
	{
		return false;
	}
}
void TelemetryStop()
{
	Assert(g_Initialised);

	if(g_Initialised)
	{
		TELEMETRY_SAFECONTEXT_AUTOLOCK;

		if(g_TelemetryContext)
		{
			tmClose(g_TelemetryContext);
		
			HTELEMETRY context = g_TelemetryContext;
			g_TelemetryContext = NULL;
			tmShutdownContext(context);
		}
	}

	// delete the arena on update/shutdown to guard against deletion while in use on another thread
}

void TelemetryPause()
{
	Assert(g_Initialised);
	if(g_Initialised)
	{
		tmPause(g_TelemetryContext, 1);
	}
}
void TelemetryResume()
{
	Assert(g_Initialised);
	if(g_Initialised)
	{
		tmPause(g_TelemetryContext, 0);
	}
}

void TelemetryUpdate()
{
	Assert(g_Initialised);
	if(g_Initialised)
	{
		if(g_TelemetryContext)
		{
			// Assume that Update and Start/Stop on the same thread, so no need to critical section really.
			TELEMETRY_SAFECONTEXT_AUTOLOCK;
			tmTick(g_TelemetryContext);
		}
		else if(g_TelemetryArena)
		{
			TM_DEALLOCATE(g_TelemetryArena);
			g_TelemetryArena = NULL;
		}
	}
}

}

#endif // USE_TELEMETRY