//
// move/move_statedef.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_STATEDEF_H
#define MOVE_STATEDEF_H

#include "move_node.h"

#define MOVE_STATEDEF_H_SAFE

namespace rage {

class mvMotionWeb;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition base for state nodes
class mvNodeStateBaseDef : public mvNodeDef
{
public:
	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory resource
	void Init(mvMemoryResource* resource);

	// PURPOSE: Initialize transition definition data
	// NOTES: Helper function to endian read in transitions
	static void InitTransitions(mvMemoryResource* resource, mvTransition* transitions, u32 transitionCount);

	// PURPOSE: Offset to the definition data for the initial node in the state
	Offset m_Initial;

	// PURPOSE: When using block update on transition, allow one final update before disabling transitions
	// NOTES: U32 for alignment (wasteful, but wasn't sure how to break down the below further without potentially running into endian problems from the tool)
	u32 m_DeferBlockUpdate;

	// PURPOSE: Is the on enter event enabled
	// SEE ALSO: m_OnEnterId
	u8 m_OnEnterEnabled;

	// PURPOSE: Is the on exit event enabled
	// SEE ALSO: m_OnExitId
	u8 m_OnExitEnabled;

	// PURPOSE: Number of children in this state
	// NOTES: This is so move knows how much space to allocate for child nodes in
	//	its create context.
	u8 m_ChildCount;

	// PURPOSE: How many transitions exists from this node
	// SEE ALSO: m_Transitions
	u8 m_TransitionCount;

	// PURPOSE: Event id to post when state is entered
	// NOTES: Only used when m_OnEnterEnabled != 0
	u32 m_OnEnterId;

	// PURPOSE: Event id to post when state is exited.
	// NOTES: Only used when m_OnExitEnabled != 0
	u32 m_OnExitId;

	// PURPOSE: Offset to the definition for the first transition from this node
	// NOTES: Transitions are not constant size. Variable length condition data
	//	hangs off them.
	TOffset<mvTransition> m_Transitions;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition for state machines
class mvNodeStateMachineDef : public mvNodeStateBaseDef
{
public:
	// PURPOSE: Create an instance of this blend node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	crmtNode* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'event'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeStateMachineDef);

	// PURPOSE: In memory layout of node id/offset lookup for forced state
	struct StateNode {
		u32 Id;
		Offset Node;
	};

	// PURPOSE: Get the child state data if we need to force a state
	// NOTES: Offset table hangs off the end of the struct
	const StateNode* GetChildData() const;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Register this class with move's type system
MOVE_DECLARE_REGISTERED_CLASS(mvNodeStateMachineClass);

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition for inline state machine
class mvNodeInlinedStateMachineDef : public mvNodeStateBaseDef
{
public:
	// PURPOSE: Create an instance of this inline state machine definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	crmtNode* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'event'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeInlineDef);

	// PURPOSE: Offset to input node definition data
	Offset m_Input;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Register this class with move's type system
MOVE_DECLARE_REGISTERED_CLASS(mvNodeInlinedStateMachine);

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition for state nodes
class mvNodeStateDef : public mvNodeStateBaseDef
{
public:
	// PURPOSE: Create an instance of this state node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	crmtNode* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this defintion be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'event'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeStateDef);

	// PURPOSE: Register parameter lookup, parameter update, operator and event observers
	// NOTES: Calls through to following register functions
	void RegisterHandlers(mvMotionWeb* web, crmtNode** children) const;

	// PURPOSE: Register parameter lookup handlers
	// NOTES: Register observers that take a parameter from the parameter buffer and
	//	set a value on a node. If a single node has multiple values updated from the
	//	parameter buffer then it will have multiple observers managing those connections.
	void RegisterParameterLookupHandlers(mvMotionWeb* web, crmtNode** children) const;

	// PURPOSE: Register parameter update handlers
	// NOTES: REgiseter observers that take a value from a node and push it into the
	//	parameter buffer. If a single node has multiple values updating the parameter
	//	parameter buffer then it will have multiple observers managing those connections.
	void RegisterParameterUpdateHandlers(mvMotionWeb* web, crmtNode** children) const;

	// PURPOSE: Register event handlers
	// NOTES: Register observers that handle an event firing on a node and push an
	//	event into the parameter buffer. If a single node has multiple events
	//	then it will have multiple observers managing those connections.
	void RegisterEventHandlers(mvMotionWeb* web, crmtNode** children) const;

	// PURPOSE: Register operator handler
	// NOTES: Operators are baked down into a series of stack based operations.
	//	Usually the operator will hang off the node that it is setting a parameter
	//	on so that it is pushed into the node at the right time, but this isn't
	//	 a requirement.
	void RegisterOperatorHandlers(mvMotionWeb* web, crmtNode** children) const;

	// PURPOSE: In memory representation of a parameter hookup
	struct Parameter {
		enum
		{
			kOutputIndex = 0xffff,
		};
		u32 ParameterCrc;
		u16 NodeIndex;
		u16 TypeIndex;
		u32 ParameterIndex;
	};

	// PURPOSE: In memory representation of an event hookup
	struct Event {
		enum
		{
			kAttributeCount = 4
		};

		u16 NodeIndex;
		u16 TypeIndex;
		u32 EventCrc;
	};

	// PURPOSE: In memory representation of an operation
	// NOTES: An operation is a series of operators
	struct Operation {
		// PURPOSE: The index of the node from the children array
		u16 NodeIndex;

		// PURPOSE: The type of parameter set to resolve to on the destination node for the result of the operation
		// NOTES: Which function pointer should the operation use to set the result on the node
		u16 TypeIndex;

		// PURPOSE: Size of the entire operation in memory
		u16 Size;

		// PURPOSE: The index of the parameter to set. Used for array parameters. Should be 0 for non-array parameters
		u16 Index;
	};

	// PURPOSE: Operator data
	struct Operator {

		// PURPOSE: Available operator types
		enum {
			kEnd = 0,
			kPushValue,
			kFindAndPushValue,
			kAdd,
			kMultiply,
			kFCurve,
		};

		// PURPOSE: Type of the operator
		u32 OperatorType;

		// PURPSOE: Operator data
		// NOTES: These are context specific depending on the operator type
		union {
			u32 ParameterCrc;
			Offset Data;
			float Value;
		} Packet;
	};

	// PURPOSE: Offset to definition data describing input parameter hookups
	// TODO: these can probably go where m_signal_count & m_signals are, but for now they're tacked on the end.
	//	also think i can get away with less offsets
	// SEE ALSO: RegisterParameterLookupHandlers
	Offset m_ParamInputs;

	// PURPOSE: Number of parameter inputs that need to be registered
	// SEE ALSO: RegisterParmaeterLookupHandlers
	u32 m_ParamInputCount;

	// PURPOSE: Offset to definition data describing event hookups
	// SEE ALSO: RegiseterEventHandlers
	Offset m_Events;

	// PURPOSE: Number of event hookups that need to be registered
	// SEE ALSO: RegisterEventHandlers
	u32 m_EventCount;

	// PURPOSE: Offset to definition data describing output parameter hookups
	// SEE ALSO: RegisterParameterUpdateHandlers
	Offset m_ParamOutputs;

	// PURPOSE: Number of parameter outputs that need to be registered
	// SEE ALSO: RegisterParameterUpdateHandlers
	u32 m_ParamOutputCount;

	// PURPOSE: Offset to definition data describing operation hookups
	// SEE ALSO: RegisterOperatorHandlers
	Offset m_Operations;

	// PURPOSE: Number of operator hookups that need to be registered
	// SEE ALSO: RegisterOperatorHandlers
	u32 m_OperationCount;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create blend node
class mvNodeTailDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this tail node (really creates a proxy node)
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	crmtNode* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'event'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with move's type system
	MOVE_DECLARE_NODE_DEF(mvNodeTailDef);
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Register this class with move's type system
MOVE_DECLARE_REGISTERED_CLASS(mvNodeTail);

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#include "move_parameter.h"

#endif // MOVE_STATEDEF_H
