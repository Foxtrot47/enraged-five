//
// move/blend.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//
#include "move_blend.h"

#include "move_funcs.h"
#include "move_synchronizer.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

unsigned mvNodePairDef::GetWeightFlagFrom(u32 flags)
{
	return flags & 0x00000003;
}

unsigned mvNodePairDef::GetFilterFlagFrom(u32 flags)
{
	return (flags & 0x0000000c) >> 0x02;
}

// NOTE: probably some space in here that flags could go if needed!

unsigned mvNodePairDef::GetTransitionalFlagFrom(u32 flags)
{
	return (flags & 0x00000040) >> 0x06;
}

unsigned mvNodePairDef::GetImmutableFlagFrom(u32 flags)
{
	return (flags & 0x00000180) >> 0x07;
}

unsigned mvNodePairDef::GetChild0InfluenceOverrideFlagFrom(u32 flags)
{
	return (flags & 0x00003000) >> 0x0c;
}

unsigned mvNodePairDef::GetChild1InfluenceOverrideFlagFrom(u32 flags)
{
	return (flags & 0x0000c000) >> 0x0e;
}

unsigned mvNodePairDef::GetSynchronizerFrom(u32 flags)
{
	return (flags & 0x00180000) >> 0x13;
}

unsigned mvNodePairDef::GetMergeBlendFrom(u32 flags)
{
	return (flags & 0x80000000) >> 0x1f;
}


//////////////////////////////////////////////////////////////////////////

void mvNodePairDef::Create(crmtNodePairWeighted* node, mvCreateContext* context) const
{
	const mvMotionWeb* network = context->m_Network;
	const mvParameterBuffer* params = context->m_Parameters;
	u32 flags = m_Flags;

	unsigned weightf = GetWeightFlagFrom(flags);
	unsigned filterf = GetFilterFlagFrom(flags);
	unsigned transitional = GetTransitionalFlagFrom(flags);
	unsigned immutable = GetImmutableFlagFrom(flags);
	unsigned influence0 = GetChild0InfluenceOverrideFlagFrom(flags);
	unsigned influence1 = GetChild1InfluenceOverrideFlagFrom(flags);
	unsigned sync = GetSynchronizerFrom(flags);

	const void* block = this + 1;

	u32 tag = 0;
	if (sync == mvConstant::kSynchronizerTag)
	{
		tag = ReadUInt32Block();
	}

	// weight
	float weight = 0.f;
	switch (weightf)
	{
	case mvConstant::kFlagValue:
		{
			weight = ReadFloatBlock();
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(0.f);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			weight = param->GetReal();
		}
		break;
	}

	// filter
	crFrameFilter* filter = NULL;
	switch (filterf)
	{
	case mvConstant::kFlagValue:
		{
			filter = mvAllocAndInitFilter(network, block);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetFilterUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterFilter, &ret);
			filter = param->GetFilter();
		}
		break;
	}

	node->SetWeight(weight);
	node->SetFilter(filter);

	// TODO: when nodes are fixed size just attach the synchronizer back in the node create
	if (node->GetNodeType() == crmtNode::kNodeBlend)
	{
		mvSynchronizer::AttachToBlend(*(crmtNodeBlend*)node, sync, tag, transitional?true:false, immutable);
	}
	else
	{
		Assert(node->GetNodeType() == crmtNode::kNodeAddSubtract);
		mvSynchronizer::AttachToAdd(*(crmtNodeAddSubtract*)node, sync, tag, immutable);
	}

	mvNodeDef* input0 = m_Inputs[0].Get<mvNodeDef>();
	crmtNode* child0 = input0->Create(context);
	node->AddChild(*child0);

	mvNodeDef* input1 = m_Inputs[1].Get<mvNodeDef>();
	crmtNode* child1 = input1->Create(context);
	node->AddChild(*child1);

	FastAssert(influence0 == crmtNodePair::kInfluenceOverrideNone || influence0 == crmtNodePair::kInfluenceOverrideZero || influence0 == crmtNodePair::kInfluenceOverrideOne);
	node->SetInfluenceOverride(0, crmtNodePair::eInfluenceOverride(influence0));

	FastAssert(influence1 == crmtNodePair::kInfluenceOverrideNone || influence1 == crmtNodePair::kInfluenceOverrideZero || influence1 == crmtNodePair::kInfluenceOverrideOne);
	node->SetInfluenceOverride(1, crmtNodePair::eInfluenceOverride(influence1));
}

//////////////////////////////////////////////////////////////////////////

void mvNodePairDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_Flags, 1);

	m_Inputs[0].Init(resource);
	m_Inputs[1].Init(resource);

	u32 flags = m_Flags;
	unsigned weightf = GetWeightFlagFrom(flags);
	MV_VALIDATE(weightf < mvConstant::kFlagCount, "(mvNodePairDef) weight flag out of range");
	unsigned filterf = GetFilterFlagFrom(flags);
	MV_VALIDATE(filterf < mvConstant::kFlagCount, "(mvNodePairDef) filter flag out of range");
	unsigned sync = GetSynchronizerFrom(flags);
	MV_VALIDATE(sync < mvConstant::kSynchronizerCount, "(mvNodePairDef) sync flag out of range");
	void* block = this + 1;

	if (sync == mvConstant::kSynchronizerTag)
	{
		u32* dest = (u32*)block;
		resource->ReadInt(dest, 1);
		block = dest+1;
	}

	// weight
	MV_REAL_BLOCK_INIT_FLAG(block, resource, weightf);
	// filter
	MV_FILTER_BLOCK_INIT(block, resource, filterf);
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodePairDef::QueryMemorySize(u32 size) const
{
	const void* block = this+1;
	u32 flags = m_Flags;

	unsigned weightf = GetWeightFlagFrom(flags);
	unsigned filterf = GetFilterFlagFrom(flags);
	unsigned sync = GetSynchronizerFrom(flags);

	if (sync == mvConstant::kSynchronizerTag)
	{
		size += sizeof(u32);
		block = (u32*)block + 1;
	}

	// weight
	MV_REAL_BLOCK_SIZE_FLAG(block, weightf, size);
	// filter
	MV_FILTER_BLOCK_SIZE(block, filterf, size);

	return size;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeBlendDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeBlend* mvNodeBlendDef::Create(mvCreateContext* context) const
{
	mvNodeBlend* node = mvNodeBlend::CreateNode(*context->m_MotionTree);
	mvNodePairDef::Create(node, context);

	unsigned mergeBlend = GetMergeBlendFrom(m_Flags);

	node->SetWeightRate(0.f);
	node->SetDestroyWhenComplete(false);
	node->SetMergeBlend(mergeBlend != 0);
	node->SetWeightModifier(NULL);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeBlendDef::Init(mvMemoryResource* resource)
{
	mvNodePairDef::Init(resource);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeBlendDef::QueryMemorySize() const
{
	return mvNodePairDef::QueryMemorySize(sizeof(mvNodeBlendDef));
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeBlendDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeBlend::ApplyFilter),
		MOVE_MAKE_SIGNAL(mvNodeBlend::ApplyWeight)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeBlendDef::ResolveParameterSetType(u16 parameter)
{
	// these need to match the order in ::ResolveParameterSet
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterFilter,
		mvParameter::kParameterReal
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeBlendDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeBlend::GetFilter),
		MOVE_MAKE_SIGNAL(mvNodeBlend::GetWeight)
	};

	Assert(parameter < kParameterCount);
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeBlendDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeBlend, crmtNodeBlend, mvNode::kNodeBlend, mvNodeBlendDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
