//
// move/move_clip.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_CLIP_H
#define MOVE_CLIP_H

#include "move_node.h"
#include "move_observer.h"

#include "crclip/clip.h"
#include "crmetadata/properties.h"
#include "crmotiontree/nodeclip.h"

namespace rage {

class crClip;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create a clip node
class mvNodeClipDef : public mvNodeDef
{
public:
	// PURPOSE: Context determining how a clip should be sourced
	// NOTES: U+1F4A9
	enum eClipContext
	{
		kClipContextVariableClipSet,
		kClipContextClipDictionary,
		kClipContextAbsoluteClipSet,
		kClipContextLocalFile
	};

	// PURPOSE: Create an instance of this clip node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeClip* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 type);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeClipDef);

private:
	static unsigned GetClipFlagFrom(u32 flags);
	static unsigned GetPhaseFlagFrom(u32 flags);
	static unsigned GetRateFlagFrom(u32 flags);
	static unsigned GetDeltaFlagFrom(u32 flags);
	static unsigned GetLoopedFlagFrom(u32 flags);
	static unsigned GetIsZeroWeightFlagFrom(u32 flags);

	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES: Flag layout is:
	// Clip - flags & 0x00000003
	// Phase - (flags & 0x0000000c) >> 0x02
	// Rate - (flags & 0x00000030) >> 0x04
	// Delta - (flags & 0x000000c0) >> 0x06
	// Looped - (flags & 0x00000300) >> 0x08
	// Is Zero Weight - (flags & 0x00000c00) >> 0x0A
	u32 m_flags;
};

// PURPOSE: Thunk generation of keyed property type
// NOTES: U+1F4A9
#define MOVE_PROPERTY_GET_KEYED(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32 _key) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetPropertyUnsafe(_this->__signal(_key)); \
	} \
	const crProperty* __signal(u32) const;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move clip node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeClip : public crmtNodeClip
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_CLIP_SET(T, ApplyClip) \
		MOVE_CLIP_GET(T, GetClip) \
		MOVE_REAL_SET(T, ApplyPhase) \
		MOVE_REAL_GET(T, GetPhase) \
		MOVE_REAL_SET(T, ApplyRate) \
		MOVE_REAL_GET(T, GetRate) \
		MOVE_REAL_SET(T, ApplyDelta) \
		MOVE_REAL_GET(T, GetDelta) \
		MOVE_BOOLEAN_SET(T, ApplyLooped) \
		MOVE_BOOLEAN_GET(T, GetLooped) \
		MOVE_PROPERTY_GET_KEYED(T, GetProperty) \
	
	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeClip)

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeClip);
};

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeClip::ApplyClip(const crClip* clip)
{
	crClipPlayer& player = GetClipPlayer();
	player.SetClip(clip);
}

///////////////////////////////////////////////////////////////////////////////

inline const crClip* mvNodeClip::GetClip() const
{
	const crClipPlayer& player = GetClipPlayer();
	return player.GetClip();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeClip::ApplyPhase(float phase)
{
	crClipPlayer& player = GetClipPlayer();
	const crClip* clip = player.GetClip();
	if (clip) {
		player.SetTime(clip->ConvertPhaseToTime(phase));
	}
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeClip::GetPhase() const
{
	const crClipPlayer& player = GetClipPlayer();
	const crClip* clip = player.GetClip();
	if (clip) {
		return clip->ConvertTimeToPhase(player.GetTime());
	}

	return 0.f;
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeClip::ApplyRate(float rate)
{
	crClipPlayer& player = GetClipPlayer();
	player.SetRate(rate);
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeClip::GetRate() const
{
	const crClipPlayer& player = GetClipPlayer();
	return player.GetRate();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeClip::ApplyDelta(float delta)
{
	crClipPlayer& player = GetClipPlayer();
	player.SetDeltaHiddenSupplement(delta);
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeClip::GetDelta() const
{
	const crClipPlayer& player = GetClipPlayer();
	return player.GetDeltaHiddenSupplement();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeClip::ApplyLooped(bool looped)
{
	crClipPlayer& player = GetClipPlayer();
	player.SetLooped(true, looped);
}

///////////////////////////////////////////////////////////////////////////////

inline bool mvNodeClip::GetLooped() const
{
	const crClipPlayer& player = GetClipPlayer();
	return player.IsLooped();
}

//////////////////////////////////////////////////////////////////////////

inline const crProperty* mvNodeClip::GetProperty(u32 key) const
{
	const crClipPlayer& player = GetClipPlayer();
	const crClip* clip = player.GetClip();
	if (clip != NULL) {
		const crProperties* properties = clip->GetProperties();
		return properties->FindProperty(key);
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

#if HACK_GTA4 && !NEW_MOVE_TAGS

//////////////////////////////////////////////////////////////////////////
//
//	***** GSALES - Temporary hack to expose tags on clips *****
//	This observer exposes any type generic string tag with a given
//	subtype in a signal parameter with the name specified in the string.
//	This is provided purely as a temporary means of exposing tags now
//	without having to make any tool changes and will be replaced as soon
//	as the new property based tags are brought over from the head.
//
//	The observer now a
//////////////////////////////////////////////////////////////////////////

class mvUpdateEventFromClipTag : public crmtObserver
{
public:

	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

	enum eRelevantTo {
		kRelevantToState = 0,
		kRelevantToSubnetwork
	};

	void Init(mvMotionWeb* web, u32 subType)
	{
		m_Web = web;
		m_SubType = subType;
	}
private:

	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: Returns false if the clip's weight amounts to zero within the current state
	bool IsClipNodeRelevant(float fRelevantWeightThreshold, float &compositeWeight, eRelevantTo relevantTo = kRelevantToState);


public:	

	mvUpdateEventFromClipTag()
	{
	}

	~mvUpdateEventFromClipTag()
	{
		Shutdown();
	}

	virtual void Shutdown()
	{
		m_Web = NULL;
	}
public:
	MOVE_DECLARE_ALLOCATOR();
private:
	mvMotionWeb* m_Web; //a pointer to the motion web this observer is attached to
	u32 m_SubType;  // the subtype used to identify the tags to fire as events
};

#endif //HACK_GTA4 && !NEW_MOVE_TAGS


} // namespace rage

#endif // MOVE_CLIP_H
