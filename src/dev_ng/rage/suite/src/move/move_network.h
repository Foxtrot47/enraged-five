//
// move/network.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_NETWORK_H
#define MOVE_NETWORK_H

#include "move_node.h"
#include "move_command.h"
#include "move_fixedheap.h"
#include "move_types_internal.h"

#include "atl/functor.h"
#include "paging/base.h"

#define MOVE_NETWORK_H_SAFE


namespace rage {

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Manage flags and requests. Network-wide state data.
// NOTES:
//	Since crc's of flags and requests are stored in the network def
//	There's no need to hold an int for each request / flag
//	We'll accept a maximum number of requests and flags per network (32 at the moment)
//	in order to remove the need for a defrag heap
struct mvSignalData
{
	// PURPOSE: Flag storage
	u32 m_Requests;
	u32 m_Flags;

	// PURPOSE: Constructor
	mvSignalData();

	// PURPOSE: Reset flags and request bits
	void Reset();

	// PURPOSE: Only reset request bits
	// NOTES:
	//	Flags are an on-off switch controlled by the user.
	//	Requests get reset every update and must be pushed
	//	back in by the user if they're to stay on.
	void ResetRequests();

	// PURPOSE: Convenience method to shift of request bits
	u32 GetRequests() const;

	// PURPOSE: Convenience method to mask of flag bits
	u32 GetFlags() const;

	// PURPOSE: Convenience method to set request bit
	void SetRequest(u32 index);

	// PURPOSE: Convenience method to get request bit
	bool GetRequest(u32 index) const;

	// PURPOSE: Convenience method to set flag bit
	void SetFlag(u32 index, bool value);

	// PURPOSE: Convenience method to get flag bit
	bool GetFlag(u32 index) const;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory representation of a network definition
// NOTES:
//	While the definition is serialized in, the on disk representation
//	is the same memory layout in memory with the exception of any
//	padding the compiler may have added (if any).
class mvNetworkDef : public pgBaseRefCounted
{
public:
	// PURPOSE: Constructor
	mvNetworkDef();

	// PURPOSE: Destructor
	~mvNetworkDef();

	// PURPOSE: Serialize in the network definition
	// RETURNS:
	//	true  - Success
	//	false - Wrong file type of incorrect version. Check tty.
	// NOTES:
	//	Will also load debug names if they exist as well as
	//	any referenced networks (the path and names of these
	//	are baked into the network).
	bool Load(const char* filename);

	// PURPOSE: Instantiate a new network based on this definition
	// RETURNS: New network. NULL on serialization failure (if it
	//	makes it this far.
	// NOTES:
	//	outputBuffer must be a valid pointer. Ownership of
	//	outputBuffer must be maintained by the caller and must
	//	not be destroyed before the returned network has finished
	//	updating and has been destroyed.
	mvNetwork* Create(mvParameterBuffers& outputBuffer) const;

	// PURPOSE: The raw network definition data.
	// NOTES: Not much use to anyone other than mvNetwork::Load
	u8* GetNetwork() const { return m_network; }

	// PURPOSE: Definition request lookup
	// NOTES: An array of (key,index) pairs that allow a hash
	//	to index transform for finding specific requests in the
	//	per instance request bits.
	const request_t* GetRequests() const { return m_requests; }

	// PURPOSE:
	u32 GetRequestCount() const { return m_reqcount; }

	// PURPOSE:
	int ConvertRequestKeyToIndex(u32 key) const;


	// PURPOSE: Definition flag lookup
	// NOTES: An array of (key, index) pairs that allow a hash
	//	to index transform for finding specific flags in the
	//	per instance flag bits.
	inline const flag_t* GetFlags() const { return m_flags; }

	// PURPOSE:
	inline u32 GetFlagCount() const { return m_flagcount; }

	// PURPOSE:
	int ConvertFlagKeyToIndex(u32 key) const;

#if CR_DEV
	// PURPOSE: Debug get network name
	const char* GetName() const { return m_Name; }
#endif // CR_DEV

private:
	// PURPOSE: Request (key,index) hash lookup
	request_t* m_requests;

	// PURPOSE: How many requests in the network
	u32 m_reqcount;

	// PURPOSE: Flags (key, index) hash lookup
	flag_t* m_flags;

	// PURPOSE: How many flags
	u32 m_flagcount;

	// PURPOSE: Number of networks this network references
	u32 m_XRefCount;

	// PURPOSE: Pointer to reference names
	// NOTE: These don't need to be stored, that's just a hang up
	//	from the initial implementation that was never fully removed
	u32* m_XRefs;

	// PURPOSE: Actual move network data
	u8* m_network;

#if CR_DEV
	const char* m_Name;
#endif // CR_DEV

#if __ASSERT
public:
	virtual const char *GetDebugName(char * /*buffer*/, size_t /*bufferSize*/) const { return ms_GetNameFunctor ? ms_GetNameFunctor(this) : "NO NAME"; }
	typedef Functor1Ret<const char*, const mvNetworkDef*> NetworkDefNameFunctor;
	static void SetNetworkDefNameFunctor(NetworkDefNameFunctor func) { ms_GetNameFunctor = func; }
private:
	static NetworkDefNameFunctor ms_GetNameFunctor;
#endif // __ASSERT
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Base class for network and subnetwork
// NOTES:
//	Internally move will try keep the most concrete network type possible,
//	this usually ends up being the mvMotionWeb base though since most
//	functionality is shared and handles that are stored internally could
//	be either the main network or a subnetwork.
class mvMotionWeb
{
public:
	// PURPOSE: Constructor
	mvMotionWeb();

	// PURPOSE: Destructor
	virtual ~mvMotionWeb() {}

	// PURPOSE: Reference counting
	// NOTES:
	//	Networks and subnetworks are reference counted because of uncertain
	//	ownership scope. Rather than force the client code to do some kind of
	//	wait/poll to figure out if a network can be freed we allow them
	//	to release and allow the lifetime to then be managed by the motion tree/move.
	void AddRef() const;

	// PURPOSE: Reference counting
	int Release() const;

	// PURPOSE: Reference counting
	int GetNumRefs() const { return m_RefCount; }

	// PURPOSE: Network-wide signal data
	const mvSignalData& GetSignalData() const { return m_SignalData; }

	// PURPOSE: Network-wide signal data
	mvSignalData& GetSignalData() { return m_SignalData; }

	// PURPOSE: Parameter buffer
	// NOTES:
	//	This is the current parameter buffer. Parameters from the command buffer
	//	that have been pushed into the network this frame will be available in this
	//	buffer for internal move machinery to access.
	//	As part of a PrepareProcessBuffer phase this buffer will swap with the
	//	internal parameter buffer, allowing parameters that were pushed during the
	//	move update to be used on the next frame/update.
	// SEE ALSO:
	//	mvMotionWeb::GetInternalParameters
	//	mvNetwork::PrepareProcessBuffers
	const mvParameterBuffer* GetParameters() const  { return &m_ParameterBuffers.GetCurrentBuffer(); }
	mvParameterBuffer* GetParameters()  { return &m_ParameterBuffers.GetCurrentBuffer(); }

	// PURPOSE: Internal parameter buffer
	// NOTES:
	//	This is the buffer that internal elements inside move push their results
	//	into if they're to be used (eg. output parameters before they become output
	//	parameters). After the PrepareProcessBuffer phase this parameter buffer
	//	will become the current parameter buffer making these internally added parameters
	//	available to other parts of the system that require them as input.
	// SEE ALSO:
	//	mvMotionWeb::GetParameters
	//	mvNetwork::PrepareProcessBuffers
	const mvParameterBuffer* GetInternalParameters() const  { return &m_ParameterBuffers.GetNextBuffer(); }
	mvParameterBuffer* GetInternalParameters()  { return &m_ParameterBuffers.GetNextBuffer(); }

	// PURPOSE: Get both the current and internal parameter buffers
	const mvParameterBuffers& GetParameterBuffers() const  { return m_ParameterBuffers; }
	mvParameterBuffers& GetParameterBuffers()  { return m_ParameterBuffers; }


	// PURPOSE: Output ring buffer
	// NOTES:
	//	Internally pushed parameters need to be made available to client code. This
	//	serves as an interface between that buffer and the client so the threading
	//	issues are more easily managed.
	//	The choice of a ring buffer was originally chosen to allow the game to read
	//	on demand in a thread-safe manner without requiring locking and managing
	//	buffers. The game instead chose to buffer all the data in this buffer every
	//	frame, potentially making this buffer redundant (this hasn't been varified).
	mvParameterBuffers* GetOutputBuffer() { return m_OutputBuffers; }

	// PURPOSE: Network definition this instance is following
	const mvNetworkDef* GetDefinition() const { return m_NetworkDef; }

	// PURPOSE: Hack handle to the owning entity
	void* GetMoveNetworkInterface() const {return m_Entity; }

	// PURPOSE: Hack handle to the owning entity
	void SetMoveNetworkInterface(void* pEntity) { m_Entity = pEntity; }

protected:
	// PURPOSE: Fixed bit arrays of requests and flags
	mvSignalData m_SignalData;

	// PURPOSE: The current and internal parameter buffers
	mvParameterBuffers m_ParameterBuffers;

	// PURPOSE: Output buffer
	//	See GetOutputBuffer for more details
	mvParameterBuffers* m_OutputBuffers;

	// PURPOSE: Network definition
	//	See GetDefintion for more details
	const mvNetworkDef* m_NetworkDef;

	// PURPOSE: Hack entity
	//	See GetMoveNetworkInterface for more details
	void* m_Entity;

	// PURPOSE:
	mutable u32 m_RefCount;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Handle to the main move network.
//	Each animatable 'thing' should only have one of these
class mvNetwork : public mvMotionWeb
{
public:
	// PURPOSE: Constructor
	// NOTES: output parameter ring buffer and def CANNOT be NULL
	mvNetwork(mvParameterBuffers& output, const mvNetworkDef* def);

	// PURPOSE: Initialization
	// NOTES:
	//	You must maintain ownership of the command buffer.
	//	All communication with the network will be through
	//	this command buffer
	void Init(crmtMotionTree& mt, mvCommandBuffers& cmds);

	// PURPOSE: Dump the current state of network flags and requests
	void Dump() const;

	// PURPOSE: Flip internal buffers. Insert payload data from
	//	the command buffer into the internal parameter buffer.
	// NOTES:
	//	There's no thread protection around this. It's assumed
	//	that this call will be done in a DMZ after the motion
	//	tree update and after the AI has finished pushing parameters.
	void PrepareBuffers(mvCommandBuffers& cmds);

	// PURPOSE: Allocate from move's memory allocator
	MOVE_DECLARE_ALLOCATOR();
};

/////////////////////////////////////////////////////////////////

inline mvSignalData::mvSignalData()
{
	Reset();
}

//////////////////////////////////////////////////////////////////////////

inline void mvSignalData::Reset()
{
	m_Requests = m_Flags = 0;
}

//////////////////////////////////////////////////////////////////////////

inline void mvSignalData::ResetRequests()
{
	m_Requests = 0;
}

//////////////////////////////////////////////////////////////////////////

inline u32 mvSignalData::GetRequests() const
{
	return m_Requests;
}

//////////////////////////////////////////////////////////////////////////

inline u32 mvSignalData::GetFlags() const
{
	return m_Flags;
}

//////////////////////////////////////////////////////////////////////////

inline void mvSignalData::SetRequest(u32 index)
{
	m_Requests |= 1<<index;
}

//////////////////////////////////////////////////////////////////////////

inline bool mvSignalData::GetRequest(u32 index) const
{
	return (m_Requests & (1<<index))!=0;
}

//////////////////////////////////////////////////////////////////////////

inline void mvSignalData::SetFlag(u32 index, bool value)
{
	if (value)
		m_Flags |= 1<<index;
	else
		m_Flags &= ~(1<<index);
}

//////////////////////////////////////////////////////////////////////////

inline bool mvSignalData::GetFlag(u32 index) const
{
	return (m_Flags & (1<<index))!=0;
}

//////////////////////////////////////////////////////////////////////////

inline int mvNetworkDef::ConvertRequestKeyToIndex(u32 key) const
{
	const request_t* requests = GetRequests();
	u32 count = GetRequestCount();
	if (count != 0)
	{
		u32 i = key % count;
		while (requests[i].key != 0xffffffff)
		{
			if(requests[i].key == key)
			{
				return requests[i].index;
			}
			i = (i + 1) % count;
		}
	}
	return -1;
}

//////////////////////////////////////////////////////////////////////////

inline int mvNetworkDef::ConvertFlagKeyToIndex(u32 key) const
{
	const flag_t* flags = GetFlags();
	u32 count = GetFlagCount();
	if (count != 0)
	{
		u32 i = key % count;
		while (flags[i].key != 0xffffffff)
		{
			if(flags[i].key == key)
			{
				return flags[i].index;
			}
			i = (i + 1) % count;
		}
	}
	return -1;
}

//////////////////////////////////////////////////////////////////////////

inline void mvNetwork::PrepareBuffers(mvCommandBuffers& cmds)
{
	GetParameterBuffers().SwapBuffers();

	// If there is a debug interface on this network, then we ignore that parameter buffer. Internal parameter buffer is ok.
	cmds.GetNextBuffer().PrepareBuffer(*this, *m_NetworkDef, *GetParameters()); 
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#include "move_parameter.h"

#endif // MOVE_NETWORK_H
