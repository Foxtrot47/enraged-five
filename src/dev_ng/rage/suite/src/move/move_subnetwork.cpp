//
// move/move_subnetwork.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_subnetwork.h"

#include "move_command.h"
#include "move_network.h"
#include "move_observer.h"
#include "move_parameter.h"
#include "move_parameterbuffer.h"

#include "crmotiontree/iterator.h"
#include "crmotiontree/nodeblend.h"
#include "crmotiontree/nodeinvalid.h"

namespace rage {

const u32 mvSubNetwork::ms_BlendDurationId = 3526052382u; // "SubnetworkBlendDuration"
const u32 mvSubNetwork::ms_BlendOutDurationId = 1541263251;	// "SubnetworkBlendOutDuration"

//////////////////////////////////////////////////////////////////////////

mvNodeSubNetwork::mvNodeSubNetwork()
: crmtNodeParent(kNodeSubNetwork)
{
}

//////////////////////////////////////////////////////////////////////////

mvNodeSubNetwork::mvNodeSubNetwork(datResource& rsc)
: crmtNodeParent(rsc)
{
}

//////////////////////////////////////////////////////////////////////////

mvNodeSubNetwork::~mvNodeSubNetwork()
{
	Shutdown();
}

//////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(mvNodeSubNetwork, crmtNode::kNodeSubNetwork, CRMT_NODE_RECEIVE_PREUPDATES|CRMT_NODE_RECEIVE_UPDATES);

//////////////////////////////////////////////////////////////////////////

void mvNodeSubNetwork::Init(mvNetwork* root, const mvMotionWeb* network, u32 networkId)
{
	m_OwnerNetwork = network;
	m_OwnerNetwork->AddRef();

	m_RootNetwork = root;
	m_NetworkId = networkId;
	m_Data = NULL;
	m_DisableInserts = false;

	crmtMotionTree& motionTree = GetMotionTree();
	crmtNode* ret = NULL;
	const mvParameterBuffer* params = m_OwnerNetwork->GetParameters();
	const mvParameter* param = params->FindFirst(m_NetworkId, NULL);
	if (param != NULL)
	{
		mvSubNetwork* sub = param->GetNetwork();
		if (sub != NULL)
		{
//			Displayf("Found subnetwork: %d", m_NetworkId);
			const mvNetworkDef* def = sub->GetDefinition();
			mvNodeDef* root = (mvNodeDef*)def->GetNetwork();

			m_Data = sub;
			m_Data->AddRef();

			// DARCHARD: Not attached? I think there should be a SetAttached here somewhere! Probably never being used (eg. Everything gets attached in ::Update)
			mvCreateContext context;
			context.m_MotionTree = &motionTree;
			context.m_Network = sub;
			context.m_OwnerNetwork = m_RootNetwork;
			context.m_Parameters = m_Data->GetParameters();
			context.m_Tail = NULL;

			ret = root->Create(&context);
		}
	}

	if(!ret)
	{
		crmtNodeInvalid* node = crmtNodeInvalid::CreateNode(motionTree);
		node->Init();
		ret = node;
	}

	AddChild(*ret, false);
}

//////////////////////////////////////////////////////////////////////////

void mvNodeSubNetwork::Shutdown()
{
	crmtNodeParent::Shutdown();

	if (m_Data != NULL)
	{
		m_Data->SetAttached(false);
		m_Data->Release();
		m_Data = NULL;
	}

	m_OwnerNetwork->Release();
}

//////////////////////////////////////////////////////////////////////////

// NOTES: If the network id for this subnetwork exists in the parameter buffer,
//	we're going to change the subnetwork. If it's NULL then we replace the subnetwork
//	with an invalid node, if it's another network definition then we build
//	up those motion tree nodes and slot them in underneath this subnetwork node.
void mvNodeSubNetwork::PreUpdate(float)
{
	crmtMotionTree& motionTree = GetMotionTree();
	if (m_DisableInserts)
		return;

	const mvParameterBuffer* params = m_OwnerNetwork->GetParameters();
	const mvParameter* param = params->FindFirst(m_NetworkId, NULL);
	if (param != NULL)
	{
		mvSubNetwork* sub = param->GetNetwork();
		if (sub != NULL)
		{
//			Displayf("Found subnetwork: %d", m_NetworkId);
			if (m_Data != NULL)
			{
				m_Data->SetAttached(false);
				m_Data->Release();
			}
			m_Data = sub;
			m_Data->AddRef();

			m_Data->SetAttached(true);

			const mvNetworkDef* def = sub->GetDefinition();
			mvNodeDef* root = (mvNodeDef*)def->GetNetwork();

			mvCreateContext context;
			context.m_MotionTree = &motionTree;
			context.m_Network = sub;
			context.m_OwnerNetwork = m_RootNetwork;
			context.m_Parameters = sub->GetParameters();
			context.m_Tail = NULL;

			crmtNode* ret = root->Create(&context);
			float duration = 0.0f;

			// Look for a custom observer coming from the subnetwork's parameter buffer
			if (ret && m_Data)
			{
				static u32 s_customObserverId = ATSTRINGHASH("move_network_observer",0x7DD50177);

				const mvParameterBuffer* params = m_Data->GetParameters();
				const mvParameter* param = params->FindFirst(s_customObserverId, NULL);

				if (param)
				{
					mvObserver* observer = param->GetObserver();
					if (observer)
					{
						observer->Attach(*(ret));
					}
				}

				// TEMPORARY FUNCTIONALITY!
				// provide the optional ability to provide a cross blend duration as a parameter when inserting these networks
				// Want some tool support for this in the long term so we can specify which network slot to apply the cross blend to.
				// At the moment look in the new networks param buffer for the duration to use
				const mvParameter* durationParam = params->FindFirst(mvSubNetwork::ms_BlendDurationId, NULL);
				if (durationParam != NULL)
				{
					duration = durationParam->GetReal();
				}
			}

			if (duration>0.0f)
			{
				crmtNode* old = GetFirstChild();

				crmtNodeBlend* blend = crmtNodeBlend::CreateNode(motionTree);
				blend->Init(0.f, 1.f/duration, true, false);
				old->AddRef();
				ReplaceChild(*old, *blend, false);
				blend->AddChild(*old);
				old->Release();
				blend->AddChild(*ret);
			}
			else
			{
				crmtNode* old = GetFirstChild();
				ReplaceChild(*old, *ret);
			}
		}
		else
		{
			crmtNodeInvalid* invalid = crmtNodeInvalid::CreateNode(motionTree);
			invalid->Init();
			float duration = 0.0f;

			// Look for a blend out duration coming from the parent networks param buffer
			// need proper tool support for blend durations so we can avoid clashes here!
			// at the moment all subnetworks blended out of this network on the frame this fires will get this duration
			if (m_Data)
			{
				const mvParameter* durationParam = params->FindFirst(mvSubNetwork::ms_BlendOutDurationId, NULL);
				if (durationParam != NULL)
				{
					duration = durationParam->GetReal();
				}
			}

			if (duration>0.0f)
			{
				crmtNode* old = GetFirstChild();

				crmtNodeBlend* blend = crmtNodeBlend::CreateNode(motionTree);
				blend->Init(0.f, 1.f/duration, true, false);
				old->AddRef();
				ReplaceChild(*old, *blend, false);
				blend->AddChild(*old);
				old->Release();
				blend->AddChild(*invalid);
			}
			else
			{
				crmtNode* old = GetFirstChild();
				ReplaceChild(*old, *invalid);
			}
			if (m_Data != NULL)
			{
				m_Data->SetAttached(false);
				m_Data->Release();
				m_Data = NULL;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void mvNodeSubNetwork::Update(float)
{
	bool childDisabled = GetFirstChild()->IsDisabled();
	SetDisabled(childDisabled);
	SetSilent(!childDisabled);

	if (m_Data != NULL)
	{
		m_Data->GetSignalData().ResetRequests();

		m_Data->GetOutputBuffer()->GetNextBuffer().Copy(*m_Data->GetInternalParameters());
	}
}

//////////////////////////////////////////////////////////////////////////

#if CR_DEV
void mvNodeSubNetwork::Dump(crmtDumper& dumper) const
{
	crmtNodeParent::Dump(dumper);

	dumper.Outputf(2, "network", "%s", (m_OwnerNetwork&&m_OwnerNetwork->GetDefinition())?m_OwnerNetwork->GetDefinition()->GetName():"NULL");
}
#endif // CR_DEV

//////////////////////////////////////////////////////////////////////////

void mvNodeSubNetwork::DisableInserts()
{
	m_DisableInserts = true;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeSubNetworkDef);

//////////////////////////////////////////////////////////////////////////

crmtNode* mvNodeSubNetworkDef::Create(mvCreateContext* context) const
{
	mvNodeSubNetwork* node = mvNodeSubNetwork::CreateNode(*context->m_MotionTree);
	node->Init(context->m_OwnerNetwork, context->m_Network, m_networkId);
	return node;
}

//////////////////////////////////////////////////////////////////////////

void mvNodeSubNetworkDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_networkId, 1);
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeSubNetworkDef::QueryMemorySize() const
{
	return sizeof(mvNodeSubNetworkDef);
}

//////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeSubNetworkDef::ResolveParameterSet(u16)
{
	return NULL;
}

//////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeSubNetworkDef::ResolveParameterSetType(u16)
{
	return mvParameter::ParamType(~0U);
}

//////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeSubNetworkDef::ResolveParameterGet(u16)
{
	return NULL;
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeSubNetworkDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_REGISTERED_TYPE(mvNodeSubNetworkClass, mvNode::kNodeSubNetwork, mvNodeSubNetworkDef);

//////////////////////////////////////////////////////////////////////////

mvSubNetwork::mvSubNetwork()
: mvMotionWeb ()
, m_IsAttached(false)
{
}

//////////////////////////////////////////////////////////////////////////

void mvSubNetwork::Init(const mvNetworkDef* def, mvParameterBuffers& outputBuffers)
{
	Assert(def != NULL);
	m_NetworkDef = def;

	m_OutputBuffers = &outputBuffers;
}

//////////////////////////////////////////////////////////////////////////

void mvSubNetwork::SetAttached(bool attached)
{
#if CR_DEV
	Assertf(!attached || !m_IsAttached, "Double insert of move sub network '%s'!", m_NetworkDef?m_NetworkDef->GetName():"UNKNOWN");
#endif // CR_DEV

	m_IsAttached = attached;
}

//////////////////////////////////////////////////////////////////////////

}// namespace rage
