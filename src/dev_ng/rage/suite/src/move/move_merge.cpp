//
// move/move_merge.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_merge.h"

#include "move_parameterbuffer.h"
#include "move_funcs.h"
#include "move_macros.h"
#include "move_network.h"
#include "move_synchronizer.h"

namespace rage {

extern mvFilterAllocator g_mvFilterAllocator;

//////////////////////////////////////////////////////////////////////////

unsigned mvNodeMergeDef::GetFilterFlagFrom(u32 flags)
{
	return flags & 0x00000003;
}

unsigned mvNodeMergeDef::GetChild0InfluenceOverrideFlagFrom(u32 flags)
{
	return (flags & 0x0000000c) >> 0x02;
}

unsigned mvNodeMergeDef::GetChild1InfluenceOverrideFlagFrom(u32 flags)
{
	return (flags & 0x00000030) >> 0x04;
}

unsigned mvNodeMergeDef::GetTransitionalFlagFrom(u32 flags)
{
	return (flags & 0x00000040) >> 0x06;
}

unsigned mvNodeMergeDef::GetImmutableFlagFrom(u32 flags)
{
	return (flags & 0x00000180) >> 0x07;
}

unsigned mvNodeMergeDef::GetSynchronizerFrom(u32 flags)
{
	return (flags & 0x00180000) >> 0x13;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeMergeDef);

//////////////////////////////////////////////////////////////////////////

mvNodeMerge* mvNodeMergeDef::Create(mvCreateContext* context) const
{
	mvNodeMerge* node = mvNodeMerge::CreateNode(*context->m_MotionTree);

	const mvParameterBuffer* params = context->m_Parameters;
	const mvMotionWeb* network = context->m_Network;

	const void* block = this+1;
	u32 flags = m_flags;

	unsigned filterf = GetFilterFlagFrom(flags);
	unsigned sync = GetSynchronizerFrom(flags);

	unsigned influence0 = GetChild0InfluenceOverrideFlagFrom(flags);
	unsigned influence1 = GetChild1InfluenceOverrideFlagFrom(flags);

//	unsigned transitional = GetTransitionalFlagFrom(flags);  // NOTE - transitional not possible here (only on blend2)
	unsigned immutable = GetImmutableFlagFrom(flags);

	u32 tag = 0;
	if (sync == mvConstant::kSynchronizerTag)
	{
		tag = ReadUInt32Block();
	}

	// filter
	crFrameFilter* filter = NULL;
	switch (filterf)
	{
	case mvConstant::kFlagValue:
		{
			// expected to be init'd with mvConstant::FILTER_COUNT & bone data added
			filter = mvAllocAndInitFilter(network, block);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetFilterUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterFilter, &ret);
			filter = param->GetFilter();
		}
		break;
	}

	node->Init(filter);

	FastAssert(influence0 == crmtNodePair::kInfluenceOverrideNone || influence0 == crmtNodePair::kInfluenceOverrideZero || influence0 == crmtNodePair::kInfluenceOverrideOne);
	node->SetInfluenceOverride(0, crmtNodePair::eInfluenceOverride(influence0));

	FastAssert(influence1 == crmtNodePair::kInfluenceOverrideNone || influence1 == crmtNodePair::kInfluenceOverrideZero || influence1 == crmtNodePair::kInfluenceOverrideOne);
	node->SetInfluenceOverride(1, crmtNodePair::eInfluenceOverride(influence1));

	mvSynchronizer::AttachToMerge(*node, sync, tag, immutable);

	mvNodeDef* input0 = m_inputs[0].Get<mvNodeDef>();
	crmtNode* child0 = input0->Create(context);
	node->AddChild(*child0);

	mvNodeDef* input1 = m_inputs[1].Get<mvNodeDef>();
	crmtNode* child1 = input1->Create(context);
	node->AddChild(*child1);

	return node;
}

//////////////////////////////////////////////////////////////////////////

void mvNodeMergeDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_flags, 1);

	m_inputs[0].Init(resource);
	m_inputs[1].Init(resource);

	void* block = this+1;
	u32 flags = m_flags;

	unsigned filterf = GetFilterFlagFrom(flags);
	MV_VALIDATE(filterf < mvConstant::kFlagCount, "(mvNodeMergeDef) filter flag out of range");
	unsigned sync = GetSynchronizerFrom(flags);
	MV_VALIDATE(sync < mvConstant::kSynchronizerCount, "(mvNodeMergeDef) sync flag out of range");

	if (sync == mvConstant::kSynchronizerTag)
	{
		u32* dest = (u32*)block;
		resource->ReadInt(dest, 1);
		block = dest+1;
	}

	// filter
	MV_FILTER_BLOCK_INIT(block, resource, filterf);
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeMergeDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeMergeDef);

	const void* block = this+1;
	u32 flags = m_flags;

	unsigned filterf = GetFilterFlagFrom(flags);
	unsigned sync = GetSynchronizerFrom(flags);

	if (sync == mvConstant::kSynchronizerTag)
	{
		size += sizeof(u32);
		block = (u32*)block + 1;
	}

	// filter
	MV_FILTER_BLOCK_SIZE(block, filterf, size);

	return size;
}

//////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeMergeDef::ResolveParameterSet(u16)
{
	return MOVE_MAKE_SIGNAL(mvNodeMerge::ApplyFilter);
}

//////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeMergeDef::ResolveParameterSetType(u16)
{
	return mvParameter::kParameterFilter;
}

//////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeMergeDef::ResolveParameterGet(u16)
{
	return MOVE_MAKE_SIGNAL(mvNodeMerge::GetFilter);
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeMergeDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeMerge, crmtNodeMerge, mvNode::kNodeMerge, mvNodeMergeDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
