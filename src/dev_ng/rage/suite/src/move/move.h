//
// move/move.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_MOVE_H
#define MOVE_MOVE_H

#include "move_network.h"
#include "move_subnetwork.h"
#include "move_types.h"

#include "atl/functor.h"
#include "atl/pool.h"

namespace rage {

class crAnimation;
class crClip;
class crExpressions;
class crpmParameterizedMotion;
class crFrameFilterBoneMultiWeight;

class mvNetwork;
class mvRelocatablePool;

// PURPOSE: Temporary flag to enable/disable reference counting on the output buffer
// NOTE: Not for your use!
// TODO: Remove. It's been enabled for long enough.
extern bool g_mvRefOutputs;

// PURPOSE: Main entry point to move
class mvMove
{
public:
	// PURPOSE: Initialization parameters
	struct InitParams
	{
		// PURPOSE: Pointer to the memory move can use for its allocator
		// NOTES: Move doesn't own this memory, it must be freed by the
		// caller.
		void* PageMemBuffer;

		// PURPOSE: The size of PageMemBuffer
		u32 PageMemBufferSize;

		// PURPOSE: The maximum number of network defs that can be stored
		u32 NetworkRegistrySize;

		// PURPOSE: Default constructor
		InitParams()
		: PageMemBuffer (NULL)
		, PageMemBufferSize (0)
		, NetworkRegistrySize (8)
		{}
	};
public:
	// PURPOSE: Initialize move and it's sub-systems
	// NOTES: Override any loaders and allocaters as you see fit
	static void InitClass(InitParams* params = NULL, const mvAnimationLoader* animLoader = NULL,
		const mvClipLoader* clipLoader = NULL, const mvExpressionLoader* exprLoader = NULL,
		const mvParameterizedMotionLoader* pmLoader = NULL, const mvFilterAllocator* filterAllocator = NULL,
		const mvTransitionFilterAllocator* transitionFilterAllocator = NULL,
		const mvNetworkDefLoader* networkDefLoader = NULL);

	// PURPOSE: Debug functionality
	// NOTES: I don't know much about these. I think they were put in by the game team
	static void RegisterPauseChanged(Functor0 func);
	static void RegisterSingleStep(Functor0 func);
	static void RegisterTimeScaleChanged(Functor1<float> func);

	// PURPOSE: Shutdown move and it's subsystems
	static void Shutdown();

	// PURPOSE: move memory bucket
	static int sm_MemoryBucket;
};

} // namespace rage

#endif // MOVE_MOVE_H
