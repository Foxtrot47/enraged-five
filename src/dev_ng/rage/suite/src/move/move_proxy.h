//
// move/move_proxy.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_PROXY_H
#define MOVE_PROXY_H

#include "move_node.h"
#include "move_observer.h"

#include "crmotiontree/nodeproxy.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create a pose node
class mvNodeProxyDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this pose node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeProxy* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memeroy stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeProxyDef);

private:
	// PURPOSE: the id to search the parameter buffer to find the observer of the node to be proxied
	u32 m_signal;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move proxy node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeProxy : public crmtNodeProxy
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_NODE_SET(T, ApplyNode) \
		MOVE_NODE_GET(T, GetNode)

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeProxy)

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeProxy);
};

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeProxy::ApplyNode(crmtNode* node)
{
	SetProxyNode(node);
}

///////////////////////////////////////////////////////////////////////////////

inline crmtNode* mvNodeProxy::GetNode() const
{
	return GetProxyNode();
}

} // namespace rage

#endif // MOVE_PROXY_H
