//
// move/move_mergen.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_MERGEN_H
#define MOVE_MERGEN_H

#include "move_noden.h"

#include "crmotiontree/nodemergen.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create an n-way merge node
class mvNodeMergeNDef : public mvNodeNDef
{
public:
	// PURPOSE: Create an instance of this n-way merge node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeMergeN* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 type);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeMergeNDef);
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move n-way merge node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeMergeN : public crmtNodeMergeN
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_FILTER_SET(T, ApplyFilter) \
		MOVE_FILTER_GET(T, GetFilter) \
		MOVE_REAL_SET_INDEXED(T, ApplyWeight) \
		MOVE_REAL_GET_INDEXED(T, GetWeight) \
		MOVE_FILTER_SET_INDEXED(T, ApplyInputFilter) \
		MOVE_FILTER_GET_INDEXED(T, GetInputFilter)

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeMergeN)

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeMergeN);
};

//////////////////////////////////////////////////////////////////////////

inline void mvNodeMergeN::ApplyFilter(crFrameFilter* filter)
{
	crmtNodeMergeN::SetFilter(filter);
}

//////////////////////////////////////////////////////////////////////////

inline crFrameFilter* mvNodeMergeN::GetFilter() const
{
	return crmtNodeMergeN::GetFilter();
}

//////////////////////////////////////////////////////////////////////////

inline void mvNodeMergeN::ApplyWeight(u32 childidx, float weight)
{
	crmtNodeMergeN::SetWeight(childidx, weight);
}

//////////////////////////////////////////////////////////////////////////

inline float mvNodeMergeN::GetWeight(u32 childidx) const
{
	return crmtNodeMergeN::GetWeight(childidx);
}

//////////////////////////////////////////////////////////////////////////

inline void mvNodeMergeN::ApplyInputFilter(u32 childidx, crFrameFilter* filter)
{
	crmtNodeMergeN::SetInputFilter(childidx, filter);
}

//////////////////////////////////////////////////////////////////////////

inline crFrameFilter* mvNodeMergeN::GetInputFilter(u32 childidx) const
{
	return crmtNodeMergeN::GetInputFilter(childidx);
}

} // namespace rage

#endif // MOVE_MERGEN_H
