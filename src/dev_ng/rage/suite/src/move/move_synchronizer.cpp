//
// move/move_synchronizer.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_synchronizer.h"

#include "move_types_internal.h"

#include "cranimation/framedata.h"
#include "cranimation/framefilters.h"
#include "crmetadata/propertyattributes.h"
#include "crmotiontree/node.h"
#include "crmotiontree/nodeaddn.h"
#include "crmotiontree/nodeaddsubtract.h"
#include "crmotiontree/nodeblend.h"
#include "crmotiontree/nodeblendn.h"
#include "crmotiontree/nodeclip.h"
#include "crmotiontree/nodemerge.h"
#include "crmotiontree/nodeproxy.h"

namespace rage {

// PURPOSE: The business end of adding nodes to the synchronizer
class AddWeightedTargetIterator
{
public:

	AddWeightedTargetIterator(crmtSynchronizer& synchronizer, bool delayed, bool autoUpdate, bool immutable, bool warp)
		: m_Synchronizer(&synchronizer)
		, m_DelayNewTargets(delayed)
		, m_AutoUpdateWeight(autoUpdate)
		, m_Immutable(immutable)
		, m_Warp(warp)
	{
	}

	~AddWeightedTargetIterator()
	{
		m_Synchronizer = NULL;
	}

	void IterateNoSiblings(crmtNode& node)
	{
		crmtNodeParent* nodeParent = node.GetParent();
		float influence = nodeParent?nodeParent->GetChildInfluence(nodeParent->GetChildIndex(node)):1.f;

		// iterate the eldest child
		if(node.GetFirstChild())
		{
			Iterate(*node.GetFirstChild(), influence);
		}

		// visit self - will happen after all children, but before younger siblings
		Visit(node, influence);
	}

	void IterateNoSiblings(crmtNode& node, float rootInfluence)
	{
		// iterate the eldest child
		if(node.GetFirstChild())
		{
			Iterate(*node.GetFirstChild(), rootInfluence);
		}

		// visit self - will happen after all children, but before younger siblings
		Visit(node, rootInfluence);
	}

	void Iterate(crmtNode& rootNode, float rootInfluence)
	{
		const u32 N = CRMT_ITERATE_BATCH;

		crmtNode* nodes[N];
		int indices[N];
		float influences[N];

		nodes[0] = NULL;
		indices[0] = -1;
		influences[0] = rootInfluence;

		nodes[1] = &rootNode;
		indices[1] = 0;
		influences[1] = rootInfluence;

		crmtNodeParent* rootParent = rootNode.GetParent();
		if(rootParent)
		{
			indices[1] = rootParent->GetChildIndex(rootNode);
			influences[1] = rootInfluence*rootParent->GetChildInfluence(indices[1]);
		}

		int top = 1;

		do
		{
			crmtNode* node = nodes[top];

			// iterate the eldest child
			crmtNode* nodeChild = node->GetFirstChild();
			if(nodeChild && Verifyf(top < N-1, "too many nodes on the stack!"))
			{
				int index = 0;

				if(node->SkipFirstChild()) 
				{
					nodeChild = nodeChild->GetNextSibling();
					index = 1;
				}
				
				if(nodeChild)
				{
					++top;
					nodes[top] = nodeChild;
					indices[top] = index;
					influences[top] = influences[top-1]*node->GetChildInfluence(index);
					continue;
				}
			}

			// iterate the siblings
			do
			{
				node = nodes[top];

				crmtNode* nodeSibling = node->GetNextSibling();
				crmtNodeParent* nodeParent = node->GetParent();

				// visit self - will happen after all children, but before younger siblings
				Visit(*node, influences[top]);

				// continue with siblings or parent
				if(nodeSibling)
				{
					nodes[top] = nodeSibling;
					influences[top] = influences[top-1]*nodeParent->GetChildInfluence(++indices[top]);
					break;
				}
				else
				{
					top--;
				}
			}
			while(top > 0);
		}
		while (top > 0);
	}

	__forceinline void Visit(crmtNode& node, float influence)
	{
		switch(node.GetNodeType())
		{
		case crmtNode::kNodeClip:
			{
				float weight = m_AutoUpdateWeight?influence:0.f;
				const crmtNodeClip& clipNode = static_cast<crmtNodeClip&>(node);
				weight = clipNode.IsZeroWeight()?0.f:weight;
				m_Synchronizer->AddTargetByNode(node, weight, m_DelayNewTargets, m_AutoUpdateWeight, m_Immutable, m_Warp);
			}
			break;

		case crmtNode::kNodeAnimation:
		case crmtNode::kNodePm:
			{
				float weight = m_AutoUpdateWeight?influence:0.f;
				m_Synchronizer->AddTargetByNode(node, weight, m_DelayNewTargets, m_AutoUpdateWeight, m_Immutable, m_Warp);
			}
			break;

		case crmtNode::kNodeProxy:
			{
				crmtNodeProxy& proxy = static_cast<crmtNodeProxy&>(node);
				crmtNode* proxyNode = proxy.GetProxyNode();
				if (proxyNode != NULL)
				{
					AddWeightedTargetIterator it(*m_Synchronizer, m_DelayNewTargets, m_AutoUpdateWeight, m_Immutable, m_Warp);
					it.IterateNoSiblings(*proxyNode, influence);
				}
			}
			break;

		default:
			break;
		}
	}

private:
	crmtSynchronizer* m_Synchronizer;
	bool m_DelayNewTargets;
	bool m_AutoUpdateWeight;
	bool m_Immutable;
	bool m_Warp;
};


void mvSynchronizer::AttachToMerge(crmtNodeMerge& node, u32 type, u32 tag, u32 immutableMask)
{
	switch (type)
	{
	case mvConstant::kSynchronizerPhase:
		{
			mvSynchronizerPhaseMerge* synchronizer = rage_new mvSynchronizerPhaseMerge(immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	case mvConstant::kSynchronizerTag:
		{
			mvSynchronizerTagMerge* synchronizer = rage_new mvSynchronizerTagMerge(tag, immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	}
}

void mvSynchronizer::AttachToNwayAdd(crmtNodeAddN& node, u32 type, u32 tag, u32 immutableMask)
{
	switch (type)
	{
	case mvConstant::kSynchronizerPhase:
		{
			mvSynchronizerPhaseNwayAdd* synchronizer = rage_new mvSynchronizerPhaseNwayAdd(immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	case mvConstant::kSynchronizerTag:
		{
			mvSynchronizerTagNwayAdd* synchronizer = rage_new mvSynchronizerTagNwayAdd(tag, immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	}
}

void mvSynchronizer::AttachToNway(crmtNodeN& node, u32 type, u32 tag, u32 immutableMask)
{
	switch (type)
	{
	case mvConstant::kSynchronizerPhase:
		{
			mvSynchronizerPhaseNway* synchronizer = rage_new mvSynchronizerPhaseNway(immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	case mvConstant::kSynchronizerTag:
		{
			mvSynchronizerTagNway* synchronizer = rage_new mvSynchronizerTagNway(tag, immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	}
}

void mvSynchronizer::AttachToAdd(crmtNodeAddSubtract& node, u32 type, u32 tag, u32 immutableMask)
{
	switch (type)
	{
	case mvConstant::kSynchronizerPhase:
		{
			mvSynchronizerPhaseAdd* synchronizer = rage_new mvSynchronizerPhaseAdd(immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	case mvConstant::kSynchronizerTag:
		{
			mvSynchronizerTagAdd* synchronizer = rage_new mvSynchronizerTagAdd(tag, immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	}
}

void mvSynchronizer::AttachToBlend(crmtNodeBlend& node, u32 type, u32 tag, bool transitional, u32 immutableMask)
{
	switch (type)
	{
	case mvConstant::kSynchronizerPhase:
		{
			mvSynchronizerPhaseBlend* synchronizer = rage_new mvSynchronizerPhaseBlend(transitional, immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	case mvConstant::kSynchronizerTag:
		{
			mvSynchronizerTagBlend* synchronizer = rage_new mvSynchronizerTagBlend(tag, transitional, immutableMask);
			synchronizer->SetAutoAddTarget(true);
			node.AddObserver(*synchronizer);
		}
		break;
	}
}

crProperty mvSynchronizerTag::sm_LeftFootHeel;
crProperty mvSynchronizerTag::sm_RightFootHeel;

mvSynchronizerTag::mvSynchronizerTag(u32 tag, bool transitional, u32 immutableMask)
: m_Synchronized(false)
, m_Transitional(transitional)
, m_ImmutableMask(immutableMask)
{
	if(m_Transitional)
	{
		SetForceLooping(true);
	}

	static const u32 AEF_FOOT_HEEL_L = (1<<5);
	static const u32 AEF_FOOT_HEEL_R = (1<<6);
	if(AEF_FOOT_HEEL_L & tag)
	{
		AddProperty(&sm_LeftFootHeel);
	}
	if(AEF_FOOT_HEEL_R & tag)
	{
		AddProperty(&sm_RightFootHeel);
	}
}

void mvSynchronizerTag::InitClass()
{
	crPropertyAttributeBool attribHeel;
	attribHeel.SetName("Heel");
	attribHeel.GetBool() = true;

	crPropertyAttributeBool attribRight;
	attribRight.SetName("Right");

	attribRight.GetBool() = false;
	sm_LeftFootHeel.SetName("Foot");
	sm_LeftFootHeel.AddAttribute(attribHeel);
	sm_LeftFootHeel.AddAttribute(attribRight);
	sm_LeftFootHeel.CalcSignature();

	attribRight.GetBool() = true;
	sm_RightFootHeel.SetName("Foot");
	sm_RightFootHeel.AddAttribute(attribHeel);
	sm_RightFootHeel.AddAttribute(attribRight);
	sm_RightFootHeel.CalcSignature();
}

static void AddTargetsForAdd(crmtSynchronizer& synchronizer, bool delayed, u32 immutableMask)
{
	synchronizer.ResetAllTargetWeights();
	crmtNodeAddSubtract* node = (crmtNodeAddSubtract*)synchronizer.GetNode();
	AddWeightedTargetIterator it0(synchronizer, delayed, true, (immutableMask&1)!=0, false);
	crmtNode* child = node->GetFirstChild();
	it0.IterateNoSiblings(*child);
	child = child->GetNextSibling();
	AddWeightedTargetIterator it1(synchronizer, delayed, false, (immutableMask&2)!=0, false);
	it1.IterateNoSiblings(*child);
}

void mvSynchronizerPhaseAdd::AddTargets()
{
	// NECESSARY - probably, appears to suppress child 1
	AddTargetsForAdd(*this, IsSynchronized(), GetImmutableMask());
}

void mvSynchronizerTagAdd::AddTargets()
{
	// NECESSARY - probably, appears to suppress child 1
	AddTargetsForAdd(*this, IsSynchronized(), GetImmutableMask());
}

static void AddWeightedTargetsForNwayAdd(crmtSynchronizer& synchronizer, bool delay, u32 immutableMask)
{
	synchronizer.ResetAllTargetWeights();
	crmtNodeN* node = (crmtNodeN*)synchronizer.GetNode();
	crmtNode* child = node->GetFirstChild();
	if (child != NULL)
	{
		AddWeightedTargetIterator it(synchronizer, delay, true, (immutableMask&1)!=0, false);
		it.IterateNoSiblings(*child);
		child = child->GetNextSibling();
	}

	int idx=1;
	while (child != NULL)
	{
		AddWeightedTargetIterator it(synchronizer, delay, false, (immutableMask&(1<<idx))!=0, false);
		it.IterateNoSiblings(*child);
		child = child->GetNextSibling();
		++idx;
	}
}

void mvSynchronizerPhaseNwayAdd::AddTargets()
{
	// NECESSARY - probably, appears to suppress child 1+
	AddWeightedTargetsForNwayAdd(*this, IsSynchronized(), GetImmutableMask());
}

void mvSynchronizerTagNwayAdd::AddTargets()
{
	// NECESSARY - probably, appears to suppress child 1+
	AddWeightedTargetsForNwayAdd(*this, IsSynchronized(), GetImmutableMask());
}

static void AddWeightedTargetsForBlend(crmtSynchronizer& synchronizer, bool delayed, bool transitional, u32 immutableMask)
{
	synchronizer.ResetAllTargetWeights();
	crmtNodePairWeighted* node = static_cast<crmtNodePairWeighted*>(synchronizer.GetNode());
	AddWeightedTargetIterator it0(synchronizer, delayed, true, (immutableMask&1)!=0, false);
	crmtNode* child = node->GetFirstChild();
	it0.IterateNoSiblings(*child);
	// TODO: Don't know that I need this... Since the siblings are iterated on anyway!
	child = child->GetNextSibling();
	AddWeightedTargetIterator it1(synchronizer, delayed || transitional, true, (immutableMask&2)!=0, transitional);
	it1.IterateNoSiblings(*child);
}

void mvSynchronizerPhaseBlend::AddTargets()
{
	// NECESSARY - doubtful, seems to replicate influence auto weight calculation
	AddWeightedTargetsForBlend(*this, IsSynchronized(), IsTransitional(), GetImmutableMask());
}

void mvSynchronizerTagBlend::AddTargets()
{
	// NECESSARY - doubtful, seems to replicate influence auto weight calculation
	AddWeightedTargetsForBlend(*this, IsSynchronized(), IsTransitional(), GetImmutableMask());
}

static void AddWeightedTargetsForMerge(crmtSynchronizer& synchronizer, bool delayed, u32 immutableMask)
{
	synchronizer.ResetAllTargetWeights();
	crmtNodeParent* node = static_cast<crmtNodeParent*>(synchronizer.GetNode());
	AddWeightedTargetIterator it0(synchronizer, delayed, true, (immutableMask&1)!=0, false);
	crmtNode* child = node->GetFirstChild();
	it0.IterateNoSiblings(*child);
	// TODO: Don't know that I need this... Since the siblings are iterated on anyway!
	child = child->GetNextSibling();
	AddWeightedTargetIterator it1(synchronizer, delayed, true, (immutableMask&2)!=0, false);
	it1.IterateNoSiblings(*child);
}

void mvSynchronizerPhaseMerge::AddTargets()
{
	// NECESSARY - doubtful, seems to replicate influence auto weight calculation
	AddWeightedTargetsForMerge(*this, IsSynchronized(), GetImmutableMask());
}

void mvSynchronizerTagMerge::AddTargets()
{
	// NECESSARY - doubtful, seems to replicate influence auto weight calculation
	AddWeightedTargetsForMerge(*this, IsSynchronized(), GetImmutableMask());
}

static void AddWeightedTargetsForNway(crmtSynchronizer& synchronizer, bool delayed, u32 immutableMask)
{
	synchronizer.ResetAllTargetWeights();
	crmtNodeN* node = (crmtNodeN*)synchronizer.GetNode();

	crmtNode* child = node->GetFirstChild();
	int idx = 0;
	while (child != NULL)
	{
		AddWeightedTargetIterator it(synchronizer, delayed, true, (immutableMask&(1<<idx))!=0, false);
		it.IterateNoSiblings(*child);
		child = child->GetNextSibling();
		++idx;
	}
}

void mvSynchronizerPhaseNway::AddTargets()
{
	// NECESSARY - doubtful, seems to replicate influence auto weight calculation
	AddWeightedTargetsForNway(*this, IsSynchronized(), GetImmutableMask());
}

void mvSynchronizerTagNway::AddTargets()
{
	// NECESSARY - doubtful, seems to replicate influence auto weight calculation
	AddWeightedTargetsForNway(*this, IsSynchronized(), GetImmutableMask());
}

} // namespace rage
