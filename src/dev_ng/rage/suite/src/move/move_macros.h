//
// move/move_macros.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_MOVE_MACROS_H
#define MOVE_MOVE_MACROS_H

#include "move_config.h"

namespace rage {


/*
	Block Init
	*/
// PURPOSE: Conditionally serialize in animation parameter def
#define MV_ANIMATION_BLOCK_INIT(_block, _resource, _flag)	\
	if (_flag == mvConstant::kFlagSignal) { \
		unsigned* dest = (unsigned*)_block; \
		_resource->ReadInt(dest, 1);	\
		_block = dest+1;	\
	} \
	else if (_flag == mvConstant::kFlagValue) { \
		unsigned* len = (unsigned*)_block; \
		_resource->ReadInt(len, 1);	\
		_block = len+1; \
		char* dest = (char*)_block;	\
		_resource->ReadByte(dest, *len);	\
		_block = dest+(*len);	\
	}

// PURPOSE: Conditionally serialize in bool parameter def
#define MV_BOOLEAN_BLOCK_INIT(_block, _resource, _flags, _mask) \
	if ((_flags & _mask) != mvConstant::kFlagIgnored) {	\
		unsigned* dest = (unsigned*)_block; \
		_resource->ReadInt(dest, 1);	\
		_block = dest+1;	\
	}

// PURPOSE: Conditionally serialize in bool parameter def
#define MV_BOOLEAN_BLOCK_INIT_FLAG(_block, _resource, _flag) \
		if (_flag != mvConstant::kFlagIgnored) {	\
		unsigned* dest = (unsigned*)_block; \
		_resource->ReadInt(dest, 1);	\
		_block = dest+1;	\
	}

// PURPOSE: Conditionally serialize in clip parameter def
#define MV_CLIP_BLOCK_INIT(_block, _resource, _flag) \
	if (_flag == mvConstant::kFlagSignal) { \
		unsigned* dest = (unsigned*)_block; \
		_resource->ReadInt(dest, 1);	\
		_block = dest+1;	\
	} \
	else if (_flag == mvConstant::kFlagValue) { \
		u32* clipContext = (u32*)_block;	\
		_resource->ReadInt(clipContext, 1);	\
		_block = clipContext+1;	\
		u32* clipParamA = (u32*)_block;	\
		_resource->ReadInt(clipParamA, 1);	\
		_block = clipParamA+1;	\
		if (*clipContext != kClipContextLocalFile) { \
			u32* clipParamB = (u32*)_block;	\
			_resource->ReadInt(clipParamB, 1);	\
			_block = clipParamB+1;	\
		} \
	}

// PURPOSE: Conditionally serialize in expression def
#define MV_EXPRESSIONS_BLOCK_INIT(_block, _resource, _flag) \
	if (_flag == mvConstant::kFlagSignal) { \
		unsigned* dest = (unsigned*)_block; \
		_resource->ReadInt(dest, 1); \
		_block = dest+1; \
	} \
	else if (_flag == mvConstant::kFlagValue) { \
		u32* expressionDict = (u32*)_block;	\
		_resource->ReadInt(expressionDict, 1);	\
		_block = expressionDict+1;	\
		u32* expression = (u32*)_block;	\
		_resource->ReadInt(expression, 1);	\
		_block = expression+1;	\
	}

	// PURPOSE: Conditionally serialize in float def
#define MV_EXPRESSIONS_VAR_BLOCK_INIT(_block, _resource, _flag) \
	u32* hash = (u32*)_block;	\
	_resource->ReadInt(hash, 1);	\
	_block = hash+1;	\
	if (_flag == mvConstant::kFlagSignal) {		  \
		u32* id = (u32*)_block;	\
		_resource->ReadInt(id, 1);	\
		_block = id+1;	\
	} \
	else if (_flag == mvConstant::kFlagValue) { \
		f32* weight = (f32*)_block;	\
		_resource->ReadFloat(weight, 1);	\
		_block = (u32*)weight+1;	\
	}

// PURPOSE: Conditionally serialize in filter def
#define MV_FILTER_BLOCK_INIT(_block, _resource, _flag) \
	if ((_flag) == mvConstant::kFlagSignal) { \
		unsigned* dest = (unsigned*)_block; \
		_resource->ReadInt(dest, 1); \
		_block = dest+1; \
	} \
	else if ((_flag) == mvConstant::kFlagValue) { \
		u32* filterDict = (u32*)_block;	\
		_resource->ReadInt(filterDict, 1);	\
		_block = filterDict+1;	\
		u32* filter = (u32*)_block;	\
		_resource->ReadInt(filter, 1);	\
		_block = filter+1;	\
	}

// PURPOSE: Conditionally serialize in filtern def
#define MV_FILTERN_BLOCK_INIT(_block, _resource, _flag) \
	if ((_flag) == mvConstant::kFlagSignal) { \
		unsigned* dest = (unsigned*)_block; \
		_resource->ReadInt(dest, 1); \
		_block = dest+1; \
	} \
	else if ((_flag) == mvConstant::kFlagValue) { \
		f32* dest = (f32*)_block; \
		_resource->ReadFloat(dest, 19); \
		_block = dest+19; \
	}

// PURPOSE: Conditionally serialize in frame def
#define MV_FRAME_BLOCK_INIT(_block, _resource, _flags, _mask) \
	if ((_flags & _mask) != mvConstant::kFlagIgnored) { \
		unsigned* dest = (unsigned*)_block; \
		_resource->ReadInt(dest, 1); \
		_block = dest+1; \
	}

// PURPOSE: Conditionally serialize in frame def
#define MV_FRAME_BLOCK_INIT_FLAG(_block, _resource, _flag) \
	if (_flag != mvConstant::kFlagIgnored) { \
	unsigned* dest = (unsigned*)_block; \
	_resource->ReadInt(dest, 1); \
	_block = dest+1; \
	}

// PURPOSE: Conditionally serialize in parameterized motion def
#define MV_PARAMETERIZED_MOTION_BLOCK_INIT(_block, _resource, _flags) \
	if ((_flags) == mvConstant::kFlagSignal) { \
		unsigned* dest = (unsigned*)_block; \
		_resource->ReadInt(dest, 1); \
		_block = dest+1; \
	} \
	else if ((_flags) == mvConstant::kFlagValue) { \
		unsigned* len = (unsigned*)_block; \
		_resource->ReadInt(len, 1);	\
		_block = len+1; \
		char* dest = (char*)_block;	\
		_resource->ReadByte(dest, *len);	\
		_block = dest+(*len);	\
	}

// PURPOSE: Conditionally serialize in float def
#define MV_REAL_BLOCK_INIT(_block, _resource, _flags, _mask) \
	if ((_flags & _mask) != mvConstant::kFlagIgnored) {	\
		f32* dest = (f32*)_block; \
		_resource->ReadFloat(dest, 1);	\
		_block = dest+1;	\
	}

// PURPOSE: Conditionally serialize in float def
#define MV_REAL_BLOCK_INIT_FLAG(_block, _resource, _flag) \
  if ((_flag) != mvConstant::kFlagIgnored) {		  \
		f32* dest = (f32*)_block; \
		_resource->ReadFloat(dest, 1);	\
		_block = dest+1;	\
	}

/*
	Block Size
	*/
// PURPOSE: Conditionally determine animation def size
#define MV_ANIMATION_BLOCK_SIZE(_block, _flag, _size) \
	if (_flag == mvConstant::kFlagSignal) { \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	} \
	else if (_flag == mvConstant::kFlagValue) { \
		unsigned* len = (unsigned*)_block; \
		size += sizeof(unsigned); \
		_block = len+1; \
		size += *len; \
		_block = (char*)block+(*len); \
	}

// PURPOSE: Conditionally determine bool def size
#define MV_BOOLEAN_BLOCK_SIZE(_block, _flags, _mask, _size) \
	if ((_flags & _mask) != mvConstant::kFlagIgnored) {	\
		size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	}

// PURPOSE: Conditionally determine bool def size
#define MV_BOOLEAN_BLOCK_SIZE_FLAG(_block, _flag, _size) \
	if (_flag != mvConstant::kFlagIgnored) {	\
		size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	}

// PURPOSE: Conditionally determine clip def size
#define MV_CLIP_BLOCK_SIZE(_block, _flag, _size) \
	if (_flag == mvConstant::kFlagSignal) { \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	} \
	else if (_flag == mvConstant::kFlagValue) { \
		u32* clipContext = (u32*)_block; \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
		if (*clipContext != kClipContextLocalFile) { \
			_size += sizeof(u32); \
			_block = (u32*)_block + 1; \
		} \
	}

// PURPOSE: Conditionally determine expression def size
#define MV_EXPRESSIONS_BLOCK_SIZE(_block, _flag, _size) \
	if (_flag  == mvConstant::kFlagSignal) { \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	} \
	else if (_flag == mvConstant::kFlagValue) { \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	}

#define MV_EXPRESSIONS_VAR_BLOCK_SIZE(_block, _flag, _size) \
	_size += sizeof(u32); \
	_block = (u32*)_block + 1; \
	if (_flag  == mvConstant::kFlagSignal) { \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	} \
	else if (_flag == mvConstant::kFlagValue) { \
		_size += sizeof(f32); \
		_block = (u32*)_block + 1; \
	}

// PURPOSE: Conditionally determine filter def size
#define MV_FILTER_BLOCK_SIZE(_block, _flag, _size) \
	if ((_flag) == mvConstant::kFlagSignal) { \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	} \
	else if ((_flag) == mvConstant::kFlagValue) { \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	}

// PURPOSE: Conditionally determine filtern def size
#define MV_FILTERN_BLOCK_SIZE(_block, _flag, _size) \
	if ((_flag) == mvConstant::kFlagSignal) { \
		_size = sizeof(u32); \
		_block = (u32*)block+1; \
	}

// PURPOSE: Conditionally determine frame def size
#define MV_FRAME_BLOCK_SIZE(_block, _flags, _mask, _size) \
	if ((_flags & _mask) != mvConstant::kFlagIgnored) { \
		_size += sizeof(u32); \
		_block = (u32*)block+1; \
	}

// PURPOSE: Conditionally determine frame def size
#define MV_FRAME_BLOCK_SIZE_FLAG(_block, _flag, _size) \
	if (_flag != mvConstant::kFlagIgnored) { \
	_size += sizeof(u32); \
	_block = (u32*)block+1; \
	}


// PURPOSE: Conditionally determine parameterized motion def size
#define MV_PARAMETERIZED_MOTION_BLOCK_SIZE(_block, _flags, _size) \
	if ((_flags) == mvConstant::kFlagSignal) { \
		_size += sizeof(u32); \
		_block = (u32*)_block + 1; \
	} \
	else if ((_flags) == mvConstant::kFlagValue) { \
		unsigned* len = (unsigned*)_block; \
		size += sizeof(unsigned); \
		_block = len+1; \
		size += *len; \
		_block = (char*)block+(*len); \
	}

// PURPOSE: Conditionally determine float def size
#define MV_REAL_BLOCK_SIZE(_block, _flags, _mask, _size) \
	if ((_flags & _mask) != mvConstant::kFlagIgnored) {	\
		size += sizeof(f32); \
		_block = (f32*)_block + 1; \
	}

// PURPOSE: Conditionally determine float def size
#define MV_REAL_BLOCK_SIZE_FLAG(_block, _flag, _size) \
  if ((_flag) != mvConstant::kFlagIgnored) {	      \
		size += sizeof(f32); \
		_block = (f32*)_block + 1; \
	}

// PURPOSE: Call the registered animation loader
#define MV_REQUISITION_ANIMATION(_id, _network) g_mvAnimationLoader(_id, _network)

// PURPOSE: Call the registered clip loader
#define MV_REQUISITION_CLIP(_clipContext, _clipParamA, _clipParamB, _network) g_mvClipLoader(_clipContext, clipParamA, clipParamB, _network)

// PURPOSE: Call the registered expressions loader
#define MV_REQUISITION_EXPRESSIONS(_expressionDict, _expression, _network) g_mvExpressionLoader(_expressionDict, _expression, _network)

// PURPOSE: Call the regisetered parameterized motion loader
#define MV_REQUISITION_PM(_id, _network) g_mvParameterizedMotionLoader(_id, _network)

// PURPOSE: Call the registered filter allocator
#define MV_ALLOC_FILTER(_filterDict, _filter, _network) g_mvFilterAllocator(_filterDict, _filter, _network)

// PURPOSE: Call the registered transition filter allocator
// NOTES: Don't think this is used any more
#define MV_ALLOC_TRANSITION_FILTER(_filterDict, _filter, _network) g_mvTransitionFilterAllocator(_filterDict, _filter, _network)

} // namespace rage

#endif // MOVE_MOVE_MACROS_H
