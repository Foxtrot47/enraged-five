//
// move/move_funcs.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_MOVE_FUNCS_H
#define MOVE_MOVE_FUNCS_H

#include "cranimation/framefilters.h"
#include "move_macros.h"
#include "move_types.h"

namespace rage {

class mvNetwork;

// PURPOSE: Access to the filter allocator
extern mvFilterAllocator g_mvFilterAllocator;

// PURPOSE: Access to the transition filter allocator
// NOTES: Doesn't appear this is being used any more.
extern mvTransitionFilterAllocator g_mvTransitionFilterAllocator;

// PURPOSE: Helper function to allocate filter
inline crFrameFilterMultiWeight* mvAllocAndInitFilter(const mvMotionWeb* network, const void* data)
{
	//get the filter file name from the data
	u32* filterDict = (u32*)data;
	data = (u32*)data+1;
	u32* filter = (u32*)data;
	data = (u32*)data+1;

	//new MoVE XML filter definition system. Move tool passes through the file name of the
	//xml filter file to load. We need to access the file here
	crFrameFilterMultiWeight* ffmw = MV_ALLOC_FILTER(*filterDict, *filter, network);
	return ffmw;
}

// PURPOSE: Helper function to allocate a transition filter
// NOTES: Doesn't appear this is being used any more.
inline mvTransitionFrameFilter* mvAllocAndInitTransitionFilter(const mvMotionWeb* network, const void* data)
{
	u32* filterDict = (u32*)data;
	data = (u32*)data+1;
	u32* filter = (u32*)data;
	data = (u32*)data+1;

	mvTransitionFrameFilter* tff = MV_ALLOC_TRANSITION_FILTER(*filterDict, *filter, network);
	return tff;
}


} // namespace rage

#endif // MOVE_MOVE_FUNCS_H
