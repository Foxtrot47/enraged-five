//
// move/move_parameter.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_parameter.h"

#include "move.h"
#include "move_observer.h"
#include "move_subnetwork.h"

#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/framefilters.h"
#include "crclip/clip.h"
#include "crextra/expressions.h"
#include "crmotiontree/node.h"
#include "crparameterizedmotion/parameterizedmotion.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

#if __BANK

#define APPEND(_string, _val, _type) {\
	char buf[32]; \
	sprintf(buf, _type, _val); \
	_string += buf;}

void mvKeyedParameter::Dump(atString& output) const
{
	atHashString tempKey;
	
	tempKey.SetHash(Key);
	const char * name = tempKey.TryGetCStr();
	if (name)
	{
		output += "	"; output += name;
	}
	else
	{
		APPEND(output, Key, "	key=%u");
	}

	output +=" ";

	Parameter.Dump(output);
}

//////////////////////////////////////////////////////////////////////////

void mvParameter::Dump(atString& output) const
{
	switch (GetType())
	{
	case mvParameter::kParameterAnimation: APPEND(output, GetAnimation() ? GetAnimation()->GetName() : "NULL", " Animation: %s"); break;
	case mvParameter::kParameterClip: APPEND(output, GetClip() ? GetClip()->GetName() : "NULL", " Clip: %s"); break;
	case mvParameter::kParameterExpressions: APPEND(output, GetExpressions() ? GetExpressions()->GetName() : "NULL", " Expressions: %s"); break;
	case mvParameter::kParameterFilter: APPEND(output, GetFilter(), " Filter: %p"); break;
	case mvParameter::kParameterFilterN: output += " FilterN:"; break;
	case mvParameter::kParameterFrame: APPEND(output, GetFrame(), " Frame: %p"); break;
	case mvParameter::kParameterPM: APPEND(output, GetPM(), " PM: %p"); break;
	case mvParameter::kParameterBoolean: APPEND(output, GetBool()? "TRUE" : "FALSE", " Bool: %s"); break;
	case mvParameter::kParameterReal: APPEND(output, GetReal(), " Float: %.3f"); break;
	case mvParameter::kParameterInt: APPEND(output, GetInt(), " Int: %d"); break;
	case mvParameter::kParameterData: APPEND(output, GetData(), " Data: %p"); break;
	case mvParameter::kParameterNode: APPEND(output, GetNode(), " Node: %p"); break;
	case mvParameter::kParameterObserver: APPEND(output, GetObserver(), " Observer: %p"); break;
	case mvParameter::kParameterNetwork: APPEND(output, GetNetwork()? GetNetwork()->GetDefinition()->GetDebugName(NULL, 0) : "NULL", " Subnetwork: %s"); break;
	case mvParameter::kParameterReference: APPEND(output, GetData(), " Reference: %p"); break;
	case mvParameter::kParameterProperty: APPEND(output, GetProperty() ? GetProperty()->GetName() : "NULL", " Clip property: %s"); break;
	case mvParameter::kParameterNodeId: output += " NodeId:"; APPEND(output, GetNodeId(), " %u - "); output += atHashString(GetNodeId()).TryGetCStr() ? atHashString(GetNodeId()).TryGetCStr() : "NULL"; break;
	case mvParameter::kParameterNodeName: output += " NodeName:"; APPEND(output, GetNodeId(), " %u - "); output += atHashString(GetNodeId()).TryGetCStr() ? atHashString(GetNodeId()).TryGetCStr() : "NULL"; break;
	case mvParameter::kParameterNone: break;
	case mvParameter::kParameterRequest: APPEND(output, GetRequest()? "TRUE" : "FALSE", " Request: %s"); break;
	case mvParameter::kParameterFlag: APPEND(output, GetFlag()? "TRUE" : "FALSE", " Flag: %s"); break;
	}
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

} // namespace rage
