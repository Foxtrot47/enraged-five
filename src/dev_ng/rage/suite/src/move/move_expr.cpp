//
// move/expressions.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_expr.h"

#include "move_parameterbuffer.h"
#include "move_macros.h"
#include "move_state.h"
#include "move_types.h"
#include "move_types_internal.h"

#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeexpression.h"

namespace rage {

extern mvExpressionLoader g_mvExpressionLoader;

//////////////////////////////////////////////////////////////////////////

unsigned mvNodeExpressionDef::GetExpressionsFlagFrom(u32 flags)
{
	return flags & 0x00000003;
}

unsigned mvNodeExpressionDef::GetWeightFlagFrom(u32 flags)
{
	return (flags & 0x0000000C) >> 0x02;
}

unsigned mvNodeExpressionDef::GetVariableCountFrom(u32 flags)
{
	return (flags & 0xf0000000) >> 0x1c;
}

unsigned mvNodeExpressionDef::GetVariableFlagsFrom(u32 flags)
{
	return (flags & 0x0ffffff0) >> 0x04;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeExpressionDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeExpression* mvNodeExpressionDef::Create(mvCreateContext* context) const
{
	mvNodeExpression* node = mvNodeExpression::CreateNode(*context->m_MotionTree);

	void* block = (void*)(this + 1);
	u32 flags = m_flags;

	unsigned exprFlag = GetExpressionsFlagFrom(flags);
	unsigned weightFlag = GetWeightFlagFrom(flags);

	const mvParameterBuffer* params = context->m_Parameters;
	const mvMotionWeb* network = context->m_Network;

	// expressions
	switch (exprFlag)
	{
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetExpressionsUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterExpressions, &ret);
			const crExpressions* expr = param->GetExpressions();
			node->Init(expr);
		}
		break;
	case mvConstant::kFlagValue:
		{
			u32 expressionDict = ReadUInt32Block();
			u32 expression = ReadUInt32Block();
			const crExpressions* expr = MV_REQUISITION_EXPRESSIONS(expressionDict, expression, network);
			node->Init(expr);
			if(expr != NULL)
			{
				expr->Release();
			}
			break;
		}
	}

	// weight
	switch (weightFlag)
	{
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(1.0F);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			float weight = param->GetReal();
			node->SetWeight(weight);
		}
		break;
	case mvConstant::kFlagValue:
		{
			float weight = ReadFloatBlock();
			node->SetWeight(weight);
		}
		break;
	}

	// variables
	unsigned count = GetVariableCountFrom(flags);
	unsigned varFlags = GetVariableFlagsFrom(flags);

	for (u32 i = 0; i<count; ++i)
	{
		u32 hash = ReadUInt32Block();

		u32 varFlag = (varFlags >> (i*2)) & 0x03;
		switch(varFlag)
		{
		case mvConstant::kFlagSignal:
			{
				u32 id = ReadUInt32Block();
				mvParameter ret; ret.SetReal(0.f);
				const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
				float weight = param->GetReal();
				node->ApplyVariable(hash, weight);
			}
			break;

		case mvConstant::kFlagValue:
			{
				float weight = ReadFloatBlock();
				node->ApplyVariable(hash, weight);
			}
			break;
		}
	}

	mvNodeDef* def = m_input.Get<mvNodeDef>();
	crmtNode* child = def->Create(context);

	node->AddChild(*child);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeExpressionDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_flags, 1);

	m_input.Init(resource);

	void* block = this + 1;
	u32 flags = m_flags;

	unsigned eflag = GetExpressionsFlagFrom(flags);
	unsigned wflag = GetWeightFlagFrom(flags);
	
	MV_VALIDATE(eflag < mvConstant::kFlagCount, "(mvNodeExpressionDef) expression flag out of range");

	// expressions
	MV_EXPRESSIONS_BLOCK_INIT(block, resource, eflag);

	MV_REAL_BLOCK_INIT_FLAG(block, resource, wflag);

	unsigned count = GetVariableCountFrom(flags);
	unsigned vflags = GetVariableFlagsFrom(flags);

	MV_VALIDATE(count <= 12, "(mvNodeExpressionDef) variable count out of range");

	for (u32 i = 0; i < count; ++i)
	{
		unsigned vflag = (vflags >> (i*2)) & 0x03;
		MV_EXPRESSIONS_VAR_BLOCK_INIT(block, resource, vflag);
	}
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeExpressionDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeExpressionDef);

	const void* block = this+1;
	u32 flags = m_flags;

	unsigned eflag = GetExpressionsFlagFrom(flags);
	unsigned wflag = GetWeightFlagFrom(flags);

	// expressions
	MV_EXPRESSIONS_BLOCK_SIZE(block, eflag, size);

	MV_REAL_BLOCK_SIZE_FLAG(block, wflag, size);

	unsigned count = GetVariableCountFrom(flags);
	unsigned vflags = GetVariableFlagsFrom(flags);
	for (u32 i = 0; i < count; ++i)
	{
		unsigned vflag = (vflags >> (i*2)) & 0x03;
		MV_EXPRESSIONS_VAR_BLOCK_SIZE(block, vflag, size);
	}

	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeExpressionDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeExpression::ApplyExpressions),
		MOVE_MAKE_SIGNAL(mvNodeExpression::ApplyWeight),
		MOVE_MAKE_SIGNAL(mvNodeExpression::ApplyVariable)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeExpressionDef::ResolveParameterSetType(u16 parameter)
{
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterExpressions,
		mvParameter::kParameterReal,
		mvParameter::kParameterReal,
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeExpressionDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeExpression::GetExpressions),
		MOVE_MAKE_SIGNAL(mvNodeExpression::GetWeight),
		MOVE_MAKE_SIGNAL(mvNodeExpression::GetVariable)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeExpressionDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeExpression, crmtNodeExpression, mvNode::kNodeExpression, mvNodeExpressionDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
