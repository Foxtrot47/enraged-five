//
// move/move_node.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_NODE_H
#define MOVE_NODE_H

#include "move_config.h"
#include "move_fixedheap.h"
#include "move_parameterbuffer.h"
#include "move_types_internal.h"

#include "atl/array.h"
#include "atl/map.h"
#include "crmotiontree/node.h"

#define MOVE_NODE_H_SAFE

namespace rage {

class crmtMotionTree;
class mvMotionWeb;
class mvNetwork;
class mvParameter;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Context in which to create new nodes from the definition data
// NOTES: This contains data that any instance of a node may be interested
//	in when it's being initialized.
class mvCreateContext
{
public:
	// PURPOSE: Constructor
	mvCreateContext() : m_Children(NULL) {}

	// PURPOSE: Motion tree this network is 'attached' to
	crmtMotionTree* m_MotionTree;

	// PURPOSE: The most immediate parent network to this part of the hierarchy
	mvMotionWeb* m_Network;

	// PURPOSE: Any children of the current container node
	// NOTES: In some cases such as motion tree nodes (nodes in the move
	//	hierarchy that contain a piece of motion tree as opposed to
	//	state nodes or vanilla nodes) observes will be attached
	//	to nodes for parameter updating or some other operation. These
	//	observers reference this list of children to figure out
	//	which node they should be attached to.
	crmtNode** m_Children;

	// PURPOSE: How many many children in the above list
	u32 m_ChildrenCount;

	// PURPOSE: A tail node that a node may want to attach
	// NOTES: Most nodes that have inputs will create their own
	//	through a local offset to the input nodes definition
	//	data. This is used as a helper for inline states that
	//	don't know how to create their input as it's not defined
	//	until runtime. Input in this case will be created before
	//	the inline state machine then plugged into that part of
	//	the tree when that node is created.
	crmtNode* m_Tail;

	// PURPOSE: The network that owns everything
	mvNetwork* m_OwnerNetwork;

	// PURPOSE: The parameter buffer
	// NOTES: On node construction, some nodes will want to reference
	//	the parameter buffer to initialize their values.
	const mvParameterBuffer* m_Parameters;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory base definition for all node definitions
class mvNodeDef
{
public:
	// PURPOSE: Base create call.
	// NOTES: Will use the move type system to figure out what kind of node to actually create.
	crmtNode* Create(mvCreateContext*) const;

	// PURPOSE: Base init call.
	// NOTES: Will use the move type system to figure out what kind of node to init.
	void Init(mvMemoryResource*);

	// PURPOSE: Base query size call.
	// NOTES: Will use the move types system to figure out what kind of node to query.
	u32 QueryMemorySize() const;

public:
	// PURPOSE: Type of this node. Determines which ad-hoc virtual calls to use
	u16 m_type;

	// PURPOSE: The index of this node in the children array
	u16 m_index;

	// PURPOSE: Node's unique id. Generated from the node name in makenetwork
	u32 m_id;
};

//////////////////////////////////////////////////////////////////////////

class mvNode
{
public:
	// PURPOSE: Available node types supported by move
	// NOTE: Values DON'T need to line up with crmtNode::eNodes
	enum
	{
		kNodeNone,			// 0
		
		kNodeStateMachine,		// 1
		kNodeTail,			// 2
		kNodeInlinedStateMachine,  // 3
		kNodeAnimation,		// 4
		kNodeBlend,			// 5
		kNodeAdd,			// 6
		kNodeFilter,		// 7
		kNodeMirror,		// 8
		kNodeFrame,			// 9
		kNodeIK,			// 10
		kNodeRagdoll,		// 11
		kNodeMover,			// 12
		kNodeBlendN,		// 13
		kNodePonyTail,		// 14
		kNodeClip,			// 15
		kNodeStyle,			// 16
		kNodePM,			// 17
		kNodeExtrapolate,	// 18
		kNodeExpression,	// 19
		kNodeCapture,		// 20
		kNodeProxy,			// 21
		kNodeAddN,			// 22
		kNodeIdentity,		// 23
		kNodeMerge,			// 24
		kNodePose,			// 25
		kNodeMergeN,		// 26
		kNodeState,			// 27
		kNodeInvalid,		// 28
		kNodeJointLimit,	// 29
		kNodeSubNetwork,	// 30
		kNodeReference,		// 31

		kNodeCount,

		kNodeCustom = 0x80,
	};

	enum
	{
		kInvalidEvent = 0xffffffff,
	};

	// PURPOSE: Register nodes with the move type system.
	static void InitClass();

	// PURPOSE: Node create function type
	typedef crmtNode* NodeCreateFn(const mvNodeDef*, mvCreateContext*);

	// PURPOSE: Node init function type
	typedef void NodeInitFn(mvNodeDef*, mvMemoryResource*);

	// PURPOSE: Node query function type
	typedef u32 NodeQueryFn(const mvNodeDef*);

	// PURPOSE: Set function type
	typedef void SetParameterFunc(crmtNode*, const mvParameter&, u32);

	// PURPOSE: Resolve set function type
	typedef SetParameterFunc* ResolveParameterSetFn(u16);

	// PURPOSE: Resolve set type function type
	typedef mvParameter::ParamType ResolveParameterSetTypeFn(u16);

	// PURPOSE: Get parameter function type
	typedef void GetParameterFunc(mvParameter*, crmtNode*, u32);

	// PURPOSE: Resolve get parameter function type
	typedef GetParameterFunc* ResolveParameterGetFn(u16);

	// PURPOSE: Resolve event function type
	typedef u32 ResolveEventFn(u16);

	// PURPOSE: Move type system type info
	class NodeTypeInfo
	{
	public:
		// PURPOSE: Constructor
		// NOTES: Calls to this are generated with move type system helper macros
		NodeTypeInfo(u32 nodetype,
			NodeCreateFn* createfn,
			NodeInitFn* initfn,
			NodeQueryFn* queryfn,
			ResolveParameterSetFn* setfn,
			ResolveParameterSetTypeFn* setTypeFn,
			ResolveParameterGetFn* getfn,
			ResolveEventFn* eventfn);

		// PURPOSE: Destructor
		~NodeTypeInfo();

		// PURPOSE: Register this node type info with the type system
		// NOTES: This is called from move type system helper macros
		void Register() const;

		// PURPOSE: Node type id
		// SEE ALSO: Above type enumeration
		u32 m_NodeType;

		// PURPOSE: Function pointer to node's create function
		NodeCreateFn* m_CreateFn;

		// PURPOSE: Function pointer to the node's init function
		NodeInitFn* m_InitFn;

		// PURPOSE: Function pointer to the node's query function
		NodeQueryFn* m_QueryFn;

		// PURPOSE: Function pointer to the nodes resolve parameter set function
		ResolveParameterSetFn* m_ResolveParameterSetFn;

		// PURPOSE: Function pointer to the nodes resolve parameter set type function
		ResolveParameterSetTypeFn* m_ResolveParameterSetTypeFn;

		// PURPOSE: Function pointer to the nodes resolve parameter get function
		ResolveParameterGetFn* m_ResolveParameterGetFn;

		// PURPOSE: Function pointer to the nodes resolve event function
		ResolveEventFn* m_ResolveEventFn;
	};

	// PURPOSE: Declare node type helper.
	// NOTES: Sets up a definition to be registered with the move type system
	#define MOVE_DECLARE_NODE_TYPE(__typename) \
		static __typename* CreateNode(rage::crmtMotionTree& tree);	\
		static void InitClass(); \
		static const rage::mvNode::NodeTypeInfo sm_NodeTypeInfo;

	// PURPOSE: Implement the calls that register this definition to the move type system
	// WARNING: pretty average. calls the base CreateNode to allocate the node
	//	then placement new's over the allocated memory so the correct Shutdown
	//	is called.
	#define MOVE_IMPLEMENT_NODE_TYPE(__typename, __typebase, __typeid, __typedef) \
		__typename* __typename::CreateNode(rage::crmtMotionTree& tree) \
		{ \
			union { __typename* derived; __typebase* base; } u; \
			u.base = __typebase::CreateNode(tree); \
		\
			return u.derived; \
		}	\
		void __typename::InitClass() \
		{ \
			sm_NodeTypeInfo.Register();	\
		}	\
		const rage::mvNode::NodeTypeInfo __typename::sm_NodeTypeInfo(__typeid, &__typedef::CreateThunk, &__typedef::InitThunk, &__typedef::QueryMemorySizeThunk, &__typedef::ResolveParameterSet, &__typedef::ResolveParameterSetType, &__typedef::ResolveParameterGet, &__typedef::ResolveEvent);

	// PURPOSE: Declare node type helper.
	// NOTES: Sets up a definition to be registered with the move type system.
	//	This is a stand alone version of MOVE_DECLARE_REGISTERED_TYPE
	#define MOVE_DECLARE_REGISTERED_CLASS(__typename) \
		class __typename { \
		public: \
			static void InitClass();	\
			static const rage::mvNode::NodeTypeInfo sm_NodeTypeInfo; \
		}

	// PURPOSE: Declare node type helper.
	// NOTES: Sets up a definition to be registered with the move type system.
	//	This is a cut down version of MOVE_DECLARE_NODE_TYPE without CreateNode
	#define MOVE_DECLARE_REGISTERED_TYPE(__typename) \
			static void InitClass();	\
			static const rage::mvNode::NodeTypeInfo sm_NodeTypeInfo; \

	// PURPOSE: Implement the calls that register this definition to the move type system
	#define MOVE_IMPLEMENT_REGISTERED_TYPE(__typename, __typeid, __typedef)	\
		void __typename::InitClass() \
		{ \
			sm_NodeTypeInfo.Register(); \
		} \
		const rage::mvNode::NodeTypeInfo __typename::sm_NodeTypeInfo(__typeid, &__typedef::CreateThunk, &__typedef::InitThunk, &__typedef::QueryMemorySizeThunk, &__typedef::ResolveParameterSet, &__typedef::ResolveParameterSetType, &__typedef::ResolveParameterGet, &__typedef::ResolveEvent);

	// PURPOSE: Helper to declare the support thunks for callbacks in the move type system
	#define MOVE_DECLARE_NODE_DEF(__typedef)	\
		static crmtNode* CreateThunk(const mvNodeDef* base, mvCreateContext* context); \
		static void InitThunk(mvNodeDef* base, mvMemoryResource* resource); \
		static u32 QueryMemorySizeThunk(const mvNodeDef* base);

	// PURPOSE: Implement helper for the callbalck in the move type system
	#define MOVE_IMPLEMENT_NODE_DEF(__typedef)	\
		crmtNode* __typedef::CreateThunk(const mvNodeDef* base, mvCreateContext* context) \
		{ \
			union { const __typedef* derived; const mvNodeDef* base; } u; \
			u.base = base; \
			return u.derived->Create(context); \
		} \
		void __typedef::InitThunk(mvNodeDef* base, mvMemoryResource* resource) \
		{ \
			union { __typedef* derived; mvNodeDef* base; } u; \
			u.base = base; \
			u.derived->Init(resource); \
		} \
		u32 __typedef::QueryMemorySizeThunk(const mvNodeDef* base) \
		{ \
			union { const __typedef* derived; const mvNodeDef* base; } u; \
			u.base = base; \
			return u.derived->QueryMemorySize(); \
		}	

	// PURPOSE: Array of registered node types
	// NOTES: If a node type without an id is missing from this array
	//	it won't be able to be created at runtime form the makenetwork
	//	exported data.
	static atArray<const NodeTypeInfo*> sm_NodeTypeInfos;

private:
	// PURPOSE: Actually register the type info into the above array
	static void RegisterNodeTypeInfo(const NodeTypeInfo& info);
};

//////////////////////////////////////////////////////////////////////////
// PURPOSE: Declare a signal set thunk for animation types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_ANIMATION_SET(__type, __signal)	\
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_val.GetAnimation()); \
	} \
	void __signal(const crAnimation*);

// PURPOSE: Declare a signal get thunk for animation types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_ANIMATION_GET(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetAnimationUnsafe(_this->__signal()); \
	} \
	const crAnimation* __signal() const;

// PURPOSE: Declare a signal set thunk for clip types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_CLIP_SET(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_val.GetClip()); \
	} \
	void __signal(const crClip*);

// PURPOSE: Declare a signal get thunk for clip types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_CLIP_GET(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetClipUnsafe(_this->__signal()); \
	} \
	const crClip* __signal() const;

// PURPOSE: Declare a signal set thunk for expression types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_EXPRESSIONS_SET(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_val.GetExpressions()); \
	} \
	void __signal(const crExpressions*);

// PURPOSE: Declare a signal get thunk for expression types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_EXPRESSIONS_GET(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetExpressionsUnsafe(_this->__signal()); \
	} \
	const crExpressions* __signal() const;

// PURPOSE: Declare a signal set thunk for filter types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_FILTER_SET(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_val.GetFilter()); \
	} \
	void __signal(crFrameFilter*);

// PURPOSE: Declare a signal get thunk for filter types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_FILTER_GET(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetFilterUnsafe(_this->__signal()); \
	} \
	crFrameFilter* __signal() const;

// PURPOSE: Declare a signal set thunk for frame types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_FRAME_SET(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_val.GetFrame()); \
	} \
	void __signal(crFrame*);

// PURPOSE: Declare a signal get thunk for frame types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_FRAME_GET(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetFrameUnsafe(_this->__signal()); \
	} \
	crFrame* __signal() const;

// PURPOSE: Declare a signal set thunk for parameterized motion types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_PM_SET(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_val.GetPM()); \
	} \
	void __signal(crpmParameterizedMotion*);

// PURPOSE: Declare a signal get thunk for parameterized motion types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_PM_GET(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetPMUnsafe(_this->__signal()); \
	} \
	crpmParameterizedMotion* __signal();

// PURPOSE: Declare a signal set thunk for float types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_REAL_SET(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_val.GetReal()); \
	} \
	void __signal(float);

// PURPOSE: Declare a signal get thunk for float types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_REAL_GET(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetReal(_this->__signal()); \
	} \
	float __signal() const;

// PURPOSE: Declare a signal set thunk for float array types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_REAL_SET_INDEXED(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32 _idx) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_idx, _val.GetReal()); \
	} \
	void __signal(u32, float);

// PURPOSE: Declare a signal get thunk for float array types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_REAL_GET_INDEXED(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32 _idx) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetReal(_this->__signal(_idx)); \
	} \
	float __signal(u32) const;

// PURPOSE: Declare a signal set thunk for filter array types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_FILTER_SET_INDEXED(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32 _idx) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_idx, _val.GetFilter()); \
	} \
	void __signal(u32, crFrameFilter*);

// PURPOSE: Declare a signal get thunk for filter array types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_FILTER_GET_INDEXED(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32 _idx) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetFilterUnsafe(_this->__signal(_idx)); \
	} \
	crFrameFilter* __signal(u32) const;

// PURPOSE: Declare a signal set thunk for bool types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_BOOLEAN_SET(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_val.GetBool()); \
	} \
	void __signal(bool);

// PURPOSE: Declare a signal get thunk for bool types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_BOOLEAN_GET(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetBool(_this->__signal()); \
	} \
	bool __signal() const;

// PURPOSE: Declare a signal set thunk for node types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_NODE_SET(__type, __signal) \
	static void __signal##Thunk(crmtNode* _node, const mvParameter& _val, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_this->__signal(_val.GetNode()); \
	} \
	void __signal(crmtNode*);

// PURPOSE: Declare a signal get thunk for node types
// NOTES: This is useful when used with an xmacro, but not required
#define MOVE_NODE_GET(__type, __signal) \
	static void __signal##Thunk(mvParameter* _param, crmtNode* _node, u32) \
	{ \
		__type* _this = reinterpret_cast<__type*>(_node); \
		_param->SetNode(_this->__signal()); \
	} \
	crmtNode* __signal() const;

// PURPOSE: Helper macro to build the thunk name from a signal method name
#define MOVE_MAKE_SIGNAL(__func) &__func##Thunk

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#include "move_parameter.h"

#endif // MOVE_NODE_H
