//
// move/move_capture.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_capture.h"

#include "move_parameterbuffer.h"
#include "move_macros.h"
#include "move_network.h"
#include "move_types.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeCaptureDef);

//////////////////////////////////////////////////////////////////////////

mvNodeCapture* mvNodeCaptureDef::Create(mvCreateContext* context) const
{
	mvNodeCapture* node = mvNodeCapture::CreateNode(*context->m_MotionTree);

	u32 flags = m_flags;
	const void* block = this+1;

	const mvParameterBuffer* params = context->m_Parameters;

	bool allocated = false;
	//bool owner = false;

	// frame
	crFrame* frame = NULL;
	switch (flags & kFrameMask)
	{
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();

			mvInitParameter<crFrame*> ret (NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterFrame, &ret);
			frame = param->GetFrame();
		}
		break;
	case mvConstant::kFlagIgnored:
		{
			// there's no proper ownership of the frame yet if i do this. eg. can't pass this to another node and know it's going to survive and be destroyed properly
			/*
			frame = MV_ALLOC_FRAME(network.Get());
			allocated = true;
			owner = false; // TODO: need to figure out a better way to manage frames, because they aren't ref counted
			*/
		}
		break;
	}

	// frame owner
	if (allocated != true)
	{
		switch ((flags & kFrameOwnerMask) >> kFrameOwnerShift)
		{
		case mvConstant::kFlagValue:
			{
				//owner = ReadBoolBlock();
			}
			break;
		case mvConstant::kFlagSignal:
			{
				/*
				u32 id = ReadUInt32Block();
				mvParameter ret; ret.SetBool(true);
				const mvParameter* param = params->Find(id, &ret);
				owner = param->GetBool();
				*/
			}
			break;
		}
	}

	node->Init(frame);

	mvNodeDef* input = m_input.Get<mvNodeDef>();
	crmtNode* child = input->Create(context);

	node->AddChild(*child);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeCaptureDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_flags, 1);

	m_input.Init(resource);

	void* block = this+1;
	u32 flags = m_flags;

	// frame
	MV_FRAME_BLOCK_INIT(block, resource, flags, kFrameMask);
	// frame owner
	MV_BOOLEAN_BLOCK_INIT(block, resource, flags, kFrameOwnerMask);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeCaptureDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeCaptureDef);

	const void* block = this+1;
	u32 flags = m_flags;

	// frame
	MV_FRAME_BLOCK_SIZE(block, flags, kFrameMask, size);
	// frame owner
	MV_BOOLEAN_BLOCK_SIZE(block, flags, kFrameOwnerMask, size);

	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeCaptureDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeCapture::ApplyFrame)
	};

	Assert(parameter < NELEM(funcs));

	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeCaptureDef::ResolveParameterSetType(u16)
{
	return mvParameter::kParameterFrame;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeCaptureDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeCapture::GetFrame)
	};

	Assert(parameter < NELEM(funcs));

	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeCaptureDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeCapture, crmtNodeCapture, mvNode::kNodeCapture, mvNodeCaptureDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
