//
// move/move_synchronizer.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_SYNCHRONIZER_H
#define MOVE_SYNCHRONIZER_H

#include "crmotiontree/synchronizer.h"

#include "move_fixedheap.h"

namespace rage {

class crmtNodeAddSubtract;
class crmtNodeBlend;
class crmtNodeMerge;
class crmtNodeAddN;
class crmtNodeN;
class crmtSynchronizer;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Container for static synchronizer attach calls
class mvSynchronizer
{
public:
	enum { kSyncInvalid = 255 };

	// PURPOSE: Attach a synchronizer to an add node
	// PARAMS:
	//	node - node to attach to
	//	type - type of synchronizer (mvConstant::kSynchronizerPhase or mvConstant::kSynchronizerTag)
	//	tag	- if type is kSynchronizerTag, the tag to synchronize on
	//	transitional - is this a transitional synchronizer
	//	immutableMask - is this synchronizer immutable
	static void AttachToAdd(crmtNodeAddSubtract& node, u32 type, u32 tag, u32 immutableMask);

	// PURPOSE: Attach a synchronizer to an blend node
	// PARAMS:
	//	node - node to attach to
	//	type - type of synchronizer (mvConstant::kSynchronizerPhase or mvConstant::kSynchronizerTag)
	//	tag	- if type is kSynchronizerTag, the tag to synchronize on
	//	transitional - is this a transitional synchronizer
	//	immutableMask - is this synchronizer immutable
	static void AttachToBlend(crmtNodeBlend& node, u32 type, u32 tag, bool transitional, u32 immutableMask);

	// PURPOSE: Attach a synchronizer to an merge node
	// PARAMS:
	//	node - node to attach to
	//	type - type of synchronizer (mvConstant::kSynchronizerPhase or mvConstant::kSynchronizerTag)
	//	tag	- if type is kSynchronizerTag, the tag to synchronize on
	//	transitional - is this a transitional synchronizer
	//	immutableMask - is this synchronizer immutable
	static void AttachToMerge(crmtNodeMerge& node, u32 type, u32 tag, u32 immutableMask);

	// PURPOSE: Attach a synchronizer to an n-way add node
	// PARAMS:
	//	node - node to attach to
	//	type - type of synchronizer (mvConstant::kSynchronizerPhase or mvConstant::kSynchronizerTag)
	//	tag	- if type is kSynchronizerTag, the tag to synchronize on
	//	immutableMask - is this synchronizer immutable
	static void AttachToNwayAdd(crmtNodeAddN& node, u32 type, u32 tag, u32 immutableMask);

	// PURPOSE: Attach a synchronizer to an n-way node
	// PARAMS:
	//	node - node to attach to
	//	type - type of synchronizer (mvConstant::kSynchronizerPhase or mvConstant::kSynchronizerTag)
	//	tag	- if type is kSynchronizerTag, the tag to synchronize on
	//	immutableMask - is this synchronizer immutable
	static void AttachToNway(crmtNodeN& node, u32 type, u32 tag, u32 immutableMask);
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Move phase based synchronization
// NOTES: Move creates its own types for synchronizers so that it can control
//	the way they attached in the motion tree.
class mvSynchronizerPhase : public crmtSynchronizerPhase
{
protected:
	// PURPOSE: Constructor
	// PARAMS:
	//	transition - enabled transitional synchronization
	//	immutableMask - is the synchronizer immutable
	mvSynchronizerPhase(bool transitional, u32 immutableMask)
		: m_Synchronized (false)
		, m_Transitional (transitional)
		, m_ImmutableMask (immutableMask) { Assert(!immutableMask); }

	// PURPOSE: Set synchronized flag
	void SetSynchronized()
	{
		m_Synchronized = true;
	}

	// PURPOSE: Check synchronized flag
	bool IsSynchronized() const
	{
		return m_Synchronized;
	}

	// PURPOSE: Is this a transitional synchronizer
	bool IsTransitional() const
	{
		return m_Transitional;
	}

	// PURPOSE: Is this synchronizer immutable
	u32 GetImmutableMask() const
	{
		return m_ImmutableMask;
	}

	// PURPOSE: Synchronize
	virtual void Synchronize(float deltaTime)
	{
		crmtSynchronizerPhase::Synchronize(deltaTime);
		SetSynchronized();
	}

protected:

	// PURPOSE: Override sync observer so it can be allocated from the move pool
	class mvSyncObserver : public SyncObserver
	{
	public:
		// PURPOSE: Constructor
		// PARAMS:
		//	delay - delay this element from participating in the weight calculation until it's caught up with other synchronized elements
		//	autoUpdateWeight - automatically calculate this elements contribution to synchronization
		//	immutable - don't override the source for this element that's participating in synchronization
		mvSyncObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp)
			: SyncObserver(delay, autoUpdateWeight, immutable, warp)  { }

		// PURPOSE: We allocate from move's buffer
		MOVE_DECLARE_ALLOCATOR();
	};

	// PURPOSE: Create the correct observer type
	virtual SyncObserver* CreateSyncObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp)
	{
		return rage_new mvSyncObserver(delay, autoUpdateWeight, immutable, warp);
	}

private:
	// PURPOSE: Immutable synchronizer
	u32 m_ImmutableMask;

	// PURPOSE: Synchronized flag.
	// NOTES: Used when figuring out if newly attached delayed observer should participate.
	bool m_Synchronized;

	// PURPOSE: Transitional synchronizer
	bool m_Transitional;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Move tag based synchronization
// NOTES: Move creates its own types for synchronizers so that it can control
//	the way they attached in the motion tree.
class mvSynchronizerTag : public crmtSynchronizerTag
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	tag - tag to synchronize on
	//	transition - enabled transitional synchronization
	//	immutable - is the synchronizer immutable
	mvSynchronizerTag(u32 tag, bool transitional, u32 immutableMask);

	// PURPOSE: Register some useful static properties
	static void InitClass();

	// PURPOSE: Set synchronized flag
	void SetSynchronized()
	{
		m_Synchronized = true;
	}

	// PURPOSE: Check synchronized flag
	bool IsSynchronized() const
	{
		return m_Synchronized;
	}

	// PURPOSE: Is this a transitional synchronizer
	bool IsTransitional() const
	{
		return m_Transitional;
	}

	// PURPOSE: Is this synchronizer immutable
	u32 GetImmutableMask() const
	{
		return m_ImmutableMask;
	}

	// PURPOSE: Synchronize
	virtual void Synchronize(float deltaTime)
	{
		crmtSynchronizerTag::Synchronize(deltaTime);
		SetSynchronized();
	}

protected:
	// PURPOSE: Override sync observer so it can be allocated from the move pool
	class mvSyncTagObserver : public SyncTagObserver
	{
	public:
		// PURPOSE: Constructor
		// PARAMS:
		//	delay - delay this element from participating in the weight calculation until it's caught up with other synchronized elements
		//	autoUpdateWeight - automatically calculate this elements contribution to synchronization
		//	immutable - don't override the source for this element that's participating in synchronization
		mvSyncTagObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp)
			: SyncTagObserver(delay, autoUpdateWeight, immutable, warp)  { }

		// PURPOSE: We allocate from move's buffer
		MOVE_DECLARE_ALLOCATOR();
	};

	// PURPOSE: Create the correct observer type
	virtual SyncObserver* CreateSyncObserver(bool delay, bool autoUpdateWeight, bool immutable, bool warp)
	{
		return rage_new mvSyncTagObserver(delay, autoUpdateWeight, immutable, warp);
	}

private:
	// PURPOSE: Static property
	// SEE ALSO: InitClass
	static crProperty sm_LeftFootHeel;

	// PURPOSE: Static property
	// SEE ALSO: InitClass
	static crProperty sm_RightFootHeel;

	// PURPOSE: Immutable synchronizer
	u32 m_ImmutableMask;

	// PURPOSE: Synchronized flag
	// NOTES: Used when figuring out if newly attached delayed observer should participate.
	bool m_Synchronized;

	// PURPOSE: Transitional synchronizer
	bool m_Transitional;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Phase based synchronization for add nodes
class mvSynchronizerPhaseAdd : public mvSynchronizerPhase
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	immutableMask - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerPhaseAdd(u32 immutableMask) : mvSynchronizerPhase(false, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerPhaseAdd() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Tag based synchronization for add nodes
class mvSynchronizerTagAdd : public mvSynchronizerTag
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	tag - tag to use in synchronization calculation
	//	immutable - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerTagAdd(u32 tag, u32 immutableMask) : mvSynchronizerTag(tag, false, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerTagAdd() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Phase based synchronization for n-way add nodes
class mvSynchronizerPhaseNwayAdd : public mvSynchronizerPhase
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	transition - enable transitional synchronization
	//	immutableMask - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerPhaseNwayAdd(u32 immutableMask) : mvSynchronizerPhase(false, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerPhaseNwayAdd() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Tag based synchronization for n-way add nodes
class mvSynchronizerTagNwayAdd : public mvSynchronizerTag
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	tag - tag to use in synchronization calculation
	//	immutableMask - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerTagNwayAdd(u32 tag, u32 immutableMask) : mvSynchronizerTag(tag, false, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerTagNwayAdd() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Phase based synchronization for blend nodes
class mvSynchronizerPhaseBlend : public mvSynchronizerPhase
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	transition - enable transitional synchronization
	//	immutableMask - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerPhaseBlend(bool transitional, u32 immutableMask) : mvSynchronizerPhase(transitional, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerPhaseBlend() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Tag based synchronization for blend nodes
class mvSynchronizerTagBlend : public mvSynchronizerTag
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	tag - tag to use in synchronization calculation
	//	transition - enable transitional synchronization
	//	immutableMask - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerTagBlend(u32 tag, bool transitional, u32 immutableMask) : mvSynchronizerTag(tag, transitional, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerTagBlend() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Phased based synchronization for merge nodes
class mvSynchronizerPhaseMerge : public mvSynchronizerPhase
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	immutableMask - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerPhaseMerge(u32 immutableMask) : mvSynchronizerPhase(false, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerPhaseMerge() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Tag based synchronization for merge nodes
class mvSynchronizerTagMerge : public mvSynchronizerTag
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	tag - tag to use in synchronization calculation
	//	immutable - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerTagMerge(u32 tag, u32 immutableMask) : mvSynchronizerTag(tag, false, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerTagMerge() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Phase based synchronization for n-way nodes
class mvSynchronizerPhaseNway : public mvSynchronizerPhase
{
public:
	// PURPOSE: Constructor
	//	immutableMask - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerPhaseNway(u32 immutableMask) : mvSynchronizerPhase(false, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerPhaseNway() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Tag based synchronization for n-way nodes
class mvSynchronizerTagNway : public mvSynchronizerTag
{
public:
	// PURPOSE: Constructor
	// PARAMS:
	//	tag - tag to use in synchronization calculation
	//	immutableMask - is the synchronizer immutable. Node will participate in synchronization but will not be scrubbed as a result.
	mvSynchronizerTagNway(u32 tag, u32 immutableMask) : mvSynchronizerTag(tag, false, immutableMask) {}

	// PURPOSE: Destructor
	virtual ~mvSynchronizerTagNway() {}

	// PURPOSE: Traverse the nodes lower in the tree, adding a sync observer for any that can participate in synchronization
	virtual void AddTargets();

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

} // namespace rage

#endif // MOVE_SYNCHRONIZER_H
