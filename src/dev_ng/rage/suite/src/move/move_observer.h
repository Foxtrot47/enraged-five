//
// move/observer.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_OBSERVER_H
#define MOVE_OBSERVER_H

#include "move_node.h"
#include "move_statedef.h"

#include "crmotiontree/observer.h"

#define MOVE_OBSERVER_H_SAFE

namespace rage {

class mvNodeStateMachine;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Base observer type for move
class mvObserver : public crmtObserver
{
public:
	// PURPOSE: Constructor
	// PARAMS: 'detachOnRequest' - Observer with ::Detach when kMsgDetachUpdaters is received
	mvObserver(bool detachOnRequest);

	// PURPOSE: Destructor
	~mvObserver();

	// PURPOSE: Initialize observer
	// NOTES: Observers in move hold a reference on the network of the nodes they are observing
	void Init(mvMotionWeb* web);

	// PURPOSE: Release reference on network
	virtual void Shutdown();

	// PURPOSE: Handle detach on kMsgDetachUpdaters
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: Our message
	enum eMessages {
		kMsgDetachUpdaters = CRMT_PACK_MSG_ID(0, (crmtNode::kNodeCustom + 0x80)),
	};

protected:
	// PURPOSE: network we're holding a reference to
	mvMotionWeb* m_Web;

	// PURPOSE: flag if we detach on request
	bool m_DetachOnRequest;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Set a parameter on a node from the parameter buffer
// NOTES: If there are multiple parameters on a node that can be
//	set from the parameter buffer, multiple different instances
//	of this observer will exist.
class mvSetFromParameterBuffer : public mvObserver
{
public:
	// PURPOSE: Constructor
	mvSetFromParameterBuffer();

	// PURPOSE: Initialize observer
	// PARAMS:
	// 'web'		- network in which the node this observer is attached to exists (for reference counting network)
	// 'parameter'	- the parameter to lookup in the parameter buffer
	// 'func'		- function pointer to the set function on the node
	// 'idx'		- index of the parameter. Used for array parameters
	// 'type'		- type of the parameter that's being set
	// NOTES:
	//	idx should be set to 0 on non-array parameters. This data should come from the network def data exported from
	//	makenetwork. type is important because lookups in the parameter buffer are type specific. Mutliple parameters
	//	with the same name (or a crc clash) can safely live in the parameter buffer if they have a different type.
	void Init(mvMotionWeb* web, u32 parameter, mvNode::SetParameterFunc* func, u32 idx, mvParameter::ParamType type);

	// PURPOSE: Register for kMsgUpdate and kMsgDetachUpdaters messages
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

	// PURPOSE: Handle kMsgUpdate to set value on motion tree node from parameter buffer
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
private:
	// PURPOSE: Function pointer to set the value on the node
	mvNode::SetParameterFunc* m_SetParameterFn;

	// PURPOSE: The parameter to lookup in the parameter buffer
	u32 m_Parameter;

	// PURPOSE: The index of the parameter. Used for array parameters
	u32 m_Index;

	// PURPOSE: Type of the parameter that's being set
	mvParameter::ParamType m_Type;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Update the parameter buffer from a node's value after it's just been updated
// NOTES: If there are multiple parameter os a node that be stored in the internal parameter
//	buffer, multiple difference instances of this observer will exist.
class mvUpdateParameterBuffer : public mvObserver
{
public:
	// PURPOSE: Constructor
	mvUpdateParameterBuffer();

	// PURPOSE: Initialize observer
	// PARAMS:
	// 'web'		- network in which the node this observer is attached to exists (for reference counting the network)
	// 'parameter'	- the parameter to set in the parameter buffer
	// 'func'		- function pointer to the get function on the node
	// 'idx'		- index of the parameter. Used for array parameters
	// NOTES:
	//	idx should be set to 0 on non-array parameters. This data should come from the network def data exported from
	//	makenetwork. type hasn't been supported as we've not had an occurance of multiple parameters clashing yet.
	void Init(mvMotionWeb* web, u32 parameter, mvNode::GetParameterFunc* func, u32 idx);

	// PURPOSE: Register for kMsgPostUpdate and kMsgDetachUpdaters messages
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

	// PURPOSE: Handle kMsgPostUpdate to get the value from a motion tree node and push it into the internal parameter buffer
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
private:
	// PURPOSE: Function pointer to get the value from the node
	mvNode::GetParameterFunc* m_GetParameterFn;

	// PURPOSE: The parameter to insert the value into the parameter buffer with
	u32 m_Parameter;

	// PURPOSE: The index of the parameter. Used for array parameters
	u32 m_Index;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Update the parameter buffer with a reference to this observer that can be used to proxy
//	a node in the motion tree.
// NOTES: It's assumed that this observer is observing the node that is to be proxied.
class mvUpdateProxyParameter : public mvObserver
{
public:
	// PURPOSE: Constructor
	mvUpdateProxyParameter();

	// PURPOSE: Initialize observer
	// PARAMS:
	// 'web'		- network in which the node this observer is attached to exists (for reference counting the network)
	// 'parameter'	- the parameter to set in the parameter buffer
	void Init(mvMotionWeb* web, u32 parameter);

	// PURPOSE: Register for kMsgPostUpdate message
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

	// PURPOSE: Handle kMsgPostUpdate to push this observer into the parameter buffer
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
private:
	// PURPOSE: The parameter to insert the observer into the parameter buffer with
	u32 m_Parameter;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Update the parameter buffer with the result of a mathematical operation
// NOTES:
//	This observer will be attached to the parent node in which the operator nodes reside. The series of operations
//	are treated as a stack based machine.
class mvUpdateParameterFromOperation : public mvObserver
{
public:
	// PURPOSE: Constructor
	mvUpdateParameterFromOperation();

	// PURPOSE: Initialize observer
	// PARAMS:
	// 'web'		- network in which the node this observer is attached to exists (for reference counting the network)
	// 'operation'	- an array of operators representing the series of operations that this observer will execute
	// 'size'		- the desired stack size to use. If this isn't big enough you'll trash the real stack
	// 'index'		- index of the parameter. Used for array parameters
	// 'func'		- function pointer of the set function on the node
	// NOTES:
	//	There's no parameter as this is baked into the series of operations that are executed as a 'set' operation. Stack size
	//	should be calculated and set by makenetwork but currently it's set arbitrarily to handle the current range of operations.
	//	If this stack isn't long enough the space allocated with alloca will be overrun and trash the stack. index should be 0
	//	for non-array parameters.
	void Init(mvMotionWeb* web, const mvNodeStateDef::Operator* operation, u32 size, u32 index, mvNode::SetParameterFunc* func);

	// PURPOSE: Register for kMsgUpdate and kMsgDetachUpdaters messages
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

	// PURPOSE: Handle kMgUpdate to execute the series of operations
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: Helper struct for dealing with function curves data
	struct FCurve
	{
		struct Range
		{
			float Minimum;
			float Maximum;
		} ClampRange;

		u32 KeyframeCount;

		struct Keyframe
		{
			u32 Type;
			float Key;
			float Constants[2];
		};

		Offset Keyframes;
	};

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
private:
	// PURPOSE: Array of operators representing the series of operations to execute
	const mvNodeStateDef::Operator* m_Operation;

	// PURPOSE: Function pointer to the set function on the node
	mvNode::SetParameterFunc* m_SetParameterFn;

	// PURPOSE: Stack size to allocate for operation calculation. This needs to be properly calculated on export
	u16 m_StackSize;

	// PURPOSE: Index of the parameter. Used for array parameters.
	u16 m_Index;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Update the parameter buffer when an interesting event happens on a node
class mvUpdateParameterFromEvent : public mvObserver
{
public:
	// PURPOSE: Supported event data types
	enum eType {
		kTypeNone,
		kTypeFloat,
		kTypeInt,
		kTypeBool,
		kTypeHashString,

		kTypeNum,
	};

	// PUPROSE: Constructor
	// PARAMS: 'msg' - the message we want to register this observer to pickup
	mvUpdateParameterFromEvent(u32 msg);

	// PURPOSE: Initialize observer
	// PARAMS:
	// 'web'		- network in which the node this observer is attached to exists (for reference counting the network)
	// 'eventId'	- event id that will be used as the parameter value when this event is pushed into the parameter buffer
	void Init(mvMotionWeb* web, u32 eventId);

	// PURPOSE: Register to receive event 'msg'
	// SEE ALSO: mvUpdateParameterFromEvent::mvUpdateParameterFromEvent
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

	// PURPOSE: Handle 'msg', inserting eventId into the parameter buffer
	// SEE ALSO: ResetMessageIntercepts
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
private:
	// PURPOSE: Event id that will be used as the parameter value with this event is pushed into the parameter buffer
	u32 m_Event;

	// PURPOSE: Message that this observer is registered to receive
	u32 m_Msg;
};

//////////////////////////////////////////////////////////////////////////

#if NEW_MOVE_TAGS
// PURPOSE: Update parameter buffer with any tags triggered on the node
class mvUpdateParameterFromTag : public mvObserver
{
public:

	// PURPOSE: Constructor
	mvUpdateParameterFromTag(u32 enterTagMsgId, u32 exitTagMsgId, u32 updateTagMsgId);

	// PURPOSE: Initializer
	void Init(mvMotionWeb* web);

	// PURPOSE: Register to receive event 'msg'
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

	// PURPOSE: Handle 'msg'
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: Allocate from move buffer
	MOVE_DECLARE_ALLOCATOR();

protected:

	// PURPOSE: Is node relevant to parent state
	bool IsRelevantToState();


private:

	u32 m_EnterTagMsgId;
	u32 m_ExitTagMsgId;
	u32 m_UpdateTagMsgId;

};
#endif // NEW_MOVE_TAGS

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Watch for when a node shuts down so we can insert an exit id into the parameter buffer
// NOTES: This watches for when the node is 'kMsgShuttingDown' so it fires before the node
//	is fully transitioned out if there is a blend between transitions.
class mvExitParameter : public mvObserver
{
public:
	// PURPOSE: Constructor
	mvExitParameter();

	// PURPOSE: Initialize observer
	// PARAMS:
	// 'web'		- network in which the node this observer is attached to exists (for reference counting the network)
	// 'onExitId'	- the identifier to use when inserting the event into the parameter buffer (value will be true)
	void Init(mvMotionWeb* web, u32 onExitId);

	// PURPOSE: Register for kMsgShuttingDown message
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

	// PURPOSE: Handle kMsgShuttingDown. Insert m_OnExitId into the parameter buffer
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();

private:
	// PURPOSE: The identifier to use when inserting the event into the parameter buffer
	u32 m_OnExitId;
};

//////////////////////////////////////////////////////////////////////////

#if MOVE_CONCURRENT_TRANSITION_LIMIT
// PURPOSE: Watch for when a node transition completes, so active transitions can be tracked within state
class mvActiveTransition : public mvObserver
{
public:

	// PURPOSE: Constructor
	mvActiveTransition();

	// PURPOSE: Initialize observer
	// PARAMS:
	// web - network in which the node this observer is attached to exists (for reference counting the network)
	// nodeState - state node transition owned by
	void Init(mvMotionWeb* web, mvNodeStateMachine& nodeState);

	// PURPOSE: Register for kMsgShuttingDown message
	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor);

	// PURPOSE: Handle kMsgShuttingDown. Insert m_OnExitId into the parameter buffer
	virtual void ReceiveMessage(const crmtMessage& msg);

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();

private:

	// PURPOSE: Remove active transition from state node and list
	void RemoveActiveTransition();

	mvActiveTransition* m_PrevTransition;
	mvActiveTransition* m_NextTransition;

	mvNodeStateMachine* m_NodeState;

	friend class mvNodeStateMachine;
};
#endif // MOVE_CONCURRENT_TRANSITION_LIMIT

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#include "move_parameter.h"

#endif // MOVE_OBSERVER_H
