//
// move/move_jointlimit.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_jointlimit.h"

#include "move_parameterbuffer.h"
#include "move_funcs.h"
#include "move_macros.h"
#include "move_state.h"
#include "move_types.h"
#include "move_types_internal.h"

#include "cranimation/framefilters.h"
#include "crmotiontree/motiontree.h"
#include "crskeleton/jointdata.h"

namespace rage {

extern mvFilterAllocator g_mvFilterAllocator;

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeJointLimitDef);

//////////////////////////////////////////////////////////////////////////

mvNodeJointLimit* mvNodeJointLimitDef::Create(mvCreateContext* context) const
{
	mvNodeJointLimit* node = mvNodeJointLimit::CreateNode(*context->m_MotionTree);

	u32 flags = m_flags;
	const void* block = this+1;

	const mvParameterBuffer* params = context->m_Parameters;
	const mvMotionWeb* network = context->m_Network;

	// filter
	crFrameFilter* filter = NULL;
	switch (flags & FLAG_MASK)
	{
	case mvConstant::kFlagValue:
		{
			filter = mvAllocAndInitFilter(network, block);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetFilterUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterFilter, &ret);
			filter = param->GetFilter();
		}
		break;
	}

	crJointData jointData;
	Assert(false); // Needs parameters that can be passed through

	node->Init(jointData, filter);

	mvNodeDef* input = m_input.Get<mvNodeDef>();
	crmtNode* child = input->Create(context);

	node->AddChild(*child);

	return node;
}

//////////////////////////////////////////////////////////////////////////

void mvNodeJointLimitDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_flags, 1);

	m_input.Init(resource);

	void* block = this + 1;
	u32 flags = m_flags;

	//filter
	MV_FILTER_BLOCK_INIT(block, resource, flags & kFilterMask);
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeJointLimitDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeJointLimitDef);

	const void* block = this+1;
	u32 flags = m_flags;

	// filter
	MV_FILTER_BLOCK_SIZE(block, (flags & kFilterMask), size);

	return size;
}

//////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeJointLimitDef::ResolveParameterSet(u16)
{
	return MOVE_MAKE_SIGNAL(mvNodeJointLimit::ApplyFilter);
}

//////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeJointLimitDef::ResolveParameterSetType(u16)
{
	return mvParameter::kParameterFilter;
}

//////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeJointLimitDef::ResolveParameterGet(u16)
{
	return NULL;
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeJointLimitDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeJointLimit, crmtNodeJointLimit, mvNode::kNodeJointLimit, mvNodeJointLimitDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
