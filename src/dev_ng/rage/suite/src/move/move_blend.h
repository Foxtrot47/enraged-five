//
// move/move_blend.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_BLEND_H
#define MOVE_BLEND_H

#include "move_node.h"

#include "crmotiontree/nodeblend.h"


namespace rage {

class crFrameFilter;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition base for pair-wise nodes
class mvNodePairDef : public mvNodeDef
{
protected:
	// PURPOSE: Base create instance of this pair-wise node definition
	// PARAMS:
	//	node - this pair-wise node as allocated by the derived def
	//	context - information about this nodes position in the motion tree/network hierarchy
	void Create(crmtNodePairWeighted* node, mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory resource
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize(u32 size) const;

protected:
	static unsigned GetWeightFlagFrom(u32 flags);
	static unsigned GetFilterFlagFrom(u32 flags);
	static unsigned GetTransitionalFlagFrom(u32 flags);
	static unsigned GetImmutableFlagFrom(u32 flags);
	static unsigned GetChild0InfluenceOverrideFlagFrom(u32 flags);
	static unsigned GetChild1InfluenceOverrideFlagFrom(u32 flags);
	static unsigned GetSynchronizerFrom(u32 flags);
	static unsigned GetMergeBlendFrom(u32 flags);

	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES: Flag layout is:
	// Weight - flags & 0x00000003
	// Filter - (flags & 0x0000000c) >> 0x02
	// Transitional - (flags & 0x00000040) >> 0x06
	// Immutable - (flags & 0x00000180) >> 0x07
	// Child 0 influence - (flags & 0x00003000) >> 0x0c
	// Child 1 influence - (flags & 0x0000c000) >> 0x0e
	// Synchronizer - (flags & 0x00180000) >> 0x13
	// MergeBlend - (flags & 0x80000000) >> 0x1f
	u32 m_Flags;

	// PURPOSE: Offset to nodes input definitions
	Offset m_Inputs[2];
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create a blend node
class mvNodeBlendDef : public mvNodePairDef
{
public:
	// PURPOSE: Create an instance of this blend node definition
	// PARAMS: context - information about his nodes position in the motion tree/network hierarchy
	class mvNodeBlend* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Nothing!
	// NOTES: Doesn't look like this is used anymore
	enum Parameter
	{
		kParameterFilter = 0,
		kParameterWeight = 1,

		kParameterCount,
	};

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 type);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeBlendDef);
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move blend node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeBlend : public crmtNodeBlend
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_FILTER_SET(T, ApplyFilter) \
		MOVE_FILTER_GET(T, GetFilter) \
		MOVE_REAL_SET(T, ApplyWeight) \
		MOVE_REAL_GET(T, GetWeight)

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeBlend)

	// PURPOSE: Don't polute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system	
	MOVE_DECLARE_NODE_TYPE(mvNodeBlend);
};

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeBlend::ApplyFilter(crFrameFilter* filter)
{
	crmtNodeBlend::SetFilter(filter);
}

///////////////////////////////////////////////////////////////////////////////

inline crFrameFilter* mvNodeBlend::GetFilter() const
{
	return crmtNodeBlend::GetFilter();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeBlend::ApplyWeight(float weight)
{
	crmtNodeBlend::SetWeight(weight);
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeBlend::GetWeight() const
{
	return crmtNodeBlend::GetWeight();
}

///////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_BLEND_H
