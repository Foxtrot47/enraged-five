//
// move/extrapolate.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_extrap.h"

#include "move_parameterbuffer.h"
#include "move_macros.h"
#include "move_state.h"
#include "move_types_internal.h"

#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeextrapolate.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeExtrapolateDef);

//////////////////////////////////////////////////////////////////////////

mvNodeExtrapolate* mvNodeExtrapolateDef::Create(mvCreateContext* context) const
{
	mvNodeExtrapolate* node = mvNodeExtrapolate::CreateNode(*context->m_MotionTree);

	u32 flags = m_flags;
	void* block = (void*)(this + 1);

	const mvParameterBuffer* params = context->m_Parameters;

	// damping
	float damping = 1.f;
	switch (GetDampingFlagFrom(flags))
	{
	case mvConstant::kFlagValue:
		{
			damping = ReadFloatBlock();
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(damping);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			float p = param->GetReal();
			damping = p != 0.f ? p : 1.f;
		}
		break;
	}

	node->Init(damping);

	mvNodeDef* input = m_Input.Get<mvNodeDef>();
	crmtNode* child = input->Create(context);

	node->AddChild(*child);

	return node;
}

//////////////////////////////////////////////////////////////////////////

void mvNodeExtrapolateDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_flags, 1);

	m_Input.Init(resource);

	void* block = this + 1;
	u32 flags = m_flags;

	MV_REAL_BLOCK_INIT_FLAG(block, resource, GetDampingFlagFrom(flags));
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeExtrapolateDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeExtrapolateDef);

	const void* block = this+1;
	u32 flags = m_flags;

	MV_REAL_BLOCK_SIZE_FLAG(block, GetDampingFlagFrom(flags), size);

	return size;
}

//////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeExtrapolateDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeExtrapolate::ApplyDamping),
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

//////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeExtrapolateDef::ResolveParameterSetType(u16 parameter)
{
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterReal,
		mvParameter::kParameterFrame,
		mvParameter::kParameterBoolean,
		mvParameter::kParameterFrame,
		mvParameter::kParameterBoolean,
		mvParameter::kParameterReal,
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

//////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeExtrapolateDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeExtrapolate::GetDamping),
		MOVE_MAKE_SIGNAL(mvNodeExtrapolate::GetLastFrame),
		MOVE_MAKE_SIGNAL(mvNodeExtrapolate::GetDeltaFrame)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeExtrapolateDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeExtrapolate, crmtNodeExtrapolate, mvNode::kNodeExtrapolate, mvNodeExtrapolateDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
