//
// move/move_expr.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_EXPR_H
#define MOVE_EXPR_H

#include "move_node.h"

#include "crmotiontree/nodeexpression.h"

namespace rage {

class crExpressions;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create an expression node
class mvNodeExpressionDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this expression node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeExpression* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeExpressionDef);

private:
	static unsigned GetExpressionsFlagFrom(u32 flags);
	static unsigned GetWeightFlagFrom(u32 flags);
	static unsigned GetVariableCountFrom(u32 flags);
	static unsigned GetVariableFlagsFrom(u32 flags);

	// PURPOSE: flags define how to read the definition data that hangs of this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES:
	// Expressions - flags & 0x00000003
	// Weight - (flags & 0x0000000C) >> 0x02
	u32 m_flags;

	// PURPOSE: Offset to input nodes definition
	Offset m_input;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move expression node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeExpression : public crmtNodeExpression
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_EXPRESSIONS_SET(T, ApplyExpressions) \
		MOVE_EXPRESSIONS_GET(T, GetExpressions) \
		MOVE_REAL_SET(T, ApplyWeight) \
		MOVE_REAL_GET(T, GetWeight) \
		MOVE_REAL_SET_INDEXED(T, ApplyVariable) \
		MOVE_REAL_GET_INDEXED(T, GetVariable)

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeExpression)


	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeExpression);
};

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeExpression::ApplyExpressions(const crExpressions* expr)
{
	crExpressionPlayer& player = GetExpressionPlayer();
	player.SetExpressions(expr);
}

///////////////////////////////////////////////////////////////////////////////

inline const crExpressions* mvNodeExpression::GetExpressions() const
{
	const crExpressionPlayer& player = GetExpressionPlayer();
	return player.GetExpressions();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeExpression::ApplyWeight(float weight)
{
	SetWeight(weight);
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeExpression::GetWeight() const
{
	return crmtNodeExpression::GetWeight();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeExpression::ApplyVariable(u32 hash, float val)
{
	crmtNodeExpression::GetExpressionPlayer().SetVariable(hash, Vec4V(val, val, val, val));
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeExpression::GetVariable(u32 hash) const
{
	Vec4V ret(V_ZERO);
	crmtNodeExpression::GetExpressionPlayer().GetVariable(hash, ret);
	return ret.GetXf();
}

///////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_EXPR_H
