//
// move/move_identity.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_IDENTITY_H
#define MOVE_IDENTITY_H

#include "move_node.h"

#include "crmotiontree/nodeidentity.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory defintion of how to create an identity node
class mvNodeIdentityDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this identity node definition
	// PARAMS: cotext - information about this nodes position in the motion tree/network hierarchy
	class mvNodeIdentity* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeIdentityDef);
private:
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move identity node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeIdentity : public crmtNodeIdentity
{
public:
	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeIdentity);
};

} // namespace rage

#endif // MOVE_IDENTITY_H
