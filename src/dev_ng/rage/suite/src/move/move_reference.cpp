//
// move/move_reference.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "move_reference.h"

#include "move_statedef.h"
#include "move_types.h"

#include "crmotiontree/nodeinvalid.h"

namespace rage {

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeReferenceDef);

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_REGISTERED_TYPE(mvNodeReferenceClass, mvNode::kNodeReference, mvNodeReferenceDef);

//////////////////////////////////////////////////////////////////////////

crmtNode* mvNodeReferenceDef::Create(mvCreateContext* context) const
{
	mvNodeReference* node = mvNodeReference::CreateNode(*context->m_MotionTree);
	node->Init(context->m_OwnerNetwork, context->m_Network, this);

	return node;
}

//////////////////////////////////////////////////////////////////////////

void mvNodeReferenceDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_NetworkId, 1);
	m_SourceParameterData.Init(resource);
	resource->ReadInt(&m_SourceParameterCount, 1);
	resource->ReadInt(&m_ParameterCount, 1);
	resource->ReadInt(&m_FlagCount, 1);
	resource->ReadInt(&m_RequestCount, 1);
#if MV_ENABLE_REFERENCE_OUTPUT_SIGNALS
	m_OutputParameterData.Init(resource);
	resource->ReadInt(&m_OutputParameterCount, 1);
#endif

	u32* block = reinterpret_cast<u32*>(this+1);
	for (u32 i = 0; i < m_ParameterCount; ++i) {
		resource->ReadInt(block, 1);
		++block;
		resource->ReadInt(block, 1);
		++block;
	}

	for (u32 i = 0; i < m_FlagCount; ++i) {
		resource->ReadInt(block, 1);
		++block;
		resource->ReadInt(block, 1);
		++block;
	}

	for (u32 i = 0; i < m_RequestCount; ++i) {
		resource->ReadInt(block, 1);
		++block;
		resource->ReadInt(block, 1);
		++block;
	}

	u32* parameters = m_SourceParameterData.Get<u32>();
	for (u32 i = 0; i < m_SourceParameterCount; ++i) {
		resource->ReadInt(parameters, 1);
		++parameters;
		resource->ReadInt(parameters, 1);
		++parameters;
		resource->ReadInt(parameters, 1);
		++parameters;
	}

#if MV_ENABLE_REFERENCE_OUTPUT_SIGNALS
	u32* outputs = m_OutputParameterData.Get<u32>();
	for (u32 i = 0; i < m_OutputParameterCount; ++i) {
		resource->ReadInt(outputs, 1);
		++outputs;
		resource->ReadInt(outputs, 1);
		++outputs;
	}
#endif
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeReferenceDef::QueryMemorySize() const
{
	return sizeof(mvNodeReferenceDef) + m_ParameterCount*sizeof(u32)*2 + m_FlagCount*sizeof(u32)*2 + m_RequestCount*sizeof(u32)*2
		+ m_SourceParameterCount*sizeof(ParameterSourceData);
}

//////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeReferenceDef::ResolveParameterSet(u16)
{
	AssertMsg(false, "Shouldn't be calling mvNodeReferenceDef::ResolveParameterSet");
	return NULL;
}

//////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeReferenceDef::ResolveParameterSetType(u16)
{
	AssertMsg(false, "Shouldn't ge calling mvNodeReferenceDef::ResolveParameterSetType");
	return mvParameter::ParamType(~0U);
}

//////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeReferenceDef::ResolveParameterGet(u16)
{
	AssertMsg(false, "Shouldn't be calling mvNodeReferenceDef::ResolveParameterGet");
	return NULL;
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeReferenceDef::ResolveEvent(u16)
{
	AssertMsg(false, "Shouldn't be calling mvNodeReferenceDef::ResolveEvent");
	return (u32)mvNode::kInvalidEvent;
}

//////////////////////////////////////////////////////////////////////////

template <typename T>
int FindFromWeb(u32 key, const mvMotionWeb* UNUSED_PARAM(web), const T* data, u32 dataCount, u32* buffer)
{
	const T* flags = data;
	u32 flagCount = dataCount;
	if (flagCount != 0) {
		u32* flagData = buffer;
		u32 i = key%flagCount;
		while (flags[i].key != 0xffffffff && flags[i].key != key) {
			i = (i+1)%flagCount;
		}
		if (flags[i].key != 0xffffffff) {
			u32 index = flags[i].index;

			return (flagData[0] & (1<<index)) >> index;

		}
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(mvNodeReference, crmtNode::kNodeReference, CRMT_NODE_RECEIVE_PREUPDATES|CRMT_NODE_RECEIVE_UPDATES);

//////////////////////////////////////////////////////////////////////////

mvNodeReference::mvNodeReference()
: crmtNodeParent(crmtNode::kNodeReference)
{
}

//////////////////////////////////////////////////////////////////////////

mvNodeReference::mvNodeReference(datResource& rsc)
: crmtNodeParent(rsc)
{
}

//////////////////////////////////////////////////////////////////////////

extern mvAnimationLoader g_mvAnimationLoader;
extern mvClipLoader g_mvClipLoader;
extern mvExpressionLoader g_mvExpressionLoader;
extern mvParameterizedMotionLoader g_mvParameterizedMotionLoader;
extern mvNetworkDefLoader g_mvNetworkDefLoader;

void SourceParameterFromData(mvParameter* parameter, u32 type, mvNodeReferenceDef::ParameterSourceData::PayloadData* payload, const mvMotionWeb* web)
{
	switch (type) {
		case kSignalAnimation:
			{
				const char* filename = payload->Filename.Get<const char>();
				parameter->SetAnimationUnsafe(g_mvAnimationLoader(filename, web));
			}
			break;
		case kSignalClip:
			{
				u32 *clipContext = payload->Filename.Get<u32>() - 4;
				u32 *clipParamA = reinterpret_cast< u32 * >(clipContext + 1);
				u32 *clipParamB = clipParamA + 1;
				parameter->SetClipUnsafe(g_mvClipLoader(*clipContext, *clipParamA, *clipParamB, web));
			}
			break;
		case kSignalExpression:
			{
				u32 *expressionDict = payload->Filename.Get<u32>() - 4;
				u32 *expression = expressionDict + 1;
				parameter->SetExpressionsUnsafe(g_mvExpressionLoader(*expressionDict, *expression, web));
			}
			break;
		case kSignalPM:
			{
				const char* filename = payload->Filename.Get<const char>();
				parameter->SetPMUnsafe(g_mvParameterizedMotionLoader(filename, web));
			}
			break;
		case kSignalNetwork:
			{
				// Might be cool to be able to load up a network from here.
				//const char* filename = payload.Filename.Get<const char>();
			}
			break;
		case kSignalReal:
			{
				parameter->SetReal(payload->Float);
			}
			break;
		default:
			// TODO: Catch a potential export error here
			break;
	}
}

//////////////////////////////////////////////////////////////////////////

void mvNodeReference::Init(mvNetwork* root, mvMotionWeb* web, const mvNodeReferenceDef* def)
{
	m_RootNetwork = root;
	m_Web = web;
	m_Def = def;

	mvParameterBuffer* pb = GetParameters();
	mvParameterBuffer* ipb = GetInternalParameters();

	crmtNode* ret = NULL;
	crmtMotionTree& motionTree = crmtNodeParent::GetMotionTree();
	const mvNetworkDef* ref = g_mvNetworkDefLoader(m_Def->m_NetworkId, m_Web);
	if (ref != NULL) {
		mvMotionWeb* web = m_Web;
		mvParameterBuffer* parentpb = web->GetParameters();
		mvParameterBuffer* parentipb = web->GetInternalParameters();
		const u32* block = reinterpret_cast<const u32*>(m_Def+1);
		for (u32 i = 0; i < m_Def->m_ParameterCount; ++i) {
			const mvParameter* parameter = parentipb->FindFirst(*block);
			parameter = parameter ? parameter : parentpb->FindFirst(*block);
			++block;
			if (parameter != NULL) {
				ipb->Insert(*block, parameter);
			}
			++block;
		}

		// Source the parameter and push it into the parent's parameter buffer
		mvNodeReferenceDef::ParameterSourceData* data = m_Def->m_SourceParameterData.Get<mvNodeReferenceDef::ParameterSourceData>();
		u32 count = m_Def->m_SourceParameterCount;
		for (u32 i = 0; i < count; ++i) {
			mvParameter parameter;
			SourceParameterFromData(&parameter, data->Type, &data->Payload, web);
			parameter.AddRef();
			pb->Insert(data->Key, &parameter);
			++data;
		}

		//mvNodeStateDef* root = (mvNodeStateDef*)((*ref)->m_network);
		mvNodeStateDef* root = (mvNodeStateDef*)(ref->GetNetwork());
		// this node (reference) is going to become the new parent
		//	and replace what would usually be a state node interface... savvy? DARCHARD - This isn't exactly true anymore
		mvNodeDef* source = root->m_Initial.Get<mvNodeDef>();

		// part of mvMotionWeb
		// We already have one of these if we're a node! Store it anyway in-case something else uses it.
		// Sloppy but I'm trying to keep as much the same as possible before old code can be stripped!
		m_NetworkDef = ref;

		crmtNode** children = Alloca(crmtNode*, root->m_ChildCount);
		mvCreateContext cxt;
		cxt.m_MotionTree = &motionTree;
		cxt.m_Network = this;
		cxt.m_OwnerNetwork = m_RootNetwork;
		cxt.m_Parameters = GetParameters();
		cxt.m_Children = children;
		cxt.m_ChildrenCount = root->m_ChildCount;
		cxt.m_Tail = NULL;

		ret = source->Create(&cxt);
		root->RegisterHandlers(this, children);
	}

	if(!ret)
	{
		Errorf("Couldn't find a referenced network, motion will probably look wrong!");
		// if we can't find the network to build, stuff in an invalid just in case
		crmtNodeInvalid* dummy = crmtNodeInvalid::CreateNode(motionTree);
		dummy->Init();
		ret = dummy;
	}

	AddChild(*ret);

	m_SignalData.Reset();
}

//////////////////////////////////////////////////////////////////////////

void mvNodeReference::PreUpdate(float)
{
	GetParameters()->Reset();
	GetParameterBuffers().SwapBuffers();
	mvParameterBuffer* pb = GetParameters();

	mvMotionWeb* web = m_Web;
	mvParameterBuffer* wpb = web->GetParameters();
	mvParameterBuffer* wipb = web->GetInternalParameters();

	u32 parameterCount = m_Def->m_ParameterCount;
	u32 flagCount = m_Def->m_FlagCount;
	u32 requestCount = m_Def->m_RequestCount;

	const u32* block = reinterpret_cast<const u32*>(m_Def+1);
	for (u32 i = 0; i < parameterCount; ++i) {
		const mvParameter* parameter = wipb->FindFirst(*block);
		parameter = parameter ? parameter : wpb->FindFirst(*block);
		++block;
		if (parameter != NULL) 
		{
			pb->Insert(*block, parameter);
		}
		++block;
	}

	const mvNetworkDef* def = web->GetDefinition();
	mvSignalData& signalData = web->GetSignalData();
	for (u32 i = 0; i < flagCount; ++i) {
		u32 flagsData = signalData.GetFlags();
		u32* flagsPtr = &flagsData;

		int flag = FindFromWeb<flag_t>(*block, web, def->GetFlags(), def->GetFlagCount(), flagsPtr);
		++block;
		u32 key = *block;
		const mvNetworkDef* ref = m_NetworkDef;
		const flag_t* flags = ref->GetFlags();
		u32 count = ref->GetFlagCount();
		if (count != 0) {
			u32 i = key % count;
			while (flags[i].key!=0xffffffff && flags[i].key != key) {
				i = (i+1)%count;
			}
			if (flags[i].key != 0xffffffff) {
				u32 idx = flags[i].index;

				signalData.SetFlag(idx, flag?true:false);

			}
		}
		++block;
	}

	for (u32 i = 0; i < requestCount; ++i) {

		u32 requestData = signalData.GetRequests();
		u32* requestPtr = &requestData;

		int request = FindFromWeb<request_t>(*block, web, def->GetRequests(), def->GetRequestCount(), requestPtr);
		++block;
		u32 key = *block;
		const mvNetworkDef* ref = m_NetworkDef;
		const request_t* requests = ref->GetRequests();
		u32 count = ref->GetRequestCount();
		if (count != 0) {
			u32 i = key % count;
			while (requests[i].key != 0xffffffff && requests[i].key != key) {
				i = (i+1)%count;
			}
			if (requests[i].key != 0xffffffff) {
				u32 idx = requests[i].index;

				if (request) {
					signalData.SetRequest(idx);
				}
			}
		}
		++block;
	}
}

//////////////////////////////////////////////////////////////////////////

void mvNodeReference::Update(float)
{
	bool childDisabled = GetFirstChild()->IsDisabled();
	SetDisabled(childDisabled);
	SetSilent(!childDisabled);

	GetParameters()->Reset();
	GetParameterBuffers().SwapBuffers();
	mvParameterBuffer* pb = GetParameters();

	mvMotionWeb* web = m_Web;
	mvParameterBuffer* wpb = web->GetParameters();
	mvParameterBuffer* wipb = web->GetInternalParameters();

	u32 parameterCount = m_Def->m_ParameterCount;
	u32 flagCount = m_Def->m_FlagCount;
	u32 requestCount = m_Def->m_RequestCount;

	const u32* block = reinterpret_cast<const u32*>(m_Def+1);
	for (u32 i = 0; i < parameterCount; ++i) {
		const mvParameter* parameter = wipb->FindFirst(*block);
		parameter = parameter ? parameter : wpb->FindFirst(*block);
		++block;
		if (parameter != NULL) 
		{
			pb->Insert(*block, parameter);
		}
		++block;
	}

	const mvNetworkDef* def = web->GetDefinition();
	mvSignalData& signalData = web->GetSignalData();
	for (u32 i = 0; i < flagCount; ++i) {
		u32 flagsData = signalData.GetFlags();
		u32* flagsPtr = &flagsData;

		int flag = FindFromWeb<flag_t>(*block, web, def->GetFlags(), def->GetFlagCount(), flagsPtr);
		++block;
		u32 key = *block;
		const mvNetworkDef* ref = m_NetworkDef;
		const flag_t* flags = ref->GetFlags();
		u32 count = ref->GetFlagCount();
		if (count != 0) {
			u32 i = key % count;
			while (flags[i].key!=0xffffffff && flags[i].key != key) {
				i = (i+1)%count;
			}
			if (flags[i].key != 0xffffffff) {
				u32 idx = flags[i].index;

				signalData.SetFlag(idx, flag?true:false);

			}
		}
		++block;
	}

	for (u32 i = 0; i < requestCount; ++i) {

		u32 requestData = signalData.GetRequests();
		u32* requestPtr = &requestData;

		int request = FindFromWeb<request_t>(*block, web, def->GetRequests(), def->GetRequestCount(), requestPtr);
		++block;
		u32 key = *block;
		const mvNetworkDef* ref = m_NetworkDef;
		const request_t* requests = ref->GetRequests();
		u32 count = ref->GetRequestCount();
		if (count != 0) {
			u32 i = key % count;
			while (requests[i].key != 0xffffffff && requests[i].key != key) {
				i = (i+1)%count;
			}
			if (requests[i].key != 0xffffffff) {
				u32 idx = requests[i].index;

				if (request) {
					signalData.SetRequest(idx);
				}
			}
		}
		++block;
	}

	m_SignalData.ResetRequests();

#if MV_ENABLE_REFERENCE_OUTPUT_SIGNALS
	mvParameterBuffer* external = m_Web->GetInternalParameters();
	mvParameterBuffer* internal = m_InternalParameters;

	const u32* outputParameters = m_Def->m_OutputParameterData.Get<u32>();
	u32 outputParameterCount = m_Def->m_OutputParameterCount;

	for (u32 i = 0; i < outputParameterCount; ++i) {
		const u32 from = *outputParameters++;
		const u32 to = *outputParameters++;
		mvParameter* parameter = internal->FindFirst(from);
		if (parameter != NULL) {
			external->Insert(to, parameter);
		}
	}
#endif
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage
