//
// move/move_parameter.inl
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#define SAFE_INCLUDE(x) (!defined( x##_H ) || defined( x##_H_SAFE ))
#if SAFE_INCLUDE(MOVE_NODE) && SAFE_INCLUDE(MOVE_NETWORK) && SAFE_INCLUDE(MOVE_PARAMETERBUFFER) && SAFE_INCLUDE(MOVE_COMMAND) && SAFE_INCLUDE(MOVE_SUBNETWORK) && SAFE_INCLUDE(MOVE_OBSERVER) && SAFE_INCLUDE(MOVE_STATEDEF)

#include "move_node.h"
#include "move_observer.h"
#include "move_subnetwork.h"

#ifndef MOVE_PARAMETER_INL
#define MOVE_PARAMETER_INL

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameter::SetObserverSafe(mvObserver* observer)
{
	m_type = kParameterObserver;
	m_observer = observer;
	if(m_observer)
	{
		m_observer->AddRef();
	}
}

//////////////////////////////////////////////////////////////////////////

__forceinline void mvParameter::SetNetworkSafe(mvSubNetwork* network)
{
	m_type = kParameterNetwork;
	m_network = network;
	if(m_network)
	{
		m_network->AddRef();
	}
}

//////////////////////////////////////////////////////////////////////////

__forceinline void mvParameter::Release()
{
	if(m_anim)
	{
		switch (m_type)
		{
#if !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterAnimation:
			m_anim->Release();
			break;
#endif // !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterClip:
			m_clip->Release();
			break;
		case mvParameter::kParameterExpressions:
			m_expr->Release();
			break;
		case mvParameter::kParameterFilter:
			m_filter->Release();
			break;
		case mvParameter::kParameterFrame:
			m_frame->Release();
			break;
#if !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterPM:
#endif // !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterBoolean:
		case mvParameter::kParameterReal:
		case mvParameter::kParameterData:
			break;
		case mvParameter::kParameterObserver:
			m_observer->Release();
			break;
		case mvParameter::kParameterNetwork:
			m_network->Release();
			break;
		}
	}
}

//////////////////////////////////////////////////////////////////////////

__forceinline void mvParameter::AddRef()
{
	if(m_anim)
	{
		switch (m_type)
		{
#if !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterAnimation:
			m_anim->AddRef();
			break;
#endif // !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterClip:
			m_clip->AddRef();
			break;
		case mvParameter::kParameterExpressions:
			m_expr->AddRef();
			break;
		case mvParameter::kParameterFilter:
			m_filter->AddRef();
			break;
		case mvParameter::kParameterFrame:
			m_frame->AddRef();
			break;
#if !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterPM:
#endif // !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterBoolean:
		case mvParameter::kParameterReal:
		case mvParameter::kParameterData:
			break;
		case mvParameter::kParameterObserver:
			m_observer->AddRef();
			break;
		case mvParameter::kParameterNetwork:
			m_network->AddRef();
			break;
		}
	}
}

//////////////////////////////////////////////////////////////////////////

__forceinline void mvParameter::AddKnownRef()
{
	// These tests are reasonably sane because all three classes derive from pgBase.
#if RESTRICTED_PARAMTER_TYPES
	if(m_anim && (m_type == kParameterProperty || m_type == kParameterClip))
#else // RESTRICTED_PARAMTER_TYPES
	if(m_anim && (m_type == kParameterAnimation || m_type == kParameterClip || m_type == kParameterPM || m_type == kParameterProperty))
#endif // RESTRICTED_PARAMTER_TYPES
	{
		BANK_SAFE_ADD_KNOWN_REF(m_anim);
	}
}

//////////////////////////////////////////////////////////////////////////

__forceinline void mvParameter::RemoveKnownRef()
{
	// These tests are reasonably sane because all three classes derive from pgBase.
#if RESTRICTED_PARAMTER_TYPES
	if(m_anim && (m_type == kParameterProperty || m_type == kParameterClip))
#else // RESTRICTED_PARAMTER_TYPES
	if(m_anim && (m_type == kParameterAnimation || m_type == kParameterClip || m_type == kParameterPM || m_type == kParameterProperty))
#endif // RESTRICTED_PARAMTER_TYPES
	{
		BANK_SAFE_REMOVE_KNOWN_REF(m_anim);
	}
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_PARAMETER_INL

#endif

