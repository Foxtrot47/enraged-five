//
// move/move_memorysystem.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_memorysystem.h"

#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/new.h"

#include "move.h"
#include "move_fixedheap.h"

#define MOVE_MEMORY_HIGH_WATERMARK __DEV

namespace rage {

using namespace mvFixedHeapSystem;

class Page;

struct PageHeader
{
	Page* Next; // ePageLocation stored in ptr to next

	void Init()
	{
		Next = NULL;
	}
};

class Page
{
public:
	enum { kDataBytes = mvMemorySystem::kPageMemSize-sizeof(PageHeader) };

	char m_Data[kDataBytes];
	PageHeader m_Header;
public:
	Page* GetNext() const
	{
		return m_Header.Next;
	}

	void SetNext(Page* page)
	{
		m_Header.Next = page;
	}

	void Init()
	{
		m_Header.Init();
	}
};

static Page* s_PagesUnused = NULL;
static Page* s_PagePool = NULL;
static u32 s_PagePoolMemSize = 0;
static u32 s_PagePoolCount = 0;
static void* s_PagePoolEnd = NULL;

class PageOverflow
{
private:
	Page Page;
	PageOverflow* Next;
public:
	void SetNext(PageOverflow* next)
	{
		Next = next;
	}

	PageOverflow* GetNext() const
	{
		return Next;
	}

	rage::Page* GetPage()
	{
		return &Page;
	}

	void Init()
	{
		Page.Init();
		Next = NULL;
	}
};
static PageOverflow* s_PageOverflow = NULL;

#if MOVE_MEMORY_HIGH_WATERMARK
static u32 s_UsedPageCount = 0;
static u32 s_UsedPageWaterMark = (RSG_PC || RSG_DURANGO || RSG_ORBIS) ? 512 : 160;
#endif // MOVE_MEMORY_HIGH_WATERMARK

static sysCriticalSectionToken s_MemorySystemToken;

void mvMemorySystem::InitializeMemorySystem(void* buffer, u32 size)
{
	Assert(!s_PagePool);

	Assert(sizeof(Page) == kPageMemSize);
	s_PagePoolMemSize = size;
	s_PagePoolCount = s_PagePoolMemSize / kPageMemSize;
	Assert(kPageMemSize*s_PagePoolCount == s_PagePoolMemSize);
	s_PagePool = (Page*)buffer;
	Assert(s_PagePool);

	s_PagePoolEnd = ((char*)s_PagePool) + size;

	// add them all to the Unused list :

	s_PagesUnused = s_PagePool;
	for(u32 i=0;i<s_PagePoolCount;i++)
	{
		s_PagePool[i].Init();
		if (i < s_PagePoolCount-1)
			s_PagePool[i].SetNext(s_PagePool+i+1);
	}

	InitializeFixedHeap(buffer, size);
}

void mvMemorySystem::ShutdownMemorySystem()
{
	ShutdownFixedHeap();

	// TODO: free main memory

	if (!s_PagePool)
	{
		return;
	}

	s_PagePool = NULL;

	s_PagePoolEnd = NULL;
	s_PagesUnused = NULL;
}

void* mvMemorySystem::AllocatePage()
{
	sysCriticalSection cs (s_MemorySystemToken);
	sysMemUseMemoryBucket b(mvMove::sm_MemoryBucket);

	Page* page = s_PagesUnused;
	if (Likely(page != NULL))
	{
		s_PagesUnused = s_PagesUnused->GetNext();
	}
	else
	{
		PageOverflow* overflow = s_PageOverflow;
		s_PageOverflow = rage_new PageOverflow();
		s_PageOverflow->Init();
		s_PageOverflow->SetNext(overflow);

		page = s_PageOverflow->GetPage();
	}
#if MOVE_MEMORY_HIGH_WATERMARK
	++s_UsedPageCount;
	if (s_UsedPageCount > s_UsedPageWaterMark)
	{
		s_UsedPageWaterMark = s_UsedPageCount;
		Displayf("!!!=========================================================================================");
		Displayf("\t New high water mark for move page allocator: %d pages!", s_UsedPageWaterMark);
		Displayf("!!!=========================================================================================");
	}
#endif // MOVE_MEMORY_HIGH_WATERMARK

	return page->m_Data;
}

void mvMemorySystem::FreePage(void* page)
{
	sysCriticalSection cs (s_MemorySystemToken);

	Page* curr = (Page*)page;
	curr->SetNext(s_PagesUnused);
	s_PagesUnused = curr;

#if MOVE_MEMORY_HIGH_WATERMARK
	--s_UsedPageCount;
#endif // MOVE_MEMORY_HIGH_WATERMARK
}

bool mvMemorySystem::AvailablePage()
{
	sysCriticalSection cs (s_MemorySystemToken);

	return s_PagesUnused != NULL;
}

void* mvMemorySystem::GetPageForAddress(void* ptr)
{
	Assert(s_PagePool);
	if (Likely(ptr >= s_PagePool && ptr < s_PagePoolEnd))
	{
		const size_t offset = (u8*)ptr - (u8*)s_PagePool;

		const size_t index = offset >> kPageMemSizeShift;
		Assert(index < s_PagePoolCount);

		return s_PagePool + index;
	}
	else
	{
		PageOverflow* curr = s_PageOverflow;
		while (curr != NULL)
		{
			Page* page = curr->GetPage();
			if (page <= ptr && ptr < page+1)
			{
				return page;
			}

			curr = curr->GetNext();
		}

		// we're in trouble here
		return NULL;
	}
}

} // namespace rage
