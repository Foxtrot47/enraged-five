//
// move/move_blendn.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_BLENDN_H
#define MOVE_BLENDN_H

#include "move_noden.h"

#include "crmotiontree/nodeblendn.h"

namespace rage {

class crFrameFilter;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create an n-way blend node
class mvNodeBlendNDef : public mvNodeNDef
{
public:
	// PURPOSE: Create an instance of this n-way blend node
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeBlendN* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 type);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeBlendNDef);
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move n-way blend node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeBlendN : public crmtNodeBlendN
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_FILTER_SET(T, ApplyFilter) \
		MOVE_FILTER_GET(T, GetFilter) \
		MOVE_REAL_SET_INDEXED(T, ApplyWeight) \
		MOVE_REAL_GET_INDEXED(T, GetWeight) \
		MOVE_FILTER_SET_INDEXED(T, ApplyInputFilter) \
		MOVE_FILTER_GET_INDEXED(T, GetInputFilter)

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeBlendN)

	// PURPOSE: Don't polute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeBlendN);
};

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeBlendN::ApplyFilter(crFrameFilter* filter)
{
	crmtNodeBlendN::SetFilter(filter);
}

///////////////////////////////////////////////////////////////////////////////

inline crFrameFilter* mvNodeBlendN::GetFilter() const
{
	return crmtNodeBlendN::GetFilter();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeBlendN::ApplyWeight(u32 childidx, float weight)
{
	crmtNodeBlendN::SetWeight(childidx, weight);
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeBlendN::GetWeight(u32 childidx) const
{
	return crmtNodeBlendN::GetWeight(childidx);
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeBlendN::ApplyInputFilter(u32 childidx, crFrameFilter* filter)
{
	crmtNodeBlendN::SetInputFilter(childidx, filter);
}

///////////////////////////////////////////////////////////////////////////////

inline crFrameFilter* mvNodeBlendN::GetInputFilter(u32 childidx) const
{
	return crmtNodeBlendN::GetInputFilter(childidx);
}

///////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_BLENDN_H
