//
// move/pm.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_pm.h"

#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodepm.h"

#include "move_parameterbuffer.h"
#include "move_macros.h"
#include "move_state.h"
#include "move_types.h"
#include "move_types_internal.h"

namespace rage {

extern mvParameterizedMotionLoader g_mvParameterizedMotionLoader;

//////////////////////////////////////////////////////////////////////////

unsigned mvNodePmDef::GetParameterizedMotionFlagFrom(u32 flags)
{
	return flags & 0x00000003;
}

unsigned mvNodePmDef::GetRateFlagFrom(u32 flags)
{
	return (flags & 0x0000000c) >> 0x02;
}

unsigned mvNodePmDef::GetPhaseFlagFrom(u32 flags)
{
	return (flags & 0x00000030) >> 0x04;
}

unsigned mvNodePmDef::GetDeltaFlagFrom(u32 flags)
{
	return (flags & 0x000000c0) >> 0x06;
}

unsigned mvNodePmDef::GetParameterCountFrom(u32 flags)
{
	return (flags & 0x00003c00) >> 0x0a;
}

// This gives all the parameter flags
unsigned mvNodePmDef::GetParameterValuesFlagFrom(u32 flags)
{
	return (flags & 0xfff80000) >> kParameterShift;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodePmDef);

///////////////////////////////////////////////////////////////////////////////

mvNodePm* mvNodePmDef::Create(mvCreateContext* context) const
{
	mvNodePm* node = mvNodePm::CreateNode(*context->m_MotionTree);
	crpmParameterizedMotionPlayer& player = node->GetParameterizedMotionPlayer();
	crpmParameterizedMotion* pm = NULL;

	const void* block = this + 1;
	u32 flags = m_flags;

	const mvParameterBuffer* params = context->m_Parameters;
	const mvMotionWeb* network = context->m_Network;

	unsigned pmFlag = GetParameterizedMotionFlagFrom(flags);
	unsigned rateFlag = GetRateFlagFrom(flags);
	unsigned phaseFlag = GetPhaseFlagFrom(flags);
	unsigned deltaFlag = GetDeltaFlagFrom(flags);

	// pm
	switch (pmFlag)
	{
	case mvConstant::kFlagSignal:
			{
				u32 id = ReadUInt32Block();
				mvParameter ret; ret.SetPMUnsafe(NULL);
				const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterPM, &ret);
				pm = param->GetPM();
				player.SetParameterizedMotion(pm);
			}
			break;
		case mvConstant::kFlagValue:
			{
				unsigned* len = (unsigned*)block;
				block = (u32*)block+1;
				const char* filename = (const char*)block;
				block = (char*)block+(*len);
				pm = MV_REQUISITION_PM(filename, network);
				player.SetParameterizedMotion(pm);
			}
			break;
	}

	// rate
	switch (rateFlag)
	{
	case mvConstant::kFlagValue:
		{
			float rate = ReadFloatBlock();
			player.SetRate(rate);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(1.f);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			float rate = param->GetReal();
			player.SetRate(rate);
		}
		break;
	}

	if (!pm) return node;

	// phase
	switch (phaseFlag)
	{
	case mvConstant::kFlagValue:
		{
			float phase = ReadFloatBlock();
			pm->SetU(phase);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(0.f);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			float phase = param->GetReal();
			pm->SetU(phase);
		}
		break;
	}

	// delta
	switch (deltaFlag)
	{
	case mvConstant::kFlagValue:
		{
			float delta = ReadFloatBlock();
			pm->SetPostDeltaU(delta / pm->GetDuration());
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(0.f);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			float delta = param->GetReal();
			pm->SetPostDeltaU(delta / pm->GetDuration());
		}
		break;
	}

	unsigned count = GetParameterCountFrom(flags);
	unsigned parameters = GetParameterValuesFlagFrom(flags);
//	u32 count = (flags & PARAMETER_COUNT_MASK) >> PARAMETER_COUNT_SHIFT;
//	u32 parameters = flags >> PARAMETER_SHIFT;

	count = Min(count, (u32)pm->GetNumParameterDimensions());
	for (u32 i = 0; i < count; ++i)
	{
		switch (parameters & kParameterValueMask)
		{
		case mvConstant::kFlagValue:
			{
				float parameter = ReadFloatBlock();
				pm->SetParameterValue(i, parameter);
			}
			break;
		case mvConstant::kFlagSignal:
			{
				u32 id = ReadUInt32Block();
				mvParameter ret; ret.SetReal(0.f);
				const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
				float parameter = param->GetReal();
				pm->SetParameterValue(i, parameter);
			}
			break;
		}
		parameters >>= kParameterValueShift;

		// TODO: parameter value update signals
	}

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodePmDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_flags, 1);

	void* block = this + 1;
	u32 flags = m_flags;

	unsigned parameterizedMotionFlag = GetParameterizedMotionFlagFrom(flags);
	MV_VALIDATE(parameterizedMotionFlag < mvConstant::kFlagCount, "(mvNodePmDef) parameterized motion flag out of range");
	unsigned rateFlag = GetRateFlagFrom(flags);
	MV_VALIDATE(rateFlag < mvConstant::kFlagCount, "(mvNodePmDef) rate flag out of range");
	unsigned phaseFlag = GetPhaseFlagFrom(flags);
	MV_VALIDATE(phaseFlag < mvConstant::kFlagCount, "(mvNodePmDef) phase flag out of range");
	unsigned deltaFlag = GetDeltaFlagFrom(flags);
	MV_VALIDATE(deltaFlag < mvConstant::kFlagCount, "(mvNodePmDef) delta flag out of range");

	// pm
	MV_PARAMETERIZED_MOTION_BLOCK_INIT(block, resource, parameterizedMotionFlag);
	// rate
	MV_REAL_BLOCK_INIT_FLAG(block, resource, rateFlag);
	// phase
	MV_REAL_BLOCK_INIT_FLAG(block, resource, phaseFlag);
	// delta
	MV_REAL_BLOCK_INIT_FLAG(block, resource, deltaFlag);

	// parameters
	u32 count = GetParameterCountFrom(flags);
	u32 parameters = GetParameterValuesFlagFrom(flags);
	for (u32 i = 0; i < count; ++i)
	{
		MV_REAL_BLOCK_INIT_FLAG(block, resource, parameters & kParameterValueMask);
		parameters >>= kParameterValueShift;
	}
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodePmDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodePmDef);

	const void* block = this + 1;
	u32 flags = m_flags;

	unsigned parameterizedMotionFlag = GetParameterizedMotionFlagFrom(flags);
	unsigned rateFlag = GetRateFlagFrom(flags);
	unsigned phaseFlag = GetPhaseFlagFrom(flags);
	unsigned deltaFlag = GetDeltaFlagFrom(flags);

	// pm
	MV_PARAMETERIZED_MOTION_BLOCK_SIZE(block, parameterizedMotionFlag, size);
	// rate
	MV_REAL_BLOCK_SIZE_FLAG(block, rateFlag, size);
	// phase
	MV_REAL_BLOCK_SIZE_FLAG(block, phaseFlag, size);
	// delta
	MV_REAL_BLOCK_SIZE_FLAG(block, deltaFlag, size);

	// parameters
	unsigned count = GetParameterCountFrom(flags);
	unsigned parameters = GetParameterValuesFlagFrom(flags);
	//	u32 count = (flags & PARAMETER_COUNT_MASK) >> PARAMETER_COUNT_SHIFT;
	//	u32 parameters = flags >> PARAMETER_SHIFT;
	for (u32 i = 0; i < count; ++i)
	{
	  MV_REAL_BLOCK_SIZE_FLAG(block, parameters & kParameterValueMask, size);
	  parameters >>= kParameterValueShift;
	}

	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodePmDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodePm::ApplyParameterizedMotion),
		MOVE_MAKE_SIGNAL(mvNodePm::ApplyPhase),
		MOVE_MAKE_SIGNAL(mvNodePm::ApplyDelta),
		MOVE_MAKE_SIGNAL(mvNodePm::ApplyRate),
		MOVE_MAKE_SIGNAL(mvNodePm::ApplyParameterValue)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodePmDef::ResolveParameterSetType(u16 parameter)
{
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterPM,
		mvParameter::kParameterReal,
		mvParameter::kParameterReal,
		mvParameter::kParameterReal,
		mvParameter::kParameterReal,
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodePmDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodePm::GetParameterizedMotion),
		MOVE_MAKE_SIGNAL(mvNodePm::GetPhase),
		MOVE_MAKE_SIGNAL(mvNodePm::GetDelta),
		MOVE_MAKE_SIGNAL(mvNodePm::GetRate),
		MOVE_MAKE_SIGNAL(mvNodePm::GetParameterValue)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodePmDef::ResolveEvent(u16 event)
{
	static u32 events[] = {
		crmtNodePm::kMsgPmLooped,
		crmtNodePm::kMsgPmEnded,
	};

	Assert(event < NELEM(events));
	return events[event];
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodePm, crmtNodePm, mvNode::kNodePM, mvNodePmDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
