//
// move/move_mergen.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_mergen.h"

#include "system/nelem.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

enum
{
	kParameterFilterN = 0,
	kParameterFilter = 1,
	kParameterWeight = 2,
	kParameterInputFilter = 3,

	kParameterCount,
};

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeMergeNDef);

//////////////////////////////////////////////////////////////////////////

mvNodeMergeN* mvNodeMergeNDef::Create(mvCreateContext* context) const
{
	mvNodeMergeN* node = mvNodeMergeN::CreateNode(*context->m_MotionTree);
	mvNodeNDef::Create(node, context);

	return node;
}

//////////////////////////////////////////////////////////////////////////

void mvNodeMergeNDef::Init(mvMemoryResource* resource)
{
	mvNodeNDef::Init(resource);
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeMergeNDef::QueryMemorySize() const
{
	return mvNodeNDef::QueryMemorySize(sizeof(mvNodeMergeNDef));
}

//////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeMergeNDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		NULL,
		MOVE_MAKE_SIGNAL(mvNodeMergeN::ApplyFilter),
		MOVE_MAKE_SIGNAL(mvNodeMergeN::ApplyWeight),
		MOVE_MAKE_SIGNAL(mvNodeMergeN::ApplyInputFilter)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

//////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeMergeNDef::ResolveParameterSetType(u16 parameter)
{
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterFilterN,
		mvParameter::kParameterFilter,
		mvParameter::kParameterReal,
		mvParameter::kParameterFilter
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

//////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeMergeNDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		NULL,
		MOVE_MAKE_SIGNAL(mvNodeMergeN::GetFilter),
		MOVE_MAKE_SIGNAL(mvNodeMergeN::GetWeight),
		MOVE_MAKE_SIGNAL(mvNodeMergeN::GetInputFilter)
	};

	Assert(parameter < kParameterCount);
	return funcs[parameter];
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeMergeNDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeMergeN, crmtNodeMergeN, mvNode::kNodeMergeN, mvNodeMergeNDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
