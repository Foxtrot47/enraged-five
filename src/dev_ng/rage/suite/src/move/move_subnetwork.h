//
// move/move_subnetwork.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_SUBNETWORK_H
#define MOVE_SUBNETWORK_H

#include "move_command.h"
#include "move_network.h"
#include "move_node.h"

#define MOVE_SUBNETWORK_H_SAFE

namespace rage {

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition on how to create a subnetwork node
class mvNodeSubNetworkDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this subnetwork definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	crmtNode* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read in to memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is epxorted by makenetwork, but the indices between makenetwork and
	//	code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	// makenetowrk and code must match
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'event'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition in moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeSubNetworkDef);

private:
	// PURPOSE: The network id this subnetwork node responds to when the parameter buffer contains subnetworks
	u32 m_networkId;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Handle for the subnetwork.
// NOTES: This is half the meat of the subnetwork system. It's the handle
//	that the game code can interface with and is also held internally to
//	move by an actaul node: mvNdoeSubNetwork
class mvSubNetwork : public mvMotionWeb
{
public:
	// PURPOSE: Default constructor
	mvSubNetwork();

	// PURPOSE: Initialization
	// PARAMS:
	//	def - definition this subnetwork is initialized with.
	//	buf - memory for the output buffer
	//	count - number of elements in the output buffer
	// NOTES:
	//	Caller must still take control of memory allocated for buf
	void Init(const mvNetworkDef* def, mvParameterBuffers& outputBuffers);

	// PURPOSE: Copy command buffer to the parameter  buffer
	// SEE ALSO: mvCommandBuffer::PrepareBuffer
	void PrepareBuffers(mvCommandBuffers& cmds);

	// PURPOSE: Set the subnetwork as attached
	void SetAttached(bool attached);

	// PURPOSE: Return true if it's attached
	// NOTES: This is sometimes used by the game so they can test
	//	if the network has been attached yet.
	bool IsAttached() const { return m_IsAttached; }

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();

	// PURPOSE: supporting data for the subnetwork blend duration signal
	// NOTES: These are game hacks
	static const u32 ms_BlendDurationId;
	static const u32 ms_BlendOutDurationId;

private:

	// PURPOSE: Have we been attached to an mvNodeSubNetwork yet.
	bool m_IsAttached;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Represent a subnetwork in the motion tree
class mvNodeSubNetwork : public crmtNodeParent
{
public:
	// PURPOSE: Default constructor
	mvNodeSubNetwork();

	// PURPOSE: Resource constructor
	mvNodeSubNetwork(datResource&);

	// PURPOSE: Destructor
	virtual ~mvNodeSubNetwork();

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(mvNodeSubNetwork);

	// PURPOSE: Initialize this node
	// PARAMS:
	//	root - the very top network
	//	owner - the network that owns this subnetwork node
	//	networkId - The id this subnetwork node responds to
	void Init(mvNetwork* root, const mvMotionWeb* owner, u32 networkId);

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Pre update call
	// NOTES: Check the parameter buffer of the owner network. If signal m_NetworkId
	//	exists then transition to that network, or invalid if there's no network
	//	but the parameter id still exists (eg. Is NULL)
	virtual void PreUpdate(float);

	// PURPOSE: Update call
	// NOTES: Resets the attached networks request buffer, force state buffer and writes
	//	any internally pushed parameters into the output buffer.
	virtual void Update(float);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: Disable inserts
	// NOTES: Early's out of ::PreUpdate so the attached network doesn't change
	void DisableInserts();

#if __DEV
	// PURPOSE: Useful stuff for debug systems
	const mvMotionWeb* DebugGetNetwork() const { return m_OwnerNetwork; }
	const mvSubNetwork* DebugGetSubNetwork() const { return m_Data; }
	u32 DebugGetNetworkId() const {return m_NetworkId; }
#endif // __DEV

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();

private:
	// PURPOSE: The network that owns this node
	const mvMotionWeb* m_OwnerNetwork;

	// PURPOSE: The very parent network
	mvNetwork* m_RootNetwork;

	// PURPOSE: The subnetwork, if any has been attached to this node
	mvSubNetwork* m_Data;

	// PURPOSE: The id that this network node responds to
	u32 m_NetworkId;

	// PURPOSE: Don't allow subnetwork to be changed
	bool m_DisableInserts;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Register with the move type system so we
//	can construct nodes of this type
MOVE_DECLARE_REGISTERED_CLASS(mvNodeSubNetworkClass);

//////////////////////////////////////////////////////////////////////////

inline void mvSubNetwork::PrepareBuffers(mvCommandBuffers& cmds)
{
	GetParameterBuffers().SwapBuffers();

	cmds.GetNextBuffer().PrepareBuffer(*this, *m_NetworkDef, *GetParameters());
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#include "move_parameter.h"

#endif // MOVE_SUBNETWORK_H
