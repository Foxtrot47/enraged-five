//
// move/move_parameterbuffer.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_PARAMETERBUFFER_H
#define MOVE_PARAMETERBUFFER_H

#include "move_config.h"
#include "move_parameter.h"

#include "atl/array.h"
#include "atl/map.h"
#include "math/simplemath.h"
#include "system/cache.h"

#define MOVE_PARAMETERBUFFER_H_SAFE

#define VALIDATE_COMPLEX_PARAMETERBUFFERS (__ASSERT)

namespace rage {

struct mvSignalData;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Move parameter buffer
// Supports reserved capacity, but automatically grows beyond this (with performance penalty)
// Fast inserts, newer duplicates override older entries
// Fast appending of other buffers
// DeDuplication/Compact to prevent endless buffer size growth with continuous insert/append
// Fast find, on finalized buffers (which no longer accept new inserts)
class mvParameterBuffer
{
	struct Buffer;
	struct Map;

public:

	// PURPOSE: Constructor
	mvParameterBuffer();

	// PURPOSE: Destructor
	~mvParameterBuffer();

	// PURPOSE: Initialize
	// reservedCapacity - initial parameter capacity (must be a power of two)
	// NOTE: Buffers will automatically grow beyond reserved capacity - but with performance penalty
	void Init(u16 reservedCapacity);

	// PURPOSE: Shutdown, free/release all dynamic resources
	void Shutdown();

	// PURPOSE: Reset buffer
	// NOTES: Clears out and releases all parameters
	// (reduces capacity back to original reserved value)
	void Reset();

	// PURPOSE: Clear buffer
	// NOTES: Clears out and releases all parameters
	// (keeps capacity at current value)
	void Clear();

	// PURPOSE: Finalizes the buffer(s)
	// NOTES: Optimizes Find performance
	// WARNING: No new Insert/Append/Copy/Invalidate calls can be made after calling Finalize
	void Finalize();

	// PURPOSE: UnFinalizes the buffers(s)
	// NOTES: Removes optimizations, allowing future Insert/Append/Copy/Invalidate calls
	void UnFinalize();

	// PURPOSE: Removes stale duplicate entries from the buffer(s)
	// NOTES: To optimize Insert performance, newer entries with same key are just appended
	// to the buffers, but they override any previous duplicate ones
	// But old entries are still retained in the buffer (causing the buffers to continue growing)
	// DeDuplicate finds duplicate entries in the buffers, and invalidates them
	// (but it does not reclaim any memory, see Compact)
	void DeDuplicate();

	// PURPOSE: Compacts the buffer(s)
	// NOTES: Repacks the buffers to remove invalid entries, reclaiming wasted memory
	// Only removes entries already marked as invalid, does not detect stale duplicates.
	// Call DeDuplicate first to remove duplicates and reclaim their memory.
	void Compact();

	// PURPOSE: Append contents of a buffer into this buffer
	// WARNING: This is *DESTRUCTIVE* to the buffer passed in
	// All parameters in passed in buffer are *MOVED* to this buffer,
	// passed in buffer will be empty following this call.
	// Cannot be called on a finalized buffer, call UnFinalize() first.
	// NOTES: See Copy() for non-destructive (but slower) append
	// or Move() for a somewhat slower, destructive but capacity retaining append
	void Append(mvParameterBuffer& pb);

	// PURPOSE: Move contents of a buffer into this buffer
	// WARNING: This is *DESTRUCTIVE* to the buffer passed in
	// All parameters in passed in buffer are *MOVED* to this buffer,
	// passed in buffer will be empty following this call
	// Though unlike Append() passed in buffer will have same capacity as before.
	// Cannot be called on a finalized buffer, call UnFinalize() first.
	// NOTES: See Copy() for non-destructive (but slower) append
	// or Append() for faster but fully destructive append
	void Move(mvParameterBuffer& pb);

	// PURPOSE: Copy contents of a buffer into this buffer
	// NOTES: Non-destructive to buffer passed in, for faster (but destructive) versions, see Append() or Move()
	// Cannot be called on a finalized buffer, call UnFinalize() first.
	void Copy(const mvParameterBuffer& pb);

	// PURPOSE: Dump all parameters
#if __BANK
	void Dump() const;
	void Dump(atString&) const;
#endif // __BANK

	// PURPOSE: Get count of parameters inside buffer
	// WARNING: This is an upper bound, actual parameters may be less (due to duplication/invalidation)
	// NOTES: See CalcCount for (a much more costly) actual count of parameters
	u16 GetCount() const;

	// PURPOSE: Get capacity of parameter buffer
	// NOTE: As more parameters are inserted or buffers appended, this capacity will automatically grow
	u16 GetCapacity() const;

	// PURPOSE: Calculate count of parameters actually used inside the buffer
	// WARNING: This is a very expensive operation, as it searches for invalid entries
	// NOTES: See GetCount for much cheaper, upper bound estimate on count of parameters
	u16 CalcCount() const;

	// PURPOSE: Find a parameter inside the buffer
	const mvParameter* FindFirst(u32 key, const mvParameter* fallback=NULL) const;
	const mvParameter* FindFirstOfType(u32 key, mvParameter::ParamType type, const mvParameter* fallback=NULL) const;

	// PURPOSE: Convert flags and requests
	void ConvertToSignal(const mvNetworkDef* def, mvSignalData& signalData);

	// PURPOSE: Iterate the parameters
	// Search parameter types/keys that occur more than once
	// Starts with oldest parameters in the buffer, and advances to most recent
	class Iterator
	{
	public:

		// PURPOSE: Default constructor
		Iterator() {}

		// PURPOSE: Reset iterator to beginning
		mvKeyedParameter* Begin(mvParameterBuffer& parent, mvParameter::ParamType type=mvParameter::kParameterNone, u32 key=0);

		// PURPOSE: Reset iterator to end
		mvKeyedParameter* End(mvParameterBuffer& parent, mvParameter::ParamType type=mvParameter::kParameterNone, u32 key=0);

		// PURPOSE: Advance iterator to next parameter
		mvKeyedParameter* Next();

		// PURPOSE: Advance iterator to previous parameter
		mvKeyedParameter* Previous();

	protected:

		mvParameterBuffer* m_Parent;
		mvParameter::ParamType m_Type;
		u32 m_Key;
		s32 m_BufferIdx;
		s32 m_ParameterIdx;
	};

	// PURPOSE: Safe insert for the different types
	// NOTES: Cannot be called on a finalized buffer, call UnFinalize() first.
	void Insert(u32 key, const crAnimation* anim);
	void Insert(u32 key, const crClip* clip);
	void Insert(u32 key, const crExpressions* expr);
	void Insert(u32 key, const crProperty* prop);
	void Insert(u32 key, crFrameFilter* filter);
	void Insert(u32 key, crFrame* frame);
	void Insert(u32 key, crpmParameterizedMotion* pm);
	void Insert(u32 key, bool boolean);
	void Insert(u32 key, float real);
	void Insert(u32 key, int integer);
	void Insert(u32 key, void* data);
	void Insert(u32 key, mvObserver* observer);
	void Insert(u32 key, mvSubNetwork* network);
	void Insert(u32 key, const mvNetworkDef* ref);
	void InsertNodeName(u32 key, u32 name);
	void InsertNodeId(u32 key, u32 id);
	void InsertRequest(u32 key, bool boolean);
	void InsertFlag(u32 key, bool boolean);

	// PURPOSE: Raw insert for parameter
	// NOTES: Cannot be called on a finalized buffer, call UnFinalize() first.
	void Insert(u32 key, const mvParameter* parameter);
	void Insert(u32 key, const mvParameter& parameter);

private:

	// PURPOSE: Raw insert for parameter (with no ref counting)
	// NOTES: For internal use only - no ref/known ref registration, use with extreme care
	// Cannot be called on a finalized buffer, call UnFinalize() first.
	mvKeyedParameter& InsertNoRef(u32 key, const mvParameter& parameter);

	// PURPOSE: Internal find
	const mvParameter* FindFirstInternal(u32 key) const;
	const mvParameter* FindFirstOfTypeInternal(u32 key, mvParameter::ParamType type) const;

	// PURPOSE: Return next free parameter
	// PARAM: complex - is new parameter simple or complex
	// NOTES: Automatically generates additional capacity if required
	mvKeyedParameter& Grow(bool complex);

	// PURPOSE: Compact and/or create buffer
	void GenerateFreeCapacity();

	// PURPOSE: Internal map generation
	void GenerateMap(Map* map, bool removeDuplicates);

	// PURPOSE: Internal parameter buffer
	struct Buffer
	{
		// PURPOSE: Constructor with capacity
		Buffer(u16 capacity);
		
		// PURPOSE: Destructor
		~Buffer();

		// PURPOSE: Shuts down buffer (releases all parameters)
		// NOTES: Doesn't clear buffer contents, use Clear() if intending to reuse buffer
		void Shutdown();

		// PURPOSE: Clears all parameters, releasing them, resets count
		void Clear();

		// PURPOSE: Append a buffer to this, assumes caller has checked there is sufficient free space
		void Append(Buffer& buf);

		// PURPOSE: Find parameters
		const mvParameter* FindFirst(u32 key) const;
		const mvParameter* FindFirstOfType(u32 key, mvParameter::ParamType type) const;

		// PURPOSE: Insert a parameter, assumes caller has checked there is sufficient free space
		void Insert(u32 key, const mvParameter& parameter);

		// PURPOSE: Return next free parameter, assumes caller has checked there is sufficient free space
		// PARAMS: complex - is new parameter simple or complex
		mvKeyedParameter& Grow(bool complex);

		// PURPOSE: Calculate actual count (checking for invalid entries)
		u16 CalcCount() const;

		// PURPOSE: Dump parameters
#if __BANK
		void Dump(atString& output) const;
#endif // __BANK

		// PURPOSE: Get keyed parameters living at the end of the buffer
		const mvKeyedParameter* GetKeyedParameters() const;
		mvKeyedParameter* GetKeyedParameters();

		// PURPOSE: Calculate memory required to allocate parameter buffer
		static u32 CalcMemory(u16 capacity);

		// PURPOSE: Allocate parameter buffer
		static Buffer* Allocate(u16 capacity);

		// PURPOSE: Free parameter buffer
		static void Free(Buffer* buffer);

	public:

		static const u16 sm_MaxCapacity = 128;

		u16 m_Count;
		u16 m_Capacity;

		bool m_Complex;
		datPadding<3> m_Padding;
	};

	static const int sm_MaxNumBuffers = 8;
	atFixedArray<Buffer*, sm_MaxNumBuffers> m_Buffers;

	// PURPOSE: Internal map, used for fast parameter lookup in finalized buffers
	struct Map
	{
		// PURPOSE: Constructor with capacity
		Map(u16 capacity);

		// PURPOSE: Destructor
		~Map();

		// PURPOSE: Shutdown
		void Shutdown();

		
		// PURPOSE: Find parameter
		const mvParameter* Find(u32 key) const;

		// PURPOSE: Insert parameter, assumes caller has checked there is sufficient capacity
		void Insert(u32 key, mvParameter* p);


		// PURPOSE: Calculate memory required to allocate map
		static u32 CalcMemory(u16 capacity);

		// PURPOSE: Allocate map
		static Map* Allocate(u16 capacity);

		// PURPOSE: Free map
		static void Free(Map* map);


		// PURPOSE: Helper class for managing atMap memory policy
		template <typename _Key, typename _Data>
		struct MapMemory
		{
		public:

			// PURPOSE: Entry type
			typedef atMapEntry<_Key, _Data> _EntryType;

			// PURPOSE: Constructor
			MapMemory(_EntryType* entries, u16 capacity, _EntryType** hashTable);

			// PURPOSE: Allocate entry
			_EntryType* Allocate(const _Key &k, _EntryType *n);
			_EntryType* Allocate(const _Key &k, const _Data &d, _EntryType *n);

			// PURPOSE: Deallocate entry
			void DeAllocate(_EntryType*);

			// PURPOSE: Allocate hash table
			void AllocateHash(int capacity, _EntryType**& hash);

			// PURPOSE: Deallocate hash table
			void DeAllocateHash(_EntryType**);

			u16 m_Count;
			u16 m_Capacity;

			_EntryType* m_Entries;
			_EntryType** m_HashTable;
		};

		static const int sm_MapHashTableSize = 11;
		atRangeArray<atMapEntry<u32, mvParameter*>*, sm_MapHashTableSize> m_MapHashTable;

		MapMemory<u32, mvParameter*> m_MapMemory;	

		atMap<u32, mvParameter*, atMapHashFn<u32>, atMapEquals<u32>, MapMemory<u32, mvParameter*> > m_Map;

		// PURPOSE: Fake entry type used to allocate storage array
		struct EntryType
		{
			char m_Data[sizeof(atMapEntry<u32, mvParameter*>)];
		};

		EntryType m_MapEntries[];
	};

	Map* m_Map;

	u16 m_Count;
	u16 m_Capacity;

	u32 m_TypeMask;
	u32 m_KeyMask;

	static const u16 sm_DefaultCapacity = 16;
	u16 m_DefaultCapacity;

	// TODO --- peak buffer capacity
	// TODO --- high water mark?
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Double buffering of parameter buffers
template<typename _BufferType>
class mvBuffers
{
public:

	// PURPOSE: Constructor
	mvBuffers();

	// PURPOSE: Destructor
	~mvBuffers();

	// PURPOSE: Initializer
	void Init(u16 reservedSize);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Get the current parameter buffer
	const _BufferType& GetCurrentBuffer() const;
	_BufferType& GetCurrentBuffer();

	// PURPOSE: Get the next parameter buffer
	const _BufferType& GetNextBuffer() const;
	_BufferType& GetNextBuffer();

	// PURPOSE: Swap the parameter buffers
	void SwapBuffers();

protected:
	u32 m_BufferIndex;
	_BufferType m_Buffers[2];
};

////////////////////////////////////////////////////////////////////////////////

typedef mvBuffers<mvParameterBuffer> mvParameterBuffers;

////////////////////////////////////////////////////////////////////////////////

inline u16 mvParameterBuffer::GetCount() const
{
	return m_Count;
}

////////////////////////////////////////////////////////////////////////////////

inline u16 mvParameterBuffer::GetCapacity() const
{
	return m_Capacity;
}

////////////////////////////////////////////////////////////////////////////////

inline const mvParameter* mvParameterBuffer::FindFirst(u32 key, const mvParameter* fallback) const
{
	const mvParameter* p = FindFirstInternal(key);
	return p?p:fallback;
}

////////////////////////////////////////////////////////////////////////////////

inline const mvParameter* mvParameterBuffer::FindFirstOfType(u32 key, mvParameter::ParamType type, const mvParameter* fallback) const
{
	const mvParameter* p = FindFirstOfTypeInternal(key, type);
	return p?p:fallback;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, const crAnimation* anim)
{
	mvKeyedParameter& kp = Grow(RESTRICTED_PARAMTER_TYPES?false:(anim != NULL));
	kp.Key = key;
	kp.Parameter.SetAnimationSafe(anim);
	m_TypeMask |= 1<<mvParameter::kParameterAnimation;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, const crClip* clip)
{
	mvKeyedParameter& kp = Grow(clip != NULL);
	kp.Key = key;
	kp.Parameter.SetClipSafe(clip);
	m_TypeMask |= 1<<mvParameter::kParameterClip;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, const crExpressions* expr)
{
	mvKeyedParameter& kp = Grow(expr != NULL);
	kp.Key = key;
	kp.Parameter.SetExpressionsSafe(expr);
	m_TypeMask |= 1<<mvParameter::kParameterExpressions;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, const crProperty* prop)
{
	mvKeyedParameter& kp = Grow(prop != NULL);
	kp.Key = key;
	kp.Parameter.SetPropertySafe(prop);
	m_TypeMask |= 1<<mvParameter::kParameterProperty;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, crFrameFilter* filter)
{
	mvKeyedParameter& kp = Grow(filter != NULL);
	kp.Key = key;
	kp.Parameter.SetFilterSafe(filter);
	m_TypeMask |= 1<<mvParameter::kParameterFilter;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, crFrame* frame)
{
	mvKeyedParameter& kp = Grow(frame != NULL);
	kp.Key = key;
	kp.Parameter.SetFrameSafe(frame);
	m_TypeMask |= 1<<mvParameter::kParameterFrame;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, crpmParameterizedMotion* pm)
{
	mvKeyedParameter& kp = Grow(RESTRICTED_PARAMTER_TYPES?false:(pm != NULL));
	kp.Key = key;
	kp.Parameter.SetPMSafe(pm);
	m_TypeMask |= 1<<mvParameter::kParameterPM;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, bool boolean)
{
	mvKeyedParameter& kp = Grow(false);
	kp.Key = key;
	kp.Parameter.SetBool(boolean);
	m_TypeMask |= 1<<mvParameter::kParameterBoolean;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, float real)
{
	mvKeyedParameter& kp = Grow(false);
	kp.Key = key;
	kp.Parameter.SetReal(real);
	m_TypeMask |= 1<<mvParameter::kParameterReal;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, int integer)
{
	mvKeyedParameter& kp = Grow(false);
	kp.Key = key;
	kp.Parameter.SetInt(integer);
	m_TypeMask |= 1<<mvParameter::kParameterInt;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, void* data)
{
	mvKeyedParameter& kp = Grow(false);
	kp.Key = key;
	kp.Parameter.SetData(data);
	m_TypeMask |= 1<<mvParameter::kParameterData;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, mvObserver* observer)
{
	mvKeyedParameter& kp = Grow(observer != NULL);
	kp.Key = key;
	kp.Parameter.SetObserverSafe(observer);
	m_TypeMask |= 1<<mvParameter::kParameterObserver;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, mvSubNetwork* network)
{
	mvKeyedParameter& kp = Grow(network != NULL);
	kp.Key = key;
	kp.Parameter.SetNetworkSafe(network);
	m_TypeMask |= 1<<mvParameter::kParameterNetwork;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, const mvNetworkDef* ref)
{
	mvKeyedParameter& kp = Grow(false);
	kp.Key = key;
	kp.Parameter.SetReference(ref);
	m_TypeMask |= 1<<mvParameter::kParameterReference;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::InsertNodeName(u32 key, u32 name)
{
	mvKeyedParameter& kp = Grow(false);
	kp.Key = key;
	kp.Parameter.SetNodeName(name);
	m_TypeMask |= 1<<mvParameter::kParameterNodeName;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::InsertNodeId(u32 key, u32 id)
{
	mvKeyedParameter& kp = Grow(false);
	kp.Key = key;
	kp.Parameter.SetNodeId(id);
	m_TypeMask |= 1<<mvParameter::kParameterNodeId;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::InsertRequest(u32 key, bool boolean)
{
	mvKeyedParameter& kp = Grow(false);
	kp.Key = key;
	kp.Parameter.SetRequest(boolean);
	m_TypeMask |= 1<<mvParameter::kParameterRequest;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::InsertFlag(u32 key, bool boolean)
{
	mvKeyedParameter& kp = Grow(false);
	kp.Key = key;
	kp.Parameter.SetFlag(boolean);
	m_TypeMask |= 1<<mvParameter::kParameterFlag;
	m_KeyMask |= 1<<(key&31);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, const mvParameter* insert)
{
	if(insert)
	{
		Insert(key, *insert);
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Insert(u32 key, const mvParameter& insert)
{
	Assert(!m_Map);

	// if out of space, create a new buffer on demand
	Assert(m_Count <= m_Capacity);
	if(m_Count == m_Capacity)
	{
		GenerateFreeCapacity();
	}

	m_Count++;
	m_TypeMask |= 1<<u32(insert.m_type);
	m_KeyMask |= 1<<(key&31);
	m_Buffers.Top()->Insert(key, insert);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline mvKeyedParameter& mvParameterBuffer::InsertNoRef(u32 key, const mvParameter& insert)
{
	mvKeyedParameter& kp = Grow((insert.m_data != NULL) && insert.IsComplex());
	kp.Key = key;
	kp.Parameter.m_type = insert.m_type;
	kp.Parameter.m_data = insert.m_data;
	return kp;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline mvKeyedParameter& mvParameterBuffer::Grow(bool complex)
{
	Assert(!m_Map);

	// if out of space, create a new buffer on demand
	Assert(m_Count <= m_Capacity);
	if(m_Count == m_Capacity)
	{
		GenerateFreeCapacity();
	}

	m_Count++;
	return m_Buffers.Top()->Grow(complex);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline const mvParameter* mvParameterBuffer::FindFirstInternal(u32 key) const
{
	if(!((1<<(key&31)) & m_KeyMask))
	{
		return NULL;
	}

	if(!key)
	{
		return NULL;
	}

	if(m_Map)
	{
		return m_Map->Find(key);
	}
	else
	{
		const mvParameter* p = NULL;
		for(int i=m_Buffers.GetCount()-1; !p && i>=0; --i)
		{
			p = m_Buffers[i]->FindFirst(key);
		}
		return p;
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline const mvParameter* mvParameterBuffer::FindFirstOfTypeInternal(u32 key, mvParameter::ParamType type) const
{
	if(!((1<<u32(type)) & m_TypeMask))
	{
		return NULL;
	}

	if(!((1<<(key&31)) & m_KeyMask))
	{
		return NULL;
	}

	if(!key)
	{
		return NULL;
	}

	if(m_Map)
	{
		const mvParameter* p = m_Map->Find(key);
#if __DEV
		if(p && p->m_type != u32(type))
		{
			atHashString tempKey;

			tempKey.SetHash(key);
			const char *name = tempKey.TryGetCStr();

			Displayf("mvParameterBuffer::FindFirstOfTypeInternal type mismatch %d != %d on parameter %d %s", type, p->m_type, key, name);
		}
#endif // __DEV
		Assert(!p || p->m_type == u32(type));
		return p;
	}
	else
	{
		const mvParameter* p = NULL;
		for(int i=m_Buffers.GetCount()-1; !p && i>=0; --i)
		{
			p = m_Buffers[i]->FindFirstOfType(key, type);
		}
		return p;
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Buffer::Shutdown()
{
	if(m_Count)
	{
		mvKeyedParameter* params = GetKeyedParameters();
		if(m_Complex)
		{
			mvKeyedParameter* pkp = params;
			for(int i=m_Count; i>0; --i)
			{
				pkp->Parameter.RemoveKnownRef();
				pkp->Parameter.Release();
				++pkp;
			}
			m_Complex = false;
		}
		m_Count = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Buffer::Clear()
{
	if(m_Count)
	{
		mvKeyedParameter* params = GetKeyedParameters();
		if(m_Complex)
		{
			mvKeyedParameter* pkp = params;
			for(int i=m_Count; i>0; --i)
			{
#if !RESTRICTED_PARAMTER_TYPES
				pkp->ResetRelease();
#else // !RESTRICTED_PARAMTER_TYPES
				pkp->Parameter.RemoveKnownRef();
				pkp->Parameter.Release();
#endif //! RESTRICTED_PARAMTER_TYPES
				++pkp;
			}
			m_Complex = false;
		}
#if !RESTRICTED_PARAMTER_TYPES
		else
		{
			for(int i=0; i<m_Count; ++i)
			{
				mvKeyedParameter& kp = params[i];
				Assert((kp.Parameter.m_data == NULL) || !kp.Parameter.IsComplex());
				kp.Key = 0;
				kp.Parameter.m_type = mvParameter::kParameterNone;
				kp.Parameter.m_data = NULL;
			}
		}
#else // !RESTRICTED_PARAMTER_TYPES
#if VALIDATE_COMPLEX_PARAMETERBUFFERS
		else
		{
			for(int i=0; i<m_Count; ++i)
			{
				mvKeyedParameter& kp = params[i];
				Assert((kp.Parameter.m_data == NULL) || !kp.Parameter.IsComplex());
			}
		}
#endif // VALIDATE_COMPLEX_PARAMETERBUFFERS
		sysMemSet(params, 0, sizeof(mvKeyedParameter)*m_Count);
#endif // !RESTRICTED_PARAMTER_TYPES
		m_Count = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline const mvParameter* mvParameterBuffer::Buffer::FindFirst(u32 key) const
{
	const mvKeyedParameter* params = GetKeyedParameters();
	for(int i=m_Count-1; i>=0; --i)
	{
		const mvKeyedParameter& kp = params[i];
		if(kp.Key == key)
		{
			return &kp.Parameter;
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline const mvParameter* mvParameterBuffer::Buffer::FindFirstOfType(u32 key, mvParameter::ParamType type) const
{
	const mvKeyedParameter* params = GetKeyedParameters();
	for(int i=m_Count-1; i>=0; --i)
	{
		const mvKeyedParameter& kp = params[i];
		if(kp.Key == key && kp.Parameter.m_type == u32(type))
		{
			return &kp.Parameter;
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Buffer::Insert(u32 key, const mvParameter& insert)
{
	mvKeyedParameter& kp = Grow((insert.m_data != NULL) && insert.IsComplex());
	kp.Key = key;
	kp.Parameter.m_type = insert.m_type;
	kp.Parameter.m_data = insert.m_data;
	kp.Parameter.AddRef();
	kp.Parameter.AddKnownRef();
}

////////////////////////////////////////////////////////////////////////////////

__forceinline mvKeyedParameter& mvParameterBuffer::Buffer::Grow(bool complex)
{
	Assert(m_Count < m_Capacity);

	m_Complex |= complex;
	return GetKeyedParameters()[m_Count++];
}

////////////////////////////////////////////////////////////////////////////////

inline u16 mvParameterBuffer::Buffer::CalcCount() const
{
	const mvKeyedParameter* params = GetKeyedParameters();
	u16 count = 0;
	for(int i=0; i<m_Count; ++i)
	{
		const mvKeyedParameter& kp = params[i];
		if(kp.Parameter.m_type != mvParameter::kParameterNone)
		{
			count++;
		}
	}
	return count;
}

////////////////////////////////////////////////////////////////////////////////

inline const mvKeyedParameter* mvParameterBuffer::Buffer::GetKeyedParameters() const
{
	return reinterpret_cast<const mvKeyedParameter*>(this + 1);
}

////////////////////////////////////////////////////////////////////////////////

inline mvKeyedParameter* mvParameterBuffer::Buffer::GetKeyedParameters()
{
	return reinterpret_cast<mvKeyedParameter*>(this + 1);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline const mvParameter* mvParameterBuffer::Map::Find(u32 key) const
{
	const mvParameter*const* pp = m_Map.Access(key);
	return pp?*pp:NULL;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void mvParameterBuffer::Map::Insert(u32 key, mvParameter* p)
{
	Assert(m_MapMemory.m_Count < m_MapMemory.m_Capacity);
	m_Map.Insert(key, p);
}

////////////////////////////////////////////////////////////////////////////////

template <typename _Key, typename _Data>
inline mvParameterBuffer::Map::MapMemory<_Key, _Data>::MapMemory(_EntryType* entries, u16 capacity, _EntryType** hashTable)
: m_Count(0)
, m_Capacity(capacity)
, m_Entries(entries)
, m_HashTable(hashTable)
{
}

////////////////////////////////////////////////////////////////////////////////

template <typename _Key, typename _Data>
inline typename mvParameterBuffer::Map::MapMemory<_Key,_Data>::_EntryType* mvParameterBuffer::Map::MapMemory<_Key, _Data>::Allocate(const _Key &k, _EntryType *n)
{
	ATL_MAP_FUNC();
	Assert(m_Count < m_Capacity);
	return ::new (&m_Entries[m_Count++]) _EntryType(k,n);
}

////////////////////////////////////////////////////////////////////////////////

template <typename _Key, typename _Data>
inline typename mvParameterBuffer::Map::MapMemory<_Key,_Data>::_EntryType* mvParameterBuffer::Map::MapMemory<_Key, _Data>::Allocate(const _Key &k, const _Data &d, _EntryType *n)
{
	ATL_MAP_FUNC();
	Assert(m_Count < m_Capacity);
	return ::new (&m_Entries[m_Count++]) _EntryType(k,d,n);
}

////////////////////////////////////////////////////////////////////////////////

template <typename _Key, typename _Data>
inline void mvParameterBuffer::Map::MapMemory<_Key,_Data>::DeAllocate(_EntryType*)
{
}

////////////////////////////////////////////////////////////////////////////////

template <typename _Key, typename _Data>
inline void mvParameterBuffer::Map::MapMemory<_Key,_Data>::AllocateHash(int UNUSED_PARAM(capacity), _EntryType**& hash)
{
	hash = m_HashTable;
}

////////////////////////////////////////////////////////////////////////////////

template <typename _Key, typename _Data>
inline void mvParameterBuffer::Map::MapMemory<_Key,_Data>::DeAllocateHash(_EntryType**)
{
}

////////////////////////////////////////////////////////////////////////////////

template <typename _BufferType>
mvBuffers<_BufferType>::mvBuffers()
: m_BufferIndex(0)
{
}

////////////////////////////////////////////////////////////////////////////////

template <typename _BufferType>
mvBuffers<_BufferType>::~mvBuffers()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

template <typename _BufferType>
void mvBuffers<_BufferType>::Init(u16 reservedSize)
{
	m_Buffers[0].Init(reservedSize);
	m_Buffers[1].Init(reservedSize);
}

////////////////////////////////////////////////////////////////////////////////

template <typename _BufferType>
void mvBuffers<_BufferType>::Shutdown()
{
	m_Buffers[0].Shutdown();
	m_Buffers[1].Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

template <typename _BufferType>
inline const _BufferType& mvBuffers<_BufferType>::GetCurrentBuffer() const
{
	return m_Buffers[m_BufferIndex];
}

////////////////////////////////////////////////////////////////////////////////

template <typename _BufferType>
inline _BufferType& mvBuffers<_BufferType>::GetCurrentBuffer()
{
	return m_Buffers[m_BufferIndex];
}

////////////////////////////////////////////////////////////////////////////////

template <typename _BufferType>
inline const _BufferType& mvBuffers<_BufferType>::GetNextBuffer() const
{
	return m_Buffers[(m_BufferIndex+1)&1];
}

////////////////////////////////////////////////////////////////////////////////

template <typename _BufferType>
inline _BufferType& mvBuffers<_BufferType>::GetNextBuffer()
{
	return m_Buffers[(m_BufferIndex+1)&1];
}

////////////////////////////////////////////////////////////////////////////////

template <typename _BufferType>
inline void mvBuffers<_BufferType>::SwapBuffers()
{
	m_BufferIndex = (m_BufferIndex+1)&1;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#include "move_parameter.h"

#endif // MOVE_PARAMETERBUFFER_H

