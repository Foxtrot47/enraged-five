//
// move/add.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_add.h"

#include "system/nelem.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeAddSubtractDef);

//////////////////////////////////////////////////////////////////////////

mvNodeAddSubtract* mvNodeAddSubtractDef::Create(mvCreateContext* context) const
{
	mvNodeAddSubtract* node = mvNodeAddSubtract::CreateNode(*context->m_MotionTree);
	mvNodePairDef::Create(node, context);
	return node;
}

//////////////////////////////////////////////////////////////////////////

void mvNodeAddSubtractDef::Init(mvMemoryResource* resource)
{
	mvNodePairDef::Init(resource);
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeAddSubtractDef::QueryMemorySize() const
{
	return mvNodePairDef::QueryMemorySize(sizeof(mvNodeAddSubtractDef));
}

//////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeAddSubtractDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeAddSubtract::ApplyFilter),
		MOVE_MAKE_SIGNAL(mvNodeAddSubtract::ApplyWeight)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

//////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeAddSubtractDef::ResolveParameterSetType(u16 parameter)
{
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterFilter,
		mvParameter::kParameterReal,
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

//////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeAddSubtractDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeAddSubtract::GetFilter),
		MOVE_MAKE_SIGNAL(mvNodeAddSubtract::GetWeight)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeAddSubtractDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;		
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeAddSubtract, crmtNodeAddSubtract, mvNode::kNodeAdd, mvNodeAddSubtractDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
