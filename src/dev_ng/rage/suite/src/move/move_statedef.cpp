//
// move/move_statedef.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "move_statedef.h"

#include "move_observer.h"
#include "move_state.h"

#include "crmotiontree/nodeproxy.h"

namespace rage {

///////////////////////////////////////////////////////////////////////////////

void mvNodeStateBaseDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	m_Initial.Init(resource);
	resource->ReadInt(&m_DeferBlockUpdate, 1);
	resource->ReadByte(&m_OnEnterEnabled, 1);
	resource->ReadByte(&m_OnExitEnabled, 1);
	resource->ReadByte(&m_ChildCount, 1);
	resource->ReadByte(&m_TransitionCount, 1);
	resource->ReadInt(&m_OnEnterId, 1);
	resource->ReadInt(&m_OnExitId, 1);
	resource->ReadInt(&m_Transitions.offset, 1);
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeStateBaseDef::InitTransitions(mvMemoryResource* resource, mvTransition* transitions, u32 transitionCount)
{
	mvTransition* transition = transitions;
	for (u32 transitionIdx = 0; transitionIdx < transitionCount; ++transitionIdx) {
		u32 flags;
		resource->ReadInt(&flags, 1);
		transition->SetFromFlags(flags);
#if !MOVE_TRANSITION_EVENTS
		MV_VALIDATE(transition->Type == 0, "Invalid transition type");
#endif // !MOVE_TRANSITION_EVENTS
		MV_VALIDATE(transition->Modifier < mvConstant::kModifierCount, "Invalid modifier type on transition");
		MV_VALIDATE(transition->Synchronizer < mvConstant::kSynchronizerCount, "Invalid synchronizer on transition");
		resource->ReadInt(&transition->Tag, 1);
		resource->ReadFloat(&transition->Duration, 1);
		resource->ReadInt(&transition->Signal, 1);

		resource->ReadInt(&transition->TransitionWeightSignal, 1);

		MV_VALIDATE(transition->Duration >= 0.f, "Transition duration is less than 0.f");

#if MOVE_TRANSITION_EVENTS
		resource->ReadInt(&transition->TransitionEventId, 1);
		MV_VALIDATE((transition->TransitionEvent != 0) == (transition->TransitionEventId != 0), "Transition event id invalid");
#endif // MOVE_TRANSITION_EVENTS

		transition->Target.Init(resource);

		u32 conditionCount = transition->ConditionCount;
		mvCondition* condition = reinterpret_cast<mvCondition*>(transition+1);
		for (u32 conditionIdx = 0; conditionIdx < conditionCount; ++conditionIdx) {
			resource->ReadShort(&condition->Type, 1);
			resource->ReadShort(&condition->Set, 1);
			switch (condition->Type) {
			case mvConstant::kConditionInRange:
			case mvConstant::kConditionOutOfRange:
				{
					mvRange* data = static_cast<mvRange*>(condition);
					resource->ReadInt(&data->Signal, 1);
					resource->ReadFloat(&data->Upper, 1);
					resource->ReadFloat(&data->Lower, 1);
					condition = static_cast<mvCondition*>(data+1);
				}
				break;
			case mvConstant::kConditionOnRequest:
				{
					mvOnRequest* data = static_cast<mvOnRequest*>(condition);
					resource->ReadInt(&data->Request, 1);
					resource->ReadInt(&data->NotSet, 1);
					condition = static_cast<mvOnRequest*>(data+1);
				}
				break;
			case mvConstant::kConditionOnFlag:
				{
					mvOnFlag* data = static_cast<mvOnFlag*>(condition);
					resource->ReadInt(&data->Flag, 1);
					resource->ReadInt(&data->NotSet, 1);
					condition = static_cast<mvOnFlag*>(data+1);
				}
				break;
			case mvConstant::kConditionAtEvent:
				{
					mvAtEvent* data = static_cast<mvAtEvent*>(condition);
					resource->ReadInt(&data->Event, 1);
					resource->ReadInt(&data->NotSet, 1);
					condition = static_cast<mvCondition*>(data+1);
				}
				break;
			case mvConstant::kConditionGreaterThan:
			case mvConstant::kConditionGreaterThanEqual:
			case mvConstant::kConditionLessThan:
			case mvConstant::kConditionLessThanEqual:
				{
					mvValue* data = static_cast<mvValue*>(condition);
					resource->ReadInt(&data->Signal, 1);
					resource->ReadFloat(&data->Value, 1);
					condition = static_cast<mvCondition*>(data+1);
				}
				break;
			case mvConstant::kConditionLifetimeLessThan:
			case mvConstant::kConditionLifetimeGreaterThan:
				{
					mvConditionLifetime* data = static_cast<mvConditionLifetime*>(condition);
					resource->ReadFloat(&data->Lifetime, 1);
					condition = static_cast<mvCondition*>(data+1);
				}
				break;
			case mvConstant::kConditionOnMoveEventTag:
				{
					mvOnMoveEventTag* data = static_cast<mvOnMoveEventTag*>(condition);
					resource->ReadInt(&data->Tag, 1);
					resource->ReadInt(&data->NotSet, 1);
					condition = static_cast<mvOnMoveEventTag*>(data+1);
				}
				break;
			case mvConstant::kConditionBoolEquals:
				{
					mvBoolValue* data = static_cast<mvBoolValue*>(condition);
					resource->ReadInt(&data->Signal, 1);
					resource->ReadInt(&data->Value, 1);
					condition = static_cast<mvCondition*>(data+1);
				}
				break;
			}
		}
		if (transition->Filter) {
			u32 *filterDict = reinterpret_cast<u32*>(condition);
			resource->ReadInt(filterDict, 1);
			u32 *filter = filterDict+1;
			resource->ReadInt(filter, 1);
		}
		transition = reinterpret_cast<mvTransition*>(reinterpret_cast<u8*>(transition) + transition->Size);
	}
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_REGISTERED_TYPE(mvNodeStateMachineClass, mvNode::kNodeStateMachine, mvNodeStateMachineDef);

///////////////////////////////////////////////////////////////////////////////

const mvNodeStateMachineDef::StateNode* mvNodeStateMachineDef::GetChildData() const
{
	return reinterpret_cast<const mvNodeStateMachineDef::StateNode*>(this+1);
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeStateMachineDef);

///////////////////////////////////////////////////////////////////////////////

crmtNode* mvNodeStateMachineDef::Create(mvCreateContext* context) const
{
	mvNodeStateMachine* node = mvNodeStateMachine::CreateNode(*context->m_MotionTree);
	node->Init(context->m_Network, context->m_OwnerNetwork, this, context->m_Tail);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeStateMachineDef::Init(mvMemoryResource* resource)
{
	mvNodeStateBaseDef::Init(resource);
	StateNode* data = reinterpret_cast<StateNode*>(this+1);
	for (u32 i = 0; i < m_ChildCount; ++i) {
		resource->ReadInt(&data->Id, 1);
		data->Node.Init(resource);
		++data;
	}
	mvTransition* transitions = m_Transitions.Get();
	InitTransitions(resource, transitions, m_TransitionCount);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeStateMachineDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeStateMachineDef) + sizeof(StateNode)*m_ChildCount;
	u32 transitionCount = m_TransitionCount;
	const mvTransition* transition = m_Transitions.Get();
	for (u32 transitionIdx = 0; transitionIdx < transitionCount; ++transitionIdx) {
		size += transition->Size;
		transition = reinterpret_cast<const mvTransition*>(reinterpret_cast<const u8*>(transition) + transition->Size);
	}
	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeStateMachineDef::ResolveParameterSet(u16)
{
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeStateMachineDef::ResolveParameterSetType(u16)
{
	return mvParameter::ParamType(~0U);
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeStateMachineDef::ResolveParameterGet(u16)
{
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeStateMachineDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_REGISTERED_TYPE(mvNodeInlinedStateMachine, mvNode::kNodeInlinedStateMachine, mvNodeInlinedStateMachineDef);

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeInlinedStateMachineDef);

///////////////////////////////////////////////////////////////////////////////

crmtNode* mvNodeInlinedStateMachineDef::Create(mvCreateContext* context) const
{
	mvNodeDef* input = m_Input.Get<mvNodeDef>();
	crmtNode* tail = input->Create(context); // this can use the parent's children buffer because that's all it's doing

	mvNodeStateMachine* node = mvNodeStateMachine::CreateNode(*context->m_MotionTree);
	node->Init(context->m_Network, context->m_OwnerNetwork, this, tail);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeInlinedStateMachineDef::Init(mvMemoryResource* resource)
{
	mvNodeStateBaseDef::Init(resource);
	m_Input.Init(resource);
	mvNodeStateMachineDef::StateNode* data = reinterpret_cast<mvNodeStateMachineDef::StateNode*>(this+1);
	for (u32 i = 0; i < m_ChildCount; ++i) {
		resource->ReadInt(&data->Id, 1);
		data->Node.Init(resource);
		++data;
	}
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeInlinedStateMachineDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeInlinedStateMachineDef) + sizeof(mvNodeStateMachineDef::StateNode)*m_ChildCount;
	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeInlinedStateMachineDef::ResolveParameterSet(u16)
{
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeInlinedStateMachineDef::ResolveParameterSetType(u16)
{
	return mvParameter::ParamType(~0U);
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeInlinedStateMachineDef::ResolveParameterGet(u16)
{
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeInlinedStateMachineDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeStateDef);

///////////////////////////////////////////////////////////////////////////////

crmtNode* mvNodeStateDef::Create(mvCreateContext* context) const
{
	mvNodeDef* def = m_Initial.Get<mvNodeDef>();

	crmtNode** children = Alloca(crmtNode*, m_ChildCount);
	mvCreateContext childContext;
	childContext.m_MotionTree = context->m_MotionTree;
	childContext.m_Network = context->m_Network;
	childContext.m_OwnerNetwork = context->m_OwnerNetwork;
	childContext.m_Parameters = context->m_Parameters;
	childContext.m_Children = children;
	childContext.m_ChildrenCount = m_ChildCount;
	childContext.m_Tail = context->m_Tail;

	crmtNode* child = def->Create(&childContext);

	RegisterHandlers(context->m_Network, children);

	return child;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeStateDef::Init(mvMemoryResource* resource)
{
	mvNodeStateBaseDef::Init(resource);

	m_ParamInputs.Init(resource);
	resource->ReadInt(&m_ParamInputCount, 1);
	m_Events.Init(resource);
	resource->ReadInt(&m_EventCount, 1);
	m_ParamOutputs.Init(resource);
	resource->ReadInt(&m_ParamOutputCount, 1);
	m_Operations.Init(resource);
	resource->ReadInt(&m_OperationCount, 1);

	mvTransition* transitions = m_Transitions.Get();
	InitTransitions(resource, transitions, m_TransitionCount);

	u32	paramInputCount = m_ParamInputCount;
	mvNodeStateDef::Parameter* paramInputs = m_ParamInputs.Get<mvNodeStateDef::Parameter>();
	for (u32 i = 0; i < paramInputCount; ++i)
	{
		resource->ReadInt(&paramInputs->ParameterCrc, 1);
		resource->ReadShort(&paramInputs->NodeIndex, 1);
		resource->ReadShort(&paramInputs->TypeIndex, 1);
		resource->ReadInt(&paramInputs->ParameterIndex, 1);

		++paramInputs;
	}

	u32 eventCount = m_EventCount;
	mvNodeStateDef::Event* events = m_Events.Get<mvNodeStateDef::Event>();
	for (u32 i = 0; i < eventCount; ++i)
	{
		resource->ReadShort(&events->NodeIndex, 1);
		resource->ReadShort(&events->TypeIndex, 1);
		resource->ReadInt(&events->EventCrc, 1);

		++events;
	}

	u32 paramOutputCount = m_ParamOutputCount;
	mvNodeStateDef::Parameter* paramOutputs = m_ParamOutputs.Get<mvNodeStateDef::Parameter>();
	for (u32 i = 0; i < paramOutputCount; ++i)
	{
		resource->ReadInt(&paramOutputs->ParameterCrc, 1);
		resource->ReadShort(&paramOutputs->NodeIndex, 1);
		resource->ReadShort(&paramOutputs->TypeIndex, 1);
		resource->ReadInt(&paramOutputs->ParameterIndex, 1);

		++paramOutputs;
	}

	u32 operationCount = m_OperationCount;
	mvNodeStateDef::Operation* operations = m_Operations.Get<mvNodeStateDef::Operation>();
	for (u32 i = 0; i < operationCount; ++i)
	{
		resource->ReadShort(&operations->NodeIndex, 1);
		resource->ReadShort(&operations->TypeIndex, 1);
		resource->ReadShort(&operations->Size, 1);
		resource->ReadShort(&operations->Index, 1);

		mvNodeStateDef::Operator* op = (mvNodeStateDef::Operator*)(operations+1);
		u32 optype = 1;
		while (optype)
		{
			resource->ReadInt(&op->OperatorType, 1);
			resource->ReadInt(&op->Packet.ParameterCrc, 1);
			optype = op->OperatorType;
			if (optype == mvNodeStateDef::Operator::kFCurve)
			{
				// read in the data.
				mvUpdateParameterFromOperation::FCurve* fcurve =
					op->Packet.Data.Get<mvUpdateParameterFromOperation::FCurve>();
				resource->ReadFloat(&fcurve->ClampRange.Minimum, 1);
				resource->ReadFloat(&fcurve->ClampRange.Maximum, 1);
				resource->ReadInt(&fcurve->KeyframeCount, 1);
				resource->ReadInt((int*)(&fcurve->Keyframes), 1);
				mvUpdateParameterFromOperation::FCurve::Keyframe* keyframe =
					fcurve->Keyframes.Get<mvUpdateParameterFromOperation::FCurve::Keyframe>();
				for (u32 i = 0; i < fcurve->KeyframeCount; ++i)
				{
					resource->ReadInt(&keyframe->Type, 1);
					resource->ReadFloat(&keyframe->Key, 1);
					resource->ReadFloat(keyframe->Constants, 2);
					++keyframe;
				}

				op = (mvNodeStateDef::Operator*)keyframe;
			}
			else
			{
				++op;
			}
		}

		operations = (mvNodeStateDef::Operation*)op;
	}
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeStateDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeStateDef);
	u32 transitionCount = m_TransitionCount;
	const mvTransition* transition = m_Transitions.Get();
	for (u32 transitionIdx = 0; transitionIdx < transitionCount; ++transitionIdx) {
		size += transition->Size;
		transition = reinterpret_cast<const mvTransition*>(reinterpret_cast<const u8*>(transition) + transition->Size);
	}

	size += m_ParamInputCount*sizeof(Parameter);
	size += m_EventCount*sizeof(Event);
	size += m_ParamOutputCount*sizeof(Parameter);

	u32 operationCount = m_OperationCount;
	mvNodeStateDef::Operation* operations = m_Operations.Get<mvNodeStateDef::Operation>();
	for (u32 i = 0; i < operationCount; ++i)
	{
		size += sizeof(mvNodeStateDef::Operation);

		mvNodeStateDef::Operator* op = (mvNodeStateDef::Operator*)(operations+1);
		u32 optype = 1;
		while (optype)
		{
			optype = op->OperatorType;
			if (optype == mvNodeStateDef::Operator::kFCurve)
			{
				size += sizeof(mvNodeStateDef::Operator);
				size += sizeof(mvUpdateParameterFromOperation::FCurve);
				mvUpdateParameterFromOperation::FCurve* fcurve =
					op->Packet.Data.Get<mvUpdateParameterFromOperation::FCurve>();
				size += sizeof(mvUpdateParameterFromOperation::FCurve::Keyframe)*fcurve->KeyframeCount;
				op = (mvNodeStateDef::Operator*)(fcurve->Keyframes.Get<mvUpdateParameterFromOperation::FCurve::Keyframe>() + fcurve->KeyframeCount);
			}
			else
			{
				size += sizeof(mvNodeStateDef::Operator);
				++op;
			}
		}

		operations = (mvNodeStateDef::Operation*)op;
	}

	return size;
}

//////////////////////////////////////////////////////////////////////////

void mvNodeStateDef::RegisterParameterLookupHandlers(mvMotionWeb* web, crmtNode** children) const
{
	Parameter* parameters = m_ParamInputs.Get<mvNodeStateDef::Parameter>();
	int count = m_ParamInputCount;

	for (int i = 0; i < count; ++i)
	{
		u32 crc = parameters->ParameterCrc;
		u16 idx = parameters->NodeIndex;
		u16 type = parameters->TypeIndex;
		u32 param = parameters->ParameterIndex;
		++parameters;

		crmtNode* node = children[idx];

		const mvNode::NodeTypeInfo* info = mvNode::sm_NodeTypeInfos[node->GetNodeType()];
		mvNode::SetParameterFunc* func = info->m_ResolveParameterSetFn(type);

		mvParameter::ParamType paramType = info->m_ResolveParameterSetTypeFn(type);

		mvSetFromParameterBuffer* observer = rage_new mvSetFromParameterBuffer;
		observer->Init(web, crc, func, param, paramType);
		observer->Attach(*node);
	}
}

//////////////////////////////////////////////////////////////////////////

void mvNodeStateDef::RegisterParameterUpdateHandlers(mvMotionWeb* web, crmtNode** children) const
{
	Parameter* parameters = m_ParamOutputs.Get<mvNodeStateDef::Parameter>();
	int count = m_ParamOutputCount;

	for (int i = 0; i < count; ++i)
	{
		u32 crc = parameters->ParameterCrc;
		u16 idx = parameters->NodeIndex;
		u16 type = parameters->TypeIndex;
		u32 param = parameters->ParameterIndex;
		++parameters;

		crmtNode* node = children[idx];

		if (type != mvNodeStateDef::Parameter::kOutputIndex)
		{
			const mvNode::NodeTypeInfo* info = mvNode::sm_NodeTypeInfos[node->GetNodeType()];
			mvNode::GetParameterFunc* func = info->m_ResolveParameterGetFn(type);
			mvUpdateParameterBuffer* observer = rage_new mvUpdateParameterBuffer;
			observer->Init(web, crc, func, param);
			observer->Attach(*node);
		}
		else
		{
			mvUpdateProxyParameter* observer = rage_new mvUpdateProxyParameter;
			observer->Init(web, crc);
			observer->Attach(*node);
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void mvNodeStateDef::RegisterOperatorHandlers(mvMotionWeb* web, crmtNode** children) const
{
	Operation* ops = m_Operations.Get<mvNodeStateDef::Operation>();
	int count = m_OperationCount;

	for (int i = 0; i < count; ++i)
	{
		u16 idx = ops->NodeIndex;
		u16 type = ops->TypeIndex;
		//u32 size = ops->Size;
		// this size isn't correct...

		mvNodeStateDef::Operator* operators = (mvNodeStateDef::Operator*)(ops+1);

		crmtNode* node = children[idx];

		const mvNode::NodeTypeInfo* info = mvNode::sm_NodeTypeInfos[node->GetNodeType()];
		mvNode::SetParameterFunc* func = info->m_ResolveParameterSetFn(type);
		FastAssert(func); // Likely an export error

		mvUpdateParameterFromOperation* observer = rage_new mvUpdateParameterFromOperation;
		observer->Init(web, operators, ops->Size, ops->Index, func);
		observer->Attach(*node);

		// this is a bit of a hack because operator export is horribly complex
		mvNodeStateDef::Operator* op = operators;
		u32 optype = 1;
		while (optype)
		{
			optype = op->OperatorType;
			if (optype == mvNodeStateDef::Operator::kFCurve)
			{
				mvUpdateParameterFromOperation::FCurve* fcurve =
					op->Packet.Data.Get<mvUpdateParameterFromOperation::FCurve>();
				mvUpdateParameterFromOperation::FCurve::Keyframe* keyframe =
					fcurve->Keyframes.Get<mvUpdateParameterFromOperation::FCurve::Keyframe>();
				op = (mvNodeStateDef::Operator*)(keyframe + fcurve->KeyframeCount);
			}
			else
			{
				++op;
			}
		}		

		ops = (mvNodeStateDef::Operation*)(op);
	}
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeStateDef::RegisterEventHandlers(mvMotionWeb* web, crmtNode** children) const
{
	Event* events = m_Events.Get<mvNodeStateDef::Event>();
	int count = m_EventCount;

	for (int i = 0; i < count; ++i)
	{
		u16 idx = events->NodeIndex;
		u16 type = events->TypeIndex;

		crmtNode* node = children[idx];

		const mvNode::NodeTypeInfo* info = mvNode::sm_NodeTypeInfos[node->GetNodeType()];
		u32 msg = info->m_ResolveEventFn(type);
		mvUpdateParameterFromEvent* observer = rage_new mvUpdateParameterFromEvent(msg);
		observer->Init(web, events->EventCrc);
		observer->Attach(*node);

		++events;
	}
}

//////////////////////////////////////////////////////////////////////////

void mvNodeStateDef::RegisterHandlers(mvMotionWeb* web, crmtNode** children) const
{
	RegisterParameterLookupHandlers(web, children);
	RegisterParameterUpdateHandlers(web, children);
	RegisterOperatorHandlers(web, children);
	RegisterEventHandlers(web, children);
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeStateDef::ResolveParameterSet(u16)
{
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeStateDef::ResolveParameterSetType(u16)
{
	return mvParameter::ParamType(~0U);
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeStateDef::ResolveParameterGet(u16)
{
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeStateDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_REGISTERED_TYPE(mvNodeTail, mvNode::kNodeTail, mvNodeTailDef);

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeTailDef);

///////////////////////////////////////////////////////////////////////////////

crmtNode* mvNodeTailDef::Create(mvCreateContext* context) const
{
	crmtNodeProxy* proxy = crmtNodeProxy::CreateNode(*context->m_MotionTree);
	proxy->Init(context->m_Tail);
	return proxy;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeTailDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeTailDef::QueryMemorySize() const
{
	return sizeof(mvNodeTailDef);
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeTailDef::ResolveParameterSet(u16)
{
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeTailDef::ResolveParameterSetType(u16)
{
	return mvParameter::ParamType(~0U);
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeTailDef::ResolveParameterGet(u16)
{
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeTailDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace rage
