//
// move/move.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move.h"

#include "move_memorysystem.h"
#include "move_node.h"

#include "atl/array.h"
#include "cranimation/animation.h"
#include "cranimation/framefilters.h"
#include "cranimation/weightmodifier.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeblend.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "crskeleton/skeleton.h"

namespace rage {

using namespace mvMemorySystem;

int mvMove::sm_MemoryBucket=1;

mvAnimationLoader g_mvAnimationLoader;
mvClipLoader g_mvClipLoader;
mvExpressionLoader g_mvExpressionLoader;
mvParameterizedMotionLoader g_mvParameterizedMotionLoader;
mvNetworkDefLoader g_mvNetworkDefLoader;

#if CR_DEV
static const crAnimation* AllocateAndLoadAnimation(const char* id, const mvMotionWeb*)
{
	return crAnimation::AllocateAndLoad(id);
}

static const crClip* AllocateAndLoadClip(u32 /*clipContext*/, u32 /*clipParamA*/, u32 /*clipParamB*/, const mvMotionWeb*)
{
	Assertf(false, "Default version of AllocateAndLoadClip is intentionally not implemented!");
	return NULL;
}

static const crExpressions* AllocateAndLoadExpressions(u32 /*expressionDict*/, u32 /*expression*/, const mvMotionWeb*)
{
	Assertf(false, "Default version of AllocateAndLoadExpressions is intentionally not implemented!");
	return NULL;
}

static crpmParameterizedMotion* AllocateAndLoadParameterizedMotion(const char* id, const mvMotionWeb*)
{
	return crpmParameterizedMotion::AllocateFromData(*crpmParameterizedMotionData::AllocateAndLoad(id));
}
#endif // CR_DEV

static crFrameFilterMultiWeight* AllocateFrameFilterMultiWeight(u32 /*filterDict*/, u32 /*filter*/, const mvMotionWeb*)
{
	Assertf(false, "Default version of AllocateFrameFilterMultiWeight is intentionally not implemented!");
	return NULL;
}

mvFilterAllocator g_mvFilterAllocator = MakeFunctorRet(AllocateFrameFilterMultiWeight);

static mvTransitionFrameFilter* AllocateTransitionFrameFilter(u32 /*filterDict*/, u32 /*filter*/, const mvMotionWeb*)
{
	Assertf(false, "Default version of AllocateTransitionFrameFilter is intentionally not implemented!");
	return NULL;
}

mvTransitionFilterAllocator g_mvTransitionFilterAllocator = MakeFunctorRet(AllocateTransitionFrameFilter);

crFrameFilterMultiWeight* alloc_filter()
{
	return NULL;
}

PARAM(refoutput, "[move] Enable AddRef/Remove and AddKnownRef/RemoveKnownRef in move output buffer");
bool g_mvRefOutputs = true;

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Any one time initialization of move stuff should go in here
void mvMove::InitClass(InitParams* params, const mvAnimationLoader* animLoader, const mvClipLoader* clipLoader, const mvExpressionLoader* exprLoader, const mvParameterizedMotionLoader* pmLoader, const mvFilterAllocator* filterAllocator, const mvTransitionFilterAllocator* transitionFilterAllocator, const mvNetworkDefLoader* networkDefLoader)
{
	g_mvRefOutputs = !PARAM_refoutput.Get();

	mvNode::InitClass();
	//mvOperator::InitClass();
	mvTransitionFrameFilter::InitClass();

	InitializeMemorySystem(params->PageMemBuffer, params->PageMemBufferSize);

#if CR_DEV
	g_mvAnimationLoader = MakeFunctorRet(AllocateAndLoadAnimation);
	g_mvClipLoader = MakeFunctorRet(AllocateAndLoadClip);
	g_mvExpressionLoader = MakeFunctorRet(AllocateAndLoadExpressions);
	g_mvParameterizedMotionLoader = MakeFunctorRet(AllocateAndLoadParameterizedMotion);
#endif // CR_DEV

	if (animLoader) {
		g_mvAnimationLoader = *animLoader;
	}

	if (clipLoader) {
		g_mvClipLoader = *clipLoader;
	}

	if (exprLoader) {
		g_mvExpressionLoader = *exprLoader;
	}

	if (pmLoader) {
		g_mvParameterizedMotionLoader = *pmLoader;
	}

	if (filterAllocator) {
		g_mvFilterAllocator = *filterAllocator;
	}

	if (transitionFilterAllocator) {
		g_mvTransitionFilterAllocator = *transitionFilterAllocator;
	}

	if( networkDefLoader)
	{
		g_mvNetworkDefLoader = *networkDefLoader;
	}
}

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Any one time shutdown of move stuff should go in here
void mvMove::Shutdown()
{
	ShutdownMemorySystem();
}

///////////////////////////////////////////////////////////////////////////////

Functor0 g_mvPauseChanged = NULL;
Functor0 g_mvSingleStep = NULL;
Functor1<float> g_mvTimeScaleChanged = NULL;

void mvMove::RegisterPauseChanged(Functor0 func)
{
	g_mvPauseChanged = func;
}

void mvMove::RegisterSingleStep(Functor0 func)
{
	g_mvSingleStep = func;
}

void mvMove::RegisterTimeScaleChanged(Functor1<float> func)
{
	g_mvTimeScaleChanged = func;
}

///////////////////////////////////////////////////////////////////////////////

class mvTransitionObserver : public crmtObserver
{
public:
	mvTransitionObserver(mvTransitionFrameFilter* filter)
		:	m_Filter (filter)
	{
		Assert(m_Filter);
		filter->AddRef();
	}

	virtual void ReceiveMessage(const crmtMessage& msg)
	{
		crmtObserver::ReceiveMessage(msg);

		switch (msg.GetMsgId()) {
			case crmtNode::kMsgUpdate:
				{
					Assert(GetNode()->GetNodeType() == crmtNode::kNodeBlend);
					crmtNodeBlend* blend = (crmtNodeBlend*)GetNode();
					m_Filter->Update(blend->GetWeight());
				}
				break;
			case crmtNode::kMsgDetach:
				{
					m_Filter->Release();
					m_Filter = NULL;
				}
				break;
		}
	}

	virtual void ResetMessageIntercepts(crmtInterceptor& interceptor)
	{
		crmtObserver::ResetMessageIntercepts(interceptor);

		interceptor.RegisterMessageIntercept(crmtNode::kMsgUpdate);
		interceptor.RegisterMessageIntercept(crmtNode::kMsgDetach);
	}

	MOVE_DECLARE_ALLOCATOR();
private:
	mvTransitionFrameFilter* m_Filter;
};

///////////////////////////////////////////////////////////////////////////////

void mvTransitionFrameFilter::Update(float UNUSED_PARAM(weight))
{
	// do nothing. derive off this bad boy to update the filter
}

///////////////////////////////////////////////////////////////////////////////

void mvTransitionFrameFilter::Attach(crmtNodeBlend* node)
{
	mvTransitionObserver* observer = rage_new mvTransitionObserver(this);
	node->AddObserver(*observer);
	m_Observer = observer;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace rage
