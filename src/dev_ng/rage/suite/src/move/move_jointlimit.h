//
// move/move_jointlimit.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_JOINTLIMIT_H
#define MOVE_JOINTLIMIT_H

#include "move_node.h"

#include "crmotiontree/nodejointlimit.h"

namespace rage {

class crFrameFilter;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create a joint limit node
class mvNodeJointLimitDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this joint limit node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeJointLimit* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeJointLimitDef);

private:
	enum { kFilterMask = 3 };

	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES: Flag layout is:
	//	Filter - flags & 0x00000003
	u32 m_flags;

	// PURPOSE: Offset to input nodes description data
	Offset m_input;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move joint limit node
class mvNodeJointLimit : public crmtNodeJointLimit
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_FILTER_SET(T, ApplyFilter)
	
	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeJointLimit);

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeJointLimit);
};

//////////////////////////////////////////////////////////////////////////

inline void mvNodeJointLimit::ApplyFilter(crFrameFilter* filter)
{
	crmtNodeJointLimit::SetFilter(filter);
}


} // namespace rage

#endif // MOVE_JOINTLIMIT_H
