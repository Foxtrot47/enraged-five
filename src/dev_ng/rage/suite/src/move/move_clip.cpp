//
// move/clip.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_clip.h"

#include "move_parameterbuffer.h"
#include "move_macros.h"
#include "move_parameter.h"
#include "move_state.h"
#include "move/move_subnetwork.h"
#include "move_types.h"
#include "move_types_internal.h"

#include "crclip/clip.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tag.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeblend.h"
#include "crmotiontree/nodeblendn.h"
#include "crmotiontree/nodeclip.h"
#include "zlib/zlib.h"

//#include "system/timemgr.h"

namespace rage {

extern mvClipLoader g_mvClipLoader;

//////////////////////////////////////////////////////////////////////////

unsigned mvNodeClipDef::GetClipFlagFrom(u32 flags)
{
	return flags & 0x00000003;
}

unsigned mvNodeClipDef::GetPhaseFlagFrom(u32 flags)
{
	return (flags & 0x0000000c) >> 0x02;
}

unsigned mvNodeClipDef::GetRateFlagFrom(u32 flags)
{
	return (flags & 0x00000030) >> 0x04;
}

unsigned mvNodeClipDef::GetDeltaFlagFrom(u32 flags)
{
	return (flags & 0x000000c0) >> 0x06;
}

unsigned mvNodeClipDef::GetLoopedFlagFrom(u32 flags)
{
	return (flags & 0x00000300) >> 0x08;
}

unsigned mvNodeClipDef::GetIsZeroWeightFlagFrom(u32 flags)
{
	return (flags & 0x00000c00) >> 0x0A;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeClipDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeClip* mvNodeClipDef::Create(mvCreateContext* context) const
{
	mvNodeClip* node = mvNodeClip::CreateNode(*context->m_MotionTree);
	crClipPlayer& player = node->GetClipPlayer();

	const mvParameterBuffer* params = context->m_Parameters;
	const mvMotionWeb* network = context->m_Network;

	u32 flags = m_flags;

	unsigned cflag = GetClipFlagFrom(flags);
	unsigned pflag = GetPhaseFlagFrom(flags);
	unsigned rflag = GetRateFlagFrom(flags);
	unsigned dflag = GetDeltaFlagFrom(flags);
	unsigned lflag = GetLoopedFlagFrom(flags);
	unsigned isZeroWeight = GetIsZeroWeightFlagFrom(flags);

	const void* block = this+1;

	// clip
	switch (cflag)
	{
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetClipUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterClip, &ret);
			const crClip* clip = param->GetClip();
			player.SetClip(clip);
		}
		break;
	case mvConstant::kFlagValue:
		{
			u32 clipContext = ReadUInt32Block();
			u32 clipParamA = ReadUInt32Block();
			u32 clipParamB = 0;
			if(clipContext != kClipContextLocalFile)
			{
				clipParamB = ReadUInt32Block();
			}
			const crClip *clip = MV_REQUISITION_CLIP(clipContext, clipParamA, clipParamB, network);
			player.SetClip(clip);
			if(clip != NULL)
			{
				clip->Release();
			}
		}
		break;
	}

	// phase
	switch (pflag)
	{
	case mvConstant::kFlagValue:
		{
			float phase = ReadFloatBlock();
			const crClip* clip = player.GetClip();
			if (clip != NULL)
			{
				player.SetTime(clip->ConvertPhaseToTime(phase));
			}
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			const crClip* clip = player.GetClip();
			if (clip != NULL)
			{
				mvParameter ret; ret.SetReal(0.f);
				const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
				float phase = param->GetReal();
				player.SetTime(clip->ConvertPhaseToTime(phase));
			}
		}
		break;
	}

	// rate
	switch (rflag)
	{
	case mvConstant::kFlagValue:
		{
			float rate = ReadFloatBlock();
			player.SetRate(rate);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(1.f);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			float rate = param->GetReal();
			player.SetRate(rate);
		}
		break;
	}

	// delta
	switch (dflag)
	{
	case mvConstant::kFlagValue:
		{
			float delta = ReadFloatBlock();
			player.SetDelta(delta);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(0.f);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			float delta = param->GetReal();
			player.SetDelta(delta);
		}
		break;
	}

	// looped
	switch (lflag)
	{
	case mvConstant::kFlagValue:
		{
			bool looped = ReadBoolBlock();
			player.SetLooped(true, looped);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetBool(true);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterBoolean, &ret);
			bool looped = param->GetBool();
			player.SetLooped(true, looped);
		}
		break;
	}

	if (isZeroWeight)
	{
		node->SetZeroWeight(true);
	}

#if HACK_GTA4 && !NEW_MOVE_TAGS

	// GSALES - TEMPORARY! generic tag hack for exposing events on clips (the tool work for Dan's changes has taken a back seat
	//	until the new style tags are brought down from the head). We'll remove this as soon as we've integrated down the new
	//	tags and done the tool work to support it.

	mvUpdateEventFromClipTag* observer = rage_new mvUpdateEventFromClipTag;
	observer->Init( context->m_Network, kTrackFirstReservedForProjectUse + 1 );
	observer->Attach(*node);

#else //HACK_GTA4 && !NEW_MOVE_TAGS

	mvUpdateParameterFromTag* observer = rage_new mvUpdateParameterFromTag(crmtNodeClip::kMsgClipTagEnter, crmtNodeClip::kMsgClipTagExit, crmtNodeClip::kMsgClipTagUpdate);
	observer->Init(context->m_Network);
	observer->Attach(*node);

#endif // HACK_GTA4 && !NEW_MOVE_TAGS

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeClipDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_flags, 1);

	void* block = this + 1;
	u32 flags = m_flags;

	unsigned cflag = GetClipFlagFrom(flags);
	MV_VALIDATE(cflag < mvConstant::kFlagCount, "(mvNodeClipDef) clip flag out of range");
	unsigned pflag = GetPhaseFlagFrom(flags);
	MV_VALIDATE(pflag < mvConstant::kFlagCount, "(mvNodeClipDef) phase flag out of range");
	unsigned rflag = GetRateFlagFrom(flags);
	MV_VALIDATE(rflag < mvConstant::kFlagCount, "(mvNodeClipDef) rate flag out of range");
	unsigned dflag = GetDeltaFlagFrom(flags);
	MV_VALIDATE(dflag < mvConstant::kFlagCount, "(mvNodeClipDef) delta flag out of range");
	unsigned lflag = GetLoopedFlagFrom(flags);
	MV_VALIDATE(lflag < mvConstant::kFlagCount, "(mvNodeClipDef) looped flag out of range");

	// clip
	MV_CLIP_BLOCK_INIT(block, resource, cflag);
	// phase
	MV_REAL_BLOCK_INIT_FLAG(block, resource, pflag);
	// rate
	MV_REAL_BLOCK_INIT_FLAG(block, resource, rflag);
	// delta
	MV_REAL_BLOCK_INIT_FLAG(block, resource, dflag);
	// looped
	MV_BOOLEAN_BLOCK_INIT_FLAG(block, resource, lflag);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeClipDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeClipDef);

	const void* block = this + 1;
	u32 flags = m_flags;

	unsigned cflag = GetClipFlagFrom(flags);
	unsigned pflag = GetPhaseFlagFrom(flags);
	unsigned rflag = GetRateFlagFrom(flags);
	unsigned dflag = GetDeltaFlagFrom(flags);
	unsigned lflag = GetLoopedFlagFrom(flags);

	// clip
	MV_CLIP_BLOCK_SIZE(block, cflag, size);
	// phase
	MV_REAL_BLOCK_SIZE_FLAG(block, pflag, size);
	// rate
	MV_REAL_BLOCK_SIZE_FLAG(block, rflag, size);
	// delta
	MV_REAL_BLOCK_SIZE_FLAG(block, dflag, size);
	// looped
	MV_BOOLEAN_BLOCK_SIZE_FLAG(block, lflag, size);

	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeClipDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeClip::ApplyClip),
		MOVE_MAKE_SIGNAL(mvNodeClip::ApplyPhase),
		MOVE_MAKE_SIGNAL(mvNodeClip::ApplyRate),
		MOVE_MAKE_SIGNAL(mvNodeClip::ApplyDelta),
		MOVE_MAKE_SIGNAL(mvNodeClip::ApplyLooped)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeClipDef::ResolveParameterSetType(u16 parameter)
{
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterClip,
		mvParameter::kParameterReal,
		mvParameter::kParameterReal,
		mvParameter::kParameterReal,
		mvParameter::kParameterBoolean
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeClipDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeClip::GetClip),
		MOVE_MAKE_SIGNAL(mvNodeClip::GetPhase),
		MOVE_MAKE_SIGNAL(mvNodeClip::GetRate),
		MOVE_MAKE_SIGNAL(mvNodeClip::GetDelta),
		MOVE_MAKE_SIGNAL(mvNodeClip::GetLooped),
		MOVE_MAKE_SIGNAL(mvNodeClip::GetProperty)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeClipDef::ResolveEvent(u16 event)
{
	static u32 events[] = {
		crmtNodeClip::kMsgClipLooped,
		crmtNodeClip::kMsgClipEnded,
		crmtNodeClip::kMsgClipTagEnter,
		crmtNodeClip::kMsgClipTagExit,
		crmtNodeClip::kMsgClipTagUpdate,
	};

	Assert(event < NELEM(events));
	return events[event];
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeClip, crmtNodeClip, mvNode::kNodeClip, mvNodeClipDef);

//////////////////////////////////////////////////////////////////////////

#if HACK_GTA4 && !NEW_MOVE_TAGS

void mvUpdateEventFromClipTag::ReceiveMessage(const crmtMessage& msg)
{
	// Until we have the ability to specify these via data in the move tool we need to define project tags here.

	static const crProperty::Key moveEventKey = crProperty::CalcKey("MoveEvent", 0x7EA9DFC4);

	static const crProperty::Key footKey = crProperty::CalcKey("Foot", 0xB82E430B); // Heel and Right
	static const crProperty::Key heelKey = crProperty::CalcKey("Heel", 0x2BC03824);
	static const crProperty::Key rightKey = crProperty::CalcKey("Right", 0xB8CCC339);

	static const crProperty::Key lookIkKey = crProperty::CalcKey("LookIk", 0x4977AB78);
	static const crProperty::Key lookIkControlKey = crProperty::CalcKey("LookIkControl", 0x1EFF20B5);
	static const crProperty::Key armsIkKey = crProperty::CalcKey("ArmsIk", 0x540c5a2f);
	static const crProperty::Key legsIkKey = crProperty::CalcKey("LegsIk", 0xd56454b0);
	static const crProperty::Key legsIkOptionsKey = crProperty::CalcKey("LegsIkOptions", 4005337966);
	static const crProperty::Key selfieHeadControlKey = crProperty::CalcKey("SelfieHeadControl",0x378904C7);
	static const crProperty::Key selfieLeftArmControlKey = crProperty::CalcKey("SelfieLeftArmControl",0xE61C5647);

	static const crProperty::Key gestureControlKey = crProperty::CalcKey("GestureControl", 0x11f0c2d9);

	static const crProperty::Key applyForceKey = crProperty::CalcKey("ApplyForce", 0x6a287745);
	static const crProperty::Key blendOutKey = crProperty::CalcKey("BlendOutWithDuration", 0xe8703510);

	static const crProperty::Key branchGetupKey = crProperty::CalcKey("BranchGetup", 3140000743);

	static const crProperty::Key facialKey = crProperty::CalcKey("Facial", 0x511E56C2);
	
	static const crProperty::Key scriptVisible = crProperty::CalcKey("VisibleToScript", 0xF301E135);
	static const crProperty::Key audioKey = crProperty::CalcKey("Audio", 0xB80C6AE8);
	static const crProperty::Key relevantWeightThresholdKey = crProperty::CalcKey("RelevantWeightThreshold", 0x368d9c33);
	static const crProperty::Key loopingAudioKey = crProperty::CalcKey("LoopingAudio", 0xA31D8F23);

	static const crProperty::Key flashlightKey = crProperty::CalcKey("FlashLight", 0x630AA491 );
	static const crProperty::Key blockRagdollKey = crProperty::CalcKey("BlockRagdoll", 0xDE53B1C9 );
	
	static const crProperty::Key enablePhysicsDrivenJointKey = crProperty::CalcKey("EnablePhysicsDrivenJoint", 0x78c21fd4);

	static const crProperty::Key cameraShakeKey = crProperty::CalcKey("CameraShake", 0xe34a0cb0);

	static const crProperty::Key padShakeKey = crProperty::CalcKey("PadShake", 0x44ad78be);

	static const crProperty::Key firstPersonCameraKey = crProperty::CalcKey("FirstPersonCamera", 0xaff55091);
	static const crProperty::Key firstPersonCameraInputKey = crProperty::CalcKey("FirstPersonCameraInput", 0xc261b2d3);

	static const crProperty::Key motionBlurInputKey = crProperty::CalcKey("MotionBlur", 0xfdbaead5);

	static const crProperty::Key firstPersonHelmetCullKey = crProperty::CalcKey("FirstPersonHelmetCull", 0xae6df4aa);
	
	static const u32 footHeelIds[] = { ATSTRINGHASH("AEF_FOOT_HEEL_L",0x1C2B5199), ATSTRINGHASH("AEF_FOOT_HEEL_R",0xD97C4C50)};

	float fRelevantWeightThreshold = 0.01f;

	switch(msg.GetMsgId())
	{
	case mvObserver::kMsgDetachUpdaters:
		{
			Detach();
		}
		break;

	case crmtNodeClip::kMsgClipTagEnter:
		{
			mvParameterBuffer* params = m_Web->GetInternalParameters();
			crmtNodeClip::TagMsgPayload* payload = reinterpret_cast<crmtNodeClip::TagMsgPayload*>(msg.GetPayload());
			const crProperty& prop = payload->m_Tag->GetProperty();

			// If the tag contains an Audio property
			if(prop.GetKey() == audioKey)
			{
				const crPropertyAttribute* attrib = prop.GetAttribute(relevantWeightThresholdKey);
				if(attrib && attrib->GetType() == crPropertyAttribute::kTypeFloat)
				{
					// These tags are inserted to the buffer if the relative weight of the clip node is > than the relevantWeightThreshold attribute
					const crPropertyAttributeFloat* attribFloat = static_cast<const crPropertyAttributeFloat*>(attrib);
					float clipWeightOut;
					if (!IsClipNodeRelevant(attribFloat->GetFloat(), clipWeightOut, kRelevantToSubnetwork))
					{
						//Printf("%d - mvUpdateEventFromClipTag::ReceiveMessage - EARLY OUT - %s, %f, (%u), %s, %f !> %f\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, prop.GetKey(), "(audioKey & kMsgClipTagEnter)", clipWeightOut, attribFloat->GetFloat());
						return;
					}
				}

				// If the property contains the AudioEvent attribute, we need to expose it via the param buffer
				params->Insert(prop.GetKey(), &prop);
				//Printf("%d - mvUpdateEventFromClipTag::ReceiveMessage - INSERT - %s, %f, (%u)%s, %f > %f\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, prop.GetKey(), "audioKey & kMsgClipTagEnter", clipWeightOut, fRelevantWeightThreshold);
			}
		}
		break;

	case crmtNodeClip::kMsgClipTagUpdate:
		{
			mvParameterBuffer* params = m_Web->GetInternalParameters();
			crmtNodeClip::TagMsgPayload* payload = reinterpret_cast<crmtNodeClip::TagMsgPayload*>(msg.GetPayload());
			const crProperty& prop = payload->m_Tag->GetProperty();			

			//
			// These tags are inserted to the buffer regardless of the relative weight of the clip node
			//

			// If the property key is VisibleToScript
			if(prop.GetKey() == scriptVisible)
			{
				params->Insert(prop.GetKey(), &prop);
			}			
			// If the tag contains a LoopingAudio property
			else if(prop.GetKey() == loopingAudioKey)
			{
				const crPropertyAttribute* attrib = prop.GetAttribute(relevantWeightThresholdKey);
				if(attrib && attrib->GetType() == crPropertyAttribute::kTypeFloat)
				{
					// These tags are inserted to the buffer if the relative weight of the clip node is > than the data driven relevantWeightThreshold attribute
					const crPropertyAttributeFloat* attribFloat = static_cast<const crPropertyAttributeFloat*>(attrib);
					float clipWeightOut;
					if (!IsClipNodeRelevant(attribFloat->GetFloat(), clipWeightOut, kRelevantToSubnetwork))
					{
						//Printf("%d - mvUpdateEventFromClipTag::ReceiveMessage - EARLY OUT - %s, %f, (%u), %s, %f !> %f\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, prop.GetKey(), "(loopingAudioKey & kMsgClipTagUpdate)", clipWeightOut, attribFloat->GetFloat());
						return;
					}
				}

				params->Insert(prop.GetKey(), &prop);
				//Printf("%d - mvUpdateEventFromClipTag::ReceiveMessage - INSERT - %s, %f, (%u)%s, %f > %f\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, prop.GetKey(), "loopingAudioKey & kMsgClipTagUpdate", clipWeightOut, fRelevantWeightThreshold);

				return;
			}
			else if(prop.GetKey() == facialKey || prop.GetKey() == moveEventKey)
			{
				const crPropertyAttribute* attrib = prop.GetAttribute(relevantWeightThresholdKey);
				if(attrib && attrib->GetType() == crPropertyAttribute::kTypeFloat)
				{
					// These tags are inserted to the buffer if the relative weight of the clip node is > than the data driven relevantWeightThreshold attribute
					const crPropertyAttributeFloat* attribFloat = static_cast<const crPropertyAttributeFloat*>(attrib);
					fRelevantWeightThreshold = attribFloat->GetFloat();
				}
			}

			// check if the relative weight of the clip node is > relevantWeightThreshold
			float clipWeightOut;
			if (!IsClipNodeRelevant(fRelevantWeightThreshold, clipWeightOut, kRelevantToState))
			{
				//Printf("%d - mvUpdateEventFromClipTag::ReceiveMessage - EARLY OUT - %s, %f, (%u), %f !> %f\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, prop.GetKey(), clipWeightOut, fRelevantWeightThreshold);
				return;
			}

			//
			// These tags are inserted to the buffer if the relative weight clip node is > relevantWeightThreshold
			//

			if(prop.GetKey() == moveEventKey)
			{
				const crPropertyAttribute* attrib = prop.GetAttribute(moveEventKey);
				if(attrib && attrib->GetType() == crPropertyAttribute::kTypeHashString)
				{
					const crPropertyAttributeHashString* attribHashString = static_cast<const crPropertyAttributeHashString*>(attrib);
					if (attribHashString->GetHashString().GetHash())
					{
						params->Insert(attribHashString->GetHashString().GetHash(), true);
					}

					//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, eventName ? eventName : "unknown", hashKey);
				}
			}
			else if(prop.GetKey() == footKey)
			{
				const crPropertyAttribute* attribHeel = prop.GetAttribute(heelKey);
				if(attribHeel && attribHeel->GetType() == crPropertyAttribute::kTypeBool)
				{
					const crPropertyAttribute* attribRight = prop.GetAttribute(rightKey);
					if(attribRight && attribRight->GetType() == crPropertyAttribute::kTypeBool)
					{
						bool heel = static_cast<const crPropertyAttributeBool*>(attribHeel)->GetBool();
						bool right = static_cast<const crPropertyAttributeBool*>(attribRight)->GetBool();
						if (heel)
						{
							params->Insert(footHeelIds[right], true);
						}
					
						//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, eventName, prop.GetKey());
					}
				}
			}
			else if(prop.GetKey() == lookIkKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, eventName, prop.GetKey());
			}
			else if(prop.GetKey() == lookIkControlKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, eventName, prop.GetKey());
			}
			else if(prop.GetKey() == armsIkKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, eventName, prop.GetKey());
			}
			else if(prop.GetKey() == legsIkKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, eventName, prop.GetKey());
			}
			else if(prop.GetKey() == legsIkOptionsKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, eventName, prop.GetKey());
			}
			else if((prop.GetKey() == selfieHeadControlKey) || (prop.GetKey() == selfieLeftArmControlKey))
			{
				params->Insert(prop.GetKey(), &prop);
			}
			else if(prop.GetKey() == gestureControlKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, "GestureControl", prop.GetKey());
			}
			else if(prop.GetKey() == applyForceKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, "ApplyForce", prop.GetKey());
			}
			else if(prop.GetKey() == blendOutKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, "BlendOutWithDuration", prop.GetKey());
			}
			else if(prop.GetKey() == facialKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %d, %s, %f, %u, %s(%u)\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, msg.GetMsgId(), "FacialKey", prop.GetKey());
			}
			else if(prop.GetKey() == branchGetupKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %d, %s, %f, %u, %s(%u)\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, msg.GetMsgId(), "BranchGetup", prop.GetKey());
			}
			else if(prop.GetKey() == flashlightKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %d, %s, %f, %u, %s(%u)\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, msg.GetMsgId(), "Flashlight", prop.GetKey());
			}
			else if(prop.GetKey() == blockRagdollKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %d, %s, %f, %u, %s(%u)\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, msg.GetMsgId(), "BlockRagdoll", prop.GetKey());
			}
			else if(prop.GetKey() == enablePhysicsDrivenJointKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %d, %s, %f, %u, %s(%u)\n", TIME.GetFrameCount(), payload->m_Clip->GetName(), payload->m_Phase, msg.GetMsgId(), "EnablePhysicsDrivenJointKey", prop.GetKey());
			}
			else if(prop.GetKey() == cameraShakeKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, "CameraShake", prop.GetKey());
			}
			else if(prop.GetKey() == padShakeKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, "PadShake", prop.GetKey());
			}
			else if(prop.GetKey() == firstPersonCameraKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, "FirstPersonCamera", prop.GetKey());
			}
			else if(prop.GetKey() == firstPersonCameraInputKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, "FirstPersonCameraInput", prop.GetKey());
			}
			else if (prop.GetKey() == motionBlurInputKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, "motionBlur", prop.GetKey());
			}
			else if (prop.GetKey() == firstPersonHelmetCullKey)
			{
				params->Insert(prop.GetKey(), &prop);
				//Printf("mvUpdateEventFromClipTag::ReceiveMessage - %s, %f, %s(%u)\n", payload->m_Clip->GetName(), payload->m_Phase, "FirstPersonHelmetCull", prop.GetKey());
			}
		}
		break;

	case crmtNodeClip::kMsgClipTagExit:
		{
		}
		break;
	default:
		{
			//do nothing
		}
		break;
	}
}

void mvUpdateEventFromClipTag::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	crmtObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(mvObserver::kMsgDetachUpdaters);
	interceptor.RegisterMessageIntercept(crmtNodeClip::kMsgClipTagUpdate);
	interceptor.RegisterMessageIntercept(crmtNodeClip::kMsgClipTagEnter);
	interceptor.RegisterMessageIntercept(crmtNodeClip::kMsgClipTagExit);
}

bool mvUpdateEventFromClipTag::IsClipNodeRelevant(float fRelevantWeightThreshold, float &compositeWeight, eRelevantTo relevantTo)
{
	if(fRelevantWeightThreshold <= 0.0f)
	{
		return true;
	}

	//walk up the state nodes checking blend weights till we hit a state node, or a weight is zero

	crmtNode* child = GetNode();
	crmtNode* parent = child->GetParent();

	compositeWeight = 1.0f;

	while (parent)
	{
		switch(parent->GetNodeType())
		{
		case crmtNode::kNodeBlend:
			{
				crmtNodeBlend* blend = static_cast<crmtNodeBlend*>(parent);
				
				if (blend->GetFilter())
				{
					// Ultimately we'll need to be more clever about this, (perhaps be able to specify in the tool?)
					// but for now if the blend has a filter on it we'll simply treat both weights as 1.0f
					// this is to stop us masking off important events when playing upper body events oover the top.
					
				}
				else
				{
					// multiply out the weight
					if (child==blend->GetFirstChild())
					{				
						compositeWeight *= 1.0f-blend->GetWeight();
					}
					else
					{
						compositeWeight *= blend->GetWeight();
					}
				}

			}
			break;

		case crmtNode::kNodeBlendN:
			{
				// multiply out the weight
				crmtNodeBlendN* blendn = static_cast<crmtNodeBlendN*>(parent);
	
				int childIndex = blendn->GetChildIndex(*child);
	
				compositeWeight *= blendn->GetWeight(childIndex);
			}
			break;

		case crmtNode::kNodeStateRoot:
		case crmtNode::kNodeStateMachine:
			{
				// early out here
				if (relevantTo==kRelevantToState)
					return true;
			}
			break;
		case crmtNode::kNodeSubNetwork:
			{
				// early out here
				if (relevantTo==kRelevantToSubnetwork)
					return true;
			}
			break;
		default:
			{
				//do nothing
			}

		}

		// if the weight within this state node is negligible, we should ignore this node
		if (compositeWeight < fRelevantWeightThreshold)
			return false;

		child = parent;
		parent = parent->GetParent();
	}

	return true;
}

#endif //HACK_GTA4 && !NEW_MOVE_TAGS

} // namespace rage
