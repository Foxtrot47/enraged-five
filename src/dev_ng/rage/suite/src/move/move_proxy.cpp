//
// move/move_proxy.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_proxy.h"

#include "move_parameterbuffer.h"
#include "move_parameter.h"

namespace rage {

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeProxyDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeProxy* mvNodeProxyDef::Create(mvCreateContext* context) const
{
	mvNodeProxy* node = mvNodeProxy::CreateNode(*context->m_MotionTree);

	const mvParameterBuffer* params = context->m_Parameters;

	u32 signal = m_signal;
	crmtNode* proxy = NULL;
	const mvParameter* param = params->FindFirstOfType(signal, mvParameter::kParameterObserver, NULL);
	if (param != NULL)
	{
		mvObserver* observer = param->GetObserver();
		proxy = observer->GetNode();
	}

	node->Init(proxy);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeProxyDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_signal, 1);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeProxyDef::QueryMemorySize() const
{
	return sizeof(mvNodeProxyDef);
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeProxyDef::ResolveParameterSet(u16)
{
	return MOVE_MAKE_SIGNAL(mvNodeProxy::ApplyNode);
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeProxyDef::ResolveParameterSetType(u16)
{
	return mvParameter::kParameterObserver;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeProxyDef::ResolveParameterGet(u16)
{
	return MOVE_MAKE_SIGNAL(mvNodeProxy::GetNode);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeProxyDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeProxy, crmtNodeProxy, mvNode::kNodeProxy, mvNodeProxyDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
