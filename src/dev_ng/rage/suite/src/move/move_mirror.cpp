//
// move/move_mirror.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_mirror.h"

#include "move_parameterbuffer.h"
#include "move_funcs.h"
#include "move_macros.h"
#include "move_network.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeMirrorDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeMirror* mvNodeMirrorDef::Create(mvCreateContext* context) const
{
	mvNodeMirror* node = mvNodeMirror::CreateNode(*context->m_MotionTree);

	u32 flags = m_flags;
	const void* block = this+1;

	const mvParameterBuffer* params = context->m_Parameters;
	const mvMotionWeb* network = context->m_Network;

	// filter
	crFrameFilter* filter = NULL;
	switch (flags & kFilterMask)
	{
	case mvConstant::kFlagValue:
		{
			// expected to be init'd with mvConstant::FILTER_COUNT & bone data added
			filter = mvAllocAndInitFilter(network, block);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetFilterUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterFilter, &ret);
			filter = param->GetFilter();
		}
		break;
	}

	node->Init(filter);

	mvNodeDef* input = m_input.Get<mvNodeDef>();
	crmtNode* child = input->Create(context);

	node->AddChild(*child);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeMirrorDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_flags, 1);

	m_input.Init(resource);

	void* block = this + 1;
	u32 flags = m_flags;

	// filter
	MV_FILTER_BLOCK_INIT(block, resource, flags & kFilterMask);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeMirrorDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeMirrorDef);

	const void* block = this+1;
	u32 flags = m_flags;

	// filter
	MV_FILTER_BLOCK_SIZE(block, (flags & kFilterMask), size);

	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeMirrorDef::ResolveParameterSet(u16)
{
	return MOVE_MAKE_SIGNAL(mvNodeMirror::ApplyFilter);
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeMirrorDef::ResolveParameterSetType(u16)
{
	return mvParameter::kParameterFilter;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeMirrorDef::ResolveParameterGet(u16)
{
	return MOVE_MAKE_SIGNAL(mvNodeMirror::GetFilter);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeMirrorDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeMirror, crmtNodeMirror, mvNode::kNodeMirror, mvNodeMirrorDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
