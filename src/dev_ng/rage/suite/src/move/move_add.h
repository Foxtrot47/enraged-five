//
// move/move_add.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_ADD_H
#define MOVE_ADD_H

#include "move_node.h"
#include "move_blend.h"

#include "crmotiontree/nodeaddsubtract.h"

namespace rage {

class crFrameFilter;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create an add node
class mvNodeAddSubtractDef : public mvNodePairDef
{
public:
	// PURPOSE: Create an instance of this add node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeAddSubtract* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read in to memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 type);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeAddSubtractDef);
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move add node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeAddSubtract : public crmtNodeAddSubtract
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_FILTER_SET(T, ApplyFilter) \
		MOVE_FILTER_GET(T, GetFilter) \
		MOVE_REAL_SET(T, ApplyWeight) \
		MOVE_REAL_GET(T, GetWeight)

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeAddSubtract)

	// PURPOSE: Don't polute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeAddSubtract);
};

//////////////////////////////////////////////////////////////////////////

inline void mvNodeAddSubtract::ApplyFilter(crFrameFilter* filter)
{
	crmtNodeAddSubtract::SetFilter(filter);
}

//////////////////////////////////////////////////////////////////////////

inline crFrameFilter* mvNodeAddSubtract::GetFilter() const
{
	return crmtNodeAddSubtract::GetFilter();
}

//////////////////////////////////////////////////////////////////////////

inline void mvNodeAddSubtract::ApplyWeight(float weight)
{
	crmtNodeAddSubtract::SetWeight(weight);
}

//////////////////////////////////////////////////////////////////////////

inline float mvNodeAddSubtract::GetWeight() const
{
	return crmtNodeAddSubtract::GetWeight();
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_ADD_H
