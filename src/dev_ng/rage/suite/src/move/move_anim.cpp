//
// move/animation.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_anim.h"

#include "move_parameterbuffer.h"
#include "move_macros.h"
#include "move_state.h"
#include "move_types.h"
#include "move_types_internal.h"

#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/synchronizer.h"

namespace rage {

extern mvAnimationLoader g_mvAnimationLoader;

//////////////////////////////////////////////////////////////////////////

struct mvNodeAnimationFlag
{
	unsigned Animation : 2;
	unsigned Phase : 2;
	unsigned Rate : 2;
	unsigned Delta : 2;
	unsigned Looped : 2;
	unsigned Absolute : 2;
	int pad : 20;
};

//////////////////////////////////////////////////////////////////////////

unsigned mvNodeAnimationDef::GetAnimationFlagFrom(u32 flags)
{
	return flags & 0x00000003;
}

unsigned mvNodeAnimationDef::GetPhaseFlagFrom(u32 flags)
{
	return (flags & 0x0000000c) >> 0x02;
}

unsigned mvNodeAnimationDef::GetRateFlagFrom(u32 flags)
{
	return (flags & 0x00000030) >> 0x04;
}

unsigned mvNodeAnimationDef::GetDeltaFlagFrom(u32 flags)
{
	return (flags & 0x000000c0) >> 0x06;
}

unsigned mvNodeAnimationDef::GetLoopedFlagFrom(u32 flags)
{
	return (flags & 0x00000300) >> 0x08;
}

unsigned mvNodeAnimationDef::GetAbsoluteFlagFrom(u32 flags)
{
	return (flags & 0x00000c00) >> 0x0a;
}

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeAnimationDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeAnimation* mvNodeAnimationDef::Create(mvCreateContext* context) const
{
	mvNodeAnimation* node = mvNodeAnimation::CreateNode(*context->m_MotionTree);
	crAnimPlayer& player = node->GetAnimPlayer();

	const mvParameterBuffer* params = context->m_Parameters;
	const mvMotionWeb* network = context->m_Network;

	u32 flags = m_Flags;

	unsigned animflag = GetAnimationFlagFrom(flags);
	unsigned phaseflag = GetPhaseFlagFrom(flags);
	unsigned rateflag = GetRateFlagFrom(flags);
	unsigned deltaflag = GetDeltaFlagFrom(flags);
	unsigned loopedflag = GetLoopedFlagFrom(flags);

	void* block = (void*)(this + 1);

	// anim init
	switch (animflag)
	{
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetAnimationUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterAnimation, &ret);
			const crAnimation* anim = param->GetAnimation();
			player.SetAnimation(anim);

			break;
		}
	case mvConstant::kFlagValue:
		{
			unsigned* len = (unsigned*)block;
			block = (u32*)block+1;
			const char* filename = (const char*)block;
			block = (char*)block+(*len);
			const crAnimation* anim = MV_REQUISITION_ANIMATION(filename, network);
			player.SetAnimation(anim);
			if (anim != NULL)
			{
				anim->Release();
			}
			break;
		}
	}

	// phase init
	switch (phaseflag)
	{
	case mvConstant::kFlagValue:
		{
			float phase = ReadFloatBlock();

			const crAnimation* anim = player.GetAnimation();
			if (anim != NULL)
			{
				player.SetTime(anim->ConvertPhaseToTime(phase));
			}
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();

			const crAnimation* anim = player.GetAnimation();
			if (anim != NULL)
			{
				mvParameter ret; ret.SetReal(0.f);
				const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
				float phase = param->GetReal();
				player.SetTime(anim->ConvertPhaseToTime(phase));
			}
		}
		break;
	}

	// rate init
	switch (rateflag)
	{
	case mvConstant::kFlagValue:
		{
			float rate = ReadFloatBlock();
			player.SetRate(rate);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(1.f);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			float rate = param->GetReal();
			player.SetRate(rate);
		}
		break;
	}

	// delta init
	switch (deltaflag)
	{
	case mvConstant::kFlagValue:
		{
			float delta = ReadFloatBlock();
			player.SetDelta(delta);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetReal(0.f);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
			float delta = param->GetReal();
			player.SetDeltaHiddenSupplement(delta);
		}
		break;
	}

	// looped init
	switch (loopedflag)
	{
	case mvConstant::kFlagValue:
		{
			bool looped = ReadBoolBlock();
			player.SetLooped(true, looped);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetBool(true);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterBoolean, &ret);
			bool looped = param->GetBool();
			player.SetLooped(true, looped);
		}
		break;
	}

	// absolute init
	block=((u32*)block)+1;

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeAnimationDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_Flags, 1);

	void* block = this + 1;
	u32 flags = m_Flags;

	unsigned anim = GetAnimationFlagFrom(flags);
	MV_VALIDATE(anim < mvConstant::kFlagCount, "(mvNodeAnimationDef) animation flag out of range");
	unsigned phase = GetPhaseFlagFrom(flags);
	MV_VALIDATE(phase < mvConstant::kFlagCount, "(mvNodeAnimationDef) phase flag out of range");
	unsigned rate = GetRateFlagFrom(flags);
	MV_VALIDATE(rate < mvConstant::kFlagCount, "(mvNodeAnimationDef) rate flag out of range");
	unsigned delta = GetDeltaFlagFrom(flags);
	MV_VALIDATE(delta < mvConstant::kFlagCount, "(mvNodeAnimationDef) delta flag out of range");
	unsigned looped = GetLoopedFlagFrom(flags);
	MV_VALIDATE(looped < mvConstant::kFlagCount, "(mvNodeAnimationDef) looped flag out of range");
	unsigned abs = GetAbsoluteFlagFrom(flags);
	MV_VALIDATE(abs < mvConstant::kFlagCount, "(mvNodeAnimationDef) absolute flag out of range");

	// animation
	MV_ANIMATION_BLOCK_INIT(block, resource, anim);
	// phase
	MV_REAL_BLOCK_INIT_FLAG(block, resource, phase);
	// rate
	MV_REAL_BLOCK_INIT_FLAG(block, resource, rate);
	// delta
	MV_REAL_BLOCK_INIT_FLAG(block, resource, delta);
	// looped
	MV_BOOLEAN_BLOCK_INIT_FLAG(block, resource, looped);
	// absolute
	MV_BOOLEAN_BLOCK_INIT_FLAG(block, resource, abs);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeAnimationDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeAnimationDef);

	const void* block = this + 1;
	u32 flags = m_Flags;

	unsigned anim = GetAnimationFlagFrom(flags);
	unsigned phase = GetPhaseFlagFrom(flags);
	unsigned rate = GetRateFlagFrom(flags);
	unsigned delta = GetDeltaFlagFrom(flags);
	unsigned looped = GetLoopedFlagFrom(flags);
	unsigned abs = GetAbsoluteFlagFrom(flags);

	// animation
	MV_ANIMATION_BLOCK_SIZE(block, anim, size);
	// phase
	MV_REAL_BLOCK_SIZE_FLAG(block, phase, size);
	// rate
	MV_REAL_BLOCK_SIZE_FLAG(block, rate, size);
	// delta
	MV_REAL_BLOCK_SIZE_FLAG(block, delta, size);
	// looped
	MV_BOOLEAN_BLOCK_SIZE_FLAG(block, looped, size);
	// absolute
	MV_BOOLEAN_BLOCK_SIZE_FLAG(block, abs, size);

	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeAnimationDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeAnimation::ApplyAnimation),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::ApplyPhase),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::ApplyRate),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::ApplyDelta),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::ApplyLooped),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::ApplyAbsolute)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeAnimationDef::ResolveParameterSetType(u16 parameter)
{
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterAnimation,
		mvParameter::kParameterReal,
		mvParameter::kParameterReal,
		mvParameter::kParameterReal,
		mvParameter::kParameterBoolean,
		mvParameter::kParameterBoolean,
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeAnimationDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeAnimation::GetAnimation),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::GetPhase),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::GetRate),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::GetDelta),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::GetLooped),
		MOVE_MAKE_SIGNAL(mvNodeAnimation::GetAbsolute)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeAnimationDef::ResolveEvent(u16 event)
{
	static u32 events[] = {
		crmtNodeAnimation::kMsgAnimationLooped,
		crmtNodeAnimation::kMsgAnimationEnded,
	};

	Assert(event < NELEM(events));
	return events[event];
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeAnimation, crmtNodeAnimation, mvNode::kNodeAnimation, mvNodeAnimationDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
