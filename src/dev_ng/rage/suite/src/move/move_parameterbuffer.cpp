//
// move/move_parameterbuffer.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_parameterbuffer.h"

#include "move_subnetwork.h"
#include "move_observer.h"

#include "cranimation/animation.h"
#include "crclip/clip.h"
#include "crextra/expressions.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "math/simplemath.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/memops.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

#if CR_STATS
	PF_PAGE(mvParameterBufferPage, "mv ParameterBuffer");
	PF_GROUP(mvParameterBufferStats);
	PF_LINK(mvParameterBufferPage, mvParameterBufferStats);

	PF_COUNTER(mvParameterBuffer_GenFree, mvParameterBufferStats);
	PF_COUNTER(mvParameterBuffer_GenFreeCompact, mvParameterBufferStats);
	PF_COUNTER(mvParameterBuffer_GenFreeAppend, mvParameterBufferStats);
	PF_COUNTER(mvParameterBuffer_AppendCompact, mvParameterBufferStats);

	PF_COUNTER_CUMULATIVE(mvParameterBuffer_ParamBuffers, mvParameterBufferStats);

	PF_COUNTER(mvParameterBuffer_BufferAlloc, mvParameterBufferStats);
	PF_COUNTER(mvParameterBuffer_BufferFree, mvParameterBufferStats);
	PF_COUNTER(mvParameterBuffer_MapAlloc, mvParameterBufferStats);
	PF_COUNTER(mvParameterBuffer_MapFree, mvParameterBufferStats);
#endif // CR_STATS

////////////////////////////////////////////////////////////////////////////////

mvParameterBuffer::mvParameterBuffer()
: m_Map(NULL)
, m_Count(0)
, m_Capacity(0)
, m_TypeMask(0)
, m_KeyMask(0)
, m_DefaultCapacity(sm_DefaultCapacity)
{
#if CR_STATS
	PF_INCREMENT(mvParameterBuffer_ParamBuffers);
#endif
}

////////////////////////////////////////////////////////////////////////////////

mvParameterBuffer::~mvParameterBuffer()
{
	Shutdown();
#if CR_STATS
	PF_INCREMENTBY(mvParameterBuffer_ParamBuffers, -1);
#endif
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Init(u16 reservedCapacity)
{
	Assert(!m_Capacity);
	Assert(reservedCapacity > 0);
	Assert(GetNextPow2(reservedCapacity-1) == reservedCapacity);

	reservedCapacity = Min(Max(reservedCapacity, sm_DefaultCapacity), Buffer::sm_MaxCapacity);

	m_Capacity = reservedCapacity;
	m_DefaultCapacity = reservedCapacity;

	if(m_Capacity > 0)
	{
		m_Buffers.Append() = Buffer::Allocate(reservedCapacity);
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Shutdown()
{
	Map::Free(m_Map);
	m_Map = NULL;

	// free all the buffers
	u32 numBuffers = m_Buffers.GetCount();
	for(u32 i=0; i < numBuffers; i++)
	{
		Buffer::Free(m_Buffers[i]);
	}
	m_Buffers.Resize(0);

	m_Count = 0;
	m_Capacity = 0;

	m_TypeMask = 0;
	m_KeyMask = 0;
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Reset()
{
	Map::Free(m_Map);
	m_Map = NULL;

	// free all except first buffer
	u32 numBuffers = m_Buffers.GetCount();
	if(numBuffers > 0)
	{
		for(u32 i=1; i<numBuffers; i++)
		{
			Buffer::Free(m_Buffers[i]);
		}
		m_Buffers.Resize(1);

		Buffer* firstBuffer = m_Buffers[0];
		m_Capacity = firstBuffer->m_Capacity;
		firstBuffer->Clear();
	}
	else
	{
		m_Capacity = 0;
	}
	m_Count = 0;

	m_TypeMask = 0;
	m_KeyMask = 0;
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Clear()
{
	if(m_Map)
	{
		Map::Free(m_Map);
	}
	m_Map = NULL;

	u32 numBuffers = m_Buffers.GetCount();
	if(numBuffers == 1)
	{
		m_Buffers[0]->Clear();
		m_Count = 0;

		m_TypeMask = 0;
		m_KeyMask = 0;
	}
	else if(numBuffers > 1)
	{
		// if there's more than one buffer, consolidate on next power of two
		u16 capacity = m_Capacity;
		Shutdown();
		Init(u16(GetNextPow2(capacity-1)));
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Finalize()
{
	Assert(!m_Map);

	if(m_Count > 8)
	{
		// with a capacity of 164, map will exceed 2k alloc limit
		if(m_Count >= 164)
		{
			// try and deduplicate on stack first, then compact to reduce count - not efficient, but exceptionally rare
			DeDuplicate();
			Compact();
		}

		m_Map = Map::Allocate(m_Count);

		GenerateMap(m_Map, false);  // TODO -- deduplicate here?
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::UnFinalize()
{
	if(m_Map)
	{
		Map::Free(m_Map);
		m_Map = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::DeDuplicate()
{
	Assert(!m_Map);

	if(m_Count > 1)
	{
		u8* buf = Alloca(u8, Map::CalcMemory(m_Count));
		Map* map = ::new (buf) Map(m_Count);

		GenerateMap(map, true);
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Compact()
{
	Assert(!m_Map);

	if(m_Buffers.GetCount() > 1)
	{
		u16 count = CalcCount();
		count = Max(count, m_DefaultCapacity);
		count = u16(GetNextPow2(count-1));

		Assert(count <= Buffer::sm_MaxCapacity);
		count = Min(count, Buffer::sm_MaxCapacity);

		Buffer* buf = Buffer::Allocate(count);

		for(int i=0; i<m_Buffers.GetCount(); ++i)
		{
			buf->Append(*m_Buffers[i]);
			Buffer::Free(m_Buffers[i]);
		}
		m_Buffers.Resize(0);

		m_Buffers.Append() = buf;
		m_Count = buf->m_Count;
		m_Capacity = buf->m_Capacity;

		Assert(m_Count <= m_Capacity);
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Append(mvParameterBuffer& pb)
{
	Assert(!m_Map);

	// check free slots for buffer append, if insufficient, compact buffers
	if((m_Buffers.GetCount() + pb.m_Buffers.GetCount()) > m_Buffers.GetMaxCount())
	{
		DeDuplicate();
		Compact();
#if CR_STATS
		PF_INCREMENT(mvParameterBuffer_AppendCompact);
#endif

		if((m_Buffers.GetCount() + pb.m_Buffers.GetCount()) > m_Buffers.GetMaxCount())
		{
			pb.DeDuplicate();
			pb.Compact();
#if CR_STATS
			PF_INCREMENT(mvParameterBuffer_AppendCompact);
#endif
		}
	}

	// appending hides access to any free slots remaining in current top buffer
	int count = m_Count;
	int capacity = m_Capacity;

	Buffer* top = NULL;
	int curr = m_Buffers.GetCount();
	if(curr > 0)
	{
		top = m_Buffers[curr-1];
		count += top->m_Capacity - top->m_Count;
	}

	// copy buffer pointers over
	int numToAdd = pb.m_Buffers.GetCount();
	m_Buffers.Resize(curr + numToAdd);
	for(int i=0; i<numToAdd; ++i)
	{
		top = pb.m_Buffers[i];
		capacity += top->m_Capacity;
		count += top->m_Capacity;
		m_Buffers[curr++] = top;
	}

	// re-adjust count as any free slots in new top buffer are available
	if(curr > 0)
	{
		count -= top->m_Capacity - top->m_Count;
	}

	Assert(count <= capacity);
	Assign(m_Count, count);
	Assign(m_Capacity, capacity);

	m_TypeMask |= pb.m_TypeMask;
	m_KeyMask |= pb.m_KeyMask;

	// reset buffer pointers, as ownership has been transfered, before performing normal reset operation
	pb.m_Buffers.Reset();
	Map::Free(pb.m_Map); 
	pb.m_Map = NULL;
	pb.m_Count = 0;
	pb.m_Capacity = 0;
	pb.m_TypeMask = 0;
	pb.m_KeyMask = 0;
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Move(mvParameterBuffer& pb)
{
	Assert(!m_Map);

	// transfer contents of passed in buffer, don't add or release refs (but do manage known refs)
	for(int i=0; i<pb.m_Buffers.GetCount(); ++i)
	{
		Buffer& buffer = *pb.m_Buffers[i];
		mvKeyedParameter* params = buffer.GetKeyedParameters();

#if RESTRICTED_PARAMTER_TYPES
		if(!buffer.m_Complex && m_Capacity > m_Count)
		{
			Buffer& dest = *m_Buffers.Top();
			if((dest.m_Capacity - dest.m_Count) >= buffer.m_Count)
			{
#if VALIDATE_COMPLEX_PARAMETERBUFFERS
				for(int i=0; i<buffer.m_Count; ++i)
				{
					mvKeyedParameter& kp = params[i];
					Assert((kp.Parameter.m_data == NULL) || !kp.Parameter.IsComplex());
				}
#endif // VALIDATE_COMPLEX_PARAMETERBUFFERS
				sysMemCpy(dest.GetKeyedParameters()+dest.m_Count, params, sizeof(mvKeyedParameter)*buffer.m_Count);
				dest.m_Count += buffer.m_Count;
				m_Count += buffer.m_Count;

				continue;
			}
		}
#endif // RESTRICTED_PARAMTER_TYPES

		if(m_Capacity > m_Count)
		{
			Buffer& dest = *m_Buffers.Top();
			if((dest.m_Capacity - dest.m_Count) >= buffer.m_Count)
			{
				mvKeyedParameter* pkp = params;
				mvKeyedParameter* pkpdest = dest.GetKeyedParameters()+dest.m_Count;
				for(int j=buffer.m_Count; j>0; --j)
				{
					if(pkp->Parameter.m_type != mvParameter::kParameterNone)
					{
						pkpdest->Key = pkp->Key;
						pkpdest->Parameter.m_type = pkp->Parameter.m_type;
						pkpdest->Parameter.m_data = pkp->Parameter.m_data;
						pkpdest->Parameter.AddKnownRef();

						pkp->Parameter.RemoveKnownRef();
						pkp->Parameter.m_type = mvParameter::kParameterData;

						++pkpdest;
						++dest.m_Count;
						++m_Count;
					}
					++pkp;
				}	
				dest.m_Complex |= buffer.m_Complex;
				continue;
			}
		}

		mvKeyedParameter* pkp = params;
		for(int j=buffer.m_Count; j>0; --j)
		{
			if(pkp->Parameter.m_type != mvParameter::kParameterNone)
			{
				mvKeyedParameter& kpNew = InsertNoRef(pkp->Key, pkp->Parameter);
				kpNew.Parameter.AddKnownRef();

				pkp->Parameter.RemoveKnownRef();
				pkp->Parameter.m_type = mvParameter::kParameterData;
			}
			++pkp;
		}
		buffer.m_Complex = false;
	}

	m_TypeMask |= pb.m_TypeMask;
	m_KeyMask |= pb.m_KeyMask;

	pb.Clear();
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Copy(const mvParameterBuffer& pb)
{
	Assert(!m_Map);

	for(int i=0; i<pb.m_Buffers.GetCount(); ++i)
	{
		Buffer& buffer = *pb.m_Buffers[i];
		mvKeyedParameter* params = buffer.GetKeyedParameters();

#if RESTRICTED_PARAMTER_TYPES
		if(!buffer.m_Complex && m_Capacity > m_Count)
		{
			Buffer& dest = *m_Buffers.Top();
			if((dest.m_Capacity - dest.m_Count) >= buffer.m_Count)
			{
#if VALIDATE_COMPLEX_PARAMETERBUFFERS
				for(int i=0; i<buffer.m_Count; ++i)
				{
					mvKeyedParameter& kp = params[i];
					Assert((kp.Parameter.m_data == NULL) || !kp.Parameter.IsComplex());
				}
#endif // VALIDATE_COMPLEX_PARAMETERBUFFERS
				sysMemCpy(dest.GetKeyedParameters()+dest.m_Count, params, sizeof(mvKeyedParameter)*buffer.m_Count);
				dest.m_Count += buffer.m_Count;
				m_Count += buffer.m_Count;
				continue;
			}
		}
#endif // RESTRICTED_PARAMTER_TYPES

		if(m_Capacity > m_Count)
		{
			Buffer& dest = *m_Buffers.Top();
			if((dest.m_Capacity - dest.m_Count) >= buffer.m_Count)
			{
				mvKeyedParameter* pkp = params;
				mvKeyedParameter* pkpdest = dest.GetKeyedParameters()+dest.m_Count;
				for(int j=buffer.m_Count; j>0; --j)
				{
					if(pkp->Parameter.m_type != mvParameter::kParameterNone)
					{
						pkpdest->Key = pkp->Key;
						pkpdest->Parameter.m_type = pkp->Parameter.m_type;
						pkpdest->Parameter.m_data = pkp->Parameter.m_data;
						pkpdest->Parameter.AddKnownRef();
						pkpdest->Parameter.AddRef();

						++pkpdest;
						++dest.m_Count;
						++m_Count;
					}
					++pkp;
				}	
				dest.m_Complex |= buffer.m_Complex;
				continue;
			}
		}

		mvKeyedParameter* pkp = params;
		for(int j=buffer.m_Count; j>0; --j)
		{
			if(pkp->Parameter.m_type != mvParameter::kParameterNone)
			{
				Insert(pkp->Key, pkp->Parameter);
			}
			++pkp;
		}
	}

	m_TypeMask |= pb.m_TypeMask;
	m_KeyMask |= pb.m_KeyMask;
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void mvParameterBuffer::Dump() const
{
	Displayf("------Dumping parameters-----");

	atString output;
	Dump(output);

	if(output.c_str())
	{
		Printf("%s", output.c_str());
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Dump(atString& output) const
{
	for(int i=0; i<m_Buffers.GetCount(); ++i)
	{
		m_Buffers[i]->Dump(output);
	}
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

u16 mvParameterBuffer::CalcCount() const
{
	u16 count = 0;
	for(int i=0; i<m_Buffers.GetCount(); ++i)
	{
		count += m_Buffers[i]->CalcCount();
	}
	return count;
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::GenerateFreeCapacity()
{
	Assert(m_Count == m_Capacity);
#if CR_STATS
	bool wasEmpty = (m_Capacity == 0);
	if(!wasEmpty)
	{
		PF_INCREMENT(mvParameterBuffer_GenFree);
	}
#endif

	// if all free buffer slots used, de-duplicate and compact buffers
	if(m_Buffers.GetCount() == m_Buffers.GetMaxCount())
	{
		DeDuplicate();
		Compact();
#if CR_STATS
		PF_INCREMENT(mvParameterBuffer_GenFreeCompact);
#endif
	}

	// check capacity again (compact may have freed unused entries)
	if(m_Count == m_Capacity)
	{
		// make the buffer as big as the previous buffer (or the default value if we're the first)
		u16 capacity = m_DefaultCapacity;
		if(m_Buffers.GetCount() > 0)
		{
			capacity = m_Buffers.Top()->m_Capacity;
			capacity = Min(capacity, Buffer::sm_MaxCapacity);
		}

		m_Buffers.Append() = Buffer::Allocate(capacity);

		m_Capacity += capacity;
#if CR_STATS
		if(!wasEmpty)
		{
			PF_INCREMENT(mvParameterBuffer_GenFreeAppend);
		}
#endif
	}

	Assert(m_Count < m_Capacity);
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::GenerateMap(Map* map, bool removeDuplicates)
{
	for(int i=m_Buffers.GetCount()-1; i>=0; --i)
	{
		Buffer& buffer = *m_Buffers[i];
		mvKeyedParameter* params = buffer.GetKeyedParameters();

		for(int j=buffer.m_Count-1; j>=0; --j)
		{
			mvKeyedParameter& kp = params[j];

			if(kp.Parameter.m_type != mvParameter::kParameterNone)
			{
				if(map->Find(kp.Key))
				{
					Assert(kp.Parameter.m_type == map->Find(kp.Key)->m_type);
					if(removeDuplicates)
					{
						if(!kp.Parameter.IsDuplicable())
						{
							kp.ResetRelease();
						}
					}
				}
				else
				{
					map->Insert(kp.Key, &kp.Parameter);
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::ConvertToSignal(const mvNetworkDef* def, mvSignalData& signalData)
{
	Buffer** buffers = m_Buffers.GetElements();
	u32 numBuffers = m_Buffers.GetCount();

	u32 flags = signalData.m_Flags;
	u32 requests = signalData.m_Requests;
	for(; numBuffers; numBuffers--)
	{
		Buffer* buffer = *buffers;
		mvKeyedParameter* parameter = buffer->GetKeyedParameters();
		u32 numParameters = buffer->m_Count;

		for(; numParameters; numParameters--)
		{
			u32 paramType = parameter->Parameter.m_type;
			if(Unlikely(paramType == mvParameter::kParameterFlag))
			{
				int idx = def->ConvertFlagKeyToIndex(parameter->Key);
				if(idx >= 0)
				{
					flags ^= (-(int)parameter->Parameter.GetFlag() ^ flags) & (1<<idx);
				}
				parameter->Key = 0;
				parameter->Parameter.m_type = mvParameter::kParameterNone;
				parameter->Parameter.m_data = NULL;
			}
			else if(Unlikely(paramType == mvParameter::kParameterRequest))
			{
				int idx = def->ConvertRequestKeyToIndex(parameter->Key);
				if(idx >= 0)
				{
					requests |= 1<<idx;
				}
				parameter->Key = 0;
				parameter->Parameter.m_type = mvParameter::kParameterNone;
				parameter->Parameter.m_data = NULL;
			}

			parameter++;
		}

		buffers++;
	}

	signalData.m_Flags = flags;
	signalData.m_Requests = requests;
}

////////////////////////////////////////////////////////////////////////////////

mvParameterBuffer::Buffer::Buffer(u16 capacity)
: m_Count(0)
, m_Capacity(capacity)
, m_Complex(false)
{
	mvKeyedParameter* params = GetKeyedParameters();
	for(int i=0; i<m_Capacity; ++i)
	{
		params[i].Key = 0;
		params[i].Parameter.m_type = mvParameter::kParameterNone;
		params[i].Parameter.m_data = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

mvParameterBuffer::Buffer::~Buffer()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Buffer::Append(Buffer& buf)
{
	Assert((m_Count + buf.CalcCount()) <= m_Capacity);

	mvKeyedParameter* params = buf.GetKeyedParameters();
	for(int i=0; i<buf.m_Count; ++i)
	{
		mvKeyedParameter& kp = params[i];
		if(kp.Parameter.m_type != mvParameter::kParameterNone)
		{
			Insert(kp.Key, kp.Parameter);
		}
	}

	m_Complex |= buf.m_Complex;
}

////////////////////////////////////////////////////////////////////////////////

u32 mvParameterBuffer::Buffer::CalcMemory(u16 capacity)
{
	return sizeof(Buffer) + capacity*sizeof(mvKeyedParameter);
}

////////////////////////////////////////////////////////////////////////////////

 mvParameterBuffer::Buffer* mvParameterBuffer::Buffer::Allocate(u16 capacity)
{
	Assert(capacity <= sm_MaxCapacity);
	void* buf = mvFixedHeap::Alloc(CalcMemory(capacity));
	Assert(buf);
#if CR_STATS
	PF_INCREMENT(mvParameterBuffer_BufferAlloc);
#endif
	return rage_placement_new(buf) Buffer(capacity);
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Buffer::Free(Buffer* buffer)
{
	if(buffer)
	{
		buffer->Shutdown();
		mvFixedHeap::Free(buffer);
#if CR_STATS
		PF_INCREMENT(mvParameterBuffer_BufferFree);
#endif
	}
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void mvParameterBuffer::Buffer::Dump(atString& output) const
{
	const mvKeyedParameter* params = GetKeyedParameters();
	for(int i=0; i<m_Count; ++i)
	{
		const mvKeyedParameter& kp = params[i];

		if(kp.Parameter.m_type != mvParameter::kParameterNone)
		{
			kp.Dump(output);
			output += "\n";
		}
	}
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

mvParameterBuffer::Map::Map(u16 capacity)
: m_MapMemory((atMapEntry<u32, mvParameter*>*)m_MapEntries, capacity, &m_MapHashTable[0])
, m_Map(atMapHashFn<u32>(), atMapEquals<u32>(), m_MapMemory)
{
	m_Map.Create(sm_MapHashTableSize-1, false);
}

////////////////////////////////////////////////////////////////////////////////

mvParameterBuffer::Map::~Map()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Map::Shutdown()
{
	m_Map.Kill();
}

////////////////////////////////////////////////////////////////////////////////

u32 mvParameterBuffer::Map::CalcMemory(u16 capacity)
{
	return sizeof(Map) + capacity*sizeof(EntryType);
}

////////////////////////////////////////////////////////////////////////////////

mvParameterBuffer::Map* mvParameterBuffer::Map::Allocate(u16 capacity)
{
	void* buf = mvFixedHeap::Alloc(CalcMemory(capacity));
	Assert(buf);
#if CR_STATS
	PF_INCREMENT(mvParameterBuffer_MapAlloc);
#endif
	return rage_placement_new(buf) Map(capacity);
}

////////////////////////////////////////////////////////////////////////////////

void mvParameterBuffer::Map::Free(Map* map)
{
	if(map)
	{
#if CR_STATS
		PF_INCREMENT(mvParameterBuffer_MapFree);
#endif
		map->Shutdown();
		mvFixedHeap::Free(map);
	}
}

////////////////////////////////////////////////////////////////////////////////

mvKeyedParameter* mvParameterBuffer::Iterator::Begin(mvParameterBuffer& parent, mvParameter::ParamType type, u32 key)
{
	// first look inside the map
	if(key && parent.m_Map && !parent.FindFirstInternal(key))
	{
		return NULL;
	}

	// linear search inside the parameter arrays
	Buffer** buffers = parent.m_Buffers.GetElements();
	int numBuffers = parent.m_Buffers.GetCount();

	int bufferIdx = 0;
	int parameterIdx = 0;
	for(; bufferIdx < numBuffers; bufferIdx++)
	{
		Buffer* buffer = buffers[bufferIdx];
		mvKeyedParameter* parameters = buffer->GetKeyedParameters();
		int numParameters = buffer->m_Count;

		for(; parameterIdx < numParameters; parameterIdx++)
		{
			mvKeyedParameter* kp = parameters + parameterIdx;

			if(kp->Parameter.GetType() != mvParameter::kParameterNone)
			{
				if(!key || key == kp->Key)
				{
					if(type == mvParameter::kParameterNone || type == kp->Parameter.GetType())
					{
						m_ParameterIdx = parameterIdx+1;
						m_BufferIdx = bufferIdx;
						m_Parent = &parent;
						m_Type = type;
						m_Key = key;
						return kp;
					}
				}
			}
		}

		parameterIdx = 0;
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

mvKeyedParameter* mvParameterBuffer::Iterator::End(mvParameterBuffer& parent, mvParameter::ParamType type, u32 key)
{
	// first look inside the map
	if(key && parent.m_Map && !parent.FindFirstInternal(key))
	{
		return NULL;
	}

	// linear backward search
	Buffer** buffers = parent.m_Buffers.GetElements();
	int numBuffers = parent.m_Buffers.GetCount();

	int bufferIdx = numBuffers-1;
	for(; bufferIdx >= 0; bufferIdx--)
	{
		Buffer* buffer = buffers[bufferIdx];
		mvKeyedParameter* parameters = buffer->GetKeyedParameters();
		int parameterIdx = buffer->m_Count-1;

		for(; parameterIdx >= 0; parameterIdx--)
		{
			mvKeyedParameter* kp = parameters + parameterIdx;

			if(kp->Parameter.GetType() != mvParameter::kParameterNone)
			{
				if(!key || key == kp->Key)
				{
					if(type == mvParameter::kParameterNone || type == kp->Parameter.GetType())
					{
						m_ParameterIdx = parameterIdx-1;
						m_BufferIdx = bufferIdx;
						m_Parent = &parent;
						m_Type = type;
						m_Key = key;
						return kp;
					}
				}
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

mvKeyedParameter* mvParameterBuffer::Iterator::Next()
{
	Buffer** buffers = m_Parent->m_Buffers.GetElements();
	int numBuffers = m_Parent->m_Buffers.GetCount();
	
	int bufferIdx = m_BufferIdx;
	int parameterIdx = m_ParameterIdx;
	for(; bufferIdx < numBuffers; bufferIdx++)
	{
		Buffer* buffer = buffers[bufferIdx];
		mvKeyedParameter* parameters = buffer->GetKeyedParameters();
		int numParameters = buffer->m_Count;

		for(; parameterIdx < numParameters; parameterIdx++)
		{
			mvKeyedParameter* kp = parameters + parameterIdx;

			if(kp->Parameter.GetType() != mvParameter::kParameterNone)
			{
				if(!m_Key || m_Key == kp->Key)
				{
					if(m_Type == mvParameter::kParameterNone || m_Type == kp->Parameter.GetType())
					{
						m_ParameterIdx = parameterIdx+1;
						m_BufferIdx = bufferIdx;
						return kp;
					}
				}
			}
		}

		parameterIdx = 0;
	}

	return NULL;
}


////////////////////////////////////////////////////////////////////////////////

mvKeyedParameter* mvParameterBuffer::Iterator::Previous()
{
	Buffer** buffers = m_Parent->m_Buffers.GetElements();

	int bufferIdx = m_BufferIdx;
	int parameterIdx = m_ParameterIdx;
	for(; bufferIdx >= 0; bufferIdx--)
	{
		Buffer* buffer = buffers[bufferIdx];
		mvKeyedParameter* parameters = buffer->GetKeyedParameters();

		for(; parameterIdx >= 0; parameterIdx--)
		{
			Assert(parameterIdx < buffer->m_Count);
			mvKeyedParameter* kp = parameters + parameterIdx;

			if(kp->Parameter.GetType() != mvParameter::kParameterNone)
			{
				if(!m_Key || m_Key == kp->Key)
				{
					if(m_Type == mvParameter::kParameterNone || m_Type == kp->Parameter.GetType())
					{
						m_ParameterIdx = parameterIdx-1;
						m_BufferIdx = bufferIdx;
						return kp;
					}
				}
			}
		}

		if(bufferIdx > 0)
		{
			parameterIdx = buffers[bufferIdx-1]->m_Count-1;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage