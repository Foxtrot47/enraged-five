//
// move/state.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "move_state.h"

#include "move_blend.h"
#include "move_command.h"
#include "move_fixedheap.h"
#include "move_funcs.h"
#include "move_parameterbuffer.h"
#include "move_network.h"
#include "move_node.h"
#include "move_observer.h"
#include "move_parameter.h"
#include "move_statedef.h"
#include "move_subnetwork.h"
#include "move_synchronizer.h"
#include "move_types.h"
#include "move_types_internal.h"

#include "crmotiontree/motiontree.h"
#include "cranimation/weightmodifier.h"


namespace rage {

///////////////////////////////////////////////////////////////////////////////

class mvWeightModifierSlowInSlowOut : public crWeightModifierSlowInSlowOut
{
public:
	mvWeightModifierSlowInSlowOut(bool slowIn, bool slowOut)
		: crWeightModifierSlowInSlowOut(slowIn, slowOut) {}

	MOVE_DECLARE_ALLOCATOR();
};

///////////////////////////////////////////////////////////////////////////////

static inline crWeightModifier* create_modifier(u32 type)
{
	switch (type) {
	case mvConstant::kModifierEaseInOut:
		return rage_new mvWeightModifierSlowInSlowOut(true, true);
	case mvConstant::kModifierEaseOut:
		return rage_new mvWeightModifierSlowInSlowOut(false, true);
	case mvConstant::kModifierEaseIn:
		return rage_new mvWeightModifierSlowInSlowOut(true, false);
	case mvConstant::kModifierLinear:
		break;
	case mvConstant::kModifierStep:
		break;
	}

	return NULL;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_REGISTERED_TYPE(mvNodeState, mvNode::kNodeState, mvNodeStateDef);

mvNodeState::mvNodeState(crmtNode::eNodes nodeType)
: crmtNodeParent(nodeType)
, m_RootNetwork(NULL)
, m_ParentNetwork(NULL)
, m_BaseDef(NULL)
{
}

///////////////////////////////////////////////////////////////////////////////

mvNodeState::mvNodeState(datResource& rsc)
: crmtNodeParent(rsc)
{
}

///////////////////////////////////////////////////////////////////////////////

mvNodeState::~mvNodeState()
{
	Shutdown();
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeState::Init(mvNetwork* root, mvMotionWeb* parent, const mvNodeStateBaseDef* def)
{
#if CRMT_NODE_2_DEF_MAP
	if(PARAM_movedebug.Get())
	{
		const void **ppDef = g_crmtNode2DefMap.Access(this);
		if(ppDef != NULL)
		{
			if(*ppDef == NULL)
			{
				g_crmtNode2DefMap.Delete(this);
				g_crmtNode2DefMap.Insert(this, def);
			}
		}
		else
		{
			g_crmtNode2DefMap.Insert(this, def);
		}
	}
#endif //CRMT_NODE_2_DEF_MAP

	m_RootNetwork = root;
	m_ParentNetwork = parent;
	m_ParentNetwork->AddRef();
	m_BaseDef = def;

	// send the state on enter event
	if (m_BaseDef->m_OnEnterEnabled)
	{
		m_ParentNetwork->GetInternalParameters()->Insert(m_BaseDef->m_OnEnterId, true);
	}
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeState::Shutdown()
{
	crmtNodeParent::Shutdown();

	// send the state on exit event
	if (m_BaseDef->m_OnExitEnabled)
	{
		m_ParentNetwork->GetInternalParameters()->Insert(m_BaseDef->m_OnExitId, true);
	}

	m_ParentNetwork->Release();
	m_ParentNetwork = NULL;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeState::Update(float)
{
	bool childDisabled = GetFirstChild()->IsDisabled();
	SetDisabled(childDisabled);
	SetSilent(!childDisabled);
}

/////////////////////////////////////////////////////////////////////////

#if CR_DEV
void mvNodeState::Dump(crmtDumper& dumper) const
{
	crmtNodeParent::Dump(dumper);

}
#endif // CR_DEV

/////////////////////////////////////////////////////////////////////////

static __forceinline mvCondition* Advance(const mvCondition* condition)
{
	static const u32 kSizeOf[] = {
		sizeof(mvRange), // kConditionInRange
		sizeof(mvRange), // kConditionOutOfRange
		sizeof(mvOnRequest), // kConditionOnRequest
		sizeof(mvOnFlag), // kConditionOnFlag
		sizeof(mvAtEvent), // kConditionAtEvent
		sizeof(mvValue), // kConditionGreaterThan
		sizeof(mvValue), // kConditionGreaterThanEqual
		sizeof(mvValue), // kConditionLessThan
		sizeof(mvValue), // kConditionLessThanEqual
		sizeof(mvConditionLifetime), // kConditionLifetimeGreaterThan
		sizeof(mvConditionLifetime), // kConditionLifetimeLessThan
		sizeof(mvOnMoveEventTag), // kConditionOnMoveEventTag
		sizeof(mvBoolValue), // kConditionBoolEquals
	};

	CompileTimeAssert(NELEM(kSizeOf) == mvConstant::kConditionCount);

	return (mvCondition*)(((u8*)condition)+kSizeOf[condition->Type]);
}

/////////////////////////////////////////////////////////////////////////

static u32 CheckInRange(mvCondition* condition, const mvParameterBuffer* params)
{
	mvRange* data = reinterpret_cast<mvRange*>(condition);
	u32 id = data->Signal;
	u32 flag = 0;
	if (const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal)) {
		float p = param->GetReal();
		if (data->Lower < p && p < data->Upper) {
			flag = 1;
		}
	}
	return flag;
}

/////////////////////////////////////////////////////////////////////////

static u32 CheckOutOfRange(mvCondition* condition, const mvParameterBuffer* params)
{
	mvRange* data = reinterpret_cast<mvRange*>(condition);
	u32 id = data->Signal;
	u32 flag = 0;
	if (const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal)) {
		float p = param->GetReal();
		if (p < data->Lower || data->Upper < p) {
			flag = 1;
		}
	}
	return flag;
}

/////////////////////////////////////////////////////////////////////////

// used for flags too
static __forceinline u32 CheckOnRequest(mvCondition* condition, u32 requests)
{
	mvOnRequest* data = reinterpret_cast<mvOnRequest*>(condition);
	u32 bit = 1 << data->Request;
	u32 set = (requests & bit) != 0;
	return set != data->NotSet;
}

/////////////////////////////////////////////////////////////////////////

static u32 CheckAtEvent(mvCondition* condition, const mvParameterBuffer* params)
{
	mvAtEvent* data = reinterpret_cast<mvAtEvent*>(condition);
	u32 flag = 0;
	// NOTE: Not sure if this is meant to be a boolean or an int!!!
	if (params->FindFirstOfType(data->Event, mvParameter::kParameterBoolean)) {
		flag = 1;
	}
	return flag != data->NotSet;
}

/////////////////////////////////////////////////////////////////////////

static u32 CheckOnMoveEventTag(mvCondition* condition, const mvParameterBuffer* params)
{
	mvOnMoveEventTag* data = reinterpret_cast<mvOnMoveEventTag*>(condition);
	u32 flag = 0;
	// NOTE: Not sure if this is meant to be a boolean or an int!!!
	if (params->FindFirstOfType(data->Tag, mvParameter::kParameterBoolean)) {
		flag = 1;
	}
	return flag != data->NotSet;
}


/////////////////////////////////////////////////////////////////////////

static u32 CheckGreaterThan(mvCondition* condition, const mvParameterBuffer* params)
{
	mvValue* data = reinterpret_cast<mvValue*>(condition);
	u32 flag = 0;
	if (const mvParameter* param = params->FindFirstOfType(data->Signal, mvParameter::kParameterReal)) {
		if (data->Value < param->GetReal()) {
			flag = 1;
		}
	}
	return flag;
}

/////////////////////////////////////////////////////////////////////////

static u32 CheckGreaterThanEqual(mvCondition* condition, const mvParameterBuffer* params)
{
	mvValue* data = reinterpret_cast<mvValue*>(condition);
	u32 flag = 0;
	if (const mvParameter* param = params->FindFirstOfType(data->Signal, mvParameter::kParameterReal)) {
		if (data->Value <= param->GetReal()) {
			flag = 1;
		}
	}
	return flag;
}

/////////////////////////////////////////////////////////////////////////

static u32 CheckLessThan(mvCondition* condition, const mvParameterBuffer* params)
{
	mvValue* data = reinterpret_cast<mvValue*>(condition);
	u32 flag = 0;
	if (const mvParameter* param = params->FindFirstOfType(data->Signal, mvParameter::kParameterReal)) {
		if (data->Value > param->GetReal()) {
			flag = 1;
		}
	}
	return flag;
}

/////////////////////////////////////////////////////////////////////////

static u32 CheckLessThanEqual(mvCondition* condition, const mvParameterBuffer* params)
{
	mvValue* data = reinterpret_cast<mvValue*>(condition);
	u32 flag = 0;
	if (const mvParameter* param = params->FindFirstOfType(data->Signal, mvParameter::kParameterReal)) {
		if (data->Value >= param->GetReal()) {
			flag = 1;
		}
	}
	return flag;
}

/////////////////////////////////////////////////////////////////////////

static u32 CheckBoolEqual(mvCondition* condition, const mvParameterBuffer* params)
{
	mvBoolValue* data = reinterpret_cast<mvBoolValue*>(condition);
	u32 flag = 0;
	if (const mvParameter* param = params->FindFirstOfType(data->Signal, mvParameter::kParameterBoolean)) {
		if (data->Value == (rage::u32)param->GetBool()) {
			flag = 1;
		}
	}
	return flag;
}

/////////////////////////////////////////////////////////////////////////

class DetachUpdateObservers : public crmtIterator
{
public:
	virtual void Visit(crmtNode& node)
	{
		node.MessageObservers(crmtMessage(mvObserver::kMsgDetachUpdaters));
		// The better thing would be to use an observer on the subnetwork node,
		//	but we have enough of those already, so lets just disable subnetwork
		//	inserts while we're going through the nodes.

		switch(node.GetNodeType())
		{
		case crmtNode::kNodeSubNetwork:
			{
				mvNodeSubNetwork& subnetworkNode = static_cast<mvNodeSubNetwork&>(node);
				subnetworkNode.DisableInserts();
			}
			break;
		case crmtNode::kNodeStateMachine:
			{
				mvNodeStateMachine& statemachineNode = static_cast<mvNodeStateMachine&>(node);
				statemachineNode.DisableTransitions();
			}
			break;
		default:
			break;
		}
	}
};

/////////////////////////////////////////////////////////////////////////

#if __DEV || __BANK
mvDRTrackInterface *mvDRTrackInterface::sm_Instance = NULL;
bool mvDRTrackInterface::sm_bEnabled;
#endif

#if __DEV
void RecordCondition(const mvNetworkDef* networkDef, const crCreature* creature, int iPassed, int iConditionType, int iConditionIndex, const mvNodeStateBaseDef* from, const mvNodeStateBaseDef* to)
{
	if (mvDRTrackInterface::sm_bEnabled && mvDRTrackInterface::sm_Instance)
	{
		mvDRTrackInterface::sm_Instance->RecordCondition(networkDef, creature, iPassed, iConditionType, iConditionIndex, from, to);
	}
}

/////////////////////////////////////////////////////////////////////////

void RecordEndConditions()
{
	if (mvDRTrackInterface::sm_bEnabled && mvDRTrackInterface::sm_Instance)
	{
		mvDRTrackInterface::sm_Instance->RecordEndConditions();
	}
}

/////////////////////////////////////////////////////////////////////////

void RecordTransition(const mvNetworkDef* networkDef, const crCreature* creature, const mvNodeStateBaseDef* stateMachine, const mvNodeStateBaseDef* fromState, const mvNodeStateBaseDef* toState)
{
	if (mvDRTrackInterface::sm_bEnabled && mvDRTrackInterface::sm_Instance)
	{
		mvDRTrackInterface::sm_Instance->RecordTransition(networkDef, creature, stateMachine, fromState, toState);
	}
}
#endif // __DEV

/////////////////////////////////////////////////////////////////////////

mvNodeStateMachine::mvNodeStateMachine()
: mvNodeState(kNodeStateMachine)
, m_Tail(NULL)
, m_StateDef(NULL)
, m_Lifetime(0.f)
#if MOVE_CONCURRENT_TRANSITION_LIMIT
, m_ActiveTransitions(NULL)
, m_NumActiveTransitions(0)
#endif // MOVE_CONCURRENT_TRANSITION_LIMIT
, m_InlineState(false)
, m_PendingDisableTransitions(false)
, m_DisableTransitions(false)
{
}

//////////////////////////////////////////////////////////////////////////

mvNodeStateMachine::mvNodeStateMachine(datResource& rsc)
: mvNodeState(rsc)
{
}

//////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(mvNodeStateMachine, crmtNode::kNodeStateMachine, CRMT_NODE_RECEIVE_PREUPDATES|CRMT_NODE_RECEIVE_UPDATES);

//////////////////////////////////////////////////////////////////////////

void mvNodeStateMachine::Init(mvMotionWeb* parent, mvNetwork* root, const mvNodeStateMachineDef* def, crmtNode* tail)
{
	InitInternal(false, parent, root, def, tail);
}

//////////////////////////////////////////////////////////////////////////

void mvNodeStateMachine::Init(mvMotionWeb* parent, mvNetwork* root, const mvNodeInlinedStateMachineDef* def, crmtNode* tail)
{
	InitInternal(true, parent, root, def, tail);
}

//////////////////////////////////////////////////////////////////////////

void mvNodeStateMachine::Shutdown()
{
#if MOVE_CONCURRENT_TRANSITION_LIMIT
	while(m_ActiveTransitions)
	{
		m_ActiveTransitions->Detach();
	}
	Assert(m_NumActiveTransitions == 0);
#endif // MOVE_CONCURRENT_TRANSITION_LIMIT

	mvNodeState::Shutdown();
}

//////////////////////////////////////////////////////////////////////////

void mvNodeStateMachine::InitInternal(bool inlineState, mvMotionWeb* parent, mvNetwork* root, const mvNodeStateBaseDef* def, crmtNode* tail)
{
	mvNodeState::Init(root, parent, def);

	m_InlineState = inlineState;
	m_Tail = tail;

	mvNodeStateBaseDef* pDef = NULL;

	const mvNodeStateMachineDef::StateNode* data = static_cast<const mvNodeStateMachineDef*>(m_BaseDef)->GetChildData();
	const mvParameter* parameter = m_ParentNetwork->GetParameters()->FindFirstOfType(m_BaseDef->m_id, mvParameter::kParameterNodeId);
	if (parameter) {
		Assert(parameter->GetType() == mvParameter::kParameterNodeId);
		const u32 id = parameter->GetNodeId();
		for (u32 i = 0; i < m_BaseDef->m_ChildCount; ++i) {
			const mvNodeStateMachineDef::StateNode& sn = data[i];
			if (id == sn.Id) {
				pDef = sn.Node.Get<mvNodeStateBaseDef>();
				break;
			}
		}
	}
	if (!pDef) {
		pDef = m_BaseDef->m_Initial.Get<mvNodeStateBaseDef>();
	}

	if (m_InlineState && m_Tail)
	{
		SetSkipFirstChild(true);
		AddChild(*m_Tail);
	}

	crmtNode* current = ApplyState(pDef);
	AddChild(*current);
}

//////////////////////////////////////////////////////////////////////////

void mvNodeStateMachine::PreUpdate(float dt)
{
	// NOTE: By default we want to take transitions first, and if they happen ignore forced
	//	state since that should only be used in exceptional circumstances (eg.network games)
	m_Lifetime += dt;

	// test whether the current state needs to change
	bool transitioned = EvaluateAndTransition();

	// Only check for a forced state if we haven't transitioned to a new state already this frame
	if (!transitioned)
	{
		const mvNodeStateMachineDef::StateNode* data = static_cast<const mvNodeStateMachineDef*>(m_BaseDef)->GetChildData();
		// Try find this state's node id in the parameter list. If it exists then use the id from that to select the state
		const mvParameter* parameter = m_ParentNetwork->GetParameters()->FindFirstOfType(m_BaseDef->m_id, mvParameter::kParameterNodeId);
		if (parameter) {
			Assert(parameter->GetType() == mvParameter::kParameterNodeId);
			const u32 id = parameter->GetNodeId();

			// don't want to force if we're already in the state - This leads to incorrect state exit events
			if (id != m_StateDef->m_id)
			{
				for (u32 i = 0; i < m_BaseDef->m_ChildCount; ++i) {
					const mvNodeStateMachineDef::StateNode& sn = data[i];
					if (id == sn.Id) {
						mvNodeDef* def = sn.Node.Get<mvNodeDef>();
						crmtNode* ret = ApplyState(def);
						crmtNode* old = (m_InlineState && m_Tail)?GetFirstChild()->GetNextSibling():GetFirstChild();
						ReplaceChild(*old, *ret);
						break;
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////

bool mvNodeStateMachine::EvaluateAndTransition()
{
	if(m_DisableTransitions)
	{
		return false;
	}

	const u32 transitionCount = m_StateDef->m_TransitionCount;
	const mvTransition* transition = m_StateDef->m_Transitions.Get();
	const mvSignalData& signalData = m_ParentNetwork->GetSignalData();
	const u32 requests = signalData.GetRequests();
	const u32 flags = signalData.GetFlags();

	bool transitioned = false;

	const mvParameterBuffer* params = m_ParentNetwork->GetParameters();
	for (u32 t = 0; t < transitionCount; ) {
		mvCondition* cond = (mvCondition*)(transition + 1);
		bool allConditionsPassed = true;
		const u32 conditionCount = transition->ConditionCount;
		for (u32 c = 0; c < conditionCount; ++c) {
			u32 test = 0;

			switch (cond->Type) {
			case mvConstant::kConditionInRange:
				test = CheckInRange(cond, params);
				break;
			case mvConstant::kConditionOutOfRange:
				test = CheckOutOfRange(cond, params);
				break;
			case mvConstant::kConditionOnRequest:
				{
					test = CheckOnRequest(cond, requests);
				}
				break;
			case mvConstant::kConditionOnFlag:
				{
					// yes that's meant to be CheckOnRequest
					test = CheckOnRequest(cond, flags);
				}
				break;
			case mvConstant::kConditionAtEvent:
				test = CheckAtEvent(cond, params);
				break;
			case mvConstant::kConditionGreaterThan:
				test = CheckGreaterThan(cond, params);
				break;
			case mvConstant::kConditionGreaterThanEqual:
				test = CheckGreaterThanEqual(cond, params);
				break;
			case mvConstant::kConditionLessThan:
				test = CheckLessThan(cond, params);
				break;
			case mvConstant::kConditionLessThanEqual:
				test = CheckLessThanEqual(cond, params);
				break;
			case mvConstant::kConditionLifetimeGreaterThan:
				{
					mvConditionLifetime* lifetime = static_cast<mvConditionLifetime*>(cond);
					test = m_Lifetime > lifetime->Lifetime;
				}
				break;
			case mvConstant::kConditionLifetimeLessThan:
				{
					mvConditionLifetime* lifetime = static_cast<mvConditionLifetime*>(cond);
					test = m_Lifetime  < lifetime->Lifetime;
				}
				break;
			case mvConstant::kConditionOnMoveEventTag:
				test = CheckOnMoveEventTag(cond, params);
				break;
			case mvConstant::kConditionBoolEquals:
				test = CheckBoolEqual(cond, params);
				break;
			}

			DEV_ONLY(RecordCondition(m_ParentNetwork->GetDefinition(), GetMotionTree().GetCreature(), test, cond->Type, c, m_StateDef, transition->Target.Get<mvNodeStateBaseDef>()));

			if(!test)
			{
				allConditionsPassed = false;
				break;
			}

			cond = Advance(cond);
		}

		DEV_ONLY(RecordEndConditions());	//Terminates the debug tracking

		// do it
		if (Unlikely(allConditionsPassed)) {
			crFrameFilter* filter = NULL;
			if (Unlikely(transition->Filter)) {
				const char* filename = (const char*)cond;
				filter = mvAllocAndInitFilter(m_ParentNetwork, filename);
			}

			transitioned = true;

#if MOVE_TRANSITION_EVENTS
			if(transition->TransitionEvent)
			{
				m_ParentNetwork->GetInternalParameters()->Insert(transition->TransitionEventId, true);
			}
#endif // MOVE_TRANSITION_EVENTS

			DoTransition(transition, filter, params);
		} else {
			transition = reinterpret_cast<const mvTransition*>((u8*)transition + transition->Size);
			++t;
			continue;
		}

		break;
	}

	m_DisableTransitions = m_PendingDisableTransitions;

	return transitioned;
}

/////////////////////////////////////////////////////////////////////////

void mvNodeStateMachine::DoTransition(const mvTransition* transition, crFrameFilter* filter, const mvParameterBuffer* params)
{
	//mvDisplayf( "--------------------------------------------");
	//mvDisplayf( "State transition in parent state node:%p (%s) ",this, m_RootNetwork->FindName(this));

	const mvNodeStateBaseDef* toState = transition->Target.Get<mvNodeStateBaseDef>();
	mvMotionWeb* web = m_ParentNetwork;
	DEV_ONLY(RecordTransition(m_ParentNetwork->GetDefinition(), GetMotionTree().GetCreature(), m_BaseDef, GetActiveDef(), toState));

	// Need to deal with the tail parent here, as it's going to change in toState->Create(&context)
	// TODO: Check the state's type and make sure it's a state node... eg we want to make sure the cast is valid which it should be!
	crmtNode* add = ApplyState(toState);
	crmtNode* remove = (m_InlineState && m_Tail)?GetFirstChild()->GetNextSibling():GetFirstChild();

	bool durationFromSignal = transition->DurationFromSignal;
	if (m_Tail) {
		float duration = transition->Duration;
		if (durationFromSignal) {
			mvInitParameter<float> fallback(duration);
			const mvParameter* p = params->FindFirstOfType(transition->Signal, mvParameter::kParameterReal, &fallback);
			duration = p->GetReal();
		}

		if (duration > 0.f) {
			crWeightModifier* modifier = create_modifier(transition->Modifier);

			crmtNodeBlend* blend =
				crmtNodeBlend::CreateNode(GetMotionTree());
			blend->Init(0.f, 1.f/duration, true, false, filter, modifier);

#if IGNORE_TRANSITION_TRANSITIONAL_THINGY
			mvSynchronizer::AttachToBlend(*blend, transition->Synchronizer, transition->Tag, true, transition->Immutable?true:false);
#else
			mvSynchronizer::AttachToBlend(*blend, transition->Synchronizer, transition->Tag, transition->Transitional?true:false, transition->Immutable?true:false);
#endif // IGNORE_STUFF
			remove->AddRef();
			ReplaceChild(*remove, *blend, false);
			blend->AddChild(*remove);

			if (transition->EnableTransitionWeightSignal)
			{
				const mvNode::NodeTypeInfo* info = mvNode::sm_NodeTypeInfos[crmtNode::kNodeBlend];
				mvNode::GetParameterFunc* func = info->m_ResolveParameterGetFn(mvNodeBlendDef::kParameterWeight);
				mvUpdateParameterBuffer* updater = rage_new mvUpdateParameterBuffer;
				updater->Init(web, transition->TransitionWeightSignal, func, 0);
				updater->Attach(*blend);
			}

			if (transition->BlockUpdateAfterTransition) {
				DetachUpdateObservers iter;
				iter.Iterate(*remove);
			}

			remove->Release();

			blend->AddChild(*add);
			//mvDisplayf( "Transition from node:%p (%s) -> %p (%s_ with blend node %p: duration %.3f", remove, removing, add, adding, blend, duration);

#if MOVE_CONCURRENT_TRANSITION_LIMIT
			mvActiveTransition* activeTransition = rage_new mvActiveTransition;
			activeTransition->Attach(*blend);
			activeTransition->Init(web, *this);

			if(Unlikely(m_NumActiveTransitions > MOVE_CONCURRENT_TRANSITION_LIMIT))
			{
				TerminateExcessTransitions();
			}
#endif // MOVE_CONCURRENT_TRANSITION_LIMIT
		}
		else {
			if (remove) {
				m_Tail->AddRef();
				ReplaceChild(*remove, *add, true);
				m_Tail->Release();
				//mvDisplayf( "Transition from node:%p (%s) -> %p (%s)", remove, removing, add, adding);
			}
			else {
				AddChild(*add);
				//mvDisplayf( "Entering state node:%p (%s)", add, adding);
			}
		}
	}
	else {
		bool durationFromSignal = transition->DurationFromSignal;
		float duration = transition->Duration;

		if (durationFromSignal) {
			mvInitParameter<float> fallback(duration);
			const mvParameter* p = params->FindFirstOfType(transition->Signal, mvParameter::kParameterReal, &fallback);
			duration = p->GetReal();
		}

		if (duration > 0.f) {
			crWeightModifier* modifier = create_modifier(transition->Modifier);
			crmtNodeBlend* blend =
				crmtNodeBlend::CreateNode(GetMotionTree());
			blend->Init(0.f, 1.f/duration, true, false, filter, modifier);

#if IGNORE_TRANSITION_TRANSITIONAL_THINGY
			mvSynchronizer::AttachToBlend(*blend, transition->Synchronizer, transition->Tag, true, transition->Immutable?true:false);
#else
			mvSynchronizer::AttachToBlend(*blend, transition->Synchronizer, transition->Tag, transition->Transitional?true:false, transition->Immutable?true:false);
#endif // IGNORE_STUFF
			remove->AddRef();
			ReplaceChild(*remove, *blend, false);
			blend->AddChild(*remove);

			if (transition->EnableTransitionWeightSignal)
			{
				const mvNode::NodeTypeInfo* info = mvNode::sm_NodeTypeInfos[crmtNode::kNodeBlend];
				mvNode::GetParameterFunc* func = info->m_ResolveParameterGetFn(mvNodeBlendDef::kParameterWeight);
				mvUpdateParameterBuffer* updater = rage_new mvUpdateParameterBuffer;
				updater->Init(web, transition->TransitionWeightSignal, func, 0);
				updater->Attach(*blend);
			}

			if (transition->BlockUpdateAfterTransition) {
				DetachUpdateObservers iter;
				iter.Iterate(*remove);
			}

			remove->Release();
			blend->AddChild(*add);
			//mvDisplayf( "Transition from node:%p (%s) -> %p (%s) with blend node %p: duration %.3f", remove, removing, add, adding, blend, duration);

#if MOVE_CONCURRENT_TRANSITION_LIMIT
			mvActiveTransition* activeTransition = rage_new mvActiveTransition;
			activeTransition->Attach(*blend);
			activeTransition->Init(web, *this);

			if(Unlikely(m_NumActiveTransitions > MOVE_CONCURRENT_TRANSITION_LIMIT))
			{
				TerminateExcessTransitions();
			}
#endif // MOVE_CONCURRENT_TRANSITION_LIMIT
		}
		else {
			ReplaceChild(*remove, *add);
			//mvDisplayf( "Transition from node:%p (%s) -> %p (%s)", remove, removing, add, adding);
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void mvNodeStateMachine::Update(float)
{
	bool childDisabled = ((m_InlineState && m_Tail)?GetFirstChild()->GetNextSibling():GetFirstChild())->IsDisabled();
	SetDisabled(childDisabled);
	SetSilent(!childDisabled);
}

/////////////////////////////////////////////////////////////////////////

#if CR_DEV
void mvNodeStateMachine::Dump(crmtDumper& dumper) const
{
	crmtNodeParent::Dump(dumper);

}
#endif // CR_DEV

/////////////////////////////////////////////////////////////////////////

crmtNode* mvNodeStateMachine::ApplyState(const mvNodeDef* def)
{
	const mvNodeStateBaseDef* stateDef = static_cast<const mvNodeStateBaseDef*>(def);

	mvCreateContext context;
	context.m_MotionTree = &GetMotionTree();
	context.m_Network = m_ParentNetwork;
	context.m_OwnerNetwork = m_RootNetwork;
	context.m_Parameters = m_ParentNetwork->GetParameters();
	context.m_Tail = m_Tail;

	// send enter signal
	if (stateDef->m_OnEnterEnabled)
	{
		m_ParentNetwork->GetInternalParameters()->Insert(stateDef->m_OnEnterId, true);
	}

	crmtNode* node = stateDef->Create(&context);

	// add an observer to send the exit signal when the node is shutdown
	if(stateDef->m_OnExitEnabled)
	{
		mvExitParameter* observer = rage_new mvExitParameter;
		observer->Init(m_ParentNetwork, stateDef->m_OnExitId);
		observer->Attach(*node);
	}

	m_Lifetime = 0.f;
	m_StateDef = stateDef;

	return node;
}

//////////////////////////////////////////////////////////////////////////

#if MOVE_CONCURRENT_TRANSITION_LIMIT
void mvNodeStateMachine::TerminateExcessTransitions()
{
	mvActiveTransition* transition = m_ActiveTransitions;
	for(u32 n=1; transition; ++n)
	{
		if(n > MOVE_CONCURRENT_TRANSITION_LIMIT)
		{
			Assert(transition->IsAttached());
			Warningf("Terminating excess transition at depth %d", n);

			crmtNodeBlend* node = static_cast<crmtNodeBlend*>(transition->GetNode());
			node->SetWeight(1.f);
		}
		transition = transition->m_NextTransition;
	}
}
#endif // MOVE_CONCURRENT_TRANSITION_LIMIT

//////////////////////////////////////////////////////////////////////////

CRMT_IMPLEMENT_NODE_TYPE(mvNodeStateRoot, crmtNode::kNodeStateRoot, CRMT_NODE_RECEIVE_UPDATES);

///////////////////////////////////////////////////////////////////////////////

mvNodeStateRoot::mvNodeStateRoot()
: mvNodeState(crmtNode::kNodeStateRoot)
{
}

///////////////////////////////////////////////////////////////////////////////

mvNodeStateRoot::mvNodeStateRoot(datResource& rsc)
: mvNodeState(rsc)
{
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeStateRoot::Init(mvNetwork* root, const mvNodeStateDef* def, mvCommandBuffers& cmds)
{
	mvNodeState::Init(root, root, def);

	cmds.GetNextBuffer().PrepareBuffer(*root, *root->GetDefinition(), *root->GetParameters());

	crmtNode** children = Alloca(crmtNode*, m_BaseDef->m_ChildCount);

	mvCreateContext context;
	context.m_MotionTree = &GetMotionTree();
	context.m_Network = m_ParentNetwork;
	context.m_OwnerNetwork = m_RootNetwork;
	context.m_Parameters = m_ParentNetwork->GetParameters();
	context.m_Children = children;
	context.m_ChildrenCount = m_BaseDef->m_ChildCount;
	context.m_Tail = NULL;

	mvNodeDef* childDef = m_BaseDef->m_Initial.Get<mvNodeDef>();
	crmtNode* child = childDef->Create(&context);

	// This is only needed for the state nodes
	static_cast<const mvNodeStateDef*>(m_BaseDef)->RegisterHandlers(m_ParentNetwork, children);

	AddChild(*child);

	m_ParentNetwork->GetSignalData().ResetRequests();
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeStateRoot::Update(float dt)
{
	mvNodeState::Update(dt);

	mvMotionWeb* web = m_ParentNetwork;
	web->GetSignalData().ResetRequests();

	mvParameterBuffers* output = web->GetOutputBuffer();
	if (output != NULL)
	{
		output->GetNextBuffer().Copy(*web->GetInternalParameters());
	}
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage
