//
// move/types.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_TYPES_H
#define MOVE_TYPES_H

#include "move_macros.h"
#include "move_node.h"

#include "cranimation/framefilters.h"

namespace rage {

class crAnimation;
class crFrameFilterMultiWeight;
class mvMotionWeb;

// PURPOSE: Animation loading functor type
// NOTES: Clients can create one of these functors to override move's animation loader
// SEE ALSO: mvMove::InitClass
typedef Functor2Ret<const crAnimation*, const char*, const mvMotionWeb*> mvAnimationLoader;

// PURPOSE: Clip loading functor type
// NOTES: Clients can create one of these functors to override move's clip loader
// SEE ALSO: mvMove::InitClass
typedef Functor4Ret<const crClip*, u32, u32, u32, const mvMotionWeb*> mvClipLoader;	

// PURPOSE: Expression loading functor type
// NOTES: Clients can create one of these functors to override move's expression loader
// SEE ALSO: mvMove::InitClass
typedef Functor3Ret<const crExpressions*, u32, u32, const mvMotionWeb*> mvExpressionLoader;

// PURPOSE: Parameterized motion loading functor type
// NOTES: Clients can create one of these functors to override move's parameterized motion loader
// SEE ALSO: mvMove::InitClass
typedef Functor2Ret<crpmParameterizedMotion*, const char*, const mvMotionWeb*> mvParameterizedMotionLoader;

// PURPOSE: Network definition loading functor type
// NOTES: Clients can create one of these functors to override move's network def loader
// SEE ALSO: mvMove::InitClass
typedef Functor2Ret<const mvNetworkDef*, u32, const mvMotionWeb*> mvNetworkDefLoader;

// PURPOSE: Filter allocator functor type
// NOTES: Clients can create one of these functors to override move's filter allocator
// SEE ALSO: mvMove::InitClass
typedef Functor3Ret<crFrameFilterMultiWeight*, u32, u32, const mvMotionWeb*> mvFilterAllocator;

// PURPOSE: Frame allocator functor type
// NOTES: Clients can create one of these functors to override move's frame allocator
// SEE ALSO: mvMove::InitClass
typedef Functor1Ret<crFrame*, const mvMotionWeb*> mvFrameAllocator;

///////////////////////////////////////////////////////////////////////////////

namespace mvConstant
{
	// PURPOSE: Old data driven filter groups
	// NOTES: Probably not used anymore
	enum
	{
		FILTER_ROOT		= 0,
		FILTER_FOOT_RIGHT,
		FILTER_FOOT_LEFT,
		FILTER_KNEE_RIGHT,
		FILTER_KNEE_LEFT,
		FILTER_HIP_RIGHT,
		FILTER_HIP_LEFT,
		FILTER_PELVIS,
		FILTER_SPINE,
		FILTER_NECK,
		FILTER_HEAD,
		FILTER_FACE,
		FILTER_HAND_RIGHT,
		FILTER_HAND_LEFT,
		FILTER_ELBOW_RIGHT,
		FILTER_ELBOW_LEFT,
		FILTER_SHOULDER_RIGHT,
		FILTER_SHOULDER_LEFT,
		FILTER_MOVER,

		FILTER_COUNT,
	};
};

class crmtNodeBlend;

// PURPOSE: Transition frame filter
// NOTES: Doesn't look like it's used anymore. Investigate further.
class mvTransitionFrameFilter : public crFrameFilterMultiWeight
{
public:
	mvTransitionFrameFilter() : crFrameFilterMultiWeight(kFrameFilterTypeMoveTransition), m_Observer (NULL) { }
	mvTransitionFrameFilter(int numWeightGroups, const crSkeletonData* skelData=NULL) : crFrameFilterMultiWeight(kFrameFilterTypeMoveTransition), m_Observer (NULL) { Init(numWeightGroups, skelData); }
	mvTransitionFrameFilter(datResource &rsc) : crFrameFilterMultiWeight(rsc) { }

	CR_DECLARE_FRAME_FILTER_TYPE(mvTransitionFrameFilter);

	virtual void Update(float weight);

	MOVE_DECLARE_ALLOCATOR();

public: // Internal use only

	void Attach(crmtNodeBlend* node);

private:

	class mvTransitionObserver* m_Observer;
};

// UNUSED:
typedef Functor3Ret<mvTransitionFrameFilter*, u32, u32, const mvMotionWeb*> mvTransitionFilterAllocator;

} // namespace rage

#endif // MOVE_TYPES_H
