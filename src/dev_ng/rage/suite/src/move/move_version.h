//
// move/move_version.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_VERSION_H
#define MOVE_VERSION_H

namespace rage {

/* The numeric compile-time version constants. These constants are the
* authoritative version numbers for MoVE.
*/

/** major version
 * Major API changes that could cause compatibility problems for older
 * programs such as structure size changes.  No binary compatibility is
 * possible across a change in the major version.
*/
#define MOVE_MAJOR_VERSION       2

/** minor version
 * Minor API changes that do not cause binary compatibility problems.
 * Reset to 0 when upgrading APR_MAJOR_VERSION
 */
#define MOVE_MINOR_VERSION       0

/** patch level
 * The Patch Level never includes API changes, simply bug fixes.
 * Reset to 0 when upgrading MOVE_MINOR_VERSION
 */
#define MOVE_PATCH_VERSION       0

/** revision version
 * Indicates work in progress. Reset to 0 when changes are finished.
 */
#define MOVE_REVISION_VERSION	 0

/**
 * Check at compile time if the APR version is at least a certain
 * level.
 * @param major The major version component of the version checked
 * for (e.g., the "1" of "1.3.0").
 * @param minor The minor version component of the version checked
 * for (e.g., the "3" of "1.3.0").
 * @param patch The patch level component of the version checked
 * for (e.g., the "0" of "1.3.0").
 */
#define MOVE_VERSION_AT_LEAST(major,minor,patch)						\
	(((major) < MOVE_MAJOR_VERSION)                                     \
	|| ((major) == MOVE_MAJOR_VERSION && (minor) < MOVE_MINOR_VERSION)	\
	|| ((major) == MOVE_MAJOR_VERSION && (minor) == MOVE_MINOR_VERSION && (patch) <= MOVE_PATCH_VERSION))

#if MOVE_REVISION_VERSION != 0
/** Internal: string form of the "is dev" flag */
#define		MOVE_IS_DEV_STRING "-dev"
#else
#define		MOVE_IS_DEV_STRING ""
#endif

#define MOVE_STRINGIFY(n)	#n

/** The formatted string of MoVE's version */
#define MOVE_VERSION_STRING \
	MOVE_STRINGIFY(MOVE_MAJOR_VERSION) "." \
	MOVE_STRINGIFY(MOVE_MINOR_VERSION) "." \
	MOVE_STRINGIFY(MOVE_PATCH_VERSION) "." \
	MOVE_STRINGIFY(MOVE_REVISION_VERSION)

/** An alternative formatted string of MoVE's version */
#define MOVE_VERSION_STRING_CSV MOVE_MAJOR_VERSION ##,	\
							##MOVE_MINOR_VERSION ##,	\
							##MOVE_PATCH_VERSION ##,	\
							##MOVE_REVISION_VERSION

#ifndef MOVE_VERSION_ONLY

namespace mvVersion
{

struct Version {
	int Major;
	int Minor;
	int Patch;
	int Revision;
};

inline void GetVersion(Version* version)
{
	version->Major = MOVE_MAJOR_VERSION;
	version->Minor = MOVE_MINOR_VERSION;
	version->Patch = MOVE_PATCH_VERSION;
	version->Revision = MOVE_REVISION_VERSION;
}

inline const char* GetVersionString()
{
	return MOVE_VERSION_STRING;
}

}
#endif // MOVE_VERSION_ONLY

} // namespace rage

#endif // MOVE_VERSION_H
