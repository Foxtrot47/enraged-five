//
// move/move_noden.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_noden.h"

#include "move_parameterbuffer.h"
#include "move_funcs.h"
#include "move_macros.h"
#include "move_state.h"
#include "move_synchronizer.h"
#include "move_types_internal.h"

#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeblendn.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

unsigned mvNodeNDef::GetFilterNFlagFrom(u32 flags)
{
	return flags & 0x00000003;
}

unsigned mvNodeNDef::GetFilterFlagFrom(u32 flags)
{
	return (flags & 0x0000000c) >> 0x02;
}

unsigned mvNodeNDef::GetIsChildZeroDestinationFlagFrom(u32 flags)
{
	return (flags & 0x00000010) >> 0x04;
}

unsigned mvNodeNDef::GetTransitionalFlagFrom(u32 flags)
{
	return (flags & 0x00000040) >> 0x06;
}

unsigned mvNodeNDef::GetImmutableFlagFrom(u32 flags)
{
	return (flags & 0x0007FF80) >> 0x07;
}

unsigned mvNodeNDef::GetSynchronizerFrom(u32 flags)
{
	return (flags & 0x00180000) >> 0x13;
}

unsigned mvNodeNDef::GetSourceCountFrom(u32 flags)
{
	return (flags & 0xfc000000) >> 0x1a;
}

unsigned mvNodeNDef::GetChildWeightFlagFrom(u32 flags)
{
	return flags & 0x00000003;
}

unsigned mvNodeNDef::GetChildFilterFlagFrom(u32 flags)
{
	return (flags & 0x00000030) >> 0x04;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeNDef::Create(crmtNodeN* node, mvCreateContext* context) const
{
	node->Init();

	const mvParameterBuffer* params = context->m_Parameters;
	const mvMotionWeb* network = context->m_Network;

	u32 flags = m_Flags;

	unsigned filterf = GetFilterFlagFrom(flags);
	unsigned zeroDestF = GetIsChildZeroDestinationFlagFrom(flags);
//	unsigned transitional = GetTransitionalFlagFrom(flags);  // NOTE - transitional not possible here (only on blend2)
	unsigned immutable = GetImmutableFlagFrom(flags);
	unsigned sync = GetSynchronizerFrom(flags);
	unsigned count = GetSourceCountFrom(flags);
	Assert(count <= crmtNodeN::sm_MaxChildren);
	node->SetNumChildren(count);


	u32* block = (u32*)(this + 1);
	u32 tag = 0;
	if (sync == mvConstant::kSynchronizerTag)
	{
		tag = ReadUInt32Block();
	}

	// filter
	crFrameFilter* filter = NULL;
	switch (filterf)
	{
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetFilterUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterFilter, &ret);
			filter = param->GetFilter();
		}
		break;
	case mvConstant::kFlagValue:
		{
			filter = mvAllocAndInitFilter(network, block);
			u32 len = *block;
			++block;
			block = (u32*)((u8*)block + len);
		}
		break;
	}

	node->SetFilter(filter);

	if (zeroDestF) {
		node->SetZeroDestination(true);
	}

	if (node->GetNodeType() == crmtNode::kNodeAddN)
	{
		mvSynchronizer::AttachToNwayAdd(*(crmtNodeAddN*)node, sync, tag, immutable);
	}
	else
	{
		mvSynchronizer::AttachToNway(*node, sync, tag, immutable);
	}

	Offset* offset = (Offset*)(block);
	for (u32 i = 0; i < count; ++i)
	{
		mvNodeDef* def = offset->Get<mvNodeDef>();
		crmtNode* child = def->Create(context);

		node->AddChild(*child);

		++offset;
	}

	u32* weights = (u32*)offset;

	u32 words = ((count*2 | 7) + 1) >> 3;
	block = weights + words;

	for (u32 i = 0; i < count; ++i)
	{
		// there are 8 possible flag sets in each word
		u32 idx = (i*2 & ~7) >> 3;
		u32 set = (i*2 & 7);
		u32 flag = weights[idx] >> (set * kShift);

		unsigned weightcf = GetChildWeightFlagFrom(flag);
		unsigned filtercf = GetChildFilterFlagFrom(flag);

		// weight init
		switch (weightcf)
		{
		case mvConstant::kFlagValue:
			{
				float weight = ReadFloatBlock();
				node->SetWeight(i, weight);
			}
			break;
		case mvConstant::kFlagSignal:
			{
				u32 id = ReadUInt32Block();
				mvInitParameter<float> ret(0.f);
				const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterReal, &ret);
				float weight = param->GetReal();
				node->SetWeight(i, weight);
			}
			break;
		}

		// filter input init
		switch (filtercf)
		{
		case mvConstant::kFlagValue:
			{
				crFrameFilter* filter = mvAllocAndInitFilter(network, block);
				u32 len = *block;
				++block;
				block = (u32*)((u8*)block + len);
				node->SetInputFilter(i, filter);
			}
			break;
		case mvConstant::kFlagSignal:
			{
				u32 id = ReadUInt32Block();
				mvParameter ret; ret.SetFilterUnsafe(NULL);
				const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterFilter, &ret);
				crFrameFilter* filter = param->GetFilter();
				node->SetInputFilter(i, filter);
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeNDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_Flags, 1);

	u32 flags = m_Flags;

	unsigned filternf = GetFilterNFlagFrom(flags);
	MV_VALIDATE(filternf < mvConstant::kFlagCount, "(mvNodeNDef) filtern flag out of range");
	unsigned filterf = GetFilterFlagFrom(flags);
	MV_VALIDATE(filterf < mvConstant::kFlagCount, "(mvNodeNDef) filter flag out of range");
	unsigned sync = GetSynchronizerFrom(flags);
	MV_VALIDATE(sync < mvConstant::kSynchronizerCount, "(mvNodeNDef) sync flag out of range");
	unsigned count = GetSourceCountFrom(flags);
	MV_VALIDATE(count <= crmtNodeN::sm_MaxChildren, "(mvNodeNDef) child count out of range");

	void* block = this + 1;

	if (sync == mvConstant::kSynchronizerTag)
	{
		u32* dest = (u32*)block;
		resource->ReadInt(dest, 1);
		block = dest+1;
	}

	// filtern
	MV_FILTERN_BLOCK_INIT(block, resource, filternf);
	// filter
	MV_FILTER_BLOCK_INIT(block, resource, filterf);

	s32* offset = (s32*)block;
	for (u32 i = 0; i < count; ++i, ++offset)
	{
		resource->ReadInt(offset, 1);
	}

	u32* weights = (u32*)offset;
	u32* weight = weights;
	u32 words = ((count*2 | 7) + 1) >> 3;

	for (u32 i = 0; i < words; ++i, ++weight)
	{
		resource->ReadInt(weight, 1);
	}

	block = weight;

	for (u32 i = 0; i < count; ++i)
	{
		u32 idx = (i*2 & ~7) >> 3;
		u32 set = (i*2 & 7);

		u32 flag = weights[idx] >> (set * kShift);

		unsigned weightcf = GetChildWeightFlagFrom(flag);
		unsigned filtercf = GetChildFilterFlagFrom(flag);

		MV_REAL_BLOCK_INIT_FLAG(block, resource, weightcf);
		MV_FILTER_BLOCK_INIT(block, resource, filtercf);
	}
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeNDef::QueryMemorySize(u32 size) const
{
	const void* block = this + 1;
	u32 flags =  m_Flags;
	
	unsigned filternf = GetFilterNFlagFrom(flags);
	unsigned filterf = GetFilterFlagFrom(flags);
	unsigned sync = GetSynchronizerFrom(flags);
	unsigned count = GetSourceCountFrom(flags);

	if (sync == mvConstant::kSynchronizerTag)
	{
		size += sizeof(u32);
		block = (u32*)block + 1;
	}

	// filtern
	MV_FILTERN_BLOCK_SIZE(block, filternf, size);
	// filter
	MV_FILTER_BLOCK_SIZE(block, filterf, size);

	size += sizeof(Offset)*count;
	
	u32* inputs = (u32*)((u8*)this + size);

	u32 words = ((count*2 | 7)+1) >> 3;
	size += words * sizeof(u32);

	for (u32 i = 0; i < count; ++i)
	{
		u32 idx = (i*2 & ~7) >> 3;
		u32 set = (i*2 & 7);

		u32 flag = inputs[idx] >> (set * kShift);
		unsigned weightcf = GetChildWeightFlagFrom(flag);
		unsigned filtercf = GetChildFilterFlagFrom(flag);

		void* p = (u32*)((u8*)this + size);

		MV_REAL_BLOCK_SIZE_FLAG(p, weightcf, size);
		MV_FILTER_BLOCK_SIZE(p, filtercf, size);
	}

	return size;
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage
