//
// move/blendn.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_blendn.h"

#include "system/nelem.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeBlendNDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeBlendN* mvNodeBlendNDef::Create(mvCreateContext* context) const
{
	mvNodeBlendN* node = mvNodeBlendN::CreateNode(*context->m_MotionTree);
	mvNodeNDef::Create(node, context);
	
	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeBlendNDef::Init(mvMemoryResource* resource)
{
	mvNodeNDef::Init(resource);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeBlendNDef::QueryMemorySize() const
{
	return mvNodeNDef::QueryMemorySize(sizeof(mvNodeBlendNDef));
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeBlendNDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		NULL,
		MOVE_MAKE_SIGNAL(mvNodeBlendN::ApplyFilter),
		MOVE_MAKE_SIGNAL(mvNodeBlendN::ApplyWeight),
		MOVE_MAKE_SIGNAL(mvNodeBlendN::ApplyInputFilter)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeBlendNDef::ResolveParameterSetType(u16 parameter)
{
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterFilterN,
		mvParameter::kParameterFilter,
		mvParameter::kParameterReal,
		mvParameter::kParameterFilter,
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeBlendNDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		NULL,
		MOVE_MAKE_SIGNAL(mvNodeBlendN::GetFilter),
		MOVE_MAKE_SIGNAL(mvNodeBlendN::GetWeight),
		MOVE_MAKE_SIGNAL(mvNodeBlendN::GetInputFilter)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeBlendNDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeBlendN, crmtNodeBlendN, mvNode::kNodeBlendN, mvNodeBlendNDef);

///////////////////////////////////////////////////////////////////////////////

} // namespace rage
