//
// move/command.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_command.h"

#include "move_network.h"

#include "math/simplemath.h"


namespace rage {

////////////////////////////////////////////////////////////////////////////////

mvCommandBuffer::mvCommandBuffer(Manager* mgr)
: m_HasSignal(false)
, m_Manager(NULL)
, m_NextBuffer(NULL)
, m_PrevBuffer(NULL)
{
	SetManager(mgr);
}

////////////////////////////////////////////////////////////////////////////////

mvCommandBuffer::~mvCommandBuffer()
{
	Shutdown();

	SetManager(NULL);
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::Init(u16)
{
	// TODO -- RESERVE BUFFER SIZE
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::Shutdown()
{
	m_Buffer.Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::Reset()
{
	m_Buffer.Reset();
	m_HasSignal = false;
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::Clear()
{
	m_Buffer.Clear();
	m_HasSignal = false;
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::Append(mvParameterBuffer& buf)
{
	m_Buffer.Append(buf);
	m_HasSignal = true;
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::PrepareBuffer(mvMotionWeb& network, const mvNetworkDef& def, mvParameterBuffer& buffer)
{
	if(m_HasSignal)
	{	
		m_Buffer.ConvertToSignal(&def, network.GetSignalData());
		m_HasSignal = false;
	}

	buffer.Move(m_Buffer);
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void mvCommandBuffer::Dump(atString& output) const
{	
	m_Buffer.Dump(output);
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

mvCommandBuffer::Manager::Manager()
: m_HeadBuffer(NULL)
, m_CurrBuffer(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

mvCommandBuffer::Manager::~Manager()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::Manager::Init(u32 ASSERT_ONLY(maxCommandBuffers))
{
	Assert(maxCommandBuffers > 0);

	Assert(!m_HeadBuffer);
	Assert(!m_CurrBuffer);
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::Manager::Shutdown()
{
	while(m_HeadBuffer)
	{
		DeRegister(*m_HeadBuffer);
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::Manager::Update(u32 bufferCountThreshold, u32 maxInspections, u32 maxCompacts)
{
	if(m_HeadBuffer)
	{
		for(u32 i=0; i<maxInspections && maxCompacts>0; ++i)
		{
			if(!m_CurrBuffer)
			{
				m_CurrBuffer = m_HeadBuffer;
			}

			if(m_CurrBuffer->m_Buffer.GetCount() > bufferCountThreshold)
			{
				m_CurrBuffer->m_Buffer.DeDuplicate();
				m_CurrBuffer->m_Buffer.Compact();
				maxCompacts--;
			}

			m_CurrBuffer = m_CurrBuffer->m_NextBuffer;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void mvCommandBuffer::SetManager(Manager* mgr)
{
	if(m_Manager != mgr)
	{
		if(m_Manager)
		{
			m_Manager->DeRegister(*this);
			m_Manager = NULL;
		}

		if(mgr)
		{
			if(AssertVerify(mgr->Register(*this)))
			{
				m_Manager = mgr;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


