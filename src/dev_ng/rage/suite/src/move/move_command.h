//
// move/command.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_COMMAND_H
#define MOVE_COMMAND_H

#include "move_parameterbuffer.h"

#include "system/criticalsection.h"

#define MOVE_COMMAND_H_SAFE

namespace rage
{

class mvMotionWeb;
class mvNetworkDef;
class mvObserver;
class mvSubNetwork;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Move command buffer
// Wraps a move parameter buffer, adding flag handling and managed garbage collection
class mvCommandBuffer
{
public:

	class Manager;

public:

	// TODO --- handle registration / management of deduplicate/compact here
	// since growing is only an issue for command buffers

	// PURPOSE: Constructor
	mvCommandBuffer(Manager* mgr=NULL);

	// PURPOSE: Destructor
	~mvCommandBuffer();

	// PURPOSE: Initialize
	void Init(u16 reservedCapacity);

	// PURPOSE:
	void Shutdown();

	// PURPOSE: Release any references and reset to default empty state
	void Reset();

	// PURPOSE: Release any references and empty (but keep current capacity)
	void Clear();

	// PURPOSE: Append contents of a buffer into this command buffer
	// WARNING: This is *DESTRUCTIVE* to the buffer passed in
	void Append(mvParameterBuffer&);

	// PURPOSE: Append to command buffer
	void AppendSetAnimation(u32 id, const crAnimation* value);
	void AppendSetClip(u32 id, const crClip* value);
	void AppendSetExpressions(u32 id, const crExpressions* value);
	void AppendSetFilter(u32 id, crFrameFilter* value);
	void AppendSetFrame(u32 id, crFrame* value);
	void AppendSetParameterizedMotion(u32 id, crpmParameterizedMotion* value);
	void AppendSetUserData(u32 id, void* value);
	void AppendSetFloat(u32 id, float value);
	void AppendSetBoolean(u32 id, bool value);
	void AppendSetObserver(u32 id, mvObserver* value);
	void AppendBroadcastRequest(u32 id);
	void AppendSetFlag(u32 id);
	void AppendResetFlag(u32 id);
	void AppendSetNetwork(u32 id, mvSubNetwork* network);
	void AppendForceSelectState(u32 parent, u32 select);


	// PURPOSE: Prepare the parameter buffer and network by appending parameters/requests/flags
	// NOTES: This is handled from the network.
	// SEE ALSO: mvNetwork::PrepareBuffers
	void PrepareBuffer(mvMotionWeb& network, const mvNetworkDef& def, mvParameterBuffer& buffer);

	MOVE_DECLARE_ALLOCATOR();


#if __BANK
	// PURPOSE: Dump the contents of the buffer
	void Dump(atString& output) const;
#endif //__BANK

	// PURPOSE: Get internal parameter buffer
	mvParameterBuffer& GetInternalBuffer();

	// PURPOSE: Manager for all the command buffers
	class Manager
	{
	public:

		// PURPOSE: Constructor
		Manager();

		// PURPOSE: Destructor
		~Manager();

		// PURPOSE: Initialize manager with maximum number of command buffers it can track
		void Init(u32 maxCommandBuffers);

		// PURPOSE: Shutdown
		void Shutdown();

		// PURPOSE: Update manager, call once per frame
		// WARNING: Call from safe DMZ only (when no command buffers are in active use on other threads)
		void Update(u32 bufferCountThreshold=64, u32 maxInspections=32, u32 maxCompacts=4);

	private:

		// PURPOSE: Internal call, register a new command buffer with manager
		bool Register(mvCommandBuffer&);

		// PURPOSE: Internal call, de-register a command buffer
		bool DeRegister(mvCommandBuffer&);

	private:

		sysCriticalSectionToken m_CsToken;

		mvCommandBuffer* m_HeadBuffer;
		mvCommandBuffer* m_CurrBuffer;

		friend class mvCommandBuffer;
	};

	// PURPOSE: Set, change or clear the manager
	// NOTE: Buffer will de-register with existing manager (if any) and register with new manager (if any)
	void SetManager(Manager*);

private:

	mvParameterBuffer m_Buffer;

	bool m_HasSignal;

	Manager* m_Manager;

	mvCommandBuffer* m_NextBuffer;
	mvCommandBuffer* m_PrevBuffer;
};

////////////////////////////////////////////////////////////////////////////////

typedef mvBuffers<mvCommandBuffer> mvCommandBuffers;

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetAnimation(u32 id, const crAnimation* anim)
{
	m_Buffer.Insert(id, anim);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetClip(u32 id, const crClip* clip)
{
	m_Buffer.Insert(id, clip);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetExpressions(u32 id, const crExpressions* expr)
{
	m_Buffer.Insert(id, expr);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetFilter(u32 id, crFrameFilter* filter)
{
	m_Buffer.Insert(id, filter);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetFrame(u32 id, crFrame* frame)
{
	m_Buffer.Insert(id, frame);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetParameterizedMotion(u32 id, crpmParameterizedMotion* pm)
{
	m_Buffer.Insert(id, pm);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetUserData(u32 id, void* data)
{
	m_Buffer.Insert(id, data);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetFloat(u32 id, float real)
{
	m_Buffer.Insert(id, real);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetBoolean(u32 id, bool boolean)
{
	m_Buffer.Insert(id, boolean);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetObserver(u32 id, mvObserver* observer)
{
	m_Buffer.Insert(id, observer);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendBroadcastRequest(u32 id)
{
	m_Buffer.InsertRequest(id, true);
	m_HasSignal = true;
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetFlag(u32 id)
{
	m_Buffer.InsertFlag(id, true);
	m_HasSignal = true;
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendResetFlag(u32 id)
{
	m_Buffer.InsertFlag(id, false);
	m_HasSignal = true;
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendSetNetwork(u32 id, mvSubNetwork* network)
{
	m_Buffer.Insert(id, network);
}

////////////////////////////////////////////////////////////////////////////////

inline void mvCommandBuffer::AppendForceSelectState(u32 parent, u32 select)
{
	m_Buffer.InsertNodeId(parent, select);
}

////////////////////////////////////////////////////////////////////////////////

inline mvParameterBuffer& mvCommandBuffer::GetInternalBuffer()
{
	return m_Buffer;
}

////////////////////////////////////////////////////////////////////////////////

inline bool mvCommandBuffer::Manager::Register(mvCommandBuffer& buffer)
{
	Assert(!buffer.m_Manager);
	Assert(!buffer.m_NextBuffer);
	Assert(!buffer.m_PrevBuffer);

	sysCriticalSection cs(m_CsToken);

	buffer.m_NextBuffer = m_HeadBuffer;
	if(buffer.m_NextBuffer)
	{
		buffer.m_NextBuffer->m_PrevBuffer = &buffer;
	}
	m_HeadBuffer = &buffer;

	buffer.m_Manager = this;

	return true;
}

////////////////////////////////////////////////////////////////////////////////

inline bool mvCommandBuffer::Manager::DeRegister(mvCommandBuffer& buffer)
{
	Assert(buffer.m_Manager);

	sysCriticalSection cs(m_CsToken);

	if(m_HeadBuffer == &buffer)
	{
		m_HeadBuffer = buffer.m_NextBuffer;
	}
	if(m_CurrBuffer == &buffer)
	{
		m_CurrBuffer = buffer.m_NextBuffer;
	}

	if(buffer.m_PrevBuffer)
	{
		buffer.m_PrevBuffer->m_NextBuffer = buffer.m_NextBuffer;
	}
	if(buffer.m_NextBuffer)
	{
		buffer.m_NextBuffer->m_PrevBuffer = buffer.m_PrevBuffer;
	}

	buffer.m_NextBuffer = NULL;
	buffer.m_PrevBuffer = NULL;

	buffer.m_Manager = NULL;

	return true;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#include "move_parameter.h"

#endif // MOVE_COMMAND_H
