//
// move/move_fixedheap.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_FIXEDHEAP_H
#define MOVE_FIXEDHEAP_H

#include "atl/array.h"

#include "move_config.h"

namespace rage {

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Memory management calls for move heap
// NOTES:
//	This can be used by itself but it's really made available for the below macro
class mvFixedHeap
{
public:

	// PURPOSE: Allocate memory from the move memory heap
	static void* Alloc(int size);

	// PURPOSE: Free memory from the move memory heap
	// NOTES: This can fail if the page cannot be found for the passed in address.
	static void Free(void* ptr);

	// PURPOSE: Free memory from the move memory heap
	static void Free(void* ptr, int size);

	// PURPOSE: Free list
	// Batches free requests from a single thread, while in scope
	struct FreeList
	{
		// PURPOSE: Constructor, being batching free requests
		FreeList();

		// PURPOSE: Destructor, flushes all free requests in single batch
		~FreeList();

	protected:

		// PURPOSE: Append free request to list
		void Append(void*);

		// PURPOSE: Flush all free requests
		void Flush();

	private:

		static const u32 sm_MaxNumPtrs = (RSG_PC || RSG_DURANGO || RSG_ORBIS) ? 1024 : 256;
		atFixedArray<void*, sm_MaxNumPtrs> m_Ptrs;

		friend class mvFixedHeap;
	};

private:

	static void FreeInternal(void* ptr);

};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Declare new/delete overrides so this object will be allocated from the move heap
#define MOVE_DECLARE_ALLOCATOR() \
	void* operator new(size_t, size_t size RAGE_NEW_EXTRA_ARGS_UNUSED) { return mvFixedHeap::Alloc((int)size); } \
	void* operator new(size_t size RAGE_NEW_EXTRA_ARGS_UNUSED) { return mvFixedHeap::Alloc((int)size); } \
	void operator delete(void* ptr) { mvFixedHeap::Free(ptr); }

// PURPOSE: Internal use only
// NOTES:
//	The move fixed heap is initialized and shutdown through mvMemorySystem
namespace mvFixedHeapSystem
{
	// PURPOSE: Initialize fixed heap
	// NOTES:
	//	There is only a fixed a heap in move. This initialization is just to
	//	check some sizes and initialize the heap tracker 'memvisualize' if
	//	RAGE_TRACK_MOVE is enabled.
	void InitializeFixedHeap(void* buffer, u32 size);

	// PURPOSE: Clears page counts for given sizes
	void ShutdownFixedHeap();

#if MV_ENABLE_MEMORY_STATS
	// PURPOSE: Dump some info about requests that came to the heap
	void DisplayStats();
#endif // MV_ENABLE_MEMORY_STATS
}

#if MV_ENABLE_MEMORY_STATS
#define mvDisplayFixedHeapStats()	mvFixedHeapSystem::DisplayStats()
#else
#define	mvDisplayFixedHeapStats()
#endif // MV_ENABLE_MEMORY_STATS

////////////////////////////////////////////////////////////////////////////////

inline void mvFixedHeap::FreeList::Append(void* ptr)
{
	if(Unlikely(m_Ptrs.GetCount() == m_Ptrs.GetMaxCount()))
	{
		Flush();
	}
	m_Ptrs.Append() = ptr;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_FIXEDHEAP_H
