//
// move/move_types.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "move_types.h"

namespace rage
{

CR_IMPLEMENT_FRAME_FILTER_TYPE(mvTransitionFrameFilter, kFrameFilterTypeMoveTransition, crFrameFilterMultiWeight);

} // namespace rage



