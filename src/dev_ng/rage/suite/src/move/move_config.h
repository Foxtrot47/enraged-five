//
// move/move_config.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_CONFIG_H
#define MOVE_CONFIG_H

#include "cranimation/animation_config.h"

// check to make sure the network def is valid. If you disable this then don't be surprised if you get a crash.
#if !__FINAL
#define MV_VALIDATE(c_, s_) if(!Likely(c_)) { Errorf(s_); }
#else
#define MV_VALIDATE(c_, s_)
#endif

// PURPOSE: Enable memory tracking info when there's no more pages to allocate from.
#define MV_ENABLE_MEMORY_STATS (0)

// PURPOSE: Turn this bad boy off when data has been converted with makenetwork to support transitional and immutable flags (only affects transitions)
#define IGNORE_TRANSITION_TRANSITIONAL_THINGY (1)

// PURPOSE: New move tag observer
#define NEW_MOVE_TAGS (0)

// PURPOSE: Generate events on transitions
#define MOVE_TRANSITION_EVENTS (0)

// EXPERIMENTAL DEFINES
#define MOVE_CONCURRENT_TRANSITION_LIMIT (3)

#endif // MOVE_CONFIG_H
