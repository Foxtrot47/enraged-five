//
// move/move_capture.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_CAPTURE_H
#define MOVE_CAPTURE_H

#include "move_node.h"

#include "crmotiontree/nodecapture.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create a capture node
class mvNodeCaptureDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this capture node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeCapture* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 type);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeCaptureDef);

private:
	enum
	{
		kFrameMask = 3,
		kFrameOwnerShift = 4,
		kFrameOwnerMask = 3 << kFrameOwnerShift,
	};

	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES: Flag layout is:
	// Frame - flags & 0x00000003
	u32 m_flags;

	// PURPOSE: offset to the input defintion data
	Offset m_input;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move capture node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeCapture : public crmtNodeCapture
{
public:

	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_FRAME_SET(T, ApplyFrame) \
		MOVE_FRAME_GET(T, GetFrame)

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeCapture)

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeCapture);
};

//////////////////////////////////////////////////////////////////////////

inline void mvNodeCapture::ApplyFrame(crFrame* frame)
{
	crmtNodeCapture::SetFrame(frame);
}

//////////////////////////////////////////////////////////////////////////

inline crFrame* mvNodeCapture::GetFrame() const
{
	return crmtNodeCapture::GetFrame();
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_CAPTURE_H
