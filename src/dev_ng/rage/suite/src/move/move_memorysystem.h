//
// move/move_memorysystem.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_MEMORYSYSTEM_H
#define MOVE_MEMORYSYSTEM_H

namespace rage {

// PURPOSE: Move memory system heap management
namespace mvMemorySystem
{
	enum
	{
		kAlignShift = 4,
		kAlignRound = (1<<kAlignShift)-1,
		kPageMemSizeShift = 13, // 8192
		kPageMemSize = 1<<kPageMemSizeShift,
	};

	// PURPOSE: Initialize move memory system
	// NOTES: size == kPageMemSize * (size/kPageMemSize)
	//	That's really a lame way of saying that you should choose a buffer size
	//	that exactly fits some number of kPageMemSize'd pages
	//	Will initialize the fixed heap as part of the spin up
	void InitializeMemorySystem(void* buffer, u32 size);

	// PURPOSE: Shutdown the fixed heap and cleanup some internal management stuff
	// NOTES:
	//	Ownership of the buffer does NOT reside withing the move memory system.
	//	The calling code must manage that memory itself.
	void ShutdownMemorySystem();

	// PURPOSE: Reserve a new page from the move memory system buffer.
	// NOTES:
	//	This buffer is a chunk of free memory with a page header which is actually a
	//	footer at the end of the buffer. The header just stores the next page in the
	//	series of pages. The fixed heap allocator stomps all over this with its own
	//	header which contains a next ptr, the size of the allocations that are coming
	//	from this page and an internal free list. In effect a page then only contains
	//	kPageMemSize-sizeof(this header, which is 16 bytes).
	//	If MOVE_FAILED_ALLOC_FROM_MAIN exists then overflow pages outside the move
	//	memory system buffer will be allocated on demand. This is a greedy allocation,
	//	pages are never returned to main memory.
	void* AllocatePage();

	// PURPOSE: Return a page to the free pool
	// NOTES:
	//	Pages aren't actually freed. They're just put back into a free list for the next
	//	AllocatePage request. See notes on AllocatePage regarding overflow pages.
	void FreePage(void* page);

	// PURPOSE: Are there any free pages.
	// NOTES:
	//	This could return false, but an AllocatePage will still succeed if
	//	MOVE_FAILED_ALLOC_FROM_MAIN exists.
	bool AvailablePage();

	// PURPOSE: Lookup page this address came from.
	// NOTES:
	//	If this returns NULL, you're in trouble
	void* GetPageForAddress(void* ptr);
}

} // namespace rage

#endif // MOVE_MEMORYSYSTEM_H
