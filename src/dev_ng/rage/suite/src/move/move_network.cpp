//
// move/network.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "move_network.h"

#include "move_command.h"
#include "move_parameterbuffer.h"
#include "move_state.h"
#include "move_types_internal.h"

#include "crmotiontree/motiontree.h"
#include "crmotiontree/request.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

mvMotionWeb::mvMotionWeb()
: m_OutputBuffers(NULL)
, m_NetworkDef(NULL)
, m_RefCount(0)
, m_Entity(NULL)
{
}

//////////////////////////////////////////////////////////////////////////

void mvMotionWeb::AddRef() const
{
	sysInterlockedIncrement(&m_RefCount);
}

//////////////////////////////////////////////////////////////////////////

int mvMotionWeb::Release() const
{
	int refCount = sysInterlockedDecrement(&m_RefCount);
	if (refCount == 0) {
		delete this;
		return 0;
	} else {
		return refCount;
	}
}

//////////////////////////////////////////////////////////////////////////

mvNetwork::mvNetwork(mvParameterBuffers& outputBuffers, const mvNetworkDef* def)
: mvMotionWeb()
{
	m_NetworkDef = def;
	m_OutputBuffers = &outputBuffers;
}

///////////////////////////////////////////////////////////////////////////////

class mvRequestStateRoot : public crmtRequest
{
public:
	// PURPOSE: Default constructor
	mvRequestStateRoot(mvNetwork* rootNetwork, mvNodeStateDef* networkDef, mvCommandBuffers& cmds);

	// PURPOSE: Internal call, this actually generates the node
	virtual crmtNode* GenerateNode(crmtMotionTree& tree, crmtNode* node);

private:
	mvNetwork* m_RootNetwork;
	mvNodeStateDef* m_NetworkDef;
	mvCommandBuffers* m_Cmds;
};

//////////////////////////////////////////////////////////////////////////

mvRequestStateRoot::mvRequestStateRoot(mvNetwork* rootNetwork, mvNodeStateDef* networkDef, mvCommandBuffers& cmds)
: m_RootNetwork(rootNetwork)
, m_NetworkDef(networkDef)
, m_Cmds(&cmds)
{
}

//////////////////////////////////////////////////////////////////////////

crmtNode* mvRequestStateRoot::GenerateNode(crmtMotionTree& tree, crmtNode* node)
{
	mvNodeStateRoot* rootNode = mvNodeStateRoot::CreateNode(tree);
	Assert(rootNode);

	rootNode->Init(m_RootNetwork, m_NetworkDef, *m_Cmds);

	if (node)
	{
		node->Replace(*rootNode);
	}

	return rootNode;
}

//////////////////////////////////////////////////////////////////////////

void mvNetwork::Init(crmtMotionTree& mt, mvCommandBuffers& cmds)
{
	mvNodeStateDef* data = (mvNodeStateDef*)m_NetworkDef->GetNetwork();

	mvRequestStateRoot request(this, data, cmds);
	mt.Request(request);
}

//////////////////////////////////////////////////////////////////////////

void mvNetwork::Dump() const
{
	const request_t* req = m_NetworkDef->GetRequests();
	int reqcount = m_NetworkDef->GetRequestCount();

	Printf("Requests:");
	for (int i=0; i<reqcount; ++i)
	{
		Printf("0x%8X:%s ", req->key, m_SignalData.GetRequest(req->index)? "true" : "false" );
		++req;
	}

	const flag_t* flag = m_NetworkDef->GetFlags();
	int flagcount = m_NetworkDef->GetFlagCount();

	Printf("Flags:");
	for (int i=0; i<flagcount; ++i)
	{
		Printf("0x%8X:%s ", flag->key, m_SignalData.GetFlag(flag->index)? "true" : "false" );
		++flag;
	}

	Printf("\n");
}

//////////////////////////////////////////////////////////////////////////

#if __ASSERT
mvNetworkDef::NetworkDefNameFunctor mvNetworkDef::ms_GetNameFunctor(NULL);
#endif //__ASSERT

//////////////////////////////////////////////////////////////////////////

mvNetwork* mvNetworkDef::Create(mvParameterBuffers& outputBuffers) const
{
	// Check for network data
	if(!m_network)
	{
		return NULL;
	}

	mvNetwork* network = rage_new mvNetwork(outputBuffers, this);
	network->AddRef();

	return network;
}

//////////////////////////////////////////////////////////////////////////

mvNetworkDef::mvNetworkDef()
:	pgBaseRefCounted(0)
,	m_requests (0)
,	m_reqcount (0)
,	m_flags (0)
,	m_flagcount (0)
,	m_XRefCount (0)
,	m_network (0)
#if CR_DEV
,	m_Name (NULL)
#endif
{
}

//////////////////////////////////////////////////////////////////////////

mvNetworkDef::~mvNetworkDef()
{
	delete[] (u8*)(m_XRefs);

#if CR_DEV
	delete[] m_Name;
#endif
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage
