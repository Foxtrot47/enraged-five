//
// move/state.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_STATE_H
#define MOVE_STATE_H

#include "move_network.h"
#include "move_node.h"
#include "move_command.h"


namespace rage {

class crCreature;
class mvActiveTransition;
class mvMotionWeb;
class mvNetwork;
class mvNodeInlinedStateMachineDef;
class mvNodeStateBaseDef;
class mvNodeStateDef;
class mvNodeStateMachineDef;
class phInst;
struct mvTransition;

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Base class for the state machine and root nodes
class mvNodeState : public crmtNodeParent
{
public:
	// PURPOSE: Constructor
	mvNodeState(crmtNode::eNodes nodeType);

	// PURPOSE: Resource constructor
	mvNodeState(datResource&);

	// PURPOSE: Destructor
	virtual ~mvNodeState();

	// PURPOSE: Init call
	// PARAMS:
	//	root - the root network handle
	//	parent - the parent network handle this state resides in
	//	def - the node definition used to create this node
	// NOTES: This will send of an enter id if 'def' defines an enter enabled event
	void Init(mvNetwork* root, mvMotionWeb* parent, const mvNodeStateBaseDef* def);

	// PURPOSE: Shutdown
	// NOTES: Release reference to the parent network. Post event that the state
	//	is exiting.
	virtual void Shutdown();

	// PURPOSE: Update call
	// NOTES: For inline state nodes the tail hangs of the state node
	//	so it has somewhere to be proxied from in the tree. If this is the
	//	case the first child is disabled. The update call makes sure the
	//	flags are correctly set on this node.
	virtual void Update(float dt);
	
#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: Get the state node definition
	const mvNodeStateBaseDef* GetDefinition() const { return m_BaseDef; }

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();

	// PURPOSE: Register with move's type system
	MOVE_DECLARE_REGISTERED_TYPE(mvNodeState);

protected:
	// PURPOSE: Root network for reference counting
	mvNetwork* m_RootNetwork;

	// PURPOSE: The parent network this node resides in
	mvMotionWeb* m_ParentNetwork;

	// PURPOSE: The definition used to create this node
	const mvNodeStateBaseDef* m_BaseDef;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Node controlling a state machine having a current state
class mvNodeStateMachine : public mvNodeState
{
public:
	// PURPOSE: Default constructor
	mvNodeStateMachine();

	// PURPOSE: Resource constructor
	mvNodeStateMachine(datResource&);

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(mvNodeStateMachine);

	// PURPOSE: Initialize this node
	// PARAMS:
	//	web - the parent network this node resides in
	//	network - the root network
	//	def - the definition used to create this node
	//	tail - used for inline state machines
	// NOTES: Will create the initial state from the def data, unless a force state has been pushed
	//	in the parent network, then that will be the first created state. tail must be passed through
	//	the state machine init as opposed to the inline state machine init because it may have to
	//	be passed on to child state nodes until we find the tail call to hook the tail nodes in to the
	//	motion tree.
	void Init(mvMotionWeb* web, mvNetwork* network, const mvNodeStateMachineDef* def, crmtNode* tail);

	// PURPOSE: Initialize this node
	// PARAMS:
	//	web - the parent network in which the node resides
	//	network - the root network
	//	def - the definition used to create this node
	//	node - the tail the attach as input to this state
	void Init(mvMotionWeb* web, mvNetwork* network, const mvNodeInlinedStateMachineDef* def, crmtNode* node);

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Pre update call
	// NOTES: Whether to change state is evaluated here. This state machine node will always
	//	exists in the hierarchy so it can control forced state transitions as well as
	//	regular transitions.
	virtual void PreUpdate(float);

	// PURPOSE: Update call
	// NOTES: Whether to change state is evaluated here. This state machine node will always
	//	exists in the hierarchy so it can control forced state transitions as well as
	//	regular transitions.
	virtual void Update(float);

#if CR_DEV
	// PURPOSE: Dump.  Implement output of debugging information
	virtual void Dump(crmtDumper& dumper) const;
#endif // CR_DEV

	// PURPOSE: Get active definition, for debugging purpose
	const mvNodeStateBaseDef* GetActiveDef() const { return m_StateDef; }

	// PURPOSE: Inhibit state node from making further transitions
	void DisableTransitions();
	
	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();

private:
	// PURPOSE: Internal initialization
	// SEE ALSO: Init
	// NOTES: This is actually where the juice of the Init function takes place
	void InitInternal(bool inlineState, mvMotionWeb* parent, mvNetwork* root, const mvNodeStateBaseDef* def, crmtNode* tail);

	// PURPOSE: Evaluate if we need to transition, must be called each frame
	// NOTES: Checks all the conditions on all the transitions for the current active node.
	//	The first that is fully satisfied will begin a transition. If none are satisfied
	//	the current active state node stays active.
	// SEE ALSO: DoTransition
	bool EvaluateAndTransition();

	// PURPOSE: Apply a transition to a new state node
	// NOTES: Begin transitioning from the current state node to the new state node using the details
	//	found in 'transition'. It's possible to filter the transition using 'filter'. 'params' is
	//	made available so the transition can acquire its duration from a parameter if required as well as
	//	post a blend weight value for that transition once it begins. When this call returns this nodes
	//	concept of the current state will be the new state that was transitioned to, even if there is
	//	a blend duration greater than 0 (ie. Each state machine is only in one state in move, even if
	//	multiple states leave behind nodes in the motion tree).
	void DoTransition(const mvTransition* transition, crFrameFilter* filter, const mvParameterBuffer* params);

	// PURPOSE: Set current state
	// NOTES: Creates the node that the state machine is about to transition too. Fires of on enter event
	//	if required. If and exist signal is required when the node is shutdown an observer is connected
	//	to the node here.
	crmtNode* ApplyState(const mvNodeDef* def);

#if MOVE_CONCURRENT_TRANSITION_LIMIT
	// PURPOSE: Terminate excess concurrent transitions beyond limit
	void TerminateExcessTransitions();
#endif // MOVE_CONCURRENT_TRANSITION_LIMIT
	
	// PURPOSE: The tail nodes if required
	crmtNode* m_Tail;

	// PURPOSE: The definition data used to create this node
	const mvNodeStateBaseDef* m_StateDef;

	// PURPOSE: The lifetime of the current active state. Allows testing of state lifetime in conditions.
	float m_Lifetime;
	
#if MOVE_CONCURRENT_TRANSITION_LIMIT
	mvActiveTransition* m_ActiveTransitions;
	u32 m_NumActiveTransitions;
#endif // MOVE_CONCURRENT_TRANSITION_LIMIT

	// PURPOSE: Is this state machine an inline state machine
	bool m_InlineState;

	// PURPOSE: Inhibit transitions
	bool m_PendingDisableTransitions;
	bool m_DisableTransitions;

	friend class mvActiveTransition;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Node used as the root of a network
// NOTES: The root state is a little special in that it requires some extra processing
//	at the end of its update
class mvNodeStateRoot : public mvNodeState
{
public:
	// PURPOSE: Constructor
	mvNodeStateRoot();

	// PURPOSE: Resource constructor
	mvNodeStateRoot(datResource& rsc);

	// PURPOSE: Initialization
	// NOTES: Call the Create recursively on the network definition. This
	//	will effectively create the default state of the motion tree
	void Init(mvNetwork* root, const mvNodeStateDef* state, mvCommandBuffers& cmds);

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(mvNodeStateRoot);

	// PURPOSE: Reset request flags, update the output buffer
	// NOTES: This will only update the output buffer for the main
	//	network. Subnetworks manage their own output buffer.
	virtual void Update(float);

	// PURPOSE:  We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();
};

//////////////////////////////////////////////////////////////////////////

#if __DEV || __BANK
struct mvDRTrackInterface
{
	virtual void RecordGetAnimValue(const phInst *pInst, const char *pValueName, float fValue) const=0;
	virtual void RecordSetAnimValue(const phInst *pInst, const char *pValueName, float fValue) const=0;
	virtual void RecordSetAnimValue(const phInst *pInst, const char *pStringName, const char *pString1id, const char *pString1, const char *pString2id=0, const char *pString2=0) const=0;
	virtual void RecordCondition(const mvNetworkDef* networkDef, const crCreature* creature, int iPassed, int iConditionType, int iConditionIndex, const mvNodeStateBaseDef* /*from*/, const mvNodeStateBaseDef* to)=0;
	virtual void RecordEndConditions() const=0;
	virtual void RecordTransition(const mvNetworkDef* networkDef, const crCreature* creature, const mvNodeStateBaseDef* stateMachine, const mvNodeStateBaseDef* fromState, const mvNodeStateBaseDef* toState)=0;
	mvDRTrackInterface()
	{
		Assert(!sm_Instance);
		sm_Instance = this;
	}
	virtual ~mvDRTrackInterface()
	{
		Assert(sm_Instance);
		sm_Instance = 0;
	}
	static bool sm_bEnabled;
	static mvDRTrackInterface *sm_Instance;
};
#endif

/////////////////////////////////////////////////////////////////////////

inline void mvNodeStateMachine::DisableTransitions()
{
	if (m_StateDef && !m_StateDef->m_DeferBlockUpdate)
		m_DisableTransitions = true;

	m_PendingDisableTransitions = true;
}

/////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_STATE_H
