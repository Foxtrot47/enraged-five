//
// move/move_extrap.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_EXTRAP_H
#define MOVE_EXTRAP_H

#include "move_node.h"

#include "crmotiontree/nodeextrapolate.h"

namespace rage {

class crFrame;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create an animation node
class mvNodeExtrapolateDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this extrapolate node
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeExtrapolate* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeExtrapolateDef);

private:
	enum
	{
		kDampingMask = 3,
	};

	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES: Flag layout is:
	//	Damping - flags & 0x00000003
	u32 m_flags;

	static unsigned GetDampingFlagFrom(u32 flags) { return flags & 0x00000003; }

	// PURPOSE: Offset to the input nodes definition data
	Offset m_Input;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move extrapolate node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeExtrapolate : public crmtNodeExtrapolate
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_REAL_SET(T, ApplyDamping) \
		MOVE_REAL_GET(T, GetDamping) \
		MOVE_FRAME_GET(T, GetLastFrame) \
		MOVE_FRAME_GET(T, GetDeltaFrame)
	
	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeExtrapolate)

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeExtrapolate);
};

//////////////////////////////////////////////////////////////////////////

inline void mvNodeExtrapolate::ApplyDamping(float damping)
{
	crmtNodeExtrapolate::SetDamping(damping);
}

//////////////////////////////////////////////////////////////////////////

inline float mvNodeExtrapolate::GetDamping() const
{
	return crmtNodeExtrapolate::GetDamping();
}

//////////////////////////////////////////////////////////////////////////

inline crFrame* mvNodeExtrapolate::GetLastFrame() const
{
	return crmtNodeExtrapolate::GetLastFrame();
}

//////////////////////////////////////////////////////////////////////////

inline crFrame* mvNodeExtrapolate::GetDeltaFrame() const
{
	return crmtNodeExtrapolate::GetDeltaFrame();
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_EXTRAP_H
