//
// move/filter.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_filter.h"

#include "move_parameterbuffer.h"
#include "move_funcs.h"
#include "move_macros.h"
#include "move_state.h"
#include "move_types.h"
#include "move_types_internal.h"

#include "cranimation/framefilters.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodefilter.h"

namespace rage {

extern mvFilterAllocator g_mvFilterAllocator;

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeFilterDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeFilter* mvNodeFilterDef::Create(mvCreateContext* context) const
{
	mvNodeFilter* node = mvNodeFilter::CreateNode(*context->m_MotionTree);

	const mvMotionWeb* network = context->m_Network;
	const mvParameterBuffer* params = context->m_Parameters;
	const void* block = this + 1;

	u32 flags = m_Flags;

	// filter
	crFrameFilter* filter = NULL;
	switch (flags & FLAG_MASK)
	{
	case mvConstant::kFlagValue:
		{
			filter = mvAllocAndInitFilter(network, block);
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
	
			mvParameter ret; ret.SetFilterUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterFilter, &ret);
			filter = param->GetFilter();
		}
		break;
	}

	node->Init(filter);

	mvNodeDef* input = m_Input.Get<mvNodeDef>();
	crmtNode* child = input->Create(context);

	node->AddChild(*child);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeFilterDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_Flags, 1);

	m_Input.Init(resource);

	void* block = this + 1;
	u32 flags = m_Flags;

	// filter
	MV_FILTER_BLOCK_INIT(block, resource, flags & kFilterMask);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeFilterDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeFilterDef);

	const void* block = this+1;
	u32 flags = m_Flags;

	// filter
	MV_FILTER_BLOCK_SIZE(block, (flags & kFilterMask), size);

	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeFilterDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeFilter::ApplyFilter)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeFilterDef::ResolveParameterSetType(u16)
{
	return mvParameter::kParameterFilter;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeFilterDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeFilter::GetFilter)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeFilterDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeFilter, crmtNodeFilter, mvNode::kNodeFilter, mvNodeFilterDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
