//
// move/frame.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_frame.h"

#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeframe.h"

#include "move_parameterbuffer.h"
#include "move_macros.h"
#include "move_state.h"
#include "move_types_internal.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeFrameDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeFrame* mvNodeFrameDef::Create(mvCreateContext* context) const
{
	mvNodeFrame* node = mvNodeFrame::CreateNode(*context->m_MotionTree);
	const mvParameterBuffer* params = context->m_Parameters;
	u32 flags = m_Flags;

	void* block = (void*)(this + 1);

	// frame init
	crFrame* frame = NULL;
	switch (flags & kFrameMask)
	{
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetFrameUnsafe(NULL);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterFrame, &ret);
			frame = param->GetFrame();
		}
		break;
	}

	// owner init
	bool owner = false;

	switch ((flags & kFrameOwnerMask) >> kFrameOwnerShift)
	{
	case mvConstant::kFlagValue:
		{
			block=((u32*)block)+1;
		}
		break;
	case mvConstant::kFlagSignal:
		{
			u32 id = ReadUInt32Block();
			mvParameter ret; ret.SetBool(owner);
			const mvParameter* param = params->FindFirstOfType(id, mvParameter::kParameterBoolean, &ret);
			owner = param->GetBool();
		}
		break;
	}

	node->Init(frame);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeFrameDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_Flags, 1);

	u32* block = (u32*)(this + 1);
	u32 flags = m_Flags;

	// frame
	MV_FRAME_BLOCK_INIT(block, resource, flags, kFrameMask);
	// frame owner
	MV_BOOLEAN_BLOCK_INIT(block, resource, flags, kFrameOwnerMask);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeFrameDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodeFrameDef);

	const void* block = this+1;
	u32 flags = m_Flags;

	// frame
	MV_FRAME_BLOCK_SIZE(block, flags, kFrameMask, size);
	// frame owner
	MV_BOOLEAN_BLOCK_SIZE(block, flags, kFrameOwnerMask, size);

	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeFrameDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeFrame::ApplyFrame)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeFrameDef::ResolveParameterSetType(u16)
{
	return mvParameter::kParameterFrame;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeFrameDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodeFrame::GetFrame)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeFrameDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeFrame, crmtNodeFrame, mvNode::kNodeFrame, mvNodeFrameDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
