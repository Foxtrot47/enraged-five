//
// move/addn.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_addn.h"

#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeaddn.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodeAddNDef);

///////////////////////////////////////////////////////////////////////////////

mvNodeAddN* mvNodeAddNDef::Create(mvCreateContext* context) const
{
	mvNodeAddN* node = mvNodeAddN::CreateNode(*context->m_MotionTree);
	mvNodeNDef::Create(node, context);

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodeAddNDef::Init(mvMemoryResource* resource)
{
	mvNodeNDef::Init(resource);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeAddNDef::QueryMemorySize() const
{
	return mvNodeNDef::QueryMemorySize(sizeof(mvNodeAddNDef));
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodeAddNDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		NULL,
		MOVE_MAKE_SIGNAL(mvNodeAddN::ApplyFilter),
		MOVE_MAKE_SIGNAL(mvNodeAddN::ApplyWeight),
		MOVE_MAKE_SIGNAL(mvNodeAddN::ApplyInputFilter)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodeAddNDef::ResolveParameterSetType(u16 parameter)
{
	static mvParameter::ParamType types[] = {
		mvParameter::kParameterFilterN,
		mvParameter::kParameterFilter,
		mvParameter::kParameterReal,
		mvParameter::kParameterFilter,
	};

	Assert(parameter < NELEM(types));
	return types[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodeAddNDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		NULL,
		MOVE_MAKE_SIGNAL(mvNodeAddN::GetFilter),
		MOVE_MAKE_SIGNAL(mvNodeAddN::GetWeight),
		MOVE_MAKE_SIGNAL(mvNodeAddN::GetInputFilter)
	};

	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodeAddNDef::ResolveEvent(u16)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodeAddN, crmtNodeAddN, mvNode::kNodeAddN, mvNodeAddNDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
