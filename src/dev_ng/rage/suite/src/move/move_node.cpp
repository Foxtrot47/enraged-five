//
// move/move_node.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_node.h"

#include "move_add.h"
#include "move_addn.h"
#include "move_anim.h"
#include "move_blend.h"
#include "move_blendn.h"
#include "move_capture.h"
#include "move_clip.h"
#include "move_expr.h"
#include "move_extrap.h"
#include "move_filter.h"
#include "move_frame.h"
#include "move_identity.h"
#include "move_ik.h"
#include "move_invalid.h"
#include "move_jointlimit.h"
#include "move_merge.h"
#include "move_mergen.h"
#include "move_mirror.h"
#include "move_pm.h"
#include "move_pose.h"
#include "move_proxy.h"
#include "move_reference.h"
#include "move_state.h"
#include "move_subnetwork.h"
#include "move_synchronizer.h"

namespace rage {

atArray<const mvNode::NodeTypeInfo*> mvNode::sm_NodeTypeInfos;

//////////////////////////////////////////////////////////////////////////

crmtNode* mvNodeDef::Create(mvCreateContext* context) const
{
	const mvNode::NodeTypeInfo* info = mvNode::sm_NodeTypeInfos[m_type];
	crmtNode* node = info->m_CreateFn(this, context);

#if CRMT_NODE_2_DEF_MAP
	if(PARAM_movedebug.Get())
	{
		const void **ppDef = g_crmtNode2DefMap.Access(node);
		if(ppDef != NULL)
		{
			if(*ppDef == NULL)
			{
				g_crmtNode2DefMap.Delete(node);
				g_crmtNode2DefMap.Insert(node, this);
			}
		}
		else
		{
			g_crmtNode2DefMap.Insert(node, this);
		}
	}
#endif //CRMT_NODE_2_DEF_MAP

	crmtNode** children = context->m_Children;
	if(children && info->m_NodeType!=mvNode::kNodeTail)
	{
		FastAssert(m_index < context->m_ChildrenCount);
		children[m_index] = node;
	}

	return node;
}

//////////////////////////////////////////////////////////////////////////

void mvNodeDef::Init(mvMemoryResource* resource)
{
	const mvNode::NodeTypeInfo* info = mvNode::sm_NodeTypeInfos[m_type];
	info->m_InitFn(this, resource);
}

//////////////////////////////////////////////////////////////////////////

u32 mvNodeDef::QueryMemorySize() const
{
	const mvNode::NodeTypeInfo* info = mvNode::sm_NodeTypeInfos[m_type];
	return info->m_QueryFn(this);
}

//////////////////////////////////////////////////////////////////////////

void mvNode::InitClass()
{
	static bool initialized = false;
	if (!initialized) {
		sm_NodeTypeInfos.Resize(kNodeCount);
		for (int i = 0; i < sm_NodeTypeInfos.GetCount(); ++i) {
			sm_NodeTypeInfos[i] = NULL;
		}

		mvNodeSubNetworkClass::InitClass();
		mvNodeReferenceClass::InitClass();
		mvNodeStateMachineClass::InitClass();

		mvNodeStateMachine::InitClass();
		mvNodeStateRoot::InitClass();
		mvNodeTail::InitClass();
		mvNodeInlinedStateMachine::InitClass();
		mvNodeAnimation::InitClass();
		mvNodeBlend::InitClass();
		mvNodeAddSubtract::InitClass();
		mvNodeFilter::InitClass();
		mvNodeMirror::InitClass();
		mvNodeFrame::InitClass();
		mvNodeIk::InitClass();
		mvNodeBlendN::InitClass();
		mvNodeClip::InitClass();
		mvNodePm::InitClass();
		mvNodeExtrapolate::InitClass();
		mvNodeExpression::InitClass();
		mvNodeCapture::InitClass();
		mvNodeProxy::InitClass();
		mvNodeAddN::InitClass();
		mvNodeIdentity::InitClass();
		mvNodeMerge::InitClass();
		mvNodePose::InitClass();
		mvNodeMergeN::InitClass();
		mvNodeState::InitClass();
		mvNodeInvalid::InitClass();
		mvNodeJointLimit::InitClass();
		mvNodeSubNetwork::InitClass();
		mvNodeReference::InitClass();

		mvSynchronizerTag::InitClass();

		initialized = true;
	}
}

//////////////////////////////////////////////////////////////////////////

mvNode::NodeTypeInfo::NodeTypeInfo(u32 nodetype, NodeCreateFn* createfn, NodeInitFn* initfn, NodeQueryFn* queryfn, ResolveParameterSetFn* setfn, ResolveParameterSetTypeFn* setTypeFn, ResolveParameterGetFn* getfn, ResolveEventFn* eventfn)
:	m_NodeType (nodetype)
,	m_CreateFn (createfn)
,	m_InitFn (initfn)
,	m_QueryFn (queryfn)
,	m_ResolveParameterSetFn (setfn)
,	m_ResolveParameterSetTypeFn(setTypeFn)
,	m_ResolveParameterGetFn (getfn)
,	m_ResolveEventFn (eventfn)
{
}

//////////////////////////////////////////////////////////////////////////

mvNode::NodeTypeInfo::~NodeTypeInfo()
{
}

//////////////////////////////////////////////////////////////////////////

void mvNode::NodeTypeInfo::Register() const
{
	mvNode::RegisterNodeTypeInfo(*this);
}

//////////////////////////////////////////////////////////////////////////

void mvNode::RegisterNodeTypeInfo(const mvNode::NodeTypeInfo& info)
{
	u32 type = info.m_NodeType;
	if (type < kNodeCount) {
		sm_NodeTypeInfos[type] = &info;
	}
	else {
		sm_NodeTypeInfos.Grow() = &info;
	}
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage
