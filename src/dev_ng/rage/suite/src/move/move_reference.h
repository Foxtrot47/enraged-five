//
// move/move_reference.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_REFERENCE_H
#define MOVE_REFERENCE_H

#include "move_network.h"

#define MV_ENABLE_REFERENCE_OUTPUT_SIGNALS (0)

namespace rage {

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create a reference node
class mvNodeReferenceDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this reference node defintion
	// PARAMS: context - information about this nodes position in the motion tre/network hierarchy
	crmtNode* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'param'
	// NOTES: param is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 param);

	// PURPOSE: Get the type of the parameter that the set function at index 'param' requires
	// NOTES: param is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 param);

	// PURPOSE: Get the type of the parameter that the set function at index 'param' requires
	// NOTES: param is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 param);

	// PURPOSE: Get the event id to fire at event index 'event'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeReferenceDef);

	// PURPOSE: Helper memory layout so we can read connect the parameters from
	//	the parent network and feed them into the referenced network.
	struct ParameterSourceData {
		u32 Type;
		u32 Key;
		union PayloadData
		{
			Offset Filename;
			float Float;
		} Payload;
	};

private:
	// PURPOSE: The id of this network
	// NOTES: usable with MOVE_FindRegisteredNetwork
	u32 m_NetworkId;

	// PURPOSE: Offset to the parameter source data
	Offset m_SourceParameterData;

	// PURPOSE: number of parameter connectors found at m_SourceParameterData
	u32 m_SourceParameterCount;

	// PURPOSE: Number of actual input parameters in this reference network
	u32 m_ParameterCount;

	// PURPOSE: Number of flags in this network
	u32 m_FlagCount;

	// PURPOSE: Number of requests in this network
	u32 m_RequestCount;

#if MV_ENABLE_REFERENCE_OUTPUT_SIGNALS
	Offset m_OutputParameterData;
	u32 m_OutputParameterCount;
#endif
	friend class mvNodeReference;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move reference node
// NOTES: A reference node is a container node that allows another network
//	to be referenced into another network hierarchy. This is a useful
//	mechanism for sharing pieces of network. The reference node does this
//	by remapping the parent networks parameters into its own internal understanding
//	of it's parameters.
class mvNodeReference : public crmtNodeParent, public mvMotionWeb
{
public:
	// PURPOSE: Default constructor
	mvNodeReference();

	// PURPOSE: Resource constructor
	mvNodeReference(datResource&);

	// PURPOSE: Node type registration
	CRMT_DECLARE_NODE_TYPE(mvNodeReference);

	// PURPOSE: Initialize this node
	// NOTES: This will find the network to use from the 'def' m_NetworkId.
	//	It is assumed that this network has already been loaded into memory
	//	which should have been the case by the parent networks load call.
	//	Any parameters that require remapping will be pulled from the parent
	//	networks parameter buffer and pushed into the reference networks
	//	buffer.
	void Init(mvNetwork* root, mvMotionWeb* web, const mvNodeReferenceDef* def);

	// PURPOSE: Pre update call
	// NOTES: Any parameters that require remapping will be pulled from teh parent
	//	networks parameter buffer and pushed into the refrence networks buffer.
	virtual void PreUpdate(float);

	// PURPOSE: Update call
	// NOTES: update any output parameter buffers
	// EXTRA: In looking over this function there seems to be a lot of duplicated
	//	code from PreUpdate. I'm not sure what that's about.
	virtual void Update(float);

	// PURPOSE: We allocate from move's buffer
	MOVE_DECLARE_ALLOCATOR();

private:
	// PURPOSE: The root network for the main network hierarchy
	mvNetwork* m_RootNetwork;

	// PURPOSE: The parent network that this reference node resides in
	mvMotionWeb* m_Web;

	// PURPOSE: The definition from the parent network def used to create this node
	const mvNodeReferenceDef* m_Def;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Register this class with the move type system
MOVE_DECLARE_REGISTERED_CLASS(mvNodeReferenceClass);

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_REFERENCE_H