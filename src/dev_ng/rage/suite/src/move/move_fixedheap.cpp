//
// move/move_fixedheap.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "move_fixedheap.h"

#include "diag/tracker.h"
#include "move_memorysystem.h"
#include "system/criticalsection.h"
#include "system/memops.h"
#include "system/memory.h"
#include "system/memvisualize.h"

#if MV_ENABLE_MEMORY_STATS
#include "profile/page.h"
#include "profile/group.h"
#include "profile/element.h"
#endif //MV_ENABLE_MEMORY_STATS

#define DO_VALIDATE 0
#define DO_MTF_ON_ALLOCATE 1
#define DO_MTE_ON_FREE_EMPTY 1
#define DO_MTE_ON_NEW 0

#if __DEV
#define DO_INVALIDATE_DATA(dest,c,count) sysMemSet(dest,c,count)
#else
#define DO_INVALIDATE_DATA(dest,c,count)
#endif

#define RAGE_TRACK_MOVE (RAGE_TRACKING && 1)
#if RAGE_TRACK_MOVE
#define RAGE_TRACK_MOVE_ONLY(x) x
#else
#define RAGE_TRACK_MOVE_ONLY(x)
#endif

namespace rage {

////////////////////////////////////////////////////////////////////////////////

__THREAD mvFixedHeap::FreeList* g_FixedHeapFreeList = NULL;

////////////////////////////////////////////////////////////////////////////////

#if MV_ENABLE_MEMORY_STATS
	namespace CMoveFixedHeapProfile
	{
		PF_PAGE(CMoveFixedHeapPage, "CMoveFixedHeapProfile");

		PF_GROUP(CMoveFixedHeapStats);
		PF_LINK(CMoveFixedHeapPage, CMoveFixedHeapStats);
		
		PF_VALUE_INT(CMoveFixedHeap_StatBalance,			CMoveFixedHeapStats);
		PF_VALUE_INT(CMoveFixedHeap_StatAllocated,	CMoveFixedHeapStats);
		PF_VALUE_INT(CMoveFixedHeap_StatFrees,			CMoveFixedHeapStats);
		PF_VALUE_INT(CMoveFixedHeap_StatAllocs,			CMoveFixedHeapStats);
		PF_VALUE_INT(CMoveFixedHeap_StatEmpties,			CMoveFixedHeapStats);
		PF_VALUE_INT(CMoveFixedHeap_StatMtfs,				CMoveFixedHeapStats);
		PF_VALUE_INT(CMoveFixedHeap_StatMtes,				CMoveFixedHeapStats);
		PF_VALUE_INT(CMoveFixedHeap_StatPageNotFounds,	CMoveFixedHeapStats);
		PF_VALUE_INT(CMoveFixedHeap_StatUnusedListEmpty,	CMoveFixedHeapStats);
		PF_VALUE_INT(CMoveFixedHeap_StatDynamicPages,	CMoveFixedHeapStats);
	};

	using namespace CMoveFixedHeapProfile;
#endif // MV_ENABLE_MEMORY_STATS

using namespace mvMemorySystem;

#define ALLOCATOR_UNUSED(x) ((void)sizeof(x))

enum
{
	kWordMaxBytes = __64BIT ? 4*1024 : 2*1024,
	kPageNumSizes = kWordMaxBytes>>kAlignShift,
};

inline int IndexToSize(const int index)
{
	return (index+1)<<kAlignShift;
}
inline int SizeToIndex(const int size)
{
	return ((size+kAlignRound)>>kAlignShift) - 1;
}

#if MV_ENABLE_MEMORY_STATS
static int s_StatBalance = 0;
static int s_StatFrees = 0;
static int s_StatAllocs = 0;
static int s_StatAllocatedBytes = 0;
static int s_StatEmpties = 0;
static int s_StatMtfs = 0;
static int s_StatMtes = 0;
static int s_StatPageNotFounds = 0;
static int s_StatUnusedListEmpty = 0;
static int s_DynamicPages = 0;
#endif // MV_ENABLE_MEMORY_STATS

class SmallPageAllocator;

struct FreeWord
{
	FreeWord* Next;
};

struct SmallPageHeader
{
	SmallPageAllocator* Next;
	int NumAllocatedWords;
	int WordSize;
	FreeWord* Free;

	void Init()
	{
		Next = NULL;
		Free = NULL;
		NumAllocatedWords = 0;
		WordSize = 0;
	}
};

class SmallPageAllocator
{
private:
	enum { kDataBytes = kPageMemSize-sizeof(SmallPageHeader) };

	char m_Data[kDataBytes];
	SmallPageHeader m_Header;

public:
	bool Available() const
	{
		return m_Header.Free != NULL;
	}

	bool IsEmpty() const
	{
		return m_Header.NumAllocatedWords == 0;
	}

	SmallPageAllocator* GetNext() const
	{
		return m_Header.Next;
	}

	void SetNext(SmallPageAllocator * page)
	{
		m_Header.Next = page;
	}

	bool PointerBelongs(const char * ptr) const
	{
		// is this pointer from this Page ?
		return ( ptr >= m_Data && ptr < (m_Data + kDataBytes) );
	}

	int GetWordSize() const
	{
		return m_Header.WordSize;
	}

	void Init()
	{
		m_Header.Init();
	}

	void Setup(const int wordSize)
	{
		Assert(sizeof(*this) == kPageMemSize);

		m_Header.Init();

		Assert((wordSize&3) == 0);
		Assert(wordSize >= 4);

		m_Header.WordSize = wordSize;

		// set up pFree to point at data
		const int numWords = kDataBytes / wordSize;

		// add all Pages in the Word to the free list :
		// make sure they're added in order, not reverse order			
		char* ptr = m_Data;
		FreeWord** freePtr = &m_Header.Free;
		for(int i=0;i<numWords;i++)
		{
			FreeWord* free = (FreeWord*)ptr;
			*freePtr = free;
			freePtr = &(free->Next);
			ptr += wordSize;
		}
		*freePtr = NULL;
	}

	void* Alloc()
	{
		Assert(m_Header.Free);	
		Validate();

		m_Header.NumAllocatedWords++;

		void* ptr = m_Header.Free;
		m_Header.Free = m_Header.Free->Next;

		Validate();

		return ptr;
	}

	void Free(void * ptr)
	{
		Assert(ptr);
		Assert(PointerBelongs((char *)ptr));
		// just push it on the free list :

		Validate();

		Assert(m_Header.NumAllocatedWords > 0);
		m_Header.NumAllocatedWords--;

		((FreeWord*) ptr)->Next = m_Header.Free;
		m_Header.Free = (FreeWord*) ptr;

		Assert(PointerBelongs((char*)m_Header.Free));

		Validate();
	}

	void Validate()
	{
#if DO_VALIDATE
		FreeWord* free = m_Header.Free;
		while (free != NULL)
		{
			void* page = GetFailablePageForAddress(free);
			Assert(PointerBelongs((char*)free));
			free = free->Next;

			(void)sizeof(page);
		}
#endif // DO_VALIDATE
	}
};

static SmallPageAllocator* s_PagesForSize[kPageNumSizes] = { 0 };
static sysCriticalSectionToken s_FixedHeapToken;

void mvFixedHeapSystem::InitializeFixedHeap(void* RAGE_TRACK_MOVE_ONLY(buffer), u32 RAGE_TRACK_MOVE_ONLY(size))
{
	Assert(sizeof(SmallPageAllocator) == kPageMemSize);

#if RAGE_TRACK_MOVE
	diagTracker* t = diagTracker::GetCurrent();
	if (t && sysMemVisualize::GetInstance().HasPlatform() && !sysMemVisualize::GetInstance().HasXTL())
	{
		t->InitHeap("MoVE Fixed Heap", buffer, size);
	}
#endif
}

void mvFixedHeapSystem::ShutdownFixedHeap()
{	
	sysMemSet(s_PagesForSize, 0, sizeof(void*) * kPageNumSizes);
}

void ReclaimAnEmptyPage()
{
	// @@ what order to search in?  smallest-to-largest ?
	//	largest-to-smallest ?
	for(int which=0;which<kPageNumSizes;which++)
	{
		SmallPageAllocator* curr = s_PagesForSize[which];
		SmallPageAllocator* prev = NULL;
		while (curr)
		{
			if (curr->IsEmpty())
			{
				// found it :
				// cut it from the list :
				if (prev)
				{
					prev->SetNext(curr->GetNext());
				}
				else
				{
					s_PagesForSize[which] = curr->GetNext();
				}

				// put it on the Unused list :
				FreePage(curr);

				return;
			}
			prev = curr;
			curr = curr->GetNext();
		}
	}
	// bad !! will Fail outside here
}

void MoveToEnd(SmallPageAllocator* page, const int which)
{
	// already at the end !
	Assert(page->GetNext() != NULL);

	// must walk to find pPrev :
	SmallPageAllocator* curr = s_PagesForSize[which];
	SmallPageAllocator* prev = NULL;
	while (curr)
	{
		if (curr == page)
		{
			// found it :
			// cut it from the list :
			curr = page->GetNext();
			page->SetNext(NULL);

			Assert(curr);

			if (prev)
			{
				prev->SetNext(curr);
			}
			else
			{
				s_PagesForSize[which] = curr;
			}

			// keep going to the end :
			while (curr)
			{
				prev = curr;
				curr = curr->GetNext();
			}
			// now prev is the end
			prev->SetNext(page);
			return;
		}
		prev = curr;
		curr = curr->GetNext();
	}

	Errorf("MoveToEnd : Page not found!!");
}

void ReturnToUnusedList(SmallPageAllocator* page, const int which)
{
	Assert(page);
	Assert(page->IsEmpty());
	// must find it's prev since it's singly linked

	SmallPageAllocator* curr = s_PagesForSize[which];
	SmallPageAllocator* prev = NULL;
	while (curr)
	{
		if (curr == page)
		{
			// found it :
			// cut it from the list :
			if (prev)
			{
				prev->SetNext(page->GetNext());
			}
			else
			{
				s_PagesForSize[which] = page->GetNext();
			}

			// put it on the Unused list :
			FreePage(page);

			return;
		}
		prev = curr;
		curr = curr->GetNext();
	}

	Errorf("ReturnToUnusedList : Page not found!!");
}

int	GetSizeOfAllocation(const int size)
{
	return (size+kAlignRound)&(~kAlignRound);
}

void* mvFixedHeap::Alloc(int size)
{
	sysCriticalSection cs (s_FixedHeapToken);

	if (size > kWordMaxBytes || size == 0)
	{
		Errorf("mvFixedHeap::Alloc - Allocation too large for allocator!");
		return NULL;
	}
	else
	{
#if MV_ENABLE_MEMORY_STATS
		s_StatAllocs ++;
		s_StatBalance ++;
		PF_SET(CMoveFixedHeap_StatAllocs, s_StatAllocs);
		PF_SET(CMoveFixedHeap_StatBalance, s_StatBalance);
#endif // MV_ENABLE_MEMORY_STATS

		const int which = SizeToIndex(size);
		Assert(which >= 0 && which < kPageNumSizes);

		SmallPageAllocator* page = s_PagesForSize[which];

#if DO_MTF_ON_ALLOCATE
		SmallPageAllocator* prev = NULL;
#endif

		while (page)
		{
			if (page->Available())
			{
				void* ptr = page->Alloc();

				Assert(ptr);
				Assert(GetPageForAddress(ptr) == page);

				DO_INVALIDATE_DATA(ptr, 0xCD, size);

#if RAGE_TRACK_MOVE
				if (::rage::diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
				{
					::rage::diagTracker::GetCurrent()->Tally(ptr, size, MEMTYPE_GAME_VIRTUAL);
				}
#endif // RAGE_TRACK_MOVE

				// MTF the page we found
#if DO_MTF_ON_ALLOCATE
				if (prev && page->Available())
				{
#if MV_ENABLE_MEMORY_STATS
					s_StatMtfs ++;
					PF_SET(CMoveFixedHeap_StatMtfs, s_StatMtfs);
#endif // MV_ENABLE_MEMORY_STATS

					Assert(prev->GetNext() == page);
					prev->SetNext(page->GetNext());
					page->SetNext(s_PagesForSize[which]);
					s_PagesForSize[which] = page;
				}
#endif // DO_MTF_ON_ALLOCATE

#if MV_ENABLE_MEMORY_STATS
				s_StatAllocatedBytes += IndexToSize(which);
				PF_SET(CMoveFixedHeap_StatAllocated, s_StatAllocatedBytes);
#endif // MV_ENABLE_MEMORY_STATS

				return ptr;
			}

#if DO_MTF_ON_ALLOCATE
			prev = page;
#endif // DO_MTF_ON_ALLOCATE

			page = page->GetNext();
		}

#if MV_ENABLE_MEMORY_STATS
		s_StatPageNotFounds++;
		PF_SET(CMoveFixedHeap_StatPageNotFounds,	s_StatPageNotFounds);
#endif // MV_ENABLE_MEMORY_STATS

		// no more available pages, try and reclaim one
		if (!AvailablePage())
		{
#if MV_ENABLE_MEMORY_STATS
			s_StatUnusedListEmpty ++;
			PF_SET(CMoveFixedHeap_StatUnusedListEmpty,	s_StatUnusedListEmpty);
#endif // MV_ENABLE_MEMORY_STATS

			// try and reclaim an empty page
			ReclaimAnEmptyPage();
			if (!AvailablePage())
			{
				Errorf("mvFixedHeap: Out of pooled pages attempting to dynamically allocate a page");

#if MV_ENABLE_MEMORY_STATS
				s_DynamicPages ++;
				PF_SET(CMoveFixedHeap_StatDynamicPages,	s_DynamicPages);
#endif // MV_ENABLE_MEMORY_STATS

				mvDisplayFixedHeapStats();
			}
		}

		// pull off one from s_PagesUnused;
		page = (SmallPageAllocator*)AllocatePage();

		// initialize for this Page size :
		//	(expensive)
		const int wordSize = IndexToSize(which);
		page->Setup(wordSize);
		Assert(page->Available());

		// put it on the head of the list :
		page->SetNext(s_PagesForSize[which]);
		s_PagesForSize[which] = page;

		void* ptr = page->Alloc();

		DO_INVALIDATE_DATA(ptr, 0xCD, size);

#if RAGE_TRACK_MOVE
		if (::rage::diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL()) 
		{
			::rage::diagTracker::GetCurrent()->Tally(ptr, size, MEMTYPE_GAME_VIRTUAL);
		}
#endif // RAGE_TRACK_MOVE

		Assert(ptr);
		Assert(GetPageForAddress(ptr) == page);

#if MV_ENABLE_MEMORY_STATS
		s_StatAllocatedBytes += wordSize;
		PF_SET(CMoveFixedHeap_StatAllocated, s_StatAllocatedBytes);
#endif // MV_ENABLE_MEMORY_STATS

		return ptr;
	}
}

inline void mvFixedHeap::FreeInternal(void* p)
{
	// figure out which page allocation came from
	SmallPageAllocator* page = (SmallPageAllocator*)GetPageForAddress(p);
	if(Unlikely(!page))
	{
		Errorf("mvFixedHeap::FreeInternal - Can't find page to free memory from");
		return;
	}

	const int size = page->GetWordSize();
	DO_INVALIDATE_DATA(p, 0xDD, size);

	page->Free(p);

#if RAGE_TRACK_MOVE
	if (::rage::diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
	{
		::rage::diagTracker::GetCurrent()->UnTally(p, size);
	}
#endif // RAGE_TRACK_MOVE

#if MV_ENABLE_MEMORY_STATS
	s_StatFrees ++;
	PF_SET(CMoveFixedHeap_StatFrees, s_StatFrees);

	s_StatBalance--;
	PF_SET(CMoveFixedHeap_StatBalance, s_StatBalance);

	s_StatAllocatedBytes -= size;
	PF_SET(CMoveFixedHeap_StatAllocated, s_StatAllocatedBytes);

	if (page->IsEmpty())
		s_StatEmpties ++;
		PF_SET(CMoveFixedHeap_StatEmpties, s_StatEmpties);
#endif // MV_ENABLE_MEMORY_STATS

#if DO_MTE_ON_FREE_EMPTY
	if(page->IsEmpty() && page->GetNext())
	{
		// put it back on the free list	
		const int which = SizeToIndex(size);
		Assert(which >= 0 && which < kPageNumSizes);
		
		bool available = false;
		SmallPageAllocator* curr = s_PagesForSize[which];
		while (curr)
		{
			if(curr != page)
			{
				if(curr->IsEmpty())
				{
					ReturnToUnusedList(page, which);
					return;
				}
				else if(curr->Available())
				{
					available = true;
				}
			}
			curr = curr->GetNext();
		}

		if(available)
		{
			// put it on the end of the list for this size
			MoveToEnd(page, which);

#if MV_ENABLE_MEMORY_STATS
			s_StatMtes ++;
			PF_SET(CMoveFixedHeap_StatMtes, s_StatMtes);
#endif // MV_ENABLE_MEMORY_STATS
		}
	}
#endif // DO_MTE_ON_FREE_EMPTY
}

void mvFixedHeap::Free(void* p)
{
	if(Likely(p))
	{
		if(g_FixedHeapFreeList)
		{
			g_FixedHeapFreeList->Append(p);
			return;
		}

		sysCriticalSection cs (s_FixedHeapToken);
		FreeInternal(p);
	}
}

void mvFixedHeap::Free(void* p, int size)
{
	if(Likely(size <= kWordMaxBytes))
	{
		Free(p);
	}
}

#if MV_ENABLE_MEMORY_STATS
void mvFixedHeapSystem::DisplayStats()
{
	Displayf("s_StatFrees = %d\n", s_StatFrees);
	Displayf("s_StatAllocs = %d\n", s_StatAllocs);
	Displayf("s_StatEmpties = %d\n", s_StatEmpties);
	Displayf("s_StatMtfs = %d\n", s_StatMtfs);
	Displayf("s_StatMtes = %d\n", s_StatMtes);
	Displayf("s_StatPageNotFounds = %d\n", s_StatPageNotFounds);
	Displayf("s_StatUnusedListEmpty = %d\n", s_StatUnusedListEmpty);
}
#endif // MV_ENABLE_MEMORY_STATS

////////////////////////////////////////////////////////////////////////////////

mvFixedHeap::FreeList::FreeList()
{
	Assert(!g_FixedHeapFreeList);
	g_FixedHeapFreeList = this;
}

////////////////////////////////////////////////////////////////////////////////

mvFixedHeap::FreeList::~FreeList()
{
	Assert(g_FixedHeapFreeList == this);
	g_FixedHeapFreeList = NULL;

	Flush();
}

////////////////////////////////////////////////////////////////////////////////

void mvFixedHeap::FreeList::Flush()
{
	s_FixedHeapToken.Lock();
	for(int i=0; i<m_Ptrs.GetCount(); ++i)
	{
		void* p = m_Ptrs[i];
		FreeInternal(p);
	}
	s_FixedHeapToken.Unlock();

	m_Ptrs.Reset();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
