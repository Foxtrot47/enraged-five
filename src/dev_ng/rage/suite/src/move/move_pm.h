//
// move/move_pm.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_PM_H
#define MOVE_PM_H

#include "move_node.h"

#include "crmotiontree/nodepm.h"

namespace rage {

class crpmParameterizedMotion;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create a parameterized motion node
class mvNodePmDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this animation node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodePm* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodePmDef);

private:
	enum
	{
		PARAMETER_COUNT_MASK = 3 << 16,
		PARAMETER_COUNT_SHIFT = 16,
		PARAMETER_SHIFT = 20,
		kParameterShift = 19,
		kParameterValueShift = 2,
		kParameterValueMask = 3,
	};

	static unsigned GetParameterizedMotionFlagFrom(u32 flags);
	static unsigned GetRateFlagFrom(u32 flags);
	static unsigned GetPhaseFlagFrom(u32 flags);
	static unsigned GetDeltaFlagFrom(u32 flags);
	static unsigned GetParameterCountFrom(u32 flags);
	static unsigned GetParameterValuesFlagFrom(u32 flags);

	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// Parameterized motion flag - flags & 0x00000003
	// Rate flag - (flags & 0x0000000c) >> 0x02
	// Phase flag - (flags & 0x00000030) >> 0x04
	// Delta flag - (flags & 0x000000c0) >> 0x06
	// Parameter count - (flags & 0x00003c00) >> 0x0A
	// Parameter values flags - (flags & 0xfff80000) >> 0x13
	//		- This gives all the parameter flags. Each parameter value has a 2 bit flag that defines it defintion
	u32 m_flags;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move parameterized motion node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodePm : public crmtNodePm
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_PM_SET(T, ApplyParameterizedMotion) \
		MOVE_PM_GET(T, GetParameterizedMotion) \
		MOVE_REAL_SET(T, ApplyPhase) \
		MOVE_REAL_GET(T, GetPhase) \
		MOVE_REAL_SET(T, ApplyDelta) \
		MOVE_REAL_GET(T, GetDelta) \
		MOVE_REAL_SET(T, ApplyRate) \
		MOVE_REAL_GET(T, GetRate) \
		MOVE_REAL_SET_INDEXED(T, ApplyParameterValue) \
		MOVE_REAL_GET_INDEXED(T, GetParameterValue)

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodePm)

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodePm);
};

///////////////////////////////////////////////////////////////////////////////

inline void mvNodePm::ApplyParameterizedMotion(crpmParameterizedMotion* pm)
{
	crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	player.SetParameterizedMotion(pm);
}

///////////////////////////////////////////////////////////////////////////////

inline crpmParameterizedMotion* mvNodePm::GetParameterizedMotion()
{
	crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	return player.GetParameterizedMotion();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodePm::ApplyPhase(float phase)
{
	crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	crpmParameterizedMotion* pm = player.GetParameterizedMotion();
	if (pm) {
		pm->SetU(phase);
	}
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodePm::GetPhase() const
{
	const crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	const crpmParameterizedMotion* pm = player.GetParameterizedMotion();
	if (pm) {
		return pm->GetU();
	}

	return 0.f;
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodePm::ApplyDelta(float delta)
{
	crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	crpmParameterizedMotion* pm = player.GetParameterizedMotion();
	if (pm) {
		pm->SetPostDeltaU(delta);
	}
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodePm::GetDelta() const
{
	const crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	const crpmParameterizedMotion* pm = player.GetParameterizedMotion();
	if (pm) {
		return pm->GetPostDeltaU();
	}

	return 0.f;
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodePm::ApplyRate(float rate)
{
	crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	player.SetRate(rate);
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodePm::GetRate() const
{
	const crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	return player.GetRate();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodePm::ApplyParameterValue(u32 index, float parameter)
{
	crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	crpmParameterizedMotion* pm = player.GetParameterizedMotion();
	if(pm) {
		pm->SetParameterValue(index, parameter);
	}
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodePm::GetParameterValue(u32 index) const
{
	const crpmParameterizedMotionPlayer& player = GetParameterizedMotionPlayer();
	const crpmParameterizedMotion* pm = player.GetParameterizedMotion();
	if (pm) {
		return pm->GetParameterValue(index);
	}

	return 0.f;
}

} // namespace rage

#endif // MOVE_PM_H
