//
// move/types_internal.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_TYPES_INTERNAL_H
#define MOVE_TYPES_INTERNAL_H

#include "file/stream.h"

#include "move_config.h"

namespace rage {

typedef fiStream mvMemoryResource;

// PURPOSE: Read a float from a memory buffer and progress the block
// NOTES: Requires a memory ptr called block. Modifies the block ptr outside of the macro
#define ReadFloatBlock()			*(float*)(block); block=((u32*)block)+1

// PURPOSE: Read an s32 from a memory buffer and progress the block
// NOTES: Requires a memory ptr called block. Modifies the block ptr outside of the macro
#define ReadInt32Block()			*(s32*)(block); block=((u32*)block)+1

// PURPOSE: Read a u32 from a memory buffer and progress the block
// NOTES: Requires a memory ptr called block. Modifies the block ptr outside of the macro
#define ReadUInt32Block()			*(u32*)(block); block=((u32*)block)+1

// PURPOSE: Read a bool as a u32 from a memory buffer and progress the block
// NOTES: Requires a memory ptr called block. Modifies the block ptr outside of the macro
#define ReadBoolBlock()				*(u32*)(block)!=0; block=((u32*)block)+1

// PURPOSE: Read a string of bytes from a memroy buffer and progress the block
// NOTES: Requires a memory ptr called block. Modifies the block ptr outside of the macro
#define ReadStringBlock(_length)	(char*)block; block=((u8*)block)+_length

#define FLAG_BITS	2
#define FLAG_MASK	((1 << FLAG_BITS) - 1)

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Supported signal types
enum
{
	kSignalAnimation = 0,
	kSignalClip,
	kSignalExpression,
	kSignalFilter,
	kSignalFilterN,
	kSignalFrame,
	kSignalPM,
	kSignalReal,
	kSignalBoolean,
	kSignalData,
	kSignalRequest,
	kSignalFlag,
	kSignalNetwork,
};

//////////////////////////////////////////////////////////////////////////

namespace mvConstant
{

///////////////////////////////////////////////////////////////////////////////
/*
	Conditions
	*/
// PURPOSE: Supported condition types
enum
{
	kConditionInRange = 0,
	kConditionOutOfRange,
	kConditionOnRequest,
	kConditionOnFlag,
	kConditionAtEvent,
	kConditionGreaterThan,
	kConditionGreaterThanEqual,
	kConditionLessThan,
	kConditionLessThanEqual,
	kConditionLifetimeGreaterThan,
	kConditionLifetimeLessThan,
	kConditionOnMoveEventTag,
	kConditionBoolEquals,

	kConditionCount,
};

///////////////////////////////////////////////////////////////////////////////
/*
	Modifier
	*/
// PURPOSE: Supported transition modifier types
enum
{
	kModifierEaseInOut = 0,
	kModifierEaseOut,
	kModifierEaseIn,
	kModifierLinear,
	kModifierStep,

	kModifierCount,
};

///////////////////////////////////////////////////////////////////////////////
/*
	Flag
	*/
// PURPOSE: Supported node parameter types
// NOTES: When a node is instantiated from a defintion, the parameters on that
//	new node an initialized in different ways. Nothing can happen (kFlagIgnored),
//	the value can be read from the memory stream (kFlagValue), it can be read
//	from a parameter buffer (kFlagSignal) or in some cases it can callback to
//	code (kFlagCallback, I'm not sure this is supported by anything anymore).
enum
{
	
	kFlagIgnored	= 0,
	kFlagValue		= 1,
	kFlagSignal		= 2,
	kFlagCallback	= 3,

	kFlagCount,

};

///////////////////////////////////////////////////////////////////////////////
/*
	Synchronizer
	*/
// PURPOSE: Supported synchronizer types
enum
{
	kSynchronizerPhase = 0,
	kSynchronizerTag = 1,
	kSynchronizerNone = 2,

	kSynchronizerCount,
};

} // namespace mvConstant

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Offset in memory to another piece of data
// NOTES: We can't serialize pointers, and move isn't using the resourcer so
//	to simulate pointers we write an offset to the memory instead. This
//	little class is a helper to get at that data. This works because we know
//	that we read all the data into one long buffer.
class Offset
{
public:
	// PURPOSE: Get the data of type T at the stored offset
	template <typename T>
	T* Get() const { return (T*)((u8*)(this) + offset); }

	// PURPOSE: Check if the offset if valid
	// NOTES: Offset 0 would point at this offset struct,
	//	which isn't valid.
	operator bool() { return offset != 0; }

	// PURPOSE: Initialize from memory stream
	void Init(mvMemoryResource* resource)
	{
		resource->ReadInt(&offset, 1);
		Assertf(offset, "Export error: Offset shouldn't be '0'");
	}
private:
	// PURPOSE: The offset to the pointed to data.
	s32 offset;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Pre-known type version of Offset
template <typename T>
class TOffset
{
public:
	// PURPOSE: Const get
	T* Get() const { return (T*)((u8*)this + offset); }

	// PURPOSE: Non-const get
	T* Get() { return (T*)((u8*)this + offset); }

	// PURPOSE: Assignment operator
	void operator =(int rhs) { offset = rhs; }
public:
	// PURPOSE: The offset to the pointer to data
	int offset;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Memory layout for requests
struct request_t
{
	// PURPOSE: The key used to lookup this request
	u32 key;

	// PURPOSE: The index of this request in the bit flags stored in the network instance
	u32 index;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Memory layout for flags
struct flag_t
{
	// PURPOSE: The key used to lookup this flag
	u32 key;

	// PURPOSE: The index of this flag in the bit flags stored in the network instance
	u32 index;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Memory layout for a transition
// NOTE: this struct probably isn't as compact as you think it is! The compiler is
//	probably padding the crap out of some of those bit flags.
struct mvTransition
{
	// PURPOSE: Read the flags in from a serialized int
	__forceinline void SetFromFlags(unsigned flags)
	{
#if !MOVE_TRANSITION_EVENTS
		Type = flags & 0x00000001;
#else // !MOVE_TRANSITION_EVENTS
		TransitionEvent = flags & 0x00000001;
#endif // !MOVE_TRANSITION_EVENTS
		EnableTransitionWeightSignal = (flags & 0x00000002) >> 0x01;
		BlockUpdateAfterTransition = (flags & 0x00000004) >> 0x02;
		DurationFromSignal = (flags & 0x00000008) >> 0x03;

//		Size = (flags & 0x000ffff0) >> 0x04;
		Size = (flags & 0x0003fff0) >> 0x04;
		Transitional = (flags & 0x00300000) >> 0x12;
		Immutable = (flags & 0x00680000) >> 0x13;
		ConditionCount = (flags & 0x00f00000) >> 0x14;
		Modifier = (flags & 0x07000000) >> 0x18;
		ReEvaluate = (flags & 0x08000000) >> 0x1b;
		Synchronizer = (flags & 0x30000000) >> 0x1c;
		Filter = (flags & 0x40000000) >>0x1e;
		MergeBlend = (flags & 0x80000000) >> 0x1f;
	}

#if !MOVE_TRANSITION_EVENTS
	// PURPOSE: Once there were different types of transitions
	// NOTES: Just used as padding currently
	unsigned Type : 1;
#else // !MOVE_TRANSITION_EVENTS
	// PURPOSE: Generate event on transition
	unsigned TransitionEvent : 1;
#endif // !MOVE_TRANSITION_EVENTS

	// PURPOSE: If enabled push TransitionWeightSignal into the parameter buffer during the blend with its weight
	unsigned EnableTransitionWeightSignal : 1;

	// PURPOSE: Detach any update observers as soon as this transition is active
	unsigned BlockUpdateAfterTransition : 1;

	// PURPOSE: Use 'Signal' to search parameter buffer for transition duration
	unsigned DurationFromSignal : 1;

	// PURPOSE: Size of the transition plus conditions, so we don't have to calculate it
	unsigned Size : 14;  // NOTE: Size is only using 12 bits, top two bits are free...

	// PURPOSE: Is this a synchronized transitional transition (most are by default)
	// SEE ALSO: Synchronizer
	unsigned Transitional : 1;

	// PURPOSE: Is this an immutable synchronized transition
	// SEE ALSO: Synchronizer
	unsigned Immutable : 1;

	// PURPOSE: Number of conditions for this transition
	unsigned ConditionCount : 4;

	// PURPOSE: Transition modifier
	// SEE ALSO: Above enum
	unsigned Modifier : 3;

	// PURPOSE: When this transition passes, should be re-evaluate the next set of transitions on the resultant state
	unsigned ReEvaluate : 1;

	// PURPOSE: Attach a synchronizer and what type
	unsigned Synchronizer : 2;

	// PURPOSE: Filter this transition.
	// NOTES: The name of the transition hangs off the end of this struct
	unsigned Filter : 1;

	// PURPOSE: Merge blend
	unsigned MergeBlend : 1;

	// PURPOSE: If the synchronizer is tag synchronzed, here is the tag
	u32 Tag; // optional tag

	// PURPOSE: Default duration of the transition
	f32 Duration;

	// PURPOSE: The signal to use to find the duration if DurationFromSignal is enabled
	u32 Signal; // Duration signal

	// PURPOSE: The signal to push if EnableTransitionWeightSignal is enabled
	u32 TransitionWeightSignal;

#if MOVE_TRANSITION_EVENTS
	// PURPOSE: The event to generate if transition is successful
	u32 TransitionEventId;
#endif // MOVE_TRANSITION_EVENTS

	// PURPOSE: If the conditions pass, the offset to the new state def
	Offset Target;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Base condition memory layout
struct mvCondition
{
	// PURPOSE: The type of the condition
	// SEE ALSO: above condition enum
	u16 Type;

	// PURPOSE: If this conditions passes, should the fact it's already passed influence later tests of this transition
	// NOTES: Allows a simple hysteresis of conditions passing to be used.
	u16 Set;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Lifetime condition memory layout
struct mvConditionLifetime : public mvCondition
{
	// PURPOSE: The lifetime of the state
	f32 Lifetime;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Ranged condition memory layout
struct mvRange : public mvCondition
{
	// PURPOSE: The parameter to test against in the parameter buffer
	u32 Signal;

	// PURPOSE: The upper bound of the test range
	f32 Upper;

	// PURPOSE: The lower bound of the test range
	f32 Lower;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Value condition memory layout
struct mvValue : public mvCondition
{
	// PURPOSE: The parameter to test against in the parameter buffer
	u32 Signal;

	// PURPOSE: The value to test against
	f32 Value;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Boolean Value condition memory layout
struct mvBoolValue : public mvCondition
{
	// PURPOSE: The parameter to test against in the parameter buffer
	u32 Signal;

	// PURPOSE: The value to test against
	u32 Value; // Represented as a u32
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: On request condition memory layout
struct mvOnRequest : public mvCondition
{
	// PURPOSE: The request index to use
	// NOTES: This is an index, doesn't need to be this big
	u32 Request;		

	// PURPOSE: Test if it's set or not-set.
	u32 NotSet;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: On flag condition memory layout
struct mvOnFlag : public mvCondition
{
	// PURPOSE: The flag index to use
	u32 Flag;

	// PURPOSE: Test if it's set or not-set.
	u32 NotSet;
};

///////////////////////////////////////////////////////////////////////////////

struct mvOnMoveEventTag : public mvCondition
{
	u32 Tag;
	u32 NotSet;
};

///////////////////////////////////////////////////////////////////////////////

// PURPOSE: At event condition memory layout
struct mvAtEvent : public mvCondition
{
	// PURPOSE: Event to test if exists in the parameter buffer
	u32 Event;
	u32 NotSet;
};

} // namespace rage

#endif // MOVE_TYPES_INTERNAL_H
