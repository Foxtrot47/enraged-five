//
// move/move_anim.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_ANIM_H
#define MOVE_ANIM_H

#include "move_node.h"

#include "crmotiontree/nodeanimation.h"

namespace rage {

class crAnimation;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create an animation node
class mvNodeAnimationDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this animation node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeAnimation* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeAnimationDef);

private:
	static unsigned GetAnimationFlagFrom(u32 flags);
	static unsigned GetPhaseFlagFrom(u32 flags);
	static unsigned GetRateFlagFrom(u32 flags);
	static unsigned GetDeltaFlagFrom(u32 flags);
	static unsigned GetLoopedFlagFrom(u32 flags);
	static unsigned GetAbsoluteFlagFrom(u32 flags);

	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES: Flag layout is:
	//	Animation - flags & 0x00000003
	//	Phase - (flags & 0x0000000c) >> 0x02
	//	Rate - (flags & 0x00000030) >> 0x04
	//	Delta - (flags & 0x000000c0) >> 0x06
	//	Looped - (flags & 0x00000300) >> 0x08
	//	Absolute - (flags & 0x00000c00) >> 0x0a
	u32 m_Flags;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move animation node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeAnimation : public crmtNodeAnimation
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_ANIMATION_SET(T, ApplyAnimation) \
		MOVE_ANIMATION_GET(T, GetAnimation) \
		MOVE_REAL_SET(T, ApplyPhase) \
		MOVE_REAL_GET(T, GetPhase) \
		MOVE_REAL_SET(T, ApplyRate) \
		MOVE_REAL_GET(T, GetRate) \
		MOVE_REAL_SET(T, ApplyDelta) \
		MOVE_REAL_GET(T, GetDelta) \
		MOVE_BOOLEAN_SET(T, ApplyLooped) \
		MOVE_BOOLEAN_GET(T, GetLooped) \
		MOVE_BOOLEAN_SET(T, ApplyAbsolute) \
		MOVE_BOOLEAN_GET(T, GetAbsolute)

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeAnimation)

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeAnimation);
};

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeAnimation::ApplyAnimation(const crAnimation* anim)
{
	crAnimPlayer& player = GetAnimPlayer();
	player.SetAnimation(anim);
}

///////////////////////////////////////////////////////////////////////////////

inline const crAnimation* mvNodeAnimation::GetAnimation() const
{
	const crAnimPlayer& player = GetAnimPlayer();
	return player.GetAnimation();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeAnimation::ApplyPhase(float phase)
{
	crAnimPlayer& player = GetAnimPlayer();
	const crAnimation* anim = player.GetAnimation();
	if (anim) {
		player.SetTime(anim->ConvertPhaseToTime(phase));
	}
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeAnimation::GetPhase() const
{
	const crAnimPlayer& player = GetAnimPlayer();
	const crAnimation* anim = player.GetAnimation();
	if (anim) {
		return anim->ConvertTimeToPhase(player.GetTime());
	}

	return 0.f;
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeAnimation::ApplyRate(float rate)
{
	crAnimPlayer& player = GetAnimPlayer();
	player.SetRate(rate);
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeAnimation::GetRate() const
{
	const crAnimPlayer& player = GetAnimPlayer();
	return player.GetRate();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeAnimation::ApplyDelta(float delta)
{
	crAnimPlayer& player = GetAnimPlayer();
	player.SetDeltaHiddenSupplement(delta);
}

///////////////////////////////////////////////////////////////////////////////

inline float mvNodeAnimation::GetDelta() const
{
	const crAnimPlayer& player = GetAnimPlayer();
	return player.GetDeltaHiddenSupplement();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeAnimation::ApplyLooped(bool looped)
{
	crAnimPlayer& player = GetAnimPlayer();
	player.SetLooped(true, looped);
}

///////////////////////////////////////////////////////////////////////////////

inline bool mvNodeAnimation::GetLooped() const
{
	const crAnimPlayer& player = GetAnimPlayer();
	return player.IsLooped();
}

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeAnimation::ApplyAbsolute(bool)
{
	Assertf(false, "absolute is no longer supported");
}

///////////////////////////////////////////////////////////////////////////////

inline bool mvNodeAnimation::GetAbsolute() const
{
	Assertf(false, "absolute is no longer supported");
	return false;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_ANIM_H
