//
// move/pool.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVERT_POOL_H
#define MOVERT_POOL_H

#error 'mvPool has been replaced by mvAllocator'

} // namespace rage

#endif // MOVERT_POOL_H
