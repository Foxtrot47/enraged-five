//
// move/move_noden.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_NODEN_H
#define MOVE_NODEN_H

#include "move_node.h"

namespace rage {

class crmtNodeN;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition base for n-way nodes
class mvNodeNDef : public mvNodeDef
{
protected:
	// PURPOSE: Base create instance of this n-way node definition
	// PARAMS:
	//	node - the n-way node as allocated by the derived def
	//	contesxt - information about this nodes position in the motion tree/network hierarchy
	void Create(crmtNodeN* node, mvCreateContext* context) const;

	// PUROPSE: Initialize this defintion from streamed data
	// PARAMS: resource - memory resource
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize(u32 size) const;

private:
	enum { kShift = 4 };

	static unsigned GetFilterNFlagFrom(u32 flags);
	static unsigned GetFilterFlagFrom(u32 flags);
	static unsigned GetIsChildZeroDestinationFlagFrom(u32 flags);
	static unsigned GetTransitionalFlagFrom(u32 flags);
	static unsigned GetImmutableFlagFrom(u32 flags);
	static unsigned GetSynchronizerFrom(u32 flags);
	static unsigned GetSourceCountFrom(u32 flags);
	static unsigned GetChildWeightFlagFrom(u32 flags);
	static unsigned GetChildFilterFlagFrom(u32 flags);

	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES: Flag layout is:
	// Filter flag - (flags & 0x0000000c) >> 0x02
	// Is child zero destination - (flags & 0x00000010) >> 0x04
	// Transitional flag - (flags & 0x00000040) >> 0x06
	// Immutable flag - (flags & 0x00000080) >> 0x07
	// Synchronizer - (flags & 0x00180000) >> 0x13
	// Source count - (flags & 0xfc000000) >> 0x1a
	// Child weight flag - flags & 0x00000003
	// Child filter flag - (flags & 0x00000030) >> 0x04
	u32 m_Flags;
};

//////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_NODEN_H
