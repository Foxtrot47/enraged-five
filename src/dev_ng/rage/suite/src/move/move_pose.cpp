//
// move/move_pose.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "move_pose.h"

#include "move_parameterbuffer.h"
#include "move_macros.h"
#include "move_network.h"

#include "crmotiontree/motiontree.h"

namespace rage {

enum {
	kNormalizeMask = 3,
};

//////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_DEF(mvNodePoseDef);

///////////////////////////////////////////////////////////////////////////////

mvNodePose* mvNodePoseDef::Create(mvCreateContext* context) const
{
	mvNodePose* node = mvNodePose::CreateNode(*context->m_MotionTree);
	node->Init();

	const void* block = (this+1);

	u32 normalize = m_Flags & kNormalizeMask;
	if (normalize == mvConstant::kFlagSignal) {
		block=((u32*)block)+1;
		mvParameter ret;
		ret.SetBool(false);
	} else if (normalize == mvConstant::kFlagValue) {
		block=((u32*)block)+1;
	}

	return node;
}

///////////////////////////////////////////////////////////////////////////////

void mvNodePoseDef::Init(mvMemoryResource* resource)
{
	resource->ReadInt(&m_id, 1);
	resource->ReadInt(&m_Flags, 1);

	u32* block = (u32*)(this+1);
	u32 flags = m_Flags;

	MV_BOOLEAN_BLOCK_INIT(block, resource, flags, kNormalizeMask);
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodePoseDef::QueryMemorySize() const
{
	u32 size = sizeof(mvNodePoseDef);
	const void* block = this+1;
	u32 flags = m_Flags;
	MV_BOOLEAN_BLOCK_SIZE(block, flags, kNormalizeMask, size);
	return size;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::SetParameterFunc* mvNodePoseDef::ResolveParameterSet(u16 parameter)
{
	static mvNode::SetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodePose::ApplySetNormalized)
	};
	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

mvParameter::ParamType mvNodePoseDef::ResolveParameterSetType(u16)
{
	return mvParameter::kParameterBoolean;
}

///////////////////////////////////////////////////////////////////////////////

mvNode::GetParameterFunc* mvNodePoseDef::ResolveParameterGet(u16 parameter)
{
	static mvNode::GetParameterFunc* funcs[] = {
		MOVE_MAKE_SIGNAL(mvNodePose::GetIsNormalized)
	};
	Assert(parameter < NELEM(funcs));
	return funcs[parameter];
}

///////////////////////////////////////////////////////////////////////////////

u32 mvNodePoseDef::ResolveEvent(u16 /*event*/)
{
	return (u32)mvNode::kInvalidEvent;
}

///////////////////////////////////////////////////////////////////////////////

MOVE_IMPLEMENT_NODE_TYPE(mvNodePose, crmtNodePose, mvNode::kNodePose, mvNodePoseDef);

//////////////////////////////////////////////////////////////////////////

} // namespace rage
