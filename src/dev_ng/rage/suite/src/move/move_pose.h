//
// move/move_pose.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_POSE_H
#define MOVE_POSE_H

#include "move_node.h"

#include "crmotiontree/nodepose.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to create a pose node
class mvNodePoseDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instace of this pose node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodePose* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodePoseDef);

private:
	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES: Flag layout is:
	// Normalize - flags & 0x00000003
	u32 m_Flags;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move pose node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodePose : public crmtNodePose
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_BOOLEAN_SET(T, ApplySetNormalized) \
		MOVE_BOOLEAN_GET(T, GetIsNormalized);

	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodePose);

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodePose);
};

inline void mvNodePose::ApplySetNormalized(bool)
{
}

inline bool mvNodePose::GetIsNormalized() const
{
	return true;
}

} // namespace rage

#endif // MOVE_POSE_H
