//
// move/observer.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "move_observer.h"

#include "move.h"
#include "move_parameterbuffer.h"
#include "move_fixedheap.h"
#include "move_state.h"

#include "crclip/clip.h"
#include "creature/creature.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tag.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/node.h"
#include "crmotiontree/nodeclip.h"

#include "cranimation/animation.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

mvObserver::mvObserver(bool detachOnRequest)
: m_Web(NULL)
, m_DetachOnRequest(detachOnRequest)
{}

//////////////////////////////////////////////////////////////////////////

mvObserver::~mvObserver()
{
	mvObserver::Shutdown();
}

//////////////////////////////////////////////////////////////////////////

void mvObserver::Init(mvMotionWeb* web)
{
	Assert(!m_Web);
	web->AddRef();
	m_Web = web;
}

//////////////////////////////////////////////////////////////////////////

void mvObserver::Shutdown()
{
	crmtObserver::Shutdown();
	if (m_Web)
	{
		m_Web->Release();
		m_Web = NULL;
	}
}

//////////////////////////////////////////////////////////////////////////

void mvObserver::ReceiveMessage(const crmtMessage& msg)
{
	if (msg.GetMsgId() != kMsgDetachUpdaters)
		return;

	if(m_DetachOnRequest)
	{
		Detach();
	}
}

//////////////////////////////////////////////////////////////////////////

mvSetFromParameterBuffer::mvSetFromParameterBuffer()
: mvObserver(true)
{
}

//////////////////////////////////////////////////////////////////////////

void mvSetFromParameterBuffer::Init(mvMotionWeb* web, u32 parameter, mvNode::SetParameterFunc* func, u32 idx, mvParameter::ParamType type)
{
	mvObserver::Init(web);
	m_Parameter = parameter;
	m_SetParameterFn = func;
	m_Index = idx;
	m_Type = type;
}

//////////////////////////////////////////////////////////////////////////

void mvSetFromParameterBuffer::ReceiveMessage(const crmtMessage& msg)
{
	mvObserver::ReceiveMessage(msg);

	if(msg.GetMsgId() == crmtNode::kMsgUpdate)
	{
		const mvParameterBuffer* params = m_Web->GetParameters();
		const mvParameter* parameter = params->FindFirstOfType(m_Parameter, m_Type);
		if (parameter != NULL)
		{
			m_SetParameterFn(GetNode(), *parameter, m_Index);
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void mvSetFromParameterBuffer::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	mvObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(crmtNode::kMsgUpdate);
	interceptor.RegisterMessageIntercept(mvObserver::kMsgDetachUpdaters);
}

//////////////////////////////////////////////////////////////////////////

mvUpdateParameterBuffer::mvUpdateParameterBuffer()
: mvObserver(true)
{
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterBuffer::Init(mvMotionWeb* web, u32 parameter, mvNode::GetParameterFunc* func, u32 idx)
{
	mvObserver::Init(web);
	m_Parameter = parameter;
	m_GetParameterFn = func;
	m_Index = idx;
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterBuffer::ReceiveMessage(const crmtMessage& msg)
{
	mvObserver::ReceiveMessage(msg);

	if(msg.GetMsgId() == crmtNode::kMsgPostUpdate)
	{
		mvParameterBuffer* params = m_Web->GetInternalParameters();
		mvParameter parameter;
		m_GetParameterFn(&parameter, GetNode(), m_Index);
		params->Insert(m_Parameter, &parameter);
	}
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterBuffer::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	mvObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(crmtNode::kMsgPostUpdate);
	interceptor.RegisterMessageIntercept(mvObserver::kMsgDetachUpdaters);
}

//////////////////////////////////////////////////////////////////////////

mvUpdateProxyParameter::mvUpdateProxyParameter()
: mvObserver(false)
{
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateProxyParameter::Init(mvMotionWeb* web, u32 parameter)
{
	mvObserver::Init(web);
	m_Parameter = parameter;
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateProxyParameter::ReceiveMessage(const crmtMessage& msg)
{
	mvObserver::ReceiveMessage(msg);

	if(msg.GetMsgId() == crmtNode::kMsgPostUpdate)
	{
		mvParameterBuffer* params = m_Web->GetParameters();
		mvParameter parameter;
		parameter.SetObserverUnsafe(this);
		params->Insert(m_Parameter, &parameter);
	}
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateProxyParameter::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	mvObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(crmtNode::kMsgPostUpdate);
}

//////////////////////////////////////////////////////////////////////////

mvUpdateParameterFromOperation::mvUpdateParameterFromOperation()
: mvObserver(true)
{
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterFromOperation::Init(mvMotionWeb* web, const mvNodeStateDef::Operator* operation, u32 size, u32 index, mvNode::SetParameterFunc* func)
{
	mvObserver::Init(web);
	m_Operation = operation;
	m_StackSize = (u16)size;
	m_Index = (u16)index;
	m_SetParameterFn = func;
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterFromOperation::ReceiveMessage(const crmtMessage& msg)
{
	mvObserver::ReceiveMessage(msg);

	if(msg.GetMsgId() != crmtNode::kMsgUpdate)
		return;
	
	mvParameterBuffer* params = m_Web->GetParameters();
	const mvNodeStateDef::Operator* op = m_Operation;
	float* stack = Alloca(float, m_StackSize/4); // /4 because we're not exporting the proper stack size yet
	s32 top = -1;

	while (op->OperatorType != mvNodeStateDef::Operator::kEnd)
	{
		switch (op->OperatorType)
		{
		case mvNodeStateDef::Operator::kPushValue:
			{
				++top;
				stack[top] = op->Packet.Value;
#if HACK_GTA4
				++op;
#endif // HACK_GTA4
			}
			break;
		case mvNodeStateDef::Operator::kFindAndPushValue:
			{
				// if we're missing a parameter we don't want to set anything
				const mvParameter* parameter = params->FindFirstOfType(op->Packet.ParameterCrc, mvParameter::kParameterReal);
				if (!parameter)
				{
					return;
				}

				++top;
				stack[top] = parameter->GetReal();

#if HACK_GTA4
				++op;
#endif // HACK_GTA4
			}
			break;
		case mvNodeStateDef::Operator::kAdd:
			{
				stack[top-1] = stack[top-1]+stack[top];
				--top;
#if HACK_GTA4
				++op;
#endif // HACK_GTA4
			}
			break;
		case mvNodeStateDef::Operator::kMultiply:
			{
				stack[top-1] = stack[top-1]*stack[top];
				--top;
#if HACK_GTA4
				++op;
#endif // HACK_GTA4
			}
			break;
		case mvNodeStateDef::Operator::kFCurve:
			{
				mvUpdateParameterFromOperation::FCurve* fcurve =
					op->Packet.Data.Get<mvUpdateParameterFromOperation::FCurve>();
				float min = fcurve->ClampRange.Minimum;
				float max = fcurve->ClampRange.Maximum;
				float val = Clamp(stack[top], min, max);
#if HACK_GTA4
				float range = (max - min);
				if(range > 0.0f)
					val = (val - min) / range;
#endif // HACK_GTA4

				mvUpdateParameterFromOperation::FCurve::Keyframe* keyframe =
					fcurve->Keyframes.Get<mvUpdateParameterFromOperation::FCurve::Keyframe>();
				for (u32 i = 0; i < fcurve->KeyframeCount; ++i)
				{
					if (val <= keyframe->Key)
					{
						stack[top] = keyframe->Constants[0]*val + keyframe->Constants[1];
						break;
					}
					
					++keyframe;
				}

#if HACK_GTA4
				// GSALES - the next operator starts immediately after the collection of keyframes in memory, not at ++op
				op = (mvNodeStateDef::Operator*)(fcurve->Keyframes.Get<mvUpdateParameterFromOperation::FCurve::Keyframe>() + fcurve->KeyframeCount);
#endif // HACK_GTA4
			}
			break;
#if HACK_GTA4
		default:
			{
				++op;
			}
			break;
#endif // HACK_GTA4
		};
#if !HACK_GTA4
		++op;
#endif // !HACK_GTA4
	}

	Assert(top == 0);
	mvParameter param;
	param.SetReal(stack[top]);
	m_SetParameterFn(GetNode(), param, m_Index);
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterFromOperation::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	mvObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(crmtNode::kMsgUpdate);
	interceptor.RegisterMessageIntercept(mvObserver::kMsgDetachUpdaters);
}

//////////////////////////////////////////////////////////////////////////

mvUpdateParameterFromEvent::mvUpdateParameterFromEvent(u32 msg)
: mvObserver(true)
, m_Msg(msg)
{
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterFromEvent::Init(mvMotionWeb* web, u32 eventId)
{
	mvObserver::Init(web);
	m_Event = eventId;
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterFromEvent::ReceiveMessage(const crmtMessage& msg)
{
	mvObserver::ReceiveMessage(msg);

	// if we detach, we should leave now because this observer was deleted
	if(msg.GetMsgId() == mvObserver::kMsgDetachUpdaters)
		return;

	if(msg.GetMsgId() != m_Msg)
		return;

	mvParameterBuffer* params = m_Web->GetInternalParameters();
	params->Insert(m_Event, true);
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterFromEvent::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	mvObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(m_Msg);
}

//////////////////////////////////////////////////////////////////////////

#if NEW_MOVE_TAGS
mvUpdateParameterFromTag::mvUpdateParameterFromTag(u32 enterTagMsgId, u32 exitTagMsgId, u32 updateTagMsgId)
: mvObserver(true)
, m_EnterTagMsgId(enterTagMsgId)
, m_ExitTagMsgId(exitTagMsgId)
, m_UpdateTagMsgId(updateTagMsgId)
{
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterFromTag::Init(mvMotionWeb* web)
{
	mvObserver::Init(web);

}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterFromTag::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	mvObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(mvObserver::kMsgDetachUpdaters);

	interceptor.RegisterMessageIntercept(m_EnterTagMsgId);
	interceptor.RegisterMessageIntercept(m_ExitTagMsgId);
	interceptor.RegisterMessageIntercept(m_UpdateTagMsgId);
}

//////////////////////////////////////////////////////////////////////////

void mvUpdateParameterFromTag::ReceiveMessage(const crmtMessage& msg)
{
	if(msg.GetMsgId() == m_UpdateTagMsgId || msg.GetMsgId() == m_EnterTagMsgId || msg.GetMsgId() == m_ExitTagMsgId)
	{
		mvParameterBuffer* params = m_Web->GetInternalParameters();

		crmtNodeClip::TagMsgPayload* payload = reinterpret_cast<crmtNodeClip::TagMsgPayload*>(msg.GetPayload());
		const crProperty& prop = payload->m_Tag->GetProperty();
		const crProperty::Key key = prop.GetKey();

		if(msg.GetMsgId() == m_EnterTagMsgId || IsRelevantToState())
		{
			// special case: if move event tag, extract hash and insert into parameter buffer
			const crProperty::Key keyMoveEvent = crProperty::CalcKey("MoveEvent", 0x7EA9DFC4);
			if(key == keyMoveEvent)
			{
				crPropertyAttributeHashStringAccessor accessor(prop.GetAttribute(keyMoveEvent));
				if(accessor.Valid() && accessor.GetPtr()->GetHash())
				{
					params->Insert(accessor.GetPtr()->GetHash(), true);
				}
			}

			// insert property directly into buffer
			params->Insert(key, &prop);
		}
	}
	else if(msg.GetMsgId() == mvObserver::kMsgDetachUpdaters)
	{
		Detach();
	}
}

//////////////////////////////////////////////////////////////////////////

bool mvUpdateParameterFromTag::IsRelevantToState()
{
	// TODO --- CACHE THIS RESULT, IT'S CONSTANT PER UPDATE

	const float threshold = 0.01f;

	crmtNode* node = GetNode();
	crmtNodeParent* parent = node->GetParent();

	float weight = 1.f;

	while(parent)
	{
		switch(parent->GetNodeType())
		{
		case crmtNode::kNodeBlend:
		case crmtNode::kNodeBlendN:
			weight *= parent->GetChildInfluence(parent->GetChildIndex(*node));
			break;

		case crmtNode::kNodeStateRoot:
		case crmtNode::kNodeStateMachine:
			return true;

		default:
			break;
		}

		// if the combined weight is negligible, ignore this node
		if (weight < threshold)
		{
			return false;
		}

		node = parent;
		parent = parent->GetParent();
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////

#endif // NEW_MOVE_TAGS

//////////////////////////////////////////////////////////////////////////

mvExitParameter::mvExitParameter()
: mvObserver(false)
, m_OnExitId(0)
{
}

//////////////////////////////////////////////////////////////////////////

void mvExitParameter::Init(mvMotionWeb* web, u32 onExitId)
{
	mvObserver::Init(web);
	m_OnExitId = onExitId;
}

//////////////////////////////////////////////////////////////////////////

void mvExitParameter::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	mvObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(crmtNode::kMsgShuttingDown);
}

//////////////////////////////////////////////////////////////////////////

void mvExitParameter::ReceiveMessage(const crmtMessage& msg)
{
	mvObserver::ReceiveMessage(msg);

	if(msg.GetMsgId() == crmtNode::kMsgShuttingDown)
	{
		m_Web->GetInternalParameters()->Insert(m_OnExitId, true);
	}
}

//////////////////////////////////////////////////////////////////////////

#if MOVE_CONCURRENT_TRANSITION_LIMIT
mvActiveTransition::mvActiveTransition()
: mvObserver(false)
, m_PrevTransition(NULL)
, m_NextTransition(NULL)
, m_NodeState(NULL)
{
}

//////////////////////////////////////////////////////////////////////////

void mvActiveTransition::Init(mvMotionWeb* web, mvNodeStateMachine& nodeState)
{
	mvObserver::Init(web);

	m_NodeState = &nodeState;

	if(m_NodeState->m_ActiveTransitions)
	{
		Assert(!m_NodeState->m_ActiveTransitions->m_PrevTransition);
		m_NextTransition = m_NodeState->m_ActiveTransitions;
		m_NextTransition->m_PrevTransition = this;
	}
	m_NodeState->m_ActiveTransitions = this;
	m_NodeState->m_NumActiveTransitions++;
}

//////////////////////////////////////////////////////////////////////////

void mvActiveTransition::ResetMessageIntercepts(crmtInterceptor& interceptor)
{
	mvObserver::ResetMessageIntercepts(interceptor);

	interceptor.RegisterMessageIntercept(crmtNode::kMsgDetach);
//	interceptor.RegisterMessageIntercept(crmtNode::kMsgShuttingDown);
}

//////////////////////////////////////////////////////////////////////////

void mvActiveTransition::ReceiveMessage(const crmtMessage& msg)
{
	mvObserver::ReceiveMessage(msg);

	if(msg.GetMsgId() == crmtNode::kMsgDetach)
	{
		RemoveActiveTransition();
	}
}

//////////////////////////////////////////////////////////////////////////

void mvActiveTransition::RemoveActiveTransition()
{
	if(m_NodeState)
	{
		if(m_NextTransition)
		{
			Assert(m_NextTransition->m_PrevTransition == this);
			m_NextTransition->m_PrevTransition = m_PrevTransition;
		}
		if(m_PrevTransition)
		{
			Assert(m_PrevTransition->m_NextTransition == this);
			m_PrevTransition->m_NextTransition = m_NextTransition;
		}
		else
		{
			Assert(m_NodeState->m_ActiveTransitions == this);
			m_NodeState->m_ActiveTransitions = m_NextTransition;
		}

		Assert(m_NodeState->m_NumActiveTransitions > 0);
		m_NodeState->m_NumActiveTransitions--;

		m_PrevTransition = NULL;
		m_NextTransition = NULL;
		m_NodeState = NULL;
	}
}
#endif // MOVE_CONCURRENT_TRANSITION_LIMIT

//////////////////////////////////////////////////////////////////////////

} // namespace rage