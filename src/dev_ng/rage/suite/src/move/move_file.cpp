//
// move/file.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "system/memory.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "zlib/zlib.h"

#include "move_network.h"
#include "move_node.h"
#include "move_types_internal.h"
#include "move_version.h"

namespace rage {

#if __WIN32
	#pragma warning(push)
	#pragma warning(disable: 4244)
#endif

/* using */
using namespace mvVersion;

/* static constants */
enum { kHeaderSizeOffset = sizeof(u32)*2 }; // request & flag count

#if !__NO_OUTPUT
static const char* kOpenFailed = "mvNetworkDef::Load - failed to open file '%s'";
static const char* kWrongType = "mvNetworkDef::Load - '%s' is not a MoVE file";
static const char* kWrongVersion = "mvNetworkDef::Load - '%s' version '%d.%d.%d.%d' is incompatible with '%d.%d.%d.%d'";
#endif

/* typedefs */
typedef request_t Request;
typedef u32 (*Swap)(void*);


namespace
{
	struct FileHeader
	{
		s32 Magic;
		Version Version;	// version moved!
		s32 DefinitionLength;		// the length of the definition data
		s32 StringTableLength;		// the length of the string table
		s32 XRefs;		// # of references used in this file
		//s32 Count;		// # of nodes serialized
	};
}
///////////////////////////////////////////////////////////////////////////////

// PURPOSE: Helper for the reading a stream of memory
struct ReadIterator
{
	ReadIterator(u8* buffer, int length)
		:	Buffer (buffer)
		,	Curr (buffer)
	{
		End = buffer+length;
	}

	template <typename T>
	T* Get()
	{
		return reinterpret_cast<T*>(Curr);
	}

	void Advance(int size)
	{
		Curr = Curr+size;
	}

	void ReadInt(fiStream* stream)
	{
		int* p = reinterpret_cast<int*>(Curr);
		stream->ReadInt(p, 1);
		Curr = reinterpret_cast<u8*>(p + 1);
	}

	void ReadInt(u32 v)
	{
		u32* p = reinterpret_cast<u32*>(Curr);
		*p = v;
		Curr = reinterpret_cast<u8*>(p+1);
	}

	void ReadShort(fiStream* stream)
	{
		short* p = reinterpret_cast<short*>(Curr);
		stream->ReadShort(p, 1);
		Curr = reinterpret_cast<u8*>(p + 1);
	}

	void Read(fiStream* stream, const u32 len)
	{
		for (u32 i = 0; i < len; ++i) {
			stream->ReadByte(Curr, 1);
			++Curr;
		}
   	}

	u8 *Buffer, *Curr, *End;
};


///////////////////////////////////////////////////////////////////////////////

// PURPOSE: We just keep reading until we hit the end of our buffer
// NOTES: If the network size is exported incorrectly, we'll try read
//	dud memory. If QueryMemorySize is incorrectly implemented in the runtime
//	or the correct data isn't exported from move, we'll jump to the
//	incorrect position in the buffer.
static void ReadNetwork(ReadIterator* iter, fiStream* stream)
{
	while (iter->Curr < iter->End) {
		mvNodeDef* def = iter->Get<mvNodeDef>();
		stream->ReadShort(&def->m_type, 1);
		MV_VALIDATE(mvNode::kNodeNone < def->m_type && def->m_type < mvNode::kNodeCount, "Not a valid node type");
		stream->ReadShort(&def->m_index, 1);
		def->Init(stream);

		iter->Advance(def->QueryMemorySize());
	}
}

#if __WIN32
	#pragma warning(pop)
#endif

///////////////////////////////////////////////////////////////////////////////

static void ReadHeader(fiStream* stream, FileHeader* header)
{
	stream->ReadByte((char*)&header->Magic, 4);	

	Version& version = header->Version;
	stream->ReadInt(&version.Major, 1);
	stream->ReadInt(&version.Minor, 1);
	stream->ReadInt(&version.Patch, 1);
	stream->ReadInt(&version.Revision, 1);

	stream->ReadInt(&header->DefinitionLength, 1);
	MV_VALIDATE(header->DefinitionLength> 0, "Invalid Length");
	stream->ReadInt(&header->StringTableLength, 1);
	stream->ReadInt(&header->XRefs, 1);
	MV_VALIDATE(header->XRefs >= 0, "Invalid xref count");
}

///////////////////////////////////////////////////////////////////////////////

static inline bool CheckHeaderMagic(const FileHeader& header)
{
	return strncmp((char*)(&header.Magic), "MoVE", 4) == 0;
}

///////////////////////////////////////////////////////////////////////////////

static inline bool CheckHeaderVersion(const FileHeader& header)
{
	Version version;
	GetVersion(&version);

	// major version change is binary compatibility
	// no guarantee of compatibility for revisions

	return
		header.Version.Major == version.Major &&
		header.Version.Minor == version.Minor &&
		header.Version.Revision == version.Revision;
}

#if __DEV && CR_DEV
///////////////////////////////////////////////////////////////////////////////
/*
	Load the debug names into the string table
	by initialising an atHashString with all the elements.
	*/
static void LoadDebugNames(const char * filename)
{
	char debugFile[256];

	const char *paths[] = {
		"common:/non_final/anim/move/nodemaps/",
		"assets:/anim/move/networks/test/",
		"assets:/anim/move/networks/",
		NULL };

	for(int pathIndex = 0; paths[pathIndex] != NULL; pathIndex ++)
	{
		if(strncmp(filename, "memory:", strlen("memory:")) == 0)
		{
			strcpy(debugFile, paths[pathIndex]);
			const char *pos = strrchr(filename, ':');
			if(pos)
			{
				pos ++;
				strcpy(&debugFile[strlen(debugFile)], pos);
			}
			else
			{
				strcpy(&debugFile[strlen(debugFile)], filename);
			}
			strncpy(&debugFile[strlen(debugFile)], "_nodemap\0", 9);
		}
		else
		{
			strcpy(debugFile, paths[pathIndex]);
			const char *pos = strrchr(filename, '/');
			if(!pos)
			{
				pos = strrchr(filename, '\\');
			}
			if(pos)
			{
				pos ++;
				const char *end = strrchr(filename, '.');
				if(end)
				{
					size_t len = strlen(debugFile);
					strncpy(&debugFile[len], pos, end - pos);
					len += end - pos;
					strncpy(&debugFile[len], "_nodemap\0", 9);
				}
			}
		}

		//	formatf(debugFile, sizeof(debugFile), "%s_nodemap", filename);

		if(ASSET.Exists(debugFile, "xml"))
		{
			sysMemStartTemp();
			parTree* tree = PARSER.LoadTree(debugFile, "xml");
			sysMemEndTemp();
			if (tree != NULL) {
				parTreeNode* root = tree->GetRoot();

				if(parUtils::StringEquals(root->GetElement().GetName(), "NodeMap"))
				{
					// New debug name format
					parTreeNode* debugNames = root->FindChildWithName("debugNames");
					if(debugNames)
					{
						for (parTreeNode* debugName = debugNames->GetChild(); debugName; debugName = debugName->GetSibling())
						{
							const char *szName = debugName->GetData();
							atHashString temp;
							temp.SetFromString(szName);
						}
					}
				}
				else
				{
					// Old debug name format
					Assertf(parUtils::StringEquals(root->GetElement().GetName(), "root"), "<root> tag missing.");
					for (parTreeNode::ChildNodeIterator iter = root->BeginChildren(); iter != root->EndChildren(); ++iter)
					{
						parElement& element = (*iter)->GetElement();
						const parAttribute* name = element.FindAttribute("name");
						atHashString temp;
						temp.SetFromString(name->GetStringValue());
					}
				}

				sysMemStartTemp();
				delete tree;
				sysMemEndTemp();

				break;
			}
		}
	}
}

#endif //__DEV && CR_DEV

///////////////////////////////////////////////////////////////////////////////

bool mvNetworkDef::Load(const char* filename)
{
	fiStream* stream = ASSET.Open(filename, "mrf");
	if (!stream) {
		OUTPUT_ONLY(Errorf(kOpenFailed, filename));
		return false;
	}

	FileHeader header;
	ReadHeader(stream, &header);

	if (!CheckHeaderMagic(header)) {
		OUTPUT_ONLY(Errorf(kWrongType, filename));
		return false;
	}

#if CR_DEV
	Assert(!m_Name);
	m_Name = StringDuplicate(filename);
#endif // CR_DEV

	if (!CheckHeaderVersion(header)) {
		OUTPUT_ONLY(mvVersion::Version& version = header.Version);
		OUTPUT_ONLY(Errorf(kWrongVersion, filename, version.Major, version.Minor, version.Patch, version.Revision,
			MOVE_MAJOR_VERSION, MOVE_MINOR_VERSION, MOVE_PATCH_VERSION, MOVE_REVISION_VERSION));
		return false;
	}

#if __DEV && CR_DEV
	LoadDebugNames(filename);
#endif //__DEV && CR_DEV

	const int size = header.DefinitionLength-kHeaderSizeOffset+(header.XRefs*sizeof(u32));
	ReadIterator iter (rage_new u8[size+header.StringTableLength], size);
	m_XRefs = iter.Get<u32>();

	// TODO: goober! don't actually need to store the xrefs.
#define kMaxPath (1024)
	s32 xrefcount = header.XRefs;
	for (s32 xref = 0; xref < xrefcount; ++xref) {
		unsigned len;
		stream->ReadInt(&len, 1);
		char ref[kMaxPath];
		Assert(len < kMaxPath);
		stream->ReadByte(ref, len);

		// double work but at the moment i don't give a shit
		u32 id = atStringHash(ref);
		iter.ReadInt(id);

		// Shouldn't need to do this anymore - the reference node will request the
		// network def using the mvNetworkDefLoader (loading of these defs is likely to be project specific).
		// projects will need to take care of dependencies themselves.
		// TODO: should be mvNetworkDef::AllocateAndLoad
		//mvNetworkDef* def = rage_new mvNetworkDef;
		//if (def->Load(ref)) {
		//	// TODO: store the crc of the reference somewhere so we can push the signals into the tree
		//} else {
		//	mvErrorf("Failed to load referenced network '%s'", ref);
		//	// TODO: something redundant so the motion tree isn't fucked because
		//	//	part of it is missing. maybe just push in an invalid
		//}
	}
#undef kMaxPath

	m_XRefCount = xrefcount;

	stream->ReadInt(&m_reqcount, 1);
	m_requests = iter.Get<request_t>();
	for (u32 i = 0; i < m_reqcount; ++i) {
		iter.ReadInt(stream);	// key
		iter.ReadInt(stream);	// index
	}

	stream->ReadInt(&m_flagcount, 1);
	m_flags = iter.Get<flag_t>();
	for (u32 i = 0; i < m_flagcount; ++i) {
		iter.ReadInt(stream);	// key
		iter.ReadInt(stream);	// index
	}

	m_network = iter.Get<u8>();
	ReadNetwork(&iter, stream);
	
	iter.Read(stream, header.StringTableLength);

	stream->Close();

	return true;
}

} // namespace rage
