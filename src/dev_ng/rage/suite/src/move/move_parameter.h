//
// move/move_parameter.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_PARAMETER_H
#define MOVE_PARAMETER_H

#include "atl/referencecounter.h"
#include "cranimation/animation.h"
#include "cranimation/frame.h"
#include "cranimation/framefilters.h"
#include "crclip/clip.h"
#include "crextra/expressions.h"
#include "crparameterizedmotion/parameterizedmotion.h"
#include "data/base.h"

#include "move_fixedheap.h"


#define RESTRICTED_PARAMTER_TYPES (HACK_GTA4 && 1)

namespace rage {

class atString;
class crAnimation;
class crClip;
class crExpressions;
class crFrameFilter;
class crFrame;
class crProperty;
class crpmParameterizedMotion;
class crmtNode;

class mvNetworkDef;
class mvNodeState;
class mvObserver;
class mvSubNetwork;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Sort of type-safe storage for parameters supported by move
class mvParameter
{
public:
	// PURPOSE: Parameter types supported
	enum ParamType
	{
#if !RESTRICTED_PARAMTER_TYPES
		kParameterAnimation,
#else // !RESTRICTED_PARAMTER_TYPES
		kParameterNone = 0,
#endif // !RESTRICTED_PARAMTER_TYPES

		kParameterClip,
		kParameterExpressions,
		kParameterFilter,
		kParameterFilterN,
		kParameterFrame,
		kParameterPM,
		kParameterBoolean,
		kParameterReal,
		kParameterInt,
		kParameterData,
		kParameterNode,
		kParameterObserver,
		kParameterNetwork,
		kParameterReference,
		kParameterProperty,
		kParameterNodeId,
		kParameterNodeName,

#if !RESTRICTED_PARAMTER_TYPES
		kParameterNone,
#else // !RESTRICTED_PARAMTER_TYPES
		kParameterAnimation,
#endif // !RESTRICTED_PARAMTER_TYPES

		kParameterRequest,
		kParameterFlag,
	};

#if __ASSERT
public:
	// PURPOSE: Resolve type into string
	static const char* GetParamTypeName(ParamType type)
	{
		static const char* paramTypeNames[] = {
#if !RESTRICTED_PARAMTER_TYPES
			"kParameterAnimation",
#else // !RESTRICTED_PARAMTER_TYPES
			"kParameterNone",
#endif // !RESTRICTED_PARAMTER_TYPES
			"kParameterClip",
			"kParameterExpressions",
			"kParameterFilter",
			"kParameterFilterN",
			"kParameterFrame",
			"kParameterPM",
			"kParameterBoolean",
			"kParameterReal",
			"kParameterInt",
			"kParameterData",
			"kParameterNode",
			"kParameterObserver",
			"kParameterNetwork",
			"kParameterReference",
			"kParameterProperty",
			"kParameterNodeId",
			"kParameterNodeName",
#if !RESTRICTED_PARAMTER_TYPES
			"kParameterNone",
#else // !RESTRICTED_PARAMTER_TYPES
			"kParameterAnimation",
#endif // !RESTRICTED_PARAMTER_TYPES
			"kParameterRequest",
			"kParameterFlag"
		};
		return paramTypeNames[type];
	}
#endif //__ASSERT

public:

	// PURPOSE: Get type of the parameter data stored
	ParamType GetType() const {
		return (ParamType)m_type;
	}

	// PURPOSE: Store animation in parameter
#if	RESTRICTED_PARAMTER_TYPES
	void SetAnimationUnsafe(const crAnimation* ASSERT_ONLY(anim))
	{
		Assert(!anim);
	}
#else // RESTRICTED_PARAMTER_TYPES
	void SetAnimationUnsafe(const crAnimation* anim)
	{
		m_type = kParameterAnimation;
		m_anim = anim;
	}
#endif // RESTRICTED_PARAMTER_TYPES

	// PURPOSE: Store animation in parameter
#if RESTRICTED_PARAMTER_TYPES
	void SetAnimationSafe(const crAnimation* ASSERT_ONLY(anim))
	{
		Assert(!anim);
	}
#else // RESTRICTED_PARAMTER_TYPES
	void SetAnimationSafe(const crAnimation* anim)
	{
		m_type = kParameterAnimation;
		m_anim = anim;
		if(m_anim)
		{
			m_anim->AddRef();
			BANK_SAFE_ADD_KNOWN_REF(m_anim);
		}
	}
#endif // RESTRICTED_PARAMTER_TYPES

	// PURPOSE: Get animation from parameter
	// NOTES:
	//	type of stored data must be kParameterAnimation
	const crAnimation* GetAnimation() const
	{
		Assert(m_type == kParameterAnimation);
		return m_anim;
	}

	// PURPOSE: Store clip in parameter
	void SetClipUnsafe(const crClip* clip)
	{
		m_type = kParameterClip;
		m_clip = clip;
	}

	// PURPOSE: Store clip in parameter
	void SetClipSafe(const crClip* clip)
	{
		m_type = kParameterClip;
		m_clip = clip;
		if(m_clip)
		{
			m_clip->AddRef();
			BANK_SAFE_ADD_KNOWN_REF(m_clip);
		}
	}

	// PURPOSE: Get clip from parameter
	// NOTES:
	//	type of stored data must be kParameterClip
	const crClip* GetClip() const
	{
		Assert(m_type == kParameterClip);
		return m_clip;
	}

	// PURPOSE: Set expressions in parameter
	void SetExpressionsUnsafe(const crExpressions* expr)
	{
		m_type = kParameterExpressions;
		m_expr = expr;
	}

	// PURPOSE: Set expressions in parameter
	void SetExpressionsSafe(const crExpressions* expr)
	{
		m_type = kParameterExpressions;
		m_expr = expr;
		if(m_expr)
		{
			m_expr->AddRef();
		}
	}

	// PURPOSE: Get expressions from parameter
	// NOTES:
	//	type of stored data must be kParameterExpressions
	const crExpressions* GetExpressions() const
	{
		Assert(m_type == kParameterExpressions);
		return m_expr;
	}
	
	// PURPOSE: Set filter in parameter
	void SetFilterUnsafe(crFrameFilter* filter)
	{
		m_type = kParameterFilter;
		m_filter = filter;
	}

	// PURPOSE: Set filter in parameter
	void SetFilterSafe(crFrameFilter* filter)
	{
		m_type = kParameterFilter;
		m_filter = filter;
		if(m_filter)
		{
			m_filter->AddRef();
		}
	}

	// PURPOSE: Get filter from parameter
	// NOTES:
	//	type of stored data must be kParameterFilter
	crFrameFilter* GetFilter() const
	{
		Assert(m_type == kParameterFilter);
		return m_filter;
	}

	// PURPOSE: Set frame in parameter
	void SetFrameUnsafe(crFrame* frame)
	{
		m_type = kParameterFrame;
		m_frame = frame;
	}

	// PURPOSE: Set frame in parameter
	void SetFrameSafe(crFrame* frame)
	{
		m_type = kParameterFrame;
		m_frame = frame;
		if(m_frame)
		{
			m_frame->AddRef();
		}
	}

	// PURPOSE: Get frame from parameter
	// NOTES:
	//	type of stored data must be kParameterFrame
	crFrame* GetFrame() const
	{
		Assert(m_type == kParameterFrame);
		return m_frame;
	}

	// PURPOSE: Set parameterized motion in parameter
#if RESTRICTED_PARAMTER_TYPES
	void SetPMUnsafe(crpmParameterizedMotion* ASSERT_ONLY(pm))
	{
		Assert(!pm);
	}
#else // RESTRICTED_PARAMTER_TYPES
	void SetPMUnsafe(crpmParameterizedMotion* pm)
	{
		m_type = kParameterPM;
		m_pm = pm;
	}
#endif // RESTRICTED_PARAMTER_TYPES

	// PURPOSE: Set parameterized motion in parameter
#if RESTRICTED_PARAMTER_TYPES
	void SetPMSafe(crpmParameterizedMotion* ASSERT_ONLY(pm))
	{
		Assert(!pm);
	}
#else // RESTRICTED_PARAMTER_TYPES
	void SetPMSafe(crpmParameterizedMotion* pm)
	{
		m_type = kParameterPM;
		m_pm = pm;
		if(m_pm)
		{
			//m_pm->AddRef();
			//BANK_SAFE_ADD_KNOWN_REF(m_pm);
		}
	}
#endif // RESTRICTED_PARAMTER_TYPES

	// PURPOSE: Get parameterized motion from parameter
	// NOTES:
	//	type of stored data must be kParameterPM
	crpmParameterizedMotion* GetPM() const
	{
		Assert(m_type == kParameterPM);
		return m_pm;
	}
	
	// PURPOSE: Set bool in parameter
	void SetBool(bool boolean)
	{
		m_type = kParameterBoolean;
		m_data = NULL;
		m_boolean = boolean;
	}

	// PURPOSE: Get bool from parameter
	// NOTES:
	//	type of stored data must be kParameterBoolean
	bool GetBool() const
	{
		Assert(m_type == kParameterBoolean);
		return m_boolean;
	}
	
	// PURPOSE: Set float in parameter
	void SetReal(float real)
	{
		m_type = kParameterReal;
		m_data = NULL;
		m_real = real;
	}

	// PURPOSE: Get float from parameter
	// NOTES:
	//	type of stored data must be kParameterReal
	float GetReal() const
	{
		Assert(m_type == kParameterReal);
		return m_real;
	}

	// PURPOSE: Store integer in parameter
	void SetInt(int integer)
	{
		m_type = kParameterInt;
		m_data = NULL;
		m_int = integer;
	}

	// PURPOSE: Get integer from parameter
	// NOTES:
	//	type of stored data must be kParameterInt
	int GetInt() const
	{
		Assert(m_type == kParameterInt);
		return m_int;
	}
	
	// PURPOSE: Store user data in parameter
	void SetData(void* data)
	{
		m_type = kParameterData;
		m_data = data;
	}

	// PURPOSE: Get user data from parameter
	// NOTES:
	//	type of stored data must be kParameterData
	void* GetData() const
	{
		Assert(m_type == kParameterData);
		return m_data;
	}
	
	// PURPOSE: Store node in parameter
	void SetNode(crmtNode* node)
	{
		m_type = kParameterNode;
		m_node = node;
	}

	// PURPOSE: Get node from parameter
	// NOTES:
	//	type of stored data must be kParameterNode
	crmtNode* GetNode() const
	{
		Assert(m_type == kParameterNode);
		return m_node;
	}
	
	// PURPOSE: Store observer in parameter
	void SetObserverUnsafe(mvObserver* observer)
	{
		m_type = kParameterObserver;
		m_observer = observer;
	}

	// PURPOSE: Store observer in parameter
	void SetObserverSafe(mvObserver* observer);

	// PURPOSE: Get observer from parameter
	// NOTES:
	//	type of stored data must be kParameterObserver
	mvObserver* GetObserver() const
	{
		Assert(m_type == kParameterObserver);
		return m_observer;
	}
	
	// PURPOSE: Store network in parameter
	void SetNetworkUnsafe(mvSubNetwork* network)
	{
		m_type = kParameterNetwork;
		m_network = network;
	}

	// PURPOSE: Store network in parameter
	void SetNetworkSafe(mvSubNetwork* network);

	// PURPOSE: Get network from parameter
	// NOTES:
	//	type of stored data must be kParameterNetwork
	mvSubNetwork* GetNetwork() const
	{
		Assert(m_type == kParameterNetwork);
		return m_network;
	}

	// PURPOSE: Store reference in parameter
	void SetReference(const mvNetworkDef* ref)
	{
		m_type = kParameterReference;
		m_Ref = ref;
	}

	// PURPOSE: Get reference from parameter
	// NOTES:
	//	type of stored data must be kParameterReference
	const mvNetworkDef* GetNetReference() const
	{
		Assert(m_type == kParameterReference);
		return m_Ref;
	}

	// PURPOSE: Store property in parameter
	// NOTES:
	//	This is not safe. If a defrag happens or the owning clip is removed from memory, dereferencing the crProperty will blow up
	void SetPropertyUnsafe(const crProperty* property)
	{
		m_type = kParameterProperty;
		m_Property = property;
	}

	// PURPOSE: Store property in parameter
	// NOTES:
	void SetPropertySafe(const crProperty* property)
	{
		m_type = kParameterProperty;
		m_Property = property;
		if(m_Property)
		{
			BANK_SAFE_ADD_KNOWN_REF(m_Property);
		}
	}

	// PURPOSE: Get property from parameter
	// NOTES:
	//	type of stored data must be kParameterProperty.
	// SEE ALSO: SetProperty for warning of dereferencing the return value from this call.
	const crProperty* GetProperty() const
	{
		Assert(m_type == kParameterProperty);
		return m_Property;
	}

	// PURPOSE: Store node name in parameter
	const void SetNodeName(const u32 name)
	{
		m_type = kParameterNodeName;
		m_data = NULL;
		m_Id = name;
	}

	// PURPOSE: Set node id in parameter
	const void SetNodeId(const u32 id)
	{
		m_type = kParameterNodeId;
		m_data = NULL;
		m_Id = id;
	}

	// PURPOSE: Get node id from parameter
	// NOTES:
	//	type of stored data must be kParameterNodeId
	const u32 GetNodeId() const
	{
		Assert(m_type == kParameterNodeId);
		return m_Id;
	}

	// PURPOSE: Set request in parameter
	void SetRequest(bool boolean)
	{
		m_type = kParameterRequest;
		m_data = NULL;
		m_boolean = boolean;
	}

	// PURPOSE: Get request from parameter
	// NOTES:
	//	type of stored data must be kParameterRequest
	bool GetRequest() const
	{
		Assert(m_type == kParameterRequest);
		return m_boolean;
	}

	// PURPOSE: Set flag in parameter
	void SetFlag(bool boolean)
	{
		m_type = kParameterFlag;
		m_data = NULL;
		m_boolean = boolean;
	}

	// PURPOSE: Get flag from parameter
	// NOTES:
	//	type of stored data must be kParameterFlag
	bool GetFlag() const
	{
		Assert(m_type == kParameterFlag);
		return m_boolean;
	}

	// PURPOSE: Get untyped data
	// NOTES:
	//	Try not to use this if you can, but if you don't want the type checks this is the way around that. Used internally.
	void* GetRawData() const
	{
		return m_data;
	}

	// PURPOSE: Call release on parameters that support reference counting.
	// NOTES:
	//	This is slow. If you already know the type this parameter contains you're better of calling Release directly yourself.
	void Release();

	// PURPOSE: Call addref on parameters that support reference counting.
	// NOTES:
	//	This is slow. If you already know the type this parameter contains you're better off calling AddRef directly yourself.
	void AddRef();

	// PURPOSE: Call AddKnownRef on parameters that support defragmentation.
	// NOTES:
	//	This is kind of slow. If you already know the type of this parameter call knownref yourself.
	void AddKnownRef();

	// PURPOSE: Call RemoveKnownRef on parameters that support defragmentation.
	// NOTES:
	// This is kind of slow. If you already know the type of this parameter call knownref yourself.
	void RemoveKnownRef();

	// PURPOSE: Is parameter duplicable in buffers
	// NOTES: This might be data driven by flag in future, but for now its based on type inspection
	bool IsDuplicable() const
	{
		return m_type == kParameterProperty;
	}

	// PURPOSE: is parameter complex (ie does it require ref counting)
	bool IsComplex() const
	{
		switch(m_type)
		{
#if !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterAnimation:
#endif // !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterClip:
		case mvParameter::kParameterExpressions:
		case mvParameter::kParameterFilter:
		case mvParameter::kParameterFrame:
#if !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterPM:
#endif // !RESTRICTED_PARAMTER_TYPES
		case mvParameter::kParameterObserver:
		case mvParameter::kParameterNetwork:
		case mvParameter::kParameterProperty:
			return true;
		default:
			return false;
		}
	}

#if __BANK
	// PURPOSE: Dump human readable string of this parameter
	void Dump(atString&) const;
#endif  //__BANK

private:

	// PURPOSE: Parameter storage
	union
	{
		const crAnimation* m_anim;
		const crClip* m_clip;
		const crExpressions* m_expr;
		crFrameFilter* m_filter;
		crFrame* m_frame;
		crpmParameterizedMotion* m_pm;
		bool m_boolean;
		float m_real;
		void* m_data;
		crmtNode* m_node;
		mvObserver* m_observer;
		mvSubNetwork* m_network;
		const mvNetworkDef* m_Ref;
		const crProperty* m_Property;
		int m_int;
		u32 m_Id;
	};

	// PURPOSE: Parameter type
	u32 m_type;

	friend class mvParameterBuffer;
	friend struct mvKeyedParameter;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Type safe parameter initialization
template <typename T>
class mvInitParameter : public mvParameter
{
public:
	mvInitParameter(T t);
};

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<const crAnimation*>::mvInitParameter(const crAnimation* anim)
{
	SetAnimationUnsafe(anim);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<const crClip*>::mvInitParameter(const crClip* clip)
{
	SetClipUnsafe(clip);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<const crExpressions*>::mvInitParameter(const crExpressions* expr)
{
	SetExpressionsUnsafe(expr);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<crFrameFilter*>::mvInitParameter(crFrameFilter* filter)
{
	SetFilterUnsafe(filter);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<crFrame*>::mvInitParameter(crFrame* frame)
{
	SetFrameUnsafe(frame);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<crpmParameterizedMotion*>::mvInitParameter(crpmParameterizedMotion* pm)
{
	SetPMUnsafe(pm);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<bool>::mvInitParameter(bool boolean)
{
	SetBool(boolean);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<float>::mvInitParameter(float t)
{
	SetReal(t);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<int>::mvInitParameter(int i)
{
	SetInt(i);
};

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<void*>::mvInitParameter(void* data)
{
	SetData(data);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<crmtNode*>::mvInitParameter(crmtNode* node)
{
	SetNode(node);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<mvObserver*>::mvInitParameter(mvObserver* observer)
{
	SetObserverUnsafe(observer);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<mvSubNetwork*>::mvInitParameter(mvSubNetwork* network)
{
	SetNetworkUnsafe(network);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<const mvNetworkDef*>::mvInitParameter(const mvNetworkDef* ref)
{
	SetReference(ref);
}

//////////////////////////////////////////////////////////////////////////

template<>
inline mvInitParameter<const crProperty*>::mvInitParameter(const crProperty* prop)
{
	SetPropertyUnsafe(prop);
}

//////////////////////////////////////////////////////////////////////////

template <typename T>
inline mvInitParameter<T>::mvInitParameter(T t)
{
}

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Keyed parameter. Represents the parameter input as a key,value pair
struct mvKeyedParameter
{
	// PURPOSE: Identifier for this parameter
	// NOTES: This value will be defined by an atStringHash of a parameter name in makenetwork
	u32 Key;

	// PURPOSE: The value of this parameter with it's type
	mvParameter Parameter;

#if __BANK
	// PURPOSE: Dump human readable version of this keyed parameter
	void Dump(atString&) const;
#endif //__BANK

	// PURPOSE: Release and reset keyed parameter
	void ResetRelease();
};

//////////////////////////////////////////////////////////////////////////

__forceinline void mvKeyedParameter::ResetRelease()
{
	Parameter.RemoveKnownRef();
	Parameter.Release();

	Key = 0;
	Parameter.m_type = mvParameter::kParameterNone;
	Parameter.m_data = NULL;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // MOVE_PARAMETER_H

#include "move_parameter.inl"

