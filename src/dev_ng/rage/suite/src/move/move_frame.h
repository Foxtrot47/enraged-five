//
// move/move_frame.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef MOVE_FRAME_H
#define MOVE_FRAME_H

#include "move_node.h"

#include "crmotiontree/nodeframe.h"

namespace rage {

class crFrame;

//////////////////////////////////////////////////////////////////////////

// PURPOSE: In memory definition of how to craete a frame node
class mvNodeFrameDef : public mvNodeDef
{
public:
	// PURPOSE: Create an instance of this frame node definition
	// PARAMS: context - information about this nodes position in the motion tree/network hierarchy
	class mvNodeFrame* Create(mvCreateContext* context) const;

	// PURPOSE: Initialize this definition from streamed data
	// PARAMS: resource - memory stream
	void Init(mvMemoryResource* resource);

	// PURPOSE: How big will this definition be once it's read into memory
	u32 QueryMemorySize() const;

	// PURPOSE: Get the parameter set function for the parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	 makenetwork and code must match.
	static mvNode::SetParameterFunc* ResolveParameterSet(u16 parameter);

	// PURPOSE: Get the type of the parameter that the set function at index 'parameter' requires
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvParameter::ParamType ResolveParameterSetType(u16 parameter);

	// PURPOSE: Get the parameter get function for parameter at index 'parameter'
	// NOTES: parameter is exported by makenetwork, but the indices between
	//	makenetwork and code must match.
	static mvNode::GetParameterFunc* ResolveParameterGet(u16 parameter);

	// PURPOSE: Get the event id to fire at event index 'type'
	static u32 ResolveEvent(u16 event);

	// PURPOSE: Register this definition with moves type system
	MOVE_DECLARE_NODE_DEF(mvNodeFrameDef);

private:
	enum
	{
		kFrameMask = 3,
		kFrameOwnerShift = 4,
		kFrameOwnerMask = 3 << kFrameOwnerShift,
	};

	// PURPOSE: flags define how to read the definition data that hangs off this struct
	// SEE ALSO: Flag enum in move_types_internal
	// NOTES: Flag layout is:
	//	Frame - flags & 0x00000003
	//	Frame owner - (flags & 0x00000030) >> 0x04
	// TODO: Frame owner is no longer used as crFrame now supports reference counting.
	//	The data that is written out by makenetwork should be update to reflect this.
	u32 m_Flags;
};

//////////////////////////////////////////////////////////////////////////

// PURPOSE: Concrete move frame node
// NOTES: We extend the motion tree node but don't add any extra data
//	of our own. That way we can define some extra interface functions
//	but continue to use the crmt node factory.
class mvNodeFrame : public crmtNodeFrame
{
public:
	// PURPOSE: Xmacro defines the signals we can set/get
	// NOTES: These prove static thunks that we can take
	//	a function pointer to and use from observers to
	//	update values in nodes and the parameter buffer.
	#define MOVE_SIGNALS(T) \
		MOVE_FRAME_SET(T, ApplyFrame) \
		MOVE_FRAME_GET(T, GetFrame)
		
	// PURPOSE: Unroll the above Xmacro
	MOVE_SIGNALS(mvNodeFrame)

	// PURPOSE: Don't pollute everything with the xmacro
	#undef MOVE_SIGNALS

	// PURPOSE: Register this node type with moves type system
	MOVE_DECLARE_NODE_TYPE(mvNodeFrame);
};

///////////////////////////////////////////////////////////////////////////////

inline void mvNodeFrame::ApplyFrame(crFrame* frame)
{
	crmtNodeFrame::SetFrame(frame);
}

///////////////////////////////////////////////////////////////////////////////

inline crFrame* mvNodeFrame::GetFrame() const
{
	return const_cast<crFrame*>(crmtNodeFrame::GetFrame());
}

} // namespace rage

#endif // MOVE_FRAME_H
