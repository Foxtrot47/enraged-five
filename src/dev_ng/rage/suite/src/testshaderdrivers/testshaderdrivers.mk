SHELL = $(WINDIR)\system32\cmd.exe

ifeq 	($(PLATFORM),psn)
SHADERDIR = psn
SHADEREXT = cgx
else
ifeq 	($(PLATFORM),xenon)
SHADERDIR = fxl_final
SHADEREXT = fxc
else
SHADERDIR	= win32_30
SHADERDIR_30ATI9 = win32_30_atidx9
SHADERDIR_30ATI10 = win32_30_atidx10
SHADERDIR_30NV9 = win32_30_nvdx9
SHADERDIR_30NV10 = win32_30_nvdx10
SHADERDIR_40ATI10 = win32_40_atidx10
SHADERDIR_40NV10 = win32_40_nvdx10
SHADEREXT = fxc
endif
endif

ifeq    ($(RAGE_ASSET_ROOT),)
badEnv:
	@echo $$(RAGE_ASSET_ROOT) env variable is not defined.
	@exit 1
endif

RAGE_DIRECTORY = $(subst \,/,$(RAGE_DIR))

MAKEDEP_PATH = $(subst \,/,$(RS_TOOLSROOT)\bin\coding\makedep.exe)

MAKESHADER_PATH = $(subst \,/,$(RS_TOOLSROOT)\script\coding\shaders\makeshader.bat)

SHADERPATH = $(subst \,/,$(RAGE_ASSET_ROOT)\testshaderdrivers\shaders\lib\)

SHADERPATHDOS = $(subst /,\,$(SHADERPATH))

all: alltargets

clean:
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR)/*.$(SHADEREXT))
	if exist $(SHADERDIR)\*.d del $(SHADERDIR)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI9)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI9)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30ATI9)\*.d del $(SHADERDIR_30ATI9)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30ATI10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30ATI10)\*.d del $(SHADERDIR_30ATI10)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV9)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV9)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30NV9)\*.d del $(SHADERDIR_30NV9)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_30NV10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_30NV10)\*.d del $(SHADERDIR_30NV10)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_40ATI10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_40ATI10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_40ATI10)\*.d del $(SHADERDIR_40ATI10)\*.d
	if exist $(subst /,\,$(SHADERPATH)$(SHADERDIR_40NV10)/*.$(SHADEREXT)) del $(subst /,\,$(SHADERPATH)$(SHADERDIR_40NV10)/*.$(SHADEREXT))
	if exist $(SHADERDIR_40NV10)\*.d del $(SHADERDIR_40NV10)\*.d

# FLAGS = -useATGCompiler
FLAGS = -noPerformanceDump -quiet

TARGETS += $(SHADERPATH)$(SHADERDIR)/rage_mirror.$(SHADEREXT)

-include $(SHADERDIR)/rage_mirror.d

$(SHADERPATH)$(SHADERDIR)/rage_mirror.$(SHADEREXT): rage_mirror.fx
	if not exist $(SHADERDIR)\* mkdir $(SHADERDIR)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR)/rage_mirror.$(SHADEREXT) rage_mirror.fx $(SHADERDIR)/rage_mirror.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR) $(FLAGS) rage_mirror.fx

ifeq ($(PLATFORM),win32)
TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mirror.$(SHADEREXT)

-include $(SHADERDIR_30ATI9)/rage_mirror.d

$(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mirror.$(SHADEREXT): rage_mirror.fx
	if not exist $(SHADERDIR_30ATI9)\* mkdir $(SHADERDIR_30ATI9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI9)/rage_mirror.$(SHADEREXT) rage_mirror.fx $(SHADERDIR_30ATI9)/rage_mirror.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI9) $(FLAGS) rage_mirror.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mirror.$(SHADEREXT)

-include $(SHADERDIR_30ATI10)/rage_mirror.d

$(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mirror.$(SHADEREXT): rage_mirror.fx
	if not exist $(SHADERDIR_30ATI10)\* mkdir $(SHADERDIR_30ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mirror.$(SHADEREXT) rage_mirror.fx $(SHADERDIR_30ATI10)/rage_mirror.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30ATI10) $(FLAGS) rage_mirror.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30ATI10)/rage_mirror.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mirror.$(SHADEREXT)

-include $(SHADERDIR_30NV9)/rage_mirror.d

$(SHADERPATH)$(SHADERDIR_30NV9)/rage_mirror.$(SHADEREXT): rage_mirror.fx
	if not exist $(SHADERDIR_30NV9)\* mkdir $(SHADERDIR_30NV9)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV9)/rage_mirror.$(SHADEREXT) rage_mirror.fx $(SHADERDIR_30NV9)/rage_mirror.d
	call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV9) $(FLAGS) rage_mirror.fx

TARGETS += $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mirror.$(SHADEREXT)

-include $(SHADERDIR_30NV10)/rage_mirror.d

$(SHADERPATH)$(SHADERDIR_30NV10)/rage_mirror.$(SHADEREXT): rage_mirror.fx
	if not exist $(SHADERDIR_30NV10)\* mkdir $(SHADERDIR_30NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_30NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_30NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mirror.$(SHADEREXT) rage_mirror.fx $(SHADERDIR_30NV10)/rage_mirror.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_30NV10) $(FLAGS) rage_mirror.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_30NV10)/rage_mirror.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mirror.$(SHADEREXT)

-include $(SHADERDIR_40ATI10)/rage_mirror.d

$(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mirror.$(SHADEREXT): rage_mirror.fx
	if not exist $(SHADERDIR_40ATI10)\* mkdir $(SHADERDIR_40ATI10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40ATI10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40ATI10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mirror.$(SHADEREXT) rage_mirror.fx $(SHADERDIR_40ATI10)/rage_mirror.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40ATI10) $(FLAGS) rage_mirror.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40ATI10)/rage_mirror.$(SHADEREXT)

TARGETS += $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mirror.$(SHADEREXT)

-include $(SHADERDIR_40NV10)/rage_mirror.d

$(SHADERPATH)$(SHADERDIR_40NV10)/rage_mirror.$(SHADEREXT): rage_mirror.fx
	if not exist $(SHADERDIR_40NV10)\* mkdir $(SHADERDIR_40NV10)
	if not exist $(SHADERPATHDOS)$(SHADERDIR_40NV10)\* mkdir $(SHADERPATHDOS)$(SHADERDIR_40NV10)
	$(MAKEDEP_PATH) -I $(RAGE_DIRECTORY)\base\src $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mirror.$(SHADEREXT) rage_mirror.fx $(SHADERDIR_40NV10)/rage_mirror.d
	if exist %windir%\system32\d3d10.dll call $(MAKESHADER_PATH) -platform $(SHADERDIR_40NV10) $(FLAGS) rage_mirror.fx
	if not exist %windir%\system32\d3d10.dll echo DUMMY > $(SHADERPATH)$(SHADERDIR_40NV10)/rage_mirror.$(SHADEREXT)

endif

TARGETS += $(SHADERPATH)preload.list

$(SHADERPATH)preload.list: preload.list
	if not exist $(SHADERPATHDOS)* mkdir $(SHADERPATHDOS)
	if exist $(SHADERPATHDOS)preload.list del /f $(SHADERPATHDOS)preload.list
	type preload.list > $(SHADERPATHDOS)preload.list

alltargets: $(TARGETS)
	echo DONE > $(SHADERDIR).dummy
