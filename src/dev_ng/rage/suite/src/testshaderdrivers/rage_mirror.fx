
#define NO_SKINNING

#include "../../../base/src/shaderlib/rage_common.fxh"
#include "../../../base/src/shaderlib/rage_drawbucket.fxh"
#include "../../../base/src/shaderlib/rage_specular.fxh"
#include "../wildshaders/rage_refraction.fxh"

RAGE_DRAWBUCKET(15)

struct VS_OUTPUT
{
	float4	Pos:POSITION;
	float3	Normal:TEXCOORD1;
	float3	WorldPos: TEXCOORD2;
	float4  ScrPos : TEXCOORD3;
	float3	View : TEXCOORD4;
};

BeginSampler(sampler2D,ReflectionTexture,ReflectionSampler,ReflectionTex )
    string ResourceName= "__reflectionplanert__.dds";
ContinueSampler(sampler2D,ReflectionTexture,ReflectionSampler,ReflectionTex )
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	AddressW  = CLAMP;
	MIPFILTER = NONE;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MipMapLodBias = 0;
EndSampler;

float Reflectivity
<
	string UIName = "Reflectivity ( 0.02 water 1.0f mirror )";
	string UIWidget = "Numeric";
	float UIMin = 0.0;
	float UIMax = 1.0;
	float UIStep = 0.01;	
> = 1.0;

float3 ReflectionPlanarGet( float4 scrPos )
{
	scrPos.xy /= scrPos.w;
	float2 refl = scrPos.xy;

	refl = refl * 0.5 + 0.5;
	refl.y = 1-refl.y;	

	return tex2D(ReflectionSampler,refl).xyz;
}

/*
**
*/
 
VS_OUTPUT vs_main( rageVertexInputBump IN) 
{
	VS_OUTPUT		OUT;
		
	OUT.Pos = mul( float4(IN.pos, 1.0f), gWorldViewProj );
	OUT.Normal = mul( IN.normal.xyz, (float3x3) gWorld );
	OUT.WorldPos = mul( float4(IN.pos, 1.0f),  gWorld ).xyz;
	OUT.ScrPos = OUT.Pos;
	OUT.View = normalize(OUT.WorldPos - gViewInverse[3].xyz);

	return OUT;
}
float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	float3 refractedColor = RefractionPlanarGet(IN.WorldPos, normalize(IN.Normal), normalize(IN.View));
	float3 reflectedColor =  ReflectionPlanarGet( IN.ScrPos ).xyz;

	return float4( reflectedColor,1.0f);
}
//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
      ZENABLE = true;
     // ZWriteEnable = true;
  	  CullMode = None;
      ALPHATESTENABLE = false;
      ALPHABLENDENABLE = true;
 #if __XENON
	  HighPrecisionBlendEnable = true;
#endif

	  BlendOp = Add;
      SrcBlend = SRCALPHA;
      DestBlend = INVSRCALPHA;
      
      VertexShader = compile VERTEXSHADER vs_main();
      PixelShader = compile PIXELSHADER ps_main();
   }
}
