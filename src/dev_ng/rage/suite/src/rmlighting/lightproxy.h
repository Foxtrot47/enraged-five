// 
// rmlighting/lightproxy.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMLIGHTING_LIGHTPROXY_H
#define RMLIGHTING_LIGHTPROXY_H

#include "atl/array.h"
#include "system/bit.h"
#include "vector/matrix34.h"

namespace rage {

class crSkeleton;
class grcViewport;
class rmcDrawableBase;
class rmlLightSource;
class grmShaderData;

// PURPOSE: Container for an object to render by the lighting/shadowing system
struct rmlRenderInstance {
	const rmcDrawableBase *m_Drawable;
	const Matrix34 *m_Mtx;
	const crSkeleton *m_Skel;
	u32 m_Lod;
};


// PURPOSE: This is an interface layer so that the various lighting / shadowing systems can
//			properly communicate with higher level code for items such as instance lists, etc.
// NOTES:	A clear distinction is being made here between methods callable by the higher level game
//			code, and methods called by the various lighting subsystems.
class rmlLightProxy {
	friend class rmlLightingMgr;
	friend class rmlShadowMap;
public:
	rmlLightProxy();
	virtual ~rmlLightProxy();

	enum { FLG_LIGHT_OCCLUDERS = BIT0,		// Only need light occluder drawables
		FLG_CASTER_LIGHT_PASS = BIT1,	// Indication that this is needed for a shadow casting light
		FLG_CASTER_COLOR_PASS = BIT2,	// Indication that this is needed for a shadow casting light
		FLG_LAST = BIT3 };

	// PURPOSE: Initialize the proxy layer
	// PARAMS:	bucketCount - The number of distinct render buckets supported
	//			maxVisibleInstances - The number of instances potentially visible in one scene
	// NOTES:	Higher level code is responsible for calling this before passing into lightmgr
	virtual void Init( int bucketCount, int maxVisibleInstances );

	// PURPOSE: Register a render instance
	// PARAMS:	drawable - The object to render
	//			mtx - The matrix (or array of matricies) to use for this model 
	//					MUST BE VALID FOR THIS ENTIRE FRAME
	//			skel - The skeleton to use for skinned models
	//			lod - Which LOD is needed for this pass
	// TODO:  UNIFY RM modules with a VERY basic renderable class, along with a method of
	//			handling renderlists
	// NOTES:  When registering instances for a given light source, it will probably be best to use
	//			the same LOD as computed from the camera's frustrum.
	inline void RegisterRenderInstance(const rmcDrawableBase *drawable, const Matrix34 &mtx, const crSkeleton *skel, u32 lod);

protected:
	// PURPOSE: This is called when a shadow system needs a list of objects with a particular frustrum
	// PARAMS: viewport -- the viewport to use for culling purposes
	//			flags -- misc. flags to define the query better
	//			source -- (MAY BE NULL) The light source to consider for the query
	// NOTES:	Derived class is responsible for calling Resize(0) on m_VisibleInstances
	virtual void BuildVisibleListForViewport( const grcViewport *viewport, u32 flags, const rmlLightSource *source )=0;

	// PURPOSE: This is called when the lighting system wants the game to render the scene from a particular viewport
	//			This method will be called AFTER the BuildVisibleListForViewport method has been called
	// PARAMS: viewport -- the viewport to use for rendering
	//			flags -- misc. flags to assist with rendering
	virtual void DrawVisibleItemsForViewport( const grcViewport *viewport, u32 flags ) = 0;

	// RETURNS: The number of renderable instances
	inline int GetVisibleInstanceCount() const;

	// PURPOSE: Retrieve the specified visible instance
	// PARAMS:	idx - The index of the instance to retrieve
	// RETURNS:	The instance data structure to use for rendering.
	inline const rmlRenderInstance *GetVisibleInstance(int idx) const;

	// PURPOSE:	Called by lighting code at the beginning of a particular bucket render
	// PARAMS:	bucketId - The bucket that will soon be rendered.
	//			flags - misc. flags to help derived classes have a better idea who is doing the rendering
	virtual void BeginBucketRender(int /*bucketId*/, u32 /*flags*/) { /* EMPTY */ }

	// PURPOSE: Called by lighting code at the end of a particular bucket's render
	// PARAMS:	bucketId - The bucket that was just rendered.
	//			flags - misc. flags to help derived classes have a better idea who was doing the rendering
	virtual void EndBucketRender(int /*bucketId*/, u32 /*flags*/) { /* EMPTY */ }	

	// Information needed to render the visible instances
	atArray<rmlRenderInstance>	m_VisibleInstances;
};

inline void rmlLightProxy::RegisterRenderInstance(const rmcDrawableBase *drawable, const Matrix34 &mtx, const crSkeleton *skel, u32 lod) {
	rmlRenderInstance &inst = m_VisibleInstances.Append();
	inst.m_Drawable = drawable;
	inst.m_Mtx = &mtx;
	inst.m_Skel = skel;
	inst.m_Lod = lod;
}

inline int rmlLightProxy::GetVisibleInstanceCount() const {
	return m_VisibleInstances.GetCount();
}

inline const rmlRenderInstance *rmlLightProxy::GetVisibleInstance(int idx) const {
	return &m_VisibleInstances[idx];
}

}	// namespace rage

#endif		// RMLIGHTING_LIGHTPROXY_H
