// 
// rmlighting/lightingmgr.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMLIGHTING_LIGHTINGMGR_H
#define RMLIGHTING_LIGHTINGMGR_H

#include "lightproxy.h"
#include "lightsource.h"

#include "atl/array.h"
#include "grshadowmap/cascadedshadowmap.h"
#include "grshadowmap/cloudshadowmap.h"
#include "spatialdata/shaft.h"
#include "system/bit.h"
#include "system/noncopyable.h"
#include "vector/vector4.h"

namespace rage {

class rmlLightProxy;
class rmlLightSource;
class rmlShadowMap;

// PURPOSE: Provide a means of initializing and shutting down the lighting and shadowing
//			subsystems, as well as provide a storage for light sources
//	NOTES:	Higher level code should derive their own versions of this manager to handle the
//			various subsystem initialization.
class rmlLightingMgr : public rage::datBase
{
public:
	rmlLightingMgr();

	// PURPOSE: Destructor. Note that this function will automatically set the current
	// lighting manager (SetLightingManager) to NULL if this object had been the
	// most recently set lighting manager.
	virtual ~rmlLightingMgr();

	// PURPOSE: This is called BEFORE any shaders are preloaded, which gives the lighting & shadowing
	//			systems a chance to register any custom behaviors they need from the shader system
	virtual void InitShaderSystem();

	// PURPOSE: Initialize the lighting and shadowing subsystems
	// PARAMS:	proxy - the interface object to use for cross communication
	//			maxLights - The maximum number of lights active at a given time
	//			shadowTiles - Number of shadow map tiles to use
	//	NOTES:  This method will, in turn, call the various virtual init methods listed below
	void Init(rmlLightProxy& proxy, s32 maxLights=128, u32 maxBuckets=4, u32 alphaBucketMask=BIT1, int shadowTiles=2);

	// PURPOSE:  Axe murder the lighting system & all subsystems
	void Shutdown();

	// RETURNS: A light source if any are free for use, NULL if not
	// PARAMS
	//		castsShadow -- set true if you want this source to cast a shadow (default false)
	//		castsLight  -- set false if you do not want this source to cast light 
	//					   useful for invisible shadow casting sources (default true)
	//	NOTES:	Light source is NOT active by default
	rmlLightSource& CreateLightSource(bool castsShadow=false, bool castsLight=true);

	// PURPOSE: Releases the specified light source (frees it for use by others)
	// PARAMS:	source -- the light source to activate
	void DestroyLightSource(rmlLightSource& source);

	// PURPOSE: Activates the specified light source
	// PARAMS:	source -- the light source to activate
	void ActivateLightSource(rmlLightSource& source);

	// PURPOSE: Deactivates the specified light source
	// PARAMS:	source -- the light source to deactivate
	void DeactivateLightSource(rmlLightSource&	source);

	// PURPOSE: Deactivates all active light sources
	void DeactivateAllLightSources();

	// PURPOSE: Set the ambient color
	// PARAMS:	color -- the color to set
	inline void SetAmbientColor(const Vector4 &color);

	// PURPOSE: Get the ambient color
	inline Vector4 GetAmbientColor() const;

	// PURPOSE: Register the light source as a shadow casting light
	// PARAMS:	source -- the light source to treat as a shadow caster
	// !!!!!!!!!!!!!!!! PULL THIS OUT, PUT IN RMLIGHSOURCE !!!!!!!!!!!!!!!!!
//	void RegisterShadowCasterSource(const rmlLightSource *source);	

	// PURPOSE: Update the lighting sub systems
	virtual void Update();

	// Begin the render phase
	virtual void DrawScene();

	void ActivateLightGroup();

	virtual void PostDraw() {};

	// PURPOSE: Set the current lighting manager.
	static void SetLightingManager(rmlLightingMgr *mgr)
	{
		sm_CurrentMgr = mgr;
	}

	// RETURNS: The current lighting manager that has been most
	// recently set using SetLightingManager. Asserts if there
	// is no lighting manager (or if the most recent lighting
	// manager has been deleted).
	static rmlLightingMgr *GetLightingManager()
	{
		Assert(sm_CurrentMgr);
		return sm_CurrentMgr;
	}

	// RETURNS: Pointer to initalized shadowmap system, may return NULL
	inline rmlShadowMap* GetShadowMapSystem();

	// RETURNS: Pointer to initialized cloud shadow system
	inline CloudShadowMap* GetCloudShadowMapSystem();

	// PURPOSE: This is the callback that will initialize command
	// buffers for use with the shadow pass.
	void ShadowPassGraphicsState();

#if __BANK
	//PURPOSE: Add tuning vars
	virtual void AddWidgets(class bkBank&bank);
#endif

	rmlLightSource* GetLight(int i) {
		return m_LightList[i];
	}

protected:
	virtual void ActivateShadowLightGroup();

	// PURPOSE: Initialize the shadow subsystem
	virtual void InitShadowSystem(rmlLightProxy& proxy, int shadowTiles = 2);

	// PURPOSE: Initialize the light sources
	// PARAMS:	lightCount - The number of lights to init
	virtual void InitLightSources(rmlLightProxy& proxy, s32 lightCount, u32 maxBuckets, u32 alphaBucketMask);

	// PURPOSE: Initialize anything else you would like to initialize
	virtual void InitClient() { /*EMPTY*/ }

	// PURPOSE: Shutdown the lighting sources
	virtual void ShutdownLightSources();
	
	// PURPOSE: Shutdown the shadow system
	virtual void ShutdownShadowSystem();
	
	// PURPOSE: Shutdown any extra client systems
	virtual void ShutdownClient() { /* EMPTY */ }

	static rmlLightingMgr *		sm_CurrentMgr;		// The currently active lighting manager
	rmlShadowMap *				m_ShadowMap;		// Shadow map object to use
	CloudShadowMap *			m_CloudShadowMap;	// Optional cloud shadow map object
	atArray<rmlLightSource *>	m_LightList;		// Current of available lights (array count indicates # active)
	rmlLightSource				m_SunShadowLight;	// this is the light for the sun shadow 
	rmlLightSource *			m_LightPool;		// All allocated light source objects
	int							m_ActiveCount;		// Total number of lights active
	Vector4						m_AmbientColor;		// AmbientLight Color (w term is intensity)
#if __BANK
	bool						m_RenderShadowMaps;		// do we render into shadow maps?
#endif // __BANK
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::SetAmbientColor(const Vector4 &color)
{
	m_AmbientColor.Set(color);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Vector4 rmlLightingMgr::GetAmbientColor() const
{
	return m_AmbientColor;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmlShadowMap* rmlLightingMgr::GetShadowMapSystem()
{
	return m_ShadowMap;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CloudShadowMap* rmlLightingMgr::GetCloudShadowMapSystem()
{
	return m_CloudShadowMap;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class rmlShadowMap : public CShadowMap
{
public:
	rmlShadowMap(rmlLightProxy& proxy,int numTiles);


	// PURPOSE: Registers a light source as a shadow casting light
	// PARAMS:	source -- the light source that is being used as a shadow caster
	void RegisterShadowCaster( rmlLightSource& source );

	// PURPOSE: Removes a light source from the list of shadow casters
	// PARAMS:	source -- the light source that is to be removed from the shadow caster list
	void RemoveShadowCaster( rmlLightSource& source );

	void RenderShadowCasters( rage::grcViewport &vp, bool  );

#if __BANK
	void AddSaveWidgets(bkBank& bank);
	void AddWidgets(bkBank &bank);

	void DebugDrawShafts() const;

	void CollectShaftInfo() 
	{
		m_ShaftIndex=0;
	}

	const spdShaft &GetShadowShaft(int x)
	{
		return m_Shafts[x];
	}

#endif

protected:
	inline void BuildVisibleListForViewport( const grcViewport *viewport, u32 flags, const rmlLightSource *source );
	inline void DrawVisibleItemsForViewport( const grcViewport *viewport, u32 flags );

private:
	NON_COPYABLE(rmlShadowMap);

	atArray<rmlLightSource*>	m_ShadowCasters;		// Array of shadow casters, first n are active
	u32							m_ActiveCount;			// Number of active shadow casters
	rmlLightProxy&				m_Proxy;				// The communication layer with game code
	u32							m_AlphaBucketMask;		// Flags for which buckets contain transparency
	u32							m_MaxBuckets;			// Maximum number of buckets needed by game
	u32							m_MaxLights;			// Maximum number of lights which could cast shadows
#if __BANK
	enum {MAX_DEBUG_SHAFTS=4};
	int							m_ShaftIndex;
	spdShaft					m_Shafts[MAX_DEBUG_SHAFTS];
	bool						m_DrawShadowShafts;
#endif

	PAR_PARSABLE;
};

inline void rmlShadowMap::BuildVisibleListForViewport(const grcViewport *vp, u32 flags, const rmlLightSource *src) {
	m_Proxy.BuildVisibleListForViewport(vp,flags,src);
}

inline void rmlShadowMap::DrawVisibleItemsForViewport( const grcViewport *viewport, u32 flags ) {
	m_Proxy.DrawVisibleItemsForViewport(viewport, flags);
}
}	// namespace rage

#endif	// RMLIGHTING_LIGHTINGMGR_H
