// 
// rmlighting/lightingmgr.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "lightingmgr.h"

#include "lightsource.h"

#include "bank/bank.h"
#include "file/asset.h"
#include "grcore/state.h"
#include "grshadowmap/cascadedshadowmap.h"
#include "profile/profiler.h"	  
#include "system/param.h"

#include "lightingmgr_parser.h"

using namespace rage;

PARAM(enableshadowmap,"enables the shadowmap");
PARAM(enablecloudshadows,"enables cloud shadows (enableshadowmap must also be enabled!)");

namespace rmlLightingStats
{
	PF_PAGE(rmlLighting, "Rmworld Lighting");
	PF_GROUP(Draw);
	PF_LINK(rmlLighting,Draw);
	PF_TIMER(ShadowMapTotal,Draw);
	PF_TIMER(ShadowMapCull,Draw);
	PF_TIMER(ShadowMapDraw,Draw);
};

using namespace rmlLightingStats;

// The currently active lighting manager
rmlLightingMgr *rmlLightingMgr::sm_CurrentMgr = NULL;



rmlLightingMgr::rmlLightingMgr() : 
	m_ShadowMap(0), 
	m_CloudShadowMap(0),
	m_LightPool(0), 
	m_ActiveCount(0),
	m_AmbientColor(0.5f,0.5f,0.5f,1.0f)
#if __BANK
	,m_RenderShadowMaps(true)
#endif
{

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmlLightingMgr::~rmlLightingMgr() {
	Shutdown();

	// Unregister automatically if we had been the active manager
	if (sm_CurrentMgr == this)
	{
		sm_CurrentMgr = NULL;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::InitShaderSystem() {
	if ( m_ShadowMap ) {
		m_ShadowMap->InitShaders();
	}
	if ( m_CloudShadowMap ) {
		m_CloudShadowMap->InitShaders();
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::Init(rmlLightProxy& proxy, s32 maxLights, u32 maxBuckets, u32 alphaBucketMask, int shadowTiles) 
{
	InitLightSources(proxy, maxLights, maxBuckets, alphaBucketMask);
	InitShadowSystem(proxy, shadowTiles);
	InitClient();
	InitShaderSystem();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::Shutdown() {
	ShutdownLightSources();
	ShutdownShadowSystem();
	ShutdownClient();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rmlLightSource& rmlLightingMgr::CreateLightSource(bool castsShadow, bool castsLight) {
	AssertMsg(m_LightPool, "Need to init system first");
	// TODO: FIX LIGHT SOURCE BASED ON PARAMS
	rmlLightSource *light =  m_LightList.Append();
	AssertMsg(light , "No more lights avaliable! Specify more during rmlLightingMgr::Init");
	light->m_CastShadow = castsShadow;
	light->m_CastLight = castsLight;
	return *light;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::DestroyLightSource(rmlLightSource& source) {
	AssertMsg(m_LightPool,"Need to init system first");
	AssertMsg(source.GetLightId() < m_LightList.GetCount(),"Light source destroyed twice or null ptr");
	
	if ( source.GetLightId() < m_ActiveCount ) {
		DeactivateLightSource(source);
	}

	int lastId = m_LightList.GetCount() - 1;
	int srcId = source.GetLightId();
	if ( srcId < lastId ) {
		source.SetLightId(lastId);		
		m_LightList[lastId]->SetLightId(srcId);
		m_LightList[srcId] = m_LightList[lastId];
		m_LightList[lastId] = &source;
	}
	m_LightList.Pop();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::ActivateLightSource(rmlLightSource& source) {
	AssertMsg(m_LightPool,"Need to init system first");
	int srcId = source.GetLightId();
	if ( srcId < m_ActiveCount ) {
		return;
	}
	
	if ( srcId > m_ActiveCount ) {
		source.SetLightId( m_ActiveCount );
		m_LightList[m_ActiveCount]->SetLightId(srcId);
		m_LightList[srcId] = m_LightList[m_ActiveCount];
		m_LightList[m_ActiveCount] = &source;
	}
	if (source.DoesCastShadow())
	{
		if (m_ShadowMap)
		{
			m_ShadowMap->RegisterShadowCaster( source );
		}
		// TO DO: register shadow caster for cloudshadowmap?  shit doesn't seem to be used by m_ShadowMap!
		//m_CloudShadowMap
	}

	++m_ActiveCount;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::DeactivateLightSource(rmlLightSource& source) {
	AssertMsg(m_LightPool,"Need to init system first");
	int srcId = source.GetLightId();
	if ( srcId >= m_ActiveCount ) {
		return;
	}
	
	if(source.DoesCastShadow())
		m_ShadowMap->RemoveShadowCaster(source);

	if ( srcId < --m_ActiveCount ) {
		source.SetLightId(m_ActiveCount);
		m_LightList[m_ActiveCount]->SetLightId(srcId);
		
		m_LightList[srcId] = m_LightList[m_ActiveCount];
		m_LightList[m_ActiveCount] = &source;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::DeactivateAllLightSources() {
	AssertMsg(m_LightPool,"Need to init system first");
	while ( m_ActiveCount ) {
		if (m_LightList[m_ActiveCount-1])
			DeactivateLightSource(*m_LightList[m_ActiveCount-1]);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::Update() {
	AssertMsg(m_LightPool,"Need to init system first");
	/* EMPTY */
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void rmlLightingMgr::ActivateShadowLightGroup()
{

}

void rmlLightingMgr::ActivateLightGroup()
{
	grcLightState::SetEnabled(true);

	// HACK: Stuff the first 3 lights into the lighting group structure
	grcLightGroup group(m_ActiveCount);

	for (int i = 0; i < group.GetActiveCount(); ++i) {
		if ( m_LightList[i]->IsPoint() ) {
			group.SetLightType(i, grcLightGroup::LTTYPE_POINT);
			group.SetPosition( i, VECTOR3_TO_VEC3V(m_LightList[i]->GetPosition()) );
		}
		else {
			group.SetLightType(i, grcLightGroup::LTTYPE_DIR);
			group.SetDirection(i, VECTOR3_TO_VEC3V(m_LightList[i]->GetDirection()) );
		}
		group.SetIntensity(i,m_LightList[i]->GetIntensity());
		group.SetColor(i, m_LightList[i]->GetColor());
	}
	//Set the ambient color
	Vector3 ambColor(m_AmbientColor.x,m_AmbientColor.y,m_AmbientColor.z);
	ambColor.Scale(m_AmbientColor.w);
	group.SetAmbient( Vector4(ambColor) );

	grcState::SetLightingGroup(group);
	// -- END HACK
}

void rmlLightingMgr::DrawScene() {
	
	AssertMsg(m_LightPool,"Need to init system first");

	ActivateLightGroup();

	PF_START(ShadowMapTotal);
	if(m_ShadowMap
#if __BANK
		&& m_RenderShadowMaps
#endif
	)
	{
#if __BANK
		m_ShadowMap->CollectShaftInfo();
#endif
		ActivateShadowLightGroup();

		// render twice, once for each buffer:
		m_ShadowMap->RenderIntoShadowMaps();

		m_CloudShadowMap->RenderIntoShadowMap();

		ActivateLightGroup();
	}
	else
	{
		ActivateLightGroup();
	}

	PF_STOP(ShadowMapTotal);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::InitLightSources(rmlLightProxy& /*proxy*/, s32 lightCount, u32 /*maxBuckets*/, u32 /*alphaBucketMask*/) {
	m_LightPool = rage_new rmlLightSource[lightCount];
	m_LightList.Reserve(lightCount);

	for (s32 i = 0; i < lightCount; ++i) {
		m_LightPool[i].SetLightId(i);
		m_LightList.Append()= &m_LightPool[i];
	}
	m_LightList.Resize(0);
	m_ActiveCount = 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::ShadowPassGraphicsState()
{
	float depthBias = 0.0f;
	float scopeScaledDepthBias = -1.725f;

	if (GetShadowMapSystem())
	{
	//	depthBias = GetShadowMapSystem()->GetDepthBias();
	//	scopeScaledDepthBias = GetShadowMapSystem()->GetDepthSlopeBias();
	}

	grcState::SetDepthFunc(grcdfLessEqual);
	grcState::SetDepthTest(true);
	grcState::SetDepthWrite(true);

	grcState::SetState(grcsDepthBias, *(u32*)&depthBias);
	grcState::SetState(grcsSlopeScaleDepthBias, *(u32*)&scopeScaledDepthBias);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//void rmlLightingMgr::ShadowColorPassGraphicsState()
//{
//	grcState::SetDepthFunc(grcdfLessEqual);
//	grcState::SetDepthTest(true);
//	grcState::SetDepthWrite(true);
//
//}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if !__WIN32PC
#define NOT_WIN32PC(x)	x
#else
#define NOT_WIN32PC(x)
#endif
void rmlLightingMgr::InitShadowSystem(rmlLightProxy& NOT_WIN32PC(proxy), int NOT_WIN32PC(shadowTiles)) {
	// don't show shadows on PC:
#define ENABLE_SHADOW_SYSTEM	__XENON || __PS3

#if ENABLE_SHADOW_SYSTEM
	if (PARAM_enableshadowmap.Get())
	{
		m_ShadowMap = rage_new rmlShadowMap(proxy, shadowTiles);
		m_ShadowMap->CreateRenderTargets();
	}
	else
#endif
	{
		m_ShadowMap=NULL;
	}

#if ENABLE_SHADOW_SYSTEM
	if (PARAM_enablecloudshadows.Get())
	{
		m_CloudShadowMap = rage_new CloudShadowMap();
		m_CloudShadowMap->CreateRenderTargets();
	}
	else
#endif
	{
		m_CloudShadowMap=NULL;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::ShutdownLightSources() {
	delete[] m_LightPool;
	m_LightList.Reset();

	m_LightPool=0;
	m_ActiveCount = 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlLightingMgr::ShutdownShadowSystem() {
	if (m_ShadowMap)
	{
		m_ShadowMap->DeleteRenderTargets();
		delete m_ShadowMap;
		m_ShadowMap = NULL;
	}

	if (m_CloudShadowMap)
	{
		m_CloudShadowMap->DeleteRenderTargets();
		delete m_CloudShadowMap;
		m_CloudShadowMap = NULL;
	}
}

#if __BANK
void rmlLightingMgr::AddWidgets(bkBank& bank)
{
	if(m_ShadowMap)
	{
		m_ShadowMap->AddWidgets(bank);
		bank.AddToggle("Render Into ShadowMaps?",&m_RenderShadowMaps);
	}
	if (m_CloudShadowMap)
	{
		// TO DO: add cloud shadow widgets
	}
}
#endif  //__BANK

rmlShadowMap::rmlShadowMap(rmlLightProxy& proxy,int numTiles) : CShadowMap(numTiles),
	m_Proxy(proxy)
#if __BANK
	,m_ShaftIndex(0)
	,m_DrawShadowShafts(false)
#endif
{
	REGISTER_PARSABLE_CLASS(rmlShadowMap);
	ASSET.PushFolder("tune/settings");
	LoadTunables<rmlShadowMap>("shadowmap"); 
	ASSET.PopFolder();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlShadowMap::RegisterShadowCaster( rmlLightSource& source )
{
	if(source.GetCasterId()<m_ShadowCasters.GetCount())
		return;
	m_ShadowCasters.Append()=&source;
	source.SetCasterId(m_ShadowCasters.GetCount()-1);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rmlShadowMap::RemoveShadowCaster( rmlLightSource& source )
{
	if((source.GetCasterId()>=m_ShadowCasters.GetCount()) || (source.GetCasterId()<0) )
		return;

	if(source.GetCasterId()==m_ShadowCasters.GetCount()-1)
	{
		m_ShadowCasters.Pop();
		return;
	}
	int last = m_ShadowCasters.GetCount()-1;
	int sid = source.GetCasterId();

	m_ShadowCasters[sid] = m_ShadowCasters[last];
	m_ShadowCasters[sid]->SetCasterId(sid);
	m_ShadowCasters[last]->SetCasterId(-1);
	m_ShadowCasters.Pop();
}

void rmlShadowMap::RenderShadowCasters( grcViewport &vp, bool )
{
#if __BANK
	Assert(m_ShaftIndex<MAX_DEBUG_SHAFTS);
	m_Shafts[m_ShaftIndex++].Set(vp, RCC_MATRIX34(vp.GetCameraMtx()));
#endif

	PF_START(ShadowMapCull);
	m_Proxy.BuildVisibleListForViewport(&vp,rmlLightProxy::FLG_CASTER_LIGHT_PASS,0);
	PF_STOP(ShadowMapCull);

	PF_START(ShadowMapDraw);
	m_Proxy.DrawVisibleItemsForViewport(&vp,rmlLightProxy::FLG_CASTER_LIGHT_PASS);	
	PF_STOP(ShadowMapDraw);
}

#if __BANK
typedef void (CShadowMap::*CShadowMapMember0)();

#if __DEV
void rmlShadowMap::AddSaveWidgets(bkBank& bk)
{
	// gcc wants a double cast to remove all doubt as to how you want this to work ...
	bk.AddButton("Save Tunables...",datCallback((Member0)(CShadowMapMember0)&CShadowMap::SaveTunablesCB<CShadowMap>,this));
	bk.AddButton("Load Tunables...",datCallback((Member0)(CShadowMapMember0)&CShadowMap::LoadTunablesCB<CShadowMap>,this));
}
#endif

void rmlShadowMap::AddWidgets(bkBank& bank)
{
#if __DEV
	AddSaveWidgets(bank);
#endif
	CShadowMap::AddWidgets(bank);
	bank.AddToggle("Debug Draw Shafts",&m_DrawShadowShafts);
}

void rmlShadowMap::DebugDrawShafts() const
{
	if (m_DrawShadowShafts)
	{
		for (int i=0;i<m_ShaftIndex;i++)
		{
			m_Shafts[i].DrawDebug();
		}
	}
}
#endif

