// 
// rmlighting/lightsource.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "lightsource.h"

using namespace rage;

rmlLightSource::rmlLightSource() : m_Position(0.f,0.f,0.f), m_Direction(0.f,-1.f,0.f), m_Color(255,255,255), m_Intensity(1.0f), 
							m_LightType(TYPE_POINT), m_LightId(-1), m_CasterId(-1) {
	/* EMPTY */							
}

rmlLightSource::~rmlLightSource() {
}

void rmlLightSource::Update() {
	/* EMPTY */
}
