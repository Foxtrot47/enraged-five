// 
// rmlighting/lightsource.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMLIGHTING_LIGHTSOURCE_H
#define RMLIGHTING_LIGHTSOURCE_H

#include "grcore/light.h"
#include "grcore/texture.h"
#include "vector/color32.h"
#include "vector/vector3.h"
#include "vector/vector4.h"


namespace rage {

// PURPOSE: Provide a container class for the various light types supported by rmlighting
//	NOTES:	This will probably eventually require a light manager so that a variety of lighting
//			& shadowing systems can work from the same basic light objects.
class rmlLightSource {
	friend class rmlLightingMgr;
	friend class rmlShadowMap;

	// Require a light manager to allocate/deallocate
	rmlLightSource();
	virtual ~rmlLightSource();
public:

	struct HackVisualize
	{
		HackVisualize()
		{
			m_Draw=false;
			m_Radius=10.0f;
			m_Alpha=1.0f;
			m_GlowAlpha=1.5f;
		}
		bool			m_Draw;				// Flag wether to draw the light or not
		float			m_Radius;			// Radius of the texture
		float			m_Alpha;			// Alpha of the "light source" 
		float			m_GlowAlpha;		// Alpha of the glow halow
	};


	// PURPOSE: Set the position of the light source (converts to direction with directional lights)
	// PARAMS:	The position you want to use for the light source
	inline void SetPosition( const Vector3 &pos );

	// PURPOSE: Set the direction vector of the light source (NOP for non directional light sources)
	// PARAMS:  The direction of the light source
	inline void SetDirection( const Vector3 &dir );

	// PURPOSE: Set the type of the light source to be a point light
	// NOTES:	Does NOT adjust previously set point or directional values
	inline void SetPointType();

	// PURPOSE: Set the type of the light source to be a directional light
	// NOTES:	Does NOT adjust previously set point or directional values
	inline void SetDirectionalType();
	
	// PURPOSE: Set the color of the light source
	// PARAMS: c -- the color to set
	inline void SetLightColor( const Color32 &c );

	// PURPOSE: Set the color of the light source
	// PARAMS: c -- the color to set
	inline void SetLightColor( const Vector3 &c );

	// PURPOSE: Set the intensity of the light
	// PARAMS: i -- the intensity to set
	inline void SetIntensity( float i);
	
	// PURPOSE: Set the color of the shadow
	// PARAMS: c -- the color to set
	inline void SetShadowColor( const Color32 &c );

	// PURPOSE: Set the color of the shadow
	// PARAMS: c -- the color to set
	inline void SetShadowColor( const Vector4 &c );


	// RETURNS: The position of the light source (undefined for directional lights)
	inline const Vector3 &GetPosition() const;

	// RETURNS: The light direction of the source (undefined for point lights)
	inline const Vector3 &GetDirection() const;

	// RETURNS: The color of the light source
	inline const Color32 &GetColor() const;

	// RETURNS: The color of the light source
	inline void GetColor(Vector3 &outVect) const;

	// RETURNS: The intensity of the light source
	inline float GetIntensity() const;

	// RETURNS: The color of the shadow
	inline const Vector4 &GetShadowColor() const;

	// RETURNS: The color of the shadow
	//inline void GetShadowColor(Vector4 &outVect) const;

	// RETURNS: True if light is a point light
	inline bool IsPoint() const;

	// RETURNS: True if light is a directional light
	inline bool IsDirectional() const;

	// RETURNS: True if light casts a shadow
	inline bool DoesCastShadow() const;

	// RETURNS: True if light casts light
	inline bool	DoesCastLight() const;

	// RETURNS: True if light wants to be drawn
	inline bool GetVisualizeLight () const;

	// PURPOSE: Set if the light should be visulised
	// PARAMS: b -- true or false
	inline void SetVisualizeLight(bool b);

	//HACK
	HackVisualize   m_Visualize;		// Visualization info

protected:
	enum rmlLightType { TYPE_POINT = grcLightGroup::LTTYPE_POINT, 
		TYPE_DIR = grcLightGroup::LTTYPE_DIR };

	// For future expansion...  :)
	void Update();

	// PURPOSE: Retrieve the light id of this light source
	inline s32 GetLightId() const;

	// PURPOSE: Set the id of this light source
	inline void SetLightId(s32 id);

	// PURPOSE: Retrieve the shadow caster id of this light source
	inline s32 GetCasterId() const;

	// PURPOSE: Set the caster id of this light source
	inline void SetCasterId(s32 id);

private:

	Vector3			m_Position;			// Position 
	Vector3			m_Direction;		// Direction of light source
	Color32			m_Color;			// The light color
	float			m_Intensity;		// The light's intensity
	Vector4			m_ShadowColor;		// The shadow color
	rmlLightType	m_LightType;		// The type of light source to use
	s16				m_LightId;			// The index of this light source
	s16				m_CasterId;			// The index of this shadow caster
	bool			m_CastLight;		// If this source casts light
	bool			m_CastShadow;       // If this source casts shadow

};

inline bool rmlLightSource::DoesCastShadow() const {
	return m_CastShadow;
}
inline bool rmlLightSource::DoesCastLight() const {
	return m_CastLight;
}

inline bool rmlLightSource::IsPoint() const {
	return m_LightType == TYPE_POINT;
}

inline bool rmlLightSource::IsDirectional() const {
	return m_LightType == TYPE_DIR;
}

inline void rmlLightSource::SetPointType() {
	m_LightType = TYPE_POINT;
}

inline void rmlLightSource::SetDirectionalType() {
	m_LightType = TYPE_DIR;
}

inline void rmlLightSource::SetPosition( const Vector3 &pos ) {
	m_Position.Set(pos);
	/*if ( IsDirectional() ) {
		m_Direction.Scale(pos, -1.f);
		m_Direction.Normalize();
	}*/
}

inline void rmlLightSource::SetDirection( const Vector3 &dir ) {
	m_Direction.Set( dir );
}

inline void rmlLightSource::SetLightColor( const Color32 &c ) {
	m_Color = c;
}

inline void rmlLightSource::SetLightColor( const Vector3 &c ) {
	m_Color.Setf(c.x, c.y, c.z);
}

inline void rmlLightSource::SetIntensity( float i) {
	m_Intensity = i;
}

//inline void rmlLightSource::SetShadowColor( const Color32 &c ) {
//	m_ShadowColor = c;
//}

inline void rmlLightSource::SetShadowColor( const Vector4 &c ) {
	m_ShadowColor.Set(c.x, c.y, c.z, c.w);
}

inline const Vector3 &rmlLightSource::GetPosition() const {
	return m_Position;
}

inline const Vector3 &rmlLightSource::GetDirection() const {
	return m_Direction;
}

inline void rmlLightSource::GetColor(Vector3 &outVect) const {
	outVect.x = m_Color.GetRedf();
	outVect.y = m_Color.GetGreenf();
	outVect.z = m_Color.GetBluef();
}

inline const Color32 &rmlLightSource::GetColor() const {
	return m_Color;
}

inline float rmlLightSource::GetIntensity() const {
	return m_Intensity;
}
/*inline void rmlLightSource::GetShadowColor(Vector4 &outVect) const {
	outVect.x = m_ShadowColor.GetRedf();
	outVect.y = m_ShadowColor.GetGreenf();
	outVect.z = m_ShadowColor.GetBluef();
	outVect.w = m_ShadowColor.GetAlphaf();
}*/

/*inline const Color32 &rmlLightSource::GetShadowColor() const {
	return m_ShadowColor;
}
*/
inline const Vector4& rmlLightSource::GetShadowColor() const {
	return m_ShadowColor;
}

inline s32 rmlLightSource::GetLightId() const {
	return m_LightId;
}

inline void rmlLightSource::SetLightId( s32 id ) {
	m_LightId = (u16) id;
	AssertMsg( m_LightId == id, "Not enough storage for id" );
}

inline s32 rmlLightSource::GetCasterId() const {
	return m_CasterId;
}

inline void rmlLightSource::SetCasterId( s32 id ) {
	m_CasterId = (u16) id;
	AssertMsg( m_CasterId == id , "Not enough storage for id" );
}

inline bool rmlLightSource::GetVisualizeLight() const
{
	return m_Visualize.m_Draw;
}

inline void rmlLightSource::SetVisualizeLight(bool b)
{
	m_Visualize.m_Draw = b;
}


}	// namespace rage

#endif	// RMLIGHTING_SHADOWMAP_H
