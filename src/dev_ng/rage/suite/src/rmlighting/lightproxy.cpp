// 
// rmlighting/lightproxy.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "lightproxy.h"

using namespace rage;

rmlLightProxy::rmlLightProxy() {
	/* EMPTY */
}

rmlLightProxy::~rmlLightProxy() {

}

void rmlLightProxy::Init(int /*bucketCount*/, int maxVisibleInstances) {
	m_VisibleInstances.Reserve(maxVisibleInstances);
}

