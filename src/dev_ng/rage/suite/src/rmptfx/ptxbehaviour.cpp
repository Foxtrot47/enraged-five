// 
// rmptfx/ptxbehaviour.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxbehaviour.h"
#include "rmptfx/ptxbehaviour_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxdebug.h"

// includes (rage)
#include "parser/visitorutils.h"
#include "system/timer.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


// static variables
#if RMPTFX_EDITOR
int ptxBehaviour::sm_numDrawTypes = 0;
atArray<const char*> ptxBehaviour::sm_drawTypeNames;
#endif


// code
#if RMPTFX_XML_LOADING
ptxBehaviour::ptxBehaviour()
: m_accumCPUUpdateTicks(0)
, m_hashName(0)
{
	m_keyframePropList.Reserve(parCountInstances<ptxKeyframeProp>(*this));
}
#endif

ptxBehaviour::ptxBehaviour(datResource& rsc)
	: m_keyframePropList(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxBehaviour_num++;
#endif
}

#if __DECLARESTRUCT
void ptxBehaviour::DeclareStruct(datTypeStruct& s)
{
	ptxAssertf(m_hashName, "invalid hash name");

	SSTRUCT_BEGIN(ptxBehaviour)
		SSTRUCT_FIELD(ptxBehaviour, m_hashName)
		SSTRUCT_FIELD(ptxBehaviour, m_keyframePropList)
		SSTRUCT_IGNORE(ptxBehaviour, m_accumCPUUpdateTicks)
		SSTRUCT_IGNORE(ptxBehaviour, m_pad)
	SSTRUCT_END(ptxBehaviour)
}
#endif

void ptxBehaviour::PlaceSubtype(ptxBehaviour* pBehaviour, datResource& rsc)
{
	RMPTFXMGR.FindBehaviourPlace(pBehaviour->m_hashName)(pBehaviour, rsc);
}

void ptxBehaviour::InitClass()
{
#if RMPTFX_EDITOR
	AddDrawType("sprite");
	AddDrawType("mesh");
	AddDrawType("trail");
#endif
}

void ptxBehaviour::CalcHashName()
{
	m_hashName = atHashString(GetName());
}

void ptxBehaviour::PrepareEvoData(ptxEvolutionList* pEvoList)
{
	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		m_keyframePropList.GetKeyframePropByIndex(i)->PrepareEvoData(pEvoList);
	}
}

void ptxBehaviour::SetBiasLinks(atArray<ptxBiasLink>& biasLinks)
{
	m_keyframePropList.SetBiasLinks(biasLinks);
}

bool ptxBehaviour::HasProp(u32 propHashId)
{
	return m_keyframePropList.GetKeyframePropByPropId(propHashId) ? true : false;
}

// editor code
#if RMPTFX_EDITOR
void ptxBehaviour::AddDrawType(const char* pName)
{
	sm_drawTypeNames.PushAndGrow(pName);
	sm_numDrawTypes = sm_drawTypeNames.size();
}

ptxDrawType ptxBehaviour::GetDrawType(const char* pName)
{
	for (int i=0; i<sm_drawTypeNames.GetCount(); i++)
	{
		if (strcmp(pName, sm_drawTypeNames[i])==0)
		{
			return (ptxDrawType)i;
		}
	}

	ptxAssertf(0, "draw type not found in list (%s)", pName);
	return (ptxDrawType)0;
}

void ptxBehaviour::SendDefnToEditor(ptxByteBuff& buff)
{
	buff.Write_const_char(GetEditorName());
	buff.Write_const_char(GetName());

	if (GetTypeFilter()==PTXPARTICLERULE_DRAWTYPE_SPRITE)
	{
		buff.Write_const_char("sprite");
	}
	else if (GetTypeFilter()==PTXPARTICLERULE_DRAWTYPE_MODEL)
	{
		buff.Write_const_char("mesh");
	}
	else if (GetTypeFilter()==PTXPARTICLERULE_DRAWTYPE_TRAIL)
	{
		buff.Write_const_char("trail");
	}
	else
	{
		buff.Write_const_char("all");
	}

	buff.Write_bool(GetHasCustomUI());

	if (GetHasCustomUI())
	{
		buff.Write_const_char(GetCustomUIName());
	}
}

void ptxBehaviour::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* UNUSED_PARAM(pParticleRule))
{
	buff.Write_const_char(GetEditorName());
	buff.Write_const_char(GetName());
}

void ptxBehaviour::SendNumDefnsToEditor(ptxByteBuff& buff, u8 val)
{
	buff.Write_u8(val);
}

void ptxBehaviour::SendKeyframeDefnToEditor(ptxByteBuff& buff, const char* pName)
{
	buff.Write_const_char("keyframe");
	buff.Write_const_char(pName);
}

void ptxBehaviour::SendIntDefnToEditor(ptxByteBuff& buff, const char* pName, int min, int max)
{
	buff.Write_const_char("int");
	buff.Write_const_char(pName);
	buff.Write_s32(min);
	buff.Write_s32(max);
}

void ptxBehaviour::SendFloatDefnToEditor(ptxByteBuff& buff, const char* pName, float min, float max)
{
	buff.Write_const_char("float");
	buff.Write_const_char(pName);
	buff.Write_float(min);
	buff.Write_float(max);
}

void ptxBehaviour::SendBoolDefnToEditor(ptxByteBuff& buff, const char* pName)
{
	buff.Write_const_char("bool");
	buff.Write_const_char(pName);
}

void ptxBehaviour::SendComboDefnToEditor(ptxByteBuff& buff, const char* pName, int numItems, const char* pItemNames[])
{
	buff.Write_const_char("combo");
	buff.Write_const_char(pName);
	buff.Write_s32(numItems);
	for (int i=0; i<numItems; i++)
	{
		buff.Write_const_char(pItemNames[i]);
	}
}

void ptxBehaviour::SendKeyframeToEditor(ptxByteBuff& buff, ptxKeyframeProp& keyframeProp, const ptxParticleRule* pParticleRule)
{
	keyframeProp.SendToEditor(buff, pParticleRule->GetName(), PTXRULETYPE_PARTICLE);
}

void ptxBehaviour::SendIntToEditor(ptxByteBuff& buff, int val)
{
	buff.Write_s32(val);
}

void ptxBehaviour::SendFloatToEditor(ptxByteBuff& buff, float val)
{
	buff.Write_float(val);
}

void ptxBehaviour::SendBoolToEditor(ptxByteBuff& buff, bool val)
{
	buff.Write_bool(val);
}

void ptxBehaviour::SendVecToEditor(ptxByteBuff& buff, const Vec3V& vVal)
{
	buff.Write_float(vVal.GetXf()); buff.Write_float(vVal.GetYf()); buff.Write_float(vVal.GetZf());
}

void ptxBehaviour::SendComboItemToEditor(ptxByteBuff& buff, int val)
{
	buff.Write_s32(val);
}

void ptxBehaviour::ReceiveKeyframeFromEditor(const ptxByteBuff& buff, ptxKeyframeProp& keyframeProp)
{
	keyframeProp.ReceiveFromEditor(buff);
}

void ptxBehaviour::ReceiveIntFromEditor(const ptxByteBuff& buff, int& val)
{
	val = buff.Read_s32();
}

void ptxBehaviour::ReceiveFloatFromEditor(const ptxByteBuff& buff, float& val)
{
	val = buff.Read_float();
}

void ptxBehaviour::ReceiveBoolFromEditor(const ptxByteBuff& buff, bool& val)
{
	val = buff.Read_bool();
}

void ptxBehaviour::ReceiveVecFromEditor(const ptxByteBuff& buff, Vec3V& vVal)
{
	vVal.SetXf(buff.Read_float()); vVal.SetYf(buff.Read_float()); vVal.SetZf(buff.Read_float());
}

void ptxBehaviour::ReceiveComboItemFromEditor(const ptxByteBuff& buff, int& val)
{
	val = buff.Read_s32();
}

#endif

} // namespace rage