// 
// rmptfx/ptxfxlist.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXFXLIST_H 
#define RMPTFX_PTXFXLIST_H 


// includes
#include "parser/visitor.h"
#include "rmcore/drawable.h"
#include "rmptfx/ptxeffectrule.h"
#include "rmptfx/ptxemitterrule.h"
#include "rmptfx/ptxparticlerule.h"


// namespaces
namespace rage 
{


// forward declarations
class parMemberStruct;


// enumerations
enum ptxFxListFlags
{
	FXLISTFLAG_SHAREABLE				= BIT(0),
	FXLISTFLAG_NONSHAREABLE				= BIT(1),
	FXLISTFLAG_RESOURCED				= BIT(2),
	FXLISTFLAG_NONRESOURCED				= BIT(3),
	FXLISTFLAG_LOOSE					= BIT(4),
	FXLISTFLAG_NONLOOSE					= BIT(5),
	FXLISTFLAG_UNDERCONSTRUCTION		= BIT(6),
	FXLISTFLAG_NOTUNDERCONSTRUCTION		= BIT(7),
	FXLISTFLAG_ALL						= ~0u
};


// typedefs
typedef pgDictionary<ptxEffectRule>		ptxEffectRuleDictionary;
typedef pgDictionary<ptxEmitterRule>	ptxEmitterRuleDictionary;
typedef pgDictionary<ptxParticleRule>	ptxParticleRuleDictionary;
typedef pgDictionary<grcTexture>		ptxTextureDictionary;
typedef pgDictionary<rmcDrawable>		ptxModelDictionary;
typedef pgDictionaryPusher<grcTexture>	ptxTextureDictionaryPusher;
typedef pgDictionaryPopper<grcTexture>	ptxTextureDictionaryPopper;

class ptxTokenIterator
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxTokenIterator(const char* pFilename, const char* pExtn);
		~ptxTokenIterator();

		const char* GetLineBuffer() {return m_lineBuffer;}
		bool GetNext();

		
	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////
	
		char m_lineBuffer[256];
		fiStream* m_pStream;
		fiTokenizer* m_pToken;

};

class ptxFxList: public pgBase
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		DECLARE_PLACE(ptxFxList);

		ptxFxList();
		virtual ~ptxFxList();

		explicit ptxFxList(datResource& rsc);
	#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
	#endif

		typedef atMap<void*, ConstString> PtrToFilenameMap;

#if RMPTFX_XML_LOADING
		void Load(const char* pBaseName, const char* pModelPath, const char* pTexturePath);
#endif
		void PostLoad(u32 fxListFlags);

		static ptxFxList* CreateEmptyFxlist();

		const char* GetName() const {return m_name.c_str();}
		ptxEffectRuleDictionary* GetEffectRuleDictionary() const {return m_pEffectRuleDict;}
		ptxEmitterRuleDictionary* GetEmitterRuleDictionary() const {return m_pEmitterRuleDict;}
		ptxParticleRuleDictionary* GetParticleRuleDictionary() const {return m_pParticleRuleDict;}
		ptxTextureDictionary* GetTextureDictionary() const {return m_pTextureDict;}
		ptxModelDictionary* GetModelDictionary() const {return m_pModelDict;}

#if __DEV
		void PrintReferences(const char* names);
#endif

		bool AreAllShadersInSync();

		// RETURNS: The number of external references this fxlist is aware of
		// (counting references to subobjects)
		int FindExternalReferenceCount();

		// This is all the same as pgBaseRefCounted, with one important difference... bringing the ref count
		// to 0 does NOT delete the fxlist. This is because we expect that whoever loaded the fxlist into memory
		// will get final day on when it does or doesn't get deleted.
		// (Yes, that object could maintain a reference to the fxlist - doesn't work that easily at the moment)
		void AddRef() const {sysInterlockedIncrement(&m_refCount);}
		void DecRef() const {sysInterlockedDecrement(&m_refCount);}
		int Release() const {ptxAssert(m_refCount);	return (int)sysInterlockedDecrement(&m_refCount);}
		int GetRefCount() const {return (int) m_refCount;}


	private: //////////////////////////

#if RMPTFX_XML_LOADING
		void LoadEffectRules(const char* pEffectRuleListName, PtrToFilenameMap& fileNameMap);
		void LoadEmitterRules(const char* pEmitterRuleListName, PtrToFilenameMap& fileNameMap);
		void LoadParticleRules(const char* pParticleRuleListName, PtrToFilenameMap& fileNameMap);
		void LoadModels(const char* pModelListName, const char* pModelPath);
		void LoadTextures(const char* pTextureListName, const char* pTexturePath);
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	public: ///////////////////////////

		static const int RORC_VERSION = PTXEFFECTRULE_RORC_VERSION + PTXEMITTERRULE_RORC_VERSION + PTXPARTICLERULE_RORC_VERSION + 7;


	private: //////////////////////////

		ConstString m_name;
		mutable unsigned m_refCount;

		// NOTE: be careful if changing the order of these variables! Resource constructors run in order from top to bottom
		// and we need some c'tors to run before others. In particular we need the textures to be placed before we place
		// the models and the particle rules (which may need to resolve references to the textures in the texture dict)
		// See paging/dictionary.h to see what the pusher and popper objects do.
		datOwner<ptxTextureDictionary>		m_pTextureDict;

		ptxTextureDictionaryPusher			m_textureDictPusher;
		datOwner<ptxModelDictionary>		m_pModelDict;
		datOwner<ptxParticleRuleDictionary>	m_pParticleRuleDict;
		ptxTextureDictionaryPopper			m_textureDictPopper;

		datOwner<ptxEffectRuleDictionary>	m_pEffectRuleDict;
		datOwner<ptxEmitterRuleDictionary>	m_pEmitterRuleDict;
};


} //namespace rage


#endif // RMPTFX_PTXFXLIST_H 
