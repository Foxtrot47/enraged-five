// 
// rmptfx/ptxtcpcomm.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXTCPCOMM_H
#define RMPTFX_PTXTCPCOMM_H


// includes (rmptfx)
#include "ptxconfig.h"
#include "ptxByteBuff.h"

// includes (rage)
#include "file/tcpip.h"
#include "system/ipc.h"


// namespaces
namespace rage {


// classes
#if RMPTFX_EDITOR
class ptxTcpComm
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxTcpComm();
		virtual ~ptxTcpComm();

		void StartListening();
		void Update();
		virtual void Disconnect();
		bool SendByteBuffer(ptxByteBuff* pByteBuff);

		// accessors
		bool IsConnected() {return m_isConnected;}


	protected: ////////////////////////

		virtual void Connected();											// called from the ListenerThread


	private: //////////////////////////

		static DECLARE_THREAD_FUNC(ListenerThread);
		void ListenLoop();
		virtual void LostConnection();
		virtual void ProcessByteBuffer(ptxByteBuff& byteBuff);
		void BeginNewByteBuffer();
		void ContinueByteBuffer();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////
		
		bool m_isConnected;													// whether we have established a connection
		bool m_isListening;													// whether we are listening for a connection

		fiHandle m_listenSocket;											// the listening socket
		fiHandle m_tcpSocket;												// the tcp socket

		int m_pingCount;													// number of frames that a ping has been unsuccessful

		int m_numBytesToRead;
		ptxByteBuff m_inByteBuffer;											// the incoming communication buffer
		ptxByteBuff m_pingBuffer;											// the ping buffer
	
};
#endif // RMPTFX_EDITOR


} // namespace rage


#endif // RMPTFX_PTXTCPCOMM_H
