// 
// rmptfx/ptxd_drawsprite.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "ptxd_drawsprite.h"
#include "ptxd_drawsprite_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxparticlerule.h"

#if __OPENGL || __D3D
#include "grmodel/shaderfx.h"
#endif

#include "system/timer.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


// code
#if RMPTFX_XML_LOADING
ptxd_Sprite::ptxd_Sprite()
{
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxd_Sprite::SetDefaultData()
{
	// default keyframes

	// default other data
	m_alignAxis = Vec3V(V_Z_AXIS_WZERO);

	m_alignmentMode = PTXD_SPRITE_ALIGNMENTMODE_CAMERA;

	m_flipChanceU = 0.0f;
	m_flipChanceV = 0.0f;

	m_nearClipDist = 0.0f;
	m_farClipDist = 0.0f;

	m_projectionDepth = 0.5f;
	m_shadowCastIntensity = 0.0f;

	m_isScreenSpace = false;
	m_isHiRes = true;
	m_nearClip = false;
	m_farClip = false;
	m_uvClip = false;
	m_disableDraw = false;
}

ptxd_Sprite::ptxd_Sprite(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_alignAxis(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxd_Sprite_num++;
#endif
}

#if __DECLARESTRUCT
void ptxd_Sprite::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxd_Sprite, ptxBehaviour)
		SSTRUCT_FIELD(ptxd_Sprite, m_alignAxis)
		SSTRUCT_FIELD(ptxd_Sprite, m_alignmentMode)
		SSTRUCT_FIELD(ptxd_Sprite, m_flipChanceU)
		SSTRUCT_FIELD(ptxd_Sprite, m_flipChanceV)
		SSTRUCT_FIELD(ptxd_Sprite, m_nearClipDist)
		SSTRUCT_FIELD(ptxd_Sprite, m_farClipDist)
		SSTRUCT_FIELD(ptxd_Sprite, m_projectionDepth)
		SSTRUCT_FIELD(ptxd_Sprite, m_shadowCastIntensity)
		SSTRUCT_FIELD(ptxd_Sprite, m_isScreenSpace)
		SSTRUCT_FIELD(ptxd_Sprite, m_isHiRes)
		SSTRUCT_FIELD(ptxd_Sprite, m_nearClip)
		SSTRUCT_FIELD(ptxd_Sprite, m_farClip)
		SSTRUCT_FIELD(ptxd_Sprite, m_uvClip)
		SSTRUCT_FIELD(ptxd_Sprite, m_disableDraw)
		SSTRUCT_IGNORE(ptxd_Sprite, m_pad)
	SSTRUCT_END(ptxd_Sprite)
}
#endif

IMPLEMENT_PLACE(ptxd_Sprite);

void ptxd_Sprite::DrawPoints(ptxEffectInst* RESTRICT pEffectInst,
							 ptxEmitterInst* RESTRICT pEmitterInst,
							 ptxParticleRule* RESTRICT pParticleRule,
							 ptxEvolutionList* RESTRICT pEvoList
							 PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface))
{
	ptxAssertf(pEffectInst, "missing effect instance");
	ptxAssertf(pEmitterInst, "missing emitter instance");
	ptxAssertf(pParticleRule->GetPointPool() , "missing point pool");

	// MN - why are we passing through a particle rule when it can be got from the emitter inst
	//    - if this assert never fires then they're the same and we can stop passing it through
	ptxAssertf(pParticleRule == pEmitterInst->GetParticleRule(), "particle rule mismatch");

	ptxAssertf(pEmitterInst->GetEmitterRule(), "can't get emitter rule from emitter inst");
	ptxAssertf(pEmitterInst->GetEmitterRule()->GetCreationDomainObj().GetDomain(), "can't get emitter creation domain from emitter inst");

#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif

	if(m_disableDraw)
	{
		return;
	}

#if RMPTFX_USE_PARTICLE_SHADOWS
	//If shadow cast intensity is really low, lets skip it
	if(RMPTFXMGR.IsShadowPass() && (m_shadowCastIntensity * RMPTFXMGR.GetGlobalShadowIntensity()) < RMPTFX_MIN_PARTICLE_SHADOW_INTENSITY)
	{
		return;
	}
#endif

	// return if there are no points to draw
	const unsigned drawBufferId = RMPTFXTHREADER.GetDrawBufferId();
	if (pEmitterInst->GetPointList().GetNumItemsMT(drawBufferId)==0)
	{
		return;
	}

	// sort the points
	unsigned numPoints = pEmitterInst->GetNumActivePointsMT(drawBufferId);
	ptxPointItem** ppPoints = Alloca(ptxPointItem*, numPoints);
	const unsigned numPoints2 = pEmitterInst->SortPoints(ppPoints);
	Assert(numPoints >= numPoints2);
	numPoints = numPoints2;

	// set the new render state
	grcRasterizerStateHandle RS_Prev = grcStateBlock::RS_Active;
	grcBlendStateHandle BS_Prev = grcStateBlock::BS_Active;
	grcDepthStencilStateHandle DSS_Prev = grcStateBlock::DSS_Active;
	pParticleRule->SetRenderState(pParticleRule->GetShaderInst().GetProjectionMode());
	grcViewport::SetCurrentWorldIdentity();
	

	// set up the shader variables
	pParticleRule->GetShaderInst().SetShaderVars(pParticleRule->GetBlendSet());

	// call pre draw in all cases - this will not do any redundant work (i.e. lights in area logic, etc.) 
	// for entities flagged as PTXEFFECTFLAG_MANUAL_DRAW, and will  ensure that sorted 
	// entities also get the correct ambient globals
	if (pEffectInst->GetRenderSetup()) 
	{ 
		Vec3V vBBMin = pEmitterInst->GetDataDBMT(drawBufferId)->GetCurrBoundBoxMin();
		Vec3V vBBMax = pEmitterInst->GetDataDBMT(drawBufferId)->GetCurrBoundBoxMax();

		if (ptxVerifyf(IsLessThanOrEqualAll(vBBMin, vBBMax), "%s - %s - min bound box is greater than max (min:%.3f, %.3f, %.3f max:%.3f, %.3f, %.3f", pEffectInst->GetEffectRule()->GetName(), pEmitterInst->GetEmitterRule()->GetName(), vBBMin.GetXf(), vBBMin.GetYf(), vBBMin.GetZf(), vBBMax.GetXf(), vBBMax.GetYf(), vBBMax.GetZf()))
		{
			if (!pEffectInst->GetRenderSetup()->PreDraw(pEffectInst, pEmitterInst, vBBMin, vBBMax, pParticleRule->GetShaderInst().GetShaderTemplateHashName(), pParticleRule->GetShaderInst().GetGrmShader()))
			{
				ptxWarningf("Error in Particle Setup for %s , aborting rendering particles", pParticleRule->GetShaderInst().GetShaderTemplateName());
				return;
			}
		}
	}

	// draw the particles
	ptxDrawInterface::DrawSprites(pEffectInst, pEmitterInst, ppPoints, numPoints, this, pParticleRule, pEvoList, m_alignmentMode!=PTXD_SPRITE_ALIGNMENTMODE_CAMERA, m_isHiRes, pParticleRule->GetShaderInst().GetProjectionMode() != PTXPROJMODE_NONE PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));

	grcStateBlock::SetRasterizerState(RS_Prev);
	grcStateBlock::SetBlendState(BS_Prev);
	grcStateBlock::SetDepthStencilState(DSS_Prev);
	// do any post draw only if the fx instance is not sorted - i.e. no PTXEFFECTFLAG_MANUAL_DRAW flag,
	// as in that case pre draw setup has already been done during the regular render list building.
	if (!pEffectInst->GetFlag(PTXEFFECTFLAG_MANUAL_DRAW) && pEffectInst->GetRenderSetup())
	{
		pEffectInst->GetRenderSetup()->PostDraw(pEffectInst, pEmitterInst);
	}

	// clear the shader variables
	pParticleRule->GetShaderInst().ClearShaderVars();

	// update the editor cpu draw time
#if RMPTFX_EDITOR
	pEmitterInst->GetEmitterRule()->GetUIData().IncAccumCPUDrawTime(timer.GetMsTime()); 
#endif
}


// editor code
#if RMPTFX_EDITOR
void ptxd_Sprite::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 16);

	//option For Disable
	SendBoolDefnToEditor(buff, "Disable Draw");

	// send keyframes

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Align Axis X", -1.0f, 1.0f);
	SendFloatDefnToEditor(buff, "Align Axis Y", -1.0f, 1.0f);
	SendFloatDefnToEditor(buff, "Align Axis Z", -1.0f, 1.0f);

	const char* pComboItemNames[PTXD_SPRITE_ALIGNMENTMODE_NUM] = {"Camera", "Velocity", "World", "Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Alignment Mode", PTXD_SPRITE_ALIGNMENTMODE_NUM, pComboItemNames);

	SendFloatDefnToEditor(buff, "Flip Chance U", 0.0f, 1.0f);
	SendFloatDefnToEditor(buff, "Flip Chance V", 0.0f, 1.0f);

	SendFloatDefnToEditor(buff, "Near Clip Dist", -100.0f, 100.0f);
	SendFloatDefnToEditor(buff, "Far Clip Dist", -100.0f, 100.0f);

	SendFloatDefnToEditor(buff, "Projection Depth", 0.0f, 5.0f);
	SendFloatDefnToEditor(buff, "Shadow Cast Intensity", 0.0f, 1.0f);

	SendBoolDefnToEditor(buff, "Screen Space");
	SendBoolDefnToEditor(buff, "Hi Res");
	SendBoolDefnToEditor(buff, "Near Clip");
	SendBoolDefnToEditor(buff, "Far Clip");
	SendBoolDefnToEditor(buff, "UV Clip");
}

void ptxd_Sprite::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	//option For Disable
	SendBoolToEditor(buff, m_disableDraw);
	// send keyframes

	// send non-keyframes
	SendVecToEditor(buff, m_alignAxis);

	SendComboItemToEditor(buff, m_alignmentMode);

	SendFloatToEditor(buff, m_flipChanceU);
	SendFloatToEditor(buff, m_flipChanceV);

	SendFloatToEditor(buff, m_nearClipDist);
	SendFloatToEditor(buff, m_farClipDist);

	SendFloatToEditor(buff, m_projectionDepth);
	SendFloatToEditor(buff, m_shadowCastIntensity);

	SendBoolToEditor(buff, m_isScreenSpace);
	SendBoolToEditor(buff, m_isHiRes);
	SendBoolToEditor(buff, m_nearClip);
	SendBoolToEditor(buff, m_farClip);
	SendBoolToEditor(buff, m_uvClip);
}

void ptxd_Sprite::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	//option For Disable
	ReceiveBoolFromEditor(buff, m_disableDraw);

	// receive keyframes

	// receive non-keyframes
	ReceiveVecFromEditor(buff, m_alignAxis);

	ReceiveComboItemFromEditor(buff, m_alignmentMode);

	ReceiveFloatFromEditor(buff, m_flipChanceU);
	ReceiveFloatFromEditor(buff, m_flipChanceV);

	ReceiveFloatFromEditor(buff, m_nearClipDist);
	ReceiveFloatFromEditor(buff, m_farClipDist);

	ReceiveFloatFromEditor(buff, m_projectionDepth);
	ReceiveFloatFromEditor(buff, m_shadowCastIntensity);

	ReceiveBoolFromEditor(buff, m_isScreenSpace);
	ReceiveBoolFromEditor(buff, m_isHiRes);
	ReceiveBoolFromEditor(buff, m_nearClip);
	ReceiveBoolFromEditor(buff, m_farClip);
	ReceiveBoolFromEditor(buff, m_uvClip);
}
#endif //_RMPTFX_INTERFACE


} // namespace rage
