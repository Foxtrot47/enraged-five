// 
// rmptfx/ptxu_collision.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "rmptfx/ptxu_collision.h"
#include "rmptfx/ptxu_collision_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"

#include "math/vecmath.h"

#if RMPTFX_BANK
#include "grcore/debugdraw.h"
#endif


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{

//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Collision::sm_bouncinessDefn(				"Bounce Factor",			atHashValue("ptxu_Collision:m_bouncinessKFP"),				PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		100.0f,		0.01f),		"Percent Min",		"Percent Max");
ptxKeyframeDefn ptxu_Collision::sm_bounceDirVarDefn(			"Bounce Direction Variance",atHashValue("ptxu_Collision:m_bounceDirVarKFP"),			PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Rotation Min",		"Rotation Max",		"Elevation Min",	"Elevation Max");


// code
#if RMPTFX_XML_LOADING
ptxu_Collision::ptxu_Collision()
{
	// create keyframes
	m_bouncinessKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Collision:m_bouncinessKFP"));
	m_bounceDirVarKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Collision:m_bounceDirVarKFP"));

	m_keyframePropList.AddKeyframeProp(&m_bouncinessKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_bounceDirVarKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Collision::SetDefaultData()
{
	// default keyframes
	m_bouncinessKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_bounceDirVarKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_radiusMult = 1.0f;
	m_restSpeed = 0.0f;
	m_colnChance = 100;
	m_killChance = 0;

	m_debugDraw = false;

	m_overrideMinRadius = 0.0f;
}

ptxu_Collision::ptxu_Collision(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_bouncinessKFP(rsc)
	, m_bounceDirVarKFP(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Collision_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Collision::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Collision, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Collision, m_bouncinessKFP)
		SSTRUCT_FIELD(ptxu_Collision, m_bounceDirVarKFP)
		SSTRUCT_FIELD(ptxu_Collision, m_radiusMult)
		SSTRUCT_FIELD(ptxu_Collision, m_restSpeed)
		SSTRUCT_FIELD(ptxu_Collision, m_colnChance)
		SSTRUCT_FIELD(ptxu_Collision, m_killChance)
		SSTRUCT_FIELD(ptxu_Collision, m_debugDraw)
		SSTRUCT_IGNORE(ptxu_Collision, m_pad)
		SSTRUCT_FIELD(ptxu_Collision, m_overrideMinRadius)
		SSTRUCT_IGNORE(ptxu_Collision, m_pad2)
	SSTRUCT_END(ptxu_Collision)
}
#endif

IMPLEMENT_PLACE(ptxu_Collision);

void ptxu_Collision::InitPoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();

	// decide if this particle collides or not
	if (g_DrawRand.GetRanged(0, 100) <= m_colnChance)
	{
		pPointSB->SetFlag(PTXPOINT_FLAG_COLLIDES);
	}

#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointColn)
#endif
	{
		UpdatePoint(params);
	}
}

void ApplyBounceVariance(Vec3V_InOut vR, Vec3V_In vColnNormal, ScalarV_In vBounceDirRotationVar, ScalarV_In vBounceDirElevationVar)
{
	// get the sphere radius
	ScalarV vSphereRadius = Mag(vR);
	vR = Normalize(vR);

	// create matrix to rotate from cartesian to normal space
	Mat34V rotMtx = Mat34V(V_IDENTITY);
	const Vec3V vRand = Vec3VConstant<FLOAT_TO_INT(-0.353f), FLOAT_TO_INT(0.712f), FLOAT_TO_INT(0.607f)>();
	Vec3V col2 = vColnNormal;
	Vec3V col1 = Cross(col2, vRand);
	col1 = Normalize(col1);
	Vec3V col0 = Cross(col1, col2);
	col0 = Normalize(col0);
	rotMtx.SetCol0(col0);
	rotMtx.SetCol1(col1);
	rotMtx.SetCol2(col2);

	// invert matrix to go from normal to cartesian space
	Mat34V rotMtxInv;
	Transpose3x3(rotMtxInv, rotMtx);

	// rotate the input vector into cartesian space
	vR = Transform(rotMtxInv, vR);
	vR = Normalize(vR);

	// calculate spherical coords from cartesian coords
	ScalarV vThetaCosAngle = SplatZ(vR);
	ScalarV vPhiSinAngle = SplatY(vR);
	ScalarV vPhiCosAngle = SplatX(vR);

	ScalarV vSafeThetaCosAngle	= Clamp(vThetaCosAngle, ScalarV(V_NEGONE), ScalarV(V_ONE));
	ScalarV vSafePhiSinAngle	= Clamp(vPhiSinAngle, ScalarV(V_NEGONE), ScalarV(V_ONE));
	ScalarV vSafePhiCosAngle	= Clamp(vPhiCosAngle, ScalarV(V_NEGONE), ScalarV(V_ONE));

	ScalarV vTheta = Arccos(vSafeThetaCosAngle);
	ScalarV vPhi = Arctan2(vSafePhiSinAngle, vSafePhiCosAngle);

	// calc new phi and theta (rotation and elevation)
	static const ScalarV MIN_ROTATION = -ScalarV(V_PI);
	static const ScalarV MAX_ROTATION = ScalarV(V_PI);				
	ScalarV vPhiRand = g_ptxVecRandom.GetRangedScalarV(MIN_ROTATION, MAX_ROTATION);
	ScalarV vPhiNew = vPhi + ((vPhiRand-vPhi)*vBounceDirRotationVar);

	static const ScalarV MIN_ELEVATION = ScalarV(V_ZERO);											// in the direction of the normal
	static const ScalarV MAX_ELEVATION = ScalarV(V_PI_OVER_TWO) - ScalarV(V_FLT_SMALL_2);			// in the direction of the ground (subtract a little so it doesn't go straight along the ground and drop through)
	ScalarV vThetaRand = g_ptxVecRandom.GetRangedScalarV(MIN_ELEVATION, MAX_ELEVATION);
	ScalarV vThetaNew = vTheta + ((vThetaRand-vTheta)*vBounceDirElevationVar);

	// calculate cartesian coords from spherical coords
	Vec2V vSinCosTheta = SinCosApprox(vThetaNew);
	Vec2V vSinCosPhi = SinCosApprox(vPhiNew);
	vR.SetX(vSinCosTheta.GetX()*vSinCosPhi.GetY());
	vR.SetY(vSinCosTheta.GetX()*vSinCosPhi.GetX());
	vR.SetZ(vSinCosTheta.GetY());
	vR *= vSphereRadius;

	// rotate the input vector back into normal space
	vR = Transform(rotMtx, vR);
}

void ptxu_Collision::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	if (pEffectInst->HasCollisionSet() && pPointSB->GetFlag(PTXPOINT_FLAG_COLLIDES))
	{
		// cache some data
		Vec3V vPtxDir = Normalize(pPointDB->GetCurrVel());
		ScalarV vKeyTimePoint = pPointDB->GetKeyTime();
		ScalarV vRadiusMult = ScalarVFromF32(m_radiusMult);
		const ScalarV vDiv100 = ScalarV(V_FLT_SMALL_2);
		const ScalarV vTwo = ScalarV(V_TWO);

		// calculate the radius of the particle
		ScalarV vOverrideMinRadius = ScalarVFromF32(m_overrideMinRadius);
		ScalarV vPtxRadius = CalcRadius(pPointSB, pPointDB, vRadiusMult, vOverrideMinRadius);

		// check if the particle is moving too quick for the quick collision test to handle
		Vec3V vDistVec = pPointDB->GetCurrPos()-pPointDB->GetPrevPos();
		ScalarV vDist = Mag(vDistVec);
		bool bTooFast = IsGreaterThanAll(vDist,(vTwo*vPtxRadius))?true:false;

		// debug switches
#if RMPTFX_BANK
		static bool alwaysTooFast = false;
		static bool neverTooFast = false;
		if (alwaysTooFast)
		{
			bTooFast = true;
		}
		if (neverTooFast)
		{
			bTooFast = false;
		}
#endif

		// test against all polys
		ptxCollisionSet& collisionSet = *pEffectInst->GetCollisionSet();
		for (int i=0; i<collisionSet.GetNumColnPolys(); i++)
		{
			ScalarV vColnDepth;
			Vec3V vColnPos, vColnNormal;

			ptxCollisionPoly& colnPoly = collisionSet.GetColnPoly(i);
			if (HasCollidedWithPoly(vColnPos, vColnNormal, vColnDepth, colnPoly, pPointDB->GetCurrPos(), pPointDB->GetPrevPos(), vPtxDir, vPtxRadius, bTooFast))
			{
				// set the point's 'has collided' flag
				pPointSB->SetFlag(PTXPOINT_FLAG_HAS_COLLIDED);

				// store the collision data on the point
				pPointSB->SetColnPos(vColnPos);
				pPointSB->SetColnNormal(vColnNormal);
				pPointSB->SetColnSpeed(Mag(pPointDB->GetCurrVel()).Getf());
				pPointSB->SetColnMtlId(colnPoly.GetMtlId());

				ptxEvolutionList* pEvolutionList = params.pEvolutionList;

				// bounciness
				Vec4V vBouncinessMinMax = m_bouncinessKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
				ScalarV vBouncinessBias = GetBiasValue(m_bouncinessKFP, pPointSB->GetRandIndex());
				ScalarV vBounciness = Lerp(vBouncinessBias, SplatX(vBouncinessMinMax), SplatY(vBouncinessMinMax)) * vDiv100;

				// bounce direction variance
				Vec4V vBounceDirVarMinMax = m_bounceDirVarKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
				ScalarV vBounceDirVarBias = GetBiasValue(m_bounceDirVarKFP, pPointSB->GetRandIndex());
				ScalarV vBounceDirRotationVar = Lerp(vBounceDirVarBias, SplatX(vBounceDirVarMinMax), SplatY(vBounceDirVarMinMax));
				ScalarV vBounceDirElevationVar = Lerp(vBounceDirVarBias, SplatZ(vBounceDirVarMinMax), SplatW(vBounceDirVarMinMax));


				// we don't want to apply bounce variance when velocity is zero
				// this fixes NaN asserts in ApplyBounceVariance
				// (e.g.: vR was being normalised even though it's a zero vector)
				if (!IsZeroAll(pPointDB->GetCurrVel()))
				{

					// calc the new velocity (reflect the old velocity about the collision normal)
					// r = i-(2n*dot(i,n))
					ptxAssertf(IsFiniteAll(pPointDB->GetCurrVel()), "pPointDB->GetCurrVel() is not finite");
					ptxAssertf(IsFiniteAll(vColnNormal), "vColnNormal is not finite");
					Vec3V vI = pPointDB->GetCurrVel();
					ScalarV vIdotN = Dot(vI, vColnNormal);
					Vec3V vR = vI - (vTwo*vColnNormal*vIdotN);

					// apply any bounce variance to the reflected vector
					// commented out while figuring out what's wrong with this code (causes a stack overflow).
					ptxAssertf(IsFiniteAll(vBounceDirRotationVar), "vBounceDirRotationVar is not finite");
					ptxAssertf(IsFiniteAll(vBounceDirElevationVar), "vBounceDirElevationVar is not finite");
					ApplyBounceVariance(vR, vColnNormal, vBounceDirRotationVar, vBounceDirElevationVar);

					// set the new velocity of the particle
					ptxAssertf(IsFiniteAll(vR), "vR is not finite");
					ptxAssertf(IsFiniteAll(vBounciness), "vBounciness is not finite");
					pPointDB->SetCurrVel(vR * vBounciness);
				}

				// check for being at rest 
				if ((pPointSB->GetFlag(PTXPOINT_FLAG_AT_REST) || MagSquared(pPointDB->GetCurrVel()).Getf()<=m_restSpeed*m_restSpeed))
				{
					// zero any velocity on the particle
					pPointDB->SetCurrVel(Vec3V(V_ZERO));
					pPointSB->SetFlag(PTXPOINT_FLAG_AT_REST);
				}

				// set the new position and direction of the particle
				pPointDB->IncCurrPos(vColnNormal*vColnDepth);
				vPtxDir = Normalize(pPointDB->GetCurrVel());

				// kill particle if tuned to do so
				if (m_killChance>0 && g_DrawRand.GetRanged(0, 100)<=m_killChance)
				{
					pPointDB->SetIsDead(true);
				}
			}
		}
	}
}

void ptxu_Collision::UpdateFinalize(ptxBehaviourParams& RMPTFX_BANK_ONLY(params))
{
#if RMPTFX_BANK
	// debug drawing
	if (m_debugDraw)
	{
		// cache data from params
		ptxEffectInst* pEffectInst = params.pEffectInst;

		// get the particle data (single and double buffered)
		ptxPointSB* RESTRICT pPointSB = params.pPointItem->GetDataSB();
		ptxPointDB* RESTRICT pPointDB = params.pPointItem->GetDataDB();

		if (pEffectInst->HasCollisionSet() && pPointSB->GetFlag(PTXPOINT_FLAG_COLLIDES))
		{
			// calculate the radius of the particle
			ScalarV vRadiusMult = ScalarVFromF32(m_radiusMult);
			ScalarV vOverrideMinRadius = ScalarVFromF32(m_overrideMinRadius);
			ScalarV vPtxRadius = CalcRadius(pPointSB, pPointDB, vRadiusMult, vOverrideMinRadius);

			// draw the collision sphere
			grcDebugDraw::Sphere(pPointDB->GetCurrPos(), vPtxRadius.Getf(), Color32(70, 255, 200, 50), true);
			grcDebugDraw::Sphere(pPointDB->GetCurrPos(), vPtxRadius.Getf(), Color32(70, 255, 200, 255), false);

			// draw the velocity vector
			grcDebugDraw::Line(pPointDB->GetCurrPos(), pPointDB->GetCurrPos()+pPointDB->GetCurrVel(), Color32(255, 255, 0, 255));
		}
	}
#endif
}

void ptxu_Collision::SetKeyframeDefns()
{
	m_bouncinessKFP.GetKeyframe().SetDefn(&sm_bouncinessDefn);
	m_bounceDirVarKFP.GetKeyframe().SetDefn(&sm_bounceDirVarDefn);
}

void ptxu_Collision::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_bouncinessKFP, &sm_bouncinessDefn);
	pEvolutionList->SetKeyframeDefn(&m_bounceDirVarKFP, &sm_bounceDirVarDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Collision::IsActive()
{
	ptxu_Collision* pCollision = static_cast<ptxu_Collision*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_bouncinessKFP == pCollision->m_bouncinessKFP &&
				   m_bounceDirVarKFP == pCollision->m_bounceDirVarKFP &&
				   m_radiusMult == pCollision->m_radiusMult &&
				   m_restSpeed == pCollision->m_restSpeed &&
				   m_colnChance == pCollision->m_colnChance &&
				   m_killChance == pCollision->m_killChance && 
				   m_overrideMinRadius == pCollision->m_overrideMinRadius;

	return !isEqual;
}

void ptxu_Collision::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 8);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Bounce Direction Variance");
	SendKeyframeDefnToEditor(buff, "Bounce Dampening");

	// send non-keyframes
	SendFloatDefnToEditor(buff,"Collision Radius Mult", 0.0f, 5.0f);
	SendFloatDefnToEditor(buff,"Stop At Velocity", 0.0f, 10000.0f);
	SendIntDefnToEditor(buff,"Percent Physical", 0, 100);
	SendIntDefnToEditor(buff,"Percent Kill", 0, 100);
	SendBoolDefnToEditor(buff, "Debug Draw Collision");
	SendFloatDefnToEditor(buff,"Override Min Radius", 0.0f, 5.0f);
}

void ptxu_Collision::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_bouncinessKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_bounceDirVarKFP, pParticleRule);

	// send non-keyframes
	SendFloatToEditor(buff, m_radiusMult);
	SendFloatToEditor(buff, m_restSpeed);
	SendIntToEditor(buff, m_colnChance);
	SendIntToEditor(buff, m_killChance);
	SendBoolToEditor(buff, m_debugDraw);
	SendFloatToEditor(buff, m_overrideMinRadius);
}

void ptxu_Collision::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_bouncinessKFP);
	ReceiveKeyframeFromEditor(buff, m_bounceDirVarKFP);

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_radiusMult);
	ReceiveFloatFromEditor(buff, m_restSpeed);
	ReceiveIntFromEditor(buff, m_colnChance);
	ReceiveIntFromEditor(buff, m_killChance);
	ReceiveBoolFromEditor(buff, m_debugDraw);
	ReceiveFloatFromEditor(buff, m_overrideMinRadius);
}
#endif // RMPTFX_EDITOR


} // namespace rage
