// 
// rmptfx/ptxu_noise.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "rmptfx/ptxu_noise.h"
#include "rmptfx/ptxu_noise_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Noise::sm_posNoiseMinDefn(					"Position Noise Min", 		atHashValue("ptxu_Noise:m_posNoiseMinKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1000.0f,	0.01f),		"Min X",	"Min Y",	"Min Z");
ptxKeyframeDefn ptxu_Noise::sm_posNoiseMaxDefn(					"Position Noise Max", 		atHashValue("ptxu_Noise:m_posNoiseMaxKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1000.0f,	0.01f),		"Max X",	"Max Y",	"Max Z");
ptxKeyframeDefn ptxu_Noise::sm_velNoiseMinDefn(					"Velocity Noise Min", 		atHashValue("ptxu_Noise:m_velNoiseMinKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1000.0f,	0.01f),		"Min X",	"Min Y",	"MinZ");
ptxKeyframeDefn ptxu_Noise::sm_velNoiseMaxDefn(					"Velocity Noise Max", 		atHashValue("ptxu_Noise:m_velNoiseMaxKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1000.0f,	0.01f),		"Max X",	"Max Y",	"Max Z");


// code
#if RMPTFX_XML_LOADING
ptxu_Noise::ptxu_Noise()
{
	// create keyframes
	m_posNoiseMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Noise:m_posNoiseMinKFP"));
	m_posNoiseMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Noise:m_posNoiseMaxKFP"));
	m_velNoiseMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Noise:m_velNoiseMinKFP"));
	m_velNoiseMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Noise:m_velNoiseMaxKFP"));

	m_keyframePropList.AddKeyframeProp(&m_posNoiseMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_posNoiseMaxKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_velNoiseMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_velNoiseMaxKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Noise::SetDefaultData()
{
	// default keyframes
	m_posNoiseMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_posNoiseMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_velNoiseMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_velNoiseMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_referenceSpace = PTXU_NOISE_REFERENCESPACE_WORLD;
	m_keepConstantSpeed = false;

}

ptxu_Noise::ptxu_Noise(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_posNoiseMinKFP(rsc)
	, m_posNoiseMaxKFP(rsc)
	, m_velNoiseMinKFP(rsc)
	, m_velNoiseMaxKFP(rsc)	
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Noise_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Noise::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Noise, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Noise, m_posNoiseMinKFP)
		SSTRUCT_FIELD(ptxu_Noise, m_posNoiseMaxKFP)
		SSTRUCT_FIELD(ptxu_Noise, m_velNoiseMinKFP)
		SSTRUCT_FIELD(ptxu_Noise, m_velNoiseMaxKFP)
		SSTRUCT_FIELD(ptxu_Noise, m_referenceSpace)
		SSTRUCT_FIELD(ptxu_Noise, m_keepConstantSpeed)
		SSTRUCT_IGNORE(ptxu_Noise, m_pad)
	SSTRUCT_END(ptxu_Noise)
}
#endif

IMPLEMENT_PLACE(ptxu_Noise);

void ptxu_Noise::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointNoise)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Noise::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	
	ScalarV	vDeltaTime = params.vDeltaTime;

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// cache data from the point
	ScalarV	vPlaybackRate = ScalarVFromF32(pPointSB->GetPlaybackRate());

	// get the particle life ratio key time
	ScalarV vPtxTimePoint = pPointDB->GetKeyTime();

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vPosNoiseMin = m_posNoiseMinKFP.Query(vPtxTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vPosNoiseMax = m_posNoiseMaxKFP.Query(vPtxTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vVelNoiseMin = m_velNoiseMinKFP.Query(vPtxTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vVelNoiseMax = m_velNoiseMaxKFP.Query(vPtxTimePoint, pEvolutionList, pPointSB->GetEvoValues());

	// calculate the bias values
	ScalarV vPosBias = GetBiasValue(m_posNoiseMinKFP, pPointSB->GetRandIndex());
	ScalarV vVelBias = GetBiasValue(m_velNoiseMinKFP, pPointSB->GetRandIndex());

	// calculate the lerped noise between the min and max keyframe data
	Vec4V vPosNoise = Lerp(vPosBias, vPosNoiseMin, vPosNoiseMax);
	Vec4V vVelNoise = Lerp(vVelBias, vVelNoiseMin, vVelNoiseMax);

	// apply any position noise
	if (IsZeroAll(vPosNoise)==false)
	{
		// calculate the noise
		Vec3V vNoise = g_ptxVecRandom.GetSignedVec3V();
		vNoise *= vPosNoise.GetXYZ() * Vec3V(vDeltaTime) * vPlaybackRate;

		// change the world space noise into effect or emitter space, if required
		ChangeSpace(vNoise, pEffectInst, pEmitterInst);

		// add noise to the particle position
		pPointDB->IncCurrPos(vNoise);
	}

	// apply any velocity noise
	if (IsZeroAll(vVelNoise)==false)
	{
		// calculate the noise
		Vec3V vNoise = g_ptxVecRandom.GetSignedVec3V();
		vNoise *= vVelNoise.GetXYZ() * Vec3V(vDeltaTime) * vPlaybackRate;
		ptxAssertf(IsFiniteAll(vNoise), "vNoise is not finite (1)");

		// change the world space noise into effect or emitter space, if required
		ChangeSpace(vNoise, pEffectInst, pEmitterInst);
		ptxAssertf(IsFiniteAll(vNoise), "vNoise is not finite (2)");

		if (m_keepConstantSpeed)
		{
			// calculate the new particle velocity with the noise added but the speed the same
			Vec3V vPtxVel = pPointDB->GetCurrVel();
			ptxAssertf(IsFiniteAll(vPtxVel), "vPtxVel is not finite (1)");
			ScalarV vPtxSpeed = Mag(vPtxVel);
			if (IsGreaterThanAll(vPtxSpeed, ScalarV(V_FLT_SMALL_3)))
			{
				vPtxVel += vNoise;
				vPtxVel = NormalizeFast(vPtxVel);
				vPtxVel *= Vec3V(vPtxSpeed);
				ptxAssertf(IsFiniteAll(vPtxVel), "vPtxVel is not finite (2)");
			}

			// add noise to the particle velocity
			if (IsFiniteAll(vPtxVel))
			{
				if (!pPointSB->GetFlag(PTXPOINT_FLAG_AT_REST))
				{
					pPointDB->SetCurrVel(vPtxVel);
				}
			}
		}
		else
		{
			// add noise to the particle velocity	
			if (IsFiniteAll(vNoise))
			{
				if (!pPointSB->GetFlag(PTXPOINT_FLAG_AT_REST))
				{
					pPointDB->IncCurrVel(vNoise);
				}
			}
		}
	}
}

void ptxu_Noise::ChangeSpace(Vec3V_InOut vNoise, const ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst)
{
	// calc the dampening in effect space if required
	if (m_referenceSpace!=PTXU_NOISE_REFERENCESPACE_WORLD)
	{
		// get the require matrix
		Mat34V vMtx = Mat34V(V_IDENTITY);
		if (m_referenceSpace==PTXU_NOISE_REFERENCESPACE_EFFECT)
		{
			vMtx = pEffectInst->GetMatrix();
		}
		else if (m_referenceSpace==PTXU_NOISE_REFERENCESPACE_EMITTER)
		{
			vMtx = pEmitterInst->GetCreationDomainMtxWld(pEffectInst->GetMatrix());
		}
		else
		{
			ptxAssertf(0, "unsupported reference space");
		}

		// scale the matrix by the values in each world axis
		vMtx.SetCol0(vMtx.GetCol0() * Vec3V(SplatX(vNoise)));
		vMtx.SetCol1(vMtx.GetCol1() * Vec3V(SplatY(vNoise)));
		vMtx.SetCol2(vMtx.GetCol2() * Vec3V(SplatZ(vNoise)));

		// add up the lengths of each axis to get the world space lengths
		//Abs(vMtx, vMtx);
		vNoise = vMtx.GetCol0() + vMtx.GetCol1() + vMtx.GetCol2();
	}
}

void ptxu_Noise::SetKeyframeDefns()
{
	m_posNoiseMinKFP.GetKeyframe().SetDefn(&sm_posNoiseMinDefn);
	m_posNoiseMaxKFP.GetKeyframe().SetDefn(&sm_posNoiseMaxDefn);
	m_velNoiseMinKFP.GetKeyframe().SetDefn(&sm_velNoiseMinDefn);
	m_velNoiseMaxKFP.GetKeyframe().SetDefn(&sm_velNoiseMaxDefn);
}

void ptxu_Noise::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_posNoiseMinKFP, &sm_posNoiseMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_posNoiseMaxKFP, &sm_posNoiseMaxDefn);
	pEvolutionList->SetKeyframeDefn(&m_velNoiseMinKFP, &sm_velNoiseMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_velNoiseMaxKFP, &sm_velNoiseMaxDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Noise::IsActive()
{
	ptxu_Noise* pNoise = static_cast<ptxu_Noise*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_posNoiseMinKFP == pNoise->m_posNoiseMinKFP &&
				   m_posNoiseMaxKFP == pNoise->m_posNoiseMaxKFP &&
				   m_velNoiseMinKFP == pNoise->m_velNoiseMinKFP &&
				   m_velNoiseMaxKFP == pNoise->m_velNoiseMaxKFP && 
				   m_referenceSpace == pNoise->m_referenceSpace &&
				   m_keepConstantSpeed == pNoise->m_keepConstantSpeed;

	return !isEqual;
}

void ptxu_Noise::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 6);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Position Noise Min");
	SendKeyframeDefnToEditor(buff, "Position Noise Max");
	SendKeyframeDefnToEditor(buff, "Velocity Noise Min");
	SendKeyframeDefnToEditor(buff, "Velocity Noise Max");

	// send non-keyframes
	const char* pComboItemNames[PTXU_NOISE_REFERENCESPACE_NUM] = {"World", "Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Reference Space", PTXU_NOISE_REFERENCESPACE_NUM, pComboItemNames);

	SendBoolDefnToEditor(buff, "Keep Constant Speed");
}

void ptxu_Noise::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_posNoiseMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_posNoiseMaxKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_velNoiseMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_velNoiseMaxKFP, pParticleRule);

	// send non-keyframes
	SendComboItemToEditor(buff, m_referenceSpace);
	SendBoolToEditor(buff, m_keepConstantSpeed);

}

void ptxu_Noise::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_posNoiseMinKFP);
	ReceiveKeyframeFromEditor(buff, m_posNoiseMaxKFP);
	ReceiveKeyframeFromEditor(buff, m_velNoiseMinKFP);
	ReceiveKeyframeFromEditor(buff, m_velNoiseMaxKFP);

	// receive non-keyframes
	ReceiveComboItemFromEditor(buff, m_referenceSpace);
	ReceiveBoolFromEditor(buff, m_keepConstantSpeed);

}
#endif // RMPTFX_EDITOR


} // namespace rage
