// 
// rmptfx/ptxu_animatetexture.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "rmptfx/ptxu_animatetexture.h"
#include "rmptfx/ptxu_animatetexture_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_AnimateTexture::sm_animRateDefn(			"Anim Rate",				atHashValue("ptxu_AnimateTexture:m_animRateKFP"),			PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1000.0f,	0.01f),		"Min FPS",		"Max FPS");


// code
#if RMPTFX_XML_LOADING
ptxu_AnimateTexture::ptxu_AnimateTexture()
{
	// create keyframes
	m_animRateKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_AnimateTexture:m_animRateKFP"));
	m_keyframePropList.AddKeyframeProp(&m_animRateKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_AnimateTexture::SetDefaultData()
{
	// default keyframes
	m_animRateKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_keyframeMode = PTXU_ANIMATETEXTURE_KEYFRAMEMODE_POINT;

	m_lastFrameId = 0;
	m_loopMode = PTXU_ANIMATETEXTURE_LOOPMODE_SAME;

	m_isRandomised = false;
	m_isScaledOverParticleLife = false;
	m_isHeldOnLastFrame = false;
	m_doFrameBlending = true;

}

ptxu_AnimateTexture::ptxu_AnimateTexture(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_animRateKFP(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_AnimateTexture_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_AnimateTexture::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_AnimateTexture, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_AnimateTexture, m_animRateKFP)
		SSTRUCT_FIELD(ptxu_AnimateTexture, m_keyframeMode)
		SSTRUCT_FIELD(ptxu_AnimateTexture, m_lastFrameId)
		SSTRUCT_FIELD(ptxu_AnimateTexture, m_loopMode)
		SSTRUCT_FIELD(ptxu_AnimateTexture, m_isRandomised)
		SSTRUCT_FIELD(ptxu_AnimateTexture, m_isScaledOverParticleLife)
		SSTRUCT_FIELD(ptxu_AnimateTexture, m_isHeldOnLastFrame)
		SSTRUCT_FIELD(ptxu_AnimateTexture, m_doFrameBlending)
		//SSTRUCT_IGNORE(ptxu_AnimateTexture, m_pad)
	SSTRUCT_END(ptxu_AnimateTexture)
}
#endif

IMPLEMENT_PLACE(ptxu_AnimateTexture);

void ptxu_AnimateTexture::InitPoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	pPointDB->SetTexFrameIdA(pPointDB->GetInitTexFrameId());
	pPointDB->SetTexFrameIdB(pPointDB->GetInitTexFrameId());
	
	if (m_doFrameBlending)
	{
		UpdateTextures(params);
	}

	// now update the particle 
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointAnimTex)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_AnimateTexture::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	
	ScalarV	vDeltaTime = params.vDeltaTime;

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// cache data from the point
	ScalarV	vPlaybackRate = ScalarVFromF32(pPointSB->GetPlaybackRate());

	// default the keyframe time to use the particle life ratio
	ScalarV vKeyTime = pPointDB->GetKeyTime();
	if (m_keyframeMode==PTXU_ANIMATETEXTURE_KEYFRAMEMODE_EFFECT)
	{
		// override with effect life ratio if requested
		vKeyTime = pEffectInst->GetKeyTime();
	}
	else if (m_keyframeMode==PTXU_ANIMATETEXTURE_KEYFRAMEMODE_EMITTER)
	{
		// override with emitter life ratio if requested
		vKeyTime = pEmitterInst->GetKeyTime();
	}
	else
	{
		ptxAssertf(m_keyframeMode==PTXU_ANIMATETEXTURE_KEYFRAMEMODE_POINT, "unsupported keyframe mode");
	}

	// cache some data that we'll need later
	ScalarV vTexFrameRatio = pPointDB->GetTexFrameRatioV();
	ScalarV vNumTexFrames = ScalarVFromS32(m_lastFrameId-pPointDB->GetInitTexFrameId());
	vNumTexFrames = IntToFloatRaw<0>(vNumTexFrames);
	ScalarV vOne(V_ONE);

	// calculate the animation rate
	ScalarV vAnimRate;
	if (m_isScaledOverParticleLife)
	{	
		// anim rate is scaled over the life of the particle
		vAnimRate = vNumTexFrames * ScalarVFromF32(pPointSB->GetOOLife()) / vPlaybackRate;
	}
	else
	{
		// anim rate is looked up from the keyframe data
		ptxEvolutionList* pEvolutionList = params.pEvolutionList;
		Vec4V vAnimRateMinMax = m_animRateKFP.Query(vKeyTime, pEvolutionList, pPointSB->GetEvoValues());
		ScalarV vBias = GetBiasValue(m_animRateKFP, pPointSB->GetRandIndex());
		vAnimRate = Lerp(vBias, SplatX(vAnimRateMinMax), SplatY(vAnimRateMinMax));
	}

	// update the current blend ratio
	vTexFrameRatio += vAnimRate * vDeltaTime * vPlaybackRate;

	// check if the current blend is finished 
	while (IsGreaterThanOrEqualAll(vTexFrameRatio, vOne) != 0)
	{
		// get the next texture to blend to
		UpdateTextures(params);

		vTexFrameRatio -= vOne;
	}

	// store the new texture data on the particle
	pPointDB->SetTexFrameRatio(vTexFrameRatio);
}

void ptxu_AnimateTexture::UpdateTextures(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	u8 texFrameIdMin = (u8)params.texFrameIdMin;
	u8 texFrameIdMax = (u8)params.texFrameIdMax;

	// set the previous destination texture to be the new source
	pPointDB->SetTexFrameIdA(pPointDB->GetTexFrameIdB());

	// calculate the new destination texture
	if (m_isHeldOnLastFrame && pPointDB->GetTexFrameIdA()==m_lastFrameId)
	{
		// we're holding on the last frame and we're at the last frame - stay here
		pPointDB->SetTexFrameIdB(pPointDB->GetTexFrameIdA());
	}
	else if (m_isRandomised)
	{
		if (texFrameIdMin == texFrameIdMax)
		{
			pPointDB->SetTexFrameIdB((u8)texFrameIdMin);
		}
		else
		{
			// make sure tex frame b is in range
			pPointDB->SetTexFrameIdB(Clamp(pPointDB->GetTexFrameIdB(), texFrameIdMin, texFrameIdMax));

			// the destination frame is a random one
			pPointDB->SetTexFrameIdB((u8)g_DrawRand.GetRangedDifferent(pPointDB->GetTexFrameIdB(), texFrameIdMin, texFrameIdMax));
		}
	}
	else
	{
		// the destination frame is the next one
		pPointDB->IncTexFrameIdB(1);

		// check if we've gone past the last frame
		if (pPointDB->GetTexFrameIdB()>m_lastFrameId)
		{
			// we need to loop - calculate and set the new init frame		
			if (m_loopMode==PTXU_ANIMATETEXTURE_LOOPMODE_ZERO)
			{
				// the start index is always 0
				pPointDB->SetInitTexFrameId(0);
			}
			else if (m_loopMode==PTXU_ANIMATETEXTURE_LOOPMODE_MIN)
			{
				// the start index is set to the min of the start range
				pPointDB->SetInitTexFrameId(texFrameIdMin);
			}
			else if (m_loopMode==PTXU_ANIMATETEXTURE_LOOPMODE_MAX)
			{
				// the start index is set to the max of the start range 
				pPointDB->SetInitTexFrameId(texFrameIdMax);
			}
			else if (m_loopMode==PTXU_ANIMATETEXTURE_LOOPMODE_RAND_MIN_MAX)
			{
				// the start index is recalculated between min and max
				pPointDB->SetInitTexFrameId((u8)g_DrawRand.GetRanged(texFrameIdMin, texFrameIdMax));
			}
			else
			{
				// the start index remains as it was previously
			}

			// set the destination frame to be the new init frame
			pPointDB->SetTexFrameIdB(pPointDB->GetInitTexFrameId());
		}
	}

	// if there's no frame blending then copy the destination frame to the source frame
	if (m_doFrameBlending==false)
	{
		pPointDB->SetTexFrameIdA(pPointDB->GetTexFrameIdB());
	}
}

void ptxu_AnimateTexture::SetKeyframeDefns()
{
	m_animRateKFP.GetKeyframe().SetDefn(&sm_animRateDefn);
}

void ptxu_AnimateTexture::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_animRateKFP, &sm_animRateDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_AnimateTexture::IsActive()
{
	ptxu_AnimateTexture* pAnimateTexture = static_cast<ptxu_AnimateTexture*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_animRateKFP == pAnimateTexture->m_animRateKFP && 
				   m_keyframeMode == pAnimateTexture->m_keyframeMode &&
				   m_lastFrameId == pAnimateTexture->m_lastFrameId && 
				   m_loopMode == pAnimateTexture->m_loopMode &&
				   m_isRandomised == pAnimateTexture->m_isRandomised &&
				   m_isScaledOverParticleLife == pAnimateTexture->m_isScaledOverParticleLife &&
				   m_isHeldOnLastFrame == pAnimateTexture->m_isHeldOnLastFrame &&
				   m_doFrameBlending == pAnimateTexture->m_doFrameBlending;

	return !isEqual;
}

void ptxu_AnimateTexture::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 8);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Texture Animation Rate");

	// send non-keyframes
	const char* pKeyframeComboItemNames[PTXU_ANIMATETEXTURE_KEYFRAMEMODE_NUM] = {"Point", "Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Keyframe Mode", PTXU_ANIMATETEXTURE_KEYFRAMEMODE_NUM, pKeyframeComboItemNames);

	SendIntDefnToEditor(buff, "Last Frame Id", 0, 99*99);

	const char* pLoopComboItemNames[PTXU_ANIMATETEXTURE_LOOPMODE_NUM] = {"Same", "Zero", "Min", "Max", "RandMinMax"};
	SendComboDefnToEditor(buff, "Loop Mode", PTXU_ANIMATETEXTURE_LOOPMODE_NUM, pLoopComboItemNames);

	SendBoolDefnToEditor(buff, "Is Randomised");
	SendBoolDefnToEditor(buff, "Is Scaled Over Particle Life");
	SendBoolDefnToEditor(buff, "Is Held On Last Frame");
	SendBoolDefnToEditor(buff, "Blend Frames");
}

void ptxu_AnimateTexture::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_animRateKFP, pParticleRule);

	// send non-keyframes
	SendComboItemToEditor(buff, m_keyframeMode);

	SendIntToEditor(buff, m_lastFrameId);
	SendIntToEditor(buff, m_loopMode);

	SendBoolToEditor(buff, m_isRandomised);
	SendBoolToEditor(buff, m_isScaledOverParticleLife);
	SendBoolToEditor(buff, m_isHeldOnLastFrame);
	SendBoolToEditor(buff, m_doFrameBlending);
}

void ptxu_AnimateTexture::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_animRateKFP);

	// receive non-keyframes
	ReceiveComboItemFromEditor(buff, m_keyframeMode);

	ReceiveIntFromEditor(buff, m_lastFrameId);
	ReceiveIntFromEditor(buff, m_loopMode);

	ReceiveBoolFromEditor(buff, m_isRandomised);
	ReceiveBoolFromEditor(buff, m_isScaledOverParticleLife);
	ReceiveBoolFromEditor(buff, m_isHeldOnLastFrame);
	ReceiveBoolFromEditor(buff, m_doFrameBlending);

}
#endif // RMPTFX_EDITOR


} // namespace rage
