<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxEffectRule" cond="RMPTFX_XML_LOADING">
	<float name="m_fileVersion"/>
	<string name="m_name" type="ConstString"/>
	<struct name="m_timeline" type="rage::ptxTimeLine"/>
	<pointer name="m_pEvolutionList" type="rage::ptxEvolutionList" policy="owner"/>
	<Vector3 name="m_vRandOffsetPos"/>
	<int name="m_numLoops"/>
  <bool name="m_sortEventsByDistance" init="0"/>
  <u8 name="m_drawListId" init="0"/>
  <bool name="m_isShortLived" init="0"/>
  <bool name="m_hasNoShadows" init="false"/>
	<float name="m_preUpdateTime"/>
  <float name="m_preUpdateTimeInterval" init="0.25"/>
	<float name="m_durationMin"/>
	<float name="m_durationMax"/>
	<float name="m_playbackRateScalarMin"/>
	<float name="m_playbackRateScalarMax"/>
	<u8 name="m_viewportCullingMode"/>
	<bool name="m_renderWhenViewportCulled"/>
	<bool name="m_updateWhenViewportCulled"/>
	<bool name="m_emitWhenViewportCulled"/>
	<u8 name="m_distanceCullingMode"/>
	<bool name="m_renderWhenDistanceCulled"/>
	<bool name="m_updateWhenDistanceCulled"/>
	<bool name="m_emitWhenDistanceCulled"/>
  <Vector3 name="m_viewportCullingSphereOffset"/>
  <float name="m_viewportCullingSphereRadius"/>
	<float name="m_distanceCullingFadeDist"/>
	<float name="m_distanceCullingCullDist"/>
	<float name="m_lodEvoDistMin"/>
	<float name="m_lodEvoDistMax"/>
  <float name="m_colnRange"/>
  <float name="m_colnProbeDist"/>
	<u8 name="m_colnType"/>
	<bool name="m_shareEntityColn"/>
	<bool name="m_onlyUseBVHColn"/>
	<u8 name="m_gameFlags" init="0"/>
  <struct name="m_colourTintMinKFP" type="rage::ptxKeyframeProp"/>
  <struct name="m_colourTintMaxKFP" type="rage::ptxKeyframeProp"/>
  <struct name="m_zoomScalarKFP" type="rage::ptxKeyframeProp"/>
  <struct name="m_dataSphereKFP" type="rage::ptxKeyframeProp"/>
  <struct name="m_dataCapsuleKFP" type="rage::ptxKeyframeProp"/>
  <bool name="m_colourTintMaxEnable" init="0"/>
  <bool name="m_useDataVolume" init="0"/>
  <u8 name="m_dataVolumeType" init="0"/>
</structdef>

</ParserSchema>
