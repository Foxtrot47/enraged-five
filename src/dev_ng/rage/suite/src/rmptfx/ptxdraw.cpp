// 
// rmptfx/ptxdraw.cpp
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxdraw.h"

// includes (rmptfx)
#include "rmptfx/ptxclipregion.h"
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxpoint.h"
#include "rmptfx/ptxkeyframeprop.h"
#include "rmptfx/ptxd_drawsprite.h"
#include "rmptfx/ptxd_drawtrail.h"
#include "rmptfx/ptxmanager.h"

// includes (rage)
#include "grcore/vertexbuffer.h"
#include "system/nelem.h"
#include "system/xtl.h"
#include "math/float16.h"

#if RMPTFX_BANK
#include "rmptfx/ptxdebug.h"
#endif

// optimisations
RMPTFX_OPTIMISATIONS()

// namespaces
namespace rage
{
// defines
#if PTXDRAW_USE_INSTANCING
#define PTXDRAW_INST_STREAM_ID				1 // use instanced data
#define PTXDRAW_VTX_STREAM_ID				0
#define PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER	1
#else
#define PTXDRAW_INST_STREAM_ID				0 // use non-instanced data
#define PTXDRAW_VTX_STREAM_ID				0
#define PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER	0
#endif

#if __D3D11 || RSG_ORBIS
#define	PackColor32IntoWComponent	PackABGRColor32IntoWComponent
#elif RSG_PC || RSG_XENON 
#define	PackColor32IntoWComponent	PackARGBColor32IntoWComponent
#else	//RSG_PS3
#define	PackColor32IntoWComponent	PackRGBAColor32IntoWComponent
#endif	//platforms

//	lo res
//	1 ___________ 3	
//	|             |	
//	| |        /|\|	
//	| |    A    | |	
//	| |         | |	
//	| |_________| |	
//	0 ----------- 2	

#define PTXDRAW_SPRITE_VTX_OFFSETS_LORES	{Vec2V(V_NEGONE), Vec2V(-1.0f, 1.0f), Vec2V(1.0f, -1.0f),	Vec2V(V_ONE)}

#if PTXDRAW_SPRITE_USE_QUADS
#define PTXDRAW_SPRITE_INDICES_LORES	{0, 2, 3, 1}	
#else
#define PTXDRAW_SPRITE_INDICES_LORES	{0, 2, 3, 0, 3, 1}
#endif

//	hi res
//	2 ___________ 5	___________ 8
//	|             |	 _________  |
//	| |        /|\|	|           |
//	| |    B    | |	|    D      |
//	| |         | |	|           |
//	| |_________| |	|_________> |
//	1 ----------- 4	 ---------- 7
//	| <_________  |	 _________  |
//	|           | | |         | |
//	|      A    | |	|    C    | |
//	|           | |\|/        | |
//	| __________| |	            |
//	0 ----------- 3	----------- 6

#define PTXDRAW_SPRITE_VTX_OFFSETS_HIRES	{Vec2V(V_NEGONE), Vec2V(-1.0f, 0.0f), Vec2V(-1.0f, 1.0f), Vec2V(0.0f, -1.0f), Vec2V(V_ZERO), Vec2V(V_Y_AXIS_WZERO), Vec2V(1.0f, -1.0f), Vec2V(V_X_AXIS_WZERO), Vec2V(V_ONE)}

#if PTXDRAW_SPRITE_USE_QUADS
#define PTXDRAW_SPRITE_INDICES_HIRES	{0, 3, 4, 1, 2, 1, 4, 5, 6, 7, 4, 3, 8, 5, 4, 7}		
#else
#define PTXDRAW_SPRITE_INDICES_HIRES	{0, 3, 4, 0, 4, 1, 2, 1, 4, 2, 4, 5, 6, 7, 4, 6, 4, 3, 8, 5, 4, 8, 4, 7}
#endif

#define PTXDRAW_SPRITE_INDICES_PROJECTED {0,0,1,3,2,7,6,4,5,5,2,2,6,1,5,0,4,3,7,7}


// static variables
#if RMPTFX_BANK
grcRasterizerStateHandle		ptxDrawInterface::sm_debugWireframeRasterizerState			= grcStateBlock::RS_Invalid;
grcBlendStateHandle				ptxDrawInterface::sm_debugWireframeBlendState				= grcStateBlock::BS_Invalid;
grcDepthStencilStateHandle		ptxDrawInterface::sm_debugWireframeDepthStencilState		= grcStateBlock::DSS_Invalid;
grcRasterizerStateHandle		ptxDrawInterface::sm_exitDebugWireframeRasterizerState		= grcStateBlock::RS_Invalid;
grcBlendStateHandle				ptxDrawInterface::sm_exitDebugWireframeBlendState			= grcStateBlock::BS_Invalid;
grcDepthStencilStateHandle		ptxDrawInterface::sm_exitDebugWireframeDepthStencilState	= grcStateBlock::DSS_Invalid;
#endif

#if GS_INSTANCED_SHADOWS
grcVertexDeclaration* ptxDrawInterface::sm_pSpriteVtxDecl[NUMBER_OF_RENDER_THREADS] = {NULL}; 
grcVertexDeclaration* ptxDrawInterface::sm_pSpriteVtxDeclInst = NULL;
grcVertexDeclaration* ptxDrawInterface::sm_pSpriteVtxDeclDefault = NULL;
#else
grcVertexDeclaration* ptxDrawInterface::sm_pSpriteVtxDecl = NULL;
#endif
grcVertexDeclaration* ptxDrawInterface::sm_pTrailVtxDecl = NULL; 
grcVertexDeclaration* ptxDrawInterface::sm_pTrailVtxDecl_lit = NULL; 

#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
int ptxDrawInterface::sm_posIdxTableHiResIndexed[PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED*2];
int ptxDrawInterface::sm_posIdxTableLoResIndexed[PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED*2];

ALIGNAS(16) u16 ptxDrawInterface::sm_spriteVtxIndicesLoRes[PTXDRAW_LOW_RES_INDEXBUFFER_SIZE];
ALIGNAS(16) u16 ptxDrawInterface::sm_spriteVtxIndicesHiRes[PTXDRAW_HI_RES_INDEXBUFFER_SIZE];
ALIGNAS(16) u16 ptxDrawInterface::sm_spriteProjVtxIndices[PTXDRAW_PROJECTED_INDEXBUFFER_SIZE];

#if PTXDRAW_SPRITE_USE_INDEX_BUFFER
grcIndexBuffer *ptxDrawInterface::sm_pLoResIndexBuffer = NULL;
grcIndexBuffer *ptxDrawInterface::sm_pHiResIndexBuffer = NULL;
grcIndexBuffer *ptxDrawInterface::sm_pProjIndexBuffer = NULL;
#endif

#if PTXDRAW_USE_INSTANCING
grcVertexBuffer *ptxDrawInterface::sm_pLoResVertBuffer = NULL;
grcVertexBuffer *ptxDrawInterface::sm_pHiResVertBuffer = NULL;
grcVertexBuffer *ptxDrawInterface::sm_pProjVertBuffer = NULL;
#endif
#endif //PTXDRAW_SPRITE_USE_INDEXED_VERTS

#if PTXDRAW_TRAIL_USE_INDEX_BUFFERS
const grcIndexBuffer *ptxDrawInterface::sm_pTrailIndexBuffers[PTXDRAW_TRAIL_TESS_X_MAX - PTXDRAW_TRAIL_TESS_X_MIN + 1][PTXDRAW_TRAIL_TESS_Y_MAX - PTXDRAW_TRAIL_TESS_Y_MIN + 1];
#endif

#if PTXDRAW_USE_INSTANCING
ptxSpriteInst* ptxDrawInterface::sm_pCurrInstance[NUMBER_OF_RENDER_THREADS];
#else
ptxSpriteVtxCommon* ptxDrawInterface::sm_pCurrCommon[NUMBER_OF_RENDER_THREADS];
#endif

Vec4V ptxDrawInterface::sm_defaultClipRegion;

int ptxDrawInterface::sm_posIdxTableLoRes[PTXDRAW_NUM_VERTS_PER_SPRITE_LORES*2];
int ptxDrawInterface::sm_posIdxTableHiRes[PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES*2];

static const grcDrawMode s_DrawMode = PTXDRAW_SPRITE_USE_QUADS ? drawQuads : drawTris;

#if  PTXDRAW_TRAIL_USE_INDEX_BUFFERS

__forceinline void DrawTrailTessellator_BuildIndices(u16* pIndices, int numControlPoints, int tessX, int tessY, int ASSERT_ONLY(numIndices))
{
	ASSERT_ONLY(const u16* pIndicesStart = pIndices);
	Assert(tessY > 1); // if tessY=1 we should have used the non-indexed codepath

	const int num = tessX*(numControlPoints - 1);
	int index0 = 0;
	int index1 = tessY + 1;

	for (int i = 0; i < num; i++)
	{
		*(pIndices++) = (u16)(index0);

		for (int j = 0; j <= tessY; j++)
		{
			*(pIndices++) = (u16)(index0 + j);
			*(pIndices++) = (u16)(index1 + j);
		}

		*(pIndices++) = (u16)(index1 + tessY);

		index0 = index1;
		index1 += tessY + 1;
	}

	Assert(pIndicesStart + numIndices == pIndices);
}

#else // not (PTXDRAW_TRAIL_USE_INDEX_BUFFERS)

template <bool bLit> static __forceinline void DrawTrailTessellator_BuildNonIndexedMesh(ptxTrailVtx<bLit>* pDst, const ptxTrailVtx<bLit>* pSrc, int numControlPoints, int tessX, int tessY)
{
	const int numPatches = numControlPoints - 1;
	const int numCols    = numPatches*tessX + 1;
	const int numRows    = tessY + 1; // stride
#if __ASSERT
	const int numIndicesPerPatch = (tessY > 1) ? (tessX*(tessY*2 + 4)) : (tessX*2 + 2);
	const int numIndices         = numPatches*numIndicesPerPatch;
	const ptxTrailVtx<bLit>* pEnd = pDst + numIndices;
#endif // __ASSERT

	if (tessY > 1)
	{
		for (int colIndex = 1; colIndex < numCols; colIndex++)
		{
			*(pDst++) = pSrc[0]; // extra vertex to join strips

			for (int rowIndex = 0; rowIndex < numRows; rowIndex++)
			{
				*(pDst++) = pSrc[rowIndex];
				*(pDst++) = pSrc[rowIndex + numRows];
			}

			*(pDst++) = pSrc[numRows*2 - 1]; // extra vertex to join strips

			pSrc += numRows;
		}
	}
	else // tessY == 1
	{
		memcpy(pDst, pSrc, (tessX*2 + 2)*sizeof(ptxTrailVtx<bLit>));

		pDst += (tessX*2 + 2);
	}

	ptxAssert(pDst == pEnd);
}

#endif // not (PTXDRAW_TRAIL_USE_INDEX_BUFFERS)

// code
void ptxDrawInterface::Init()
{
#if RMPTFX_BANK
	ptxDebug::sm_pointAlphaCullThreshold = PTXDRAW_ALPHA_CULL_THRESHOLD;

	InitRenderStateBlocks();
#endif

#if !__RESOURCECOMPILER
	grcFvf::grcStreamFrequencyMode streamFrequencyMode = grcFvf::grcsfmDivide;
#if PTXDRAW_USE_INSTANCING
	streamFrequencyMode = grcFvf::grcsfmIsInstanceStream;
#endif

	const grcVertexElement spriteVtxElements[] =
	{
		// ptxSpriteInst
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetPosition, 0, 12, grcFvf::grcdsFloat3, streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// centrePosX, centrePosY, centrePosZ
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetColor,    0, 4,  grcFvf::grcdsColor,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// colour
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetNormal,   0, 8,  grcFvf::grcdsHalf4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// normalX, normalY, normalZ
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  0, 8,  grcFvf::grcdsHalf4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// texFrameRatio, emissiveIntensity, spare, spare
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  1, 8,  grcFvf::grcdsHalf4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// customVars
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  2, 8,  grcFvf::grcdsHalf4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// customVars
	
#if PTXDRAW_USE_INSTANCING
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  6, 16,  grcFvf::grcdsFloat4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// vDirUp
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  7, 16,  grcFvf::grcdsFloat4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// vDirRight
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  8, 16,  grcFvf::grcdsFloat4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// vDimensionsScaled
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  9, 16,  grcFvf::grcdsFloat4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// vClipRegion
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  10,16,  grcFvf::grcdsFloat4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// vUVinfoA
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  11,16,  grcFvf::grcdsFloat4,  streamFrequencyMode, PTXDRAW_SPRITE_STREAM_FREQ_DIVIDER),	// vUVinfoB
#endif

		// ptxSpriteVertex
		grcVertexElement(PTXDRAW_VTX_STREAM_ID,  grcVertexElement::grcvetTexture,  3, 16, grcFvf::grcdsFloat4),																// wldPosX, wldPosY, wldPosZ, spare
		grcVertexElement(PTXDRAW_VTX_STREAM_ID,  grcVertexElement::grcvetTexture,  4, 8,  grcFvf::grcdsHalf4),																// uTexA, vTexA, uTexB, vTexB
		grcVertexElement(PTXDRAW_VTX_STREAM_ID,  grcVertexElement::grcvetTexture,  5, 8,  grcFvf::grcdsHalf4),																// uNorm, vNorm, distToCentre, spare


	};
#if GS_INSTANCED_SHADOWS
	streamFrequencyMode = grcFvf::grcsfmIsInstanceStream;
	const grcVertexElement spriteVtxElementsInst[] =
	{
		// ptxSpriteInst
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetPosition, 0, 12, grcFvf::grcdsFloat3, streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// centrePosX, centrePosY, centrePosZ
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetColor,    0, 4,  grcFvf::grcdsColor,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// colour
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetNormal,   0, 8,  grcFvf::grcdsHalf4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// normalX, normalY, normalZ
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  0, 8,  grcFvf::grcdsHalf4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// texFrameRatio, emissiveIntensity, spare, spare
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  1, 8,  grcFvf::grcdsHalf4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// customVars
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  2, 8,  grcFvf::grcdsHalf4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// customVars

#if PTXDRAW_USE_INSTANCING
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  6, 16,  grcFvf::grcdsFloat4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// vDirUp
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  7, 16,  grcFvf::grcdsFloat4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// vDirRight
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  8, 16,  grcFvf::grcdsFloat4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// vDimensionsScaled
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  9, 16,  grcFvf::grcdsFloat4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// vClipRegion
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  10,16,  grcFvf::grcdsFloat4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// vUVinfoA
		grcVertexElement(PTXDRAW_INST_STREAM_ID, grcVertexElement::grcvetTexture,  11,16,  grcFvf::grcdsFloat4,  streamFrequencyMode, CASCADE_SHADOWS_COUNT),	// vUVinfoB
#endif

		// ptxSpriteVertex
		grcVertexElement(PTXDRAW_VTX_STREAM_ID,  grcVertexElement::grcvetTexture,  3, 16, grcFvf::grcdsFloat4),																// wldPosX, wldPosY, wldPosZ, spare
		grcVertexElement(PTXDRAW_VTX_STREAM_ID,  grcVertexElement::grcvetTexture,  4, 8,  grcFvf::grcdsHalf4),																// uTexA, vTexA, uTexB, vTexB
		grcVertexElement(PTXDRAW_VTX_STREAM_ID,  grcVertexElement::grcvetTexture,  5, 8,  grcFvf::grcdsHalf4),																// uNorm, vNorm, distToCentre, spare


	};
#if !PTXDRAW_USE_INSTANCING
	streamFrequencyMode = grcFvf::grcsfmDivide;
#endif
#endif

	const grcVertexElement trailVtxElements[] =
	{
		// ptxTrailVertex
		grcVertexElement(0, grcVertexElement::grcvetPosition, 0, 12, grcFvf::grcdsFloat3), // pos
		grcVertexElement(0, grcVertexElement::grcvetColor   , 0,  4, grcFvf::grcdsColor ), // colour
		grcVertexElement(0, grcVertexElement::grcvetTexture , 0, 16, grcFvf::grcdsFloat4), // texcoord
	};

	const grcVertexElement trailVtxElements_lit[] =
	{
		// ptxTrailVertex
		grcVertexElement(0, grcVertexElement::grcvetPosition, 0, 12, grcFvf::grcdsFloat3), // pos
		grcVertexElement(0, grcVertexElement::grcvetColor   , 0,  4, grcFvf::grcdsColor ), // colour
		grcVertexElement(0, grcVertexElement::grcvetTexture , 0, 16, grcFvf::grcdsFloat4), // texcoord
		grcVertexElement(0, grcVertexElement::grcvetTexture , 1, 16, grcFvf::grcdsFloat4), // tangent
	};

	const int strideOverride = 0;
	const int strideOverride_lit = 0;

#if GS_INSTANCED_SHADOWS
	sm_pSpriteVtxDeclDefault = GRCDEVICE.CreateVertexDeclaration(spriteVtxElements, NELEM(spriteVtxElements));
	sm_pSpriteVtxDeclInst = GRCDEVICE.CreateVertexDeclaration(spriteVtxElementsInst, NELEM(spriteVtxElementsInst));
	for(int i = 0; i < NELEM(sm_pSpriteVtxDecl); i++)
	{
		sm_pSpriteVtxDecl[i] = sm_pSpriteVtxDeclDefault;
	}
#else
	sm_pSpriteVtxDecl = GRCDEVICE.CreateVertexDeclaration(spriteVtxElements, NELEM(spriteVtxElements));
#endif
	sm_pTrailVtxDecl = GRCDEVICE.CreateVertexDeclaration(trailVtxElements, NELEM(trailVtxElements), strideOverride);
	sm_pTrailVtxDecl_lit = GRCDEVICE.CreateVertexDeclaration(trailVtxElements_lit, NELEM(trailVtxElements_lit), strideOverride_lit);

	SetupPosIdxTables();

#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
	// set up the vertex index table
	//const int numVertsPerIndexedSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED;
	//const int numIndicesPerIndexedSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_LORES;
	const u16 singleSpriteIndicesLoRes[PTXDRAW_NUM_VERTS_PER_SPRITE_LORES] = PTXDRAW_SPRITE_INDICES_LORES;

	int numIndexedSpritesLoRes = PTXDRAW_MAX_INDEXED_SPRITES_PER_BATCH_LORES;
	int numIndexedSpritesHiRes = PTXDRAW_MAX_INDEXED_SPRITES_PER_BATCH_HIRES;
	int numIndexedSpritesProjected = PTXDRAW_MAX_INDEXED_SPRITES_PROJ_PER_BATCH;
#if PTXDRAW_USE_INSTANCING
	numIndexedSpritesLoRes = 1;
	numIndexedSpritesHiRes = 1;
	numIndexedSpritesProjected = 1;
#endif

	for (int i=0; i<numIndexedSpritesLoRes; i++)
	{
		for (int j=0; j<PTXDRAW_NUM_VERTS_PER_SPRITE_LORES; j++)
		{
			sm_spriteVtxIndicesLoRes[i*PTXDRAW_NUM_VERTS_PER_SPRITE_LORES+j] = (u16)(singleSpriteIndicesLoRes[j]+(i*PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED));
		}
	}

	//const int numVertsPerIndexedSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED;
	//const int numIndicesPerIndexedSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES;
	const u16 singleSpriteIndicesHiRes[PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES] = PTXDRAW_SPRITE_INDICES_HIRES;

	for (int i=0; i<numIndexedSpritesHiRes; i++)
	{
		for (int j=0; j<PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES; j++)
		{
			sm_spriteVtxIndicesHiRes[i*PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES+j] = (u16)(singleSpriteIndicesHiRes[j]+(i*PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED));
		}
	}

	//const int numVertsPerIndexedSpriteProj = PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED;
	//const int numIndicesPerIndexedSpriteProj = PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED;
	const u16 singleSpriteProjIndices[PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED] = PTXDRAW_SPRITE_INDICES_PROJECTED;

	for (int i=0; i<numIndexedSpritesProjected; i++)
	{
		for (int j=0; j<PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED; j++)
		{
			sm_spriteProjVtxIndices[i*PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED+j] = (u16)(singleSpriteProjIndices[j]+(i*PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED));
		}
	}
#if PTXDRAW_SPRITE_USE_INDEX_BUFFER

#if RSG_DURANGO || RSG_ORBIS
	sm_pLoResIndexBuffer	= grcIndexBuffer::CreateWithData(PTXDRAW_LOW_RES_INDEXBUFFER_SIZE,	false, (void *)sm_spriteVtxIndicesLoRes);
	sm_pHiResIndexBuffer	= grcIndexBuffer::CreateWithData(PTXDRAW_HI_RES_INDEXBUFFER_SIZE,	false, (void *)sm_spriteVtxIndicesHiRes);
	sm_pProjIndexBuffer		= grcIndexBuffer::CreateWithData(PTXDRAW_PROJECTED_INDEXBUFFER_SIZE,false, (void *)sm_spriteProjVtxIndices);
#else // RSG_DURANGO || RSG_ORBIS
	sm_pLoResIndexBuffer	= grcIndexBuffer::CreateWithData(PTXDRAW_LOW_RES_INDEXBUFFER_SIZE,	grcsBufferCreate_NeitherReadNorWrite, (void *)sm_spriteVtxIndicesLoRes, false);
	sm_pHiResIndexBuffer	= grcIndexBuffer::CreateWithData(PTXDRAW_HI_RES_INDEXBUFFER_SIZE,	grcsBufferCreate_NeitherReadNorWrite, (void *)sm_spriteVtxIndicesHiRes, false);
	sm_pProjIndexBuffer		= grcIndexBuffer::CreateWithData(PTXDRAW_PROJECTED_INDEXBUFFER_SIZE,grcsBufferCreate_NeitherReadNorWrite, (void *)sm_spriteProjVtxIndices,	false);
#endif // RSG_DURANGO || RSG_ORBIS

#if PTXDRAW_USE_INSTANCING

	ptxSpriteVertex singleSpriteVertHiRes[PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED];

	for (int i=0; i<PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED; i++)
	{
		float xPosIndex = (float)sm_posIdxTableHiResIndexed[i*2];
		float yPosIndex = (float)sm_posIdxTableHiResIndexed[(i*2)+1];
		singleSpriteVertHiRes[i].vWorldPos = Vec4V(xPosIndex,yPosIndex,0.0f,0.0f);
		singleSpriteVertHiRes[i].vTexAUV_TexBUV_NormUV_distToCentre = Vec4V(V_ZERO);
	}

	ptxSpriteVertex singleSpriteVertLoRes[PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED];

	for (int i=0; i<PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED; i++)
	{
		float xPosIndex = (float)sm_posIdxTableLoResIndexed[i*2];
		float yPosIndex = (float)sm_posIdxTableLoResIndexed[(i*2)+1];
		singleSpriteVertLoRes[i].vWorldPos = Vec4V(xPosIndex,yPosIndex,0.0f,0.0f);
		singleSpriteVertLoRes[i].vTexAUV_TexBUV_NormUV_distToCentre = Vec4V(V_ZERO);
	}

	ptxSpriteVertex singleSpriteVertProj[PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED];

	for (int i=0; i<PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED; i++)
	{
		singleSpriteVertProj[i].vWorldPos = Vec4V(V_ZERO);
		singleSpriteVertProj[i].vTexAUV_TexBUV_NormUV_distToCentre = Vec4V(V_ZERO);
	}	
	
	grcFvf fvf0;
	fvf0.SetTextureChannel(3, true, grcFvf::grcdsFloat4);
	fvf0.SetTextureChannel(4, true, grcFvf::grcdsHalf4);
	fvf0.SetTextureChannel(5, true, grcFvf::grcdsHalf4);
	//fvf0.SetPosChannel(false);
#if RSG_PC
	sm_pHiResVertBuffer = grcVertexBuffer::CreateWithData(PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED, fvf0, grcsBufferCreate_NeitherReadNorWrite, (void *)singleSpriteVertHiRes, false);
	sm_pLoResVertBuffer = grcVertexBuffer::CreateWithData(PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED, fvf0, grcsBufferCreate_NeitherReadNorWrite, (void *)singleSpriteVertLoRes, false);
	sm_pProjVertBuffer = grcVertexBuffer::CreateWithData(PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED, fvf0, grcsBufferCreate_NeitherReadNorWrite, (void *)singleSpriteVertProj, false);
#else // RSG_PC
	sm_pHiResVertBuffer = grcVertexBuffer::CreateWithData(PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED, fvf0, (void *)singleSpriteVertHiRes);
	sm_pLoResVertBuffer = grcVertexBuffer::CreateWithData(PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED, fvf0, (void *)singleSpriteVertLoRes);
	sm_pProjVertBuffer = grcVertexBuffer::CreateWithData(PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED, fvf0, (void *)singleSpriteVertProj);
#endif // RSG_PC


#endif //PTXDRAW_USE_INSTANCING
#endif //PTXDRAW_SPRITE_USE_INDEX_BUFFER

#endif // PTXDRAW_SPRITE_USE_INDEXED_VERTS

#if PTXDRAW_TRAIL_USE_INDEX_BUFFERS
	u16 *pIndices = rage_new u16[PTXDRAW_TRAIL_TESS_X_MAX * ( PTXDRAW_TRAIL_TESS_Y_MAX * 2 + 4) * (PTXDRAW_TRAIL_MAX_CONTROL_POINTS-1)];
	CompileTimeAssert((PTXDRAW_TRAIL_TESS_X_MAX * ( PTXDRAW_TRAIL_TESS_Y_MAX * 2 + 4)) >= (PTXDRAW_TRAIL_TESS_X_MAX*2 + 2));
	for(int tessX=PTXDRAW_TRAIL_TESS_X_MIN; tessX<=PTXDRAW_TRAIL_TESS_X_MAX; tessX++)
	{
		for(int tessY=PTXDRAW_TRAIL_TESS_Y_MIN; tessY<=PTXDRAW_TRAIL_TESS_Y_MAX; tessY++)
		{
			sm_pTrailIndexBuffers[tessX - PTXDRAW_TRAIL_TESS_X_MIN][tessY - PTXDRAW_TRAIL_TESS_Y_MIN] = NULL;
			//skip when tessY is 1. We end up using non indexed buffers for this
			if(tessY>1)
			{
				const int numIndicesPerPatch = (tessY > 1) ? (tessX*(tessY*2 + 4)) : (tessX*2 + 2);
				const int numIndices = numIndicesPerPatch * (PTXDRAW_TRAIL_MAX_CONTROL_POINTS-1);

				DrawTrailTessellator_BuildIndices(pIndices, PTXDRAW_TRAIL_MAX_CONTROL_POINTS, tessX, tessY, numIndices);
#if RSG_DURANGO || RSG_ORBIS
				sm_pTrailIndexBuffers[tessX - PTXDRAW_TRAIL_TESS_X_MIN][tessY - PTXDRAW_TRAIL_TESS_Y_MIN] = (const grcIndexBuffer *) grcIndexBuffer::CreateWithData(numIndices, false, pIndices);
#else // RSG_DURANGO || RSG_ORBIS
				sm_pTrailIndexBuffers[tessX - PTXDRAW_TRAIL_TESS_X_MIN][tessY - PTXDRAW_TRAIL_TESS_Y_MIN] = (const grcIndexBuffer *) grcIndexBuffer::CreateWithData(numIndices, grcsBufferCreate_NeitherReadNorWrite, pIndices, false);
#endif // RSG_DURANGO	 || RSG_ORBIS
			}

		}
	}
	delete [] pIndices;
#endif

#endif // !__RESOURCECOMPILER
}

void ptxDrawInterface::Shutdown()
{
#if GS_INSTANCED_SHADOWS
	if (sm_pSpriteVtxDeclInst)
	{
		GRCDEVICE.DestroyVertexDeclaration(sm_pSpriteVtxDeclInst);
		sm_pSpriteVtxDeclInst = NULL;
	}
	if (sm_pSpriteVtxDeclDefault)
	{
		GRCDEVICE.DestroyVertexDeclaration(sm_pSpriteVtxDeclDefault);
		sm_pSpriteVtxDeclDefault = NULL;
	}
	sysMemSet(sm_pSpriteVtxDecl, 0, sizeof(sm_pSpriteVtxDecl));
#else
	if (sm_pSpriteVtxDecl)
	{
		GRCDEVICE.DestroyVertexDeclaration(sm_pSpriteVtxDecl);
		sm_pSpriteVtxDecl = NULL;
	}
#endif

	if (sm_pTrailVtxDecl)
	{
		GRCDEVICE.DestroyVertexDeclaration(sm_pTrailVtxDecl);
		sm_pTrailVtxDecl = NULL;
	}

	if (sm_pTrailVtxDecl_lit)
	{
		GRCDEVICE.DestroyVertexDeclaration(sm_pTrailVtxDecl_lit);
		sm_pTrailVtxDecl_lit = NULL;
	}

#if PTXDRAW_TRAIL_USE_INDEX_BUFFERS
	for(int i=PTXDRAW_TRAIL_TESS_X_MIN; i<=PTXDRAW_TRAIL_TESS_X_MAX; i++)
	{
		for(int j=PTXDRAW_TRAIL_TESS_Y_MIN; j<=PTXDRAW_TRAIL_TESS_Y_MAX; j++)
		{
			if(sm_pTrailIndexBuffers[i - PTXDRAW_TRAIL_TESS_X_MIN][j - PTXDRAW_TRAIL_TESS_Y_MIN])
			{
				delete sm_pTrailIndexBuffers[i - PTXDRAW_TRAIL_TESS_X_MIN][j - PTXDRAW_TRAIL_TESS_Y_MIN];;
				sm_pTrailIndexBuffers[i - PTXDRAW_TRAIL_TESS_X_MIN][j - PTXDRAW_TRAIL_TESS_Y_MIN] = NULL;
 			}
		}
	}
#endif

#if PTXDRAW_SPRITE_USE_INDEX_BUFFER
	if(sm_pLoResIndexBuffer)
	{
		delete sm_pLoResIndexBuffer;
		sm_pLoResIndexBuffer = NULL;
	}

	if(sm_pHiResIndexBuffer)
	{
		delete sm_pHiResIndexBuffer;
		sm_pHiResIndexBuffer = NULL;
	}

	if(sm_pProjIndexBuffer)
	{
		delete sm_pProjIndexBuffer;
		sm_pProjIndexBuffer = NULL;
	}

#if PTXDRAW_USE_INSTANCING
	if(sm_pLoResVertBuffer)
	{
		delete sm_pLoResVertBuffer;
		sm_pLoResVertBuffer = NULL;
	}

	if(sm_pHiResVertBuffer)
	{
		delete sm_pHiResVertBuffer;
		sm_pHiResVertBuffer = NULL;
	}

	if(sm_pProjVertBuffer)
	{
		delete sm_pProjVertBuffer;
		sm_pProjVertBuffer = NULL;
	}
#endif // PTXDRAW_USE_INSTANCING


#endif //PTXDRAW_SPRITE_USE_INDEX_BUFFER
}

void ptxDrawInterface::DeviceReset()
{
	Init();
}

void ptxDrawInterface::DeviceLost()
{
	Shutdown();
}

#if GS_INSTANCED_SHADOWS
void ptxDrawInterface::SetShadowPass(bool bEnable)
{
	// change sprite vtx decl based on shadow pass
#if PTXDRAW_USE_INSTANCING
	if (bEnable && grcViewport::GetInstancing())
		sm_pSpriteVtxDecl[g_RenderThreadIndex] = sm_pSpriteVtxDeclInst;
	else
#endif // PTXDRAW_USE_INSTANCING
		sm_pSpriteVtxDecl[g_RenderThreadIndex] = sm_pSpriteVtxDeclDefault;
}
#endif

#if !PTXDRAW_SPRITE_USE_INDEX_BUFFER
bool ptxDrawInterface::BeginSprites(int numSpritesToDraw, int numVertsPerSprite, bool PTXDRAW_SPRITE_USE_INDEXED_VERTS_ONLY(triStrip))
{
	int numVertsToDraw = numSpritesToDraw * numVertsPerSprite;
	const unsigned rti = g_RenderThreadIndex;
#if GS_INSTANCED_SHADOWS
	GRCDEVICE.SetVertexDeclaration(sm_pSpriteVtxDecl[rti]);
#else
	GRCDEVICE.SetVertexDeclaration(sm_pSpriteVtxDecl);
#endif
#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
	if (numVertsPerSprite==PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED)
	{
		int numIndicesPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES;
		int numIndices = numSpritesToDraw*numIndicesPerSprite;
		u16* pIndexData = NULL;
		GRCDEVICE.BeginIndexedVertices(
			s_DrawMode,
			numVertsToDraw,
			sizeof(ptxSpriteVtxCommon),
			numIndices,
			(void**)&pIndexData,
			(void**)(sm_pCurrCommon+rti));

		if (!pIndexData || !sm_pCurrCommon[rti])
		{
			return false;
		}
		memcpy(pIndexData, sm_spriteVtxIndicesHiRes, numIndices*sizeof(u16));
	}
	else if (triStrip && numVertsPerSprite == PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED)
	{
		int numIndicesPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED;
		int numIndices = numSpritesToDraw*numIndicesPerSprite;
		u16* pIndexData = NULL;
		GRCDEVICE.BeginIndexedVertices(
			drawTriStrip,
			numVertsToDraw,
			sizeof(ptxSpriteVtxCommon),
			numIndices,
			(void**)&pIndexData,
			(void**)(sm_pCurrCommon+rti));

		if (!pIndexData || !sm_pCurrCommon[rti])
		{
			return false;
		}
		memcpy(pIndexData, sm_spriteProjVtxIndices, numIndices*sizeof(u16));
	}
	else
#endif // PTXDRAW_SPRITE_USE_INDEXED_VERTS
	{
		sm_pCurrCommon[rti] = reinterpret_cast<ptxSpriteVtxCommon*>(GRCDEVICE.BeginVertices(s_DrawMode, numVertsToDraw, sizeof(ptxSpriteVtxCommon)));
	}
	return sm_pCurrCommon[rti]!=NULL;
}
 

void ptxDrawInterface::EndSprites(int numVertsPerSprite PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface)) 
{	
	(void)numVertsPerSprite;
#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH
	if (pMultipleDrawInterface)
	{
#if GS_INSTANCED_SHADOWS
		if (pMultipleDrawInterface->IsUsingInstancedShadow())
			GRCDEVICE.EndInstancedVertices(sm_pCurrCommon[rti],CASCADE_SHADOWS_COUNT);
		else
#endif
		{
			EndCreateSpritesInternal(numVertsPerSprite);
			for(u32 i=0; i<pMultipleDrawInterface->NoOfDraws(); i++)
			{
				if (pMultipleDrawInterface->GetCulledState(i) == false)
				{
					pMultipleDrawInterface->PreDraw(i);
					DrawSpritesInternal(numVertsPerSprite);
					pMultipleDrawInterface->PostDraw(i);
				}
			}
			ReleaseSpritesInternal(numVertsPerSprite);
		}
	}
	else
	{
		EndSpritesInternal(numVertsPerSprite);
	}
#else
	EndSpritesInternal(numVertsPerSprite);
#endif
}

void ptxDrawInterface::EndCreateSpritesInternal(int numVertsPerSprite)
{
	(void)numVertsPerSprite;
#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
	if (numVertsPerSprite==PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED ||
		numVertsPerSprite==PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED)
	{
		GRCDEVICE.EndCreateIndexedVertices(0,numVertexPerSprite);
	}
	else
#endif
	{
#if __D3D11 || RSG_ORBIS
		GRCDEVICE.EndCreateVertices(sm_pCurrCommon[rti]);
#endif // __D3D11 || RSG_ORBIS
	}
}

void ptxDrawInterface::DrawSpritesInternal(int numVertsPerSprite)
{
	(void)numVertsPerSprite;
#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
	if (numVertsPerSprite==PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED ||
		numVertsPerSprite==PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED)
	{
		GRCDEVICE.DrawIndexedVertices(0,numVertexPerSprite);
	}
	else
#endif
	{
#if __D3D11 || RSG_ORBIS
		GRCDEVICE.DrawVertices(sm_pCurrCommon[rti]);
#endif // __D3D11 || RSG_ORBIS
	}
}

void ptxDrawInterface::ReleaseSpritesInternal(int numVertsPerSprite)
{
	(void)numVertsPerSprite;
#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
	if (numVertsPerSprite==PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED ||
		numVertsPerSprite==PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED)
	{
		GRCDEVICE.ReleaseIndexedVertices(0,numVertexPerSprite);
	}
	else
#endif
	{
#if __D3D11 || RSG_ORBIS
		GRCDEVICE.ReleaseVertices(sm_pCurrCommon[rti]);
#endif // __D3D11 || RSG_ORBIS
	}
}

void ptxDrawInterface::EndSpritesInternal(int numVertsPerSprite)
{
	EndCreateSpritesInternal(numVertsPerSprite);
	DrawSpritesInternal(numVertsPerSprite);
	ReleaseSpritesInternal(numVertsPerSprite);
}

#elif PTXDRAW_USE_INSTANCING

bool ptxDrawInterface::BeginSpritesIndexInstancedBuffer(int numSpritesToDraw, bool triStrip)
{
	grcDrawMode drawMode = triStrip ? drawTriStrip : s_DrawMode;

	const unsigned rti = g_RenderThreadIndex;

#if GS_INSTANCED_SHADOWS
	GRCDEVICE.SetVertexDeclaration(sm_pSpriteVtxDecl[rti]);
#else
	GRCDEVICE.SetVertexDeclaration(sm_pSpriteVtxDecl);
#endif

	GRCDEVICE.BeginIndexedInstancedVertices(drawMode, numSpritesToDraw,sizeof(ptxSpriteInst), 0, (void**)(sm_pCurrInstance+rti), NULL, 1);
	return (sm_pCurrInstance[rti] != NULL);
}


void ptxDrawInterface::EndSpritesIndexInstancedBuffer(u32 numSpritesToDraw, u32 vertexSize, u32 numIndicesPerSprite, const grcIndexBuffer *pIndexBuffer, const grcVertexBuffer *pVertBuffer PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface))
{
	const unsigned rti = g_RenderThreadIndex;
#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH
	if(pMultipleDrawInterface)
	{
#if GS_INSTANCED_SHADOWS
		if (pMultipleDrawInterface->IsUsingInstanced())
		{
			pMultipleDrawInterface->PreDrawInstanced();
			GRCDEVICE.EndIndexedInstancedVertices(sm_pCurrInstance[rti], vertexSize, numIndicesPerSprite, *pVertBuffer, *pIndexBuffer, numSpritesToDraw * pMultipleDrawInterface->GetNumActiveCascades(), 0, 0, 0);
			pMultipleDrawInterface->PostDrawInstanced();
		}
		else
#endif
		{
			GRCDEVICE.EndCreateIndexedInstancedVertices(sm_pCurrInstance[rti], vertexSize, numIndicesPerSprite, *pVertBuffer, *pIndexBuffer, numSpritesToDraw, 0, 0, 0);
			grcDrawMode drawMode = s_DrawMode;
			bool firstDraw = true;
			for(u32 i=0; i<pMultipleDrawInterface->NoOfDraws(); i++)
			{
				if (pMultipleDrawInterface->GetCulledState(i) == false)
				{
					pMultipleDrawInterface->PreDraw(i);
					if(firstDraw)
					{
						GRCDEVICE.DrawIndexedInstancedVertices(sm_pCurrInstance[rti], vertexSize, numIndicesPerSprite, *pVertBuffer, *pIndexBuffer, numSpritesToDraw, 0, 0, 0);
						firstDraw = false;
					}
					else
						GRCDEVICE.DrawInstancedIndexedPrimitive(drawMode, numIndicesPerSprite, numSpritesToDraw, 0, 0, false);	//careful with the last param
					pMultipleDrawInterface->PostDraw(i);
				}
			}
			GRCDEVICE.ReleaseIndexedInstancedVertices(sm_pCurrInstance[rti], vertexSize, numIndicesPerSprite, *pVertBuffer, *pIndexBuffer, numSpritesToDraw, 0, 0, 0);
		}
	}
	else
	{
		GRCDEVICE.EndIndexedInstancedVertices(sm_pCurrInstance[rti], vertexSize, numIndicesPerSprite, *pVertBuffer, *pIndexBuffer, numSpritesToDraw, 0, 0, 0);
	}
#else
	GRCDEVICE.EndIndexedInstancedVertices(sm_pCurrInstance[rti], vertexSize, numIndicesPerSprite, *pVertBuffer, *pIndexBuffer, numSpritesToDraw, 0, 0, 0);
#endif
}

#else // PTXDRAW_SPRITE_USE_INDEX_BUFFER

bool ptxDrawInterface::BeginSpritesIndexBuffer(int numSpritesToDraw, int numVertsPerSprite, bool triStrip)
{
	grcDrawMode drawMode = s_DrawMode;

	int numVertsToDraw = numSpritesToDraw * numVertsPerSprite;

	const unsigned rti = g_RenderThreadIndex;

#if GS_INSTANCED_SHADOWS
	GRCDEVICE.SetVertexDeclaration(sm_pSpriteVtxDecl[rti]);
#else
	GRCDEVICE.SetVertexDeclaration(sm_pSpriteVtxDecl);
#endif

	if(triStrip)
	{
		drawMode = drawTriStrip;
	}
	sm_pCurrCommon[rti] = reinterpret_cast<ptxSpriteVtxCommon*>(GRCDEVICE.BeginVertices(drawMode, numVertsToDraw, sizeof(ptxSpriteVtxCommon)));
	return (sm_pCurrCommon[rti] != NULL);
}


void ptxDrawInterface::EndSpritesIndexBuffer(int numSpritesToDraw, int numIndicesPerSprite, const grcIndexBuffer *pIndexBuffer PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface))
{
#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH
	if(pMultipleDrawInterface)
	{
		u32 i;
		grcDrawMode drawMode = s_DrawMode;

		GRCDEVICE.EndCreateVertices(sm_pCurrCommon[rti], (u32)0, *pIndexBuffer);
		GRCDEVICE.DrawVertices(sm_pCurrCommon[rti], (u32)0, *pIndexBuffer);

		for(i=0; i<pMultipleDrawInterface->NoOfDraws(); i++)
		{
			pMultipleDrawInterface->PreDraw(i);
			GRCDEVICE.DrawIndexedPrimitive(drawMode, 0, (numSpritesToDraw*numIndicesPerSprite));
			pMultipleDrawInterface->PostDraw(i);
		}
#if __D3D11 || RSG_ORBIS
		GRCDEVICE.ReleaseVertices(sm_pCurrCommon[rti], (u32)0, *pIndexBuffer);
#endif // __D3D11 || RSG_ORBIS
	}
	else
	{
		GRCDEVICE.EndVertices(sm_pCurrCommon[rti], (u32)(numSpritesToDraw*numIndicesPerSprite), *pIndexBuffer);
	}
#else
	GRCDEVICE.EndVertices(sm_pCurrCommon[rti], (u32)(numSpritesToDraw*numIndicesPerSprite), *pIndexBuffer);
#endif
}
#endif // PTXDRAW_SPRITE_USE_INDEX_BUFFER

void ptxDrawInterface::SetupPosIdxTables() 
{
	// lo res
	Vec2V pointsLoRes[4] = PTXDRAW_SPRITE_VTX_OFFSETS_LORES;
	int indicesLoRes[PTXDRAW_NUM_VERTS_PER_SPRITE_LORES] = PTXDRAW_SPRITE_INDICES_LORES;

	int currIdx = 0;
	for (int i=0; i<PTXDRAW_NUM_VERTS_PER_SPRITE_LORES; i++)
	{
		Vec2V point = pointsLoRes[indicesLoRes[i]];
		sm_posIdxTableLoRes[currIdx++] = point.GetXf()<0.0f ? 0 : point.GetXf()==0.0f ? 1 : 2;
		sm_posIdxTableLoRes[currIdx++] = point.GetYf()<0.0f ? 0 : point.GetYf()==0.0f ? 1 : 2;
	}

	// hi res
	Vec2V pointsHiRes[9] = PTXDRAW_SPRITE_VTX_OFFSETS_HIRES;
	int indicesHiRes[PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES] = PTXDRAW_SPRITE_INDICES_HIRES;

	currIdx = 0;
	for (int i=0; i<PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES; i++)
	{
		Vec2V point = pointsHiRes[indicesHiRes[i]];
		sm_posIdxTableHiRes[currIdx++] = point.GetXf()<0.0f ? 0 : point.GetXf()==0.0f ? 1 : 2;
		sm_posIdxTableHiRes[currIdx++] = point.GetYf()<0.0f ? 0 : point.GetYf()==0.0f ? 1 : 2;
	}

#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
	// hi res indexed
	currIdx = 0;
	for (int i=0; i<PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED; i++)
	{
		Vec2V point = pointsHiRes[i];
		sm_posIdxTableHiResIndexed[currIdx++] = point.GetXf()<0.0f ? 0 : point.GetXf()==0.0f ? 1 : 2;
		sm_posIdxTableHiResIndexed[currIdx++] = point.GetYf()<0.0f ? 0 : point.GetYf()==0.0f ? 1 : 2;
	}

	currIdx = 0;
	for (int i=0; i<PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED; i++)
	{
		Vec2V point = pointsLoRes[i];
		sm_posIdxTableLoResIndexed[currIdx++] = point.GetXf()<0.0f ? 0 : point.GetXf()==0.0f ? 1 : 2;
		sm_posIdxTableLoResIndexed[currIdx++] = point.GetYf()<0.0f ? 0 : point.GetYf()==0.0f ? 1 : 2;
	}
	
#endif
}

void ptxDrawInterface::DrawSprites(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxPointItem** ppPoints, unsigned numPoints, ptxd_Sprite* pDrawSpriteBehaviour, ptxParticleRule* pParticleRule, ptxEvolutionList* UNUSED_PARAM(pEvoList), bool isDirectional, bool isHiRes, bool isProjected PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface))
{
#if !PTXDRAW_SPRITE_USE_INDEX_BUFFER && PTXDRAW_MULTIPLE_DRAWS_PER_BATCH
	(void) pMultipleDrawInterface;
#endif
	if (pParticleRule->GetShaderInst().Begin())
	{
		const Vec4V* pvClipRegionData = NULL;
		int numTexColumns;
		int numTexRows;
		u32 textureHashName = pParticleRule->GetTextureHashNameForClipRegions();

		ASSERT_ONLY(bool regionOk = )ptxClipRegions::GetData(textureHashName, pvClipRegionData, numTexColumns, numTexRows);
		ptxAssertf(regionOk == true,"Couldn't find clip region for texture %s in rule %s",pParticleRule->GetTextureNameForClipRegions(),pParticleRule->GetName());

#if PTXDRAW_USE_ALPHA_CULL 
		const float alphaCullThreshold = RMPTFX_BANK_SWITCH(ptxDebug::sm_pointAlphaCullThreshold, PTXDRAW_ALPHA_CULL_THRESHOLD);
#endif

		// calc the view axis info
		bool alignToAxis = pDrawSpriteBehaviour->m_alignmentMode==PTXD_SPRITE_ALIGNMENTMODE_WORLD || pDrawSpriteBehaviour->m_alignmentMode==PTXD_SPRITE_ALIGNMENTMODE_EFFECT || pDrawSpriteBehaviour->m_alignmentMode==PTXD_SPRITE_ALIGNMENTMODE_EMITTER;
		Vec3V vAlignAxis = Normalize(pDrawSpriteBehaviour->m_alignAxis);

		if (pDrawSpriteBehaviour->m_alignmentMode==PTXD_SPRITE_ALIGNMENTMODE_EMITTER) 
		{
			Mat34V vEmitterMtxWld = pEmitterInst->GetCreationDomainMtxWld(pEffectInst->GetMatrix());
			vAlignAxis = Transform3x3(vEmitterMtxWld, vAlignAxis);
		} 
		else if (pDrawSpriteBehaviour->m_alignmentMode==PTXD_SPRITE_ALIGNMENTMODE_EFFECT) 
		{
			vAlignAxis = Transform3x3(pEffectInst->GetMatrix(), vAlignAxis);
		}

		// init the draw inst params 
		ptxDrawSpriteInstParams drawInstParams;

		// calc how many verts we need to draw a single particle sprite
	#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
		drawInstParams.numVertsPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED;
		drawInstParams.pPosIdxTable = sm_posIdxTableLoRes;
	#if PTXDRAW_SPRITE_USE_INDEX_BUFFER
		drawInstParams.pPosIdxTable = sm_posIdxTableLoResIndexed;
		int numIndicesPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_LORES;
		grcIndexBuffer *pIndexBuffer = sm_pLoResIndexBuffer;
	#endif
	#else
		drawInstParams.numVertsPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_LORES;
		drawInstParams.pPosIdxTable = sm_posIdxTableLoRes;
	#endif

		if (isHiRes)
		{
		#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
			drawInstParams.numVertsPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED;
			drawInstParams.pPosIdxTable = sm_posIdxTableHiResIndexed;
		#if PTXDRAW_SPRITE_USE_INDEX_BUFFER
			numIndicesPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES;
			pIndexBuffer = sm_pHiResIndexBuffer;
		#endif
		#else
			drawInstParams.numVertsPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES;
			drawInstParams.pPosIdxTable = sm_posIdxTableHiRes;
		#endif
		}
		if (isProjected)
		{
		#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
			drawInstParams.numVertsPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED;
		#if PTXDRAW_SPRITE_USE_INDEX_BUFFER
			numIndicesPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED;
			pIndexBuffer = sm_pProjIndexBuffer;
		#endif
		#else
			drawInstParams.numVertsPerSprite = PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED;
		#endif
			drawInstParams.pPosIdxTable = NULL;
		}

		drawInstParams.vAlignAxis = vAlignAxis;
		drawInstParams.nearClipDist = pDrawSpriteBehaviour->m_nearClipDist;
		drawInstParams.farClipDist = pDrawSpriteBehaviour->m_farClipDist;
		drawInstParams.projectionDepth = pDrawSpriteBehaviour->m_projectionDepth;

		drawInstParams.isDirectional = isDirectional;
		drawInstParams.isScreenSpace = pDrawSpriteBehaviour->m_isScreenSpace;
		drawInstParams.isHiRes = isHiRes;
		drawInstParams.isProjected = isProjected;
		drawInstParams.alignToAxis = alignToAxis;
		drawInstParams.nearClip = pDrawSpriteBehaviour->m_nearClip;
		drawInstParams.farClip = pDrawSpriteBehaviour->m_farClip;
		drawInstParams.uvClip = pDrawSpriteBehaviour->m_uvClip;

		drawInstParams.vCamMtx = grcViewport::GetCurrent()->GetCameraMtx();
		drawInstParams.vUpWld = Vec3V(V_Y_AXIS_WZERO);
		drawInstParams.vAspectRatio = Vec3V(grcViewport::GetCurrent()->GetProjection().GetCol0().GetX(), ScalarV(V_ONE), grcViewport::GetCurrent()->GetProjection().GetCol1().GetY());
		drawInstParams.pvClipRegionData = pvClipRegionData;	
		drawInstParams.numTexRows = numTexRows;
		drawInstParams.numTexColumns = numTexColumns;
		drawInstParams.alphaMult = 1.0f;

#if RMPTFX_USE_PARTICLE_SHADOWS
		//if we're drawing the shadows, adjust the alpha based on the shadow cast intensity
		if (RMPTFXMGR.IsShadowPass())
		{
			drawInstParams.alphaMult *= pDrawSpriteBehaviour->m_shadowCastIntensity * RMPTFXMGR.GetGlobalShadowIntensity();
		}
#endif

		float overriddenFarClipDist = pEffectInst->GetOverrideFarClipDist();
		if (overriddenFarClipDist>-1.0f)
		{
			drawInstParams.farClipDist = overriddenFarClipDist;
			drawInstParams.farClip = true;
		}

		int numVertsPerSprite = drawInstParams.numVertsPerSprite;

		// init the counters
		int maxSpritesPerBatch = GPU_VERTICES_MAX_SIZE/numVertsPerSprite/sizeof(ptxSpriteVtxCommon);
		int numSpritesLeftToDraw = numPoints;
		int numSpritesToDraw = Min(numSpritesLeftToDraw, maxSpritesPerBatch);
		int numBatchedSprites = 0;

#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH && GS_INSTANCED_SHADOWS
		// move this here since ps4 needs this before vp binding
		if ((pMultipleDrawInterface) && pMultipleDrawInterface->IsUsingInstanced())
			grcViewport::RegenerateInstVPMatrices(Mat44V(V_IDENTITY));
#endif
		// render the points
		for (unsigned i=0; i<numPoints; ++i)
		{
			if (numBatchedSprites==0)
			{
			#if !PTXDRAW_SPRITE_USE_INDEX_BUFFER
				BeginSprites(numSpritesToDraw, numVertsPerSprite, isProjected);
			#elif PTXDRAW_USE_INSTANCING
				BeginSpritesIndexInstancedBuffer(numSpritesToDraw, isProjected);
			#else
				BeginSpritesIndexBuffer(numSpritesToDraw, numVertsPerSprite, isProjected);
			#endif
			}

			ptxPointItem* pCurrPointItem = ppPoints[i];

			ptxCustomVars customVars;
			ptxPointDB* pPointDB = pCurrPointItem->GetDataDBMT(RMPTFXTHREADER.GetDrawBufferId());
			pParticleRule->GetShaderInst().SetKeyframeVars(customVars, pPointDB->GetKeyTime());

			// set up the draw params vertex data 
			ptxDrawSpriteParams drawSpriteParams;
			drawSpriteParams.pPointSB = pCurrPointItem->GetDataSB();
			drawSpriteParams.pPointDB = pPointDB;
			drawSpriteParams.pCustomVars = &customVars;

#if PTXDRAW_USE_INSTANCING		
			grcVertexBuffer* pVertBuffer = sm_pLoResVertBuffer;
			if( isProjected )
				pVertBuffer = sm_pProjVertBuffer;
			else if (isHiRes)
				pVertBuffer = sm_pHiResVertBuffer;

			drawSpriteParams.ppCurrInst = sm_pCurrInstance+g_RenderThreadIndex;
#else
			drawSpriteParams.ppCurrCommon = sm_pCurrCommon+g_RenderThreadIndex;
#endif

			drawSpriteParams.bDrawDegenerateWhenAlphaCulled = false;
#if PTXDRAW_USE_ALPHA_CULL
			if (Unlikely(pPointDB->GetColour().GetAlphaf() < alphaCullThreshold))
			{
				drawSpriteParams.bDrawDegenerateWhenAlphaCulled = true;
			}
#endif 

			float randU = ptxRandomTable::GetValue(drawSpriteParams.pPointSB->GetRandIndex());
			float randV = randU*10.0f;
			randV -= (int)randV;
			drawSpriteParams.flipU = randU<=pDrawSpriteBehaviour->m_flipChanceU;
			drawSpriteParams.flipV = randV<=pDrawSpriteBehaviour->m_flipChanceV;

			ptxDrawInterface::BatchSprite(drawInstParams, drawSpriteParams);
			numBatchedSprites++;

			if (numBatchedSprites==numSpritesToDraw)
			{
			#if !PTXDRAW_SPRITE_USE_INDEX_BUFFER
				EndSprites(numVertsPerSprite
					PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
			#elif PTXDRAW_USE_INSTANCING
				u32 vertexSize = sizeof(ptxSpriteVertex);
				EndSpritesIndexInstancedBuffer(numSpritesToDraw, vertexSize, numIndicesPerSprite, pIndexBuffer, pVertBuffer PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
			#else
				EndSpritesIndexBuffer(numSpritesToDraw, numIndicesPerSprite, pIndexBuffer PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
			#endif
				numSpritesLeftToDraw -= numSpritesToDraw;
				numSpritesToDraw = Min(numSpritesLeftToDraw, maxSpritesPerBatch);
				numBatchedSprites = 0;
			}
		}

		pParticleRule->GetShaderInst().End();
	}
}

class ptxTrailControlPoint
{
public:
	Vec4V m_vPosA_XYZ_texCoordX_W;
	Vec4V m_vPosB_XYZ_texCoordX_W;
	Vec4V m_vColour;
	Vec3V m_vTangent; // w is unused
};

static __forceinline void DrawTrailGetPointData(const ptxPointItem*& pCurrPointItemEA, Vec3V_InOut vPos, Vec4V_InOut vColour, ScalarV_InOut vSize, ScalarV_InOut vLifeRatio, bool& isBroken, int drawBufferId)
{
	const ptxPointDB* pPointDB = pCurrPointItemEA->GetDataDBMT(drawBufferId); 

	vPos = pPointDB->GetCurrPos();
	vColour = pPointDB->GetColour().GetRGBA();
	vSize = pPointDB->GetDimensions().GetX();
	vLifeRatio = ScalarV(pPointDB->GetLifeRatio());

	isBroken = pPointDB->GetIsBrokenTrail();

	pCurrPointItemEA = (const ptxPointItem*)pCurrPointItemEA->GetNextMT(drawBufferId); // now it's EA again
}

static __forceinline Vec3V_Out DrawTrailCalculateTangent(const ptxTrailControlPoint* pControlPoints, int index, int numPoints)
{
	Assert(index >= 0 && index < numPoints);

#if RMPTFX_BANK
	if (ptxDebug::sm_debugTrailTangentColours)
	{
		const Vec3V test[] = { Vec3V(V_X_AXIS_WZERO), Vec3V(V_Y_AXIS_WZERO), Vec3V(V_Z_AXIS_WZERO), -Vec3V(V_X_AXIS_WZERO), -Vec3V(V_Y_AXIS_WZERO), -Vec3V(V_Z_AXIS_WZERO) };
		return test[(index/ptxDebug::sm_debugTrailTangentSteps)%NELEM(test)];
	}
#endif

	const ptxTrailControlPoint& pA = pControlPoints[Max<int>(0, index - 1)];
	const ptxTrailControlPoint& pB = pControlPoints[Min<int>(index + 1, numPoints - 1)];

	const Vec4V vA = pA.m_vPosA_XYZ_texCoordX_W + pA.m_vPosB_XYZ_texCoordX_W;
	const Vec4V vB = pB.m_vPosA_XYZ_texCoordX_W + pB.m_vPosB_XYZ_texCoordX_W;

	return NormalizeSafe((vB - vA).GetXYZ(), Vec3V(V_X_AXIS_WZERO));
}

static int DrawTrailBuildControlPoints(ptxTrailControlPoint* pControlPoints, Vec3V_In vAlignAxis_In, Vec3V_In vCamPos, Vec3V_In vAnchorPosIn, ScalarV_In vAnchorSizeIn, Vec4V_In vAnchorColIn, ScalarV_In vTexWrapLength, ScalarV_In vTexOffset, const ptxList* pPointList, ptxd_Trail* pDrawTrailBehaviour, int drawBufferId)
{
	Vec3V vAnchorPos = vAnchorPosIn;
	Vec4V vAnchorCol = vAnchorColIn;
	ScalarV vAnchorSize = vAnchorSizeIn;

#if RMPTFX_BANK
	if (ptxDebug::sm_numDebugTrailPoints>0)
	{
		vAnchorPos = ptxDebug::sm_debugTrailPoints[0].vPosition;
		vAnchorCol = ptxDebug::sm_debugTrailPoints[0].vColour;
		vAnchorSize = ptxDebug::sm_debugTrailPoints[0].vSize;
	}
#endif

	Vec3V vAlignAxis = Normalize(vAlignAxis_In);
	if (pDrawTrailBehaviour->m_flipV)
	{
		vAlignAxis = -vAlignAxis;
	}

	const int numControlPoints = Min<int>(PTXDRAW_TRAIL_MAX_CONTROL_POINTS, 1 + pPointList->GetNumItemsMT(drawBufferId));
	const ptxPointItem* pCurrPointItemEA = (const ptxPointItem*)pPointList->GetHeadMT(drawBufferId);

	bool isCurrBroken = false;
	bool isNextBroken = false;

	Vec3V vPrevPos = vAnchorPos;
	Vec3V vCurrPos = vAnchorPos;
	Vec3V vNextPos = Vec3V(V_ZERO);

	Vec4V vCurrCol = vAnchorCol;
	Vec4V vNextCol = Vec4V(V_ZERO);

	ScalarV vCurrSize = vAnchorSize;
	ScalarV vNextSize = ScalarV(V_ZERO);

	ScalarV vNextLifeRatio = ScalarV(V_ZERO);

	ScalarV vCurrTexCoordX = vTexOffset;

	const ScalarV vTexCoordSign = pDrawTrailBehaviour->m_flipU ? ScalarV(V_80000000) : ScalarV(V_ZERO);

	// calc the control point data
	for (int i=0; i<numControlPoints-1; i++)
	{
		// get the point data at the end of this patch
		DrawTrailGetPointData(pCurrPointItemEA, vNextPos, vNextCol, vNextSize, vNextLifeRatio, isNextBroken, drawBufferId);

#if RMPTFX_BANK
		if (ptxDebug::sm_numDebugTrailPoints>1 && i<ptxDebug::sm_numDebugTrailPoints-1)
		{
			vNextPos = ptxDebug::sm_debugTrailPoints[i+1].vPosition;
			vNextCol = ptxDebug::sm_debugTrailPoints[i+1].vColour;
			vNextSize = ptxDebug::sm_debugTrailPoints[i+1].vSize;
			vNextLifeRatio = ptxDebug::sm_debugTrailPoints[i+1].vLifeRatio;
		}
#endif

		if (pDrawTrailBehaviour->m_alignmentMode==PTXD_TRAIL_ALIGNMENTMODE_CAMERA)
		{
			if (pDrawTrailBehaviour->m_flipV)
			{
				vAlignAxis = Normalize(vCamPos-vCurrPos);
			}
			else
			{

				vAlignAxis = Normalize(vCurrPos-vCamPos);
			}
		}

		// calc the current control point data
		const Vec3V vSide = Normalize(Cross(vNextPos-vPrevPos, vAlignAxis))*vCurrSize;
		pControlPoints[i].m_vPosA_XYZ_texCoordX_W = Vec4V(vCurrPos - vSide, vCurrTexCoordX^vTexCoordSign);
		pControlPoints[i].m_vPosB_XYZ_texCoordX_W = Vec4V(vCurrPos + vSide, vCurrTexCoordX^vTexCoordSign);

#if RMPTFX_USE_PARTICLE_SHADOWS
		if(RMPTFXMGR.IsShadowPass())
		{
			vCurrCol.SetW(vCurrCol.GetW() * ScalarVFromF32(pDrawTrailBehaviour->m_shadowCastIntensity * RMPTFXMGR.GetGlobalShadowIntensity()));
		}
#endif
		pControlPoints[i].m_vColour = vCurrCol;

		if (isCurrBroken || isNextBroken)
		{
			pControlPoints[i].m_vColour = Vec4V(V_ZERO);
		}

		// update texture coordinate
		if (pDrawTrailBehaviour->m_wrapTextureOverParticleLife)
		{
			// wrap n times over the particle life
			vCurrTexCoordX = AddScaled(vTexOffset, vNextLifeRatio, vTexWrapLength);
		}
		else
		{
			// wrap once each n metres
			ScalarV vLength = MagFast(vNextPos - vCurrPos);
			vCurrTexCoordX += vLength/vTexWrapLength;
		}

		// buffer data for next iteration
		vPrevPos = vCurrPos; 
		vCurrPos = vNextPos;
		vCurrCol = vNextCol;
		vCurrSize = vNextSize;
		isCurrBroken = isNextBroken;
	}

	// calc the last control point data
	const Vec3V vSide = Normalize(Cross(vCurrPos - vPrevPos, vAlignAxis))*vCurrSize;
	pControlPoints[numControlPoints-1].m_vPosA_XYZ_texCoordX_W = Vec4V(vCurrPos - vSide, vCurrTexCoordX^vTexCoordSign);
	pControlPoints[numControlPoints-1].m_vPosB_XYZ_texCoordX_W = Vec4V(vCurrPos + vSide, vCurrTexCoordX^vTexCoordSign);
	pControlPoints[numControlPoints-1].m_vColour = vCurrCol;

	if (isCurrBroken)
	{
		pControlPoints[numControlPoints-1].m_vColour = Vec4V(V_ZERO);
	}

	for (int i=0; i<numControlPoints; i++)
	{
		pControlPoints[i].m_vTangent = DrawTrailCalculateTangent(pControlPoints, i, numControlPoints);
	}

	return numControlPoints;
}

static __forceinline const Vec4V* ptxTrailGetCubicSplineCoefficients_Position(int tessX)
{
	static const Vec4V kCoefficients_Position_1[1*4] =
	{
		Vec4V(-8.f/125.f, 114.f/125.f,  21.f/125.f, -2.f/125.f), // @ x = 1/5
		Vec4V(-9.f/125.f,  87.f/125.f,  53.f/125.f, -6.f/125.f), // @ x = 2/5
		Vec4V(-6.f/125.f,  53.f/125.f,  87.f/125.f, -9.f/125.f), // @ x = 3/5
		Vec4V(-2.f/125.f,  21.f/125.f, 114.f/125.f, -8.f/125.f), // @ x = 4/5
	};
	static const Vec4V kCoefficients_Position_2[2*4] =
	{
		Vec4V(-32.f/729.f, 708.f/729.f,  57.f/729.f,  -4.f/729.f), // @ x = 1/9
		Vec4V(-49.f/729.f, 651.f/729.f, 141.f/729.f, -14.f/729.f), // @ x = 2/9
		Vec4V(-54.f/729.f, 567.f/729.f, 243.f/729.f, -27.f/729.f), // @ x = 3/9
		Vec4V(-50.f/729.f, 465.f/729.f, 354.f/729.f, -40.f/729.f), // @ x = 4/9
		Vec4V(-40.f/729.f, 354.f/729.f, 465.f/729.f, -50.f/729.f), // @ x = 5/9
		Vec4V(-27.f/729.f, 243.f/729.f, 567.f/729.f, -54.f/729.f), // @ x = 6/9
		Vec4V(-14.f/729.f, 141.f/729.f, 651.f/729.f, -49.f/729.f), // @ x = 7/9
		Vec4V( -4.f/729.f,  57.f/729.f, 708.f/729.f, -32.f/729.f), // @ x = 8/9
	};
	static const Vec4V kCoefficients_Position_3[3*4] =
	{
		Vec4V( -72.f/2197.f, 2166.f/2197.f,  109.f/2197.f,   -6.f/2197.f), // @ x = 1/13
		Vec4V(-121.f/2197.f, 2079.f/2197.f,  261.f/2197.f,  -22.f/2197.f), // @ x = 2/13
		Vec4V(-150.f/2197.f, 1945.f/2197.f,  447.f/2197.f,  -45.f/2197.f), // @ x = 3/13
		Vec4V(-162.f/2197.f, 1773.f/2197.f,  658.f/2197.f,  -72.f/2197.f), // @ x = 4/13
		Vec4V(-160.f/2197.f, 1572.f/2197.f,  885.f/2197.f, -100.f/2197.f), // @ x = 5/13
		Vec4V(-147.f/2197.f, 1351.f/2197.f, 1119.f/2197.f, -126.f/2197.f), // @ x = 6/13
		Vec4V(-126.f/2197.f, 1119.f/2197.f, 1351.f/2197.f, -147.f/2197.f), // @ x = 7/13
		Vec4V(-100.f/2197.f,  885.f/2197.f, 1572.f/2197.f, -160.f/2197.f), // @ x = 8/13
		Vec4V( -72.f/2197.f,  658.f/2197.f, 1773.f/2197.f, -162.f/2197.f), // @ x = 9/13
		Vec4V( -45.f/2197.f,  447.f/2197.f, 1945.f/2197.f, -150.f/2197.f), // @ x = 10/13
		Vec4V( -22.f/2197.f,  261.f/2197.f, 2079.f/2197.f, -121.f/2197.f), // @ x = 11/13
		Vec4V(  -6.f/2197.f,  109.f/2197.f, 2166.f/2197.f,  -72.f/2197.f), // @ x = 12/13
	};
	static const Vec4V kCoefficients_Position_4[4*4] =
	{
		Vec4V(-128.f/4913.f, 4872.f/4913.f,  177.f/4913.f,   -8.f/4913.f), // @ x = 1/17
		Vec4V(-225.f/4913.f, 4755.f/4913.f,  413.f/4913.f,  -30.f/4913.f), // @ x = 2/17
		Vec4V(-294.f/4913.f, 4571.f/4913.f,  699.f/4913.f,  -63.f/4913.f), // @ x = 3/17
		Vec4V(-338.f/4913.f, 4329.f/4913.f, 1026.f/4913.f, -104.f/4913.f), // @ x = 4/17
		Vec4V(-360.f/4913.f, 4038.f/4913.f, 1385.f/4913.f, -150.f/4913.f), // @ x = 5/17
		Vec4V(-363.f/4913.f, 3707.f/4913.f, 1767.f/4913.f, -198.f/4913.f), // @ x = 6/17
		Vec4V(-350.f/4913.f, 3345.f/4913.f, 2163.f/4913.f, -245.f/4913.f), // @ x = 7/17
		Vec4V(-324.f/4913.f, 2961.f/4913.f, 2564.f/4913.f, -288.f/4913.f), // @ x = 8/17
		Vec4V(-288.f/4913.f, 2564.f/4913.f, 2961.f/4913.f, -324.f/4913.f), // @ x = 9/17
		Vec4V(-245.f/4913.f, 2163.f/4913.f, 3345.f/4913.f, -350.f/4913.f), // @ x = 10/17
		Vec4V(-198.f/4913.f, 1767.f/4913.f, 3707.f/4913.f, -363.f/4913.f), // @ x = 11/17
		Vec4V(-150.f/4913.f, 1385.f/4913.f, 4038.f/4913.f, -360.f/4913.f), // @ x = 12/17
		Vec4V(-104.f/4913.f, 1026.f/4913.f, 4329.f/4913.f, -338.f/4913.f), // @ x = 13/17
		Vec4V( -63.f/4913.f,  699.f/4913.f, 4571.f/4913.f, -294.f/4913.f), // @ x = 14/17
		Vec4V( -30.f/4913.f,  413.f/4913.f, 4755.f/4913.f, -225.f/4913.f), // @ x = 15/17
		Vec4V(  -8.f/4913.f,  177.f/4913.f, 4872.f/4913.f, -128.f/4913.f), // @ x = 16/17
	};

	/*static const Vec4V kCoefficients_Position_4_alt[4*4] =
	{
		Vec4V(4096.f/29478.f, 19553.f/29478.f,  5828.f/29478.f,    1.f/29478.f), // @ x = 1/17
		Vec4V(3375.f/29478.f, 19268.f/29478.f,  6827.f/29478.f,    8.f/29478.f), // @ x = 2/17
		Vec4V(2744.f/29478.f, 18815.f/29478.f,  7892.f/29478.f,   27.f/29478.f), // @ x = 3/17
		Vec4V(2197.f/29478.f, 18212.f/29478.f,  9005.f/29478.f,   64.f/29478.f), // @ x = 4/17
		Vec4V(1728.f/29478.f, 17477.f/29478.f, 10148.f/29478.f,  125.f/29478.f), // @ x = 5/17
		Vec4V(1331.f/29478.f, 16628.f/29478.f, 11303.f/29478.f,  216.f/29478.f), // @ x = 6/17
		Vec4V(1000.f/29478.f, 15683.f/29478.f, 12452.f/29478.f,  343.f/29478.f), // @ x = 7/17
		Vec4V( 729.f/29478.f, 14660.f/29478.f, 13577.f/29478.f,  512.f/29478.f), // @ x = 8/17
		Vec4V( 512.f/29478.f, 13577.f/29478.f, 14660.f/29478.f,  729.f/29478.f), // @ x = 9/17
		Vec4V( 343.f/29478.f, 12452.f/29478.f, 15683.f/29478.f, 1000.f/29478.f), // @ x = 10/17
		Vec4V( 216.f/29478.f, 11303.f/29478.f, 16628.f/29478.f, 1331.f/29478.f), // @ x = 11/17
		Vec4V( 125.f/29478.f, 10148.f/29478.f, 17477.f/29478.f, 1728.f/29478.f), // @ x = 12/17
		Vec4V(  64.f/29478.f,  9005.f/29478.f, 18212.f/29478.f, 2197.f/29478.f), // @ x = 13/17
		Vec4V(  27.f/29478.f,  7892.f/29478.f, 18815.f/29478.f, 2744.f/29478.f), // @ x = 14/17
		Vec4V(   8.f/29478.f,  6827.f/29478.f, 19268.f/29478.f, 3375.f/29478.f), // @ x = 15/17
		Vec4V(   1.f/29478.f,  5828.f/29478.f, 19553.f/29478.f, 4096.f/29478.f), // @ x = 16/17
	};*/

	switch (tessX)
	{
	case (1*4 + 1): return kCoefficients_Position_1;
	case (2*4 + 1): return kCoefficients_Position_2;
	case (3*4 + 1): return kCoefficients_Position_3;
	case (4*4 + 1): return kCoefficients_Position_4;
	}

	return NULL;
}

#if 0
static __forceinline const Vec4V* ptxTrailGetCubicSplineCoefficients_Tangent4(int tessX)
{
	static const Vec4V kCoefficients_Tangent4_1[1*4] =
	{
		Vec4V(-8.f/50.f, -41.f/50.f, 56.f/50.f, -7.f/50.f), // @ x = 1/5
		Vec4V( 3.f/50.f, -64.f/50.f, 69.f/50.f, -8.f/50.f), // @ x = 2/5
		Vec4V( 8.f/50.f, -69.f/50.f, 64.f/50.f, -3.f/50.f), // @ x = 3/5
		Vec4V( 7.f/50.f, -56.f/50.f, 41.f/50.f,  8.f/50.f), // @ x = 4/5
	};
	static const Vec4V kCoefficients_Tangent4_2[2*4] =
	{
		Vec4V(-16.f/54.f, -27.f/54.f, 48.f/54.f, -5.f/54.f), // @ x = 1/9
		Vec4V( -7.f/54.f, -48.f/54.f, 63.f/54.f, -8.f/54.f), // @ x = 2/9
		Vec4V(  0.f/54.f, -63.f/54.f, 72.f/54.f, -9.f/54.f), // @ x = 3/9
		Vec4V(  5.f/54.f, -72.f/54.f, 75.f/54.f, -8.f/54.f), // @ x = 4/9
		Vec4V(  8.f/54.f, -75.f/54.f, 72.f/54.f, -5.f/54.f), // @ x = 5/9
		Vec4V(  9.f/54.f, -72.f/54.f, 63.f/54.f,  0.f/54.f), // @ x = 6/9
		Vec4V(  8.f/54.f, -63.f/54.f, 48.f/54.f,  7.f/54.f), // @ x = 7/9
		Vec4V(  5.f/54.f, -48.f/54.f, 27.f/54.f, 16.f/54.f), // @ x = 8/9
	};
	static const Vec4V kCoefficients_Tangent4_3[3*4] =
	{
		Vec4V(-120.f/338.f, -121.f/338.f, 264.f/338.f, -23.f/338.f), // @ x = 1/13
		Vec4V( -77.f/338.f, -224.f/338.f, 341.f/338.f, -40.f/338.f), // @ x = 2/13
		Vec4V( -40.f/338.f, -309.f/338.f, 400.f/338.f, -51.f/338.f), // @ x = 3/13
		Vec4V(  -9.f/338.f, -376.f/338.f, 441.f/338.f, -56.f/338.f), // @ x = 4/13
		Vec4V(  16.f/338.f, -425.f/338.f, 464.f/338.f, -55.f/338.f), // @ x = 5/13
		Vec4V(  35.f/338.f, -456.f/338.f, 469.f/338.f, -48.f/338.f), // @ x = 6/13
		Vec4V(  48.f/338.f, -469.f/338.f, 456.f/338.f, -35.f/338.f), // @ x = 7/13
		Vec4V(  55.f/338.f, -464.f/338.f, 425.f/338.f, -16.f/338.f), // @ x = 8/13
		Vec4V(  56.f/338.f, -441.f/338.f, 376.f/338.f,   9.f/338.f), // @ x = 9/13
		Vec4V(  51.f/338.f, -400.f/338.f, 309.f/338.f,  40.f/338.f), // @ x = 10/13
		Vec4V(  40.f/338.f, -341.f/338.f, 224.f/338.f,  77.f/338.f), // @ x = 11/13
		Vec4V(  23.f/338.f, -264.f/338.f, 121.f/338.f, 120.f/338.f), // @ x = 12/13
	};
	static const Vec4V kCoefficients_Tangent4_4[4*4] =
	{
		Vec4V(-224.f/578.f, -161.f/578.f, 416.f/578.f, -31.f/578.f), // @ x = 1/17
		Vec4V(-165.f/578.f, -304.f/578.f, 525.f/578.f, -56.f/578.f), // @ x = 2/17
		Vec4V(-112.f/578.f, -429.f/578.f, 616.f/578.f, -75.f/578.f), // @ x = 3/17
		Vec4V( -65.f/578.f, -536.f/578.f, 689.f/578.f, -88.f/578.f), // @ x = 4/17
		Vec4V( -24.f/578.f, -625.f/578.f, 744.f/578.f, -95.f/578.f), // @ x = 5/17
		Vec4V(  11.f/578.f, -696.f/578.f, 781.f/578.f, -96.f/578.f), // @ x = 6/17
		Vec4V(  40.f/578.f, -749.f/578.f, 800.f/578.f, -91.f/578.f), // @ x = 7/17
		Vec4V(  63.f/578.f, -784.f/578.f, 801.f/578.f, -80.f/578.f), // @ x = 8/17
		Vec4V(  80.f/578.f, -801.f/578.f, 784.f/578.f, -63.f/578.f), // @ x = 9/17
		Vec4V(  91.f/578.f, -800.f/578.f, 749.f/578.f, -40.f/578.f), // @ x = 10/17
		Vec4V(  96.f/578.f, -781.f/578.f, 696.f/578.f, -11.f/578.f), // @ x = 11/17
		Vec4V(  95.f/578.f, -744.f/578.f, 625.f/578.f,  24.f/578.f), // @ x = 12/17
		Vec4V(  88.f/578.f, -689.f/578.f, 536.f/578.f,  65.f/578.f), // @ x = 13/17
		Vec4V(  75.f/578.f, -616.f/578.f, 429.f/578.f, 112.f/578.f), // @ x = 14/17
		Vec4V(  56.f/578.f, -525.f/578.f, 304.f/578.f, 165.f/578.f), // @ x = 15/17
		Vec4V(  31.f/578.f, -416.f/578.f, 161.f/578.f, 224.f/578.f), // @ x = 16/17
	};

	switch (tessX)
	{
	case (1*4 + 1): return kCoefficients_Tangent4_1;
	case (2*4 + 1): return kCoefficients_Tangent4_2;
	case (3*4 + 1): return kCoefficients_Tangent4_3;
	case (4*4 + 1): return kCoefficients_Tangent4_4;
	}

	return NULL;
}
#endif // 0

template <bool bLit> static __forceinline void SetTrailVertex(ptxTrailVtx<bLit>* pDest, Vec4V_In vPos, ScalarV_In vTexCoordY, Vec4V_In vColour, Vec3V_In);

template <> __forceinline void SetTrailVertex<false>(ptxTrailVtx<false>* pDest, Vec4V_In vPos, ScalarV_In vTexCoordY, Vec4V_In vColour, Vec3V_In)
{
	pDest->pos_colour	= PackColor32IntoWComponent(vPos.GetXYZ(), vColour);	// we could compute the packed colour only once per column ..
	pDest->tex			= Vec4V(vPos.GetW(), vTexCoordY, Vec2V(V_ZERO));		// store texcoord.xy

#if RMPTFX_BANK
	if (ptxDebug::sm_drawTrailsWireframe)
	{
		pDest->pos_colour	= PackColor32IntoWComponent(vPos.GetXYZ(), Vec4V(1.0f, 1.0f, 1.0f, 1.0f));
	}
#endif
}

template <> __forceinline void SetTrailVertex<true>(ptxTrailVtx<true>* pDest, Vec4V_In vPos, ScalarV_In vTexCoordY, Vec4V_In vColour, Vec3V_In vTangent)
{
	pDest->pos_colour	= PackColor32IntoWComponent(vPos.GetXYZ(), vColour);	// we could compute the packed colour only once per column ..
	pDest->tex			= Vec4V(vPos.GetW(), vTexCoordY, Vec2V(V_ZERO));		// store texcoord.xy
	pDest->tangent		= vTangent;

#if RMPTFX_BANK
	if (ptxDebug::sm_drawTrailsWireframe)
	{
		pDest->pos_colour	= PackColor32IntoWComponent(vPos.GetXYZ(), Vec4V(1.0f, 1.0f, 1.0f, 1.0f));
	}
#endif
}

#if __ASSERT
	#define SET_TRAIL_VERTEX(offset,vPos,vTexCoordY,vColour,vTangent) { Assert((pVertices + offset) >= pVerticesStart && (pVertices + offset) < pVerticesEnd); SetTrailVertex<bLit>(&pVertices[offset], vPos, vTexCoordY, vColour, vTangent); }
#else
	#define SET_TRAIL_VERTEX(offset,vPos,vTexCoordY,vColour,vTangent) { SetTrailVertex<bLit>(pVertices + offset, vPos, vTexCoordY, vColour, vTangent); }
#endif

template <bool bLit> static __forceinline int DrawTrailTessellator(ptxTrailVtx<bLit>* pVertices, const ptxTrailControlPoint* pControlPoints, int numControlPoints, const Vec4V* pvCoefficientsP, int tessX, int tessY)
{
	const bool bAlt = false;

	const int numPatches  = numControlPoints - 1;
	const int numSegs     = (tessX - 1)/4;
	const int numCols     = numPatches*tessX + 1;
	const int numRows     = tessY + 1; // stride
	const int numVertices = numCols*numRows;

	const ScalarV ooy = ScalarV(1.0f/(float)tessY);
	const Vec4V vOOX_1234 = Vec4V(1.0f/(float)tessX, 2.0f/(float)tessX, 3.0f/(float)tessX, 4.0f/(float)tessX);

#if __ASSERT
	const ptxTrailVtx<bLit>* pVerticesStart = pVertices;
	const ptxTrailVtx<bLit>* pVerticesEnd   = pVertices + numVertices;
#endif // __ASSERT

	Vec4V vP1 = pControlPoints[0].m_vPosA_XYZ_texCoordX_W;
	Vec4V vQ1 = pControlPoints[0].m_vPosB_XYZ_texCoordX_W;
	Vec4V vP2 = pControlPoints[1].m_vPosA_XYZ_texCoordX_W;
	Vec4V vQ2 = pControlPoints[1].m_vPosB_XYZ_texCoordX_W;

	Vec4V vP0 = vP1;
	Vec4V vQ0 = vQ1;

	// first column (P1..Q1)
	{
		if (bAlt)
		{
			vP1 = (vP0 + vP1*ScalarV(V_FOUR) + vP2)*ScalarV(1.0f/6.0f);
			vQ1 = (vQ0 + vQ1*ScalarV(V_FOUR) + vQ2)*ScalarV(1.0f/6.0f);
		}

		vQ1 = (vQ0 - vP0)*ooy;

		ScalarV vTexCoordY = ScalarV(V_ZERO);

		const Vec4V vColour1  = pControlPoints[0].m_vColour;
		const Vec3V vTangent1 = pControlPoints[0].m_vTangent;

		for (int rowIndex = 0; rowIndex < numRows; rowIndex++)
		{
			SET_TRAIL_VERTEX(rowIndex, vP1, vTexCoordY, vColour1, vTangent1);
			vP1 += vQ1;

			vTexCoordY += ooy;
		}

		pVertices += numRows;

		vP1 = vP0; // restore
		vQ1 = vQ0; // restore
	}

	for (int patchIndex = 0; patchIndex < numPatches; patchIndex++) // patches, corresponds to [i]..[i+1]
	{
		const int srcIndex = Min<int>(patchIndex + 2, numPatches);

		Vec4V vP3 = pControlPoints[srcIndex].m_vPosA_XYZ_texCoordX_W;
		Vec4V vQ3 = pControlPoints[srcIndex].m_vPosB_XYZ_texCoordX_W;

		Vec4V vP0_adj = vP0;
		Vec4V vP3_adj = vP3;
		Vec4V vQ0_adj = vQ0;
		Vec4V vQ3_adj = vQ3;

		if (patchIndex==0)
		{
			// normalise the tangent on the first patch so the trail doesn't glitch at the anchor pos
			/// const Vec4V vP01 = vP0 - vP1;
			const Vec4V vP02 = vP0 - vP2;
			const Vec4V vP12 = vP1 - vP2;
			const Vec4V vP13 = vP1 - vP3;
			/// const Vec4V vQ01 = vQ0 - vQ1;
			const Vec4V vQ02 = vQ0 - vQ2;
			const Vec4V vQ12 = vQ1 - vQ2;
			const Vec4V vQ13 = vQ1 - vQ3;

			vP0_adj = vP1 - vP12 + (Dot(vP02, vP12)/Dot(vP02, vP02))*vP02*ScalarV(V_TWO);
			vP3_adj = vP2 + vP12 - (Dot(vP13, vP12)/Dot(vP13, vP13))*vP13*ScalarV(V_TWO);
			vQ0_adj = vQ1 - vQ12 + (Dot(vQ02, vQ12)/Dot(vQ02, vQ02))*vQ02*ScalarV(V_TWO);
			vQ3_adj = vQ2 + vQ12 - (Dot(vQ13, vQ12)/Dot(vQ13, vQ13))*vQ13*ScalarV(V_TWO);
		}

		const Mat44V vPM = Mat44V(vP0_adj,vP1,vP2,vP3_adj);
		const Mat44V vQM = Mat44V(vQ0_adj,vQ1,vQ2,vQ3_adj);

		const Vec4V vColour1 = pControlPoints[patchIndex + 0].m_vColour;
		const Vec4V vColour2 = pControlPoints[patchIndex + 1].m_vColour - vColour1; // delta

		const Vec3V vTangent1 = pControlPoints[patchIndex + 0].m_vTangent;
		const Vec3V vTangent2 = pControlPoints[patchIndex + 1].m_vTangent - vTangent1; // delta

		Vec4V vColourInterp = vOOX_1234;

		for (int segIdx = 0; segIdx < numSegs; segIdx++) // segments
		{
			Vec4V vKA = pvCoefficientsP[0 + segIdx*4]; // cubic spline coefficients for positions
			Vec4V vKB = pvCoefficientsP[1 + segIdx*4];
			Vec4V vKC = pvCoefficientsP[2 + segIdx*4];
			Vec4V vKD = pvCoefficientsP[3 + segIdx*4];

			Vec4V vPA = Multiply(vPM, vKA); // positions (points) on bottom row
			Vec4V vPB = Multiply(vPM, vKB);
			Vec4V vPC = Multiply(vPM, vKC);
			Vec4V vPD = Multiply(vPM, vKD);

			Vec4V vQA = (Multiply(vQM, vKA) - vPA)*ooy; // positions (points) on top row, converted to deltas
			Vec4V vQB = (Multiply(vQM, vKB) - vPB)*ooy;
			Vec4V vQC = (Multiply(vQM, vKC) - vPC)*ooy;
			Vec4V vQD = (Multiply(vQM, vKD) - vPD)*ooy;

			ScalarV vTexCoordY = ScalarV(V_ZERO);

			const Vec4V vColourA = AddScaled(vColour1, vColour2, vColourInterp.GetX());
			const Vec4V vColourB = AddScaled(vColour1, vColour2, vColourInterp.GetY());
			const Vec4V vColourC = AddScaled(vColour1, vColour2, vColourInterp.GetZ());
			const Vec4V vColourD = AddScaled(vColour1, vColour2, vColourInterp.GetW());

			const Vec3V vTangentA = AddScaled(vTangent1, vTangent2, vColourInterp.GetX());
			const Vec3V vTangentB = AddScaled(vTangent1, vTangent2, vColourInterp.GetY());
			const Vec3V vTangentC = AddScaled(vTangent1, vTangent2, vColourInterp.GetZ());
			const Vec3V vTangentD = AddScaled(vTangent1, vTangent2, vColourInterp.GetW());

			vColourInterp += Vec4V(vOOX_1234.GetW());

			for (int rowIndex = 0; rowIndex < numRows; rowIndex++)
			{
				SET_TRAIL_VERTEX(rowIndex + numRows*0, vPA, vTexCoordY, vColourA, vTangentA); vPA += vQA;
				SET_TRAIL_VERTEX(rowIndex + numRows*1, vPB, vTexCoordY, vColourB, vTangentB); vPB += vQB;
				SET_TRAIL_VERTEX(rowIndex + numRows*2, vPC, vTexCoordY, vColourC, vTangentC); vPC += vQC;
				SET_TRAIL_VERTEX(rowIndex + numRows*3, vPD, vTexCoordY, vColourD, vTangentD); vPD += vQD;
				vTexCoordY += ooy;
			}

			pVertices += numRows*4;
		}

		// last column of patch (P2..Q2)
		{
			if (bAlt)
			{
				vP0 = (vP1 + vP2*ScalarV(V_FOUR) + vP3)*ScalarV(1.0f/6.0f);
				vQ0 = (vQ1 + vQ2*ScalarV(V_FOUR) + vQ3)*ScalarV(1.0f/6.0f);
			}
			else
			{
				vP0 = vP2;
				vQ0 = vQ2;
			}

			vQ0 = (vQ0 - vP0)*ooy;

			ScalarV vTexCoordY = ScalarV(V_ZERO);

			const Vec4V vColour0  = pControlPoints[patchIndex + 1].m_vColour;
			const Vec3V vTangent0 = pControlPoints[patchIndex + 1].m_vTangent;

			for (int rowIndex = 0; rowIndex < numRows; rowIndex++)
			{
				SET_TRAIL_VERTEX(rowIndex, vP0, vTexCoordY, vColour0, vTangent0); vP0 += vQ0;
				vTexCoordY += ooy;
			}

			pVertices += numRows;
		}

		vP0 = vP1;
		vP1 = vP2;
		vP2 = vP3;

		vQ0 = vQ1;
		vQ1 = vQ2;
		vQ2 = vQ3;
	}

	Assert(pVertices == pVerticesEnd);

	return numVertices;
}

#undef SET_TRAIL_VERTEX

#if RMPTFX_BANK
template <bool bLit> static __forceinline void DrawTrailTessellatorSimple(ptxTrailVtx<bLit>* pVertices, const ptxTrailControlPoint* pControlPoints, int numControlPoints)
{
	for (int i = 0; i < numControlPoints; i++)
	{
		const ptxTrailControlPoint& p = *(pControlPoints++);

		SetTrailVertex(pVertices++, p.m_vPosA_XYZ_texCoordX_W, ScalarV(V_ZERO), Vec4V(V_ONE), p.m_vTangent);
		SetTrailVertex(pVertices++, p.m_vPosB_XYZ_texCoordX_W, ScalarV(V_ONE ), Vec4V(V_ONE), p.m_vTangent);
	}
}
#endif

void ptxDrawInterface::DrawTrail(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxd_Trail* pDrawTrailBehaviour, ptxParticleRule* pParticleRule, ptxEvolutionList* UNUSED_PARAM(pEvolutionList), Vec3V_In vAnchorPos, ScalarV_In vAnchorSize, Vec4V_In vAnchorCol, ScalarV_In vTexWrapLength, ScalarV_In vTexOffset PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface))
{
	if (pParticleRule->GetShaderInst().Begin())
	{
		if (pParticleRule->GetShaderInst().GetIsLit())
		{
			DrawTrail<true>(pEffectInst, pEmitterInst, pDrawTrailBehaviour, NULL, vAnchorPos, vAnchorSize, vAnchorCol, vTexWrapLength, vTexOffset PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
		}
		else 
		{
			DrawTrail<false>(pEffectInst, pEmitterInst, pDrawTrailBehaviour, NULL, vAnchorPos, vAnchorSize, vAnchorCol, vTexWrapLength, vTexOffset PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
		}

		pParticleRule->GetShaderInst().End();
	}
}

//////////////////////////////////////////////////////////////////////////
//
template <bool bIsLit>
void ptxDrawInterface::DrawTrail(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxd_Trail* pDrawTrailBehaviour, ptxEvolutionList* UNUSED_PARAM(pEvolutionList), Vec3V_In vAnchorPos, ScalarV_In vAnchorSize, Vec4V_In vAnchorCol, ScalarV_In vTexWrapLength, ScalarV_In vTexOffset PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface))
{
	const int drawBufferId = RMPTFXTHREADER.GetDrawBufferId();
	const int numControlPoints = Min<int>(PTXDRAW_TRAIL_MAX_CONTROL_POINTS, 1 + pEmitterInst->GetPointList().GetNumItemsMT(drawBufferId));

	// return if nothing to draw
	if (numControlPoints < 1)
	{
		return; 
	}

	const Mat34V vCameraMtx = grcViewport::GetCurrent()->GetCameraMtx();

	const int tessX = Clamp<int>(pDrawTrailBehaviour->m_tessellationU*4 + 1, PTXDRAW_TRAIL_TESS_X_MIN, PTXDRAW_TRAIL_TESS_X_MAX);
	const int tessY = Clamp<int>(pDrawTrailBehaviour->m_tessellationV      , PTXDRAW_TRAIL_TESS_Y_MIN, PTXDRAW_TRAIL_TESS_Y_MAX);

	// calc the align axis 
	Vec3V vAlignAxis = Normalize(pDrawTrailBehaviour->m_alignAxis);
	if (pDrawTrailBehaviour->m_alignmentMode==PTXD_TRAIL_ALIGNMENTMODE_EMITTER) 
	{
		Mat34V vEmitterMtxWld = pEmitterInst->GetCreationDomainMtxWld(pEffectInst->GetMatrix());
		vAlignAxis = Transform3x3(vEmitterMtxWld, vAlignAxis);
	} 
	else if (pDrawTrailBehaviour->m_alignmentMode==PTXD_TRAIL_ALIGNMENTMODE_EFFECT) 
	{
		vAlignAxis = Transform3x3(pEffectInst->GetMatrix(), vAlignAxis);
	}

	const ptxList* pPointList = &pEmitterInst->GetPointList();


	const u32 vtxSize = sizeof(ptxTrailVtx<bIsLit>);

	const int maxVertsPerBatch = GPU_VERTICES_MAX_SIZE/vtxSize;
	const Vec4V* pvCoefficientsP = ptxTrailGetCubicSplineCoefficients_Position(tessX);
	ptxTrailControlPoint controlPoints[PTXDRAW_TRAIL_MAX_CONTROL_POINTS];

	Vec3V vCamPos;
#if RMPTFX_USE_PARTICLE_SHADOWS
	if (RMPTFXMGR.IsShadowPass())
		vCamPos = RMPTFXMGR.GetShadowSourcePos();
	else
#endif
		vCamPos = vCameraMtx.GetCol3();

	DrawTrailBuildControlPoints(controlPoints, vAlignAxis, vCameraMtx.GetCol3(), vAnchorPos, vAnchorSize, vAnchorCol, vTexWrapLength, vTexOffset, pPointList, pDrawTrailBehaviour, drawBufferId);


	GRCDEVICE.SetVertexDeclaration((bIsLit ? sm_pTrailVtxDecl_lit : sm_pTrailVtxDecl));

#if RMPTFX_BANK
	if (ptxDebug::sm_drawTrailsWireframe)
	{
		SetDebugWireframeRenderState();
	}

	if (ptxDebug::sm_simpleTrails)
	{
		const int numVertices = numControlPoints*2;

		if (numVertices <= maxVertsPerBatch)
		{
			ptxTrailVtx<bIsLit>* pVertices = (ptxTrailVtx<bIsLit>*)GRCDEVICE.BeginVertices(drawTriStrip, numVertices, vtxSize);
			DrawTrailTessellatorSimple(pVertices, controlPoints, numControlPoints);
			GRCDEVICE.EndVertices();
		}
	}
	else
#endif
	{
		const int numPatches  = numControlPoints - 1;
		const int numVertices = (numPatches*tessX + 1)*(tessY + 1);

		if (tessY > 1 || numVertices > maxVertsPerBatch)
		{
			const int numIndicesPerPatch = (tessY > 1) ? (tessX*(tessY*2 + 4)) : (tessX*2 + 2); // non-indexed mesh vertices

			if (numIndicesPerPatch <= maxVertsPerBatch)
			{
#if PTXDRAW_TRAIL_USE_INDEX_BUFFERS	

#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH && GS_INSTANCED_SHADOWS
				// move this here since ps4 needs this before vp binding
				if (pMultipleDrawInterface && pMultipleDrawInterface->IsUsingInstanced())
					grcViewport::RegenerateInstVPMatrices(Mat44V(V_IDENTITY));
#endif

				ptxTrailVtx<bIsLit>* pVertices = (ptxTrailVtx<bIsLit>*)GRCDEVICE.BeginVertices(drawTriStrip, numVertices, sizeof(ptxTrailVtx<bIsLit>));
				DrawTrailTessellator(pVertices, controlPoints, numControlPoints, pvCoefficientsP, tessX, tessY);
				const int numIndices = numIndicesPerPatch * numPatches;
				const grcIndexBuffer *pIndexBuffer = sm_pTrailIndexBuffers[tessX - PTXDRAW_TRAIL_TESS_X_MIN][tessY - PTXDRAW_TRAIL_TESS_Y_MIN];
				Assertf(pIndexBuffer != NULL, "Trail index buffer with tessX = %d, tessY = %d at index x = %d, y = %d is not initialized", tessX, tessY, tessX - PTXDRAW_TRAIL_TESS_X_MIN, tessY - PTXDRAW_TRAIL_TESS_Y_MIN);

			#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH
				if(pMultipleDrawInterface)
				{
			#if GS_INSTANCED_SHADOWS
					if (pMultipleDrawInterface->IsUsingInstanced())
					{
						GRCDEVICE.EndVertices(NULL, 0, *pIndexBuffer);
						pMultipleDrawInterface->PreDrawInstanced();
						GRCDEVICE.DrawInstancedIndexedPrimitive(drawTriStrip,numIndices,pMultipleDrawInterface->GetNumActiveCascades(),0,0,0);
						pMultipleDrawInterface->PostDrawInstanced();
					}
					else
			#endif
					{
						GRCDEVICE.EndCreateVertices(NULL, 0, *pIndexBuffer);
						for(u32 i=0; i<pMultipleDrawInterface->NoOfDraws(); i++)
						{
							pMultipleDrawInterface->PreDraw(i);
							if( i == 0) 
								GRCDEVICE.DrawVertices(NULL, 0, *pIndexBuffer);
							else
								GRCDEVICE.DrawIndexedPrimitive(drawTriStrip, 0, numIndices);
							pMultipleDrawInterface->PostDraw(i);
						}
#if __D3D11 || RSG_ORBIS
						GRCDEVICE.ReleaseVertices(NULL, 0, *pIndexBuffer);
#endif // __D3D11 || RSG_ORBIS
					}
				}
				else
				{
					GRCDEVICE.EndVertices(NULL, numIndices, *pIndexBuffer);
				}
			#else
				GRCDEVICE.EndVertices(NULL, numIndices, *pIndexBuffer);
			#endif
#else // not PTXDRAW_TRAIL_USE_INDEX_BUFFERS

				ptxTrailVtx<bIsLit>* pVerticesTemp = rage_new ptxTrailVtx<bIsLit>[numIndicesPerPatch*numPatches];
				DrawTrailTessellator(pVerticesTemp, controlPoints, numControlPoints, pvCoefficientsP, tessX, tessY);

				ptxTrailVtx<bIsLit>* pVerticesTemp1 = pVerticesTemp;

				for (int i = 0; i < numPatches; i++)
				{
					ptxTrailVtx<bIsLit>* pVertices = (ptxTrailVtx<bIsLit>*)GRCDEVICE.BeginVertices(drawTriStrip, numIndicesPerPatch, sizeof(ptxTrailVtx<bIsLit>));

					DrawTrailTessellator_BuildNonIndexedMesh(pVertices, pVerticesTemp1, 2, tessX, tessY);
					pVerticesTemp1 += tessX*(tessY + 1); // skip one patch of vertices

					GRCDEVICE.EndVertices();
				}

				delete[] pVerticesTemp;

#endif // not PTXDRAW_TRAIL_USE_INDEX_BUFFERS
			}
		}
		else // simpler case, don't need to build non-indexed mesh
		{
			ptxTrailVtx<bIsLit>* pVertices = (ptxTrailVtx<bIsLit>*)GRCDEVICE.BeginVertices(drawTriStrip, numVertices, vtxSize);
			DrawTrailTessellator(pVertices, controlPoints, numControlPoints, pvCoefficientsP, tessX, tessY);
			GRCDEVICE.EndVertices();
		}
	}

#if RMPTFX_BANK
	if (ptxDebug::sm_drawTrailsWireframe)
	{
		RestoreDefaultRenderState();
	}
#endif 
}

void ptxDrawInterface::CalcUVInfo(Vec4V_InOut vUVInfo, int numColumns, int numRows, int texId, ScalarV_In vClipMin, ScalarV_In vClipMax, bool flipU, bool flipV)
{
	// calculate the uv info (for correcting the uv coordinates in the texture page)
	ScalarV uIndex((float)(texId%numColumns));
	ScalarV vIndex((float)(texId/numColumns));
	ScalarV uStep(1.0f/numColumns);
	ScalarV vStep(1.0f/numRows);

	vUVInfo.SetX(uIndex*uStep);						// u min
	vUVInfo.SetY(vUVInfo.GetX()+uStep);				// u max
	vUVInfo.SetZ(AddScaled(vIndex*vStep, vClipMin, vStep));	// v min
	vUVInfo.SetW(AddScaled(vIndex*vStep, vClipMax, vStep));	// v max

 	if (flipU)
	{
		vUVInfo = vUVInfo.Get<Vec::Y,Vec::X,Vec::Z,Vec::W>(); // flip x,y
	}
 
 	if (flipV)
 	{
		vUVInfo = vUVInfo.Get<Vec::X,Vec::Y,Vec::W,Vec::Z>(); // flip z,w
 	}
}

//Adding this function to make code more manageable across different platforms
void ptxDrawInterface::SetupPtxSpriteInst(ptxSpriteInst* pCurrInst, const ptxDrawSpriteInstParams& drawInstParams, ptxDrawSpriteParams& drawSpriteParams, const Vec3V& vCentrePos, const Vec3V& vSpriteUp, const Vec3V& vSpriteNormal)
{
	const ptxPointDB* pPointDB = drawSpriteParams.pPointDB;
	const Vec3V& vSpritePos = pPointDB->GetCurrPos();
	const Vec3V vCamPos = drawInstParams.vCamMtx.GetCol3();
	// calculate camera shrink multiplier
	ScalarV vCamShrink = ScalarV(V_ONE);
	if (drawSpriteParams.pCustomVars->data[3] > 0.0f) 
	{
		vCamShrink = ScalarVFromF32(1.0f/drawSpriteParams.pCustomVars->data[3]);
		vCamShrink = Saturate(vCamShrink*Dist(vCamPos, vSpritePos));
	}

	const float xStep = 1.0f/drawInstParams.numTexColumns;
	const float yStep = 1.0f/drawInstParams.numTexRows;

#if PTXDRAW_USE_INSTANCING
	//When instancing put the sprite pos in here instead and calculate the center in the shader
	(void)vCentrePos;
	const Vec3V& vSourcePos = vSpritePos;
#else
	const Vec3V& vSourcePos = vCentrePos;
#endif	//PTXDRAW_USE_INSTANCING

	Vec4V vColour = pPointDB->GetColour().GetRGBA();
	vColour.SetW(vColour.GetW() * ScalarVFromF32(drawInstParams.alphaMult));

	pCurrInst->vCentrePos_Colour = PackColor32IntoWComponent( vSourcePos, vColour );

	// pack custom variables
	// TODO -- even V4LoadUnaligned should be faster than constructing from floats
	//Vec4V vCustomVars03 = Vec4V(Vec::V4LoadUnaligned(drawSpriteParams.pCustomVars->data));
	//Vec4V vCustomVars47 = Vec4V(Vec::V4LoadUnaligned(drawSpriteParams.pCustomVars->data + 4));

	Vec4V vCustomVars03(	drawSpriteParams.pCustomVars->data[0],
		drawSpriteParams.pCustomVars->data[1],
		drawSpriteParams.pCustomVars->data[2],
		vCamShrink.Getf() );

	Vec4V vCustomVars47(	drawSpriteParams.pCustomVars->data[4],
		drawSpriteParams.pCustomVars->data[5],
		drawInstParams.isProjected ? xStep : drawSpriteParams.pCustomVars->data[6],
		drawInstParams.isProjected ? yStep : drawSpriteParams.pCustomVars->data[7] );

	pCurrInst->vCustomVars = Float16Vec8Pack( vCustomVars03, vCustomVars47 );

	// pack normal and misc settings
	Vec4V vTexFrameRatio_emissiveIntensity = Vec4V(pPointDB->GetTexFrameRatio(), pPointDB->GetEmissiveIntensity(), 0.0f, 0.0f);

	if(drawInstParams.isProjected == false)
	{
		pCurrInst->vNormal_texFrameRatio_emissiveIntensity = Float16Vec8Pack( Vec4V(vSpriteNormal), vTexFrameRatio_emissiveIntensity );
	}
	else
	{
		pCurrInst->vNormal_texFrameRatio_emissiveIntensity = Float16Vec8Pack( Vec4V(vSpriteUp.GetXY(), Vec2V(V_ONE) ), vTexFrameRatio_emissiveIntensity );
	}
}
void ptxDrawInterface::BatchSprite(const ptxDrawSpriteInstParams& drawInstParams, ptxDrawSpriteParams& drawSpriteParams)
{
	// extract some data from the draw params
	const ptxPointSB* pPointSB = drawSpriteParams.pPointSB;
	const ptxPointDB* pPointDB = drawSpriteParams.pPointDB;

#if PTXDRAW_USE_INSTANCING
	ptxSpriteInst*& pCurrInst = *drawSpriteParams.ppCurrInst;
#else
	ptxSpriteVtxCommon*& pCurrCommon = *drawSpriteParams.ppCurrCommon;
	ptxSpriteVertex* pCurrVertex = &pCurrCommon->vtx;
	ptxSpriteInst* pCurrInst = &pCurrCommon->inst;
#endif

	// cache some point data
	const Vec3V& vSpritePos = pPointDB->GetCurrPos();
	const Vec4V vDimensions = pPointDB->GetDimensions();
	Vec3V vCamPos;
#if RMPTFX_USE_PARTICLE_SHADOWS
	if (RMPTFXMGR.IsShadowPass())
		vCamPos = RMPTFXMGR.GetShadowSourcePos();
	else
#endif
		vCamPos = drawInstParams.vCamMtx.GetCol3();

	// calculate the up and right vectors of the sprite
	const ScalarV vAngle = pPointSB->GetInitRot().GetX() + pPointDB->GetCurrRot().GetX();
	Vec3V vDirRight = Vec3V(V_X_AXIS_WZERO);
	Vec3V vDirUp = Vec3V(V_Z_AXIS_WZERO);

	// initialise the direction alignment shrink factor (for shrinking direction particles as the cross product calcs become unstable)
	ScalarV vDotShrinkStart = ScalarVFromF32(0.999990f);
	ScalarV vDotShrinkStop = ScalarVFromF32(0.999995f);
	ScalarV vDirAlignmentShrinkFactor(V_ONE);

	// deal with directional points
	if (drawInstParams.isDirectional)
	{
		// calculate the sprite orientation vectors
		if (Likely(!drawInstParams.isScreenSpace))
		{
			// calc the up direction (along the particle velocity)
			bool upAlignedToVelocity = false;
			vDirUp = pPointDB->GetCurrVel();
			if (Likely(!IsZeroAll(vDirUp)))
			{
				vDirUp = Normalize(vDirUp);
				upAlignedToVelocity = true;
			}
			else
			{
				vDirUp = drawInstParams.vUpWld;
			}

			// align to axis - point the normal of the point in the direction of the axis
			if (Unlikely(drawInstParams.alignToAxis))
			{
				Vec3V vAlignAxis = drawInstParams.vAlignAxis;

				// calculate the right direction (by crossing the up and align vectors)
				if (IsLessThanAll(Dot(vAlignAxis, vDirUp), vDotShrinkStop))
				{
					vDirRight = Cross(vDirUp, vAlignAxis);
					vDirRight = Normalize(vDirRight);

					// recalc the up vector
					vDirUp = Cross(vAlignAxis, vDirRight);
					vDirUp = Normalize(vDirUp);
				}
				else 
				{
					// try using world up if using velocity for up didn't work
					vDirUp = drawInstParams.vUpWld;
					if (upAlignedToVelocity && IsLessThanAll(Dot(vAlignAxis, vDirUp), vDotShrinkStop))
					{
						vDirRight = Cross(vDirUp, vAlignAxis);
						vDirRight = Normalize(vDirRight);

						// recalc the up vector
						vDirUp = Cross(vAlignAxis, vDirRight);
						vDirUp = Normalize(vDirUp);
					}
					else
					{
						// unless the align axis is too close to the up vector
						vDirRight = Vec3V(V_X_AXIS_WZERO);
						vDirUp = Vec3V(V_Y_AXIS_WZERO);
					}
				}
			}
			// align so the particle is facing the camera as best as it can
			else
			{
				// calculate the view axis (based on the view angle or the sprite alignment)
				Vec3V vViewAxis = vCamPos - vSpritePos;
				vViewAxis = Normalize(vViewAxis);

				// calculate the right direction (by crossing the up and view vectors) and the directional alignment shrink factor
				ScalarV vDot = Dot(vViewAxis, vDirUp);
				if (IsLessThanAll(vDot, vDotShrinkStop))
				{
					vDirRight = Cross(vDirUp, vViewAxis);
					vDirRight = Normalize(vDirRight);

					// check if we want to flip the right direction to keep the sprite upright
					Vec3V vCamRightRel = Cross(vViewAxis, Vec3V(V_Z_AXIS_WZERO));
					vCamRightRel = Normalize(vCamRightRel);
					if (Dot(vDirUp, vCamRightRel).Getf()<0.0f)
					//if (Dot(vDirUp, drawInstParams.vCamMtx.GetCol0()).Getf()<0.0f)
					{
						vDirRight = -vDirRight;
					}

					// check if the vectors are within the shrink range
					if (IsGreaterThanAll(vDot, vDotShrinkStart))
					{
						vDirAlignmentShrinkFactor =	ScalarV(V_ONE) - ((vDot-vDotShrinkStart) / (vDotShrinkStop-vDotShrinkStart));
					}
				}
				else
				{
					// vectors are too close - particle should be shrunk to zero size
					vDirAlignmentShrinkFactor = ScalarV(V_ZERO);
				}
			}

			// apply rotation
			ScalarV vSinTheta, vCosTheta;
			SinAndCos(vSinTheta, vCosTheta, vAngle);
			Vec3V vDirRightRot = SubtractScaled(vDirRight * vCosTheta, vDirUp, vSinTheta);
			Vec3V vDirUpRot = AddScaled(vDirUp * vCosTheta, vDirRight, vSinTheta);
			vDirRight = vDirRightRot;
			vDirUp = vDirUpRot;
		}
	}
	else
	{
		// non-directional
		// align camera-facing particles to view point, rather than to the view plane
		if (Likely(!drawInstParams.isScreenSpace)) 
		{
			Vec3V vViewAxis = vCamPos - vSpritePos;
			vViewAxis = Normalize(vViewAxis);

			// calculate the right direction (by crossing the up and align vectors)
			if (IsLessThanAll(Dot(vViewAxis, vDirUp), vDotShrinkStop))
			{
				vDirRight = Cross(vDirUp, vViewAxis);
				vDirRight = Normalize(vDirRight);

				// recalc the up vector
				vDirUp = Cross(vViewAxis, vDirRight);
				vDirUp = Normalize(vDirUp);
			}
			else
			{
				// unless the align axis is too close to the up vector
				vDirRight = Vec3V(V_X_AXIS_WZERO);
				vDirUp = Vec3V(V_Y_AXIS_WZERO);
			}

			// apply rotation
			ScalarV vSinTheta, vCosTheta;
			SinAndCos(vSinTheta, vCosTheta, vAngle);
			Vec3V vDirRightRot = SubtractScaled(vDirRight * vCosTheta, vDirUp, vSinTheta);	
			Vec3V vDirUpRot = AddScaled(vDirUp * vCosTheta, vDirRight, vSinTheta);
			vDirRight = vDirRightRot;
			vDirUp = vDirUpRot;

		}
		else
		{
			// calculate the sprite orientation vectors
			vDirRight = RotateAboutYAxis(vDirRight, vAngle);
			vDirUp	= RotateAboutYAxis(vDirUp, vAngle);
		}
	}

	// apply aspect ratio correction to screen space sprites, as they're not going to be transformed any further
	if (Unlikely(drawInstParams.isScreenSpace)) 
	{
		const Vec3V vRatio = drawInstParams.vAspectRatio;
		vDirRight *= vRatio;
		vDirUp *= vRatio;
	}


	// calculate the size scales
	Vec4V vDimensionsScaled = vDimensions * vDirAlignmentShrinkFactor;
	ScalarV vSizeUp		= vDimensionsScaled.GetX();
	ScalarV vSizeDown	= vDimensionsScaled.GetY();
	ScalarV vSizeLeft	= vDimensionsScaled.GetZ();
	ScalarV vSizeRight	= vDimensionsScaled.GetW();

	// calculate the sprite offsets
	Vec3V vOffsetUp		= Scale(vDirUp, vSizeUp);
	Vec3V vOffsetDown	= Scale(-vDirUp, vSizeDown);
	Vec3V vOffsetLeft	= Scale(-vDirRight, vSizeLeft);
	Vec3V vOffsetRight	= Scale(vDirRight, vSizeRight);

	// deal with sprites that need clipped to the spawn position
	ScalarV vClipMin = ScalarV(V_ZERO);
	ScalarV vClipMax = ScalarV(V_ONE);
	if (drawInstParams.isDirectional)
	{
		if (Unlikely(drawInstParams.nearClip) || Unlikely(drawInstParams.farClip))
		{
			const ScalarV vOrigSizeUp = vSizeUp;
			const ScalarV vOrigSizeDown = vSizeDown;

			// calculate the distance from the effect inst to the sprite centre
			const Vec3V vSpawnToSpriteVec = vSpritePos - pPointDB->GetSpawnPos();
			ScalarV vSpawnToSpriteDist = Mag(vSpawnToSpriteVec);	
		
			if (IsLessThanAll(Dot(vSpawnToSpriteVec,vOffsetUp), ScalarV(V_ZERO)))
			{
				// negate the distance if required
				vSpawnToSpriteDist = -vSpawnToSpriteDist;
			}

			if (Unlikely(drawInstParams.nearClip))
			{
				const ScalarV vNearClipDist = ScalarV(drawInstParams.nearClipDist);

				if (IsLessThanAll(vSpawnToSpriteDist+vSizeUp, vNearClipDist))
				{
					vSizeDown = -vSizeUp;
					vOffsetDown = vOffsetUp;
				}
				else if (IsLessThanAll(vSpawnToSpriteDist-vSizeDown, vNearClipDist))
				{
					vSizeDown = vSpawnToSpriteDist-vNearClipDist;
					vOffsetDown = Normalize(vOffsetDown);
					vOffsetDown *= vSizeDown;
				}
			}

			if (Unlikely(drawInstParams.farClip))
			{
				const ScalarV vFarClipDist = ScalarV(drawInstParams.farClipDist);

				if (IsGreaterThanAll(vSpawnToSpriteDist-vSizeDown, vFarClipDist))
				{
					// the sprite is totally outside the far clamp range - set the up vector to be the same as the down (i.e. zero area)
					vSizeUp = -vSizeDown;
					vOffsetUp = vOffsetDown;
				}
				else if (IsGreaterThanAll(vSpawnToSpriteDist+vSizeUp, vFarClipDist))
				{
					// the top of the sprite is outside the far clamp range - calculate a new up vector that sets the top of the sprite on the boundary
					vSizeUp = vFarClipDist-vSpawnToSpriteDist;
					vOffsetUp = Normalize(vOffsetUp);
					vOffsetUp *= vSizeUp;
				}
			}

			// calc the new v clip data if we've clipped the sprite
			if (drawInstParams.uvClip)
			{
				const ScalarV vOrigHeight = vOrigSizeUp+vOrigSizeDown;
				vClipMin = (vOrigSizeUp-vSizeUp)/vOrigHeight;
				vClipMax = (vOrigSizeUp+vSizeDown)/vOrigHeight;
			}
		}
	}

#if PTXDRAW_USE_INSTANCING
	pCurrInst->vOffsetUp = Vec4V( vOffsetUp.GetXf(), vOffsetUp.GetYf(), vOffsetUp.GetZf(), vOffsetLeft.GetXf());
	pCurrInst->vOffsetDown = Vec4V( vOffsetDown.GetXf(), vOffsetDown.GetYf(), vOffsetDown.GetZf(), vOffsetLeft.GetYf());
	pCurrInst->vOffsetRight = Vec4V( vOffsetRight.GetXf(), vOffsetRight.GetYf(), vOffsetRight.GetZf(), vOffsetLeft.GetZf());
#endif

	// calc the bottom left world vertex, the right and up vectors and the normal
	const Vec3V vSpriteBLPosWld = vSpritePos + vOffsetDown + vOffsetLeft;
	const Vec3V vSpriteRight = vOffsetRight-vOffsetLeft;
	const Vec3V vSpriteUp = vOffsetUp-vOffsetDown;
	Vec3V vSpriteNormal(V_ZERO);
	vSpriteNormal = Cross(vSpriteRight, vSpriteUp);
	vSpriteNormal = Normalize(vSpriteNormal);

	// calc the actual centre position of the sprite (the sprite pos may not be the centre pos currently as the tblr parameters may differ)
	const Vec3V vCentrePos = AddScaled(vSpriteBLPosWld, vSpriteRight + vSpriteUp, ScalarV(V_HALF));


	// calc the clip regions
	u8 texFrameIdA = pPointDB->GetTexFrameIdA();
	u8 texFrameIdB = pPointDB->GetTexFrameIdB();
	if (texFrameIdA>=drawInstParams.numTexRows*drawInstParams.numTexColumns)
	{
		ptxAssertf(0, "texture frame A out of range");
		texFrameIdA = 0;
	}

	if (texFrameIdB>=drawInstParams.numTexRows*drawInstParams.numTexColumns)
	{
		ptxAssertf(0, "texture frame B out of range");
		texFrameIdB = 0;
	}

	const Vec4V vClipRegionA = drawInstParams.pvClipRegionData[texFrameIdA];
	const Vec4V vClipRegionB = drawInstParams.pvClipRegionData[texFrameIdB];
	Vec4V vClipRegion = Lerp(ScalarVFromF32(pPointDB->GetTexFrameRatio()), vClipRegionA, vClipRegionB);
	Vec4V vClipRegionNormal = vClipRegion;	

	if (drawSpriteParams.flipU)
	{
		ScalarV vOne = ScalarV(V_ONE);
		ScalarV vTemp = vClipRegion.GetX();
		vClipRegion.SetX(vOne - vClipRegion.GetY());
		vClipRegion.SetY(vOne - vTemp);

		vTemp = vClipRegionNormal.GetX();
		vClipRegionNormal.SetX(vOne - vClipRegionNormal.GetY());
		vClipRegionNormal.SetY(vOne - vTemp);
	}

	if (drawSpriteParams.flipV)
	{
		ScalarV vOne = ScalarV(V_ONE);
		ScalarV vTemp = vClipRegion.GetZ();
		vClipRegion.SetZ(vOne - vClipRegion.GetW());
		vClipRegion.SetW(vOne - vTemp);

		vTemp = vClipRegionNormal.GetZ();
		vClipRegionNormal.SetZ(vOne - vClipRegionNormal.GetW());
		vClipRegionNormal.SetW(vOne - vTemp);
	}

#if PTXDRAW_USE_INSTANCING
	pCurrInst->vClipRegion = vClipRegion;
#endif

	// calculate the texture coordinates
	Vec4V uvInfoA;
	Vec4V uvInfoB;
	CalcUVInfo(uvInfoA, drawInstParams.numTexColumns, drawInstParams.numTexRows, texFrameIdA, vClipMin, vClipMax, drawSpriteParams.flipU, drawSpriteParams.flipV);
	CalcUVInfo(uvInfoB, drawInstParams.numTexColumns, drawInstParams.numTexRows, texFrameIdB, vClipMin, vClipMax, drawSpriteParams.flipU, drawSpriteParams.flipV);

#if PTXDRAW_USE_INSTANCING
	pCurrInst->vMisc1 = uvInfoA;
	pCurrInst->vMisc2 = uvInfoB;
#endif

	const ScalarV clipBoundsU[3] = {vClipRegion.GetX(), ScalarV(V_HALF), vClipRegion.GetY()};
	const ScalarV clipBoundsV[3] = {vClipRegion.GetZ(), ScalarV(V_HALF), vClipRegion.GetW()};

	const ScalarV clipBoundsUN[3] = {vClipRegionNormal.GetX(), ScalarV(V_HALF), vClipRegionNormal.GetY()};
	const ScalarV clipBoundsVN[3] = {vClipRegionNormal.GetZ(), ScalarV(V_HALF), vClipRegionNormal.GetW()};

	if (drawInstParams.isProjected == false)
	{
#if PTXDRAW_USE_INSTANCING
		(void) clipBoundsU[0];
		(void) clipBoundsV[0];
		(void) clipBoundsUN[0];
		(void) clipBoundsVN[0];
#else
		// used to normalise the distance to the sprite centre that will be passed down to the shader in each vertex
		const ScalarV vInvSqrSpriteHalfLength = InvMag(vCentrePos - vSpriteBLPosWld);

		const ScalarV drawDegenerate = drawSpriteParams.bDrawDegenerateWhenAlphaCulled ? ScalarV(V_ZERO) : ScalarV(V_ONE);

		// calc the tex coord deltas
		Vec4V uvInfoN = uvInfoA;
		const ScalarV duA = uvInfoA.GetY() - uvInfoA.GetX();
		const ScalarV dvA = uvInfoA.GetZ() - uvInfoA.GetW();
		const ScalarV duB = uvInfoB.GetY() - uvInfoB.GetX();
		const ScalarV dvB = uvInfoB.GetZ() - uvInfoB.GetW();
		const ScalarV duN = uvInfoN.GetY() - uvInfoN.GetX();
		const ScalarV dvN = uvInfoN.GetZ() - uvInfoN.GetW();
		
		for (int j=0; j<drawInstParams.numVertsPerSprite; j++)
		{
			const int xPosIndex = drawInstParams.pPosIdxTable[j*2];
			const int yPosIndex = drawInstParams.pPosIdxTable[(j*2)+1];

			const ScalarV tRight = clipBoundsU[xPosIndex];
			const ScalarV tUp = clipBoundsV[yPosIndex];

			
			const Vec3V vVtxPosWld = Scale(AddScaled(AddScaled(vSpriteBLPosWld, vSpriteRight, tRight), vSpriteUp, tUp), drawDegenerate);

			pCurrVertex->vWorldPos = Vec4V(vVtxPosWld);

			Vec4V vUVInfoAB = Vec4V(	AddScaled(uvInfoA.GetX(), clipBoundsU[xPosIndex], duA),
										AddScaled(uvInfoA.GetW(), clipBoundsV[yPosIndex], dvA),
										AddScaled(uvInfoB.GetX(), clipBoundsU[xPosIndex], duB),
										AddScaled(uvInfoB.GetW(), clipBoundsV[yPosIndex], dvB)	);



			Vec4V vUVInfoN_DistToCentre = Vec4V(	AddScaled(uvInfoN.GetX(), clipBoundsUN[xPosIndex], duN),
													AddScaled(uvInfoN.GetW(), clipBoundsVN[yPosIndex], dvN),
													Mag(vCentrePos-vVtxPosWld)*vInvSqrSpriteHalfLength,
													ScalarV(V_ZERO) );

			pCurrVertex->vTexAUV_TexBUV_NormUV_distToCentre = Float16Vec8Pack( vUVInfoAB, vUVInfoN_DistToCentre );

			// point to the next vertex data in the merged structure
			u8* pTemp = (u8*)pCurrVertex;
			pTemp += sizeof(ptxSpriteVtxCommon);
			pCurrVertex = (ptxSpriteVertex*)pTemp;
		}
#endif //PTXDRAW_USE_INSTANCING
	}
	else // projected sprite
	{
		Vec3V vHeightVec(Vec2V(V_ZERO), ScalarV(drawInstParams.projectionDepth*0.5f));
		Vec3V vForwardVec(vSpriteUp.GetXY(), ScalarV(V_ZERO));
		Vec3V vSideVec(vSpriteRight.GetXY(), ScalarV(V_ZERO));

#if PTXDRAW_USE_INSTANCING
		pCurrInst->vMisc1 = Vec4V(vSpriteUp.GetXY(), vSpriteRight.GetXY());
		pCurrInst->vMisc2 = Vec4V(drawInstParams.projectionDepth, (float)texFrameIdA, (float)texFrameIdB, (float)drawInstParams.numTexColumns);
#else
		Vec3V vVerts[8];
		vVerts[0] = vSpritePos + vForwardVec - vSideVec + vHeightVec;
		vVerts[1] = vSpritePos + vForwardVec + vSideVec + vHeightVec;
		vVerts[2] = vSpritePos - vForwardVec + vSideVec + vHeightVec;
		vVerts[3] = vSpritePos - vForwardVec - vSideVec + vHeightVec;	

		vVerts[4] = SubtractScaled(vVerts[0], vHeightVec, ScalarV(V_TWO));		
		vVerts[5] = SubtractScaled(vVerts[1], vHeightVec, ScalarV(V_TWO));		
		vVerts[6] = SubtractScaled(vVerts[2], vHeightVec, ScalarV(V_TWO));
		vVerts[7] = SubtractScaled(vVerts[3], vHeightVec, ScalarV(V_TWO));

		const int xIndexA = texFrameIdA % drawInstParams.numTexColumns;
		const int yIndexA = texFrameIdA / drawInstParams.numTexColumns;
		const int xIndexB = texFrameIdB % drawInstParams.numTexColumns;
		const int yIndexB = texFrameIdB / drawInstParams.numTexColumns;

		for (int k = 0; k < drawInstParams.numVertsPerSprite; k++) 
		{
#if !PTXDRAW_SPRITE_USE_INDEXED_VERTS
			const u8  projBoxVtxIdx[PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED]=PTXDRAW_SPRITE_INDICES_PROJECTED;
			const Vec3V vVtxPosWld = vVerts[projBoxVtxIdx[k]];	
#else
			const Vec3V vVtxPosWld = vVerts[k];	
#endif
			pCurrVertex->vWorldPos = Vec4V(vVtxPosWld);

			Vec4V vUVInfoAB = Vec4V(	(float)xIndexA,
										(float)yIndexA,
										(float)xIndexB,
										(float)yIndexB	);

			Vec4V vUVInfoN_ProjDir = Vec4V(	Vec2V(V_ONE), vForwardVec.GetXY() );

			pCurrVertex->vTexAUV_TexBUV_NormUV_distToCentre = Float16Vec8Pack( vUVInfoAB, vUVInfoN_ProjDir );

			// point to the next vertex data in the merged structure
			u8* pTemp = (u8*)pCurrVertex;
			pTemp += sizeof(ptxSpriteVtxCommon);
			pCurrVertex = (ptxSpriteVertex*)pTemp;
		}
#endif
	}

	//Removed all the construction code from the for loop and dumped them into a function
	//Also, made sure that the function is called only once for 360/PC, and then it would copy the object 
	// in the for loop (instead of reconstructing the object with identical details every time in the for loop

#if PTXDRAW_USE_INSTANCING
	SetupPtxSpriteInst(	pCurrInst, drawInstParams, drawSpriteParams, vCentrePos, vSpriteUp, vSpriteNormal);
#else
	ptxSpriteInst ptxSpriteInstProperties;
	SetupPtxSpriteInst(	&ptxSpriteInstProperties, drawInstParams, drawSpriteParams, vCentrePos, vSpriteUp, vSpriteNormal);
#endif

#if PTXDRAW_USE_INSTANCING
	pCurrInst++;
#else
	for (int j=0; j<drawInstParams.numVertsPerSprite; j++)
	{

		*pCurrInst = ptxSpriteInstProperties;
		// point to the next inst data in the merged structure
		u8* pTemp = (u8*)pCurrInst;
		pTemp += sizeof(ptxSpriteVtxCommon);
		pCurrInst = (ptxSpriteInst*)pTemp;

		pCurrCommon++;  
	}
#endif //PTXDRAW_USE_INSTANCING

	/*
	// TODO -- something like this ... (and don't loop above)
	ptxSpriteInst inst0 = pCurrInst[0];
	for (int j = 1; j < drawInstParams.numVertsPerSprite; j++)
	{
		*(pCurrInst++) = inst0;
	}
	*/
}

#if RMPTFX_BANK
void ptxDrawInterface::InitRenderStateBlocks() 
{

	grcRasterizerStateDesc rsd;
	rsd.CullMode = grcRSV::CULL_NONE;
	rsd.FillMode = grcRSV::FILL_WIREFRAME;
	sm_debugWireframeRasterizerState = grcStateBlock::CreateRasterizerState(rsd);

	rsd.FillMode = grcRSV::FILL_SOLID;
	sm_exitDebugWireframeRasterizerState = grcStateBlock::CreateRasterizerState(rsd);

	grcBlendStateDesc bsd;
	bsd.BlendRTDesc[0].BlendEnable = 1;
	bsd.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	bsd.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_SRCALPHA;
	bsd.BlendRTDesc[0].BlendOp = grcRSV::BLENDOP_ADD;
	sm_debugWireframeBlendState = grcStateBlock::CreateBlendState(bsd);
	sm_exitDebugWireframeBlendState = sm_debugWireframeBlendState;

	grcDepthStencilStateDesc dssd;
	dssd.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	sm_debugWireframeDepthStencilState = grcStateBlock::CreateDepthStencilState(dssd);
	sm_exitDebugWireframeDepthStencilState = sm_debugWireframeDepthStencilState;

}

void ptxDrawInterface::SetDebugWireframeRenderState()
{
	grcStateBlock::SetRasterizerState(sm_debugWireframeRasterizerState);
	grcStateBlock::SetBlendState(sm_debugWireframeBlendState);
	grcStateBlock::SetDepthStencilState(sm_debugWireframeDepthStencilState);
}

void ptxDrawInterface::RestoreDefaultRenderState()
{
	grcStateBlock::SetRasterizerState(sm_exitDebugWireframeRasterizerState);
	grcStateBlock::SetBlendState(sm_exitDebugWireframeBlendState);
	grcStateBlock::SetDepthStencilState(sm_exitDebugWireframeDepthStencilState);
}

#endif // RMPTFX_BANK

} // namespace rage
