// 
// rmptfx/ptxd_drawtrail.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "rmptfx/ptxd_drawtrail.h"
#include "rmptfx/ptxd_drawtrail_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxparticlerule.h"
#include "rmptfx/ptxu_colour.h"
#include "rmptfx/ptxu_size.h"

#if __OPENGL || __D3D
#include "grmodel/shaderfx.h"
#endif

#include "system/timer.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{



// static variables
	//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
	ptxKeyframeDefn ptxd_Trail::sm_texInfoDefn(						"Texture Info",				atHashValue("ptxd_Trail:m_texInfoKFP"),						PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ONE),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		100.0f,		0.01f),		"Wrap Length Min",	"Wrap Length Max",	"Offset Min",	"Offset Max");


// code
#if RMPTFX_XML_LOADING
ptxd_Trail::ptxd_Trail()
{
	// create keyframes
	m_texInfoKFP.Init(Vec4V(V_ONE), atHashValue("ptxd_Trail:m_texInfoKFP"));

	m_keyframePropList.AddKeyframeProp(&m_texInfoKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxd_Trail::SetDefaultData()
{
	// default keyframes
	m_texInfoKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));

	// default other data
	m_alignAxis = Vec3V(V_Z_AXIS_WZERO);

	m_alignmentMode = PTXD_TRAIL_ALIGNMENTMODE_CAMERA;

	m_tessellationU = 2;
	m_tessellationV = 4;

	m_smoothnessX = 0.0f;
	m_smoothnessY = 0.0f;

	m_projectionDepth = 0.5f;
	m_shadowCastIntensity = 0.0f;

	m_flipU = false;
	m_flipV = false;
	m_wrapTextureOverParticleLife = false;
	m_disableDraw = false;
}

ptxd_Trail::ptxd_Trail(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_texInfoKFP(rsc)
	, m_alignAxis(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxd_Trail_num++;
#endif
}

#if __DECLARESTRUCT
void ptxd_Trail::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxd_Trail, ptxBehaviour)
		SSTRUCT_FIELD(ptxd_Trail, m_texInfoKFP)
		SSTRUCT_FIELD(ptxd_Trail, m_alignAxis)
		SSTRUCT_FIELD(ptxd_Trail, m_alignmentMode)
		SSTRUCT_FIELD(ptxd_Trail, m_tessellationU)
		SSTRUCT_FIELD(ptxd_Trail, m_tessellationV)
		SSTRUCT_FIELD(ptxd_Trail, m_smoothnessX)
		SSTRUCT_FIELD(ptxd_Trail, m_smoothnessY)
		SSTRUCT_FIELD(ptxd_Trail, m_projectionDepth)
		SSTRUCT_FIELD(ptxd_Trail, m_shadowCastIntensity)
		SSTRUCT_FIELD(ptxd_Trail, m_flipU)
		SSTRUCT_FIELD(ptxd_Trail, m_flipV)
		SSTRUCT_FIELD(ptxd_Trail, m_wrapTextureOverParticleLife)
		SSTRUCT_FIELD(ptxd_Trail, m_disableDraw)
	SSTRUCT_END(ptxd_Trail)
}
#endif

IMPLEMENT_PLACE(ptxd_Trail);

void ptxd_Trail::UpdateFinalize(ptxBehaviourParams& params)
{
	if (m_smoothnessX>0.0f || m_smoothnessY>0.0f)
	{
		ptxAssertf(params.pPointItem, "invalid point passed in");

		ptxPointItem* pPointItemCurr = params.pPointItem;
		ptxPointItem* pPointItemPrev = (ptxPointItem*)pPointItemCurr->GetPrev();
		ptxPointItem* pPointItemNext = (ptxPointItem*)pPointItemCurr->GetNext();

		const ScalarV vSmoothnessY = ScalarVFromF32(m_smoothnessY);
		const ScalarV vSmoothnessX = ScalarVFromF32(m_smoothnessX);

		if (pPointItemPrev && pPointItemNext)
		{
			const Vec3V vPosCurr = pPointItemCurr->GetDataDB()->GetCurrPos();
			const Vec3V vPosPrev = pPointItemPrev->GetDataDB()->GetCurrPos();
			const Vec3V vPosNext = pPointItemNext->GetDataDB()->GetCurrPos();
			const Vec3V vPrevToCurr = vPosCurr - vPosPrev;
			const Vec3V vPrevToNext = vPosNext - vPosPrev;

			// calculate the projection of vPosCurr onto vPrevToNext
			const Vec3V vPosProj = vPosPrev + vPrevToNext*(Dot(vPrevToCurr, vPrevToNext)/Dot(vPrevToNext, vPrevToNext));

			// calculate the mid point between vPosPrev and vPosNext
			const Vec3V vPosMid = (vPosNext+vPosPrev) * ScalarV(V_HALF);

			// calculate the new position
			Vec3V vNewPosCurr = vPosCurr;

			// apply smoothness towards the projection position
			vNewPosCurr += (vPosProj-vPosCurr) * vSmoothnessY;		

			// apply smoothness towards the midpoint position
			vNewPosCurr += (vPosMid-vPosCurr) * vSmoothnessX;																				

			// set the new position
			pPointItemCurr->GetDataDB()->SetCurrPos(vNewPosCurr);
		}
	}
}

void ptxd_Trail::DrawPoints(ptxEffectInst* RESTRICT pEffectInst,
							 ptxEmitterInst* RESTRICT pEmitterInst,
							 ptxParticleRule* RESTRICT pParticleRule,
							 ptxEvolutionList* RESTRICT pEvoList
							 PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface))
{
	ptxAssertf(pEffectInst, "missing effect instance");
	ptxAssertf(pEmitterInst, "missing emitter instance");
	ptxAssertf(pParticleRule->GetPointPool() , "missing point pool");

	// MN - why are we passing through a particle rule when it can be got from the emitter inst
	//    - if this assert never fires then they're the same and we can stop passing it through
	ptxAssertf(pParticleRule == pEmitterInst->GetParticleRule(), "particle rule mismatch");

	ptxAssertf(pEmitterInst->GetEmitterRule(), "can't get emitter rule from emitter inst");
	ptxAssertf(pEmitterInst->GetEmitterRule()->GetCreationDomainObj().GetDomain(), "can't get emitter creation domain from emitter inst");

#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif

	if(m_disableDraw)
	{
		return;
	}

#if RMPTFX_USE_PARTICLE_SHADOWS
	//If shadow cast intensity is really low, lets skip it
	if(RMPTFXMGR.IsShadowPass() && (m_shadowCastIntensity * RMPTFXMGR.GetGlobalShadowIntensity()) < RMPTFX_MIN_PARTICLE_SHADOW_INTENSITY)
	{
		return;
	}
#endif

	// return if there are no points to draw
	const unsigned drawBufferId = RMPTFXTHREADER.GetDrawBufferId();
	if (pEmitterInst->GetPointList().GetNumItemsMT(drawBufferId)==0)
	{
		return;
	}

	// sort the points
	unsigned numPoints = pEmitterInst->GetNumActivePointsMT(drawBufferId);
	ptxPointItem** ppPoints = Alloca(ptxPointItem*, numPoints);
	const unsigned numPoints2 = pEmitterInst->SortPoints(ppPoints);
	Assert(numPoints >= numPoints2);
	numPoints = numPoints2;

	// set the new render state
	grcRasterizerStateHandle RS_Prev = grcStateBlock::RS_Active;
	grcBlendStateHandle BS_Prev = grcStateBlock::BS_Active;
	grcDepthStencilStateHandle DSS_Prev = grcStateBlock::DSS_Active;
	pParticleRule->SetRenderState(pParticleRule->GetShaderInst().GetProjectionMode());
	grcViewport::SetCurrentWorldIdentity();
	

	// set up the shader variables
	pParticleRule->GetShaderInst().SetShaderVars(pParticleRule->GetBlendSet());

	// call pre draw in all cases - this will not do any redundant work (i.e. lights in area logic, etc.) 
	// for entities flagged as PTXEFFECTFLAG_MANUAL_DRAW, and will  ensure that sorted 
	// entities also get the correct ambient globals
	if (pEffectInst->GetRenderSetup()) 
	{ 
		Vec3V vBBMin = pEmitterInst->GetDataDBMT(drawBufferId)->GetCurrBoundBoxMin();
		Vec3V vBBMax = pEmitterInst->GetDataDBMT(drawBufferId)->GetCurrBoundBoxMax();

		if (ptxVerifyf(IsLessThanOrEqualAll(vBBMin, vBBMax), "%s - %s - min bound box is greater than max (min:%.3f, %.3f, %.3f max:%.3f, %.3f, %.3f", pEffectInst->GetEffectRule()->GetName(), pEmitterInst->GetEmitterRule()->GetName(), vBBMin.GetXf(), vBBMin.GetYf(), vBBMin.GetZf(), vBBMax.GetXf(), vBBMax.GetYf(), vBBMax.GetZf()))
		{
			if (!pEffectInst->GetRenderSetup()->PreDraw(pEffectInst, pEmitterInst, vBBMin, vBBMax, pParticleRule->GetShaderInst().GetShaderTemplateHashName(), pParticleRule->GetShaderInst().GetGrmShader()))
			{
				ptxWarningf("Error in Particle Setup for %s , aborting rendering particles", pParticleRule->GetShaderInst().GetShaderTemplateName());
				return;
			}
		}
	}

	// query the texture info keyframes
	ScalarV vKeyTimeEffect = pEffectInst->GetKeyTime();
	Vec4V vTexInfo = m_texInfoKFP.Query(vKeyTimeEffect, pEvoList, pEffectInst->GetEvoValues());
	ScalarV vBias = ptxRandomTable::GetValueV(pEffectInst->GetTrailTexInfoRandIndex());
	ScalarV vTexWrapLength = Lerp(vBias, SplatX(vTexInfo), SplatY(vTexInfo));
	ScalarV vTexOffset = Lerp(vBias, SplatZ(vTexInfo), SplatW(vTexInfo));

	// query the anchor size and colour of a fake particle
	ptxPointItem fakePointItem;
	ptxPointSB* RESTRICT pPointSB = fakePointItem.GetDataSB();
	ptxPointDB* RESTRICT pPointDB = fakePointItem.GetDataDB();
	pPointSB->Init();
	pPointDB->Init();

	ptxBehaviourParams params;
	pParticleRule->SetParams(params, pEffectInst, pEmitterInst, pEvoList, &fakePointItem, ScalarV(V_ZERO), ScalarV(V_ZERO), drawBufferId);

	pPointSB->SetFlag(PTXPOINT_FLAG_IS_TRAIL);
	for (int i=0; i<PTXEVO_MAX_EVOLUTIONS; i++)
	{
		pPointSB->SetEvoValue(i, pEffectInst->GetEvoValueFromIndex(i));
	}

	ptxu_Size* pSizeBehaviour = static_cast<ptxu_Size*>(pParticleRule->GetBehaviour("ptxu_Size"));
	if (pSizeBehaviour)
	{
		pSizeBehaviour->InitPoint(params);
	}

	ptxu_Colour* pColourBehaviour = static_cast<ptxu_Colour*>(pParticleRule->GetBehaviour("ptxu_Colour"));
	if (pColourBehaviour)
	{
		pColourBehaviour->InitPoint(params);
	}

	ScalarV vAnchorSize = fakePointItem.GetDataDB()->GetDimensions().GetX();
	Vec4V vAnchorCol = fakePointItem.GetDataDB()->GetColour().GetRGBA();
	Vec3V vAnchorPos = pEmitterInst->GetDataDBMT(drawBufferId)->GetCurrPosWld();

	// draw the trail
	ptxDrawInterface::DrawTrail(pEffectInst, pEmitterInst, this, pParticleRule, pEvoList, vAnchorPos, vAnchorSize, vAnchorCol, vTexWrapLength, vTexOffset PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));

	grcStateBlock::SetRasterizerState(RS_Prev);
	grcStateBlock::SetBlendState(BS_Prev);
	grcStateBlock::SetDepthStencilState(DSS_Prev);
	// do any post draw only if the fx instance is not sorted - i.e. no PTXEFFECTFLAG_MANUAL_DRAW flag,
	// as in that case pre draw setup has already been done during the regular render list building.
	if (!pEffectInst->GetFlag(PTXEFFECTFLAG_MANUAL_DRAW) && pEffectInst->GetRenderSetup())
	{
		pEffectInst->GetRenderSetup()->PostDraw(pEffectInst, pEmitterInst);
	}

	// clear the shader variables
	pParticleRule->GetShaderInst().ClearShaderVars();

	// update the editor cpu draw time
#if RMPTFX_EDITOR
	pEmitterInst->GetEmitterRule()->GetUIData().IncAccumCPUDrawTime(timer.GetMsTime()); 
#endif
}

void ptxd_Trail::SetKeyframeDefns()
{
	m_texInfoKFP.GetKeyframe().SetDefn(&sm_texInfoDefn);
}

void ptxd_Trail::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_texInfoKFP, &sm_texInfoDefn);
}


// editor code
#if RMPTFX_EDITOR
void ptxd_Trail::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 15);

	//Option for Disable
	SendBoolDefnToEditor(buff, "Disable Draw");

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Texture Info");

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Align Axis X", -1.0f, 1.0f);
	SendFloatDefnToEditor(buff, "Align Axis Y", -1.0f, 1.0f);
	SendFloatDefnToEditor(buff, "Align Axis Z", -1.0f, 1.0f);

	const char* pComboItemNames[PTXD_TRAIL_ALIGNMENTMODE_NUM] = {"Camera", "World", "Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Alignment Mode", PTXD_TRAIL_ALIGNMENTMODE_NUM, pComboItemNames);

	SendIntDefnToEditor(buff, "Tessellation U", 0, 5);
	SendIntDefnToEditor(buff, "Tessellation V", 1, 6);

	SendFloatDefnToEditor(buff, "Smoothness X", 0.0f, 1.0f);
	SendFloatDefnToEditor(buff, "Smoothness Y", 0.0f, 1.0f);

	SendFloatDefnToEditor(buff, "Projection Depth", 0.0f, 5.0f);
	SendFloatDefnToEditor(buff, "Shadow Cast Intensity", 0.0f, 1.0f);

	SendBoolDefnToEditor(buff, "Flip U");
	SendBoolDefnToEditor(buff, "Flip V");
	SendBoolDefnToEditor(buff, "Wrap Texture Over Particle Life");
}

void ptxd_Trail::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	//Option for Disable
	SendBoolToEditor(buff, m_disableDraw);

	// send keyframes
	SendKeyframeToEditor(buff, m_texInfoKFP, pParticleRule);

	// send non-keyframes
	SendVecToEditor(buff, m_alignAxis);

	SendComboItemToEditor(buff, m_alignmentMode);

	SendIntToEditor(buff, m_tessellationU);
	SendIntToEditor(buff, m_tessellationV);

	SendFloatToEditor(buff, m_smoothnessX);
	SendFloatToEditor(buff, m_smoothnessY);

	SendFloatToEditor(buff, m_projectionDepth);
	SendFloatToEditor(buff, m_shadowCastIntensity);

	SendBoolToEditor(buff, m_flipU);
	SendBoolToEditor(buff, m_flipV);
	SendBoolToEditor(buff, m_wrapTextureOverParticleLife);
}

void ptxd_Trail::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	//Option for Disable
	ReceiveBoolFromEditor(buff, m_disableDraw);
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_texInfoKFP);

	// receive non-keyframes
	ReceiveVecFromEditor(buff, m_alignAxis);

	ReceiveComboItemFromEditor(buff, m_alignmentMode);

	ReceiveIntFromEditor(buff, m_tessellationU);
	ReceiveIntFromEditor(buff, m_tessellationV);

	ReceiveFloatFromEditor(buff, m_smoothnessX);
	ReceiveFloatFromEditor(buff, m_smoothnessY);

	ReceiveFloatFromEditor(buff, m_projectionDepth);
	ReceiveFloatFromEditor(buff, m_shadowCastIntensity);

	ReceiveBoolFromEditor(buff, m_flipU);
	ReceiveBoolFromEditor(buff, m_flipV);
	ReceiveBoolFromEditor(buff, m_wrapTextureOverParticleLife);
	
}
#endif //_RMPTFX_INTERFACE


} // namespace rage
