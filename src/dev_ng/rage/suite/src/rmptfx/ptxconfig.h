// 
// rmptfx/ptxconfig.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXCONFIG_H
#define RMPTFX_PTXCONFIG_H

// all code needed to allow full tuning with the editor interface and error messages
#define RMPTFX_EDITOR										(__DEV && !__RESOURCECOMPILER)

#if RMPTFX_EDITOR
#define RMPTFX_EDITOR_ONLY(x)								x
#else
#define RMPTFX_EDITOR_ONLY(x)
#endif

// code needed for loading raw particle XML files
// note that RMPTFX_EDITOR depends on being able to do xml loading
#define RMPTFX_XML_LOADING									(RMPTFX_EDITOR || __TOOL || __RESOURCECOMPILER)

#define PTXKEYFRAME_VERSION									1

// tells the game to use the original timeline ordering - set to 0 to reorder based on the editor inputs and set to RMPTFX_EDITOR for everything else
#define RMPTFX_BANK											(__BANK && !__RESOURCECOMPILER)
#define RMPTFX_THOROUGH_ERROR_CHECKING						(0 && !__FINAL)
#define RMPTFX_TIMERS										(RMPTFX_EDITOR && 1) //disabling ptx timers in bank release as it screws the overall timing

#if RMPTFX_BANK
#define RMPTFX_BANK_ONLY(...)								__VA_ARGS__
#else
#define RMPTFX_BANK_ONLY(...)
#endif

#if RMPTFX_BANK
#define RMPTFX_BANK_SWITCH(_if_BANK_,_else_)				_if_BANK_
#else
#define RMPTFX_BANK_SWITCH(_if_BANK_,_else_)				_else_
#endif

// alpha culling
#define PTXDRAW_USE_ALPHA_CULL								(0)
#define PTXDRAW_ALPHA_CULL_THRESHOLD						(1.0f/255.0f)

// used for specifying what order the behaviours should be applied in.
#define RMPTFX_BEHORDER_AGE									10.0f
#define RMPTFX_BEHORDER_ACCELERATION						20.0f
#define RMPTFX_BEHORDER_ATTRACTOR							30.0f
#define RMPTFX_BEHORDER_VELOCITY							40.0f
#define RMPTFX_BEHORDER_PAUSE								45.0f
#define RMPTFX_BEHORDER_ROTATION							50.0f
#define RMPTFX_BEHORDER_NOISE								60.0f
#define RMPTFX_BEHORDER_SIZE								70.0f
#define RMPTFX_BEHORDER_DAMPENING							80.0f
#define RMPTFX_BEHORDER_MATRIXWEIGHT						90.0f
#define RMPTFX_BEHORDER_RIVER								100.0f						// defined in framework
#define RMPTFX_BEHORDER_WIND								110.0f
#define RMPTFX_BEHORDER_ZCULL								120.0f						// defined in framework
#define RMPTFX_BEHORDER_COLLISION							130.0f
#define RMPTFX_BEHORDER_ANIMATETEXTURE						140.0f
#define RMPTFX_BEHORDER_COLOUR								150.0f
#define RMPTFX_BEHORDER_FIRE								160.0f						// defined in framework
#define RMPTFX_BEHORDER_DECAL								170.0f						// defined in framework
#define RMPTFX_BEHORDER_DECALPOOL							180.0f						// defined in framework
#define RMPTFX_BEHORDER_DECALTRAIL							190.0f						// defined in framework
#define RMPTFX_BEHORDER_LIQUID								200.0f						// defined in framework
#define RMPTFX_BEHORDER_LIGHT								210.0f						// defined in framework
#define RMPTFX_BEHORDER_FOGVOLUME							220.0f						// defined in framework
#define RMPTFX_BEHORDER_DRAWSPRITE							230.0f
#define RMPTFX_BEHORDER_DRAWTRAIL							240.0f
#define RMPTFX_BEHORDER_DRAWMODEL							250.0f

#define RMPTFX_FORCE_DRAW_BUCKET							-1

// optimisations
#define RMPTFX_OPTIMISATIONS_OFF							0							// PRAGMA-OPTIMIZE-ALLOW
#define RMPTFX_BEHAVIOUR_OPTIMISATIONS_OFF					0							// PRAGMA-OPTIMIZE-ALLOW

#if RMPTFX_OPTIMISATIONS_OFF															// PRAGMA-OPTIMIZE-ALLOW
#define RMPTFX_OPTIMISATIONS()								OPTIMISATIONS_OFF()			// PRAGMA-OPTIMIZE-ALLOW
#else
#define RMPTFX_OPTIMISATIONS()
#endif

#if RMPTFX_BEHAVIOUR_OPTIMISATIONS_OFF													// PRAGMA-OPTIMIZE-ALLOW
#define RMPTFX_BEHAVIOUR_OPTIMISATIONS()					OPTIMISATIONS_OFF()			// PRAGMA-OPTIMIZE-ALLOW
#else
#define RMPTFX_BEHAVIOUR_OPTIMISATIONS()
#endif

#define RMPTFX_BIGGER_UPDATE_THREAD_STACK_FOR_DEBUGGING		0

#define IMPLEMENT_BLEND_MODES_AS_PASSES						1							// see counterpart in ptfx_common.fxh

#define RMPTFX_MAX_PASSES									6

#define RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES			0

// particle shadows rely on the extra passes
#define RMPTFX_USE_PARTICLE_SHADOWS							(1 && IMPLEMENT_BLEND_MODES_AS_PASSES && (__D3D11 || RSG_DURANGO || RSG_ORBIS))
#define RMPTFX_MIN_PARTICLE_SHADOW_INTENSITY				(0.01f)

// dynamic switching between hi/low res particles
#define PTFX_DYNAMIC_QUALITY								(0 && !__RESOURCECOMPILER)

// number of parallel particle update tasks
#define RMPTFX_MAX_PARTICLE_UPDATE_TASKS					4

//Detailed timebars for rmptfx update
#define RMPTFX_UPDATE_CAPTURE_MARKER						0



#if RMPTFX_UPDATE_CAPTURE_MARKER

#if RSG_ORBIS
#include <perf.h>
#endif

class ptxUpdateParallelCaptureMarker
{
public:
	ptxUpdateParallelCaptureMarker(const char* str) 
	{
#if RSG_ORBIS
		sceRazorCpuPushMarker(str, SCE_RAZOR_COLOR_GREEN, SCE_RAZOR_MARKER_DISABLE_HUD);
#else
		PIXBeginC(1,0x00FFFF,str);
#endif
	}

	~ptxUpdateParallelCaptureMarker() 
	{
#if RSG_ORBIS
		sceRazorCpuPopMarker();
#else
		PIXEnd();
#endif
	}
};
#define RMPTFX_UPDATE_CAPTURE_MARKER_PUSH_POP(_fmt, ...)		\
	char str[256];								\
	formatf(str, _fmt, ##__VA_ARGS__);			\
	ptxUpdateParallelCaptureMarker MacroJoin(ptxUpdateParallelCaptureMarker_, __LINE__)(str)

#else // RMPTFX_UPDATE_CAPTURE_MARKER
#define RMPTFX_UPDATE_CAPTURE_MARKER_PUSH_POP(_fmt, ...)
#endif // RMPTFX_UPDATE_CAPTURE_MARKER

#endif // RMPTFX_PTXCONFIG_H
