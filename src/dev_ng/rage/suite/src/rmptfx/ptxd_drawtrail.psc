<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxd_Trail" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_texInfoKFP" type="::rage::ptxKeyframeProp"/>
	<Vector3 name="m_alignAxis"/>
	<int name="m_alignmentMode"/>
	<int name="m_tessellationU"/>
	<int name="m_tessellationV"/>
	<int name="m_smoothnessX"/>
	<int name="m_smoothnessY"/>
	<float name="m_projectionDepth"/>
  <float name="m_shadowCastIntensity"/>
	<bool name="m_flipU"/>
	<bool name="m_flipV"/>
	<bool name="m_wrapTextureOverParticleLife"/>
  <bool name="m_disableDraw"/>
</structdef>

</ParserSchema>