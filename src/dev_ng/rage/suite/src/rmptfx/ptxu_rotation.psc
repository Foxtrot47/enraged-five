<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Rotation" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_initialAngleMinKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_initialAngleMaxKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_angleMinKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_angleMaxKFP" type="::rage::ptxKeyframeProp"/>
	<int name="m_initRotationMode"/>
	<int name="m_updateRotationMode"/>
	<bool name="m_accumulateAngle"/>
	<bool name="m_rotateAngleAxes"/>
	<bool name="m_rotateInitAngleAxes"/>
  <float name="m_speedFadeThresh"/>
</structdef>

</ParserSchema>