<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxTechniqueDesc" cond="RMPTFX_XML_LOADING">
	<int name="m_diffuseMode"/>
	<int name="m_projMode"/>
	<bool name="m_isLit"/>
	<bool name="m_isSoft"/>
	<bool name="m_isScreenSpace"/>
	<bool name="m_isRefract"/>
	<bool name="m_isNormalSpec"/>
</structdef>

<structdef type="rage::ptxShaderInst" cond="RMPTFX_XML_LOADING" onPreLoad="PreLoad">
	<string name="m_shaderTemplateName" type="ConstString"/>
	<string name="m_shaderTemplateTechniqueName" type="ConstString"/>
	<struct name="m_techniqueDesc" type="rage::ptxTechniqueDesc"/>
	<array name="m_instVars" type="atArray">
		<pointer type="rage::ptxShaderVar" policy="owner"/>
	</array>
</structdef>

</ParserSchema>