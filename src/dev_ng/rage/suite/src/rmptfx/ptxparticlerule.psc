<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxParticleRule" cond="RMPTFX_XML_LOADING">
	<float name="m_fileVersion"/>
	<string name="m_name" type="ConstString"/>
	<struct name="m_renderState" type="ptxRenderState"/>
	<struct name="m_effectSpawnerAtRatio" type="rage::ptxEffectSpawner"/>
	<struct name="m_effectSpawnerOnColn" type="rage::ptxEffectSpawner"/>
	<int name="m_texFrameIdMin"/>
	<int name="m_texFrameIdMax"/>
	<array name="m_allBehaviours" type="atArray">
		<pointer type="::rage::ptxBehaviour" policy="owner"/>
	</array>
	<array name="m_biasLinks" type="atArray">
		<struct type="::rage::ptxBiasLink"/>
	</array>
	<struct name="m_shaderInst" type="::rage::ptxShaderInst"/>
	<array name="m_drawables" type="atArray" align="16">
		<struct type="::rage::ptxDrawable"/>
	</array>
	<u8 name="m_sortType"/>
	<u8 name="m_drawType"/>
</structdef>

</ParserSchema>