// 
// rmptfx/ptxchannel.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXCHANNEL_H 
#define RMPTFX_PTXCHANNEL_H 


// include (rage)
#include "diag/channel.h"
#include "file/remote.h"


// channels
RAGE_DECLARE_CHANNEL(rmptfx)

#define ptxAssert(cond)							RAGE_ASSERT(rmptfx,cond)
#define ptxAssertf(cond,fmt,...)				RAGE_ASSERTF(rmptfx,cond,fmt,##__VA_ARGS__)
#define ptxVerifyf(cond,fmt,...)				RAGE_VERIFYF(rmptfx,cond,fmt,##__VA_ARGS__)
#define ptxErrorf(fmt,...)						RAGE_ERRORF(rmptfx,fmt,##__VA_ARGS__)
#define ptxWarningf(fmt,...)					RAGE_WARNINGF(rmptfx,fmt,##__VA_ARGS__)
#define ptxDisplayf(fmt,...)					RAGE_DISPLAYF(rmptfx,fmt,##__VA_ARGS__)
#define ptxDebugf1(fmt,...)						RAGE_DEBUGF1(rmptfx,fmt,##__VA_ARGS__)
#define ptxDebugf2(fmt,...)						RAGE_DEBUGF2(rmptfx,fmt,##__VA_ARGS__)
#define ptxDebugf3(fmt,...)						RAGE_DEBUGF3(rmptfx,fmt,##__VA_ARGS__)
#define ptxLogf(severity,fmt,...)				RAGE_LOGF(rmptfx,severity,fmt,##__VA_ARGS__)
#define ptxCondLogf(cond,severity,fmt,...)		RAGE_CONDLOGF(cond,rmptfx,severity,fmt,##__VA_ARGS__)


// namespaces
namespace rage 
{


// classes
class ptxMsg
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if __FINAL
		static inline void AssetErrorf(const char*, ...) {}
#else
		static void AssetErrorf(const char* pMessage, ...)
		{
			va_list args;
			va_start(args, pMessage);
			char buff[1024];
			vformatf(buff, sizeof(buff), pMessage, args);
			va_end(args);
			
#if __RESOURCECOMPILER
			ptxErrorf("%s", buff);
#else
			ptxLogf(DIAG_SEVERITY_WARNING, "%s", buff);
#endif
		}
#endif
	
#if __FINAL
		static inline void AssetWarningf(const char*, ...) {}
#else
		static void AssetWarningf(const char* pMessage, ...)
		{
			va_list args;
			va_start(args, pMessage);
			char buff[1024];
			vformatf(buff, sizeof(buff), pMessage, args);
			va_end(args);

#if __RESOURCECOMPILER
			ptxWarningf("%s", buff);
#else
			ptxLogf(DIAG_SEVERITY_WARNING, "%s", buff);
#endif
		}
#endif
};


} // namespace rage


#endif // RMPTFX_PTXCHANNEL_H 
