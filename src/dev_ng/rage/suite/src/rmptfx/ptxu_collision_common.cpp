// 
// rmptfx/ptxu_collision_common.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "grprofile/drawmanager.h"
#include "rmptfx/ptxeffectinst.h"
#include "vector/colors.h"
#include "vector/geometry.h"


// namepaces
namespace rage
{


// code
// copied from vector/geometry.cpp
int My_FindImpactPolygonToSphere(Vec3V_ConstRef vSphereCenter, ScalarV_In vSphereRadius, const Vec3V_Ptr vVertices,
	int numVertices, Vec3V_ConstRef vPolyNormal, Vec3V_InOut vSpherePosition,
	Vec3V_InOut vPolyPosition, int& idNum, Vec3V_InOut vNormal, ScalarV_InOut vDepth)
{

	Vec3V vPolySide;
		// A vector from the first vertex in an edge to the second vertex in an edge.
	Vec3V vEdgeNormal;
		// The edge normal, i.e. a vector that points into the polygon, perpendicular
		// to the edge & the polygon's normal vector.
		// (Note that, most of the time, edge-normals are defined to point outside the
		// polygon. This one doesn't.)
	Vec3V vVert1ToCenter;
		// A vector from the first vertex in an edge, to the sphere center.  Alternately,
		// one could consider this the location of the sphere center, in the first vertex's
		// "coordinate system".
	Vec3V vVert2ToCenter;
		// A vector from the second vertex in an edge, to the sphere center.  Alternately,
		// one could consider this the location of the sphere center, in the second vertex's
		// "coordinate system".
	ScalarV vDistanceSphereCenterToPlane;
		// The distance from the sphere center to the polygon's plane.  (This is calculated
		// near the beginning, as a "quick reject", but the value is needed later.)
	ScalarV vDistance;
		// Other distances that get calculated along the way.
	int index, nextIndex = 0;
		// The index of the first & second vertices in an edge, respectively.
	int insideThisEdge[4];
		// For each edge of the polygon, this tracks where the sphere center is relative
		// to the edge.  -1 means the first vertex is closest to the sphere center, -2
		// means the second vertex is closest to the sphere center, and other values
		// mean that neither vertex is the closest polygon feature to the sphere center.
	bool allInside=true;
		// True if the sphere's center is inside of all of the triangle's edges.

	// If the sphere doesn't intersect with the triangle's plane, we can stop now.
	// But if it does intersect, we may need that distance later.
	
	vVert1ToCenter = vSphereCenter-vVertices[0];
	vDistanceSphereCenterToPlane = Dot(vPolyNormal, vVert1ToCenter);

	if(IsGreaterThanAll(Abs(vDistanceSphereCenterToPlane), vSphereRadius))
	{
		return GEOM_NO_IMPACT;
	}

	// Determine where the sphere center is, relative to the polygon's edges.
	for(index=0;index<numVertices;index++)
	{
		// Calculate the index of the other vertex in the edge about to be tested.
		nextIndex = (index+1)%numVertices;

		// Calculate the location of the sphere center, in the first vertex's
		// "coordinate system".
		vVert1ToCenter = vSphereCenter-vVertices[index];

		// Generate a vector that represents the current edge of the polygon.
		vPolySide = vVertices[nextIndex]-vVertices[index];

		// Generate the edge's normal vector.  (It doesn't need to be normalized,
		// so don't pay for the square root or the division.)
		vEdgeNormal = Cross(vPolyNormal, vPolySide);

		// Initially, we don't know the relation between this edge and the sphere.
		insideThisEdge[index]=0;

		// Is the sphere center behind this edge?
		if(IsGreaterThanOrEqualAll(Dot(vEdgeNormal, vVert1ToCenter), ScalarV(V_ZERO)))
		{
			// The point on the polygon's plane closest to the sphere center is inside this edge.
			insideThisEdge[index]=1;
			continue;
		}

		// The point on the plane closest to the sphere center is outside this edge.
		// That means the projection of the sphere center onto the triangle's plane is
		// not inside the triangle.
		allInside=false;

		// Is the sphere center behind the edge?  (That is, if the sphere center was
		// projected onto an axis defined by the current edge, would it occur earlier
		// on the axis than the first vertex.  Another way to look at this is, if a
		// plane was constructed that lied on the first vertex and whose normal vector
		// was the edge vector, would the sphere center be behind that plane.)
		if(IsLessThanAll(Dot(vPolySide, vVert1ToCenter), ScalarV(V_ZERO)))
		{
			// Yes.  That means the closest point on the edge to the sphere is the
			// first vertex.
			insideThisEdge[index]=-1;		// -1 means vertex1 is the closest edge point
			continue;
		}

		// Is the sphere center in front of the edge?  (That is, if the sphere center was
		// projected onto an axis defined by the current edge, would it occur later
		// on the axis than the second vertex.  Another way to look at this is, if a
		// plane was constructed that lied on the second vertex and whose normal vector
		// was the edge vector, would the sphere center be in front of that plane.)
		vVert2ToCenter = vSphereCenter - vVertices[nextIndex];

		if(IsGreaterThanAll(Dot(vPolySide, vVert2ToCenter), ScalarV(V_ZERO)))
		{
			// Yes.  That means the closest point on the edge to the sphere is the
			// second vertex.
			insideThisEdge[index]=-2;		// -2 means vertex2 is the closest edge point
			continue;
		}

		// The closest point on this edge to the sphere center is not a vertex and
		// the closest plane point to the sphere center is outside this edge, so
		// a point on the interior of this edge is the closest polygon point to the
		// sphere center.  See if the sphere center is on the correct side of the edge, relative
		// to the polygon normal.


		if(IsLessThanOrEqualAll(Dot(vPolyNormal, vVert1ToCenter), ScalarV(V_ZERO)))
		{
			continue;
		}

		// Form a triangle with the 2 points of this edge & the sphere center.  Then,
		// calculate the height of the triangle from the point of view of the polygon's
		// edge being the base.  The area of a triangle is (base * height / 2); the area
		// of a triangle can also be calculated from the lengths of the sides using
		// Heron's formula.  Combine the two, and one can calculate the height of a
		// triangle from the length of the sides.  (Here, though, we calculate the square
		// of the height from the square of the lengths of the sides.)
		ScalarV vA2 = MagSquared(vVert1ToCenter);
		ScalarV vB2 = MagSquared(vVert2ToCenter);
		ScalarV vC2 = MagSquared(vPolySide);
		ScalarV vInverseC2 = Invert(vC2);

		const ScalarV vQuarter(0.25f);
		const ScalarV vTwo(V_TWO);
		ScalarV vA2PlusB2 = (vA2+vB2);
		ScalarV vA2MinusB2Sqr = (vA2-vB2);
		vA2MinusB2Sqr = vA2MinusB2Sqr*vA2MinusB2Sqr;

		// distance2=0.25f*(2.0f*(a2+b2)-c2-square(a2-b2)*inverseC2)
		ScalarV vDistance2 = vQuarter*(vTwo*vA2PlusB2-vC2-vA2MinusB2Sqr*vInverseC2);



		if(IsGreaterThanAll(vDistance2,vSphereRadius*vSphereRadius))
		{
			// The whole polygon is outside the sphere.
			return GEOM_NO_IMPACT;
		}

		// The polygon intersects the sphere, at this edge.
		// We had the distance squared; we need the distance.
		vDistance = SqrtSafe(vDistance2);

		// This edge point is inside the sphere.
		idNum = index;		// idnum refers to the edge number in the polygon
		vDepth = vSphereRadius-vDistance;

		// Calculate the location of the impact on the edge.  We do this by first
		// calculating the lerp, along polySide, of the point on the edge closest to
		// the sphere.  The lerp is calculated with the Pythagorean theorem.
		vB2=(vA2-vDistance2)*vInverseC2;	// using b2 as a temp here.

		if(IsGreaterThanAll(vB2,ScalarV(V_ZERO)))
		{
			vPolyPosition = Scale(vPolySide, Sqrt(vB2));
		}
		else
		{
			// Roundoff errors put the impact on a vertex even though the edge test put it on an edge,
			// so put it on the first vertex.
			vPolyPosition.ZeroComponents();
		}
	
		vPolyPosition = vPolyPosition + vVertices[index];

		// Calculate the normal vector of the collision.  That's a vector from the impact point
		// to the center of the sphere, normalized.
		vNormal = vSphereCenter-vPolyPosition;

		if (IsGreaterThanAll(vDistance, ScalarV(V_FLT_SMALL_12)))
		{
			vNormal = InvScale(vNormal, vDistance);
		}
		else
		{
			vNormal = vPolyNormal;
		}

		// Calculate the location of the impact on the sphere.  Start at the location of the impact
		// on the edge, and move, in the direction of the normal, the distance of the penetration
		// depth.
		vSpherePosition = SubtractScaled(vPolyPosition,vNormal,vDepth);

		// Let our caller know that an edge collided with the sphere.
		return GEOM_EDGE;
	}

	// If the sphere's center, projected onto the triangle's plane, is inside all the edges,
	// then the sphere is colliding with the plane of the polygon, i.e. not with any of
	// the edges or the vertices, but with the flat "middle" part.
	if(allInside)
	{
		// The closest polygon point to the sphere center is inside the polygon.

		// There's no "part number" for the entire polygon, so we use zero.
		idNum = 0;

		// The penetration depth is the radius of the sphere minus the distance from the
		// sphere to the plane.  Simple enough.  But if the sphere is colliding with the
		// back side of the polygon, this leads to a depth that's larger than the sphere
		// radius, and a really big impulse.  This is what we want, though -- we want
		// objects on the back side of polygonal data to get "sucked through" to the
		// front side.
		vDepth = vSphereRadius-vDistanceSphereCenterToPlane;

		// Calculate the locations of the impact, on the sphere and on the polygon.
		vPolyPosition = SubtractScaled(vSphereCenter,vPolyNormal,vDistanceSphereCenterToPlane);
		vSpherePosition = SubtractScaled(vSphereCenter,vPolyNormal,vSphereRadius);

		// Since we're impacting with the polygon, use its normal for the collision.
		vNormal = vPolyNormal;

		// Let our caller know the sphere collided with the entire polygon.
		return GEOM_POLYGON;
	}

	// Find the closest vertex.
	for (index=0;index<numVertices;index++)
	{
		// Get the index number of the next vertex.
		nextIndex = (index+1)%numVertices;

		// See if the sphere center is closest to the second vertex on this edge and no the second vertex on
		// the next edge, or closest to the first vertex on the next edge and not the first vertex on this edge.
		if ((insideThisEdge[index]==-2 || insideThisEdge[nextIndex]==-1) &&
			insideThisEdge[index]!=insideThisEdge[nextIndex])		//lint !e644 
		{
			break;
		}
	}

	if (index==numVertices)
	{
		// The sphere doesn't hit any vertex.
		return GEOM_NO_IMPACT;
	}

	// The impact point is the vertex itself.
	vPolyPosition = vVertices[nextIndex];

	// The impact normal points from the vertex toward the sphere center.
	vNormal = vSphereCenter-vPolyPosition;

	// Only now can we check to see if the colliding vertex is actually in the
	// sphere.  If it's not, there was no impact.
	vDistance = MagSquared(vNormal);
	
	
	if(IsGreaterThanAll(vDistance,ScalarV(vSphereRadius*vSphereRadius)))
	{
		return GEOM_NO_IMPACT;
	}

	// OK, now we're sure the vertex collided with the sphere.  The index number
	// we pass back to our caller is the vertex number.
	idNum = nextIndex;

	// Scale the normal to be unit length.
	if (IsGreaterThanAll(vDistance, ScalarV(V_FLT_SMALL_12)))
	{
		vDistance=Sqrt(vDistance);
		vNormal = InvScale(vNormal, vDistance);
	}
	else
	{
		vDistance.ZeroComponents();
		vNormal = vPolyNormal;
	}

	// The penetration depth is the radius of the sphere, minus the distance between
	// the sphere center & the vertex.  In other words, it's the distance from the
	// vertex to the sphere's surface, measured along the line from the vertex to the
	// sphere's center.
	vDepth = vSphereRadius-vDistance;

	// The impact point on the sphere is on the surface, along the impact normal that
	// we already calculated.
	vSpherePosition = SubtractScaled(vPolyPosition,vNormal,vDepth);

	// Let our caller know the sphere collided with a vertex of the polygon.
	return GEOM_VERTEX;
}

// copied from vector/geometry.cpp
int My_SegmentTriangleIntersectUndirected(Vec3V_In vStart, Vec3V_In vDir,
	Vec3V_In vEnd, Vec3V_In vNormal, Vec3V_In v0, Vec3V_In v1, Vec3V_In v2, const Vec3V_Ptr v3,
	ScalarV_InOut vROutAbove, ScalarV_InOut vROutBelow, ScalarV_InOut vROutT)
{
	Vec3V vEdge1, vEdge2, vT, vP, vQ;
	ScalarV vDet;
	ScalarV vU, vV;

	const ScalarV vEpsilon(1.0f/131072);
	Vec3V vT2;

	// Compute perpendicular distance from A & B to the triangle.
	// Reject if A & B are on the same side of the triangle.
	ScalarV vOutAbove, vOutBelow, vOutT;

	vT = vStart-v0;
	vOutAbove = Dot(vT, vNormal);

	vT2 = vEnd-v0;
	vOutBelow = Dot(vT2, vNormal);

	// Find where along the segment the intersection with the triangle is.
	vOutT = vOutAbove / (vOutAbove - vOutBelow);

	// Find vectors for the two edges sharing v0.
	vEdge1 = v1-v0;
	vEdge2 = v2-v0;

	// Calculate determinant.
	vP = Cross(vDir, vEdge2);
	vDet = Dot(vEdge1, vP);


	ScalarV vSignDet = ScalarV(V_NEGONE);
	if (IsGreaterThanAll(vDet, ScalarV(V_ZERO))) 
	{
		vSignDet = ScalarV(V_ONE);
	}

	vDet = vDet*vSignDet;

	// Calculate U parameter and test bounds.
	vU = Dot(vT,vP);
	vU = vU*vSignDet;

	// Calculate V parameter and test bounds.
	vQ = Cross(vT,vEdge1);

	vV = Dot(vDir,vQ);
	vV = vV*vSignDet;

	vROutAbove = vOutAbove;
	vROutBelow = vOutBelow;
	vROutT = vOutT;

	ScalarV vRR = vOutAbove*vOutBelow;
	ScalarV vUV = vU+vV;


	if(IsLessThanAll(vRR, ScalarV(V_ZERO)) == 0)
	{
		return 3;
	}


	if(IsLessThanAll(vOutT, ScalarV(V_ZERO)) || IsGreaterThanAll(vOutT, ScalarV(V_ONE)))
	{
		return 4;
	}


	if (IsLessThanAll(vDet,vEpsilon))
	{
		return 1;	// Segment in plane.
	}

	int iRet1;

	
	if (IsLessThanAll(vU,ScalarV(V_ZERO)) || IsGreaterThanAll(vU,vDet))
	{
		iRet1 = 5;
		goto quad;
	}

	if (IsLessThanAll(vV,ScalarV(V_ZERO)) || IsGreaterThanAll(vUV,vDet))
	{
		iRet1 = 6;
		goto quad;
	}

	// The segment and triangle intersect.
	return 0;

quad:
	ScalarV vDet2, vUQ, vVQ, vSignDet2;
	Vec3V vEdge3, vP2, vQ2;

	// If this isn't a quad, skip it.
	if (v3 == NULL)
	{
		return iRet1;
	}

	// Calculate determinant.
	vEdge3 = *v3 - v0;
	vP2 = Cross(vDir, vEdge3);
	vDet2 = Dot(vEdge2, vP2);

	vSignDet2 = ScalarV(V_NEGONE);
	if (IsGreaterThanAll(vDet2, ScalarV(V_ZERO))) 
	{
		vSignDet2 = ScalarV(V_ONE);
	}

	vDet2 = vDet2*vSignDet2;

	vUQ = Dot(vT,vP2);
	vUQ = vUQ*vSignDet2;

	vQ2 = Cross(vT, vEdge2);

	vVQ = Dot(vDir,vQ2);
	vVQ = vVQ*vSignDet2;

	if (IsLessThanAll(vDet2,vEpsilon))
	{
		return 2;	// Segment in plane.
	}

	// Calculate U parameter and test bounds.
	if (IsLessThanAll(vUQ,ScalarV(V_ZERO)) || IsGreaterThanAll(vUQ,vDet2))
	{
		return 7;
	}

	// Calculate V parameter and test bounds.
	ScalarV vUVQ;
	vUVQ = vUQ+vVQ;

	if (IsLessThanAll(vVQ,ScalarV(V_ZERO)) || IsGreaterThanAll(vUVQ,vDet2))
	{
		return 8;
	}

	// The segment and triangle intersect.
	return 0;

}

ScalarV_Out CalcRadius(ptxPointSB* pPointSB, ptxPointDB* pPointDB, ScalarV_In vRadiusMult, ScalarV_In vOverrideMinRadius)
{
	// calculate the radius of the particle
	ScalarV vPtxRadius;
	if (pPointSB->GetFlag(PTXPOINT_FLAG_IS_SPRITE) || pPointSB->GetFlag(PTXPOINT_FLAG_IS_TRAIL))
	{
		// sprite radius calculated from tblr sizes
		Vec2V vTopBottom = pPointDB->GetDimensions().GetXY();
		Vec2V vLeftRight = pPointDB->GetDimensions().GetZW();
		Vec2V vMaxDims = Vec2V(MaxElement(vTopBottom), MaxElement(vLeftRight));
		vPtxRadius = Mag(vMaxDims);
	}
	else if (pPointSB->GetFlag(PTXPOINT_FLAG_IS_MODEL))
	{
		// model radius stored in w component
		vPtxRadius = SplatW(pPointDB->GetDimensions());
	}
	else
	{
		vPtxRadius = ScalarV(V_ONE);
		ptxAssertf(0, "unsupported particle type");
	}

	// apply the radius scale
	vPtxRadius *= vRadiusMult;

	// make sure the radius doens't get too small
	ScalarV vMinRadius = ScalarVConstant<FLOAT_TO_INT(0.05f)>();
	vMinRadius = SelectFT(IsGreaterThan(vOverrideMinRadius, ScalarV(V_ZERO)), vMinRadius, vOverrideMinRadius);
	vPtxRadius = Max(vPtxRadius, vMinRadius);

	return vPtxRadius;
}

bool HasCollidedWithPoly(Vec3V_InOut vColnPos, Vec3V_InOut vColnNormal, ScalarV_InOut vColnDepth, ptxCollisionPoly& colnPoly, Vec3V_In vPtxCurrPos, Vec3V_In vPtxLastPos, Vec3V_In vPtxDir, ScalarV_In vPtxRadius, bool bTooFast)
{
	// cache some poly data
	int numVerts = colnPoly.GetNumVerts();
	Vec3V vPolyVerts[4];
	vPolyVerts[0] = colnPoly.GetVertex(0);
	vPolyVerts[1] = colnPoly.GetVertex(1);
	vPolyVerts[2] = colnPoly.GetVertex(2);
	vPolyVerts[3] = colnPoly.GetVertex(3);
	Vec3V vPolyNormal = colnPoly.GetNormal();

	// early out - don't collide with backfaces
	ScalarV vDot = Dot(vPtxDir, vPolyNormal);
	if (IsGreaterThanOrEqualAll(vDot, ScalarV(V_ZERO))) 
	{
		return false;
	}

	// check for collisions
	vColnDepth = ScalarV(V_ZERO);
	if (bTooFast)
	{
		// particles that move too quickly (i.e. have moved further in a frame that twice the collision radius) need to do a line against the poly test
		Vec3V vSegBegin = vPtxLastPos;
		Vec3V vSegEnd = vPtxCurrPos;	
		Vec3V vSegDir = vSegEnd - vSegBegin;
		vSegDir = Normalize(vSegDir);

		// add radius in the direction of the particle to approximate the collision radius into the collision test
		vSegEnd += vSegDir*vPtxRadius;

		// test for the line segment intersecting the poly
		int hitCode = 0;
		ScalarV vAbove, vBelow, vT;
		hitCode = My_SegmentTriangleIntersectUndirected(vSegBegin, 
														vSegDir, 
														vSegEnd, 
														vPolyNormal, 
														vPolyVerts[0], 
														vPolyVerts[1], 
														vPolyVerts[2], 
														(numVerts==4 ? &(vPolyVerts[3]) : NULL),
														vAbove, 
														vBelow, 
														vT);

		if (hitCode==0)
		{
			vColnDepth = -vBelow;
			vColnNormal = vPolyNormal;
			vColnPos = vPtxCurrPos + (vColnNormal*vColnDepth);	

			return true;
		}
	}
	else
	{
		// quick collision test for slow moving particles (i.e where the collision sphere intersects the previous collision sphere) 
		// these just need to do a sphere against poly test
		int idx;
		Vec3V vSpherePos, vBoxPos;
		if (My_FindImpactPolygonToSphere(vPtxCurrPos, 
										 vPtxRadius,   
										 &vPolyVerts[0], 
										 numVerts, 
										 vPolyNormal, 
										 vSpherePos, 
										 vBoxPos, 
										 idx, 
										 vColnNormal, 
										 vColnDepth)
			!= GEOM_NO_IMPACT)
		{

			vColnNormal = vPolyNormal;
			vColnPos = vPtxCurrPos + (vColnNormal*(vColnDepth-vPtxRadius));	

			return true;
		}
	}

	return false;
}




} // namespace rage

