// 
// rmptfx/ptxu_size.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "rmptfx/ptxu_size.h"
#include "rmptfx/ptxu_size_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Size::sm_whdMin2dDefn(						"WH Min", 					atHashValue("ptxu_Size:m_whdMinKFP"),						PTXKEYFRAMETYPE_FLOAT2, 	Vec4V(V_ZERO), 						Vec3V(0.0f, 	1.0f,		0.01f), 	Vec3V(0.0f, 	1000.0f, 	0.01f), 	"Width Min", 	"Height Min");
ptxKeyframeDefn ptxu_Size::sm_whdMax2dDefn(						"WH Max", 					atHashValue("ptxu_Size:m_whdMaxKFP"),						PTXKEYFRAMETYPE_FLOAT2, 	Vec4V(V_ZERO), 						Vec3V(0.0f, 	1.0f,		0.01f), 	Vec3V(0.0f, 	1000.0f, 	0.01f), 	"Width Max", 	"Height Max");
ptxKeyframeDefn ptxu_Size::sm_whdMin3dDefn(						"WHD Min", 					atHashValue("ptxu_Size:m_whdMinKFP"),						PTXKEYFRAMETYPE_FLOAT3, 	Vec4V(V_ZERO), 						Vec3V(0.0f, 	1.0f,		0.01f), 	Vec3V(0.0f, 	1000.0f, 	0.01f), 	"Width Min", 	"Height Min",	"Depth Min");
ptxKeyframeDefn ptxu_Size::sm_whdMax3dDefn(						"WHD Max", 					atHashValue("ptxu_Size:m_whdMaxKFP"),						PTXKEYFRAMETYPE_FLOAT3, 	Vec4V(V_ZERO), 						Vec3V(0.0f, 	1.0f,		0.01f), 	Vec3V(0.0f, 	1000.0f, 	0.01f), 	"Width Max", 	"Height Max",	"Depth Max");
ptxKeyframeDefn ptxu_Size::sm_tblrScalarDefn(					"TBLR Scalar",				atHashValue("ptxu_Size:m_tblrScalarKFP"),					PTXKEYFRAMETYPE_FLOAT4, 	Vec4V(V_ZERO), 						Vec3V(0.0f, 	1.0f,		0.01f), 	Vec3V(0.0f, 	1000.0f, 	0.01f), 	"Top",			"Bottom", 		"Left", 		"Right");
ptxKeyframeDefn ptxu_Size::sm_tblrVelScalarDefn(				"TBLR Velocity Scalar",		atHashValue("ptxu_Size:m_tblrVelScalarKFP"),				PTXKEYFRAMETYPE_FLOAT4, 	Vec4V(V_ZERO), 						Vec3V(0.0f, 	1.0f,		0.01f), 	Vec3V(0.0f, 	1000.0f, 	0.01f), 	"Top",			"Bottom", 		"Left", 		"Right");


// code
#if RMPTFX_XML_LOADING
ptxu_Size::ptxu_Size()
{
	// create keyframes
	m_whdMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Size:m_whdMinKFP"));
	m_whdMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Size:m_whdMaxKFP"));
	m_tblrScalarKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Size:m_tblrScalarKFP"));
	m_tblrVelScalarKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Size:m_tblrVelScalarKFP"));

	m_keyframePropList.AddKeyframeProp(&m_whdMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_whdMaxKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_tblrScalarKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_tblrVelScalarKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Size::SetDefaultData()
{
	// default keyframes
	m_whdMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_whdMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_tblrScalarKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_tblrVelScalarKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_keyframeMode = PTXU_SIZE_KEYFRAMEMODE_POINT;

	m_isProportional = true;
}

ptxu_Size::ptxu_Size(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_whdMinKFP(rsc)
	, m_whdMaxKFP(rsc)
	, m_tblrScalarKFP(rsc)
	, m_tblrVelScalarKFP(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Size_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Size::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Size, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Size, m_whdMinKFP)
		SSTRUCT_FIELD(ptxu_Size, m_whdMaxKFP)
		SSTRUCT_FIELD(ptxu_Size, m_tblrScalarKFP)
		SSTRUCT_FIELD(ptxu_Size, m_tblrVelScalarKFP)
		SSTRUCT_FIELD(ptxu_Size, m_keyframeMode)
		SSTRUCT_FIELD(ptxu_Size, m_isProportional)
		SSTRUCT_IGNORE(ptxu_Size, m_pad)
	SSTRUCT_END(ptxu_Size)
}
#endif

IMPLEMENT_PLACE(ptxu_Size);

void ptxu_Size::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointSize)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Size::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// default the keyframe time to use the particle life ratio
	ScalarV vKeyTime = pPointDB->GetKeyTime();
	if (m_keyframeMode==PTXU_SIZE_KEYFRAMEMODE_EFFECT)
	{
		// override with effect life ratio if requested
		vKeyTime = pEffectInst->GetKeyTime();
	}
	else if (m_keyframeMode==PTXU_SIZE_KEYFRAMEMODE_EMITTER)
	{
		// override with emitter life ratio if requested
		vKeyTime = pEmitterInst->GetKeyTime();
	}
	else
	{
		ptxAssertf(m_keyframeMode==PTXU_SIZE_KEYFRAMEMODE_POINT, "unsupported keyframe mode");
	}

	// cache some data that we'll need later
	ptxEmitterRule* pEmitterRule = static_cast<ptxEmitterRule*>(pEmitterInst->GetEmitterRule());
	ScalarV vKeyTimePointEmitter = ScalarVFromF32(pPointSB->GetEmitterLifeRatio());
	ScalarV vDiv100 = ScalarV(V_FLT_SMALL_2);

	// get the keyframe data
	// the ptx size behaviour (us) defines min and max size keyframe data that specifies width, height and depth channels
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vPtxSizeWHDMin = m_whdMinKFP.Query(vKeyTime, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vPtxSizeWHDMax = m_whdMaxKFP.Query(vKeyTime, pEvolutionList, pPointSB->GetEvoValues());
	// the emitter defines a size scalar keyframe data that specifies width, height and depth channels
	Vec4V vEmitSizeWHDScalar = pEmitterRule->GetSizeScalarKFP().Query(vKeyTimePointEmitter, pEvolutionList, pPointSB->GetEvoValues()) * vDiv100;
	// the ptx size behaviour (us) defines scalars with top, bottom, left and right channels that get applied to sprites only
	Vec4V vPtxSizeTBLRScalar = m_tblrScalarKFP.Query(vKeyTime, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vPtxSizeTBLRVelScalar = m_tblrVelScalarKFP.Query(vKeyTime, pEvolutionList, pPointSB->GetEvoValues());

	// calculate the bias values for each component of size
	u8 randOffsetX = 0;
	u8 randOffsetY = 1;
	u8 randOffsetZ = 2;
	u8 randOffsetW = 3;
	if (m_isProportional)
	{
		// proportional size needs the same bias values for each component
		randOffsetY = randOffsetX;
		randOffsetZ = randOffsetX;
		randOffsetW = randOffsetX;
	}
	Vec4V vBias = Vec4V(GetBiasValue(m_whdMinKFP, pPointSB->GetRandIndex()+randOffsetX), 
		GetBiasValue(m_whdMinKFP, pPointSB->GetRandIndex()+randOffsetY), 
		GetBiasValue(m_whdMinKFP, pPointSB->GetRandIndex()+randOffsetZ), 
		GetBiasValue(m_whdMinKFP, pPointSB->GetRandIndex()+randOffsetW));

	// calculate the lerped size between the min and max keyframe data
	Vec4V vPtxSizeWHD = Lerp(vBias, vPtxSizeWHDMin, vPtxSizeWHDMax);

	// we should really be doing the zoom and emitter scale calcs at the end (see commented out code below)
	// as any sprite tblr velocity scale will get added to the already scaled result later on
	// however, this breaks so many effects at the moment that we can't make the change
	{
		// scale this by the emitter size scalar
		vPtxSizeWHD *= vEmitSizeWHDScalar;

		// calculate the zoom value 
		ScalarV vZoom = ScalarVFromF32(pEffectInst->GetFinalZoom() * pEmitterInst->GetZoomScalar());

		// apply the zoom to the size
		vPtxSizeWHD *= vZoom;
	}

	// calculate the final particle size (models just use whd but sprites need to convert to tblr)
	Vec4V vPtxSizeFinal = vPtxSizeWHD;
	if (pPointSB->GetFlag(PTXPOINT_FLAG_IS_SPRITE) || pPointSB->GetFlag(PTXPOINT_FLAG_IS_TRAIL))
	{
		Vec3V vPtxVelocity = pPointDB->GetCurrVel();
		ScalarV vPtxSpeed = Mag(vPtxVelocity);

		Vec4V vPtxSizeTBLR = vPtxSizeWHD.Get<Vec::Y, Vec::Y, Vec::X, Vec::X>() * Vec4V(V_HALF);
		vPtxSizeTBLR *= vPtxSizeTBLRScalar;
		vPtxSizeTBLR += vPtxSpeed*vPtxSizeTBLRVelScalar;

		vPtxSizeFinal = vPtxSizeTBLR;
	}
	else if (pPointSB->GetFlag(PTXPOINT_FLAG_IS_MODEL))
	{
		// set the radius of the model in the w component
		Vec3V vBBSize = params.vModelDimensions.GetXYZ();
		ScalarV vRadius = SplatW(params.vModelDimensions);
		ScalarV vMagOrig = Mag(vBBSize);
		ScalarV vMagFinal = Mag(vPtxSizeFinal.GetXYZ());
		ScalarV vScale = vMagFinal/vMagOrig;

		vPtxSizeFinal.SetW(vRadius*vScale);
	}
	else
	{
		ptxAssertf(0, "unsupported particle type");
	}

	// 	// scale this by the emitter size scalar
	// 	vPtxSizeFinal *= vEmitSizeWHDScalar;
	// 
	// 	// calculate and apply the zoom value 
	// 	ScalarV vZoom = ScalarVFromF32(pEffectInst->GetFinalZoom() * pEmitterInst->GetZoomScalar());
	// 	vPtxSizeFinal *= vZoom;

	// set the particle dimension data
	pPointDB->SetDimensions(vPtxSizeFinal);
}

void ptxu_Size::SetKeyframeDefns()
{
	m_whdMinKFP.GetKeyframe().SetDefn(&sm_whdMin2dDefn);
	m_whdMaxKFP.GetKeyframe().SetDefn(&sm_whdMax2dDefn);
	m_tblrScalarKFP.GetKeyframe().SetDefn(&sm_tblrScalarDefn);
	m_tblrVelScalarKFP.GetKeyframe().SetDefn(&sm_tblrVelScalarDefn);
}

void ptxu_Size::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_whdMinKFP, &sm_whdMin2dDefn);
	pEvolutionList->SetKeyframeDefn(&m_whdMaxKFP, &sm_whdMax2dDefn);
	pEvolutionList->SetKeyframeDefn(&m_tblrScalarKFP, &sm_tblrScalarDefn);
	pEvolutionList->SetKeyframeDefn(&m_tblrVelScalarKFP, &sm_tblrVelScalarDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Size::IsActive()
{
	ptxu_Size* pSize = static_cast<ptxu_Size*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_whdMinKFP == pSize->m_whdMinKFP &&
		   		   m_whdMaxKFP == pSize->m_whdMaxKFP &&
				   m_tblrScalarKFP == pSize->m_tblrScalarKFP &&
				   m_tblrVelScalarKFP == pSize->m_tblrVelScalarKFP &&
				   m_keyframeMode == pSize->m_keyframeMode && 
				   m_isProportional == pSize->m_isProportional;

	return !isEqual;
}

void ptxu_Size::UpdateKeyframeDefns(ptxDrawType drawType, const ptxParticleRule* pParticleRule)
{
	if (drawType==PTXPARTICLERULE_DRAWTYPE_SPRITE)
	{
		m_whdMinKFP.UpdateBehaviourDefn(&sm_whdMin2dDefn, pParticleRule);
		m_whdMaxKFP.UpdateBehaviourDefn(&sm_whdMax2dDefn, pParticleRule);
	}
	else
	{
		m_whdMinKFP.UpdateBehaviourDefn(&sm_whdMin3dDefn, pParticleRule);
		m_whdMaxKFP.UpdateBehaviourDefn(&sm_whdMax3dDefn, pParticleRule);
	}
}

void ptxu_Size::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 6);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "WHD Min");
	SendKeyframeDefnToEditor(buff, "WHD Max");
	SendKeyframeDefnToEditor(buff, "TBLR Scalar");
	SendKeyframeDefnToEditor(buff, "TBLR Velocity Scalar");

	// send non-keyframes
	const char* pComboItemNames[PTXU_SIZE_KEYFRAMEMODE_NUM] = {"Point", "Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Keyframe Mode", PTXU_SIZE_KEYFRAMEMODE_NUM, pComboItemNames);

	SendBoolDefnToEditor(buff, "Proportional");
}

void ptxu_Size::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_whdMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_whdMaxKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_tblrScalarKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_tblrVelScalarKFP, pParticleRule);

	// send non-keyframes
	SendComboItemToEditor(buff, m_keyframeMode);

	SendBoolToEditor(buff, m_isProportional);
}

void ptxu_Size::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_whdMinKFP);
	ReceiveKeyframeFromEditor(buff, m_whdMaxKFP);
	ReceiveKeyframeFromEditor(buff, m_tblrScalarKFP);
	ReceiveKeyframeFromEditor(buff, m_tblrVelScalarKFP);

	// receive non-keyframes
	ReceiveComboItemFromEditor(buff, m_keyframeMode);

	ReceiveBoolFromEditor(buff, m_isProportional);
}
#endif // RMPTFX_EDITOR


} // namespace rage
