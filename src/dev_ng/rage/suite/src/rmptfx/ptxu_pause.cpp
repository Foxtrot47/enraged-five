// 
// rmptfx/ptxu_pause.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "rmptfx/ptxu_pause.h"
#include "rmptfx/ptxu_pause_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"

#if !__RESOURCECOMPILER && !RSG_TOOL
#include "vfx/ptfx/ptfxcallbacks.h"
#endif


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


// code
#if RMPTFX_XML_LOADING
ptxu_Pause::ptxu_Pause()
{	
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Pause::SetDefaultData()
{
	// default keyframes

	// default other data
	m_pauseLifeRatio = -1.0f;
	m_unpauseTime = 10.0f;
	m_unpauseEffectDist = 2.0f;
	m_unpauseCamDist = 25.0f;
}

ptxu_Pause::ptxu_Pause(datResource& rsc)
	: ptxBehaviour(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Pause_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Pause::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Pause, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Pause, m_pauseLifeRatio)
		SSTRUCT_FIELD(ptxu_Pause, m_unpauseTime)
		SSTRUCT_FIELD(ptxu_Pause, m_unpauseEffectDist)
		SSTRUCT_FIELD(ptxu_Pause, m_unpauseCamDist)
	SSTRUCT_END(ptxu_Pause)
}
#endif

IMPLEMENT_PLACE(ptxu_Pause);

void ptxu_Pause::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointPause)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Pause::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxPointItem* pPointItem = params.pPointItem;	
	ScalarV vDeltaTime = params.vDeltaTime;

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// check if pausing is enabled
	if (m_pauseLifeRatio>-1.0f)
	{
		// check if this particle hasn't been unpaused (if it has we don't do anything)
		if (pPointSB->GetFlag(PTXPOINT_FLAG_LIFE_UNPAUSED)==false)
		{
			// if the particle hasn't been paused yet we need to check if it needs paused
			if (pPointSB->GetFlag(PTXPOINT_FLAG_LIFE_PAUSED)==false)
			{
				if (pPointDB->GetLifeRatio()>m_pauseLifeRatio)
				{
					pPointSB->SetFlag(PTXPOINT_FLAG_LIFE_PAUSED);
					pPointSB->SetPauseTimer(m_unpauseTime);
				}
			}

			// if the particle is currently paused we need to check if it need unpaused
			if (pPointSB->GetFlag(PTXPOINT_FLAG_LIFE_PAUSED))
			{
				// make sure the life ratio is paused
				pPointDB->SetLifeRatio(m_pauseLifeRatio);
				pPointSB->SetPauseTimer(pPointSB->GetPauseTimer()-vDeltaTime.Getf());

				// get distance of particle from effect inst
				float dist = Dist(pEffectInst->GetCurrPos(), pPointDB->GetCurrPos()).Getf();
				if (dist>m_unpauseEffectDist)
				{
					pPointSB->ClearFlag(PTXPOINT_FLAG_LIFE_PAUSED);
					pPointSB->SetFlag(PTXPOINT_FLAG_LIFE_UNPAUSED);
				}

				// get distance of particle from camera
#if	!__RESOURCECOMPILER && !RSG_TOOL
				Vec3V vCamPos = g_pPtfxCallbacks->GetGameCamPos();
				dist = Dist(vCamPos, pPointDB->GetCurrPos()).Getf();
				if (dist>m_unpauseCamDist)
				{
					pPointSB->ClearFlag(PTXPOINT_FLAG_LIFE_PAUSED);
					pPointSB->SetFlag(PTXPOINT_FLAG_LIFE_UNPAUSED);
				}
#endif

				if (pPointSB->GetPauseTimer()<0.0f)
				{
					pPointSB->ClearFlag(PTXPOINT_FLAG_LIFE_PAUSED);
					pPointSB->SetFlag(PTXPOINT_FLAG_LIFE_UNPAUSED);
					pPointSB->SetPauseTimer(0.0f);
				}
			}
		}
	}
}

// editor code
#if RMPTFX_EDITOR
bool ptxu_Pause::IsActive()
{
	ptxu_Pause* pPause = static_cast<ptxu_Pause*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_pauseLifeRatio == pPause->m_pauseLifeRatio && 
				   m_unpauseTime == pPause->m_unpauseTime &&
				   m_unpauseEffectDist == pPause->m_unpauseEffectDist && 
				   m_unpauseCamDist == pPause->m_unpauseCamDist;

	return !isEqual;
}

void ptxu_Pause::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 4);

	// send keyframes

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Pause Life Ratio");
	SendFloatDefnToEditor(buff, "Unpause Time");
	SendFloatDefnToEditor(buff, "Unpause Effect Dist");
	SendFloatDefnToEditor(buff, "Unpause Camera Dist");
}

void ptxu_Pause::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes

	// send non-keyframes
	SendFloatToEditor(buff, m_pauseLifeRatio);
	SendFloatToEditor(buff, m_unpauseTime);
	SendFloatToEditor(buff, m_unpauseEffectDist);
	SendFloatToEditor(buff, m_unpauseCamDist);
}

void ptxu_Pause::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_pauseLifeRatio);
	ReceiveFloatFromEditor(buff, m_unpauseTime);
	ReceiveFloatFromEditor(buff, m_unpauseEffectDist);
	ReceiveFloatFromEditor(buff, m_unpauseCamDist);
}
#endif // RMPTFX_EDITOR












} // namespace rage
