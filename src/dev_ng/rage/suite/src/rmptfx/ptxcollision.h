// 
// rmptfx/ptxcollision.h 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXCOLLISION_H
#define RMPTFX_PTXCOLLISION_H

// includes (rmptfx)
#include "rmptfx/ptxconfig.h"

// includes (rage)
#include "atl/array.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"


// namespaces
namespace rage
{


// classes
class ptxCollisionPoly 
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		void SetTri(const Vec3V* pvVerts, Vec3V_In vNormal, u8 mtlId);
		void SetQuad(const Vec3V* pvVerts, Vec3V_In vNormal, u8 mtlId);

		// accessors
		void SetVertex(int idx, Vec3V_In vCoords) {m_vData[idx].SetXYZ(vCoords);}
		Vec3V_Out GetVertex(int idx) {return m_vData[idx].GetXYZ();}

		void SetNormal(Vec3V_In vNormal) {m_vData[0].SetW(vNormal.GetX()); m_vData[1].SetW(vNormal.GetY()); m_vData[2].SetW(vNormal.GetZ());}
		Vec3V_Out GetNormal() {return Vec3V(m_vData[0].GetW(), m_vData[1].GetW(), m_vData[2].GetW());}

		void SetMtlId(u8 mtlId) {int& packedData = *(((int*)&(m_vData[3].GetIntrin128Ref()))+3); packedData &= 0xffffff00; packedData |= mtlId;}
		u8 GetMtlId() {int packedData = *(((int*)&(m_vData[3].GetIntrin128Ref()))+3); return (u16)(packedData & 0x000000ff);}

		void SetIsQuad(bool isQuad) {int& packedData = *(((int*)&(m_vData[3].GetIntrin128Ref()))+3); packedData &= 0xfffffeff; if (isQuad) packedData |= (1<<8);}
		bool GetIsQuad() {int packedData = *(((int*)&(m_vData[3].GetIntrin128Ref()))+3); return (packedData & 0x00000100) > 0;}

		int GetNumVerts() {return GetIsQuad() ? 4 : 3;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		Vec4V m_vData[4];

};


class ptxCollisionSet
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		void Init(int maxNumColnPolys);
		void Clear() {m_colnPolys.ResetCount();}
		void AddPoly(ptxCollisionPoly& colnPoly);
		void AddPolys(ptxCollisionPoly* colnPolys, int numColnPolys);
		void AddTri(const Vec3V* pvVerts, Vec3V_In vNormal, u8 mtlId);
		void AddQuad(const Vec3V* pvVerts, Vec3V_In vNormal, u8 mtlId);
		void CopyFrom(ptxCollisionSet* pColnSet);

		// accessors
		int GetNumColnPolys() {return m_colnPolys.GetCount();}
		ptxCollisionPoly& GetColnPoly(int idx) {return m_colnPolys[idx];}

		// debug
#if RMPTFX_EDITOR && RMPTFX_BANK
		void DebugDraw();
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		atArray<ptxCollisionPoly> m_colnPolys;


};


} // namespace rage


#endif // RMPTFX_PTXCOLLISION_H
