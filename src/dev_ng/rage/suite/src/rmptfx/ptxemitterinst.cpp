// 
// rmptfx/ptxemitterinst.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxemitterinst.h"

// includes (rmptfx)
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxd_drawsprite.h"

// includes (rage)
#include "grcore/debugdraw.h"
#include "system/cache.h"

// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// code
ptxEmitterInst::ptxEmitterInst()
{
	// points
	m_pointList.ClearBuffers();
	ptxAssertf(m_pointList.GetNumItems()==0, "point list not empty");
	for (int i=0; i<PTX_NUM_BUFFERS; i++)
	{
		ptxAssertf(m_pointList.GetNumItemsMT(i)==0, "point list not empty");
	}

	// state
	m_vPrevPosWld = Vec3V(V_ZERO);

	m_prevLifeRatio = 0.0f;
	m_currSpawnCount = 0.0f;
	m_startRatio = 0.0f;
	m_endRatio = 0.0f;

	m_playState = PTXEMITINSTPLAYSTATE_WAITING_TO_START;

	// flags
	m_flags.Reset();

	// settings
	m_colourTint = Color32(1.0f, 1.0f, 1.0f, 1.0f);
	m_playbackRateScalar = 1.0f;
	m_zoomScalar = 1.0f;

	// rules
	m_pEmitterRule = NULL;
	m_pParticleRule = NULL;

	// domains
	m_pAttractorDomainInst = NULL;

	// double buffered emitter data
	m_dataDB.Init();		
	for (int i=0; i<PTX_NUM_BUFFERS; i++)
	{
		m_dataDBMT[i].Init();
	}

	// debug
#if RMPTFX_BANK
	m_outputPlayStateDebug = false;
#endif

	//m_ambientScale[0] = 0;
	//m_ambientScale[1] = 0;
}

#if RMPTFX_THOROUGH_ERROR_CHECKING
void ptxEmitterInst::VerifyLists()
{
	// make sure all points are valid
	ptxPointItem* pCurrPointItem = (ptxPointItem*)m_pointList.GetHead();
	int SBCounter = 0, DBCounter[PTX_NUM_BUFFERS];
	while (pCurrPointItem)
	{
		ptxAssertf(RMPTFXMGR.GetPointPool().IsInFreeList(pCurrPointItem)==false, "active point is in the free list");
		pCurrPointItem = (ptxPointItem*)pCurrPointItem->GetNext();
		SBCounter++;
	}
	ptxAssertf(SBCounter == m_pointList.GetNumItems(),"Number of items does not match list");

	for (int i=0; i<PTX_NUM_BUFFERS; i++)
	{
		DBCounter[i] = 0;
		pCurrPointItem = (ptxPointItem*)m_pointList.GetHeadMT(i);
		while (pCurrPointItem)
		{
			ptxAssertf(RMPTFXTHREADER.GetUpdateBufferId() != i || RMPTFXMGR.GetPointPool().IsInFreeList(pCurrPointItem)==false, "active point is in the free list");
			pCurrPointItem = (ptxPointItem*)pCurrPointItem->GetNextMT(i);
			DBCounter[i]++;
		}
		ptxAssertf(DBCounter[i] == m_pointList.GetNumItemsMT(i),"Number of items does not match list");
	}
}
#endif

void ptxEmitterInst::Init(ptxEffectInst* pEffectInst)
{
	// points
	m_pointList.ClearBuffers();
	ptxAssertf(m_pointList.GetNumItems()==0, "point list not empty");
	for (int i=0; i<PTX_NUM_BUFFERS; i++)
	{
		ptxAssertf(m_pointList.GetNumItemsMT(i)==0, "point list not empty");
	}

	// state
	m_vPrevPosWld = Vec3V(V_ZERO);

	m_prevLifeRatio = 0.0f;
	m_currSpawnCount = 0.0f;
	m_startRatio = 0.0f;
	m_endRatio = 0.0f;

	SetPlayState(pEffectInst, PTXEMITINSTPLAYSTATE_WAITING_TO_START);

	// flags
	m_flags.Reset();

	// settings
	m_colourTint = Color32(1.0f, 1.0f, 1.0f, 1.0f);
	m_playbackRateScalar = 1.0f;
	m_zoomScalar = 1.0f;

	// rules
	m_pEmitterRule = NULL;
	m_pParticleRule = NULL;

	// domains
	ptxAssertf(m_pAttractorDomainInst==NULL, "attractor domain inst already allocated");

	// double buffered emitter data
	m_dataDB.Init();		
	for (int i=0; i<PTX_NUM_BUFFERS; i++)
	{
		m_dataDBMT[i].Init();
	}

	// debug
#if RMPTFX_BANK
	m_outputPlayStateDebug = false;
#endif
}

void ptxEmitterInst::Start(ptxEffectInst* pEffectInst, ptxEmitterRule* pEmitterRule, ptxParticleRule* pParticleRule, float startRatio, float endRatio, float playbackRateScalar, float zoomScalar, Color32 colourTint)
{
	(void)pEffectInst;

	ptxAssertf(pEffectInst, "trying to start an emitter inst without an effect inst");
	ptxAssertf(pEmitterRule, "trying to start an emitter inst without an emitter rule");
	ptxAssertf(pParticleRule, "trying to start an emitter inst without a particle rule");

	ptxAssertf(m_playState==PTXEMITINSTPLAYSTATE_WAITING_TO_START ||
			   m_playState==PTXEMITINSTPLAYSTATE_STOPPED || 
			   m_playState==PTXEMITINSTPLAYSTATE_FINISHED, "trying to start an emitter with an invalid state (%d)", m_playState);

	// rules
	m_pEmitterRule = pEmitterRule;
	m_pParticleRule = pParticleRule;

	// settings
	m_playbackRateScalar = playbackRateScalar;
	m_zoomScalar = zoomScalar;
	m_colourTint = colourTint;

	// set the ratios during the effect life that the emitter is active
	m_startRatio = startRatio;
	m_endRatio = endRatio;

#if RMPTFX_BANK
	m_outputPlayStateDebug = ShouldOutputPlayStateInfo(pEffectInst);
	OutputPlayStateFunction(pEffectInst, "Start");
#endif
	SetPlayState(pEffectInst, PTXEMITINSTPLAYSTATE_PLAYING);

	// set the ratios of the emitter
	m_prevLifeRatio = 0.0f;
	GetDataDB()->SetCurrLifeRatio(0.0f);
	m_currSpawnCount = 0.0f;

	// initialise flags
	SetFlag(PTXEMITINSTFLAG_HAS_JUST_STARTED);
	ClearFlag(PTXEMITINSTFLAG_ON_MAIN_THREAD_ONLY);
	if (m_pParticleRule->GetUpdateOnMainThreadOnly())
	{
		SetFlag(PTXEMITINSTFLAG_ON_MAIN_THREAD_ONLY);
	}

	// copy point data to the update buffer
	CopyToUpdateBuffer();
}

void ptxEmitterInst::Loop(ptxEffectInst* UNUSED_PARAM(pEffectInst))
{
	ptxAssertf(m_pEmitterRule, "trying to start an emitter inst without an emitter rule");
	ptxAssertf(m_pParticleRule, "trying to start an emitter inst without a particle rule");

	ptxAssertf(m_playState==PTXEMITINSTPLAYSTATE_PLAYING, "trying to loop an emitter that isn't playing");

	if (GetIsContinous()==false)
	{
		// the emitter has looped but is inactive
		// i.e. its duration is shorter than that of the effect
		// reset the state
		m_prevLifeRatio = 0.0f;
		GetDataDB()->SetCurrLifeRatio(0.0f);
		m_currSpawnCount = 0.0f;
	}

	SetFlag(PTXEMITINSTFLAG_HAS_JUST_LOOPED);

	// copy point data to the update buffer
	CopyToUpdateBuffer();
}

void ptxEmitterInst::Stop(ptxEffectInst* pEffectInst)
{
#if RMPTFX_BANK
	OutputPlayStateFunction(pEffectInst, "Stop");
#endif

	// stop the emitter if it's playing
	if (m_playState==PTXEMITINSTPLAYSTATE_PLAYING)
	{
		SetPlayState(pEffectInst, PTXEMITINSTPLAYSTATE_STOPPED);
	}
}

void ptxEmitterInst::Relocate(ptxEffectInst* UNUSED_PARAM(pEffectInst), Vec3V_In vDeltaPos)
{
	// relocate the emitter
	m_vPrevPosWld += vDeltaPos;
	GetDataDB()->SetCurrPosWld(GetDataDB()->GetCurrPosWld() + vDeltaPos);

	// relocate the points
	ptxPointItem* pCurrPointItem = (ptxPointItem*)m_pointList.GetHead();
	while (pCurrPointItem)
	{
		ptxPointItem* pNextPointItem = (ptxPointItem*)pCurrPointItem->GetNext();

		pCurrPointItem->GetDataDB()->IncPrevPos(vDeltaPos);
		pCurrPointItem->GetDataDB()->IncCurrPos(vDeltaPos);
		pCurrPointItem->GetDataDB()->IncSpawnPos(vDeltaPos);

		// MN - can we relocate any spawned effects here?
		//      we now only have a pointer on the effect inst to the point that created it - not the other way around

		pCurrPointItem = pNextPointItem;
	}
}

void ptxEmitterInst::Finish(ptxEffectInst* pEffectInst)
{
#if RMPTFX_BANK
	OutputPlayStateFunction(pEffectInst, "Finish");
#endif

	// return early if emitter isn't started yet or is already finished
	if (m_playState==PTXEMITINSTPLAYSTATE_WAITING_TO_START || 
		m_playState==PTXEMITINSTPLAYSTATE_FINISHED)
	{
		return;
	}

	// stop the emitter
	Stop(pEffectInst);

	//Clear out the MT List
	m_pointList.BeginBuffering(RMPTFXTHREADER.GetUpdateBufferId());

	// return the points as if the effect had naturally stopped
	ptxPointItem* pCurrPointItem = (ptxPointItem*)m_pointList.GetHead();
	while (pCurrPointItem!=NULL)
	{
		ptxPointItem* pNextPointItem = (ptxPointItem*)pCurrPointItem->GetNext();

		// set the point as dead so any spawned effects that are pointing to it can detach themselves
		pCurrPointItem->GetDataDB()->SetIsDead(true);

		// return the point to the pool
		m_pParticleRule->ReturnPoint(&m_pointList, pCurrPointItem);

		pCurrPointItem = pNextPointItem;
	}

	// we're now finished
	ptxAssertf(m_playState==PTXEMITINSTPLAYSTATE_STOPPED, "trying to finish an emitter (%s - %s) that isn't stopped (%d)", pEffectInst->GetEffectRule()->GetName(), m_pEmitterRule->GetName(), m_playState);
	SetPlayState(pEffectInst, PTXEMITINSTPLAYSTATE_FINISHED);
}


Mat34V_Out ptxEmitterInst::GetCreationDomainLclToWldMtx(Mat34V_In vEffectInstMtx) const 
{
	Mat34V vTransformMtx = Mat34V(V_IDENTITY); 

	//If it's a screenspace sprite, then dont apply the transformation from the effectInst Matrix
	if(!m_pParticleRule->GetIsScreenSpace())
	{
		vTransformMtx = vEffectInstMtx;
		if(m_pEmitterRule->GetCreationDomain()->GetIsWorldSpace()) 
		{
			vTransformMtx.SetIdentity3x3();
		} 
	}

	return vTransformMtx;
}

Mat34V_Out ptxEmitterInst::GetTargetDomainLclToWldMtx(Mat34V_In vEffectInstMtx) 
{
	Mat34V vTransformMtx = Mat34V(V_IDENTITY); 

	//If it's a screenspace sprite, then dont apply the transformation from the effectInst Matrix	
	if(!m_pParticleRule->GetIsScreenSpace())
	{
		vTransformMtx = vEffectInstMtx;
		if (m_pEmitterRule->GetTargetDomain()->GetIsCreationRelative())
		{
			vTransformMtx = GetCreationDomainMtxWld(vEffectInstMtx);
		}

		if (m_pEmitterRule->GetTargetDomain()->GetIsWorldSpace()) 
		{
			vTransformMtx.SetIdentity3x3();
		}
	}

	return vTransformMtx;
}

Mat34V_Out ptxEmitterInst::GetAttractorDomainLclToWldMtx(Mat34V_In vEffectInstMtx) 
{
	ptxAssertf(m_pEmitterRule->GetAttractorDomain(), "emitter doesn't have an attractor domain"); 

	Mat34V vTransformMtx = Mat34V(V_IDENTITY); 

	//If it's a screenspace sprite, then dont apply the transformation from the effectInst Matrix
	if(!m_pParticleRule->GetIsScreenSpace())
	{
		vTransformMtx = vEffectInstMtx;

		if (m_pEmitterRule->GetAttractorDomain()->GetIsCreationRelative()) 
		{
			vTransformMtx = GetCreationDomainMtxWld(vEffectInstMtx);
		}
		else if (m_pEmitterRule->GetAttractorDomain()->GetIsTargetRelative()) 
		{
			vTransformMtx = GetTargetDomainMtxWld(vEffectInstMtx);
		}

		if (m_pEmitterRule->GetAttractorDomain()->GetIsWorldSpace()) 
		{
			vTransformMtx.SetIdentity3x3();
		} 
	}

	return vTransformMtx;
}
void ptxEmitterInst::UpdateSetup(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, float UNUSED_PARAM(dt))
{
	// return if we haven't started yet or are finished
	if (m_playState==PTXEMITINSTPLAYSTATE_WAITING_TO_START || m_playState==PTXEMITINSTPLAYSTATE_FINISHED)
	{
		return;
	}

	ptxAssertf(pEffectInst, "trying to update an emitter without an effect inst");
	ptxAssertf(m_pEmitterRule, "trying to update an emitter inst without an emitter rule (effect rule is %s)", pEffectInst->GetEffectRule()->GetName());
	ptxAssertf(m_pParticleRule, "trying to update an emitter inst without a particle rule (effect rule is %s)", pEffectInst->GetEffectRule()->GetName());

	// clear the flags
	ClearFlag(PTXEMITINSTFLAG_CAN_SPAWN);

	// update the life ratios
	if (pEffectInst->GetIsPlaying())
	{
		// calc our life ratios
		float prevEffectLifeRatio = pEffectInst->GetPrevLifeRatio();
		float currEffectLifeRatio = pEffectInst->GetDataDB()->GetCurrLifeRatio();
		m_prevLifeRatio = (prevEffectLifeRatio-m_startRatio) / (m_endRatio-m_startRatio);
		float currLifeRatio = (currEffectLifeRatio-m_startRatio) / (m_endRatio-m_startRatio);
		GetDataDB()->SetCurrLifeRatio(currLifeRatio); 

		// work out whether or not we can spawn particles
		if ((m_prevLifeRatio>=0.0f && m_prevLifeRatio<1.0f) || (currLifeRatio>=0.0f && currLifeRatio<1.0f))
		{
			// check if culling or flags allow us to spawn
			if (pEffectInst->GetIsEmitCulled()==false && pEffectInst->GetDontEmitThisFrame()==false)
			{
				SetFlag(PTXEMITINSTFLAG_CAN_SPAWN);

				// don't let one shots emit unless they've just triggered
				if (m_pEmitterRule->GetIsOneShot() && GetFlag(PTXEMITINSTFLAG_HAS_JUST_STARTED)==false && GetFlag(PTXEMITINSTFLAG_HAS_JUST_LOOPED)==false)
				{
					ClearFlag(PTXEMITINSTFLAG_CAN_SPAWN);
				}
			}
		}
	}

	// update the domains
	UpdateDomains(pEffectInst, pEvoList, GetKeyTime());

	// update the bounds
	UpdateBounds(pEffectInst);
}

void ptxEmitterInst::UpdateCulled(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList)
{
	// return if we haven't started yet or are finished
	if (m_playState==PTXEMITINSTPLAYSTATE_WAITING_TO_START || m_playState==PTXEMITINSTPLAYSTATE_FINISHED)
	{
		return;
	}

	ptxAssertf(m_pEmitterRule, "trying to update an emitter inst without an emitter rule");
	ptxAssertf(m_pParticleRule, "trying to update an emitter inst without a particle rule");

	// update the domains (even when culled we want the previous and current positions to update as these can affect spawning over distance when we become unculled)
	UpdateDomains(pEffectInst, pEvoList, GetKeyTime());

	//Clear out the MT List
	m_pointList.BeginBuffering(RMPTFXTHREADER.GetUpdateBufferId());

	// go through the point list double buffering the active points
	ptxPointItem* pCurrPointItem = (ptxPointItem*)m_pointList.GetHead();
	while (pCurrPointItem!=NULL)
	{
		ptxPointItem* pNextPointItem = (ptxPointItem*)pCurrPointItem->GetNext();

		// check if the point is still alive
		if (pCurrPointItem->GetDataDB()->GetLifeRatio()<=1.0f)
		{
			// copy point data to the update buffer
			pCurrPointItem->CopyToUpdateBuffer();

			bool bufferToHead = m_pParticleRule->GetSortType()==PTXPARTICLERULE_SORTTYPE_OLDEST_FIRST;
			m_pointList.BufferData(RMPTFXTHREADER.GetUpdateBufferId(), pCurrPointItem, bufferToHead);
		}

		pCurrPointItem = pNextPointItem;
	}
}

void ptxEmitterInst::UpdateParallel(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, float dt)
{
	// start a profile timer
#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif

	RMPTFX_UPDATE_CAPTURE_MARKER_PUSH_POP(GetEmitterRule()->GetName());

	// skip update parallel if we haven't started yet or are finished
	bool bSkipUpdateParallel = (m_playState==PTXEMITINSTPLAYSTATE_WAITING_TO_START || m_playState==PTXEMITINSTPLAYSTATE_FINISHED);
	if(!bSkipUpdateParallel)
	{
		ptxAssertf(pEffectInst, "trying to update an emitter without an effect inst");
		ptxAssertf(m_pEmitterRule, "trying to update an emitter inst without an emitter rule");
		ptxAssertf(m_pParticleRule, "trying to update an emitter inst without a particle rule");
		// prepare the evolution data
		if(m_pEmitterRule)
		{
			m_pEmitterRule->GetKeyframePropList().PrepareEvoData(pEvoList);
		}
		// do the parallel update
		if (m_pParticleRule)
		{
			m_pParticleRule->UpdateParallel(pEffectInst, this, pEvoList, dt);
		}

	}

	//call finalize which will spawn the points
	RMPTFX_BANK_ONLY(if(ptxDebug::sm_performParallelFinalize))
	{
		UpdateFinalize(pEffectInst, pEvoList, dt);
	}

	// update the profile data
#if RMPTFX_EDITOR
	if (m_pointList.GetNumItems())
	{
		m_pEmitterRule->GetUIData().IncNumActiveInstances(1);
	}
	if(m_pEmitterRule)
	{
		m_pEmitterRule->GetUIData().IncAccumCPUUpdateTime(timer.GetMsTime());
	}
#endif
}

bool ptxEmitterInst::UpdateFinalize(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, float dt)
{
	// return if we haven't started yet
	if (m_playState==PTXEMITINSTPLAYSTATE_WAITING_TO_START)
	{
		// tell the caller that we are inactive
		return true;
	}

	// return if we have finished
	if (m_playState==PTXEMITINSTPLAYSTATE_FINISHED)
	{
		//Clear out the MT List
		m_pointList.BeginBuffering(RMPTFXTHREADER.GetUpdateBufferId());
		return true;
	}

	ptxAssertf(pEffectInst, "trying to update an emitter without an effect inst");
	ptxAssertf(m_pEmitterRule, "trying to update an emitter inst without an emitter rule");
	ptxAssertf(m_pParticleRule, "trying to update an emitter inst without a particle rule");

	//Clear out the MT List
	m_pointList.BeginBuffering(RMPTFXTHREADER.GetUpdateBufferId());

	// go through each point
	bool prevPointDied = false;
	ptxPointItem* RESTRICT pCurrPointItem = (ptxPointItem*)m_pointList.GetHead();

	if (pCurrPointItem && m_playState!=PTXEMITINSTPLAYSTATE_PLAYING && m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_TRAIL)
	{
		pCurrPointItem->GetDataDB()->SetIsBrokenTrail(true);
	}

	while (pCurrPointItem)
	{
		// get the point data
		ptxPointSB* RESTRICT pPointSB = pCurrPointItem->GetDataSB();
		ptxPointDB* RESTRICT pPointDB = pCurrPointItem->GetDataDB();

		// store the next point
		ptxPointItem* RESTRICT pNextPointItem = (ptxPointItem*)pCurrPointItem->GetNext();
		if(Likely(pNextPointItem))
		{
			PrefetchDC(pNextPointItem->GetDataSB());
			PrefetchDC(pNextPointItem->GetDataDB());
			PrefetchDC(pNextPointItem->GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId()));
		}

		// check if we need to spawn any effect
#if RMPTFX_EDITOR
		if Unlikely(!GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
#endif
		{
			if (Unlikely(pPointSB->GetFlag(PTXPOINT_FLAG_SPAWN_EFFECT)))
			{
				m_pParticleRule->GetEffectSpawnerAtRatio().Trigger(pCurrPointItem, pEffectInst, false);
				pPointSB->ClearFlag(PTXPOINT_FLAG_SPAWN_EFFECT);
			}

			if (Unlikely(pPointSB->GetFlag(PTXPOINT_FLAG_HAS_COLLIDED)))
			{
#if RMPTFX_BANK
				if (ptxDebug::sm_drawPointCollisions)
				{
					//grcDebugDraw::Sphere(pPointSB->GetColnPos(), 0.05f, Color32(0.5f, 0.0f, 1.0f, 1.0f), false, ptxDebug::sm_numPointCollisionFrames);
					grcDebugDraw::Line(pPointSB->GetColnPos(), pPointSB->GetColnPos()+(pPointSB->GetColnNormal()*ScalarVFromF32(0.2f)), Color32(1.0f, 0.0f, 1.0f, 1.0f), Color32(1.0f, 1.0f, 1.0f, 0.5f), -ptxDebug::sm_numPointCollisionFrames);
				}
#endif
				m_pParticleRule->GetEffectSpawnerOnColn().Trigger(pCurrPointItem, pEffectInst, true);
			}
		}


		// update the particle behaviours
		m_pParticleRule->UpdateFinalize(pEffectInst, this, pCurrPointItem, pEvoList, dt);

		if (prevPointDied && m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_TRAIL)
		{
			pPointDB->SetIsBrokenTrail(true);
		}

		// check for dead points
		prevPointDied = false;
		if (Unlikely(pPointDB->GetIsDead()))
		{
			// even though the point gets returned to the pool it gets put into a returned list and takes 2 frames to get back into the pool - enough time for spawned effects to check if the particle is dead and no longer point to it

			// return the dead point
			m_pParticleRule->ReturnPoint(&m_pointList, pCurrPointItem);

			prevPointDied = true;
		}
		else
		{
			// copy point data to the update buffer
			pCurrPointItem->CopyToUpdateBuffer();

			// the point is still alive
			bool bufferToHead = m_pParticleRule->GetSortType()==PTXPARTICLERULE_SORTTYPE_OLDEST_FIRST;
			m_pointList.BufferData(RMPTFXTHREADER.GetUpdateBufferId(), pCurrPointItem, bufferToHead);
		}

		// clear the 'has collided' flag
		pCurrPointItem->GetDataSB()->ClearFlag(PTXPOINT_FLAG_HAS_COLLIDED);

		pCurrPointItem = pNextPointItem;
	}

	// spawn points 
	if (GetFlag(PTXEMITINSTFLAG_CAN_SPAWN))
	{
		SpawnPoints(pEffectInst, pEvoList, dt);
	}

	// copy point data to the update buffer
	CopyToUpdateBuffer();

	// update the 'finished' flag
	int numPoints = 0;
	numPoints = m_pointList.GetNumItems() + m_pointList.GetNumItemsMT(RMPTFXTHREADER.GetDrawBufferId());
	if (numPoints==0 && pEffectInst->GetIsPlaying()==false)
	{
#if RMPTFX_BANK
		OutputPlayStateFunction(pEffectInst, "UpdateFinalize");
#endif
		ptxAssertf(m_playState==PTXEMITINSTPLAYSTATE_STOPPED, "trying to finish an emitter (%s - %s) that isn't stopped (%d)", pEffectInst->GetEffectRule()->GetName(), m_pEmitterRule->GetName(), m_playState);
		SetPlayState(pEffectInst, PTXEMITINSTPLAYSTATE_FINISHED);
	}

	// clear the 'has just...' flags
	ClearFlag(PTXEMITINSTFLAG_HAS_JUST_STARTED);
	ClearFlag(PTXEMITINSTFLAG_HAS_JUST_LOOPED);

	// update the profile data
#if RMPTFX_EDITOR
	int numActivePoints = m_pointList.GetNumItems();
	if (numActivePoints)
	{
		m_pEmitterRule->GetUIData().IncNumActiveInstances(1);
		m_pEmitterRule->GetUIData().IncNumPointsUpdated(numActivePoints);
	}
#endif

	PF_INCREMENTBY(CounterPointsActive, m_pointList.GetNumItems());

	// tell the caller whether we have finished
	return m_playState==PTXEMITINSTPLAYSTATE_FINISHED;
}

void ptxEmitterInst::UpdateBounds(ptxEffectInst* pEffectInst)
{
	ptxEffectRule* pEffectRule = pEffectInst->GetEffectRule();

	// cache some data
	ScalarV vZero(V_ZERO);
	ScalarV vOne(V_ONE);
	ScalarV vZoom = ScalarVFromF32(pEffectInst->GetFinalZoom()*m_zoomScalar);

	// get the cull sphere offset and radius
	Vec3V vCullSphereOffset = pEffectRule->GetViewportCullingSphereOffset();
	ScalarV vCullSphereRadius = ScalarVFromF32(pEffectRule->GetViewportCullingSphereRadius());

	// apply zoom to the cull offset and radius
	Vec3V vCullSphereOffsetZoom = vCullSphereOffset*vZoom;
	ScalarV vCullSphereRadiusZoom = vCullSphereRadius*vZoom;

	// if no cull sphere is present then provide one
	vCullSphereRadiusZoom = SelectFT(IsLessThanOrEqual(vCullSphereRadius, vZero), vCullSphereRadiusZoom, vOne);

	// calc the position by adding the cull sphere offset to the effect inst position
	Vec3V vPos = Transform(pEffectInst->GetMatrix(), vCullSphereOffsetZoom);
	ScalarV vRadius = Abs(vCullSphereRadiusZoom);

	// store the bound data
	GetDataDB()->SetCurrBoundPosRadius(Vec4V(vPos, vRadius));
}

void ptxEmitterInst::Draw(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface))
{
	ptxAssertf(pEffectInst, "trying to draw an emitter without an effect inst");
	ptxAssertf(m_pEmitterRule, "trying to draw an emitter inst without an emitter rule");
	ptxAssertf(m_pParticleRule, "trying to draw an emitter inst without a particle rule");

	// start a profile timer
#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif 

	// draw the active points (via the draw behaviours)
	if (m_pointList.GetNumItemsMT(RMPTFXTHREADER.GetDrawBufferId()))
	{
		m_pParticleRule->Draw(pEffectInst, this, pEvoList PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
	}

	// update the profile data
#if RMPTFX_EDITOR
	m_pEmitterRule->GetUIData().IncAccumCPUDrawTime(timer.GetMsTime());
#endif
}

#if RMPTFX_BANK
void ptxEmitterInst::DebugDraw(ptxEffectInst* pEffectInst, ptxEvolutionList* UNUSED_PARAM(pEvoList), int eventIndex)
{
	bool passedFilterTest = true;

	// don't pass the filter test if an emitter event id filter is specified and the event index isn't the one we're after
	if (ptxDebug::sm_emitterInstEventIdFilter>-1 && ptxDebug::sm_emitterInstEventIdFilter!=eventIndex)
	{
		passedFilterTest = false;
	}

	// don't pass the filter test if an emitter name filter is specified and the emitter name isn't the one we're after
	u32 debugHashValue = atHashValue(ptxDebug::sm_emitterInstNameFilter);
	if (debugHashValue!=0 && debugHashValue!=atHashValue(m_pEmitterRule->GetName()))
	{
		passedFilterTest = false;
	}

	if (passedFilterTest)
	{
		// DRAW DOMAINS
		if (m_pEmitterRule)
		{
			ptxAssertf(pEffectInst, "trying to debug draw an emitter without an effect inst");

			// draw the domains
			if (ptxDebug::sm_drawEmitterInstCreationDomain
#if RMPTFX_EDITOR
				|| m_pEmitterRule->GetUIData().GetShowCreationDomain()
#endif
				)
			{
				Mat34V vCreationDomainWldMtx = GetCreationDomainMtxWld(pEffectInst->GetMatrix());
				m_pEmitterRule->GetCreationDomain()->Draw(vCreationDomainWldMtx, m_creationDomainInst.GetCurrSizeOuter(), m_creationDomainInst.GetCurrSizeInner());
			}

			if (ptxDebug::sm_drawEmitterInstTargetDomain
#if RMPTFX_EDITOR
				|| m_pEmitterRule->GetUIData().GetShowTargetDomain()
#endif
				)
			{
				Mat34V vTargetDomainWldMtx = GetTargetDomainMtxWld(pEffectInst->GetMatrix());
				m_pEmitterRule->GetTargetDomain()->Draw(vTargetDomainWldMtx, m_targetDomainInst.GetCurrSizeOuter(), m_targetDomainInst.GetCurrSizeInner());
			}

			if (m_pEmitterRule->GetAttractorDomain() && (ptxDebug::sm_drawEmitterInstAttractorDomain
#if RMPTFX_EDITOR
				|| m_pEmitterRule->GetUIData().GetShowAttractorDomain()
#endif
				))
			{
				if (m_pAttractorDomainInst)
				{
					Mat34V vAttractorDomainWldMtx = GetAttractorDomainMtxWld(pEffectInst->GetMatrix());
					m_pEmitterRule->GetAttractorDomain()->Draw(vAttractorDomainWldMtx, m_pAttractorDomainInst->GetCurrSizeOuter(), m_pAttractorDomainInst->GetCurrSizeInner());
				}
			}
		}

		// DRAW BOUND BOX
		if (ptxDebug::sm_drawEmitterInstBoundBoxes)
		{
			grcDebugDraw::BoxAxisAligned(GetDataDBMT(RMPTFXTHREADER.GetDrawBufferId())->GetCurrBoundBoxMin(), GetDataDBMT(RMPTFXTHREADER.GetDrawBufferId())->GetCurrBoundBoxMax(), Color32(1.0f, 1.0f, 0.0f, 1.0f), false);	
		}

		// DRAW DEBUG POINTS
		if (ptxDebug::sm_drawPoints)
		{
			// check if there are any active points
			if (m_pointList.GetNumItemsMT(RMPTFXTHREADER.GetDrawBufferId()))
			{
#if PTXDRAW_USE_ALPHA_CULL 
				const float alphaCullThreshold = RMPTFX_BANK_SWITCH(ptxDebug::sm_pointAlphaCullThreshold, PTXDRAW_ALPHA_CULL_THRESHOLD);
#endif

				// set the render state
				ptxPointDB* pPointDB;
				ptxPointItem* pCurrPointItem = (ptxPointItem*)m_pointList.GetHeadMT(RMPTFXTHREADER.GetDrawBufferId());
				ptxPointItem* pNextPointItem = NULL;
				while (pCurrPointItem)
				{
					pPointDB = pCurrPointItem->GetDataDBMT(RMPTFXTHREADER.GetDrawBufferId());
					pNextPointItem = (ptxPointItem*)pCurrPointItem->GetNextMT(RMPTFXTHREADER.GetDrawBufferId());

#if PTXDRAW_USE_ALPHA_CULL 
					float pointAlpha = pPointDB->GetColour().GetAlphaf();
					if (pointAlpha<alphaCullThreshold)
					{
						grcDebugDraw::Cross(pPointDB->GetCurrPos(), 0.1f, Color32(1.0f, 0.2f, 0.2f, 1.0f));
					}
					else
#endif
					{
						grcDebugDraw::Cross(pPointDB->GetCurrPos(), 0.1f, Color32(1.0f, 0.8f, 0.2f, 1.0f));
					}

					if (ptxDebug::sm_drawPointAlphas)
					{
						char displayTxt[8];
						displayTxt[0] = '\0';
						formatf(displayTxt, 8, "%.2f", pPointDB->GetColour().GetAlphaf());

#if PTXDRAW_USE_ALPHA_CULL 
						if (pointAlpha<alphaCullThreshold)
						{
							grcDebugDraw::Text(pPointDB->GetCurrPos(), Color32(1.0f, 0.0f, 0.0f, 1.0f), 0, 10, displayTxt, false);
						}
						else
#endif
						{
							grcDebugDraw::Text(pPointDB->GetCurrPos(), Color32(1.0f, 0.5f, 0.0f, 1.0f), 0, 10, displayTxt, false);
						}

					}

					pCurrPointItem = pNextPointItem;
				}
			}
		}

		// DRAW DEBUG TRAILS
		if (ptxDebug::sm_drawTrails)
		{
			if (m_pParticleRule && m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_TRAIL)
			{
				// check if there are any active points
				if (m_pointList.GetNumItemsMT(RMPTFXTHREADER.GetDrawBufferId()))
				{
					// set the render state
					ptxPointDB* pPointDB;
					ptxPointDB* pNextPointDB;
					int numPoints = m_pointList.GetNumItemsMT(RMPTFXTHREADER.GetDrawBufferId());
					float greenShift = 1.0f/numPoints;
					float currGreen = 0.0f;
					ptxPointItem* pCurrPointItem = (ptxPointItem*)m_pointList.GetHeadMT(RMPTFXTHREADER.GetDrawBufferId());
					ptxPointItem* pNextPointItem = NULL;
					while (pCurrPointItem)
					{
						pPointDB = pCurrPointItem->GetDataDBMT(RMPTFXTHREADER.GetDrawBufferId());
						pNextPointItem = (ptxPointItem*)pCurrPointItem->GetNextMT(RMPTFXTHREADER.GetDrawBufferId());
						pNextPointDB = pNextPointItem->GetDataDBMT(RMPTFXTHREADER.GetDrawBufferId());

						grcDebugDraw::Cross(pPointDB->GetCurrPos(), 0.1f, Color32(1.0f, currGreen, 0.0f, 1.0f));

						if (m_pParticleRule->GetSortType()==PTXPARTICLERULE_SORTTYPE_OLDEST_FIRST)
						{
							if (pNextPointItem && pPointDB->GetIsBrokenTrail()==false)
							{
								grcDebugDraw::Line(pPointDB->GetCurrPos(), pNextPointDB->GetCurrPos(), Color32(1.0f, 0.0f, 0.0f, 1.0f));
							}
						}
						else if (m_pParticleRule->GetSortType()==PTXPARTICLERULE_SORTTYPE_NEWEST_FIRST)
						{
							if (pNextPointItem && pNextPointDB->GetIsBrokenTrail()==false)
							{
								grcDebugDraw::Line(pPointDB->GetCurrPos(), pNextPointDB->GetCurrPos(), Color32(1.0f, 0.0f, 0.0f, 1.0f));
							}
						}

						currGreen += greenShift;

						pCurrPointItem = pNextPointItem;
					}
				}
			}
		}
	}
}
#endif

void ptxEmitterInst::AllocateMemory(ptxEmitterRule* pEmitterRule)
{
	if (ptxVerifyf(m_pAttractorDomainInst==NULL, "attractor domain inst already allocated"))
	{
		// check if the emitter rule has an attractor domain
		if (pEmitterRule->GetAttractorDomain())
		{
			// it does - we need to allocate an attractor domain inst
			m_pAttractorDomainInst = rage_new ptxDomainInst;
		}
	}
}

void ptxEmitterInst::DeallocateMemory()
{
	// check if we allocated an attractor domain inst
	if (m_pAttractorDomainInst)
	{
		// we did - tidy up
		delete m_pAttractorDomainInst;
		m_pAttractorDomainInst = NULL;
	}
}

unsigned ptxEmitterInst::SortPoints(ptxPointItem** ppSorted)
{
	u8 drawBufferId = RMPTFXTHREADER.GetDrawBufferId();

	// return if there are no points to sort
	const int numPoints = m_pointList.GetNumItemsMT(drawBufferId);
	if (numPoints==0)
	{
		return 0;
	}

	// start a profile timer
#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif

	// oldest first and newest first are already sorted as they get added to the list at one end when spawned
	if (m_pParticleRule->GetSortType()==PTXPARTICLERULE_SORTTYPE_OLDEST_FIRST || m_pParticleRule->GetSortType()==PTXPARTICLERULE_SORTTYPE_NEWEST_FIRST)
	{
		// copy pointers
		ptxPointItem* pCurrPointItem = (ptxPointItem*)m_pointList.GetHeadMT(drawBufferId);
		for (int i=0; i<numPoints; i++)
		{
			ppSorted[i] = pCurrPointItem;
			ptxAssertf(((i == (numPoints-1)) || pCurrPointItem->GetNextMT(drawBufferId)), "numPoints don't match list");
			pCurrPointItem = (ptxPointItem*)pCurrPointItem->GetNextMT(drawBufferId);
		}

		// update the profile data
#if RMPTFX_EDITOR
		m_pEmitterRule->GetUIData().IncAccumCPUSortTime(timer.GetMsTime());
#endif
		return numPoints;
	}

	struct KeyData
	{
		float key;
		ptxPointItem *pPointItem;

		inline bool operator<(const KeyData& rhs) const
		{
			return rhs.key < key;
		}
	};

	// acquire a buffer for sorting the points
	ptxAssertf(numPoints > 0 && numPoints < RMPTFXMGR.GetPointPool().GetNumItems(), "numPoints (%d) out of range", numPoints);

	KeyData* ppSortBuffer = Alloca(KeyData, numPoints);
#if RMPTFX_BANK
	KeyData* ppSortBufferOrig = Alloca(KeyData, numPoints);
#endif

	// cache the camera position
	Vec3V vCamPos;
#if RMPTFX_USE_PARTICLE_SHADOWS
	if (RMPTFXMGR.IsShadowPass())
		vCamPos = RMPTFXMGR.GetShadowSourcePos();
	else
#endif
		vCamPos = grcViewport::GetCurrent()->GetCameraPosition();

	// temporary debug code for BS#591930
#if RMPTFX_BANK
	ptxManager::sm_debugLastSortPointsCount = numPoints;
	ptxManager::sm_debugLastSortPointsBuffer = ppSortBuffer;
	ptxManager::sm_debugLastSortPointsBufferOrig = ppSortBufferOrig;
	ptxAssertf(numPoints == m_pointList.GetNumItemsMT(drawBufferId), "numPoints has changed");
#endif

	// go through the points calculating their sort value (if required) and adding them to the sort buffer
	ptxPointItem* pCurrPointItem = (ptxPointItem*)m_pointList.GetHeadMT(drawBufferId);
	ptxPointItem* pNextPointItem = pCurrPointItem;
	for (int i=0; i<numPoints; i++)
	{
		ptxPointSB* pPointSB = pCurrPointItem->GetDataSB();
		ptxPointDB* pPointDB = pCurrPointItem->GetDataDBMT(drawBufferId);
		pNextPointItem = (ptxPointItem*)pCurrPointItem->GetNextMT(drawBufferId);
		if (pNextPointItem)
		{
			ptxAssertf(i<numPoints-1, "last point has an non-null next pointer");
			PrefetchObject(pNextPointItem);
		}
#if __ASSERT
		else
		{
			ptxAssertf(i==numPoints-1, "last point has an invalid next pointer");
		}
#endif

		switch (m_pParticleRule->GetSortType())
		{
		case PTXPARTICLERULE_SORTTYPE_RANDOM:
			// use the random sort value that was set on initialisation
			ppSortBuffer[i].key = pPointSB->GetSortVal();
			ptxAssertf(ppSortBuffer[i].key==ppSortBuffer[i].key, "trying to sort a point with a non finite sort key (1)");
			ptxAssertf(FPIsFinite(ppSortBuffer[i].key), "trying to sort a point with a non finite sort key (1a)");
			break;

		case PTXPARTICLERULE_SORTTYPE_FARTHEST_FIRST:
			// use the distance to the camera (squared)
			ptxAssertf(IsFiniteAll(pPointDB->GetCurrPos()), "trying to sort a point with a non finite position");
			ptxAssertf(IsFiniteAll(vCamPos), "trying to sort a point with a non finite camera position");
			ppSortBuffer[i].key = DistSquared(pPointDB->GetCurrPos(), vCamPos).Getf();
			ptxAssertf(ppSortBuffer[i].key==ppSortBuffer[i].key, "trying to sort a point with a non finite sort key (2)");
			ptxAssertf(FPIsFinite(ppSortBuffer[i].key), "trying to sort a point with a non finite sort key (2a)");
			break;

		case PTXPARTICLERULE_SORTTYPE_CLOSEST_FIRST:
			// use the negative distance to the camera (squared)
			ptxAssertf(IsFiniteAll(pPointDB->GetCurrPos()), "trying to sort a point with a non finite position");
			ptxAssertf(IsFiniteAll(vCamPos), "trying to sort a point with a non finite camera position");
			ppSortBuffer[i].key = -DistSquared(pPointDB->GetCurrPos(), vCamPos).Getf();
			ptxAssertf(ppSortBuffer[i].key==ppSortBuffer[i].key, "trying to sort a point with a non finite sort key (3)");
			ptxAssertf(FPIsFinite(ppSortBuffer[i].key), "trying to sort a point with a non finite sort key (3a)");
			break;

		default:
			ptxAssertf(0, "sort type unsupported");
			ppSortBuffer[i].key = 0.0f;
		}

		ppSortBuffer[i].pPointItem = pCurrPointItem;

		// check for non finite keys - we don't want this to crash!
		if (ppSortBuffer[i].key!=ppSortBuffer[i].key || FPIsFinite(ppSortBuffer[i].key)==false)
		{
			ppSortBuffer[i].key = 0.0f;
		}

#if RMPTFX_BANK
		ppSortBufferOrig[i] = ppSortBuffer[i];
#endif

		pCurrPointItem = pNextPointItem;
	}

	// place the last point into the array
	ptxAssertf(pCurrPointItem==NULL, "point list is invalid");

	// sort the buffer
	std::sort(ppSortBuffer, ppSortBuffer+numPoints);

	for (int i=0; i<numPoints; ++i)
	{
		ppSorted[i] = ppSortBuffer[i].pPointItem;
	}

	// update the profile data
#if RMPTFX_EDITOR
	m_pEmitterRule->GetUIData().IncAccumCPUSortTime(timer.GetMsTime());
#endif

	return numPoints;
}

void ptxEmitterInst::UpdateDomains(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, ScalarV_In vKeyTimeEmitter)
{
	ptxAssertf(pEffectInst, "trying to update an emitter without an effect inst");
	ptxAssertf(m_pEmitterRule, "trying to update an emitter inst without an emitter rule");
	ptxAssertf(m_pParticleRule, "trying to update an emitter inst without a particle rule");

	// prepare the evolution data
	// Keeping this commented out as we need to prepare the evo data on the update task also
	//if (RMPTFXMGR.GetUpdateThreadId() == sysIpcGetCurrentThreadId())
	{
		m_pEmitterRule->GetCreationDomain()->PrepareEvoData(pEvoList);
		m_pEmitterRule->GetTargetDomain()->PrepareEvoData(pEvoList);

		if (m_pEmitterRule->GetAttractorDomain())
		{
			m_pEmitterRule->GetAttractorDomain()->PrepareEvoData(pEvoList);
		}
	}

	// update the creation and target domains
	UpdateDomain(PTXDOMAIN_TYPE_CREATION, pEffectInst, pEvoList, vKeyTimeEmitter);
	UpdateDomain(PTXDOMAIN_TYPE_TARGET, pEffectInst, pEvoList, vKeyTimeEmitter);
	UpdateDomain(PTXDOMAIN_TYPE_ATTRACTOR, pEffectInst, pEvoList, vKeyTimeEmitter);

	// store the previous domain position
	m_vPrevPosWld = GetDataDB()->GetCurrPosWld();

	// set the current domain position
	Vec3V vCurrPosLcl = m_creationDomainInst.GetPosLcl();
	GetDataDB()->SetCurrPosWld(Transform(pEffectInst->GetMatrix(), vCurrPosLcl));

	// if this emitter has just started then make sure the previous position is the same as the current
	if (GetFlag(PTXEMITINSTFLAG_HAS_JUST_STARTED))
	{
		m_vPrevPosWld = GetDataDB()->GetCurrPosWld();
	}
}

void ptxEmitterInst::UpdateDomain(ptxDomainType domainType, ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, ScalarV_In vKeyTimeEmitter)
{
	// cache the required domain and domain inst
	ptxDomain* pDomain = m_pEmitterRule->GetCreationDomain();
	ptxDomainInst* pDomainInst = &m_creationDomainInst;
	if (domainType==PTXDOMAIN_TYPE_TARGET)
	{
		pDomain = m_pEmitterRule->GetTargetDomain();
		pDomainInst = &m_targetDomainInst;
	}
	else if (domainType==PTXDOMAIN_TYPE_ATTRACTOR)
	{
		pDomain = m_pEmitterRule->GetAttractorDomain();
		pDomainInst = m_pAttractorDomainInst;

		// attractor domains don't always exist
		if (pDomain==NULL || pDomainInst==NULL)
		{
			return;
		}
	}

	// cache some data
	Vec3V vScale = Vec3VFromF32(pEffectInst->GetFinalZoom() * m_zoomScalar);

	ptxEvoValueType* pEvoValues = NULL;
	if (pEffectInst->GetEffectRule()->GetEvolutionList())
	{
		pEvoValues = pEffectInst->GetEvoValues();
	}

	// deal with any size overrides
	Vec3V vOverrideSize = Vec3V(V_ZERO);
	if (domainType==PTXDOMAIN_TYPE_CREATION)
	{
		vOverrideSize = pEffectInst->GetOverrideCreationDomainSize();
	}

	// update the domain
	pDomainInst->Update(domainType, pDomain, vKeyTimeEmitter, vScale, pEvoList, pEvoValues, vOverrideSize);
}

void ptxEmitterInst::SpawnPoints(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, float dt)
{
#if RMPTFX_TIMERS
	sysTimer timer;
	timer.Reset();
#endif

	const ScalarV vDt = ScalarVFromF32(dt);
	// check everything is ok
	float currLifeRatio = GetDataDB()->GetCurrLifeRatio();
	ptxAssertf((m_prevLifeRatio>=0.0f && m_prevLifeRatio<1.0f) || (currLifeRatio>=0.0f && currLifeRatio<1.0f), "trying to spawn points from an inactive emitter");

	// cache some data
	float origEmitterLifeRatio = currLifeRatio;
	ScalarV origEmitterLifeRatioV = GetDataDB()->GetCurrLifeRatioV();
	Vec3V vOrigEmitterPrevPosWld = m_vPrevPosWld;
	Vec3V vOrigEmitterCurrPosWld = GetDataDB()->GetCurrPosWld();
	float prevSpawnCount = m_currSpawnCount;

	// calc the elapsed life ratio and over what ratio of the frame the emitter was active
	float elapsedEmitterLifeRatio = origEmitterLifeRatio - m_prevLifeRatio;		// the elapsed emitter life ratio this frame (curr-prev unless we've looped)
	float frameCoverageStartRatio = 0.0f;										// the ratio through the frame that emission started
	float frameCoverageEndRatio = 1.0f;											// the ratio through the frame that emission stopped
	bool emitterEndedThisFrame = false;

	float emitterRatio = m_endRatio - m_startRatio; 
	float invEmitterRatio = 1.0f/emitterRatio;

	// get the emitter life ratio key time
	ScalarV vKeyTimeEmitter = GetKeyTime();

	if (GetIsContinous())
	{
		// continuous emitters last the entire duration of the effect so the coverage start ratio is always zero
		// the coverage end ratio may be less than one if the effect is finishing
		float currEffectLifeRatio = pEffectInst->GetDataDB()->GetCurrLifeRatio();
		if (currEffectLifeRatio>=1.0f && origEmitterLifeRatio>=1.0f)
		{
			// emitter only active over first part of frame
			frameCoverageEndRatio = (1.0f-m_prevLifeRatio) / (origEmitterLifeRatio-m_prevLifeRatio);
			emitterEndedThisFrame = true;
		}

		if (m_prevLifeRatio<origEmitterLifeRatio)
		{
			// effect hasn't looped this frame
		}
		else
		{
			// effect has looped this frame
			float currLifeRatioUnclipped = origEmitterLifeRatio+invEmitterRatio;
			elapsedEmitterLifeRatio = currLifeRatioUnclipped - m_prevLifeRatio;
		}
	}
	else
	{
		if (m_prevLifeRatio<origEmitterLifeRatio)
		{
			// effect hasn't looped this frame 
			if (m_prevLifeRatio<0.0f)
			{
				// emitter only active over last part of frame
				frameCoverageStartRatio = (0.0f-m_prevLifeRatio) / (origEmitterLifeRatio-m_prevLifeRatio);
			}
			else if (origEmitterLifeRatio>1.0f)
			{
				// emitter only active over first part of frame
				frameCoverageEndRatio = (1.0f-m_prevLifeRatio) / (origEmitterLifeRatio-m_prevLifeRatio);
				emitterEndedThisFrame = true;
			}
			else
			{
				// emitter active the entire frame - use default values
				ptxAssertf(m_prevLifeRatio>=0.0f && origEmitterLifeRatio<=1.0f, "invalid emitter state encountered");
			}
		}
		else
		{
			// effect has looped this frame
			float currLifeRatioUnclipped = origEmitterLifeRatio+invEmitterRatio;
			elapsedEmitterLifeRatio = currLifeRatioUnclipped - m_prevLifeRatio;

			ptxAssertf(m_prevLifeRatio>=0.0f, "invalid emitter state encountered");
			if (origEmitterLifeRatio<0.0f)
			{
				// emitter hasn't restarted yet
				if (m_prevLifeRatio>1.0f)
				{
					// emitter was inactive previously
					frameCoverageEndRatio = 0.0f;
				}
				else
				{
					// emitter was active previously
					frameCoverageEndRatio = (1.0f-m_prevLifeRatio) / (currLifeRatioUnclipped-m_prevLifeRatio);
					emitterEndedThisFrame = true;
				}
			}
			else 
			{
				// emitter has restarted
				if (m_prevLifeRatio>1.0f)
				{
					// emitter was inactive previously
					float prevLifeRatioUnclipped = m_prevLifeRatio-invEmitterRatio;
					frameCoverageStartRatio = (0.0f-prevLifeRatioUnclipped) / (origEmitterLifeRatio-prevLifeRatioUnclipped);
				}
				else
				{
					// emitter was active previously
					// problem case - the emitter was active previously and is active again now but was inactive at some point during the frame
					// for simplicity just pretend that it's been active the entire frame (but flag an assert so that the artist can re-tune if necessary)
					// note that one shot emitter may hit this assert - just ignore if it is a one shot
					if (m_pEmitterRule->GetIsOneShot()==false)
					{
						ptxAssertf(0, "%s's %s emitter has a problematic start and end ratio (%.3f and %.3f) - try re-tuning", pEffectInst->GetEffectRule()->GetName(), m_pEmitterRule->GetName(), m_startRatio, m_endRatio);
					}
				}
			}
		}
	}

	ptxAssertf(elapsedEmitterLifeRatio>=0.0f, "invalid elapsed emitter life ratio calculated");

	ptxAssertf(frameCoverageEndRatio>=frameCoverageStartRatio, "invalid frame coverage data calculated (%.3f, %.3f - %.3f, %.3f)", frameCoverageStartRatio, frameCoverageEndRatio, origEmitterLifeRatio, m_prevLifeRatio);
	float frameCoverageRatio = frameCoverageEndRatio - frameCoverageStartRatio;


	// get the keyframe data
	Vec4V vSpawnRateOverTimeMinMax = m_pEmitterRule->GetSpawnRateOverTimeKFP().Query(vKeyTimeEmitter, pEvoList, pEffectInst->GetEvoValues());
	Vec4V vSpawnRateOverDistMinMax = m_pEmitterRule->GetSpawnRateOverDistKFP().Query(vKeyTimeEmitter, pEvoList, pEffectInst->GetEvoValues());
	Vec4V vPlaybackRateScalar = m_pEmitterRule->GetPlaybackRateScalarKFP().Query(vKeyTimeEmitter, pEvoList, pEffectInst->GetEvoValues());

	// 
	float spawnRateOverTime = Max(ScalarV(V_ZERO), g_ptxVecRandom.GetRangedScalarV(vSpawnRateOverTimeMinMax.GetX(), vSpawnRateOverTimeMinMax.GetY())).Getf();
	float playbackRate = pEffectInst->GetPlaybackRateScalar() * m_playbackRateScalar * vPlaybackRateScalar.GetXf();

	// update the spawn count and calculate how many points to spawn this frame
	int numToSpawn = 0;
	float spawnCountThisFrame = 0.0f;
	if (m_pEmitterRule->GetIsOneShot())
	{
		// one shot emitters spawn all points on the first frame
		numToSpawn = (int)spawnRateOverTime;
	}
	else
	{
		// get the keyframe data for spawn over distance
		float distTravelled = Dist(m_vPrevPosWld, GetDataDB()->GetCurrPosWld()).Getf();
		distTravelled = FPIfGteZeroThenElse(pEffectInst->GetOverrideDistTravelled(), pEffectInst->GetOverrideDistTravelled(), distTravelled);
		float spawnRateOverDist = Max(0.0f, g_DrawRand.GetRanged(vSpawnRateOverDistMinMax.GetXf(), vSpawnRateOverDistMinMax.GetYf()));

		if (pEffectInst->GetCanMoveVeryFast()==false)
		{
			// check if an emitter that spawns over distance have moved too far this frame
			//ptxAssertf(spawnRateOverDist==0.0f || (distTravelled>=0.0f && distTravelled<=25.0f), "emitter has travelled more than 25m spawning over distance (effect:%s emitter:%s - prevPos:%.3f,%.3f,%.3f, currPos:%.3f,%.3f,%.3f), origPrevPos:%.3f,%.3f,%.3f, origCurrPos:%.3f,%.3f,%.3f, override:%.3f spawnRate:%.3f)", pEffectInst->GetEffectRule()->GetName(), m_pEmitterRule->GetName(), m_vPrevPosWld.GetXf(), m_vPrevPosWld.GetYf(), m_vPrevPosWld.GetZf(), GetDataDB()->GetCurrPosWld().GetXf(), GetDataDB()->GetCurrPosWld().GetYf(), GetDataDB()->GetCurrPosWld().GetZf(), vOrigEmitterPrevPosWld.GetXf(), vOrigEmitterPrevPosWld.GetYf(), vOrigEmitterPrevPosWld.GetZf(), vOrigEmitterCurrPosWld.GetXf(), vOrigEmitterCurrPosWld.GetYf(), vOrigEmitterCurrPosWld.GetZf(), pEffectInst->GetOverrideDistTravelled(), spawnRateOverDist);
			if (spawnRateOverDist>0.0f && distTravelled>25.0f)
			{
				// it has - don't spawn
				m_currSpawnCount = 0.0f;
				return;
			}
		}

#if __ASSERT
		if (m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_TRAIL)
		{
			ptxAssertf(spawnRateOverDist==0.0f, "%s.effectrule has a spawn over time trail event (%s.emitrule - %s.ptxrule)", pEffectInst->GetEffectRule()->GetName(), m_pEmitterRule->GetName(), m_pParticleRule->GetName());
		}
#endif

		// update how many particles need spawned by this emitter this frame (over time and distance)
		spawnCountThisFrame += (dt*playbackRate*spawnRateOverTime*frameCoverageRatio);
		ptxAssertf(spawnCountThisFrame>=0.0f && spawnCountThisFrame<=2000.0f, "spawn count this frame has gone wrong (effect:%s emitter:%s - dt:%.3f, playbackRate:%.3f, spawnRateOverTime:%.3f, frameCoverageRatio:%.3f)", pEffectInst->GetEffectRule()->GetName(), m_pEmitterRule->GetName(), dt, playbackRate, spawnRateOverTime, frameCoverageRatio);
		spawnCountThisFrame += (distTravelled*spawnRateOverDist*frameCoverageRatio);
		ptxAssertf(spawnCountThisFrame>=0.0f && spawnCountThisFrame<=2000.0f, "spawn count this frame has gone wrong (effect:%s emitter:%s - distTravelled:%.3f, spawnRateOverTime:%.3f, frameCoverageRatio:%.3f)", pEffectInst->GetEffectRule()->GetName(), m_pEmitterRule->GetName(), distTravelled, spawnRateOverTime, frameCoverageRatio);
		
		// if we're spawning this frame and the emitter is finishing then add a little extra to the spawn count to accommodate errors
		// e.g. a 2 seconds emitter emitting 2 particles a seconds should always emit 4 particles but spawn count may be just under a full particle
		if (spawnCountThisFrame>0.0f && emitterEndedThisFrame)
		{
			spawnCountThisFrame += 0.001f;
		}

		// add this to the current spawn count for this emitter
		ptxAssertf(m_currSpawnCount>=0.0f && m_currSpawnCount<=2000.0f, "spawn count has gone wrong");
		m_currSpawnCount += spawnCountThisFrame;
		ptxAssertf(m_currSpawnCount>=0.0f && m_currSpawnCount<=2000.0f, "spawn count has gone wrong");

		// get number of particles to spawn this frame (as a whole number)
		numToSpawn = (int)m_currSpawnCount;

		// update how many are left to spawn
		m_currSpawnCount -= rage::FloorfFast(m_currSpawnCount);
	}

	// apply the active point cap (if specified)
	int activePointCap = (int)vSpawnRateOverTimeMinMax.GetZf();
	if (activePointCap>0)
	{
		numToSpawn = Min(numToSpawn, activePointCap-m_pointList.GetNumItems());
	}

	// calculate how many points we can actually spawn (we may be running out)
	int numToSpawnActual = numToSpawn;
	ptxPointItem* pFirstNewPoint = m_pParticleRule->ReservePoints(&m_pointList, numToSpawnActual);
#if	RMPTFX_BANK
	if (m_pEmitterRule->GetIsOneShot() && ptxDebug::sm_enableExpensiveOneShotSubFrameCalcs==false)
#else
	if (m_pEmitterRule->GetIsOneShot())
#endif
	{		
		float subFrameRatio = frameCoverageStartRatio;
		const ScalarV vSubFrameRatio = ScalarVFromF32(frameCoverageStartRatio);
		Vec3V vSubFrameEffectPos = Lerp(vSubFrameRatio, pEffectInst->GetPrevPos(), pEffectInst->GetCurrPos());

		// set up the sub frame life ratio of the particle (as it would be when the particle was spawned)
		float currLifeRatio = m_prevLifeRatio + (subFrameRatio*elapsedEmitterLifeRatio);
		if (emitterEndedThisFrame==false && currLifeRatio>=invEmitterRatio)
		{
			currLifeRatio -= invEmitterRatio;
		}

		// errors may make this go slightly out of range - check the range with a little room for error then clamp
		ptxAssertf(currLifeRatio>-0.001f && currLifeRatio<1.001f, "emitter sub frame life ratio is out of range (effect:%s emitter:%s - dt:%.3f, currLifeRatio:%.3f, origLifeRatio:%.3f, prevLifeRatio:%.3f, subFrameRatio:%.3f, elpasedEmitterLifeRatio:%.3f)", pEffectInst->GetEffectRule()->GetName(), m_pEmitterRule->GetName(), dt, currLifeRatio, origEmitterLifeRatio, m_prevLifeRatio, subFrameRatio, elapsedEmitterLifeRatio);
		currLifeRatio = Clamp(currLifeRatio, 0.0f, 1.0f);
		GetDataDB()->SetCurrLifeRatio(currLifeRatio);

		// update the current position to the sub frame position (but keep the original previous position)
		UpdateDomains(pEffectInst, pEvoList, ScalarVFromF32(currLifeRatio));
		m_vPrevPosWld = vOrigEmitterPrevPosWld;
		ptxPointItem* pPointItem = pFirstNewPoint;
		// spawn the actual points
		for (int i=0; i<numToSpawnActual; i++)
		{	
			ptxAssertf(pPointItem, "Current Point is NULL");

			// initialise the new point
			InitPoint(pEffectInst, pPointItem, pEvoList, vDt, playbackRate, vSubFrameRatio, vSubFrameEffectPos);

			// check if we need to spawn any effect
#if RMPTFX_EDITOR
			if Unlikely(!GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
#endif
			{
				ptxPointSB* pPointSB = pPointItem->GetDataSB();
				if (Unlikely(pPointSB->GetFlag(PTXPOINT_FLAG_SPAWN_EFFECT)))
				{
					m_pParticleRule->GetEffectSpawnerAtRatio().Trigger(pPointItem, pEffectInst, false);
					pPointSB->ClearFlag(PTXPOINT_FLAG_SPAWN_EFFECT);
				}
			}

			//Items are added as the new head of the group. So we should traverse backwards from the first item
			pPointItem = (ptxPointItem*)pPointItem->GetPrev();
		}
	}
	else
	{
		ptxPointItem* pPointItem = pFirstNewPoint;
		// spawn the actual points
		for (int i=0; i<numToSpawnActual; i++)
		{	
			ptxAssertf(pPointItem, "Current Point is NULL");
			// calculate the ratio within the frame that the points should be spawned
			// we need the time ratios during this frame that the spawn count was a whole number
			float subFrameRatio;
#if RMPTFX_BANK
			if (m_pEmitterRule->GetIsOneShot())
			{
				subFrameRatio = frameCoverageStartRatio;
			}
			else
#endif
			{
				float currSpawn = i+(1.0f-prevSpawnCount);
				subFrameRatio = frameCoverageStartRatio + ((currSpawn/spawnCountThisFrame) * frameCoverageRatio);
			}
			ScalarV vSubFrameRatio = ScalarV(subFrameRatio);

			// calculate the sub frame effect position
			// if its a screen space particle effect, lets default it to zero
			Vec3V vSubFrameEffectPos = (m_pParticleRule->GetIsScreenSpace()? Vec3V(V_ZERO):Lerp(vSubFrameRatio, pEffectInst->GetPrevPos(), pEffectInst->GetCurrPos()));

			// set up the sub frame life ratio of the particle (as it would be when the particle was spawned)
			float currLifeRatio = m_prevLifeRatio + (subFrameRatio*elapsedEmitterLifeRatio);
			if (emitterEndedThisFrame==false && currLifeRatio>=invEmitterRatio)
			{
				currLifeRatio -= invEmitterRatio;
			}

			// errors may make this go slightly out of range - check the range with a little room for error then clamp
			ptxAssertf(currLifeRatio>-0.001f && currLifeRatio<1.001f, "emitter sub frame life ratio is out of range (effect:%s emitter:%s - dt:%.3f, currLifeRatio:%.3f, origLifeRatio:%.3f, prevLifeRatio:%.3f, subFrameRatio:%.3f, elpasedEmitterLifeRatio:%.3f)", pEffectInst->GetEffectRule()->GetName(), m_pEmitterRule->GetName(), dt, currLifeRatio, origEmitterLifeRatio, m_prevLifeRatio, subFrameRatio, elapsedEmitterLifeRatio);
			currLifeRatio = Clamp(currLifeRatio, 0.0f, 1.0f);
			GetDataDB()->SetCurrLifeRatio(currLifeRatio);

			// update the current position to the sub frame position (but keep the original previous position)
			UpdateDomains(pEffectInst, pEvoList, ScalarVFromF32(currLifeRatio));
			m_vPrevPosWld = vOrigEmitterPrevPosWld;

			// initialise the new point
			InitPoint(pEffectInst, pPointItem, pEvoList, vDt, playbackRate, vSubFrameRatio, vSubFrameEffectPos);

			// check if we need to spawn any effect
#if RMPTFX_EDITOR
			if Unlikely(!GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
#endif
			{
				ptxPointSB* pPointSB = pPointItem->GetDataSB();
				if (Unlikely(pPointSB->GetFlag(PTXPOINT_FLAG_SPAWN_EFFECT)))
				{
					m_pParticleRule->GetEffectSpawnerAtRatio().Trigger(pPointItem, pEffectInst, false);
					pPointSB->ClearFlag(PTXPOINT_FLAG_SPAWN_EFFECT);
				}
			}

			//Items are added as the new head of the group. So we should traverse backwards from the first item
			pPointItem = (ptxPointItem*)pPointItem->GetPrev();
		}
	}

	// reset the life ratio to the value it was when entering the function (we have updated this during each spawn)
	GetDataDB()->SetCurrLifeRatioV(origEmitterLifeRatioV);

	// reset the positions to the values they were when entering the function (we have updated them during each spawn)
	m_vPrevPosWld = vOrigEmitterPrevPosWld;
	GetDataDB()->SetCurrPosWld(vOrigEmitterCurrPosWld);

	// output profile data
#if RMPTFX_TIMERS
	OutputUpdateProfileInfo(pEffectInst, "SpawnPoints", timer);
#endif
}

void ptxEmitterInst::InitPoint(ptxEffectInst* pEffectInst, ptxPointItem* pPointItem, ptxEvolutionList* pEvolutionList, ScalarV_In dt, float playbackRate, ScalarV_In subFrameRatio, Vec3V_In vSubFrameEffectPos)
{
#if RMPTFX_TIMERS
	sysTimer timer;
	timer.Reset();
#endif

	// ratio: 0.0 = start of frame, 1.0 = end of frame

	// NOTE: the emitter domain information and the effect rotation have values set at the end of the frame
	//       only the effect position has the correct sub-frame value set
	//       this could be improved but would be more costly

	// initial particle positions and velocities are determined within this function
	if (pPointItem==NULL) 
	{
		return;
	}

	PrefetchDC(pPointItem->GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId()));
	PrefetchDC(pPointItem->GetDataDB());



	// get the emitter life ratio key time
	ScalarV vKeyTimeEmitter = GetKeyTime();

	// get the keyframe data
	Vec4V vParticleLifeMinMax = m_pEmitterRule->GetParticleLifeKFP().Query(vKeyTimeEmitter, pEvolutionList, pEffectInst->GetEvoValues());

	ScalarV particleLife = g_ptxVecRandom.GetRangedScalarV(vParticleLifeMinMax.GetX(), vParticleLifeMinMax.GetY());
	particleLife /= ScalarVFromF32(playbackRate);

	// make sure the particle life never gets too small
	particleLife = SelectFT(IsLessThan(particleLife, ScalarV(V_FLT_SMALL_2)), particleLife, ScalarV(V_FLT_SMALL_2));

	const ScalarV ooParticleLife = Invert(particleLife);

	Vec4V vSpeedScalarMinMax = m_pEmitterRule->GetSpeedScalarKFP().Query(vKeyTimeEmitter, pEvolutionList, pEffectInst->GetEvoValues());
	ScalarV speedScalar = g_ptxVecRandom.GetRangedScalarV(vSpeedScalarMinMax.GetX(), vSpeedScalarMinMax.GetY());

	Vec4V vInheritVelocityMinMax = m_pEmitterRule->GetInheritVelocityKFP().Query(vKeyTimeEmitter, pEvolutionList, pEffectInst->GetEvoValues());
	ScalarV inheritVelocity = g_ptxVecRandom.GetRangedScalarV(vInheritVelocityMinMax.GetX(), vInheritVelocityMinMax.GetY()) * ScalarV(V_FLT_SMALL_2);

	// get the sub frame effect matrix
	Mat34V vEffectMtxSubFrame = pEffectInst->GetMatrix();
	vEffectMtxSubFrame.SetCol3(vSubFrameEffectPos);

	// get a random point in the creation domain's local space
	Vec3V vPosition = m_pEmitterRule->GetCreationDomain()->GetRandomPoint(m_creationDomainInst.GetMtxLcl(), m_creationDomainInst.GetCurrSizeOuter(), m_creationDomainInst.GetCurrSizeInner());

	// transform into the creation domain's world space
	Mat34V vCreationDomainMtx = GetCreationDomainLclToWldMtx(vEffectMtxSubFrame);
	vPosition = Transform(vCreationDomainMtx, vPosition);

	// get a random point in the target domain's local space
	Vec3V vVelocity = m_pEmitterRule->GetTargetDomain()->GetRandomPoint(m_targetDomainInst.GetMtxLcl(), m_targetDomainInst.GetCurrSizeOuter(), m_targetDomainInst.GetCurrSizeInner());

	// transform into the target domain's world space
	Mat34V vTargetDomainMtx = GetTargetDomainLclToWldMtx(vEffectMtxSubFrame);
	vVelocity = Transform(vTargetDomainMtx, vVelocity);

	if (m_pEmitterRule->GetTargetDomain()->GetIsPointRelative())
	{
		// velocity is relative to the position of the effect
		vVelocity -= vSubFrameEffectPos;
	}
	else
	{
		// velocity is relative to the position of the point
		vVelocity -= vPosition;
	}

	vVelocity = AddScaled(vVelocity * speedScalar, pEffectInst->GetVelocityAdd(), ScalarV(inheritVelocity));

	// texture info
	u8 initTexFrameId = 0;
	if (m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_SPRITE)
	{
		initTexFrameId = (u8)g_DrawRand.GetRanged(m_pParticleRule->GetTexFrameIdMin(), m_pParticleRule->GetTexFrameIdMax());
	}
	else if (m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_MODEL)
	{
		initTexFrameId = (u8)g_DrawRand.GetRanged(0, Max(0, m_pParticleRule->GetNumDrawables()-1));
	}
	else if (m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_TRAIL)
	{
		initTexFrameId = 0;
	}	
	else
	{
		ptxAssertf(0, "undefined particle draw type found (%d)", m_pParticleRule->GetDrawType());
	}

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// initialise the point
	pPointSB->Init();
	pPointDB->Init();

	// set up the point
	pPointDB->SetCurrVel(vVelocity);
	pPointDB->SetCurrPos(vPosition);
	pPointDB->SetPrevPos(pPointDB->GetCurrPos());
	pPointDB->SetSpawnPos(pPointDB->GetCurrPos());
	pPointDB->SetLifeRatio(0.0f);										// don't set the sub frame life ratio here - it will get calculated by the age behaviour's InitPoint below
	pPointSB->SetEmitterLifeRatio(GetDataDB()->GetCurrLifeRatio());
	pPointSB->SetPlaybackRate(playbackRate);
	pPointSB->SetOOLife(ooParticleLife.Getf());
	pPointSB->SetRandIndex(ptxRandomTable::GetCurrIndex());
	pPointSB->SetSortVal(g_DrawRand.GetRanged(0.0f, 100.0f));	
	pPointDB->SetInitTexFrameId(initTexFrameId);
	pPointDB->SetTexFrameIdA(pPointDB->GetInitTexFrameId());
	pPointDB->SetTexFrameIdB(pPointDB->GetInitTexFrameId());
	if (m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_SPRITE) 
	{
		pPointSB->SetFlag(PTXPOINT_FLAG_IS_SPRITE); 
	}
	else if (m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_MODEL) 
	{
		pPointSB->SetFlag(PTXPOINT_FLAG_IS_MODEL);
	}
	else if (m_pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_TRAIL) 
	{
		pPointSB->SetFlag(PTXPOINT_FLAG_IS_TRAIL);
	}

	//copying from ptxEvoValueType into ptxEvoValueType (which is the Raw functions)
	// instead of unpacking from u8 to float and packing it back to u8 again
	for (int i=0; i<PTXEVO_MAX_EVOLUTIONS; i++)
	{
		pPointSB->SetEvoValueRaw(i, pEffectInst->GetEvoValueRaw(i));
	}

	// let the particle rule go through the behaviours 
	m_pParticleRule->InitPoint(pEffectInst, this, pPointItem, pEvolutionList, subFrameRatio, dt);

	// copy point data to the update buffer
	pPointItem->CopyToUpdateBuffer();

	// insert the point into the active list
	bool bufferToHead = m_pParticleRule->GetSortType()!=PTXPARTICLERULE_SORTTYPE_OLDEST_FIRST;
	m_pointList.BufferData(RMPTFXTHREADER.GetUpdateBufferId(), pPointItem, bufferToHead);

	// output profile data
#if RMPTFX_TIMERS
	OutputUpdateProfileInfo(pEffectInst, "InitPoints", timer);
#endif
}

void ptxEmitterInst::CopyToUpdateBuffer()
{
	GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId())->m_vCurrPosWldLifeRatio = GetDataDB()->m_vCurrPosWldLifeRatio;
	GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId())->m_vCurrBoundPosRadius = GetDataDB()->m_vCurrBoundPosRadius;
}

#if RMPTFX_BANK
bool ptxEmitterInst::ShouldOutputPlayStateInfo(ptxEffectInst* pEffectInst)
{
	if (pEffectInst && pEffectInst->GetOutputPlayStateDebug() && ptxDebug::sm_outputEmitterInstDrawStateDebug)
	{
		u32 debugHashValue = atHashValue(ptxDebug::sm_emitterInstNameFilter);
		if (debugHashValue==0 || debugHashValue==atHashValue(m_pEmitterRule->GetName()))
		{
			return true;
		}
	}

	return false;
}
#endif

#if RMPTFX_BANK
void ptxEmitterInst::OutputUpdateProfileInfo(ptxEffectInst* pEffectInst, const char* pFunctionName, sysTimer& timer)
{
	if (ptxDebug::sm_outputEffectInstSpawningProfile)
	{
		u32 debugHashValue = atHashValue(ptxDebug::sm_effectInstNameFilter);
		if (debugHashValue==0 || debugHashValue==atHashValue(pEffectInst->GetEffectRule()->GetName()))
		{
			ptxDebugf1("  %s - %30s took %.2fms lr=%.2f, pts=%d", m_pEmitterRule->GetName(), pFunctionName, timer.GetMsTime(), GetDataDB()->GetCurrLifeRatio(), GetNumActivePoints());
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxEmitterInst::OutputPlayStateFunction(ptxEffectInst* pEffectInst, const char* pFunctionName)
{
	if (m_outputPlayStateDebug)
	{
		ptxDebugf1("effect %p (%s), emitter %p (%s) called %s()", this, pEffectInst->GetEffectRule()->GetName(), this, m_pEmitterRule->GetName(), pFunctionName);
	}
}
#endif

#if RMPTFX_BANK
void ptxEmitterInst::SetPlayState(ptxEffectInst* pEffectInst, ptxEmitterInstPlayState val) 
{
	static const char* s_pPlayStateNames[PTXEMITINSTPLAYSTATE_NUM] = 
	{
		"WAITING_TO_START", 
		"PLAYING",
		"STOPPED",
		"FINISHED",
	};

	ptxEmitterInstPlayState origPlayState = m_playState;
	m_playState = val; 

	if (m_pEmitterRule && m_outputPlayStateDebug)
	{
		ptxDebugf1("effect %p (%s), emitter %p (%s) changing from %s to %s", this, pEffectInst->GetEffectRule()->GetName(), this, m_pEmitterRule->GetName(), s_pPlayStateNames[origPlayState], s_pPlayStateNames[m_playState]);
	}
}
#endif

// void ptxEmitterInst::SetAmbientScales(float natAmbScale, float artAmbScale)
// {
// 	m_ambientScale[0] = (u8)(natAmbScale*255.0f);
// 	m_ambientScale[1] = (u8)(artAmbScale*255.0f);
// }

// void ptxEmitterInst::GetAmbientScales(float& natAmbScale, float& artAmbScale) const
// {
// 	natAmbScale = (float)m_ambientScale[0]/255.0f;
// 	artAmbScale = (float)m_ambientScale[1]/255.0f;
// }
