// 
// rmptfx/ptxu_wind.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "rmptfx/ptxu_wind.h"
#include "rmptfx/ptxu_wind_parser.h"

#include "pheffects/wind.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Wind::sm_influenceDefn(					"Wind Influence",			atHashValue("ptxu_Wind:m_influenceKFP"),					PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		10.0f,		0.01f),		"Min Influence",	"Max Influence");


// code
#if RMPTFX_XML_LOADING
ptxu_Wind::ptxu_Wind()
{
	// create keyframes
	m_influenceKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Wind:m_influenceKFP"));

	m_keyframePropList.AddKeyframeProp(&m_influenceKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();

	// init global data
	m_pGlobalData_NOTUSED = NULL;
	m_pWindEval_NOTUSED = NULL;
}
#endif

void ptxu_Wind::SetDefaultData()
{
	// default keyframes
	m_influenceKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_highLodRange = 0.0f;
	m_lowLodRange = 0.0f;

	m_highLodDisturbanceMode = PTXU_WIND_DISTURBANCE_NONE;
	m_lowLodDisturbanceMode = PTXU_WIND_DISTURBANCE_NONE;

	m_ignoreMtxWeight = false;
}

ptxu_Wind::ptxu_Wind(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_influenceKFP(rsc)
{
	m_pGlobalData_NOTUSED = NULL;
	m_pWindEval_NOTUSED = NULL;

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Wind_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Wind::DeclareStruct(datTypeStruct& s)
{
	m_pGlobalData_NOTUSED = NULL; 
	m_pWindEval_NOTUSED = NULL;

	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Wind, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Wind, m_influenceKFP)
		SSTRUCT_IGNORE(ptxu_Wind, m_pGlobalData_NOTUSED)
		SSTRUCT_IGNORE(ptxu_Wind, m_pWindEval_NOTUSED)
		SSTRUCT_FIELD(ptxu_Wind, m_highLodRange)
		SSTRUCT_FIELD(ptxu_Wind, m_lowLodRange)
		SSTRUCT_FIELD(ptxu_Wind, m_highLodDisturbanceMode)
		SSTRUCT_FIELD(ptxu_Wind, m_lowLodDisturbanceMode)
		SSTRUCT_FIELD(ptxu_Wind, m_ignoreMtxWeight)
		SSTRUCT_IGNORE(ptxu_Wind, m_pad)
	SSTRUCT_END(ptxu_Wind)
}
#endif

IMPLEMENT_PLACE(ptxu_Wind);

void ptxu_Wind::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointWind)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Wind::UpdatePoint(ptxBehaviourParams& params)
{
	// return early if we're outside the low lod range
	float distSqrToCamera = params.distSqrToCamera;
	bool isInHighLodRange = distSqrToCamera<=m_highLodRange*m_highLodRange;
	bool isInLowLodRange = distSqrToCamera<=m_lowLodRange*m_lowLodRange;
	if (isInLowLodRange==false)
	{
		return;
	}

	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	
	ScalarV	vDeltaTime = params.vDeltaTime;
	Vec3V vEffectWindVelocity = params.vEffectWindVelocity;

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// get the particle life ratio key time
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vWindInfluenceMinMax = m_influenceKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());

	// calculate the bias value
	ScalarV vBias = GetBiasValue(m_influenceKFP, pPointSB->GetRandIndex());

	// calculate the lerped wind influence between the min and max keyframe data
	ScalarV vWindInfluence = Lerp(vBias, SplatX(vWindInfluenceMinMax), SplatY(vWindInfluenceMinMax));

	// scale by the matrix weight of the particle (particles with full matrix weight don't want any wind to affect them)
	if (!m_ignoreMtxWeight)
	{
		vWindInfluence *= (ScalarV(V_ONE) - pPointDB->GetMatrixWeightV());
	}

	// get the air velocity at the particle position
	Vec3V vAirVel = Vec3V(V_ZERO);

	if (isInHighLodRange)
	{
		bool includeWindFieldDisturbance = false;
		bool includeGlobalDisturbances = false;
		if (m_highLodDisturbanceMode==PTXU_WIND_DISTURBANCE_NONE)
		{
			includeWindFieldDisturbance = false;
			includeGlobalDisturbances = false;
		}
		else if (m_highLodDisturbanceMode==PTXU_WIND_DISTURBANCE_FIELD)
		{
			includeWindFieldDisturbance = true;
			includeGlobalDisturbances = false;
		}
		else if (m_highLodDisturbanceMode==PTXU_WIND_DISTURBANCE_FIELD_AND_GLOBAL)
		{
			includeWindFieldDisturbance = true;
			includeGlobalDisturbances = true;
		}

		WIND.GetLocalVelocity(pPointDB->GetCurrPos(), vAirVel, includeWindFieldDisturbance, includeGlobalDisturbances);

		ptxAssertf(IsFiniteAll(vAirVel), "vAirVel is not finite (1)");
	}
	else 
	{
		vAirVel = vEffectWindVelocity;
		ptxAssertf(IsFiniteAll(vAirVel), "vAirVel is not finite (2)");
	}

	// calculate the difference between the particle and air velocity
	Vec3V vDeltaVel = vAirVel - pPointDB->GetCurrVel();
	ptxAssertf(IsFiniteAll(pPointDB->GetCurrVel()), "pPointDB->GetCurrVel() is not finite");
	ptxAssertf(IsFiniteAll(vDeltaVel), "vDeltaVel is not finite (1)");

	// determine how quickly the particle velocity should match the wind velocity
	// if running at 30fps when a wind influence of 30 would match it instantly i.e. (delta mult = 1.0f)
	// a wind influence of 1 would mean it would take 1 second to match the wind velocity (delta mult = 1/30)
	ScalarV vDeltaMult = vWindInfluence * vDeltaTime;
	ptxAssertf(IsFiniteAll(vDeltaTime), "vDeltaTime is not finite");
	ptxAssertf(IsFiniteAll(vDeltaMult), "vDeltaMult is not finite (1)");
	vDeltaMult = SelectFT(IsGreaterThan(vDeltaMult, ScalarV(V_ONE)), vDeltaMult, ScalarV(V_ONE));
	ptxAssertf(IsFiniteAll(vDeltaMult), "vDeltaMult is not finite (2)");

	// apply the scale
	vDeltaVel *= vDeltaMult;
	ptxAssertf(IsFiniteAll(vDeltaVel), "vDeltaVel is not finite (2)");

	// update the particle's velocity
	if (IsFiniteAll(vDeltaVel))
	{
		if (!pPointSB->GetFlag(PTXPOINT_FLAG_AT_REST))
		{
			pPointDB->IncCurrVel(vDeltaVel);
		}
	}
}

void ptxu_Wind::UpdateEmitterInst(ptxBehaviourParams& params)
{
	ptxEffectInst* pEffectInst = params.pEffectInst;

	Vec3V pos = pEffectInst->GetCurrPos();
	Vec3V outVelocity;
	bool includeWindFieldDisturbance = false;
	bool includeGlobalDisturbances = false;

	// query the wind at the effect instance position if this effect is currently using low lod wind
	if (params.distSqrToCamera>m_highLodRange*m_highLodRange && 
		params.distSqrToCamera<=m_lowLodRange*m_lowLodRange)
	{
		if (m_lowLodDisturbanceMode==PTXU_WIND_DISTURBANCE_NONE)
		{
			includeWindFieldDisturbance = false;
			includeGlobalDisturbances = false;
		}
		else if (m_lowLodDisturbanceMode==PTXU_WIND_DISTURBANCE_FIELD)
		{
			includeWindFieldDisturbance = true;
			includeGlobalDisturbances = false;

		}
		else if (m_lowLodDisturbanceMode==PTXU_WIND_DISTURBANCE_FIELD_AND_GLOBAL)
		{
			includeWindFieldDisturbance = true;
			includeGlobalDisturbances = true;
		}

		WIND.GetLocalVelocity(pos, params.vEffectWindVelocity, includeWindFieldDisturbance, includeGlobalDisturbances);

		ptxAssertf(IsFiniteAll(params.vEffectWindVelocity), "vEffectWindVelocity is not finite");
	}
	else
	{
		params.vEffectWindVelocity.ZeroComponents();
	}
}

void ptxu_Wind::SetKeyframeDefns()
{
	m_influenceKFP.GetKeyframe().SetDefn(&sm_influenceDefn);
}

void ptxu_Wind::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_influenceKFP, &sm_influenceDefn);
}

// editor code
#if RMPTFX_EDITOR
bool ptxu_Wind::IsActive()
{
	ptxu_Wind* pWind = static_cast<ptxu_Wind*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_influenceKFP == pWind->m_influenceKFP && 
				   m_highLodRange == pWind->m_highLodRange &&
				   m_lowLodRange == pWind->m_lowLodRange &&
				   m_highLodDisturbanceMode == pWind->m_highLodDisturbanceMode &&
				   m_lowLodDisturbanceMode == pWind->m_lowLodDisturbanceMode &&
				   m_ignoreMtxWeight == pWind->m_ignoreMtxWeight;

	return !isEqual;
}

void ptxu_Wind::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 6);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Wind Influence");

	// send non-keyframes
	SendFloatDefnToEditor(buff, "High LOD Range");
	SendFloatDefnToEditor(buff, "Low LOD Range");

	const char* pComboItemNames[PTXU_WIND_DISTURBANCE_NUM] = {"None", "Field", "Field and Global"};
	SendComboDefnToEditor(buff, "High LOD Disturbance Mode", PTXU_WIND_DISTURBANCE_NUM, pComboItemNames);
	SendComboDefnToEditor(buff, "Low LOD Disturbance Mode", PTXU_WIND_DISTURBANCE_NUM, pComboItemNames);

	SendBoolDefnToEditor(buff, "Ignore Matrix Weight");
}

void ptxu_Wind::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_influenceKFP, pParticleRule);

	// send non-keyframes
	SendFloatToEditor(buff, m_highLodRange);
	SendFloatToEditor(buff, m_lowLodRange);

	SendIntToEditor(buff, m_highLodDisturbanceMode);
	SendIntToEditor(buff, m_lowLodDisturbanceMode);

	SendBoolToEditor(buff, m_ignoreMtxWeight);
}

void ptxu_Wind::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_influenceKFP);

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_highLodRange);
	ReceiveFloatFromEditor(buff, m_lowLodRange);

	ReceiveIntFromEditor(buff, m_highLodDisturbanceMode);
	ReceiveIntFromEditor(buff, m_lowLodDisturbanceMode);

	ReceiveBoolFromEditor(buff, m_ignoreMtxWeight);
}
#endif // RMPTFX_EDITOR



} // namespace rage
