<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxTimeLine" cond="RMPTFX_XML_LOADING">
	<array name="m_events" type="atArray">
		<pointer type="rage::ptxEvent" policy="owner"/>
	</array>
</structdef>

</ParserSchema>