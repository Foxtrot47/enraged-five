// 
// rmptfx/ptxmisc.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxmisc.h"

// includes (rmptfx)
#include "rmptfx/ptxconfig.h"


// optimsations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


} // namespace rage
