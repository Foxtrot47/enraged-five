// 
// rmptfx/ptxd_drawmodel.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXD_DRAWMODEL_H 
#define PTXD_DRAWMODEL_H 


// includes
#include "grcore/effect_typedefs.h"
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"
#include "rmptfx/ptxdraw.h"


// namespaces
namespace rage
{


// classes
class ptxd_Model : public ptxBehaviour
{
	friend class ptxParticleRule;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxd_Model();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxd_Model(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxd_Model);

		// info
		const char* GetName() {return "ptxd_Model";}											// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_DRAWMODEL;}									// the ordering of the behaviour (see ptxconfig.h)
		ptxDrawType GetTypeFilter() {return PTXPARTICLERULE_DRAWTYPE_MODEL;}					// filter for deciding which type of particle this behaviour should be active
																								
		bool NeedsToInit() {return false;}														// whether we call init when a particle is created
		bool NeedsToUpdate() {return false;}													// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}											// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return true;}														// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}													// whether we call release when a particle dies

		void DrawPoints(ptxEffectInst* RESTRICT pEffectInst,									// called for each active particle to draw them
						ptxEmitterInst* RESTRICT pEmitterInst, 
						ptxParticleRule* RESTRICT pParticleRule, 
						ptxEvolutionList* RESTRICT pEvoList
						PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface));

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Draw (Model)";}									// name of the behaviour shown in the editor
		bool IsActive() {return true;}															// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);												// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);		// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);								// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif

	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	private: //////////////////////////

		grcEffectGlobalVar m_colourControlShaderId;


	public: ///////////////////////////
		
		// keyframe data

		// non keyframe data
		float m_cameraShrink;
		float m_shadowCastIntensity;
		bool m_disableDraw;
		
		datPadding<3> m_pad;

};


} // namespace rage


#endif // PTXD_DRAWMODEL_H 
