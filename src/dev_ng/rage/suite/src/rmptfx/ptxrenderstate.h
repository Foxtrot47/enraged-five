// 
// rmptfx/ptxrenderstate.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXRENDERSTATE_H
#define RMPTFX_PTXRENDERSTATE_H


// includes
#include "grcore/effect_values.h"
#include "grcore/stateblock.h"
#include "parser/macros.h"
#include "rmptfx/ptxconfig.h"
#include "data/safestruct.h"


// namespaces
namespace rage 
{


// forward decalarations
class ptxByteBuff;


// externally declared
extern u32 TranslateCull[];


// enumerations
enum ptxProjectionMode
{
	PTXPROJMODE_NONE						= 0,
	PTXPROJMODE_WATER,
	PTXPROJMODE_NON_WATER,
	PTXPROJMODE_ALL,

	PTXPROJMODE_NUM
};

enum ptxCullMode {
	grccmNone,		// No culling
	grccmFront,		// Cull front-facing polygons
	grccmBack,		// Cull back-facing polygons
	grccCount
};

enum ptxBlendSet {
	grcbsNormal,					// Standard dest*(1-srcAlpha) + src*srcAlpha blend
	grcbsAdd,						// dest + src
	grcbsSubtract,					// src - dest (not that useful, you probably want grcbsReverseSubtract instead)
	grcbsLightmap,					// dest*srcAlpha + src*(1-srcAlpha), same as grcbsNormal but with inverted alpha
	grcbsMatte,						// dest + src*destAlpha, for "additive" HUD cutout
	grcbsOverwrite,					// src only (same as disabling alpha blending)
	grcbsDest,						// dest*(1-destAlpha) + src*destAlpha, for "blend" HUD cutout
	grcbsAlphaAdd,					// dest + src*srcAlpha (not that useful, might as well use grcbsAdd and do the multiply in the shader)
	grcbsReverseSubtract,			// dest - src
	grcbsMin,						// Min(src,dest)
	grcbsMax,						// Max(src,dest)
	grcbsAlphaSubtract, 			// dest - src*srcAlpha (not that useful, might as well use grcbsReverseSubtract and do the multiply in the shader)
	grcbsMultiplySrcDest,			// multiply dest*src
	grcbsCompositeAlpha,			// dest*(1-srcAlpha) + src
	grcbsCompositeAlphaSubtract,	// dest*(1-srcAlpha) - src
	grcbsCount
};

// classes
class ptxRenderState
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxRenderState();
		
		// resourcing
		DECLARE_PLACE(ptxRenderState);
		ptxRenderState(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif

		static void InitClass();

		// render states
		inline void SetRenderState(ptxProjectionMode projMode);

		// accessors
		int GetBlendSet() const {return m_blendSet;}

		// editor
#if RMPTFX_EDITOR
		void SendToEditor(ptxByteBuff& buff);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_SIMPLE_PARSABLE
#endif

	private: //////////////////////////

		static void InitRenderStateBlocks();
		static grcBlendStateHandle SelectBlendStateBlock(int blendMode);
		static grcRasterizerStateHandle SelectRasterizerStateBlock(int cullMode);
		static grcDepthStencilStateHandle SelectDepthStencilStateBlock(bool bWriteDepth, bool bTestDepth, ptxProjectionMode projMode);

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		int m_cullMode;
		int m_blendSet;
		int m_lightingMode;		// can be removed
		bool m_depthWrite;
		bool m_depthTest;
		bool m_alphaBlend;					// can be removed
		datPadding<5> m_pad;	

		// common state blocks
		static grcRasterizerStateHandle		ms_rasterizerStateCullModes[3];								// 0: grccmNone, 1: grccmFront, 2: grccmBack
		static grcDepthStencilStateHandle	ms_depthStateWriteDepthTestDepth[2][2][PTXPROJMODE_NUM];	// [0,0]: write off, test off; [0, 1]: write off, test on; etc. [n,n, no_stencil/stencil_water/stencil_terrain]
		static grcBlendStateHandle			ms_blendNormal;
		static grcBlendStateHandle			ms_blendAdd;
		static grcBlendStateHandle			ms_blendSubtract;
		static grcBlendStateHandle			ms_blendCompositeAlpha;
		static grcBlendStateHandle			ms_blendCompositeAlphaSubtract;

};


// inline functions
inline void ptxRenderState::SetRenderState(ptxProjectionMode projMode)
{
	grcRasterizerStateHandle rasterizerState = SelectRasterizerStateBlock(m_cullMode);
	grcBlendStateHandle blendState = SelectBlendStateBlock(m_blendSet);
	grcDepthStencilStateHandle depthStencilState = SelectDepthStencilStateBlock(m_depthWrite, m_depthTest, projMode);

	grcStateBlock::SetRasterizerState(rasterizerState);
	grcStateBlock::SetBlendState(blendState);
	grcStateBlock::SetDepthStencilState(depthStencilState);
}



} //namespace rage

#endif //RMPTFX_PTXRENDERSTATE_H

