// 
// rmptfx/ptxfxlist.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "rmptfx/ptxfxlist.h"
#include "rmptfx/ptxdebug.h"

#include "diag/output.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "parser/visitorutils.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()

#if	MESH_LIBRARY
	#define MESH_LIBRARY_ONLY(_x)	_x
#else // MESH_LIBRARY
	#define MESH_LIBRARY_ONLY(_x)
#endif // MESH_LIBRARY

size_t totalRuleSize = 0;
size_t totalStringSize = 0;

// namespaces
namespace rage
{


// forward declarations
#if RMPTFX_EDITOR
void CreateUiObjects(ptxFxList& list, ptxFxList::PtrToFilenameMap* nameMap);
#endif

#if RMPTFX_XML_LOADING
void RepairAndUpgradeData(ptxFxList& list, ptxFxList::PtrToFilenameMap* nameMap);
void ResolveReferences(ptxFxList& list, ptxFxList::PtrToFilenameMap* nameMap);
#endif

ptxTokenIterator::ptxTokenIterator(const char* pFilename, const char* pExtn)
: m_pStream(NULL)
, m_pToken(NULL)
{
	// use temp memory
	sysMemAutoUseTempMemory useTempMem;

	// open the asset and create the tokeniser
	m_pStream = ASSET.Open(pFilename, pExtn);
	if (m_pStream)
	{
		m_pToken = rage_new fiTokenizer(pFilename, m_pStream);
	}
}

ptxTokenIterator::~ptxTokenIterator()
{
	// use temp memory
	sysMemAutoUseTempMemory useTempMem;

	// close the stream
	if (m_pStream)
	{
		// delete the tokeniser
		if (m_pToken)
		{
			delete m_pToken;
		}

		m_pStream->Close();
	}
}

bool ptxTokenIterator::GetNext() 
{
	// gets the next valid line and stores it in the line buffer
	bool finished = (m_pToken==NULL);
	bool hasToken = false;

	while (!finished) 
	{
		// get the next line
		hasToken = (m_pToken->GetToken(m_lineBuffer, 256) != 0);
		if (m_lineBuffer[0]=='#')
		{
			// it's commented out - skip and try again
			m_pToken->SkipToEndOfLine();
			finished = false;
		}
		else
		{
			// the line is valid - we're done
			finished = true;
		}
	}

	return hasToken;
}

ptxFxList::ptxFxList()
	: m_refCount(0)
	, m_textureDictPusher(NULL)
	, m_textureDictPopper(NULL)
{
}

ptxFxList::~ptxFxList()
{
	ptxDebugf1("Deleting fxlist %s at 0x%p", m_name.c_str(), this);

	ptxAssertf(m_refCount == 0, "Deleting an fxlist with %d remaining references", m_refCount);

	if (m_pEffectRuleDict)
	{
		(void)ptxVerifyf(m_pEffectRuleDict->Release()==0, "effect rule dictionary not returning zero references");
	}

	if (m_pEmitterRuleDict)
	{
		(void)ptxVerifyf(m_pEmitterRuleDict->Release()==0, "emit rule dictionary not returning zero references");
	}

	if (m_pParticleRuleDict)
	{
		(void)ptxVerifyf(m_pParticleRuleDict->Release()==0, "ptx rule dictionary not returning zero references");
	}

	if (m_pModelDict)
	{
		(void)ptxVerifyf(m_pModelDict->Release()==0, "model dictionary not returning zero references");
	}

	if (m_pTextureDict)
	{
		(void)ptxVerifyf(m_pTextureDict->Release()==0, "texture dictionary not returning zero references");
	}
}

IMPLEMENT_PLACE(ptxFxList);

ptxFxList::ptxFxList(datResource& rsc)
	: m_name(rsc)
	, m_pTextureDict(rsc)
	, m_textureDictPusher(m_pTextureDict)
	, m_pModelDict(SPU_INIT)					// use this to suppress normal initialization
	, m_pParticleRuleDict(rsc)
	, m_textureDictPopper(m_pTextureDict)
	, m_pEffectRuleDict(rsc)
	, m_pEmitterRuleDict(rsc)
{
	if (!datResource_IsDefragmentation)
	{
		// Take care of m_pModelDict placement in PostLoad()
		ptxDebugf1("Placing fxlist %s at 0x%p", m_name.c_str(), this);
	}
	else
	{
		datResource::Place(m_pModelDict.ptr);
		ptxDebugf1("Defragmenting fxlist %s, now at 0x%p", m_name.c_str(), this);
	}

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxFxList_num++;
#endif
}

ptxFxList* ptxFxList::CreateEmptyFxlist()
{
	ptxFxList* fxlist = rage_new ptxFxList();
	fxlist->m_pEffectRuleDict = rage_new pgDictionary<ptxEffectRule>(0);
	fxlist->m_pEmitterRuleDict = rage_new pgDictionary<ptxEmitterRule>(0);
	fxlist->m_pParticleRuleDict = rage_new pgDictionary<ptxParticleRule>(0);
	fxlist->m_pTextureDict = rage_new pgDictionary<grcTexture>(0);
	fxlist->m_pModelDict = rage_new pgDictionary<rmcDrawable>(0);

	return fxlist;
}

#if __DECLARESTRUCT
void ptxFxList::DeclareStruct(datTypeStruct& s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxFxList, pgBase)
		SSTRUCT_FIELD(ptxFxList, m_name)
		SSTRUCT_FIELD(ptxFxList, m_refCount)
		SSTRUCT_FIELD(ptxFxList, m_pTextureDict)
		SSTRUCT_FIELD(ptxFxList, m_textureDictPusher)
		SSTRUCT_FIELD(ptxFxList, m_pModelDict)
		SSTRUCT_FIELD(ptxFxList, m_pParticleRuleDict)
		SSTRUCT_FIELD(ptxFxList, m_textureDictPopper)
		SSTRUCT_FIELD(ptxFxList, m_pEffectRuleDict)
		SSTRUCT_FIELD(ptxFxList, m_pEmitterRuleDict)
	SSTRUCT_END(ptxFxList)
}
#endif

struct ptxFxListHelperData
{
	ptxFxListHelperData(ptxFxList* list, ptxFxList::PtrToFilenameMap* nameMap)
		: m_FxList(list)
		, m_NameMap(nameMap)
	{
	}

	ptxFxList* m_FxList;
	ptxFxList::PtrToFilenameMap* m_NameMap;
};

class ptxFxListHelpers
{
public:
#if RMPTFX_XML_LOADING
	template<typename _Type> 
	static bool RepairAndUpgradeFunc(_Type& rule, u32, void* data)
	{					
		ptxFxListHelperData& helperData = *reinterpret_cast<ptxFxListHelperData*>(data);
		ptxAssertf(helperData.m_NameMap, "Name map is required for RepairAndUpgrade");
		ptxFxList::PtrToFilenameMap& map = *helperData.m_NameMap;
		DIAG_CONTEXT_MESSAGE("RepairAndUpgrade for %s", map[&rule].c_str());
		rule.RepairAndUpgradeData(map[&rule].c_str());
		return true;
	}

	template<typename _Type> 
	static bool ResolveReferencesFunc(_Type& rule, u32, void* ASSERT_ONLY(data))
	{
		ASSERT_ONLY(ptxFxListHelperData& helperData = *reinterpret_cast<ptxFxListHelperData*>(data);)
		ptxAssertf(helperData.m_NameMap, "Name map is required for RepairAndUpgrade");
		ASSERT_ONLY(DIAG_CONTEXT_MESSAGE("ResolveReferences for %s", (*helperData.m_NameMap)[&rule].c_str()));
		rule.ResolveReferences();
		return true;
	}

	static bool ResolveReferencesForEffectRuleFunc(ptxEffectRule& rule, u32, void* data)
	{
		ptxFxListHelperData& helperData = *reinterpret_cast<ptxFxListHelperData*>(data);
		ptxAssertf(helperData.m_NameMap, "Name map is required for RepairAndUpgrade");
		DIAG_CONTEXT_MESSAGE("ResolveReferences for %s", (*helperData.m_NameMap)[&rule].c_str());
		rule.ResolveReferences(helperData.m_FxList);
		return true;
	}
#endif

	template<typename _Type>
	static bool PostLoad(_Type& rule, u32, void* /* data */	)
	{
		rule.PostLoad();
		return true;
	}

#if RMPTFX_EDITOR
	template<typename _Type> 
	static bool CreateUiObjectsFunc(_Type& rule, u32, void* data)
	{
		const char* filename = "(resource?)";
		ptxFxListHelperData& helperData = *reinterpret_cast<ptxFxListHelperData*>(data);
		if (helperData.m_NameMap)
		{
			ptxFxList::PtrToFilenameMap& map = *helperData.m_NameMap;
			filename = map[&rule].c_str();
		}
		DIAG_CONTEXT_MESSAGE("CreateUiObjects for %s", filename);
		rule.CreateUiObjects();
		return true;
	}
#endif
};

#if RMPTFX_EDITOR
void CreateUiObjects(ptxFxList& list, ptxFxList::PtrToFilenameMap* nameMap)
{
	ptxFxListHelperData data(&list, nameMap);
	list.GetEffectRuleDictionary()->ForAll(&ptxFxListHelpers::CreateUiObjectsFunc<ptxEffectRule>, &data);
	list.GetEmitterRuleDictionary()->ForAll(&ptxFxListHelpers::CreateUiObjectsFunc<ptxEmitterRule>, &data);
	list.GetParticleRuleDictionary()->ForAll(&ptxFxListHelpers::CreateUiObjectsFunc<ptxParticleRule>, &data);
}
#endif

#if RMPTFX_XML_LOADING
void RepairAndUpgradeData(ptxFxList& list, ptxFxList::PtrToFilenameMap* nameMap)
{
	ptxFxListHelperData data(&list, nameMap);
	list.GetEffectRuleDictionary()->ForAll(&ptxFxListHelpers::RepairAndUpgradeFunc<ptxEffectRule>, &data); 
	list.GetEmitterRuleDictionary()->ForAll(&ptxFxListHelpers::RepairAndUpgradeFunc<ptxEmitterRule>, &data);
	list.GetParticleRuleDictionary()->ForAll(&ptxFxListHelpers::RepairAndUpgradeFunc<ptxParticleRule>, &data);
}
#endif

#if RMPTFX_XML_LOADING
void ResolveReferences(ptxFxList& list, ptxFxList::PtrToFilenameMap* nameMap)
{
	ptxFxListHelperData data(&list, nameMap);
	// Note the first one calls a different function here - so we can pass the fxlist through
	list.GetEffectRuleDictionary()->ForAll(&ptxFxListHelpers::ResolveReferencesForEffectRuleFunc, &data);
	list.GetEmitterRuleDictionary()->ForAll(&ptxFxListHelpers::ResolveReferencesFunc<ptxEmitterRule>, &data);
	list.GetParticleRuleDictionary()->ForAll(&ptxFxListHelpers::ResolveReferencesFunc<ptxParticleRule>, &data);
}
#endif

#if RMPTFX_XML_LOADING
void ptxFxList::Load(const char* pBaseName, const char* MESH_LIBRARY_ONLY(pModelPath), const char* pTexturePath)
{
	// search for the final '/' in the base name
	int baseNameLength = istrlen(pBaseName);
	int slashPos = 0;
	for (int i=baseNameLength-1; i>=0; i--)
	{
		if (pBaseName[i] == '/')
		{
			slashPos = i;
			break;
		}
	}

	// copy over to actual name
	int currCharPos = 0;
	char actualName[32];
	for (int i=slashPos+1; i<baseNameLength; i++)
	{
		actualName[currCharPos++] = pBaseName[i];
	}

	// end the actual name
	actualName[currCharPos] = '\0';

	// set the actual name
	m_name = actualName;

	sysMemStartTemp();
	PtrToFilenameMap* fileNameMap = rage_new PtrToFilenameMap;
	sysMemEndTemp();

	DEV_ONLY(int baseMemory = static_cast<int>(sysMemAllocator::GetCurrent().GetMemoryUsed());)

	totalRuleSize = 0;
	totalStringSize = 0;

	// load the textures
	LoadTextures(pBaseName, pTexturePath);
	DEV_ONLY(int textureMemory = static_cast<int>(sysMemAllocator::GetCurrent().GetMemoryUsed());)

	// load the models
#if MESH_LIBRARY
	LoadModels(pBaseName, pModelPath);
	DEV_ONLY(int modelMemory = static_cast<int>(sysMemAllocator::GetCurrent().GetMemoryUsed());)
#endif

	// load the particle rules
	LoadParticleRules(pBaseName, *fileNameMap);
	DEV_ONLY(int ptxMemory = static_cast<int>(sysMemAllocator::GetCurrent().GetMemoryUsed());)

	// load the emitter rules
	LoadEmitterRules(pBaseName, *fileNameMap);
	DEV_ONLY(int emitMemory = static_cast<int>(sysMemAllocator::GetCurrent().GetMemoryUsed());)

	// load the effect rules
	LoadEffectRules(pBaseName, *fileNameMap);
	DEV_ONLY(int effectMemory = static_cast<int>(sysMemAllocator::GetCurrent().GetMemoryUsed());)

	// Tell the manager about this list - make it available for queries (temporarily)
	sysMemStartTemp();
	RMPTFXMGR.PostLoadFxList(this, "under_construction", FXLISTFLAG_UNDERCONSTRUCTION);
	sysMemEndTemp();

	RepairAndUpgradeData(*this, fileNameMap);
	ResolveReferences(*this, fileNameMap);

	sysMemStartTemp();
	RMPTFXMGR.PreUnloadFxList(this);
	delete fileNameMap;
	sysMemEndTemp();

	ptxDisplayf("Total rules: %d bytes", (s32)totalRuleSize);
	ptxDisplayf("Total strings: %d bytes", (s32)totalStringSize);

#if __DEV
	// output debug info
	ptxDisplayf("ptxFxList %s loaded - sizes are:\n", pBaseName);
	ptxDisplayf("textures:       %d bytes\n", textureMemory-baseMemory);
	ptxDisplayf("models:         %d bytes\n", modelMemory-textureMemory);
	ptxDisplayf("particle rules: %d bytes\n", ptxMemory-modelMemory);
	ptxDisplayf("emitter rules:  %d bytes\n", emitMemory-ptxMemory);
	ptxDisplayf("effect rules:   %d bytes\n", effectMemory-emitMemory);

#endif
}
#endif

#if RMPTFX_XML_LOADING
int GetNumEntries(const char* pFilename, const char* pExtn)
{
	// return the number of entries in this list file
	ptxTokenIterator tokenIterator(pFilename, pExtn);

	int count = 0;
	while (tokenIterator.GetNext())
	{
		count++;
	}

	return count;
}
#endif

class parFindStringSizeVisitor : public parInstanceVisitor
{
public:
	parFindStringSizeVisitor();

	void StringMember(parPtrToMember, const char*, parMemberString&);
	void WideStringMember(parPtrToMember, const char16*, parMemberString&);

	size_t m_Size;
	size_t m_NumAllocs;
};

parFindStringSizeVisitor::parFindStringSizeVisitor()
: m_Size(0)
, m_NumAllocs(0)
{
	parInstanceVisitor::TypeMask typeMask(false);
	typeMask.Set(parMemberType::TYPE_STRING);
	SetMemberTypeMask(typeMask);
}

void parFindStringSizeVisitor::StringMember(parPtrToMember /*ptrToMember*/, const char* stringData, parMemberString& metadata)
{
	if (metadata.HasExternalStorage() && stringData)
	{
		m_Size += strlen(stringData) + 1;
		m_NumAllocs++;
	}
}

void parFindStringSizeVisitor::WideStringMember(parPtrToMember /*ptrToMember*/, const char16* stringData, parMemberString& metadata)
{
	if (metadata.HasExternalStorage() && stringData)
	{
		m_Size += sizeof(char16) * (wcslen(stringData) + 1);
		m_NumAllocs++;
	}
}

#if RMPTFX_XML_LOADING
template<typename _RuleType>
void LoadRulesFromFile(const char* filename, const char* listextn, const char* ruleextn, const char* foldername, datOwner<pgDictionary<_RuleType> >& dict, ptxFxList::PtrToFilenameMap& nameMap)
{
	// create a new dictionary of the required type
	dict = rage_new pgDictionary<_RuleType>(GetNumEntries(filename, listextn));

	// go through the list file and load in the required resources
	ptxTokenIterator tokenIterator(filename, listextn);

	ASSET.PushFolder(RMPTFXMGR.GetAssetPath());
	ASSET.PushFolder(foldername);

	while (tokenIterator.GetNext())
	{
		_RuleType* rule = NULL;
		bool success = PARSER.LoadObjectPtr(tokenIterator.GetLineBuffer(),ruleextn,rule);
		if (success && rule)
		{
			// add the resource to the dictionary and add a reference
			dict->AddEntry(tokenIterator.GetLineBuffer(), rule);
			rule->AddRef();

			size_t size = parFindTotalSize(*rule);
			totalRuleSize += size;

			parFindStringSizeVisitor vis;
			vis.Visit(*rule);
			size_t stringSize = vis.m_Size;
			totalStringSize += stringSize;

			sysMemStartTemp();
			ptxDisplayf("Size of %s.%s: %" SIZETFMT "d", tokenIterator.GetLineBuffer(), ruleextn, size);

			nameMap.Insert(rule, ConstString(tokenIterator.GetLineBuffer()));
			sysMemEndTemp();
		}
		else
		{
			ptxErrorf("couldn't find rule %s in %s.%s", tokenIterator.GetLineBuffer(), filename, listextn);
		}
	}

	ASSET.PopFolder();
	ASSET.PopFolder();
}
#endif

#if RMPTFX_XML_LOADING
void ptxFxList::LoadEffectRules(const char* pEffectRuleListName, PtrToFilenameMap& map)
{
	LoadRulesFromFile(pEffectRuleListName, "effectlist", "effectrule", "effectrules", m_pEffectRuleDict, map);
}
#endif

#if RMPTFX_XML_LOADING
void ptxFxList::LoadEmitterRules(const char* pEmitterRuleListName, PtrToFilenameMap& map)
{
	LoadRulesFromFile(pEmitterRuleListName, "emitlist", "emitrule", "emitrules", m_pEmitterRuleDict, map);
}
#endif

#if RMPTFX_XML_LOADING
void ptxFxList::LoadParticleRules(const char* pParticleRuleListName, PtrToFilenameMap& map)
{
	LoadRulesFromFile(pParticleRuleListName, "ptxlist", "ptxrule", "ptxrules", m_pParticleRuleDict, map);
}
#endif

#if RMPTFX_XML_LOADING
#if MESH_LIBRARY
void ptxFxList::LoadModels(const char* pModelListName, const char* pModelPath)
{
	// create a new drawable dictionary
	m_pModelDict = rage_new pgDictionary<rmcDrawable>(GetNumEntries(pModelListName, "modellist"));

	// open the model list for reading
	ptxTokenIterator tokenIterator(pModelListName, "modellist");

	// store the current asset path (so it can be restored later)
	char origPath[RAGE_MAX_PATH];
	strcpy(origPath, ASSET.GetPath());

	// set the required asset path
	ASSET.SetPath(pModelPath);

	// go through the entries in the list file
	while (tokenIterator.GetNext())
	{
		// get the path of the current model to load
		char currModelPath[RAGE_MAX_PATH];
		strcpy(currModelPath, tokenIterator.GetLineBuffer());

		// remove the filename from the path so we're left with the folder
		int stringIndex = 0;
		for (int i=(int)strlen(currModelPath)-1; i>=0; i--)
		{
			if (currModelPath[i]=='/' || currModelPath[i]=='\\')
			{
				currModelPath[i] = '\0';
				stringIndex = i;
				break;
			}
		}

		// get the name of the model to load
		char modelName[RAGE_MAX_PATH];
		strcpy(modelName, &(tokenIterator.GetLineBuffer())[stringIndex+1]);

		// load in the model
		ASSET.PushFolder(currModelPath);
		rmcDrawable* pDrawable = rage_new rmcDrawable();
		if (pDrawable->Load(modelName))
		{
			// add to the model dictionary
			m_pModelDict->AddEntry(tokenIterator.GetLineBuffer(), pDrawable);
		}
		ASSET.PopFolder();
	}

	// restore the original asset path
	ASSET.SetPath(origPath);
}
#endif
#endif

#if RMPTFX_XML_LOADING
void ptxFxList::LoadTextures(const char* pTextureListName, const char* pTexturePath)
{
	// get the name of the texture list file
	char textureListName[RAGE_MAX_PATH];
	formatf(textureListName, RAGE_MAX_PATH, "%s.texlist", pTextureListName);

	// store the current asset path (so it can be restored later)
	char origPath[RAGE_MAX_PATH];
	strcpy(origPath, ASSET.GetPath());

	// set the required asset path
	ASSET.SetPath(pTexturePath);

	// Store the old texture dictionary pointers - we don't want to find the textures in he tex list anywhere else
	// (note that this will waste memory but is more like what we'd do when loading a resource that had specified those
	// textures too)
	pgDictionary<grcTexture>* oldTexDict = pgDictionary<grcTexture>::GetCurrent();
	pgDictionary<grcTexture>::SetCurrent(NULL);

	// create the texture dictionary from the texture list 
#if !__FINAL
	m_pTextureDict = grmShaderGroup::CreateTextureDictionary(textureListName, true);
	if (!m_pTextureDict)
	{
		m_pTextureDict = rage_new pgDictionary<grcTexture>(0);
	}
#endif

	pgDictionary<grcTexture>::SetCurrent(oldTexDict);

	// restore the original asset path
	ASSET.SetPath(origPath);
}
#endif

int ptxFxList::FindExternalReferenceCount()
{
	int extRefs = 0;

	int effectRuleCount = m_pEffectRuleDict->GetCount();
	for(int i = 0; i < effectRuleCount; i++)
	{
		ptxEffectRule* rule = m_pEffectRuleDict->GetEntry(i);

		extRefs += (rule->GetRefCount() - 1); // All rules have a ref count of 1 (the dictionary itself owns a reference)
	}

	int textureCount = m_pTextureDict->GetCount();
	for(int i = 0; i < textureCount; i++)
	{
		grcTexture* tex = m_pTextureDict->GetEntry(i);

		extRefs += (tex->GetRefCount() - 1); // All textures have a ref count of 1 (the dictionary itself owns a reference)
	}

	return extRefs;
}

bool ptxFxList::AreAllShadersInSync()
{
	bool inSync = true;

	int ptxRuleCount = m_pParticleRuleDict->GetCount();
	for(int i = 0; i < ptxRuleCount; i++)
	{
		ptxParticleRule* rule = m_pParticleRuleDict->GetEntry(i);
		inSync = inSync && rule->GetShaderInst().GetIsDataInSync();
	}

	return inSync;
}

void ptxFxList::PostLoad(u32 fxListFlags)
{
	if (datResource::GetCurrent())
	{
#if __TASKLOG
		if (pgDictionary<grcTexture>::GetCurrent() != NULL)
		{
			TASKLOG_ONLY(s_TaskLog.Dump());
			Quitf("Why is there a pgDictionary on the stack still? See above for the task log");
		}
#endif
		// This has to be done in PostLoad, on the main thread, so that we know no one else is messing with the 
		// fxLists while we iterate over them

		ptxFxListIterator fxListIterator = RMPTFXMGR.CreateFxListIterator();
		for (fxListIterator.Start(FXLISTFLAG_SHAREABLE); !fxListIterator.AtEnd(); fxListIterator.Next())
		{
			fxListIterator.GetCurrFxList()->GetTextureDictionary()->Push();
		}

		// all shareable fxlists have already been pushed
		// only push this if it's not shareable as we don't want it in the txd stack twice
		bool isShareable = (fxListFlags & FXLISTFLAG_SHAREABLE) != 0;
		if (!isShareable)
		{
			m_pTextureDict->Push();
		}

		datResource::Place(m_pModelDict.ptr);

		if (!isShareable)
		{
			m_pTextureDict->Pop();
		}

		fxListIterator = RMPTFXMGR.CreateFxListIterator();
		for (fxListIterator.Start(FXLISTFLAG_SHAREABLE); !fxListIterator.AtEnd(); fxListIterator.Next())
		{
			fxListIterator.GetCurrFxList()->GetTextureDictionary()->RemoveFromList();
		}
	}

	// Call PostLoad on any child objects that need it.
	m_pEffectRuleDict->ForAll(ptxFxListHelpers::PostLoad, NULL);
	// m_pEmitterRuleDict->ForAll(ptxFxListHelpers::PostLoad, NULL);
	m_pParticleRuleDict->ForAll(ptxFxListHelpers::PostLoad, NULL);

#if RMPTFX_EDITOR
	sysMemStartTemp();
	CreateUiObjects(*this, NULL);
	sysMemEndTemp();
#endif
}

#if __DEV
bool PrintTextureRefCount(grcTexture& tex, u32, void*)
{
	ptxDisplayf("\t\t%s: count %d, known? %s", tex.GetName(), tex.GetRefCount(), tex.IsReferenced() ? "true" : "false");
	return true;
}
#endif

#if __DEV
bool PrintEffectRuleRefCount(ptxEffectRule& rule, u32, void*)
{
	ptxDisplayf("\t\t%s: count %d, known? %s", rule.GetName(), rule.GetRefCount(), rule.IsReferenced() ? "true" : "false");
	return true;
}
#endif

#if __DEV
bool PrintModelRefCount(rmcDrawable& model, u32, void*)
{
	ptxDisplayf("\t\t0x%p: known? %s", &model, model.IsReferenced() ? "true" : "false");
	return true;
}
#endif

#if __DEV
template<typename _T>
void PrintDictRefCount(datOwner<_T>& dict, const char* name)
{
	ptxDisplayf("\t%s dict: count %d, known? %s", name, dict->GetRefCount(), dict->IsReferenced() ? "true" : "false");
}
#endif

#if __DEV
void ptxFxList::PrintReferences(const char* fxListName)
{
	ptxDisplayf("");
	ptxDisplayf("ptxFxList %s: count %d, known? %s ", fxListName, GetRefCount(), IsReferenced() ? "true" : "false");
	PrintDictRefCount(m_pTextureDict, "Texture");
	m_pTextureDict->ForAll(&PrintTextureRefCount, NULL);
	PrintDictRefCount(m_pModelDict, "Model");
	m_pModelDict->ForAll(&PrintModelRefCount, NULL);
	PrintDictRefCount(m_pEffectRuleDict, "EffectRule");
	m_pEffectRuleDict->ForAll(&PrintEffectRuleRefCount, NULL);
	PrintDictRefCount(m_pEmitterRuleDict, "EmitRule");
	PrintDictRefCount(m_pParticleRuleDict, "PtxRule");
}
#endif


} // namespace rage
