<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Wind" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_influenceKFP" type="::rage::ptxKeyframeProp"/>
	<float name="m_highLodRange"/>
	<float name="m_lowLodRange"/>
	<int name="m_highLodDisturbanceMode"/>
	<int name="m_lowLodDisturbanceMode"/>
	<bool name="m_ignoreMtxWeight"/>
</structdef>

</ParserSchema>