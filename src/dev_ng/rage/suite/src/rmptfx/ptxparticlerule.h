// 
// rmptfx/ptxparticlerule.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXPARTICLERULE_H
#define RMPTFX_PTXPARTICLERULE_H


// includes (us)
#include "rmptfx/ptxbehaviour.h"
#include "rmptfx/ptxdrawable.h"
#include "rmptfx/ptxeffectspawning.h"
#include "rmptfx/ptxmisc.h"
#include "rmptfx/ptxrenderstate.h"
#include "rmptfx/ptxshader.h"

// includes (rage)
#include "atl/referencecounter.h"
#include "paging/base.h"


// namespaces
namespace rage 
{


// forward declarations
class ptxBehaviour;
class ptxEffectRule;
class rmcDrawable;


// defines
#define PTXPARTICLERULE_RORC_VERSION		(19 + grcEffect::RORC_VERSION)


enum ptxSortType
{
	PTXPARTICLERULE_SORTTYPE_CLOSEST_FIRST	= 0,
	PTXPARTICLERULE_SORTTYPE_FARTHEST_FIRST,
	PTXPARTICLERULE_SORTTYPE_NEWEST_FIRST,
	PTXPARTICLERULE_SORTTYPE_OLDEST_FIRST,
	PTXPARTICLERULE_SORTTYPE_RANDOM,

	PTXPARTICLERULE_SORTTYPE_ALL
};


// structures
#if RMPTFX_EDITOR
struct ptxParticleRuleUIData
{
	ptxParticleRuleUIData() {}
};
#endif


// classes
class ptxParticleRule : public pgBaseRefCounted
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor / destructor
		explicit ptxParticleRule();
		~ptxParticleRule();

		// resourcing
		explicit ptxParticleRule(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxParticleRule);

		// init functions
		void Init();
		void PostLoad();
		ptxPointSB* InitPoint(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxPointItem* pPointItem, ptxEvolutionList* pEvoList, ScalarV_In ptxFrameRatio, ScalarV_In dt);

		// update functions
		void UpdateParallel(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxEvolutionList* pEvoList, float dt);
		void UpdateFinalize(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxPointItem* pPointItem, ptxEvolutionList* pEvoList, float dt);

		// render functions
		void Draw(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxEvolutionList* pEvoList PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface));

		// point functions
		void SetPointPool(ptxPointPool* pPointPool);
		ptxPointPool* GetPointPool() const;
		int	GetNumPointsAvailable();
		ptxPointItem* ReservePoints(ptxList* pList, int &numPoints);
		ptxPointItem* CreateNewPoint(ptxList* pList);
		void ReturnPoint(ptxList* pPointList, ptxPointItem* pPointItem);

		// misc
#if RMPTFX_XML_LOADING
		static ptxParticleRule* CreateFromFile(const char * pFilename);
		void Save();
#endif
		void ReplaceModel(const char* pName, rmcDrawable* pDrawable);

		// evolutions
		void PrepareEvoData(ptxEvolutionList* pEvoList, bool isUpdateThread=true);
		
		// keyframe definitions
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);
		
		// params
		void SetParams(ptxBehaviourParams& params, ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxEvolutionList* pEvolutionList, ptxPointItem* pPointItem, ScalarV_In vDeltaTime, ScalarV_In vFrameRatio, int updateBufferIdx);
		void SetParamsPointItem(ptxBehaviourParams& params, ptxPointItem* pPointItem);

		// queries
		bool AddBehaviour(const char* pName);
		ptxBehaviour* GetBehaviour(const char* pName);
		int GetBehaviourIndex(const char* pName);
		bool GetUpdateOnMainThreadOnly() {return m_runtimeFlags.IsSet(PTXPARTICLERULE_ON_MAIN_THREAD_ONLY);}
		bool GetIsScreenSpace() {return m_runtimeFlags.IsSet(PTXPARTICLERULE_SCREENSPACE);}
		bool ShouldBeRendered();

		// accessors
		void SetName(const char* pName);
		const char* GetName() const {return m_name;}	
		ptxShaderInst& GetShaderInst() {return m_shaderInst;}
		const ptxShaderInst& GetShaderInst() const {return m_shaderInst;}
		void SetRenderState(ptxProjectionMode projMode) {m_renderState.SetRenderState(projMode);}
		int GetBlendSet() const {return m_renderState.GetBlendSet();}
		int GetNumDrawables() const {return m_drawables.GetCount();}
		ptxDrawable& GetDrawable(int idx) {return m_drawables[idx];}
		ptxSortType GetSortType() const {return (ptxSortType)m_sortType;}
		ptxDrawType GetDrawType() const {return (ptxDrawType)m_drawType;}
		ptxEffectSpawner& GetEffectSpawnerAtRatio() {return m_effectSpawnerAtRatio;}
		ptxEffectSpawner& GetEffectSpawnerOnColn() {return m_effectSpawnerOnColn;}
		int GetTexFrameIdMin() const {return m_texFrameIdMin;}
		int GetTexFrameIdMax() const {return m_texFrameIdMax;}
		__forceinline const u32 GetTextureHashNameForClipRegions() const {

			u32 textureHashName = m_shaderInst.GetTextureHashName(PTXSTR_DIFFUSETEXTURE_HASH);

			if(m_shaderInst.GetIsRefract() && textureHashName == 0)
			{
				textureHashName = m_shaderInst.GetTextureHashName(PTXSTR_REFRACTIONMAP_HASH);
				ptxAssertf(textureHashName != 0,"Refraction particle does not specify a refraction/diffuse texture");
			}

			return textureHashName;

		}
		__forceinline const char* GetTextureNameForClipRegions() const {

			const char* textureName = m_shaderInst.GetTextureName(PTXSTR_DIFFUSETEXTURE_HASH);
			if(textureName[0] == '\0')
			{
				return textureName;
			}

			if(m_shaderInst.GetIsRefract())
			{
				const char* textureRefractName = m_shaderInst.GetTextureName(PTXSTR_REFRACTIONMAP_HASH);
				ptxAssertf(textureRefractName != 0,"Refraction particle does not specify a refraction/diffuse texture");
				return textureRefractName;
			}
			return NULL;

		}

		void SetFlags(u8 flag) {m_flags |= flag;}
		bool GetFlags(u8 flag) const {return (m_flags & flag) ? true : false;}
		void ClearFlags(u8 flag) {m_flags &= ~flag;}
		void ClearAllFlags() {m_flags = 0;}
		u8 GetAllFlags() {return m_flags;}

		// referencing
		bool IsInUse() const {return GetRefCount()>1;}													// 1 reference is for the manager

		// profiling
#if RMPTFX_EDITOR
		ptxParticleRule* Clone();
#endif

		// debug or editor
#if RMPTFX_BANK || RMPTFX_EDITOR
		void RemoveDefaultKeyframes();
#endif

		// debug
#if RMPTFX_BANK
		int GetNumActiveBehaviours() const {return m_allBehaviours.GetCount();}
		ptxBehaviour* GetActiveBehaviour(int idx) {return m_allBehaviours[idx];}

		void GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def);
		void OutputKeyframeEntryInfo();
#endif

#if !__FINAL
		static sysSpinLockToken* GetUpdateProfileLockPtr() {return &sm_updateProfileLock;}
#endif

		// editor
#if RMPTFX_EDITOR
		void AddModel(const char* pName, bool reload=false);
		void RemoveModel(const char* pName);

		void SendToEditor(ptxByteBuff& buff);
		void ReceiveFromEditor(const ptxByteBuff& buff);
		void UpdateKeyframeDefns();

		void StartProfileUpdate();
		void StartProfileDraw();

		bool RemoveBehaviour(const char* pName);
		void ValidateBehaviour(const char* pName);
		void ValidateAllBehaviours();
		ptxKeyframeProp* GetKeyframeProp(u32 keyframePropId);
		virtual void CreateUiObjects();
		void UpdateUIData(ptxEvolutionList* pEvoList, const ptxEffectRule* pEffectRule, s32 eventIdx);
#endif 

		// xml loading
#if RMPTFX_XML_LOADING
		virtual void RepairAndUpgradeData(const char* pFilename); // If we loaded in an old version, upgrade it to the lastest
		virtual void ResolveReferences();
#endif


	private: //////////////////////////

		// behaviours
		void InitDefaultBehaviours();
		static ptxBehaviour* CreateBehaviour(const char* pName);
		void ShutdownBehaviours();
		void BuildBehaviourLists();

		void SetupRuntimeFlags();

		// misc
		void SetBiasLinks();

		// ui data
#if RMPTFX_EDITOR
		void CreateUIData();
		void DeleteUIData() {delete m_pUIData; m_pUIData=NULL;}
#else
		void CreateUIData() {m_pUIData = NULL;}
		void DeleteUIData() {m_pUIData = NULL;}
#endif

		// parser
#if RMPTFX_XML_LOADING
		void RemoveAllModels();

		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

#if RMPTFX_EDITOR
		ptxParticleRuleUIData* m_pUIData;
#else
		void* m_pUIData;
#endif

		ptxEffectSpawner m_effectSpawnerAtRatio;
		ptxEffectSpawner m_effectSpawnerOnColn;
		ptxRenderState m_renderState;

		float m_fileVersion;

		int m_texFrameIdMin;
		int m_texFrameIdMax;

		ConstString m_name;																		// stores effect/file name

		atArray<datRef<ptxBehaviour> > m_allBehaviours;											// would be datOwner<ptxBehaviour> but there is intentionally no Place function in ptxBehaviour - which breaks the template.
		atArray<datRef<ptxBehaviour> > m_initBehaviours;
		atArray<datRef<ptxBehaviour> > m_updateBehaviours;
		atArray<datRef<ptxBehaviour> > m_updateFinalizeBehaviours;
		atArray<datRef<ptxBehaviour> > m_drawBehaviours;
		atArray<datRef<ptxBehaviour> > m_releaseBehaviours;

		atArray<ptxBiasLink> m_biasLinks;
		datRef<ptxPointPool> m_pPointPool;														// shared point pool

		// This is built at the same time that the atArray's above are filled out.
		// The reason we don't just do virtual UpdatePoint() calls is that we want
		// to DMA the update code over to the SPU. So we use free functions instead,
		// to avoid performance loss as well as nightmares.
	
		struct ptxTemp_NOTUSED
		{
			void*	a;
			void*	b;
			void*	c;
			void*	d;
			u32		e;
			u32		f;
			u32		g;

#if __DECLARESTRUCT
			void DeclareStruct(datTypeStruct& UNUSED_PARAM(s)) {}
#endif
		};

		atArray<ptxTemp_NOTUSED> m_funcTable_NOTUSED;

		ptxShaderInst m_shaderInst;																// copied from ptxSprite
		atArray<ptxDrawable> m_drawables;
		u8 m_sortType; 																			// ptxSortType
		u8 m_drawType; 																			// ptxDrawType

		u8 m_flags;


		// runtime modifiable (i.e. non-constant) data
		enum 
		{
			PTXPARTICLERULE_ON_MAIN_THREAD_ONLY,
			PTXPARTICLERULE_SCREENSPACE
		};
		atFixedBitSet8 m_runtimeFlags;															// not resourced - set at runtime.

		void* m_pLastEvoList_NOTUSED;															// it's a void* cause you're not supposed to do anything with it but check its value - could be a hash

		// Wind is currently special - it still exists in the behaviour lists above, but we also keep a reference to it here
		// because unlike all the other behaviors wind needs a special per-emit-instance update call, so for now that'll just be custom code.
		class ptxu_Wind* m_pWindBehaviour;

		datPadding<8> m_pad2;


	public: ///////////////////////////

		// static variables
		static float sm_fileVersion;
#if !__FINAL
		static sysSpinLockToken sm_updateProfileLock;
#endif 

};


} // namespace rage


#endif // RMPTFX_PTXPARTICLERULE_H
