<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Noise" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_posNoiseMinKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_posNoiseMaxKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_velNoiseMinKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_velNoiseMaxKFP" type="::rage::ptxKeyframeProp"/>
	<int name="m_referenceSpace"/>
	<bool name="m_keepConstantSpeed"/>
</structdef>

</ParserSchema>