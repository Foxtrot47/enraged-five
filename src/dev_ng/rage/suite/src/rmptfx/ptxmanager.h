// 
// rmptfx/ptxmanager.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXMANAGER_H
#define RMPTFX_PTXMANAGER_H


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxfxlist.h"
#include "rmptfx/ptxiterators.h"
#include "rmptfx/ptxlist.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxupdatetask.h"
#include "rmptfx/ptxdraw.h"

// includes (rage)
#include "data/base.h"
#include "atl/hashstring.h"
#include "atl/ptr.h"
#include "math/vecrand.h"
#include "grcore/effect.h"
#include "grcore/stateblock.h"


// namespaces
namespace rage {


// forward declarations
class ptxInterface;

// defines
#define RMPTFX_VERSION										(3.40f)			// this needs to agree with the version number in the editor (DataMod.cs)
#define RMPTFX_MAX_CAMERAS									(16)

#if RMPTFX_XML_LOADING
#define RMPTFX_REGISTER_BEHAVIOUR(CLASSNAME)				(RMPTFXMGR.RegisterBehaviour(#CLASSNAME, ptxManager::CreateBehaviour<CLASSNAME>, ptxManager::PlaceBehaviour<CLASSNAME>))
#else
#define RMPTFX_REGISTER_BEHAVIOUR(CLASSNAME)				(RMPTFXMGR.RegisterBehaviour(#CLASSNAME, NULL, ptxManager::PlaceBehaviour<CLASSNAME>))
#endif

#define RMPTFX_CREATE_BEHAVIOUR_FROM_ID(CLASSNAMESTR)		((RMPTFXMGR.FindBehaviourFactory(CLASSNAMESTR))(RMPTFXMGR.GetPermanentBehaviourNamePtr(CLASSNAMESTR), NULL))
#define RMPTFX_CLONE_BEHAVIOUR(BEHAVIOURPTR)				((RMPTFXMGR.FindBehaviourFactory(BEHAVIOURPTR->GetName()))(BEHAVIOURPTR->GetName(), BEHAVIOURPTR))

#define RMPTFX_PTXEFFECTINST_SKIP_OCCLUSION_FRAME_COUNT		(10)

// enumerations
#if __RESOURCECOMPILER || RMPTFX_EDITOR
enum ptxSaveState 
{
	PTXSAVESTATE_INACTIVE									= 0,
	PTXSAVESTATE_START,
	PTXSAVESTATE_SAVING_EFFECT_RULES_XML, 
	PTXSAVESTATE_SAVING_EMITTER_RULES_XML, 
	PTXSAVESTATE_SAVING_PARTICLE_RULES_XML,
	PTXSAVESTATE_CANCEL_SAVE_ALL,
	PTXSAVESTATE_TELL_EDITOR_COMPLETE
};
#endif

#if RMPTFX_BANK
enum ptxQualityState
{
	PTX_QUALITY_HIRES = 0,
	PTX_QUALITY_LOWRES
};
#endif

enum ptxUpdateType
{
	PTXUPDATETYPE_ALL_AT_ONCE = 0,							// update the effects all at once
	PTXUPDATETYPE_USE_CALLBACK_FENCE						// make sure all callback effects are complete by the time WaitForCallbackEffects returns 
};

enum ptxUpdatePhase
{
	PTXUPDATEPHASE_ALL_EFFECTS = 0,
	PTXUPDATEPHASE_CALLBACK_EFFECTS,
	PTXUPDATEPHASE_LOCAL_EFFECTS
};

enum UpdatePhase
{
	UPDATE_ALL,
	UPDATE_CALLBACK_EFFECTS,
	UPDATE_LOCAL_EFFECTS,
};

// typedefs
typedef ptxBehaviour*(*ptxBehaviourFactoryFunc)(const char*, ptxBehaviour*);
typedef void(*ptxBehaviourPlaceFunc)(ptxBehaviour*, datResource&);

typedef Functor1<ptxEffectInst*>												ptxEffectInstFunctor;
typedef Functor2Ret<bool, ptxEffectInst*, ptxEffectInst*>						ptxEffectInstSpawnFunctor;
typedef Functor1Ret<float, ptxEffectInst*>										ptxEffectInstDistToCamFunctor;
typedef Functor1Ret<bool, Vec4V*>												ptxEffectInstOcclusionFunctor;
typedef Functor2Ret<bool, ptxEffectInst*, bool>									ptxEffectInstShouldBeRenderedFunctor;
typedef Functor1<ptxParticleRule*>												ptxParticleRuleFunctor;
typedef Functor1Ret<bool, ptxParticleRule*>										ptxParticleRuleShouldBeRenderedFunctor;
typedef Functor1<ptxShaderTemplate*>											ptxShaderFunctor;
typedef Functor3Ret<grcDepthStencilStateHandle, bool, bool, ptxProjectionMode>	ptxDepthStencilStateFunctor;
typedef Functor1Ret<grcBlendStateHandle, int>									ptxBlendStateFunctor;	

// structures
#if __RESOURCECOMPILER || RMPTFX_EDITOR
struct ptxSaveInfo
{
	ptxSaveInfo(ptxFxListIterator fxListIterator)
		: effectRuleIterator(fxListIterator)
		, emitterRuleIterator(fxListIterator)
		, particleRuleIterator(fxListIterator)
	{

	}

	int currEffectRuleId;
	int currEmitterRuleId;
	int currParticleRuleId;

	int numEffectRules;
	int numEmitterRules;
	int numParticleRules;

	ptxEffectRuleIterator effectRuleIterator;
	ptxEmitterRuleIterator emitterRuleIterator;
	ptxParticleRuleIterator particleRuleIterator;

	u32 fxListFlags;

	ptxSaveState currState;
};
#endif

struct ptxEmbeddedShader
{
	const char* m_pName;
	grmShader* m_pShader;
};

struct ptxEffectInstUpdateItem 
{
	static bool PartitionCallbackInsts(const ptxEffectInstUpdateItem& a) {return a.m_callbackUpdate;}

	ptxEffectInstItem* m_pEffectInst;										// this could be a u16 index
	bool m_callbackUpdate;
};


// classes
class ptxManager : public datBase
{
	friend class ptxUpdateTask;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxManager();
		virtual ~ptxManager();

		// main interface
		static void InitClass(int parallelUpdateScheduler = -1);		
		static void ShutdownClass();

		void CreatePointPool(int numPoints);
		void CreateEffectInstPool(int numEffectInsts);

		void BeginFrame();																		// must be called every frame whether updating or not - before Update()
		void Update(float dt, const grcViewport* pViewport, ptxUpdateType updateType = PTXUPDATETYPE_ALL_AT_ONCE);
		void Draw(ptxDrawList* pDrawList PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface) RMPTFX_BANK_ONLY(, bool bDebugDraw = true));
		void DrawManual(ptxEffectInst* pEffectInst PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface));

		// You MUST call this once a frame from a non-rmptfx thread at the point where it's no longer safe to update callback effects
		void WaitForCallbackEffects();
		// Call this once a frame to start the update local effects phase
		void StartLocalEffectsUpdate();

		void Reset(ptxEffectRule* pEffectRule);
		void ResetAllImmediately();
		

#if RMPTFX_BANK
		void InitDebug(bkBank* pVfxBank = NULL, bkGroup* pParticleGroup = NULL);
		void UpdateDebug(float dt);
#endif

		// pools
		ptxPointPool& GetPointPool() {return m_pointPool;}

		// draw lists
		ptxDrawList* CreateDrawList(const char* pName);
		int GetNumDrawLists() {return m_drawLists.GetCount();}
		ptxDrawList* GetDrawList(int idx) {return m_drawLists[idx];}
		atArray<ptxDrawList*>& GetDrawLists() {return m_drawLists;}

		// fx lists
		ptxFxList* LoadFxList(const char* pFilename, u32 fxListFlags);
		void PostLoadFxList(ptxFxList* pFxList, const atHashWithStringNotFinal& fxListName, u32 fxListFlags);

		void UnloadFxList(ptxFxList* pFxList, bool isResourced);
		void PreUnloadFxList(ptxFxList* pFxList);
		void PostUnloadFxList();

		// effect insts
		ptxEffectInst* GetEffectInst(const atHashWithStringNotFinal effectRuleHashName, const atHashWithStringNotFinal fxListName);
		ptxEffectInst* GetEffectInst(const atHashWithStringNotFinal effectRuleHashName, ptxFxList* pFxList=NULL, u32 effectInstflags=0);
		ptxEffectInst* GetEffectInst(ptxEffectRule* pEffectRule, u32 effectInstflags=0);
		void ActivateEffectInst(ptxEffectInst* pEffectInst);
		void DeactivateEffectInst(ptxEffectInst* pEffectInst);

		// rule queries
		ptxEffectRule* GetEffectRule(const atHashWithStringNotFinal effectRuleHashName, const atHashWithStringNotFinal fxListName);
		ptxEffectRule* GetEffectRule(const atHashWithStringNotFinal effectRuleHashName, ptxFxList* pFxList=NULL, u32 fxListFlags=FXLISTFLAG_ALL);
		ptxEmitterRule* GetEmitterRule(const atHashWithStringNotFinal emitterRuleHashName, ptxFxList* pFxList=NULL, u32 fxListFlags=FXLISTFLAG_ALL);
		ptxParticleRule* GetParticleRule(const atHashWithStringNotFinal particleRuleHashName, ptxFxList* pFxList=NULL, u32 fxListFlags=FXLISTFLAG_ALL);

		// graphics
		void SetRenderSetup(ptxRenderSetup *pRenderSetup);
		void SetFrameBuffer(grcRenderTarget* pRenderTarget) {m_pFrameBuffer[g_RenderThreadIndex] = pRenderTarget;}
		void SetFrameBufferSampler(grcRenderTarget* pRenderTarget) {m_pFrameBufferSampler[g_RenderThreadIndex] = pRenderTarget;}
		grcRenderTarget* GetFrameBuffer();
		grcRenderTarget* GetFrameBufferSampler();
		void SetDepthBuffer(grcRenderTarget* pRenderTarget) {m_pDepthBuffer[g_RenderThreadIndex] = pRenderTarget;}
		grcRenderTarget* GetDepthBuffer();	

#if RSG_PC
		void SetDepthWriteEnabled(bool depthWriteEnabled) {m_bDepthWriteEnabled[g_RenderThreadIndex] = depthWriteEnabled; }
		bool GetDepthWriteEnabled() { return m_bDepthWriteEnabled[g_RenderThreadIndex]; }
#endif

		// models and textures
		rmcDrawable* LoadModel(const char* pFilename, bool reload=false);
		grcTexture* LoadTexture(const char* pFilename, bool reload=false, bool filenameIsFullPath=false, bool* pIsExternalSource=NULL);

		// behaviours
		void RegisterBehaviour(const char* pBehaviourName, ptxBehaviourFactoryFunc behaviourFactoryFunc, ptxBehaviourPlaceFunc behaviourPlaceFunc);
		template <typename T> static ptxBehaviour* CreateBehaviour(const char* pBehaviourName, ptxBehaviour* pBehaviourToClone = NULL);
		template <typename T> static void PlaceBehaviour(ptxBehaviour* pBehaviour, datResource& rsc);
		ptxBehaviourFactoryFunc FindBehaviourFactory(const char* pBehaviourName) {ptxBehaviourFactoryFunc* pBehaviourFactoryFunc = m_behaviourFactoryMap.Access(pBehaviourName); return pBehaviourFactoryFunc ? *pBehaviourFactoryFunc : NULL;}
		ptxBehaviourPlaceFunc FindBehaviourPlace(u32 behaviourId) {ptxBehaviourPlaceFunc* pBehaviourPlaceFunc = m_behaviourPlaceMap.Access(behaviourId); return pBehaviourPlaceFunc ? *pBehaviourPlaceFunc : NULL;}
		const char* GetPermanentBehaviourNamePtr(const char* pBehaviourName);

		// callbacks and functors
		void SetEffectInstInitFunctor(ptxEffectInstFunctor functor) {m_effectInstInitFunctor = functor;}
		ptxEffectInstFunctor GetEffectInstInitFunctor() {return m_effectInstInitFunctor;}
		void SetEffectInstStartFunctor(ptxEffectInstFunctor functor) {m_effectInstStartFunctor = functor;}
		ptxEffectInstFunctor GetEffectInstStartFunctor() {return m_effectInstStartFunctor;}
		void SetEffectInstStopFunctor(ptxEffectInstFunctor functor) {m_effectInstStopFunctor = functor;}
		ptxEffectInstFunctor GetEffectInstStopFunctor() {return m_effectInstStopFunctor;}
		void SetEffectInstDeactivateFunctor(ptxEffectInstFunctor functor) {m_effectInstDeactivateFunctor = functor;}
		ptxEffectInstFunctor GetEffectInstDeactivateFunctor() {return m_effectInstDeactivateFunctor;}
		void SetEffectInstPreUpdateFunctor(ptxEffectInstFunctor functor) {m_effectInstPreUpdateFunctor = functor;}
		ptxEffectInstFunctor GetEffectInstPreUpdateFunctor() {return m_effectInstPreUpdateFunctor;}
		void SetEffectInstPostUpdateFunctor(ptxEffectInstFunctor functor) {m_effectInstPostUpdateFunctor = functor;}
		ptxEffectInstFunctor GetEffectInstPostUpdateFunctor() {return m_effectInstPostUpdateFunctor;}
		void SetEffectInstUpdateSetupFunctor(ptxEffectInstFunctor functor) {m_effectInstUpdateSetupFunctor = functor;}
		ptxEffectInstFunctor GetEffectInstUpdateSetupFunctor() {return m_effectInstUpdateSetupFunctor;}
		void SetEffectInstPreDrawFunctor(ptxEffectInstFunctor functor) {m_effectInstPreDrawFunctor = functor;}
		ptxEffectInstFunctor GetEffectInstPreDrawFunctor() {return m_effectInstPreDrawFunctor;}
		void SetEffectInstPostDrawFunctor(ptxEffectInstFunctor functor) {m_effectInstPostDrawFunctor = functor;}
		ptxEffectInstFunctor GetEffectInstPostDrawFunctor() {return m_effectInstPostDrawFunctor;}
		void SetEffectInstCullFunctor(ptxEffectInstFunctor functor) {m_effectInstCullFunctor = functor;};
		ptxEffectInstFunctor GetEffectInstCullFunctor() {return m_effectInstCullFunctor;};
		void SetEffectInstUpdateCullingFunctor(ptxEffectInstFunctor functor) {m_effectInstUpdateCullingFunctor = functor;};
		ptxEffectInstFunctor GetEffectInstUpdateCullingFunctor() {return m_effectInstUpdateCullingFunctor;};
		void SetEffectInstUpdateFinalizeFunctor(ptxEffectInstFunctor functor) {m_effectInstUpdateFinalizeFunctor = functor;}
		void SetEffectInstSpawnFunctor(ptxEffectInstSpawnFunctor functor) {m_effectInstSpawnFunctor = functor;}
		ptxEffectInstSpawnFunctor GetEffectInstSpawnFunctor() {return m_effectInstSpawnFunctor;}
		void SetEffectInstDistToCamFunctor(ptxEffectInstDistToCamFunctor functor) {m_effectInstDistToCamFunctor = functor;}
		ptxEffectInstDistToCamFunctor GetEffectInstDistToCamFunctor() {return m_effectInstDistToCamFunctor;}
		void SetEffectInstOcclusionFunctor(ptxEffectInstOcclusionFunctor functor) {m_effectInstOcclusionFunctor = functor;}
		ptxEffectInstOcclusionFunctor GetEffectInstOcclusionFunctor() {return m_effectInstOcclusionFunctor;}
		void SetEffectInstShouldBeRenderedFunctor(ptxEffectInstShouldBeRenderedFunctor functor) {m_effectInstShouldBeRenderedFunctor = functor;}
		ptxEffectInstShouldBeRenderedFunctor GetEffectInstShouldBeRenderedFunctor() {return m_effectInstShouldBeRenderedFunctor;}
		void SetParticleRuleInitFunctor(ptxParticleRuleFunctor functor) {m_particleRuleInitFunctor = functor;}
		ptxParticleRuleFunctor GetParticleRuleInitFunctor() {return m_particleRuleInitFunctor;}
		void SetParticleRuleShouldBeRenderedFunctor(ptxParticleRuleShouldBeRenderedFunctor functor) {m_particleRuleShouldBeRenderedFunctor = functor;}
		ptxParticleRuleShouldBeRenderedFunctor GetParticleRuleShouldBeRenderedFunctor() {return m_particleRuleShouldBeRenderedFunctor;}
		void SetShaderInitProgVarsFunctor(ptxShaderFunctor functor) {m_shaderInitProgVarsFunctor = functor;}
		ptxShaderFunctor GetShaderInitProgVarsFunctor() {return m_shaderInitProgVarsFunctor;}
		void SetShaderSetProgVarsFunctor(ptxShaderFunctor functor) {m_shaderSetProgVarsFunctor = functor;}
		ptxShaderFunctor GetShaderSetProgVarsFunctor() {return m_shaderSetProgVarsFunctor;}
		void SetOverrideDepthStencilStateFunctor(ptxDepthStencilStateFunctor functor) {m_overrideDepthStencilStateFunctor = functor;}
		ptxDepthStencilStateFunctor GetOverrideDepthStencilStateFunctor() {return m_overrideDepthStencilStateFunctor;}
		void SetOverrideBlendStateFunctor(ptxBlendStateFunctor functor) {m_overrideBlendStateFunctor = functor;}
		ptxBlendStateFunctor GetOverrideBlendStateFunctor() {return m_overrideBlendStateFunctor;}


		// critical sections
		sysCriticalSectionToken& GetUpdateListCSToken() {return m_updateListCSToken;}

		// misc
		static float GetVersion() {return RMPTFX_VERSION;}
		bool GetIsMultithreaded() {return RMPTFXTHREADER.GetIsMultithreaded();}
		ptxFxListIterator CreateFxListIterator();
		sysIpcCurrentThreadId GetUpdateThreadId() {return m_updateThreadId;}
		bool GetIsUpdating() {return m_isUpdating;}
		void SetupPtxKeyFramePropThreadId();
		bool IsParticleUpdateThreadOrTask() {return ((GetUpdateThreadId() == sysIpcGetCurrentThreadId()) || (ptxUpdateTask::GetPtxUpdateTaskIdx(sysIpcGetCurrentThreadId()) > -1));}

		__forceinline void AddEmitterInstUpdateItem(const ptxEmitterInstUpdateItem& emitterInstUpdateItem);

		// static 
		static void SetAssetPath(const char* pAssetPath) {formatf(sm_assetPath, sizeof(sm_assetPath), "%s", pAssetPath);}
		static const char* GetAssetPath() {return sm_assetPath;}
		static void SetForcedColourShaderVarName(const char* pShaderVarName) {formatf(sm_forcedColourShaderVarName, sizeof(sm_forcedColourShaderVarName), "%s", pShaderVarName);}
		static const char* GetForcedColourShaderVarName() {return sm_forcedColourShaderVarName;}
		static u8 GetMaxFrameSkipOcclusionTestForEffectInstances() { return RMPTFX_BANK_SWITCH(sm_maxFrameSkipOcclusionTest, RMPTFX_PTXEFFECTINST_SKIP_OCCLUSION_FRAME_COUNT); }

#if RMPTFX_BANK
		static bool* GetUpdateOnSingleThreadPtr() {return &sm_updateOnSingleThread;}
		static bool* GetSingleUpdateTaskPtr() {return &sm_singleUpdateTask;}
		static bool* GetUpdateOnChildAndMainThreadsPtr() {return &sm_updateOnChildAndMainThreads;}

		static void SetSkipOcclusionTestForEffectInstances(bool value) { sm_skipOcclusionTestForEffectInstances = value; }
		static bool GetIsSkipOcclusionTestForEffectInstances() { return sm_skipOcclusionTestForEffectInstances; }
		static bool* GetIsSkipOcclusionTestForEffectInstancesPtr() { return &sm_skipOcclusionTestForEffectInstances; }
		static void SetMaxFrameSkipOcclusionTestForEffectInstances(u8 numFrames) { sm_maxFrameSkipOcclusionTest = numFrames; }
		static u8* GetMaxFrameSkipOcclusionTestForEffectInstancesPtr() { return &sm_maxFrameSkipOcclusionTest; }
		static ptxQualityState GetQualityState() {return sm_ptxQualityState;}
		static void SetQualityState(ptxQualityState qualityState) {sm_ptxQualityState = qualityState;}
#endif

		// settings
		void SetHighQualityEvo(float val) {m_highQualityEvo = val;}
		float GetHighQualityEvo() {return m_highQualityEvo;}

		// debug
#if RMPTFX_THOROUGH_ERROR_CHECKING
		void VerifyLists();
		void CheckDataIntegrity();
#endif

#if __DEV
		void OutputActiveEffectInsts();
		void OutputFxListReferences();
		void OutputLoadedFxLists();
#endif

		// editor
#if RMPTFX_EDITOR
		const ptxInterface& GetInterface() const {return *m_pInterface;}
		ptxInterface& GetInterface() {return *m_pInterface;}

		int GetAllEffectRules(const atHashWithStringNotFinal effectRuleHashName, ptxEffectRule** ppEffectRules, s32 maxResults, u32 fxListFlags=FXLISTFLAG_ALL);
		int GetAllEmitterRules(const atHashWithStringNotFinal emitterRuleHashName, ptxEmitterRule** ppEmitterRules, s32 maxResults, u32 fxListFlags=FXLISTFLAG_ALL);
		int GetAllParticleRules(const atHashWithStringNotFinal particleRuleHashName, ptxParticleRule** ppParticleRules, s32 maxResults, u32 fxListFlags=FXLISTFLAG_ALL);
		int GetAllKeyframes(ptxKeyframe** ppKeyframes, s32 maxResults, const atHashWithStringNotFinal ruleHashName, const ptxRuleType ruleType, const u32 propertyId, const s32 eventIdx, const s32 evoIdx);

		void SaveFxList(const char* pFxListName);
		ptxEffectRule* CreateNewEffectRule(const char* pEffectRuleName);
		ptxEmitterRule* CreateNewEmitterRule(const char* pEmitterRuleName);
		ptxParticleRule* CreateNewParticleRule(const char* pParticleRuleName);
		void RenameEffectRule(const char* pCurrEffectRuleName, const char* pNewEffectRuleName);
		void RenameEmitterRule(const char* pCurrEmitterRuleName, const char* pNewEmitterRuleName);
		void RenameParticleRule(const char* pCurrParticleRuleName, const char* pNewParticleRuleName);
		ptxParticleRule* CloneParticleRule(const char* pParticleRuleName);

		int GetNumEffectRuleGameFlagNames() {return m_effectRuleGameFlagNames.GetCount();}
		void AddEffectRuleGameFlagName(const char* pGameFlagName);

		int GetNumEffectRuleDataVolumeTypeNames() {return m_effectRuleDataVolumeTypeNames.GetCount();}
		void AddEffectRuleDataVolumeTypeName(const char* pDataVolumeTypeName);

		const char* FindAnalogousBehaviour(const char* pBehaviourName, ptxDrawType drawType);
		void SendBehaviourListToEditor(ptxByteBuff& buff);
		void SendEvolvedKeyframeDefnToEditor(ptxKeyframe* pKeyframe, const ptxEffectRule* pEffectRule, u32 propertyId, s32 eventIdx, s32 evoIdx);
		void SendUpdatedBehaviourKFPDefnToEditor(ptxKeyframeProp* pKeyframeProp, const ptxParticleRule* pParticleRule);
		void SendProfileDataToEditor(ptxByteBuff& buff);
		void SendGameFlagNamesToEditor(ptxByteBuff& buff);
		void SendDataVolumeTypeNamesToEditor(ptxByteBuff& buff);

		void SetDeltaTimeScalar(float val) {m_deltaTimeScalar=val;}

		// debug effect
		void SetDebugEffectCameraMtx(const Mat34V* pvCameraMtx) {m_pvDebugEffectCameraMtx = pvCameraMtx;}
		void StartDebugEffect(const char* pEffectRuleName);
		void UpdateDebugEffect(float dt);
		void ResetDebugEffect();
		void DeactivateDebugEffect();
		bool IsDebugEffect(ptxEffectInst* pFxInst) {return m_pDebugEffectInst==pFxInst;}
#endif

#if RMPTFX_BANK
		ptxFxList* GetFxList(const atHashWithStringNotFinal fxListHashName);
		int GetAllEffectRuleRefs(const atHashWithStringNotFinal effectRuleHashName, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags=FXLISTFLAG_ALL);
		int GetAllEmitterRuleRefs(const atHashWithStringNotFinal emitterRuleHashName, ptxEffectRule** ppEffectRules, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags=FXLISTFLAG_ALL);
		int GetAllParticleRuleRefs(const atHashWithStringNotFinal particleRuleHashName, ptxEffectRule** ppEffectRules, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags=FXLISTFLAG_ALL);
		int GetAllTextureRefs(const atHashWithStringNotFinal textureHashName, ptxParticleRule** ppParticleRules, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags=FXLISTFLAG_ALL);
		int GetAllModelRefs(const atHashWithStringNotFinal modelHashName, ptxParticleRule** ppParticleRules, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags=FXLISTFLAG_ALL);

		void RemoveDefaultKeyframes();
		void GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def);

		// debug helpers
		void DebugDrawFxListReferences();
#endif

		// saving
#if __RESOURCECOMPILER || RMPTFX_EDITOR
		void SaveAll();
		void SaveAllCancel();
		void SaveAllUpdate();
#endif

		// xml loading
#if RMPTFX_XML_LOADING
		ptxEffectRule* LoadEffectRule(const char* pEffectRuleName); 
		ptxEmitterRule* LoadEmitterRule(const char* pEmitterRuleName);
		ptxParticleRule* LoadParticleRule(const char* pParticleRuleName);
		
		bool RemoveEffectRule(ptxEffectRule* pEffectRule, bool removeDependents, bool justRemoveFromList=false);
		bool RemoveEmitterRule(ptxEmitterRule* pEmitterRule, bool removeDependents, bool justRemoveFromList=false);
		bool RemoveParticleRule(ptxParticleRule* pParticleRule, bool removeDependents, bool justRemoveFromList=false);

		ptxBehaviour* GetBehaviour(const char* pBehaviourName) {return *m_behaviourMap.Access(pBehaviourName);}
		ptxBehaviour* FindBehaviourFromPropertyId(u32 propertyId);
		void AddBehavioursWithActiveEvos();
#endif 

#if RMPTFX_USE_PARTICLE_SHADOWS
		void SetShadowPass(bool enable);
		bool IsShadowPass() {return m_ShadowPass[g_RenderThreadIndex];}

		void SetShadowSourcePos(Vec3V_In pos) {m_ShadowSourcePos[g_RenderThreadIndex] = pos;}
		const Vec3V GetShadowSourcePos() { return m_ShadowSourcePos[g_RenderThreadIndex];}

		void SetGlobalShadowIntensity(float intensity) {m_globalShadowIntensity = intensity;}
		float GetGlobalShadowIntensity() {return m_globalShadowIntensity;}
#endif

#if GS_INSTANCED_SHADOWS
		void SetUsingGeometryShader(bool enable) {m_UsingGeometryShader = enable;}
		bool IsUsingGeometryShader() { return m_UsingGeometryShader; }
#endif

	private: //////////////////////////

		void UpdateSetup(float dt, const grcViewport* pViewport);
		void UpdateParallel(float dt, ptxUpdatePhase updatePhase);
		void UpdateFinalize(float dt, ptxUpdatePhase updatePhase);
		void UpdateCleanup( float dt );

		// draw helpers
		void PreDraw();
		unsigned SortEffectInstList(ptxEffectInstItem** ppSorted, ptxList* pEffectInstList);
		void DrawEffectInstList(ptxEffectInstItem** ppSorted, unsigned num PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface));
		void PostDraw();

		// effect inst helpers
		ptxEffectInst* GetEffectInstFromPool();
		void MoveEffectInst(ptxEffectInst* pEffectInst, int drawListIdx);
		void FinishEffectInsts(ptxEffectRule* pEffectRule);
		void FinishEffectInsts(ptxFxList* pFxList);
		static void UnReferenceEffectInstItemCB(ptxListItemBase* pPtxListItem);

		// behaviour helpers
		void RegisterBehaviours();
		
		// model helpers
		ptxDrawableEntry* LoadDrawable(const char* pFilename);

		// debug helpers
#if RMPTFX_BANK
		void DebugDrawEffectInstList(ptxEffectInstItem** ppSorted, unsigned num);
#endif

		// editor helpers
#if RMPTFX_EDITOR
		void AddBehaviourToList();

		const char* GenerateUniqueEffectRuleName();
		const char* GenerateUniqueEmitterRuleName();
		const char* GenerateUniqueParticleRuleName();
		const char* GenerateUniqueClonedParticleRuleName(const char* pOldParticleRuleName);


		void StartProfileUpdate();
		void StartProfileDraw();
		
		void UpdateParticleRuleTexture(const char* pFilename, grcTexture* pTexture);
#endif

		void SetDrawManualRenderState();
		void SetExitDrawManualRenderState();

		static void InitRenderStateBlocks();

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		// pools
		ptxPointPool m_pointPool;													// pool of points
		ptxEffectInstPool m_effectInstPool;											// pool of effect insts

		// lists
		ptxList m_effectInstList;													// list of inactive effect insts
		atArray<ptxDrawList*> m_drawLists;											// list of draw lists (which have lists of active effect insts in them)
		atArray<ptxDrawList*> m_spawnLists;											// list of draw lists for effects that just got spawned

		// maps
		ptxFxListMap m_fxListMap;													// map of all registered fx lists
		atStringMap<datOwner<ptxDrawableEntry> > m_drawableEntries;					// map of drawables entries

		// graphics
		ptxRenderSetup *m_pRenderSetup[NUMBER_OF_RENDER_THREADS];
		grcEffectGlobalVar m_forcedColourShaderVar;									// 
		grcRenderTarget* m_pFrameBuffer[NUMBER_OF_RENDER_THREADS];					// pointer to the external frame buffer
		grcRenderTarget* m_pDepthBuffer[NUMBER_OF_RENDER_THREADS];					// pointer to the external depth buffer
		grcRenderTarget* m_pFrameBufferSampler[NUMBER_OF_RENDER_THREADS];			// pointer to the external frame buffer texture used for sampling in shaders

#if RSG_PC
		bool			m_bDepthWriteEnabled[NUMBER_OF_RENDER_THREADS];
#endif
		bool m_swapBuffers;

		// behaviours
		atMap<const char*, ptxBehaviourFactoryFunc> m_behaviourFactoryMap;			// map of behaviour creation functions
		atMap<u32, ptxBehaviourPlaceFunc> m_behaviourPlaceMap;						// TODO: we should be able to use the parser for this

		// update task 
		atArray<ptxEmitterInstUpdateItem> m_emitterInstUpdateItems;					// array of all the emitter instances to update this frame - created in UpdateSetup and iterated over in UpdateParallel and UpdateFinalize
		volatile u32 m_currEmitterInstUpdateItemIdx;

		atArray<ptxEffectInstUpdateItem> m_effectInstUpdateItems;					// array of all the effect instances to update this frame

		// for 2 pass update
		u32 m_numCallbackEmitterInstUpdateItems;
		u32 m_numCallbackEffectInstUpdateItems;
		sysIpcSema m_callbackEffectsUpdateCompleteSema;
		sysIpcSema m_localEffectsUpdateStartedSema;
		bool m_waitedForCallbackEffectUpdate;
		bool m_isDoingCallbackFenceUpdate;

		// functors
		ptxEffectInstFunctor m_effectInstInitFunctor;
		ptxEffectInstFunctor m_effectInstStartFunctor;
		ptxEffectInstFunctor m_effectInstStopFunctor;
		ptxEffectInstFunctor m_effectInstDeactivateFunctor;
		ptxEffectInstFunctor m_effectInstPreUpdateFunctor;
		ptxEffectInstFunctor m_effectInstPostUpdateFunctor;
		ptxEffectInstFunctor m_effectInstPreDrawFunctor;
		ptxEffectInstFunctor m_effectInstUpdateSetupFunctor;
		ptxEffectInstFunctor m_effectInstPostDrawFunctor;
		ptxEffectInstFunctor m_effectInstCullFunctor;
		ptxEffectInstFunctor m_effectInstUpdateCullingFunctor;
		ptxEffectInstFunctor m_effectInstUpdateFinalizeFunctor;
		ptxEffectInstSpawnFunctor m_effectInstSpawnFunctor;
		ptxEffectInstDistToCamFunctor m_effectInstDistToCamFunctor;
		ptxEffectInstOcclusionFunctor m_effectInstOcclusionFunctor;
		ptxEffectInstShouldBeRenderedFunctor m_effectInstShouldBeRenderedFunctor;
		ptxParticleRuleFunctor m_particleRuleInitFunctor;
		ptxParticleRuleShouldBeRenderedFunctor m_particleRuleShouldBeRenderedFunctor;
		ptxShaderFunctor m_shaderInitProgVarsFunctor;
		ptxShaderFunctor m_shaderSetProgVarsFunctor;
		ptxDepthStencilStateFunctor m_overrideDepthStencilStateFunctor;
		ptxBlendStateFunctor m_overrideBlendStateFunctor;

		// critical sections
		sysCriticalSectionToken m_updateListCSToken;
		sysCriticalSectionToken m_freeListCSToken;

		// settings
		float m_highQualityEvo;

		// debug
		sysIpcCurrentThreadId m_updateThreadId;
		sysIpcCurrentThreadId m_mainThreadId;
		volatile bool m_isUpdating;
#if __DEV
		bool m_beginFrameCalled;

		volatile unsigned m_isDrawing;
		volatile bool m_isUpdatingParallel;
#endif

		// saving
#if __RESOURCECOMPILER || RMPTFX_EDITOR
		ptxSaveInfo* m_pSaveInfo;
#endif

		// editor
#if RMPTFX_EDITOR
		ptxInterface* m_pInterface;													// pointer to the interface module for communicating with the particle editor

		atArray<ptxBehaviour*> m_behaviours;
		struct char128 { char c[128]; };
		atArray<char128> m_effectRuleGameFlagNames;									// game specific flags editor names
		atArray<char128> m_effectRuleDataVolumeTypeNames;							// data volume type editor names

		// playback 
		float m_deltaTimeScalar;													// debug time scalar

		// debug effect
		const Mat34V* m_pvDebugEffectCameraMtx;										// pointer to the game's camera matrix so debug effects can play relative to the camera
		ptxEffectInst* m_pDebugEffectInst;											// pointer to the current debug effect instance
		char m_debugEffectInstName[128];											// name of the debug effect instance
		Vec3V m_vDebugEffectAnchorPos;												// anchor position of the debug effect instance
		float m_debugEffectAnimTimer;												// current animation timer of the debug effect instance
		float m_debugEffectRepeatTimer;												// current repeat timer of the debug effect instance

		// profiling
		int m_numActiveInstances;
		int m_numDrawCulledInstances;
		int m_numUpdateCulledInstances;
		float m_totalCPUUpdateTime;
		float m_totalCPUDrawTime;
		float m_totalGPUDrawTime;
#endif

		//states for which pass
		bool m_ShadowPass[NUMBER_OF_RENDER_THREADS];
		Vec3V m_ShadowSourcePos[NUMBER_OF_RENDER_THREADS];
		float m_globalShadowIntensity;

#if GS_INSTANCED_SHADOWS
		bool m_UsingGeometryShader;
#endif

		// xml loading
#if RMPTFX_XML_LOADING
		atMap<const char*, ptxBehaviour*> m_behaviourMap;							// map of all behaviours registered with the system
		pgRef<ptxFxList> m_pLooseFxList;											// all the rules (and models and textures) that weren't loaded in via an fxlist
#endif

		// static variables
		static char	sm_assetPath[256];												// path to the assets
		static char sm_forcedColourShaderVarName[64];								// name of the shader var which controls model colour
		static bool sm_updateOnSingleThread;
		static bool sm_singleUpdateTask;
		static bool sm_updateOnChildAndMainThreads;
		static ptxEmbeddedShader sm_embeddedShaders[];

#if RMPTFX_BANK
		static u8 sm_maxFrameSkipOcclusionTest;
		static bool sm_skipOcclusionTestForEffectInstances;
		static ptxQualityState sm_ptxQualityState;
#endif

		static grcRasterizerStateHandle		sm_drawManualRasterizerState;
		static grcDepthStencilStateHandle	sm_drawManualDepthStencilState;
		static grcBlendStateHandle			sm_drawManualBlendState;

		static grcRasterizerStateHandle		sm_drawManualShadowsRasterizerState;
		static grcDepthStencilStateHandle	sm_drawManualShadowsDepthStencilState;
		static grcBlendStateHandle			sm_drawManualShadowsBlendState;

		static grcRasterizerStateHandle		sm_exitDrawManualRasterizerState;
		static grcDepthStencilStateHandle	sm_exitDrawManualDepthStencilState;
		static grcBlendStateHandle			sm_exitDrawManualBlendState;

		// temporary debug info for BS#591930
public:
		static int							sm_debugLastSortPointsCount;
		static void*						sm_debugLastSortPointsBuffer;
		static void*						sm_debugLastSortPointsBufferOrig;
};


// singletons
typedef atSingleton<ptxManager> g_ptxManager;
#define RMPTFXMGR g_ptxManager::InstanceRef()


// externs
extern mthVecRand g_ptxVecRandom;


// templates
template <typename T> ptxBehaviour* ptxManager::CreateBehaviour(const char* UNUSED_PARAM(behaviourId), ptxBehaviour* pBehaviourToClone)
{
	T* pNewBehaviour = rage_new T;
	if(pBehaviourToClone)
	{
		*pNewBehaviour = *((T*)(pBehaviourToClone));
		pNewBehaviour->SetDefaultData();
	}
	return pNewBehaviour;
}

template <typename T> void ptxManager::PlaceBehaviour(ptxBehaviour* pBehaviour, datResource& rsc)
{
	T* pBehaviourDerived = reinterpret_cast<T*>(pBehaviour);
	if (pBehaviourDerived)
	{
		T::Place(pBehaviourDerived, rsc);
	}
}


// inlines 
__forceinline void ptxManager::AddEmitterInstUpdateItem(const ptxEmitterInstUpdateItem& emitterInstUpdateItem)
{
	m_emitterInstUpdateItems.Grow() = emitterInstUpdateItem;
}


} // namespace rage


#endif // RMPTFX_PTXMANAGER_H
