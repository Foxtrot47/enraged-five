<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<const name="PTXKEYFRAME_MAX_CHANNELS" value="4"/>

<structdef type="rage::ptxKeyframeDefn">
	<string name="m_name" type="pointer"/>
	<array name="m_labels" type="member" size="PTXKEYFRAME_MAX_CHANNELS">
		<string type="pointer"/>
	</array>
	<array name="m_colours" type="member" size="PTXKEYFRAME_MAX_CHANNELS">
		<Color32/>
	</array>
	<Vector4 name="m_vDefaultValue"/>
	<Vector3 name="m_vMinMaxDeltaX"/>
	<Vector3 name="m_vMinMaxDeltaY"/>
	<int name="m_propertyId"/>
	<int name="m_type"/>
	<bool name="m_useColourGradient"/>
</structdef>

<structdef type="rage::ptxKeyframe" onPostSave="PostSaveCallback" onPreLoad="PreLoadCallback">
</structdef>

<structdef type="rage::ptxKeyframeXmlData">
	<int name="m_numKeyEntries"/>
	<array name="m_keyEntryData" type="atArray" xValuesPerLine="5">
		<float/>
	</array>
</structdef>

<structdef type="rage::ptxKeyframeSpec">
	<string name="m_pDefnName" type="pointer"/>
	<string name="m_pRuleName" type="pointer"/>
	<s32 name="m_ruleType"/>
	<u32 name="m_propertyId"/>
</structdef>

</ParserSchema>