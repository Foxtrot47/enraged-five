//
// rmptfx/ptxeffectinst.cpp 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxeffectinst.h"

// includes (rmptfx)
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxfxlist.h"
#include "rmptfx/ptxinterface.h"
#include "rmptfx/ptxmanager.h"

// includes (rage)
#include "grcore/debugdraw.h"
#include "grcore/viewport_inline.h"
#if RMPTFX_BANK
#include "system/stack.h"
#endif
#include "vector/colors.h"
#include "vector/geometry.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// defines


// namespaces
using namespace rage;

#if RMPTFX_BANK
PARAM(rmptfxPrintCallstackWhenStoppingEffect, "Prints the callstack when an effect instance is getting stopped");
#endif

// code
float ptxDataVolume::GetPointTValue(Vec3V_In vPoint)
{
	if (m_length>0.0f)
	{
		// it's a capsule - get t value to the capsule's line segment
		Vec3V vVecStartToEnd = m_vPosWldEnd - m_vPosWldStart;
		return geomTValues::FindTValueSegToPoint(RCC_VECTOR3(m_vPosWldStart), RCC_VECTOR3(vVecStartToEnd), RCC_VECTOR3(vPoint));
	}
	else
	{
		// it's a sphere
		return 1.0f;
	}
}

float ptxDataVolume::GetPointDistSqr(Vec3V_In vPoint)
{
	float distSqr;
	if (m_length>0.0f)
	{
		// it's a capsule - get distance to the capsule's line segment
		Vec3V vVecStartToEnd = m_vPosWldEnd - m_vPosWldStart;
		distSqr = geomDistances::Distance2SegToPoint(RCC_VECTOR3(m_vPosWldStart), RCC_VECTOR3(vVecStartToEnd), RCC_VECTOR3(vPoint));
	}
	else
	{
		// it's a sphere - get distance to the sphere's centre
		Vec3V vCentreToPoint = vPoint - m_vPosWldCentre;
		distSqr = MagSquared(vCentreToPoint).Getf();
	}

	return distSqr;
}

float ptxDataVolume::GetPointStrength(Vec3V_In vPoint, Vec3V_In vDir, bool useTaperedRadius, bool useDistFade, bool useLengthFade, bool useAngleFade)
{
	// cache some data
	bool isSphere = m_length==0.0f;
	float tValue = GetPointTValue(vPoint);
	float dist = Sqrtf(GetPointDistSqr(vPoint));

	// get the radius of the volume at our t value along the length
	float radius = m_radius;
	if (useTaperedRadius)
	{
		radius *= tValue;
	}

	// check if we're within the radius
	if (dist<radius)
	{
		// initialise the strength to full
		float strength = 1.0f;

		// fade strength with distance from the focal point (the sphere centre or capsule line segment)
		if (useDistFade)
		{
			strength *= 1.0f - dist/radius;
		}

		// fade stength with distance along the capsule line segment
		if (useLengthFade)
		{
			if (tValue>0.5f && !isSphere)
			{
				strength *= 1.0f - ((tValue-0.5f)*2.0f);
			}
		}

		// fade strength with angle towards the point at the start of the capsule line segment (or centre of the sphere)
		if (useAngleFade)
		{
			Vec3V vPointToStartPos = m_vPosWldStart - vPoint;
			vPointToStartPos = Normalize(vPointToStartPos);
			strength *= Max(0.0f, Dot(vPointToStartPos, vDir).Getf());
		}

		return strength;
	}
	else
	{
		return 0.0f;
	}
}

bool ptxDataVolume::IsPointInside(Vec3V_In vPoint)
{
	return GetPointDistSqr(vPoint)<=m_radius*m_radius;
}

ptxEffectInst::ptxEffectInst()
{
	// rules
	m_pEffectRule = NULL;

	// event data
	m_numEvents = 0;
	m_eventInsts.Resize(PTXEFFECTINST_MAX_EVENT_INSTS);

	// positional state
	m_vMtxBase = Mat34V(V_IDENTITY);
	m_vMtxOffset = Mat34V(V_IDENTITY);
	m_vMtxFinal = Mat34V(V_IDENTITY);
	m_vPrevPos = Vec3V(V_ZERO);

	// state
	m_currLoop = 0; 
	m_duration = 0.0f;
	m_ooLife = 0.0f;
	m_prevLifeRatio = 0.0f;
	m_finalZoom = 1.0f;
	m_numActivePoints = 0;
	m_cullState = CULLSTATE_NOT_CULLED;
	m_isEmitCulled = false;
	m_isUpdateCulled = false;
	m_isInInterior = false;
	m_playState = PTXEFFECTINSTPLAYSTATE_DEACTIVATED;

	m_ambientScaleSetup = false;
	m_ambientScale[0] = 0.0f;
	m_ambientScale[1] = 0.0f;
	m_artificialAmbientMult = 1.0f;
	m_naturalAmbientMult = 1.0f;

	// settings
	m_pRenderSetup = NULL;
	m_vVelocityAdd = Vec3V(V_ZERO);
	m_vRandOffsetPos = Vec3V(V_ZERO);
	m_vRGBATint = Vec4V(V_ONE);
	m_userZoom = 1.0f;
	m_playbackRateScalar = 1.0f;
	m_lodScalar = 1.0f;
	m_lodAlphaMult = 1.0f;
	m_lodEvoIdx = -1;
	m_hqEvoIdx = -1;
	m_drawListIdx = 0;
	m_zoomScalarRndIdx = 0;
	m_rgbaRndIdx = 0;
	m_trailTexInfoRndIdx = 0;
	m_invertAxes = 0;
	m_canBeCulled = true;
	m_dontEmitThisFrame = false;
	m_canMoveVeryFast = false;
	
	// overrides
	m_spawnedEffectScalars.Reset();
	m_overrideNearClipDist = -1.0f;
	m_overrideFarClipDist = -1.0f;
	m_overrideDistTravelled = -1.0f;
	m_overrideCreationDomainSize = Vec3V(V_ZERO);

	// evolutions
	for (int i=0; i<PTXEVO_MAX_EVOLUTIONS; i++)
	{
		m_evoValues[i] = (ptxEvoValueType)0;
	}

	// collision data
	m_pCollisionSet = &m_internalCollisionSet;
	m_internalCollisionSet.Clear();

	// flags
	m_flags = 0;
	m_internalFlags.Reset();

	// spawned effects
	m_pSpawningPointItem = NULL;
	m_pSpawningEffectInst = NULL;
	m_pEffectSpawner = NULL;

	// misc
	m_pEffectInstItem = NULL;

	m_interiorLocation = 0U;
	m_interiorLocationCached = false;
	m_ignoreDataCapsuleTests = false;

	// double buffered data (no pointer set up yet)
// 	m_pEffectInstItem->GetDataDB()->m_currLifeRatio = 0.0f;
//	m_pEffectInstItem->GetDataDB()->m_distSqrToCamera = 0.0f;

	// debug
#if RMPTFX_BANK
	m_outputPlayStateDebug = false;
	m_hasBeenActivated = false;
#endif
}

ptxEffectInst::~ptxEffectInst()
{
	// rules
	m_pEffectRule = NULL;
	
	// events
	for (int i=m_eventInsts.GetCount()-1; i>=0; i--)
	{
		m_eventInsts.Delete(i);
	}

	m_eventInsts.Reset();
}

void ptxEffectInst::Init(ptxEffectRule* pEffectRule, u32 flags)
{
	// rules
	m_pEffectRule = pEffectRule;

	// events
	for (int i=0; i<m_eventInsts.GetCount(); i++)
	{
		m_eventInsts[i].Init(this);
	}

	// positional state
	m_vMtxBase = Mat34V(V_IDENTITY);
	m_vMtxOffset = Mat34V(V_IDENTITY);
	m_vMtxFinal = Mat34V(V_IDENTITY);
	m_vPrevPos = Vec3V(V_ZERO);

	// state
	m_currLoop = 0; 
	m_duration = 0.0f;
	m_ooLife = 0.0f;
	m_prevLifeRatio = 0.0f;
	m_finalZoom = 1.0f;
	m_numActivePoints = 0;
	m_cullState = CULLSTATE_NOT_CULLED;
	m_isEmitCulled = false;
	m_isUpdateCulled = false;
	m_isInInterior = false;
	ptxAssertf(m_playState==PTXEFFECTINSTPLAYSTATE_DEACTIVATED, "trying to initialise an effect inst from an invalid state");
	SetPlayState(PTXEFFECTINSTPLAYSTATE_WAITING_TO_ACTIVATE);

	m_ambientScale[0] = 0.0f;
	m_ambientScale[1] = 0.0f;

	// settings
	m_pRenderSetup = NULL;
	m_vVelocityAdd = Vec3V(V_ZERO);
	m_vRandOffsetPos = Vec3V(V_ZERO);
	m_vRGBATint = Vec4V(V_ONE);
	m_userZoom = 1.0f;
	m_playbackRateScalar = 1.0f;
	m_lodScalar = 1.0f;
	m_lodAlphaMult = 1.0f;
	m_lodEvoIdx = -1;
	m_hqEvoIdx = -1;
	m_drawListIdx = 0;
	m_zoomScalarRndIdx = 0;
	m_rgbaRndIdx = 0;
	m_trailTexInfoRndIdx = 0;
	m_invertAxes = 0;
	m_canBeCulled = true;
	m_dontEmitThisFrame = false;
	m_canMoveVeryFast = false;
	m_ambientScaleSetup = false;
	m_artificialAmbientMult = 1.0f;
	m_naturalAmbientMult = 1.0f;

	// overrides
	m_spawnedEffectScalars.Reset();
	m_overrideNearClipDist = -1.0f;
	m_overrideFarClipDist = -1.0f;
	m_overrideDistTravelled = -1.0f;
	m_overrideCreationDomainSize = Vec3V(V_ZERO);

	//skip occlusion checks
	m_skipOcclusionTestFrameCount = 0;

	// evolutions
	for (int i=0; i<PTXEVO_MAX_EVOLUTIONS; i++)
	{
		m_evoValues[i] = (ptxEvoValueType)0;
	}

	// collision data
	m_pCollisionSet = &m_internalCollisionSet;
	m_internalCollisionSet.Clear();

	// flags
	m_flags = flags;
	m_internalFlags.Reset();
	
	// spawned effects
	m_pSpawningPointItem = NULL;
	m_pSpawningEffectInst = NULL;
	m_pEffectSpawner = NULL;

	// misc
	ptxAssertf(m_pEffectInstItem, "trying to init an effect inst without an effect inst item");
	m_interiorLocation = 0U;
	m_interiorLocationCached = false;
	m_ignoreDataCapsuleTests = false;
	m_cameraBias = 0.0f;

	// double buffered data
	m_pEffectInstItem->GetDataDB()->m_currLifeRatio = 0.0f;
	m_pEffectInstItem->GetDataDB()->m_distSqrToCamera = 0.0f;
	m_pEffectInstItem->GetDataDB()->m_customFlags = 0;
	m_pEffectInstItem->GetDataDB()->m_isDrawCulled = false;

	// debug
#if RMPTFX_BANK
	m_outputPlayStateDebug = ShouldOutputPlayStateInfo();
	OutputPlayStateFunction("Init");
#endif

	// override data if there is an effect rule
	if (m_pEffectRule)
	{
		// events
		m_numEvents = m_pEffectRule->GetTimeline().GetNumEvents();
		ptxAssertf(m_numEvents <= PTXEFFECTINST_MAX_EVENT_INSTS, "%s has too many events - maximum is %u", m_pEffectRule->GetName(), PTXEFFECTINST_MAX_EVENT_INSTS);

		for (int i=0; i<m_numEvents; i++)
		{
			if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
			{
				m_eventInsts[i].GetEmitterInst()->GetPointList().ClearBuffers();
			}
		}

		// settings
#if RMPTFX_FORCE_DRAW_BUCKET>=0
		m_drawListIdx = RMPTFX_FORCE_DRAW_BUCKET;
#else
		m_drawListIdx = m_pEffectRule->GetDrawListIdx();
#endif
	}

	if(RMPTFXMGR.GetEffectInstInitFunctor())
	{
		RMPTFXMGR.GetEffectInstInitFunctor()(this);
	}

	// copy the double buffered data to the multithreaded update buffer
	CopyToUpdateBuffer();
}
			
void ptxEffectInst::ComputeCallbackUpdateFlag()
{
	bool usesCallbacks = false;

	if (RMPTFXMGR.GetEffectInstPostUpdateFunctor() || RMPTFXMGR.GetEffectInstPreUpdateFunctor())
	{
		usesCallbacks = true;
	}
	else
	{
		m_numEvents = m_pEffectRule->GetTimeline().GetNumEvents();
		ptxAssertf(m_numEvents <= PTXEFFECTINST_MAX_EVENT_INSTS, "%s has too many events - maximum is %u", m_pEffectRule->GetName(), PTXEFFECTINST_MAX_EVENT_INSTS);

		for (int i=0; i<m_numEvents; i++)
		{
			if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
			{
				ptxEventEmitter* emitterEvent = smart_cast<ptxEventEmitter*>(m_pEffectRule->GetTimeline().GetEvent(i));
				ptxParticleRule* rule = emitterEvent->GetParticleRule();
				if (rule->GetEffectSpawnerAtRatio().GetEffectRule()  ||
					rule->GetEffectSpawnerOnColn().GetEffectRule())
				{
					usesCallbacks = true;
				}
			}
			else
			{
				usesCallbacks = true;
			}
		}
	}

	m_internalFlags.Set(PTXEFFECTFLAG_CALLBACK_UPDATE, usesCallbacks);
}

void ptxEffectInst::Start()
{
	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());

	ptxAssertf(m_pEffectRule, "trying to start an effect inst without an effect rule");

#if RMPTFX_BANK
	OutputPlayStateFunction("Start");
#endif

	// we should only be starting effects for the first time or restarting them
	ptxAssertf(m_playState==PTXEFFECTINSTPLAYSTATE_WAITING_TO_ACTIVATE ||																	// starting for the first time
			   m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING ||																				// restarting an already playing
			   m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_MANUALLY ||																		// restarting a stopped effect
			   m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_NATURALLY ||																		// restarting a stopped effect
			   m_playState==PTXEFFECTINSTPLAYSTATE_FINISHED, "trying to start an effect inst with an invalid state (%d)", m_playState);		// restarting a finished effect

	// call any callbacks
	if (RMPTFXMGR.GetEffectInstStartFunctor())
	{
		RMPTFXMGR.GetEffectInstStartFunctor()(this);
	}

	// activate the effect if it's being started for the first time (otherwise, it must be being restarted)
	if (m_playState==PTXEFFECTINSTPLAYSTATE_WAITING_TO_ACTIVATE)
	{
		RMPTFXMGR.ActivateEffectInst(this);
	}

	// set the event data
	for (int i=0; i<m_eventInsts.GetCount(); i++)
	{
		m_eventInsts[i].SetNumTriggers(0);
	}

	// store the previous position
	m_vPrevPos = GetCurrPos();

	// set the new position state
	UpdateFinalMtx();

	// set the state
	m_currLoop = 0;
	m_duration = m_pEffectRule->GetRandDuration();
	m_ooLife = 1.0f/m_duration;
	m_prevLifeRatio = 0.0f;

	m_ambientScaleSetup = false;

	// we're now playing
	SetPlayState(PTXEFFECTINSTPLAYSTATE_PLAYING);

	// set the settings
	m_vRandOffsetPos = g_ptxVecRandom.GetSignedRangedVec3V(m_pEffectRule->GetRandOffsetPos());
	m_playbackRateScalar = m_pEffectRule->GetRandPlaybackRateScalar();
	m_lodEvoIdx = m_pEffectRule->GetEvolutionList() ? (s8)m_pEffectRule->GetEvolutionList()->GetEvolutionIndexFromHash(ATSTRINGHASH("LOD", 0x2FBE6388)) : -1;
	m_hqEvoIdx = m_pEffectRule->GetEvolutionList() ? (s8)m_pEffectRule->GetEvolutionList()->GetEvolutionIndexFromHash(ATSTRINGHASH("HQ", 0x671558BE)) : -1;
	m_zoomScalarRndIdx = ptxRandomTable::GetCurrIndex();
	m_rgbaRndIdx = ptxRandomTable::GetCurrIndex();
	m_trailTexInfoRndIdx = ptxRandomTable::GetCurrIndex();

	// set the double buffered data
	GetDataDB()->m_currLifeRatio = 0.0f;

	// apply any overrides
	if (m_spawnedEffectScalars.GetIsDurationScalarActive())
	{
		m_duration *= m_spawnedEffectScalars.GetDurationScalar();
		m_ooLife = 1.0f/m_duration;
	}

	if (m_spawnedEffectScalars.GetIsPlaybackRateScalarActive())
	{
		m_playbackRateScalar *= m_spawnedEffectScalars.GetPlaybackRateScalar();
	}

	// apply any pre update
	float preUpdateTime = m_pEffectRule->GetPreUpdateTime();
	float preUpdateTimeInterval = m_pEffectRule->GetPreUpdateTimeInterval();
#if RMPTFX_BANK
	if (ptxDebug::sm_preUpdateTimeLimitActive)
	{
		preUpdateTime = Min(preUpdateTime, ptxDebug::sm_preUpdateTimeLimit);
	}
#endif
	if (preUpdateTime>0.0f)
	{	
		float currPreUpdateTime = 0.0f;
		while (currPreUpdateTime<preUpdateTime && m_playState!=PTXEFFECTINSTPLAYSTATE_FINISHED)
		{
			float deltaTime = Min(preUpdateTime-currPreUpdateTime, preUpdateTimeInterval);

			UpdateSetup(deltaTime, NULL ASSERT_ONLY(, true));
			UpdateParallel(deltaTime);
			UpdateFinalize(deltaTime);

			currPreUpdateTime += deltaTime;
		}
	}

	// copy the double buffered data to the multithreaded update buffer
	CopyToUpdateBuffer();

	ComputeCallbackUpdateFlag();

	// check for effects with no events 
#if RMPTFX_EDITOR
	if (m_pEffectRule->GetTimeline().GetNumEvents()==0)
	{
		ptxMsg::AssetErrorf("(%s) has no events - stopping the effect", m_pEffectRule->GetName());
		Finish(false);
		return;
	}
#endif
}

void ptxEffectInst::Loop()
{
	// calculate new random duration
	m_duration = m_pEffectRule->GetRandDuration();
	m_ooLife = 1.0f/m_duration;

	// calculate new random offset position
	m_vRandOffsetPos = g_ptxVecRandom.GetSignedRangedVec3V(m_pEffectRule->GetRandOffsetPos());
}

void ptxEffectInst::Stop()
{
	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());
	ptxAssertf(m_pEffectRule, "trying to stop an effect inst without an effect rule");

#if RMPTFX_BANK
	OutputPlayStateFunction("Stop");

	if (PARAM_rmptfxPrintCallstackWhenStoppingEffect.Get())
	{
		auto printLine = [](size_t addr, const char *sym, size_t offset) {
			ptxDebugf1( "%8" SIZETFMT "x - %s+%" SIZETFMT "x", addr, sym, offset );
		};
		ptxDebugf1("Effect %p (%s) is being stopped, callstask:", this, m_pEffectRule->GetName());
		sysStack::PrintStackTrace(printLine);
	}
#endif

	// we should only be stopping effects that are currently playing
	ptxAssertf(m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING, "trying to stop an effect inst that isn't playing");

	// call any callbacks
	if (RMPTFXMGR.GetEffectInstStopFunctor())
	{
		RMPTFXMGR.GetEffectInstStopFunctor()(this);
	}

	// stop all the events 
	for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
	{
		ptxEventInst* pEventInst = &m_eventInsts[i];
		m_pEffectRule->GetTimeline().GetEvent(i)->Stop(this, pEventInst);
	}

	// being made inactive - decrement the count (so long as it isn't 0 already)
	if (m_pEffectRule->GetNumActiveInstances()>0)
	{
		m_pEffectRule->DecNumActiveInstances();
	}	

	// we're now stopped
	SetPlayState(PTXEFFECTINSTPLAYSTATE_STOPPED_MANUALLY);
}	

void ptxEffectInst::Finish(bool mustDeactivateNow)
{
	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());

	ptxAssertf(m_pEffectRule, "trying to finish an effect inst without an effect rule");

#if RMPTFX_BANK
	OutputPlayStateFunction("Finish");
#endif

	// we should only be finishing effects that are playing, stopped or finished
	ptxAssertf(m_playState==PTXEFFECTINSTPLAYSTATE_WAITING_TO_ACTIVATE ||
			   m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING ||
			   m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_MANUALLY ||
			   m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_NATURALLY ||
			   m_playState==PTXEFFECTINSTPLAYSTATE_FINISHED, "trying to finish an effect inst with an invalid state (%d)", m_playState);

	// start the effect if it's currently waiting to activate
	if (m_playState==PTXEFFECTINSTPLAYSTATE_WAITING_TO_ACTIVATE)
	{
		Start();
	}

	// stop the effect if it's currently playing
	if (m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING)
	{
		Stop();
	}

	// finish all the events
	for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
	{
		ptxEventInst* pEventInst = &m_eventInsts[i];
		m_pEffectRule->GetTimeline().GetEvent(i)->Finish(this, pEventInst);
	}

	// we're now finished
	SetPlayState(PTXEFFECTINSTPLAYSTATE_FINISHED);

	// deactivate the effect inst
	if (mustDeactivateNow)
	{
		ptxAssertf(GetFlag(PTXEFFECTFLAG_KEEP_RESERVED)==false, "trying to deactivate an effect inst that is still reserved (%s)", GetEffectRule()->GetName());
		RMPTFXMGR.DeactivateEffectInst(this);
	}
	else
	{
		// only deactivate the effect inst if it's ok to do so - if not it will get deactivated during a subsequent update (when it is safe to do so)
		if (GetFlag(PTXEFFECTFLAG_KEEP_RESERVED)==false)
		{
			RMPTFXMGR.DeactivateEffectInst(this);
		}
	}
}
			 
void ptxEffectInst::Relocate()
{
	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());

	ptxAssertf(m_pEffectRule, "trying to relocate an effect inst without an effect rule");

	// cache the current position
	Vec3V vCurrPos = m_vMtxFinal.GetCol3();

	// calculate how far the effect has moved
	Vec3V vDeltaPos = vCurrPos - m_vPrevPos;

	// relocate all the events
	for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
	{
		ptxEventInst* pEventInst = &m_eventInsts[i];
		m_pEffectRule->GetTimeline().GetEvent(i)->Relocate(this, pEventInst, vDeltaPos);
	}

	// set the previous position to be the same as the current one
	m_vPrevPos = vCurrPos;
}	 

void ptxEffectInst::Deactivate()
{
	if (RMPTFXMGR.GetEffectInstDeactivateFunctor())
	{
		RMPTFXMGR.GetEffectInstDeactivateFunctor()(this);
	}
}

void ptxEffectInst::UnReference()
{
	ptxAssertf(m_pEffectRule, "trying to unreference an effect inst without an effect rule");

	// release a reference on the effect rule
	m_pEffectRule->Release();	

	// release any references on the fx list 
	if (m_pEffectRule->GetFxList())
	{
		m_pEffectRule->GetFxList()->Release();
		ptxAssertf(m_pEffectRule->GetFxList()->GetRefCount()>=0, "fx list has an invalid reference count (%s - %s)", m_pEffectRule->GetFxList()->GetName(), m_pEffectRule->GetName());
#if __ASSERT
		if (m_pEffectRule->GetFxList()->GetRefCount()==0)
		{
			// the fx list has no references - therefore the effect rule should only have one (until it is nulled later in the function)
			ptxAssertf(m_pEffectRule->GetRefCount()==1, "fx list %s has no references but effect rule %s still has some (%d)", m_pEffectRule->GetFxList()->GetName(), m_pEffectRule->GetName(), m_pEffectRule->GetRefCount());
		}
#endif
	}

	// clear all emitter and particle rule pointers 
	for (int i=0; i<m_eventInsts.GetCount(); i++)
	{
		m_eventInsts[i].GetEmitterInst()->SetEmitterRule(NULL);
		m_eventInsts[i].GetEmitterInst()->SetParticleRule(NULL);
	}

	// clear the effect rule pointer
	m_pEffectRule = NULL;

	m_pEffectSpawner = NULL;
}	

void ptxEffectInst::UpdateSetup(float dt, const grcViewport* pViewport ASSERT_ONLY(, bool bPerformingPreUpdate))
{
	ptxAssertf(m_pEffectRule, "trying to update an effect inst without an effect rule");

	// call any callbacks
	if (RMPTFXMGR.GetEffectInstPreUpdateFunctor())
	{
		RMPTFXMGR.GetEffectInstPreUpdateFunctor()(this);
	}

	// increment the number of effect instances
	PF_INCREMENT(CounterEffectInsts);

#if RMPTFX_EDITOR
	// update the number of events on the effect 
	m_numEvents = m_pEffectRule->GetTimeline().GetNumEvents();
#endif

#if RMPTFX_EDITOR || RMPTFX_TIMERS
	// start a timer
	sysTimer timer;
	timer.Reset();
#endif

	// do any update required if we have been spawned by a point
	if (IsSpawnedEffect())
	{
		UpdateSpawnedEffect(dt);
	}

	// store the previous position
	m_vPrevPos = GetCurrPos();

	// make sure the final matrix is up to date
	UpdateFinalMtx();

	// check if this effect needs relocating
	if (m_internalFlags.IsSet(PTXEFFECTFLAG_NEEDS_RELOCATED))
	{
		Relocate();
	}

	// prepare the evolution data
	m_pEffectRule->PrepareEvoData();

	// calc the effect's final zoom scalar (must be done before the viewport culling)
	ScalarV vDiv100 = ScalarV(V_FLT_SMALL_2);
	ScalarV vKeyTimeEffect = GetKeyTime();
	Vec4V vZoomScalarMinMax = m_pEffectRule->GetZoomScalarKFP().Query(vKeyTimeEffect, m_pEffectRule->GetEvolutionList(), m_evoValues);
	ScalarV vZoomScalar = Lerp(ptxRandomTable::GetValueV(m_zoomScalarRndIdx), SplatX(vZoomScalarMinMax), SplatY(vZoomScalarMinMax)) * vDiv100;
	ScalarV vZoomScalarFinal = vZoomScalar * ScalarVFromF32(m_userZoom);
#if RMPTFX_EDITOR
	vZoomScalarFinal *= ScalarVFromF32(m_pEffectRule->GetZoomableComponentScalar()) * vDiv100;
#endif
	if (m_spawnedEffectScalars.GetIsZoomScalarActive())
	{
		vZoomScalarFinal *= ScalarVFromF32(m_spawnedEffectScalars.GetZoomScalar());
	}
	m_finalZoom = vZoomScalarFinal.Getf();

	// update effect culling
	UpdateCulling(pViewport);

	if(RMPTFXMGR.GetEffectInstUpdateSetupFunctor())
	{
		RMPTFXMGR.GetEffectInstUpdateSetupFunctor()(this);
	}
	// deal with effects that shouldn't update due to culling
	if (m_isUpdateCulled)
	{
		// go through the emitter events
		for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
		{
			if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
			{
				ptxEventInst* pEventInst = &m_eventInsts[i];

				// cull update emitter events
				m_pEffectRule->GetTimeline().GetEvent(i)->UpdateCulled(this, pEventInst);
			}
		}

		// update the ui data
#if RMPTFX_EDITOR
		m_pEffectRule->GetUIData().IncNumActiveInstances(1);
		m_pEffectRule->GetUIData().IncNumCulledInstances(1);
		m_pEffectRule->GetUIData().IncAccumCPUUpdateTime(timer.GetMsTime());
#endif 

		// output profile data
#if RMPTFX_TIMERS
		OutputUpdateProfileInfo("UpdateSetup", timer);
#endif

		return;
	}

	// override the given evolution values with those from the editor, if required
#if RMPTFX_EDITOR
	ptxEvolutionList* pEvoList = m_pEffectRule->GetEvolutionList();
	if (pEvoList)
	{
		for (int i=0; i<pEvoList->GetNumEvolutions(); i++)
		{
			if (pEvoList->GetEvolutionIsOverriden(i))
			{
				m_evoValues[i] = PackEvoValue(pEvoList->GetEvolutionOverrideValue(i));
			}
		}
	}
#endif

	// set the lod evolution based on the distance to the camera
	if (m_lodEvoIdx>=0 && m_pEffectRule->GetLodEvoDistMax()>0.0f)
	{
		float distToCamera = Sqrtf(GetDataDB()->m_distSqrToCamera);
		m_evoValues[m_lodEvoIdx] = PackEvoValue(ClampRange(distToCamera, m_pEffectRule->GetLodEvoDistMin(), m_pEffectRule->GetLodEvoDistMax()));
	}

	// set the hq evolution
	if (m_hqEvoIdx>=0)
	{
#if RMPTFX_BANK
		if (ptxDebug::sm_overrideHighQualityEvo>=0.0f)
		{
			m_evoValues[m_hqEvoIdx] = PackEvoValue(ptxDebug::sm_overrideHighQualityEvo);
		}
		else
#endif
		{
			m_evoValues[m_hqEvoIdx] = PackEvoValue(RMPTFXMGR.GetHighQualityEvo());
		}
	}

	// update the life ratio if we're playing
	if (m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING)
	{
		if (GetDataDB()->m_currLifeRatio<1.0f)
		{
			// store the previous life ratio
			m_prevLifeRatio = GetDataDB()->m_currLifeRatio;

			// calculate the new life ratio
			GetDataDB()->m_currLifeRatio += dt*m_ooLife; 

			// check if the effect has finished
			bool loopedThisFrame = false;
			if (GetDataDB()->m_currLifeRatio>=1.0f) 
			{
				// check looping mode
				if ((m_pEffectRule->GetNumLoops()<0 || m_currLoop<m_pEffectRule->GetNumLoops()))
				{
					// loop the effect
					int numLoopsDone = (int)GetDataDB()->m_currLifeRatio;
					GetDataDB()->m_currLifeRatio -= numLoopsDone;
					m_currLoop += numLoopsDone;

					Loop();
					
					loopedThisFrame = true;
#if __ASSERT
					if(!bPerformingPreUpdate)
					{
						ptxAssertf(GetDataDB()->m_currLifeRatio < m_prevLifeRatio, "looped effect (%s) has life ratio issues", m_pEffectRule->GetName());
					}
#endif //__ASSERT
				}
			}

			// check for any events that need started
			// NOTE: events must be started before updating the active events to keep things in sync
			for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
			{	
				ptxEventInst* pEventInst = &m_eventInsts[i];

				// check if the event has triggered this frame
				float startRatio = m_pEffectRule->GetTimeline().GetEvent(i)->GetStartRatio();

				if (loopedThisFrame)
				{
					// check if triggered between previous life ratio and 1.0 or between 0.0 and current life ratio
					if ((startRatio >= m_prevLifeRatio && startRatio <= 1.0f) ||
						(startRatio >= 0.0f && startRatio <= GetDataDB()->m_currLifeRatio))
					{
						m_pEffectRule->GetTimeline().GetEvent(i)->Trigger(this, pEventInst);
					}
				}
				else
				{
					// check if triggered between previous life ratio and current life ratio
					if (startRatio >= m_prevLifeRatio && startRatio <= GetDataDB()->m_currLifeRatio)
					{
						m_pEffectRule->GetTimeline().GetEvent(i)->Trigger(this, pEventInst);
					}
				}
			}
		}
	}

	// update emitter events
	for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
	{
		if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
		{
			ptxEventInst* pEventInst = &m_eventInsts[i];

			// pre update emitter events
			m_pEffectRule->GetTimeline().GetEvent(i)->UpdateSetup(this, pEventInst, dt);

			// add an update item to the task list
			ptxEmitterInstUpdateItem emitterInstUpdateItem;
			emitterInstUpdateItem.m_pEffectInstItem = GetEffectInstItem();
			emitterInstUpdateItem.m_eventIdx = i;

			RMPTFXMGR.AddEmitterInstUpdateItem(emitterInstUpdateItem);
		}
		// Do we want non-emitter events added to the update item list too?
	}

	// update the ui data
#if RMPTFX_EDITOR
	m_pEffectRule->GetUIData().IncNumActiveInstances(1);
	m_pEffectRule->GetUIData().IncNumCulledInstances(1);
	m_pEffectRule->GetUIData().IncAccumCPUUpdateTime(timer.GetMsTime());
#endif 

	// output profile data
#if RMPTFX_TIMERS
	OutputUpdateProfileInfo("UpdateSetup", timer);
#endif
}

void ptxEffectInst::UpdateParallel(float dt)
{
	ptxAssertf(m_pEffectRule, "trying to update an effect inst without an effect rule");

#if RMPTFX_TIMERS
	// start a timer
	sysTimer timer;
	timer.Reset();
#endif

	if (m_isUpdateCulled)
	{
		return;
	}
	

	// prepare the evolution data
	m_pEffectRule->PrepareEvoData();

	// update emitter events
	for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
	{
		if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
		{
			ptxEventInst* pEventInst = &m_eventInsts[i];

			// parallel update emitter events
			m_pEffectRule->GetTimeline().GetEvent(i)->UpdateParallel(this, pEventInst, dt);
		}
	}

	// output profile data
#if RMPTFX_TIMERS
	OutputUpdateProfileInfo("UpdateParallel", timer);
#endif
}

void ptxEffectInst::UpdateFinalize(float dt)
{
	ptxAssertf(m_pEffectRule, "trying to update an effect inst without an effect rule");

#if RMPTFX_TIMERS
	// start a timer
	sysTimer timer;
	timer.Reset();
#endif

	// clear the relocation flag
	if (m_internalFlags.IsSet(PTXEFFECTFLAG_NEEDS_RELOCATED))
	{
		m_internalFlags.Clear(PTXEFFECTFLAG_NEEDS_RELOCATED);
	}

	// skip any effects that shouldn't update due to culling
	if (m_isUpdateCulled)
	{
		return;
	}

	// update emitter events keeping a count of how many are active
	int numActiveEvents = 0;
	for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
	{
		if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
		{
			ptxEventInst* pEventInst = &m_eventInsts[i];

			// post update emitter events
			bool isEventInactive = m_pEffectRule->GetTimeline().GetEvent(i)->UpdateFinalize(this, pEventInst, dt);
			if (isEventInactive==false)
			{
				numActiveEvents++;
			}

			// stop any emitter events if the effect is finished
			if (GetDataDB()->m_currLifeRatio>=1.0f) 
			{
				m_pEffectRule->GetTimeline().GetEvent(i)->Stop(this, pEventInst);
			}
		}
	}

	// update the play state
	UpdatePlayState(numActiveEvents);

	// update some stats
	if (m_playState!=PTXEFFECTINSTPLAYSTATE_FINISHED)
	{
		m_numActivePoints = CalcNumActivePoints();

#if RMPTFX_EDITOR
		PF_INCREMENT(CounterEffectInstsActive);
		m_pEffectRule->GetUIData().IncNumActiveInstances(1);
		m_pEffectRule->GetUIData().IncNumPointsUpdated(m_numActivePoints);
#endif
	}

	// reset the flag that stopped emission this frame
	m_dontEmitThisFrame = false;

	// call the callback
	if (RMPTFXMGR.GetEffectInstPostUpdateFunctor())
	{
		RMPTFXMGR.GetEffectInstPostUpdateFunctor()(this);
	}

	// output profile data
#if RMPTFX_TIMERS
	OutputUpdateProfileInfo("UpdateFinalize", timer);
#endif
}

void ptxEffectInst::UpdatePlayState(int numActiveEvents)
{
	// we should only be updating effects that are playing, stopped or finished
	ptxAssertf(m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING ||
			   m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_MANUALLY ||
			   m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_NATURALLY ||
			   m_playState==PTXEFFECTINSTPLAYSTATE_FINISHED, "trying to update an effect inst with an invalid state (%d)", m_playState);

	// check for playing effects that have played out
	if (m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING)
	{
		// check if we've stopped naturally
		if (GetDataDB()->m_currLifeRatio>=1.0f)
		{
			// we're now stopped
#if RMPTFX_BANK
			OutputPlayStateFunction("UpdatePlayState (stopped naturally)");
#endif
			SetPlayState(PTXEFFECTINSTPLAYSTATE_STOPPED_NATURALLY);

			// being made inactive - decrement the count (so long as it isn't 0 already)
			if (m_pEffectRule->GetNumActiveInstances()>0)
			{
				m_pEffectRule->DecNumActiveInstances();
			}	
		}
	}

	// check for stopped effects that are finished
	if ((m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_MANUALLY || m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_NATURALLY) && numActiveEvents==0)
	{
#if RMPTFX_BANK
		OutputPlayStateFunction("UpdatePlayState (finished)");
#endif
		SetPlayState(PTXEFFECTINSTPLAYSTATE_FINISHED);
	}

	// check for finished effects that need deactivated
	if (m_playState==PTXEFFECTINSTPLAYSTATE_FINISHED && GetFlag(PTXEFFECTFLAG_KEEP_RESERVED)==false)  
	{
#if RMPTFX_BANK
		OutputPlayStateFunction("UpdatePlayState (deactivated)");
#endif
		RMPTFXMGR.DeactivateEffectInst(this);
	}
}

void ptxEffectInst::Draw(PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface))
{
#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif 

	// call any callbacks
	if (RMPTFXMGR.GetEffectInstPreDrawFunctor())
	{
		RMPTFXMGR.GetEffectInstPreDrawFunctor()(this);
	}

	const unsigned drawBufferId = RMPTFXTHREADER.GetDrawBufferId();
#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH
	if (pMultipleDrawInterface)
	{
		//First test if this effect Inst should be rendered
		if(!pMultipleDrawInterface->ShouldBeRendered(this))
		{
			return;
		}
		bool allCulled = true;
		u32 numActiveCascades = 0;
		for(u32 i=0; i<pMultipleDrawInterface->NoOfDraws(); i++)
		{
			bool culled = CalcCullState(pMultipleDrawInterface->GetViewport(i), false) != CULLSTATE_NOT_CULLED;
			pMultipleDrawInterface->SetCulledState(culled,i);
			if (!culled)
			{
				allCulled = false;
				numActiveCascades++;
			}
		}
		pMultipleDrawInterface->SetNumActiveCascades((u8)numActiveCascades);

		if (allCulled)
		{
			return;
		}
	}
	else
#endif
	{
		//If we're not using the interface then used the cached main viewport cull state
		// return early if draw culled
		if (GetDataDBMT(drawBufferId)->GetIsDrawCulled())
		{
#if RMPTFX_EDITOR
			m_pEffectRule->GetUIData().IncNumCulledInstances(1);
#endif
			return;
		}

	}

	// debug point quads - bind
#if RMPTFX_EDITOR
	if (m_pEffectRule->GetUIData().GetShowPointQuads())
	{
#if RMPTFX_BANK
		ptxShaderTemplateOverride::BindDebugTechniqueOverride(PTXSHADERDEBUG_ALPHA);
#endif
	}
#endif

	// draw
	if (m_pEffectRule->GetSortEventsByDistance())
	{
		// get the camera position
		Vec3V vCamPos;
#if RMPTFX_USE_PARTICLE_SHADOWS
		if (RMPTFXMGR.IsShadowPass())
			vCamPos = RMPTFXMGR.GetShadowSourcePos();
		else
#endif
			vCamPos = grcViewport::GetCurrent()->GetCameraPosition();

		struct KeyData
		{
			float         key;
			ptxEventInst* eventInst;
			ptxEvent*     event;

			inline bool operator<(const KeyData& rhs) const
			{
				return rhs.key < key;
			}
		};
		KeyData pSortBuffer[PTXTIMELINE_MAX_CHANNELS];

		// go through the emitter events
		int numEmitterEvents = 0;
		for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
		{
			if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
			{
				ptxParticleRule* pParticleRule = ((ptxEventEmitter*)m_pEffectRule->GetTimeline().GetEvent(i))->GetParticleRule();
				if(pParticleRule && pParticleRule->ShouldBeRendered())
				{
					// calc the squared distance to the camera and add to the sort buffer
					ptxEventInst* pEventInst = &m_eventInsts[i];
					pSortBuffer[numEmitterEvents].key = DistSquared(vCamPos, pEventInst->GetEmitterInst()->GetDataDBMT(drawBufferId)->GetCurrPosWld()).Getf();
					pSortBuffer[numEmitterEvents].eventInst = pEventInst;
					pSortBuffer[numEmitterEvents].event = m_pEffectRule->GetTimeline().GetEvent(i);
					++numEmitterEvents;
				}
			}
		}

		// do the sort
		std::sort(pSortBuffer, pSortBuffer+numEmitterEvents);

		// draw the sorted emitter events
		for (int i=0; i<numEmitterEvents; i++)
		{
			ptxEventInst* pEventInst = pSortBuffer[i].eventInst;
			pSortBuffer[i].event->Draw(this, pEventInst  PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
		}
	}
	else
	{

		// draw emitter events
		for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
		{
			if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
			{	
				ptxParticleRule* pParticleRule = ((ptxEventEmitter*)m_pEffectRule->GetTimeline().GetEvent(i))->GetParticleRule();
				if(pParticleRule && pParticleRule->ShouldBeRendered())
				{
					ptxEventInst* pEventInst = &m_eventInsts[i];
					m_pEffectRule->GetTimeline().GetEvent(i)->Draw(this, pEventInst PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
				}
			}
		}
	}

	// call any callbacks
	if (RMPTFXMGR.GetEffectInstPostDrawFunctor())
	{
		RMPTFXMGR.GetEffectInstPostDrawFunctor()(this);
	}

	// debug point quads - unbind
#if RMPTFX_EDITOR
	if (m_pEffectRule->GetUIData().GetShowPointQuads())
	{
#if RMPTFX_BANK
		ptxShaderTemplateOverride::UnbindDebugTechniqueOverride();
#endif
	}
#endif

	// update ui data
#if RMPTFX_EDITOR
	m_pEffectRule->GetUIData().IncAccumCPUDrawTime(timer.GetMsTime());
#endif 
}

void ptxEffectInst::AllocateMemory()
{
	for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
	{
		if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
		{	
			ptxEventEmitter* pEmitterEvent = static_cast<ptxEventEmitter*>(m_pEffectRule->GetTimeline().GetEvent(i));

			ptxEventInst* pEventInst = &m_eventInsts[i];
			pEventInst->GetEmitterInst()->AllocateMemory(pEmitterEvent->GetEmitterRule());
		}	
	}
}

bool ptxEffectInst::ShouldBeRendered(bool bSkipManualDraw)
{
	if(RMPTFXMGR.GetEffectInstShouldBeRenderedFunctor())
	{
		return RMPTFXMGR.GetEffectInstShouldBeRenderedFunctor()(this, bSkipManualDraw);
	}

	return true;
}

void ptxEffectInst::DeallocateMemory()
{
	for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
	{
		if (m_pEffectRule->GetTimeline().GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
		{	
			ptxEventInst* pEventInst = &m_eventInsts[i];
			pEventInst->GetEmitterInst()->DeallocateMemory();
		}	
	}
}

void ptxEffectInst::UpdateSpawnedEffect(float dt)
{
	// cache the single and double buffered point data
	ptxPointSB* pPointSB = m_pSpawningPointItem->GetDataSB();
	ptxPointDB* pPointDB = m_pSpawningPointItem->GetDataDB();

	// store the current base matrix
	Mat34V vMtx = m_vMtxBase;

	// check if we should be tracking the point's direction
	if (m_pEffectSpawner->GetTracksPointDir() || m_pEffectSpawner->GetTracksPointNegDir())
	{
		// init the forward and up vectors
		Vec3V vForward = Vec3V(V_X_AXIS_WZERO);
		Vec3V vUp = Vec3V(V_Z_AXIS_WZERO);

		// update the forward vector to the point's velocity
		Vec3V vCurrPointVel = pPointDB->GetCurrVel();
		float currPointSpeed = Mag(vCurrPointVel).Getf();
		//if (ptxVerifyf(currPointSpeed>0.000001f, "trying to track direction on a particle with low/zero velocity (%s) (%s)", m_pEffectRule->GetName(), m_pEffectSpawner->GetEffectRule()->GetName()))
		if (currPointSpeed>0.000001f)
		{
			vForward = Normalize(vCurrPointVel);
		}

		// change the up vector if forward is too close to it 
		ScalarV vDot = Abs(Dot(vForward, vUp));
		if (IsGreaterThanOrEqualAll(vDot, ScalarV(0.995f)))
		{
			vUp = Vec3V(V_Y_AXIS_WZERO);
		}

		// deal with negative directions
		if (m_pEffectSpawner->GetTracksPointNegDir())
		{
			vForward = -vForward;
			vUp = -vUp;
		}

		// create the matrix from the vectors
		LookDown(vMtx, vForward, vUp);
	}

	// check if we should be tracking the point's position
	bool parentEffectRelocated = false;
	if (m_pEffectSpawner->GetTracksPointPos())
	{
		// if the effect inst that spawned us needs relocated then so do we
		// this, however, is nasty as we don't know if the parent effect has been updated this frame yet
		// if it has been then we could just set the relocate flag on the child effect and everything would be good
		// one way to do this would be to change the sorting on the effect insts so that all child effects were at the end
		// however, this would also affect draw order and make things look bad
		// so, as a quick fix, let's just pretend that the parent particle is dead
		if (m_pSpawningEffectInst->m_internalFlags.IsSet(PTXEFFECTFLAG_NEEDS_RELOCATED))
		{
			parentEffectRelocated = true;
		}
		else
		{
			// set our position to be the same as the point's position
			Vec3V vCurrPointPos = pPointDB->GetCurrPos();

			// the point's position hasn't been updated yet this frame so approximate where it will be
			vCurrPointPos += pPointDB->GetCurrVel() * ScalarVFromF32(dt) * ScalarVFromF32(pPointSB->GetPlaybackRate());
			vMtx.SetCol3(vCurrPointPos);
		}
	}

	// set the new base matrix
	SetBaseMtx(vMtx);

	// check if the point is dead
	if (pPointDB->GetIsDead() || parentEffectRelocated)
	{
		// it is - reset our spawn pointers
		m_pSpawningPointItem = NULL;
		m_pSpawningEffectInst = NULL;
		m_pEffectSpawner = NULL;
	}
}

void ptxEffectInst::UpdateLODAlphaMultipler()
{
	// calc the lod alpha multiplier (relies on the cull state being calculated first)
	if (m_canBeCulled && m_pEffectRule->GetDistanceCullingMode()>0 && m_pEffectRule->GetDistanceCullingCullDist()>0.0f)
	{
		float cullDist = m_pEffectRule->GetDistanceCullingCullDist() * m_lodScalar;
		float fadeDist = m_pEffectRule->GetDistanceCullingFadeDist() * m_lodScalar;
		m_lodAlphaMult = 1.0f - ClampRange(sqrtf(GetDataDB()->m_distSqrToCamera), fadeDist, cullDist);
	}

}

void ptxEffectInst::UpdateCulling(const grcViewport* pViewport)
{
	// initialise the culling state
	m_cullState = CULLSTATE_NOT_CULLED;
	m_isEmitCulled = false;
	m_isUpdateCulled = false;
	GetDataDB()->m_isDrawCulled = false;

	// don't try to cull if we've finished
	if (m_playState==PTXEFFECTINSTPLAYSTATE_FINISHED)
	{
		return;
	}

	// call any game side culling
	if(RMPTFXMGR.GetEffectInstUpdateCullingFunctor())
	{
		RMPTFXMGR.GetEffectInstUpdateCullingFunctor() (this);
		return;
	}

	// calc the cull state
	m_cullState = CalcCullState(pViewport, true);

	// calc the lod alpha multiplier (relies on the cull state being calculated first)
	UpdateLODAlphaMultipler();

	// set the cull flags
	if (m_cullState!=CULLSTATE_NOT_CULLED) 
	{
		// we are either viewport or distance culled
		PF_INCREMENT(CounterEffectInstsCulled);

		// check if the cull state requires the effect to be killed
		if ((m_cullState==CULLSTATE_VIEWPORT_CULLED || m_cullState==CULLSTATE_OCCLUSION_CULLED) && m_pEffectRule->GetViewportCullingMode()==1)
		{
			// this will get into here multiple times until the effect is deactivated
			// initially the game may still have the inst reserved and it may take a few frames until it is no longer reserved
			Finish(false);
		}

		//We test for shadow cull so if the effect is out of the viewport, and its shadow is visible, we would get pops.
		//They still follow same distance cull rule

		// check if the cull state requires us to stop emitting, updating or drawing
		m_isEmitCulled = (m_cullState==CULLSTATE_DISTANCE_CULLED && m_pEffectRule->GetEmitWhenDistanceCulled()==false) || 
			((m_cullState==CULLSTATE_VIEWPORT_CULLED || m_cullState==CULLSTATE_OCCLUSION_CULLED) && m_pEffectRule->GetEmitWhenViewportCulled()==false);

		m_isUpdateCulled = (m_cullState==CULLSTATE_DISTANCE_CULLED && m_pEffectRule->GetUpdateWhenDistanceCulled()==false) || 
			((m_cullState==CULLSTATE_VIEWPORT_CULLED || m_cullState==CULLSTATE_OCCLUSION_CULLED) && m_pEffectRule->GetUpdateWhenViewportCulled()==false);

		GetDataDB()->m_isDrawCulled = (m_cullState==CULLSTATE_DISTANCE_CULLED && m_pEffectRule->GetRenderWhenDistanceCulled()==false) || 
			((m_cullState==CULLSTATE_VIEWPORT_CULLED || m_cullState==CULLSTATE_OCCLUSION_CULLED) && m_pEffectRule->GetRenderWhenViewportCulled()==false);

	}
}

ptxEffectCullState ptxEffectInst::CalcCullState(const grcViewport* pViewport, bool bPerformOcclusionTest)
{
	// we're not culled if a valid viewport isn't passed
	if (pViewport==NULL)
	{
		return CULLSTATE_NOT_CULLED;
	}

	// call the callback
	if (RMPTFXMGR.GetEffectInstCullFunctor())
	{
		RMPTFXMGR.GetEffectInstCullFunctor()(this);
		return m_cullState;
	}

	// we're not culled if we can't be culled
	if (m_canBeCulled==false)
	{
		return CULLSTATE_NOT_CULLED;
	}

	// we're not culled if the viewport or distance culling aren't set up
	if ((m_pEffectRule->GetViewportCullingMode()==0 || m_pEffectRule->GetViewportCullingSphereRadius()==0.0f) && (m_pEffectRule->GetDistanceCullingMode()==0 || m_pEffectRule->GetDistanceCullingCullDist()==0.0f))
	{
		return CULLSTATE_NOT_CULLED;
	}

	// get the sphere position in world space
	Vec3V vCullSpherePosWld = m_pEffectRule->GetViewportCullingSphereOffset() * Vec3VFromF32(m_finalZoom);
	vCullSpherePosWld = Transform(GetMatrix(), vCullSpherePosWld);

	// distance culling
	if (m_pEffectRule->GetDistanceCullingMode()>0 && m_pEffectRule->GetDistanceCullingCullDist()>0.0f)
	{
		// check if we're beyond the effect's cull distance
		float cullDist = m_pEffectRule->GetDistanceCullingCullDist() * m_lodScalar;
		if (GetDataDB()->m_distSqrToCamera > (cullDist*cullDist))
		{
			// we are - we're distance culled 
			return m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING ? CULLSTATE_DISTANCE_CULLED : CULLSTATE_NOT_CULLED;
		}
	}

	// viewport/occlusion culling
	if (m_pEffectRule->GetViewportCullingMode()>0 && m_pEffectRule->GetViewportCullingSphereRadius()>0.0f)
	{
		// set up the cull sphere 
		ScalarV vRadius = ScalarVFromF32(m_pEffectRule->GetViewportCullingSphereRadius() * m_finalZoom);
		Vec4V vCullSphere = Vec4V(vCullSpherePosWld, vRadius);

		// viewport culling
		{
			// test the cull sphere against the viewport
			if ((pViewport->GetSphereCullStatusInline(vCullSphere)) == cullOutside) 
			{
				// it's outside - we're viewport culled
				return CULLSTATE_VIEWPORT_CULLED;
			}
		}

		// occlusion culling
		if(bPerformOcclusionTest)
		{
			//Update the skip occlusion test frame count
			if(m_skipOcclusionTestFrameCount > 0)
			{
				m_skipOcclusionTestFrameCount --;
			}
			else if (RMPTFXMGR.GetEffectInstOcclusionFunctor())
			{
				if (RMPTFXMGR.GetEffectInstOcclusionFunctor()(&vCullSphere))
				{
					// it's occluded - we're viewport culled
					return CULLSTATE_OCCLUSION_CULLED;
				}
				else
				{
					//it passed occlusion test. Let's start skipping the test for the next few frames
#if RMPTFX_BANK
					if(RMPTFXMGR.GetIsSkipOcclusionTestForEffectInstances())
#endif
					{
						m_skipOcclusionTestFrameCount = RMPTFXMGR.GetMaxFrameSkipOcclusionTestForEffectInstances();
					}
				}
			}
		}
	}

	// default to not being culled
	return CULLSTATE_NOT_CULLED;
}

void ptxEffectInst::UpdateFinalMtx()
{
	// calc the final matrix 
	m_vMtxFinal = m_vMtxOffset;
	m_vMtxFinal.SetCol3(m_vMtxFinal.GetCol3() + m_vRandOffsetPos);
	Transform(m_vMtxFinal, m_vMtxBase, m_vMtxFinal);

	// apply any axis inversions
	ScalarV vNegOne(V_NEGONE);

#if RMPTFX_BANK
	if (ptxDebug::sm_disableInvertedAxes==false)
#endif
	{
		if (m_invertAxes & PTXEFFECTINVERTAXIS_X) 
		{
			m_vMtxFinal.SetCol0(m_vMtxFinal.GetCol0()*vNegOne);
		}

		if (m_invertAxes & PTXEFFECTINVERTAXIS_Y) 
		{
			m_vMtxFinal.SetCol1(m_vMtxFinal.GetCol1()*vNegOne);
		}

		if (m_invertAxes & PTXEFFECTINVERTAXIS_Z) 
		{
			m_vMtxFinal.SetCol2(m_vMtxFinal.GetCol2()*vNegOne);
		}
	}

	ptxAssertf(IsFiniteAll(m_vMtxFinal.GetCol0()), "final matrix col0 is invalid");
	ptxAssertf(IsFiniteAll(m_vMtxFinal.GetCol1()), "final matrix col1 is invalid");
	ptxAssertf(IsFiniteAll(m_vMtxFinal.GetCol2()), "final matrix col2 is invalid");
	ptxAssertf(IsFiniteAll(m_vMtxFinal.GetCol3()), "final matrix col3 is invalid");

	// update the distance to the camera (unless we're overriding it)
	if (m_internalFlags.IsSet(PTXEFFECTFLAG_OVERRIDE_DISTSQR_TO_CAM)==false)
	{
		// get the squared distance from the effect to the camera
		float distSqrToCamera = 0.0f;

		if (ptxVerifyf(RMPTFXMGR.GetEffectInstDistToCamFunctor(), "no distance callback function defined"))
		{
			distSqrToCamera = RMPTFXMGR.GetEffectInstDistToCamFunctor()(this);
			distSqrToCamera = rage::Max(distSqrToCamera + m_cameraBias, 0.0f);
			distSqrToCamera *= distSqrToCamera;
		}

		GetDataDB()->m_distSqrToCamera = distSqrToCamera;
	}
}

Vec4V_Out ptxEffectInst::GetBoundingSphere()
{
	ptxAssertf(m_pEffectRule, "trying to query bounding sphere of an effect inst without an effect rule");

	// get the sphere position and radius 
	Vec4V vSphere = m_pEffectRule->GetViewportCullingSphere()*ScalarVFromF32(m_finalZoom);
	ScalarV vRadius = Max(vSphere.GetW(), ScalarV(V_FLT_SMALL_1)); // max(0.1f, radius)
	return Vec4V(Transform(m_vMtxFinal, vSphere.GetXYZ()), vRadius);
}

void ptxEffectInst::AddCollisionPolygons(ptxCollisionPoly* pPolyArray, int numPolys)
{
	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());

	ptxAssertf(numPolys>0, "trying to add 0 collision polys");
	m_internalCollisionSet.AddPolys(pPolyArray, numPolys);
}

void ptxEffectInst::ClearCollisionSet()
{
	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());

	m_internalCollisionSet.Clear();
}

void ptxEffectInst::SetCollisionSet(ptxCollisionSet* pColnSet)
{
	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());

	m_pCollisionSet = pColnSet;
}

void ptxEffectInst::CopyCollisionSet(ptxCollisionSet* pColnSet)
{
	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());

	m_internalCollisionSet.CopyFrom(pColnSet);
}

bool ptxEffectInst::GetUseDataVolume()
{
	ptxAssertf(m_pEffectRule, "trying to query data volume of an effect inst without an effect rule");

	return m_pEffectRule->GetUseDataVolume();
}

void ptxEffectInst::GetDataVolume(ptxDataVolume* pDataVolume)
{
	ptxAssertf(m_pEffectRule, "trying to query data volume of an effect inst without an effect rule");
	ptxAssertf(pDataVolume, "effect inst has no data volume");

	pDataVolume->Clear();

	if (m_pEffectRule->GetUseDataVolume())
	{
		// prepare the evolution data
		m_pEffectRule->PrepareEvoData();

		ScalarV vFinalZoom = ScalarVFromF32(m_finalZoom);

		// query the data volume keyframes
		ScalarV vKeyTimeEffect = GetKeyTime();
		Vec4V vDataSphere = m_pEffectRule->GetDataSphereKFP().Query(vKeyTimeEffect, m_pEffectRule->GetEvolutionList(), m_evoValues);
		Vec4V vDataCapsule = m_pEffectRule->GetDataCapsuleKFP().Query(vKeyTimeEffect, m_pEffectRule->GetEvolutionList(), m_evoValues);

		// extract the data volume data
		Vec3V vPos = vDataSphere.GetXYZ();
		ScalarV vRadius = vDataSphere.GetW() * vFinalZoom;
		Vec3V vRot = vDataCapsule.GetXYZ();
		ScalarV vLength = Abs(vDataCapsule.GetW()) * vFinalZoom;			// taking the absolute value here as this component can go negative (which makes no sense)

		// construct the matrix
		Matrix34 dataVolumeMtx;
		dataVolumeMtx.Identity();
		dataVolumeMtx.RotateFullX(vRot.GetXf()*DtoR);
		dataVolumeMtx.RotateFullY(vRot.GetYf()*DtoR);
		dataVolumeMtx.RotateFullZ(vRot.GetZf()*DtoR);
		dataVolumeMtx.d = RCC_VECTOR3(vPos) * RCC_VECTOR3(vFinalZoom);

		// transform by the effect inst matrix
		Mat34V vMtxWld;
		Transform(vMtxWld, GetMatrix(), RCC_MAT34V(dataVolumeMtx));

		// set the start and end positions of the line segment
		Vec3V vPosWldStart = vMtxWld.GetCol3();
		Vec3V vPosWldEnd = vPosWldStart + (vMtxWld.GetCol1()*vLength*ScalarV(V_ONE));
		Vec3V vPosWldCentre = (vPosWldStart + vPosWldEnd) * ScalarV(V_HALF);

		// override the world matrix position to be at the centre of the line segment
		vMtxWld.SetCol3(vPosWldCentre);

		// set up the data volume
		pDataVolume->SetType(m_pEffectRule->GetDataVolumeType());
		pDataVolume->SetPosWldCentre(vPosWldCentre);
		pDataVolume->SetPosWldStart(vPosWldStart);
		pDataVolume->SetPosWldEnd(vPosWldEnd);
		pDataVolume->SetLength(vLength.Getf());
		pDataVolume->SetRadius(vRadius.Getf());
	}
}

u8 ptxEffectInst::GetDataVolumeType()
{
	ptxAssertf(m_pEffectRule, "trying to query data volume of an effect inst without an effect rule");

	return m_pEffectRule->GetDataVolumeType();
}

void ptxEffectInst::CopyToUpdateBuffer()
{
	m_pEffectInstItem->GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId())->m_currLifeRatio = m_pEffectInstItem->GetDataDB()->m_currLifeRatio;
	m_pEffectInstItem->GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId())->m_customFlags = m_pEffectInstItem->GetDataDB()->m_customFlags;
	m_pEffectInstItem->GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId())->m_distSqrToCamera = m_pEffectInstItem->GetDataDB()->m_distSqrToCamera;
	m_pEffectInstItem->GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId())->m_isDrawCulled = m_pEffectInstItem->GetDataDB()->m_isDrawCulled;
}

Vec3V_Out ptxEffectInst::SetBasePos(Vec3V_In vPos)
{
	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());

	Vec3V vPosDiff = vPos - m_vMtxBase.GetCol3();

	m_vMtxBase = Mat34V(V_IDENTITY);
	m_vMtxBase.SetCol3(vPos);

	return vPosDiff;
}

Vec3V_Out ptxEffectInst::SetBaseMtx(Mat34V_In vMtx)
{
	ptxAssertf(IsFiniteAll(vMtx.GetCol0()), "base matrix col0 is invalid (%s - %.3f, %.3f, %.3f)", GetEffectRule()->GetName(), vMtx.GetCol0().GetXf(), vMtx.GetCol0().GetYf(), vMtx.GetCol0().GetZf());
	ptxAssertf(IsFiniteAll(vMtx.GetCol1()), "base matrix col1 is invalid (%s - %.3f, %.3f, %.3f)", GetEffectRule()->GetName(), vMtx.GetCol1().GetXf(), vMtx.GetCol1().GetYf(), vMtx.GetCol1().GetZf());
	ptxAssertf(IsFiniteAll(vMtx.GetCol2()), "base matrix col2 is invalid (%s - %.3f, %.3f, %.3f)", GetEffectRule()->GetName(), vMtx.GetCol2().GetXf(), vMtx.GetCol2().GetYf(), vMtx.GetCol2().GetZf());
	ptxAssertf(IsFiniteAll(vMtx.GetCol3()), "base matrix col3 is invalid (%s - %.3f, %.3f, %.3f)", GetEffectRule()->GetName(), vMtx.GetCol3().GetXf(), vMtx.GetCol3().GetYf(), vMtx.GetCol3().GetZf());

	// test for orthogonal rather than orthonormal here - we may be attached to a scaled entity matrix
	// allow zero matrices through as well - IsOrthogonal3x3 seems to assert on them even though IsOrthonormal3x3 doesn't
	ptxAssertf(IsZeroAll(vMtx.GetCol0()) || vMtx.IsOrthogonal3x3(ScalarV(V_ONE)), "trying to set a base effect matrix that isn't orthogonal (%s - %.3f, %.3f, %.3f - %.3f, %.3f, %.3f - %.3f, %.3f, %.3f)", 
		GetEffectRule()->GetName(), 
		vMtx.GetCol0().GetXf(), vMtx.GetCol0().GetYf(), vMtx.GetCol0().GetZf(),
		vMtx.GetCol1().GetXf(), vMtx.GetCol1().GetYf(), vMtx.GetCol1().GetZf(),
		vMtx.GetCol2().GetXf(), vMtx.GetCol2().GetYf(), vMtx.GetCol2().GetZf());

	SYS_CS_SYNC_CONDITIONAL(RMPTFXMGR.GetUpdateListCSToken(), !RMPTFXMGR.IsParticleUpdateThreadOrTask());

	Vec3V vPosDiff = vMtx.GetCol3() - m_vMtxBase.GetCol3();

	m_vMtxBase = vMtx;

	return vPosDiff;
}

void ptxEffectInst::SetOffsetMtx(Mat34V_In vMtx)
{
	ptxAssertf(vMtx.IsOrthonormal3x3(ScalarV(V_ONE)), "trying to set an offset effect matrix that isn't orthonormal (%s - %.3f, %.3f, %.3f - %.3f, %.3f, %.3f - %.3f, %.3f, %.3f)", 
		GetEffectRule()->GetName(), 
		vMtx.GetCol0().GetXf(), vMtx.GetCol0().GetYf(), vMtx.GetCol0().GetZf(),
		vMtx.GetCol1().GetXf(), vMtx.GetCol1().GetYf(), vMtx.GetCol1().GetZf(),
		vMtx.GetCol2().GetXf(), vMtx.GetCol2().GetYf(), vMtx.GetCol2().GetZf());

	m_vMtxOffset = vMtx;
}

void ptxEffectInst::SetOffsetPos(Vec3V_In vPos)
{
	m_vMtxOffset.SetCol3(vPos);
}

void ptxEffectInst::SetOffsetRot(Vec3V_In vEulerAngles)
{
	RC_MATRIX34(m_vMtxOffset).Identity3x3();
	RC_MATRIX34(m_vMtxOffset).RotateY(vEulerAngles.GetYf());
	RC_MATRIX34(m_vMtxOffset).RotateX(vEulerAngles.GetXf());
	RC_MATRIX34(m_vMtxOffset).RotateZ(vEulerAngles.GetZf());
}

void ptxEffectInst::SetOffsetRot(Vec3V_In vAxis, float angle)
{
	RC_MATRIX34(m_vMtxOffset).Identity3x3();
	RC_MATRIX34(m_vMtxOffset).Rotate(RCC_VECTOR3(vAxis), angle);
}

void ptxEffectInst::SetOffsetRot(QuatV_In vQuat)
{
	RC_MATRIX34(m_vMtxOffset).Identity3x3();
	RC_MATRIX34(m_vMtxOffset).FromQuaternion(RCC_QUATERNION(vQuat));
}

void ptxEffectInst::SetOffsetRot(Mat34V_In vMtx)
{
	ptxAssertf(vMtx.IsOrthonormal3x3(ScalarV(V_ONE)), "trying to set an offset effect matrix that isn't orthonormal (%s - %.3f, %.3f, %.3f - %.3f, %.3f, %.3f - %.3f, %.3f, %.3f)", 
		GetEffectRule()->GetName(), 
		vMtx.GetCol0().GetXf(), vMtx.GetCol0().GetYf(), vMtx.GetCol0().GetZf(),
		vMtx.GetCol1().GetXf(), vMtx.GetCol1().GetYf(), vMtx.GetCol1().GetZf(),
		vMtx.GetCol2().GetXf(), vMtx.GetCol2().GetYf(), vMtx.GetCol2().GetZf());

	m_vMtxOffset.Set3x3(vMtx);
}

void ptxEffectInst::SetRenderSetup(ptxRenderSetup *pRenderSetup)
{
	m_pRenderSetup = pRenderSetup;
}

bool ptxEffectInst::HasEvoFromHash(atHashValue hash)
{
	ptxAssertf(m_pEffectRule, "trying to get evo data of an effect inst without an effect rule");

	if (m_pEffectRule->GetEvolutionList())
	{
		int evoIdx = m_pEffectRule->GetEvolutionList()->GetEvolutionIndexFromHash(hash);
		if (evoIdx>=0)
		{
			return true;
		}
	}

	return false;
}

void ptxEffectInst::SetEvoValueFromIndex(int evoIdx, float val) 
{
	FastAssert(evoIdx>=0 && evoIdx<PTXEVO_MAX_EVOLUTIONS); 
	m_evoValues[evoIdx] = PackEvoValue(val);
}

//void ptxEffectInst::SetEvoValue(const char* pEvoName, float val)
//{
//	ptxAssertf(m_pEffectRule, "trying to set evo data of an effect inst without an effect rule");
//
//	if (m_pEffectRule->GetEvolutionList())
//	{
//		int evoIdx = m_pEffectRule->GetEvolutionList()->GetEvolutionIndex(pEvoName);
//		if (evoIdx>=0)
//		{
//			m_evoValues[evoIdx] = PackEvoValue(val);
//		}
//	}
//}

void ptxEffectInst::SetEvoValueFromHash(atHashValue evoHashValue, float val)
{
	ptxAssertf(m_pEffectRule, "trying to set evo data of an effect inst without an effect rule");

	if (m_pEffectRule->GetEvolutionList())
	{
		int evoIdx = m_pEffectRule->GetEvolutionList()->GetEvolutionIndexFromHash(evoHashValue);
		if (evoIdx>=0)
		{
			m_evoValues[evoIdx] = PackEvoValue(val);
		}
	}
}

void ptxEffectInst::SetEvoValueFromHash16(u16 evoHashValue, float val)
{
	ptxAssertf(m_pEffectRule, "trying to set evo data of an effect inst without an effect rule");

	if (m_pEffectRule->GetEvolutionList())
	{
		int evoIdx = m_pEffectRule->GetEvolutionList()->GetEvolutionIndexFromHash16(evoHashValue);
		if (evoIdx>=0)
		{
			m_evoValues[evoIdx] = PackEvoValue(val);
		}
	}
}

float ptxEffectInst::GetEvoValueFromIndex(int evoIdx)
{
	FastAssert(evoIdx>=0 && evoIdx<PTXEVO_MAX_EVOLUTIONS); 
	return UnPackEvoValue(m_evoValues[evoIdx]);
}

ptxEvoValueType ptxEffectInst::GetEvoValueRaw(int evoIdx)
{
	FastAssert(evoIdx>=0 && evoIdx<PTXEVO_MAX_EVOLUTIONS); 
	return m_evoValues[evoIdx];
}


//float ptxEffectInst::GetEvoValue(const char* pEvoName)
//{
//	ptxAssertf(m_pEffectRule, "trying to get evo data of an effect inst without an effect rule");
//
//	if (m_pEffectRule->GetEvolutionList())
//	{
//		int evoIdx = m_pEffectRule->GetEvolutionList()->GetEvolutionIndex(pEvoName);
//		if (evoIdx>=0)
//		{
//			return UnPackEvoValue(m_evoValues[evoIdx]);
//		}
//	}
//
//	return 0.0f;
//}

float ptxEffectInst::GetEvoValueFromHash(atHashValue evoHashValue)
{
	ptxAssertf(m_pEffectRule, "trying to get evo data of an effect inst without an effect rule");

	if (m_pEffectRule->GetEvolutionList())
	{
		int evoIdx = m_pEffectRule->GetEvolutionList()->GetEvolutionIndexFromHash(evoHashValue);
		if (evoIdx>=0)
		{
			return UnPackEvoValue(m_evoValues[evoIdx]);
		}
	}

	return 0.0f;
}

void ptxEffectInst::SetOverrideDistSqrToCamera(float distSqr, bool always)
{
	if (distSqr == -1.0f)
	{
		m_internalFlags.Set(PTXEFFECTFLAG_OVERRIDE_DISTSQR_TO_CAM, false);
		m_internalFlags.Set(PTXEFFECTFLAG_OVERRIDE_DISTSQR_TO_CAM_ALWAYS, always);
	}
	else
	{
		m_internalFlags.Set(PTXEFFECTFLAG_OVERRIDE_DISTSQR_TO_CAM, true);
		m_internalFlags.Set(PTXEFFECTFLAG_OVERRIDE_DISTSQR_TO_CAM_ALWAYS, always);

		GetDataDB()->m_distSqrToCamera = distSqr;
	}
}

int ptxEffectInst::CalcNumActivePoints()
{
	int numActivePoints = 0;

	// go through the events adding up the number of active points
	for (int i=0; i<m_eventInsts.GetCount(); i++)
	{
		numActivePoints += m_eventInsts[i].GetEmitterInst()->GetPointList().GetNumItems();
	}

	return numActivePoints;
}

#if RMPTFX_BANK
const char* ptxEffectInst::GetEvoName(int evoIdx)
{
	ptxAssertf(m_pEffectRule, "trying to get evo data of an effect inst without an effect rule");

	if (m_pEffectRule->GetEvolutionList())
	{
		return m_pEffectRule->GetEvolutionList()->GetEvolutionName(evoIdx);
	}

	return NULL;
}
#endif

#if RMPTFX_BANK
void ptxEffectInst::DebugDraw()
{
	// global effect instance debug drawing
	u32 debugHashValue = atHashValue(ptxDebug::sm_effectInstNameFilter);
	if (debugHashValue==0 || debugHashValue==atHashValue(m_pEffectRule->GetName()))
	{
		if (ptxDebug::sm_drawEffectInstInfo)
		{
			char displayTxt[1024];
			char tempTxt[1024];

			displayTxt[0] = '\0';

			// add any time info to the display text
			if (ptxDebug::sm_drawEffectInstNames)
			{
				formatf(tempTxt, 256, "%s\n", m_pEffectRule->GetName());
				formatf(displayTxt, 256, "%s", tempTxt);
			}

			if(ptxDebug::sm_drawEffectInstAddress)
			{
				formatf(tempTxt, 256, "%saddress: %p\n", displayTxt, this);
				formatf(displayTxt, 256, "%s", tempTxt);
			}

			// add any fxlist info to the display text
			if (ptxDebug::sm_drawEffectInstFxList)
			{
				formatf(tempTxt, 256, "%sfxlist: %s\n", displayTxt, m_pEffectRule->GetFxList()->GetName());
				formatf(displayTxt, 256, "%s", tempTxt);
			}

			// add any time info to the display text
			if (ptxDebug::sm_drawEffectInstLifeRatios)
			{
				formatf(tempTxt, 256, "%slife ratio: %.2f\n", displayTxt, GetDataDB()->m_currLifeRatio);
				formatf(displayTxt, 256, "%s", tempTxt);
			}

			// add any point info to the display text
			if (ptxDebug::sm_drawEffectInstNumPoints)
			{
				formatf(tempTxt, 256, "%spoints: %d\n", displayTxt, m_numActivePoints);
				formatf(displayTxt, 256, "%s", tempTxt);
			}

			// add any evolution info to the display text
			if (ptxDebug::sm_drawEffectInstEvolutions)
			{
				if (m_pEffectRule->GetEvolutionList())
				{
					for (int i=0; i<m_pEffectRule->GetEvolutionList()->GetNumEvolutions(); i++)
					{
						formatf(tempTxt, 256, "%s%s evo: %.2f\n", displayTxt, GetEvoName(i), UnPackEvoValue(m_evoValues[i]));
						formatf(displayTxt, 256, "%s", tempTxt);
					}
				}
			}

			// distance to camera
			if (ptxDebug::sm_drawEffectInstDistToCam)
			{
				float distSqr = GetDataDB()->GetDistSqrToCamera();
				float dist = Sqrtf(distSqr);
				formatf(tempTxt, 256, "%sdist: %.2f\n", displayTxt, dist);
				formatf(displayTxt, 256, "%s", tempTxt);
			}

			// max duration
			if (ptxDebug::sm_drawEffectInstMaxDuration)
			{
				float maxDuration = GetEffectRule()->GetMaxDuration();
				formatf(tempTxt, 256, "%smaxduration: %.2f\n", displayTxt, maxDuration);
				formatf(displayTxt, 256, "%s", tempTxt);
			}

			if(ptxDebug::sm_drawEffectInstCameraBias)
			{
				formatf(tempTxt, 256, "%scamera bias: %f\n", displayTxt, m_cameraBias);
				formatf(displayTxt, 256, "%s", tempTxt);
				
			}

			// add any collision info to the display text
			if (ptxDebug::sm_drawEffectInstColnPolys)
			{
				if (m_pCollisionSet && m_pCollisionSet->GetNumColnPolys()>0)
				{
					formatf(tempTxt, 256, "%scoln polys: %d\n", displayTxt, m_pCollisionSet->GetNumColnPolys());
					formatf(displayTxt, 256, "%s", tempTxt);
				}
			}

			// render display text
			if (displayTxt[0] != '\0')
			{
				Color32 col = Color32(1.0f, 1.0f, 1.0f, 1.0f);

				if (ptxDebug::sm_drawEffectInstQualityHighlighted)
				{
					static const Color32 hiResColor = Color32(0.95f, 0.05f, 0.05f, 1.0f);
					static const Color32 lowResColor = Color32(0.05f, 0.95f, 0.05f, 1.0f);

					if (RMPTFXMGR.GetQualityState() == PTX_QUALITY_HIRES)
					{
						col = hiResColor;
					}
					else
					{
						col = lowResColor;
					}

				}
				else if (ptxDebug::sm_drawEffectInstCallbacksHighlighted)
				{
					static const Color32 callbackColor = Color32(0.95f, 0.05f, 0.05f, 1.0f);
					static const Color32 localColor = Color32(0.05f, 0.95f, 0.05f, 1.0f);
					if (GetNeedsCallbackUpdate())
					{
						col = callbackColor;
					}
					else
					{
						col = localColor;
					}
				}
				else
				{
					if (m_playState==PTXEFFECTINSTPLAYSTATE_PLAYING)
					{
						if (m_cullState!=CULLSTATE_NOT_CULLED)
						{
							col = Color32(0.05f, 0.05f, 0.95f, 1.0f);
						}
						else
						{
							col = Color32(0.05f, 0.95f, 0.05f, 1.0f);
						}
					}
					else if (m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_MANUALLY)
					{
						col = Color32(0.95f, 0.05f, 0.5f, 1.0f);
					}
					else if (m_playState==PTXEFFECTINSTPLAYSTATE_STOPPED_NATURALLY)
					{
						col = Color32(0.95f, 0.5f, 0.05f, 1.0f);
					}
					else if (m_playState==PTXEFFECTINSTPLAYSTATE_FINISHED)
					{
						col = Color32(0.95f, 0.05f, 0.05f, 1.0f);
					}
					else if (m_playState==PTXEFFECTINSTPLAYSTATE_DEACTIVATED)
					{
						col = Color32(0.0f, 0.0f, 0.0f, 1.0f);
					}
				}

				Vec3V vEffectPos = GetCurrPos();
				grcDebugDraw::Text(vEffectPos, col, 0, 10, displayTxt, false);
			}

			// render base and final axes
			grcDebugDraw::Axis(m_vMtxBase, ptxDebug::sm_debugAxisScale*0.5f);
			grcDebugDraw::Axis(m_vMtxFinal, ptxDebug::sm_debugAxisScale);

			// render position trace
			if (ptxDebug::sm_drawEffectInstPositionTrace)
			{
				grcDebugDraw::Line(m_vPrevPos, GetCurrPos(), Color32(1.0f, 0.5f, 0.0f, 1.0f), -200);
			}
		}

		// per effect rule debug drawing
#if RMPTFX_EDITOR
		{
			// render base and final axes
			if (m_pEffectRule->GetUIData().GetShowInstances())
			{
				grcDebugDraw::Axis(m_vMtxBase, ptxDebug::sm_debugAxisScale*0.5f);
				grcDebugDraw::Axis(m_vMtxFinal, ptxDebug::sm_debugAxisScale);
			}

			// render cull sphere
			if (m_pEffectRule->GetUIData().GetShowCullSpheres() && m_pEffectRule->GetViewportCullingMode()>0)
			{
				Vec3V vPos = Scale(m_pEffectRule->GetViewportCullingSphereOffset(), Vec3VFromF32(m_finalZoom));
				vPos = Transform(GetMatrix(), vPos);

				grcDebugDraw::Sphere(vPos, m_pEffectRule->GetViewportCullingSphereRadius()*m_finalZoom, Color32(138, 221, 255, 70), false);
			}

			// render collision polys
			if (m_pEffectRule->GetUIData().GetShowCollision())
			{
				if (HasCollisionSet())
				{
					GetCollisionSet()->DebugDraw();
				}
			}

			// render data volumes
			if (m_pEffectRule->GetUIData().GetShowDataVolumes() && m_pEffectRule->GetUseDataVolume())
			{
				ptxDataVolume dataVolume;
				GetDataVolume(&dataVolume);

				grcDebugDraw::Capsule(dataVolume.GetPosWldStart(), dataVolume.GetPosWldEnd(), dataVolume.GetRadius(), Color32(255, 100, 10, 170), false);
			}
		}
#endif

		// render timeline events
		for (int i=0; i<m_pEffectRule->GetTimeline().GetNumEvents(); i++)
		{
			ptxEventInst* pEventInst = &m_eventInsts[i];

			if (m_playState!=PTXEFFECTINSTPLAYSTATE_FINISHED)
			{
				m_pEffectRule->GetTimeline().GetEvent(i)->DebugDraw(this, pEventInst);
			}
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxEffectInst::OutputUpdateProfileInfo(const char* pFunctionName, sysTimer& timer)
{
	if (ptxDebug::sm_outputEffectInstUpdateProfile)
	{
		u32 debugHashValue = atHashValue(ptxDebug::sm_effectInstNameFilter);
		if (debugHashValue==0 || debugHashValue==atHashValue(m_pEffectRule->GetName()))
		{
			ptxDebugf1("%s - %32s took %.2fms lr=%.2f, pts=%d", m_pEffectRule->GetName(), pFunctionName, timer.GetMsTime(), GetDataDB()->m_currLifeRatio, m_numActivePoints);
		}
	}
}
#endif

#if RMPTFX_BANK
bool ptxEffectInst::ShouldOutputPlayStateInfo()
{
	if (ptxDebug::sm_outputEffectInstPlayStateDebug)
	{
		u32 debugHashValue = atHashValue(ptxDebug::sm_effectInstNameFilter);
		if (debugHashValue==0 || debugHashValue==atHashValue(m_pEffectRule->GetName()))
		{
			return true;
		}
	}

	return false;
}
#endif

#if RMPTFX_BANK
void ptxEffectInst::OutputPlayStateFunction(const char* pFunctionName)
{
	if (m_outputPlayStateDebug)
	{
		ptxDebugf1("effect %p (%s) called %s()", this, m_pEffectRule->GetName(), pFunctionName);
	}
}
#endif

#if RMPTFX_BANK
void ptxEffectInst::SetPlayState(ptxEffectInstPlayState val) 
{
	static const char* s_pPlayStateNames[PTXEFFECTINSTPLAYSTATE_NUM] = 
	{
		"WAITING_TO_ACTIVATE", 
		"ACTIVATED",
		"PLAYING",
		"STOPPED_MANUALLY",
		"STOPPED_NATURALLY",
		"FINISHED",
		"DEACTIVATED"
	};

	ptxEffectInstPlayState origPlayState = m_playState;
	m_playState = val; 
	
	if (m_pEffectRule && m_outputPlayStateDebug)
	{
		ptxDebugf1("effect %p (%s) changing from %s to %s", this, m_pEffectRule->GetName(), s_pPlayStateNames[origPlayState], s_pPlayStateNames[m_playState]);
	}

	if (m_playState==PTXEFFECTINSTPLAYSTATE_ACTIVATED)
	{
		ptxAssertf(origPlayState==PTXEFFECTINSTPLAYSTATE_WAITING_TO_ACTIVATE, "trying to activate effect inst %s from an invalid state", m_pEffectRule->GetName());
		ptxAssertf(m_hasBeenActivated==false, "trying to activate effect inst %s that is flagged as activated already", m_pEffectRule->GetName());
		m_hasBeenActivated = true;
	}

	if (m_playState==PTXEFFECTINSTPLAYSTATE_DEACTIVATED)
	{
		ptxAssertf(m_hasBeenActivated, "trying to deactivate effect inst %s that hasn't been activated", m_pEffectRule->GetName());
		m_hasBeenActivated = false;
	}
}
#endif


