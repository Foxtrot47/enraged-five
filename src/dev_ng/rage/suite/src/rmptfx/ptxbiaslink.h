// 
// rmptfx/ptxbiaslink.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXBIASLINK_H 
#define RMPTFX_PTXBIASLINK_H 


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxrandomtable.h"

// includes (rage)
#include "atl/array.h"
#include "data/struct.h"
#include "parser/macros.h"


// namespaces
namespace rage 
{


// forward declarations
class ptxByteBuff;
class ptxKeyframeProp;


// classes
class ptxBiasLink
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxBiasLink();
		ptxBiasLink(const char* pName);

		// resourcing
		ptxBiasLink(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxBiasLink);

		// misc
		void GenerateRandom() {m_randIndex = ptxRandomTable::GetCurrIndex();}
		void SetLink(ptxKeyframeProp& keyframeProp);

		// editor
#if RMPTFX_EDITOR
		void SendToEditor(ptxByteBuff& buff);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif


	private: //////////////////////////

		// editor
#if RMPTFX_EDITOR
		void Add(int keyframePropId) {m_keyframePropIds.PushAndGrow(keyframePropId);}
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_SIMPLE_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		char m_name[64];
		atArray<int> m_keyframePropIds;
		u8 m_randIndex;
		datPadding<3> m_pad;

};


} // namespace rage


#endif // RMPTFX_PTXBIASLINK_H 
