// 
// rmptfx/ptxByteBuff.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxbytebuff.h"

// includes (rmptfx)
#include "rmptfx/ptxconfig.h"

// includes (rage)
#include "system/memory.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// global variables


// code
ptxByteBuff::ptxByteBuff()
{
	m_size = 0;
	m_currWritePos = 0;
	m_currReadPos = 0;
	m_pBuffer = NULL;
}

ptxByteBuff::~ptxByteBuff()
{
	delete[] m_pBuffer;
}

void ptxByteBuff::Create(u32 maxSize)
{
	USE_DEBUG_MEMORY();

	m_size = maxSize;
	m_currWritePos = 0;
	m_currReadPos = 0;
	m_pBuffer = rage_new u8[maxSize];
}

bool ptxByteBuff::IsReady()
{
	// return if there is no buffered data
	if (m_currWritePos==0) 
	{
		return false;
	}

	// store the original read position
	u32 origReadPos = m_currReadPos;

	// read a u32 from the start of the buffer
	m_currReadPos = 0;
	u32 size = Read_u32();

	// restore the original read position
	m_currReadPos = origReadPos;

	// the buffer is considered ready if the first 4 bytes match its size
	if (size == m_currWritePos-4)
	{
		return true;
	}

	return false;
}

bool ptxByteBuff::Read_bool() const 
{
	ptxAssertf(m_currReadPos<m_currWritePos, "read overflow");
	return m_pBuffer[m_currReadPos++]==0 ? false : true;
}

void ptxByteBuff::Write_bool(bool data) 
{
	Write_bool(m_currWritePos, data);
	m_currWritePos += 1;
	ptxAssertf(m_currWritePos<=m_size, "write overflow");
}

void ptxByteBuff::Write_bool(u32 writePos, bool data) 
{
	m_pBuffer[writePos++] = (u8)data;
	ptxAssertf(writePos<=m_size, "write overflow");
}

u8 ptxByteBuff::Read_u8() const 
{
	ptxAssertf(m_currReadPos<m_currWritePos, "read overflow");
	return m_pBuffer[m_currReadPos++];
}

void ptxByteBuff::Write_u8(u8 data) 
{
	Write_u8(m_currWritePos, data);
	m_currWritePos += 1;
	ptxAssertf(m_currWritePos<=m_size, "write overflow");
}

void ptxByteBuff::Write_u8(u32 writePos, u8 data) 
{
	m_pBuffer[writePos++] = data;
	ptxAssertf(writePos<=m_size, "write overflow");
}

s8 ptxByteBuff::Read_s8() const
{
	return (s8)Read_u8();
}

void ptxByteBuff::Write_s8(s8 data) 
{
	Write_u8((u8)data);
}

void ptxByteBuff::Write_s8(u32 writePos, s8 data) 
{
	Write_u8(writePos, (u8)data);
}

u16 ptxByteBuff::Read_u16() const 
{
	ptxAssertf(m_currReadPos<m_currWritePos, "read overflow");
	u16 byteA = m_pBuffer[m_currReadPos++];
	u16 byteB = m_pBuffer[m_currReadPos++];
	return (u16)(byteA|(byteB<<8));
}

void ptxByteBuff::Write_u16(u16 data) 
{
	Write_u16(m_currWritePos, data);
	m_currWritePos += 2;
	ptxAssertf(m_currWritePos<=m_size, "write overflow");
}

void ptxByteBuff::Write_u16(u32 writePos, u16 data) 
{
	m_pBuffer[writePos++] = (u8)(data);
	m_pBuffer[writePos++] = (u8)(data>>8);
	ptxAssertf(writePos<=m_size, "write overflow");
}

s16 ptxByteBuff::Read_s16() const
{
	return (s16)Read_u16();
}

void ptxByteBuff::Write_s16(s16 data) 
{
	Write_u16((u16)data);
}

void ptxByteBuff::Write_s16(u32 writePos, s16 data) 
{
	Write_u16(writePos, (u16)data);
}

u32 ptxByteBuff::Read_u32() const 
{
	ptxAssertf(m_currReadPos<m_currWritePos, "read overflow");
	u32 byteA = m_pBuffer[m_currReadPos++];
	u32 byteB = m_pBuffer[m_currReadPos++];
	u32 byteC = m_pBuffer[m_currReadPos++];
	u32 byteD = m_pBuffer[m_currReadPos++];
	return (byteA | (byteB<<8) | (byteC<<16) | (byteD<<24));
}

void ptxByteBuff::Write_u32(u32 data) 
{
	Write_u32(m_currWritePos, data);
	m_currWritePos += 4;
	ptxAssertf(m_currWritePos<=m_size, "write overflow");
}

void ptxByteBuff::Write_u32(u32 writePos, u32 data)
{
	m_pBuffer[writePos++] = (u8)(data);
	m_pBuffer[writePos++] = (u8)(data>>8);
	m_pBuffer[writePos++] = (u8)(data>>16);
	m_pBuffer[writePos++] = (u8)(data>>24);
	ptxAssertf(writePos<=m_size, "write overflow");
}

s32 ptxByteBuff::Read_s32() const 
{
	return (s32)Read_u32();
}

void ptxByteBuff::Write_s32(s32 data)
{
	Write_u32((u32)data);
}

void ptxByteBuff::Write_s32(u32 writePos,s32 data)
{
	Write_u32(writePos, (u32)data);
}

float ptxByteBuff::Read_float() const 
{
	union 
	{ 
		float f; 
		unsigned u; 
	} x;

	ptxAssertf(m_currReadPos<m_currWritePos, "read overflow");
	x.u = Read_u32();
	return x.f;
}

void ptxByteBuff::Write_float(float data) 
{
	union 
	{ 
		float f; 
		unsigned u; 
	} x;

	x.f = data;
	Write_u32(x.u);
}

const char* ptxByteBuff::Read_const_char() const 
{
	ptxAssertf(m_currReadPos<m_currWritePos, "read overflow");

	u32 numChars = Read_u32();
	if (numChars > 0)
	{
		const char* pString = (const char*)(m_pBuffer + m_currReadPos);
		m_currReadPos += numChars + 1;
		return pString;
	}
	else 
	{
		return NULL;
	}
}

void ptxByteBuff::Write_const_char(const char* pData) 
{
	if (pData==NULL || pData[0] == '\0')
	{
		Write_u32(0);
	}
	else 
	{
		int numChars = StringLength(pData) + 1;
		Write_u32((u32)numChars);
		memcpy(m_pBuffer+m_currWritePos, pData, numChars);
		m_currWritePos = m_currWritePos + numChars;
	}

	ptxAssertf(m_currWritePos<=m_size, "write overflow");
}