// 
// rmptfx/ptxmanager.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxmanager.h"

// includes (rmptfx)
#include "rmptfx/ptxbehaviours.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxdrawlist.h"
#include "rmptfx/ptxinterface.h"
#include "rmptfx/ptxkeyframewidget.h"
#include "rmptfx/ptxupdatetask.h"

// includes (rage)
#include "atl/atfixedstring.h"
#include "bank/bkmgr.h"
#include "bank/msgbox.h"
#include "diag/output.h"
#include "grcore/allocscope.h"
#include "grcore/gputimer.h"
#include "grcore/debugdraw.h"
#include "paging/rscbuilder.h"
#include "profile/element.h"
#include "profile/page.h"
#include "profile/group.h"
#include "profile/cputrace.h"
#include "profile/timebars.h"
#include "system/memops.h"
#include "system/threadtype.h"

#include "vector/color32.h"
// includes (debug)
#if 0
#include "system/findsize.h"			// 05/12/11		21/03/12	10/07/12	02/10/12	26/10/12
FindSize(rage::ptxEffectRule);			// 656 bytes	656			656			336			336
FindSize(rage::ptxEffectInst);			// 672 bytes	656			672			672			672
FindSize(rage::ptxEffectInstDB);		//   8 bytes	  8			  8			  8			  8
FindSize(rage::ptxEmitterRule);			// 864 bytes	880			880			400			400
FindSize(rage::ptxEmitterInst);			// 416 bytes	368			368			368			368
FindSize(rage::ptxEmitterInstDB);		//  48 bytes	 32			 32			 32			 32
FindSize(rage::ptxParticleRule);		// 364 bytes	368			368			368			400
FindSize(rage::ptxPointItem);			// 480 bytes	480			480			480			480
FindSize(rage::ptxPointSB);				//  80 bytes	 80			 80			 80			 80
FindSize(rage::ptxPointDB);				// 128 bytes	128			128			128			128
FindSize(rage::ptxKeyframeEntry);		//							 48			 32			 32
#endif


// optimisations
RMPTFX_OPTIMISATIONS()


// params
PARAM(rmptfxpath, "RMPTFX asset path");
PARAM(rmptfx_tty_windows, "Create RAG windows for rmptfx related TTY");
#if RMPTFX_BANK
XPARAM(ptfxOutputEffectInstPlayStateDebug);
#endif
#if !__FINAL
PARAM(ptfxassetoverride, "sets the override particle asset");
#endif

// channels
RAGE_DEFINE_CHANNEL(rmptfx);


// namespaces
using namespace rage;


// namespaces (again - this is fucked up!)
namespace rage 
{


// traces
RAGETRACE_DECL(RMPTFX_Update);
RAGETRACE_DECL(RMPTFX_UpdateSetup);
RAGETRACE_DECL(RMPTFX_UpdateParallel);
RAGETRACE_DECL(RMPTFX_UpdateFinalize);
RAGETRACE_DECL(RMPTFX_UpdateWait);


// static variables
char ptxManager::sm_assetPath[256] = "$\\tune\\rmptfx";
char ptxManager::sm_forcedColourShaderVarName[64] = "ForcedColor";
bool ptxManager::sm_updateOnSingleThread = false;
bool ptxManager::sm_singleUpdateTask = false;
bool ptxManager::sm_updateOnChildAndMainThreads = false;

#if RMPTFX_BANK
u8 ptxManager::sm_maxFrameSkipOcclusionTest = RMPTFX_PTXEFFECTINST_SKIP_OCCLUSION_FRAME_COUNT;
bool ptxManager::sm_skipOcclusionTestForEffectInstances = true;
ptxQualityState ptxManager::sm_ptxQualityState = PTX_QUALITY_HIRES;
#endif

ptxEmbeddedShader ptxManager::sm_embeddedShaders[] = 
{
	{"rmptfx_default", NULL},
	{"rmptfx_litsprite", NULL},
	{NULL, NULL}
};

grcRasterizerStateHandle	ptxManager::sm_drawManualRasterizerState;
grcDepthStencilStateHandle	ptxManager::sm_drawManualDepthStencilState;
grcBlendStateHandle			ptxManager::sm_drawManualBlendState;

grcRasterizerStateHandle	ptxManager::sm_exitDrawManualRasterizerState;
grcDepthStencilStateHandle	ptxManager::sm_exitDrawManualDepthStencilState;
grcBlendStateHandle			ptxManager::sm_exitDrawManualBlendState;

grcRasterizerStateHandle	ptxManager::sm_drawManualShadowsRasterizerState;
grcDepthStencilStateHandle	ptxManager::sm_drawManualShadowsDepthStencilState;
grcBlendStateHandle			ptxManager::sm_drawManualShadowsBlendState;

#if RMPTFX_EDITOR
grcGpuTimer g_gpuTimer;
#endif

mthVecRand g_ptxVecRandom;

int ptxManager::sm_debugLastSortPointsCount = 0;
void* ptxManager::sm_debugLastSortPointsBuffer = NULL;
void* ptxManager::sm_debugLastSortPointsBufferOrig = NULL;

template<> DICTIONARY_THREAD pgDictionary<rmcDrawable>* pgDictionary<rmcDrawable>::sm_CurrentDict = NULL;


// code
ptxManager::ptxManager()
{
	// pools

	// lists

	// maps

	// graphics
	sysMemSet(m_pFrameBuffer,        0, sizeof(m_pFrameBuffer));
	sysMemSet(m_pFrameBufferSampler, 0, sizeof(m_pFrameBufferSampler));
	sysMemSet(m_pDepthBuffer,        0, sizeof(m_pDepthBuffer));
	m_swapBuffers = true;
	
#if RSG_PC
	sysMemSet(m_bDepthWriteEnabled,   0, sizeof(m_bDepthWriteEnabled));
#endif

	// behaviours

	// functors
	m_effectInstInitFunctor = NULL;
	m_effectInstStartFunctor = NULL;
	m_effectInstStopFunctor = NULL;
	m_effectInstDeactivateFunctor = NULL;
	m_effectInstPreUpdateFunctor = NULL;
	m_effectInstPostUpdateFunctor = NULL;
	m_effectInstPreDrawFunctor = NULL;
	m_effectInstUpdateSetupFunctor = NULL;
	m_effectInstPostDrawFunctor = NULL;
	m_effectInstCullFunctor = NULL;
	m_effectInstUpdateCullingFunctor = NULL;
	m_effectInstSpawnFunctor = NULL;
	m_effectInstDistToCamFunctor = NULL;
	m_effectInstOcclusionFunctor = NULL;
	m_effectInstUpdateFinalizeFunctor = NULL;
	m_effectInstShouldBeRenderedFunctor = NULL;
	m_particleRuleInitFunctor = NULL;
	m_particleRuleShouldBeRenderedFunctor = NULL;
	m_shaderInitProgVarsFunctor = NULL;
	m_shaderSetProgVarsFunctor = NULL;
	m_overrideDepthStencilStateFunctor = NULL;
	m_overrideBlendStateFunctor = NULL;

	// synchronization objects
	m_callbackEffectsUpdateCompleteSema = sysIpcCreateSema(false);
	m_localEffectsUpdateStartedSema = sysIpcCreateSema(false);
	m_waitedForCallbackEffectUpdate = true;
	m_isDoingCallbackFenceUpdate = false;

	// settings
	m_highQualityEvo = 0.0f;

	// debug
#if __DEV	
	m_beginFrameCalled = false;
	m_isDrawing = 0;
	m_isUpdatingParallel = false;
#endif
	m_isUpdating = false;

	m_mainThreadId = sysIpcGetCurrentThreadId();

	// saving
#if __RESOURCECOMPILER || RMPTFX_EDITOR
	m_pSaveInfo = rage_new ptxSaveInfo(CreateFxListIterator());
	m_pSaveInfo->currState = PTXSAVESTATE_INACTIVE; 
#endif

	// editor
#if RMPTFX_EDITOR
	m_pInterface = rage_new ptxInterface;

	m_deltaTimeScalar = 1.0f;

	m_pvDebugEffectCameraMtx = NULL;
	m_pDebugEffectInst = NULL;
	m_debugEffectInstName[0] = '\0';
	m_vDebugEffectAnchorPos = Vec3V(V_ZERO);
	m_debugEffectAnimTimer = 0.0f;
	m_debugEffectRepeatTimer = 0.0f;

	m_numActiveInstances = 0;
	m_totalCPUUpdateTime = 0.0f;
	m_totalCPUDrawTime = 0.0f;
	m_totalGPUDrawTime = 0.0f;
#endif

	// xml loading
#if RMPTFX_XML_LOADING
	m_pLooseFxList = NULL;
#endif

#if RMPTFX_USE_PARTICLE_SHADOWS
	sysMemSet(m_ShadowPass, 0, sizeof(m_ShadowPass));
	sysMemSet(m_ShadowSourcePos, 0, sizeof(m_ShadowSourcePos));
#endif //RMPTFX_USE_PARTICLE_SHADOWS

#if GS_INSTANCED_SHADOWS
	m_UsingGeometryShader = false;
#endif
}

ptxManager::~ptxManager()
{
	// delete the loose rules fx list
#if RMPTFX_XML_LOADING
	m_pLooseFxList->Release();
	ptxAssertf(m_pLooseFxList->FindExternalReferenceCount()==0 && m_pLooseFxList->GetRefCount()==0, "the loose rules fx list still has references");
	delete m_pLooseFxList;
#endif

	for (int i=0; i<m_drawableEntries.GetCount(); i++)
	{
		// release any drawables that we own
		if ((*m_drawableEntries.GetItem(i))->GetIsOwnedByRMPTFX())
		{
			(*m_drawableEntries.GetItem(i))->GetDrawable()->Release();
		}

		// delete the drawable entry
		delete (*m_drawableEntries.GetItem(i));
	}

	// delete the draw lists
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		delete m_spawnLists[i];
		delete m_drawLists[i];
	}

#if RMPTFX_XML_LOADING
	// delete the behaviour map iterators
	for (atMap<const char*, ptxBehaviour*>::Iterator iter = m_behaviourMap.CreateIterator(); !iter.AtEnd(); iter.Next())
	{
		delete iter.GetData();
	}
	m_behaviourMap.Kill();
#endif

	// kill the behaviour maps
	m_behaviourFactoryMap.Kill();
	m_behaviourPlaceMap.Kill();

	// reset the draw lists
	m_drawLists.Reset();
	m_spawnLists.Reset();

	// reset the drawable entry list
	m_drawableEntries.Reset();

#if RMPTFX_EDITOR
	// delete the behaviours
	for (int i=0; i<m_behaviours.GetCount(); i++)
	{
		delete m_behaviours[i];
	}

	// delete the interface
	delete m_pInterface;
	m_pInterface = NULL;
#endif
}

void ptxManager::InitClass(int parallelUpdateScheduler)
{
	// create the tty output window
#if RMPTFX_BANK
	if (PARAM_rmptfx_tty_windows.Get())
	{
		BANKMGR.CreateOutputWindow("rmptfx", "NamedColor:Green"); 
	}
#endif

	// initialise default render state blocks
	InitRenderStateBlocks();

	// initialise the domain keyframe definitions
	ptxDomain::InitKeyframeDefns();

	ptxRenderState::InitClass();

	// initialise the pgBaseRefCounted classes
	ptxEffectRule::InitClass();
	ptxEmitterRule::InitClass();

	// initialise the behaviour class
	ptxBehaviour::InitClass();

	// instantiate singletons
	g_ptxManager::Instantiate();
	g_ptxThreadController::Instantiate();

	// generate the random number table
	ptxRandomTable::Generate();

	// initialise the update task
	ptxUpdateTask::Init(parallelUpdateScheduler);

	// initialise the parser
	INIT_PARSER;

	// editor 
#if RMPTFX_EDITOR
	// reserve space for 8 effect rule game flags
	RMPTFXMGR.m_effectRuleGameFlagNames.Reserve(8);

	// reserve space for 16 effect rule data volume type names
	RMPTFXMGR.m_effectRuleDataVolumeTypeNames.Reserve(16);

	// initialise the keyframe widget class
#if RMPTFX_BANK
	ptxKeyframeWidget::InitClass();
#endif
#endif

	// xml loading
#if RMPTFX_XML_LOADING
	// set up an fxlist for all of the 'loose' rules (ones loaded without an fxlist) to go
	RMPTFXMGR.m_pLooseFxList = ptxFxList::CreateEmptyFxlist();
	RMPTFXMGR.m_pLooseFxList->AddRef();
	RMPTFXMGR.PostLoadFxList(RMPTFXMGR.m_pLooseFxList, atHashWithStringNotFinal("loose",0x78753984), FXLISTFLAG_NONRESOURCED | FXLISTFLAG_LOOSE);
#endif

	// override the asset path with any params passed on the command line
	if (PARAM_rmptfxpath.Get())
	{
		const char* pNewPath = NULL;
		PARAM_rmptfxpath.Get(pNewPath);
		SetAssetPath(pNewPath);
	}

#if RMPTFX_BANK
	if (PARAM_ptfxOutputEffectInstPlayStateDebug.Get())
	{
		ptxDebug::sm_outputEffectInstPlayStateDebug = true;
	}
#endif

	// initialise embedded shaders
	for (int i=0; sm_embeddedShaders[i].m_pName!=NULL; i++)
	{
		grmShader* pShader = grmShaderFactory::GetInstance().Create();
		if (pShader) 
		{
			pShader->Load(sm_embeddedShaders[i].m_pName);
			sm_embeddedShaders[i].m_pShader = pShader;
		}
	}

	RMPTFXMGR.RegisterBehaviours();

	// settings
	RMPTFXMGR.m_highQualityEvo = 0.0f;
}

void ptxManager::ShutdownClass()
{
	// reset all effect insts
	RMPTFXMGR.ResetAllImmediately();

	// shutdown the pgBaseRefCounted classes
	ptxEffectRule::ShutdownClass();
	ptxEmitterRule::ShutdownClass();

	// shutdown the draw interface
	ptxDrawInterface::Shutdown();

	// destroy the singletons
	g_ptxThreadController::Destroy();
	g_ptxManager::Destroy();

	// shutdown embedded shaders
	for (int i=0; sm_embeddedShaders[i].m_pName!=NULL; i++)
	{
		delete sm_embeddedShaders[i].m_pShader;
		sm_embeddedShaders[i].m_pShader = NULL;
	}

	// shutdown the shader template list
	ptxShaderTemplateList::Shutdown();

	// shutdown the parser
	SHUTDOWN_PARSER;
}

void ptxManager::CreatePointPool(int numPoints)
{
	// create the point pool
	m_pointPool.Create(numPoints);
	
	// initialise the draw interface 
	ptxDrawInterface::Init();
}

CompileTimeAssertSize(ptxEmitterInstUpdateItem, 12, 16);

void ptxManager::CreateEffectInstPool(int numEffectInsts)
{
	// create the effect instance pools
	m_effectInstPool.Create(numEffectInsts);
	
	m_effectInstUpdateItems.Reserve(numEffectInsts);
	m_emitterInstUpdateItems.Resize(numEffectInsts*PTXEFFECTINST_MAX_EVENT_INSTS);
}

void ptxManager::BeginFrame()
{
	// Make sure we clear the semaphore somewhere
	if (!m_waitedForCallbackEffectUpdate)
	{
		PF_AUTO_PUSH_TIMEBAR("ptxManager - WaitForCallbackEffects");
		WaitForCallbackEffects();
	}
#if RMPTFX_EDITOR
	// copy the number of culled draw instances to the update
	m_numUpdateCulledInstances = m_numDrawCulledInstances;

	// do any interface message processing
	m_pInterface->Update();
#endif

	// return if we don't need to swap the buffers (e.g. if the delta time was zero)
	if (m_swapBuffers==false)
	{
		return;
	}

#if __DEV
	// make sure we aren't currently updating or drawing
	ptxAssertf(m_isUpdating==false, "trying to begin a new frame while the rmptfx is updating");
	ptxAssertf(m_isDrawing==0, "trying to begin a new frame while the rmptfx is drawing");

	// set a flag so we know we have been called
	m_beginFrameCalled = true;
#endif 
	
	// set up the critical sections
	sysCriticalSection localUpdateListCS(m_updateListCSToken);
	sysCriticalSection localFreeListCS(m_freeListCSToken);

	// swap the buffers
	RMPTFXTHREADER.SwapBuffers();

	// service the returned lists
	m_pointPool.EndFrame(NULL);
	m_effectInstPool.EndFrame(UnReferenceEffectInstItemCB);

	// clear the flag - we've now swapped the buffers
	m_swapBuffers = false;
}

void ptxManager::WaitForCallbackEffects()
{
	PF_FUNC(TimerUpdate_WaitOnCallbackEffects);

	sysIpcWaitSema(m_callbackEffectsUpdateCompleteSema);
	m_waitedForCallbackEffectUpdate = true;
}

void ptxManager::StartLocalEffectsUpdate()
{
	if(m_isDoingCallbackFenceUpdate)
	{
		sysIpcSignalSema(m_localEffectsUpdateStartedSema);
	}
}

/* 
Synchronization:

Key: ul = update list CS

DrawThread			ParticleThread			UpdateThread
---------------------------------------------------------
   |					 .						|
   |					 .						| (Main frame stuff)
   |					 .						|
   | 					 .						| (Kicks particle update)
   |					 .<---------------------|
   |					 |						|
   |					 | LOCK ul				| WAIT cbfx_done
   | (begin frame)		 | 						.
   |					 | (update cbfx)		.
   | LOCK ul			 |						.
   .					 | SIGNAL cbfx_done	--->.
   .					 |						|
   .					 | WAIT start_locfx		|
   .					 .						| (do defrag)
   .					 .						|
   .					 .						|
   .					 .<---------------------| SIGNAL start_locfx
   .					 | (update locfx)		|
   .					 |						|
   .					 | UNLOCK ul			|
   | (swap buffers)		 x						|
   |					 .						|
   | UNLOCK ul			 .						|
   |					 .						|
   |					 .						|
   |					 .						|
   |					 .						|
   |					 .						|
   |					 .						|

*/

void ptxManager::Update(float dt, const grcViewport* pViewport, ptxUpdateType updateType)
{
	PF_FUNC(TimerUpdate);

	// override with any debug fixed time step
#if __DEV
	static float fixedTimeStep = -1.0f;
	if (fixedTimeStep>0.0f)
	{
		dt = fixedTimeStep;
	}
#endif

	// scale by any editor time scalar
#if RMPTFX_EDITOR
	dt *= m_deltaTimeScalar;
#endif

	// deal with any saving that is active
#if RMPTFX_EDITOR
	if (m_pSaveInfo->currState != PTXSAVESTATE_INACTIVE)
	{
		SaveAllUpdate();
		sysIpcSignalSema(m_callbackEffectsUpdateCompleteSema);
		return; 
	}
#endif

	// don't bother updating if the delta time is zero
	if (dt==0.0f)
	{
		sysIpcSignalSema(m_callbackEffectsUpdateCompleteSema);
		return;	
	}

	// start a trace
	RAGETRACE_SCOPE(RMPTFX_Update);

	// do the update

#if __DEV
	// check we've been called correctly
	ptxAssertf(m_isUpdating==false, "trying to update rmptfx when it's already updating (is it being called from 2 threads?)");
	if (m_isUpdating)
	{
		// Don't signal the CallbackEffectUpdateComplete semaphore here - the other updater (if there was one) would.
		return;
	}

	ptxAssertf(m_beginFrameCalled , "trying to update without calling BeginFrame from a thread safe point first");
	m_beginFrameCalled = false;
#endif
	m_isUpdating = true;

	SYS_CS_SYNC(m_updateListCSToken);

	// store the update thread id
	m_updateThreadId = sysIpcGetCurrentThreadId();

	//Setup the key frame prop ID for the particle update thread
	SetupPtxKeyFramePropThreadId();

	// flag a buffer swap to happen 
	m_swapBuffers = true;

#if RMPTFX_EDITOR
	// start the profile update
	StartProfileUpdate();

	// start a timer
	sysTimer timer;
	timer.Reset();
#endif

	m_waitedForCallbackEffectUpdate = false;

	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		m_drawLists[i]->m_isAnyActivePoints[RMPTFXTHREADER.GetUpdateBufferId()] = false;
	}

	if (updateType==PTXUPDATETYPE_ALL_AT_ONCE)
	{
		PF_PUSH_TIMEBAR("ptfx Update all at once");
		UpdateSetup(dt, pViewport);
		PF_START(TimerUpdateCallback_ParallelAndFinalize);
		UpdateParallel(dt, PTXUPDATEPHASE_ALL_EFFECTS);
		UpdateFinalize(dt, PTXUPDATEPHASE_ALL_EFFECTS);
		PF_STOP(TimerUpdateCallback_ParallelAndFinalize);
		sysIpcSignalSema(m_callbackEffectsUpdateCompleteSema);
		UpdateCleanup(dt);
		PF_POP_TIMEBAR();
	}
	else
	{
		PF_PUSH_TIMEBAR("ptfx Callback Effects Update");
		m_isDoingCallbackFenceUpdate = true;
		UpdateSetup(dt, pViewport);
		if (m_numCallbackEmitterInstUpdateItems > 0 || m_numCallbackEffectInstUpdateItems > 0)
		{
			PF_START(TimerUpdateCallback_ParallelAndFinalize);
			UpdateParallel(dt, PTXUPDATEPHASE_CALLBACK_EFFECTS);
			UpdateFinalize(dt, PTXUPDATEPHASE_CALLBACK_EFFECTS);
			PF_STOP(TimerUpdateCallback_ParallelAndFinalize);
		}

		PF_POP_TIMEBAR();
		PF_START(TimerUpdateLocal_WaitOnEffectUpdateComplete);
		PF_PUSH_TIMEBAR_IDLE("WaitForSafeExecution");
		sysIpcSignalSema(m_callbackEffectsUpdateCompleteSema);
		sysIpcWaitSema(m_localEffectsUpdateStartedSema);
		PF_POP_TIMEBAR();
		PF_STOP(TimerUpdateLocal_WaitOnEffectUpdateComplete);

		PF_PUSH_TIMEBAR("ptfx Local Effects Update");
		PF_START(TimerUpdateLocal_ParallelAndFinalize);
		UpdateParallel(dt, PTXUPDATEPHASE_LOCAL_EFFECTS);
		UpdateFinalize(dt, PTXUPDATEPHASE_LOCAL_EFFECTS);
		PF_STOP(TimerUpdateLocal_ParallelAndFinalize);
		PF_POP_TIMEBAR();
		
		UpdateCleanup(dt);
		m_isDoingCallbackFenceUpdate = false;
	}


	// update the timer
#if RMPTFX_EDITOR
	m_totalCPUUpdateTime = timer.GetMsTime();
#endif

	m_isUpdating = false;

}

void ptxManager::UpdateSetup(float dt, const grcViewport* pViewport)
{
	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	m_currEmitterInstUpdateItemIdx = 0;
	m_emitterInstUpdateItems.Resize(0);

	// start a trace
	RAGETRACE_SCOPE(RMPTFX_UpdateSetup);

	// set up the profiler
	PF_FUNC(TimerUpdateSetup);

	m_effectInstUpdateItems.clear();
	m_numCallbackEffectInstUpdateItems = 0;
	m_numCallbackEmitterInstUpdateItems = 0;

	// go through the draw lists
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// start buffering the effect instance list
		m_drawLists[i]->m_effectInstList.BeginBuffering(RMPTFXTHREADER.GetUpdateBufferId());

		// go through the effect instance list
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// update the effect instance
			pCurrEffectInstItem->GetDataSB()->UpdateSetup(dt, pViewport);

			// add a work item to the array of effect insts - assuming that UpdateSetup didn't just deactivate the effect
			if (pCurrEffectInstItem->GetDataSB()->GetPlayState() != PTXEFFECTINSTPLAYSTATE_DEACTIVATED)
			{
				ptxEffectInstUpdateItem effectInstUpdateItem;
				effectInstUpdateItem.m_pEffectInst = pCurrEffectInstItem;
				effectInstUpdateItem.m_callbackUpdate = pCurrEffectInstItem->GetDataSB()->GetNeedsCallbackUpdate();
				if (effectInstUpdateItem.m_callbackUpdate)
				{
					m_numCallbackEffectInstUpdateItems++;
				}
				m_effectInstUpdateItems.Push(effectInstUpdateItem);
			}

			// set the next effect instance in the list
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}

	// sort the update task items and partition the effect inst items
	{
		PF_FUNC(TimerUpdateSetupSort);
		std::sort(m_emitterInstUpdateItems.begin(), m_emitterInstUpdateItems.end(), ptxEmitterInstUpdateItem::SortCompare);
		std::partition(m_effectInstUpdateItems.begin(), m_effectInstUpdateItems.end(), ptxEffectInstUpdateItem::PartitionCallbackInsts);
		PF_INCREMENTBY(CounterEmitterInstsTotal, m_emitterInstUpdateItems.size());
	}

	// count and the number of callback events in the update task items
	int callbackItems = 0;
	for (ptxEmitterInstUpdateItem* pEmitterInstUpdateItem=m_emitterInstUpdateItems.begin(); pEmitterInstUpdateItem!=m_emitterInstUpdateItems.end(); ++pEmitterInstUpdateItem)
	{
		ptxEffectInst* pEffectInst = pEmitterInstUpdateItem->m_pEffectInstItem->GetDataSB();
		if (pEffectInst->GetNeedsCallbackUpdate())
		{
			callbackItems++;
		}
		else
		{
			break;
		}
	}

	m_numCallbackEmitterInstUpdateItems = callbackItems;

#if __STATS
	for (ptxEmitterInstUpdateItem* pEmitterInstUpdateItem=m_emitterInstUpdateItems.begin(); pEmitterInstUpdateItem!=m_emitterInstUpdateItems.end(); ++pEmitterInstUpdateItem)
	{
		ptxEffectInst* pEffectInst = pEmitterInstUpdateItem->m_pEffectInstItem->GetDataSB();
		ptxEffectRule* pEffectRule = pEffectInst->GetEffectRule();
		ptxEmitterInst* pEmitterInst = pEffectInst->GetEventInst(pEmitterInstUpdateItem->m_eventIdx).GetEmitterInst();

		// check for items that aren't emitter events or don't have any particles
		if (pEffectRule->GetTimeline().GetEvent(pEmitterInstUpdateItem->m_eventIdx)->GetType()!=PTXEVENT_TYPE_EMITTER ||
			pEmitterInst->GetNumActivePoints()==0)
		{
			PF_INCREMENT(CounterEmitterInstsWontUpdate);
		}
		// check for items that are callback effects
		else if (pEffectInst->GetNeedsCallbackUpdate())
		{
			PF_INCREMENT(CounterEmitterInstsEarlyWait);
		}
		// check for other items (non-callback effects)
		else
		{
			PF_INCREMENT(CounterEmitterInstsLateWait);
		}
	}
#endif

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}


void ptxManager::UpdateParallel(float dt, ptxUpdatePhase updatePhase)
{
#if __DEV
	// we're now doing the parallel update
	m_isUpdatingParallel = true;
#endif

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	// start a trace
	RAGETRACE_SCOPE(RMPTFX_UpdateParallel);

	// set up the profiler
	PF_FUNC(TimerUpdateParallel);


	u32 lastUpdateItem = 0;
	switch(updatePhase)
	{
	case PTXUPDATEPHASE_ALL_EFFECTS:
		m_currEmitterInstUpdateItemIdx = 0;
		lastUpdateItem = (u32)m_emitterInstUpdateItems.GetCount();
		break;
	case PTXUPDATEPHASE_CALLBACK_EFFECTS:
		m_currEmitterInstUpdateItemIdx = 0;
		lastUpdateItem = m_numCallbackEmitterInstUpdateItems; 
		break;
	case PTXUPDATEPHASE_LOCAL_EFFECTS:
		m_currEmitterInstUpdateItemIdx = m_numCallbackEmitterInstUpdateItems;
		lastUpdateItem = (u32)m_emitterInstUpdateItems.GetCount(); 
		break;
	}

	// calculate how many child tasks we want to spawn
	int numChildTasksToSpawn = 0;
	if (!sm_updateOnSingleThread)
	{
		ptxAssertf(ptxUpdateTask::GetTaskCount()==0, "some particle update tasks (%d) are still in progress", ptxUpdateTask::GetTaskCount());
	
		numChildTasksToSpawn = RMPTFX_MAX_PARTICLE_UPDATE_TASKS-ptxUpdateTask::GetTaskCount();

		if (sm_singleUpdateTask)
		{
			numChildTasksToSpawn = Min(numChildTasksToSpawn, 1);
		}
	}

	// work out if we should process on the main thread
	bool processOnMainThread = numChildTasksToSpawn==0 || sm_updateOnChildAndMainThreads;

	// kick off the child tasks
	for (int i=0; i<numChildTasksToSpawn; i++)
	{
		ptxUpdateTask::Update(dt, lastUpdateItem);
	}

	// do main thread processing
	if (processOnMainThread)
	{
		while(true)
		{
			// main thread grabs one item from the array at a time, since it doesn't have the caching needs that worker threads do
			u32 onePastUpdateItemIndex = sysInterlockedIncrement(&m_currEmitterInstUpdateItemIdx);
			u32 updateItemIndex = onePastUpdateItemIndex-1;

			if (updateItemIndex>=lastUpdateItem)
			{
				break;
			}

			ptxEmitterInstUpdateItem& emitterInstUpdateItem = m_emitterInstUpdateItems[updateItemIndex];

			ptxEffectInst* pEffectInst = emitterInstUpdateItem.m_pEffectInstItem->GetDataSB();
			ptxEffectRule* pEffectRule = pEffectInst->GetEffectRule();
			ptxEmitterInst* pEmitterInst = pEffectInst->GetEventInst(emitterInstUpdateItem.m_eventIdx).GetEmitterInst();
			ptxEvolutionList* pEvoList = pEffectRule->GetTimeline().GetEvent(emitterInstUpdateItem.m_eventIdx)->GetEvolutionList();

			// skip any items that aren't emitter events or don't have any particles
			if (pEffectRule->GetTimeline().GetEvent(emitterInstUpdateItem.m_eventIdx)->GetType()!=PTXEVENT_TYPE_EMITTER ||
				pEmitterInst->GetNumActivePoints()==0)
			{
				continue;
			}

#if RMPTFX_EDITOR
			sysTimer timer;
			timer.Reset();
#endif

			pEmitterInst->UpdateParallel(pEffectInst, pEvoList, dt);

			// update the timer
#if RMPTFX_EDITOR
			pEffectRule->GetUIData().IncAccumCPUUpdateTime(timer.GetMsTime());
#endif
		}
	}

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

#if __DEV
	// we're no longer doing the parallel update
	m_isUpdatingParallel = false;
#endif
}

void ptxManager::UpdateFinalize(float dt, ptxUpdatePhase updatePhase)
{
	// wait for the update tasks to finish
	RAGETRACE_PUSH(RMPTFX_UpdateWait);
	ptxUpdateTask::WaitForAll();
	RAGETRACE_POP();

	PF_FUNC(TimerUpdateFinalize);

	u32 emitterInstUpdateItemIdxStart = 0;
	u32 emitterInstUpdateItemIdxEnd = 0;
	u32 effectInstUpdateItemIdxStart = 0;
	u32 effectInstUpdateItemIdxEnd = 0;
	switch(updatePhase)
	{
	case PTXUPDATEPHASE_ALL_EFFECTS:
		emitterInstUpdateItemIdxStart = 0; 
		emitterInstUpdateItemIdxEnd = m_emitterInstUpdateItems.GetCount();
		effectInstUpdateItemIdxStart = 0; 
		effectInstUpdateItemIdxEnd = m_effectInstUpdateItems.GetCount();
		break;
	case PTXUPDATEPHASE_CALLBACK_EFFECTS:
		emitterInstUpdateItemIdxStart = 0; 
		emitterInstUpdateItemIdxEnd = m_numCallbackEmitterInstUpdateItems;
		effectInstUpdateItemIdxStart = 0; 
		effectInstUpdateItemIdxEnd = m_numCallbackEffectInstUpdateItems;
		break;
	case PTXUPDATEPHASE_LOCAL_EFFECTS:
		emitterInstUpdateItemIdxStart = m_numCallbackEmitterInstUpdateItems; 
		emitterInstUpdateItemIdxEnd = m_emitterInstUpdateItems.GetCount();
		effectInstUpdateItemIdxStart = m_numCallbackEffectInstUpdateItems; 
		effectInstUpdateItemIdxEnd = m_effectInstUpdateItems.GetCount();
		break;
	}
	
	// PHASE 1 - UpdateFinalize each item
	for (u32 effectInstIdx=effectInstUpdateItemIdxStart; effectInstIdx!=effectInstUpdateItemIdxEnd; ++effectInstIdx)
	{
		// go through the effect instance list
		ptxEffectInstItem* pCurrEffectInstItem = m_effectInstUpdateItems[effectInstIdx].m_pEffectInst;

		// go through the effect instance list
		ptxEffectInst* pCurrEffectInst = pCurrEffectInstItem->GetDataSB();

		// update the effect instance
		pCurrEffectInst->UpdateFinalize(dt);

		// buffer the data
		// We should skip fx that are finished or deactivated
		if (pCurrEffectInst->GetIsFinished()==false && pCurrEffectInst->GetIsDeactivated()==false)
		{
			pCurrEffectInst->CopyToUpdateBuffer();

			int drawListIdx = pCurrEffectInst->GetDrawListIdx();
			if (drawListIdx>=m_drawLists.GetCount())
			{
				// deal with out of range indices
				ptxWarningf("Effect \"%s\" uses draw list %d which does not exist - using default. [EffectRule Drawlist: %d | Max Drawlist: %d]",
							(pCurrEffectInst->GetEffectRule() && pCurrEffectInst->GetEffectRule()->GetName() ? pCurrEffectInst->GetEffectRule()->GetName() : "<UnknownFx>"), drawListIdx,
							(pCurrEffectInst->GetEffectRule() ? pCurrEffectInst->GetEffectRule()->GetDrawListIdx() : -1), m_drawLists.GetCount()	);
				drawListIdx = 0;
			}

			m_drawLists[drawListIdx]->m_effectInstList.BufferData(RMPTFXTHREADER.GetUpdateBufferId(), pCurrEffectInstItem, true);

			ptxEffectInst* pFxInst = pCurrEffectInstItem->GetDataSB();

			if (pFxInst)
			{
				//Checking to see if any points are active (used for ptfx downsampling)
				if (!m_drawLists[drawListIdx]->m_isAnyActivePoints[RMPTFXTHREADER.GetUpdateBufferId()])
				{
					for (int j=0; j<pFxInst->GetNumEvents(); j++)
					{
						if (pFxInst->GetEventInst(j).GetEmitterInst()->GetNumActivePointsMT(RMPTFXTHREADER.GetUpdateBufferId())>0 && 
							pFxInst->GetCullState()==CULLSTATE_NOT_CULLED)
						{
							m_drawLists[drawListIdx]->m_isAnyActivePoints[RMPTFXTHREADER.GetUpdateBufferId()] = true;
							break;
						}
					}
				}

				if(m_effectInstUpdateFinalizeFunctor)
				{
					m_effectInstUpdateFinalizeFunctor(pFxInst);
				}
			}
		}

		// increment the stats
#if RMPTFX_EDITOR
		m_numActiveInstances++;
#endif
	}


	// PHASE 2
	// UpdateFinalize above might have added new effect instances to the spawn lists (because particles might have spawned new effect instances)
	// so loop over the effect instances again to make sure all effect instance data gets copied to the update buffer

	// go through the spawn lists
	for (int i=0; i<m_spawnLists.GetCount(); i++)
	{
		// go through the effect instance list
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_spawnLists[i]->m_effectInstList.GetHead();
		ptxAssertf((updatePhase != PTXUPDATEPHASE_LOCAL_EFFECTS || pCurrEffectInstItem == NULL), "No spawning allowed in the local effects phase!");
		while(pCurrEffectInstItem)
		{
			// Move the new effect onto the main draw list
			m_effectInstPool.Move(pCurrEffectInstItem, &m_spawnLists[i]->m_effectInstList, &m_drawLists[i]->m_effectInstList);

			// buffer the data
			// We should skip fx that are finished or deactivated
			if (pCurrEffectInstItem->GetDataSB()->GetIsFinished()==false && pCurrEffectInstItem->GetDataSB()->GetIsDeactivated()==false)
			{
				pCurrEffectInstItem->GetDataSB()->CopyToUpdateBuffer();
				m_drawLists[i]->m_effectInstList.BufferData(RMPTFXTHREADER.GetUpdateBufferId(), pCurrEffectInstItem, true);

				ptxEffectInst* pFxInst = pCurrEffectInstItem->GetDataSB();

				if(pFxInst)
				{
					//Checking to see if any points are active (used for ptfx downsampling)
					if(!m_drawLists[i]->m_isAnyActivePoints[RMPTFXTHREADER.GetUpdateBufferId()])
					{
						for(int j=0; j<pFxInst->GetNumEvents(); j++)
						{
							if(pFxInst->GetEventInst(j).GetEmitterInst()->GetNumActivePointsMT(RMPTFXTHREADER.GetUpdateBufferId()) > 0
								&& pFxInst->GetCullState() == CULLSTATE_NOT_CULLED)
							{
								m_drawLists[i]->m_isAnyActivePoints[RMPTFXTHREADER.GetUpdateBufferId()] = true;
								break;
							}
						}

					}

					if(m_effectInstUpdateFinalizeFunctor)
					{
						m_effectInstUpdateFinalizeFunctor(pFxInst);
					}
				}
			}

			// increment the stats
#if RMPTFX_EDITOR
			m_numActiveInstances++;
#endif

			// Get the next head-of-list item
			pCurrEffectInstItem = (ptxEffectInstItem*)m_spawnLists[i]->m_effectInstList.GetHead();
		}
	}
}

void ptxManager::UpdateCleanup(float UNUSED_PARAM(dt))
{
	// set up the profiler
	PF_FUNC(TimerUpdateFinalize);

	// go through the inactive effect instances making sure nothing has gone wrong
#if __DEV
	ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_effectInstList.GetHead();
	while (pCurrEffectInstItem)
	{
		// store the next effect instance in the list
		ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

		ptxAssertf(pCurrEffectInstItem->GetDataSB()->GetPlayState()==PTXEFFECTINSTPLAYSTATE_DEACTIVATED, "non deactivated effect found in the inactive list (%s, %d)", pCurrEffectInstItem->GetDataSB()->GetEffectRule()->GetName(), pCurrEffectInstItem->GetDataSB()->GetPlayState());

		// set the next effect instance in the list
		pCurrEffectInstItem = pNextEffectInstItem;
	}
#endif

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}

void ptxManager::Draw(ptxDrawList* pDrawList PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface) RMPTFX_BANK_ONLY(, bool bDebugDraw))
{
	// we're now drawing
#if __DEV 
	sysInterlockedIncrement(&m_isDrawing);
#endif

#if RMPTFX_EDITOR 
	// start timers
	sysTimer timer;
	timer.Reset();

	g_gpuTimer.Start();

	// start the profile draw
	StartProfileDraw();
#endif

	//Setup the key frame prop thread ID when we start rendering the particles. It can be done from different renderphases which could be run on different 
	//sub render threads
	RMPTFXMGR.SetupPtxKeyFramePropThreadId();
	// do pre drawing
	PreDraw();

	// start the profiler
	PF_START(TimerDraw);

	// check that we have effect instances to process
	unsigned num = pDrawList->m_effectInstList.GetNumItemsMT(RMPTFXTHREADER.GetDrawBufferId());
	ptxEffectInstItem** ppSorted = NULL; // shouldn't really need to initialize this, but some compilers (eg XB1) warn otherwise
	if (num > 0)
	{
		// sort and draw the effect instance lists
		ppSorted = Alloca(ptxEffectInstItem*, num);
#		if __ASSERT
			// If SortEffectInstList writes too many ptxEffectInstItem pointers,
			// it will blow the stack and corrupt the saved register containing
			// num, so store it off separately in the .bss (eg. B*1873866).
			static unsigned savedNum[NUMBER_OF_RENDER_THREADS];
			savedNum[g_RenderThreadIndex] = num;
#		endif
		const unsigned num2 = SortEffectInstList(ppSorted, &pDrawList->m_effectInstList);
		Assert(num2 <= savedNum[g_RenderThreadIndex]);
		num = num2;
		DrawEffectInstList(ppSorted, num PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));
	}

	// stop the profiler
	PF_STOP(TimerDraw);

	// do post drawing
	PostDraw();

#if RMPTFX_EDITOR
	m_totalCPUDrawTime = timer.GetMsTime();
	m_totalGPUDrawTime = g_gpuTimer.Stop();
#endif

	// we're no longer drawing
#if __DEV
	sysInterlockedDecrement(&m_isDrawing);
#endif 

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	// debug drawing
#if RMPTFX_BANK
	if (bDebugDraw && num > 0)
	{
		DebugDrawEffectInstList(ppSorted, num);
	}
#endif
}

void ptxManager::PreDraw()
{
	// set the global shader variables
	ptxShaderTemplateList::SetGlobalVars();

	// get the forced colour shader variable
	m_forcedColourShaderVar = grcEffect::LookupGlobalVar(sm_forcedColourShaderVarName, false);
}

void ptxManager::PostDraw()
{
	// reset the forced colour shader variable
	if (m_forcedColourShaderVar != grcegvNONE)
	{
		grcEffect::SetGlobalVar(m_forcedColourShaderVar, Vec4V(V_ONE));
	}
}

unsigned ptxManager::SortEffectInstList(ptxEffectInstItem** ppSorted, ptxList* pEffectInstList)
{
	u8 drawBufferId = RMPTFXTHREADER.GetDrawBufferId();

	Vec3V vCamPos;
#if RMPTFX_USE_PARTICLE_SHADOWS
	if (RMPTFXMGR.IsShadowPass())
		vCamPos = RMPTFXMGR.GetShadowSourcePos();
	else
#endif
		vCamPos = grcViewport::GetCurrent()->GetCameraPosition();

	struct KeyData
	{
		int                 dist;
		ptxEffectInstItem*  item;

		inline bool operator>(const KeyData& rhs) const
		{
			if (dist == rhs.dist)
			{
				return item > rhs.item;
			}
			return dist > rhs.dist;
		}

	};

	// go through the effect instances
	int numEffectInsts = pEffectInstList->GetNumItemsMT(drawBufferId);
	KeyData *kd = Alloca(KeyData, numEffectInsts);
	ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)pEffectInstList->GetHeadMT(drawBufferId);
	for (int i=0; i<numEffectInsts; i++)
	{
		// set the squared distance to the camera
		float dist;
		ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();
		if (pEffectInst->GetOverrideDistSqrToCamera())
		{
			if (pEffectInst->GetOverrideDistSqrToCameraAlways()==false)
			{
				// stop overriding the squared distance
				pEffectInst->SetOverrideDistSqrToCamera(-1.0f);
			}

			// use the overridden squared distance
			dist = pEffectInst->GetDataDBMT(drawBufferId)->GetDistSqrToCamera();
		}
		else
		{
			// calculate the actual squared distance to the camera
			dist = DistSquared(pEffectInst->GetCurrPos(), vCamPos).Getf();
		}

		// add to the sort buffer
		// the distance squared is always positive so just treat it as an int to avoid expensive fcmp
		// Could use PositiveFloatLessThan here, but we also want to check for equality
		kd[i].dist = FloatToIntBitwise(dist);
		kd[i].item = pCurrEffectInstItem;

		// set the next effect instance
		pCurrEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNextMT(drawBufferId);
	}

#if __ASSERT
	ptxAssertf(pCurrEffectInstItem==NULL, "effect inst list is invalid");
#endif

	// sort the buffer from furthest to closest
	std::sort(kd, kd+numEffectInsts, std::greater<KeyData>());

	for (int i=0; i<numEffectInsts; ++i)
	{
		ppSorted[i] = kd[i].item;
	}

	return numEffectInsts;
}

void ptxManager::DrawEffectInstList(ptxEffectInstItem** ppSorted, unsigned num PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface))
{
	// go through the effect instances
	const unsigned rti = g_RenderThreadIndex;
	for (unsigned i=0; i<num; ++i)
	{
		ptxEffectInstItem* pCurrEffectInstItem = ppSorted[i];
		Assert(pCurrEffectInstItem);
		ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();

		if(pEffectInst && pEffectInst->ShouldBeRendered(true))
		{
			// There can be a gigantic number of effect instances, so use a
			// tighter alloc scope around each one to prevent running out of
			// command buffer space.
			GRC_ALLOC_SCOPE_AUTO_PUSH_POP();

			// use default render setup if not overridden
			if (pEffectInst->GetRenderSetup()==NULL)
			{
				pEffectInst->SetRenderSetup(m_pRenderSetup[rti]);
			}

			// draw the effect instance
			pEffectInst->Draw(PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY(pMultipleDrawInterface));

		}

		// increment the stats
#if RMPTFX_EDITOR
		if (pCurrEffectInstItem->GetDataSB()->GetCullState()!=CULLSTATE_NOT_CULLED)
		{
			m_numDrawCulledInstances++;
		}
#endif
	}
}

void ptxManager::DrawManual(ptxEffectInst* pEffectInst PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface))
{
	//Make sure we setup  the key frame prop thread ID whenever Draw Manual is called. It can be done from different renderphases which could be run on different 
	//sub render threads
	RMPTFXMGR.SetupPtxKeyFramePropThreadId();
	if ( pEffectInst && pEffectInst->ShouldBeRendered(false) && ptxVerifyf(pEffectInst->GetFlag(PTXEFFECTFLAG_MANUAL_DRAW), "trying to manually draw an effect instance whose manual draw flag isn't set"))
	{
		SetDrawManualRenderState();

		// do pre drawing
		PreDraw();

		// use default render setup if not overridden
		if (pEffectInst->GetRenderSetup()==NULL)
		{
			pEffectInst->SetRenderSetup(m_pRenderSetup[g_RenderThreadIndex]);
		}

		// draw the effect instance
		pEffectInst->Draw(PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY(pMultipleDrawInterface));

		// do post drawing
		PostDraw();

		// restore previous render state
		SetExitDrawManualRenderState();
	}
}

void ptxManager::Reset(ptxEffectRule* pEffectRule)
{
#if __DEV 
	// this could be replaced with a critical section
	if (m_isUpdating)
	{
		ptxAssertf(0, "trying to reset an effect rule whilst rmptfx is updating");
		return;
	}
#endif

	// set up the critical section
	SYS_CS_SYNC(m_updateListCSToken);

	// go through the draw lists
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// start buffering the effect instance list
		m_drawLists[i]->m_effectInstList.BeginBuffering(RMPTFXTHREADER.GetUpdateBufferId());

		// go through the effect instance list
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// get the effect instance
			ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();
			
			// finish the effect inst if it matches the desired effect rule (or we're resetting everything)
			if (pEffectRule==NULL || pEffectRule==pEffectInst->GetEffectRule())
			{
				pEffectInst->Finish(false);
			}

			// buffer the data
			pCurrEffectInstItem->GetDataSB()->CopyToUpdateBuffer();			
			m_drawLists[i]->m_effectInstList.BufferData(RMPTFXTHREADER.GetUpdateBufferId(), pCurrEffectInstItem, true);

			// set the next effect instance in the list
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}

void ptxManager::ResetAllImmediately()
{
#if __DEV 
	// this could be replaced with a critical section
	if (m_isUpdating)
	{
		ptxAssertf(0, "trying to reset an effect rule whilst rmptfx is updating");
		return;
	}

	// this could be replaced with a critical section
	if (m_isDrawing)
	{
		ptxAssertf(0, "trying to reset an effect rule whilst rmptfx is drawing");
		return;
	}
#endif

	// set up the critical sections
	SYS_CS_SYNC(m_updateListCSToken);

	// go through the draw lists
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// go through the effect instance list
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();
			
			// finish the effect instance
			ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();
			pEffectInst->Finish(true);

			// unreference the effect as it won't get done through the normal route
			pEffectInst->UnReference();

			// set the next effect instance in the list
			pCurrEffectInstItem = pNextEffectInstItem;
		}

		// clear the effect instance list buffers
		m_drawLists[i]->m_effectInstList.ClearBuffers();
	}

	// reset the effect inst pool
	m_effectInstPool.Reset();

	// check that all fxlists have no references
#if __DEV
	ptxManager::OutputActiveEffectInsts();
#endif

#if __ASSERT
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start((u32)FXLISTFLAG_NONLOOSE); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		int refCount = fxListIterator.GetCurrFxList()->GetRefCount();
		ptxAssertf(refCount==0, "fxlist still has references (%s - %d) - see TTY for active effects", fxListIterator.GetCurrFxList()->GetName(), refCount);
	}
#endif

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}

#if RMPTFX_BANK
void ptxManager::InitDebug(bkBank* pVfxBank, bkGroup* pParticleGroup)
{
	ptxDebug::Init(pVfxBank, pParticleGroup);
}
#endif

#if RMPTFX_BANK
void ptxManager::UpdateDebug(float RMPTFX_EDITOR_ONLY(dt))
{
	ptxDebug::Update();

	// update the debug effect
#if RMPTFX_EDITOR
	UpdateDebugEffect(dt);
#endif
}
#endif

ptxDrawList* ptxManager::CreateDrawList(const char* pName)
{
	// create a new draw list
	ptxDrawList* pDrawList = rage_new ptxDrawList();
	ptxDrawList* pSpawnList = rage_new ptxDrawList();

	// add it to the draw list array
	m_drawLists.PushAndGrow(pDrawList, 1);
	m_spawnLists.PushAndGrow(pSpawnList, 1);

	// set its index and name
	pDrawList->SetIndex(m_drawLists.GetCount()-1);
	pSpawnList->SetIndex(m_spawnLists.GetCount()-1);
	formatf(pDrawList->GetName(), 128, "%s", pName);
	formatf(pSpawnList->GetName(), 128, "%s", pName);

	// return the draw list pointer
	return pDrawList;
}

ptxFxList* ptxManager::LoadFxList(const char* pFilename, u32 fxListFlags)
{
	// TODO: if we force this function to only be called during dma then we don't have to lock

	// set up the critical sections
	SYS_CS_SYNC(m_updateListCSToken);

#if __DEV
	// start a timer
	sysTimer timer;
	timer.Reset();
#endif 	

	// clear the resourced and non-resourced flags - we determine those below
	fxListFlags &= ~(FXLISTFLAG_NONRESOURCED | FXLISTFLAG_RESOURCED); 

	// create a pointer to an fx list
	ptxFxList* pFxList = NULL;

	// load in the fx list
	const char* pExtension = ASSET.FindExtensionInPath(pFilename);
	if (pExtension && strcmp(pExtension, ".fxlist"))
	{
		// load in from resource file
		ASSET.PushFolder(sm_assetPath);
		ASSET.PushFolder("fxlists");
		pgRscBuilder::Load(pFxList, pFilename, "#pfl", ptxFxList::RORC_VERSION);
		fxListFlags |= FXLISTFLAG_RESOURCED;
		ASSET.PopFolder();
		ASSET.PopFolder();
	}
	else
	{
		// load from xml
#if RMPTFX_XML_LOADING
		char modelPath[RAGE_MAX_PATH];
		formatf(modelPath, "%s/models", GetAssetPath());

		char texturePath[RAGE_MAX_PATH];
		formatf(texturePath, "%s/textures;%s/modeltex", GetAssetPath(), GetAssetPath());

		char baseName[RAGE_MAX_PATH];
		formatf(baseName, "%s/fxlists/%s/%s", GetAssetPath(), pFilename, pFilename);

		pFxList = rage_new ptxFxList();
		pFxList->Load(baseName, modelPath, texturePath);
		fxListFlags |= FXLISTFLAG_NONRESOURCED;
#endif
	}

	// check for any errors
	if (pFxList==NULL)
	{
		ptxMsg::AssetErrorf("could not load particle assets (%s)\n", pFilename);
		return NULL;
	}

	// do the post load
	PostLoadFxList(pFxList, pFilename, fxListFlags);

	// output load information
#if __DEV
	ptxDisplayf("fxlist (%s) load time was %6.4fms", pFilename, timer.GetMsTime());
#endif

	// return the loaded fx list
	return pFxList;
}

void ptxManager::PostLoadFxList(ptxFxList* pFxList, const atHashWithStringNotFinal& fxListName, u32 fxListFlags)
{
	// check if the fx list is already in the map
	if (/*ptxFxListMap::DataType* pData = */ m_fxListMap.Access(fxListName))
	{
		// it is - should we add a reference? (See atReferenceCounter and/or pgBaseRefCounted)
	}
	else
	{
		// it isn't

		// make sure the flags are valid
		if ((fxListFlags & FXLISTFLAG_RESOURCED) == 0)
		{
			fxListFlags |= FXLISTFLAG_NONRESOURCED;
		}

		if ((fxListFlags & FXLISTFLAG_SHAREABLE) == 0)
		{
			fxListFlags |= FXLISTFLAG_NONSHAREABLE;
		}

		if ((fxListFlags & FXLISTFLAG_LOOSE) == 0)
		{
			fxListFlags |= FXLISTFLAG_NONLOOSE;
		}

		if ((fxListFlags & FXLISTFLAG_UNDERCONSTRUCTION) == 0)
		{
			fxListFlags |= FXLISTFLAG_NOTUNDERCONSTRUCTION;
		}

		// create a new fx list info
		ptxFxListInfo fxListInfo;
		fxListInfo.m_pFxList = pFxList;
		fxListInfo.m_flags = fxListFlags;
		
		// add it to the map
		m_fxListMap.Insert(fxListName, fxListInfo);
	}

	pFxList->PostLoad(fxListFlags);

	// send the fx list rules to the editor
#if RMPTFX_EDITOR
	m_pInterface->SendRulesLists();
#endif
}

void ptxManager::UnloadFxList(ptxFxList* pFxList, bool isResourced)
{
	ptxDebugf1("Unloading FX List %s", pFxList->GetName());
	DIAG_CONTEXT_MESSAGE("Unloading fxlist %s", pFxList->GetName());
	// do the pre unload steps
	PreUnloadFxList(pFxList);

	// update the resourcing system, if required
	if (isResourced)
	{
		pgRscBuilder::Delete(pFxList);
	}

	// do the post unload steps
	PostUnloadFxList();
	ptxDebugf1("Done unloading FX List");
}

void ptxManager::PreUnloadFxList(ptxFxList* pFxList)
{
	// finish effect insts that belong to this fx list
	FinishEffectInsts(pFxList);

	// reset effect rules that belong to this fx list
	for (int i=0; i<pFxList->GetEffectRuleDictionary()->GetCount(); i++)
	{
		ptxEffectRule* pEffectRule = pFxList->GetEffectRuleDictionary()->GetEntry(i);
		pEffectRule->Reset();
	}

	// search for the fx list in the map
	ptxFxListMap::Iterator fxListMapIterator = m_fxListMap.CreateIterator();
	for (fxListMapIterator.Start(); !fxListMapIterator.AtEnd(); fxListMapIterator.Next())
	{
		if (fxListMapIterator.GetData().m_pFxList==pFxList)
		{
			break;
		}
	}

	// delete the fx list from the map
	if (ptxVerifyf(!fxListMapIterator.AtEnd(), "couldn't find fx list in the map"))
	{
		// we should probably move all the unloading code below into this scope, but for now lets just focus on maintaining our fxlist map
		// we would ideally check the reference count here before removing from the map and deleting the fxlist, but for now we'll simply remove it from the map
		ASSERT_ONLY(bool deletedOk =) 
		m_fxListMap.Delete(fxListMapIterator.GetKey());
		ptxAssertf(deletedOk, "couldn't delete the fx list from the map");
	}
}

void ptxManager::PostUnloadFxList()
{
	// send the fx list rules to the editor
#if RMPTFX_EDITOR
	m_pInterface->SendRulesLists();
#endif
}

ptxEffectInst* ptxManager::GetEffectInst(const atHashWithStringNotFinal effectRuleHashName, const atHashWithStringNotFinal fxListName)
{
	ptxFxListInfo* pFxListInfo = m_fxListMap.Access(fxListName);
	if (ptxVerifyf(pFxListInfo && pFxListInfo->m_pFxList, "requesting effect rule %s from fxlist %s but the fxlist is not loaded", effectRuleHashName.GetCStr(), fxListName.GetCStr()))
	{
		return GetEffectInst(effectRuleHashName, pFxListInfo->m_pFxList);	
	}

	return NULL;
}

ptxEffectInst* ptxManager::GetEffectInst(const atHashWithStringNotFinal effectRuleHashName, ptxFxList* pFxList, u32 effectInstflags)
{
	ptxEffectRule* pEffectRule = GetEffectRule(effectRuleHashName, pFxList);
	if (pEffectRule)
	{
		return ptxManager::GetEffectInst(pEffectRule, effectInstflags);
	}

	return NULL;
}

ptxEffectInst* ptxManager::GetEffectInst(ptxEffectRule* pEffectRule, u32 effectInstflags)
{
	// check for valid data
	ptxAssertf(pEffectRule, "invalid effect rule passed");

	// set up the critical section
	SYS_CS_SYNC(m_freeListCSToken);

	// try to get an effect instance form the pool
	ptxEffectInst* pEffectInst = GetEffectInstFromPool();
	if (pEffectInst==NULL)
	{
		ptxWarningf("cannot get an effect instance from the pool - it is empty");
	}
	else
	{
		// initialise the effect instance
		pEffectInst->Init(pEffectRule, effectInstflags);
	}

	// return the effect instance
	return pEffectInst;
}

void ptxManager::ActivateEffectInst(ptxEffectInst* pEffectInst)
{
	// set up the critical section
	SYS_CS_SYNC_CONDITIONAL(m_updateListCSToken, !IsParticleUpdateThreadOrTask());

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	ptxAssertf(pEffectInst, "trying to activate an invalid effect inst");

	if (pEffectInst)
	{
		// we should only be activating effects that are waiting to activate
		ptxAssertf(pEffectInst->GetPlayState()==PTXEFFECTINSTPLAYSTATE_WAITING_TO_ACTIVATE, "trying to activate an effect inst that isn't waiting to activate");

		// check we have created at least one draw list
		ptxAssertf(m_drawLists.GetCount()>0, "can't activate an effect instance if there are no draw lists");

		// get the draw list index from the effect instance
		int drawListIdx = pEffectInst->GetDrawListIdx();
		if (drawListIdx>=m_drawLists.GetCount())
		{
			// deal with out of range indices
			ptxWarningf("Effect \"%s\" uses draw list %d which does not exist - using default. [EffectRule Drawlist: %d | Max Drawlist: %d]",
						(pEffectInst->GetEffectRule() && pEffectInst->GetEffectRule()->GetName() ? pEffectInst->GetEffectRule()->GetName() : "<UnknownFx>"), drawListIdx,
						(pEffectInst->GetEffectRule() ? pEffectInst->GetEffectRule()->GetDrawListIdx() : -1), m_drawLists.GetCount()	);
			drawListIdx = 0;
		}

		// check that the effect instance is in the correct list 
#if RMPTFX_THOROUGH_ERROR_CHECKING
		ptxAssertf(m_drawLists[drawListIdx]->m_effectInstList.IsInList(pEffectInst->GetEffectInstItem())==false, "effect inst item is in the draw list already");
		ptxAssertf(m_effectInstList.IsInList(pEffectInst->GetEffectInstItem()), "effect inst item is not in the inactive list");
#endif

		// move the effect instance item from the inactive list to the draw list (or spawn list, if we're doing this while updating other effects)
		ptxList* destList = m_isUpdating ? &m_spawnLists[drawListIdx]->m_effectInstList : &m_drawLists[drawListIdx]->m_effectInstList;
		m_effectInstPool.Move(pEffectInst->GetEffectInstItem(), &m_effectInstList, destList);

		// flag as being activated
		pEffectInst->SetPlayState(PTXEFFECTINSTPLAYSTATE_ACTIVATED);

		// allocate any memory
		pEffectInst->AllocateMemory();

		// update the referencing
		pEffectInst->GetEffectRule()->AddRef();
		ptxAssertf(pEffectInst->GetEffectRule()->GetFxList(), "effect inst doesn't belong to an fx list");
		pEffectInst->GetEffectRule()->GetFxList()->AddRef();
	}

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}

void ptxManager::DeactivateEffectInst(ptxEffectInst* pEffectInst)
{
	// set up the critical section
	SYS_CS_SYNC_CONDITIONAL(m_updateListCSToken, !IsParticleUpdateThreadOrTask());

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	// we should only be deactivating effects that are finished
	ptxAssertf(pEffectInst->GetPlayState()==PTXEFFECTINSTPLAYSTATE_FINISHED, "trying to deactivate an effect inst that isn't finished");

	// check for errors
	if (pEffectInst->GetEffectRule()==NULL) 
	{
		ptxErrorf("trying to deactivate an effect instance that isn't set up");
		return;
	}

#if __DEV
	if (m_isUpdating)
	{
		if (m_updateThreadId != sysIpcGetCurrentThreadId())
		{
			ptxErrorf("trying to deactivate an effect instance (%s) from a different thread whilst update is in progress", pEffectInst->GetEffectRule()->GetName());		
			return;
		}
	}
#endif

	// tidy up effect instance data
	pEffectInst->SetSpawningPointItem(NULL);
	pEffectInst->SetSpawningEffectInst(NULL);
	pEffectInst->ClearCollisionSet();

	// deallocate any memory
	pEffectInst->DeallocateMemory();

	// do any user recycling callbacks
	pEffectInst->Deactivate();

	// get the draw list index from the effect instance
	int drawListIdx = pEffectInst->GetDrawListIdx();
	if (drawListIdx>=m_drawLists.GetCount())
	{
		// deal with out of range indices
		drawListIdx = 0;
	}

	// check that the effect instance item exists in the draw list
#if RMPTFX_THOROUGH_ERROR_CHECKING
	ptxAssertf(m_drawLists[drawListIdx]->m_effectInstList.IsInList(pEffectInst->GetEffectInstItem()), "trying to deactivate an effect instance that isn't in the draw list");
#endif

	// remove the effect instance item from the draw list
	if (m_spawnLists[drawListIdx]->m_effectInstList.IsInList(pEffectInst->GetEffectInstItem()))
	{
		m_effectInstPool.ReturnItem(&m_spawnLists[drawListIdx]->m_effectInstList, pEffectInst->GetEffectInstItem());
	}
	else
	{
		m_effectInstPool.ReturnItem(&m_drawLists[drawListIdx]->m_effectInstList, pEffectInst->GetEffectInstItem());
	}

	// we're now finished and no longer active
	pEffectInst->SetPlayState(PTXEFFECTINSTPLAYSTATE_DEACTIVATED);

	// we can't unreference the effect instance now as we need to keep the fx list pointers around for a bit longer
	// pEffectInst->UnReference();

	// clear the flags
	pEffectInst->ClearFlags();


#if RMPTFX_EDITOR
	if (IsDebugEffect(pEffectInst))
	{
		DeactivateDebugEffect();
	}
#endif

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}

ptxEffectInst* ptxManager::GetEffectInstFromPool()
{
	// check that there are some effect instance in the pool
	if (m_effectInstPool.GetNumItemsFree()==0)
	{
		// there aren't
		return NULL;
	}

	// get an effect instance item from the pool
	ptxEffectInstItem* pEffectInstListItem = (ptxEffectInstItem*)m_effectInstPool.GetNewItem(&m_effectInstList);
	if (pEffectInstListItem)
	{
		// get the item's effect instance
		ptxEffectInst* pEffectInst = pEffectInstListItem->GetDataSB();

		// set up some data on the effect instance
		pEffectInst->SetEffectInstItem(pEffectInstListItem);

		// return the effect instance
		return pEffectInst;
	}

	// we couldn't get an effect instance
	return NULL;
}

void ptxManager::MoveEffectInst(ptxEffectInst* pEffectInst, int drawListIdx)
{
#if __DEV 
	// this could be replaced with a critical section
	if (m_isUpdating)
	{
		ptxAssertf(0, "trying to move an effect rule whilst rmptfx is updating");
		return;
	}

	// this could be replaced with a critical section
	if (m_isDrawing)
	{
		ptxAssertf(0, "trying to move an effect rule whilst rmptfx is drawing");
		return;
	}
#endif

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	// check for errors
#if __DEV
	if (m_isUpdating)
	{
		if (m_updateThreadId != sysIpcGetCurrentThreadId())
		{
			ptxErrorf("trying to move an effect instance (%s) from a different thread whilst update is in progress", pEffectInst->GetEffectRule()->GetName());
			return;
		}
	}
#endif

	ptxAssertf(drawListIdx>=0 && drawListIdx<m_drawLists.GetCount(), "invalid draw list index");

	// we should only be moving effects that are playing, stopped or finished
	ptxAssertf(pEffectInst->GetPlayState()==PTXEFFECTINSTPLAYSTATE_PLAYING ||
			   pEffectInst->GetPlayState()==PTXEFFECTINSTPLAYSTATE_STOPPED_MANUALLY ||
			   pEffectInst->GetPlayState()==PTXEFFECTINSTPLAYSTATE_STOPPED_NATURALLY ||
			   pEffectInst->GetPlayState()==PTXEFFECTINSTPLAYSTATE_FINISHED, "trying to move an effect inst with an invalid state (%d)", pEffectInst->GetPlayState());

	if (pEffectInst)
	{
		// check that the effect instance is in the correct list 
#if RMPTFX_THOROUGH_ERROR_CHECKING
		ptxAssertf(m_drawLists[pEffectInst->GetDrawListIdx()]->m_effectInstList.IsInList(pEffectInst->GetEffectInstItem()), "effect inst item is not in the old draw list");
		ptxAssertf(m_drawLists[drawListIdx]->m_effectInstList.IsInList(pEffectInst->GetEffectInstItem())==false, "effect inst item is in the new draw list");
#endif

		// move from the old to new draw list
		m_effectInstPool.Move(pEffectInst->GetEffectInstItem(), &m_drawLists[pEffectInst->GetDrawListIdx()]->m_effectInstList,&m_drawLists[drawListIdx]->m_effectInstList);

		// set the new draw list index on the effect instance
		pEffectInst->SetDrawListIdx(drawListIdx);
	}

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}

void ptxManager::FinishEffectInsts(ptxEffectRule* pEffectRule)
{
	// TODO: if we force this function to only be called during dma then we don't have to lock

	// set up the critical sections
	SYS_CS_SYNC_CONDITIONAL(m_updateListCSToken, !IsParticleUpdateThreadOrTask());

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	// go through the draw lists
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// go through the effect instance list
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// get the effect instance
			ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();

			// finish the effect inst if it matches the desired effect rule
			if (pEffectInst->GetEffectRule()==pEffectRule)
			{
				pEffectInst->Finish(false);
			}

			// set the next effect instance in the list
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}

void ptxManager::FinishEffectInsts(ptxFxList* pFxList)
{
	// TODO: if we force this function to only be called during dma then we don't have to lock

	// set up the critical sections
	SYS_CS_SYNC_CONDITIONAL(m_updateListCSToken, !IsParticleUpdateThreadOrTask());

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	// go through the draw lists
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// go through the effect instance list
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// get the effect instance
			ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();

			// finish the effect inst if it belongs to the desired fx list
			if (pEffectInst->GetEffectRule()->GetFxList()==pFxList)
			{
				pEffectInst->Finish(true);
			}

			// set the next effect instance in the list
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}

void ptxManager::UnReferenceEffectInstItemCB(ptxListItemBase* pPtxListItem)
{
	// unreference the effect instance
	ptxEffectInstItem* pEffectInstItem = (ptxEffectInstItem*)pPtxListItem;
	pEffectInstItem->GetDataSB()->UnReference();
}

ptxEffectRule* ptxManager::GetEffectRule(const atHashWithStringNotFinal effectRuleHashName, const atHashWithStringNotFinal fxListName)
{
	ptxFxListInfo* pFxListInfo = m_fxListMap.Access(fxListName);
	if (ptxVerifyf(pFxListInfo && pFxListInfo->m_pFxList, "requesting effect rule %s from fxlist %s but the fxlist is not loaded", effectRuleHashName.GetCStr(), fxListName.GetCStr()))
	{
		return GetEffectRule(effectRuleHashName, pFxListInfo->m_pFxList);	
	}

	return NULL;
}

ptxEffectRule* ptxManager::GetEffectRule(const atHashWithStringNotFinal effectRuleHashName, ptxFxList* pFxList, u32 fxListFlags)
{
	// if an fx list is specified look up the effect rule in it
	if (pFxList)
	{
		ptxEffectRule* pEffectRule = pFxList->GetEffectRuleDictionary()->LookupLocal(effectRuleHashName);
		if (pEffectRule)
		{
			// found it
			return pEffectRule;
		}

		// the effect rule wasn't found
		ptxAssertf(0, "effect rule %s does not exist in fxlist %s", effectRuleHashName.TryGetCStr(), pFxList->GetName());
		return NULL;
	}
	else
	{
		// no fx list specified - search for the effect rule all fx lists
		ptxFxListIterator fxListIterator = CreateFxListIterator();
		for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
		{
			ptxEffectRule* pEffectRule = fxListIterator.GetCurrFxList()->GetEffectRuleDictionary()->LookupLocal(effectRuleHashName);
			if (pEffectRule)
			{
				// found it
				return pEffectRule;
			}
		}

		// the effect rule wasn't found
		ptxAssertf(0, "effect rule %s does not exist in any fxlist", effectRuleHashName.TryGetCStr());
		return NULL;
	}
}

ptxEmitterRule* ptxManager::GetEmitterRule(const atHashWithStringNotFinal emitterRuleHashName, ptxFxList* pFxList, u32 fxListFlags)
{
	// if an fx list is specified look up the emitter rule in it
	if (pFxList)
	{
		return pFxList->GetEmitterRuleDictionary()->LookupLocal(emitterRuleHashName);
	}

	// no fx list specified - search for the emitter rule all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxEmitterRule* pEmitterRule = fxListIterator.GetCurrFxList()->GetEmitterRuleDictionary()->LookupLocal(emitterRuleHashName);
		if (pEmitterRule)
		{
			// found it
			return pEmitterRule;
		}
	}

	ptxAssertf(0, "emitter rule (%s) is not loaded", emitterRuleHashName.TryGetCStr());

	// the emitter rule wasn't found
	return NULL;
}

ptxParticleRule* ptxManager::GetParticleRule(const atHashWithStringNotFinal particleRuleHashName, ptxFxList* pFxList, u32 fxListFlags)
{
	// if an fx list is specified look up the particle rule in it
	if (pFxList)
	{
		return pFxList->GetParticleRuleDictionary()->LookupLocal(particleRuleHashName);
	}

	// no fx list specified - search for the particle rule all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxParticleRule* pParticleRule = fxListIterator.GetCurrFxList()->GetParticleRuleDictionary()->LookupLocal(particleRuleHashName);
		if (pParticleRule)
		{
			// found it
			return pParticleRule;
		}
	}

	ptxAssertf(0, "particle rule (%s) is not loaded", particleRuleHashName.TryGetCStr());

	// the particle rule wasn't found
	return NULL;
}

void ptxManager::SetRenderSetup(ptxRenderSetup *pRenderSetup)
{
	if (g_IsSubRenderThread)
	{
		m_pRenderSetup[g_RenderThreadIndex] = pRenderSetup;
	}
	else
	{
		for (unsigned i=0; i<NELEM(m_pRenderSetup); ++i)
		{
			m_pRenderSetup[i] = pRenderSetup;
		}
	}
}

grcRenderTarget* ptxManager::GetFrameBuffer()
{
	// if we have a frame buffer return it
	grcRenderTarget* rt = m_pFrameBuffer[g_RenderThreadIndex];
	if (rt)
	{
		return rt;
	}

	// no back buffer set
	return NULL;
}

grcRenderTarget* ptxManager::GetFrameBufferSampler()
{
	// if we have a frame buffer return it
	grcRenderTarget* rt = m_pFrameBufferSampler[g_RenderThreadIndex];
	if (rt)
	{
		return rt;
	}

	// no back buffer sampler set
	return NULL;
}

grcRenderTarget* ptxManager::GetDepthBuffer()
{
	// if we have a depth buffer return it
	grcRenderTarget *rt = m_pDepthBuffer[g_RenderThreadIndex];
	if (rt)
	{
		return rt;
	}

	// no depth buffer set
	return NULL;
}

rmcDrawable* ptxManager::LoadModel(const char* pFilename, bool reload)
{
	// create a drawable pointer to load the model into
	rmcDrawable* pDrawable = NULL;

	// check for valid data
	if (ptxVerifyf(pFilename, "invalid model filename passed"))
	{
		// create an fx list iterator
		ptxFxListIterator fxListIterator = CreateFxListIterator();

#if RMPTFX_XML_LOADING
		// first check for models in the fx list we're building
		for (fxListIterator.Start(FXLISTFLAG_UNDERCONSTRUCTION); !fxListIterator.AtEnd(); fxListIterator.Next())
		{
			pDrawable = fxListIterator.GetCurrFxList()->GetModelDictionary()->LookupLocal(pFilename);
			if (pDrawable)
			{
				return pDrawable;
			}
		}
#endif

		// then check in any shareable fx lists
		for (fxListIterator.Start(FXLISTFLAG_SHAREABLE); !fxListIterator.AtEnd(); fxListIterator.Next())
		{
			pDrawable = fxListIterator.GetCurrFxList()->GetModelDictionary()->LookupLocal(pFilename);
			if (pDrawable)
			{
				return pDrawable;
			}
		}

		// then check the global drawable dictionaries
		if (pgDictionary<rmcDrawable>::GetCurrent())
		{
			pDrawable = pgDictionary<rmcDrawable>::GetCurrent()->Lookup(pFilename);
			if (pDrawable)
			{
				return pDrawable;
			}
		}

		// early out if building a resource - we don't want to load the model up
		if (sysMemAllocator::GetCurrent().IsBuildingResource())
		{
			ptxWarningf("couldn't find model %s while creating resource - hopefully it exists in another resource", pFilename);
			return NULL;
		}

		// check if the drawable entry exists in the drawable map
		datOwner<ptxDrawableEntry>* pDrawableEntry = m_drawableEntries.SafeGet(pFilename);
		if (pDrawableEntry && *pDrawableEntry)
		{
#if !RMPTFX_EDITOR
			(void)reload;
#else
			// check if we need to reload the drawable
			if (reload)
			{
				// we do - don't return
				pDrawable = (*pDrawableEntry)->GetDrawable();
			}
			else
#endif
			{
				// we don't - just return this one
				return (*pDrawableEntry)->GetDrawable();
			}
		}

		// load up the drawable - it doesn't exists anywhere yet
		ptxDrawableEntry* pNewDrawableEntry = NULL;
		pNewDrawableEntry = LoadDrawable(pFilename);

		if (pNewDrawableEntry)
		{
			// insert the new, replacing the old, if present
			m_drawableEntries.InsertSorted(pFilename, pNewDrawableEntry);

#if RMPTFX_EDITOR
			// deal with drawables being reloaded
			if (pDrawable)
			{
				// find all the models and replace the old with the new
				ptxParticleRuleIterator particleRuleIterator(CreateFxListIterator());
				for (particleRuleIterator.Start((u32)FXLISTFLAG_ALL); !particleRuleIterator.AtEnd(); particleRuleIterator.Next())
				{
					ptxParticleRule* pParticleRule = particleRuleIterator.GetCurrRule();
					pParticleRule->ReplaceModel(pFilename, pNewDrawableEntry->GetDrawable());
				}

				// release our reference to the drawable - the particle rules now own it
				pDrawable->Release();
			}
#endif

			pDrawable = pNewDrawableEntry->GetDrawable();
		}
#if RMPTFX_EDITOR
		else
		{
			// null the drawable - we may have set it up if it needed reloaded
			pDrawable = NULL;
		}
#endif
	}

	// return the drawable
	return pDrawable;
}

ptxDrawableEntry* ptxManager::LoadDrawable(const char* pFilename)
{
	ptxDrawableEntry* pDrawableEntry = NULL;

	// parse out the folder name and model name
	const char* pSlashChar = strchr(pFilename, '/');
	const char* pModelName = pSlashChar+1;
	atString folderName(pFilename);
	folderName.Truncate((int)(pSlashChar-pFilename));
	atString modelName(pModelName);

	// push the asset path
	ASSET.PushFolder(sm_assetPath);
	ASSET.PushFolder("models");
	ASSET.PushFolder(folderName.c_str());

#if MESH_LIBRARY
	// load up the model
	rmcDrawable* pNewDrawable = rage_new rmcDrawable();
	if (pNewDrawable->Load(modelName.c_str()))
	{
		// loaded ok - set up the drawable entry
		pDrawableEntry = rage_new ptxDrawableEntry;
		pDrawableEntry->SetDrawable(pNewDrawable);
		pDrawableEntry->SetIsOwnedByRMPTFX(true);
	}
	else
	{
		// didn't load - tidy up
		pNewDrawable->Release();
		pNewDrawable = NULL;
	}
#endif

	// pop the asset path
	ASSET.PopFolder();
	ASSET.PopFolder();
	ASSET.PopFolder();

	// return the drawable entry
	return pDrawableEntry;
}

grcTexture* ptxManager::LoadTexture(const char* pFilename, bool reload, bool filenameIsFullPath, bool* pIsExternalSource)
{
	// make sure these params get referenced in all builds
	(void)reload;
	(void)filenameIsFullPath;

	// check for valid filename
	if (pFilename==NULL || pFilename[0] == '\0') 
	{
		// filename isn't valid
		return (grcTexture*)grcTexture::NoneBlackTransparent;
	}

	grcTexture* pTexture = NULL;

	// default to not being from an exteral source 
	if (pIsExternalSource)
	{
		*pIsExternalSource = false;
	}

	// create an fx list iterator
	ptxFxListIterator fxListIterator = CreateFxListIterator();

#if RMPTFX_XML_LOADING
	// first check for textures in the fx list we're building
	for (fxListIterator.Start(FXLISTFLAG_UNDERCONSTRUCTION); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		pTexture = fxListIterator.GetCurrFxList()->GetTextureDictionary()->LookupLocal(pFilename);
		if (pTexture)
		{
			return pTexture;
		}
	}
#endif

	// we now must be from an external source
	if (pIsExternalSource)
	{
		*pIsExternalSource = true;
	}

	// then check in any shareable fx lists
	for (fxListIterator.Start(FXLISTFLAG_SHAREABLE); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		pTexture = fxListIterator.GetCurrFxList()->GetTextureDictionary()->LookupLocal(pFilename);
		if (pTexture)
		{
			pTexture->AddRef();
			return pTexture;
		}
	}

#if RMPTFX_EDITOR
	// next check the non shareable fx lists (only if we using the particle editor)
	// we might be editing a particle rule but not changing the texture
	// we don't want to have to load this from disk again so we search the other fx lists for it
	if (sysMemAllocator::GetCurrent().IsBuildingResource()==false)
	{
		for (fxListIterator.Start(FXLISTFLAG_NONSHAREABLE); !fxListIterator.AtEnd(); fxListIterator.Next())
		{
			pTexture = fxListIterator.GetCurrFxList()->GetTextureDictionary()->LookupLocal(pFilename);
			if (pTexture)
			{
				pTexture->AddRef();
				return pTexture;
			}
		}
	}
#endif

	// then check the global texture dictionaries
	if (pgDictionary<grcTexture>::GetCurrent())
	{
		pTexture = pgDictionary<grcTexture>::GetCurrent()->Lookup(pFilename);
		if (pTexture)
		{
			pTexture->AddRef();
			return pTexture;
		}
	}

	// early out if building a resource - we don't want to load the texture up
	if (sysMemAllocator::GetCurrent().IsBuildingResource())
	{
		ptxDisplayf("couldn't find texture %s while creating resource - hopefully it exists in another resource", pFilename);

		// create a new texture reference that we can use to resolve at runtime
		sysMemStartTemp();
		pTexture = grcTextureFactory::GetInstance().CreateTexture(pFilename);
		sysMemEndTemp();

		return pTexture;
	}

	// check if the texture exists in the loose fx list
#if RMPTFX_XML_LOADING
	pgDictionary<grcTexture>* pTextureDict = m_pLooseFxList->GetTextureDictionary();
	int dictElementIdx = pTextureDict->LookupLocalIndex(pgDictionary<grcTexture>::ComputeHash(pFilename));
	if (dictElementIdx>=0)
	{
#if RMPTFX_EDITOR
		// check if we need to reload the texture
		if (reload)
		{
			grcTexture* pNewTexture;

			// set up the asset path
			ASSET.PushFolder(sm_assetPath);
			if (!filenameIsFullPath)
			{
				ASSET.PushFolder("textures");
			}

			// create the new texture
			pNewTexture = grcCreateTexture(pFilename);

			// restore the asset path
			if (!filenameIsFullPath)
			{
				ASSET.PopFolder();
			}
			ASSET.PopFolder();

			// check if the texture created ok
			if (pNewTexture)
			{
				// release the dictionary's reference to this texture
				grcReleaseTexture(pTextureDict->GetEntry(dictElementIdx));

				// swap out the references
				pTextureDict->SetEntryUnsafe(dictElementIdx, pNewTexture); 
			}

			// now swap out every instance of this reference
			UpdateParticleRuleTexture(pFilename, pNewTexture); 
		}
#endif

		// return the texture in the loose fx list
		return pTextureDict->GetEntry(dictElementIdx);
	}

	// load up the texture - it doesn't exists anywhere yet

	// set up the asset path
	ASSET.PushFolder(sm_assetPath);
	if (!filenameIsFullPath)
	{
		ASSET.PushFolder("textures");
	}

	// create the new texture
	pTexture = grcCreateTexture(pFilename);
	if (pTexture)
	{
		pTexture->SetName(pFilename);
	}

	// restore the asset path
	if (!filenameIsFullPath)
	{
		ASSET.PopFolder();
	}
	ASSET.PopFolder();

	// check if we created a new texture
	if (pTexture)
	{
		pTexture->AddRef();
		
		// add it to the loose fx list's texture dictionary
		if (pTexture)
		{
			pTextureDict->AddNewEntry(pFilename, pTexture);
		}
	}
	else
	{
		ptxAssertf(0, "failed to load particle texture from disk - either it doesn't exist or we're out of memory");
		pTexture = (grcTexture*)grcTexture::None;
	}
#endif

	// return the texture
	return pTexture;
}

void ptxManager::RegisterBehaviour(const char* pBehaviourName, ptxBehaviourFactoryFunc behaviourFactoryFunc, ptxBehaviourPlaceFunc behaviourPlaceFunc)
{	
	// check that this behaviour hasn't been registered already
	if (ptxVerifyf((m_behaviourFactoryMap.Access(pBehaviourName)==NULL), "%s behaviour is already registered", pBehaviourName))
	{
		// insert the behaviour into the maps
		m_behaviourFactoryMap.Insert(pBehaviourName, behaviourFactoryFunc);
		m_behaviourPlaceMap.Insert(atHashString(pBehaviourName), behaviourPlaceFunc);
		
#if RMPTFX_XML_LOADING
		m_behaviourMap.Insert(pBehaviourName, RMPTFX_CREATE_BEHAVIOUR_FROM_ID(pBehaviourName));
#endif

#if RMPTFX_EDITOR
		// add to the editor list
		AddBehaviourToList();
#endif
	}
}

const char* ptxManager::GetPermanentBehaviourNamePtr(const char* pBehaviourName)
{
	// find the behaviour in the factory func map
	atMap<const char*, ptxBehaviourFactoryFunc>::Iterator behaviourFactoryFuncIterator = m_behaviourFactoryMap.CreateIterator();
	for (behaviourFactoryFuncIterator.Start(); !behaviourFactoryFuncIterator.AtEnd(); behaviourFactoryFuncIterator.Next())
	{
		if (strcmp(behaviourFactoryFuncIterator.GetKey(), pBehaviourName)==0)
		{
			// return pointer to a permanent name
			return behaviourFactoryFuncIterator.GetKey();
		}
	}

	// couldn't find the behaviour in the map
	ptxAssertf(0, "%s behaviour couldn't be found in the map", pBehaviourName);
	return NULL;
}

#if RMPTFX_XML_LOADING
ptxBehaviour* ptxManager::FindBehaviourFromPropertyId(u32 propertyId)
{
	// find the behaviour in the map
	atMap<const char*, ptxBehaviour*>::Iterator behaviourMapIterator = m_behaviourMap.CreateIterator();
	for (behaviourMapIterator.Start(); !behaviourMapIterator.AtEnd(); behaviourMapIterator.Next())
	{
		// check if it contains this property
		if (behaviourMapIterator.GetData()->HasProp(propertyId))
		{
			// it does - return the behaviour
			return behaviourMapIterator.GetData();
		}
	}

	// couldn't find the behaviour in the map
	return NULL;
}
#endif

void ptxManager::RegisterBehaviours()
{
	// register all behaviours
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Acceleration);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Age);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_AnimateTexture);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Attractor);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Collision);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Colour);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Dampening);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_MatrixWeight);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Noise);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Pause);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Rotation);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Size);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Velocity);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Wind);
	RMPTFX_REGISTER_BEHAVIOUR(ptxd_Sprite);
	RMPTFX_REGISTER_BEHAVIOUR(ptxd_Model);
	RMPTFX_REGISTER_BEHAVIOUR(ptxd_Trail);
}

ptxFxListIterator ptxManager::CreateFxListIterator()
{
	Assertf(m_mainThreadId == sysIpcGetCurrentThreadId(), "Only call CreateFxListIterator from the main thread!");

	ptxFxListMap::Iterator fxListMapIterator = m_fxListMap.CreateIterator();
	return ptxFxListIterator(fxListMapIterator);
}

void ptxManager::SetupPtxKeyFramePropThreadId()
{
	//If we hit this compile time assert, it means that the max number of threads has increased (could be number of subrender threads or the amount of particle tasks)
	//You will have to increase PTXEVO_KFP_THREAD_MAX and rebuild ragebuilder and particle resources 
	//If this is not done then game will crash on startup.
	CompileTimeAssert(PTXEVO_KFP_THREAD_TOTAL <= PTXEVO_KFP_THREAD_MAX);

	//This function is called once at the start of the threads that makes queries into the ptxKeyframes
	//We make sure that each thread has a unique ID so that by chance if one of the threads goes to sleep
	//then the data does not get stomped by by another thread.
	//ptxKeyFrameProp::m_pEvolvedKeyframeProp is now an array to store the per thread instance of the evolved key frame prop

	if( sysThreadType::IsUpdateThread())
	{
		ptxKeyframeProp::ms_ptxKeyframePropThreadId = PTXEVO_KFP_THREAD_MAIN;
	}
	else if(sysThreadType::IsRenderThread())
	{
		ptxKeyframeProp::ms_ptxKeyframePropThreadId = PTXEVO_KFP_THREAD_SUBRENDER_THREAD_FIRST + g_RenderThreadIndex;
	}
	else if(RMPTFXMGR.GetUpdateThreadId() == sysIpcGetCurrentThreadId())
	{
		ptxKeyframeProp::ms_ptxKeyframePropThreadId = PTXEVO_KFP_THREAD_RMPTFX_UPDATE;
	}
	else
	{
		//if not any of the above then check if it's a particle update task
		int ptxUpdateTaskIdx = ptxUpdateTask::GetPtxUpdateTaskIdx(sysIpcGetCurrentThreadId());
		if(ptxUpdateTaskIdx > -1)
		{
			ptxKeyframeProp::ms_ptxKeyframePropThreadId = PTXEVO_KFP_THREAD_PARTICLE_THREAD_FIRST + (s8)ptxUpdateTaskIdx;
		}
		else
		{
			ptxKeyframeProp::ms_ptxKeyframePropThreadId = -1;
			ptxAssertf(ptxKeyframeProp::ms_ptxKeyframePropThreadId != -1, "Unable to assign thread ID type to ptxKeyframeProp::ms_ptxKeyframePropThreadId");
		}
	}
#if __ASSERT
	ptxKeyframeProp::ms_ptxKeyframePropEvoSetup = false;
#endif

}

#if RMPTFX_THOROUGH_ERROR_CHECKING
void ptxManager::VerifyLists()
{
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// go through the effect instance list
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// verify the effect inst lists
			ptxAssertf(m_effectInstPool.IsInFreeList(pCurrEffectInstItem)==false, "active effect inst is in the free list");

			// verify the emitter point lists
			for (int j=0; j<pCurrEffectInstItem->GetDataSB()->GetNumEvents(); j++)
			{
				pCurrEffectInstItem->GetDataSB()->GetEventInst(j).GetEmitterInst()->VerifyLists();
			}

			// set the next effect instance in the list
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}

	for (int k=0; k<PTX_NUM_BUFFERS; k++)
	{
		for (int i=0; i<m_drawLists.GetCount(); i++)
		{
			// go through the effect instance list
			ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHeadMT(k);
			while (pCurrEffectInstItem)
			{
				// store the next effect instance in the list
				ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNextMT(k);

				// verify the effect inst lists
				ptxAssertf(RMPTFXTHREADER.GetUpdateBufferId() != k || m_effectInstPool.IsInFreeList(pCurrEffectInstItem)==false, "active effect inst is in the free list");

				// verify the emitter point lists
				for (int j=0; j<pCurrEffectInstItem->GetDataSB()->GetNumEvents(); j++)
				{
					pCurrEffectInstItem->GetDataSB()->GetEventInst(j).GetEmitterInst()->VerifyLists();
				}

				// set the next effect instance in the list
				pCurrEffectInstItem = pNextEffectInstItem;
			}
		}
	}
}
#endif

#if RMPTFX_THOROUGH_ERROR_CHECKING
void ptxManager::CheckDataIntegrity()
{
	// keep track of how many effect instances we process
	u32 numEffectInstsInTotal = 0;

	// go through the draw lists
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// store how many effect instances the list thinks it has
		u32 numInstancesInList = (u32)m_drawLists[i]->m_effectInstList.GetNumItemsMT(RMPTFXTHREADER.GetDrawBufferId());

		// go through the effect instances in the draw list
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// increment the total count
			numEffectInstsInTotal++;

			// check for problematic data in the list
#if __ASSERT
			ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();
			ptxEffectRule* pEffectRule = pEffectInst->GetEffectRule();

			ptxAssertf(pNextEffectInstItem!=pCurrEffectInstItem, "effect instance in list multiple times");
			ptxAssertf(pEffectRule, "drawlist %u's effect instance list is corrupt - NULL effect rule", i);
			ptxAssertf(numEffectInstsInTotal<numInstancesInList+1000, "drawlist %u's effect instance list is corrupt - list is longer than expected", i);
#endif

			// proceed to the next effect instance in the list
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}
}
#endif 

#if __DEV
void ptxManager::OutputActiveEffectInsts()
{
	// display the header
	ptxDisplayf("---------------------------");
	ptxDisplayf("--- ACTIVE EFFECT INSTS ---");
	ptxDisplayf("---------------------------");
	ptxDisplayf("");
	ptxDisplayf("Draw Lists");
	ptxDisplayf("Name,\tNumPoints,\tPlayState,\tPlaying,\tStopped,\tFinished,\tReserved,\tUser1,\tUser2,\tUser3,\tUser4,\tX,\tY,\tZ,\tCurrLifeRatio,\tEvo0,\tEvo1,\tEvo2,\tEvo3,\tEvo4,\tEvo5,\tCullState");
	
	// go through the draw lists
	int numItems = 0;
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// go through the effect instances
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			numItems++;

			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// cache some data
			ptxEffectRule* pEffectRule = pCurrEffectInstItem->GetDataSB()->GetEffectRule();
			Vec3V vCurrPos = (pCurrEffectInstItem->GetDataSB()->GetCurrPos());

			ptxEvoValueType* pEvoValues = pCurrEffectInstItem->GetDataSB()->GetEvoValues();

			// output the effect instance information
			ptxDisplayf("%s,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f\t%d",
				pEffectRule->GetName(), 
				pCurrEffectInstItem->GetDataSB()->GetNumActivePoints(),
				pCurrEffectInstItem->GetDataSB()->GetPlayState(),
				pCurrEffectInstItem->GetDataSB()->GetIsPlaying(),
				pCurrEffectInstItem->GetDataSB()->GetIsStopped(),
				pCurrEffectInstItem->GetDataSB()->GetIsFinished(),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_KEEP_RESERVED),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_USER1),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_USER2),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_USER3),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_USER4),
				vCurrPos.GetXf(), vCurrPos.GetYf(), vCurrPos.GetZf(), 
				pCurrEffectInstItem->GetDataDB()->GetCurrLifeRatio(),
				UnPackEvoValue(pEvoValues[0]), UnPackEvoValue(pEvoValues[1]), UnPackEvoValue(pEvoValues[2]), 
				UnPackEvoValue(pEvoValues[3]), UnPackEvoValue(pEvoValues[4]), UnPackEvoValue(pEvoValues[5]),
				pCurrEffectInstItem->GetDataSB()->GetCullState());

			// set the next effect instance
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}
	ptxDisplayf("Num Draw List Items = %d", numItems);

	// display the header
	ptxDisplayf("");
	ptxDisplayf("Spawn Lists");
	ptxDisplayf("Name,\tNumPoints,\tPlayState,\tPlaying,\tStopped,\tFinished,\tReserved,\tUser1,\tUser2,\tUser3,\tUser4,\tX,\tY,\tZ,\tCurrLifeRatio,\tEvo0,\tEvo1,\tEvo2,\tEvo3,\tEvo4,\tEvo5,\tCullState");

	// go through the draw lists
	numItems = 0;
	for (int i=0; i<m_spawnLists.GetCount(); i++)
	{
		// go through the effect instances
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_spawnLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			numItems++;

			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// cache some data
			ptxEffectRule* pEffectRule = pCurrEffectInstItem->GetDataSB()->GetEffectRule();
			Vec3V vCurrPos = (pCurrEffectInstItem->GetDataSB()->GetCurrPos());

			ptxEvoValueType* pEvoValues = pCurrEffectInstItem->GetDataSB()->GetEvoValues();

			// output the effect instance information
			ptxDisplayf("%s,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f\t%d",
				pEffectRule->GetName(), 
				pCurrEffectInstItem->GetDataSB()->GetNumActivePoints(),
				pCurrEffectInstItem->GetDataSB()->GetPlayState(),
				pCurrEffectInstItem->GetDataSB()->GetIsPlaying(),
				pCurrEffectInstItem->GetDataSB()->GetIsStopped(),
				pCurrEffectInstItem->GetDataSB()->GetIsFinished(),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_KEEP_RESERVED),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_USER1),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_USER2),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_USER3),
				pCurrEffectInstItem->GetDataSB()->GetFlag(PTXEFFECTFLAG_USER4),
				vCurrPos.GetXf(), vCurrPos.GetYf(), vCurrPos.GetZf(), 
				pCurrEffectInstItem->GetDataDB()->GetCurrLifeRatio(),
				UnPackEvoValue(pEvoValues[0]), UnPackEvoValue(pEvoValues[1]), UnPackEvoValue(pEvoValues[2]), 
				UnPackEvoValue(pEvoValues[3]), UnPackEvoValue(pEvoValues[4]), UnPackEvoValue(pEvoValues[5]),
				pCurrEffectInstItem->GetDataSB()->GetCullState());

			// set the next effect instance
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}
	ptxDisplayf("Num Spawn List Items = %d", numItems);

	// 
	ptxDisplayf("");
	ptxDisplayf("Pool");
	ptxDisplayf("Num Free List Items = %d", m_effectInstPool.GetNumItemsFree());
	ptxDisplayf("Num Returned List 0 Items = %d", m_effectInstPool.GetNumItemsReturned(0));
	ptxDisplayf("Num Returned List 1 Items = %d", m_effectInstPool.GetNumItemsReturned(1));
	ptxDisplayf("Num Returned List 2 Items = %d", m_effectInstPool.GetNumItemsReturned(2));

	ptxDisplayf("");
	ptxDisplayf("---------------------------");
}
#endif

#if __DEV
void ptxManager::OutputFxListReferences()
{
	ptxFxListMap::Iterator fxListMapIterator = m_fxListMap.CreateIterator();
	for (fxListMapIterator.Start(); !fxListMapIterator.AtEnd(); fxListMapIterator.Next())
	{
		fxListMapIterator.GetData().m_pFxList->PrintReferences(fxListMapIterator.GetKey().GetCStr());
	}
}
#endif

#if __DEV
void ptxManager::OutputLoadedFxLists()
{
	ptxDisplayf("");
	ptxDisplayf("");
	ptxDisplayf("Registered FXLists:");
	ptxFxListMap::Iterator fxListMapIterator = m_fxListMap.CreateIterator();
	for (fxListMapIterator.Start(); !fxListMapIterator.AtEnd(); fxListMapIterator.Next())
	{
		static const char *sUnknownHash = "<Unknown Hash>";
		const rage::atHashString &name = fxListMapIterator.GetKey();
		ptxDisplayf("\t%s", (name.GetCStr() ? name.GetCStr() : sUnknownHash));
	}
	ptxDisplayf("");
	ptxDisplayf("");
}
#endif

#if RMPTFX_EDITOR
int ptxManager::GetAllEffectRules(const atHashWithStringNotFinal effectRuleHashName, ptxEffectRule** ppEffectRules, s32 maxResults, u32 fxListFlags)
{
	int numEffectRules = 0;

	// no fx list specified - search for the effect rule all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxEffectRule* pEffectRule = fxListIterator.GetCurrFxList()->GetEffectRuleDictionary()->LookupLocal(effectRuleHashName);
		if (pEffectRule)
		{
			if (ptxVerifyf(numEffectRules<maxResults, "more results found than we can handle"))
			{
				ppEffectRules[numEffectRules] = pEffectRule;
				numEffectRules++;
			}
		}
	}

	return numEffectRules;
}
#endif

#if RMPTFX_EDITOR
int ptxManager::GetAllEmitterRules(const atHashWithStringNotFinal emitterRuleHashName, ptxEmitterRule** ppEmitterRules, s32 maxResults, u32 fxListFlags)
{
	int numEmitterRules = 0;

	// no fx list specified - search for the emitter rule all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxEmitterRule* pEmitterRule = fxListIterator.GetCurrFxList()->GetEmitterRuleDictionary()->LookupLocal(emitterRuleHashName);
		if (pEmitterRule)
		{
			if (ptxVerifyf(numEmitterRules<maxResults, "more results found than we can handle"))
			{
				ppEmitterRules[numEmitterRules] = pEmitterRule;
				numEmitterRules++;
			}
		}
	}

	return numEmitterRules;
}
#endif

#if RMPTFX_EDITOR
int ptxManager::GetAllParticleRules(const atHashWithStringNotFinal particleRuleHashName, ptxParticleRule** ppParticleRules, s32 maxResults, u32 fxListFlags)
{
	int numParticleRules = 0;

	// no fx list specified - search for the particle rule all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxParticleRule* pParticleRule = fxListIterator.GetCurrFxList()->GetParticleRuleDictionary()->LookupLocal(particleRuleHashName);
		if (pParticleRule)
		{
			if (ptxVerifyf(numParticleRules<maxResults, "more results found than we can handle"))
			{
				ppParticleRules[numParticleRules] = pParticleRule;
				numParticleRules++;
			}
		}
	}

	return numParticleRules;
}
#endif

#if RMPTFX_EDITOR
int ptxManager::GetAllKeyframes(ptxKeyframe** ppKeyframes, s32 maxResults, const atHashWithStringNotFinal ruleHashName, const ptxRuleType ruleType, const u32 propertyId, const s32 eventIdx, const s32 evoIdx)
{
	int numKeyframes = 0;

	if (evoIdx==-1)
	{
		// this is not an evolution keyframe
		if (ruleType==PTXRULETYPE_EFFECT)
		{
			ptxEffectRule* pEffectRules[32];
			int numEffectRules = RMPTFXMGR.GetAllEffectRules(ruleHashName, &pEffectRules[0], 32);
			ptxAssertf(numEffectRules>0, "effect rule not found");
			for (int i=0; i<numEffectRules; i++)
			{
				ptxKeyframeProp* pKeyframeProp = pEffectRules[i]->GetKeyframePropList().GetKeyframePropByPropId(propertyId);
				if (pKeyframeProp)
				{
					if (ptxVerifyf(numKeyframes<maxResults, "more results found than we can handle"))
					{
						ppKeyframes[numKeyframes++] = &pKeyframeProp->GetKeyframe();
					}
				}
			}
		}
		else if (ruleType==PTXRULETYPE_EMITTER)
		{
			ptxEmitterRule* pEmitterRules[32];
			int numEmitterRules = RMPTFXMGR.GetAllEmitterRules(ruleHashName, &pEmitterRules[0], 32);
			ptxAssertf(numEmitterRules>0, "emitter rule not found");
			for (int i=0; i<numEmitterRules; i++)
			{
				ptxKeyframeProp* pKeyframeProp = pEmitterRules[i]->GetKeyframePropList().GetKeyframePropByPropId(propertyId);
				if (pKeyframeProp)
				{
					if (ptxVerifyf(numKeyframes<maxResults, "more results found than we can handle"))
					{
						ppKeyframes[numKeyframes++] = &pKeyframeProp->GetKeyframe();
					}
				}

				pKeyframeProp = pEmitterRules[i]->GetCreationDomain()->GetKeyframePropList().GetKeyframePropByPropId(propertyId);
				if (pKeyframeProp)
				{
					if (ptxVerifyf(numKeyframes<maxResults, "more results found than we can handle"))
					{
						ppKeyframes[numKeyframes++] = &pKeyframeProp->GetKeyframe();
					}
				}

				pKeyframeProp = pEmitterRules[i]->GetTargetDomain()->GetKeyframePropList().GetKeyframePropByPropId(propertyId);
				if (pKeyframeProp)
				{
					if (ptxVerifyf(numKeyframes<maxResults, "more results found than we can handle"))
					{
						ppKeyframes[numKeyframes++] = &pKeyframeProp->GetKeyframe();
					}
				}

				if (pEmitterRules[i]->GetAttractorDomain())
				{
					pKeyframeProp = pEmitterRules[i]->GetAttractorDomain()->GetKeyframePropList().GetKeyframePropByPropId(propertyId);
					if (pKeyframeProp)
					{
						if (ptxVerifyf(numKeyframes<maxResults, "more results found than we can handle"))
						{
							ppKeyframes[numKeyframes++] = &pKeyframeProp->GetKeyframe();
						}
					}
				}
			}
		}
		else if (ruleType==PTXRULETYPE_PARTICLE)
		{
			ptxParticleRule* pParticleRules[32];
			int numParticleRules = RMPTFXMGR.GetAllParticleRules(ruleHashName, &pParticleRules[0], 32);
			ptxAssertf(numParticleRules>0, "particle rule not found");
			for (int i=0; i<numParticleRules; i++)
			{
				ptxKeyframeProp* pKeyframeProp = pParticleRules[i]->GetKeyframeProp(propertyId);
				if (pKeyframeProp)
				{
					if (ptxVerifyf(numKeyframes<maxResults, "more results found than we can handle"))
					{
						ppKeyframes[numKeyframes++] = &pKeyframeProp->GetKeyframe();
					}
				}

				ptxInstVars& instVars = pParticleRules[i]->GetShaderInst().GetShaderVars();
				for (int i=0; i<instVars.GetCount(); i++)
				{
					if (instVars[i]->GetType()==PTXSHADERVAR_KEYFRAME)
					{
						ptxShaderVarKeyframe* pShaderVarKeyframe = static_cast<ptxShaderVarKeyframe*>(instVars[i].ptr);
						if (pShaderVarKeyframe->GetHashName()==propertyId)
						{
							if (ptxVerifyf(numKeyframes<maxResults, "more results found than we can handle"))
							{
								ppKeyframes[numKeyframes++] = &pShaderVarKeyframe->GetKeyframe();
							}
						}
					}
				}
			}
		}
		else
		{
			ptxAssertf(0, "unrecognised rule type");
		}
	}
	else
	{
		// this is an evolution keyframe
		if (ruleType==PTXRULETYPE_EFFECT)
		{
			ptxEffectRule* pEffectRules[32];
			int numEffectRules = RMPTFXMGR.GetAllEffectRules(ruleHashName, &pEffectRules[0], 32);
			ptxAssertf(numEffectRules>0, "effect rule not found");
			for (int i=0; i<numEffectRules; i++)
			{
				ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = NULL;
				if (eventIdx==-1)
				{
					pEvolvedKeyframeProp = pEffectRules[i]->GetEvolutionList()->GetEvolvedKeyframePropById(propertyId);
				}
				else
				{
					ptxEvent* pEvent = pEffectRules[i]->GetTimeline().GetEvent(eventIdx);
					pEvolvedKeyframeProp = pEvent->GetEvolutionList()->GetEvolvedKeyframePropById(propertyId);
				}

				if (pEvolvedKeyframeProp)
				{
					for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
					{
						ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);
						if (evolvedKeyframe.GetEvolutionIndex()==evoIdx)
						{
							if (ptxVerifyf(numKeyframes<maxResults, "more results found than we can handle"))
							{
								ppKeyframes[numKeyframes++] = &evolvedKeyframe.GetKeyframe();
							}
						}
					}
				}
			}
		}
		else
		{
			ptxAssertf(0, "unrecognised rule type");
		}
	}

	return numKeyframes;
}
#endif

#if RMPTFX_BANK
ptxFxList* ptxManager::GetFxList(const atHashWithStringNotFinal fxListHashName)
{
	u32 fxListFlags = (u32)FXLISTFLAG_ALL;
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxFxList* pFxList = fxListIterator.GetCurrFxList();
		if (atHashValue(pFxList->GetName())==fxListHashName)
		{
			return pFxList;
		}
	}

	return NULL;
}
#endif

#if RMPTFX_BANK
int ptxManager::GetAllEffectRuleRefs(const atHashWithStringNotFinal effectRuleHashName, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags)
{
	int numFxLists = 0;

	// no fx list specified - search for the effect rule in all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxEffectRule* pEffectRule = fxListIterator.GetCurrFxList()->GetEffectRuleDictionary()->LookupLocal(effectRuleHashName);
		if (pEffectRule)
		{
			if (ptxVerifyf(numFxLists<maxResults, "more results found than we can handle"))
			{
				ppFxLists[numFxLists] = fxListIterator.GetCurrFxList();
				numFxLists++;
			}
		}
	}

	return numFxLists;
}
#endif

#if RMPTFX_BANK
int ptxManager::GetAllEmitterRuleRefs(const atHashWithStringNotFinal emitterRuleHashName, ptxEffectRule** ppEffectRules, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags)
{
	int numEffectRules = 0;

	// no fx list specified - search for the emitter rule in all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxFxList* pFxList = fxListIterator.GetCurrFxList();
		ptxEmitterRule* pEmitterRule = pFxList->GetEmitterRuleDictionary()->LookupLocal(emitterRuleHashName);
		if (pEmitterRule)
		{
			for (int i=0; i<pFxList->GetEffectRuleDictionary()->GetCount(); i++)
			{
				ptxEffectRule* pEffectRule = pFxList->GetEffectRuleDictionary()->GetEntry(i);
				ptxTimeLine& timeline = pEffectRule->GetTimeline();
				int numEvents = timeline.GetNumEvents();
				for (int i=0; i<numEvents; i++)
				{
					ptxEvent* pEvent = timeline.GetEvent(i);
					if (pEvent->GetType()==PTXEVENT_TYPE_EMITTER)
					{
						ptxEventEmitter* pEventEmitter = static_cast<ptxEventEmitter*>(pEvent);
						if (atHashWithStringNotFinal(pEventEmitter->GetEmitterRule()->GetName())==emitterRuleHashName)
						{
							if (ptxVerifyf(numEffectRules<maxResults, "more results found than we can handle"))
							{
								ppEffectRules[numEffectRules] = pEffectRule;
								ppFxLists[numEffectRules] = pFxList;
								numEffectRules++;
							}
						}
					}
				}
			}
		}
	}

	return numEffectRules;
}
#endif

#if RMPTFX_BANK
int ptxManager::GetAllParticleRuleRefs(const atHashWithStringNotFinal particleRuleHashName, ptxEffectRule** ppEffectRules, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags)
{
	int numEffectRules = 0;

	// no fx list specified - search for the particle rule in all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxFxList* pFxList = fxListIterator.GetCurrFxList();
		ptxParticleRule* pParticleRule = pFxList->GetParticleRuleDictionary()->LookupLocal(particleRuleHashName);
		if (pParticleRule)
		{
			for (int i=0; i<pFxList->GetEffectRuleDictionary()->GetCount(); i++)
			{
				ptxEffectRule* pEffectRule = pFxList->GetEffectRuleDictionary()->GetEntry(i);
				ptxTimeLine& timeline = pEffectRule->GetTimeline();
				int numEvents = timeline.GetNumEvents();
				for (int i=0; i<numEvents; i++)
				{
					ptxEvent* pEvent = timeline.GetEvent(i);
					if (pEvent->GetType()==PTXEVENT_TYPE_EMITTER)
					{
						ptxEventEmitter* pEventEmitter = static_cast<ptxEventEmitter*>(pEvent);
						if (atHashWithStringNotFinal(pEventEmitter->GetParticleRule()->GetName())==particleRuleHashName)
						{
							if (ptxVerifyf(numEffectRules<maxResults, "more results found than we can handle"))
							{
								ppEffectRules[numEffectRules] = pEffectRule;
								ppFxLists[numEffectRules] = pFxList;
								numEffectRules++;
							}
						}
					}
				}
			}
		}
	}

	return numEffectRules;
}
#endif

#if RMPTFX_BANK
int ptxManager::GetAllTextureRefs(const atHashWithStringNotFinal textureHashName, ptxParticleRule** ppParticleRules, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags)
{
	int numParticleRules = 0;

	// no fx list specified - search for the texture in all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxFxList* pFxList = fxListIterator.GetCurrFxList();
		for (int i=0; i<pFxList->GetParticleRuleDictionary()->GetCount(); i++)
		{
			ptxParticleRule* pParticleRule = pFxList->GetParticleRuleDictionary()->GetEntry(i);
			if (pParticleRule)
			{
				if (pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_SPRITE || pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_TRAIL)
				{
					if (pParticleRule->GetShaderInst().GetTextureHashName(PTXSTR_DIFFUSETEXTURE_HASH)==textureHashName ||
						pParticleRule->GetShaderInst().GetTextureHashName(PTXSTR_REFRACTIONMAP_HASH)==textureHashName)
					{
						if (ptxVerifyf(numParticleRules<maxResults, "more results found than we can handle"))
						{
							ppParticleRules[numParticleRules] = pParticleRule;
							ppFxLists[numParticleRules] = pFxList;
							numParticleRules++;
						}
					}
				}
				else if (pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_MODEL)
				{
					for (int j=0; j<pParticleRule->GetNumDrawables(); j++)
					{
						ptxDrawable& drawable = pParticleRule->GetDrawable(j);
						rmcDrawable* pDrawable = drawable.GetDrawable();
						if (pDrawable)
						{
							for (int k=0; k<pDrawable->GetShaderGroup().GetCount(); k++)
							{
								grmShader& shader = pDrawable->GetShaderGroup().GetShader(k);
								for (int s=0; s<shader.GetInstanceData().GetCount(); s++)
								{
									if (shader.GetInstanceData().IsTexture(s))
									{
										if (shader.GetInstanceData().Data()[s].Texture && atHashValue(shader.GetInstanceData().Data()[s].Texture->GetName())==textureHashName)
										{
											if (ptxVerifyf(numParticleRules<maxResults, "more results found than we can handle"))
											{
												ppParticleRules[numParticleRules] = pParticleRule;
												ppFxLists[numParticleRules] = pFxList;
												numParticleRules++;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return numParticleRules;
}
#endif

#if RMPTFX_BANK
int ptxManager::GetAllModelRefs(const atHashWithStringNotFinal modelHashName, ptxParticleRule** ppParticleRules, ptxFxList** ppFxLists, s32 maxResults, u32 fxListFlags)
{
	int numParticleRules = 0;

	// no fx list specified - search for the texture in all fx lists
	ptxFxListIterator fxListIterator = CreateFxListIterator();
	for (fxListIterator.Start(fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxFxList* pFxList = fxListIterator.GetCurrFxList();
		for (int i=0; i<pFxList->GetParticleRuleDictionary()->GetCount(); i++)
		{
			ptxParticleRule* pParticleRule = pFxList->GetParticleRuleDictionary()->GetEntry(i);
			if (pParticleRule)
			{
				for (int j=0; j<pParticleRule->GetNumDrawables(); j++)
				{
					ptxDrawable& drawable = pParticleRule->GetDrawable(j);
					if (drawable.GetHashName()==modelHashName)
					{
						if (ptxVerifyf(numParticleRules<maxResults, "more results found than we can handle"))
						{
							ppParticleRules[numParticleRules] = pParticleRule;
							ppFxLists[numParticleRules] = pFxList;
							numParticleRules++;
						}
					}
				}
			}
		}
	}

	return numParticleRules;
}
#endif

#if RMPTFX_EDITOR
void ptxManager::SaveFxList(const char* pFxListName)
{
	// set up the asset path
	ASSET.PushFolder(sm_assetPath);

	// create an output stream
	fiSafeStream stream(ASSET.Create(pFxListName, "fxlist"));
	if (stream)
	{
		// go through the loaded effect rules saving their names out to the stream
		ptxEffectRuleIterator effectRuleIterator(CreateFxListIterator());
		for (effectRuleIterator.Start((u32)FXLISTFLAG_ALL); !effectRuleIterator.AtEnd(); effectRuleIterator.Next())
		{
			fprintf(stream, "%s\n", effectRuleIterator.GetCurrRule()->GetName());
		}
	}

	// restore the asset path
	ASSET.PopFolder();
}
#endif

#if RMPTFX_EDITOR
ptxEffectRule* ptxManager::CreateNewEffectRule(const char* pEffectRuleName)
{
	// create a unique effect rule name if one isn't passed in
	if (pEffectRuleName==NULL || strlen(pEffectRuleName)==0)
	{
		pEffectRuleName = GenerateUniqueEffectRuleName();
	}

	// check if the effect rule already exists
	ptxEffectRule* pEffectRule = GetEffectRule(atHashWithStringNotFinal(pEffectRuleName));
	if (pEffectRule)
	{
		// it does - return it
		return pEffectRule;
	}

	// effect rule doesn't exist - create a new one and set it up
	pEffectRule = rage_new ptxEffectRule;
	pEffectRule->Init();
	pEffectRule->SetName(pEffectRuleName);

	// set the loose fx list to own the effect rule
	pEffectRule->AddRef(); 
	m_pLooseFxList->GetEffectRuleDictionary()->AddNewEntry(pEffectRuleName, pEffectRule);

	// return the effect rule
	return pEffectRule;
}
#endif

#if RMPTFX_EDITOR
ptxEmitterRule* ptxManager::CreateNewEmitterRule(const char* pEmitterRuleName)
{
	// create a unique emitter rule name if one isn't passed in
	if (pEmitterRuleName==NULL || strlen(pEmitterRuleName)==0)
	{
		pEmitterRuleName = GenerateUniqueEmitterRuleName();
	}

	// check if the emitter rule already exists
	ptxEmitterRule* pEmitterRule = GetEmitterRule(atHashWithStringNotFinal(pEmitterRuleName));
	if (pEmitterRule)
	{
		// it does - return it
		return pEmitterRule;
	}

	// emitter rule doesn't exist - create a new one and set it up
	pEmitterRule = rage_new ptxEmitterRule;
	pEmitterRule->Init();
	pEmitterRule->SetName(pEmitterRuleName);

	// set the loose fx list to own the emitter rule
	pEmitterRule->AddRef(); 
	m_pLooseFxList->GetEmitterRuleDictionary()->AddNewEntry(pEmitterRuleName, pEmitterRule);

	// return the emitter rule
	return pEmitterRule;
}
#endif

#if RMPTFX_EDITOR
ptxParticleRule* ptxManager::CreateNewParticleRule(const char* pParticleRuleName)
{
	// create a unique particle rule name if one isn't passed in
	if (pParticleRuleName==NULL || strlen(pParticleRuleName)==0)
	{
		pParticleRuleName = GenerateUniqueParticleRuleName();
	}

	// check if the particle rule already exists
	ptxParticleRule* pParticleRule = GetParticleRule(atHashWithStringNotFinal(pParticleRuleName));
	if (pParticleRule)
	{
		// it does - return it
		return pParticleRule;
	}

	// particle rule doesn't exist - create a new one and set it up
	pParticleRule = rage_new ptxParticleRule;
	pParticleRule->Init();
	pParticleRule->SetName(pParticleRuleName);
	pParticleRule->SetPointPool(&m_pointPool);

	// set the loose fx list to own the particle rule
	pParticleRule->AddRef(); 
	m_pLooseFxList->GetParticleRuleDictionary()->AddNewEntry(pParticleRuleName, pParticleRule);

	// return the particle rule
	return pParticleRule;
}
#endif

#if RMPTFX_EDITOR
void ptxManager::RenameEffectRule(const char* pCurrEffectRuleName, const char* pNewEffectRuleName)
{
	// find the effect rule we want to rename
	ptxEffectRule* pEffectRule = GetEffectRule(atHashWithStringNotFinal(pCurrEffectRuleName));
	if (ptxVerifyf(pEffectRule, "can't find the effect rule to rename"))
	{
		// make sure the new name isn't already in use
		if (ptxVerifyf(GetEffectRule(atHashWithStringNotFinal(pNewEffectRuleName))==NULL, "the new effect rule name already exists"))
		{
			// rename all effect rules in all fx lists that have a matching name
			ptxFxListIterator fxListIterator = CreateFxListIterator();
			for (fxListIterator.Start((u32)FXLISTFLAG_ALL); !fxListIterator.AtEnd(); fxListIterator.Next())
			{
				ptxFxList* pCurrFxList = fxListIterator.GetCurrFxList();
				ptxEffectRule* pMatchingEffectRule = pCurrFxList->GetEffectRuleDictionary()->LookupLocal(pCurrEffectRuleName);
				if (pMatchingEffectRule)
				{
					// delete the current effect rule from the dictionary
					pCurrFxList->GetEffectRuleDictionary()->DeleteEntry(pCurrEffectRuleName);

					// set the new name and re-add to the dictionary
					pMatchingEffectRule->SetName(pNewEffectRuleName);
					pCurrFxList->GetEffectRuleDictionary()->AddNewEntry(pNewEffectRuleName, pMatchingEffectRule);
				}
			}
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::RenameEmitterRule(const char* pCurrEmitterRuleName, const char* pNewEmitterRuleName)
{
	// find the emitter rule we want to rename
	ptxEmitterRule* pEmitterRule = GetEmitterRule(atHashWithStringNotFinal(pCurrEmitterRuleName));
	if (ptxVerifyf(pEmitterRule, "can't find the emitter rule to rename"))
	{
		// make sure the new name isn't already in use
		if (ptxVerifyf(GetEmitterRule(atHashWithStringNotFinal(pNewEmitterRuleName))==NULL, "the new emitter rule name already exists"))
		{
			// rename all emitter rules in all fx lists that have a matching name
			ptxFxListIterator fxListIterator = CreateFxListIterator();
			for (fxListIterator.Start((u32)FXLISTFLAG_ALL); !fxListIterator.AtEnd(); fxListIterator.Next())
			{
				ptxFxList* pCurrFxList = fxListIterator.GetCurrFxList();
				ptxEmitterRule* pMatchingEmitterRule = pCurrFxList->GetEmitterRuleDictionary()->LookupLocal(pCurrEmitterRuleName);
				if (pMatchingEmitterRule)
				{
					// delete the current emitter rule from the dictionary
					pCurrFxList->GetEmitterRuleDictionary()->DeleteEntry(pCurrEmitterRuleName);

					// set the new name and re-add to the dictionary
					pMatchingEmitterRule->SetName(pNewEmitterRuleName);
					pCurrFxList->GetEmitterRuleDictionary()->AddNewEntry(pNewEmitterRuleName, pMatchingEmitterRule);
				}
			}
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::RenameParticleRule(const char* pCurrParticleRuleName, const char* pNewParticleRuleName)
{
	// find the particle rule we want to rename
	ptxParticleRule* pParticleRule = GetParticleRule(atHashWithStringNotFinal(pCurrParticleRuleName));
	if (ptxVerifyf(pParticleRule, "can't find the particle rule to rename"))
	{
		// make sure the new name isn't already in use
		if (ptxVerifyf(GetParticleRule(atHashWithStringNotFinal(pNewParticleRuleName))==NULL, "the new particle rule name already exists"))
		{
			// rename all particle rules in all fx lists that have a matching name
			ptxFxListIterator fxListIterator = CreateFxListIterator();
			for (fxListIterator.Start((u32)FXLISTFLAG_ALL); !fxListIterator.AtEnd(); fxListIterator.Next())
			{
				ptxFxList* pCurrFxList = fxListIterator.GetCurrFxList();
				ptxParticleRule* pMatchingParticleRule = pCurrFxList->GetParticleRuleDictionary()->LookupLocal(pCurrParticleRuleName);
				if (pMatchingParticleRule)
				{
					// delete the current particle rule from the dictionary
					pCurrFxList->GetParticleRuleDictionary()->DeleteEntry(pCurrParticleRuleName);

					// set the new name and re-add to the dictionary
					pMatchingParticleRule->SetName(pNewParticleRuleName);
					pCurrFxList->GetParticleRuleDictionary()->AddNewEntry(pNewParticleRuleName, pMatchingParticleRule);
				}
			}
		}
	}
}
#endif

#if RMPTFX_EDITOR
ptxParticleRule* ptxManager::CloneParticleRule(const char* pParticleRuleName)
{
	// get the particle rule we want to clone
	ptxParticleRule* pParticleRule = GetParticleRule(atHashWithStringNotFinal(pParticleRuleName));
	if (ptxVerifyf(pParticleRule, "can't find the particle rule to clone"))
	{
		ptxAssertf(pParticleRule->GetRefCount()==1, "if the original ref count is not one we will screw up the clone's ref count because it will be copied from the original - needs fixed");
		
		// make a clone of the particle rule
		pParticleRule = pParticleRule->Clone();

		// assign it a new name
		pParticleRule->SetName(GenerateUniqueClonedParticleRuleName(pParticleRule->GetName()));

		// add it to the loose fx list
		m_pLooseFxList->GetParticleRuleDictionary()->AddNewEntry(pParticleRule->GetName(), pParticleRule);
	}

	return pParticleRule;
}
#endif

#if RMPTFX_EDITOR
void ptxManager::AddEffectRuleGameFlagName(const char* pGameFlagName)
{
	// check there is room to add the new flag name
	if (ptxVerifyf(m_effectRuleGameFlagNames.GetCount()<m_effectRuleGameFlagNames.GetCapacity(), "too many effect rule game flag names added"))
	{
		// there is - add it
		formatf(m_effectRuleGameFlagNames.Append().c, 128, "%s", pGameFlagName);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::AddEffectRuleDataVolumeTypeName(const char* pDataVolumeTypeName)
{
	// check there is room to add the new flag name
	if (ptxVerifyf(m_effectRuleDataVolumeTypeNames.GetCount()<m_effectRuleDataVolumeTypeNames.GetCapacity(), "too many effect rule data volume type names added"))
	{
		// there is - add it
		formatf(m_effectRuleDataVolumeTypeNames.Append().c, 128, "%s", pDataVolumeTypeName);
	}
}
#endif

#if RMPTFX_EDITOR
const char* ptxManager::FindAnalogousBehaviour(const char* pBehaviourName, ptxDrawType drawType)
{
	// replace any drawsprite or drawtrail behaviour with a drawmodel one
	if (drawType==PTXPARTICLERULE_DRAWTYPE_MODEL && (stricmp(pBehaviourName, "ptxd_Sprite")==0 || stricmp(pBehaviourName, "ptxd_Trail")==0))
	{
		for (s32 i=0; i<m_behaviours.size(); i++)
		{
			atString newBehaviourName(m_behaviours[i]->GetName());
			if (stricmp(newBehaviourName, "ptxd_Model")==0)
			{
				return m_behaviours[i]->GetName();
			}
		}
	}
	// replace any drawmodel or drawtrail behaviour with a drawsprite one
	else if (drawType==PTXPARTICLERULE_DRAWTYPE_SPRITE && (stricmp(pBehaviourName, "ptxd_Model")==0 || stricmp(pBehaviourName, "ptxd_Trail")==0))
	{
		for (s32 i=0; i<m_behaviours.size(); i++)
		{
			atString newBehaviourName(m_behaviours[i]->GetName());
			if (stricmp(newBehaviourName, "ptxd_Sprite")==0)
			{
				return m_behaviours[i]->GetName();
			}
		}
	}
	// replace any drawmodel or drawsprite behaviour with a drawtrail one
	else if (drawType==PTXPARTICLERULE_DRAWTYPE_TRAIL && (stricmp(pBehaviourName, "ptxd_Model")==0 || stricmp(pBehaviourName, "ptxd_Sprite")==0))
	{
		for (s32 i=0; i<m_behaviours.size(); i++)
		{
			atString newBehaviourName(m_behaviours[i]->GetName());
			if (stricmp(newBehaviourName, "ptxd_Trail")==0)
			{
				return m_behaviours[i]->GetName();
			}
		}
	}

	// no replacement needed - return the original
	return pBehaviourName;
}
#endif

#if RMPTFX_EDITOR
void ptxManager::AddBehaviourToList()
{
	// clear the current list of editor behaviours
	for (int i=0; i<m_behaviours.GetCount(); i++)
	{
		delete m_behaviours[i];
	}

	m_behaviours.clear();

	// create a local list of behaviours
	atArray<ptxBehaviour*> localBehaviours;
	atMap<const char*, ptxBehaviourFactoryFunc>::Iterator behaviourFactoryFuncIterator = m_behaviourFactoryMap.CreateIterator();
	for (behaviourFactoryFuncIterator.Start(); !behaviourFactoryFuncIterator.AtEnd(); behaviourFactoryFuncIterator.Next())
	{
		localBehaviours.PushAndGrow(RMPTFX_CREATE_BEHAVIOUR_FROM_ID(behaviourFactoryFuncIterator.GetKey()));
	}

	// set the local behaviours' default data
	for (int i=0; i<localBehaviours.GetCount(); i++)
	{
		localBehaviours[i]->SetDefaultData();
	}

	// go through the local behaviour list sorting the behaviours into order
	while (localBehaviours.GetCount()!=0)
	{
		// find the behaviour with the lowest ordering
		float lowestOrder = FLT_MAX;
		u32 lowestIndex = 0;
		ptxBehaviour* pLowestBehaviour = NULL;

		for (int j=0; j<localBehaviours.GetCount(); j++)
		{
			if (localBehaviours[j]->GetOrder()<lowestOrder)
			{
				lowestOrder = localBehaviours[j]->GetOrder();
				lowestIndex = j;
				pLowestBehaviour = localBehaviours[j];
			}
		}

		// add to the behaviour list 
		m_behaviours.PushAndGrow(pLowestBehaviour);

		// remove from the local behaviours
		localBehaviours.Delete(lowestIndex);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::SendBehaviourListToEditor(ptxByteBuff& buff)
{
	// send the draw types
	int numDrawTypes = ptxBehaviour::GetNumDrawTypes();
	buff.Write_u32(numDrawTypes);
	for (int i=0;i<numDrawTypes;i++)
	{
		buff.Write_const_char(ptxBehaviour::GetDrawTypeName(i));
	}

	// send the behaviour definitions
	int numBehaviours = m_behaviours.GetCount();
	buff.Write_u32(numBehaviours);
	for (int i=0; i<numBehaviours; i++)
	{
		m_behaviours[i]->SendDefnToEditor(buff);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::SendEvolvedKeyframeDefnToEditor(ptxKeyframe* pKeyframe, const ptxEffectRule* pEffectRule, u32 propertyId, s32 eventIdx, s32 evoIdx)
{
	m_pInterface->SendKeyframeDefn(pKeyframe, pEffectRule->GetName(), PTXRULETYPE_EFFECT, propertyId, eventIdx, evoIdx);
}
#endif

#if RMPTFX_EDITOR
void ptxManager::SendUpdatedBehaviourKFPDefnToEditor(ptxKeyframeProp* pKeyframeProp, const ptxParticleRule* pParticleRule)
{
	// send the keyframe to the editor
	m_pInterface->SendKeyframeDefn(&pKeyframeProp->GetKeyframe(), pParticleRule->GetName(), PTXRULETYPE_PARTICLE, pKeyframeProp->GetPropertyId(), -1, -1);

	// go through the effect rules
	ptxEffectRuleIterator effectRuleIterator(CreateFxListIterator());
	for (effectRuleIterator.Start((u32)FXLISTFLAG_ALL); !effectRuleIterator.AtEnd(); effectRuleIterator.Next())
	{
		// if the effect rule has evolution and has the desired particle rule then update the editor evolution info
		ptxEffectRule* pEffectRule = effectRuleIterator.GetCurrRule();
		if (pEffectRule->GetEvolutionList() && pEffectRule->ContainsParticleRule(pParticleRule->GetName()))
		{
			pEffectRule->UpdateEvoUI(pKeyframeProp);
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::SendProfileDataToEditor(ptxByteBuff& buff)
{
	buff.Write_s32(m_pointPool.GetNumItemsActive());
	buff.Write_s32(m_numActiveInstances);
	buff.Write_s32(m_numUpdateCulledInstances);
	buff.Write_float(m_totalCPUUpdateTime);
	buff.Write_float(m_totalCPUDrawTime);
	buff.Write_float(m_totalGPUDrawTime);
}
#endif

#if RMPTFX_EDITOR
void ptxManager::SendGameFlagNamesToEditor(ptxByteBuff& buff)
{
	// send the game flag names
	buff.Write_u32(m_effectRuleGameFlagNames.GetCount());
	for (int i=0; i<m_effectRuleGameFlagNames.GetCount(); i++)
	{
		buff.Write_const_char(m_effectRuleGameFlagNames[i].c);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::SendDataVolumeTypeNamesToEditor(ptxByteBuff& buff)
{
	// send the data volume type names
	buff.Write_u32(m_effectRuleDataVolumeTypeNames.GetCount());
	for (int i=0; i<m_effectRuleDataVolumeTypeNames.GetCount(); i++)
	{
		buff.Write_const_char(m_effectRuleDataVolumeTypeNames[i].c);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::StartDebugEffect(const char* pEffectRuleName)
{
	// check the effect rule name is valid
	if (pEffectRuleName)
	{
		// get an effect instance of the effect rule
		ptxEffectInst* pEffectInst = GetEffectInst(atHashWithStringNotFinal(pEffectRuleName));
		if (pEffectInst)
		{
			// calculate the position of the effect instance
			Vec3V vPos = GetInterface().GetSpawnWorldPos();
			if (GetInterface().GetSpawnCamRelative() && m_pvDebugEffectCameraMtx)
			{
				// place the effect instance in front of the camera
				Mat34V vMtx = *m_pvDebugEffectCameraMtx;
				vPos = vMtx.GetCol3() + vMtx.GetCol1() * ScalarVFromF32(GetInterface().GetSpawnCamDist());
			} 

			// set up the effect instance
			pEffectInst->SetBasePos(vPos);
			pEffectInst->SetOffsetPos(Vec3V(V_ZERO));

			m_pDebugEffectInst = pEffectInst;
			strcpy(m_debugEffectInstName, pEffectRuleName);
			m_vDebugEffectAnchorPos = vPos;
			m_debugEffectAnimTimer = 0.0f;
			m_debugEffectRepeatTimer = 0.0f;

			// start the effect instance
			pEffectInst->Start();
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::UpdateDebugEffect(float dt)
{
	// repeat playback
	if (GetInterface().GetShouldRepeatPlayback() && m_debugEffectInstName[0]!='\0')
	{
		ptxEffectRule* pEffectRule = GetEffectRule(atHashWithStringNotFinal(m_debugEffectInstName));
		if (pEffectRule->GetIsInfinitelyLooped()==false)
		{
			m_debugEffectRepeatTimer += dt;
			if (m_debugEffectRepeatTimer>GetInterface().GetRepeatPlaybackTime())
			{
				StartDebugEffect(m_debugEffectInstName);
			}
		}
		else
		{
			m_debugEffectRepeatTimer = 0.0f;
		}
	}
	else
	{
		m_debugEffectRepeatTimer = 0.0f;
	}

	// animation
	if (m_pDebugEffectInst)
	{
		if (GetInterface().GetAnimEnabled())
		{
			// increment the debug animation timer
			m_debugEffectAnimTimer += RMPTFXMGR.GetInterface().GetAnimSpeed() * dt;

			// calculate the current sin and cos valued pre multiplied by the animation distance setting
			float animateRadius = RMPTFXMGR.GetInterface().GetAnimRadius();
			float dsin = sinf(m_debugEffectAnimTimer) * animateRadius;
			float dcos = cosf(m_debugEffectAnimTimer) * animateRadius;

			// create a base matrix at the anchor position
			Matrix34 baseMtx;
			baseMtx.Identity();
			baseMtx.d.Set(RCC_VECTOR3(m_vDebugEffectAnchorPos));

			// set up the offset matrix depending on the animation mode
			Matrix34 offsetMtx;
			offsetMtx.Identity();
			switch (RMPTFXMGR.GetInterface().GetAnimMode())
			{
				case 0: 
				{
					// rotate about the xy plane with fixed orientation
					offsetMtx.MakeTranslate(dcos, dsin, 0.0f);
					break;
				}
				case 1: 
				{
					// rotate about the xz plane with fixed orientation
					offsetMtx.MakeTranslate(dsin, 0.0f, dcos);
					break;
				}
				case 2:
				{
					// rotate about the xy plane
					offsetMtx.RotateFullZ(m_debugEffectAnimTimer);
					offsetMtx.MakeTranslate(dcos, dsin, 0.0f);
					break;
				}
				case 3:
				{
					// rotate about the xz plane
					offsetMtx.RotateFullY(m_debugEffectAnimTimer);
					offsetMtx.MakeTranslate(dsin, 0.0f, dcos);
					break;
				}
				case 4:	
				{
					// rotate about the z axis
					offsetMtx.RotateFullZ(m_debugEffectAnimTimer); 
					break;
				}
				case 5: 
				{
					// rotate about the y axis
					offsetMtx.RotateFullY(m_debugEffectAnimTimer);
					break;
				}
				case 6: 
				{
					// ping pong 
					offsetMtx.MakeTranslate(dsin, 0.0f, 0.0f);
					break;
				}
				case 7: 
				{
					// ping pong with pause
					dsin *= 2.0f;
					dsin = Clamp(dsin, -animateRadius, animateRadius);
					offsetMtx.MakeTranslate(dsin, 0.0f, 0.0f);
					break;
				}
			}

			// set the base and offset matrices
			m_pDebugEffectInst->SetBaseMtx(RCC_MAT34V(baseMtx));
			m_pDebugEffectInst->SetOffsetMtx(RCC_MAT34V(offsetMtx));
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::ResetDebugEffect()
{
	if (m_pDebugEffectInst)
	{
		m_pDebugEffectInst->Finish(false);
		m_pDebugEffectInst = NULL;
		m_debugEffectInstName[0] = '\0';
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::DeactivateDebugEffect()
{
	if (m_pDebugEffectInst)
	{
		m_pDebugEffectInst = NULL;
	}
}
#endif

#if RMPTFX_EDITOR
const char* ptxManager::GenerateUniqueEffectRuleName()
{
	// generate new names until we find one that doesn't exist already
	int loopCount = 0;
	static char effectRuleName[128];

	do
	{
		formatf(effectRuleName, sizeof(effectRuleName), "New_EffectRule_%03d", loopCount++);
	}
	while (GetEffectRule(atHashWithStringNotFinal(effectRuleName), NULL, (u32)FXLISTFLAG_ALL));

	return effectRuleName;
}
#endif

#if RMPTFX_EDITOR
const char* ptxManager::GenerateUniqueEmitterRuleName()
{
	// generate new names until we find one that doesn't exist already
	int loopCount = 0;
	static char emitterRuleName[128];

	do
	{
		formatf(emitterRuleName, sizeof(emitterRuleName), "New_EmitterRule_%03d", loopCount++);
	}
	while (GetEmitterRule(atHashWithStringNotFinal(emitterRuleName), NULL, (u32)FXLISTFLAG_ALL));

	return emitterRuleName;
}
#endif

#if RMPTFX_EDITOR
const char* ptxManager::GenerateUniqueParticleRuleName()
{
	// generate new names until we find one that doesn't exist already
	int loopCount = 0;
	static char particleRuleName[128];

	do
	{
		formatf(particleRuleName, sizeof(particleRuleName), "New_ParticleRule_%03d", loopCount++);
	}
	while (GetParticleRule(atHashWithStringNotFinal(particleRuleName), NULL, (u32)FXLISTFLAG_ALL));

	return particleRuleName;
}
#endif

#if RMPTFX_EDITOR
const char* ptxManager::GenerateUniqueClonedParticleRuleName(const char* pOldParticleRuleName)
{
	// generate new names until we find one that doesn't exist already
	int loopCount = 0;
	static char particleRuleName[128];

	do
	{
		if (loopCount==0)
		{
			formatf(particleRuleName, sizeof(particleRuleName), "Copy_Of_%s", pOldParticleRuleName);
		}
		else
		{
			formatf(particleRuleName, sizeof(particleRuleName), "Copy_%03d_Of_%s", loopCount, pOldParticleRuleName);
		}

		loopCount++;
	}
	while (GetParticleRule(atHashWithStringNotFinal(particleRuleName), NULL, (u32)FXLISTFLAG_ALL));

	return particleRuleName;

}
#endif

#if RMPTFX_BANK
void ptxManager::DebugDrawEffectInstList(ptxEffectInstItem** ppSorted, unsigned num)
{
	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	if (ptxDebug::sm_enableDebugDrawing)
	{
		// go through the effect instances
		for (unsigned i=0; i<num; ++i)
		{
			// debug draw the effect instance
			ptxEffectInstItem* pCurrEffectInstItem = ppSorted[i];
			ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();
			pEffectInst->DebugDraw();
		}
	}

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}
#endif

#if RMPTFX_BANK
void ptxManager::DebugDrawFxListReferences()
{
	static const char* sUnknownHash = "<Unknown Hash>";

	// create an fx list iterator
	ptxFxListIterator fxListIterator = CreateFxListIterator();

	// out put effect rule references
	grcDebugDraw::AddDebugOutputEx(false, "Effect Rule References");
	for (fxListIterator.Start((u32)FXLISTFLAG_ALL); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxFxListMap::Iterator& fxListMapIterator = fxListIterator.GetRawIter();
		const rage::atHashString& fxListName = fxListMapIterator.GetKey();
		ptxFxList* pFxList = fxListIterator.GetCurrFxList();
		ptxEffectRuleDictionary* pEffectRuleDict = pFxList->GetEffectRuleDictionary();

		grcDebugDraw::AddDebugOutputEx(false, "    %s: %d, %d",
			(fxListName.GetCStr() ? fxListName.GetCStr() : sUnknownHash),
			pEffectRuleDict->GetRefCount(), pEffectRuleDict->IsReferenced());

		for (int i=0; i<pEffectRuleDict->GetCount(); i++)
		{
			ptxEffectRule* pEffectRule = pEffectRuleDict->GetEntry(i);
			if (pEffectRule->GetRefCount()>1)
			{
				grcDebugDraw::AddDebugOutputEx(false, "        %s: %d, %d",
					pEffectRule->GetName(), (pEffectRule->GetRefCount()-1), pEffectRule->IsReferenced());
			}
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxManager::StartProfileUpdate()
{
	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	// initialise the stats 
	m_numActiveInstances = 0;

	// go through the draw lists
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// go through the effect instances
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHead();
		while (pCurrEffectInstItem)
		{
			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();

			// start update profile on each effect rule
			ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();
			ptxEffectRule* pEffectRule = pEffectInst->GetEffectRule();
			ptxAssertf(pEffectRule, "drawlist %u's effect instance list is corrupt - NULL effect rule", i);
			if (pEffectRule)
			{
				pEffectRule->StartProfileUpdate();
			}

			// set the next effect instance in the list
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}
#endif

#if RMPTFX_EDITOR
void ptxManager::StartProfileDraw()
{
	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif

	u8 drawBufferId = RMPTFXTHREADER.GetDrawBufferId();

	// initialise the stats 
	m_numDrawCulledInstances =0;

	// go through the draw lists
	for (int i=0; i<m_drawLists.GetCount(); i++)
	{
		// go through the effect instances
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)m_drawLists[i]->m_effectInstList.GetHeadMT(drawBufferId);
		while(pCurrEffectInstItem)
		{
			// store the next effect instance in the list
			ptxEffectInstItem* pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNextMT(drawBufferId);

			// start draw profile on each effect rule
			ptxEffectInst* pEffectInst = pCurrEffectInstItem->GetDataSB();
			ptxEffectRule* pEffectRule = pEffectInst->GetEffectRule();
			ptxAssertf(pEffectRule, "drawlist %u's effect instance list is corrupt - NULL effect rule", i);
			if (pEffectRule)
			{
				pEffectRule->StartProfileDraw();
			}

			// set the next effect instance in the list
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}

	// check the effect instance list validity
#if RMPTFX_THOROUGH_ERROR_CHECKING
	CheckDataIntegrity();
#endif
}
#endif

#if RMPTFX_EDITOR
void ptxManager::UpdateParticleRuleTexture(const char* pFilename, grcTexture* pTexture)
{
	// rsTODO: should we loop over everything here or just loose / non-resourced rules?

	// go through the particle rules
	ptxParticleRuleIterator particleRuleIterator(CreateFxListIterator());
	for (particleRuleIterator.Start((u32)FXLISTFLAG_ALL); !particleRuleIterator.AtEnd(); particleRuleIterator.Next())
	{
		// update the texture on the shader insts
		ptxParticleRule* pParticleRule = particleRuleIterator.GetCurrRule();
		ptxShaderInst& shaderInst = pParticleRule->GetShaderInst();
		shaderInst.ReplaceTexture(pFilename, pTexture);
	}
}
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
void ptxManager::SaveAll()
{
	// start saving if we're inactive
	if (m_pSaveInfo->currState==PTXSAVESTATE_INACTIVE)
	{
		m_pSaveInfo->currState = PTXSAVESTATE_START; 
	}
}
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
void ptxManager::SaveAllCancel()
{
	// cancel saving if we're active
	if (m_pSaveInfo->currState!=PTXSAVESTATE_INACTIVE)
	{
		ptxDisplayf("Cancelling Save");
		m_pSaveInfo->currState = PTXSAVESTATE_INACTIVE; 
	}
}
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
void ptxManager::SaveAllUpdate()
{
	// settings
	const int NUM_EFFECT_RULE_SAVES_PER_FRAME = 200; 
	const int NUM_EMITTER_RULE_SAVES_PER_FRAME = 200; 
	const int NUM_PARTICLE_RULE_SAVES_PER_FRAME = 200;

	// state machine
	switch (m_pSaveInfo->currState)
	{
		// the save is just starting
		case PTXSAVESTATE_START:
		{
			// initialise the save info
			m_pSaveInfo->currEffectRuleId = 0; 
			m_pSaveInfo->currEmitterRuleId = 0; 
			m_pSaveInfo->currParticleRuleId = 0; 
			m_pSaveInfo->numEffectRules = 0;
			m_pSaveInfo->numEmitterRules = 0;
			m_pSaveInfo->numParticleRules = 0;
			m_pSaveInfo->fxListFlags = FXLISTFLAG_LOOSE | FXLISTFLAG_NONRESOURCED;	// always save loose and nonresourced files - ask about resourced

			// check if there are any resourced fx lists
			bool hasResourced = false;
			ptxFxListIterator fxListIterator = CreateFxListIterator();

			// check if any fx lists are resourced
			fxListIterator.Start(FXLISTFLAG_RESOURCED);
			if (!fxListIterator.AtEnd())
			{
				hasResourced = true;
			}

			// check if resourced data wants saved out as xml
			if (hasResourced)
			{  
				if (bkMessageBox("Save resourced particles as XML?", "Some resourced particle rules are loaded\nDo you want to save them as XML files (resources will not be changed)", bkMsgYesNo, bkMsgQuestion)==1)  
				{   
					ptxDisplayf("Saving...");

					// save resourced fx lists as well
					m_pSaveInfo->fxListFlags |= FXLISTFLAG_RESOURCED;

					// change the state to saving effect rules
					m_pSaveInfo->currState = PTXSAVESTATE_SAVING_EFFECT_RULES_XML; 
				}
				else 
				{
					ptxDisplayf("Cancelling Save");

					// change the state to cancelled
					m_pSaveInfo->currState = PTXSAVESTATE_CANCEL_SAVE_ALL;
				}
			}
			else
			{
				// change the state to saving effect rules
				m_pSaveInfo->currState = PTXSAVESTATE_SAVING_EFFECT_RULES_XML; 
			}

			// check that we haven't cancelled the save
			if (m_pSaveInfo->currState!=PTXSAVESTATE_CANCEL_SAVE_ALL)
			{
				// go through the fx lists counting the total number of rules
				ptxFxListIterator fxListIterator = CreateFxListIterator();
				for (fxListIterator.Start(m_pSaveInfo->fxListFlags); !fxListIterator.AtEnd(); fxListIterator.Next())
				{
					m_pSaveInfo->numEffectRules += fxListIterator.GetCurrFxList()->GetEffectRuleDictionary()->GetCount();
					m_pSaveInfo->numEmitterRules += fxListIterator.GetCurrFxList()->GetEmitterRuleDictionary()->GetCount();
					m_pSaveInfo->numParticleRules += fxListIterator.GetCurrFxList()->GetParticleRuleDictionary()->GetCount();
				}

				// reset the rule iterators for the next state
				m_pSaveInfo->effectRuleIterator.Start(m_pSaveInfo->fxListFlags);
				m_pSaveInfo->emitterRuleIterator.Start(m_pSaveInfo->fxListFlags);
				m_pSaveInfo->particleRuleIterator.Start(m_pSaveInfo->fxListFlags);
			}

			break;
		}
 

		// we're saving out effect rules
		case PTXSAVESTATE_SAVING_EFFECT_RULES_XML: 
		{
			// loop over the number of effect rules we're allowed to save per frame
			for (int i=0; i<NUM_EFFECT_RULE_SAVES_PER_FRAME; i++)
			{
				// check we haven't ran out of effect rules
				if (!m_pSaveInfo->effectRuleIterator.AtEnd())
				{
					// save the effect rule
					ptxEffectRule* pEffectRule = m_pSaveInfo->effectRuleIterator.GetCurrRule();
					ptxDisplayf("Saving Effect Rule %d - %s", m_pSaveInfo->currEffectRuleId, pEffectRule->GetName());
					pEffectRule->Save();
					m_pSaveInfo->currEffectRuleId++;

					// send periodic progress update to the editor
#if RMPTFX_EDITOR
					if (i%10 == 0)
					{
						m_pInterface->SendSaveAllProgressUpdate(0, m_pSaveInfo->numEffectRules, m_pSaveInfo->currEffectRuleId);
					}
#endif

					// get the next effect rule
					m_pSaveInfo->effectRuleIterator.Next();
				}
				else
				{
					// no more effect rules - send progress update to the editor
#if RMPTFX_EDITOR
					m_pInterface->SendSaveAllProgressUpdate(0, m_pSaveInfo->numEffectRules, m_pSaveInfo->currEffectRuleId);
#endif

					// change the state to saving emitter rules
					m_pSaveInfo->currState = PTXSAVESTATE_SAVING_EMITTER_RULES_XML;
					break;
				}
			}

			break; 
		}

		// we're saving out emitter rules
		case PTXSAVESTATE_SAVING_EMITTER_RULES_XML : 
		{
			// loop over the number of emitter rules we're allowed to save per frame
			for (int i=0; i<NUM_EMITTER_RULE_SAVES_PER_FRAME; i++)
			{
				// check we haven't ran out of emitter rules
				if (!m_pSaveInfo->emitterRuleIterator.AtEnd())
				{
					// save the emitter rule
					ptxEmitterRule* pEmitterRule = m_pSaveInfo->emitterRuleIterator.GetCurrRule();
					ptxDisplayf("Saving Emitter Rule %d - %s", m_pSaveInfo->currEmitterRuleId, pEmitterRule->GetName());
					pEmitterRule->Save();
					m_pSaveInfo->currEmitterRuleId++;

					// send periodic progress update to the editor
#if RMPTFX_EDITOR
					if (i%10 == 0)
					{
						m_pInterface->SendSaveAllProgressUpdate(1, m_pSaveInfo->numEmitterRules, m_pSaveInfo->currEmitterRuleId);
					}
#endif

					// get the next emitter rule
					m_pSaveInfo->emitterRuleIterator.Next();
				}
				else 
				{
					// no more emitter rules - send progress update to the editor
#if RMPTFX_EDITOR
					m_pInterface->SendSaveAllProgressUpdate(1,m_pSaveInfo->numEmitterRules,m_pSaveInfo->currEmitterRuleId);
#endif

					// change the state to saving emitter rules
					m_pSaveInfo->currState = PTXSAVESTATE_SAVING_PARTICLE_RULES_XML;
					break;
				}
			}

			break; 
		}

		// we're saving out particle rules
		case PTXSAVESTATE_SAVING_PARTICLE_RULES_XML: 
		{
			// loop over the number of particle rules we're allowed to save per frame
			for (int i=0; i<NUM_PARTICLE_RULE_SAVES_PER_FRAME; i++)
			{
				// check we haven't ran out of particle rules
				if (!m_pSaveInfo->particleRuleIterator.AtEnd())
				{
					// save the particle rule
					ptxParticleRule* pParticleRule = m_pSaveInfo->particleRuleIterator.GetCurrRule();
					ptxDisplayf("Saving Particle Rule %d - %s", m_pSaveInfo->currParticleRuleId, pParticleRule->GetName());
					pParticleRule->Save();
					m_pSaveInfo->currParticleRuleId++;

					// send periodic progress update to the editor
#if RMPTFX_EDITOR
					if (i % 10 == 0)
					{
						m_pInterface->SendSaveAllProgressUpdate(2, m_pSaveInfo->numParticleRules, m_pSaveInfo->currParticleRuleId);
					}
#endif

					// get the next particle rule
					m_pSaveInfo->particleRuleIterator.Next();
				}
				else 
				{
					// no more particle rules - send progress update to the editor
#if RMPTFX_EDITOR
					m_pInterface->SendSaveAllProgressUpdate(2, m_pSaveInfo->numParticleRules, m_pSaveInfo->currParticleRuleId);
#endif

					// change the state to tell the editor that saving is complete
					ptxDisplayf("Finished Saving");
					m_pSaveInfo->currState = PTXSAVESTATE_TELL_EDITOR_COMPLETE; 
					break;
				}
			}

			break; 
		}

		// we're cancelling the save
		case PTXSAVESTATE_CANCEL_SAVE_ALL: 
		{
			// tell the editor that the save has been cancelled
#if RMPTFX_EDITOR
			m_pInterface->SendSaveAllCancelled();
#endif

			// change the state to inactive
			m_pSaveInfo->currState = PTXSAVESTATE_INACTIVE;

			break;
		}

		// we're finished saving
		case PTXSAVESTATE_TELL_EDITOR_COMPLETE: 
		{
			// tell the editor that saving is complete
#if RMPTFX_EDITOR
			m_pInterface->SendSaveAllComplete();
#endif

			// change the state to inactive
			m_pSaveInfo->currState = PTXSAVESTATE_INACTIVE;

			break;
		}

		// we're inactive
		case PTXSAVESTATE_INACTIVE:
			break;

	};
}
#endif // __RESOURCECOMPILER || RMPTFX_EDITOR

#if RMPTFX_XML_LOADING
ptxEffectRule* ptxManager::LoadEffectRule(const char* pEffectRuleName)
{
	// check for a valid effect rule name
	if (pEffectRuleName==NULL || strlen(pEffectRuleName)==0)
	{
		return NULL;
	}

	// rsTODO: which lists should we check here? just shareable?
	// the old code checked fx lists first, then the loose list later - we could mimic this if necessary

	// get the effect rule
	atHashWithStringNotFinal effectRuleHashName;
#if __RESOURCECOMPILER
	effectRuleHashName.SetHash(atHashValue(pEffectRuleName));
#else
	effectRuleHashName.SetFromString(pEffectRuleName);
#endif
	ptxEffectRule* pEffectRule = GetEffectRule(effectRuleHashName, NULL, (u32)FXLISTFLAG_ALL);
	if (pEffectRule)
	{
		// the effect rule already exists - return it
		return pEffectRule;
	}

	// check for cycles as effects can cause other effects to load via effect spawning
	static int s_loadDepth = 0;
	s_loadDepth++;
	if (s_loadDepth >= 16)
	{
		ptxAssertf(false, "found cycle of effect rules while loading %s", pEffectRuleName);
		return NULL;
	}

	// load the effect rule
	pEffectRule = ptxEffectRule::CreateFromFile(pEffectRuleName);

	s_loadDepth--;

	// check that the load was successful
	if (pEffectRule)
	{
		// set the loose fx list to own the effect rule
		pEffectRule->AddRef();
		m_pLooseFxList->GetEffectRuleDictionary()->AddNewEntry(pEffectRuleName, pEffectRule);

#if __ASSERT
		// check for problematic effect rule tunings
		if (pEffectRule)
		{
			// check for effects that when off-screen will never die out, accumulating in number until they overflow the system
			if (pEffectRule && (pEffectRule->GetUpdateWhenViewportCulled()==false || pEffectRule->GetUpdateWhenDistanceCulled()==false) && pEffectRule->GetNumLoops()!=-1)
			{
				ptxAssertf(0, "effect rule %s has bad tuning data", pEffectRule->GetName());
			}
		}
#endif
	}

	// return the effect rule
	return pEffectRule;
}
#endif

#if RMPTFX_XML_LOADING
ptxEmitterRule* ptxManager::LoadEmitterRule(const char* pEmitterRuleName)
{
	// check for a valid emitter rule name
	if (pEmitterRuleName==NULL || strlen(pEmitterRuleName)==0)
	{
		return NULL;
	}

	// rsTODO: which lists should we check here? just shareable?
	// the old code checked fx lists first, then the loose list later - we could mimic this if necessary

	// get the emitter rule
	atHashWithStringNotFinal emitterRuleHashName;
#if __RESOURCECOMPILER
	emitterRuleHashName.SetHash(atHashValue(pEmitterRuleName));
#else
	emitterRuleHashName.SetFromString(pEmitterRuleName);
#endif
	ptxEmitterRule* pEmitterRule = GetEmitterRule(emitterRuleHashName, NULL, (u32)FXLISTFLAG_ALL);
	if (pEmitterRule)
	{
		// the emitter rule already exists - return it
		return pEmitterRule;
	}

	// load the emitter rule
	pEmitterRule = ptxEmitterRule::CreateFromFile(pEmitterRuleName);
	if (pEmitterRule)
	{
		// set the loose fx list to own the emitter rule
		pEmitterRule->AddRef();
		m_pLooseFxList->GetEmitterRuleDictionary()->AddNewEntry(pEmitterRuleName, pEmitterRule);
	}

	// return the emitter rule
	return pEmitterRule;
}
#endif

#if RMPTFX_XML_LOADING
ptxParticleRule* ptxManager::LoadParticleRule(const char* pParticleRuleName)
{
	// check for a valid particle rule name
	if (pParticleRuleName==NULL || strlen(pParticleRuleName)==0)
	{
		return NULL;
	}

	// rsTODO: which lists should we check here? just shareable?
	// the old code checked fx lists first, then the loose list later - we could mimic this if necessary

	// get the particle rule
	atHashWithStringNotFinal particleRuleHashName;
#if __RESOURCECOMPILER
	particleRuleHashName.SetHash(atHashValue(pParticleRuleName));
#else
	particleRuleHashName.SetFromString(pParticleRuleName);
#endif
	ptxParticleRule* pParticleRule = GetParticleRule(particleRuleHashName, NULL, (u32)FXLISTFLAG_ALL);
	if (pParticleRule)
	{
		// the particle rule already exists - return it
		return pParticleRule;
	}

	// load the particle rule
	pParticleRule = ptxParticleRule::CreateFromFile(pParticleRuleName);
	if (pParticleRule)
	{
		// give it the correct point pool
		pParticleRule->SetPointPool(&m_pointPool);

		// set the loose fx list to own the particle rule
		pParticleRule->AddRef();
		m_pLooseFxList->GetParticleRuleDictionary()->AddNewEntry(pParticleRuleName, pParticleRule);
	}

	// return the particle rule
	return pParticleRule;
}
#endif

#if RMPTFX_XML_LOADING
bool ptxManager::RemoveEffectRule(ptxEffectRule* pEffectRule, bool UNUSED_PARAM(removeDependents), bool justRemoveFromList)
{
	ptxAssertf(pEffectRule, "effect rule is invalid");

	// finish all effect insts using this effect rule
	FinishEffectInsts(pEffectRule);

	// reset the effect rule
	pEffectRule->Reset();

	// remove the effect rule from the loose fx list
	ptxVerifyf(m_pLooseFxList->GetEffectRuleDictionary()->DeleteEntry(pEffectRule->GetName()), "tried to remove an effect rule (%s) that wasn't in the loose fx list", pEffectRule->GetName());

	// delete the effect rule, if required
	if (justRemoveFromList==false)
	{
		delete pEffectRule;
	}

	return true;
}
#endif

#if RMPTFX_XML_LOADING
bool ptxManager::RemoveEmitterRule(ptxEmitterRule* pEmitterRule, bool UNUSED_PARAM(removeDependents), bool justRemoveFromList)
{
	ptxAssertf(pEmitterRule, "emitter rule is invalid");
	ptxDisplayf("attempting to remove emitter rule %s with %d referenes", pEmitterRule->GetName(), pEmitterRule->GetRefCount()); 

	// don't remove the emitter rule if it's in use
	if (pEmitterRule->IsInUse())
	{
		return false;
	}

	// remove the emitter rule from the loose fx list
	ptxVerifyf(m_pLooseFxList->GetEmitterRuleDictionary()->DeleteEntry(pEmitterRule->GetName()), "tried to remove an emitter rule (%s) that wasn't in the loose fx list", pEmitterRule->GetName());

	// release the emitter rule, if required
	if (justRemoveFromList==false)
	{
		pEmitterRule->Release();
	}

	return true;
}
#endif

#if RMPTFX_XML_LOADING
bool ptxManager::RemoveParticleRule(ptxParticleRule* pParticleRule, bool UNUSED_PARAM(removeDependents), bool justRemoveFromList)
{
	ptxAssertf(pParticleRule, "particle rule is invalid");

	// don't remove the particle rule if it's in use
	if (pParticleRule->IsInUse())
	{
		return false;
	}

	// remove the emitter rule from the loose fx list
	ptxVerifyf(m_pLooseFxList->GetParticleRuleDictionary()->DeleteEntry(pParticleRule->GetName()), "tried to remove a particle rule (%s) that wasn't in the loose fx list", pParticleRule->GetName());

	// release the particle rule, if required
	if (justRemoveFromList==false)
	{
		pParticleRule->Release();
	}

	return true;
}
#endif

#if RMPTFX_XML_LOADING
void ptxManager::AddBehavioursWithActiveEvos()
{
	ptxEffectRuleIterator effectRuleIterator(CreateFxListIterator());	   
	for (effectRuleIterator.Start((u32)FXLISTFLAG_ALL); !effectRuleIterator.AtEnd(); effectRuleIterator.Next())
	{
		ptxEffectRule* pEffectRule = effectRuleIterator.GetCurrRule();
		pEffectRule->AddBehavioursWithActiveEvos();
	}
}
#endif

#if RMPTFX_USE_PARTICLE_SHADOWS
void ptxManager::SetShadowPass(bool enable)
{
	m_ShadowPass[g_RenderThreadIndex] = enable;
#if GS_INSTANCED_SHADOWS
	ptxDrawInterface::SetShadowPass(enable);
#endif
}
#endif

void ptxManager::InitRenderStateBlocks()
{
	grcRasterizerStateDesc rsd;
	rsd.CullMode = grcRSV::CULL_BACK;
	rsd.FillMode = grcRSV::FILL_SOLID;
	sm_exitDrawManualRasterizerState = grcStateBlock::CreateRasterizerState(rsd);

	grcBlendStateDesc bsd;
	bsd.BlendRTDesc[0].BlendEnable = 1;
	bsd.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	bsd.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_SRCALPHA;
	bsd.BlendRTDesc[0].BlendOp = grcRSV::BLENDOP_ADD;
	sm_exitDrawManualBlendState = grcStateBlock::CreateBlendState(bsd);

	grcDepthStencilStateDesc dssd;
	dssd.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	sm_exitDrawManualDepthStencilState = grcStateBlock::CreateDepthStencilState(dssd);

	sm_drawManualRasterizerState = grcStateBlock::RS_Default;
	sm_drawManualBlendState = grcStateBlock::BS_Normal;
	grcDepthStencilStateDesc dssmanual;
	dssmanual.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	sm_drawManualDepthStencilState = grcStateBlock::CreateDepthStencilState(dssmanual);

	sm_drawManualShadowsRasterizerState = grcStateBlock::RS_Default;
	sm_drawManualShadowsBlendState = grcStateBlock::BS_Normal;
	sm_drawManualShadowsDepthStencilState = grcStateBlock::DSS_TestOnly_LessEqual;
}

void ptxManager::SetDrawManualRenderState()
{
#if RMPTFX_USE_PARTICLE_SHADOWS
	if( IsShadowPass() )
	{
		grcStateBlock::SetRasterizerState(sm_drawManualShadowsRasterizerState);
		grcStateBlock::SetBlendState(sm_drawManualShadowsBlendState);
		grcStateBlock::SetDepthStencilState(sm_drawManualShadowsDepthStencilState);
	}
	else
	{
#endif
		grcStateBlock::SetRasterizerState(sm_drawManualRasterizerState);
		grcStateBlock::SetBlendState(sm_drawManualBlendState);
		grcStateBlock::SetDepthStencilState(sm_drawManualDepthStencilState);
#if RMPTFX_USE_PARTICLE_SHADOWS
	}
#endif
}

void ptxManager::SetExitDrawManualRenderState() 
{
	grcStateBlock::SetRasterizerState(sm_exitDrawManualRasterizerState);
	grcStateBlock::SetBlendState(sm_exitDrawManualBlendState);
	grcStateBlock::SetDepthStencilState(sm_exitDrawManualDepthStencilState);
}


#if RMPTFX_BANK
void ptxManager::RemoveDefaultKeyframes()
{
	for (int j=0; j<ptxShaderTemplateList::GetNumShaders(); j++)
	{
		const ptxShaderTemplate* pShaderTemplate = ptxShaderTemplateList::GetShader(j);
		const ptxShaderTemplateVars& templateVars = pShaderTemplate->GetShaderVars();
		for (int i=0; i<templateVars.GetCount(); i++)
		{
			ptxShaderVar* pShaderVar = *templateVars.GetItem(i);
			if (pShaderVar && pShaderVar->GetType()==PTXSHADERVAR_KEYFRAME)
			{
				ptxShaderVarKeyframe* pShaderVarKF = reinterpret_cast<ptxShaderVarKeyframe*>(pShaderVar);
				if (pShaderVarKF)
				{
					ptxKeyframe& keyframe = pShaderVarKF->GetKeyframe();
					if (keyframe.IsDefault())
					{
						keyframe.DeleteEntry(0);
					}
				}
			}
		}
	}
}
#endif


#if RMPTFX_BANK
void ptxManager::GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def)
{
	for (int j=0; j<ptxShaderTemplateList::GetNumShaders(); j++)
	{
		const ptxShaderTemplate* pShaderTemplate = ptxShaderTemplateList::GetShader(j);
		const ptxShaderTemplateVars& templateVars = pShaderTemplate->GetShaderVars();
		for (int i=0; i<templateVars.GetCount(); i++)
		{
			ptxShaderVar* pShaderVar = *templateVars.GetItem(i);
			if (pShaderVar && pShaderVar->GetType()==PTXSHADERVAR_KEYFRAME)
			{
				ptxShaderVarKeyframe* pShaderVarKF = reinterpret_cast<ptxShaderVarKeyframe*>(pShaderVar);
				if (pShaderVarKF)
				{
					ptxKeyframe& keyframe = pShaderVarKF->GetKeyframe();

					int currNum = keyframe.GetNumEntries();
					num += currNum;
					mem += keyframe.GetEntryMem();

					if (currNum>max)
					{
						max = currNum;
					}

					if (keyframe.IsDefault())
					{
						def++;
					}
				}
			}
		}
	}
}
#endif


} // namespace rage
