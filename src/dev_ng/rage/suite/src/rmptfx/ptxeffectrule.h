// 
// rmptfx/ptxeffectrule.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXEFFECTRULE_H
#define RMPTFX_PTXEFFECTRULE_H


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxkeyframeprop.h"
#include "rmptfx/ptxtimeline.h"

// includes (rage)
#include "math/random.h"
#include "paging/base.h"
#include "system/interlocked.h"
#include "system/typeinfo.h"
#include "vectormath/vec3v.h"


// namespaces
namespace rage 
{


// forward declarations
class ptxEffectInst;
class ptxEvolutionList;
class ptxFxList;


// defines
#define PTXEFFECTRULE_RORC_VERSION		(21)
#define PTXEFFECTRULE_FILE_VERSION		(4.2f)


// structures
#if RMPTFX_EDITOR
struct ptxEffectRuleUIData
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxEffectRuleUIData();

 		void StartProfileUpdate();
 		void StartProfileDraw();
 
 		void IncAccumCPUUpdateTime(float val)					{ AtomicAddF32(&m_accumCPUUpdateTime, val); }
 		void IncAccumCPUDrawTime(float val)						{ AtomicAddF32(&m_accumCPUDrawTime,   val); }
 		void IncNumPointsUpdated(int val)						{ sysInterlockedAdd((u32*)&m_numPointsUpdated,   val); }
		void IncNumActiveInstances(int val)						{ sysInterlockedAdd((u32*)&m_numActiveInstances, val); }
		void IncNumCulledInstances(int val)						{ sysInterlockedAdd((u32*)&m_numCulledInstances, val); }

		float GetAccumCPUUpdateTime()							{ return m_accumCPUUpdateTime; }
		float GetAccumCPUDrawTime()								{ return m_accumCPUDrawTime; }
		int GetNumPointsUpdated()								{ return m_numPointsUpdated; }
		int GetNumActiveInstances()								{ return m_numActiveInstances; }
		int GetNumCulledInstances()								{ return m_numCulledInstances; }
 
 		bool GetShowInstances()									{ return m_showInstances; }
		bool GetShowCullSpheres()								{ return m_showCullSpheres; }
		bool GetShowCollision()									{ return m_showColn; }
		bool GetShowDataVolumes()								{ return m_showDataVolumes; }
		bool GetShowPointQuads()								{ return m_showPointQuads; }
 
 		void SendDebugDataToEditor(ptxByteBuff& buff);
		void ReceiveDebugDataFromEditor(ptxByteBuff& buff);

		void AccumulateFrom(ptxEffectRuleUIData& uiData);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		// profile data
		int m_numPointsUpdated;
		int m_numActiveInstances;
		int m_numCulledInstances;
		float m_accumCPUUpdateTime;
		float m_accumCPUDrawTime;

		// debug data
		bool m_showInstances;
		bool m_showCullSpheres;
		bool m_showColn;
		bool m_showDataVolumes;
		bool m_showPointQuads;

		static inline void AtomicAddF32(volatile float *dst, float val)
		{
			float prevF32 = *dst;
			u32 prevU32 = union_cast<u32>(prevF32);
			for (;;)
			{
				u32 updated = union_cast<u32>(prevF32+val);
				u32 result = sysInterlockedCompareExchange((volatile u32*)dst, updated, prevU32);
				if (result == prevU32)
				{
					return;
				}
				prevU32 = result;
				prevF32 = union_cast<float>(prevU32);
			}
		}
};
#endif


// classes
class ptxEffectRule : public pgBaseRefCounted
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEffectRule();
		~ptxEffectRule();

		// resourcing
		ptxEffectRule(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxEffectRule);

		// hide implementation of these functions in pgBase
		static void InitClass() {}
		static void ShutdownClass() {}

		// init functions
		void Init() {}
		void PostLoad();
		void Reset();

		// evolutions
		void PrepareEvoData();

		// keyframe definitions
		void SetKeyframeDefns();
		void SetEvolvedKeyframeDefns();

		// file info accessors
		float GetFileVersion()									{ return m_fileVersion; }
		const char* GetName() const								{ return m_name.c_str(); }
		void SetName(const char* pName)							{ if (m_name != pName)	{m_name = pName;} }
		ptxFxList* GetFxList()									{ return m_pFxList; }

		// timeline and evolution data accessors
		ptxTimeLine& GetTimeline()								{ return m_timeline; }
		bool GetIsInfinitelyLooped()							{ return m_numLoops==-1; }
		ptxEvolutionList* GetEvolutionList()					{ return m_pEvolutionList; }
		bool GetIsShortLived() const 							{ return m_isShortLived; }

		bool GetHasNoShadows() const							{ return m_hasNoShadows; }

		// effect setup accessors
		Vec3V_Out GetRandOffsetPos()							{ return m_vRandOffsetPos; }
		int GetNumLoops()										{ return m_numLoops; }
		float GetPreUpdateTime()								{ return m_preUpdateTime; }
		float GetPreUpdateTimeInterval()						{ return m_preUpdateTimeInterval; }
		u8 GetDrawListIdx()										{ return m_drawListId; }
		float GetRandDuration()									{ return g_DrawRand.GetRanged(m_durationMin, m_durationMax); }
		float GetRandPlaybackRateScalar()						{ return g_DrawRand.GetRanged(m_playbackRateScalarMin, m_playbackRateScalarMax); }
		bool GetSortEventsByDistance()							{ return m_sortEventsByDistance; }

		// culling accessors
		u8 GetViewportCullingMode()								{ return m_viewportCullingMode; }
		bool GetUpdateWhenViewportCulled()						{ return m_updateWhenViewportCulled; }	
		bool GetRenderWhenViewportCulled()						{ return m_renderWhenViewportCulled; }	
		bool GetEmitWhenViewportCulled()						{ return m_emitWhenViewportCulled; }	
		u8 GetDistanceCullingMode()								{ return m_distanceCullingMode; }
		bool GetUpdateWhenDistanceCulled()						{ return m_updateWhenDistanceCulled; }	
		bool GetRenderWhenDistanceCulled()						{ return m_renderWhenDistanceCulled; }	
		bool GetEmitWhenDistanceCulled()						{ return m_emitWhenDistanceCulled; }
		float GetViewportCullingSphereRadius()					{ return m_viewportCullingSphereRadius; }
		Vec3V_Out GetViewportCullingSphereOffset()				{ return m_viewportCullingSphereOffset; }
		Vec4V_Out GetViewportCullingSphere()					{ return Vec4V(m_viewportCullingSphereOffset, ScalarV(m_viewportCullingSphereRadius)); }
		float GetDistanceCullingFadeDist()						{ return m_distanceCullingFadeDist; }
		float GetDistanceCullingCullDist()						{ return m_distanceCullingCullDist; }

		// lod accessors
		float GetLodEvoDistMin()								{ return m_lodEvoDistMin; }
		float GetLodEvoDistMax()								{ return m_lodEvoDistMax; }

		// collision accessors
		u8 GetColnType()										{ return m_colnType; }
		bool GetShareEntityColn()								{ return m_shareEntityColn; }
		bool GetOnlyUseBVHColn()								{ return m_onlyUseBVHColn; }
		float GetColnRange()									{ return m_colnRange; }
		float GetColnProbeDist()								{ return m_colnProbeDist; }

		// game specific accessors
		bool GetGameFlag(const unsigned bit)					{ return BIT_ISSET(bit, m_gameFlags); }

		// keyframe accessors
		u8 GetColourTintMaxEnable()								{ return m_colourTintMaxEnable; }
		bool GetUseDataVolume()									{ return m_useDataVolume; }
		u8 GetDataVolumeType()									{ return m_dataVolumeType; }
		ptxKeyframeProp& GetColourTintMinKFP()					{ return m_colourTintMinKFP; }
		ptxKeyframeProp& GetColourTintMaxKFP()					{ return m_colourTintMaxKFP; }
		ptxKeyframeProp& GetZoomScalarKFP()						{ return m_zoomScalarKFP; }
		ptxKeyframeProp& GetDataSphereKFP()						{ return m_dataSphereKFP; }
		ptxKeyframeProp& GetDataCapsuleKFP()					{ return m_dataCapsuleKFP; }
		ptxKeyframePropList& GetKeyframePropList()				{ return m_keyframePropList; }

		// info accessors
#if RMPTFX_EDITOR
		float GetZoomableComponentScalar()						{ return m_zoomLevel; }
#endif
		int GetNumActiveInstances()								{ return m_numActiveInstances;}
		void IncNumActiveInstances()							{ m_numActiveInstances++;}
		void DecNumActiveInstances()							{ m_numActiveInstances--;}
	
		// queries
		bool HasBehaviour(const char* pBehaviourName);

		float GetMaxDuration();
		bool CheckForPtxRuleShadows();

		// xml loading / saving
#if RMPTFX_XML_LOADING
		static ptxEffectRule* CreateFromFile(const char* pFilename);
		bool Load(const char* pFilename);
		void AddBehavioursWithActiveEvos();
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
		void Save();
		void SaveAll();
#endif

		// editor
#if RMPTFX_EDITOR
		void CreateUiObjects();

		void SendToEditor(ptxByteBuff& buff);
		void ReceiveFromEditor(ptxByteBuff& buff);

		bool ContainsParticleRule(const char* pParticleRuleName);
		void SetGameFlag(const unsigned bit, bool state)		{ m_gameFlags = state ? BIT_SET(bit, m_gameFlags) : BIT_CLEAR(bit, m_gameFlags); }

		void RescaleZoomableComponents();

		// evolutions
		void CreateEvolution(const char* pEvoName);
		void DeleteEvolution(const char* pEvoName);
		void AddEvolvedKeyframeProp(const char* pEvoName, u32 propertyId, int eventId);
		void DeleteEvolvedKeyframeProp(const char* pEvoName, u32 propertyId, int eventId);
		void UpdateEvoUI(ptxKeyframeProp* pKeyframeProp);

		// profiling
		ptxEffectRuleUIData& GetUIData()						{ return *m_pUIData; }
		void StartProfileUpdate();
		void StartProfileDraw();
#endif

		// debug or editor
#if RMPTFX_BANK || RMPTFX_EDITOR
		void RemoveDefaultKeyframes();
#endif

		// debug
#if RMPTFX_BANK
		void GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def);
		void OutputKeyframeEntryInfo();
#endif

		// parser
#if RMPTFX_XML_LOADING
		void RepairAndUpgradeData(const char* pFilename);
		void ResolveReferences(ptxFxList* pFxList);
#endif

	private: //////////////////////////

		// misc
		void RemoveDependents(bool removeFromManager);

		// editor
#if RMPTFX_EDITOR
		void CreateUIData();
#else
		void CreateUIData()										{ m_pUIData = NULL; }
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE
#endif

public:


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////
		// file info
		float m_fileVersion;												// the effect rule's file version
		ConstString m_name;													// the name of the effect rule
		datRef<ptxFxList> m_pFxList;										// pointer to the effect list where we have loaded from

		// timeline and evolution data
		ptxTimeLine m_timeline;												// the effect rule's timeline
		datOwner<ptxEvolutionList> m_pEvolutionList;						// pointer to the list of evolutions on the effect

		// effect setup
		int m_numLoops;														// the number of times the timeline should loop back to the start
		bool m_sortEventsByDistance;										// whether the timeline events are sorted by distance to the camera or not
		u8 m_drawListId;													// the index of the draw list that this effect belongs to
		bool m_isShortLived;												// whether the effect is short lived (so it's ambient scale lookup can be optimised)
		bool m_hasNoShadows;												// whether the effect has no shadows
		//48
		Vec3V m_vRandOffsetPos;												// random offset position applied to the effect every loop
		//64
		float m_preUpdateTime;												// the amount of time that the effect should be updated for when first started
		float m_preUpdateTimeInterval;										// the interval for each update for the effect for when first started
		float m_durationMin;												// minimum duration of the effect
		float m_durationMax;												// maximum duration of the effect
		//80
		float m_playbackRateScalarMin;										// minimum playback rate scalar of the effect
		float m_playbackRateScalarMax;										// maximum playback rate scalar of the effect

		// culling
		u8 m_viewportCullingMode;											// the viewport culling mode
		bool m_renderWhenViewportCulled;									// whether to render this effect when it is viewport culled
		bool m_updateWhenViewportCulled;									// whether to update this effect when it is viewport culled	
		bool m_emitWhenViewportCulled;										// whether this effect emits particles when it is viewport culled
		u8 m_distanceCullingMode;											// the distance culling mode
		bool m_renderWhenDistanceCulled;									// whether to render this effect when it is distance culled	
		bool m_updateWhenDistanceCulled;									// whether to update this effect when it is distance culled		
		bool m_emitWhenDistanceCulled;										// whether this effect emits particles when it is distance culled
		//96
		// [SPHERE-OPTIMISE] should be "Vec4V m_viewportCullingSphere"
		Vec3V m_viewportCullingSphereOffset;								// the offset of the viewport culling sphere
		//112
		float m_viewportCullingSphereRadius;								// the radius of the viewport culling sphere
		float m_distanceCullingFadeDist;									// the distance at which the effect starts to fade out
		float m_distanceCullingCullDist;									// the distance at which the effect is totally culled	

		// lod
		float m_lodEvoDistMin;												// the distance at which the lod evolution kicks in 
		//128
		float m_lodEvoDistMax;												// the distance at which the lod evolution is at max

		// collision
		float m_colnRange;													// the range to query for collision
		float m_colnProbeDist;												// the distance to probe for collision	

		u8 m_colnType;														// the type of collision that this effect has	
		bool m_shareEntityColn;												// whether the collision is registered on the entity (so it can be shared with other effects)
		bool m_onlyUseBVHColn;												// whether the collision only uses bvh bounds

		// game specific
		u8 m_gameFlags;														// per game flags that can be tuned via the editor
		//144

		// keyframes
		ptxKeyframeProp m_colourTintMinKFP;									// keyframeable minimum colour tint of the effect
		//176
		ptxKeyframeProp m_colourTintMaxKFP;									// keyframeable maximum colour tint of the effect
		//208
		ptxKeyframeProp m_zoomScalarKFP;									// keyframeable size scalar of the effect
		//240
		ptxKeyframeProp m_dataSphereKFP;									// keyframeable data sphere attached to the effect
		//272
		ptxKeyframeProp m_dataCapsuleKFP; 									// keyframeable capsule sphere attached to the effect  
		//304
		ptxKeyframePropList m_keyframePropList;								// list of keyframeable properties

		bool m_colourTintMaxEnable;											// whether the max colour tint keyframe is enabled
		bool m_useDataVolume;												// whether this effect uses a data volume
		u8 m_dataVolumeType;												// the type of data volume

		// info
		datPadding<1> m_pad3;
		u32 m_numActiveInstances;											// number of active instances of this effect rule
		//320

		// zoom
		float m_zoomLevel;													// editor only zoom level used when rescaling zoomable components (MN - needs renamed to m_zoomableComponentScalar) 

		// ui data
#if RMPTFX_EDITOR
		ptxEffectRuleUIData* m_pUIData;										// profile and debug data used by the editor
#else																		
		void* m_pUIData;													// fake editor data (to avoid different size resources)
#endif	
		datPadding<8> m_pad4;
		//336

		static ptxKeyframeDefn sm_colourTintMinDefn;						// keyframeable property definitions
		static ptxKeyframeDefn sm_colourTintMaxDefn;
		static ptxKeyframeDefn sm_zoomScalarDefn;
		static ptxKeyframeDefn sm_dataSphereDefn;
		static ptxKeyframeDefn sm_dataCapsuleDefn;


};


} // namespace rage


#endif // RMPTFX_PTXEFFECTRULE_H
