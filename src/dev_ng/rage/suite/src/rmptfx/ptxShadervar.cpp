// 
// rmptfx/ptxshadervar.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "ptxshadervar.h"
#include "ptxshadervar_parser.h"

#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxfxlist.h"
#include "rmptfx/ptxmanager.h"

// includes
#include "file/asset.h"
#include "grcore/texturereference.h"
#include "parser/treenode.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// defines
#define PTXSTR_SHADERVARTYPE_INVALID	""
#define PTXSTR_SHADERVARTYPE_FLOAT		"float"
#define PTXSTR_SHADERVARTYPE_FLOAT2 	"float2"
#define PTXSTR_SHADERVARTYPE_FLOAT3 	"float3"
#define PTXSTR_SHADERVARTYPE_FLOAT4		"float4"
#define PTXSTR_SHADERVARTYPE_TEXTURE	"texture"
#define PTXSTR_SHADERVARTYPE_KEYFRAME	"keyframe"

#define PTXSTR_PROGVAR_FRAMEMAP			"FrameMap"
#define	PTXSTR_PROGVAR_BLENDMODE		"BlendMode"
#define	PTXSTR_PROGVAR_CHANNELMASK		"ChannelMask"

#define PTXSTR_UIDATA_INVALID			"null"
#define PTXSTR_UIDATA_TOKEN				"uidata"

#define PTXSTR_TREENODE_TEXTURENAME		"textureName"
#define PTXSTR_TEXTURE_PACK_PATH		"pack:/"
#define PTXSTR_TEXTURE_EXT				".dds"
#define PTX_TEXTURE_EXT_LENGTH			4


// namespaces
using namespace rage;


// static variables
const char* ptxShaderVarUtils::sm_varTypeNameTable[PTXSHADERVAR_COUNT] = 
{ 	
	PTXSTR_SHADERVARTYPE_INVALID,
	PTXSTR_SHADERVARTYPE_INVALID,
	PTXSTR_SHADERVARTYPE_FLOAT,	
	PTXSTR_SHADERVARTYPE_FLOAT2, 
	PTXSTR_SHADERVARTYPE_FLOAT3, 
	PTXSTR_SHADERVARTYPE_FLOAT4,	
	PTXSTR_SHADERVARTYPE_TEXTURE,
	PTXSTR_SHADERVARTYPE_KEYFRAME,
};

const char* ptxShaderVarUtils::sm_progVarNameTable[PTXPROGVAR_RMPTFX_COUNT] = 
{	
	PTXSTR_PROGVAR_FRAMEMAP,
	PTXSTR_PROGVAR_BLENDMODE,
	PTXSTR_PROGVAR_CHANNELMASK
};


// code
const char* ptxShaderVarUtils::GetVarTypeName(ptxShaderVarType type) 
{ 
	ptxAssertf((type>=PTXSHADERVAR_FLOAT && type<PTXSHADERVAR_COUNT), "shader var type out of range");
	return sm_varTypeNameTable[type]; 
};

const char* ptxShaderVarUtils::GetProgrammedVarName(ptxShaderProgVarType type) 
{
	ptxAssertf((type>=PTXPROGVAR_FIRST && type<PTXPROGVAR_RMPTFX_COUNT), "programmed shader var out of range");
	return sm_progVarNameTable[type];
}





ptxShaderVar::ptxShaderVar() 
{
	m_pInfo = NULL; 
	m_isKeyframeable = false; 
	m_ownsInfo = true; 
	m_type = PTXSHADERVAR_INVALID;
}

ptxShaderVar::~ptxShaderVar() 
{
	if (m_ownsInfo) 
	{
		delete m_pInfo;
	}
}

ptxShaderVar::ptxShaderVar(datResource& /*rsc*/)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxShaderVar_num++;
#endif
}	

#if __DECLARESTRUCT
void ptxShaderVar::DeclareStruct(datTypeStruct& s)
{
	// m_pInfo points to shader data (in the temp heap) - null it out for now
	m_pInfo = NULL;

	SSTRUCT_BEGIN_BASE(ptxShaderVar, datBase)
		SSTRUCT_IGNORE(ptxShaderVar, m_pInfo)
		SSTRUCT_FIELD(ptxShaderVar, m_hashName)
		SSTRUCT_FIELD_AS(ptxShaderVar, m_type, u32)
		SSTRUCT_FIELD_AS(ptxShaderVar, m_id, u32)
		SSTRUCT_FIELD(ptxShaderVar, m_isKeyframeable)
		SSTRUCT_FIELD(ptxShaderVar, m_ownsInfo)
		SSTRUCT_IGNORE(ptxShaderVar, m_pad)
	SSTRUCT_END(ptxShaderVar)
}	
#endif

void ptxShaderVar::Place(ptxShaderVar* pShaderVar, datResource& rsc)
{
	switch (pShaderVar->m_type)
	{
		case PTXSHADERVAR_FLOAT:
		case PTXSHADERVAR_FLOAT2:
		case PTXSHADERVAR_FLOAT3:
		case PTXSHADERVAR_FLOAT4:
		{
			ptxShaderVarVector* pShaderVarVec = reinterpret_cast<ptxShaderVarVector*>(pShaderVar);
			ptxShaderVarVector::Place(pShaderVarVec, rsc);

			if (!datResource_IsDefragmentation)
			{
				pShaderVarVec->SetType(pShaderVar->GetType());

				if (pShaderVar->GetType()<=PTXSHADERVAR_FLOAT3)
				{
					pShaderVarVec->m_vector.SetW(ScalarV(V_ZERO));
				}
				if (pShaderVar->GetType()<=PTXSHADERVAR_FLOAT2)
				{
					pShaderVarVec->m_vector.SetZ(ScalarV(V_ZERO));
				}
				if (pShaderVar->GetType()<=PTXSHADERVAR_FLOAT)
				{
					pShaderVarVec->m_vector.SetY(ScalarV(V_ZERO));
				}
			}
			break;
		}
		case PTXSHADERVAR_TEXTURE:
		{
			ptxShaderVarTexture* pShaderVarTex = reinterpret_cast<ptxShaderVarTexture*>(pShaderVar);
			ptxShaderVarTexture::Place(pShaderVarTex, rsc);
			break;
		}
		case PTXSHADERVAR_KEYFRAME:
		{
			ptxShaderVarKeyframe* pShaderVarKF = reinterpret_cast<ptxShaderVarKeyframe*>(pShaderVar);
			ptxShaderVarKeyframe::Place(pShaderVarKF, rsc);
			break;
		}
		default:
		{
			ptxErrorf("invalid shader var type - could be out of date resource");
			break;
		}
	}
}

void ptxShaderVar::OnPostSaveBase(parTreeNode* node)
{
	const char* varName = atFinalHashString::TryGetString(m_hashName);
	node->AppendStdLeafChild("name", varName, false);
}

void ptxShaderVar::OnPreLoadBase(parTreeNode* node)
{
	const char* varName = NULL;
	if (node->FindValueFromPath("name", varName))
	{
		atFinalHashString hash(varName);
		m_hashName = hash.GetHash();
	}
	else
	{
		ptxErrorf("Missing <name> in shader variable");
	}
}

void ptxShaderVar::CopyUiDataFromTemplate(ptxShaderVar* pShaderVar)
{
	m_pInfo = pShaderVar->m_pInfo;
	m_id = pShaderVar->m_id;
	m_isKeyframeable = pShaderVar->m_isKeyframeable;
	m_ownsInfo = false;
	m_hashName = pShaderVar->m_hashName;
}

void ptxShaderVar::SetInfo(const grmVariableInfo& varInfo)
{
	grmVariableInfo* pNewVarInfo = rage_new grmVariableInfo();
	*pNewVarInfo = varInfo;
	m_pInfo  = pNewVarInfo;
}

#if RMPTFX_EDITOR
void ptxShaderVar::SendDefnToEditor(ptxByteBuff& buff)
{
	if (m_pInfo==NULL) 
	{
		return;
	}

	buff.Write_const_char(PTXSTR_UIDATA_TOKEN);
	buff.Write_const_char(m_pInfo->m_Name);
	buff.Write_const_char(ptxShaderVarUtils::GetVarTypeName(m_type));
	buff.Write_float(m_pInfo->m_UiMin);
	buff.Write_float(m_pInfo->m_UiMax);
	buff.Write_float(m_pInfo->m_UiStep);
}

void ptxShaderVar::SendToEditor(ptxByteBuff& buff, const ptxParticleRule* UNUSED_PARAM(pParticleRule))
{
	buff.Write_const_char(PTXSTR_UIDATA_INVALID);
}
#endif //RMPTFX_EDITOR




ptxShaderVarVector::ptxShaderVarVector() : m_vector(0.0f, 0.0f, 0.0f, 0.0f) {m_type = PTXSHADERVAR_INVALID;};
ptxShaderVarVector::ptxShaderVarVector(ptxShaderVarType type) : m_vector(0.0f, 0.0f, 0.0f, 0.0f) {m_type = type;};


ptxShaderVarVector::ptxShaderVarVector(datResource& rsc)
: ptxShaderVar(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxShaderVarVector_num++;
#endif
}

#if __DECLARESTRUCT
void ptxShaderVarVector::DeclareStruct(datTypeStruct& s)
{
	ptxShaderVar::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxShaderVarVector, ptxShaderVar)
		SSTRUCT_FIELD(ptxShaderVarVector, m_vector)
	SSTRUCT_END(ptxShaderVarVector)
}
#endif

IMPLEMENT_PLACE(ptxShaderVarVector);

ptxShaderVar* ptxShaderVarVector::Clone()
{
	ptxShaderVarVector* pShaderVarVec = rage_new ptxShaderVarVector( static_cast<ptxShaderVarType>(m_type));
	pShaderVarVec->m_hashName = m_hashName;
	pShaderVarVec->m_id = m_id;
	pShaderVarVec->m_vector = m_vector;
	pShaderVarVec->m_ownsInfo = false;
	pShaderVarVec->m_pInfo = m_pInfo;

	return pShaderVarVec;
}

void ptxShaderVarVector::SetShaderVar(grmShader* pShader)		
{
	// TBR: there's room for optimising this 
	// ideally would always send the Vector4, however, cannot send the full 16 bytes as 360 implementation will complain about types not matching
	switch(m_type)
	{
	case PTXSHADERVAR_FLOAT:
		{
			pShader->SetVar(m_id, m_vector.GetXf()); 
			break;
		};
	case PTXSHADERVAR_FLOAT2:
		{
			Vec2V vec2(m_vector.GetXf(), m_vector.GetYf());
			pShader->SetVar(m_id, vec2); 
			break;
		};
	case PTXSHADERVAR_FLOAT3:
		{
			Vec3V vec3(m_vector.GetXf(), m_vector.GetYf(), m_vector.GetZf());
			pShader->SetVar(m_id, vec3); 
			break;
		};
	case PTXSHADERVAR_FLOAT4:
		{
			pShader->SetVar(m_id, m_vector); 
			break;
		};
	default:
		{
			break;
		}
	}
};

#if RMPTFX_EDITOR
void ptxShaderVarVector::SendToEditor(ptxByteBuff& buff, const ptxParticleRule* UNUSED_PARAM(pParticleRule))
{
	buff.Write_const_char(ptxShaderVarUtils::GetVarTypeName(m_type));
	buff.Write_float(m_vector.GetXf());
	buff.Write_float(m_vector.GetYf());
	buff.Write_float(m_vector.GetZf());
	buff.Write_float(m_vector.GetWf());
}

void ptxShaderVarVector::ReceiveFromEditor(const ptxByteBuff& buff)
{
#if __ASSERT
	const char* pTypeName =
#endif
	buff.Read_const_char();
	ptxAssert(stricmp(pTypeName, ptxShaderVarUtils::GetVarTypeName(m_type))==0);

	m_vector.SetXf(buff.Read_float());
	m_vector.SetYf(buff.Read_float());
	m_vector.SetZf(buff.Read_float());
	m_vector.SetWf(buff.Read_float());
}
#endif






ptxShaderVarKeyframe::ptxShaderVarKeyframe() : ptxShaderVar()
{
	m_type = PTXSHADERVAR_KEYFRAME;
	m_isKeyframeable = true;
	m_keyframe.Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
}

ptxShaderVarKeyframe::~ptxShaderVarKeyframe() 
{
#if RMPTFX_EDITOR
	if (m_ownsInfo) 
	{
		m_keyframe.DeleteDefn();
	}
#endif
};

ptxShaderVarKeyframe::ptxShaderVarKeyframe(datResource& rsc)
: ptxShaderVar(rsc)
, m_keyframe(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxShaderVarKeyframe_num++;
#endif
}

#if __DECLARESTRUCT
void ptxShaderVarKeyframe::DeclareStruct(datTypeStruct& s)
{
	ptxShaderVar::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxShaderVarKeyframe, ptxShaderVar)
		SSTRUCT_FIELD(ptxShaderVarKeyframe, m_keyframe)
	SSTRUCT_END(ptxShaderVarKeyframe)
}
#endif

IMPLEMENT_PLACE(ptxShaderVarKeyframe);

ptxShaderVar* ptxShaderVarKeyframe::Clone()
{
	ptxShaderVarKeyframe* pShaderVarKF = rage_new ptxShaderVarKeyframe();
	pShaderVarKF->m_hashName = m_hashName;
	pShaderVarKF->m_id = m_id;

	pShaderVarKF->m_keyframe.CopyEntriesFrom(m_keyframe);
	pShaderVarKF->m_keyframe.CopyDefnFrom(m_keyframe);

	pShaderVarKF->m_ownsInfo = false;

	pShaderVarKF->m_pInfo = m_pInfo;

	return pShaderVarKF;
}

void ptxShaderVarKeyframe::SetDefaultData(grmShader* pShader)
{
	Vec4V vDefaultValue(V_ZERO);
	ptxKeyframeType type = PTXKEYFRAMETYPE_FLOAT;
	int channels = 0;

	switch(m_pInfo->m_Type)
	{
	case grcEffect::VT_FLOAT:
#if __D3D11 || RSG_ORBIS
	case grcEffect::VT_UNUSED1:
#endif // __D3D11 || RSG_ORBIS
		{
			float val;
			pShader->GetVar(m_id, val);
			vDefaultValue = Vec4V(val, 0.0f, 0.0f, 0.0f);
			type = PTXKEYFRAMETYPE_FLOAT;
			channels = 1;
		}
		break;
	case grcEffect::VT_VECTOR2:
#if __D3D11 || RSG_ORBIS
	case grcEffect::VT_UNUSED2:
#endif // __D3D11 || RSG_ORBIS
		{
			Vector2 vec2;
			pShader->GetVar(m_id, vec2);
			vDefaultValue = Vec4V(vec2.x, vec2.y, 0.0f, 0.0f);
			type = PTXKEYFRAMETYPE_FLOAT2;
			channels = 2;
		}
		break;
	case grcEffect::VT_VECTOR3:
#if __D3D11 || RSG_ORBIS
	case grcEffect::VT_UNUSED3:
#endif // __D3D11 || RSG_ORBIS
		{
			Vector3 vec3;
			pShader->GetVar(m_id, vec3);
			vDefaultValue = Vec4V(vec3.x, vec3.y, vec3.z, 0.0f);
			type = PTXKEYFRAMETYPE_FLOAT3;
			channels = 3;
		}
		break;
	case grcEffect::VT_VECTOR4:
#if __D3D11 || RSG_ORBIS	
	case grcEffect::VT_UNUSED4:
#endif // __D3D11 || RSG_ORBIS
		{
			pShader->GetVar(m_id, RC_VECTOR4(vDefaultValue));
			type = PTXKEYFRAMETYPE_FLOAT4;
			channels = 4;
		}
		break;
	default:
		ptxAssertf(0, "invalid type for keyframe object");
	}

	m_keyframe.Init(ScalarV(V_ZERO), vDefaultValue);

	ptxKeyframeDefn* pKeyframeDefn = rage_new ptxKeyframeDefn
	(
		m_pInfo->m_Name,
		0,
		type, 
		vDefaultValue,
		Vec3V(0.0f, 1.0f, 0.01f), 
		Vec3V(m_pInfo->m_UiMin, m_pInfo->m_UiMax, m_pInfo->m_UiStep), 
		channels>=1 ? "X" : NULL, 
		channels>=2 ? "Y" : NULL, 
		channels>=3 ? "Z" : NULL,
		channels>=4 ? "W" : NULL,
		false
	);

	m_keyframe.SetDefn(pKeyframeDefn);
}

void ptxShaderVarKeyframe::CopyUiDataFromTemplate(ptxShaderVar* pShaderVar)
{
	ptxShaderVar::CopyUiDataFromTemplate(pShaderVar);
	ptxShaderVarKeyframe* pShaderVarKF = SafeCast(ptxShaderVarKeyframe, pShaderVar);
	if (pShaderVarKF && !sysMemAllocator::GetCurrent().IsBuildingResource())
	{
		m_keyframe.SetDefn(pShaderVarKF->m_keyframe.GetDefn());
	}
}

#if RMPTFX_EDITOR
void ptxShaderVarKeyframe::SendToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	buff.Write_const_char(ptxShaderVarUtils::GetVarTypeName(m_type));

	buff.Write_const_char(pParticleRule->GetName());
	buff.Write_s32(PTXRULETYPE_PARTICLE);
	buff.Write_u32(m_hashName);
}
#endif

#if RMPTFX_EDITOR
void ptxShaderVarKeyframe::ReceiveFromEditor(const ptxByteBuff& buff)
{
#if __ASSERT
	const char* pTypeName = 
#endif
		buff.Read_const_char();

	ptxAssert(stricmp(pTypeName, ptxShaderVarUtils::GetVarTypeName(m_type))==0);
	
	buff.Read_const_char();
	buff.Read_s32();
	buff.Read_u32();
}
#endif

ptxShaderVarTexture::ptxShaderVarTexture(datResource& rsc) : ptxShaderVar(rsc), 
m_textureName(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxShaderVarTexture_num++;
#endif
}

void ptxShaderVarTexture::PostLoad()
{
	m_textureHashName = atHashValue(m_textureName);
	m_externalReference = false;

	// need to do this when loading an excluded texture
	if (m_pTexture==NULL)
	{
		if(m_textureHashName != 0)
		{
			// First check any fxlists marked as shareable
			ptxFxListIterator iter = RMPTFXMGR.CreateFxListIterator();
			for(iter.Start(FXLISTFLAG_SHAREABLE); !iter.AtEnd(); iter.Next())
			{
				m_pTexture = iter.GetCurrFxList()->GetTextureDictionary()->LookupLocal(m_textureHashName);
				if (m_pTexture)
				{
					m_pTexture->AddRef();
					m_externalReference = true;
					return;
				}
			}

			// Now check the global texture dictionaries
			if (pgDictionary<grcTexture>* pTexDict = pgDictionary<grcTexture>::GetCurrent()) 
			{
				u32 hashName = pTexDict->ComputeHash(m_textureName);
				m_pTexture = pTexDict->Lookup(hashName);
				if (m_pTexture) 
				{
					m_pTexture->AddRef();
					m_externalReference = true;
				}
			}
		}
		else
		{
			SetTexture(NULL);
		}
	}
}

ptxShaderVarTexture::ptxShaderVarTexture() : m_pTexture(NULL), m_externalReference(false)
{
	m_type = PTXSHADERVAR_TEXTURE;
}

ptxShaderVarTexture::~ptxShaderVarTexture()
{
	if (m_externalReference && m_pTexture)
	{
		m_pTexture->Release();
	}
}

#if __DECLARESTRUCT
void ptxShaderVarTexture::DeclareStruct(datTypeStruct& s)
{
	// fix up texture references too
	if (m_pTexture && m_pTexture->GetResourceType()==grcTexture::REFERENCE)
	{
		datTypeStruct ts;
		m_pTexture->DeclareStruct(ts);
	}

	ptxShaderVar::DeclareStruct(s);
	
	SSTRUCT_BEGIN_BASE(ptxShaderVarTexture, ptxShaderVar)
		SSTRUCT_FIELD(ptxShaderVarTexture, m_pTexture)
		SSTRUCT_FIELD(ptxShaderVarTexture, m_textureName)
		SSTRUCT_FIELD(ptxShaderVarTexture, m_textureHashName)
		SSTRUCT_FIELD(ptxShaderVarTexture, m_externalReference)
		SSTRUCT_IGNORE(ptxShaderVarTexture, m_pad)
	SSTRUCT_END(ptxShaderVarTexture)
}
#endif

IMPLEMENT_PLACE(ptxShaderVarTexture);

void ptxShaderVarTexture::OnPreLoad(parTreeNode* pNode)
{
	sysMemAutoUseTempMemory mem;

	parTreeNode* pTexNameNode = pNode->FindChildWithName(PTXSTR_TREENODE_TEXTURENAME);
	if (pTexNameNode)
	{
		const char* pOldTexName = pTexNameNode->ReadStdLeafString();
		if (pOldTexName)
		{
			char texName[128];
			texName[0] = '\0';
			safecpy(texName, ASSET.FileName(pOldTexName), sizeof(texName));
			if (strstr(texName, PTXSTR_TEXTURE_EXT))
			{
				texName[strlen(texName)-PTX_TEXTURE_EXT_LENGTH] = '\0';
			}

			pTexNameNode->SetData(texName, (u32)strlen(texName)+1);
		}
	}
}

void ptxShaderVarTexture::OnPostLoad()
{
	char texName[128];
	texName[0] = '\0';

	if (m_textureName)
	{
		safecpy(texName, ASSET.FileName(m_textureName), sizeof(texName));
	}

	if (strstr(texName, PTXSTR_TEXTURE_EXT))
	{
		texName[strlen(texName)-PTX_TEXTURE_EXT_LENGTH] = '\0';
	}

	if (m_textureName != texName)
	{
		m_textureName = texName;
	}

	m_textureHashName = atHashValue(m_textureName);
}

void ptxShaderVarTexture::SetData()
{
	SetTexture(m_textureName);
}

ptxShaderVar* ptxShaderVarTexture::Clone()
{
	ptxShaderVarTexture* pShaderVarTex = rage_new ptxShaderVarTexture();
	pShaderVarTex->m_hashName = m_hashName;
	pShaderVarTex->m_id = m_id;
	pShaderVarTex->m_textureName = m_textureName;
	pShaderVarTex->m_textureHashName = m_textureHashName;
	pShaderVarTex->m_pTexture = m_pTexture.ptr;
	pShaderVarTex->m_externalReference = m_externalReference;
	if (m_externalReference && m_pTexture)
	{
		pShaderVarTex->m_pTexture->AddRef();
	}
	pShaderVarTex->m_ownsInfo = false;
	pShaderVarTex->m_pInfo = m_pInfo;

	return pShaderVarTex;
}

void ptxShaderVarTexture::SetTexture(grcTexture* pTexture, bool isExternal)
{
	if (pTexture==NULL)
	{
		return;
	}

#if RMPTFX_EDITOR
	// If we're using the particle editor and an unresourced texture is reloaded,
	// ptxShaderInst::ReplaceTexture would have already patched the texture pointers
	// in all the affected particle rules, as well as having adjusted the reference counting.
	// In such case, the editor will send a "set texture" message *after* the texture replacement has taken place
	// (i.e. after the ptxShaderInst::ReplaceTexture call), and if we allow the texture to be released again, 
	// the ref counting will go wrong, resulting in this function trying to release already deleted memory.
	// The simplest way to detect this case is to compare the current texture (m_pTexture) with the incoming one.
	bool bTextureAlreadyReplaced = (pTexture == m_pTexture);
#endif

	// Release the old texture, if it was externally referenced
	// Note that the new texture gets referenced (or not) by the calling function.
	if (RMPTFX_EDITOR_ONLY(bTextureAlreadyReplaced == false &&) m_pTexture && m_externalReference) 
	{
		m_pTexture->Release();
	}

	m_pTexture = pTexture;

	char texName[128];
	formatf(texName, sizeof(texName), "%s", m_pTexture->GetName());
	if (strstr(texName, PTXSTR_TEXTURE_PACK_PATH))
	{
		formatf(texName, sizeof(texName), "%s", m_pTexture->GetName()+6);
	}
	if (strstr(texName, PTXSTR_TEXTURE_EXT))
	{
		texName[strlen(texName)-PTX_TEXTURE_EXT_LENGTH] = '\0';
	}
	if (m_pTexture==grcTexture::None || m_pTexture==grcTexture::NoneBlackTransparent)
	{
		texName[0] = '\0';
		m_externalReference = false; // Don't consider an empty texture to be externally referenced (so we don't try to unref it)
	}
	else
	{
		m_externalReference = isExternal;
	}

	if (m_textureName != texName)
	{
		m_textureName = texName;
		m_textureHashName = atHashValue(m_textureName);
	}

	// need this code when resourcing an excluded texture
	if (sysMemAllocator::GetCurrent().IsBuildingResource())
	{
		if (m_pTexture->GetResourceType()==grcTexture::REFERENCE && (grcTextureReference*)m_pTexture->GetReference()==NULL)
		{
			sysMemStartTemp();
			m_pTexture->Release();
			sysMemEndTemp();
			m_pTexture = NULL;
		}
	}
}

void ptxShaderVarTexture::SetTexture(const char* pName, const char* pPath)
{
	if (sysMemAllocator::GetCurrent().IsBuildingResource())
	{
		bool isExternal = false;
		grcTexture* tex = RMPTFXMGR.LoadTexture(pName, true, true, &isExternal);
		SetTexture(tex, isExternal);
		return;
	}

	if (pPath==NULL || (strlen(pPath)==0))
	{
		SetTexture(pName);
		return;
	}

	char oldPath[512];
	formatf(oldPath, sizeof(oldPath), "%s", RMPTFXMGR.GetAssetPath());
	RMPTFXMGR.SetAssetPath(pPath);
	bool isExternal = false;
	grcTexture* tex = RMPTFXMGR.LoadTexture(pName, true, true, &isExternal);
	SetTexture(tex, isExternal);
	RMPTFXMGR.SetAssetPath(oldPath);
}

void ptxShaderVarTexture::SetTexture(const char* pName)
{
	bool isExternal = false;
	grcTexture* tex = RMPTFXMGR.LoadTexture(pName, false, false, &isExternal);
	SetTexture(tex, isExternal);
}

#if RMPTFX_EDITOR
void ptxShaderVarTexture::SendToEditor(ptxByteBuff& buff, const ptxParticleRule* UNUSED_PARAM(pParticleRule))
{
	buff.Write_const_char(ptxShaderVarUtils::GetVarTypeName(m_type));
	buff.Write_const_char(m_textureName);
}

void ptxShaderVarTexture::ReceiveFromEditor(const ptxByteBuff& buff)
{
#if __ASSERT
	const char* pTypeName =
#endif
	buff.Read_const_char();
	ptxAssert(stricmp(pTypeName, ptxShaderVarUtils::GetVarTypeName(m_type))==0);

	const char* pPath = buff.Read_const_char();
	const char* pName = buff.Read_const_char();

	if (m_textureHashName!=atHashValue(pName) || m_pTexture==NULL)
	{
		SetTexture(pName, pPath);
	}
}
#endif // RMPTFX_EDITOR
