// 
// rmptfx/ptxtimeline.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
// 

#ifndef RMPTFX_PTXTIMELINE_H
#define RMPTFX_PTXTIMELINE_H


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxevent.h"

// includes (rage)
#include "atl/array.h"
#include "parser/macros.h"


// namespaces
namespace rage 
{


// defines
#define PTXTIMELINE_MAX_CHANNELS	(32)


// forward declarations
class ptxEvent;


// classes
class ptxTimeLine
{	
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxTimeLine();
		virtual ~ptxTimeLine();

		// resourcing
		ptxTimeLine(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxTimeLine);

		// accessors
		s32 GetNumEvents() {return m_events.GetCount();}
		ptxEvent* GetEvent(s32 idx) {return m_events[idx].ptr;}
		void ResizeEvents(s32 newSize) {m_events.Resize(newSize);}

		// xml loading / saving
#if RMPTFX_XML_LOADING
		void RepairAndUpgradeData(const char* pFilename);
		void ResolveReferences();
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
		void Save();
#endif

		// editor
#if RMPTFX_EDITOR
		void ReorderEvents();
		void CreateUiObjects();

		void SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule);
		void ReceiveFromEditor(ptxByteBuff& buff);

		void StartProfileUpdate();
		void StartProfileDraw();

		void AddEmitterEvent(ptxEffectRule* pEffectRule);
		void DeleteEvent(s32 eventIndex);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE
#endif


	private: //////////////////////////

		static int EventSorter(const void* pA, const void* pB);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		atArray<datOwner<ptxEvent> > m_events;


};


} // namespace rage


#endif // RMPTFX_PTXTIMELINE_H
