<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxSpawnedEffectScalars" cond="RMPTFX_XML_LOADING">
	<float name="m_durationScalar"/>
	<float name="m_playbackRateScalar"/>
	<Color32 name="m_colourTintScalar"/>
	<float name="m_zoomScalar"/>
	<u32 name="m_flags"/>
</structdef>

<structdef type="rage::ptxEffectSpawner" cond="RMPTFX_XML_LOADING">
	<string name="m_name" type="ConstString"/>
	<float name="m_triggerInfo"/>
	<bool name="m_inheritsPointLife"/>
	<bool name="m_tracksPointPos"/>
	<bool name="m_tracksPointDir"/>
	<bool name="m_tracksPointNegDir"/>
	<struct name="m_spawnedEffectScalarsMin" type="rage::ptxSpawnedEffectScalars"/>
	<struct name="m_spawnedEffectScalarsMax" type="rage::ptxSpawnedEffectScalars"/>
</structdef>

</ParserSchema>