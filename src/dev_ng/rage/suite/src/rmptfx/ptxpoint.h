// 
// rmptfx/ptxpoint.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
// 

#ifndef RMPTFX_PTDPOINT_H
#define RMPTFX_PTDPOINT_H


// includes (rmptfx)
#include "rmptfx/ptxevolution.h"
#include "rmptfx/ptxlist.h"
#include "rmptfx/ptxmisc.h"

// includes (rage)
#include "atl/bitset.h"
#include "vectormath/vectormath.h"


// namespaces
namespace rage 
{


// forward declarations
class ptxPointDB;
class ptxPointSB;
class ptxEffectInst;


// enumerations
enum ptxPointFlag
{
	PTXPOINT_FLAG_IS_SPRITE,													// is this particle a sprite
	PTXPOINT_FLAG_IS_MODEL,														// is this particle a model
	PTXPOINT_FLAG_IS_TRAIL,														// is this particle a trail
	PTXPOINT_FLAG_COLLIDES,														// should the particle collide
	PTXPOINT_FLAG_HAS_COLLIDED,													// has the particle collided
	PTXPOINT_FLAG_AT_REST,														// has a particle that can collide come to rest
	PTXPOINT_FLAG_SPAWN_EFFECT,													// should we spawn an effect this frame (due to particle life passing the spawner's trigger ratio)
	PTXPOINT_FLAG_LOOPED,														// should the particle loop at the end of its life
	PTXPOINT_FLAG_IS_BEING_INITIALISED,											// is the particle being initialised (i.e. on its first frame)
	PTXPOINT_FLAG_LIFE_PAUSED,
	PTXPOINT_FLAG_LIFE_UNPAUSED,

	PTXPOINT_NUM_FLAGS
};


// typedefs
typedef ptxPool<ptxPointSB, ptxPointDB>					ptxPointPool;
typedef ptxPool<ptxPointSB, ptxPointDB>::ptxListItem	ptxPointItem; 

typedef atFixedBitSet<PTXPOINT_NUM_FLAGS>				ptxPointFlags;



// classes
// particle point data that doesn't need double buffered
// the data either doesn't change after initialisation or it is only used in the update thread exclusively - we should not be altering this data on the render thread
class ptxPointSB
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		inline void Init();

		// accessors
		inline QuatV_Out GetInitRot() const {return m_vInitRot;}
		inline void SetInitRot(QuatV_In val) {m_vInitRot = val;}
		inline void SetInitRotX(float val) {m_vInitRot.SetXf(val);}
		inline void SetInitRotX(ScalarV_In val) {m_vInitRot.SetX(val);}
		inline Vec3V_Out GetColnPos() const {return m_vColnPos_colnSpeed.GetXYZ();}
		inline void SetColnPos(Vec3V_In val) {m_vColnPos_colnSpeed.SetXYZ(val);}
		inline float GetColnSpeed() const {return m_vColnPos_colnSpeed.GetWf();}
		inline void SetColnSpeed(float val) {m_vColnPos_colnSpeed.SetWf(val);}
		inline Vec3V_Out GetColnNormal() const {return m_vColnNormal_pauseTimer.GetXYZ();}
		inline void SetColnNormal(Vec3V_In val) {m_vColnNormal_pauseTimer.SetXYZ(val);}
		inline float GetPauseTimer() const {return m_vColnNormal_pauseTimer.GetWf();}
		inline void SetPauseTimer(float val) {m_vColnNormal_pauseTimer.SetWf(val);}
		inline ptxEvoValueType* GetEvoValues() {return &m_evoValues[0];}
		inline float GetEvoValue(int idx) const {return UnPackEvoValue(m_evoValues[idx]);}
		inline void SetEvoValue(int idx, float val) {m_evoValues[idx] = PackEvoValue(val);}
		inline void SetEvoValueRaw(int idx, ptxEvoValueType val) {m_evoValues[idx] = val;}
		inline float GetOOLife() const {return m_ooLife;}
		inline void SetOOLife(float val) {m_ooLife = val;}
		inline float GetEmitterLifeRatio() const {return m_emitterLifeRatio;}
		inline void SetEmitterLifeRatio(float val) {m_emitterLifeRatio = val;}
		inline float GetPlaybackRate() const {return m_playbackRate;}
		inline void SetPlaybackRate(float val) {m_playbackRate = val;}
		inline float GetSortVal() const {return m_sortVal;}
		inline void SetSortVal(float val) {m_sortVal = val;}
		inline void ResetFlags() {m_flags.Reset();}
		inline void SetFlag(ptxPointFlag flag) {m_flags.Set(flag);}
		inline void SetFlag(ptxPointFlag flag, bool val) {m_flags.Set(flag, val);}
		inline bool GetFlag(ptxPointFlag flag) {return m_flags.IsSet(flag);}
		inline void ClearFlag(ptxPointFlag flag) {m_flags.Clear(flag);}
		inline u8 GetColnMtlId() const {return m_colnMtlId;}
		inline void SetColnMtlId(u8 val) {m_colnMtlId = val;}
		inline u8 GetRandIndex() const {return m_randIndex;}
		inline void SetRandIndex(u8 val) {m_randIndex = val;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		QuatV m_vInitRot;														// initial rotation of the particle
		Vec4V m_vColnPos_colnSpeed;												// the particle's last collision position and collision speed
		Vec4V m_vColnNormal_pauseTimer;											// the particle's last collision normal 
#if !RMPTFX_LOW_PRECISION_EVO_VALUES
		ptxEvoValueType m_evoValues[PTXEVO_MAX_EVOLUTIONS];						// the effect's evolution values when this particle was spawned
#endif
		float m_ooLife;															// the reciprocal of the particle life
		float m_emitterLifeRatio;												// the emitter's life ratio when this particle was spawned
		float m_playbackRate;													// the playback rate of the particle when it was spawned
		float m_sortVal;														// value used for sorting particles (either a static random value or the distance to the camera)
		ptxPointFlags m_flags;													// particle point flags (e.g. collides, looped, spawns etc)
#if RMPTFX_LOW_PRECISION_EVO_VALUES
		ptxEvoValueType m_evoValues[PTXEVO_MAX_EVOLUTIONS];						// the effect's evolution values when this particle was spawned
#endif
		u8 m_colnMtlId;															// the material id of the collision poly that the particle collided with
		u8 m_randIndex;															// this particle's index into the random number table
};

// particle point data that needs double buffered as it can change and needs accessed in the update and render threads
class ptxPointDB
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		inline void Init();
		inline ScalarV_Out GetKeyTime() const {return Clamp(GetLifeRatioV(), ScalarV(V_ZERO), ScalarV(V_ONE));}

		// accessors
		inline Vec3V_Out GetPrevPos() const {return m_vPrevPos_emissiveIntensity.GetXYZ();}
		inline void SetPrevPos(Vec3V_In val) {ptxAssertf(IsFiniteAll(val), "trying to set a non finite position on a point"); m_vPrevPos_emissiveIntensity.SetXYZ(val);}
		inline void IncPrevPos(Vec3V_In val) {m_vPrevPos_emissiveIntensity.SetXYZ(m_vPrevPos_emissiveIntensity.GetXYZ()+val);}
		inline Vec3V_Out GetCurrPos() const {return m_vCurrPos_lifeRatio.GetXYZ();}
		inline void SetCurrPos(Vec3V_In val) {ptxAssertf(IsFiniteAll(val), "trying to set a non finite position on a point"); m_vCurrPos_lifeRatio.SetXYZ(val);}
		inline void IncCurrPos(Vec3V_In val) {m_vCurrPos_lifeRatio.SetXYZ(m_vCurrPos_lifeRatio.GetXYZ()+val);}
		inline Vec3V_Out GetSpawnPos() const {return m_vSpawnPos_mtxWeight.GetXYZ();}
		inline void SetSpawnPos(Vec3V_In val) {m_vSpawnPos_mtxWeight.SetXYZ(val);}
		inline void IncSpawnPos(Vec3V_In val) {m_vSpawnPos_mtxWeight.SetXYZ(m_vSpawnPos_mtxWeight.GetXYZ()+val);}
		inline Vec3V_Out GetCurrVel() const {return m_vCurrVel_texFrameRatio.GetXYZ();}
		inline void SetCurrVel(Vec3V_In val) {ptxAssertf(IsFiniteAll(val), "trying to set a non finite velocity on a point"); m_vCurrVel_texFrameRatio.SetXYZ(val);}
		inline void IncCurrVel(Vec3V_In val) {ptxAssertf(IsFiniteAll(val), "trying to increment a non finite velocity on a point"); m_vCurrVel_texFrameRatio.SetXYZ(m_vCurrVel_texFrameRatio.GetXYZ()+val);}
		inline void ScaleCurrVel(Vec3V_In val) {ptxAssertf(IsFiniteAll(val), "trying to scale a non finite velocity on a point"); m_vCurrVel_texFrameRatio.SetXYZ(m_vCurrVel_texFrameRatio.GetXYZ()*val);}
		inline Vec4V_Out GetDimensions() const {return m_vDimensions;}
		inline void SetDimensions(Vec4V_In val) {m_vDimensions = val;}
		inline QuatV_Out GetCurrRot() const {return m_vCurrRot;}
		inline void SetCurrRot(QuatV_In val) {m_vCurrRot = val;}
		inline void SetCurrRotX(float val) {m_vCurrRot.SetXf(val);}
		inline void SetCurrRotX(ScalarV_In val) {m_vCurrRot.SetX(val);}
		inline void IncCurrRotX(float val) {m_vCurrRot.SetXf(m_vCurrRot.GetXf()+val);}
		inline void IncCurrRotX(ScalarV_In val) {m_vCurrRot.SetX(m_vCurrRot.GetX()+val);}
		inline Vec3V_Out GetCurrAngle() const {return m_vCurrAngle;}
		inline void SetCurrAngle(Vec3V_In val) {m_vCurrAngle = val;}
		inline void IncCurrAngle(Vec3V_In val) {m_vCurrAngle += val;}
		inline Color32 GetColour() const {return m_col;}
		inline void SetColour(const Color32 &val) {m_col = val;}
		inline void SetColour(Vec4V_In val) {m_col.SetFromRGBA(val);}
		inline float GetEmissiveIntensity() const {return m_vPrevPos_emissiveIntensity.GetWf();}
		inline void SetEmissiveIntensity(float val) {m_vPrevPos_emissiveIntensity.SetWf(val);}
		inline void SetEmissiveIntensityV(ScalarV_In val) {m_vPrevPos_emissiveIntensity.SetW(val);}
		inline float GetLifeRatio() const {return m_vCurrPos_lifeRatio.GetWf();}
		inline ScalarV_Out GetLifeRatioV() const {return m_vCurrPos_lifeRatio.GetW();}
		inline void SetLifeRatio(float val) {m_vCurrPos_lifeRatio.SetWf(val);}
		inline void SetLifeRatio(ScalarV_In val) {m_vCurrPos_lifeRatio.SetW(val);}
		inline ScalarV_Out GetMatrixWeightV() const {return m_vSpawnPos_mtxWeight.GetW();}
		inline float GetMatrixWeight() const {return m_vSpawnPos_mtxWeight.GetWf();}
		inline void SetMatrixWeight(ScalarV_In val) {m_vSpawnPos_mtxWeight.SetW(val);}
		inline void SetMatrixWeight(float val) {m_vSpawnPos_mtxWeight.SetWf(val);}
		inline ScalarV_Out GetTexFrameRatioV() const {return m_vCurrVel_texFrameRatio.GetW();}
		inline float GetTexFrameRatio() const {return m_vCurrVel_texFrameRatio.GetWf();}
		inline void SetTexFrameRatio(ScalarV_In val) {m_vCurrVel_texFrameRatio.SetW(val);}
		inline void SetTexFrameRatio(float val) {m_vCurrVel_texFrameRatio.SetWf(val);}
		inline u8 GetTexFrameIdA() const {return m_texFrameIdA;}
		inline void SetTexFrameIdA(u8 val) {m_texFrameIdA = val;}
		inline u8 GetTexFrameIdB() const {return m_texFrameIdB;}
		inline void SetTexFrameIdB(u8 val) {m_texFrameIdB = val;}
		inline void IncTexFrameIdB(u8 val) {m_texFrameIdB += val;}
		inline u8 GetInitTexFrameId() const {return m_initTexFrameId;}
		inline void SetInitTexFrameId(u8 val) {m_initTexFrameId = val;}
		inline u8 GetAlphaMult() const {return m_alphaMult;}
		__forceinline ScalarV_Out GetAlphaMultV() const
		{
			//TODO: Optimize for Durango/Orbis
			return ScalarV((float)m_alphaMult/255.0f);
		}
 
		inline void SetAlphaMult(u8 val) {m_alphaMult = val;}
		inline bool GetIsDead() const {return m_isDead;}
		inline void SetIsDead(bool val) {m_isDead = val;}
		inline bool GetIsBrokenTrail() const {return m_isBrokenTrail;}
		inline void SetIsBrokenTrail(bool val) {m_isBrokenTrail = val;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		Vec4V m_vPrevPos_emissiveIntensity;										// the previous position of the particle
																				// the current emissive intensity of the particle
		Vec4V m_vCurrPos_lifeRatio;												// the current position of the particle 
																				// how far through its life the particle is (0.0 = birth, 1.0 = death)
		Vec4V m_vSpawnPos_mtxWeight;											// position where this particle was spawned (can change due to matrix weight and relocation)
																				// the current matrix weight of the particle (used only by the wind behaviour)
		Vec4V m_vCurrVel_texFrameRatio;											// the current velocity of the particle
																				// the blend ratio from texture frame A to B
		Vec4V m_vDimensions;													// the dimensions of the particle (top, bottom, left, right for sprites - width, height, depth, radius for models) (used by the collision and rendering behaviours)
		QuatV m_vCurrRot;														// current rotation of the particle
		Vec3V m_vCurrAngle;														// used to accumulate angles that need applied with euler angles
		Color32 m_col;															// the current colour of the particle
		u8 m_texFrameIdA;														// the frame id of the texture we are blending from
		u8 m_texFrameIdB;														// the frame id of the texture we are blending to
		u8 m_initTexFrameId;													// the initial texture frame id of this particle 
		u8 m_alphaMult;															// multiplier for the alpha colour (used only by the zcull behaviour)
		bool m_isDead;															// whether or not the particle is dead (e.g. from age or collision)
		bool m_isBrokenTrail;
};


// inline functions
inline void ptxPointSB::Init()
{
	m_vInitRot = QuatV(V_ZERO);	
	m_vColnPos_colnSpeed = Vec4V(V_ZERO);
	m_vColnNormal_pauseTimer = Vec4V(V_ZERO);
	for (int i=0; i<PTXEVO_MAX_EVOLUTIONS; i++)
	{
		m_evoValues[i] = (ptxEvoValueType)0;
	}
	m_ooLife = 1.0f;		
	m_emitterLifeRatio = 0.0f;
	m_playbackRate = 1.0f;
	m_sortVal = 0.0f; 	
	ResetFlags();
	m_colnMtlId = 0;
	m_randIndex = 0;
}

inline void ptxPointDB::Init()
{
	m_vPrevPos_emissiveIntensity = Vec4V(V_ZERO);				
	m_vCurrPos_lifeRatio = Vec4V(V_ZERO);	
	m_vSpawnPos_mtxWeight = Vec4V(V_ZERO);				
	m_vCurrVel_texFrameRatio = Vec4V(V_ZERO);			
	m_vDimensions = Vec4V(V_ONE);	
	m_vCurrRot = QuatV(V_ZERO);
	m_vCurrAngle = Vec3V(V_ZERO);	
	m_col = Color32(1.0f, 1.0f, 1.0f, 1.0f);			
	m_texFrameIdA = 0;	
	m_texFrameIdB = 0;		
	m_initTexFrameId = 0;
	m_alphaMult = 255;		
	m_isDead = false;	
	m_isBrokenTrail = false;
}


} // namespace rage


#endif // RMPTFX_PTDPOINT_H
