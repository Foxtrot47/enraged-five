// 
// rmptfx/ptxinterface.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxinterface.h"

// includes (rmptfx
#include "rmptfx/ptxclipregion.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxdrawlist.h"
#include "rmptfx/ptxfxlist.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// editor only file
#if RMPTFX_EDITOR


// namespaces
using namespace rage;


// defines
#define PTXINTERFACE_DEFAULT_BUFFER_SIZE		(128*1024)


// code
ptxInterface::ptxInterface()
{
	m_byteBuff.Create(PTXINTERFACE_DEFAULT_BUFFER_SIZE);

	m_pCurrEffectRule = NULL;
	m_pCurrEmitterRule = NULL;

	m_needsReply = false;

	// global controls
	m_timeScalar = 1.0f;

	// debug effect controls
	m_shouldRepeatPlayback = false;
	m_repeatPlaybackTime = 5.0f;

	// debug effect spawn info
	m_spawnCamRelative = true;
	m_spawnCamDist = 10.0f;
	m_spawnWorldPos = Vec3V(V_ZERO);

	// debug effect anim info
	m_animEnabled = false;
	m_animSpeed = 1.0f;
	m_animRadius = 5.0f;
	m_animMode = 0;
}

ptxInterface::~ptxInterface()
{
	Disconnect();
}

void ptxInterface::ProcessByteBuffer(ptxByteBuff& buff)
{
	// skip the header
	buff.SetReadPos(4);

	// read the message id
	ptxMessageId messageId = (ptxMessageId)buff.Read_u32();
	
	// process the message
	switch (messageId)
	{
		case MSGID_NOTHING_TO_PROCESS:					break;
	
		// fx lists
		case MSGID_RECEIVE_LOADFXLIST:					ReceiveLoadFxList(buff); break;
		case MSGID_RECEIVE_SAVEFXLIST:					ReceiveSaveFxList(buff); break;

		// rule lists
		case MSGID_SEND_EFFECTRULELIST:					SendEffectRuleList(); break;
		case MSGID_SEND_EMITTERRULELIST:				SendEmitterRuleList(); break;
		case MSGID_SEND_PARTICLERULELIST:				SendParticleRuleList(); break;

		// effect rules
		case MSGID_SEND_EFFECTRULE:						SendEffectRule(buff.Read_const_char()); break;
		case MSGID_RECEIVE_EFFECTRULE:					ReceiveEffectRule(buff); break;
		case MSGID_RECEIVE_LOADEFFECTRULE:				ReceiveLoadEffectRule(buff); break;
		case MSGID_RECEIVE_LOADEFFECTRULES:    			ReceiveLoadEffectRules(buff); break;
		case MSGID_RECEIVE_CREATEEFFECTRULE:			ReceiveCreateEffectRule(buff); break;
		case MSGID_RECEIVE_REMOVEEFFECTRULE:			ReceiveRemoveEffectRule(buff); break;
		case MSGID_RECEIVE_RENAMEEFFECTRULE:			ReceiveRenameEffectRule(buff); break;

		// emitter rules
		case MSGID_SEND_EMITTERRULE:					SendEmitterRule(buff.Read_const_char()); break;
		case MSGID_RECEIVE_EMITTERRULE:					ReceiveEmitterRule(buff); break;
		case MSGID_RECEIVE_LOADEMITTERRULE:				ReceiveLoadEmitterRule(buff); break;
		case MSGID_RECEIVE_CREATEEMITTERRULE:			ReceiveCreateEmitterRule(buff); break;
		case MSGID_RECEIVE_REMOVEEMITTERRULE:			ReceiveRemoveEmitterRule(buff); break;
		case MSGID_RECEIVE_RENAMEEMITTERRULE:			ReceiveRenameEmitterRule(buff); break;

		// particle rules
		case MSGID_SEND_PARTICLERULE:					SendParticleRule(buff.Read_const_char()); break;
		case MSGID_RECEIVE_PARTICLERULE:				ReceiveParticleRule(buff); break;
		case MSGID_RECEIVE_LOADPARTICLERULE:			ReceiveLoadParticleRule(buff); break;
		case MSGID_RECEIVE_CREATEPARTICLERULE:			ReceiveCreateParticleRule(buff); break;
		case MSGID_RECEIVE_REMOVEPARTICLERULE:			ReceiveRemoveParticleRule(buff); break;
		case MSGID_RECEIVE_RENAMEPARTICLERULE:			ReceiveRenameParticleRule(buff); break;
		case MSGID_RECEIVE_CLONEPARTICLERULE:			ReceiveCloneParticleRule(buff); break;

		// timeline events
		case MSGID_RECEIVE_CREATETIMELINEEVENT: 		ReceiveCreateTimelineEvent(buff); break;
		case MSGID_RECEIVE_DELETETIMELINEEVENT:			ReceiveDeleteTimelineEvent(buff); break;
		case MSGID_RECEIVE_REORDERTIMELINEEVENTS:		ReceiveReorderTimelineEvents(buff); break;

		// domains
		case MSGID_SEND_DOMAINSHAPELIST:				SendDomainShapeList(); break;
		case MSGID_SEND_TOGGLEATTRACTORDOMAIN:			ReceiveToggleAttractorDomain(buff); break;

		// behaviours
		case MSGID_SEND_BEHAVIOURLIST:					SendBehaviourList(); break;
		case MSGID_RECEIVE_ADDBEHAVIOUR:				ReceiveAddBehaviour(buff); break;
		case MSGID_RECEIVE_DELETEBEHAVIOUR:				ReceiveDeleteBehaviour(buff); break;
		case MSGID_RECEIVE_VALIDATEBEHAVIOUR:			ReceiveValidateBehaviour(buff); break;

		// keyframes
		case MSGID_SEND_KEYFRAMEDEFN:					break;
		case MSGID_RECEIVE_KEYFRAMEREQUEST:				ReceiveKeyframeRequest(buff); break;
		case MSGID_RECEIVE_KEYFRAME:					ReceiveKeyframe(buff); break;
		case MSGID_RECEIVE_KEYFRAMEEXISTS:				ReceiveKeyframeExists(buff); break;
		
		// evolutions
		case MSGID_RECEIVE_CREATEEVOLUTION:				ReceiveCreateEvolution(buff); break;
		case MSGID_RECEIVE_DELETEEVOLUTION:				ReceiveDeleteEvolution(buff); break;
		case MSGID_RECEIVE_ADDEVOLVEDKEYFRAMEPROP:		ReceiveAddEvolvedKeyframeProp(buff); break;
		case MSGID_RECEIVE_DELETEEVOLVEDKEYFRAMEPROP:	ReceiveDeleteEvolvedKeyframeProp(buff); break;
		case MSGID_RECEIVE_LOADEVOLUTION:				ReceiveLoadEvolution(buff); break;
		case MSGID_RECEIVE_SAVEEVOLUTION:				ReceiveSaveEvolution(buff); break;

		// graphics
		case MSGID_SEND_CLIPREGIONDATA:					SendClipRegionData(); break;		
		case MSGID_SEND_BLENDSETLIST:					SendBlendSetList(); break;
		case MSGID_SEND_SHADERTEMPLATELIST:				SendShaderTemplateList(); break;
		case MSGID_RECEIVE_SETSHADERTEMPLATE:			ReceiveSetShaderTemplate(buff); break;

		// models
		case MSGID_RECEIVE_ADDDELETEMODEL:				ReceiveAddDeleteModel(buff); break;

		// saving
		case MSGID_RECEIVE_SAVE:						ReceiveSave(buff); break;
		case MSGID_RECEIVE_SAVEALL:						ReceiveSaveAll(buff); break;
		case MSGID_RECEIVE_CANCELSAVEALL:				ReceiveCancelSaveAll(buff); break;

		// misc
		case MSGID_SEND_VERSION:						SendVersion(); break;
		case MSGID_SEND_DRAWLISTS:						SendDrawLists(); break;
		case MSGID_SEND_GAMESPECIFICDATA:				SendGameSpecificData(); break;
		case MSGID_RECEIVE_RESCALEZOOMABLECOMPONENTS:	ReceiveRescaleZoomableComponents(buff); break;

		// profile
		case MSGID_SEND_GLOBALPROFILEDATA:				SendGlobalProfileData(); break;
		case MSGID_SEND_EFFECTRULEPROFILEDATA:			SendEffectRuleProfileData(buff); break;
		case MSGID_SEND_EMITTERRULEPROFILEDATA:			SendEmitterRuleProfileData(buff); break;
		case MSGID_SEND_PARTICLERULEPROFILEDATA:		SendParticleRuleProfileData(buff); break;

		// debug
		case MSGID_RECEIVE_PLAYCONTROLDATA:				ReceivePlayControlData(buff); break;
		case MSGID_RECEIVE_STARTDEBUGEFFECT:			ReceiveStartDebugEffect(buff); break;
		case MSGID_RECEIVE_SHOWDEBUGINFO:				ReceiveShowDebugInfo(buff); break;
		case MSGID_REQUEST_RESET:						ReceiveReset(buff); break;

		// default
		default:										ptxAssertf(0, "unsupported message id (%d)", messageId); break;
	}
}

void ptxInterface::Connected()
{
	ptxTcpComm::Connected();
	SendVersion();
}

void ptxInterface::SendBooleanReply(u32 replyId, bool val)
{
	if (IsConnected()==false)
	{
		return;
	}

	if (replyId != 0)
	{
		// begin buffering
		m_byteBuff.BeginTCPBuffer();
		m_byteBuff.Write_u32(replyId);

		// write the reply value
		m_byteBuff.Write_bool(val);
		
		// end buffering
		m_byteBuff.EndTCPBuffer();

		// send the buffer
		SendByteBuffer(&m_byteBuff);
	}
}

void ptxInterface::ReceiveLoadFxList(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in the path and the fx list name
	const char* pPath = buff.Read_const_char();
	const char* pName = buff.Read_const_char();

	// store the old path and set the new
	char oldPath[256];
	formatf(oldPath, sizeof(oldPath), "%s", RMPTFXMGR.GetAssetPath());
	RMPTFXMGR.SetAssetPath(pPath);

	// load in the fx list
	RMPTFXMGR.LoadFxList(pName, 0);

	// restore the old path
	RMPTFXMGR.SetAssetPath(oldPath);

	// send lists of all rules
	SendRulesLists();
}

void ptxInterface::ReceiveSaveFxList(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in the path and the fx list name
	const char* pPath = buff.Read_const_char();
	const char* pName = buff.Read_const_char();

	// store the old path and set the new
	char oldPath[256];
	formatf(oldPath, sizeof(oldPath), "%s", RMPTFXMGR.GetAssetPath());
	RMPTFXMGR.SetAssetPath(pPath);

	// save out the fx list
	RMPTFXMGR.SaveFxList(pName);

	// restore the old path
	RMPTFXMGR.SetAssetPath(oldPath);
}

void ptxInterface::SendRulesLists()
{
	if (IsConnected()==false)
	{
		return;
	}

	SendEffectRuleList();
	SendEmitterRuleList();
	SendParticleRuleList();
}

void ptxInterface::SendEffectRuleList()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_EFFECTRULELIST);

	// count the number of effect rules
	int numEffectRules = 0;
	ptxFxListIterator fxListIterator = RMPTFXMGR.CreateFxListIterator();
	for (fxListIterator.Start((u32)FXLISTFLAG_ALL); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		numEffectRules += fxListIterator.GetCurrFxList()->GetEffectRuleDictionary()->GetCount();
	}

	// write the number of effect rules
	m_byteBuff.Write_u32(numEffectRules);

	// go through the effect rules
#if __ASSERT
	int numEffectRulesWritten = 0;
#endif
	ptxEffectRuleIterator effectRuleIterator(RMPTFXMGR.CreateFxListIterator());
	for (effectRuleIterator.Start((u32)FXLISTFLAG_ALL); !effectRuleIterator.AtEnd(); effectRuleIterator.Next())
	{
		// buffer the data
		ptxEffectRule* pEffectRule = effectRuleIterator.GetCurrRule();
		if (pEffectRule)
		{
			m_byteBuff.Write_const_char(pEffectRule->GetName());
			m_byteBuff.Write_u32(atHashValue(pEffectRule->GetName()));

#if __ASSERT
			numEffectRulesWritten++;
#endif
		}
	}

	// check that we've written the correct number of effect rules
#if __ASSERT
	ptxAssertf(numEffectRules == numEffectRulesWritten, "num effect rules mismatch");
#endif

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendEmitterRuleList()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_EMITTERRULELIST);

	// count the number of emitter rules
	int numEmitterRules = 0;
	ptxFxListIterator fxListIterator = RMPTFXMGR.CreateFxListIterator();
	for (fxListIterator.Start((u32)FXLISTFLAG_ALL); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		numEmitterRules += fxListIterator.GetCurrFxList()->GetEmitterRuleDictionary()->GetCount();
	}

	// write the number of emitter rules
	m_byteBuff.Write_u32(numEmitterRules);

	// go through the emitter rules
#if __ASSERT
	int numEmitterRulesWritten = 0;
#endif
	ptxEmitterRuleIterator emitterRuleIterator(RMPTFXMGR.CreateFxListIterator());
	for (emitterRuleIterator.Start((u32)FXLISTFLAG_ALL); !emitterRuleIterator.AtEnd(); emitterRuleIterator.Next())
	{
		// buffer the data
		ptxEmitterRule* pEmitterRule = emitterRuleIterator.GetCurrRule();
		if (pEmitterRule)
		{
			m_byteBuff.Write_const_char(pEmitterRule->GetName());
			m_byteBuff.Write_u32(atHashValue(pEmitterRule->GetName()));

#if __ASSERT
			numEmitterRulesWritten++;
#endif
		}
	}

	// check that we've written the correct number of emitter rules
#if __ASSERT
	ptxAssertf(numEmitterRules == numEmitterRulesWritten, "num emitter rules mismatch");
#endif

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendParticleRuleList()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_PARTICLERULELIST);  

	// count the number of emitter rules
	int numParticleRules = 0;
	ptxFxListIterator fxListIterator = RMPTFXMGR.CreateFxListIterator();
	for (fxListIterator.Start((u32)FXLISTFLAG_ALL); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		numParticleRules += fxListIterator.GetCurrFxList()->GetParticleRuleDictionary()->GetCount();
	}

	// write the number of particle rules
	m_byteBuff.Write_u32(numParticleRules);

	// go through the emitter rules
#if __ASSERT
	int numParticleRulesWritten = 0;
#endif
	ptxParticleRuleIterator particleRuleIterator(RMPTFXMGR.CreateFxListIterator());
	for (particleRuleIterator.Start((u32)FXLISTFLAG_ALL); !particleRuleIterator.AtEnd(); particleRuleIterator.Next())
	{
		// buffer the data
		ptxParticleRule* pParticleRule = particleRuleIterator.GetCurrRule();
		if (pParticleRule)
		{
			m_byteBuff.Write_const_char(pParticleRule->GetName());
			m_byteBuff.Write_u32(atHashValue(pParticleRule->GetName()));

#if __ASSERT
			numParticleRulesWritten++;
#endif
		}
	}

	// check that we've written the correct number of particle rules
#if __ASSERT
	ptxAssertf(numParticleRules == numParticleRulesWritten, "num particle rules mismatch");
#endif

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendEffectRule(const char* pEffectRuleName)
{
	if (IsConnected()==false)
	{
		return;
	}

	if (pEffectRuleName)
	{
		ptxEffectRule* pEffectRule = RMPTFXMGR.GetEffectRule(atHashWithStringNotFinal(pEffectRuleName));
		if (pEffectRule)
		{
			// begin buffering
			m_byteBuff.BeginTCPBuffer();
			m_byteBuff.Write_u32(MSGID_SEND_EFFECTRULE);

			// buffer the data
			m_byteBuff.Write_const_char(pEffectRuleName);
			m_byteBuff.Write_u32(atHashValue(pEffectRuleName));
			pEffectRule->SendToEditor(m_byteBuff);

			// end buffering
			m_byteBuff.EndTCPBuffer();

			// send the buffer
			SendByteBuffer(&m_byteBuff);
		}
	}
}

void ptxInterface::ReceiveEffectRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");
	
	// read the effect rule name and hash name
	const char* pEffectRuleName = buff.Read_const_char();
	buff.Read_u32();
	
	// store the current read position
	int currReadPos = buff.GetCurrReadPos();

	ptxEffectRule* pEffectRules[32];
	int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
	ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
	for (int i=0; i<numEffectRules; i++)
	{
		// restore the current read position
		buff.SetCurrReadPos(currReadPos);

		// set the current effect rule
		m_pCurrEffectRule = pEffectRules[i];

		// receive the data
		pEffectRules[i]->ReceiveFromEditor(buff);

		// send the effect rule back if a reply is requested
		if (m_pCurrEffectRule && m_needsReply)
		{
			SendEffectRule(pEffectRuleName);
			m_needsReply = false;
		}

		// reset the current effect rule
		m_pCurrEffectRule = NULL;
	}
}

void ptxInterface::ReceiveLoadEffectRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in the path and effect rule name
	const char* pEffectRuleName = buff.Read_const_char();
	const char* pPath = buff.Read_const_char();

	// store the old path and set the new
	char oldPath[256];
	formatf(oldPath, sizeof(oldPath), "%s", RMPTFXMGR.GetAssetPath());
	RMPTFXMGR.SetAssetPath(pPath);

	// load in the effect rule
	RMPTFXMGR.LoadEffectRule(pEffectRuleName);

	// restore the old path
	RMPTFXMGR.SetAssetPath(oldPath);

	// send lists of all rules
	SendRulesLists();
}

void ptxInterface::ReceiveLoadEffectRules(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the path and count
	const char* pPath = buff.Read_const_char();
	int numEffectRules = buff.Read_s32();

	// store the old path and set the new
	char oldPath[256];
	formatf(oldPath, sizeof(oldPath), "%s", RMPTFXMGR.GetAssetPath());
	RMPTFXMGR.SetAssetPath(pPath);

	// load in the effect rules
	for (int i=0;i<numEffectRules;i++)
	{
		const char* pEffectRuleName = buff.Read_const_char();
		RMPTFXMGR.LoadEffectRule(pEffectRuleName);
	}

	// restore the old path
	RMPTFXMGR.SetAssetPath(oldPath);

	// send lists of all rules
	SendRulesLists();
}

void ptxInterface::ReceiveCreateEffectRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// check if a reply is required
	bool needsReply = buff.Read_bool();

	// create a new effect rule
	ptxEffectRule* pEffectRule = RMPTFXMGR.CreateNewEffectRule("");

	// reply if required
	if (pEffectRule && needsReply)
	{
		// send the new effect rule list
		SendEffectRuleList();

		// send the new effect rule
		SendEffectRule(pEffectRule->GetName());
	}
}

void ptxInterface::ReceiveRemoveEffectRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the reply id
	u32 replyId = buff.Read_u32();

	// read the effect rule to remove
	const char* pEffectRuleName = buff.Read_const_char();
	if (pEffectRuleName)
	{
		bool removedOk = false;

		// find the effect rules
		ptxEffectRule* pEffectRules[32];
		int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
		ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
		for (int i=0; i<numEffectRules; i++)
		{
			// try to remove the effect rule
			removedOk = RMPTFXMGR.RemoveEffectRule(pEffectRules[i], false);
		}

		// reply with removal status 
		SendBooleanReply(replyId, removedOk);
	}
}

void ptxInterface::ReceiveRenameEffectRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the current and new effect rule names
	const char* pCurrEffectRuleName = buff.Read_const_char();
	const char* pNewEffectRuleName = buff.Read_const_char();

	// rename the effect rule
	RMPTFXMGR.RenameEffectRule(pCurrEffectRuleName, pNewEffectRuleName);
}

void ptxInterface::SendEmitterRule(const char* pEmitterRuleName)
{
	if (IsConnected()==false)
	{
		return;
	}

	if (pEmitterRuleName)
	{
		ptxEmitterRule* pEmitterRule = RMPTFXMGR.GetEmitterRule(atHashWithStringNotFinal(pEmitterRuleName));
		if (pEmitterRule)
		{
			// begin buffering
			m_byteBuff.BeginTCPBuffer();
			m_byteBuff.Write_u32(MSGID_SEND_EMITTERRULE);

			// buffer the data
			m_byteBuff.Write_const_char(pEmitterRuleName);
			m_byteBuff.Write_u32(atHashValue(pEmitterRuleName));
			pEmitterRule->SendToEditor(m_byteBuff);

			// end buffering
			m_byteBuff.EndTCPBuffer();

			// send the buffer
			SendByteBuffer(&m_byteBuff);
		}
	}
}

void ptxInterface::ReceiveEmitterRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the emitter rule name and hash name
	const char* pEmitterRuleName = buff.Read_const_char();
	buff.Read_u32(); 

	// store the current read position
	int currReadPos = buff.GetCurrReadPos();

	// find the emitter rules
	ptxEmitterRule* pEmitterRules[32];
	int numEmitterRules = RMPTFXMGR.GetAllEmitterRules(atHashWithStringNotFinal(pEmitterRuleName), &pEmitterRules[0], 32);
	ptxAssertf(numEmitterRules>0, "emitter rule not found %s", pEmitterRuleName);
	for (int i=0; i<numEmitterRules; i++)
	{
		// restore the current read position
		buff.SetCurrReadPos(currReadPos);

		// set the current emitter rule
		m_pCurrEmitterRule = pEmitterRules[i];

		// receive the data
		pEmitterRules[i]->ReceiveFromEditor(buff);

		// send the emitter rule back if a reply is requested
		if (m_pCurrEmitterRule && m_needsReply)
		{
			SendEmitterRule(pEmitterRuleName);
			m_needsReply = false;
		}

		// reset the current emitter rule
		m_pCurrEmitterRule = NULL;
	}
}

void ptxInterface::ReceiveLoadEmitterRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in the path and emitter rule name
	const char* pEmitterRuleName = buff.Read_const_char();
	const char* pPath = buff.Read_const_char();

	// store the old path and set the new
	char oldPath[256];
	formatf(oldPath, sizeof(oldPath), "%s", RMPTFXMGR.GetAssetPath());
	RMPTFXMGR.SetAssetPath(pPath);

	// load in the emitter rule
	RMPTFXMGR.LoadEmitterRule(pEmitterRuleName);

	// restore the old path
	RMPTFXMGR.SetAssetPath(oldPath);

	// send lists of all rules
	SendRulesLists();
}

void ptxInterface::ReceiveCreateEmitterRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// check if a reply is required
	bool reply = buff.Read_bool();

	// create a new emitter rule
	ptxEmitterRule* pEmitterRule = RMPTFXMGR.CreateNewEmitterRule("");

	// reply if required
	if (pEmitterRule && reply)
	{
		// send the new emitter rule list
		SendEmitterRuleList();

		// send the new emitter rule
		SendEmitterRule(pEmitterRule->GetName());
	}
}

void ptxInterface::ReceiveRemoveEmitterRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the reply id
	u32 replyId = buff.Read_u32();

	// read the emitter rule to remove
	const char* pEmitterRuleName = buff.Read_const_char();
	if (pEmitterRuleName)
	{
		bool removedOk = false;

		// find the emitter rules
		ptxEmitterRule* pEmitterRules[32];
		int numEmitterRules = RMPTFXMGR.GetAllEmitterRules(atHashWithStringNotFinal(pEmitterRuleName), &pEmitterRules[0], 32);
		ptxAssertf(numEmitterRules>0, "emitter rule not found %s", pEmitterRuleName);
		for (int i=0; i<numEmitterRules; i++)
		{
			removedOk = RMPTFXMGR.RemoveEmitterRule(pEmitterRules[i], false);
		}

		// reply with removal status 
		SendBooleanReply(replyId, removedOk);
	}
}

void ptxInterface::ReceiveRenameEmitterRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the current and new effect rule names
	const char* pCurrEmitterRuleName = buff.Read_const_char();
	const char* pNewEmitterRuleName = buff.Read_const_char();

	// rename the effect rule
	RMPTFXMGR.RenameEmitterRule(pCurrEmitterRuleName, pNewEmitterRuleName);
}

void ptxInterface::SendParticleRule(const char* pParticleRuleName)
{
	if (IsConnected()==false)
	{
		return;
	}

	if (pParticleRuleName)
	{
		ptxParticleRule* pParticleRule = RMPTFXMGR.GetParticleRule(atHashWithStringNotFinal(pParticleRuleName));
		if (pParticleRule)
		{
			// begin buffering
			m_byteBuff.BeginTCPBuffer();
			m_byteBuff.Write_u32(MSGID_SEND_PARTICLERULE);

			// buffer the data
			m_byteBuff.Write_const_char(pParticleRuleName);
			pParticleRule->SendToEditor(m_byteBuff);

			// end buffering
			m_byteBuff.EndTCPBuffer();

			// send the buffer
			SendByteBuffer(&m_byteBuff);
		}
	}
}

void ptxInterface::ReceiveParticleRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the particle rule name
	const char* pParticleRuleName = buff.Read_const_char();

	// store the current read position
	int currReadPos = buff.GetCurrReadPos();

	ptxParticleRule* pParticleRules[32];
	int numParticleRules = RMPTFXMGR.GetAllParticleRules(atHashWithStringNotFinal(pParticleRuleName), &pParticleRules[0], 32);
	for (int i=0; i<numParticleRules; i++)
	{
		// restore the current read position
		buff.SetCurrReadPos(currReadPos);

		pParticleRules[i]->ReceiveFromEditor(buff);
	}
}

void ptxInterface::ReceiveLoadParticleRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in the path and effect rule name
	const char* pParticleRuleName = buff.Read_const_char();
	const char* pPath = buff.Read_const_char();

	// store the old path and set the new
	char oldPath[256];
	formatf(oldPath, sizeof(oldPath), "%s", RMPTFXMGR.GetAssetPath());
	RMPTFXMGR.SetAssetPath(pPath);

	// load in the particle rule
	RMPTFXMGR.LoadParticleRule(pParticleRuleName);

	// restore the old path
	RMPTFXMGR.SetAssetPath(oldPath);

	// send lists of all rules
	SendRulesLists();
}

void ptxInterface::ReceiveCreateParticleRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in the type of particle rule
	buff.Read_s32();

	// check if a reply is required
	bool needsReply = buff.Read_bool();

	// create a new particle rule
	ptxParticleRule* pParticleRule = RMPTFXMGR.CreateNewParticleRule("");

	// reply if required
	if (pParticleRule && needsReply)
	{
		// send the new particle rule list
		SendParticleRuleList();

		// send the new particle rule
		SendParticleRule(pParticleRule->GetName());
	}
}

void ptxInterface::ReceiveRemoveParticleRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the reply id
	u32 replyId = buff.Read_u32();

	// read the particle rule to remove
	const char* pParticleRuleName = buff.Read_const_char();
	if (pParticleRuleName)
	{
		bool removedOk = false;

		// find the particle rules
		ptxParticleRule* pParticleRules[32];
		int numParticleRules = RMPTFXMGR.GetAllParticleRules(atHashWithStringNotFinal(pParticleRuleName), &pParticleRules[0], 32);
		ptxAssertf(numParticleRules>0, "particle rule not found %s", pParticleRuleName);
		for (int i=0; i<numParticleRules; i++)
		{
			removedOk = RMPTFXMGR.RemoveParticleRule(pParticleRules[i], false);
		}

		// reply with removal status 
		SendBooleanReply(replyId, removedOk);
	}
}

void ptxInterface::ReceiveRenameParticleRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the current and new particle rule names
	const char* pCurrParticleRuleName = buff.Read_const_char();
	const char* pNewParticleRuleName = buff.Read_const_char();

	// rename the particle rule
	RMPTFXMGR.RenameParticleRule(pCurrParticleRuleName, pNewParticleRuleName);
}

void ptxInterface::ReceiveCloneParticleRule(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the current and new particle rule names
	const char* pParticleRuleName = buff.Read_const_char();

	// clone the particle rule
	ptxParticleRule* pParticleRule = RMPTFXMGR.CloneParticleRule(pParticleRuleName);
	if (pParticleRule)
	{
		// send the new particle rule list
		SendParticleRuleList();

		// send the new particle rule
		SendParticleRule(pParticleRule->GetName());
	}
}

void ptxInterface::ReceiveCreateTimelineEvent(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the effect rule name
	const char* pEffectRuleName = buff.Read_const_char();
	if (pEffectRuleName && strlen(pEffectRuleName)>0)
	{
		// read the event type and whether the editor needs a reply
		buff.Read_s32();
		bool needsReply = buff.Read_bool();

		// find the effect rules
		ptxEffectRule* pEffectRules[32];
		int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
		ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
		for (int i=0; i<numEffectRules; i++)
		{
			// add an emitter event to the timeline
			pEffectRules[i]->GetTimeline().AddEmitterEvent(pEffectRules[i]);
		}

		// send the effect rule back if a reply is requested
		if (needsReply)
		{
			SendEffectRule(pEffectRuleName);
		}
	}
}

void ptxInterface::ReceiveDeleteTimelineEvent(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the effect rule name
	const char* pEffectRuleName = buff.Read_const_char();

	if (pEffectRuleName)
	{
		// read the index of the timeline event to delete
		s32 eventIndex = buff.Read_s32();

		// find the effect rules
		ptxEffectRule* pEffectRules[32];
		int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
		ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
		for (int i=0; i<numEffectRules; i++)
		{
			// delete the timeline event
			pEffectRules[i]->GetTimeline().DeleteEvent(eventIndex);
		}

		// send the effect rule back
		SendEffectRule(pEffectRuleName);
	}
}

void ptxInterface::ReceiveReorderTimelineEvents(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the effect rule name
	const char* pEffectRuleName = buff.Read_const_char();

	if (pEffectRuleName)
	{
		// find the effect rules
		ptxEffectRule* pEffectRules[32];
		int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
		ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
		for (int i=0; i<numEffectRules; i++)
		{
			// reorder the timeline events
			pEffectRules[i]->GetTimeline().ReorderEvents();
		}

		// send the effect rule back
		SendEffectRule(pEffectRuleName);
	}
}

void ptxInterface::SendDomainShapeList()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_DOMAINSHAPELIST);

	// write the number of domain shapes
	m_byteBuff.Write_u32(PTXDOMAIN_SHAPE_NUM);

	// write the domain shape name and filter data
	for (int i=0; i<PTXDOMAIN_SHAPE_NUM; i++)
	{
		m_byteBuff.Write_const_char(ptxDomain::GetShapeName((ptxDomainShape)i));
		m_byteBuff.Write_u8((u8)(ptxDomain::GetTypeFilter((ptxDomainShape)i)));
	}

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::ReceiveToggleAttractorDomain(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the emitter rule name
	const char* pEmitterRuleName = buff.Read_const_char();

	if (pEmitterRuleName)
	{
		// find the emitter rules
		ptxEmitterRule* pEmitterRules[32];
		int numEmitterRules = RMPTFXMGR.GetAllEmitterRules(atHashWithStringNotFinal(pEmitterRuleName), &pEmitterRules[0], 32);
		ptxAssertf(numEmitterRules>0, "emitter rule not found %s", pEmitterRuleName);
		for (int i=0; i<numEmitterRules; i++)
		{
			// create the evolution 
			pEmitterRules[i]->ToggleAttractorDomain();
		}

		// send the emitter rule back
		SendEmitterRule(pEmitterRuleName);
	}
}

void ptxInterface::SendBehaviourList()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_BEHAVIOURLIST);

	// write the behaviour list 
	RMPTFXMGR.SendBehaviourListToEditor(m_byteBuff);

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::ReceiveAddBehaviour(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the particle rule and behaviour names
	const char* pParticleRuleName = buff.Read_const_char();
	const char* pBehaviourName = buff.Read_const_char();

	// read the reply id
	u32 replyId = buff.Read_u32();

	if (pParticleRuleName)
	{
		bool addedOk = false;

		// find the particle rules
		ptxParticleRule* pParticleRules[32];
		int numParticleRules = RMPTFXMGR.GetAllParticleRules(atHashWithStringNotFinal(pParticleRuleName), &pParticleRules[0], 32);
		ptxAssertf(numParticleRules>0, "particle rule not found %s", pParticleRuleName);
		for (int i=0; i<numParticleRules; i++)
		{
			// try to add the behaviour
			addedOk = pParticleRules[i]->AddBehaviour(pBehaviourName);
		}
			
		// send the particle rule back
		SendParticleRule(pParticleRuleName);

		// reply with add status 
		SendBooleanReply(replyId, addedOk);
	}
}

void ptxInterface::ReceiveDeleteBehaviour(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the particle rule and behaviour names
	const char* pParticleRuleName = buff.Read_const_char();
	const char* pBehaviourName = buff.Read_const_char();
	
	if (pParticleRuleName)
	{
		// find the particle rules
		ptxParticleRule* pParticleRules[32];
		int numParticleRules = RMPTFXMGR.GetAllParticleRules(atHashWithStringNotFinal(pParticleRuleName), &pParticleRules[0], 32);
		ptxAssertf(numParticleRules>0, "particle rule not found %s", pParticleRuleName);
		for (int i=0; i<numParticleRules; i++)
		{
			// remove the behaviour
			pParticleRules[i]->RemoveBehaviour(pBehaviourName);
		}

		// send the particle rule back
		SendParticleRule(pParticleRuleName);
	}
}

void ptxInterface::ReceiveValidateBehaviour(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the particle rule and behaviour names
	const char* pParticleRuleName = buff.Read_const_char();
	const char* pBehaviourName = buff.Read_const_char();

	if (pParticleRuleName)
	{
		// find the particle rules
		ptxParticleRule* pParticleRules[32];
		int numParticleRules = RMPTFXMGR.GetAllParticleRules(atHashWithStringNotFinal(pParticleRuleName), &pParticleRules[0], 32);
		ptxAssertf(numParticleRules>0, "particle rule not found %s", pParticleRuleName);
		for (int i=0; i<numParticleRules; i++)
		{
			// validate the behaviour
			pParticleRules[i]->ValidateBehaviour(pBehaviourName);
		}
	}
}

void ptxInterface::SendKeyframeDefn(ptxKeyframe* pKeyframe, const char* pRuleName, ptxRuleType ruleType, u32 propertyId, s32 eventIdx, s32 evoIdx)
{
	if (IsConnected()==false)
	{
		return;
	}

	if (pKeyframe && pKeyframe->GetDefn())
	{
		// create a new buffer
		ptxByteBuff buff;
		buff.Create(32*1024);

		// being buffering
		buff.BeginTCPBuffer();
		buff.Write_u32(MSGID_SEND_KEYFRAMEDEFN);

		// write the keyframe net id, definition and data
		buff.Write_const_char(pRuleName);
		buff.Write_s32(ruleType);
		buff.Write_u32(propertyId);
		buff.Write_s32(eventIdx);
		buff.Write_s32(evoIdx);

		ptxInterface::SaveXmlToByteBuffer(*pKeyframe->GetDefn(), buff);
		ptxInterface::SaveXmlToByteBuffer(*pKeyframe, buff);

		// end buffering
		buff.EndTCPBuffer();

		// send the buffer
		SendByteBuffer(&buff);
	}
}

void ptxInterface::ReceiveKeyframeRequest(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in info about the keyframe we're receiving
	const char* pRuleName = buff.Read_const_char();
	ptxAssertf(strcmp(pRuleName, "invalid")!=0, "invalid keyframe rule name received");
	
	ptxRuleType ruleType = static_cast<ptxRuleType>(buff.Read_s32());
	ptxAssertf(ruleType==PTXRULETYPE_EFFECT || ruleType==PTXRULETYPE_EMITTER || ruleType==PTXRULETYPE_PARTICLE, "invalid keyframe rule type received");
	
	u32 propertyId = buff.Read_u32();
	ptxAssertf(propertyId>0, "invalid keyframe property id received");
	
	s32 eventIdx = buff.Read_s32();
	ptxAssertf(eventIdx>=-1 && eventIdx<PTXTIMELINE_MAX_CHANNELS, "invalid keyframe event idx received");
	
	s32 evoIdx = buff.Read_s32();
	ptxAssertf(evoIdx>=-1 && evoIdx<PTXEVO_MAX_EVOLUTIONS, "invalid keyframe evolution index received");

	ptxKeyframe* pKeyframes[32];
	s32 numKeyframes = RMPTFXMGR.GetAllKeyframes(&pKeyframes[0], 32, atHashWithStringNotFinal(pRuleName), ruleType, propertyId, eventIdx, evoIdx);

	if (ptxVerifyf(numKeyframes>0, "keyframe not found"))
	{
		// read the reply id
		u32 replyNetId = buff.Read_u32();

		// begin buffering
		m_byteBuff.BeginTCPBuffer();
		m_byteBuff.Write_u32(replyNetId);
		
		// write the keyframe definition and data
		SaveXmlToByteBuffer(*pKeyframes[0]->GetDefn(), m_byteBuff);	
		SaveXmlToByteBuffer(*pKeyframes[0], m_byteBuff);

		// end buffering
		m_byteBuff.EndTCPBuffer();

		// send the buffer
		SendByteBuffer(&m_byteBuff);
	}
}

void ptxInterface::ReceiveKeyframe(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in info about the keyframe we're receiving
	const char* pRuleName = buff.Read_const_char();
	ptxAssertf(strcmp(pRuleName, "invalid")!=0, "invalid keyframe rule name received");
	
	ptxRuleType ruleType = static_cast<ptxRuleType>(buff.Read_s32());
	ptxAssertf(ruleType==PTXRULETYPE_EFFECT || ruleType==PTXRULETYPE_EMITTER || ruleType==PTXRULETYPE_PARTICLE, "invalid keyframe rule type received");
	
	u32 propertyId = buff.Read_u32();
	ptxAssertf(propertyId>0, "invalid keyframe property id received");
	
	s32 eventIdx = buff.Read_s32();
	ptxAssertf(eventIdx>=-1 && eventIdx<PTXTIMELINE_MAX_CHANNELS, "invalid keyframe event idx received");
	
	s32 evoIdx = buff.Read_s32();
	ptxAssertf(evoIdx>=-1 && evoIdx<PTXEVO_MAX_EVOLUTIONS, "invalid keyframe evolution index received");

	ptxKeyframe* pKeyframes[32];
	s32 numKeyframes = RMPTFXMGR.GetAllKeyframes(&pKeyframes[0], 32, atHashWithStringNotFinal(pRuleName), ruleType, propertyId, eventIdx, evoIdx);
	ptxAssertf(numKeyframes>0, "keyframe not found");

	// store the current read position
	int currReadPos = buff.GetCurrReadPos();

	for (int i=0; i<numKeyframes; i++)
	{
		// restore the current read position
		buff.SetCurrReadPos(currReadPos);

		// read the keyframe data
		LoadXmlFromByteBuffer(*pKeyframes[i], buff);
	}
}

void ptxInterface::ReceiveKeyframeExists(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in info about the keyframe we're receiving
	const char* pRuleName = buff.Read_const_char();
	ptxAssertf(strcmp(pRuleName, "invalid")!=0, "invalid keyframe rule name received");

	ptxRuleType ruleType = static_cast<ptxRuleType>(buff.Read_s32());
	ptxAssertf(ruleType==PTXRULETYPE_EFFECT || ruleType==PTXRULETYPE_EMITTER || ruleType==PTXRULETYPE_PARTICLE, "invalid keyframe rule type received");

	u32 propertyId = buff.Read_u32();
	ptxAssertf(propertyId>0, "invalid keyframe property id received");

	s32 eventIdx = buff.Read_s32();
	ptxAssertf(eventIdx>=-1 && eventIdx<PTXTIMELINE_MAX_CHANNELS, "invalid keyframe event idx received");

	s32 evoIdx = buff.Read_s32();
	ptxAssertf(evoIdx>=-1 && evoIdx<PTXEVO_MAX_EVOLUTIONS, "invalid keyframe evolution index received");

	ptxKeyframe* pKeyframes[32];
	s32 numKeyframes = RMPTFXMGR.GetAllKeyframes(&pKeyframes[0], 32, atHashWithStringNotFinal(pRuleName), ruleType, propertyId, eventIdx, evoIdx);

	// read the reply id
	u32 replyId = buff.Read_u32();

	// reply with whether the id is in use (i.e. the net object exists)
	SendBooleanReply(replyId, numKeyframes>0);
}

void ptxInterface::ReceiveCreateEvolution(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the effect rule and evolution names
	const char* pEffectRuleName = buff.Read_const_char();
	const char* pEvolutionName = buff.Read_const_char();

	if (pEffectRuleName)
	{
		// find the effect rules
		ptxEffectRule* pEffectRules[32];
		int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
		ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
		for (int i=0; i<numEffectRules; i++)
		{
			// create the evolution 
			pEffectRules[i]->CreateEvolution(pEvolutionName);
		}

		// send the effect rule back
		SendEffectRule(pEffectRuleName);
	}
}

void ptxInterface::ReceiveDeleteEvolution(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the effect rule and evolution names
	const char* pEffectRuleName = buff.Read_const_char();
	const char* pEvolutionName = buff.Read_const_char();

	if (pEffectRuleName)
	{
		// find the effect rules
		ptxEffectRule* pEffectRules[32];
		int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
		ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
		for (int i=0; i<numEffectRules; i++)
		{
			// delete the evolution
			pEffectRules[i]->DeleteEvolution(pEvolutionName);
		}

		// send the effect rule back
		SendEffectRule(pEffectRuleName);
	}
}

void ptxInterface::ReceiveAddEvolvedKeyframeProp(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the effect rule and evolution names
	const char* pEffectRuleName = buff.Read_const_char();
	const char* pEvolutionName = buff.Read_const_char();

	// read the ids
	int eventId = buff.Read_s32();
	u32 propertyId = buff.Read_u32();

	if (pEffectRuleName)
	{
		// find the effect rules
		ptxEffectRule* pEffectRules[32];
		int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
		ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
		for (int i=0; i<numEffectRules; i++)
		{
			// add the evolved keyframe property
			pEffectRules[i]->AddEvolvedKeyframeProp(pEvolutionName, propertyId, eventId);
		}

		// send the effect rule back
		SendEffectRule(pEffectRuleName);
	}
}

void ptxInterface::ReceiveDeleteEvolvedKeyframeProp(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the effect rule and evolution names
	const char* pEffectRuleName = buff.Read_const_char();
	const char* pEvolutionName = buff.Read_const_char();

	// read the ids
	int eventId = buff.Read_s32();
	u32 propertyId = buff.Read_u32();

	if (pEffectRuleName)
	{
		// find the effect rules
		ptxEffectRule* pEffectRules[32];
		int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
		ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
		for (int i=0; i<numEffectRules; i++)
		{
			// delete the evolved keyframe property
			pEffectRules[i]->DeleteEvolvedKeyframeProp(pEvolutionName, propertyId, eventId);
		}

		// send the effect rule back
		SendEffectRule(pEffectRuleName);
	}
}

void ptxInterface::ReceiveLoadEvolution(ptxByteBuff& ASSERT_ONLY(buff))
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	ptxAssertf(0, "evolution loading needs re-implemented");	
}

void ptxInterface::ReceiveSaveEvolution(ptxByteBuff& ASSERT_ONLY(buff))
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	ptxAssertf(0, "evolution saving needs re-implemented");	
}

void ptxInterface::SendClipRegionData()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_CLIPREGIONDATA);

	// write the clip region data
	ptxClipRegions::SendToEditor(m_byteBuff);

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendBlendSetList()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_BLENDSETLIST);

	// write the number of blend sets
	//m_byteBuff.Write_u8(grcbsCount);
	m_byteBuff.Write_u8(2);

	// write the blend set names
	int numBlendSets = 0;
	numBlendSets++; m_byteBuff.Write_const_char("Normal");
	numBlendSets++; m_byteBuff.Write_const_char("Add");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Subtract");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Lightmap");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Matte");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Overwrite");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Dest");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Alpha Add");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Reverse Subtract");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Min");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Max");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Alpha Subtract");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Multiply Src Dest");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Composite Alpha");
// 	numBlendSets++; m_byteBuff.Write_const_char("(DO NOT USE) Composite Alpha Subtract");
//	ptxAssertf(numBlendSets == grcbsCount, "blend set mismatch (%d, %d)", numBlendSets, grcbsCount);

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendShaderTemplateList()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_SHADERTEMPLATELIST);  

	// write the shader template list
	ptxShaderTemplateList::SendToEditor(m_byteBuff);

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::ReceiveSetShaderTemplate(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the particle rule name
	const char* pParticleRuleName = buff.Read_const_char();

	// read the shader name
	const char* pShaderName = buff.Read_const_char();

	// store the current read position
	int currReadPos = buff.GetCurrReadPos();

	// find the particle rules
	ptxParticleRule* pParticleRules[32];
	int numParticleRules = RMPTFXMGR.GetAllParticleRules(atHashWithStringNotFinal(pParticleRuleName), &pParticleRules[0], 32);
	ptxAssertf(numParticleRules>0, "particle rule not found %s", pParticleRuleName);
	for (int i=0; i<numParticleRules; i++)
	{
		// restore the current read position
		buff.SetCurrReadPos(currReadPos);

		// set the shader template
		pParticleRules[i]->GetShaderInst().SetShaderTemplate(pShaderName, pParticleRules[i]->GetShaderInst().GetShaderTechniqueName());

		// set the shader data
		pParticleRules[i]->GetShaderInst().ReceiveFromEditor(buff);
	}

	// send the particle rule back
	SendParticleRule(pParticleRuleName);
}

void ptxInterface::ReceiveAddDeleteModel(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the particle rule name
	const char* pParticleRuleName = buff.Read_const_char();

	// read the flag that tells us whether we are adding or deleting the model
	bool shouldAddModel = buff.Read_bool();

	// read the model name
	const char* pModelName = buff.Read_const_char();

	const char* pPath = NULL;
	if (shouldAddModel)
	{
		// read in the path
		pPath = buff.Read_const_char();
	}

	// find the particle rules
	ptxParticleRule* pParticleRules[32];
	int numParticleRules = RMPTFXMGR.GetAllParticleRules(atHashWithStringNotFinal(pParticleRuleName), &pParticleRules[0], 32);
	ptxAssertf(numParticleRules>0, "particle rule not found %s", pParticleRuleName);
	for (int i=0; i<numParticleRules; i++)
	{
		if (shouldAddModel)
		{
			// store the old path and set the new
			char oldPath[256];
			formatf(oldPath, sizeof(oldPath), "%s", RMPTFXMGR.GetAssetPath());
			RMPTFXMGR.SetAssetPath(pPath);

			// add the model
			pParticleRules[i]->AddModel(pModelName, true);

			// restore the old path
			RMPTFXMGR.SetAssetPath(oldPath);
		}
		else
		{
			// remove the model
			pParticleRules[i]->RemoveModel(pModelName);
		}
	}

	// send the particle rule back
	SendParticleRule(pParticleRuleName);
}

void ptxInterface::ReceiveSave(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the type of rule that we are saving
	ptxRuleType ruleType = static_cast<ptxRuleType>(buff.Read_u8());

	// read the name of the rule we are saving
	const char* pRuleName = buff.Read_const_char();

	// read whether we want to save the rule dependants as well
	bool saveDependents = buff.Read_bool();

	// save out the rule 
	switch (ruleType)
	{
		// effect rule
	case PTXRULETYPE_EFFECT:
		{
			ptxEffectRule* pEffectRule = RMPTFXMGR.GetEffectRule(atHashWithStringNotFinal(pRuleName));
			if (pEffectRule) 
			{
				if (saveDependents) 
				{
					pEffectRule->SaveAll();
				}
				else
				{
					pEffectRule->Save();
				}
			}

			break;
		}

		// emitter rule
	case PTXRULETYPE_EMITTER:
		{
			ptxEmitterRule* pEmitterRule = RMPTFXMGR.GetEmitterRule(atHashWithStringNotFinal(pRuleName));
			if (pEmitterRule) 
			{
				pEmitterRule->Save();
			}

			break;
		}

		// particle rule
	case PTXRULETYPE_PARTICLE:
		{
			ptxParticleRule* pParticleRule = RMPTFXMGR.GetParticleRule(atHashWithStringNotFinal(pRuleName));
			if (pParticleRule)
			{
				pParticleRule->Save();
			}

			break;
		}

	default:
		{
			ptxAssertf(0, "rule type not supported");
			break;
		}
	}
}

void ptxInterface::ReceiveSaveAll(ptxByteBuff& ASSERT_ONLY(buff))
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	RMPTFXMGR.SaveAll();
}

void ptxInterface::ReceiveCancelSaveAll(ptxByteBuff& ASSERT_ONLY(buff))
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	RMPTFXMGR.SaveAllCancel();
}

void ptxInterface::SendSaveAllCancelled()
{
	if (IsConnected()==false)
	{
		return;
	}

	// being buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_RECEIVE_CANCELSAVEALL);

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendSaveAllComplete()
{
	if (IsConnected()==false)
	{
		return;
	}

	// being buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_SAVEALLCOMPLETE);

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendSaveAllProgressUpdate(int progressBarId, int numItems, int currItem)
{
	if (IsConnected()==false)
	{
		return;
	}

	// being buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_SAVEALLPROGRESS);

	// write the save progress info
	m_byteBuff.Write_s32(progressBarId);
	m_byteBuff.Write_s32(numItems);
	m_byteBuff.Write_s32(currItem);

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendVersion()
{
	if (IsConnected()==false)
	{
		return;
	}

	// create a new buffer
	ptxByteBuff buff;
	buff.Create(32*1024);

	// begin buffering
	buff.BeginTCPBuffer();
	buff.Write_u32(MSGID_SEND_VERSION);

	// write the version number
	buff.Write_float(RMPTFX_VERSION);

	// end buffering
	buff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&buff);
}

void ptxInterface::SendDrawLists()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_DRAWLISTS);

	// write the number of draw lists
	int numDrawLists = RMPTFXMGR.GetNumDrawLists();
	m_byteBuff.Write_u32(numDrawLists);

	// write the draw list names
	for (int i=0; i<numDrawLists; i++)
	{
		m_byteBuff.Write_const_char(RMPTFXMGR.GetDrawList(i)->GetName());
	}

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendGameSpecificData()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_GAMESPECIFICDATA);

	// write the data
	RMPTFXMGR.SendGameFlagNamesToEditor(m_byteBuff);
	RMPTFXMGR.SendDataVolumeTypeNamesToEditor(m_byteBuff);

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::ReceiveRescaleZoomableComponents(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the effect rule name
	const char* pEffectRuleName = buff.Read_const_char();
	
	if (pEffectRuleName)
	{
		// find the effect rules
		ptxEffectRule* pEffectRules[32];
		int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
		ptxAssertf(numEffectRules>0, "effect rule not found %s", pEffectRuleName);
		for (int i=0; i<numEffectRules; i++)
		{
			// rescale the zoomable components of the effect rule
			pEffectRules[i]->RescaleZoomableComponents();
		}
	}
}

void ptxInterface::SendGlobalProfileData()
{
	if (IsConnected()==false)
	{
		return;
	}

	// begin buffering
	m_byteBuff.BeginTCPBuffer();
	m_byteBuff.Write_u32(MSGID_SEND_GLOBALPROFILEDATA);

	// write the profile data
	RMPTFXMGR.SendProfileDataToEditor(m_byteBuff);

	// end buffering
	m_byteBuff.EndTCPBuffer();

	// send the buffer
	SendByteBuffer(&m_byteBuff);
}

void ptxInterface::SendEffectRuleProfileData(ptxByteBuff& buff)
{
	if (IsConnected()==false)
	{
		return;
	}

	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the effect rule name
	const char* pEffectRuleName = buff.Read_const_char();

	// find the effect rules
	ptxEffectRule* pEffectRules[32];
	int numEffectRules = RMPTFXMGR.GetAllEffectRules(atHashWithStringNotFinal(pEffectRuleName), &pEffectRules[0], 32);
	if (ptxVerifyf(numEffectRules>0, "effect rule not found %s", pEffectRuleName))
	{
		// create ui data to accumulate all the rule data
		ptxEffectRuleUIData accumUIData;

		for (int i=0; i<numEffectRules; i++)
		{
			accumUIData.AccumulateFrom(pEffectRules[i]->GetUIData());
		}

		// begin buffering
		m_byteBuff.BeginTCPBuffer();
		m_byteBuff.Write_u32(MSGID_SEND_EFFECTRULEPROFILEDATA);

		// write the accumulated profile data
		m_byteBuff.Write_const_char(pEffectRuleName);
		m_byteBuff.Write_s32(accumUIData.GetNumPointsUpdated());
		m_byteBuff.Write_s32(accumUIData.GetNumActiveInstances());
		m_byteBuff.Write_s32(accumUIData.GetNumCulledInstances());
		m_byteBuff.Write_float(accumUIData.GetAccumCPUUpdateTime());
		m_byteBuff.Write_float(accumUIData.GetAccumCPUDrawTime());

		// end buffering
		m_byteBuff.EndTCPBuffer();

		// send the buffer
		SendByteBuffer(&m_byteBuff);
	}

}

void ptxInterface::SendEmitterRuleProfileData(ptxByteBuff& buff)
{
	if (IsConnected()==false)
	{
		return;
	}

	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the emitter rule name
	const char* pEmitterRuleName = buff.Read_const_char();

	// find the emitter rules
	ptxEmitterRule* pEmitterRules[32];
	int numEmitterRules = RMPTFXMGR.GetAllEmitterRules(atHashWithStringNotFinal(pEmitterRuleName), &pEmitterRules[0], 32);
	if (ptxVerifyf(numEmitterRules>0, "emitter rule not found %s", pEmitterRuleName))
	{
		// create ui data to accumulate all the rule data
		ptxEmitterRuleUIData accumUIData;

		for (int i=0; i<numEmitterRules; i++)
		{
			accumUIData.AccumulateFrom(pEmitterRules[i]->GetUIData());
		}

		// begin buffering
		m_byteBuff.BeginTCPBuffer();
		m_byteBuff.Write_u32(MSGID_SEND_EMITTERRULEPROFILEDATA);

		// write the accumulated profile data
		m_byteBuff.Write_const_char(pEmitterRuleName);
		m_byteBuff.Write_float(accumUIData.GetAccumCPUUpdateTime());
		m_byteBuff.Write_float(accumUIData.GetAccumCPUDrawTime());
		m_byteBuff.Write_float(accumUIData.GetAccumCPUSortTime());
		m_byteBuff.Write_s32(accumUIData.GetNumPointsUpdated());
		m_byteBuff.Write_s32(accumUIData.GetNumActiveInstances());

		// end buffering
		m_byteBuff.EndTCPBuffer();

		// send the buffer
		SendByteBuffer(&m_byteBuff);
	}
}

void ptxInterface::SendParticleRuleProfileData(ptxByteBuff& buff)
{
	if (IsConnected()==false)
	{
		return;
	}

	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read the particle rule name
	const char* pParticleRuleName = buff.Read_const_char();

	// find the particle rules
	ptxParticleRule* pParticleRules[32];
	int numParticleRules = RMPTFXMGR.GetAllParticleRules(atHashWithStringNotFinal(pParticleRuleName), &pParticleRules[0], 32);
	if (ptxVerifyf(numParticleRules>0, "particle rule not found %s", pParticleRuleName))
	{
		// create ui data to accumulate all the rule data
		int numActiveBehaviours = pParticleRules[0]->GetNumActiveBehaviours();
		u32* pAccumCPUUpdateTicks = rage_new u32[numActiveBehaviours];
		memset(pAccumCPUUpdateTicks, 0, sizeof(u32)*numActiveBehaviours);

		for (int i=0; i<numParticleRules; i++)
		{
			ptxAssertf(pParticleRules[i]->GetNumActiveBehaviours()==numActiveBehaviours, "active behaviour mismatch");
			for (int j=0; j<numActiveBehaviours; j++)
			{
				pAccumCPUUpdateTicks[j] += pParticleRules[i]->GetActiveBehaviour(j)->GetAccumCPUUpdateTicks();
			}
		}

		// begin buffering
		m_byteBuff.BeginTCPBuffer();
		m_byteBuff.Write_u32(MSGID_SEND_PARTICLERULEPROFILEDATA);

		// write the accumulated profile data
		m_byteBuff.Write_const_char(pParticleRuleName);
		m_byteBuff.Write_s32(numActiveBehaviours);
		for (int j=0; j<numActiveBehaviours; j++)
		{
			ptxBehaviour* pBehaviour = pParticleRules[0]->GetActiveBehaviour(j);
			m_byteBuff.Write_const_char(pBehaviour->GetEditorName());
			m_byteBuff.Write_float(pAccumCPUUpdateTicks[j] * sysTimer::GetTicksToMilliseconds());
		}

		// clean up allocated memory
		delete[] pAccumCPUUpdateTicks;

		// end buffering
		m_byteBuff.EndTCPBuffer();

		// send the buffer
		SendByteBuffer(&m_byteBuff);
	}
}

void ptxInterface::ReceivePlayControlData(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in the effect rule name
	buff.Read_const_char();

	// read in the frame time data (global control)
	m_timeScalar = buff.Read_float();
	RMPTFXMGR.SetDeltaTimeScalar(m_timeScalar);

	// read in the repeat data (effect control)
	m_shouldRepeatPlayback = buff.Read_bool();
	m_repeatPlaybackTime = buff.Read_float();

	// read in data about where to play the debug effect
	m_spawnCamRelative = buff.Read_bool();
	m_spawnCamDist = buff.Read_float();
	m_spawnWorldPos.SetXf(buff.Read_float());
	m_spawnWorldPos.SetYf(buff.Read_float());
	m_spawnWorldPos.SetZf(buff.Read_float());

	// read in the debug animation data (anim info)
	m_animEnabled = buff.Read_bool();
	m_animSpeed = buff.Read_float();
	m_animRadius = buff.Read_float();
	m_animMode = buff.Read_u8();
}

void ptxInterface::ReceiveStartDebugEffect(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in the effect rule name
	const char* pEffectRuleName = buff.Read_const_char();

	// start the debug effect
	RMPTFXMGR.StartDebugEffect(pEffectRuleName);
}

void ptxInterface::ReceiveShowDebugInfo(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in whether we want to show the names 
	bool showNames = buff.Read_bool();

	// update the profile draw data
#if RMPTFX_BANK
	ptxDebug::sm_drawEffectInstInfo = showNames;
	ptxDebug::sm_drawEffectInstNames = showNames;
#else
	showNames = showNames;
#endif
}

void ptxInterface::ReceiveReset(ptxByteBuff& buff)
{
	// check we're at the correct reading position
	ptxAssertf(buff.GetCurrReadPos()==8, "buffer read pos error");

	// read in whether we are just resetting a single effect
	bool resetDebugEffectOnly = buff.Read_bool();
	if (resetDebugEffectOnly)
	{
		RMPTFXMGR.ResetDebugEffect();
	}
	else
	{
		// reset all effect rules
		RMPTFXMGR.Reset(NULL);
	}
}

// void ptxInterface::TestSendEffectRule(const char *name)
// {
// 	SendObject(RMPTFXMGR.GetEffectRule(atHashWithStringNotFinal(name)), MSGID_SEND_EFFECTRULE);
// }


#endif // RMPTFX_EDITOR

