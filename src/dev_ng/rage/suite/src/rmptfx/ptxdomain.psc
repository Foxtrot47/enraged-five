<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxDomainObj" cond="RMPTFX_XML_LOADING">
	<pointer name="m_pDomain" type="rage::ptxDomain" policy="owner"/>
</structdef>

<structdef type="rage::ptxDomain" onPostLoad="OnPostLoad" cond="RMPTFX_XML_LOADING" onPreLoad="OnPreLoad">
	<float name="m_fileVersion"/>
	<bool name="m_isWorldSpace"/>
	<bool name="m_isPointRelative"/>
  <bool name="m_isCreationRelative"/>
  <bool name="m_isTargetRelative"/>
	<int name="m_type"/>
	<struct name="m_positionKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_rotationKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_sizeOuterKFP" type="rage::ptxKeyframeProp"/>
	<struct name="m_sizeInnerKFP" type="rage::ptxKeyframeProp"/>
</structdef>

<structdef type="rage::ptxDomainBox" base="rage::ptxDomain" cond="RMPTFX_XML_LOADING">
</structdef>

<structdef type="rage::ptxDomainSphere" base="rage::ptxDomain" cond="RMPTFX_XML_LOADING">
</structdef>

<structdef type="rage::ptxDomainCylinder" base="rage::ptxDomain" cond="RMPTFX_XML_LOADING">
</structdef>

<structdef type="rage::ptxDomainAttractor" base="rage::ptxDomain" cond="RMPTFX_XML_LOADING">
</structdef>

</ParserSchema>