<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Size" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_whdMinKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_whdMaxKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_tblrScalarKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_tblrVelScalarKFP" type="::rage::ptxKeyframeProp"/>
	<int name="m_keyframeMode"/>
	<bool name="m_isProportional"/>
</structdef>

</ParserSchema>