//
// rmptfx/rendersetup.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef RMPTFX_RENDERSETUP_H
#define RMPTFX_RENDERSETUP_H

#include "vectormath/vec3v.h"

namespace rage
{
	class grmShader;
	class ptxEffectInst;
	class ptxEmitterInst;

	// PURPOSE : Interface class to allow for setting up game specific constants
	// and rendering setup.
	//
	class ptxRenderSetup
	{
	public:
		virtual ~ptxRenderSetup() { /*NoOp */ };

		// PURPOSE : Setup rendering, bounds passed to allow for setting local settings
		// NOTES: hash value passed to make it easier to switch on specific shaders
		// RETURNS: false if error occured and drawing should be aborted.
		virtual bool PreDraw(const ptxEffectInst* pEffectInst, const ptxEmitterInst* pEmitterInst, Vec3V_In vMin, Vec3V_In vMax, u32 shaderHashName, grmShader* pShader) const = 0;

		// PURPOSE : Cleans up Rendering, returns false if error detected with state.
		virtual	bool PostDraw(const ptxEffectInst* UNUSED_PARAM(pEffectInst), const ptxEmitterInst* UNUSED_PARAM(pEmitterInst)) const {return true;}
	};


} // namespace rage

#endif
