// 
// rmptfx/ptxu_matrixweight.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_MATRIXWEIGHT_H 
#define PTXU_MATRIXWEIGHT_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// enumerations
enum ptxu_MatrixWeight_ReferenceSpace
{
	PTXU_MATRIXWEIGHT_REFERENCESPACE_EFFECT		= 0,
	PTXU_MATRIXWEIGHT_REFERENCESPACE_EMITTER,

	PTXU_MATRIXWEIGHT_REFERENCESPACE_NUM
};


// classes
class ptxu_MatrixWeight : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_MatrixWeight();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_MatrixWeight(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_MatrixWeight);

		// info
		const char* GetName() {return "ptxu_MatrixWeight";}												// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_MATRIXWEIGHT;}											// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}																// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}																// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}													// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}																// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}															// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);														// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);													// called from InitPoint and UpdatePoint to update the particle accordingly 

		void SetKeyframeDefns();																		// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);									// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "MatrixWeight";}											// name of the behaviour shown in the editor
		bool IsActive();																				// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);														// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);				// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);										// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif



	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_mtxWeightKFP;

		// non keyframe data
		int m_referenceSpace;

		datPadding<12> m_pad;

		// static data
		static ptxKeyframeDefn sm_mtxWeightDefn;

};


} // namespace rage


#endif // PTXU_MATRIXWEIGHT_H 
