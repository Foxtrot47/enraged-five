// 
// rmptfx/ptxmisc.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXMISC_H
#define RMPTFX_PTXMISC_H

// includes
#include "vectormath/scalarv.h"

// defines
#define RMPTFX_LOW_PRECISION_EVO_VALUES		(1)


// namespaces
namespace rage 
{


// enumerations
enum ptxRuleType
{
	PTXRULETYPE_INVALID						= -1,

	PTXRULETYPE_EFFECT						= 0,
	PTXRULETYPE_EMITTER,
	PTXRULETYPE_PARTICLE
};


// typedefs
#if RMPTFX_LOW_PRECISION_EVO_VALUES
typedef u8									ptxEvoValueType;
#else
typedef float								ptxEvoValueType;
#endif


// global functions
inline ptxEvoValueType PackEvoValue(float val)
{
#if RMPTFX_LOW_PRECISION_EVO_VALUES
	return (u8)(val*255.0f);
#else
	return val;
#endif
}

inline float UnPackEvoValue(ptxEvoValueType val)
{
#if RMPTFX_LOW_PRECISION_EVO_VALUES
	return (float)val/255.0f;
#else
	return val;
#endif
}

__forceinline ScalarV_Out UnPackEvoValueV(const ptxEvoValueType& val)
{
#if RMPTFX_LOW_PRECISION_EVO_VALUES
	//TODO: Optimize for Durango/Orbis
	return ScalarV((float)val/255.0f);
#else
	return ScalarVFromF32(val);
#endif
}

} // namespace rage


#endif // RMPTFX_PTXMISC_H

