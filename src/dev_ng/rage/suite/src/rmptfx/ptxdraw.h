// 
// rmptfx/ptxdraw.h
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXDRAW_H
#define RMPTFX_PTXDRAW_H


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxdrawcommon.h"
#include "rmptfx/ptxlist.h"
#include "rmptfx/ptxpoint.h"

// includes (rage)
#include "grcore/device.h"
#include "grcore/effect.h"
#include "grcore/effect_config.h"
#include "grcore/vertexdecl.h"
#include "grcore/stateblock.h"
#include "grcore/indexbuffer.h"
#include "math/float16.h"
#include "system/taskheader.h"
#include "shaderlib/rage_constants.h"

// namespaces
namespace rage
{
// defines
#define PTXDRAW_SPRITE_USE_QUADS						(RSG_ORBIS)
#define PTXDRAW_SPRITE_USE_INDEXED_VERTS				(__D3D11 || RSG_ORBIS)
#define PTXDRAW_SPRITE_USE_INDEX_BUFFER					(DEVICE_IM_INSTANCED_INDEXED)

//Also need to set PTFX_USE_INSTANCING in ptx_common.fxh
//currently on works with indexed verts
#define PTXDRAW_USE_INSTANCING							(PTXDRAW_SPRITE_USE_INDEXED_VERTS && (__D3D11 || RSG_ORBIS))

#define PTXDRAW_TRAIL_USE_INDEX_BUFFERS					(__D3D11 || RSG_ORBIS)

#define PTXDRAW_TRAIL_USE_CLIP_REGION					(0)
#define PTXDRAW_TRAIL_USE_RANDOM_TABLE					(0)

#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
#define PTXDRAW_SPRITE_USE_INDEXED_VERTS_ONLY(x)		x
#else
#define PTXDRAW_SPRITE_USE_INDEXED_VERTS_ONLY(x)
#endif

#define PTXDRAW_MULTIPLE_DRAWS_PER_BATCH				(__D3D11 || RSG_ORBIS)

#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH
#define PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(x)	, x
#define PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY(x)		x
#else
#define PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(x)
#define PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY(x)
#endif


#if PTXDRAW_SPRITE_USE_QUADS
#define PTXDRAW_NUM_VERTS_PER_SPRITE_LORES				(4)
#define PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES				(16)
#else 
#define PTXDRAW_NUM_VERTS_PER_SPRITE_LORES				(6)
#define PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES				(24)
#endif 

#define PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED			(20)

#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
#define PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED		(8)
#define PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED		(9)
#define PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED		(4)
#define PTXDRAW_MAX_INDEXED_SPRITES_PROJ_PER_BATCH		(GPU_VERTICES_MAX_SIZE/PTXDRAW_NUM_VERTS_PER_SPRITE_PROJ_INDEXED/sizeof(ptxSpriteVtxCommon))
#define PTXDRAW_MAX_INDEXED_SPRITES_PER_BATCH_HIRES		(GPU_VERTICES_MAX_SIZE/PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED/sizeof(ptxSpriteVtxCommon))
#define PTXDRAW_MAX_INDEXED_SPRITES_PER_BATCH_LORES		(GPU_VERTICES_MAX_SIZE/PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED/sizeof(ptxSpriteVtxCommon))
#endif

#define PTXDRAW_NUM_CUSTOM_VARS							(8)

#if PTXDRAW_USE_INSTANCING
#define PTXDRAW_LOW_RES_INDEXBUFFER_SIZE 				(PTXDRAW_NUM_VERTS_PER_SPRITE_LORES)
#define PTXDRAW_HI_RES_INDEXBUFFER_SIZE 				(PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES)
#define PTXDRAW_PROJECTED_INDEXBUFFER_SIZE 				(PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED)
#else
#define PTXDRAW_LOW_RES_INDEXBUFFER_SIZE 				(PTXDRAW_MAX_INDEXED_SPRITES_PER_BATCH_LORES*PTXDRAW_NUM_VERTS_PER_SPRITE_LORES)
#define PTXDRAW_HI_RES_INDEXBUFFER_SIZE 				(PTXDRAW_MAX_INDEXED_SPRITES_PER_BATCH_HIRES*PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES)
#define PTXDRAW_PROJECTED_INDEXBUFFER_SIZE 				(PTXDRAW_MAX_INDEXED_SPRITES_PROJ_PER_BATCH*PTXDRAW_NUM_VERTS_PER_SPRITE_PROJECTED)
#endif

// forward declarations
class grcViewport;
class ptxd_Sprite;
class ptxd_Trail;
class ptxEffectInst;
class ptxEmitterInst;
class ptxEvolutionList;
class ptxKeyframeProp;
class ptxPointDB;
class ptxPointSB;
class ptxParticleRule;


// ============================================================

#define PTXDRAW_TRAIL_TESS_X_MIN     (0*4 + 1)
#define PTXDRAW_TRAIL_TESS_X_MAX     (4*4 + 1)
#define PTXDRAW_TRAIL_TESS_X_DEFAULT (2*4 + 1)

#define PTXDRAW_TRAIL_TESS_Y_MIN     (1)
#define PTXDRAW_TRAIL_TESS_Y_MAX     (6)
#define PTXDRAW_TRAIL_TESS_Y_DEFAULT (4)

#define PTXDRAW_TRAIL_MAX_CONTROL_POINTS (64)

// ============================================================

struct ptxCustomVars
{
	float data[PTXDRAW_NUM_CUSTOM_VARS];
};

struct ptxSpriteInst
{
	Vec4V vCentrePos_Colour;
	Vec4V vNormal_texFrameRatio_emissiveIntensity;
	Vec4V vCustomVars;
#if PTXDRAW_USE_INSTANCING
	Vec4V vOffsetUp;	//w offsetLeft X
	Vec4V vOffsetDown;	//w offsetLeft Y
	Vec4V vOffsetRight;	//w offsetLeft Z
	Vec4V vClipRegion;
	Vec4V vMisc1;		// non projected: uvInfoA
	Vec4V vMisc2;		// non projected: uvInfoB
						// projected: x - projectionDepth, y - TexFrameA, z - TexFrameB, w - numTexColomns
#endif
};
CompileTimeAssert((sizeof(ptxSpriteInst)&15)==0);
#if !PTXDRAW_USE_INSTANCING
CompileTimeAssert(sizeof(ptxSpriteInst)==48);
#endif

struct ptxSpriteVertex
{
	Vec4V	vWorldPos;								// worldPosX, worldPosY, worldPosZ, unused
	Vec4V	vTexAUV_TexBUV_NormUV_distToCentre;		// uTexA, vTexA, uTexB, vTexB, uNorm, vNorm, distToCentre, unused
};
CompileTimeAssert((sizeof(ptxSpriteVertex)&15)==0);
CompileTimeAssert(sizeof(ptxSpriteVertex)==32);

struct ptxSpriteVtxCommon
{
	ptxSpriteInst inst;
	ptxSpriteVertex vtx;
};
CompileTimeAssert((sizeof(ptxSpriteVtxCommon)&15)==0);
#if !PTXDRAW_USE_INSTANCING
CompileTimeAssert(sizeof(ptxSpriteVtxCommon)==80);
#endif

// ============================================================

template <bool bLit> class ptxTrailVtx;

template <> class ptxTrailVtx<false>
{
public:
	Vec4V pos_colour;
	Vec4V tex; // zw unused
};
CompileTimeAssert((sizeof(ptxTrailVtx<false>)&15)==0);

template <> class ptxTrailVtx<true>
{
public:
	Vec4V pos_colour;
	Vec4V tex; // zw unused
	Vec3V tangent; // w unused
};
CompileTimeAssert((sizeof(ptxTrailVtx<true>)&15)==0);


// ============================================================

struct ptxDrawSpriteInstParams
{
	// particle data
	Vec3V vAlignAxis;								// directional only data
	float nearClipDist;								// directional only data
	float farClipDist;								// directional only data
	float projectionDepth;
	float alphaMult;

	// particle flags
	u32 isDirectional				: 1;
	u32 isScreenSpace				: 1;
	u32	isProjected					: 1;
	u32 isHiRes						: 1;
	u32 alignToAxis					: 1;			// directional only data
	u32 nearClip					: 1;			// directional only data
	u32 farClip						: 1;			// directional only data
	u32 uvClip						: 1;			// directional only data
	u32 : 0;

	// draw data
	Mat34V vCamMtx;
	Vec3V vAspectRatio;
	Vec3V vUpWld;
	const Vec4V* pvClipRegionData;	
	int numVertsPerSprite;
	int* pPosIdxTable;
	int numTexRows;
	int numTexColumns;
};

struct ptxDrawSpriteParams
{
	ptxPointSB* pPointSB;
	ptxPointDB* pPointDB;
	ptxCustomVars* pCustomVars;	
#if PTXDRAW_USE_INSTANCING
	ptxSpriteInst** ppCurrInst;
#else
	ptxSpriteVtxCommon** ppCurrCommon;
#endif
	bool flipU;
	bool flipV;
	bool bDrawDegenerateWhenAlphaCulled; // for PTXDRAW_USE_ALPHA_CULL: on 360 we draw degenerate quads to avoid going over the point list twice
};


// classes
class ptxDrawInterface
{
	friend class ptxManager;


	///////////////////////////////////
	// CLASSES 
	///////////////////////////////////

	public: ///////////////////////////

#if PTXDRAW_MULTIPLE_DRAWS_PER_BATCH
	class ptxMultipleDrawInterface
	{
	public:
		virtual ~ptxMultipleDrawInterface() {};
		virtual u32 NoOfDraws() = 0;
		virtual void PreDraw(u32 i) = 0;
		virtual void PostDraw(u32 i) = 0;
		virtual void PreDrawInstanced() = 0;
		virtual void PostDrawInstanced() = 0;
		virtual grcViewport *GetViewport(int i) = 0;
		virtual bool IsUsingInstanced() = 0;
		virtual void SetCulledState(bool Culled, u32 i) = 0;
		virtual bool GetCulledState(int i) = 0;
		virtual bool ShouldBeRendered(ptxEffectInst* pEffectInst) = 0;
		virtual void SetNumActiveCascades(u8 i) = 0;
		virtual u32 GetNumActiveCascades() = 0;
	};
#endif

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Init();
		static void Shutdown();

		static void DeviceLost();
		static void DeviceReset();

#if GS_INSTANCED_SHADOWS
		static void SetShadowPass(bool bEnable);
#endif
#if !PTXDRAW_SPRITE_USE_INDEX_BUFFER
		static bool BeginSprites(int numSpritesToDraw, int numVertsPerSprite, bool drawTriStrip = false);
		static void EndSprites(int numVertsPerSprite PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface));
		static void EndCreateSpritesInternal(int numVertsPerSprite);
		static void DrawSpritesInternal(int numVertsPerSprite);
		static void ReleaseSpritesInternal(int numVertsPerSprite);
		static void EndSpritesInternal(int numVertsPerSprite);
#elif PTXDRAW_USE_INSTANCING
		static bool BeginSpritesIndexInstancedBuffer(int numSpritesToDraw, bool drawTriStrip = false);
		static void EndSpritesIndexInstancedBuffer(u32 numSpritesToDraw, u32 vertexSize, u32 numIndicesPerSprite, const grcIndexBuffer *pIndexBuffer, const grcVertexBuffer *pVertBuffer PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface));
#else
		static bool BeginSpritesIndexBuffer(int numSpritesToDraw, int numVertsPerSprite, bool drawTriStrip = false);
		static void EndSpritesIndexBuffer(int numSpritesToDraw, int numIndicesPerSprite, const grcIndexBuffer *pIndexBuffer PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface));
#endif

		static void SetupPosIdxTables();
		static void DrawSprites(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxPointItem** ppPoints, unsigned numPoints, ptxd_Sprite* pDrawSpriteBehaviour, ptxParticleRule* pParticleRule, ptxEvolutionList* pEvoList, bool isDirectional, bool isHiRes, bool isProjected PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface));
		static void DrawTrail(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxd_Trail* pDrawTrailBehaviour, ptxParticleRule* pParticleRule, ptxEvolutionList* pEvoList, Vec3V_In vAnchorPos, ScalarV_In vAnchorSize, Vec4V_In vAnchorCol, ScalarV_In vTexWrapLength, ScalarV_In vTexOffset PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface));
		static void BatchSprite(const ptxDrawSpriteInstParams& drawInstParams, ptxDrawSpriteParams& drawSpriteParams);
		static void SetupPtxSpriteInst(ptxSpriteInst* pCurrInst, const ptxDrawSpriteInstParams& drawInstParams, ptxDrawSpriteParams& drawSpriteParams, const Vec3V& vCentrePos, const Vec3V& vSpriteUp, const Vec3V& vSpriteNormal);

#if RMPTFX_BANK
		static void InitRenderStateBlocks();
		static void SetDebugWireframeRenderState();
		static void RestoreDefaultRenderState();
#endif

	private: //////////////////////////

		template <bool bIsLit>
		static void DrawTrail(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxd_Trail* pDrawTrailBehaviour, ptxEvolutionList* UNUSED_PARAM(pEvolutionList), Vec3V_In vAnchorPos, ScalarV_In vAnchorSize, Vec4V_In vAnchorCol, ScalarV_In vTexWrapLength, ScalarV_In vTexOffset  PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxMultipleDrawInterface* pMultipleDrawInterface));

		static void CalcUVInfo(Vec4V_InOut vUVInfo, int numColumns, int numRows, int texId, ScalarV_In vClipMin, ScalarV_In vClipMax, bool flipU, bool flipV);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		static u32 sm_maxSprites;
		static Vec4V sm_defaultClipRegion;

#if GS_INSTANCED_SHADOWS
		static grcVertexDeclaration* sm_pSpriteVtxDecl[NUMBER_OF_RENDER_THREADS];
		static grcVertexDeclaration* sm_pSpriteVtxDeclDefault, *sm_pSpriteVtxDeclInst;
#else
		static grcVertexDeclaration* sm_pSpriteVtxDecl;
#endif
		static grcVertexDeclaration* sm_pTrailVtxDecl;
		static grcVertexDeclaration* sm_pTrailVtxDecl_lit;

		static int sm_posIdxTableLoRes[PTXDRAW_NUM_VERTS_PER_SPRITE_LORES*2];
		static int sm_posIdxTableHiRes[PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES*2];
#if PTXDRAW_SPRITE_USE_INDEXED_VERTS
		static int sm_posIdxTableHiResIndexed[PTXDRAW_NUM_VERTS_PER_SPRITE_HIRES_INDEXED*2];
		static int sm_posIdxTableLoResIndexed[PTXDRAW_NUM_VERTS_PER_SPRITE_LORES_INDEXED*2];
		static u16 sm_spriteVtxIndicesLoRes[PTXDRAW_LOW_RES_INDEXBUFFER_SIZE];
		static u16 sm_spriteVtxIndicesHiRes[PTXDRAW_HI_RES_INDEXBUFFER_SIZE];
		static u16 sm_spriteProjVtxIndices[PTXDRAW_PROJECTED_INDEXBUFFER_SIZE];
#if PTXDRAW_SPRITE_USE_INDEX_BUFFER
		static grcIndexBuffer *sm_pLoResIndexBuffer;
		static grcIndexBuffer *sm_pHiResIndexBuffer;
		static grcIndexBuffer *sm_pProjIndexBuffer;
#endif

#if PTXDRAW_USE_INSTANCING
		static grcVertexBuffer *sm_pLoResVertBuffer;
		static grcVertexBuffer *sm_pHiResVertBuffer;
		static grcVertexBuffer *sm_pProjVertBuffer;
#endif //PTXDRAW_USE_INSTANCING
#endif //PTXDRAW_SPRITE_USE_INDEXED_VERTS

#if PTXDRAW_TRAIL_USE_INDEX_BUFFERS
		static const grcIndexBuffer *sm_pTrailIndexBuffers[PTXDRAW_TRAIL_TESS_X_MAX - PTXDRAW_TRAIL_TESS_X_MIN + 1][PTXDRAW_TRAIL_TESS_Y_MAX - PTXDRAW_TRAIL_TESS_Y_MIN + 1];
#endif

#if PTXDRAW_USE_INSTANCING
		static ptxSpriteInst* sm_pCurrInstance[NUMBER_OF_RENDER_THREADS];
#else
		static ptxSpriteVtxCommon* sm_pCurrCommon[NUMBER_OF_RENDER_THREADS];
#endif 

#if RMPTFX_BANK
		static grcRasterizerStateHandle			sm_debugWireframeRasterizerState;
		static grcBlendStateHandle				sm_debugWireframeBlendState;
		static grcDepthStencilStateHandle		sm_debugWireframeDepthStencilState;
		static grcRasterizerStateHandle			sm_exitDebugWireframeRasterizerState;
		static grcBlendStateHandle				sm_exitDebugWireframeBlendState;
		static grcDepthStencilStateHandle		sm_exitDebugWireframeDepthStencilState;
#endif
};


} // namespace rage


#endif // RMPTFX_PTXDRAW_H
