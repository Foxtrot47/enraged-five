// 
// rmptfx/ptxd_drawtrail.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXD_DRAWTRAIL_H 
#define PTXD_DRAWTRAIL_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// forward declarations
namespace rage
{
	struct sysTaskParameters;
}


// namespaces
namespace rage
{


// enumerations
enum ptxd_Trail_AlignmentMode
{
	PTXD_TRAIL_ALIGNMENTMODE_CAMERA	= 0,
	PTXD_TRAIL_ALIGNMENTMODE_WORLD,
	PTXD_TRAIL_ALIGNMENTMODE_EFFECT,
	PTXD_TRAIL_ALIGNMENTMODE_EMITTER,

	PTXD_TRAIL_ALIGNMENTMODE_NUM
};


// classes
class ptxd_Trail : public ptxBehaviour
{
	friend class ptxParticleRule;
	friend class ptxDrawInterface;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxd_Trail();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxd_Trail(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxd_Trail);

		// info
		const char* GetName() {return "ptxd_Trail";}											// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_DRAWTRAIL;}									// the ordering of the behaviour (see ptxconfig.h)
		ptxDrawType GetTypeFilter() {return PTXPARTICLERULE_DRAWTYPE_TRAIL;}					// filter for deciding which type of particle this behaviour should be active

		bool NeedsToInit() {return false;}														// whether we call init when a particle is created
		bool NeedsToUpdate() {return false;}													// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return true;}												// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return true;}														// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}													// whether we call release when a particle dies

		void UpdateFinalize(ptxBehaviourParams& params);

		__forceinline void DrawPoints(ptxEffectInst* RESTRICT pEffectInst,						// called for each active particle to draw them
			ptxEmitterInst* RESTRICT pEmitterInst,
			ptxParticleRule* RESTRICT pParticleRule,
			ptxEvolutionList* RESTRICT pEvoList
			PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface));

		void SetKeyframeDefns();																// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);							// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Draw (Trail)";}									// name of the behaviour shown in the editor
		bool IsActive() {return true;}															// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);												// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);		// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);								// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif

	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_texInfoKFP;

		// non keyframe data
		Vec3V m_alignAxis;

		int m_alignmentMode;

		int m_tessellationU;
		int m_tessellationV;

		float m_smoothnessX;
		float m_smoothnessY;

		float m_projectionDepth;
		float m_shadowCastIntensity;

		bool m_flipU;
		bool m_flipV;
		bool m_wrapTextureOverParticleLife;
		bool m_disableDraw;


		// static data
		static ptxKeyframeDefn sm_texInfoDefn;

};


} // namespace rage


#endif // PTXD_DRAWTRAIL_H 
