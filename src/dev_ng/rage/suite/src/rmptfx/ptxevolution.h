// 
// rmptfx/ptxevolution.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXEVOLUTION_H 
#define RMPTFX_PTXEVOLUTION_H 


// includes (rmptfx)
#include "rmptfx/ptxkeyframe.h"
#include "rmptfx/ptxmisc.h"

// includes (rage)
#include "atl/binmap.h"
#include "atl/hashstring.h"


// namespaces
namespace rage {


// forward declarations
class ptxByteBuff;
class ptxEffectRule;
class ptxKeyframeProp;
class ptxKeyframePropList;
	

// defines
#define PTXEVO_MAX_EVOLUTIONS		(6)


// enumerations
enum ptxEvoBlendMode
{
	PTXEVO_MODE_AVERAGE				= 0,
	PTXEVO_MODE_ADD,
	PTXEVO_MODE_MAX,
	PTXEVO_MODE_FULL_AVERAGE,

	PTXEVO_MODE_NUM
};


// classes
class ptxEvolution
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEvolution();
		
		// resourcing
		ptxEvolution(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxEvolution);

		// accessors
		void SetName(const char* pName);
		const char* GetNameString() const {return m_name;}
		atHashString GetNameHash() const { return m_nameHash; }
		bool GetIsOverridden() {return m_isOverriden;}
		float GetOverrideValue() {return m_overrideValue;}
		
		// editor
#if RMPTFX_EDITOR
		void SendToEditor(ptxByteBuff& buff);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_SIMPLE_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ConstString m_name;																// name of the evolution
		atHashString m_nameHash;														// hash of the evolution name
		float m_overrideValue;															// the override value (from the editor)
		bool m_isOverriden;																// whether the evolution value is overriden (from the editor)
		datPadding<3> m_pad;
};


class ptxEvolvedKeyframe
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEvolvedKeyframe();

		// resourcing
		ptxEvolvedKeyframe(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxEvolvedKeyframe);
	
		// accessorts
		void SetKeyframe(const ptxKeyframe& keyframe) {m_keyframe = keyframe;}
		ptxKeyframe& GetKeyframe() {return m_keyframe;}
		void SetEvolutionIndex(int val) {m_evolutionIdx = val;}
		int GetEvolutionIndex() {return m_evolutionIdx;}
		void SetIsLodEvolution(bool val) {m_isLodEvolution = val;}
		bool GetIsLodEvolution() {return m_isLodEvolution;}

		// editor
#if RMPTFX_EDITOR
		void SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule, s32 eventIdx, u32 propertyId);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_SIMPLE_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ptxKeyframe	m_keyframe;															// the evolved keyframe data 
		int m_evolutionIdx;																// the index of the evolution
		bool m_isLodEvolution;															// the index of any lod evolution
		datPadding<11> m_pad;
};
	

class ptxEvolvedKeyframeProp
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEvolvedKeyframeProp();
		~ptxEvolvedKeyframeProp(); 

		// resourcing
		ptxEvolvedKeyframeProp(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxEvolvedKeyframeProp);
	
		// evolved keyframe manipulation
		void ResetEvolvedKeyframes() {m_evolvedKeyframes.Reset();}
		ptxEvolvedKeyframe& AddEvolvedKeyframe() {return m_evolvedKeyframes.Grow();}
		void DeleteEvolvedKeyframe(int idx) {m_evolvedKeyframes.Delete(idx);}
		void DeleteEvolvedKeyframeFast(int idx) {m_evolvedKeyframes.DeleteFast(idx);}

		// accessors
		int GetNumEvolvedKeyframes() {return m_evolvedKeyframes.GetCount();}
		ptxEvolvedKeyframe& GetEvolvedKeyframe(int idx) {return m_evolvedKeyframes[idx];}
		void SetPropertyId(u32 val) {m_propertyId = val;}
		u32 GetPropertyId() {return m_propertyId;}
		ptxEvoBlendMode GetBlendMode() {return (ptxEvoBlendMode)m_blendMode;}
	
		// editor
#if RMPTFX_EDITOR
		void ScaleEvolvedKeyframes(ScalarV_In vScale);
		void SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule, s32 eventIdx);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif
	
		// parser
#if RMPTFX_XML_LOADING
		PAR_SIMPLE_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		atArray<ptxEvolvedKeyframe> m_evolvedKeyframes;									// array of this property's evolved keyframes
		u32 m_propertyId;																// the id of the property we're keyframing (usually set a hash of the class and variable name)
		u8 m_blendMode;																	// how the base and evolved keyframes are blended to produce the final keyframe
		datPadding<3> m_pad;

};


class ptxEvolutionList
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEvolutionList() {};
		~ptxEvolutionList();

		// resourcing
		ptxEvolutionList(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxEvolutionList);

		Vec4V_Out Query(ptxKeyframeProp* pKeyframeProp, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues);

		// accessors
		int GetNumEvolvedKeyframeProps() {return m_evolvedKeyframeProps.GetCount();}
		ptxEvolvedKeyframeProp* GetEvolvedKeyframePropById(u32 propertyId) const;
		ptxEvolvedKeyframeProp* GetEvolvedKeyframePropByIndex(int keyframePropIdx) {return &m_evolvedKeyframeProps[keyframePropIdx];}
		int GetNumEvolutions() {return m_evolutions.GetCount();}
//		int GetEvolutionIndex(const char* pEvoName) const;
//		int GetEvolutionIndex(atHashValue evoHashValue) const;
		int GetEvolutionIndexFromHash(atHashValue evoHashValue) const;
		int GetEvolutionIndexFromHash16(u16 evoHashValue) const;

		const char* GetEvolutionName(int evoIdx) {return m_evolutions[evoIdx].GetNameString();}

		void SetKeyframeDefn(ptxKeyframeProp* pKeyframeProp, ptxKeyframeDefn* pKeyframeDefn);

		// editor
#if RMPTFX_EDITOR
		void GenerateUniqueName(char* pName, int maxLength);

		int AddEvolution(const char* pEvoName);
		void DeleteEvolution(const char* pEvoName);
		void AddEvolvedKeyframeProp(ptxKeyframeProp* pKeyframeProp, const char* pEvoName);
		void DeleteEvolvedKeyframeProp(ptxKeyframeProp* pKeyframeProp, const char* pEvoName);
		
		bool GetEvolutionIsOverriden(int evoIdx) {return m_evolutions[evoIdx].GetIsOverridden();}
		float GetEvolutionOverrideValue(int evoIdx) {return m_evolutions[evoIdx].GetOverrideValue();}
		
		void UpdateUIData(ptxKeyframePropList* pKeyframePropList, const ptxEffectRule* pEffectRule, s32 eventIdx);
		void UpdateUIData(ptxKeyframeProp* pKeyframeProp, const ptxEffectRule* pEffectRule, s32 eventIdx);
		
		void SendToEditor(ptxByteBuff& buff, const ptxEffectRule* pEffectRule, s32 eventIdx);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif
	
		// parser
#if RMPTFX_XML_LOADING
		PAR_SIMPLE_PARSABLE;
#endif


	private: //////////////////////////

		// static functions
		static Vec4V_Out ApplyBlendAverage(ptxEvolvedKeyframeProp* pEvolvedKeyframeProp, Vec4V_In vBaseValue, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues);
		static Vec4V_Out ApplyBlendAdditive(ptxEvolvedKeyframeProp* pEvolvedKeyframeProp, Vec4V_In vBaseValue, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues);
		static Vec4V_Out ApplyBlendMax(ptxEvolvedKeyframeProp* pEvolvedKeyframeProp, Vec4V_In vBaseValue, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues);
		static Vec4V_Out ApplyBlendFullAverage(ptxEvolvedKeyframeProp* pEvolvedKeyframeProp, Vec4V_In vBaseValue, ScalarV_In vKeyTime, const ptxEvoValueType* pEvoValues);
		
		// xml loading
#if RMPTFX_XML_LOADING
		void OnPostLoad();
		void GenerateKeyframePropMap(); 
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		atArray<ptxEvolution> m_evolutions;												// array of evolutions
		atArray<ptxEvolvedKeyframeProp> m_evolvedKeyframeProps;							// array of evolved keyframed properties
		atStringMap<datRef<ptxEvolvedKeyframeProp> > m_evolvedKeyframePropMap;			// map of evolved keyframed properties

};


} // namespace rage


#endif // RMPTFX_PTXEVOLUTION_H 
