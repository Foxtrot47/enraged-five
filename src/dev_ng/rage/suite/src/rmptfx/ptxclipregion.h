// 
// rmptfx/ptxregion.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXREGION_H
#define RMPTFX_PTXREGION_H

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxconfig.h"

// includes (rage)
#include "file/token.h"
#include "atl/array.h"
#include "vectormath/classes.h"



// namespaces
namespace rage
{
// structures
class ptxClipRegionData
{
	friend class ptxClipRegionTable;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	private: //////////////////////////

		int Load(fiTokenizer& token, Vec4V*& quadmem);
		Vec4V_Out Get(int column, int row) const;
		Vec4V_Out operator[](int x) const;

		int GetNumColumns() const {return m_numColumns;}
		int GetNumRows() const {return m_numRows;}
		int GetNumFrames() const {return m_numColumns*m_numRows;}
		Vec4V* GetData() {return m_pData;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		int m_numColumns;
		int m_numRows;
		Vec4V* m_pData;
};


// classes
class ptxClipRegionTable
{
	friend class ptxClipRegions;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_EDITOR
		void SendToEditor(ptxByteBuff& buff);
#endif


	private: //////////////////////////

		~ptxClipRegionTable();

		bool Load(const char* pName);

		int GetIdx(u32 hashName) const;
		void GetData(int id, const Vec4V*& pRegions, int& numColumns, int& numRows);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		atArray<u32> m_hashNames;
		atArray<ptxClipRegionData> m_clipRegions;

		Vec4V* m_pClipRegionPool;
		int m_numClipRegions;

#if RMPTFX_EDITOR
		atArray<ConstString> m_textureNames;
#endif
};

// 
class ptxClipRegions
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void	Load(const char* pName);
		static bool GetData(u32 textureHashName, const Vec4V*& pvClipRegionData, int& numColumns, int& numRows);
		static Vec4V_Out GetDefaultData() {return sm_defaultData;}

#if RMPTFX_EDITOR
		static void SendToEditor(ptxByteBuff& buff) {sm_clipRegionTable.SendToEditor(buff);}
#endif

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		static const Vec4V sm_defaultData;
		static ptxClipRegionTable sm_clipRegionTable ;

		static bool sm_isInitialised;

};

} // namespace rage

#endif //RMPTFX_PTXREGION_H