// 
// rmptfx/ptxu_collision.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_COLLISION_H 
#define PTXU_COLLISION_H 


// includes
#include "parser/macros.h"
#include "grprofile/drawcore.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// forward declarations (for functions in ptxu_collision_common.cpp - could be moved to a ptxu_collision_common.h file eventually)
class ptxCollisionPoly;

int My_FindImpactPolygonToSphere(Vec3V_ConstRef vSphereCenter, ScalarV_In vSphereRadius, const Vec3V_Ptr vVertices,
	 int numVertices, Vec3V_ConstRef vPolyNormal, Vec3V_InOut vSpherePosition,
	 Vec3V_InOut vPolyPosition, int& idNum, Vec3V_InOut vNormal, ScalarV_InOut vDepth);

int My_SegmentTriangleIntersectUndirected(Vec3V_In vStart, Vec3V_In vDir,
	Vec3V_In vEnd, Vec3V_In vNormal, Vec3V_In v0, Vec3V_In v1, Vec3V_In v2, const Vec3V_Ptr v3,
	ScalarV_InOut vROutAbove, ScalarV_InOut vROutBelow, ScalarV_InOut vROutT);

ScalarV_Out CalcRadius(ptxPointSB* pPointSB, ptxPointDB* pPointDB, ScalarV_In vRadiusMult, ScalarV_In vOverrideMinRadius);

bool HasCollidedWithPoly(Vec3V_InOut vColnPos, Vec3V_InOut vColnNormal, ScalarV_InOut vColnDepth, ptxCollisionPoly& poly, Vec3V_In vPtxCurrPos, Vec3V_In vPtxLastPos, Vec3V_In vPtxDir, ScalarV_In vPtxRadius, bool bTooFast);


// classes
class ptxu_Collision : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Collision();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_Collision(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Collision);

		// info
		const char* GetName() {return "ptxu_Collision";}												// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_COLLISION;}											// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}																// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}																// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return true;}														// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}																// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}															// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);														// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);													// called from InitPoint and UpdatePoint to update the particle accordingly 

		void UpdateFinalize(ptxBehaviourParams& params);

		void SetKeyframeDefns();																		// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);									// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Collision";}												// name of the behaviour shown in the editor
		bool IsActive();																				// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);														// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);				// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);										// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif



	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_bouncinessKFP;
		ptxKeyframeProp m_bounceDirVarKFP;

		// non keyframe data
		float m_radiusMult;
		float m_restSpeed;
		int m_colnChance;
		int m_killChance;

		// non parsable data
		bool m_debugDraw;

		datPadding<3> m_pad;

		float m_overrideMinRadius;
		datPadding<8> m_pad2;

		// static data
		static ptxKeyframeDefn sm_bouncinessDefn;
		static ptxKeyframeDefn sm_bounceDirVarDefn;

};


} // namespace rage


#endif // PTXU_COLLISION_H 
