// 
// rmptfx/ptxcollision.cpp 
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxcollision.h"

// includes (rage)
#include "grcore/debugdraw.h"
#include "rmptfx/ptxchannel.h"
#include "vectormath/legacyconvert.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// code
void ptxCollisionPoly::SetTri(const Vec3V* pvVerts, Vec3V_In vNormal, u8 mtlId)
{
	ptxAssertf(IsFiniteAll(vNormal), "vNormal is not finite");

	SetIsQuad(false);

	for (int i=0; i<3; i++)
	{
		SetVertex(i, pvVerts[i]);
	}

	SetNormal(vNormal);
	SetMtlId(mtlId);
}

void ptxCollisionPoly::SetQuad(const Vec3V* pvVerts, Vec3V_In vNormal, u8 mtlId)
{
	ptxAssertf(IsFiniteAll(vNormal), "vNormal is not finite");

	SetIsQuad(true);

	for (int i=0; i<4; i++)
	{
		SetVertex(i, pvVerts[i]);
	}

	SetNormal(vNormal);
	SetMtlId(mtlId);
}


void ptxCollisionSet::Init(int maxNumColnPolys)
{
	m_colnPolys.Reserve(maxNumColnPolys);
}

void ptxCollisionSet::AddPoly(ptxCollisionPoly& colnPoly) 
{
	m_colnPolys.PushAndGrow(colnPoly);
}

void ptxCollisionSet::AddPolys(ptxCollisionPoly* pColnPolys, int numColnPolys)
{
	for (int i=0; i<numColnPolys; i++)
	{
		AddPoly(pColnPolys[i]);
	}
}

void ptxCollisionSet::AddTri(const Vec3V* pvVerts, Vec3V_In vNormal, u8 mtlId)
{
	ptxAssertf(IsFiniteAll(vNormal), "vNormal is not finite");

	ptxCollisionPoly colnPoly;
	colnPoly.SetTri(pvVerts, vNormal, mtlId);
	AddPoly(colnPoly);
}

void ptxCollisionSet::AddQuad(const Vec3V* pvVerts, Vec3V_In vNormal, u8 mtlId)
{
	ptxAssertf(IsFiniteAll(vNormal), "vNormal is not finite");

	ptxCollisionPoly colnPoly;
	colnPoly.SetQuad(pvVerts, vNormal, mtlId);
	AddPoly(colnPoly);
}

void ptxCollisionSet::CopyFrom(ptxCollisionSet* pColnSet)
{
	if (pColnSet)
	{
		Clear();

		for (int i=0; i<pColnSet->m_colnPolys.GetCount(); i++)
		{
			AddPoly(pColnSet->m_colnPolys[i]);
		}
	}
}

#if RMPTFX_EDITOR && RMPTFX_BANK
void rage::ptxCollisionSet::DebugDraw()
{
	for (int i=0; i<m_colnPolys.GetCount(); i++)
	{
		int numVerts = m_colnPolys[i].GetNumVerts();
		Vec3V vVerts[4];
		vVerts[0] = m_colnPolys[i].GetVertex(0);
		vVerts[1] = m_colnPolys[i].GetVertex(1);
		vVerts[2] = m_colnPolys[i].GetVertex(2);
		vVerts[3] = m_colnPolys[i].GetVertex(3);
		// Vec3V vNormal = m_colnPolys[i].GetNormal();

		if (numVerts==4)
		{
			grcDebugDraw::Quad(vVerts[0], vVerts[1], vVerts[2], vVerts[3], Color32(255, 128, 128, 100), false, true);
			grcDebugDraw::Quad(vVerts[0], vVerts[1], vVerts[2], vVerts[3], Color32(255, 128, 128, 255), false, false);
		}
		else
		{
			grcDebugDraw::Poly(vVerts[0], vVerts[1], vVerts[2], Color32(255, 128, 128, 100), false, true);
			grcDebugDraw::Poly(vVerts[0], vVerts[1], vVerts[2], Color32(255, 128, 128, 255), false, false);
		}
	}
}
#endif