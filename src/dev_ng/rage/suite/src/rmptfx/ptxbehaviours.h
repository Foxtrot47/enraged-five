// 
// rmptfx/ptxbehaviours.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 
// 

#ifndef BEHAVIOURLIST_H
#define BEHAVIOURLIST_H


// draw behaviours
#include "ptxd_drawmodel.h"
#include "ptxd_drawsprite.h"
#include "ptxd_drawtrail.h"

// update behaviours
#include "ptxu_acceleration.h"
#include "ptxu_age.h"
#include "ptxu_animatetexture.h"
#include "ptxu_attractor.h"
#include "ptxu_collision.h"
#include "ptxu_colour.h"
#include "ptxu_dampening.h"
#include "ptxu_matrixweight.h"
#include "ptxu_noise.h"
#include "ptxu_pause.h"
#include "ptxu_rotation.h"
#include "ptxu_size.h"
#include "ptxu_velocity.h"
#include "ptxu_wind.h"


#endif // BEHAVIOURLIST_H
