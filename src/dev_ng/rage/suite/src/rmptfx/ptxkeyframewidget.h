// 
// rmptfx/ptxkeyframewidget.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXKEYFRAMEWIDGET_H
#define RMPTFX_PTXKEYFRAMEWIDGET_H


// includes (us)
#include "rmptfx/ptxkeyframe.h"

// includes
#include "bank/widget.h"


// namespaces
namespace rage {


// classes
#if RMPTFX_BANK
class ptxKeyframeWidget : public bkWidget
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor / destructor
		ptxKeyframeWidget();
		ptxKeyframeWidget(bkBank& bank, ptxKeyframe& keyframe, const char* pTitle, const char* pMemo, const char* pFillColor=NULL);
		~ptxKeyframeWidget();

		// operator overloading
		const ptxKeyframeWidget& operator=(ptxKeyframeWidget&) {return *this;}

		// virtual overrides
		static void InitClass();
		int GetGuid() const;
		void RemoteCreate();
		void RemoteUpdate();
		void UpdateUIData();
		int DrawLocal(int x, int y);
		static void RemoteHandler(const bkRemotePacket& remotePacket);
#if __WIN32PC
		void WindowCreate();
		rageLRESULT WindowMessage(rageUINT, rageWPARAM, rageLPARAM) {return 0;}
#endif

		void SetCallback(datCallback callback) {
			if (m_ExtraData)
			{
				m_ExtraData->m_Callback = callback;
			}
			else if (!(callback == NullCB))
			{
				m_ExtraData = rage_new ExtraData;
				m_ExtraData->m_Callback = callback;
			}
		}

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		bkBank& m_bank;
		ptxKeyframe& m_keyframe;

};
#endif // RMPTFX_BANK


} // namespace rage


#endif // RMPTFX_PTXKEYFRAMEWIDGET_H
