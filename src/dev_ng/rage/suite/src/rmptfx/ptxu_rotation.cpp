// 
// rmptfx/ptxu_rotation.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "rmptfx/ptxu_rotation.h"
#include "rmptfx/ptxu_rotation_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Rotation::sm_initialAngleMin2dDefn(		"Initial Angle Min",		atHashValue("ptxu_Rotation:m_initialAngleMinKFP"),			PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"Min");
ptxKeyframeDefn ptxu_Rotation::sm_initialAngleMax2dDefn(		"Initial Angle Max",		atHashValue("ptxu_Rotation:m_initialAngleMaxKFP"),			PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"Max");
ptxKeyframeDefn ptxu_Rotation::sm_initialAngleMin3dDefn(		"Initial Angle Min",		atHashValue("ptxu_Rotation:m_initialAngleMinKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X Min",		"Y Min",		"Z Min");
ptxKeyframeDefn ptxu_Rotation::sm_initialAngleMax3dDefn(		"Initial Angle Max",		atHashValue("ptxu_Rotation:m_initialAngleMaxKFP"),			PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X Max",		"Y Max",		"Z Max");
ptxKeyframeDefn ptxu_Rotation::sm_angleMin2dDefn(				"Angle Min",				atHashValue("ptxu_Rotation:m_angleMinKFP"),					PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-1800.0f,	1800.0f,	0.01f),		"Min");
ptxKeyframeDefn ptxu_Rotation::sm_angleMax2dDefn(				"Angle Max",				atHashValue("ptxu_Rotation:m_angleMaxKFP"),					PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-1800.0f,	1800.0f,	0.01f),		"Max");
ptxKeyframeDefn ptxu_Rotation::sm_angleMin3dDefn(				"Angle Min",				atHashValue("ptxu_Rotation:m_angleMinKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-1800.0f,	1800.0f,	0.01f),		"X Min",		"Y Min",		"Z Min");
ptxKeyframeDefn ptxu_Rotation::sm_angleMax3dDefn(				"Angle Max",				atHashValue("ptxu_Rotation:m_angleMaxKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-1800.0f,	1800.0f,	0.01f),		"X Max",		"Y Max",		"Z Max");																												


// code
#if RMPTFX_XML_LOADING
ptxu_Rotation::ptxu_Rotation()
{
	// create keyframes
	m_initialAngleMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Rotation:m_initialAngleMinKFP"));
	m_initialAngleMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Rotation:m_initialAngleMaxKFP"));
	m_angleMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Rotation:m_angleMinKFP"));
	m_angleMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Rotation:m_angleMaxKFP"));

	m_keyframePropList.AddKeyframeProp(&m_initialAngleMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_initialAngleMaxKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_angleMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_angleMaxKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Rotation::SetDefaultData()
{
	// default keyframes
	m_initialAngleMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_initialAngleMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_angleMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_angleMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_initRotationMode = PTXU_ROTATION_ROTATEMODE_KEYFRAME;
	m_updateRotationMode = PTXU_ROTATION_ROTATEMODE_KEYFRAME;

	m_accumulateAngle = false;
	m_rotateAngleAxes = false;
	m_rotateInitAngleAxes = false;

	m_speedFadeThresh = 0.0f;
}

ptxu_Rotation::ptxu_Rotation(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_initialAngleMinKFP(rsc)
	, m_initialAngleMaxKFP(rsc)
	, m_angleMinKFP(rsc)
	, m_angleMaxKFP(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Rotation_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Rotation::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Rotation, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Rotation, m_initialAngleMinKFP)
		SSTRUCT_FIELD(ptxu_Rotation, m_initialAngleMaxKFP)
		SSTRUCT_FIELD(ptxu_Rotation, m_angleMinKFP)
		SSTRUCT_FIELD(ptxu_Rotation, m_angleMaxKFP)
		SSTRUCT_FIELD(ptxu_Rotation, m_initRotationMode)
		SSTRUCT_FIELD(ptxu_Rotation, m_updateRotationMode)
		SSTRUCT_FIELD(ptxu_Rotation, m_accumulateAngle)
		SSTRUCT_FIELD(ptxu_Rotation, m_rotateAngleAxes)
		SSTRUCT_FIELD(ptxu_Rotation, m_rotateInitAngleAxes)
		SSTRUCT_IGNORE(ptxu_Rotation, m_pad)
		SSTRUCT_FIELD(ptxu_Rotation, m_speedFadeThresh)
	SSTRUCT_END(ptxu_Rotation)
}
#endif

IMPLEMENT_PLACE(ptxu_Rotation);

void ptxu_Rotation::InitPoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// get the emitter life ratio key time
	ScalarV vKeyTimePointEmitter = ScalarVFromF32(pPointSB->GetEmitterLifeRatio());

	// cache some data that we'll need later
	//ptxEmitterRule*	RESTRICT pEmitterRule = static_cast<ptxEmitterRule*>(pEmitterInst->GetEmitterRule());

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vInitialAngleMin = m_initialAngleMinKFP.Query(vKeyTimePointEmitter, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vInitialAngleMax = m_initialAngleMaxKFP.Query(vKeyTimePointEmitter, pEvolutionList, pPointSB->GetEvoValues());

	// calculate the bias value
	ScalarV vBias = GetBiasValue(m_initialAngleMinKFP, pPointSB->GetRandIndex());

	// calculate the lerped angle between the min and max keyframe data and convert to radians
	Vec4V vInitialAngle = Lerp(vBias, vInitialAngleMin, vInitialAngleMax) * Vec4V(ScalarV(V_TO_RADIANS));

	if (pPointSB->GetFlag(PTXPOINT_FLAG_IS_SPRITE) || pPointSB->GetFlag(PTXPOINT_FLAG_IS_TRAIL))
	{
		pPointSB->SetInitRotX(vInitialAngle.GetX());
	}
	else
	{
		// calc the initial rotation matrix
		Mat34V initMtx = Mat34V(V_IDENTITY);
		if (m_initRotationMode==PTXU_ROTATION_ROTATEMODE_EFFECT)
		{
			initMtx = pEffectInst->GetMatrixNoInvertedAxes();
		}
		else if (m_initRotationMode==PTXU_ROTATION_ROTATEMODE_EMITTER)
		{
			initMtx = pEmitterInst->GetCreationDomainMtxWld(pEffectInst->GetMatrixNoInvertedAxes());
		}
		else 
		{
			ptxAssertf(m_initRotationMode==PTXU_ROTATION_ROTATEMODE_KEYFRAME, "unsupported rotation mode");

			if (m_rotateInitAngleAxes)
			{
				Mat34VRotateLocalX(initMtx, vInitialAngle.GetX());
				Mat34VRotateLocalY(initMtx, vInitialAngle.GetY());
				Mat34VRotateLocalZ(initMtx, vInitialAngle.GetZ());
			}
			else
			{
				Mat34VFromEulersXYZ(initMtx, vInitialAngle.GetXYZ());
			}
		}

		// store the initial rotation as a quaternion and set the current to be the same
		QuatV vInitRot = QuatVFromMat33V(initMtx.GetMat33());
		vInitRot = Normalize(vInitRot);
		pPointSB->SetInitRot(vInitRot);
		pPointDB->SetCurrRot(vInitRot);
	}

	// now update the particle 
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointRotation)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Rotation::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	
	ScalarV	vDeltaTime = params.vDeltaTime;

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// cache data from the point
	ScalarV	vPlaybackRate = ScalarVFromF32(pPointSB->GetPlaybackRate());

	// get the particle life ratio key time
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();

	// cache some data that we'll need later
	//ptxEmitterRule*	RESTRICT pEmitterRule = static_cast<ptxEmitterRule*>(pEmitterInst->GetEmitterRule());

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vAngleMin = m_angleMinKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vAngleMax = m_angleMaxKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());

	// calculate the bias value
	ScalarV vBias = GetBiasValue(m_angleMinKFP, pPointSB->GetRandIndex());

	// get the particle speed scalar
	ScalarV vSpeedFadeThresh = ScalarVFromF32(m_speedFadeThresh);
	ScalarV vPtxSpeed = Mag(pPointDB->GetCurrVel());
	ScalarV vPtxSpeedScalar = SelectFT(IsLessThan(vPtxSpeed, vSpeedFadeThresh), ScalarV(V_ONE), vPtxSpeed/vSpeedFadeThresh);
	// 	if (pPointSB->GetFlag(PTXPOINT_FLAG_AT_REST))
	// 	{
	// 		vPtxSpeedScalar = ScalarV(V_ZERO);
	// 	}

	// calculate the lerped angle between the min and max keyframe data and convert to radians
	Vec4V vAngle = Lerp(vBias, vAngleMin, vAngleMax) * Vec4V(ScalarV(V_TO_RADIANS));

	// get the initial and current rotations as matrices
	Mat34V initMtx, currMtx;
	Mat34VFromQuatV(initMtx, pPointSB->GetInitRot());
	Mat34VFromQuatV(currMtx, pPointDB->GetCurrRot());

	// calc the new rotation matrix
	if (pPointSB->GetFlag(PTXPOINT_FLAG_IS_SPRITE) || pPointSB->GetFlag(PTXPOINT_FLAG_IS_TRAIL))
	{
		if (m_accumulateAngle)
		{
			ScalarV vAngleToApply = vAngle.GetX() * vDeltaTime * vPlaybackRate * vPtxSpeedScalar;
			pPointDB->IncCurrRotX(vAngleToApply);
		}
		else
		{
			pPointDB->SetCurrRotX(vAngle.GetX());
		}
	}
	else if (pPointSB->GetFlag(PTXPOINT_FLAG_IS_MODEL))
	{
		Mat34V newMtx = Mat34V(V_IDENTITY);
		if (m_updateRotationMode==PTXU_ROTATION_ROTATEMODE_EFFECT)
		{
			newMtx = pEffectInst->GetMatrixNoInvertedAxes();
		}
		else if (m_updateRotationMode==PTXU_ROTATION_ROTATEMODE_EMITTER)
		{
			newMtx = pEmitterInst->GetCreationDomainMtxWld(pEffectInst->GetMatrixNoInvertedAxes());
		}
		else
		{
			ptxAssertf(m_updateRotationMode==PTXU_ROTATION_ROTATEMODE_KEYFRAME, "unsupported rotation mode");

			// calculate the angle to apply this frame
			Vec3V vAngleToApply = vAngle.GetXYZ();
			if (m_accumulateAngle)
			{
				// we're accumulating angles so the angle is specified per second instead of absolute
				vAngleToApply *= vDeltaTime * vPlaybackRate * vPtxSpeedScalar;
			}

			if (m_rotateAngleAxes)
			{
				// we are rotating the local axes with each rotation
				// initialise the new matris
				if (m_accumulateAngle)
				{
					// set to the current matrix as we're accumulating angles
					newMtx = currMtx;
				}
				else
				{
					// set to the initial matrix as we're applying absolute angles
					newMtx = initMtx;
				}

				// apply this frame's rotation about the local axes
				Mat34VRotateLocalX(newMtx, vAngleToApply.GetX());
				Mat34VRotateLocalY(newMtx, vAngleToApply.GetY());
				Mat34VRotateLocalZ(newMtx, vAngleToApply.GetZ());
			}
			else
			{
				// we are rotating about static axes
				// update any accumulated angle
				if (m_accumulateAngle)
				{
					pPointDB->IncCurrAngle(vAngleToApply);
					vAngleToApply = pPointDB->GetCurrAngle();
				}

				// calculate the rotation matrix
				Mat34V vRotMtx;
				Mat34VFromEulersXYZ(vRotMtx, vAngleToApply);

				// apply this to the initial matrix
				Transform(newMtx, initMtx, vRotMtx);
			}
		}

		// store the current rotation as a quaternion
		QuatV vCurrRot = QuatVFromMat33V(newMtx.GetMat33());
		vCurrRot = Normalize(vCurrRot);
		pPointDB->SetCurrRot(vCurrRot);
	}
	else
	{
		ptxAssertf(0, "unsupported particle type");
	}
}

void ptxu_Rotation::SetKeyframeDefns()
{
	m_initialAngleMinKFP.GetKeyframe().SetDefn(&sm_initialAngleMin2dDefn);
	m_initialAngleMaxKFP.GetKeyframe().SetDefn(&sm_initialAngleMax2dDefn);
	m_angleMinKFP.GetKeyframe().SetDefn(&sm_angleMin2dDefn);
	m_angleMaxKFP.GetKeyframe().SetDefn(&sm_angleMax2dDefn);
}

void ptxu_Rotation::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_initialAngleMinKFP, &sm_initialAngleMin2dDefn);
	pEvolutionList->SetKeyframeDefn(&m_initialAngleMaxKFP, &sm_initialAngleMax2dDefn);
	pEvolutionList->SetKeyframeDefn(&m_angleMinKFP, &sm_angleMin2dDefn);
	pEvolutionList->SetKeyframeDefn(&m_angleMaxKFP, &sm_angleMax2dDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Rotation::IsActive()
{
	ptxu_Rotation* pRotation = static_cast<ptxu_Rotation*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_initialAngleMinKFP == pRotation->m_initialAngleMinKFP &&
				   m_initialAngleMaxKFP == pRotation->m_initialAngleMaxKFP &&
				   m_angleMinKFP == pRotation->m_angleMinKFP &&
				   m_angleMaxKFP == pRotation->m_angleMaxKFP &&
				   m_initRotationMode == pRotation->m_initRotationMode &&
				   m_updateRotationMode == pRotation->m_updateRotationMode &&
				   m_accumulateAngle == pRotation->m_accumulateAngle &&
				   m_rotateAngleAxes == pRotation->m_rotateAngleAxes &&
				   m_rotateInitAngleAxes == pRotation->m_rotateInitAngleAxes && 
				   m_speedFadeThresh == pRotation->m_speedFadeThresh;

	return !isEqual;
}

void ptxu_Rotation::UpdateKeyframeDefns(ptxDrawType drawType, const ptxParticleRule* pParticleRule)
{
	if (drawType==PTXPARTICLERULE_DRAWTYPE_SPRITE)
	{
		m_initialAngleMinKFP.UpdateBehaviourDefn(&sm_initialAngleMin2dDefn, pParticleRule);
		m_initialAngleMaxKFP.UpdateBehaviourDefn(&sm_initialAngleMax2dDefn, pParticleRule);
		m_angleMinKFP.UpdateBehaviourDefn(&sm_angleMin2dDefn, pParticleRule);
		m_angleMaxKFP.UpdateBehaviourDefn(&sm_angleMax2dDefn, pParticleRule);
	}
	else
	{
		m_initialAngleMinKFP.UpdateBehaviourDefn(&sm_initialAngleMin3dDefn, pParticleRule);
		m_initialAngleMaxKFP.UpdateBehaviourDefn(&sm_initialAngleMax3dDefn, pParticleRule);
		m_angleMinKFP.UpdateBehaviourDefn(&sm_angleMin3dDefn, pParticleRule);
		m_angleMaxKFP.UpdateBehaviourDefn(&sm_angleMax3dDefn, pParticleRule);
	}
}

void ptxu_Rotation::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 10);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Initial Angle Min");
	SendKeyframeDefnToEditor(buff, "Initial Angle Max");
	SendKeyframeDefnToEditor(buff, "Angle Min");
	SendKeyframeDefnToEditor(buff, "Angle Max");

	// send non-keyframes
	const char* pComboItemNames[PTXU_ROTATION_ROTATEMODE_NUM] = {"Keyframe", "Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Init Rotation Mode", PTXU_ROTATION_ROTATEMODE_NUM, pComboItemNames);
	SendComboDefnToEditor(buff, "Update Rotation Mode", PTXU_ROTATION_ROTATEMODE_NUM, pComboItemNames);

	SendBoolDefnToEditor(buff, "Accumulate Angle");
	SendBoolDefnToEditor(buff, "Rotate Angle Axes");
	SendBoolDefnToEditor(buff, "Rotate Init Angle Axes");

	SendFloatDefnToEditor(buff, "Speed Fade Thresh");
}

void ptxu_Rotation::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_initialAngleMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_initialAngleMaxKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_angleMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_angleMaxKFP, pParticleRule);

	// send non-keyframes
	SendComboItemToEditor(buff, m_initRotationMode);
	SendComboItemToEditor(buff, m_updateRotationMode);

	SendBoolToEditor(buff, m_accumulateAngle);
	SendBoolToEditor(buff, m_rotateAngleAxes);
	SendBoolToEditor(buff, m_rotateInitAngleAxes);

	SendFloatToEditor(buff, m_speedFadeThresh);
}

void ptxu_Rotation::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_initialAngleMinKFP);
	ReceiveKeyframeFromEditor(buff, m_initialAngleMaxKFP);
	ReceiveKeyframeFromEditor(buff, m_angleMinKFP);
	ReceiveKeyframeFromEditor(buff, m_angleMaxKFP);

	// receive non-keyframes
	ReceiveComboItemFromEditor(buff, m_initRotationMode);
	ReceiveComboItemFromEditor(buff, m_updateRotationMode);

	ReceiveBoolFromEditor(buff, m_accumulateAngle);
	ReceiveBoolFromEditor(buff, m_rotateAngleAxes);
	ReceiveBoolFromEditor(buff, m_rotateInitAngleAxes);

	ReceiveFloatFromEditor(buff, m_speedFadeThresh);
}
#endif // RMPTFX_EDITOR


} // namespace rage
