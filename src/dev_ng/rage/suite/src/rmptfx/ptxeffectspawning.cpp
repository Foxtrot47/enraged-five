// 
// rmptfx/ptxeffectspawning.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxeffectspawning.h"
#include "rmptfx/ptxeffectspawning_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"

// includes (rage)
#include "data/safestruct.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// code
ptxSpawnedEffectScalars::ptxSpawnedEffectScalars()
{
	Reset();
}

ptxSpawnedEffectScalars::ptxSpawnedEffectScalars(datResource& rsc)
: m_colourTintScalar(rsc)
, m_flags(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxSpawnedEffectScalars_num++;
#endif
}

#if __DECLARESTRUCT
void ptxSpawnedEffectScalars::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxSpawnedEffectScalars)
		SSTRUCT_FIELD(ptxSpawnedEffectScalars, m_durationScalar)
		SSTRUCT_FIELD(ptxSpawnedEffectScalars, m_playbackRateScalar)
		SSTRUCT_FIELD(ptxSpawnedEffectScalars, m_colourTintScalar)
		SSTRUCT_FIELD(ptxSpawnedEffectScalars, m_zoomScalar)
		SSTRUCT_FIELD(ptxSpawnedEffectScalars, m_flags)
		SSTRUCT_IGNORE(ptxSpawnedEffectScalars, m_pad)
		SSTRUCT_END(ptxSpawnedEffectScalars);
}
#endif

IMPLEMENT_PLACE(ptxSpawnedEffectScalars);

void ptxSpawnedEffectScalars::Reset()
{
	m_flags.Reset();

	m_durationScalar = 1.0f;
	m_playbackRateScalar = 1.0f;
	m_colourTintScalar.Set(255, 255, 255, 255);
	m_zoomScalar = 100.0f;
}

void ptxSpawnedEffectScalars::SetRandomly(const ptxSpawnedEffectScalars& min, const ptxSpawnedEffectScalars& max)
{
	// check the min and max have the same flags set
	ptxAssert(min.m_flags.IsEqual(max.m_flags));

	// reset the flags
	m_flags.Reset();

	// set duration
	if (min.m_flags.IsSet(PTXSPAWNSCALAR_ACTIVE_DURATION)) 
	{
		SetDurationScalar(Lerp(g_DrawRand.GetFloat(), min.GetDurationScalar(), max.GetDurationScalar()));
	}

	// set playback rate
	if (min.m_flags.IsSet(PTXSPAWNSCALAR_ACTIVE_PLAYBACK_RATE))
	{
		SetPlaybackRateScalar(Lerp(g_DrawRand.GetFloat(), min.GetPlaybackRateScalar(), max.GetPlaybackRateScalar()));
	}

	// set colour tint
	if (min.m_flags.IsSet(PTXSPAWNSCALAR_ACTIVE_COLOUR_TINT))
	{
		float randRGB = g_DrawRand.GetFloat();
		float randA = g_DrawRand.GetFloat();

		Color32 colTint;
		colTint.SetRed((u8)Lerp(randRGB, (int)min.GetColourTintScalar().GetRed(), (int)max.GetColourTintScalar().GetRed()));
		colTint.SetGreen((u8)Lerp(randRGB, (int)min.GetColourTintScalar().GetGreen(), (int)max.GetColourTintScalar().GetGreen()));
		colTint.SetBlue((u8)Lerp(randRGB, (int)min.GetColourTintScalar().GetBlue(), (int)max.GetColourTintScalar().GetBlue()));
		colTint.SetAlpha((u8)Lerp(randA, (int)min.GetColourTintScalar().GetAlpha(), (int)max.GetColourTintScalar().GetAlpha()));

		SetColourTintScalar(colTint);
	}

	// set zoom
	if (min.m_flags.IsSet(PTXSPAWNSCALAR_ACTIVE_ZOOM))
	{
		SetZoomScalar(Lerp(g_DrawRand.GetFloat(), min.GetZoomScalar(), max.GetZoomScalar()));
	}
}

#if RMPTFX_EDITOR
void ptxSpawnedEffectScalars::SendMinMaxToEditor(ptxByteBuff& buff,  ptxSpawnedEffectScalars& min, ptxSpawnedEffectScalars& max)
{
	// check the min and max have the same flags set
	ptxAssert(min.m_flags.IsEqual(max.m_flags));

	// send the duration info
	buff.Write_bool(min.GetIsDurationScalarActive());
	buff.Write_float(min.GetDurationScalar());
	buff.Write_float(max.GetDurationScalar());

	// send the playback rate info
	buff.Write_bool(min.GetIsPlaybackRateScalarActive());
	buff.Write_float(min.GetPlaybackRateScalar());
	buff.Write_float(max.GetPlaybackRateScalar());

	// send the colour tint info
	buff.Write_bool(min.GetIsColourTintScalarActive());
	buff.Write_u32(min.GetColourTintScalar().GetColor());
	buff.Write_u32(max.GetColourTintScalar().GetColor());

	// send the zoom info
	buff.Write_bool(min.GetIsZoomScalarActive());
	buff.Write_float(min.GetZoomScalar());
	buff.Write_float(max.GetZoomScalar());
}
#endif

#if RMPTFX_EDITOR
void ptxSpawnedEffectScalars::ReceiveMinMaxFromEditor(const ptxByteBuff& buff, ptxSpawnedEffectScalars& min, ptxSpawnedEffectScalars& max)
{
	// check the min and max have the same flags set
	ptxAssert(min.m_flags.IsEqual(max.m_flags));

	// read the duration info
	bool duration = buff.Read_bool();
	float durationMin = buff.Read_float();
	float durationMax = buff.Read_float();

	// read the playback rate info
	bool playbackSpeed = buff.Read_bool();
	float playbackSpeedMin = buff.Read_float();
	float playbackSpeedMax = buff.Read_float();

	// read the colour tint info
	bool colorTint = buff.Read_bool();
	Color32 colorTintMin = Color32(buff.Read_u32());
	Color32 colorTintMax = Color32(buff.Read_u32());

	// read the zoom info
	bool zoom = buff.Read_bool();
	float zoomMin = buff.Read_float();
	float zoomMax = buff.Read_float();

	// set the state from the read info
	min.m_flags.Reset();
	max.m_flags.Reset();

	if (duration)
	{
		min.SetDurationScalar(durationMin);
		max.SetDurationScalar(durationMax);
	}

	if (playbackSpeed)
	{
		min.SetPlaybackRateScalar(playbackSpeedMin);
		max.SetPlaybackRateScalar(playbackSpeedMax);
	}

	if (colorTint)
	{
		min.SetColourTintScalar(colorTintMin);
		max.SetColourTintScalar(colorTintMax);
	}

	if (zoom)
	{
		min.SetZoomScalar(zoomMin);
		max.SetZoomScalar(zoomMax);
	}
}
#endif



ptxEffectSpawner::ptxEffectSpawner()
{
	m_pEffectRule = NULL;
	m_triggerInfo = -1.0f;
	m_inheritsPointLife = false;
	m_tracksPointPos = false;
	m_tracksPointDir = false;
	m_tracksPointNegDir = false;
};

ptxEffectSpawner::ptxEffectSpawner(datResource& rsc)
: m_spawnedEffectScalarsMin(rsc)
, m_spawnedEffectScalarsMax(rsc)
, m_pEffectRule(rsc)
, m_name(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxEffectSpawner_num++;
#endif
}

#if __DECLARESTRUCT
void ptxEffectSpawner::DeclareStruct(datTypeStruct& s)
{
	pgBase::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxEffectSpawner, pgBase)
		SSTRUCT_IGNORE(ptxEffectSpawner, m_pad)
		SSTRUCT_FIELD(ptxEffectSpawner, m_spawnedEffectScalarsMin)
		SSTRUCT_FIELD(ptxEffectSpawner, m_spawnedEffectScalarsMax)
		SSTRUCT_FIELD(ptxEffectSpawner, m_pEffectRule)
		SSTRUCT_FIELD(ptxEffectSpawner, m_name)
		SSTRUCT_FIELD(ptxEffectSpawner, m_triggerInfo)
		SSTRUCT_FIELD(ptxEffectSpawner, m_inheritsPointLife)
		SSTRUCT_FIELD(ptxEffectSpawner, m_tracksPointPos)
		SSTRUCT_FIELD(ptxEffectSpawner, m_tracksPointDir)
		SSTRUCT_FIELD(ptxEffectSpawner, m_tracksPointNegDir)
		SSTRUCT_END(ptxEffectSpawner)
}
#endif

IMPLEMENT_PLACE(ptxEffectSpawner);

void ptxEffectSpawner::Trigger(ptxPointItem* pPointItem, ptxEffectInst* pEffectInst, bool isColnTrigger)
{
	if (m_pEffectRule)
	{
		if (ptxVerifyf(pEffectInst->IsSpawnedEffect()==false, "too many levels of spawning detected - %s has spawned %s which is now trying to spawn %s - please add a bug for DefaultVfx", pEffectInst->GetSpawningEffectInst()->GetEffectRule()->GetName(), pEffectInst->GetEffectRule()->GetName(), m_pEffectRule->GetName()))
		{
			ptxPointSB* pPointSB = pPointItem->GetDataSB();
			ptxPointDB* pPointDB = pPointItem->GetDataDB();

			// effects spawned due to collision use the trigger info as a speed threshold
			if (isColnTrigger)
			{
				if (pPointSB->GetColnSpeed()<m_triggerInfo)
				{
					return;
				}
			}

			ptxEffectInst* pEffectInstToSpawn = RMPTFXMGR.GetEffectInst(m_pEffectRule);
			if (pEffectInstToSpawn)
			{
				ptxAssertf(pEffectInstToSpawn->GetEffectRule()->GetIsInfinitelyLooped()==false, "trying to spawn an infinitely looping effect (%s)", pEffectInstToSpawn->GetEffectRule()->GetName());

				// check if we definitely want to create the spawned effect 
				if (RMPTFXMGR.GetEffectInstSpawnFunctor() && RMPTFXMGR.GetEffectInstSpawnFunctor()(pEffectInst, pEffectInstToSpawn)==false)
				{
					// we don't - finish the effect instance
					pEffectInst->Finish(false);
					return;
				}

				// set up the pointers to the point item and effect spawner
				pEffectInstToSpawn->SetSpawningPointItem(pPointItem);
				pEffectInstToSpawn->SetSpawningEffectInst(pEffectInst);
				pEffectInstToSpawn->SetEffectSpawner(this);

				// set the random spawned effect scalars
				pEffectInstToSpawn->SetSpawnedEffectScalars(m_spawnedEffectScalarsMin, m_spawnedEffectScalarsMax);

				// calculate the matrix of the new effect
				Mat34V vMtx = Mat34V(V_IDENTITY);

				// check if we should be setting the point's direction
				if (m_tracksPointDir || m_tracksPointNegDir || isColnTrigger)
				{
					// init the forward and up vectors
					Vec3V vUp = Vec3V(V_Z_AXIS_WZERO);
					Vec3V vForward = Vec3V(V_X_AXIS_WZERO);

					// update the forward vector
					if (m_tracksPointDir || m_tracksPointNegDir)
					{
						// orient the z axis of the matrix to the point's velocity
						Vec3V vCurrPointVel = pPointDB->GetCurrVel();
						float currPointSpeed = Mag(vCurrPointVel).Getf();
						//if (ptxVerifyf(currPointSpeed>0.000001f, "trying to track direction on a particle with low/zero velocity (%s) (%s)", pEffectInst->GetEffectRule()->GetName(), pEffectInstToSpawn->GetEffectRule()->GetName()))
						if (currPointSpeed>0.000001f)
						{
							vForward = Normalize(vCurrPointVel);
						}
					}
					else
					{
						// this must be a collision trigger that isn't tracking the point direction - use the collision normal
						ptxAssertf(isColnTrigger, "trying to trigger a spawned effect that doesn't track direction and isn't a collision trigger");
						vForward = pPointSB->GetColnNormal();
					}

					// change the up vector if forward is too close to it 
					ScalarV vDot = Abs(Dot(vForward, vUp));
					if (IsGreaterThanOrEqualAll(vDot, ScalarV(0.995f)))
					{
						vUp = Vec3V(V_Y_AXIS_WZERO);
					}

					// deal with negative directions
					if (m_tracksPointNegDir)
					{
						vForward = -vForward;
						vUp = -vUp;
					}

					// create the matrix from the vectors
					LookDown(vMtx, vForward, vUp);
				}

				// set the new effect position
				if (isColnTrigger)
				{
					// collision effects should use the collision position
					vMtx.SetCol3(pPointSB->GetColnPos());
				}
				else
				{
					// non collision effects should use the current point position
					vMtx.SetCol3(pPointDB->GetCurrPos());
				}

				// set the new effect matrix
				pEffectInstToSpawn->SetBaseMtx(vMtx);

				// copy collision data
				if (pEffectInst->HasCollisionSet())
				{
					// only copy if the spawned effect needs it
					if (pEffectInstToSpawn->GetEffectRule()->HasBehaviour("ptxu_Collision"))
					{
						if(pEffectInst->GetCollisionSet() != pEffectInst->GetInternalCollisionSet())
						{
							pEffectInstToSpawn->SetCollisionSet(pEffectInst->GetCollisionSet());
						}
						else
						{
							pEffectInstToSpawn->CopyCollisionSet(pEffectInst->GetCollisionSet());
						}
					}
				}

				// copy over data from the parent effect
				pEffectInstToSpawn->SetColourTint(pEffectInst->GetColourTint());
				pEffectInstToSpawn->SetAlphaTint(pEffectInst->GetAlphaTint());
				pEffectInstToSpawn->SetLODScalar(pEffectInst->GetLODScalar());
				pEffectInstToSpawn->SetUserZoom(pEffectInst->GetUserZoom());

				// start the spawned effect
				pEffectInstToSpawn->Start();

				// copy visibility flags (this needs done better)
				if (pEffectInst->GetFlag(PTXEFFECTFLAG_USER9))
				{
					pEffectInstToSpawn->SetFlag(PTXEFFECTFLAG_USER9);
				}

				if (pEffectInst->GetFlag(PTXEFFECTFLAG_USER10))
				{
					pEffectInstToSpawn->SetFlag(PTXEFFECTFLAG_USER10);
				}

				// override with the point's remaining life if requested
				if (m_inheritsPointLife)
				{
					float pointDuration = 1.0f/pPointSB->GetOOLife();
					float currPointLifeRatio = pPointDB->GetLifeRatio();
					float pointDurationRemaining = pointDuration * (1.0f-currPointLifeRatio);
					pEffectInstToSpawn->SetOOLife(1.0f/pointDurationRemaining);
				}
			}
			else
			{
				ptxWarningf("effect %s not found!", m_pEffectRule->GetName());
			}
		}
	}
}

#if RMPTFX_XML_LOADING
void ptxEffectSpawner::ResolveReferences()
{
	if (m_name.c_str())
	{
		m_pEffectRule = RMPTFXMGR.LoadEffectRule(m_name);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEffectSpawner::SendToEditor(ptxByteBuff& buff)
{
	buff.Write_const_char(m_pEffectRule ? m_pEffectRule->GetName() : NULL);
	buff.Write_float(m_triggerInfo);
	buff.Write_bool(m_inheritsPointLife);
	buff.Write_bool(m_tracksPointPos);
	buff.Write_bool(m_tracksPointDir);
	buff.Write_bool(m_tracksPointNegDir);
	ptxSpawnedEffectScalars::SendMinMaxToEditor(buff, m_spawnedEffectScalarsMin, m_spawnedEffectScalarsMax);
}
#endif

#if RMPTFX_EDITOR
void ptxEffectSpawner::ReceiveFromEditor(const ptxByteBuff& buff)
{
	m_name = buff.Read_const_char();
	m_pEffectRule = m_name.c_str() ? RMPTFXMGR.GetEffectRule(atHashWithStringNotFinal(m_name.c_str())) : NULL;
	m_triggerInfo = buff.Read_float();
	m_inheritsPointLife = buff.Read_bool();
	m_tracksPointPos = buff.Read_bool();
	m_tracksPointDir = buff.Read_bool();
	m_tracksPointNegDir = buff.Read_bool();
	ptxSpawnedEffectScalars::ReceiveMinMaxFromEditor(buff, m_spawnedEffectScalarsMin, m_spawnedEffectScalarsMax);
}
#endif 










