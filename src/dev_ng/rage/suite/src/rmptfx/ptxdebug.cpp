// 
// rmptfx/ptxdebug.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxdebug.h"

// includes (rmptfx)
#include "rmptfx/ptxchannel.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxd_drawmodel.h"
#include "rmptfx/ptxd_drawsprite.h"
#include "rmptfx/ptxd_drawtrail.h"
#include "rmptfx/ptxu_acceleration.h"
#include "rmptfx/ptxu_age.h"
#include "rmptfx/ptxu_animatetexture.h"
#include "rmptfx/ptxu_attractor.h"
#include "rmptfx/ptxu_collision.h"
#include "rmptfx/ptxu_colour.h"
#include "rmptfx/ptxu_dampening.h"
#include "rmptfx/ptxu_matrixweight.h"
#include "rmptfx/ptxu_noise.h"
#include "rmptfx/ptxu_pause.h"
#include "rmptfx/ptxu_rotation.h"
#include "rmptfx/ptxu_size.h"
#include "rmptfx/ptxu_velocity.h"
#include "rmptfx/ptxu_wind.h"


// includes (rage)
#include "bank/bkmgr.h"


// optimsations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


// profile stats
PF_PAGE(rmptfxPage, "RMPTFX");

PF_GROUP_OFF(rmptfxCounters);
PF_LINK(rmptfxPage, rmptfxCounters);
PF_COUNTER(CounterPointsActive, rmptfxCounters);
PF_COUNTER(CounterEffectInsts, rmptfxCounters);
PF_COUNTER(CounterEffectInstsActive, rmptfxCounters);
PF_COUNTER(CounterEffectInstsCulled, rmptfxCounters);
PF_COUNTER(CounterEmitterInstsTotal, rmptfxCounters);
PF_COUNTER(CounterEmitterInstsWontUpdate, rmptfxCounters);
PF_COUNTER(CounterEmitterInstsEarlyWait, rmptfxCounters);
PF_COUNTER(CounterEmitterInstsLateWait, rmptfxCounters);

PF_GROUP(rmptfxTimers);
PF_LINK(rmptfxPage, rmptfxTimers); 
PF_TIMER(TimerUpdate, rmptfxTimers);
PF_TIMER(TimerUpdateSetup, rmptfxTimers);
PF_TIMER(TimerUpdateSetupSort, rmptfxTimers);
PF_TIMER(TimerUpdateParallel, rmptfxTimers);
PF_TIMER(TimerUpdateFinalize, rmptfxTimers);
PF_TIMER(TimerUpdateParallel_WaitOnChildTasks, rmptfxTimers);
PF_TIMER(TimerUpdate_WaitOnCallbackEffects, rmptfxTimers);
PF_TIMER(TimerExt_WaitForUpdateToFinish, rmptfxTimers);
PF_TIMER(TimerUpdateLocal_ParallelAndFinalize, rmptfxTimers);
PF_TIMER(TimerUpdateCallback_ParallelAndFinalize, rmptfxTimers);
PF_TIMER(TimerUpdateLocal_WaitOnEffectUpdateComplete, rmptfxTimers);
PF_TIMER(TimerDraw, rmptfxTimers);



#if RMPTFX_BANK

PARAM(ptfxOutputEffectInstPlayStateDebug, "Outputs the state changes in particle effect instances (init, start, stop, etc.)");

// static variables
bkBank* ptxDebug::sm_pBank;		

// info
float ptxDebug::sm_version = RMPTFX_VERSION;	

// fx lists
bool ptxDebug::sm_drawFxlistReferences = false;

// global 
bool ptxDebug::sm_enableDebugDrawing = false;
bool ptxDebug::sm_enableExpensiveOneShotSubFrameCalcs = false;
bool ptxDebug::sm_disableInitPointAccn = false;
bool ptxDebug::sm_disableInitPointAge = false;
bool ptxDebug::sm_disableInitPointAnimTex = false;
bool ptxDebug::sm_disableInitPointAttractor = false;
bool ptxDebug::sm_disableInitPointColn = false;
bool ptxDebug::sm_disableInitPointColour = false;
bool ptxDebug::sm_disableInitPointDampening = false;
bool ptxDebug::sm_disableInitPointMtxWeight = false;
bool ptxDebug::sm_disableInitPointNoise = false;
bool ptxDebug::sm_disableInitPointPause = false;
bool ptxDebug::sm_disableInitPointRotation = false;
bool ptxDebug::sm_disableInitPointSize = false;
bool ptxDebug::sm_disableInitPointVelocity = false;
bool ptxDebug::sm_disableInitPointWind = false;
bool ptxDebug::sm_disableInitPointDecal = false;
bool ptxDebug::sm_disableInitPointDecalPool = false;
bool ptxDebug::sm_disableInitPointDecalTrail = false;
bool ptxDebug::sm_disableInitPointLight = false;
bool ptxDebug::sm_disableInitPointLiquid = false;
bool ptxDebug::sm_disableInitPointRiver = false;
bool ptxDebug::sm_disableInitPointZCull = false;
float ptxDebug::sm_overrideHighQualityEvo = -0.01f;
bool ptxDebug::sm_preUpdateTimeLimitActive = false;
float ptxDebug::sm_preUpdateTimeLimit = 0.25f;
bool ptxDebug::sm_performParallelFinalize = true;
bool ptxDebug::sm_disableShadowCullStateCalcs = false;

// effect instances	
char ptxDebug::sm_effectInstNameFilter[64];
bool ptxDebug::sm_drawEffectInstInfo = false;	
bool ptxDebug::sm_drawEffectInstNames = false;
bool ptxDebug::sm_drawEffectInstAddress = false;
bool ptxDebug::sm_drawEffectInstFxList = false;
bool ptxDebug::sm_drawEffectInstLifeRatios = false;
bool ptxDebug::sm_drawEffectInstNumPoints = false;
bool ptxDebug::sm_drawEffectInstEvolutions = false;
bool ptxDebug::sm_drawEffectInstDistToCam = false;
bool ptxDebug::sm_drawEffectInstMaxDuration = false;
bool ptxDebug::sm_drawEffectInstCameraBias = false;
bool ptxDebug::sm_drawEffectInstColnPolys = false;
bool ptxDebug::sm_drawEffectInstPositionTrace = false;
bool ptxDebug::sm_drawEffectInstCallbacksHighlighted = false;
bool ptxDebug::sm_drawEffectInstQualityHighlighted = false;
bool ptxDebug::sm_outputEffectInstPlayStateDebug = false;
bool ptxDebug::sm_outputEffectInstUpdateProfile = false;

// emitter instances
char ptxDebug::sm_emitterInstNameFilter[64];	
int ptxDebug::sm_emitterInstEventIdFilter = -1;
bool ptxDebug::sm_drawEmitterInstBoundBoxes = false;	
bool ptxDebug::sm_drawEmitterInstCreationDomain = false;	
bool ptxDebug::sm_drawEmitterInstTargetDomain = false;	
bool ptxDebug::sm_drawEmitterInstAttractorDomain = false;	
bool ptxDebug::sm_outputEmitterInstDrawStateDebug = false;
bool ptxDebug::sm_outputEffectInstSpawningProfile = false;

// trails
bool ptxDebug::sm_drawTrails = false;	
bool ptxDebug::sm_drawTrailsWireframe = false;
bool ptxDebug::sm_simpleTrails = false;
int ptxDebug::sm_numDebugTrailPoints = 0;
ptxDebugTrailPoint ptxDebug::sm_debugTrailPoints[PTXDEBUG_MAX_DEBUG_TRAIL_POINTS];	
bool ptxDebug::sm_debugTrailTangentColours = false;
int ptxDebug::sm_debugTrailTangentSteps = 1;

// points
bool ptxDebug::sm_drawPoints = false;	
bool ptxDebug::sm_drawPointAlphas = false;
bool ptxDebug::sm_drawPointCollisions = false;
int ptxDebug::sm_numPointCollisionFrames = 1;
float ptxDebug::sm_pointAlphaCullThreshold = 0.0f;

// misc
float ptxDebug::sm_debugAxisScale = 1.0f;
bool ptxDebug::sm_disableInvertedAxes = false;
bool ptxDebug::sm_toggleQuickDebug = false;


#if RMPTFX_EDITOR
//RMPTFX editor interface
bool ptxDebug::sm_rmptfxListen = false;
#endif

// profiling
int ptxDebug::sm_profilingFrameCount = 0;
float ptxDebug::sm_accumKeyframeQueryTimeMs = 0.0f;
int ptxDebug::sm_accumKeyframeQueryCount = 0;
float ptxDebug::sm_averageKeyframeQueryTimeMs = 0.0f;
int ptxDebug::sm_averageKeyframeQueryCount = 0;

// fxlist info
ptxDebugFxListInfo ptxDebug::sm_debugFxListInfo;

// reporting
char ptxDebug::sm_reportFxListName[64];
char ptxDebug::sm_reportEffectRuleName[64];
char ptxDebug::sm_reportEmitterRuleName[64];
char ptxDebug::sm_reportParticleRuleName[64];
char ptxDebug::sm_reportTextureName[64];
char ptxDebug::sm_reportModelName[64];

bool ptxDebug::sm_bEnableShaderDebugOverrideAlpha = false;

// code
void ptxDebugFxListInfo::Clear()
{
	ptxFxList_num = 0;
	ptxEffectRule_num = 0;
	ptxTimeLine_num = 0;
	ptxEvent_num = 0;
	ptxEventEmitter_num = 0;
	ptxEvolutionList_num = 0;
	ptxEvolution_num = 0;
	ptxEvolvedKeyframeProp_num = 0;
	ptxEvolvedKeyframe_num = 0;
	ptxEffectSpawner_num = 0;
	ptxSpawnedEffectScalars_num = 0;
	ptxEmitterRule_num = 0;
	ptxDomainObj_num = 0;
	ptxDomain_num = 0;
	ptxDomainBox_num = 0;
	ptxDomainSphere_num = 0;
	ptxDomainCylinder_num = 0;
	ptxDomainAttractor_num = 0;
	ptxParticleRule_num = 0;
	ptxBehaviour_num = 0;
	ptxBiasLink_num = 0;
	ptxRenderState_num = 0;
	ptxTechniqueDesc_num = 0;
	ptxShaderInst_num = 0;
	ptxShaderVar_num = 0;
	ptxShaderVarVector_num = 0;
	ptxShaderVarKeyframe_num = 0;
	ptxShaderVarTexture_num = 0;
	ptxDrawable_num = 0;
	ptxDrawableEntry_num = 0;
	ptxKeyframePropList_num = 0;
	ptxKeyframeProp_num = 0;
	ptxKeyframe_num = 0;
#if PTXKEYFRAME_VERSION==0 || PTXKEYFRAME_VERSION==1
	ptxKeyframeEntry_num = 0;
#endif
	ptxd_Model_num = 0;
	ptxd_Sprite_num = 0;
	ptxd_Trail_num = 0;
	ptxu_Acceleration_num = 0;
	ptxu_Age_num = 0;
	ptxu_AnimateTexture_num = 0;
	ptxu_Attractor_num = 0;
	ptxu_Collision_num = 0;
	ptxu_Colour_num = 0;
	ptxu_Dampening_num = 0;
	ptxu_MatrixWeight_num = 0;
	ptxu_Noise_num = 0;
	ptxu_Pause_num = 0;
	ptxu_Rotation_num = 0;
	ptxu_Size_num = 0;
	ptxu_Velocity_num = 0;
	ptxu_Wind_num = 0;
}

void ptxDebugFxListInfo::Output(const ptxFxList* pFxList)
{
	// get the sizes of the particle structure used in the resourcing process
	static int ptxFxList_size						= sizeof(ptxFxList);
	static int ptxEffectRule_size					= sizeof(ptxEffectRule);
	static int ptxTimeLine_size						= sizeof(ptxTimeLine);
	static int ptxEvent_size						= sizeof(ptxEvent);
	static int ptxEventEmitter_size					= sizeof(ptxEventEmitter);
	static int ptxEvolutionList_size				= sizeof(ptxEvolutionList);
	static int ptxEvolution_size					= sizeof(ptxEvolution);
	static int ptxEvolvedKeyframeProp_size			= sizeof(ptxEvolvedKeyframeProp);
	static int ptxEvolvedKeyframe_size				= sizeof(ptxEvolvedKeyframe);
	static int ptxEffectSpawner_size				= sizeof(ptxEffectSpawner);
	static int ptxSpawnedEffectScalars_size			= sizeof(ptxSpawnedEffectScalars);
	static int ptxEmitterRule_size					= sizeof(ptxEmitterRule);
	static int ptxDomainObj_size					= sizeof(ptxDomainObj);
	static int ptxDomain_size						= sizeof(ptxDomain);
	static int ptxDomainBox_size					= sizeof(ptxDomainBox);
	static int ptxDomainSphere_size					= sizeof(ptxDomainSphere);
	static int ptxDomainCylinder_size				= sizeof(ptxDomainCylinder);
	static int ptxDomainAttractor_size				= sizeof(ptxDomainAttractor);
	static int ptxParticleRule_size					= sizeof(ptxParticleRule);
	static int ptxBehaviour_size					= sizeof(ptxBehaviour);
	static int ptxBiasLink_size						= sizeof(ptxBiasLink);
	static int ptxRenderState_size					= sizeof(ptxRenderState);
	static int ptxTechniqueDesc_size				= sizeof(ptxTechniqueDesc);
	static int ptxShaderInst_size					= sizeof(ptxShaderInst);
	static int ptxShaderVar_size					= sizeof(ptxShaderVar);
	static int ptxShaderVarVector_size				= sizeof(ptxShaderVarVector);
	static int ptxShaderVarKeyframe_size			= sizeof(ptxShaderVarKeyframe);
	static int ptxShaderVarTexture_size				= sizeof(ptxShaderVarTexture);
	static int ptxDrawable_size						= sizeof(ptxDrawable);
	static int ptxDrawableEntry_size				= sizeof(ptxDrawableEntry);
	static int ptxKeyframePropList_size				= sizeof(ptxKeyframePropList);
	static int ptxKeyframeProp_size					= sizeof(ptxKeyframeProp);
	static int ptxKeyframe_size						= sizeof(ptxKeyframe);
#if PTXKEYFRAME_VERSION==0 || PTXKEYFRAME_VERSION==1
	static int ptxKeyframeEntry_size				= sizeof(ptxKeyframeEntry);
#endif
	static int ptxd_Model_size						= sizeof(ptxd_Model);
	static int ptxd_Sprite_size						= sizeof(ptxd_Sprite);
	static int ptxd_Trail_size						= sizeof(ptxd_Trail);
	static int ptxu_Acceleration_size				= sizeof(ptxu_Acceleration);
	static int ptxu_Age_size						= sizeof(ptxu_Age);
	static int ptxu_AnimateTexture_size				= sizeof(ptxu_AnimateTexture);
	static int ptxu_Attractor_size					= sizeof(ptxu_Attractor);
	static int ptxu_Collision_size					= sizeof(ptxu_Collision);
	static int ptxu_Colour_size						= sizeof(ptxu_Colour);
	static int ptxu_Dampening_size					= sizeof(ptxu_Dampening);
	static int ptxu_MatrixWeight_size				= sizeof(ptxu_MatrixWeight);
	static int ptxu_Noise_size						= sizeof(ptxu_Noise);
	static int ptxu_Pause_size						= sizeof(ptxu_Pause);
	static int ptxu_Rotation_size					= sizeof(ptxu_Rotation);
	static int ptxu_Size_size						= sizeof(ptxu_Size);
	static int ptxu_Velocity_size					= sizeof(ptxu_Velocity);
	static int ptxu_Wind_size						= sizeof(ptxu_Wind);

	// adjust these sizes if the classes contain any of the other classes
	static int ptxFxList_sizeAdj					= ptxFxList_size;
	static int ptxEffectRule_sizeAdj				= ptxEffectRule_size - ptxTimeLine_size - (ptxKeyframeProp_size*6) - ptxKeyframePropList_size;
	static int ptxTimeLine_sizeAdj					= ptxTimeLine_size;
	static int ptxEvent_sizeAdj						= ptxEvent_size;
	static int ptxEventEmitter_sizeAdj				= ptxEventEmitter_size;
	static int ptxEvolutionList_sizeAdj				= ptxEvolutionList_size;
	static int ptxEvolution_sizeAdj					= ptxEvolution_size;
	static int ptxEvolvedKeyframeProp_sizeAdj		= ptxEvolvedKeyframeProp_size;
	static int ptxEvolvedKeyframe_sizeAdj			= ptxEvolvedKeyframe_size - ptxKeyframe_size;
	static int ptxEffectSpawner_sizeAdj				= ptxEffectSpawner_size - (ptxSpawnedEffectScalars_size*2);
	static int ptxSpawnedEffectScalars_sizeAdj		= ptxSpawnedEffectScalars_size;
	static int ptxEmitterRule_sizeAdj				= ptxEmitterRule_size - (ptxDomainObj_size*3) - (ptxKeyframeProp_size*10) - ptxKeyframePropList_size;
	static int ptxDomainObj_sizeAdj					= ptxDomainObj_size;
	static int ptxDomain_sizeAdj					= ptxDomain_size - (ptxKeyframeProp_size*4) - ptxKeyframePropList_size;
	static int ptxDomainBox_sizeAdj					= ptxDomainBox_size - ptxDomain_size;
	static int ptxDomainSphere_sizeAdj				= ptxDomainSphere_size - ptxDomain_size;
	static int ptxDomainCylinder_sizeAdj			= ptxDomainCylinder_size - ptxDomain_size;
	static int ptxDomainAttractor_sizeAdj			= ptxDomainAttractor_size - ptxDomain_size;
	static int ptxParticleRule_sizeAdj				= ptxParticleRule_size - (ptxEffectSpawner_size*2) - ptxRenderState_size - ptxShaderInst_size;
	static int ptxBehaviour_sizeAdj					= ptxBehaviour_size - ptxKeyframePropList_size;
	static int ptxBiasLink_sizeAdj					= ptxBiasLink_size;
	static int ptxRenderState_sizeAdj				= ptxRenderState_size;
	static int ptxTechniqueDesc_sizeAdj				= ptxTechniqueDesc_size;
	static int ptxShaderInst_sizeAdj				= ptxShaderInst_size - ptxTechniqueDesc_size;
	static int ptxShaderVar_sizeAdj					= ptxShaderVar_size;
	static int ptxShaderVarVector_sizeAdj			= ptxShaderVarVector_size - ptxShaderVar_size;
	static int ptxShaderVarKeyframe_sizeAdj			= ptxShaderVarKeyframe_size - ptxShaderVar_size - ptxKeyframe_size;
	static int ptxShaderVarTexture_sizeAdj			= ptxShaderVarTexture_size - ptxShaderVar_size;
	static int ptxDrawable_sizeAdj					= ptxDrawable_size;
	static int ptxDrawableEntry_sizeAdj				= ptxDrawableEntry_size;
	static int ptxKeyframePropList_sizeAdj			= ptxKeyframePropList_size;
	static int ptxKeyframeProp_sizeAdj				= ptxKeyframeProp_size - ptxKeyframe_size;
#if PTXKEYFRAME_VERSION==0
	static int ptxKeyframe_sizeAdj					= ptxKeyframe_size - ptxKeyframeEntry_size;
	static int ptxKeyframeEntry_sizeAdj				= ptxKeyframeEntry_size;
#elif PTXKEYFRAME_VERSION==1
	static int ptxKeyframe_sizeAdj					= ptxKeyframe_size;
	static int ptxKeyframeEntry_sizeAdj				= ptxKeyframeEntry_size;
#else
	static int ptxKeyframe_sizeAdj					= ptxKeyframe_size;
#endif
	static int ptxd_Model_sizeAdj					= ptxd_Model_size;
	static int ptxd_Sprite_sizeAdj					= ptxd_Sprite_size;
	static int ptxd_Trail_sizeAdj					= ptxd_Trail_size - ptxKeyframeProp_size;
	static int ptxu_Acceleration_sizeAdj			= ptxu_Acceleration_size - (ptxKeyframeProp_size*2);
	static int ptxu_Age_sizeAdj						= ptxu_Age_size;
	static int ptxu_AnimateTexture_sizeAdj			= ptxu_AnimateTexture_size - ptxKeyframeProp_size;
	static int ptxu_Attractor_sizeAdj				= ptxu_Attractor_size - ptxKeyframeProp_size;
	static int ptxu_Collision_sizeAdj				= ptxu_Collision_size - (ptxKeyframeProp_size*2);
	static int ptxu_Colour_sizeAdj					= ptxu_Colour_size - (ptxKeyframeProp_size*3);
	static int ptxu_Dampening_sizeAdj				= ptxu_Dampening_size - (ptxKeyframeProp_size*2);
	static int ptxu_MatrixWeight_sizeAdj			= ptxu_MatrixWeight_size - ptxKeyframeProp_size;
	static int ptxu_Noise_sizeAdj					= ptxu_Noise_size - (ptxKeyframeProp_size*4);
	static int ptxu_Pause_sizeAdj					= ptxu_Pause_size;
	static int ptxu_Rotation_sizeAdj				= ptxu_Rotation_size - (ptxKeyframeProp_size*4);
	static int ptxu_Size_sizeAdj					= ptxu_Size_size - (ptxKeyframeProp_size*4);
	static int ptxu_Velocity_sizeAdj				= ptxu_Velocity_size;
	static int ptxu_Wind_sizeAdj					= ptxu_Wind_size - ptxKeyframeProp_size;

	// calculate the amount of memory exclusively in each class
	int ptxFxList_mem								= ptxFxList_num					* ptxFxList_sizeAdj;
	int ptxEffectRule_mem							= ptxEffectRule_num				* ptxEffectRule_sizeAdj;
	int ptxTimeLine_mem								= ptxTimeLine_num				* ptxTimeLine_sizeAdj;
	int ptxEvent_mem								= ptxEvent_num					* ptxEvent_sizeAdj;
	int ptxEventEmitter_mem							= ptxEventEmitter_num			* ptxEventEmitter_sizeAdj;
	int ptxEvolutionList_mem						= ptxEvolutionList_num			* ptxEvolutionList_sizeAdj;
	int ptxEvolution_mem							= ptxEvolution_num				* ptxEvolution_sizeAdj;
	int ptxEvolvedKeyframeProp_mem					= ptxEvolvedKeyframeProp_num	* ptxEvolvedKeyframeProp_sizeAdj;
	int ptxEvolvedKeyframe_mem						= ptxEvolvedKeyframe_num		* ptxEvolvedKeyframe_sizeAdj;
	int ptxEffectSpawner_mem						= ptxEffectSpawner_num			* ptxEffectSpawner_sizeAdj;
	int ptxSpawnedEffectScalars_mem					= ptxSpawnedEffectScalars_num	* ptxSpawnedEffectScalars_sizeAdj;
	int ptxEmitterRule_mem							= ptxEmitterRule_num			* ptxEmitterRule_sizeAdj;
	int ptxDomainObj_mem							= ptxDomainObj_num				* ptxDomainObj_sizeAdj;
	int ptxDomain_mem								= ptxDomain_num					* ptxDomain_sizeAdj;
	int ptxDomainBox_mem							= ptxDomainBox_num				* ptxDomainBox_sizeAdj;
	int ptxDomainSphere_mem							= ptxDomainSphere_num			* ptxDomainSphere_sizeAdj;
	int ptxDomainCylinder_mem						= ptxDomainCylinder_num			* ptxDomainCylinder_sizeAdj;
	int ptxDomainAttractor_mem						= ptxDomainAttractor_num		* ptxDomainAttractor_sizeAdj;
	int ptxParticleRule_mem							= ptxParticleRule_num			* ptxParticleRule_sizeAdj;
	int ptxBehaviour_mem							= ptxBehaviour_num				* ptxBehaviour_sizeAdj;
	int ptxBiasLink_mem								= ptxBiasLink_num				* ptxBiasLink_sizeAdj;
	int ptxRenderState_mem							= ptxRenderState_num			* ptxRenderState_sizeAdj;
	int ptxTechniqueDesc_mem						= ptxTechniqueDesc_num			* ptxTechniqueDesc_sizeAdj;
	int ptxShaderInst_mem							= ptxShaderInst_num				* ptxShaderInst_sizeAdj;
	int ptxShaderVar_mem							= ptxShaderVar_num				* ptxShaderVar_sizeAdj;
	int ptxShaderVarVector_mem						= ptxShaderVarVector_num		* ptxShaderVarVector_sizeAdj;
	int ptxShaderVarKeyframe_mem					= ptxShaderVarKeyframe_num		* ptxShaderVarKeyframe_sizeAdj;
	int ptxShaderVarTexture_mem						= ptxShaderVarTexture_num		* ptxShaderVarTexture_sizeAdj;
	int ptxDrawable_mem								= ptxDrawable_num				* ptxDrawable_sizeAdj;
	int ptxDrawableEntry_mem						= ptxDrawableEntry_num			* ptxDrawableEntry_sizeAdj;
	int ptxKeyframePropList_mem						= ptxKeyframePropList_num		* ptxKeyframePropList_sizeAdj;
	int ptxKeyframeProp_mem							= ptxKeyframeProp_num			* ptxKeyframeProp_sizeAdj;
	int ptxKeyframe_mem								= ptxKeyframe_num				* ptxKeyframe_sizeAdj;
#if PTXKEYFRAME_VERSION==0 || PTXKEYFRAME_VERSION==1
	int ptxKeyframeEntry_mem						= ptxKeyframeEntry_num			* ptxKeyframeEntry_sizeAdj;
#endif
	int ptxd_Model_mem								= ptxd_Model_num				* ptxd_Model_sizeAdj;
	int ptxd_Sprite_mem								= ptxd_Sprite_num				* ptxd_Sprite_sizeAdj;
	int ptxd_Trail_mem								= ptxd_Trail_num				* ptxd_Trail_sizeAdj;
	int ptxu_Acceleration_mem						= ptxu_Acceleration_num			* ptxu_Acceleration_sizeAdj;
	int ptxu_Age_mem								= ptxu_Age_num					* ptxu_Age_sizeAdj;
	int ptxu_AnimateTexture_mem						= ptxu_AnimateTexture_num		* ptxu_AnimateTexture_sizeAdj;
	int ptxu_Attractor_mem							= ptxu_Attractor_num			* ptxu_Attractor_sizeAdj;
	int ptxu_Collision_mem							= ptxu_Collision_num			* ptxu_Collision_sizeAdj;
	int ptxu_Colour_mem								= ptxu_Colour_num				* ptxu_Colour_sizeAdj;
	int ptxu_Dampening_mem							= ptxu_Dampening_num			* ptxu_Dampening_sizeAdj;
	int ptxu_MatrixWeight_mem						= ptxu_MatrixWeight_num			* ptxu_MatrixWeight_sizeAdj;
	int ptxu_Noise_mem								= ptxu_Noise_num				* ptxu_Noise_sizeAdj;
	int ptxu_Pause_mem								= ptxu_Pause_num				* ptxu_Pause_sizeAdj;
	int ptxu_Rotation_mem							= ptxu_Rotation_num				* ptxu_Rotation_sizeAdj;
	int ptxu_Size_mem								= ptxu_Size_num					* ptxu_Size_sizeAdj;
	int ptxu_Velocity_mem							= ptxu_Velocity_num				* ptxu_Velocity_sizeAdj;
	int ptxu_Wind_mem								= ptxu_Wind_num					* ptxu_Wind_sizeAdj;

	// add up the total memory
	int totalMem = 0;
	totalMem += ptxFxList_mem;
	totalMem += ptxEffectRule_mem;
	totalMem += ptxTimeLine_mem;
	totalMem += ptxEvent_mem;
	totalMem += ptxEventEmitter_mem;
	totalMem += ptxEvolutionList_mem;
	totalMem += ptxEvolution_mem;
	totalMem += ptxEvolvedKeyframeProp_mem;
	totalMem += ptxEvolvedKeyframe_mem;
	totalMem += ptxEffectSpawner_mem;
	totalMem += ptxSpawnedEffectScalars_mem;
	totalMem += ptxEmitterRule_mem;
	totalMem += ptxDomainObj_mem;
	totalMem += ptxDomain_mem;
	totalMem += ptxDomainBox_mem;
	totalMem += ptxDomainSphere_mem;
	totalMem += ptxDomainCylinder_mem;
	totalMem += ptxDomainAttractor_mem;
	totalMem += ptxParticleRule_mem;
	totalMem += ptxBehaviour_mem;
	totalMem += ptxBiasLink_mem;
	totalMem += ptxRenderState_mem;
	totalMem += ptxTechniqueDesc_mem;
	totalMem += ptxShaderInst_mem;
	totalMem += ptxShaderVar_mem;
	totalMem += ptxShaderVarVector_mem;
	totalMem += ptxShaderVarKeyframe_mem;
	totalMem += ptxShaderVarTexture_mem;
	totalMem += ptxDrawable_mem;
	totalMem += ptxDrawableEntry_mem;
	totalMem += ptxKeyframePropList_mem;
	totalMem += ptxKeyframeProp_mem;
	totalMem += ptxKeyframe_mem;
#if PTXKEYFRAME_VERSION==0 || PTXKEYFRAME_VERSION==1
	totalMem += ptxKeyframeEntry_mem;
#endif
	totalMem += ptxd_Model_mem;
	totalMem += ptxd_Sprite_mem;
	totalMem += ptxd_Trail_mem;
	totalMem += ptxu_Acceleration_mem;
	totalMem += ptxu_Age_mem;
	totalMem += ptxu_AnimateTexture_mem;
	totalMem += ptxu_Attractor_mem;
	totalMem += ptxu_Collision_mem;
	totalMem += ptxu_Colour_mem;
	totalMem += ptxu_Dampening_mem;
	totalMem += ptxu_MatrixWeight_mem;
	totalMem += ptxu_Noise_mem;
	totalMem += ptxu_Pause_mem;
	totalMem += ptxu_Rotation_mem;
	totalMem += ptxu_Size_mem;
	totalMem += ptxu_Velocity_mem;
	totalMem += ptxu_Wind_mem;

	// output the memory info
	ptxDebugf1("%s", pFxList->GetName());
	ptxDebugf1("  ptxFxList                 - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxFxList_num,					ptxFxList_sizeAdj,					ptxFxList_size,						ptxFxList_mem/1024.0f);
	ptxDebugf1("  ptxEffectRule             - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxEffectRule_num,				ptxEffectRule_sizeAdj,				ptxEffectRule_size,					ptxEffectRule_mem/1024.0f);
	ptxDebugf1("  ptxTimeLine               - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxTimeLine_num,				ptxTimeLine_sizeAdj,				ptxTimeLine_size,					ptxTimeLine_mem/1024.0f);
	ptxDebugf1("  ptxEvent                  - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxEvent_num,					ptxEvent_sizeAdj,					ptxEvent_size,						ptxEvent_mem/1024.0f);
	ptxDebugf1("  ptxEventEmitter           - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxEventEmitter_num,			ptxEventEmitter_sizeAdj,			ptxEventEmitter_size,				ptxEventEmitter_mem/1024.0f);
	ptxDebugf1("  ptxEvolutionList          - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxEvolutionList_num,			ptxEvolutionList_sizeAdj,			ptxEvolutionList_size,				ptxEvolutionList_mem/1024.0f);
	ptxDebugf1("  ptxEvolution              - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxEvolution_num,				ptxEvolution_sizeAdj,				ptxEvolution_size,					ptxEvolution_mem/1024.0f);
	ptxDebugf1("  ptxEvolvedKeyframeProp    - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxEvolvedKeyframeProp_num,	ptxEvolvedKeyframeProp_sizeAdj,		ptxEvolvedKeyframeProp_size,		ptxEvolvedKeyframeProp_mem/1024.0f);
	ptxDebugf1("  ptxEvolvedKeyframe        - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxEvolvedKeyframe_num,		ptxEvolvedKeyframe_sizeAdj,			ptxEvolvedKeyframe_size,			ptxEvolvedKeyframe_mem/1024.0f);
	ptxDebugf1("  ptxEffectSpawner          - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxEffectSpawner_num,			ptxEffectSpawner_sizeAdj,			ptxEffectSpawner_size,				ptxEffectSpawner_mem/1024.0f);
	ptxDebugf1("  ptxSpawnedEffectScalars   - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxSpawnedEffectScalars_num,	ptxSpawnedEffectScalars_sizeAdj,	ptxSpawnedEffectScalars_size,		ptxSpawnedEffectScalars_mem/1024.0f);
	ptxDebugf1("  ptxEmitterRule            - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxEmitterRule_num,			ptxEmitterRule_sizeAdj,				ptxEmitterRule_size,				ptxEmitterRule_mem/1024.0f);
	ptxDebugf1("  ptxDomainObj              - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxDomainObj_num,				ptxDomainObj_sizeAdj,				ptxDomainObj_size,					ptxDomainObj_mem/1024.0f);
	ptxDebugf1("  ptxDomain                 - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxDomain_num,					ptxDomain_sizeAdj,					ptxDomain_size,						ptxDomain_mem/1024.0f);
	ptxDebugf1("  ptxDomainBox              - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxDomainBox_num,				ptxDomainBox_sizeAdj,				ptxDomainBox_size,					ptxDomainBox_mem/1024.0f);
	ptxDebugf1("  ptxDomainSphere           - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxDomainSphere_num,			ptxDomainSphere_sizeAdj,			ptxDomainSphere_size,				ptxDomainSphere_mem/1024.0f);
	ptxDebugf1("  ptxDomainCylinder         - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxDomainCylinder_num,			ptxDomainCylinder_sizeAdj,			ptxDomainCylinder_size,				ptxDomainCylinder_mem/1024.0f);
	ptxDebugf1("  ptxDomainAttractor        - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxDomainAttractor_num,		ptxDomainAttractor_sizeAdj,			ptxDomainAttractor_size,			ptxDomainAttractor_mem/1024.0f);
	ptxDebugf1("  ptxParticleRule           - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxParticleRule_num,			ptxParticleRule_sizeAdj,			ptxParticleRule_size,				ptxParticleRule_mem/1024.0f);
	ptxDebugf1("  ptxBehaviour              - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxBehaviour_num,				ptxBehaviour_sizeAdj,				ptxBehaviour_size,					ptxBehaviour_mem/1024.0f);
	ptxDebugf1("  ptxBiasLink               - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxBiasLink_num,				ptxBiasLink_sizeAdj,				ptxBiasLink_size,					ptxBiasLink_mem/1024.0f);
	ptxDebugf1("  ptxRenderState            - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxRenderState_num,			ptxRenderState_sizeAdj,				ptxRenderState_size,				ptxRenderState_mem/1024.0f);
	ptxDebugf1("  ptxTechniqueDesc          - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxTechniqueDesc_num,			ptxTechniqueDesc_sizeAdj,			ptxTechniqueDesc_size,				ptxTechniqueDesc_mem/1024.0f);
	ptxDebugf1("  ptxShaderInst             - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxShaderInst_num,				ptxShaderInst_sizeAdj,				ptxShaderInst_size,					ptxShaderInst_mem/1024.0f);
	ptxDebugf1("  ptxShaderVar              - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxShaderVar_num,				ptxShaderVar_sizeAdj,				ptxShaderVar_size,					ptxShaderVar_mem/1024.0f);
	ptxDebugf1("  ptxShaderVarVector        - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxShaderVarVector_num,		ptxShaderVarVector_sizeAdj,			ptxShaderVarVector_size,			ptxShaderVarVector_mem/1024.0f);
	ptxDebugf1("  ptxShaderVarKeyframe      - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxShaderVarKeyframe_num,		ptxShaderVarKeyframe_sizeAdj,		ptxShaderVarKeyframe_size,			ptxShaderVarKeyframe_mem/1024.0f);
	ptxDebugf1("  ptxShaderVarTexture       - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxShaderVarTexture_num,		ptxShaderVarTexture_sizeAdj,		ptxShaderVarTexture_size,			ptxShaderVarTexture_mem/1024.0f);
	ptxDebugf1("  ptxDrawable               - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxDrawable_num,				ptxDrawable_sizeAdj,				ptxDrawable_size,					ptxDrawable_mem/1024.0f);
	ptxDebugf1("  ptxDrawableEntry          - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxDrawableEntry_num,			ptxDrawableEntry_sizeAdj,			ptxDrawableEntry_size,				ptxDrawableEntry_mem/1024.0f);
	ptxDebugf1("  ptxKeyframePropList       - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxKeyframePropList_num,		ptxKeyframePropList_sizeAdj,		ptxKeyframePropList_size,			ptxKeyframePropList_mem/1024.0f);
	ptxDebugf1("  ptxKeyframeProp           - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxKeyframeProp_num,			ptxKeyframeProp_sizeAdj,			ptxKeyframeProp_size,				ptxKeyframeProp_mem/1024.0f);
	ptxDebugf1("  ptxKeyframe               - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxKeyframe_num,				ptxKeyframe_sizeAdj,				ptxKeyframe_size,					ptxKeyframe_mem/1024.0f);
#if PTXKEYFRAME_VERSION==0 || PTXKEYFRAME_VERSION==1
	ptxDebugf1("  ptxKeyframeEntry          - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxKeyframeEntry_num,			ptxKeyframeEntry_sizeAdj,			ptxKeyframeEntry_size,				ptxKeyframeEntry_mem/1024.0f);
#endif
	ptxDebugf1("  ptxd_Model                - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxd_Model_num,				ptxd_Model_sizeAdj,					ptxd_Model_size,					ptxd_Model_mem/1024.0f);
	ptxDebugf1("  ptxd_Sprite               - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxd_Sprite_num,				ptxd_Sprite_sizeAdj,				ptxd_Sprite_size,					ptxd_Sprite_mem/1024.0f);
	ptxDebugf1("  ptxd_Trail                - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxd_Trail_num,				ptxd_Trail_sizeAdj,					ptxd_Trail_size,					ptxd_Trail_mem/1024.0f);
	ptxDebugf1("  ptxu_Acceleration         - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Acceleration_num,			ptxu_Acceleration_sizeAdj,			ptxu_Acceleration_size,				ptxu_Acceleration_mem/1024.0f);
	ptxDebugf1("  ptxu_Age                  - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Age_num,					ptxu_Age_sizeAdj,					ptxu_Age_size,						ptxu_Age_mem/1024.0f);
	ptxDebugf1("  ptxu_AnimateTexture       - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_AnimateTexture_num,		ptxu_AnimateTexture_sizeAdj,		ptxu_AnimateTexture_size,			ptxu_AnimateTexture_mem/1024.0f);
	ptxDebugf1("  ptxu_Attractor            - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Attractor_num,			ptxu_Attractor_sizeAdj,				ptxu_Attractor_size,				ptxu_Attractor_mem/1024.0f);
	ptxDebugf1("  ptxu_Collision            - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Collision_num,			ptxu_Collision_sizeAdj,				ptxu_Collision_size,				ptxu_Collision_mem/1024.0f);
	ptxDebugf1("  ptxu_Colour               - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Colour_num,				ptxu_Colour_sizeAdj,				ptxu_Colour_size,					ptxu_Colour_mem/1024.0f);
	ptxDebugf1("  ptxu_Dampening            - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Dampening_num,			ptxu_Dampening_sizeAdj,				ptxu_Dampening_size,				ptxu_Dampening_mem/1024.0f);
	ptxDebugf1("  ptxu_MatrixWeight         - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_MatrixWeight_num,			ptxu_MatrixWeight_sizeAdj,			ptxu_MatrixWeight_size,				ptxu_MatrixWeight_mem/1024.0f);
	ptxDebugf1("  ptxu_Noise                - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Noise_num,				ptxu_Noise_sizeAdj,					ptxu_Noise_size,					ptxu_Noise_mem/1024.0f);
	ptxDebugf1("  ptxu_Pause		        - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Pause_num,				ptxu_Pause_sizeAdj,					ptxu_Pause_size,					ptxu_Pause_mem/1024.0f);
	ptxDebugf1("  ptxu_Rotation             - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Rotation_num,				ptxu_Rotation_sizeAdj,				ptxu_Rotation_size,					ptxu_Rotation_mem/1024.0f);
	ptxDebugf1("  ptxu_Size                 - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Size_num,					ptxu_Size_sizeAdj,					ptxu_Size_size,						ptxu_Size_mem/1024.0f);
	ptxDebugf1("  ptxu_Velocity             - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Velocity_num,				ptxu_Velocity_sizeAdj,				ptxu_Velocity_size,					ptxu_Velocity_mem/1024.0f);
	ptxDebugf1("  ptxu_Wind                 - %8d * %8d bytes (%8d bytes) = %8.2fk", ptxu_Wind_num,					ptxu_Wind_sizeAdj,					ptxu_Wind_size,						ptxu_Wind_mem/1024.0f);
	ptxDebugf1("  total = %.2fk", totalMem/1024.0f);
	ptxDebugf1("");
}

void ptxDebug::Init(bkBank* pVfxBank, bkGroup* pParticleGroup)
{
	sm_pBank = NULL;

	// info
	sm_version = RMPTFX_VERSION;

	// fx lists
	sm_drawFxlistReferences = false;

	// global 
	sm_enableDebugDrawing = false;
	sm_enableExpensiveOneShotSubFrameCalcs = false;
	sm_disableInitPointAccn = false;
	sm_disableInitPointAge = false;
	sm_disableInitPointAnimTex = false;
	sm_disableInitPointAttractor = false;
	sm_disableInitPointColn = false;
	sm_disableInitPointColour = false;
	sm_disableInitPointDampening = false;
	sm_disableInitPointMtxWeight = false;
	sm_disableInitPointNoise = false;
	sm_disableInitPointPause = false;
	sm_disableInitPointRotation = false;
	sm_disableInitPointSize = false;
	sm_disableInitPointVelocity = false;
	sm_disableInitPointWind = false;
	sm_disableInitPointDecal = false;
	sm_disableInitPointDecalPool = false;
	sm_disableInitPointDecalTrail = false;
	sm_disableInitPointLight = false;
	sm_disableInitPointLiquid = false;
	sm_disableInitPointRiver = false;
	sm_disableInitPointZCull = false;
	sm_overrideHighQualityEvo = -0.01f;
	sm_preUpdateTimeLimitActive = false;
	sm_preUpdateTimeLimit = 0.25f;
	sm_performParallelFinalize = true;
	sm_disableShadowCullStateCalcs = false;

	// effect instances
	sprintf(sm_effectInstNameFilter, "%s", "");
	sm_drawEffectInstInfo = false;
	sm_drawEffectInstNames = false;
	sm_drawEffectInstAddress = false;
	sm_drawEffectInstFxList = false;
	sm_drawEffectInstLifeRatios = false;
	sm_drawEffectInstNumPoints = false;
	sm_drawEffectInstEvolutions = false;
	sm_drawEffectInstDistToCam = false;
	sm_drawEffectInstMaxDuration = false;
	sm_drawEffectInstCameraBias = false;
	sm_drawEffectInstColnPolys = false;
	sm_drawEffectInstPositionTrace = false;
	sm_drawEffectInstCallbacksHighlighted = false;
	sm_drawEffectInstQualityHighlighted = false;
	sm_outputEffectInstPlayStateDebug = PARAM_ptfxOutputEffectInstPlayStateDebug.Get();
	sm_outputEffectInstUpdateProfile = false;

	// emitter instances
	sprintf(sm_emitterInstNameFilter, "%s", "");
	sm_emitterInstEventIdFilter = -1;
	sm_drawEmitterInstBoundBoxes = false;
	sm_drawEmitterInstCreationDomain = false;
	sm_drawEmitterInstTargetDomain = false;
	sm_drawEmitterInstAttractorDomain = false;
	sm_outputEmitterInstDrawStateDebug = false;
	sm_outputEffectInstSpawningProfile = false;
	
	// trails				
	sm_drawTrails = false;
	sm_drawTrailsWireframe = false;
	sm_simpleTrails = false;
	sm_numDebugTrailPoints = 0;
	for (int i=0; i<4; i++)
	{
		sm_debugTrailPoints[i].vPosition = Vec3V(V_ZERO);
		sm_debugTrailPoints[i].vColour = Vec4V(V_ONE);
		sm_debugTrailPoints[i].vSize = ScalarV(V_ONE);
		sm_debugTrailPoints[i].vLifeRatio = ScalarV(V_ZERO);
	}

	// points
	sm_drawPoints = false;		
	sm_drawPointAlphas = false;
	sm_drawPointCollisions = false;
	sm_numPointCollisionFrames = 1;

	// misc
	sm_debugAxisScale = 1.0f;
	sm_disableInvertedAxes = false;
	sm_toggleQuickDebug = false;

	// shader debug
	sm_bEnableShaderDebugOverrideAlpha = false;

	// profiling
	ResetProfilingData();

	// fxlist info
	sm_debugFxListInfo.Clear();

	// reporting
	sprintf(sm_reportFxListName, "%s", "");
	sprintf(sm_reportEffectRuleName, "%s", "");
	sprintf(sm_reportEmitterRuleName, "%s", "");
	sprintf(sm_reportParticleRuleName, "%s", "");
	sprintf(sm_reportTextureName, "%s", "");
	sprintf(sm_reportModelName, "%s", "");

	InitWidgets(pVfxBank, pParticleGroup);
}

void ptxDebug::ToggleQuickDebug()
{
	sm_toggleQuickDebug = !sm_toggleQuickDebug;
	if (sm_toggleQuickDebug)
	{
		sm_enableDebugDrawing = true;

		sm_drawEffectInstInfo = true;
		sm_drawEffectInstNames = true;
		sm_drawEffectInstFxList = true;
		sm_drawEffectInstLifeRatios = true;
		sm_drawEffectInstNumPoints = true;

		sm_drawPoints = true;	
	}
	else
	{
		sm_enableDebugDrawing = false;

		sm_drawEffectInstInfo = false;
		sm_drawEffectInstNames = false;
		sm_drawEffectInstFxList = false;
		sm_drawEffectInstLifeRatios = false;
		sm_drawEffectInstNumPoints = false;

		sm_drawPoints = false;	
	}
}

void ptxDebug::Update()
{
	sm_profilingFrameCount++;

	sm_averageKeyframeQueryTimeMs = sm_accumKeyframeQueryTimeMs/(float)sm_profilingFrameCount;
	sm_averageKeyframeQueryCount = sm_accumKeyframeQueryCount/sm_profilingFrameCount;
}

void ptxDebug::ResetProfilingData()
{
	sm_profilingFrameCount = 0;
	sm_accumKeyframeQueryTimeMs = 0.0f;
	sm_accumKeyframeQueryCount = 0;
	sm_averageKeyframeQueryTimeMs = 0.0f;
	sm_averageKeyframeQueryCount = 0;
}

void ptxDebug::ToggleDisableInitPointMost()
{
	if (sm_disableInitPointAccn)
	{
		sm_disableInitPointAccn = false;
		sm_disableInitPointAge = false;
		sm_disableInitPointAnimTex = false;
		sm_disableInitPointAttractor = false;
		sm_disableInitPointColn = false;
		sm_disableInitPointColour = false;
		sm_disableInitPointDampening = false;
		sm_disableInitPointMtxWeight = false;
		sm_disableInitPointNoise = false;
		sm_disableInitPointPause = false;
		sm_disableInitPointRotation = false;
		sm_disableInitPointSize = false;
		sm_disableInitPointVelocity = false;
		sm_disableInitPointWind = false;
		sm_disableInitPointDecal = false;
		sm_disableInitPointDecalPool = false;
		sm_disableInitPointDecalTrail = false;
		sm_disableInitPointLight = false;
		sm_disableInitPointLiquid = false;
		sm_disableInitPointRiver = false;
		sm_disableInitPointZCull = false;
	}
	else
	{
		sm_disableInitPointAccn = true;
		sm_disableInitPointAge = false;
		sm_disableInitPointAnimTex = true;
		sm_disableInitPointAttractor = true;
		sm_disableInitPointColn = true;
		sm_disableInitPointColour = false;
		sm_disableInitPointDampening = true;
		sm_disableInitPointMtxWeight = false;
		sm_disableInitPointNoise = true;
		sm_disableInitPointPause = true;
		sm_disableInitPointRotation = true;
		sm_disableInitPointSize = false;
		sm_disableInitPointVelocity = false;
		sm_disableInitPointWind = true;
		sm_disableInitPointDecal = true;
		sm_disableInitPointDecalPool = true;
		sm_disableInitPointDecalTrail = true;
		sm_disableInitPointLight = true;
		sm_disableInitPointLiquid = true;
		sm_disableInitPointRiver = true;
		sm_disableInitPointZCull = true;
	}
}

void ptxDebug::ReportFxList()
{
	ptxFxList* pFxList = RMPTFXMGR.GetFxList(atHashWithStringNotFinal(sm_reportFxListName));
	if (pFxList)
	{
		// title
		ptxDebugf1("");
		ptxDebugf1("FxList Report");

		// name
		ptxDebugf1(" name:            %s", sm_reportFxListName);

		// effect rules
		ptxDebugf1(" effectrules:     %d", pFxList->GetEffectRuleDictionary()->GetCount());
		for (int i=0; i<pFxList->GetEffectRuleDictionary()->GetCount(); i++)
		{
			ptxEffectRule* pEffectRule = pFxList->GetEffectRuleDictionary()->GetEntry(i);
			ptxDebugf1("                  %s", pEffectRule->GetName());
		}

		// emitter rules
		ptxDebugf1(" emitterrules:    %d", pFxList->GetEmitterRuleDictionary()->GetCount());
		for (int i=0; i<pFxList->GetEmitterRuleDictionary()->GetCount(); i++)
		{
			ptxEmitterRule* pEmitterRule = pFxList->GetEmitterRuleDictionary()->GetEntry(i);
			ptxDebugf1("                  %s", pEmitterRule->GetName());
		}

		// particle rules
		ptxDebugf1(" particlerules:   %d", pFxList->GetParticleRuleDictionary()->GetCount());
		for (int i=0; i<pFxList->GetParticleRuleDictionary()->GetCount(); i++)
		{
			ptxParticleRule* pParticleRule = pFxList->GetParticleRuleDictionary()->GetEntry(i);
			ptxDebugf1("                  %s", pParticleRule->GetName());
		}

		// textures
		ptxDebugf1(" textures:        %d", pFxList->GetTextureDictionary()->GetCount());
		for (int i=0; i<pFxList->GetTextureDictionary()->GetCount(); i++)
		{
			grcTexture* pTexture = pFxList->GetTextureDictionary()->GetEntry(i);
			ptxDebugf1("                  %s", pTexture->GetName());
		}

		// models
		ptxDebugf1(" models:          %d", pFxList->GetModelDictionary()->GetCount());
		for (int i=0; i<pFxList->GetModelDictionary()->GetCount(); i++)
		{
			rmcDrawable* pDrawable = pFxList->GetModelDictionary()->GetEntry(i);

			//char tempBuff[32];
			//ptxDebugf1("                  %s", pDrawable->GetDebugName(tempBuff, 32));

			bool foundName = false;
			for (int j=0; j<pFxList->GetParticleRuleDictionary()->GetCount(); j++)
			{
				if (foundName==false)
				{
					ptxParticleRule* pParticleRule = pFxList->GetParticleRuleDictionary()->GetEntry(j);
					if (pParticleRule->GetDrawType()==PTXPARTICLERULE_DRAWTYPE_MODEL)
					{
						for (int k=0; k<pParticleRule->GetNumDrawables(); k++)
						{
							ptxDrawable& drawable = pParticleRule->GetDrawable(k);
							if (drawable.GetDrawable()==pDrawable)
							{
								foundName = true;
								ptxDebugf1("                  %s", drawable.GetName().c_str());
								break;
							}
						}
					}
				}
			}
		}
	}
}

void ptxDebug::ReportEffectRule()
{
	ptxEffectRule* pEffectRule = RMPTFXMGR.GetEffectRule(atHashWithStringNotFinal(sm_reportEffectRuleName));
	if (pEffectRule)
	{
		// title
		ptxDebugf1("");
		ptxDebugf1("Effect Rule Report");

		// name
		ptxDebugf1(" name:            %s", sm_reportEffectRuleName);

		// keyframe entries
		int keyframeCount = 0;
		int keyframeMemory = 0;
		int keyframeMax = 0;
		int keyframeDefault = 0;
		pEffectRule->GetKeyframeEntryInfo(keyframeCount, keyframeMemory, keyframeMax, keyframeDefault);
		ptxDebugf1(" kf entries:      %d (%.2fk) (max: %d) (def: %d)", keyframeCount, keyframeMemory/1024.0f, keyframeMax, keyframeDefault);

		// events
		ptxTimeLine& timeline = pEffectRule->GetTimeline();
		int numEvents = timeline.GetNumEvents();
		ptxDebugf1(" events:          %d", numEvents);
		for (int i=0; i<numEvents; i++)
		{
			ptxEvent* pEvent = timeline.GetEvent(i);
			if (pEvent->GetType()==PTXEVENT_TYPE_EMITTER)
			{
				ptxEventEmitter* pEventEmitter = static_cast<ptxEventEmitter*>(pEvent);
				ptxDebugf1("                  %s / %s", pEventEmitter->GetEmitterRule()->GetName(), pEventEmitter->GetParticleRule()->GetName());
			}
		}

		// fxlists
		const int MAX_RESULTS = 64;
		ptxFxList* pFxLists[MAX_RESULTS];
		int fxListRefCount = RMPTFXMGR.GetAllEffectRuleRefs(sm_reportEffectRuleName, pFxLists, MAX_RESULTS);
		ptxDebugf1(" fxlists:         %d", fxListRefCount);
		for (int i=0; i<fxListRefCount; i++)
		{
			ptxDebugf1("                  %s", pFxLists[i]->GetName());
		}
	}
}

void ptxDebug::ReportEffectRuleKeyframeEntries()
{
	ptxEffectRule* pEffectRule = RMPTFXMGR.GetEffectRule(atHashWithStringNotFinal(sm_reportEffectRuleName));
	if (pEffectRule)
	{
		// title
		ptxDebugf1("");
		ptxDebugf1("Effect Rule Keyframe Entry Report");

		// name
		ptxDebugf1(" name:            %s", sm_reportEffectRuleName);

		// keyframe entries
		ptxDebugf1(" kf entries:");
		pEffectRule->OutputKeyframeEntryInfo();
	}
}

void ptxDebug::ReportEmitterRule()
{
	ptxEmitterRule* pEmitterRule = RMPTFXMGR.GetEmitterRule(atHashWithStringNotFinal(sm_reportEmitterRuleName));
	if (pEmitterRule)
	{
		// title
		ptxDebugf1("");
		ptxDebugf1("Emitter Rule Report");

		// name
		ptxDebugf1(" name:            %s", sm_reportEmitterRuleName);

		// keyframe entries
		int keyframeCount = 0;
		int keyframeMemory = 0;
		int keyframeMax = 0;
		int keyframeDefault = 0;
		pEmitterRule->GetKeyframeEntryInfo(keyframeCount, keyframeMemory, keyframeMax, keyframeDefault);
		ptxDebugf1(" kf entries:      %d (%.2fk) (max: %d) (def: %d)", keyframeCount, keyframeMemory/1024.0f, keyframeMax, keyframeDefault);

		// effect rules
		const int MAX_RESULTS = 64;
		ptxEffectRule* pEffectRules[MAX_RESULTS];
		ptxFxList* pFxLists[MAX_RESULTS];
		int effectRuleRefCount = RMPTFXMGR.GetAllEmitterRuleRefs(sm_reportEmitterRuleName, pEffectRules, pFxLists, MAX_RESULTS);
		ptxDebugf1(" effectrules:     %d", effectRuleRefCount);
		for (int i=0; i<effectRuleRefCount; i++)
		{
			ptxDebugf1("                  %s (%s)", pEffectRules[i]->GetName(), pFxLists[i]->GetName());
		}
	}
}

void ptxDebug::ReportEmitterRuleKeyframeEntries()
{
	ptxEmitterRule* pEmitterRule = RMPTFXMGR.GetEmitterRule(atHashWithStringNotFinal(sm_reportEmitterRuleName));
	if (pEmitterRule)
	{
		// title
		ptxDebugf1("");
		ptxDebugf1("Emitter Rule Keyframe Entry Report");

		// name
		ptxDebugf1(" name:            %s", sm_reportEmitterRuleName);

		// keyframe entries
		ptxDebugf1(" kf entries:");
		pEmitterRule->OutputKeyframeEntryInfo();
	}
}

void ptxDebug::ReportParticleRule()
{
	ptxParticleRule* pParticleRule = RMPTFXMGR.GetParticleRule(atHashWithStringNotFinal(sm_reportParticleRuleName));
	if (pParticleRule)
	{
		// title
		ptxDebugf1("");
		ptxDebugf1("Particle Rule Report");

		// name
		ptxDebugf1(" name:            %s", sm_reportParticleRuleName);

		// keyframe entries
		int keyframeCount = 0;
		int keyframeMemory = 0;
		int keyframeMax = 0;
		int keyframeDefault = 0;
		pParticleRule->GetKeyframeEntryInfo(keyframeCount, keyframeMemory, keyframeMax, keyframeDefault);
		ptxDebugf1(" kf entries:      %d (%.2fk) (max: %d) (def: %d)", keyframeCount, keyframeMemory/1024.0f, keyframeMax, keyframeDefault);

		// behaviour info
		int numActiveBehaviours = pParticleRule->GetNumActiveBehaviours();
		ptxDebugf1(" behaviours:      %d", numActiveBehaviours);
		for (int i=0; i<numActiveBehaviours; i++)
		{
			ptxDebugf1("                  %s", pParticleRule->GetActiveBehaviour(i)->GetName());
		}

		// referenced in
		const int MAX_RESULTS = 64;
		ptxEffectRule* pEffectRules[MAX_RESULTS];
		ptxFxList* pFxLists[MAX_RESULTS];
		int effectRuleRefCount = RMPTFXMGR.GetAllParticleRuleRefs(sm_reportParticleRuleName, pEffectRules, pFxLists, MAX_RESULTS);
		ptxDebugf1(" effectrules:     %d", effectRuleRefCount);
		for (int i=0; i<effectRuleRefCount; i++)
		{
			ptxDebugf1("                  %s (%s)", pEffectRules[i]->GetName(), pFxLists[i]->GetName());
		}
	}
}

void ptxDebug::ReportParticleRuleKeyframeEntries()
{
	ptxParticleRule* pParticleRule = RMPTFXMGR.GetParticleRule(atHashWithStringNotFinal(sm_reportParticleRuleName));
	if (pParticleRule)
	{
		// title
		ptxDebugf1("");
		ptxDebugf1("Particle Rule Keyframe Entry Report");

		// name
		ptxDebugf1(" name:            %s", sm_reportParticleRuleName);

		// keyframe entries
		ptxDebugf1(" kf entries:");
		pParticleRule->OutputKeyframeEntryInfo();
	}
}

void ptxDebug::ReportTexture()
{
	// title
	ptxDebugf1("");
	ptxDebugf1("Texture Report");

	// name
	ptxDebugf1(" name:            %s", sm_reportTextureName);

	// particle rules
	const int MAX_RESULTS = 1024;
	ptxParticleRule* pParticleRules[MAX_RESULTS];
	ptxFxList* pFxLists[MAX_RESULTS];
	int particleRuleRefCount = RMPTFXMGR.GetAllTextureRefs(sm_reportTextureName, pParticleRules, pFxLists, MAX_RESULTS);
	ptxDebugf1(" ptxrules:        %d", particleRuleRefCount);
	for (int i=0; i<particleRuleRefCount; i++)
	{
		ptxDebugf1("                  %s (%s)", pParticleRules[i]->GetName(), pFxLists[i]->GetName());
	}
}

void ptxDebug::ReportModel()
{
	char fullModelName[128];
	sprintf(fullModelName, "%s/%s", sm_reportModelName, sm_reportModelName);

	// title
	ptxDebugf1("");
	ptxDebugf1("Model Report");

	// name
	ptxDebugf1(" name:            %s", sm_reportModelName);

	// particle rules
	const int MAX_RESULTS = 512;
	ptxParticleRule* pParticleRules[MAX_RESULTS];
	ptxFxList* pFxLists[MAX_RESULTS];
	int particleRuleRefCount = RMPTFXMGR.GetAllModelRefs(fullModelName, pParticleRules, pFxLists, MAX_RESULTS);
	ptxDebugf1(" ptxrules:        %d", particleRuleRefCount);
	for (int i=0; i<particleRuleRefCount; i++)
	{
		ptxDebugf1("                  %s (%s)", pParticleRules[i]->GetName(), pFxLists[i]->GetName());
	}

	// textures
	if (particleRuleRefCount>0)
	{
		ptxDebugf1(" textures:");
		for (int j=0; j<pParticleRules[0]->GetNumDrawables(); j++)
		{
			ptxDrawable& drawable = pParticleRules[0]->GetDrawable(j);
			rmcDrawable* pDrawable = drawable.GetDrawable();
			if (pDrawable)
			{
				for (int k=0; k<pDrawable->GetShaderGroup().GetCount(); k++)
				{
					grmShader& shader = pDrawable->GetShaderGroup().GetShader(k);
					for (int s=0; s<shader.GetInstanceData().GetCount(); s++)
					{
						if (shader.GetInstanceData().IsTexture(s))
						{
							ptxDebugf1("                  %s", shader.GetInstanceData().Data()[s].Texture->GetName());
						}
					}
				}
			}
		}
	}
}

void ptxDebug::InitWidgets(bkBank* pVfxBank, bkGroup* pParticleGroup)
{

	//Use the passing bank if valid
	bool bCreateBank = (pVfxBank == NULL);
	bool bUseGroup = (pVfxBank && pParticleGroup);

	if(bCreateBank)
	{
		// destroy any previous rmptfx bank
		sm_pBank = BANKMGR.FindBank("RMPTFX");
		if (sm_pBank)
		{
			BANKMGR.DestroyBank(*sm_pBank);
			sm_pBank = NULL;
		}

		// create a new bank widget
		ptxAssertf(sm_pBank==NULL, "the RMPTFX bank widget already exists");
		sm_pBank = &BANKMGR.CreateBank("RMPTFX", 0, 0, false);
	}
	else
	{
		sm_pBank = pVfxBank;
	}

	ptxAssertf(sm_pBank, "Bank used is NULL");
	if (sm_pBank)
	{
		if(bUseGroup)
		{
			sm_pBank->SetCurrentGroup(*pParticleGroup);
		}
		// info
		sm_pBank->AddTitle("Info");
		sm_pBank->AddSlider("Version", &sm_version, 0.0f, 1000.0f, 0.0f);

		// settings
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Settings");
		sm_pBank->AddToggle("Update On Main Thread Only", ptxManager::GetUpdateOnSingleThreadPtr());
		sm_pBank->AddToggle("Perform Update Finalize in Parallel", &sm_performParallelFinalize);

		sm_pBank->AddToggle("Limit One Child Update Thread", ptxManager::GetSingleUpdateTaskPtr());
		sm_pBank->AddToggle("Let Main Thread Help Child Threads", ptxManager::GetUpdateOnChildAndMainThreadsPtr());
		sm_pBank->AddToggle("Enable Skip Occlusion Test for N Frames", ptxManager::GetIsSkipOcclusionTestForEffectInstancesPtr());
		sm_pBank->AddSlider("Max Num Frames for Skip Occlusion", ptxManager::GetMaxFrameSkipOcclusionTestForEffectInstancesPtr(), 0, 255, 1);

		// fx lists
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Fx Lists");
		sm_pBank->AddToggle("Draw References", &sm_drawFxlistReferences);
#if __DEV
		sm_pBank->AddButton("Output References", datCallback(MFA(ptxManager::OutputFxListReferences), g_ptxManager::InstancePtr()));
		sm_pBank->AddButton("Output Loaded", datCallback(MFA(ptxManager::OutputLoadedFxLists), g_ptxManager::InstancePtr()));
#endif

		// global
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Global");
		sm_pBank->AddToggle("Enable Debug Drawing", &sm_enableDebugDrawing);
		sm_pBank->AddToggle("Enable Expensive One Shot Sub Frame Calcs", &sm_enableExpensiveOneShotSubFrameCalcs);

		sm_pBank->AddToggle("Disable InitPoint Accn", &sm_disableInitPointAccn);
		sm_pBank->AddToggle("Disable InitPoint Age", &sm_disableInitPointAge);
		sm_pBank->AddToggle("Disable InitPoint AnimTex", &sm_disableInitPointAnimTex);
		sm_pBank->AddToggle("Disable InitPoint Attractor", &sm_disableInitPointAttractor);
		sm_pBank->AddToggle("Disable InitPoint Coln", &sm_disableInitPointColn);
		sm_pBank->AddToggle("Disable InitPoint Colour", &sm_disableInitPointColour);
		sm_pBank->AddToggle("Disable InitPoint Dampening", &sm_disableInitPointDampening);
		sm_pBank->AddToggle("Disable InitPoint MtxWeight", &sm_disableInitPointMtxWeight);
		sm_pBank->AddToggle("Disable InitPoint Noise", &sm_disableInitPointNoise);
		sm_pBank->AddToggle("Disable InitPoint Pause", &sm_disableInitPointPause);
		sm_pBank->AddToggle("Disable InitPoint Rotation", &sm_disableInitPointRotation);
		sm_pBank->AddToggle("Disable InitPoint Size", &sm_disableInitPointSize);
		sm_pBank->AddToggle("Disable InitPoint Velocity", &sm_disableInitPointVelocity);
		sm_pBank->AddToggle("Disable InitPoint Wind", &sm_disableInitPointWind);
		sm_pBank->AddToggle("Disable InitPoint Decal", &sm_disableInitPointDecal);
		sm_pBank->AddToggle("Disable InitPoint Decal Pool", &sm_disableInitPointDecalPool);
		sm_pBank->AddToggle("Disable InitPoint Decal Trail", &sm_disableInitPointDecalTrail);
		sm_pBank->AddToggle("Disable InitPoint Light", &sm_disableInitPointLight);
		sm_pBank->AddToggle("Disable InitPoint Liquid", &sm_disableInitPointLiquid);
		sm_pBank->AddToggle("Disable InitPoint River", &sm_disableInitPointRiver);
		sm_pBank->AddToggle("Disable InitPoint ZCull", &sm_disableInitPointZCull);
		sm_pBank->AddButton("Disable InitPoint Most", ToggleDisableInitPointMost);

		sm_pBank->AddSlider("Override High Quality Evo", &sm_overrideHighQualityEvo, -0.01f, 1.0f, 0.01f);
		sm_pBank->AddToggle("Pre Update Time Limit Active", &sm_preUpdateTimeLimitActive);
		sm_pBank->AddSlider("Pre Update Time Limit", &sm_preUpdateTimeLimit, 0.0f, 1000.0f, 0.25f);

		sm_pBank->AddToggle("Disable Shadow Cull State Calcs", &sm_disableShadowCullStateCalcs);
		
		// effect instance
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Effect Instances");
		sm_pBank->AddText("Effect Inst Name Filter", sm_effectInstNameFilter, 64);
		sm_pBank->AddToggle("Draw Info", &sm_drawEffectInstInfo);
		sm_pBank->AddToggle("- Names", &sm_drawEffectInstNames);
		sm_pBank->AddToggle("- Address", &sm_drawEffectInstAddress);
		sm_pBank->AddToggle("- FxList", &sm_drawEffectInstFxList);
		sm_pBank->AddToggle("- Life Ratios", &sm_drawEffectInstLifeRatios);
		sm_pBank->AddToggle("- Num Points", &sm_drawEffectInstNumPoints);
		sm_pBank->AddToggle("- Evolutions", &sm_drawEffectInstEvolutions);
		sm_pBank->AddToggle("- Dist To Cam", &sm_drawEffectInstDistToCam);
		sm_pBank->AddToggle("- Max Duration", &sm_drawEffectInstMaxDuration);
		sm_pBank->AddToggle("- Camera Bias", &sm_drawEffectInstCameraBias);
		sm_pBank->AddToggle("- Collision Polys", &sm_drawEffectInstColnPolys);
		sm_pBank->AddToggle("- Position Trace", &sm_drawEffectInstPositionTrace);
		sm_pBank->AddToggle("- Highlight Callback Effects", &sm_drawEffectInstCallbacksHighlighted);
		sm_pBank->AddToggle("- Highlight Quality", &sm_drawEffectInstQualityHighlighted);
		sm_pBank->AddToggle("Output Play State Debug", &sm_outputEffectInstPlayStateDebug);
		sm_pBank->AddToggle("Output Update Profile", &sm_outputEffectInstUpdateProfile);

		// emitter instance
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Emitter Instances");
		sm_pBank->AddText("Emitter Inst Name Filter", sm_emitterInstNameFilter, 64);
		sm_pBank->AddSlider("Emitter Inst Event Id Filter", &sm_emitterInstEventIdFilter, -1, PTXEFFECTINST_MAX_EVENT_INSTS, 1);
		sm_pBank->AddToggle("Draw Bound Boxes", &sm_drawEmitterInstBoundBoxes);
		sm_pBank->AddToggle("Draw Creation Domains", &sm_drawEmitterInstCreationDomain);
		sm_pBank->AddToggle("Draw Target Domains", &sm_drawEmitterInstTargetDomain);
		sm_pBank->AddToggle("Draw Attractor Domains", &sm_drawEmitterInstAttractorDomain);
		sm_pBank->AddToggle("Output Play State Debug", &sm_outputEmitterInstDrawStateDebug);
		sm_pBank->AddToggle("Output Spawning Profile", &sm_outputEffectInstSpawningProfile);
		
		// trails
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Trails");
		sm_pBank->AddToggle("Draw Trails", &sm_drawTrails);
		sm_pBank->AddToggle("Draw Trails Wireframe", &sm_drawTrailsWireframe);		
		sm_pBank->AddToggle("Simple Trails", &sm_simpleTrails);

		sm_pBank->AddSlider("Num Debug Trail Points", &sm_numDebugTrailPoints, 0, PTXDEBUG_MAX_DEBUG_TRAIL_POINTS, 1);
		for (int i=0; i<PTXDEBUG_MAX_DEBUG_TRAIL_POINTS; i++)
		{
			char groupTitle[32];
			sprintf(groupTitle, "Debug Trail Point %d", i);
			bkGroup* pGroup = sm_pBank->AddGroup(groupTitle, false);
			pGroup->AddSlider("Position", &RC_VECTOR3(sm_debugTrailPoints[i].vPosition), Vector3(-1000.0f, -1000.0f, -1000.0f), Vector3(1000.0f, 1000.0f, 1000.0f), Vector3(0.1f, 0.1f, 0.1f));
			pGroup->AddSlider("Colour", &RC_VECTOR4(sm_debugTrailPoints[i].vColour), Vector4(0.0f, 0.0f, 0.0f, 0.0f), Vector4(1.0f, 1.0f, 1.0f, 1.0f), Vector4(0.01f, 0.01f, 0.01f, 0.01f));
			pGroup->AddSlider("Size", &sm_debugTrailPoints[i].vSize, 0.0f, 10.0f, 0.1f);
			pGroup->AddSlider("Life Ratio", &sm_debugTrailPoints[i].vLifeRatio, 0.0f, 1.0f, 0.01f);
		}

		sm_pBank->AddToggle("Debug Trail Tangent Colours", &sm_debugTrailTangentColours);
		sm_pBank->AddSlider("Debug Trail Tangent Steps", &sm_debugTrailTangentSteps, 1, 8, 1);

		// point
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Points");
		sm_pBank->AddToggle("Draw Points", &sm_drawPoints);
		sm_pBank->AddToggle("Draw Point Alphas", &sm_drawPointAlphas);
		sm_pBank->AddToggle("Draw Point Collisions", &sm_drawPointCollisions);
		sm_pBank->AddSlider("Num Point Collision Frames", &sm_numPointCollisionFrames, 0, 1000, 1);
		sm_pBank->AddToggle("Draw Debug Quads", &sm_bEnableShaderDebugOverrideAlpha);

#if RMPTFX_EDITOR
		//rmptfx editor interface
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("RMPTFX Editor");
		sm_pBank->AddToggle("Enable RMPTFX Editor Listener", &sm_rmptfxListen);
#endif

		// misc
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Misc");
		sm_pBank->AddSlider("Debug Axis Scale", &sm_debugAxisScale, 0.0f, 2.0f, 0.01f);
		sm_pBank->AddToggle("Disable Inverted Axes", &sm_disableInvertedAxes);
		sm_pBank->AddSlider("Sprite Alpha Cull Threshold", &sm_pointAlphaCullThreshold, 0.0f, 1.0f, 0.01f);

		// profiling
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Profiling");
		sm_pBank->AddSlider("Ave Keyframe Query Time Per Frame", &sm_averageKeyframeQueryTimeMs, 0.0f, 999999.0f, 1.0f);
		sm_pBank->AddSlider("Ave Keyframe Query Count Per Frame", &sm_averageKeyframeQueryCount, 0, 999999, 1);
		sm_pBank->AddButton("Reset Profiling", ResetProfilingData);

		// reporting
		sm_pBank->AddTitle("");
		sm_pBank->AddTitle("Reporting");

		sm_pBank->AddTitle("");
		sm_pBank->AddText("FxList To Report", sm_reportFxListName, 64);
		sm_pBank->AddButton("FxList Report", ReportFxList);

		sm_pBank->AddTitle("");
		sm_pBank->AddText("Effect Rule To Report", sm_reportEffectRuleName, 64);
		sm_pBank->AddButton("Effect Rule Report", ReportEffectRule);
		sm_pBank->AddButton("Effect Rule Keyframe Entry Report", ReportEffectRuleKeyframeEntries);
		

		sm_pBank->AddTitle("");
		sm_pBank->AddText("Emitter Rule To Report", sm_reportEmitterRuleName, 64);
		sm_pBank->AddButton("Emitter Rule Report", ReportEmitterRule);
		sm_pBank->AddButton("Emitter Rule Keyframe Entry Report", ReportEmitterRuleKeyframeEntries);

		sm_pBank->AddTitle("");
		sm_pBank->AddText("Particle Rule To Report", sm_reportParticleRuleName, 64);
		sm_pBank->AddButton("Particle Rule Report", ReportParticleRule);
		sm_pBank->AddButton("Particle Rule Keyframe Entry Report", ReportParticleRuleKeyframeEntries);

		sm_pBank->AddTitle("");
		sm_pBank->AddText("Texture To Report", sm_reportTextureName, 64);
		sm_pBank->AddButton("Texture Report", ReportTexture);

		sm_pBank->AddTitle("");
		sm_pBank->AddText("Model To Report", sm_reportModelName, 64);
		sm_pBank->AddButton("Model Report", ReportModel);

		if(bUseGroup)
		{
			sm_pBank->UnSetCurrentGroup(*pParticleGroup);
		}
	}
}


#endif // RMPTFX_BANK


} // namespace rage

