// 
// rmptfx/ptxshader.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXSHADER_H
#define RMPTFX_PTXSHADER_H


// includes (us)
#include "rmptfx/ptxchannel.h"
#include "rmptfx/ptxdraw.h"
#include "rmptfx/ptxshadervar.h"
#include "rmptfx/ptxrenderstate.h"

// includes
#include "atl/array.h"
#include "atl/binmap.h"
#include "grcore/effect.h"

// defines
#define PTXSHADER_MAX_SHADERS				(2)
#define PTXSTR_DIFFUSETEXTURE_HASH			0xdf47048					// atHashValue("DiffuseTex2")
#define PTXSTR_REFRACTIONMAP_HASH			0xdf7cc018					// atHashValue("RefractionMap")

// namespaces
namespace rage 
{

// typedef
typedef atArray<datOwner<ptxShaderVar> >	ptxInstVars; 				// shader instanced variables  (better if it was atStringMap -but parser does not save/load them yet)
typedef atStringMap<ptxShaderVar*>			ptxShaderTemplateVars;

// forward declarations
class ptxByteBuff;
class ptxShaderTemplate;
class ptxShaderInst;


// enumerations
enum ptxDiffuseMode
{
	PTXDIFFUSEMODE_FIRST					= 0,

	PTXDIFFUSEMODE_TEX1_RGBA				= 0,
	PTXDIFFUSEMODE_TEX1_RRRR,
	PTXDIFFUSEMODE_TEX1_GGGG,
	PTXDIFFUSEMODE_TEX1_BBBB,
	PTXDIFFUSEMODE_TEX1_RGB,
	PTXDIFFUSEMODE_TEX1_RG_BLEND,

	PTXDIFFUSEMODE_NUM
};


// classes
class ptxShaderTemplateList
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Shutdown();

		// shaders
		static ptxShaderTemplate* LoadShader(const char* pName);
		static ptxShaderTemplate* GetShader(const char* pName);
		static const char* GetDefaultShaderName() {ptxAssertf(sm_numShaderNames>0, "no shaders are loaded"); return sm_pShaderNames[0];}

		static int GetNumShaders() {return sm_shaders.GetCount();}
		static const ptxShaderTemplate* GetShader(int idx) {return *sm_shaders.GetItem(idx);}

		// global vars
		static void SetGlobalVars();

#if RMPTFX_EDITOR
		static void SendToEditor(ptxByteBuff& buff);
#endif

	private: //////////////////////////
		
		static void LookUpGlobalVars();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		static atStringMap<ptxShaderTemplate*> sm_shaders;
		static atStringMap<grcEffectGlobalVar> sm_globalVars;

		// shader list
		static const int sm_numShaderNames;
		static const char* sm_pShaderNames[PTXSHADER_MAX_SHADERS];

};

enum ptxShaderDebugOverrideId {
	PTXSHADERDEBUG_ALPHA = 0,
};

class ptxShaderTemplateOverride
{
	///////////////////////////////////
	// Class
	///////////////////////////////////

	public: ///////////////////////////
	struct techniqueOverride
	{
		bool bDisable;
		bool bOverride;
		u16 techniqueId;
	};
	

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

	void Init(grmShader *pSrcShader);

	bool IsTechniqueDisabled(grcEffectTechnique technique) {return m_techniqueOverrides[technique].bDisable;}

	bool IsTechniqueOverriden(grcEffectTechnique technique) {return m_techniqueOverrides[technique].bOverride;}
	
	grcEffectTechnique GetOverrideTechniqueId(grcEffectTechnique technique) {return (grcEffectTechnique)m_techniqueOverrides[technique].techniqueId;}
	
	techniqueOverride &GetTechniqueOverride(int idx) { return m_techniqueOverrides[idx]; }
	
	void OverrideAllTechniquesWith(grcEffectTechnique technique);
	void OverrideTechniqueWith(grcEffectTechnique overridenTechnique, grcEffectTechnique overrideTechnique);
	void DisableAllTechniques();
	void EnableAllTechniques();
	void DisableTechnique(grcEffectTechnique technique);
	void EnableTechnique(grcEffectTechnique technique);

	int GetOverrideTechniqueCount() const { return m_techniqueOverrides.GetCount(); }

#if RMPTFX_BANK
	static void InitDebug();
	static void BindDebugTechniqueOverride(ptxShaderDebugOverrideId id);
	static void UnbindDebugTechniqueOverride();
#endif

	private: //////////////////////////

#if RMPTFX_BANK
	// shader debugging
	static ptxShaderTemplateOverride	sm_shaderDebugOverrideAlpha;
	static ptxShaderTemplate*			sm_pSpriteShader;
#endif

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////
	
	atArray<techniqueOverride> m_techniqueOverrides;
};

class ptxShaderTemplate
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		enum PTXTemplatePass
		{
			PTX_DEFAULT_PASS = 0,
		#if RMPTFX_USE_PARTICLE_SHADOWS
			PTX_SHADOW_PASS,
		#if GS_INSTANCED_SHADOWS
			PTX_INSTSHADOW_PASS,
		#endif	
		#endif
			PTX_MAX_PASS
		};


		ptxShaderTemplate();
		virtual ~ptxShaderTemplate();
	
		// general
		const char* GetName() const {return m_shaderName;}
		const char* GetName() {return m_shaderName;}

		// shader
		void SetGrmShader(grmShader* pShader);
		grmShader* GetGrmShader() {return m_pShader;}
		
		// Override
		void SetShaderOverride(ptxShaderTemplateOverride *pShaderOverride) {m_pShaderOverride[g_RenderThreadIndex] = pShaderOverride;}
		ptxShaderTemplateOverride *GetShaderOverride() {return m_pShaderOverride[g_RenderThreadIndex];}
		bool IsTechniqueDisabled(grcEffectTechnique technique) {return m_pShaderOverride[g_RenderThreadIndex] ? m_pShaderOverride[g_RenderThreadIndex]->IsTechniqueDisabled(technique) : false;}
		bool IsTechniqueOverriden(grcEffectTechnique technique) {return m_pShaderOverride[g_RenderThreadIndex] ? m_pShaderOverride[g_RenderThreadIndex]->IsTechniqueOverriden(technique) : false;}
		grcEffectTechnique GetOverrideTechniqueId(grcEffectTechnique technique) {return m_pShaderOverride[g_RenderThreadIndex] ? m_pShaderOverride[g_RenderThreadIndex]->GetOverrideTechniqueId(technique) : technique;}
		
		// shader variables
		void ClearShaderVars();
		void SetShaderVars(int blendSet, Vec4V_In vChannelMask);
		const ptxShaderTemplateVars& GetShaderVars() const {return m_shaderVars;}
		bool SyncShaderVars(ptxInstVars* pInstVars);									// returns true if the particle rule is in sync with the shader template
		

		void SetShaderVar(ptxShaderProgVarType shaderVar, Vec4V_In value)
		{
			if(m_progVars[shaderVar] != grcevNONE)
			{
				m_pShader->SetVar(m_progVars[shaderVar], value);
			}
		}
		void SetShaderVar(ptxShaderProgVarType shaderVar, grcRenderTarget* value)
		{
			if(m_progVars[shaderVar] != grcevNONE)
			{
				m_pShader->SetVar(m_progVars[shaderVar], value);
			}
		}

		void InitProgVar(ptxShaderProgVarType shaderVar, grcEffectVar varHandle)
		{
			m_progVars[shaderVar] = varHandle;

		}

		// editor
#if RMPTFX_EDITOR
		void SendShaderVarToEditor(u32 hashName, ptxByteBuff& buff);
//		void SendTechniqueListToEditor(ptxByteBuff& buff);
#endif

		void SetRenderPass(PTXTemplatePass pass) { m_RenderPassToUse[g_RenderThreadIndex] = pass; }
#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
		int GetRenderPass(const char *pShaderName, const char *pTechniqueName);
#else
		int GetRenderPass();
#endif

#if RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES
		typedef struct RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES_FILTER
		{
			char *pShader;
			char *pTechnique;
			u8 BlendModes[RMPTFX_MAX_PASSES];
			u32 Hash;
		} RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES_FILTER;

		static bool g_DebugRecordsHashed;
		static RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES_FILTER *g_pDebugPrintFilter;

		static void HashDebugRecords(void);
		static bool IsInADebugRecord(const char *pShaderName, const char *pTechniqueName, u32 blendMode);
		static u32 BuildShaderStringHash(const char *pShaderName, const char *pTechniqueName);
		static void SetDebugPrintFilter(RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES_FILTER *pFilter);
#endif //RMPTFX_DEBUG_PRINT_SHADER_NAMES_AND_PASSES

	private: //////////////////////////

		ptxShaderVar* CreateShaderVar(grmVariableInfo& varInfo);
		void InitProgVars(grmShader* pShader);

		// static 
		static bool CompareShaderVarType(ptxShaderVar* pVarA, ptxShaderVar* pVarB) {return (pVarA->GetType() == pVarB->GetType());}
		static int SortByVarID(const datOwner<ptxShaderVar>* ppVarA, const datOwner<ptxShaderVar>* ppVarB);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		grmShader* m_pShader;
		char m_shaderName[128];		

		ptxShaderTemplateVars m_shaderVars;										// variables including UI info from shader	
		grcEffectVar m_progVars[PTXPROGVAR_COUNT]; 								// programmed shader variables
		ptxShaderTemplateOverride *m_pShaderOverride[NUMBER_OF_RENDER_THREADS];	// Override system

		int m_BlendSet[NUMBER_OF_RENDER_THREADS];
		PTXTemplatePass m_RenderPassToUse[NUMBER_OF_RENDER_THREADS];
};


class ptxTechniqueDesc 
{
	friend class ptxShaderInst;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////
	
	public: ///////////////////////////

		ptxTechniqueDesc();
		virtual ~ptxTechniqueDesc() {}

		// resourcing
		ptxTechniqueDesc(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxTechniqueDesc);

		void GenerateName(ConstString& techniqueName);
		ptxProjectionMode GetProjectionMode() {return m_projMode;}
		bool GetIsLit() const { return m_isLit; }
		bool GetIsRefract() const { return m_isRefract; }


	private: //////////////////////////

#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		ptxDiffuseMode m_diffuseMode;
		ptxProjectionMode m_projMode;
		bool m_isLit;
		bool m_isSoft;
		bool m_isScreenSpace;
		bool m_isRefract;
		bool m_isNormalSpec;

		datPadding<3> m_pad;
		
};

class ptxShaderInst
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxShaderInst();
		virtual ~ptxShaderInst();

		// resourcing
		ptxShaderInst(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxShaderInst);
		void PostLoad();

		// operator overloads
		ptxShaderInst& operator=(const ptxShaderInst& rhs);

		//
#if RMPTFX_XML_LOADING
		void PreLoad(parTreeNode* pNode);

		// These are for internal use only!
		void RepairAndUpgradeData(); // If we loaded in an old version, upgrade it to the lastest
		void ResolveReferences();
#endif

#if RMPTFX_BANK
		typedef void (*CustomOverrideFuncType)(ptxInstVars* instVars);
		static void SetCustomOverrideFunc(CustomOverrideFuncType func);		
		static void RunCustomOverrideFunc(ptxInstVars* instVars);
		static CustomOverrideFuncType sm_customOverrideFunc;
#endif

		int Begin();
		void End(bool isBound=true);

		grmShader* GetGrmShader() const {return m_pShaderTemplate ? m_pShaderTemplate->GetGrmShader() : NULL;}
		bool SetShaderTemplate(const char* pShaderName, const char* pTechniqueName);
		const ptxShaderTemplate* GetShaderTemplate() const {return m_pShaderTemplate;}
		const char* GetShaderTemplateName() const {return m_shaderTemplateName;}
		u32 GetShaderTemplateHashName() const {return m_shaderTemplateHashName;}
		const char* GetShaderTechniqueName() const {return m_shaderTemplateTechniqueName;}
		grcEffectTechnique GetShaderTemplateTechnique() {return m_shaderTemplateTechniqueId;}
		grcEffectTechnique GetShaderTemplateTechnique() const {return m_shaderTemplateTechniqueId;}
		ptxProjectionMode GetProjectionMode() {return m_techniqueDesc.GetProjectionMode();}
		bool GetIsLit() const { return m_techniqueDesc.GetIsLit(); }
		bool GetIsRefract() const { return m_techniqueDesc.GetIsRefract(); }

		void ClearShaderVars();
		void SetShaderVars(int blendSet);
		ptxInstVars& GetShaderVars() {return m_instVars;}	
		const ptxInstVars& GetShaderVars() const {return m_instVars;}
		ptxInstVars* GetShaderVarsPtr() {return &m_instVars;}

		const char* GetTextureName(u32 samplerHashName) const;
		const u32 GetTextureHashName(u32 samplerHashName) const;
		bool GetIsDataInSync() {return m_isDataInSync;}

		void SetKeyframeVars(ptxCustomVars& customVars, ScalarV_In vKeyTime) const;

#if RMPTFX_EDITOR
		void ReplaceTexture(const char* pName, grcTexture* pTexture);

		void SendToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif

	private: //////////////////////////

#if RMPTFX_XML_LOADING
		static bool SyncShaderVarsXml(parTreeNode* pNode);
		PAR_PARSABLE
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		ConstString m_shaderTemplateName;		
		ConstString m_shaderTemplateTechniqueName;	
		ptxShaderTemplate* m_pShaderTemplate;
		grcEffectTechnique m_shaderTemplateTechniqueId;
		ptxTechniqueDesc m_techniqueDesc;

		ptxInstVars m_instVars;												// instanced variables - does *NOT* include UI info from shader

		bool m_isDataInSync;												// flag to determine if data matches shader
		datPadding<7> m_pad;

		u32 m_shaderTemplateHashName;

};

} // namespace rage

#endif //RMPTFX_PTXSHADER_H
