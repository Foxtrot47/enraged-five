// 
// rmptfx/ptxdrawlist.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXDRAWLIST_H 
#define RMPTFX_PTXDRAWLIST_H 


// includes
#include "ptxlist.h"


// namespaces
namespace rage
{
// class
class ptxDrawList
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxDrawList() {m_name[0] = 0; m_index = -1;}
			
		const char* GetName() const {return m_name;}	
		char* GetName() {return m_name;}
		int GetIndex() {return m_index;}
		void SetIndex(int index) {m_index = index;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	public: ///////////////////////////

		ptxList		m_effectInstList;			
		char		m_name[128];
		bool		m_isAnyActivePoints[PTX_NUM_BUFFERS];
		int			m_index;			
				
};
} // namespace rage


#endif // RMPTFX_PTXDRAWLIST_H 
