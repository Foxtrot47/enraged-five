//
// rmptfx/ptxrandomtable.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "ptxrandomtable.h"

// includes (rmptfx)
#include "ptxconfig.h"

// includes (rage)
#include "math/random.h"



// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
using namespace rage;


// static variables
u8 ptxRandomTable::sm_currIndex = 0;
float ptxRandomTable::sm_data[PTXRANDTABLE_SIZE]  = {0.0f};

// code
void ptxRandomTable::Generate()
{
	sm_currIndex = 0;

	for (int i=0; i<PTXRANDTABLE_SIZE; i++)
	{
		sm_data[i] = g_DrawRand.GetRanged(0.0f, 1.0f);
	}
}