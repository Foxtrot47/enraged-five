// 
// rmptfx/ptxbehaviour.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXBEHAVIOUR_H
#define RMPTFX_PTXBEHAVIOUR_H


// includes (us)
#include "rmptfx/ptxbiaslink.h"
#include "rmptfx/ptxkeyframeprop.h"
#include "rmptfx/ptxdraw.h"
#include "rmptfx/ptxpoint.h"

// includes 
#include "data/safestruct.h"


// enumerations
enum ptxDrawType
{
	PTXPARTICLERULE_DRAWTYPE_SPRITE = 0,
	PTXPARTICLERULE_DRAWTYPE_MODEL,
	PTXPARTICLERULE_DRAWTYPE_TRAIL,

	PTXPARTICLERULE_DRAWTYPE_ALL
};


// namespaces
namespace rage 
{


// forward declarations
class mthRandom;
class mthVecRand;
class phWind;
class ptxParticleRule;


// structures
// a struct that will be passed to each update behaviour
// - there is some data that only certain behaviours need - to keep the signature the same, we pass this struct
// - data can be calculated outside of the main while() point update loop, and stored here, for performance gains
// - data can be written from within each behaviour into here, and passed to other update behaviours
struct ptxBehaviourParams
{
	ptxEffectInst* 					pEffectInst;						// pointer to effect instance
	ptxEmitterInst* 				pEmitterInst;						// pointer to emitter instance
	ptxPointItem* 					pPointItem;							// pointer to the point we're interested in

	ptxEvolutionList*				pEvolutionList;						// pointer to the event's evolution list 

	ScalarV							vDeltaTime;							// delta time of the last frame
	ScalarV							vPtxFrameRatio;						// the ratio of the last frame that the particle has been alive

	Vec4V							vModelDimensions;					// original dimensions of a model particle's bound (width, height, depth, radius)

	float							effectSpawnerTriggerRatio;
	int								texFrameIdMin;
	int								texFrameIdMax;

	Vec3V							vEffectWindVelocity;				// the local wind velocity at the camera position
	float							distSqrToCamera;					// the squared distance from the effect to the camera
};


// classes
class ptxBehaviour
{
	///////////////////////////////////
	// FRIENDS 
	///////////////////////////////////
	
	friend class ptxManager;
	friend class ptxParticleRule;
	friend class ptxUpdateTask;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
#if RMPTFX_XML_LOADING
		ptxBehaviour();
#endif

		virtual ~ptxBehaviour() {}

		// resourcing
		ptxBehaviour(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		static void PlaceSubtype(ptxBehaviour* pBehaviour, datResource& rsc);														// not using DECLARE_PLACE to force child classes to make their own Place functions

		// try to stop these needing to be public
		virtual const char* GetName() = 0;
		virtual float GetOrder() = 0;
		u32* GetAccumCPUUpdateTicksPtr() {return &m_accumCPUUpdateTicks;}
		u32 GetAccumCPUUpdateTicks() {return m_accumCPUUpdateTicks;}
		void SetAccumCPUUpdateTicks(u32 val) {m_accumCPUUpdateTicks = val;}

#if RMPTFX_EDITOR
		virtual const char* GetEditorName() {return "unnamed";}																		// name of the behaviour shown in the editor
#endif

	protected: ////////////////////////

		// static
		static void InitClass();

		// virtual interface
		virtual void SetDefaultData() {}
		virtual ptxDrawType GetTypeFilter() {return PTXPARTICLERULE_DRAWTYPE_ALL;}													// filter for deciding which type of particle this behaviour should be active

		virtual bool NeedsToInit() = 0;																								// whether we call init when a particle is created
		virtual bool NeedsToUpdate() = 0;																							// whether we call update on an existing particle
		virtual bool NeedsToUpdateFinalize() = 0;																					// whether we call update finalize on an existing particle
		virtual bool NeedsToDraw() = 0;																								// whether we call draw on an existing particle
		virtual bool NeedsToRelease() = 0;																							// whether we call release when a particle dies
		virtual bool UpdateOnMainThreadOnly() {return false;}																		// whether the behaviour only updates on the main thread (as opposed to the child threads)

		virtual void InitPoint(ptxBehaviourParams& UNUSED_PARAM(params)) {}															// called for each newly created particle if NeedsToInit returns true						
		virtual void UpdatePoint(ptxBehaviourParams& UNUSED_PARAM(params)) {}
		virtual void UpdateFinalize(ptxBehaviourParams& UNUSED_PARAM(params)) {}													// called at the end of update for all points 						
		virtual void DrawPoints(ptxEffectInst* RESTRICT UNUSED_PARAM(pEffectInst),													// called for each active particle to draw them
								ptxEmitterInst* RESTRICT UNUSED_PARAM(pEmitterInst),
								ptxParticleRule* RESTRICT UNUSED_PARAM(pParticleRule),
								ptxEvolutionList* RESTRICT UNUSED_PARAM(pEvoList)
								PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface)) { PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY((void)pMultipleDrawInterface);}
		virtual void ReleasePoint(ptxPointItem* RESTRICT UNUSED_PARAM(pPointItem)) {}												// called when a point is released - never use at the moment - remove?

		virtual void SetKeyframeDefns() {}																							// sets the keyframe definitions on the keyframe properties
		virtual void SetEvolvedKeyframeDefns(ptxEvolutionList* UNUSED_PARAM(pEvolutionList)) {}										// sets the keyframe definitions on the evolved keyframe properties in the evolution list

		void CalcHashName();																										// converts the behaviour name to a hash name - called when saving the resource so the hash name is available on load
		void PrepareEvoData(ptxEvolutionList* pEvoList);																			// prepare keyframes for faster evolution processing
		void SetBiasLinks(atArray<ptxBiasLink>& biasLinks);																			// sets the property bias links

		// queries
		bool HasProp(u32 propHashId);
		__forceinline ScalarV_Out GetBiasValue(ptxKeyframeProp& keyframeProp, u8 randTableIdx) {return m_keyframePropList.GetBiasValue(keyframeProp, randTableIdx);}

		// accessors
		ptxKeyframePropList& GetKeyframePropListRef() {return m_keyframePropList;}
		void IncAccumCPUUpdateTicks(u32 val) {m_accumCPUUpdateTicks += val;}

		// editor
#if RMPTFX_EDITOR
		// static
		static void AddDrawType(const char* pName);
		static int GetNumDrawTypes() {return sm_numDrawTypes;}																		// gets the number of draw types
		static const char* GetDrawTypeName(int idx) {return sm_drawTypeNames[idx];}													// gets the name of a specific draw type
		static ptxDrawType GetDrawType(const char* pName);																			// gets the draw type from the name

		// virtual
		virtual bool IsActive() = 0;																								// whether this behaviour is active or not	

		virtual bool GetHasCustomUI() {return false;}																				// whether this behaviour has a custom editor ui
		virtual const char* GetCustomUIName() {return "none";}																		// the name of the custom editor ui
																							
		virtual void UpdateKeyframeDefns(ptxDrawType UNUSED_PARAM(drawType), const ptxParticleRule* UNUSED_PARAM(pParticleRule)) {}	// updates the keyframe definitions on the keyframe properties (when the draw type changes)

		virtual void SendDefnToEditor(ptxByteBuff& UNUSED_PARAM(buff));																// sends the definition of this behaviour's tuning data to the editor
		virtual void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);																		// sends tuning data to the editor
		virtual void ReceiveTuneDataFromEditor(const ptxByteBuff& UNUSED_PARAM(buff)) {}											// receives tuning data from the editor

		// profiling
		void StartProfileUpdate() {m_accumCPUUpdateTicks = 0;}

		void SendNumDefnsToEditor(ptxByteBuff& buff, u8 val);
		void SendKeyframeDefnToEditor(ptxByteBuff& buff, const char* pName);
		void SendIntDefnToEditor(ptxByteBuff& buff, const char* pName, int min =-10000, int max=10000);
		void SendFloatDefnToEditor(ptxByteBuff& buff, const char* pName, float min =-10000.0f, float max=10000.0f);
		void SendBoolDefnToEditor(ptxByteBuff& buff, const char* pName);
		void SendComboDefnToEditor(ptxByteBuff& buff, const char* pName, int numItems, const char* pItemNames[]);

		void SendKeyframeToEditor(ptxByteBuff& buff, ptxKeyframeProp& keyframeProp, const ptxParticleRule* pParticleRule);
		void SendIntToEditor(ptxByteBuff& buff, int val);
		void SendFloatToEditor(ptxByteBuff& buff, float val);
		void SendBoolToEditor(ptxByteBuff& buff, bool val);
		void SendVecToEditor(ptxByteBuff& buff, const Vec3V &val);
		void SendComboItemToEditor(ptxByteBuff& buff, int val);

		void ReceiveKeyframeFromEditor(const ptxByteBuff& buff, ptxKeyframeProp& keyframeProp);
		void ReceiveIntFromEditor(const ptxByteBuff& buff, int& val);
		void ReceiveFloatFromEditor(const ptxByteBuff& buff, float& val);
		void ReceiveBoolFromEditor(const ptxByteBuff& buff, bool& val);
		void ReceiveVecFromEditor(const ptxByteBuff& buff, Vec3V& val);
		void ReceiveComboItemFromEditor(const ptxByteBuff& buff, int& val);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		u32 m_hashName;


	protected: ////////////////////////
		
		ptxKeyframePropList m_keyframePropList;																						// convenient location for the system to check property ids against and prepared evos
	

	private: //////////////////////////
		
		u32 m_accumCPUUpdateTicks;																									// profiling info
		datPadding<12> m_pad;	

		// editor 
#if RMPTFX_EDITOR
		static int sm_numDrawTypes;																									// the number of draw types
		static atArray<const char*> sm_drawTypeNames;																				// the names of the draw types e.g. "sprite", "mesh", "trail"
#endif
};







} // namespace rage


#endif // RMPTFX_PTXBEHAVIOUR_H 
