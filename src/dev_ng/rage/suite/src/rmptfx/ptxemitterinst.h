// 
// rmptfx/ptxemitterinst.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 
// 

#ifndef RMPTFX_PTXEMITTERINST_H
#define RMPTFX_PTXEMITTERINST_H


// includes (rmptfx)
#include "rmptfx/ptxemitterrule.h"
#include "rmptfx/ptxparticlerule.h"

// includes (rage)
#include "atl/bitset.h"
#include "paging/ref.h"
#include "system/timer.h"
#include "vector/color32.h"
#include "vectormath/classes.h"


// namespaces
namespace rage 
{


// enumerations
enum ptxEmitterInstPlayState
{
	PTXEMITINSTPLAYSTATE_WAITING_TO_START		= 0,
	PTXEMITINSTPLAYSTATE_PLAYING,
	PTXEMITINSTPLAYSTATE_STOPPED,
	PTXEMITINSTPLAYSTATE_FINISHED,

	PTXEMITINSTPLAYSTATE_NUM
};

enum ptxEmitterInstFlags
{
	PTXEMITINSTFLAG_CAN_SPAWN					= 0,						// whether the emitter is allowed to spawn particles - 
	PTXEMITINSTFLAG_HAS_JUST_STARTED,										// whether the emitter is on its first frame since starting
	PTXEMITINSTFLAG_HAS_JUST_LOOPED,										// whether the emitter is on its first frame since looping
	PTXEMITINSTFLAG_ON_MAIN_THREAD_ONLY,
#if RMPTFX_EDITOR
	PTXEMITINSTFLAG_NOT_ACTIVE,
#endif
};


// classes
class ptxEmitterInstDB
{
	friend class ptxEmitterInst;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEmitterInstDB() {Init();}

		void Init() {m_vCurrPosWldLifeRatio=Vec4V(V_ZERO); m_vCurrBoundPosRadius=Vec4V(V_ZERO);}

		// accessors
		__forceinline ScalarV_Out GetCurrLifeRatioV() const {return m_vCurrPosWldLifeRatio.GetW();}
		__forceinline float GetCurrLifeRatio() const {return m_vCurrPosWldLifeRatio.GetWf();}
		__forceinline void SetCurrLifeRatio(float lifeRatio) {m_vCurrPosWldLifeRatio.SetWf(lifeRatio);}
		__forceinline void SetCurrLifeRatioV(ScalarV_In lifeRatio) {m_vCurrPosWldLifeRatio.SetW(lifeRatio);}
		__forceinline Vec3V_Out GetCurrPosWld() const {return m_vCurrPosWldLifeRatio.GetXYZ();}
		__forceinline void SetCurrPosWld(Vec3V_In vPos) {m_vCurrPosWldLifeRatio.SetXYZ(vPos);}

		__forceinline Vec4V_Out GetCurrBoundPosRadius() const {return m_vCurrBoundPosRadius;}
		__forceinline void SetCurrBoundPosRadius(Vec4V_In vBound) {m_vCurrBoundPosRadius = vBound;}
		__forceinline Vec3V_Out GetCurrBoundBoxMin() const {return m_vCurrBoundPosRadius.GetXYZ()-Vec3V(m_vCurrBoundPosRadius.GetW());}
		__forceinline Vec3V_Out GetCurrBoundBoxMax() const {return m_vCurrBoundPosRadius.GetXYZ()+Vec3V(m_vCurrBoundPosRadius.GetW());}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		Vec4V m_vCurrPosWldLifeRatio;										// the current world position of the emitter (from the creation domain)
		Vec4V m_vCurrBoundPosRadius;
};

class ptxEmitterInst
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEmitterInst();
		~ptxEmitterInst() {}

		void Init(ptxEffectInst* pEffectInst);

		// playback functions
		void Start(ptxEffectInst* pEffectInst, ptxEmitterRule* pEmitterRule, ptxParticleRule* pParticleRule, float startRatio, float endRatio, float playbackRateScalar, float zoomScalar, Color32 colourTint);
		void Loop(ptxEffectInst* pEffectInst);
		void Stop(ptxEffectInst* pEffectInst);
		void Relocate(ptxEffectInst* pEffectInst, Vec3V_In vDeltaPos);
		void Finish(ptxEffectInst* pEffectInst);

		// update functions
		void UpdateSetup(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, float dt);
		void UpdateCulled(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList);
		void UpdateParallel(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, float dt);
		bool UpdateFinalize(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, float dt);

		// render functions
		void Draw(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface));
#if RMPTFX_BANK
		void DebugDraw(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, int eventIndex);
#endif

		// memory
		void AllocateMemory(ptxEmitterRule* pEmitterRule);
		void DeallocateMemory();

		// misc functions
		unsigned SortPoints(ptxPointItem** ppSorted);

		// accessors
		void SetEmitterRule(ptxEmitterRule* pEmitterRule) {m_pEmitterRule = pEmitterRule;}
		void SetEmitterRuleRaw(ptxEmitterRule* pEmitterRule) {m_pEmitterRule.ptr = pEmitterRule;}
		ptxEmitterRule* GetEmitterRule() {return m_pEmitterRule.ptr;}
		void SetParticleRule(ptxParticleRule* pParticleRule) {m_pParticleRule = pParticleRule;}
		ptxParticleRule* GetParticleRule() {return m_pParticleRule.ptr;}
		const ptxParticleRule* GetParticleRule() const {return m_pParticleRule.ptr;}

		ptxDomainInst* GetCreationDomainInst() {return &m_creationDomainInst;}
		ptxDomainInst* GetTargetDomainInst() {return &m_targetDomainInst;}
		ptxDomainInst* GetAttractorDomainInst() {return m_pAttractorDomainInst;}
		void SetAttractorDomainInst(ptxDomainInst* pDomainInst) {m_pAttractorDomainInst = pDomainInst;}
		Mat34V_Out GetCreationDomainMtxWld(Mat34V_In vEffectInstMtx) {Mat34V vMtxWld; Transform(vMtxWld, GetCreationDomainLclToWldMtx(vEffectInstMtx), m_creationDomainInst.GetMtxLcl()); return vMtxWld;}
		Mat34V_Out GetTargetDomainMtxWld(Mat34V_In vEffectInstMtx) {Mat34V vMtxWld; Transform(vMtxWld, GetTargetDomainLclToWldMtx(vEffectInstMtx), m_targetDomainInst.GetMtxLcl()); return vMtxWld;}
		Mat34V_Out GetAttractorDomainMtxWld(Mat34V_In vEffectInstMtx) {Mat34V vMtxWld; Transform(vMtxWld, GetAttractorDomainLclToWldMtx(vEffectInstMtx), m_pAttractorDomainInst->GetMtxLcl()); return vMtxWld;}

		ptxList& GetPointList() {return m_pointList;}
		int GetNumActivePointsMT(int bufferId) const {return m_pointList.GetNumItemsMT(bufferId);}
		int GetNumActivePoints() const {return m_pointList.GetNumItems();}

		__forceinline Vec4V_Out GetColourTintRGBA() const {return m_colourTint.GetRGBA();}
		__forceinline float GetZoomScalar() const {return m_zoomScalar;}

		__forceinline Vec3V_Out GetPrevPosWld() const {return m_vPrevPosWld;}

		ptxEmitterInstPlayState GetPlayState() const {return m_playState;}

		void SetFlag(ptxEmitterInstFlags flag) {m_flags.Set(flag);}
		void ClearFlag(ptxEmitterInstFlags flag) {m_flags.Clear(flag);}
		bool GetFlag(ptxEmitterInstFlags flag) const {return m_flags.IsSet(flag);}
		void CopyFlagsFrom(const ptxEmitterInst& emitterInst) {m_flags = emitterInst.m_flags;}
		atFixedBitSet<32>* GetFlagsPtr() {return &m_flags;}
		u32 GetFlagsSize() const {return sizeof(m_flags);}

		ScalarV_Out GetKeyTime() const {return Clamp(m_dataDB.GetCurrLifeRatioV(), ScalarV(V_ZERO), ScalarV(V_ONE));}
		ptxEmitterInstDB* GetDataDB() {return &m_dataDB;}
		const ptxEmitterInstDB* GetDataDB() const {return &m_dataDB;}

		ptxEmitterInstDB* GetDataDBMT(int bufferId) {return &m_dataDBMT[bufferId];}
		const ptxEmitterInstDB* GetDataDBMT(int bufferId) const {return &m_dataDBMT[bufferId];}

		//void GetAmbientScales(float& natAmbScale, float& artAmbScale) const;
		//void SetAmbientScales(float natAmbScale, float artAmbScale);

		// debug
#if RMPTFX_THOROUGH_ERROR_CHECKING
		void VerifyLists();
#endif


	private: //////////////////////////

#if RMPTFX_BANK
		void OutputUpdateProfileInfo(ptxEffectInst* pEffectInst, const char* pFunctionName, sysTimer& timer);
		bool ShouldOutputPlayStateInfo(ptxEffectInst* pEffectInst);
		void OutputPlayStateFunction(ptxEffectInst* pEffectInst, const char* pFunctionName);
		void SetPlayState(ptxEffectInst* pEffectInst, ptxEmitterInstPlayState val);
#else
		void SetPlayState(ptxEffectInst* UNUSED_PARAM(pEffectInst), ptxEmitterInstPlayState val) {m_playState = val;}
#endif

		bool GetIsContinous() const {return m_startRatio==0.0f && m_endRatio==1.0f && m_pEmitterRule->GetIsOneShot()==false;}

		void UpdateDomains(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, ScalarV_In vKeyTimeEmitter);
		void UpdateDomain(ptxDomainType domainType, ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, ScalarV_In vKeyTimeEmitter);
		void UpdateBounds(ptxEffectInst* pEffectInst);

		Mat34V_Out GetCreationDomainLclToWldMtx(Mat34V_In vEffectInstMtx) const; 

		Mat34V_Out GetTargetDomainLclToWldMtx(Mat34V_In vEffectInstMtx);

		Mat34V_Out GetAttractorDomainLclToWldMtx(Mat34V_In vEffectInstMtx);

		void SpawnPoints(ptxEffectInst* pEffectInst, ptxEvolutionList* pEvoList, float dt);
		void InitPoint(ptxEffectInst* pEffectInst, ptxPointItem* pPointItem, ptxEvolutionList* pEvolutionList, ScalarV_In dt, float playbackRate, ScalarV_In subFrameRatio, Vec3V_In vSubFrameEffectPos);

		void CopyToUpdateBuffer();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////
		
		// points
		ptxList m_pointList;												// list of points spawned by this emitter 

		// state
		Vec3V m_vPrevPosWld;												// the previous world position of the emitter (from the creation domain) (NOTE: the current position is double buffered)

		float m_prevLifeRatio;												// the previous ratio through the emitter life
		float m_currSpawnCount;												// current accumulated number of points to spawn - points get spawned when this is 1.0 or above
		float m_startRatio;													// the ratio through the effect duration when this emitter starts
		float m_endRatio;													// the ratio through the effect duration when this emitter ends

		ptxEmitterInstPlayState m_playState;								// the current play state (playing, finished etc)

		atFixedBitSet<32> m_flags;											// emitter flags

		// settings
		Color32 m_colourTint;												// colour to tint the points with
		float m_playbackRateScalar;											// emitter playback rate scalar
		float m_zoomScalar;													// emitter zoom scalar
		
		// rules
		pgRef<ptxEmitterRule> m_pEmitterRule;								// pointer to the emitter rule
		pgRef<ptxParticleRule> m_pParticleRule;								// pointer to the particle rule

		// domains
		ptxDomainInst* m_pAttractorDomainInst;								// the current state of the attractor domain (if we have one)
		ptxDomainInst m_creationDomainInst;									// the current state of the creation domain
		ptxDomainInst m_targetDomainInst;									// the current state of the target domain

		// double buffered data
		ptxEmitterInstDB m_dataDB;											// double buffered emitter data
		ptxEmitterInstDB m_dataDBMT[PTX_NUM_BUFFERS];

		// debug
#if RMPTFX_BANK
		bool m_outputPlayStateDebug;
#endif
		
		// state (cont)
		//u8 m_ambientScale[2];
};


} //namespace rage


#endif // RMPTFX_PTXEMITTERINST_H
