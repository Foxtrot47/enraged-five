<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxRenderState" cond="RMPTFX_XML_LOADING">
	<int name="m_blendSet"/>
	<int name="m_lightingMode"/>
	<bool name="m_depthWrite"/>
	<bool name="m_depthTest"/>
	<bool name="m_alphaBlend"/>
	<int name="m_cullMode"/>
</structdef>

</ParserSchema>