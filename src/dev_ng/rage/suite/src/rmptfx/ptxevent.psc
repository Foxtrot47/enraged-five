<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="rage::ptxEvent" cond="RMPTFX_XML_LOADING">
	<float name="m_startRatio"/>
	<float name="m_endRatio"/>
</structdef>

<structdef type="rage::ptxEventEmitter" base="rage::ptxEvent" onPreSave="OnPreSave" cond="RMPTFX_XML_LOADING">
	<string name="m_emitterRuleName" type="ConstString"/>
	<string name="m_particleRuleName" type="ConstString"/>
	<float name="m_playbackRateScalarMin"/>
	<float name="m_playbackRateScalarMax"/>
	<float name="m_zoomScalarMin" init="1.0"/>
	<float name="m_zoomScalarMax" init="1.0"/>
	<Color32 name="m_colourTintMin" init="-1"/>
	<Color32 name="m_colourTintMax" init="-1"/>
	<pointer name="m_pEvolutionList" type="rage::ptxEvolutionList" policy="owner"/>
</structdef>

</ParserSchema>