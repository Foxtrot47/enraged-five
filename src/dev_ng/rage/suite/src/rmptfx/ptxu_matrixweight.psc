<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_MatrixWeight" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_mtxWeightKFP" type="::rage::ptxKeyframeProp"/>
	<int name="m_referenceSpace"/>
</structdef>

</ParserSchema>