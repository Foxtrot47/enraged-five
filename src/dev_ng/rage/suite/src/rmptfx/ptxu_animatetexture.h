// 
// rmptfx/ptxu_animatetexture.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_ANIMATETEXTURE_H 
#define PTXU_ANIMATETEXTURE_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// enumerations
enum ptxu_AnimateTexture_KeyframeMode
{
	PTXU_ANIMATETEXTURE_KEYFRAMEMODE_POINT			= 0,
	PTXU_ANIMATETEXTURE_KEYFRAMEMODE_EFFECT,
	PTXU_ANIMATETEXTURE_KEYFRAMEMODE_EMITTER,

	PTXU_ANIMATETEXTURE_KEYFRAMEMODE_NUM
};

enum ptxu_AnimateTexture_LoopMode
{
	PTXU_ANIMATETEXTURE_LOOPMODE_SAME				= 0,
	PTXU_ANIMATETEXTURE_LOOPMODE_ZERO,
	PTXU_ANIMATETEXTURE_LOOPMODE_MIN,
	PTXU_ANIMATETEXTURE_LOOPMODE_MAX,
	PTXU_ANIMATETEXTURE_LOOPMODE_RAND_MIN_MAX,

	PTXU_ANIMATETEXTURE_LOOPMODE_NUM
};


// classes
class ptxu_AnimateTexture : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_AnimateTexture();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_AnimateTexture(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_AnimateTexture);

		// info
		const char* GetName() {return "ptxu_AnimateTexture";}											// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_ANIMATETEXTURE;}										// the ordering of the behaviour (see ptxconfig.h)
		ptxDrawType GetTypeFilter() {return PTXPARTICLERULE_DRAWTYPE_SPRITE;}							// filter for deciding which type of particle this behaviour should be active

		bool NeedsToInit() {return true;}																// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}																// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}													// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}																// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}															// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);														// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);													// called from InitPoint and UpdatePoint to update the particle accordingly 
		void UpdateTextures(ptxBehaviourParams& params);												// called when a texture blend is complete and the new source and destination textures need calculated

		void SetKeyframeDefns();																		// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);									// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Animate Texture";}											// name of the behaviour shown in the editor
		bool IsActive();																				// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);														// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);				// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);										// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_animRateKFP;

		// non keyframe data
		int m_keyframeMode;

		int m_lastFrameId;
		int m_loopMode;

		bool m_isRandomised;
		bool m_isScaledOverParticleLife;
		bool m_isHeldOnLastFrame;
		bool m_doFrameBlending;

		//datPadding<12> m_pad;

		// static data
		static ptxKeyframeDefn sm_animRateDefn;

};


} // namespace rage


#endif // PTXU_ANIMATETEXTURE_H 
