// 
// rmptfx/ptxu_acceleration.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "rmptfx/ptxu_acceleration.h"
#include "rmptfx/ptxu_acceleration_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"

#if !__RESOURCECOMPILER && !RSG_TOOL
#include "vfx/ptfx/ptfxcallbacks.h"
#endif


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Acceleration::sm_xyzMinDefn(				"Acceleration Min",			atHashValue("ptxu_Acceleration:m_xyzMinKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-10000.0f,10000.0f,	0.01f),		"Min Accn X",	"Min Accn Y",	"Min Accn Z");
ptxKeyframeDefn ptxu_Acceleration::sm_xyzMaxDefn(				"Acceleration Max",			atHashValue("ptxu_Acceleration:m_xyzMaxKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-10000.0f,10000.0f,	0.01f),		"Max Accn X",	"Max Accn Y",	"Max Accn Z");																												


// code
#if RMPTFX_XML_LOADING
ptxu_Acceleration::ptxu_Acceleration()
{
	// create keyframes
	m_xyzMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Acceleration:m_xyzMinKFP"));
	m_xyzMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Acceleration:m_xyzMaxKFP"));

	m_keyframePropList.AddKeyframeProp(&m_xyzMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_xyzMaxKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();

	// init global data
	m_pGlobalData_NOTUSED = NULL;
}
#endif

void ptxu_Acceleration::SetDefaultData()
{
	// default keyframes
	m_xyzMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_xyzMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_referenceSpace = PTXU_ACCELERATION_REFERENCESPACE_WORLD;
	m_isAffectedByZoom = true;
	m_enableGravity = false;

}

ptxu_Acceleration::ptxu_Acceleration(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_xyzMinKFP(rsc)
	, m_xyzMaxKFP(rsc)
{
	m_pGlobalData_NOTUSED = NULL;

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Acceleration_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Acceleration::DeclareStruct(datTypeStruct& s)
{
	m_pGlobalData_NOTUSED = NULL; 

	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Acceleration, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Acceleration, m_xyzMinKFP)
		SSTRUCT_FIELD(ptxu_Acceleration, m_xyzMaxKFP)
		SSTRUCT_IGNORE(ptxu_Acceleration, m_pGlobalData_NOTUSED)
		SSTRUCT_FIELD(ptxu_Acceleration, m_referenceSpace)
		SSTRUCT_FIELD(ptxu_Acceleration, m_isAffectedByZoom)
		SSTRUCT_FIELD(ptxu_Acceleration, m_enableGravity)
		SSTRUCT_IGNORE(ptxu_Acceleration, m_pad)
	SSTRUCT_END(ptxu_Acceleration)

	m_pGlobalData_NOTUSED = NULL;
}
#endif

IMPLEMENT_PLACE(ptxu_Acceleration);

void ptxu_Acceleration::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointAccn)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Acceleration::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	
	ScalarV	vDeltaTime = params.vDeltaTime;

	ScalarV vZoom = ScalarVFromF32(pEffectInst->GetFinalZoom());
	const ScalarV ptxZoomScalar = ScalarVFromF32(pEmitterInst->GetZoomScalar());

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// cache data from the point
	ScalarV	vPlaybackRate = ScalarVFromF32(pPointSB->GetPlaybackRate());

	// get the particle and emitter life ratio key times
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();
	ScalarV vKeyTimePointEmitter = ScalarVFromF32(pPointSB->GetEmitterLifeRatio());

	// cache some data that we'll need later
	ptxEmitterRule*	RESTRICT pEmitterRule = static_cast<ptxEmitterRule*>(pEmitterInst->GetEmitterRule());
	const ScalarV vOne = ScalarV(V_ONE);	
	const ScalarV vDiv100 = ScalarV(V_FLT_SMALL_2);
#if __RESOURCECOMPILER  || RSG_TOOL
	const ScalarV vGravity = vOne;
#else
	const ScalarV vGravity = -ScalarVFromF32(g_pPtfxCallbacks->GetGravitationalAcceleration());
#endif

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vPtxAccelerationMin = m_xyzMinKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vPtxAccelerationMax = m_xyzMaxKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vEmitAcceleration = pEmitterRule->GetAccnScalarKFP().Query(vKeyTimePointEmitter, pEvolutionList, pPointSB->GetEvoValues()) * vDiv100;

	// calculate the bias value
	ScalarV vBias = GetBiasValue(m_xyzMinKFP, pPointSB->GetRandIndex());

	// calculate the lerped acceleration between the min and max keyframe data
	Vec4V vAcceleration = Lerp(vBias, vPtxAccelerationMin, vPtxAccelerationMax);
	ptxAssertf(IsFiniteAll(vAcceleration), "vAcceleration is not finite (1)");

	// scale this by the emit acceleration
	vAcceleration *= vEmitAcceleration;
	ptxAssertf(IsFiniteAll(vAcceleration), "vAcceleration is not finite (2)");

	vZoom *= ptxZoomScalar;
	// apply any zoom to the acceleration
	if (Unlikely(m_isAffectedByZoom))
	{
		vAcceleration *= vZoom;
		ptxAssertf(IsFiniteAll(vAcceleration), "vAcceleration is not finite (3)");
	}

	// calc the acceleration in effect space if required
	if (m_referenceSpace!=PTXU_ACCELERATION_REFERENCESPACE_WORLD)
	{
		Mat34V vMtx = Mat34V(V_IDENTITY);
		if (m_referenceSpace==PTXU_ACCELERATION_REFERENCESPACE_EFFECT)
		{
			vMtx = pEffectInst->GetMatrix();
		}
		else if (m_referenceSpace==PTXU_ACCELERATION_REFERENCESPACE_EMITTER)
		{
			vMtx = pEmitterInst->GetCreationDomainMtxWld(pEffectInst->GetMatrix());
		}
		else
		{
			ptxAssertf(0, "unsupported reference space");
		}

		// rotate the acceleration into effect/emitter space
		Vec3V vAccelerationNewSpace;
		vAccelerationNewSpace = Transform3x3(vMtx, vAcceleration.GetXYZ());
		ptxAssertf(IsFiniteAll(vAccelerationNewSpace), "vAccelerationNewSpace is not finite");

		// override the accleration with this
		vAcceleration = Vec4V(vAccelerationNewSpace, vOne);
		ptxAssertf(IsFiniteAll(vAcceleration), "vAcceleration is not finite (4)");
	}

	// scale by gravity
	if (m_enableGravity)
	{
		vAcceleration *= Vec4V(vOne, vOne, vGravity, vOne);
		ptxAssertf(IsFiniteAll(vAcceleration), "vAcceleration is not finite (5)");
	}

	// scale the particle's velocity
	Vec3V vVelInc = vAcceleration.GetXYZ() * Vec3V(vDeltaTime) * vPlaybackRate;
	ptxAssertf(IsFiniteAll(vVelInc), "vVelInc is not finite");
	if (IsFiniteAll(vVelInc))
	{
		if (!pPointSB->GetFlag(PTXPOINT_FLAG_AT_REST))
		{
			pPointDB->IncCurrVel(vVelInc);
		}
	}
}

void ptxu_Acceleration::SetKeyframeDefns()
{
	m_xyzMinKFP.GetKeyframe().SetDefn(&sm_xyzMinDefn);
	m_xyzMaxKFP.GetKeyframe().SetDefn(&sm_xyzMaxDefn);
}

void ptxu_Acceleration::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_xyzMinKFP, &sm_xyzMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_xyzMaxKFP, &sm_xyzMaxDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Acceleration::IsActive()
{
	ptxu_Acceleration* pAcceleration = static_cast<ptxu_Acceleration*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_xyzMinKFP == pAcceleration->m_xyzMinKFP &&
				   m_xyzMaxKFP == pAcceleration->m_xyzMaxKFP && 
				   m_referenceSpace == pAcceleration->m_referenceSpace &&
				   m_isAffectedByZoom == pAcceleration->m_isAffectedByZoom &&
				   m_enableGravity == pAcceleration->m_enableGravity;

	return !isEqual;
}

void ptxu_Acceleration::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 5);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Acceleration Min");
	SendKeyframeDefnToEditor(buff, "Acceleration Max");

	// send non-keyframes
	const char* pComboItemNames[PTXU_ACCELERATION_REFERENCESPACE_NUM] = {"World", "Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Reference Space", PTXU_ACCELERATION_REFERENCESPACE_NUM, pComboItemNames);

	SendBoolDefnToEditor(buff, "Zoom Affects Acceleration");
	SendBoolDefnToEditor(buff, "Scale Z With Game Gravity");
}

void ptxu_Acceleration::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_xyzMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_xyzMaxKFP, pParticleRule);

	// send non-keyframes
	SendComboItemToEditor(buff, m_referenceSpace);
	SendBoolToEditor(buff, m_isAffectedByZoom);
	SendBoolToEditor(buff, m_enableGravity);

}

void ptxu_Acceleration::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_xyzMinKFP);
	ReceiveKeyframeFromEditor(buff, m_xyzMaxKFP);

	// receive non-keyframes
	ReceiveComboItemFromEditor(buff, m_referenceSpace);
	ReceiveBoolFromEditor(buff, m_isAffectedByZoom);
	ReceiveBoolFromEditor(buff, m_enableGravity);

}
#endif // RMPTFX_EDITOR


} // namespace rage
