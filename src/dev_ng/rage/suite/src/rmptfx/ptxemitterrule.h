// 
// rmptfx/ptxemitterrule.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXEMITTERRULE_H
#define RMPTFX_PTXEMITTERRULE_H


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxdomain.h"
#include "rmptfx/ptxkeyframeprop.h"
#include "rmptfx/ptxpoint.h"

// includes (rage)
#include "paging/base.h"
#include "grprofile/drawcore.h"


// namespaces
namespace rage 
{


// forward declarations
class ptxEmitterInst;
class ptxParticleRule;


// defines
#define PTXEMITTERRULE_RORC_VERSION		(8)
#define PTXEMITTERRULE_FILE_VERSION		(4.1f)


// structures
#if RMPTFX_EDITOR
class ptxEmitterRuleUIData
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxEmitterRuleUIData();

		void StartProfileUpdate();
		void StartProfileDraw();

		void IncAccumCPUUpdateTime(float val) {m_accumCPUUpdateTime+=val;}
		void IncAccumCPUDrawTime(float val) {m_accumCPUDrawTime+=val;}
		void IncAccumCPUSortTime(float val) {m_accumCPUSortTime+=val;}
		void IncNumPointsUpdated(int val) {m_numPointsUpdated+=val;}
		void IncNumActiveInstances(int val) {m_numActiveInstances+=val;}

		float GetAccumCPUUpdateTime() {return m_accumCPUUpdateTime;}
		float GetAccumCPUDrawTime() {return m_accumCPUDrawTime;}
		float GetAccumCPUSortTime() {return m_accumCPUSortTime;}
		int GetNumPointsUpdated() {return m_numPointsUpdated;}
		int GetNumActiveInstances() {return m_numActiveInstances;}

		bool GetShowCreationDomain() {return m_showCreationDomain;}
		bool GetShowTargetDomain() {return m_showTargetDomain;}
		bool GetShowAttractorDomain() {return m_showAttractorDomain;}

		void SendDebugDataToEditor(ptxByteBuff& buff);
		void ReceiveDebugDataFromEditor(ptxByteBuff& buff);

		void AccumulateFrom(ptxEmitterRuleUIData& uiData);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		// profile data
		float m_accumCPUUpdateTime;											// 
		float m_accumCPUDrawTime;											//
		float m_accumCPUSortTime;											//
		int m_numPointsUpdated;												//
		int m_numActiveInstances;											//

		// debug data
		bool m_showCreationDomain;											// whether to draw the creation domain info
		bool m_showTargetDomain;											// whether to draw the target domain info
		bool m_showAttractorDomain;											// whether to draw the attractor domain info
};
#endif


// classes
class ptxEmitterRule : public pgBaseRefCounted
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEmitterRule();
		~ptxEmitterRule();

		// resourcing
		ptxEmitterRule(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxEmitterRule);

		// hide implementation of these functions in pgBase
		static void InitClass() {}
		static void ShutdownClass() {}

		// init functions
		void Init();

		// evolutions 
		void PrepareEvoData(ptxEvolutionList* pEvoList);

		// keyframe definitions
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);
		void SetKeyframeDefns();

		// accessors
		float GetFileVersion() {return m_fileVersion;}
		const char* GetName() const {return m_name;}
		void SetName(const char* pNewName);
		bool GetIsOneShot() {return m_isOneShot;}
		
		ptxDomain* GetCreationDomain() {return m_creationDomainObj.GetDomain();}
		ptxDomain* GetTargetDomain() {return m_targetDomainObj.GetDomain();}
		ptxDomain* GetAttractorDomain() {return m_attractorDomainObj.GetDomain();}
		ptxDomainObj& GetCreationDomainObj() {return m_creationDomainObj;}
		ptxDomainObj& GetTargetDomainObj() {return m_targetDomainObj;}
		ptxDomainObj& GetAttractorDomainObj() {return m_attractorDomainObj;}

		ptxKeyframeProp& GetSpawnRateOverTimeKFP() {return m_spawnRateOverTimeKFP;}
		ptxKeyframeProp& GetSpawnRateOverDistKFP() {return m_spawnRateOverDistKFP;}
		ptxKeyframeProp& GetParticleLifeKFP() {return m_particleLifeKFP;}
		ptxKeyframeProp& GetPlaybackRateScalarKFP() {return m_playbackRateScalarKFP;}
		ptxKeyframeProp& GetSpeedScalarKFP() {return m_speedScalarKFP;}
		ptxKeyframeProp& GetSizeScalarKFP() {return m_sizeScalarKFP;}
		ptxKeyframeProp& GetAccnScalarKFP() {return m_accnScalarKFP;}
		ptxKeyframeProp& GetDampeningScalarKFP() {return m_dampeningScalarKFP;}
		ptxKeyframeProp& GetMatrixWeightScalarKFP() {return m_matrixWeightScalarKFP;}
		ptxKeyframeProp& GetInheritVelocityKFP() {return m_inheritVelocityKFP;}

		ptxKeyframePropList& GetKeyframePropList() {return m_keyframePropList;}

		// referencing
		bool IsInUse() {return GetRefCount()>1;}							// 1 ref is for the manager

		// xml loading / saving
#if RMPTFX_XML_LOADING
		static ptxEmitterRule* CreateFromFile(const char* pFilename);
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
		void Save();
#endif

		// editor
#if RMPTFX_EDITOR
		void RescaleZoomableComponents(float scalar, ptxEvolutionList* pEvolutionList);
		ptxKeyframeProp* GetKeyframeProp(u32 keyframePropId);

		void CreateUiObjects();
		void UpdateUIData(ptxEvolutionList* pEvoList, const ptxEffectRule* pEffectRule, s32 eventIdx);

		void ToggleAttractorDomain();

		void SendToEditor(ptxByteBuff& buff);
		void ReceiveFromEditor(ptxByteBuff& buff);

		// profiling
		ptxEmitterRuleUIData& GetUIData() {return *m_pUIData;}
#endif

		// debug or editor
#if RMPTFX_BANK || RMPTFX_EDITOR
		void RemoveDefaultKeyframes();
#endif

		// debug
#if RMPTFX_BANK
		void GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def);
		void OutputKeyframeEntryInfo();
#endif

		// xml loading
#if RMPTFX_XML_LOADING
		void RepairAndUpgradeData(const char* pFilename);
		void ResolveReferences() {}
#endif

	private: //////////////////////////

		// misc
		void RemoveDependents(bool UNUSED_PARAM(removeFromManager)) {}

		// xml loading
#if RMPTFX_XML_LOADING
		void OnPreLoad(parTreeNode* UNUSED_PARAM(pNode)) {}
		PAR_PARSABLE
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////
		float m_fileVersion;												// the emitter rule's file version
		ConstString m_name;													// the name of the emitter rule
		void* m_pLastEvoList_NOTUSED;										// it's a void* cause you're not supposed to do anything with it but check its value - could be a hash

#if RMPTFX_EDITOR
		ptxEmitterRuleUIData* m_pUIData;									// profile and debug data used by the editor
#else
		void* m_pUIData;													// fake editor data (to avoid different size resources)
#endif

		ptxDomainObj m_creationDomainObj;									// the emitter rule's creation domain (defines where points get created - i.e. sets their initial position)
		ptxDomainObj m_targetDomainObj;										// the emitter rule's target domain (defines where points initially travel towards - i.e. sets their initial velocity)
		ptxDomainObj m_attractorDomainObj;									// the emitter rule's attractor domain (defines an area where particle with an attractor behaviour will be attracted towards)

		datPadding<12> m_pad;

		ptxKeyframeProp m_spawnRateOverTimeKFP;								// keyframeable rate at which the emitter spawns points over time
		ptxKeyframeProp m_spawnRateOverDistKFP;								// keyframeable rate at which the emitter spawns points over distance
		ptxKeyframeProp m_particleLifeKFP;									// keyframeable	life of the emitted points
		ptxKeyframeProp m_playbackRateScalarKFP;							// keyframeable playback rate scalar of the emitted points
		ptxKeyframeProp m_speedScalarKFP;									// keyframeable speed scalar of the emitted points
		ptxKeyframeProp m_sizeScalarKFP;									// keyframeable size scalar of the emitted points
		ptxKeyframeProp m_accnScalarKFP;									// keyframeable acceleration scalar of the emitted points
		ptxKeyframeProp m_dampeningScalarKFP;								// keyframeable dampening scalar of the emitted points 
		ptxKeyframeProp m_matrixWeightScalarKFP;							// keyframeable matrix weight scalar of the emitted points
		ptxKeyframeProp m_inheritVelocityKFP;								// keyframeable amount of velocity that the emitted points inherit from the emitter

		ptxKeyframePropList m_keyframePropList;								// list of keyframeable properties

		bool m_isOneShot;													// whether this emitter spawns all of its points in its first frame of life
		datPadding<7> m_pad2;

		// static variables
		static ptxKeyframeDefn sm_spawnRateOverTimeDefn;					// keyframeable property definitions
		static ptxKeyframeDefn sm_spawnRateOverDistDefn;
		static ptxKeyframeDefn sm_particleLifeDefn;
		static ptxKeyframeDefn sm_playbackRateScalarDefn;
		static ptxKeyframeDefn sm_speedScalarDefn;
		static ptxKeyframeDefn sm_sizeScalarDefn;
		static ptxKeyframeDefn sm_accnScalarDefn;
		static ptxKeyframeDefn sm_dampeningScalarDefn;
		static ptxKeyframeDefn sm_matrixWeightScalarDefn;
		static ptxKeyframeDefn sm_inheritVelocityDefn;

};


} //namespace rage


#endif // RMPTFX_PTXEMITTERRULE_H
