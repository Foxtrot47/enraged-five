// 
// rmptfx/ptxu_velocity.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_VELOCITY_H 
#define PTXU_VELOCITY_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// classes
class ptxu_Velocity : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Velocity();
#endif
		void SetDefaultData();

		// resourcing
		ptxu_Velocity(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Velocity);

		// info
		const char* GetName() {return "ptxu_Velocity";}									// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_VELOCITY;}								// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}												// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}												// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}									// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}												// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}											// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);										// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);									// called from InitPoint and UpdatePoint to update the particle accordingly 								

#if RMPTFX_EDITOR
		// editor specific
		bool IsActive() {return true;}													// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& UNUSED_PARAM(buff)) {}						// sends the definition of this behaviour's tuning data to the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif



	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// static data

};


} // namespace rage


#endif // PTXU_VELOCITY_H 
