// 
// rmptfx/ptxeffectspawning.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_EFFECTSPAWNING_H
#define RMPTFX_EFFECTSPAWNING_H


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxpoint.h"


// namespaces
namespace rage 
{


// forward declarations
class ptxEffectInst;
class ptxEffectRule;


// enumerations
enum ptxSpawnedEffectScalarFlags
{
	PTXSPAWNSCALAR_ACTIVE_DURATION,
	PTXSPAWNSCALAR_ACTIVE_PLAYBACK_RATE,
	PTXSPAWNSCALAR_ACTIVE_COLOUR_TINT,
	PTXSPAWNSCALAR_ACTIVE_ZOOM,
	PTXSPAWNSCALAR_ACTIVE_SIZE_SCALAR,

	PTXSPAWNSCALAR_ACTIVE_NUM
};


// classes
class ptxSpawnedEffectScalars
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxSpawnedEffectScalars();
		~ptxSpawnedEffectScalars(){}

		// resourcing
		ptxSpawnedEffectScalars(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxSpawnedEffectScalars);

		void Reset();

		inline bool GetIsDurationScalarActive() const {return m_flags.IsSet(PTXSPAWNSCALAR_ACTIVE_DURATION);}
		inline bool GetIsPlaybackRateScalarActive() const {return m_flags.IsSet(PTXSPAWNSCALAR_ACTIVE_PLAYBACK_RATE);}
		inline bool GetIsColourTintScalarActive() const {return m_flags.IsSet(PTXSPAWNSCALAR_ACTIVE_COLOUR_TINT);}
		inline bool GetIsZoomScalarActive() const {return m_flags.IsSet(PTXSPAWNSCALAR_ACTIVE_ZOOM);}

		inline float GetDurationScalar() const {return m_durationScalar;}
		inline float GetPlaybackRateScalar() const {return m_playbackRateScalar;}
		inline Color32 GetColourTintScalar() const {return m_colourTintScalar;}
		inline float GetZoomScalar() const {return m_zoomScalar;}

		void SetRandomly(const ptxSpawnedEffectScalars& min, const ptxSpawnedEffectScalars& max);

		// editor
#if RMPTFX_EDITOR
		static void SendMinMaxToEditor(class ptxByteBuff& buff, ptxSpawnedEffectScalars& min, ptxSpawnedEffectScalars& max);
		static void ReceiveMinMaxFromEditor(const class ptxByteBuff& buff, ptxSpawnedEffectScalars& min, ptxSpawnedEffectScalars& max);
#endif


	private: //////////////////////////

		inline void SetDurationScalar(float val) {m_flags.Set(PTXSPAWNSCALAR_ACTIVE_DURATION); m_durationScalar = val;}
		inline void SetPlaybackRateScalar(float val) {m_flags.Set(PTXSPAWNSCALAR_ACTIVE_PLAYBACK_RATE); m_playbackRateScalar = val;}
		inline void SetColourTintScalar(Color32 val) {m_flags.Set(PTXSPAWNSCALAR_ACTIVE_COLOUR_TINT); m_colourTintScalar = val;}
		inline void SetZoomScalar(float val) {m_flags.Set(PTXSPAWNSCALAR_ACTIVE_ZOOM); m_zoomScalar = val;}

		// parser
#if RMPTFX_XML_LOADING
		PAR_SIMPLE_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		float m_durationScalar;
		float m_playbackRateScalar;
		Color32 m_colourTintScalar;
		float m_zoomScalar;

		atFixedBitSet<PTXSPAWNSCALAR_ACTIVE_NUM> m_flags;

		datPadding<12> m_pad;


};


class ptxEffectSpawner : public pgBase
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxEffectSpawner();

		// resourcing
		ptxEffectSpawner(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxEffectSpawner);

		// main interface
		void Trigger(ptxPointItem* pPointItem, ptxEffectInst* pEffectInst, bool isColnTrigger);

		// accessors
		ptxEffectRule* GetEffectRule() {return m_pEffectRule;}
		void SetTriggerRatio(float val) {m_triggerInfo = val;}
		float GetTriggerRatio() {return m_triggerInfo;}
		bool GetTracksPointPos() {return m_tracksPointPos;}
		bool GetTracksPointDir() {return m_tracksPointDir;}
		bool GetTracksPointNegDir() {return m_tracksPointNegDir;}

		// editor
#if RMPTFX_EDITOR
		void SendToEditor(ptxByteBuff& buff);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif

		// xml loading
#if RMPTFX_XML_LOADING
		void ResolveReferences();
#endif


	private: //////////////////////////

		// xml loading
#if RMPTFX_XML_LOADING
		void PreLoad(parTreeNode* pNode);
		void PostSave(parTreeNode* pNode);

		// parser
		PAR_SIMPLE_PARSABLE
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		datPadding<8>	m_pad;
		ptxSpawnedEffectScalars m_spawnedEffectScalarsMin;				// min spawned effect scalar settings
		ptxSpawnedEffectScalars m_spawnedEffectScalarsMax;				// max spawned effect scalar settings
		datRef<class ptxEffectRule> m_pEffectRule;						// pointer to the effect rule that gets spawned
		ConstString m_name;												// the name of the effect rule that gets spawned - rsTODO: get rid of this (and others like it) if we do a 2 pass dependency check then load when loading xml data
		float m_triggerInfo;											// trigger info (either the ratio through the point life when the effect gets spawned or the velocity threshold of an effect that gets spawned on collision)
		bool m_inheritsPointLife;										// whether the spawned effect inherits the life of the point
		bool m_tracksPointPos;											// whether the spawned effect tracks the position of the point
		bool m_tracksPointDir;											// whether the spawned effect tracks the direction of the point
		bool m_tracksPointNegDir;										// whether the spawned effect tracks the negative direction of the point

};


} // namespace rage


#endif // RMPTFX_EFFECTSPAWNING_H

