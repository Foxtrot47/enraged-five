// 
// rmptfx/ptxdomain.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXDOMAIN_H
#define RMPTFX_PTXDOMAIN_H


// includes (rmptfx)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxkeyframeprop.h"

// includes (rage)
#include "grprofile/drawcore.h"


// namespaces
namespace rage 
{


// forward declarations
class ptxDomain;
class ptxEmitterRule;
class ptxByteBuff;


// enumerations
enum ptxDomainType
{
	PTXDOMAIN_TYPE_CREATION			= 0,
	PTXDOMAIN_TYPE_TARGET,
	PTXDOMAIN_TYPE_ATTRACTOR,

	PTXDOMAIN_TYPE_NUM
};

enum ptxDomainShape
{
	PTXDOMAIN_SHAPE_INVALID			= -1,

	PTXDOMAIN_SHAPE_BOX				= 0, 
	PTXDOMAIN_SHAPE_SPHERE,
	PTXDOMAIN_SHAPE_CYLINDER,
	PTXDOMAIN_SHAPE_ATTRACTOR,
	
	PTXDOMAIN_SHAPE_NUM
};

enum ptxDomainProperty
{
	PTXDOMAIN_PROP_POSITION			= 0,
	PTXDOMAIN_PROP_ROTATION,
	PTXDOMAIN_PROP_SIZE_OUTER,
	PTXDOMAIN_PROP_SIZE_INNER,

	PTXDOMAIN_PROP_NUM
};


// classes
class ptxDomainInst
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxDomainInst();

		// main interface
		void Update(ptxDomainType domainType, ptxDomain* pDomain, ScalarV_In vKeyTimeEmitter, Vec3V_In vScale, ptxEvolutionList* pEvolutionList, ptxEvoValueType* pEvoValues, Vec3V_In overrideSize);

		// accessors
		Vec3V_Out GetPosLcl() {return m_vCurrPos;}
		Mat34V_Out GetMtxLcl() {Mat34V vMtx = Mat34V(V_IDENTITY); Mat34VFromEulersXYZ(vMtx, m_vCurrRot*ScalarVFromF32(DtoR)); vMtx.SetCol3(m_vCurrPos);	return vMtx;}
		Vec3V_Out GetCurrSizeOuter() {return m_vCurrSizeOuter;}
		Vec3V_Out GetCurrSizeInner() {return m_vCurrSizeInner;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		Vec3V m_vCurrPos;
		Vec3V m_vCurrRot;
		Vec3V m_vCurrSizeOuter;
		Vec3V m_vCurrSizeInner;

};


class ptxDomain : datBase
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxDomain(ptxDomainType type=PTXDOMAIN_TYPE_CREATION, ptxDomainShape shape=PTXDOMAIN_SHAPE_INVALID);
		virtual ~ptxDomain() {}

		// resourcing
		ptxDomain(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxDomain);

		// evolutions
		void PrepareEvoData(ptxEvolutionList* pEvoList) {m_keyframePropList.PrepareEvoData(pEvoList);}

		// keyframe definitions
		static void InitKeyframeDefns();
		virtual void SetKeyframeDefns() {}
		virtual void SetEvolvedKeyframeDefns(ptxEvolutionList* UNUSED_PARAM(pEvolutionList)) {}

		// factory 
		static ptxDomain* CreateDomain(ptxDomainType type, ptxDomainShape shape);

		// queries
		virtual Vec3V_Out GetRandomPoint(Mat34V_In UNUSED_PARAM(vMtxLcl), Vec3V_In UNUSED_PARAM(vSizeOuter), Vec3V_In UNUSED_PARAM(vSizeInner)) {return Vec3V(V_ZERO);}

		// parser
		void OnPreLoad(parTreeNode* pNode);
		void OnPostLoad();

		// accessors
		ptxDomainType GetType() {return (ptxDomainType)m_type;}
		ptxDomainShape GetShape() {return (ptxDomainShape)m_shape;}
		void SetShape(ptxDomainShape shape) {m_shape = shape;}

		ptxKeyframeProp& GetPositionKFP() {return m_positionKFP;}
		ptxKeyframeProp& GetRotationKFP() {return m_rotationKFP;}
		ptxKeyframeProp& GetSizeOuterKFP() {return m_sizeOuterKFP;}
		ptxKeyframeProp& GetSizeInnerKFP() {return m_sizeInnerKFP;}

		bool GetIsWorldSpace() {return m_isWorldSpace;}
		void SetIsWorldSpace(bool val) {m_isWorldSpace = val;}
		bool GetIsPointRelative() {return m_isPointRelative;}
		void SetIsPointRelative(bool val) {m_isPointRelative = val;}
		bool GetIsCreationRelative() {return m_isCreationRelative;}
		void SetIsCreationRelative(bool val) {m_isCreationRelative = val;}
		bool GetIsTargetRelative() {return m_isTargetRelative;}
		void SetIsTargetRelative(bool val) {m_isTargetRelative = val;}

		// editor
#if RMPTFX_EDITOR
		static const char* GetShapeName(ptxDomainShape shape);
		static u32 GetTypeFilter(ptxDomainShape shape);
		ptxKeyframePropList& GetKeyframePropList() {return m_keyframePropList;}
		ptxKeyframeProp* GetKeyframeProp(u32 propId) {return m_keyframePropList.GetKeyframePropByPropId(propId);}

		virtual void SendToEditor(ptxByteBuff& buff, const ptxEmitterRule* pEmitterRule);
		virtual void ReceiveFromEditor(ptxByteBuff& buff);

		virtual void RescaleZoomableComponents(float scalar, ptxEvolutionList* pEvolutionList);

#endif  
		
		// debug or editor
#if RMPTFX_BANK || RMPTFX_EDITOR
		void RemoveDefaultKeyframes();
#endif

		// debug
#if RMPTFX_BANK		
		virtual void Draw(Mat34V_In UNUSED_PARAM(vMtxWld), Vec3V_In UNUSED_PARAM(vSizeOuter), Vec3V_In UNUSED_PARAM(vSizeInner)) {}

		void GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def);
		void OutputKeyframeEntryInfo();
#endif


	protected: ////////////////////////

		static ptxKeyframeDefn sm_propertyDefns[PTXDOMAIN_TYPE_NUM][PTXDOMAIN_SHAPE_NUM][PTXDOMAIN_PROP_NUM];		// keyframeable property definitions


	private: //////////////////////////

#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	protected: ////////////////////////

		int m_type;															// the type of domain (creation, target or attractor)
		int m_shape;														// the shape of the domain (box, sphere, cylinder, attractor)

		bool m_isWorldSpace;												// whether the domain rotation is in world space or effect space (the position is always in effect space)
		bool m_isPointRelative;												// whether the (target) domain is relative to the effect or point
		bool m_isCreationRelative;											// whether the (target or attractor) domain is relative to the creation domain
		bool m_isTargetRelative;											// whether the (attractor) domain is relative to the target domain

		ptxKeyframeProp m_positionKFP;										// keyframeable position of the domain
		ptxKeyframeProp m_rotationKFP;										// keyframeable rotation of the domain
		ptxKeyframeProp m_sizeOuterKFP;										// keyframeable outer size of the domain
		ptxKeyframeProp m_sizeInnerKFP;										// keyframeable inner size of the domain


	private: //////////////////////////

		float m_fileVersion;												// this domain's file version
		ptxKeyframePropList m_keyframePropList;								// list of keyframeable properties

		datPadding<4> m_pad2;

};

class ptxDomainObj
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxDomainObj() : m_pDomain(NULL) {}
		~ptxDomainObj()	{if (m_pDomain) delete m_pDomain;}

		// resourcing
		ptxDomainObj(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxDomainObj);

		// evolutions
		void PrepareEvoData(ptxEvolutionList* pEvoList) {if (m_pDomain) m_pDomain->PrepareEvoData(pEvoList);}

		// accessors
		ptxDomain* GetDomain() {return m_pDomain;}
		void SetDomain(ptxDomain* pDomain) {if (m_pDomain) delete m_pDomain; m_pDomain=pDomain;}

		// editor
#if RMPTFX_EDITOR
		void SetNewDomainShape(int val) {m_newDomainShape = val;}
		int GetNewDomainShape() {return m_newDomainShape;}

		static void DomainShapeChangedCB(ptxDomainObj* pDomainObj);
		ptxKeyframeProp* GetKeyframeProp(u32 propId) {return m_pDomain ? m_pDomain->GetKeyframeProp(propId) : NULL;}

		void SendToEditor(ptxByteBuff& buff, const ptxEmitterRule* pEmitterRule);
		void ReceiveFromEditor(ptxByteBuff& buff);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_SIMPLE_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		datOwner<ptxDomain> m_pDomain;										// the actual domain
		ATTR_UNUSED int m_newDomainShape;									// the new domain shape we want to convert to (this is resourced)
};

class ptxDomainBox : public ptxDomain
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxDomainBox(ptxDomainType type=PTXDOMAIN_TYPE_CREATION);
		virtual ~ptxDomainBox() {}

		// resourcing
		ptxDomainBox(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxDomainBox);

		// virtual overrides
		Vec3V_Out GetRandomPoint(Mat34V_In vMtxLcl, Vec3V_In vSizeOuter, Vec3V_In vSizeInner);

		void SetKeyframeDefns();
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);

		// editor
#if RMPTFX_BANK
		void Draw(Mat34V_In vMtxWld, Vec3V_In vSizeOuter, Vec3V_In vSizeInner);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////


};

class ptxDomainSphere : public ptxDomain
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxDomainSphere(ptxDomainType type=PTXDOMAIN_TYPE_CREATION);
		virtual ~ptxDomainSphere() {}

		// resourcing
		ptxDomainSphere(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxDomainSphere);

		// virtual overrides
		Vec3V_Out GetRandomPoint(Mat34V_In vMtxLcl, Vec3V_In vSizeOuter, Vec3V_In vSizeInner);

		void SetKeyframeDefns();
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);

		// editor
#if RMPTFX_BANK
		void Draw(Mat34V_In vMtxWld, Vec3V_In vSizeOuter, Vec3V_In vSizeInner);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////


};

class ptxDomainCylinder : public ptxDomain
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxDomainCylinder(ptxDomainType type=PTXDOMAIN_TYPE_CREATION);
		virtual ~ptxDomainCylinder() {}

		// resourcing
		ptxDomainCylinder(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxDomainCylinder);

		// virtual overrides
		Vec3V_Out GetRandomPoint(Mat34V_In vMtxLcl, Vec3V_In vSizeOuter, Vec3V_In vSizeInner);

		void SetKeyframeDefns();
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);

		// editor
#if RMPTFX_BANK
		void Draw(Mat34V_In vMtxWld, Vec3V_In vSizeOuter, Vec3V_In vSizeInner);
		void DrawCylinder(float length, float sizeX, float sizeZ, Mat34V_In vMtxWld, Color32 colour, int numSteps);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////


};

class ptxDomainAttractor : public ptxDomain
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxDomainAttractor(ptxDomainType type=PTXDOMAIN_TYPE_CREATION);
		virtual ~ptxDomainAttractor() {}

		// resourcing
		ptxDomainAttractor(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxDomainAttractor);

		// virtual overrides
		void SetKeyframeDefns();
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);

		// editor
#if RMPTFX_BANK
		void Draw(Mat34V_In vMtxWld, Vec3V_In vSizeOuter, Vec3V_In vSizeInner);
		void DrawAttractor(float radius, float length, Mat34V_In vMtxWld, Color32 colour, int numSteps);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////


};


} //namespace rage


#endif // RMPTFX_PTXDOMAIN_H
