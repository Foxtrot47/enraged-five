// 
// rmptfx/ptxdrawcommon.h
// 
// Copyright (C) 2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXDRAWCOMMON_H
#define RMPTFX_PTXDRAWCOMMON_H

#if defined(__GTA_COMMON_FXH) // shader code
	#define PTXDRAW_SHADER_CODE 1
	#define PTXDRAW_ENGINE_CODE 0
	#define PTXDRAW_SHADER_ONLY(x) x
	#define PTXDRAW_ENGINE_ONLY(x)
#else // engine code
	#define PTXDRAW_SHADER_CODE 0
	#define PTXDRAW_ENGINE_CODE 1
	#define PTXDRAW_SHADER_ONLY(x)
	#define PTXDRAW_ENGINE_ONLY(x) x
#endif

#define PTXGPU_CAMANGLE_FORWARD_MIN			(0.05f)
#define PTXGPU_CAMANGLE_FORWARD_MAX			(0.4f)
#define PTXGPU_CAMANGLE_FORWARD_INV_RANGE	(1.0f/(PTXGPU_CAMANGLE_FORWARD_MAX - PTXGPU_CAMANGLE_FORWARD_MIN))
#define PTXGPU_CAMANGLE_UP_MIN				(0.75f)
#define PTXGPU_CAMANGLE_UP_MAX				(1.0f)
#define PTXGPU_CAMANGLE_UP_INV_RANGE		(1.0f/(PTXGPU_CAMANGLE_UP_MAX - PTXGPU_CAMANGLE_UP_MIN))


#endif // RMPTFX_PTXDRAWCOMMON_H
