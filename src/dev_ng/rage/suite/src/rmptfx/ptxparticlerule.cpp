// 
// rmptfx/ptxparticlerule.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxparticlerule.h"
#include "rmptfx/ptxparticlerule_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxu_wind.h"
#include "rmptfx/ptxclipregion.h"
#include "rmptfx/ptxd_drawsprite.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxinterface.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxu_animatetexture.h"
#include "rmptfx/ptxu_colour.h"

// includes (rage)
#include "bank/msgbox.h"
#include "diag/output.h"
#include "file/asset.h"
#include "parser/manager.h"
#include "pheffects/wind.h"
#include "system/cache.h"
#include "system/timer.h"


// defines
#define PTXSTR_TREENODE_TRIGGERTYPE				"type"
#define PTXSTR_TREENODE_EFFECTNAME				"name"


// optimisations
RMPTFX_OPTIMISATIONS()


// params
#if !__FINAL
XPARAM(ptfxassetoverride);
#endif

// namespaces
namespace rage
{


// defines
#ifndef STRINGIFY
#define STRINGIFY(x) #x
#endif


// static variables
float ptxParticleRule::sm_fileVersion = 1.0f;
#if !__FINAL
sysSpinLockToken ptxParticleRule::sm_updateProfileLock = SYS_SPINLOCK_UNLOCKED;
#endif


// code
ptxParticleRule::ptxParticleRule() : pgBaseRefCounted(0)
{
	CreateUIData();

	m_fileVersion = sm_fileVersion;
#if __RESOURCECOMPILER
	m_pLastEvoList_NOTUSED = NULL;
#else
	m_pLastEvoList_NOTUSED = (void*)0xFFFFFFFF; // not even NULL, to make sure we get a "cache miss" the first time it runs
#endif
	m_runtimeFlags.Reset();

	m_effectSpawnerAtRatio.SetTriggerRatio(0.0f);
	m_effectSpawnerOnColn.SetTriggerRatio(1.0f);

	m_texFrameIdMin = 0;
	m_texFrameIdMax = 0;
	m_flags = 0;

	// initialise the bias links with a random table entry
	for (int i=0; i<m_biasLinks.GetCount(); i++)
	{
		m_biasLinks[i].GenerateRandom();
	}

	m_sortType = PTXPARTICLERULE_SORTTYPE_FARTHEST_FIRST;
	m_drawType = PTXPARTICLERULE_DRAWTYPE_SPRITE;

	m_pWindBehaviour = NULL;
}

ptxParticleRule::~ptxParticleRule()
{
	delete[] (char*)m_pUIData;
	ShutdownBehaviours();
}

ptxParticleRule::ptxParticleRule(datResource& rsc)
: pgBaseRefCounted(rsc)
, m_effectSpawnerAtRatio(rsc)
, m_effectSpawnerOnColn(rsc)
, m_renderState(rsc)
, m_name(rsc)
, m_allBehaviours(rsc, 1)
, m_initBehaviours(rsc, 1)
, m_updateBehaviours(rsc, 1)
, m_updateFinalizeBehaviours(rsc, 1)
, m_drawBehaviours(rsc, 1)
, m_releaseBehaviours(rsc, 1)
, m_biasLinks(rsc, 1)
, m_pPointPool(rsc)
, m_funcTable_NOTUSED(rsc)
, m_shaderInst(rsc)
, m_drawables(rsc, 1)
{	
	// doing this manually to call PlaceSubtypes instead of Place
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		ptxBehaviour::PlaceSubtype(m_allBehaviours[i].ptr, rsc);
	}


	if (!datResource_IsDefragmentation)
	{
		m_pLastEvoList_NOTUSED = (void*)0xFFFFFFFF; // not even NULL, to make sure we get a "cache miss" the first time it runs
		m_runtimeFlags.Reset();

		for (int i=0; i<m_allBehaviours.GetCount(); i++)
		{
			m_allBehaviours[i]->SetKeyframeDefns();
		}

		// set the bias links
		SetBiasLinks();

		SetPointPool(&RMPTFXMGR.GetPointPool());

		// set runtime flags
		SetupRuntimeFlags();
		//initialize the flags here as we are loading it the first time. Post load will be
		//called which sets this flag correctly. If we are doing a defrag (in the else part)
		// we should NOT be resetting this flag.
		m_flags = 0;
	}

	m_pWindBehaviour = static_cast<ptxu_Wind*>(GetBehaviour("ptxu_Wind"));

	m_pLastEvoList_NOTUSED = (void*)0xFFFFFFFF;

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxParticleRule_num++;
#endif

}

void ptxParticleRule::PostLoad()
{

#if RMPTFX_EDITOR
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		m_allBehaviours[i]->UpdateKeyframeDefns((ptxDrawType)m_drawType, this);
	}
#endif

	CreateUIData();
	m_shaderInst.PostLoad();
	for(int i = 0; i < m_drawables.GetCount(); i++)
	{
		m_drawables[i].PostLoad();
	}

	if(RMPTFXMGR.GetParticleRuleInitFunctor())
	{
		RMPTFXMGR.GetParticleRuleInitFunctor()(this);
	}

}

#if __DECLARESTRUCT
void ptxParticleRule::DeclareStruct(datTypeStruct& s)
{
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		datTypeStruct ss;
		m_allBehaviours[i]->CalcHashName();
		m_allBehaviours[i]->DeclareStruct(ss);
	}

	m_pPointPool = NULL;
	m_pWindBehaviour = NULL;

	pgBaseRefCounted::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxParticleRule, pgBaseRefCounted)
		SSTRUCT_FIELD_VP(ptxParticleRule, m_pUIData)
		SSTRUCT_FIELD(ptxParticleRule, m_effectSpawnerAtRatio)
		SSTRUCT_FIELD(ptxParticleRule, m_effectSpawnerOnColn)
		SSTRUCT_FIELD(ptxParticleRule, m_renderState)
		SSTRUCT_FIELD(ptxParticleRule, m_fileVersion)
		SSTRUCT_FIELD(ptxParticleRule, m_texFrameIdMin)
		SSTRUCT_FIELD(ptxParticleRule, m_texFrameIdMax)
		SSTRUCT_FIELD(ptxParticleRule, m_name)
		SSTRUCT_FIELD(ptxParticleRule, m_allBehaviours)
		SSTRUCT_FIELD(ptxParticleRule, m_initBehaviours)
		SSTRUCT_FIELD(ptxParticleRule, m_updateBehaviours)
		SSTRUCT_FIELD(ptxParticleRule, m_updateFinalizeBehaviours)
		SSTRUCT_FIELD(ptxParticleRule, m_drawBehaviours)
		SSTRUCT_FIELD(ptxParticleRule, m_releaseBehaviours)
		SSTRUCT_FIELD(ptxParticleRule, m_biasLinks)
		SSTRUCT_IGNORE(ptxParticleRule, m_pPointPool)
		SSTRUCT_FIELD(ptxParticleRule, m_funcTable_NOTUSED)
		SSTRUCT_FIELD(ptxParticleRule, m_shaderInst)
		SSTRUCT_FIELD(ptxParticleRule, m_drawables)
		SSTRUCT_FIELD(ptxParticleRule, m_sortType)
		SSTRUCT_FIELD(ptxParticleRule, m_drawType)
		SSTRUCT_IGNORE(ptxParticleRule, m_flags)
		SSTRUCT_FIELD(ptxParticleRule, m_runtimeFlags)
		SSTRUCT_IGNORE(ptxParticleRule, m_pLastEvoList_NOTUSED)
		SSTRUCT_IGNORE(ptxParticleRule, m_pWindBehaviour)
		SSTRUCT_IGNORE(ptxParticleRule, m_pad2)
	SSTRUCT_END(ptxParticleRule)
}
#endif // __DECLARESTRUCT

IMPLEMENT_PLACE(ptxParticleRule);

void ptxParticleRule::Init()
{
	InitDefaultBehaviours();
	BuildBehaviourLists();


#if RMPTFX_EDITOR
	if (m_drawables.GetCapacity()==0)
	{
		m_drawables.Reserve(16);
	}
#endif

	m_shaderInst.SetShaderTemplate(ptxShaderTemplateList::GetDefaultShaderName(), NULL);
}

void ptxParticleRule::SetupRuntimeFlags()
{
	m_runtimeFlags.Reset();

	// set runtime flags
	for (s32 i=0; i<m_updateBehaviours.size(); i++)
	{
		if (m_updateBehaviours[i]->UpdateOnMainThreadOnly())
		{
			m_runtimeFlags.Set(PTXPARTICLERULE_ON_MAIN_THREAD_ONLY);
		}
	}

	ptxd_Sprite* pPtxSpriteDrawBehavior = static_cast<ptxd_Sprite*>(GetBehaviour("ptxd_Sprite"));
	if(pPtxSpriteDrawBehavior && pPtxSpriteDrawBehavior->m_isScreenSpace)
	{
		m_runtimeFlags.Set(PTXPARTICLERULE_SCREENSPACE);
	}

}

ptxPointSB* ptxParticleRule::InitPoint(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxPointItem* pPointItem, ptxEvolutionList* pEvoList, ScalarV_In ptxFrameRatio, ScalarV_In dt)
{
	// set up the behaviour parameters
	ptxBehaviourParams params;
	SetParams(params, pEffectInst, pEmitterInst, pEvoList, pPointItem, dt, ScalarV(V_ONE)-ptxFrameRatio, RMPTFXTHREADER.GetUpdateBufferId());
	SetParamsPointItem(params, pPointItem);

	// set the point as being initialised
	pPointItem->GetDataSB()->SetFlag(PTXPOINT_FLAG_IS_BEING_INITIALISED);

	for (int i=0; i<m_initBehaviours.GetCount(); i++)
	{
		m_initBehaviours[i]->PrepareEvoData(pEvoList);
	}

	// initialise the behaviours
	for (int i=0; i<m_initBehaviours.GetCount(); i++)
	{
		m_initBehaviours[i]->InitPoint(params);
	}

	// clear the point as being initialised
	pPointItem->GetDataSB()->ClearFlag(PTXPOINT_FLAG_IS_BEING_INITIALISED);

	return NULL;
}

bool ptxParticleRule::ShouldBeRendered()
{
	if(RMPTFXMGR.GetParticleRuleShouldBeRenderedFunctor())
	{
		return RMPTFXMGR.GetParticleRuleShouldBeRenderedFunctor()(this);
	}

	return true;
}

void ptxParticleRule::UpdateParallel(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxEvolutionList* pEvoList, float dt)
{
#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif

	// cache data for use later
	ScalarV vDeltaTime = ScalarVFromF32(dt);

	// get the first point 
	ptxPointItem* RESTRICT pPointItem = (ptxPointItem*)pEmitterInst->GetPointList().GetHead();
	if (pPointItem==NULL)
	{
		// return if no active points
		return;
	}

	int updateBufferIdx = RMPTFXTHREADER.GetUpdateBufferId();

	// set up the behaviour parameters
	ptxBehaviourParams params;
	SetParams(params, pEffectInst, pEmitterInst, pEvoList, NULL, vDeltaTime, ScalarV(V_ONE), updateBufferIdx);

	ptxPointItem* RESTRICT pCurrPointItem;
	ptxPointDB* RESTRICT pPointDB;

	// prepare all the behaviour properties for this evolution list
	// We need to call PrepareEvoData on the particle update task threads
	// So leaving the following condition check commented out
	//if (RMPTFXMGR.GetUpdateThreadId()==sysIpcGetCurrentThreadId())
	{
		// call this even if the evolution list is NULL
		for (int i=0; i<m_updateBehaviours.GetCount(); i++)
		{
			m_updateBehaviours[i]->PrepareEvoData(pEvoList);
		}
	}

#if RMPTFX_EDITOR
	sysTimer timer2;
#endif

	// go through each point
	pCurrPointItem = pPointItem;
	while (pCurrPointItem)
	{
		// set up the point in the params
		SetParamsPointItem(params, pCurrPointItem);

		// get the next point
		ptxPointItem* pNextPointItem = (ptxPointItem*)pCurrPointItem->GetNext();

		// prefetch next point
		PrefetchObject(pNextPointItem);
		PrefetchDC(pCurrPointItem->GetDataDBMT(updateBufferIdx));

		// get the point's double buffered data
		pPointDB = pCurrPointItem->GetDataDB();

		// store the previous position of the point
		pPointDB->SetPrevPos(pPointDB->GetCurrPos());

		// update the behaviours set 0
		for (int i=0; i<m_updateBehaviours.GetCount(); i++)
		{
			if (Likely(pPointDB->GetIsDead()==false))
			{
#if RMPTFX_EDITOR
				timer2.Reset();
#endif

				m_updateBehaviours[i]->UpdatePoint(params);

#if RMPTFX_EDITOR
				m_updateBehaviours[i]->IncAccumCPUUpdateTicks((u32)timer2.GetTickTime());
#endif
			}
		}

		// buffer the data
		pCurrPointItem->CopyToUpdateBuffer();
		pCurrPointItem = pNextPointItem;
	}
}

void ptxParticleRule::UpdateFinalize(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxPointItem* pPointItem, ptxEvolutionList* pEvoList, float dt)
{
#if __ASSERT
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		ptxAssertf(m_allBehaviours[i]->m_hashName!=0, "behaviour has an invalid hash name");
	}
#endif

	int numUpdateFinalizeBehaviours = m_updateFinalizeBehaviours.GetCount();
	if (numUpdateFinalizeBehaviours>0)
	{
		// set up the behaviour parameters
		ptxBehaviourParams params;
		SetParams(params, pEffectInst, pEmitterInst, pEvoList, pPointItem, ScalarVFromF32(dt), ScalarV(V_ONE), RMPTFXTHREADER.GetUpdateBufferId());

		for (int i=0; i<numUpdateFinalizeBehaviours; i++)
		{
			m_updateFinalizeBehaviours[i]->UpdateFinalize(params);
		}
	}
}

void ptxParticleRule::Draw(ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxEvolutionList* pEvoList PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* pMultipleDrawInterface))
{
#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif

	// draw any draw behaviours we have
	for (u32 i=0; i<(u32)m_drawBehaviours.size(); i++)
	{
#if RMPTFX_EDITOR
		sysTimer timer;
		timer.Reset();
#endif
		// prepare the evolution data - call this even if the evolution list is NULL
		m_drawBehaviours[i]->PrepareEvoData(pEvoList);

		// draw 
		m_drawBehaviours[i]->DrawPoints(pEffectInst, pEmitterInst, this, pEvoList PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(pMultipleDrawInterface));

#if RMPTFX_EDITOR
		m_drawBehaviours[i]->IncAccumCPUUpdateTicks((u32)timer.GetTickTime());
#endif
	}
}

void ptxParticleRule::SetPointPool(ptxPointPool* pPointPool)
{
	m_pPointPool = pPointPool;
	ptxAssertf(m_pPointPool && m_pPointPool->GetItemsPtr() , "invalid point pool");
}

ptxPointPool* ptxParticleRule::GetPointPool() const
{
	ptxAssertf(m_pPointPool && m_pPointPool->GetItemsPtr() , "invalid point pool");
	return m_pPointPool;
}

int	ptxParticleRule::GetNumPointsAvailable()
{
	ptxAssertf(m_pPointPool, "invalid point pool");
	return m_pPointPool->GetNumItemsFree();
}

ptxPointItem* ptxParticleRule::CreateNewPoint(ptxList* pList)
{
	return m_pPointPool->GetNewItem(pList);
}

ptxPointItem* ptxParticleRule::ReservePoints(ptxList* pList, int &numPoints)
{
	//returns the first new point from the list
	return ((numPoints>0)?m_pPointPool->GetNewItems(pList, numPoints):NULL);

}

void ptxParticleRule::ReturnPoint(ptxList* pPointList, ptxPointItem* pPointItem)
{
	for (s32 i=0; i<m_releaseBehaviours.size(); i++)
	{
		// release the point for any behaviour that needs to
		m_releaseBehaviours[i]->ReleasePoint(pPointItem);
	}		

	// return the dead point
	m_pPointPool->ReturnItem(pPointList, pPointItem);
}

#if RMPTFX_XML_LOADING
void ptxParticleRule::RepairAndUpgradeData(const char* pFilename)
{
	// set the name of the particle rule
	SetName(pFilename);

	// check for out of data files
	if (m_fileVersion<sm_fileVersion)
	{
		ptxWarningf("%s.ptxrule is out of date - please resave from the particle editor!", pFilename);
	}

	m_shaderInst.RepairAndUpgradeData();
}

void ptxParticleRule::ResolveReferences()
{
	// build the behaviour lists
	BuildBehaviourLists();

	// initialise the shader data
	m_shaderInst.ResolveReferences();

	// load any models
	for (int i=0; i<m_drawables.GetCount(); i++)
	{
		m_drawables[i].SetDrawable(RMPTFXMGR.LoadModel(m_drawables[i].GetName()));
		m_drawables[i].SetInfo();
	}

	// set the bias links
	SetBiasLinks();

	SetPointPool(&RMPTFXMGR.GetPointPool());

	m_effectSpawnerAtRatio.ResolveReferences();	
	m_effectSpawnerOnColn.ResolveReferences();
}
#endif // RMTPFX_XML_LOADING

#if RMPTFX_EDITOR
void ptxParticleRule::CreateUiObjects()
{
	// update the editor keyframe definitions
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		m_allBehaviours[i]->UpdateKeyframeDefns((ptxDrawType)m_drawType, this);
	}

}
#endif 

#if RMPTFX_XML_LOADING
ptxParticleRule* ptxParticleRule::CreateFromFile(const char* pFilename)
{
	ptxParticleRule* pParticleRule = NULL;
	ASSET.PushFolder(RMPTFXMGR.GetAssetPath());
	ASSET.PushFolder("ptxrules");
	PARSER.LoadObjectPtr(pFilename, "ptxrule", pParticleRule);
	ASSET.PopFolder();
	ASSET.PopFolder();

	if (pParticleRule)
	{
		pParticleRule->RepairAndUpgradeData(pFilename);
		pParticleRule->ResolveReferences();
#if RMPTFX_EDITOR
		pParticleRule->CreateUiObjects();
#endif
	}

	return pParticleRule;
}
#endif // RMPTFX_XML_LOADING

#if RMPTFX_EDITOR
ptxParticleRule* ptxParticleRule::Clone()
{
	// create a new particle rule
	ptxParticleRule* pClonedParticleRule = rage_new ptxParticleRule;

	// copy over our data
	*pClonedParticleRule = *this;

	// copy behaviours
	for (int i=0; i <pClonedParticleRule->m_allBehaviours.size(); i++)
	{
		pClonedParticleRule->m_allBehaviours[i] = RMPTFX_CLONE_BEHAVIOUR(pClonedParticleRule->m_allBehaviours[i]);
	}

	// re-reference pointers, duplicate shader data etc
	ResolveReferences();
	CreateUiObjects();

	return pClonedParticleRule;
}
#endif // RMPTFX_EDITOR

#if (RMPTFX_BANK || RMPTFX_EDITOR)
void ptxParticleRule::RemoveDefaultKeyframes()
{
	ptxInstVars& instVars = m_shaderInst.GetShaderVars();
	for (int i=0; i<instVars.GetCount(); i++)
	{
		ptxShaderVar* pShaderVar = instVars[i];
		if (pShaderVar && pShaderVar->GetType()==PTXSHADERVAR_KEYFRAME)
		{
			ptxShaderVarKeyframe* pShaderVarKF = reinterpret_cast<ptxShaderVarKeyframe*>(pShaderVar);
			if (pShaderVarKF)
			{
				ptxKeyframe& keyframe = pShaderVarKF->GetKeyframe();
				if (keyframe.IsDefault())
				{
					keyframe.DeleteEntry(0);
				}
			}
		}
	}

	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		ptxKeyframePropList& keyframePropList = m_allBehaviours[i]->GetKeyframePropListRef();
		for (int j=0; j<keyframePropList.GetNumKeyframeProps(); j++)
		{
			ptxKeyframeProp* pKeyframeProp = keyframePropList.GetKeyframePropByIndex(j);
			if (pKeyframeProp)
			{
				ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();
				if (keyframe.IsDefault())
				{
					keyframe.DeleteEntry(0);
				}
			}
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxParticleRule::GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def)
{
	ptxInstVars& instVars = m_shaderInst.GetShaderVars();
	for (int i=0; i<instVars.GetCount(); i++)
	{
		ptxShaderVar* pShaderVar = instVars[i];
		if (pShaderVar && pShaderVar->GetType()==PTXSHADERVAR_KEYFRAME)
		{
			ptxShaderVarKeyframe* pShaderVarKF = reinterpret_cast<ptxShaderVarKeyframe*>(pShaderVar);
			if (pShaderVarKF)
			{
				ptxKeyframe& keyframe = pShaderVarKF->GetKeyframe();

				int currNum = keyframe.GetNumEntries();

				num += currNum;
				mem += keyframe.GetEntryMem();

				if (currNum>max)
				{
					max = currNum;
				}

				if (keyframe.IsDefault())
				{
					def++;
				}
			}
		}
	}

	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		ptxKeyframePropList& keyframePropList = m_allBehaviours[i]->GetKeyframePropListRef();
		for (int j=0; j<keyframePropList.GetNumKeyframeProps(); j++)
		{
			ptxKeyframeProp* pKeyframeProp = keyframePropList.GetKeyframePropByIndex(j);
			if (pKeyframeProp)
			{
				ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();

				int currNum = keyframe.GetNumEntries();

				num += currNum;
				mem += keyframe.GetEntryMem();

				if (currNum>max)
				{
					max = currNum;
				}

				if (keyframe.IsDefault())
				{
					def++;
				}
			}
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxParticleRule::OutputKeyframeEntryInfo()
{
	ptxInstVars& instVars = m_shaderInst.GetShaderVars();
	for (int i=0; i<instVars.GetCount(); i++)
	{
		ptxShaderVar* pShaderVar = instVars[i];
		if (pShaderVar && pShaderVar->GetType()==PTXSHADERVAR_KEYFRAME)
		{
			ptxShaderVarKeyframe* pShaderVarKF = reinterpret_cast<ptxShaderVarKeyframe*>(pShaderVar);
			if (pShaderVarKF)
			{
				ptxKeyframe& keyframe = pShaderVarKF->GetKeyframe();
				ptxKeyframeDefn* pKeyframeDefn = keyframe.GetDefn();
				if (ptxVerifyf(pKeyframeDefn, "keyframe doesn't have a definition"))
				{
					ptxDebugf1("                  %d - %s (%u)", keyframe.GetNumEntries(), pKeyframeDefn->GetName(), pKeyframeDefn->GetPropertyId());
				}
			}
		}
	}

	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		ptxKeyframePropList& keyframePropList = m_allBehaviours[i]->GetKeyframePropListRef();
		for (int j=0; j<keyframePropList.GetNumKeyframeProps(); j++)
		{
			ptxKeyframeProp* pKeyframeProp = keyframePropList.GetKeyframePropByIndex(j);
			if (pKeyframeProp)
			{
				ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();
				ptxKeyframeDefn* pKeyframeDefn = keyframe.GetDefn();
				if (ptxVerifyf(pKeyframeDefn, "keyframe doesn't have a definition"))
				{
					ptxAssertf(pKeyframeProp->GetPropertyId()==pKeyframeDefn->GetPropertyId(), "property ids don't match");
					ptxDebugf1("                  %d - %s (%u)", keyframe.GetNumEntries(), pKeyframeDefn->GetName(), pKeyframeDefn->GetPropertyId());
				}
			}
		}
	}
}
#endif

#if RMPTFX_XML_LOADING
void ptxParticleRule::Save()
{
#if RMPTFX_EDITOR
	RemoveDefaultKeyframes();
#endif

	// set the file version
	m_fileVersion = sm_fileVersion;

	// make sure the behaviour hash names are correct
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		m_allBehaviours[i]->CalcHashName();
	}

	// validate all behaviours so that we don't have active ones with default data 
#if RMPTFX_EDITOR
	ValidateAllBehaviours();
#endif

	// remove any models from any non model types
	if (m_drawType!=PTXPARTICLERULE_DRAWTYPE_MODEL)
	{
		RemoveAllModels();
	}

	// save out the particle rule
	ASSET.PushFolder(RMPTFXMGR.GetAssetPath());
	ASSET.PushFolder("ptxrules");

#if !__FINAL
	bool folderOverrideUsed = false;
	const char* pPtfxAssetOverride = NULL;
	PARAM_ptfxassetoverride.Get(pPtfxAssetOverride);
	if (pPtfxAssetOverride)
	{
		ASSET.PushFolder(pPtfxAssetOverride);
		if (ASSET.Exists(GetName(), "ptxrule"))
		{
			folderOverrideUsed = true;
		}
		else
		{
			ASSET.PopFolder();
		}
	}
#endif

	ASSET.CreateLeadingPath(GetName());
#if RMPTFX_EDITOR
	bool res = 
#endif
	PARSER.SaveObject(GetName(), "ptxrule", this);

#if !__FINAL
	if (folderOverrideUsed)
	{
		ASSET.PopFolder();
	}
#endif

	ASSET.PopFolder();
	ASSET.PopFolder();

#if RMPTFX_EDITOR
	// check for failure
	if (res==false)
	{
		char path[512];
		formatf(path, sizeof(path), "%s\\%s", RMPTFXMGR.GetAssetPath(), "ptxrules");

		char msg[512];
		formatf(msg, sizeof(msg), "unable to save %s\\%s.ptxrule - check permissions", path, GetName());
		
		bkMessageBox("RMPTFX", msg, bkMsgOk, bkMsgInfo);
	}
#endif
}
#endif // RMPTFX_XML_LOADING

void ptxParticleRule::ReplaceModel(const char* pName, rmcDrawable* pDrawable)
{
	// check that the model exists
	for (int i=0; i<m_drawables.GetCount(); i++)
	{
		if (strcmp(m_drawables[i].GetName(), pName)==0)
		{
			m_drawables[i].SetDrawable(pDrawable);
			m_drawables[i].SetInfo();
			return;
		}
	}
}

void ptxParticleRule::PrepareEvoData(ptxEvolutionList* pEvoList, bool isUpdateThread)
{
	//Keeping this commented to disable the evo list caching. Reason is we call PrepareEvoData on each thread separately.
	//And a single caching pointer is used for all the threads. It won't be worth it to actually expand this into a per thread
	//storage because we will hardly satisfy the condition (because the same rule can be updated by the same instance on any thread)
	/*
	if (m_pLastEvoList == pEvoList
#if RMPTFX_EDITOR
		&& !RMPTFXMGR.GetInterface().IsConnected()
#endif // RMPTFX_EDITOR
		)
	{
		// we've already prepped with this evo list, just return
		return;
	}

	m_pLastEvoList = pEvoList;
	*/

	if (isUpdateThread)
	{
		// InsertPotentialUpdateBehaviours(pEvoList);

		for (int i=0; i<m_initBehaviours.GetCount(); i++)
		{
			m_initBehaviours[i]->PrepareEvoData(pEvoList);
		}

		for (int i=0; i<m_updateBehaviours.GetCount() ;i++)
		{
			m_updateBehaviours[i]->PrepareEvoData(pEvoList);
		}
	}
	else
	{
		for (int i=0; i<m_drawBehaviours.GetCount(); i++)
		{
			m_drawBehaviours[i]->PrepareEvoData(pEvoList);
		}
	}
}

void ptxParticleRule::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		m_allBehaviours[i]->SetEvolvedKeyframeDefns(pEvolutionList);
	}
}

bool ptxParticleRule::AddBehaviour(const char* pName)
{
	// check if the behaviour is already added
	if (GetBehaviour(pName))
	{
		return false;
	}

	// create a new behaviour
	ptxBehaviour* pBehaviour = CreateBehaviour(pName);
	if (pBehaviour==NULL)
	{
		return false;
	}

	// initialise the new behaviour and add it the the list
	pBehaviour->CalcHashName();
	pBehaviour->SetDefaultData();
	m_allBehaviours.PushAndGrow(pBehaviour);

	// build the behaviour lists
	BuildBehaviourLists();

#if RMPTFX_EDITOR
	// update the editor keyframe definitions
	pBehaviour->UpdateKeyframeDefns((ptxDrawType)m_drawType, this);
#endif 

	return true;
}

ptxBehaviour* ptxParticleRule::GetBehaviour(const char* pName)
{
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		if (stricmp(m_allBehaviours[i]->GetName(), pName)==0)
		{
			return m_allBehaviours[i];
		}
	}

	return NULL;
}

int ptxParticleRule::GetBehaviourIndex(const char* pName)
{
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		if (stricmp(m_allBehaviours[i]->GetName(), pName)==0)
		{
			return i;
		}
	}

	return -1;
}

void ptxParticleRule::SetName(const char* pName) 
{
	if (m_name==NULL || ((strcasecmp(m_name.c_str(), pName)!=0))) 
	{
		m_name = pName;
	}
}

// editor code
#if RMPTFX_EDITOR
void ptxParticleRule::AddModel(const char* pName, bool reload)
{
	// check if the model already exists
	bool found = false;
	for (int i=0; i<m_drawables.GetCount(); i++) 
	{
		if (strcmp(m_drawables[i].GetName(), pName)==0) 
		{
			found = true;
			break;
		}
	}

	// load the model if it doesn't exist
	if (!found) 
	{
		rmcDrawable* pDrawable = RMPTFXMGR.LoadModel(pName, reload);
		if (pDrawable)
		{
			ptxDrawable& drawable = m_drawables.Grow(1);
			drawable.SetName(pName);
			drawable.SetDrawable(pDrawable);
			drawable.SetInfo();
		}
	}
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
void ptxParticleRule::RemoveModel(const char* pName)
{
	// check if the model exists
	bool found = false;
	int index = -1;
	for (int i=0; i<m_drawables.GetCount(); i++) 
	{
		if (strcmp(m_drawables[i].GetName(), pName)==0) 
		{
			found = true;
			index = i;
			break;
		}
	}

	// remove if found
	if (found)
	{
		m_drawables.Delete(index);
	}
}
#endif //RMPTFX_EDITOR

#if RMPTFX_XML_LOADING
void ptxParticleRule::RemoveAllModels()
{
	m_drawables.Reset();
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
void ptxParticleRule::SendToEditor(ptxByteBuff& buff)
{
	buff.Write_const_char("ptxparticlerule");
	buff.Write_float(sm_fileVersion);   

	m_renderState.SendToEditor(buff);

	m_effectSpawnerAtRatio.SendToEditor(buff);
	m_effectSpawnerOnColn.SendToEditor(buff);

	buff.Write_s32(m_texFrameIdMin);
	buff.Write_s32(m_texFrameIdMax);

	if (m_drawType==PTXPARTICLERULE_DRAWTYPE_SPRITE)
	{
		buff.Write_const_char("sprite");
	}
	else if (m_drawType==PTXPARTICLERULE_DRAWTYPE_MODEL)
	{
		buff.Write_const_char("mesh");
	}
	else if (m_drawType==PTXPARTICLERULE_DRAWTYPE_TRAIL)
	{
		buff.Write_const_char("trail");
	}
	else
	{
		ptxAssertf(0, "invalid draw type (%d)", m_drawType);
		buff.Write_const_char("invalid");
	}

	buff.Write_u8(m_sortType);

	m_shaderInst.SendToEditor(buff, this);

	// send drawable data
	if (m_drawType==PTXPARTICLERULE_DRAWTYPE_MODEL)
	{
		buff.Write_u32(m_drawables.GetCount());
		for (s32 i=0; i<m_drawables.GetCount(); i++)
		{
			buff.Write_const_char(m_drawables[i].GetName());
		}
	}

	// send behaviour data
	buff.Write_s32(m_allBehaviours.size());
	for (u32 i=0; i<(u32)m_allBehaviours.size(); i++)
	{
		m_allBehaviours[i]->SendTuneDataToEditor(buff, this);
	}

	// send bias lin kdata
	buff.Write_s32(m_biasLinks.GetCount());
	for (int i=0; i<m_biasLinks.GetCount(); i++)
	{
		m_biasLinks[i].SendToEditor(buff);
	}
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
void ptxParticleRule::ReceiveFromEditor(const ptxByteBuff& buff)
{
	// skip the id and version number
	buff.Read_const_char();
	buff.Read_float();

	m_renderState.ReceiveFromEditor(buff);

	m_effectSpawnerAtRatio.ReceiveFromEditor(buff);
	m_effectSpawnerOnColn.ReceiveFromEditor(buff);

	m_texFrameIdMin = buff.Read_s32();
	m_texFrameIdMax = buff.Read_s32();

	// read the draw type and check if it has changed
	bool hasChangedDrawType = false;

	const char* newDrawTypeName = buff.Read_const_char();
	ptxDrawType newDrawType = ptxBehaviour::GetDrawType(newDrawTypeName);
	
	if (m_drawType!=newDrawType)
	{
		hasChangedDrawType = true;
		m_drawType = (u8)newDrawType;
	}

	m_sortType = buff.Read_u8();

	m_shaderInst.ReceiveFromEditor(buff);

	// read behaviour data
	s32 numBehaviours = buff.Read_s32();
	for (s32 i=0; i<numBehaviours; i++)
	{
		// read the name
		const char* behaviourName = buff.Read_const_char();
		if (behaviourName)
		{
			// read behaviour properties
			for (s32 j=0; j<m_allBehaviours.size(); j++)
			{
				if (stricmp(m_allBehaviours[j]->GetName(), behaviourName)==0)
				{
					m_allBehaviours[j]->ReceiveTuneDataFromEditor(buff);
					break;
				}
			}
		}
	}

	if(RMPTFXMGR.GetParticleRuleInitFunctor())
	{
		RMPTFXMGR.GetParticleRuleInitFunctor()(this);
	}

	// update the editor keyframe definitions if the draw type has changed
	if (hasChangedDrawType)
	{
		UpdateKeyframeDefns();
	}

	// read bias link data
	m_biasLinks.Reset();
	int numBiasLinks = buff.Read_s32();
	for (int i=0; i<numBiasLinks; i++)
	{
		ptxBiasLink newBiasLink;
		newBiasLink.ReceiveFromEditor(buff);
		m_biasLinks.PushAndGrow(newBiasLink);
	}

	// set the bias links
	if (m_biasLinks.GetCount())
	{
		SetBiasLinks();
	}
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
void ptxParticleRule::UpdateKeyframeDefns()
{
	// go through the behaviour
	for (int i=0; i<m_allBehaviours.size(); i++)
	{
		// do a swap if needed to make sure the behaviour is compatible with the draw type
		const char* behaviourName = RMPTFXMGR.FindAnalogousBehaviour(m_allBehaviours[i]->GetName(), (ptxDrawType)m_drawType);

		if (behaviourName == NULL)
		{
			// delete any behaviours that are no longer valid
			delete m_allBehaviours[i];
			m_allBehaviours.Delete(i);
			i--;
		}
		else if (stricmp(behaviourName, m_allBehaviours[i]->GetName())==0)
		{
			// we have the same behaviour back - do nothing
		}
		else
		{
			// we have a new behaviour - delete the original and create the new one
			delete m_allBehaviours[i];
			m_allBehaviours[i] = ptxParticleRule::CreateBehaviour(behaviourName);
			m_allBehaviours[i]->CalcHashName();
			m_allBehaviours[i]->SetDefaultData();
		}
	}

	// rebuild the behaviour lists
	BuildBehaviourLists();

	// update the editor keyframe definitions
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		m_allBehaviours[i]->UpdateKeyframeDefns((ptxDrawType)m_drawType, this);
	}

}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
void ptxParticleRule::StartProfileUpdate()
{
	// called once per frame for active effects

	// start the profile on each behaviour
	for (s32 i=0; i<m_updateBehaviours.size(); i++)
	{
		m_updateBehaviours[i]->StartProfileUpdate();
	}
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
void ptxParticleRule::StartProfileDraw()
{
	// called once per frame for active effects

	// reset the profile data

	// start the profile on each behaviour
	for (s32 i=0; i<m_drawBehaviours.size(); i++)
	{
		m_drawBehaviours[i]->StartProfileUpdate();
	}
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
bool ptxParticleRule::RemoveBehaviour(const char* pName)
{
	int behaviourIdx = GetBehaviourIndex(pName);
	if (behaviourIdx>=0)
	{
		// don't remove key behaviours, just reset them
		if (stricmp(pName, STRINGIFY(ptxd_Model))==0	||
			stricmp(pName, STRINGIFY(ptxd_Sprite))==0	||
			stricmp(pName, STRINGIFY(ptxd_Trail))==0	||
			stricmp(pName, STRINGIFY(ptxu_Size))==0		||
			stricmp(pName, STRINGIFY(ptxu_Colour))==0	||
			stricmp(pName, STRINGIFY(ptxu_Age))==0		||
			stricmp(pName, STRINGIFY(ptxu_Velocity))==0)
		{
			m_allBehaviours[behaviourIdx]->SetDefaultData();
		}
		else
		{
			delete m_allBehaviours[behaviourIdx];
			m_allBehaviours.Delete(behaviourIdx);
			BuildBehaviourLists();
		}

		// re-add any behaviour with an active evolution
		RMPTFXMGR.AddBehavioursWithActiveEvos();

		return true;
	}

	return false;
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
void ptxParticleRule::ValidateBehaviour(const char* pName)
{
	int behaviourIdx = GetBehaviourIndex(pName);
	if (behaviourIdx>=0)
	{
		if (m_allBehaviours[behaviourIdx]->IsActive()==false)
		{
			RemoveBehaviour(pName);
		}
	}
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
void ptxParticleRule::ValidateAllBehaviours()
{
	// do some non-behaviour clamping that is useful to do here (to do with texture frames)
	int numTexFrames = 0;

	if (GetDrawType()==PTXPARTICLERULE_DRAWTYPE_SPRITE || GetDrawType()==PTXPARTICLERULE_DRAWTYPE_TRAIL)
	{
		u32 textureHashName = GetTextureHashNameForClipRegions();

		const Vec4V* pvClipRegionData = NULL;
		int numTexColumns;
		int numTexRows;
		ASSERT_ONLY(bool regionOk = )ptxClipRegions::GetData(textureHashName, pvClipRegionData, numTexColumns, numTexRows);
		ptxAssertf(regionOk == true,"Couldn't find clip region for texture %s in rule %s",GetTextureNameForClipRegions(),GetName());
		numTexFrames = numTexColumns*numTexRows;

		ptxAssertf(m_texFrameIdMin<numTexFrames, "%s - min texture frame id (%d) to the max number of frames (%d)", GetName(), m_texFrameIdMin, numTexFrames);
		if (m_texFrameIdMin>=numTexFrames)
		{
			m_texFrameIdMin = numTexFrames-1;
		}

		ptxAssertf(m_texFrameIdMax<numTexFrames, "%s - max texture frame id (%d) to the max number of frames (%d)", GetName(), m_texFrameIdMax, numTexFrames);
		if (m_texFrameIdMax>=numTexFrames)
		{
			m_texFrameIdMax = numTexFrames-1;
		}
	}

	// remove any non-active behaviours
	for (int i=m_allBehaviours.GetCount()-1; i>=0; i--)
	{
		if (m_allBehaviours[i]->IsActive()==false)
		{
			RemoveBehaviour(m_allBehaviours[i]->GetName());
		}
	}

	// check over specific behaviours
	ptxu_AnimateTexture* pAnimateTexture = static_cast<ptxu_AnimateTexture*>(GetBehaviour("ptxu_AnimateTexture"));
	if (pAnimateTexture)
	{
		// make sure the last frame id is in range
		if (pAnimateTexture->m_doFrameBlending)
		{		
			ptxAssertf(pAnimateTexture->m_lastFrameId<numTexFrames, "%s - clamping the animate texture behaviour last frame id (%d) to the last blended frame (%d)", GetName(), pAnimateTexture->m_lastFrameId, numTexFrames-1);
			if (pAnimateTexture->m_lastFrameId>=numTexFrames)
			{
				pAnimateTexture->m_lastFrameId = numTexFrames-1;
			}
		}
		else
		{
			// but allow it to go 1 over (numTexFrames instead of numTexFrames-1) so that animation works ok when frame blending is disabled
			ptxAssertf(pAnimateTexture->m_lastFrameId<=numTexFrames, "%s - clamping the animate texture behaviour last frame id (%d) to the last non-blended frame (%d)", GetName(), pAnimateTexture->m_lastFrameId, numTexFrames);
			if (pAnimateTexture->m_lastFrameId>numTexFrames)
			{
				pAnimateTexture->m_lastFrameId = numTexFrames;
			}
		}
	}

// 	ptxu_Colour* pColour = static_cast<ptxu_Colour*>(GetBehaviour("ptxu_Colour"));
// 	if (pColour)
// 	{
// 		ptxKeyframe& keyframe = pColour->m_emissiveIntensityKFP.GetKeyframe();
// 		for (int i=0; i<keyframe.GetNumEntries(); i++)
// 		{
// 			float time = keyframe.GetTime(i);
// 			Vec4V vValue = keyframe.GetValue(i);
// 
// 			if (vValue.GetXf()>16.0f)
// 			{
// 				vValue.SetXf(16.0f);
// 			}
// 
// 			if (vValue.GetYf()>16.0f)
// 			{
// 				vValue.SetYf(16.0f);
// 			}
// 
// 			keyframe.SetEntry(i, time, vValue);
// 		}
// 	}
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
ptxKeyframeProp* ptxParticleRule::GetKeyframeProp(u32 keyframePropId)
{
	ptxKeyframeProp* pKeyframeProp = NULL;
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		pKeyframeProp = m_allBehaviours[i]->GetKeyframePropListRef().GetKeyframePropByPropId(keyframePropId);
		if (pKeyframeProp)
		{
			return pKeyframeProp;
		}
	}

	return NULL;
}
#endif //RMPTFX_EDITOR

#if RMPTFX_EDITOR
void ptxParticleRule::UpdateUIData(ptxEvolutionList* pEvoList, const ptxEffectRule* pEffectRule, s32 eventIdx)
{
	for (int i=0; i<m_allBehaviours.GetCount(); i++)
	{
		pEvoList->UpdateUIData(&m_allBehaviours[i]->GetKeyframePropListRef(), pEffectRule, eventIdx);
	}
}
#endif //RMPTFX_EDITOR




void ptxParticleRule::InitDefaultBehaviours()
{
	ptxAssertf(m_allBehaviours.empty(), "behaviour list isn't empty");

	m_allBehaviours.PushAndGrow(RMPTFX_CREATE_BEHAVIOUR_FROM_ID("ptxu_Age"));
	m_allBehaviours.PushAndGrow(RMPTFX_CREATE_BEHAVIOUR_FROM_ID("ptxu_Velocity"));
	m_allBehaviours.PushAndGrow(RMPTFX_CREATE_BEHAVIOUR_FROM_ID("ptxu_Colour"));
	m_allBehaviours.PushAndGrow(RMPTFX_CREATE_BEHAVIOUR_FROM_ID("ptxu_Size"));
	m_allBehaviours.PushAndGrow(RMPTFX_CREATE_BEHAVIOUR_FROM_ID("ptxd_Sprite"));

	for (int i=0; i<m_allBehaviours.size(); i++)
	{
		m_allBehaviours[i]->CalcHashName();
		m_allBehaviours[i]->SetDefaultData();
	}
}

ptxBehaviour* ptxParticleRule::CreateBehaviour(const char* pName)
{
	ptxAssertf(pName, "invalid behaviour name");
	return RMPTFX_CREATE_BEHAVIOUR_FROM_ID(pName);
}

void ptxParticleRule::ShutdownBehaviours()
{
	for (int i=0; i<m_allBehaviours.size(); i++)
	{
		delete m_allBehaviours[i];
	}

	m_allBehaviours.clear();
	m_initBehaviours.clear();
	m_updateBehaviours.clear();
	m_updateFinalizeBehaviours.clear();
	m_drawBehaviours.clear();
	m_releaseBehaviours.clear();
}

bool BehaviourSorter(const datRef<ptxBehaviour>& pBehaviourA, const datRef<ptxBehaviour>& pBehaviourB)
{
	return pBehaviourA->GetOrder() < pBehaviourB->GetOrder();
}

void ptxParticleRule::BuildBehaviourLists()
{
	// sort the behaviours
	std::sort(m_allBehaviours.begin(), m_allBehaviours.end(), BehaviourSorter);

	// create the behaviour sub lists
	int numInitBehaviours = 0;
	int numUpdateBehaviours = 0;
	int numUpdateFinalizeBehaviours = 0;
	int numDrawBehaviours = 0;
	int numReleaseBehaviours = 0;

	for (s32 i=0; i<m_allBehaviours.size(); i++)
	{
		if (m_allBehaviours[i]->NeedsToInit())
		{
			numInitBehaviours++;
		}

		if (m_allBehaviours[i]->NeedsToUpdate())
		{
			numUpdateBehaviours++;
		}

		if (m_allBehaviours[i]->NeedsToUpdateFinalize())
		{
			numUpdateFinalizeBehaviours++;
		}

		if (m_allBehaviours[i]->NeedsToDraw())
		{
			numDrawBehaviours++;
		}

		if (m_allBehaviours[i]->NeedsToRelease())
		{
			numReleaseBehaviours++;
		}
	}

	m_initBehaviours.Reset();
	m_initBehaviours.Reserve(numInitBehaviours);

	m_updateBehaviours.Reset();
	m_updateBehaviours.Reserve(numUpdateBehaviours);

	m_updateFinalizeBehaviours.Reset();
	m_updateFinalizeBehaviours.Reserve(numUpdateFinalizeBehaviours);

	m_drawBehaviours.Reset();
	m_drawBehaviours.Reserve(numDrawBehaviours);

	m_releaseBehaviours.Reset();
	m_releaseBehaviours.Reserve(numReleaseBehaviours);

	for (s32 i=0; i<m_allBehaviours.size(); i++)
	{
		if (m_allBehaviours[i]->NeedsToInit())
		{
			m_initBehaviours.Push(m_allBehaviours[i]);
		}

		if (m_allBehaviours[i]->NeedsToUpdate())
		{
			m_updateBehaviours.Push(m_allBehaviours[i]);
		}

		if (m_allBehaviours[i]->NeedsToUpdateFinalize())
		{
			m_updateFinalizeBehaviours.Push(m_allBehaviours[i]);
		}

		if (m_allBehaviours[i]->NeedsToDraw())
		{
			m_drawBehaviours.Push(m_allBehaviours[i]);
		}

		if (m_allBehaviours[i]->NeedsToRelease())
		{
			m_releaseBehaviours.Push(m_allBehaviours[i]);
		}
	}

	// if we're building a resource do this in the resource c'tor
	if (!sysMemAllocator::GetCurrent().IsBuildingResource())
	{
		SetupRuntimeFlags();
	}

	m_pWindBehaviour = static_cast<ptxu_Wind*>(GetBehaviour("ptxu_Wind"));

}

void ptxParticleRule::SetBiasLinks()
{
	// initialise the bias links with a random table entry
	for (int i=0; i<m_biasLinks.GetCount(); i++)
	{
		m_biasLinks[i].GenerateRandom();
	}

	for (int i=0; i<m_allBehaviours.size(); i++)
	{
		m_allBehaviours[i]->SetBiasLinks(m_biasLinks);
	}
}

void ptxParticleRule::SetParams(ptxBehaviourParams& params, ptxEffectInst* pEffectInst, ptxEmitterInst* pEmitterInst, ptxEvolutionList* pEvolutionList, ptxPointItem* pPointItem, ScalarV_In vDeltaTime, ScalarV_In vFrameRatio, int updateBufferIdx)
{
	// set up the misc data
	float effectSpawnerTriggerRatio = -1.0f;
	if (m_effectSpawnerAtRatio.GetEffectRule())
	{
		effectSpawnerTriggerRatio = m_effectSpawnerAtRatio.GetTriggerRatio();
	}

	// set up the params
	memset(&params, 0, sizeof(ptxBehaviourParams));
	params.pEffectInst = pEffectInst;
	params.pEmitterInst = pEmitterInst;
	params.pEvolutionList = pEvolutionList;
	params.pPointItem = pPointItem;
	params.vDeltaTime = vDeltaTime * vFrameRatio;
	params.vPtxFrameRatio = vFrameRatio;
	params.effectSpawnerTriggerRatio = effectSpawnerTriggerRatio;
	params.texFrameIdMin = m_texFrameIdMin;
	params.texFrameIdMax = m_texFrameIdMax;

	params.distSqrToCamera = pEffectInst->GetDataDBMT(updateBufferIdx)->GetDistSqrToCamera();

	// wind related
	params.vEffectWindVelocity.ZeroComponents();
	if (m_pWindBehaviour)
	{
		m_pWindBehaviour->UpdateEmitterInst(params);
	}
}

void ptxParticleRule::SetParamsPointItem(ptxBehaviourParams& params, ptxPointItem* pPointItem)
{
	params.pPointItem = pPointItem;

	// get the model dimensions
	params.vModelDimensions = Vec4V(V_ZERO);
	if (m_drawType==PTXPARTICLERULE_DRAWTYPE_MODEL && GetNumDrawables()>0)
	{
		params.vModelDimensions = GetDrawable(pPointItem->GetDataDB()->GetInitTexFrameId()).GetBoundInfo();
	}
}

#if RMPTFX_EDITOR
void ptxParticleRule::CreateUIData()
{
	m_pUIData = rage_new ptxParticleRuleUIData();
}
#endif


} // namespace rage
