// 
// rmptfx/ptxlist.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXLIST_H
#define RMPTFX_PTXLIST_H


// includes (us)
#include "rmptfx/ptxchannel.h"
#include "rmptfx/ptxconfig.h"

// include (rage)
#include "atl/singleton.h"
#include "system/criticalsection.h"
#include "system/new.h"


// namespaces
namespace rage 
{

// defines 
#define PTX_NUM_BUFFERS				(2)
#define PTX_NUM_RETURN_BUFFERS		(PTX_NUM_BUFFERS+1)


// classes
// ptxThreadController
class ptxThreadController
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxThreadController()
		{
			m_updateBufferId = 0;
			m_drawBufferId = 0;
			m_isMultithreaded = true;
			m_drawBufferId = PTX_NUM_BUFFERS-1;
		}

		void SwapBuffers()
		{
			m_updateBufferId = m_updateBufferId+1>=PTX_NUM_BUFFERS ? 0 : m_updateBufferId+1;
			m_drawBufferId = (m_drawBufferId-1)<0 ? PTX_NUM_BUFFERS-1 : m_drawBufferId-1;
		}

		bool GetIsMultithreaded() const {return m_isMultithreaded;}
		u8 GetUpdateBufferId() const {return m_updateBufferId;}
		u8 GetDrawBufferId() const {return m_drawBufferId;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		bool m_isMultithreaded;
		u8 m_updateBufferId;
		u8 m_drawBufferId;
};

typedef atSingleton<ptxThreadController> g_ptxThreadController;

#define RMPTFXTHREADER g_ptxThreadController::InstanceRef()





// ptxListItemBase
class ptxListItemBase 
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxListItemBase()
		{
			m_pPrev = NULL;
			m_pNext = NULL;

			for (int i=0; i<PTX_NUM_BUFFERS; i++)
			{
				m_pNextMT[i] = NULL;
			}
		}

		ptxListItemBase* GetNext() {return m_pNext;}
		const ptxListItemBase* GetNext() const {return m_pNext;}
		void SetNext(ptxListItemBase* pItem) {m_pNext=pItem;}

		ptxListItemBase* GetPrev() {return m_pPrev;}
		const ptxListItemBase* GetPrev() const {return m_pPrev;}
		void SetPrev(ptxListItemBase* pItem) {m_pPrev=pItem;}

		ptxListItemBase* GetNextMT(int bufferId) {return m_pNextMT[bufferId];}
		const ptxListItemBase* GetNextMT(int bufferId) const {return m_pNextMT[bufferId];}
		void SetNextMT(int bufferId, ptxListItemBase* pItem) {m_pNextMT[bufferId] = pItem;}

		/**
		 * This is a relativly safe run-time option to calculate the offset to the next MT pointer so it can be DMAed without the rest of the class.
		 */
		uptr GetOffsetNextMTPtr(int bufferId) const { return reinterpret_cast<uptr>(&m_pNextMT[bufferId]) - reinterpret_cast<uptr>(this); }

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ptxListItemBase* m_pNext;
		ptxListItemBase* m_pPrev;
		ptxListItemBase* m_pNextMT[PTX_NUM_BUFFERS];

} ;





// ptxList
class ptxList 
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxList()
		{
			for (int i=0; i<PTX_NUM_BUFFERS; i++)
			{
				m_pHeadMT[i] = NULL;
				m_pTailMT[i] = NULL;
				m_numItemsMT[i] = 0;
			} 

			m_pHead = NULL;
			m_numItems = 0;
		}

		ptxListItemBase* GetHead() {return m_pHead;}
		const ptxListItemBase* GetHead() const {return m_pHead;}
		void SetHead(ptxListItemBase* pItem) {m_pHead=pItem;}

		int GetNumItems() const {return m_numItems;}
		void SetNumItems(int numItems) {m_numItems=numItems;}
		void IncNumItems(int numItems) {m_numItems+=numItems;}

		ptxListItemBase* GetHeadMT(int bufferId) {return m_pHeadMT[bufferId];}
		const ptxListItemBase* GetHeadMT(int bufferId) const {return m_pHeadMT[bufferId];}
		void SetHeadMT(int bufferId, ptxListItemBase* pItem) {m_pHeadMT[bufferId] = pItem;}

		int GetNumItemsMT(int bufferId) const {return m_numItemsMT[bufferId];}
		void SetNumItemsMT(int bufferId, int numItems) {m_numItemsMT[bufferId]=numItems;}
		void IncNumItemsMT(int bufferId, int numItems) {m_numItemsMT[bufferId]+=numItems;}

		void ClearBuffers()
		{
			for (int i=0; i<PTX_NUM_BUFFERS; i++)
			{
				m_pHeadMT[i] = NULL;
				m_pTailMT[i] = NULL;
				m_numItemsMT[i] = 0;
			}
		}

		void BeginBuffering(int bufferId)
		{
			m_pHeadMT[bufferId] = NULL;
			m_pTailMT[bufferId] = NULL;
			m_numItemsMT[bufferId] = 0;
		}

		void BufferData(int bufferId, ptxListItemBase* pItem, bool bufferToHead=false)
		{
			if (bufferToHead)
			{
				if (m_pTailMT[bufferId]==NULL)
				{	
					m_pTailMT[bufferId] = pItem;	
				}

				pItem->SetNextMT(bufferId, m_pHeadMT[bufferId]);
				m_pHeadMT[bufferId] = pItem;
			}
			else
			{
				if (m_pHeadMT[bufferId]==NULL)
				{	
					m_pHeadMT[bufferId] = pItem;	
				}

				if (m_pTailMT[bufferId])
				{
					m_pTailMT[bufferId]->SetNextMT(bufferId, pItem);
				}

				pItem->SetNextMT(bufferId, NULL);
				m_pTailMT[bufferId] = pItem;
			}

			m_numItemsMT[bufferId]++;
		}

		bool IsInList(ptxListItemBase* pItem)
		{
			ptxListItemBase* pCurrItem = m_pHead;
			while (pCurrItem)
			{
				if (pCurrItem==pItem)
				{
					return true;
				}

				pCurrItem = pCurrItem->GetNext();
			}

			return false;
		}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ptxListItemBase* m_pHead;										// pointer to the head item
		ptxListItemBase* m_pHeadMT[PTX_NUM_BUFFERS];
		ptxListItemBase* m_pTailMT[PTX_NUM_BUFFERS];
		int m_numItemsMT[PTX_NUM_BUFFERS];

		int	m_numItems;
}

;



// ptxPool
template<class _SB, class _DB> 
class ptxPool 
{
	///////////////////////////////////
	// CLASSES 
	///////////////////////////////////

	public: ///////////////////////////

		// ptxListItem
		class ptxListItem : public ptxListItemBase 
		{
			///////////////////////////////////
			// FUNCTIONS 
			///////////////////////////////////

			public: ///////////////////////////

				// access to the single buffered data
				_SB* GetDataSB() {return &m_dataSB;}
				const _SB* GetDataSB()const {return &m_dataSB;}
				
				// access to the current double buffered data
				_DB* GetDataDB() {return &m_dataDB;}
				const _DB* GetDataDB() const {return &m_dataDB;}

				// access to the multi-threaded double buffered data
				_DB* GetDataDBMT(int bufferId) {return &m_dataDBMT[bufferId];}
				const _DB* GetDataDBMT(int bufferId) const {return &m_dataDBMT[bufferId];}

				void CopyToUpdateBuffer()
				{
					_DB* RESTRICT pDBDst = GetDataDBMT(RMPTFXTHREADER.GetUpdateBufferId());
					_DB* RESTRICT pDBSrc = GetDataDB();

					*pDBDst = *pDBSrc;
				}


			///////////////////////////////////
			// VARIABLES 
			///////////////////////////////////	

			public: ///////////////////////////

				_SB	m_dataSB; 											// the data that is only ever single buffered (MN - this needs to be public for the OffsetOf function in ptxupdatetask to work)


			private: //////////////////////////

				_DB	m_dataDB; 											// the double buffered data that can be accessed from the main thread and the particle update thread
				_DB	m_dataDBMT[PTX_NUM_BUFFERS];						// the double buffered data that can be accessed from the particle update task and render thread


		} ;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		ptxPool()
		{
			m_numItems = 0;
			m_pItems = NULL;

			m_numActiveItems = 0;

			m_updateBufferId = 0;
			m_drawBufferId = 0;

			m_drawBufferId = PTX_NUM_BUFFERS-1;
			for (int i=0; i<PTX_NUM_RETURN_BUFFERS; i++)
			{
				m_pReturnedTail[i] = NULL;
			}
		}

		~ptxPool() 
		{
			delete[] m_pItems;
		}

		void Create(int numItems)
		{
			FastAssert(numItems);
			ptxAssertf(m_pItems==NULL && CheckNumActive()==0, "can't create pool if it's not empty to start with");

			if (m_pItems)
			{
				delete m_pItems;
			}

			m_pItems = rage_new ptxListItem[numItems];  
			m_numItems = numItems;

			InitItemList();
		}

		void Reset(ptxList* pList=NULL)
		{
			SYS_CS_SYNC(m_poolCS);
			if (pList)
			{
				// clear active ones
				ptxListItem* pCurrItem = (ptxListItem*)pList->GetHead();
				while (pCurrItem)
				{
					ptxListItem* pNextItem = (ptxListItem*)pCurrItem->GetNext();

					AddItem(&m_freeList, pCurrItem);
					m_numActiveItems--;

					pCurrItem = pNextItem;
				}

				pList->SetHead(NULL);
				pList->SetNumItems(0);

				pList->SetNumItemsMT(m_updateBufferId, 0);
				pList->SetHeadMT(m_updateBufferId, NULL);

				return;
			}

			// no list specified so reset the whole thing
			InitItemList();
		}	

		ptxListItem* GetNewItem(ptxList* pListToAddTo)
		{
			SYS_CS_SYNC(m_poolCS);
			if (m_freeList.GetHead()==NULL) 
			{
#if RMPTFX_BANK
				if (m_canShowWarningThisFrame)
				{
					// ptxDebugf1("Ran out of points in point list %p - Capacity is %d", this, m_numItems);
					m_canShowWarningThisFrame = false;
				}
#endif
				// no items left
				return NULL;
			}

			// get a new item from the free list
			ptxListItem* pNewItem = (ptxListItem*)m_freeList.GetHead();
			m_freeList.SetHead(pNewItem->GetNext());
			m_freeList.IncNumItems(-1);

			m_numActiveItems++;

			// initialise the new item
			pNewItem->SetPrev(NULL);
			pNewItem->SetNext(NULL);
			for (int i=0; i<PTX_NUM_BUFFERS; i++)
			{
				pNewItem->SetNextMT(i, NULL);
			}

			// insert into active list
			InsertItem(pListToAddTo, pNewItem);

			return pNewItem;
		}

		ptxListItem* GetNewItems(ptxList* pListToAddTo, int &numItems)
		{
			SYS_CS_SYNC(m_poolCS);
			ptxAssertf(numItems>0, "Function was called with 0 numItems");
			numItems = Min(numItems, GetNumItemsFree());
			if (m_freeList.GetHead()==NULL) 
			{
#if RMPTFX_BANK
				if (m_canShowWarningThisFrame)
				{
					// ptxDebugf1("Ran out of points in point list %p - Capacity is %d", this, m_numItems);
					m_canShowWarningThisFrame = false;
				}
#endif
				// no items left
				return NULL;
			}

			ptxListItem* pPrevNewItem = NULL;
			ptxListItem* pFirstNewItem = (ptxListItem*)m_freeList.GetHead();
			for(int i=0; i<numItems; i++)
			{
				// get a new item from the free list
				ptxListItem* pNewItem = (ptxListItem*)m_freeList.GetHead();
				m_freeList.SetHead(pNewItem->GetNext());
				// initialise the new item
				pNewItem->SetNext(pPrevNewItem);
				pNewItem->SetPrev(NULL);
				if(Likely(pPrevNewItem))
				{
					pPrevNewItem->SetPrev(pNewItem);
				}

				for (int i=0; i<PTX_NUM_BUFFERS; i++)
				{
					pNewItem->SetNextMT(i, NULL);
				}
				pPrevNewItem = pNewItem;
			}

			ptxListItem* pLastNewItem = pPrevNewItem;

			m_freeList.IncNumItems(-numItems);
			m_numActiveItems+=numItems;

			// insert into active list
			InsertItems(pListToAddTo, pFirstNewItem, pLastNewItem, numItems);

			//return the first item because we need to start from there
			return pFirstNewItem;
		}

		void ReturnItem(ptxList* pListToRemoveFrom, ptxListItem* pItemToRemove)
		{
			SYS_CS_SYNC(m_poolCS);
			if (pListToRemoveFrom) 
			{
				RemoveItem(pListToRemoveFrom, pItemToRemove);
			}

			// always return items to the first buffer
			// they will progress through the buffers until they are in the last buffer when they will get returned to the free list
			// this should allow enough time for any draw thread to draw them before they get freed
			if (m_returnedList[0].GetHead() == NULL) 
			{
				m_pReturnedTail[0] = pItemToRemove;
			}
			AddItem(&m_returnedList[0], pItemToRemove);
		}

		void EndFrame(void(*onFreeCB)(ptxListItemBase*))
		{	   
			SYS_CS_SYNC(m_poolCS);
#if RMPTFX_BANK
			// reset the warning flag
			m_canShowWarningThisFrame = true;
#endif

			m_updateBufferId = RMPTFXTHREADER.GetUpdateBufferId();
			m_drawBufferId = RMPTFXTHREADER.GetDrawBufferId();

			// move returned items in the last buffer to the free list
			if (m_returnedList[PTX_NUM_RETURN_BUFFERS-1].GetHead())
			{
				if (onFreeCB)
				{
					for (ptxListItemBase* pItem=m_returnedList[PTX_NUM_RETURN_BUFFERS-1].GetHead(); pItem; pItem=pItem->GetNext())
					{
						onFreeCB(pItem);
					}
				}

				m_numActiveItems -= m_returnedList[PTX_NUM_RETURN_BUFFERS-1].GetNumItems();
				m_freeList.IncNumItems(m_returnedList[PTX_NUM_RETURN_BUFFERS-1].GetNumItems());

				m_pReturnedTail[PTX_NUM_RETURN_BUFFERS-1]->SetNext(m_freeList.GetHead());
				m_freeList.SetHead(m_returnedList[PTX_NUM_RETURN_BUFFERS-1].GetHead());

				m_pReturnedTail[PTX_NUM_RETURN_BUFFERS-1] = NULL;
			}

			for (int i=PTX_NUM_RETURN_BUFFERS-1; i>0; i--)
			{
				m_returnedList[i].SetHead(m_returnedList[i-1].GetHead());
				m_returnedList[i].SetNumItems(m_returnedList[i-1].GetNumItems());
				m_pReturnedTail[i] = m_pReturnedTail[i-1];
			}

			// clear out the first reserved list
			m_returnedList[0].SetNumItems(0);
			m_returnedList[0].SetHead(NULL);
			m_pReturnedTail[0] = NULL;
		}

		void Move(ptxListItem* pItem, ptxList* pFromList, ptxList* pToList)
		{
			SYS_CS_SYNC(m_poolCS);
			RemoveItem(pFromList, pItem);
			InsertItem(pToList, pItem);
		}

		ptxListItem* GetItemsPtr()
		{
			return m_pItems;
		}

		int GetNumItems()
		{
			SYS_CS_SYNC(m_poolCS);
			return m_numItems;
		}

		int GetNumItemsFree()
		{
			SYS_CS_SYNC(m_poolCS);
			ptxAssertf(m_numItems-m_numActiveItems==m_freeList.GetNumItems(), "num free items mismatch");
			return m_freeList.GetNumItems();
		}	

		int GetNumItemsReturned(int idx)
		{
			SYS_CS_SYNC(m_poolCS);
			if (ptxVerifyf(idx<PTX_NUM_RETURN_BUFFERS, "returned list index out of range"))
			{
				return m_returnedList[idx].GetNumItems();
			}

			return 0;
		}

		int GetNumItemsActive()
		{
			SYS_CS_SYNC(m_poolCS);
			ptxAssertf(m_numItems-m_freeList.GetNumItems()==m_numActiveItems, "num active items mismatch");
			return m_numActiveItems;
		}

#if RMPTFX_THOROUGH_ERROR_CHECKING
		bool IsInFreeList(ptxListItemBase* pItem)
		{
			ptxListItemBase* pCurrItem = m_freeList.GetHead();
			while (pCurrItem)
			{
				if (pCurrItem==pItem)
				{
					return true;
				}

				pCurrItem = pCurrItem->GetNext();
			}

			return false;
		}
#endif


	private: //////////////////////////

		void InitItemList()
		{
			// add the items to the free list
			m_freeList.SetNumItems(m_numItems);
			m_freeList.SetHead(m_pItems);

			// initialise the items so each has links to the next and previous
			for (int i=0; i<m_numItems-1; i++)
			{
				m_pItems[i].SetNext(&m_pItems[i+1]);
				m_pItems[i+1].SetPrev(&m_pItems[i]);
			}
			m_pItems[m_numItems-1].SetNext(NULL);

			// set the number of active items to zero
			m_numActiveItems = 0;

			// reserved buffers
			for (int i=0; i<PTX_NUM_RETURN_BUFFERS; i++)
			{
				m_returnedList[i].SetHead(NULL);
				m_pReturnedTail[i] = NULL;

				for (int d=0; d<PTX_NUM_BUFFERS; d++)
				{
					m_returnedList[i].SetHeadMT(d, NULL);
				}
			}
		}

		void AddItem(ptxList* pList, ptxListItem* pItem)
		{
			// does not preserve prev pointer
			pItem->SetNext(pList->GetHead());
			pItem->SetPrev(NULL);
			pList->SetHead(pItem);
			pList->IncNumItems(1);
		}

		void AddItems(ptxList* pList, ptxListItem* pFirstItem, ptxListItem* pLastItem, int numItems)
		{
#if __ASSERT //&& 0
			//This part is to just check if the first item and the last item belong to the same list
			ptxListItem* pIterator = pFirstItem;
			int count = 0;
			while(pIterator != NULL)
			{
				pIterator = (ptxListItem*)pIterator->GetPrev();
				count++;
			}
			ptxAssertf(count == numItems, "Number of new items does not match count, count = %d, numItems = %d", count, numItems);
#endif
			// does not preserve prev pointer
			pFirstItem->SetNext(pList->GetHead());
			pLastItem->SetPrev(NULL);
			pList->SetHead(pLastItem);
			pList->IncNumItems(numItems);
		}

		void InsertItem(ptxList* pList, ptxListItem* pItem)
		{
			// preserves prev pointer
			if (pList->GetHead())
			{
				ptxListItemBase* pNextItem = pList->GetHead();
				pNextItem->SetPrev(pItem);
			}
			AddItem(pList, pItem);
		}

		void InsertItems(ptxList* pList, ptxListItem* pFirstItem, ptxListItem* pLastItem, int numItems)
		{
			// preserves prev pointer
			if (pList->GetHead())
			{
				ptxListItemBase* pNextItem = pList->GetHead();
				pNextItem->SetPrev(pFirstItem);
			}
			AddItems(pList, pFirstItem, pLastItem, numItems);
		}

		void RemoveItem(ptxList* pList, ptxListItem* pItem)
		{
			// preserves Prev Pointer
			pList->IncNumItems(-1);
			ptxListItemBase* pNextItem = pItem->GetNext();
			ptxListItemBase* pPrevItem = pItem->GetPrev();
			if (pList->GetHead()==pItem)
			{
				pList->SetHead(pItem->GetNext());
			}
			if (pItem->GetNext())
			{
				pNextItem->SetPrev(pItem->GetPrev());
			}
			if (pItem->GetPrev())
			{
				pPrevItem->SetNext(pItem->GetNext());
			}
			pItem->SetPrev(NULL);
		}

#if __ASSERT
		int CheckNumActive()
		{
			ptxListItemBase* pItemHeadUpdate = m_freeList.GetHead();
			ptxListItemBase* pNextItem = pItemHeadUpdate;

			int cnt = 0;
			while (pNextItem)
			{
				pNextItem = pNextItem->GetNext();
				cnt++;
			}

			int count = m_numItems-cnt;

			if (m_numActiveItems!=count)
			{
				ptxErrorf("CheckNumActive, count is different [%d,%d]", m_numActiveItems, count);
			}

			return count;
		}
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ptxList m_freeList;									// the list of free (inactive) items  
		ptxList m_returnedList[PTX_NUM_RETURN_BUFFERS];		// list of returned items waiting to be added to the free list

		ptxListItem* m_pItems;								// the pool's allocated items
		ptxListItem* m_pReturnedTail[PTX_NUM_RETURN_BUFFERS];	  
	
		int m_numItems;										// the number of items in this pool
		int m_numActiveItems;								// the number of items that are active

		sysCriticalSectionToken m_poolCS;					// making operations thread safe

		u8 m_drawBufferId;									
		u8 m_updateBufferId;
		
#if RMPTFX_BANK
		bool m_canShowWarningThisFrame;
#endif
}
;


} //namespace rage

#endif //RMPTFX_PTXLIST_H

