<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxd_Model" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<float name="m_cameraShrink"/>
  <float name="m_shadowCastIntensity"/>  
  <bool name="m_disableDraw"/>
</structdef>

</ParserSchema>