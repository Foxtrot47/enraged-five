// 
// rmptfx/ptxeffectrule.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 


// includes (us)
#include "rmptfx/ptxeffectrule.h"
#include "rmptfx/ptxeffectrule_parser.h"

// includes (rmptfx)
#include "rmptfx/ptxbytebuff.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxemitterrule.h"
#include "rmptfx/ptxevent.h"
#include "rmptfx/ptxinterface.h"
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxparticlerule.h"
#include "rmptfx/ptxd_drawmodel.h"
#include "rmptfx/ptxd_drawsprite.h"
#include "rmptfx/ptxd_drawtrail.h"

// includes (rage)
#include "bank/msgbox.h"
#include "file/asset.h"
#include "parser/manager.h"

// defines
#define PTXEFFECTRULE_PREUPDATE_TIMESTEP			(0.25f)

// optimisations
RMPTFX_OPTIMISATIONS()


// params
#if !__FINAL
XPARAM(ptfxassetoverride);
#endif

// namespaces
using namespace rage;

// static variables
//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxEffectRule::sm_colourTintMinDefn(			"Colour Tint Min",			atHashValue("ptxEffectRule:m_colourTintMinKFP"),			PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ONE),						Vec3V(0.0f, 	1.0f, 		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red",		"Green",		"Blue",		"Alpha",	true);
ptxKeyframeDefn ptxEffectRule::sm_colourTintMaxDefn(			"Colour Tint Max",			atHashValue("ptxEffectRule:m_colourTintMaxKFP"),			PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ONE),						Vec3V(0.0f, 	1.0f, 		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red",		"Green",		"Blue",		"Alpha",	true);
ptxKeyframeDefn ptxEffectRule::sm_zoomScalarDefn(				"Zoom Scalar",				atHashValue("ptxEffectRule:m_zoomScalarKFP"),				PTXKEYFRAMETYPE_FLOAT2,		Vec4V(ScalarVFromF32(100.0f)),		Vec3V(0.0f, 	1.0f, 		0.01f),		Vec3V(0.0f,		10000.0f,	0.01f),		"Min %",	"Max %");
ptxKeyframeDefn ptxEffectRule::sm_dataSphereDefn(				"Data Sphere",				atHashValue("ptxEffectRule:m_dataSphereKFP"),				PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ZERO),						Vec3V(0.0f, 	1.0f, 		0.01f),		Vec3V(-1000.0f,	1000.0f,	0.01f),		"Pos X",	"Pos Y",		"Pos Z",	"Radius");
ptxKeyframeDefn ptxEffectRule::sm_dataCapsuleDefn(				"Data Capsule",				atHashValue("ptxEffectRule:m_dataCapsuleKFP"),				PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ZERO),						Vec3V(0.0f, 	1.0f, 		0.01f),		Vec3V(-1000.0f,	1000.0f,	0.01f),		"Rot X",	"Rot Y",		"Rot Z",	"Length");																												


// code
#if RMPTFX_EDITOR
ptxEffectRuleUIData::ptxEffectRuleUIData()
: m_numPointsUpdated(0)
, m_numActiveInstances(0)
, m_numCulledInstances(0)
, m_accumCPUUpdateTime(0.0f)
, m_accumCPUDrawTime(0.0f)
, m_showInstances(false)
, m_showCullSpheres(false)
, m_showColn(false)
, m_showDataVolumes(false)
, m_showPointQuads(false)
{
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRuleUIData::StartProfileUpdate()
{
 	m_numPointsUpdated = 0;
 	m_numActiveInstances = 0;
	m_accumCPUUpdateTime = 0.0f;
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRuleUIData::StartProfileDraw()
{
	m_numCulledInstances = 0;
	m_accumCPUDrawTime = 0.0f;
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRuleUIData::SendDebugDataToEditor(ptxByteBuff& buff)
{
	buff.Write_bool(m_showInstances);
	buff.Write_bool(m_showCullSpheres);
	buff.Write_bool(m_showColn);
	buff.Write_bool(m_showDataVolumes);
	buff.Write_bool(m_showPointQuads);
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRuleUIData::ReceiveDebugDataFromEditor(ptxByteBuff& buff)
{
	m_showInstances = buff.Read_bool();
	m_showCullSpheres = buff.Read_bool();
	m_showColn = buff.Read_bool();
	m_showDataVolumes = buff.Read_bool();
	m_showPointQuads = buff.Read_bool();
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRuleUIData::AccumulateFrom(ptxEffectRuleUIData& uiData)
{
	m_numPointsUpdated += uiData.m_numPointsUpdated;
	m_numActiveInstances += uiData.m_numActiveInstances;
	m_numCulledInstances += uiData.m_numCulledInstances;
	m_accumCPUUpdateTime += uiData.m_accumCPUUpdateTime;
	m_accumCPUDrawTime += uiData.m_accumCPUDrawTime;
}
#endif




ptxEffectRule::ptxEffectRule() : pgBaseRefCounted(0)
{
	// file info
	m_fileVersion = PTXEFFECTRULE_FILE_VERSION;
	m_pFxList = NULL;

	// timeline and evolution data
	m_pEvolutionList = NULL;

	// effect setup
	m_vRandOffsetPos = Vec3V(V_ZERO);
	m_numLoops = 0;
	m_preUpdateTime = 0.0f;
	m_preUpdateTimeInterval = PTXEFFECTRULE_PREUPDATE_TIMESTEP;
	m_durationMin = 1.0f;
	m_durationMax = 1.0f;
	m_playbackRateScalarMin = 1.0f;
	m_playbackRateScalarMax = 1.0f;
	m_sortEventsByDistance = false;	
	m_drawListId = 0;
	m_isShortLived = false;
	m_hasNoShadows = true;

	// culling
	m_viewportCullingMode = 0;						
	m_renderWhenViewportCulled = false;				
	m_updateWhenViewportCulled = false;				
	m_emitWhenViewportCulled = false;
	m_distanceCullingMode = 0;						
	m_renderWhenDistanceCulled = false;				
	m_updateWhenDistanceCulled = false;				
	m_emitWhenDistanceCulled = false;				
	m_viewportCullingSphereRadius = 0.0f;			
	m_viewportCullingSphereOffset = Vec3V(V_ZERO);					
	m_distanceCullingFadeDist = 0.0f;				
	m_distanceCullingCullDist = 0.0f;

	// lod
	m_lodEvoDistMin = -1.0f;							
	m_lodEvoDistMax = -1.0f;	

	// collision	
	m_colnType = 0;					
	m_shareEntityColn = false;						
	m_onlyUseBVHColn = true;									
	m_colnRange = 0.0f;								
	m_colnProbeDist = 0.0f;			

	// game specific
	m_gameFlags = 0;

	// keyframes
	m_colourTintMaxEnable = false;
	m_useDataVolume = false;
	m_dataVolumeType = 0;

	Vec4V vOneHundred = Vec4V(ScalarVFromF32(100.0f));
	Vec4V vOne = Vec4V(V_ONE);
	Vec4V vZero = Vec4V(V_ZERO);
	m_colourTintMinKFP.Init(vOne, atHashValue("ptxEffectRule:m_colourTintMinKFP"));
	m_colourTintMaxKFP.Init(vOne, atHashValue("ptxEffectRule:m_colourTintMaxKFP"));
	m_zoomScalarKFP.Init(vOneHundred, atHashValue("ptxEffectRule:m_zoomScalarKFP"));
	m_dataSphereKFP.Init(vZero, atHashValue("ptxEffectRule:m_dataSphereKFP"));
	m_dataCapsuleKFP.Init(vZero, atHashValue("ptxEffectRule:m_dataCapsuleKFP"));

// #if RMPTFX_EDITOR
// 	m_keyframePropList.AddKeyframeProp(&m_colourTintMinKFP, &sm_colourTintMinDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_colourTintMaxKFP, &sm_colourTintMaxDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_zoomScalarKFP, &sm_zoomScalarDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_dataSphereKFP, &sm_dataSphereDefn);
// 	m_keyframePropList.AddKeyframeProp(&m_dataCapsuleKFP, &sm_dataCapsuleDefn);
// #else
	m_keyframePropList.AddKeyframeProp(&m_colourTintMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_colourTintMaxKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_zoomScalarKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_dataSphereKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_dataCapsuleKFP, NULL);
//#endif

	// zoom
	m_zoomLevel = 100.0f;

	// info
	m_numActiveInstances = 0;

	// ui data
	m_pUIData = NULL;

	SetKeyframeDefns();

#if RMPTFX_EDITOR
	CreateUIData();
#endif 
}

ptxEffectRule::~ptxEffectRule()
{
	RemoveDependents(false);
	delete[] (char*)m_pUIData;
}

ptxEffectRule::ptxEffectRule(datResource& rsc)
: pgBaseRefCounted(rsc)
, m_name(rsc)
, m_pFxList(rsc)
, m_timeline(rsc)
, m_pEvolutionList(rsc)
, m_colourTintMinKFP(rsc)
, m_colourTintMaxKFP(rsc)
, m_zoomScalarKFP(rsc)
, m_dataSphereKFP(rsc)
, m_dataCapsuleKFP(rsc)
, m_keyframePropList(rsc)
{
	SetKeyframeDefns();

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxEffectRule_num++;
#endif
}

void ptxEffectRule::PostLoad()
{
	CreateUIData();
#if !__RESOURCECOMPILER
	SetEvolvedKeyframeDefns();
#endif
}

#if __DECLARESTRUCT
void ptxEffectRule::DeclareStruct(datTypeStruct& s)
{
	pgBaseRefCounted::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxEffectRule, pgBaseRefCounted)				
		SSTRUCT_FIELD(ptxEffectRule, m_fileVersion)
		SSTRUCT_FIELD(ptxEffectRule, m_name)
		SSTRUCT_FIELD(ptxEffectRule, m_pFxList)
		SSTRUCT_FIELD(ptxEffectRule, m_timeline)
		SSTRUCT_FIELD(ptxEffectRule, m_pEvolutionList)
		SSTRUCT_FIELD(ptxEffectRule, m_numLoops)
		SSTRUCT_FIELD(ptxEffectRule, m_sortEventsByDistance)
		SSTRUCT_FIELD(ptxEffectRule, m_drawListId)
		SSTRUCT_FIELD(ptxEffectRule, m_isShortLived)
		SSTRUCT_FIELD(ptxEffectRule, m_hasNoShadows)
		SSTRUCT_FIELD(ptxEffectRule, m_vRandOffsetPos)
		SSTRUCT_FIELD(ptxEffectRule, m_preUpdateTime)
		SSTRUCT_FIELD(ptxEffectRule, m_preUpdateTimeInterval)
		SSTRUCT_FIELD(ptxEffectRule, m_durationMin)
		SSTRUCT_FIELD(ptxEffectRule, m_durationMax)
		SSTRUCT_FIELD(ptxEffectRule, m_playbackRateScalarMin)
		SSTRUCT_FIELD(ptxEffectRule, m_playbackRateScalarMax)		
		SSTRUCT_FIELD(ptxEffectRule, m_viewportCullingMode)			
		SSTRUCT_FIELD(ptxEffectRule, m_renderWhenViewportCulled)			
		SSTRUCT_FIELD(ptxEffectRule, m_updateWhenViewportCulled)			
		SSTRUCT_FIELD(ptxEffectRule, m_emitWhenViewportCulled)
		SSTRUCT_FIELD(ptxEffectRule, m_distanceCullingMode)			
		SSTRUCT_FIELD(ptxEffectRule, m_renderWhenDistanceCulled)			
		SSTRUCT_FIELD(ptxEffectRule, m_updateWhenDistanceCulled)			
		SSTRUCT_FIELD(ptxEffectRule, m_emitWhenDistanceCulled)
		SSTRUCT_FIELD(ptxEffectRule, m_viewportCullingSphereOffset)
		SSTRUCT_FIELD(ptxEffectRule, m_viewportCullingSphereRadius)
		SSTRUCT_FIELD(ptxEffectRule, m_distanceCullingFadeDist)		
		SSTRUCT_FIELD(ptxEffectRule, m_distanceCullingCullDist)
		SSTRUCT_FIELD(ptxEffectRule, m_lodEvoDistMin)					
		SSTRUCT_FIELD(ptxEffectRule, m_lodEvoDistMax)	
		SSTRUCT_FIELD(ptxEffectRule, m_colnRange)		
		SSTRUCT_FIELD(ptxEffectRule, m_colnProbeDist)	
		SSTRUCT_FIELD(ptxEffectRule, m_colnType)				
		SSTRUCT_FIELD(ptxEffectRule, m_shareEntityColn)			
		SSTRUCT_FIELD(ptxEffectRule, m_onlyUseBVHColn)
		SSTRUCT_FIELD(ptxEffectRule, m_gameFlags)
		SSTRUCT_FIELD(ptxEffectRule, m_colourTintMinKFP)
		SSTRUCT_FIELD(ptxEffectRule, m_colourTintMaxKFP)
		SSTRUCT_FIELD(ptxEffectRule, m_zoomScalarKFP)
		SSTRUCT_FIELD(ptxEffectRule, m_dataSphereKFP)
		SSTRUCT_FIELD(ptxEffectRule, m_dataCapsuleKFP)
		SSTRUCT_FIELD(ptxEffectRule, m_keyframePropList)
		SSTRUCT_FIELD(ptxEffectRule, m_colourTintMaxEnable)	
		SSTRUCT_FIELD(ptxEffectRule, m_useDataVolume)
		SSTRUCT_FIELD(ptxEffectRule, m_dataVolumeType)
		SSTRUCT_IGNORE(ptxEffectRule, m_pad3)
		SSTRUCT_FIELD(ptxEffectRule, m_numActiveInstances)
		SSTRUCT_FIELD(ptxEffectRule, m_zoomLevel)
		SSTRUCT_FIELD_VP(ptxEffectRule, m_pUIData)	
		SSTRUCT_IGNORE(ptxEffectRule, m_pad4)	
	SSTRUCT_END(ptxEffectRule)
}
#endif

IMPLEMENT_PLACE(ptxEffectRule);

void ptxEffectRule::Reset()
{
#if RMPTFX_EDITOR
	m_pUIData->StartProfileUpdate();
#endif
}

void ptxEffectRule::PrepareEvoData()
{
	if (m_pEvolutionList)
	{
		m_keyframePropList.PrepareEvoData(m_pEvolutionList);
	}
}

void ptxEffectRule::SetKeyframeDefns()
{
	m_colourTintMinKFP.GetKeyframe().SetDefn(&sm_colourTintMinDefn);
	m_colourTintMaxKFP.GetKeyframe().SetDefn(&sm_colourTintMaxDefn);
	m_zoomScalarKFP.GetKeyframe().SetDefn(&sm_zoomScalarDefn);
	m_dataSphereKFP.GetKeyframe().SetDefn(&sm_dataSphereDefn);
	m_dataCapsuleKFP.GetKeyframe().SetDefn(&sm_dataCapsuleDefn);
}

void ptxEffectRule::SetEvolvedKeyframeDefns()
{
	if (m_pEvolutionList)
	{
		m_pEvolutionList->SetKeyframeDefn(&m_colourTintMinKFP, &sm_colourTintMinDefn);
		m_pEvolutionList->SetKeyframeDefn(&m_colourTintMaxKFP, &sm_colourTintMaxDefn);
		m_pEvolutionList->SetKeyframeDefn(&m_zoomScalarKFP, &sm_zoomScalarDefn);
		m_pEvolutionList->SetKeyframeDefn(&m_dataSphereKFP, &sm_dataSphereDefn);
		m_pEvolutionList->SetKeyframeDefn(&m_dataCapsuleKFP, &sm_dataCapsuleDefn);

		// check for any evolved keyframe property that has a keyframe with no definitions - these must be old and need removed
		for (int j=0; j<m_pEvolutionList->GetNumEvolvedKeyframeProps(); j++)
		{
			ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = m_pEvolutionList->GetEvolvedKeyframePropByIndex(j);
			if (pEvolvedKeyframeProp)
			{
				for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
				{
					ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);
					ptxKeyframe& keyframe = evolvedKeyframe.GetKeyframe();
					ptxKeyframeDefn* pKeyframeDefn = keyframe.GetDefn();
					if (pKeyframeDefn==NULL)
					{
						ptxAssertf(0, "effect rule %s has an evolved keyframe from no definition (%u)", GetName(), pEvolvedKeyframeProp->GetPropertyId());
					}
				}
			}
		}
	}

	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		if (m_timeline.GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
		{
			ptxEventEmitter* pEmitterEvent = static_cast<ptxEventEmitter*>(m_timeline.GetEvent(i));
			if (pEmitterEvent)
			{
				pEmitterEvent->SetEvolvedKeyframeDefns(this);
			}
		}
	}
}

void ptxEffectRule::RemoveDependents(bool UNUSED_PARAM(removeFromManager))
{
	// release all the references to emitter rules on the timeline events
	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		if (m_timeline.GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER)
		{
			ptxEventEmitter* pEmitterEvent = static_cast<ptxEventEmitter*>(m_timeline.GetEvent(i));

			if (pEmitterEvent->GetEmitterRule())
			{
				pEmitterEvent->GetEmitterRule()->Release();
				pEmitterEvent->SetEmitterRule(NULL);
			}

			if (pEmitterEvent->GetParticleRule())
			{
				pEmitterEvent->GetParticleRule()->Release();
				pEmitterEvent->SetParticleRule(NULL);
			}
		}
	}
}

bool ptxEffectRule::HasBehaviour(const char* pBehaviourName)
{
	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		// get the emitter event's particle rule
		ptxEventEmitter* pEmitterEvent =  static_cast<ptxEventEmitter*>(m_timeline.GetEvent(i));
		ptxParticleRule* pParticleRule = pEmitterEvent->GetParticleRule();
		if (pParticleRule)
		{
			if (pParticleRule->GetBehaviour(pBehaviourName)!=NULL)
			{
				return true;
			}
		}
	}

	return false;
}

float ptxEffectRule::GetMaxDuration()
{
	// NOTE: this function does NOT take into account any evolutions on the emitter playback rate and particle life keyframe properties

	// if we loop infinitely then we cannot estimate the life
	if (GetIsInfinitelyLooped())
	{
		return -1.0f;
	}

	// store the max effect life
	float maxEffectLife = m_durationMax;

	// go through the events 
	float maxEventLife = 0.0f;
	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		// get the emitter event
		ptxEventEmitter* pEmitterEvent =  static_cast<ptxEventEmitter*>(m_timeline.GetEvent(i));

		// get the emitter rule
		ptxEmitterRule* pEmitterRule = pEmitterEvent->GetEmitterRule();
		if (pEmitterRule)
		{
			// work out when the last point will be emitted this loop
			float lastEmissionTime = 0.0f;
			if (pEmitterRule->GetIsOneShot())
			{
				lastEmissionTime = pEmitterEvent->GetStartRatio()*maxEffectLife;
			}
			else
			{
				lastEmissionTime = pEmitterEvent->GetEndRatio()*maxEffectLife;
			}

			// work out the min playback (non evolved)
			float minPointPlaybackRate = 100.0f;
			ptxKeyframeProp& playbackRateKFP = pEmitterRule->GetPlaybackRateScalarKFP();
			if (m_pEvolutionList && m_pEvolutionList->GetEvolvedKeyframePropById(playbackRateKFP.GetPropertyId()))
			{
				// there are evolutions on this property - this makes it too difficult to calculate an accurate duration
				return -1.0f;
			}
			ptxKeyframe& playbackRateKF = playbackRateKFP.GetKeyframe();
			for (int j=0; j<playbackRateKF.GetNumEntries(); j++)
			{
				Vec4V_Out keyframeEntryValue = playbackRateKF.GetValue(j);
				float keyframeEntryMinPointPlaybackRate = keyframeEntryValue.GetXf();
				minPointPlaybackRate = Min(minPointPlaybackRate, keyframeEntryMinPointPlaybackRate);
			}

			// work out the max point life (non evolved)
			float maxPointLife = 0.0f;
			ptxKeyframeProp& particleLifeKFP = pEmitterRule->GetParticleLifeKFP();
			if (m_pEvolutionList && m_pEvolutionList->GetEvolvedKeyframePropById(particleLifeKFP.GetPropertyId()))
			{
				// there are evolutions on this property - this makes it too difficult to calculate an accurate duration
				return -1.0f;
			}
			ptxKeyframe& particleLifeKF = particleLifeKFP.GetKeyframe();
			for (int j=0; j<particleLifeKF.GetNumEntries(); j++)
			{
				Vec4V_Out keyframeEntryValue = particleLifeKF.GetValue(j);
				float keyframeEntryMaxPointLife = keyframeEntryValue.GetYf();
				maxPointLife = Max(maxPointLife, keyframeEntryMaxPointLife);
			}

			// calc the event's min playback rate
			float eventMinPlaybackRate = minPointPlaybackRate*pEmitterEvent->GetPlaybackRateScalarMin()*m_playbackRateScalarMin;

			// scale by the event's min playback rate
			maxPointLife /= eventMinPlaybackRate;

			// update the max event life
			maxEventLife = Max(maxEventLife, lastEmissionTime+maxPointLife);
		}
	}

	return (maxEffectLife*GetNumLoops()) + maxEventLife;
}

bool ptxEffectRule::CheckForPtxRuleShadows()
{
	bool bNoShadows = true;
	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		// get the emitter event
		ptxEventEmitter* pEmitterEvent =  static_cast<ptxEventEmitter*>(m_timeline.GetEvent(i));

		// get the emitter rule
		ptxParticleRule* pParticleRule = pEmitterEvent->GetParticleRule();
		if (pParticleRule)
		{
			ptxDrawType drawType = pParticleRule->GetDrawType();

			if(drawType == PTXPARTICLERULE_DRAWTYPE_MODEL)
			{
				ptxd_Model* pDrawBehavior = static_cast<ptxd_Model*>(pParticleRule->GetBehaviour("ptxd_Model"));
				if(pDrawBehavior && pDrawBehavior->m_shadowCastIntensity >= RMPTFX_MIN_PARTICLE_SHADOW_INTENSITY)
				{
					bNoShadows = false;
				}
			}
			else if(drawType == PTXPARTICLERULE_DRAWTYPE_SPRITE)
			{
				ptxd_Sprite* pDrawBehavior = static_cast<ptxd_Sprite*>(pParticleRule->GetBehaviour("ptxd_Sprite"));
				if(pDrawBehavior && pDrawBehavior->m_shadowCastIntensity >= RMPTFX_MIN_PARTICLE_SHADOW_INTENSITY)
				{
					bNoShadows = false;
				}

			}
			else if(drawType == PTXPARTICLERULE_DRAWTYPE_TRAIL)
			{
				ptxd_Trail* pDrawBehavior = static_cast<ptxd_Trail*>(pParticleRule->GetBehaviour("ptxd_Trail"));
				if(pDrawBehavior && pDrawBehavior->m_shadowCastIntensity >= RMPTFX_MIN_PARTICLE_SHADOW_INTENSITY)
				{
					bNoShadows = false;
				}
			}			
		}
	}
	
	return bNoShadows;
}

#if RMPTFX_XML_LOADING
ptxEffectRule* ptxEffectRule::CreateFromFile(const char* pFilename)
{
	ptxEffectRule* pEffectRule = NULL;

	// load the effect rule
	ASSET.PushFolder(RMPTFXMGR.GetAssetPath());
	ASSET.PushFolder("effectrules");
	PARSER.LoadObjectPtr(pFilename, "effectrule", pEffectRule);
	ASSET.PopFolder();
	ASSET.PopFolder();

	if (pEffectRule)
	{
		pEffectRule->RepairAndUpgradeData(pFilename);
		pEffectRule->ResolveReferences(NULL);
#if RMPTFX_EDITOR
		pEffectRule->CreateUiObjects();
#endif
	}

	return pEffectRule;
}
#endif

#if RMPTFX_XML_LOADING
bool ptxEffectRule::Load(const char* pFilename)
{
	// load the effect rule
	ASSET.PushFolder(RMPTFXMGR.GetAssetPath());
	ASSET.PushFolder("effectrules");
	bool loadedOK = PARSER.LoadObject(pFilename, "effectrule", *this);
	ASSET.PopFolder();
	ASSET.PopFolder();

	return loadedOK;
}
#endif

#if RMPTFX_XML_LOADING
void ptxEffectRule::RepairAndUpgradeData(const char* pFilename)
{
	// ensure that the names are the same
	if (strcasecmp(pFilename, GetName()) != 0)
	{
		// they're not set ours
		SetName(pFilename);
	}

	// repair and upgrade the timeline data
	m_timeline.RepairAndUpgradeData(pFilename);

	// check for overflowing of timeline events
	if (m_timeline.GetNumEvents() > PTXEFFECTINST_MAX_EVENT_INSTS)
	{
		ptxMsg::AssetErrorf("too many events in effect '%s' - max is %u", GetName(), PTXEFFECTINST_MAX_EVENT_INSTS);
		m_timeline.ResizeEvents(PTXEFFECTINST_MAX_EVENT_INSTS);
	}

	// add any missing behaviours that have active evolutions on them
	if (m_pEvolutionList)
	{
		AddBehavioursWithActiveEvos();
	}
}
#endif

#if RMPTFX_XML_LOADING
void ptxEffectRule::AddBehavioursWithActiveEvos()
{
	// go through the timeline events
	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		// only process emitter events with evolutions
		if (m_timeline.GetEvent(i) && m_timeline.GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER && m_timeline.GetEvent(i)->GetEvolutionList())
		{
			// get the emitter event's particle rule
			ptxEventEmitter* pEmitterEvent =  static_cast<ptxEventEmitter*>(m_timeline.GetEvent(i));
			ptxParticleRule* pParticleRule = pEmitterEvent->GetParticleRule();
			if (pParticleRule)
			{
				// go through the emitter evolution list properties
				for (int j=0; j<pEmitterEvent->GetEvolutionList()->GetNumEvolvedKeyframeProps(); j++)
				{
					ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = pEmitterEvent->GetEvolutionList()->GetEvolvedKeyframePropByIndex(j);

					// find the behaviour that this evolution property belongs to
					ptxBehaviour* pBehaviour = RMPTFXMGR.FindBehaviourFromPropertyId(pEvolvedKeyframeProp->GetPropertyId());
					if (pBehaviour)
					{
						// check if the particle rule has this behaviour
						if (pParticleRule->GetBehaviourIndex(pBehaviour->GetName())==-1)
						{
							// it doesn't - add it
							pParticleRule->AddBehaviour(pBehaviour->GetName());
						}
					}
				}
			}
		}
	}
}
#endif

#if RMPTFX_XML_LOADING
void ptxEffectRule::ResolveReferences(ptxFxList* pFxList)
{
	m_pFxList = pFxList;
	m_timeline.ResolveReferences();
	m_numActiveInstances = 0;
}
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
void ptxEffectRule::Save()
{
#if __ASSERT
	// check for multiple entries of the same keyframe property
	for (int k=0; k<m_timeline.GetNumEvents(); k++)
	{
		ptxEvolutionList* pEvoList = m_timeline.GetEvent(k)->GetEvolutionList();
		if (pEvoList)
		{
			for (int i=0; i<pEvoList->GetNumEvolvedKeyframeProps(); i++)
			{
				for (int j=i+1; j<pEvoList->GetNumEvolvedKeyframeProps(); j++)
				{
					if (pEvoList->GetEvolvedKeyframePropByIndex(i)->GetPropertyId()==pEvoList->GetEvolvedKeyframePropByIndex(j)->GetPropertyId())
					{
						ptxAssertf(0, "multiple entries of the same keyframe property (%u) found in emitter %d of effect %s", pEvoList->GetEvolvedKeyframePropByIndex(i)->GetPropertyId(), k, GetName());
					}
				}
			}
		}
	}

	// check flags on evolved keyframes that are lod evolutions
	if (m_pEvolutionList)
	{
		for (int j=0; j<m_pEvolutionList->GetNumEvolvedKeyframeProps(); j++)
		{
			ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = m_pEvolutionList->GetEvolvedKeyframePropByIndex(j);

			if (pEvolvedKeyframeProp)
			{
				// get the index of any lod evolution
				int lodEvoIdx = m_pEvolutionList->GetEvolutionIndexFromHash(ATSTRINGHASH("LOD", 0x2FBE6388));

				// flag any evolved lod keyframes
				for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
				{
					ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);
					ptxAssertf(evolvedKeyframe.GetIsLodEvolution()==(evolvedKeyframe.GetEvolutionIndex()==lodEvoIdx), "evolved keyframe is not flagged correctly as an lod evolution");
				}
			}
		}
	}

	for (int k=0; k<m_timeline.GetNumEvents(); k++)
	{
		ptxEvolutionList* pEvoList = m_timeline.GetEvent(k)->GetEvolutionList();
		if (pEvoList)
		{
			for (int j=0; j<pEvoList->GetNumEvolvedKeyframeProps(); j++)
			{
				ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = pEvoList->GetEvolvedKeyframePropByIndex(j);

				if (pEvolvedKeyframeProp)
				{
					// get the index of any lod evolution
					int lodEvoIdx = pEvoList->GetEvolutionIndexFromHash(ATSTRINGHASH("LOD", 0x2FBE6388));

					// flag any evolved lod keyframes
					for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
					{
						ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);
						ptxAssertf(evolvedKeyframe.GetIsLodEvolution()==(evolvedKeyframe.GetEvolutionIndex()==lodEvoIdx), "evolved keyframe is not flagged correctly as an lod evolution");
					}
				}
			}
		}
	}
#endif

	// set whether this effect is short lived
	float maxDuration = GetMaxDuration();
	m_isShortLived = maxDuration>-1.0f && maxDuration<2.0f;

	m_hasNoShadows = CheckForPtxRuleShadows();

#if RMPTFX_EDITOR
	RemoveDefaultKeyframes();
#endif

	// set the current file version
	m_fileVersion = PTXEFFECTRULE_FILE_VERSION;

	// save the effect rule
	ASSET.PushFolder(RMPTFXMGR.GetAssetPath());
	ASSET.PushFolder("effectrules");

#if !__FINAL
	bool folderOverrideUsed = false;
	const char* pPtfxAssetOverride = NULL;
	PARAM_ptfxassetoverride.Get(pPtfxAssetOverride);
	if (pPtfxAssetOverride)
	{
		ASSET.PushFolder(pPtfxAssetOverride);
		if (ASSET.Exists(GetName(), "effectrule"))
		{
			folderOverrideUsed = true;
		}
		else
		{
			ASSET.PopFolder();
		}
	}
#endif

	ASSET.CreateLeadingPath(GetName());
	bool savedOK = PARSER.SaveObject(GetName(), "effectrule", this);

#if __FINAL
	if (folderOverrideUsed)
	{
		ASSET.PopFolder();
	}
#endif

	ASSET.PopFolder();
	ASSET.PopFolder();

	if (savedOK)
	{
		m_zoomLevel = 100.0f;
	}
	else
	{
		// output error message
		char msg[512];
		char path[512];
		formatf(path, sizeof(path), "%s\\%s", RMPTFXMGR.GetAssetPath(), "effectrules");
		formatf(msg, sizeof(msg), "Unable to save file: %s\\%s.effectrule, check permissions.", path, GetName());
		bkMessageBox("RMPTFX", msg, bkMsgOk, bkMsgInfo);
	}
}
#endif

#if __RESOURCECOMPILER || RMPTFX_EDITOR
void ptxEffectRule::SaveAll()
{
	m_timeline.Save();
	Save();
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::SendToEditor(ptxByteBuff& buff)
{
	// file info
	buff.Write_float(PTXEFFECTRULE_FILE_VERSION);   
	buff.Write_const_char(GetName());  

	// timeline and evolution data
	m_timeline.SendToEditor(buff, this);
	if (m_pEvolutionList)
	{	
		m_pEvolutionList->UpdateUIData(&m_keyframePropList, this, -1);
		buff.Write_bool(true);
		m_pEvolutionList->SendToEditor(buff, this, -1);
	}
	else
	{
		buff.Write_bool(false);
	}

	// effect setup
	buff.Write_float(m_vRandOffsetPos.GetXf());
	buff.Write_float(m_vRandOffsetPos.GetYf());
	buff.Write_float(m_vRandOffsetPos.GetZf());
	buff.Write_s32(m_numLoops);
	buff.Write_float(m_preUpdateTime);
	buff.Write_float(m_preUpdateTimeInterval);
	buff.Write_float(m_durationMin);
	buff.Write_float(m_durationMax);
	buff.Write_float(m_playbackRateScalarMin);
	buff.Write_float(m_playbackRateScalarMax);				
	buff.Write_bool(m_sortEventsByDistance);
	buff.Write_u8(m_drawListId);

	// culling
	buff.Write_u8(m_viewportCullingMode);					
	buff.Write_bool(m_renderWhenViewportCulled);			
	buff.Write_bool(m_updateWhenViewportCulled);			
	buff.Write_bool(m_emitWhenViewportCulled);	
	buff.Write_u8(m_distanceCullingMode);					
	buff.Write_bool(m_renderWhenDistanceCulled);			
	buff.Write_bool(m_updateWhenDistanceCulled);			
	buff.Write_bool(m_emitWhenDistanceCulled);
	buff.Write_float(m_viewportCullingSphereRadius);		
	buff.Write_float(m_viewportCullingSphereOffset.GetXf());
	buff.Write_float(m_viewportCullingSphereOffset.GetYf());
	buff.Write_float(m_viewportCullingSphereOffset.GetZf());
	buff.Write_float(m_distanceCullingFadeDist);			
	buff.Write_float(m_distanceCullingCullDist);

	// lod
	buff.Write_float(m_lodEvoDistMin);						
	buff.Write_float(m_lodEvoDistMax);	

	// collision
	buff.Write_u8(m_colnType);						
	buff.Write_bool(m_shareEntityColn);						
	buff.Write_bool(m_onlyUseBVHColn);							
	buff.Write_float(m_colnRange);							
	buff.Write_float(m_colnProbeDist);

	// game specific
	int numGameFlags = RMPTFXMGR.GetNumEffectRuleGameFlagNames();
	buff.Write_s32(numGameFlags);
	for (int i=0; i<numGameFlags; i++)
	{
		buff.Write_bool(GetGameFlag(i));
	}

	// keyframes
	buff.Write_bool(m_colourTintMaxEnable);
	buff.Write_bool(m_useDataVolume);
	buff.Write_u8(m_dataVolumeType);
	m_keyframePropList.SendToEditor(buff, GetName(), PTXRULETYPE_EFFECT);

	// zoom
	buff.Write_float(m_zoomLevel);

	// info

	// ui data
	m_pUIData->SendDebugDataToEditor(buff);
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::ReceiveFromEditor(ptxByteBuff & buff)
{
	// file info
	buff.Read_float();									// skip version
	SetName(buff.Read_const_char());

	// timeline and evolution data
	m_timeline.ReceiveFromEditor(buff);
	bool hasEvoData = buff.Read_bool();
	if (hasEvoData && m_pEvolutionList)
	{
		m_pEvolutionList->ReceiveFromEditor(buff);
	}

	// effect setup
	m_vRandOffsetPos.SetXf(buff.Read_float());
	m_vRandOffsetPos.SetYf(buff.Read_float());
	m_vRandOffsetPos.SetZf(buff.Read_float());
	m_numLoops = buff.Read_s32();
	m_preUpdateTime = buff.Read_float();
	m_preUpdateTimeInterval = buff.Read_float();
	m_durationMin = buff.Read_float();
	m_durationMax = buff.Read_float();
	m_playbackRateScalarMin = buff.Read_float();
	m_playbackRateScalarMax = buff.Read_float();
	m_sortEventsByDistance = buff.Read_bool();
	m_drawListId = buff.Read_u8();

	// culling	
	m_viewportCullingMode = buff.Read_u8();					
	m_renderWhenViewportCulled = buff.Read_bool();			
	m_updateWhenViewportCulled = buff.Read_bool();			
	m_emitWhenViewportCulled = buff.Read_bool();
	m_distanceCullingMode = buff.Read_u8();					
	m_renderWhenDistanceCulled = buff.Read_bool();			
	m_updateWhenDistanceCulled = buff.Read_bool();			
	m_emitWhenDistanceCulled = buff.Read_bool();
	m_viewportCullingSphereRadius = buff.Read_float();		
	m_viewportCullingSphereOffset.SetXf(buff.Read_float());	
	m_viewportCullingSphereOffset.SetYf(buff.Read_float());	
	m_viewportCullingSphereOffset.SetZf(buff.Read_float());	
	m_distanceCullingFadeDist = buff.Read_float();			
	m_distanceCullingCullDist = buff.Read_float();						

	// lod
	m_lodEvoDistMin = buff.Read_float();						
	m_lodEvoDistMax = buff.Read_float();

	// collision
	m_colnType = buff.Read_u8();				
	m_shareEntityColn = buff.Read_bool();					
	m_onlyUseBVHColn = buff.Read_bool();								
	m_colnRange = buff.Read_float();						
	m_colnProbeDist = buff.Read_float();		

	// game specific
	int numFlags = buff.Read_s32();
	for (int i=0; i<numFlags; i++)
	{
		SetGameFlag(i, buff.Read_bool());
	}

	// keyframes
	m_colourTintMaxEnable = buff.Read_bool();								
	m_useDataVolume = buff.Read_bool();
	m_dataVolumeType = buff.Read_u8();		

	// zoom
	m_zoomLevel = buff.Read_float();

	// info

	// ui data
	m_pUIData->ReceiveDebugDataFromEditor(buff);
}
#endif

#if RMPTFX_EDITOR
bool ptxEffectRule::ContainsParticleRule(const char* pParticleRuleName)
{
	// go through the timeline events
	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		// check if this event contains the particle rule
		if (m_timeline.GetEvent(i)->ContainsParticleRule(pParticleRuleName))
		{
			return true;
		}
	}

	return false;
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::RescaleZoomableComponents()
{
	float zoomableComponentScalar = m_zoomLevel*0.01f;

	m_viewportCullingSphereOffset *= ScalarVFromF32(zoomableComponentScalar);
	m_viewportCullingSphereRadius *= zoomableComponentScalar;
	m_distanceCullingCullDist *= zoomableComponentScalar;
	m_distanceCullingFadeDist *= zoomableComponentScalar;
	m_lodEvoDistMin *= zoomableComponentScalar;
	m_lodEvoDistMax *= zoomableComponentScalar;

	// normalise the zoom on the timeline events
	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		m_timeline.GetEvent(i)->RescaleZoomableComponents(zoomableComponentScalar);
	}

	m_zoomLevel = 100.0f;

	// send the update effect rule to the editor
#if RMPTFX_EDITOR
	RMPTFXMGR.GetInterface().SendEffectRule(m_name);
#endif
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::CreateEvolution(const char* pEvoName)
{
	// create a new evolution list if we don't have one already
	if (m_pEvolutionList == NULL)
	{
		m_pEvolutionList = rage_new ptxEvolutionList();
	}

	// get the evolution name
	char newEvoName[128];
	if ((pEvoName==NULL) || strlen(pEvoName)==0)
	{
		// generate a new name when first added
		m_pEvolutionList->GenerateUniqueName(newEvoName, sizeof(newEvoName));
	}
	else
	{
		formatf(newEvoName, sizeof(newEvoName), "%s", pEvoName);
	}

	// add the evolution to the list
	m_pEvolutionList->AddEvolution(newEvoName);

	// create the evolution on each of the timeline events
	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		m_timeline.GetEvent(i)->CreateEvolution(newEvoName);
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::DeleteEvolution(const char* pEvoName)
{
	if (m_pEvolutionList)
	{
		m_pEvolutionList->DeleteEvolution(pEvoName);

		// delete the evolution on each of the timeline events
		for (int i=0; i<m_timeline.GetNumEvents(); i++)
		{
			m_timeline.GetEvent(i)->DeleteEvolution(pEvoName);
		}

		// delete the evolution list if it's now empty
		if (m_pEvolutionList->GetNumEvolutions()==0)
		{
			delete m_pEvolutionList;
			m_pEvolutionList = NULL;
		}

		// validate the behaviours (we may have removed an evolution that was keeping a behaviour active)
		for (int i=0; i<m_timeline.GetNumEvents(); i++)
		{
			// look only for emitter events which have evolutions
			if (m_timeline.GetEvent(i) && m_timeline.GetEvent(i)->GetType()==PTXEVENT_TYPE_EMITTER && m_timeline.GetEvent(i)->GetEvolutionList())
			{
				ptxEventEmitter* pEmitterEvent = static_cast<ptxEventEmitter*>(m_timeline.GetEvent(i));
				ptxParticleRule* pParticleRule = pEmitterEvent->GetParticleRule();
				if (pParticleRule)
				{
					pParticleRule->ValidateAllBehaviours();
				}
			}
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::AddEvolvedKeyframeProp(const char* pEvoName, u32 propertyId, int eventId)
{
	if (m_pEvolutionList)
	{
		// add the evolution property
		if (eventId>=0)
		{
			// add to the event's evolution list
			ptxAssertf((eventId<m_timeline.GetNumEvents()), "timeline event id is out of range");
			m_timeline.GetEvent(eventId)->AddEvolvedKeyframeProp(propertyId, pEvoName);
		}
		else
		{
			// add to the effect rule's evolution list
			ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByPropId(propertyId);
			m_pEvolutionList->AddEvolvedKeyframeProp(pKeyframeProp, pEvoName);
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::DeleteEvolvedKeyframeProp(const char* pEvoName, u32 propertyId, int eventId)
{
	if (m_pEvolutionList)
	{
		// delete the evolution property
		if (eventId>=0)
		{
			// delete from the event's evolution list
			ptxAssertf((eventId<m_timeline.GetNumEvents()), "timeline event id is out of range");
			m_timeline.GetEvent(eventId)->DeleteEvolvedKeyframeProp(propertyId, pEvoName);
		}
		else
		{
			// delete from the our evolution list
			ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByPropId(propertyId);
			m_pEvolutionList->DeleteEvolvedKeyframeProp(pKeyframeProp, pEvoName);
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::UpdateEvoUI(ptxKeyframeProp* pKeyframeProp)
{
	// update our ui evo data
	if (m_pEvolutionList)
	{
		m_pEvolutionList->UpdateUIData(pKeyframeProp, this, -1);
	}

	// update the timeline events' ui evo data
	for (int i=0; i<m_timeline.GetNumEvents(); i++)
	{
		if (m_timeline.GetEvent(i)->GetEvolutionList())
		{
			m_timeline.GetEvent(i)->GetEvolutionList()->UpdateUIData(pKeyframeProp, this, i);
		}
	}
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::StartProfileUpdate()
{
	m_pUIData->StartProfileUpdate();
	m_timeline.StartProfileUpdate();
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::StartProfileDraw()
{
	m_pUIData->StartProfileDraw();
	m_timeline.StartProfileDraw();
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::CreateUiObjects()
{
	m_timeline.CreateUiObjects();
}
#endif

#if RMPTFX_EDITOR
void ptxEffectRule::CreateUIData()
{
	m_pUIData = rage_new ptxEffectRuleUIData;
}
#endif

#if RMPTFX_BANK || RMPTFX_EDITOR
void ptxEffectRule::RemoveDefaultKeyframes()
{
	if (m_pEvolutionList)
	{
		for (int j=0; j<m_pEvolutionList->GetNumEvolvedKeyframeProps(); j++)
		{
			ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = m_pEvolutionList->GetEvolvedKeyframePropByIndex(j);

			if (pEvolvedKeyframeProp)
			{
				for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
				{
					ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

					ptxKeyframe& keyframe = evolvedKeyframe.GetKeyframe();
					if (keyframe.IsDefault())
					{
						keyframe.DeleteEntry(0);
					}
				}
			}
		}
	}

	for (int k=0; k<m_timeline.GetNumEvents(); k++)
	{
		ptxEvent* pEvent = m_timeline.GetEvent(k);

		ptxEvolutionList* pEvolutionList = pEvent->GetEvolutionList();

		if (pEvolutionList)
		{
			for (int j=0; j<pEvolutionList->GetNumEvolvedKeyframeProps(); j++)
			{
				ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = pEvolutionList->GetEvolvedKeyframePropByIndex(j);

				if (pEvolvedKeyframeProp)
				{
					for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
					{
						ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

						ptxKeyframe& keyframe = evolvedKeyframe.GetKeyframe();
						if (keyframe.IsDefault())
						{
							keyframe.DeleteEntry(0);
						}
					}
				}
			}
		}
	}

	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByIndex(i);
		if (pKeyframeProp)
		{
			ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();
			if (keyframe.IsDefault())
			{
				keyframe.DeleteEntry(0);
			}
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxEffectRule::GetKeyframeEntryInfo(int& num, int& mem, int& max, int& def)
{
	if (m_pEvolutionList)
	{
		for (int j=0; j<m_pEvolutionList->GetNumEvolvedKeyframeProps(); j++)
		{
			ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = m_pEvolutionList->GetEvolvedKeyframePropByIndex(j);

			if (pEvolvedKeyframeProp)
			{
				for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
				{
					ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

					ptxKeyframe& keyframe = evolvedKeyframe.GetKeyframe();

					int currNum = keyframe.GetNumEntries();

					num += currNum;
					mem += keyframe.GetEntryMem();

					if (currNum>max)
					{
						max = currNum;
					}

					if (keyframe.IsDefault())
					{
						def++;
					}
				}
			}
		}
	}

	for (int k=0; k<m_timeline.GetNumEvents(); k++)
	{
		ptxEvent* pEvent = m_timeline.GetEvent(k);

		ptxEvolutionList* pEvolutionList = pEvent->GetEvolutionList();

		if (pEvolutionList)
		{
			for (int j=0; j<pEvolutionList->GetNumEvolvedKeyframeProps(); j++)
			{
				ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = pEvolutionList->GetEvolvedKeyframePropByIndex(j);

				if (pEvolvedKeyframeProp)
				{
					for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
					{
						ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

						ptxKeyframe& keyframe = evolvedKeyframe.GetKeyframe();

						int currNum = keyframe.GetNumEntries();

						num += currNum;
						mem += keyframe.GetEntryMem();

						if (currNum>max)
						{
							max = currNum;
						}

						if (keyframe.IsDefault())
						{
							def++;
						}
					}
				}
			}
		}
	}

	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByIndex(i);
		if (pKeyframeProp)
		{
			ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();

			int currNum = keyframe.GetNumEntries();

			num += currNum;
			mem += keyframe.GetEntryMem();

			if (currNum>max)
			{
				max = currNum;
			}

			if (keyframe.IsDefault())
			{
				def++;
			}
		}
	}
}
#endif

#if RMPTFX_BANK
void ptxEffectRule::OutputKeyframeEntryInfo()
{
	if (m_pEvolutionList)
	{
		for (int j=0; j<m_pEvolutionList->GetNumEvolvedKeyframeProps(); j++)
		{
			ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = m_pEvolutionList->GetEvolvedKeyframePropByIndex(j);

			if (pEvolvedKeyframeProp)
			{
				for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
				{
					ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

					ptxKeyframe& keyframe = evolvedKeyframe.GetKeyframe();
					ptxKeyframeDefn* pKeyframeDefn = keyframe.GetDefn();
					if (ptxVerifyf(pKeyframeDefn, "keyframe doesn't have a definition"))
					{
						ptxAssertf(pEvolvedKeyframeProp->GetPropertyId()==pKeyframeDefn->GetPropertyId(), "property ids don't match");
						ptxDebugf1("                  %d - %s (%u) (%s)", keyframe.GetNumEntries(), pKeyframeDefn->GetName(), pKeyframeDefn->GetPropertyId(), m_pEvolutionList->GetEvolutionName(evolvedKeyframe.GetEvolutionIndex()));
					}
				}
			}
		}
	}

	for (int k=0; k<m_timeline.GetNumEvents(); k++)
	{
		ptxEvent* pEvent = m_timeline.GetEvent(k);

		ptxEvolutionList* pEvolutionList = pEvent->GetEvolutionList();

		if (pEvolutionList)
		{
			for (int j=0; j<pEvolutionList->GetNumEvolvedKeyframeProps(); j++)
			{
				ptxEvolvedKeyframeProp* pEvolvedKeyframeProp = pEvolutionList->GetEvolvedKeyframePropByIndex(j);

				if (pEvolvedKeyframeProp)
				{
					for (int i=0; i<pEvolvedKeyframeProp->GetNumEvolvedKeyframes(); i++)
					{
						ptxEvolvedKeyframe& evolvedKeyframe = pEvolvedKeyframeProp->GetEvolvedKeyframe(i);

						ptxKeyframe& keyframe = evolvedKeyframe.GetKeyframe();
						ptxKeyframeDefn* pKeyframeDefn = keyframe.GetDefn();
						if (ptxVerifyf(pKeyframeDefn, "keyframe doesn't have a definition"))
						{
							ptxAssertf(pEvolvedKeyframeProp->GetPropertyId()==pKeyframeDefn->GetPropertyId(), "property ids don't match");
							ptxDebugf1("                  %d - %s (%u) (%s)", keyframe.GetNumEntries(), pKeyframeDefn->GetName(), pKeyframeDefn->GetPropertyId(), m_pEvolutionList->GetEvolutionName(evolvedKeyframe.GetEvolutionIndex()));
						}
					}
				}
			}
		}
	}

	for (int i=0; i<m_keyframePropList.GetNumKeyframeProps(); i++)
	{
		ptxKeyframeProp* pKeyframeProp = m_keyframePropList.GetKeyframePropByIndex(i);
		if (pKeyframeProp)
		{
			ptxKeyframe& keyframe = pKeyframeProp->GetKeyframe();
			ptxKeyframeDefn* pKeyframeDefn = keyframe.GetDefn();
			if (ptxVerifyf(pKeyframeDefn, "keyframe doesn't have a definition"))
			{
				ptxAssertf(pKeyframeProp->GetPropertyId()==pKeyframeDefn->GetPropertyId(), "property ids don't match");
				ptxDebugf1("                  %d - %s (%u)", keyframe.GetNumEntries(), pKeyframeDefn->GetName(), pKeyframeDefn->GetPropertyId());
			}
		}
	}
}
#endif
