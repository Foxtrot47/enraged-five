<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Pause" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
  <float name="m_pauseLifeRatio"/>
  <float name="m_unpauseTime"/>
  <float name="m_unpauseEffectDist"/>
  <float name="m_unpauseCamDist"/>
</structdef>

</ParserSchema>