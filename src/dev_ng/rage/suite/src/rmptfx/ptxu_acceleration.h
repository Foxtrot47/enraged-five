// 
// rmptfx/ptxu_acceleration.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_ACCELERATION_H
#define PTXU_ACCELERATION_H


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// enumerations
enum ptxu_Acceleration_ReferenceSpace
{
	PTXU_ACCELERATION_REFERENCESPACE_WORLD	= 0,
	PTXU_ACCELERATION_REFERENCESPACE_EFFECT,
	PTXU_ACCELERATION_REFERENCESPACE_EMITTER,

	PTXU_ACCELERATION_REFERENCESPACE_NUM
};


// classes
class ptxu_Acceleration : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Acceleration();
#endif
		void SetDefaultData();
	
		// resourcing
		explicit ptxu_Acceleration(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Acceleration);

		// info
		const char* GetName() {return "ptxu_Acceleration";}										// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_ACCELERATION;}									// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}														// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}														// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return false;}											// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}														// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}													// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);												// called for each newly created particle if NeedsToInit returns true						
		void UpdatePoint(ptxBehaviourParams& params);											// called from InitPoint and UpdatePoint to update the particle accordingly 						

		void SetKeyframeDefns();																// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);							// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Acceleration";}									// name of the behaviour shown in the editor
		bool IsActive();																		// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);												// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);		// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);								// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif

	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_xyzMinKFP;
		ptxKeyframeProp m_xyzMaxKFP;

		// global data
		float* m_pGlobalData_NOTUSED;														// should always point to sm_globalData - so spu can load the global data

		// non keyframe data
		int m_referenceSpace;
		bool m_isAffectedByZoom;															// whether the acceleration is affected by zoom
		bool m_enableGravity;																// whether to scale the z component with game gravity	

		datPadding<6> m_pad;

		// static data
		static ptxKeyframeDefn sm_xyzMinDefn;
		static ptxKeyframeDefn sm_xyzMaxDefn;

};


} // namespace rage


#endif // PTXU_ACCELERATION_H 
