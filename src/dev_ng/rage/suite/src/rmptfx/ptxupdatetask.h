// 
// rmptfx/ptxupdatetask.h 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXUPDATETASK_H
#define RMPTFX_PTXUPDATETASK_H


// includes (us)
#include "rmptfx/ptxeffectinst.h"

// includes (rage)
#include "atl/bitset.h"
#include "system/task.h"


// namespaces
namespace rage
{


// defines


// structures
struct ptxEmitterInstUpdateItem
{
	static bool SortCompare(const ptxEmitterInstUpdateItem& a, const ptxEmitterInstUpdateItem& b) 
	{ 
		ptxEffectInst* pEffectInstA = a.m_pEffectInstItem->GetDataSB();
		ptxEffectInst* pEffectInstB = b.m_pEffectInstItem->GetDataSB();

		// sort by when we can update
		bool cbUpdateA = pEffectInstA->GetNeedsCallbackUpdate();
		bool cbUpdateB = pEffectInstB->GetNeedsCallbackUpdate();
		if (cbUpdateA!=cbUpdateB)
		{
			// since they are not equal, consider "a < b" true when cbUpdateA == true and cbUpdateB == false
			return cbUpdateA; 
		}
		else
		{
			// since they are equal, it doesn't matter which we return here, just return false
			return false; 
		}
	}

	ptxEffectInstItem* m_pEffectInstItem;
	int m_eventIdx;
};

struct
ALIGNAS(16)
ptxUpdateTaskInputData
{
	float m_deltaTime;

	u32 m_numEmitterInstUpdateItems;
	ptxEmitterInstUpdateItem* m_pEmitterInstUpdateItems;
	volatile u32* m_pCurrEmitterInstUpdateItemIdx;
	u8 m_ptxUpdateTaskIdx;
};


// classes
class ptxUpdateTask
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Init(int scheduler=-1);
		static void Update(float dt, u32 lastUpdateItemIdx);
		static void WaitForAll();
		static int GetTaskCount() {return sm_taskCount;}
		static sysIpcCurrentThreadId GetPtxUpdateTaskThreadID(u8 index) {return sm_taskThreadIDs[index];}
		static __forceinline int GetPtxUpdateTaskIdx(sysIpcCurrentThreadId threadID) 
		{
			//access the cached thread ID for the thread and return the index. That will be the particle Task number
			for(int i=0; i<RMPTFX_MAX_PARTICLE_UPDATE_TASKS; i++)
			{
				if(sm_taskThreadIDs[i] == threadID)
				{
					return i;
				}
			}
			return -1;
		}
		static void SetPtxUpdateTaskThreadID(u8 index, sysIpcCurrentThreadId threadID) {sm_taskThreadIDs[index] = threadID;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		static int sm_schedulerIndex;
		static u32 sm_taskCount;
		static ptxUpdateTaskInputData sm_updateInputData[RMPTFX_MAX_PARTICLE_UPDATE_TASKS];
		static sysTaskHandle sm_taskHandles[RMPTFX_MAX_PARTICLE_UPDATE_TASKS];
		static sysIpcCurrentThreadId sm_taskThreadIDs[RMPTFX_MAX_PARTICLE_UPDATE_TASKS];


};


} // namespace rage


#endif // RMPTFX_PTXUPDATETASK_H
