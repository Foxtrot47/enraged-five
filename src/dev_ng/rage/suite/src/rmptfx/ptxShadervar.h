// 
// rmptfx/ptxshadervar.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXSHADERVAR_H
#define RMPTFX_PTXSHADERVAR_H


// includes (us)
#include "rmptfx/ptxkeyframe.h"

// includes
#include "grcore/texture.h"
#include "grmodel/shader.h"
#include "paging/ref.h"

// namespaces
namespace rage
{


// forward declarations
class grmShader;
class ptxParticleRule;
class ptxByteBuff;


// enumerations
enum ptxShaderVarType 
{
	PTXSHADERVAR_BOOL			= 0,				// not used at the moment - could be removed but will need to change the editor code and data to match
	PTXSHADERVAR_INT,								// not used at the moment - could be removed but will need to change the editor code and data to match
	PTXSHADERVAR_FLOAT,
	PTXSHADERVAR_FLOAT2,
	PTXSHADERVAR_FLOAT3,
	PTXSHADERVAR_FLOAT4,
	PTXSHADERVAR_TEXTURE,
	PTXSHADERVAR_KEYFRAME,

	PTXSHADERVAR_COUNT,
	PTXSHADERVAR_INVALID,
};

enum ptxShaderProgVarType
{
	PTXPROGVAR_FIRST			= 0,

	PTXPROGVAR_FRAMEMAP			= 0,
	PTXPROGVAR_BLENDMODE,
	PTXPROGVAR_CHANNELMASK,
	PTXPROGVAR_RMPTFX_LAST = PTXPROGVAR_CHANNELMASK,
	PTXPROGVAR_GAME_FIRST,
	PTXPROGVAR_GAME_VAR_1 = PTXPROGVAR_GAME_FIRST,
	PTXPROGVAR_GAME_VAR_2,
	PTXPROGVAR_GAME_VAR_3,
	PTXPROGVAR_GAME_VAR_4,
	PTXPROGVAR_GAME_VAR_5,
	PTXPROGVAR_GAME_VAR_6,
	PTXPROGVAR_GAME_VAR_7,
	PTXPROGVAR_GAME_VAR_8,
	PTXPROGVAR_GAME_VAR_9,
	PTXPROGVAR_COUNT,
	PTXPROGVAR_RMPTFX_COUNT = PTXPROGVAR_RMPTFX_LAST + 1,
	PTXPROGVAR_GAME_COUNT = PTXPROGVAR_COUNT - PTXPROGVAR_RMPTFX_COUNT
};


// classes
class ptxShaderVarUtils
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static const char* GetVarTypeName(ptxShaderVarType type);
		static const char* GetProgrammedVarName(ptxShaderProgVarType type);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		static const char* sm_varTypeNameTable[PTXSHADERVAR_COUNT];
		static const char* sm_progVarNameTable[PTXPROGVAR_RMPTFX_COUNT];

};


class ALIGNAS(16) ptxShaderVar : public datBase
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////
		
		// constructor / destructor
		ptxShaderVar();
		virtual ~ptxShaderVar();

		// resourcing
		ptxShaderVar(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxShaderVar);
		virtual void PostLoad() {}

		// virtual interface
		virtual void OnPreLoadBase(parTreeNode*);
		virtual void OnPostSaveBase(parTreeNode*);
		virtual void ClearShaderVar(grmShader* UNUSED_PARAM(pShader)) {};
		virtual void SetShaderVar(grmShader* UNUSED_PARAM(pShader)) {};
		virtual void SetDefaultData(grmShader* UNUSED_PARAM(pShader)) {};
		virtual void SetData() {};
		virtual ptxShaderVar* Clone() {return NULL;}	
		virtual void CopyUiDataFromTemplate(ptxShaderVar* pShaderVar);

		// editor
#if RMPTFX_EDITOR
		virtual void SendToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);
		virtual void ReceiveFromEditor(const ptxByteBuff& UNUSED_PARAM(buff)) {};
		void SendDefnToEditor(ptxByteBuff& buff);
#endif

		// accessors
		void SetInfo(const grmVariableInfo& varInfo);
		const grmVariableInfo* GetInfo() const {return m_pInfo;};
		void SetId(grcEffectVar id) {m_id = id;};
		grcEffectVar GetId() const {return m_id;};
		bool GetIsKeyframeable() const {return m_isKeyframeable;};
		bool GetOwnsInfo() const {return m_ownsInfo;};
		void SetHashName(u32 hashName) {m_hashName = hashName;};
		u32 GetHashName() const {return m_hashName;}
		void SetType(ptxShaderVarType type) {m_type = type;};
		ptxShaderVarType GetType() const {return static_cast<ptxShaderVarType>(m_type);};
		grcEffect::VarType GetGRCType() const {return (m_pInfo ? (m_pInfo->m_Type) : (grcEffect::VT_NONE));};

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	protected: ////////////////////////

		grmVariableInfo* m_pInfo;
		u32 m_hashName;
		ptxShaderVarType m_type;	
		grcEffectVar m_id;				
		bool m_isKeyframeable;	
		bool m_ownsInfo;
		datPadding<10> m_pad;
} ;


class ptxShaderVarVector : public ptxShaderVar 
{
	friend class ptxShaderVar;

	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////	

	public: ///////////////////////////

		// constructor / destructor
		ptxShaderVarVector();
		ptxShaderVarVector(ptxShaderVarType type);

		// resourcing
		ptxShaderVarVector(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxShaderVarVector);

		// virtual interface
		void SetShaderVar(grmShader* pShader);
		void SetDefaultData(grmShader* pShader) {pShader->GetVar(m_id, RC_VECTOR4(m_vector));};
		ptxShaderVar* Clone();

		// derived interface
		void SetFloat(float a) {m_vector = Vec4V(a, 0.0f, 0.0f, 0.0f);};
		void SetFloat2(float a, float b) {m_vector = Vec4V(a, b, 0.0f, 0.0f);};
		void SetFloat3(float a, float b, float c) {m_vector = Vec4V(a, b, c, 0.0f);};
		void SetFloat4(float a, float b, float c, float d) {m_vector = Vec4V(a, b, c, d);};

		// editor
#if RMPTFX_EDITOR
		void SendToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif

#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////	

	private: //////////////////////////

		Vec4V m_vector;

};


class ptxShaderVarKeyframe : public ptxShaderVar 
{
	friend class ptxShaderInst;

	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////	

	public: ///////////////////////////

		// constructor / destructor
		ptxShaderVarKeyframe();
		~ptxShaderVarKeyframe();

		// resourcing
		ptxShaderVarKeyframe(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxShaderVarKeyframe);

		// virtual interface
		void SetDefaultData(grmShader* pShader);
		ptxShaderVar* Clone();
		void CopyUiDataFromTemplate(ptxShaderVar* pShaderVar);

		// derived interface
		ptxKeyframe& GetKeyframe() {return m_keyframe;};

#if RMPTFX_EDITOR
		virtual void SendToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);
		void ReceiveFromEditor(const ptxByteBuff& buff);										// not required as the keyframes get received through the generic interface
#endif

	// parser
#if RMPTFX_XML_LOADING
	PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////	

	private: //////////////////////////

		ptxKeyframe m_keyframe;

};


class ptxShaderVarTexture  : public ptxShaderVar 
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor / destructor
		ptxShaderVarTexture();
		~ptxShaderVarTexture();

		// resourcing
		ptxShaderVarTexture(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxShaderVarTexture);
		virtual void PostLoad();

		// loading
		void OnPreLoad(parTreeNode* pNode);
		void OnPostLoad();

		// virtual interface
		void ClearShaderVar(grmShader* pShader) {pShader->SetVar(m_id, (grcTexture*)(NULL));};
		void SetShaderVar(grmShader* pShader) {pShader->SetVar(m_id, m_pTexture);};
		void SetData();
		ptxShaderVar* Clone();
		
		// derived interface
		void SetTexture(grcTexture* pTexture, bool isExternal);
		void SetTexture(const char* pName);
		void SetTexture(const char* pName, const char* pPath);

		const char* GetTextureName() const {return m_textureName;}
		u32 GetTextureHashName() const {return m_textureHashName;};

#if RMPTFX_BANK
		const grcTexture* GetTexture() const {return m_pTexture;};
#endif

		// editor
#if RMPTFX_EDITOR
		virtual void SendToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif

	// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		pgRef<grcTexture> m_pTexture;
		ConstString m_textureName;
		u32 m_textureHashName;
		bool m_externalReference;
		datPadding<3> m_pad;

};

} // namespace rage

#endif // RMPTFX_PTXSHADERVAR_H
