// 
// rmptfx/ptxu_matrixweight.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "rmptfx/ptxu_matrixweight.h"
#include "rmptfx/ptxu_matrixweight_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_MatrixWeight::sm_mtxWeightDefn(			"Matrix Weight",			atHashValue("ptxu_MatrixWeight:m_mtxWeightKFP"),			PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Min Weight",	"Max Weight");


// code
#if RMPTFX_XML_LOADING
ptxu_MatrixWeight::ptxu_MatrixWeight()
{
	// create keyframes
	m_mtxWeightKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_MatrixWeight:m_mtxWeightKFP"));

	m_keyframePropList.AddKeyframeProp(&m_mtxWeightKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_MatrixWeight::SetDefaultData()
{
	// default keyframes
	m_mtxWeightKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_referenceSpace = PTXU_MATRIXWEIGHT_REFERENCESPACE_EFFECT;
}

ptxu_MatrixWeight::ptxu_MatrixWeight(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_mtxWeightKFP(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_MatrixWeight_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_MatrixWeight::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_MatrixWeight, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_MatrixWeight, m_mtxWeightKFP)
		SSTRUCT_FIELD(ptxu_MatrixWeight, m_referenceSpace)
		SSTRUCT_IGNORE(ptxu_MatrixWeight, m_pad)
	SSTRUCT_END(ptxu_MatrixWeight)
}
#endif

IMPLEMENT_PLACE(ptxu_MatrixWeight);

void ptxu_MatrixWeight::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointMtxWeight)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_MatrixWeight::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	
	ptxParticleRule* pParticleRule = params.pEmitterInst->GetParticleRule();

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// get the particle and emitter life ratio key times
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();
	ScalarV vKeyTimePointEmitter = ScalarVFromF32(pPointSB->GetEmitterLifeRatio());

	// cache some data that we'll need later
	ptxEmitterRule*	RESTRICT pEmitterRule = static_cast<ptxEmitterRule*>(pEmitterInst->GetEmitterRule());
	const ScalarV vDiv100 = ScalarV(V_FLT_SMALL_2);

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vEmitMatrixWeight = pEmitterRule->GetMatrixWeightScalarKFP().Query(vKeyTimePointEmitter, pEvolutionList, pPointSB->GetEvoValues()) * vDiv100;
	Vec4V vPtxMatrixWeight = m_mtxWeightKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());

	// calculate the bias value
	ScalarV vBias = GetBiasValue(m_mtxWeightKFP, pPointSB->GetRandIndex());

	// calculate the lerped matrix weight between the min and max keyframe data
	ScalarV vMatrixWeight = Lerp(vBias, SplatX(vPtxMatrixWeight), SplatY(vPtxMatrixWeight));

	// scale this by the emit matrix weight
	vMatrixWeight *= SplatX(vEmitMatrixWeight);

	// scale by the ratio of the frame that the particle has been alive
	vMatrixWeight *= params.vPtxFrameRatio;

	// get the velocity of the thing we're attached to (effect or emitter)
	Vec3V vVelocity = Vec3V(V_ZERO);
	if(!pParticleRule->GetIsScreenSpace())
	{
		if (m_referenceSpace==PTXU_MATRIXWEIGHT_REFERENCESPACE_EFFECT)
		{
			vVelocity = pEffectInst->GetCurrPos() - pEffectInst->GetPrevPos();;
		}
		else if (m_referenceSpace==PTXU_MATRIXWEIGHT_REFERENCESPACE_EMITTER)
		{	
			vVelocity = pEmitterInst->GetDataDB()->GetCurrPosWld() - pEmitterInst->GetPrevPosWld();
		}
		else
		{
			ptxAssertf(0, "unsupported reference space");
		}
	}

	// scale this velocity by the matrix weight
	vVelocity = vVelocity * Vec3V(vMatrixWeight);

	// update the particle's position data
	pPointDB->IncPrevPos(vVelocity);
	pPointDB->IncCurrPos(vVelocity);
	pPointDB->IncSpawnPos(vVelocity);

	// store matrix weight on the particle
	pPointDB->SetMatrixWeight(vMatrixWeight);
}

void ptxu_MatrixWeight::SetKeyframeDefns()
{
	m_mtxWeightKFP.GetKeyframe().SetDefn(&sm_mtxWeightDefn);
}

void ptxu_MatrixWeight::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_mtxWeightKFP, &sm_mtxWeightDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_MatrixWeight::IsActive()
{
	ptxu_MatrixWeight* pMatrixWeight = static_cast<ptxu_MatrixWeight*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_mtxWeightKFP == pMatrixWeight->m_mtxWeightKFP &&
				   m_referenceSpace == pMatrixWeight->m_referenceSpace;

	return !isEqual;
}

void ptxu_MatrixWeight::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 2);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Matrix Weight");

	// send non-keyframes
	const char* pComboItemNames[PTXU_MATRIXWEIGHT_REFERENCESPACE_NUM] = {"Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Reference Space", PTXU_MATRIXWEIGHT_REFERENCESPACE_NUM, pComboItemNames);
}

void ptxu_MatrixWeight::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_mtxWeightKFP, pParticleRule);

	// send non-keyframes
	SendComboItemToEditor(buff, m_referenceSpace);
}

void ptxu_MatrixWeight::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_mtxWeightKFP);

	// receive non-keyframes
	ReceiveComboItemFromEditor(buff, m_referenceSpace);
}
#endif // RMPTFX_EDITOR

} // namespace rage
