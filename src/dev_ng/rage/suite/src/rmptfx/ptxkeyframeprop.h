// 
// rmptfx/ptxkeyframeprop.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RMPTFX_PTXKEYFRAMEPROP_H 
#define RMPTFX_PTXKEYFRAMEPROP_H 


// includes (rmptfx)
#include "rmptfx/ptxchannel.h"
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxevolution.h"
#include "rmptfx/ptxkeyframe.h"
#include "rmptfx/ptxrandomtable.h"

// includes (rage)
#include "data/base.h"
#include "data/struct.h"
#include "parser/macros.h"
#include "vectormath/classes.h"
#include "vectormath/classfreefuncsv.h"


// namespaces
namespace rage 
{


// forward declarations
class ptxBiasLink;
class ptxEvolvedKeyframeProp;
class ptxKeyframe;
class ptxParticleRule;

//Max amount of evolved keyframe prop pointers so there wont be any concurrency issues
#define PTXEVO_KFP_THREAD_MAX (12)

//Defining enum to keep track of all the max number of threads possible that could access an evolved keyframeProp
//It's mainly used for a compile time assert
enum ptxEvolutionKeyframeThread
{
	PTXEVO_KFP_THREAD_MAIN = 0, 																									//Main Thread
	PTXEVO_KFP_THREAD_RMPTFX_UPDATE,																								//RMPTFX Update Thread
	PTXEVO_KFP_THREAD_PARTICLE_THREAD_FIRST,																						//Particle Update Tasks
	PTXEVO_KFP_THREAD_PARTICLE_THREAD_LAST = RMPTFX_MAX_PARTICLE_UPDATE_TASKS + PTXEVO_KFP_THREAD_PARTICLE_THREAD_FIRST - 1,
	PTXEVO_KFP_THREAD_SUBRENDER_THREAD_FIRST,																						//Render Threads
	PTXEVO_KFP_THREAD_SUBRENDER_THREAD_LAST = PTXEVO_KFP_THREAD_SUBRENDER_THREAD_FIRST + NUMBER_OF_RENDER_THREADS - 1,
	PTXEVO_KFP_THREAD_TOTAL = PTXEVO_KFP_THREAD_SUBRENDER_THREAD_LAST + 1
};


// classes
class ptxKeyframeProp
	: public datBase
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxKeyframeProp();
		virtual ~ptxKeyframeProp();

		// resourcing
		ptxKeyframeProp(datResource& rsc);
#if __DECLARESTRUCT
		virtual void DeclareStruct(datTypeStruct& s);
#endif
//		DECLARE_PLACE(ptxKeyframeProp);

		// inline functions
		__forceinline void Init(Vec4V_In vValue, u32 propertyId)
		{		
			m_keyframe.Init(ScalarV(V_ZERO), vValue);
			m_propertyId = propertyId;
		}
		__forceinline void PrepareEvoData(const ptxEvolutionList* pEvolutionList)
		{
#if __ASSERT
			Assertf(ms_ptxKeyframePropThreadId > -1, "ptxKeyframeProp::ms_ptxKeyframePropThreadId is not setup for this thread. ptxManager::SetupPtxKeyFramePropThreadId should be called for this thread");
			//Indicate that we have cached the evolvedKeyframe prop for this thread
			ptxKeyframeProp::ms_ptxKeyframePropEvoSetup = true;
#endif
			m_pEvolvedKeyframeProp[ms_ptxKeyframePropThreadId] = pEvolutionList ? pEvolutionList->GetEvolvedKeyframePropById(m_propertyId) : NULL;
		}

		__forceinline Vec4V_Out Query(ScalarV_In vKeyTime, ptxEvolutionList* pEvolutionList, const ptxEvoValueType* pEvoValues)
		{
			if(pEvolutionList)
			{
				return pEvolutionList->Query(this, vKeyTime, pEvoValues);
			}

			return this->Query(vKeyTime);
		}
		
		__forceinline Vec4V_Out Query(ScalarV_In vKeyTime)
		{
			ptxAssertf(vKeyTime.Getf()>=0.0f && vKeyTime.Getf()<=1.0f, "querying a keyframe with an invalid time (%.3f)", vKeyTime.Getf());
			return m_keyframe.Query(vKeyTime);
		}

		// accessors
		ptxEvolvedKeyframeProp* GetEvolvedKeyframeProp() 
		{
			Assertf(ms_ptxKeyframePropThreadId > -1, "ptxKeyframeProp::ms_ptxKeyframePropThreadId is not setup for this thread. ptxManager::SetupPtxKeyFramePropThreadId should be called for this thread");
			Assertf(ms_ptxKeyframePropEvoSetup, "PrepareEvoData is not setup for this thread");
			return m_pEvolvedKeyframeProp[ms_ptxKeyframePropThreadId];
		}
		void SetPropertyId(u32 propertyId) {m_propertyId = propertyId;}
		u32 GetPropertyId() {return m_propertyId;}
		void SetRandIndex(char val) {m_randIndex = val;}
		char GetRandIndex() {return m_randIndex;}
		bool GetInvertBiasLink() {return m_invertBiasLink;}
		ptxKeyframe& GetKeyframe() {return m_keyframe;}
		
		// editor
#if RMPTFX_EDITOR
		bool operator==(const ptxKeyframeProp& other);

		void UpdateBehaviourDefn(ptxKeyframeDefn* pKeyframeDefn, const ptxParticleRule* pParticleRule);

		void SendToEditor(ptxByteBuff& buff, const char* pRuleName, ptxRuleType ruleType);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif

public:
	static __THREAD s8 ms_ptxKeyframePropThreadId;								//Used for storing the thread ID in TLS and accessing the cached m_pEvolvedKeyframeProp
#if __ASSERT
	static __THREAD bool ms_ptxKeyframePropEvoSetup;							//Used for making sure that PrepareEvoData is called for that specific thread before it gets used
#endif

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		ptxEvolvedKeyframeProp *m_pEvolvedKeyframeProp[PTXEVO_KFP_THREAD_MAX];	// pointer to any evolution data that this keyframed property has on each thread
		u32 m_propertyId;														// the id of the property we're keyframing (usually set a hash of the class and variable name)
		bool m_invertBiasLink;													// whether any bias linking on this property should be inverted
		char m_randIndex;														// an index into the random table that this property uses
		datPadding<2> m_pad;
		ptxKeyframe m_keyframe;													// the keyframe data for this property
};

class ptxKeyframePropList
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// constructor/destructor
		ptxKeyframePropList() {}
		~ptxKeyframePropList() {};

		// resourcing
		ptxKeyframePropList(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxKeyframePropList);
	
		// 
		void Reserve(int numKeyframeProps);
		void AddKeyframeProp(ptxKeyframeProp* pKeyframeProp, ptxKeyframeDefn* pKeyframeDefn);
		void PrepareEvoData(ptxEvolutionList* pEvolutionList);
		void SetBiasLinks(atArray<ptxBiasLink>& biasLinks);
		
		//static functions
		__forceinline static ScalarV_Out GetBiasValue(ptxKeyframeProp& keyframeProp, u8 randIndex) {const ScalarV vRandVal=ptxRandomTable::GetValueV(u8(randIndex+keyframeProp.GetRandIndex())); const BoolV invertBiasLink(keyframeProp.GetInvertBiasLink()); return SelectFT(invertBiasLink, vRandVal, ScalarV(V_ONE)-vRandVal);}

		// accessors
		int GetNumKeyframeProps() {return m_pKeyframeProps.GetCount();}
		ptxKeyframeProp* GetKeyframePropByIndex(int idx) {return m_pKeyframeProps[idx];}
		ptxKeyframeProp* GetKeyframePropByPropId(u32 keyframePropId);

		// editor
#if RMPTFX_EDITOR
		void SendToEditor(ptxByteBuff& buff, const char* pRuleName, ptxRuleType ruleType);
		void ReceiveFromEditor(const ptxByteBuff& buff);
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		atArray<datRef<ptxKeyframeProp> > m_pKeyframeProps;						// array of pointers to keyframed properties

};


} // namespace rage


#endif // RMPTFX_PTXKEYFRAMEPROP_H 
