<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxd_Sprite" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<Vector3 name="m_alignAxis"/>
	<int name="m_alignmentMode"/>
	<float name="m_flipChanceU"/>
	<float name="m_flipChanceV"/>
	<float name="m_nearClipDist"/>
	<float name="m_farClipDist"/>
	<float name="m_projectionDepth"/>
  <float name="m_shadowCastIntensity"/>
  <bool name="m_isScreenSpace"/>
	<bool name="m_isHiRes"/>
	<bool name="m_nearClip"/>
	<bool name="m_farClip"/>
	<bool name="m_uvClip"/>
  <bool name="m_disableDraw"/>
</structdef>

</ParserSchema>