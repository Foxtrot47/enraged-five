// 
// rmptfx/ptxu_colour.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "rmptfx/ptxu_colour.h"
#include "rmptfx/ptxu_colour_parser.h"

#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_BEHAVIOUR_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Colour::sm_rgbaMinDefn(					"RGBA Min", 				atHashValue("ptxu_Colour:m_rgbaMinKFP"),					PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red Min",		"Green Min",	"Blue Min",		"Alpha Min",	true);
ptxKeyframeDefn ptxu_Colour::sm_rgbaMaxDefn(					"RGBA Max", 				atHashValue("ptxu_Colour:m_rgbaMaxKFP"),					PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red Max",		"Green Max",	"Blue Max",		"Alpha Max",	true);
ptxKeyframeDefn ptxu_Colour::sm_emissiveIntensityDefn(			"Emissive Intensity",		atHashValue("ptxu_Colour:m_emissiveIntensityKFP"),			PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		512.0f,		0.1f),		"Emissive Intensity Min",		"Emissive Intensity Max");


// code
#if RMPTFX_XML_LOADING
ptxu_Colour::ptxu_Colour()
{
	// create keyframes
	m_rgbaMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Colour:m_rgbaMinKFP"));
	m_rgbaMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Colour:m_rgbaMaxKFP"));
	m_emissiveIntensityKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Colour:m_emissiveIntensityKFP"));

	m_keyframePropList.AddKeyframeProp(&m_rgbaMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_rgbaMaxKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_emissiveIntensityKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Colour::SetDefaultData()
{
	// default keyframes
	m_rgbaMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_rgbaMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_emissiveIntensityKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_keyframeMode = PTXU_COLOUR_KEYFRAMEMODE_POINT;

	m_rgbaMaxEnable = false;
	m_rgbaProportional = true;
	m_rgbCanTint = false;

}

ptxu_Colour::ptxu_Colour(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_rgbaMinKFP(rsc)
	, m_rgbaMaxKFP(rsc)
	, m_emissiveIntensityKFP(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxu_Colour_num++;
#endif
}

#if __DECLARESTRUCT
void ptxu_Colour::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Colour, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Colour, m_rgbaMinKFP)
		SSTRUCT_FIELD(ptxu_Colour, m_rgbaMaxKFP)
		SSTRUCT_FIELD(ptxu_Colour, m_emissiveIntensityKFP)
		SSTRUCT_FIELD(ptxu_Colour, m_keyframeMode)
		SSTRUCT_FIELD(ptxu_Colour, m_rgbaMaxEnable)
		SSTRUCT_FIELD(ptxu_Colour, m_rgbaProportional)
		SSTRUCT_FIELD(ptxu_Colour, m_rgbCanTint)
		SSTRUCT_IGNORE(ptxu_Colour, m_pad)
	SSTRUCT_END(ptxu_Colour)
}
#endif

IMPLEMENT_PLACE(ptxu_Colour);

void ptxu_Colour::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointColour)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Colour::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// get the particle and emitter life ratio key times
	ScalarV vKeyTime = pPointDB->GetKeyTime();
	const ScalarV vKeyTimeEffect = pEffectInst->GetKeyTime();
	if (m_keyframeMode==PTXU_COLOUR_KEYFRAMEMODE_EFFECT)
	{
		// override with effect life ratio if requested
		vKeyTime = vKeyTimeEffect;
	}
	else if (m_keyframeMode==PTXU_COLOUR_KEYFRAMEMODE_EMITTER)
	{
		// override with emitter life ratio if requested
		vKeyTime = pEmitterInst->GetKeyTime();
	}
	else
	{
		ptxAssertf(m_keyframeMode==PTXU_COLOUR_KEYFRAMEMODE_POINT, "unsupported keyframe mode");
	}

	// cache some data that we'll need later
	ptxEffectRule* pEffectRule = pEffectInst->GetEffectRule();
	const u8 randIndex = pPointSB->GetRandIndex();
	ScalarV vPtxAlphaMult = pPointDB->GetAlphaMultV();

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	ptxEvolutionList* pEvolutionListEffect = pEffectRule->GetEvolutionList();
	ptxEvoValueType* pEvoValues = pPointSB->GetEvoValues();
	Vec4V vRGBAMinPtx = m_rgbaMinKFP.Query(vKeyTime, pEvolutionList, pEvoValues);
	Vec4V vRGBAMaxPtx = m_rgbaMaxKFP.Query(vKeyTime, pEvolutionList, pEvoValues);
	Vec4V vRGBAMinEffect = pEffectRule->GetColourTintMinKFP().Query(vKeyTimeEffect, pEvolutionListEffect, pEvoValues);
	Vec4V vRGBAMaxEffect = pEffectRule->GetColourTintMaxKFP().Query(vKeyTimeEffect, pEvolutionListEffect, pEvoValues);
	Vec4V vEmissiveIntensityMinMax = m_emissiveIntensityKFP.Query(vKeyTime, pEvolutionList, pEvoValues);

	Vec4V vBiasRGBAPtx;
	if (m_rgbaProportional)
	{
		vBiasRGBAPtx = Vec4V(GetBiasValue(m_rgbaMinKFP, randIndex));
	}
	else
	{
		vBiasRGBAPtx = Vec4V(GetBiasValue(m_rgbaMinKFP, randIndex), 
			GetBiasValue(m_rgbaMinKFP, randIndex+1), 
			GetBiasValue(m_rgbaMinKFP, randIndex+2), 
			GetBiasValue(m_rgbaMinKFP, randIndex+3));
	}

	const ScalarV vBiasRGBAEffect = ptxRandomTable::GetValueV(pEffectInst->GetRGBARandIndex());
	const ScalarV vBiasEmissiveIntensity = GetBiasValue(m_emissiveIntensityKFP, randIndex);

	// calculate the lerped rgba and emissive intensity between the min and max keyframe data
	Vec4V vRGBA = vRGBAMinPtx;
	if (m_rgbaMaxEnable)
	{
		vRGBA = Lerp(vBiasRGBAPtx, vRGBAMinPtx, vRGBAMaxPtx);
	}
	const ScalarV vEmissiveIntensity = Lerp(vBiasEmissiveIntensity, SplatX(vEmissiveIntensityMinMax), SplatY(vEmissiveIntensityMinMax));

	// apply the emitter inst tint
	vRGBA *= pEmitterInst->GetColourTintRGBA();

	// apply the effect inst tint
	if (pEffectInst && pEffectInst->GetSpawnedEffectScalars().GetIsColourTintScalarActive())
	{
		// this is active if the effect has been spawned from another effect
		vRGBA *= pEffectInst->GetSpawnedEffectScalars().GetColourTintScalar().GetRGBA();
	}
	else
	{
		Vec4V vRGBAEffect = vRGBAMinEffect;
		if (pEffectRule->GetColourTintMaxEnable())
		{
			vRGBAEffect = Lerp(vBiasRGBAEffect, vRGBAMinEffect, vRGBAMaxEffect);
		}

		vRGBA *= vRGBAEffect;
	}

	// apply the effect inst user tint (where the alpha also contains the lod fade) and alpha multiplier
	const ScalarV alpha = pEffectInst->GetAlphaTintV() * ScalarVFromF32(pEffectInst->GetLodAlphaMult()) * vPtxAlphaMult;

	if (Unlikely(m_rgbCanTint))
	{
		vRGBA *= Vec4V(pEffectInst->GetColourTint(), ScalarV(V_ONE));
	}

	// apply alpha multiplier
	vRGBA *= Vec4V(Vec3V(V_ONE), alpha);

	// set the final colour and emissive intensity on the particle
	pPointDB->SetColour(vRGBA);
	pPointDB->SetEmissiveIntensityV(vEmissiveIntensity);
}

void ptxu_Colour::SetKeyframeDefns()
{
	m_rgbaMinKFP.GetKeyframe().SetDefn(&sm_rgbaMinDefn);
	m_rgbaMaxKFP.GetKeyframe().SetDefn(&sm_rgbaMaxDefn);
	m_emissiveIntensityKFP.GetKeyframe().SetDefn(&sm_emissiveIntensityDefn);
}

void ptxu_Colour::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_rgbaMinKFP, &sm_rgbaMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_rgbaMaxKFP, &sm_rgbaMaxDefn);
	pEvolutionList->SetKeyframeDefn(&m_emissiveIntensityKFP, &sm_emissiveIntensityDefn);
}


// editor code
#if RMPTFX_EDITOR
void ptxu_Colour::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 7);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "RGBA Min");
	SendKeyframeDefnToEditor(buff, "RGBA Max");
	SendKeyframeDefnToEditor(buff, "Emissive Intensity");

	// send non-keyframes
	const char* pComboItemNames[PTXU_COLOUR_KEYFRAMEMODE_NUM] = {"Point", "Effect", "Emitter"};
	SendComboDefnToEditor(buff, "Keyframe Mode", PTXU_COLOUR_KEYFRAMEMODE_NUM, pComboItemNames);

	SendBoolDefnToEditor(buff, "RGBA Max Enable");
	SendBoolDefnToEditor(buff, "RGBA Proportional");
	SendBoolDefnToEditor(buff, "RGBA Can Tint");
}

void ptxu_Colour::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_rgbaMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_rgbaMaxKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_emissiveIntensityKFP, pParticleRule);

	// send non-keyframes
	SendComboItemToEditor(buff, m_keyframeMode);

	SendBoolToEditor(buff, m_rgbaMaxEnable);
	SendBoolToEditor(buff, m_rgbaProportional);
	SendBoolToEditor(buff, m_rgbCanTint);
}

void ptxu_Colour::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_rgbaMinKFP);
	ReceiveKeyframeFromEditor(buff, m_rgbaMaxKFP);
	ReceiveKeyframeFromEditor(buff, m_emissiveIntensityKFP);

	// receive non-keyframes
	ReceiveComboItemFromEditor(buff, m_keyframeMode);

	ReceiveBoolFromEditor(buff, m_rgbaMaxEnable);
	ReceiveBoolFromEditor(buff, m_rgbaProportional);
	ReceiveBoolFromEditor(buff, m_rgbCanTint);
}
#endif // RMPTFX_EDITOR


} // namespace rage
