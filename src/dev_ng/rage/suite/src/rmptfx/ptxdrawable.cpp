// 
// rmptfx/ptxdraw.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

// includes (us)
#include "rmptfx/ptxdrawable.h"
#include "rmptfx/ptxdrawable_parser.h"

// includes (rage)
#include "rmptfx/ptxconfig.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxmanager.h"


// defines
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


// code

// ptxDrawable
IMPLEMENT_PLACE(ptxDrawable);

#if __DECLARESTRUCT
void ptxDrawable::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxDrawable)
		SSTRUCT_FIELD(ptxDrawable, m_vBoundInfo)
		SSTRUCT_FIELD(ptxDrawable, m_name)
		SSTRUCT_FIELD(ptxDrawable, m_pDrawable)
		SSTRUCT_FIELD(ptxDrawable, m_hashName)
		SSTRUCT_IGNORE(ptxDrawable, m_pad)
	SSTRUCT_END(ptxDrawable)
}
#endif

ptxDrawable::ptxDrawable()
{
	m_pDrawable=NULL;
}

ptxDrawable::ptxDrawable(datResource& rsc)
: m_name(rsc)
, m_pDrawable(rsc)
{
#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxDrawable_num++;
#endif
}

void ptxDrawable::PostLoad()
{
	if (!m_pDrawable)
	{
		m_pDrawable = RMPTFXMGR.LoadModel(m_name);
	}
}

void ptxDrawable::SetInfo()
{
	// set the hash name
	m_hashName = atHashValue(m_name);
	if (m_pDrawable)
	{

		// set the bound info
		Vec3V vBBMin, vBBMax;
		m_pDrawable->GetLodGroup().GetBoundingBox(RC_VECTOR3(vBBMin), RC_VECTOR3(vBBMax));
		Vec3V vBBSize = vBBMax-vBBMin;
		m_vBoundInfo.SetXYZ(vBBSize);

		ScalarV vCullRadius = ScalarVFromF32(m_pDrawable->GetLodGroup().GetCullRadius());
		m_vBoundInfo.SetW(vCullRadius);
	}
	else
	{
		m_vBoundInfo = Vec4V(V_ZERO);
	}
}


// ptxDrawableEntry
IMPLEMENT_PLACE(ptxDrawableEntry);

ptxDrawableEntry::ptxDrawableEntry(datResource& UNUSED_PARAM(rsc))
{
	if (m_ownedByRMPTFX)
	{
		datResource::Fixup(m_pDrawable);
	}
	else
	{
		datResource::Place(m_pDrawable);
	}

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.ptxDrawableEntry_num++;
#endif
}

#if __DECLARESTRUCT
void ptxDrawableEntry::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(ptxDrawableEntry)
	SSTRUCT_IGNORE(ptxDrawableEntry, m_pDrawable)
	SSTRUCT_FIELD(ptxDrawableEntry, m_ownedByRMPTFX)
	SSTRUCT_IGNORE(ptxDrawableEntry, m_pad)
	SSTRUCT_END(ptxDrawableEntry)
}
#endif


} // namespace rage
